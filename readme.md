the code works on generated netcdf files.
given the original emc3 outputs, you can generate them yourself
and put them into atomdatpy/NetCDFs-New[/Ez]
otherwise you can get pregenerated files from 
https://gitlab.mpcdf.mpg.de/phg/EMC3Sim-Case-Emissivity-repository

atomdat     is mostly original data from Felix Reimold with some code edits to fit my needs
atomdatpy    is used to convert emc3 -> emissivity data 
    calc_and_insert_E    takes the emc3 data of each cell and uses the adas stuff to calculate ionization stages in each cell, and emissivities + radiation fraction coeffs from that  
    combination_load    looks at the emissivity distributions output by combination_save
    combination_save    loads the emc3 data. converts toi emiss. and formats the outputdata into netcdfs able to be used
Gabriele    is concerned with the data for the bolometry chapter and loads emc3emissivity data and partesotti grid sensitiviy data and maps them to each other.

    
