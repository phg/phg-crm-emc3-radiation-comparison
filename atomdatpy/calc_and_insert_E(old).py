import xemc3
import xarray as xr
import numpy as np
import sys
import pathlib

sys.path.append('/u/phg/adas')
from importlib import reload
import atomdat.adas.ibal_phg as ibal; reload(ibal)
import atomdat.adas.adf11 as adf11; reload(adf11)

# define data wrappers
PLT = adf11.ADF11()
PRB = adf11.ADF11()
_ = PLT.read('plt96/plt96_c.dat','/u/phg/adas/adf11/')
_ = PRB.read('prb96/prb96_c.dat','/u/phg/adas/adf11/')

def arbitrary_shape_adas(te, ne, idx, PLT=PLT, PRB=PRB, ne_default=1e20, te_default=1e2):
    # force inputs to numpy array
    ne, te = np.array(ne, dtype='float64'), np.array(te, dtype='float64')  
    # check that ne and te fit together
    assert ne.shape == te.shape  
    # find where anything is nan
    nanmask = np.isnan(ne) | np.isnan(te) 
    # mask them out so calc_fz doesn't crash
    ne[nanmask], te[nanmask] = ne_default, te_default 
    # flatten
    fne, fte = ne.flatten(), te.flatten()
    # ignore everything but lz and flatten inputs so the function can handle them
    plt = PLT.interpolate(ne=fne, te=fte, idx=idx)
    prb = PRB.interpolate(ne=fne, te=fte, idx=idx)
    # they are now a shape (length) array and we need to work with that
    # shape flat array into the same shape as the inputs again
    plt, prb = plt.reshape(ne.shape), prb.reshape(ne.shape) 
    # check for ability to reintroduce nans and do so
    assert plt.dtype == prb.dtype == np.dtype('float64')
    plt[nanmask], prb[nanmask] = np.nan, np.nan
    return plt, prb


def make_file(save_all=False, experiment_folder=None):
    felix_folder = "/u/flr/data/EMC3-runs/W7X/EIM-SC/Reference"
    philip_folder =  "/u/phg/adas/atomdatpy"

    # get grid dat
    try:
        ds = xr.open_dataset(f"{philip_folder}/{experiment_folder}.nc")
    except:
        ds = xemc3.load.all(f"{felix_folder}/{experiment_folder}")
        # if os.path.exists(f"{philip_folder}/{experiment_folder}.nc"): 
        #   os.remove(f"{philip_folder}/{experiment_folder}.nc")


    # get electron data for every voxel
    ne, te = np.array(ds["ne"]), np.array(ds["Te"])
    # calculate absolute and fractional densities
    nzs = np.zeros((7, *ds["nZ1"].shape))
    if ds.get("IMPURITY_NEUTRAL_0", None) is None:
        no_neutral = True
        print("NO IMPURITY_NEUTRAL_0, RESULTS MAY DIFFER")
    else:
        no_neutral = False
        nzs[0] = ds["IMPURITY_NEUTRAL_0"]

    for i in range(1,7):
        nzs[i] = ds[f"nZ{i}"]

    fAz = nzs / nzs.sum(axis=0)

    # create PLT and PRB arrays
    PLTa, PRBa = np.zeros((7, *ne.shape)), np.zeros((7, *ne.shape))
    for i in range(6):
        PLTa[i], PRBa[i] = arbitrary_shape_adas(te, ne, i)
        
        
    # from that calculate Ez ...
    Ez = ne * nzs * PLTa
    Ez[:-1] += ne * nzs[:-1] * PLTa[1:] # index offset problems
    # ... and Lztot
    Lz = np.nansum(Ez, axis=0) / ne / nzs.sum(axis=0)
    #Lz2 = fAz * PLTa
    #Lz2[:-1] += fAz[:-1] * PLTa[1:]  # the same with less loss of precision
    #Lz2 = Lz2.sum(axis=0)    
        
    ds = ds.assign_coords(Ionization = range(7))
    ds = ds.assign(Ez=(("Ionization", "r", "theta", "phi"), Ez),
                   Lz=(("r", "theta", "phi"), Lz))
    if no_neutral:
        ds["Ez"].attrs["No Neutrals"] = "neutral C density not given, assumed to be 0"
        ds["Lz"].attrs["No Neutrals"] = "neutral C density not given, assumed to be 0"
    else:
        ds["Ez"].attrs["No Neutrals"] = "neutral C density given"
        ds["Lz"].attrs["No Neutrals"] = "neutral C density given"

    if save_all:
        ds.to_netcdf(f"{philip_folder}/{experiment_folder}.nc")
    ds["Ez"].to_netcdf(f"{philip_folder}/{experiment_folder}_Ez.nc")

if __name__ == "__main__":
    # P = pathlib.Path("/u/flr/data/EMC3-runs/W7X/EIM-SC/Reference/")
    # for path in P.iterdir():
    P = [pathlib.Path("/u/flr/data/EMC3-runs/W7X/EIM-SC/Reference/N03.00_P05.00_D05_C04.50-TEST"),
         pathlib.Path("/u/flr/data/EMC3-runs/W7X/EIM-SC/Reference/N03.00_P05.00_D05_C03.50-TEST")]
    for path in P:
        if str(path.name)[0] == "N":
            try:
                print("started", path.name)
                make_file(save_all=True, experiment_folder=path.name)
                print("successfully", end=" ")
            except: ...
            print("finished", path.name)



        
        
        

        
        
        
        
        
        
        
        
        
        
        
        
        
        
        