import numpy as np
import matplotlib.pyplot as plt


def sigma4_3_5(E, q=8, n=0):
    '''
    Analytical formula for CX cross-section of Aq+ + H(1s) --> A(q-1)+ + H+ (q>8)
    Reaction 4.3.5 (p.171)
    E in keV/amu
    sigma in m-2
    '''
    if q < 8:
        print('CX rate invalid: Use 3.4.6')
    Etilde = E/q**(3./7.)
    A = [0.73362,2.9391E4,41.8648,7.1023E-3,3.4749E-6,1.1832E-10]
    sigma = 1.E-16*A[0]*np.log(A[1]/Etilde+A[2])
    tmp = 1+A[3]*Etilde+A[4]*Etilde**3.5+A[5]*Etilde**5.4
    sigma = sigma * q * 1E-4 #rescale from cm-2 to m-2
    Elim = 0.05
    if any(Etilde < Elim):
        print('Caution: Indeterminate Errors below threshold energies (0.05,0.005)keV/amu')
    return [sigma, Elim*q**(3./7.)]

def sigma4_3_3(E, q=0, n=0):
    '''
    Reaction 4.3.3 C6+ + H --> C5+ + H+
    E in keV/amu
    sigma in m-2
    '''
    A = [484.18,2.1585,3.4808E-4,5.3333E-9,4.6556,0.33755,0.81736,0.27874,1.8003E-6,7.1033E-2,0.53261]
    return sigma_ref1(E,A)

def sigma4_3_4(E, q=0, n=0):
    '''
    Reaction 4.3.4 O8+ + H --> O7+ + H+
    E in keV/amu
    sigma in m-2
    '''
    A = [54.535,0.27486,1.0104E-7,2.0745E-9,4.4416,7.6555E-3,1.1134,1.1621,0.15826,3.6613E-2,3.9741E-2]
    return sigma_ref1(E,A)

def sigma_ref1(E,A):
    '''
    Reaction 4.3.3 (p.170)
    E in keV/amu
    sigma in m-2
    '''
    Etilde = E
    sigma = 1.E-16*A[0]
    tmp1 = 1+A[2]*Etilde**2+A[3]*Etilde**A[4]+A[5]*Etilde**A[6]
    tmp1 = np.exp(-A[1]/Etilde**A[7])/tmp1
    tmp2 = A[8]*np.exp(-A[9]*Etilde)/Etilde**A[10]
    sigma = sigma*(tmp1+tmp2) * 1E-4
    Elim = 0.05
    if any(Etilde < Elim):
        print('Caution: Indeterminate Errors below threshold energies (0.05,0.005)keV/amu')
    return [sigma, Elim]


def sigmaCCD(E, q=1, n=1):
    '''
    Analitical formula for the CX cross-section. H+ + H(n) as a function of
    the kinetic energy E in keV/amu
    See page 19 and 167 of http://www.eirene.de/report_4105.pdf
    '''
    CoeffMatrix =np.array(\
    [[3.2345, 9.2750e-1, 3.7271e-1, 2.1336e-1],\
    [2.3588e+2, 6.5040e+3, 2.7645e+6, 1.0000e+10],\
    [2.3713, 2.0699e+1, 1.4857e+3, 1.3426e+6],\
    [3.8371e-2, 1.3405e-2, 1.5720e-3, 1.8184e-3],\
    [3.8068e-6, 3.0842e-6, 3.0842e-6, 3.0842e-6],\
    [1.1832e-10, 1.1832e-10, 1.1832e-10, 1.1832e-10]])

    Asub = CoeffMatrix[:,n-1]
    Etilde = E*n**2
    numerator = Asub[0]*n**4*np.log(Asub[1]/Etilde + Asub[2])
    denominator = 1. + Asub[3]*Etilde + Asub[4]*Etilde**(3.5) + Asub[5]*Etilde**(5.4)

    sigmaCX = 1.e-16*numerator/denominator *1E-4 # rescale from cm-2 to m-2
    return [sigmaCX,Elim]

def get_sigma(E,q,n,ID):
    sigma = {
            '3.4.3':sigma4_3_3,\
            '3.4.4':sigma4_3_4,\
            '3.4.5':sigma4_3_5,\
            }
             
    return sigma[ID](E,q,n)

def plot(q,n,ID):
    E = np.logspace(-5.,4.,100)
    sigma,Elim = get_sigma(E,q,n,ID)
    plt.loglog(E,sigma)
    plt.axvline(Elim,ls='--',c='gray')
    plt.xlabel('Normalized Energy [keV/amu]')
    plt.ylabel('Cross Section [m$^{-2}$]')
    return