;+
; PROJECT: ADAS
;
; NAME: XXELEM
;
; PURPOSE:
;       Convert atomic number (no. of electrons) into element name
;
; EXPLANATION:
;       This function is a direct translation of the FORTRAN routine
;       XXELEM.FOR. Given an elements atomic number it returns the
;       name of that element. If the name is given the nuclear charge
;       is returned.
;
; INPUT/OUTPUT:
;       IZ0     -       Integer - the atomic number of the element
;       ENAME   -       String - the name of the element (blank if IZ0
;                                is out of range).
;
; KEYWORD PARAMETERS:
;       return_z  - Assumes an element name is supplied and returns the
;                   atomic number.
;
; CALLS:
;       None
;
; SIDE EFFECTS:
;       None
;
; CATEGORY:
;       System routine.
;
; WRITTEN:
;       Tim Hammond (Tessella Support Services plc), 7th September 1995
;
; MODIFIED:
;       1.1     Tim Hammond (Tessella Support Services plc)     07/09/95
;       1.2     Hugh Summers                                    17/09/99
;               Extended element number to 92
;       1.3     Martin O'Mullane
;                 - Corrected spelling of phosphorus - no third o.
;       1.4     Martin O'Mullane
;                 - Add return_z keyword to switch operaation to
;                   returning the atomic number if the element name
;                   is supplied.
;       1.5     Stuart Henderson
;                 - Correct spelling of praseodymium.
;
; VERSION:
;       1.1     First release.
;       1.2     17/09/99
;       1.3     31-08-2012
;       1.4     09-09-2012
;       1.5     19-05-2015
;
;-----------------------------------------------------------------------------

PRO xxelem, iz0, ename, return_z=return_z

    names =  ['hydrogen',   'helium',     'lithium',      'beryllium',    'boron',      'carbon',     'nitrogen',   'oxygen',       $
              'fluorine',   'neon',       'sodium',       'magnesium',    'aluminium',  'silicon',    'phosphorus', 'sulphur',      $
              'chlorine',   'argon',      'potassium',    'calcium',      'scandium',   'titanium',   'vanadium',   'chromium',     $
              'manganese',  'iron',       'cobalt',       'nickel',       'copper',     'zinc',       'gallium',    'germanium',    $
              'arsenic',    'selenium',   'bromine',      'krypton',      'rubidium',   'strontium',  'yttrium',    'zirconium',    $
              'niobium',    'molybdenum', 'technetium',   'ruthenium',    'rhodium',    'palladium',  'silver',     'cadmium',      $
              'indium',     'tin',        'antimony',     'tellurium',    'iodine',     'xenon',      'cesium',     'barium',       $
              'lanthanum',  'cerium ',    'praseodymium', 'neodymium',    'promethium', 'samarium',   'europium',   'gadolinium',   $
              'terbium',    'dysprosium', 'holmium',      'erbium',       'thulium',    'ytterbium',  'lutetium',   'hafnium',      $
              'tantalum',   'tungsten',   'rhenium',      'osmium',       'iridium',    'platinum',   'gold',       'mercury',      $
              'thallium',   'lead',       'bismuth',      'polonium',     'astatine',   'radon',      'francium',   'radium',       $
              'actinium',   'thorium',    'protactinium', 'uranium']

    if keyword_set(return_z) then begin

       str = strcompress(strlowcase(ename), /remove_all)
       ind = where(strpos(names, str) NE -1, count)

       iz0 = -1
       if count EQ 1 then begin
          iz0 = ind[0] + 1
       endif else begin
          for j = 0, count-1 do begin
             if names[ind[j]] EQ str then iz0 = ind[j] + 1
          endfor
       endelse

       if iz0 EQ -1 then message, 'Ambiguous name or non-existent element', /continue

    endif else begin

       if (iz0 le 0) or (iz0 gt 92) then ename = '  ' else ename = names[iz0-1]

    endelse

END
