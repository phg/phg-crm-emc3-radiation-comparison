; Copyright (c) 2002, Strathclyde University.
;----------------------------------------------------------------------
;+
; PROJECT    :  ADAS
;
; NAME       :  xxcftr
;
; PURPOSE    :  Converts a configuration character string, such as occurs
;               in a specific ion file level list, between Eissner and
;               standard forms.
;
; ARGUMENTS  :  All output arguments will be defined appropriately.
;               Inputs will be converted to correct type, if possible,
;               internally without changing calling type.
;
;                xxcftr, in_cfg = in_cfg, out_cfg=out_cfg, type=type
;
;               NAME      I/O    TYPE    DETAILS
; REQUIRED   :  in_cfg     I     string  input configuration
;               out_cfg    I     string  output configuration
;               type       I     integer type of conversion
;                                    = 1 => standard form out, standard form in
;                                      2 => Eissner  form out, standard form in
;                                      3 => standard form out, Eissner  form in
;                                      4 => Eissner  form out, Eissner  form in
;
; KEYWORDS      None
;
;
; NOTES      :  Calls the fortran code.
;
;
; AUTHOR     :  Martin O'Mullane
;
; DATE       :  08-04-2002
;
;
; MODIFIED:
;       1.1     Martin O'Mullane
;               - First version.
;       1.2     Allan Whiteford
;               - Changed wrapper path to be just ADASFORT.
;       1.3     Martin O'Mullane
;               - Transfer configurations via bytes.
;       1.4     Martin O'Mullane
;               - Allow configuration strings up to 64 characters.
;
; VERSION:
;       1.1    08-04-2002
;       1.2    10-08-2004
;       1.3    28-10-2009
;       1.4    04-07-2012
;-
;----------------------------------------------------------------------



PRO xxcftr, in_cfg = in_cfg, out_cfg=out_cfg, type=type

fortdir = getenv('ADASFORT')
fortdir = fortdir

iflag = LONG(type)

; input string length is arbitrary - use 64 to transfer to fortran

if strlen(in_cfg) GT 64 then message, 'input configuration string too long'

incfg = string(replicate(32B, 64))
len   = strlen(in_cfg)
tmp   = strmid(in_cfg, 0, min([64,len]))
strput, incfg, tmp, 0
incfg =  long(byte(incfg))

str    = string(replicate(32B, 64))
outcfg = long(byte(str))


dummy = CALL_EXTERNAL(fortdir+'/xxcftr_if.so','xxcftr_if', $
                      incfg, outcfg, iflag)

if arg_present(out_cfg) then begin
   res     = byte(outcfg)
   out_cfg = string(res)
endif

END
