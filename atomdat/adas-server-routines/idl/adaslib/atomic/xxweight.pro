;----------------------------------------------------------------------
;
; PROJECT    : ADAS
;
; NAME       : xxweight
;
; PURPOSE    : to calculate the weigths (scale factors for the
;              metastable fractionation) for the terms belonging
;              to the same configuration
;
; INPUT      : terms    strarr   terms
;
; OUTPUT     : l        intarr   L in number (e.g. L=S-> L=0)
;              s        fltarr   2.*S+1.
;              wt       fltarr   statistical weight
;              scfac    fltarr   fraction of total weigth (or scale factors)
;
; EXAMPLE    : Examples of how to run:
;              IDL> xxweight, ['4P','2D','2S'], l, s, w, frac
;
; AUTHOR     : Alessandra Giunta
;
; DATE       : 14-05-2012
;
;
; MODIFIED   : Alessandra Giunta
;       1.1
;              - First version.
;
; VERSION:
;       1.1    14-05-2012
;
;----------------------------------------------------------------------

pro xxweight, terms, l, s, wt, frac

terms=strlowcase(terms)
nterms=n_elements(terms)
scfac=fltarr(nterms)
lpart=fltarr(nterms)
lpartlet=strarr(nterms)
wpart=fltarr(nterms)
spart=fltarr(nterms)
totpart=fltarr(nterms)

;list of standard orbitals
lorb_std=['s', 'p', 'd', 'f', 'g', 'h', 'i', 'k', 'l', 'm', 'n', 'o', 'q', 'r', 't', 'u']
nlorb_std=n_elements(lorb_std)
orb_std=fix(findgen(nlorb_std))

for i=0,nterms-1 do begin
  lpartlet[i]=strmid(strtrim(terms[i],2),1,1)
  for j=0,nlorb_std-1 do begin
    if lpartlet[i] eq lorb_std[j] then lpart[i]=float(orb_std[j])
  endfor
  spart[i]=float(strmid(strtrim(terms[i],2),0,1))
  wpart[i]=(2.*lpart[i]+1.)*spart[i]
endfor

totpart=total(wpart)
lpart=fix(lpart)
scfac=wpart/totpart

; return variables

l    = lpart
s    = spart
wt   = wpart
frac = scfac

end
