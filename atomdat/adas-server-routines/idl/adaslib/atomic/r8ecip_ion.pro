;----------------------------------------------------------------------
;+
; PROJECT    :  ADAS
;
; NAME       :  r8ecip_ion
;
; PURPOSE    :  Calculates ECIP approximation for ionisation cross sections.
;               See H P Summers, Appleton Laboratory Report 367, 1974 and
;               A Burgess and H P Summers, Mon. Not. R. Astron. Soc., p345 (1976)
;
; ARGUMENTS  :  All output arguments will be defined appropriately.
;               Inputs will be converted to correct type, if possible,
;               internally without changing calling type.
;
;               NAME      I/O    TYPE    DETAILS
; REQUIRED   :  iz         I     integer iz-1 is charge on ionising ion
;               xi         I     double  effective ionisation potential (eV)
;               energy     I     double  electron energy (eV)
;
;
; KEYWORDS      None
;
;
; NOTES      :  Returns cross section in units of result are cm^2
;
;
; AUTHOR     :  Martin O'Mullane
;
; DATE       :  11-05-2011
;
;
; MODIFIED:
;         1.1       Martin O'Mullane
;              	        - First version.
;
; VERSION:
;         1.1      11-05-2011
;-
;----------------------------------------------------------------------

FUNCTION r8ecip_ion, iz     = iz,    $
                     xi     = xi,    $
                     energy = energy


; Convert to Rydbergs for internal use

xi_in     = xi / 13.6D0
energy_in = energy / 13.6D0


num_eng = n_elements(energy_in)
ai      = dblarr(num_eng)
z       = double(iz + 1)

for j = 0, num_eng-1 do begin

   if energy_in[j] LT xi_in then begin

      ai[j] = 0.0

   endif else begin

      v     = z / sqrt(xi_in)
      b     = (energy_in[J] - xi_in) / xi_in
      b1    = b + 1.0D0
      b2    = b + 2.0D0

      r     =  (1.25 * v^2 + 0.25) / z
      delta =  (z / v) * (r + 2.0*v^2 * sqrt(b1) / (z^2 * b2)) / (sqrt(b1) + sqrt(b))

      p1    = 4.0 / (xi_in^2 * b1)
      p2    = 1.0 / b2
      p3    = b - ((b1 / b2) * alog(b1))
      p4    = 0.65343D0 * (1.0D0 - 1.0D0 / b1^3)

      p5    = r8yip(0.0d0, delta) / v

      ai[j] = 8.7972d-17 * p1 * (p2 * p3 + p4 *p5)

   endelse

endfor

return, ai

END
