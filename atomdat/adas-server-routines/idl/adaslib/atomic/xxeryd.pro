; Copyright (c) 2005, Strathclyde University.
;----------------------------------------------------------------------
;+
; PROJECT    :  ADAS
;
; NAME       :  xxeryd
;
; PURPOSE    :  To calculate the energy levels in rydbergs ( from
;               wave numbers) relative to level 1, and the energies
;               (also in ryd.) relative to the ionisation potential.
;
; USE:
;       An example;
;               read_adf04,file=datafile,fulldata=adf04
;		xxeryd,bwno=adf04.bwno, il=(size(adf04.ia))[1], $
;		wa=adf04.wa, er=er, xia=xia
;
; INPUTS (as keywords):
;       bwno:  	Ionisation potential (cm-1)
;       il:    	Number of energy levels (<= size of wa)
;       wa:    	Energy relative to level 1 (cm-1)
;
; OPTIONAL INPUTS (as keywords):
;       help:   Display documentation and exit.
;
; OUTPUTS (as keywords):
;        er:	Energy relative to level 1 (Rydbergs)
;        xia:	Energy relative to ion. pot. (Rydbergs)
;
; OPTIONAL OUTPUTS:
;       None.
;
; CALLS:
;       Fortran/C code via a shared object.
;
; SIDE EFFECTS:
;
; AUTHOR     :  Allan Whiteford
; 
; DATE       :  17-06-05
; 
;
; MODIFIED:
;       1.1     Allan Whiteford
;              	- First version.
;
; VERSION:
;       1.1    17-06-05
;-
;----------------------------------------------------------------------

pro xxeryd,bwno=bwno,il=il,wa=wa,er=er,xia=xia,help=help

; If asked for help

if keyword_set(help) then begin 
   doc_library, 'xxeryd' 
   return
endif

if not (keyword_set(bwno) and keyword_set(il) and keyword_set(wa)) then begin
   doc_library, 'xxeryd' 
   return
endif

fortdir = getenv('ADASFORT')
fortdir = fortdir

; Cast variables appropriately

bwno_in=double(bwno)
il_in=long(il)
wa_in=double(wa)

; Generate suitable arrays for er/xia

er=dblarr(il)
xia=dblarr(il)

dummy = CALL_EXTERNAL(fortdir+'/xxeryd_if.so','xxeryd_if',        $
                      bwno_in, il_in, wa_in, er, xia, /unload)


end
