;----------------------------------------------------------------------
;+
; PROJECT:
;       ADAS
;
; NAME: R8WAVEH()
;
; PURPOSE:
;       Calculates n-n' wavelength for hydrogen
;
; EXPLANATION:
;       n, n' and mass return air corrected wavelength for
;       the transition. Returned wavelength is in Angstroms.
;       Divide by Z^2 for hydrogenic species.
;
; USE:
;       General
;
; INPUTS:
;       NU      -       Integer -  upper value of N quantum number.
;       NL      -       Integer -  lower value of N quantum number.
;
;
; OPTIONAL INPUTS:
;       None.
;
; OUTPUTS:
;       R8WAVEH   -       Float - Hydrogenic air corrected wavelength
;
; OPTIONAL OUTPUTS:
;       None
;
; KEYWORD PARAMETERS:
;       None
;
; CALLS:
;       None
;
; SIDE EFFECTS:
;       None
;
; CATEGORY:
;       System routine.
;
; WRITTEN:
;       Martin O'Mullane
;
; MODIFIED:
;       1.1  Martin O'Mullane
;              - First Release

; VERSION:
;       1.1     22-06-2009
;
;-----------------------------------------------------------------------------
FUNCTION r8waveh, NU, NL

; Constants

rz = 109737.31534D0
c1 = 6432.8D0
c2 = 2949810.0D0
c3 = 25540.0D0
c4 = 146.0D0
c5 = 41.0D0

; Principal quantum numbers of transition

nlow = min(long([nu, nl]), max=nhigh)

d     = (1.0D0 / (nlow^2) - 1.0D0 / (nhigh^2))
delta = rz * D
sig2  = delta^2 * 1.0D-8
rf    = 1.0D0 + 1.0D-8 * ( c1 + ( c2 / ( c4 - sig2 ) ) + $
        ( c3 / ( c5 - sig2 ) ) )
wave  = 1.0D8 / ( delta * rf )

return, wave

END
