;----------------------------------------------------------------------
;+
; PROJECT    :  ADAS
;
; NAME       :  continuo - calculated continuum emission at a
;                          requested wavelength and temperature
;                          for an ionisation stage.
;
; PURPOSE    :
;
; ARGUMENTS  :  All output arguments will be defined appropriately.
;               Inputs will be converted to correct type, if possible,
;               internally without changing calling type.
;
;                continuo
;
;               NAME      I/O    TYPE    DETAILS
; REQUIRED   :  wave()     I     real    wavelength required (A)
;               tev()      I     real    electron temperature (eV)
;               iz0        I     long    atomic number
;               iz1        I     long    ion stage + 1
;               contff(,)  O     real    free-free emissivity (ph cm3 s-1 A-1)
;               contin(,)  O     real    total continuum emissivity 
;                                        (free-free + free-bound) (ph cm3 s-1 A-1)
;                                        dimensions: wave, te (dropped if just 1).
;
; KEYWORDS      help       I      -      prints help to screen
;
;
; NOTES      :  Calls fortran code. Based on an original program
;               from Lorne Horton.
;
;
; AUTHOR     :  Martin O'Mullane
;
; DATE       :  02-03-2005
;
;
; MODIFIED:
;       1.1     Martin O'Mullane
;               - First version.
;       1.2     Martin O'Mullane
;               - Comments now give correct definition of contin.
;       1.3     Martin O'Mullane
;               - Vectorize in wavelength and temperature.
;
;
; VERSION:
;       1.1    02-03-2005
;       1.2    04-05-2005
;       1.3    19-04-2013
;
;-
;----------------------------------------------------------------------

PRO continuo, wave, tev, iz0, iz1, contff, contin,  help = help


; If asked for help

if keyword_set(help) then begin
   doc_library, 'continuo'
   return
endif


fortdir = getenv('ADASFORT')

iz0_in  = long(iz0)
iz1_in  = long(iz1)

n_te = n_elements(tev)
n_wv = n_elements(wave)

contff = fltarr(n_wv, n_te)
contin = fltarr(n_wv, n_te)

for it = 0L, n_te-1 do begin
   for iw = 0L, n_wv-1 do begin

      val_ff  = 0.0D0
      val_in  = 0.0D0
      wave_in = double(wave[iw])
      tev_in  = double(tev[it])

      dummy   = CALL_EXTERNAL(fortdir+'/continuo_if.so','continuo_if',  $
                              wave_in, tev_in, iz0_in, iz1_in,          $
                              val_ff, val_in)

      contff[iw, it] = val_ff
      contin[iw, it] = val_in

   endfor
endfor

contff = reform(contff)
contin = reform(contin)

if n_elements(contff) EQ 1 then begin
   contff = contff[0]
   contin = contin[0]
endif

END
