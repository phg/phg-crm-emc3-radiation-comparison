;----------------------------------------------------------------------
;+
; PROJECT    :  ADAS
;
; NAME       :  r8giii
;
; PURPOSE    :  Calculates GIII given in eqn (11) and (15) of
;               A. Burgess, J. Phys. B7, L364, 1974.
;
;
; INPUTS:
;       jz    :  Set l=1. Set jz zero for the zero charge (neutral atom) case. 
;       l     :  For partial sums set l to lower limit of summation which
;                must be greater than zero.
;       e1    : (kappa1)**2 for non zero charge, (k1)**2 for zero charge.
;       e1    : (kappa2)**2 for non zero charge, (k2)**2 for zero charge.
;
; OPTIONAL INPUTS:
;       None.
;
; OUTPUTS:
;       This function returns free-free gaunt factor.
;
; OPTIONAL OUTPUTS:
;       None.
;
; KEYWORD PARAMETERS:
;       None.
;
; CALLS:
;       Fortran/C code via a shared object.
;
; SIDE EFFECTS:
;

; MODIFIED:
;       1.1     Martin O'Mullane
;               - First version.
;
; VERSION:
;       1.1    09-04-2015
;-
;----------------------------------------------------------------------

function r8giii, jz, l, e1, e2

jz_in  = long(jz)
l_in   = long(l)
e1_in  = double(e1)
e2_in  = double(e2)
result = 0.0D0

fortdir = getenv('ADASFORT')

dummy = CALL_EXTERNAL(fortdir+'/r8giii_if.so','r8giii_if',    $
                      jz_in, l_in, e1_in, e2_in, result)

return, result

END
