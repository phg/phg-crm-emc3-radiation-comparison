; Copyright (c) 2004, Strathclyde University.
;----------------------------------------------------------------------
;+
; PROJECT    :  ADAS
;
; NAME       :  wig3j
;
; PURPOSE    :  Evaluates a wigner 3j symbol 
;
; EXPLANATION:
;       This function evaluates with wigner 3j symbol:
;
;                             ( a1 a2 a3 )
;                             ( b1 b2 b3 )
;
; USE:
;       An example;
;               result = wig3j(1d0,1d0,3d0,0d0,0d0,0d0)
;
; INPUTS:
;       A1:  3j symbol argument (see above)
;       A2:  3j symbol argument (see above)
;       A3:  3j symbol argument (see above)
;       B1:  3j symbol argument (see above)
;       B2:  3j symbol argument (see above)
;       B3:  3j symbol argument (see above)
;
; OPTIONAL INPUTS:
;       None.
;
; OUTPUTS:
;       This function returns the evaluated 3j symbol.
;
; OPTIONAL OUTPUTS:
;       None.
;
; KEYWORD PARAMETERS:
;       None.
;
; CALLS:
;       Fortran/C code via a shared object.
;
; SIDE EFFECTS:
;
; AUTHOR     :  Allan Whiteford
; 
; DATE       :  12-10-04
; 
;
; MODIFIED:
;       1.1     Allan Whiteford
;              	- First version.
;       1.2     Martin O'Mullane
;              	- Cast the inputs to doubles for call_external. 
;
; VERSION:
;       1.1    12-10-04
;       1.2    12-10-04
;-
;----------------------------------------------------------------------

function wig3j, a1, a2, a3, b1, b2, b3

fortdir = getenv('ADASFORT')
fortdir = fortdir

answer=0d0

a1_in = double(a1)
a2_in = double(a2)
a3_in = double(a3)
b1_in = double(b1)
b2_in = double(b2)
b3_in = double(b3)

dummy = CALL_EXTERNAL(fortdir+'/wig3j_if.so','wig3j_if',        $
                      a1_in, a2_in, a3_in, b1_in, b2_in, b3_in, $
                      answer, /unload)

return,answer

END
