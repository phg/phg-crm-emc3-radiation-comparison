;----------------------------------------------------------------------
;+
; PROJECT    :  ADAS
;
; NAME       :  xxprs3
;
; PURPOSE    :  To analyse a configuration character string in Standard
;               form into an integer array of occupation numbers in the
;               normal collating order.
;
; ARGUMENTS  :  All output arguments will be defined appropriately.
;               Inputs will be converted to correct type, if possible,
;               internally without changing calling type.
;
;                xxprs3, config = config, z_nuc=z_nuc.....
;
;               NAME      I/O    TYPE    DETAILS
; REQUIRED   :  config     I     string  input configuration
;               z_nuc      I     int     atomic number
;               z_ion      I     int     Ion stage of interest.
;               occup()    O     int     vector holding occupation numbers
;                                        of shells: 1s, 2s, 2p, 3s,3p, 3d etc.
;               valence    O     int     outer occupied shell index.
;
; KEYWORDS      /help      -             shows this header.
;
;
; NOTES      :  Calls the fortran code.
;
;
; AUTHOR     :  Martin O'Mullane
;
; DATE       :  26-08-2009
;
;
; MODIFIED:
;       1.1     Martin O'Mullane
;               - First version.
;
; VERSION:
;       1.1     26-08-2009
;-
;----------------------------------------------------------------------



PRO xxprs3, config  = config,  $
            z_nuc   = z_nuc,   $
            z_ion   = z_ion,   $
            occup   = occup,   $
            valence = valence, $
            help    = help


if keyword_set(help) then begin
   doc_library,'xxprs3'
   return
endif


; Dimensions

ia_dim = 66L

; Initialise variables

iz0 = long(z_nuc)
iz  = long(z_ion)

ia  = lonarr(ia_dim)
ia_valence = -1L

str = string(replicate(32B, 30))
strput, str, config
iconfig = long(byte(str))

; Get binary directory and call shared object

fortdir = getenv('ADASFORT')

dummy = CALL_EXTERNAL(fortdir+'/xxprs3_if.so','xxprs3_if', $
                      iconfig, ia_dim, iz0, iz, ia, ia_valence)

; Return results

if arg_present(occup) then occup = ia
if arg_present(valence) then valence = ia_valence

END
