; Copyright (c) 2003 Strathclyde University .
; SCCS info: Module @(#)$Header: /home/adascvs/idl/adaslib/atomic/xxmwel.pro,v 1.1 2004/07/06 15:37:54 whitefor Exp $ Date $Date: 2004/07/06 15:37:54 $
; PROJECT:
;       IDL-ADAS
; NAME:
;       xxmwel.pro
;
; PURPOSE:
;       Routine to return a Maxwell electron distribution function 
;       vector for specified temperature at a set of specified energies.
;
; USE:
;       General
;
; INPUTS:
;       TEV     -       Electron temperature (eV).
;       NEEV    -       Number of electron energies
;       EEVA()  -       Set of electron energies (eV)
; OPTIONAL INPUTS:
;       None
;
; OUTPUTS:
;       FMA()	-	The Maxwell distribution normalised so that 
;                       integral FMA()d(E/kT) = 1.
;
; OPTIONAL OUTPUTS:
;       None
;
; KEYWORD PARAMETERS:
;       None
;
; CALLS:
;       None
;
; SIDE EFFECTS:
;       None
;
; CATEGORY:
;       Adas system.
;
; WRITTEN:
;       Hugh Summers, University of Strathclyde,  21 June 2001
;
; MODIFIED:
;       1.1     Hugh Summers
;               First release
; VERSION:
;       1.1     21/06/01
;
;-----------------------------------------------------------------------------
;-

PRO xxmwel, neev, tev, eeva, fma

    i = 0
    
    for i = 0, neev-1 do begin
       x = eeva(i)/tev
       fma(i)  = 1.128379e0*sqrt(x)*exp(-x)/tev
    endfor
    
    return

END
