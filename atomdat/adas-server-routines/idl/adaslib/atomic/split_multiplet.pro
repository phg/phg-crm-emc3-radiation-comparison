;-----------------------------------------------------------------------------
;+
;
; NAME     : split_multiplet
;
; PURPOSE  : Returns the relative intenisty of lines in an LS multiplet.
;
; NOTES    : From Condon And Shortley, Chap 9, Sec. 2, eqn. 2a and 2b.
;
; ARGUMENTS: (s,l) quantum numbers for upper and lower terms.
;
;             NAME      I/O    TYPE    DETAILS
; REQUIRED :  s_low                    Spin of lower level (note: s not 2s+1)
;             l_low                    L of lower level
;             s_up                     upper S
;             l_up                     upper L
; OPTIONAL    j_low      O     int     j values of lower levels in multiplet
;             j_up       O     int     j values of upper levels in multiplet
;
; KEYWORDS :  norm              -      set smallest component to one
;             white             -      set largest componet to 100 following
;                                      White and Eliason, PRA, 44, p753 (1933).
;             help              -      display this comment section
;
; EXAMPLE  :  print,  split_multiplet(1, 3, 1, 2, /white, j_low=j1, j_up=j2)
;             gives: 46.6667  8.64198 0.246914 69.1358 8.64198 100.000
;             It is necessary to check the J_low and J_up vectors to
;             identify the levels.
;
; MODIFIED:
;       1.1     Martin O'Mullane
;                - First release
;       1.2     Martin O'Mullane
;                - Do not consider zeros when calculating the White or
;                  standard normalisation.
;       1.3     Martin O'Mullane
;                - Improve the rules for calculating possible j values.
;       1.4     Martin O'Mullane
;                - Incorrect sign in 3rd same-l check.
;
; VERSION:
;       1.1     16-04-2010
;       1.2     08-10-2010
;       1.3     04-02-2011
;       1.4     04-07-2012
;
;-
;-----------------------------------------------------------------------------



FUNCTION split_condon_shortley, s1, l1, j1, s2, l2, j2

; From Condon and Shortley, Chapter 9, section 2, equations 2a and 2b.

case 1 of

  (j1 EQ j2-1) and (l1 EQ l2+1) : res = (j1+s1-l1+1) * (l1+s1-j1) * (j1+s1-l1+2)  * (l1+s1-j1-1) / (4.0 * (j1+1) )
  (j1 EQ j2)   and (l1 EQ l2+1) : res = (2*j1+1) * (j1+l1-s1) * (j1+s1-l1+1) * (s1+l1+1+j1) * (s1+l1-j1) / ( 4.0*j1*(j1+1) )
  (j1 EQ j2+1) and (l1 EQ l2+1) : res = (j1+l1-s1-1) * (j1+l1-s1) * (s1+l1+j1+1) * (s1+l1+j1) / ( 4.0*j1 )

  (j1 EQ j2-1) and (l1 EQ l2)   : res = (j1-s1+l1+1) * (j1+s1-l1+1) * (s1+l1+j1+2) * (s1+l1-j1) / ( 4.0*(j1+1) )
  (j1 EQ j2)   and (l1 EQ l2)   : res = (2*j1+1) * ( j1*(j1+1) - s1*(s1+1) + l1*(l1+1) )^2.0 / ( 4.0*j1*(j1+1) )
  (j1 EQ j2+1) and (l1 EQ l2)   : res = (j1-s1+l1) * (j1+s1-l1) * (s1+l1+j1+1) * (s1+l1+1-j1) / ( 4.0*j1 )

  (j1 EQ j2-1) and (l1 EQ l2-1) : res = (j1+l1-s1-1) * (j1+1l-s1) * (s1+l1+j1+1) * (s1+l1+j1) / ( 4.0*j1 )
  (j1 EQ j2)   and (l1 EQ l2-1) : res = (2*j1+1) * (j1+l1-s1) * (j1+s1-l1+1) * (s1+l1+1+j1) * (s1+l1-j1) / ( 4.0*j1*(j1+1) )
  (j1 EQ j2+1) and (l1 EQ l2-1) : res = (j1+s1-l1+1) * (l1+s1-j1) * (j1+s1-l1+2)  * (l1+s1-j1-1) / (4.0 * (j1+1) )

  else : res = !values.f_nan

endcase

if res EQ 0.0 then res = !values.f_nan

return, res

END
;-----------------------------------------------------------------------------



FUNCTION split_multiplet, s_low, l_low, s_up, l_up, $
                          j_low = j_low,            $
                          j_up  = j_up,             $
                          norm  = norm,             $
                          white = white,            $
                          help  = help


; If asked for help

if keyword_set(help) then begin
   doc_library, 'split_multiplet'
   return, -1L
endif


; Make sure L_up is larger than L_low

if (l_low GT l_up) then begin
   message, 'SL, S''L'' swapped', /continue
   l_t   = l_low
   l_low = l_up
   l_up  = l_t
   s_t   = s_low
   s_low = s_up
   s_up  = s_t
endif

j_low = -1
j_up  = -1
res   = 0.0


; Cycle over possible j-j' combinations

for j1 = abs(l_up-s_up), abs(l_up+s_up) do begin


   for j2 = abs(l_low-s_low), abs(l_low+s_low) do begin

      if (j1 GE 0)              AND $
         (j2 GE 0)              AND $
         (abs(j2-j1) LT 2)      AND $
         (abs(l_up-l_low) LT 2) AND $
         (abs(j1+j2) GE 1)      then begin
      
         j_up  = [j_up, j1]
         j_low = [j_low, j2]

         tmp = split_condon_shortley(s_up, l_up, j1, s_low, l_low, j2)
         res = [res, tmp]
      
      endif

   endfor

endfor

j_low = j_low[1:*]
j_up  = j_up[1:*]
res   = res[1:*]

; Two optional types of normalisation

if keyword_set(norm) then res = res / min(res, /nan)
if keyword_set(white) then res = 100.0 * res / max(res, /nan)


return, res

END
