;----------------------------------------------------------------------
;+
; PROJECT:
;       ADAS
;
; NAME: R8AH()
;
; PURPOSE:
;       Calculates A-values for hydrogen
;
; EXPLANATION:
;       Based on the FORTRAN routine r8ah.for in adaslib library.
;
; USE:
;       General
;
; INPUTS:
;       NU      -       Integer -  upper value of N quantum number.
;       LU      -       Integer -  upper value of L quantum number.
;       NL      -       Integer -  lower value of N quantum number.
;       LL      -       Integer -  lower value of L quantum number.
;
; NOTE: NU, LU, NL and LL are positional parameters so values for
;       all 4 must be set if the multiplet nn is selected - in this
;       mode values of LL and LU are ignored.
;
;
; OPTIONAL INPUTS:
;       None.
;
; OUTPUTS:
;       R8AH   -       Float - Hydrogenic A-value
;
; OPTIONAL OUTPUTS:
;       nn     -       Float n-n' A-value
;
; KEYWORD PARAMETERS:
;       None
;
; CALLS:
;       None
;
; SIDE EFFECTS:
;       None
;
; CATEGORY:
;       System routine.
;
; WRITTEN:
;       Martin O'Mullane
;
; MODIFIED:
;       1.1  Martin O'Mullane
;              - First Release
;       1.2  Martin O'Mullane
;              - Remove IZ0 from input list in documentation as it
;                was never an argument.
;       1.3  Martin O'Mullane
;              - Add n-n' A-value (nn) as an optional output.
;       1.4  Martin O'Mullane
;              - Add n-n' oscillator strength (fn) as an optional output.
;
; VERSION:
;       1.1     10-05-2001
;       1.2     14-08-2008
;       1.3     22-07-2009
;       1.3     28-06-2015
;
;-----------------------------------------------------------------------------
FUNCTION r8ah, NU , LU , NL , LL, nn=nn, fn=fn

P1 = 2.67744D+09

; Check for valid nl - n'l' transition

ltest = (nu LT 2) or (lu LT 0) or (lu GE nu) or $
        (nl LT 1) or (ll LT 0) or (ll GE nl) or (nl GE nu)

if (ltest) then begin

   r8ah = 0.0

endif else begin

   xnu = double(nu)
   xlu = double(lu)
   xnl = double(nl)
   xll = double(ll)

   T1   = P1 / ( 2.0D+00 * XLU + 1.0D+00 )
   DE   = 1.0D+00 / XNL^2 - 1.0D+00 / XNU^2

   R8AH = T1 * MAX([XLU , XLL]) * DE^3 * R8RD2B(NU , LU , NL ,LL)

endelse

; Calculate multiplet, n-n', A-value if requested

if arg_present(nn) then begin

   a_nn = 0.0
   wt   = 0.0

   for lu = 0, nu-1 do wt = wt + 2.0 * (2.0*lu + 1)
   for LU = 0, nl do begin
      for ll = LU+1, LU-1, -2 do begin
         if ll GE 0 AND ll LT nl then a_nn = a_nn + 2.0 * (2.0*lu + 1)  * r8ah(nu,lu,nl,ll)
      endfor
   endfor

   nn = a_nn / wt
   
   e   = 1.0 / nl^2.0
   wi  = 2.0*nl^2
   e11 = 1.0 / nu^2.0     
   wj  = 2.0*nu^2
   lam = 1.0e8 / ((e - e11)*109737.0)
   fn  = nn * lam^2 * wj / (wi * 6.6702e15)
   
endif

return, r8ah

END
