;----------------------------------------------------------------------
;+
; PROJECT    :  ADAS
;
; NAME       :  r8fbch
;
; PURPOSE    :  Calculates a shell contribution to the ionisation rate
;               coefficient in the Burgess-Chidichimo approximation.
;
; ARGUMENTS  :  All output arguments will be defined appropriately.
;               Inputs will be converted to correct type, if possible,
;               internally without changing calling type.
;                   result = r8fbch(iz=iz, xi=xi, zeta=zeta, te=te)
;
;               r8fbch, iz     = iz,    $
;                       xi     = xi,    $
;                       zeta   = zeta,  $
;                       te     = te
;
;
;               NAME      I/O    TYPE    DETAILS
; REQUIRED   :  iz         I     integer recombined ion charge
;               xi         I     double  effective ionisation potential (Ryd)
;               zeta       I     double  effective number of equivalent electrons
;               te         I     double  array of electron temperature (K)
;
;
; KEYWORDS      help       I      -      prints help to screen
;
;
; NOTES      :  Calls the fortran code.
;               Units of result are cm^3/s
;
;
; AUTHOR     :  Martin O'Mullane
;
; DATE       :  18-09-2008
;
;
; MODIFIED:
;         1.1       Martin O'Mullane
;                       - First version.
;         1.2       Martin O'Mullane
;                       - Return 0 if ionisation potential is zero.
;                       - Pass iz as a long to IDL - do not use fix.
;
; VERSION:
;         1.1      18-09-2008
;         1.2      18-08-2010
;-
;----------------------------------------------------------------------

FUNCTION r8fbch, iz     = iz,    $
		 xi     = xi,    $
		 zeta   = zeta,  $
		 te     = te,    $
		 help   = help


; If asked for help

if keyword_set(help) then begin
   doc_library, 'r8fbch'
   return, -1L
endif

fortdir = getenv('ADASFORT')
fortdir = fortdir

if n_elements(iz) EQ 0 OR   $
   n_elements(xi) EQ 0 OR   $
   n_elements(zeta) EQ 0 OR $
   n_elements(te) EQ 0      $
   then message, 'essential parameters missing'

; inputs

n_tek = n_elements(te)

iz_in   = long(iz)
xi_in   = double(xi)
zeta_in = double(zeta)
te_in   = double(te)

; results

rate = dblarr(n_tek)

; If xi is 0.0 then do not call fortran code

if xi EQ 0.0 then return, rate

dummy = CALL_EXTERNAL(fortdir+'/r8fbch_if.so','r8fbch_if', $
		      n_tek, iz_in, xi_in, zeta_in, te_in, rate)


return, rate

END
