;-----------------------------------------------------------------------------
;+
; PROJECT:
;       ADAS
;
; NAME:
;       metastable
;
; PURPOSE:
;       Returns as a string array the configurations and/or LS terms of
;       a selected ion from the first 26 isoelectronic sequences. A structure
;       containing the J-resolved level information can now also be returned.
;
; EXPLANATION:
;
; INPUTS:
;        seq - Isoelectronic sequence ('H' to 'Fe') required and is
;              not case sensitive. A special case is 'FS' for fully
;              stripped.
;
; OPTIONAL INPUTS:
;       None.
;
; OUTPUTS:
;       config - String array of metastable configurations for
;                isoelectronic sequence seq.
;       term   - String array of metastables terms for isoelectronic
;                sequence seq.
;       levels - A structure containing the J values of the metastables levels
;		 in one array and the corresponding term for each level in another
;		 such that the heritage can be tracked. Ordering of the levels
;		 is according to the terms from which they are derived, and
; 		 therefore level crossings have not been considered. Moreover,
;	 	 the simple assumption has been made that all levels from a
;		 term will also be metastable: this is not necessarily the case.
;	           = { term : array of LS term designations for each level,
;		       fj   : array of J values for each level }
;       spin   - Integer array of possible spin systems for isoelectronic
;                sequence seq.
;
;
; EXAMPLE:
;       metastable, 'li', term=term, config=config
;
; OPTIONAL OUTPUTS:
;       None
;
; KEYWORD PARAMETERS:
;       help    : Displays documentation.
;       lower   : Return configuration (not term) in lower case (default).
;       upper   : Return configuration in upper case.
;
;
; CALLS:
;       XXLVALS
;
; SIDE EFFECTS:
;       None
;
; CATEGORY:
;       Utility
;
; WRITTEN:
;       Martin O'Mullane
;
; MODIFIED:
;       1.1     Martin O'Mullane
;                - First release
;       1.2     Martin O'Mullane
;                - Extend to Ar-like.
;                - Add /help, /upper and /lower keywords.
;       1.3     Martin O'Mullane
;                - Add spin as output.
;       1.4     Martin O'Mullane
;                - Incorrect configuration for third Si-like metastable.
;       1.5     Alessandra Giunta
;                - Extend to Fe-like.
;       1.6     Matthew Bltueau
;                - Dynamic J-resolved level output added.
;
; VERSION:
;       1.1     11-08-2000
;       1.2     24-08-2009
;       1.3     29-09-2009
;       1.4     19-01-2010
;       1.5     19-07-2012
;       1.6     24-05-2017
;
;-
;-----------------------------------------------------------------------------

PRO metastable, seq    = seq,    $
                config = config, $
                term   = term,   $
                levels = levels, $
                spin   = spin,   $
                lower  = lower,  $
                upper  = upper,  $
                help   = help


if keyword_set(help) then begin
    doc_library, 'metastable'
    return
endif

tst = strtrim(strupcase(seq),2)

if arg_present(term) GT 0 then begin

   CASE tst OF

    'FS' : term = ['1S']
    'H'  : term = ['2S']
    'HE' : term = ['1S', '3S']
    'LI' : term = ['2S']
    'BE' : term = ['1S', '3P']
    'B'  : term = ['2P', '4P']
    'C'  : term = ['3P', '1D','1S', '5S']
    'N'  : term = ['4S', '2D','2P']
    'O'  : term = ['3P', '1D','1S', '5S']
    'F'  : term = ['2P', '4P']
    'NE' : term = ['1S', '3P']
    'NA' : term = ['2S']
    'MG' : term = ['1S', '3P']
    'AL' : term = ['2P', '4P']
    'SI' : term = ['3P', '1D','1S', '5S']
    'P'  : term = ['4S', '2D','2P']
    'S'  : term = ['3P', '1D','1S', '5D']
    'CL' : term = ['2P', '4D']
    'AR' : term = ['1S', '3P']
    'K'  : term = ['2D', '2S']
    'CA' : term = ['3F', '1D','3D']
    'SC' : term = ['4F', '2G','4F'] ;?
    'TI' : term = ['5D', '3P','5F']
    'V'  : term = ['6S', '4G','6D']
    'CR' : term = ['5D', '3P','7S']
    'MN' : term = ['6D', '4F']
    'FE' : term = ['5D', '5F','3P']

   ELSE : message,'Sequence not available'

   ENDCASE


   ; Generate LSJ resolved metastable levels from terms

   if arg_present(levels) GT 0 then begin

       lvals = xxlvals(/upper)
       
       jvals  = -1
       lsvals = 'NULL'
       
       FOR k = 0, n_elements(term)-1 DO BEGIN

           trm = term[k]
           
           ; split the term designation into S and L components
           qnums = STREGEX(trm, '([0-9]+)([A-Z]+)', /EXTRACT, /SUBEXPR)
	   IF N_ELEMENTS(qnums) NE 3 THEN MESSAGE, 'Incorrect term spec: ' + trm
	   s = DOUBLE(qnums[1])
	   s = (s - 1.0) / 2.0
	   l = DOUBLE(WHERE(lvals EQ qnums[2]))
	   l = l[0]
	   ; vector sum rule: J = |L-S|, |L-S|+1, ... , (L+S)
	   high = s + l
	   low = ABS(l - s)

	   ; add J values to storage array for indexing along with term
	   FOR j = low, high DO BEGIN
               jvals = [jvals, [j]]
	       lsvals = [lsvals, [trm]]
           ENDFOR

       ENDFOR
       if n_elements(jvals) GT 1 then begin
          lsvals = lsvals[1:*]
          jvals  = jvals[1:*]
       endif

       levels = { term : lsvals, fj : jvals }

   endif

endif



if arg_present(spin) GT 0 then begin

  CASE tst OF

   'FS' : spin = [1]
   'H'  : spin = [2]
   'HE' : spin = [1, 3]
   'LI' : spin = [2]
   'BE' : spin = [1, 3]
   'B'  : spin = [2, 4]
   'C'  : spin = [3, 1, 5]
   'N'  : spin = [4, 2, 2]
   'O'  : spin = [3, 1, 5]
   'F'  : spin = [2, 4]
   'NE' : spin = [1, 3]
   'NA' : spin = [2]
   'MG' : spin = [1, 3]
   'AL' : spin = [2, 4]
   'SI' : spin = [3, 1, 5]
   'P'  : spin = [4, 2]
   'S'  : spin = [3, 1, 5]
   'CL' : spin = [2, 4]
   'AR' : spin = [1, 3]
   'K'  : spin = [2, 2]
   'CA' : spin = [3, 1, 3]
   'SC' : spin = [4, 2, 4] ;?
   'TI' : spin = [5, 3, 5]
   'V'  : spin = [6, 4, 6]
   'CR' : spin = [5, 3, 7]
   'MN' : spin = [6, 4]
   'FE' : spin = [5, 5, 3]

  ELSE : message,'Sequence not available'

  ENDCASE

endif

if arg_present(config) GT 0 then begin

  CASE tst OF

   'FS' : config = ['(*)']
   'H'  : config = ['1s1']
   'HE' : config = ['1s2', '1s1 2s1']
   'LI' : config = ['1s2 2s1']
   'BE' : config = ['1s2 2s2', '1s2 2s1 2p1']
   'B'  : config = ['1s2 2s2 2p1', '1s2 2s1 2p2']
   'C'  : config = ['1s2 2s2 2p2', '1s2 2s2 2p2','1s2 2s2 2p2', '1s2 2s1 2p3']
   'N'  : config = ['1s2 2s2 2p3', '1s2 2s2 2p3','1s2 2s2 2p3']
   'O'  : config = ['1s2 2s2 2p4', '1s2 2s2 2p4','1s2 2s2 2p4', '1s2 2s2 2p3 3s1']
   'F'  : config = ['1s2 2s2 2p5', '1s2 2s2 2p4 3s1']
   'NE' : config = ['1s2 2s2 2p6', '1s2 2s2 2p5 3s1']
   'NA' : config = ['1s2 2s2 2p6 3s1']
   'MG' : config = ['1s2 2s2 2p6 3s2', '1s2 2s2 2p6 3s1 3p1']
   'AL' : config = ['1s2 2s2 2p6 3s2 3p1', '1s2 2s2 2p6 3s1 3p2 ']
   'SI' : config = ['1s2 2s2 2p6 3s2 3p2', '1s2 2s2 2p6 3s2 3p2', $
                    '1s2 2s2 2p6 3s2 3p2', '1s2 2s2 2p6 3s1 3p3']
   'P'  : config = ['1s2 2s2 2p6 3s2 3p3', '1s2 2s2 2p6 3s2 3p3', $
                    '1s2 2s2 2p6 3s2 3p3']
   'S'  : config = ['1s2 2s2 2p6 3s2 3p4', '1s2 2s2 2p6 3s2 3p4', $
                   '1s2 2s2 2p6 3s2 3p4', '1s2 2s2 2p6 3s2 3p3 3d1']
   'CL' : config = ['1s2 2s2 2p6 3s2 3p5', '1s2 2s2 2p6 3s2 3p4 3d1']
   'AR' : config = ['1s2 2s2 2p6 3s2 3p6', '1s2 2s2 2p6 3s2 3p5 4s1']
   'K'  : config = ['1s2 2s2 2p6 3s2 3p6 3d1', '1s2 2s2 2p6 3s2 3p6 4s1']
   'CA' : config = ['1s2 2s2 2p6 3s2 3p6 3d2', '1s2 2s2 2p6 3s2 3p6 3d2', $
                    '1s2 2s2 2p6 3s2 3p6 3d1 4s1']
   'SC' : config = ['1s2 2s2 2p6 3s2 3p6 3d3', '1s2 2s2 2p6 3s2 3p6 3d3', $
                    '1s2 2s2 2p6 3s2 3p6 3d2 4s1'] ;?
   'TI' : config = ['1s2 2s2 2p6 3s2 3p6 3d4', '1s2 2s2 2p6 3s2 3p6 3d4', $
                    '1s2 2s2 2p6 3s2 3p6 3d3 4s1']
   'V'  : config = ['1s2 2s2 2p6 3s2 3p6 3d5', '1s2 2s2 2p6 3s2 3p6 3d5', $
                    '1s2 2s2 2p6 3s2 3p6 3d4 4s1']
   'CR' : config = ['1s2 2s2 2p6 3s2 3p6 3d6', '1s2 2s2 2p6 3s2 3p6 3d6', $
                    '1s2 2s2 2p6 3s2 3p6 3d5 4s1']
   'MN' : config = ['1s2 2s2 2p6 3s2 3p6 3d6 4s1', '1s2 2s2 2p6 3s2 3p6 3d7'] ;?
   'FE' : config = ['1s2 2s2 2p6 3s2 3p6 3d6 4s2', '1s2 2s2 2p6 3s2 3p6 3d7 4s1', $
                    '1s2 2s2 2p6 3s2 3p6 3d6 4s2']

  ELSE : message,'Sequence not available'

  ENDCASE

endif

if keyword_set(lower) then config = strlowcase(config)
if keyword_set(upper) then config = strupcase(config)

END
