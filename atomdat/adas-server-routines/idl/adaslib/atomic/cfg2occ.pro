;----------------------------------------------------------------------
;+
; PROJECT    :  ADAS
;
; NAME       :  cfg2occ
;
; PURPOSE    :  Converts a textual configuration to an occupation
;               number array
;
; EXPLANATION:
;       This function takes a textual configuration, nuclear charge
;	and ion charge to return occupation numbers of electrons
;
; USE:
;       An example;
;               result = cfg2occ('2s 2p',6,2)
;		print,result
;		2 1 1 0 0 0 0 0 0 0 0 0 0 ...
;
; INPUTS:
;       CFG: The configuration in either standard or Eissner
;            notation.
;       IZ0: The nuclear charge of the ion.
;       ZZ : The charge of the ion.
;
; OPTIONAL INPUTS:
;       None.
;
; OUTPUTS:
;       An occupation number array with all electrons specified
;
; OPTIONAL OUTPUTS:
;       None.
;
; KEYWORD PARAMETERS:
;       help : Displays documentation
;
; CALLS:
;       read_adf00 for ground configurations.
;       xxlvals for mapping 's','p',... to l values
;       xxorbs for sequence of '1s','2s','2p',...
;
; SIDE EFFECTS:
;       None
;
; NOTES:
;       Determines unspecified electrons by filling in occupation
;       numbers based on the ground configuration of the ion in
;       question in order of spectroscopic notation, this means for
;       the W+14 with a configuration of simply '5p' it will assume
;       that a 5s electron has been promoted rather than a 4f.
;
;       Note, however, that the routine does not assume lower
;       occupation numbers are filled if the ground configuration
;       has them partially empty so for neutral tungsten with
;       a configuration of '6p' it will leave the 5f and 5g orbitals
;       empty and the 5d partially filled as one would expect.
;
; AUTHOR     :  Allan Whiteford
; 
; DATE       :  12-10-04
; 
;
; MODIFIED:
;       1.1     Allan Whiteford
;              	- First version.
;       1.2     Allan Whiteford
;              	- Updated to use xxlvals and xxorbs
;       1.3     Martin O'Mullane
;              	- Extend check of tcfg to 8k orbital (from 5g)
;                 Check up to Eissner 'a', up from 'F'.
;
; VERSION:
;       1.1    31-07-08
;       1.2    22-06-09
;       1.3    28-11-2011
;-
;----------------------------------------------------------------------

function cfg2occ,cfg,iz0,iz,help=help

	if keyword_set(help) then begin
        	doc_library,'cfg2occ'
        	return,0
        endif

 	lvals=xxlvals(/upper)
	orbs=xxorbs(maxo=maxo,/upper)

        occ=intarr(n_elements(orbs))
        
        if (strlen((res=stregex(cfg,'^ *n=([0-9])+ *\*? *$',/fold_case,/sub,/extr))))[0] ne 0 then occ[total(indgen(fix(res[1])))]=1

        tcfg=strtrim(cfg,2)
        if strpos(tcfg,'*') eq strlen(tcfg)-1 then tcfg=strtrim(strmid(tcfg,0,strlen(tcfg)-1),2)
        if total(occ) eq 0 and stregex(tcfg,'^[0-9A-Za]+$') ne -1 and (strlen(tcfg) mod 3 eq 2 or strlen(tcfg) mod 3 eq 0) then begin
                badcnt=0
                for i=0,(strlen(strtrim(tcfg,2))-2)/3 do begin
                        cnt = fix(strmid(tcfg,3*i-(i ne 0)+(strlen(tcfg) mod 3 ne 2)-(i eq 0 and strlen(tcfg) mod 3 ne 2) ,1+(i ne 0 or strlen(tcfg) mod 3 ne 2))) mod 50
                	if cnt eq 0 then badcnt=1
                        orb = strmid(tcfg,3*i+1+(strlen(tcfg) mod 3 ne 2),1)
			if byte(orb) ge byte('1') and byte(orb) le byte('9') then pos=fix(orb)-1
			if byte(orb) ge byte('A') and byte(orb) le byte('Z') then pos=byte(orb)-byte('A')+9
			if byte(orb) ge byte('a') and byte(orb) le byte('z') then pos=byte(orb)-byte('a')+9
                        if cnt gt maxo[pos] then badcnt=1
			occ[pos]=cnt
                end
                if badcnt eq 1 or total(occ) gt iz0-iz then begin
                	occ[*]=0
		end
        endif
        
        if stregex(cfg,'^\(.*',/fold_case) ne -1 then begin
                return,-1
	end
        
        if total(occ) eq 0 then begin
        	i=(strmid(cfg,0,1) eq '-')-1
	        lasti=i
                
	        while (i=i+strlen(((res=stregex(strmid(cfg,(lasti=i)+1),'^\.?([0-9]+[SPDFGHIK])([0-9ABC]*|\([0-9]*\))\.?\-? ? ?',/extract,/subexpr,/fold_case)))[0])) ne lasti do begin
        		if stregex(strmid(cfg,i+1,1),'[SPDFGHIK]',/fold_case) ne -1 then res[2]=strmid(res[2],0,strlen(res[2])-(i-1 eq (i=i-1)))
	        	i=i+strlen(stregex(strmid(cfg,i+1),'^\([0-9][SPDFGHIK]\*?\)',/extr))
        		if strmid(res[2],0,1) eq '(' then res[2]=strmid(res[2],1,strlen(res[2])-2)
        	        if res[2] eq '' then res[2]='1'
	        	idx=where(strupcase(res[1]) eq orbs)
        		if idx eq -1 then return,-1
                        if fix(res[2]) gt maxo[idx] then return,-1
                        occ[idx]=occ[idx]+fix(res[2])
	        end
        
		if 	strlen(cfg)-1 ne i $
                and 	stregex(strmid(cfg,i+1),'^ *\([0-9][SPDFGHIK][0-9\*]?\) *\*?$') eq -1 $
		and	stregex(strmid(cfg,i+1),'^\*? *[0-9][SPDFGHIK]\(J=[0-9]\.[0-9]\) *$') eq -1 $
		and	stregex(strmid(cfg,i+1),'^\([0-9]/[0-9]\) *$') eq -1 $
		and	stregex(strmid(cfg,i+1),'^ *\* *$') eq -1 $
		and	stregex(strmid(cfg,i+1),'^ *$') eq -1 $
		and	stregex(strmid(cfg,i+1),'^'' *$') eq -1 $
        	        then return,-1

	endif

	elec=iz0-iz-total(occ)
	if elec lt 0 then return,-1
	if elec gt 0 then begin
		i=-1
		read_adf00,z0=iz0,z1=iz,config=config,/cache
		ground=cfg2occ(strtrim(config,2),iz0,iz)
                while ++i lt n_elements(occ) and elec gt 0 do $
                	if occ[i] eq 0 and ground[i] ne 0 then $
                                elec=elec-(occ[i]=min([ground[i],elec]))
        endif

	if total(occ) ne iz0-iz then return,-1
	return,occ
end
