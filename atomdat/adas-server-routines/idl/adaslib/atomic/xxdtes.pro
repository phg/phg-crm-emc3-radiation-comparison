; Copyright (c) 2002 Strathclyde University .
;----------------------------------------------------------------------
;+
; PROJECT    :  ADAS
;
; NAME       :  xxdtes
;
; PURPOSE    :  Detects if the configuration string from a specific ion
;               level list line is of Eissner or standard form.
;
; ARGUMENTS  :  All output arguments will be defined appropriately.
;               Inputs will be converted to correct type, if possible,
;               internally without changing calling type.
;                   
;                xxdtes, in_cfg = in_cfg, is_eissner=is_eissner, ...  etc
;
;               NAME       I/O    TYPE     DETAILS
; REQUIRED   :  in_cfg      I     string   input configuration
;               is_eissner  O     integer  1 = true if in_cfg is of Eissner form
;                                          0 = false
;                                          2 = cannot be determined
;               is_standard O     integer  1 = true if in_cfg is of standard/ADAS form
;                                          0 = false
;                                          2 = cannot be determined
;               is_bundle   O     integer  1 = true if bundled form ('*' found)
;                                          0 = false
;               is_parent   O     integer  1 = true if parent form ('[...]' found)
;                                          0 = false
;               cfg_top     O     string   leading part of config. string in Eissner
;                                          format (no leading blank, trailing blanks)
;               cfg_tail    O     string   trailing part of config. string in Eissner
;                                          format (no leading blank, trailing blanks)
;               nvlce       O     integer  outer electron n-shell if recognisable
;                                          -1 otherwise
;               lvlce       O     integer  outer electron l-shell if recognisable
;                                          -1 otherwise
;
; KEYWORDS      None
; 
;
; NOTES      :  Calls the fortran code.
;
;
;
; AUTHOR     :  Martin O'Mullane
; 
; DATE       :  08-04-2002 
; 
;
; MODIFIED:
;       1.1     Martin O'Mullane
;              	- First version.
;       1.2     Martin O'Mullane
;              	- Add an interface fortran subroutine to turn logicals
;                 into integers. Should fix endian problems.
;	1.3	Allan Whiteford
;		- Changed wrapper path to be just ADASFORT.
;       1.4     Martin O'Mullane
;              	- Extra configuration analysis outputs.
;              	- Configuration input can be up to 99 characters.
;       1.5     Martin O'Mullane
;              	- Add is_standard as an output.
;
; VERSION:
;       1.1    08-04-2002 
;       1.2    05-02-2003 
;       1.3    10-08-2004 
;       1.4    20-08-2010 
;       1.5    09-05-2019 
;-
;----------------------------------------------------------------------

PRO xxdtes, in_cfg      =  in_cfg,      $
            is_eissner  =  is_eissner,  $
            is_standard =  is_standard, $
            is_bundle   =  is_bundle,   $ 
            is_parent   =  is_parent,   $ 
            cfg_top     =  cfg_top,     $  
            cfg_tail    =  cfg_tail,    $ 
            nvlce       =  nvlce,       $     
            lvlce       =  lvlce     



; strings have to be padded to 99 characters - leading ' ' for in_cfg

incfg = string(replicate(32B, 98))
len   = strlen(in_cfg)
tmp   = strmid(in_cfg, 0, min([98,len]))
strput, incfg, tmp, 0
incfg = ' ' + incfg
incfg =  long(byte(incfg))

ieiss = 2L
istan = 2L
iprnt = 2L
ibndl = 2L
nvlce = -1L
lvlce = -1L

cfgtop  = long(byte(string(replicate(32B, 19))))
cfgtail = long(byte(string(replicate(32B, 99))))

fortdir = getenv('ADASFORT')

dummy = CALL_EXTERNAL(fortdir+'/xxdtes_if.so','xxdtes_if', $
                      incfg, ieiss, istan, ibndl, iprnt,   $
                      cfgtop, cfgtail, nvlce, lvlce)

; Assemble output

is_eissner = 2

if ieiss EQ 1 and istan EQ 0 then is_eissner = 1
if ieiss EQ 0 and istan EQ 1 then is_eissner = 0


is_standard = 2

if ieiss EQ 1 and istan EQ 0 then is_standard = 0
if ieiss EQ 0 and istan EQ 1 then is_standard = 1


if arg_present(cfg_top) then begin
   res      = byte(cfgtop)
   cfg_top = string(res)
endif

if arg_present(cfg_tail) then begin
   res      = byte(cfgtail)
   cfg_tail = string(res)
endif


END 
