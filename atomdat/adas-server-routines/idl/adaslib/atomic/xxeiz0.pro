; Copyright (c) 2002, Strathclyde University.
;+
; PROJECT:
;       ADAS
;
; NAME: XXEIZ0()
;
; PURPOSE:
;       Convert element symbol into atomic number.
;
; EXPLANATION:
;
; USE:
;       General.
;
; INPUTS:
;       symb    -       String Array - the name(s) of the element.
;
;
; OPTIONAL INPUTS:
;       None.
;
; OUTPUTS:
;       iz0     -       Integer Array - the atomic number of the element
; OPTIONAL OUTPUTS:
;       None
;
; KEYWORD PARAMETERS:
;       None
;
; CALLS:
;       None
;
; SIDE EFFECTS:
;       None
;
; CATEGORY:
;       System routine.
;
; WRITTEN:
;       Susan Turnbull
;       Submitted to central ADAS by Allan Whiteford
;
; MODIFIED:
;       1.1     Susan Turnbull
;               First release
; MODIFIED:
;       1.2     Allan Whiteford
;               Allowed input symbol to be of any case
;       1.3     Martin O'Mullane
;               Vectorize input.
;
; VERSION:
;       1.1     01/07/01
;       1.2     29/05/02
;       1.3     31-05-2010
;
;-----------------------------------------------------------------------------

FUNCTION xxeiz0, symb

    ON_ERROR, 2

    symbol = ['h','he','li','be','b','c','n','o','f','ne','na','mg']
    symbol = [symbol,'al','si','p', 's', 'cl','ar','k', 'ca','sc','ti','v','cr']
    symbol = [symbol,'mn','fe','co','ni','cu','zn','ga','ge','as','se','br']
    symbol = [symbol,'kr','rb','sr','y', 'zr','nb','mo','tc','ru','rh','pd']
    symbol = [symbol,'ag','cd','in','sn','sb','te','i', 'xe','cs','ba','la']
    symbol = [symbol,'ce','pr','nd','pm','sm','eu','gd','tb','dy','ho','er']
    symbol = [symbol,'tm','yb','lu','hf','ta','w', 're','os','ir','pt','au']
    symbol = [symbol,'hg','tl','pb','bi','po','at','rn','fr','ra','ac','th','pa','u']

    symb_in = strcompress(string(symb), /remove_all)

    n_elem = n_elements(symb_in)

    if n_elem GT 1 then iz0 = intarr(n_elem) else iz0 = 0

    for j = 0, n_elem-1 do begin

       for i = 0, 91 do begin
          if (strlowcase(symb_in[j]) eq symbol[i]) then iz0[j] = i+1
       endfor

    endfor

    return, iz0

END
