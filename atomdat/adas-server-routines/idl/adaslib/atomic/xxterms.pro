;----------------------------------------------------------------------
;
; PROJECT    : ADAS
;
; NAME       : xxterms
;
; PURPOSE    : to find all possible terms for a given configuration
;              (Condon & Shortley, 1991, "The theory of atomic
;              spectra", Cambridge University Press)
;
; INPUT      : config   string        configuration
;
; OUTPUT     : terms()  strarr        terms
;                       1st dimension number of terms
;              Ltot()   intarr        angular quantum number
;                                     (e.g. terms='1S', Ltot=0)
;                       1st dimension number of terms
;              Stot()   fltarr        spin quantum number
;                                     (e.g. terms='1S', Stot=0)
;                       1st dimension number of terms
; KEYWORDS     eissner                use to tell routine the input
;                                     configuration is in Eissner form
;
; EXAMPLE    : Examples of how to run:
;              IDL> config='1S2 2S1 2P5'
;              IDL> xxterms, config, terms, Ltot, Stot
;
; NOTES      : 1. config must be in the form of standard configuration
;              2. it does work up to the orbital f full
;
; AUTHOR     : Alessandra Giunta
;
; DATE       : 14-05-2012
;
;
; MODIFIED   : Alessandra Giunta
;       1.1
;              - First version.
;
; VERSION:
;       1.1    14-05-2012
;
;----------------------------------------------------------------------
;
pro xxterms, config_in, terms, Ltot, Stot, eissner=eissner

if keyword_set(eissner) then xxcftr, in_cfg=config_in, out_cfg=config, type=3 $
                        else config = config_in

loop=''
loop2=''


;list of standard orbitals and their maximum occupation numbers
lorb_std=['s', 'p', 'd', 'f', 'g', 'h', 'i', 'k', 'l', 'm', 'n', 'o', 'q', 'r', 't', 'u']
nlorb_std=n_elements(lorb_std)
orb_std=fix(findgen(nlorb_std))
noccmax_std=2*(2*orb_std+1)


config=strlowcase(config)
config=strtrim(config,2)+' '
lconf=fix(strlen(config))


;when the number of occupation is greater than 9, letters are used
;instead (e.g. 3d10=3da)
strorg9=['a','b','c','d','e']
intorg9=[10,11,12,13,14]
norg9=n_elements(stror9)

;norb=(lconf+1)/4  ;+1 is not needed because +' ' is added to the
;config string
norb=lconf/4
orb=strarr(norb)
lnum=intarr(norb)
orblet=strarr(norb)
nocc=intarr(norb)
strnocc=strarr(norb)
for i=0,norb-1 do begin
  orb[i]=strmid(config,i*4,2)
  orblet[i]=strmid(config,i*4+1,1)
  strnocc[i]=strlowcase(strmid(config,i*4+2,1))
  woreorg9=where(strnocc[i] eq strorg9)
  if woreorg9[0] ne -1 then begin
     nocc[i]=intorg9[woreorg9]
  endif else begin
     nocc[i]=fix(strnocc[i])
  endelse


  for j=0,nlorb_std-1 do begin
    if orblet[i] eq lorb_std[j] then lnum[i]=orb_std[j]
  endfor

endfor


;find the parity
parity=1
for i=0,norb-1 do begin
  parity=parity*(-1)^(lnum[i]*nocc[i])
endfor
if parity eq 1 then begin
    ev=parity
    print,'parity: even'
endif else begin
    od=parity
    print,'parity: odd'
endelse


;find the open shells (otherwise take the last one
;(and include in any case the last shell also if it is not open)
fullorb=intarr(norb)
for i=0,norb-1 do begin
  for j=0,nlorb_std-1 do begin
    if orblet[i] eq lorb_std[j] and nocc[i] eq  noccmax_std[j] then begin
        fullorb[i]=0
    endif
    if orblet[i] eq lorb_std[j] and nocc[i] ne  noccmax_std[j] then begin
        fullorb[i]=1
    endif
  endfor
endfor

opshell=where(fullorb eq 1)
nopshell=n_elements(opshell)

opshellbkp=opshell
opshell32=opshell


if nopshell eq 1 then begin
   ;if all shells are closed
  if opshell[0] eq -1 then begin
    Stot=0.
    Sweight=fix(2.*Stot+1.)
    Ltot=0
    Lstring=strupcase(lorb_std(where(orb_std eq Ltot)))
    terms=strtrim(string(Sweight),2)+strtrim(Lstring,2)
  endif else begin


   ONESHELL:
  if nocc[opshell] eq 0 then begin
    print,'*** ERROR: the empty shells must be omitted - STOP***'
    stop
  endif


   ;one electron only or full-1
   if nocc[opshell] eq 1 or nocc[opshell] eq noccmax_std(where(lorb_std eq orblet[opshell[0]]))-1 then begin
      Stot=0.5
      Sweight=fix(2.*Stot+1.)
      Lstring=strupcase(lorb_std(where(lorb_std eq orblet[opshell[0]])))
      Ltot=orb_std(where(lorb_std eq orblet[opshell[0]]))
      terms=strtrim(string(Sweight),2)+strtrim(Lstring,2)
   endif else begin
  ;more than one electron
      if orblet[opshell] eq 'p' then begin
         if (nocc[opshell] eq 2 or nocc[opshell] eq 4) then begin
            Stot=[0.,0.,1.]
            Ltot=[0,2,1]
            terms=['1S','1D','3P']
         endif else begin
            Stot=[0.5,0.5,1.5]
            Ltot= [1,2,0]
            terms=['2P','2D','4S']
         endelse
      endif
      if orblet[opshell] eq 'd' then begin
         if (nocc[opshell] eq 2 or nocc[opshell] eq 8) then begin
            Stot=[0.,0.,0.,1.,1.]
            Ltot=[0,2,4,1,3]
            terms=['1S','1D','1G','3P','3F']
         endif
         if (nocc[opshell] eq 4 or nocc[opshell] eq 6) then begin
            Stot=[0.,0.,0.,0.,0.,1.,1.,1.,1.,1.,2.,0.,0.,0.,1.,1.]
            Ltot=[0,2,3,4,6,1,2,3,4,5,2,0,2,4,1,3]
            terms=['1S','1D','1F','1G','1I','3P','3D','3F','3G','3H','5D','1S','1D','1G','3P','3F']
         endif
         if (nocc[opshell] eq 3 or nocc[opshell] eq 7) then begin
            Stot=[0.5,0.5,0.5,0.5,0.5,1.5,1.5,0.5]
            Ltot=[1,2,3,4,5,1,3,2]
            terms=['2P','2D','2F','2G','2H','4P','4F','2D']
         endif
         if (nocc[opshell] eq 5) then begin
            Stot=[0.5,0.5,0.5,0.5,0.5,0.5,1.5,1.5,0.5,0.5,0.5,0.5,0.5,1.5,1.5,2.5]
            Ltot=[0,1,2,3,4,5,1,3,2,2,3,4,6,2,4,0]
            terms=['2S','2P','2D','2F','2G','2H','4P','4F','2D','2D','2F','2G','2I','4D','4G','6S']
         endif
      endif
      if orblet[opshell] eq 'f' then begin
         if (nocc[opshell] eq 2 or nocc[opshell] eq 12) then begin
            Stot=[0.,0.,0.,0.,1.,1.,1.]
            Ltot=[0,2,4,6,1,3,5]
            terms=['1S','1D','1G','1I','3P','3F','3H']
         endif
         if (nocc[opshell] eq 3 or nocc[opshell] eq 11) then begin
            Stot=[0.5,0.5,0.5,0.5,0.5,0.5,0.5,0.5,0.5,0.5,0.5,0.5,1.5,1.5,1.5,1.5,1.5]
            Ltot=[1,2,3,4,5,6,7,8,2,3,4,5,0,2,3,4,6]
            terms=['2P','2D','2F','2G','2H','2I','2K','2L','2D','2F','2G','2H','4S','4D','4F','4G','4I']
         endif
         if (nocc[opshell] eq 4 or nocc[opshell] eq 10) then begin
            Stot=[0.,0.,0.,0.,0.,0.,0.,0.,0.,$
                  0.,0.,0.,0.,0.,0.,0.,0.,0.,0.,0.,$
                  1.,1.,1.,1.,1.,1.,1.,1.,1.,$
                  1.,1.,1.,1.,1.,1.,1.,1.,1.,1.,1.,1.,1.,$
                  2.,2.,2.,2.,2.]
            Ltot=[0,2,3,4,5,6,7,8,10,$
                  0,2,2,2,4,4,4,5,6,6,8,$
                  1,2,3,4,5,6,7,8,9,$
                  1,1,2,3,3,3,4,4,5,5,5,6,7,$
                  0,2,3,4,6]
            terms=['1S','1D','1F','1G','1H','1I','1K','1L','1N',$
                   '1S','1D','1D','1D','1G','1G','1G','1H','1I','1I','1L',$
                   '3P','3D','3F','3G','3H','3I','3K','3L','3M',$
                   '3P','3P','3D','3F','3F','3F','3G','3G','3H','3H','3H','3I','3K',$
                   '5S','5D','5F','5G','5I']
         endif
         if (nocc[opshell] eq 5 or nocc[opshell] eq 9) then begin
            Stot=[0.5,0.5,0.5,0.5,0.5,0.5,0.5,0.5,0.5,0.5,0.5,$
                  1.5,1.5,1.5,1.5,1.5,1.5,1.5,1.5,1.5,1.5,$
                  2.5,2.5,2.5,$
                  0.5,0.5,0.5,0.5,0.5,0.5,0.5,0.5,0.5,0.5,0.5,0.5,0.5,0.5,0.5,0.5,0.5,0.5,$
                  0.5,0.5,0.5,0.5,0.5,0.5,0.5,0.5,0.5,0.5,0.5,0.5,0.5,0.5,0.5,0.5,0.5,$
                  1.5,1.5,1.5,1.5,1.5,1.5,1.5,1.5,1.5,1.5,1.5,1.5,1.5,1.5]
            Ltot=[1,2,3,4,5,6,7,8,9,10,11,$
                  0,1,2,3,4,5,6,7,8,9,$
                  1,3,5,$
                  1,1,1,2,2,2,2,3,3,3,3,3,3,4,4,4,4,4,$
                  5,5,5,5,5,5,6,6,6,6,7,7,7,7,8,8,9,$
                  1,2,2,3,3,3,4,4,4,5,5,6,6,7]
            terms=['2P','2D','2F','2G','2H','2I','2K','2L','2M','2N','2O',$
                   '4S','4P','4D','4F','4G','4H','4I','4K','4L','4M',$
                   '6P','6F','6H',$
                   '2P','2P','2P','2D','2D','2D','2D','2F','2F','2F','2F','2F','2F','2G','2G','2G','2G','2G',$
                   '2H','2H','2H','2H','2H','2H','2I','2I','2I','2I','2K','2K','2K','2K','2L','2L','2M',$
                   '4P','4D','4D','4F','4F','4F','4G','4G','4G','4H','4H','4I','4I','4K']

         endif
         if (nocc[opshell] eq 6 or nocc[opshell] eq 8) then begin
            Stot=[0.,0.,0.,0.,0.,0.,0.,0.,0.,0.,0.,0.,$
                  1.,1.,1.,1.,1.,1.,1.,1.,1.,1.,1.,$
                  2.,2.,2.,2.,2.,2.,2.,2.,2.,$
                  3.,$
                  0.,0.,0.,0.,0.,0.,0.,0.,0.,0.,0.,0.,0.,0.,0.,0.,0.,0.,$
                  0.,0.,0.,0.,0.,0.,0.,0.,0.,0.,0.,0.,0.,0.,0.,0.,$
                  1.,1.,1.,1.,1.,1.,1.,1.,1.,1.,1.,1.,1.,1.,1.,1.,1.,$
                  1.,1.,1.,1.,1.,1.,1.,1.,1.,1.,1.,1.,1.,1.,1.,1.,1.,1.,1.,$
                  1.,1.,1.,1.,1.,1.,1.,1.,1.,$
                  2.,2.,2.,2.,2.,2.,2.]
            Ltot=[0,1,2,3,4,5,6,7,8,9,10,12,$
                  1,2,3,4,5,6,7,8,9,10,11,$
                  0,1,2,3,4,5,6,7,8,$
                  3,$
                  0,0,0,2,2,2,2,2,3,3,3,4,4,4,4,4,4,4,$
                  5,5,5,6,6,6,6,6,6,7,7,8,8,8,9,10,$
                  1,1,1,1,1,2,2,2,2,3,3,3,3,3,3,3,3,$
                  4,4,4,4,4,4,5,5,5,5,5,5,5,5,6,6,6,6,6,$
                  7,7,7,7,7,8,8,9,9,$
                  2,2,3,4,4,5,6]
            terms=['1S','1P','1D','1F','1G','1H','1I','1K','1L','1M','1N','1Q',$
                   '3P','3D','3F','3G','3H','3I','3K','3L','3M','3N','3O',$
                   '5S','5P','5D','5F','5G','5H','5I','5K','5L',$
                   '7F',$
                   '1S','1S','1S','1D','1D','1D','1D','1D','1F','1F','1F','1G','1G','1G','1G','1G','1G','1G',$
                   '1H','1H','1H','1I','1I','1I','1I','1I','1I','1K','1K','1L','1L','1L','1M','1N',$
                   '3P','3P','3P','3P','3P','3D','3D','3D','3D','3F','3F','3F','3F','3F','3F','3F','3F',$
                   '3G','3G','3G','3G','3G','3G','3H','3H','3H','3H','3H','3H','3H','3H','3I','3I','3I','3I','3I',$
                   '3K','3K','3K','3K','3K','3L','3L','3M','3M',$
                   '5D','5D','5F','5G','5G','5H','5I']
         endif
         if (nocc[opshell] eq 7) then begin
            Stot=[0.5,0.5,0.5,0.5,0.5,0.5,0.5,0.5,0.5,0.5,0.5,0.5,0.5,$
                  1.5,1.5,1.5,1.5,1.5,1.5,1.5,1.5,1.5,1.5,1.5,$
                  2.5,2.5,2.5,2.5,2.5,2.5,$
                  3.5,$
                  0.5,0.5,0.5,0.5,0.5,0.5,0.5,0.5,0.5,0.5,0.5,0.5,0.5,0.5,0.5,0.5,0.5,0.5,0.5,0.5,$
                  0.5,0.5,0.5,0.5,0.5,0.5,0.5,0.5,0.5,0.5,0.5,0.5,0.5,0.5,0.5,0.5,0.5,$
                  0.5,0.5,0.5,0.5,0.5,0.5,0.5,0.5,0.5,0.5,0.5,0.5,0.5,0.5,0.5,0.5,0.5,0.5,0.5,0.5,0.5,0.5,$
                  1.5,1.5,1.5,1.5,1.5,1.5,1.5,1.5,1.5,1.5,1.5,1.5,1.5,1.5,1.5,1.5,1.5,$
                  1.5,1.5,1.5,1.5,1.5,1.5,1.5,1.5,1.5,1.5,1.5,1.5]
            Ltot=[0,1,2,3,4,5,6,7,8,9,10,11,12,$
                  0,1,2,3,4,5,6,7,8,9,10,$
                  1,2,3,4,5,6,$
                  0,$
                  0,1,1,1,1,2,2,2,2,2,2,3,3,3,3,3,3,3,3,3,$
                  4,4,4,4,4,4,4,4,4,5,5,5,5,5,5,5,5,$
                  6,6,6,6,6,6,6,6,7,7,7,7,7,7,8,8,8,8,9,9,9,10,$
                  0,1,2,2,2,2,2,3,3,3,3,4,4,4,4,4,4,$
                  5,5,5,5,6,6,6,6,7,7,8,8]
            terms=['2S','2P','2D','2F','2G','2H','2I','2K','2L','2M','2N','2O','2Q',$
                   '4S','4P','4D','4F','4G','4H','4I','4K','4L','4M','4N',$
                   '6P','6D','6F','6G','6H','6I',$
                   '8S',$
                   '2S','2P','2P','2P','2P','2D','2D','2D','2D','2D','2D','2F','2F','2F','2F','2F','2F','2F','2F','2F',$
                   '2G','2G','2G','2G','2G','2G','2G','2G','2G','2H','2H','2H','2H','2H','2H','2H','2H',$
                   '2I','2I','2I','2I','2I','2I','2I','2I','2K','2K','2K','2K','2K','2K','2L','2L','2L','2L','2M','2M','2M','2N',$
                   '4S','4P','4D','4D','4D','4D','4D','4F','4F','4F','4F','4G','4G','4G','4G','4G','4G',$
                   '4H','4H','4H','4H','4I','4I','4I','4I','4K','4K','4L','4L']
         endif
      endif
      if orblet[opshell] ne 'p' and  orblet[opshell] ne 'd'  and orblet[opshell] ne 'f' then begin
         print,'*** NOT IMPLEMENTED FOR ORBITAL BEYOND f -STOP ***'
         stop
      endif

   endelse

  endelse
endif


MORESHELL:

if loop eq 'cont' then begin
   if opshell[0] eq opshell0 then begin
      goto,CONTINUE0
   endif else begin
      goto, CONTINUE1
   endelse
endif

if nopshell eq 2 or n_elements(opshell32) eq 2 then begin

   loop='cont'

   if loop2 ne 'cont2' then begin
      opshell=opshellbkp[0]
      opshell0=opshellbkp[0]
   endif else begin
      opshell=opshellbkp[1]
      opshell0=opshellbkp[1]
      newopshell=opshell32
   endelse

   goto,ONESHELL
   CONTINUE0:
   L1=Ltot
   S1=Stot
   if opshell[0] eq opshellbkp[1] then begin
      opshell=opshellbkp[2]
   endif else begin
      opshell=opshellbkp[1]
   endelse
   goto,ONESHELL
   CONTINUE1:
   L2=Ltot
   S2=Stot

   str_store = ''

   if n_elements(L1) ne n_elements(S1) and n_elements(L2) ne n_elements(S2) then begin
      print,'*** ERROR IN THE VALUES OF L1S1 OR L2S2 COUPLE - STOP ***'
      stop
   endif
   for gg=0,n_elements(L2)-1 do begin
     for hh=0,n_elements(L1)-1 do begin
       Lmin=abs(L1[hh]-L2[gg])
       Lmax=L1[hh]+L2[gg]
       Smin=abs(S1[hh]-S2[gg])
       Smax=S1[hh]+S2[gg]
       if Lmin ne Lmax then begin
          nL=Lmax-Lmin+1
          Lterm=intarr(nL)
          Lterm[0]=Lmin
          Lstring=strarr(nL)
          for ll=1,nL-1 do begin
            Lterm[ll]=Lmin+ll
          endfor
          for ll=0,nL-1 do begin
            Lstring[ll]=strupcase(lorb_std(where(orb_std eq Lterm[ll])))
          endfor
       endif else begin
          nL=1
          Lterm=Lmin
          Lstring=strupcase(lorb_std(where(orb_std eq Lmin)))
       endelse
       if Smin ne Smax then begin
          nS=fix(Smax-Smin+1)
          Sterm=fltarr(nS)
          Sweight=intarr(nS)
          Sterm[0]=Smin
          Sstring=strarr(nS)
          for ss=1,nS-1 do begin
            Sterm[ss]=Smin+ss
          endfor
          for ss=0,nS-1 do begin
            Sweight[ss]=fix(2.*Sterm[ss]+1.)
          endfor
        endif else begin
          nS=1
          Sterm=Smin
          Sweight=fix(2.*Smin+1.)
        endelse
        nterm=nL*nS
        terms=strarr(nterm)
        Ltot=intarr(nterm)
        Stot=fltarr(nterm)
        Lstringtot=strarr(nterm)

        for ll=0,nL-1 do begin
          Stot[ll*nS:(ll+1)*nS-1]=Sterm
        endfor
        for ss=0,nS-1 do begin
          Ltot[ss*nL:(ss+1)*nL-1]=Lterm
        endfor
        Ltot=Ltot(sort(Ltot))

        Sweighttot=fix(2.*Stot+1.)
        for i=0,nterm-1 do begin
          Lstringtot[i]=strupcase(lorb_std(where(orb_std eq Ltot[i])))
        endfor
        for i=0,nterm-1 do begin
          terms[i]=strtrim(string(Sweighttot[i]),2)+strtrim(Lstringtot[i],2)
        endfor

        str_store = [str_store, terms]

     endfor
   endfor

   str_store = str_store[1:*]
   nset= n_elements(str_store)

   stringt=strarr(nset)
   strn=''
   numtermstr=intarr(nset)
   for w=0,nset-1 do begin
     strn = str_store[w]
     stringt[w]=strtrim(strn,2)+' '
     numtermstr[w]=fix(strlen(stringt[w]))/3.
   endfor

   numtermtot=fix(total(numtermstr))
   terms=strarr(numtermtot)
   Ltot=intarr(numtermtot)
   Stot=fltarr(numtermtot)

   for w=0,nset-1 do begin
     iterms=strarr(numtermstr[w])
     for xx=0,numtermstr[w]-1 do begin
       iterms[xx]=strtrim(strmid(stringt[w],xx*3,2),2)
       if w eq 0 then begin
         aa=fix(xx)
       endif else begin
         aa=fix(xx+total(numtermstr[0:w-1]))
       endelse
       terms[aa]=iterms[xx]
     endfor
   endfor

   for tt=0,numtermtot-1 do begin
     Lnummore=strlowcase(strmid(terms[tt],1,1))
     Ltot[tt]=orb_std(where(lorb_std eq Lnummore))
     Stot[tt]=(float(strmid(terms[tt],0,1))-1.)/2.
   endfor

endif

;three open shells
if loop2 eq 'cont2' then begin


   if opshell[0] eq opshell30 then begin
      goto,CONTINUE03S
   endif else begin
      goto,CONTINUE13S
   endelse
endif

if nopshell eq 3 then begin

   loop2='cont2'



   opshell=opshellbkp[0]
   opshell30=opshellbkp[0]
   goto,ONESHELL
   CONTINUE03S:
   L13S=Ltot
   S13S=Stot
   opshell=opshellbkp[1:2]
   opshell32=opshellbkp[1:2]
   goto,MORESHELL
   CONTINUE13S:
   L23S=Ltot
   S23S=Stot


   str_store = ''

   if n_elements(L13S) ne n_elements(S13S) and n_elements(L23S) $
      ne n_elements(S23S) then begin
      print,'*** ERROR IN THE VALUES OF L1S1 OR L2S2 COUPLE - STOP ***'
      stop
   endif
   for gg=0,n_elements(L23S)-1 do begin
     for hh=0,n_elements(L13S)-1 do begin
       Lmin=abs(L13S[hh]-L23S[gg])
       Lmax=L13S[hh]+L23S[gg]
       Smin=abs(S13S[hh]-S23S[gg])
       Smax=S13S[hh]+S23S[gg]
       if Lmin ne Lmax then begin
          nL=Lmax-Lmin+1
          Lterm=intarr(nL)
          Lterm[0]=Lmin
          Lstring=strarr(nL)
          for ll=1,nL-1 do begin
            Lterm[ll]=Lmin+ll
          endfor
          for ll=0,nL-1 do begin
            Lstring[ll]=strupcase(lorb_std(where(orb_std eq Lterm[ll])))
          endfor
       endif else begin
          nL=1
          Lterm=Lmin
          Lstring=strupcase(lorb_std(where(orb_std eq Lmin)))
       endelse
       if Smin ne Smax then begin
          nS=fix(Smax-Smin+1)
          Sterm=fltarr(nS)
          Sweight=intarr(nS)
          Sterm[0]=Smin
          Sstring=strarr(nS)
          for ss=1,nS-1 do begin
            Sterm[ss]=Smin+ss
          endfor
          for ss=0,nS-1 do begin
            Sweight[ss]=fix(2.*Sterm[ss]+1.)
          endfor
        endif else begin
          nS=1
          Sterm=Smin
          Sweight=fix(2.*Smin+1.)
        endelse
        nterm=nL*nS
        terms=strarr(nterm)
        Ltot=intarr(nterm)
        Stot=fltarr(nterm)
        Lstringtot=strarr(nterm)

        for ll=0,nL-1 do begin
          Stot[(ll*nS):(ll+1)*nS-1]=Sterm
        endfor
        for ss=0,nS-1 do begin
          Ltot[(ss*nL):(ss+1)*nL-1]=Lterm
        endfor
        Ltot=Ltot(sort(Ltot))

        Sweighttot=fix(2.*Stot+1.)
        for i=0,nterm-1 do begin
          Lstringtot[i]=strupcase(lorb_std(where(orb_std eq Ltot[i])))
        endfor
        for i=0,nterm-1 do begin
          terms[i]=strtrim(string(Sweighttot[i]),2)+strtrim(Lstringtot[i],2)
        endfor

        str_store = [str_store, terms]

     endfor
   endfor

   str_store = str_store[1:*]

   nset=n_elements(str_store)

   stringt=strarr(nset)
   strn=''
   numtermstr=intarr(nset)
   for w=0,nset-1 do begin
     strn = str_store[w]
     stringt[w]=strtrim(strn,2)+' '

     numtermstr[w]=fix(strlen(stringt[w]))/3.
   endfor

   numtermtot=fix(total(numtermstr))
   terms=strarr(numtermtot)
   Ltot=intarr(numtermtot)
   Stot=fltarr(numtermtot)

   for w=0,nset-1 do begin
     iterms=strarr(numtermstr[w])
     for xx=0,numtermstr[w]-1 do begin
       iterms[xx]=strtrim(strmid(stringt[w],xx*3,2),2)
       if w eq 0 then begin
         aa=fix(xx)
       endif else begin
         aa=fix(xx+total(numtermstr[0:w-1]))
       endelse
       terms[aa]=iterms[xx]
     endfor
   endfor

   for tt=0,numtermtot-1 do begin
     Lnummore=strlowcase(strmid(terms[tt],1,1))
     Ltot[tt]=orb_std(where(lorb_std eq Lnummore))
     Stot[tt]=(float(strmid(terms[tt],0,1))-1.)/2.
   endfor

endif

opshell=opshellbkp

end
