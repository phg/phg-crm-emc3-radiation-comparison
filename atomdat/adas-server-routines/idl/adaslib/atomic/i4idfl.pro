;----------------------------------------------------------------------
;+
; PROJECT:
;       ADAS
;
; NAME:
;       i4idfl
;
; PURPOSE:
;       IDL translation of FORTRAN routine I4IDFL.FOR which
;       returns a unique index number based on the value of the
;       n and l quantum numbers passed to it. The index is used to
;       reference arrays containing data dependent on the n and l
;       quantum numbers.
;
; USE:
;       General
;
; INPUTS:
;       N,L      -       quantum numbers.
;
; OPTIONAL INPUTS:
;       None
;
; OUTPUTS:
;       I4IDFL    -     Index.
;
; OPTIONAL OUTPUTS:
;       None
;
; KEYWORD PARAMETERS:
;       idl_index - if set returns index assuming 0, not 1 as for 
;                   fortran, as the first element in the array.
;
; CALLS:
;       None
;
; SIDE EFFECTS:
;       None
;
; CATEGORY:
;       Adas system.
;
; WRITTEN:
;       Martin O'Mullane
;
; MODIFIED:
;       1.1     Martin O'Mullane
;                - First release
;       1.2     Martin O'Mullane
;                - Add idl_index keyword
;
; VERSION:
;       1.1     07-10-2005
;       1.2     14-03-2007
;
;-----------------------------------------------------------------------------

FUNCTION i4idfl, n, l, idl_index=idl_index

; Check odd cases

index = ((N * (N - 1)) / 2) + L + 1

if keyword_set(idl_index)  then index = index - 1

return, index

END
