;-----------------------------------------------------------------------------
;+
; PROJECT:
;       ADAS
;
; NAME: XXESYM()
;
; PURPOSE:
;       Convert atomic number (no. of protons) into element symbol.
;
; EXPLANATION:
;
; USE:
;       General.
;
; INPUTS:
;       IZ0	-	Integer - the atomic number of the element
;
;
; OPTIONAL INPUTS:
;       None.
;
; OUTPUTS:
;       ENAME	-	String - the name of the element (blank if IZ0
;			         is out of range).
;
; OPTIONAL OUTPUTS:
;       None
;
; KEYWORD PARAMETERS:
;       LOWER   -       Returns string in lower case.
;       UPPER   -       Returns string in upper case.
;
; CALLS:
;       None
;
; SIDE EFFECTS:
;       None
;
; CATEGORY:
;       System routine.
;
; WRITTEN:
;       Martin O'Mullane
;
; MODIFIED:
;	1.1	Martin O'Mullane
;		  First release 
;	1.2	Allan Whiteford
;		  Added upper and lower keywords.
;	1.3	Martin O'Mullane
;		  Vectorize
;
; VERSION:
;       1.1	28-01-2000
;       1.2	26-04-2007
;       1.3	12-03-2018
;
;-----------------------------------------------------------------------------

FUNCTION xxesym, z0, lower=lower, upper=upper

    ON_ERROR, 2

    symbol = ['H','He','Li','Be','B','C','N','O','F','Ne','Na','Mg']
    symbol = [symbol,'Al','Si','P', 'S', 'Cl','Ar','K', 'Ca','Sc','Ti','V','Cr']
    symbol = [symbol,'Mn','Fe','Co','Ni','Cu','Zn','Ga','Ge','As','Se','Br']
    symbol = [symbol,'Kr','Rb','Sr','Y', 'Zr','Nb','Mo','Tc','Ru','Rh','Pd']
    symbol = [symbol,'Ag','Cd','In','Sn','Sb','Te','I', 'Xe','Cs','Ba','La']
    symbol = [symbol,'Ce','Pr','Nd','Pm','Sm','Eu','Gd','Tb','Dy','Ho','Er']
    symbol = [symbol,'Tm','Yb','Lu','Hf','Ta','W', 'Re','Os','Ir','Pt','Au']
    symbol = [symbol,'Hg','Tl','Pb','Bi','Po','At','Rn','Fr','Ra','Ac','Th','Pa','U']

    num   = n_elements(z0)
    ename = strarr(num)
    
    for j = 0, num-1 do begin
    
       iz0 = fix(z0[j])

       if (iz0 le 0) or (iz0 gt 92) then begin
           ename[j] = '  '
       endif else begin
           ename[j] = symbol[iz0-1]
       endelse
       
    endfor
    
    if num EQ 1 then ename = ename[0]

    if keyword_set(lower) then return,strlowcase(ename)
    if keyword_set(upper) then return,strupcase(ename)
    return, ename

END
