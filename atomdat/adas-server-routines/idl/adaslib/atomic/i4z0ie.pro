; Copyright (c) 1995, Strathclyde University
; SCCS info: Module @(#)$Header: /home/adascvs/idl/adaslib/atomic/i4z0ie.pro,v 1.3 2004/07/06 14:08:13 whitefor Exp $ Date $Date: 2004/07/06 14:08:13 $
;+
; PROJECT:
;       ADAS IBM MVS to UNIX conversion
;
; NAME:
;       i4z0ie
;
; PURPOSE:
;	Returns the element symbol esym for the given nuclear charge.
;
; USE:
;       General
;
; INPUTS:
;       ISYM	-	The integer nuclear charge of the element.
;
; OPTIONAL INPUTS:
;       None
;
; OUTPUTS:
;       ESYM	-	The symbol of the corresponding element. 
;			If the value of isym is not valid then 'XX' is
;			returned.
;
; OPTIONAL OUTPUTS:
;       None
;
; KEYWORD PARAMETERS:
;       None
;
; CALLS:
;       None
;
; SIDE EFFECTS:
;       None
;
; CATEGORY:
;       Adas system.
;
; WRITTEN:
;       Tim Hammond, Tessella Support Services plc, 18/12/95
;
; MODIFIED:
;	1.1	Tim Hammond
;		First release
;	1.2	Richard Martin
;		Added Sb to Xe
;	1.3	Hugh Summers
;		Extended element number to 92
;
; VERSION:
;	1.1	18/12/95
;	1.2	24/02/98
;	1.3	17/09/99
;
;-
;-----------------------------------------------------------------------------

PRO i4z0ie, isym, esym

    ON_ERROR, 2

    symbol = ['H','He','Li','Be','B','C','N','O','F','Ne','Na','Mg']
    symbol = [symbol,'Al','Si','P','S','Cl','Ar','K','Ca','Sc','Ti','V','Cr']
    symbol = [symbol,'Mn','Fe','Co','Ni','Cu','Zn','Ga','Ge','As','Se','Br']
    symbol = [symbol,'Kr','Rb','Sr','Y','Zr','Nb','Mo','Tc','Ru','Rh','Pd']
    symbol = [symbol,'Ag','Cd','In','Sn','Sb','Te','I','Xe','Cs','Ba','La']
    symbol = [symbol,'Ce','Pr','Nd','Pm','Sm','Eu','Gd','Tb','Dy','Ho','Er']
    symbol = [symbol,'Tm','Yb','Lu','Hf','Ta','W ','Re','Os','Ir','Pt','Au']
    symbol = [symbol,'Hg','Tl','Pb','Bi','Po','At','Rn','Fr','Ra','Ac','Th','Pa','U ']

    if isym gt 0 and isym lt 93 then begin
	esym = symbol(isym-1)
    endif else begin
	esym = 'XX'
    endelse
      
    RETURN

END
