; Copyright (c) 1995, Strathclyde University.
; Alessandro Lanzafame  acl@phys.strath.ac.uk   14 March 1996
;+
; PROJECT:
;       ADAS support programs
;
; NAME:
;       roman2z
;
; PURPOSE:
;       Given the spectroscopic symbol for the ion charge (roman
;       numeral), this function returns the ion charge (arabic
;       numeral). The maximum number is 92.
;
; EXPLANATION:
;       It builds up the roman number sequence and compare it with the
;       input. Exit when these two strings are equal, returning the
;       counter minus one. It returns -1 if the input is not a valid
;       number or if the input is out of range (greater than 92).
;
; USE:
;       Example:
;
;           izs = roman2z('X')
;
;       returns izs=9.
;
; INPUTS:
;       SPNUM = string containing the spectroscopic nontation as
;               roman numeral.
;
; OPTIONAL INPUTS:
;       None
;
; OUTPUTS:
;       This function returns the the ion charge (integer)
;       corresponding to the spectroscopic notation (roman numeral)
;       in input.
;
; OPTIONAL OUTPUTS:
;       None
;
; KEYWORD PARAMETERS:
;       None
;
; CALLS:
;       None
;
; SIDE EFFECTS:
;       None
;
; CATEGORY:
;       Adas support and applications.
;
; WRITTEN:
;       Alessandro Lanzafame, University of Strathclyde, mar14-95
;
; MODIFIED:
;       1.0     Alessandro Lanzafame
;                - First version
;       1.1     Tim Hammond
;                - Put under S.C.C.S. control
;       1.2     Alessandro Lanzafame
;                - Added trimming of input string
;       1.3     Martin O'Mullane
;                - Vectorize.
;                - If not a valid Roman number return -1
;
; VERSION:
;       1.0     14-03-1995
;       1.1     03-05-1996
;       1.2     24-02-1997
;       1.3     11-09-2017
;
;-------------------------------------------------------------------------
;-

FUNCTION roman2z, spnum

; Construct Roman numbers

SPECU = ['I','II','III','IV','V','VI','VII','VIII','IX']
SPECT = ['X','XX','XXX','XL','L','LX','LXX','LXXX','XC']
ISPEC = [1,2,3,2,1,2,3,4,2]
rnum  = strarr(92)

for l = 1, 92 do begin
    if l lt 10 then begin
        rnum[l-1] = specu[l-1]
    endif else begin
        kt = l/10
        ku = l-kt*10
        if ku eq 0 then begin
           rnum[l-1] = spect[kt-1]
        endif else begin
           itt = ispec[kt-1]
           iuu = ispec[ku-1]
           rnum[l-1] = spect[kt-1]+specu[ku-1]
        endelse
    endelse
endfor


; Compare input against Roman number list

nval = n_elements(spnum)
z1   = intarr(nval) - 1

for j = 0, nval-1 do begin
   res = where(rnum EQ strtrim(spnum[j],2), c1)
   if c1 EQ 1 then z1[j] = res[0]
endfor

if nval EQ 1 then return, z1[0] else return, z1

END
