;----------------------------------------------------------------------
;+
; PROJECT:
;       ADAS
;
; NAME:
;       tev_alf_s
;
; PURPOSE:
;       Returns the electron temperature in eV at which the 
;       recombination coefficient alf(z+1 --> z) equals the 
;       ionisation coefficient S(z --> z+1) using Seaton (1964)
;       expressions for the coefficients.
;
; USE:
;       General
;
; INPUTS:
;       z         = ionising ion charge (= recombined ion charge).
;       ip_ev     = ionisation potential of ion z (eV).
;
; OPTIONAL INPUTS:
;       See keyword parameters
;
; OUTPUTS:
;       tev_alf_s =  function returned value = electron temperature (eV).
;
; OPTIONAL OUTPUTS:
;       None
;
; KEYWORD PARAMETERS:
;       zeta      = number of equivalent electrons for ionisation
;                   coefficient (defaults to 1.0).
;       ph_frac   = phase space availability fraction for recombination
;                   coefficient (defaults to 1.0).
;       accur     = fractional change in temperature at which 
;                   iteration terminates (defaults to 0.001).     
;
; CALLS:
;       None
;
; SIDE EFFECTS:
;       None
;
; CATEGORY:
;       Adas system.
;
; WRITTEN:
;       Hugh Summers
;
; MODIFIED:
;       1.1     Hugh Summers
;                - First release
;
; VERSION:
;       1.1     9-10-2008
;
;-----------------------------------------------------------------------------

FUNCTION tev_alf_s, z, ip_ev, zeta = zeta, ph_frac = ph_frac, accur=accur 

;---------------------------------------------
;  initialise
;---------------------------------------------

    if n_elements(accur) EQ 0 then accur = 0.001
    
    if n_elements(zeta) EQ 0 then zeta = 1.0
    
    if n_elements(ph_frac) EQ 0 then ph_frac = 1.0

    const = 1.105e7*ph_frac/(zeta*ip_ev*(z+1)^2)
    
;---------------------------------------------
;  bracket solution initially
;---------------------------------------------
     x  = 1.0
     vx = 1.0-const*exp(-x)/x
     if vx gt 0.0 then begin
        a  = x
        va = vx
        b  = x/2.0
        while 1.0-const*exp(-b)/b ge 0.0 do begin
           b=b/2.0
           vb = 1.0-const*exp(-b)/b 
        endwhile 
        vb = 1.0-const*exp(-b)/b 
     endif else begin
        b  = x
        vb = vx
        a  = 2.0*x
        while 1.0-const*exp(-a)/a le 0.0 do begin
           a = 2.0*a
           va = 1.0-const*exp(-a)/a  
        endwhile
        va = 1.0-const*exp(-a)/a  
     endelse 

;    print,'a,va,b,vb=',a,va,b,vb
      
;----------------------------------------------------
;  alternate mean and halving steps until convergence
;----------------------------------------------------

     x1 = x                                           
     x = (a*vb-b*va)/(vb-va)                          
     while (abs(x1-x)/x gt accur) do begin            
;        print,'x,x1=',x,x1                           
         x1=x                                         
         vx = 1.0-const*exp(-x)/x                     
         if vx gt 0.0 then begin                      
              a=x
              va=vx
              x=0.5*(a+b)
              vx = 1.0-const*exp(-x)/x
;             print,'a,va,x,vx,b,vb=',a,va,x,vx,b,vb
         endif else begin
              if vx eq 0.0 then return, ip_ev/x
              b=x
              vb=vx
              x=0.5*(a+b)
              vx = 1.0-const*exp(-x)/x
;             print,'a,va,x,vx,b,vb=',a,va,x,vx,b,vb
         endelse
         
         if vx gt 0.0 then begin                      
              a=x
              va=vx
              x=(a*vb-b*va)/(vb-va)
              vx = 1.0-const*exp(-x)/x
         endif else begin
              if vx eq 0.0 then return, ip_ev/x
              b=x
              vb=vx
              x=(a*vb-b*va)/(vb-va)
              vx = 1.0-const*exp(-x)/x
         endelse
 ;       print,'a,va,x,vx,b,vb=',a,va,x,vx,b,vb      
              
     endwhile                                         
                                                      
 ;   print,'a,va,x,vx,b,vb=',a,va,x,vx,b,vb           
   
     return, ip_ev/x                                                       

END
