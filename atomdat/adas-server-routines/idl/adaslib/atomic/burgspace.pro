; Copyright (c) 2005 Strathclyde University .
;+
; PROJECT:
;       ADAS 
;
; NAME:
;       burgspace
;
; PURPOSE:
;       Takes (effective) collision strangth data and returns the
;       data in Burgess-Tully space.
;
; EXPLANATION:
;       The routine takes input data for a single transition and returns new
;       data mapped on to Burgess-Tully space. See A Burgess and J A Tully,
;       Astron & Astrophys. 254 436-453. Infinite energy (i.e. x=1) limit 
;       points are also computed when asked for/possible. The code is
;       loosely based around code appearing in read_adf04.pro by
;       Martin O'Mullane.
;
; INPUTS:
;        xin   - Temperature/Energy of effective collision strength / collision strength.
;        yin   - Effective collision strength / collision strength.
;        ediff - Energy difference
;
; OPTIONAL INPUTS:
;       None.
;
; OUTPUTS:
;       xout   - Burgess-Tully X parameter (transition type dependent)
;       yout   - Burgess-Tully Y parameter (transition type dependent)
; EXAMPLE:
;	burgspace,x1,reform(plot1),xb1,yb1,info.data1.wa[index2]-info.data1.wa[index1],type=type,avalue=info.data1.aval[(*info.didx1)[info.selected]],stat_wt=1d0+2*info.data1.xja[index2],born=info.data1.beth[(*info.didx1)[info.selected]]

;
; OPTIONAL OUTPUTS:
;       None
;
; KEYWORD PARAMETERS:
;       c            - Burgess-Tully C parameter (assumed c=e if unspecified)
;       type         - Transition type:
;                                 1 - dipole
;                                 2 - non-dipole, non-spin-change
;                                 3 - spin-change
;                                 4 - weak dipole
;       energy       - Set to indicate energies/omegas are being specified
;                      rather than temperatures/upsilons.
;       avalue       - A-value of transition (for type 1 limit points)
;       linestrength - Linestrength of transition (for type 1 limit points)
;       stat_wt      - Statistical weight of upper level (for type 1 limit points)
;       born         - Born limit point for type 2 transitions
;       ev           - Set to indicate temperarues are in eV.
;       kelvin       - Set to indicate temperarues are in Kelvin (default).
;       help         - Display help and exit
;
;  Note that the procedure will attempt to automatically identify the transition
;  type if it isn't specified. Also note that to get type 1 limit points, it's only
;  necessary to specify the linestrenth, or the avalue and statisical weight.
;
; CALLS:
;       None
;       
; SIDE EFFECTS:
;       None
;
; CATEGORY:
;       Utility
;
; WRITTEN:
;       Allan Whiteford
;
; MODIFIED:
;       1.1     Allan Whiteford
;               First release 
;
; VERSION:
;       1.1     29-06-2005
;
;-
;-----------------------------------------------------------------------------


pro burgspace,xin,yin,xout,yout,ediff,					$
                                      type=type,			$
                                      c=c,				$
                                      energy=energy,			$
                                      avalue=avalue,			$
                                      stat_wt=stat_wt,			$
                                      linestrength=linestrength,	$
                                      born=born,			$
                                      ev=ev,				$
                                      kelvin=kelvin,			$
                                      help=help				

        if keyword_set(help) then begin
        	doc_library,'burgspace'
		return
        endif

	if n_params() lt 5 then begin
        	doc_library,'burgspace'
		return
	endif

	if not keyword_set(type) then begin
        	if arg_present(avalue) then begin
                	type=1
                endif else begin
                	ratio=yin[n_elements(yin)-1] / yin[n_elements(yin)-2]
                        if (ratio gt 0.8 or ratio lt 1.2) then begin
				type=2                        
                        endif else begin
                        	type=3
                        endelse
                endelse
        
        endif

	if not keyword_set(c) then c=exp(1)

	if keyword_set(energy) then begin
	
        endif else begin
		if keyword_set(ev) then begin
	        	et=8065.54445d0*xin/ediff ; eV -> cm-1
        	endif else begin
                	et=0.69488623*xin/ediff  ; K -> cm-1
                endelse
        endelse

	limitpoint=0
	if type eq 1 then begin
		if keyword_set(linestrength) then begin
                	limitpoint=1
                endif else begin
                	if keyword_set(avalue) and keyword_set(ediff) and keyword_set(stat_wt) then begin
                		linestrength=493559.34*stat_wt*avalue/(ediff^3)
                		limitpoint=1
                	endif                
                endelse        
        endif

	if type eq 2 then begin
        	if keyword_set(born) then limitpoint=1
        endif

	CASE type OF

          1 : Begin
                xout = 1.0 - alog(c) / alog(et + c)
                yout = yin / alog(et + exp(1d0))
                if limitpoint eq 1 then begin
                   xout = [xout, 1.0]
                   yout = [yout, (4d00/3d0)*linestrength]
                endif
              End
          2 : Begin
                xout = (et) / (et + c)
                yout = yin
                if limitpoint eq 1 then begin
		   if born gt 0.0d0 then begin
                     xout = [xout, 1.0]
                     yout = [yout, born]
                   endif
                endif
              End
          3 : Begin
                xout = (et) / (et + c)
                yout = yin * (et + 1.0)
              End
          4 : Begin
                xout = 1.0 - alog(c) / alog(et + c)
                yout = yin / alog(et + c)
                if  limitpoint eq 1 then begin
                   xout = [xout, 1.0]
                   yout = [yout, (4d00/3d0)*linestrength]
                endif
              End

        ENDCASE


end
