; Copyright (c) 2001, Strathclyde University.
; SCCS info: Module @(#)$Header: /home/adascvs/idl/adaslib/atomic/r8rd2b.pro,v 1.1 2004/07/06 14:42:42 whitefor Exp $ Date $Date: 2004/07/06 14:42:42 $
;----------------------------------------------------------------------
;+
; PROJECT:
;       ADAS
;
; NAME: R8RD2B()
;
; PURPOSE:
;       Calculates hydrogenic bound-bound radial integrals using
;       recurrence relations.
;
; EXPLANATION:
;       Based on the FORTRAN routine r8rd2b.for in adaslib library.
;
; USE:
;       General
;
; INPUTS:
;       IZ0     -       Integer - the atomic number of the element
;       NU      -       Integer -  upper value of N quantum number.
;       LU      -       Integer -  upper value of L quantum number.
;       NL      -       Integer -  lower value of N quantum number.
;       LL      -       Integer -  lower value of L quantum number.
;
;
; OPTIONAL INPUTS:
;       None.
;
; OUTPUTS:
;       R8AH   -       Float - Result of integral.
;
; OPTIONAL OUTPUTS:
;       None
;
; KEYWORD PARAMETERS:
;       None
;
; CALLS:
;       None
;
; SIDE EFFECTS:
;       None
;
; CATEGORY:
;       System routine.
;
; WRITTEN:
;       Martin O'Mullane
;
; MODIFIED:
;       1.1  Martin O'Mullane
;              - First Release   
; VERSION:
;       1.1     10-05-2001.
;
;-----------------------------------------------------------------------------
FUNCTION r8rd2b, NU , LU , NL , LL

P1  = 64.0D0     
P12 = P1^2
P2  = 1.0D0 / P1
P22 = P2^2

ltest = (nu LT 2) or (lu LT 0) or (lu GE nu) or $
        (nl LT 1) or (ll LT 0) or (ll GE nl) or (nl GE nu)
        
if (ltest) then begin

   r8rd2b = 0.0
   
endif else begin

   xnu = double(nu)
   xnl = double(nl)
   
   xnl2 = xnl^2
   xnu2 = xnu^2
   
   V  = 1.0D0 - XNL2 / XNU2
   W  = ( 1.0D0 + XNL / XNU )^2
   U  = 8.0D0 * XNL2 / W^2
   P  = 1.0D0
   JS = 0
   
   for i = 1, nl do begin
   
     XI = double(I)
     P  = P * U * ( 1.0D0 - XI^2 / XNU2 ) / ( XI * ( 2.0D0 * XI - 1.0D0 ) )
     IF (ABS(P) LT P22) THEN begin
        JS = JS - 1
        P  = P * P12
     ENDIF
   
   endfor

   P = 2.0D0 * P * XNL / XNU
   P = 4.0D0 * SQRT( P ) / ( XNU * V^2 )

   for i = nl+1, nu do begin
        
     P  = P * V / W
     IF (ABS( P ) LT P2) THEN begin
        JS = JS - 1
        P  = P * P1
     ENDIF
   
   endfor
   
   
   R8RD2B = 0.0
   
   
   IF (LU - LL) EQ 1 THEN begin
      
      T2 = P
      
      IF (NL - LL) LT 2 THEN begin
      
         T3 = T2 
      
      endif ELSE begin
      
         U  = SQRT( ( 2.0D0 * XNL - 1.0D0 ) * V )
         T3 = U * T2 / 2.0D0
         for I = NL-2 , LL+1 , -1 do begin
            
            XI   = double(I)
            XI1  = double(I + 1)
            XI12 = XI1^2
            T1   = T2
            T2   = T3
            T3   = T2 * ( 4.0D0 * ( XNL2 - XI12 ) +      $
                   XI1 * ( 2.0D0 * XI1 - 1.0D0 ) * V ) - $
                   (2.0D0 * XNL * U * T1)
            U   = sqrt( ( XNL2 - XI^2) * ( 1.0D0 - XI12 / XNU2 ) )
            T3  = T3 / ( 2.0D0 * XNL * U )
            IF ABS( T3 ) GT P1 THEN begin
               JS = JS + 1
               T3 = T3 * P2
               T2 = T2 * P2
            ENDIF
         
         endfor
     
      endelse

      R8RD2B = XNL2^2 * T3^2 * P12^JS
      
   endif


   IF (LL - LU) EQ 1 THEN begin
   
      T2   = P
      XNL1 = double( NL - 1 )
      U    = sqrt( V / ( 1.0D0 - XNL1^2 / XNU2 ) )
      T2   = T2 * U / ( 2.0D0 * XNL )
      
      IF (NL - LL) LT 2 THEN begin
      
         T3 = T2
      
      endif else begin
      
         U  = sqrt( (2.0D0 * XNL - 1.0D0) * $
                    (1.0D0 - ( XNL - 2.0D0 )^2 / XNU2 ) )
         T3 = ( 4.0D0 + XNL1 * V ) * ( 2.0D0 * XNL - 1.0D0 ) * $
              T2 / ( 2.0D0 * XNL * U )
         
         for I = NL-3 , LL , -1 do begin
       
            XI  = double(I)
            XI1 = double(I + 1)
            XI12 = XI1^2
            T1  = T2
            T2  = T3
            T3  = T2 * ( 4.0D0 * ( XNL2 - XI12 ) +        $
                  XI1 * ( 2.0D0 * XI1 + 1.0D0 ) * V ) -   $
                  2.0D0 * XNL * U * T1
            U   = sqrt( (XNL2 - XI12 ) * ( 1.0D0 - XI^2 / XNU2 ) )
            T3  = T3 / ( 2.0D0 * XNL * U )
            
            IF abs(T3) GT P1 THEN begin
               JS = JS + 1
               T3 = T3 * P2
               T2 = T2 * P2
            ENDIF
         
         endfor
      
      endelse

      R8RD2B = XNL2^2 * T3^2 * P12^JS

   endif

endelse



return, r8rd2b

END        
