;----------------------------------------------------------------------
;+
; PROJECT    :  ADAS
;
; NAME       :  r8yip
;
; PURPOSE    :  Evaluates impact parameter cross-section second bessel
;               integral Y [Burgess and Summers: MNRAS (1976) 172,345 - EQN C14].
;
; ARGUMENTS  :  All output arguments will be defined appropriately.
;               Inputs will be converted to correct type, if possible,
;               internally without changing calling type.
;                   result = r8yip(xi, delta)
;
;               NAME      I/O    TYPE    DETAILS
; REQUIRED   :  xi         I     double  z[1/kn-1/kn1]/a0 with z target charge,
;                                        kn, kn1 initial and final electron wave
;                                        numbers and a0 the Bohr radius(ryd)
;               delta      I     double  rc[kn-kn1] with rc the closest approach
;
;
; KEYWORDS      None
;
;
; NOTES      :  Calls fortran code.
;
;
; AUTHOR     :  Martin O'Mullane
;
; DATE       :  16-08-2007
;
;
; MODIFIED:
;         1.1       Martin O'Mullane
;                       - First version.
;
; VERSION:
;         1.1      16-08-2007
;-
;----------------------------------------------------------------------

FUNCTION r8yip, xi, delta

fortdir = getenv('ADASFORT')

if n_elements(xi) EQ 0 OR      $
   n_elements(delta) EQ 0      $
   then message, 'essential parameters missing'

; inputs

xi_in    = double(xi)
delta_in = double(delta)

; result

rate = 0.0D0

dummy = CALL_EXTERNAL(fortdir+'/r8yip_if.so','r8yip_if', $
                      xi_in, delta_in, rate)

return, rate

END
