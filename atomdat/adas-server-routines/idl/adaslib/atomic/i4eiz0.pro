; Copyright (c) 1995, Strathclyde University
; SCCS info: Module @(#)$Header: /home/adascvs/idl/adaslib/atomic/i4eiz0.pro,v 1.3 2004/07/06 14:06:37 whitefor Exp $ Date $Date: 2004/07/06 14:06:37 $
;+
; PROJECT:
;       ADAS IBM MVS to UNIX conversion
;
; NAME:
;       i4eiz0
;
; PURPOSE:
;       IDL translation of FORTRAN routine I4EIZ0.FOR which returns
;	the nuclear charge for the given element symbol esym.
;
; USE:
;       General
;
; INPUTS:
;       ESYM	-	The symbol of the element required (note that
;			case is unimportant as the uppercase of the
;			input value is always taken).
; OPTIONAL INPUTS:
;       None
;
; OUTPUTS:
;       ISYM	-	The integer nuclear charge of the element.
;			If the symbol is not recognised then the value
;			of isym returned is zero.
;
; OPTIONAL OUTPUTS:
;       None
;
; KEYWORD PARAMETERS:
;       None
;
; CALLS:
;       None
;
; SIDE EFFECTS:
;       None
;
; CATEGORY:
;       Adas system.
;
; WRITTEN:
;       Tim Hammond, Tessella Support Services plc, 24/07/95
;
; MODIFIED:
;	1.1	Tim Hammond
;		First release
;	1.2	Richard Martin
;		Added SB to XE.
;	1.3	Hugh Summers
;		extended element number to 92
;
; VERSION:
;	1.1	24/07/95
;	1.2	24/02/98
;	1.3	17/09/99
;
;-----------------------------------------------------------------------------
;-

PRO i4eiz0, esym, isym

    ON_ERROR, 2

    symbol = ['H','HE','LI','BE','B','C','N','O','F','NE','NA','MG']
    symbol = [symbol,'AL','SI','P','S','CL','AR','K','CA','SC','TI','V','CR']
    symbol = [symbol,'MN','FE','CO','NI','CU','ZN','GA','GE','AS','SE','BR']
    symbol = [symbol,'KR','RB','SR','Y','ZR','NB','MO','TC','RU','RH','PD']
    symbol = [symbol,'AG','CD','IN','SN','SB','TE','I','XE','CS','BA','LA']
    symbol = [symbol,'CE','PR','ND','PM','SM','EU','GD','TB','DY','HO','ER']
    symbol = [symbol,'TM','YB','LU','HF','TA','W','RE','OS','IR','PT','AU']
    symbol = [symbol,'HG','TL','PB','BI','PO','AT','RN','FR','RA','AC','TH','PA','U']
              
    symtest = strcompress(strupcase(esym), /remove_all)
    symmatch = where(symbol eq symtest)
    
    if (symmatch(0) gt -1) then begin
        isym = symmatch(0) + 1
    endif else begin
	isym = 0
    endelse
      
    return

END
