;----------------------------------------------------------------------
;+
; PROJECT    :  ADAS
;
; NAME       :  config_orbital_energies
;
; PURPOSE    :  Calculates the shell orbital energies for the ground
;               configuration, or supplied conffiguration, of an ion.
;
; EXPLANATION:
;               Configurations are taken from adf00 datasets if none are
;               supplied. RCN from Cowan is run and orbital energies, along
;               with nlq are returned.
;
;
; ARGUMENTS  :  All output arguments will be defined appropriately.
;
;               NAME      I/O    TYPE   DETAILS
; REQUIRED   :  z0_nuc     I     int    atomic number
;               z_ion      I     int    ionisation stage
; OPTIONAL      config     I     str    full configuration string suitable for adf34
;               n          O     int()  principal quantum number
;               l          O     int()  angular quantum number
;               q          O     int()  occupation number
;               energy     O     real() orbital energy for nlq shell
;               elec       O     int    number of electrons from RCN output file
;
; KEYWORDS
;               help       I     Display help entry
;
; NOTES      :   - Spawns rcn.x and get_orbital.x. Can leave temporary files in
;                  working directory if exits uncleanly.
;                - The configuration string must start ar 1s and list all shells. It
;                  must be suitable for inclusion as adf34. See any adf00 configuration
;                  for a suitable example.
;
; AUTHOR     :  Martin O'Mullane
;
; DATE       :  08-10-2008
;
;
; MODIFIED:
;       1.1     Martin O'Mullane
;               - First version.
;       1.2     Allan Whiteford
;               - Updated to use xxlvals.
;
; VERSION:
;       1.1    08-10-2008
;       1.2    22-06-2009
;-
;----------------------------------------------------------------------

PRO config_orbital_energies, z0_nuc  = z0_nuc,  $
                             z_ion   = z_ion,   $
                             config  = config,  $
                             n       = n,       $
                             l       = l,       $
                             q       = q,       $
                             energy  = energy,  $
                             elec    = elec,    $
                             help    = help


if keyword_set(help) then begin
   doc_library,'config_orbital_energies'
   return
endif

; Location of executables

fortdir = getenv('ADASFORT')

; Compulsory inputs

if n_elements(z0_nuc) EQ 0 then message, 'Atomic number must be given'
if n_elements(z_ion) EQ 0 then message, 'Charge state must be given'

iz0 = fix(z0_nuc)
iz1 = fix(z_ion)

; If no configuration is specified take ground from adf00
; Make sure user supplied configuration start with two spaces (for rcn).

if n_elements(config) EQ 0 then begin
   read_adf00, z0=iz0, z1=iz1, config=cfg
endif else begin
   res = strtrim(config, 2)
   cfg = '  ' + config
endelse


; Split configuration string into shells

res    = strsplit(cfg, count=n_parts, /extract)
n      = intarr(n_parts)
l      = intarr(n_parts)
q      = intarr(n_parts)

lvals = xxlvals(/upper)

for j = 0, n_parts-1 do begin

   ndum = 0
   ldum = 'x'
   qdum = 0
   reads, res[j], ndum, ldum, qdum, format='(i1,a1,i2)'

   n[j] = ndum
   q[j] = qdum

   ind = where(lvals EQ strupcase(ldum), count)
   if count EQ 1 then l[j] = ind[0] else l[j] = -1

endfor

; Remove zero occupancy shells - often found in adf00 high Z configurations
; but keep n_cfg for writing a correct adf34 input file.

n_cfg = n_parts

ind = where(q EQ 0, count, complement=ind_c, ncomplement=n_parts)
if count GT 0 then begin
   n = n[ind_c]
   l = l[ind_c]
   q = q[ind_c]
endif



; Write input file and run RCN

; key all files with a random tag

rand = strmid(strtrim(string(randomu(seed)), 2), 2, 4)

rcn_input  = 'rcn.in_' + rand
rcn_script = 'rcn.sh_' + rand
rcn_output = 'rcn.output_' + rand
rcn_d2     = 'rcn.d2_' + rand


; adf34 input file - restrict to a max of 8 shells and count down for certain
; rare gas cores to ensure the correct number of electrons. See details in
; occ2cow.pro for details. Note the one special case for neutral radon.

res = cfg2occ(strtrim(cfg[0], 2), iz0, iz1)
izc = 0
str = occ2cow(res, iz0, iz1, izc, start_shell)
if iz0 GT 54 then iz1_val = izc-1 else iz1_val = iz1+1
if iz0 EQ 86 AND iz1 EQ 0 then iz1_val = 1
str = string([iz0, iz1_val], format='(2i5)') + '   xx  ground          ' + str

openw, lun, rcn_input, /get_lun

printf, lun, '2  -5    2   10  1.0    5.d-09    5.d-11-2  0130    1.0 0.65  0.0  0.5'
printf, lun, str
printf, lun, '   -1'

free_lun, lun


; RCN script file

openw, lun, rcn_script, /get_lun

printf, lun, '0'
printf, lun, rcn_input
printf, lun, rcn_output
printf, lun, rcn_d2

free_lun,lun



; Run rcn

cmd = fortdir + '/rcn.x < ' + rcn_script
spawn, cmd, res

; Get the orbital energies

cmd = fortdir + '/get_orbital.x < ' + rcn_output
spawn, cmd, res_energies


; Get number of electrons and put energies into 'elec' argument if requested

if arg_present(elec) then begin

   adas_readfile, file=rcn_output, all=all
   ind = where(strpos(all, '.                                      ') NE -1)
   nz  = -1
   reads, all[ind[0]], nz, format='(6x,i3)'
   elec = nz

endif

energy = dblarr(n_parts)

reads, strmid(res_energies[0], 6), energy


; Clean-up by removing all files generated by this routine

files = [rcn_input, rcn_script, rcn_output, rcn_d2]
file_delete, files

END
