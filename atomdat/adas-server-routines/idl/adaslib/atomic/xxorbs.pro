;----------------------------------------------------------------------
;+
; PROJECT    :  ADAS
;
; NAME       :  xxorbs
;
; PURPOSE    :  Returns orbital specifications, i.e.
;               1s, 2s, 2p, 3s, 3p, 3d....
;
; EXPLANATION:
;       This function determines and returns orbital
;	specifications using xxlvals for control
;	of angular momentum specification.
;
; USE:
;       An example;
;               result = xxorbs()
;               print,result
;               1s 2s 2p 3s 3p 3d 4s 4p 4d 4f 5s 5p 5d 5f 5g
;		6s 6p 6d 6f 6g 6h 7s 7p 7d 7f 7g 7h 7i 8s 8p
;		8d 8f 8g 8h 8i 8k
;
; INPUTS:
;       None.
;
; OPTIONAL INPUTS:
;       n: The n-shell to stop at, defaults to 8. Maximum is
;	   determined indirectly by xxlvals.pro.
;
; KEYWORD PARAMETERS:
;       help  : Displays documentation
;       usej  : Include the use of 'j' as an orbital angular
;		momentum specification, this is non-standard.
;	lower:  Return orbitals in lower case (default)
;	upper:  Return orbitals in upper case
;
; OUTPUTS:
;       A string array of orbitals.
;
; OPTIONAL OUTPUTS:
;       maxo:     Maximum occupation number of each orbital.
;
; CALLS:
;       xxlvals : Returns orbital angular momentum specifications.
;
; SIDE EFFECTS:
;       None
;
; NOTES:
;       There has been some confusion as to whether the letter 'j' should
;       be used as an angular momentum specifier, pleas see xxlvals
;	for further discussion.
;
; AUTHOR     :  Allan Whiteford
; 
; DATE       :  18-06-09
; 
;
; MODIFIED:
;       1.1     Allan Whiteford
;               - First version.
;
; VERSION:
;       1.1    18-06-09
;-
;----------------------------------------------------------------------

function xxorbs,n=n,usej=usej,maxo=maxo,upper=upper,lower=lower,help=help
        if keyword_set(help) then begin
                doc_library,'xxorbs'
                return,0
        endif
        lvals=xxlvals(usej=usej,upper=upper,lower=lower)
	if n_elements(n) eq 0 then n=8
        if n gt n_elements(lvals) then return,-1
        orbs=['1' + lvals[0]]
	for i=2,n do orbs=[orbs,replicate(strtrim(string(i),2),i) + lvals]
	if arg_present(maxo) then begin
        	maxo=[2]
		for i=2,n do maxo=[maxo,4*indgen(i)+2]
        endif
        return,orbs
end
