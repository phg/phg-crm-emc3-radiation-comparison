; Copyright (c) 2000 Strathclyde University .
; SCCS info: Module @(#)$Header: /home/adascvs/idl/adaslib/atomic/period_system.pro,v 1.2 2007/03/12 10:45:28 mog Exp $ Date $Date: 2007/03/12 10:45:28 $
;----------------------------------------------------------------------
;+
; PROJECT:
;       ADAS
;
; NAME:
;       PERIOD_SYSTEM
;
; PURPOSE:
;       Returns fundamental data about a selected element.
;
; NOTES:
;       .
;
; INPUT/OUTPUT: cha: nuclear charge of element
;               sym: element symbol
;               mas: element mass (AMU)
;
; KEYWORDS:     charge: search elements by nuclear charge
;               symbol: search elements by element symbol
;               mass: search element by element mass
;               density: contains mass density at normal conditions
;                        on output (unit: kg/m^3)
;               number_density: contains number density at normal conditions
;                        on output (unit: 1/m^3)
;
; CALLS:
;       None
;
; SIDE EFFECTS:
;       None
;
; CATEGORY:
;       UNIX system IDL utility.
;
; WRITTEN:
;       Ralph Dux, 21-03-2000
;
;
; MODIFIED:
;       1.1     Ralph Dux
;               - First release.
;       1.2     Martin O'Mullane
;               - Change from round to square bracket notation
;                 when indexing array elements.
;               - Add standard ADAS documentation header.
;
; VERSION:
;       1.1     21-03-2000
;       1.2     12-03-2007
;
;-
;----------------------------------------------------------------------

pro period_system, cha,   $
                   sym,   $
                   mas,   $
                   charge = charge, $
                   symbol = symbol, $
                   mass   = mass,   $
                   density = density, $
                   number_density= number_density

s =  strarr(107)
c =  intarr(107)
m =  fltarr(107)
d =  fltarr(107)

amu =  1.6605d-27  ; atomic mass unit

s[0:89] = [ ' H',' D',' T','He','Li',$
            'Be',' B',' C',' N',' O',$
            ' F','Ne','Na','Mg','Al',$
            'Si',' P',' S','Cl','Ar',$
            ' K','Ca','Sc','Ti',' V',$
            'Cr','Mn','Fe','Co','Ni',$
            'Cu','Zn','Ga','Ge','As',$
            'Se','Br','Kr','Rb','Sr',$
            ' Y','Zr','Nb','Mo','Tc',$
            'Ru','Rh','Pd','Ag','Cd',$
            'In','Sn','Sb','Te',' I',$
            'Xe','Cs','Ba','La','Ce',$
            'Pr','Nd','Pm','Sm','Eu',$
            'Gd','Tb','Dy','Ho','Er',$
            'Tm','Yb','Lu','Hf','Ta',$
            ' W','Re','Os','Ir','Pt',$
            'Au','Hg','Tl','Pb','Bi',$
            'Po','At','Rn','Fr','Ra' ]

s[90:106] =  ['Ac','Th','Pa',' U','Np',$
              'Pu','Am','Cm','Bk','Cf',$
              'Es','Fm','Md','No','Lw',$
              'Ku','Ha' ]

d[0:89] =  [0.08988, 0.17976, 0.26964, 0.1785, 534.,$
            1870.,   2340.,   2240.,   1.2505,  1.42895, $
            1.696,   0.9002 , 970.,    1739.,   2702., $
            2420.,   1820.,   1960.,   3.2142,  1.784, $
            860.,    1550.,   2990.,  4520.,   5960., $
            6930.,   7200.,   7860.,   8900.,   8900., $
            8920.,   7140.,   5910.,   5350.,   5720., $
            4820.,   3120.,   3.744,   1532.,   2600., $
            4500.,   6500.,   8550.,   10210.,  11500., $
            12600.,  12400.,  11400.,  10500.,  8650., $
            7362.,   6500.,   6690.,  6250.,   4930., $
            5.897,   1873.,   3500.,   6180.,   6700., $
            6700.,   6900.,      0,    7500.,   5245., $
            7960.,   8250.,   8450.,   8760.,   9050., $
            9290.,   7000.,   9820.,   13360.,  16600., $
            19300.,  20530.,  22480.,  22420.,  21450., $
            19290.,  13546.,  11850.,  11340.,  9800., $
                0.,      0.,      0.,      0.,  5000. ]
d[90:106] = [   0.,  11724.,  15370.,  18970.,  20450., $
             19737., 13671.,  13510.,      0.,      0., $
                 0.,     0.,      0.,      0.,      0., $
                 0.,     0.]

c[0:89] = [ 1,  1,  1,  2,  3,  4,  5,  6,  7,  8, $
            9, 10, 11, 12, 13, 14, 15, 16, 17, 18, $
            19, 20, 21, 22, 23, 24, 25, 26, 27, 28,$
            29, 30, 31, 32, 33, 34, 35, 36, 37, 38,$
            39, 40, 41, 42, 43, 44, 45, 46, 47, 48,$
            49, 50, 51, 52, 53, 54, 55, 56, 57, 58,$
            59, 60, 61, 62, 63, 64, 65, 66, 67, 68,$
            69, 70, 71, 72, 73, 74, 75, 76, 77, 78,$
            79, 80, 81, 82, 83, 84, 85, 86, 87, 88 ]
c[90:106] = [ 89, 90, 91, 92, 93, 94, 95, 96, 97, 98,$
              99,100,101,102,103,104,105]

m[0:89] = [  1.00,  2.00,  3.00,  4.00,  6.94,  9.01, 10.81, 12.01, 14.01, 16.00,$
             19.00, 20.18, 22.99, 24.31, 26.98, 28.09, 30.97, 32.06, 35.45, 39.95,$
             39.10, 40.08, 44.96, 47.90, 50.94, 52.00, 54.94, 55.84, 58.93, 58.71,$
             63.54, 65.38, 69.72, 72.59, 74.92, 78.96, 79.91, 83.80, 85.47, 87.62,$
             88.91, 91.22, 92.91, 95.94, 98.00,101.07,102.91,106.40,107.87,112.40,$
             114.82,118.69,121.75,127.60,126.90,131.30,132.91,137.34,138.91,140.12,$
             140.91,144.24,147.00,150.35,151.96,157.25,158.93,162.50,164.93,167.26,$
             168.93,173.04,174.97,178.49,180.95,183.85,186.20,190.20,192.20,195.10,$
             196.97,200.59,204.37,207.20,208.98,210.00,210.00,222.00,223.00,226.00 ]

m[90:106] = [ 227.00,232.04,231.00,238.03,237.00,239.00,243.00,247.00,249.00,252.00,$
              254.00,257.00,258.00,255.00,257.00,260.00,260.00]

 if (keyword_set(charge)) then begin
    ind = intarr(n_elements(cha))

   ;takes the first isotope in the list

    for i=0,n_elements(cha)-1 do ind[i] = (where(cha[i] eq c))(0)
 endif $
 else if (keyword_set(symbol)) then begin
    ind = intarr(n_elements(sym))
    for i=0,n_elements(sym)-1 do $
       ind[i] = where(strlowcase(strtrim(sym[i],2)) eq $
                      strlowcase(strtrim(s,2)))
 endif $
 else if (keyword_set(mass)) then begin
    ind = 0 > nint(interpol(indgen(n_elements(m)),m,mas)) < (n_elements(m)-1)
 endif else ind=-1

 tmp = where(ind ne -1,count)

 if (count eq n_elements(ind)) then begin
    cha = float(c[ind])
    sym = strtrim(s[ind], 2)
    mas = m[ind]
    density = d[ind]
    number_density =  density/mas/amu
 endif $
 else message,'some elements were not found'

return
end
