;----------------------------------------------------------------------
;+
; PROJECT    :  ADAS
;
; NAME       :  xxorbocc
;
; PURPOSE    :  given an orbital (e.g. s,p,d,...)
;               returns the maximum occupation number
;
; INPUT      :  orb      strarr  orbitals
;
; OUTPUT     :  maxocc   intarr  maximum occupation number
;
;    
; EXANPLE    : Examples of how to run:
;                IDL> orb='s'
;                IDL> print, xxorbxocc(orb)
;                IDL> 2
;               
; AUTHOR     :  Alessandra Giunta
; 
; DATE       :  12-06-12
; 
;
; MODIFIED   :
;       1.1     Alessandra Giunta
;               - First version.
;
; VERSION    :   
;       1.1    12-06-2012
;-
;----------------------------------------------------------------------
function xxorbocc, orb

;list of standard orbitals and their maximum occupation numbers

lorb=['s', 'p', 'd', 'f', 'g', 'h', 'i', $
      'k', 'l', 'm', 'n', 'o', 'q', 'r', $
      't', 'u']

nlorb   = n_elements(lorb)
noccmax = 2*(2*findgen(nlorb)+1)

niorb  = n_elements(orb)
maxocc = intarr(niorb) -1

for i = 0, niorb-1 do begin
  
  io = where(strtrim(strlowcase(orb[i]),2) eq lorb, count)
  if count EQ 1 then maxocc[i]=noccmax[io] $
                else message, 'Incorrect orbital specification : ' + orb[i]

endfor

return, maxocc

end
