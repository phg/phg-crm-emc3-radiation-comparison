;----------------------------------------------------------------------
;+
; PROJECT    :  ADAS
;
; NAME       :  xxlvals
;
; PURPOSE    :  Returns orbital angular momentum specifications, i.e.
;               s, p, d, f...
;
; EXPLANATION:
;       This function contains hard coded orbital angular momentum
;       specifications as given by chapter V of Condon and Shortley
;
; USE:
;       An example;
;               result = xxlvals()
;               print,result
;               s p d f g h i k l m n o q r t u
;
; INPUTS:
;       None.
;
; OPTIONAL INPUTS:
;       cut: Either an l-value or a specification to cut-off at.
;                For instance:
;                     result = xxlvals(cut=3)
;                     print,result
;                     s p d f
;                or:
;                     result = xxlvals(cut='g')
;                     print,result
;                     s p d f g
;            Note that if the routine can't determine a cut-off
;            (e.g. cut='a') then this argument is ignored without
;            warning or error.
;
; KEYWORD PARAMETERS:
;       help : Displays documentation
;       lower : Return letters in lower case (default)
;       upper : Return letters in upper case
;       usej  : Include the use of 'j' as a specification, this is
;               non-standard.
;
; OUTPUTS:
;       A string array of orbital angular momentum letter specifications.
;
; CALLS:
;       None
;
; SIDE EFFECTS:
;       None
;
; NOTES:
;       There has been some confusion as to whether the letter 'j' should
;       be used as an angular momentum specifier since it conflicts with
;       commonly used variable to denote angular momentum. This routine
;       defaults to NOT using it (in keeping with the definition given
;       by Condon and Shortley) but allows the caller to specify that it
;       should be used to allow for correct interpretation of datasets
;       which follow the different convention. There is no such
;       confusion with the use of the letter 'l'.
;
; AUTHOR     :  Allan Whiteford
; 
; DATE       :  18-06-09
; 
;
; MODIFIED:
;       1.1     Allan Whiteford
;               - First version.
;
; VERSION:
;       1.1    18-06-09
;-
;----------------------------------------------------------------------

function xxlvals,lower=lower,upper=upper,cut=cut,usej=usej,help=help
        if keyword_set(help) then begin
                doc_library,'xxlvals'
                return,0
        endif
        lvals=['s','p','d','f','g','h','i','k','l','m','n','o','q','r','t','u']
        if keyword_set(usej) then lvals=[lvals[0:6],'j',lvals[7:14]]
        if n_elements(cut) eq 1 then begin
                if size(cut,/type) eq 2 or size(cut,/type) eq 3 then begin
                        if cut lt n_elements(lvals) and cut ge 0 then $
                                lvals=lvals[0:cut]
                endif
                if size(cut,/type) eq 7 then begin
                        idx=where(lvals eq strlowcase(cut))
                        if idx[0] ne -1 then lvals=lvals[0:idx]
                endif
        endif
        if keyword_set(upper) then lvals=strupcase(lvals)
        return,lvals
end
