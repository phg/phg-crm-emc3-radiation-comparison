; Copyright (c) 2002, Strathclyde University.
; SCCS info: Module @(#)$Header: /home/adascvs/idl/adaslib/atomic/xxseqid.pro,v 1.2 2012/07/23 10:05:33 mog Exp $ Date $Date: 2012/07/23 10:05:33 $
;+
; PROJECT:
;       ADAS
;
; NAME: XXSEQID()
;
; PURPOSE:
;       Returns sequence ID from nuclear charge and charge on ion
;
; EXPLANATION:
;
; USE:
;       General.
;
; INPUTS:
;       izo	-	Integer - nuclear charge
;       iz	-	Integer - charge of ion
;
;
; OPTIONAL INPUTS:
;       None.
;
; OUTPUTS:
;       seqid	-	String - iso-electronic sequence id
; OPTIONAL OUTPUTS:
;       None
;
; KEYWORD PARAMETERS:
;       None
;
; CALLS:
;       None
;
; SIDE EFFECTS:
;       None
;
; CATEGORY:
;       System routine.
;
; WRITTEN:
;       Stuart Loch
;
; MODIFIED:
;	1.1	Stuart Loch
;		First release 
;
; VERSION:
;       1.1	17/09/02
;
;-----------------------------------------------------------------------------

FUNCTION xxseqid, iz0,iz

    ON_ERROR, 2

    symbol = [' h','he','li','be',' b',' c',' n',' o',' f','ne','na','mg']
    symbol = [symbol,'al','si',' p',' s','cl','ar',' k','ca','sc','ti',' v','cr']
    symbol = [symbol,'mn','fe','co','ni','cu','zn','ga','ge','as','se','br']
    symbol = [symbol,'kr','rb','sr',' y','zr','nb','mo','tc','ru','rh','pd']
    symbol = [symbol,'ag','cd','in','sn','sb','te',' i','xe','cs','ba','la']
    symbol = [symbol,'ce','pr','nd','pm','sm','eu','gd','tb','dy','ho','er']
    symbol = [symbol,'tm','yb','lu','hf','ta',' w','re','os','ir','pt','au']
    symbol = [symbol,'hg','tl','pb','bi','po','at','rn','fr','ra','ac','th','pa',' u']
       
    seqid=symbol[iz0-iz-1]    
    return, seqid

END
