; Copyright (c) 1995, Strathclyde University
;+
; PROJECT:
;       ADAS
;
; NAME:
;       xxeiam.pro
;
; PURPOSE:
;       Routine to return the atomic mass number for a given element
;       symbol esym.
;
; USE:
;       General
;
; INPUTS:
;       ESYM    -       The symbol of the element required (note that
;                       case is unimportant as the uppercase of the
;                       input value is always taken).
; OPTIONAL INPUTS:
;       None
;
; OUTPUTS:
;       XSYM    -       The float value of the atomic mass of the element.
;                       If the symbol is not recognised then the value
;                       of xsym returned is zero.
;
; OPTIONAL OUTPUTS:
;       None
;
; KEYWORD PARAMETERS:
;       None
;
; CALLS:
;       None
;
; SIDE EFFECTS:
;       None
;
; CATEGORY:
;       Adas system.
;
; WRITTEN:
;       Tim Hammond, Tessella Support Services plc, 5th October 1995
;
; MODIFIED:
;       1.1     Tim Hammond
;               First release
;       1.2     Richard Martin
;               Added Dueterium and Tritium
;       1.3     Hugh Summers
;               Extended element number to 99
;       1.4     Martin O'Mullane
;               Vectorize.
; VERSION:
;       1.1     05/10/95
;       1.2     12-10-99
;       1.3     15/10/99
;       1.4     23-03-2011
;
;-----------------------------------------------------------------------------
;-

PRO xxeiam, esym, xsym

    ON_ERROR, 2

    symbol = ['H','D','T','HE','LI','BE','B','C','N','O','F','NE','NA','MG']
    symbol = [symbol,'AL','SI','P','S','CL','AR','K','CA','SC','TI','V','CR']
    symbol = [symbol,'MN','FE','CO','NI','CU','ZN','GA','GE','AS','SE','BR']
    symbol = [symbol,'KR','RB','SR','Y','ZR','NB','MO','TC','RU','RH','PD']
    symbol = [symbol,'AG','CD','IN','SN','SB','TE','I','XE','CS','BA','LA']
    symbol = [symbol,'CE','PR','ND','PM','SM','EU','GD','TB','DY','HO','ER']
    symbol = [symbol,'TM','YB','LU','HF','TA','W','RE','OS','IR','PT','AU']
    symbol = [symbol,'HG','TL','PB','BI','PO','AT','RN','FR','RA','AC','TH']
    symbol = [symbol,'PA','U']

    masses = ['1.00794','2.00','3.00','4.00260','6.941','9.01218','10.81','12.011','14.0067','15.9994','18.998403','20.179','22.98977','24.305']
    masses = [masses,'26.98154','28.0855','30.97376','32.06','35.453','39.948','39.0983','40.08','44.9559','47.88','50.9415','51.996']
    masses = [masses,'54.9380','55.847','58.9332','58.69','63.546','65.38','69.72','72.59','74.9216','78.96','79.904']
    masses = [masses,'83.80','85.4678','87.62','88.9059','91.22','92.9064','95.94','98.0','101.07','102.9055','106.42']
    masses = [masses,'107.8682','112.41','114.82','118.69','121.75','127.60','126.9045','131.30','132.9054','137.33','138.9055']
    masses = [masses,'140.12','140.9077','144.24','145.0','150.4','151.96','157.25','158.9254','162.50','164.9304','167.26']
    masses = [masses,'168.9342','173.04','174.97','178.49','180.9479','183.85','186.2','190.2','192.22','195.09','196.9665']
    masses = [masses,'200.59','204.37','207.2','208.9808','210.0','210.0','222.0','223.0','226.0254','227.0','232.0381']
    masses = [masses,'231.0359','238.029']

    n_esym = n_elements(esym)
    xsym   = fltarr(n_esym)

    for  j = 0, n_esym-1 do begin

       symtest = strcompress(strupcase(esym[j]), /remove_all)
       symmatch = where(symbol eq symtest)
       if symmatch[0] gt -1 then xsym[j] = masses[symmatch[0]] else xsym[j] = 0.0

    endfor

    if n_esym EQ 1 then xsym = xsym[0]

END
