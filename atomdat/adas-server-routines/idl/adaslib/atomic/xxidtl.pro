;----------------------------------------------------------------------
;+
; PROJECT:
;       ADAS
;
; NAME:
;       xxidtl
;
; PURPOSE:
;       IDL translation of FORTRAN routine XXIDTL.FOR which
;       is the inverse function of I4IDFL.
;
; USE:
;       General
;
; INPUTS:
;       INDEX   - unique index
;
; OUTPUTS:
;       N,L     - quantum numbers.
;
;
; KEYWORD PARAMETERS:
;       idl_index - if set assumes the index starts at 0, not 1 as for
;                   fortran, as the first element in the array.
;
; CALLS:
;       None
;
; SIDE EFFECTS:
;       None
;
; CATEGORY:
;       Adas system.
;
; WRITTEN:
;       Martin O'Mullane
;
; MODIFIED:
;       1.1     Martin O'Mullane
;                - First release
;
; VERSION:
;       1.1     11-08-2011
;
;-----------------------------------------------------------------------------

PRO xxidtl, index, n, l, idl_index=idl_index

N = 1
L = 0

if keyword_set(idl_index)  then index = index + 1

while i4idfl(N+1,L) LE index do N = N + 1
while i4idfl(N,L+1) LE index do L = L + 1

END
