;----------------------------------------------------------------------
;+
; PROJECT    :  ADAS
;
; NAME       :  r8giiiav
;
; PURPOSE    :  Returns Maxwell averaged free-free Gaunt factor.
;
; USE:
;       An example;
;               u = h * c / ( lambda * 1.e-10 * te * e )
;               gam2 = z^2*ih/te
;               result = r8giiiav(u, gam2)
;
; INPUTS:
;       u   :  h * c / ( lambda * 1.e-10 * te * e )
;       gam2:  z^2*ih/te
;               - h the Plank constant (J s), c the speed of light (m/s), 
;                 lambda wavelength in A, z charge of ion, Te electron
;                 temperature (eV) and IH the Rydberg energy (13.6eV).
;
; OPTIONAL INPUTS:
;       None.
;
; OUTPUTS:
;       This function returns the evaluated 3j symbol.
;
; OPTIONAL OUTPUTS:
;       None.
;
; KEYWORD PARAMETERS:
;       None.
;
; CALLS:
;       Fortran/C code via a shared object.
;
; SIDE EFFECTS:
;

; MODIFIED:
;       1.1     Martin O'Mullane
;               - First version.
;       1.2     Martin O'Mullane
;               - Vectorize in u and gam2.
;
; VERSION:
;       1.1    19-06-2007
;       1.2    11-12-2015
;-
;----------------------------------------------------------------------

function r8giiiav, u, gam2

fortdir = getenv('ADASFORT')

n_u    = n_elements(u)
n_gam2 = n_elements(gam2)

result  = dblarr(n_u, n_gam2)

for iu = 0L, n_u-1 do begin
   for ig = 0L, n_gam2-1 do begin

      u_in    = double(u[iu])
      gam2_in = double(gam2[ig])
      res     = 0.0D0 
      dummy   = CALL_EXTERNAL(fortdir+'/r8giiiav_if.so','r8giiiav_if',    $
                              u_in, gam2_in, res)

      result[iu, ig] = res
   
   endfor
endfor

result = reform(result)
if n_elements(result) EQ 1 then result = result[0]
   
return, result

END
