;----------------------------------------------------------------------
;+
; PROJECT:
;       ADAS
;
; NAME:
;       i4idfm
;
; PURPOSE:
;       IDL translation of FORTRAN routine i4idfm.FOR which
;       returns a unique index number based on the value of the
;       n, l amd m quantum numbers passed to it. The index is used to
;       reference arrays containing data dependent on the n, l and m
;       quantum numbers.
;
; USE:
;       General
;
; INPUTS:
;       N,L,M    -       quantum numbers.
;
; OPTIONAL INPUTS:
;       None
;
; OUTPUTS:
;       i4idfm    -     Index.
;
; OPTIONAL OUTPUTS:
;       None
;
; KEYWORD PARAMETERS:
;       idl_index - if set returns index assuming 0, not 1 as for 
;                   fortran, as the first element in the array.
;
; CALLS:
;       None
;
; SIDE EFFECTS:
;       None
;
; CATEGORY:
;       Adas system.
;
; WRITTEN:
;       Martin O'Mullane
;
; MODIFIED:
;       1.1     Martin O'Mullane
;                - First release
;
; VERSION:
;       1.1     14-03-2007
;
;-----------------------------------------------------------------------------

FUNCTION i4idfm, n, l, m, idl_index=idl_index

index = (((N - 1) * N * (N + 1)) / 6) + ((L * (L + 1)) / 2) + M + 1

if keyword_set(idl_index)  then index = index - 1

return, index

END
