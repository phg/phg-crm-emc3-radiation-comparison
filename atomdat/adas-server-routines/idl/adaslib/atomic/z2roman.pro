; Copyright (c) 1995, Strathclyde University.
; Alessandro Lanzafame  acl@phys.strath.ac.uk   14 March 1996
;+
; PROJECT:
;       ADAS support programs
;
; NAME:
;       z2roman
;
; PURPOSE:
;       Given the ion charge, returns the spectroscopic symbol for
;       the ion charge (roman numeral). For ion charge 0 to 91.
;
;
; EXPLANATION:
;       It builds up the roman number sequence and compares it with the
;       input. When these two strings are equal set the value to the
;       counter. The roman value is set to '' of the input is wrong or
;       is out of range (greater than 92). Put a message on screen in
;       this case.
;
; USE:
;       Example:
;
;           ion = z2roman([9,21])
;
;       returns ion=['X', 'XXII'].
;
; INPUTS:
;       IZS - Integer; ion charge
;
; OPTIONAL INPUTS:
;       None
;
; OUTPUTS:
;       This function returns a string which translates the ion charge
;       into its corresponding spectroscopic symbol as roman numeral.
;
; OPTIONAL OUTPUTS:
;       None
;
; KEYWORD PARAMETERS:
;       None
;
; CALLS:
;       None
;
; SIDE EFFECTS:
;       None
;
; CATEGORY:
;       Adas support and applications.
;
; WRITTEN:
;       Alessandro Lanzafame, University of Strathclyde, mar14-95
;
; MODIFIED:
;       1.0     Alessandro Lanzafame
;               First version
;       1.1     Tim Hammond
;               Put under S.C.C.S. control
;       1.2     Martin O'Mullane
;                - Vectorize for compatibility with roman2z.
;                - Correct the explanation.
;
; VERSION:
;       1.0     14-03-1995
;       1.1     03-05-1996
;       1.2     11-09-2017
;
;----------------------------------------------------------------------
;-

FUNCTION z2roman,izs

nval = n_elements(izs)
rnum = strarr(nval)


SPECU = ['I','II','III','IV','V','VI','VII','VIII','IX']
SPECT = ['X','XX','XXX','XL','L','LX','LXX','LXXX','XC']
ISPEC = [1,2,3,2,1,2,3,4,2]

for j = 0, nval-1 do begin

    iz = izs[j] + 1

    if iz LT 1 OR iz GT 92 then begin
       message, 'Charge out of range. Roman value set to blank '+string(iz-1), /continue
       rnum[j] = ''
    endif else begin
    
       if iz lt 10 then begin
           rnum[j] = specu[iz-1]
       endif else begin
           snum = strtrim(string(iz),2)
           it   = long(strmid(snum,0,1))
           iu   = long(strmid(snum,1,1))
           if iu gt 0 then begin
               rnum[j] = spect[it-1]+specu[iu-1]
           endif else begin
               rnum[j] = spect[it-1]
           endelse
       endelse
    
    endelse
    
endfor

if nval EQ 1 then return, rnum[0] else return, rnum

END
