;----------------------------------------------------------------------
;+
; PROJECT    :  ADAS
;
; NAME       :  occ2cfg
;
; PURPOSE    :  Convert  an occupation number array to a
;               configuration string.
;
; EXPLANATION:
;       This function takes a textual configuration, nuclear charge
;       and ion charge to return occupation numbers of electrons
;
; USE:
;       An example;
;               result = cfg2occ([1,2,0,1])
;               print,result
;               1s1 2s2 3p1
;
; INPUTS:
;       OCC: Occupation vector in convention form (1s, 2s,2p, 3s, 3p, 3d etc.)
;
; OPTIONAL INPUTS:
;       None.
;
; OUTPUTS:
;       A string of the configuration.
;
; OPTIONAL OUTPUTS:
;       None.
;
; KEYWORD PARAMETERS:
;       help    : Displays documentation
;       eissner : Output configuration is Eissner.
;       display : Do not replace 10 by A etc for no. electrons.
;       lower   : Return configuration in lower case for standard (default)
;       upper   : Return configuration in upper case for standard
;
; CALLS:
;       xxlvals for mapping 's','p',... to l values
;       xxorbs for sequence of '1s','2s','2p',...
;       xxcftr to give Eissner configuration
;
; SIDE EFFECTS:
;       None
;
; NOTES:
;       occ2cow in the series 8 library should be used if configurations
;       usable for input to Cowan code (adas801) are required.
;
; AUTHOR     :  Martin O'Mullane
;
; DATE       :  26-08-2009
;
;
; MODIFIED:
;       1.1     Martin O'Mullane
;               - First version.
;
; VERSION:
;       1.1    26-08-2009
;-
;----------------------------------------------------------------------

function occ2cfg, occ,               $
                  display = display, $
                  eissner = eissner, $
                  lower   = lower,   $
                  upper   = upper,   $
                  help    = help

if keyword_set(help) then begin
   doc_library, 'occ2cfg'
   return,0
endif


; Get list of orbitals and restrict input 'occ' to this set of orbitals

orbs   = xxorbs()
n_orbs = n_elements(orbs)
n_occ  = n_elements(occ)

if n_occ GT n_orbs then begin
   if total(occ[n_orbs+1:*]) GT 0 then message, 'Occupied shells above maximum' $
                                  else occup = occ[0:n_orbs-1]
endif else occup = occ

n_occ  = n_elements(occup)


; Construct configuration string - 10, 11, 12 -> A, B, C etc.

if keyword_set(display) then begin

   e_str = strcompress(string(occup), /remove_all)

endif else begin

   ; Fill vec with position of numbers and captial letters (pos 49=1, pos 65=A)

   vec = byte(occup) + 48B
   ind = where(occup GE 10, count)
   if count GT 0 then vec[ind] = vec[ind] + 7B

   ; Because string(byte array) is one string rather than an array of strings

   e_str = strarr(n_occ)
   for j = 0, n_occ-1 do e_str[j] = strcompress(string(vec[j]), /remove_all)

endelse


; Remove un-occupied shells

ind = where(occup NE 0, count)

if count EQ 0 then begin

   cfg = ''

endif else begin

   o_str = orbs[ind]
   e_str = e_str[ind]
   cfg   = o_str + e_str + ' '

endelse

cfg = strtrim(strjoin(cfg, / single), 2)


; React to options

if keyword_set(eissner) then begin

   xxcftr, in_cfg=cfg, out_cfg=str, type=2
   cfg = str

endif else begin

   if keyword_set(lower) then cfg = strlowcase(cfg)
   if keyword_set(upper) then cfg = strupcase(cfg)

endelse

return, cfg

end
