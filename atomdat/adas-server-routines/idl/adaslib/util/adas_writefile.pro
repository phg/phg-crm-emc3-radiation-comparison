;----------------------------------------------------------------------
;+
; PROJECT    :  ADAS
;
; NAME       :  adas_writefile
;
; PURPOSE    :  Write (or append) a string array to a named file.
;
;
; ARGUMENTS  :  All output arguments will be defined appropriately.
;
;               NAME      I/O    TYPE    DETAILS
; REQUIRED   :  file       I      str    name of file to be written.
;               all        I      str()  contents to be written.     .
;               comments   I      str()  write these comments at end.
;
; KEYWORDS      append             -     Add contents of all to existing file.
;               trim               -     trim-off trailing blanks for each line
;                                        applies to body and comments.
;               help               -     if specified this comment
;                                        section is written to screen.
;
;
; NOTES      :
;
;
; AUTHOR     :  Martin O'Mullane
;
; DATE       :  20-02-2009
;
;
; MODIFIED:
;       1.1     Martin O'Mullane
;               - First version.
;       1.2     Martin O'Mullane
;               - Change counter variable to LONG in the writing loop.
;       1.3     Martin O'Mullane
;               - Add keyword /trim to remove trailing blanks.
;
; VERSION:
;       1.1    20-02-2009
;       1.2    29-11-2011
;       1.3    09-09-2012
;-
;----------------------------------------------------------------------



PRO adas_writefile, file     = file,     $
                    all      = all,      $
                    comments = comments, $
                    append   = append,   $
                    trim     = trim,     $
                    help     = help


; If asked for help

if keyword_set(help) then begin
   doc_library, 'adas_writefile'
   return
endif



if n_elements(all) LT 1 then message, 'need at least one line'

if keyword_set(trim) then all = strtrim(all,0)

if keyword_set(append) then begin

   res = file_info(file)
   if res.exists EQ 0 then nlines = 0 $
                      else adas_readfile, file=file, all=all_exist, nlines=nlines

   openw, lun, file, /get_lun
   for j = 0, nlines-1 do printf, lun, all_exist[j]
   for j = 0, n_elements(all)-1 do printf, lun, all[j]

endif else begin

   openw, lun, file, /get_lun
   for j = 0L, n_elements(all)-1 do printf, lun, all[j]

endelse


; Comments

n_com = n_elements(comments)

if n_com GT 0 then begin
   if keyword_set(trim) then comments = strtrim(comments, 0)
   for j = 0, n_com-1 do printf, lun, comments[j]
endif

free_lun, lun


END
