;
; PROJECT:
;       ADAS
;
; NAME:
;	ADAS_IS_FLOAT
;
; PURPOSE:
;	Check a string is a valid floating point number.
;
; EXPLANATION:
;	Does soem string scanning to check the input is a valid floating
;	point number.
;	
; USE:
;	Called with a string, returns 1 if it is valid, 0 if not.
;
; INPUTS:
;	string - string to check
;
; OPTIONAL INPUTS:
;	None
;
; OUTPUTS:
;	Returns 1 if the string is a valid floating point number, 0 otherwise.
;
; OPTIONAL OUTPUTS:
;	None
;
; KEYWORD PARAMETERS:
;	None
;
; CALLS:
;	None
;
; CALLED BY:
;	ADAS605
;
; SIDE EFFECTS:
;	None
;
; INTERNAL VARIABLES:
;
;
; WRITTEN:
;       Allan D Whiteford, University of Strathclyde
;
; VERSION:
;       0.9B   	Allan D Whiteford    
;              	First release.
;		07/09/00
;		
;-----------------------------------------------------------------------------

function adas_is_float,string

	if strlen(string) eq 0 then return,0
	foundpoint=0
	foundminus=0
	founde=0
	foundplus=0
		
	for i=0,strlen(string)-1 do begin
		char=strmid(string,i,1)
		if ((char lt '0' or char gt '9') and char ne '.' and char ne '-' and char ne '+' and char ne 'e' and char ne 'E') then return,0
		if (char eq '.' and foundpoint eq 1) then return,0
		if (char eq '-' and foundminus eq 2) then return,0
		if (char eq 'e' and founde eq 1) then return,0
		if (char eq 'E' and founde eq 1) then return,0
		if (char eq '+' and foundplus eq 1) then return,0
		if char eq '.' then foundpoint=1
		if char eq '-' then foundminus=foundminus+1
		if char eq '+' then foundplus=1
		if char eq 'E' or char eq 'e' then founde=1
	end
		
	return,1
end
