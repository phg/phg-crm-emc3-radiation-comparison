;-----------------------------------------------------------------------------
;+
; PROJECT:
;       ADAS 
;
; NAME:
;       adas_string_pad
;
; PURPOSE:
;	Takes a string or array of strings and pads them out with trailing
;	spaces to a given length.
;
; INPUTS:
;        string_matrix	- String or string array to be padded
;        length		- Length to make padded string
;
; OPTIONAL INPUTS:
;       None.
;
; OUTPUTS:
;       string_matrix - Modified input
;
; OPTIONAL OUTPUTS:
;       None
;
; KEYWORD PARAMETERS:
;	None       
;
; CALLS:
;       None
;       
; SIDE EFFECTS:
;       None
;
; CATEGORY:
;       Utility
;
; WRITTEN:
;       Allan Whiteford
;
; MODIFIED:
;       1.1     Allan Whiteford
;               First release, all code from cxf_string_pad by Lorne Horton
;
; VERSION:
;       1.1     11-10-2006
;
;-
;-----------------------------------------------------------------------------

PRO adas_string_pad, string_matrix, length

; Pad/truncate a matrix of strings to a given length

  IF SIZE(string_matrix,/TYPE) NE 7 THEN BEGIN
    print, 'Input matrix is not of string type, returning'
    RETURN
  ENDIF

  nelements   = SIZE(string_matrix,/N_ELEMENTS)
  ndimensions = SIZE(string_matrix,/N_DIMENSIONS)
  dimensions  = SIZE(string_matrix,/DIMENSIONS)

  IF ndimensions GE 1 THEN $
    string_matrix = REFORM(string_matrix,nelements)
  FOR i = 0L, nelements-1 DO BEGIN
    str_len = STRLEN(string_matrix[i])
    IF str_len GT length THEN BEGIN
      string_matrix[i] = STRMID(string_matrix[i],0,length)
    ENDIF ELSE IF STRLEN(string_matrix[i]) LT length THEN BEGIN
      blanks = STRING(' ', $
                FORMAT='(a'+STRTRIM(STRING(length-str_len),2)+')')
      string_matrix[i] = string_matrix[i]+blanks
    ENDIF
  ENDFOR

  IF ndimensions GE 1 THEN $
    string_matrix = REFORM(string_matrix,dimensions)

END
