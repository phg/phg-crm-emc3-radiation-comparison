;----------------------------------------------------------------------
;+
; PROJECT    :  ADAS
;
; NAME       :  cast_value
;
; PURPOSE    :  Checks that input is numeric and changes it to required
;               type of byte, long, integer, float or double. The
;               function stops with a message if this is not possible.
;
;               NAME      I/O    TYPE   DETAILS
; INPUTS     :  value      I            Input scalar or array.
;               type       I     str    byte, long, integer, float or double
;               mess       I     str    prepend to error message  
;
;
; OUTPUTS    :  value      O            cast to input requested type
;                                       
;
; NOTES      :  Does not work with string input.
;
; AUTHOR     :  Martin O'Mullane
; 
; DATE       :  18-09-2005
; 
; UPDATE     :  
;
; MODIFIED:
;       1.1     Martin O'Mullane
;                - First version.
;
; VERSION:
;       1.1     18-09-2005
;-
;----------------------------------------------------------------------


FUNCTION cast_value, value, type=type, mess=mess

if n_elements(mess) EQ 0 then mess_out = 'Value' else mess_out = mess
if n_elements(type) EQ 0 then type_in  = 'X'     else type_in  = type


partype = size(value, /type)

if (partype LT 1) OR (partype GT 5) then  message, mess_out + ' must be numeric'

typ = strupcase(strcompress(type_in, /remove_all))
typ = strmid(typ, 0, 1)

case typ of 

   'B' : value = byte(value)
   'I' : value = fix(value)
   'L' : value = long(value)
   'F' : value = float(value) 
   'D' : value = double(value)
   else : message, mess_out + ' type unchanged', /continue

endcase

return, value

END
