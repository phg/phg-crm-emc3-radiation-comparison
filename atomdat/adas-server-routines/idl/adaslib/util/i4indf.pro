;----------------------------------------------------------------------
;+
; PROJECT:
;       ADAS
;
; NAME:
;       i4indf
;
; PURPOSE:
;       IDL translation of FORTRAN routine I4INDF.FOR which finds
;       the index of the array closest to the requested value
;
; USE:
;       General
;
; INPUTS:
;       ARRAY    -       Input array.
;       VALUE    -       Required value.
;
; OPTIONAL INPUTS:
;       None
;
; OUTPUTS:
;       IEINDF    -     Index which is -1 if outside the range.
;
; OPTIONAL OUTPUTS:
;       None
;
; KEYWORD PARAMETERS:
;       None
;
; CALLS:
;       None
;
; SIDE EFFECTS:
;       None
;
; CATEGORY:
;       Adas system.
;
; WRITTEN:
;       Tim Hammond, Tessella Support Services plc, 24/07/95
;
; MODIFIED:
;       1.1     Martin O'Mullane
;                 - First release
;       1.2     Martin O'Mullane
;                 - Vectorize by allowing the search value to be an array.
;
; VERSION:
;       1.1     08-03-2002
;       1.2     24-02-2011
;
;-----------------------------------------------------------------------------

FUNCTION i4indf, array, value

; Check odd cases


if n_elements(array) LE 0 OR n_elements(value) LE 0 then index = -1L
if n_elements(array) EQ 1 then index=0L

if n_elements(index) EQ 0 then begin
   num   = n_elements(value)
   index = lonarr(num)
   for j = 0, num-1 do begin
      abdiff   = abs(array - value[j])       ; absolute difference
      mindiff  = min(abdiff, i1)             ; find smallest difference
      index[j] = i1
   endfor
   if num EQ 1 then index = index[0]
endif


return, index
       
END

