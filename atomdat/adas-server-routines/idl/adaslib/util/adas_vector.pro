;----------------------------------------------------------------------
;+
; PROJECT:
;       ADAS
;
; NAME:
;       ADAS_VECTOR
;
; PURPOSE:
;       Generate a vector of values given the minimum, maximum
;       and number of points.
;
; NOTES:
;       The default is to generate a log based distribution.
;
;
; INPUTS:
;       LOW    -  minimum value.
;       HIGH   -  maximum value.
;       NUM    -  number of points.
;
; OPTIONAL INPUTS:
;       None
;
; OUTPUTS:
;
; OPTIONAL OUTPUTS:
;       None.
;
; KEYWORD PARAMETERS:
;       LINEAR - if selected the distribution is linear.
;
; CALLS:
;       None
;
; SIDE EFFECTS:
;       None
;
; CATEGORY:
;       UNIX system IDL utility.
;
; WRITTEN:
;       Martin O'Mullane, 25-11-2004
;
;
; MODIFIED:
;       1.1     Martin O'Mullane
;               - First release.
;       1.2     Martin O'Mullane
;               - Work internally in double and long integer.
;
; VERSION:
;       1.1     25-11-2004
;       1.2     10-01-2014
;
;-
;----------------------------------------------------------------------

FUNCTION adas_vector, low=low, high=high, num=num, linear=linear

if n_elements(num)  EQ 0 then n_value = 10    else n_value = long(num)
if n_elements(low)  EQ 0 then l_value = 1.0   else l_value = double(low)
if n_elements(high) EQ 0 then h_value = 100.0 else h_value = double(high)

if n_value EQ 1L then return, -1L


if keyword_set(linear) then begin

   value = l_value + lindgen(n_value) * (h_value-l_value) / (n_value-1L)

endif else begin

   value = alog(l_value) + dindgen(n_value) / double(n_value-1) * (alog(h_value)-alog(l_value))
   value = exp(value)

endelse

return, value

END
