; Copyright (c) 1995, Strathclyde University .
; SCCS info: Module @(#)$Header: /home/adascvs/idl/adaslib/util/dynlabel.pro,v 1.1 2004/07/06 13:34:14 whitefor Exp $ Date $Date: 2004/07/06 13:34:14 $
;
;+
; PROJECT:
;       ADAS IBM MVS to   UNIX conversion
;
; NAME:
;	DYNLABEL
;
; PURPOSE:
;	To make v4.0.1 and later of IDL resize label widgets in the same way
;	as versions 3.x and lower. v4.0 is an anomoly - it doesn't naturally
;	allow resizing and doesn't have the DYNAMIC_RESIZE keyword
;
; EXPLANATION:
;	The DYNLABEL procedure, sets the DYNAMIC_RESIZE
;	keyword for any labels contained in the specified widget tree
;	when used with IDL version 4 or later. (For older IDL versions,
;	it simply returns without changing anything, so it can be used
;	with any version of IDL.)
; USE:
;	To use DYNLABEL, do the following:
;
;	1) Make sure you place the DYNLABEL procedure
;	   (and its companion routine, DYNLABEL_CALL) before any routines
;	   that call it in your .pro file.
;
;	2) Call DYNLABEL on the line just above where you realize your
;	   widget program, passing it the top level widget ID for the
;	   widget hierarchy. For example:
;
;	   base = WIDGET_BASE(/COLUMN)
;	   ...
;	   widget code
;	   ...
;	   DYNLABEL, base
;	   WIDGET_CONTROL, base, /REALIZE
;
; INPUTS:
;	tlb	-	Long; base of the widget to recursively search through
;			and set all DYNAMIC_RESIZE keywords on children label
;			widgets
; OPTIONAL INPUTS:
;	None.
;
; OUTPUTS:
;	None.
;
; OPTIONAL OUTPUTS:
;       None
;
; KEYWORD PARAMETERS:
;	None
;
; CALLS:
;       See side effects for other related widget management routines.
;
; SIDE EFFECTS:
;	The routine dynlabel_call is called to recursively search for label
;	widgets and set their DYNAMIC_RESIZE property
;
; CATEGORY:
;	Auxiliary routine
;
; WRITTEN:
;       Research Systems, Inc. FTP: ftp.rsinc.com (192.5.156.17)
;	Directory: pub/rsi/idl/notes  File: widgets.txt
;
; MODIFIED:
;	1.1	William Osborn
;		Put under S.C.C.S.
;
; VERSION:
;	1.1	21-05-96
;
;-
;-----------------------------------------------------------------------------

    PRO dynlabel_call, w
      ; Called by DYNLABEL. Does recursive search for
      ; label widgets and sets their DYNAMIC_RESIZE property
      type = WIDGET_INFO(w, /TYPE)
      IF (type EQ 5) THEN BEGIN
        WIDGET_CONTROL, /DYNAMIC_RESIZE, w
      ENDIF ELSE IF (type EQ 0) THEN BEGIN
        child = WIDGET_INFO(W, /CHILD)
        WHILE (child NE 0) DO BEGIN
          DYNLABEL_CALL, CHILD
          CHILD = WIDGET_INFO(CHILD, /SIBLING)
        ENDWHILE
      ENDIF 
    END

    PRO dynlabel, tlb
      ; For IDL versions 4.0 and newer, find all label widgets
      ; rooted and the specified top level widget and set their
      ; DYNAMIC_RESIZE property.
      ; Actually had to be modified since only works with 4.0.1. WRO
      rel = BYTE(!version.release)
      reln = (rel(0) - (BYTE('0'))(0))
      IF (reln gt 4 or (reln eq 4 and n_elements(rel) gt 3)) THEN $
          dynlabel_call, tlb
    END
	
;  
;	We find this technique of using WIDGET_INFO to find widgets of a
;	certain type recursively to be very handy. You can use it to avoid
;	spreading awkward version-dependent code throughout your applications.
;
;
