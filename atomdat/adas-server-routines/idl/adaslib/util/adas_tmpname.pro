;----------------------------------------------------------------------
;+
; PROJECT    :  ADAS
;
; NAME       :  adas_tmpname
;
; PURPOSE    :  Returns the name of a temporary file.
;
;
; ARGUMENTS  :  All output arguments will be defined appropriately.
;
;               NAME      I/O    TYPE    DETAILS
; OPTIONAL   :  suffix     I      str    append suffix to name.
;
;
; NOTES      :  Uses the dry-run version of the unix mktemp command
;               which is slightly unsafe since it cannot guarantee that
;               another file with the returned name could exist when used.
;
;               Use suffix to ensure a know file extension, eg suffix='.py'
;               for a python script.
;
; AUTHOR     :  Martin O'Mullane
;
; DATE       :  05-11-2018
;
;
; MODIFIED:
;       1.1     Martin O'Mullane
;               - First version.
;
; VERSION:
;       1.1    05-11-2018
;-
;----------------------------------------------------------------------

FUNCTION adas_tmpname, suffix=suffix

cmd = 'mktemp -u'
spawn, cmd, res

name = res[0]
if n_elements(suffix) EQ 1 then name = name + suffix

return, name

END
