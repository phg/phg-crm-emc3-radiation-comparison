;-----------------------------------------------------------------------------
;+
; PROJECT:
;       ADAS
;
; NAME:
;       wheresubarray
;
; PURPOSE:
;       Finds the indices where a sub-array matches a larger one.
;
; EXPLANATION:
;
; INPUTS:
;       a,b - Two arrays of any type.
;
; OPTIONAL INPUTS:
;       count - number of elements in common.
;
; OUTPUTS:
;       An array.
;
; EXAMPLE:
;       Take a = [14, 15, 16, 17, 26, 27, 28]
;            b = [14, 26]
;       print,wheresubarray(a,b) gives [0,4]
;       print,wheresubarray(a,b,ct) gives [0,4] and ct is set to 2
;
; OPTIONAL OUTPUTS:
;       None
;
; KEYWORD PARAMETERS:
;       None
;
; CALLS:
;       None
;
; SIDE EFFECTS:
;       None - but the algorithm is slow.
;
; CATEGORY:
;       Utility
;
; WRITTEN:
;       Martin O'Mullane
;
; MODIFIED:
;       1.1     Martin O'Mullane
;               First release
;
; VERSION:
;       1.1     02-09-2010
;
;-
;-----------------------------------------------------------------------------

FUNCTION wheresubarray, a, b, count

count = 0L

n_a = n_elements(a)
n_b = n_elements(b)

r = -1
if n_a GT n_b then begin
   for j = 0, n_b-1 do begin
     for i = 0, n_a-1 do  if a[i] EQ b[j] then r = [r,i]
   endfor
endif else begin
   for j = 0, n_a-1 do begin
     for i = 0, n_b-1 do  if b[i] EQ a[j] then r = [r,i]
   endfor
endelse

n_r = n_elements(r)

if n_r GT 1 then begin
   count = n_r-1
   r = r[1:*]
endif else begin
   count = 0
endelse

return, r

END
