;----------------------------------------------------------------------
;+
; PROJECT:
;       ADAS
;
; NAME:
;       ADAS_UNIQ
;
; PURPOSE:
;       Return the unique elements in an array. Related to the
;       built-in uniq but the elements, rather than indices are
;       returned (ie return array[uniq(array, sort(array))]).
;
; NOTES:
;
; INPUTS:
;       array of any type - this is not altered within the function.
;
; OPTIONAL INPUTS:
;       None
;
; OUTPUTS:
;
; OPTIONAL OUTPUTS:
;       None.
;
; KEYWORD PARAMETERS:
;
; CALLS:
;       None
;
; SIDE EFFECTS:
;       None
;
; CATEGORY:
;       IDL utility.
;
; WRITTEN:
;       Martin O'Mullane, 19-10-2012
;
;
; MODIFIED:
;       1.1     Martin O'Mullane
;               - First release.
;       1.2     Martin O'Mullane
;               - Add a count keyword.
;
; VERSION:
;       1.1     19-10-2012
;
;-
;----------------------------------------------------------------------

FUNCTION adas_uniq, array, count=count

if n_elements(array) EQ 0 then message, 'An input array is required'

a   = array
res = a[uniq(a, sort(a))]

if arg_present(count) then count = n_elements(res)

return, res

END
