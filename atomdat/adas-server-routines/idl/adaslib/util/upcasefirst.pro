;-----------------------------------------------------------------------------
;+
; PROJECT:
;       ADAS
;
; NAME:
;       upcasefirst
;
; PURPOSE:
;       Capitalise the first letter in a string (or array of strings).
;
; EXPLANATION:
;
; INPUTS:
;       str - string
;
; OPTIONAL INPUTS:
;       None
;
; OUTPUTS:
;       Same as input with first character in capitals.
;
; EXAMPLE:
;       print, upcasefirst('cat') gives 'Cat'
;       print, upcasefirst(['dog', 'rabbit']) gives Dog Rabbit
;
; OPTIONAL OUTPUTS:
;       None
;
; KEYWORD PARAMETERS:
;       None
;
; CALLS:
;       None
;
; SIDE EFFECTS:
;       None
;
; CATEGORY:
;       Utility
;
; WRITTEN:
;       Martin O'Mullane
;
; MODIFIED:
;       1.1     Martin O'Mullane
;               First release
;
; VERSION:
;       1.1     02-09-2010
;
;-
;-----------------------------------------------------------------------------

FUNCTION upcasefirst, str

num = n_elements(str)
out = strarr(num)

for j = 0, num-1 do begin

   b   = byte(str[j])
   ind = where(((b GE 65B) AND (b LE 90B)) OR ((B GE 97B) AND (B LE 122B)), ct)

   if ct eq 0 then begin                   ; No letters
      out[j] = str
   endif else begin
      if ind[0] EQ 0 then begin            ; First character is a letter
         b[ind[0]] = b[ind[0]] AND 95B
         out[j]    = string(b)
      endif else out[j] = str
   endelse

endfor

if num EQ 1 then out = out[0]

return, out

END
