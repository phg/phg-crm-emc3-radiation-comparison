; PROJECT:
;       ADAS
;
; NAME:
;       ADAS_STRING_JUSTIFY()
;
; PURPOSE:
;       Takes a scalar text string and splits it across
;       multiple lines to return a string array.
;
; EXPLANATION:
;       This function takes a scalar string and a maximum
;       number of columns, it then splits the string where
;       there are spaces such that all the lines are as close
;       as possible to the number of columns specified.
;
; USE:
;       An example of how to use the function:
;          myval = "This is a scalar string with no carriage "+$
;                  "returns present at all, by the time it "  +$
;                  "comes back from adas_string_justify it "  +$
;                  "will be a four element string array"
;          split=adas_string_justify(myval,40)
;          help,split
;          for i=0,n_elements(split)-1 do print,split[i]
;
; INPUTS:
;       IN       - Scalar string to split
;       COLS     - Number of characters allowed per line
;
; OPTIONAL INPUTS:
;       None
;
; OUTPUTS:
;       The return value of this function is a string array
;       split at the appropriate place
;
; OPTIONAL OUTPUTS:
;       None
;
; KEYWORD PARAMETERS:
;       None
;
; CALLS:
;       None
;
; SIDE EFFECTS:
;       None
;
; CATEGORY:
;       Utility
;
; WRITTEN:
;       Allan Whiteford
;
; MODIFIED:
;
;       1.1     Allan Whiteford
;               Initial release
;
;       1.2     Allan Whiteford
;               Caught for infinite loop if break happens on a space
;
;       1.3     Martin O'Mullane
;                - Changed rstrpos to strpos with /reverse_search.
;
; VERSION:
;       1.1     15-Aug-2008
;       1.2     24-Nov-2008
;       1.3     17-10-2018
;-
;-----------------------------------------------------------------------------

function adas_string_justify,in,cols
        out=[in]
        while strlen(out[0]) gt cols and strpos(out[0],' ') ne -1 do begin
                if (idx=strpos(strmid(out[0],0,cols+1),' ', /reverse_search)) eq -1 then idx=strpos(out[0],' ')
                out=[out,strmid(out[0],0,idx)]
                out[0]=strmid(out[0],idx+1)
        end
        return,([out,out[0]])[1:*]
end
