; Copyright (c) 1996 Strathclyde University .
; SCCS info: Module @(#)$Header: /home/adascvs/idl/adaslib/util/anno.pro,v 1.1 2004/07/06 11:16:13 whitefor Exp $ Date $Date: 2004/07/06 11:16:13 $
;+
; PROJECT:
;       ADAS IBM MVS to DEC UNIX conversion
;
; NAME:
;       ANNO
;
; PURPOSE:
;       To annotate a plot while keeping a record of where annotations have
;	been made and positioning the new annotation to avoid previous ones.
;
; EXPLANATION:
;       A linked list of annotation positions is searched through to see if
;	there is an overlap with previous annotations made. If so then the
;	routine calls itself recursively with an offset position until a clear
;	space for the annotation is found. The annotation position is then
;	added to the end of the linked list.
;
; USE:
;       May be used by any routine that has opened a window and has a data
;	coordinate system set up for that window.
;
;	Example:
;
;	; Put two sets of annotations on top of one another
;	x = double(findgen(100)/10)
;	y=sin(x)
;	plot, x, y
;	top_handle = create_handle()
;	for i=0,99,9 do begin
;	    anno, x(i),y(i), 'FIRST', top_handle
;	endfor
;	for i=0,99,9 do begin
;	    anno, x(i),y(i), 'SECOND', top_handle
;	endfor
;	handle_free, top_handle
;
; INPUTS:
;
;	X	-	Float; x position in data coordinates
;
;	Y	-	Float; y position in data coordinates
;
;       STRIN 	-	String; the string to be placed at position (x,y)
;
;	TOP_HANDLE -    Long; a reference to a handle to be used as the parent
;			of the linked list. See the IDL Users' Guide 11-16
;			for information on handles and their use in
;			implementing linked lists.
; OPTIONAL INPUTS:
;       None.
;
; OUTPUTS:
;       None.
;
; OPTIONAL OUTPUTS:
;       None.
;
; CALLS:
;       None.
;
; SIDE EFFECTS:
;       None.
;
; CATEGORY:
;       Adas system.
;
; WRITTEN:
;       William Osborn, Tessella Support Services plc,  25th June 1996
;
; MODIFIED:
;       1.1     William Osborn    
;		First release
;
; VERSION:
;	1.1	25-06-96
;
;-
;----------------------------------------------------------------------------

PRO anno, x, y, strin, top_handle

    done = 0B

    ;**** Convert to device coordinates since they are easier to use ****
    ;**** with logarithmic axes and can be used with all other types ****

    c = convert_coord(x, y, /data, /to_device)
    xx = c(0)
    yy = c(1)

    ;**** Calculate the x and y size of strin

    xs = (strlen(strin)*!d.x_ch_size)
    ys = !d.y_ch_size

    ;**** Find the first element of the linked list, stored as the
    ;**** first child of top_handle. Then go through the linked list,
    ;**** member by member

    child = handle_info(top_handle, /first_child)
    while handle_info(child) do begin

	handle_value, child, val

	;**** Check whether there is an overlap

	if xx+xs ge val.xmin and xx le val.xmax and yy+ys ge val.ymin $
	   and yy le val.ymax then begin

	    ;**** If so, call anno again but with an offset in the y direction

            d = convert_coord(xx,yy-1.1*ys, /device, /to_data)
	    anno, d(0), d(1), strin, top_handle
	    done = 1B
	    child = 0L

	endif else child = handle_info(child, /sibling)

    endwhile
	    
    ;**** If we've been through all the members and not found a clash then
    ;**** print the annotation and create a new member of the linked list

    if not done then begin
    	xyouts, xx, yy, strin, alignment=0.5, /device
	val = {xmin:xx, ymin:yy, xmax:xx+xs, ymax:yy+ys}
	a = handle_create(top_handle, value = val)
    endif

END

