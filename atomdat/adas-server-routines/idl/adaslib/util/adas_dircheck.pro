; Copyright (c) 1999 Strathclyde University .
; SCCS info: Module @(#)$Header: /home/adascvs/idl/adaslib/util/adas_dircheck.pro,v 1.1 2004/07/06 11:08:40 whitefor Exp $    Date $Date: 2004/07/06 11:08:40 $
;+
; PROJECT:
;       ADAS
;
; NAME:
;       ADAS_DIRCHECK
;
; PURPOSE:
;       Determine whether a directory exists and if the user
;       has permission to write to it.
;
; EXPLANATION:
;       This routine is a wrapper for the ADAS utility FILE_ACC which
;       checks if a directory exists and whether the user can write
;       a file into it. It checks in order
;           - existence
;           - whether it is a directory
;           - write permission
;
;       An error message and number are returned.
;
;
; INPUTS:
;       FILE    - The UNIX system name of the file. For example
;                 'file.dat' refers to the current directory and
;                 '/disk2/fred/data' is a full path name.
;
; OPTIONAL INPUTS:
;       None
;
; OUTPUTS:
;       ERROR   - Integer, 1 if there is an error, 0 if it does not.
;
;       MESSAGE - String, indicating nature of the error. 
;
; OPTIONAL OUTPUTS:
;       None
;
; KEYWORD PARAMETERS:
;       None
;
; CALLS:
;
;       FILE_ACC        Get basic file information through 'ls' command.
;
;
; SIDE EFFECTS:
;       A number of UNIX commands are spawned.
;
; CATEGORY:
;       UNIX system IDL utility.
;
; WRITTEN:
;       Martin O'Mullane, 26-8-99
;
; MODIFIED:
;       1.1     Martin O'Mullane
;               - First release.
;   
; VERSION:
;       1.1     26-8-99 
;
;----------------------------------------------------------------------------

PRO adas_dircheck, file, error, message

message = ''
error   = 0

file_acc, file, exist, read, write, execute, filetype

if exist NE 1 then begin
   error = 1
   message = 'Directory does not exist'
endif
if error EQ 0 AND filetype NE 'd' then begin
   error = 1
   message = 'Choice is not a directory'
 endif
if error EQ 0 AND write NE 1 then begin
   error = 1
   message = 'No write permission for this directory'
endif

END
