;----------------------------------------------------------------------
;+
; PROJECT:
;       ADAS
;
; NAME:
;       ADAS_READFILE
;
; PURPOSE:
;       Read in a text file and return it as a string array.
;
; EXPLANATION:
;       Based in part on the rd_tfile.pro routine of the 
;       NASACOM library. The file is read as a byte array
;       and split into lines at each LF.
;        
;
; INPUTS:
;       FILE    - The UNIX system name of the file. For example
;                 'file.dat' refers to the current directory and
;                 '/disk2/fred/data' is a full path name.
;
; OPTIONAL INPUTS:
;       None
;
; OUTPUTS:
;
; OPTIONAL OUTPUTS:
;       NLINES  - The number of lines in the file. 
;
; KEYWORD PARAMETERS:
;       None
;
; CALLS:
;       None
;
; SIDE EFFECTS:
;       None
;
; CATEGORY:
;       UNIX system IDL utility.
;
; WRITTEN:
;       Martin O'Mullane, 11-11-2004
;
;
; MODIFIED:
;       1.1     Martin O'Mullane
;               - First release.
;   
; VERSION:
;       1.1     11-11-2004 
;
;-
;----------------------------------------------------------------------

PRO adas_readfile, file=file, all=all, nlines=nlines

; read file into text buffer

openr, lun, /get_lun, file

fstatus = fstat(lun)                           ; determine file size
btext   = bytarr(fstatus.size)                 ; byte buffer for all
readu, lun, btext                              ; read into byte buffer
wlfs    = where(btext eq 10b, lfcount)         ; number of line feeds
btext   = 0                                    ; release memory

all = strarr(lfcount)                          ; now use string arrary

point_lun, lun, 0                              ; reset to beginning
readf, lun, all                                ; read into string array
free_lun, lun


if arg_present(nlines) then nlines = n_elements(all)


END
