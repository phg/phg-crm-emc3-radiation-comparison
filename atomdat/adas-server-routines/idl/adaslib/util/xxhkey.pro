; Copyright (c) 1995, Strathclyde University.
; Alessandro Lanzafame	acl@phys.strath.ac.uk	14 March 1996
; SCCS info: Module @(#)$Header: /home/adascvs/idl/adaslib/util/xxhkey.pro,v 1.1 2004/07/06 15:36:28 whitefor Exp $ Date $Date: 2004/07/06 15:36:28 $
;+
; PROJECT:
;       ADAS support programs
;
; NAME:
;       XXHKEY
;
; PURPOSE:  
;           To extract from a line of text 'CTEXT' a response to a key
;           in the form of '<CKEY> = <CANS>'.
;
;
; EXPLANATION:
;           This is an IDL adaptation of Briden's FORTRAN routine
;           XXHKEY. NOTE: THE EMBEDDED CASE BEHAVES DIFFERENTLY FROM
;           THE FORTRAN COUNTERPART.
;
;
;           this routine extracts from 'CTEXT' a response to a given key
;           in ther form of '<CKEY> = <CANS>'. e.g. 'FILE = DSN001'
;           would require as input CKEY='FILE' and would give as output
;           CANS='DSN001'. All key/response pairs must be separated by
;           the character given by 'CBREAK' e.g. a slash, and each key
;           must be followed by an equals sign. The number of spaces
;           between the key and the equal sign and between the response
;           and the equal sign is not important.
;
;           The byte preceeding the key must be a blank or 'CBREAK'
;           character unless it starts at byte one in 'CTEXT'.
;
;           If a key does not exist in 'CTEXT' then 'CANS' is returned
;           blank.
;           The key is taken as 'CKEY' removing any trailing blanks.
;           leading blanks are left in place and will used when the
;           the search for the key is made:
;
;           i.e.  'DATA   ' and 'DATA' are the same key but
;                 ' DATA '  and 'DATA ' are different keys although
;                 both will give the same results if a space exists
;                 before 'DATA' in the input text line.
;
;           An example of an input text line is:
;
;           8524.0 A    5 7 /FILMEM = FBBH91BE/   CODE=   V2B DLN1   /
;
;           This would give the following:
;
;           CKEY='FILMEM'  =>  CANS='FBBH91BE'
;           CKEY=' FILMEM' =>  CANS=' '
;           CKEY='CODE'    =>  CANS='V2B DLN1'
;           CKEY=' CODE'   =>  CANS='V2B DLN1'
;           CKEY='OTHER'   =>  CANS=' '
;
;           (If the character string is shorter than the response then
;            the response is truncated accordingly.)
;
;           Spaces can exist in the key. i.e. CKEY='PLOT A'. but care
;           should be taken when using prefixes on a common key base,
;           i.e. 'A PLOT', 'B PLOT'. This is because if a subsequent
;           key to be found is 'PLOT' then  either of these satisfy
;           this criterion as well as 'PLOT' itself.
;
;           An example of an input text line is:
;
;           A FILE=TEST0/B FILE = TEST1/FILE=TEST2/FILE 1=TEST3/FILE 2=/
;
;           This would give the following:
;
;           CKEY='A FILE'  =>  CANS='TEST0'
;           CKEY='B FILE'  =>  CANS='TEST1'
;           CKEY='FILE'    =>  CANS='TEST0' (WRONG RESPONSE PICKED UP)
;           CKEY='FILE 1'  =>  CANS='TEST3'
;           CKEY='FILE 2'  =>  CANS=''      (Note: error in Briden's
;                                            comments at this pont)
;
;           It is also possible to embed responses, but here the IDL
;           version has a different behavior than FORTRAN (at least
;           for the time being).
;
;           An example of an input text line is:
;
;           FILE 1 =  Z1 = 23 / FILE = FILE 1 = 6 /
;
;           This would give the following:
;
;           CKEY='FILE 1'  =>  CANS='Z1 = 23'
;           CKEY=' FILE 1' =>  CANS='FILE 1 = 6' (Note: different than
;                                                 FORTRAN)
;           CKEY='Z1'      =>  CANS='23'
;           CKEY='FILE'    =>  CANS='FILE 1 = 6'
;
; USE:
;     Example:
;
;     xxhkey,'8524.0 A    5 7 /FILMEM = FBBH91BE/   CODE=   V2B DLN1/',$
;            ' CODE','/',cans
;
;      returns
;
;              CANS            STRING    = 'V2B DLN1'
;
; INPUTS:
;
;         CTEXT   = INPUT TEXT LINE CONTAINING KEY & RESPONSES
;         CKEY    = KEY TEXT
;         CBREAK  = KEY/RESPONSE PAIR SEPERATOR SYMBOL
;
; OPTIONAL INPUTS:
;	None
;
; OUTPUTS:
;         CANS    = RERSPONSE FOR GIVEN KEY: BLANK IF NOT FOUND
;
; OPTIONAL OUTPUTS:
;	None
;
; KEYWORD PARAMETERS:
;	None
;
; CALLS:
;       None
;
; SIDE EFFECTS:
;       None known
;
; CATEGORY:
;	Adas support and applications.
;	
; WRITTEN:
;       Alessandro Lanzafame, University of Strathclyde, mar18-95
;
; MODIFIED:
;	1.0	Alessandro Lanzafame
;		First version
;	1.1	Tim Hammond
;		Put under S.C.C.S. control
;
; VERSION:
;	1.0	18-03-95
;	1.1	03-05-96
;
;-
;-----------------------------------------------------------------------

PRO xxhkey, ctext, ckey, cbreak, cans

;-----------------------------------------------------------------------

    lentxt = strlen(ctext)
    lenkey = strlen(ckey)
;    lenans = strlen(cans)


;-----------------------------------------------------------------------
; FIND THE LENGTH OF THE KEY IGNORING ANY TRAILING BLANKS
;-----------------------------------------------------------------------


    ikey = strlen(strtrim(ckey))
    if ikey eq 0 then goto, EXIT

;-----------------------------------------------------------------------
; ESTABLISH IF A VALID KEY CAN BE FOUND IN 'CTEXT'
;-----------------------------------------------------------------------

    ipos1 = 0

LABEL100:

    ipos1 = strpos(strmid(ctext,ipos1,lentxt), strtrim(ckey))+ipos1
    ipos2 = ipos1 + ikey

    CASE 1 OF
       (ipos2 gt lentxt): goto, EXIT
       (ipos1 eq 0)     : ipos1 = ipos2 
       (ipos1 ne -1)    : begin
                            ipos1 = ipos1 - 1
                            if strmid(ctext,ipos1,1) eq ' ' $
                            or strmid(ctext,ipos1,1) eq cbreak then $
                            ipos1=ipos2 
                          end
       else             : goto, EXIT
    ENDCASE

;-----------------------------------------------------------------------
; ESTABLISH IF EQUAL SIGN EXISTS AFTER THE KEY (SEPERATED BY BLANKS).
;-----------------------------------------------------------------------
 
    ipos2 = strpos(strmid(ctext,ipos1,(lentxt-ipos1)) , '=')
    ipos3 = ipos2 + ipos1 + 1
    CASE 1 OF 
       (ipos3 gt lentxt) : goto, EXIT
       (ipos2 eq 0)      : ipos2 = ipos3
       (ipos2 ne -1)     : begin
                              ipos2 = ipos3 - 1
    
; see if valid key exists further down the 'ctext' string

                              if strmid(ctext,ipos1,(ipos2-ipos1)) eq ' ' then $
                              ipos2 = ipos3 else goto, LABEL100 
    
                           end
       else              : goto, EXIT
    ENDCASE
 
;-----------------------------------------------------------------------
; FIND SEPERATOR CHARACTER AND IDENTIFY RESPONSE (IF PRESENT).
;-----------------------------------------------------------------------

    ipos1 = 0
    ipos3 = strpos(strmid(ctext,ipos2,(lentxt-ipos2)), cbreak)
    
    if ipos3 eq -1 then ipos3 = lentxt-1 else $
    if ipos3 eq 0 then goto, EXIT    else ipos3 = ipos3 + ipos2 - 1  

    for i = ipos2, ipos3 do begin
       if ipos1 eq 0 and strmid(ctext,i,1) ne ' ' then ipos1 = i
    endfor

    if ipos1 eq 0 then goto, EXIT

;if ipos1 eq 0 then goto, EXIT else $
;if (ipos3-ipos1+1) gt lenans then ipos3 = ipos1+lenans

 
;-----------------------------------------------------------------------
; VALID RESPONSE FOUND - SET UP 'CANS'
;-----------------------------------------------------------------------

    cans = strtrim(strmid(ctext, ipos1, (ipos3-ipos1+1)))

    goto, GOTIT

;-----------------------------------------------------------------------
; INVALID OR NO RESPONSE/KEY FOUND - RETURN 'CANS' AS BLANK
;-----------------------------------------------------------------------
 
EXIT:
    cans = ' '

;-----------------------------------------------------------------------

GOTIT:

END
