;----------------------------------------------------------------------
;+
; PROJECT    :  ADAS
;
; NAME       :  adas_array_print
;
; PURPOSE    :  Prints an array with a user supplied format suitable for
;               embedding in an IDL or python script.

;               NAME      I/O    TYPE   DETAILS
; INPUTS     :  a          I            array to be printed
;               name       I     str    The name to be used in the print
;                                       statement. Defaults to 'array = '
;               format     I     str    Valid format statement for outpur.
;               linecont   I     str    Line continuation parameter. Defaults
;                                       to '$' for IDL.
;               nlines     O     int    Number of line in output array
;
;
; NOTES      :  - To generate statements suitable for python set linecont=''
;               - Set the format for a single element, eg 'f8.3' rather than
;                 using a range, eg '7f10.4'.
;
; AUTHOR     :  Martin O'Mullane
;
; MODIFIED :
;               1.1 Martin O'Mullane
;                    - First version
;
; VERSION :
;               1.1 02-11-2018
;-
;----------------------------------------------------------------------


FUNCTION adas_array_print, a, name=name, format=format, linecont=linecont, $
                              nlines=nlines

; Check options

if n_elements(name) EQ 0 then n_str = 'array' else n_str = name
if n_elements(format) EQ 0 then message, 'Suppy a format statement'
if n_elements(linecont) EQ 0 then c_char = ' $' else c_char = linecont

; If format statement is enclosed in () remove them

fmt = strcompress(strlowcase(format), /remove_all)
c1  = strmid(fmt, 0, 1)
c2  = strmid(fmt, strlen(fmt)-1)
if c1 EQ '(' AND c2 EQ ')' then fmt = strmid(fmt, 1, strlen(fmt)-2)

; Only a limited set of numeric formats are permitted

c1  = strmid(fmt, 0, 1)
if strpos('ifeg', c1) EQ -1 then message, 'Not a usable format'


; Find width of field

i1 = strpos(fmt, '.')
if i1 EQ -1 then width = fix(strmid(fmt,1)) else width = fix(strmid(fmt,1,strlen(fmt)-1))
if width GT 40 then message, 'Keep width of number below 40 characters'

; Assume 80 columns and construct format statement with 80/width entries
; and write into a string array

n_col  = 80 / width
fmt0   = '(' + strcompress(string(n_col), /remove_all) + '(' + fmt + ',",",:)' + ')'
outstr = string(a, format=fmt0)

; Add 'name = ', a continuation character per line and remove any trailing commas

num    = n_elements(outstr)

tmp = outstr[num-1]
i1  = strpos(tmp, ',', /reverse_search)
strput, tmp, ' ', i1
outstr[num-1] = tmp
if num GT 1 then begin
   for j = 0, num-2 do outstr[j] = outstr[j] + c_char
endif

padstr = string(replicate(32B, strlen(n_str)+4))
if num EQ 1 then padstr = ''
outstr[0] = n_str + ' = [' + outstr[0]
if num GT 2 then for j = 1, num-2 do outstr[j] = padstr + outstr[j]
outstr[num-1] = padstr + outstr[num-1] + ']'

; Return the array

nlines = num

return, outstr

END
