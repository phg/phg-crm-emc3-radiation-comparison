;----------------------------------------------------------------------
;+
; PROJECT  : ADAS
;
; NAME     : i4itran04
;
; PURPOSE  : Find the index in the adf04 A-value and gamma arrays
;            corresponding to user supplied upper and lower levels.
;
;            NAME         I/O   TYPE   DETAILS
; REQUIRED : a04_upper()  I     int    vector of upper levels in adf04 file
;            a04_lower()  I     int              lower
;            upper        I     int    upper index of transition required
;            lower        I     int    lower
;
; RETURNS  : index        O     int    index corresponding to the
;                                      transition upper->lower.
;
; NOTES    : lower and upper are as given in the adf04 file. ie 1 is ground.
;            The returned index follows the zero-indexing convention of IDL.
;
; MODIFIED:
;       1.1     Martin O'Mullane
;                 - First release
;
; VERSION:
;       1.1     14-08-2018
;
;-----------------------------------------------------------------------------

FUNCTION i4itran04, a04_upper = a04_upper, $
                    a04_lower = a04_lower, $
                    upper     = upper,     $
                    lower     = lower

iexp = ceil(alog10(max([a04_upper, a04_lower])))
mul  = long(10.0^iexp)

a04_index = long(a04_upper) * mul + long(a04_lower)

index = where(a04_index EQ long(upper)*mul + long(lower), count)

if count EQ 0 then message, 'No index found'
if count GT 1 then message, 'No unique index found'

return, index[0]

END
