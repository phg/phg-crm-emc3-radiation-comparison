; Copyright (c) 1995, Strathclyde University
; SCCS info: Module @(#)$Header: /home/adascvs/idl/adaslib/util/xxtcon.pro,v 1.1 2004/07/06 15:39:54 whitefor Exp $ Date $Date: 2004/07/06 15:39:54 $
;+
; PROJECT:
;       ADAS IBM MVS to UNIX conversion
;
; NAME:
;       xxtcon
;
; PURPOSE:
;       IDL translation of FORTRAN routine XXTCON.FOR which converts
;       an array of temperatures into specified units.
;
; USE:
;       General
;
; INPUTS:
;	INTYP	-	Int. 1 => 'TIN(array)' UNITS: KELVIN
;			     2 => 'TIN(array)' UNITS: eV
;			     3 => 'TIN(array)' UNITS: REDUCED TEMP.
;
;	OUTTYP	-	Int. 1 => 'TOUT(array)' UNITS: KELVIN
;			     2 => 'TOUT(array)' UNITS: eV
;			     3 => 'TOUT(array)' UNITS: REDUCED TEMP.
;
;	IZ1	-	Int. Recombining ion charge (=Z+1)
;
;	ITVAL	-	Int. Number of temperatures in TIN array
;
;	TIN()	-	Fltarr. Input temperatures (stated units)
;
;	TOUT()	-	Fltarr. Output temperatures (stated units)
;
; OPTIONAL INPUTS:
;       None
;
; OUTPUTS:
;       TOUT()  -       Fltarr. Output temperatures (stated units)
;
; OPTIONAL OUTPUTS:
;       None
;
; KEYWORD PARAMETERS:
;       None
;
; CALLS:
;       None
;
; SIDE EFFECTS:
;       None
;	
; CATEGORY:
;       Adas system.
;
; WRITTEN:
;       Tim Hammond, Tessella Support Services plc, 27/07/95
;
; MODIFIED:
;       1.1     Tim Hammond
;               First release - translated from FORTRAN written by
;		Paul Briden (04/01/91)
; VERSION:
;       1.1     27/07/95
;
;-----------------------------------------------------------------------------
;-

PRO xxtcon, intyp, outtyp, iz1, itval, tin, tout

    ON_ERROR, 2

    ;**** Set up conversion factors ****

    z1p2 = (double(iz1))^2		;conversions to/from reduced
    ev2kel = 1.16054d+4			;eV -> Kelvin conversion
    kel2ev = 8.61668d-5			;Kelvin -> eV conversion
    tconv = fltarr(3)			;Temp conversion param. array
    tout = dblarr(itval)		;Output values
    
    ;**** Case 1: output in Kelvin ****

    if (outtyp eq 1) then begin
	tconv(0) = 1.0
	tconv(1) = ev2kel
	tconv(2) = z1p2

    ;**** Case 2: output in eV ****

    endif else if (outtyp eq 2) then begin
	tconv(0) = kel2ev
	tconv(1) = 1.0
	tconv(2) = z1p2 * kel2ev

    ;**** Case 3: output in Reduced temp. form ****

    endif else if (outtyp eq 3) then begin
	tconv(0) = 1.0 / z1p2
	tconv(1) = tconv(1) * ev2kel
	tconv(2) = 1.0

    ;**** Else an incorrect form has been requested ****

    endif else begin
	message,'Invalid Output Temperature Specifier'
    endelse

    ;**** Construct the outputs ****

    for i=0, (itval-1) do begin
	tout(i) = tconv(intyp-1) * tin(i)
    endfor

    ;**** Routine completed, return result ****

    return

END
