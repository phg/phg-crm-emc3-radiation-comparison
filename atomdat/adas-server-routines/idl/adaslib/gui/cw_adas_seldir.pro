; Copyright (c) 2000 Strathclyde University .
; SCCS info: Module @(#)$Header: /home/adascvs/idl/adaslib/gui/cw_adas_seldir.pro,v 1.1 2004/07/06 13:02:35 whitefor Exp $ Date $Date: 2004/07/06 13:02:35 $
;+
; PROJECT:
;       ADAS
;
; NAME:
;	CW_ADAS_SELDIR()
;
; PURPOSE:
;	Directory selection for use in input file selection.
;
; EXPLANATION:
;	This function creates a compound widget consisting of;
;	1. A text widget with the current root directory
;	   path name.
;       Either
;	  2. A button to set the root to a user default root.
;	  3. A button to set the root to a central default root.
;	
;       Or
;	  2. A default button to set the root to a user default root.
;       And
;       4. A toggle button to enable/disable direct keyboard entry
;	   of the root.
;	This widget performs is own checking and validation of the
;	path and issues its own error messages.
;
;	This widget generates events as the user interacts with it.
;	The event structure returned is;
;	{ID:0L, TOP:0L, HANDLER:0L, ACTION:''}
;	ACTION has one of two values indicating the state of the root
;	selection process, either 'rootedit' when the root is being
;	edited and is not verified or 'newroot' when a new verified
;	root has been selected.
;
;	To operate the widget the user can press the 'User Data' or
;	'Central Data' buttons to copy those default roots to the text
;	widget showing the current root.  Alternatively the 'Edit Path Name'
;	toggle can be switched on to allow direct keyboard entry of the
;	current root.  To complete keyboard entry the user either unsets
;	the toggle button or presses the return key.  Whenever a new root
;	name is set, either with defaults buttons or by direct entry,
;	the path name is checked.  Any error is flagged with a message
;	and the widget is automatically put into 'Edit Path Name' mode
;	until a valid path is entered.  Every time the path is changed
;	an event is generated as described above.  This is to notify
;	any dependent IDL routines of the change.
;
;       Based on cw_adas_root.pro.
;
; USE:
;	
;
; INPUTS:
;       PARENT   - The ID of the parent widget.
;
; OPTIONAL INPUTS:
;	None.
;
; OUTPUTS:
;       The return value of this function is the ID of the compound
;       widget which is of type LONG integer.
;
; OPTIONAL OUTPUTS:
;       None
;
; KEYWORD PARAMETERS:
;	VALUE	- A structure which must be;
;		  {ROOTPATH:'', CENTROOT:'', USERROOT:'' }
;		  The elements of the structure are as follows;
;
;		  ROOTPATH - Current data directory e.g '/usr/fred/adas/'
;		  CENTROOT - Default central data store e.g '/usr/adas/'
;		  USERROOT - Default user data store e.g '/usr/fred/adas/'
;
;		  Path names may be supplied with or without the trailing
;		  '/'.  This widget will add this where required so that
;		  USERROOT will always end in '/' on output.  The default
;		  value is;
;		  {ROOTPATH:'./', CENTROOT:'', USERROOT:''}
;		  i.e ROOTPATH is set to the user's current directory.
;
;       FONT     - A font to use for all text in this widget.
;
;	UVALUE	 - A user value for this widget.
;
;       EFUN     - A user supplied event handler.
;
;	DEFAULT  - Setting this keyword enables the central and user
;		   switching buttons to be hidden and replaced by a 
;                  single 'Default' button. Setting a central output
;                  directory is not always appropriate.
;
; CALLS:
;	FILE_ACC - Used to verify path name.
;
; SIDE EFFECTS:
;
;       Three other routines are included which are used to manage the
;       widget;
;
;	seldir_SET_VAL
;	seldir_GET_VAL()
;	seldir_EVENT()
;
; CATEGORY:
;	Compound Widget
;
; WRITTEN:
;       Martin O'Mullane, 24-Jan-2000
;
; MODIFIED:
;       1.1     Martin O'Mullane
;               First release.
;
; VERSION:
;       1.1	24-01-2000
;-
;-----------------------------------------------------------------------------

PRO seldir_set_val, id, value

		;**** Return to caller on error ****
  ON_ERROR, 2

		;**** Retrieve the state ****

    first_child = widget_info(id,/child)
    widget_control, first_child, get_uvalue=state, /no_copy


		;**** Tidy path name to conform to routine convention ****
  if strtrim(value.rootpath) eq '' then begin
    value.rootpath = './'
  end else if strmid(value.rootpath,strlen(value.rootpath)-1,1) ne $
								'/' then begin
    value.rootpath = value.rootpath+'/'
  end
		;**** Copy new rootname to widget ****
  widget_control,state.rootid,set_value=value.rootpath

		;**** Copy the new value to state structure ****
  state.rootval.rootpath = value.rootpath
  state.rootval.centroot = value.centroot
  state.rootval.userroot = value.userroot

		;**** Save the new state ****
                
    widget_control, first_child, set_uvalue=state, /no_copy

END

;-----------------------------------------------------------------------------


FUNCTION seldir_get_val, id


		;**** Return to caller on error ****
  ON_ERROR, 2

		;**** Retrieve the state ****
                
    first_child = widget_info(id, /child)
    widget_control, first_child, get_uvalue=state



  RETURN, state.rootval

END

;-----------------------------------------------------------------------------

FUNCTION seldir_event, event

		;**** Base ID of compound widget ****
  
  base=event.handler

		;**** Retrieve the state ****

  first_child = widget_info(base, /child)
  widget_control, first_child, get_uvalue = state, /no_copy

  default = state.default

		;************************
		;**** Process Events ****
		;************************
  CASE event.id OF


    state.centid: begin
	  widget_control,state.rootid,set_value=state.rootval.centroot
	  state.rootval.rootpath = state.rootval.centroot
	  if state.editon eq 1 then begin
	    action = ''
	  end else begin
	    action = 'newroot'
	  end
	end

    state.userid: begin
	  widget_control,state.rootid,set_value=state.rootval.userroot
	  state.rootval.rootpath = state.rootval.userroot
	  if state.editon eq 1 then begin
	    action = ''
	  end else begin
	    action = 'newroot'
	  end
	end

    state.defaultid: begin
	  widget_control,state.rootid,set_value=state.rootval.userroot
	  state.rootval.rootpath = state.rootval.userroot
	  if state.editon eq 1 then begin
	    action = ''
	  end else begin
	    action = 'newroot'
	  end
	end

    state.editid: if state.editon eq 0 then begin
	  state.editon = 1
	  widget_control,state.rootid,/sensitive,/input_focus
	  action = 'rootedit'
	end else begin
	  state.editon = 0
	  widget_control,state.rootid,sensitive=0
	  widget_control,state.rootid,get_value=rootpath
	  state.rootval.rootpath = rootpath(0)
	  action = 'newroot'
	end

    state.rootid: begin
	  state.editon = 0
	  widget_control,state.rootid,sensitive=0
	  widget_control,state.editid,set_button=0
	  widget_control,state.rootid,get_value=rootpath
	  state.rootval.rootpath = rootpath(0)
	  action = 'newroot'
	end

    ELSE:

  ENDCASE

		;**** Clear any existing error message ****
  widget_control,state.messid,set_value=' '

		;**** Check if the supplied name is okay     ****
		;**** Force edit mode in the event of error. ****
		;**** Tidy path name if it is okay.          ****
  if action eq 'newroot' then begin
    file_acc,state.rootval.rootpath,fileexist,read,write,execute,filetype
    if fileexist ne 1 then begin
      error = 1
      message = 'Error: The supplied name is invalid.'
      widget_control,state.messid,set_value=message
      state.editon = 1
      widget_control,state.editid,set_button=1
      widget_control,state.rootid,/sensitive,/input_focus
      action = 'rootedit'
    end else begin
      if strtrim(state.rootval.rootpath) eq '' then begin
        state.rootval.rootpath = './'
      end else if $
	  strmid(state.rootval.rootpath,strlen(state.rootval.rootpath)-1,1) $
							ne '/' then begin
        state.rootval.rootpath = state.rootval.rootpath+'/'
      end
      widget_control,state.rootid,set_value=state.rootval.rootpath
    end
  end

		;**** Save the new state structure ****


  efun = state.efun
  widget_control, first_child, set_uvalue=state, /no_copy
  
; Return a struct or pass to a user supplied event handler?

  ret = {ID:base, TOP:event.top, HANDLER:0L, ACTION:action}
  if efun NE '' then return, call_function(efun,ret) else return, ret

END

;-----------------------------------------------------------------------------

FUNCTION CW_ADAS_SELDIR, parent, VALUE=value, FONT=font, UVALUE=uvalue,	$
                         DEFAULT=default,  EVENT_FUNCT=efun
                         

  IF (N_PARAMS() LT 1) THEN MESSAGE, 'Must specify PARENT for CW_ADAS_SELDIR'

		;**** Set defaults for keywords ****
  IF NOT (KEYWORD_SET(value)) THEN BEGIN
    rootval = {rootval, ROOTPATH:'./', $
			CENTROOT:'', $
			USERROOT:'' }
  END ELSE BEGIN
    rootval = {rootval, ROOTPATH:value.rootpath, $
			CENTROOT:value.centroot, $
			USERROOT:value.userroot }
    if strtrim(rootval.rootpath) eq '' then begin
      rootval.rootpath = './'
    end else if strmid(rootval.rootpath,strlen(rootval.rootpath)-1,1) ne $
								'/' then begin
      rootval.rootpath = rootval.rootpath+'/'
    end
  END
  IF NOT (KEYWORD_SET(font)) THEN font = ''
  IF NOT (KEYWORD_SET(uvalue))  THEN uvalue = 0
  IF NOT (KEYWORD_SET(default)) THEN default = 0

		;**** Create the main base for the widget ****
  
  frameflag = 0
  main = WIDGET_BASE(parent, UVALUE = uvalue, 				$
		EVENT_FUNC = "seldir_event", 				$
		FUNC_GET_VALUE = "seldir_get_val", 			$
		PRO_SET_VALUE = "seldir_set_val", 			$
		/COLUMN, frame=frameflag)

                ;******************************************************
                ;**** Create a dummy widget just to hold value of *****
                ;**** "state" variable so as not to get confused  *****
                ;**** with any other values. Adopt IDL practice   *****
                ;**** of using first child widget                 *****
                ;******************************************************

  first_child = widget_base(main)
  
  cwid = widget_base(first_child,/column)

		;**** Root path name for data ****
  base = widget_base(cwid,/row)
  rc = widget_label(base,value='Data Root ',font=font)
  rootid = widget_text(base,value=rootval.rootpath,/edit,xsize=40,font=font)
  widget_control,rootid,sensitive=0

		;**** Base for all buttons ****
  base = widget_base(cwid,/row)



		;**** Default path name buttons ****
                
  rc        = widget_base(base)
  rc2       = widget_base(rc, /row)
  rc3       = widget_base(rc, /row)
  
  centid    = widget_button(rc2,value='Central Data',font=font)
  userid    = widget_button(rc2,value='User Data',font=font)
  defaultid = widget_button(rc3,value='Default',font=font)
  
  if (default eq 0) then begin
      widget_control, centid, map=1
      widget_control, defaultid, map=0
  endif else begin
      widget_control, centid, map=0
      widget_control, defaultid, map=1
  endelse
 

		;**** Pathname override switch ****
                
  rc = widget_base(base,/row,/nonexclusive)
  editid = widget_button(rc,value='Edit Path Name',font=font)

		;**** Message ****
  messid = widget_label(cwid,value=' ',font=font)

		;**** Create state structure ****
  if n_elements(efun) le 0 then efun = ''
  new_state = { EFUN      :   efun,        $
                ROOTID    :   rootid,      $
                CENTID    :   centid,      $
                USERID    :   userid,      $
                DEFAULTID :   defaultid,   $
		EDITID    :   editid,      $
                MESSID    :   messid,      $
                EDITON    :   0,           $
		ROOTVAL   :   rootval,     $
                FONT      :   font,        $
                DEFAULT   :   default      }

		;**** Save initial state structure ****
  widget_control, first_child, set_uvalue=new_state,/no_copy

  RETURN, main

END
