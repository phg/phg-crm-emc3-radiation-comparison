; Copyright (c) 1995 Strathclyde University .
; SCCS info: Module @(#)$Header: /home/adascvs/idl/adaslib/gui/cw_list_select.pro,v 1.1 2004/07/06 13:07:12 whitefor Exp $ Date $Date: 2004/07/06 13:07:12 $
;+
; PROJECT:
;       ADAS IBM MVS to DEC UNIX conversion
;
; NAME: CW_LIST_SELECT()
;
; PURPOSE:
;       Allows selection to be made from a list and displays current
;	selection at top of list.
;
; EXPLANATION:
;       This routine creates a compound widget consisting of  the
;	compound widget cw_single_sel which allows the selection
;	of a single item from a presented list and a variable
;	number of buttons (always at least one) which allow the
;	different actions of the user to be interpreted. 
;
;	This routine generates events when any of the buttons are
;	activated. The selection from the list is handled internally
;	by the cw_single_sel routine. Here the events are not passed
;	out as a structure but stored in the COMMON block listselcom
;	and passed out when the widget window is destroyed and this
;	routine returns. Note that whatever button is pressed, the
;	widget is still destroyed.
;
; USE:
;       General use. See cw_adas401_in.pro for an example.
;
; INPUTS:
;       HEADINGS	-	String array containing the column headings
;				to be used in the list. These are passed
;				directly on to the cw_single_sel routine.
;
;	CHOICES		-	String array containing the items which
;				are to go in the list and be chosen from.
;
; OPTIONAL INPUTS:
;       None.
;
; OUTPUTS:
;       INDEX		-	Integer: the index of the element in the
;				array CHOICES which has been selected.
;
;	ACTION		-	Integer: index showing which of the
;				buttons has been pressed.
;
; OPTIONAL OUTPUTS:
;       None
;
; KEYWORD PARAMETERS:
;       TITLE		-	A heading to go above the list.
;
;	FONT		-	A font to use for all text in this widget.
;
;	XOFFSET		-	Integer: the offset of the widget (in pixels)
;				from the left edge of the screen.
;
;	YOFFSET		-	Integer: the offset of the widget (in pixels)
;                               from the top of the screen.
;
;	BUTTONS		-	String array containing the titles
;				of the buttons to be used.
;
;	TOPTITLE	-	String: a title for the window.
;
;	YSIZE		-	Integer: the length of the widget in rows.
;
; CALLS:
;	CW_SINGLE_SEL	Compound widget which handles the actual list.
;
; SIDE EFFECTS:
;	One other routine is included which is used to manage the
;       widget:
;	
;	CW_LIST_SELECT_EVENT()
;
; CATEGORY:
;       General use compound widget.
;
; WRITTEN:
;       Tim Hammond (Tessella Support Services plc), 7th September 1995
;
; MODIFIED:
;       1.1     Tim Hammond (Tessella Support Services plc)     07-09-95
; VERSION:
;       1.1     First release. 
;-
;-----------------------------------------------------------------------------

PRO cw_list_select_event, event

    COMMON listselcom, headings, choices, index, nbuttons, action

           ;**** Base ID of compound widget ****;

    parent=event.handler

           ;**********************************************;
           ;**** Retrieve the user value state        ****;
           ;**** Get id of first_child widget because ****;
           ;**** user value "state" is stored there   ****;
           ;**********************************************;

    first_child = widget_info(parent, /child)
    widget_control, first_child, get_uvalue=state, /no_copy

           ;************  Process events  ***************;

    buttonpress = 0
    for i = 0, (nbuttons-1) do begin
	if event.id eq state.buttonids(i) then begin
	    buttonpress = buttonpress + 1
	    widget_control, state.list, get_value=selected
	    index = selected
	    widget_control, /destroy, event.top
	    action = i
	endif
    endfor
    if buttonpress eq 0 then begin
	widget_control, first_child, set_uvalue=state, /no_copy
    endif

END

;-------------------------------------------------------------------------

PRO cw_list_select, headings, choices, index, action, TITLE=title, 	$
		    FONT=font, XOFFSET=xoffset, YOFFSET=yoffset, 	$
		    BUTTONS=buttons, TOPTITLE=toptitle, YSIZE=ysize

    COMMON listselcom, headingcom, choicescom, indexcom, nbuttons, actcom

    IF NOT (KEYWORD_SET(title)) THEN title = ' '
    IF NOT (KEYWORD_SET(font)) THEN font = ''
    IF NOT (KEYWORD_SET(xoffset)) THEN xoffset = 100
    IF NOT (KEYWORD_SET(yoffset)) THEN yoffset = 1
    IF NOT (KEYWORD_SET(buttons)) THEN buttons = [' OK ']
    IF NOT (KEYWORD_SET(toptitle)) THEN toptitle = ' '
    IF NOT (KEYWORD_SET(ysize)) THEN ysize=10
    if n_elements(choices) lt ysize then ysize = n_elements(choices)

           ;***************************************;
           ;********** declare variables **********;
           ;***************************************;
                                                               
    choicescom = choices
    headingcom = headings
    indexcom = index
    actcom = action

           ;***************************************;
           ;******* create base and widgets *******;
           ;***************************************;

    parent = widget_base(/column, title=toptitle, xoffset=100, yoffset=1)
    first_child = widget_base(parent)
    base = widget_base(first_child, /column)
    listbase = widget_base(base, /frame)
    list = cw_single_sel(listbase, choices, VALUE=index, FONT=font,	$
		         COLTITLES=headings, TITLE=title, YSIZE=ysize)
    buttonbase = widget_base(base, /row)
    buttonids = intarr(n_elements(buttons))
    for i=0, (n_elements(buttons) -1) do begin
	buttonids(i) = widget_button(buttonbase, value=buttons(i), font=font)
    endfor
    nbuttons = n_elements(buttons)

           ;***************************************;
           ;*** create a state structure for the **;
           ;*** popup window                     **;
           ;***************************************;

    new_state = {  list:list, buttonids:buttonids  }

           ;**** Save initial state structure *****;

    widget_control, first_child, set_uvalue=new_state, /no_copy

           ;***************************************;
           ;*********** realise widget  ***********;
           ;***************************************;

    widget_control, /realize, parent

           ;***************************************;
           ;** pass control to event handler and **;
           ;** make xmanager modal to avoid clash *;
           ;* with the currently running xmanager *;
           ;***************************************;

    xmanager, 'cw_list_select', parent, event_handler='cw_list_select_event',$
	      /modal

           ;***************************************;
           ;**** Copy selections back to index ****;
           ;**** to pass to calling program    ****;
           ;***************************************;

    index = indexcom
    action = actcom

END

