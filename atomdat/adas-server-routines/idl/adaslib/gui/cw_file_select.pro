; Copyright (c) 1995, Strathclyde University.
; SCCS info: Module  @(#)$Header: /home/adascvs/idl/adaslib/gui/cw_file_select.pro,v 1.13 2004/07/16 09:42:46 allan Exp $  Date $Date: 2004/07/16 09:42:46 $
;
;+
; PROJECT:
;       ADAS IBM MVS to UNIX conversion
;
; NAME: CW_FILE_SELECT()
;
; PURPOSE:
;	Allows UNIX system directory/file selection.
;
; EXPLANATION:
;	This function produces a compound widget which allows the
;	user to step through a UNIX file system selecting directories
;	and files.  The widget consists of a non-editable text widget
;	which displays the name of the currently selected file/directory
;	and below it a selection list of all available choices
;	in the currently selected directory.  The '..' symbol, meaning
;	topparent directory, may also appear in the selection list to allow
;	the user to back-track through the file system to explore
;	another directory path.  The file selection process starts
;	from a root directory which is specified by the caller.  The
;	user is not able select the topparent of this 'root' directory.
;	This is useful for confining the user to a particular part
;	of the UNIX file-system hierarchy.
;
;	This widget generates events as the user interacts with it.
;	The event structure returned is;
;	{ID:0L, TOP:0L, HANDLER:0L, ACTION:''}
;	ACTION has one of two values indicating the state of the file
;	selection process, either 'nofile' when the current selection
;	is a directory or 'newfile' when the current selection is file.
;
; USE:
;	An example of how to use this widget;
;
;		value = {ROOT:'/', FILE:''}
;		base = widget_base()
;		id = cw_file_sel(base,VALUE=value)
;		widget_control,base,/realize
;
;	.... process events using widget_event or xmanager ....
;		rc = widget_event()
;
;	.... get the selected file ....
;		widget_control,id,get_value=value
;		filename = value.root+value.file
;
;	Note: this widget will generate events at every user interaction.
;
; INPUTS:
;       topparent   - The ID of the topparent widget.
;
; OPTIONAL INPUTS:
;	None.
;
; OUTPUTS:
;       The return value of this function is the ID of the compound
;       widget which is of type LONG integer.
;
; OPTIONAL OUTPUTS:
;       None
;
; KEYWORD PARAMETERS:
;	VALUE	- A structure which must be;
;		  {ROOT:'', FILE:''}
;		  The elements of the structure are as follows;
;
;		  ROOT - Root directory e.g '/usr/fred/adas/'
;		  FILE - Current data file in ROOT e.g 'adf/input.dat'
;
;		  The caller must ensure that ROOT and FILE are valid
;		  names otherwise this widget may cause an error.
;		  Both ROOT and FILE may be set to '' on entry.  This
;		  will cause ROOT to be set to the user's current
;		  directory './'.
;
;		  The default value is;
;		  fselval = {ROOT:'./', FILE:''}
;
;	TITLE	- A title to be added to the left of the selection list.
;
;	XSIZE	- The width in characters of the current selection text
;		  widget.
;
;	YSIZE	- The height of the selection list in rows of characters.
;
;       FONT    - A font to use for all text in this widget.
;
;	UVALUE	- A user value for this widget.
;
; CALLS:
;	FILE_ACC	Determine file type and access.
;	EXTRACT_PATH	Extract directory path to current file from
;			full name (Routine in this file).
;       CW_LOADSTATE    Recover compound widget state.
;       CW_SAVESTATE    Save compound widget state.
;       See side effects for other related widget management routines.
;
; SIDE EFFECTS:
;	This widget uses a COMMON BLOCK: CW_FILESEL_BLK to hold the
;       widget state.
;
;       Three other routines are included which are used to manage the
;       widget;
;
;	FILESEL_SET_VAL
;	FILESEL_GET_VAL()
;	FILESEL_EVENT()
;
;	One extra utility routine is included;
;
;	EXTRACT_PATH()
;
; CATEGORY:
;	Compound Widget
;
; WRITTEN:
;       Andrew Bowen, Tessella Support Services plc, 13-May-1993
;
; MODIFIED:
;       1.1       	Andrew Bowen
;                       First release.
;	1.11		Tim Hammond
;			Added check to see whether state.list is an
;			array or not before attempts are made to assign
;			it an index - search for listcheck to see where
;			this has been done (previous modification history
;			unknown).
;	1.12		Richard Martin
;			Removed obsolete cw_savestate/loadstate statements.
;	1.13		Allan Whiteford
;			Do not display 'CVS' directories in the file
;                       selection widgets.
;
; VERSION:
;       1.1     25-May-1993
;	1.11	19-Dec-1995
;	1.12	09-01-02
;       1.13    15-07-2004
;
;-
;-----------------------------------------------------------------------------

FUNCTION extract_path, filename

		;**** Find the end of the directory path ***
  pathend = -1
  i = strlen(filename) - 1
  while (i gt 0 and pathend eq -1) do begin
    if strmid(filename,i,1) eq '/' then pathend = i
    i = i - 1
  end

		;**** Extract directory path ****
  if pathend gt 0 then begin
    path = strmid(filename,0,pathend)
  end else begin
    path = ''
  end

  RETURN, path
END

;-----------------------------------------------------------------------------

PRO filesel_set_val, id, value


		;**** Return to caller on error ****
  ON_ERROR, 2

		;**** Retrieve the state ****

    first_child = widget_info(id,/child)
    widget_control, first_child, get_uvalue=state, /no_copy

		;**** Tidy pathname and rootname to conform to ****
		;**** routine pathname convention	       ****
  if strtrim(value.root) eq '' then begin
    value.root = './'
  end else if strmid(value.root,strlen(value.root)-1,1) ne '/' then begin
    value.root = value.root+'/'
  end
  if strmid(value.file,0,1) eq '/' then begin
    value.file = strmid(value.file,1,strlen(value.file)-1)
  end

		;**** File name ****
  widget_control,state.fileid,set_value=value.file

		;**** See if the new 'file' is a file or a directory ****
  file = value.root+value.file
  file_acc,file,fileexist,read,write,execute,filetype

  if filetype ne 'd' then begin
		;**** Extract directory path ****
    file = extract_path(file)
  end

		;**** Get list of files in directory ****
  list = findfile(file)

		;**** If the current directory is not 'root' ****
		;**** add the '..' symbol to the list.       ****
  if strtrim(value.file) ne '' then begin
    if strtrim(list(0)) ne '' then begin
      list = ['..',list]
    end else begin
      list = ['..']
    end
  end

		;**** Do not display CVS directories ****
  if (where(list eq 'CVS'))[0] ne -1 then list=list[where(list ne 'CVS')]

		;**** Set selection list ****
  widget_control,state.selid,set_value=list

  fselval = {fselval, ROOT:value.root, FILE:value.file }
		;**** Recreate state structure to hold new list ****
  new_state =  { FILEID:state.fileid, SELID:state.selid, $
		 LIST:list, FSELVAL:fselval }

		;**** Save the new state ****

    widget_control, first_child, set_uvalue=new_state, /no_copy

END

;-----------------------------------------------------------------------------


FUNCTION filesel_get_val, id


		;**** Return to caller on error ****
  ON_ERROR, 2

		;**** Retrieve the state ****

    first_child = widget_info(id,/child)
    widget_control, first_child, get_uvalue=state


  RETURN, state.fselval

END

;-----------------------------------------------------------------------------

FUNCTION filesel_event, event

		;**** Base ID of compound widget ****
  base=event.handler

		;**** Retrieve the state ****

    first_child = widget_info(base,/child)
    widget_control, first_child, get_uvalue=state, /no_copy
    
		;**** Desensitise selection list to prevent ****
		;**** double click.                         ****
  widget_control,state.selid,sensitive=0

		;**** The default event is no file selected ****
  action = 'nofile'

		;************************
		;**** Process Events ****
		;************************
  CASE event.id OF

		;******************************
		;**** Selection list event ****
		;******************************
    state.selid: begin


		;**** See if the current 'file' is a file or directory ****
	file = state.fselval.root+state.fselval.file
	file_acc,file,fileexist,read,write,execute,filetype

	if n_elements(state.list) gt 1 then begin
	    listcheck = state.list(event.index)
	endif else begin
	    listcheck = state.list
	endelse
	if listcheck eq '..' then begin
			;*****************************
			;**** Move up a directory ****
			;*****************************
	  if filetype ne 'd' then begin
		;**** strip off two names for a file ***
	    state.fselval.file = extract_path(state.fselval.file)
	  end
	  state.fselval.file = extract_path(state.fselval.file)

	  widget_control,state.fileid,set_value=state.fselval.file
	  file = state.fselval.root+state.fselval.file
	  list = findfile(file)
	  if strtrim(state.fselval.file) ne '' then begin
	    if strtrim(list(0)) ne '' then begin
	      list = ['..',list]
	    end else begin
	      list = ['..']
	    end
	  end
		
		;**** Do not display CVS directories ****
          if (where(list eq 'CVS'))[0] ne -1 then list=list[where(list ne 'CVS')]

	  widget_control,state.selid,set_value=list

	end else begin
			;*******************************************
			;**** Down a directory or select a file ****
			;*******************************************
		;**** Determine the new file name ****
	  if filetype eq 'd' then begin
	    if strtrim(state.fselval.file) eq '' then begin
	      state.fselval.file = listcheck
	    end else begin
	      state.fselval.file = $
				state.fselval.file+'/'+listcheck
	    end
	  end else begin
	    path = extract_path(state.fselval.file)
	    if strtrim(path) eq '' then begin
	      state.fselval.file = listcheck
	    end else begin
	      state.fselval.file = path+'/'+listcheck
	    end
	  end

		;**** Display new name ****
	  widget_control,state.fileid,set_value=state.fselval.file

		;**** See if the new 'file' is a file or directory ****
	  file = state.fselval.root+state.fselval.file
	  file_acc,file,fileexist,read,write,execute,filetype
  
		;**** Update list if new selection is a directory ****
	  if filetype eq 'd' then begin
	    list = findfile(file)
	    if strtrim(state.fselval.file) ne '' then begin
	      if strtrim(list(0)) ne '' then begin
		list = ['..',list]
	      end else begin
		list = ['..']
	      end
	    end
	    
	       ;**** Do not display CVS directories ****
            if (where(list eq 'CVS'))[0] ne -1 then list=list[where(list ne 'CVS')]

	    widget_control,state.selid,set_value=list
	  end else begin
	    list = state.list
	    action = 'newfile'
	  end

	end
      end

    ELSE:

  ENDCASE

		;**** recreate state structure to fit in new list ****
  new_state =  { FILEID:state.fileid, SELID:state.selid, $
		 LIST:list, FSELVAL:state.fselval }


		;**** Re-sensitise selection list ****
  widget_control,state.selid,/sensitive

		;**** Save the new state structure ****

   widget_control, first_child, set_uvalue=new_state, /no_copy

		;**** Generate event indicating if file is selected ****
  RETURN, {ID:base, TOP:event.top, HANDLER:0L, ACTION:action}

END

;-----------------------------------------------------------------------------

FUNCTION cw_file_select, topparent, VALUE=value, TITLE=title, $
		XSIZE=xsize, YSIZE=ysize, FONT=font, UVALUE=uvalue


  IF (N_PARAMS() LT 1) THEN MESSAGE, 'Must specify topparent for cw_file_select'

		;**** Set defaults for keywords ****
  IF NOT (KEYWORD_SET(value)) THEN begin
    fselval = {fselval, ROOT:'./', FILE:''}
  END ELSE BEGIN
    fselval = {fselval, ROOT:value.root, FILE:value.file }
    if strtrim(fselval.root) eq '' then begin
      fselval.root = './'
   end else if strmid(fselval.root,strlen(fselval.root)-1,1) ne '/' then begin
      fselval.root = fselval.root+'/'
    end
    if strmid(fselval.file,0,1) eq '/' then begin
      fselval.file = strmid(fselval.file,1,strlen(fselval.file)-1)
    end
  END

  IF NOT (KEYWORD_SET(title)) THEN title = 'Data File'
  IF NOT (KEYWORD_SET(xsize)) THEN xsize = 60
  IF NOT (KEYWORD_SET(ysize)) THEN ysize = 15
  IF NOT (KEYWORD_SET(font)) THEN font = ''
  IF NOT (KEYWORD_SET(uvalue))  THEN uvalue = 0

		;**** Create the main base for the widget ****
  parent = WIDGET_BASE(topparent, UVALUE = uvalue, $
		EVENT_FUNC = "filesel_event", $
		FUNC_GET_VALUE = "filesel_get_val", $
		PRO_SET_VALUE = "filesel_set_val", $
		/ROW)

		;**** Create base to hold the value of state ****

    first_child = widget_base(parent)

    cwid = widget_base(first_child,/row)

		;**** Title ****
  rc = widget_label(cwid,value=title,font=font)

  base = widget_base(cwid,/column)

		;**** File name ****
  fileid = widget_text(base,value=fselval.file,xsize=xsize,font=font)

		;**** Selection list ****
  selid = widget_list(base,ysize=ysize,font=font)

		;**** See if the current 'file' is a file ****
		;**** or a directory.                     ****
  listdir = fselval.root+fselval.file
  file_acc,listdir,fileexist,read,write,execute,filetype

  if filetype ne 'd' then begin
		;**** Extract directory path ****
    listdir = extract_path(listdir)
  end

		;**** Get list of files in directory ****
  list = findfile(listdir)

		;**** If the current directory is not 'root' ****
		;**** add the '..' symbol to the list.       ****
  if strtrim(fselval.file) ne '' then begin
    if strtrim(list(0)) ne '' then begin
      list = ['..',list]
    end else begin
      list = ['..']
    end
  end

		;**** Do not display CVS directories ****
  if (where(list eq 'CVS'))[0] ne -1 then list=list[where(list ne 'CVS')]

		;**** Set selection list ****
  widget_control,selid,set_value=list

		;**** Create state structure ****
  new_state =  { FILEID:fileid, SELID:selid, LIST:list, FSELVAL:fselval }

		;**** Save initial state structure ****

    widget_control, first_child, set_uvalue=new_state, /no_copy

  RETURN, parent

END
