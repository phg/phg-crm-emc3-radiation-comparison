; Copyright (c) 1996, Strathclyde University.
; SCCS info: Module @(#)$Header: /home/adascvs/idl/adaslib/gui/cw_demap_tgridsel.pro,v 1.2 2004/07/06 13:05:23 whitefor Exp $ Date $Date: 2004/07/06 13:05:23 $
;+
; PROJECT:
;       ADAS IBM MVS to UNIX conversion
;
; NAME:
;	CW_DEMAP_TGRIDSEL()
;
; PURPOSE:
;	Produces a widget for DEMAP temperature grid selection
;
; EXPLANATION:
;	This function declares a compound widget consisting of the
;       temperature grid selection widget.
;       This widget includes a 'Cancel' button and a 'Done' button.
;	The compound widget cw_demap_ranges.pro
;	included in this file are self managing.  This widget only
;	handles events from the 'Done' and 'Cancel' buttons.
;
;	This widget only generates events for the 'Done' and 'Cancel'
;	buttons.
;
; USE:
;	This widget is specific to DEMAP, tgrid_sel.pro	for use.
;
; INPUTS:
;	PARENT	- Long integer; ID of parent widget.
;
; OPTIONAL INPUTS:
;	None.
;
; OUTPUTS:
;	The return value of this function is the ID of the declared widget.
;
; OPTIONAL OUTPUTS:
;	None.
;
; KEYWORD PARAMETERS:
;
;	VALUE	- A structure which determines the initial settings of
;		  the output options widget. The default value is;
;
;                 { grselval, $
;                   ntemp : '', tmin : '', tmax : '' ,$
;                   step : '' , logtmin : '', logtmax : '' ,$
;                   grpmess : '' }
;
;	UVALUE	- A user value for this widget.
;
;	FONT	- String; the name of a font to be used for all text in
;		  this widget.  Default to system default font.
;
; CALLS:
;	CW_DEMAP_RANGES	Input temperature grid parameters.
;
; SIDE EFFECTS:
;
;
; CATEGORY:
;	Compound Widget
;
; WRITTEN:
;       Alessandro Lanzafame, University of Strathclyde, unknown
;
; MODIFIED:
;	1.0	Alessandro Lanzafame
;		First Release
;	1.1	William Osborn
;		Put under SCCS control
;	1.2	William Osborn
;		Removed xsize=n keyword from widget_label command.
;		The dynlabel procedure does not override these
;		assignments and they were taken as being values in pixels
;		on some machines.
; VERSION:
;	1.0	23-04-96
;	1.1	07-05-96
;	1.2	01-08-96
;
;-
;-----------------------------------------------------------------------------

FUNCTION tgridsel_get_val, id

;---------------------------------------------------------------------------
;	Return to caller on error
;---------------------------------------------------------------------------

ON_ERROR, 2

;---------------------------------------------------------------------------
;	Retrieve the state
;---------------------------------------------------------------------------

parent=widget_info(id,/parent)
widget_control, parent, get_uvalue=state, /no_copy

;---------------------------------------------------------------------------
;	Get temperature grid parameters
;---------------------------------------------------------------------------

widget_control,state.grpid,get_value=grselval

    os = { tgrid_set, 							$
           NTEMP:grselval.ntemp, TMIN:grselval.tmin, TMAX:grselval.tmax,$
           STEP:grselval.step,                                          $
           LOGTMIN:grselval.logtmin, LOGTMAX:grselval.logtmax,          $
           GRPMESS:grselval.grpmess                                     $
       }

;---------------------------------------------------------------------------
;	Return the state
;---------------------------------------------------------------------------

widget_control, parent, set_uvalue=state, /no_copy

RETURN, os 

END

;-----------------------------------------------------------------------------

FUNCTION tgridsel_event, event

COMMON cwtgridsel_blk, cwoval

;---------------------------------------------------------------------------
;	Base ID of compound widget
;---------------------------------------------------------------------------

parent=event.top

;---------------------------------------------------------------------------
;	Retrieve the state
;---------------------------------------------------------------------------

widget_control, parent, get_uvalue=state, /no_copy

;---------------------------------------------------------------------------
;	Clear previous messages
;---------------------------------------------------------------------------

widget_control,state.messid,set_value=' '

;---------------------------------------------------------------------------
;	Process events
;---------------------------------------------------------------------------

CASE event.id OF

;---------------------------------------------------------------------------
;	Cancel button
;---------------------------------------------------------------------------

state.cancelid: new_event = {ID:parent, TOP:event.top, 		$
	    		     HANDLER:0L, ACTION:'Cancel'}

;---------------------------------------------------------------------------
;	Done button
;---------------------------------------------------------------------------

state.doneid: begin

;---------------------------------------------------------------------------
;	Return the state before checking can start 
;	with the get_value keyword.                
;---------------------------------------------------------------------------

          widget_control, parent, set_uvalue=state, /no_copy

;---------------------------------------------------------------------------
;	Check for errors in the input
;---------------------------------------------------------------------------

	  error = 0

;---------------------------------------------------------------------------
;	Get a copy of the widget value
;---------------------------------------------------------------------------

	  widget_control,event.handler,get_value=os
	
;---------------------------------------------------------------------------
;	Check for widget error messages
;---------------------------------------------------------------------------

	  if strtrim(os.grpmess) ne '' then error=1

;---------------------------------------------------------------------------
;	Retrieve the state 
;---------------------------------------------------------------------------

          widget_control, parent, get_uvalue=state, /no_copy

	  if error eq 1 then begin
	    widget_control,state.messid,set_value= 			$

		'**** Error in output settings ****'
	    new_event = 0L
	  end else begin
	    new_event = {ID:parent, TOP:event.top, HANDLER:0L, ACTION:'Done'}
	  end

        end

    ELSE: new_event = 0L

ENDCASE

;---------------------------------------------------------------------------
;	Return the state
;---------------------------------------------------------------------------

widget_control, parent, set_uvalue=state, /no_copy

RETURN, new_event
END

;-----------------------------------------------------------------------------

FUNCTION cw_demap_tgridsel, parent, VALUE=value, UVALUE=uvalue, FONT=font

COMMON cwtgridsel_blk, cwoval

IF (N_PARAMS() EQ 0) THEN MESSAGE, 'Must specify parent for cw_demap_tgridsel'
ON_ERROR, 2					;return to caller

cwoval=value

;---------------------------------------------------------------------------
;	Set defaults for keywords
;---------------------------------------------------------------------------

IF NOT (KEYWORD_SET(uvalue)) THEN uvalue = 0
IF NOT (KEYWORD_SET(font)) THEN font = ''
IF NOT (KEYWORD_SET(value)) THEN begin
   os = {tgrid_set,                             $
         ntemp:'', tmin:'', tmax:'' ,           $
         step : '' , logtmin : '', logtmax : '',$
         grpmess : '' }
ENDIF ELSE BEGIN
   os = {tgrid_set,              $
         NTEMP:value.ntemp,      $
         TMIN:value.tmin,        $
         TMAX:value.tmax,        $
         STEP:value.step,        $
         LOGTMIN:value.logtmin,  $
         LOGTMAX:value.logtmax,  $ 
         GRPMESS:value.grpmess}

ENDELSE

;---------------------------------------------------------------------------
;	create base widget
;---------------------------------------------------------------------------

cwid = widget_base( parent, UVALUE = uvalue, 				$
			EVENT_FUNC = "tgridsel_event", 			$
			FUNC_GET_VALUE = "tgridsel_get_val", 		$
			/COLUMN)


;---------------------------------------------------------------------------
;	Widget for temperature grid selection
;---------------------------------------------------------------------------

grselval = os

base = widget_base(cwid,/row,/frame)

grpid = cw_demap_ranges(base,VALUE=grselval, FONT=font)

;---------------------------------------------------------------------------
;	Error message
;---------------------------------------------------------------------------

messid = widget_label(cwid,value=' ',font=font)

;---------------------------------------------------------------------------
;	add the exit buttons
;---------------------------------------------------------------------------

base = widget_base(cwid,/row)
cancelid = widget_button(base,value='Cancel',font=font)
doneid = widget_button(base,value='Done',font=font)
  
;---------------------------------------------------------------------------
;	create a state structure for the pop-up window.
;---------------------------------------------------------------------------

new_state = { GRPID:grpid, CANCELID:cancelid, DONEID:doneid, MESSID:messid }

;---------------------------------------------------------------------------
;	Save initial state structure
;---------------------------------------------------------------------------

widget_control, parent, set_uvalue=new_state, /no_copy
RETURN, cwid

END

