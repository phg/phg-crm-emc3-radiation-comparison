; Copyright (c) 1995, Strathclyde University.
; SCCS info: Module @(#)$Header: /home/adascvs/idl/adaslib/gui/cw_adas_root.pro,v 1.11 2004/07/06 13:02:05 whitefor Exp $	Date $Date: 2004/07/06 13:02:05 $
;
;+
; PROJECT:
;       ADAS IBM MVS to DEC ULTRIX conversion
;
; NAME:
;	CW_ADAS_ROOT()
;
; PURPOSE:
;	Root directory selection for use in input file selection.
;
; EXPLANATION:
;	This function creates a compound widget consisting of;
;	1. A text widget with the current root directory
;	   path name.
;	2. A button to set the root to a user default root.
;	3. A button to set the root to a central default root.
;	4. A toggle button to enable/disable direct keyboard entry
;	   of the root.
;	This widget performs is own checking and validation of the
;	path and issues its own error messages.
;
;	This widget generates events as the user interacts with it.
;	The event structure returned is;
;	{ID:0L, TOP:0L, HANDLER:0L, ACTION:''}
;	ACTION has one of two values indicating the state of the root
;	selection process, either 'rootedit' when the root is being
;	edited and is not verified or 'newroot' when a new verified
;	root has been selected.
;
;	To operate the widget the user can press the 'User Data' or
;	'Central Data' buttons to copy those default roots to the text
;	widget showing the current root.  Alternatively the 'Edit Path Name'
;	toggle can be switched on to allow direct keyboard entry of the
;	current root.  To complete keyboard entry the user either unsets
;	the toggle button or presses the return key.  Whenever a new root
;	name is set, either with defaults buttons or by direct entry,
;	the path name is checked.  Any error is flagged with a message
;	and the widget is automatically put into 'Edit Path Name' mode
;	until a valid path is entered.  Every time the path is changed
;	an event is generated as described above.  This is to notify
;	any dependent IDL routines of the change.
;
; USE:
;	See cw_adas_in.pro for an example of use.
;
; INPUTS:
;       PARENT   - The ID of the parent widget.
;
; OPTIONAL INPUTS:
;	None.
;
; OUTPUTS:
;       The return value of this function is the ID of the compound
;       widget which is of type LONG integer.
;
; OPTIONAL OUTPUTS:
;       None
;
; KEYWORD PARAMETERS:
;	VALUE	- A structure which must be;
;		  {ROOTPATH:'', CENTROOT:'', USERROOT:'' }
;		  The elements of the structure are as follows;
;
;		  ROOTPATH - Current data directory e.g '/usr/fred/adas/'
;		  CENTROOT - Default central data store e.g '/usr/adas/'
;		  USERROOT - Default user data store e.g '/usr/fred/adas/'
;
;		  Path names may be supplied with or without the trailing
;		  '/'.  This widget will add this where required so that
;		  USERROOT will always end in '/' on output.  The default
;		  value is;
;		  {ROOTPATH:'./', CENTROOT:'', USERROOT:''}
;		  i.e ROOTPATH is set to the user's current directory.
;
;       FONT     - A font to use for all text in this widget.
;
;	UVALUE	 - A user value for this widget.
;
;	NOCENTRAL- Setting this keyword enables the central and user
;		   switching buttons to be hidden (e.g. as in adas101
;		   where they are not appropriate). The default value
;		   is not set.
;
; CALLS:
;	FILE_ACC	Used to verify path name.
;       CW_LOADSTATE    Recover compound widget state.
;       CW_SAVESTATE    Save compound widget state.
;       See side effects for other related widget management routines.
;
; SIDE EFFECTS:
;	This widget uses a COMMON BLOCK: CW_ROOT_BLK to hold the
;       widget state.
;
;       Three other routines are included which are used to manage the
;       widget;
;
;	ROOT_SET_VAL
;	ROOT_GET_VAL()
;	ROOT_EVENT()
;
; CATEGORY:
;	Compound Widget
;
; WRITTEN:
;       Andrew Bowen, Tessella Support Services plc, 13-May-1993
;
; MODIFIED:
;       1.1     Andrew Bowen 
;               First release.
;	1.9	Tim Hammond
;		(Previous history unknown)
;		Added NOCENTRAL keyword to allow the user/central
;		data buttons to be hidden if required.
;	1.10	William Osborn
;		Removed xsize=n keyword from widget_label command.
;		The dynlabel procedure does not override these
;		assignments and they were taken as being values in pixels.
;       1.11    Martin O'Mullane
;               Renamed to cw_adas_infile to get rid of the very obsolete
;               cw_loadstate and cw_savestate calls. Introduced YSIZE as
;               a keyword which defaults to the standard 15 if it is not
;               set. The major functionality addition is the EVENT_TUNCT
;               keyword which allows for an optional user supplied event
;               handler. Added following the cw_bgroup example in
;               ....rsi/idl/lib/cw_bgroup.pro.
;
; VERSION:
;       1.1	25-May-1993
;	1.9	13-Jul-1995
;	1.10	01-08-96
;	1.11	10-11-99
;-
;-----------------------------------------------------------------------------

PRO root_set_val, id, value

		;**** Return to caller on error ****
  ON_ERROR, 2

		;**** Retrieve the state ****

    first_child = widget_info(id,/child)
    widget_control, first_child, get_uvalue=state, /no_copy


		;**** Tidy path name to conform to routine convention ****
  if strtrim(value.rootpath) eq '' then begin
    value.rootpath = './'
  end else if strmid(value.rootpath,strlen(value.rootpath)-1,1) ne $
								'/' then begin
    value.rootpath = value.rootpath+'/'
  end
		;**** Copy new rootname to widget ****
  widget_control,state.rootid,set_value=value.rootpath

		;**** Copy the new value to state structure ****
  state.rootval.rootpath = value.rootpath
  state.rootval.centroot = value.centroot
  state.rootval.userroot = value.userroot

		;**** Save the new state ****
                
    widget_control, first_child, set_uvalue=state, /no_copy

END

;-----------------------------------------------------------------------------


FUNCTION root_get_val, id


		;**** Return to caller on error ****
  ON_ERROR, 2

		;**** Retrieve the state ****
                
    first_child = widget_info(id, /child)
    widget_control, first_child, get_uvalue=state



  RETURN, state.rootval

END

;-----------------------------------------------------------------------------

FUNCTION root_event, event

		;**** Base ID of compound widget ****
  
  base=event.handler

		;**** Retrieve the state ****

  first_child = widget_info(base, /child)
  widget_control, first_child, get_uvalue = state, /no_copy

  nocentral = state.nocentral

		;************************
		;**** Process Events ****
		;************************
  CASE event.id OF


    state.centid: begin
          if (nocentral eq 0) then begin
	      widget_control,state.rootid,set_value=state.rootval.centroot
	      state.rootval.rootpath = state.rootval.centroot
	      if state.editon eq 1 then begin
	        action = ''
	      end else begin
	        action = 'newroot'
	      end
          endif
	end

    state.userid: begin
          if (nocentral eq 0) then begin
	      widget_control,state.rootid,set_value=state.rootval.userroot
	      state.rootval.rootpath = state.rootval.userroot
	      if state.editon eq 1 then begin
	        action = ''
	      end else begin
	        action = 'newroot'
	      end
          endif
	end

    state.editid: if state.editon eq 0 then begin
	  state.editon = 1
	  widget_control,state.rootid,/sensitive,/input_focus
	  action = 'rootedit'
	end else begin
	  state.editon = 0
	  widget_control,state.rootid,sensitive=0
	  widget_control,state.rootid,get_value=rootpath
	  state.rootval.rootpath = rootpath(0)
	  action = 'newroot'
	end

    state.rootid: begin
	  state.editon = 0
	  widget_control,state.rootid,sensitive=0
	  widget_control,state.editid,set_button=0
	  widget_control,state.rootid,get_value=rootpath
	  state.rootval.rootpath = rootpath(0)
	  action = 'newroot'
	end

    ELSE:

  ENDCASE

		;**** Clear any existing error message ****
  widget_control,state.messid,set_value=' '

		;**** Check if the supplied name is okay     ****
		;**** Force edit mode in the event of error. ****
		;**** Tidy path name if it is okay.          ****
  if action eq 'newroot' then begin
    file_acc,state.rootval.rootpath,fileexist,read,write,execute,filetype
    if fileexist ne 1 then begin
      error = 1
      message = 'Error: The supplied name is invalid.'
      widget_control,state.messid,set_value=message
      state.editon = 1
      widget_control,state.editid,set_button=1
      widget_control,state.rootid,/sensitive,/input_focus
      action = 'rootedit'
    end else begin
      if strtrim(state.rootval.rootpath) eq '' then begin
        state.rootval.rootpath = './'
      end else if $
	  strmid(state.rootval.rootpath,strlen(state.rootval.rootpath)-1,1) $
							ne '/' then begin
        state.rootval.rootpath = state.rootval.rootpath+'/'
      end
      widget_control,state.rootid,set_value=state.rootval.rootpath
    end
  end

		;**** Save the new state structure ****

;  if action ne '' then begin
;    RETURN, {ID:base, TOP:event.top, HANDLER:0L, ACTION:action}
;  end else begin
;    RETURN, 0L
;  end

    efun = state.efun
    widget_control, first_child, set_uvalue=state, /no_copy
  
; Return a struct or pass to a user supplied event handler?

  ret = {ID:base, TOP:event.top, HANDLER:0L, ACTION:action}
  if efun NE '' then return, call_function(efun,ret) else return, ret

END

;-----------------------------------------------------------------------------

FUNCTION cw_adas_root, parent, VALUE=value, FONT=font, UVALUE=uvalue,	$
                       NOCENTRAL=nocentral, EVENT_FUNCT=efun


  IF (N_PARAMS() LT 1) THEN MESSAGE, 'Must specify PARENT for cw_adas_root'

		;**** Set defaults for keywords ****
  IF NOT (KEYWORD_SET(value)) THEN BEGIN
    rootval = {rootval, ROOTPATH:'./', $
			CENTROOT:'', $
			USERROOT:'' }
  END ELSE BEGIN
    rootval = {rootval, ROOTPATH:value.rootpath, $
			CENTROOT:value.centroot, $
			USERROOT:value.userroot }
    if strtrim(rootval.rootpath) eq '' then begin
      rootval.rootpath = './'
    end else if strmid(rootval.rootpath,strlen(rootval.rootpath)-1,1) ne $
								'/' then begin
      rootval.rootpath = rootval.rootpath+'/'
    end
  END
  IF NOT (KEYWORD_SET(font)) THEN font = ''
  IF NOT (KEYWORD_SET(uvalue))  THEN uvalue = 0
  IF NOT (KEYWORD_SET(nocentral)) THEN nocentral = 0

		;**** Create the main base for the widget ****
  
  frameflag = 0
  if (nocentral ne 0) then frameflag = 1
  main = WIDGET_BASE(parent, UVALUE = uvalue, 				$
		EVENT_FUNC = "root_event", 				$
		FUNC_GET_VALUE = "root_get_val", 			$
		PRO_SET_VALUE = "root_set_val", 			$
		/COLUMN, frame=frameflag)

                ;******************************************************
                ;**** Create a dummy widget just to hold value of *****
                ;**** "state" variable so as not to get confused  *****
                ;**** with any other values. Adopt IDL practice   *****
                ;**** of using first child widget                 *****
                ;******************************************************

  first_child = widget_base(main)
  
  cwid = widget_base(first_child,/column)

		;**** Root path name for data ****
  base = widget_base(cwid,/row)
  rc = widget_label(base,value='Data Root ',font=font)
  rootid = widget_text(base,value=rootval.rootpath,/edit,xsize=40,font=font)
  widget_control,rootid,sensitive=0

		;**** Base for all buttons ****
  base = widget_base(cwid,/row)

		;**** Default path name buttons ****
  rc = widget_base(base,/row)
  rc2 = widget_base(rc, /row)
  rc3 = widget_base(rc, /row)
  centid = widget_button(rc2,value='Central Data',font=font)
  userid = widget_button(rc2,value='User Data',font=font)
  blank = widget_label(rc3,value='',font=font)
  if (nocentral eq 0) then begin
      widget_control, centid, map=1
      widget_control, blank, map=0
  endif else begin
      widget_control, centid, map=0
      widget_control, blank, map=1
  endelse
 

		;**** Pathname override switch ****
  rc = widget_base(base,/row,/nonexclusive)
  editid = widget_button(rc,value='Edit Path Name',font=font)

		;**** Message ****
  messid = widget_label(cwid,value=' ',font=font)

		;**** Create state structure ****
  if n_elements(efun) le 0 then efun = ''
  new_state = { efun      : efun,                               $
                ROOTID:rootid, CENTID:centid, USERID:userid, $
		EDITID:editid, MESSID:messid, EDITON:0, $
		ROOTVAL:rootval, FONT:font, NOCENTRAL:nocentral }

		;**** Save initial state structure ****
  widget_control, first_child, set_uvalue=new_state,/no_copy

  RETURN, main

END
