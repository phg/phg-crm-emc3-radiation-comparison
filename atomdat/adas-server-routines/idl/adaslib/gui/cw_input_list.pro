; Copyright (c) 1995 Strathclyde University .
; SCCS info: Module @(#)$Header: /home/adascvs/idl/adaslib/gui/cw_input_list.pro,v 1.5 2004/07/06 13:07:10 whitefor Exp $ Date $Date: 2004/07/06 13:07:10 $
;
;+
; PROJECT:
;       ADAS IBM MVS to DEC UNIX conversion
;
; NAME:
;	CW_INPUT_LIST()
;
; PURPOSE:
;	Provides a simple list of inputs each with its own label.
;	
;
; EXPLANATION:
;	This compound widget consists of a multi-line title and a
;	column of text widget inputs.  Each text widget input has its
;	own text label.  The title and the labels are all left justified
;	in the widget and the text input widgets will all align in a
;	column.  By careful selection of the multi-line title and the
;	labels a table effect can be achieved with a single input column
;	to the right.  By default the inputs are just text values but
;	the caller can specify that they should represent integer or
;	floating point values.
;
;	Click on a text widget to direct keyboard input to that input.
;	When the user presses the return key the keyboard input is
;	automatically transfered to the next input.  Also the values
;	entered are checked to see if they are legal numeric values as
;	specified by the caller.
;
;	When the GET_VALUE call is used, the widget checks that all 
;	inputs have been supplied and also that they are legal numbers
;	if required by the caller. The caller can test to see if the
;	widget input is in error by testing to see if the message
;	returned in the widget value structure is not the null string.
;
;	This widget does not generate any events.
;
; USE:
;	An example of usage;
;
;	title = 'Enter data values for each level below'
;	labels = 'Value for level '+strtrim(string(indgen(10)))
;	font='courier_bold14'
;	base = widget_base(/column)
;	cwid = cw_input_list(base,labels=labels,title=title,font=font,/integer)
;	rc = widget_button(base,value='Done')
;	widget_control,base,/realize
;	rc = widget_event()
;	widget_control,cwid,get_value=value
;	if strtrim(value.message) ne '' then begin
;	  print,'Error:'+value.message
;	end else begin
;	  print,'Input values: ',value.inputs
;	end
;
; INPUTS:
;       PARENT   - The ID of the parent widget.
;
; OPTIONAL INPUTS:
;	None.
;
; OUTPUTS:
;       The return value of this function is the ID of the compound
;       widget which is of type LONG integer.
;
; OPTIONAL OUTPUTS:
;       None
;
; KEYWORD PARAMETERS:
;	You should supply at least one of the VALUE or LABELS keywords.
;	If both VALUE and LABELS are specified then the number of labels
;	and the number of inputs must be the same.  If only of the
;	keywords is supplied then that array defines the number of
;	inputs.
;
;	VALUE	- A structure describing the value of the widget;
;		   {INPUTS:strarr(), MESSAGE:''}
;
;		   INPUTS	Array of strings which are the input
;				values in the input list.
;		   MESSAGE	Error message for input.
;
;	LABELS	- A string array; One element for each input.
;		  Default ['Value 1','Value 2' etc...]
;
;	TITLE	- A string array; A title for the widget, one element
;		  for each line of the title.
;
;	FLOAT	- Set this keyword to specify that inputs should
;		  represent floating point numbers.  Defaults to no
;		  checking on numeric representation.  If both
;		  /FLOAT and /INTEGER are supplied INTEGER takes
;		  priority.
;
;	INTEGER	- Set thie keyword to specify that inputs should
;		  represent integer numbers.  Defaults to no checking
;		  on numeric representation.
;
;       FONT    - A font to use for all text in this widget.
;
;	UVALUE	- A user value for this widget.
;
; CALLS:
;       CW_LOADSTATE    Recover compound widget state.
;       CW_SAVESTATE    Save compound widget state.
;	NUM_CHK		Called fron IN_LIST_GET_VAL, check numeric values.
;       See side effects for other related widget management routines.
;
; SIDE EFFECTS:
;	This widget uses a COMMON BLOCK: CW_RNG_BLK to hold the
;       widget state.
;
;       Three other routines are included which are used to manage the
;       widget;
;
;	IN_LIST_SET_VAL
;	IN_LIST_GET_VAL()
;	IN_LIST_EVENT()
;
; CATEGORY:
;	Compound Widget
;
; WRITTEN:
;       Andrew Bowen, Tessella Support Services plc, 14-Jun-1993
;
; MODIFIED:
;       Version 1       Andrew Bowen    14-Jun-1993
;                       First release.
;	1.4	William Osborn
;		Removed xsize=n keyword from widget_label commands.
;		The dynlabel procedure does not override these
;		assignments and they were taken as being values in pixels
;		on some machines.
;	1.5	Richard Martin
;		Remove obsolete cw_loadstate/savestate statements.
;
; VERSION:
;       1   14-Jun-1993
;	1.4	01-08-96
;	1.5	10-01-02
;-
;-----------------------------------------------------------------------------

PRO in_list_set_val, id, value

  first_child = widget_info(id,/child)
  widget_control, first_child, get_uvalue=state, /no_copy

		;**** Return to caller on error ****
  ON_ERROR, 2

		;**** Retrieve the state ****

  first_child = widget_info(id,/child)
  widget_control, first_child, get_uvalue=state, /no_copy
  
		;**** copy value to widget ****
  for i = 0, state.numin-1 do begin
    widget_control,state.inid(i),set_value=value.inputs(i)
  end

		;**** Copy message to widget ****
  if strtrim(value.message) ne '' then begin
    message = value.message
  end else begin
    message = ' '
  end
  widget_control,state.messid,set_value=message


		;**** Copy the new value to state structure ****
  state.inputs = value.inputs
  state.message = value.message

		;**** Save the new state ****

  widget_control, first_child, set_uvalue=state, /no_copy

END

;-----------------------------------------------------------------------------


FUNCTION in_list_get_val, id


		;**** Return to caller on error ****
  ON_ERROR, 2

		;**** Retrieve the state ****

  first_child = widget_info(id,/child)
  widget_control, first_child, get_uvalue=state, /no_copy

		;**** Clear existing error message ****
  widget_control,state.messid,set_value=' '
  state.message = ''

		;**** Get input values ****
  for i = 0, state.numin-1 do begin
    widget_control,state.inid(i),get_value=input
    state.inputs(i) = input(0)
  end

  error = 0

		;**** Check for missing values ****
  if error eq 0 then begin
    for i = 0, state.numin-1 do begin
      if strtrim(state.inputs(i)) eq '' then error = 1
    end
    if error eq 1 then state.message = '**** Missing Value ****'
  end

		;**** Check numeric values ****
  if error eq 0 and (state.integer gt 0 or state.float gt 0) then begin

    if state.integer gt 0 then begin
      message = '**** Illegal Integer Value ****'
    end else begin
      message = '**** Illegal Float Value ****'
    end

    error = 0
    i = 0
    while (error eq 0 and i lt state.numin) do begin

      error = num_chk(state.inputs(i),integer=state.integer)
      if error eq 1 then begin
        widget_control,state.inid(i),/input_focus
	state.message = message
      end
      i = i + 1

    end
  end

		;**** display error message in widget ****
  if error eq 1 then $
	widget_control,state.messid,set_value=state.message
	

		;**** Reset the value of state variable ****
  inputs=state.inputs
  message=state.message
  widget_control, first_child, set_uvalue=state, /no_copy
	

  RETURN, {INPUTS:inputs, MESSAGE:message}

END

;-----------------------------------------------------------------------------

FUNCTION in_list_event, event


		;**** Base ID of compound widget ****
  base=event.handler

		;**** Retrieve the state ****

  first_child = widget_info(base, /child)
  widget_control, first_child, get_uvalue = state, /no_copy

		;**** Clear existing error message ****
  widget_control,state.messid,set_value=' '

		;************************
		;**** Process Events ****
		;************************

		;**** identify which input generated event ****
  input = where(state.inid eq event.id)
  input = input(0)

		;**** switch input_focus to next text widget ****
  if input+1 eq state.numin then input=0 else input=input+1
  widget_control,state.inid(input),/input_focus


		;**** Check numeric values ****
  if state.integer gt 0 or state.float gt 0 then begin

    if state.integer gt 0 then begin
      message = '**** Illegal Integer Value ****'
    end else begin
      message = '**** Illegal Float Value ****'
    end

    error = 0
    i = 0
    while (error eq 0 and i lt state.numin) do begin

      widget_control,state.inid(i),get_value=num
      if strtrim(num(0)) ne '' then begin
        error = num_chk(num(0),integer=state.integer)
        if error eq 1 then begin
          widget_control,state.messid,set_value=message
          widget_control,state.inid(i),/input_focus
        end
      end
      i = i + 1

    end
  end


		;**** Save the new state structure ****

  widget_control, first_child, set_uvalue=state, /no_copy

  RETURN, 0L
END

;-----------------------------------------------------------------------------

FUNCTION cw_input_list, topparent, VALUE=value, LABELS=labels, TITLE=title, $
			FLOAT=float, INTEGER=integer, FONT=font, UVALUE=uvalue


		;**** Return to caller on error ****
  ON_ERROR, 2


  IF (N_PARAMS() LT 1) THEN MESSAGE, 'Must specify PARENT for cw_input_list'

		;**** Number of labels specified ****
  numlab = 0
  IF KEYWORD_SET(labels) THEN begin
    numlab = size(labels)
    if numlab(0) gt 0 then numlab=numlab(1) else numlab=1
  end

		;**** Number of values specified ****
  numin = 0
  IF KEYWORD_SET(value) THEN begin
    numin = size(value.inputs)
    if numin(0) gt 0 then numin=numin(1) else numin=1
  end

		;**** Test for consistency ****
  if numlab gt 0 and numin gt 0 then begin
    if numlab ne numin then $
	MESSAGE, 'LABELS and VALUE.INPUTS arrays must be the same length.'
  end else begin
    numin = max([numin,numlab])
  end

		;**** Set defaults for keywords ****
  IF NOT (KEYWORD_SET(value)) THEN begin
				inputs = strarr(numin)
				message = ' '
			      end else begin
				inputs = value.inputs
				message = value.message
			      end
  IF NOT (KEYWORD_SET(labels)) THEN labels = 'Value '+string(indgen(numin))
  IF NOT (KEYWORD_SET(title)) THEN title = ''
  IF NOT (KEYWORD_SET(float)) THEN float = 0
  IF NOT (KEYWORD_SET(integer)) THEN integer = 0
  IF NOT (KEYWORD_SET(font)) THEN font = ''
  IF NOT (KEYWORD_SET(uvalue))  THEN uvalue = 0

		;**** Blanks for making up string lengths ****
  blanks = '                                                            '

		;**** Find maximum label length ****
  lablen = max(strlen(labels))

		;**** Find number of title lines and maximum length ****
  numhead = size(title)
  if numhead(0) gt 0 then numhead=numhead(1) else numhead=1
  titlen = max(strlen(title))

		;**** Create the main base for the widget ****
  parent = WIDGET_BASE(topparent, UVALUE = uvalue, $
		     EVENT_FUNC = "in_list_event", $
		     FUNC_GET_VALUE = "in_list_get_val", $
		     PRO_SET_VALUE = "in_list_set_val", $
		     /COLUMN)

		;**** Create base to hold the value of state ****

  first_child = widget_base(parent)

  cwid = widget_base(first_child,/column)

		;**** Title ****
  base = widget_base(cwid,/column)
  for i = 0, numhead-1 do begin
    line = title(i)+strmid(blanks,0,titlen-strlen(title(i)))
    rc = widget_label(base,value=line,font=font)
  end

		;**** Labels and input values ****
  inid = lonarr(numin)
  for i = 0, numin-1 do begin
    base = widget_base(cwid,/row)
    line = labels(i)+strmid(blanks,0,lablen-strlen(labels(i)))
    rc = widget_label(base,value=line,font=font)
    inid(i) = widget_text(base,value=inputs(i),/editable,xsize=8,font=font)
  end

		;**** Message ****
  messid = widget_label(cwid,value=' ',font=font)

		;**** Create state structure ****
  new_state = { INID:inid, INPUTS:inputs, MESSAGE:message, $
		FLOAT:float, INTEGER:integer, $
		NUMIN:numin, MESSID:messid }

		;**** Save initial state structure ****

  widget_control, first_child, set_uvalue=new_state, /no_copy

  RETURN, parent

END
