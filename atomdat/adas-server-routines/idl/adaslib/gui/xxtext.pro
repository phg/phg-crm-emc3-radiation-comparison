;+
; PROJECT:
;       ADAS
;
; NAME:
;	XXTEXT
;
; PURPOSE:
;	A popup window for browsing ADAS dataset comments.
;
; EXPLANATION:
;	This routine pops up a window which uses a scrollable text widget to
;	display the comment records from a text file.  Comment records
;	all begin with 'C' in the first column.  A 'Done' button is
;	also included in the widget for the user to complete the browsing.
;
; USE:
;	An example of how to use this routine;
;
;		datafile = '/disk2/adas/file.dat'
;		xxtext, datafile, xsize=132, ysize=10
;
; INPUTS:
;       DATAFILE - String; Data file name, e.g '/disk2/bowen/adas/file.dat'
;		   Note: It is the responsibility of the caller to
;		   ensure that the file name is valid and readable.
;
; OPTIONAL INPUTS:
;	None.
;
; OUTPUTS:
;       The return value of this function is the ID of the compound
;       widget which is of type LONG integer.
;
; OPTIONAL OUTPUTS:
;       None
;
; KEYWORD PARAMETERS:
;	XSIZE	- Integer; x size in characters of browse widget.
;		  Default is 80.
;
;	YSIZE	- Integer; y size in characters of browse widget.
;		  Default is 30.
;
;	FONT	- A font to use for all text in this widget.
;
; CALLS:
;	CW_LOADSTATE    Recover compound widget state.
;	CW_SAVESTATE    Save compound widget state.
;	See side effects for other related widget management routines.
;
; SIDE EFFECTS:
;	This widget uses a COMMON BLOCK: CW_OUTFILE_BLK to hold the
;	widget state.
;
;	One other routine is included which is used to manage the widget;
;
;	XXTEXT_EVENT
;
; CATEGORY:
;	Widgets
;
; WRITTEN:
;       Andrew Bowen, Tessella Support Services plc, 12-May-1993
;
; MODIFIED:
;       1.1     Andrew Bowen
;               First release.
;	1.6	William Osborn, Tessella Support Services plc
;		Changed routine so that data is read in and then all
;		sent to the text widget rather than added line by line.
;		This is much faster for long comment blocks and doesn't
;		cause flicker.
;	1.7	Martin O'Mullane
;               Make sure the file can be read before trying to
;               display it.
;	1.8	Martin O'Mullane
;               Allow lower case 'c' to denote a comment line.
;               Permit wide comments but cap at 132 characters.
;
; VERSION:
;       1.1     09-06-96
;	1.6	26-06-96
;	1.7	29-05-2003
;	1.8	18-10-2012
;-
;-----------------------------------------------------------------------------

PRO xxtext_event, event

		;**** Destroy the widget ****
    widget_control,event.top,/destroy

END

;-----------------------------------------------------------------------------


PRO xxtext, datafile, XSIZE = xsize, YSIZE = ysize, FONT = font



		;**** Set defaults for keywords ****
  IF NOT (KEYWORD_SET(xsize)) THEN xsize = 80
  IF NOT (KEYWORD_SET(ysize)) THEN ysize = 30
  IF NOT (KEYWORD_SET(font)) THEN font = ''


		;**** create titled base widget ****
  parent = widget_base(title='Data File Comments', $
			xoffset=100,yoffset=100,/column)

		;**** Title ****
  rc = widget_label(parent,value=datafile,font=font)

		;**** Read comments into text widget ****
                
  file_acc, datafile, ex, re, wr, ex, ty

  if ty NE '-' then begin
 
     comstr='**** File cannot be read ****'
     if ty EQ 'd' then comstr= ['**** File cannot be read ****', $
                                '     This is a directory!'    ]
  
  endif else begin
  
     record = ''
     openr, unit, datafile, /get_lun
     comments = 0
     comstr='**** NO COMMENTS FOUND ****'
     while (not eof(unit)) do begin
       readf, unit, record
       if strupcase(strmid(record,0,1)) eq 'C' then begin
         comments = comments + 1
         comstr=[comstr,record]
       end
     end

    close, unit
    free_lun, unit
    if comments gt 0 then comstr=comstr[1:comments]
    
 endelse

		;**** try to accommodate long comment lines ****
  
  lencmt = max(strlen(comstr))

  if lencmt GT 80 then xsize = lencmt < 132
  
		;**** Text widget to display comments ****
  textid = widget_text(parent,value=comstr,/scroll,xsize=xsize,		$
			ysize=ysize,font=font)

		;**** done button ****
  rc = widget_button(parent,value='Done',font=font)
  
		;**** realize the widget ****
  widget_control,parent,/realize

		;**** make widget modal ****
  xmanager,'xxtext',parent,event_handler='xxtext_event',/modal,/just_reg
 

END

