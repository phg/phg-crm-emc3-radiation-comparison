; Copyright (c) 1995 Strathclyde University .
; SCCS info: Module @(#)$Header: /home/adascvs/idl/adaslib/gui/adas_menus.pro,v 1.5 2009/10/01 12:53:13 mog Exp $ Date $Date: 2009/10/01 12:53:13 $
;
;+
; PROJECT:
;	ADAS IBM MVS to DEC UNIX conversion.
;
; NAME:
;	ADAS_MENUS
;
; PURPOSE:
;	Runs the ADAS menuing system for application selection.
;
; EXPLANATION:
;	This routine uses the pop-up menu routine adas_menu.pro
;	to implement the adas menuing system.  The menu system 
;	has two levels.  A main menu which identifies adas applications
;	by category and for each category a submenu of individual
;	applications.  The routine returns the user's main and sub
;	menu choices.
;
; USE:
;	See adas.pro for useage.
;
; INPUTS:
;	MAINMENU   - A string array; Holds the text of the main ADAS
;		     menu options.  One array element per option.
;
;	SUBMENUS   - A 2D string array; Holds the text of the ADAS
;		     submenu options i.e the ADAS application names.
;		     submenus(i,*) holds the submenu options for
;		     mainmenu option MAINMENU(i) for all main menu
;		     options.  Where the number of submenu options for
;		     a main option does not fill the array column
;		     there will be null strings stored.
;
;	MAINCHOICE - Integer; index, indicates which main menu choice is
;		     has been made.  If MAINCHOICE is greater than -1
;		     display of the main menu is skipped and the
;		     appropriate sub-menu is displayed immediately.
;		     This feature is used by adas.pro to return to the
;		     correct place in the menu system when an adas
;		     application has just completed.
;
; OPTIONAL INPUTS:
;	None.
;
; OUTPUTS:
;	MAINCHOICE - Integer; index, the user's selection from the
;		     main menu.  A return value of -1 indicates that the
;		     user pressed the 'Exit' button.
;
;	SUBCHOICE  - Integer; index, the user's selection from the
;		     sub-menu pointed to by MAINCHOICE.  Value will
;		     be -1 when MAINCHOICE is -1.
;
; OPTIONAL OUTPUTS:
;	None.
;
; KEYWORDS:
;	ADASREL	- A string; ADAS release information, copied to the
;		  main menu title. e.g ' ADAS RELEASE: ADAS93 V1.1.0'
;
;	FONT	- A string; the name of a font to use for all text
;		  in the menus.  Defaults to default system font.
;
; CALLS:
;	ADAS_MENU 	Popup a menu.
;
; SIDE EFFECTS:
;	None.
;
; CATEGORY:
;	Adas system.
;
; WRITTEN:
;       Andrew Bowen, Tessella Support Services plc, 25-May-1993
;
; MODIFIED:
;       Version 1       Andrew Bowen    1-Jun-1993
;                       First release.
;	        1.2     Martin O'Mullane
;		        Remove 'Software Package' from title and
;                       add a full-stop to improve grammar.
;
; VERSION:
;       1       1-Jun-1993
;	1.2	01-10-2009
;-
;-----------------------------------------------------------------------------

PRO adas_menus, mainmenu, submenus, mainchoice, subchoice, $
		ADASREL=adasrel, FONT=font

  IF NOT(KEYWORD_SET(adasrel)) THEN adasrel = ''
  IF NOT(KEYWORD_SET(font)) THEN font = ''

  mainhead = [ $
	'Welcome to A.D.A.S.', $
	'The Atomic Data and Analysis Structure', $
	adasrel $
	    ]
  title = 'ADAS SYSTEM MENU'


MAIN:
		;**** If a main choice is currently active skip ****
		;**** directly to sub-menu.			****
  if mainchoice eq -1 then begin
    mainchoice = adas_menu(mainmenu, TITLE=title, HEADER=mainhead, FONT=font)
  end

		;**** If a main choice has been made display ****
		;**** submenu.				     ****
  if mainchoice gt -1 then begin

    header = mainmenu(mainchoice) 
    subchoice = adas_menu(reform(submenus(mainchoice,*)), TITLE=title, $
				HEADER=header, FONT=font)

    if subchoice lt 0 then begin
		;**** Exit from sub-menu, back to main menu ****
      mainchoice = -1
      goto, MAIN
    end

  end else begin
		;**** No choices (exit button) prepare for exit ****
    subchoice = -1

  end

END
