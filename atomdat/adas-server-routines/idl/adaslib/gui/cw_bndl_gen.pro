; Copyright (c) 1996, Strathclyde University.
; SCCS info: Module @(#)$Header: /home/adascvs/idl/adaslib/gui/cw_bndl_gen.pro,v 1.2 2004/07/06 13:04:47 whitefor Exp $ Date $Date: 2004/07/06 13:04:47 $
;+
; PROJECT:
;       IDL-ADAS development
;
; NAME:
;       CW_BNDL_GEN()
;
; PURPOSE:
;       Unbundling selection list with text boxes for specification of
;       groupings and automatic 'next row' input focus.
;
; EXPLANATION:
;       This compound widget produces an interactive selection list 
;       which consists of a WIDGET_LABEL and an editable WIDGET_TEXT
;       box for each item in a given list of string labels. A title
;       can also be specified.
;
;       The list of labels is specified as a one dimensional string 
;       array of the items to be grouped. The procedure passes back
;       an integer array containing the numbers input by the user
;       at the index of the label they chose and zeros everywhere
;       else.
; USE:    
;       General purpose.
;
;       See the routine CW_ADAS210_PROC for a working example of how to 
;       use CW_BNDL_GEN.
;
;       The user makes selections by entering a number in the text box
;       next to the one they choose, and can group selections together
;       by putting the same number next to each choice. The user MUST 
;       press return to register their selection with XMANAGER. Any 
;       number of selections can be made and choices can also be altered.
;       The selections are returned in bndls, with zeros where no
;       selections were made.
;  
;       The following code is an example of a popup selection widget
;       displaying a list of the numbers five to ten.
;         
;       x = ['five','six','seven','eight','nine','ten']
;       title = 'Select Prime Numbers'
;       nsizex = n_elements(x)
;       sels = intarr(nsizex)
;       val = { labels:x, title:title, bndls:sels }
;       wid = cw_bndl_gen(parent,nsizex,value=val)
;
;       After selecting the prime numbers, val.bndls will contain;
;       [1,0,1,0,0,0]
;           
; INPUTS: 
;
;       PARENT     - The parent ID of the compound widget.
;
;       NLABELS    - Integer; The size of the array labels.
;
; OUTPUTS:
;       None
;
; KEYWORD PARAMETERS:
;       FONT       - The name of a font.
;
;       VALUE      - The structure which contains the initial settings
;                    of the widget, and finally the user selections.
;                    The default value is;
;                    bndl_val = { bndl_val,                  $
;                                 labels:arr,                $
;                                 title:'',                  $     
;                                 bndls:arr                  $
;                                }
;                    where arr = strarr(nlabels)
;                    labels is an array containing the strings that will
;                    appear as labels next to the input boxes. Title, is
;                    the title that will appear on the header. Bndls
;                    is the selection vector recording the numbers entered
;                    by the user and zeros elsewhere. For a working example
;                    see cw_adas210_proc.pro. 
; CALLS:
;        BNDL_GEN_EVENT   Called indirectly during widget management
;                         to handle the widget events. Procedure is
;                         included in this file.
;        BNDL_GEN_GET_VAL Widget management, included in this file.
;
; SIDE EFFECTS:
;        None
;
; CATEGORY:
;        Compound widget
;
; WRITTEN:
;        David H.Brooks, Univ.of Strathclyde, 21-11-1995
;
; MODIFIED:
;	1.1	David H. Brooks	
;		First version
;	1.2	Tim Hammond
;		Modified length of columns from 15 to 13
;
; VERSION:
;       1.1	21-11-95
;	1.2	22-01-96
;
;-
;-----------------------------------------------------------------------------

FUNCTION bndl_gen_get_val, id

    ON_ERROR, 2

	;**** Retrieve the structure from the child ****
        ;**** that contains the sub ids.            ****

    stash = WIDGET_INFO(id, /CHILD)
    WIDGET_CONTROL, stash, GET_UVALUE=state, /NO_COPY

  
    for i = 0, (state.nlabels-1) do begin
        widget_control, state.groups(i), get_value=value
        state.bndls(i) = value
    endfor 

        ;**** copy selections to vector ****

    bndls = state.bndls

	;**** Restore the state. ****

    WIDGET_CONTROL, stash, SET_UVALUE=state, /NO_COPY

    RETURN, bndls

END

;-----------------------------------------------------------------------------

FUNCTION bndl_gen_event, event

    parent=event.handler

	;**** Retrieve the structure from child ****

    stash = WIDGET_INFO(parent, /CHILD)

    WIDGET_CONTROL, stash, GET_UVALUE=state, /NO_COPY

    name = strmid(tag_names(event, /structure_name),7,1000)

           ;***************************************;
           ;******* process user selections *******;
           ;***************************************;

    case(name) of

    	"TEXT_CH":begin

            widget_control, event.id, get_uvalue = uvalue
            if (uvalue ne state.nlabels-1) then	                        $
            widget_control, /input_focus, state.groups(uvalue+1)
        end

	ELSE:				;**** Do nothing ****

     ENDCASE

        ;**** Restore the state structure ****

    WIDGET_CONTROL, stash, SET_UVALUE=state, /NO_COPY

    RETURN, new_event 

END

;-----------------------------------------------------------------------------

FUNCTION cw_bndl_gen, parent, nlabels, labels, bndls, TITLE=title, 	$
                      UVALUE=uval, FONT=font 
                
    IF (N_PARAMS() EQ 0) THEN MESSAGE, 'Must specify a parent for cw_bndl_gen'

    ON_ERROR, 2					;return to caller
    IF NOT (KEYWORD_SET(title))  THEN title = ''
    IF NOT (KEYWORD_SET(uval))  THEN uval = 0
    IF NOT (KEYWORD_SET(xoffset)) THEN xoffset = 200
    IF NOT (KEYWORD_SET(font)) THEN font = ''

           ;**** declare some variables ****

    terms = indgen(nlabels)
    groups = terms & space = terms
    rowtxt = intarr(nlabels)

    base = WIDGET_BASE(parent, UVALUE = uval, 				$
                       EVENT_FUNC = "bndl_gen_event", 			$
                       FUNC_GET_VALUE = "bndl_gen_get_val", 		$
                       /row, XOFFSET=1, YOFFSET=1)

    row  = widget_base(base, /row)
    row1 = widget_base(row, /column)
    row2 = widget_base(row1, /row)

           ;***************************************;
           ;*determine number of columns required *;
           ;* each column is length 13 but can be *;
           ;*************** altered ***************;
           ;***************************************;

    ncols = fix(1 + nlabels/13)
    col = intarr(ncols)
 
    for j = 0, (ncols-1) do begin
        col(j) = widget_base(row2, /column)
        for i = (13*j), min([(13*j)+12,nlabels-1]) do begin
            rowtxt(i) = widget_base(col(j),/row)
            terms(i)  = widget_label(rowtxt(i), value = labels(i),	$
            font=font, /frame)
            groups(i) = widget_text(rowtxt(i), xsize = 5, /edit)
            widget_control, groups(i), set_uvalue = i
        endfor
    endfor

    state = {GROUPS:groups, NLABELS:nlabels, BNDLS:bndls }

    WIDGET_CONTROL, WIDGET_INFO(base, /CHILD), SET_UVALUE=state, /NO_COPY

    RETURN, base

END





