; Copyright (c) 1995, Strathclyde University .
; SCCS info: Module @(#)$Header: /home/adascvs/idl/adaslib/gui/cw_edit_table.pro,v 1.19 2004/07/06 13:06:16 whitefor Exp $ Date $Date: 2004/07/06 13:06:16 $
;
;+
; PROJECT:
;       ADAS IBM MVS to   UNIX conversion
;
; NAME:
;	CW_EDIT_TABLE()
;
; PURPOSE:
;	Spread-sheet-like 2-D array screen editor.
;
; EXPLANATION:
;	This compound widget function produces a spreadsheet-type table
;	of text data which can be edited by the user.  The table is defined
;	in terms of a number of columns and a number of rows and hence
;	takes data from a 2-D array.  Each column in the table can be
;	given a text header and an integer row index can be optionally
;	displayed in a column along the edge of the table.  The table may
;	include editable and non-editable columns as defined by the
;	caller.  Optionally the text data which is entered may be
;	specified as representing integer or float values in which case
;	any illegal values will be flagged as in error.
;
;	The widget consists of an array of single line text widgets.
;	The user can direct the keyboard input to any text 'Cell' by
;	clicking on that cell with the mouse.  If the user presses the 
;	return key the keyboard input moves to the next cell.  The
;	direction in which the active cell moves, down columns or across
;	rows, is under the control of the user via the 'Row_skip' and
;	'Column_skip' toggle buttons.  When the end of a column or row
;	is reached the input moves to the begining of the next column or
;	row.
;
;	The caller can specify two different fonts via the FONTS keyword
;	to distinguish between editable and non-editable data.
;
;	A number of editing features are provided with this edit widget.
;	These features are listed with toggle buttons along the bottom
;	of the edit cell array.  To use an edit feature position the
;	keyboard input cursor on the required cell, switch on the edit
;	facility with its toggle button and then press the return key
;	to perform the operation.  The edit facilities are;
;
;		Delete - Set the current text cell to empty.
;		Remove - Shift all values in the column up one space
;			 from one below the current cell to the bottom
;			 of the column.
;		Insert - Insert a blank value at the current cell
;			 by moving values in the column down on space.
;		Copy   - Copy the value from the current cell into a
;			 buffer.
;		Paste  - Copy the current buffer contents into the
;			 current cell.
;
;	Also for lists which are too large "scrolling" can be used
;	Two buttons are available - 
;		Scroll up - move the list up in the table.
;		Scroll Down - Move the list down in the table.
;
;	These edit control buttons can be removed from the widget by
;	specifying the NOCON keyword.
;
;       This file includes the event management and SET_VALUE/GET_VALUE
;       routines.  The VALUE of this widget is the 2-D string array of
;	text from the array of table edit cells.
;
; USE:
;	The following creates a table edit widget for an array of
;	float values.  The keyword /FLOAT instructs the widget to check
;	all user input to see if it represents a valid floating point
;	number.  This code does not realize the widget.
;
;	data = make_array(5,6,/float,value=25.0)
;	strdata = string(data)
;	base = WIDGET_BASE(/COLUMN)
;	edid = CW_EDIT_TABLE(base,NCOLS=5,NROWS=6,VALUE=strdata,/FLOAT)
;
;	The value of the widget, the string array, can be set and
;	read using WIDGET_CONTROL.  The following code will reset the
;	data in the table to new values;
;
;	WIDGET_CONTROL,edid,SET_VALUE=strdata2
;
;	The following code will read the current string data from the
;	edit table, perhaps after the user has made modifications;
;
;	WIDGET_CONTROL,edid,GET_VALUE=outdata
;
;	This code creates an edit table with column headers.  Column
;	headers 1 and 3 have two rows of text.  The row index column
;	which appears to the left of the table by default is switched off.
;	Of the four columns only the first and second columns are
;	editable.  The data is taken to be text data.  The edit control
;	buttons are switched off.
;
;	strdata = make_array(4,3,value='Text Data')
;	base = WIDGET_BASE(/COLUMN)
;	edid = CW_EDIT_TABLE(base,NCOLS=4,NROWS=3,VALUE=strdata, $
;		COLHEAD=[['Column 1','Column 2,'Column 3','Column 4'], $
;			 ['Data','','Data','']], $
;		COLEDIT=[1,1,0,0],/NOINDEX,/NOCON)
;
;	The basic widget does not include 'Quit' or 'Done' buttons.
;	This widget does not generate events itself, all events are
;	handled internally.
;
; INPUTS:
;       PARENT   - The ID of the parent widget.
;
; OPTIONAL INPUTS:
;       None.  See the keywords for additional controls.
;
; OUTPUTS:
;       The return value of this function is the ID of the compound
;       widget which is of type LONG integer.
;
; OPTIONAL OUTPUTS:
;       None
;
; KEYWORD PARAMETERS:
;	NCOLS    - The number of columns of data.  The default is one column.
;
;	NROWS    - The number of rows of data. The default is one row.
;
;	COLEDIT  - Flags to indicate which columns are editable, type
;		   INTARR(NCOLS).  The default is all columns are editable.
;
;	COLHEAD  - Column headers, type STRARR(NCOLS,n).  Each of the n
;		   rows of header is placed one below another at the top
;		   of each column.
;
;	VALUE    - Array of initial data values, type STRARR(NCOLS,NROWS).
;		   Elements of the string array may contain empty strings.
;		   The default is blank data fields.
;
;	FLOAT    - Set this keyword to indicate that the data values must
;		   represent valid floating point numbers.
;
;	INTEGER  - Set this keyword to indicate that the data values must
;		   represent valid integer numbers.
;
;	UVALUE   - Supplies the user value for the widget.
;
;	NOINDEX  - Set this keyword to remove the column of integer index
;		   values at the far left of the table.  The default is
;		   for the index column to appear.
;
;	NOCON    - Set this keyword to remove the edit control buttons
;		   delete, remove, insert etc.
;
;	YSIZE    - Specifies the ysize of the table in rows of cells.
;		   If ysize is less than the number of rows the columns
;		   are split and placed side-by-side.
;
;	CELLSIZE - Specifies the character width of the text widget
;		   cells which make up the table.  The default is 8.
;		   In some circumstances the windows toolkit may ignore
;		   this setting depending on the width of the column
;		   header.
;
;	NOEDIT   - Set this keyword to make all data fields non-editable.
;		   The default is for all data columns specified by COLEDIT
;		   to be editable.
;
;	ROWSKIP  - By default the cursor skips down columns of cells
;		   when the return key is pressed.  Set this keyword to
;		   cause the cursor to skip across rows in the table.
;
;	FONTS	 - Structure of two fonts to use for table,
;		   {font_norm:string, font_input:string}
;
; CALLS:
;	NUM_CHK		Checks that a string represents a legal number.
;	CW_LOADSTATE	Recover compound widget state.
;	CW_SAVESTATE	Save compound widget state.
;	See side effects for other related widget management routines.
;
; SIDE EFFECTS:
;	This widget uses a COMMON BLOCK: CW_EDTAB_BLK to hold the
;       widget state.
;
;       Three other routines are included which are used to manage the
;       widget;
;
;	EDTAB_SET_VAL
;	EDTAB_GET_VAL()
;	EDTAB_EVENT()
;
; CATEGORY:
;	Compound Widget?
;
; WRITTEN:
;       Andrew Bowen, Tessella Support Services plc, 4-Mar-1993
;
; MODIFIED:
;       Version 1       Andrew Bowen    28-May-1993
;                       First release.
;
;	Version 1.1	Lalit Jalota    20-Dec-1995
;			Modified "insert" and "delete" options so that they only
;			need to be pressed once for multiple goes.
;
;	Version 1.2	Lalit Jalota	21-Dec-1995
;			Added scroll option to table.
;
;	Version 1.3     Lalit Jalota    10-Jan-1995
;			Removed obsolete calls to CW_SAVE_STATE & CW_LOAD_STATE
;			with "widget_control,wid,set_uvalue= "
;			and  "widget_control,wid,get_uvalue= "
;
;	Version 1.4	Lalit Jalota	15-Mar-1995
;			Included checks for whether units and index column 
;			are requested'
;
;	Version 1.13	Tim Hammond
;			Added FLTINT keyword parameter.
;			Prevented routine crashing when enter is pressed in
;			final row of table.
;
;	Version 1.14	Tim Hammond/David Brooks
;			Further minor correction to prevent above crash.
;
;	Version 1.15	Tim Hammond
;			Corrected SC CS version numbers.
;
;	Version 1.16	Tim Hammond
;			To prevent crash when a single column table is
;			edited have no longer referenced state.fltint(i)
;			as IDL appears to automatically set this to a 
;			scalar variable if it only has one value and so
;			attempts to reference it with an idex cause a 
;			crash. The index has instead been transferred
;			to the variable numformat.
;	Version 1.17	Tim Hammond
;			Have added capability for handling text as well
;			as numbers. The table editor now no longer performs
;			any checks for columns which have a fltint value
;			of '(a)'. This means that they can be set to anything
;			but in general text columns will probably be
;			non-editable.
;     Version 1.18    William Osborn
;                     Removed xzise=40 directive from widget_label command
;	Version 1.19    Richard Martin
;			    Added 'default' edit button to enable user to exit
;			    from 'Delete', 'Remove' and 'Insert' modes.
;				
;
; VERSION:
;       1       28-May-1995
;	1.1	20-Dec-1995	
;	1.2	21-Dec-1995
;	1.3	10-Jan-1995
;	1.4	15-Mar-1995
;	1.13	20-Jun-1995
;	1.14	21-Jun-1995
;	1.15	21-Jun-1995
;	1.16	26-Jun-1995
;	1.17	23-Nov-1995
;	1.18	04-Oct-1996
;	1.19	25-Jun-1998
;-
;-----------------------------------------------------------------------------

PRO edtab_set_val, id, value

		;**** Return to caller on error ****
  ON_ERROR, 2

		;**** Retrieve the state ****
   widget_control, id, get_uvalue=state, /no_copy

		;**** Set the data field values ****
  for i = 0 , state.ncols-1 do begin
    for j = 0 , state.lstrow-1 do begin
      widget_control, state.fields(i,j), set_value = value(i,j)
    end
  end
		;**** Reset the value of state variable ****
   widget_control, id, set_uvalue=state, /no_copy

END

;-----------------------------------------------------------------------------


FUNCTION edtab_get_val, id
		;**** The value is recovered directly from   ****
		;**** the text widgets.  The copy of VALUE   ****
		;**** which is held in the state structure   ****
		;**** is only used to prevent unnecessary    ****
		;**** calls to NUM_CHK in the event handler. ****

		;**** Return to caller on error ****
  ON_ERROR, 2

		;**** Retrieve the state ****
   widget_control, id, get_uvalue=state, /no_copy

		;**** Declare the output value ****
  value = strarr(state.ncols,state.nrows)

		;**** Get the data field values ****

  for i = 0 , state.ncols-1 do begin
    for j = 0 , state.lstrow-1 do begin
      widget_control, state.fields(i,j), get_value = text
      value(i,j) = text
    end
  end

		;**** Check input is valid numeric data if required ****
		;**** overwrite bad values with '!error'.           ****
  if state.integer eq 1 or state.float eq 1 then begin
    if state.float eq 1 then integer = 0 else integer = 1
    numformat = state.fltint
    for i = 0 , state.ncols-1 do begin
      if strmid(numformat(i),0,2) ne '(a' then begin
        for j = 0 , state.lstrow-1 do begin
	  if strtrim(value(i,j),2) ne '' then begin
	    error = num_chk(value(i,j),integer=integer)
	    if error eq 1 then value(i,j) = '!error'
	  endif
        endfor
      endif
    endfor
  endif

		;**** Reset the state variable ****
     widget_control, id, set_uvalue=state, /no_copy

  RETURN, value

END

;-----------------------------------------------------------------------------

FUNCTION edtab_event, event

		;**** Base ID of compound widget ****
  parent=event.handler

		;**** Retrieve the state ****
   widget_control, parent, get_uvalue=state, /no_copy

		;**** Clear any error message ****
  widget_control,state.messid,set_value=' '

		;**** Structure of text widget uvalue ****
  pos = {row:0,col:0}

		;************************
		;**** Process Events ****
		;************************

  CASE event.id OF

		;**** Toggle edit controls on/off ****

    state.defid: if event.select eq 1 then  state.control = 'none'                   

    state.delid: if event.select eq 1 then $
		       state.control = 'Delete' else $
		       state.control = 'none'

    state.remid: if event.select eq 1 then $
                   state.control = 'Remove' else $
                   state.control = 'none'

    state.insid: if event.select eq 1 then $
                   state.control = 'Insert' else $
                   state.control = 'none'

    state.copid: if event.select eq 1 then $
                   state.control = 'Copy' else $
                   state.control = 'none'

    state.pasid: if event.select eq 1 then $
                   state.control = 'Paste' else $
                   state.control = 'none'
                   
		;**** Toggle row/column skip control ****
    state.rskipid: begin
		     if event.select eq 1 then begin
		       state.rowskip = 1
		       widget_control,state.cskipid,set_button=0
		     endif else begin
		       state.rowskip = 0
		       widget_control,state.cskipid,set_button=1
		     endelse
		   end


    state.cskipid: begin
		     if event.select eq 1 then begin
		       state.rowskip = 0
		       widget_control,state.rskipid,set_button=0
		     endif else begin
		       state.rowskip = 1
		       widget_control,state.rskipid,set_button=1
		     endelse
		   end

		;**** Scroll up or down ****
    state.scrollupid: begin

		;*** check that table values can be scrolled up ***
		if (state.lstrow_index lt state.nrows) then begin
		   for i = state.fstrow, state.lstrow-1 do begin
			  ;*** change index ***
		     index_size = size(state.indexrc)
                     if index_size(0) gt 0 then begin
		        widget_control, state.indexrc(i), set_value = $
		  	      strcompress(string(state.fstrow_index+i+2))
                     endif
			   ;*** change the data/table values

		     for j = 0, state.ncols-1 do begin
		         widget_control, state.fields(j,i), $
			      set_value=state.value(j, (state.fstrow_index+i+1))
		     endfor
		   endfor
		   state.fstrow_index = state.fstrow_index + 1
		   state.lstrow_index = state.lstrow_index + 1
		   ;*** increment scroll_offset ***
		   state.scroll_offset = state.scroll_offset + 1
		endif 
		   ;*** reset button ***
		widget_control, state.scrollupid,set_button=0

			end
    state.scrolldownid: begin

		;*** check that table values can be scrolled down ***
		if (state.fstrow_index gt 0) then begin
		   for i = state.fstrow, state.lstrow-1 do begin
		;*** change index ***
		     index_size = size(state.indexrc)
                     if index_size(0) gt 0 then begin
		         widget_control, state.indexrc(i), set_value = $
		  	     strcompress(string(state.fstrow_index+i))
                      endif
		;*** change the data/table values
		       for j = 0, state.ncols-1 do begin
			  widget_control, state.fields(j,i), $
			  set_value=state.value(j, (state.fstrow_index+i-1))
		       endfor
		   end
		   state.fstrow_index = state.fstrow_index - 1
		   state.lstrow_index = state.lstrow_index - 1
		;*** decrement scroll_offset ***
		   state.scroll_offset = state.scroll_offset - 1
		endif
		;*** reset button ***
		widget_control, state.scrolldownid,set_button=0

			end

		;**** Text widget event, return key.           ****
		;**** NB only editable widgets generate events ****
    ELSE: begin

      CASE state.control OF

	'Delete' : begin
		;**** Copy value to paste buffer and then delete ****
	  widget_control,event.id,get_value=temp
	  state.paste_buf = temp(0)
	  widget_control,event.id,set_value=''
		;*** removed next lines so 'delete' is repeatable ***
	  ;state.control = 'none'
	  ;widget_control,state.delid,set_button=0
			;**** Get the position of text event widget ****
	  widget_control,event.id,get_uvalue=pos
	  row = pos.row
	  col = pos.col
          state.value(col,row+state.scroll_offset) =  ''
		;*** added next line to skip down one cell ***
          if (row lt state.ysize-1) then begin
              widget_control,/input_focus,state.fields(col,row+1)
          endif
	end

	'Remove' : begin
		;**** shift all values up one cell from event widget ****
			;**** Get the position of text event widget ****
	  widget_control,event.id,get_uvalue=pos
	  row = pos.row 
	  col = pos.col

			;**** Copy values up one cell ****
			;**** now reset data array ****
	  for i = row+state.scroll_offset, state.nrows-2 do begin
	     state.value(col,i) = state.value(col,i+1)
	  end
			;**** blank in the last cell ****
            state.value(col,state.nrows-1) =  ''

			;**** Now display the values to screen. ****
	  for i = row, state.lstrow-1 do begin
	    widget_control,state.fields(col,i), $
		set_value=state.value(col,i+state.scroll_offset) 
	  end

		;**** removed next lines so that instead of ****
		;**** having to press button again for      ****
		;**** multiple removes, user can just press ****
		;**** return button. LJ 22/12/94            ****
	  ;state.control = 'none'
	  ;widget_control,state.remid,set_button=0
	  
	end

	'Insert' : begin
                ;**** shift all values down one cell from event widget ****
                        ;**** Get the position of text event widget ****
          widget_control,event.id,get_uvalue=pos
          row = pos.row
          col = pos.col

                        ;**** Copy values down one cell ****
          for i = state.nrows-1, row+1+state.scroll_offset, -1 do begin
	    state.value(col,i) = state.value(col,i-1)
          endfor
                        ;**** blank in the event cell ****
	  state.value(col,row+state.scroll_offset) = ''

		 	;**** update the display table ****
          for i = state.lstrow-1, row, -1 do begin
            widget_control,state.fields(col,i),$
			set_value=state.value(col,i+state.scroll_offset)
          end
	  

		;**** removed next lines so that instead of ****
		;**** having to press button again for      ****
		;**** multiple inserts, user can just press ****
		;**** return button. LJ 22/12/94            ****
          ;state.control = 'none'
          ;widget_control,state.insid,set_button=0
	end

	'Copy'   : begin
		;**** Copy value to paste buffer ****
	  widget_control,event.id,get_value= temp
	  state.paste_buf = temp(0)
	  state.control = 'none'
	  widget_control,state.copid,set_button=0
	end

	'Paste'  : begin
		;**** Copy paste buffer to text widget ****
	  widget_control,event.id,set_value=state.paste_buf
	  state.control = 'none'
	  widget_control,state.pasid,set_button=0
	end
	  
	ELSE     : begin
			;******************************
			;**** Skip cursor position ****
			;******************************

			;**** Get the position of text event widget ****
	  widget_control,event.id,get_uvalue=pos
	  row = pos.row
	  col = pos.col

	  if state.rowskip eq 0 then begin
			;**** Skip cursor down one field, move to the ****
			;**** next editable column when the bottom of ****
			;**** current column is reached.              ****
	    row = row + 1
	    if row ge (state.lstrow) then begin
	      row = 0
	      col = col + 1
	      if col ge (state.ncols) then begin
	        col = 0
	      end

	      while (state.coledit(col) eq 0) do begin
	        col = col + 1
	        if col ge (state.ncols) then begin
	          col = 0
	        end
	      end
	    end
	  endif else begin
			;**** Skip cursor across one field to the ****
			;**** next editable column.  When the end ****
			;**** of the table is reached wrap around ****
			;**** to the first column and step down   ****
			;**** one row.				  ****
	    col = col + 1
	    if col ge (state.ncols) then begin
	      col = 0
	      row = row + 1
	      if row ge (state.lstrow) then begin
	        row = 0
	      end
	    end

	      while (state.coledit(col) eq 0) do begin
	        col = col + 1
	        if col ge (state.ncols) then begin
	          col = 0
	          row = row + 1
	          if row ge (state.lstrow) then begin
	            row = 0
	          end
	        end
	      end
	  endelse
	  widget_control, /input_focus, state.fields(col,row)

	end

      ENDCASE

    end

  ENDCASE

		;**** Check input is valid numeric data if required ****
  if state.integer eq 1 or state.float eq 1 then begin
    error = 0
    mess = ['Floating Point','Integer']
    if state.float eq 1 then integer = 0 else integer = 1

		;**** Find the first error and then stop ****
    i = 0 
    while (i lt state.ncols and error eq 0) do begin
      j = 0

      while (j lt state.lstrow and error eq 0) do begin

	  widget_control,state.fields(i,j),get_value=numstr
          numstr = numstr(0)
		;**** Only check values which have changed ****
		;**** Only update the state.value when new ****
		;**** values check out okay.               ****
	  if numstr ne state.value(i,j+state.scroll_offset) then begin
            numformat = state.fltint
            if (strmid(numformat(i),0,2) eq '(i') or 			$
            (strmid(numformat(i),0,2) eq '(a') then begin
                if (strtrim(numstr, 2) ne '') then begin
                    numstr = string(numstr, format=numformat(i))
                endif
            endif else begin
                if (strtrim(numstr, 2) ne '') then begin
                    numstr = strtrim(string(numstr, format=numformat(i)), 2)
                endif
            endelse
	    if strcompress(numstr,/remove_all) ne '' then begin
	      if strmid(numformat(i),0,2) eq '(a' then begin
		error = 0
	      endif else begin
	        error = num_chk(numstr,integer=integer)
	      endelse
	      if error eq 1 then begin
	        widget_control,state.messid, $
			set_value='****  Illegal '+mess(integer)+' Value ****'
	        widget_control,state.fields(i,j),/input_focus
	      endif else begin
	        state.value(i,j+state.scroll_offset) = numstr
	      endelse
	    endif else begin
                state.value(i,j+state.scroll_offset) = numstr
            endelse
	  endif

	j = j+1
      endwhile
      i = i+1
    endwhile

  endif

		;**** Reset the value of state variable ****
     widget_control, parent, set_uvalue=state, /no_copy

  RETURN, 0L
END

;-----------------------------------------------------------------------------

FUNCTION cw_edit_table,	parent, NCOLS = ncols, NROWS = nrows, $
			COLEDIT = coledit, COLHEAD = colhead, $
			VALUE = value, FLOAT = float, INTEGER = integer, $
			UVALUE = uvalue, NOINDEX = noindex, $
			NOCON = nocon, YSIZE = ysize, CELLSIZE = cellsize, $
			NOEDIT = noedit, ROWSKIP = rowskip, FONTS = fonts, $
			NUM_FORM=num_form, FLTINT=fltint


  IF (N_PARAMS() EQ 0) THEN MESSAGE, 'Must specify a parent for cw_edit_tab'
  ON_ERROR, 2					;return to caller

		;**** Set defaults for keywords ****
  IF NOT (KEYWORD_SET(ncols)) THEN ncols = 1
  IF NOT (KEYWORD_SET(nrows)) THEN nrows = 1
  IF NOT (KEYWORD_SET(coledit)) THEN coledit = make_array(ncols,/int,value=1)
  IF NOT (KEYWORD_SET(colhead)) THEN $
			colhead = make_array(ncols,/string,value='')
  IF NOT (KEYWORD_SET(value)) THEN $
			value = make_array(ncols,nrows,/string,value='')
  IF NOT (KEYWORD_SET(float)) THEN float = 0
  IF NOT (KEYWORD_SET(integer)) THEN integer = 0
  IF NOT (KEYWORD_SET(uvalue))  THEN uvalue = 0
  IF NOT (KEYWORD_SET(noindex)) THEN noindex = 0
  IF NOT (KEYWORD_SET(nocon)) THEN nocon = 0
  IF NOT (KEYWORD_SET(ysize)) THEN ysize = nrows
  IF NOT (KEYWORD_SET(cellsize)) THEN cellsize = 8
  IF NOT (KEYWORD_SET(noedit)) THEN  noedit = 0
  IF NOT (KEYWORD_SET(rowskip)) THEN rowskip = 0
  IF NOT (KEYWORD_SET(num_form)) THEN num_form = '(E10.3)'
  IF NOT (KEYWORD_SET(fltint)) then fltint = make_array(ncols,/string,$
                                                value=num_form)
  IF NOT (KEYWORD_SET(fonts)) THEN fonts = {font_norm:'',font_input:''}

		;**** Create the main base for the table ****
  mainbase = WIDGET_BASE(parent, UVALUE = uvalue, $
		EVENT_FUNC = "edtab_event", $
		FUNC_GET_VALUE = "edtab_get_val", $
		PRO_SET_VALUE = "edtab_set_val", $
		/COLUMN)

		;**** Structure for uvalue of text widgets **** 
  pos = {col:0,row:0}

		;**** Array of text widget IDs ****
  fields = lonarr(ncols,nrows)

		;**** Create table base ****
  tabbase = widget_base(mainbase,/row)

		;**** Create controls base ****
  if nocon eq 0 then conbase = widget_base(mainbase,/column)

		;**** Create message area ****
  messid = widget_label(mainbase,value=' ',font=fonts.font_norm)

		;********************************
		;**** The Main Table of Data ****
		;********************************

		;**** How many rows of header are there? ****
  headsize = size(colhead)
  if headsize(0) eq 1 then numhrow = 1 else numhrow = headsize(2)

		;*** decide on suitable table size ***
    ysize = 10 < nrows

		;*** create counters for table size
    fstrow = 0 
    fstrow_index = 0
    lstrow = ysize
    lstrow_index = ysize


		;**** Create index column if required ****
    if noindex eq 0 then begin
      col_base = widget_base(tabbase,/column)
      rc = widget_label(col_base,value='INDEX',font=fonts.font_norm)
      for i = 1 , numhrow-1 do begin
          rc = widget_label(col_base,value=' ',font=fonts.font_norm)
      endfor
      indexrc = intarr(lstrow - fstrow +1)
      for j = fstrow, lstrow-1 do begin
        indexrc(j) = widget_text(col_base,xsize=3, $
			value=strcompress(string(j+1)), $
				font=fonts.font_norm)
      endfor
    endif else begin
      indexrc = 0
    endelse

		;**** Create field columns, label at the top ****
    for i = 0 , ncols-1 do begin
 
      col_base = widget_base(tabbase,/column)
      for j = 0 , numhrow-1 do begin
        header = colhead(i,j)
        if header eq '' then header = ' '
        rc = widget_label(col_base,value=header,font=fonts.font_norm)
      endfor

      for j = fstrow, lstrow-1 do begin
        pos.col = i
        pos.row = j
        if coledit(i) eq 1 then begin
          fields(i,j) = widget_text(col_base,/editable,xsize=cellsize, $
			font=fonts.font_input,value=value(i,j),uvalue=pos)
        endif else begin
          fields(i,j) = widget_text(col_base,xsize=cellsize, $
			font=fonts.font_norm,value=value(i,j),uvalue=pos)
        endelse
      endfor

    endfor


		;**** Create controls buttons ****
  if nocon eq 0 then begin
    leftcon = widget_base(conbase,/row,/exclusive,/frame)
    defid = widget_button(leftcon,value='Default',font=fonts.font_norm)
    delid = widget_button(leftcon,value='Delete',font=fonts.font_norm)
    remid = widget_button(leftcon,value='Remove',font=fonts.font_norm)
    insid = widget_button(leftcon,value='Insert',font=fonts.font_norm)
    copid = widget_button(leftcon,value='Copy',font=fonts.font_norm)
    pasid = widget_button(leftcon,value='Paste',font=fonts.font_norm)
    widget_control,defid,set_button=1

    rightcon = widget_base(conbase,/row,/nonexclusive,/frame)
    rskipid = widget_button(rightcon,value='Row_skip',font=fonts.font_norm)
    cskipid = widget_button(rightcon,value='Column_skip',font=fonts.font_norm)
    if rowskip eq 0 then begin
      widget_control,cskipid,set_button=1
    endif else begin
      widget_control,rskipid,set_button=1
    endelse
    scrollupid = widget_button(rightcon,value='Scroll up',font=fonts.font_norm)
    scrolldownid = widget_button(rightcon,value='Scroll down', $
					font=fonts.font_norm)
  endif else begin
    defid = long(0)
    delid = long(0)
    remid = long(0)
    insid = long(0)
    copid = long(0)
    pasid = long(0)
  endelse

		;**** Create an empty paste buffer ****
  paste_buf = ''

		;**** Create state structure ****
  new_state =  {NCOLS:ncols, NROWS:nrows, FIELDS:fields, $ 
		VALUE:value, $
		INTEGER:integer, FLOAT:float, MESSID:messid, $
		DEFID:defid, DELID:delid, REMID:remid, INSID:insid, $
		COPID:copid, PASID:pasid, CSKIPID:cskipid, $
		RSKIPID:rskipid, INDEXRC:indexrc, PASTE_BUF:paste_buf, $
		COLEDIT:coledit, NOEDIT:noedit, CONTROL:'none', $
		ROWSKIP:rowskip, $
		FSTROW:fstrow, LSTROW:lstrow, $
		FSTROW_INDEX:fstrow_index, LSTROW_INDEX:lstrow_index, $
		SCROLLUPID:scrollupid, SCROLLDOWNID:scrolldownid, $
                FLTINT:fltint, YSIZE:ysize,          $
		SCROLL_OFFSET : 0 , NUM_FORM:num_form $
	       }
		;**** Save initial state structure ****
   widget_control, mainbase, set_uvalue=new_state, /no_copy


  RETURN, mainbase

END
