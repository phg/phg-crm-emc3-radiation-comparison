; Copyright (c) 1995 Strathclyde University .
; SCCS info: Module @(#)$Header: /home/adascvs/idl/adaslib/gui/popin.pro,v 1.3 2004/07/06 14:35:54 whitefor Exp $ Date $Date: 2004/07/06 14:35:54 $
;
;+
; PROJECT:
;       ADAS IBM MVS to DEC UNIX conversion
;
; NAME:
;       POPIN()
;
; PURPOSE:
;       Produces a popup window with a warning message and a typein box.
;
; EXPLANATION:
;       This is simply a way of prompting the user to enter a numerical
;	value. A box pops up, with an optional message and title and
;	a typein text widget. When the user enters a value it is checked
;	to ensure that it is a good number and if it is the window is
;	closed and the value passed back to the calling routine, 
;	otherwise the user must try again.
;
; INPUTS:
;       None
;
; OPTIONAL INPUTS:
;       None
;
; OUTPUTS:
;       The return value of this function is the string of the number
;	entered by the user.
;
; OPTIONAL OUTPUTS:
;       None
;
; KEYWORD PARAMETERS:
;       MESSAGE - A string; A message which appears in the widget.
;                 Default 'Please enter a value: '
;
;       TITLE   - A string; The title of the popup window.  Default 'Warning!'
;
;       XOFFSET - Integer; X offset of pop-up window in pixels. Default 500.
;
;       YOFFSET - Integer; Y offset of pop-up window in pixels. Default 500.
;
;       FONT    - A font to use for all text in this widget.
;
; CALLS:
;       NUM_CHK - to check that the input is a valid number.
;	XMANAGER
;
; SIDE EFFECTS:
;       One other routine is included which ise used to manage the
;       widget;
;
;       POPIN_EVENT
;
; CATEGORY:
;       Compound Widget
;
; WRITTEN:
;       Tim Hammond, Tessella Support Services plc, 6th October 1995
;
; MODIFIED:
;       1.1	Tim Hammond
;		First release.
;	1.2	Tim Hammond
;		Tidied up comments and code.
;	1.3	William Osborn
;		Added value keyword so that default can be given. Also, 'value'
;		must be defined in the set-up in case the user kills the window
;		without having entered a number, leaving it undefined and
;		causing Adas to crash.
;
; VERSION:
;       1.1	06-10-95
;	1.2	07-11-95
;	1.3	07-05-96
;
;-
;-----------------------------------------------------------------------------

PRO popin_event, event

    COMMON popin_com, value


                ;**** Base ID of compound widget ****

    parent=event.handler

                ;**********************************************
                ;**** Retrieve the user value state        ****
                ;**** Get id of first_child widget because ****
                ;**** user value "state" is stored there   ****
                ;**********************************************

    first_child = widget_info(parent, /child)
    widget_control, first_child, get_uvalue=state, /no_copy

                ;*********************************
                ;**** Clear previous messages ****
                ;*********************************

    widget_control, state.messid, set_value=' '

                ;************************
                ;**** Process events ****
                ;************************

    destroy = 0
    CASE event.id OF
	state.userval: begin
	    widget_control, state.userval, get_value=userstring
	    if num_chk(userstring(0)) eq 0 then begin
		value = double(userstring)
		widget_control, event.top, /destroy
		destroy = 1
	    endif else begin
		messvalue = 'Invalid entry, please try again'
		widget_control, state.messid, set_value=messvalue
		destroy = 0
	    endelse
	end

	ELSE:			;do nothing

    ENDCASE

                ;*********************************************
                ;*** make "state" available to other files ***
                ;*********************************************

    if destroy eq 0 then widget_control, first_child, set_uvalue=state, /no_copy

END

;-----------------------------------------------------------------------------

FUNCTION popin, MESSAGE=message, TITLE=title, XOFFSET=xoffset, 		$
                YOFFSET=yoffset, FONT=font, VALUE = value_in

    COMMON popin_com, value

    ON_ERROR, 2

                ;**** Set defaults for keywords ****
		;**** Default value is set later ***
    IF NOT (KEYWORD_SET(message)) THEN message = 'Please enter a value: '
    IF NOT (KEYWORD_SET(title)) THEN title = 'Warning!'
    IF NOT (KEYWORD_SET(xoffset)) THEN xoffset = 500
    IF NOT (KEYWORD_SET(yoffset)) THEN yoffset = 500
    IF NOT (KEYWORD_SET(font)) THEN font = ''

                ;**** create titled base widget ****

    topparent = widget_base(title=title, xoffset=xoffset,		$
                            yoffset=yoffset, /column)

                ;******************************************************
                ;**** Create a dummy widget just to hold value of *****
                ;**** "state" variable so as not to get confused  *****
                ;**** with any other values. Adopt IDL practice   *****
                ;**** of using first child widget                 *****
                ;******************************************************

    first_child = widget_base(topparent)

    parent = widget_base(first_child, /column)

                ;**** Message ****

    rc = widget_label(parent, value=message, font=font)

		;**** Typein ****

    IF NOT (KEYWORD_SET(value_in)) THEN begin
	value = 0.0
        userval = widget_text(parent, xsize=8, font=font, /editable)
    endif else begin
	value = value_in
        userval = widget_text(parent, value=string(value,format='(e12.4)'), $
				 xsize=8, font=font, /editable)
    endelse

		;**** warning message ****

    messid = widget_label(parent, value=' ', font=font)

                ;*************************************************
                ;**** create a state structure for the pop-up ****
                ;****                window.                  ****
                ;*************************************************

    new_state = {userval	:	userval,			$
	         messid		:	messid				}

                 ;**** Save initial state structure ****

    widget_control, first_child, set_uvalue=new_state, /no_copy

                ;**** realize the widget ****

    widget_control, topparent, /realize
    widget_control, userval, /input_focus

                ;**** make widget modal ****

    xmanager, 'popin', topparent, /modal, /just_reg

                ;**** Return the output value from common ****

    RETURN, value

END
