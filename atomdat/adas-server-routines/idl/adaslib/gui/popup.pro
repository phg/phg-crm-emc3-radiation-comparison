; Copyright (c) 1995 Strathclyde University .
; SCCS info: Module @(#)$Header: /home/adascvs/idl/adaslib/gui/popup.pro,v 1.7 2004/07/06 14:36:19 whitefor Exp $ Date $Date: 2004/07/06 14:36:19 $
;
;+
; PROJECT:
;       ADAS IBM MVS to DEC UNIX conversion
;
; NAME:
;	POPUP()
;
; PURPOSE:
;	Produces a popup window with a warning message and action buttons.
;
; EXPLANATION:
;	This is very like a menu of horizontal buttons but the window
;	title can be set and an extra warning message is included in the
;	widget.  This function is intended for general use where the user
;	is warned of consequences of an action and asked to confirm.
;	For example 'Do You Really Want to xxxx? Y/N'.  However with
;	suitable titles and messages it could be used for many purposes.
;
;	Consider using the IDL supplied menus as an alternative to this
;	routine, they may better suit your purposes.
;
; USE:
;	The following example produces a popup window with the three
;	buttons 'Replace', 'Append' and 'Cancel'.
;
;		buttons = ['Replace','Append','Cancel']
;		title = 'File Warning'
;		message = 'The Output File Already Exists...'
;		action = popup(message=message,title=title,buttons=buttons)
;		print,'User selected ',action
;
; INPUTS:
;       None
;
; OPTIONAL INPUTS:
;	None
;
; OUTPUTS:
;       The return value of this function is the text label of the button
;       selected by the user.
;
; OPTIONAL OUTPUTS:
;       None
;
; KEYWORD PARAMETERS:
;	MESSAGE	- A string or array of strings; A message which appears
;		  in the widget.  Default 'Cancel or Continue?'
;
;	TITLE	- A string; The title of the popup window.  Default 'Warning!'
;
;	BUTTONS	- A string array; Each element of the array gives the
;		  text label for a button.  Default ['Cancel','Continue']
;
;	XOFFSET	- Integer; X offset of pop-up window in pixels. Default 500.
;
;	YOFFSET	- Integer; Y offset of pop-up window in pixels. Default 500.
;
;	COLUMN	- Set this keyword to arrange the buttons in a column.
;		  The default is for the buttons to appear in a row.
;
;       FONT	- A font to use for all text in this widget.
;
;	LINE2	- A string; if the message is too long for a single line
;		  set this keyword to the message required for the second
;		  line. With message an array, this is the last line.
;
; CALLS:
;       See side effects for other related widget management routines.
;
; SIDE EFFECTS:
;       One other routine is included which ise used to manage the
;       widget;
;
;	POPUP_EVENT
;
; CATEGORY:
;	Compound Widget?
;
; WRITTEN:
;       Andrew Bowen, Tessella Support Services plc, 21-May-1992
;
; MODIFIED:
;       1.1     Andrew Bowen 
;               First release.
;	1.2	Andrew Bowen
;               Header comments added.
;	1.3	Lalit Jalota
;	1.4	Lalit Jalota
;	1.5	Tim Hammond	
;		Replaced outdated references to CW_LOADSTATE and
;		CW_SAVESTATE.
;	1.6	Tim Hammond
;		Added keyword LINE2
;	1.7	William Osborn
;		Added capability to use an array of strings
;
; VERSION:
;       1.1     21-05-93
;	1.2	02-06-95
;	1.3	13-03-95
;	1.4	13-03-95
;	1.5	23-05-95
;	1.6	05-03-96
;	1.7	16-04-96
;-
;-----------------------------------------------------------------------------

PRO popup_event, event

    COMMON popup_com, action

		;**** Get button pressed ****

    widget_control,event.id,get_uvalue=button

		;**** Destroy the widget ****
    widget_control,event.top,/destroy

		;**** Copy the action to common ****
    action = button

END

;-----------------------------------------------------------------------------

FUNCTION popup, MESSAGE=message, TITLE=title, BUTTONS=buttons, 		$
		XOFFSET=xoffset, YOFFSET=yoffset, COLUMN=column, 	$
                FONT=font, LINE2=line2

  COMMON popup_com,action


		;**** Set defaults for keywords ****

  IF NOT (KEYWORD_SET(message)) THEN message='Cancel or Continue?'
  IF NOT (KEYWORD_SET(title)) THEN title='Warning!'
  IF NOT (KEYWORD_SET(buttons)) THEN buttons=['Cancel','Continue']
  IF NOT (KEYWORD_SET(xoffset)) THEN xoffset=500
  IF NOT (KEYWORD_SET(yoffset)) THEN yoffset=500
  IF NOT (KEYWORD_SET(column)) THEN column=0
  IF NOT (KEYWORD_SET(font)) THEN font=''
  IF NOT (KEYWORD_SET(line2)) THEN line2=''

		;**** determine the number of buttons ****

  numbut = size(buttons)
  numbut = numbut(1)

		;**** create titled base widget ****
  parent = widget_base(title=title, $
			xoffset=xoffset,yoffset=yoffset,/column)

		;**** Message ****
  sz=size(message)
  if sz(0) eq 0 then begin
    	rc = widget_label(parent,value=message,font=font)
  endif else begin
	for i=0,sz(1)-1 do begin
	    rc=widget_label(parent,value=message(i),font=font)
	endfor
  endelse

		;**** Optional last line ****
  if line2 ne '' then begin
	rc2 = widget_label(parent, value=line2, font=font)
  endif

		;**** action buttons ****

  if column eq 0 then begin
    base = widget_base(parent,/row)
  end else begin
    base = widget_base(parent,/column)
  end
  for i = 0, numbut-1 do begin
    rc = widget_button(base,value=buttons(i),uvalue=buttons(i),font=font)
  end
  
		;**** realize the widget ****
  widget_control,parent,/realize

		;**** make widget modal ****
  xmanager,'popup',parent,event_handler='popup_event',/modal,/just_reg
 
		;**** Return the output value from common ****
  RETURN, action

END

