; Copyright (c) 1995 Strathclyde University .
; SCCS info: Module @(#)$Header: /home/adascvs/idl/adaslib/gui/adas_menu.pro,v 1.4 2004/07/06 11:10:39 whitefor Exp $ Date $Date: 2004/07/06 11:10:39 $
;
;+
; PROJECT:
;	ADAS IBM MVS to DEC UNIX conversion.
;
; NAME:
;	ADAS_MENU()
;
; PURPOSE:
;	Produces a pop-up menu window and returns the user's selection.
;
; EXPLANATION:
;	This pop-up menu window is used in preference to the IDL
;	supplied menu routines simply because it looks better.  This
;	pop-up menu uses the widget cw_adas_menu.pro which has
;	exclusive toggle buttons for the menu options.  This causes
;	all menu text to be left aligned and looks better than 
;	plain buttons.  This function returns the index of the
;	selected item or -1 if the 'Exit' button was pressed.
;	A caller defined title is provided to head the menu.
;
; USE:
;	list = ['Red','Green','Blue','Brown','Black']
;	ind = adas_menu(list,header='Colour Choices')
;	if ind ge 0 then begin
;	  print,'Menu option chosen:  ',list(ind)
;	end else begin
;	  print,'Exit button pressed'
;	end
;
; INPUTS:
;	LIST	- A string array; a list of menu options, one
;		  array element per option.
;
; OPTIONAL INPUTS:
;	None.
;
; OUTPUTS:
;	This function returns the index of the item selected from the
;	menu or -1 if the 'Exit' button was pressed.
;
; OPTIONAL OUTPUTS:
;	None.
;
; KEYWORDS:
;	TITLE	- A string; title for the pop-up window.
;
;	HEADER	- A string array; header to appear at the top of the
;		  menu.  The array may be of any length, each element
;		  starts a new line in the widget.
;
;	FONT	- A string; the name of a font to use for all text
;		  in the menus.  Defaults to default system font.
;
;	XOFFSET	- X offset of the pop-up window in pixels.
;
;	YOFFSET	- Y offset of the pop-up window in pixels.
;
; CALLS:
;	CW_ADAS_MENU	Declares the menu widget.
;	ADAS_MENU_EVENT	Included in this file, called through XMANAGER
;			during event management.
;	XMANAGER
;
; SIDE EFFECTS:
;	None.
;
; CATEGORY:
;	Widgets.
;
; WRITTEN:
;       Andrew Bowen, Tessella Support Services plc, 24-May-1993
;
; MODIFIED:
;       Version 1       Andrew Bowen    1-Jun-1993
;                       First release.
;
; VERSION:
;       1       1-Jun-1993
;-
;-----------------------------------------------------------------------------

PRO adas_menu_event, event

  COMMON menu_com, exitid, select

		;**** establish what event occured ****
  if event.id eq exitid then begin

    select = -1

  end else begin

    select = event.select

  end

		;**** destroy the menu window ****
  widget_control,event.top,/destroy

END


;-----------------------------------------------------------------------------



FUNCTION adas_menu, list, TITLE=title, HEADER=header, FONT=font, $
			  XOFFSET=xoffset, YOFFSET=yoffset

		;**** private common, id of selection widget and ****
		;**** the list of selections.                    ****
  COMMON menu_com, exitid, select

		;**** Set defaults for keywords ****
  IF NOT (KEYWORD_SET(title)) THEN title = 'ADAS Menu'
  IF NOT (KEYWORD_SET(header)) THEN header = ''
  IF NOT (KEYWORD_SET(font))  THEN font = ''
  IF NOT (KEYWORD_SET(xoffset)) THEN xoffset = 200
  IF NOT (KEYWORD_SET(yoffset)) THEN yoffset = 200

		;**** create titled base widget ****
  base = widget_base(title=title,XOFFSET=xoffset,YOFFSET=yoffset,/column)

		;**** menu header ****
  if strtrim(header(0)) ne '' then begin
		;**** see how many lines in the header ****
    headsize = size(header)
    if headsize(0) gt 0 then begin
      nlines = headsize(1)
    end else begin
      nlines = 1
    end
		;**** Add header to window ****
    for i = 0, nlines-1 do begin
      rc = widget_label(base,value=header(i),font=font)
    end
  end

		;**** Add the menu buttons ****
  cwid = cw_adas_menu(base,list,font=font)

		;**** add the exit button ****
  butbase = widget_base(base,/row)
  exitid = widget_button(butbase,value='Exit',font=font)
  
  widget_control,base,/realize

		;**** make widget modal and wait until destroyed ****
  xmanager,'adas_menu',base,event_handler='adas_menu_event',/modal,/just_reg
 
		;**** return sels to the caller ****
  RETURN, select

END
