; Copyright (c) 1995 Strathclyde University .
; SCCS info: Module @(#)$Header: /home/adascvs/idl/adaslib/gui/cw_polyfit.pro,v 1.4 2004/07/06 13:09:06 whitefor Exp $ Date $Date: 2004/07/06 13:09:06 $
;
;+
; PROJECT:
;       ADAS IBM MVS to DEC UNIX conversion
;
; NAME:
;	CW_POLYFIT()
;
; PURPOSE:
;	A widget to switch on Minimax Polynomial fitting and specify
;	a tolerance.
;
; EXPLANATION:
;	This compound widget is for general use where Minimax Polynomial
;	fitting is available.  It consists of a labelled toggle button
;	and a labelled editable text widget.  The button is used to
;	'switch-on' fitting and the text widget is for the entry of
;	a numeric tolerance parameter.  The text widget is automatically
;	de-sensitised when the toggle button is off.
;
;	The widget-value for this compound widget is a structure;
;
;	{ TOL:'', FITBUT:0 }
;
;	TOL is the tolerance value as a string.  This widget does not
;	check that the string in TOL is a valid number representation,
;	it is the caller's responsibility to verify the value of TOL.
;	FITBUT represents the state of the toggle button.  A value of 1
;	is button on and 0 is button off.  Note when the toggle button
;	is off the TOL value is hidden although it is still saved as
;	part of the 'value' of the widget.
;
;	This widget does not generate any events.
;
; USE:
;	Normally this widget would be used as part of a larger
;	collection of widgets.  Remember to check the TOL string
;	with NUM_CHK and make sure it is in the range 0-100%.
;
;	base = widget_base(/column)
;	cwid = cw_polyfit(base,font='courier_bold14')
;	rc = widget_button(base,value='Done')
;	widget_control,base,/realize
;	rc=widget_event()
;	widget_control,cwid,get_value=value
;	if value.fitbut eq 1 then begin
;	  error = num_chk(value.tol)
;	  if error eq 0 then begin
;	    print,'Minimax Polynomial Fitting Activated'
;	    if float(value.tol) lt 0.0 or float(value.tol) gt 100.0 then begin
;	      print,'Tolerance out of range'
;	    end else begin
;	      print,'Tolerance ',value.tol,'%'
;	    end
;	  end else begin
;	    print,'Tolerance value is not a number'
;	  end
;	end else begin
;	  print,'Polynomial Fitting not Activated'
;	end
;
; INPUTS:
;       PARENT   - The ID of the parent widget.
;
; OPTIONAL INPUTS:
;	None.
;
; OUTPUTS:
;       The return value of this function is the ID of the compound
;       widget which is of type LONG integer.
;
; OPTIONAL OUTPUTS:
;       None
;
; KEYWORD PARAMETERS:
;	VALUE	 - The initial value of the widget representing the
;		   start-up settings.  The value is a structure defined
;		   in the Explanation section above.  The default value is;
;		   { TOL:'', FITBUT:0 }
;
;       FONT     - A font to use for all text in this widget.  Defaults
;		   to the current system default.
;
;	UVALUE	 - A user value for this widget.
;
; CALLS:
;       CW_LOADSTATE    Recover compound widget state.
;       CW_SAVESTATE    Save compound widget state.
;       See side effects for other related widget management routines.
;
; SIDE EFFECTS:
;	This widget uses a COMMON BLOCK: CW_POLYFIT_BLK to hold the
;       widget state.
;
;       Three other routines are included which are used to manage the
;       widget;
;
;	POLYFIT_SET_VAL
;	POLYFIT_GET_VAL()
;	POLYFIT_EVENT()
;
; CATEGORY:
;	Compound Widget
;
; WRITTEN:
;       Andrew Bowen, Tessella Support Services plc, 10-Jun-1993
;
; MODIFIED:
;       Version 1       Andrew Bowen    10-Jun-1993
;                       First release.
;	1.4	Richard Martin
;		Removed obsolete cw_loadstate/savestate statements.
;
; VERSION:
;       1       10-Jun-1993
;	1.4	10-01-02
;-
;-----------------------------------------------------------------------------

PRO polyfit_set_val, id, value



		;**** Return to caller on error ****
  ON_ERROR, 2

		;**** Retrieve the state ****

  first_child = widget_info(id,/child)
  widget_control, first_child, get_uvalue=state, /no_copy

		;*************************
		;**** Update settings ****
		;*************************
  if value.fitbut eq 1 then begin
    widget_control,state.fitid,set_button=1
    widget_control,state.tolbase,/sensitive
    widget_control,state.tolid,set_value=value.tol
  end else begin
    widget_control,state.fitid,set_button=0
    widget_control,state.tolbase,sensitive=0
    widget_control,state.tolid,set_value=''
  end

		;**** Copy the new value to state structure ****
  state.fitset.fitbut = value.fitbut
  state.fitset.tol = value.tol

		;**** Save the new state ****

  widget_control, first_child, set_uvalue=state, /no_copy

END

;-----------------------------------------------------------------------------


FUNCTION polyfit_get_val, id


		;**** Return to caller on error ****
  ON_ERROR, 2

		;**** Retrieve the state ****

  first_child = widget_info(id,/child)
  widget_control, first_child, get_uvalue=state, /no_copy

		;**** Get the latest tolerance value ****
  if state.fitset.fitbut eq 1 then begin
    widget_control,state.tolid,get_value=value
    state.fitset.tol=value(0)
  end

		;**** Reset the value of state variable ****
		
  fitset=state.fitset
  widget_control, first_child, set_uvalue=state, /no_copy

  RETURN, fitset

END

;-----------------------------------------------------------------------------

FUNCTION polyfit_event, event


		;**** Base ID of compound widget ****
  base=event.handler

		;**** Retrieve the state ****

  first_child = widget_info(base, /child)
  widget_control, first_child, get_uvalue = state, /no_copy

		;************************
		;**** Process Events ****
		;************************
  CASE event.id OF

		;**** toggle state of widget with button ****
    state.fitid: begin
		if state.fitset.fitbut eq 1 then begin
		  state.fitset.fitbut = 0
		  widget_control,state.tolbase,sensitive=0
		  widget_control,state.tolid,set_value=''
		end else begin
		  state.fitset.fitbut = 1
		  widget_control,state.tolbase,/sensitive
		  widget_control,state.tolid,set_value=state.fitset.tol
		  widget_control,state.tolid,/input_focus
		end
    end

    ELSE:

  ENDCASE

		;**** Save the new state structure ****

  widget_control, first_child, set_uvalue=state, /no_copy
    
  RETURN, 0L
END

;-----------------------------------------------------------------------------

FUNCTION cw_polyfit, topparent, VALUE=value, $
			FONT=font, UVALUE=uvalue


  IF (N_PARAMS() LT 1) THEN MESSAGE, 'Must specify PARENT for cw_polyfit'

		;**** Set defaults for keywords ****
  IF NOT (KEYWORD_SET(value)) THEN begin
		     fitset = {fitset, TOL:'', FITBUT:0}
		end else begin
		     fitset = {fitset, TOL:value.tol, FITBUT:value.fitbut}
		end
  IF NOT (KEYWORD_SET(font)) THEN font = ''
  IF NOT (KEYWORD_SET(uvalue))  THEN uvalue = 0


		;**** Create the main base for the widget ****
  parent = WIDGET_BASE(topparent, UVALUE = uvalue, $
		EVENT_FUNC = "polyfit_event", $
		FUNC_GET_VALUE = "polyfit_get_val", $
		PRO_SET_VALUE = "polyfit_set_val", $
		/COLUMN)

		;**** Create base to hold the value of state ****

  first_child = widget_base(parent)

  cwid = widget_base(first_child,/column)
    
		;**** Fit polynomial button ****
  butbase = widget_base(cwid,/column,/nonexclusive)
  fitid = widget_button(butbase,value='Fit Minimax Polynomial',font=font)

		;**** Tolerance input ****
  tolbase = widget_base(cwid,/row)
  rc = widget_label(tolbase,value='Tolerance (0-->100%)',font=font)
  tolid = widget_text(tolbase,/edit,xsize=5,font=font)


		;******************************
		;**** Set startup settings ****
		;******************************
  if fitset.fitbut eq 1 then begin
    widget_control,fitid,set_button=1
    widget_control,tolid,set_value=fitset.tol
  end else begin
    widget_control,fitid,set_button=0
    widget_control,tolbase,sensitive=0
  end

		;**** Create state structure ****
  new_state =  {FITID:fitid, TOLBASE:tolbase, TOLID:tolid, FITSET:fitset}

		;**** Save initial state structure ****

  widget_control, first_child, set_uvalue=new_state, /no_copy

  RETURN, parent

END
