; Copyright (c) 1995 Strathclyde University .
; SCCS info: Module @(#)$Header: /home/adascvs/idl/adaslib/gui/cw_adas_outfile.pro,v 1.22 2004/07/06 13:00:30 whitefor Exp $   Date $Date: 2004/07/06 13:00:30 $
;+
; PROJECT:
;       ADAS
;
; NAME:
;	CW_ADAS_OUTFILE()
;
; PURPOSE:
;	Produces a widget used for naming an output file.
;
; EXPLANATION:
;	This widget consists of a titled activation button and a text
;	widget for file name entry.  Optionally it may also include
;	a 'Default' file name button, a 'Replace' button and an 'Append'
;	button.  When the activation toggle button is on all of the
;	widget accepts interaction, when the toggle is off all other
;	elements of the widget become desensitized.
;
;	This widget checks to see if the user has write access to the
;	named output file.  This checking is initiated when the
;	value of the widget is recovered using widget_control,id,get_value.
;	If the file exists and is writable the widget checks to see
;	if the replace or append buttons have been set, if they are
;	available.  If not the widget produces a popup window asking
;	the user to Cancel the output or request Replace or Append if
;	those were available.  Which ever button the user presses the
;	value and state of the widget are modified to reflect that
;	action. i.e the 'Cancel' button will cause the activation
;	button to be turned off and the 'Replace' button will cause
;	the replace toggle to be set.  These changes are also made
;	in the value returned by get_value.  The appearence of the
;	popup window and the resulting changes to value and state are
;	transparent to the caller.  The caller only sees the final
;	value of the widget.
;
;	If an error has occured in the specification of the file name,
;	for example the user does not have write permission or the
;	directory path name is incorrect, the widget will set an error
;	message.  Again these checks are only performed when get_value
;	is used.  The caller should test for an error message in order
;	to decide if the supplied file name is valid.
;
;	This widget does not generate any events.
;
; USE:
;	An example of how to use the widget; this example produces
;	a window with the outfile widget and a 'Done' button.  On
;	startup the activation button is off.  There is no 'Append'
;	button but there is a 'Replace' button which is set off.
;	No default file name is specified and so the 'Default' button
;	does not appear.  Activate the output, type in a file name
;	and then press 'Done' to see the effect of get_value.  Type
;	in the name of an existing file with the replace button off
;	to see the popup window.
;
;	value={ OUTBUT:0, APPBUT:-1, REPBUT:0, $
;		FILENAME:'', DEFNAME:'', MESSAGE:''}
;	base=widget_base(/column)
;	outid=cw_adas_outfile(base,value=value)
;	rc=widget_button(base,value='Done')
;	widget_control,base,/realize
;	rc=widget_event()
;	widget_control,outid,get_value=value
;
;	if strtrim(value.message) eq '' then begin
;	  if value.outbut eq 1 then begin
;	    print,'Output file: ',value.filename
;	    if value.repbut eq 1 then print,'Replacement requested'
;	  end else begin
;	    print,'No output requested'
;	  end
;	end else begin
;	  print,'Output file specification error;'
;	  print,value.message
;	end
;
; INPUTS:
;       PARENT   - The ID of the parent widget.
;
; OPTIONAL INPUTS:
;       EVENT_FUNCT - The name of an optional user-supplied event function.
;                     This function is called with the return value
;                     structure whenever an action occurs.
;       XSIZE       - Widthin characters of output filenames.
;
;
; OUTPUTS:
;       The return value of this function is the ID of the compound
;       widget which is of type LONG integer.
;
; OPTIONAL OUTPUTS:
;       None
;
; KEYWORD PARAMETERS:
;	OUTPUT	- A string; Label to use for activate button.  Default
;		  'Write Output'.
;
;	VALUE	- A structure which defines the value of this widget.
;		  { OUTBUT:0, APPBUT:-1, REPBUT:0, $
;		    FILENAME:'', DEFNAME:'', MESSAGE:''}
;
;		  OUTBUT    Integer; 1 activate button on, 0 off
;		  APPBUT    Integer; 1 append button on, 0 off, -1 no button
;		  REPBUT    Integer; 1 replace button on, 0 off, -1 no button
;		  FILENAME  String; output file name
;		  DEFNAME   String; default file name, if null then no button
;		  MESSAGE   String; error message text
;
;		  Default;
;		  { OUTBUT:0, APPBUT:-1, REPBUT:0, $
;		    FILENAME:'', DEFNAME:'', MESSAGE:''}
;
;       FONT    - A font to use for all text in this widget.
;
;	UVALUE	- A user value for this widget.
;
;	NOTOGGLE- Set to toggle widget sensitivity.
;
;       DIRECTORY-Set to allow the output filename to be a directory.
;
; CALLS:
;	FILE_ACC	Determine file existence and access permissions.
;	POPUP		Popup warning window.
;       See side effects for other related widget management routines.
;
; SIDE EFFECTS:
;       Three other routines are included which are used to manage the
;       widget;
;
;	OUTFILE_SET_VAL
;	OUTFILE_GET_VAL()
;	OUTFILE_EVENT()
;
; CATEGORY:
;	Compound Widget
;
; WRITTEN:
;       Andrew Bowen, Tessella Support Services plc, 7-Apr-1993
;
; MODIFIED:
;       1       Andrew Bowen    2-Jun-1993
;               First release.
;
;	1.1	Lalit Jalota	
;		Added checks to input title length.
;
;	1.16	Tim Hammond
;		Replaced outdated calls to cw_loadstate and 
;		cw_savestate
;
;	1.17	Tim Hammond
;		Increased allowed filename length to 80 characters.
;
;	1.18	William Osborn
;		Removed xsize=n keyword from widget_label command.
;		The dynlabel procedure does not override these
;		assignments and they were taken as being values in pixels.
;
;	1.19	David Brooks
;		Cleared error messages after each event. Added notoggle 
;               keyword.
;
;       1.20    Martin O'Mullane
;               Added the EVENT_FUNCT keyword which allows for an optional 
;               user supplied event handler. Return a standard event structure
;               with additional elements - activate (1 request, 0 turn off) and active 
;               (1 on, 0 off). Also added an xsize keyword, which defaults
;                to 40 (as before) if not set.
;
;       1.21    Allan Whiteford
;               Added the DIRECTORY keyword which allows the output file
;               to be a directory name.        
;	1.22	Richard Martin
;		Removed select tag from ret_u structure in outfile_event that
;		appears to be undefined, hence causes crashes.
;
; VERSION:
;       1       2-Jun-1993
;	1.1	21-Feb-1995
;	1.16	23-May-1995
;	1.17	14-Nov-1995
;	1.18	01-08-96
;	1.19	24-04-97
;	1.19	21-08-2000
;	1.20	10-11-2001
;	1.21	14-11-2001
;	1.22	13-03-02
;-
;-----------------------------------------------------------------------------

PRO outfile_set_val, id, value


		;**** Return to caller on error ****
  ON_ERROR, 2

                ;**********************************************
                ;**** Retrieve the user value state        ****
                ;**** Get id of first_child widget because ****
                ;**** user value "state" is stored there   ****
                ;**********************************************

  first_child = widget_info(id, /child)
  widget_control, first_child, get_uvalue=state,/no_copy

		;**** Sensitise or desensitise with output button setting ****
  if value.outbut eq 0 then begin

    widget_control,state.actid,set_button=0
    if state.appid gt 0 then widget_control,state.appid,sensitive=0
    if state.repid gt 0 then widget_control,state.repid,sensitive=0
    if state.defid gt 0 then widget_control,state.defid,sensitive=0
    widget_control,state.fbaseid,sensitive=0
		;**** Hide the button settings ****
    if state.appid ne 0 then widget_control,state.appid,set_button=0
    if state.repid ne 0 then widget_control,state.repid,set_button=0

  end else begin

    widget_control,state.actid,set_button=1
    if state.appid gt 0 then widget_control,state.appid,/sensitive
    if state.repid gt 0 then widget_control,state.repid,/sensitive
    if state.defid gt 0 then widget_control,state.defid,/sensitive
    widget_control,state.fbaseid,/sensitive
    widget_control,state.fileid,set_value=value.filename

		;**** Set the append button ****
    if state.appid ne 0 then widget_control,state.appid,set_button=value.appbut

		;**** Set the replace button ****
    if state.repid ne 0 then widget_control,state.repid,set_button=value.repbut
  end


		;**** Copy message to widget ****
  if strtrim(value.message) ne '' then begin
    message = value.message
  end else begin
    message = ' '
  end
  widget_control,state.messid,set_value=message

		;**** Copy the new value to state structure ****
  state.outfval.outbut = value.outbut
  state.outfval.appbut = value.appbut
  state.outfval.repbut = value.repbut
  state.outfval.filename = value.filename
  state.outfval.defname = value.defname
  state.outfval.message = value.message

		;**** Save the new state ****
  widget_control, first_child, set_uvalue=state, /no_copy

END

;-----------------------------------------------------------------------------


FUNCTION outfile_get_val, id


		;**** Return to caller on error ****
  ON_ERROR, 2

               ;***************************************
               ;****     Retrieve the state        ****
               ;**** Get first_child widget id     ****
               ;**** because state is stored there ****
               ;***************************************

  first_child = widget_info(id, /child)
  widget_control, first_child, get_uvalue = state

		;**** Get the latest filename ****
  if state.outfval.outbut eq 1 then begin
    widget_control,state.fileid,get_value=filename
    filename = filename(0)

		;*** limit filename to 80 characters ***
    if (strlen(filename) gt 80 ) then begin
       message = 'Warning : Filename has been truncated'
       widget_control,state.messid,set_value=message
       message = '' 
    endif
    filename = strmid(filename, 0,80)

		;**********************************************
		;**** Perform checks on the specified file ****
		;**********************************************
    error = 0

		;**** Check that a file name has been provided ****
    if error eq 0 then begin
      if strtrim(filename,2) eq '' then begin
	fileexist = -1
	error = 1
	message = 'Error: No file name has been entered.'
      end
    end

		;**** Check if file already exists ****
    if error eq 0 then begin
      file_acc,filename,fileexist,read,write,execute,filetype
      if filetype eq 'd' and state.directory eq 0 then begin
	error = 1
	message = 'Error: The supplied name is a directory.'
      end
    end

		;**** If file doesn't exist check path name is correct ****
    if error eq 0 and fileexist eq 0 then begin

		;**** Find the end of the directory path ***
      pathend = -1
      i = strlen(filename) - 1
      while (i gt 0 and pathend eq -1) do begin
        if strmid(filename,i,1) eq '/' then pathend = i
        i = i - 1
      end

		;**** Extract directory path ****
      if pathend gt 0 then begin
        path = strmid(filename,0,pathend)
      end else begin
        path = '.'
      end

		;**** Check path ****
      file_acc,path,pathexist,read,write,execute,filetype
       if pathexist eq 0 or filetype ne 'd' then begin
         error = 1
	 message = 'Error: The path name is invalid.'
       end

    end

		;**** Check for write access ****
		;**** Look for write access to an existing file     ****
		;**** or for write and execute access on the path   ****
		;**** for a new file.  This will also trap any      ****
		;**** path which passes the previous test but isn't ****
		;**** really a directory.                           ****
    if error eq 0 then begin
      if fileexist eq 1 then begin
	if write ne 1 then begin
	  error = 1
	  message = 'Error: No write permission for file.'
        end
      end else begin
	if write ne 1 or execute ne 1 then begin
          error = 1
          message = 'Error: No write permission for directory.'
        end
      end
    end

		;**** If the file already exists and no overwrite ****
		;**** or append has been requested prompt user.   ****
    if error eq 0 then begin
      if fileexist eq 1 and filetype ne 'd' then begin
        if state.repid gt 0 or state.appid gt 0 then begin
	  if state.outfval.repbut ne 1 and state.outfval.appbut ne 1 then begin
		;**** Prompt user for file overwrite/append ****
	    buttons = 'Cancel'
	    if state.repid gt 0 then buttons = [buttons,'Replace']
	    if state.appid gt 0 then buttons = [buttons,'Append']
	    action = popup(title='File Warning', $
			   message='File '+filename+' already exists.', $
			   buttons=buttons, FONT=state.font)
			
	    CASE action OF
		'Replace': state.outfval.repbut = 1
		'Append' : state.outfval.appbut = 1
		'Cancel' : begin
				state.outfval.outbut = 0
			end
	    ENDCASE
          end
        end else begin
	  error = 1
	  message = 'Error: The file already exists.'
        end
      end
    end

		;**** Check that append action is to the currently ****
		;**** open file, name stored as default.           ****
    if error eq 0 and state.outfval.appbut eq 1 then begin
      if filename ne state.outfval.defname then begin
        error = 1
	message = 'Error: Output may only be appended to the last '+ $
		  'file output.'
      end
    end

		;**** If an error has occured set the error message.  ****
    if error eq 1 then begin
      state.outfval.message = message
    end else begin
      state.outfval.message = ' '
    end


    state.outfval.filename = filename

  end else begin
    state.outfval.message = ' '
  end

		;**** This GET_VALUE routine can change the value ****
		;**** of the widget so call SET_VALUE to update   ****
		;**** the screen.                                 ****
  widget_control,id,set_value=state.outfval

  RETURN, state.outfval

END

;-----------------------------------------------------------------------------

FUNCTION outfile_event, event


		;**** Base ID of compound widget ****
  base=event.handler

               ;***************************************
               ;****     Retrieve the state        ****
               ;**** Get first_child widget id     ****
               ;**** because state is stored there ****
               ;***************************************

  first_child = widget_info(base, /child)
  widget_control, first_child, get_uvalue = state

               ;**** clear error messages ****

  widget_control, state.messid, set_value=' '
  state.outfval.message = ' '

		;************************
		;**** Process Events ****
		;************************
  CASE event.id OF

    state.actid: begin
			;**** Sensitise and desensitise with toggle ****
			;**** of output button if toggle allowed.   ****
              if state.notoggle eq 0 then begin

		if state.outfval.outbut eq 1 then begin

		  state.outfval.outbut = 0

			;**** desensitise other widgets ****
		  if state.appid gt 0 then $
					widget_control,state.appid,sensitive=0
		  if state.repid gt 0 then $
					widget_control,state.repid,sensitive=0
		  if state.defid gt 0 then $
					widget_control,state.defid,sensitive=0
		  widget_control,state.fbaseid,sensitive=0

			;**** hide the button settings and copy ****
			;**** the latest filename to state      ****
		  widget_control,state.fileid,get_value=filename
		  state.outfval.filename = filename(0)
		  if state.appid gt 0 then $
					widget_control,state.appid,set_button=0
		  if state.repid gt 0 then $
					widget_control,state.repid,set_button=0

		end else begin

		  state.outfval.outbut = 1
		  if state.appid gt 0 then $
					widget_control,state.appid,/sensitive
		  if state.repid gt 0 then $
					widget_control,state.repid,/sensitive
		  if state.defid gt 0 then $
					widget_control,state.defid,/sensitive
		  widget_control,state.fbaseid,/sensitive

			;**** restore button settings ****
		  if state.appid gt 0 then $
		     widget_control,state.appid,set_button=state.outfval.appbut
		  if state.repid gt 0 then $
		     widget_control,state.repid,set_button=state.outfval.repbut

		end

              end else begin
                widget_control, state.actid, /set_button
              end 
    end


    state.appid: if state.outfval.appbut eq 1 then begin
			state.outfval.appbut=0
		 end else begin
			state.outfval.appbut=1
			if state.repid gt 0 then begin
				widget_control,state.repid,set_button=0
				state.outfval.repbut=0
			end
		 end

    state.repid: if state.outfval.repbut eq 1 then begin
			state.outfval.repbut=0
		 end else begin
			state.outfval.repbut=1
			if state.appid gt 0 then begin
				widget_control,state.appid,set_button=0
				state.outfval.appbut=0
			end
		 end

    state.defid: widget_control,state.fileid,set_value=state.outfval.defname

    ELSE:

  ENDCASE
  if event.id EQ state.actid then activate = 1 else activate = 0
  action = state.outfval.outbut
  ret_u = {id       : event.id,       $
           top      : event.top,      $
           handler  : event.handler,  $
           activate : activate,       $
           action   : action          }
  
		;**** Save the new state structure ****

  efun = state.efun
  widget_control, first_child, set_uvalue=state, /no_copy

; Return a struct or pass to a user supplied event handler?
    
  ret = 0L
  if efun NE '' then return, call_function(efun,ret_u) else return, ret


END

;-----------------------------------------------------------------------------

FUNCTION cw_adas_outfile, parent            ,                   $
                          OUTPUT=output     , VALUE=value,      $ 
                          FONT=font         , UVALUE=uvalue,    $
                          NOTOGGLE=notoggle , EVENT_FUNCT=efun, $
                          XSIZE=xsize       , DIRECTORY=directory



  IF (N_PARAMS() LT 1) THEN MESSAGE, 'Must specify PARENT for cw_adas_reac2'

		;**** Set defaults for keywords ****
  IF NOT (KEYWORD_SET(output)) THEN output = 'Write Output'
  IF NOT (KEYWORD_SET(value)) THEN begin
		     outfval = {outfval, OUTBUT:0 , APPBUT:-1, REPBUT:0, $
					FILENAME:'', DEFNAME:'', MESSAGE:''}
		end else begin
		     outfval = {outfval, OUTBUT:value.outbut, $
					APPBUT:value.appbut, $
					REPBUT:value.repbut, $
					FILENAME:value.filename, $
					DEFNAME:value.defname, $
					MESSAGE:value.message }
		end
  IF NOT (KEYWORD_SET(font)) THEN font = ''
  IF NOT (KEYWORD_SET(uvalue))  THEN uvalue = 0
  IF NOT (KEYWORD_SET(notoggle))  THEN notoggle = 0
  IF NOT (KEYWORD_SET(xsize))  THEN xsize = 40
  IF NOT (KEYWORD_SET(directory))  THEN directory = 0

		;**** Create the main base for the widget ****
  cwid = WIDGET_BASE(parent, UVALUE = uvalue, $
		EVENT_FUNC = "outfile_event", $
		FUNC_GET_VALUE = "outfile_get_val", $
		PRO_SET_VALUE = "outfile_set_val", $
		/COLUMN)

              ;******************************************************
              ;**** Create a dummy widget just to hold value of *****
              ;**** "state" variable so as not to get confused  *****
              ;**** with any other values. Adopt IDL practice   *****
              ;**** of using first child widget                 *****
              ;******************************************************

  first_child = widget_base(cwid)

  topbase = widget_base(first_child,/row)

		;**** Button base ****
  base = widget_base(topbase,/row,/nonexclusive)

		;**** Activate output button ****
  actid = widget_button(base,value=output,font=font)

		;**** Append button ****
  if outfval.appbut ge 0 then begin
    appid = widget_button(base,value='Append',font=font)
  end else begin
    appid = 0L
  end

		;**** Replace button ****
  if outfval.repbut ge 0 then begin
    repid = widget_button(base,value='Replace',font=font)
  end else begin
    repid = 0L
  end

		;**** Button for default file name ****
  if outfval.defname ne '' then begin
    defid = widget_button(topbase,value='Default File Name',font=font)
  end else begin
    defid = 0L
  end

		;**** File name ****
  fbaseid = widget_base(cwid,/row)
  rc = widget_label(fbaseid,value='File Name : ',font=font)
  fileid = widget_text(fbaseid,value=outfval.filename,/edit, $
                       xsize=xsize,font=font)

		;**** Message ****
  if strtrim(outfval.message,2) eq '' then begin
    message = ' '
  end else begin
    message = outfval.message
  end
  messid = widget_label(cwid,value=message,font=font)
    

		;**** Set initial state according to value ****
  if outfval.outbut eq 1 then begin
      widget_control,actid,set_button=1
  end else begin
    if appid gt 0 then widget_control,appid,sensitive=0
    if repid gt 0 then widget_control,repid,sensitive=0
    widget_control,fbaseid,sensitive=0
    if defid gt 0 then widget_control,defid,sensitive=0
  end

  if outfval.outbut eq 1 and outfval.appbut eq 1 then begin
    widget_control,appid,set_button=1
  end

  if outfval.outbut eq 1 and outfval.repbut eq 1 then begin
    widget_control,repid,set_button=1
  end

		;**** Create state structure ****
                
  if n_elements(efun) le 0 then efun = ''
  new_state = { efun      :  efun,       $
                ACTID     :  actid,      $
                APPID     :  appid,      $
                REPID     :  repid,      $
                FBASEID   :  fbaseid,    $
		DEFID     :  defid,      $
                FILEID    :  fileid,     $
                MESSID    :  messid,     $
		OUTFVAL   :  outfval,    $
                FONT      :  font,       $
                NOTOGGLE  :  notoggle,   $
                DIRECTORY :  directory   }

		;**** Save initial state structure ****
  widget_control, first_child, set_uvalue=new_state,/no_copy

  RETURN, cwid

END
