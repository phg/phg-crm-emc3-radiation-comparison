;+
; PROJECT:
;       ADAS
;
; NAME:
;       adas_show_text
;
; PURPOSE:
;       This function puts up a widget to display text arrays. It is
;       non-blocking. Pass the parent widget ID for it to terminate
;       when the calling widget finishes.
;
;
; EXPLANATION:
;
;
; NOTES:
;
; INPUTS:
;        text  - a string array
;        title - information string
;
; OPTIONAL INPUTS:
;       None
;
; OUTPUTS:
;       None
;
; OPTIONAL OUTPUTS:
;       None
;
; KEYWORD PARAMETERS:
;       FONT - The name of a font e.g 'courier_bold14'
;
;
; CALLS:
;
;
; SIDE EFFECTS:
;
; CATEGORY:
;       Widgets
;
; WRITTEN:
;       Martin O'Mullane
;
; MODIFIED:
;       1.1     Martin O'Mullane
;               First release
;
; VERSION:
;       1.1     18-10-2012
;
;-
;-----------------------------------------------------------------------------

PRO adas_show_text_EVENT, event

; Get the info structure out of the user value of the top-level base.

Widget_Control, event.top, Get_UValue=info

; All events come here. Destroy the widget if cancelled. Otherwise grab
; all data to put into an event structure which is passed straight on
; to a named notified widget.
;
; These values are not necessarily strings. Hence the string conversion
; to find if the cancel button was pressed.

Widget_Control, event.id, Get_Value=userEvent

userEventStr = string(userEvent)

CASE userEventStr OF

  'Cancel' : widget_Control, event.top, /destroy
   else  :

ENDCASE

END

;-----------------------------------------------------------------------------


PRO adas_show_text, GROUP_LEADER = group_leader, NOTIFYID = notifyID, $
                    XSIZE = xsize, YSIZE = ysize, FONT = font,        $
                    text = text, title = title


; We can have many instances of this widget so comment out this line.

;  IF XREGISTERED('adas_show_text') THEN RETURN


; Set defaults for keywords.

if n_elements(title) eq 0 then title = 'TEXT'
if not (keyword_set(xsize)) then xsize = 80
if not (keyword_set(ysize)) then ysize = 30
if n_elements(font) eq 0 then font_large = ''


if n_elements(notifyID) eq 0 then notifyID = -1L


parent  = widget_base(column=1, title=title)
topbase = widget_base(parent,/column,/frame)
label   = widget_label(topbase,value=' ')


lentext = max(strlen(text))
if lentext GT 80 then xsize = lentext < 132

textid = widget_text(topbase, value=text, /scroll, $
                     xsize=xsize, ysize=ysize, font=font)



; One button

cancelID = widget_button(topbase, value='Cancel', font=font)


; Realize the  widget.

widget_Control, parent, /realize


; Create an info structure with program information.

info = { cancelID  :  cancelID,   $
         font      :  font        }


; Store the info structure in the user value of the top-level base and launch
; the widget into the world.

widget_control, parent, Set_UValue=info

XManager, 'adas_show_text', parent, Event_Handler='ADAS_SHOW_TEXT_EVENT', $
                                    Group_Leader = group_leader

END
