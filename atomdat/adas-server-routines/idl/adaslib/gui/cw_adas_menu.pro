; Copyright (c) 1995 Strathclyde University .
; SCCS info: Module @(#)$Header: /home/adascvs/idl/adaslib/gui/cw_adas_menu.pro,v 1.5 2004/07/06 12:59:24 whitefor Exp $ Date $Date: 2004/07/06 12:59:24 $
;
;+
; PROJECT:
;	ADAS IBM MVS to DEC UNIX conversion
;
; NAME:
;	CW_ADAS_MENU()
;
; PURPOSE:
;	A column of exclusive buttons for use as a menu.
;
; EXPLANATION:
;	This widget consists of a column of exclusive buttons
;	each labelled with an item from LIST.  This widget
;	generates an event when a button is selected.
;	The event structure generated is;
;
;	{ID:0L, TOP:0L, HANDLER:0L, SELECT:0}
;
;	where select is the index within LIST of the selected
;	option.
;
; USE:
;	See adas_menu.pro for an example of usage.
;
; INPUTS:
;	PARENT	- The ID of the parent widget.
;
;	LIST	- A string array; a list of menu options, one array
;		  element per option.
;
; OPTIONAL INPUTS:
;	None.
;
; OUTPUTS:
;	The return value of this function is the ID of the widget.
;
; OPTIONAL OUTPUTS:
;	None.
;
; KEYWORDS PARAMETERS:
;	UVALUE	- A user value for this widget.
;
;       FONT    - A string; the name of a font to use for all text
;                 in the menus.  Defaults to default system font.
;
; CALLS:
;	See side effects for other related widget management routines.
;
; SIDE EFFECTS:
;	This widget uses a COMMON BLOCK: CW_MENU_BLK to hold the
;	widget state.
;
;	One other routine is included which is used to manage the
;	widget;
;
;	ADMENU_EVENT()
;
; CATEGORY:
;	Compound widget.
;
; WRITTEN:
;	Andrew Bowen, Tessella Support Services plc, 25-May-1993
;
; MODIFIED:
;	Version 1	Andrew Bowen	1-Jun-1993
;			First release.
;	VERSION 1.1     Lalit Jalota    15-Mar-1995
;			Changed calls to CW_SAVESTATE and CW_LOADSTATE to
;			widget_control, get_ and set_uvalue
; VERSION:
;	1	1-Jun-1993
;	1.1    15-Mar-1995
;-
;-----------------------------------------------------------------------------

FUNCTION admenu_event, event

		;**** Base ID of compound widget ****
  parent=event.handler

		;**** Retrieve the state ****
  widget_control, parent, get_uvalue=state, /no_copy

		;**** Identify selection number ****
  widget_control,event.id,get_uvalue=select

  RETURN, {ID:parent, TOP:event.top, HANDLER:0L, SELECT:select}
END

;-----------------------------------------------------------------------------

FUNCTION cw_adas_menu,	parent, list, UVALUE=uvalue, FONT=font

		;**** Flag error and return to caller ****
  IF (N_PARAMS() LT 2) THEN MESSAGE, $
		'Must specify a PARENT and LIST'
  ON_ERROR, 2

		;**** Set defaults for keywords ****
  IF NOT (KEYWORD_SET(uvalue))  THEN uvalue = 0
  IF NOT (KEYWORD_SET(font)) THEN font = ''

		;**** Create the main base for the table ****
  mainbase = WIDGET_BASE(parent, UVALUE = uvalue, $
		EVENT_FUNC = "admenu_event", $
		/COLUMN)

		;**** Add dummy base for use by SAVESTATE ****
  butbase = widget_base(mainbase,/column,/exclusive)

		;**** See how many buttons are requried ****
  items = list(where(strtrim(list) ne ''))
  listdim = size(items)
  if listdim(0) gt 0 then begin
    nitems = listdim(1)
  end else begin
    nitems = 1
  end

		;**** Create menu list of button widgets ****
  for i = 0, nitems-1 do begin
    rc = widget_button(butbase,value=items(i),uvalue=i,font=font)
  end


		;**** Create state structure ****
  new_state =  { LIST:list }

		;**** Save initial state structure ****
  widget_control, parent, set_uvalue=new_state, /no_copy

  RETURN, mainbase

END


