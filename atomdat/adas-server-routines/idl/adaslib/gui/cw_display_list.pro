; Copyright (c) 1995 Strathclyde University .
; SCCS info: Module @(#)$Header: /home/adascvs/idl/adaslib/gui/cw_display_list.pro,v 1.1 2004/07/06 13:05:26 whitefor Exp $ Date $Date: 2004/07/06 13:05:26 $
;+
; PROJECT:
;       ADAS IBM MVS to DEC UNIX conversion
;
; NAME: CW_DISPLAY_LIST()
;
; PURPOSE:
;       Displays an uneditable list in a popup window.
;
; EXPLANATION:
;       This routine creates a compound widget consisting of a couple of
;	labels and a non-editable list widget along with a definable
;	number of button widgets. The main purpose of the widget is
;	to pop up and display a list of strings (e.g. the result of
;	a search of some kind) and allow the user to close the
;	display by clicking on any of the displayed buttons. The list
;	of items cannot be edited in any way.
;
; USE:
;       General use. See cw_master_classes.pro for an example.
;
; INPUTS:
;       HEADINGS        -       String array containing the column headings
;                               to be used in the list. 
;
;	ITEMS		-	String array containing the items in the list.
;
; OPTIONAL INPUTS:
;       None.
;
; OUTPUTS:
;       ACTION          -       Integer: index showing which of the
;                               buttons has been pressed.
;
; OPTIONAL OUTPUTS:
;       None
;
; KEYWORD PARAMETERS:
;       TITLE           -       A heading to go above the list.
;
;       FONT            -       A font to use for all text in this widget.
;
;       XOFFSET         -       Integer: the offset of the widget (in pixels)
;                               from the left edge of the screen.
;
;       YOFFSET         -       Integer: the offset of the widget (in pixels)
;                               from the top of the screen.
;
;       BUTTONS         -       String array containing the titles
;                               of the buttons to be used.
;
;       TOPTITLE        -       String: a title for the window.
;
;       YSIZE           -       Integer: the length of the widget in rows.
;
;	FOOTMESSAGE	-	String: a message to go at the foot of the list.
;
; CALLS:
;
; SIDE EFFECTS:
;       One other routine is included which is used to manage the
;       widget:
;
;       CW_DISPLAY_LIST_EVENT()
;
; CATEGORY:
;       General use compound widget.
;
; WRITTEN:
;       Tim Hammond (Tessella Support Services plc), 297th September 1995
;
; MODIFIED:
;       1.1     Tim Hammond 
;		Initial version.
;
; VERSION:
;       1.1     29-09-95
;
;-
;-----------------------------------------------------------------------------

PRO cw_display_list_event, event

    COMMON display_listcom, action, nbuttons

           ;**** Base ID of compound widget ****;

    parent=event.handler

           ;**********************************************;
           ;**** Retrieve the user value state        ****;
           ;**** Get id of first_child widget because ****;
           ;**** user value "state" is stored there   ****;
           ;**********************************************;

    first_child = widget_info(parent, /child)
    widget_control, first_child, get_uvalue=state, /no_copy

           ;************  Process events  ***************;

    buttonpress = 0
    for i = 0, (nbuttons-1) do begin
        if event.id eq state.buttonids(i) then begin
            buttonpress = buttonpress + 1
            widget_control, /destroy, event.top
            action = i
        endif
    endfor
    if buttonpress eq 0 then begin
        widget_control, first_child, set_uvalue=state, /no_copy
    endif
                                                           
END

;-------------------------------------------------------------------------

PRO cw_display_list, headings, items, action, TITLE=title,		$
		     FONT=font, XOFFSET=xoffset, YOFFSET=yoffset,  	$
                     BUTTONS=buttons, TOPTITLE=toptitle, YSIZE=ysize,	$
		     FOOTMESSAGE=footmessage

    COMMON display_listcom, actcom, nbuttons
                                                                      

    IF NOT (KEYWORD_SET(title)) THEN title = ' '
    IF NOT (KEYWORD_SET(footmessage)) THEN footmessage = ' '
    IF NOT (KEYWORD_SET(font)) THEN font = ''
    IF NOT (KEYWORD_SET(xoffset)) THEN xoffset = 100
    IF NOT (KEYWORD_SET(yoffset)) THEN yoffset = 1
    IF NOT (KEYWORD_SET(buttons)) THEN buttons = [' OK ']
    IF NOT (KEYWORD_SET(toptitle)) THEN toptitle = ' '
    IF NOT (KEYWORD_SET(ysize)) THEN ysize=10
    if n_elements(items) lt ysize then ysize = n_elements(items)

           ;***************************************;
           ;********** declare variables **********;
           ;***************************************;
                                            
    actcom = action

           ;***************************************;
           ;******* create base and widgets *******;
           ;***************************************;

    parent = widget_base(/column, title=toptitle, xoffset=xoffset,	$
		         yoffset=yoffset)
    first_child = widget_base(parent)
    base = widget_base(first_child, /column)

    title = widget_label(base, value=title, font=font)

    colid = widget_list(base, value=headings, font=font, ysize=3)

    itemlist = widget_list(base, value=items, font=font,		$
			   ysize=ysize)


    buffer = widget_label(base, value='         ', font=font)
    footer = widget_label(base, value=footmessage, font=font)

    buttonbase = widget_base(base, /row)
    buttonids = intarr(n_elements(buttons))
    for i=0, (n_elements(buttons) -1) do begin
        buttonids(i) = widget_button(buttonbase, value=buttons(i), font=font)
    endfor
    nbuttons = n_elements(buttons)

           ;***************************************;
           ;*** create a state structure for the **;
           ;*** popup window                     **;
           ;***************************************;

    new_state = {  	buttonids	:	buttonids		}

           ;**** Save initial state structure *****;

    widget_control, first_child, set_uvalue=new_state, /no_copy

           ;***************************************;
           ;*********** realise widget  ***********;
           ;***************************************;

    widget_control, /realize, parent

           ;***************************************;
           ;** pass control to event handler and **;
           ;** make xmanager modal to avoid clash *;
           ;* with the currently running xmanager *;
           ;***************************************;

    xmanager, 'cw_display_list', parent, 				$
              event_handler='cw_display_list_event', /modal

    action = actcom

END
