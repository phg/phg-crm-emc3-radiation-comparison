;+
; PROJECT:
;       ADAS
;
; NAME:
;       ADAS
;
; PURPOSE:
;       The main routine to start the ADAS system.
;
; EXPLANATION:
;       This is the higest level routine in the ADAS system.  Call this
;       routine to start ADAS at the main system primary menu.  The only
;       necessary preparation before calling adas.pro is to set up the
;       environment variables read in adas_sys_set.pro.
;
; USE:
;       To start the ADAS system from IDL simply call this routine;
;
;               adas
;
; INPUTS:
;       None
;
; OPTIONAL INPUTS:
;       None
;
; OUTPUTS:
;       None
;
; OPTIONAL OUTPUTS:
;       None
;
; KEYWORD PARAMETERS:
;       None
;
; CALLS:
;               General system routines;
;       ADAS_SYS_SET    Initialises system settings
;       ADAS_MENU_DEF   Returns adas system menu definitions
;       ADAS_MENUS      Pops-up menu windows, returns user choices
;
;               ADAS program routines;
;       ADAS101/103/105/106
;       ADAS201/203/204/205/206/207/208/209/210/211/212
;       ADAS301/302/303/304/306/307/308/309/310/312/314
;       ADAS401/402/404/405/406/407/408
;       ADAS501/502/503/504/505/506/509
;       ADAS601/602/603
;       ADAS701/702/703
;
; SIDE EFFECTS:
;       This routine now loads the COMMON block Global_adas_num with the
;       numbers corresponding to the adas routine which is selected by the
;       user. This is a global COMMON block accessed by other routines.
;
; CATEGORY:
;       Adas system.
;
; WRITTEN:
;       Andrew Bowen, Tessella Support Services plc, 25-May-1993
;
; MODIFIED:
;       1.1     Andrew Bowen
;               First release.
;       1.2     Andrew Bowen
;               Completed header.
;       1.3     Lalit Jalota
;               Added call to adas501
;       1.4     Lalit Jalota
;               Added call to adas503
;       1.5     Lalit Jalota
;               Added call to adas201
;       1.6     Lalit Jalota
;               Added call to adas207
;       1.7     Lalit Jalota
;       1.8     Lalit Jalota
;       1.9     Tim Hammond
;               Added call to adas301
;       1.10    Tim Hammond
;               Corrected comments in header
;       1.11    Tim Hammond
;               Updated menu structures
;       1.12    Tim Hammond
;               Added call to adas303
;       1.13    Tim Hammond
;               Added call to adas308
;       1.14    Tim Hammond
;               Added call to adas506
;       1.15    Tim Hammond
;               Added call to adas209
;       1.16    Tim Hammond
;               Added call to adas309
;       1.17    Tim Hammond
;               Added call to adas101
;       1.18    Tim Hammond
;               Added call to adas401
;       1.19    Tim Hammond
;               Added call to adas405
;       1.20    Tim Hammond
;               Added call to adas304
;       1.21    Tim Hammond
;               Added call to adas502
;       1.22    Tim Hammond
;               Added call to adas504
;       1.23    Tim Hammond
;               Added call to adas210
;       1.24    Tim Hammond
;               Added call to adas601
;       1.25    Tim Hammond
;               Added call to adas310 and temporarily removed call to adas601.
;       1.26    Tim Hammond/Ed Mansky (ORNL)
;               Added new COMMON block Global_adas_num
;       1.27    Tim Hammond
;               Re-instated call to adas601/demap.
;       1.28    Tim Hammond
;               Added call to adas203
;       1.29    William Osborn
;               Added call to adas505
;       1.30    William Osborn
;               Added calls to adas407 and adas408
;       1.31    William Osborn
;               Added call to adas509
;       1.32    William Osborn
;               Added call to adas208
;       1.33    William Osborn
;               Added call to demap_main
;       1.34    William Osborn
;               Added call to adas306
;       1.35    William Osborn
;               Added call to adas307
;       1.36    William Osborn
;               Added call to adas206
;       1.37    William Osborn
;               Added call to adas406
;       1.38    William Osborn
;               Added call to adas211
;       1.39    William Osborn
;               Added call to adas212
;       1.40    William Osborn
;               Added call to adas204
;       1.41    William Osborn
;               Added call to adas105
;       1.42    William Osborn
;               Added call to adas106
;       1.43    William Osborn
;               Added calls to adas103 and adas402
;       1.44    William Osborn
;               Added call to adas302
;       1.45    William Osborn
;               Added call to adas302 - really this time
;       1.46    William Osborn
;               Added call to adas404
;       1.47    William Osborn
;               Added call to adas102
;       1.48    Richard Martin
;               Added call to adas602 and adas312
;       1.49    Richard Martin
;               Added call to adas410 and adas412
;       1.50    Richard Martin
;               Added call to adas409 and adas411
;       1.51    Richard Martin
;               Added call to adas413, adas213, adas214, adas215, adas703.
;               Added writing of choices to ~adas/adas/.use.dat (Temporary)
;       1.52    Richard Martin
;               Added call to adas314 and adas603.
;               Commented out writing to .use.dat
;       1.53    Richard Martin
;               Added call to adas216, adas311 and adas313.
;       1.54    Richard Martin
;               Added call to adas108, adas403
;       1.55    Richard Martin
;               Added call to adas801, adas807, adas414 and adas415
;       1.56    Richard Martin
;               Added adas704.
;       1.57    Martin O'Mullane
;               Added adas507 and test for IDL v5.3.
;       1.58    Richard Martin
;               Added adas811.
;       1.59    Richard Martin
;               Added adas510, adas804, adas808, adas809, adas810.
;       1.60    Allan Whiteford
;               Added ADAS812
;       1.61    Allan Whiteford
;               Changed version number checking to check for '5.3' rather
;               than just a minor version number of 3.
;       1.62    Martin O'Mullane
;               Added adas315 and adas316.
;       1.63    Martin O'Mullane
;               Added adas416.
;       1.64    Martin O'Mullane
;               Added series 9 codes.
;
; VERSION:
;       1.1     25-05-93
;       1.2     25-05-93
;       1.3     23-02-95
;       1.4     28-02-95
;       1.5     13-03-95
;       1.6     13-03-95
;       1.7     13-03-95
;       1.8     30-03-95
;       1.9     11-04-95
;       1.10    11-04-95
;       1.11    21-04-95
;       1.12    03-05-95
;       1.13    20-06-95
;       1.14    26-06-95
;       1.15    27-06-95
;       1.16    10-07-95
;       1.17    11-07-95
;       1.18    06-09-95
;       1.19    07-11-95
;       1.20    08-12-95
;       1.21    13-12-95
;       1.22    17-01-96
;       1.23    22-01-96
;       1.24    26-01-96
;       1.25    09-02-96
;       1.26    26-02-96
;       1.27    06-03-96
;       1.28    01-04-96
;       1.29    02-04-96
;       1.30    23-04-96
;       1.31    30-04-96
;       1.32    07-05-96
;       1.33    07-05-96
;       1.34    24-05-96
;       1.35    29-05-96
;       1.36    17-06-96
;       1.37    26-06-96
;       1.38    07-08-96
;       1.39    22-08-96
;       1.40    30-08-96
;       1.41    03-10-96
;       1.42    11-10-96
;       1.43    08-11-96
;       1.44    19-11-96
;       1.45    19-11-96
;       1.46    20-11-96
;       1.47    25-11-96
;       1.48    17-07-97
;       1.49    ?
;       1.50    09-03-98
;       1.51    09-06-98
;       1.52    04-12-98
;       1.53    24-03-99
;       1.54
;       1.55
;       1.56    14-11-00
;       1.57    06-02-01
;       1.58    17-06-02
;       1.59    18-02-03
;       1.60    13-12-05
;       1.61    25-05-06
;       1.62    04-07-07
;       1.63    04-10-07
;       1.64    17-10-12
;
;-
;-----------------------------------------------------------------------------

PRO adas



	device,retain=2
	adashome=getenv('ADASHOME')
	cd,'~/adas/pass'
	

  COMMON Global_adas_num, maincom, subcom

                ;**** Get the local system settings ****

  adas_sys_set, adasrel, fortdir, userroot, centroot,                   $
                devlist, devcode, font_large, font_small, edit_fonts

                ;**** Get the menu definitions ****

  adas_menu_def, mainmenu, submenus

                ;**** Set no main menu choice to begin ****

  mainchoice = -1

START:



                ;**** Warn if we are running IDL v5.3 ****

  if !version.release EQ '5.3' then begin

     mess = ['                                        ',  $
             '        I N F O R M A T I O N           ',  $
             '                                        ',  $
             '  ADAS does not work with version 5.3   ',  $
             '  of IDL.                               ',  $
             '                                        ',  $
             '  Critical functionality for ADAS in    ',  $
             '  the spawn subsystem does not work     ',  $
             '  reliably in this IDL version.         ',  $
             '                                        ',  $
             '  RSI have acknowledged the problem.    ',  $
             '  Their solution is to upgrade to v5.4  ',  $
             '  or continue to use v5.1 or v5.2.      ',  $
             '                                        ' ]

     tell= popup(message=mess, buttons=['Accept'],font=font_large)

     if tell EQ 'Accept' then goto, ADASEND


  endif


                ;**** Pop-up menus for program selection ****

  adas_menus, mainmenu, submenus, mainchoice, subchoice, $
                ADASREL=adasrel, FONT=font_large

                ;**** Put value of selected routine into COMMON ****

  maincom = mainchoice
  subcom = subchoice

;  ** Following added as 'one off' only **
;
;  if (maincom ge 0 AND subcom ge 0 ) then begin
;       date = xxdate()
;       path = GETENV('ADASCENT')
;       file = path+'/.use.dat'
;       name = GETENV('USER')
;       OPENU,unit,file,/GET_LUN,/APPEND
;       printf,unit,strtrim(string(maincom+1),2)+':'+$
;               strtrim(string(subcom+1),2)+' '+date(0)+' '+date(1)+ $
;               ' '+name
;       CLOSE,unit
;       FREE_LUN,unit
;  endif

                ;**** Invoke adas program selected ****

  CASE mainchoice OF

                        ;**** Main choice 0 ****

    0:  CASE subchoice OF

          0: adas101, adasrel, fortdir, userroot, centroot, devlist, $
                      devcode, font_large, font_small, edit_fonts
          1: adas102, adasrel, fortdir, userroot, centroot, devlist, $
                      devcode, font_large, font_small, edit_fonts
          2: adas103, adasrel, fortdir, userroot, centroot, devlist, $
                      devcode, font_large, font_small, edit_fonts
          3:
          4: adas105, adasrel, fortdir, userroot, centroot, devlist, $
                      devcode, font_large, font_small, edit_fonts
          5: adas106, adasrel, fortdir, userroot, centroot, devlist, $
                      devcode, font_large, font_small, edit_fonts
          6:

          7: adas108, adasrel, fortdir, userroot, centroot, devlist, $
                      devcode, font_large, font_small, edit_fonts

          ELSE:

        ENDCASE

                        ;**** Main choice 1 ****

    1:  CASE subchoice OF

          0: adas201, adasrel, fortdir, userroot, centroot, devlist, $
                      devcode, font_large, font_small, edit_fonts
          1:
          2: adas203, adasrel, fortdir, userroot, centroot, devlist, $
                      devcode, font_large, font_small, edit_fonts
          3: adas204, adasrel, fortdir, userroot, centroot, devlist, $
                      devcode, font_large, font_small, edit_fonts
          4: adas205, adasrel, fortdir, userroot, centroot, devlist, $
                      devcode, font_large, font_small, edit_fonts
          5: adas206, adasrel, fortdir, userroot, centroot, devlist, $
                      devcode, font_large, font_small, edit_fonts
          6: adas207, adasrel, fortdir, userroot, centroot, devlist, $
                      devcode, font_large, font_small, edit_fonts
          7: adas208, adasrel, fortdir, userroot, centroot, devlist, $
                      devcode, font_large, font_small, edit_fonts
          8: adas209, adasrel, fortdir, userroot, centroot, devlist, $
                      devcode, font_large, font_small, edit_fonts
          9: adas210, adasrel, fortdir, userroot, centroot, devlist, $
                      devcode, font_large, font_small, edit_fonts
         10: adas211, adasrel, fortdir, userroot, centroot, devlist, $
                      devcode, font_large, font_small, edit_fonts
         11: adas212, adasrel, fortdir, userroot, centroot, devlist, $
                      devcode, font_large, font_small, edit_fonts
       12: adas213, adasrel, fortdir, userroot, centroot, devlist, $
                      devcode, font_large, font_small, edit_fonts
       13: adas214, adasrel, fortdir, userroot, centroot, devlist, $
                      devcode, font_large, font_small, edit_fonts
       14: adas215, adasrel, fortdir, userroot, centroot, devlist, $
                      devcode, font_large, font_small, edit_fonts
       15: adas216, adasrel, fortdir, userroot, centroot, devlist, $
                      devcode, font_large, font_small, edit_fonts


          ELSE:

        ENDCASE

                        ;**** Main choice 2 ****

    2:  CASE subchoice OF

          0: adas301, adasrel, fortdir, userroot, centroot, devlist, $
                      devcode, font_large, font_small, edit_fonts
          1: adas302, adasrel, fortdir, userroot, centroot, devlist, $
                      devcode, font_large, font_small, edit_fonts
          2: adas303, adasrel, fortdir, userroot, centroot, devlist, $
                      devcode, font_large, font_small, edit_fonts
          3: adas304, adasrel, fortdir, userroot, centroot, devlist, $
                      devcode, font_large, font_small, edit_fonts
          4:
          5: adas306, adasrel, fortdir, userroot, centroot, devlist, $
                      devcode, font_large, font_small, edit_fonts
          6: adas307, adasrel, fortdir, userroot, centroot, devlist, $
                      devcode, font_large, font_small, edit_fonts
          7: adas308, adasrel, fortdir, userroot, centroot, devlist, $
                      devcode, font_large, font_small, edit_fonts
          8: adas309, adasrel, fortdir, userroot, centroot, devlist, $
                      devcode, font_large, font_small, edit_fonts
          9: adas310, adasrel, fortdir, userroot, centroot, devlist, $
                      devcode, font_large, font_small, edit_fonts
         10: adas311, adasrel, fortdir, userroot, centroot, devlist, $
                      devcode, font_large, font_small, edit_fonts
         11: adas312, adasrel, fortdir, userroot, centroot, devlist, $
                      devcode, font_large, font_small, edit_fonts
         12: adas313, adasrel, fortdir, userroot, centroot, devlist, $
                      devcode, font_large, font_small, edit_fonts
         13: adas314, adasrel, fortdir, userroot, centroot, devlist, $
                      devcode, font_large, font_small, edit_fonts
         14: adas315, adasrel, fortdir, userroot, centroot, devlist, $
                      devcode, font_large, font_small, edit_fonts
         15: adas316, adasrel, fortdir, userroot, centroot, devlist, $
                      devcode, font_large, font_small, edit_fonts

          ELSE:

        ENDCASE

                        ;**** Main choice 3 ****

    3:  CASE subchoice OF

          0: adas401, adasrel, fortdir, userroot, centroot, devlist, $
                      devcode, font_large, font_small, edit_fonts
          1: adas402, adasrel, fortdir, userroot, centroot, devlist, $
                      devcode, font_large, font_small, edit_fonts
          2: adas403, adasrel, fortdir, userroot, centroot, devlist, $
                      devcode, font_large, font_small, edit_fonts
          3: adas404, adasrel, fortdir, userroot, centroot, devlist, $
                      devcode, font_large, font_small, edit_fonts
          4: adas405, adasrel, fortdir, userroot, centroot, devlist, $
                      devcode, font_large, font_small, edit_fonts
          5: adas406, adasrel, fortdir, userroot, centroot, devlist, $
                      devcode, font_large, font_small, edit_fonts
          6: adas407, adasrel, fortdir, userroot, centroot, devlist, $
                      devcode, font_large, font_small, edit_fonts
          7: adas408, adasrel, fortdir, userroot, centroot, devlist, $
                      devcode, font_large, font_small, edit_fonts
          8: adas409, adasrel, fortdir, userroot, centroot, devlist, $
                      devcode, font_large, font_small, edit_fonts
          9: adas410, adasrel, fortdir, userroot, centroot, devlist, $
                      devcode, font_large, font_small, edit_fonts
          10: adas411, adasrel, fortdir, userroot, centroot, devlist, $
                      devcode, font_large, font_small, edit_fonts
          11: adas412, adasrel, fortdir, userroot, centroot, devlist, $
                      devcode, font_large, font_small, edit_fonts
          12: adas413, adasrel, fortdir, userroot, centroot, devlist, $
                      devcode, font_large, font_small, edit_fonts
          13: adas414, adasrel, fortdir, userroot, centroot, devlist, $
                      devcode, font_large, font_small, edit_fonts
          14: adas415, adasrel, fortdir, userroot, centroot, devlist, $
                      devcode, font_large, font_small, edit_fonts
          15: adas416, adasrel, fortdir, userroot, centroot, devlist, $
                      devcode, font_large, font_small, edit_fonts
          ELSE:

        ENDCASE

                        ;**** Main choice 4 ****

    4:  CASE subchoice OF

          0: adas501,  adasrel, fortdir, userroot, centroot, devlist, $
                       devcode, font_large, font_small, edit_fonts
          1: adas502,  adasrel, fortdir, userroot, centroot, devlist, $
                       devcode, font_large, font_small, edit_fonts
          2: adas503,  adasrel, fortdir, userroot, centroot, devlist, $
                       devcode, font_large, font_small, edit_fonts
          3: adas504,  adasrel, fortdir, userroot, centroot, devlist, $
                       devcode, font_large, font_small, edit_fonts
          4: adas505,  adasrel, fortdir, userroot, centroot, devlist, $
                       devcode, font_large, font_small, edit_fonts
          5: adas506,  adasrel, fortdir, userroot, centroot, devlist, $
                       devcode, font_large, font_small, edit_fonts
          6: adas507,  adasrel, fortdir, userroot, centroot, devlist, $
                       devcode, font_large, font_small, edit_fonts
          7:

          8: adas509,  adasrel, fortdir, userroot, centroot, devlist, $
                       devcode, font_large, font_small, edit_fonts
          9: adas510,  adasrel, fortdir, userroot, centroot, devlist, $
                       devcode, font_large, font_small, edit_fonts
          ELSE:

        ENDCASE

                        ;**** Main choice 5 ****

    5:  CASE subchoice OF

          0: demap_main,  adasrel, fortdir, userroot, centroot, devlist, $
                       devcode, font_large, font_small, edit_fonts
          1: adas602,  adasrel, fortdir, userroot, centroot, devlist, $
                       devcode, font_large, font_small, edit_fonts
          2: adas603,  adasrel, fortdir, userroot, centroot, devlist, $
                       devcode, font_large, font_small, edit_fonts
          4: adas605,  adasrel, fortdir, userroot, centroot, devlist, $
                       devcode, font_large, font_small, edit_fonts
          ELSE:

        ENDCASE

    6:  CASE subchoice OF

          0: adas701, adasrel, fortdir, userroot, centroot, devlist, $
                       devcode, font_large, font_small, edit_fonts
        1: adas702, adasrel, fortdir, userroot, centroot, devlist, $
                           devcode, font_large, font_small, edit_fonts

        2: adas703, adasrel, fortdir, userroot, centroot, devlist, $
                       devcode, font_large, font_small, edit_fonts

          3: adas704, adasrel, fortdir, userroot, centroot, devlist, $
                       devcode, font_large, font_small, edit_fonts

          ELSE:

        ENDCASE

    7:  CASE subchoice OF

          0:  adas801, adasrel, fortdir, userroot, centroot, devlist, $
                       devcode, font_large, font_small, edit_fonts
          3:  adas804, adasrel, fortdir, userroot, centroot, devlist, $
                       devcode, font_large, font_small, edit_fonts
          6:  adas807, adasrel, fortdir, userroot, centroot, devlist, $
                       devcode, font_large, font_small, edit_fonts
          7:  adas808, adasrel, fortdir, userroot, centroot, devlist, $
                       devcode, font_large, font_small, edit_fonts
          8:  adas809, adasrel, fortdir, userroot, centroot, devlist, $
                       devcode, font_large, font_small, edit_fonts
          9:  adas810, adasrel, fortdir, userroot, centroot, devlist, $
                       devcode, font_large, font_small, edit_fonts
          10: adas811, adasrel, fortdir, userroot, centroot, devlist, $
                       devcode, font_large, font_small, edit_fonts
          11: adas812, adasrel, fortdir, userroot, centroot, devlist, $
                       devcode, font_large, font_small, edit_fonts

          ELSE:

        ENDCASE

    8:  CASE subchoice OF

          0:  adas901, adasrel, fortdir, userroot, centroot, devlist, $
                       devcode, font_large, font_small, edit_fonts

          ELSE:

        ENDCASE

                        ;**** Exit system ****

    -1: goto, ADASEND

    ELSE:

  ENDCASE

                ;**** Back to the menu system ****

  goto, START

ADASEND:

END
