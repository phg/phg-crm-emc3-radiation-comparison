; Copyright (c) 1999, Strathclyde University.
; SCCS info: Module @(#)$Header: /home/adascvs/idl/adaslib/gui/cw_adas_pairsel.pro,v 1.4 2004/07/06 13:00:41 whitefor Exp $ Date $Date: 2004/07/06 13:00:41 $
;+
; PROJECT:
;       ADAS 
;
; NAME:
;	CW_ADAS_PAIRSEL_PLOT
;
; PURPOSE:
;	TO BE DOCUMENTED
; MODIFIED:
;	1.1	Richard Martin
;		First release 
;	1.2	Richard Martin
;		Added support for 24-bit colour
;	1.3	??
;	1.4	Richard Martin
;		Correct bug that caused crash when n_elements(state.numte) < n_elements(state.numden) 
;
; VERSION:
;	1.1	15-10-99
;	1.2	14-12-01
;	1.3	??-??-??
;	1.4	13-06-02
;
;-
;-----------------------------------------------------------------------------

PRO adas_pairsel_event, event

 Widget_Control, event.top, Get_UValue=state, /no_copy
 Widget_Control, event.id,  Get_UValue=userEvent

COMMON new_data, newt, newd, ntindex, ndindex, xwidth, yheight

 ; cannot do both yet!
 ;widget_control, state.ch_ids(2), sensitive=0

 
 ; All events are connected with plotting - set the structure on 
 ; entering the event handler. All fields are part of state so
 ; the values are stored there permanently. 
 
 widget_control, state.drawid1, get_value = draw_win1
 widget_control, state.drawid2, get_value = draw_win2
 wset,  draw_win1
 
 ;erase

	;************************
	;**** Set up colours ****
	;************************
		
  TVLCT, red, green, blue, /get

  device,get_visual_depth=depth

  if depth eq 24 then begin
  	  colour = { black   : red(0)+256L*(green(0)+256L*blue(0)),   $
  			 white   : red(1)+256L*(green(1)+256L*blue(1)),   $
  			 red     : red(2)+256L*(green(2)+256L*blue(2)),   $	  
  			 green   : red(3)+256L*(green(3)+256L*blue(3)),   $
  			 blue    : red(4)+256L*(green(4)+256L*blue(4)),   $
  			 yellow  : red(5)+256L*(green(5)+256L*blue(5)),   $
  			 magenta : red(6)+256L*(green(6)+256L*blue(6)),   $
  			 cyan    : red(7)+256L*(green(7)+256L*blue(7))      }
  endif else begin
  	  colour = { black   : 0, $
  			 white   : 1, $
  			 red     : 2, $			 
  			 green   : 3, $
  			 blue    : 4, $
  			 yellow  : 5, $
  			 magenta : 6, $
  			 cyan    : 7    }
  endelse

CASE userEvent OF

            
  'XROT'   : begin
               
               widget_control, state.xrotid, get_value=ax
               state.ax = ax 
               
               state.plot_stuff.ax = state.ax
               adas_pairsel_plot, state.updateid, value = state.plot_stuff
               
               index=where(newt gt 0.0,count)
		   if count gt 0 then begin	  
		   	plots,newt(index),newd(index),state.z(index),psym=2,/t3d,color=colour.red
 		   	wset,  draw_win2		   	
 		   	plots, newt(index), newd(index),psym=-2,color=colour.red

			window, 9, /pixmap, xsize=xwidth, ysize=yheight
 	 		device, copy=[0,0,xwidth,yheight,0,0,draw_win1]											 
			
 		   endif
               
             end
	
  'ZROT'   : begin

               widget_control, state.zrotid, get_value=az
               state.az = az  
              
               state.plot_stuff.az = state.az
               adas_pairsel_plot, state.updateid, value = state.plot_stuff

               index=where(newt gt 0.0,count)
		   if count gt 0 then begin
		   	plots,newt(index),newd(index),state.z(index),psym=2,/t3d,color=colour.red
 		   	wset,  draw_win2
 		   	plots,newt(index),newd(index),psym=-2,color=colour.red

			window, 9, /pixmap, xsize=xwidth, ysize=yheight
 	 		device, copy=[0,0,xwidth,yheight,0,0,draw_win1]											 
			
 		   endif
 		               
             end

  'STYLES' : begin
  
               state.sty_ind = event.index 
               
               state.plot_stuff.style = state.sty_ind
		   if state.plot_stuff.style eq 0 then begin
		   		adasloadct
		   endif else begin
		   		cvec = [0,3,1,8]  ; B-W,Red Temp,Blue,Green
		   		loadct,cvec(state.col_ind),bottom=8
		   endelse
		   		
               adas_pairsel_plot, state.updateid, value = state.plot_stuff

               index=where(newt gt 0.0,count)
		   if count gt 0 then begin
		   	plots,newt(index),newd(index),state.z(index),psym=2,/t3d,color=colour.red
 		   	wset,  draw_win2
 		   	plots,newt(index),newd(index),psym=-2,color=colour.red

			window, 9, /pixmap, xsize=xwidth, ysize=yheight
 	 		device, copy=[0,0,xwidth,yheight,0,0,draw_win1]											 
			
 		   endif
 		                                
             end
  
  'COLOURS': begin
   
               cvec = [0,3,1,8]  ; B-W,Red Temp,Blue,Green
                
               state.col_ind = event.index 
               loadct, cvec(state.col_ind), bottom=8
               
               state.plot_stuff.colour = state.col_ind
               adas_pairsel_plot, state.updateid, value = state.plot_stuff

               index=where(newt gt 0.0,count)
		   if count gt 0 then begin
		   	plots,newt(index),newd(index),state.z(index),psym=2,/t3d,color=colour.red
 		   	wset,  draw_win2
 		   	plots,newt(index),newd(index),psym=-2,color=colour.red

			window, 9, /pixmap, xsize=xwidth, ysize=yheight
 	 		device, copy=[0,0,xwidth,yheight,0,0,draw_win1]											 
			
 		   endif
 		   
             end

  'XSTYLE': begin

               state.xlog = event.index 
               state.plot_stuff.xlog = state.xlog
               adas_pairsel_plot, state.updateid, value = state.plot_stuff               
               index=where(newt gt 0.0,count)
		   if count gt 0 then begin
		   	plots,newt(index),newd(index),state.z(index),psym=2,/t3d,color=colour.red
 		   	wset,  draw_win2
 		   	plots,newt(index),newd(index),psym=2,color=colour.red

			window, 9, /pixmap, xsize=xwidth, ysize=yheight
 	 		device, copy=[0,0,xwidth,yheight,0,0,draw_win1]											 
			
 		   endif               
             end
 
   'YSTYLE': begin

               state.ylog = event.index 
               state.plot_stuff.ylog = state.ylog
               adas_pairsel_plot, state.updateid, value = state.plot_stuff               
               index=where(newt gt 0.0,count)
		   if count gt 0 then begin
		   	plots,newt(index),newd(index),state.z(index),psym=2,/t3d,color=colour.red
 		   	wset,  draw_win2
 		   	plots,newt(index),newd(index),psym=-2,color=colour.red

			window, 9, /pixmap, xsize=xwidth, ysize=yheight
 	 		device, copy=[0,0,xwidth,yheight,0,0,draw_win1]											 
			
 		   endif               
             end
             
  'DRAW2': begin
 		   wset,  draw_win2
 		   xval1=!x.window(0)*!d.x_size
 		   xval2=!x.window(1)*!d.x_size 
 		   yval1=!y.window(0)*!d.y_size
 		   yval2=!y.window(1)*!d.y_size 		   		   
		   tmpx=(float(event.x)-xval1)/(xval2-xval1)
		   tmpy=(float(event.y)-yval1)/(yval2-yval1)
		   if state.plot_stuff.xlog eq 1 then begin	
 		   	xtmp=10^(!x.crange(0)+tmpx*(!x.crange(1)-!x.crange(0)))
 		   	xmin=10^!x.crange(0)
		   endif else begin
 		   	xtmp=!x.crange(0)+tmpx*!x.crange(1)
 		   	xmin=!x.crange(0)
 		   endelse		      
		   if state.plot_stuff.ylog eq 1 then begin		   		
 		   	ytmp=10^(!y.crange(0)+tmpy*(!y.crange(1)-!y.crange(0)))	
		   	ymin=10^!y.crange(0) 
		   endif else begin
 		   	ytmp=!y.crange(0)+tmpy*!y.crange(1) 		 		   			   	
 		   	ymin=!y.crange(0)
 		   endelse
		   dummy=strtrim(string(xtmp),2)
		   widget_control,state.tempid, set_value=dummy	
		   dummy=strtrim(string(ytmp),2)
		   widget_control,state.densid, set_value=dummy		

		   dum=[state.numte,state.numden]
		   minpair=min(dum)						

		   if event.type eq 0 AND state.iflag eq 0 then begin

		   		;****************
		   		;** ADD AT END **
		   		;****************	   

		   	xindex=where(state.plot_stuff.x gt xtmp)
 		      yindex=where(state.plot_stuff.y gt ytmp)			
 		      index=where(newt gt 0.0,count)
 		      if xtmp ge xmin AND xtmp le state.plot_stuff.x(state.plot_stuff.nx-1)  $
 		      	AND ytmp ge ymin AND ytmp le state.plot_stuff.y(state.plot_stuff.ny-1) $
 		      	AND count le minpair-1 AND yindex(0)*xindex(0) gt 0 then begin
		   	  newt(count)=xtmp
		   	  newd(count)=ytmp		   			
			  plots,newt,newd,psym=-2,color=colour.red
 		        wset,  draw_win1
 		        ixtmp=(xtmp-state.plot_stuff.x(xindex(0)-1))/	$
 		        	(state.plot_stuff.x(xindex(0))-state.plot_stuff.x(xindex(0)-1))+xindex(0)-1
 		        iytmp=(double(ytmp)-double(state.plot_stuff.y(yindex(0)-1)))/	$
 		        	(double(state.plot_stuff.y(yindex(0)))-double(state.plot_stuff.y(yindex(0)-1)))+yindex(0)-1
			  state.z(count)=bilinear(state.pec,ixtmp,iytmp)
 		        plots,xtmp,ytmp,state.z(count),psym=2,/t3d,color=colour.red		 

			  window, 9, /pixmap, xsize=xwidth, ysize=yheight
 	 		  device, copy=[0,0,xwidth,yheight,0,0,draw_win1]											 
			   		        			      	   	
			endif
		   endif
		   if event.type eq 0 AND state.iflag eq 1 then begin 
		   
		   		;**********************
		   		;** ADD AT BEGINNING **
		   		;**********************
	      
 		       index =where(newt gt 0.0,npoints)	
 		       xindex=where(state.plot_stuff.x gt xtmp)
 		       yindex=where(state.plot_stuff.y gt ytmp)

			 if xtmp ge xmin AND xtmp le state.plot_stuff.x(state.plot_stuff.nx-1)  $
 		      	AND ytmp ge ymin AND ytmp le state.plot_stuff.y(state.plot_stuff.ny-1) $
 		      	AND npoints le minpair-1 AND yindex(0)*xindex(0) gt 0 then begin
	   
 		       	if npoints lt state.numte then begin
 		       	  for i=npoints,1,-1 do begin
 		       		newt(i)=newt(i-1)
  		       		newd(i)=newd(i-1)
  		       		state.z(i)=state.z(i-1)
  		       	  endfor
  		       	  newt(0)=xtmp
  		       	  newd(0)=ytmp
			 	  plots,newt,newd,psym=-2,color=colour.red
 		       	  wset,  draw_win1
 		       	  ixtmp=(xtmp-state.tval(xindex(0)-1))/	$
 		       		(state.plot_stuff.x(xindex(0))-state.plot_stuff.x(xindex(0)-1))+xindex(0)-1
 		       	  iytmp=(ytmp-state.plot_stuff.y(yindex(0)-1))/	$
 		       		(state.plot_stuff.y(yindex(0))-state.plot_stuff.y(yindex(0)-1))+yindex(0)-1
			 	  state.z(0)=bilinear(state.pec,ixtmp,iytmp)
 		       	  plots,xtmp,ytmp,state.z(0),psym=2,/t3d,color=colour.red

			 	  window, 9, /pixmap, xsize=xwidth, ysize=yheight
 	 		 	  device, copy=[0,0,xwidth,yheight,0,0,draw_win1]
	
 		       	endif
			endif
		   endif
		   if event.type eq 0 AND state.iflag eq 2 then begin
		   
		   		;************
		   		;** INSERT **
		   		;************
 
 		      index =where(newt gt 0.0,npoints)
 		      insert=-1 
			if npoints le minpair-1 then begin
			   for i=1,npoints-1 do begin
			   	   if xtmp ge newt(i-1) AND xtmp le newt(i) then begin
			   		if ytmp ge newd(i-1) AND ytmp le newd(i) then begin
			   		   insert=i
			   		endif else if ytmp ge newd(i) AND ytmp le newd(i-1) then begin
 		         		   insert=i
 		         		endif
 		         	   endif else if xtmp ge newt(i) AND xtmp le newt(i-1) then begin
			   		if ytmp ge newd(i-1) AND ytmp le newd(i) then begin
			   		   insert=i
			   		endif else if ytmp ge newd(i) AND ytmp le newd(i-1) then begin
 		         		   insert=i
 		         		endif
 		         	   endif
 		         endfor
 		         if insert ne -1 then begin
 		         	   if npoints lt state.numte then begin
 		         	     plots,newt,newd,psym=-2,color=colour.black 			   
 		         	     for i=npoints,insert+1,-1 do begin
 		         		   newt(i)=newt(i-1)
  		         		   newd(i)=newd(i-1)
  		         		   state.z(i)=state.z(i-1)
  		         	     endfor
  		         	     newt(insert)=xtmp
  		         	     newd(insert)=ytmp     
			   	     plots,newt,newd,psym=-2,color=colour.red
 		         	     wset,  draw_win1
 		         	     xindex=where(state.plot_stuff.x gt xtmp)
 		         		     yindex=where(state.plot_stuff.y gt ytmp)
 		         	     ixtmp=(xtmp-state.plot_stuff.x(xindex(0)-1))/ $
 		         		   (state.plot_stuff.x(xindex(0))-state.plot_stuff.x(xindex(0)-1))+xindex(0)-1
 		         	     iytmp=(ytmp-state.plot_stuff.y(yindex(0)-1))/ $
 		         		   (state.plot_stuff.y(yindex(0))-state.plot_stuff.y(yindex(0)-1))+yindex(0)-1
			   	     state.z(insert)=bilinear(state.pec,ixtmp,iytmp)
 		         	     plots,xtmp,ytmp,state.z(insert),psym=2,/t3d,color=colour.red 					      					

			   	     window, 9, /pixmap, xsize=xwidth, ysize=yheight
 	 		   	     device, copy=[0,0,xwidth,yheight,0,0,draw_win1]  						   	      		 

 		         	   endif
 		         endif
			endif 
		   endif		   	
		   if event.type eq 0 AND state.iflag eq 3 then begin	
		   	
		   		;************
		   		;** DELETE **
		   		;************

 		      index =where(newt gt 0.0,npoints)
 		      if npoints ge 1 then begin
		   		x1=where(newt gt xtmp-xtmp/10.0 AND newt(0:npoints-1) lt xtmp+xtmp/10.0)
		   		y1=where(newd gt ytmp-ytmp/5.0 AND newd(0:npoints-1) lt ytmp+ytmp/5.0)
						
				if x1(0) eq -1 OR y1(0) eq -1 then begin
					pindex=-1
				endif else begin
					pindex=-1
	   				pindex=where(x1 eq y1)
	   			endelse
	      
		   		if pindex(0) ne -1  then begin
			  		plots,xtmp,ytmp,psym=2,color=colour.black
					index=where(newt gt 0.0,npoints)			  	
			  		for i=x1(pindex(0)),npoints-2 do begin
			  			newt(i)=newt(i+1)
			  			newd(i)=newd(i+1)
			  		endfor			  	
			  		newt(npoints-1)=0.0
			  		newd(npoints-1)=0.0
					wset,  draw_win1
		      			adas_pairsel_plot, state.updateid, value = state.plot_stuff
		   			if npoints gt 0 then begin
		   				plots,newt(index),newd(index),state.z(index),psym=2,/t3d, color=colour.red
 		   				wset,  draw_win2
 		   				plots,newt(index),newd(index),psym=-2, color=colour.red
			  			window, 9, /pixmap, xsize=xwidth, ysize=yheight
 	 		  			device, copy=[0,0,xwidth,yheight,0,0,draw_win1]						
 		   			endif		      				   					  				  			  	
				endif
			endif		
		   endif 	
		   
		   wset,  draw_win1
		   xindex=where(state.plot_stuff.x gt xtmp)
		   yindex=where(state.plot_stuff.y gt ytmp) 
 		   if xtmp ge xmin AND xtmp le state.plot_stuff.x(state.plot_stuff.nx-1)  $
 		      	AND ytmp ge ymin AND ytmp le state.plot_stuff.y(state.plot_stuff.ny-1) $
				AND xindex(0)*yindex(0) gt 0 then begin
 		   	  ixtmp=(xtmp-state.plot_stuff.x(xindex(0)-1))/	  $
 		   	  	  (state.plot_stuff.x(xindex(0))-state.plot_stuff.x(xindex(0)-1))+xindex(0)-1
 		   	  iytmp=(double(ytmp)-double(state.plot_stuff.y(yindex(0)-1)))/ $
 		   	  	  (double(state.plot_stuff.y(yindex(0)))-double(state.plot_stuff.y(yindex(0)-1)))+yindex(0)-1
		   	  ztmp=bilinear(state.pec,ixtmp,iytmp)
			  device, copy=[0,0,xwidth,yheight,0,0,9]
 		   	  plots,xtmp,ytmp,ztmp,psym=2,/t3d,color=colour.yellow
		   endif 
             end 
             
   'ERASE'   : begin
     
		   newt(*)=0.0
		   newd(*)=0.0
               adas_pairsel_plot, state.updateid, value = state.plot_stuff	
               butval=0
               widget_control,state.eraseid, set_value=butval   		                  
               window, 9, /pixmap, xsize=xwidth, ysize=yheight
 	         device, copy=[0,0,xwidth,yheight,0,0,draw_win1]											 

             end                                     
  
   'POINTS'   : begin
		   widget_control,state.pointid,get_value=butval
		   state.iflag = butval
             end                                     


  'DONE'   : begin
  
               ; restore original colour table before leaving
               TVLCT, state.red, state.green, state.blue
			
               widget_control, event.top, /destroy
               
             end
  
             
             
  else : print,'BGOUT_GR_EVENT : You should not see this message! ',userEvent
                   
ENDCASE

                ;*****************************************
		;**** Update state but only if we are ****
                ;**** NOT leaving the widget.         ****
                ;*****************************************

 if userEvent ne 'DONE' then $
    widget_control, event.top, set_uvalue=state, /no_copy

END

;-----------------------------------------------------------------------------

PRO cw_adas_pairsel, VALUE=value, XSIZE = xsize, YSIZE = ysize, $
              FONT_LARGE = font_large, FONT_SMALL = font_small

COMMON new_data, newt, newd, ntindex, ndindex, xwidth, yheight

		;**** Set defaults for keywords ****

  IF NOT (KEYWORD_SET(xsize)) THEN xsize = 400
  IF NOT (KEYWORD_SET(ysize)) THEN ysize = 340
  IF NOT (KEYWORD_SET(font_large))  THEN font_large = ''
  IF NOT (KEYWORD_SET(font_small))  THEN font_small = ''
  IF NOT (KEYWORD_SET(value)) THEN $  
          message,'A structure must be passed to CW_ADAS_PAIRSEL'

;stop

  
  numte     = value.numte
  numden    = value.numden
  nbsel	= value.nbsel
  
  te        = value.te
  den       = value.den
  pec		= value.pec
  tval	= value.tval
  dval	= value.dval
  newt	= value.newt
  newd	= value.newd
  ntindex   = value.ntindex
  ndindex   = value.ndindex
  xstore	= 0.0
  ystore	= 0.0
  zstore	= 0.0
  z		= fltarr(numte)
  units	= value.units
    
  iunit_t   = 1
  iunit_d   = 1
  title     = '' 
  subtitle = ''
 

  
                ;***********************************
		;**** create titled base widget ****
                ;***********************************
                
  parent = widget_base(title='Point Value Selection Widget', $
			xoffset=20,yoffset=20,/column)
                
  rc = widget_label(parent,value='  ',font=font_large)

                 
  
                ;*****************************************
                ;**** Draw widget to display results. ****
                ;*****************************************

  device, get_screen_size=scrsz
  xwidth  = scrsz(0)*0.250
  yheight = scrsz(1)*0.30


  drow   = widget_base(parent, /row, /frame)
  dcol   = widget_base(drow, /column, xpad=10,ypad=10)  

  ; leave some space before user's choice column
  
  skip   = widget_label(drow, value='  ', font=font_large)
  
 
  drawid1 = widget_draw(dcol, xsize=xwidth, ysize=yheight) 
  drawid2 = widget_draw(dcol, xsize=xwidth, ysize=yheight, $
  			event_pro='adas_pairsel_event',/motion_events,$
  			/button_events,Uvalue='DRAW2')
  coordid = widget_base(dcol,/row,/frame)
  dummy  = widget_label(coordid,value='Te:',font=font_large)  
  tempid = widget_label(coordid,value='0.00000e+00',/align_left)
  if units eq 0 then begin
   dummy  = widget_label(coordid,value='K ',font=font_large)
  endif else if units eq 1 then begin
   dummy  = widget_label(coordid,value='eV',font=font_large)  
  endif else begin
   dummy  = widget_label(coordid,value='Red',font=font_large)  
  endelse	
  dummy  = widget_label(coordid,value=' Ne:',font=font_large)  
  densid = widget_label(coordid,value='0.00000e+00',/align_left)  
  dummy  = widget_label(coordid,value='cm-3',font=font_large)    
			
  prevx=0.0
  prevy=0.0
 
                ;*******************************************************
		;**** User choices - level, colour, style, position ****
                ;*******************************************************

  mcol     = widget_base(drow,/column,/base_align_left)
       
  ocol     = widget_base(mcol,/column,/frame)
  
  mlab     = widget_label(ocol, value='Graph Style: ', font=font_large)
  mlab     = widget_label(ocol, value=' ', font=font_large)  

  xrotid   = widget_slider(ocol,title='X rotation', font=font_large,     $
                           minimum=-90, maximum=90, scroll=10, value=30, $
                           Uvalue='XROT')

  zrotid   = widget_slider(ocol,title='Z rotation', font=font_large,     $
                           minimum=-90, maximum=90, scroll=10, value=30, $
                           Uvalue='ZROT')

  lab		= widget_label(ocol,value='Surface Styles:',font=font_large)
  sbase    = widget_base(ocol,/row)
  styles   = ['Mesh   ','Surface']
  styleid  = widget_droplist(sbase, value=styles,/align_left,  $
                          font=font_large,  Uvalue='STYLES')
  
  ; correspond to loadct 0, 3, 1, 8
  colours   = ['B-W     ','Red Temp','Blue    ','Green   ']
  colourid  = widget_droplist(sbase, value=colours,/align_left,  $
                          font=font_large,  Uvalue='COLOURS')                                       


  xbase = widget_base(ocol,/row)
  styles  = ['Linear','Log   ']
  if tval(te(nbsel)-1)/tval(0) ge 1e2 then xlog=1 else xlog=0
  xlab     = widget_label(xbase,value='X axis style:',font=font_large )
  xstyleid =  cw_bselector(xbase, styles, set_value=xlog, $
                          font=font_large,  Uvalue='XSTYLE')       

  ybase = widget_base(ocol,/row)                                         
  if dval(den(nbsel)-1)/dval(0) ge 1e2 then ylog=1 else ylog=0
  ylab     = widget_label(ybase,value='Y axis style:',font=font_large )
  ystyleid =  cw_bselector(ybase, styles, set_value=ylog, $
                          font=font_large,  Uvalue='YSTYLE') 
                                 

  elab = widget_label(mcol,value=' ',font=font_large)
  elab = widget_label(mcol,value=' ',font=font_large)  
  
  butbase = widget_base(mcol,/column,/frame)
  label   = widget_label(butbase,value='Pair value selector',font=font_large)
  label   = widget_label(butbase,value=' ')
  eraseid = cw_bgroup(butbase,'Erase',/nonexclusive,font=font_large,Uvalue='ERASE') 
  label   = widget_label(butbase,value=' ')
  buttons = [ 'Add at end','Add at beginning','Insert point','Delete point']
  iflag=0
  pointid = cw_bgroup(butbase,buttons,font=font_large,Uvalue='POINTS',	$
  		set_value=iflag,/exclusive,/column,/no_release)
              
 		;***********************************
		;**** set up initial conditions ****
		;***********************************
 
  widget_control, styleid,  set_droplist_select = 0
  widget_control, colourid, set_droplist_select = 0

  widget_control, xrotid,   sensitive = 1
  widget_control, zrotid,   sensitive = 1
  widget_control, styleid,  sensitive = 1
  widget_control, colourid, sensitive = 1

               ;*****************
		;**** Buttons ****
                ;*****************
                
  base    = widget_base(parent,/row)
  doneid  = widget_button(base,value='Done',font=font_large,Uvalue='DONE')
  
  mlab   = widget_label(base, value='     ', font=font_large)
  
  

                ;**************************
		;**** Read data update ****
                ;**************************
  
  updateid = widget_label(base,value='                                      ',$
                          font=font_large,/align_right)
  

  
  
                ;**************************************
		;**** Get the current colour table ****
                ;**************************************
                
  TVLCT, r,g,b,/get
  
               
               
                ;****************************************************
		;**** put all needed info into a state structure ****
                ;****************************************************
   

 plot_stuff = {  x       	: tval(*,units),        $
                 y      	: dval,        		$
                 z	  	: pec,			$
                 nx	  	: te(nbsel),		$
                 ny		: den(nbsel),		$
                 title    	: title,      		$
                 subtitle 	: subtitle,   		$
                 ax       	: 30,         		$
                 az       	: 30,         		$
                 colour   	: 0,    			$
                 style    	: 0,				$
                 xlog		: xlog,			$
                 ylog		: ylog,			$                 
                 win1	  	: drawid1,			$
                 win2	  	: drawid2     }
               

   state = { numte      :  numte,       $
             numden     :  numden,      $
             nbsel	:  nbsel,	$
	       xrotid     :  xrotid,      $
	       zrotid     :  zrotid,      $
	       styleid    :  styleid,     $
	       colourid   :  colourid,    $
             xstyleid   :  xstyleid,	$
             ystyleid   :  ystyleid,	$ 
             eraseid	:  eraseid,		$
             pointid	:  pointid,		$  
             iflag 	:  iflag,	$         
             te         :  te,          $
             den        :  den,         $
             pec		:  pec,	$
             tval		:  tval,	$
             dval		:  dval,	$
             newt	      :  newt,	$
             newd		:  newd,	$
             z		:  z,		$
             units	:  units,	$
             xlog		:  xlog,	$
             ylog		:  ylog,	$
	     drawid1     :  drawid1,   $
	     drawid2     :  drawid2,    $	
            lev_ind    :  1,          $         ; default values if
	     sty_ind    :  0,           $         ; plot is pressed first
	     col_ind    :  0,           $
;	     ch_ids     :  ch_ids,      $
	     ax         :  30,          $
	     az         :  30,          $
	     title      :  title,       $
	     subtitle   :  subtitle,    $
	     updateid   :  updateid,    $
	     font       :  font_large,  $
           red	    :  r,		  $
           green      :  g,		  $
           blue	    :  b ,  	    $
           tempid     : tempid,	    $
           densid     : densid,	    $
	     plot_stuff : plot_stuff  	   }
  
  
                ;****************************
		;**** realize the widget ****
                ;****************************

;  dynlabel, parent


  widget_control,parent, /realize, set_uvalue=state, /no_copy

  widget_control,drawid1,get_value=draw_win1
  wset,draw_win1
  adas_pairsel_plot, updateid, value=plot_stuff

  xwidth=!d.x_size
  yheight=!d.y_size 
  window, 9, /pixmap, xsize=xwidth, ysize=yheight
  device, copy=[0,0,xwidth,yheight,0,0,draw_win1]
  
;  struct={widget_slider, id:0L, top:0L, handler: 0L, value: 30, drag:0L}
;  widget_control,xrotid,send_event=struct,/no_copy
 
              adas_pairsel_plot, updateid, value=plot_stuff
		;**** and make it modal ****
                
  xmanager,'adas_pairsel', parent, $
           event_handler = 'adas_pairsel_event', /modal, /just_reg
 
value.newt = newt
value.newd = newd
value.ntindex = ntindex
value.ndindex = ndindex

END
