; Copyright (c) 1995, Strathclyde University .
; SCCS Info : Module @(#)$Header: /home/adascvs/idl/adaslib/gui/cw_opt_value.pro,v 1.7 2004/07/06 13:08:56 whitefor Exp $   Date $Date: 2004/07/06 13:08:56 $
;
;+
; PROJECT:
;       ADAS IBM MVS to   UNIX conversion
;
; NAME:
;	CW_OPT_VALUE()
;
; PURPOSE:
;	Compound widget consisting of option button(s) and associated
;	text widget for inputting suitable value(s).
;
; EXPLANATION:
;	This widget produces a set of buttons which can be set or unset
;       and if set then a text widget is activated to accept a value
;	from the user. 
;
; USE:
;	This simple example creates a base and then creates this
;	reaction selection widget as the only child.  Note that in order
;	for the widget buttons to react properly and for the widget-value
;	to be correctly maintained this widget must be managed using
;	XMANAGER or widget_event()
;
; INPUTS:
;       PARENT   - The ID of the parent widget.
;
;	OPTIONS  - A string array of labels for each button.
;
; OPTIONAL INPUTS:
;	None.
;
; OUTPUTS:
;       The return value of this function is the ID of the compound
;       widget which is of type LONG integer.
;
; OPTIONAL OUTPUTS:
;       None
;
; KEYWORD PARAMETERS:
;	TITLE    - An optional title to go at the top of the widget.
;
;	ONEOFMANY- allows more than one option to be chosen
;
;	LIMITS   - this will be an array of values which define
;		   sensible limits for each option. If both limits for
;		   a particular button are zero :
;       FONT     - A font to use for all text in this widget.
;
;	UVALUE	 - A user value for this widget.
;
; CALLS:
;       See side effects for other related widget management routines.
;
; SIDE EFFECTS:
;
;       Three other routines are included which are used to manage the
;       widget;
;
;	ADASOPT_SET_VAL
;	ADASOPT_GET_VAL()
;	ADASOPT_EVENT()
;
; CATEGORY:
;	Compound Widget
;
; WRITTEN:
;       Lalit Jalota, Tessella Support Services plc, 9-Dec-1995
;
; MODIFIED:
;	27-Feb-1995	L. Jalota	Tidied up display of values so that 
;					numbers are always seen, instead of
;					numbers with spaces being off display
;
;	1.6	William Osborn(Tessella Support Services plc)
;		Removed ysize=1 from widget_label call. It produced labels
;		one pixel high on IDL v4.0.
;
;	1.7	William Osborn(Tessella Support Services plc)
;		Let button widget be nonexclusive if only one button
;		present
;
; VERSION:
;	1	21-Feb-1995	First Release
;	1.1	27-Feb-1995	Improved display of values. 
;	
;	1.6	20-05-96	
;	1.7	01-11-96	
;-
;-----------------------------------------------------------------------------

pro opt_set_val, id, value

		;**** Return to caller on error ****
  on_error, 2

		;**** Retrieve the state ****
  widget_control, id, get_uvalue=state, /no_copy
 
		;**** set option settings ****
  for i = 0, n_elements(butid)-1 do begin 
      widget_control, butid(i), set_button= butval(i)
  endfor

		;**** Return the state ****
  widget_control, id, set_uvalue=state, /no_copy
   
END

;-----------------------------------------------------------------------------


FUNCTION opt_get_val, id


		;**** Return to caller on error ****
  ON_ERROR, 2

		;**** Retrieve the state ****
  widget_control, id, get_uvalue=state, /no_copy

		;**** If options are requested then get values ****
  for i = 0, state.noptions-1 do begin
      if (state.butid(i) eq 1) then begin
	 widget_control, state.inpid(i), get_value=value

	 if (num_chk(value) eq 0) then  begin
	    state.optionset.value(i) = strcompress(value)
	 endif else begin
	    print, 'INVALID value selected'
         endelse
      endif
  endfor

  widget_control, id, set_uvalue=state,/no_copy
  RETURN, state.inp_state

END

;-----------------------------------------------------------------------------

FUNCTION opt_event, event


		;**** Retrieve the state ****
  widget_control, event.handler,get_uvalue=state, /no_copy
  
		;************************************************
		;**** 		Process Events 		     ****
		;**** Change the text and input window state ****
		;**** to the opposite of the current state   ****
		;**** Also save new state values	     ****
		;************************************************

		;*** See if a button was pressed ***
		;*** If pressed change states    ***
  button_index = where(event.id eq state.butid)
  if (button_index(0) ne -1) then begin
     state.but_state(button_index) = 1-state.but_state(button_index)
     state.optionset.option(button_index(0)) = $
			      1-state.optionset.option(button_index(0))

     new_text_state = (1-state.txt_state(button_index(0)))
     widget_control, state.txtid(button_index(0)), $
	sensitive= new_text_state
     state.txt_state(button_index(0)) = new_text_state

     new_input_state = new_text_state
     state.inp_state(button_index(0)) = new_input_state
     widget_control, state.inpid(button_index(0)), $
	sensitive= new_input_state
  endif 
  		;*** get the input values and save to state ***
  for i = 0, state.noptions-1 do begin
      widget_control, state.inpid(i), get_value = new_value
      state.optionset.value(i) = strcompress(new_value)
  endfor

		;****************************************************
		;*** Check to see if options are exclusive or not ***
		;*** and if they are then de-sensitize other ones ***
		;****************************************************

  if (state.oneofmany eq 1) then begin
     other_button_index = where(event.id ne state.butid)
     for i = 0, state.noptions-2 do begin
        index = other_button_index(i)
	widget_control, state.butid(index), set_button = 0
        state.but_state(index) = 0
	widget_control, state.txtid(index), sensitive = 0
        widget_control, state.inpid(index), sensitive = 0 
        widget_control, state.inpid(index), set_value = ' ' 
     endfor
     state.inp_state(other_button_index) = 0
     state.txt_state(other_button_index) = 0
  endif

		;************************************
		;*** reset the values in state   ****
		;************************************
  widget_control, event.handler, set_uvalue=state,/no_copy 

		;*** Return the event structure *****
      action = 'reaction'
  RETURN, {ID:event.handler, TOP:event.top, HANDLER:0L, ACTION:action}
END

;-----------------------------------------------------------------------------

function cw_opt_value, parent, options, $
			    title=title, oneofmany=oneofmany, $
			    limits=limits, $
			    font=font, value=value


  if (n_params() lt 1) then message, 'Must specify PARENT for cw_opt_value'

		;**** Set defaults for keywords ****
  if (not keyword_set(title)) then title = ''
  if (not keyword_set(oneofmany)) then oneofmany=0
  if (not keyword_set(font))  then font = ''

		;*** See how many options have been supplied ****
  soption = size(options)
  if (soption(0) eq 0) or (soption(0) gt 1) then begin
     message, 'INVALID: OPTIONS should be a 1-D array'
  endif else begin
     noptions = soption(1)
  endelse
		;**** set the value structure if not set ****
  if (not keyword_set(value)) then  begin
     optarr = intarr(noptions)
     valarr = strcompress(sindgen(noptions))
     optionset = {opt, option:optarr, value:valarr}
  endif else begin
     optionset = {opt, option:[value.option], value:[value.val] } 
  endelse
  
  if (not keyword_set(limits)) then begin
     limits = fltarr(2*noptions)
     optflag = intarr(noptions*2)
  endif else begin
		;*** set up flags for limits to options ***
     optflag = intarr(noptions*2)
     nlim = 2*fix(noptions) -2 
     for i = 0, nlim, 2 do begin
        if limits(i) eq limits(i+1) then begin
           optflag(i:i+1) = 0
        endif else begin
           optflag(i:i+1) = 1
        endelse
     endfor
  endelse

		;*********************************************
		;**** Create the main base for the widget ****
		;*********************************************
  cwid = widget_base(parent, $;UVALUE = uvalue, $
		event_func = "opt_event", $
		func_get_value = "opt_get_val", $
		pro_set_value = "opt_set_val", $
		/column)

		;**** Title ****
  labelid = widget_label(cwid, value=title, font=font)

	        ;**** create new base and create buttons + input ***
  base = widget_base(cwid, /column, /frame)
  butid = intarr(noptions)
  txtid = intarr(noptions)
  inpid = intarr(noptions)
  rowid = intarr(noptions)
  for i = 0, noptions-1 do begin
     rowid(i) = widget_base(base, /row)

		;*** Create buttons ****
     if noptions gt 1 then begin
         butbase = widget_base(rowid(i) , /exclusive)
     endif else begin
         butbase = widget_base(rowid(i) , /nonexclusive)
     endelse
     butid(i) = widget_button(butbase, value='  ')

		;**** Create text accompanying button ****
     txtid(i) = widget_label(rowid(i), $
		             value=options(i)+ '    value % : ', $
			     font=font			    )

 		;**** Create input widget for value input ****
     inpid(i) = widget_text(rowid(i),/edit,xsize=8,ysize=1, font=font)
     if n_elements(optionset.value) gt 1 then begin
        widget_control,inpid(i),set_value=strcompress(optionset.value(i))
     endif else begin
        widget_control, inpid(i), set_value=strcompress(optionset.value)
     endelse
  endfor

		;****  set buttons ****
  for i = 0, noptions-1 do begin
      widget_control, butid(i), set_button=optionset.option(i)
      widget_control, txtid(i), sensitive=optionset.option(i)
      widget_control, inpid(i), sensitive=optionset.option(i)
  endfor

		;**** Create state structure ****
   start_state = intarr(noptions)
   for i = 0, noptions-1 do begin
      start_state(i) = optionset.option(i)
   endfor 
  text_start_state = strarr(noptions)
  state =  { noptions:noptions, $
	     oneofmany:oneofmany, $
	     rowid:rowid,	$
	     butid:butid, 	$
	     but_state:start_state, $
	     txtid:txtid, 	$
             txt_state:start_state, $
	     inpid:inpid,	$
	     inp_state:text_start_state, $
	     optionset:optionset $
           }

		;**** Save initial state structure ****
   widget_control, cwid, set_uvalue=state

  RETURN, cwid

END
