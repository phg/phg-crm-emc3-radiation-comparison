; Copyright (c) 1995 Strathclyde University 
; SCCS info: Module @(#)$Header: /home/adascvs/idl/adaslib/gui/adas_progressbar.pro,v 1.4 2007/09/03 12:10:30 mog Exp $ Date $Date: 2007/09/03 12:10:30 $
;+
; PROJECT:
;       ADAS 
;
; NAME:
;       ADAS_PROGRESSBAR
;
; PURPOSE:
;       Pops up a progress bar and updates.
;
; EXPLANATION:
;
; INPUTS:
;       pipe     - value of bi-directional pipe between IDL and fortran.
;       n_call   - number of times it should be called before being destroyed.
;
; OPTIONAL INPUTS:
;       adasprog - name of ADAS calling program (for title).
;       font     - font for on screen display.
;
; OUTPUTS:
;       None.
;
; EXAMPLE:
;       
; OPTIONAL OUTPUTS:
;       None
;
; KEYWORD PARAMETERS:
;       None
;
; CALLS:
;       None
;       
; SIDE EFFECTS:
;       Puts up a widget and interacts with fortran.
;
; CATEGORY:
;       Widgets.
;
; WRITTEN:
;       Martin O'Mullane
;
; MODIFIED:
;       1.1     Martin O'Mullane
;                - First release 
;       1.2     Martin O'Mullane
;                - Size of progress bar was not the correct percentage.
;       1.3     Martin O'Mullane
;                - Change integer constants to longs to avoid overflows
;                  and subsequent negative percentages.
;       1.4     Martin O'Mullane
;                - Add optional variable for message text.
;
; VERSION:
;       1.1     29-04-2003
;       1.2     28-02-2004
;       1.3     23-02-2005
;
;-
;-----------------------------------------------------------------------------


PRO adas_progressbar, pipe     = pipe,     $
                      n_call   = n_call,   $
                      adasprog = adasprog, $
                      message  = message,  $
                      font     = font


; Check input and set sensible defaults.                      

if n_elements(adasprog) EQ 0 then tstr = '' else tstr = adasprog
if n_elements(pipe) EQ 0 then message, 'A fortran pipe is required input'
if n_elements(message) EQ 0 then mess='Computation Underway - Please Wait'$
                            else mess=message
if n_elements(font) EQ 0 then font=''


; Create a progress bar widget

    widget_control, /hourglass
    base  = widget_base(/column, xoffset=300, yoffset=200,           $
                        title = tstr+' : INFORMATION')

    lab     = widget_label(base, value=' ')
    lab     = widget_label(base,  font=font,  value='   '+ tstr + ' : ' + mess)
    lab     = widget_label(base, value=' ')
    
    barbase = widget_base(base,/align_center)
    
    grap    = widget_draw(barbase, ysize=20, xsize=480) 
    
    mlab    = widget_label(base, value = ' ')
    mbase   = widget_base(base, /row, /align_center)
    barID   = widget_label(mbase, value = '   0', font=font)
    blab    = widget_label(mbase, value = '% complete', font=font)
    
    dynlabel, base
    widget_control, base, /realize


    ; Get events from program and update progress bar.
    
        
    nlines = 0
    next   = 0
    step   = 1000.0/n_call
    
    for i = 0, n_call-1 do begin
    
      ON_IOERROR, jump_out      
      readf, pipe, next
      p = float(i)/n_call*1000L   
      q = p+step              
      
      for j=fix(p),fix(q) do begin
          x = (float(j)+1)/1000.0
          plots, [x, x],[0.0,1.0], /normal
      endfor
            
      widget_control, barID, set_value = string(fix(100L*(i+1)/n_call), format='(i3)')
                          
    endfor
    jump_out:


    ; Destroy it

    widget_control, base, /destroy



END
