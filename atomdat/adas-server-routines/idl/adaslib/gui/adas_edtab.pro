; Copyright (c) 1996, Strathclyde University.
; SCCS info: Module @(#)$Header: /home/adascvs/idl/adaslib/gui/adas_edtab.pro,v 1.26 2004/07/06 11:10:02 whitefor Exp $ Date $Date: 2004/07/06 11:10:02 $
;+
; PROJECT:
;       ADAS IBM MVS to   UNIX conversion
;
; NAME:
;	ADAS_EDTAB
;
; PURPOSE:
;	A pop-up window to edit ADAS numerical data in a table format.
;
; EXPLANATION:
;	This routine generates a pop-up window which allows the user to
;	edit ADAS numerical data in a table format.  The data is
;	actually passed into and out of the routine as an array of
;	strings which should all represent valid floating point numbers.
;	Using strings in this way means that values can be left blank
;	and that this routine can be used for data entry as well as
;	editing.
;
;	The pop-up window consists of a main title, the table editor
;	widget CW_EDIT_TABLE, a Cancel button and a Done button.
;	Optionally the caller can add a series of 'Units' which apply
;	to nominated columns in the table.  A number of toggle buttons
;	allow the user to switch the numeric data between the available
;	units.  To implement this unit change extra data must be
;	included in the VALUE input array.  Units changing works by
;	pulling out different subsets of data from the VALUE array.
;	The caller must specify which columns from VALUE make up the
;	table for each of the valid units via the UNITIND input.  Any
;	table column which has its data switched in this way should
;	be non-editable since changes made by a user will not be carried
;	forward automatically when the units are changed.
;
;	The table editor is a spreadsheet-type array of cells.  The VALUE
;	input should be a 1-D or 2-D array of text string representations
;	of numerical data.  The table sizes automatically to fit the array
;	dimensions.  Each column in the table can be given a header
;	consisting of a number of rows of text.  An integer row index can
;	be optionally displayed in a column along the edge of the table.
;	The table may include editable and non-editable columns as defined
;	by the caller.
;
;	The user can direct the keyboard input to any table cell by
;	clicking on that cell with the mouse.  If the user presses the 
;       return key the keyboard input moves to the next cell.  The
;       direction in which the active cell moves, down columns or across
;       rows, is under the control of the user via the 'Row_skip' and
;       'Column_skip' toggle buttons.  When the end of a column or row
;       is reached the input moves to the begining of the next column or
;       row.
;
;	The caller can specify two different fonts via the FONTS keyword
;	to distinguish between editable and non-editable data.
;
;	A number of editing features are provided with the edit widget.
;	These features are listed with toggle buttons along the bottom
;	of the edit cell array.  To use an edit feature position the
;	keyboard input cursor on the required cell, switch on the edit
;	facility with its toggle button and then press the return key
;	to perform the operation.  The edit facilities are;
;
;		Delete - Set the current text cell to empty.
;		Remove - Shift all values in the column up one space
;			 from one below the current cell to the bottom
;			 of the column.
;		Insert - Insert a blank value at the current cell
;			 by moving values in the column down on space.
;		Copy   - Copy the value from the current cell into a
;			 buffer.
;		Paste  - Copy the current buffer contents into the
;			 current cell.
;
;	Also included are two buttons which allow the user to scroll
;	up and down a list when the number of rows is too large to display 
;	at once.
;		Scroll up - This moves the list upwards
;		Scroll down - This moves the list downwards.
;
;	These edit control buttons can be removed from the widget by
;	specifying the NOCON keyword.
;
; USE:
;	The following code produces the pop-up data edit window.  The
;	data is in 4 columns of 5 rows and has the inital value of
;	100.0 for all elements.  There are no units specified for the
;	numeric data.  The table has column headers.  Column 4 of the
;	table is not editable.  Fonts have been specified to distinguish
;	between the editable and non-editable columns.  The size of
;	each cell in the table has been set at 8 characters long.  When
;	run the edit window appears.  When the user completes the edit
;	by activating either the 'Done' or 'Cancel' buttons the data is
;	printed.  If the cancel button is pressed any changes made by
;	the user during the edit will be ignored.
;
;	data = make_array(4,5,/float,value=100.0)
;	value = strtrim(string(data),2)
;	adas_edtab,value,coledit=[1,1,1,0], $
;	    colhead=['Col 1','Col 2','Col 3','Col 4'], $
;	    fonts={font_norm:'times_roman14',font_input:'times_italic14'}, $
;	    /rowskip,/noindex,cellsize=8
;	data = float(value)
;	print,data
;
;	This second piece of code produces an edit table with units
;	switching.  The units are units of volume and apply to the first
;	two columns only as defined by the UNITIND array.  For example
;	column 2 of the table has 'ml' values of 500.0, 'cl' values of 
;	50.0 and 'litres' values of 0.5 from data(3,*), data(4,*) and
;	data(5,*).  On input the volume units of the data are displayed as
;	'ml' indicated by a unit index of unit=0.  The columns for which
;	the units may switch are non-editable.  The table has two-row
;	column headers and a window label has been specified.  The NOINDEX
;	keyword has been sepcifed to remove the integer index column from
;	the table display.  When the user quits the window the numeric
;	data and the current units are printed out.
;
;	data = strarr(8,5)
;	data(0,*) = 1000.0
;	data(1,*) = 100.0
;	data(2,*) = 1.0
;	data(3,*) = 500.0
;	data(4,*) = 50.0
;	data(5,*) = 0.5
;	data([6,7],*) = 40.0
;	value = strtrim(string(data),2)
;	units = 0
;	unitname = ['ml','cl','litres']
;	unitind = [[0,1,2],[3,4,5],[6,6,6],[7,7,7]]
;	unitstitle = 'Volume Units'
;	adas_edtab,value,units,unitname,unitind, $
;	    unitstitle=unitstitle, coledit=[0,0,1,1], $
;	    colhead=[['Col 1' ,'Col 2' ,'Col 3'      ,'Col 4'      ], $
;		     ['Volume','Volume','Temperature','Temperature']], $
;	    fonts={font_norm:'times_roman14',font_input:'times_italic14'}, $
;	    /noindex,cellsize=8,winlabel='Volume and Temperature Data'
;	data = float(value)
;	print,data(unitind(units,*),*)
;	print,'Volume Units are ',unitname(units)
;
; INPUTS:
;	VALUE    - Array of initial data values, type STRARR(ncols,nrows).
;		   Each string value should represent a valid floating
;		   point number.  Elements of the string array may contain
;		   empty strings.
;
; OPTIONAL INPUTS:
;	(Note: either ALL or NONE of these optional inputs should be
;	       provided by the caller)
;
;	UNITS    - Index number of the current units in the arrays that
;		   follow.
;
;	UNITNAME - 1D or 2D array of text strings giving the names of the
;		   units available. If 2D then each column has a set of units
;		   separate from the other columns.
;		   If different columns have different numbers of units then
;		   the string 'DUMMY' (case insensitive) may be used as
;		   padding where needed. Any string starting with 'HUGHS'
;		   (case insensitive) is also ignored (for backward
;		   compatibility). See ADAS101 and ADAS105 for examples of
;		   using different sets of units with different columns.
;
;	UNITIND  - 2D array if columns indecies which make up the table
;		   for the available units.  For example a 4 column
;		   table switchable between 3 possible units would have
;		   dimensions UNITIND(3,4).  A value of;
;		     [[0,0,0],[1,1,1],[2,3,4],[5,6,7]]
;		   would refer to the columns from a VALUE array with
;		   dimensions VALUE(8,nrows).  Columns 0 and 1 in the
;		   table always come from columns 0 and 1 in the VALUE
;		   array but column 2 in the table is taken from columns
;		   2, 3 and 4 in VALUE as the units are changed between
;		   0, 1 and 2.
;
; OUTPUTS:
;	VALUE    - Array of final data values, type STRARR(ncols,nrows).
;		   Elements of the string array may contain empty strings.
;
; OPTIONAL OUTPUTS:
;	UNITS    - Index number of the current units selected by the
;		   user during the edit.
;
; KEYWORD PARAMETERS:
;	UNITSTITLE - A title to place above the units box.
;
;	COLEDIT  - Flags to indicate which columns are editable, type
;		   INTARR(NCOLS).  The default is all columns are editable.
;		   A value of [1,0,1] would cause the first and third
;		   columns to be editable.
;
;	COLHEAD  - Column headers, type STRARR(NCOLS,n).  Each of the n
;		   rows of header is placed one below another at the top
;		   of each column.
;
;	NOINDEX  - Set this keyword to remove the column of integer index
;		   values at the far left of the table.  The default is
;		   for the index column to appear.
;
;	TITLE    - The title for the edit table.  A string array with
;		   one element for each line of the title.
;
;	NOCON    - Set this keyword to remove the edit control buttons
;		   delete, remove, insert etc.
;
;	YSIZE    - Specifies the ysize of the table in rows of cells.
;		   If ysize is less than the number of rows the columns
;		   are split and placed side-by-side.
;
;	GAPS	 - Set this keyword to allow gaps in the columns of data.
;		   By default this routine will check that there are no
;		   embedded blank cells in a column of data (blank
;		   values are allowed at the end of a column)
;
;	DIFFLEN  - Set this keyword to allow the columns of data in the
;		   table to be of differing lengths when completed by
;		   the user.  By default this routine will check that
;		   the data entered by the user in each column has the
;		   same number of values.  Blanks at the ends of the
;		   table columns are allowed.
;
;	ORDER 	 - Specifies that the values in an editable column must
;		   be in ascending or descending numerical order, type
;		   intarr(NCOLS).  A value of [1,1,0,-1] would indicate
;		   that columns 1 and 2 should be in ascending order and
;		   column4 should be in descending order.  No checking
;		   would be performed on column 3. If you specify
;		   checking on a non-editable column that checking is
;		   ignored.  By default no order checking is performed.
;
;	LIMITS	 - Specifies the boundaries within which the values in the
;		   table columns must lie, type intarr(NCOLS).  A value
;		   of [1,2,0,-2,-1] for a 5 column table would indicate
;		   that column 1 values should be >=0, column 2 values
;		   should be >0, column 3 values are not checked, column
;		   4 values should be <0 and column 5 values <=0.
;		   By default no limits checking is performed.
;
;	CELLSIZE - Specifies the character width of the text widget
;		   cells which make up the table.  The default is 8.
;		   In some circumstances the windows toolkit may ignore
;		   this setting depending on the width of the column
;		   header.
;
;	NOEDIT   - Set this keyword to make all data fields non-editable.
;		   The default is for all data columns specified by COLEDIT
;		   to be editable.
;
;	ROWSKIP  - By default the cursor skips down columns of cells
;		   when the return key is pressed.  Set this keyword to
;		   cause the cursor to skip across rows in the table.
;
;	NUM_FORM - Format style used to write numbers out.
;
;       FLTINT   - String array containing different formats for
;                  different columns. If this is not set then all
;                  columns are set by default to NUM_FORM.
;
;	FONTS	 - Structure of two fonts to use for table to distinguish
;		   between non-editable (norm) and editable (input) cells;
;		   {font_norm:<string>, font_input:<string>}
;
;	WINLABEL - A label for the pop-up window.
;
; CALLS:
;	CW_EDIT_TABLE	Creates the edit table widget.
;	XMANAGER
;
;	See side effects for other related widget management routines.
;
; SIDE EFFECTS:
;       A COMMON BLOCK: ADAS_EDTAB_COM is used which is private to
;       ADAS_EDTAB and ADASEDTAB_EVENT.
;
;       This routine uses XMANAGER to manage the data edit window
;       and hence via event generation and use of the WIDGET_CONTROL
;	procedure the following routines and functions are indirectly
;	called;
;
;	ADASEDTAB_EVENT		(included in this file)
;	EDTAB_GET_VAL()		(see CW_EDIT_TABLE.PRO)
;	EDTAB_SET_VAL		(see CW_EDIT_TABLE.PRO)
;	EDTAB_EVENT()		(see CW_EDIT_TABLE.PRO)
;	NUM_CHK()		(used to check number input)
;
;	This edit table widget uses a COMMON BLOCK: CW_EDTAB_BLK to hold
;	the widget state.
;
; CATEGORY:
;	Widgets
;
; WRITTEN:
;       Andrew Bowen, Tessella Support Services plc, 17-Mar-1993
;
; MODIFIED:
;       Version 1       Andrew Bowen 
;                       First release.
;
;	Version 1.1	Lalit Jalota
;			Added Scroll options, and keyword NUM_FORM
;
;	Version 1.2	Lalit Jalota
;			Inluded checks to see if units are requested
;			previously assumed that they were.
;
;	Version 1.3	Lalit Jalota	
;			When updating or changing table, the data values are
;			obtained from structure associated with edit table which
;			always keeps an updated and full set of values even if 
;			they are scrolled off the screen. Also, checks are made
;			on full set of valueseven if scrolled off screen, 
;			previously assumed all values visble.
;
;	Version 1.15	Tim Hammond
;			Added keyword parameter fltint.
;	Version 1.16	Tim Hammond
;			Added capability to deal with an array of units
;			so that (e.g. in adas101) several buttons are
;			available and the units of different quantities
;			can be changed independently. These changes
;			were originally made by David Brooks of the
;			University of Strathclyde.
;	Version 1.17	Tim Hammond
;			Modified default value of checking variable
;			unitchk (1) for 1D array or no units given.
;	Version 1.18	Tim Hammond
;			Modified use of state.limits variable to allow
;			for use of a 1D array.
;	Version 1.19	Tim Hammond
;			Modified how limits are checked slightly in order
;			to avoid crashes when checking the limits in cases
;			when only one column must be checked, but that
;			doesn't necessarily imply that there is only one
;			column in the table (search for limset to see
;			parts of code affected)
;	Version 1.20	Tim Hammond
;			Yet more checking for single column tables. To 
;			avoid problems where IDL implicitly converts single
;			element arrays into integers or float values
;			have ensured that when a single column table
;			is edited and the limits are set, the value of
;			limits is set once again to an array.
;	Version 1.21	William Osborn
;			Corrected error in checking for single column tables
;			spotted by David Brooks - 'if n_elements(limset eq 1)'
;			changed to 'if n_elements(limset) eq 1'
;	Version 1.22	William Osborn
;			The case unitchk gt 1 was not being treated correctly.
;			In some places only unitchk eq 2 was treated. Also,
;			loops over the columns for unitchk gt 1 were only
;			from 0 to 2 in some places, so replaced with 0 to
;			ncols-1.
;	Version 1.23	William Osborn
;			Added dynlabel procedure for correct resizing of
;			label widgets with IDL v4.0.1.
;
;	Version 1.24	William Osborn
;			Removed xsize from message label widget.
;
;	Version 1.25	William Osborn
;			Changed code for multiple units so that any unit
;			called 'Dummy' or starting with 'Hughs' (case
;			insensitive) is ignored, allowing more flexible
;			definition of the number of units for each column.
;			They were hardwired to work for ADAS101 before.
;	Version 1.26	Hugh Summers
;			
;
; VERSION:
;       1       28-May-1993
;	1.1	21-Feb-1995
;	1.2	15-Mar-1995
;	1.3	28-Mar-1995
;	1.15	20-Jun-1995
;	1.16	12-Jul-1995
;	1.17	14-Jul-1995
;	1.18	08-Jan-1996
;	1.19	26-Jan-1996
;	1.20	30-Jan-1996
;	1.21	26-Apr-1996
;	1.22	12-Jun-1996
;	1.23	12-Sep-1996
;	1.24	12-Sep-1996
;	1.25	12-Sep-1996
;	1.26	05-03-2003
;-
;-----------------------------------------------------------------------------


PRO adasedtab_event, event

  COMMON adas_edtab_com, state, unitchk

		;**** Get information about the edit table ****
		;**** using the state variable saved with  ****
		;**** "edid" edit table widget id          ****

  widget_control, state.edid, get_uvalue=edit_state, /no_copy

		;**** Current scroll state ***

  scroll_offset = edit_state.scroll_offset

		;**** Number of rows ***

  nrows = edit_state.nrows

		;**** Current values in table       ****
		;**** This is used throughout this  ****
		;**** event routine 		    ****

  etvalue = edit_state.value

  widget_control, state.edid, set_uvalue=edit_state, /no_copy

		;**** clear any existing error message ****

  widget_control,state.messid,set_value=''

		;**** check for units toggle buttons ****

  if unitchk eq 1 then begin
      iunit = where(state.unitid eq event.id)
  endif else if (unitchk gt 1) then begin
      iunit = intarr(state.ncols)
      for j = 0,state.ncols-1 do begin
          iunit(j) = where(state.unitid(j,*) eq event.id)
      endfor
  endif

		;************************
		;**** process events ****
		;************************

  mk = -1
  if unitchk eq 1 then begin
      mk = iunit(0)
  endif else if (unitchk gt 1) then begin
      for i = 0,state.ncols-1 do begin
          if iunit(i) ge 0 then mk = mk + 1
      endfor
  endif

  if mk ge 0 then begin

		;**** units toggle buttons ****

    if event.select eq 0 then begin
      widget_control,event.id,set_button=1
      unit_change=0
    endif else begin
      oldunits = state.units
      if (unitchk eq 1) then begin
          state.units = iunit(0)
          widget_control,state.unitbase,set_button=0
      endif else if (unitchk gt 1) then begin
          for j = 0,state.ncols-1 do begin
              if iunit(j) ge 0 then begin
                  state.units(j) = iunit(j)
                  widget_control,state.unitbase(j),set_button=0
              endif
          endfor
      endif
      widget_control,event.id,set_button=1
      unit_change=1
    endelse

		;**** recover current values ****

    if unit_change eq 1 then begin
      widget_control, state.edid, get_value=value

		;*************************************************
		;**** Update table with new values            ****
		;**** First we select the Columns of the data ****
		;**** appropriate for the current units chosen****
		;**** Then subset this for those columns      ****
		;**** which are editable, so only modify those****
		;**** which need to be			      ****
		;*************************************************

      if (unitchk eq 1) then begin
          change_index = state.unitind(oldunits,*)
          edit_index = where(state.coledit eq 1)
          update_index = change_index(edit_index)

          state.value(update_index, scroll_offset:nrows-1) =  		$
		    value(edit_index,0:nrows-scroll_offset-1)

		;**** update table with new values ****

          widget_control,state.edid,set_value = 			$
          state.value(state.unitind(state.units,*),scroll_offset:*)

      		;**** Also update array stored in cw_edit_table ****

          widget_control, state.edid, get_uvalue=edit_state, /no_copy
          edit_state.value = state.value(state.unitind(state.units,*),*)
          widget_control, state.edid, set_uvalue=edit_state, /no_copy

      endif else if (unitchk gt 1) then begin
          
               ;**** update table with new values ****

          change_index=intarr(state.ncols)
          for j = 0,state.ncols-1 do begin
              tempo=state.unitind(state.units(j),*)
              change_index(j)=tempo(j)
          endfor
          widget_control,state.edid,set_value = $
          state.value(change_index,scroll_offset:*)

                ;**** Also update array stored in cw_edit_table ****

          widget_control, state.edid, get_uvalue=edit_state, /no_copy
          edit_state.value = state.value(change_index,*)
          widget_control, state.edid, set_uvalue=edit_state, /no_copy
      endif
    endif

  endif else begin

		;**** check for cancel/done buttons ****
    widget_control,event.id,get_uvalue=button
    CASE button OF

	'Cancel': begin
		    widget_control,event.top,/destroy
		    state.exitbut = 'Cancel'
		  end

	'Done'  : begin
		    widget_control,state.edid,get_value=value

			;**************************************
			;**** check table data as required ****
			;**************************************

			;*************************************************
			;**** Get updated values of table from        ****
			;**** CW_EDIT_TABLE because this always keeps ****
			;**** updated versions even if some numbers   ****
			;**** are scrolled off the page               ****
			;*************************************************


		    error = 0

			;**** find how many editable columns there are ****
		    edit = where(state.coledit ne 0)
		    numedit = size(edit)
		    numedit = numedit(1)

			;**** Check for illegal numeric values ****
			;**** cw_edit_table sets bad cells to  ****
			;**** !error when the widget value is  ****
			;**** retrieved using GET_VALUE.       ****
		    illval = where(value eq '!error')
		    if illval(0) ge 0 then begin
		      error = 1
		      widget_control,state.messid,set_value= $
			'Error: Illegal numeric value in the table.'
		    end

			;**** Check for gaps in the table. ****
			;**** Loop over the rows in a column and find ****
			;**** the first blank cell.  If any non-blank ****
			;**** values occur after that an error is     ****
			;**** flagged.				      ****

		    if numedit ge 1 and state.gaps eq 0 and $
							error eq 0 then begin
		      for i = 0, numedit-1 do begin
			if error eq 0 then begin
			  blank = 0
			  for j = 0, state.nrows-1 do begin
			    val = strtrim(etvalue(edit(i),j),2)
			    if val eq '' and blank eq 0 then blank = 1
			    if val ne '' and blank eq 1 then error = edit(i)+1
			  endfor
			endif
		      endfor

			;**** Flag error to user ****
		      if error gt 0 then begin
			widget_control,state.messid,set_value= $
			'Error: No gaps are allowed in the table - Column '+ $
						strtrim(string(error),2)
		      endif
		    endif


			;**** Check editable columns have equal length.****
			;**** First find the last value in each column ****
			;**** and store the positions in the array     ****
			;**** colend.  Then compare all the colend     ****
			;**** values to see if they are the same.      ****
		    if numedit gt 1 and state.difflen eq 0 and $
							error eq 0 then begin
		      colend = intarr(numedit)
		      for i = 0, numedit-1 do begin
	 		if error eq 0 then begin
			  cend = $
			   where(strtrim(etvalue(edit(i),*),2) eq '')
			  colend(i) = cend(0)
			end
		      end
		      colend1 = colend(0)
		      for i = 1, numedit-1 do begin
			if colend(i) ne colend1 then error = 1
		      end

			;**** Flag error to user ****
		      if error gt 0 then begin
			widget_control,state.messid,set_value= $
			'Error: All table columns must be of the same length.'
		      end
		    end

			;**** Check value limits in columns. ****
			;**** Loop over columns and rows checking ****
			;**** values with respect to zero as      ****
			;**** flagged in the limits array.        ****
		    limset = where(state.limits ne 0)
		    if numedit ge 1 and limset(0) ge 0 and error eq 0 then begin

		      for i = 0, numedit-1 do begin
			if n_elements(limset) eq 1 then begin
			    limit_check = state.limits(limset)
		   	endif else begin
			    limit_check = state.limits(edit(i))
			endelse
			if limit_check ne 0 and error eq 0 then begin

			  for j = 0, state.nrows-1 do begin
			    if strtrim(etvalue(edit(i),j),2) $
					ne '' then begin
			      val = double(etvalue(edit(i),j))
			      CASE limit_check OF
				 2:	if val le 0 then error = edit(i) + 1
				 1:	if val lt 0 then error = edit(i) + 1
				-1:	if val gt 0 then error = edit(i) + 1
				-2:	if val ge 0 then error = edit(i) + 1
			      ENDCASE
			    end
			  end

			end
		      end

			;**** Flag error to user ****
		      if error gt 0 then begin
			CASE state.limits(error-1) OF
			  2:	message = 'Error: Values in column '+ $
					strtrim(string(error),2)+ $
					' should be > zero'
			  1:	message = 'Error: Values in column '+ $
					strtrim(string(error),2)+ $
					' should be >= zero'
			  -1:	message = 'Error: Values in column '+ $
					strtrim(string(error),2)+ $
					' should be <= zero'
			  -2:	message = 'Error: Values in column '+ $
					strtrim(string(error),2)+ $
					' should be < zero'
			ENDCASE
			widget_control,state.messid,set_value=message
		      end

		    end

			;**** Check numerical order in columns. ****
			;**** Set up an array to hold the converted ****
			;**** numerical values for a column.  Loop  ****
			;**** over the rows in each editable column ****
			;**** checking the order according to the   ****
			;**** flags in state.order.		    ****
		    ordset = where(state.order ne 0)
		    if numedit ge 1 and ordset(0) ge 0 and error eq 0 then begin

		      vals = dblarr(state.nrows)
		      for i = 0, numedit-1 do begin
			if state.order(edit(i)) ne 0 then begin

			  if error eq 0 and $
				strtrim(etvalue(edit(i),0),2) ne '' $
					then begin
			    vals(0) = double(etvalue(edit(i),0))
			    for j = 1, state.nrows-1 do begin
			      if strtrim(etvalue(edit(i),j),2) $
					ne '' then begin
				vals(j) = double(etvalue(edit(i),j))
				CASE state.order(edit(i)) OF
				   1:	if vals(j) le vals(j-1) then $
							error = edit(i) + 1
				  -1:	if vals(j) ge vals(j-1) then $
							error = edit(i) + 1
				ENDCASE
			      end
			    end
			  end

			end
		      end

			;**** Flag error to user ****
		      if error gt 0 then begin
			CASE state.order(error-1) OF
			   1: message = 'Error: Values in column '+ $
					strtrim(string(error),2)+ $
					' should be in ascending order'
			  -1: message = 'Error: Values in column '+ $
					strtrim(string(error),2)+ $
					' should be in descending order'
			ENDCASE
			widget_control,state.messid,set_value=message
		      end

		    end


			;**** Update the state block and destroy the ****
			;**** widget if no errors have been found.   ****
			;**** Get updated values of table from        ****
			;**** CW_EDIT_TABLE because this always keeps ****
			;**** updated versions even if some numbers   ****
			;**** are scrolled off the page               ****

		    if error eq 0 then begin
                        if unitchk eq 1 then begin
		            if state.units eq -1 then begin
			        state.value = etvalue
		            endif else begin
			        change_index = state.unitind(state.units,*)
			        state.value(change_index,*) = etvalue
		            endelse
                        endif else if (unitchk gt 1) then begin
;                            if state.units(0) eq -1 and state.units(1) eq -1 $
;                            and state.units(2) eq -1 then begin
                            if state.units(0) eq -1 and state.units(1) eq -1 $
                            and state.units(state.ncols-1) eq -1 then begin
                                state.value = etvalue
                            endif else begin
                                change_index=intarr(state.ncols)
                                for j = 0,state.ncols-1 do begin
                                    tempo=state.unitind(state.units(j),*)
                                    change_index(j)=tempo(j)
                                endfor
                                state.value(change_index,*) = etvalue
                            endelse
                       endif
		       widget_control,event.top,/destroy
		       state.exitbut = 'Done'
		    endif
		  end

    ENDCASE

  end

END


;-----------------------------------------------------------------------------


PRO adas_edtab,	value, units, unitname, unitind, 			$
		UNITSTITLE = unitstitle, 				$
		COLEDIT = coledit, COLHEAD = colhead, 			$
		NOINDEX = noindex, TITLE = title, 			$
		NOCON = nocon, YSIZE = ysize, 				$
		GAPS = gaps, DIFFLEN = difflen, ORDER = order, 		$
		LIMITS = limits, 					$
		CELLSIZE = cellsize, NOEDIT = noedit, 			$
		ROWSKIP = rowskip, NUM_FORM = num_form, FONTS = fonts, 	$
		FLTINT = fltint, WINLABEL = winlabel 

  COMMON adas_edtab_com, state, unitchk


  IF (N_PARAMS() EQ 0) THEN MESSAGE, 'Must specify VALUE for adas_edtab'
  IF (N_PARAMS() GT 1 and N_PARAMS() NE 4) THEN MESSAGE, 		$
	'UNITS, UNITNAME, and UNITIND go together for adas_edtab'

  ON_ERROR, 2					;return to caller

                ;**** Determine the size of the table ****

  valsize = size(value)
  ndim = valsize(0)
  if units(0) eq -1 then begin
    ncols = valsize(ndim-1)
    nrows = valsize(ndim)
  end else begin
    indsize = size(unitind)
    ncols = indsize(2)
    nrows = valsize(ndim)
  end

		;**** Set defaults for keywords ****

  IF NOT (KEYWORD_SET(unitstitle)) THEN unitstitle = ''
  IF NOT (KEYWORD_SET(gaps)) THEN gaps = 0
  IF NOT (KEYWORD_SET(difflen)) THEN difflen = 0
  IF NOT (KEYWORD_SET(order)) THEN order = 0
  IF NOT (KEYWORD_SET(limits)) THEN begin
	limits = 0
  ENDIF ELSE begin
	limitsize = size(limits)
	if limitsize(0) eq 0 then begin		;if limits is a scalar
	    limitdummy = limits			;change to 1-d array
	    limits = intarr(1)
	    limits(0) = limitdummy
	endif
  ENDELSE
  IF NOT (KEYWORD_SET(num_form)) THEN num_form = '(E10.3)'
  IF NOT (KEYWORD_SET(fltint)) THEN fltint =make_array(ncols,/string,$
                                                value=num_form)
  IF NOT (KEYWORD_SET(fonts)) THEN fonts = {font_norm:'',font_input:''}
  IF NOT (KEYWORD_SET(winlabel)) THEN winlabel = ' '
  IF NOT (KEYWORD_SET(title)) THEN BEGIN 
	title = ' '
  ENDIF ELSE BEGIN
	if (n_elements(title) gt 1) then begin
	   title = title(0)
        endif
  ENDELSE

		;**** Check for consistency in units info ****
  IF (N_PARAMS() GT 1) THEN begin
    if units(0) gt -1 then begin
      namesize = size(unitname)
      indsize = size(unitind)
      ncols = indsize(2)
       if ((namesize(0) ne 1) and (namesize(0) ne 2)) then 		$
          MESSAGE, 'adas_edtab; UNITNAME must be 1D/2D array' 
      if (indsize(0) ne 2) then 					$
          MESSAGE, 'adas_edtab; UNITIND must be a 2D array'
      nunits = namesize(1)
      if (namesize(0) eq 1) then begin
          unitchk = 1
      endif else begin
          unitchk = namesize(1)
      endelse
      if (nunits ne indsize(1)) then MESSAGE, 				$
        'adas_edtab; dimension of UNITNAME and first dimension of UNITIND'+ $
	' must be the same'
      if (indsize(2) gt valsize(1)) then MESSAGE, 			$
        'adas_edtab; second dimension of UNITID must be <= first dimension'+ $
	' of VALUE'
      if unitchk eq 1 then begin
          if (units lt 0) or (units ge nunits) then MESSAGE, 		$
          'adas_edtab; UNITS must point to an element in the array UNITNAME'
      endif else begin
          if (units(0) lt 0) or (units(ncols-1) ge nunits) then MESSAGE, $
          'adas_edtab; UNITS must point to an element in the array UNITNAME'
      endelse
    endif else begin
        unitchk = 1
    endelse
  endif else begin
    units = -1
    unitchk = 1
  endelse

		;**** Check the input VALUE and get row/col sizes ****

  ndim = valsize(0)
  if ndim gt 2 then MESSAGE, 'adas_edtab; VALUE must be 1D or 2D only'
  if valsize(ndim+1) ne 7 then MESSAGE, 				$
  'adas_edtab; VALUE must be a string array'
  if units(0) eq -1 then begin 
    ncols = valsize(ndim-1)
    ysize = ysize - 2
  end else begin
    ncols = indsize(2)
  end
  nrows = valsize(ndim)

		;**** create titled base widget ****

  parent = widget_base(title=winlabel,XOFFSET=50,YOFFSET=50,/column)

		;**** add title across the top of the table ****

  rc = widget_label(parent,value=title,font=fonts.font_norm)

	;**** extract value sub-set if units in operation ****
        ;**** Now done in cw_edit_table *****

  if unitchk eq 1 then begin
      if units eq -1 then begin
          data = value
      endif else begin
          data = value(unitind(units,*),*)
      endelse
  endif else if (unitchk gt 1) then begin
      data = strarr(ncols,valsize(2))
      if units(0) eq -1 then begin
          data = value
      endif else begin
          for i = 0,ncols-1 do begin
              data(i,*) = value(unitind(units(i),i),*)
          endfor
      endelse
  endif

		;**** add the edit_table compound widget ****

  edid = cw_edit_table(	parent, NCOLS = ncols, NROWS = nrows, 		$
			COLEDIT = coledit, COLHEAD = colhead, 		$
			VALUE = data, /FLOAT, 				$
			UVALUE = uvalue, NOINDEX = noindex, 		$
			NOCON = nocon, YSIZE = ysize, CELLSIZE = cellsize, $
			NOEDIT = noedit, ROWSKIP = rowskip, 		$
			NUM_FORM = num_form,  FLTINT= fltint,		$
                        FONTS = fonts)

		;**** lower base for button controls ****

  base = widget_base(parent,/column)

		;**** add the unit switching buttons ****

  if unitchk eq 1 then begin
      if units ge 0 then begin
          rc = widget_label(base,value=unitstitle,font=fonts.font_norm)
          unitid = lonarr(nunits)
          unitbase = widget_base(base,/row,/frame,/nonexclusive)
          for i = 0, nunits-1 do begin
              unitid(i) = widget_button(unitbase, value=unitname(i),	$
	 	                        font=fonts.font_norm)
          endfor
          widget_control,unitid(units),set_button=1
      endif else begin
          unitid = long(0)
          unitbase = long(0)
          unitind = 0
      endelse
  endif else if (unitchk gt 1) then begin
      if units(0) ge 0 then begin
          unitid = lonarr(nunits,nunits)
          unitbase = lonarr(nunits)
          ntitles = n_elements(unitstitle)
          for j = 0, ntitles-1 do begin
              rc = widget_label(base,value=unitstitle(j),font=fonts.font_norm)
              unitbase(j) = widget_base(base,/row,/frame,/nonexclusive)
              for i = 0, nunits-1 do begin
		;**** Miss out those units named DUMMY at the end ****
		name = strmid(strupcase(strtrim(unitname(i,j),2)),0,5)
		if name ne 'DUMMY' and name ne 'HUGHS' then begin
		  unitid(j,i) = widget_button(unitbase(j),		$
                                   value=unitname(i,j), font=fonts.font_norm)
		endif
              endfor
              widget_control, unitid(j,units(j)), set_button=1
          endfor
      endif else begin
          unitid = long(0)
          unitbase = long(0)
          unitind = 0
      endelse
  endif

		;**** error/warning message ****

  messid = widget_label(base,font=fonts.font_norm,value=' ')

		;**** add the exit buttons ****

  butbase = widget_base(base,/row)

  rc = widget_button(butbase,value='Cancel',uvalue='Cancel', 		$
		     font=fonts.font_norm)
  rc = widget_button(butbase,value='Done',uvalue='Done',font=fonts.font_norm)
  
  dynlabel, parent
  widget_control, parent, /realize


		;**** create a state structure for the pop-up ****
		;**** window and save in"parent widget id     ****

  state = {	NCOLS:ncols, NROWS:nrows, COLEDIT:coledit, $
		EDID:edid, VALUE:value, UNITBASE:unitbase, $
		UNITS:units, UNITID:unitid, UNITIND:unitind, $
		MESSID:messid, EXITBUT:'', GAPS:gaps, $
		DIFFLEN:difflen, ORDER:order, LIMITS:limits }

		;**** make widget modal ****

  xmanager,'adas_edtab',parent,event_handler='adasedtab_event',/modal,/just_reg
 
		;**** Copy the output values from common ****

  if state.exitbut eq 'Done' then begin
    value = state.value
    if units(0) ge 0 then begin
      units = state.units
    end
  end

END

