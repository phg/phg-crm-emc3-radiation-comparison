; Copyright (c) 2000 Strathclyde University .
; SCCS info: Module @(#)$Header: /home/adascvs/idl/adaslib/gui/adas_proc_null_events.pro,v 1.1 2004/07/06 11:12:13 whitefor Exp $    Date $Date: 2004/07/06 11:12:13 $
;+           
;
; Version: 
;		1.1	Martin O'Mullane
;
; Date:
;		04-04-2000
;
;-----------------------------------------------------------------------
FUNCTION ADAS_PROC_NULL_EVENTS, event

   ; The purpose of this event handler is to do nothing
   ;and ignore all events that come to it.
   
   ; need a function for cw_bgroup.
   
   RETURN,0

END 
