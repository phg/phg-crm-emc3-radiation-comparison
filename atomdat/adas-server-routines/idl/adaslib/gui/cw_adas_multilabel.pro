; PROJECT:
;       ADAS
;
; NAME:
;       CW_ADAS_MULTILABEL()
;
; PURPOSE:
;       Produces a multi line text widget.
;
; EXPLANATION:
;       This widget generates a vertical set of label
;       widgets to store a single text string in. It calls
;       adas_string_justify to split the single string
;       into multiple lines then displays each line on
;       
;       This widget does not generate any events.
;
;       After creation, the text in the widgets can
;       be updated by calling set_value via widget
;       control on the main widget ID returned by
;       this function.
;
; USE:
;       An example of how to use the widget:
;          myval = "This is a scalar string with no carriage "+$
;                  "returns present at all, by the time it "  +$
;                  "ends up in the widget it will be split "  +$
;                  "across four lines"
;          container=widget_base()
;          multi=cw_adas_multilabel(container,lines=4,cols=40)
;          widget_control,container,/realize
;          widget_control,multi,set_value=myval
;
; INPUTS:
;       PARENT   - The ID of the parent widget.
;
; OPTIONAL INPUTS:
;       None
;
; OUTPUTS:
;       The return value of this function is the ID of the compound
;       widget which is of type LONG integer.
;
; OPTIONAL OUTPUTS:
;       None
;
; KEYWORD PARAMETERS:
;       VALUE   - An initial value for the widget to have
;       FONT    - A font to use for all labels in this widget.
;       COLS    - Number of columns to use (default 40)
;       ROWS    - Number of rows to use (default 4)
;       FRAME   - Draw a frame around the base widget
;
; CALLS:
;       ADAS_STRING_JUSTIFY     Split a scalar text string into a
;                               string array
;
;       See side effects for other related widget management routines.
;
; SIDE EFFECTS:
;       Three other routines are included which are used to manage the
;       widget;
;
;       CW_ADAS_MULTILABEL_SV
;
; CATEGORY:
;       Compound Widget
;
; WRITTEN:
;       Allan Whiteford
;
; MODIFIED:
;
;       1.1     Allan Whiteford
;               Initial release
;
;       1.2     Allan Whiteford
;               More sensible initial values
;
; VERSION:
;       1.1     15-Aug-2008
;       1.1     24-Nov-2008
;-
;-----------------------------------------------------------------------------

pro cw_adas_multilabel_sv,base,val
        widget_control,base,get_uvalue=labels
        widget_control,labels[0],get_uvalue=cols
        text=adas_string_justify(val,cols)

        for i=0,n_elements(labels)-1 do begin
                if i lt n_elements(text) then $
                        widget_control,labels[i],set_value=text[i] $
                else $
                        widget_control,labels[i],set_value=''
        end
end

function cw_adas_multilabel,parent,value=value, $
                                   font=font,   $
                                   lines=lines, $
                                   frame=frame, $
                                   cols=cols

        if n_elements(cols) eq 0 then cols=50
        if n_elements(lines) eq 0 then begin
        	if n_elements(value) gt 0 then lines=n_elements(adas_string_justify(value,cols)) else lines=4
        endif

        labels=lonarr(lines)
        base=widget_base(parent,/column,frame=frame)
        for i=0,lines-1 do labels[i]=widget_label(base,font=font,value=string(bytarr(cols)+(byte(' '))[0]),/dynamic_resize,/align_left)

        widget_control,labels[0],set_uvalue=cols
        widget_control,base,set_uvalue=labels,pro_set_value='cw_adas_multilabel_sv'
        if n_elements(value) gt 0 then widget_control,base,set_value=value

        return,base
end
