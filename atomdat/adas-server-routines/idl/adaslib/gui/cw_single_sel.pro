; Copyright (c) 1995, Strathclyde University .
; SCCS info: Module @(#)$Header: /home/adascvs/idl/adaslib/gui/cw_single_sel.pro,v 1.10 2004/07/06 13:09:37 whitefor Exp $ Date $Date: 2004/07/06 13:09:37 $
;
;+
; PROJECT:
;       ADAS IBM MVS to   UNIX conversion
;
; NAME:
;	CW_SINGLE_SEL()
;
; PURPOSE:
;	A selection list allowing a single selection.
;
; EXPLANATION:
;	The compound widget goes one step further than the basic IDL
;	list widget.  The difficulty with a basic list widget is that
;	you cannot 'set' the highlighted selection from the code.  This
;	widget consists of a list widget with a single line text widget
;	above it.  As the user selects an item from the list it appears
;	in the single line text widget.  This means that when the widget
;	is created a 'default' selection can be in place.  Also SET_VALUE
;	can be used to reset the selected item.
;
;	The 'value' of this widget is the index of the selected item.  A
;	value of -1 indicates no selection.
;
;	The widget also includes a title above the list.
;
;	This widget generates events as items are selected.  The event
;	structure is;
;		{ID:0L, TOP:0L, HANDLER:0L, INDEX:0}
;	where INDEX is the index of the item selected from the list.
;
; USE:
;	An example of how to use this widget;
;
;		list = ['Red','Green','Blue','Black','Yellow','Orange']
;		base = widget_base()
;		selid = cw_single_sel(base,list,value=2)
;		widget_control,base,/realize
;		ev=widget_event()
;		print,list(ev.index),' was selected'
;
;	You could use;
;		widget_control,selid,get_value=index 
;	to recover the users selection.
;
; INPUTS:
;       PARENT	- The ID of the parent widget.
;
;	LIST	- A 1D string array; The list for selection.
;
;
; OPTIONAL INPUTS:
;	None.
;
; OUTPUTS:
;       The return value of this function is the ID of the compound
;       widget which is of type LONG integer.
;
; OPTIONAL OUTPUTS:
;       None
;
; KEYWORD PARAMETERS:
;	VALUE	- Integer; the index of the selected item in LIST.
;		  An initial value of -1 indicates no selection.
;		  Default value 0, i.e list(0) is the selected item.
;
;	TITLE	- String or string array; a title for the widget.
;		  A sting array gives a multi-line title.
;
;	YSIZE	- The height of the list widget in rows. Default 4.
;		  Scroll bars will appear for lists longer than ysize.
;
;       FONT    - A font to use for all text in this widget.
;
;	BIG_FONT - A font to be used for titles and column headers.
;
;	UVALUE	- A user value for this widget.
;
;  	COLTITLES - A string array containing the titles for each
;		    column.  Can be more than one line.
; CALLS:
;       CW_LOADSTATE    Recover compound widget state.
;       CW_SAVESTATE    Save compound widget state.
;       See side effects for other related widget management routines.
;
; SIDE EFFECTS:
;	This widget uses a COMMON BLOCK: CW_SING_SEL_BLK to hold the
;       widget state.
;
;       Three other routines are included which are used to manage the
;       widget;
;
;	SSEL_SET_VAL
;	SSEL_GET_VAL()
;	SSEL_EVENT()
;
; CATEGORY:
;	Compound Widget
;
; WRITTEN:
;       Andrew Bowen, Tessella Support Services plc, 6-May-1995
;
; MODIFIED:
;       1       Andrew Bowen   
;               First release.
;	1.1     Lalit Jalota    
;		Added two keywwords, big_font, coltitles
;	1.9	Tim Hammond
;		Modified default value of big_font so it = font - thus
;		avoiding possible very large disparity between the 2
;		if only one is specified.
;	1.10	Richard Martin
;		Removed obsolete cw_loadstate/savestate statements.
;
; VERSION:
;       1       09-06-95
;       1.1    	10-06-95
;	1.9	02-08-96
;	1.10	09-01-02
;
;-
;-----------------------------------------------------------------------------

PRO ssel_set_val, id, value


		;**** Return to caller on error ****
  ON_ERROR, 2

		;**** Retrieve the state ****

    first_child = widget_info(id,/child)
    widget_control, first_child, get_uvalue=state, /no_copy

		;**** Check selection in bounds ****
  if value ge 0 and value lt state.numitem then begin
    widget_control,state.selid,set_value=state.list(value)
  end else begin
    value = -1
  end
		;******************************
		;**** Refresh list display ****
		;**** if valid selection   ****
		;******************************
  if (value ne -1 ) then begin 
     widget_control,state.listid,set_value=state.list
  endif else begin
     message, "Invalid selection"
  endelse

		;**** Copy value to state ****
  state.select = value

		;**** Save the new state ****

    widget_control, first_child, set_uvalue=state, /no_copy
    
END

;-----------------------------------------------------------------------------


FUNCTION ssel_get_val, id


		;**** Return to caller on error ****
  ON_ERROR, 2

		;**** Retrieve the state ****

    first_child = widget_info(id,/child)
    widget_control, first_child, get_uvalue=state
    
		;**** Return number of current selection ****
  RETURN, state.select

END

;-----------------------------------------------------------------------------

FUNCTION ssel_event, event

		;**** Base ID of compound widget ****
  base=event.handler

		;**** Retrieve the state ****

    first_child = widget_info(base,/child)
    widget_control, first_child, get_uvalue=state, /no_copy

		;************************
		;**** Process Events ****
		;************************
  CASE event.id OF

    state.listid: begin
		      state.select = event.index
		      widget_control,state.selid, $
			  set_value=state.list(state.select)
    		  end

    ELSE:

  ENDCASE

  select=state.select
		;**** Save the new state structure ****

    widget_control, first_child, set_uvalue=state, /no_copy

  RETURN, {ID:base, TOP:event.top, HANDLER:0L, INDEX:select}
END

;-----------------------------------------------------------------------------

FUNCTION cw_single_sel, topparent, list, VALUE=value, COLTITLES=coltitles, $
			TITLE=title, YSIZE=ysize, FONT=font, $
			BIG_FONT=big_font, UVALUE=uvalue


  IF (N_PARAMS() LT 1) THEN MESSAGE, 'Must specify PARENT for cw_adas_ranges'

		;**** Set defaults for keywords ****
  IF NOT (KEYWORD_SET(value)) THEN value = 0 
  IF NOT (KEYWORD_SET(title)) THEN title = 'Select Item'
  IF NOT (KEYWORD_SET(ysize)) THEN ysize = 4
  IF NOT (KEYWORD_SET(font)) THEN font = ''
  IF NOT (KEYWORD_SET(big_font))  THEN big_font = font
  IF NOT (KEYWORD_SET(uvalue))  THEN uvalue = 0

  parent = WIDGET_BASE(topparent, UVALUE = uvalue, $
		EVENT_FUNC = "ssel_event", $
		FUNC_GET_VALUE = "ssel_get_val", $
		PRO_SET_VALUE = "ssel_set_val", $
		/COLUMN)

		;**** Create the main base for the widget ****

  first_child = widget_base(parent)

  cwid = widget_base(first_child,/column)

		;**** Find number of items in list ****
  numitem = size(list)
  if numitem(0) gt 0 then begin
    numitem = numitem(1)
  end else begin
    numitem = 1
  end

		;**** Find the maximum length of text in list ****
  maxlen = max(strlen(list))

		;**** Find number of lines in title ****
  numtitle = size(title)
  if numtitle(0) gt 0 then begin
    numtitle = numtitle(1)
  end else begin
    numtitle = 1
  end

		;**** Title for list ****
  for i = 0 , numtitle-1 do begin
    rc = widget_label(parent,value=title(i),font=big_font)
  end

		;**** Column titles for list ****
  if keyword_set(coltitles) then begin
     numcoltitle = n_elements(coltitles)
     colid = widget_list(parent,value=coltitles, $
			    ysize=numcoltitle, font=font $
		        )
  endif

		;**** The selected item ****
  selid = widget_text(parent,xsize=maxlen,font=font)

		;**** The selection list ****
  ysize = ysize < numitem
  listid = widget_list(parent,value=list,ysize=ysize,font=font)

		;**** Set initial selection if any ***
  if value ge 0 and value lt numitem then begin
    widget_control,selid,set_value=list(value)
  end else begin
    value = -1
  end

		;**** Create state structure ****
  new_state = { SELID:selid, LISTID:listid, LIST:list, $
		NUMITEM:numitem, SELECT:value }

		;**** Save initial state structure ****

    widget_control, first_child, set_uvalue=new_state, /no_copy

  RETURN, parent

END
