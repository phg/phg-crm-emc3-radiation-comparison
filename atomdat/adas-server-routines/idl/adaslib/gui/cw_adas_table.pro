; Copyright (c) 1995, Strathclyde University .
; SCCS info: Module @(#)$Header: /home/adascvs/idl/adaslib/gui/cw_adas_table.pro,v 1.23 2004/07/06 13:03:36 whitefor Exp $	Date $Date: 2004/07/06 13:03:36 $
;
;+
; PROJECT:
;       ADAS IBM MVS to   UNIX conversion
;
; NAME:
;	CW_ADAS_TABLE()
;
; PURPOSE:
;	Widget for previewing a data table and invoking the table editor.
;
; EXPLANATION:
;	This compound widget provides a single entry point to the ADAS
;	table editing features.  The widget consists of a non-editable
;	text widget in which the information from the table is displayed
;	and a button which may be pressed to invoke the table editor for
;	that data.  This widget is intended to be incorporated in an
;	ADAS processing options widget along with other tables and
;	selection lists etc.
;
;	The event handler for the CW_ADAS_TABLE compound widget
;	calls the routine ADAS_EDTAB which produces the ADAS table
;	editor in a pop-up window.  See adas_edtab.pro for details
;	of how the table editor works.
;
;	Note: The VALUE which is supplied to this widget for creation via
;	the VALUE keyword is a string array representation of the table.
;	By contrast the value which is returned by this widget using
;	GET_VALUE and set using SET_VALUE is a structure consisting
;	of the data in the table and the units, if any, which apply to that
;	data. {VALUE:value, UNITS:units}.  A units value of -1 indicates
;	that units do not apply.
;
; USE:
;	The following code produces a table widget.
;	The data is in 4 columns of 5 rows and has the inital value of
;	100.0 for all elements.  There are no units specified for the
;	numeric data.  The table has column headers.  Column 4 of the
;	table is not editable.  Fonts have been specified to distinguish
;	between the editable and non-editable columns.
;
;	This code produces a pop-up window with a table widget in it.
;	Press the 'Edit Table' button to activate the table editor.
;
;	data = make_array(4,5,/float,value=100.0)
;	value = strtrim(string(data),2)
;	coledit=[1,1,1,0]
;	colhead=['Col 1','Col 2','Col 3','Col 4']
;	fonts={font_norm:'courier_bold14',font_input:'courier14'}
;
;	base = widget_base(/column)
;	cwid = cw_adas_table( base, value, $
;                	COLEDIT = coledit, COLHEAD = colhead, $
;                	TITLE = title, FONTS = fonts)
;	rc=widget_button(base,value='Done')
;	widget_control,base,/realize
;	rc=widget_event()
;
;	widget_control,cwid,get_value=value
;	data = float(value.value)
;	print,data
;
;
;	See cw_adas205_proc.pro for more involved examples.
;
; INPUTS:
;       PARENT   - The ID of the parent widget.
;
;       VALUE    - Array of initial data values, type STRARR(k,nrows).
;		   For a table which has no UNITS defined then k is the
;		   number of columns in the table.  Where units are
;		   defined VALUE has extra columns of data which give
;		   table values in various different units.
;                  Each string value should represent a valid floating
;                  point number.  Elements of the string array may contain
;                  empty strings.
;
; OPTIONAL INPUTS:
;       (Note: either ALL or NONE of these optional inputs should be
;              provided by the caller.  If none are supplied the units
;	       switching facilities are ignored.)
;
;       UNITS    - Index number of the current units in the arrays that
;                  follow.
;
;       UNITNAME - 1D array of text strings giving the names of the
;                  units available.
;
;       UNITIND  - 2D array of column indicies which make up the table
;                  for the available units.  For example a 4 column
;                  table switchable between 3 possible units would have
;                  dimensions UNITIND(3,4).  A value of;
;                    [[0,0,0],[1,1,1],[2,3,4],[5,6,7]]
;                  would refer to the columns from a VALUE array with
;                  dimensions VALUE(8,nrows).  Columns 0 and 1 in the
;                  table always come from columns 0 and 1 in the VALUE
;                  array but column 2 in the table is taken from columns
;                  2, 3 and 4 in VALUE as the units are changed between
;                  0, 1 and 2.
;
; OUTPUTS:
;       The return value of this function is the ID of the compound
;       widget which is of type LONG integer.
;
; OPTIONAL OUTPUTS:
;	None
;
; KEYWORD PARAMETERS:
;	XTEXSIZE - X-Size in characters of the text widget for the table.
;		   The defualt is for the text widget to be automatically
;		   sized to fit the whole table.
;
;	YTEXSIZE - Y-Size in characters of the text widget for the table.
;                  The defualt is for the text widget to be automatically
;                  sized to fit the whole table.
;
;	SCROLL	 - Set this keyword to put scroll bars on the table
;		   display text widget.  This is useful if you explicitly
;		   size the widget with XTEXSIZE and YTEXSIZE.  By default
;		   scroll bars do not appear.
;
;	UVALUE	 - A user value for this compound widget.
;
;       UNITSTITLE - A title to place above the units box.
;
;	UNITS_PLUS - Optional user string to be placed after units label
;
;       COLEDIT  - Flags to indicate which columns are editable, type
;                  INTARR(NCOLS).  The default is all columns are editable.
;                  A value of [1,0,1] would cause the first and third
;                  columns to be editable.
;
;       COLHEAD  - Column headers, type STRARR(NCOLS,n).  Each of the n
;                  rows of header is placed one below another at the top
;                  of each column.
;
;       NOINDEX  - Set this keyword to remove the column of integer index
;                  values at the far left of the table.  The default is
;                  for the index column to appear.
;
;       TITLE    - The title for the edit table.  A string array with
;                  one element for each line of the title.
;
;       NOCON    - Set this keyword to remove the edit control buttons
;                  delete, remove, insert etc.
;
;       YTABSIZE - Specifies the ysize of the table in rows of cells.
;                  If ysize is less than the number of rows the columns
;                  are split and placed side-by-side.
;
;	GAPS	 - Set this keyword to allow gaps in the columns of data.
;		   By default this routine will check that there are no
;		   embedded blank cells in a column of data (blank
;		   values are allowed at the end of a column)
;
;	DIFFLEN	 - Set this keyword to allow the columns of data in the
;		   table to be of differing lengths when completed by
;		   the user.  By default this routine will check that
;		   the data entered by the user in each column has the
;		   same number of values.  Blanks at the ends of the
;		   table columns are allowed.
;
;	ORDER	 - Specifies that the values in an editable column must
;		   be in ascending or descending numerical order, type
;		   intarr(NCOLS).  A value of [1,1,0,-1] would indicate
;		   that columns 1 and 2 should be in ascending order and
;		   column4 should be in descending order.  No checking
;		   would be performed on column 3. If you specify
;		   checking on a non-editable column that checking is
;		   ignored.  By default no order checking is performed.
;
;       LIMITS   - Specifies the boundaries within the values in the
;                  table columns must lie, type intarr(NCOLS).  A value
;                  of [1,2,0,-2,-1] for a 5 column table would indicate
;                  that column 1 values should be >=0, column 2 values
;                  should be >0, column 3 values are not checked, column
;                  4 values should be <0 and column 5 values <=0.
;                  By default no limits checking is performed.
;
;       CELLSIZE - Specifies the character width of the text widget
;                  cells which make up the table.  The default is 12.
;                  In some circumstances the windows toolkit may ignore
;                  this setting depending on the width of the column
;                  header.
;
;       NOEDIT   - Set this keyword to make all data fields non-editable.
;                  The default is for all data columns specified by COLEDIT
;                  to be editable.
;
;       ROWSKIP  - By default the cursor skips down columns of cells
;                  when the return key is pressed.  Set this keyword to
;                  cause the cursor to skip across rows in the table.
;
;	NUM_FORM - This keyword defines format of numbers in table
;
;       FLTINT   - An array of different formats of numbers in table
;                  This can be used if different formats are required
;                  for different columns.
;
;       FONTS    - Structure of two fonts to use for table to distinguish
;                  between non-editable (norm) and editable (input) cells;
;                  {font_norm:<string>, font_input:<string>}
;
;	FONT_LARGE - A large font for the widget title and button.
;
;	FONT_SMALL - A small font for the text widget display of the table.
;
;       EFUN       - A user supplied event handler.
;
;
; CALLS:
;	ADAS_EDTAB	Pop-up table editor window.
;	TEXT_TABLE()	Produces a text version of a data table.
;       See side effects for other related widget management routines.
;
; SIDE EFFECTS:
;
;       Three other routines are included which are used to manage the
;       widget;
;
;	ADASTAB_SET_VAL
;	ADASTAB_GET_VAL()
;	ADASTAB_EVENT()
;
; CATEGORY:
;	Compound Widget
;
; WRITTEN:
;       Andrew Bowen, Tessella Support Services plc, 5-Apr-1993
;
; MODIFIED:
;       1.1   	Andrew Bowen    
;               First release.
;	1.2	Andrew Bowen
;               Various improvements and fixes.
;	1.3	Andrew Bowen
;               Modified to use large and small fonts in combination.
;	1.4	Andrew Bowen
;               Extra table checking added, gaps, order and limits.
;	1.5	Andrew Bowen
;               More table checking added.
;	1.6	Andrew Bowen
;               Modifications for a default numeric format.
;	1.7	Andrew Bowen
;               Header comments added.
;	1.8	Lalit Jalota
;		Modified display of units to be in separate widget
;		so that they are visible even with scrolling lists 
;		and titles are above table too.
; 	1.9     Lalit Jalota
;		Added units_plus keyword.
;	1.10	Lalit Jalota
;	1.11	Lalit Jalota
;       1.12	Lalit Jalota
;       	Included checks to see if units are requested.
;	1.13	Lalit Jalota
;		Corrected definition of ysize used if not explicitly
; 		set via keyword ytexsize.  Removed calls to 
;		CW_LOAD_STATE and CW_SAVE_STATE
;	1.14	Lalit Jalota
;	1.15	Tim Hammond	
;		Corrected error which missed off the bottom line
;		of an edited table which had no units data
;		associated with it.
;	1.16	Tim Hammond
;		Added FLTINT keyword.
;	1.17	David Brooks	
;		Altered tests on units to take account of the fact
;               that units is now an array e.g 'if units eq -1' becomes
;               'if units(0) eq -1' etc.
;	1.18	Tim Hammond
;		Altered set_val line so that index is table_rows-1
;		rather than table_rows-2 so all table values are
;		included when table value is set.
;	1.19	Tim Hammond
;		Further corrections in line with 1.18 to ensure that if
;		a units title is present then table_rows-2 is taken rather
;		than table_rows-1 as otherwise the units title is included
;		in the table when a 'Clear Table' button is used.
;	1.20 	William Osborn
;		Added code in set_val to make a change in units visible
;	1.21 	William Osborn
;		Corrected set_val so that unitstitle being an array does
;		not cause errors
;	1.22 	Martin O'Mullane
;           Added user supplied event function.
;	1.23	Richard Martin
;		IDL 5.5 fixes.
;
; VERSION:
;	1.1	06-04-93
;	1.2	06-04-93
;	1.3	14-04-93
;	1.4	19-04-93
;	1.5	20-04-93
;	1.6	22-04-93
;	1.7	02-06-93
;	1.8	21-02-95
;	1.9	10-03-95
;	1.10	13-03-95
;	1.11	13-03-95
;	1.12	16-03-95
;	1.13	24-03-95
;	1.14	27-03-95
;	1.15	19-06-95
;	1.16	20-06-95
;	1.17	11-07-95
;	1.18	05-01-96
;	1.19	05-03-96
;	1.20	09-05-96
;	1.21	28-06-96
;	1.22	17-02-00
;	1.23	30-01-02
;-
;-----------------------------------------------------------------------------

PRO adastab_set_val, id, value


		;**** Return to caller on error ****
  ON_ERROR, 2

		;**** Retrieve the state ****
  
  first_child = widget_info(id,/child)
  widget_control, first_child, get_uvalue=state, /no_copy

		;**** The table ****
  table = text_table(value.value, value.units, state.unitname, state.unitind, $
                        UNITSTITLE = state.unitstitle, $
			COLHEAD = state.colhead, $
                        NOINDEX = state.noindex, MINCSIZE = state.cellsize-2)

		;**** Copy new table to the text widget ****
  		;**** get size of table first ****
  table_rows = n_elements(table)
	;**** If there are no unitstitles then use 0:table_rows-1
  w = where(strcompress(state.unitstitle, /remove_all) ne '')
  if w(0) eq -1 then begin
      widget_control,state.tabid,set_value=table(0:table_rows-1)
  endif else begin
      widget_control,state.tabid,set_value=table(0:table_rows-2)
  endelse

		;**** Change the units too ****
  if (state.unitid ne -1 ) then begin
     units_label = (strcompress(table(table_rows-1))) + $
			   state.units_plus
     widget_control, state.unitid, set_value=units_label
  endif

		;**** Copy the new value and units to state structure ****
 
  state.value = value.value
  state.units = value.units
  widget_control, first_child, set_uvalue=state, /no_copy

END

;-----------------------------------------------------------------------------


FUNCTION adastab_get_val, id


		;**** Return to caller on error ****
  ON_ERROR, 2

		;**** Retrieve the state ****
                
  first_child = widget_info(id, /child)
  widget_control, first_child, get_uvalue=state


  value = state.value
  units = state.units


  RETURN, {VALUE:value, UNITS:units}

END

;-----------------------------------------------------------------------------

FUNCTION adastab_event, event

		;**** Base ID of compound widget ****
  base=event.handler

		;**** Retrieve the state ****

  first_child = widget_info(base, /child)
  widget_control, first_child, get_uvalue = state, /no_copy


		;************************
		;**** Process Events ****
		;************************
  CASE event.id OF

		;*****************************
		;**** Invoke table editor ****
		;*****************************
    state.editid: begin

      action = 'edit_table'
      value = state.value
      units = state.units
		;*** next line ensures order is saved as an array ***
      adas_edtab,       value, units, state.unitname, state.unitind, 	$
			UNITSTITLE = state.unitstitle, 			$
			COLEDIT = [state.coledit], COLHEAD = state.colhead, $
			NOINDEX = state.noindex, TITLE = state.title[0], 	$
			NOCON = state.nocon, YSIZE = state.ytabsize, 	$
			GAPS = state.gaps, DIFFLEN = state.difflen, 	$
			ORDER = [state.order], LIMITS = state.limits, 	$
			CELLSIZE = state.cellsize, 			$
			NOEDIT = state.noedit, ROWSKIP = state.rowskip, $
			NUM_FORM=state.num_form, FONTS = state.fonts, 	$
                        FLTINT = state.fltint,          		$
			WINLABEL = 'ADAS Table Editor'

		;**** Form new text table ****
      table = text_table(value, units, $
			state.unitname, state.unitind, $
                        UNITSTITLE = state.unitstitle, $
			COLHEAD = state.colhead, $
                        NOINDEX = state.noindex, MINCSIZE=state.cellsize-2)

       		;**** get number of rows in table ****
      tablesize = n_elements(table)

		;**** Copy new table to the text widget ****
		;**** except for units                  ****
      if (state.unitid gt 0) then begin
          tablelimit = tablesize - 2
      endif else begin
          tablelimit = tablesize - 1
      endelse
      widget_control,state.tabid,set_value=table(0:tablelimit)

		;**** Copy new units to unit display widget ****
		;**** if units exist  			    ****

      if (state.unitid gt 0) then begin
         units_label = strcompress(table(tablesize-1))
         len_label = strlen(units_label)
         blanks = '                                                       '
         space = 25 - len_label
         units_label= units_label + strmid(blanks,0,space) + $
			   state.units_plus
         widget_control, state.unitid, set_value=units_label
      endif

		;**** Copy the new value to state structure ****
      state.value = value
      state.units = units


    end

  ENDCASE
  

  efun = state.efun
  widget_control, first_child, set_uvalue=state, /no_copy

; Return a struct or pass to a user supplied event handler?
  
  ret = {ID:base, TOP:event.top, HANDLER:0L, ACTION:action}
  if efun NE '' then return, call_function(efun,ret) else return, ret
  
END

;-----------------------------------------------------------------------------

FUNCTION cw_adas_table, parent, value, units, unitname, unitind,         $
			XTEXSIZE = xtexsize, YTEXSIZE = ytexsize,        $
			SCROLL = scroll, UVALUE = uvalue, 	         $
                	UNITSTITLE = unitstitle, UNITS_PLUS=units_plus,  $
                	COLEDIT = coledit, COLHEAD = colhead, 	         $
                	NOINDEX = noindex, TITLE = title, 	         $
                	NOCON = nocon, YTABSIZE = ytabsize, 	         $
			GAPS = gaps, DIFFLEN = difflen, ORDER = order,   $
			LIMITS = limits, CELLSIZE = cellsize, 	         $
                	NOEDIT = noedit, ROWSKIP = rowskip, 	         $
			NUM_FORM = num_form, FONTS = fonts, 	         $
                        FLTINT = fltint,                                 $
			FONT_LARGE = font_large, FONT_SMALL = font_small,$
                        EVENT_FUNCT = efun

  COMMON cw_adastab_blk, state_base, state_stash, state

  IF (N_PARAMS() LT 2) THEN MESSAGE, 'Must specify PARENT and VALUE'+ $
						' for cw_adas_table'
  IF (N_PARAMS() GT 2 and N_PARAMS() NE 5) THEN MESSAGE, $
        'UNITS, UNITNAME, and UNITIND go together for cw_adas_table'
  ON_ERROR, 2                                   ;return to caller

		;**** Set dummy values for units if not given ****
  IF (N_PARAMS() LE 2) THEN begin
    units = -1
    unitname = ''
    unitind = 0
  end
 
		;**** Determine the size of the table ****
  valsize = size(value)
  ndim = valsize(0)
  if units(0) eq -1 then begin
    ncols = valsize(ndim-1)
    nrows = valsize(ndim)
  end else begin
    indsize = size(unitind)
    ncols = indsize(2)
    nrows = valsize(ndim)
  end

		;**** Set defaults for keywords ****
  IF NOT (KEYWORD_SET(title)) THEN title = ' '
  IF NOT (KEYWORD_SET(scroll)) THEN scroll = 0
  IF NOT (KEYWORD_SET(uvalue)) THEN uvalue = 0
  IF NOT (KEYWORD_SET(unitstitle)) THEN unitstitle = ' '
  IF NOT (KEYWORD_SET(units_plus)) THEN units_plus = ' ' 
  IF NOT (KEYWORD_SET(coledit)) THEN coledit = make_array(ncols,/int,value=1)
  IF NOT (KEYWORD_SET(colhead)) THEN $
				colhead = make_array(ncols,/string,value='')
  IF NOT (KEYWORD_SET(noindex)) THEN noindex = 0
  IF NOT (KEYWORD_SET(nocon)) THEN nocon = 0
  IF NOT (KEYWORD_SET(ytabsize)) THEN ytabsize = nrows
  IF NOT (KEYWORD_SET(gaps)) THEN gaps = 0
  IF NOT (KEYWORD_SET(difflen)) THEN difflen = 0
  IF NOT (KEYWORD_SET(order)) THEN order = 0
  IF NOT (KEYWORD_SET(limits)) THEN limits = 0
  IF NOT (KEYWORD_SET(cellsize)) THEN cellsize = 12
  IF NOT (KEYWORD_SET(noedit)) THEN  noedit = 0
  IF NOT (KEYWORD_SET(rowskip)) THEN rowskip = 0
  IF NOT (KEYWORD_SET(num_form)) then num_form = '(E10.3)'
  IF NOT (KEYWORD_SET(fltint)) then fltint = make_array(ncols,/string,$
                                                value=num_form)
  IF NOT (KEYWORD_SET(fonts)) THEN begin
		tab_fnt = {tab_fnt,font_norm:'',font_input:''}
	end else begin
		tab_fnt = {tab_fnt,font_norm:fonts.font_norm, $
				   font_input:fonts.font_input}
	end
  IF NOT (KEYWORD_SET(font_large)) THEN font_large = ''
  IF NOT (KEYWORD_SET(font_small)) THEN font_small = ''


		;**** Create the main base for the widget ****
  main = WIDGET_BASE(parent, UVALUE = uvalue, $
		EVENT_FUNC = "adastab_event", $
		FUNC_GET_VALUE = "adastab_get_val", $
		PRO_SET_VALUE = "adastab_set_val", $
		/COLUMN)


                ;******************************************************
                ;**** Create a dummy widget just to hold value of *****
                ;**** "state" variable so as not to get confused  *****
                ;**** with any other values. Adopt IDL practice   *****
                ;**** of using first child widget                 *****
                ;******************************************************

  first_child = widget_base(main)
  
  cwid = widget_base(first_child,/column)

		;**** Title ****
  numtitle_lines = n_elements(title)
  if (numtitle_lines gt 1 ) then begin 
     for i = 0, numtitle_lines-1 do begin
        rc = widget_label(cwid,value=title(i),font=font_large)
     endfor
  endif else begin
     rc = widget_label(cwid,value=title(0),font=font_large)
  endelse


		;**** The table ****
  table = text_table(value, units, unitname, unitind, $
                        UNITSTITLE = unitstitle, COLHEAD = colhead, $
                        NOINDEX = noindex, MINCSIZE = cellsize-2)
  tablesize = size(table)
  ntablerows = n_elements(table)

  IF NOT (KEYWORD_SET(xtexsize)) THEN xtexsize = max(strlen(table))
  IF NOT (KEYWORD_SET(ytexsize)) THEN ytexsize = tablesize(2)
  if (units(0) eq -1) then begin
     tabid = widget_text(cwid,value=table(0:ntablerows-1),$ 
			   XSIZE=xtexsize,YSIZE=ytexsize, $
			   SCROLL=scroll,font=font_small)
  endif else begin
     tabid = widget_text(cwid,value=table(0:ntablerows-2),$ 
			   XSIZE=xtexsize,YSIZE=ytexsize, $
			   SCROLL=scroll,font=font_small)
  endelse
     


  if (units(0) ne -1 ) then begin
		;*** A separate widget displaying units ****
                ;*** Add the density units here as well ****
     units_label = (strcompress(table(ntablerows-1))) + $
			   units_plus
     unitid = widget_text(cwid, value=units_label,font=font_small) 
  endif else begin
     unitid = -1
  endelse
		;**** The edit button ****
  editid = widget_button(cwid,value='Edit Table',font=font_large)

		;**** Create state structure ****
  if n_elements(efun) le 0 then efun = ''
  new_state =  {EFUN:efun, TABID:tabid, EDITID:editid, UNITID:unitid,         $
                VALUE:value,                                                  $
		UNITS:units, UNITNAME:unitname, UNITIND:unitind, 	      $
		UNITSTITLE:unitstitle, COLEDIT:coledit, 		      $
		COLHEAD:colhead, NOINDEX:noindex, TITLE:title, NOCON:nocon,   $
		YTABSIZE:ytabsize, GAPS:gaps, DIFFLEN:difflen, 		      $
		ORDER:order, LIMITS:limits, CELLSIZE:cellsize, NOEDIT:noedit, $
		UNITS_PLUS:units_plus, 					      $
                FLTINT:fltint,                         			      $
		ROWSKIP:rowskip, NUM_FORM:num_form, FONTS:tab_fnt }

		;**** Save initial state structure ****
 widget_control, first_child,  set_uvalue=new_state, /no_copy


  RETURN, main

END
