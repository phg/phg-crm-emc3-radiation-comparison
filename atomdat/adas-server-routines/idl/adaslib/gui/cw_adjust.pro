; Copyright (c) 1999, Strathclyde University.
; SCCS info: Module @(#)$Header: /home/adascvs/idl/adaslib/gui/cw_adjust.pro,v 1.2 2004/07/06 13:03:51 whitefor Exp $ Date $Date: 2004/07/06 13:03:51 $
;+
; PROJECT:
;       ADAS 
;
; NAME:
;       CW_ADJUST
;
; PURPOSE:
;       TO BE DOCUMENTED
; MODIFIED:
;       1.1     Richard Martin
;               First release 
;	  1.2	    Richard Martin
;		    Set data.ldef=1 when axes modified.
;
; VERSION:
;       1.1     15-10-99
;	  1.2	    14-04-2000
;
;-
;--------------------------------------------------------------------
PRO cw_adjust_event,event

      COMMON adjust_block, data

	top_base = widget_info(event.top,/child)

	widget_control,top_base,get_uvalue=state

	done=0
	ranges=0
		
	CASE event.id OF
	
	  state.list1id: begin
	  	widget_control,state.list1id,get_value=temp
		if temp eq 0 then begin
	  		data.xlog=0
	  		data.ylog=0
	  	endif else if temp eq 1 then begin
	  		data.xlog=1
	  		data.ylog=0
	  	endif else if temp eq 2 then begin
	  		data.xlog=0
	  		data.ylog=1	  
	  	endif else if temp eq 3 then begin
	  		data.xlog=1
	  		data.ylog=1	 
	  	endif  				

            adasplot, data.x , data.y, data.itval, 	$
            	data.nplots, data.nmx, 	data.title, $
            	data.xtitle,  data.xunit,data.xunits,data.ytitle, data.strg,$
            	data.head1, data.head2a,data.head2b,$
	         	data.teva, data.din, data.ldef, 	$
	         	data.xmin, data.xmax, data.ymin, 	$
	         	data.ymax, data.lfsel, xlog=data.xlog, ylog=data.ylog , $
	         	nodata=state.nodata, multilabels=state.multilabels
	  end

	  state.list2id: begin

	  	widget_control,state.list2id,get_value=temp
	  	if temp eq 0 then begin
	  		data.x=data.x1 
	  		data.xtitle=data.xtitles(0)
	  		data.title(0)=data.maintitle(0)
			widget_control,state.list3ida,get_value=temp
			if temp eq 0 then data.xunit=0 $
				else data.xunit=1
	  		widget_control,state.tempbase,map=1
	  		widget_control,state.densbase,map=0
	  		data.xmin=data.xmin1
	  		data.xmax=data.xmax1
	  		widget_control,state.xminid,set_value=data.xmin	
	  		widget_control,state.xmaxid,set_value=data.xmax
	  		widget_control,state.yminid,set_value=data.ymin	
	  		widget_control,state.ymaxid,set_value=data.ymax	
	  		data.ldsel=0
	  	endif else begin 
	  		data.x=data.x2
	  		widget_control,state.tempbase,map=0
	  		widget_control,state.densbase,map=1
	  		widget_control,state.densbase,sensitive=state.dsens  	
	  		data.xtitle=data.xtitles(1)
	  		data.title(0)=data.maintitle(1)
			if state.dsens eq 1 then begin
			   if temp eq 0 then data.xunit=2 else data.xunit=3
			endif else begin
			   data.xunit=2
			endelse
	  		data.xmin=data.xmin2
	  		data.xmax=data.xmax2
	  		widget_control,state.xminid,set_value=data.xmin
	  		widget_control,state.xmaxid,set_value=data.xmax
	  		widget_control,state.yminid,set_value=data.ymin
	  		widget_control,state.ymaxid,set_value=data.ymax		     
	  		data.ldsel=1
	  	endelse 
   
            adasplot, data.x , data.y, data.itval, 	$
            	data.nplots, data.nmx, 	data.title, $
            	data.xtitle, data.xunit,data.xunits,			$
            	data.ytitle, data.strg,				$
            	data.head1, data.head2a,data.head2b,$
	         	data.teva, data.din, data.ldef, 	$
	         	data.xmin, data.xmax, data.ymin, 	$
	         	data.ymax, data.lfsel, xlog=data.xlog, ylog=data.ylog , $
	         	nodata=state.nodata, multilabels=state.multilabels
	         	
	  end	  	

	  state.list3ida: begin	  
	  	widget_control,state.list3ida,get_value=temp
	  	if temp eq 0 then begin
	  		if data.xunit eq 1 then begin
	  			data.x=data.x/11605.4
	  			data.teva=data.teva/11605.4
	  			data.xmax=data.xmax/11605.4
	  			data.xmin=data.xmin/11605.4
	  			data.x1=data.x
	  			data.xmin1=data.xmin
	  			data.xmax1=data.xmax
	  			widget_control,state.xminid,set_value=data.xmin
	  			widget_control,state.xmaxid,set_value=data.xmax
	  		endif
	  		index=strpos(data.head2b,strtrim(data.xunits(0,1),2))
	  		if index ne -1 then begin
	  			len1=strlen(data.xunits(0,1))
	  			len2=strlen(data.head2b)
	  		      dummy=strmid(data.head2b,0,index-1)+data.xunits(0,0)+$
	  		      	strmid(data.head2b,index-1+len1,len2)
	  			data.head2b=dummy
	  		endif	  		  				  		

	  		data.xunit=0
	  	endif else begin
	  		if data.xunit eq 0 then begin
	  			data.x=data.x*11605.4
	  			data.teva=data.teva*11605.4	  			
	  			data.xmax=data.xmax*11605.4
	  			data.xmin=data.xmin*11605.4
	  			data.x1=data.x	  			
	  			data.xmin1=data.xmin
	  			data.xmax1=data.xmax	  			
	  			widget_control,state.xminid,set_value=data.xmin
	  			widget_control,state.xmaxid,set_value=data.xmax
	  		endif
	  		index=strpos(data.head2b,strtrim(data.xunits(0,0),2))
	  		if index ne -1 then begin
	  			len1=strlen(data.xunits(0,0))
	  			len2=strlen(data.head2b)
	  		      dummy=strmid(data.head2b,0,index-1)+data.xunits(0,1)+$
	  		      	strmid(data.head2b,index-1+len1,len2)
	  			data.head2b=dummy
	  		endif	  		  				  			  			  	
	  		data.xunit=1
	  	endelse	  		
            adasplot, data.x , data.y, data.itval, 		$
            	data.nplots, data.nmx, 	data.title, 	$
            	data.xtitle, data.xunit,data.xunits,	$
            	data.ytitle, data.strg,				$
            	data.head1, data.head2a,data.head2b,	$
	         	data.teva, data.din, data.ldef, 		$
	         	data.xmin, data.xmax, data.ymin, 		$
	         	data.ymax, data.lfsel, xlog=data.xlog, ylog=data.ylog , $
	         	nodata=state.nodata, multilabels=state.multilabels
	  end
	  
	  state.xminid: begin
		ranges=1	
	  end

	  state.xmaxid: begin
	  	ranges=1	
	  end	  

	  state.yminid: begin
		ranges=1		
	  end

	  state.ymaxid: begin
	  	ranges=1	
	  end	  
	  
	  state.doneid: begin
			done=1
	  end	

	ELSE:	
      ENDCASE
      
	
      if ranges eq 1 then begin
      	widget_control,state.xminid,get_value=tmpxmin
      	widget_control,state.xmaxid,get_value=tmpxmax
       	widget_control,state.yminid,get_value=tmpymin
      	widget_control,state.ymaxid,get_value=tmpymax 

		data.xmin=float(strtrim(tmpxmin(0),2))
		data.xmax=float(strtrim(tmpxmax(0),2))
		data.ymin=float(strtrim(tmpymin(0),2))
		data.ymax=float(strtrim(tmpymax(0),2))
	
		data.ldef=1
	
		if data.fitflag eq 0 then begin
      		widget_control,state.list2id,get_value=temp	
			if temp eq 0 then begin
			  data.ldef1=1
			  data.xmin1=data.xmin
			  data.xmax1=data.xmax
			  data.ldef=data.ldef1
			endif else begin
			  data.ldef2=1
			  data.xmin2=data.xmin
			  data.xmax2=data.xmax 
			  data.ldef=data.ldef2
			endelse	
		endif
		      
		adasplot, data.x , data.y, data.itval,		$
			data.nplots, data.nmx,  data.title, 	$
			data.xtitle, data.xunit,data.xunits,	$
			data.ytitle, data.strg, 		$
			data.head1, data.head2a,data.head2b,	$
			data.teva, data.din, data.ldef,		$
			data.xmin, data.xmax, data.ymin,	$
	      		data.ymax, data.lfsel, xlog=data.xlog, ylog=data.ylog, $
	      		nodata=state.nodata, multilabels=state.multilabels
      endif

 	widget_control,top_base,set_uvalue=state     
      
      if done eq 1 then widget_control, event.top, /destroy
END
;--------------------------------------------------------------------
FUNCTION cw_adjust, data_in, tdsens=tdsens, dsens=dsens, ysens=ysens, nodata=nodata, $
		multilabels=multilabels, notopbase=notopbase, font=font

  COMMON adjust_block, data

  data=data_in

  if keyword_set(dsens) then dsens=0   
  if keyword_set(ysens) then ysens=0
  if NOT keyword_set(tdsens) then tdsens=0
  if NOT keyword_set(nodata) then nodata=0
  if NOT keyword_set(multilabels) then multilabels=0  
  if NOT keyword_set(notopbase) then notopbase=0  
  
  cwid = WIDGET_BASE(TITLE='Graph Adjust Widget', /COLUMN)

  topbase = widget_base(cwid,/column,/frame)
  label   = widget_label(topbase,value='')
  label   = widget_label(topbase,value=' Scales:',font=font,/align_left)
  label   = widget_label(topbase,value='')


  style = ['X Linear/Y Linear','X Log/Y Linear   ',	$
  	     'X Linear/Y Log   ','X Log/Y Log      '] 	     
  if data.xlog eq 0 AND data.ylog eq 0 then begin
  	listval=0
  endif else if data.xlog eq 1 AND data.ylog eq 0 then begin
  	listval=1
  endif else if data.xlog eq 0 AND data.ylog eq 1 then begin
  	listval=2 
  endif else if data.xlog eq 1 AND data.ylog eq 1 then begin
  	listval=3
  endif
  l1baseid = widget_base(topbase,/row)
  label =widget_label(l1baseid,value='   Axes: ',font=font)
  list1id   = cw_bselector(l1baseid,style,set_value=listval,font=font)
  
  xvariables=['Temperature','Density']
  l2baseid=widget_base(topbase,/row)
  label =widget_label(l2baseid,value=' X-axis: ',font=font)
  if data.fitflag eq 1 then dumval=data.ldsel else dumval=0
  list2id   = cw_bselector(l2baseid,xvariables,set_value=dumval,font=font) 
  if tdsens eq 1 then widget_control,l2baseid,sensitive=0


  xunitsbase=widget_base(topbase)
  tempbase  =widget_base(xunitsbase,/row)
  
  xunits=data.xunits(0,*)
  if data.xunit eq 0 then xunit=0
  if data.xunit eq 1 then xunit=1 
  if data.xunit eq 2 then xunit=0
  if data.xunit eq 3 then xunit=1 
  label = widget_label(tempbase,value='X-units: ',font=font)
  list3ida   = cw_bselector(tempbase,xunits,set_value=xunit,font=font) 
  
  densbase  =widget_base(xunitsbase,/row)
  xunits=data.xunits(1,*)
  label = widget_label(densbase,value='X-units: ',font=font)
  list3idb   = cw_bselector(densbase,xunits,font=font)  

  if data.fitflag eq 1 AND data.ldsel eq 1 then begin
  	widget_control, tempbase, map=0
  	widget_control, densbase, map=1
  	if dsens eq 0 then widget_control, densbase, sensitive=0	
  endif else begin
      widget_control, tempbase, map=1
  	widget_control, densbase, map=0
  endelse	

  
  yunits= data.yunits
  l4baseid = widget_base(topbase,/row)
  label = widget_label(l4baseid,value='Y-units: ',font=font)
  list4id   = cw_bselector(l4baseid,yunits,font=font)
  if ysens eq 0 then widget_control, l4baseid, sensitive=0
 
  if notopbase eq 1 then widget_control,topbase, sensitive=0	 
 
  botbase = widget_base(cwid,/column)
  rangebase=widget_base(botbase,/frame,/column)
  toprangebase = widget_base(rangebase,/row)
  xmin=10^(!x.crange(0))
  xmax=10^(!x.crange(1))
  if data.ldsel eq 0 then begin
  	xmin=data.xmin1
  	xmax=data.xmax1
  endif else begin
  	xmin=data.xmin2
  	xmax=data.xmax2
  endelse
  xminid = cw_field(toprangebase,value=xmin,title='xmin',/return_events,font=font)
  xmaxid = cw_field(toprangebase,value=xmax,title='xmax',/return_events,font=font) 
  botrangebase = widget_base(rangebase,/row)
  ymin=10^(!y.crange(0))
  ymax=10^(!y.crange(1))
  yminid = cw_field(botrangebase,value=ymin,title='ymin',/return_events,font=font)
  ymaxid = cw_field(botrangebase,value=ymax,title='ymax',/return_events,font=font)   
  
  doneid  = widget_button(botbase,value='Done',font=font)
  
  state   = { list1id: list1id, list2id: list2id, list3ida: list3ida, $
  			list3idb: list3idb,tempbase: tempbase, densbase: densbase, $
  			xminid: xminid, xmaxid: xmaxid, yminid: yminid, ymaxid: ymaxid, $
  			list4id: list4id, dsens:dsens, nodata: nodata, $
  			multilabels: multilabels, doneid: doneid }
  
  widget_control, topbase, set_uvalue= state    

  widget_control,cwid,/realize

  xmanager, 'cw_adjust', cwid, event_handler='cw_adjust_event',  $
              /modal,/just_reg             
  
;  widget_control, topbase, get_uvalue= state  

 baseid = { cwid: cwid, data:data }
 return,baseid
 
END
