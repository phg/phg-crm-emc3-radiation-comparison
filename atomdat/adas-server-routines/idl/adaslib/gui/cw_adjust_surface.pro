; Copyright (c) 2002, Strathclyde University.
; SCCS info: Module @(#)$Header: /home/adascvs/idl/adaslib/gui/cw_adjust_surface.pro,v 1.1 2004/07/06 13:03:54 whitefor Exp $ Date $Date: 2004/07/06 13:03:54 $
;+
; PROJECT:
;       ADAS 
;
; NAME:
;       CW_ADJUST_SURFACE
;
; PURPOSE:
;     To allow interactive manipulation of surface_plot window parameters.
;
; EXPLANATION:
;	On activation a pop-up window is produced allowing the user to modify the
;	properties of the surface plot produced by the routine adasplot_surface.pro. 
;	For example, the user is able to select between log and linear scaling for all axes,
;	select and change the explicit scaling of the axes, change between temperature and density
;	values to be displayed on X axis, change between mesh and shaded surfaces etc.   
;
; USE
;	This routine is a general routine to be used in conjunction with 
;	adasplot_surface.pro. For an example on its use, see adas510_plot.pro.
;
; INPUTS:
;	DATA_IN:	Structure containing all the input data necessary for adasplot_surface.
;			See adasplot_surface.pro.
;
; OPTIONAL INPUTS:
;	NONE
;
; OUTPUTS:
;	The widget returns the following structure:
;
;	baseid = { cwid: cwid, data:data }
;
;		CWID:		Widget ID of the top level base.
;		DATA:		Structure containing modified values of the input data_in structure
;				(see adasplot_surface.pro).
;
; OPTIONAL OUTPUTS:
;	NONE
;
; KEYWORD PARAMETERS:
;
; 	TDSENS:	
;
; 	NODATA:	Set up plot window, but do not plot data.
;
; 	NOTOPBASE:  
;
; 	FONT:		Font to be used in place of the default.
;
; CALLS:
;	
;	ADASPLOT_SURFACE
;
; SIDE EFFECTS:
;
;	NONE
;
; WRITTEN
;	Richard Martin, University of Strathclyde, 17-06-02
;	Based on cw_adjust_surface.pro v1.2 .
;
; MODIFIED:
;       1.1     Richard Martin
;               First release 
;
; VERSION:
;       1.1     17-06-02
;
;-
;--------------------------------------------------------------------
PRO cw_adjust_surface_event,event

      COMMON adjust_block, data

	top_base = widget_info(event.top,/child)

	widget_control,top_base,get_uvalue=state

	done=0
	error=0
	
      grprmess='                               '
	widget_control, state.messid, set_value=grprmess

	sxmin=strtrim(string(data.xmin,format='(G10.3)'),2)
  	sxmax=strtrim(string(data.xmax,format='(G10.3)'),2)  
  	symin=strtrim(string(data.ymin,format='(G10.3)'),2)
    	symax=strtrim(string(data.ymax,format='(G10.3)'),2)  
  	szmin=strtrim(string(data.zmin,format='(G10.3)'),2)
    	szmax=strtrim(string(data.zmax,format='(G10.3)'),2)

	CASE event.id OF

	  state.xstyleid: begin
	  	widget_control,state.xstyleid,get_value=temp	 
		data.xlog=temp(0)
	  end

	  state.ystyleid: begin
	  	widget_control,state.ystyleid,get_value=temp	 
		data.ylog=temp(0)
	  end
	  
	  state.zstyleid: begin
	  	widget_control,state.zstyleid,get_value=temp	 
		data.zlog=temp(0)
	  end
	  	  
	  state.list2id: begin
	  	widget_control,state.list2id,get_value=temp
	  	if temp(0) eq 0 then begin
	  		data.x=data.x1 
	  		data.xtitle=data.xtitles(0)
	  		data.title(0)=data.maintitle(0)

	  		data.xmin=data.xmin1
	  		data.xmax=data.xmax1
			if data.grpscal eq 1 then begin		
				 sxmin=strtrim(string(data.xmin,format='(G10.3)'),2)
  				 sxmax=strtrim(string(data.xmax,format='(G10.3)'),2)  
   	    			 widget_control, state.xminid, set_value=sxmin
   	    		       widget_control, state.xmaxid, set_value=sxmax  	 	     
			endif
			
	  		data.ldsel=0
	  	endif else begin 
	  		data.x=data.x2

	  		data.xtitle=data.xtitles(1)
	  		data.title(0)=data.maintitle(1)

	  		data.xmin=data.xmin2
	  		data.xmax=data.xmax2	
			
			if data.grpscal eq 1 then begin
				 sxmin=strtrim(string(data.xmin,format='(G10.3)'),2)
  				 sxmax=strtrim(string(data.xmax,format='(G10.3)'),2)        
   	    			 widget_control, state.xminid, set_value=sxmin
   	    			 widget_control, state.xmaxid, set_value=sxmax   
		    	endif 
			  			
	  		data.ldsel=1			
	  	endelse 			
	  end	
	  
	  state.grbase: begin
	  	widget_control,state.grbase,get_uvalue=toggle
		if toggle eq 0 then begin
			toggle=1
			widget_control,state.xminid,set_value=sxmin
			widget_control,state.xmaxid,set_value=sxmax
			widget_control,state.yminid,set_value=symin
			widget_control,state.ymaxid,set_value=symax
			widget_control,state.zminid,set_value=szmin
			widget_control,state.zmaxid,set_value=szmax						
		endif else begin
			toggle=0
			widget_control,state.xminid,set_value=''
			widget_control,state.xmaxid,set_value=''
			widget_control,state.yminid,set_value=''
			widget_control,state.ymaxid,set_value=''
			widget_control,state.zminid,set_value=''
			widget_control,state.zmaxid,set_value=''			
		endelse
		widget_control,state.grbase,set_button=toggle
		widget_control,state.grbase,set_uvalue=toggle
		widget_control,state.rangebase2,sensitive=toggle
		data.grpscal=toggle	
	  end  	

    	  state.xminid: begin

    	  		  ; **** get range limit and check input is legal ****
    	  		  ; **** +/-ve x-ranges allowed ****
    	    widget_control, state.xminid, get_value=temp
    	  	if strtrim(temp(0)) ne '' then begin
    	  	  error = num_chk(temp(0),sign=0)
    	  	  if error eq 1 then begin
    	  	    grprmess =   '**** Illegal Numeric Value ****'
    	  	    widget_control, state.xminid, /input_focus
    	  	  end else begin
		    if ( float(temp(0)) lt data.xmax ) then begin
    	  	    	data.xmin = float(temp(0))
			if data.ldsel eq 0 then data.xmin1=data.xmin else data.xmin2=data.xmin
		    endif else begin
    	  	      grprmess = '**** X-min must be < X-max ****'
			error = 1		    
		    endelse
    	  	  end
		  widget_control, state.messid, set_value=grprmess		    		  
    	  	end
    	  end

    	  state.xmaxid: begin

    	  		  ; **** get range limit and check input is legal ****
    	  		  ; **** +/-ve x-ranges allowed ****
    	    widget_control, state.xmaxid, get_value=temp
    	  	if strtrim(temp(0)) ne '' then begin
    	  	  error = num_chk(temp(0),sign=0)
    	  	  if error eq 1 then begin
    	  	    grprmess = '**** Illegal Numeric Value ****'	    
    	  	    widget_control, state.xmaxid, /input_focus
    	  	  end else begin
    	  	    data.xmax = float(temp(0))
		    if ( float(temp(0)) gt data.xmin ) then begin
    	  	    	data.xmax = float(temp(0))
			if data.ldsel eq 0 then data.xmax1=data.xmax else data.xmax2=data.xmax
		    endif else begin
    	  	      grprmess = '**** X-max must be > X-min ****'	
			error = 1	    
		    endelse		    
    	  	  end		
		  widget_control, state.messid, set_value=grprmess		    		    
    	  	end
    	  end


    	  state.yminid: begin

    	  		  ; **** get range limit and check input is legal ****
    	  		  ; **** +/-ve y-ranges allowed ****
    	    widget_control, state.yminid, get_value=temp
    	  	if strtrim(temp(0)) ne '' then begin
    	  	  error = num_chk(temp(0),sign=0)
    	  	  if error eq 1 then begin
    	  	    grprmess =   '**** Illegal Numeric Value ****'
    	  	    widget_control, state.yminid, /input_focus
    	  	  end else begin
		    if ( float(temp(0)) lt data.ymax ) then begin
    	  	    	data.ymin = float(temp(0))
		    endif else begin
    	  	      grprmess = '**** Y-min must be < Y-max ****'
			error = 1		    
		    endelse
    	  	  end
		  widget_control, state.messid, set_value=grprmess		    		  
    	  	end
    	  end

    	  state.ymaxid: begin

    	  		  ; **** get range limit and check input is legal ****
    	  		  ; **** +/-ve y-ranges allowed ****
    	    widget_control, state.ymaxid, get_value=temp
    	  	if strtrim(temp(0)) ne '' then begin
    	  	  error = num_chk(temp(0),sign=0)
    	  	  if error eq 1 then begin
    	  	    grprmess = '**** Illegal Numeric Value ****'	    
    	  	    widget_control, state.ymaxid, /input_focus
    	  	  end else begin
    	  	    data.ymax = float(temp(0))
		    if ( float(temp(0)) gt data.ymin ) then begin
    	  	    	data.ymax = float(temp(0))
		    endif else begin
    	  	      grprmess = '**** Y-max must be > Y-min ****'	
			error = 1	    
		    endelse		    
    	  	  end		
		  widget_control, state.messid, set_value=grprmess		    		    
    	  	end
    	  end

    	  state.zminid: begin

    	  		  ; **** get range limit and check input is legal ****
    	  		  ; **** +/-ve z-ranges allowed ****
    	    widget_control, state.zminid, get_value=temp
    	  	if strtrim(temp(0)) ne '' then begin
    	  	  error = num_chk(temp(0),sign=0)
    	  	  if error eq 1 then begin
    	  	    grprmess =   '**** Illegal Numeric Value ****'
    	  	    widget_control, state.zminid, /input_focus
    	  	  end else begin
		    if ( float(temp(0)) lt data.zmax ) then begin
    	  	    	data.zmin = float(temp(0))
		    endif else begin
    	  	      grprmess = '**** Z-min must be < Z-max ****'
			error = 1		    
		    endelse
    	  	  end
		  widget_control, state.messid, set_value=grprmess		    		  
    	  	end
    	  end

    	  state.zmaxid: begin

    	  		  ; **** get range limit and check input is legal ****
    	  		  ; **** +/-ve z-ranges allowed ****
    	    widget_control, state.zmaxid, get_value=temp
    	  	if strtrim(temp(0)) ne '' then begin
    	  	  error = num_chk(temp(0),sign=0)
    	  	  if error eq 1 then begin
    	  	    grprmess = '**** Illegal Numeric Value ****'	    
    	  	    widget_control, state.zmaxid, /input_focus
    	  	  end else begin
    	  	    data.zmax = float(temp(0))
		    if ( float(temp(0)) gt data.zmin ) then begin
    	  	    	data.zmax = float(temp(0))
		    endif else begin
    	  	      grprmess = '**** Z-max must be > Z-min ****'	
			error = 1	    
		    endelse		    
    	  	  end		
		  widget_control, state.messid, set_value=grprmess		    		    
    	  	end
    	  end


	  state.xslidid: begin
	  	  widget_control,state.xslidid,get_value=temp
		  ax=temp(0)
		  data.ax=ax
	  end

	  state.yslidid: begin
	  	  widget_control,state.yslidid,get_value=temp
		  az=temp(0)
		  data.az=az
	  end

	  state.surfstyleid: begin
			data.surface=event.index
	  end	  

	  state.colourid: begin
			cvec=[0,3,1,8]
			data.colour=cvec(event.index)
	  end	  

	  state.doneid: begin
			done=1
	  end	

	ELSE:	
      ENDCASE

	if error eq 0 then begin
	  adasplot_surface, data.x , data.y, data.peca, data.itval,	$
	      	data.title, data.xtitle, data.ytitle,	$
	      	data.strg, data.head1, data.head2a,data.head2b,	data.teva, 		$
			data.din, data.grpscal, data.xmin, data.xmax, data.ymin, 		$
			data.ymax, data.zmin, data.zmax, xlog=data.xlog, ylog=data.ylog,	$
			zlog=data.zlog, ax=data.ax, az=data.az, nodata=state.nodata,	$
			surface=data.surface, scolour=data.colour	   
	endif
	
 	widget_control,top_base,set_uvalue=state     
      
      if done eq 1 then widget_control, event.top, /destroy
END
;--------------------------------------------------------------------
FUNCTION cw_adjust_surface, data_in, tdsens=tdsens,  	$
		nodata=nodata, notopbase=notopbase, font=font

  COMMON adjust_block, data

  data=data_in

  if NOT keyword_set(tdsens) then tdsens=0
  if NOT keyword_set(nodata) then nodata=0
  if NOT keyword_set(notopbase) then notopbase=0  
  
  cwid = WIDGET_BASE(TITLE='Surface Plot Adjust Widget', /COLUMN)

  topbase = widget_base(cwid,/column,/frame)
  label   = widget_label(topbase,value='')
  label   = widget_label(topbase,value=' Scales:',font=font,/align_left)
  label   = widget_label(topbase,value='')


  scaleid = widget_base(topbase,/column,/frame)
  
  xvariables=['Temperature','Density']
  xscalebaseid=widget_base(scaleid,/row)
  label =widget_label(xscalebaseid,value=' X-axis scale: ',font=font)
  list2id   = cw_bselector(xscalebaseid,xvariables,set_value=data.ldsel,font=font) 
  if tdsens eq 1 then widget_control,l2baseid,sensitive=0  

  axesbaseid=widget_base(scaleid,/row)
  
  xbaseid = widget_base(axesbaseid,/column,/frame)
  label = widget_label(xbaseid,value='X-axis:',font=font)
  style=['linear','log']
  xstyleid = cw_bselector(xbaseid,style,set_value=data.xlog,font=font)

  ybaseid = widget_base(axesbaseid,/column,/frame)
  label = widget_label(ybaseid,value='Y-axis:',font=font)
  style=['linear','log']
  ystyleid = cw_bselector(ybaseid,style,set_value=data.ylog,font=font)

  zbaseid = widget_base(axesbaseid,/column,/frame)
  label = widget_label(zbaseid,value='Z-axis:',font=font)
  style=['linear','log']
  zstyleid = cw_bselector(zbaseid,style,set_value=data.zlog,font=font) 

 
  if notopbase eq 1 then widget_control,topbase, sensitive=0	 
 
  midbase = widget_base(cwid,/column)
  
  rangebase = widget_base(midbase,/column,/frame )

  if data.ldsel eq 0 then begin
     xmin=data.xmin1
     xmax=data.xmax1
  endif else begin
     xmin=data.xmin2
     xmax=data.xmax2
  endelse 
  
  if data.grpscal eq 1 then begin
  	  sxmin=strtrim(string(xmin,format='(G10.3)'),2)
  	  sxmax=strtrim(string(xmax,format='(G10.3)'),2)  
  	  symin=strtrim(string(data.ymin,format='(G10.3)'),2)
  	  symax=strtrim(string(data.ymax,format='(G10.3)'),2)    
  	  szmin=strtrim(string(data.zmin,format='(G10.3)'),2)
    	  szmax=strtrim(string(data.zmax,format='(G10.3)'),2)    
  endif else begin
  	  sxmin=''
  	  sxmax=''
  	  symin=''
  	  symax=''   
  	  szmin=''
    	  szmax=''  
  endelse 

  rangebutbase = widget_base(rangebase,/row,/nonexclusive)  
  grbase = widget_button(rangebutbase,value='Graph Range Scaling',font=font)
  rangebase2 = widget_base(rangebase,/row)  
  minbase = widget_base(rangebase2,/column) 
  maxbase = widget_base(rangebase2,/column) 
  
  base = widget_base(minbase,/row)  
  rc = widget_label(base, value='X-min :',font=font)   
  xminid = widget_text(base,value=sxmin,/editable,xsize=8,font=font)

  base = widget_base(maxbase,/row)
  rc = widget_label(base, value='X-max :',font=font)
  xmaxid = widget_text(base,value=sxmax,/editable,xsize=8,font=font)

  base = widget_base(minbase,/row)
  rc = widget_label(base, value='Y-min :',font=font)
  yminid = widget_text(base,value=symin,/editable,xsize=8,font=font)

  base = widget_base(maxbase,/row)
  rc = widget_label(base, value='Y-max :',font=font)
  ymaxid = widget_text(base,value=symax,/editable,xsize=8,font=font)

  base = widget_base(minbase,/row)
  rc = widget_label(base, value='Z-min :',font=font)
  zminid = widget_text(base,value=szmin,/editable,xsize=8,font=font)

  base = widget_base(maxbase,/row)
  rc = widget_label(base, value='Z-max :',font=font)
  zmaxid = widget_text(base,value=szmax,/editable,xsize=8,font=font)

  midbase = widget_base(cwid,/column,/frame)

  midlab = widget_base(midbase,/row)
  label=widget_label(midlab,value='Graph Style:',font=font)
  label=widget_label(midlab,value=' ',font=font) 
  
  widget_control, grbase, set_button=data.grpscal
  widget_control, grbase, set_uvalue=data.grpscal
  widget_control, rangebase2,sensitive= data.grpscal
  
  slidbase = widget_base(midbase,/row) 
  
  xslidid = widget_slider(slidbase, value=data.ax, maximum=90, minimum=0, $
  		xsize=160,title='     X rotation', FONT=font,/frame)
  label=widget_label(slidbase,value='   ',font=font)  
  yslidid = widget_slider(slidbase, value=data.az, maximum=90, minimum=-90, $
  		xsize=160,title='     Y rotation', FONT=font,/frame)
  
  stylebase = widget_base(midbase,/row)  
  styles   = ['Mesh   ','Surface']
  surfstyleid  = widget_droplist(stylebase, value=styles, /align_left,  $
                          font=font)

  label=widget_label(stylebase,value='            ',font=font)	
  
  ; correspond to loadct 0, 3, 1, 8
  colours   = ['B-W     ','Red Temp','Blue    ','Green   ']
  colourid  = widget_droplist(stylebase, value=colours, /align_left,  $
                          font=font)           			  			  

	;**************************
	;**** Add message line ****
	;**************************

  messbase = widget_base(cwid,/row)
  messrc = widget_label(messbase,value='') 
  messid = widget_label(messbase,value='                               ',font=font)   
  messrc = widget_label(messbase,value='')  

	;*********************
	;**** Add buttons ****
	;*********************

  buttrow = widget_base(cwid,/row)
  doneid  = widget_button(buttrow,value='Done',font=font)
  
  state   = { xstyleid: xstyleid, ystyleid: ystyleid, zstyleid: zstyleid, 	$
  			xslidid:xslidid, yslidid:yslidid, surfstyleid: surfstyleid, $
			colourid: colourid, list2id:list2id, grbase: grbase, 		$
			xminid: xminid, xmaxid: xmaxid, yminid: yminid, ymaxid: ymaxid, $
			zminid: zminid, zmaxid: zmaxid, rangebase2: rangebase2, nodata: nodata,	$
			messid: messid, doneid: doneid }  


  
  widget_control, topbase, set_uvalue= state    

  widget_control,cwid,/realize

  xmanager, 'cw_adjust_surface', cwid, event_handler='cw_adjust_surface_event',  $
              /modal,/just_reg             
  

 baseid = { cwid: cwid, data:data }
 
 return, baseid
 
END
