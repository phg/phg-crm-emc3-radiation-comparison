; Copyright (c) 1996, Strathclyde University.
; SCCS info: Module @(#)$Header: /home/adascvs/idl/adaslib/gui/cw_demap_ranges.pro,v 1.3 2004/07/06 13:05:18 whitefor Exp $ Date $Date: 2004/07/06 13:05:18 $
;
; cw_demap_ranges.pro
;
;+
; PROJECT:
;       ADAS support and application programs
;
; NAME:
;	CW_DEMAP_RANGES()
;
; PURPOSE:
;	Produces a widget for specifying grid of temperature
;
; EXPLANATION:
;	This function declares a compound widget to be used for
;	specifying a temperature grid to be used in calculations.
;	The widget consists of six text widgets used
;	for numeric data entry and an error message. 
;
;	The six text widgets are used for NTEMP, TMIN, TMAX, STEP, LOGTMIN
;       and LOGTMAX. Click on a text widget to direct keyboard input to
;       that input.
;	When the user presses the return key the keyboard input is
;	automatically transfered to the next input.  Also the values
;	entered so far are checked to see if they are legal numeric
;	values.
;
;	When the GET_VALUE call is used, the widget makes a more
;	complete check on the entered numeric values.  It checks that
;	all six values have been entered as legal numbers and that
;	minimum values are less than maximum values.  In the event of 
;	an error an error message is issued.  The caller can test to
;	see if the widget input is in error by testing to see if the
;	message returned in the widget value structure is not the
;	null string.
;
;	This widget does not generate any events.
;
; USE:
;	An example of usage;
;
;	base = widget_base(/column)
;	rngid = cw_demap_ranges(base)
;	rc = widget_button(base,value='Done')
;	widget_control,base,/realize
;	rc = widget_event()
;	widget_control,rngid,get_value=ranges
;	if ranges.scalbut eq 1 then begin
;	  if strtrim(ranges.grpmess) eq '' then begin
;           print,'STEP:',ranges.step,' NTEMP:',ranges,ntemp
;	    print,'TMIN:',ranges.tmin,'  TMAX:',ranges.tmax
;	    print,'LOG(TMIN):',ranges.logtmin,'  LOG(TMAX):',ranges.logtmax
;	  end else begin
;	    print,'Errors in ranges input:',ranges.grpmess
;	  end
;	end
;
; INPUTS:
;       PARENT   - The ID of the parent widget.
;
; OPTIONAL INPUTS:
;	None.
;
; OUTPUTS:
;       The return value of this function is the ID of the compound
;       widget which is of type LONG integer.
;
; OPTIONAL OUTPUTS:
;       None
;
; KEYWORD PARAMETERS:
;	VALUE	 - A structure describing the value of the widget;
;		   {NTEMP:'', TMIN:''   , TMAX:'',
;                   STEP:'' , LOGTMIN:'', LOGTMAX:'', GRPMESS:''}
;
;		   NTEMP    Number of points
;		   TMIN     Minimum temperature, as a string.
;		   TMAX     Maximum temperature, as a string.
;                  STEP     Step.
;		   LOGTMIN  Log of minimum temperature, as a string.
;		   LOGTMAX  Log of maximum temperature, as a string.
;		   GRPMESS Error message for ranges input.
;
;	SIGN  	 - Defines if values need to be positive or negative
;		   if sign < 0 then values must be negative
;		   if sign > 0 then values must be positive
;		   if sign = 0 then values can be either pos. or neg.
;
;       FONT     - A font to use for all text in this widget.
;
;	UVALUE	 - A user value for this widget.
;
;	BELOW	 - If set to a non-zero value allows the user to 
;		   position the axes range boxes below the title
;		   rather than to the side. On wide widgets this
;	     	   can improve the look of the disply. THe default
;		   is for them to be slightly to the right of the
;	           title, i.e. BELOW=0.
;
; CALLS:
;	NUM_CHK		Called fron RNG_GET_VAL, check numeric values.
;       See side effects for other related widget management routines.
;
; SIDE EFFECTS:
;       Three other routines are included which are used to manage the
;       widget;
;
;	RNG_GET_VAL()
;	RNG_EVENT()
;
; CATEGORY:
;	Compound Widget
;
; WRITTEN:
;       Alessandro Lanzafame, University of Strathclyde, 16th April 1996
;
; MODIFIED:
;	1.0	Alessandro Lanzafame
;		First Release
;	1.1	William Osborn
;		Put under S.C.C.S. control
;	1.2	William Osborn
;		Changed name of structure rngval to drngval so as not to
;		conflict with cw_adas_ranges
;	1.3	William Osborn
;		Removed xsize=n keyword from widget_label commands.
;		The dynlabel procedure does not override these
;		assignments and they were taken as being values in pixels.
; VERSION:
;	1.0	23-04-96
;	1.1	07-05-96
;	1.2	14-05-96
;	1.3	01-08-96
;
;-
;-----------------------------------------------------------------------------

FUNCTION dem_rng_get_val, id

;-----------------------------------------------------------------------
; Return to caller on error
;-----------------------------------------------------------------------

ON_ERROR, 2

;-----------------------------------------------------------------------
; Retrieve the state
; Get first_child widget id because state is stored there
;-----------------------------------------------------------------------

first_child = widget_info(id, /child)
widget_control, first_child, get_uvalue = state

;-----------------------------------------------------------------------
; Clear existing error message
;-----------------------------------------------------------------------

widget_control,state.messid,set_value=' '
state.rngval.grpmess = ''

;-----------------------------------------------------------------------
; Get scaling ranges and copy to value
;-----------------------------------------------------------------------

widget_control, state.ntempid,   get_value=ntemp
widget_control, state.tminid,    get_value=tmin
widget_control, state.tmaxid,    get_value=tmax
widget_control, state.stepid,    get_value=step
widget_control, state.logtminid, get_value=logtmin
widget_control, state.logtmaxid, get_value=logtmax

;-----------------------------------------------------------------------
; Update state
;-----------------------------------------------------------------------

state.rngval.ntemp   = ntemp(0)
state.rngval.tmin    = tmin(0)
state.rngval.tmax    = tmax(0)
state.rngval.step    = step(0)
state.rngval.logtmin = logtmin(0)
state.rngval.logtmax = logtmax(0)

error = 0

;-----------------------------------------------------------------------
; Check for missing values
;-----------------------------------------------------------------------

if error eq 0 then begin

   if strtrim(state.rngval.ntemp) eq '' and $
      strtrim(state.rngval.tmin) eq '' and $
      strtrim(state.rngval.step) eq '' then error = 1

   if strtrim(state.rngval.ntemp) eq '' and $
      strtrim(state.rngval.logtmin) eq '' and $
      strtrim(state.rngval.step) eq '' then error = 1

   if strtrim(state.rngval.ntemp) eq '' and $
      strtrim(state.rngval.tmax) eq '' and $
      strtrim(state.rngval.step) eq '' then error = 1

   if strtrim(state.rngval.ntemp) eq '' and $
      strtrim(state.rngval.logtmax) eq '' and $
      strtrim(state.rngval.step) eq '' then error = 1

   if strtrim(state.rngval.ntemp) eq '' and $
      strtrim(state.rngval.tmin) eq '' and $
      strtrim(state.rngval.tmax) eq '' then error = 1

   if strtrim(state.rngval.ntemp) eq '' and $
      strtrim(state.rngval.logtmin) eq '' and $
      strtrim(state.rngval.logtmax) eq '' then error = 1

   if error eq 1 then begin
      state.rngval.grpmess = '**** Missing Value ****'
      print,'ERROR: MISSING VALUE'
   endif
endif

;-----------------------------------------------------------------------
; Check for illegal values
;-----------------------------------------------------------------------

if error eq 0 then begin
   if num_chk(state.rngval.ntemp,/sign) eq 1 then error = 1
   if num_chk(state.rngval.tmin,/sign) eq 1 then error = 1
   if num_chk(state.rngval.tmax,/sign) eq 1 then error = 1
   if num_chk(state.rngval.step,/sign) eq 1 then error = 1
   if num_chk(state.rngval.logtmin,/sign) eq 1 then error = 1
   if num_chk(state.rngval.logtmax,/sign) eq 1 then error = 1
   if error eq 1 then begin
      state.rngval.grpmess = '**** Illegal Numeric Value ****'
   endif
endif

;-----------------------------------------------------------------------
; Check T range
;-----------------------------------------------------------------------

if error eq 0 then begin
   if float(state.rngval.tmin) ge float(state.rngval.tmax) then error=1
   if error eq 1 then begin
      state.rngval.grpmess = '**** T-MIN must be less than T-MAX ****'
   endif
endif

;-----------------------------------------------------------------------
; Check LOG(T) range
;-----------------------------------------------------------------------

if error eq 0 then begin
   if float(state.rngval.logtmin) ge float(state.rngval.logtmax) then error=1
   if error eq 1 then begin
      state.rngval.grpmess = '** LOG(T-MIN) must be less than LOG(T-MAX) **'
   endif
endif

if error eq 1 then $
   widget_control,state.messid,set_value=state.rngval.grpmess

RETURN, state.rngval

END

;-----------------------------------------------------------------------------

FUNCTION dem_rng_event, event

;-----------------------------------------------------------------------
; Base ID of compound widget
;-----------------------------------------------------------------------

base=event.handler

;-----------------------------------------------------------------------
; Retrieve the state
; Get first_child widget id because state is stored there
;-----------------------------------------------------------------------

first_child = widget_info(base, /child)
widget_control, first_child, get_uvalue = state

;-----------------------------------------------------------------------
; Clear existing error message
;-----------------------------------------------------------------------

widget_control,state.messid,set_value=' '
state.rngval.grpmess = ''

;-----------------------------------------------------------------------
;***********************************************************************
;******************** Process Events ***********************************
;***********************************************************************
;-----------------------------------------------------------------------

CASE event.id OF
   state.ntempid:$
        BEGIN
	  widget_control, state.ntempid, get_value=ntempval
	  ntempval=ntempval(0)
	  if num_chk(ntempval,/integer) eq 0 then begin
	     state.rngval.ntemp = ntempval
             DT = alog10((double(state.rngval.tmax)/$
                         double(state.rngval.tmin))^$
                        (1.0/(long(state.rngval.ntemp) -1))) 
             state.rngval.step=string(DT,format='(e12.4)')
             widget_control, state.stepid, set_value = state.rngval.step 
	  endif
          widget_control,state.tminid,/input_focus
         END
   state.tminid:$
        BEGIN
          widget_control,state.tminid,get_value=tminval
          tminval=tminval(0)
          if num_chk(tminval) eq 0 then begin
             state.rngval.tmin=tminval
             state.rngval.logtmin = string($
                  alog10(double(tminval)),format='(f12.6)')
             ntempval=long(                               $
                       1 + alog10(double(state.rngval.tmax)/$
                                  double(state.rngval.tmin))/$
                                  double(state.rngval.step))
             state.rngval.ntemp=string(ntempval,format='(i4)')
             DT = alog10((double(state.rngval.tmax)/$
                         double(state.rngval.tmin))^$
                        (1.0/(long(state.rngval.ntemp) -1))) 
             state.rngval.step=string(DT,format='(e12.4)')
             widget_control, state.stepid, set_value = state.rngval.step 
             widget_control,state.ntempid,set_value=state.rngval.ntemp
             widget_control,state.logtminid,set_value=state.rngval.logtmin
          endif
          widget_control,state.tmaxid,/input_focus
        END
   state.tmaxid:$
        BEGIN
          widget_control,state.tmaxid,get_value=tmaxval
          tmaxval=tmaxval(0)
          if num_chk(tmaxval) eq 0 then begin
             state.rngval.tmax=tmaxval
             state.rngval.logtmax = string($
                  alog10(double(tmaxval)),format='(f12.6)')
             ntempval=long(                               $
                       1 + alog10(double(state.rngval.tmax)/$
                                  double(state.rngval.tmin))/$
                                  double(state.rngval.step))
             state.rngval.ntemp=string(ntempval,format='(i4)')
             DT = alog10((double(state.rngval.tmax)/$
                         double(state.rngval.tmin))^$
                        (1.0/(long(state.rngval.ntemp) -1))) 
             state.rngval.step=string(DT,format='(e12.4)')
             widget_control, state.stepid, set_value = state.rngval.step 
             widget_control,state.ntempid,set_value=state.rngval.ntemp
             widget_control,state.logtmaxid,set_value=state.rngval.logtmax
          endif
          widget_control,state.stepid,/input_focus
        END
   state.stepid:$
        BEGIN
	  widget_control, state.stepid, get_value=stepval
	  stepval=stepval(0)
	  if num_chk(stepval) eq 0 then begin
	    state.rngval.step = stepval
            NTEMPVAL = long(1 + alog10(double(state.rngval.tmax)/$
                                       double(state.rngval.tmin))/$
                                       double(state.rngval.step))
            state.rngval.ntemp=string(NTEMPVAL,format='(i4)')
            widget_control, state.ntempid, set_value = state.rngval.ntemp 
	  endif
          widget_control,state.logtminid,/input_focus
         END
   state.logtminid:$
        BEGIN
          widget_control,state.logtminid,get_value=logtminval
          logtminval=logtminval(0)
          if num_chk(logtminval) eq 0 then begin
             state.rngval.logtmin=logtminval
             state.rngval.tmin = string($
                  10^(double(logtminval)),format='(e12.4)')
             ntempval=long(                               $
                       1 + alog10(double(state.rngval.tmax)/$
                                  double(state.rngval.tmin))/$
                                  double(state.rngval.step))
             state.rngval.ntemp=string(ntempval,format='(i4)')
             DT = alog10((double(state.rngval.tmax)/$
                         double(state.rngval.tmin))^$
                        (1.0/(long(state.rngval.ntemp) -1))) 
             state.rngval.step=string(DT,format='(e12.4)')
             widget_control, state.stepid, set_value = state.rngval.step 
             widget_control,state.ntempid,set_value=state.rngval.ntemp
             widget_control,state.tminid,set_value=state.rngval.tmin
          endif
          widget_control,state.logtmaxid,/input_focus
        END
    state.logtmaxid:$
        BEGIN
          widget_control,state.logtmaxid,get_value=logtmaxval
          logtmaxval=logtmaxval(0)
          if num_chk(logtmaxval) eq 0 then begin
             state.rngval.logtmax=logtmaxval
             state.rngval.tmax = string($
                  10^(double(logtmaxval)),format='(e12.4)')
             ntempval=long(                               $
                       1 + alog10(double(state.rngval.tmax)/$
                                  double(state.rngval.tmin))/$
                                  double(state.rngval.step))
             state.rngval.ntemp=string(ntempval,format='(i4)')
             widget_control,state.ntempid,set_value=state.rngval.ntemp
             DT = alog10((double(state.rngval.tmax)/$
                         double(state.rngval.tmin))^$
                        (1.0/(long(state.rngval.ntemp) -1))) 
             state.rngval.step=string(DT,format='(e12.4)')
             widget_control, state.stepid, set_value = state.rngval.step 
             widget_control,state.tmaxid,set_value=state.rngval.tmax
          endif
          widget_control,state.ntempid,/input_focus
        END
    ELSE:

ENDCASE

;-----------------------------------------------------------------------
; Check numeric values
;-----------------------------------------------------------------------

error = 0
    i = 0

id = [state.ntempid, state.tminid,    state.tmaxid,$
      state.stepid,    state.logtminid, state.logtmaxid]

while (error eq 0 and i lt 6) do begin
   valueid = id(i)
   widget_control,valueid,get_value=num
   if strtrim(num(0)) ne '' then begin
      error = num_chk(num(0),/sign)
      if error eq 1 then begin
         state.rngval.grpmess = '**** Illegal Numeric Value ****'
         widget_control,state.messid,set_value=state.rngval.grpmess
         widget_control,valueid,/input_focus
      endif
   endif
   i = i + 1
endwhile

;-----------------------------------------------------------------------
; Save the new state structure 
;-----------------------------------------------------------------------

widget_control, first_child, set_uvalue=state, /no_copy

RETURN, 0L
END

;-----------------------------------------------------------------------------

FUNCTION cw_demap_ranges, parent, SIGN=sign, BELOW=below,$
		VALUE=value, FONT=font, UVALUE=uvalue

IF (N_PARAMS() LT 1) THEN MESSAGE, 'Must specify PARENT for cw_demap_ranges'
IF (NOT KEYWORD_SET(sign)) then sign=0

;-----------------------------------------------------------------------
; Set defaults for keywords
;-----------------------------------------------------------------------

IF NOT (KEYWORD_SET(value)) THEN begin
   rngval = {drngval,$
             NTEMP:'', TMIN:'',TMAX:'',$
             STEP:'', LOGTMIN:'',LOGTMAX:'', GRPMESS:''}
ENDIF ELSE BEGIN
   rngval = { drngval, $
              NTEMP:value.ntemp,$
              TMIN:value.tmin,$
              TMAX:value.tmax,$
              STEP:value.step,$
              LOGTMIN:value.logtmin,$
              LOGTMAX:value.logtmax,$
              GRPMESS:value.grpmess}

ENDELSE

IF NOT (KEYWORD_SET(font)) THEN font = ''
IF NOT (KEYWORD_SET(uvalue))  THEN uvalue = 0
IF NOT (KEYWORD_SET(below)) THEN below = 0

;-----------------------------------------------------------------------
; Create the main base for the widget
;-----------------------------------------------------------------------

main    = WIDGET_BASE(parent, UVALUE = uvalue, $
	  EVENT_FUNC = "dem_rng_event", $
	  FUNC_GET_VALUE = "dem_rng_get_val", $
	  /COLUMN)

;-----------------------------------------------------------------------
; Create a dummy widget just to hold value of
; "state" variable so as not to get confused
; with any other values. Adopt IDL practice 
; of using first child widget
;-----------------------------------------------------------------------

first_child = widget_base(main)

scbasid = widget_base(first_child,/column)

;-----------------------------------------------------------------------
;			Range widgets
;-----------------------------------------------------------------------

inbase = widget_base(scbasid,/row)

if below eq 0 then begin
     base = widget_base(inbase,/column,/nonexclusive)
     rngbasid = widget_base(inbase,/row)
endif else begin
     base = widget_base(inbase,/column)
     newrow = widget_base(base,/row,/nonexclusive)
     rngbasid = widget_base(base,/row)
endelse

base = widget_base(inbase,/column)
rngbasid = widget_base(base,/row)

pnstbase = widget_base(rngbasid,/column)
minbase = widget_base(rngbasid,/column)
maxbase = widget_base(rngbasid,/column)

base = widget_base(pnstbase,/row)
msg_text = 'No of Temperature points :'
rc = widget_label(base,value=msg_text,FONT=font)
ntempid = widget_text(base,/editable,xsize=5,font=font)

base = widget_base(minbase,/row)
rc = widget_label(base,value='      T-min :',font=font)
tminid = widget_text(base,/editable,xsize=12,font=font)

base = widget_base(maxbase,/row)
rc = widget_label(base,value='      T-max :',font=font)
tmaxid = widget_text(base,/editable,xsize=12,font=font)

base = widget_base(pnstbase,/row)
msg_text = '   Step in Log(T) :'
rc = widget_label(base,value=msg_text,FONT=font)
stepid = widget_text(base,/editable,xsize=12,font=font)

base = widget_base(minbase,/row)
rc = widget_label(base,value=' Log(T-min) :',font=font)
logtminid = widget_text(base,/editable,xsize=12,font=font)

base = widget_base(maxbase,/row)
rc = widget_label(base,value=' Log(T-max) :',font=font)
logtmaxid = widget_text(base,/editable,xsize=12,font=font)

;-----------------------------------------------------------------------
; Message
;-----------------------------------------------------------------------

if strtrim(rngval.grpmess) eq '' then begin
    message = ' '
endif else begin
    message = rngval.grpmess
endelse
messid = widget_label(scbasid,value=message,font=font)


;-----------------------------------------------------------------------
; Set initial state according to value
;-----------------------------------------------------------------------

    widget_control, ntempid,   set_value=rngval.ntemp
    widget_control, tminid,    set_value=rngval.tmin
    widget_control, tmaxid,    set_value=rngval.tmax
    widget_control, stepid,    set_value=rngval.step
    widget_control, logtminid, set_value=rngval.logtmin
    widget_control, logtmaxid, set_value=rngval.logtmax

;-----------------------------------------------------------------------
; Create state structure
;-----------------------------------------------------------------------

  new_state = { RNGBASID:rngbasid, MESSID:messid, $
		NTEMPID:ntempid, TMINID:tminid, TMAXID:tmaxid,$
                STEPID:stepid, LOGTMINID:logtminid, LOGTMAXID:logtmaxid, $
		RNGVAL:rngval, $
		FONT:font }

;-----------------------------------------------------------------------
; Save initial state structure
;-----------------------------------------------------------------------
    widget_control, first_child, set_uvalue=new_state,/no_copy

  RETURN, main    

END
