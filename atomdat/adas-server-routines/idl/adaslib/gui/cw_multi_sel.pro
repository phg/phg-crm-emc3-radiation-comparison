; Copyright (c) 1995 Strathclyde University .
; SCCS info: Module @(#)$Header: /home/adascvs/idl/adaslib/gui/cw_multi_sel.pro,v 1.8 2004/07/06 13:08:35 whitefor Exp $ Date $Date: 2004/07/06 13:08:35 $
;
;+
; PROJECT:
;	ADAS IBM MVS to DEC UNIX conversion
;
; NAME:
;	CW_MULTI_SEL()
;
; PURPOSE:
;	Multiple selection list compound widget.
;
; EXPLANATION:
;	This compound widget function produces a multiple selection list.
;	The LIST is a one dimensional string array of text items.
;	The selection list consists of toggle buttons, one for each item
;	in the list.  The buttons are stacked in columns of 20.  The
;	caller can specify the maximum number of selections which are
;	permitted.  This widget does not generate any events.
;
;	This file includes the event management and SET_VALUE/GET_VALUE
;	routines.  The VALUE of this widget is an integer array which gives
;	the indices of the items selected from LIST.  The array is of
;	length MAXSEL, the maximum number of selections permitted.
;	A set of selections can be specified when the widget is created
;	using the SELECTED keyword, or dynamically using WIDGET_CONTROL,
;	SET_VALUE.  WIDGET_CONTROL,GET_VALUE may be used to recover the
;	user's selections.  WIDGET_CONTROL,GET_VALUE will only give the
;	correct selections when the buttons have been managed with the
;	event handler MULSEL_EVENT, ie. when WIDGET_EVENT or XMANAGER
;	have been used to trap widget events.  The array of index
;	selections which is the VALUE of this widget preserves the order
;	in which the selections were made unless the ORDERED keyword is
;	set.  If fewer than the maximum	number of selections are made
;	the 'empty' spaces in the array	are given the value -1.
;
; USE:
;	See the routine MULTI_SEL for a working example of how to use
;	CW_MULTI_SEL.
;
;	The following creates a multiple selection list for the items
;	'Red','Blue','Green','Yellow','Orange' and 'Black'.  The user
;	is allowed to select three of these items.  The widget is not
;	realized at this stage;
;
;		list = ['Red','Blue','Green','Yellow','Orange','Black']
;		base = WIDGET_BASE(/COLUMN)
;		selid = CW_MULTI_SEL(base,list,MAXSEL=3,UVALUE='colours')
;
;	The following code creates the same selection list but this time
;	items 'Green' and 'Orange' are set as selected when the widget
;	is realized and the maximum number of selections is four;
;
;               list = ['Red','Blue','Green','Yellow','Orange','Black']
;		selected = [2,4,-1,-1]
;               base = WIDGET_BASE(/COLUMN)
;               selid = CW_MULTI_SEL(base,list,SELECTED=selected)
;
;	The following code can be used to reset the current selections
;	for the widget;
;
;		WIDGET_CONTROL,selid,SET_VALUE=[1,3,-1,-1]
;
;	The following code will recover the value for the multiple
;	selection list widget;
;
;		WIDGET_CONTROL,selid,GET_VALUE=selected
;
;	The GET_VALUE keyword will only return the correct selections
;	if XMANAGER or WIDGET_EVENT have been in operation whilst the
;	user operated the selection list buttons.  This widget does not
;	generate any events, all button events are managed internally.
;
; INPUTS:
;       PARENT   - The ID of the parent widget.
;
;	LIST     - One dimensional string array of items for the
;		   selection list.  Each list item will be assigned
;		   a button.
;
; OPTIONAL INPUTS:
;	None.  See the keywords for additional controls.
;
; OUTPUTS:
;	The return value of this function is the ID of the compound
;	widget which is of type LONG integer.
;
; OPTIONAL OUTPUTS:
;	None
;
; KEYWORDS:
;	MAXSEL   - Maximum number of selections allowed.  The default is
;		   one.  If a SELECTED array is specified then the value
;		   of MAXSEL is set to the length of SELECTED.
;
;	SELECTED - One dimensional integer array of the indices of the
;		   items in LIST which are to be set as selected.
;		   MAXSEL will be set to the length of SELECTED.
;		   If fewer than MAXSEL items are selected the unused
;		   values should be set to -1.  All -1 values should be
;		   at the end of the array.  For example:
;		   SELECTED=[0,5,-1,-1] will cause MAXSEL to be 4,
;		   the current selections are elements 0 and 5 from LIST.
;
;	ORDERED  - When the ordered keyword is non-zero the list of 
;		   selections in the SELECTED array is arranged in order
;		   of ascending value regardless of the order in which 
;		   the list items were chosen.  The -1 values used to
;		   pad the selected array still remain at the end.
;		   For example if the ordered keyword is not set the
;		   values in selected will match the order in which the
;		   items were selected from LIST e.g [3,1,5,-1,-1].
;		   With the ordered keyword non-zero the SELECTED array
;		   would be [1,3,5,-1,-1].
;
;	UVALUE   - Supplies the user value for the widget.  This is 
;		   exclusively for the caller's use and has no effect on
;		   the operation of the widget.
;
;	FONT	 - Supplies the font to be used for the widget.
;
; CALLS:
;	None directly.  See SIDE EFFECTS below for a list of the widget
;	management routines.
;
; RESTRICTIONS:
;	None.
;
; SIDE EFFECTS:
;	This widget uses a COMMON BLOCK: CW_MULSEL_BLK to hold the
;	widget state.
;
;	Three other routines are included which are used to manage the
;	widget;
;
;	MULSEL_SET_VAL
;	MULSEL_GET_VAL()
;	MULSEL_EVENT()
;
; CATEGORY:
;	Compound Widget?
;
; WRITTEN:
;	Andrew Bowen, Tessella Support Services plc, 4-Mar-1993
;
; MODIFIED:
;	Version 1	Andrew Bowen	
;			First release.
;	Version 1.1	Andrew Bowen
;			New selection list keyword ORDERED
;	Version 1.7	Tim Hammond
;			Previous history unknown. Now added second 
;			message widget so that number of selections
;			allowed always remains visible. Also tidied
;			up code.
;	1.8	Richard Martin
;		Removed obsolete cw_loadstate/savestate statements.
;
; VERSION:
;       1.1       11-Oct-1993
;	1.7	  11-Dec-1995
;	1.8	10-01-02
;-

;-----------------------------------------------------------------------------

PRO mulsel_set_val, id, value


		;**** Return to caller on error ****
  ON_ERROR, 2

		;**** Retrieve the state ****

  first_child = widget_info(id,/child)
  widget_control, first_child, get_uvalue=state, /no_copy

		;**** copy the new selected list ****
  state.selected = value

		;**** Set all buttons off ****
  widget_control, state_base, set_button=0

		;**** Set buttons for new selections and ****
		;**** see how many selections exist.     ****
  state.numsel = 0
  for i = 0, state.maxsel-1 do begin
    if state.selected(i) ge 0 then begin
      widget_control,state.listid(state.selected(i)),/set_button
      state.numsel = state.numsel+1
    end
  end

  widget_control, first_child, set_uvalue=state, /no_copy

END

;-----------------------------------------------------------------------------

FUNCTION mulsel_get_val, id


		;**** Return to caller on error ****
  ON_ERROR, 2

		;**** Retrieve the state ****

  first_child = widget_info(id,/child)
  widget_control, first_child, get_uvalue=state, /no_copy

		;**** If "ordered" set re-order selections in index order ****
  if state.ordered gt 0 and state.numsel gt 1 then begin
    change = 1
    while (change eq 1) do begin
      change = 0
      for i = 0, state.numsel-2 do begin
	if state.selected(i) gt state.selected(i+1) then begin
	  keep = state.selected(i)
	  state.selected(i) = state.selected(i+1)
	  state.selected(i+1) = keep
	  change = 1
	end
      end
    end
  end

  selected=state.selected
  widget_control, first_child, set_uvalue=state, /no_copy

  RETURN, selected

END

;-----------------------------------------------------------------------------

FUNCTION mulsel_event, event


		;**** Base ID of compound widget ****
  parent=event.handler

		;**** Retrieve the state ****

  first_child = widget_info(parent,/child)
  widget_control, first_child, get_uvalue=state, /no_copy

		;**** Remove existing error message ****

  widget_control,state.errmessid,set_value=' '

		;************************
		;**** Process events ****
		;************************
  CASE event.select OF

		;**** Selection ****
    1: begin

      if state.numsel lt state.maxsel then begin
		;**** Add new selection to selected ****
        widget_control,event.id,get_uvalue=index
        state.selected(state.numsel) = index
        state.numsel = state.numsel+1
      endif else begin
		;**** Warn maximum reached, switch button off ****
        widget_control,state.errmessid,set_value='Maximum selections reached'
        widget_control,event.id,set_button=0
      endelse

    end

		;**** Unselection ****
    0: begin
		;**** Find the item to be unselected in the ****
		;**** selected array.                       ****
      widget_control,event.id,get_uvalue=index
      pos = where(state.selected eq index)

		;**** Remove the item from the selected list ****
      state.numsel = state.numsel-1
      sels = state.selected

      case pos(0) of

        0: $ 
	  if state.maxsel gt 1 then begin
            state.selected = [sels(1:(state.maxsel-1)),-1]
	  endif else begin
	    state.selected = [-1]
	  endelse

        state.maxsel-1: $
          state.selected = [sels(0:(state.maxsel-2)),-1]

        else: $
          state.selected = [sels(0:pos(0)-1),sels(pos(0)+1:(state.maxsel-1)),-1]

      endcase

    end

  ENDCASE

		;**** Reset the value of state variable ****

  widget_control, first_child, set_uvalue=state, /no_copy


  RETURN, 0L
END

;-----------------------------------------------------------------------------

FUNCTION cw_multi_sel,	topparent, list, SELECTED = selected, $
			MAXSEL = maxsel, ORDERED = ordered, $
			UVALUE = uvalue, FONT=font


		;**** Flag error and return to caller ****
  IF (N_PARAMS() LT 2) THEN MESSAGE, $
		'Must specify a PARENT and LIST for cw_multi_sel'
  ON_ERROR, 2

		;**** Set defaults for keywords ****
  IF NOT (KEYWORD_SET(maxsel)) THEN maxsel = 1
  IF NOT (KEYWORD_SET(selected)) THEN $
			selected = make_array(maxsel,/int,value=-1)
  IF NOT (KEYWORD_SET(ordered)) THEN ordered = 0
  IF NOT (KEYWORD_SET(uvalue))  THEN uvalue = 0
  IF NOT (KEYWORD_SET(font)) THEN font = ''

		;**** Over-ride MAXSEL with the length of SELECTED ****
		;**** this is in case they are different.          ****
  IF (KEYWORD_SET(selected)) THEN begin
    seldim = size(selected)
    maxsel = seldim(1)
  end

		;**** Create the main base for the table ****
  parent = WIDGET_BASE(topparent, UVALUE = uvalue, $
		EVENT_FUNC = "mulsel_event", $
		FUNC_GET_VALUE = "mulsel_get_val", $
		PRO_SET_VALUE = "mulsel_set_val", $
		/COLUMN)

		;**** Create base to hold the value of state ****

  first_child = widget_base(parent)

  mainbase = widget_base(first_child,/column)

		;**** Create top and bottom areas ****
  bigtopbase = widget_base(mainbase, /column, /frame)
  topbase = widget_base(bigtopbase,/row)
  botbase = widget_base(mainbase,/column)

		;**** See how many columns of buttons are requried ****
  listdim = size(list)
  ncols = fix((listdim(1)+19)/20)

		;**** Create selection list of toggle button widgets ****
  listid = lonarr(listdim(1))
  for j = 0, ncols-1 do begin
    colbase = widget_base(topbase,/nonexclusive,/column,/frame)
    for i = (j*20), min([(j*20)+19,listdim(1)-1]) do begin
      listid(i) = widget_button(colbase,value=list(i),uvalue=i,font=font)
    end
  end

		;**** Set buttons for current selections and ****
		;**** see how many selections exist.         ****
  numsel = 0
  for i = 0, maxsel-1 do begin
    if selected(i) ge 0 then begin
      widget_control,listid(selected(i)),/set_button
      numsel = numsel+1
    end
  end

		;**** Create error message ****

    errmessid = widget_label(bigtopbase, value=' ', font=font)

		;**** Create main message ****

    messid = widget_label(botbase, value='Make a Maximum of '+ $
                strcompress(string(maxsel),/remove_all)+' selections', $
                font=font)

		;**** Create state structure ****

    new_state =  {LISTID:listid, MESSID:messid, 			$
		  LIST:list, SELECTED:selected, 			$
		  MAXSEL:maxsel, NUMSEL:numsel, 			$
		  ERRMESSID:errmessid, ORDERED:ordered}

		;**** Save initial state structure ****

  widget_control, first_child, set_uvalue=new_state, /no_copy

  RETURN, parent

END





