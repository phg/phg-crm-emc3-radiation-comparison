; Copyright (c) 1999, Strathclyde University.
; SCCS info: Module @(#)$Header: /home/adascvs/idl/adaslib/gui/adas_file_gr.pro,v 1.1 2004/07/06 11:10:05 whitefor Exp $ Date $Date: 2004/07/06 11:10:05 $
;+
; PROJECT:
;       ADAS 
;
; NAME:
;	ADAS_file_gr
;
; PURPOSE:
;	A popup window asking for the name of a graphics output file.
;
; EXPLANATION:
;	This routine pops up a window which asks for the name of a file
;       for graphics output. Different graphics formats are allowed. A 
;       choice of paper size (A4 or US letter) is allowed. 'Done' and 'Cancel'
;       buttons are included in the widget.
;
; INPUTS:
;       VALUE - Structure holding file information.
;
; OPTIONAL INPUTS:
;	None.
;
; OUTPUTS:
;       The return value of this function is the ID of the compound
;       widget which is of type LONG integer.
;
; OPTIONAL OUTPUTS:
;       None
;
; KEYWORD PARAMETERS:
;	XSIZE	 - Integer; x size in characters of browse widget.
;		   Default is 50.
;
;	YSIZE	 - Integer; y size in characters of browse widget.
;		   Default is 20.
;
;	FONT	 - A font to use for all text in this widget.
;
;	TITLE	 - Question to ask the user.
;
;	WINTITLE - Title of the popup window.
;
; CALLS:
;	CW_ADAS_FILE_GR    the work is done here
;
; SIDE EFFECTS:
;	One other routine is included which is used to manage the widget;
;
;	ADAS_FILE_GR_EVENT
;
;       A common block is used to store external information. Until all
;       sites are using IDL v5 (and above) we are stuck with this method.
;
;
; CATEGORY:
;	Widgets
;
; WRITTEN:
;       Martin O'Mullane
;
; MODIFIED:
;	1.1	Martin O'Mullane
;		First release 
;
; VERSION:
;       1.1	17-03-99
;
;-
;-----------------------------------------------------------------------------

PRO adas_file_gr_event, event


                ;**** Must(?) use this method of returning info ****
                ;**** from a modal widget - can use pointers    ****
                ;**** have to wait until everyone is using v5.  ****

COMMON adas_file_gr_com, action, value

action = event.action


CASE action OF

 
  'Done'    : begin
                child = widget_info(event.id,/child)
                widget_control, child,get_value = value 
                widget_control, event.top, /destroy
              end
 
  'Cancel'  : widget_control, event.top, /destroy

  else      : print,'ADAS_FILE_GR_EVENT : You should not see this message! '
                   
                   
ENDCASE


END

;-----------------------------------------------------------------------------


PRO adas_file_gr, val, act, XSIZE = xsize, YSIZE = ysize, FONT = font, $
                         WINTITLE = wintitle, TITLE = title

COMMON adas_file_gr_com, action, value

  value = val
		;**** Set defaults for keywords ****

  IF NOT (KEYWORD_SET(xsize))    THEN xsize = 50
  IF NOT (KEYWORD_SET(ysize))    THEN ysize = 20
  IF NOT (KEYWORD_SET(font))     THEN font  = ''
  IF NOT (KEYWORD_SET(title))    THEN title = 'Enter a filename'
  IF NOT (KEYWORD_SET(wintitle)) THEN wintitle = 'ADAS FILE ENTRY'


                ;***********************************
		;**** create titled base widget ****
                ;***********************************
                
  parent = widget_base(title=wintitle, xoffset=xsize,yoffset=ysize,/column)

                ;**** Declare output options widget ****
                
  cwid = cw_adas_file_gr(parent, VALUE=value, TITLE=title, FONT=font)

  
  
                ;****************************
		;**** realize the widget ****
                ;****************************

  
  dynlabel, parent
  widget_control,parent,/realize


		;**** make widget modal ****
                
  xmanager,'adas_file_gr',parent,event_handler='adas_file_gr_event',/modal,/just_reg
 
  act = action
  val = value

END
