; Copyright (c) 2000 Strathclyde University .
;+
; PROJECT:
;       ADAS 
;
; NAME:
;       ADAS_IN_V2
;
; PURPOSE:
;       IDL ADAS user interface, input file selection.
;
; EXPLANATION:
;       This routine creates and manages a pop-up window which allows
;       the user to select a file (or multiple files) from an ADAS adfXX
;       directory or from the UNIX file system.  It is a general routine.
;       This should be a direct replacement for adas_in but with added
;       capability of selecting more than one file. Note that it is not
;       possible to select files from different directories.
;
; USE:  adas_in_v2, val, act, /MANY
;       
;
; INPUTS:
;       INVAL   - A structure which determines the initial settings of
;                 the dataset selection widget.
;
; OPTIONAL INPUTS:
;       None
;
; OUTPUTS:
;       INVAL   - On output the structure records the final settings of
;                 the dataset selection widget if the user pressed the
;                 'Done' button otherwise it is not changed from input.
;                
;                 A structure which determines the initial settings of
;                 the dataset selection widget. The structure must be,
;                 {ROOTPATH:'', FILE:'', CENTROOT:'', USERROOT:'' }
;
;                 ROOTPATH - Current data directory.
;                 FILE     - Current data file(s) in ROOTPATH.
;                 CENTROOT - Default central data store.
;                 USERROOT - Default user data store.
;
;                 The data file selected by the user is obtained by
;                 appending ROOTPATH and FILE.
;
;                 Path names may be supplied with or without the trailing
;                 '/'.  The underlying routines add this character where
;                 required so that USERROOT will always end in '/' on
;                 output.
;
;                 The default value is;
;                 {ROOTPATH:'./', FILE:'', CENTROOT:'', USERROOT:''}
;                 i.e ROOTPATH is set to the user's current directory.;
;
;       ACTION    - A string; indicates the user's action when the pop-up
;                 window is terminated, i.e which button was pressed to
;                 complete the input.  Possible values are 'Done' and
;                 'Cancel'.
;
; OPTIONAL OUTPUTS:
;       None
;
; KEYWORD PARAMETERS:
;       WINTITLE   - A title to be used on the banner for the pop-up window.
;                    Intended to indicate which application is running,
;                    e.g 'ADAS 205 INPUT'
;
;       TITLE      - The title to be included in the input file widget, used
;                    to indicate exactly what the required input dataset is,
;                    e.g 'Input COPASE Dataset'
;
;       FONT_LARGE - Supplies the font to be used for the interface widgets.
;
;       FONT_SMALL - Supplies the font to be used for the interface widgets.
;
;       MANY       - Indicates that more than one file may be selected.
;
;       PARENT     - The group leader for this widget program. Used if we
;                    need this widget to be modal.
;
;
; CALLS:
;       XMANAGER        Manages the pop=up window.
;       ADAS_IN_EV      Event manager, called indirectly during XMANAGER
;                       event management.
;       ACQUIRE_PATH    Extracts the path of an input file.
;
; RESTRICTIONS:
;       Version 5 of IDL is required.
;
;
; SIDE EFFECTS:
;
;
; CATEGORY:
;       Widgets
;
; WRITTEN:
;       Martin O'Mullane
;
; MODIFIED:
;       1.1     Martin O'Mullane
;               First release 
;       1.2     Martin O'Mullane
;                 - Check against listcheck[0] rather than listcheck to
;                   fix a problem with IDL v5.4 on DEC machines (GA).
;                   This is harmless to other machines.
;       1.3     Allan Whiteford
;                 - Do not display 'CVS' directories in the file selection
;                   widgets.
;       1.4     Allan Whiteford
;                 - Changed rstrpos to strpos with /reverse_search.
;       1.5     Martin O'Mullane
;                 - Protect pointer de-reference.
;
; VERSION:
;       1.1     21-08-2000
;       1.2     07-11-2002
;       1.3     15-07-2004
;       1.4     13-04-2005
;       1.5     27-11-2010
;
;-
;-----------------------------------------------------------------------------



FUNCTION acquire_path, filename

; Find the end of the directory path and extract it

  pathend = strpos(filename,'/',/reverse_search)

  if pathend gt 0 then path = strmid(filename,0,pathend) $
                  else path = ''

  RETURN, path

END

;-----------------------------------------------------------------------------


FUNCTION ADAS_IN_V2_FILE, event

   ; Sensitise 'Done' and 'Browse' if we are ready to continue 

   Widget_Control, event.top, Get_UValue=info
     
   Widget_Control, info.flID, Get_Value=file

   filename = file.rootpath + file.file
   file_acc, filename, exist, read, write, execute, filetype
     
   
   if event.action EQ 'newfile' AND filetype EQ '-' then begin
      Widget_Control, info.doneID, sensitive=1
      Widget_Control, info.browseID, sensitive=1
   endif else begin
      Widget_Control, info.doneID, sensitive=0
      Widget_Control, info.browseID, sensitive=0
   endelse
   
   RETURN,0

END 

;-----------------------------------------------------------------------------

PRO ADAS_IN_V2_EVENT, event

; Get the info structure out of the user value of the top-level base.

Widget_Control, event.top, Get_UValue=info

; Reset any error messages

Widget_Control, info.messID, Set_Value='                                 '

; Something nefarious may cause an error also.
;  -- if so trap it and exit gracefully

;CATCH, error
;IF error NE 0 THEN BEGIN
;   formdata = {cancel:1}
;   *info.ptrToFormData = formdata
;   Widget_Control, event.top, /Destroy
;   message,!err_string
;   RETURN
;ENDIF


; What type of event brought us here - buttons (Done, Cancel or Browse)
; or one of the others.

list = [info.doneID, info.browseID, info.cancelID]
ind  = where(list EQ event.id, count)
if count GT 0 then Widget_Control, event.id, Get_Value=userButton $
              else userButton = 'Not me'



CASE event.id OF

  info.rootID : Begin
    
     if event.action EQ 'newroot' then begin

         ; Reset the value of the file selection widget

         widget_control, info.rootID, get_value=rootval
         fle = acquire_path(info.inval.file[0])
         widget_control, info.fileID, set_value=fle

         ; Update the state

         info.inval.file = ''
         info.inval.rootpath = rootval.rootpath

         ; Resensitise file selection

         widget_control, info.fileid, /sensitive

     endif else if event.action eq 'rootedit' then begin

         ; When path is being edited desensitise file selection 

         widget_control, info.fileid, sensitive=0

     endif
     
     
   ; Is the current 'file' is a file or a directory?
   ; if not extract directory path. In any case get the list of files 
   ; in the directory.

     listdir = info.inval.rootpath + info.inval.file[0]
     file_acc,listdir,fileexist,read,write,execute,filetype

     if filetype NE 'd' then listdir = acquire_path(listdir)

     list = findfile(listdir)

   ; If the current directory is not 'root' add the '..' symbol to the list.  

     if strtrim(info.inval.file[0]) ne '' then $
        if strtrim((*(info.list))[0]) ne '' then list = ['..',list] $
                                            else list = ['..']
    ; Do not display CVS directories
     
     if (where(list eq 'CVS'))[0] ne -1 then list=list[where(list ne 'CVS')]

     widget_control, info.selID, set_value=list
     
     *(info.list) = list            

     if filetype NE '-' then Widget_Control, info.browseID, sensitive=0 $
                        else Widget_Control, info.browseID, sensitive=1

  End

  
  info.selID : Begin
    
    
   ; See if the current 'file' is a file or directory
   
        file = info.inval.rootpath + info.inval.file[0]
        file_acc,file,fileexist,read,write,execute,filetype

        n_list = n_elements(*(info.list))
        
        ; Shift-click can give large, nonsensical event.index values
        ; do nothing when this happens.
        
        if (event.index LT 0L) OR (event.index GT n_list) then return
        
        if (n_list GT 1) then listcheck = (*info.list)[event.index] $
                         else listcheck = *info.list

        if filetype NE '-' then Widget_Control, info.browseID, sensitive=0 $
                           else Widget_Control, info.browseID, sensitive=1

        if listcheck[0] EQ '..' then begin
          
          ; up a directory 
           
          info.inval.file[0] = acquire_path(info.inval.file[0])

          widget_control,info.fileID,set_value=info.inval.file[0]
          file = info.inval.rootpath + info.inval.file[0]
          list = findfile(file)
          if strtrim(info.inval.file[0]) ne '' then begin
            if strtrim(list[0]) NE '' then list = ['..',list]  $
                                      else list = ['..']
          endif
         
	  ; Do not display CVS directories
          if (where(list eq 'CVS'))[0] ne -1 then list=list[where(list ne 'CVS')]
 
          widget_control,info.selID,set_value=list


        endif else begin

          ; down a directory or select file(s)
           
          if filetype eq 'd' then begin
          
            if strtrim(info.inval.file[0]) eq '' then begin
               info.inval.file[0] = listcheck
            endif else begin
              info.inval.file[0] =  info.inval.file+'/'+listcheck
            endelse
          
          endif else begin
          
            path = acquire_path(info.inval.file[0])
            if strtrim(path) eq '' then begin
              info.inval.file[0] = listcheck
            endif else begin
              info.inval.file[0] = path+'/'+listcheck
            endelse
          
          endelse

          if info.many EQ 0 then begin
             fle = info.inval.file[0]
          endif else begin
             file = info.inval.rootpath + info.inval.file[0]
             file_acc,file,fileexist,read,write,execute,filetype
             if filetype eq 'd' then fle = info.inval.file[0] $
                                else fle = acquire_path(info.inval.file[0])
          endelse
          
          widget_control, info.fileID, set_value=fle


          ; See if the new 'file' is a file or directory
          ; and update list if new selection is a directory.
          ; Toggle Browse on/off if a file can be viewed. 
          
          file = info.inval.rootpath + info.inval.file[0]
          file_acc,file,fileexist,read,write,execute,filetype
  
          if filetype eq 'd' then begin
            
             list = findfile(file[0])
             if strtrim(info.inval.file[0]) ne '' then begin
               if strtrim(list[0]) ne '' then list = ['..',list] $
                                         else list = ['..']
             endif
             
	     
	     ; Do not display CVS directories
             if (where(list eq 'CVS'))[0] ne -1 then list=list[where(list ne 'CVS')]

	     widget_control,info.selID,set_value=list
             Widget_control, info.browseID, sensitive=0
         
          endif else begin
             list = *info.list
             Widget_control, info.browseID, sensitive=1
             Widget_control, info.doneID, sensitive=1
          endelse


        endelse
        
       *info.list = list

  End

  ELSE :
  
ENDCASE


CASE userButton OF

  'Cancel' : begin
               formdata = {cancel:1}
               *info.ptrToFormData = formdata
               widget_Control, event.top, /destroy
             end 

      
  'Done'   : begin
                 
                ; gather the data for return to calling program
                ; 'Done' is not sensitised until valid filenames
                ; have been chosen.
               
                
                if info.many EQ 0 then begin
                   file = info.inval.rootpath + info.inval.file[0]
                endif else begin
                   path = acquire_path(info.inval.file[0])
                   list = widget_info(info.selID, /list_select)
                   file = info.inval.rootpath + path + '/' + (*info.list)[list]
                endelse
                                
                ; do some checking
                
                err   = 0
                for j = 0, n_elements(file)-1 do begin
                   file_acc,file[j],fileexist,read,write,execute,filetype
                   if err EQ 0 AND filetype NE '-' then begin
                      err = 1
                      mess = 'Choose a file not a directory'
                   endif
                endfor
                
                
                               
                               
                if err EQ 0 then begin
                   formdata = {path     : info.inval.rootpath, $
                               file     : file,                $
                               cancel   : 0                    }
                    *info.ptrToFormData = formdata
                    widget_control, event.top, /destroy
                endif else begin
                   widget_Control, info.messID, Set_Value=mess
                endelse

             End
             
    'Browse Comments' : Begin
    
                if info.many EQ 0 then begin
                   file = info.inval.rootpath + info.inval.file[0]
                endif else begin
                   path = acquire_path(info.inval.file[0])
                   list = widget_info(info.selID, /list_select)
                   file = info.inval.rootpath + path + '/' + *(info.list)[list]
                endelse
                                
                for j = 0, n_elements(file)-1 do begin
                   xxtext, file[j], font=info.font_large
                endfor
    
             End        
  ELSE : 
                   
ENDCASE

; Update the info.inval for those events which modify it.

if count EQ 0 then Widget_Control, event.top, Set_UValue=info


END
  
;-----------------------------------------------------------------------------


PRO ADAS_IN_V2, inval, action,           $
                PARENT     = parent,     $
                WINTITLE   = wintitle,   $
                TITLE      = title,      $
                FONT_LARGE = font_large, $
                FONT_SMALL = font_small, $
                MANY       = many

                ;**** Set defaults for keywords ****

  IF NOT (KEYWORD_SET(wintitle)) THEN wintitle = 'ADAS INPUT FILE'
  IF NOT (KEYWORD_SET(title))    THEN title = ''
  IF n_elements(font_large) EQ 0 THEN font_large = ''
  IF n_elements(font_small) EQ 0 THEN font_small = ''
  IF NOT (KEYWORD_SET(many))     THEN many = 0


; Store central and user root values from inval

  centroot = inval.centroot
  userroot = inval.userroot
    

; Path names may be supplied with or without the trailing '/'.  Add
; this character where required so that USERROOT will always end in
; '/' on output.
  

  if strtrim(inval.rootpath) eq '' then begin
      inval.rootpath = './'
  endif else if                                                   $
  strmid(inval.rootpath, strlen(inval.rootpath)-1,1) ne '/' then begin
      inval.rootpath = inval.rootpath+'/'
  endif

 
; Make this widget modal if keyword parent is set. Otherwise assume
; that it stands alone.
                
  if n_elements(parent) eq 0 then begin
     topbase = Widget_Base(Column=1, Title=wintitle, XOFFSET=100, YOFFSET=1)
  endif else begin
     topbase = Widget_Base(Column=1, Title=wintitle, XOFFSET=100, YOFFSET=1, $
                Modal=1, Group_Leader=parent)
  endelse


  rc     = widget_label(topbase,value='  ',font=font_large)

  base   = widget_base(topbase, /column)
  
 
  
; File selection widget

  flbase = widget_base(base, /column, /frame)

  fltitle = widget_label(flbase, font=font_large, value=title)
                        
                        
; Root path name widget

  rootval = { ROOTPATH  :  inval.rootpath, $
              CENTROOT  :  inval.centroot, $
              USERROOT  :  inval.userroot  }
  rootID  = cw_adas_root(flbase, value=rootval, font=font_large)



; File selection widget
; File name window (for one only) and the select window  

  if n_elements(ysize) le 0 then ysize = 15
  machine = GETENV('TARGET_MACHINE')
  if machine eq 'HPUX' then begin
      ysize = ysize - 2
  endif 


  if many EQ 0 then begin
     fle    = inval.file
     fileID = widget_text(flbase,value=fle,xsize=xsize,font=font_large)
     selID  = widget_list(flbase,ysize=ysize,font=font_large)   
  endif else begin
     file = inval.rootpath + inval.file[0]
     file_acc,file,fileexist,read,write,execute,filetype
     if filetype eq 'd' then fle = inval.file[0] $
                        else fle = acquire_path(inval.file[0])
     fileID = widget_text(flbase,value=fle,xsize=xsize,font=font_large)
     selID  = widget_list(flbase,ysize=ysize,font=font_large,/multiple)
  endelse
  

; Is the current 'file' is a file or a directory?
; if not extract directory path. In any case get the list of files 
; in the directory.

  listdir = inval.rootpath+inval.file[0]
  file_acc,listdir,fileexist,read,write,execute,filetype

  if filetype NE 'd' then listdir = acquire_path(listdir)

  list = findfile(listdir)

; If the current directory is not 'root' add the '..' symbol to the list.  

  if strtrim(inval.file[0]) ne '' then $
     if strtrim(list[0]) ne '' then list = ['..',list] $
                               else list = ['..']

; Do not display CVS directories ****
  if (where(list eq 'CVS'))[0] ne -1 then list=list[where(list ne 'CVS')]

  widget_control, selID, set_value=list

                                                      
                ;*****************
                ;**** Buttons ****
                ;*****************
                
  mrow     = widget_base(topbase,/row,/align_center)
  messID   = widget_label(mrow,font=font_large, $
                          value='                                          ')
                
  mrow     = widget_base(topbase,/row)
  
  browseID = widget_button(mrow, value='Browse Comments', font=font_large)
  cancelID = widget_button(mrow,value='Cancel',font=font_large)
  doneID   = widget_button(mrow,value='Done',font=font_large)




; Initial settings - query first file only.
; Check filenames and desensitise buttons if it is a directory or   
; it is a file without read access.                    

  filename = inval.rootpath+inval.file[0]
  file_acc,filename,fileexist,read,write,execute,filetype
  
  if filetype ne '-' then begin
     widget_control, browseID, sensitive=0
     widget_control, doneID, sensitive=0
  end else begin
    if read eq 0 then begin
       widget_control, browseID, sensitive=0
       widget_control, doneID, sensitive=0
    end
  end



; Realize the input widget.

   widget_Control, topbase, /realize

; Create a pointer to store the information collected from the form.
; The initial data stored here is set to CANCEL, so nothing needs to
; be done if the user kills the widget with the mouse.

   ptrToFormData = Ptr_New({cancel:1})
   ptrToList     = Ptr_New(list)

; Create an info structure with program information.

  info = { fileID          :  fileID,         $
           rootID          :  rootID,         $
           selID           :  selID,          $
           doneID          :  doneID,         $
           cancelID        :  cancelID,       $
           browseID        :  browseID,       $
           messID          :  messID,         $
           many            :  many,           $
           list            :  ptrTolist,      $
           inval           :  inval,          $ 
           font_large      :  font_large,     $
           ptrToFormData   :  ptrToFormData   }  
            
               
; Store the info structure in the user value of the top-level base and launch
; the widget into the world.

  widget_control, topbase, Set_UValue=info

  XManager, 'ADAS_IN_V2', topbase, Event_Handler='ADAS_IN_V2_EVENT'


; When the widget is destroyed, the block is released, and we
; return here. Get the data that was collected by the widget
; and stored in the pointer location. Finally free the pointer.

formdata = *ptrToFormData

action = 'Done'
IF N_Elements(formdata) EQ 0 THEN BEGIN
   Ptr_Free, ptrToFormData
   action = 'Cancel'
ENDIF
 
IF formdata.cancel EQ 1 THEN BEGIN
   Ptr_Free, ptrToFormData
   action = 'Cancel'
ENDIF

if action eq 'Done' then begin

   ; redefine inval for number of files

   n_file  = n_elements(formdata.file)
   
   inval = { rootpath : formdata.path,     $
             file     : strarr(n_file),    $
             centroot : centroot,          $
             userroot : userroot           }
  
   pos = strlen(formdata.path)
   for j = 0, n_file-1  do inval.file[j] = strmid(formdata.file[j],pos)
   
   Ptr_Free, ptrToFormData

endif       



RETURN

END
