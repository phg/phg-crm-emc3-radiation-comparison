; Copyright (c) 1999, Strathclyde University.
; SCCS info: Module @(#)$Header: /home/adascvs/idl/adaslib/gui/cw_adas_file_gr.pro,v 1.1 2004/07/06 12:56:46 whitefor Exp $ Date $Date: 2004/07/06 12:56:46 $
;+
; PROJECT:
;       ADAS 
;
; NAME:
;	cw_adas_file_gr
;
; PURPOSE:
;	A compound widget which asks for the name of a graphics output file,
;       the type of file and the size of paper.
;
; EXPLANATION:
;	This routine pops up a window which asks for the information.  
;       'Done' and 'Cancel' buttons complete the interaction.
;
; INPUTS:
;       VALUE - Structure holding file information.
;
; OPTIONAL INPUTS:
;	None.
;
; OUTPUTS:
;       The return value of this function is the ID of the compound
;       widget which is of type LONG integer.
;
; OPTIONAL OUTPUTS:
;       None
;
; KEYWORD PARAMETERS:
;	FONT	- A font to use for all text in this widget.
;
;	VALUE	- input/output information.
;
; CALLS:
;	FILE_GR_GET_VAL    gets the information
;	FILE_GR_EVENT      event handler
;	CW_BGROUP          IDL button group widget
;       CW_ADAS_OUTFILE    ADAS file entry
;
;
; SIDE EFFECTS:
;
; CATEGORY:
;	Widgets
;
; WRITTEN:
;       Martin O'Mullane
;
; MODIFIED:
;	1.1	Martin O'Mullane
;		First release 
;
; VERSION:
;       1.1	17-03-99
;
;-
;-----------------------------------------------------------------------------
FUNCTION file_gr_get_val, id

                ;**** Return to caller on error ****

    ON_ERROR, 2

                ;**** Retrieve the state ****

    parent = widget_info(id, /parent)
    widget_control, parent, get_uvalue = state
  


                ;**** Get text output settings ****

    widget_control, state.fileid, get_value=fout

    os = { os, 	   	                $
           dsn      :  fout.filename,   $
           defname  :  fout.defname,    $
           replace  :  fout.repbut,     $
           devlist  :  state.devlist,   $
           dev      :  state.dev,       $
           paper    :  state.paper,     $
           write    :  fout.outbut,     $
           message  :  fout.message     }

                ;**** Return the state ****

    widget_control, parent, set_uvalue=state, /no_copy

    RETURN, os 

END

;-----------------------------------------------------------------------------
FUNCTION file_gr_event, event


                ;**** parent ID of compound widget ****
               
  parent = event.top

                ;**** Retrieve the state ****

  widget_control, parent, get_uvalue=state, /no_copy 
  
  
               ;**** clear error messages ****

  widget_control, state.messid, set_value=' '
  

CASE event.id OF
  
  state.grtypeid  : begin
                      widget_control, state.grtypeid, get_value=index
                      case index of
                       0 : state.dev = 'Post-Script'
                       1 : state.dev = 'HP-PCL'
                       2 : state.dev = 'HP-GL'
                     endcase
                   end

  state.paperid   :  begin
                      widget_control, state.paperid, get_value=index
                      if  index eq 0 then state.paper = 'A4' $
                                    else state.paper = 'Letter'
                    end
                      
               
  state.doneid    :  begin
  
                       ; Send back the state, get widget value, check for
                       ; errors and then retrieve the state. If an error occurs
                       ; give a message and send a new_event of 0L. This keeps
                       ; the widget alive. Otherwise pass control to the
                       ; calling widget which destroys this compound widget. 
                       
                       widget_control, parent, set_uvalue=state, /no_copy
                       
                       error = 0
                       widget_control, event.handler, get_value=abc
                       mess=''
                       if (abc.write eq 1 and strtrim(abc.message) ne '') $
                          then error=1
                       
                       widget_control, parent, get_uvalue=state, /no_copy
                       
                       if error eq 1 then begin
                           if mess eq '' then mess = '**** Error in output settings ****'
                           widget_control, state.messid, set_value= mess
                           new_event = 0L
                       endif else begin
                           new_event = {ID      : parent,    $ 
                                        TOP     : event.top, $
                                        HANDLER : 0L,        $
                                        ACTION  : 'Done'  }
                       endelse
                       
                     end

  state.cancelid  :  new_event = {ID      : parent,    $
                                  TOP     : event.top, $
                                  HANDLER : 0L,        $
                                  ACTION  : 'Cancel'   }

  else      :  new_event = 0L
                   
ENDCASE


                ;*****************************
                ;*** make state available  ***
                ;*****************************

    widget_control, parent, set_uvalue=state, /no_copy
    
    RETURN, new_event


END

;-----------------------------------------------------------------------------


FUNCTION cw_adas_file_gr, parent, VALUE = value, FONT   = font, $
                                  TITLE = title, UVALUE = uvalue 


		;**** Set defaults for keywords ****

  IF NOT (KEYWORD_SET(font))   THEN font  = ''
  IF NOT (KEYWORD_SET(title))  THEN title = 'Enter a filename'
  IF NOT (KEYWORD_SET(uvalue)) THEN uvalue = 0

  IF NOT (KEYWORD_SET(value)) THEN begin
                     os = {os,		 $
                                dsn     : '',            $
                                defname : 'adas_gr.ps',  $
                                replace :  0,            $
                                devlist :  devlist,      $
                                dev     :  'Post-Script',$
                                paper   :  'A4',         $
                                write   :  1,            $
                                message :  ' '           }
                end else begin
                     os = {os, 		    $
                                dsn     :  value.dsn ,      $
                                defname :  value.defname,   $
                                replace :  value.replace,   $
                                devlist :  value.devlist,   $
                                dev     :  value.dev,       $
                                paper   :  value.paper,     $ 
                                write   :  value.write,     $ 
                                message :  value.message    }
                                
                endelse

               
                ;*********************************
		;**** create main base widget ****
                ;*********************************
                

  cwid = WIDGET_BASE(parent, UVALUE = uvalue,       $
                EVENT_FUNC     = "file_gr_event",   $
                FUNC_GET_VALUE = "file_gr_get_val", $
                /COLUMN)
                
  
                

                ;**** A title ****
                
  rc = widget_label(cwid,value=title,font=font)


                ;********************************
                ;**** Widget for file output ****
                ;********************************

  base = widget_base(cwid,/column,/frame)
  

                ;**** Widget for text output ****

  filesel = { OUTBUT   :  1,         $
              APPBUT   :  -1,        $
              REPBUT   :  1,         $
              FILENAME : os.dsn,     $
              DEFNAME  : os.defname, $
              MESSAGE  : os.message  }

  fileid = cw_adas_outfile(base, VALUE=filesel, FONT=font)


                ;*************************
                ;**** Graphic options ****
                ;*************************
  

  grtypeid = cw_bgroup(base,os.devlist,exclusive=1,row=1,		$
                       label_left = 'Graphic file :',			$
                       /no_release,  font=font, Uvalue='GRTYPE')

  paper_list = ['A4', 'Letter']
  paperid = cw_bgroup(base,paper_list,exclusive=1,row=1,		$
                       label_left = 'Paper type   :',			$
                       /no_release,  font=font, Uvalue='PAPER')



                ;*****************
                ;**** Message ****
                ;*****************
                
  messid = widget_label(base,value='              ',font=font)
    
  
                ;*****************
		;**** Buttons ****
                ;*****************
                
  base     = widget_base(cwid,/row)
  doneid   = widget_button(base,value='Done',font=font,Uvalue='DONE')
  cancelid = widget_button(base,value='Cancel',font=font,Uvalue='CANCEL')
  
  
  
                ;**************************
		;**** initial settings ****
                ;**************************
                
  if os.paper eq 'A4' $ 
       then widget_control, paperid, set_value = 0  $           
       else widget_control, paperid, set_value = 1           
   
  widget_control, grtypeid, set_value = 0


                ;**********************************
                ;**** create a state structure ****
                ;**********************************

  new_state = {  fileid   :  fileid,    $ 
                 paper    :  os.paper,  $
                 devlist  :  os.devlist,$
                 dev      :  os.dev,    $
                 grtypeid :  grtypeid,  $
                 paperid  :  paperid,   $
                 doneid   :  doneid,    $
                 cancelid :  cancelid,  $
                 messid   :  messid     }
  
  
                 ;**** Save initial state structure ****
 
  widget_control, parent, set_uvalue = new_state, /no_copy

  return, cwid 

END
