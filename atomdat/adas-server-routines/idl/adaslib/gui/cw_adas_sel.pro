; Copyright (c) 1995 Strathclyde University .
; SCCS info: Module @(#)$Header: /home/adascvs/idl/adaslib/gui/cw_adas_sel.pro,v 1.8 2004/07/06 13:02:27 whitefor Exp $ Date $Date: 2004/07/06 13:02:27 $
;
;+
; PROJECT:
;       ADAS IBM MVS to DEC UNIX conversion
;
; NAME:
;	CW_ADAS_SEL()
;
; PURPOSE:
;	Widget for viewing current selections and invoking a selection
;	list window.
;
; EXPLANATION:
;	This compound widget provides a single entry point to the ADAS
;	multiple selection list features.  The widget consists of a
;	title, a text widget showing the items currently selected and
;	a button which can be used to invoke the multiple selection list
;	in order to modify the selections.
;
;	This widget generates an event whenever the 'Selections' button
;	has been pressed and a new set of selections completed.  The
;	event structure is;
;
;	{ID:0L, TOP:0L, HANDLER:0L, ACTION:''}
;
;	Where ACTION will have the value 'multisel'.
;
; USE:
;	The following code adds a selection list widget to a base.  The
;	list of selected items includes 'Red' and 'Blue' at realization.
;	The maximum number of selections allowed is set at 3 since that
;	is the length of the 'selected' array.  Press the 'Selections'
;	button to modify the selections.
;
;	    list = ['Red','Green','Blue','Black','Brown','Grey']
;	    selected = [0,2,-1]
;	    title = 'Colour Selections'
;	    base = widget_base(/column)
;	    selid = cw_adas_sel(base,list,selected=selected,title=title)
;	    widget_control,base,/realize
;	    rc=widget_event()
;	    widget_control,selid,get_value=selected
;	    print,'Colours Selected are',list(selected(where(selected ge 0)))
;
; INPUTS:
;       PARENT   - The ID of the parent widget.
;
;	LIST	 - List of items for the selection list.  A string array
;		   with one element per list item.
;
; OPTIONAL INPUTS:
;	None.
;
; OUTPUTS:
;       The return value of this function is the ID of the compound
;       widget which is of type LONG integer.
;
; OPTIONAL OUTPUTS:
;       None
;
; KEYWORD PARAMETERS:
;       SELECTED - One dimensional integer array of the indices of the
;                  items in LIST which are to be set as selected when
;                  first displayed.  The length of SELECTED is the
;                  maximum number of selections allowed.  Extra spaces
;                  can be reserved in SELECTED with values of -1.  All
;                  -1 values should be at the end of the array.
;                  For example SELECTED=[0,5,-1,-1] will give a maximum
;                  of 4 selections and items 0 and 5 from list are
;                  selected on start up.  The default is no selections
;                  set on startup.
;
;       MAXSEL   - Maximum number of selections allowed.  The default is
;                  one unless the SELECTED keyword is specified in which
;                  case the value of MAXSEL is ignored.
;
;	ORDERED  - When the ordered keyword is non-zero the list of 
;		   selections in the SELECTED array is arranged in order
;		   of ascending value regardless of the order in which 
;		   the list items were chosen.  The -1 values used to
;		   pad the selected array still remain at the end.
;		   For example if the ordered keyword is not set the
;		   values in selected will match the order in which the
;		   items were selected from LIST e.g [3,1,5,-1,-1].
;		   With the ordered keyword non-zero the SELECTED array
;		   would be [1,3,5,-1,-1].
;
;       TITLE    - Title to appear on the selection list window banner.
;                  The default is 'Make your Selections'.
;
;       FONT     - The text font to be used for the window.  The default
;                  is the current X default.
;
;	UVALUE	 - User value for the compound widget.
;
;	YTEXSIZE - Allows user to explicitly set the ysize of the text
;		   widget. If not set (or set to an invalid value) then
;		   the default setting is the maximum number of choices
;		   allowed.
;
; CALLS:
;       CW_LOADSTATE    Recover compound widget state.
;       CW_SAVESTATE    Save compound widget state.
;	MULTI_SEL	Pop-up multiple selection list window.
;       See side effects for other related widget management routines.
;
; SIDE EFFECTS:
;	This widget uses a COMMON BLOCK: CW_ADASSEL_BLK to hold the
;       widget state.
;
;       Three other routines are included which are used to manage the
;       widget;
;
;	ADASSEL_SET_VAL
;	ADASSEL_GET_VAL()
;	ADASSEL_EVENT()
;
; CATEGORY:
;	Compound Widget?
;
; WRITTEN:
;       Andrew Bowen, Tessella Support Services plc, 5-Apr-1993
;
; MODIFIED:
;       1.1  	Andrew Bowen    
;               First release.
;	1.2	Andrew Bowen
;		Fixed to accept zero selections.
;	1.3	Andrew Bowen
;		Header comments added.
;	1.4	Andrew Bowen
;		New selection list keyword ORDERED
;	1.5	Lalit Jalota
;	1.6	Lalit Jalota
;	1.7	Tim Hammond
;		Added new keyword YTEXSIZE
;	1.8	Richard Martin
;		Removed obsolete cw_loadstate/savestate statements.
;
; VERSION:
;	1.1	06-04-93
;	1.2	14-04-93
;	1.3	02-06-93
;	1.4	11-10-93
;	1.5	13-03-95
;	1.6	13-03-95
;	1.7	26-02-96
;	1.8	10-01-02
;
;-
;-----------------------------------------------------------------------------

PRO adassel_set_val, id, selected


		;**** Return to caller on error ****
  ON_ERROR, 2

		;**** Retrieve the state ****

  first_child = widget_info(id,/child)
  widget_control, first_child, get_uvalue=state, /no_copy

		;**** The current selections ****
  selsind = where(selected gt -1)
  if selsind(0) ge 0 then currsels = state.list(selected(selsind)) $
							else currsels = ''
  widget_control,state.curselid,set_value=currsels

		;**** Copy the new value to state structure ****
  state.selected = selected

  widget_control, first_child, set_uvalue=state, /no_copy

END

;-----------------------------------------------------------------------------


FUNCTION adassel_get_val, id


		;**** Return to caller on error ****
  ON_ERROR, 2

		;**** Retrieve the state ****

  first_child = widget_info(id,/child)
  widget_control, first_child, get_uvalue=state, /no_copy

  selected=state.selected
  widget_control, first_child, set_uvalue=state, /no_copy

  RETURN, selected

END

;-----------------------------------------------------------------------------

FUNCTION adassel_event, event


		;**** Base ID of compound widget ****
  base=event.handler

		;**** Retrieve the state ****

  first_child = widget_info(base, /child)
  widget_control, first_child, get_uvalue = state, /no_copy

		;************************
		;**** Process Events ****
		;************************
  CASE event.id OF

    state.selectid: begin

      action = 'multisel'
      selected = multi_sel(state.list, SELECTED=state.selected, $
				MAXSEL=state.maxsel, ORDERED=state.ordered, $
				TITLE=state.title, FONT=state.font)

		;**** The current selections ****
      selsind = where(selected gt -1)
      if selsind(0) ge 0 then currsels = state.list(selected(selsind)) $
							 else currsels = ''
      widget_control,state.curselid,set_value=currsels

		;**** Copy the new value to state structure ****
      state.selected = selected

      widget_control, first_child, set_uvalue=state, /no_copy
    
    end

  ENDCASE

  RETURN, {ID:base, TOP:event.top, HANDLER:0L, ACTION:action}
END

;-----------------------------------------------------------------------------

FUNCTION cw_adas_sel, topparent, list, SELECTED=selected, 			$
         MAXSEL=maxsel, ORDERED=ordered, TITLE=title, FONT=font, 	$
	 UVALUE=uvalue, YTEXSIZE=ytexsize


  IF (N_PARAMS() LT 2) THEN MESSAGE, 'Must specify PARENT and LIST'+ $
						' for cw_adas_sel'

                ;**** Set defaults for keywords ****
  IF NOT (KEYWORD_SET(maxsel)) THEN maxsel = 1
  IF NOT (KEYWORD_SET(selected)) THEN $
                        selected = make_array(maxsel,/int,value=-1)
  IF NOT (KEYWORD_SET(ordered)) THEN ordered = 0
  IF NOT (KEYWORD_SET(title)) THEN title = ' '
  IF NOT (KEYWORD_SET(font)) THEN font = ''
  IF NOT (KEYWORD_SET(uvalue))  THEN uvalue = 0

                ;**** Over-ride MAXSEL with the length of SELECTED ****
                ;**** this is in case they are different.          ****

  IF (KEYWORD_SET(selected)) THEN begin
    seldim = size(selected)
    maxsel = seldim(1)
  end
  IF (KEYWORD_SET(ytexsize)) THEN begin
    if ytexsize gt maxsel then begin
	ytsize = maxsel
    endif else begin
	ytsize = ytexsize
    endelse
  ENDIF ELSE BEGIN
    ytsize = maxsel
  ENDELSE

		;**** Create the main base for the widget ****
  parent = WIDGET_BASE(topparent, UVALUE = uvalue, $
		EVENT_FUNC = "adassel_event", $
		FUNC_GET_VALUE = "adassel_get_val", $
		PRO_SET_VALUE = "adassel_set_val", $
		/COLUMN)

		;**** Create base to hold the value of state ****

  first_child = widget_base(parent)

  cwid = widget_base(first_child,/column)
    
		;**** Title ****
  rc = widget_label(cwid,value=title,font=font)

		;**** The current selections ****
  selsind = where(selected gt -1)
  if selsind(0) ge 0 then currsels = list(selected(selsind)) $
							else currsels = ''
  maxlen = max(strlen(list))
  curselid = widget_text(cwid,value=currsels,XSIZE=maxlen,YSIZE=ytsize, $
			 font=font, /scroll)

		;**** The edit button ****
  selectid = widget_button(cwid,value='Selections',font=font)

		;**** Create state structure ****
  new_state =  {CURSELID:curselid, SELECTID:selectid, LIST:list, $
		SELECTED:selected, MAXSEL:maxsel, ORDERED:ordered, $
		TITLE:title, FONT:font}

		;**** Save initial state structure ****

  widget_control, first_child, set_uvalue=new_state, /no_copy

  RETURN, parent

END
