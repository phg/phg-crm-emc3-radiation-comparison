; Copyright (c) 1995 Strathclyde University .
; SCCS info: Module @(#)$Header: /home/adascvs/idl/adaslib/gui/adas_text.pro,v 1.2 2004/07/06 11:14:06 whitefor Exp $    Date $Date: 2004/07/06 11:14:06 $
;+
; PROJECT:
;       ADAS 
;
; NAME:
;	ADAS_TEXT
;
; PURPOSE:
;	A popup window for browsing a string array with the option
;       of printing the text to a file.
;
; EXPLANATION:
;	This routine pops up a window which uses a scrollable text widget to
;	display the input string array.  'Done' and 'Write to file'
;       buttons are included in the widget for the user to complete the 
;       browsing and to (optionally) save the results to a file for printing.
;
; INPUTS:
;       browsestr - string array to browse/print.
;       def_file  - default file name.
;
; OPTIONAL INPUTS:
;	None.
;
; OUTPUTS:
;       The return value of this function is the ID of the compound
;       widget which is of type LONG integer.
;
; OPTIONAL OUTPUTS:
;       None
;
; KEYWORD PARAMETERS:
;	XSIZE	  - Integer; x size in characters of browse widget.
;		    Default is 50.
;
;	YSIZE	  - Integer; y size in characters of browse widget.
;		    Default is 20.
;
;	FONT	  - A font to use for all text in this widget.
;
;	TITLE	  - Description of widget.
;
;	WINTITLE  - Title of window.
;
; CALLS:
;	CW_ADAS_OUTFILE    output dialog for patch file
;
; SIDE EFFECTS:
;	One other routine is included which is used to manage the widget 
;
;	ADAS_TEXT_EVENT
;
; CATEGORY:
;	Widgets
;
; WRITTEN:
;       Martin O'Mullane
;
; MODIFIED:
;	1.1	Martin O'Mullane
;		First release 
;	1.2	Martin O'Mullane
;		Made writing to file a parameter. The default is to write
;               a file to keep backward compatibility.
;
; VERSION:
;       1.1	17-03-99
;       1.2	20-08-99
;-
;-----------------------------------------------------------------------------

PRO adas_text_event, event

 Widget_Control, event.top, Get_UValue=state
 Widget_Control, event.id,  Get_UValue=buttonEvent

CASE buttonEvent OF
  'DONE'  : widget_control, event.top, /destroy
  'WRITE' : begin
              widget_control,state.patch, get_value=fval
              if fval.outbut eq 1 then begin
                 if fval.appbut ne 1 then begin
                    openw,lun,fval.filename,/get_lun
                    fval.appbut = 0
                 endif else begin
                    openw,lun,fval.filename,/get_lun,/append
                 endelse
                 
                 for j = 0,n_elements(state.text)-1 do begin
                    printf, lun, state.text(j)
                 endfor
                 
                 free_lun,lun                 
                 fval.message='Output written to file'
                 widget_control,state.patch, set_value=fval
              endif
 
            end
                   
ENDCASE

END

;-----------------------------------------------------------------------------


PRO adas_text, browsestr, def_file, XSIZE = xsize, YSIZE = ysize, FONT = font, $
                                    TITLE = title, WINTITLE = wintitle,        $
                                    WRITEFILE = writefile


		;**** Set defaults for keywords ****

  IF NOT (KEYWORD_SET(xsize))     THEN xsize = 50
  IF NOT (KEYWORD_SET(ysize))     THEN ysize = 20
  IF NOT (KEYWORD_SET(font))      THEN font = ''
  IF NOT (KEYWORD_SET(title))     THEN title = ''
  IF NOT (KEYWORD_SET(wintitle))  THEN wintitle = 'ADAS TEXT'
  IF NOT (KEYWORD_SET(writefile)) THEN writefile = 'YES'


                ;***********************************
		;**** create titled base widget ****
                ;***********************************
                
  parent = widget_base(title=wintitle, xoffset=100, yoffset=100, /column)


		;**** Title ****
                
  rc = widget_label(parent,value=title,font=font)


		;**** Default case in text widget ****
                
  errstr='**** NOTHING TO BROWSE ****'
                
  numlines = n_elements(browsestr)
  if numlines ne 0                $
     then textstr = browsestr     $
     else textstr = errstr  


                 
                ;*****************************************
		;**** Text widget to display comments ****
                ;*****************************************

  textid = widget_text(parent,value=textstr,/scroll,xsize=xsize,		$
			ysize=ysize,font=font,/no_copy)
   

                ;**************************************
                ;**** Widget for patch file output ****
                ;**************************************

    outfval = { OUTBUT   : 1,        $
                APPBUT   : -1,       $
                REPBUT   : 1,        $
                FILENAME : '',       $
                DEFNAME  : def_file, $
                MESSAGE  : ' '       }
                
    base   = widget_base(parent,/row,/frame)
    if writefile EQ 'YES' then begin
         patch  = cw_adas_outfile(base, OUTPUT='File Output',   $
                                        VALUE=outfval, FONT=font)
    endif else patch = 0L


                ;*****************
		;**** Buttons ****
                ;*****************
                
  base    = widget_base(parent,/row)
  doneid  = widget_button(base,value='Done',font=font,Uvalue='DONE')
  if writefile EQ 'YES' then writeid = widget_button(base,value='Write to file',font=font,Uvalue='WRITE')
  
                ;***************************************************
		;**** put text and patch file info into a state ****
                ;***************************************************
   
  state = {  patch  :  patch,    $ 
             text   :  textstr   }
  
  
                ;****************************
		;**** realize the widget ****
                ;****************************

  dynlabel, parent
  widget_control,parent,/realize, set_uvalue=state


		;**** make widget modal ****
                
  xmanager,'adas_text',parent,event_handler='adas_text_event',/modal,/just_reg
 

END
