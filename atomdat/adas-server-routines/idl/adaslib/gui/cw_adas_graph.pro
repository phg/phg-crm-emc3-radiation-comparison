; Copyright (c) 1995 Strathclyde University 
; SCCS info: Module @(#)$Header: /home/adascvs/idl/adaslib/gui/cw_adas_graph.pro,v 1.20 2004/11/29 11:10:30 allan Exp $ Date $Date: 2004/11/29 11:10:30 $
;
;+
; PROJECT:
;       ADAS IBM MVS to   UNIX conversion
;
; NAME:
;	CW_ADAS_GRAPH()
;
; PURPOSE:
;	Widget for displaying a sequence of ADAS plots.
;
; EXPLANATION:
;	This widget is a convenient collection of a graphics window
;	and a number of graphical control buttons.  No actual graphics
;	are included with the widget.  This widget simply generates
;	events each time one of the control buttons is pressed.
;	The event returned is the structure;
;
;	{ID:0L, TOP:0L, HANDLER:0L, ACTION:''}
;
;	Where ACTION may have one of five values;
;
;	'previous', 'next', 'print', 'printall' or 'done'.
;
; USE:
;	An example of how to use the widget.
;
;		base = widget_base()
;		graphid = cw_adas_graph(base,/print,/multiplot)
;		widget_control,base,/realize
;		widget_control,graphid,get_value=value
;		win = value.win
;
;	Then use WSET,win to set the widget to receive graphics output.
;	Use XMANAGER or WIDGET_EVENT() to process the button events from
;	the widget.
;
; INPUTS:
;       PARENT   - The ID of the parent widget.
;
; OPTIONAL INPUTS:
;       None.  See the keywords for additional controls.
;
; OUTPUTS:
;       The return value of this function is the ID of the compound
;       widget which is of type LONG integer.
;
; OPTIONAL OUTPUTS:
;       None
;
; KEYWORD PARAMETERS:
;	VALUE	 - A structure which is the value of this widget;
;		   {WIN:0, MESSAGE:''}
;
;		   WIN	   An integer; The IDL window number of the graphical
;			   output area.
;
;		   MESSAGE A string; A message to be displayed in the widget.
;
;		   WIN is only relevant when the value of the widget
;		   is recovered using GET_VALUE.  The value of WIN is
;		   ignored when cw_adas_graph is first created or
;		   SET_VALUE is used.
;
;	MULTIPLOT- Set this keyword to get the 'Next' and 'Previous' buttons.
;		   By default MULTIPLOT is not set.
;
;	PRINT	 - Set this keyword to get the 'Print' button.  If multiplot
;		   is also set a 'Print All' button will also be included on
;		   the widget.  By default PRINT is not set.
;
;	XSIZE	 - The xsize of the graphics area in pixels.  Default
;		   850 pixels.
;
;	YSIZE	 - The ysize of the graphics area in pixels.  Default
;		   750 pixels.
;
;	TITLE	 - A title to appear at the top of the widget.
;
;	FONT	 - The font to be used for text in the widget.  Default
;		   to current screen font.
;	
;	NODONE	 - Set to 1 if 'done' button not required.
;
;	LOWERBASE- Set to 1 if lower-base not required.
;
; CALLS:
;	LOCALIZE	Routine to set the various position parameters
;			used in output plotting.
;
; SIDE EFFECTS:
;	This widget uses a COMMON BLOCK: CW_ADASGR_BLK to hold the
;       widget state.
;
;       Three other routines are included which are used to manage the
;       widget;
;
;	ADAS_GR_SET_VAL
;	ADAS_GR_GET_VAL()
;	ADAS_GR_EVENT()
;
; CATEGORY:
;	Compound Widget
;
; WRITTEN:
;       Andrew Bowen, Tessella Support Services plc, 2-Apr-1993
;
; MODIFIED:
;       1.1 	Andrew Bowen    
;               First release.
;       1.2 	Andrew Bowen    
;		Re-modelling of widget.
;       1.3 	Andrew Bowen    
;		Message widget added.
;       1.4 	Andrew Bowen    
;		Header comments added.
;       1.5 	Andrew Bowen    
;		Header changed.  Bug in example in USE section.
;       1.6 	Andrew Bowen    
;		Changed default aspect ratio of plot window to allow for
;		fatter characters on OSF/IDL.
;	1.7	Unknown
;	1.8	Lalit Jalota
;	1.9	Lalit Jalota
;	1.10	Tim Hammond	
;		Added keyword BITBUTTON which allows the addition of a button
;		with a bitmapped label if required
;	1.11	Tim Hammond/Ed Mansky (ORNL)
;		Tidied up comments and added new sections to customise output
;		display for different machines - currently only different
;		for HP-UX machines. The procedure localize is called to set
;		various parameters used in the positioning of the output plot.
;	1.12	William Osborn
;		Removed xsize=n keyword from widget_label command.
;		The dynlabel procedure did not override these
;		assignments and they were taken as being values in pixels.
;	1.13	William Osborn
;		Added 'Done but retain' button
;	1.14	William Osborn
;		Added keepid=0L if keep is not selected
;       1.15    William Osborn
;               Added dynlabel
;       1.16    David H.Brooks
;               Added nodone and nolowerbase options as keywords and flag
;               notitle to cut out space if no title is given.
;	1.17	Richard Martin
;		Added adjust & retain buttons/keywords.
;	1.18	Martin O'Mullane
;		 - Added mouse_active keyword to turn on mouse activity
;                  from the plot window.
;		 - Added backstore keyword to change backing store method.
;                - Removed common block and replaced by passing structures.
;	1.19	Martin O'Mullane
;		 - Make retain=2 the default backing store behaviour.
;	1.20	Allan Whiteford
;		 - Routine had forked at 1.17, Hugh Summers added to his
;                  version: "Added contraction of graph area when
;                  VERY_SMALL environment." on 18-06-01. Changes
;                  reimplemented here.
;
; VERSION:
;       1.1     05-05-1993
;	1.2	11-05-1993
;	1.3	20-05-1993
;	1.4	02-06-1993
;	1.5	15-06-1993
;	1.6	15-06-1993
;	1.7	21-02-1995
;	1.8	13-03-1995
;	1.9	13-03-1995
;	1.10	07-11-1995
;	1.12	01-08-1996
;	1.13	07-08-1996
;	1.14	07-08-1996
;	1.15	04-10-1996
;	1.16	09-04-1997
;	1.17	13-10-1999
;	1.18	10-11-1999
;	1.19	04-02-2004
;	1.20	24-11-2004
;-
;-----------------------------------------------------------------------------

PRO adas_gr_set_val, id, value

		;**** Return to caller on error ****
  ON_ERROR, 2

		;**** Retrieve the state ****
  
  first_child = widget_info(id, /child)
  widget_control, first_child, get_uvalue = state ,/no_copy

		;**** Update value in state ****
  state.grval.message = value.message

		;**** Update screen message ****
  widget_control,state.messid,set_value=value.message
  
  
		;**** Save new state structure ****

    widget_control, first_child, set_uvalue=state, /no_copy

END

;-----------------------------------------------------------------------------


FUNCTION adas_gr_get_val, id

		;**** Return to caller on error ****
  ON_ERROR, 2

		;**** Retrieve the state ****

  first_child = widget_info(id, /child)
  widget_control, first_child, get_uvalue = state


		;**** Get IDL window number of the drawing area ****
  widget_control,state.drawid,get_value=value

		;**** Update value ****
  state.grval.win = value

		;**** Return value ****
  RETURN, state.grval

END

;-----------------------------------------------------------------------------

FUNCTION adas_gr_event, event


		;**** Base ID of compound widget ****
  base=event.handler

		;**** Retrieve the state ****

  first_child = widget_info(base, /child)
  widget_control, first_child, get_uvalue=state 


		;**** Clear any message ****
  widget_control,state.messid,set_value=' '
  state.grval.message = ''

		;************************
		;**** Process Events ****
		;************************
  mouse_event = 0
  
  CASE event.id OF

    state.previd:	action = 'previous'

    state.nextid:	action = 'next'

    state.printid:	action = 'print'

    state.printallid:	action = 'printall'

    state.doneid:	action = 'done'

    state.keepid:	action = 'done_but_keep'

    state.adjustid:	action = 'adjust'    
    
    state.retainid:	action = 'retain'

    state.bitid:	action = 'bitbutton'
    
  ELSE : mouse_event = 1

  ENDCASE
  drawId = state.drawID
                ;**** Save the new state structure ****
  
  widget_control, first_child, set_uvalue=state, /no_copy

  if mouse_event EQ 0 then begin 
     RETURN, {ID:base, TOP:event.top, HANDLER:0L, ACTION:action}
  endif else begin
     ms_st = { DRAWID  : drawID,         $
               TYPE    : event.TYPE,     $
               X       : event.X,        $
               Y       : event.Y,        $
               PRESS   : event.PRESS,    $
               RELEASE : event.RELEASE,  $
               CLICKS  : event.CLICKS    }
     RETURN, {ID:base, TOP:event.top, HANDLER:0L, ACTION:'mouse',MOUSE:ms_st}
  endelse
END

;-----------------------------------------------------------------------------

FUNCTION cw_adas_graph,	parent, VALUE=value, MULTIPLOT = multiplot, 	$
			PRINT = print, BITBUTTON = bitbutton,		$
			XSIZE = xsize, YSIZE = ysize, TITLE = title, 	$
			FONT = font, KEEP = keep, ADJUST = adjust, RETAIN=retain,$
			NODONE = nodone, NOLOWERBASE = nolowerbase,     $
                        MOUSE_ACTIVE = mouse_active, BACKSTORE = backstore


  IF (N_PARAMS() EQ 0) THEN MESSAGE, 'Must specify a parent for cw_adas_graph'
  ON_ERROR, 2					;return to caller

		;**** Set defaults for keywords ****
  IF NOT (KEYWORD_SET(value)) THEN begin
	grval = {grval, WIN:0, MESSAGE:''}
  end else begin
	grval = {grval, WIN:0, MESSAGE:value.message}
  end
  IF NOT (KEYWORD_SET(multiplot)) THEN multiplot = 0
  IF NOT (KEYWORD_SET(print)) THEN print = 0
  IF NOT (KEYWORD_SET(xsize)) THEN xsize = 850

		;**** Customize YSIZE for different TARGET_MACHINEs ****

  machine = GETENV('TARGET_MACHINE')
  IF (machine EQ 'HPUX') THEN BEGIN
    yheight = 650
  ENDIF ELSE BEGIN
    yheight = 750
  ENDELSE
  IF NOT (KEYWORD_SET(ysize)) THEN ysize = 750
  IF NOT (KEYWORD_SET(title)) THEN BEGIN
      title = ' '
      notitle=1
  ENDIF ELSE BEGIN
      notitle=0
  ENDELSE
  IF NOT (KEYWORD_SET(font)) THEN font = ''
  IF NOT (KEYWORD_SET(keep)) THEN keep = 0 else keep=1
  IF NOT (KEYWORD_SET(nodone)) THEN nodone = 0 else nodone=1
  IF NOT (KEYWORD_SET(nolowerbase)) THEN nolowerbase = 0 else nolowerbase=1
  IF NOT (KEYWORD_SET(bitbutton)) THEN BEGIN
      bitflag=0
  ENDIF ELSE BEGIN
      bitflag=1
  ENDELSE
  IF NOT (KEYWORD_SET(adjust)) THEN adjust= 0 else adjust = 1
  IF NOT (KEYWORD_SET(retain)) THEN retain= 0 else retain = 1


		;**** Allow button events from the drawing pane? ****
  if not (keyword_set(mouse_active)) then mouse_on = 0 else mouse_on  = 1

  if n_elements(backstore) EQ 0 then backing_store = 2 $
                                else backing_store = backstore
      




		;**********************************************************
		;**** Assign parameters defining the size of the graph ****
		;**** in the graphics window in IDL procedure localize ****
		;**********************************************************

  localize

		;**** Create the main base for the graph widget ****

  uvalue = 0
  cwid = WIDGET_BASE(parent, UVALUE = uvalue, 				$
		EVENT_FUNC = "adas_gr_event", 				$
		FUNC_GET_VALUE = "adas_gr_get_val", 			$
		PRO_SET_VALUE = "adas_gr_set_val", 			$
		/COLUMN)
  
		;******************************************************
		;**** Create a dummy widget just to hold value of *****
		;**** "state" variable so as not to get confused  *****
		;**** with any other values. Adopt IDL practice   *****
		;**** of using first child widget                 *****
		;******************************************************

  first_child = widget_base(cwid)
  
  

		;**** Create upper base ****

  upperbase = widget_base(cwid,/row)

		;**** Create lower base ****
  if nolowerbase eq 0 then begin
    lowerbase = widget_base(cwid,/row,space=20)
  end else if nolowerbase eq 1 then begin
    if nodone eq 0 or keep eq 1 or bitflag eq 1 or multiplot ne 0 $
      or print eq 1 then stop,$
  'CW_ADAS_GRAPH ERROR: CHOICE OF KEYWORDS INCOMPATIBLE WITH NOWLOWERBASE'
  end

		;********************************
		;**** The Upper Area         ****
		;********************************

		;**** Graphics area ****

  leftupper = widget_base(upperbase,/column)
  if notitle eq 0 then begin
    rc = widget_label(leftupper,value=title)
  end

  vsmall=GETENV('VERY_SMALL')
  if (vsmall EQ 'YES' ) then begin 
      xsize=0.8*xsize
      ysize=0.8*ysize
  end

  drawid = widget_draw(leftupper,/frame,xsize=xsize,ysize=ysize, $
                       button_events = mouse_on, retain = backstore)


		;********************************
		;**** The lower Area         ****
		;********************************

		;**** Extra bitmapped button if specified ****

  if (bitflag eq 1) then begin
      read_X11_bitmap, bitbutton, bitmap1
      bitid = widget_button(lowerbase, value=bitmap1)
  endif else begin
      bitid = 0L
  endelse

		;**** Previous and Next only needed for multi-plots ****
  if multiplot ne 0 then begin
    previd = widget_button(lowerbase,value='Previous',FONT=font)
    nextid = widget_button(lowerbase,value='Next    ',FONT=font)
  end else begin
    previd = 0L
    nextid = 0L
  end

		;**** Print buttons if enabled ****
  if print eq 1 then begin
    printid = widget_button(lowerbase,value='Print   ',FONT=font)
		;**** Printall only needed for multi-plots ****
    if multiplot ne 0 then begin
      printallid = widget_button(lowerbase,value='Print All',FONT=font)
    end else begin
      printallid = 0L
    end
  end else begin
    printid = 0L
    printallid = 0L
  end

		;**** Done button ****
  if nodone eq 0 then $       
    doneid = widget_button(lowerbase,value='Done',FONT=font) $
  else doneid = 0L 

		;**** 'Done but retain' button ****
  if keep eq 1 then $
    keepid = widget_button(lowerbase,value='Done but retain',FONT=font) $
  else keepid=0L
  
		;**** 'Adjust' button ****
  if adjust eq 1 then $
    adjustid = widget_button(lowerbase,value='Adjust',FONT=font) $
  else adjustid=0L  
  
 		;**** 'Retain' button ****
  if retain eq 1 then $
    retainid = widget_button(lowerbase,value='Retain',FONT=font) $
  else retainid=0L 
  
		;**** Message area ****
  if strtrim(grval.message) ne '' then begin
    message = grval.message
  end else begin
    message=' '
  end
  messid = widget_label(cwid,value=message,font=font)

		;**** Create state structure ****

  new_state =  {DRAWID:drawid, PREVID:previd, BITID:bitid, $
		NEXTID:nextid, PRINTID:printid, PRINTALLID:printallid, $
		DONEID:doneid, MESSID:messid, GRVAL:grval, KEEPID:keepid, $
		RETAINID:retainid, ADJUSTID:adjustid}

		;**** Save initial state structure ****

  widget_control, first_child, set_uvalue=new_state, /no_copy


  dynlabel, parent
  RETURN, cwid

END
