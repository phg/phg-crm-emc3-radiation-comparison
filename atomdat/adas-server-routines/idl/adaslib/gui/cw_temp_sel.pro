; Copyright (c) 1995, Strathclyde University .
; SCCS info: Module @(#)$Header: /home/adascvs/idl/adaslib/gui/cw_temp_sel.pro,v 1.1 2004/07/06 13:09:40 whitefor Exp $ Date $Date: 2004/07/06 13:09:40 $
;
;+
; PROJECT:
;       ADAS IBM MVS to   UNIX conversion
;
; NAME:
;	CW_TEMP_SEL()
;
; PURPOSE:
;	A selection list allowing a single selection and a button to
;	choose between an array of lists to display.
;
; EXPLANATION:
;	This widget is based on the cw_single_sel widget with an
;	additional ability to choose which items are displayed in the list
;	from an	array of lists. It consists of a list widget with a single
;	line text widget above it and a bselector to choose the list.
;	As the user selects an item from the list it appears
;	in the single line text widget.  This means that when the widget
;	is created a 'default' selection can be in place.  Also SET_VALUE
;	can be used to reset the selected item. The bselector allows
;	the user to choose which list to display from an array - typically
;	this can be used to choose between temperature units.
;
;	The 'value' of this widget is a structure consisting of the index
;	of the selected item within the current list and the index of the
;	list from within the array of lists.  An index value of -1
;	indicates no selection.
;
;	The widget also includes a title above the list.
;
;	This widget generates events as items are selected.  The event
;	structure is;
;		{ID:0L, TOP:0L, HANDLER:0L, INDEX:0, LISTIX:0}
;	where INDEX is the index of the item selected from the list and
;	LISTIX is the index of the list in the array of lists.
;
; USE:
;	An example of how to use this widget;
;
;		lists = [['Red','Green','Blue','Black','Yellow','Orange'], $
;			['1','4','5','0','3','2']]
;		base = widget_base()
;		selid = cw_single_sel(base,lists,value={index:2,listix:0}, $
;			    bsel=['Colour','Value'])
;
;	You could use;
;		widget_control,selid,get_value=value
;	to recover the users' selection and
;		widget_control,selid,set_value=value
;	to set the index and list index.
;
; INPUTS:
;       PARENT	- The ID of the parent widget.
;
;	LISTS	- A 2D string array; The lists for selection.
;
;
; OPTIONAL INPUTS:
;	None.
;
; OUTPUTS:
;       The return value of this function is the ID of the compound
;       widget which is of type LONG integer.
;
; OPTIONAL OUTPUTS:
;       None
;
; KEYWORD PARAMETERS:
;	VALUE	- Structure; {Index:Integer,Listix:Integer}
;	          Index is the index of the selected item in LISTS.
;		  An initial value of -1 indicates no selection.
;		  Default value 0, i.e list(0) is the selected item.
;		  Listix is the index to the row in the 2D string
;		  array, LISTS.
;
;	TITLE	- String or string array; a title for the widget.
;		  A sting array gives a multi-line title.
;
;	YSIZE	- The height of the list widget in rows. Default 4.
;		  Scroll bars will appear for lists longer than ysize.
;
;       FONT    - A font to use for all text in this widget.
;
;	BIG_FONT - A font to be used for titles and column headers.
;
;	UVALUE	- A user value for this widget.
;
;  	COLTITLES - A string array containing the titles for the
;		    column.  Can be more than one line.
;
;	BSEL    - A string array of titles for the bselector, one element
;		  for each list in LISTS. Default is an array of the indices.
;
;	BLABEL 	- The label to put to the left of the bselector.
;
; CALLS:
;       See side effects for other related widget management routines.
;
; SIDE EFFECTS:
;       Three other routines are included which are used to manage the
;       widget;
;
;	TSEL_SET_VAL
;	TSEL_GET_VAL()
;	TSEL_EVENT()
;
; CATEGORY:
;	Compound Widget
;
; WRITTEN:
;       William Osborn, Tessella Support Services plc, 3-April-1996
;
; MODIFIED:
;	1.1	William Osborn
;		First version.
; VERSION:
;	1.1	03-04-96
;
;-
;-----------------------------------------------------------------------------

PRO tsel_set_val, id, value

		;**** Return to caller on error ****
    ON_ERROR, 2

		;**** Retrieve the state ****
    first_child = widget_info(id, /child)
    widget_control, first_child, get_uvalue=state, /no_copy

		;**** Check selection in bounds ****
    if (value.listix ge 0 and value.listix lt state.numlist and 	$
    	value.index ge 0 and value.index lt state.numitem) then begin
	widget_control, state.listid, set_value=state.lists(*,value.listix)
	widget_control, state.listid, set_list_select=value.index
    	widget_control, state.selid,					$
		       set_value=state.lists(value.index,value.listix)
    endif else begin
	message, "cw_temp_sel: invalid selection"
    endelse

		;**** Copy value to state ****
    state.select = value

               ;**** Save the new state structure ****

    widget_control, first_child, set_uvalue=state, /no_copy

END

;-----------------------------------------------------------------------------


FUNCTION tsel_get_val, id


		;**** Return to caller on error ****
    ON_ERROR, 2

		;**** Retrieve the state ****
    first_child = widget_info(id, /child)
    widget_control, first_child, get_uvalue=state

		;**** Return the current selection ****

    RETURN, state.select

END

;-----------------------------------------------------------------------------

FUNCTION tsel_event, event

		;**** Base ID of compound widget ****
    base=event.handler

		;**** Retrieve the state ****
    first_child = widget_info(base, /child)
    widget_control, first_child, get_uvalue=state, /no_copy


		;************************
		;**** Process Events ****
		;************************
    CASE event.id OF

    	state.listid: begin

		state.select.index = event.index
		widget_control, state.selid, set_value=state.lists(	$
			         state.select.index,state.select.listix)

    		      end

	state.bselid: begin

		state.select.listix = event.index
		widget_control, state.listid, set_value=state.lists(*,	$
				 state.select.listix)
		widget_control, state.selid, set_value=state.lists(	$
			         state.select.index,state.select.listix)
	        widget_control, state.listid,set_list_select=state.select.index

		      end

    ELSE:

    ENDCASE

    index=state.select.index
    listix=state.select.listix

		;**** Save the state structure ****

    widget_control, first_child, set_uvalue=state, /no_copy

    RETURN, {ID:base, TOP:event.top, HANDLER:0L, INDEX:index,  $
	   LISTIX:listix}
END

;-----------------------------------------------------------------------------

FUNCTION cw_temp_sel, parent, lists, VALUE=value, COLTITLES=coltitles, $
			TITLE=title, YSIZE=ysize, FONT=font, 		$
			BIG_FONT=big_font, UVALUE=uvalue,		$
			BSEL=bsel, BLABEL=blabel


    IF (N_PARAMS() LT 1) THEN MESSAGE, 'Must specify PARENT for cw_adas_ranges'

		;**** Set defaults for keywords ****
    IF NOT (KEYWORD_SET(value)) THEN value = {index:0,listix:0}
    IF NOT (KEYWORD_SET(title)) THEN title = 'Select Item'
    IF NOT (KEYWORD_SET(ysize)) THEN ysize = 4
    IF NOT (KEYWORD_SET(font)) THEN font = ''
    IF NOT (KEYWORD_SET(big_font))  THEN big_font = ''
    IF NOT (KEYWORD_SET(uvalue))  THEN uvalue = 0
    IF NOT (KEYWORD_SET(blabel))  THEN blabel = ''

		;**** Create the main base for the widget ****
    topbase = WIDGET_BASE(parent, UVALUE         = uvalue,		$
				  EVENT_FUNC     = "tsel_event", 	$
				  FUNC_GET_VALUE = "tsel_get_val",	$
				  PRO_SET_VALUE  = "tsel_set_val", 	$
			  	  /COLUMN)

    first_child = widget_base(topbase)

    cwid = widget_base(first_child, /column)

		;**** Find number of items in list ****
    numitem = size(lists)
    if numitem(0) gt 1 then begin
	numlist = numitem(2)
    	numitem = numitem(1)
    endif else begin
	numlist = 1
    	numitem = numitem(1)
    endelse

    if (not keyword_set(bsel))  then bsel=string(fix(findgen(numlist)))
    if (n_elements(bsel) ne numlist) then bsel = bsel(0:numlist-1)
		;**** Find the maximum length of text in list ****
    maxlen = max(strlen(lists))

		;**** Find number of lines in title ****
    numtitle = size(title)
    if numtitle(0) gt 0 then begin
      	numtitle = numtitle(1)
    endif else begin
      	numtitle = 1
    endelse

		;**** Title for list ****
    for i = 0 , numtitle-1 do begin
     	rc = widget_label(cwid,value=title(i),font=big_font)
    endfor

		;**** Column titles for list ****
    if keyword_set(coltitles) then begin
  	numcoltitle = n_elements(coltitles)
     	colid = widget_list(cwid,value=coltitles, $
			    ysize=numcoltitle, font=font $
  		           )
    endif

		;**** The selected item ****
    selid = widget_text(cwid,xsize=maxlen,font=font)

		;**** The selection list ****
    ysize = ysize < numitem
    listid = widget_list(cwid,value=lists(*,value.listix),ysize=ysize,  $
			 font=font)

		;**** The bselector menu ****
    tttbase = widget_base(cwid,/row,/frame)
    lab = widget_label(tttbase,value=blabel,font=font)
    bselid = cw_bselector(tttbase,bsel,set_value=value.listix,font=font)


		;**** Set initial selection if any ***
    if value.index ge 0 and value.index lt numitem then begin
    	widget_control,selid,set_value=lists(value.index,value.listix)
	widget_control,listid, set_list_select=value.index
    endif else begin
    	value.index = -1
    endelse

		;**** Create state structure ****
    new_state = { SELID:selid, LISTID:listid, BSELID:bselid, LISTS:lists, $
		NUMITEM:numitem, NUMLIST:numlist, SELECT:value }

		;**** Save initial state structure ****

    widget_control, first_child, set_uvalue=new_state, /no_copy

    RETURN, topbase

END
