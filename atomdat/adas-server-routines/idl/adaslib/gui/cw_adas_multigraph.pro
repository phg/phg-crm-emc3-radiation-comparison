; Copyright (c) 1999, Strathclyde University .
;
;+
; PROJECT:
;       ADAS
;
; NAME:
;	CW_ADAS_MULTIGRAPH()
;
; PURPOSE:
;	Widget for displaying a multiple ADAS plots.
;
; EXPLANATION:
;	To be documented
;
; WRITTEN:
;      Based on cw_adas_graph
;
; MODIFIED:
;	1.1	Richard Martin  
;		First release.
;	1.2	Richard Martin
;		Moved declaration of charsize
;	1.3	Richard Martin
;		Added support for 24-bit colour
;		Removed obsolete cw_loadstate/savestate statements.
;	1.4	Allan Whiteford
;		Removed color=colour.white from the oplot statement so that
;               plotting to a postscript device worked.
;
; VERSION:
;	1.1	15-10-99
;	1.2	14-04-99
;	1.3	30-01-02
;	1.4	09-11-04
;-
;-----------------------------------------------------------------------------

PRO adas_multgr_set_val, id, value

  COMMON cw_adasmultgr_blk, data, multistruct, xmin,xmax,ymin,ymax,ldef

		;**** Return to caller on error ****
  ON_ERROR, 2

		;**** Retrieve the state ****

    first_child = widget_info(id, /child)
    widget_control, first_child, get_uvalue=state, /no_copy
    
		;**** Update value in state ****
  state.grval.message = value.message

		;**** Update screen message ****
  widget_control,state.messid,set_value=value.message

		;**** Save new state structure ****

    widget_control, first_child, set_uvalue=state, /no_copy
    
END

;-----------------------------------------------------------------------------


FUNCTION adas_multgr_get_val, id

  COMMON cw_adasmultgr_blk, data, multistruct, xmin,xmax,ymin,ymax,ldef

		;**** Return to caller on error ****
  ON_ERROR, 2

		;**** Retrieve the state ****

  first_child = widget_info(id,/child)
  widget_control, first_child, get_uvalue=state

		;**** Get IDL window number of the drawing area ****
  widget_control,state.drawid,get_value=value

		;**** Update value ****
  state.grval.win = value

		;**** Return value ****
  RETURN, state.grval

END

;-----------------------------------------------------------------------------
 PRO adas_multgr_event, event

  COMMON cw_adasmultgr_blk, data, multistruct,xmin,xmax,ymin,ymax,ldef
  COMMON Global_lw_data, left, right, top, bottom, grtop, grright

		;**** Base ID of compound widget ****
  base=event.handler

		;**** Retrieve the state ****

    first_child = widget_info(base,/child)
    widget_control, first_child, get_uvalue=state, /no_copy

		;**** Clear any message ****
  widget_control,state.messid,set_value=' '
  state.grval.message = ''

	;************************
	;**** Set up colours ****
	;************************
		
  TVLCT, red, green, blue, /get

  device,get_visual_depth=depth

  if depth eq 24 then begin
  	  colour = { black   : red(0)+256L*(green(0)+256L*blue(0)),   $
  			 white   : red(1)+256L*(green(1)+256L*blue(1)),   $
  			 red     : red(2)+256L*(green(2)+256L*blue(2)),   $	  
  			 green   : red(3)+256L*(green(3)+256L*blue(3)),   $
  			 blue    : red(4)+256L*(green(4)+256L*blue(4)),   $
  			 yellow  : red(5)+256L*(green(5)+256L*blue(5)),   $
  			 magenta : red(6)+256L*(green(6)+256L*blue(6)),   $
  			 cyan    : red(7)+256L*(green(7)+256L*blue(7))      }
  endif else begin
  	  colour = { black   : 0, $
  			 white   : 1, $
  			 red     : 2, $			 
  			 green   : 3, $
  			 blue    : 4, $
  			 yellow  : 5, $
  			 magenta : 6, $
  			 cyan    : 7    }
  endelse 

		;************************
		;**** Process Events ****
		;************************
  done=0
  print=0
  CASE event.id OF

    state.printid: begin
               	print_val = { dsn     : '',                  $
                             defname : 'multigraph.ps',     $
                             replace :  1,                  $
                             devlist :  data.devlist,       $
                             dev     :  data.devlist(0),    $
                             paper   :  'A4',               $
                             write   :  0,                  $
                             message :  'Note: cannot append to graphics files'}
               	act = 0
               	adas_file_gr, print_val, act, font=data.font, $
                             title='File name for graphical output'

               	print=print_val.write
                             
			end
			
			
    state.doneid:		done=1

    state.keepid:		action = 'done_but_keep'

    state.adjustid: begin
			  adjustid=cw_adjust(data,/dsens,/ysens,$
			  	  /nodata,/multilabels,/notopbase,font=data.font)
			  xmin=adjustid.data.xmin
			  xmax=adjustid.data.xmax
			  ymin=adjustid.data.ymin
			  ymax=adjustid.data.ymax
			  adjustid.data.xmin=data.xmin 
			  adjustid.data.xmax=data.xmax			  
			  adjustid.data.ymin=data.ymin 
			  adjustid.data.ymax=data.ymax			  
			  data=adjustid.data
			  end   				    
    
    state.bitid:		action = 'bitbutton'
        
    ELSE: 	; *** do nothing ***
    
  ENDCASE

  if print eq 1 then begin
      grtype = data.devcode( where(print_val.dev eq print_val.devlist) )
      set_plot,grtype
          device, filename=print_val.dsn
        device, /landscape

  endif else begin
      set_plot,'X'
  endelse

  adasplot, data.x , data.y, data.itval, data.nplots, data.nmx,	    	$
  	 data.title,data.xtitle, data.xunit, data.xunits,data.ytitle,	$
  	 data.strg, data.head1, data.head2a,data.head2b,    			$
  	 data.teva, data.din,		    						$
  	 ldef, xmin, xmax, ymin, ymax,	    					$
  	 data.lfsel, xlog=data.xlog, ylog=data.ylog, /nodata, /multilabels

  for i=0,6 do begin
    if event.id eq state.buttsid(i) then begin
  	 widget_control,state.buttsid(i),get_value=temp 	 
	 if temp(0) eq 1 then multistruct(i).display=1 else multistruct(i).display=0
  	 if event.value eq 1 then begin
  	    if i lt 6 then begin
  	 	for j=i,5 do begin
  	 		multistruct(j)=multistruct(j+1)
  	 		if multistruct(j).key gt 0 then $
  	 			keystring=strtrim(string(multistruct(j).key),2) $
  	 			else keystring=''
  	 		widget_control,state.keyid(j),set_value=keystring
  	 		widget_control,state.descripid(j),set_value=multistruct(j).description
  	 		buttvals=[multistruct(j).display,0]
  	 		widget_control,state.buttsid(j),set_value=buttvals
  	 	endfor  	 	
  	    endif
  	    multistruct(6).x(*)=0.0
  	    multistruct(6).y(*)=0.0  	    
  	    multistruct(6).key=-1  	    
  	    widget_control,state.keyid(6),set_value=''  
  	    multistruct(6).description=''
  	    widget_control,state.descripid(6),set_value=''
  	    multistruct(6).display= 0 	    
  	    buttvals=[0,0]
  	    widget_control,state.buttsid(6),set_value=buttvals  	       	     	    	       	 		
  	 endif
  	 temp=[multistruct(i).display,0]
  	 widget_control,state.buttsid(i),set_value=temp   	 
    endif
    if event.id eq state.keyid(i) then begin
	widget_control,state.keyid(i),get_value=temp
	multistruct(i).key=fix(strtrim(temp(0),2))
    endif
    if event.id eq state.descripid(i) then begin
	widget_control,state.descripid(i),get_value=temp
	multistruct(i).description=temp(0)
    endif
  endfor

            ;*****************************************************
		;**** Suitable character size for current device  ****
		;**** Aim for 60  characters in y direction.     ****
   		;*****************************************************

  charsize = (!d.y_vsize/!d.y_ch_size)/60.0 
  small_check = GETENV('VERY_SMALL')
  if small_check eq 'YES' then charsize=charsize*0.8

  rhs = grright + 0.06
  xyouts,rhs, grtop-0.1,'INDEX     DESCRIPTION' , /normal, alignment=0.0, charsize=charsize  

  count=0
  for i=0,6 do begin
  	if multistruct(i).display eq 1 AND multistruct(i).key ne -1 then begin
  	   count=count+1
  	   oplot, multistruct(i).x(0,*), multistruct(i).y(0,*), linestyle=multistruct(i).key; , color=colour.white
  	   cpp = grtop - 0.1 - 0.05*count
         xyouts, rhs+0.01, cpp, strtrim(string(count),2), /normal, alignment=0.0, charsize=charsize
         xyouts, rhs+0.075, cpp, multistruct(i).description, /normal, alignment=0.0, charsize=charsize
	   if multistruct(i).x(0,data.itval-1) gt data.xmin AND $
	   	    multistruct(i).x(0,data.itval-1) le data.xmax then begin
			if multistruct(i).y(0,data.itval-1) gt data.ymin AND $
	   			multistruct(i).y(0,data.itval-1) le data.ymax then begin
	   			xout=data.itval-1	   			
				yout=data.itval-1
			endif else begin
	   			for j=0,data.itval-1 do begin
	   				if multistruct(i).y(0,j) le data.ymax then begin
	   					xout=j
	   					yout=j
	   				endif
	   			endfor				
			endelse
	   endif else begin
		   	for j=0,data.itval-1 do begin
	   			if multistruct(i).x(0,j) le data.xmax AND multistruct(i).x(0,j) gt 0.0 then begin
	   				xout=j
	   				yout=j
	   			endif
	   		endfor	  			
	   endelse	
	   xyouts, multistruct(i).x(0,xout),multistruct(i).y(0,yout), alignment=1.0, charsize=charsize, '('+strtrim(string(count),2)+')'
	endif
  endfor

  	if print eq 1 then begin
	    message = 'Plot  written to print file.'
	    grval = {WIN:0, MESSAGE:message}
	    widget_control, state.messid, set_value=message
	    device,/close
	    set_plot,'X'
	endif

  widget_control, first_child, set_uvalue=state, /no_copy
  if done eq 1 then begin 
    widget_control,event.top,/destroy  
  endif 


END

;-----------------------------------------------------------------------------

FUNCTION cw_adas_multigraph, datain, VALUE=value,  	$
			PRINT = print, BITBUTTON = bitbutton,		$
			XSIZE = xsize, YSIZE = ysize, TITLE = title, 	$
			FONT = font, KEEP = keep, ADJUST = adjust,$
			NODONE = nodone, NOLOWERBASE = nolowerbase

  COMMON cw_adasmultgr_blk, data, multistruct, xmin,xmax,ymin,ymax,ldef
  COMMON Global_lw_data, left, right, top, bottom, grtop, grright
  
  data=datain

  ON_ERROR, 2					;return to caller

		;**** Set defaults for keywords ****
  IF NOT (KEYWORD_SET(value)) THEN begin
	grval = {grval, WIN:1, MESSAGE:''}
  end else begin
	grval = {grval, WIN:1, MESSAGE:value.message}
  end

  IF NOT (KEYWORD_SET(print)) THEN print = 0
  IF NOT (KEYWORD_SET(xsize)) THEN BEGIN
    	device, get_screen_size=scrsz
    	xsize = scrsz(0)*0.6
    	ysize = scrsz(1)*0.6
  ENDIF

		;**** Customize YSIZE for different TARGET_MACHINEs ****

  machine = GETENV('TARGET_MACHINE')
  IF (machine EQ 'HPUX') THEN BEGIN
    ysize = ysize*0.9
  ENDIF 
  IF NOT (KEYWORD_SET(title)) THEN BEGIN
      title = ' '
      notitle=1
  ENDIF ELSE BEGIN
      notitle=0
  ENDELSE
  IF NOT (KEYWORD_SET(font)) THEN font = ''
  IF NOT (KEYWORD_SET(keep)) THEN keep = 0 else keep=1
  IF NOT (KEYWORD_SET(nodone)) THEN nodone = 0 else nodone=1
  IF NOT (KEYWORD_SET(nolowerbase)) THEN nolowerbase = 0 else nolowerbase=1
  IF NOT (KEYWORD_SET(bitbutton)) THEN BEGIN
      bitflag=0
  ENDIF ELSE BEGIN
      bitflag=1
  ENDELSE
  IF NOT (KEYWORD_SET(adjust)) THEN adjust= 0 else adjust = 1
  IF NOT (KEYWORD_SET(retain)) THEN retain= 0 else retain = 1

		;************************************************************
		;***      Check if $ADASUSER/pass/.multigraph exists 	    ***
		;*** If not create structure with data for multiple plots ***
		;************************************************************

  userpass=GETENV('ADASUSER')
  file=findfile('$ADASUSER/pass/.multigraph')
  
  pos=strpos(data.strg(0),'=')
  len=strlen(data.strg(0))
  stringa=strmid(data.strg(0),pos+1,len)
  pos=strpos(data.strg(3),'=')
  len=strlen(data.strg(3)) 
  stringb=strmid(data.strg(3),pos+1,len)
  description=stringa+'-'+stringb  
  
  if file(0) ne '' then begin	
     restore,file(0) 	  
     if data.ldsel ne multldfit then begin
         if multldfit eq 0 then begin
      	   message=[' ',  						   $
      			'The data for multiple plots is currently',  $
      			'plotted against Temperature. You cannot  ', $
      			'add a plot against Density to the current', $
      			'graph']
         endif else begin
      	   message=[' ',  						   $
      			'The data for multiple plots is currently',  $
      			'plotted against Density.  You cannot add',  $
      			'a plot against Temperature to the current', $
      			'graph']
         endelse
         buttons=['OK']
         messid=popup(message=message,title='Mutilgraph Warning!',buttons=buttons)
         parent=0L
         goto, labelend 
     endif
     n=0
     for i=0,6 do begin
     		if multistruct(i).key ne -1 then n=n+1     		
     endfor
     if n eq 7 then begin
       	   message=[' ',  				$
      			'You cannot add any more ',  	$
      			'plots to the current graph.', $      			
      			' ']
      			buttons=['OK']   
         	   messid=popup(message=message,title='Mutilgraph Warning!',buttons=buttons)
     endif else begin  		
     	   tempstruct= { x: data.x, xunit: data.xunit, y: data.y, key: n, description: description, display:1}
         multistruct(n).x = tempstruct.x
         multistruct(n).xunit = tempstruct.xunit
         multistruct(n).y = tempstruct.y
         multistruct(n).key = tempstruct.key
         multistruct(n).description = tempstruct.description
         multistruct(n).display = tempstruct.display
     endelse 
     xmin=1.0e+30
     xmax=0.0
     ymin=1.0e+30
     ymax=0.0
     for i=0,n do begin
      xindex=where(multistruct(i).x gt 0.0)
      yindex=where(multistruct(i).y gt 0.0)      
     	tmpxmin=min(multistruct(i).x(xindex),max=tmpxmax)
     	tmpymin=min(multistruct(i).y(yindex),max=tmpymax)
     	if tmpxmin lt xmin then xmin=tmpxmin
     	if tmpxmax gt xmax then xmax=tmpxmax     	
      if tmpymin lt ymin then ymin=tmpymin
      if tmpymax gt ymax then ymax=tmpymax         	 
     endfor
     ldef=1 
     xmin=xmin-0.2*xmin
     xmax=xmax+0.2*xmax
     ymin=ymin-0.2*ymin
     ymax=ymax+0.2*ymax 
                  
  endif else begin
     multldfit=data.ldsel
     multistruct = { x: data.x, xunit: data.xunit, y: data.y, key: -1, description: '', display:0}
     multistruct = replicate(multistruct,7)
     multistruct(0).key=0
     multistruct(0).description=description
     ldef=0
  endelse  
  if data.xunit ne multistruct(0).xunit then begin
     message=[' ',				  $
        	  'The x-units of your current plot do ',   $
        	  'not match those currently used in', $  			  
        	  'the multiple graph widget. Please ',$
        	  'change your x-units to '+data.xunits(0,multistruct(0).xunit)+'.',$
        	  ' ']   
     buttons=['OK']       	  
     messid=popup(message=message,title='Mutilgraph Warning!',buttons=buttons)
     parent=0L				  
     goto,labelend  
  endif

		;**********************************************************
		;**** Assign parameters defining the size of the graph ****
		;**** in the graphics window in IDL procedure localize ****
		;**********************************************************

  localize

		;**** Create the main base for the graph widget ****	

  parent = widget_base(TITLE='ADAS multiplot',XOFFSET=1, YOFFSET=1)


  uvalue = 0
  parent2 = WIDGET_BASE(parent, UVALUE = uvalue, 			$
		EVENT_PRO = "adas_multgr_event", 				$
		FUNC_GET_VALUE = "adas_multgr_get_val", 			$
		PRO_SET_VALUE = "adas_multgr_set_val",/COLUMN)

		;**** Create base to hold the value of state ****

  first_child = widget_base(parent2)
  
  cwid= widget_base(first_child,/COLUMN)

		;**** Create upper base ****

  upperbase = widget_base(cwid,/row)

		;**** Create lower base ****
  if nolowerbase eq 0 then begin
    lowerbase = widget_base(cwid,/row,space=20)
  end else if nolowerbase eq 1 then begin
    if nodone eq 0 or keep eq 1 or bitflag eq 1 or multiplot ne 0 $
      or print eq 1 then stop,$
  'CW_ADAS_GRAPH ERROR: CHOICE OF KEYWORDS INCOMPATIBLE WITH NOWLOWERBASE'
  end

		;********************************
		;**** The Upper Area         ****
		;********************************

		;**** Graphics area ****

  leftupper = widget_base(upperbase,/column)
  if notitle eq 0 then begin
    rc = widget_label(leftupper,value=title)
  end
  drawid = widget_draw(leftupper,/frame,xsize=xsize,ysize=ysize)

		;********************************
		;**** The lower Area         ****
		;********************************

		;**** Extra bitmapped button if specified ****

  if (bitflag eq 1) then begin
      read_X11_bitmap, bitbutton, bitmap1
      bitid = widget_button(lowerbase, value=bitmap1)
  endif else begin
      bitid = 0L
  endelse


		;**** Print buttons if enabled ****
  if print eq 1 then begin
    printid = widget_button(lowerbase,value='Print   ',FONT=font)
  end else begin
    printid = 0L
  end

		;**** Done button ****
  if nodone eq 0 then $       
    doneid = widget_button(lowerbase,value='Done',FONT=font) $
  else doneid = 0L 

		;**** 'Done but retain' button ****
  if keep eq 1 then $
    keepid = widget_button(lowerbase,value='Done but retain',FONT=font) $
  else keepid=0L
  
		;**** 'Adjust' button ****
  if adjust eq 1 then $
    adjustid = widget_button(lowerbase,value='Adjust',FONT=font) $
  else adjustid=0L  

		;**** Message area ****
  if strtrim(grval.message) ne '' then begin
    message = grval.message
  end else begin
    message=' '
  end
  messid = widget_label(cwid,value=message,font=font)
  
  rightbase=widget_base(upperbase,/column,/frame)

  labelid=widget_label(rightbase,value=' ') 
  labelid=widget_label(rightbase,value='List and Settings:',FONT=font,/align_left)
  labelid=widget_label(rightbase,value=' ') 

  labelid=widget_label(rightbase,value='Indx Key Description Dis Del',FONT=font,/align_left)
  labelid=widget_label(rightbase,value=' ')
     
  rowbase=lonarr(7)
  keyid=lonarr(7)
  descripid=lonarr(7)
  buttsid=lonarr(7)
  for i= 0,6 do begin
     rowbase(i)=widget_base(rightbase,/row)
     label=' '+strtrim(string(i+1),2)+': '
     labelid=widget_label(rowbase(i),value=label,FONT=font)
     if multistruct(i).key ne -1 then keystring=strtrim(string(multistruct(i).key),2) else keystring=''
     keyid(i)= widget_text(rowbase(i),xsize=3,/editable,value=keystring)
     if multistruct(i).description ne '' then dstring=multistruct(i).description else dstring=''
     descripid(i)= widget_text(rowbase(i),xsize=12,/editable,value=dstring)
     buttvals=[multistruct(i).display,0]
     buttsid(i)= cw_bgroup(rowbase(i),[' ',' '],label_left=' ',set_value=buttvals,/nonexclusive,/row)
  endfor 
  

  widget_control,parent,/realize  
 
	;************************
	;**** Set up colours ****
	;************************
		
  TVLCT, red, green, blue, /get

  device,get_visual_depth=depth

  if depth eq 24 then begin
  	  colour = { black   : red(0)+256L*(green(0)+256L*blue(0)),   $
  			 white   : red(1)+256L*(green(1)+256L*blue(1)),   $
  			 red     : red(2)+256L*(green(2)+256L*blue(2)),   $	  
  			 green   : red(3)+256L*(green(3)+256L*blue(3)),   $
  			 blue    : red(4)+256L*(green(4)+256L*blue(4)),   $
  			 yellow  : red(5)+256L*(green(5)+256L*blue(5)),   $
  			 magenta : red(6)+256L*(green(6)+256L*blue(6)),   $
  			 cyan    : red(7)+256L*(green(7)+256L*blue(7))      }
  endif else begin
  	  colour = { black   : 0, $
  			 white   : 1, $
  			 red     : 2, $			 
  			 green   : 3, $
  			 blue    : 4, $
  			 yellow  : 5, $
  			 magenta : 6, $
  			 cyan    : 7    }
  endelse 
  
            ;*****************************************************
		;**** Suitable character size for current device  ****
		;**** Aim for 60  characters in y direction.     ****
   		;*****************************************************

  charsize = (!d.y_vsize/!d.y_ch_size)/60.0 
  small_check = GETENV('VERY_SMALL')
    
  if small_check eq 'YES' then charsize=charsize*0.8

    widget_control,drawid,get_value=temp
    grval.win=temp
    wset,grval.win
    adasplot, data.x , data.y, data.itval, data.nplots, data.nmx, $
         data.title,data.xtitle, data.xunit, data.xunits, data.ytitle,		$
         data.strg, data.head1, data.head2a,data.head2b, 		$
         data.teva, data.din, ldef, 				$
         xmin, xmax, ymin, ymax, 			$
         data.lfsel, xlog=data.xlog, ylog=data.ylog, nodata=1, 	$
         multilabels=1

    rhs = grright + 0.06
    count=0
    for i=0,6 do begin
  	if multistruct(i).display eq 1 AND multistruct(i).key ne -1 then begin
  	   count=count+1
  	   oplot, multistruct(i).x(0,*), multistruct(i).y(0,*), linestyle=multistruct(i).key; , color=colour.white
  	   cpp = grtop - 0.1 - 0.05*count
         xyouts, rhs+0.01, cpp, strtrim(string(count),2), /normal, alignment=0.0, charsize=charsize
         xyouts, rhs+0.075, cpp, multistruct(i).description, /normal, alignment=0.0, charsize=charsize
	   if multistruct(i).x(0,data.itval-1) gt data.xmin AND $
	   	    multistruct(i).x(0,data.itval-1) lt data.xmax then begin
			if multistruct(i).y(0,data.itval-1) gt data.ymin AND $
	   			multistruct(i).y(0,data.itval-1) le data.ymax then begin
	   			xout=data.itval-1	   			
				yout=data.itval-1
			endif else begin
	   			for j=0,data.itval-1 do begin
	   				if multistruct(i).y(0,j) le data.ymax then begin
	   					xout=j
	   					yout=j
	   				endif
	   			endfor				
			endelse
	   endif else begin
		   	for j=0,data.itval-1 do begin
	   			if multistruct(i).x(0,j) le data.xmax AND multistruct(i).x(0,j) gt 0.0 then begin
	   				xout=j
	   				yout=j
	   			endif
	   		endfor	  			
	   endelse	
	   xyouts, multistruct(i).x(0,xout),multistruct(i).y(0,yout), alignment=1.0, charsize=charsize, '('+strtrim(string(count),2)+')'
	endif
    endfor



  rhs = grright + 0.06
  xyouts,rhs, grtop-0.1,'INDEX     DESCRIPTION', /normal, alignment=0.0, charsize=charsize  

		;**** Create state structure ****
  
  new_state ={DRAWID:drawid, 	BITID:bitid, 	PRINTID:printid, 		$
		DONEID:doneid, 	MESSID:messid, 	GRVAL:grval, 		$
		KEEPID:keepid, 	ADJUSTID:adjustid,				$
		ROWBASE:rowbase,  KEYID:keyid, DESCRIPID:descripid, BUTTSID:buttsid } 

		;**** Save initial state structure ****


	widget_control, first_child, set_uvalue=new_state, /no_copy

  dynlabel, parent
          
  xmanager,'cw_adas_multigraph',parent,/modal,/just_reg

 
  userroot = getenv('ADASUSER')
  file=userroot + '/pass/.multigraph'  
  save,filename=file,multldfit,multistruct

LABELEND:
  
  RETURN, parent

END
