; Copyright (c) 1995 Strathclyde University .
;
;+
; PROJECT:
;       ADAS
;
; NAME:
;       ADAS_MENU_DEF
;
; PURPOSE:
;       Defines the menus for the ADAS system.
;
; EXPLANATION:
;       This routine returns string arrays which are used to build
;       the ADAS menu system.  The ADAS system has a two level menu
;       structure.  The main level groups ADAS applications into
;       categories.  Each catergory then has a sub-menu of individual
;       ADAS applications.  The structure has been copied exactly from
;       the IBM ADAS system.
;
;       The structure is returned in two string arrays, one for the
;       main menu and another 2-D string array holding all of the
;       sub-menus.
;
; USE:
;       See adas.pro for usage.  The output from this routine is
;       for use as the calling arguments to adas_menus.
;
; INPUTS:
;       None.
;
; OPTIONAL INPUTS:
;       None.
;
; OUTPUTS:
;       MAINMENU - A string array; Holds the text of the main ADAS
;                  menu options.  One array element per option.
;
;       SUBMENUS - A 2D string array; Holds the text of the ADAS
;                  submenu options i.e the ADAS application names.
;                  submenus(i,*) holds the submenu options for
;                  mainmenu option MAINMENU(i) for all main menu
;                  options.  Where the number of submenu options for
;                  a main option does not fill the array column
;                  there will be null strings stored.
;
; OPTIONAL OUTPUTS:
;       None.
;
; KEYWORD PARAMETERS:
;       None.
;
; CALLS:
;       None.
;
; SIDE EFFECTS:
;       None.
;
; CATEGORY:
;       Adas system.
;
;
; WRITTEN:
;       Andrew Bowen, Tessella Support Services plc, 25-May-1993
;
; MODIFIED:
;       1.1     Andrew Bowen
;               First release.
;       1.5     Tim Hammond
;               Updated menu headings.
;       1.6     Tim Hammond
;               Updated menu headings.
;       1.7     Tim Hammond
;               Added adas209 to series 2 submenu.
;       1.8     Tim Hammond
;               Added 210, 508 and 509 to relevant sub-menus
;       1.9     Tim Hammond
;               Moved 509 to new series 6 to become 601.
;       1.10    Hugh Summers
;               Redefined 508 and moved old 508 to 509.
;       1.11    William Osborn
;               Added adas211 and adas212
;       1.12    William Osborn
;               Changed names of ADAS211 and ADAS212 in line with JET
;       1.13    David Brooks
;               Added adas602 to series 6 submenu.
;       1.14    Richard Martin
;               Corrected error made in previous SCCS submission.
;       1.15    Richard Martin
;               Added adas312
;       1.16    Richard Martin
;               Added adas410, 412
;       1.17    Richard Martin
;               Added adas409, 411
;               Changed series 5 name from 'Supplementary Programs' to
;               'General ADAS Interrogation Routines'.
;       1.18    Richard Martin
;               Added adas413, adas213, adas214, adas215, adas703
;       1.19    Richard Martin
;               Added adas314 and adas603.
;       1.20    Richard Martin
;               Added adas311,adas313 and adas216
;       1.21    Hugh Summers
;               Added adas108.
;       1.22    Richard Martin
;               Added adas414, adas415, adas801 and adas807
;       1.23    Martin O'Mullane
;               Corrected misspelling of Calculation in Series 8 menu.
;       1.25    Richard Martin
;               Added adas811
;       1.26    Richard Martin
;               Added adas510, adas804, adas808, adas809, adas810.
;       1.27    Richard Martin
;               Added adas510, adas804, adas808, adas809, adas810.
;       1.28    Allan Whiteford
;               Added adas812 and changed entry for adas811.
;       1.29    Martin O'Mullane
;               Add entries for adas315 and adas316.
;       1.30    Martin O'Mullane
;               Add entry for adas416.
;       1.31    Allan Whiteford
;               Add entry for adas605.
;       1.32    Martin O'Mullane
;               Change name of series 8 menu.
;       1.33    Martin O'Mullane
;               Add series 9.
;
; VERSION:
;       1.1     01-06-93
;       1.5     21-04-95
;       1.6     16-05-95
;       1.7     27-06-95
;       1.8     19-01-96
;       1.9     26-01-96
;       1.10    04-04-96
;       1.11    19-08-96
;       1.12    20-08-96
;       1.13    16-06-97
;       1.14    26-06-97
;       1.15    17-07-97
;       1.16    31-10-97
;       1.17    09-03-98
;       1.18    09-06-98
;       1.19    04-12-98
;       1.20    19-03-99
;       1.21    18-10-99
;       1.22    21-03-00
;       1.23    25-01-01
;       1.24    ??-??-??
;       1.25    13-06-02
;       1.26    18-02-03
;       1.28    10-08-05
;       1.29    04-07-07
;       1.30    04-10-07
;       1.31    23-02-09
;       1.32    19-08-2010
;       1.33    17-10-2012
;
;-
;-----------------------------------------------------------------------------

PRO adas_menu_def, mainmenu, submenus

                ;**** set up menu data ****

  mainmenu = [  '1 Atomic Data Entry and Verification',           $
                '2 General Z Data and Population Processing',     $
                '3 Charge Exchange Processing',                   $
                '4 Recombination and Ionisation Processing',      $
                '5 General ADAS Interrogation Routines',          $
                '6 Data Analysis Programs',                       $
                '7 Creating and Using Dielectronic Data',         $
                '8 Creating and Manipulating Excitation Data',    $
                '9 General Molecular Processing'     ]

                ;**** declare an empty array ****

  submenus = strarr(9,16)

                ;*********************************
                ;**** Fill the submenus array ****
                ;*********************************
  menu = [ $
'ADAS101: Cross Section                 - Graphing and Gamma Evaluation', $
'ADAS102: Excitation Rate               - Graphing and Interpolation',    $
'ADAS103: Dielectronic Coefficient      - Graphing and Interpolation',    $
'ADAS104: Radiative Recombination       - Graphing and Interpolation',    $
'ADAS105: Ionisation Cross Section      - Graphing and Rate Evaluation',  $
'ADAS106: Ionisation Rate               - Graphing and Interpolation',    $
'ADAS107: Charge Exchange Cross Section - Graphing and Rate Evaluation',  $
'ADAS108: Xsect:neutrals and molecules. - Graphing and Rate Evaluation'   $
         ]
  submenus[0,0:7] = menu

  menu = [ $
'ADAS201: Specific Z Excitation    File - Graph and Fit Coefficient',           $
'ADAS202: General  Z Recom./Ionis. File - Extraction from General Z File',      $
'ADAS203: General  Z Excitation    File - Extraction from General Z File',      $
'ADAS204: Specific Z Recom./Ionis. File - Process ACD,SCD and Populations',     $
'ADAS205: Specific Z Excitation    File - Process Meta./Excit.  Population',    $
'ADAS206: Specific Z Excitation    File - Process Line/Total Power',            $
'ADAS207: Meta./Excit. Population  File - Process Line Emissivities',           $
'ADAS208: Specific Z Excitation    File - Advanced Population Processing',      $
'ADAS209: General Level Bundling   File - Process Effective Collision'+         $
' Strengths',                                                                   $
'ADAS210: General Level Unbundling File - Process Effective Collision'+         $
' Strengths',                                                                   $
'ADAS211: Radiative Recombination       - Process for Specific Ion File',       $
'ADAS212: Dielectronic Recombination    - Process for Specific Ion File',       $
'ADAS213: Collisional Ionisation        - Process for Specific Ion File',       $
'ADAS214: Escape Factors                - Convert Specific ion File',           $
'ADAS215: Temperature Regrid            - Convert Specific Ion File'     ,      $
'ADAS216: Error Processing              - Examine Errors in Specific ion Files' $
         ]
  submenus[1,0:15] = menu

  menu = [ $
'ADAS301: QCX      File  - Graph and Fit Cross Section',                     $
'ADAS302: IONATOM  File  - Graph and Fit Cross Section',                     $
'ADAS303: QEF      File  - Graph and Fit Coefficient',                       $
'ADAS304: BMS      File  - Graph and Fit Coefficient',                       $
'ADAS305: QSK      File  - Graph and Fit Coefficient',                       $
'ADAS306: QCX      File  - Process Effective Coefficient: J Resolved',       $
'ADAS307: QCX      File  - Process Effective Coefficient: J Resolved/Scan',  $
'ADAS308: QCX      File  - Process Effective Coefficient: L Resolved',       $
'ADAS309: QCX      File  - Process Effective Coefficient: L Resolved/Scan',  $
'ADAS310: BEAM STOPPING  - Process Stopping Coefficient: H Beam',            $
'ADAS311: BEAM STOPPING  - Process Stopping Coefficient: He Beam' ,          $
'ADAS312: BDN      File  - Tabulate and Graph BMS and BME data: H beam',     $
'ADAS313: BNL      File  - Tabulate and Graph BMS and BME data: He beam',    $
'ADAS314: QCX      File  - Convert QCX to effective cross sections',         $
'ADAS315: QCX      File  - Arbitrary species CX cross sections',             $
'ADAS316: BDN      File  - Bundle-n CX emissivity generation'                ]
  submenus[2,0:15] = menu

  menu = [ $
'ADAS401: Iso-Electronic Sequence    - Graph and Fit Coefficient',                $
'ADAS402: Iso-Nuclear    Sequence    - Graph and Fit Coefficient',                $
'ADAS403: Iso-Electronic Master File - Merge Partial Iso-Elec.  Files',           $
'ADAS404: Iso-Nuclear    Master File - Extract from Iso-Elec.  Master Files',     $
'ADAS405: Equilibrium Ionisation     - Process Meta. Pops. and Emis. Funcs.',     $
'ADAS406: Transient   Ionisation     - Process Meta. Pops. amd Emis. Funcs.',     $
'ADAS407: Iso-Nuclear Param. Sets    - Prepare Optimised Power Params.',          $
'ADAS408: Iso-Nuclear Master Data    - Prepare from Iso-Nuc. Param. Sets',        $
'ADAS409: Equilibrium Ionisation     - Prepare G(Te,Ne) Function Tables',         $
'ADAS410: Dielectronic Recombination - Graph and Fit Data',                       $
'ADAS411: Radiative Recombination    - Graph and Fit Data',                       $
'ADAS412: Equilibrium Ionisation     - Prepare G(Te) Function Tables',            $
'ADAS413: Collisional Ionisation     - Graph and Fit Data',                       $
'ADAS414: Prepare soft X-ray filter file',                                        $
'ADAS415: Display spectral filter file',                                          $
'ADAS416: Superstages                - Repartition adf11 and emissivity datasets' $
         ]
  submenus[3,0:15] = menu

  menu = [                                                                        $
'ADAS501: SXB      File  - Graph and Fit Ionizations per Photon',                 $
'ADAS502: SZD      File  - Graph and Fit Ionization Rate-Coefficients',           $
'ADAS503: PEC      File  - Graph and Fit Photon Emissivities',                    $
'ADAS504: PZD      File  - Graph and Fit Radiated Powers',                        $
'ADAS505: QTX      File  - Graph and Fit Thermal Charge Exch. Coefft.',           $
'ADAS506: GFT      File  - Graph and Fit G(TE) Function',                         $
'ADAS507: GCF      File  - Graph and Fit General. Contribution Function',         $
'ADAS508: GTN      File  - Graph and Fit G(TE,NE) Function',                      $
'ADAS509: SCX      File  - Graph and Fit Charge Exchange Cross-sections',         $
'ADAS510: F-PEC    File  - Graph Envelope Feature Photon Emissivity Coefficients' $
    ]
  submenus[4,0:9] = menu

  menu = [                                                              $
'ADAS601:  Differential Emission Measure Analysis',                     $
'ADAS602:  Spectral Line Profile Fitting'  ,                            $
'ADAS603:  Zeeman Feature and Spectral Line Profile Fitting',           $
'ADAS604:',                                                             $
'ADAS605:  General ADAS feature inspection'                             ]
  submenus[5,0:4] = menu

  menu = [                                                              $
'ADAS701:  AUTOSTRUCTURE',                                              $
'ADAS702:  Postprocessor - AUTOSTRUCTURE > adf09 files',                $
'ADAS703:  Postprocessor - AUTOSTRUCTURE > adf04 files',                $
'ADAS704:  Postprocessor - AUTOSTRUCTURE > Spin Breakdown Auger Rates'  ]
  submenus[6,0:3] = menu

  menu = [                                                              $
'ADAS801:  Cowan Structure Code - adf04 type 1 and 3',                  $
'ADAS802:   ',                                                          $
'ADAS803:',                                                             $
'ADAS804:  Calculate cross-sections and rate coefficients',             $
'ADAS805:',                                                             $
'ADAS806:',                                                             $
'ADAS807:  Prepare cross-referencing files for GCR',                    $
'ADAS808:  Prepare driver files for ADAS801',                           $
'ADAS809:  Non-Maxwellian modelling - alter adf04 file type',           $
'ADAS810:  Generate envelope feature photon emissivity coefficients',   $
'ADAS811:  Graph and compare adf04 files',                              $
'ADAS812:  Compare adf04 files (scatterplot)'                           ]
  submenus[7,0:11] = menu

  menu = [                                                              $
'ADAS901:  Interrogate fundamental data (mdf02)',                       $
'ADAS902:  Interrogate rates and lifetimes  (mdf04 and mdf33)',     $
'ADAS903:  Generate rate data',                                     $
'ADAS904:  Assemble a data collection for population model',        $
'ADAS905:  Molecular population calculation',                       $
'ADAS906:  Interrogate molecular source coefficients',              $
'ADAS907:  Interrogate molecular emissivity coefficients',          $
'ADAS908:  Generate molecular spectral features'                    ]
  submenus[8,0:7] = menu



END
