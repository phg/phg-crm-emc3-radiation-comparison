;+
; PROJECT:
;       ADAS
;
; NAME:
;       ADAS_COLORS
;
; PURPOSE: Another general purpose colour naming scheme.
;
;
; EXPLANATION: Returns a structure with colour names which should
;              work for all output devices, postscript and screens
;              of different colour depths.
;
;
; EXAMPLE:
;        adas_colors, colors=colors
;        plot, [3,4], [3,4], color=colors.red
;
; INPUTS:
;        None.
;
; OPTIONAL INPUTS:
;       None
;
; OUTPUTS:
;       The function returns a structure of name of colours. Currently,
;                  WHITE         DARKGRAY      LIGHTGRAY
;                  DIMGRAY       GRAY          ORANGE
;                  WHEAT         HOTPINK       GOLD
;                  PEACHPUFF     CHARCOAL      BEIGE
;                  SKY           ORCHID        AQUA
;                  PINK          NAVY          BLUE
;                  RED           GREEN         MAGENTA
;                  YELLOW        CYAN          BLACK
;
; OPTIONAL OUTPUTS:
;       None
;
; KEYWORD PARAMETERS:
;       help - returns this header as help
;
;
; WRITTEN:
;       Allan Whiterford
;
; MODIFIED:
;       1.1     Martin O'Mullane
;                - First release
;
; VERSION:
;       1.1     21-11-2007
;
;-
;-----------------------------------------------------------------------------

PRO adas_colors, colors = colors, help = help

; If asked for help

if keyword_set(help) then begin
   doc_library, 'adas_colors'
   return
endif


usetrue=0

if !d.name eq 'PS' then begin
        usetrue=0
endif

if !d.name eq 'X' then begin
        device,get_decomposed=usetrue
        device,get_visual_depth=depth

        if depth lt 24 then begin
                device,decomposed=0
                device,get_decomposed=usetrue
        endif
endif

; RGB values from X11 rgb.txt.
; what about other devices? probably a z-buffer is the one to catch here

names  = ['Black']
rvalue = [  0    ]
gvalue = [  0    ]
bvalue = [  0    ]

names  = [names,   'Magenta', 'Cyan', 'Yellow', 'Green']
rvalue = [rvalue,     255,       0,      255,       0  ]
gvalue = [gvalue,       0,     255,      255,     255  ]
bvalue = [bvalue,     255,     255,        0,       0  ]

names  = [names,  'Red', 'Blue', 'Navy', 'Pink', 'Aqua']
rvalue = [rvalue,  255,     0,      0,    255,    112]
gvalue = [gvalue,    0,     0,      0,    127,    219]
bvalue = [bvalue,    0,   255,    115,    127,    147]

names  = [names,  'Orchid', 'Sky', 'Beige', 'Charcoal']
rvalue = [rvalue,   219,      0,     255,       80    ]
gvalue = [gvalue,   112,    163,     171,       80    ]
bvalue = [bvalue,   219,    255,     127,       80    ]

names  = [names,  'PeachPuff', 'Gold', 'HotPink', 'Wheat', 'Orange' ]
rvalue = [rvalue,   255,       255,     255,       255,      255    ]
gvalue = [gvalue,   218,       215,     105,       231,      165    ]
bvalue = [bvalue,   185,         0,     180,       186,        0    ]

names  = [names,   'Gray', 'DimGray', 'LightGray', 'DarkGray' ]
rvalue = [rvalue,    135,   105,       211,         169       ]
gvalue = [gvalue,    135,   105,       211,         169       ]
bvalue = [bvalue,    135,   105,       211,         169       ]

names  = [names,   'White']
rvalue = [rvalue,    255  ]
gvalue = [gvalue,    255  ]
bvalue = [bvalue,    255  ]


if usetrue eq 1 then begin
        value=create_struct('n_colours',n_elements(names))
        for i=0,n_elements(names)-1 do begin
                value=create_struct(names[i],rvalue[i]+256l*gvalue[i]+256l*256l*bvalue[i],value)
        end
endif else begin
        value=create_struct('n_colours',n_elements(names))
        offset=!d.table_size-n_elements(names)-2
        tvlct,rvalue,gvalue,bvalue,offset
        for i=0,n_elements(names)-1 do begin
                value=create_struct(names[i],i+offset,value)
        end
endelse

if arg_present(colors) then colors=value

END
