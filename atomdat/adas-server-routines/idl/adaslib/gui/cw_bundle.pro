; Copyright (c) 1995, Strathclyde University .
; SCCS info: Module @(#)$Header: /home/adascvs/idl/adaslib/gui/cw_bundle.pro,v 1.3 2004/07/06 13:04:57 whitefor Exp $ Date $Date: 2004/07/06 13:04:57 $
;
;+
; PROJECT:
;       ADAS IBM MVS to   UNIX conversion
;
; NAME:
;	CW_BUNDLE()
;
; PURPOSE:
;	A selection list allowing a single selection, the 'key' of which
;	is then editable. The key is only accepted if it is one of a
;	given set.
;
; EXPLANATION:
;	This widget uses a widget_list with values made up from a TEXT
;	array and a KEY array. On selecting an item, the selection appears
;	below, the text as a label and the key as an editable field. If
;	the key entered is valid, changes are made to the list.
;
;	The 'value' of this widget is a structure consisting of the text
;	and the key arrays. {TEXT:String array, KEY:String Array}. This
;	can be set as well as got.
;	
;	The widget also includes a title above the list.
;
;	The acceptable keys are specified by the keyword valid_keys. These
;	are the keys valid to the user of the widget. The programmer can
;	use set_val to set the text and keys without restriction, although
;	the key 'comment' is reserved to mean that no key may be entered
;	for this field.
;
;	This widget generates events as items are selected.  The event
;	structure is;
;		{ID:0L, TOP:0L, HANDLER:0L, TEXT:String(), KEY:String()}
;
;
; USE:
;	An example of how to use this widget;
;
;		text = ['Red','Green','Blue','Black','Yellow','Orange']
;		key  = ['1','2','3','4','1','1']
;		val = {TEXT:text, KEY:key}
;		poss_keys = ['1','2','3','4']
;		base = widget_base()
;		selid = cw_bundle(base, poss_keys, value=val, chosen = 0)
;
;	You could use
;		widget_control,selid,get_value=value
;	to recover the users' selection and
;		widget_control,selid,set_value=value
;	to set the text and key (this resets the internal CHOSEN value to 0).
;
;	See ADAS407 for a more complex example.
;
; INPUTS:
;       PARENT	- The ID of the parent widget.
;
;
; OPTIONAL INPUTS:
;	None.
;
; OUTPUTS:
;       The return value of this function is the ID of the compound
;       widget which is of type LONG integer.
;
; OPTIONAL OUTPUTS:
;       None
;
; KEYWORD PARAMETERS:
;	VALUE	- Structure; {TEXT:String Array,KEY:Integer Array}
;		  The text to put in the list and their keys. The key
;		  'comment' is reserved to mean that no key is enterable
;		  for this field.
;
;	VALID_KEYS
;		- A string array of the legitimate keys that can be entered.
;		  Default = ['1','2','3','4','5']
;
;	TITLE	- String; a title for the widget.
;
;	YSIZE	- The height of the list widget in rows. Default 4.
;		  Scroll bars will appear for lists longer than ysize.
;
;       FONT    - A font to use for all text in this widget.
;
;	BIG_FONT - A font to be used for titles and column headers.
;
;	UVALUE	- A user value for this widget.
;
;  	COLTITLES - A string array containing the titles for the
;		    column.  Can be more than one line.
;
;	SIZE	- The maximum number of elements that can be set in the list.
;
; CALLS:
;       See side effects for other related widget management routines.
;
; SIDE EFFECTS:
;       Three other routines are included which are used to manage the
;       widget;
;
;	BUNDLE_SET_VAL
;	BUNDLE_GET_VAL()
;	BUNDLE_EVENT()
;
; CATEGORY:
;	Compound Widget
;
; WRITTEN:
;       William Osborn, Tessella Support Services plc, 3-April-1996
;
; MODIFIED:
;	1.1	William Osborn
;		First version.
;	1.2	William Osborn
;		Added 'comment' feature so that lines can exist without
;		a key choosing facility
;	1.3	William Osborn
;		Added dynlabel procedure so that IDL v4.0 will resize the
;		label widgets as v3.6.1 does.
; VERSION:
;	1.1	03-04-96
;	1.2	02-05-96
;	1.3	21-05-96
;
;-
;-----------------------------------------------------------------------------

PRO bundle_set_val, id, value

		;**** Return to caller on error ****
    ON_ERROR, 2

		;**** Retrieve the state ****
    first_child = widget_info(id, /child)
    widget_control, first_child, get_uvalue=state, /no_copy

		;**** Find number of items in list ****
    text = value.text
    key  = value.key
    numitem = n_elements(text)
    skey = n_elements(key)

    size = state.size

		;**** Fill state.text and state.key up to state.size ****
    if(numitem le size) then begin
	t=strarr(size)
	k=strarr(size)
	t(0:numitem-1)         = text
	k(0:skey-1) = key
	t(numitem:size-1) = replicate(' ',size-numitem)
	k(skey:size-1) = replicate(' ',size-skey)
	text = t
	key  = k
    endif else begin
	print, 'CW_BUNDLE: Truncating list - increase the SIZE parameter when first called'
	text = text(0:size-1)
	key = key(0:size-1)
	numitem = size
    endelse

		;**** Look for 'comment' in key and set state.comment
		;**** appropriately

    for i=0,size-1 do begin
	if key(i) eq 'comment' then begin
	    key(i)=' '
	    state.comment(i)=1
	endif else state.comment(i)=0
    endfor

    for i=numitem,size-1 do state.comment(i)=1

		;**** Insert into list widget ****
    widget_control,state.listid,set_value=text+key

		;**** Select top-most as chosen and put into label ****

    state.chosen=0

    if not state.comment(0) then 					$
	widget_control, state.listid, set_list_select=state.chosen

    widget_control, state.selid, set_value=key(0)

    if not state.comment(0) then begin
    	widget_control, state.labid, set_value=text(0)
    endif else begin
	widget_control, state.labid, set_value=' '
    endelse

		;**** Copy value to state ****
    state.text    = text
    state.key     = key
    state.numitem = numitem

               ;**** Save the new state structure ****

    widget_control, first_child, set_uvalue=state, /no_copy

END

;-----------------------------------------------------------------------------


FUNCTION bundle_get_val, id


		;**** Return to caller on error ****
    ON_ERROR, 2

		;**** Retrieve the state ****
    first_child = widget_info(id, /child)
    widget_control, first_child, get_uvalue=state

		;**** Return the current selection ****

    RETURN, {TEXT:state.text, KEY:state.key}

END

;-----------------------------------------------------------------------------

FUNCTION bundle_event, event

		;**** Base ID of compound widget ****
    base=event.handler

		;**** Retrieve the state ****
    first_child = widget_info(base, /child)
    widget_control, first_child, get_uvalue=state, /no_copy


		;************************
		;**** Process Events ****
		;************************
    CASE event.id OF

    	state.listid: begin

		chosen = event.index

		;**** Don't allow selection of a comment ****
		if not state.comment(chosen) then begin
		    state.chosen = chosen

		    if(chosen lt state.numitem) then begin

		        if(state.size gt 1) then begin
		            widget_control, state.selid,		$
			    	set_value=state.key(chosen)
		            widget_control, state.labid,		$
			     	set_value=state.text(chosen)
		    	endif else begin
		            widget_control, state.selid,		$
			    	set_value=state.key
		            widget_control, state.labid,		$
			    	set_value=state.text
		    	endelse

		;**** Move input focus to the typein ****
	 	    	widget_control, state.selid, /input_focus

		    endif

		endif else begin
		;**** Put input focus back to last selected ****
		    widget_control, state.listid, set_list_select=state.chosen
		endelse

    		      end

	state.selid: begin

		chosen = state.chosen
		widget_control, state.selid,get_value=val
		val = strtrim(val(0),2)
		ok=0
		for i=0,n_elements(state.valid_keys)-1 do begin
		    if state.valid_keys(i) eq val then ok = 1
		endfor

		;**** If we have a valid key then change the list
		;**** and get the next element for editing

		if ok eq 1  and state.comment(chosen) eq 0 then begin

		    state.key(chosen)=val
		    widget_control,state.listid,			$
			  set_value=state.text+state.key

		    chosen = chosen+1
		    if chosen ge state.numitem then chosen=0
		    state.chosen = chosen
		    if(state.size gt 1) then begin
		        widget_control, state.selid,			$
			    set_value=state.key(chosen)
		        widget_control, state.labid,			$
			    set_value=state.text(chosen)
		    endif else begin
		        widget_control, state.selid,			$
			    set_value=state.key
		        widget_control, state.labid,			$
			    set_value=state.text
		    endelse
		    widget_control, state.listid, set_list_select = chosen


		;**** Move input focus to the typein ****
		    widget_control, state.selid, /input_focus

		endif else begin
			widget_control,state.selid,set_value=''
		endelse

		      end

    ELSE:

    ENDCASE

    key = state.key

		;**** Save the state structure ****

    widget_control, first_child, set_uvalue=state, /no_copy

    RETURN, {ID:base, TOP:event.top, HANDLER:0L, KEY:key}

END

;-----------------------------------------------------------------------------

FUNCTION cw_bundle, parent,  VALUE=value, CHOSEN = chosen,		$
			TITLE=title, YSIZE=ysize, FONT=font, 		$
			BIG_FONT=big_font, UVALUE=uvalue,		$
			VALID_KEYS = valid_keys, SIZE = size


    IF (N_PARAMS() LT 1) THEN MESSAGE, 'Must specify PARENT for cw_adas_ranges'

		;**** Set defaults for keywords ****
    IF NOT (KEYWORD_SET(title)) THEN title = ''
    IF NOT (KEYWORD_SET(ysize)) THEN ysize = 4
    IF NOT (KEYWORD_SET(font)) THEN font = ''
    IF NOT (KEYWORD_SET(big_font))  THEN big_font = ''
    IF NOT (KEYWORD_SET(uvalue))  THEN uvalue = 0
    IF NOT (KEYWORD_SET(chosen))  THEN chosen = 0
    IF NOT (KEYWORD_SET(valid_keys))  THEN valid_keys = ['1','2','3','4','5']
    IF NOT (KEYWORD_SET(size)) THEN size = 10
    IF NOT (KEYWORD_SET(value)) THEN value = {text:replicate(' ',size),	$
					      key:replicate('comment',size)}

		;**** Create the main base for the widget ****
    topbase = WIDGET_BASE(parent, UVALUE         = uvalue,		$
				  EVENT_FUNC     = "bundle_event", 	$
				  FUNC_GET_VALUE = "bundle_get_val",	$
				  PRO_SET_VALUE  = "bundle_set_val", 	$
			  	  /COLUMN)

    first_child = widget_base(topbase)

    cwid = widget_base(first_child, /column)

		;**** Find number of items in list ****
    text = value.text
    key  = value.key
    numitem = size(text)
    skey = size(key)
    if numitem(0) gt 0 then begin
    	numitem = numitem(1)
    endif else begin
    	numitem = 1
    endelse
    if skey(0) gt 0 then begin
    	skey = skey(1)
    endif else begin
    	skey = 1
    endelse


		;**** Check to see if lengths aren't the same as size
    if(numitem lt size) then begin
	temptext=strarr(size)
	temptext(0:numitem-1)=text
	temptext(numitem:size-1) = replicate(' ',numitem-size)
	text=temptext
    endif
    if(skey lt size) then begin
	tempkey=strarr(size)
	tempkey(0:skey-1)=key
	tempkey(skey:size-1) = replicate(' ',skey-size)
	key=tempkey
    endif

		;**** Find 'comment's and set appropriately ****

    comment=bytarr(size)

    for i=0,size-1 do begin
	if key(i) eq 'comment' then begin
	    key(i)=' '
	    comment(i)=1
	endif else comment(i)=0
    endfor
    

		;**** Find the maximum length of text in list ****
		;****    and pad list or title accordingly    ****

    maxlen = max(strlen(text))
    maxlenkey = max(strlen(key))
    titlen = strlen(title)
    blanks = '                              '
    if (titlen gt maxlen) then begin
	text = text + replicate(strmid(blanks,0,titlen-maxlen),size)
	titlep = title + ' Key'
    endif else begin
    	titlep = title+strmid(blanks,0,maxlen-titlen)+' Key'
    endelse

		;**** Title for list ****
    rc = widget_text(cwid,value=titlep,font=font, xsize=strlen(titlep))

		;**** The selection list ****
    ysize = ysize < size
    listid = widget_list(cwid,value=text+key,ysize=ysize,  $
			 font=font)

		;**** Highlight the chosen item ****
    if not comment(chosen) then 					$
	widget_control, listid, set_list_select = chosen

		;**** The selected item and input widget ****
    tttbase = widget_base(cwid,/row,/frame)
    if comment(chosen) then begin
	labid=widget_label(tttbase, value=strmid(blanks,0,maxlen), font=font)
    endif else begin
        labid=widget_label(tttbase,value = text(chosen), font=font)
    endelse
    selid   = widget_text(tttbase, value = key(chosen),			$
			  font=font,/editable,xsize=maxlenkey+1)

		;**** Create state structure ****
    new_state = {SELID:selid, LISTID:listid, LABID:labid,		$
		CHOSEN:chosen, NUMITEM:numitem, TEXT:text, KEY:key,	$
		VALID_KEYS:valid_keys, SIZE:size, COMMENT:comment}

		;**** Save initial state structure ****

    widget_control, first_child, set_uvalue=new_state, /no_copy

    dynlabel, topbase

    RETURN, topbase

END
