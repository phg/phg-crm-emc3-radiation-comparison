;+
; PROJECT:
;  AFG
;
; NAME:
;  AFG_PRIMITIVE
;
; PURPOSE:
;  Base class from which most (if not all) AFG classes should inherit.
;  Supplies methods / variables commonly required by most AFG classes.
;
; EXPLANATION:
;  This class provides methods for set/ get debug information
;  flag and also for set / get of error messages. 
;  This class can be upgraded to provide new functionality
;  to all inheriting AFG classes.
;  
; USE:
;  Inherit this class as you would any other.
;
; INITIALISATION SYNTAX:
;  Not intended to be instantiated alone. Only to be used for inheritance by other
;  AFG classes.
;
; PUBLIC ROUTINES:
;  [seterrmsg]
;    PURPOSE:
;      Sets state variable errmsg with input error message string
;    INPUTS:
;      String - the error message to set.
;    OUTPUTS:
;      Integer - always returns 0 (this is for convienent use of 
;      the routine in return statement of originating method).
;    SIDE EFFECTS:
;      None.
;
;  [geterrmsg]
;    PURPOSE:
;     Returns the currently set error message string.
;    INPUTS:
;      None
;    OUTPUTS:
;      String - returns the currently set error message string.
;    SIDE EFFECTS:
;      None
;
;  [setdebug]
;    PURPOSE:
;      Sets debug flag.
;    INPUTS:
;      Integer - 1 or 0.
;    OUTPUTS:
;      Returns 1 if successful.
;      Returns 0 if unsuccessful.
;    SIDE EFFECTS:
;      None
;
;  [getdebug]
;    PURPOSE:
;      Returns value of debug flag.
;    INPUTS:
;      None.
;    OUTPUTS:
;      Integer - 1 or 0.
;    SIDE EFFECTS:
;      None
;
; CALLS:
;   None.
;
; SIDE EFFECTS:
;   None.
;	
; AUTHOR:
;   Christopher Nicholas, University of Strathclyde
;
; VERSION HISTORY:
;   1.1 Chris Nicholas 22-07-2008
;   	* Adapted from ffs_primitive
;-

function afg_primitive::seterrmsg,errmsg
  if round(10.0*float(!version.release)) ge 62 then $
    last_routine=((scope_traceback(/structure))[n_elements(scope_traceback(/structure))-2]).routine $
  else begin
    help, calls=calls
    last_routine=strmid(calls[1], 0, strpos(calls[1], '<')-1)
  endelse
  self.errmsg=errmsg
  if self.debug eq 1 then print, '--------------------------------------------------'
  if self.debug eq 1 then print, 'error has occured in ', last_routine
  if self.debug eq 1 then print, '--> ', errmsg
  if self.debug eq 1 then print, '--------------------------------------------------'  
  return,0
end

function afg_primitive::geterrmsg
  return, self.errmsg
end

pro afg_primitive::help
  help, self, /obj
end

function afg_primitive::setdebug, debug
  self.debug = debug
  return, 1
end

function afg_primitive::getdebug
  return, self.debug
end

pro afg_primitive::cleanup

end


function afg_primitive::init, debug=debug
  if keyword_set(debug) then self.debug = 1
   return,1
end


pro afg_primitive__define
    self= { afg_primitive, $
    	    debug:0, $
    	    errmsg:'' $
          }
end
