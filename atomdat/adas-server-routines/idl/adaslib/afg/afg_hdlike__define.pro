;+
; PROJECT:
;       ADAS
;
; NAME:
;       afg_hdlike
;
; PURPOSE:
;       Class to represent the H-like Zeeman feature (coming from
;       ADAS603) in the ADAS Feature Generation (AFG) system.
;       Provides setdesc, docalc and initpars methods as required
;       by AFG.
;
; EXPLANATION:
;       This class inherits from AFG_API to provide an object
;       representing a Zeeman feature. It provides feature-specific
;       methods which override the base-class placeholder methods.
;       Specifcally it contains the code which interfaces the
;       generalised representation of AFG to the specific ADAS603
;       subroutines.
;
; USE:
;       obj=obj_new('afg_hdlike')
;       pars=obj->getpars()
;       pars.obsangle=80.0
;       pars.lower=7
;       pars.upper=8
;       pars.ion=4
;       ok=obj->setpars(pars=pars)
;       result=obj->getcalc()
;       plot,result.wv,result.intensity,/nodata
;       for i=0,n_elements(result.wv)-1 do $
;         oplot,[result.wv[i],result.wv[i]],[0,result.intensity[i]]
;       
; PUBLIC ROUTINES:
;       Please see the documentation for AFG_API for an explanation
;       of how to use this class. No public methods are added by
;       the inheritence of this class.
;
; CALLS:
;       AFG_API (via inheritance)
;       ADAS603_GET_HDLIKE
;
; WRITTEN:
;       Allan Whiteford, University of Strathclyde
;
;       1.1     Allan Whiteford
;               Initial version
;               18/02/09
;-

FUNCTION afg_hdlike::initpars
    pars={                 $
          pol:      1     ,$
          obsangle: 90.0  ,$
          bvalue:   2.5   ,$
          lower:    7     ,$
          upper:    8     ,$
          ion:      4      }
    RETURN, self->setPars(PARS=pars)
END

function afg_hdlike::setDesc
    
    desc = { $
                name:'H-like Zeeman Feature', $
                text:'ADAS implementaion of H-like zeeman feature based on '+ $
                     'XPaschen code, original implementation in ADAS is'+ $
                     'ADAS603.', $
                parameters:{ $
                    pol:        { desc:'Polarisation Filtering:',$
                                  type:'integer', $
                                  units: 'none', $
                                  dispType:'selection', $
                                  values:['NONE','PI+SIGMA', 'PI', 'SIGMA', 'SIGMA+', 'SIGMA-'], $
                                  log:0, $
                                  altersLimits:0}, $ 
                    obsangle:   { desc:'Observation angle (relative to field)', $
                                  type:'float', $
                                  units: 'degrees', $
                                  min: '0.0', $
                                  max: '90.0', $
                                  dispType:'continuous', $
                                  log:0, $
                                  altersLimits:0  }, $
                    bvalue:     { desc:'Magnetic field strength (T)', $
                                  type:'float', $
                                  units: 'T', $
                                  min: 0.0, $
                                  max: 20.0, $
                                  dispType:'continuous', $
                                  log:0, $
                                  altersLimits:0  }, $
                    lower:      { desc:'Lower n of transition', $
                                  type:'float', $
                                  units: 'none', $
                                  min: 0.0, $
                                  max: 20.0, $
                                  dispType:'field', $
                                  log:0, $
                                  altersLimits:0  }, $
                    upper:      { desc:'Upper n of transition', $
                                  type:'integer', $
                                  units: 'none', $
                                  min: 2 , $
                                  max: 20, $
                                  dispType:'field', $
                                  log:0, $
                                  altersLimits:0  }, $
                    ion:     { desc:'Ion', $
                                  type:'integer', $
                                  units: 'none', $
                                  min: 1 , $
                                  max: 19, $  
                                  dispType:'selection', $
                                  values:['H','D','T','He','C','Ne'], $
                                  log:0, $
                                  altersLimits:0} $      
                } $      
          }
  return, self->afg_api::setDesc(DESC=desc)
END

function afg_hdlike::doCalc

    if self->setWvResolved(0) eq 0 then return, 0
    pars = self->getPars()

    fortdir=getenv('ADASFORT')
      
    pol = pars.pol
    obsangle=pars.obsangle
    bvalue= pars.bvalue
    upper=pars.upper
    lower=pars.lower
    ion=pars.ion
   
    adas603_get_hdlike, fortdir, pol, obsangle, bvalue, $
    			lower, upper, ion,              $
                        m_wlength, m_strength, err    
 
    if n_elements(err) gt 0 then return,self->seterrmsg(err + '. ')

    if ptr_valid(self.wv) then ptr_free, self.wv
    self.wv = ptr_new(m_wlength)  

    if ptr_valid(self.intensity) then ptr_free, self.intensity   
    self.intensity = ptr_new(m_strength)
   
    return, 1
END

PRO afg_hdlike::cleanup
    self->afg_api::cleanup
END

FUNCTION afg_hdlike::init, debug=debug
    IF self->afg_api::init(debug=debug) EQ 0 THEN RETURN, 0
    RETURN, 1
END

PRO afg_hdlike__define
    define = {afg_hdlike,      $
              INHERITS afg_api }
END

 
