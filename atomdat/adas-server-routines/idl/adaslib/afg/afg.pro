;----------------------------------------------------------------------
;+
; PROJECT    :  ADAS
;
; NAME       :  afg
;
; PURPOSE    :  Entry point to the ADAS Feature Generation (AFG) system
;
; EXPLANATION:
;               The AFG system is a collection of intelligent wrappers
;               to underlying ADAS routines which can generate spectral
;               data. It provides a generic interface to all of these
;               features for ease of use programmatically.
;
;               This top level routine performs a number of functions:
;                   1) Provides a list of the supported features.
;                   2) Provides a direct route to AFG objects
;                   3) Provides wrappers so that AFG objects
;                      can be called in a non-object way.
;
; USAGE:
;               As examples of the above three functions:
;
;                 To obtain a list of supported features:
;                   val=afg(/list)
;                   print,val
;               
;                 To obtain direct access to a particular
;                 AFG feature type as an object:
;                   obj=afg('zeeman',/obj)
;                 for information on using this object please
;                 see the documentation for afg_api__define.
;
;                 To use AFG features without accessing objects
;                 directly:
;                   pars=afg('zeeman',/parameters)
;                   pars.bvalue=3.5
;                   res=afg('zeeman',calculate=pars)
;                   plot,res.wv,res.intensity,/nodata
;                   for i=0,n_elements(res.wv)-1 do $
;                      oplot,[res.wv[i],res.wv[i]],[0,res.intensity[i]]
;
;
; ARGUMENTS  :  
;               feature (string): AFG feature to use, must be
;                                 in the valid list of features.
;                                 Note this argument is not required
;                                 if the /list or /help keywords
;                                 are set.
;
; KEYWORDS   :  
;               list:
;                       Return a list of the available
;                       features.
;               
;               obj:
;                       Return an object reference to
;                       the specified AFG feature.
;
;               parameters:
;                       Return a structure of parameters with
;                       suitable default values which are necessary
;                       for calculation of a feature. This structure
;                       can be modified by the user and then passed
;                       back in via the calculate keyword to
;                       generate a feature
;
;               calculate:
;                       Use parameters (passed in via the keyword
;                       as calculate=parameters) as returned
;                       and modified from a call with /parameters
;                       to generate a feature. The returned
;                       value is a structure of the following
;                       form:
;                         wv:         Wavelength values
;                         intensity:  Intensity values
;                         wvresolved: 0 - Points are at exact wavelengths
;                                         i.e. are not /wavelength unit and
;                                         represent delta functions.
;                                     1 - Points are resolved on to
;                                         a wavelength grid so are /wavelength
;                                         unit and can be plotted like
;                                         a spectrum.
;               description:
;                       Returns a detailed description of the selected
;                       feature. This is a complex structure which
;                       is document in afg_api__define.
;
; RETURNS    :
;               Return value is dependent on the keywords
;               which are used. If an error occurs then this
;               will be returned as a scalar string.
;  
; SIDE-EFFECTS:
;               Calls underlying ADAS codes which may spawn
;               other binaries or call code via shared objects.
;               None of these functions should generate output
;               files.
;
; AUTHOR     :  Allan Whiteford
;
; DATE       :  28/05/08
;
;
; MODIFIED:
;       1.1     Allan Whiteford
;               - First version.
;                 Note that this is a simple wrapper to the underlying
;                 AFG system which was largely implemented by
;                 Chris Nicholas.
;       1.2     Martin O'Mullane
;               - Add adf15 and adas306 features.
;
; VERSION:
;       1.1    28-05-2008
;       1.2    10-08-2015
;
;-
;----------------------------------------------------------------------

function afg,feature,list=list,help=help,obj=obj,description=description,parameters=parameters,calculate=calculate

        list_of_features=['stark','zeeman','hdlike', 'adf15', 'adas306'] ; ,'example','cx','envelope','balmer']
	
        if keyword_set(list) then begin
		return,list_of_features
	end

	if keyword_set(help) then begin
		doc_library,'afg'
		return,''
	end

	if n_elements(obj) ne 0 then begin
		if n_elements(feature) eq 0 then feature=obj
		if (size(feature,/type) ne 7) || ((where(feature eq list_of_features))[0] eq -1) then return,'Please specify a valid feature'
                return,obj_new('afg_'+feature)        
        endif

	if n_params() lt 1 then begin
		return,'Please specify a feature name'
	end

	if (size(feature,/type) ne 7) || ((where(feature eq list_of_features))[0] eq -1) then return,'Please specify a valid feature'

	if keyword_set(parameters) or arg_present(parameters) then begin
		tmp=obj_new('afg_'+feature)
		parameters=tmp->getpars()
                obj_destroy,tmp
                return,parameters
        endif

	if n_elements(calculate) ne 0 then begin
		tmp=obj_new('afg_'+feature)
 		junk=tmp->setpars(pars=calculate)
		if junk eq 0 then return,'Error setting parameter values'
        	result=tmp->getcalc()
		wvresolved=tmp->getWvResolved()
                result=create_struct(result,'wvresolved',wvresolved)
                obj_destroy,tmp
		return,result
	endif

	if keyword_set(description) or arg_present(description) then begin
        	tmp=obj_new('afg_'+feature)
		desc=tmp->getdesc()
                obj_destroy,tmp
                return,desc
	endif

end
