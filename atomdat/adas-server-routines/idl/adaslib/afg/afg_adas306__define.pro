;+
; PROJECT:
;       ADAS
;
; NAME:
;       AFG_ADAS306
;
; PURPOSE:
;       Class to represent the CX feature (coming from ADAS306)
;       in the ADAS Feature Generation (AFG) system.
;       Provides setdesc, docalc and initpars methods as required
;       by AFG.
;
; EXPLANATION:
;       This class inherits from AFG_API to provide an object
;       representing a CX feature. It provides feature-specific
;       methods which override the base-class placeholder methods.
;       Specifcally it contains the code which interfaces the
;       generalised representation of AFG to the specific ADAS306
;       subroutines.
;
; USE:  Consider the NeX 11-10 CX feature,
;
;       obj           = obj_new('afg_adas306')
;       pars          = obj->getpars()
;       pars.filename = '/home/adas/adas/adf01/qcx#h0/qcx#h0_old#ne10.dat'
;       pars.tion     = 1200.0
;       pars.upper    = 11
;       pars.lower    = 10
;       ok            = obj->setpars(pars=pars)
;       result        = obj->getcalc()
;       plot, result.wv, result.intensity, /nodata
;       for i = 0, n_elements(result.wv)-1 do $
;         oplot, [result.wv[i],result.wv[i]], [0,result.intensity[i]]
;
;       Note that this is an object so there are default values for beam
;       energy, mass, field energy fraction etc. Set these explicitly for
;       your conditions.
;
; PUBLIC ROUTINES:
;       Please see the documentation for AFG_API for an explanation
;       of how to use this class. No public methods are added by
;       the inheritence of this class.
;
; CALLS:
;       AFG_API (via inheritance)
;
;
; AUTHOR     :  Stuart Henderson
; DATE       :  08-04-2015
;
;
; UPDATE     :
;       1.1     Stuart Henderson
;                - First version
;
; VERSION    :
;       1.1     08-04-2015
;-
;--------------------------------------------------------------------------------------

function afg_adas306::initpars

    pars = { filename : 'NULL',         $
             lower    : 7,              $
             upper    : 8,              $
             tion     : 100.0,          $
             te       : 100.0,          $
             dens     : 1e13,           $
             eng      : 1000.0,         $    ; usual adf01 have 1keV/amu limit
             frac     : 1.0,            $
             zeff     : 1.0,            $
             bmag     : 3.0,            $
             mass     : 12,             $
             elec     : 0               $    ; Default to CX
           }

    RETURN, self->setPars(PARS=pars)

end
;--------------------------------------------------------------------------------------

function afg_adas306::setDesc

    parameters = { filename   : {desc         : 'adf01 filename',          $
                                 type         : 'string',                  $
                                 units        : 'none',                    $
                                 disptype     : 'file',                    $
                                 alterslimits : 0},                        $
                   lower      : {desc         : 'Lower n of transition',   $
                                 type         : 'float',                   $
                                 units        : 'none',                    $
                                 min          : 1.0,                       $
                                 max          : 20.0,                      $
                                 dispType     : 'field',                   $
                                 log          : 0,                         $
                                 altersLimits : 0  },                      $
                   upper      : {desc         : 'Upper n of transition',   $
                                 type         : 'float',                   $
                                 units        : 'none',                    $
                                 min          : 2.0,                       $
                                 max          : 20.0,                      $
                                 dispType     : 'field',                   $
                                 log          : 0,                         $
                                 altersLimits : 0  },                      $
                   tion       : {desc         :'ion temperature:',         $
                                 type         :'float',                    $
                                 units        : 'eV',                      $
                                 min          : 0.0 ,                      $
                                 max          : 10000 ,                    $
                                 dispType     :'continuous',               $
                                 log          : 0 ,                        $
                                 alterslimits : 0},                        $
                   te         : {desc         : 'electron temperature',    $
                                 type         : 'float',                   $
                                 units        : 'eV',                      $
                                 min          : 0.2,                       $
                                 max          : 1.0e5,                     $
                                 disptype     : 'continuous',              $
                                 log          : 0,                         $
                                 alterslimits : 0},                        $
                   dens       : {desc         : 'electron density',        $
                                 type         : 'float',                   $
                                 units        : 'cm-3',                    $
                                 min          : 1.0e4,                     $
                                 max          : 1.0e16,                    $
                                 disptype     : 'continuous',              $
                                 log          : 0,                         $
                                 alterslimits : 0},                        $
                   eng        : {desc         : 'Beam energy',             $
                                 type         : 'float',                   $
                                 units        : 'eV/amu',                  $
                                 min          : 10,                        $
                                 max          : 1e6,                       $
                                 disptype     : 'continuous',              $
                                 log          : 0,                         $
                                 alterslimits : 0},                        $
                   frac       : {desc         : 'Energy fraction',         $
                                 type         : 'float',                   $
                                 units        : '',                        $
                                 min          : 0,                         $
                                 max          : 1,                         $
                                 disptype     : 'continuous',              $
                                 log          : 0,                         $
                                 alterslimits : 0},                        $
                   zeff       : {desc         : 'Zeffective charge',       $
                                 type         : 'float',                   $
                                 units        : 'none',                    $
                                 min          : 1,                         $
                                 max          : 10,                        $
                                 disptype     : 'continuous',              $
                                 log          : 0,                         $
                                 alterslimits : 0},                        $
                   bmag       : {desc         : 'Toroidal magnetic field', $
                                 type         : 'float',                   $
                                 units        : 'T',                       $
                                 min          : 0,                         $
                                 max          : 10,                        $
                                 disptype     : 'continuous',              $
                                 log          : 0,                         $
                                 alterslimits : 0},                        $
                   mass       : {desc         : 'Atomic mass',             $
                                 type         : 'float',                   $
                                 units        : 'amu',                     $
                                 min          : 1,                         $
                                 max          : 100,                       $
                                 disptype     : 'continuous',              $
                                 log          : 0,                         $
                                 alterslimits : 0},                        $
                   elec       : {desc         : 'Type of emission',        $
                                 type         : 'integer',                 $
                                 units        : 'none',                    $
                                 min          : 0,                         $
                                 max          : 1,                         $
                                 dispType     : 'selection',               $
                                 values       : ['CX','Electron excit'],   $
                                 log          : 0,                         $
                                 alterslimits : 0}                         $
                 }
    desc       = { name       : 'ADAS306 CX Feature',                                                         $
                   text       : 'ADAS implementaion of CX features, original implementation in ADAS is ADAS306.',$
                   parameters : parameters}


  return, self->afg_api::setDesc(DESC=desc)

END
;--------------------------------------------------------------------------------------


function afg_adas306::doCalc

    if self->setWvResolved(0) eq 0 then return, 0

; Changeable parameters

    pars   = self->getPars()

    adf01  = pars.filename
    tion   = pars.tion
    te     = pars.te
    dens   = pars.dens
    dion   = dens
    eng    = [pars.eng]
    frac   = [pars.frac]
    zeff   = pars.zeff
    bmag   = pars.bmag
    mass   = pars.mass
    upper  = pars.upper
    lower  = pars.lower
    elec   = pars.elec

    ; necessary for 306 but not for user input

    n1in  = 6
    n2in  = 5
    emiss = 1.0e6

    if (adf01 NE 'NULL') AND (upper GT 0) AND (lower GT 0) then begin

        ; Run ADAS306 code to get feature

        run_adas306, adf01 = adf01 , mass  = mass  , te    = te    , tion = tion , $
                     dens  = dens  , dion  = dion  , zeff  = zeff  , bmag = bmag , $
                     frac  = frac  , eng   = eng   , n1in  = n1in  , n2in = n2in , $
                     emiss = emiss , n1req = upper , n2req = lower , elec = elec , $
                     data  = data

        wvcomp = [data.wave]
        emcomp = [data.emiss]

    endif else begin

       wvcomp = [-1.0, -1.0]
       emcomp = [-1.0, -1.0]

    endelse

    IF PTR_VALID(self.wv) THEN PTR_FREE, self.wv
    self.wv = PTR_NEW(wvcomp)

    IF PTR_VALID(self.intensity) THEN PTR_FREE, self.intensity
    self.intensity = PTR_NEW(emcomp)

    return, 1

END
;--------------------------------------------------------------------------------------


PRO afg_adas306::cleanup
    self->afg_api::cleanup
END
;--------------------------------------------------------------------------------------


FUNCTION afg_adas306::init, debug=debug
    IF self->afg_api::init(debug=debug) EQ 0 THEN RETURN, 0
    RETURN, 1
END
;--------------------------------------------------------------------------------------

PRO afg_adas306__define
    define = {afg_adas306,      $
              INHERITS afg_api, $
              filename : ''     }
END


