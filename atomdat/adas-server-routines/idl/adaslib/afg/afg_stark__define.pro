;+
; PROJECT:
;       ADAS
;
; NAME:
;       AFG_STARK
;
; PURPOSE:
;       Class to represent the stark feature (coming from ADAS305)
;       in the ADAS Feature Generation (AFG) system.
;       Provides setdesc, docalc and initpars methods as required
;       by AFG.
;
; EXPLANATION:
;       This class inherits from AFG_API to provide an object
;       representing a stark feature. It provides feature-specific
;       methods which override the base-class placeholder methods.
;       Specifcally it contains the code which interfaces the
;       generalised representation of AFG to the specific ADAS305
;       subroutines.
;
; USE:
;       obj=obj_new('afg_stark')
;       pars=obj->getpars()
;       pars.beam_energy=80.0
;       pars.plasma_density=1e14
;       ok=obj->setpars(pars=pars)
;       result=obj->getcalc()
;       plot,result.wv,result.intensity,/nodata
;       for i=0,n_elements(result.wv)-1 do $
;         oplot,[result.wv[i],result.wv[i]],[0,result.intensity[i]]
;       
; PUBLIC ROUTINES:
;       Please see the documentation for AFG_API for an explanation
;       of how to use this class. No public methods are added by
;       the inheritence of this class.
;
; CALLS:
;       AFG_API (via inheritance)
;       ADAS305_GET_STARK
;
; WRITTEN:
;       Chris Nicholas, University of Strathclyde
;
;       1.0     Chris Nicholas and Allan Whiteford 
;               Released to ADAS
;               22/07/08
;       1.1     Allan Whiteford
;               Added documentation
;               Changed internal formatting
;               First release with ADAS
;               ??/??/??
;-

FUNCTION afg_stark::initpars
    pars = {                         $
            beam_mass:      2.0     ,$
            beam_energy:    40.0    ,$
            beam_te:        10.0    ,$
            beam_density:   1e10    ,$
            plasma_mass:    2.0     ,$
            plasma_te:      4.44e3  ,$
            plasma_density: 2.49e13 ,$
            plasma_zeff:    2.0     ,$
            beam_dc_x:      0.0     ,$
            beam_dc_y:      0.0     ,$
            beam_dc_z:      1.0     ,$
            bfield_value:   3.3915  ,$
            bfield_dc_x:    0.788   ,$
            bfield_dc_y:    0.0053  ,$
            bfield_dc_z:    0.6152  ,$
            efield_value:   0.0     ,$
            efield_dc_x:    1.0     ,$
            efield_dc_y:    0.0     ,$
            efield_dc_z:    0.0     ,$
            obs_dc_x:       0.8701  ,$
            obs_dc_y:       -0.047  ,$
            obs_dc_z:       0.4905  ,$
            obs_sigma:      0.51    ,$
            obs_pi:         1.0     ,$
            broaden:        0        }
    RETURN, self->setPars(PARS=pars)             
END

function afg_stark::setDesc
    
    desc = { $
                name:'Stark Feature', $
                text:'Stark broadened beam emission feature arising from the '+$
                     'excitation of beam hydrogen ions taking into account '+$
                     'both static electric contributions to level splitting '+$
                     'as well as the motional stark effect contribution. '+$
                     'Original implementation in ADAS is ADAS305.', $
                parameters:{ $
                    beam_mass:      { desc:'Beam Mass', $
                                      type:'float', $
                                      units: 'amu', $
                                      min: 1.0, $
                                      max: 3.0, $  
                                      disptype: 'field', $
                                      log:0, $
                                      alterslimits:0  }, $
                    beam_energy:    { desc:'Beam Energy', $
                                      type:'float', $
                                      units: 'keV/amu', $
                                      min: 10.0, $
                                      max: 500.0, $  
                                      disptype: 'continuous', $
                                      log:1, $
                                      alterslimits:0  }, $
                    beam_te:        { desc:'Beam Electron Temperature', $
                                      type:'float', $
                                      units:  'eV', $
                                      min: 1.0, $
                                      max: 100.0, $  
                                      disptype: 'continuous', $
                                      log:1, $
                                      alterslimits:0  }, $
                    beam_density:   { desc:'Beam Density', $
                                      type:'float', $
                                      units: 'cm^-3', $
                                      min: 1e8, $
                                      max: 1e12, $  
                                      disptype: 'continuous', $
                                      log:1, $
                                      alterslimits:0  }, $
                    plasma_mass:    { desc:'Plama Mass', $
                                      type:'float', $
                                      units: 'amu', $
                                      min: 1.0, $
                                      max: 2.0, $  
                                      disptype: 'field', $
                                      log:0, $
                                      alterslimits:0  }, $
                    plasma_te:      { desc:'Plasma Electron Temperature', $
                                      type:'float', $
                                      units: 'eV', $
                                      min: 100.0, $
                                      max: 10000.0, $  
                                      disptype: 'continuous', $
                                      log:1, $
                                      alterslimits:0  }, $
                    plasma_density: { desc:'Plasma Density', $
                                      type:'float', $
                                      units:'cm^-3' ,$
                                      min: 1e10, $
                                      max: 1e15, $  
                                      disptype: 'continuous', $
                                      log:1, $
                                      alterslimits:0  }, $
                    plasma_zeff:    { desc:'Plasma Effective Charge', $
                                      type:'float', $
                                      units:'none',$
                                      min: 1.0, $
                                      max: 5.0, $  
                                      disptype: 'continuous', $
                                      log:0, $
                                      alterslimits:0  }, $
                    beam_dc_x:      { desc:'Beam Direction Cosine [X]', $
                                      type:'float', $
                                      units: 'none' ,$
                                      min: 0.0, $
                                      max: 1.0, $  
                                      disptype: 'field', $
                                      log:0, $
                                      alterslimits:0  }, $
                    beam_dc_y:      { desc:'Beam Direction Cosine [Y]', $
                                      type:'float', $
                                      units:'none' ,$
                                      min: 0.0, $
                                      max: 1.0, $  
                                      disptype: 'field', $
                                      log:0, $
                                      alterslimits:0  }, $
                    beam_dc_z:      { desc:'Beam Direction Cosine [Z]', $
                                      type:'float', $
                                      units: 'none',$
                                      min: 0.0, $
                                      max: 1.0, $  
                                      disptype: 'field', $
                                      log:0, $
                                      alterslimits:0  }, $
                    bfield_value:   { desc:'B-Field Strength', $
                                      type:'float', $
                                      units: 'T' ,$
                                      min: 0.2, $
                                      max: 10.0, $  
                                      disptype: 'continuous', $
                                      log:0, $
                                      alterslimits:0  }, $
                    bfield_dc_x:    { desc:'B-Field Direction Cosine [X]', $
                                      type:'float', $
                                      units: 'none' ,$
                                      min: 0.0, $
                                      max: 1.0, $  
                                      disptype: 'field', $
                                      log:0, $
                                      alterslimits:0  }, $
                    bfield_dc_y:    { desc:'B-Field Direction Cosine [Y]', $
                                      type:'float', $
                                      units:'none' ,$
                                      min: 0.0, $
                                      max: 1.0, $  
                                      disptype: 'field', $
                                      log:0, $
                                      alterslimits:0  }, $
                    bfield_dc_z:    { desc:'B-Field Direction Cosine [Z]', $
                                      type:'float', $
                                      units: 'none',$
                                      min: 0.0, $
                                      max: 1.0, $  
                                      disptype: 'field', $
                                      log:0, $
                                      alterslimits:0  }, $
                    efield_value:   { desc:'E-Field Strength', $
                                      type:'float', $
                                      units: 'V',$
                                      min: 0.1, $
                                      max: 10.0, $  
                                      disptype: 'continuous', $
                                      log:0, $
                                      alterslimits:0  }, $
                    efield_dc_x:    { desc:'E-Field Direction Cosine [X]', $
                                      type:'float', $
                                      units: 'none',$
                                      min: 0.0, $
                                      max: 1.0, $  
                                      disptype: 'field', $
                                      log:0, $
                                      alterslimits:0  }, $
                    efield_dc_y:    { desc:'E-Field Direction Cosine [Y]', $
                                      type:'float', $
                                      units: 'none',$
                                      min: 0.0, $
                                      max: 1.0, $  
                                      disptype: 'field', $
                                      log:0, $
                                      alterslimits:0  }, $
                    efield_dc_z:    { desc:'E-Field Direction Cosine [Z]', $
                                      type:'float', $
                                      units: 'none',$
                                      min: 0.0, $
                                      max: 1.0, $  
                                      disptype: 'field', $
                                      log:0, $
                                      alterslimits:0  }, $
                    obs_dc_x:       { desc:'Observation Direction Cosine [X]', $
                                      type:'float', $
                                      units:'none' ,$
                                      min: 0.0, $
                                      max: 1.0, $  
                                      disptype: 'field', $
                                      log:0, $
                                      alterslimits:0  }, $
                    obs_dc_y:       { desc:'Observation Direction Cosine [Y]', $
                                      type:'float', $
                                      units: 'none',$
                                      min: 0.0, $
                                      max: 1.0, $  
                                      disptype: 'field', $
                                      log:0, $
                                      alterslimits:0  }, $
                    obs_dc_z:       { desc:'Observation Direction Cosine [Z]', $
                                      type:'float', $
                                      units: 'none',$
                                      min: 0.0, $
                                      max: 1.0, $  
                                      disptype: 'field', $
                                      log:0, $
                                      alterslimits:0  }, $
                    obs_sigma:      { desc:'Observed Polarisation Multiplier [sigma]', $
                                      type:'float', $
                                      units:'none' ,$
                                      min: 0.0, $
                                      max: 1.0, $  
                                      disptype: 'field', $
                                      log:0, $
                                      alterslimits:0  }, $
                    obs_pi:         { desc:'Observed Polarisation Multiplier [pi]', $
                                      type:'float', $
                                      units:'none' ,$
                                      min: 0.0, $
                                      max: 1.0, $  
                                      disptype: 'field', $
                                      log:0, $
                                      alterslimits:0  }, $
                    broaden:        { desc:'Broadening (enable/disable)', $
                                      type:'integer', $
                                      units: 'none',$
                                      min: 0, $
                                      max: 1, $  
                                      dispType:'selection', $
                                      values:['Off','On'], $
                                      log:0, $
                                      alterslimits:0  } $
                }, $
                subordinate: { $
                                observation_direction:  { title:'Observation Direction', $
                                                          members:['obs_dc_x','obs_dc_y','obs_dc_z'], $
                                                          after:'obs_pi'}, $
                                beam_direction:         { title:'Beam Direction', $
                                                          members:['beam_dc_x','beam_dc_y','beam_dc_z'],$
                                                          after:'beam_density'}, $
                                bfield_direction:       { title:'B-field Direction', $
                                                          members:['bfield_dc_x','bfield_dc_y','bfield_dc_z'],$
                                                          after:'bfield_value'}, $
                                efield_direction:       { title:'E-field Direction', $
                                                          members:['efield_dc_x','efield_dc_y','efield_dc_z'],$
                                                          after:'efield_value'} $
                },   $
                groups: { $
                                beam:           { title:'Beam', $
                                                 members:['beam_mass','beam_energy','beam_te','beam_density','beam_direction']}, $
                                plasma:         { title:'Plasma' , $
                                                  members:['plasma_mass','plasma_te','plasma_density','plasma_zeff']}, $
                                bfield:         { title:'Magnetic Field', $
                                                  members:['bfield_value','bfield_direction']}, $
                                efield:         { title:'Electric Field', $
                                                  members:['efield_value','efield_direction']}, $
                                observation:    { title:'Observation', $
                                                  members:['obs_pi','obs_sigma','observation_direction']} $
                }   $
          }
    return, self->afg_api::setDesc(DESC=desc)
END

FUNCTION afg_stark::doCalc

    pars = self->getPars()

    beam   = {dc_x:pars.beam_dc_x, dc_y:pars.beam_dc_y, dc_z:pars.beam_dc_z, $
              mass : pars.beam_mass, energy : pars.beam_energy*1000d0, $
              te : pars.beam_te, density : pars.beam_density}
    plasma = {mass : pars.plasma_mass, te : pars.plasma_te, $
              density : pars.plasma_density, zeff : pars.plasma_zeff}
    bfield = {value : pars.bfield_value, dc_x : pars.bfield_dc_x, $
              dc_y : pars.bfield_dc_x, dc_z : pars.efield_dc_z}
    efield = {value : pars.efield_value, dc_x : pars.efield_dc_x, $
              dc_y : pars.efield_dc_x, dc_z : pars.efield_dc_z}
    obs    = {dc_x : pars.obs_dc_x, dc_y : pars.obs_dc_y, $
              dc_z : pars.obs_dc_z, sigma : pars.obs_sigma, pi : pars.obs_pi}
 
    n_upper=3
    n_lower=2
      
    IF pars.broaden eq 0 THEN BEGIN      
        if self->setwvResolved(0) eq 0 then return, 0
        
        adas305_get_stark,beam=beam,plasma=plasma,bfield=bfield,efield=efield, $
                          obs=obs,n_upper=n_upper,n_lower=n_lower, $
                          wave_comp=wave_comp,emiss_comp=emiss_comp
        
        wavelength=wave_comp
        intensity=emiss_comp    
    ENDIF ELSE BEGIN
        if self->setwvResolved(1) eq 0 then return,0
        
        adas305_get_stark,beam=beam,plasma=plasma,bfield=bfield,efield=efield, $
                          obs=obs,n_upper=n_upper,n_lower=n_lower, $
                          /doppler,/auto_wave, $
                          emiss_doppler=intensity,wave_doppler=wavelength
        
    ENDELSE

    IF PTR_VALID(self.wv) THEN PTR_FREE, self.wv      
    self.wv = PTR_NEW(wavelength)

    IF PTR_VALID(self.intensity) THEN PTR_FREE, self.intensity   
    self.intensity = PTR_NEW(intensity)
    return,1   
END

PRO afg_stark::cleanup
    self->afg_api::cleanup
END

FUNCTION afg_stark::init, debug=debug
    IF self->afg_api::init(debug=debug) EQ 0 THEN RETURN, 0
    RETURN, 1
END

PRO afg_stark__define
    define = {afg_stark,       $
              INHERITS afg_api }
END

 
