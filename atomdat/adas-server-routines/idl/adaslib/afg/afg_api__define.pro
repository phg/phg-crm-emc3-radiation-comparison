;+
; PROJECT:
;       ADAS
;
; NAME:
;	AFG_API
;
; PURPOSE:
;	ADAS Feature Generation (AFG) base class. All other AFG
;	specific feature classes inherit from this class.
;	Provides the external API and standard functionallity
;	required for use of AFG.
;
; EXPLANATION:
;	To the external user it supplies the key routines
;	which should be used to interact with AFG. These
;	include routines to get descriptions of the
;	particular feature, get a parameter structure,
;	perform a calculation and write out example code.
;
;	This class is inherited from each individual AFG class
;	the higher level classes must supply at a minimum the
;	following:
;		initpars method
;			This method should supply a default
;			set of parameters if none are
;			specified.
;		setdesc method
;			This method should supply a description
;			of the specific feature according
;			to the standard AFG description
;			format (documented below).
;		docalc method
;			This method actually performs the
;			feature-specific calculation.
;
;	Please see afg_example__define for a minimal example
;	of what is required to define a new feature.
;
;
; USE (external API):
;	As an example:
;		obj=obj_new('afg_example')	
;		pars=obj->getpars()
;		pars.height=17
;		pars.position=5000
;		pars.width=30
;		ok=obj->setpars(pars=pars)
;		result=obj->getcalc()
;		plot,result.wv,result.intensity
;
; USE (as base class):
;	Please see afg_example__define for a minimal example
;	of what is required to define a new feature.
;
; INITIALISATION ARGUMENTS:
;	None
;
; PUBLIC ROUTINES:
;
;	getpars
;		PURPOSE:
;			Returns the current parameter structure.
;			Designed so that it can be modified and then
;			put back in with setpars.
;		INPUTS:
;			None
;		OUTPUTS:
;			A named structure containing the values
;			of each parameter.
;		SIDE EFFECTS:
;			None
;
;	setpars
;		PURPOSE:
;			Sets the parameters of the feature.
;			The current values can be obtained with
;			the getpars method.
;		INPUTS:
;			A named structure containing the values
;			of each parameter.
;		OUTPUTS:
;			Returns 1 if parameters are set ok
;			Returns 0 if an error occurred.
;			method for details of the error.
;		SIDE EFFECTS:
;			None but parameter bounds checking calls
;			feature-specific code which may have
;			side effects. 
;	calc
;		PURPOSE:
;			Calculates the feature and stores the
;			results internal (see getcalc for a wrapper
;			which also returns the results).
;		INPUTS:
;			Paramater names can be passed as keywords
;			so you can write:
;				obj::calc(bfield=2.5)
;			to automatically set the bfield
;			without having to to a getpars/setpars.
;		OUTPUTS:
;			Returns 1 - Success.
;                       Returns 0 - Failure.
;		SIDE EFFECTS:
;			Calls feature specific code which is
;			likely to spawn external programs
;			or used shared object calls.
;
;	getwv
;		PURPOSE:
;			Returns the wavelength vector.
;		INPUTS:
;			None
;		OUTPUTS:
;			The internally stored wavelength
;			vector as calculated by the
;			calc method.
;		SIDE EFFECTS:
;			None
;
;	getintensity
;		PURPOSE:
;			Returns the intensity vector
;		INPUTS:
;			None
;		OUTPUTS:
;			The internally stored intensity
;			vector as calculated by the
;			calc method.
;		SIDE EFFECTS:
;			None
;
;	getcalc
;		PURPOSE:
;			Wrapper to calc, getwavelength and
;			getintensity
;		INPUTS:
;			Paramater names can be passed as keywords
;			so you can write:
;				res=obj::getcalc(bfield=2.5)
;			to automatically set the bfield
;			without having to to a getpars/setpars.
;		OUTPUTS:
;			Returns a structure:
;				wv 	 : wavelength vector
;				intensity: intensity vector
;		SIDE EFFECTS:
;			Calls feature specific code which is
;			likely to spawn external programs
;			or used shared object calls.
;
;	getres
;		PURPOSE:
;			Wrapper to getwavelength and
;			getintensity
;		INPUTS:
;			None	
;		OUTPUTS:
;			Returns a structure:
;				wv 	 : wavelength vector
;				intensity: intensity vector
;		SIDE EFFECTS:
;			None
;
;	getwvresolved
;		PURPOSE:
;			Tells whether or not a feature returns
;			discrete lines or a spectrum resolved on
;			a wavelength grid.
;		INPUTS:
;			None
;		OUTPUTS:
;			0 - Points are at exact wavelengths
;			    i.e. are not /wavelength unit and
;			    represent delta functions.
;                       1 - Points are resolved on to
;                           a wavelength grid so are /wavelength
;                           unit and can be plotted like
;                           a spectrum.
;		SIDE EFFECTS:
;			None
;
;	getdesc
;		PURPOSE:
;			Gets description of feature. This is suitable
;			for determing the units/limits of the various
;			parameters as well as giving fuller descriptions
;			of them. The information supplied is enough to
;			dynamically build a GUI for the feature in
;			question.
;		INPUTS:
;			None.
;		OUTPUTS:
;			Returns a complex structure:
;				Name: Name of feature
;				Text: Textual description of feature
;				Parameters: Array of sub-structures
;                                           (see below for details)
;
;	  Parameters sub-structure:
;	          desc:           Textual description of parameter
;	          type:           One of:
;	                            float      -  Floating point value
;	                            integer    -  Integer value
;	                            long       -  Long integer value
;	                            pointer    -  Pointer to arbitrary structure
;	          units:          Textual representation of the units of the
;                                 parameter
;	          min:            Minimum value of the parameter
;	          max:            Maximum value of the parameter
;	          disptype:       Hint as to how input should be selected for 
;                                 this parameter (note related to but distinct 
;	                          from the type tag above).
;	                          One of:
;	                            continuous -  Parameter is a continously 
;                                                 varying variable.
;	                            selection  -  Parameter value should take
;                                                 one of several pre-defined 
;                                                 values (see values tag below)
;	                            field      -  Parameter is a number which
;	                                          isn't continously varying.
;	                            file       -  Parameter is a filename.
;	                            fileList   -  Parameter is a list of
;                                                 filenames.
;
;	          values:         String array of possible values the parameter
;	                          can take.
;	                          (Only necessary for disptype='selection')
;	          alterslimits:   Specifies that modifying the VALUE on this
;	                          parameter can impact on the LIMITS of other
;	                          parameters in the set.
;
;	setdelayhandler:
;		PURPOSE:
;			Allows setting of information as to what
;			the routine should do if it is going to take
;			a long time to perform a calculation.
;		INPUTS:
;			Pointer to a structure containing
;			information on routines which
;			should be called if a long delay
;			is required for calculation.
;		        Form is:
;			   [
;				{method: Method to call at start
;					 of delay
;				 object: Object which contains
;				         above method
;				},
;				{method: Method to call at end
;					 of delay
;                                object: Object which contains
;				         above method
;				}
;			   ]
;		OUTPUTS:
;			Returns 1 if set succesfully
;			Returns 0 on failure
;		SIDE EFFECTS:
;			None but the set routine could be later
;			called.
;
;	writecode
;		PURPOSE:
;			Generates a block of stand-alone IDL
;			code which would reproduce the current
;			state of the feature and plot a graph.
;		INPUTS:
;			Filename to write to.
;		OUTPUTS:
;			Returns 1 - Success.
;                       Returns 0 - Failure.
;		SIDE EFFECTS:
;			Writes a file.
; CALLS:
;	Indirect calls via specific feature class to various parts
;	of ADAS.
;	
; WRITTEN:
;       Chris Nicholas, University of Strathclyde
;
;       1.0   	Chris Nicholas
;              	Released to ADAS
;		11/05/07
;
;       1.1   	Allan Whiteford    
;              	Made setting a delay handler a method in it's own right.
;		Moved initial calls to setdesc and initpars from
;		individual features to central initalisation.
;		Changed initial call to setpars to initpars.
;              	Added top block of documentation
;		29/05/08
;
;       1.2     Chris Nicholas
;               Class now inherits from afg_primitive - see this class for
;               acquired functionality. Init and cleanup methods modified
;               accordingly.
;               Methods modified from procedure to function: 'setdesc', 
;               'setwvresolved', 'docalc', 'calc', 'writecode'.
;               Input for 'setwvresolved' changed from keyword to parameter.
;               Added error checking and message setting in:
;               'setdesc', 'setpars', 'setwvresolved', 'docalc', 'getcalc', 
;               'calc'.
;               Added third output option for 'beginwait' and 'endwait'.
;               Modified individual routine documentation above.
;               22/07/08 
;
;       1.3   	Martin O'Mullane
;              	Change SetPars and Calc methods calls to functional form
;               since IDL failed with 'undefined method' when the procedural
;               way was used.
;		27/05/2013
;-
;-----------------------------------------------------------------------------


;********************************************************************************
;+
; NAME:
;  afg_api::beginWait
;********************************************************************************
; PURPOSE:
;  when a delay is expected from length of time for calculation
;  this method should be called. This method calls the specified method of a
;  specified object from data member self.delayHandler.
;-
;********************************************************************************
FUNCTION afg_api::beginWait
  IF PTR_VALID(self.delayHandler) THEN $
    RETURN, CALL_METHOD((*self.delayHandler)[0].method, (*self.delayHandler)[0].object) $
  ELSE RETURN, 2
END


;********************************************************************************
;+
; NAME:
;  afg_api::endWait
;********************************************************************************
; PURPOSE:
;  when an expected delay from calculation has elapsed, this method should be
;  called. This method calls the specified method of a specified object from 
;  data member self.delayHandler.
; RETURNS:  0 - Failure.
;           1 - Success.
;           2 - No delay handler object.
;-
;********************************************************************************
FUNCTION afg_api::endWait
  IF PTR_VALID(self.delayHandler) THEN $
    RETURN, CALL_METHOD((*self.delayHandler)[1].method, (*self.delayHandler)[1].object) $
  ELSE RETURN, 2
END


;********************************************************************************
;+
; NAME:
;  afg_api::writeCode
;********************************************************************************
; PURPOSE:
;  Produces a file (name specified by input 'fileName') which will contain the
;  IDL code required to generate an object of the same type as the current one, in
;  its current state. This allows for easy integration of the feature into an 
;  external program
;-
;********************************************************************************
function afg_api::writeCode, fileName
  pars = self->getPars()
  desc = self->getDesc()
  parNames = TAG_NAMES(pars)
  
  OPENW, unit, fileName, /GET_LUN
  ;find the last occurence of the '/' character to extract filename from path
  pos = STRPOS(fileName ,'/',/REVERSE_SEARCH) + 1
  ;find the last occurence of the '.' character to remove the file extension
  extPos = STRLEN(fileName)-STRPOS(fileName ,'.',/REVERSE_SEARCH)
  
  
  PRINTF, unit, ";**************************************************"
  PRINTF, unit, ";* ADAS Feature Framework Example
  PRINTF, unit, ";* Created: ", SYSTIME()
  PRINTF, unit, ";**************************************************"
  PRINTF, unit, ";* The following provides an example of how to use" 
  PRINTF, unit, ";* the underlying ADAS feature framework to"
  PRINTF, unit, ";* generate the feature for use in 3rd party codes"
  PRINTF, unit, ";*"  
  
  PRINTF, unit, ";* The object's class type and available methods are"
  PRINTF, unit, ";* outlined below."
  PRINTF, unit, ";*"
  class = OBJ_CLASS(self)  
  PRINTF, unit, ";* OBJECT CLASS: ", class

  
  func_info = ROUTINE_INFO(/FUNCTIONS)
  proc_info = ROUTINE_INFO()
  
  func_methods = func_info[where(stregex(func_info, (class+'::.+'), /boolean) EQ 1, func_count)]
  proc_methods = proc_info[where(stregex(proc_info, (class+'::.+'), /boolean) EQ 1, proc_count)]
     
  PRINTF, unit, ";*   FUNCTION METHODS:"
  FOR i=0, func_count-1 DO  PRINTF, unit, ";*     ", func_methods[i]  
  PRINTF, unit, ";*   PROCEDURE METHODS:"
  FOR i=0, proc_count-1 DO  PRINTF, unit, ";*     ", proc_methods[i]
  PRINTF, unit, ";*"  
  
  PRINTF, unit, ";* The object's  superclass and available methods are"
  PRINTF, unit, ";* outlined below."
  PRINTF, unit, ";*"
  superClass = OBJ_CLASS(self, /SUPERCLASS)  
  PRINTF, unit, ";* SUPERCLASS: ", superClass
  
  ;reset counters
  func_count = 0
  proc_count = 0  
    
  func_methods = func_info[where(stregex(func_info, (superClass+'::.+'), /boolean) EQ 1, func_count)]
  proc_methods = proc_info[where(stregex(proc_info, (superClass+'::.+'), /boolean) EQ 1, proc_count)]
    
  PRINTF, unit, ";*   FUNCTION METHODS:"
  FOR i=0, func_count-1 DO  PRINTF, unit, ";*     ", func_methods[i]  
  PRINTF, unit, ";*   PROCEDURE METHODS:"
  FOR i=0, proc_count-1 DO  PRINTF, unit, ";*     ", proc_methods[i]
  PRINTF, unit, ";**************************************************"  
  
  valuestring = strarr(N_TAGS(pars))
  
  FOR i=0, N_TAGS(pars)-1 DO BEGIN
    IF STRLOWCASE(desc.parameters.(i).type) EQ 'string' $
      OR STRLOWCASE(desc.parameters.(i).type) EQ 'selection' $
      OR STRLOWCASE(desc.parameters.(i).type) EQ 'file' $
    THEN  BEGIN
      valueString[i] = "'"+ STRTRIM(STRING(pars.(i)),2) + "'"
    ENDIF ELSE IF STRLOWCASE(desc.parameters.(i).type) EQ 'pointer' THEN BEGIN
      IF STRLOWCASE(desc.parameters.(i).dispType) EQ 'filelist' THEN BEGIN      
	IF PTR_VALID(pars.(i)) THEN BEGIN
          tempString = "PTR_NEW(['"
          FOR j=0, N_ELEMENTS(*(pars.(i)))-1 DO tempString = tempString + STRTRIM((*(pars.(i)))[j],2) + "', '"
	ENDIF 
	oldLength = STRLEN(tempString)
	tempString = STRMID(tempString,0, oldLength-4)
	valueString[i] = tempString + "'])"
      ENDIF
    ENDIF ELSE  valueString[i] = STRTRIM(STRING(pars.(i)),2)
  ENDFOR
  
  
  PRINTF, unit, "FUNCTION " + STRLOWCASE(STRMID(fileName, pos, STRLEN(fileName)-pos-extPos))
  PRINTF, unit, ";create the object:"
  PRINTF, unit, "  o = OBJ_NEW('" + STRLOWCASE(OBJ_CLASS(self)) + "')"
  PRINTF, unit, ""
  PRINTF, unit, ";obtain the feature parameters using getPars method"
  PRINTF, unit, ";which will return the parameter structure:"
  PRINTF, unit, "  pars = o->getPars()"
  PRINTF, unit, ""
  PRINTF, unit, ";modify each of the parameter values:"
  FOR i=0, N_TAGS(pars)-1 DO PRINTF, unit, "  pars." + STRTRIM(STRLOWCASE(parnames[i])) + "="+valueString[i]
  PRINTF, unit, ""  
  PRINTF, unit, ";alternatively you can set the parameters by defining a structure like this:"
  PRINTF, unit, ";  pars = {ADAS_FEATURE_" + STRUPCASE(OBJ_CLASS(self)) + ", $"
  FOR i=0, N_TAGS(pars)-2 DO PRINTF, unit, ";    " + parNames[i] + ": " + valueString[i] + ", $"
  ;special case of last par in structure 
  PRINTF, unit, ";    " + parNames[i] + ": " + valueString[N_TAGS(pars)-1] + " $"
  PRINTF, unit, ";  }"
  PRINTF, unit, ""
  PRINTF, unit, ";now set these values to be used by the feature object:"
  PRINTF, unit, "  ok=o->setPars(PARS=pars)"
  PRINTF, unit, ""
  PRINTF, unit, ";perform calculation using these parameters:"
  PRINTF, unit, "  ok=o->calc()"
  PRINTF, unit, ""
  PRINTF, unit, ";obtain the wavelength and intensity arrays:"  
  PRINTF, unit, "  wavelength=o->getWv()"
  PRINTF, unit, "  intensity=o->getIntensity()"
  PRINTF, unit, ""
  PRINTF, unit, ";you could, for example, produce a plot of this data:" 
  PRINTF, unit, "  PLOT, wavelength, intensity, XTITLE='wavelength', YTITLE='intensity'"
  PRINTF, unit, ""  
    
  PRINTF, unit, "RETURN, o"
  PRINTF, unit, "END"
  
  CLOSE, unit
  FREE_LUN, unit
  return, 1
END



;********************************************************************************
;+
; NAME:
;  afg_api::calc
;********************************************************************************
; PURPOSE:
;  This should be over-ridden by classes inheriting from this class (subclasses).
;  It is only included here as a template (interface). The required method must
;  store results of the calculation into self.wv and self.intensity.
;-
;********************************************************************************
function afg_api::calc, _Extra=e
  IF N_ELEMENTS(e) GT 0 THEN BEGIN
    pars = self->getPars()
    eNames = TAG_NAMES(e)
    FOR i=0, N_TAGS(e)-1 DO BEGIN
      matches = where (TAG_NAMES(pars) EQ eNames[i], count)
      IF count EQ 1 THEN pars.(matches[0]) = e.(i)
    ENDFOR
  
    IF self->setPars(PARS=pars) EQ 0 then return, 0
  ENDIF
    
  return, self->doCalc()
END





;********************************************************************************
;+
; NAME:
;  afg_api::getCalc
;********************************************************************************
; PURPOSE:
;  Executes calc method and then returns the values which should have been stored
;  into self.wv and self.intensity as a structure.
;-
;********************************************************************************
FUNCTION afg_api::getCalc, _Extra=e
  
  if self->calc(_Extra=e) eq 0 then begin
    fail = self->seterrmsg(self->geterrmsg()+'Error has occurred when running "calc" method.')
    print, self->geterrmsg()
    return,{wv:findgen(512),intensity:fltarr(512)}
  endif  
  RETURN, {WV:*self.wv, INTENSITY:*self.intensity}
END


;********************************************************************************
;+
; NAME:
;  afg_api::getres
;********************************************************************************
; PURPOSE:
;  Returns the values which stored into self.wv and self.intensity as
;  a structure, similar to getcalc but does not perform the calculation
;  before returning result.
;-
;********************************************************************************
FUNCTION afg_api::getres, _Extra=e
    RETURN, {WV:*self.wv, INTENSITY:*self.intensity}
END



;********************************************************************************
;+
; NAME:
;  afg_api::doCalc
;********************************************************************************
; PURPOSE:
;  This should be over-ridden by classes inheriting from this class (subclasses).
;  It is only included here as a template (interface). The required method must
;  store results of the calculation into self.wv and self.intensity.
;-
;********************************************************************************
function afg_api::doCalc

  PRINT, 'no calculation has taken place...'
  PRINT, 'class inheriting from afg_api should be providing a calculate method' 
  return, self->seterrmsg('class inheriting from afg_api' + $
    'should be providing a calculate method') 
END

;********************************************************************************
;+
; NAME:
;  afg_api::setWvResolved
;********************************************************************************
; PURPOSE:
;  Sets value of the data member self.wvResolved. This should be 1 or 0;
;  corresponding to the calculated intensity data being wavelength resolved or not.
;-
;********************************************************************************
function afg_api::setWvResolved, in
    if size(in, /type) ne 2 then $
        return, self->seterrmsg('Incorrect type - must be an integer (1 or 0)')
    if in gt 1 or in lt 0 then $
        return, self->seterrmsg('must be an integer (1 or 0)')
    self.wvresolved = in
    return, 1     
END

;********************************************************************************
;+
; NAME:
;  afg_api::getWvResolved
;********************************************************************************
; PURPOSE:
;  Returns the value of the data member self.wvResolved. This should be 1 or 0;
;  corresponding to the calculated intensity data being wavelength resolved or not.
;-
;********************************************************************************
FUNCTION afg_api::getWvResolved
  RETURN, self.wvResolved
END

;********************************************************************************
;+
; NAME:
;  afg_api::setPars
;********************************************************************************
; PURPOSE:
;  Sets data member self.pars to input 'pars' (a pointer to a strucutre
;  containing all of the parameter values).
;-
;********************************************************************************
FUNCTION afg_api::setPars, PARS=pars

    IF N_ELEMENTS(pars) eq 0 THEN $
        return, self->seterrmsg('failure, no parameter structure input')
    IF PTR_VALID(self.pars) THEN PTR_FREE, self.pars
    self.pars = PTR_NEW(pars)
    desc = self->getDesc()
    found=0
    FOR i=0, N_TAGS(desc.parameters)-1 DO $
      IF desc.parameters.(i).alterslimits EQ 1 THEN $
        RETURN, self->refreshLimits()
    RETURN, 1
END

;************************************************************
;+
; NAME:
;  afg_api::getPars
;************************************************************
; PURPOSE:
;  Returns the parameter structure.
;  (De-references self.pars which is stored as a pointer).
;-
;************************************************************
FUNCTION afg_api::getPars
  RETURN, *self.pars
END

;********************************************************************************
;+
; NAME:
;  afg_api::setDesc
;********************************************************************************
; PURPOSE:
;  Sets data member self.desc to input 'desc' (a pointer to a structure
;  containing descriptive information on each of the parameters).
;-
;********************************************************************************
FUNCTION afg_api::setDesc, DESC=desc
    IF N_ELEMENTS(desc) eq 0 THEN return, self->seterrmsg('failure, no description structure input')
    IF PTR_VALID(self.desc) THEN PTR_FREE, self.desc
    self.desc = PTR_NEW(desc)
    return, 1
END

;************************************************************
;+
; NAME:
;  afg_api::getDesc
;************************************************************
; PURPOSE:
;  Returns the parameter description structure.
;  (De-references self.desc which is stored as a pointer).
;-
;************************************************************
FUNCTION afg_api::getDesc
  RETURN, *self.desc
END



;************************************************************
;+
; NAME:
;  afg_api::getWv
;************************************************************
; PURPOSE:
;  Returns the wavelength array.
;  (De-references self.wv which is stored as a pointer).
;-
;************************************************************
FUNCTION afg_api::getWv
  RETURN, *self.wv
END

;************************************************************
;+
; NAME:
;  afg_api::getIntensity
;************************************************************
; PURPOSE:
;  Returns the intensity array.
;  (De-references self.intensity which is stored as a pointer).
;-
;************************************************************
FUNCTION afg_api::getIntensity
  RETURN, *self.intensity
END


;************************************************************
;+
; NAME:
;  afg_api::cleanup
;************************************************************
; object cleanup method frees memory associated with any
; non-primitive data members from the clas definition.
;-
;************************************************************
PRO afg_api::cleanup
  ;;clear any pointers or object references.
  IF PTR_VALID(self.pars) THEN PTR_FREE, self.pars
  IF PTR_VALID(self.desc) THEN PTR_FREE, self.desc
  IF PTR_VALID(self.wv) THEN PTR_FREE, self.wv
  IF PTR_VALID(self.intensity) THEN PTR_FREE, self.intensity
  IF PTR_VALID(self.delayHandler) THEN PTR_FREE, self.delayHandler
  self->afg_primitive::cleanup
END


function afg_api::setdelayhandler, delayHandler

  if n_elements(delayhandler) gt 0 then begin
    if ptr_valid(delayhandler) eq 0 then return, 0
    delaystruct = *delayhandler
    ptr_free, delayhandler
    if size(delaystruct, /tname) ne 'STRUCT' then return, 0  
    for i=0, 1 do begin
      dummy = where(tag_names(delaystruct[i]) eq 'OBJECT' or tag_names(delaystruct[i]) eq 'METHOD', count)
      if count ne 2 then return, 0
    endfor
    self.delayhandler = ptr_new(delaystruct)
  endif else begin
  	return,0
  endelse
  
  return, 1
end


;************************************************************
;+
; NAME:
;  afg_api::init
;************************************************************
;  The method initializes the object values, i.e. performs
;  the creation and realization steps.
;-
;************************************************************
FUNCTION afg_api::init, debug=debug
  if self->afg_primitive::init(debug=debug) eq 0 then return, 0
  if self->setDesc() eq 0 then return, 0
  if self->initpars() eq 0 then return, 0
  
  RETURN, 1
END


PRO afg_api__define

  define = {afg_api, $
             pars:PTR_NEW(), $
	     desc:PTR_NEW(), $
	     wv:PTR_NEW(), $
	     intensity:PTR_NEW(), $
	     wvResolved:0, $
	     delayHandler:PTR_NEW(), $
	     inherits afg_primitive $
  }


END

 
