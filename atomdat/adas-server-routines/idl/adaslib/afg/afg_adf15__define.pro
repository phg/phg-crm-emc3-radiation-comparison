;----------------------------------------------------------------------
;+
; PROJECT:
;       ADAS
;
; NAME:
;       AFG_ADF15
;
; PURPOSE:
;       Class to generate a set of intensities selected from a supplied
;       photon emissivity file (adf15) and selected block within that file
;       in the ADAS Feature Generation (AFG) system.
;       Provides setdesc, docalc and initpars methods as required
;       by AFG.
;
; EXPLANATION:
;       This class inherits from AFG_API to provide an object representing a
;       number of emission lines. It provides feature-specific methods which
;       override the base-class placeholder methods.
;
;
; USE:
;
;   Lyman series for Be3+. Note the way the blocks are set, The field is
;   a pointer and the choice (either an array or a single PEC) is given
;   as a string.
;
;   obj  = obj_new('afg_adf15')
;   pars = obj->getpars()
;
;   pars.filename = '/home/adas/adas/adf15/pec96#be/pec96#be_pju#be3.dat'
;   *pars.block   = '[7,4,2,1]'
;   pars.te       = 65.0
;   pars.dens     = 1.0e12
;
;   ok     = obj->setpars(pars=pars)
;   result = obj->getcalc()
;
;   plot, result.wv, result.intensity, /nodata
;
;   for i = 0, n_elements(result.wv)-1 do $
;     oplot, [result.wv[i],result.wv[i]], [0,result.intensity[i]]
;
;
; PUBLIC ROUTINES:
;       Please see the documentation for AFG_API for an explanation
;       of how to use this class. No public methods are added by
;       the inheritence of this class.
;
; CALLS:
;       AFG_API (via inheritance)
;       read_adf15
;
;
; AUTHOR     :  Martin O'Mullane
; DATE       :  03-08-2015
;
;
; UPDATE     :
;       1.1     Martin O'Mullane
;                 - First version
;
; VERSION    :
;       1.1     03-08-2015
;-
;--------------------------------------------------------------------------------------


FUNCTION afg_adf15::initpars

     ptrtoblocks = ptr_new([2])

     pars = {filename : 'NULL',      $
             block    : ptrtoblocks, $
             te       : 7.0,         $
             dens     : 5.6e13       }

    return, self->setpars(pars=pars)

END
;--------------------------------------------------------------------------------------


FUNCTION afg_adf15::setDesc

    parameters = {filename  : {desc         : 'adf15 filename',          $
                               type         : 'string',                  $
                               units        : 'none',                    $
                               disptype     : 'file',                    $
                               alterslimits : 1},                        $
                  block     : {desc         : 'PEC selection',           $
                               type         : 'pointer',                 $
                               units        : 'None',                    $
                               disptype     : 'integers',                $
                               alterslimits : 0},                        $
                  te        : {desc         : 'electron temperature',    $
                               type         : 'float',                   $
                               units        : 'eV',                      $
                               min          : 0.2,                       $
                               max          : 1.0e5,                     $
                               disptype     : 'continuous',              $
                               log          : 1,                         $
                               alterslimits : 0},                        $
                  dens      : {desc         : 'electron density',        $
                               type         : 'float',                   $
                               units        : 'cm-3',                    $
                               min          : 1.0e4,                     $
                               max          : 1.0e16,                    $
                               disptype     : 'continuous',              $
                               log          : 1,                         $
                               alterslimits : 0}                         $
                 }

    desc = {name       : 'ADF15 emissivities',                             $
            text       : 'Photon emissivities of a number of transitions', $
            parameters : parameters                                   }

  return, self->afg_api::setDesc(DESC=desc)

END
;--------------------------------------------------------------------------------------


FUNCTION afg_adf15::findLimits

    if self.filename NE 'NULL' then begin

       read_adf15, file=self.filename, fulldata=fulldata

       nblocks  = n_elements(fulldata.nte)
       telims   = [min(fulldata.te[*,0]), max(fulldata.te[*,0])]
       denslims = [min(fulldata.dens[*,0]), max(fulldata.dens[*,0])]

    endif else begin

       telims   = [0.0, !values.f_infinity]
       denslims = [0.0, !values.f_infinity]

    endelse

    desc = self->getdesc()
    desc.parameters.te.min = telims[0]
    desc.parameters.te.max = telims[1]
    desc.parameters.dens.min = denslims[0]
    desc.parameters.dens.max = denslims[1]

    return, self->afg_api::setDesc(DESC=desc)

END
;--------------------------------------------------------------------------------------


FUNCTION afg_adf15::refreshLimits

    pars = self->getpars()

    IF self.filename EQ pars.filename THEN RETURN, 1 ELSE BEGIN
      self.filename = pars.filename
      IF self->findLimits() EQ 1 THEN RETURN, 2 ELSE RETURN, 0
    ENDELSE

END
;--------------------------------------------------------------------------------------


FUNCTION afg_adf15::doCalc

    if self->setWvResolved(0) eq 0 then return, 0

; Changeable parameters - extract blocks from a string - elaborate testing
; for adas605.

    pars   = self->getPars()
    te     = pars.te
    dens   = pars.dens
    file   = pars.filename

    if size((*pars.block), /type) NE 7 then begin
       block = [0]
    endif else begin
       str = (*pars.block)[0]
       cmd = 'block = ' + str
       if strcompress(str,/remove_all) NE '' then res = execute(cmd) $
                                             else block = [0]
    endelse

; Fixed parameters -- none

    res   = where(block GT 0, num)

    if (num) GT 0 then begin

       wv    = fltarr(num)
       emiss = fltarr(num)

       for j = 0, num-1 do begin

          read_adf15, file=file, block=block[j], te=te, dens=dens, data=pec, wlngth=wave

          wv[j]    = wave
          emiss[j] = pec

       endfor

    endif else begin
       wv    = [-1.0, -1.0]
       emiss = [-1.0, -1.0]
    endelse

    IF PTR_VALID(self.wv) THEN PTR_FREE, self.wv
    self.wv = PTR_NEW(wv)

    IF PTR_VALID(self.intensity) THEN PTR_FREE, self.intensity
    self.intensity = PTR_NEW(emiss)

    return, 1
END
;--------------------------------------------------------------------------------------


PRO afg_adf15::cleanup
    self->afg_api::cleanup
END
;--------------------------------------------------------------------------------------


FUNCTION adf15::init, debug=debug
    IF self->afg_api::init(debug=debug) EQ 0 THEN RETURN, 0
    RETURN, 1
END
;--------------------------------------------------------------------------------------


PRO afg_adf15__define
    define = {afg_adf15,            $
              INHERITS afg_api ,    $
              block    : ptr_new(), $
              filename : ''         }
END
