;+
; PROJECT:
;       ADAS
;
; NAME:
;       AFG_ZEEMAN
;
; PURPOSE:
;       Class to represent the Zeeman feature (coming from ADAS603)
;       in the ADAS Feature Generation (AFG) system.
;       Provides setdesc, docalc and initpars methods as required
;       by AFG.
;
; EXPLANATION:
;       This class inherits from AFG_API to provide an object
;       representing a Zeeman feature. It provides feature-specific
;       methods which override the base-class placeholder methods.
;       Specifcally it contains the code which interfaces the
;       generalised representation of AFG to the specific ADAS603
;       subroutines.
;
; USE:
;       obj=obj_new('afg_zeeman')
;       pars=obj->getpars()
;       pars.obsangle=80.0
;       pars.findex=15
;       ok=obj->setpars(pars=pars)
;       result=obj->getcalc()
;       plot,result.wv,result.intensity,/nodata
;       for i=0,n_elements(result.wv)-1 do $
;         oplot,[result.wv[i],result.wv[i]],[0,result.intensity[i]]
;       
; PUBLIC ROUTINES:
;       Please see the documentation for AFG_API for an explanation
;       of how to use this class. No public methods are added by
;       the inheritence of this class.
;
; CALLS:
;       AFG_API (via inheritance)
;       ADAS603_GET_ZEEMAN
;       ADAS603_GET_MULTIPLETS
;
; WRITTEN:
;       Chris Nicholas, University of Strathclyde
;
;       1.0     Chris Nicholas and Allan Whiteford 
;               Released to ADAS
;               22/07/08
;       1.1     Allan Whiteford
;               Added documentation
;               Changed internal formatting
;               First release with ADAS
;               ??/??/??
;-

FUNCTION afg_zeeman::initpars
    pars={                 $
          pol:      1     ,$
          obsangle: 90.0  ,$
          bvalue:   2.5   ,$
          findex:   15     }
    RETURN, self->setPars(PARS=pars)
END

function afg_zeeman::setDesc
    
    desc = { $
                name:'Zeeman Feature', $
                text:'ADAS implementaion of Zeeman features based on XPaschen '+$
                     'code, original implementation in ADAS is ADAS603.', $
                parameters:{ $
                    pol:        { desc:'Polarisation Filtering:',$
                                  type:'integer', $
                                  units: 'none', $
                                  dispType:'selection', $
                                  values:['NONE','PI+SIGMA', 'PI', 'SIGMA', 'SIGMA+', 'SIGMA-'], $
                                  log:0, $
                                  altersLimits:0}, $ 
                    obsangle:   { desc:'Observation angle (relative to field)', $
                                  type:'float', $
                                  units: 'degrees', $
                                  min: '0.0', $
                                  max: '90.0', $
                                  dispType:'continuous', $
                                  log:0, $
                                  altersLimits:0  }, $
                    bvalue:     { desc:'Magnetic field strength (T)', $
                                  type:'float', $
                                  units: 'T', $
                                  min: 0.0, $
                                  max: 20.0, $
                                  dispType:'continuous', $
                                  log:0, $
                                  altersLimits:0  }, $
                    findex:     { desc:'Feature index', $
                                  type:'integer', $
                                  units: 'none', $
                                  min: 1, $
                                  max: 30, $  
                                  dispType:'selection', $
                                  values:adas603_get_multiplets(), $
                                  log:0, $
                                  altersLimits:0} $      
                } $      
          }
  return, self->afg_api::setDesc(DESC=desc)
END

function afg_zeeman::doCalc

    if self->setWvResolved(0) eq 0 then return, 0
    pars = self->getPars()

    fortdir=getenv('ADASFORT')
      
    pol = pars.pol
    obsangle=pars.obsangle
    bvalue= pars.bvalue
    findex=pars.findex
   
    adas603_get_zeeman, fortdir, pol, obsangle, bvalue, findex, $
                        m_wlength, m_strength    
 
    IF PTR_VALID(self.wv) THEN PTR_FREE, self.wv
    self.wv = PTR_NEW(m_wlength)  

    IF PTR_VALID(self.intensity) THEN PTR_FREE, self.intensity   
    self.intensity = PTR_NEW(m_strength)
   
    return, 1
END

PRO afg_zeeman::cleanup
    self->afg_api::cleanup
END

FUNCTION afg_zeeman::init, debug=debug
    IF self->afg_api::init(debug=debug) EQ 0 THEN RETURN, 0
    RETURN, 1
END

PRO afg_zeeman__define
    define = {afg_zeeman,      $
              INHERITS afg_api }
END

 
