;----------------------------------------------------------------------
;+
; PROJECT    :  ADAS
;
; NAME       :  write_adf01
;
; PURPOSE    :  Write an adf01 specific ion file using the
;               fulldata structure (from read_adf01 or elsewhere).
;
;
; ARGUMENTS  :  All output arguments will be defined appropriately.
;
;               NAME      I/O    TYPE    DETAILS
; REQUIRED   :  fulldata   I       -     Structure containing the
;                                        adf01 details.
;               outfile    O      str    name of new adf01 file.
;               comments   I      str()  write these comments at end.
;
; KEYWORDS      help               -     if specified this comment
;                                        section is written to screen.
;
;
; NOTES      :
;
; The fulldata structure is defined:
;
;    fulldata = { filename    :  file,        $
;                 symbr       :  symbr,       $
;                 symbd       :  symbd,       $
;                 izr         :  izr,         $
;                 izd         :  izd,         $
;                 indd        :  indd,        $
;                 nenrgy      :  nenrgy,      $
;                 nmin        :  nmin,        $
;                 nmax        :  nmax,        $
;                 lparms      :  lparms,      $
;                 lsetl       :  lsetl,       $
;                 enrgya      :  enrgya,      $
;                 alphaa      :  alphaa,      $
;                 lforma      :  lforma,      $
;                 xlcuta      :  xlcuta,      $
;                 pl2a        :  pl2a,        $
;                 pl3a        :  pl3a,        $
;                 sigta       :  sigta,       $
;                 signa       :  signa,       $
;                 sigla       :  sigla        }
;
;
; AUTHOR     :  Martin O'Mullane
;
; DATE       :  05-07-2007
;
;
; MODIFIED:
;       1.1     Martin O'Mullane
;               - First version.
;       1.2     Martin O'Mullane
;               - Do not close file before writing comments.
;       1.3     Martin O'Mullane
;               - Make sure donor and receiver strings have a
;                 a length of 2 characters.
;
; VERSION:
;       1.1    05-07-2007
;       1.2    12-09-2007
;       1.3    04-06-2019
;-
;----------------------------------------------------------------------


PRO write_adf01, fulldata = fulldata,  $
                 outfile  = outfile,   $
                 comments = comments,  $
                 help     = help

; If asked for help

if keyword_set(help) then begin
   doc_library, 'write_adf01'
   return
endif

; Extract necessary parts to write

symbr   = fulldata.symbr
symbd   = fulldata.symbd
izr     = fulldata.izr
izd     = fulldata.izd
indd    = fulldata.indd
nenrgy  = fulldata.nenrgy
nmin    = fulldata.nmin
nmax    = fulldata.nmax
lparms  = fulldata.lparms
lsetl   = fulldata.lsetl
enrgya  = fulldata.enrgya
alphaa  = fulldata.alphaa
lforma  = fulldata.lforma
xlcuta  = fulldata.xlcuta
pl2a    = fulldata.pl2a
pl3a    = fulldata.pl3a
sigta   = fulldata.sigta
signa   = fulldata.signa
sigla   = fulldata.sigla

s_symbr = '  '
strput, s_symbr, symbr
s_symbd = '  '
strput, s_symbd, symbd

openw, lun, outfile, /get_lun

if lparms EQ 1 then s_lp = '/ LPARMS /' else s_lp = '/        /'

str = s_symbr + '+' + string(izr, format='(i2)') + '     ' + $
      s_symbd + '+' + string(izd, format='(i2)') + $
      ' (' + string(indd, format='(i1)') + $
      ')  / receiver, donor (donor state index)    ' + s_lp

printf, lun, str


nblock = numlines(nenrgy, 9)

for j = 0, nblock-1 do begin

  neng = min([9, nenrgy - j*9])

  printf, lun, string(neng, format='(i5)') + $
               '                / number of energies'
  printf, lun, string(nmin, format='(i5)') + $
               '                / nmin'
  printf, lun, string(nmax, format='(i5)') + $
               '                / nmax'

  i1 = j*9
  i2 = min([nenrgy, (j+1)*9])-1

  ; Mandatory parameters

  str = string(replicate(32B, 91))
  strput, str, string(enrgya[i1:i2]/1000.0, format='(9f9.2)'), 10
  str = str + ' / energies         (keV/amu)'
  printf, lun, str

  str = string(replicate(32B, 91))
  strput, str, string(alphaa[i1:i2], format='(9f9.2)'), 10
  str = str + ' / alpha'
  printf, lun, str

  ; l-parameters if supplied

  if lparms EQ 1 then begin

     str = string(replicate(32B, 91))
     strput, str, string(lforma[i1:i2], format='(9i9)'), 10
     str = str + ' / lforma'
     printf, lun, str

     str = string(replicate(32B, 91))
     strput, str, string(xlcuta[i1:i2], format='(9E9.2)'), 10
     str = str + ' / xlcuta'
     printf, lun, str

     str = string(replicate(32B, 91))
     strput, str, string(pl2a[i1:i2], format='(9E9.2)'), 10
     str = str + ' / pl2a'
     printf, lun, str

     str = string(replicate(32B, 91))
     strput, str, string(pl3a[i1:i2], format='(9E9.2)'), 10
     str = str + ' / pl3a'
     printf, lun, str

  endif

  ; now the cross section data

  str = string(replicate(32B, 91))
  strput, str, string(sigta[i1:i2], format='(9E9.2)'), 10
  str = str + ' / total xsects.    (cm2)'
  printf, lun, str

  printf, lun, '  n  l  m                                              ' +$
               '                                     / partial xsects.  (cm2)'

  for in = nmin, nmax do begin

     str = string(in, format='(i3)') + '       ' + $
           string(signa[i1:i2, in-1], format='(9E9.2)')
     printf, lun, str
     
     if sigla[0] NE -1 then begin 
        for il = 0, in-1 do begin

           ind = i4idfl(in, il)
           str = string(in, format='(i3)') +  $
                 string(il, format='(i3)') + '    ' + $
                 string(sigla[i1:i2, ind-1], format='(9E9.2)')
           printf, lun, str

        endfor
     endif

  endfor

endfor

; Terminator

printf, lun, '   -1   -1'


; Append comments if supplied

n_com = n_elements(comments)

if n_com GT 0 then for j = 0, n_com-1 do printf, lun, comments[j]


free_lun, lun

END
