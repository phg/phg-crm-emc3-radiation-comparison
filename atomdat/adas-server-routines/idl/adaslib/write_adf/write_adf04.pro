;----------------------------------------------------------------------
;+
; PROJECT    :  ADAS
;
; NAME       :  write_adf04
;
; PURPOSE    :  Write an adf04 specific ion file using the
;               fulldata structure (from read_adf04 or elsewhere).
;
;
; ARGUMENTS  :  All output arguments will be defined appropriately.
;
;               NAME      I/O    TYPE    DETAILS
; REQUIRED   :  fulldata   I       -     Structure containing the
;                                        adf04 details.
;               outfile    O      str    name of new adf04 file.
;               comments   I      str()  write these comments at end.
;
; OPTIONAL   :  options    I      struc  Structure with directives to
;                                        change the output from dffault.
;                                          no_x : 1 => do not write {X} if no
;                                                      zpla data is present.
;                                                 0 => write {X}
;
; KEYWORDS      filter             -     run new adf04 file through
;                                        command line filter04 routine.
;                                        nb: this removes R/S/H lines,
;                                            parent and ion path info.
;               sort               -     sorts excitation indices
;               help               -     if specified this comment
;                                        section is written to screen.
;
;
; NOTES      :  Not a lot of testing yet. The full flexibility of
;               the adf04 format is not supported.
;
; The fulldata structure is defined:
;
;     fulldata = { filename         :  file,              $
;                  iz               :  iz,                $
;                  iz0              :  iz0,               $
;                  ia               :  ia,                $
;                  cstrga           :  cstrga,            $
;                  isa              :  isa,               $
;                  ila              :  ila,               $
;                  xja              :  xja,               $
;                  wa               :  wa,                $
;                  zpla             :  zpla,              $
;                  ipla             :  ipla,              $
;                  bwno             :  bwno,              $
;                  bwnoa            :  bwnoa,             $
;                  cprta            :  cprta,             $
;                  eorb             :  eorb,              $
;                  te               :  scef,              $
;                  lower            :  lower_set,         $
;                  upper            :  upper_set,         $
;                  aval             :  aval_set,          $
;                  gamma            :  gamma_set,         $
;                  level_rec        :  level_rec,         $
;                  parent_rec       :  parent_rec,        $
;                  rec              :  rec,               $
;                  level_ion        :  level_ion,         $
;                  parent_ion       :  parent_ion,        $
;                  ion              :  ion,               $
;                  level_ion_rate   :  level_ion_rate,    $
;                  parent_ion_rate  :  parent_ion_rate,   $
;                  ion_rate         :  ion_rate,          $
;                  ion_source       :  source_rate,       $
;                  level_cx         :  level_cx,          $
;                  parent_cx        :  parent_cx,         $
;                  cx               :  cx,                $
;                  beth             :  beth               }
;
;
; AUTHOR     :  Martin O'Mullane
;
; DATE       :  07-02-2001
;
;
; MODIFIED:
;       1.1     Martin O'Mullane
;               - First version.
;       1.2     Martin O'Mullane
;               - Check for valid R, S or H lines before writing file.
;       1.3     Martin O'Mullane
;               - Alter writing configurations line to use actual
;                 width of configuration and maximum L quantum number.
;       1.4     Martin O'Mullane
;               - Use ia[] rather than the index when writing level line.
;       1.5     Martin O'Mullane
;               - If a Bethe limit point is present write it to outfile,
;                 unless all the values are zero (this happens when using a
;                 fulldata structure taken from a read_adf04 call).
;       1.6     Martin O'Mullane
;               - Long (not integer) index for writing excitation transitions.
;       1.7     Martin O'Mullane
;               - Use GE 10 rather than GT 10 when choosing a field length.
;       1.8     Martin O'Mullane
;               - Use iadftyp for the Te line.
;               - ignore lbeth when checking for limit point.
;       1.9     Martin O'Mullane
;               - Add /sort keyword to sort the transitions according to
;                 transition index.
;       1.10    Martin O'Mullane
;               - Check for orbital energies and write to output file 
;                 if present.
;       1.11    Martin O'Mullane
;               - Adjust xja format to fit largest value.
;       1.12    Martin O'Mullane
;               - Use a predefined strarr to hold the transitions line rather 
;                 than growing the output array because of a significant
;                 slow-down for cases with a large number of transitions.
;       1.13    Martin O'Mullane
;               - Increase number of significant difist for bwno to 4 to
;                 match energy levels.
;               - Add options structure to fine-tune output. no_x is the
;                 only directive. 
;
; VERSION:
;       1.1    30-03-2006
;       1.2    13-06-2006
;       1.3    03-08-2009
;       1.4    05-05-2010
;       1.5    09-09-2012
;       1.6    15-11-2012
;       1.7    30-11-2015
;       1.8    24-10-2016
;       1.9    24-07-2017
;       1.10   03-08-2018
;       1.11   19-09-2018
;       1.12   15-03-2019
;       1.13   20-06-2019
;-
;----------------------------------------------------------------------


FUNCTION write_adf04_str, array

tmp = string(array, format='(50(e9.2,:))')
str = strjoin(strsplit(tmp, 'e', /extract))

return, str

END
;---------------------------------------------------------------------------



FUNCTION write_adf04_orb_str, array

num  = n_elements(array)
sz   = floor(alog10(array))+1
str  = ''
for j = 0, num-1 do begin
   case sz[j] of
      3 : str = str + string(array[j], format='(f8.3)')
      2 : str = str + string(array[j], format='(f8.4)')
   else : str = str + string(array[j], format='(f8.5)')
   endcase
endfor

return, str

END
;---------------------------------------------------------------------------



PRO write_adf04, fulldata = fulldata,  $
                 outfile  = outfile,   $
                 filter   = filter,    $
                 comments = comments,  $
                 sort     = sort,      $
                 options  = options,   $
                 help     = help

; If asked for help

if keyword_set(help) then begin
   doc_library, 'write_adf04'
   return
endif

; Internal options

out_directives = { no_x : 0}

if n_elements(options) EQ 1 then begin
    if options.no_x EQ 1 then out_directives.no_x = 1
endif

; Extract necessary parts to write

iz       = fulldata.iz
iz1      = fulldata.iz + 1
iz0      = fulldata.iz0
ia       = fulldata.ia
cstrga   = fulldata.cstrga
isa      = fulldata.isa
ila      = fulldata.ila
xja      = fulldata.xja
wa       = fulldata.wa
bwnoa    = fulldata.bwnoa
cprta    = fulldata.cprta
zpla     = fulldata.zpla
ipla     = fulldata.ipla
te       = fulldata.te
lower    = fulldata.lower
upper    = fulldata.upper
aval     = fulldata.aval
gamma    = fulldata.gamma
iadftyp  = fulldata.iadftyp


n_level = n_elements(isa)
n_te    = n_elements(te)
n_tran  = n_elements(upper)

; Check for Bethe limit - check for beth() and ignore the lbeth logical

ind = where(tag_names(fulldata) EQ 'BETH', count)
if count EQ 1 then begin
   is_beth = 1
   beth = fulldata.beth
   if total(beth) EQ 0.0 then begin
      is_beth=0
      message, 'No meaningful limit points - not written', /continue
   endif
endif else is_beth=0


; Are orbital energies present?

res = where(strpos(tag_names(fulldata), 'EORB') NE -1, c_orb)

if c_orb GT 0 then begin
   
   str_orb = ''
   if fulldata.eorb[0] NE -1 then str_orb = write_adf04_orb_str(fulldata.eorb)

endif else str_orb = ''

; Top line

esym = '  +'
strput, esym, strupcase(xxesym(iz0))
esym = esym + string(iz, format='(i2)')

str = string(esym, iz0, iz1, format='(a5,7x,i3,7x,i3)') + '     '
for j = 0, n_elements(bwnoa)-1 do str = str + string(bwnoa[j], cprta[j], format='(f11.4,a4)')

outfile_str = str


; Levels

if max(ila) GE 10 then ilgap = 2 else ilgap = 1

ilen = max(strlen(strtrim(cstrga)))

if ilen LT 18 then igap = 18 - ilen + 1 else igap = 1

len_xja = max([fix(alog10(xja)+4), 4])

fmt = '(i5,1x,a' + string(ilen) + ',' + string(igap) + $
      'x, "(",i1,")",i' + string(ilgap) +              $
      ',"(",f' + string(len_xja) + '.1,")",1x,f17.4)'
fmt = strcompress(fmt, /remove_all)

for i = 0, n_level-1 do begin

   str = string(ia[i], cstrga[i], isa[i], ila[i], xja[i], wa[i], format=fmt)

   if total(zpla[*,i]) GT 0 then begin

      sz = size(zpla, /dim)
      for j = 0, sz[0]-1 do begin
         if ipla[j,i] GT 0 then str = str + '   {' +                           $
                                      string(ipla[j,i], format='(i1)') + '}' + $
                                      string(zpla[j,i], format='(f5.3)')
      endfor

   endif else begin
      if out_directives.no_x EQ 0 then str = str + '   {X}'
   endelse
   
   outfile_str = [outfile_str, str]

endfor
outfile_str = [outfile_str, '   -1   ' + str_orb]


; Excitation collision strengths

if max(upper) LE 999 then begin
   exc_fmt = '(2i4)' 
   rs1_fmt = '(i3)'
   rs2_fmt = '(i1)'
   gap     =  '      ' 
endif else begin
   exc_fmt = '(2i5)' 
   rs1_fmt = '(i4)'
   rs2_fmt = '(i2)'  
   gap     =  '        ' 
endelse

str = write_adf04_str(te)
tmp = string(iz1, format='(f5.1)') + '    ' + string(iadftyp, format='(i1)') + gap + str
outfile_str = [outfile_str, tmp]

; Process transitions. Use index as sorting tag.

k1  = n_elements(outfile_str)
k2  = k1
tag = -1L

str_store = strarr(n_tran)

k3 = 0L
for j = 0L, n_tran-1 do begin

   if upper[j] NE lower[j] then begin
      
      tag = [tag, lower[j]*1000L + upper[j]]
      k2  = k2 + 1
      
      array = [aval[j], reform(gamma[j,*])]
      if is_beth EQ 1 then array = [array, beth[j]]
      str = write_adf04_str(array)
      tmp = string([upper[j], lower[j]], format=exc_fmt)
      
      ; outfile_str = [outfile_str, tmp + str]
      str_store[k3] = tmp + str
      k3 = k3 + 1
   
   endif else message, 'self transition : ' + string(upper[j]), /continue

endfor
if k3 GT 0 then outfile_str = [outfile_str, str_store[0:k3-1]]

; Sort if requested

if keyword_set(sort) then begin 

   if n_elements(tag) EQ 1 then message, 'No valid transitions'
   
   tag = tag[1:*]

   ind = sort(tag)
   
   outfile_str[k1:k2-1] = (outfile_str[k1:k2-1])[ind]
   
endif


; Other processes - R, S and H lines

if fulldata.level_rec[0] NE -1 then begin

   for j = 0, n_elements(fulldata.level_rec)-1 do begin

     str = 'R' + string(fulldata.level_rec[j], format=rs1_fmt) + '  +' + $
                 string(fulldata.parent_rec[j], format=rs2_fmt) + '        ' + $
                 write_adf04_str(fulldata.rec[j,*])

     outfile_str = [outfile_str, str]

   endfor

endif

if fulldata.level_cx[0] NE -1 then begin

   for j = 0, n_elements(fulldata.level_cx)-1 do begin

     str = 'H' + string(fulldata.level_cx[j], format=rs1_fmt) + '  +' + $
                 string(fulldata.parent_cx[j], format=rs2_fmt) + '        ' + $
                 write_adf04_str(fulldata.cx[j,*])

     outfile_str = [outfile_str, str]

   endfor
   
endif

if fulldata.level_ion[0] NE -1 then begin

   for j = 0, n_elements(fulldata.level_ion)-1 do begin

     str = 'S' + string(fulldata.level_ion[j], format=rs1_fmt) + '  +' + $
                 string(fulldata.parent_ion[j], format=rs2_fmt) + '        ' + $
                 write_adf04_str(fulldata.ion[j,*])

     outfile_str = [outfile_str, str]

   endfor

endif

outfile_str = [outfile_str, string(-1, format=exc_fmt)]
outfile_str = [outfile_str, string([-1,-1], format=exc_fmt)]


; Write out adf04 file, running filter04.x against it if requested

if keyword_set(filter) then begin

; Open temp file

   rval = strcompress(string(randomn(seed)), /remove_all)
   rand = strmid(rval, 4, strlen(rval))

   file_tmp = 'wadf04.tmp' + rand

   adas_writefile, file=file_tmp, all=outfile_str, comments=comments
   fortdir = getenv('ADASFORT')
   cmd = fortdir + '/filter04.x < ' + file_tmp + ' > ' + outfile

   spawn, cmd, /sh
   
   file_delete, file_tmp

endif else adas_writefile, file=outfile, all=outfile_str, comments=comments

END
