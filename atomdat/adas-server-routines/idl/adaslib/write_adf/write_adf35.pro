;----------------------------------------------------------------------
;+
; PROJECT    :  ADAS
;
; NAME       :  write_adf35
;
; PURPOSE    :  Write an adf35 power filter file using the
;               fulldata structure (from read_adf35 or elsewhere).
;
;
; ARGUMENTS  :  All output arguments will be defined appropriately.
;
;               NAME      I/O    TYPE    DETAILS
; REQUIRED   :  fulldata   I       -     Structure containing the
;                                        adf35 details.
;               outfile    O      str    name of new adf35 file.
;               comments   I      str()  write these comments at end.
;
; KEYWORDS      help               -     if specified this comment
;                                        section is written to screen.
;
;
; NOTES      :
;
; The fulldata structure is defined:
;
;    fulldata = { filename    :  file,   $ - filename
;                 edge        :  edge,   $ - energy of any edges
;                 energy      :  energy, $ - energy (eV)
;                 trans       :  trans   } - transmission fraction (log10).
;
;
; AUTHOR     :  Martin O'Mullane
;
; DATE       :  07-03-2008
;
;
; MODIFIED:
;       1.1     Martin O'Mullane
;               - First version.
;       1.2     Martin O'Mullane
;               - Use g format for energies.
;       1.3     Martin O'Mullane
;               - Increase the precision, and field length, of the
;                 energies and transmission vectors.
;       1.4     Martin O'Mullane
;               - Allow number of edges to be zero.
;
; VERSION:
;       1.1    07-03-2008
;       1.2    06-12-2012
;       1.3    04-06-2014
;       1.4    17-02-2017
;-
;----------------------------------------------------------------------


PRO write_adf35, fulldata = fulldata,  $
                 outfile  = outfile,   $
                 comments = comments,  $
                 help     = help

; If asked for help

if keyword_set(help) then begin
   doc_library, 'write_adf35'
   return
endif

; Extract necessary parts to write

energy = fulldata.energy
trans  = fulldata.trans

ind = where(strpos(tag_names(fulldata), 'EDGE') EQ 0, count) 
if count EQ 1 then begin
   edge   = fulldata.edge
   n_edge = n_elements(edge)
endif else n_edge = 0

n_energy = n_elements(energy)

iopt_low  = 0
iopt_high = 1

openw, lun, outfile, /get_lun

printf, lun, n_energy, n_edge, iopt_low, iopt_high, f='(4(i5))'
printf, lun, f='(80("-"))'
if n_edge GT 0 then printf, lun, edge, f='(8(g10.8))'
printf, lun, f='(80("-"))'

printf, lun, energy, f='(8(g12.9))'
printf, lun, trans, f='(8(f12.6))'


; Append comments if supplied

n_com = n_elements(comments)

if n_com GT 0 then for j = 0, n_com-1 do printf, lun, comments[j]

; Close up and finish

free_lun, lun

END
