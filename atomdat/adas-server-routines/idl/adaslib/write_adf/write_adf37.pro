;----------------------------------------------------------------------
;+
; PROJECT    :  ADAS
;
; NAME       :  write_adf37
;
; PURPOSE    :  Write an adf37 electron distribution file from a
;               fulldata structure.
;
;
; ARGUMENTS  :  All output arguments will be defined appropriately.
;
;               NAME      I/O    TYPE    DETAILS
; REQUIRED   :  fulldata   I       -     Structure containing the
;                                        adf37 details.
;               outfile    O      str    name of new adf37 file.
;               comments   I      str()  write these comments at end.
;
; KEYWORDS      help               -     if specified this comment
;                                        section is written to screen.
;
;
; NOTES      :
;
; The fulldata structure is defined:
;
;    fulldata = {title      :  header for file
;                icateg     :  category of file
;                                  1 => superposition
;                                  2 => numerical
;                ieunit     :  energy units of distribution function
;                                  1 => kelvin
;                                  2 => eV
;                nenerg     :  type 1 => number of distribution families
;                                     2 => number of energy points
;                nblock     :  type 1 => number of members in output family
;                                     2 => number of effective temperatures
;                nform1     :  type of threshold behaviour
;                                  1 => cutoff
;                                  2 => energy^param1
;                nform2     :  type of high-energy behaviour
;                                  1 => cutoff
;                                  2 => energy^(-param2[0])
;                                  3 => exp(-param2[0]*energy)
;                                  4 => exp(-param2[0]*energy^param2[1])
;                param1     :  parameter of threshold form
;                param2     :  parameter of high-energy form
;                ea         :  energy points of tabulation
;                                  array(nblock,nenergy)
;                fa         :  distribution function tabulation
;                                  array(nblock,nenergy)
;                mode_eng   :  most probable energy (eV), array(nblock)
;                median_eng :  median energy (eV), array(nblock)
;                teff       :  effective temperature (eV), array(nblock)
;
;
; AUTHOR     :  Martin O'Mullane
;
; DATE       :  19-02-2018
;
;
; MODIFIED:
;       1.1     Martin O'Mullane
;                 - First version.
;
; VERSION:
;       1.1    19-02-2018
;-
;----------------------------------------------------------------------


PRO write_adf37, fulldata=fulldata, outfile=outfile, comments=comments

; If asked for help

if keyword_set(help) then begin
   doc_library, 'write_adf37'
   return
endif

icateg = fulldata.icateg
ieunit = fulldata.ieunit
nenerg = fulldata.nenerg
nblock = fulldata.nblock
nform1 = fulldata.nform1
param1 = fulldata.param1
nform2 = fulldata.nform2
param2 = fulldata.param2
ea     = fulldata.ea
fa     = fulldata.fa

all = ['ADF37 - Electron energy distribution description', $
       string(icateg, format='(i8)') + '          ; format of file 1 => superposition; 2 => numerical', $
       string(ieunit, format='(i8)') + '          ; energy units 1 => K; 2 => eV', $
       string(nenerg, format='(i8)') + '          ; number of tabulated values', $
       string(nblock, format='(i8)') + '          ; number of tabulated temperatures']

case nform1 of
  1    : str = '    1             ; cutoff threshold'
  2    : str = string([nform1, param1], format='(i5, e10.2)') + '   ; cutoff threshold'
  else : message, 'Invalid cutoff threshold option'
endcase

all = [all, str]

case nform2 of

  1    : str = '    1             ; high energy behaviour'
  2    : str = string([nform2, param2], format='(i5, e10.2)') + '   ; high energy behaviour'
  3    : str = string([nform2, param2], format='(i5, e10.2)') + '   ; high energy behaviour'
  4    : str = string([nform2, param2], format='(i5, 2e10.2)') + '   ; high energy behaviour'
  else : message, 'Invalid high energy behaviour option'
endcase

all = [all, str]

fmt = '(7(2x,e8.2,:))'
for j = 0, nblock-1 do all = [all, string(ea[j,*], format=fmt), string(fa[j,*], format=fmt)]

if n_elements(comments) GT 0 then all = [all, comments]

adas_writefile, file=outfile, all=all

END
