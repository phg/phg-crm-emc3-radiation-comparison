;----------------------------------------------------------------------
;+
; PROJECT    :  ADAS
;
; NAME       :  write_adf25
;
; PURPOSE    :  Write an adf25 specific ion file using the
;               fulldata structure (from read_adf25 or elsewhere).
;
;
; ARGUMENTS  :  All output arguments will be defined appropriately.
;
;               NAME      I/O    TYPE    DETAILS
; REQUIRED   :  fulldata   I       -     Structure containing the
;                                        adf25 details.
;               outfile    O      str    name of new adf25 file.
;               comments   I      str()  write these comments at end.
;
; KEYWORDS      help               -     if specified this comment
;                                        section is written to screen.
;
;
; NOTES      :  Not a lot of testing yet. The full flexibility of
;               the adf25 format is not supported.
;
; The fulldata structure is defined:
;
;       a25file  = file name of adf25 format read.
;       iz0      = nuclear charge of bundle-n ion
;       iz1      = recombining ion charge of bundle-n ion
;       a25fmt   = subdirectory type of adf25 to be read.
;       outfmt   = format of output ADAS data format for final results
;       cxfile   = file name for charge exchange data input
;       exfile   = file name for map of proj. matrix output
;       ndens    = number of electron densities
;       id_ref   = reference electron density pointer in vectors
;       densa()  = plasma electron density vector (cm-3)
;                  1st dim: index of electron density
;       denpa()  = plasma H+ density vector (cm-3)
;                  1st dim: index of electron density
;       denimpa()= plasma mean impurity ion density (cm-3)
;                  1st dim: index of electron density
;       ntemp    = number of electron temperatures
;       id_ref   = reference electron temp. pointer in vectors
;       tea()    = plasma electron temp. vector (K)
;                  1st dim: index of electron temperature
;       denpa()  = plasma H+ temp. vector (K)
;                  1st dim: index of electron temperature
;       denimpa()= plasma mean impurity ion temp (K)
;                  1st dim: index of electron temperature
;       nzef     = number of plasma zeff
;       iz_ref   = reference zeff pointer in vector
;       zefa()   = plasma zeff vector
;                  1st dim: index of zeff
;       nbeam    = number of beam energies
;       ib_ref   = reference beam energy pointer in vectors
;       bmena()  = beam energy vector (ev/amu)
;                  1st dim: index of beam energies
;       denha()  = beam H+ density vector (cm-3)
;                  1st dim: index of beam energies
;       bmfra()  = fractions of beam at each energy
;                  1st dim: index of beam energies
;       nimp     = number of plasma impurities (excl.h+)
;       im_ref   = reference impurity pointer in vectors
;       zimpa()  = impurity species charge
;                  1st dim: index of impurity
;       amimpa() = atomic mass number of impurity species
;                  1st dim: index of impurity
;       frimpa() = fraction of impurity (normalised to 1)
;                  1st dim: index of impurity
;       ts       = external radiation field temperature (K)
;       w        = general radiation dilution factor
;       w1       = external radiation field dilution factor
;                  for photo-ionisation form the ground level.
;       cion     = adjustment multiplier for ground ionis.
;       cpy      = adjustment multiplier for VR xsects.
;       nip      = range of delta n for IP xsects. (le.4)
;       intd     = order of Maxw. quad. for IP xsects.(le.3)
;       iprs     = 0  => default to VR xsects. beyond nip range
;                  1  => use PR xsects. beyond nip range
;       ilow     = 0  => no special low level data accessed
;                  1  => special low level data accessed
;       ionip    = 0 =>   no ion impact collisions included
;                  1 =>ion impact excit. and ionis. included
;       nionip   = range of delta n for ion impact
;                  excitation xsects.
;       ilprs    = 0 => default to vainshtein xsects.
;                  1 => use lodge-percival-richards xsects.
;       ivdisp   = 0 => ion impact at thermal Maxw. energies
;                  1 => ion impact at displaced thermal
;                       energies according to the neutral
;                       beam energy parameter
;                    * if(ivdisp=0 then special low level
;                      data for ion impact is not substituted -
;                      only vainshtein and lodge et al.
;                      options are open.  Electron impact
;                      data substitution does occur.
;       nmin     = lowest n-shell for population structure
;       nmax     = highest n-shell for population structure
;       imax     = number of representative n-shells
;       nrep()   = representative n-shells
;                  1st dim: index of representative n-shell
;       wbrep()  = dilution factors for nmin->nrep() trans.
;                  1st dim: index of representative n-shell
;       jdef     = number of n-shell quantum defects
;       def()    = quantum defects for n-shells
;                  1st dim: index of n-shell quantum defects
;                           upwards from nmin
;       jcor     = number of DR Bethe correction factors
;       cor()    = DR Bethe correction factors
;                  1st dim: index of correction factor
;       jmax     = number of DR core transitions
;       epsil()  = reduced energy of core transition
;                  [delta Eij/I_H=(z+1)^2*epsil()]
;                  1st dim: index of DR core transition
;       fij()    = absorption oscillator strength for
;                  DR core transition
;                  1st dim: index of DR core transition
;       wij()    = dilution factor for DR core transition
;                             1st dim: index of DR core transition
;
;
;
; AUTHOR     :  Martin O'Mullane
;
; DATE       :  14-06-2007
;
;
; MODIFIED:
;       1.1     Martin O'Mullane
;               - First version.
;       1.2     Martin O'Mullane
;               - Tighten output format of single floats.
;       1.3     Martin O'Mullane
;               - Check width of exfile and cxfile before writing.
;       1.4     Martin O'Mullane
;               - Use a for loop to print string arrays.
;
; VERSION:
;       1.1    14-06-2007
;       1.2    29-11-2007
;       1.3    17-12-2008
;       1.4    25-10-2012
;-
;----------------------------------------------------------------------

PRO write_adf25_array, lun, title, array

p1 = '("'
p2 = '", 7(E10.2,:), /, 4("         :",7(E10.2,:), /), :)'

str = '         :'
strput, str, title, 0
fmt = p1 + str + p2
outstr = string(array, format=fmt)
for j = 0, n_elements(outstr)-1 do printf, lun,  outstr[j]

END
;----------------------------------------------------------------------



PRO write_adf25, fulldata = fulldata,  $
                 outfile  = outfile,   $
                 comments = comments,  $
                 help     = help

; If asked for help

if keyword_set(help) then begin
   doc_library, 'write_adf25'
   return
endif

; Extract necessary parts to write

iz0      = fulldata.iz0
iz1      = fulldata.iz1
ndens    = fulldata.ndens
id_ref   = fulldata.id_ref
densa    = fulldata.densa
denpa    = fulldata.denpa
denimpa  = fulldata.denimpa
deniona  = fulldata.deniona
ntemp    = fulldata.ntemp
it_ref   = fulldata.it_ref
tea      = fulldata.tea
tpa      = fulldata.tpa
timpa    = fulldata.timpa
tiona    = fulldata.tiona
nzef     = fulldata.nzef
iz_ref   = fulldata.iz_ref
zefa     = fulldata.zefa
nbeam    = fulldata.nbeam
ib_ref   = fulldata.ib_ref
bmena    = fulldata.bmena
denha    = fulldata.denha
bmfra    = fulldata.bmfra
nimp     = fulldata.nimp
im_ref   = fulldata.im_ref
zimpa    = fulldata.zimpa
amimpa   = fulldata.amimpa
frimpa   = fulldata.frimpa
ts       = fulldata.ts
w        = fulldata.w
w1       = fulldata.w1
cion     = fulldata.cion
cpy      = fulldata.cpy
nip      = fulldata.nip
intd     = fulldata.intd
iprs     = fulldata.iprs
ilow     = fulldata.ilow
ionip    = fulldata.ionip
nionip   = fulldata.nionip
ilprs    = fulldata.ilprs
ivdisp   = fulldata.ivdisp
nmin     = fulldata.nmin
nmax     = fulldata.nmax
imax     = fulldata.imax
nrep     = fulldata.nrep
wbrep    = fulldata.wbrep
jdef     = fulldata.jdef
def      = fulldata.def
jcor     = fulldata.jcor
cor      = fulldata.cor
jmax     = fulldata.jmax
epsil    = fulldata.epsil
fij      = fulldata.fij
wij      = fulldata.wij
cxfile   = fulldata.cxfile
exfile   = fulldata.exfile
outfmt   = fulldata.outfmt

; Check width of filenmae lines

str_ex = 'exfile   : ' + exfile
str_cx = 'cxfile   : ' + cxfile

width = max([strlen(str_ex), strlen(str_cx), 80])

; Write the file - xxdata_25 expects quite a fixed format particularly for floats

float_fmt = '(e10.2)'

openw, lun, outfile, /get_lun, width=width

printf, lun, 'receiver ion parameters'
printf, lun, '-----------------------'

printf, lun, 'z0       : ' + string(iz0)
printf, lun, 'z1       : ' + string(iz1)
printf, lun, 'outfmt   : ' + outfmt
printf, lun, str_ex
printf, lun, str_cx
printf, lun, ''


printf, lun, 'plasma density parameters'
printf, lun, '-------------------------'
printf, lun, 'ndens    : ' + string(ndens)
printf, lun, 'id_ref   : ' + string(id_ref)
write_adf25_array, lun, 'densa', densa
write_adf25_array, lun, 'denpa', denpa
write_adf25_array, lun, 'denimpa', denimpa
write_adf25_array, lun, 'deniona', deniona
printf, lun, ''


printf, lun, 'plasma temperature parameters'
printf, lun, '-----------------------------'
printf, lun, 'ntemp    : ' + string(ntemp)
printf, lun, 'it_ref   : ' + string(it_ref)
write_adf25_array, lun, 'tea', tea
write_adf25_array, lun, 'tpa', tpa
write_adf25_array, lun, 'timpa', timpa
write_adf25_array, lun, 'tiona', tiona
printf, lun, ''


printf, lun, 'plasma zeff parameters'
printf, lun, '----------------------'
printf, lun, 'nzef     : ' + string(nzef)
printf, lun, 'iz_ref   : ' + string(iz_ref)
write_adf25_array, lun, 'zefa', zefa
printf, lun, ''


printf, lun, 'neutral beam donor parameters'
printf, lun, '-----------------------------'
printf, lun, 'nbeam    : ' + string(nbeam)
printf, lun, 'ib_ref   : ' + string(ib_ref)
write_adf25_array, lun, 'bmena', bmena
write_adf25_array, lun, 'denha', denha
write_adf25_array, lun, 'bmfra', bmfra
printf, lun, ''


printf, lun, 'plasma impurity parameters'
printf, lun, '--------------------------'
printf, lun, 'nimp     : ' + string(nimp)
printf, lun, 'im_ref   : ' + string(im_ref)
write_adf25_array, lun, 'zimpa', zimpa[0:nimp-1]
write_adf25_array, lun, 'amimpa', amimpa[0:nimp-1]
write_adf25_array, lun, 'frimpa', frimpa[0:nimp-1]
printf, lun, ''


printf, lun, 'control parameters - radiation'
printf, lun, '------------------------------'
printf, lun, 'ts       :' + string(ts, format=float_fmt)
printf, lun, 'w        :' + string(w, format=float_fmt)
printf, lun, 'w1       :' + string(w1, format=float_fmt)
printf, lun, ''


printf, lun, 'control parameters - collisions'
printf, lun, '-------------------------------'
printf, lun, 'cion     :' + string(cion, format=float_fmt)
printf, lun, 'cpy      :' + string(cpy, format=float_fmt)
printf, lun, 'nip      : ' + string(nip)
printf, lun, 'intd     : ' + string(intd)
printf, lun, 'iprs     : ' + string(iprs)
printf, lun, 'ilow     : ' + string(ilow)
printf, lun, 'ionip    : ' + string(ionip)
printf, lun, 'nionip   : ' + string(nionip)
printf, lun, 'ilprs    : ' + string(ilprs)
printf, lun, 'ivdisp   : ' + string(ivdisp)
printf, lun, ''

printf, lun, 'representative level parameters'
printf, lun, '-------------------------------'
printf, lun, 'nmin     : ' + string(nmin)
printf, lun, 'nmax     : ' + string(nmax)
printf, lun, 'imax     : ' + string(imax)

fmt = '("nrep     :", 14(i5,:), /, 4("         :",14(i5,:), /), :)'
outstr = string(nrep, format=fmt)
for j =0, n_elements(outstr)-1 do printf, lun,  outstr[j]
write_adf25_array, lun, 'wbrep', wbrep

printf, lun, 'jdef     : ' + string(jdef)
write_adf25_array, lun, 'def', def
printf, lun, ''

printf, lun, 'dielectronic recombination parameters'
printf, lun, '-------------------------------------'
printf, lun, 'jcor     : ' + string(jcor)
write_adf25_array, lun, 'cor', cor
printf, lun, 'jmax     : ' + string(jmax)
write_adf25_array, lun, 'epsil', epsil
write_adf25_array, lun, 'fij', fij
write_adf25_array, lun, 'wij', wij
printf, lun, ''



; Comments - note we need a comment line to identify the end of useful data

n_com = n_elements(comments)

if n_com GT 0 then for j = 0, n_com-1 do printf, lun, comments[j] $
              else printf, lun, 'c---'


free_lun, lun

END
