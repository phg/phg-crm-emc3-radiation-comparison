;----------------------------------------------------------------------
;+
; PROJECT    :  ADAS
;
; NAME       :  write_adf24
;
; PURPOSE    :  Write an adf24 cross section for charge exchange file
;               from a fulldata structure.
;
;
; ARGUMENTS  :  All output arguments will be defined appropriately.
;
;               NAME      I/O    TYPE    DETAILS
; REQUIRED   :  fulldata   I       -     Structure containing the
;                                        adf24 details.
;               outfile    O      str    name of new adf24 file.
;               comments   I      str()  write these comments at end.
;
; KEYWORDS      drop_zero          -     if set do not write cross sections
;                                        of value 0.0.
;               help               -     if specified this comment
;                                        section is written to screen.
;
;
; NOTES      :
;
; The fulldata structure is defined:
;
;    fulldata = { esym    : element               -- not used
;                 iz0     : atomic number         -- not used
;                 iz      : ion charge            -- not used
;                 iz1     : recombined ion charge -- not used
;                 neng    : number of energies (block)
;                 alpha   : extrapolation parameter
;                 teea    : energies (eV/amu)
;                 cdonor  : donor name
;                 crecvr  : receiver name
;                 cfstat  : final state
;                 ctype   : type of cross-section
;                 scx     : cross sections (cm2)
;
;
; AUTHOR     :  Martin O'Mullane
;
; DATE       :  19-02-2018
;
;
; MODIFIED:
;       1.1     Martin O'Mullane
;                 - First version.
;
; VERSION:
;       1.1    19-02-2018
;-
;----------------------------------------------------------------------


PRO write_adf24, fulldata  = fulldata,  $
                 outfile   = outfile,   $
                 comments  = comments,  $
                 drop_zero = drop_zero, $
                 help      = help

; If asked for help

if keyword_set(help) then begin
   doc_library, 'write_adf24'
   return
endif

; Extract necessary parts to write

neng   = fulldata.neng
alpha  = fulldata.alpha
teea   = fulldata.teea
cdonor = fulldata.cdonor
crecvr = fulldata.crecvr
cfstat = fulldata.cfstat
ctype  = fulldata.ctype
scx    = fulldata.scx

nblock = n_elements(alpha)

openw, lun, outfile, /get_lun

printf, lun, nblock, format = '(i5)'

for j = 0, nblock-1 do begin

   num_eng  = neng[j]
   val_teea = reform(teea[0:neng[j]-1,j])
   val_scx  = reform(scx[0:neng[j]-1,j])
  
   if keyword_set(drop_zero) then begin
   
      ind = where(val_scx GT 0.0, num_eng)
      if num_eng EQ 0 then message, 'All values are zero'
      
      val_teea = reform(teea[ind,j])
      val_scx  = reform(scx[ind,j])

   endif

   str = 'D + X (1)/R + X (1)  nn/FST=          /TYPE= n/ALPH0=  0.00E+00/ISEL=    n'
   strput, str, cdonor[j], 0
   strput, str, crecvr[j], 10
   strput, str, string(num_eng, format='(i2)'), 21
   strput, str, cfstat[j], 29
   strput, str, strcompress(ctype[j], /remove_all), 45
   strput, str, string(alpha[j], format='(E8.2)'), 55
   strput, str, string(j+1, format='(i2)'), 72

   printf, lun, str
   printf, lun, val_teea, format='(6E10.3)'
   printf, lun, val_scx, format='(6E10.3)'

endfor

; Append comments if supplied

n_com = n_elements(comments)

if n_com GT 0 then for j = 0, n_com-1 do printf, lun, comments[j]


free_lun, lun

END
