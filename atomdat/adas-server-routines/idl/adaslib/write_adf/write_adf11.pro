;----------------------------------------------------------------------
;+
; PROJECT    :  ADAS
;
; NAME       :  write_adf11
;
; PURPOSE    :  Writes adf11 collisional radiative files from the IDL command
;               line using the fulldata structure (from read_adf11 or elsewhere).
;               Called from IDL using the syntax
;               write_adf11, file=...,iz1=...,te=... etc
;
; ARGUMENTS  :  All output arguments will be defined appropriately.
;
;               NAME      I/O    TYPE   DETAILS
; REQUIRED   :  fulldata   I       -    Structure containing the
;                                       adf11 details.
;               outfile    I      str   full name of ADAS adf11 file
;               comments   I      str() write these comments at end.
;               project    I      str   Name of project for top line upper right.
;
; KEYWORDS      help              -     display help entry
;
;
; NOTES      :
;
; The fulldata structure is defined:
;
;          iz0       = nuclear charge
;          class     = One of 'acd','scd','ccd','prb','prc',
;                             'qcd','xcd','plt','pls','zcd',
;                             'ycd','ecd'
;          is1min    = minimum ion charge + 1
;                      (generalised to connection vector index)
;          is1max    = maximum ion charge + 1
;                      (note excludes the bare nucleus)
;                      (generalised to connection vector index
;                       and excludes last one which always remains
;                       the bare nucleus)
;          nptnl     = number of partition levels in block
;          nptn      = number of partitions in partition level
;                      1st dim: partition level
;          nptnc    = number of components in partition
;                      1st dim: partition level
;                      2nd dim: member partition in partition level
;          iptnla   = partition level label (0=resolved root,1=
;                                               unresolved root)
;                      1st dim: partition level index
;          iptna    = partition member label (labelling starts at 0)
;                      1st dim: partition level index
;                      2nd dim: member partition index in partition
;                      level
;          iptnca   = component label (labelling starts at 0)
;                      1st dim: partition level index
;                      2nd dim: member partition index in partition
;                      level
;                      3rd dim: component index of member partition
;          ncnct     = number of elements in connection vector
;          icnctv    = connection vector of number of partitions
;                      of each superstage in resolved case
;                      including the bare nucleus
;                      1st dim: connection vector index
;
;          iblmx     = number of (sstage, parent, base)
;                      blocks in isonuclear master file
;          ismax     = number of charge states
;                      in isonuclear master file
;                      (generalises to number of elements in
;                       connection vector)
;          dnr_elem  = donor element name (for ccd and prc)
;          dnr_ams   = donor element mass (for ccd and prc)
;          isppr     = 1st (parent) index for each partition block
;                      1st dim: index of (sstage, parent, base)
;                               block in isonuclear master file
;          ispbr     = 2nd (base) index for each partition block
;                      1st dim: index of (sstage, parent, base)
;                               block in isonuclear master file
;          isstgr    = s1 for each resolved data block
;                      (generalises to connection vector index)
;                      1st dim: index of (sstage, parent, base)
;                               block in isonuclear master file
;
;          idmax     = number of dens values in
;                      isonuclear master files
;          itmax     = number of temp values in
;                      isonuclear master files
;          ddens     = log10(electron density(cm-3)) from adf11
;          dtev      = log10(electron temperature (eV) from adf11
;          drcof     = if(iclass <=9):
;                         log10(coll.-rad. coefft.) from
;                         isonuclear master file
;                      if(iclass >=10):
;                         coll.-rad. coefft. from
;                         isonuclear master file
;                      1st dim: index of (sstage, parent, base)
;                               block in isonuclear master file
;                      2nd dim: electron temperature index
;                      3rd dim: electron density index
;
;          lres      = .true.  => partial file
;                    = .false. => not partial file
;          lstan     = .true.  => standard file
;                    = .false. => not standard file
;          lptn      = .true.  => partition block present
;                     = .false. => partition block not present;
;
;
;
; NOTES
;
;       This code is loosely based on read_adf11. Does not yet take into account superstaging.
;       Designed to be compatible with fulldata structure from read_adf11, although only
;       uses a fraction of the information within that structure.
;
; AUTHOR     :  Adam Foster
;
; MODIFIED:
;       1.1     Adam Foster
;               - First version.
;       1.2     Martin O'Mullane
;               - Write class sepcific headers for each ion stage.
;               - Rename arguments to match other write_adfXX routines.
;       1.3     Martin O'Mullane
;               - Enable option to write metastable resolved adf11 files.
;       1.4     Martin O'Mullane
;               - Increase the project name to be 20 characters.
;       1.5     Martin O'Mullane
;               - For acd file iprt and igrd were reversed.
;       1.6     Martin O'Mullane
;               - xcd was omitted. Use correct labels for partial
;                 plt and prb classes.
;       1.7     Martin O'Mullane
;               - Short by one when writing connection vector.
;
; VERSION:
;
;       1.1     09-02-2009
;       1.2     21-04-2010
;       1.3     15-03-2016
;       1.4     03-05-2017
;       1.5     26-05-2017
;       1.6     22-08-2017
;       1.7     24-08-2018
;-
;----------------------------------------------------------------------

PRO write_adf11, fulldata = fulldata,  $
                 outfile  = outfile,   $
                 comments = comments,  $
                 project  = project,   $
                 help     = help

if keyword_set(help) then begin
   doc_library, 'write_adf11'
   return
endif

; Set default for project

if n_elements(project) EQ 0 then project = 'Unamed project'

; get date

date = (xxdate())[0]
while (STRPOS(date, '/'))[0] NE -1 do begin
  strput, date,'.',(STRPOS(date, '/'))[0]
endwhile

; extract data from fulldata input

iz0    = fulldata.iz0
class  = strupcase(fulldata.class)
idmax  = fulldata.idmax
itmax  = fulldata.itmax
is1min = fulldata.is1min
is1max = fulldata.is1max
ismax  = fulldata.ismax

xxelem, iz0, elname
elname = strtrim(strupcase(elname))

; Resolved or partial

is_partial = 0
i1 = where(strpos(tag_names(fulldata), 'LRES') NE -1, c1)
if c1 GT 0 then begin
   if fulldata.lres EQ 1 then is_partial = 1
endif

; Build up strarr of output file

dashs = string(replicate(45B, 79))
fmt   = '(8f10.5)'

str_h = '   ' + string(iz0, format='(i2)') + $
        string([idmax, itmax, is1min, is1max], format='(4i5)') + $
        '     /              /    /                        ADF11'

strput, str_h, strupcase(elname), 31
strput, str_h, strupcase(class), 46
strput, str_h, strmid(project, 0, strlen(project)<21), 51

if is_partial EQ 1 then begin

   all = [str_h, '-' + dashs , $
          string(fulldata.icnctv, format='(16i5)'), $
          '-' + dashs , $
          string(fulldata.ddens, format=fmt), $
          string(fulldata.dtev, format=fmt)]

   for j = 0, ismax-1 do begin

     s1 = fulldata.isstgr[j]

     case class of

      'ACD' : str = '---------------------' + $
                                       '/ IPRT=' + string(fulldata.iprt[j], format='(i2)') + '  ' + $
                                       '/ IGRD=' + string(fulldata.igrd[j], format='(i2)') + $
                                       '  /--------/ Z1=' + string(s1, format='(i2)') + $
                                       '   / DATE= ' + date

      'SCD' : str = '---------------------' + $
                                       '/ IPRT=' + string(fulldata.iprt[j], format='(i2)') + '  ' + $
                                       '/ IGRD=' + string(fulldata.igrd[j], format='(i2)') + $
                                       '  /--------/ Z1=' + string(s1, format='(i2)') + $
                                       '   / DATE= ' + date

      'QCD' : str = '---------------------' + $
                                       '/ IGRD=' + string(fulldata.igrd[j], format='(i2)') + '  ' + $
                                       '/ JGRD=' + string(fulldata.jgrd[j], format='(i2)') + $
                                       '  /--------/ Z1=' + string(s1, format='(i2)') + $
                                       '   / DATE= ' + date

       'XCD' : str = '---------------------' + $
                                       '/ IPRT=' + string(fulldata.iprt[j], format='(i2)') + '  ' + $
                                       '/ JPRT=' + string(fulldata.jprt[j], format='(i2)') + $
                                       '  /--------/ Z1=' + string(s1, format='(i2)') + $
                                       '   / DATE= ' + date

      'CCD' : str = '--------------------/ IPRT= 1  / IGRD= 1  / MH=2.01/ Z1=' + $
              string(s1, format='(i2)') + '   / DATE= ' + date


      'PLT' : str = '---------------------' + $
                                       '/ IGRD=' + string(fulldata.imet[j], format='(i2)') + '  ' + $
                                       '/ IPRT= 0' + $
                                       '  /--------/ Z1=' + string(s1, format='(i2)') + $
                                       '   / DATE= ' + date

      'PRB' : str = '---------------------' + $
                                       '/ IPRT=' + string(fulldata.iprt[j], format='(i2)') + '  ' + $
                                       '/ IGRD= 0' + $
                                       '  /--------/ Z1=' + string(s1, format='(i2)') + $
                                       '   / DATE= ' + date

      'PRC' : str = '--------------------/ IPRT= 1  / IGRD= 1  / MH=2.01/ Z1=' + $
              string(s1, format='(i2)') + '   / DATE= ' + date

      'ECD' : begin
                 str = '---------------------/ IGRD= 1  / IPRT= 1  /--------/ Z1=' + $
                 string(s1, format='(i2)') + '   / DATE= ' + date
                 fmt = '(8E10.3)'
              end

      'YCD' : begin
                 str = '---------------------/ IGRD= 1  / IPRT= 0  /--------/ Z1=' + $
                 string(s1, format='(i2)') + '   / DATE= ' + date
                 fmt = '(8E10.3)'
              end

      'ZCD' : begin
                 str = '---------------------/ IGRD= 1  / IPRT= 0  /--------/ Z1=' + $
                 string(s1, format='(i2)') + '   / DATE= ' + date
                 fmt = '(8E10.3)'
              end

      else : message, 'Class ' + class + ' not implemented yet'

     endcase

     all = [all, str]
     for i = 0, itmax-1 do all = [all, string(fulldata.drcof[j,i,*], format=fmt)]

   endfor

endif else begin

   all = [str_h, '-' + dashs , string(fulldata.ddens, format=fmt), string(fulldata.dtev, format=fmt)]

   for j = 0, ismax-1 do begin

     s1 = fulldata.isstgr[j]

     case class of

      'ACD' : str = '---------------------/ IPRT= 1  / IGRD= 1  /--------/ Z1=' + $
              string(s1, format='(i2)') + '   / DATE= ' + date

      'SCD' : str = '---------------------/ IGRD= 1  / IPRT= 1  /--------/ Z1=' + $
              string(s1, format='(i2)') + '   / DATE= ' + date

      'CCD' : str = '--------------------/ IPRT= 1  / IGRD= 1  / MH=2.01/ Z1=' + $
              string(s1, format='(i2)') + '   / DATE= ' + date

      'PLT' : str = '---------------------/ IGRD= 1  / IPRT= 0  /--------/ Z1=' + $
              string(s1, format='(i2)') + '   / DATE= ' + date

      'PRB' : str = '---------------------/ IPRT= 1  / IGRD= 1  /--------/ Z1=' + $
              string(s1, format='(i2)') + '   / DATE= ' + date

      'PRC' : str = '--------------------/ IPRT= 1  / IGRD= 1  / MH=2.01/ Z1=' + $
              string(s1, format='(i2)') + '   / DATE= ' + date

      'ECD' : begin
                 str = '---------------------/ IGRD= 1  / IPRT= 1  /--------/ Z1=' + $
                 string(s1, format='(i2)') + '   / DATE= ' + date
                 fmt = '(8E10.3)'
              end

      'YCD' : begin
                 str = '---------------------/ IGRD= 1  / IPRT= 0  /--------/ Z1=' + $
                 string(s1, format='(i2)') + '   / DATE= ' + date
                 fmt = '(8E10.3)'
              end

      'ZCD' : begin
                 str = '---------------------/ IGRD= 1  / IPRT= 0  /--------/ Z1=' + $
                 string(s1, format='(i2)') + '   / DATE= ' + date
                 fmt = '(8E10.3)'
              end

      else : message, 'Class ' + class + ' not implemented yet'

     endcase

     all = [all, str]
     for i = 0, itmax-1 do all = [all, string(fulldata.drcof[j,i,*], format=fmt)]

   endfor

endelse


; Add a comment terminator and write file

all = [all, 'C' + dashs]

adas_writefile, file=outfile, all=all, comments=comments

END
