;----------------------------------------------------------------------
;+
; PROJECT    :  ADAS
;
; NAME       :  write_adf54
;
; PURPOSE    :  Write an adf54 file using the
;               fulldata structure.
;
;
; ARGUMENTS  :  All output arguments will be defined appropriately.
;
;               NAME      I/O    TYPE    DETAILS
; REQUIRED   :  fulldata   I       -     Structure containing the
;                                        adf54 details.
;               outfile    O      str    name of new adf54 file.
;               comments   I      str()  write these comments at end.
;
;
; KEYWORDS      help               -     if specified this comment
;                                        section is written to screen.
;                                                      
; NOTES      :  
;
; The fulldata structure is defined:
;
;              config     : ground configuration
;              index      : reference index
;              n_el       : number of electron
;              no_v_shl   : number of open shell 
;              max_dn_v1  : maximum delta n promotion for first
;              min_dn_v1  : minimum delta n promotion for first
;              max_dl_v1  : maximum delta l promotion for first
;              min_dl_v1  : minimum delta l promotion for first
;              max_dn_v2  : maximum delta n promotion for second
;              min_dn_v2  : minimum delta n promotion for second
;              max_dl_v2  : maximum delta l promotion for second
;              min_dl_v2  : minimum delta l promotion for second
;              prom_cl    : promote from inner shell closed shells 
;              max_n_cl   : maximum inner shell n for promotion from
;              min_n_cl   : minimum inner shell n for promotion from
;              max_l_cl   : maximum inner shell l for promotion from
;              min_l_cl   : minimum inner shell l for promotion from
;              max_dn_cl  : maximum delta n promotion for inner shell to
;              min_dn_cl  : minimum delta n promotion for inner shell to
;              max_dl_cl  : maximum delta l promotion for inner shell to
;              min_dl_cl  : minimum delta l promotion for inner shell to
;              fill_n_v1  : add all nl configurations
;              fill_par   : add parity
;              for_tr_sel : Cowan option for radiative transitions
;              last_4f    : shift an electron valence shell to 4f
;              grd_cmplx  : include configurations of same complex
;
; AUTHOR     :  Alessandra Giunta
;
; DATE       :  02-10-2007
;
;
; MODIFIED:
;       
;               
;
; VERSION:
;       1.1     02-10-2007 
;-
;----------------------------------------------------------------------


pro write_adf54, fulldata = fulldata,  $
                 outfile  = outfile,   $
                 comments = comments,  $
                 help     = help

; If asked for help

if keyword_set(help) then begin
   doc_library, 'write_adf54'
   return
endif


; Extract necessary parts to write

config     = fulldata.config
index      = fulldata.index 
n_el       = fulldata.n_el
no_v_shl   = fulldata.no_v_shl
max_dn_v1  = fulldata.max_dn_v1
min_dn_v1  = fulldata.min_dn_v1
max_dl_v1  = fulldata.max_dl_v1
min_dl_v1  = fulldata.min_dl_v1
max_dn_v2  = fulldata.max_dn_v2
min_dn_v2  = fulldata.min_dn_v2
max_dl_v2  = fulldata.max_dl_v2
min_dl_v2  = fulldata.min_dl_v2
prom_cl    = fulldata.prom_cl 
max_n_cl   = fulldata.max_n_cl
min_n_cl   = fulldata.min_n_cl
max_l_cl   = fulldata.max_l_cl
min_l_cl   = fulldata.min_l_cl
max_dn_cl  = fulldata.max_dn_cl
min_dn_cl  = fulldata.min_dn_cl
max_dl_cl  = fulldata.max_dl_cl
min_dl_cl  = fulldata.min_dl_cl
fill_n_v1  = fulldata.fill_n_v1
fill_par   = fulldata.fill_par
for_tr_sel = fulldata.for_tr_sel
last_4f    = fulldata.last_4f
grd_cmplx  = fulldata.grd_cmplx 
   
; Open outfile and write it

openw, lun, outfile, /get_lun

n_max=180
nel=10

fmt='(i5)'

str = '/ADF54'+'    /'+'PROMOTION RULES'+'    /'+strtrim(string(n_max),2)+'    /'+strtrim(string(nel),2)
printf,lun,str

for i = 0,(n_max/nel)-1 do begin

  l = i*nel 
  m = 9+i*nel

  printf,lun,'index    config'
  
  for j=0, nel-1 do begin
    a = string(index[l+j],format=fmt)+'    '
    b = strtrim(string(config[l+j],format='(a85)'),1)
    str = a+b
    printf,lun,str
  endfor
  printf,lun,''

  printf,lun,'index        ',string(index[l : m],format=fmt)

  printf,lun,'n_el         ',string(n_el[l : m],format=fmt)  

  printf,lun,'no_v_shl     ',string(no_v_shl[l : m],format=fmt)

  printf,lun,'max_dn_v1    ',string(max_dn_v1[l : m],format=fmt)                              

  printf,lun,'min_dn_v1    ',string(min_dn_v1[l : m],format=fmt)                             

  printf,lun,'max_dl_v1    ',string(max_dl_v1[l : m],format=fmt)                              

  printf,lun,'min_dl_v1    ',string(min_dl_v1[l : m],format=fmt)                               

  printf,lun,'max_dn_v2    ',string(max_dn_v2[l : m],format=fmt)                              

  printf,lun,'min_dn_v2    ',string(min_dn_v2[l : m],format=fmt)                             

  printf,lun,'max_dl_v2    ',string(max_dl_v2[l : m],format=fmt)                              

  printf,lun,'min_dl_v2    ',string(min_dl_v2[l : m],format=fmt)                               

  printf,lun,'prom_cl      ',string(prom_cl[l : m],format=fmt)                               

  printf,lun,'max_n_cl     ',string(max_n_cl[l : m],format=fmt)                               

  printf,lun,'min_n_cl     ',string(min_n_cl[l : m],format=fmt)                               

  printf,lun,'max_l_cl     ',string(max_l_cl[l : m],format=fmt)                               

  printf,lun,'min_l_cl     ',string(min_l_cl[l : m],format=fmt)                               

  printf,lun,'max_dn_cl    ',string(max_dn_cl[l : m],format=fmt)                              

  printf,lun,'min_dn_cl    ',string(min_dn_cl[l : m],format=fmt)                             

  printf,lun,'max_dl_cl    ',string(max_dl_cl[l : m],format=fmt)                              

  printf,lun,'min_dl_cl    ',string(min_dl_cl[l : m],format=fmt)                               

  printf,lun,'fill_n_v1    ',string(fill_n_v1[l : m],format=fmt)                               

  printf,lun,'fill_par     ',string(fill_par[l : m],format=fmt)                               

  printf,lun,'for_tr_sel   ',string(for_tr_sel[l : m],format=fmt)                               

  printf,lun,'last_4f      ',string(last_4f[l : m],format=fmt)                               

  printf,lun,'grd_cmplx    ',string(grd_cmplx[l : m],format=fmt)                               
  printf,lun,''

endfor

n_com = n_elements(comments)

if n_com GT 0 then for k = 0, n_com-1 do printf, lun, comments[k]

close,lun
free_lun, lun
                
end
