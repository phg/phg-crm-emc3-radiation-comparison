;----------------------------------------------------------------------
;+
; PROJECT    :  ADAS
;
; NAME       :  write_adf02
;
; PURPOSE    :  Write an adf02 ion impact reaction file from a
;               fulldata structure (from xxdata_02 or elsewhere).
;
;
; ARGUMENTS  :  All output arguments will be defined appropriately.
;
;               NAME      I/O    TYPE    DETAILS
; REQUIRED   :  fulldata   I       -     Structure containing the
;                                        adf02 details.
;               outfile    O      str    name of new adf02 file.
;               comments   I      str()  write these comments at end.
;
; KEYWORDS      help               -     if specified this comment
;                                        section is written to screen.
;
;
; NOTES      :
;
; The fulldata structure is defined:
;
;    fulldata = { nbsel       long    number of blocks present
;                 nedim       long    max number of collision energies allowed
;                 nbsel       long    number of data-blocks accepted/read in
;                 isela()     long    data-block entry indices
;                 cprimy()    str     primary species identification
;                 csecdy()    str     secondary species identification
;                 ctype()     str     cross-section type
;                 ampa()      double  primary species atomic mass number
;                 amsa()      double  secondary species atomic mass number
;                 alpha()     double  high energy extrapolation parm.
;                 ethra()     double  energy threshold (ev)
;                 iea()       double  number of collision energies
;                 teea(,)     double  collision energies (units: ev/amu)
;                                       1st dimension: collision energy index
;                                       2nd dimension: data-block index
;                 sia(,)      double  full set of collision
;                                     cross-section values (cm**2)
;                                            1st dimension: collision energy index
;                                            2nd dimension: data-block index
;
;
; AUTHOR     :  Martin O'Mullane
;
; DATE       :  14-05-2019
;
;
; MODIFIED:
;       1.1     Martin O'Mullane
;               - First version.
;
; VERSION:
;       1.1    14-05-2019
;-
;----------------------------------------------------------------------


PRO write_adf02, fulldata = fulldata,  $
                 outfile  = outfile,   $
                 comments = comments,  $
                 help     = help

; If asked for help

if keyword_set(help) then begin
   doc_library, 'write_adf02'
   return
endif

; Extract necessary parts to write

nbsel   = fulldata.nbsel
nedim   = fulldata.nedim
isela   = fulldata.isela
cprimy  = fulldata.cprimy
csecdy  = fulldata.csecdy
ctype   = fulldata.ctype
ampa    = fulldata.ampa
amsa    = fulldata.amsa
alpha   = fulldata.alpha
ethra   = fulldata.ethra
iea     = fulldata.iea
teea    = fulldata.teea
sia     = fulldata.sia


openw, lun, outfile, /get_lun

printf, lun, string(nbsel, format='(i5)')


for j = 0, nbsel-1 do begin

   str = 'X + 0/x + 4   n/P=0.000E+00/S=0.000E+00/A=0.000E+00/E=0.000E+00/T=T  /ISEL=    N'

   strput, str, cprimy[j], 0
   strput, str, csecdy[j], 6
   strput, str, string(iea[j], format='(i2)'), 13
   strput, str, string(ampa[j], format='(E9.3)'), 18
   strput, str, string(amsa[j], format='(E9.3)'), 30
   strput, str, string(alpha[j], format='(E9.3)'), 42
   strput, str, string(ethra[j], format='(E9.3)'), 54
   strput, str, ctype[j], 66
   strput, str, string(isela[j], format='(i3)'), 77

   printf, lun, str

   printf, lun, teea[0:iea[j]-1,j], format='(6E10.3)'
   printf, lun, sia[0:iea[j]-1,j], format='(6E10.3)'

endfor


; Append comments if supplied

n_com = n_elements(comments)

if n_com GT 0 then for j = 0, n_com-1 do printf, lun, comments[j]

free_lun, lun

END
