;----------------------------------------------------------------------
;+
; PROJECT    :  ADAS
;
; NAME       :  write_adf14
;
; PURPOSE    :  Write an adf14 thermal charge exchange rates (TCX) file
;               from a fulldata structure.
;
;
; ARGUMENTS  :  All output arguments will be defined appropriately.
;
;               NAME      I/O    TYPE    DETAILS
; REQUIRED   :  fulldata   I       -     Structure containing the
;                                        adf14 details.
;               outfile    O      str    name of new adf14 file.
;               comments   I      str()  write these comments at end.
;
; KEYWORDS      help               -     if specified this comment
;                                        section is written to screen.
;
;
; NOTES      :
;
; The fulldata structure is defined:
;
;       nblock    : number blocks in file
;       idz0      : nuclear charge donor
;                     - 1st dim: data block
;       irz0      : nuclear charge receiver
;                     - 1st dim: data block
;       irz1      : initial charge receiver
;                     - 1st dim: data block
;       amsda     : atomic mass donor
;                     - 1st dim: data block
;       amsra     : atomic mass receiver
;                     - 1st dim: data block
;       cdonor    : donor name
;                     - 1st dim: data block
;       crecvr    : receiver name
;                     - 1st dim: data block
;       cfstat    : final state
;                     - 1st dim: data block
;       itda      : number of donor temperatures
;                     - 1st dim: data block
;       tfda      : donor temperature
;                     - 1st dim: temperature index
;                     - 2nd dim: data block
;       itra      : number of receiver temperatures
;                     - 1st dim: data block
;       tfra      : receiver temperature
;                     - 1st dim: temperature index
;                     - 2nd dim: data block
;       lequa     : block contains equal temperature data
;                     - 1st dim: data block
;       qfteqa    : equal temperature rate coefficinets (cm3 s-1)
;                     - 1st dim: receiver temperature index
;                     - 2nd dim: data block
;       qftcxa    : rate coefficinets (cm3 s-1)
;                     - 1st dim: donor temperature index
;                     - 2nd dim: receiver temperature index
;                     - 3rd dim: data block
;
;
; AUTHOR     :  Martin O'Mullane
;
; DATE       :  06-03-2018
;
;
; MODIFIED:
;       1.1     Martin O'Mullane
;                 - First version.
;
; VERSION:
;       1.1    06-03-2018
;-
;----------------------------------------------------------------------


PRO write_adf14, fulldata  = fulldata,  $
                 outfile   = outfile,   $
                 comments  = comments,  $
                 drop_zero = drop_zero, $
                 help      = help

; If asked for help

if keyword_set(help) then begin
   doc_library, 'write_adf14'
   return
endif

; Extract necessary parts to write

nblock  = fulldata.nblock
idz0    = fulldata.idz0
irz0    = fulldata.irz0
irz1    = fulldata.irz1
amsda   = fulldata.amsda
amsra   = fulldata.amsra
cdonor  = fulldata.cdonor
crecvr  = fulldata.crecvr
cfstat  = fulldata.cfstat
itda    = fulldata.itda
tfda    = fulldata.tfda
itra    = fulldata.itra
tfra    = fulldata.tfra
lequa   = fulldata.lequa
qfteqa  = fulldata.qfteqa
qftcxa  = fulldata.qftcxa


openw, lun, outfile, /get_lun

printf, lun, nblock, format = '(i5)'

; Note the order "elem d/r number temp r/d mass d/r", temperatures are reversed

for j = 0, nblock-1 do begin

   ntrnum = itra[j]
   ntdnum = itda[j]

   str = 'X + X    /Y + Y       n   n/AMD=   x.0/AMR=   x.0/FST=          /ISEL=    n'
   strput, str, cdonor[j], 0
   strput, str, crecvr[j], 10
   strput, str, string(ntrnum, format='(i2)'), 21
   strput, str, string(ntdnum, format='(i2)'), 25
   strput, str, string(amsda[j], format='(f6.1)'), 32
   strput, str, string(amsra[j], format='(f6.1)'), 43
   strput, str, cfstat[j], 54
   strput, str, string(j+1, format='(i2)'), 73

   printf, lun, str

   printf, lun, reform(tfra[0:ntrnum-1,j]), format='(8E9.2)'

   if lequa[j] EQ 1 then begin
      i1 = min([7,ntdnum])
      printf, lun, ' *EQUAL**', reform(tfda[0:i1-1,j]), format='(a9, 7E9.2)'
      if ntdnum GT 7 then printf, lun, reform(tfda[i1:ntdnum-1,j]), format='(8E9.2)'
      for itr = 0, ntrnum-1 do begin
         printf, lun, [reform(qfteqa[itr,j]), reform(qftcxa[0:ntdnum-1,itr,j])], format='(8E9.2)'
      endfor
   endif else begin
      printf, lun, reform(tfda[0:ntdnum-1,j]), format='(8E9.2)'
      for itr = 0, ntrnum-1 do begin
         printf, lun, reform(qftcxa[0:ntdnum-1,itr,j]), format='(8E9.2)'
      endfor
   endelse

endfor

; Append comments if supplied

n_com = n_elements(comments)

if n_com GT 0 then for j = 0, n_com-1 do printf, lun, comments[j]


free_lun, lun

END
