;----------------------------------------------------------------------
;+
; PROJECT    :  ADAS
;
; NAME       :  write_adf19
;
; PURPOSE    :  Write an adf19 specific ion file using the
;               fulldata structure (from read_adf19 or elsewhere).
;
;
; ARGUMENTS  :  All output arguments will be defined appropriately.
;
;               NAME      I/O    TYPE    DETAILS
; REQUIRED   :  fulldata   I       -     Structure containing the
;                                        adf19 details.
;               outfile    O      str    name of new adf19 file.
;               comments   I      str()  write these comments at end.
;
; KEYWORDS      help               -     if specified this comment
;                                        section is written to screen.
;
;
; NOTES      :
;
; The fulldata structure is defined:
;
;       esym      =  Element
;       iz0       =  Nuclear charge
;       nblock    =  Number of ionisation rates
;       iz()      =  Initial charge
;       iz1 ()    =  Final charge
;       ciion()   =  Radiating ion
;       citype()  =  Radiation type
;       ciinfo()  =  Information string
;       nte()     =  Number of temperatures in each block
;       te()      =  Temperatures
;       pzd()     =  Ionisation rates
;       filename  =  Filename read
;
;
; AUTHOR     :  Martin O'Mullane
;
; DATE       :  04-07-2008
;
;
; MODIFIED:
;       1.1     Martin O'Mullane
;               - First version.
;
; VERSION:
;       1.1    04-07-2008
;-
;----------------------------------------------------------------------



PRO write_adf19, fulldata = fulldata,  $
                 outfile  = outfile,   $
                 comments = comments,  $
                 help     = help

; If asked for help

if keyword_set(help) then begin
   doc_library, 'write_adf19'
   return
endif

; Extract necessary parts to write

esym    = fulldata.esym
iz0     = fulldata.iz0
nblock  = fulldata.nblock
iz      = fulldata.iz
iz1     = fulldata.iz1
ciion   = fulldata.ciion
citype  = fulldata.citype
ciinfo  = fulldata.ciinfo
nte     = fulldata.nte
te      = fulldata.te
pzd     = fulldata.pzd


esym_str = '  '
strput, esym_str, esym, 0


; Write the file

fmt = '(6E10.3,:)'

openw, lun, outfile, /get_lun

printf, lun, string(nblock, format='(i5)') + '    /' + esym_str + $
             '  RADIATED POWER COEFFICIENTS    /'

for j = 0, nblock-1 do begin

   isel = j + 1

   ciion_str = '     '
   strput, ciion_str, ciion[j], 0
   citype_str = '     '
   strput, citype_str, citype[j], 0
   ciinfo_str = '                    '
   strput, ciinfo_str, ciinfo[j], 0

   str = ciion_str + '/     /' + string(nte[j], format='(i5)') + $
         '/TYPE=' + citype_str + '/INFO =' + ciinfo_str + $
         '/         /ISEL=' + string(isel, format='(i4)')

   printf, lun, str

   if nblock EQ 1 then begin
     t_out = te
     p_out = pzd
   endif else begin
     t_out = reform(te[*,j])
     p_out = reform(pzd[*,j])
   endelse

   printf, lun, t_out, format=fmt
   printf, lun, p_out, format=fmt

endfor


; Comments - note we need a comment line to identify the end of useful data

n_com = n_elements(comments)

if n_com GT 0 then for j = 0, n_com-1 do printf, lun, comments[j]

free_lun, lun

END
