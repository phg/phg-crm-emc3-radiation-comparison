;----------------------------------------------------------------------
;+
; PROJECT    :  ADAS
;
; NAME       :  write_adf42
;
; PURPOSE    :  Write an adf42 driver file for adas810.
;
; ARGUMENTS  :  All output arguments will be defined appropriately.
;
;               NAME      I/O    TYPE    DETAILS
; REQUIRED   :  fulldata   I       -     Structure containing the
;                                        adf42 details.
;               outfile    O      str    name of new adf42 file.
;               comments   I      str()  write these comments at end.
;
; KEYWORDS      help               -     if specified this comment
;                                        section is written to screen.
;
;
; NOTES      :
;
; The fulldata structure is defined:
;
;    fulldata = { DSN04     :   
;                 DSN18     :   
;                 DSN35     :   
;                 DSN15     : 
;                 DSN40     : 
;                 DSN11     : 
;                 DSN11F    : 
;                 ELEMENT   : 
;                 Z0        :      
;                 Z1        :      
;                 IP        :                            
;                 LNORM     :   
;                 NMET      :    
;                 IMETR     :                                    
;                 LIOSEL    :  
;                 LHSEL     :   
;                 LRSEL     :   
;                 LISEL     : 
;                 LNSEL     : 
;                 LPSEL     : 
;                 ZEFF      : 
;                 LMETR     :   
;                 LTSCL     :   
;                 LDSCL     :   
;                 LBRDI     :                         
;                 AMIN      :    
;                 NUMTE     :   
;                 NUMTION   : 
;                 NUMTH     :                         
;                 NUMDENS   : 
;                 NUMDION   : 
;                 NUMWVL    :
;                 te        :
;                 dens      :
;                 npix      :
;                 wvmin     :
;                 wvmax     :
;
;
; AUTHOR     :  Martin O'Mullane
;
; DATE       :  14-07-2008
;
;
; MODIFIED:
;       1.1     Martin O'Mullane
;               - First version.
;       1.2     Martin O'Mullane
;               - Allow Tion to be different to Te.
;
; VERSION:
;       1.1    14-07-2008
;       1.1    03-08-2010
;-
;----------------------------------------------------------------------


PRO write_adf42, fulldata = fulldata,  $
                 outfile  = outfile,   $
                 comments = comments,  $
                 help     = help

; If asked for help

if keyword_set(help) then begin
   doc_library, 'write_adf42'
   return
endif

; adf42 namelist and defaults

a42names = ['DSN04',   'DSN18',   'DSN35',   'DSN15', 'DSN40', 'DSN11', 'DSN11F', $
            'ELEMENT', 'Z0',      'Z1',      'IP',                                $
            'LNORM',   'NMET',    'IMETR',                                        $
            'LIOSEL',  'LHSEL',   'LRSEL',   'LISEL', 'LNSEL', 'LPSEL', 'ZEFF',   $
            'LMETR',   'LTSCL',   'LDSCL',   'LBRDI',                             $
            'AMIN',    'NUMTE',   'NUMTION', 'NUMTH',                             $
            'NUMDENS', 'NUMDION', 'NUMWVL']

a42def   = [ 'NULL', 'NULL', 'NULL', 'NULL', 'NULL', 'NULL', 'NULL',                      $
             'C',     '6',        '3',       '386241.0',                                  $
             '.TRUE.', '1', '1',                                                          $
             '.FALSE.', '.FALSE.', '.FALSE.', '.FALSE.', '.FALSE.', '.FALSE.', '3.0',     $
             '.FALSE.',  '.FALSE.',  '.FALSE.',  '.FALSE.',                               $
             '0.0',      '5',        '5',        '0',       '5',  '0', '1' ]



; Replace defaults with input data

a42out = a42def
names  = tag_names(fulldata)

for j = 0, n_elements(names)-1 do begin

   ind = where(a42names EQ names[j], count)

   if count GT 0 then a42out[ind] = fulldata.(j)

endfor

; Temperature, density and wavelength ranges

te    = fulldata.te
tion  = fulldata.tion
dens  = fulldata.dens
npix  = fulldata.npix
wvmin = fulldata.wvmin
wvmax = fulldata.wvmax

n_te   = n_elements(te)
n_dens = n_elements(dens)
n_wvl  = n_elements(npix)

a42out[26] = string(n_te)
a42out[27] = string(n_te)
a42out[29] = string(n_dens)
a42out[31] = string(n_wvl)



; Write adf42 file

openw, lun, outfile, /get_lun

printf, lun, ' &FILES'
for j = 0, 6 do printf, lun, '    ' + a42names[j] + ' = ''' + a42out[j] + '''
printf, lun, ' &END'

printf, lun, ' &ION'
printf, lun, '    ' + a42names[7] + ' = ''' + a42out[7] + '''
for j = 8, 10 do printf, lun, '    ' + a42names[j] + ' = ' + a42out[j]
printf, lun, ' &END'

printf, lun, ' &META'
for j = 11, 12 do printf, lun, '    ' + a42names[j] + ' = ' + a42out[j]
printf, lun, '    ' + a42names[13] + '(1) = ' + a42out[13]
printf, lun, ' &END'

printf, lun, ' &PROCESS'
for j = 14, 20 do printf, lun, '    ' + a42names[j] + ' = ' + a42out[j]
printf, lun, ' &END'

printf, lun, ' &OUTPUT'
for j = 21, 31 do printf, lun, '    ' + a42names[j] + ' = ' + a42out[j]
printf, lun, ' &END'

printf, lun, ' '
printf, lun, te, format='(5e10.3)'
printf, lun, ' '
printf, lun, tion, format='(5e10.3)'
printf, lun, ' '
printf, lun, dens, format='(5e10.3)'

printf, lun, ' '
for j = 0, n_wvl-1 do printf, lun, npix[j], wvmin[j], wvmax[j], $
                              format = '(i6, 2f17.4)'


; Append comments if supplied

n_com = n_elements(comments)

if n_com GT 0 then for j = 0, n_com-1 do printf, lun, comments[j]


; Close up and finish

free_lun, lun

END
