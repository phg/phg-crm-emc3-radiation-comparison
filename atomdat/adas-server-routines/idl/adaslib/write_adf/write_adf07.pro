;----------------------------------------------------------------------
;+
; PROJECT    :  ADAS
;
; NAME       :  write_adf07
;
; PURPOSE    :  Write an adf07 specific ion file using the
;               fulldata structure (from read_adf07 or elsewhere).
;
;
; ARGUMENTS  :  All output arguments will be defined appropriately.
;
;               NAME      I/O    TYPE    DETAILS
; REQUIRED   :  fulldata   I       -     Structure containing the
;                                        adf07 details.
;               outfile    O      str    name of new adf07 file.
;               comments   I      str()  write these comments at end.
;
; KEYWORDS      help               -     if specified this comment
;                                        section is written to screen.
;
;
; NOTES      :
;
; The fulldata structure is defined:
;
;    fulldata = { esym                 Element Symbol
;                 iz0                  Nuclear charge
;                 nblock               Number of ionisation blocks
;                 iz[nblock]           Initial charge
;                 iz1 [nblock]         Final charge
;                 cicode[nblock]       Initial state metastable index
;                 cfcode[nblock]       Final   state metastable index
;                 ciion[nblock]        Initial ion (as <esym>+(iz[nblock]> )
;                 cfion[nblock]        Final   ion (as <esym>+<iz1[nblock]>)
;                 bwno[nblock]         Effective ionization potential (cm-1)
;                 nte[nblock]          Number of temperatures in each block
;                 te[nte,nblock]       Temperatures in block
;                 szd[max(nte),nblock] Ionisation rates
;
;
; AUTHOR     :  Martin O'Mullane
;
; DATE       :  05-07-2007
;
;
; MODIFIED:
;       1.1     Martin O'Mullane
;               - First version.
;       1.2     Martin O'Mullane
;               - Give correct fulldata definition in comments.
;               - Consider bwno an an array rather than using same
;                 value for each block.
;
; VERSION:
;       1.1    13-10-2007
;       1.2    10-07-2009
;-
;----------------------------------------------------------------------


PRO write_adf07, fulldata = fulldata,  $
                 outfile  = outfile,   $
                 comments = comments,  $
                 help     = help

; If asked for help

if keyword_set(help) then begin
   doc_library, 'write_adf07'
   return
endif

; Extract necessary parts to write

iz0    = fulldata.iz0
esym   = fulldata.esym
nblock = fulldata.nblock
iz     = fulldata.iz
iz1    = fulldata.iz1
cicode = fulldata.cicode
cfcode = fulldata.cfcode
ciion  = fulldata.ciion
cfion  = fulldata.cfion
bwno   = fulldata.bwno
nte    = fulldata.nte
te     = fulldata.te
szd    = fulldata.szd



openw, lun, outfile, /get_lun

str_sym = '  '
strput, str_sym, strupcase(esym)

str = string(nblock, format='(i5)') + '    /' + str_sym + '  IONISATION RATE COEFFICIENTS   /'

printf, lun, str


for j = 0, nblock-1 do begin

   str = ciion[j] + '/' + cfion[j] + '/' + string(nte[j], format='(i5)') + $
         '/I.P. = ' + string(bwno[j], format='(f18.1)') + $
         '/ICODE =' + string(fix(cicode[j]), format='(i3)') + $
         '/FCODE =' + string(fix(cfcode[j]), format='(i3)') + $
         '/ISEL = ' + string(j+1, format='(i3)')
   printf, lun, str

   printf, lun, te[0:nte[j]-1,j], format='(6E10.3)'
   printf, lun, szd[0:nte[j]-1,j], format='(6E10.3)'

endfor


; Append comments if supplied

n_com = n_elements(comments)

if n_com GT 0 then for j = 0, n_com-1 do printf, lun, comments[j]


free_lun, lun

END
