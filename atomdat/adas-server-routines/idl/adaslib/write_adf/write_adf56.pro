;----------------------------------------------------------------------
;+
; PROJECT    :  ADAS
;
; NAME       :  write_adf56
;
; PURPOSE    :  Write an adf56 file using the
;               fulldata structure.
;
;
; ARGUMENTS  :  All output arguments will be defined appropriately.
;
;               NAME      I/O    TYPE    DETAILS
; REQUIRED   :  fulldata   I       -     Structure containing the
;                                        adf56 details.
;               outfile    O      str    name of new adf56 file.
;               comments   I      str()  write these comments at end.
;
;
; KEYWORDS      help               -     if specified this comment
;                                        section is written to screen.
;                                                      
; NOTES      :  
;
; The fulldata structure is defined:
; index                   :  index of ground configuration of each ion of element in adf56 file
; config                :  ground configuration for each ion of element
; n_el                   :  number of electrons for each ion of element 
; no_v_shl              :  number of shells to treat as valence shells. Max. 2relevant to relating ion and parent. 
; v1_shl                 :  first valence shell position in adf56 configuration specifications. 
; v2_shl                 :  second valence shell position in adf56 configuration specifications. zero if none defined.
; drct_eval_v           :  evaluate direct ionisation from the valence shell(s). 
; drct_eval_cl          :  evaluate direct ionisation from other non-valence (closed) shells.
; min_shl_cl            :  lowest closed shell to include (position in adf56 configuration specifications).
; exca_eval_v2          :  evaluate excitation/autoionisation from second valence shell if identified. 
; max_dn_v2             :  maximum change in v2 n-shell to be included.
; min_dn_v2             :  minimum change in v2 n-shell to be include. 
; max_dl_v2             :  maximum change in v2 l-shell to be included.
; min_dl_v2             :  minimum change in v2 l-shell to be include. 
; exca_eval_cl          :  evaluate excitation/autoionisation from other non-valence (closed) shells.
; max_dn_cl             :  maximum change in closed n-shell to be included.
; min_dn_cl             :  minimum change in closed n-shell to be included.
; max_dl_cl             :  maximum change in closed l-shell to be included.
; min_dl_cl             :  minimum change in closed l-shell to be included.
; exst_eval              :  evaluate ionisation from excited states.
; exst_adf00_prt        :  assume parent for building excited states is as present in the adf00 data set for the ion.
; exst_prt_hole_shl    :  specify position of shell in ground configuration to form parent if not from adf00 above. 
; max_n_exst            :  maximum n-shell for excited states to be included.
; max_l_exst            :  maximum l-shell for excited states to be included.
; drct_eval_exst_v     :  evaluate direct ionisation from excited state valence shells. 
; drct_eval_exst_cl    :  evaluate direct ionisation from excited state non-valence (closed) shells.
; exca_eval_exst_v     :  evaluate excitation/autoionisation for excited states from valence shells (v1 and v2 above).
; exca_eval_exst_cl    :  evaluate excitation/autoionisation for excited states from non-valence (closed) shells. 
;
; AUTHOR     :  Adam Foster
;
; DATE       :  26-01-2009
;
;
; MODIFIED:
;       
;               
;
; VERSION:
;       1.1     26-01-20079
;-
;----------------------------------------------------------------------


pro write_adf56, fulldata = fulldata,  $
                 outfile  = outfile,   $
                 comments = comments,  $
                 help     = help

; If asked for help

if keyword_set(help) then begin
   doc_library, 'write_adf56'
   return
endif


; Extract necessary parts to write

config            = fulldata.config
index             = fulldata.index
n_el              = fulldata.n_el             
no_v_shl          = fulldata.no_v_shl         
v1_shl            = fulldata.v1_shl           
v2_shl            = fulldata.v2_shl           
drct_eval_v       = fulldata.drct_eval_v      
drct_eval_cl      = fulldata.drct_eval_cl     
min_shl_cl        = fulldata.min_shl_cl       
exca_eval_v2      = fulldata.exca_eval_v2     
max_dn_v2         = fulldata.max_dn_v2        
min_dn_v2         = fulldata.min_dn_v2        
max_dl_v2         = fulldata.max_dl_v2        
min_dl_v2         = fulldata.min_dl_v2        
exca_eval_cl      = fulldata.exca_eval_cl     
max_dn_cl         = fulldata.max_dn_cl        
min_dn_cl         = fulldata.min_dn_cl        
max_dl_cl         = fulldata.max_dl_cl        
min_dl_cl         = fulldata.min_dl_cl        
exst_eval         = fulldata.exst_eval        
exst_adf00_prt    = fulldata.exst_adf00_prt   
exst_prt_hole_shl = fulldata.exst_prt_hole_shl
max_n_exst        = fulldata.max_n_exst       
max_l_exst        = fulldata.max_l_exst       
drct_eval_exst_v  = fulldata.drct_eval_exst_v 
drct_eval_exst_cl = fulldata.drct_eval_exst_cl
exca_eval_exst_v  = fulldata.exca_eval_exst_v 
exca_eval_exst_cl = fulldata.exca_eval_exst_cl
   
; Open outfile and write it

openw, lun, outfile, /get_lun

n_max=180
nel=10

fmt='(i5)'

str = '/ADF56'+'    /'+'PROMOTION RULES'+'    /'+strtrim(string(n_max),2)+'    /'+strtrim(string(nel),2)
printf,lun,str

for i = 0,(n_max/nel)-1 do begin

  l = i*nel 
  m = 9+i*nel

  printf,lun,'index    config'
  
  for j=0, nel-1 do begin
    a = string(index[l+j],format=fmt)+'    '
    b = strtrim(string(config[l+j],format='(a85)'),1)
    str = a+b
    printf,lun,str
  endfor
  printf,lun,''

  printf,lun,'index             ',string(index[l : m],format=fmt)

  printf,lun,'n_el              ',string(n_el[l : m],format=fmt)

  printf,lun,'no_v_shl          ',string(no_v_shl[l : m],format=fmt)

  printf,lun,'v1_shl            ',string(v1_shl[l : m],format=fmt)

  printf,lun,' v2_shl           ',string( v2_shl[l : m],format=fmt)

  printf,lun,'drct_eval_v       ',string(drct_eval_v[l : m],format=fmt)

  printf,lun,'drct_eval_cl      ',string(drct_eval_cl[l : m],format=fmt)

  printf,lun,'min_shl_cl        ',string(min_shl_cl[l : m],format=fmt)

  printf,lun,'exca_eval_v2      ',string(exca_eval_v2[l : m],format=fmt)

  printf,lun,'max_dn_v2         ',string(max_dn_v2[l : m],format=fmt)

  printf,lun,'min_dn_v2         ',string(min_dn_v2[l : m],format=fmt)

  printf,lun,'max_dl_v2         ',string(max_dl_v2[l : m],format=fmt)

  printf,lun,'min_dl_v2         ',string(min_dl_v2[l : m],format=fmt)

  printf,lun,'exca_eval_cl      ',string(exca_eval_cl[l : m],format=fmt)

  printf,lun,'max_dn_cl         ',string(max_dn_cl[l : m],format=fmt)

  printf,lun,'min_dn_cl         ',string(min_dn_cl[l : m],format=fmt)

  printf,lun,'max_dl_cl         ',string(max_dl_cl[l : m],format=fmt)

  printf,lun,'min_dl_cl         ',string(min_dl_cl[l : m],format=fmt)

  printf,lun,'exst_eval         ',string(exst_eval[l : m],format=fmt)

  printf,lun,'exst_adf00_prt    ',string(exst_adf00_prt[l : m],format=fmt)

  printf,lun,'exst_prt_hole_shl ',string(exst_prt_hole_shl[l : m],format=fmt)

  printf,lun,'max_n_exst        ',string(max_n_exst[l : m],format=fmt)

  printf,lun,'max_l_exst        ',string(max_l_exst[l : m],format=fmt)

  printf,lun,'drct_eval_exst_v  ',string(drct_eval_exst_v[l : m],format=fmt)

  printf,lun,'drct_eval_exst_cl ',string(drct_eval_exst_cl[l : m],format=fmt)

  printf,lun,'exca_eval_exst_v  ',string(exca_eval_exst_v[l : m],format=fmt)

  printf,lun,'exca_eval_exst_cl ',string(exca_eval_exst_cl[l : m],format=fmt)

  printf,lun,''

endfor

n_com = n_elements(comments)

if n_com GT 0 then for k = 0, n_com-1 do printf, lun, comments[k]

close,lun
free_lun, lun
                
end
