;----------------------------------------------------------------------
;+
; PROJECT    :  ADAS
;
; NAME       :  write_adf12
;
; PURPOSE    :  Write an adf12 CX effictive emissivity file using the
;               fulldata structure (from read_adf12 or elsewhere).
;
;
; ARGUMENTS  :  All output arguments will be defined appropriately.
;
;               NAME      I/O    TYPE    DETAILS
; REQUIRED   :  fulldata   I       -     Structure containing the
;                                        adf12 details.
;               outfile    O      str    name of new adf12 file.
;               comments   I      str()  write these comments at end.
;
; KEYWORDS      help               -     if specified this comment
;                                        section is written to screen.
;
;
; NOTES      :  If no comments are given 'c---------' is appended to
;               the dataset to enable read_adf12.pro to identify the
;               end of useful data.
;
; The fulldata structure is defined:
;
;               nbsel        long    number of blocks present
;               csymb()      str     element symbol
;               czion()      str     emitting ion charge
;               cwavel()     str     wavelength (A)
;               cdonor()     str     donor neutral atom
;               crecvr()     str     receiver nucleus
;               ctrans()     str     transition
;               cfile()      str     specific ion file source
;               ctype()      str     type of emissivity
;               cindm()      str     emissivity index
;               qefref()     double  reference value of rate coefficient
;               enref()      double      "       "   "  energy
;               teref()      double      "       "   "  temperature
;               deref()      double      "       "   "  density
;               zeref()      double      "       "   "  effective z
;               bmref()      double      "       "   "  magnetic field
;               nenera()     long    number of energies
;               ntempa()     long    number of temperatures
;               ndensa()     long    number of densities
;               nzeffa()     long    number of effective z's
;               nbmaga()     long    number of magnetic field values
;               enera(,)     double  energies
;               qenera(,)    double  rate coefficients for energy value
;               tempa(,)     double  temperatures
;               qtempa(,)    double  rate coefficients for temperatures
;               densa(,)     double  densities
;               qdensa(,)    double  rate coefficients for desnities
;               zeffa(,)     double  effective z
;               qzeffa(,)    double  rate coefficients for effective z
;               bmaga(,)     double  magnetic field
;               qbmaga(,)    double  rate coefficients for magnetic fields
;                                        1st dim: 12 or 24  depending on parameter
;                                        2nd dim: nstore
;
;
; AUTHOR     :  Martin O'Mullane
;
; DATE       :  20-11-2008
;
;
; MODIFIED:
;       1.1     Martin O'Mullane
;               - First version.
;
; VERSION:
;       1.1    20-11-2008
;-
;----------------------------------------------------------------------


PRO write_adf12_values, lun, value, name

; specific to adf12 - set numeric formats here

fmt = '(6e10.2)'
if strpos(strupcase(name), 'NPARMSC') NE -1 then fmt = '(6i10)'

n_val   = n_elements(value)
n_lines = numlines(n_val, 6)

i1 = 0
i2 = min([n_val, 6])

for j = 0, n_lines-1 do begin

   str = string(replicate(32B, 70))

   if n_val EQ 1 then strput, str, string(value, format=fmt) $
                 else strput, str, string(value[i1:i2-1], format=fmt)
   if j EQ 0 then strput, str, name, 63

   i1 = (j+1)*6
   i2 = min([n_val, i1+6])

   printf, lun, strtrim(str)

endfor

END
;----------------------------------------------------------------------


PRO write_adf12, fulldata = fulldata,  $
                 outfile  = outfile,   $
                 comments = comments,  $
                 help     = help

; If asked for help

if keyword_set(help) then begin
   doc_library, 'write_adf12'
   return
endif

; Extract necessary parts to write


nbsel  = fulldata.nbsel
csymb  = fulldata.csymb
czion  = fulldata.czion
cwavel = fulldata.cwavel
cdonor = fulldata.cdonor
crecvr = fulldata.crecvr
ctrans = fulldata.ctrans
cfile  = fulldata.cfile
ctype  = fulldata.ctype
cindm  = fulldata.cindm
qefref = fulldata.qefref
enref  = fulldata.enref
teref  = fulldata.teref
deref  = fulldata.deref
zeref  = fulldata.zeref
bmref  = fulldata.bmref
nenera = fulldata.nenera
ntempa = fulldata.ntempa
ndensa = fulldata.ndensa
nzeffa = fulldata.nzeffa
nbmaga = fulldata.nbmaga
enera  = fulldata.enera
tempa  = fulldata.tempa
densa  = fulldata.densa
zeffa  = fulldata.zeffa
bmaga  = fulldata.bmaga
qenera = fulldata.qenera
qtempa = fulldata.qtempa
qdensa = fulldata.qdensa
qzeffa = fulldata.qzeffa
qbmaga = fulldata.qbmaga

wave = float(fulldata.cwavel)



; Open outfile and write to it

openw, lun, outfile, /get_lun

printf, lun, nbsel, format = '(i5)'

for j = 0, nbsel-1 do begin

   isel = j + 1

   str = 'E=' + strmid(csymb[j], 0, 2) + '  ' + $
         'W=' + string(wave[j], format='(f8.2)') +' ' + $
         'D=' + strmid(cdonor[j], 0, 6) +  $
         'R=' + strmid(crecvr[j], 0, 6) +  $
         'N=' + strmid(ctrans[j], 0, 8) +  $
         'F=' + strmid(cfile[j], 0, 10) +  ' ' + $
         'M=' + strmid(ctype[j], 0, 4) + ' ' + $
         'isel=' + string(isel, format='(i4)')
   printf, lun, str

   parmref = [enref[j], teref[j], deref[j], zeref[j], bmref[j]]
   nparmsc = [nenera[j], ntempa[j], ndensa[j], nzeffa[j], nbmaga[j]]

   write_adf12_values, lun, qefref[j]  , 'qefref'
   write_adf12_values, lun, parmref , 'parmref'
   write_adf12_values, lun, nparmsc , 'nparmsc'
   write_adf12_values, lun, enera[0:nenera[j]-1, j]    , 'ener'
   write_adf12_values, lun, qenera[0:nenera[j]-1, j]   , 'qener'
   write_adf12_values, lun, tempa[0:ntempa[j]-1, j]    , 'tiev'
   write_adf12_values, lun, qtempa[0:ntempa[j]-1, j]   , 'qtiev'
   write_adf12_values, lun, densa[0:ndensa[j]-1, j]   , 'densi'
   write_adf12_values, lun, qdensa[0:ndensa[j]-1, j]  , 'qdensi'
   write_adf12_values, lun, zeffa[0:nzeffa[j]-1, j]    , 'zeff'
   write_adf12_values, lun, qzeffa[0:nzeffa[j]-1, j]   , 'qzeff'
   write_adf12_values, lun, bmaga[0:nbmaga[j]-1, j]    , 'bmag'
   write_adf12_values, lun, qbmaga[0:nbmaga[j]-1, j]   , 'qbmag'

endfor


; Comments - note we need a comment line to identify the end of useful data

n_com = n_elements(comments)

if n_com GT 0 then for j = 0, n_com-1 do printf, lun, comments[j] $
              else printf, lun, 'c---------'


; Finish up

free_lun, lun

END
