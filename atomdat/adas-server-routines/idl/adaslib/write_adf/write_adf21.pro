;----------------------------------------------------------------------
;+
; PROJECT    :  ADAS
;
; NAME       :  write_adf21
;
; PURPOSE    :  Write an adf21/adf22 BMS/BME/BMP from a
;               fulldata structure (from read_adf21 or elsewhere).
;
;
; ARGUMENTS  :  All output arguments will be defined appropriately.
;
;               NAME      I/O    TYPE    DETAILS
; REQUIRED   :  fulldata   I       -     Structure containing the
;                                        adf21 details.
;               outfile    O      str    name of new adf21 file.
;               comments   I      str()  write these comments at end.
;
; OPTIONAL      hecoeff    I      int    1-9 corresponding to He beanm GCR coefficients
;                                        gcrc11, 12, 13, 21, 22, 23, 31, 32, 33
;                                        if not set H beam is assumed.
;               code       I      str    name of processing code; 'unknown' is the
;                                        default if not set.
;
; KEYWORDS      help               -     if specified this comment
;                                        section is written to screen.
;
;
; NOTES      :
;
; The fulldata structure is defined:
;
;       itz    : target ion charge.
;       tsym   : target ion element symbol.
;       beref  : reference beam energy (eV/amu).
;       tdref  : reference target density (cm-3).
;       ttref  : reference target temperature (eV).
;       svref  : stopping coefft. at reference
;                beam energy, target density and
;                temperature (cm3 s-1).
;       be     : beam energies(eV/amu).
;       tdens  : target densities(cm-3).
;       ttemp  : target temperatures (eV).
;       svt    : stopping coefft. at reference beam
;                energy and target density (cm3 s-1)
;       sved   : stopping coefft. at reference target
;                temperature (cm3 s-1).
;
;
; AUTHOR     :  Martin O'Mullane
;
; DATE       :  02-06-2009
;
;
; MODIFIED:
;       1.1     Martin O'Mullane
;               - First version.
;
; VERSION:
;       1.1    02-06-2009
;-
;----------------------------------------------------------------------



PRO write_adf21, fulldata = fulldata,  $
                 outfile  = outfile,   $
                 code     = code,      $
                 hecoeff  = hecoeff,   $
                 comments = comments,  $
                 help     = help

; If asked for help

if keyword_set(help) then begin
   doc_library, 'write_adf21'
   return
endif

if n_elements(code) GT 0 then code_str = code else code_str = 'unknown'
if n_elements(hecoeff) GT 0 then id_str = 'GCRC' + string(hecoeff, format='(i1)')$
                            else id_str = 'SCREF'


; Extract necessary parts to write

itz    = fulldata.itz
tsym   = fulldata.tsym
beref  = fulldata.beref
tdref  = fulldata.tdref
ttref  = fulldata.ttref
svref  = fulldata.svref
be     = fulldata.be
tdens  = fulldata.tdens
ttemp  = fulldata.ttemp
svt    = fulldata.svt
sved   = fulldata.sved


; Write the file

line  = '--------------------------------------------------------------------------------'
fmt   = '(8E10.3,:)'
today = xxdate()
ibval = n_elements(fulldata.be)
idval = n_elements(fulldata.tdens)
itval = n_elements(fulldata.ttemp)



openw, lun, outfile, /get_lun

str = string(itz, format='(i5)') + ' /' + id_str + '=' + $
      string(svref, format='(E9.3)') + ' /SPEC=' + tsym + $
      ' /DATE=' + today[0] + ' /CODE=' + code_str
printf, lun, str
printf, lun, line

str = string(ibval, format='(i5)') + string(idval, format='(i5)') + $
      ' /TREF=' + string(ttref, format='(E9.3)')
printf, lun, str
printf, lun, line

printf, lun, be, format=fmt
printf, lun, tdens, format=fmt
printf, lun, line

for j = 0, idval-1 do printf, lun, sved[*,j], format=fmt
printf, lun, line

str = string(itval, format='(i5)') + ' /EREF=' + $
      string(beref, format='(E9.3)') + ' /NREF=' + $
      string(tdref, format='(E9.3)')
printf, lun, str
printf, lun, line
printf, lun, ttemp, format=fmt
printf, lun, line
printf, lun, svt, format=fmt
printf, lun, line

; Comments - note we need a comment line to identify the end of useful data

n_com = n_elements(comments)

if n_com GT 0 then for j = 0, n_com-1 do printf, lun, comments[j]

free_lun, lun

END
