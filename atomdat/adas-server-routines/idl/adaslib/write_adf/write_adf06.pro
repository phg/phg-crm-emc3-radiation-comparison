;----------------------------------------------------------------------
;+
; PROJECT    :  ADAS
;
; NAME       :  write_adf06
;
; PURPOSE    :  Write an adf06 specific ion file using the
;               fulldata structure (from read_adf06 or elsewhere).
;
;
; ARGUMENTS  :  All output arguments will be defined appropriately.
;
;               NAME      I/O    TYPE    DETAILS
; REQUIRED   :  fulldata   I       -     Structure containing the
;                                        adf06 details.
;               outfile    O      str    name of new adf06 file.
;               comments   I      str()  write these comments at end.
;
; KEYWORDS      help               -     if specified this comment
;                                        section is written to screen.
;
;
; NOTES      :
;
; The fulldata structure is defined:
;
;                filename            :  adf06 filename
;                iz                  :  recombined ion charge
;                iz0                 :  nuclear charge
;                iz1                 :  recombining ion charge
;                ia()                :  energy level index number
;                                            dimension - level index
;                cstrga()            :  nomenclature/configuration for level 'ia()'
;                isa()               :  multiplicity for level 'ia()'
;                                       note: (isa-1)/2 = quantum number (s)
;                                            dimension - level index
;                ila()               :  quantum number (l) for level 'ia()'
;                                            dimension - level index
;                xja()               :  quantum number (j-value) for level 'ia()'
;                                       note: (2*xja)+1 = statistical weight
;                                            dimension - level index
;                wa()                :  energy relative to level 1 (cm-1) for level'ia()'
;                                            dimension - level index
;                zpla(,)             :  eff. zeta param. for contributions to ionis. of level
;                                           1st dimension - parent index
;                                           2nd dimension - level index
;                ipla(,)             :  parent index for contributions to ionis. of level
;                                            1st dimension - parent index
;                                            2nd dimension - level index
;                ltied()             :  1 => specified level tied
;                                       0 => specified level is untied
;                                            dimension - level index
;                bwno                :  ionisation potential (cm-1) of lowest parent
;                bwnoa()             :  ionisation potential (cm-1) of parents
;                                            dimension - metastable index
;                cprta               :
;                te()                :  input data file: electron temperatures (K) type 3
;                                                        X = e/delta_E for type 1
;                                            dimension - temperature
;                lower()             :  upper index for transition
;                                            dimension - transition
;                upper()             :  lower index for transition
;                                            dimension - transition
;                aval()              :  E2 quadrupole A-value
;                                            dimension - transition
;                gamma(,,)           :  gamma values
;                                           effective collision strength for type 3
;                                           collision strength for type 1
;                                           scaled rate coefft.(cm3 sec-1)(case 'S')
;                                           1st dimension - transition number
;                                           2nd dimension - temperature 'scef()'
;                                           3rd dimension - ion projectile case
;                lbeth               :  1 => Bethe data on transition lines
;                                       0 => not present in file
;                beth(,)             :  Born limit
;                                            1st dimension - transition
;                                            2nd dimension - ion projectile case
;                tcode()             : data type
;                                            ' ' => ion impact transition
;                                            's','S' => Ionisation from current ion
;                                            dimension - transition
;                projectile()        : name of projectile, eg 7^Li
;                                            dimension - projecties
;                mass_target()       : mass of target
;                                            dimension - projecties
;                charge_projectile() : charge of projectile
;                                            dimension - projecties
;                mass_projectile()   : mass of projectile
;                level_ion()         : Ionisation from level
;                                            set to -1 if no data present
;                                            dimension - ionisation transitions
;                parent_ion()        : Ionisation to parent
;                                            set to -1 if no data present
;                                            dimension - ionisation transitions
;                ion(,,)             :  ion values
;                                           effective collision strength for type 3
;                                           collision strength for type 1
;                                           scaled, mass-independent, rate coefft.(cm3 sec-1)(case 'S')
;                                           1st dimension - transition number
;                                           2nd dimension - temperature 'te()'
;                                           3rd dimension - ion projectile case
;                iadftyp             : as input
;
;
;
; AUTHOR     :  Martin O'Mullane
;
; DATE       :  30-10-2016
;
;
; MODIFIED:
;       1.1     Martin O'Mullane
;                 - First version.
;
; VERSION:
;       1.1    30-10-2016
;-
;----------------------------------------------------------------------


FUNCTION write_adf06_str, array

tmp = string(array, format='(50(e9.2,:))')
str = strjoin(strsplit(tmp, 'e', /extract))

return, str

END
;---------------------------------------------------------------------------



PRO write_adf06, fulldata = fulldata,  $
                 outfile  = outfile,   $
                 comments = comments,  $
                 help     = help

; If asked for help

if keyword_set(help) then begin
   doc_library, 'write_adf06'
   return
endif


; Extract necessary parts to write

iz        = fulldata.iz
iz1       = fulldata.iz + 1
iz0       = fulldata.iz0
ia        = fulldata.ia
cstrga    = fulldata.cstrga
isa       = fulldata.isa
ila       = fulldata.ila
xja       = fulldata.xja
wa        = fulldata.wa
bwnoa     = fulldata.bwnoa
cprta     = fulldata.cprta
zpla      = fulldata.zpla
ipla      = fulldata.ipla
te        = fulldata.te
lower     = fulldata.lower
upper     = fulldata.upper
aval      = fulldata.aval
gamma     = fulldata.gamma
iadftyp   = fulldata.iadftyp
proj      = fulldata.projectile
z_proj    = fulldata.charge_projectile
mass_tar  = fulldata.mass_target
mass_proj = fulldata.mass_projectile


n_level = n_elements(isa)
n_te    = n_elements(te)
n_tran  = n_elements(upper)
n_proj  = n_elements(proj)

ind = where(strpos(tag_names(fulldata), 'BETH') EQ 0, count)
if count EQ 1 then begin
   is_beth = 1
   beth = fulldata.beth
   if total(beth) EQ 0.0 then begin
      is_beth=0
      message, 'No meaningful limit points - not written', /continue
   endif
endif else is_beth=0


; Top line

esym = '  +'
strput, esym, strupcase(xxesym(iz0))
esym = esym + string(iz, format='(i2)')

str = string(esym, iz0, iz1, format='(a5,7x,i3,7x,i3,5x)')
for j = 0, n_elements(bwnoa)-1 do str = str + string(bwnoa[j], cprta[j], format='(f11.1,a4)')

outstr = str


; Levels

if max(ila) GE 10 then ilgap = 2 else ilgap = 1

ilen = max(strlen(strtrim(cstrga)))

if ilen LT 18 then igap = 18 - ilen + 1 else igap = 1

fmt = '(i5,1x,a' + string(ilen) + ',' + string(igap) + $
      'x, "(",i1,")",i' + string(ilgap) + ',"(",f4.1,")",1x,f17.4)'
fmt = strcompress(fmt, /remove_all)

for i = 0, n_level-1 do begin

   str = string(ia[i], cstrga[i], isa[i], ila[i], xja[i], wa[i], format=fmt)

   if total(zpla[*,i]) GT 0 then begin

      sz = size(zpla, /dim)
      for j = 0, sz[0]-1 do begin
         if ipla[j,i] GT 0 then str = str + '   {' +                           $
                                      string(ipla[j,i], format='(i1)') + '}' + $
                                      string(zpla[j,i], format='(f5.3)')
      endfor

   endif else str = str + '   {X}'

   outstr = [outstr, str]

endfor
outstr = [outstr, '   -1']


; Excitation collision strengths

str = write_adf06_str(te)
tmp = string(iz1, format='(f5.1)') + '    ' + string(iadftyp, format='(i1)') + '      ' + str
outstr = [outstr, tmp]

for ip = 0, n_proj-1 do begin

   str = '/mt = x.xxxxxxx /zp = xxxxxx /mp = xxxxxxxxx / xx^X  impact /'
   strput, str, string(mass_tar[ip], format='(f9.4)'), 6
   strput, str, string(z_proj[ip], format='(f6.2)'), 22
   strput, str, string(mass_proj[ip], format='(f9.4)'), 35
   strput, str, proj[ip], 47

   outstr = [outstr, str]

   for j = 0L, n_tran-1 do begin

      if upper[j] NE lower[j] then begin

            array = [aval[j], reform(gamma[j,*,ip])]
            if is_beth EQ 1 then array = [array, beth[j,ip]]
            str = write_adf06_str(array)
            tmp = string([upper[j], lower[j]], format='(2i4)')
            outstr = [outstr, tmp + str]

      endif else message, 'self transition : ' + string(upper[j]), /continue

   endfor

   ; Other processes - S lines

   if fulldata.level_ion[0] NE -1 then begin

      for j = 0, n_elements(fulldata.level_ion)-1 do begin

        str = 'S' + string(fulldata.level_ion[j], format='(i3)') + '  +' + $
                    string(fulldata.parent_ion[j], format='(i1)') + '        ' + $
                    write_adf06_str(fulldata.ion[j,*,ip])

        outstr = [outstr, str]

      endfor

   endif

endfor

outstr = [outstr, '  -1']
outstr = [outstr, '  -1  -1']

; Write the file to disk

adas_writefile, file=outfile, all=outstr, comments=comments

END
