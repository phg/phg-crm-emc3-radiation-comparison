;----------------------------------------------------------------------
;+
; PROJECT    :  ADAS
;
; NAME       :  write_adf13
;
; PURPOSE    :  Write an adf13 ionisations per photon (SXB) file using
;               the fulldata structure (from read_adf13 or elsewhere).
;
;
; ARGUMENTS  :  All output arguments will be defined appropriately.
;
;               NAME      I/O    TYPE    DETAILS
; REQUIRED   :  fulldata   I       -     Structure containing the
;                                        adf13 details.
;               outfile    O      str    name of new adf13 file.
;               comments   I      str()  write these comments at end.
;
; KEYWORDS      help               -     if specified this comment
;                                        section is written to screen.
;
;
; NOTES      :  Not a lot of testing yet. The full flexibility of
;               the adf13 format is not supported.
;
; The fulldata structure is defined:
;
;              a13file : filename
;              esym    : element
;              iz0     : atomic number
;              iz      : ion charge
;              iz1     : recombined ion charge
;              ndens   : number of densities
;              nte     :           temperatures
;              dens    : densities
;              te      : temperatures
;              cpcode  : production code
;              cindm   : metastable
;              cwavel  : wavelength
;              cfile   : adf04 file (fragment)
;              sxb     : ionisations per photon coeffs.
;
;
; AUTHOR     :  Martin O'Mullane
;
; DATE       :  17-07-2017
;
;
; MODIFIED:
;       1.1     Martin O'Mullane
;               - First version.
;
; VERSION:
;       1.1    17-07-2017
;-
;----------------------------------------------------------------------


PRO write_adf13, fulldata = fulldata,  $
                 outfile  = outfile,   $
                 comments = comments,  $
                 help     = help

; If asked for help

if keyword_set(help) then begin
   doc_library, 'write_adf13'
   return
endif

; Extract necessary parts to write

esym   = fulldata.esym
iz0    = fulldata.iz0
iz     = fulldata.iz
iz1    = fulldata.iz1
ndens  = fulldata.ndens
nte    = fulldata.nte
dens   = fulldata.dens
te     = fulldata.te
cpcode = fulldata.cpcode
cindm  = fulldata.cindm
cwavel = fulldata.cwavel
cfile  = fulldata.cfile
sxb    = fulldata.sxb

nblock = n_elements(nte)

; Open outfile and write it

fmt = '(8E9.2,:)'

openw, lun, outfile, /get_lun

str = string(nblock, format='(i5)') + '    /' + esym + '+' + $
      string(iz, format='(i2)') + ' IONISATIONS PER PHOTON/'

printf, lun, str

for j = 0, nblock-1 do begin

   isel = j + 1

   str = cwavel[j] + string(ndens[j], format='(i4)') +  $
         string(nte[j], format='(i4)') + ' /FILMEM = ' + cfile[j] + $
         '/CODE = ' + cpcode[j] + ' /INDM =' + cindm[j] + $
         '/ISEL = ' + string(isel, format='(i4)')
   printf, lun, str

   if nblock EQ 1 then begin
     d_out = dens
     t_out = te
     p_out = sxb
   endif else begin
     d_out = reform(dens[*,j])
     t_out = reform(te[*,j])
     p_out = reform(sxb[*,*,j])
   endelse

   printf, lun, d_out, format=fmt
   printf, lun, t_out, format=fmt

   for id = 0, ndens[j]-1 do printf, lun, p_out[*,id], format=fmt

endfor

n_com = n_elements(comments)

if n_com GT 0 then for j = 0, n_com-1 do printf, lun, comments[j]


free_lun, lun

END
