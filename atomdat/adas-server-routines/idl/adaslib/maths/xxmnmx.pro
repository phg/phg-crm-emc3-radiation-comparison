;----------------------------------------------------------------------
;+
; PROJECT    :  ADAS
;
; NAME       :  xxmnmx
;
; PURPOSE    :  Minimax polynominal fit to tabulated data. The coefficients
;               c0, c1, c2 etc for the fit c0 + c1*x + c2*x^2 + ....
;
; ARGUMENTS  :  All output arguments will be defined appropriately.
;               Inputs will be converted to correct type, if possible,
;               internally without changing calling type.
;
;                xxmnmx
;
;               NAME      I/O    TYPE    DETAILS
; REQUIRED   :  maxdeg     I     int     Maximum possible degree of polynomial
;                                        allowed in the minimax fitting
;                                        (defaults to 8 and must be < 26)
;               tolval     I     float   Fractional tolerance for acceptance
;                                        of data fitted  by  minimax polynomial.  (if it equals  zero
;                                        (default is 0.0 which then runs to
;                                        max. degree).
;               xin()      I     float   Input independent variable
;               yin()      I     float   Input dependent variable
;               coef()     O     float   minimax coefficients. Size is
;                                        maxdeg+1.
;               info       O     str     Information on goodness of fit.
;
; KEYWORDS      logfit            -      Fitting is performed in log-log space.
;               help              -      Help to screen.
;
;
; NOTES      :  Calls fortran code.
;
;
; AUTHOR     :  Martin O'Mullane
;
; DATE       :  10/07/15
;
;
; MODIFIED:
;       1.1     Martin O'Mullane
;              	- First version.
;
; VERSION:
;       1.1    10-07-2015
;
;-
;----------------------------------------------------------------------

PRO xxmnmx, maxdeg = maxdeg, $
            tolval = tolval, $
            xin    = xin,    $
            yin    = yin,    $
            coef   = coef,   $
            logfit = logfit, $
            info   = info,   $
            help   = help

; If asked for help

if keyword_set(help) then begin
   doc_library, 'xxminv'
   return
endif

if keyword_set(logfit) then il_logfit = 1L else il_logfit = 0L

if n_elements(tolval) GT 0 then tolval = double(tolval/100.0) else tolval = 0.0D0
if n_elements(maxdeg) GT 0 then maxdeg = long(maxdeg) else maxdeg = 8

if maxdeg GT 26 then message, 'Maximum degree is 26'
if maxdeg LT 0 then message, 'Maximum degree must be positive'

xin  = double(xin)
yin  = double(yin)
nin  = n_elements(xin)

if nin GT 100 then message, 'tabulated data has 100 element limit'

; Set output interpolated vector to input

nout = nin
xout = xin
yout = yin

ncoef = -1L
coef  = dblarr(maxdeg+1)
cinfo = lonarr(80)


fortdir = getenv('ADASFORT')

dummy = CALL_EXTERNAL(fortdir+'/xxmnmx_if.so','xxmnmx_if', $
                      il_logfit, maxdeg, tolval,           $
                      nin, xin, yin, nout, xout, yout,     $
                      ncoef, coef, cinfo)

info = string(byte(cinfo))
coef = coef[0:ncoef-1]

END
