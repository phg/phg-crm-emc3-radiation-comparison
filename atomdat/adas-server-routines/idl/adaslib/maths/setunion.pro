; Copyright (c) 1999 Strathclyde University .
; SCCS info: Module @(#)$Header: /home/adascvs/idl/adaslib/maths/setunion.pro,v 1.1 2004/07/06 15:19:26 whitefor Exp $    Date $Date: 2004/07/06 15:19:26 $
;-----------------------------------------------------------------------------
;+
; This function is from RSI and was downloaded from
;        http://www.dfanning.com/tips/set_operations.html
;
; I assume there are no restrictions on further dissemination.
;-
;-----------------------------------------------------------------------------

FUNCTION SetUnion, a, b
IF a[0] LT 0 THEN RETURN, b    ;A union NULL = a
IF b[0] LT 0 THEN RETURN, a    ;B union NULL = b
RETURN, Where(Histogram([a,b], OMin = omin)) + omin ; Return combined set
END
