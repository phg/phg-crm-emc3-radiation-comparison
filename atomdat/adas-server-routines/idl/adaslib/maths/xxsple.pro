; SCCS Info Module @(#)$Header: /home/adascvs/idl/adaslib/maths/xxsple.pro,v 1.2 2007/07/16 18:54:25 mog Exp $ Date: $Date: 2007/07/16 18:54:25 $
;
;----------------------------------------------------------------------
;+
; PROJECT    :  ADAS
;
; NAME       :  xxsple
;
; PURPOSE    :  IDL translation of ADAS xxsple spline routine.
;
; ARGUMENTS  :  All output arguments will be defined appropriately.
;               Inputs will be converted to correct type, if possible,
;               internally without changing calling type.
;
;                xxcftr, opt=opt, myfunct='test'  etc
;
;               NAME      I/O    TYPE    DETAILS
; REQUIRED   :  opt        I     int     type of spline - set table below
;               myfunct    I     str     name of function of the interpolating
;                                        x-coordinate transformation.
;                                        defaults to x->x.
;               xin()      I     dbl     independent variable
;               yin()      I     dbl     dependent variable
;               xout()     I     dbl     requested points
;               yout()     O     dbl     calculated values
;               dy()       O     dbl     derivatives at input knots
;               extrap()   O     int     flag indicating extrapolation
;                                         1 : yes
;                                         0 : no
;               interp()   O     int     flag indicating extrapolation
;                                         1 : yes
;                                         0 : no
;
;
;
; KEYWORDS      log                -     interpolate works in log space.
;
;
; NOTES      :
;         --------------------------------------------------------------
;         | IOPT  | NOPT |  DY(1)  DDY(1)  |  DY(N)   DDY(N)  |EXTRAP'N|
;         |-------|------|-----------------|------------------|--------|
;         | < 0   |   0  |    -     0.0    |    -      0.0    |  NO    |
;         |   0   |   0  |    -     0.0    |    -      0.0    |  YES   |
;         |   1   |   1  |    -     0.0    |  -1.5      -     |  YES   |
;         |   2   |   2  |   0.0     -     |   1.0      -     |  YES   |
;         |   3   |   3  |  -0.5     -     |  -1.5      -     |  YES   |
;         |   4   |   4  |   0.0     -     |    -      0.0    |  YES   |
;         |   5   |   5  |  -4.5     -     |  -1.5      -     |  YES   |
;         |   6   |   6  |  +0.5     -     |    -      0.0    |  YES   |
;         |   7   |   7  |  -3.5     -     |    -      0.0    |  YES   |
;         --------------------------------------------------------------
;
;         This is a direct translation from the fortran and is not
;         optimised in the IDL way.
;
;
; AUTHOR     :  Martin O'Mullane
;
; DATE       :  11-02-2004
;
;
; MODIFIED:
;       1.1     Martin O'Mullane
;               - First version.
;       1.2     Martin O'Mullane
;               - Correct extrap output variable and add complementary
;                 interp as an output.
;
;  VERSION:
;       1.1    11-02-2004
;       1.2    11-07-2007
;-
;----------------------------------------------------------------------


FUNCTION xxsple_function, x

; Default function if none supplied

return, x

END
;-----------------------------------------------------------------------



PRO xxsple, opt     = opt,     $
            myfunct = myfunct, $
            xin     = xin,     $
            yin     = yin,     $
            xout    = xout,    $
            yout    = yout,    $
            dy      = dy,      $
            extrap  = extrap,  $
            interp  = interp,  $
            log     = log


; Check the input

if n_elements(opt) EQ 0 then iopt = 0 else iopt = long(opt)
if n_elements(myfunct) EQ 0 then fintx = 'xxsple_function' $
                            else fintx = myfunct

if n_elements(xout) EQ 0 then message, 'Where is the requested x'

; Internal variables - convert to double

xx_xout  = double(xout)
xx_xin   = double(xin)
xx_yin   = double(yin)

nout     = n_elements(xout)
xx_yout  = dblarr(nout)
interp   = intarr(nout)


; log or linear

if keyword_set(log) then begin

   xx_xout = alog(xx_xout)
   xx_xin = alog(xx_xin)
   xx_yin = alog(xx_yin)

endif

; Set up internal parameters

qval  = [ -0.5, -0.5,  0.0,  0.0,  0.0,  0.0,  0.0,  0.0]
d2val = [  1.5,  1.5,  0.0,  0.0,  0.0,  0.0,  0.0,  0.0]
d3val = [  0.0,  0.0,  0.0, -0.5,  0.0, -4.5,  0.5, -3.5]
uval  = [  0.0, -1.5,  1.0, -1.5,  0.0, -1.5,  0.0,  0.0]
luval = [    1,    0,    0,    0,    1,    0,    1,    1]

agrl  = dblarr(4)

; Set up arrays for splining

nin    = n_elements(xx_xin)
dy     = dblarr(nin)

nknots = nin + 1
x      = dblarr(nknots)
dely   = dblarr(nknots)
h      = dblarr(nknots)
q      = dblarr(nknots)
u      = dblarr(nknots)
d1     = dblarr(nknots)
d2     = dblarr(nknots)
d3     = dblarr(nknots)


nopt = max([0, iopt])


; Parameters for xx_xin axis

cmd = 'x = ' + fintx + '(xx_xin)'
res = execute(cmd)


h[1]  = x[1] - x[0]
q[0]  = qval[nopt]
d2[0] = d2val[nopt] / h[1]
d3[0] = d3val[nopt]

nin0  = nin-1

for i = 1, nin0-1 do begin
   h[i+1] = x[i+1] - x[i]
   t1     = 1.0 / ( h[i+1] + h[i] )
   t2     = h[i+1] * t1
   t3     = 1.0 - t2
   t4     = 1.0 / ( ( t2 * q[i-1] ) + 2.0 )
   q[i]   = -t3 * t4
   d1[i]  = ( 3.0 * t4 * t2 ) / h[i]
   d2[i]  = ( 3.0 * t4 * t3 ) / h[i+1]
   d3[i]  =  t2 * t4
endfor

t4        = 1.0 / ( q[nin0-1] + 2.0 )
d1[nin-1] = ( 3.0 * t4 ) / h[nin-1]
d3[nin-1] = t4



; Setup cubic spline derivatives

dely[1] = xx_yin[1] - xx_yin[0]
u[0]    = ( d2[0] * dely[1] ) + d3[0]

for i = 1, nin0-1 do begin
   dely[i+1] = xx_yin[i+1] - xx_yin[i]
   u[i]      = ( d1[i] * dely[i] ) + ( d2[i] * dely[i+1] ) - $
               ( d3[i] * u[i-1] )
endfor

if luval[nopt] then u[nin-1] = ( d1[nin-1] * dely[nin-1] ) - $
                               ( d3[nin-1] * u[nin0-1] )     $
               else u[nin-1] = uval[nopt]

dy[nin-1] = u[nin-1]

for i = nin0-1, 0, -1 do dy[i] = ( q[i] * dy[i+1] ) + u[i]


; Set up parameters relating to the requested 'xx_xout' array values

for k = 0, nout-1 do begin

   xk  = xx_xout[k]

   cmd = 'xkk = ' + fintx + '(xk)'
   res = execute(cmd)

   ; interpolation test

   if  xk GE xx_xin[nin-1] then begin

       inter     = nin - 1
       interp[k] = 0
       agrl[0]   = 0.0
       agrl[2]   = 0.0
       if xk EQ xx_xin[nin-1] then begin
          interp[k] = 1
          agrl[1]   = 0.0
          agrl[3]   = 1.0
       endif else if iopt GE 0 then begin
          agrl[1] = xkk - x[nin-1]
          agrl[3] = 1.0
       endif else begin
          agrl[1] = 0.0
          agrl[3] = 0.0
       endelse

   endif else if xk LE xx_xin[0] then begin

       inter     = 1
       interp[k] = 0

       agrl[1]   = 0.0
       agrl[3]   = 0.0
       if xk EQ xx_xin[0] then begin
          interp[k] = 1
          agrl[0]   = 0.0
          agrl[2]   = 1.0
       endif else if iopt GE 0 then begin
          agrl[0]   = xkk-x[0]
          agrl[2]   = 1.0
       endif else begin
          agrl[0]   = 0.0
          agrl[2]   = 0.0
       endelse

   endif else begin

        ; interpolate

        for i = nin-1, 0, -1 do begin
           if xk LT xx_xin[i] then inter = i
        endfor

        interp[k] =  1
        dl1       =  ( x[inter] - xkk    ) / h[inter]
        dl2       =  1.0 - dl1
        dl3       =  h[inter] * dl1 * dl2
        agrl[0]   =  dl1 * dl3
        agrl[1]   = -dl2 * dl3
        agrl[2]   =  dl1 * dl1 * ( 1.0 + dl2 + dl2 )
        agrl[3]   =  1.0 - agrl[2]

   endelse

   ; evaluate 'xx_yout'-value for requested 'xx_xout'-value

   xx_yout[k] = ( agrl[0] * dy[inter-1] )  + ( agrl[1] * dy[inter] ) + $
             ( agrl[2] * xx_yin[inter-1] ) + ( agrl[3] * xx_yin[inter] )

endfor


; Return yout in correct form

if keyword_set(log) then yout = exp(xx_yout) else yout = xx_yout

if arg_present(extrap) then extrap = 2 + (not interp)

END
