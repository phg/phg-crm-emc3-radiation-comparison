;----------------------------------------------------------------------
;+
; PROJECT    :  ADAS
;
; NAME       :  xxsple2d
;
; PURPOSE    :  A 2-D version of xxsple. The end-point spline options
;               can be set for each dimension independently.
;
; ARGUMENTS  :  All output arguments will be defined appropriately.
;               Inputs will be converted to correct type, if possible,
;               internally without changing calling type.
;
;                xxsple, xin=xin, yin=yin, data_in=data_in, opt=opt, myfunct='test'  etc
;
;               NAME      I/O    TYPE    DETAILS
; REQUIRED   :  opt        I     int     type of spline - set table below
;               myfunct    I     str     name of function of the interpolating
;                                        x-coordinate transformation.
;                                        defaults to x->x.
;               xin()      I     dbl     first independent variable
;               yin()      I     dbl     second independent variable
;               data_in()  I     dbl     2D dependent variable
;               xout()     I     dbl     requested points for first dimension
;               yout()     I     dbl     requested points for second dimension
;               data_out() O     dbl     calculated values
;               extrap()   O     int     flag indicating extrapolation
;                                           1 : yes
;                                           0 : no
;               interp()   O     int     flag indicating extrapolation
;                                           1 : yes
;                                           0 : no
;
; OPTIONAL      log()      I             2 element vector requesting the
;                                        interpolation in log space.
;               opt()      I             2 element vector setting the
;                                        asymptotic behaviour.
;
;
; NOTES      :
;         --------------------------------------------------------------
;         | IOPT  | NOPT |  DY(1)  DDY(1)  |  DY(N)   DDY(N)  |EXTRAP'N|
;         |-------|------|-----------------|------------------|--------|
;         | < 0   |   0  |    -     0.0    |    -      0.0    |  NO    |
;         |   0   |   0  |    -     0.0    |    -      0.0    |  YES   |
;         |   1   |   1  |    -     0.0    |  -1.5      -     |  YES   |
;         |   2   |   2  |   0.0     -     |   1.0      -     |  YES   |
;         |   3   |   3  |  -0.5     -     |  -1.5      -     |  YES   |
;         |   4   |   4  |   0.0     -     |    -      0.0    |  YES   |
;         |   5   |   5  |  -4.5     -     |  -1.5      -     |  YES   |
;         |   6   |   6  |  +0.5     -     |    -      0.0    |  YES   |
;         |   7   |   7  |  -3.5     -     |    -      0.0    |  YES   |
;         --------------------------------------------------------------
;
;
;
; AUTHOR     :  Martin O'Mullane
;
; DATE       :  25-07-2017
;
;
; MODIFIED:
;       1.1     Martin O'Mullane
;               - First version.
;
;  VERSION:
;       1.1    25-07-2017
;-
;----------------------------------------------------------------------



PRO xxsple2d, opt      = opt,      $
              myfunct  = myfunct,  $
              xin      = xin,      $
              yin      = yin,      $
              data_in  = data_in,  $
              xout     = xout,     $
              yout     = yout,     $
              data_out = data_out, $
              extrap   = extrap,   $
              interp   = interp,   $
              log      = log


; Check the input

case n_elements(opt) of

   0 : begin
          iopt_x = 0
          iopt_y = 0
       end
   1 : begin
          message, 'Setting x and y OPT to be the same', /continue
          iopt_x = long(opt)
          iopt_y = long(opt)
       end
   2 : begin
          iopt_x = long(opt[0])
          iopt_y = long(opt[1])
       end

   else : message, 'OPT can only have zero, 1 or 2 elements'

endcase

case n_elements(log) of

   0 : begin
          ilog_x = 0
          ilog_y = 0
       end
   1 : begin
          message, 'Setting x and y log behaviour to be the same', /continue
          ilog_x = long(log)
          ilog_y = long(log)
       end
   2 : begin
          ilog_x = long(log[0])
          ilog_y = long(log[1])
       end

   else : message, 'log setting can only have zero, 1 or 2 elements'

endcase

if n_elements(myfunct) EQ 0 then fintx = 'xxsple_function' $
                            else fintx = myfunct


; make the requested xout and yout into a set of pairs

len_xin = n_elements(xin)
len_yin = n_elements(yin)

len_xout = n_elements(xout)
len_yout = n_elements(yout)
num      = len_xout*len_yout

x_out = reform(transpose(rebin(xout, len_xout, len_yout)), len_xout*len_yout)
y_out = reform(rebin(yout, len_yout, len_xout), len_xout*len_yout)


; apply log options

if ilog_x EQ 1 then begin
   xx_in  = alog(xin)
   xx_out = alog(x_out)
endif else begin
   xx_in  = xin
   xx_out = x_out
endelse

if ilog_y EQ 1 then begin
   yy_in  = alog(yin)
   yy_out = alog(y_out)
endif else begin
   yy_in  = yin
   yy_out = y_out
endelse

if ilog_x EQ 1 OR ilog_y EQ 1 then dd_in = alog(data_in) else dd_in = data_in


; two-way splining

ypass = dblarr(len_xout*len_yout, len_yin)

for iy = 0, len_yin-1 do begin

   xxsple, xin=xx_in, yin=reform(dd_in[*,iy]), xout=xx_out, yout=res, opt=iopt_x
   ypass[*,iy] = res

endfor

d_out = dblarr(num)
for ix = 0, num-1 do begin

   xxsple, xin=yy_in, yin=reform(ypass[ix,*]), xout=yy_out[ix], yout=res, opt=iopt_y

   d_out[ix] = res[0]

endfor

if ilog_x EQ 1 OR ilog_y EQ 1 then d_out = exp(d_out)


; convert set of pairs back to a 2D array

data_out = transpose(reform(d_out, len_yout, len_xout))

END
