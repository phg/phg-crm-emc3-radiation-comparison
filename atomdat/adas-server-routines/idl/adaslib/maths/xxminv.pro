;----------------------------------------------------------------------
;+
; PROJECT    :  ADAS
;
; NAME       :  xxminv
;
; PURPOSE    :  Matrix inversion with accompanying solution of linear
;               equations if requested.
;
; ARGUMENTS  :  All output arguments will be defined appropriately.
;               Inputs will be converted to correct type, if possible,
;               internally without changing calling type.
;
;                xxminv,
;
;               NAME      I/O    TYPE    DETAILS
; REQUIRED   :  a          I     double  n x n matrix to be inverted
;                          O             inverted matrix
;               b          I     double  n-length vector which will solve
;                                        a.x=b if /solve is specified
; OPTIONAL   :  dint        I    double  n x n matrix to be inverted
;
; KEYWORDS   :  solve            int     +/- 1 depending on whether the number
;                                        of row interchanges was even or odd,
;                                        respectively
;               help             int     displays this header.
;
; NOTES      :  Calls the fortran code. Specification of a(,) follows the
;               fortran convention.
;
;
;
; AUTHOR     :  Martin O'Mullane
;
; DATE       :  17-05-2007
;
;
; MODIFIED:
;       1.1     Martin O'Mullane
;               - First version.
;
; VERSION:
;       1.1    17-05-2007
;-
;----------------------------------------------------------------------

PRO xxminv, a=a, b=b, solve=solve, dint=dint

; If asked for help

if keyword_set(help) then begin
   doc_library, 'xxminv'
   return
endif


fortdir = getenv('ADASFORT')

a = double(a)
b = double(b)

if keyword_set(solve) then isolve = 1L else isolve = 0L

sz    = size(a, /dim)
ndmax = long(sz[0])
ndim  = ndmax

dint_in = 0.0D0

dummy = CALL_EXTERNAL(fortdir+'/xxminv_if.so','xxminv_if', $
                      isolve, ndmax, ndim, a, b, dint_in)

if arg_present(dint) then dint = dint_in

END
