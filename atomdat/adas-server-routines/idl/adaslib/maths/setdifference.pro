; Copyright (c) 1999 Strathclyde University .
; SCCS info: Module @(#)$Header: /home/adascvs/idl/adaslib/maths/setdifference.pro,v 1.1 2004/07/06 15:19:20 whitefor Exp $    Date $Date: 2004/07/06 15:19:20 $
;+
; PROJECT:
;       ADAS 
;
; NAME:
;	SETDIFFERENCE
;
; PURPOSE: The set function of difference.
;
; EXPLANATION:
; For example:
;
;   a = [2,4,6,8]
;   b = [6,1,3,2]
;
; SetDifference(a,b) = [ 4, 8]     == Elements in A but not in B
;
; NOTES:
;   This function is from RSI and was downloaded from
;        http://www.dfanning.com/tips/set_operations.html
;
;   I assume there are no restrictions on further dissemination
;
;
; CATEGORY:
;	Mathematical utility
;
; SOURCED:
;       Martin O'Mullane
;
; MODIFIED:
;	1.1	Martin O'Mullane
;		First release (in ADAS)
;
; VERSION:
;       1.1	16-08-99
;
;-
;-----------------------------------------------------------------------------

FUNCTION SetDifference, a, b  

   ; = a and (not b) = elements in A but not in B

mina = Min(a, Max=maxa)
minb = Min(b, Max=maxb)
IF (minb GT maxa) OR (maxb LT mina) THEN RETURN, a ;No intersection...
r = Where((Histogram(a, Min=mina, Max=maxa) NE 0) AND $
          (Histogram(b, Min=mina, Max=maxa) EQ 0), count)
IF count eq 0 THEN RETURN, -1 ELSE RETURN, r + mina

END
