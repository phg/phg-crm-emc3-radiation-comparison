;----------------------------------------------------------------------
;+
; PROJECT    :  ADAS
;
; NAME       :  read_adf11
;
; PURPOSE    :  Reads adf11 collisional radiative files from the IDL command
;               line. Called from IDL using the syntax
;               read_adf11, file=...,iz1=...,te=... etc
;
; ARGUMENTS  :  All output arguments will be defined appropriately.
;
;               NAME      I/O    TYPE   DETAILS
; REQUIRED   :  file       I     str    full name of ADAS adf11 file
;                               - or -
;               uid        I     str    username of adf11 location.
;                                       'adas' for central ADAS data.
;               year       I     int    year of adf11 data.
;               partial    I     int    =1 partial data
;                                       =0 standard (default)
;               filter     I     str    filter if appropriate.
;               iz0        I     int    atomic number
;
;               is1        I     int    ionisation stage or bundle of interest
;               iz1        I     int    same as is1 - if both given is1 is used.
;               class      I     str    type of data
;                                       'acd' : recombination
;                                       'scd' : ionisation
;                                       'ccd' : charge exchange recomb.
;                                       'prb' : recob. + bremss. power
;                                       'prc' : charge exchange power
;                                       'qcd' : metastable cross coupling
;                                       'xcd' : parent cross coupling
;                                       'plt' : total line power
;                                       'pls' : specific line power
;                                       'zcd' : effective charge
;                                       'ycd' : effective charge squared
;                                       'ecd' : effective ionisation potential
;               iprt, igrd I     int    partition identifiers - see table below
;               jgrd, jprt
;               ispp, ispb
;               jspp, jspb
;               index_1
;               index_2
;               te         I     real() temperatures requested
;               dens       I     real() densities requested
; OPTIONAL      data       O      -     adf11 data
;               fulldata   O     str    all of the data from xxdata_13.
;                                         iz0       = nuclear charge
;                                         class     = One of 'acd','scd','ccd','prb','prc',
;                                                            'qcd','xcd','plt','pls','zcd',
;                                                            'ycd','ecd'
;                                         iclass    = 1..12 corresponding to above list.
;                                         is1min    = minimum ion charge + 1
;                                                     (generalised to connection vector index)
;                                         is1max    = maximum ion charge + 1
;                                                     (note excludes the bare nucleus)
;                                                     (generalised to connection vector index
;                                                      and excludes last one which always remains
;                                                      the bare nucleus)
;                                         nptnl     = number of partition levels in block
;                                         nptn      = number of partitions in partition level
;                                                     1st dim: partition level
;                                         nptnc    = number of components in partition
;                                                     1st dim: partition level
;                                                     2nd dim: member partition in partition level
;                                         iptnla   = partition level label (0=resolved root,1=
;                                                                              unresolved root)
;                                                     1st dim: partition level index
;                                         iptna    = partition member label (labelling starts at 0)
;                                                     1st dim: partition level index
;                                                     2nd dim: member partition index in partition
;                                                     level
;                                         iptnca   = component label (labelling starts at 0)
;                                                     1st dim: partition level index
;                                                     2nd dim: member partition index in partition
;                                                     level
;                                                     3rd dim: component index of member partition
;                                         ncnct     = number of elements in connection vector
;                                         icnctv    = connection vector of number of partitions
;                                                     of each superstage in resolved case
;                                                     including the bare nucleus
;                                                     1st dim: connection vector index
;
;                                         iblmx     = number of (sstage, parent, base)
;                                                     blocks in isonuclear master file
;                                         ismax     = number of charge states
;                                                     in isonuclear master file
;                                                     (generalises to number of elements in
;                                                      connection vector)
;                                         dnr_elem  = donor element name (for ccd and prc)
;                                         dnr_ams   = donor element mass (for ccd and prc)
;                                         isppr     = 1st (parent) index for each partition block
;                                                     1st dim: index of (sstage, parent, base)
;                                                              block in isonuclear master file
;                                         ispbr     = 2nd (base) index for each partition block
;                                                     1st dim: index of (sstage, parent, base)
;                                                              block in isonuclear master file
;                                         isstgr    = s1 for each resolved data block
;                                                     (generalises to connection vector index)
;                                                     1st dim: index of (sstage, parent, base)
;                                                              block in isonuclear master file
;
;                                         idmax     = number of dens values in
;                                                     isonuclear master files
;                                         itmax     = number of temp values in
;                                                     isonuclear master files
;                                         ddens     = log10(electron density(cm-3)) from adf11
;                                         dtev      = log10(electron temperature (eV) from adf11
;                                         drcof     = if(iclass <=9):
;                                                        log10(coll.-rad. coefft.) from
;                                                        isonuclear master file
;                                                     if(iclass >=10):
;                                                        coll.-rad. coefft. from
;                                                        isonuclear master file
;                                                     1st dim: index of (sstage, parent, base)
;                                                              block in isonuclear master file
;                                                     2nd dim: electron temperature index
;                                                     3rd dim: electron density index

;                                         lres      = .true.  => partial file
;                                                   = .false. => not partial file
;                                         lstan     = .true.  => standard file
;                                                   = .false. => not standard file
;                                         lptn      = .true.  => partition block present
;                                                    = .false. => partition block not present;
;
; KEYWORDS      all               -     if specified data is 2D of
;                                       temperature and density.
;               kelvin            -     requested temperature in K (default eV)
;               skipzero          -     do not use effective zero in interpolating
;                                       to avoid ringing.
;               help              -     display help entry
;
;               Giving both a filename and a user/year/partial/filter set
;               is not allowed. These are two incompatible ways of
;               specifying a file. An error message will be given.
;
; NOTES      :  This is part of a chain of programs - read_adf11.c and
;               readadf11.for are required.
;
;               To specify the required partition the input names are
;               those in the datasets and both the old or new variants
;               can be used.
;
;                       file         old names         new names
;                       class
;
;                       (all)         z1                   s1
;
;                                (indices 1 and 2)     (indices 1 and 2)
;                       ----       ----     ----         ----     ----
;                       acd        iprt     igrd         ispp     ispb
;                       scd        iprt     igrd         ispp     ispb
;                       ccd        iprt     igrd         ispp     ispb
;                       prb        iprt                  ispp
;                       prc        iprt                  ispp
;                       qcd        igrd     jgrd         ispb     jspb
;                       xcd        iprt     jprt         ispp     jspp
;                       plt        igrd                  ispb
;                       pls        igrd                  ispb
;                       zcd        igrd                  ispb
;                       ycd        igrd                  ispb
;                       ecd        igrd                  ispb
;
;                In partitioned nomenclature: s=superstage; p=partition;
;                 b=base (current superstage), p=parent (next up super-
;                 stage), c=child (next down superstage). Thus arrays
;                 `iprtr' and `igrd' in old notation are now substituted
;                 by `isppr' and `ispbr' respectively internally and in
;                 external naming.
;
;               Alternatively index_1 and index_2 may be used and will
;               take on the meaning appropriate to the data class.
;
;
; AUTHOR     :  Martin O'Mullane
;
; DATE       :  23-07-1999
;
; MODIFIED:
;
;       1.2  Martin O'Mullane
;             - now works with partial files, qcd and xcd added.
;             - returns a 2D array if /all is specified.
;       1.3  Martin O'Mullane
;            - Problem in returning large standard classes fixed.
;              The fortran reset iprt=-1 to iprt=1 so for more
;              than 50 values it failed.
;            - Temperatures can be requested in K if /kelvin is specified.
;       1.4  Martin O'Mullane
;            - Make sure that iprt and igrd are passed to C as longs.
;       1.5  Allan Whiteford
;            - Added /help keyword
;       1.6  Allan Whiteford
;            - Increased maxval to 2000
;       1.7  Martin O'Mullane
;            - Add a fulldata return option.
;            - Move filename construction into a separate routine.
;       1.8  Martin O'Mullane
;            - Nuclear charge (iz0) no longer required if file name is passed.
;            - Use new version of xxdata_11 to enable access to partitioned,
;              standard and bundled adf11 datasets.
;            - Make partitioned index selection align with names in the
;              datasets. Allow both old and new nomenclature.
;            - Add a /nocheck keyword to bypass all consistency checks,
;              class must be given in this option
;       1.9  Martin O'Mullane
;            - Modify test of iz1/is1 to account for the inclusion of
;              including the fully stripped stage as an extra block in
;              zcd and ycd data and that there is a is1=0 block in ecd.
;       1.10 Martin O'Mullane
;            - The class is returned in fulldata but was omitted from
;              the comments.
;       1.11 Martin O'Mullane
;            - Add check of parents when accessing XCD datasets.
;       1.12 Martin O'Mullane
;            - Set an internal index_2=-1 for partial plt and prb.
;       1.13 Martin O'Mullane
;            - When interpolating data, pass the (optional) user supplied 
;              class to the xxdata_11 call, to fix those cases where it 
;              cannot determine the class from the file.
;
; VERSION:
;
;       1.1     23-07-1999
;       1.2     03-01-2001
;       1.3     30-03-2001
;       1.4     21-12-2001
;       1.5     07-04-2005
;       1.6     02-12-2005
;       1.7     09-03-2007
;       1.8     13-08-2008
;       1.9     25-02-2010
;       1.10    21-04-2010
;       1.11    11-11-2014
;       1.12    08-06-2018
;       1.13    10-10-2018
;-
;----------------------------------------------------------------------

PRO read_adf11_filename, uid      = uid,      $
                         year     = year,     $
                         filter   = filter,   $
                         iz0      = iz0,      $
                         class    = class,    $
                         partial  = partial,  $
                         filename = filename, $
                         nocheck  = nocheck

; atomic weight necessary

if n_elements(iz0) eq 0 then message, 'Atomic number must be given'
iz0 = LONG(iz0)

if n_elements(uid) eq 0 and n_elements(year) eq 0 then begin
    message,'A filename or year/uid/[filter] must be given'
endif
if n_elements(uid) eq 0 and n_elements(year) ne 0 then begin
    message,'A uid must be given'
endif
if n_elements(uid) ne 0 and n_elements(year) eq 0 then begin
    message,'A year must be given'
endif

year = LONG(year)

s_year = string(year, format='(i2.2)')

uid  = strcompress(uid, /remove_all)

; we need to construct the file name in order to determine class
; (alas they are different for partial and standard)

d_adas = strcompress(getenv("ADASCENT"),/remove_all)
d_home = strcompress(getenv("HOME"),/remove_all)
user   = getenv("LOGNAME")

if uid EQ 'adas' then diradas = d_adas + '/adf11/' $
                 else diradas = d_home + '/adf11/'

if n_elements(partial) GT 0 then p_ext = 'r' else p_ext = ''
if n_elements(filter) GT 0 then s_fil = filter else s_fil = ''

s_elem = xxesym(iz0)
s_elem = s_elem[0]
s_elem = strlowcase(s_elem)

file = diradas + class + s_year + p_ext + '/' + $
       class + s_year + p_ext + '_' + s_elem

if class EQ 'plt' OR class EQ 'prb' OR class EQ 'prc' then begin
     if s_fil NE '' then file = file + '.' + s_fil + '.dat' $
                    else file = file + '.dat'
endif else begin
     file = file + '.dat'
endelse

if arg_present(filename) then filename = file

; Check if it exists

if NOT keyword_set(nocheck) then begin

   file_acc, file, exist, read, write, execute, filetype
   if exist ne 1 then message, 'adf11 file does not exist '+file
   if read ne 1 then message, 'adf11 file cannot be read from this userid '+file

endif

END
;----------------------------------------------------------------------

PRO read_adf11, uid      = uid,         $
                year     = year,        $
                filter   = filter,      $
                file     = file,        $
                iz0      = iz0,         $
                iz1      = iz1,         $
                is1      = is1,         $
                class    = class,       $
                iprt     = iprt,        $
                igrd     = igrd,        $
                jgrd     = jgrd,        $
                jprt     = jprt,        $
                ispp     = ispp,        $
                ispb     = ispb,        $
                jspb     = jspb,        $
                jspp     = jspp,        $
                index_1  = index_1,     $
                index_2  = index_2,     $
                partial  = partial,     $
                te       = te,          $
                dens     = dens,        $
                data     = data,        $
                all      = all,         $
                nocheck  = nocheck,     $
                kelvin   = kelvin,      $
                fulldata = fulldata,    $
                skipzero = skipzero,    $
                help     = help

; If asked for help

if keyword_set(help) then begin
   doc_library, 'read_adf11'
   return
endif

; Options to pass to interpolation routine

if keyword_set(skipzero) then ignore_zero = 1L else ignore_zero = 0L

; Check that we get all inputs and that they are correct. Otherwise print
; a message and return to command line

on_error, 2

; If fulldata is required use xxdata_11 and return

if arg_present(fulldata) then begin

   if n_elements(file) EQ 0 then begin

      read_adf11_filename, uid      = uid,      $
                           year     = year,     $
                           filter   = filter,   $
                           iz0      = iz0,      $
                           class    = class,    $
                           partial  = partial,  $
                           filename = file_in

   endif else file_in = file

   if n_elements(class) NE 0 then xxdata_11, file=file_in, class=class, fulldata=fulldata $
                             else xxdata_11, file=file_in, fulldata=fulldata

   return

endif


;-----------------------------------
; Now to read/interpolate adf11 data
;
; Start with check of data
;-----------------------------------



if NOT keyword_set(nocheck) then begin

   ; if a year and uid are given then file should not be set. And vica-versa

   if n_elements(file) eq 0 then begin

         if n_elements(iz0) eq 0 then message, 'Nuclear charge must be given'
         iz0 = LONG(iz0)

         if n_elements(class) eq 0 then message, 'A data class must be selected'
         class = strlowcase(class)

         read_adf11_filename, uid      = uid,      $
                              year     = year,     $
                              filter   = filter,   $
                              iz0      = iz0,      $
                              class    = class,    $
                              partial  = partial,  $
                              filename = file

   endif else begin

   ; tell fortran to use filename and not to construct one

      if n_elements(uid) ne 0 or n_elements(year) ne 0 then begin
          message,'Specifing a filename and year/uid/[filter] are incompatible'
      endif

   endelse


   ; Get data from an xxdata_11 initial read

   if n_elements(class) NE 0 then xxdata_11, file=file, fulldata=all_11, class=class $
                             else xxdata_11, file=file, fulldata=all_11

   if all_11.iclass NE 0 then begin
      case all_11.iclass of
          1L : class_in = 'ACD'
          2L : class_in = 'SCD'
          3L : class_in = 'CCD'
          4L : class_in = 'PRB'
          5L : class_in = 'PRC'
          6L : class_in = 'QCD'
          7L : class_in = 'XCD'
          8L : class_in = 'PLT'
          9L : class_in = 'PLS'
         10L : class_in = 'ZCD'
         11L : class_in = 'YCD'
         12L : class_in = 'ECD'
      endcase
   endif

   if all_11.lres EQ 1 then is_partial = 1 else is_partial = 0

   iz0 = all_11.iz0

   ; Ionisation or bundle

   if n_elements(iz1) eq 0 AND n_elements(is1) EQ 0 then $
        message, 'Ionisation stage (iz1) or bundle (is1) must be given'

   if n_elements(iz1) eq 1 AND n_elements(is1) EQ 1 then begin
       if is1 NE iz1 then message, 'Using is1 rather than iz1', /continue
   endif

   if n_elements(iz1) eq 0 AND n_elements(is1) EQ 0 then $
        message, 'Ionisation stage (iz1) or bundle (is1) must be given'

   if n_elements(iz1) eq 1 AND n_elements(is1) EQ 0 then is1 = iz1


   is1_in = long(is1)

   is1_in = long(is1)
   case class_in of
     'ZCD' : if is1_in LT all_11.is1min OR is1_in GT all_11.is1max+1 then $
                message, 'Ionisation stage/bundle outside valid range in dataset'
     'YCD' : if is1_in LT all_11.is1min OR is1_in GT all_11.is1max+1 then $
                message, 'Ionisation stage/bundle outside valid range in dataset'
     'ECD' : if is1_in LT all_11.is1min-1 OR is1_in GT all_11.is1max then $
                message, 'Ionisation stage/bundle outside valid range in dataset'
      else  : if is1_in LT all_11.is1min OR is1_in GT all_11.is1max then $
         message, 'Ionisation stage/bundle outside valid range in dataset'
   endcase


   ; Check that the type of data requested is compatible

   if n_elements(class) EQ 0 then begin
      if all_11.iclass EQ 0 then message, 'A data class must be selected - cannot be determined from dataset'
   endif else begin
      if strupcase(class) NE class_in then $
         message, 'class requested and class in dataset differ'
   endelse



   ; Check for partial index consistency

   if keyword_set(partial) and is_partial EQ 0 then $
      message, 'Partial file selected but data is in standard form', /continue

   if is_partial EQ 1 then begin

       iprt_set = n_elements(iprt)
       igrd_set = n_elements(igrd)
       jgrd_set = n_elements(jgrd)
       jprt_set = n_elements(jprt)
       ispp_set = n_elements(ispp)
       ispb_set = n_elements(ispb)
       jspb_set = n_elements(jspb)
       jspp_set = n_elements(jspp)

       index_1_set = n_elements(index_1)
       index_2_set = n_elements(index_2)

       case class_in of
          'ACD' : test_class = 1
          'SCD' : test_class = 1
          'CCD' : test_class = 1
          'PRB' : test_class = 2
          'PRC' : test_class = 2
          'QCD' : test_class = 3
          'XCD' : test_class = 4
          'PLT' : test_class = 5
          'PLS' : test_class = 5
          'ZCD' : test_class = 5
          'YCD' : test_class = 5
          'ECD' : test_class = 5
       endcase

       case test_class of

          1 : begin

                 if index_1_set EQ 1 AND index_2_set EQ 1 then begin

                    message, 'Using index_1 and index_2 and not the named indices', /continue

                 endif else begin

                    if iprt_set EQ 1 AND igrd_set EQ 1 then begin
                       index_1 = iprt
                       index_2 = igrd
                    endif

                    if ispp_set AND ispb_set EQ 1 then begin
                       index_1 = ispp
                       index_2 = ispb
                    endif

                    if ((iprt_set + igrd_set) EQ 1) OR ((ispp_set + ispb_set) EQ 1) then $
                       message, 'Set both of the required partition identifiers'

                    if ((iprt_set + igrd_set) EQ 0) AND ((ispp_set + ispb_set) EQ 0) then $
                       message, 'Set the required partition identifiers'

                 endelse

              end

          2 : begin

                 if index_1_set EQ 1 then begin

                    message, 'Using index_1 and not the named index', /continue

                 endif else begin

                    if iprt_set EQ 1 then index_1 = iprt

                    if ispp_set then index_1 = ispp

                    if (iprt_set EQ 0) AND (ispp_set EQ 0) then $
                       message, 'Set the required partition identifiers'

                 endelse
                 
                 index_2 = -1

              end

          3 : begin

                 if index_1_set EQ 1 AND index_2_set EQ 1 then begin

                    message, 'Using index_1 and index_2 and not the named indices', /continue

                 endif else begin

                    if igrd_set EQ 1 AND jgrd_set EQ 1 then begin
                       index_1 = igrd
                       index_2 = jgrd
                    endif

                    if ispb_set AND jspb_set EQ 1 then begin
                       index_1 = ispb
                       index_2 = jspb
                    endif

                    if ((igrd_set + jgrd_set) EQ 1) OR ((ispb_set + jspb_set) EQ 1) then $
                       message, 'Set both of the required partition identifiers'

                    if ((igrd_set + jgrd_set) EQ 0) AND ((ispb_set + jspb_set) EQ 0) then $
                       message, 'Set the required partition identifiers'

                 endelse

              end

          4 : begin

                 if index_1_set EQ 1 AND index_2_set EQ 1 then begin

                    message, 'Using index_1 and index_2 and not the named indices', /continue

                 endif else begin

                    if iprt_set EQ 1 AND jprt_set EQ 1 then begin
                       index_1 = iprt
                       index_2 = jprt
                    endif

                    if ispp_set AND jspp_set EQ 1 then begin
                       index_1 = ispp
                       index_2 = jspp
                    endif

                    if ((iprt_set + jprt_set) EQ 1) OR ((ispp_set + jspp_set) EQ 1) then $
                       message, 'Set both of the required partition identifiers'

                    if ((iprt_set + jprt_set) EQ 0) AND ((ispp_set + jspp_set) EQ 0) then $
                       message, 'Set the required partition identifiers'

                 endelse

              end

          5 : begin

                 if index_1_set EQ 1 then begin

                    message, 'Using index_1 and not the named index', /continue

                 endif else begin

                    if igrd_set EQ 1 then index_1 = igrd

                    if ispb_set then index_1 = ispb

                    if (igrd_set EQ 0) AND (ispb_set EQ 0) then $
                       message, 'Set the required partition identifiers'

                 endelse

                 index_2 = -1
              
              end

       endcase


       ; check if valid combination

       indsel = -1

       for is = 0, all_11.iblmx-1 do begin

          isdat = all_11.isstgr[is]

          if test_class EQ 2 OR test_class EQ 5 then begin

             if isdat EQ is1 AND all_11.isppr[is] EQ index_1 then indsel = is

          endif else begin

             if isdat EQ is1 AND all_11.isppr[is] EQ index_1 AND all_11.ispbr[is] EQ index_2 then indsel = is

          endelse

       endfor

       if indsel EQ -1 then message, 'Not a valid stage and partition indentifier combination'

   endif else begin

      index_1 = -1L
      index_2 = -1L

   endelse


   ; temperature and density

   if n_elements(te) eq 0 then message, 'User requested temperatures are missing'
   if n_elements(dens) eq 0 then message, 'User requested densities are missing'

   partype=size(te, /type)
   if (partype lt 2) or (partype gt 5) then begin
      message,'Temperature must be numeric'
   endif  else te = DOUBLE(te)

   partype=size(dens, /type)
   if (partype lt 2) or (partype gt 5) then begin
      message,'density must be numeric'
   endif else dens = DOUBLE(dens)

endif else begin

   is1_in   = long(is1)
   class_in = strlowcase(class)

   if n_elements(file) eq 0 then begin

         read_adf11_filename, uid      = uid,      $
                              year     = year,     $
                              filter   = filter,   $
                              iz0      = iz0,      $
                              class    = class_in, $
                              partial  = partial,  $
                              filename = file, /nocheck

   endif

   te   = double(te)
   dens = double(dens)

endelse


; Set internal iclass number

case  STRUPCASE(class_in) of
 'ACD' : iclass = 1L
 'SCD' : iclass = 2L
 'CCD' : iclass = 3L
 'PRB' : iclass = 4L
 'PRC' : iclass = 5L
 'QCD' : iclass = 6L
 'XCD' : iclass = 7L
 'PLT' : iclass = 8L
 'PLS' : iclass = 9L
 'ZCD' : iclass = 10L
 'YCD' : iclass = 11L
 'ECD' : iclass = 12L
endcase

index_1 = long(index_1)
index_2 = long(index_2)


; Kelvin rather than eV

if keyword_set(kelvin) then tev = te/11605.0 else tev = te


; Do we want a 2D array (/all set) or 1D on (te, dens) vector

len_te   = n_elements(tev)
len_dens = n_elements(dens)

if keyword_set(all) then begin

   ; make a big 1D list

   t_in = dblarr(len_te*len_dens)
   d_in = dblarr(len_te*len_dens)

   k = 0
   for i = 0, len_te-1 do begin
      for j = 0, len_dens-1 do begin
         t_in[k] = tev[i]
         d_in[k] = dens[j]
         k = k + 1
       endfor
    endfor
    inval = k


endif else begin

   ; are te and dens the same length and one dimensional?
   ; if so define data array

   if len_dens ne len_te then print, 'TE and DENS size mismatch - smaller  used'
   inval = min([len_te,len_dens])

   t_in   = tev
   d_in   = dens

endelse

inval = LONG(inval)


fortdir = getenv('ADASFORT')

; Get data in blocks of MAXVAL to avoid altering xxsple for large sets.

data = 0.0D0

MAXVAL = 2000L


n_call = numlines(inval, MAXVAL)

for j = 0, n_call - 1 do begin

  ist = j*MAXVAL
  ifn = min([(j+1)*MAXVAL,inval])-1

  t_ca     = t_in[ist:ifn]
  d_ca     = d_in[ist:ifn]
  data_ca  = dblarr(ifn-ist+1)
  inval_ca = ifn-ist+1
  
  dummy = CALL_EXTERNAL(fortdir+'/read_adf11.so','read_adf11', $
                        file     ,                             $
                        is1_in   , iclass , index_1, index_2,  $
                        ignore_zero       ,                    $
                        inval_ca , t_ca   , d_ca   , data_ca   )

  data = [data, data_ca]

endfor
data = data[1:*]


if keyword_set(all) then begin

   d_out = dblarr(len_te, len_dens)
   k = 0
   for i = 0, len_te-1 do begin
      for j = 0, len_dens-1 do begin
         d_out[i,j] = data[k]
         k = k + 1
      endfor
   endfor

   data = d_out

endif


END
