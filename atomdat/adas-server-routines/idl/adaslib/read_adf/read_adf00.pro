;----------------------------------------------------------------------
;+
; PROJECT    :  ADAS
;
; NAME       :  read_adf00
;
; PURPOSE    :  Reads an adf00 element file from IDL
;               Called from IDL using the syntax
;                  read_adf00,file=..., z0=... etc
;
; ARGUMENTS  :  All output arguments will be defined appropriately.
;
;               NAME      I/O    TYPE    DETAILS
; REQUIRED   :  file       I     str     full name of ADAS adf00 file
;               dir        I     str     adf00 directory
;               z_nuc      I     int     atomic number (preferred)
;               z0         I     int     atomic number (deprecated)
;               z_ion      I     int     Ion stage(s) of interest. (preferred)
;               z1         I     int     Ion stage(s) of interest. (deprecated)
;                                        [Both z_ion and z1 keywords set is disallowed]
; OPTIONAL      ionpot     O     double  ion potential(s).
;               config     O     double  ground state configuration(s).
;               isa        O     int     spin
;               ila        O     int     angular momentumn
;               xja        O     int     statistical weight
;               nmeta      O     int     number of metastables
;               nmet_fl    O     int     number of metastable recorded in adf00
;               wf         O     double  work function for term (LS) data
;
; KEYWORDS   :  /all    - return all ion stages. Note z1 must be an
;                         argument in this case.
;
;               /cache  - use an internal memory cache to store
;                         results to save reading files for
;                         already known results (only for both
;                         z0 and a single z1 specified).
;              /ls      - if no filename is given these switches
;              /ic        read from the resolved data.
;
; NOTES      : Either file or dir and z0 or z0 alone are required. If
;              only z0 is given then central ADAS adf00 is queried.
;
;              The cacheing facility does not store the adf00 file
;              which was read so do not use this facility when
;              using different files.
;
; AUTHOR     :  Martin O'Mullane
;
; DATE       :  07-02-2001
;
;
; MODIFIED:
;       1.2     Allan Whiteford
;                 - Added /help keyword
;
;       1.3     Allan Whiteford
;                 - Added /cache keyword
;
;       1.4     Hugh Summers
;                 - Added preferred keywords z_ion for z1. Deprecate z1
;                   and fault if both z1 and z_ion set.
;
;       1.5     Adam Foster
;                 - Modified check for z_ion keyword to allow for neutral
;                   ions
;
;       1.6     Martin O'Mullane
;                 - Modifications to read from resolved datafiles and
;                   to return the outer quantum numbers of the latest
;                   adf00 specification.
;
; VERSION:
;       1.1    04-12-2002
;       1.2    07-04-2005
;       1.3    28-07-2008
;       1.4    22-08-2008
;       1.5    19-03-2009
;       1.6    18-09-2012
;
;-
;----------------------------------------------------------------------



PRO read_adf00, file    =  file,       $
                dir     =  dir,        $
                z0      =  z0,         $
                z1      =  z1,         $
                z_ion   =  z_ion,      $
                z_nuc   =  z_nuc,      $
                config  =  config,     $
                ionpot  =  ionpot,     $
                all     =  all,        $
                cache   =  cache,      $
                isa     =  isa,        $
                ila     =  ila,        $
                xja     =  xja,        $
                nmeta   =  nmeta,      $
                nmet_fl =  nmet_fl,    $
                wf      =  wf,         $
                ls      =  ls,         $
                ic      =  ic,         $
                help    =  help

;-----------------------------------
; check if z1 or z_ion keywords used
;-----------------------------------

if (n_elements(z1) and n_elements(z_ion)) then  message, $
  'adf00.pro: Error - both z1 and z_ion keywords set - use one '+$
  'the other (z_ion preferred).'
if ((not n_elements(z1)) and (n_elements(z_ion))) then z1=z_ion

;-----------------------------------
; check if z0 or z_nuc keywords used
;-----------------------------------

if keyword_set(z0) and keyword_set(z_nuc) then  message, $
  'adf00.pro: Error - both z0 and z_nuc keywords set - use one '+$
  'the other (z_nuc preferred).'
if not keyword_set(z0) and keyword_set(z_nuc) then z0=z_nuc



; If asked for help

if keyword_set(help) then begin
   doc_library, 'read_adf00'
   return
endif

; cache disabled

if keyword_set(cache) then message, 'cache temporarily disabled', /continue


; Construct the adf00 filename - if file or dir are not given
; then assume the data comes from central ADAS.

if n_elements(dir) GT 0 AND n_elements(z0) GT 0 then begin

   a00_file = dir + '/' + strlowcase(xxesym(z0)) + '.dat'
   if keyword_set(ls) then a00_file = dir + '/' + strlowcase(xxesym(z0)) + '_ls.dat'
   if keyword_set(ic) then a00_file = dir + '/' + strlowcase(xxesym(z0)) + '_ic.dat'

endif

if n_elements(dir) EQ 0 AND n_elements(z0) GT 0 then begin

   adascent = getenv("ADASCENT")
   a00_file = adascent + '/adf00/' + strlowcase(xxesym(z0)) + '.dat'
   if keyword_set(ls) then a00_file = adascent + '/adf00/' + strlowcase(xxesym(z0)) + '_ls.dat'
   if keyword_set(ic) then a00_file = adascent + '/adf00/' + strlowcase(xxesym(z0)) + '_ic.dat'

endif

if n_elements(file) EQ 1 then begin

   if n_elements(z0) GT 0 then print, 'Ignoring z0 as file given'
   if n_elements(dir) GT 0 then print, 'Ignoring dir as file given'

   a00_file = file

endif

a00_file = strcompress(a00_file, /remove_all)


if n_elements(dir) EQ 0 AND n_elements(z0) EQ 0 AND $
   n_elements(file) EQ 0 then message, 'A file, dir or z0 is required'



; Check if file exists

file_acc, a00_file, exist, read, write, execute, filetype
if exist NE 1 then message, 'adf00 file does not exist '+ a00_file
if read NE 1 then message, 'adf00 file cannot be read from this userid '+ a00_file


; Read the adf00 file

adas_readfile, file=a00_file, all=all_00, nlines=nlines

str   = ''
idum  = 0
fdum  = 0.0D0

xxelem, nstg, strmid(all_00[0], 0, 15), /return_z

; Type of file

i1 = strpos(all_00[0], 'wf=')
if i1 EQ -1 then begin
   is_standard = 1 
   wf = 0.0
endif else begin
   is_standard = 0
   reads, strmid(all_00[0], i1+3), wf
endelse

if is_standard EQ 1 then begin

   stage = intarr(nstg)
   ip    = dblarr(nstg)
   cfg   = strarr(nstg)
   isa   = intarr(nstg) - 1
   ila   = intarr(nstg) - 1
   xja   = dblarr(nstg) - 1

   for j = 0, nstg-1 do begin

     reads, all_00[j+1], idum, fdum, format = '(i2,2x,f14.8)'
     stage[j] = idum
     ip[j]    = fdum

     i1 = strpos(all_00[j+1], '(')

     reads, strmid(all_00[j+1], i1), id0, id1, fd2, format='(1x,i2,1x,i1,1x,f5.1)'
     
     if i1 NE -1 then begin
        cfg[j] = strmid(all_00[j+1], 18, i1-18)
        isa[j] = id0
        ila[j] = id1
        xja[j] = fd2
     endif else begin
        cfg[j] = strmid(all_00[j+1], 18)  ; no quantum numbers
     endelse 

   endfor

   nmeta = intarr(nstg) + 1

endif else begin

   i1 = strpos(all_00[0], strcompress(string(nstg), /remove_all))
   i2 = strpos(all_00[0], 'wf=')

   num_sub = intarr(nstg)

   nl = numlines(nstg, 18)             ; metastable list in blocks of 18
   reads, strmid(all_00[0:nl-1], i1+2, i2-i1-3), num_sub

   n_term = max(num_sub) + 1

   stage   = intarr(nstg)
   nmet_fl = intarr(nstg)
   ip      = dblarr(nstg, n_term)
   cfg     = strarr(nstg, n_term)
   isa     = intarr(nstg, n_term)
   ila     = intarr(nstg, n_term)
   xja     = dblarr(nstg, n_term)
   
   k = nl
   
   for j = 0, nstg-1 do begin

      for ist = 0, num_sub[j] do begin   ; add 1 for standard IP at first entry

         idum = -1
         imt  = -1
         fdum = 0.0

         if ist EQ 0 then begin
            reads, all_00[k], idum, fdum, format = '(i2, 5x,f14.8)'
            stage[j] = idum
         endif else begin
            reads, all_00[k], imt, fdum, format = '(3x,i2,2x,f14.8)'
         endelse

         if imt NE 0 then begin
            
            ip[j,ist]  = fdum
            
            i1         = strpos(all_00[k], '(')
            cfg[j,ist] = strmid(all_00[k], 18, i1-18) ; assume ls/ic variants have quantum numbers

            reads, strmid(all_00[k], i1), id0, id1, fd2, format='(1x,i2,1x,i1,1x,f5.1)'

            isa[j,ist] = id0
            ila[j,ist] = id1
            xja[j,ist] = fd2

            nmet_fl[j] = nmet_fl[j] + 1

            k = k + 1
            
         endif

      endfor

   endfor

   nmeta   = num_sub
   nmet_fl = nmet_fl - 1    ; drop the standard IP at start of block 

endelse

; If z1 not present return all stages

if keyword_set(all) then req = indgen(n_elements(stage)) else req = fix(z1)

; Set and return

ind   = -1
for j = 0, n_elements(req)-1 do ind = [ind, where(stage EQ req[j])]
indx = where(ind NE -1, count)

if count EQ 0 then message, 'No valid ion stages' else ind = ind[indx]

if keyword_set(all) then begin
   if arg_present(z1) EQ 0 and $
      arg_present(z_ion) EQ 0 then message,'/all requires z_ion defined'
   z1 = req
   z_ion=req
endif

if n_elements(file) EQ 1 AND arg_present(z_nuc) then z_nuc=nstg
if n_elements(file) EQ 1 AND arg_present(z0) then z0=nstg


if is_standard EQ 1 then begin

   ionpot  = ip[ind]
   config  = cfg[ind]
   nmeta   = nmeta[ind]
   nmet_fl = nmeta

endif else begin

   ionpot = reform(ip[ind, *])
   config = reform(cfg[ind, *])
   isa    = reform(isa[ind, *])
   ila    = reform(ila[ind, *])
   xja    = reform(xja[ind, *])
   nmeta  = reform(nmeta[ind])
   nmet_fl= reform(nmet_fl[ind])

endelse

END
