;----------------------------------------------------------------------
;+
; PROJECT    :  ADAS
;
; NAME       :  xxdata_15
;
; PURPOSE    :
;
; ARGUMENTS  :  All output arguments will be defined appropriately.
;               Inputs will be converted to correct type, if possible,
;               internally without changing calling type.
;
;                xxdata_15
;
;               NAME       I/O    TYPE    DETAILS
; REQUIRED   :  file        I     str     adf11 file name
; OPTIONAL   :  fulldata    O     -       Output structure (see below)
;               help        I     switch  Help required
;
;
; The fulldata structure is defined as:
;
;     a15file  = adf15 filename
;     a04file  = adf04 file name used to calculate adf15 data if available
;     a18file  = adf18 file name used to calculate adf15 data if projection
;                was used.
;     esym     = emitting ion - element symbol
;     iz0      = emitting ion - nuclear charge
;     is       = emitting ion - charge
;                (generalised to superstage label)
;     is1      = emitting ion - charge + 1
;                (generalised to superstage index= is + 1)
;     nptn()   = number of partitions in partition level
;                1st dim: partition level
;     nptnc(,) = number of components in partition
;                1st dim: partition level
;                2nd dim: member partition in partition level
;     iptnla() = partition level label (0=resolved root,1=unresolved root)
;                1st dim: partition level index
;     iptna(,) = partition member label (labelling starts at 0)
;                1st dim: partition level index
;                2nd dim: member partition index in partition
;                level
;     iptnca(,,)= component label (labelling starts at 0)
;                1st dim: partition level index
;                2nd dim: member partition index in partition level
;                3rd dim: component index of member partition
;     icnctv()  = connection vector of number of partitions
;                 of each superstage in resolved case
;                 including the bare nucleus
;                 1st dim: connection vector index
;     cptn_stack()= text lines in partition block
;                   1st dim: text line index
;     lres      = 1  => partial file
;               = 0 => not partial file
;     lptn      = 1  => partition block present
;               = 0 => partition block not present
;     lsup      = 1  => ss use of filmem field
;               = 0 => old use of filmem field
;     cwavel() = wavelength string (angstroms)
;                1st dim: data-block index
;     cfile()  = specific ion file source string in older
;                forms.  Field not present in superstage
;                version, but reused for added information
;                1st dim: data-block index
;     ctype()  = data type string
;                1st dim: data-block index
;     cindm()  = metastable index string
;                1st dim: data-block index
;     wavel()   = wavelength (angstroms)
;                 dimension: data-block index
;     isppr()   = parent index for each line block
;                 1st dim: index of block in adf15 file
;     ispbr()   = base index for each line block
;                 1st dim: index of block in adf15 file
;     isstgr()  = s1 for each resolved data block
;                 1st dim: index of block in adf15 file
;     iszr()    = ion charge relating to each line
;                 1st dim: index of block in adf15 file
;     nte()     = number of electron temperatures
;                 dimension: data-block index
;     ndens()     = read - number of electron densities
;                 1st dim: data-block index
;     te(,)     =  electron temperatures (units: ev)
;                 1st dim: electron temperature index
;                 2nd dim: data-block index
;     dens(,)   = electron densities (units: cm-3)
;                 1st dim: electron density index
;                 2nd dim: data-block index
;     pec(,,)   = photon emissivity coeffts
;                 1st dim: electron temperature index
;                 2nd dim: electron density index
;                 3rd dim: data-block index
;     pec_max() = photon emissivity coefft. maximum
;                 as a function of Te at first Ne value
;                 1st dim: data-block index
;     tran_index = transition index within adf04 file
;     power_rank = the rank by its contribution to 
;                  the total power
;
; NOTES      :  Calls fortran code.
;
;
; AUTHOR     :  Martin O'Mullane
;
; DATE       :  12-04-2005
;
;
; MODIFIED:
;       1.1     Martin O'Mullane
;               - First version.
;       1.2     Martin O'Mullane
;               - Re-write for new version of xxdata_15.
;       1.3     Martin O'Mullane
;               - Dimension error in teda.
;       1.4     Martin O'Mullane
;               - Parse the transition index and power ranking from
;                 comments if the adf15 is new style.
;       1.5     Martin O'Mullane
;               - Parse the comments to extract the adf04 file used.
;       1.6     Martin O'Mullane
;               - Check for a connection vector before trimming the
;                 fulldata entries and set appropriate values
;                 if it is not present.
;               - Extend the permitted length of adf15 file name 
;                 to 132 characters.
;       1.7     Martin O'Mullane
;               - iptnca was trimmed incorrectly.
;       1.8     Martin O'Mullane
;               - Add adf18 expansion file to output structure.
;
;
; VERSION:
;       1.1    12-04-2005
;       1.2    30-08-2007
;       1.3    20-08-2008
;       1.4    09-04-2013
;       1.5    12-06-2014
;       1.6    04-05-2018
;       1.7    22-08-2018
;       1.8    26-09-2018
;
;-
;----------------------------------------------------------------------

PRO xxdata_15, file     = file,     $
               fulldata = fulldata, $
               help     = help


; Return help if requested

if keyword_set(help) then begin
    doc_library, 'xxdata_15'
    return
endif


; Check that the adf15 file exists and convert to byte array

file_acc, file, exist, read, write, execute, filetype
if exist ne 1 then message, 'adf15 file does not exist ' + file
if read ne 1 then message, 'adf15 file cannot be read from this userid ' + file

dsname = file
adas_string_pad, dsname, 132
dsnin = byte(dsname)


; Parameters to match fortran

nstore  = 500L
ntdim   = 48L
nddim   = 30L
ndptnl  = 4L
ndptn   = 128L
ndptnc  = 256L
ndcnct  = 100L
ndstack = 40L

; Other variables

iunit       = 67L
iz0         = 0L
is          = 0L
is1         = 0L
esym        = bytarr(2)
nptnl       = 0L
nptn        = lonarr(ndptnl)
nptnc       = lonarr(ndptnl, ndptn)
iptnla      = lonarr(ndptnl)
iptna       = lonarr(ndptnl, ndptn)
iptnca      = lonarr(ndptnl, ndptn, ndptnc)
ncnct       = 0L
icnctv      = lonarr(ndcnct)
ncptn_stack = 0L
cptn_stack  = bytarr(80, ndstack)
ilres       = 0L
ilptn       = 0L
ilsup       = 0L
nbsel       = 0L
isela       = lonarr(nstore)
cwavel      = bytarr(10, nstore)
cfile       = bytarr(8, nstore)
ctype       = bytarr(8, nstore)
cindm       = bytarr(2, nstore)
wavel       = dblarr(nstore)
ispbr       = lonarr(nstore)
isppr       = lonarr(nstore)
isstgr      = lonarr(nstore)
iszr        = lonarr(nstore)
ita         = lonarr(nstore)
ida         = lonarr(nstore)
teta        = dblarr(ntdim, nstore)
teda        = dblarr(nddim, nstore)
pec         = dblarr(ntdim, nddim, nstore)
pec_max     = dblarr(nstore)


; Location of sources

fortdir = getenv('ADASFORT')

dummy = CALL_EXTERNAL(fortdir+'/xxdata_15_if.so','xxdata_15_if',  $
                      iunit    , dsnin   ,                        $
                      nstore   , ntdim   , nddim  ,               $
                      ndptnl   , ndptn   , ndptnc , ndcnct ,      $
                      ndstack  ,                                  $
                      iz0      , is      , is1    , esym   ,      $
                      nptnl    , nptn    , nptnc  ,               $
                      iptnla   , iptna   , iptnca ,               $
                      ncnct    , icnctv  ,                        $
                      ncptn_stack        , cptn_stack      ,      $
                      ilres    , ilptn   , ilsup           ,      $
                      nbsel    , isela   ,                        $
                      cwavel   , cfile   , ctype  , cindm  ,      $
                      wavel    , ispbr   , isppr  , isstgr ,      $
                      iszr     ,                                  $
                      ita      , ida     ,                        $
                      teta     , teda    ,                        $
                      pec      , pec_max )



; Convert back to strings

cptn_stack = string(cptn_stack)
cfile      = string(cfile)
ctype      = string(ctype)
cindm      = string(cindm)
cwavel     = string(cwavel)
esym       = string(esym)


; Chop arrays back to valid data

isela      =  isela[0:nbsel-1]
cwavel     =  cwavel[0:nbsel-1]
cfile      =  cfile[0:nbsel-1]
ctype      =  ctype[0:nbsel-1]
cindm      =  cindm[0:nbsel-1]
ispbr      =  ispbr[0:nbsel-1]
isppr      =  isppr[0:nbsel-1]
isstgr     =  isstgr[0:nbsel-1]
iszr       =  iszr[0:nbsel-1]
ita        =  ita[0:nbsel-1]
ida        =  ida[0:nbsel-1]
teda       =  teda[0:max(ida)-1, 0:nbsel-1]
teta       =  teta[0:max(ita)-1, 0:nbsel-1]
wavel      =  wavel[0:nbsel-1]
pec        =  pec[0:max(ita)-1, 0:max(ida)-1, 0:nbsel-1]
pec_max    =  pec_max[0:nbsel-1]

; Check for absence of a connection vector

if ncnct GT 0 then begin

   icnctv     =  icnctv[0:ncnct-1]
   cptn_stack =  cptn_stack[0:ncptn_stack-1]
   nptn       =  nptn[0:nptnl-1]
   iptna      =  iptna[0:nptnl-1, 0:max(nptn)-1]
   nptnc      =  nptnc[0:nptnl-1, 0:max(nptn)-1]
   iptnca     =  iptnca[0:nptnl-1, 0:max(nptn)-1, 0:max(nptnc)-1]
   iptnla     =  iptnla[0:nptnl-1]

endif else begin

   icnctv     =  -1
   cptn_stack = 'Not available'
   nptn       =  0
   iptna      =  -1
   nptnc      =  0
   iptnca     =  -1
   iptnla     =  -1

endelse

; Extract the transition index and power ranking from comments
; if the adf15 is the new style

adas_readfile, file=file, all=all

ind = where(strpos(all, 'ispp nspp sz    tg pr wr') NE -1, count)

if count GT 0 then begin

   ind = ind[0]
   i1  = strpos(all[ind], 'tg')

   tran_index = fix(strmid(all[ind+2:ind+2+nbsel-1], i1-2,4))
   power_rank = fix(strmid(all[ind+2:ind+2+nbsel-1], i1+2,3))

endif else begin

   tran_index = -1
   power_rank = -1

endelse


; Extract adf04 and adf18 driver files

a04file = 'unknown'

ind = where(strpos(strupcase(all), 'SPECIFIC ION FILE  :') NE -1, count)
if count EQ 1 then begin
    str = all[ind[0]]
    i1  = strpos(str, ':')
    a04file = strtrim(strmid(str, i1+1),2)
endif

ind = where(strpos(strlowcase(all), 'adf04 source file    :') NE -1, count)
if count EQ 1 then begin
    str = all[ind[0]]
    i1  = strpos(str, ':')
    a04file = strtrim(strmid(str, i1+1),2)
endif

a18file = 'unknown'

ind = where(strpos(strupcase(all), 'EXPANSION FILE     :') NE -1, count)
if count EQ 1 then begin
    str = all[ind[0]]
    i1  = strpos(str, ':')
    a18file = strtrim(strmid(str, i1+1),2)
endif

ind = where(strpos(strlowcase(all), 'adf18/a17_p208 file  :') NE -1, count)
if count EQ 1 then begin
    str = all[ind[0]]
    i1  = strpos(str, ':')
    a18file = strtrim(strmid(str, i1+1),2)
endif




; Return as a fulldata structure

fulldata = {  a15file    : file,        $
              a04file    : a04file,     $
              a18file    : a18file,     $
              esym       : esym,        $
              iz0        : iz0,         $
              is         : is ,         $
              is1        : is1,         $
              nptn       : nptn,        $
              iptna      : iptna,       $
              nptnc      : nptnc,       $
              iptnca     : iptnca,      $
              iptnla     : iptnla,      $
              icnctv     : icnctv,      $
              ndens      : ida,         $
              nte        : ita,         $
              dens       : teda,        $
              te         : teta,        $
              ctype      : ctype,       $
              cindm      : cindm,       $
              cwavel     : cwavel,      $
              cfile      : cfile,       $
              cptn       : cptn_stack,  $
              wave       : wavel,       $
              isppr      : isppr,       $
              ispbr      : ispbr,       $
              isstgr     : isstgr,      $
              iszr       : iszr,        $
              lres       : ilres,       $
              lptn       : ilptn,       $
              lsup       : ilsup,       $
              pec        : pec,         $
              pec_max    : pec_max,     $
              tran_index : tran_index,  $
              power_rank : power_rank   }

END
