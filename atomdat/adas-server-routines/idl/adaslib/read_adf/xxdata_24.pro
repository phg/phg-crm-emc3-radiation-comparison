;----------------------------------------------------------------------
;+
; PROJECT    :  ADAS
;
; NAME       :  xxdata_24
;
; PURPOSE    :
;
; ARGUMENTS  :  All output arguments will be defined appropriately.
;               Inputs will be converted to correct type, if possible,
;               internally without changing calling type.
;
;                xxdata_24
;
;               NAME      I/O    TYPE    DETAILS
; REQUIRED   :  a24file     I     char    adf24 filename
;               nstore      I     long    maximum number of blocks
;               nedim       I     long    maximum number of energies
;               iz          O     long    recombined ion charge
;               iz0         O     long    nuclear charge
;               iz1         O     long    recombining ion charge
;               esym        O     char    element symbol
;               nbsel       O     long    number of blocks
;               isela()     O     long    data-set data-block entry indices
;                                           dimension : blocks
;               cdonor()    O     char    donor name
;                                           dimension : blocks
;               crecvr()    O     char    receiver name
;                                           dimension : blocks
;               cfstat()    O     char    final state
;                                           dimension : blocks
;               ctype()     O     char    type of cross section
;                                           dimension : blocks
;               alph0()      O    double  low energy extrapolation paramater
;                                           dimension : blocks
;               iea()       O     long    number of energies
;                                           dimension : blocks
;               teea()      O     double  energies
;                                           dimension : blocks
;               scx         O     double  densities
;                                           dimension : energy
;                                           dimension : blocks
;
;
; NOTES      :  Calls fortran code.
;
;
; AUTHOR     :  Martin O'Mullane
;
; DATE       :  27/03/06
;
;
; MODIFIED:
;       1.1     Martin O'Mullane
;              	- First version.
;
;
; VERSION:
;       1.1    27/03/06
;
;-
;----------------------------------------------------------------------

PRO xxdata_24, a24file,                              $
               nstore  , nedim  ,                    $
               iz0     , iz     , iz1    , esym  ,   $
               nbsel   , isela  ,                    $
               cdonor  , crecvr , cfstat , ctype ,   $
               alph0   , iea    , teea   , scx


; Check number of parameters

if n_params() NE 17 then message, 'Not all parameters present', /continue


; Check of mandatory inputs and convert to LONG type.

if nedim  EQ 0 then message, 'Set maximum number of energies'
if nstore EQ 0 then message, 'Set maximum number of blocks'

nedim  = long(nedim)
nstore = long(nstore)


; Check that the adf24 file exists

file_acc, a24file, exist, read, write, execute, filetype
if exist ne 1 then message, 'adf24 file does not exist '+a24file
if read ne 1 then message, 'adf24 file cannot be read from this userid '+a24file


; Define types of the output parameters

iunit     = 67L
esym      = '  '
iz        = -1L
iz0       = -1L
iz1       = -1L
nbsel     = -1L
isela     = lonarr(nstore)
iea       = lonarr(nstore)
alph0     = dblarr(nstore)
teea      = dblarr(nedim, nstore)
scx       = dblarr(nedim, nstore)
cdonor    = strarr(nstore) + '         '
crecvr    = strarr(nstore) + '         '
cfstat    = strarr(nstore) + '          '
ctype     = strarr(nstore) + '  '


file      = string(replicate(32B, 80))
strput, file, a24file, 0


; Combine string arrays into a byte array

str_vars = [cdonor, crecvr, cfstat, ctype]
str_vars = byte(str_vars)
str_vars = long(str_vars)


; Location of sources

fortdir = getenv('ADASFORT')

dummy = CALL_EXTERNAL(fortdir+'/xxdata_24_if.so','xxdata_24_if', $
                      iunit, nstore, nedim, iz0, nbsel,          $
                      isela, iz, iz1, alph0, iea,                $
                      teea, scx, str_vars, file, esym)

; Convert mangled string arrays  back to calling parameter names

res = byte(str_vars)
res = string(res)

cdonor = res[0:nstore-1]
crecvr = res[nstore:2*nstore-1]
cfstat = res[2*nstore:3*nstore-1]
ctype  = res[3*nstore:4*nstore-1]

END
