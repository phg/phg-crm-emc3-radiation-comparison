;----------------------------------------------------------------------
;+
; PROJECT    :  ADAS
;
; NAME       :  xxdata_06
;
; PURPOSE    :
;
; ARGUMENTS  :  All output arguments will be defined appropriately.
;               Inputs will be converted to correct type, if possible,
;               internally without changing calling type.
;
;               xxdata_06, file='example.dat', fulldata=fulldata [, itieactn=1]
;
;
;
;              NAME              TYPE    DETAILS
; REQUIRED   : file              str     adf06 file name
;
; OPTIONAL   : 
;
; OUTPUT     : fulldata          structure
;                filename            :  adf06 filename
;                iz                  :  recombined ion charge
;                iz0                 :  nuclear charge
;                iz1                 :  recombining ion charge
;                ia()                :  energy level index number
;                                            dimension - level index
;                cstrga()            :  nomenclature/configuration for level 'ia()'
;                isa()               :  multiplicity for level 'ia()'
;                                       note: (isa-1)/2 = quantum number (s)
;                                            dimension - level index
;                ila()               :  quantum number (l) for level 'ia()'
;                                            dimension - level index
;                xja()               :  quantum number (j-value) for level 'ia()'
;                                       note: (2*xja)+1 = statistical weight
;                                            dimension - level index
;                wa()                :  energy relative to level 1 (cm-1) for level'ia()'
;                                            dimension - level index
;                zpla(,)             :  eff. zeta param. for contributions to ionis. of level
;                                           1st dimension - parent index
;                                           2nd dimension - level index
;                ipla(,)             :  parent index for contributions to ionis. of level
;                                            1st dimension - parent index
;                                            2nd dimension - level index
;                bwno                :  ionisation potential (cm-1) of lowest parent
;                bwnoa()             :  ionisation potential (cm-1) of parents
;                                            dimension - metastable index
;                cprta               :
;                qdorb()             :  quantum defects for orbitals
;                lqdorb()                 1  => source data available for qd.
;                                         0 => source data not availabe qd.=0.0
;                qdn()               :  quantum defect for n-shells.  non-zero only
;                                       for adf06 files with orbital energy data
;                iorb                :  number of orbital energies
;                te()                :  input data file: electron temperatures (K) type 3
;                                                        X = e/delta_E for type 1
;                                            dimension - temperature
;                lower()             :  upper index for transition
;                                            dimension - transition
;                upper()             :  lower index for transition
;                                            dimension - transition
;                aval()              :  E2 quadrupole A-value
;                                            dimension - transition
;                gamma(,,)           :  gamma values
;                                           effective collision strength for type 3
;                                           collision strength for type 1
;                                           scaled rate coefft.(cm3 sec-1)(case 'S')
;                                           1st dimension - transition number
;                                           2nd dimension - temperature 'scef()'
;                                           3rd dimension - ion projectile case
;                lbeth               :  1 => Bethe data on transition lines
;                                       0 => not present in file
;                beth(,)             :  Born limit
;                                            1st dimension - transition
;                                            2nd dimension - ion projectile case
;                tcode()             : data type
;                                            ' ' => ion impact transition
;                                            's','S' => Ionisation from current ion
;                                            dimension - transition
;                projectile()        : name of projectile, eg 7^Li
;                                            dimension - projecties
;                mass_target()       : mass of target
;                                            dimension - projecties
;                charge_projectile() : charge of projectile
;                                            dimension - projecties
;                mass_projectile()   : mass of projectile
;                                            dimension - projecties
;                level_ion()         : Ionisation from level
;                                            set to -1 if no data present 
;                                            dimension - ionisation transitions
;                parent_ion()        : Ionisation to parent
;                                            set to -1 if no data present 
;                                            dimension - ionisation transitions
;                ion(,,)             :  ion values
;                                           effective collision strength for type 3
;                                           collision strength for type 1
;                                           scaled, mass-independent, rate coefft.(cm3 sec-1)(case 'S')
;                                           1st dimension - transition number
;                                           2nd dimension - temperature 'scef()'
;                                           3rd dimension - ion projectile case
;                ltied()             :  1 => specified level tied
;                                       0 => specified level is untied
;                iadftyp             : as input
;
;
;
; NOTES      :  Calls fortran code, xxdata_06.for.
;
;
; AUTHOR     :  Martin O'Mullane
; DATE       :  27-10-2016
;
;
; MODIFIED:
;       1.1     Martin O'Mullane
;               - First version.
;
; VERSION:
;       1.1    27-10-2016
;
;-
;----------------------------------------------------------------------

PRO xxdata_06, file=file, fulldata=fulldata, itieactn=itieactn


; Parameters for storage

ndlev = 100L
ndtrn = 500L
ndmet = 10L
ndqdn = 8L
ndpro = 20L
nvmax = 50L

if n_elements(itieactn) EQ 0 then itieactn = 0L else itieactn = long(itieactn)


; Check that the adf06 file exists

file_acc, file, exist, read, write, execute, filetype
if exist ne 1 then message, 'adf06 file does not exist ' + file
if read ne 1 then message, 'adf06 file cannot be read from this userid ' + file


; Define types of the output parameters

iz        = -1L
iz0       = -1L
iz1       = -1L
bwno      = -1.0D0
npl       = -1L
bwnoa     = dblarr(ndmet)
il_lbseta = lonarr(ndmet)
prtwta    = dblarr(ndmet)
il        = -1L
qdorb     = dblarr((ndqdn*(ndqdn+1))/2)
il_lqdorb = lonarr((ndqdn*(ndqdn+1))/2)
qdn       = dblarr(ndqdn)
iorb      = -1L
ia        = lonarr(ndlev)
isa       = lonarr(ndlev)
ila       = lonarr(ndlev)
xja       = dblarr(ndlev)
wa        = dblarr(ndlev)
npla      = lonarr(ndlev)
ipla      = lonarr(ndmet,ndlev)
zpla      = dblarr(ndmet,ndlev)
nv        = -1L
scef      = dblarr(nvmax)
itran     = -1L
maxlev    = 0L
i1a       = lonarr(ndtrn)
i2a       = lonarr(ndtrn)
aval      = dblarr(ndtrn)
scom      = dblarr(nvmax,ndtrn,ndpro)
np        = -1L
proj      = dblarr(3, ndpro)
beth      = dblarr(ndtrn,ndpro)
iadftyp   = 0L
il_lprn   = 0L
il_lcpl   = 0L
il_lorb   = 0L   
il_lbeth  = 0L
il_lityp  = 0L
il_lstyp  = 0L
il_ltied  = lonarr(ndlev)
cpla      = strarr(ndlev) + ' '
cprta     = strarr(ndmet) + '         '
cstrga    = strarr(ndlev) + string(replicate(32B, 90))
cpro      = strarr(ndpro) + '     '
tcode     = strarr(ndtrn) + ' '
titled    = '   '

file_in   = string(replicate(32B, 132))
strput, file_in, file, 0


; Combine string arrays into a byte array

str_vars = [cpla, cprta, cstrga, cpro, tcode]
str_vars = byte(str_vars)
str_vars = long(str_vars)


; Location of sources

fortdir = getenv('ADASFORT')

dummy = CALL_EXTERNAL(fortdir+'/xxdata_06_if.so','xxdata_06_if',        $
                      ndlev, ndtrn, ndmet, ndqdn,                       $
                      nvmax, ndpro, iz, iz0, iz1, bwno,                 $
                      npl, bwnoa, il_lbseta, prtwta, il,                $
                      qdorb, il_lqdorb, qdn, iorb,                      $
                      ia, isa, ila, xja, wa, npla,                      $
                      ipla, zpla, nv, scef, itran,                      $
                      maxlev, i1a, i2a, aval, scom, np, proj,           $
                      beth, iadftyp, il_lprn, il_lcpl, il_lorb,         $
                      il_lbeth, il_lityp, il_lstyp, itieactn, il_ltied, $
                      str_vars, titled,  file_in)

; Convert mangled string arrays and logicals back to calling parameter names

res = byte(str_vars)
res = string(res)

off    = 0
cpla   = res[off:il-1]

off    = ndlev
cprta  = res[off: off+npl-1]

off    = ndlev + ndmet
cstrga = res[off: off+il-1]

ilen = max(strlen(strtrim(cstrga)))
cstrga = strmid(cstrga, 0, ilen)

off  = 2*ndlev + ndmet
cpro = res[off: off+np-1]

off    = 2*ndlev + ndmet + ndpro
if itran gt 0 then begin
        tcode  = res[off: off+itran-1]
endif else begin
        tcode=''
endelse


; Make the fulldata structure - trim to actual content.


; Set of projectiles

projectile        = cpro[0:np-1]
mass_target       = reform(proj[0,0:np-1])
charge_projectile = reform(proj[1,0:np-1])
mass_projectile   = reform(proj[2,0:np-1])

tcode   = tcode[0:itran-1]
ind_exc = where(tcode EQ ' ', n_exc)
ind_sl  = where(strupcase(tcode) EQ 'S', n_sl)

if nv GT 0 then begin
   scef = scef[0:nv-1]
endif else begin
   scef       = -1
   gamma      = -1
   lower      = -1
   upper      = -1
   aval       = -1
   beth       = -1
   level_ion  = -1
   parent_ion = -1
   ion        = -1
endelse


; excitation

if n_exc GT 0 then begin

   gamma = scom[0:nv-1, ind_exc, 0:np-1]
   if np EQ 1 then gamma = transpose(gamma) $
              else gamma = transpose(gamma, [1,0,2])
   aval  = aval[ind_exc]
   beth  = beth[ind_exc, 0:np-1]

   lower = i1a[ind_exc]
   upper = i2a[ind_exc]
      
endif

; ionisation

if n_sl GT 0 then begin

   ion = scom[0:nv-1, ind_sl, 0:np-1]
   if np EQ 1 then ion = transpose(ion) $
              else ion = transpose(ion, [1,0,2])
              
   level_ion  = i2a[ind_sl]
   parent_ion = i1a[ind_sl]

endif else begin

   level_ion  = -1
   parent_ion = -1
   ion        = -1

endelse


; Check if configurations are Eissner and make a standard set.

n_cfg = n_elements(cstrga)
cstrga_std = cstrga

for j = 0, n_cfg-1 do begin

   xxdtes, in_cfg=cstrga[j], is_eissner=is_eissner

   if is_eissner EQ 1 then begin
      xxcftr, in_cfg=cstrga[j], out_cfg=str, type=3
      cstrga_std[j] = str
   endif

endfor


fulldata = { filename          :  file,                   $
             iz                :  iz,                     $
             iz0               :  iz0,                    $
             iz1               :  iz1,                    $
             ia                :  ia[0:il-1],             $
             cstrga            :  cstrga,                 $
             isa               :  isa[0:il-1],            $
             ila               :  ila[0:il-1],            $
             xja               :  xja[0:il-1],            $
             wa                :  wa[0:il-1],             $
             zpla              :  zpla[0:npl-1, 0:il-1],  $
             ipla              :  ipla[0:npl-1, 0:il-1],  $
             ltied             :  il_ltied[0:il-1],       $
             bwno              :  bwno,                   $
             bwnoa             :  bwnoa[0:npl-1],         $
             cprta             :  cprta,                  $
             qdorb             :  qdorb,                  $
             lqdorb            :  il_lqdorb,              $
             qdn               :  qdn,                    $
             iorb              :  iorb,                   $
             te                :  scef,                   $
             lower             :  lower,                  $
             upper             :  upper,                  $
             aval              :  aval,                   $
             gamma             :  gamma,                  $
             lbeth             :  il_lbeth,               $
             beth              :  beth,                   $
             tcode             :  tcode,                  $
             projectile        :  projectile,             $
             mass_target       :  mass_target,            $
             charge_projectile :  charge_projectile,      $
             mass_projectile   :  mass_projectile,        $
             level_ion         :  level_ion,              $
             parent_ion        :  parent_ion,             $
             ion               :  ion,                    $
             iadftyp           :  iadftyp                 }

END
