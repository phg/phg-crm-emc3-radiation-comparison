;----------------------------------------------------------------------
;+
; PROJECT    :  ADAS
;
; NAME       :  read_adf14
;
; PURPOSE    :  Reads adf14 (TCX) files from the IDL command line.
;               called from IDL using the syntax
;               read_adf14,file=...,block=...,te=... etc
;
; ARGUMENTS  :  All output arguments will be defined appropriately.
;
;               NAME      I/O    TYPE   DETAILS
; REQUIRED   :  file       I     str    full name of ADAS adf14 file
;               block      I     int    selected block
;               t_rec      I     real() Receiver temperatures.
;               t_don      I     real() Donor temperatures.
;               m_rec      I     real() Receiver mass (amu).
;               m_don      I     real() Donor mass (amu).
; OPTIONAL      data       O      -     TCX data in cm**3 s**-1
;               info       O     struct information on reaction
;                                          donor     : donor
;                                          receiver  : receiver
;                                          state     : final state specifer
;
;               fulldata   O     struct  all of the data in adf14 file.
;                                        file      : filename
;                                        nblock    : number blocks in file
;                                        idz0      : nuclear charge donor
;                                                      - 1st dim: data block
;                                        irz0      : nuclear charge receiver
;                                                      - 1st dim: data block
;                                        irz1      : initial charge receiver
;                                                      - 1st dim: data block
;                                        amsda     : atomic mass donor
;                                                      - 1st dim: data block
;                                        amsra     : atomic mass receiver
;                                                      - 1st dim: data block
;                                        cdonor    : donor name
;                                                      - 1st dim: data block
;                                        crecvr    : receiver name
;                                                      - 1st dim: data block
;                                        cfstat    : final state
;                                                      - 1st dim: data block
;                                        itda      : number of donor temperatures
;                                                      - 1st dim: data block
;                                        tfda      : donor temperature
;                                                      - 1st dim: temperature index
;                                                      - 2nd dim: data block
;                                        itra      : number of receiver temperatures
;                                                      - 1st dim: data block
;                                        tfra      : receiver temperature
;                                                      - 1st dim: temperature index
;                                                      - 2nd dim: data block
;                                        lequa     : block contains equal temperature data
;                                                      - 1st dim: data block
;                                        qfteqa    : equal temperature rate coefficinets (cm3 s-1)
;                                                      - 1st dim: receiver temperature index
;                                                      - 2nd dim: data block
;                                        qftcxa    : rate coefficinets (cm3 s-1)
;                                                      - 1st dim: donor temperature index
;                                                      - 2nd dim: receiver temperature index
;                                                      - 3rd dim: data block
; KEYWORDS      kelvin            -     requested temperature in K (default eV)
;
; KEYWORDS      
;
;
; NOTES      :  This is part of a chain of programs - read_adf14.c and 
;               readadf14.for are required.
;
;               If fulldata is requested, interpolation is not performed.
;
; AUTHOR     :  Martin O'Mullane
; 
; DATE       :  12-11-2001
; 
; UPDATE     :  
;       1.2     Allan Whiteford
;                 - Added /help keyword
;       1.3     Martin O'Mullane
;                 - Spline routine handled 24 (not 30) points. More
;                   caused it to crash.
;       1.4     Martin O'Mullane
;                 - Change handling on infile.
;       1.5     Martin O'Mullane
;                 - Add fulldata output option.
;
; VERSION    :
;       1.1    12-11-2001
;       1.2    07-04-2005
;       1.3    12-09-2005
;       1.4    29-01-2010
;       1.5    06-03-2018
;-
;----------------------------------------------------------------------

PRO read_adf14, file     = file,     $ 
                block    = block,    $ 
                t_rec    = t_rec,    $
                t_don    = t_don,    $
                m_rec    = m_rec,    $
                m_don    = m_don,    $ 
                data     = data,     $
                info     = info,     $ 
                kelvin   = kelvin,   $
                fulldata = fulldata, $
                help     = help

; If asked for help

if keyword_set(help) then begin
   doc_library, 'read_adf14'
   return
endif

; Check that we get all inputs and that they are correct. Otherwise print
; a message and return to command line

on_error, 2



; file name

if n_elements(file) eq 0 then message, 'A file name must be passed'
file_acc, file, exist, read, write, execute, filetype
if exist ne 1 then message, 'TCX file does not exist '+file
if read ne 1 then message, 'TCX file cannot be read from this userid '+file

; If fulldata selected retrieve the data into a structure and return.

if arg_present(fulldata) then begin

   xxdata_14, file=file, fulldata=fulldata
   return
   
endif


; selection block

if n_elements(block) eq 0 then message, 'An TCX block must be selected'
parsize=size(block, /N_DIMENSIONS)
if parsize ne 0 then message,'Selection index cannot be an array'
partype=size(block, /TYPE)
if (partype ne 2) and (partype ne 3) then begin 
   message,'Selection index must be numeric'
endif else ibsel = LONG(block)

        
                
; temperatures

if n_elements(t_rec) eq 0 then message, 'User requested receiver temperatures are missing'
if n_elements(t_don) eq 0 then message, 'User requested donor temperatures are missing'

partype=size(t_rec, /type)
if (partype lt 2) or (partype gt 5) then begin 
   message,'Receiver temperatures must be numeric'
endif  else t_rec = DOUBLE(t_rec)

partype=size(t_don, /type)
if (partype lt 2) or (partype gt 5) then begin 
   message,'Donor temperatures must be numeric'
endif  else t_don = DOUBLE(t_don)

; Kelvin rather than eV

if keyword_set(kelvin) then begin
   t_rec = t_rec/11605.0
   t_don = t_don/11605.0
endif


; masses

if n_elements(m_rec) eq 0 then message, 'User requested receiver mass is missing'
if n_elements(m_don) eq 0 then message, 'User requested donor mass is missing'

partype=size(m_rec, /type)
if (partype lt 2) or (partype gt 5) then begin 
   message,'Receiver temperatures must be numeric'
endif  else m_rec = DOUBLE(m_rec)

partype=size(m_don, /type)
if (partype lt 2) or (partype gt 5) then begin 
   message,'Donor temperatures must be numeric'
endif  else m_don = DOUBLE(m_don)


; set variables for call to C/fortran reading of data

len_r = n_elements(t_rec)
len_d = n_elements(t_don)

if len_r NE len_d then print, 'Receiver and donor temperature size mismatch - smaller  used' 
itval = min([len_r,len_d])
itval = LONG(itval)

tr_in   = t_rec
td_in   = t_don

s_don   = '123456789'
s_rec   = '123456789'
s_state = '1234567890'

infile = string(replicate(32B, 80))
strput, infile, file, 0


fortdir = getenv('ADASFORT')

; Get data in blocks of 30 to avoid altering xxspln for large sets.

data = 0.0D0

MAXVAL = 24

n_call = numlines(itval, MAXVAL)

for j = 0, n_call - 1 do begin 
  
  ist = j*MAXVAL
  ifn = min([(j+1)*MAXVAL,itval])-1
  
  tr_ca    = tr_in[ist:ifn]
  td_ca    = td_in[ist:ifn]
  data_ca  = dblarr(ifn-ist+1)
  itval_ca = ifn-ist+1
 
  dummy = CALL_EXTERNAL(fortdir+'/read_adf14.so','read_adf14', $
                        infile, ibsel,                         $
                        itval_ca, tr_ca, td_ca,  m_rec, m_don, $
                        data_ca,  s_don, s_rec, s_state    )
  
  data = [data, data_ca]
  
endfor
data = data[1:*]

info = { donor     :  s_don,   $
         receiver  :  s_rec,   $
         state     :  s_state  }


; Reset temperature to K

if keyword_set(kelvin) then begin
   t_rec = t_rec*11605.0
   t_don = t_don*11605.0
endif
       
        
END
