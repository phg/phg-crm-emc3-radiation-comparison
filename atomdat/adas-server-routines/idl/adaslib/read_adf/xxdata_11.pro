;----------------------------------------------------------------------
;+
; PROJECT    :  ADAS
;
; NAME       :  xxdata_11
;
; PURPOSE    :  Reads adf11 files from IDL
;
; ARGUMENTS  :  All output arguments will be defined appropriately.
;               Inputs will be converted to correct type, if possible,
;               internally without changing calling type.
;
;               The argument list is the same as fortran except
;               that the file name is passed rather than a unit
;               number. Logicals are replaced by integers (0 and 1)
;               as IDL does not have a logical type.
;
;
;
;              NAME       I/O    TYPE    DETAILS
; REQUIRED   : file        I     str     adf11 file name
; OPTIONAL   : class       I     str     class name - if not given attempt
;                                        to determine it from filename.
;              fulldata    O     -       Output structure (see below)
;              help        I     switch  Help required
;
; The fulldata structure is defined as:
;
;  iz0       = nuclear charge
;  class     = One of 'acd','scd','ccd','prb','prc',
;                     'qcd','xcd','plt','pls','zcd',
;                     'ycd','ecd'
;  iclass    = 1..12 corresponding to above list.
;  is1min    = minimum ion charge + 1
;              (generalised to connection vector index)
;  is1max    = maximum ion charge + 1
;              (note excludes the bare nucleus)
;              (generalised to connection vector index
;               and excludes last one which always remains
;               the bare nucleus)
;  nptnl     = number of partition levels in block
;  nptn      = number of partitions in partition level
;              1st dim: partition level
;  nptnc    = number of components in partition
;              1st dim: partition level
;              2nd dim: member partition in partition level
;  iptnla   = partition level label (0=resolved root,1=
;                                       unresolved root)
;              1st dim: partition level index
;  iptna    = partition member label (labelling starts at 0)
;              1st dim: partition level index
;              2nd dim: member partition index in partition
;              level
;  iptnca   = component label (labelling starts at 0)
;              1st dim: partition level index
;              2nd dim: member partition index in partition
;              level
;              3rd dim: component index of member partition
;  ncnct     = number of elements in connection vector
;  icnctv    = connection vector of number of partitions
;              of each superstage in resolved case
;              including the bare nucleus
;              1st dim: connection vector index
;
;  iblmx     = number of (sstage, parent, base)
;              blocks in isonuclear master file
;  ismax     = number of charge states
;              in isonuclear master file
;              (generalises to number of elements in
;               connection vector)
;  dnr_elem  = donor element name (for ccd and prc)
;  dnr_ams   = donor element mass (for ccd and prc)
;  isppr     = 1st (parent) index for each partition block
;              1st dim: index of (sstage, parent, base)
;                       block in isonuclear master file
;  ispbr     = 2nd (base) index for each partition block
;              1st dim: index of (sstage, parent, base)
;                       block in isonuclear master file
;  isstgr    = s1 for each resolved data block
;              (generalises to connection vector index)
;              1st dim: index of (sstage, parent, base)
;                       block in isonuclear master file
;
;  idmax     = number of dens values in
;              isonuclear master files
;  itmax     = number of temp values in
;              isonuclear master files
;  ddens     = log10(electron density(cm-3)) from adf11
;  dtev      = log10(electron temperature (eV) from adf11
;  drcof     = if(iclass <=9):
;                 log10(coll.-rad. coefft.) from
;                 isonuclear master file
;              if(iclass >=10):
;                 coll.-rad. coefft. from
;                 isonuclear master file
;              1st dim: index of (sstage, parent, base)
;                       block in isonuclear master file
;              2nd dim: electron temperature index
;              3rd dim: electron density index
;
;  lres      = .true.  => partial file
;            = .false. => not partial file
;  lstan     = .true.  => standard file
;            = .false. => not standard file
;  lptn      = .true.  => partition block present
;            = .false. => partition block not present
;
;
; NOTES      :  Calls fortran code, xxdata_11.for.
;
; AUTHOR     :  Allan Whiteford
;
; DATE       :  20/01/07
;
;
; MODIFIED:
;       1.1     Allan Whiteford
;               - First version.
;       1.2     Martin O'Mullane
;               - Add donor element and mass for ccd and prc data.
;               - Change class from integer to string.
;               - Allow up to 51 temperatures for 85 datasets.
;       1.3     Martin O'Mullane
;               - No need to reform iptnca array.
;       1.4     Martin O'Mullane
;               - Return class as a string as well as an integer.
;               - Add missing itmax to fulldata structure.
;               - If class is not input, and it is not part of 
;                 the filename, check if it is given in the first line.
;       1.5     Matthew Bluteau
;               - Increased the isdimd dimension, which sets the maximum number
;                 of blocks allowed in an adf11 file. Needed for larger
;                 J-resolved files.
;
; VERSION:
;       1.1    20-01-2007
;       1.2    09-03-2007
;       1.3    11-10-2009
;       1.4    21-04-2010
;
;-
;----------------------------------------------------------------------

pro xxdata_11,file=file,class=class,fulldata=fulldata,help=help

; Return help if requested

    if keyword_set(help) then begin
        doc_library, 'xxdata_11'
        return
    endif

; Check the file is valid

    if n_elements(file) eq 0 then message,'A filename must be given'
    file_acc, file, exist, read, write, execute, filetype
    if exist ne 1 then message, 'adf11 file does not exist '+file
    if read ne 1 then message, 'adf11 file cannot be read from this userid '+file


    ; Get class name - use what is set otherwise determine from file name


    class_set=[ 'acd','scd','ccd','prb','prc', $
                'qcd','xcd','plt','pls','zcd', $
                'ycd','ecd']

    if n_elements(class) NE 0 then  begin

       class_tst = strlowcase(strcompress(class, /remove_all))

       res = where(class_set EQ class_tst, count)
       if count EQ 0 then begin
           print, 'Invalid class ' + class_tst
           fulldata=-1
           return
        endif

       if count GT 1 then begin
           print, 'Ambiguity in class ' + class_tst
           fulldata=-1
           return
        endif

        iclass = long(res[0]) + 1

    endif else begin
        
        ; Try from filename
        
        class=0
        for i=0,(size(class_set))[1]-1 do begin
            if strpos(file,class_set[i]) ne -1 then begin
                if class eq 0 then begin
                    class=i+1
                endif else begin
                    print,'Class of file ambiguous from filename, please specify manually'
                    fulldata=-1
                    return
                endelse
            endif
        endfor

        ; Try from first line
        
        if class eq 0 then begin
        
           str = ''
           openr, lun, file, /get_lun
           readf, lun, str
           free_lun, lun
        
           for j = 0, n_elements(class_set)-1 do begin
             if strpos(strupcase(str), strupcase(class_set[j])) NE -1 then class = j+1
           endfor
           
           iclass = long(class)
           
        endif
        
        ; Give up as it's not possible to determine it
        
        if class eq 0 then begin
            print,'Cannot determine class of file automatically, please specify manually'
            fulldata=-1
            return
        endif

        iclass=long(class)

    endelse

; Dimensions

    isdimd = 2000L
    izdimd = 92L
    itdimd = 51L
    iddimd = 40L
    ndptnl = 4L
    ndptn  = 128L
    ndptnc = 256L
    ndcnct = 100L

; Initialise variables

     iz0     =0L
     is1min  =0L
     is1max  =0L
     iblmx   =0L
     ismax   =0L
     itmax   =0L
     idmax   =0L
     nptnl   =0L
     ncnct   =0L
     dnr_ams =0.0D0

     nptn=lonarr(ndptnl)
     nptnc=lonarr(ndptnl,ndptn)
     iptnla=lonarr(ndptnl)
     iptna=lonarr(ndptnl,ndptn)
     iptnca=lonarr(ndptnl,ndptn,ndptnc)
     icnctv=lonarr(ndcnct)
     isppr=lonarr(isdimd)
     ispbr=lonarr(isdimd)
     isstgr=lonarr(isdimd)

     ddens=dblarr(iddimd)
     dtev=dblarr(itdimd)
     drcof=dblarr(isdimd,itdimd,iddimd)

; Really logicals but need to be passed as integers

     lres=0L
     lstan=0L
     lptn=0L

; Byte array for strings

str_vars = '            '
str_vars = byte(str_vars)
str_vars = long(str_vars)

; Get binary directory and setup inputs

    fortdir = getenv('ADASFORT')
    dnsfull=file

; Call shared object

    dummy = call_external(fortdir+'/xxdata_11_if.so','xxdata_11_if',    $
                                dnsfull, iclass ,                       $
                                isdimd , iddimd , itdimd ,              $
                                ndptnl , ndptn  , ndptnc , ndcnct ,     $
                                iz0    , is1min , is1max ,              $
                                nptnl  , nptn   , nptnc  ,              $
                                iptnla , iptna  , iptnca ,              $
                                ncnct  , icnctv ,                       $
                                iblmx  , ismax  , dnr_ams,              $
                                isppr  , ispbr  , isstgr ,              $
                                idmax  , itmax  ,                       $
                                ddens  , dtev   , drcof  ,              $
                                lres   , lstan  , lptn   , str_vars     )

; Bytes back to string

dnr_elem = string(byte(str_vars))


; Throw away unncessary data

     nptn=nptn[0:nptnl-1]
     nptnc=nptnc[0:nptnl-1,0:max(nptn)-1]
     iptnla=iptnla[0:nptnl-1]
     iptna=iptna[0:nptnl-1,0:max(nptn)-1]
     iptnca=iptnca[0:nptnl-1,0:max(nptn)-1,0:max(nptnc)-1]
;     if max(nptnc) eq 1 then iptnca=reform(iptnca,nptnl,nptn,1)
     icnctv=icnctv[0:ncnct-1]
     isppr=isppr[0:iblmx-1]
     ispbr=ispbr[0:iblmx-1]
     isstgr=isstgr[0:iblmx-1]

     ddens=ddens[0:idmax-1]
     dtev=dtev[0:itmax-1]
     drcof=drcof[0:iblmx-1,0:itmax-1,0:idmax-1]

; Assemble output data structure

    fulldata={  iz0       :    iz0                , $
                class     :    class_set[iclass-1], $
                iclass    :    iclass             , $
                is1min    :    is1min             , $
                is1max    :    is1max             , $
                nptnl     :    nptnl              , $
                nptn      :    nptn               , $
                nptnc     :    nptnc              , $
                iptnla    :    iptnla             , $
                iptna     :    iptna              , $
                iptnca    :    iptnca             , $
                ncnct     :    ncnct              , $
                icnctv    :    icnctv             , $
                iblmx     :    iblmx              , $
                ismax     :    ismax              , $
                dnr_elem  :    dnr_elem           , $
                dnr_ams   :    dnr_ams            , $
                isppr     :    isppr              , $
                ispbr     :    ispbr              , $
                isstgr    :    isstgr             , $
                idmax     :    idmax              , $
                itmax     :    itmax              , $
                ddens     :    ddens              , $
                dtev      :    dtev               , $
                drcof     :    drcof              , $
                lres      :    lres               , $
                lstan     :    lstan              , $
                lptn      :    lptn               , $
                file      :    file               }
end
