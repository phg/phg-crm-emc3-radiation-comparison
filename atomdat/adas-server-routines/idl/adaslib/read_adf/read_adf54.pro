;----------------------------------------------------------------------
;+
; PROJECT    :  ADAS
;
; NAME       :  read_adf54
;
; PURPOSE    :  Reads adf54 (promotion rules) files from the IDL command line.
;               called from IDL using the syntax
;               read_adf54,file=...,fulldata=fulldata
;
; ARGUMENTS  :  All output arguments will be defined appropriately.
;
;               NAME      I/O    TYPE   DETAILS
; REQUIRED   :  file       I     str    full name of ADAS adf15 file
;               fulldata   O     str    Structure containing the
;                                        adf54 details.
; OPTIONAL   :  comments   O     str    Comments at end of the file
;
; NOTES      :  
;
; The fulldata structure is defined:
;
;              config     : ground configuration
;              index      : reference index
;              n_el       : number of electron
;              no_v_shl   : number of open shell 
;              max_dn_v1  : maximum delta n promotion for first
;              min_dn_v1  : minimum delta n promotion for first
;              max_dl_v1  : maximum delta l promotion for first
;              min_dl_v1  : minimum delta l promotion for first
;              max_dn_v2  : maximum delta n promotion for second
;              min_dn_v2  : minimum delta n promotion for second
;              max_dl_v2  : maximum delta l promotion for second
;              min_dl_v2  : minimum delta l promotion for second
;              prom_cl    : promote from inner shell closed shells 
;              max_n_cl   : maximum inner shell n for promotion from
;              min_n_cl   : minimum inner shell n for promotion from
;              max_l_cl   : maximum inner shell l for promotion from
;              min_l_cl   : minimum inner shell l for promotion from
;              max_dn_cl  : maximum delta n promotion for inner shell to
;              min_dn_cl  : minimum delta n promotion for inner shell to
;              max_dl_cl  : maximum delta l promotion for inner shell to
;              min_dl_cl  : minimum delta l promotion for inner shell to
;              fill_n_v1  : add all nl configurations
;              fill_par   : add parity
;              for_tr_sel : Cowan option for radiative transitions
;              last_4f    : shift an electron valence shell to 4f
;              grd_cmplx  : include configurations of same complex
;
; AUTHOR     :  Alessandra Giunta
;
; DATE       :  02-10-2007
;
;
; MODIFIED:
;       
;               
;
; VERSION:
;      1.1      02-10-2007 
;-
;----------------------------------------------------------------------

pro read_adf54, file = file, fulldata = fulldata , comments=comments

;Open file to read
openr,lun,file,/get_lun
;Open tmp file
;openw,low,'tmp.dat',/get_lun
;Open tmp2 file
;openw,lo2w,'tmp2.dat',/get_lun


str=''
readf,lun,str

;Read the maximum number of elements for each array (vector)
n_max = fix(strmid(str,31,3))
nel = fix(strmid(str,39,2))

n_index = CEIL(n_max/nel)

;Define the vectors (or arrays)

config     = strarr(n_max)
index      = intarr(n_max)
n_el       = intarr(n_max)
no_v_shl   = intarr(n_max)
max_dn_v1  = intarr(n_max)
min_dn_v1  = intarr(n_max)
max_dl_v1  = intarr(n_max)
min_dl_v1  = intarr(n_max)
max_dn_v2  = intarr(n_max)
min_dn_v2  = intarr(n_max)
max_dl_v2  = intarr(n_max)
min_dl_v2  = intarr(n_max)
prom_cl    = intarr(n_max) 
max_n_cl   = intarr(n_max)
min_n_cl   = intarr(n_max)
max_l_cl   = intarr(n_max)
min_l_cl   = intarr(n_max)
max_dn_cl  = intarr(n_max)
min_dn_cl  = intarr(n_max)
max_dl_cl  = intarr(n_max)
min_dl_cl  = intarr(n_max)
fill_n_v1  = intarr(n_max)
fill_par   = intarr(n_max)
for_tr_sel = intarr(n_max)
last_4f    = intarr(n_max)
grd_cmplx  = intarr(n_max)

;Get data in the defined vectors (or arrays)
for i = 0,n_index-1 do begin

   readf,lun,str
   
   ; check is final iteration, in case number of elements is less
   ; than in other iterations.
   IF (i EQ n_index-1) THEN nel = n_max-(i*nel)

   while (strmid(str,0,15) NE 'index    config') do begin
     readf, lun, str
   endwhile

   for j= 0,nel-1 do begin
     readf,lun,str
     index_p = fix(strmid(str,2,3))
     config_p = string(strmid(str,9,85))
      
     index[(i*nel)+j]=index_p
     config[(i*nel)+j]=config_p
   endfor

   while (strmid(str,0,5) NE 'index') do begin
     readf, lun, str
   endwhile
   

   fmt='(a13,'+STRCOMPRESS(STRING(nel),/REMOVE_ALL)+'0i6)'
   ftmp='(i6)'      

   n_el_p=intarr(nel)
   no_v_shl_p=intarr(nel)
   max_dn_v1_p=intarr(nel)
   min_dn_v1_p=intarr(nel)
   max_dl_v1_p=intarr(nel)
   min_dl_v1_p=intarr(nel)
   max_dn_v2_p=intarr(nel)
   min_dn_v2_p=intarr(nel)
   max_dl_v2_p=intarr(nel)
   min_dl_v2_p=intarr(nel)
   prom_cl_p=intarr(nel)
   max_n_cl_p=intarr(nel)
   min_n_cl_p=intarr(nel)
   max_l_cl_p=intarr(nel)
   min_l_cl_p=intarr(nel)
   max_dn_cl_p=intarr(nel)
   min_dn_cl_p=intarr(nel)
   max_dl_cl_p=intarr(nel)
   min_dl_cl_p=intarr(nel)
   fill_n_v1_p=intarr(nel)
   fill_par_p=intarr(nel)
   for_tr_sel_p=intarr(nel)
   last_4f_p=intarr(nel)
   grd_cmplx_p=intarr(nel)

   readf,lun,str,n_el_p,format=fmt
   readf,lun,str,no_v_shl_p,format=fmt
   readf,lun,str,max_dn_v1_p,format=fmt
   readf,lun,str,min_dn_v1_p,format=fmt
   readf,lun,str,max_dl_v1_p,format=fmt
   readf,lun,str,min_dl_v1_p,format=fmt       
   readf,lun,str,max_dn_v2_p,format=fmt
   readf,lun,str,min_dn_v2_p,format=fmt
   readf,lun,str,max_dl_v2_p,format=fmt
   readf,lun,str,min_dl_v2_p,format=fmt 
   readf,lun,str,prom_cl_p,format=fmt
   readf,lun,str,max_n_cl_p,format=fmt
   readf,lun,str,min_n_cl_p,format=fmt
   readf,lun,str,max_l_cl_p,format=fmt
   readf,lun,str,min_l_cl_p,format=fmt
   readf,lun,str,max_dn_cl_p,format=fmt
   readf,lun,str,min_dn_cl_p,format=fmt
   readf,lun,str,max_dl_cl_p,format=fmt
   readf,lun,str,min_dl_cl_p,format=fmt
   readf,lun,str,fill_n_v1_p,format=fmt
   readf,lun,str,fill_par_p,format=fmt
   readf,lun,str,for_tr_sel_p,format=fmt
   readf,lun,str,last_4f_p,format=fmt
   readf,lun,str,grd_cmplx_p,format=fmt

; store data in arrays
   
   n_el[nel*i:(nel*(i+1))-1]       = n_el_p
   no_v_shl[nel*i:(nel*(i+1))-1]   = no_v_shl_p
   max_dn_v1[nel*i:(nel*(i+1))-1]  = max_dn_v1_p
   min_dn_v1[nel*i:(nel*(i+1))-1]  = min_dn_v1_p
   max_dl_v1[nel*i:(nel*(i+1))-1]  = max_dl_v1_p
   min_dl_v1[nel*i:(nel*(i+1))-1]  = min_dl_v1_p
   max_dn_v2[nel*i:(nel*(i+1))-1]  = max_dn_v2_p
   min_dn_v2[nel*i:(nel*(i+1))-1]  = min_dn_v2_p
   max_dl_v2[nel*i:(nel*(i+1))-1]  = max_dl_v2_p
   min_dl_v2[nel*i:(nel*(i+1))-1]  = min_dl_v2_p
   prom_cl[nel*i:(nel*(i+1))-1]    = prom_cl_p
   max_n_cl[nel*i:(nel*(i+1))-1]   = max_n_cl_p
   min_n_cl[nel*i:(nel*(i+1))-1]   = min_n_cl_p
   max_l_cl[nel*i:(nel*(i+1))-1]   = max_l_cl_p
   min_l_cl[nel*i:(nel*(i+1))-1]   = min_l_cl_p
   max_dn_cl[nel*i:(nel*(i+1))-1]  = max_dn_cl_p
   min_dn_cl[nel*i:(nel*(i+1))-1]  = min_dn_cl_p
   max_dl_cl[nel*i:(nel*(i+1))-1]  = max_dl_cl_p
   min_dl_cl[nel*i:(nel*(i+1))-1]  = min_dl_cl_p
   fill_n_v1[nel*i:(nel*(i+1))-1]  = fill_n_v1_p
   fill_par[nel*i:(nel*(i+1))-1]   = fill_par_p
   for_tr_sel[nel*i:(nel*(i+1))-1] = for_tr_sel_p
   last_4f[nel*i:(nel*(i+1))-1]    = last_4f_p
   grd_cmplx[nel*i:(nel*(i+1))-1]  = grd_cmplx_p

endfor

;Define the promotion rule structure

fulldata={                          config     : config,                            $
                                    index      : index,                             $
                                    n_el       : n_el,                              $
                                    no_v_shl   : no_v_shl,                          $
                                    max_dn_v1  : max_dn_v1,                         $
                                    min_dn_v1  : min_dn_v1,                         $
                                    max_dl_v1  : max_dl_v1,                         $
                                    min_dl_v1  : min_dl_v1,                         $
                                    max_dn_v2  : max_dn_v2,                         $
                                    min_dn_v2  : min_dn_v2,                         $
                                    max_dl_v2  : max_dl_v2,                         $
                                    min_dl_v2  : min_dl_v2,                         $
                                    prom_cl    : prom_cl,                           $
                                    max_n_cl   : max_n_cl,                          $
                                    min_n_cl   : min_n_cl,                          $
                                    max_l_cl   : max_l_cl,                          $
                                    min_l_cl   : min_l_cl,                          $
                                    max_dn_cl  : max_dn_cl,                         $
                                    min_dn_cl  : min_dn_cl,                         $
                                    max_dl_cl  : max_dl_cl,                         $
                                    min_dl_cl  : min_dl_cl,                         $
                                    fill_n_v1  : fill_n_v1,                         $
                                    fill_par   : fill_par,                          $
                                    for_tr_sel : for_tr_sel,                        $      
                                    last_4f    : last_4f,                           $
                                    grd_cmplx  : grd_cmplx}






comments=['']
while not EOF(lun) do begin
 dummy=' '
 readf, lun, dummy
 if strupcase(STRMID(dummy,0,1)) EQ 'C' then comments=[comments, dummy]
endwhile

if n_elements(comments) GT 1 then comments=comments[1:*]


close,lun
free_lun,lun
   
end
