;----------------------------------------------------------------------
;+
; PROJECT    :  ADAS
;
; NAME       :  xxdata_02
;
; PURPOSE    :
;
; ARGUMENTS  :  All output arguments will be defined appropriately.
;               Inputs will be converted to correct type, if possible,
;               internally without changing calling type.
;
;                xxdata_02, file=file, fulldata=fulldata
;
;               NAME      I/O    TYPE    DETAILS
; REQUIRED   :  file       I     str     adf02 file name
; OPTIONAL      fulldata   O     struc   data in the adf02 file
;
; The fulldata structure is defined:
;
;               filename    str     adf02 file name
;               nbsel       long    number of blocks present
;
;               nedim       long    max number of collision energies allowed
;
;               nbsel       long    number of data-blocks accepted/read in
;               isela()     long    data-block entry indices
;
;               cprimy()    str     primary species identification
;               csecdy()    str     secondary species identification
;               ctype()     str     cross-section type
;               ampa()      double  primary species atomic mass number
;               amsa()      double  secondary species atomic mass number
;               alpha()     double  high energy extrapolation parm.
;               ethra()     double  energy threshold (ev)
;               iea()       double  number of collision energies
;               teea(,)     double  collision energies (units: ev/amu)
;                                     1st dimension: collision energy index
;                                     2nd dimension: data-block index
;               sia(,)      double  full set of collision
;                                   cross-section values (cm**2)
;                                          1st dimension: collision energy index
;                                          2nd dimension: data-block index
;
;
; NOTES      :  Calls fortran code.
;
;
; AUTHOR     :  Martin O'Mullane
;
; DATE       :  01-12-2005
;
;
; MODIFIED:
;       1.1     Martin O'Mullane
;               - First version.
;       1.2     Martin O'Mullane
;               - IEA in fulldata was passed back with incorrect type.
;       1.3     Martin O'Mullane
;                 - Return data as a fulldata structure to bring
;                   in line with other xxdata routines.
;                 - No need for nstore in fulldata.
;       1.4     Martin O'Mullane
;                 - Bring in line with the changed wrapper code.
;
;
; VERSION:
;       1.1    01-12-2005
;       1.2    10-06-2009
;       1.3    11-11-2018
;       1.4    10-07-2019
;
;-
;----------------------------------------------------------------------

PRO xxdata_02, file=file, fulldata=fulldata

if n_elements(file) EQ 0 then message,'Filename must be set'

; Convert filename to byte array

fileb = file
adas_string_pad, fileb, 132
dsnin = byte(fileb)


; Parameters for sizing

nstore = 200L
nedim  = 24L

; Check that the adf02 file exists

file_acc, file, exist, read, write, execute, filetype
if exist ne 1 then message, 'adf02 file does not exist ' + file
if read ne 1 then message, 'adf02 file cannot be read from this userid ' + file


; Define types of the output parameters

nbsel   = -1L
isela   = lonarr(nstore)
cprimy  = bytarr(5, nstore)
csecdy  = bytarr(5, nstore)
ctype   = bytarr(3, nstore)
ampa    = dblarr(nstore)
amsa    = dblarr(nstore)
alpha   = dblarr(nstore)
ethra   = dblarr(nstore)
iea     = lonarr(nstore)
teea    = dblarr(nedim, nstore)
sia     = dblarr(nedim, nstore)


; Location of sources

adasfort = getenv('ADASFORT')

res = call_external(adasfort+'/xxdata_02_if.so','xxdata_02_if',$
                    dsnin,                                     $
                    nstore  , nedim   ,                        $
                    nbsel   , isela   ,                        $
                    cprimy  , csecdy  , ctype  ,               $
                    ampa    , amsa    , alpha  ,               $
                    ethra   , iea     , teea   , sia)


; Convert mangled string arrays back to calling parameter names

cprimy = string(cprimy)
csecdy = string(csecdy)
ctype  = string(ctype)

fulldata = { filename  :  file,                          $
             nedim     :  nedim,                         $
             nbsel     :  nbsel,                         $
             isela     :  isela[0:nbsel-1],              $
             cprimy    :  cprimy[0:nbsel-1],             $
             csecdy    :  csecdy[0:nbsel-1],             $
             ctype     :  ctype[0:nbsel-1],              $
             ampa      :  ampa[0:nbsel-1],               $
             amsa      :  amsa[0:nbsel-1],               $
             alpha     :  alpha[0:nbsel-1],              $
             ethra     :  ethra[0:nbsel-1],              $
             iea       :  iea[0:nbsel-1],                $
             teea      :  teea[0:max(iea)-1, 0:nbsel-1], $
             sia       :  sia[0:max(iea)-1, 0:nbsel-1]   }

END
