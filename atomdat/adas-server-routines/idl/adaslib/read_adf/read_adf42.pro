;----------------------------------------------------------------------
;+
; PROJECT    :  ADAS
;
; NAME       :  read_adf42
;
; PURPOSE    :  Read an adf42 driver file for adas810.
;
; ARGUMENTS  :  All output arguments will be defined appropriately.
;
;               NAME      I/O    TYPE    DETAILS
; REQUIRED   :  fulldata   O       -     Structure containing the
;                                        adf42 details.
;               file       O      str    name of adf42 file.
;
; KEYWORDS      help               -     if specified this comment
;                                        section is written to screen.
;
;
; NOTES      :
;
; The fulldata structure is defined:4
;
;    fulldata = { DSN04     :   
;                 DSN18     :   
;                 DSN35     :   
;                 DSN15     :   
;                 DSN40     :   
;                 DSN11     :   
;                 DSN11F    :   
;                 ELEMENT   :   
;                 Z0        :   
;                 Z1        :   
;                 IP        :   
;                 LNORM     :   
;                 NMET      :    
;                 IMETR     :                                    
;                 LIOSEL    :  
;                 LHSEL     :   
;                 LRSEL     :   
;                 LISEL     : 
;                 LNSEL     : 
;                 LPSEL     : 
;                 ZEFF      : 
;                 LMETR     :   
;                 LTSCL     :   
;                 LDSCL     :   
;                 LBRDI     :                         
;                 AMIN      :    
;                 NUMTE     :   
;                 NUMTION   : 
;                 NUMTH     :                         
;                 NUMDENS   : 
;                 NUMDION   : 
;                 NUMWVL    :
;                 te        :
;                 dens      :
;                 npix      :
;                 wvmin     :
;                 wvmax     :
;
;
; AUTHOR     :  Martin O'Mullane
;
; DATE       :  14-07-2008
;
;
; MODIFIED:
;       1.1     Martin O'Mullane
;               - First version.
;
; VERSION:
;       1.1    14-07-2008
;-
;----------------------------------------------------------------------


PRO read_adf42, file=file, fulldata=fulldata



; adf42 namelist and defaults

a42names = ['DSN04',   'DSN18',   'DSN35',   'DSN15', 'DSN40', 'DSN11', 'DSN11F', $
            'ELEMENT', 'Z0',      'Z1',      'IP',                                $
            'LNORM',   'NMET',    'IMETR',                                        $
            'LIOSEL',  'LHSEL',   'LRSEL',   'LISEL', 'LNSEL', 'LPSEL', 'ZEFF',   $
            'LMETR',   'LTSCL',   'LDSCL',   'LBRDI',                             $
            'AMIN',    'NUMTE',   'NUMTION', 'NUMTH',                             $
            'NUMDENS', 'NUMDION', 'NUMWVL']

a42def   = [ 'NULL', 'NULL', 'NULL', 'NULL', 'NULL', 'NULL', 'NULL',                      $
             'C',     '6',        '3',       '386241.0',                                  $
             '.TRUE.', '1', '1',                                                          $
             '.FALSE.', '.FALSE.', '.FALSE.', '.FALSE.', '.FALSE.', '.FALSE.', '3.0',     $
             '.FALSE.',  '.FALSE.',  '.FALSE.',  '.FALSE.',                               $
             '0.0',      '5',        '5',        '0',       '5',  '0', '1' ]



; Replace defaults with input data


openr, lun, file, /get_lun
dummy=' '
readf,lun, dummy
done = 0
namelists=['&FILES', '&ION','&META', '&PROCESS', '&OUTPUT']
namelists_done=[0,0,0,0,0]
while NOT EOF(lun) AND done NE 1 do begin
  IF (STRPOS(dummy,namelists[0]))[0] NE -1 THEN BEGIN
    readf,lun, dummy
    WHILE NOT EOF(lun) AND (STRPOS(dummy,'&END'))[0] EQ -1 do begin
      dummy2=strsplit(dummy,'=',/extract)
      CASE strlowcase(strcompress(dummy2[0],/remove_all)) OF
        'dsn04' : dsn04=STRCOMPRESS((STRSPLIT(dummy2[1],"'",/extract))[1],/REMOVE_ALL)
        'dsn18' : dsn18=STRCOMPRESS((STRSPLIT(dummy2[1],"'",/extract))[1],/REMOVE_ALL)
        'dsn35' : dsn35=STRCOMPRESS((STRSPLIT(dummy2[1],"'",/extract))[1],/REMOVE_ALL)
        'dsn15' : dsn15=STRCOMPRESS((STRSPLIT(dummy2[1],"'",/extract))[1],/REMOVE_ALL)
        'dsn40' : dsn40=STRCOMPRESS((STRSPLIT(dummy2[1],"'",/extract))[1],/REMOVE_ALL)
        'dsn11' : dsn11=STRCOMPRESS((STRSPLIT(dummy2[1],"'",/extract))[1],/REMOVE_ALL)
        'dsn11f': dsn11f=STRCOMPRESS((STRSPLIT(dummy2[1],"'",/extract))[1],/REMOVE_ALL)
        ELSE    : begin
                    print, 'ERROR IN READ_ADF42: tag name '+dummy2[0] +$
                           'not recognised in file '+file
                    stop       
                  end
      ENDCASE
      readf, lun, dummy
    ENDWHILE
    namelists_done[0]=1
  ENDIF ELSE IF (STRPOS(dummy,namelists[1]))[0] NE -1 THEN BEGIN
    readf,lun, dummy
    WHILE NOT EOF(lun) AND (STRPOS(dummy,'&END'))[0] EQ -1 do begin
      dummy2=strsplit(dummy,'=',/extract)
      CASE strlowcase(strcompress(dummy2[0],/remove_all)) OF
        'element' : element=STRCOMPRESS((STRSPLIT(dummy2[1],"'",/extract))[1],/REMOVE_ALL)
        'z0'      : z0     =STRCOMPRESS((STRSPLIT(dummy2[1],",",/extract))[0],/REMOVE_ALL)
        'z1'      : z1     =STRCOMPRESS((STRSPLIT(dummy2[1],",",/extract))[0],/REMOVE_ALL)
        'ip'      : ip     =STRCOMPRESS((STRSPLIT(dummy2[1],",",/extract))[0],/REMOVE_ALL)
        ELSE    : begin
                    print, 'ERROR IN READ_ADF42: tag name '+dummy2[0] +$
                           'not recognised in file '+file
                    stop
                  end
      ENDCASE
      readf, lun, dummy
    ENDWHILE
    namelists_done[1]=1
  ENDIF ELSE IF (STRPOS(dummy,namelists[2]))[0] NE -1 THEN BEGIN
    readf,lun, dummy
    WHILE NOT EOF(lun) AND (STRPOS(dummy,'&END'))[0] EQ -1 do begin
      dummy2=strsplit(dummy,'=',/extract)
      CASE strlowcase(strcompress(dummy2[0],/remove_all)) OF
        'lnorm'   : lnorm  =STRCOMPRESS((STRSPLIT(dummy2[1],",",/extract))[0],/REMOVE_ALL)
        'nmet'    : nmet   =STRCOMPRESS((STRSPLIT(dummy2[1],",",/extract))[0],/REMOVE_ALL)
        'imetr(1)': imetr  =STRCOMPRESS((STRSPLIT(dummy2[1],",",/extract))[0],/REMOVE_ALL)
        ELSE    : begin
                    print, 'ERROR IN READ_ADF42: tag name '+dummy2[0] +$
                           'not recognised in file '+file
                    stop
                  end
      ENDCASE
      readf, lun, dummy
    ENDWHILE
    namelists_done[2]=1
  ENDIF ELSE IF(STRPOS(dummy,namelists[3]))[0] NE -1 THEN BEGIN
    readf,lun, dummy
    WHILE NOT EOF(lun) AND (STRPOS(dummy,'&END'))[0] EQ -1 do begin
      dummy2=strsplit(dummy,'=',/extract)
      CASE strlowcase(strcompress(dummy2[0],/remove_all)) OF
        'liosel'  : liosel =STRCOMPRESS((STRSPLIT(dummy2[1],",",/extract))[0],/REMOVE_ALL)
        'lhsel'   : lhsel  =STRCOMPRESS((STRSPLIT(dummy2[1],",",/extract))[0],/REMOVE_ALL)
        'lrsel'   : lrsel  =STRCOMPRESS((STRSPLIT(dummy2[1],",",/extract))[0],/REMOVE_ALL)
        'lisel'   : lisel  =STRCOMPRESS((STRSPLIT(dummy2[1],",",/extract))[0],/REMOVE_ALL)
        'lnsel'   : lnsel  =STRCOMPRESS((STRSPLIT(dummy2[1],",",/extract))[0],/REMOVE_ALL)
        'lpsel'   : lpsel  =STRCOMPRESS((STRSPLIT(dummy2[1],",",/extract))[0],/REMOVE_ALL)
        'zeff'    : zeff   =STRCOMPRESS((STRSPLIT(dummy2[1],",",/extract))[0],/REMOVE_ALL)
        ELSE    : begin
                    print, 'ERROR IN READ_ADF42: tag name '+dummy2[0] +$
                           'not recognised in file '+file
                    stop
                  end
      ENDCASE
      readf, lun, dummy
    ENDWHILE
    namelists_done[3]=1
  ENDIF ELSE IF(STRPOS(dummy,namelists[4]))[0] NE -1 THEN BEGIN
    readf,lun, dummy
    WHILE NOT EOF(lun) AND (STRPOS(dummy,'&END'))[0] EQ -1 do begin
      dummy2=strsplit(dummy,'=',/extract)
      CASE strlowcase(strcompress(dummy2[0],/remove_all)) OF
        'lmetr'    : lmetr    =STRCOMPRESS((STRSPLIT(dummy2[1],",",/extract))[0],/REMOVE_ALL)
        'ltscl'    : ltscl    =STRCOMPRESS((STRSPLIT(dummy2[1],",",/extract))[0],/REMOVE_ALL)
        'ldscl'    : ldscl    =STRCOMPRESS((STRSPLIT(dummy2[1],",",/extract))[0],/REMOVE_ALL)
        'lbrdi'    : lbrdi    =STRCOMPRESS((STRSPLIT(dummy2[1],",",/extract))[0],/REMOVE_ALL)
        'amin'     : amin     =STRCOMPRESS((STRSPLIT(dummy2[1],",",/extract))[0],/REMOVE_ALL)
        'numte'    : numte    =STRCOMPRESS((STRSPLIT(dummy2[1],",",/extract))[0],/REMOVE_ALL)
        'numtion'  : numtion  =STRCOMPRESS((STRSPLIT(dummy2[1],",",/extract))[0],/REMOVE_ALL)
        'numth'    : numth    =STRCOMPRESS((STRSPLIT(dummy2[1],",",/extract))[0],/REMOVE_ALL)
        'numdens'  : numdens  =STRCOMPRESS((STRSPLIT(dummy2[1],",",/extract))[0],/REMOVE_ALL)
        'numdion'  : numdion  =STRCOMPRESS((STRSPLIT(dummy2[1],",",/extract))[0],/REMOVE_ALL)
        'numwvl'   : numwvl   =STRCOMPRESS((STRSPLIT(dummy2[1],",",/extract))[0],/REMOVE_ALL)
        ELSE    : begin
                    print, 'ERROR IN READ_ADF42: tag name '+dummy2[0] +$
                           'not recognised in file '+file
                    stop
                  end
      ENDCASE
      readf, lun, dummy
    ENDWHILE
    namelists_done[4]=1
  ENDIF
  readf, lun, dummy
  if total(namelists_done) GE 5 then done = 1
ENDWHILE

;make non string items back into non-strings, etc
z0      = fix(z0)
z1      = fix(z1)
ip      = ip*1d0
nmet    = fix(nmet)
imetr   = fix(imetr)
zeff    = zeff*1d0
amin    = amin*1d0
numte   = fix(numte)
numtion = fix(numtion)
numth   = fix(numth)
numdens = fix(numdens)
numdion = fix(numdion)
numwvl  = fix(numwvl)


te = dblarr(numte)
if numtion GT 0 then tion = dblarr(numtion)
if numth GT 0 then th = dblarr(numth)
dens = dblarr(numdens)
if numdion GT 0 then dion = dblarr(numdion)
npix = lonarr(numwvl)
wvmin = dblarr(numwvl)
wvmax = dblarr(numwvl)

readf, lun, te
if numtion GT 0 then readf, lun, tion else tion=-1
if numth GT 0 then readf, lun, th else th = -1
readf, lun, dens
if numdion GT 0 then readf, lun,dion else dion = -1
tmpnpix=0l
tmpwvmin=0d0
tmpwvmax=0d0
for iwv=0, numwvl-1 do begin
  readf, lun, tmpnpix,tmpwvmin,tmpwvmax
  npix[iwv]=tmpnpix
  wvmin[iwv]=tmpwvmin
  wvmax[iwv]=tmpwvmax
endfor  

close, lun
free_lun, lun



fulldata = { DSN04     :DSN04  ,$
             DSN18     :DSN18  ,$
             DSN35     :DSN35  ,$
             DSN15     :DSN15  ,$
             DSN40     :DSN40  ,$
             DSN11     :DSN11  ,$
             DSN11F    :DSN11F ,$
             ELEMENT   :ELEMENT,$
             Z0        :Z0     ,$
             Z1        :Z1     ,$
             IP        :IP     ,$
             LNORM     :LNORM  ,$
             NMET      :NMET   ,$
             IMETR     :IMETR  ,$
             LIOSEL    :LIOSEL ,$
             LHSEL     :LHSEL  ,$
             LRSEL     :LRSEL  ,$
             LISEL     :LISEL  ,$
             LNSEL     :LNSEL  ,$
             LPSEL     :LPSEL  ,$
             ZEFF      :ZEFF   ,$
             LMETR     :LMETR  ,$
             LTSCL     :LTSCL  ,$
             LDSCL     :LDSCL  ,$
             LBRDI     :LBRDI  ,$
             AMIN      :AMIN   ,$
             NUMTE     :NUMTE  ,$
             NUMTION   :NUMTION,$
             NUMTH     :NUMTH  ,$
             NUMDENS   :NUMDENS,$
             NUMDION   :NUMDION,$
             NUMWVL    :NUMWVL ,$
             te        :te     ,$
             tion      :tion   ,$
             th        :th     ,$
             dens      :dens   ,$
             dion      :dion   ,$
             npix      :npix   ,$
             wvmin     :wvmin  ,$
             wvmax     :wvmax  }

END
