;----------------------------------------------------------------------
;+
; PROJECT    :  ADAS
;
; NAME       :  xxdata_13
;
; PURPOSE    :
;
; ARGUMENTS  :  All output arguments will be defined appropriately.
;               Inputs will be converted to correct type, if possible,
;               internally without changing calling type.
;
;                xxdata_13
;
;               NAME       I/O    TYPE    DETAILS
; REQUIRED   :  a13file     I     char    adf13 filename
;               nstore      I     long    maximum number of blocks
;               ntdim       I     long    maximum number of temperatures
;               nddim       I     long    maximum number of densities
;               iz          O     long    recombined ion charge
;               iz0         O     long    nuclear charge
;               iz1         O     long    recombining ion charge
;               esym        O     char    element symbol
;               nbsel       O     long    number of blocks
;               isela()     O     long    data-set data-block entry indices
;                                           dimension : blocks
;               cwavel()    O     char    wavelength string
;                                           dimension : blocks
;               cfile()     O     char    adf04 filename fragment
;                                           dimension : blocks
;               cpcode()    O     char    production code
;                                           dimension : blocks
;               cindm()     O     char    metastable/total indicator
;                                           dimension : blocks
;               ita()       O     long    number of temperatures
;                                           dimension : blocks
;               ida()       O     long    number of densities
;                                           dimension : blocks
;               teta()      O     double  temperatures
;                                           dimension : temperature
;                                           dimension : blocks
;               teda()      O     double  densities
;                                           dimension : density
;                                           dimension : blocks
;               sxb         O     double  densities
;                                           dimension : temperature
;                                           dimension : density
;                                           dimension : blocks
;
; NOTES      :  Calls fortran code.
;
;
; AUTHOR     :  Martin O'Mullane
;
; DATE       :  17-02-2006
;
;
; MODIFIED:
;       1.1     Martin O'Mullane
;               - First version (based on xxdata_15).
;       1.2     Martin O'Mullane
;               - Change ctype to cpcode.
;
;
; VERSION:
;       1.1    17-02-2006
;       1.2    17-07-2017
;
;-
;----------------------------------------------------------------------

PRO xxdata_13, a13file,                              $
               nstore  , ntdim  , nddim  ,           $
               iz0     , iz     , iz1    , esym  ,   $
               nbsel   , isela  ,                    $
               cwavel  , cfile  , cpcode , cindm ,   $
               ita     , ida    ,                    $
               teta    , teda   , sxb



; Check number of parameters

if n_params() NE 19 then message, 'Not all parameters present', /continue


; Check of mandatory inputs and convert to LONG type.

if ntdim  EQ 0 then message, 'Set maximum number of temperatures'
if nddim  EQ 0 then message, 'Set maximum number of densities'
if nstore EQ 0 then message, 'Set maximum number of blocks'

ntdim  = long(ntdim)
nddim  = long(nddim)
nstore = long(nstore)


; Check that the adf13 file exists

file_acc, a13file, exist, read, write, execute, filetype
if exist ne 1 then message, 'adf13 file does not exist '+a13file
if read ne 1 then message, 'adf13 file cannot be read from this userid '+a13file

; Define types of the output parameters

iunit     = 67L
esym      = '  '
iz        = -1L
iz0       = -1L
iz1       = -1L
nbsel     = -1L
isela     = lonarr(nstore)
ita       = lonarr(nstore)
ida       = lonarr(nstore)
teta      = dblarr(ntdim, nstore)
teda      = dblarr(nddim, nstore)
sxb       = dblarr(ntdim, nddim, nstore)
cindm     = strarr(nstore) + '  '
cfile     = strarr(nstore) + '        '
cpcode    = strarr(nstore) + '        '
cwavel    = strarr(nstore) + '          '


file      = string(replicate(32B, 80))
strput, file, a13file, 0

; Combine string arrays into a byte array

str_vars = [cindm, cfile, cpcode, cwavel]
str_vars = byte(str_vars)
str_vars = long(str_vars)


; Location of sources

fortdir = getenv('ADASFORT')

dummy = CALL_EXTERNAL(fortdir+'/xxdata_13_if.so','xxdata_13_if',  $
                      iunit, nstore, ntdim, nddim, iz0,           $
                      iz, iz1, nbsel, isela, ita,                 $
                      ida, teta, teda, sxb, str_vars, file, esym)

; Convert mangled string arrays  back to calling parameter names

res = byte(str_vars)
res = string(res)

cindm  = res[0:nstore-1]
cfile  = res[nstore:2*nstore-1]
cpcode = res[2*nstore:3*nstore-1]
cwavel = res[3*nstore:4*nstore-1]

END
