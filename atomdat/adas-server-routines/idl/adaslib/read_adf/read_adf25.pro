;----------------------------------------------------------------------
;+
; PROJECT    :  ADAS
;
; NAME       :  read_adf25
;
; PURPOSE    :  Reads adf25 bundle-n driver files from the IDL command line.
;               Called from IDL using the syntax
;                  read_adf25,file=..., fulldata=
;
; ARGUMENTS  :  All output arguments will be defined appropriately.
;
;               NAME      I/O    TYPE    DETAILS
; REQUIRED   :  file       I     str     full name of ADAS adf25 file
; OPTIONAL      help       I             display help entry
;               fulldata   O      -      Structure containing all the
;                                        adf25 data.
;
; NOTES      :  Only the fulldata strucutre is returned. There is
;               no other sensible output.
;
;
; The fulldata structure is defined:
;
;
;       a25file  = file name of adf25 format read.
;       iz0      = nuclear charge of bundle-n ion
;       iz1      = recombining ion charge of bundle-n ion
;       a25fmt   = subdirectory type of adf25 to be read.
;       outfmt   = format of output ADAS data format for final results
;       cxfile   = file name for charge exchange data input
;       exfile   = file name for map of proj. matrix output
;       ndens    = number of electron densities
;       id_ref   = reference electron density pointer in vectors
;       densa()  = plasma electron density vector (cm-3)
;                  1st dim: index of electron density
;       denpa()  = plasma H+ density vector (cm-3)
;                  1st dim: index of electron density
;       denimpa()= plasma mean impurity ion density (cm-3)
;                  1st dim: index of electron density
;       ntemp    = number of electron temperatures
;       id_ref   = reference electron temp. pointer in vectors
;       tea()    = plasma electron temp. vector (K)
;                  1st dim: index of electron temperature
;       denpa()  = plasma H+ temp. vector (K)
;                  1st dim: index of electron temperature
;       denimpa()= plasma mean impurity ion temp (K)
;                  1st dim: index of electron temperature
;       nzef     = number of plasma zeff
;       iz_ref   = reference zeff pointer in vector
;       zefa()   = plasma zeff vector
;                  1st dim: index of zeff
;       nbeam    = number of beam energies
;       ib_ref   = reference beam energy pointer in vectors
;       bmena()  = beam energy vector (ev/amu)
;                  1st dim: index of beam energies
;       denha()  = beam H+ density vector (cm-3)
;                  1st dim: index of beam energies
;       bmfra()  = fractions of beam at each energy
;                  1st dim: index of beam energies
;       nimp     = number of plasma impurities (excl.h+)
;       im_ref   = reference impurity pointer in vectors
;       zimpa()  = impurity species charge
;                  1st dim: index of impurity
;       amimpa() = atomic mass number of impurity species
;                  1st dim: index of impurity
;       frimpa() = fraction of impurity (normalised to 1)
;                  1st dim: index of impurity
;       ts       = external radiation field temperature (K)
;       w        = general radiation dilution factor
;       w1       = external radiation field dilution factor
;                  for photo-ionisation form the ground level.
;       cion     = adjustment multiplier for ground ionis.
;       cpy      = adjustment multiplier for VR xsects.
;       nip      = range of delta n for IP xsects. (le.4)
;       intd     = order of Maxw. quad. for IP xsects.(le.3)
;       iprs     = 0  => default to VR xsects. beyond nip range
;                  1  => use PR xsects. beyond nip range
;       ilow     = 0  => no special low level data accessed
;                  1  => special low level data accessed
;       ionip    = 0 =>   no ion impact collisions included
;                  1 =>ion impact excit. and ionis. included
;       nionip   = range of delta n for ion impact
;                  excitation xsects.
;       ilprs    = 0 => default to vainshtein xsects.
;                  1 => use lodge-percival-richards xsects.
;       ivdisp   = 0 => ion impact at thermal Maxw. energies
;                  1 => ion impact at displaced thermal
;                       energies according to the neutral
;                       beam energy parameter
;                    * if(ivdisp=0 then special low level
;                      data for ion impact is not substituted -
;                      only vainshtein and lodge et al.
;                      options are open.  Electron impact
;                      data substitution does occur.
;       nmin     = lowest n-shell for population structure
;       nmax     = highest n-shell for population structure
;       imax     = number of representative n-shells
;       nrep()   = representative n-shells
;                  1st dim: index of representative n-shell
;       wbrep()  = dilution factors for nmin->nrep() trans.
;                  1st dim: index of representative n-shell
;       jdef     = number of n-shell quantum defects
;       def()    = quantum defects for n-shells
;                  1st dim: index of n-shell quantum defects
;                           upwards from nmin
;       jcor     = number of DR Bethe correction factors
;       cor()    = DR Bethe correction factors
;                  1st dim: index of correction factor
;       jmax     = number of DR core transitions
;       epsil()  = reduced energy of core transition
;                  [delta Eij/I_H=(z+1)^2*epsil()]
;                  1st dim: index of DR core transition
;       fij()    = absorption oscillator strength for
;                  DR core transition
;                  1st dim: index of DR core transition
;       wij()    = dilution factor for DR core transition
;                             1st dim: index of DR core transition
;
;
;
; AUTHOR     :  Martin O'Mullane
;
; DATE       :  15-06-2027
;
; UPDATE     :
;       1.1     Martin O'Mullane
;                 - First version.
;
; VERSION    :
;       1.1    15-06-2007
;-
;----------------------------------------------------------------------

PRO read_adf25, file     = file,      $
                fulldata = fulldata,  $
                help     = help


; If asked for help

if keyword_set(help) then begin
   doc_library, 'read_adf25'
   return
endif

; Check that we get all inputs and that they are correct. Otherwise print
; a message and return to command line

on_error, 2


; Input file name

if n_elements(file) eq 0 then message, 'A file name must be passed'
file_acc, file, exist, read, write, execute, filetype
if exist ne 1 then message, 'adf25 file does not exist '+file
if read ne 1 then message, 'adf25 file cannot be read from this userid '+file


; Return everything if fulldata variable is set

if arg_present(fulldata) then begin

   xxdata_25, file=file, fulldata=fulldata
   return

endif


END
