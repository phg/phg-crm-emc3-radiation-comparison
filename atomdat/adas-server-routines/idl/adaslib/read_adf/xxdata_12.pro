;----------------------------------------------------------------------
;+
; PROJECT    :  ADAS
;
; NAME       :  xxdata_12
;
; PURPOSE    :
;
; ARGUMENTS  :  All output arguments will be defined appropriately.
;               Inputs will be converted to correct type, if possible,
;               internally without changing calling type.
;
;                xxdata_12
;
;               NAME      I/O    TYPE    DETAILS
; REQUIRED   :  a12file    I     str     adf12 file name
;               nstore     I     long    maximum number of data blocks allowed
;
;               nbsel      O     long    number of blocks present
;               csymb()    O     str     element symbol
;               czion()    O     str     emitting ion charge
;               cwavel()   O     str     wavelength (A)
;               cdonor()   O     str     donor neutral atom
;               crecvr()   O     str     receiver nucleus
;               ctrans()   O     str     transition
;               cfile()    O     str     specific ion file source
;               ctype()    O     str     type of emissivity
;               cindm()    O     str     emissivity index
;               qefref()   O     double  reference value of rate coefficient
;               enref()    O     double      "       "   "  energy
;               teref()    O     double      "       "   "  temperature
;               deref()    O     double      "       "   "  density
;               zeref()    O     double      "       "   "  effective z
;               bmref()    O     double      "       "   "  magnetic field
;               nenera()   O     long    number of energies
;               ntempa()   O     long    number of temperatures
;               ndensa()   O     long    number of densities
;               nzeffa()   O     long    number of effective z's
;               nbmaga()   O     long    number of magnetic field values
;               enera(,)   O     double  energies
;               qenera(,)  O     double  rate coefficients for energy value
;               tempa(,)   O     double  temperatures
;               qtempa(,)  O     double  rate coefficients for temperatures
;               densa(,)   O     double  densities
;               qdensa(,)  O     double  rate coefficients for desnities
;               zeffa(,)   O     double  effective z
;               qzeffa(,)  O     double  rate coefficients for effective z
;               bmaga(,)   O     double  magnetic field
;               qbmaga(,)  O     double  rate coefficients for magnetic fields
;                                        1st dim: 12 or 24  depending on parameter
;                                        2nd dim: nstore
;
;
; NOTES      :  Calls fortran code.
;
;
; AUTHOR     :  Martin O'Mullane
;
; DATE       :  09/10/05
;
;
; MODIFIED:
;       1.1     Martin O'Mullane
;               - First version.
;       1.2     Martin O'Mullane
;               - New version of xxdata_12.
;
;
; VERSION:
;       1.1    09-10-2005
;       1.2    05-06-2007
;
;-
;----------------------------------------------------------------------

PRO xxdata_12,  a12file ,                                         $
                nstore  , nbsel  ,                                $
                csymb   , czion  , cwavel , cdonor , crecvr  ,    $
                ctrans  , cfile  , ctype  , cindm  ,              $
                qefref  ,                                         $
                enref   , teref  , deref  , zeref   , bmref  ,    $
                nenera  , ntempa , ndensa , nzeffa  , nbmaga ,    $
                enera   , tempa  , densa  , zeffa   , bmaga  ,    $
                qenera  , qtempa , qdensa , qzeffa  , qbmaga


; Check number of parameters

if n_params() NE 33 then message, 'Not all parameters present', /continue


; Check of mandatory inputs and convert to LONG type.

if nstore EQ 0 then message, 'Set maximum number of data blocks'
nstore = long(nstore)

; Check that the adf12 file exists

file_acc, a12file, exist, read, write, execute, filetype
if exist ne 1 then message, 'adf12 file does not exist ' + a12file
if read ne 1 then message, 'adf12 file cannot be read from this userid ' + a12file



; Define types of the output parameters - see xxdata_12 and xxdata_12_calc for details

ndtem  = 12
ndden  = 24
ndein  = 24
ndzef  = 12 
ndmag  = 12

iunit   = 67L
nbsel   = -1L
csymb   = strarr(nstore) + '--'
czion   = strarr(nstore) + '--'
cwavel  = strarr(nstore) + '--------'
cdonor  = strarr(nstore) + '------'
crecvr  = strarr(nstore) + '-----'
ctrans  = strarr(nstore) + '-------'
cfile   = strarr(nstore) + '----------'
ctype   = strarr(nstore) + '--'
cindm   = strarr(nstore) + '---'
qefref  = dblarr(nstore)
enref   = dblarr(nstore)
teref   = dblarr(nstore)
deref   = dblarr(nstore)
zeref   = dblarr(nstore)
bmref   = dblarr(nstore)
nenera  = lonarr(nstore)
ntempa  = lonarr(nstore)
ndensa  = lonarr(nstore)
nzeffa  = lonarr(nstore)
nbmaga  = lonarr(nstore)
enera   = dblarr(ndein,nstore)
tempa   = dblarr(ndtem,nstore)
densa   = dblarr(ndden,nstore)
zeffa   = dblarr(ndzef,nstore)
bmaga   = dblarr(ndmag,nstore)
qenera  = dblarr(ndein,nstore)
qtempa  = dblarr(ndtem,nstore)
qdensa  = dblarr(ndden,nstore)
qzeffa  = dblarr(ndzef,nstore)
qbmaga  = dblarr(ndmag,nstore)


file      = string(replicate(32B, 80))
strput, file, a12file, 0


; Combine string arrays into a byte array - 2D with max(str), nstore

str_vars = [csymb   , czion  , cwavel , cdonor , crecvr  ,    $
            ctrans  , cfile  , ctype  , cindm]


str_vars = byte(str_vars)
str_vars = long(str_vars)

ind = where(str_vars EQ 0)
sz  = size(str_vars)
sz  = sz[1]
ind = transpose( [[ind mod sz], [ind / sz]] )

str_vars[ind[0,*], ind[1,*]] = 32L


; Location of sources

fortdir = getenv('ADASFORT')

dummy = CALL_EXTERNAL(fortdir+'/xxdata_12_if.so','xxdata_12_if', $
                      iunit, nstore, nbsel, qefref, enref,       $
                      teref, deref, zeref, bmref, nenera,        $
                      ntempa, ndensa, nzeffa, nbmaga, enera,     $
                      tempa, densa, zeffa, bmaga, qenera,        $
                      qtempa, qdensa, qzeffa, qbmaga,            $
                      str_vars, file)

; Convert mangled string arrays and logicals back to calling parameter names

res = byte(str_vars)
res = string(res)

csymb[0:nbsel-1]   = res[       0:          nbsel-1]
czion[0:nbsel-1]   = res[  nstore:   nstore+nbsel-1]
cwavel[0:nbsel-1]  = res[2*nstore: 2*nstore+nbsel-1]
cdonor[0:nbsel-1]  = res[3*nstore: 3*nstore+nbsel-1]
crecvr[0:nbsel-1]  = res[4*nstore: 4*nstore+nbsel-1]
ctrans[0:nbsel-1]  = res[5*nstore: 5*nstore+nbsel-1]
cfile[0:nbsel-1]   = res[6*nstore: 6*nstore+nbsel-1]
ctype[0:nbsel-1]   = res[7*nstore: 7*nstore+nbsel-1]
cindm[0:nbsel-1]   = res[8*nstore: 8*nstore+nbsel-1]


END
