;----------------------------------------------------------------------
;+
; PROJECT    :  ADAS
;
; NAME       :  read_adf49
;
; PURPOSE    :  Reads adf49 universal scaled charge exchange cross section
;               files from the IDL command line.
;               Called from IDL using the syntax
;                  read_adf49, file=..., fulldata=
;
; ARGUMENTS  :  All output arguments will be defined appropriately.
;
;               NAME      I/O    TYPE    DETAILS
; REQUIRED   :  file       I     str     full name of ADAS adf49 file
; OPTIONAL      help       I             display help entry
;               fulldata   O      -      Structure containing all the
;                                        adf49 data.
;
; NOTES      :  Only the fulldata strucutre is returned. There is
;               no other sensible output.
;
;
; The fulldata structure is defined:
;
;       a49file  = file name of adf49 format read.
;       symbd    = donor ion element symbol
;       izd      = donor charge
;       indd     = donor state index
;       nenrgy   = number of energies
;       n        = scaled n shells
;       alpha    = alpha parameter
;       beta     = beta parameter
;       gamma    = gamma parameter for scaling n shells
;       delta    = delta parameter for scaling x sects
;       ltype    = l shell parameters
;       lparms   = type of L parms used (currently only 1)
;       enrgya   = scaled energies (eV/amu)
;       sigta    = scaled total cross section
;       signa    = scaled n shell cross section
;
; Cross section data in cm-2.
;
; AUTHOR     :  Martin O'Mullane
;
; DATE       :  05-07-2027
;
; UPDATE     :
;       1.1     Martin O'Mullane
;                 - First version.
;       1.2     Martin O'Mullane
;                - Calling arguments for xxdata_49 have changed.
;
; VERSION    :
;       1.1    05-07-2007
;       1.2    11-11-2018
;-
;----------------------------------------------------------------------

PRO read_adf49, file     = file,      $
                fulldata = fulldata,  $
                help     = help


; If asked for help

if keyword_set(help) then begin
   doc_library, 'read_adf49'
   return
endif

; Check that we get all inputs and that they are correct. Otherwise print
; a message and return to command line

on_error, 2


; Input file name

if n_elements(file) eq 0 then message, 'A file name must be passed'
file_acc, file, exist, read, write, execute, filetype
if exist ne 1 then message, 'adf49 file does not exist ' + file
if read ne 1 then message, 'adf49 file cannot be read from this userid ' + file


; Return everything if fulldata variable is set

if arg_present(fulldata) then begin

   xxdata_49, file=file, fulldata=fulldata

   return

endif

END
