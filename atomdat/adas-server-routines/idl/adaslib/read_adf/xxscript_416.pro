;----------------------------------------------------------------------
;+
; PROJECT    :  ADAS
;
; NAME       :  xxscript_416
;
; PURPOSE    :  Reads and ADAS416 script file and returns parent emissivity 
;               files, partition specification data lines and child output
;               emissivity file names in a structure.
;
; ARGUMENTS  :  All output arguments will be defined appropriately.
;               Inputs will be converted to correct type, if possible,
;               internally without changing calling type.
;
;               xxscript_416, file=file, fulldata=fulldata
;
;               NAME      I/O    TYPE    DETAILS
; REQUIRED   :  file       I     str     adas416 script file name
; OPTIONAL      fulldata   O     struc   data in the script file
;
; The fulldata structure is defined as:
;
;               filename     str     adas416 script file name
;               dsngcr_p     str     parent gcr template file name from script
;               dsna_p()     str     parent emissivity source file template
;                                       1st dim: emissivity type
;               lfilea_p()   int     1 => parent emissivity type exists
;                                    0 => file does not exist
;                                       1st dim: emissivity type
;               ipl_p        int     position of parent partition in iptnla
;               dsngcr_c     str     child gcr template file name from script
;               dsna_c()     str     child emissivity source file template
;                                       1st dim: emissivity type
;               lfilea_c()   int     1  => child emissivity type required
;                                    0 => file not required
;                                       1st dim: emissivity type
;               ipl_c        int     position of child partition in iptnla
;               nptnl        int     number of partition levels in block
;               nptn()       int     number of partitions in partition level
;                                       1st dim: number of partitions
;               nptnc(,)     int     number of components in partition
;                                       1st dim: number of partitions
;                                       2nd dim: number of partitions in level
;               iptnla()     int     partition level index
;                                       1st dim: number of partitions
;               iptna(,)     int     partition index
;                                       1st dim: number of partitions
;                                       2nd dim: number of partitions in level
;               iptnca(,,)   int     component index
;                                       1st dim: number of partitions
;                                       2nd dim: number of partitions in level
;                                       3rd dim: number of components in a partition
;               ncptn_stack  int      number of text lines in partition stack
;               cptn_stack() str     stack of partition text lines
;               lres         int     1 => script file resolved
;                                    0 => script file unresolved
;
;
; NOTES      :  Calls fortran code.
;
;
; AUTHOR     :  Martin O'Mullane
;
; DATE       :  05-04-2019
;
;
; MODIFIED:
;       1.1     Martin O'Mullane
;              	- First version.
;
;
; VERSION:
;       1.1    05-04-2019
;
;-
;----------------------------------------------------------------------

PRO xxscript_416, file=file, fulldata=fulldata

; Parameters for sizing

ifdimd  = 4L
ndptn   = 128L
ndptnl  = 4L
ndptnc  = 256L
ndstack = 40L


; Check that the adf01 file exists

file_acc, file, exist, read, write, execute, filetype
if exist ne 1 then message, 'script file does not exist ' + file
if read ne 1 then message, 'script file cannot be read from this userid ' + file

file_in = string(replicate(32B, 132))
strput, file_in, file, 0
by_file_in = long(byte(file_in))


; Define types of the output parameters

ipl_c       = -1L
ipl_p       = -1L
iptna       = lonarr(ndptnl,ndptn)
iptnca      = lonarr(ndptnl,ndptn,ndptnc)
iptnla      = lonarr(ndptnl)
ircode      = -1L
ncptn_stack = -1L
nptn        = lonarr(ndptnl)
nptnc       = lonarr(ndptnl,ndptn)
nptnl       = -1L
lfilea_c    = lonarr(ifdimd)
lfilea_p    = lonarr(ifdimd)
lres        = 0L

str = string(replicate(32b, 80))
by_cptn_stack = lonarr(ndstack,80) + transpose(rebin(long(byte(str)),80,ndstack))

str = string(replicate(32b, 120))
by_dsna_c = lonarr(ifdimd,120) + transpose(rebin(long(byte(str)),120,ifdimd))

str = string(replicate(32b, 120))
by_dsna_p = lonarr(ifdimd,120) + transpose(rebin(long(byte(str)),120,ifdimd))

str = string(replicate(32b, 120))
by_dsngcr_c = long(byte(str))

str = string(replicate(32b, 120))
by_dsngcr_p = long(byte(str))

str = string(replicate(32b, 12))
by_elem = long(byte(str))

str = string(replicate(32b, 2))
by_year = long(byte(str))


; Location of sources

fortdir = getenv('ADASFORT')

dummy = CALL_EXTERNAL(fortdir+'/xxscript_416_if.so','xxscript_416_if', $
                      by_file_in  ,                                    $
                      ifdimd      , ndstack    ,                       $
                      by_dsngcr_p , by_dsna_p  ,                       $
                      lfilea_p    , ipl_p      ,                       $
                      by_dsngcr_c , by_dsna_c  ,                       $
                      lfilea_c    , ipl_c      ,                       $
                      ndptnl      , ndptn      , ndptnc ,              $
                      nptnl       , nptn       , nptnc  ,              $
                      iptnla      , iptna      , iptnca ,              $
                      ncptn_stack , by_cptn_stack       ,              $
                      lres        , by_elem    , by_year)



cptn_stack = strarr(ndstack)
for j = 0, ndstack-1 do cptn_stack[j] = string(byte(reform(by_cptn_stack[j,*])))
cptn_stack = cptn_stack[0:ncptn_stack-1]

dsna_c = strarr(ifdimd)
dsna_p = strarr(ifdimd)
for j = 0, ifdimd-1 do begin
   dsna_c[j] = string(byte(reform(by_dsna_c[j,*])))
   dsna_p[j] = string(byte(reform(by_dsna_p[j,*])))
endfor

dsngcr_c = string(byte(by_dsngcr_c))
dsngcr_p = string(byte(by_dsngcr_p))
elem     = string(byte(by_elem))
year     = string(byte(by_year))

fulldata = { filename    :   file,                $
             dsngcr_p    :   strtrim(dsngcr_p,2), $
             dsna_p      :   strtrim(dsna_p,2),   $
             lfilea_p    :   lfilea_p,            $
             ipl_p       :   ipl_p ,              $
             dsngcr_c    :   strtrim(dsngcr_c,2), $
             dsna_c      :   strtrim(dsna_c,2),   $
             lfilea_c    :   lfilea_c,            $
             ipl_c       :   ipl_c,               $
             nptnl       :   nptnl,               $
             nptn        :   nptn,                $
             nptnc       :   nptnc,               $
             iptnla      :   iptnla,              $
             iptna       :   iptna,               $
             iptnca      :   iptnca,              $
             ncptn_stack :   ncptn_stack,         $
             cptn_stack  :   strtrim(cptn_stack), $
             lres        :   lres                 }


END
