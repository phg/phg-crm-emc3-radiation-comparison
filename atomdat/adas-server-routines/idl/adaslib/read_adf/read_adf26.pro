;----------------------------------------------------------------------
;+
; PROJECT    :  ADAS
;
; NAME       :  read_adf26
;
; PURPOSE    :  Reads adf26 (bundle-n, bundle-nl) population files from the 
;               IDL command line.
;               called from IDL using the syntax
;               read_adf26,file=...,
;
; ARGUMENTS  :  All output arguments will be defined appropriately.
;
;               NAME      I/O    TYPE   DETAILS
; REQUIRED   :  file       I     str    full name of ADAS adf09 file
; OPTIONAL      fulldata   O      -     Structure containing all the
;                                       adf26 details.
;
; KEYWORDS      help       I      -     if specified this comment
;                                       section is written to screen.
; 
; As there is a lot of data in the adf26 file the arrays are given as
; functions of beam energy, electron density, n-level (for H beams) and
; nl level (He beams) at the reference temperature (with _teref suffix)
; and as functions of Te, level and spin at the reference beam energy 
; and density (_te suffix). 
;
; With He beam the output quantities are further distinguished by spin
; via _s (singlet) and _t (triplet) suffices.    
;
;
; For H beams the output is:
; 
;    fulldata = { filename     :  file,           $    name of adf26 file
;                 beam         :  'H',            $    species
;                 te           :  te,             $    electron temperature (K)
;                 teref        :  teref,          $    reference Te
;                 dens         :  dens,           $    electron density (cm-3)
;                 densref      :  densref,        $    reference density
;                 energy       :  energy,         $    beam energy (eV/amu)
;                 eref         :  eref,           $    reference energy
;                 n1n_teref    :  n1n_teref,      $    
;                 n1n_te       :  n1n_te,         $    
;                 s_teref      :  s_teref,        $    
;                 s_te         :  s_te   ,        $    
;                 a_teref      :  a_teref,        $
;                 a_te         :  a_te   ,        $
;                 nrep         :  nrep   ,        $
;                 f1_teref     :  f1_teref,       $
;                 f2_teref     :  f2_teref,       $
;                 f3_teref     :  f3_teref,       $
;                 bc_teref     :  bc_teref,       $
;                 bn_teref     :  bn_teref,       $
;                 nn_teref     :  nn_teref,       $
;                 f1_te        :  f1_te,          $
;                 f2_te        :  f2_te,          $
;                 f3_te        :  f3_te,          $
;                 bc_te        :  bc_te,          $
;                 bn_te        :  bn_te,          $
;                 nn_te        :  nn_te           $
;                 nip          :  nip,            $    ion impact
;                 intd         :  intd,           $
;                 iprs         :  iprs,           $
;                 ilow         :  ilow,           $
;                 ionip        :  ionip,          $
;                 nionip       :  nionip,         $
;                 ilprs        :  ilprs,          $
;                 ivdisp       :  ivdisp,         $
;                 zeff         :  zeff,           $
;                 ts           :  ts,             $
;                 w            :  w,              $
;                 w1           :  w1              }
; 
;  For He beams the output is:
;        
;    fulldata = { filename     :  file,           $
;                 beam         :  'He',           $
;                 te           :  te,             $
;                 teref        :  teref,          $
;                 dens         :  dens,           $
;                 densref      :  densref,        $
;                 energy       :  energy,         $
;                 eref         :  eref,           $
;                 a_teref      :  a_teref,        $  
;                 gcr_teref    :  gcr_teref,      $
;                 a_te         :  a_te    ,       $ 
;                 gcr_te       :  gcr_te ,        $  
;                 nrep_s       :  nrep_s ,        $    
;                 lrep_s       :  lrep_s ,        $    
;                 ltrep_s      :  ltrep_s ,       $   
;                 f11_teref_s  :  f11_teref_s,    $
;                 f12_teref_s  :  f12_teref_s,    $
;                 f13_teref_s  :  f13_teref_s,    $
;                 f2_teref_s   :  f2_teref_s,     $ 
;                 bc_teref_s   :  bc_teref_s,     $ 
;                 bn_teref_s   :  bn_teref_s,     $ 
;                 nn_teref_s   :  nn_teref_s,     $ 
;                 f11_te_s     :  f11_te_s,       $
;                 f12_te_s     :  f12_te_s,       $
;                 f13_te_s     :  f13_te_s,       $
;                 f2_te_s      :  f2_te_s ,       $
;                 bc_te_s      :  bc_te_s ,       $
;                 bn_te_s      :  bn_te_s ,       $
;                 nn_te_s      :  nn_te_s,        $ 
;                 nrep_t       :  nrep_t,         $     
;                 lrep_t       :  lrep_t ,        $    
;                 ltrep_t      :  ltrep_t ,       $   
;                 f11_teref_t  :  f11_teref_t,    $
;                 f12_teref_t  :  f12_teref_t,    $
;                 f13_teref_t  :  f13_teref_t,    $
;                 f2_teref_t   :  f2_teref_t ,    $
;                 bc_teref_t   :  bc_teref_t ,    $
;                 bn_teref_t   :  bn_teref_t,     $ 
;                 nn_teref_t   :  nn_teref_t,     $ 
;                 f11_te_t     :  f11_te_t,       $
;                 f12_te_t     :  f12_te_t,       $
;                 f13_te_t     :  f13_te_t,       $
;                 f2_te_t      :  f2_te_t ,       $
;                 bc_te_t      :  bc_te_t,        $ 
;                 bn_te_t      :  bn_te_t,        $ 
;                 nn_te_t      :  nn_te_t,        $
;                 nip          :  nip,            $
;                 intd         :  intd,           $
;                 iprs         :  iprs,           $
;                 ilow         :  ilow,           $
;                 ionip        :  ionip,          $
;                 nionip       :  nionip,         $
;                 ilprs        :  ilprs,          $
;                 ivdisp       :  ivdisp,         $
;                 zeff         :  zeff,           $
;                 ts           :  ts,             $
;                 w            :  w,              $
;                 w1           :  w1              }
;
; NOTES      : IDL only and does not use fortran access routines. 
;              Just the data in the file is returned so no
;              interpolation possible.
;
; AUTHOR     :  Martin O'Mullane
; 
; DATE       :  08-11-2004
; 
; MODIFIED:
;       1.1     Martin O'Mullane
;               - First version.
;
; VERSION:
;       1.1    08-11-2004 
;-
;----------------------------------------------------------------------

PRO read_adf26, file      = file,         $
                fulldata  = fulldata,     $
                help      = help


; If asked for help

if keyword_set(help) then begin 
   doc_library, 'read_adf26' 
   return
endif



; Check that we get all inputs and that they are correct. Otherwise print
; a message and return to command line

on_error, 2


; file name

if n_elements(file) eq 0 then message, 'A file name must be passed'
file_acc, file, exist, read, write, execute, filetype
if exist ne 1 then message, 'adf09 file does not exist '+file
if read ne 1 then message, 'adf09 file cannot be read from this userid '+file


; Determine whether H or He beam

adas_readfile, file=file, all=all, nlines=nlines

res = where(strpos(all, 'MULTIPLICITY') NE -1, cnt)
if cnt NE 0 then is_he = 1 else is_he = 0


; Extract Te, dens and energy arrays and the reference values
; Data is organised teref, energy1, dens
;                   te, eref, densref 

ind     = where(strpos(all, '  TE = ') NE -1, cnt)
te      = double(strmid(all[ind],33,9))
teref   = te[0]
ind     = where(te NE teref)
indref  = ind[0]
te      = te[uniq(te,sort(te))]
nte     = n_elements(te)

ind     = where(strpos(all, '  NE = ') NE -1, cnt)
dens    = double(strmid(all[ind],33,9))
densref = dens[indref]
dens    = dens[uniq(dens,sort(dens))]
ndens   = n_elements(dens)

ind     = where(strpos(all, '  NH/NE = ') NE -1, cnt)
energy  = double(strmid(all[ind],10,9))
eref    = energy[indref]
energy  = energy[uniq(energy,sort(energy))]
nenergy = n_elements(energy)


; Option switches 

ind  = where(strpos(all, ' NIP   =') NE -1, cnt)
str  = all[ind[0]]
res  = strsplit(str, '[0-9]', /extract, /regex)
reads, strmid(str, strpos(str, 'NIP   =')+7, 3), nip   
reads, strmid(str, strpos(str, 'INTD  =')+7, 3), intd  
reads, strmid(str, strpos(str, 'IPRS  =')+7, 3), iprs  
reads, strmid(str, strpos(str, 'ILOW  =')+7, 3), ilow  
reads, strmid(str, strpos(str, 'IONIP =')+7, 3), ionip 
reads, strmid(str, strpos(str, 'NIONIP=')+7, 3), nionip
reads, strmid(str, strpos(str, 'ILPRS =')+7, 3), ilprs 
reads, strmid(str, strpos(str, 'IVDISP=')+7, 3), ivdisp

nip    = fix(nip)
intd   = fix(intd)
iprs   = fix(iprs)
ilow   = fix(ilow)
ionip  = fix(ionip)
nionip = fix(nionip)
ilprs  = fix(ilprs)
ivdisp = fix(ivdisp)


ind  = where(strpos(all, 'ZEFF  =') NE -1, cnt)
str  = all[ind[0]]
reads, strmid(str, strpos(str, 'ZEFF  =')+7, 4), zeff
reads, strmid(str, strpos(str, 'TS  =')+5, 10), ts
reads, strmid(str, strpos(str, 'W   =')+5, 10), w
reads, strmid(str, strpos(str, 'W1  =')+5, 10), w1



; Extract data and organise 
;    xx_teref(energy, dens, [level]) at teref
;    xx_te(te) at eref and densref


; Ionisation coefficient and N1/N+ ratio
; population factors
;
; - determine number of levels

if is_he EQ 0 then begin

   ind = where(strpos(all, 'CHARGE EXCHANGE OFF') NE -1, cnt)
   n1n = double(strmid(all[ind],30,12))
   a   = double(strmid(all[ind],58,12))
   s   = double(strmid(all[ind],97,12))

   n1n_teref = reform(n1n[0:nenergy*ndens-1], ndens, nenergy)
   n1n_te    = reform(n1n[nenergy*ndens:*])

   s_teref = reform(s[0:nenergy*ndens-1], ndens, nenergy)
   s_te    = reform(s[nenergy*ndens:*])

   a_teref = reform(a[0:nenergy*ndens-1], ndens, nenergy)
   a_te    = reform(a[nenergy*ndens:*])

   n1n_teref = transpose(n1n_teref, [1,0])
   s_teref   = transpose(s_teref, [1,0])
   a_teref   = transpose(a_teref, [1,0])
   
   ind = where(strpos(all, 'BN = F1*(N1/N+)') NE -1, cnt)
   i2  = ind[0]
   ind = where(strpos(all, 'B(ACTUAL)') NE -1, cnt)
   i1  = ind[0]

   nlevel = i2 - i1 - 4
   
   index = -1
   for j = 0, nte + nenergy*ndens - 1 do index = [index, indgen(nlevel)+ind[j]+1]
   index = index[1:*]
   data  = dblarr(8, (nte + nenergy*ndens)*nlevel)
   reads, all[index], data

   nrep     = reform(fix(data[1, 0:nlevel-1]))
   f1_teref = reform(data[2, 0:nenergy*ndens*nlevel - 1], nlevel, ndens, nenergy)
   f2_teref = reform(data[3, 0:nenergy*ndens*nlevel - 1], nlevel, ndens, nenergy)
   f3_teref = reform(data[4, 0:nenergy*ndens*nlevel - 1], nlevel, ndens, nenergy)
   bc_teref = reform(data[5, 0:nenergy*ndens*nlevel - 1], nlevel, ndens, nenergy)
   bn_teref = reform(data[6, 0:nenergy*ndens*nlevel - 1], nlevel, ndens, nenergy)
   nn_teref = reform(data[7, 0:nenergy*ndens*nlevel - 1], nlevel, ndens, nenergy)

   f1_te = reform(data[2, nenergy*ndens*nlevel:*], nlevel, nte)       
   f2_te = reform(data[3, nenergy*ndens*nlevel:*], nlevel, nte)       
   f3_te = reform(data[4, nenergy*ndens*nlevel:*], nlevel, nte)       
   bc_te = reform(data[5, nenergy*ndens*nlevel:*], nlevel, nte)       
   bn_te = reform(data[6, nenergy*ndens*nlevel:*], nlevel, nte)       
   nn_te = reform(data[7, nenergy*ndens*nlevel:*], nlevel, nte)       
   
   f1_te    = transpose(f1_te, [1,0])
   f2_te    = transpose(f2_te, [1,0])
   f3_te    = transpose(f3_te, [1,0])
   bc_te    = transpose(bc_te, [1,0])
   bn_te    = transpose(bn_te, [1,0])
   nn_te    = transpose(nn_te, [1,0])
   f1_teref = transpose(f1_teref, [2,1,0])
   f2_teref = transpose(f2_teref, [2,1,0])
   f3_teref = transpose(f3_teref, [2,1,0])
   bc_teref = transpose(bc_teref, [2,1,0])
   bn_teref = transpose(bn_teref, [2,1,0])
   nn_teref = transpose(nn_teref, [2,1,0])

   fulldata = { filename     :  file,           $
                beam         :  'H',            $
                te           :  te,             $
                teref        :  teref,          $
                dens         :  dens,           $
                densref      :  densref,        $
                energy       :  energy,         $
                eref         :  eref,           $
                n1n_teref    :  n1n_teref,      $
                n1n_te       :  n1n_te,         $
                s_teref      :  s_teref,        $
                s_te         :  s_te   ,        $
                a_teref      :  a_teref,        $
                a_te         :  a_te   ,        $
                nrep         :  nrep   ,        $
                f1_teref     :  f1_teref,       $
                f2_teref     :  f2_teref,       $
                f3_teref     :  f3_teref,       $
                bc_teref     :  bc_teref,       $
                bn_teref     :  bn_teref,       $
                nn_teref     :  nn_teref,       $
                f1_te        :  f1_te,          $
                f2_te        :  f2_te,          $
                f3_te        :  f3_te,          $
                bc_te        :  bc_te,          $
                bn_te        :  bn_te,          $
                nn_te        :  nn_te,          $
                nip          :  nip,            $
                intd         :  intd,           $
                iprs         :  iprs,           $
                ilow         :  ilow,           $
                ionip        :  ionip,          $
                nionip       :  nionip,         $
                ilprs        :  ilprs,          $
                ivdisp       :  ivdisp,         $
                zeff         :  zeff,           $
                ts           :  ts,             $
                w            :  w,              $
                w1           :  w1              }

endif else begin


   ind = where(strpos(all, 'EFFECTIVE IONISATION') NE -1, cnt)
   index = -1
   for j = 0, nte + nenergy*ndens - 1 do index = [index, indgen(3)+ind[j]+2]
   index = index[1:*]
   data  = dblarr(6, (nte + nenergy*ndens)*3)
   reads, all[index], data

   a_teref   = reform(data[2, 0:nenergy*ndens*3 - 1], 3, ndens, nenergy)
   gcr_teref = reform(data[3:5, 0:nenergy*ndens*3 - 1], 3, 3, ndens, nenergy)
   a_te      = reform(data[2, nenergy*ndens*3:*], 3, nte)
   gcr_te    = reform(data[3:5, nenergy*ndens*3:*], 3, 3, nte)

   a_teref   = transpose(a_teref, [2,1,0])
   gcr_teref = transpose(gcr_teref, [3,2,0,1])

   ; singlet
   
   ind = where(strpos(all, 'LEVELS OF MULTIPLICITY  3') NE -1, cnt)
   i2  = ind[0]
   ind = where(strpos(all, 'LEVELS OF MULTIPLICITY  1') NE -1, cnt)
   i1  = ind[0]

   nlevel = i2 - i1 - 2
   
   index = -1
   for j = 0, nte + nenergy*ndens - 1 do index = [index, indgen(nlevel)+ind[j]+2]
   index = index[1:*]
   data  = dblarr(11, (nte + nenergy*ndens)*nlevel)
   reads, all[index], data

   nrep_s      = reform(fix(data[1, 0:nlevel-1]))
   lrep_s      = reform(fix(data[2, 0:nlevel-1]))
   ltrep_s     = reform(fix(data[3, 0:nlevel-1]))
   f11_teref_s = reform(data[4, 0:nenergy*ndens*nlevel - 1], nlevel, ndens, nenergy)
   f12_teref_s = reform(data[5, 0:nenergy*ndens*nlevel - 1], nlevel, ndens, nenergy)
   f13_teref_s = reform(data[6, 0:nenergy*ndens*nlevel - 1], nlevel, ndens, nenergy)
   f2_teref_s  = reform(data[7, 0:nenergy*ndens*nlevel - 1], nlevel, ndens, nenergy)
   bc_teref_s  = reform(data[8, 0:nenergy*ndens*nlevel - 1], nlevel, ndens, nenergy)
   bn_teref_s  = reform(data[9, 0:nenergy*ndens*nlevel - 1], nlevel, ndens, nenergy)
   nn_teref_s  = reform(data[10, 0:nenergy*ndens*nlevel - 1], nlevel, ndens, nenergy)

   f11_te_s = reform(data[4, nenergy*ndens*nlevel:*], nlevel, nte)       
   f12_te_s = reform(data[5, nenergy*ndens*nlevel:*], nlevel, nte)       
   f13_te_s = reform(data[6, nenergy*ndens*nlevel:*], nlevel, nte)       
   f2_te_s  = reform(data[7, nenergy*ndens*nlevel:*], nlevel, nte)       
   bc_te_s  = reform(data[8, nenergy*ndens*nlevel:*], nlevel, nte)       
   bn_te_s  = reform(data[9, nenergy*ndens*nlevel:*], nlevel, nte)       
   nn_te_s  = reform(data[10, nenergy*ndens*nlevel:*], nlevel, nte)       
   
   f11_te_s    = transpose(f11_te_s, [1,0])
   f12_te_s    = transpose(f12_te_s, [1,0])
   f13_te_s    = transpose(f13_te_s, [1,0])
   f2_te_s     = transpose(f2_te_s, [1,0]) 
   bc_te_s     = transpose(bc_te_s, [1,0]) 
   bn_te_s     = transpose(bn_te_s, [1,0]) 
   nn_te_s     = transpose(nn_te_s, [1,0]) 
   f11_teref_s = transpose(f11_teref_s, [2,1,0])
   f12_teref_s = transpose(f12_teref_s, [2,1,0])
   f13_teref_s = transpose(f13_teref_s, [2,1,0])
   f2_teref_s  = transpose(f2_teref_s, [2,1,0])
   bc_teref_s  = transpose(bc_teref_s, [2,1,0])
   bn_teref_s  = transpose(bn_teref_s, [2,1,0])
   nn_teref_s  = transpose(nn_teref_s, [2,1,0])
   
   
   ; triplet
   
   
   ind = where(strpos(all, 'B   = F1(I)*(N1/N+) F1(II)*(N2/N+)') NE -1, cnt)
   i2  = ind[0]
   ind = where(strpos(all, 'LEVELS OF MULTIPLICITY  3') NE -1, cnt)
   i1  = ind[0]

   nlevel = i2 - i1 - 4
   
   index = -1
   for j = 0, nte + nenergy*ndens - 1 do index = [index, indgen(nlevel)+ind[j]+2]
   index = index[1:*]
   data  = dblarr(11, (nte + nenergy*ndens)*nlevel)
   reads, all[index], data

   nrep_t      = reform(fix(data[1, 0:nlevel-1]))
   lrep_t      = reform(fix(data[2, 0:nlevel-1]))
   ltrep_t     = reform(fix(data[3, 0:nlevel-1]))
   f11_teref_t = reform(data[4, 0:nenergy*ndens*nlevel - 1], nlevel, ndens, nenergy)
   f12_teref_t = reform(data[5, 0:nenergy*ndens*nlevel - 1], nlevel, ndens, nenergy)
   f13_teref_t = reform(data[6, 0:nenergy*ndens*nlevel - 1], nlevel, ndens, nenergy)
   f2_teref_t  = reform(data[7, 0:nenergy*ndens*nlevel - 1], nlevel, ndens, nenergy)
   bc_teref_t  = reform(data[8, 0:nenergy*ndens*nlevel - 1], nlevel, ndens, nenergy)
   bn_teref_t  = reform(data[9, 0:nenergy*ndens*nlevel - 1], nlevel, ndens, nenergy)
   nn_teref_t  = reform(data[10, 0:nenergy*ndens*nlevel - 1], nlevel, ndens, nenergy)

   f11_te_t = reform(data[4, nenergy*ndens*nlevel:*], nlevel, nte)       
   f12_te_t = reform(data[5, nenergy*ndens*nlevel:*], nlevel, nte)       
   f13_te_t = reform(data[6, nenergy*ndens*nlevel:*], nlevel, nte)       
   f2_te_t  = reform(data[7, nenergy*ndens*nlevel:*], nlevel, nte)       
   bc_te_t  = reform(data[8, nenergy*ndens*nlevel:*], nlevel, nte)       
   bn_te_t  = reform(data[9, nenergy*ndens*nlevel:*], nlevel, nte)       
   nn_te_t  = reform(data[10, nenergy*ndens*nlevel:*], nlevel, nte)       
   
   f11_te_t    = transpose(f11_te_t, [1,0])
   f12_te_t    = transpose(f12_te_t, [1,0])
   f13_te_t    = transpose(f13_te_t, [1,0])
   f2_te_t     = transpose(f2_te_t, [1,0]) 
   bc_te_t     = transpose(bc_te_t, [1,0]) 
   bn_te_t     = transpose(bn_te_t, [1,0]) 
   nn_te_t     = transpose(nn_te_t, [1,0]) 
   f11_teref_t = transpose(f11_teref_t, [2,1,0])
   f12_teref_t = transpose(f12_teref_t, [2,1,0])
   f13_teref_t = transpose(f13_teref_t, [2,1,0])
   f2_teref_t  = transpose(f2_teref_t, [2,1,0])
   bc_teref_t  = transpose(bc_teref_t, [2,1,0])
   bn_teref_t  = transpose(bn_teref_t, [2,1,0])
   nn_teref_t  = transpose(nn_teref_t, [2,1,0])


   fulldata = { filename     :  file,           $
                beam         :  'He',           $
                te           :  te,             $
                teref        :  teref,          $
                dens         :  dens,           $
                densref      :  densref,        $
                energy       :  energy,         $
                eref         :  eref,           $
                a_teref      :  a_teref,        $  
                gcr_teref    :  gcr_teref,      $
                a_te         :  a_te    ,       $ 
                gcr_te       :  gcr_te ,        $  
                nrep_s       :  nrep_s ,        $    
                lrep_s       :  lrep_s ,        $    
                ltrep_s      :  ltrep_s ,       $   
                f11_teref_s  :  f11_teref_s,    $
                f12_teref_s  :  f12_teref_s,    $
                f13_teref_s  :  f13_teref_s,    $
                f2_teref_s   :  f2_teref_s,     $ 
                bc_teref_s   :  bc_teref_s,     $ 
                bn_teref_s   :  bn_teref_s,     $ 
                nn_teref_s   :  nn_teref_s,     $ 
                f11_te_s     :  f11_te_s,       $
                f12_te_s     :  f12_te_s,       $
                f13_te_s     :  f13_te_s,       $
                f2_te_s      :  f2_te_s ,       $
                bc_te_s      :  bc_te_s ,       $
                bn_te_s      :  bn_te_s ,       $
                nn_te_s      :  nn_te_s,        $ 
                nrep_t       :  nrep_t,         $     
                lrep_t       :  lrep_t ,        $    
                ltrep_t      :  ltrep_t ,       $   
                f11_teref_t  :  f11_teref_t,    $
                f12_teref_t  :  f12_teref_t,    $
                f13_teref_t  :  f13_teref_t,    $
                f2_teref_t   :  f2_teref_t ,    $
                bc_teref_t   :  bc_teref_t ,    $
                bn_teref_t   :  bn_teref_t,     $ 
                nn_teref_t   :  nn_teref_t,     $ 
                f11_te_t     :  f11_te_t,       $
                f12_te_t     :  f12_te_t,       $
                f13_te_t     :  f13_te_t,       $
                f2_te_t      :  f2_te_t ,       $
                bc_te_t      :  bc_te_t,        $ 
                bn_te_t      :  bn_te_t,        $ 
                nn_te_t      :  nn_te_t,        $
                nip          :  nip,            $
                intd         :  intd,           $
                iprs         :  iprs,           $
                ilow         :  ilow,           $
                ionip        :  ionip,          $
                nionip       :  nionip,         $
                ilprs        :  ilprs,          $
                ivdisp       :  ivdisp,         $
                zeff         :  zeff,           $
                ts           :  ts,             $
                w            :  w,              $
                w1           :  w1              }
                
endelse

END
