;----------------------------------------------------------------------
;+
; PROJECT    :  ADAS
;
; NAME       :  read_adf23
;
; PURPOSE    :  Reads adf23 (ionisation) files from the IDL command line.
;               called from IDL using the syntax
;               read_adf23,file=...,...,te=... etc
;
; ARGUMENTS  :  All output arguments will be defined appropriately.
;
;               NAME      I/O    TYPE   DETAILS
; REQUIRED   :  file       I     str    full name of ADAS adf23 file
; OPTIONAL      fulldata   O     str    all of the data from xxdata_23 dataset.
;               szd_total  O     str    calculated total rates
;
; KEYWORDS      help       I      -     returns help
;
; The fulldata structure is defined as:
;
;    seq          =  iso-electronic sequence symbol
;    iz0          =  nuclear charge
;    iz           =  ionising ion charge
;    iz1          =  ionised ion charge (=iz+1)
;    ctype        =  adf23 file resol. ('ca', 'ls' or 'ic')
;    bwno_f       =  ionis. poten. of ionised ion (cm-1)
;    nlvl_f       =  number of levels of ionised ion
;    lmet_f       =  1 - ionised metastables marked
;                    0 -                     unmarked
;    lcstrg_f     =  1 - standard config strings for ionised ion states
;                    0 - cannot be determined
;    ia_f()       =   index of ionised ion levels
;                       (ndlev) : ionised ion level index
;    code_f()     =  met. or excit. DR parent marker (* or #)
;                       (ndlev) : ionised ion level index
;    cstrga_f()   =  ionised ion configuration strings
;                       (ndlev) : ionised ion level index
;    isa_f()      =  ionised ion level multiplicity
;                       (ndlev) : ionised ion level index
;    ila_f()      =  ionised ion total orb. ang. mom.
;                       (ndlev) : ionised ion level index
;    xja_f()      =  ionised ion level (stat wt-1)/2
;                       (ndlev) : ionised ion level index
;    wa_f()       =  ionised ion level wave number (cm-1)
;                       (ndlev) : ionised ion level index
;    nmet_f       =  number of ionised ion metastables
;    imeta_f()    =  pointers to ionised metastables in full ionised ion state list
;                       (ndlev) : ionised ion level index
;    bwno_i       =  ionis. poten. of ionising ion (cm-1)
;    nlvl_i       =  number of levels of ionising ion
;    lmet_i       =  1 - ionising metastables marked
;                    0 -                     unmarked
;    lcstrg_i     =  1 - standard config strings for ionising ion states
;                    0 - cannot be determined
;    ia_i()       =   index of ionising ion levels
;                       (ndlev) : ionising ion level index
;    code_i()     =  met. or excit. DR parent marker (* or #)
;                       (ndlev) : ionising ion level index
;    cstrga_i()   =  ionising ion configuration strings
;                       (ndlev) : ionising ion level index
;    isa_i()      =  ionising ion level multiplicity
;                       (ndlev) : ionising ion level index
;    ila_i()      =  ionising ion total orb. ang. mom.
;                       (ndlev) : ionising ion level index
;    xja_i()      =  ionising ion level (stat wt-1)/2
;                       (ndlev) : ionising ion level index
;    wa_i()       =  ionising ion level wave number (cm-1)
;                       (ndlev) : ionising ion level index
;    nmet_i       =  number of ionising ion metastables
;    imeta_i()    =  pointers to ionising metastables in full ionising ion state list
;                       (ndlev) : ionised ion level index
;    nte_ion()    =  number of temperatures for direct ionisation
;                    data for initial metastable block
;                       (ndmet) : ionising ion metastable index
;    tea_ion(,)   =  temperatures (K) for direct ionisation
;                    data for initial metastable blockdblarr
;                       (ndmet) : ionising ion metastable index
;                       (ndtem) : temperature index
;    lqred_ion()  =  1 - direct ionisation data line present for ionised ion state
;                    0 - missing
;                       (ndmet) : ionising ion metastable index
;                       (ndlev) : ionised ion state index
;    qred_ion(,,) =  reduced direct ionisation rate coefficients
;                       (ndmet) : ionising ion metastable index
;                       (ndlev) : ionised ion state index
;                       (ndtem) : temperature index
;    nf_a()       =  number of Auger ionised ion final states
;                       (ndmet) : ionising ion metastable index
;    indf_a(,)    =  Auger ionised ion final state
;                       (ndmet) : ionising ion metastable index
;                       (ndlev) : final state index
;    lyld_a(,)    =  1 - Auger data for ionising ion excited state present
;                    0 - No Auger data
;                       (ndmet) : ionising ion metastable index
;                       (ndlev) : initial state index
;    yld_a(,,)    =  Auger yields
;                       (ndmet) : ionising ion metastable index
;                       (ndlev) : ionising ion excited state index
;                       (ndlev) : ionised ion excited state index
;    nte_exc()    =  number of temperatures for excitation
;                       (ndmet) : ionising ion metastable index
;    tea_exc(,)   =  temperatures (K) for direct excitation data for
;                    initial metastable block
;                       (ndmet) : ionising ion metastable index
;                       (ndtem) : temperature index
;    lqred_exc(,) =  1 - direct excitation data line present for excited ion state
;                    0 - no data
;                       (ndmet) : ionising ion metastable index
;                       (ndlev) : ionising ion excited state index
;    qred_exc(,,) =  reduced excitation rate coefficients
;                       (ndmet) : ionising ion metastable index
;                       (ndlev) : ionising ion excited state index
;                       (ndtem) : temperature index
;    l_ion()      =  1 - ionisation data present for metastable
;                    0 - not present
;                       (ndmet) : ionising ion metastable index
;    l_aug()      =  1 - Auger data present for metastable
;                    0 - not present
;                       (ndmet) : ionising ion metastable index
;    l_exc()      =  1 - excitation data present for metastable
;                    0 - not present
;                       (ndmet) : ionising ion metastable index
;
; The szd_total structure is defined as:
;
;    q_ion(,,)    = direct ionisation rate coefficients multipler
;                       (ndmet) : ionising ion metastable index
;                       (ndlev) : ionised ion state index
;                       (ndtem) : temperature index
;    is_q_ion(,,) = scaling exponent for direct ionisation coefficient
;                       (ndmet) : ionising ion metastable index
;                       (ndlev) : ionised ion state index
;                       (ndtem) : temperature index
;    q_exc(,,)    = excited ionisation rate coefficients multipler
;                       (ndmet) : ionising ion metastable index
;                       (ndlev) : excited ionising ion state index
;                       (ndtem) : temperature index
;    is_q_exc(,,) = scaling exponent for excited ionisation coefficient
;                       (ndmet) : ionising ion metastable index
;                       (ndlev) : excited ionising ion state index
;                       (ndtem) : temperature index
;    qtot(,,)     = total ionisation rate multipler
;                       (ndmet) : ionising ion metastable index
;                       (ndlev) : ionised ion metastable index
;                       (ndtem) : temperature index
;    is_qtot(,,)  = scaling exponent for total ionisation coefficient
;                       (ndmet) : ionising ion metastable index
;                       (ndlev) : ionised ion metastable index
;                       (ndtem) : temperature index
;
;
;
; NOTES      :  This is part of a chain of programs - read_adf23.c and
;               readadf23.for are required.
;
; AUTHOR     :  Martin O'Mullane
;
; DATE       :  22-05-2008
;
; MODIFIED   :
;       1.1     Martin O'Mullane
;                 - First version
;
; VERSION    :
;       1.1    22-05-2008
;-
;----------------------------------------------------------------------

PRO read_adf23, file      = file,      $
                fulldata  = fulldata,  $
                szd_total = szd_total, $
                help      = help

; If asked for help

if keyword_set(help) then begin
   doc_library, 'read_adf23'
   return
endif

; Check that we get all inputs and that they are correct. Otherwise print
; a message and return to command line

on_error, 2

; file name

if n_elements(file) eq 0 then message, 'A file name must be passed'
file_acc, file, exist, read, write, execute, filetype
if exist ne 1 then message, 'File does not exist '+file
if read ne 1 then message, 'File cannot be read from this userid '+file


; If fulldata selected retrieve the data into a structure and return.

if arg_present(fulldata) then begin

   xxdata_23, file=file, fulldata=fulldata
   
   if arg_present(szd_total) then xxtotl_23, fulldata  = fulldata,  $
                                             szd_total = szd_total
   return
   
endif

; We only get to here if szd_total is requested but fulldata is not!

if arg_present(szd_total) then begin

   xxdata_23, file = file, fulldata = all
   xxtotl_23, fulldata = all, szd_total = szd_total
   
   return
   
endif

; Interpolation (to come later)

END
