;----------------------------------------------------------------------
;+
; PROJECT    :  ADAS
;
; NAME       :  read_adf56
;
; PURPOSE    :  Reads adf56 (ionisation promotion rules) files from the
;               IDL command line.
;               called from IDL using the syntax
;               read_adf56,file=...,fulldata=fulldata
;
; ARGUMENTS  :  All output arguments will be defined appropriately.
;
;               NAME      I/O    TYPE   DETAILS
; REQUIRED   :  file       I     str    full name of ADAS adf56 file
;               fulldata   O     str    Structure containing the
;                                        adf56 details.
; NOTES      :  
;
; The fulldata structure is defined:
; index                   :  index of ground configuration of each ion of element in adf56 file
; config                :  ground configuration for each ion of element
; n_el                   :  number of electrons for each ion of element 
; no_v_shl              :  number of shells to treat as valence shells. Max. 2relevant to relating ion and parent. 
; v1_shl                 :  first valence shell position in adf56 configuration specifications. 
; v2_shl                 :  second valence shell position in adf56 configuration specifications. zero if none defined.
; drct_eval_v           :  evaluate direct ionisation from the valence shell(s). 
; drct_eval_cl          :  evaluate direct ionisation from other non-valence (closed) shells.
; min_shl_cl            :  lowest closed shell to include (position in adf56 configuration specifications).
; exca_eval_v2          :  evaluate excitation/autoionisation from second valence shell if identified. 
; max_dn_v2             :  maximum change in v2 n-shell to be included.
; min_dn_v2             :  minimum change in v2 n-shell to be include. 
; max_dl_v2             :  maximum change in v2 l-shell to be included.
; min_dl_v2             :  minimum change in v2 l-shell to be include. 
; exca_eval_cl          :  evaluate excitation/autoionisation from other non-valence (closed) shells.
; max_dn_cl             :  maximum change in closed n-shell to be included.
; min_dn_cl             :  minimum change in closed n-shell to be included.
; max_dl_cl             :  maximum change in closed l-shell to be included.
; min_dl_cl             :  minimum change in closed l-shell to be included.
; exst_eval              :  evaluate ionisation from excited states.
; exst_adf00_prt        :  assume parent for building excited states is as present in the adf00 data set for the ion.
; exst_prt_hole_shl    :  specify position of shell in ground configuration to form parent if not from adf00 above. 
; max_n_exst            :  maximum n-shell for excited states to be included.
; max_l_exst            :  maximum l-shell for excited states to be included.
; drct_eval_exst_v     :  evaluate direct ionisation from excited state valence shells. 
; drct_eval_exst_cl    :  evaluate direct ionisation from excited state non-valence (closed) shells.
; exca_eval_exst_v     :  evaluate excitation/autoionisation for excited states from valence shells (v1 and v2 above).
; exca_eval_exst_cl    :  evaluate excitation/autoionisation for excited states from non-valence (closed) shells. 
;
; AUTHOR     :  Adam Foster
;
; DATE       :  21-01-2009
;
;
; MODIFIED:
;       
;               
;
; VERSION:
;      1.1      21-01-2009 
;-
;----------------------------------------------------------------------

pro read_adf56, file = file, fulldata = fulldata, help=help

if keyword_set(help) then begin
   doc_library, 'read_adf56'
   return
endif

;Open file to read
openr,lun,file,/get_lun


str=''
readf,lun,str

;error check:
if strupcase(strmid(str,1,5)) ne 'ADF56' then begin
  print, 'Error: file is not adf56 file'
  print, file
  stop
endif

;Read the maximum number of elements for each array (vector)
n_max = fix(strmid(str,31,3))
nel = fix(strmid(str,39,2))

n_index = CEIL(n_max/nel)

;Define the vectors (or arrays)

config            = strarr(n_max)
index             = intarr(n_max)
n_el              = intarr(n_max)
no_v_shl          = intarr(n_max)
v1_shl            = intarr(n_max)
v2_shl            = intarr(n_max)
drct_eval_v       = intarr(n_max)
drct_eval_cl      = intarr(n_max)
min_shl_cl        = intarr(n_max)
exca_eval_v2      = intarr(n_max)
max_dn_v2         = intarr(n_max)
min_dn_v2         = intarr(n_max)
max_dl_v2         = intarr(n_max)
min_dl_v2         = intarr(n_max)
exca_eval_cl      = intarr(n_max)
max_dn_cl         = intarr(n_max)
min_dn_cl         = intarr(n_max)
max_dl_cl         = intarr(n_max)
min_dl_cl         = intarr(n_max)
exst_eval         = intarr(n_max)
exst_adf00_prt    = intarr(n_max)
exst_prt_hole_shl = intarr(n_max)
max_n_exst        = intarr(n_max)
max_l_exst        = intarr(n_max)
drct_eval_exst_v  = intarr(n_max)
drct_eval_exst_cl = intarr(n_max)
exca_eval_exst_v  = intarr(n_max)
exca_eval_exst_cl = intarr(n_max)


;Get data in the defined vectors (or arrays)
for i = 0,n_index-1 do begin

   readf,lun,str
   
   ; check is final iteration, in case number of elements is less
   ; than in other iterations.
   IF (i EQ n_index-1) THEN nel = n_max-(i*nel)

   while (strmid(str,0,15) NE 'index    config') do begin
     readf, lun, str
   endwhile

   for j= 0,nel-1 do begin
     readf,lun,str
     index_p = fix(strmid(str,2,3))
     config_p = string(strmid(str,9,85))
      
     index[(i*nel)+j]=index_p
     config[(i*nel)+j]=config_p
   endfor

   while (strmid(str,0,5) NE 'index') do begin
     readf, lun, str
   endwhile
   

   fmt='(a18,'+STRCOMPRESS(STRING(nel),/REMOVE_ALL)+'0i6)'
   ftmp='(i6)'      

   n_el_p=intarr(nel)
   no_v_shl_p=intarr(nel)
   v1_shl_p=intarr(nel)
   v2_shl_p=intarr(nel)
   drct_eval_v_p=intarr(nel)
   drct_eval_cl_p=intarr(nel)
   min_shl_cl_p=intarr(nel)
   exca_eval_v2_p=intarr(nel)
   max_dn_v2_p=intarr(nel)
   min_dn_v2_p=intarr(nel)
   max_dl_v2_p=intarr(nel)
   min_dl_v2_p=intarr(nel)
   exca_eval_cl_p=intarr(nel)
   max_dn_cl_p=intarr(nel)
   min_dn_cl_p=intarr(nel)
   max_dl_cl_p=intarr(nel)
   min_dl_cl_p=intarr(nel)
   exst_eval_p=intarr(nel)
   exst_adf00_prt_p=intarr(nel)
   exst_prt_hole_shl_p=intarr(nel)
   max_n_exst_p=intarr(nel)
   max_l_exst_p=intarr(nel)
   drct_eval_exst_v_p=intarr(nel)  
   drct_eval_exst_cl_p=intarr(nel)
   exca_eval_exst_v_p=intarr(nel)
   exca_eval_exst_cl_p=intarr(nel)
   readf,lun,str,n_el_p,format=fmt
   readf,lun,str,no_v_shl_p,format=fmt
   readf,lun,str,v1_shl_p,format=fmt
   readf,lun,str,v2_shl_p,format=fmt
   readf,lun,str,drct_eval_v_p,format=fmt
   readf,lun,str,drct_eval_cl_p,format=fmt       
   readf,lun,str,min_shl_cl_p,format=fmt
   readf,lun,str,exca_eval_v2_p,format=fmt
   readf,lun,str,max_dn_v2_p,format=fmt
   readf,lun,str,min_dn_v2_p,format=fmt 
   readf,lun,str,max_dl_v2_p,format=fmt
   readf,lun,str,min_dl_v2_p,format=fmt
   readf,lun,str,exca_eval_cl_p,format=fmt
   readf,lun,str,max_dn_cl_p,format=fmt
   readf,lun,str,min_dn_cl_p,format=fmt
   readf,lun,str,max_dl_cl_p,format=fmt
   readf,lun,str,min_dl_cl_p,format=fmt
   readf,lun,str,exst_eval_p,format=fmt
   readf,lun,str,exst_adf00_prt_p,format=fmt
   readf,lun,str,exst_prt_hole_shl_p,format=fmt
   readf,lun,str,max_n_exst_par_p,format=fmt
   readf,lun,str,max_l_exst_p,format=fmt
   readf,lun,str,drct_eval_exst_v_p,format=fmt
   readf,lun,str,drct_eval_exst_cl_p,format=fmt
   readf,lun,str,exca_eval_exst_v_p,format=fmt
   readf,lun,str,exca_eval_exst_cl_p,format=fmt

; store data in arrays
   
   n_el[nel*i:(nel*(i+1))-1]              = n_el_p
   no_v_shl[nel*i:(nel*(i+1))-1]          = no_v_shl_p
   v1_shl[nel*i:(nel*(i+1))-1]            = v1_shl_p
   v2_shl[nel*i:(nel*(i+1))-1]            = v2_shl_p
   drct_eval_v[nel*i:(nel*(i+1))-1]       = drct_eval_v_p
   drct_eval_cl[nel*i:(nel*(i+1))-1]      = drct_eval_cl_p
   min_shl_cl[nel*i:(nel*(i+1))-1]        = min_shl_cl_p
   exca_eval_v2[nel*i:(nel*(i+1))-1]      = exca_eval_v2_p
   max_dn_v2[nel*i:(nel*(i+1))-1]         = max_dn_v2_p
   min_dn_v2[nel*i:(nel*(i+1))-1]         = min_dn_v2_p
   max_dl_v2[nel*i:(nel*(i+1))-1]         = max_dl_v2_p
   min_dl_v2[nel*i:(nel*(i+1))-1]         = min_dl_v2_p
   exca_eval_cl[nel*i:(nel*(i+1))-1]      = exca_eval_cl_p
   max_dn_cl[nel*i:(nel*(i+1))-1]         = max_dn_cl_p
   min_dn_cl[nel*i:(nel*(i+1))-1]         = min_dn_cl_p
   max_dl_cl[nel*i:(nel*(i+1))-1]         = max_dl_cl_p
   min_dl_cl[nel*i:(nel*(i+1))-1]         = min_dl_cl_p
   exst_eval[nel*i:(nel*(i+1))-1]         = exst_eval_p
   exst_adf00_prt[nel*i:(nel*(i+1))-1]    = exst_adf00_prt_p
   exst_prt_hole_shl[nel*i:(nel*(i+1))-1] = exst_prt_hole_shl_p
   max_n_exst[nel*i:(nel*(i+1))-1]        = max_n_exst_p
   max_l_exst[nel*i:(nel*(i+1))-1]        = max_l_exst_p
   drct_eval_exst_v[nel*i:(nel*(i+1))-1]  = drct_eval_exst_v_p
   drct_eval_exst_cl[nel*i:(nel*(i+1))-1] = drct_eval_exst_cl_p
   exca_eval_exst_v[nel*i:(nel*(i+1))-1]  = exca_eval_exst_v_p
   exca_eval_exst_cl[nel*i:(nel*(i+1))-1] = exca_eval_exst_cl_p
    
    
endfor

;Define the promotion rule structure

fulldata={ config              :  config            ,$               
           index               :  index             ,$               
           n_el                :  n_el              ,$               
           no_v_shl            :  no_v_shl          ,$               
           v1_shl              :  v1_shl            ,$               
           v2_shl              :  v2_shl            ,$               
           drct_eval_v         :  drct_eval_v       ,$               
           drct_eval_cl        :  drct_eval_cl      ,$               
           min_shl_cl          :  min_shl_cl        ,$               
           exca_eval_v2        :  exca_eval_v2      ,$               
           max_dn_v2           :  max_dn_v2         ,$               
           min_dn_v2           :  min_dn_v2         ,$               
           max_dl_v2           :  max_dl_v2         ,$               
           min_dl_v2           :  min_dl_v2         ,$               
           exca_eval_cl        :  exca_eval_cl      ,$               
           max_dn_cl           :  max_dn_cl         ,$               
           min_dn_cl           :  min_dn_cl         ,$               
           max_dl_cl           :  max_dl_cl         ,$               
           min_dl_cl           :  min_dl_cl         ,$               
           exst_eval           :  exst_eval         ,$               
           exst_adf00_prt      :  exst_adf00_prt    ,$               
           exst_prt_hole_shl   :  exst_prt_hole_shl ,$               
           max_n_exst          :  max_n_exst        ,$               
           max_l_exst          :  max_l_exst        ,$
           drct_eval_exst_v    :  drct_eval_exst_v  ,$               
           drct_eval_exst_cl   :  drct_eval_exst_cl ,$
           exca_eval_exst_v    :  exca_eval_exst_v  ,$
           exca_eval_exst_cl   :  exca_eval_exst_cl }







close,lun
free_lun,lun
   
end
