; Copyright (c) 2001, Strathclyde University.
;----------------------------------------------------------------------
;+
; PROJECT    :  ADAS
;
; NAME       :  read_adf08
;
; PURPOSE    :  Reads adf08 ((radiative recombination) files from the
;               IDL command line.
;               called from IDL using the syntax
;               read_adf07,file=...,parent=...,te=... etc
;
; ARGUMENTS  :  All output arguments will be defined appropriately.
;
;               NAME      I/O    TYPE   DETAILS
; REQUIRED   :  file       I     str    full name of ADAS adf08 file
;               parent     I     int    selected parent index
;               level      I     int    selected level index
;               te         I     real() temperatures requested
; OPTIONAL      data       O      -     recombination data
;               info       O      -     information in a structure
;                                       info.s_parent     = 2S+1 of parent
;                                       info.l_parent     = L of parent
;                                       info.x_parent     = stat weight of parent
;                                       info.bwno_parent  = ionisation potential of parent ion
;                                       info.cfg_parent   = configuration string of parent
;                                       info.s_level      = 2S+1 of level
;                                       info.l_level      = L of level
;                                       info.x_level      = stat weight of level
;                                       info.bwno_level   = ionisation potential of level
;                                       info.cfg_level    = configuration string of level
;
;               fulldata   O      -     Structure containing all the
;                                       adf08 details. If requested the
;                                       other options are not used.
;           
;                                           iz       :  Recombined ion charge
;                                           iz0      :  Nuclear charge
;                                           iz1      :  Recombining ion charge
;                                           nprnt    :  Total number of parents
;                                           nprnti   :  Number of parents which are initial parents
;                                           bwnp     :  Binding wave no. of ground parent (cm-1)
;                                           ipa      :  Number of parent energy levels
;                                           ispa     :  Multiplicity for parent level 'ipa()'
;                                                       Note: (ispa-1)/2 = quantum number (sp)
;                                           ilpa     :  Quantum number (lp) for parent level 'ipa()'
;                                           xjpa     :  Quantum number (jp) for parent level 'ipa()'
;                                           wpa      :  Energy relative to parent level 1 (cm-1)
;                                                       for parent level 'ipa()'
;                                           cstrpa   :  Nomencl./config. for parent level 'ipa()'
;                                           il       :  Number of energy levels (terms) of
;                                                       recombined ion
;                                           bwnr     :  Ionisation potential (cm-1) of lowest level
;                                                       of recombined ion
;                                           ia       :  Recombined ion energy level index number
;                                           isa      :  Multiplicity for recombined level 'ia()'
;                                           ila      :  Quantum number (l) for recombined level 'ia()'
;                                           xja      :  Quantum number (j) for recombined level 'ia()'
;                                           wa       :  Energy relative to recombined level 1 (cm-1)
;                                                       for recombined level 'ia()'
;                                           cstrga   :  Nomencl./config. for recombined ion level 'ia()'
;                                           iprti    :  Initial parent block index
;                                           isprti   :  Initial parent block term
;                                           tprti    :  Initial parent block spin multiplicity
;                                           radr     :  Term selective dielec. coeffts.(cm3 s-1)
;                                                          1st.dim: level index
;                                                          2nd.dim: initial parent index
;                                                          3rd.dim: temperature index
;                                           nte      :  Number of temperatures
;                                           tea      :  Electron temperatures (k)
;                                           lradr    :  1 => diel. present for level index
;                                                       0 => diel. not present for level index
;                                                          1st.dim: level index
;                                                          2nd.dim: initial parent index
;
;  KEYWORDS      
;               help       I      -     Display help entry
;
; NOTES      :  This is part of a chain of programs - read_adf08.c and
;               readadf08.for are required.
;
; AUTHOR     :  Martin O'Mullane
;
; DATE       :  28-02-2001
;
; MODIFIED:
;       1.1     Martin O'Mullane
;               - Forgot to return the energies of the levels.
;       1.2     Allan Whiteford
;               - Added /help entry
;       1.3     Martin O'Mullane
;               - Remove internal adf08 reading routine with call to xxdata_08.
;               - Change 'all' to 'fulldata' to bring in line with other routines.
;
; VERSION:
;       1.1    16-03-2001
;       1.2    07-04-2005
;       1.3    12-01-2011
;-
;----------------------------------------------------------------------




PRO read_adf08, file     =  file,     $
                parent   =  parent,   $
                level    =  level,    $
                te       =  te,       $
                data     =  data,     $
                info     =  info,     $
                fulldata =  fulldata, $
                help     =  help

; If asked for help

if keyword_set(help) then begin
   doc_library, 'read_adf08'
   return
endif

; Check that we get all inputs and that they are correct. Otherwise print
; a message and return to command line

on_error, 2



; file name

if n_elements(file) eq 0 then message, 'A file name must be passed'
file_acc, file, exist, read, write, execute, filetype
if exist ne 1 then message, 'adf08 file does not exist '+file
if read ne 1 then message, 'adf08 file cannot be read from this userid '+file

if arg_present(fulldata) then begin

   xxdata_08, file=file, fulldata=fulldata

   return

endif


; selection of parent and level

if n_elements(parent) eq 0 then message, 'A parent must be selected'
parsize=size(parent, /N_DIMENSIONS)
if parsize ne 0 then message,'Parent index cannot be an array'
partype=size(parent, /TYPE)
if (partype ne 2) and (partype ne 3) then begin
   message,'Parent index must be numeric'
endif else iparent = LONG(parent)

if n_elements(level) eq 0 then message, 'A level must be selected'
parsize=size(level, /N_DIMENSIONS)
if parsize ne 0 then message,'level index cannot be an array'
partype=size(level, /TYPE)
if (partype ne 2) and (partype ne 3) then begin
   message,'level index must be numeric'
endif else ilevel = LONG(level)



; temperatures

if n_elements(te) eq 0 then message, 'User requested temperatures are missing'

partype=size(te, /type)
if (partype lt 2) or (partype gt 5) then begin
   message,'Temperature must be numeric'
endif  else te = DOUBLE(te)



; set variables for call to C/fortran reading of data

itval = n_elements(te)
itval = LONG(itval)
data  = DBLARR(itval)

is_par   = LONG(0)
il_par   = LONG(0)
x_par    = DOUBLE(0)
bwno_par = DOUBLE(0)
cfg_par  = 'ABCDEFGHIJKLMNOPQR'

is_lev   = LONG(0)
il_lev   = LONG(0)
x_lev    = DOUBLE(0)
bwno_lev = DOUBLE(0)
cfg_lev  = 'ABCDEFGHIJKLMNOPQR'




fortdir = getenv('ADASFORT')

dummy = CALL_EXTERNAL(fortdir+'/read_adf08.so', 'read_adf08',      $
                      file, iparent, ilevel, itval, te, data,      $
                      is_par, il_par, x_par, bwno_par,             $
                      is_lev, il_lev, x_lev, bwno_lev,             $
                      cfg_par, cfg_lev)


; make up the info structure - if one exists get rid of it.

if n_elements(info) ne 0 then xyz = temporary(info)

info = { s_parent    : is_par,   $
         l_parent    : il_par,   $
         x_parent    : x_par,    $
         bwno_parent : bwno_par, $
         cfg_parent  : cfg_par,  $
         s_level     : is_lev,   $
         l_level     : il_lev,   $
         x_level     : x_lev,    $
         bwno_level  : bwno_lev, $
         cfg_level   : cfg_lev   }



END
