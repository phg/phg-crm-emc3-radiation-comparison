; Copyright (c) 2002, Strathclyde University.
;----------------------------------------------------------------------
;+
; PROJECT    :  ADAS
;
; NAME       :  xxdata_21
;
; PURPOSE    :  
;
; ARGUMENTS  :  All output arguments will be defined appropriately.
;               Inputs will be converted to correct type, if possible,
;               internally without changing calling type.
;
;
;               The argument list is the same as fortran except
;               that the file name is passed rather than a unit
;               number.
;
;               NAME      I/O    TYPE    DETAILS
; REQUIRED   :  a21file   I      str     adf21 file name
;               mxbe      I      long    maximum number of beam energies    
;               mxtd      I      long    maximum number of target densities  
;               mxtt      I      long    maximum number of target temperatures       
;               itz       O      long    target ion charge.                          
;               tsym      O      str     target ion element symbol.                  
;               beref     O      double  reference beam energy (eV/amu).                      
;               tdref     O      double  reference target density (cm-3).                   
;               ttref     O      double  reference target temperature (eV).               
;               svref     O      double  stopping coefft. at reference 
;                                        beam energy, target density and 
;                                        temperature (cm3 s-1).             
;               nbe       O      double  number of beam energies.                    
;               be()      O      double  beam energies(eV/amu).                              
;               ntdens    O      double  number of target densities.                 
;               tdens()   O      double  target densities(cm-3).                           
;               nttemp    O      double  number of target temperatures.              
;               ttemp()   O      double  target temperatures (eV).                        
;               svt()     O      double  stopping coefft. at reference beam 
;                                        energy and target density (cm3 s-1).                         
;               sved(,)   O      double  stopping coefft. at reference target        
;                                        temperature (cm3 s-1).                                
;
;
;
; NOTES      :  Calls fortran code.
;
;
; AUTHOR     :  Martin O'Mullane
;
; DATE       :  06/02/04
;
;
; MODIFIED:
;       1.1     Martin O'Mullane
;              	- First version.
;       1.2	Allan Whiteford
;		- Changed wrapper path to be just ADASFORT.
;
;
; VERSION:
;       1.1    06/02/04
;       1.2    10/08/04
;
;-
;----------------------------------------------------------------------

PRO xxdata_21, a21file, mxbe, mxtd, mxtt, itz,     $
               beref, tdref, ttref, svref, nbe,    $
               be, ntdens, tdens, nttemp, ttemp,   $
               svt, sved,  tsym

; Convert input parameters to correct type

mxbe = LONG(mxbe)
mxtd = LONG(mxtd)
mxtt = LONG(mxtt)

; Define types of the output parameters


itz    = -1L   
tsym   = '  '   
beref  = 0.0D0   
tdref  = 0.0D0    
ttref  = 0.0D0    
svref  = 0.0D0    
        
        
nbe    = -1L 
be     = dblarr(mxbe)
ntdens = -1L          
tdens  = dblarr(mxtd)
nttemp = -1L          
ttemp  = dblarr(mxtt)
svt    = dblarr(mxtt)
sved   = dblarr(mxbe,mxtd)
 
file   = string(replicate(32B, 80))
strput, file, a21file, 0

; Internal parameters

iunit = 67L

; Location of sources

fortdir = getenv('ADASFORT')
fortdir = fortdir

dummy = CALL_EXTERNAL(fortdir+'/xxdata_21_if.so','xxdata_21_if',  $
                      iunit, mxbe, mxtd, mxtt, itz,               $ 
                      beref, tdref, ttref, svref, nbe,            $
                      be, ntdens, tdens, nttemp, ttemp,           $
                      svt, sved, file, tsym)

END
