;----------------------------------------------------------------------
;+
; PROJECT    :  ADAS
;
; NAME       :  xxdata_23
;
; PURPOSE    :  Reads and ADF23 file into an IDL structure
;
; INPUTS     :  file - Fully path of ADF23 file to read
;
; OUTPUTS    :  fulldata: Structure containing contents of ADF23 file:
;
;
; The fulldata structure is defined as:
;
;    seq          =  iso-electronic sequence symbol
;    iz0          =  nuclear charge
;    iz           =  ionising ion charge
;    iz1          =  ionised ion charge (=iz+1)
;    ctype        =  adf23 file resol. ('ca', 'ls' or 'ic')
;    bwno_f       =  ionis. poten. of ionised ion (cm-1)
;    nlvl_f       =  number of levels of ionised ion
;    lmet_f       =  1 - ionised metastables marked
;                    0 -                     unmarked
;    lcstrg_f     =  1 - standard config strings for ionised ion states
;                    0 - cannot be determined
;    ia_f()       =   index of ionised ion levels
;                       (ndlev) : ionised ion level index
;    code_f()     =  met. or excit. DR parent marker (* or #)
;                       (ndlev) : ionised ion level index
;    cstrga_f()   =  ionised ion configuration strings
;                       (ndlev) : ionised ion level index
;    isa_f()      =  ionised ion level multiplicity
;                       (ndlev) : ionised ion level index
;    ila_f()      =  ionised ion total orb. ang. mom.
;                       (ndlev) : ionised ion level index
;    xja_f()      =  ionised ion level (stat wt-1)/2
;                       (ndlev) : ionised ion level index
;    wa_f()       =  ionised ion level wave number (cm-1)
;                       (ndlev) : ionised ion level index
;    nmet_f       =  number of ionised ion metastables
;    imeta_f()    =  pointers to ionised metastables in full ionised ion state list
;                       (ndlev) : ionised ion level index
;    bwno_i       =  ionis. poten. of ionising ion (cm-1)
;    nlvl_i       =  number of levels of ionising ion
;    lmet_i       =  1 - ionising metastables marked
;                    0 -                     unmarked
;    lcstrg_i     =  1 - standard config strings for ionising ion states
;                    0 - cannot be determined
;    ia_i()       =   index of ionising ion levels
;                       (ndlev) : ionising ion level index
;    code_i()     =  met. or excit. DR parent marker (* or #)
;                       (ndlev) : ionising ion level index
;    cstrga_i()   =  ionising ion configuration strings
;                       (ndlev) : ionising ion level index
;    isa_i()      =  ionising ion level multiplicity
;                       (ndlev) : ionising ion level index
;    ila_i()      =  ionising ion total orb. ang. mom.
;                       (ndlev) : ionising ion level index
;    xja_i()      =  ionising ion level (stat wt-1)/2
;                       (ndlev) : ionising ion level index
;    wa_i()       =  ionising ion level wave number (cm-1)
;                       (ndlev) : ionising ion level index
;    nmet_i       =  number of ionising ion metastables
;    imeta_i()    =  pointers to ionising metastables in full ionising ion state list
;                       (ndlev) : ionised ion level index
;    nte_ion()    =  number of temperatures for direct ionisation
;                    data for initial metastable block
;                       (ndmet) : ionising ion metastable index
;    tea_ion(,)   =  temperatures (K) for direct ionisation
;                    data for initial metastable blockdblarr
;                       (ndmet) : ionising ion metastable index
;                       (ndtem) : temperature index
;    lqred_ion()  =  1 - direct ionisation data line present for ionised ion state
;                    0 - missing
;                       (ndmet) : ionising ion metastable index
;                       (ndlev) : ionised ion state index
;    qred_ion(,,) =  reduced direct ionisation rate coefficients
;                       (ndmet) : ionising ion metastable index
;                       (ndlev) : ionised ion state index
;                       (ndtem) : temperature index
;    nf_a()       =  number of Auger ionised ion final states
;                       (ndmet) : ionising ion metastable index
;    indf_a(,)    =  Auger ionised ion final state
;                       (ndmet) : ionising ion metastable index
;                       (ndlev) : final state index
;    lyld_a(,)    =  1 - Auger data for ionising ion excited state present
;                    0 - No Auger data
;                       (ndmet) : ionising ion metastable index
;                       (ndlev) : initial state index
;    yld_a(,,)    =  Auger yields
;                       (ndmet) : ionising ion metastable index
;                       (ndlev) : ionising ion excited state index
;                       (ndlev) : ionised ion excited state index
;    nte_exc()    =  number of temperatures for excitation
;                       (ndmet) : ionising ion metastable index
;    tea_exc(,)   =  temperatures (K) for direct excitation data for
;                    initial metastable block
;                       (ndmet) : ionising ion metastable index
;                       (ndtem) : temperature index
;    lqred_exc(,) =  1 - direct excitation data line present for excited ion state
;                    0 - no data
;                       (ndmet) : ionising ion metastable index
;                       (ndlev) : ionising ion excited state index
;    qred_exc(,,) =  reduced excitation rate coefficients
;                       (ndmet) : ionising ion metastable index
;                       (ndlev) : ionising ion excited state index
;                       (ndtem) : temperature index
;    l_ion()      =  1 - ionisation data present for metastable
;                    0 - not present
;                       (ndmet) : ionising ion metastable index
;    l_aug()      =  1 - Auger data present for metastable
;                    0 - not present
;                       (ndmet) : ionising ion metastable index
;    l_exc()      =  1 - excitation data present for metastable
;                    0 - not present
;                       (ndmet) : ionising ion metastable index
;
;
; NOTES      :  Calls fortran code via C wrapper, see:
;                       xxdata_23_if.c
;                       xxdata_23_calc.for
;
; AUTHOR     :  Martin O'Mullane
;
; DATE       :  22-05-2008
;
; MODIFIED:
;       1.1     Martin O'Mullane
;               - First version.
;       1.2     Martin O'Mullane
;               - Pass filename to fortran as a lonarr of bytes.
;
; VERSION:
;       1.1    22-05-2008
;       1.2    09-05-2019
;
;-
;----------------------------------------------------------------------


PRO xxdata_23, file = file, fulldata = fulldata

if n_elements(file) EQ 0 then message,'Filename must be set'

; Convert filename to byte array

fileb = file
adas_string_pad, fileb, 120
dsnin = long(byte(fileb))

; Parameters for storage

ndlev  = 100L
ndmet  = 10L
ndtem  = 50L
ndtext = 80L

; Allocate space

seq       =  bytarr(2)
iz0       =  0L
iz        =  0L
iz1       =  0L
ctype     =  bytarr(2)
bwno_f    =  0.0D0
nlvl_f    =  0L
lmet_f    =  0L
lcstrg_f  =  0L
ia_f      =  lonarr(ndlev)
code_f    =  bytarr(1,ndlev)
cstrga_f  =  bytarr(28,ndlev)
isa_f     =  lonarr(ndlev)
ila_f     =  lonarr(ndlev)
xja_f     =  dblarr(ndlev)
wa_f      =  dblarr(ndlev)
nmet_f    =  0L
imeta_f   =  lonarr(ndmet)
bwno_i    =  0.0D0
nlvl_i    =  0L
lmet_i    =  0L
lcstrg_i  =  0L
ia_i      =  lonarr(ndlev)
code_i    =  bytarr(1,ndlev)
cstrga_i  =  bytarr(28,ndlev)
isa_i     =  lonarr(ndlev)
ila_i     =  lonarr(ndlev)
xja_i     =  dblarr(ndlev)
wa_i      =  dblarr(ndlev)
nmet_i    =  0L
imeta_i   =  lonarr(ndmet)
nte_ion   =  lonarr(ndmet)
tea_ion   =  dblarr(ndmet,ndtem)
lqred_ion =  lonarr(ndmet,ndlev)
qred_ion  =  dblarr(ndmet,ndlev,ndtem)
nf_a      =  lonarr(ndmet)
indf_a    =  lonarr(ndmet,ndlev)
lyld_a    =  lonarr(ndmet,ndlev)
yld_a     =  dblarr(ndmet,ndlev,ndlev)
nte_exc   =  lonarr(ndmet)
tea_exc   =  dblarr(ndmet,ndtem)
lqred_exc =  lonarr(ndmet,ndlev)
qred_exc  =  dblarr(ndmet,ndlev,ndtem)
l_ion     =  lonarr(ndmet)
l_aug     =  lonarr(ndmet)
l_exc     =  lonarr(ndmet)
ntext     =  0L
ctext     =  bytarr(80, ndtext)

; Get Path

adasfort = getenv('ADASFORT')
if adasfort eq '' then message, 'Cannot locate ADAS binaries'

; Call wrapper routine

res = call_external(adasfort+'/xxdata_23_if.so','xxdata_23',       $
                    dsnin,                                         $
                    ndlev    , ndmet     , ndtem     , ndtext   ,  $
                    seq      , iz0       , iz        , iz1      ,  $
                    ctype    ,                                     $
                    bwno_f   , nlvl_f    , lmet_f    , lcstrg_f ,  $
                    ia_f     , code_f    , cstrga_f  ,             $
                    isa_f    , ila_f     , xja_f     , wa_f     ,  $
                    nmet_f   , imeta_f   ,                         $
                    bwno_i   , nlvl_i    , lmet_i    , lcstrg_i ,  $
                    ia_i     , code_i    , cstrga_i  ,             $
                    isa_i    , ila_i     , xja_i     , wa_i     ,  $
                    nmet_i   , imeta_i   ,                         $
                    nte_ion  , tea_ion   , lqred_ion , qred_ion ,  $
                    nf_a     , indf_a    , lyld_a    , yld_a    ,  $
                    nte_exc  , tea_exc   , lqred_exc , qred_exc ,  $
                    l_ion    , l_aug     , l_exc     ,             $
                    ntext    , ctext                               $
                   )

; Convert output back to strings

seq       =  string(seq)
ctype     =  string(ctype)
code_f    =  string(code_f)
cstrga_f  =  string(cstrga_f)
code_i    =  string(code_i)
cstrga_i  =  string(cstrga_i)
ctext     =  string(ctext)

; Return the valid stuff

if arg_present(fulldata) then begin

   if total(l_exc) GT 0 then begin

      qred_exc_out  =  qred_exc[0:nmet_i-1,0:nlvl_i-1,0:max(nte_exc)-1]
      tea_exc_out   =  tea_exc[0:nmet_i-1,0:max(nte_exc)-1]

   endif else begin

      qred_exc_out  =  -1
      tea_exc_out   =  -1

   endelse

   fulldata = { seq       :  seq                                               , $
                iz0       :  iz0                                               , $
                iz        :  iz                                                , $
                iz1       :  iz1                                               , $
                ctype     :  ctype                                             , $
                bwno_f    :  bwno_f                                            , $
                nlvl_f    :  nlvl_f                                            , $
                lmet_f    :  lmet_f                                            , $
                lcstrg_f  :  lcstrg_f                                          , $
                ia_f      :  ia_f[0:nlvl_f-1]                                  , $
                code_f    :  code_f[0:nlvl_f-1]                                , $
                cstrga_f  :  cstrga_f[0:nlvl_f-1]                              , $
                isa_f     :  isa_f[0:nlvl_f-1]                                 , $
                ila_f     :  ila_f[0:nlvl_f-1]                                 , $
                xja_f     :  xja_f[0:nlvl_f-1]                                 , $
                wa_f      :  wa_f[0:nlvl_f-1]                                  , $
                nmet_f    :  nmet_f                                            , $
                imeta_f   :  imeta_f[0:nmet_f-1]                               , $
                bwno_i    :  bwno_i                                            , $
                nlvl_i    :  nlvl_i                                            , $
                lmet_i    :  lmet_i                                            , $
                lcstrg_i  :  lcstrg_i                                          , $
                ia_i      :  ia_i[0:nlvl_i-1]                                  , $
                code_i    :  code_i[0:nlvl_i-1]                                , $
                cstrga_i  :  cstrga_i[0:nlvl_i-1]                              , $
                isa_i     :  isa_i[0:nlvl_i-1]                                 , $
                ila_i     :  ila_i[0:nlvl_i-1]                                 , $
                xja_i     :  xja_i[0:nlvl_i-1]                                 , $
                wa_i      :  wa_i[0:nlvl_i-1]                                  , $
                nmet_i    :  nmet_i                                            , $
                imeta_i   :  imeta_i[0:nmet_i-1]                               , $
                nte_ion   :  nte_ion[0:nmet_i-1]                               , $
                tea_ion   :  tea_ion[0:nmet_i-1,0:max(nte_ion)-1]              , $
                lqred_ion :  lqred_ion[0:nmet_i-1,0:nlvl_f-1]                  , $
                qred_ion  :  qred_ion[0:nmet_i-1,0:nlvl_f-1,0:max(nte_ion)-1]  , $
                nf_a      :  nf_a[0:nmet_i-1]                                  , $
                indf_a    :  indf_a[0:nmet_i-1,0:nlvl_i-1]                     , $
                lyld_a    :  lyld_a[0:nmet_i-1,0:nlvl_i-1]                     , $
                yld_a     :  yld_a[0:nmet_i-1,0:nlvl_i-1,0:nlvl_f-1]           , $
                nte_exc   :  nte_exc[0:nmet_i-1]                               , $
                tea_exc   :  tea_exc_out                                       , $
                lqred_exc :  lqred_exc[0:nmet_i-1,0:nlvl_i-1]                  , $
                qred_exc  :  qred_exc_out                                      , $
                l_ion     :  l_ion[0:nmet_i-1]                                 , $
                l_aug     :  l_aug[0:nmet_i-1]                                 , $
                l_exc     :  l_exc[0:nmet_i-1]                                 }

endif

END
