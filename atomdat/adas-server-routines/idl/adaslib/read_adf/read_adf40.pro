;----------------------------------------------------------------------
;+
; PROJECT    :  ADAS
;
; NAME       :  read_adf40
;
; PURPOSE    :  Reads adf40 (FPEC) files from the IDL command line.
;               called from IDL using the syntax
;               read_adf40, file=..., block=..., te=... etc
;
; ARGUMENTS  :  All output arguments will be defined appropriately.
;
;               NAME      I/O    TYPE   DETAILS
; REQUIRED   :  file       I     str    full name of ADAS adf40 file
;               block      I     int    selected block
;               te         I     real() temperatures requested
;               dens       I     real() densities requested
; OPTIONAL      data       O      -     FPEC data
;               wave       O     real() wavelength vector of selected block
;               iz0        O     int    return guess of nuclear charge
;                                       (returns -1 if unable to guess)
;               izz        O     int    return guess of ion charge
;                                       (returns -1 if unable to guess)
;               iz1        O     int    return guess of ion charge+1
;                                       (returns -1 if unable to guess)
;               fulldata   O     str    all of the data from xxdata_40.
;                                        a40file : filename
;                                        esym    : Element
;                                        iz0     : atomic number
;                                        iz      : ion charge
;                                        iz1     : recombined ion charge
;                                        ndens   : number of densities (dim - # blocks)
;                                        nte     :           temperatures
;                                        npix    :           pixels
;                                        dens    : densities (dim - dens, block)
;                                        te      : temperatures
;                                        wmin    : min wavelengths (dim - #blocks)
;                                        wmax    : max
;                                        ctype   : excitation type
;                                        cindm   : metastable
;                                        fpec    : feature emissivities (dim - pix, Te, Ne, blocks)
;                                          As different dimension per block are
;                                          possible so multi-dimension arrays
;                                          use the max value.
;
;
; KEYWORDS      all        I      -     if specified data is 2D of
;                                       temperature and density.
;               kelvin     I      -     requested temperature in K (default eV)
;               help       I      -     prints help to screen
;
; NOTES      :  This is part of a chain of programs - read_adf40.c and
;               readadf40.for are required.
;
; AUTHOR     :  Martin O'Mullane
;
; DATE       :  25-11-2004
;
; MODIFIED   :
;       1.1     Martin O'Mullane
;                 - First version.
;       1.2     Martin O'Mullane
;                 - Use the re-written version of xxdata_40.
;       1.3     Martin O'Mullane
;                 - Pass filename as a byte array.
;       1.4     Martin O'Mullane
;                 - Change names of xxdata_40 arguments to standard form.
;       1.5     Martin O'Mullane
;                 - Extend maximum length of file to 132 characters.
;
; VERSION    :
;       1.1    25-11-2004
;       1.2    14-02-2007
;       1.3    06-07-2007
;       1.4    30-07-2018
;       1.5    01-08-2018
;-
;----------------------------------------------------------------------



PRO read_adf40, file      = file,     $
                block     = block,    $
                te        = te,       $
                dens      = dens,     $
                data      = data,     $
                wave      = wave,     $
                kelvin    = kelvin,   $
                iz0       = iz0,      $
                izz       = izz,      $
                iz1       = iz1,      $
                all       = all,      $
                fulldata  = fulldata, $
                help      = help


; If asked for help

if keyword_set(help) then begin
   doc_library, 'read_adf40'
   return
endif


; Check that we get all inputs and that they are correct. Otherwise print
; a message and return to command line

; on_error, 2



; file name

if n_elements(file) eq 0 then message, 'A file name must be passed'
file_acc, file, exist, read, write, execute, filetype
if exist ne 1 then message, 'FPEC file does not exist '+file
if read ne 1 then message, 'FPEC file cannot be read from this userid '+file


; If all of the data required put it in fulldata and return

if arg_present(fulldata) then begin

   xxdata_40, file = file, fulldata=fulldata
   return

endif

; selection block

if n_elements(block) eq 0 then message, 'A FPEC index must be selected'
parsize=size(block, /N_DIMENSIONS)
if parsize ne 0 then message,'Selection index cannot be an array'
partype=size(block, /TYPE)
if (partype ne 2) and (partype ne 3) then begin
   message,'Selection index must be numeric'
endif else ibsel = LONG(block)



; temperature and density

if n_elements(te) eq 0 then message, 'User requested temperatures are missing'
if n_elements(dens) eq 0 then message, 'User requested densities are missing'

partype=size(te, /type)
if (partype lt 2) or (partype gt 5) then begin
   message,'Temperature must be numeric'
endif  else te = DOUBLE(te)

partype=size(dens, /type)
if (partype lt 2) or (partype gt 5) then begin
   message,'densmperature must be numeric'
endif else dens = DOUBLE(dens)

; Kelvin rather than eV

if keyword_set(kelvin) then te = te/11605.0


; Get the data by calling the fortran routine

; Do we want a 2D array (/all set) or 1D on (te, dens) vector

len_te   = n_elements(te)
len_dens = n_elements(dens)

if keyword_set(all) then begin

   ; make a big 1D list

   t_in = dblarr(len_te*len_dens)
   d_in = dblarr(len_te*len_dens)

   k = 0
   for i = 0, len_te-1 do begin
      for j = 0, len_dens-1 do begin
         t_in[k] = te[i]
         d_in[k] = dens[j]
         k = k + 1
       endfor
    endfor
    itval = k


endif else begin

   ; are te and dens the same length and one dimensional?
   ; if so define data array

   if len_dens ne len_te then print, 'TE and DENS size mismatch - smaller  used'
   itval = min([len_te,len_dens])

   t_in   = te
   d_in   = dens

endelse

t_in = double(t_in)
d_in = double(d_in)

itval = LONG(itval)


fortdir = getenv('ADASFORT')


; Get data in blocks of 30 to avoid altering xxspln for large sets.

data  = 0.0D0
nvpix = -1L
wvmin = 0.0D0
wvmax = 0.0D0

MAXVAL = 30L

nstore  = 10L
ndpix   = 1024L
ntdim   = 40L
nddim   = 50L
ndptnl  = 4L
ndptn   = 128L
ndptnc  = 256L
ndcnct  = 100L
ndstack = 40L
ndcmt   = 2000L
ntmax   = MAXVAL

dsn = file
adas_string_pad, dsn, 132
dsnin = byte(dsn)

n_call = numlines(itval, MAXVAL)

for j = 0L, n_call - 1 do begin

  ist = j*MAXVAL
  ifn = min([(j+1)*MAXVAL,itval])-1

  t_ca     = t_in[ist:ifn]
  d_ca     = d_in[ist:ifn]
  data_ca  = dblarr(ndpix, ifn-ist+1)
  itval_ca = ifn-ist+1

  dummy = CALL_EXTERNAL(fortdir+'/read_adf40.so','read_adf40',  $
                        nstore, ndpix, ntdim, nddim,            $
                        ndptnl, ndptn, ndptnc, ndcnct,          $
                        ndstack, ndcmt, ntmax,                  $
                        dsnin, ibsel,                           $
                        itval_ca, t_ca, d_ca, data_ca,          $
                        nvpix, wvmin, wvmax)

  data = [data, reform(data_ca, ndpix*(ifn-ist+1))]

endfor
data = data[1:*]

; Reconstruct the data as (pixel, itval)

data = reform(data, ndpix, itval)
data = data[0:nvpix-1, *]

; Generate the wavelength from the min/max values

wave = adas_vector(low=wvmin, high=wvmax, num=nvpix, /linear)


if keyword_set(all) then begin

   d_out = dblarr(nvpix, len_te, len_dens)
   k = 0
   for i = 0, len_te-1 do begin
      for j = 0, len_dens-1 do begin
         d_out[*,i,j] = data[*,k]
         k = k + 1
      endfor
   endfor

   data = d_out

endif

; Reset temperature to K

if keyword_set(kelvin) then te = te*11605.0



; Try to guess iz0, iz and iz1 if they were requested.
; Do this even if they weren't asked for since the keyword_set
; function will return zero if the variables weren't initalised
; outwith the call.

toparse = ''
openr, unit, file, /get_lun
readf, unit, toparse
free_lun, unit
toparse = strupcase(toparse)

pos1 = strpos(toparse,'+')

if pos1 eq -1 then begin

   iz0 = -1
   izz = -1
   iz1 = -1

endif else begin

   pos2     = strpos(toparse,'/', pos1, /reverse_search)
   elemstr  = strtrim(strmid(toparse,pos2+1,pos1-pos2-1),2)

   iz0 = xxeiz0(elemstr)
   if iz0 eq 0 then iz0 = -1

   pos2  = strpos(toparse,'P',pos1)
   izstr = strtrim(strmid(toparse,pos1+1,pos2-pos1-1),2)
   izz   = 0
   iz1   = -1
   for i = 0, strlen(izstr)-1 do begin
      if strmid(izstr,i,1) lt '0' or strmid(izstr,i,1) gt '9' then izz=-1
   end
   if izz ne -1 then iz1=1+((izz=fix(izstr)))

endelse

END
