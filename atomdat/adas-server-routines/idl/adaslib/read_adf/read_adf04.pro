;----------------------------------------------------------------------
;+
; PROJECT    :  ADAS
;
; NAME       :  read_adf04
;
; PURPOSE    :  Reads adf04 specific ion files from IDL
;               Called from IDL using the syntax
;                  read_adf04,file=..., te=... etc
;
; ARGUMENTS  :  All output arguments will be defined appropriately.
;
;               NAME      I/O    TYPE    DETAILS
; REQUIRED   :  file       I     str     full name of ADAS adf04 file
; OPTIONAL      fulldata   O      -      Structure containing the
;                                        adf04 details. If requested the
;                                        other options are not used.
;               te         I     float   Temperatures for interpolation.
;               lower      I     integer lower index of requested transition
;               upper      I     integer upper index of requested transition
;               gamma      O     float   effective collision strength of
;                                        the requested transition.
;               rate       O     float   Excitation rate at Te (cm3/s)
;               dex_rate   O     float   De-excitation rate at Te (cm3/s)
;               x          O     float   Burgess-Tully x parameter
;               y          O     float   Burgess-Tully y parameter
;               cfactor    I     float   Burgess-Tully C factor (default 1.5)
;               all        O       -     Deprecated - is now fulldata.
;
; KEYWORDS      limitpoint         -     if specified add the limit point
;                                        to the x and y Burgess-Tully
;                                        parameters.
;               ecipcalc           -     if specified add the ECIP ionisation
;                                        from excited states approximation to
;                                        the 'fulldata' structure. Note if
;                                        S-lines are present their rates
;                                        will be used.
;               burgess_tully      -     Interpolate in Burgess-Tully space.
;               ignore_untied      -     Ignore untied levels when reading adf04
;               noproc             -     Return raw results for transition processes
;               nosplit            -     Don't split data into different
;                                        process types.
;               kelvin             -     Requested temperature are in K (default eV)
;               help               -     if specified this comment
;                                        section is written to screen.
;
;
; NOTES      :  This version reads the data using the xxdata_04.for
;               routine. Glue C code is required.
;
;               Recombination, charge exchange and ionisation data
;               are returned if 'fulldata' is requested.
;
;               Burgess-Tully transformations taken from Astro. & Astrophys.
;               vol 254, p436-453 (1992).
;
; The fulldata structure is defined:
;
;     fulldata = { filename         :  file,              $
;                  iz               :  iz,                $
;                  iz0              :  iz0,               $
;                  ia               :  ia,                $
;                  cstrga           :  cstrga,            $
;                  isa              :  isa,               $
;                  ila              :  ila,               $
;                  xja              :  xja,               $
;                  wa               :  wa,                $
;                  zpla             :  zpla,              $
;                  ipla             :  ipla,              $
;                  bwno             :  bwno,              $
;                  bwnoa            :  bwnoa,             $
;                  te               :  scef,              $
;                  lower            :  lower_set,         $
;                  upper            :  upper_set,         $
;                  aval             :  aval_set,          $
;                  gamma            :  gamma_set,         $
;                  level_rec        :  level_rec,         $
;                  parent_rec       :  parent_rec,        $
;                  rec              :  rec,               $
;                  level_ion        :  level_ion,         $
;                  parent_ion       :  parent_ion,        $
;                  ion              :  ion,               $
;                  level_ion_rate   :  level_ion_rate,    $
;                  parent_ion_rate  :  parent_ion_rate,   $
;                  ion_rate         :  ion_rate,          $
;                  ion_source       :  source_rate,       $
;                  level_cx         :  level_cx,          $
;                  parent_cx        :  parent_cx,         $
;                  cx               :  cx                 }
;
;
; AUTHOR     :  Martin O'Mullane
;
; DATE       :  07-02-2001
;
;
; MODIFIED:
;       1.2     Martin O'Mullane
;               - Fixed an indexing problem in Burgess-Tully calculation.
;               - Add an optional user supplied C-factor.
;               - Add an optional extension to limit points.
;       1.3     Martin O'Mullane
;               - Add filename to structure which returns all info.
;               - Properly calculate energy/kT for calculating Burgess x value.
;               - Remove attempts to 'correct' configuration with norm_config.
;               - Do not fail if collision strengths are 0.0 in adf04 file.
;       1.4     Martin O'Mullane
;               - Return ionisation potentials of parents and weights.
;               - Change nz to iz0 in all structure to preserve naming.
;               - Some internal renaming by prefixing internal routines
;                 with r04.
;               - Calculate ionisation rates from excited levels and
;                 return these in all structure (r04_add_ecip) if
;                 /ecipcalc is set.
;       1.5     Martin O'Mullane
;               - Incorrect logic in assigning levels and parents when
;                 generating rates from S-line data.
;       1.6     Martin O'Mullane
;               - Read data with fortran, xxdata_04.for, which permits
;                 reading large adf04 files.
;               - Trap for case of no S-lines or no zpla data when
;                 /ecipcalc option is chosen.
;       1.7     Martin O'Mullane
;               - Change the 'all' structure name to 'fulldata' to keep
;                 consistency across the read_adfXX routines. Calling with
;                 'all' will now generate an error.
;               - Add /help keyword.
;       1.8     Allan Whiteford
;               - Increased maximum number of X-values to 50 to handle
;                 reading of type-I files.
;               - Made the routine return the filetype, transition codes
;                 and Bethe limit information.
;       1.9     Allan Whiteford
;               - Added nosplit keyword so the routine doesn't
;                 split up excitation, ionisation, recombination
;                 and charge exchange data.
;               - Return IPLA in data structure.
;       1.10    Allan Whiteford
;               - Added /noproc keyword so that data is returned essentially
;                 unprocessed from xxdata_04. Necessary for MC error
;                 propogation in IDL of the CR model.
;       1.11    Martin O'Mullane
;               - Increase dimensions in call to xxdata_04 so that
;                 the high Z adf04 file can be returned in fulldata.
;       1.12    Martin O'Mullane
;               - Add cprta to fulldata output structure.
;               - Correct labelling of CX level in fulldata.
;       1.13    Allan Whiteford
;               - Added ignore_untied keyword
;       1.14    Martin O'Mullane
;               - When returning the fulldata structure increase
;                 ndqdn to 8, ndlev to 3500 and ndtran to 380000
;                 to match the heavy species dimensions.
;       1.15    Martin O'Mullane
;               - The excitation rate (rate) and de-excitation rate
;                 (dex_rate) are new outputs.
;               - Note: The returned rates, effective collision strength and
;                       Burgess-Tully transformed variables are interpolated
;                       (and) extrapolated at Te whereas previously the values
;                       in the dataset were returned. If Te is not given the
;                       outputs are returned at the temperatures in the adf04
;                       file.
;       1.16    Martin O'Mullane
;               - Re-write routine to calculate ECIP rates.
;       1.17    Martin O'Mullane
;               - Return the interpolated gamma rather than the values in
;                 the adf04 file when Te is supplied.
;       1.18    Martin O'Mullane
;               - Return quantum defects, n-average quantum defects and
;                 vector indicating whether these are in the adf04 file.
;       1.19    Martin O'Mullane
;               - Adjust fulldata output to accommodate NIST-style adf04
;                 files which contain only data for the energy levels.
;       1.20    Martin O'Mullane
;               - Add /burgess_tully keyword to interpolate in BT space.
;               - New procedures for BT.
;               - Improve description of options.
;       1.21    Martin O'Mullane
;               - Return ltied in fulldata structure. Note this only works
;                 if the /ignore_untied keyword is set.
;       1.22    Martin O'Mullane
;               - Do not apply Kelvin correction to xxsple for type 1 files.
;       1.23    Martin O'Mullane
;               - Add a standard version of Eissner configurations to 
;                 fulldata with the cstrga_std tagname.
;       1.24    Martin O'Mullane
;               - Increase dimensions (ndmet is now 13).
;               - Determine Burgess-Tully type and add to fulldata.
;               - Add parent weight (prtwta) to fulldata.
;
; VERSION:
;       1.2    17-07-2001
;       1.3    12-03-2002
;       1.4    05-04-2002
;       1.5    05-09-2002
;       1.6    03-06-2003
;       1.7    16-11-2004
;       1.8    17-03-2004
;       1.9    23-03-2004
;       1.10   17-06-2005
;       1.11   17-09-2005
;       1.12   29-03-2006
;       1.13   21-07-2008
;       1.14   13-11-2009
;       1.15   21-06-2010
;       1.16   22-10-2010
;       1.17   10-05-2011
;       1.18   28-11-2011
;       1.19   31-08-2012
;       1.20   02-03-2015
;       1.21   21-09-2015
;       1.22   24-05-2017
;       1.23   12-06-2017
;       1.24   27-03-2018
;-
;----------------------------------------------------------------------




PRO r04_add_ecip, adf04, level_ion_rate, parent_ion_rate, ion_rate, source_rate, calc

; Decide whether we should proceed

if adf04.level_ion[0] EQ -1 OR total(adf04.zpla) EQ 0 then begin
   calc = 'NO'
   return
endif else begin
   calc = 'YES'
endelse


; Required conversion factor

WN2RYD = 9.11269D-06


; Find number of S-lines and file in level and parent details

level_ion  = -99
parent_ion = -99
source     = ''

zpla = adf04.zpla
ipla = adf04.ipla

res  = size(zpla, /dimensions)
nmet = res[0]
nlev = res[1]

if nlev NE n_elements(adf04.wa) then message, 'Incompatability between zpla and levels'


; Calculate ECIP ionisation rates

iz   = adf04.iz
te   = adf04.te
n_te = n_elements(te)
xia  = WN2RYD * (adf04.bwno - adf04.wa)


rate = dblarr(nmet, nlev, n_te)
src  = strarr(nmet, nlev) + 'NONE'
lev  = intarr(nmet, nlev)
par  = intarr(nmet, nlev)

for ip = 0, nmet-1 do begin
   for ilev = 0, nlev-1 do begin

      iprnt = ipla[ip, ilev]-1
      if iprnt GE 0 then begin
          if zpla[ip,ilev] NE 0.0 then begin
             xi = xia[ilev] + WN2RYD * (adf04.bwnoa[iprnt] - adf04.bwno)
             rate[iprnt, ilev, *] = r8necip(iz=iz, xi=xi, zeta=zpla[ip,ilev], te=te)
             src[iprnt, ilev] = 'ECIP'
             par[iprnt, ilev] = iprnt + 1
             lev[iprnt, ilev] = ilev + 1
          endif
      endif

   endfor
endfor


; Overwrite with S-lines (if available) and convert these to rates
; at the same time

level_ion  = adf04.level_ion
parent_ion = adf04.parent_ion
n_slines   = n_elements(level_ion)

for j = 0, n_slines-1 do begin

   iprnt = parent_ion[j] - 1
   ilev  = level_ion[j] - 1

   rate[iprnt, ilev, *] = adf04.ion[j,*] * $
                          exp(-1.438799*(adf04.bwnoa[iprnt] - adf04.wa[ilev])/te)
   src[iprnt, ilev] = 'SLINE'
   par[iprnt, ilev] = iprnt + 1    ; in case of extras
   lev[iprnt, ilev] = ilev + 1

endfor


; Make linear list of parents and levels

lev  = reform(lev, nmet*nlev)
par  = reform(par, nmet*nlev)
src  = reform(src, nmet*nlev)
rate = reform(rate, nmet*nlev, n_te)

ind = where(strpos(src, 'NONE') EQ -1, count)

if count GT 0 then begin

   level_ion_rate  = lev[ind]
   parent_ion_rate = par[ind]
   source_rate     = src[ind]
   ion_rate        = rate[ind, *]

endif else begin

   level_ion_rate  = -1
   parent_ion_rate = -1
   source_rate     = 'NONE'
   ion_rate        = -1

endelse

END
;-----------------------------------------------------------------------------



PRO r04_xxdata_04, file     , iz          , iz0     ,           $
                   ia       , cstrga      , isa     , ila   ,   $
                   xja      , wa          , bwno    , bwnoa ,   $
                   cprta    , prtwta      , zpla    , ipla  ,   $
                   eorb     , qdorb       , lqdorb  , qdn   ,   $
                   scef     ,                                   $
                   lower    , upper       , aval    , gamma ,   $
                   level_rec, parent_rec  , rec     ,           $
                   level_ion, parent_ion  , ion     ,           $
                   level_cx , parent_cx   , cx      , tcode ,   $
                   lbeth    , beth        , iadftyp , split ,   $
                   noproc   , noprocdata  , itieactn, ltied ,   $
                   bt_type



; Set levels appropriate for large adf04 files and read in data
; using xxdata_04. Sort transitions using bxttyp.

ndlev = 4500L
ndtrn = 750000L
ndmet = 13L
ndqdn = 8L
nvmax = 50L

ltied  = lonarr(ndlev)
lss04a = 0L

xxdata_04, file    ,                                         $
           ndlev   , ndtrn  , ndmet    , ndqdn  , nvmax ,    $
           titled  , iz     , iz0      , iz1    , bwno  ,    $
           npl     , bwnoa  , lbseta   , prtwta , cprta ,    $
           il      , qdorb  , lqdorb   , qdn    , iorb  ,    $
           ia      , cstrga , isa      , ila    , xja   ,    $
           wa      ,                                         $
           cpla    , npla   , ipla     , zpla   ,            $
           nv      , scef   ,                                $
           itran   , maxlev ,                                $
           tcode   , i1a    , i2a      , aval   , scom  ,    $
           beth    ,                                         $
           iadftyp , lprn   , lcpl     , lorb   , lbeth ,    $
           letyp   , lptyp  , lrtyp    , lhtyp  , lityp ,    $
           lstyp   , lltyp  , itieactn , ltied

bxttyp, ndlev  , ndmet  , ndtrn  , nplr  , npli  , $
        itran  , tcode  , i1a    , i2a   , aval  , $
        icnte  , icntp  , icntr  , icnth , icnti , $
        icntl  , icnts  ,                          $
        ietrn  , iptrn  , irtrn  , ihtrn , iitrn , $
        iltrn  , istrn  ,                          $
                          ie1a   , ie2a  , aa    , $
                          ip1a   , ip2a  ,         $
                          ia1a   , ia2a  , auga  , $
                          il1a   , il2a  , wvla  , $
                          is1a   , is2a  , lss04a

bt_type = -1

if noproc then begin

   noprocdata={  file         :        file,     $
                 ndlev        :        ndlev,    $
                 ndtrn        :        ndtrn,    $
                 ndmet        :        ndmet,    $
                 ndqdn        :        ndqdn,    $
                 nvmax        :        nvmax,    $
                 titled       :        titled,   $
                 iz           :        iz,       $
                 iz0          :        iz0,      $
                 iz1          :        iz1,      $
                 bwno         :        bwno,     $
                 npl          :        npl,      $
                 bwnoa        :        bwnoa,    $
                 lbseta       :        lbseta,   $
                 prtwta       :        prtwta,   $
                 cprta        :        cprta,    $
                 il           :        il,       $
                 qdorb        :        qdorb,    $
                 lqdorb       :        lqdorb,   $
                 qdn          :        qdn,      $
                 iorb         :        iorb,     $
                 ia           :        ia,       $
                 cstrga       :        cstrga,   $
                 isa          :        isa,      $
                 ila          :        ila,      $
                 xja          :        xja,      $
                 wa           :        wa,       $
                 cpla         :        cpla,     $
                 npla         :        npla,     $
                 ipla         :        ipla,     $
                 zpla         :        zpla,     $
                 nv           :        nv,       $
                 scef         :        scef,     $
                 itran        :        itran,    $
                 maxlev       :        maxlev,   $
                 tcode        :        tcode,    $
                 i1a          :        i1a,      $
                 i2a          :        i2a,      $
                 aval         :        aval,     $
                 scom         :        scom,     $
                 beth         :        beth,     $
                 iadftyp      :        iadftyp,  $
                 lprn         :        lprn,     $
                 lcpl         :        lcpl,     $
                 lorb         :        lorb,     $
                 lbeth        :        lbeth,    $
                 letyp        :        letyp,    $
                 lptyp        :        lptyp,    $
                 lrtyp        :        lrtyp,    $
                 lhtyp        :        lhtyp,    $
                 lityp        :        lityp,    $
                 lstyp        :        lstyp,    $
                 lltyp        :        lltyp,    $
                 itieactn     :        itieactn, $
                 ltied        :        ltied,    $
                 nplr         :        nplr,     $
                 npli         :        npli,     $
                 icnte        :        icnte,    $
                 icntp        :        icntp,    $
                 icntr        :        icntr,    $
                 icnth        :        icnth,    $
                 icnti        :        icnti,    $
                 icntl        :        icntl,    $
                 icnts        :        icnts,    $
                 ietrn        :        ietrn,    $
                 iptrn        :        iptrn,    $
                 irtrn        :        irtrn,    $
                 ihtrn        :        ihtrn,    $
                 iitrn        :        iitrn,    $
                 iltrn        :        iltrn,    $
                 istrn        :        istrn,    $
                 ie1a         :        ie1a,     $
                 ie2a         :        ie2a,     $
                 aa           :        aa,       $
                 ip1a         :        ip1a,     $
                 ip2a         :        ip2a,     $
                 ia1a         :        ia1a,     $
                 ia2a         :        ia2a,     $
                 auga         :        auga,     $
                 il1a         :        il1a,     $
                 il2a         :        il2a,     $
                 wvla         :        wvla,     $
                 is1a         :        is1a,     $
                 is2a         :        is2a,     $
                 lss04a       :        lss04a    }

endif else begin

   ; Convert output arrays to only include relevant data

   ia     =  ia[0:il-1]
   cstrga =  cstrga[0:il-1]
   isa    =  isa[0:il-1]
   ila    =  ila[0:il-1]
   xja    =  xja[0:il-1]
   wa     =  wa[0:il-1]
   npla   =  npla[0:il-1]
   bwnoa  =  bwnoa[0:npl-1]
   cprta  =  cprta[0:npl-1]
   prtwta =  prtwta[0:npl-1]
   zpla   =  zpla[0:npl-1, 0:il-1]
   ipla   =  ipla[0:npl-1, 0:il-1]

   if nv GT 0 then scef = scef[0:nv-1] else scef = -1

   ; Calculate orbital energies from orbital quantum defects

   if lorb EQ 1 then begin

      eorb = fltarr(n_elements(qdorb))
      ind  = where(lqdorb[0:iorb-1] EQ 1, count)
      for j = 0, count-1 do begin
         index = ind[j] + 1; this is the fortran index
         xxidtl, index, n, l
         eorb[index-1] = (float(iz1) / (float(n)-qdorb[index-1]) )^2
      endfor

      eorb = eorb[0:ind[count-1]]

   endif else eorb = -1

   ; pass back prtwta, npla, maxlev, nplr, npli

   if (not split) then begin

      lower = i1a[0:itran-1]
      upper = i2a[0:itran-1]
      tcode = tcode[0:itran-1]

      gamma = transpose(scom[0:nv-1, 0:itran-1])
      aval  = aa[0:itran-1]
      beth  = beth[0:itran-1]

      level_rec  = -1
      parent_rec = -1
      rec        = -1

      level_ion  = -1
      parent_ion = -1
      ion        = -1

      level_cx  = -1
      parent_cx = -1
      cx        = -1

   endif else begin

      if icnte GT 0 then begin

         ind   = ietrn[0:icnte-1] - 1
         lower = ie1a[0:icnte-1]
         upper = ie2a[0:icnte-1]
         tcode = tcode[0:icnte-1]

         gamma = transpose(scom[0:nv-1, ind])
         aval  = aa[ind]
         beth  = beth[ind]
         
         ; Determine Burgess-Tully type
         
         fzero   = 1.0E-4
         fbig    = 0.01   
         bt_type = intarr(icnte)
         
         for j = 0L, icnte-1 do begin
            
            wvnou = wa[upper[j]-1]
            wvnol = wa[lower[j]-1]
            elu   = abs(wvnou-wvnol)/109737.26
            wtu   = 2.0*xja[upper[j]-1]+1.0
            wtl   = 2.0*xja[lower[j]-1]+1.0
            ain   = aval[j]
            s     = 3.73491e-10*wtu*ain/(elu^3.0)                        
            fin   = 3.333333e-1*elu*s/wtl                                     
           
            if isa[upper[j]-1] EQ isa[lower[j]-1] then begin

               if (abs(ila[upper[j]-1]-ila[lower[j]-1]) LE 1) AND $
                  (fin GT fbig) then bt_type[j] = 1 else bt_type[j] = 2

            endif else begin

                if (fin GT fzero) AND (fin LT fbig) then bt_type[j] = 4 else bt_type[j] = 3  
            
            endelse

         endfor

      endif else begin

         lower = -1
         upper = -1
         gamma = -1
         beth  = -1
         aval  = -1

      endelse

      if icntr GT 0 then begin

         ind        = irtrn[0:icntr-1] - 1
         level_rec  = ia2a[0:icntr-1]
         parent_rec = ia1a[0:icntr-1]

         rec        = transpose(scom[0:nv-1, ind])

      endif else begin

         level_rec  = -1
         parent_rec = -1
         rec        = -1

      endelse

      if icnts GT 0 then begin

         ind        = istrn[0:icnts-1] - 1
         level_ion  = is2a[0:icnts-1]
         parent_ion = is1a[0:icnts-1]

         ion        = transpose(scom[0:nv-1, ind])

      endif else begin

         level_ion  = -1
         parent_ion = -1
         ion        = -1

      endelse

      if icnth GT 0 then begin

         level_cx  = i2a[ihtrn[0:icnth-1]-1]
         parent_cx = i1a[ihtrn[0:icnth-1]-1]

         cx = transpose(scom[0:nv-1, ihtrn[0:icnth-1]-1])

      endif else begin

         level_cx  = -1
         parent_cx = -1
         cx        = -1

      endelse

   endelse

endelse

END
;--------------------------------------------------------------------------



PRO r04_bt_te2x, tek, gamma_k, c, x, y, ind,            $
                 isa, ila, xja, wa, lower, upper, aval, $
                 limitpoint = limitpoint

; return Te (K) and gamma as x,y

ind   = ind[0]   ; consider just one transition
fzero = 1.0E-4
fbig  = 0.01

index_u = upper[ind]-1
index_l = lower[ind]-1

wvnou = wa[index_u]
wvnol = wa[index_l]
elu   = abs(wvnou-wvnol)/109737.26
wtu   = 2.0*xja[index_u]+1.0
wtl   = 2.0*xja[index_l]+1.0
ain   = aval[ind]
s     = 3.73491e-10*wtu*ain/(elu^3)
fin   = 3.333333e-1*elu*s/wtl

; determine type of transition

if isa[index_u] EQ isa[index_l] then begin

   if (abs(ila[index_u]-ila[index_l]) LE 1) AND $
      (fin GT fbig) then tcode = 1 $
                    else tcode = 2

endif else begin

    if (fin GT fzero) AND (fin LT fbig) then tcode = 4 $
                                        else tcode = 3
endelse

et = 1.43884*(wvnou-wvnol)/tek   ; dimensionless energy cm-1, Te in K

case tcode of

  1 : begin
        x = 1.0 - alog(c) / alog(1.0/et + c)
        y = gamma_k / alog(1.0/et + 2.718281828)
        if keyword_set(limitpoint) then begin
           x = [x, 1.0]
           y = [y, 4.0*wtl*fin/elu]
        endif
      end
  2 : begin
        x = (1.0/et) / (1.0/et + c)
        y = gamma_k
      end
  3 : begin
        x = (1.0/et) / (1.0/et + c)
        y = gamma_k * (1.0/et + 1.0)
      end
  4 : begin
        x = 1.0 - alog(c) / alog(1.0/et + c)
        y = gamma_k / alog(1.0/et + c)
        if keyword_set(limitpoint) then begin
           x = [x, 1.0]
           y = [y, 4.0*wtl*fin/elu]
        endif
      end

endcase

END
;--------------------------------------------------------------------------


PRO r04_bt_interpolation, tek, gamma_k, c, tek_out, gamma_out, ind, $
                          isa, ila, xja, wa, lower, upper, aval,    $
                          limitpoint = limitpoint


ind   = ind[0]   ; consider just one transition
fzero = 1.0E-4
fbig  = 0.01

index_u = upper[ind]-1
index_l = lower[ind]-1

wvnou = wa[index_u]
wvnol = wa[index_l]
elu   = abs(wvnou-wvnol)/109737.26
wtu   = 2.0*xja[index_u]+1.0
wtl   = 2.0*xja[index_l]+1.0
ain   = aval[ind]
s     = 3.73491e-10*wtu*ain/(elu^3)
fin   = 3.333333e-1*elu*s/wtl

; determine type of transition

if isa[index_u] EQ isa[index_l] then begin

   if (abs(ila[index_u]-ila[index_l]) LE 1) AND $
      (fin GT fbig) then tcode = 1 $
                    else tcode = 2

endif else begin

    if (fin GT fzero) AND (fin LT fbig) then tcode = 4 $
                                        else tcode = 3
endelse

et = 1.43884*(wvnou-wvnol)/tek   ; dimensionless energy cm-1, Te in K

et_out = 1.43884*(wvnou-wvnol)/tek_out

; adf04 scef and gamma onto [0,1]

case tcode of

  1 : begin
        x = 1.0 - alog(c) / alog(1.0/et + c)
        y = gamma_k / alog(1.0/et + 2.718281828)
        if keyword_set(limitpoint) then begin
           x = [x, 1.0]
           y = [y, 4.0*wtl*fin/elu]
        endif

        xout = 1.0 - alog(c) / alog(1.0/et_out + c)

      end
  2 : begin
        x = (1.0/et) / (1.0/et + c)
        y = gamma_k

        xout = (1.0/et_out) / (1.0/et_out + c)

      end
  3 : begin
        x = (1.0/et) / (1.0/et + c)
        y = gamma_k * (1.0/et + 1.0)

        xout = (1.0/et_out) / (1.0/et_out + c)

      end
  4 : begin
        x = 1.0 - alog(c) / alog(1.0/et + c)
        y = gamma_k / alog(1.0/et + c)
        if keyword_set(limitpoint) then begin
           x = [x, 1.0]
           y = [y, 4.0*wtl*fin/elu]
        endif

        xout = 1.0 - alog(c) / alog(1.0/et_out + c)

      end

endcase


; Now interpolate

xxsple, opt=0, xin=x, yin=y, xout=xout, yout=yout, /log

; Turn yout back to gamma

case tcode of

  1 : gamma_out = yout * alog(1.0/et_out + 2.718281828)
  2 : gamma_out = yout
  3 : gamma_out = yout / (1.0/et_out + 1.0)
  4 : gamma_out = yout * alog(1.0/et_out + c)

endcase


END
;--------------------------------------------------------------------------



PRO read_adf04, file          = file,            $
                te            = te,              $
                lower         = lower,           $
                upper         = upper,           $
                gamma         = gamma,           $
                rate          = rate,            $
                dex_rate      = dex_rate,        $
                x             = x,               $
                y             = y,               $
                all           = all,             $
                fulldata      = fulldata,        $
                cfactor       = cfactor,         $
                limitpoint    = limitpoint,      $
                ecipcalc      = ecipcalc,        $
                nosplit       = nosplit,         $
                noproc        = noproc,          $
                ignore_untied = ignore_untied,   $
                burgess_tully = burgess_tully,   $
                kelvin        = kelvin,          $
                help          = help



; If asked for help

if keyword_set(help) then begin
   doc_library, 'read_adf04'
   return
endif

; Catch the deprecated 'all' request

if arg_present(all) then begin
   message, 'The name all to return the data in the adf04 file', /continue
   message, 'has been replaced by fulldata.'
endif

if keyword_set(nosplit) then begin
   split=0
endif else begin
   split=1
endelse

if not keyword_set(noproc) then noproc=0

if keyword_set(ignore_untied) then itieactn=1L else itieactn=0L

; file name

if n_elements(file) EQ 0 then message,'A filename must be given'

file_acc, file, exist, read, write, execute, filetype
if exist ne 1 then message, 'adf04 file does not exist '+file
if read ne 1 then message, 'adf04 file cannot be read from this userid '+file


; Read in the adf04 by calling the IDL versions of xxdata_04 and bxttyp


r04_xxdata_04, file     , iz          , iz0     ,                  $
               ia       , cstrga      , isa     , ila      ,       $
               xja      , wa          , bwno    , bwnoa    ,       $
               cprta    , prtwta      , zpla    , ipla     ,       $
               eorb     , qdorb       , lqdorb  , qdn      ,       $
               scef     ,                                          $
               lower_set, upper_set   , aval_set, gamma_set,       $
               level_rec, parent_rec  , rec     ,                  $
               level_ion, parent_ion  , ion     ,                  $
               level_cx , parent_cx   , cx      ,                  $
               tcode    , lbeth       , beth    , iadftyp , split, $
               noproc   , noprocdata  , itieactn, ltied   , bt_type

; Check if configurations are Eissner and make a standard set.

n_cfg = n_elements(cstrga)
cstrga_std = cstrga

for j = 0, n_cfg-1 do begin

   xxdtes, in_cfg=cstrga[j], is_eissner=is_eissner

   if is_eissner EQ 1 then begin
      xxcftr, in_cfg=cstrga[j], out_cfg=str, type=3
      cstrga_std[j] = str
   endif

endfor


; Either return everything or proceed to extract the requested quantities

if arg_present(fulldata) then begin

   if keyword_set(noproc) then begin

      fulldata = noprocdata

   endif else begin

      fulldata = { filename    :  file,         $
                   iz          :  iz,           $
                   iz0         :  iz0,          $
                   ia          :  ia,           $
                   cstrga      :  cstrga,       $
                   cstrga_std  :  cstrga_std,   $
                   isa         :  isa,          $
                   ila         :  ila,          $
                   xja         :  xja,          $
                   wa          :  wa,           $
                   zpla        :  zpla,         $
                   ipla        :  ipla,         $
                   ltied       :  ltied,        $
                   bwno        :  bwno,         $
                   bwnoa       :  bwnoa,        $
                   cprta       :  cprta,        $
                   prtwta      :  prtwta,       $
                   eorb        :  eorb,         $
                   qdn         :  qdn,          $
                   qdorb       :  qdorb,        $
                   lqdorb      :  lqdorb,       $
                   te          :  scef,         $
                   lower       :  lower_set,    $
                   upper       :  upper_set,    $
                   aval        :  aval_set,     $
                   gamma       :  gamma_set,    $
                   level_rec   :  level_rec,    $
                   parent_rec  :  parent_rec,   $
                   rec         :  rec,          $
                   level_ion   :  level_ion,    $
                   parent_ion  :  parent_ion,   $
                   ion         :  ion,          $
                   level_cx    :  level_cx,     $
                   parent_cx   :  parent_cx,    $
                   cx          :  cx,           $
                   lbeth       :  lbeth,        $
                   beth        :  beth,         $
                   tcode       :  tcode,        $
                   bt_type     :  bt_type,      $
                   iadftyp     :  iadftyp       }

      if keyword_set(ecipcalc) then begin

         r04_add_ecip, fulldata, level_ion_rate, parent_ion_rate, ion_rate, $
                       source_rate, calc

         if calc EQ 'YES' then begin
            fulldata = { filename         :  file,              $
                         iz               :  iz,                $
                         iz0              :  iz0,               $
                         ia               :  ia,                $
                         cstrga           :  cstrga,            $
                         cstrga_std       :  cstrga_std,        $
                         isa              :  isa,               $
                         ila              :  ila,               $
                         xja              :  xja,               $
                         wa               :  wa,                $
                         zpla             :  zpla,              $
                         ipla             :  ipla,              $
                         bwno             :  bwno,              $
                         bwnoa            :  bwnoa,             $
                         cprta            :  cprta,             $
                         prtwta           :  prtwta,            $
                         eorb             :  eorb,              $
                         qdn              :  qdn,               $
                         qdorb            :  qdorb,             $
                         lqdorb           :  lqdorb,            $
                         te               :  scef,              $
                         lower            :  lower_set,         $
                         upper            :  upper_set,         $
                         aval             :  aval_set,          $
                         gamma            :  gamma_set,         $
                         level_rec        :  level_rec,         $
                         parent_rec       :  parent_rec,        $
                         rec              :  rec,               $
                         level_ion        :  level_ion,         $
                         parent_ion       :  parent_ion,        $
                         ion              :  ion,               $
                         level_ion_rate   :  level_ion_rate,    $
                         parent_ion_rate  :  parent_ion_rate,   $
                         ion_rate         :  ion_rate,          $
                         ion_source       :  source_rate,       $
                         level_cx         :  level_cx,          $
                         parent_cx        :  parent_cx,         $
                         cx               :  cx,                $
                         lbeth            :  lbeth,             $
                         beth             :  beth,              $
                         tcode            :  tcode,             $
                         bt_type          :  bt_type,           $
                         iadftyp          :  iadftyp            }
         endif

      endif

   endelse

endif else begin

   if n_elements(te) EQ 0 then begin
      message, 'Returning temperature set in adf04', /continue
      te = scef / 11605.0
   endif

   if keyword_set(kelvin) then te_in = te /11605.0 else te_in = te

   if lower GT upper then begin
      itmp  = lower
      lower = upper
      upper = itmp
      print, 'Reversed lower and upper indices'
   endif

   u = where(upper_set EQ upper, n_u)
   l = where(lower_set EQ lower, n_l)

   if (n_l GT 0) AND (n_u GT 0) then begin

      ind   = setintersection(u,l)
      if n_elements(ind) GT 0 AND ind[0] GT -1 then begin
          gamma = reform(gamma_set[ind,*])
      endif else begin
          print, 'Transition is not available : ' + string(upper) + ' - '+ string(lower)
          gamma = -1
          return
      endelse

      xin  = scef
      yin  = gamma
      if iadftyp EQ 3 then xout = te_in * 11605.0 else xout = te_in

      if keyword_set(burgess_tully) then begin
         if n_elements(cfactor) EQ 0 then c = 1.5 else c = cfactor
         r04_bt_interpolation, xin, yin, c, xout, yout, ind,                      $
                               isa, ila, xja, wa, lower_set, upper_set, aval_set, $
                               limitpoint = limitpoint
      endif else begin
         xxsple, opt=0, xin=xin, yin=yin, xout=xout, yout=yout, /log
      endelse

      wlower = 2.0*xja[lower-1] + 1.0
      elower = wa[lower-1]
      wupper = 2.0*xja[upper-1] + 1.0
      eupper = wa[upper-1]

      rate = 2.17161d-8 * yout * sqrt(157890.0 / xout) / $
             (wlower * exp(1.4388 * (eupper-elower) / xout))

      dex_rate = 2.17161d-8 * yout * sqrt(157890.0 / xout) / wupper

      gamma = yout

   endif else begin

      print, 'Transition is not available : ' + string(upper) + ' - '+ string(lower)
      gamma = -1
      return

   endelse


   ; Burgess-Tully transformations (assume c=1.5 if cfactor not set)

   if n_elements(te) GT 0 then begin
      tek     = xout
      gamma_k = yout
   endif else begin
      tek     = scef
      gamma_k = gamma
   endelse

   if n_elements(cfactor) EQ 0 then c = 1.5 else c = cfactor

   if arg_present(x) OR arg_present(y) then begin

      r04_bt_te2x, tek, gamma_k, c, x, y, ind,                        $
                   isa, ila, xja, wa, lower_set, upper_set, aval_set, $
                   limitpoint=limitpoint

   endif

endelse

END
