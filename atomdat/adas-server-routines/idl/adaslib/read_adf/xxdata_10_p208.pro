;----------------------------------------------------------------------
;+
; PROJECT    :  ADAS
;
; NAME       :  xxdata_10_p208
;
; PURPOSE    :  Returns the data in a partial adf10 file produced by
;               adas208. Normally named acd208.pass, plt208.pass etc.
;
; INPUTS     :  file    str    Full name of adf10 file to read
;
;               class   str    Class of data. acd, scd, ccd, qcd,
;                                             xcs, plt, prb, prc,
;                                             met
;               iz1    int     Charge + 1 (1 for neutral)
;               imet   int     Metastable (met, plt)
;               iprt   int     }
;               igrd   int     } identifiers - see table
;               jgrd   int     }
;
;                       class    (indices 1 and 2)
;                       ----       ----     ----
;                       acd        iprt     igrd
;                       scd        iprt     igrd
;                       ccd        iprt     igrd
;                       qcd        igrd     jgrd
;                       xcd        iprt     jprt
;                       prb        iprt
;                       prc        iprt
;                       plt        igrd
;                       met        igrd
;
;               type_a_xx10_p208 - reads acd, scd, ccd, qcd, xcd, prb, prc
;               type_b_xx10_p208 - reads plt and met
;
;
; KEYWORDS      skipcomments - if set, skip 3 comment lines inserted by
;                              adas208 at top of xxx208.pass files
;               misslines    - set to an arbitrary number of lines to skip
;
; OUTPUTS    :  data: Structure containing contents of adf10 file:
;                        te               : electron temperature in eV
;                        dens             : electron density (cm-3)
;                        data[nte, ndens] : rate coefficient (cm3/s or W cm3)
;
; NOTES      :  The units for the adas208 plt/prb/prc are ergs/s cm3. These
;               are converted to W cm3 in the output structure.
;
; AUTHOR     :  Martin O'Mullane
;
; DATE       :  22-05-2008
;
; MODIFIED:
;       1.1     Martin O'Mullane
;               - First version.
;
; VERSION:
;       1.1    24-07-2017
;
;-
;----------------------------------------------------------------------

PRO type_a_xx10_p208, all, itval, idval, index_1, index_2, nmet, data

len_blk = numlines(idval, 8) * itval

i1 = where(strpos(all, 'Z1=') NE -1, c1)

index_1 = intarr(c1)
index_2 = intarr(c1)
data    = fltarr(c1, itval, idval)

for j = 0, c1-1 do begin

   index_1[j] = long(strmid(all[i1[j]], 38, 2))
   index_2[j] = long(strmid(all[i1[j]], 49, 2))

   data_10 = fltarr(idval, itval)
   reads, all[i1[j]+1:i1[j]+len_blk], data_10

   data[j, *, *] = transpose(data_10)

endfor

nmet = max([index_1, index_2])

if total(index_2) EQ 0 then index_2 = -1

END
;----------------------------------------------------------------------



PRO type_b_xx10_p208, all, itval, idval, index_1, index_2, nmet, data

len_blk = numlines(idval, 8) * itval


i1   = where(strpos(all, 'NMET         =') NE -1)
nmet = long(strmid(all[i1[0]], 14, 2))

i1 = where(strpos(all, 'Z1=') NE -1, c1)

index_1 = intarr(nmet)
index_2 = -1
data    = fltarr(nmet, itval, idval)

i1 = where(strpos(all, '/') EQ 0, c1)
if c1 NE nmet then message, 'Problem with NMET'

for j = 0, nmet-1 do begin

   index_1[j] = j+1

   data_10 = fltarr(idval, itval)
   reads, all[i1[j]+1:i1[j]+len_blk], data_10

   data[j, *, *] = transpose(data_10)

endfor

END
;----------------------------------------------------------------------


PRO xxdata_10_p208, file = file, fulldata = fulldata

; Check if file exists and has data

if n_elements(file) eq 0 then message, 'A file name must be passed'
file_acc, file, exist, read, write, execute, filetype
if exist ne 1 then message, 'adf10 file does not exist ' + file
if read ne 1 then message, 'adf10 file cannot be read from this userid ' + file

res = file_info(file)
sz  = res[0].size

if sz EQ 0 then begin

    message, 'No data in file ' + file, /continue
    fulldata = {class : 'EMPTY'}
    return

endif

; Read the file. If its length is 3 lines then there is no data

adas_readfile, file=file, all=all, nline=nlines

if nlines LE 3 then begin

    message, 'No data in file ' + file, /continue
    fulldata = {class : 'EMPTY'}
    return

endif



; Determine class from the second line of the comments

str = strmid(all[1], 0, 18)

case str of

  'C  IONISATION DATA' : class = 'scd'
  'C  RECOMBINATION D' : class = 'acd'
  'C  CX RECOMBINATIO' : class = 'ccd'
  'C  METASTABLE CROS' : class = 'qcd'
  'C  CX RECOMBINATIO' : class = 'ccd'
  'C  TOTAL LINE POWE' : class = 'plt'
  'C  PARENT CROSS CO' : class = 'xcd'
  'C  METASTABLE FRAC' : class = 'met'
  'C  RECOM/CASACDE/B' : class = 'prb'

  else : message, 'Cannot determine class of file' + file

endcase


; Nuclear charge from the comment section

i0 = where(strpos(all, 'NUCLEAR CHARGE =') NE -1)
i1 = i0[0]

iz0 = -1
reads, all[i1], iz0, format='(20x, i2)'

; The adf04 file is needed to extract the ionisation potentials

i1    = where(strpos(all, 'SPECIFIC ION FILE') NE -1)
adf04 = strmid(all[i1[0]], 24)

; Charge state, temperature and density

i0 = where(strpos(all, '--------') NE -1)
i1 = i0[0]
i3 = i0[2]

itval = -1
idval = -1
nion  = -1
iz1   = -1

reads, all[i1+1], itval, idval, nion, format='(5x,3i5)'
reads, all[i1+2], iz1, format='(i4)'

te_10   = fltarr(itval)
dens_10 = fltarr(idval)

i4 = i3+1
i5 = i3+1+numlines(idval,8)-1
reads, all[i4:i5], dens_10
i6 = i5+1
i7 = i5+1+numlines(itval,8)-1
reads, all[i6:i7], te_10

; Convert temperature and density to real units

te_10   =  (te_10 * float(iz1)^2.0) / 1.16045221D+04
dens_10 =  dens_10 * float(iz1)^7.0


; Store comments for data tracking -- use the last block in the file

i0 = where(strpos(all, '--------') NE -1, c0)
i1 = i0[c0-2] + 1
i2 = i0[c0-1] - 1

comments = all[i1:i2]


; Data have different arrangements

eia     = -1
len_blk = numlines(idval, 8) * itval

case class of

   'scd' : begin

              name_ind1 = 'IPRT'
              name_ind2 = 'IGRD'

              type_a_xx10_p208, all, itval, idval, index_1, index_2, nmet, data

              read_adf04, file=adf04, fulldata=a04
              bwnoa = a04.bwnoa
              wa    = a04.wa

              eia = fltarr(max(index_1), max(index_2))
              
              for j = 0, n_elements(index_1)-1 do begin
              
                 in1 = index_1[j]-1
                 in2 = index_2[j]-1
                 
                 eia[in1, in2] = bwnoa[in1] - wa[in2]
                 
              endfor

           end

   'acd' : begin

              name_ind1 = 'IPRT'
              name_ind2 = 'IGRD'

              type_a_xx10_p208, all, itval, idval, index_1, index_2, nmet, data

           end

   'ccd' : begin

              name_ind1 = 'IPRT'
              name_ind2 = 'IGRD'

              type_a_xx10_p208, all, itval, idval, index_1, index_2, nmet, data

           end

   'qcd' : begin

              name_ind1 = 'IGRD'
              name_ind2 = 'JGRD'

              type_a_xx10_p208, all, itval, idval, index_1, index_2, nmet, data

           end

   'xcd' : begin

              name_ind1 = 'IPRT'
              name_ind2 = 'JPRT'

              type_a_xx10_p208, all, itval, idval, index_1, index_2, nmet, data

           end

   'prb' : begin

              name_ind1 = 'IPRT'
              name_ind2 = ''

              type_a_xx10_p208, all, itval, idval, index_1, index_2, nmet, data

              data = data * 1.0D-07

           end

   'prc' : begin

              name_ind1 = 'IPRT'
              name_ind2 = ''

              type_a_xx10_p208, all, itval, idval, index_1, index_2, nmet, data

              data = data * 1.0D-07

           end

   'plt' : begin

              name_ind1 = 'IMET'
              name_ind2 = ''

              type_b_xx10_p208, all, itval, idval, index_1, index_2, nmet, data

              data = data * 1.0D-07

           end

   'met' : begin

              name_ind1 = 'IMET'
              name_ind2 = ''

              type_b_xx10_p208, all, itval, idval, index_1, index_2, nmet, data

           end

   else  : message, 'Incorrect class type ' + class

endcase


; Gather data into a structure

fulldata = { iz0       : iz0,       $
             iz1       : iz1,       $
             class     : class,     $
             nmet      : nmet,      $
             name_ind1 : name_ind1, $
             index_1   : index_1,   $
             name_ind2 : name_ind2, $
             index_2   : index_2,   $
             eia       : eia,       $
             te        : te_10,     $
             dens      : dens_10,   $
             data      : data,      $
             comments  : comments   }

END
