;----------------------------------------------------------------------
;+
; PROJECT    :  ADAS
;
; NAME       :  xxdata_49
;
; PURPOSE    :  Read data from universal cross section file.
;               data. 
;
; ARGUMENTS  :  All output arguments will be defined appropriately.
;
;               NAME      I/O    TYPE     DETAILS
; REQUIRED   :  file       I     string   Name of file to read
; OPTIONAL      fulldata   O     struc    data in the adf49 file
;
; The fulldata structure is defined:
;
;               filename    str      adf01 file name
;               symbd       string   donor ion element symbol              
;               izd         int      donor charge                          
;               indd        int      donor state index                     
;               nenrgy      int      number of energies                                           
;               n           double   scaled n shells                       
;               alpha       double   alpha parameter                       
;               beta        double   beta parameter                        
;               gamma       double   gamma parameter for scaling n shells  
;               delta       double   delta parameter for scaling x sects   
;               ltype       double   l shell parameters                                           
;               lparms      integer  type of L parms used (currently only 1)                                           
;               enrgya      double   scaled energies                               
;               sigta       double   scaled total cross section                    
;               signa       double   scaled n shell cross section                  
;
; NOTES      :  Return cross sections in cm-2 and energy in eV/amu. Note that
;               energies in adf49 files are archived as keV/amu.
;
; AUTHOR     :  Adam Foster
; DATE       :  30-04-2007
; 
;
; MODIFIED:
;       1.1     Adam Foster
;               - First version.
;       1.2     Martin O'Mullane
;                 - Replace fixed arguments with keyword arguments
;                   to bring in line with the other xxdata routines.
;
; VERSION:
;       1.1    05-07-2007
;       1.2    11-11-2018
;-
;----------------------------------------------------------------------

PRO xxdata_49, file=file, fulldata=fulldata

dummy = ''

openr, lun, file, /get_lun

readf, lun, dummy

; check opening word is "Universal" as file type checking

IF (STRCMP(dummy,'Universal',9,/FOLD_CASE) NE 1) THEN BEGIN
  ; wrong type of file?
  message, 'Wrong type of file'
ENDIF


; define variables (see definitions in opening comment block)

delem      = ''
izd        = 0
zd         = 0
ltype      = ''
alpha      = 0d0
beta       = 0d0
lvld       = 0
n_energies = 0
n_n_shells = 0

; extract data from 1st line

reads, dummy, delem, izd, lvld, ltype, FORMAT='(10X,A2,X,I3,X,I1,45X,A8)'

; check for l shell switch

ltype=STRCOMPRESS(ltype, /remove_all)
IF (STRCMP(ltype,'lparms',/FOLD_CASE) EQ 1) THEN ltype=1 else ltype=0

; get atomic number of element

zd = xxeiz0(delem)

; second line

readf, lun, n_energies, FORMAT='(I5)'
readf, lun, n_n_shells, FORMAT='(I5)'
readf, lun, alpha,      FORMAT='(G9.4)'
readf, lun, beta,       FORMAT='(G9.4)'

;array to store n_shell information

n_shells = dblarr(n_n_shells)

; define x_grid (scaled energy grid), x_n_grid (scaled n shell grid)
; sig_tot_grid (total xsect grid) and sig_n_grid (partial x sect grid)

x_grid       = dblarr(n_energies)
gamma        = dblarr(n_energies)
delta        = dblarr(n_energies)
x_n_grid     = dblarr(n_n_shells)
sig_tot_grid = dblarr(n_energies)
sig_n_grid   = dblarr(n_energies, n_n_shells)

lparms       = dblarr(6, n_energies)

; start loop for reading in all the relevant data

ix=0

WHILE (ix LT n_energies) DO BEGIN
  readf,lun, nenyln, FORMAT='(I5)'  ;number of energies on the line
  
  tmp=dblarr(nenyln)
  
  ; scaled energies
  readf, lun, tmp
  x_grid[ix:ix+nenyln-1]=tmp

  ; gamma
  readf, lun, tmp
  gamma[ix:ix+nenyln-1]=tmp

  ; delta
  readf, lun, tmp
  delta[ix:ix+nenyln-1]=tmp

  ; lparms (if present)
  
  IF (ltype EQ 1) THEN BEGIN
    FOR ilparms=0, 5 DO BEGIN
      readf, lun, tmp
      lparms[ilparms,ix:ix+nenyln-1]=tmp
    ENDFOR  
  ENDIF

  ; scaled_tot_x_sect
  readf, lun, tmp
  sig_tot_grid[ix:ix+nenyln-1]=tmp  
  
  ; partial x sect header line
  readf, lun, dummy
  
  ; start partial data
  FOR in=0, n_n_shells-1 DO BEGIN
    n=''
    l=''
    readf,lun,dummy
    ; extract n, l shell information
    reads,dummy,n,l,FORMAT='(A10,A10)'

    IF (ix EQ 0) THEN BEGIN
      ; first iteration: read in the n-shells
      n_shells[in]=n*1.0d0
    ENDIF  
    ; extract cross section information
    reads,STRMID(dummy,20),tmp
    
    ; check if there is l information present
    IF (STRCMP(l,'          ',/FOLD_CASE) EQ 1) THEN BEGIN 
    
    ; no l data: this line is n shell only
      sig_n_grid[ix:ix+nenyln-1, in]=tmp
    ENDIF  ELSE BEGIN
      ; in here will go l-resolved reading. However since I don't yet know
      ; what l resolved data will look like I have not finalised its data
      ; format
    ENDELSE
    
  ENDFOR
  
  ix=ix+nenyln  

ENDWHILE


free_lun, lun

; return all the information in fulldata structure

fulldata =  { filename  :  file,          $
              symbd     :  delem,         $
              izd       :  izd,           $
              indd      :  lvld,          $
              nenrgy    :  n_energies,    $
              n         :  n_shells,      $
              alpha     :  alpha,         $
              beta      :  beta,          $
              gamma     :  gamma,         $
              delta     :  delta,         $
              ltype     :  ltype,         $
              lparms    :  lparms,        $
              enrgya    :  x_grid*1000.0, $
              sigta     :  sig_tot_grid,  $
              signa     :  sig_n_grid     }

END
