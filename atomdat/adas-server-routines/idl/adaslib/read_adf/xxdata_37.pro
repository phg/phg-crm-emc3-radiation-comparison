;----------------------------------------------------------------------
;+
; PROJECT    :  ADAS
;
; NAME       :  xxdata_37
;
; PURPOSE    :  Read adf37 electron distribution files from IDL
;
; ARGUMENTS  :  All output arguments will be defined appropriately.
;               Inputs will be converted to correct type, if possible,
;               internally without changing calling type.
;
;               The argument list is the same as fortran except
;               that the file name is passed rather than a unit
;               number.
;
;              NAME       I/O    TYPE    DETAILS
; REQUIRED   : file        I     str     adf37 file name
; OPTIONAL   : fulldata    O     -       Output structure (see below)
; KEYWORDS   : help        I     switch  Help required
;
; The fulldata structure is defined as:
;
;
;
; MODIFIED:
;       1.1    Martin O'Mullane
;                - First version.
;
; VERSION:
;       1.1    27-09-2010
;
;-
;----------------------------------------------------------------------

PRO xxdata_37, file     = file,     $
               fulldata = fulldata, $
               help     = help


; Return help if requested

if keyword_set(help) then begin
    doc_library, 'xxdata_37'
    return
endif

; Check the file is valid

if n_elements(file) eq 0 then message,'A filename must be given'
file_acc, file, exist, read, write, execute, filetype
if exist ne 1 then message, 'adf37 file does not exist '+file
if read ne 1 then message, 'adf37 file cannot be read from this userid '+file


title = ''
icateg = 0L
ieunit = 0L
nenerg = 0L
nblock = 0L

nform1 = 0L
nform2 = 0L
param2 = 0.0D0

openr, lun, file, /get_lun

readf, lun, title
readf, lun, icateg
readf, lun, ieunit
readf, lun, nenerg
readf, lun, nblock

str = ''
readf, lun, str
reads, str, nform1
if nform1 EQ 2 then reads, str, nform1, param1 else param1 = !values.f_nan
readf, lun, nform2, param2

ea = fltarr(nblock, nenerg)
fa = fltarr(nblock, nenerg)
tmp = fltarr(nenerg)

for j = 0, nblock-1 do begin
   readf, lun, tmp
   ea[j,*] = tmp
   readf, lun, tmp
   fa[j,*] = tmp
endfor

; Calculate effective temperature and mode and meadian energies

mode_eng   = fltarr(nblock)
median_eng = fltarr(nblock)
teff       = fltarr(nblock)


for iblock = 0, nblock-1 do begin
   
   contrib = fltarr(nenerg)
   
   sum = 0.0
   mode_eng[iblock] = fa[iblock, 0]
   if nform1 EQ 2 then sum = fa[iblock,0] * ea[iblock,0]^2 / (param1 + 2.0d0)
   contrib[0] = sum
   
   for ie = 0, nenerg-2 do begin
      
      de  = ea[iblock, ie+1] - ea[iblock, ie]
      sum = sum + de * ( fa[iblock,ie] * ea[iblock,ie]     + $
                         fa[iblock,ie+1] * ea[iblock,ie+1] ) / 2.0
      contrib[ie+1] = sum
      if fa[iblock, ie+1] GT mode_eng[iblock] then mode_eng[iblock] = fa[iblock, ie+1]
   
   endfor
   if nform2 EQ 2 then sum = sum + fa[iblock, nenerg-1] * ea[iblock, nenerg-1]^2 / (param2 + 2.0d0)
   if nform2 EQ 3 then sum = sum + fa[iblock, nenerg-1] * (ea[iblock, nenerg-1] + 1.0/param2) / param2
  
   teff[iblock] = 2.0 * sum / 3.0
  
   ind = where(contrib GE sum/2.0)
   ind = ind[0]
   
   median_eng[iblock] = (sum /2.0d0 - contrib[ind-1]) /        $
                        (contrib[ind] -  contrib[ind-1]) *     $
                        (ea[iblock,ind] - ea[iblock,ind-1]) +  $
                        ea[iblock,ind-1]

endfor

if arg_present(fulldata) then begin

   fulldata = { icateg     :  icateg,     $
                ieunit     :  ieunit,     $
                nenerg     :  nenerg,     $
                nblock     :  nblock,     $
                nform1     :  nform1,     $
                nform2     :  nform2,     $
                param1     :  param1,     $
                param2     :  param2,     $
                ea         :  ea,         $
                fa         :  fa,         $
                mode_eng   :  mode_eng,   $  
                median_eng :  median_eng, $
                teff       :  teff        }

endif

free_lun, lun

END
