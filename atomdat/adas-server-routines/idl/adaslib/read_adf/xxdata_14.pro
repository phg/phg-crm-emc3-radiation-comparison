;----------------------------------------------------------------------
;+
; PROJECT    :  ADAS
;
; NAME       :  xxdata_14
;
; PURPOSE    :  Reads in thermal charge exchange (TCX) files and
;               returns the contents in a fulldata structure.
;
; ARGUMENTS  :  All output arguments will be defined appropriately.
;               Inputs will be converted to correct type, if possible,
;               internally without changing calling type.
;
;                xxdata_14
;
;               NAME      I/O    TYPE    DETAILS
; REQUIRED   :  file       I     char    adf14 filename
;               fulldata   O     struct  all of the data in adf14 file.
;                                        file      : filename
;                                        nblock    : number blocks in file
;                                        idz0      : nuclear charge donor
;                                                      - 1st dim: data block
;                                        irz0      : nuclear charge receiver
;                                                      - 1st dim: data block
;                                        irz1      : initial charge receiver
;                                                      - 1st dim: data block
;                                        amsda     : atomic mass donor
;                                                      - 1st dim: data block
;                                        amsra     : atomic mass receiver
;                                                      - 1st dim: data block
;                                        cdonor    : donor name
;                                                      - 1st dim: data block
;                                        crecvr    : receiver name
;                                                      - 1st dim: data block
;                                        cfstat    : final state
;                                                      - 1st dim: data block
;                                        itda      : number of donor temperatures
;                                                      - 1st dim: data block
;                                        tfda      : donor temperature
;                                                      - 1st dim: temperature index
;                                                      - 2nd dim: data block
;                                        itra      : number of receiver temperatures
;                                                      - 1st dim: data block
;                                        tfra      : receiver temperature
;                                                      - 1st dim: temperature index
;                                                      - 2nd dim: data block
;                                        lequa     : block contains equal temperature data
;                                                      - 1st dim: data block
;                                        qfteqa    : equal temperature rate coefficinets (cm3 s-1)
;                                                      - 1st dim: receiver temperature index
;                                                      - 2nd dim: data block
;                                        qftcxa    : rate coefficinets (cm3 s-1)
;                                                      - 1st dim: donor temperature index
;                                                      - 2nd dim: receiver temperature index
;                                                      - 3rd dim: data block
;
; NOTES      :  Calls fortran code.
;
;
; AUTHOR     :  Martin O'Mullane
;
; DATE       :  06/03/18
;
;
; MODIFIED:
;       1.1     Martin O'Mullane
;              	- First version.
;
; VERSION:
;       1.1    06-03-2018
;
;-
;----------------------------------------------------------------------

PRO xxdata_14, file=file, fulldata=fulldata

; Check that the adf24 file exists

file_acc, file, exist, read, write, execute, filetype
if exist ne 1 then message, 'adf14 file does not exist '+file
if read ne 1 then message, 'adf14 file cannot be read from this userid '+file


; Define types of the output parameters

iunit     = 67L
nbsel     = -1L
nstore    = 150
ntddim    = 35
ntrdim    = 35
idz0      = lonarr(nstore)
irz0      = lonarr(nstore)
irz1      = lonarr(nstore)
isela     = lonarr(nstore)
itda      = lonarr(nstore)
itra      = lonarr(nstore)
amsda     = dblarr(nstore)
amsra     = dblarr(nstore)
qfteqa    = dblarr(ntrdim,nstore)
qftcxa    = dblarr(ntddim,ntrdim,nstore)
tfda      = dblarr(ntddim,nstore)
tfra      = dblarr(ntrdim,nstore)
cdonor    = strarr(nstore) + '         '
crecvr    = strarr(nstore) + '         '
cfstat    = strarr(nstore) + '          '
il_lequa  = lonarr(nstore) - 1L

dsname = string(replicate(32B, 132))
strput, dsname, file, 0


; Combine string arrays into a byte array

str_vars = [cdonor, crecvr, cfstat]
str_vars = byte(str_vars)
str_vars = long(str_vars)


; Location of sources

fortdir = getenv('ADASFORT')

dummy = CALL_EXTERNAL(fortdir+'/xxdata_14_if.so','xxdata_14_if', $
                      iunit    ,                                 $
                      nstore   , ntrdim , ntddim ,               $
                      nbsel    , isela  ,                        $
                      irz0     , irz1   , idz0   ,               $
                      il_lequa ,                                 $
                      amsra    , amsda  ,                        $
                      itra     , itda   ,                        $
                      tfra     , tfda   ,                        $
                      qfteqa   , qftcxa ,                        $
                      str_vars , dsname                          )

; Convert mangled string arrays  back to calling parameter names

res = byte(str_vars)
res = string(res)

cdonor = res[0:nstore-1]
crecvr = res[nstore:2*nstore-1]
cfstat = res[2*nstore:3*nstore-1]

; Return structure

fulldata = { file      :  file,                   $
             nblock    :  nbsel,                  $
             idz0      :  idz0[0:nbsel-1],        $
             irz0      :  irz0[0:nbsel-1],        $
             irz1      :  irz1[0:nbsel-1],        $
             amsda     :  amsda[0:nbsel-1],       $
             amsra     :  amsra[0:nbsel-1],       $
             cdonor    :  cdonor[0:nbsel-1],      $
             crecvr    :  crecvr[0:nbsel-1],      $
             cfstat    :  cfstat[0:nbsel-1],      $
             itda      :  itda[0:nbsel-1],        $
             tfda      :  tfda[*,0:nbsel-1],      $
             itra      :  itra[0:nbsel-1],        $
             tfra      :  tfra[*,0:nbsel-1],      $
             lequa     :  il_lequa[0:nbsel-1],    $
             qfteqa    :  qfteqa[*,0:nbsel-1],    $
             qftcxa    :  qftcxa[*,*,0:nbsel-1]   }

END
