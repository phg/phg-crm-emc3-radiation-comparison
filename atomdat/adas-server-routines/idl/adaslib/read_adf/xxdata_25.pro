;----------------------------------------------------------------------
;+
; PROJECT    :  ADAS
;
; NAME       :  xxdata_25
;
; PURPOSE    :  Reads adf25 files from IDL
;
; ARGUMENTS  :  All output arguments will be defined appropriately.
;               Inputs will be converted to correct type, if possible,
;               internally without changing calling type.
;
;               The argument list is the same as fortran except
;               that the file name is passed rather than a unit
;               number.
;
;              NAME       I/O    TYPE    DETAILS
; REQUIRED   : file        I     str     adf25 file name
; OPTIONAL   : fulldata    O     -       Output structure (see below)
; KEYWORDS   : help        I     switch  Help required
;
; The fulldata structure is defined as:
;
;       a25file  = file name of adf25 format read.
;       iz0      = nuclear charge of bundle-n ion
;       iz1      = recombining ion charge of bundle-n ion
;       a25fmt   = subdirectory type of adf25 to be read.
;       outfmt   = format of output ADAS data format for final results
;       cxfile   = file name for charge exchange data input
;       exfile   = file name for map of proj. matrix output
;       ndens    = number of electron densities
;       id_ref   = reference electron density pointer in vectors
;       densa()  = plasma electron density vector (cm-3)
;                  1st dim: index of electron density
;       denpa()  = plasma H+ density vector (cm-3)
;                  1st dim: index of electron density
;       denimpa()= plasma mean impurity ion density (cm-3)
;                  1st dim: index of electron density
;       ntemp    = number of electron temperatures
;       id_ref   = reference electron temp. pointer in vectors
;       tea()    = plasma electron temp. vector (K)
;                  1st dim: index of electron temperature
;       denpa()  = plasma H+ temp. vector (K)
;                  1st dim: index of electron temperature
;       denimpa()= plasma mean impurity ion temp (K)
;                  1st dim: index of electron temperature
;       nzef     = number of plasma zeff
;       iz_ref   = reference zeff pointer in vector
;       zefa()   = plasma zeff vector
;                  1st dim: index of zeff
;       nbeam    = number of beam energies
;       ib_ref   = reference beam energy pointer in vectors
;       bmena()  = beam energy vector (ev/amu)
;                  1st dim: index of beam energies
;       denha()  = beam H+ density vector (cm-3)
;                  1st dim: index of beam energies
;       bmfra()  = fractions of beam at each energy
;                  1st dim: index of beam energies
;       nimp     = number of plasma impurities (excl.h+)
;       im_ref   = reference impurity pointer in vectors
;       zimpa()  = impurity species charge
;                  1st dim: index of impurity
;       amimpa() = atomic mass number of impurity species
;                  1st dim: index of impurity
;       frimpa() = fraction of impurity (normalised to 1)
;                  1st dim: index of impurity
;       ts       = external radiation field temperature (K)
;       w        = general radiation dilution factor
;       w1       = external radiation field dilution factor
;                  for photo-ionisation form the ground level.
;       cion     = adjustment multiplier for ground ionis.
;       cpy      = adjustment multiplier for VR xsects.
;       nip      = range of delta n for IP xsects. (le.4)
;       intd     = order of Maxw. quad. for IP xsects.(le.3)
;       iprs     = 0  => default to VR xsects. beyond nip range
;                  1  => use PR xsects. beyond nip range
;       ilow     = 0  => no special low level data accessed
;                  1  => special low level data accessed
;       ionip    = 0 =>   no ion impact collisions included
;                  1 =>ion impact excit. and ionis. included
;       nionip   = range of delta n for ion impact
;                  excitation xsects.
;       ilprs    = 0 => default to vainshtein xsects.
;                  1 => use lodge-percival-richards xsects.
;       ivdisp   = 0 => ion impact at thermal Maxw. energies
;                  1 => ion impact at displaced thermal
;                       energies according to the neutral
;                       beam energy parameter
;                    * if(ivdisp=0 then special low level
;                      data for ion impact is not substituted -
;                      only vainshtein and lodge et al.
;                      options are open.  Electron impact
;                      data substitution does occur.
;       nmin     = lowest n-shell for population structure
;       nmax     = highest n-shell for population structure
;       imax     = number of representative n-shells
;       nrep()   = representative n-shells
;                  1st dim: index of representative n-shell
;       wbrep()  = dilution factors for nmin->nrep() trans.
;                  1st dim: index of representative n-shell
;       jdef     = number of n-shell quantum defects
;       def()    = quantum defects for n-shells
;                  1st dim: index of n-shell quantum defects
;                           upwards from nmin
;       jcor     = number of DR Bethe correction factors
;       cor()    = DR Bethe correction factors
;                  1st dim: index of correction factor
;       jmax     = number of DR core transitions
;       epsil()  = reduced energy of core transition
;                  [delta Eij/I_H=(z+1)^2*epsil()]
;                  1st dim: index of DR core transition
;       fij()    = absorption oscillator strength for
;                  DR core transition
;                  1st dim: index of DR core transition
;       wij()    = dilution factor for DR core transition
;                             1st dim: index of DR core transition
;
; Dimensions can be set in IDL code
;   ndtem    = maximum number of electron temperatures
;   ndden    = maximum number of electron densities
;   ndrep    = maximum number of representative n-shells
;   ndcor    = maximum number of DR bethe corrections
;   nddiel   = maximum number of DR core transitions
;   nddef    = maximum number of quantum defects
;   ndimp    = maximum number of plasma impurities
;   ndlev    = maximum number of n-shells
;   ndein    = maximum number of beam energies
;   ndzef    = maximum number of z effectives
;
;
; MODIFIED:
;       1.1    Martin O'Mullane
;                - First version.
;
; VERSION:
;       1.1    08-06-2007
;
;-
;----------------------------------------------------------------------

PRO xxdata_25, file     = file,     $
               fulldata = fulldata, $
               help     = help


; Return help if requested

if keyword_set(help) then begin
    doc_library, 'xxdata_25'
    return
endif

; Check the file is valid

if n_elements(file) eq 0 then message,'A filename must be given'
file_acc, file, exist, read, write, execute, filetype
if exist ne 1 then message, 'adf25 file does not exist '+file
if read ne 1 then message, 'adf25 file cannot be read from this userid '+file

; Dimensions

ndtem = 35L
ndden = 35L
ndrep = 30L
ndcor = 20L
nddiel= 10L
nddef = 10L
ndimp = 10L
ndein = 24L
ndzef = 10L

; Define all output variables

iz0       = 0L
iz1       = 0L
ndens     = 0L
id_ref    = 0L
densa     = dblarr(ndden)
denpa     = dblarr(ndden)
denimpa   = dblarr(ndden)
deniona   = dblarr(ndden)
ntemp     = 0L
it_ref    = 0L
tea       = dblarr(ndtem)
tpa       = dblarr(ndtem)
timpa     = dblarr(ndtem)
tiona     = dblarr(ndtem)
nzef      = 0L
iz_ref    = 0L
zefa      = dblarr(ndzef)
nbeam     = 0L
ib_ref    = 0L
bmena     = dblarr(ndein)
denha     = dblarr(ndein)
bmfra     = dblarr(ndein)
nimp      = 0L
im_ref    = 0L
zimpa     = dblarr(ndimp)
amimpa    = dblarr(ndimp)
frimpa    = dblarr(ndimp)
ts        = 0.0D0
w         = 0.0D0
w1        = 0.0D0
cion      = 0.0D0
cpy       = 0.0D0
nip       = 0L
intd      = 0L
iprs      = 0L
ilow      = 0L
ionip     = 0L
nionip    = 0L
ilprs     = 0L
ivdisp    = 0L
nmin      = 0L
nmax      = 0L
imax      = 0L
nrep      = lonarr(ndrep+1)
wbrep     = dblarr(ndrep)
jdef      = 0L
def       = dblarr(ndrep)
jcor      = 0L
cor       = dblarr(ndcor)
jmax      = 0L
epsil     = dblarr(nddiel)
fij       = dblarr(nddiel)
wij       = dblarr(nddiel)

; Combine all strings and put into a byte array.

dsnin = string(replicate(32B, 120))
strput, dsnin, file, 0

exfile = string(replicate(32B, 120))
cxfile = string(replicate(32B, 120))

a25fmt = 'a25_p316'
outfmt = '12345'

str_vars = a25fmt + outfmt + dsnin + exfile + cxfile
str_vars = byte(str_vars)
str_vars = long(str_vars)



; Get binary directory and setup inputs

fortdir = getenv('ADASFORT')

; Call shared object

dummy = call_external(fortdir+'/xxdata_25_if.so','xxdata_25_if'     , $
                      ndtem     , ndden     , ndrep     , ndcor     , $
                      nddiel    , nddef     , ndimp     , ndein     , $
                      ndzef     , iz0       , iz1       , ndens     , $
                      id_ref    , densa     , denpa     , denimpa   , $
                      deniona   , ntemp     , it_ref    , tea       , $
                      tpa       , timpa     , tiona     , nzef      , $
                      iz_ref    , zefa      , nbeam     , ib_ref    , $
                      bmena     , denha     , bmfra     , nimp      , $
                      im_ref    , zimpa     , amimpa    , frimpa    , $
                      ts        , w         , w1        , cion      , $
                      cpy       , nip       , intd      , iprs      , $
                      ilow      , ionip     , nionip    , ilprs     , $
                      ivdisp    , nmin      , nmax      , imax      , $
                      nrep      , wbrep     , jdef      , def       , $
                      jcor      , cor       , jmax      , epsil     , $
                      fij       , wij       , str_vars  )

; Get string out of str_var

res = byte(str_vars)
res = string(res)


a25fmt = strmid(res, 0, 8)
outfmt = strmid(res, 8, 5)
exfile = strmid(res, 133, 120)
cxfile = strmid(res, 253, 120)


; Assemble output data structure
;
; note special treatment for no impurity case

if nimp EQ 0 then begin
    im_ref  = 1
    zimpa   = [0.0]
    amimpa  = [0.0]
    frimpa  = [0.0]
endif

fulldata={  a25file   :  dsnin,              $
            iz0       :  iz0,                $
            iz1       :  iz1,                $
            a25fmt    :  a25fmt,             $
            outfmt    :  outfmt,             $
            exfile    :  exfile,             $
            cxfile    :  cxfile,             $
            ndens     :  ndens,              $
            id_ref    :  id_ref,             $
            densa     :  densa[0:ndens-1],   $
            denpa     :  denpa[0:ndens-1],   $
            denimpa   :  denimpa[0:ndens-1], $
            deniona   :  deniona[0:ndens-1], $
            ntemp     :  ntemp,              $
            it_ref    :  it_ref,             $
            tea       :  tea[0:ntemp-1],     $
            tpa       :  tpa[0:ntemp-1],     $
            timpa     :  timpa[0:ntemp-1],   $
            tiona     :  tiona[0:ntemp-1],   $
            nzef      :  nzef,               $
            iz_ref    :  iz_ref,             $
            zefa      :  zefa[0:nzef-1],     $
            nbeam     :  nbeam,              $
            ib_ref    :  ib_ref,             $
            bmena     :  bmena[0:nbeam-1],   $
            denha     :  denha[0:nbeam-1],   $
            bmfra     :  bmfra[0:nbeam-1],   $
            nimp      :  nimp,               $
            im_ref    :  im_ref,             $
            zimpa     :  zimpa,              $
            amimpa    :  amimpa,             $
            frimpa    :  frimpa,             $
            ts        :  ts,                 $
            w         :  w,                  $
            w1        :  w1,                 $
            cion      :  cion,               $
            cpy       :  cpy,                $
            nip       :  nip,                $
            intd      :  intd,               $
            iprs      :  iprs,               $
            ilow      :  ilow,               $
            ionip     :  ionip,              $
            nionip    :  nionip,             $
            ilprs     :  ilprs,              $
            ivdisp    :  ivdisp,             $
            nmin      :  nmin ,              $
            nmax      :  nmax,               $
            imax      :  imax ,              $
            nrep      :  nrep[0:imax-1],     $
            wbrep     :  wbrep[0:imax-2],    $
            jdef      :  jdef,               $
            def       :  def[0:imax-2],      $
            jcor      :  jcor,               $
            cor       :  cor [0:jcor-1],     $
            jmax      :  jmax,               $
            epsil     :  epsil[0:jmax-1],    $
            fij       :  fij[0:jmax-1],      $
            wij       :  wij[0:jmax-1]       }


END
