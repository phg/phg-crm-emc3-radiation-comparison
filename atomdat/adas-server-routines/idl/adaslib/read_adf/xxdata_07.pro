;----------------------------------------------------------------------
;+
; PROJECT    :  ADAS
;
; NAME       :  xxdata_07
;
; PURPOSE    :  Reads and ADF07 file into an IDL structure
;
; INPUTS     :  file - Fully path of ADF07 file to read
;
; OUTPUTS    :  fulldata: Structure containing contents of ADF07 file:
;               esym                 Element Symbol
;               iz0                  Nuclear charge
;               nblock               Number of ionisation rates
;               iz[nblock]           Initial charge
;               iz1 [nblock]         Final charge
;               cicode[nblock]       Initial state metastable index
;               cfcode[nblock]       Final   state metastable index
;               ciion[nblock]        Initial ion (as <esym>+(iz[nblock]> )
;               cfion[nblock]        Final   ion (as <esym>+<iz1[nblock]>)
;               bwno[nblock]         Effective ionization potential (cm-1)
;               nte[nblock]          Number of temperatures in each block
;               te[nte,nblock]       Temperatures
;               szd[max(nte),nblock] Ionisation rates
;               filename             Filename read
;
; NOTES      :  Calls fortran code via C wrapper, see:
;                       xxdata_07_if.c
;                       xxdata_07_calc.for
;
; AUTHOR     :  Martin O'Mullane
;
; DATE       :  22-05-2008
;
; MODIFIED:
;       1.1     Martin O'Mullane
;               - First version.
;       1.2     Martin O'Mullane
;               - bwno was not passed out in fulldata structure.
;       1.3     Martin O'Mullane
;               - Extend filename to 132 characters.
;
; VERSION:
;       1.1    22-05-2008
;       1.2    13-10-2018
;
;-
;----------------------------------------------------------------------


PRO xxdata_07,file = file, fulldata = fulldata

if n_elements(file) EQ 0 then message,'Filename must be set'

; Convert filename to byte array

fileb = file
adas_string_pad, fileb, 132
dsnin = byte(fileb)

; Parameters for storage

nstore = 500L
ntdim  = 24L

; Allocate space

iz0    = 0L
esym   = bytarr(2)
nbsel  = 0L
isela  = lonarr(nstore)
iz     = lonarr(nstore)
iz1    = lonarr(nstore)
cicode = bytarr(2, nstore)
cfcode = bytarr(2, nstore)
ciion  = bytarr(5, nstore)
cfion  = bytarr(5, nstore)
bwno   = dblarr(nstore)
ita    = lonarr(nstore)
teta   = dblarr(ntdim, nstore)
szd    = dblarr(ntdim, nstore)


; Get Path

adasfort = getenv('ADASFORT')
if adasfort eq '' then message, 'Cannot locate ADAS binaries'

; Call wrapper routine

res = call_external(adasfort+'/xxdata_07_if.so','xxdata_07',   $
                   dsnin,                                      $
                   nstore , ntdim  ,                           $
                   esym   , iz0    ,                           $
                   nbsel  , isela  ,                           $
                   iz     , iz1    ,                           $
                   cicode , cfcode , ciion  , cfion ,          $
                   bwno   ,                                    $
                   ita    ,                                    $
                   teta   , szd   )

; Convert output back to strings

cicode = string(cicode)
cfcode = string(cfcode)
ciion  = string(ciion)
cfion  = string(cfion)
esym   = string(esym)

; Return the valid stuff

isela  = isela[0:nbsel-1]
iz     = iz[0:nbsel-1]
iz1    = iz1[0:nbsel-1]
cicode = cicode[0:nbsel-1]
cfcode = cfcode[0:nbsel-1]
ciion  = ciion[0:nbsel-1]
cfion  = cfion[0:nbsel-1]
ita    = ita[0:nbsel-1]
bwno   = bwno[0:nbsel-1]

teta = teta[0:max(ita)-1, 0:nbsel-1]
szd  = szd[0:max(ita)-1, 0:nbsel-1]


; Put in structure

if arg_present(fulldata) then fulldata={ esym      :  esym,       $
                                         iz0       :  iz0,        $
                                         iz        :  iz,         $
                                         iz1       :  iz1,        $
                                         nblock    :  nbsel,      $
                                         cicode    :  cicode ,    $
                                         cfcode    :  cfcode,     $
                                         ciion     :  ciion  ,    $
                                         cfion     :  cfion,      $
                                         bwno      :  bwno,       $
                                         nte       :  ita,        $
                                         te        :  teta,       $
                                         szd       :  szd,        $
                                         filename  :  file        }

END
