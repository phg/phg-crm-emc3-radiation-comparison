;----------------------------------------------------------------------
;+
; PROJECT    :  ADAS
;
; NAME       :  read_adf15
;
; PURPOSE    :  Reads adf15 (PEC) files from the IDL command line.
;               called from IDL using the syntax
;               read_adf15,file=...,block=...,te=... etc
;
; ARGUMENTS  :  All output arguments will be defined appropriately.
;
;               NAME      I/O    TYPE   DETAILS
; REQUIRED   :  file       I     str    full name of ADAS adf15 file
;               block      I     int    selected block
;               te         I     real() temperatures requested
;               dens       I     real() densities requested
; OPTIONAL      data       O      -     PEC data
;               wlngth     O      -     wavelength of transition
;               iz0        O     int    return guess of nuclear charge
;                                       (returns -1 if unable to guess)
;               izz        O     int    return guess of ion charge
;                                       (returns -1 if unable to guess)
;               iz1        O     int    return guess of ion charge+1
;                                       (returns -1 if unable to guess)
;               fulldata   O     struc   all of the data from xxdata_15.
;                                a15file  = adf15 filename
;                                a04file  = adf04 file name if available
;                                esym     = emitting ion - element symbol
;                                iz0      = emitting ion - nuclear charge
;                                is       = emitting ion - charge
;                                           (generalised to superstage label)
;                                is1      = emitting ion - charge + 1
;                                           (generalised to superstage index= is + 1)
;                                nptn()   = number of partitions in partition level
;                                           1st dim: partition level
;                                nptnc(,) = number of components in partition
;                                           1st dim: partition level
;                                           2nd dim: member partition in partition level
;                                iptnla() = partition level label (0=resolved root,1=unresolved root)
;                                           1st dim: partition level index
;                                iptna(,) = partition member label (labelling starts at 0)
;                                           1st dim: partition level index
;                                           2nd dim: member partition index in partition
;                                           level
;                                iptnca(,,)= component label (labelling starts at 0)
;                                           1st dim: partition level index
;                                           2nd dim: member partition index in partition level
;                                           3rd dim: component index of member partition
;                                icnctv()  = connection vector of number of partitions
;                                            of each superstage in resolved case
;                                            including the bare nucleus
;                                            1st dim: connection vector index
;                                cptn_stack()= text lines in partition block
;                                              1st dim: text line index
;                                lres      = 1  => partial file
;                                          = 0 => not partial file
;                                lptn      = 1  => partition block present
;                                          = 0 => partition block not present
;                                lsup      = 1  => ss use of filmem field
;                                          = 0 => old use of filmem field
;                                cwavel() = wavelength string (angstroms)
;                                           1st dim: data-block index
;                                cfile()  = specific ion file source string in older
;                                           forms.  Field not present in superstage
;                                           version, but reused for added information
;                                           1st dim: data-block index
;                                ctype()  = data type string
;                                           1st dim: data-block index
;                                cindm()  = metastable index string
;                                           1st dim: data-block index
;                                wavel()   = wavelength (angstroms)
;                                            dimension: data-block index
;                                isppr()   = parent index for each line block
;                                            1st dim: index of block in adf15 file
;                                ispbr()   = base index for each line block
;                                            1st dim: index of block in adf15 file
;                                isstgr()  = s1 for each resolved data block
;                                            1st dim: index of block in adf15 file
;                                iszr()    = ion charge relating to each line
;                                            1st dim: index of block in adf15 file
;                                nte()     = number of electron temperatures
;                                            dimension: data-block index
;                                ndens()     = read - number of electron densities
;                                            1st dim: data-block index
;                                te(,)     =  electron temperatures (units: ev)
;                                            1st dim: electron temperature index
;                                            2nd dim: data-block index
;                                dens(,)   = electron densities (units: cm-3)
;                                            1st dim: electron density index
;                                            2nd dim: data-block index
;                                pec(,,)   = photon emissivity coeffts
;                                            1st dim: electron temperature index
;                                            2nd dim: electron density index
;                                            3rd dim: data-block index
;                                pec_max() = photon emissivity coefft. maximum
;                                            as a function of Te at first Ne value
;                                            1st dim: data-block index
;                                tran_index = transition index within adf04 file
;                                power_rank = the rank by its contribution to 
;                                             the total power
;
;
; KEYWORDS      all        I      -     if specified data is 2D of
;                                       temperature and density.
;               kelvin     I      -     requested temperature in K (default eV)
;
; NOTES      :  This is part of a chain of programs - read_adf15.c and
;               readadf15.for are required.
;
; AUTHOR     :  Martin O'Mullane
;
; DATE       :  20-07-99
;
; MODIFIED   :
;       1.2     Martin O'Mullane
;                 - No limit on number of Te/dens returned
;                 - Returns a 2D array if /all is specified.
;                 - Temperatures can be requested in K if /kelvin is specified.
;       1.3     Martin O'Mullane
;                 - Regularised comments
;       1.4     Allan Whiteford
;                 - Added in functionality to guess at nuclear and ion charge.
;       1.5     Allan Whiteford
;                 - Added /help keyword.
;       1.6     Martin O'Mullane
;                 - Add fulldata.
;       1.7     Martin O'Mullane
;                 - Increase nstore to 500.
;       1.8     Martin O'Mullane
;                 - Add fulldata definition to comments.
;       1.9     Martin O'Mullane
;                 - New version of xxdata_15 for superstaging with
;                   an extended fulldata return.
;       1.10    Martin O'Mullane
;                 - Increase MAXVAL to 99 to match e3spln.for.
;       1.11    Martin O'Mullane
;                 - Extend permissible length of adf15 filename to 
;                   132 characters.
;       1.12    Martin O'Mullane
;                 - Use longs for loop control when /all is set.
;
; VERSION    :
;       1.1    20-07-1999
;       1.2    05-03-2001
;       1.3    15-03-2002
;       1.4    08-05-2003
;       1.5    07-04-2005
;       1.6    12-04-2005
;       1.7    22-04-2005
;       1.8    17-02-2006
;       1.9    30-08-2007
;       1.10   06-07-2010
;       1.11   04-05-2018
;       1.12   21-08-2018
;-
;----------------------------------------------------------------------

PRO read_adf15, file     = file,     $
                block    = block,    $
                te       = te,       $
                dens     = dens,     $
                data     = data,     $
                wlngth   = wlngth,   $
                all      = all,      $
                kelvin   = kelvin,   $
                iz0      = iz0,      $
                izz      = izz,      $
                iz1      = iz1,      $
                fulldata = fulldata, $
                help     = help

; If asked for help

if keyword_set(help) then begin
   doc_library, 'read_adf15'
   return
endif

; Check that we get all inputs and that they are correct. Otherwise print
; a message and return to command line

on_error, 2

; file name

if n_elements(file) eq 0 then message, 'A file name must be passed'
file_acc, file, exist, read, write, execute, filetype
if exist ne 1 then message, 'PEC file does not exist '+file
if read ne 1 then message, 'PEC file cannot be read from this userid '+file

dsname = file
adas_string_pad, dsname, 132
dsnin = byte(dsname)



; If fulldata selected retrieve the data into a structure and return.

if arg_present(fulldata) then begin

   xxdata_15, file=file, fulldata=fulldata
   return

endif


; selection block

if n_elements(block) eq 0 then message, 'A PEC index must be selected'
parsize=size(block, /N_DIMENSIONS)
if parsize ne 0 then message,'Selection index cannot be an array'
partype=size(block, /TYPE)
if (partype ne 2) and (partype ne 3) then begin
   message,'Selection index must be numeric'
endif else ibsel = LONG(block)



; temperature and density

if n_elements(te) eq 0 then message, 'User requested temperatures are missing'
if n_elements(dens) eq 0 then message, 'User requested densities are missing'

partype=size(te, /type)
if (partype lt 2) or (partype gt 5) then begin
   message,'Temperature must be numeric'
endif  else te = DOUBLE(te)

partype=size(dens, /type)
if (partype lt 2) or (partype gt 5) then begin
   message,'densmperature must be numeric'
endif else dens = DOUBLE(dens)

; Kelvin rather than eV

if keyword_set(kelvin) then te = te/11605.0

if n_elements(wlngth) eq 0 then wlngth = DOUBLE(0.0) else wlngth = DOUBLE(wlngth)



; Get the data by calling the fortran routine

; Do we want a 2D array (/all set) or 1D on (te, dens) vector

len_te   = n_elements(te)
len_dens = n_elements(dens)

if keyword_set(all) then begin

   ; make a big 1D list

   t_in = dblarr(len_te*len_dens)
   d_in = dblarr(len_te*len_dens)

   k = 0L
   for i = 0, len_te-1 do begin
      for j = 0, len_dens-1 do begin
         t_in[k] = te[i]
         d_in[k] = dens[j]
         k = k + 1
       endfor
    endfor
    itval = k


endif else begin

   ; are te and dens the same length and one dimensional?
   ; if so define data array

   if len_dens ne len_te then print, 'TE and DENS size mismatch - smaller  used'
   itval = min([len_te,len_dens])

   t_in   = te
   d_in   = dens

endelse


itval = LONG(itval)


fortdir = getenv('ADASFORT')


; Get data in blocks of 30 to avoid altering xxspln for large sets.

data = 0.0D0
iz0  = -1L
izz  = -1L
iz1  = -1L

MAXVAL = 99L

n_call = numlines(itval, MAXVAL)

for j = 0L, n_call - 1 do begin

  ist = j*MAXVAL
  ifn = min([(j+1)*MAXVAL,itval])-1

  t_ca     = t_in[ist:ifn]
  d_ca     = d_in[ist:ifn]
  data_ca  = dblarr(ifn-ist+1)
  itval_ca = ifn-ist+1

  dummy = CALL_EXTERNAL(fortdir+'/read_adf15.so','read_adf15',  $
                        dsnin, ibsel,                           $
                        itval_ca, t_ca, d_ca, data_ca,          $
                        wlngth, iz0, izz, iz1)


  data = [data, data_ca]

endfor
data = data[1:*]


if keyword_set(all) then begin

   d_out = dblarr(len_te, len_dens)
   k = 0L                                  ; there must be a better way!
   for i = 0, len_te-1 do begin
      for j = 0, len_dens-1 do begin
         d_out[i,j] = data[k]
         k = k + 1
      endfor
   endfor

   data = d_out

endif

; Reset temperature to K

if keyword_set(kelvin) then te = te*11605.0

END
