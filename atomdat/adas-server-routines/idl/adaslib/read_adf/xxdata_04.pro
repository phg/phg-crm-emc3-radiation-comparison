; Copyright (c) 2002 Strathclyde University.
;----------------------------------------------------------------------
;+
; PROJECT    :  ADAS
;
; NAME       :  xxdata_04
;
; PURPOSE    :  
;
; ARGUMENTS  :  All output arguments will be defined appropriately.
;               Inputs will be converted to correct type, if possible,
;               internally without changing calling type.
; 
;               The argument list is the same as fortran except
;               that the file name is passed rather than a unit
;               number. Logicals are replaced by integers (0 and 1)
;               as IDL does not have a logical type.
;
;
;
;              NAME       I/O    TYPE    DETAILS
; REQUIRED   : a04file     I     str     adf04 file name
;              ndlev       I     long    maximum number of levels
;              ndtrn       I     long    maximum number of transitions
;              ndmet       I     long    maximum number of metastables
;              ndqdn       I     long    maximum number of orbital energies
;              nvmax       I     long    maximum number of temperatures
;
;              itieactn    I     long    1 return data even if some levels are untied.
;                                        0 default behaviour - terminate if untied
;                                          levels are present.
;                                          On output 1 if untied levels present
;                                                    0 for no untied levels.
;
;              titled      O     str     element symbol.
;              iz          O     long    recombined ion charge read
;              iz0         O     long    nuclear charge read
;              iz1         O     long    recombining ion charge read
;              bwno        O     double  ionisation potential (cm-1) of lowest parent
;              npl         O     long    number of parents on first line and used
;                                        in level assignments
;              bwnoa()     O     double  ionisation potential (cm-1) of parents
;              lbseta()    O     long    .true.  - parent weight set for bwnoa()
;                                        .false. - parent weight not set for bwnoa()
;              prtwta()    O     double  parent weight for bwnoa()
;              cprta()     O     str     parent name in brackets
;
;              il          O     long    input data file: number of energy levels
;              qdorb()     O     double  quantum defects for orbitals
;              lqdorb()    O     long    .true.  => source data available for qd.
;                                        .false. => source data not availabe qd.=0.0
;              qdn()       O     double  quantum defect for n-shells.  non-zero only
;                                        for adf04 files with orbital energy data
;              iorb        O     long    input data file: number of orbital energies
;
;              ia()        O     long    energy level index number
;              cstrga()    O     str     nomenclature/configuration for level 'ia()'
;              isa()       O     long    multiplicity for level 'ia()'
;                                        note: (isa-1)/2 = quantum number (s)
;              ila()       O     long    quantum number (l) for level 'ia()'
;              xja()       O     double  quantum number (j-value) for level 'ia()'
;                                        note: (2*xja)+1 = statistical weight
;              wa()        O     double  energy relative to level 1 (cm-1) for level
;                                        'ia()'
;              cpla()      O     str     char. specifying 1st parent for level 'ia()'
;                                          integer - parent in bwnoa() list
;                                          'blank' - parent bwnoa(1)
;                                          'x'     - do not assign a parent
;              npla()      O     long    no. of parent/zeta contributions to ionis.
;                                        of level
;              ipla(,)     O     long    parent index for contributions to ionis.
;                                        of level
;              zpla(,)     O     double  eff. zeta param. for contributions to ionis.
;                                        of level
;
;              nv          O     long    input data file: number of gamma/temperature
;                                        pairs for a given transition.
;              scef()      O     double  input data file: electron temperatures (k)
;                                    
;              itran       O     long    input data file: number of transitions
;              maxlev      O     long    highest index level in read transitions
;
;              tcode()     O     str     transition: data type pointer:
;                                         ' ','1','2','3' => elec. impact trans.
;                                         'p','P' => proton   impact   transition
;                                         'h','H' => charge   exchange recombination
;                                         'r','R' => free     electron recombination
;                                         'i','I' => coll. ionis. from lower stage ion
;                                         's','S' => Ionisation from current ion 
;                                         'l','L' => L-line for unresolved DR emissivity 
;              i1a()       O     long     transition:
;                                         lower energy level index (case ' ' & 'p')
;                                         signed parent index (case 'h','r','s' & 'i')
;              i2a()       O     long        transition:
;                                         upper energy level index (case ' ' & 'p')
;                                         capturing level index (case 'h','r','s' & 'i')
;              aval()      O     double   transition:
;                                         a-value (sec-1)          (case ' ')
;                                         neutral beam energy      (case 'h')
;                                         not used             (case 'p','r' & 'i')
;              scom(,)     O     double   transition:
;                                         gamma values             (case ' ' & 'p')
;                                         rate coefft.(cm3 sec-1)(case 'h','r' & 'i')
;                                         scaled rate coefft.(cm3 sec-1)(case 's')
;              beth()      O     double   transition
;                                           1st Bethe coefficient     (case ' ','1','2')
;              iadftyp     O     long    adf04 type: 1=omega, 3=upsilon, 4=non-maxwl.
;              lprn        O     long    .true.  => multiple parent data on 1st line
;                                        .false. => multiple parent data not present
;              lcpl        O     long    .true.  => parent assignment on level lines
;                                        .false. => parent assignment not present
;              lorb        O     long    .true.  => orbital data on level terminator
;                                        .false. => orbital data not present
;              lbeth       O     long    .true.  => bethe data on e-transition lines
;                                        .false. => bethe data not present
;              letyp       O     long    .true.  => e- excitation transitions present
;              lptyp       O     long    .true.  => p- excitation transitions present
;              lrtyp       O     long    .true.  => recombination transitions present
;              lhtyp       O     long    .true.  => cx transitions present
;              lityp       O     long    .true.  => ionis. trans. from z-1 ion present
;              lstyp       O     long    .true.  => ionis. trans. from current ion present
;              lltyp       O     long    .true.  => 'l'-line for unresolved DR emissivity
;              ltied()     O     long    .true.  => specified level tied
;                                        .false. => specified level is untied
;                          
;
;
; NOTES      :  Calls fortran code, xxdata_04.for.
;
;
; AUTHOR     :  Martin O'Mullane
;
; DATE       :  04/06/03
;
;
; MODIFIED:
;       1.1     Martin O'Mullane
;              	- First version.
;	1.2	Allan Whiteford
;		- Changed wrapper path to be just ADASFORT.
;	1.3	Allan Whiteford
;		- Allowed routine to handle ADF04 files which
;                 contain no transitions.
;       1.4     Martin O'Mullane
;              	- Increase cstrga to 90 but trim to max length on exit.
;       1.5     Martin O'Mullane
;              	- Return an ltied vector, trimmed to the actual number of levels.
;       1.6     Martin O'Mullane
;              	- Allow file length to be up to 200 characters long.
;
;
; VERSION:
;       1.1    04/06/03
;       1.2    10/08/04
;       1.3    31/07/08
;       1.4    04-08-2009
;       1.5    21-09-2015
;       1.6    27-07-2017
;
;-
;----------------------------------------------------------------------

PRO xxdata_04, a04file ,                                         $
               ndlev   , ndtrn  , ndmet    , ndqdn  , nvmax ,    $
               titled  , iz     , iz0      , iz1    , bwno  ,    $
               npl     , bwnoa  , lbseta   , prtwta , cprta ,    $
               il      , qdorb  , lqdorb   , qdn    , iorb  ,    $
               ia      , cstrga , isa      , ila    , xja   ,    $
               wa      ,                                         $
               cpla    , npla   , ipla     , zpla   ,            $
               nv      , scef   ,                                $
               itran   , maxlev ,                                $
               tcode   , i1a    , i2a      , aval   , scom  ,    $
               beth    ,                                         $
               iadftyp , lprn   , lcpl     , lorb   , lbeth ,    $
               letyp   , lptyp  , lrtyp    , lhtyp  , lityp ,    $
               lstyp   , lltyp  , itieactn , ltied
                                                 

; Check number of parameters

if n_params() NE 55 then message, 'Not all parameters present', /continue

; Check of mandatory inputs and convert to LONG type.

if ndlev EQ 0 then message, 'Set maximum number of levels'
if ndtrn EQ 0 then message, 'Set maximum number of transitions'
if ndmet EQ 0 then message, 'Set maximum number of metastables'
if ndqdn EQ 0 then message, 'Set maximum number of orbitals'
if nvmax EQ 0 then message, 'Set maximum number of temperatures/energies'

ndlev = long(ndlev)
ndtrn = long(ndtrn)
ndmet = long(ndmet)
ndqdn = long(ndqdn)
nvmax = long(nvmax)

if n_elements(itieactn) EQ 0 then itieactn = 0L else itieactn = long(itieactn)


; Check that the adf04 file exists

file_acc, a04file, exist, read, write, execute, filetype
if exist ne 1 then message, 'adf04 file does not exist '+a04file
if read ne 1 then message, 'adf04 file cannot be read from this userid '+a04file



; Define types of the output parameters

iz        = -1L
iz0       = -1L
iz1       = -1L
bwno      = -1.0D0
npl       = -1L
bwnoa     = dblarr(ndmet)
il_lbseta = lonarr(ndmet)
prtwta    = dblarr(ndmet)
il        = -1L
qdorb     = dblarr((ndqdn*(ndqdn+1))/2)
il_lqdorb = lonarr((ndqdn*(ndqdn+1))/2)
qdn       = dblarr(ndqdn)
iorb      = -1L
ia        = lonarr(ndlev)
isa       = lonarr(ndlev)
ila       = lonarr(ndlev)
xja       = dblarr(ndlev)
wa        = dblarr(ndlev) 
npla      = lonarr(ndlev)
ipla      = lonarr(ndmet,ndlev)
zpla      = dblarr(ndmet,ndlev)
nv        = -1L
scef      = dblarr(nvmax)
itran     = -1L
maxlev    = 0L
i1a       = lonarr(ndtrn)
i2a       = lonarr(ndtrn)
aval      = dblarr(ndtrn)
scom      = dblarr(nvmax,ndtrn)
beth      = dblarr(ndtrn)
iadftyp   = 0L
il_lprn   = 0L
il_lcpl   = 0L
il_lorb   = 0L   
il_lbeth  = 0L
il_letyp  = 0L
il_lptyp  = 0L
il_lrtyp  = 0L
il_lhtyp  = 0L
il_lityp  = 0L
il_lstyp  = 0L
il_lltyp  = 0L
il_ltied  = lonarr(ndlev)
cpla      = strarr(ndlev) + ' '
cprta     = strarr(ndmet) + '         '
cstrga    = strarr(ndlev) + string(replicate(32B, 90))
tcode     = strarr(ndtrn) + ' '
titled    = '   '

file      = string(replicate(32B, 200))
strput, file, a04file, 0


; Combine string arrays into a byte array

str_vars = [cpla, cprta, cstrga, tcode]
str_vars = byte(str_vars)
str_vars = long(str_vars)

    
; Location of sources

fortdir = getenv('ADASFORT')
fortdir = fortdir

dummy = CALL_EXTERNAL(fortdir+'/xxdata_04_if.so','xxdata_04_if',    $ 
                      i1a, i2a, ia, iadftyp, il, ila,               $
                      iorb, ipla, isa, itieactn, itran, iz,         $
                      iz0,  iz1,  maxlev, ndlev, ndmet, ndqdn,      $
                      ndtrn, npl, npla, nv,  nvmax, il_lbeth,       $ 
                      il_lbseta, il_lcpl, il_letyp, il_lhtyp,       $
                      il_lityp, il_lltyp, il_lorb, il_lprn,         $
                      il_lptyp, il_lqdorb, il_lrtyp, il_lstyp,      $
                      il_ltied, aval, beth, bwno, bwnoa,            $
                      prtwta, qdn, qdorb, scef, scom, wa,           $
                      xja,  zpla, str_vars,                         $
                      titled,  file)

; Convert mangled string arrays and logicals back to calling parameter names

res = byte(str_vars)
res = string(res)

off    = 0
cpla   = res[off:il-1]

off    = ndlev
cprta  = res[off: off+npl-1]

off    = ndlev + ndmet
cstrga = res[off: off+il-1]

ilen = max(strlen(strtrim(cstrga)))
cstrga = strmid(cstrga, 0, ilen)

off    = 2*ndlev + ndmet
if itran gt 0 then begin
	tcode  = res[off: off+itran-1]
endif else begin
	tcode=-1
endelse

lbseta = il_lbseta
lqdorb = il_lqdorb
lprn   = il_lprn  
lcpl   = il_lcpl  
lorb   = il_lorb  
lbeth  = il_lbeth 
letyp  = il_letyp 
lptyp  = il_lptyp 
lrtyp  = il_lrtyp 
lhtyp  = il_lhtyp 
lityp  = il_lityp 
lstyp  = il_lstyp 
lltyp  = il_lltyp

ltied  = il_ltied[0:il-1]

END
