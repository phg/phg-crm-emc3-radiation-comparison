;----------------------------------------------------------------------
;+
; PROJECT    :  ADAS
;
; NAME       :  xxdata_09
;
; PURPOSE    :  Reads adf09 DR files from IDL
;
; ARGUMENTS  :  All output arguments will be defined appropriately.
;               Inputs will be converted to correct type, if possible,
;               internally without changing calling type.
;
;               The argument list is the same as fortran except
;               that the file name is passed rather than a unit
;               number. Logicals are replaced by integers (0 and 1)
;               as IDL does not have a logical type.
;
;
;
;              NAME       I/O    TYPE    DETAILS
; REQUIRED   : file        I     str     adf09 file name
; OPTIONAL   : fulldata    O     -       Output structure (see below)
;              help        I     switch  Help required
;
; The fulldata structure is defined as:
;
; fulldata  = { iz             :  recombined ion charge
;               iz0            :  nuclear charge
;               iz1            :  recombining ion charge
;               sym            :  recombined ion seq
;               ctype          :  resolution type (ls/ic/ca)
;               iform          :  data set organisational form
;                                     1: LS 1993 Badnell data form
;                                     2: LS 2000 Badnell data form
;                                     3: IC 2000 Badnell data form
;                                     4: CA 2000 Badnell data form
;                                     5: hybrid LS 2015 Badnell data form
;                                     6: hybrid IC 2015 Badnell data form
;                                     7: hybrid CA 2015 Badnell data form
;                                     8: unassigned
;                                     9: LS 2016 ADAS standard data form
;                                    10: IC 2016 ADAS standard data form
;                                    11: CA 2016 ADAS standard data form
;               nprnt          :  total number of parents
;               nprnti         :  number of parents which are initial parents
;               nprntf         :  number of parents which are final parents
;               bwnp           :  binding wave no. of ground parent (cm-1)
;               ipa            :  parent energy level index number
;                                   dim : parent lvl/trm/cfg index
;               cstrpa()       :  nomencl./config. for parent level 'ipa()'
;                                   dim: parent lvl/trm/cfg index
;               ispa()         :  multiplicity for parent level 'ipa()'
;                                 note: (ispa-1)/2 = quantum number (sp)
;                                   dim: parent lvl/trm/cfg index
;               ilpa()         :  quantum number (lp) for parent level 'ipa()'
;                                   dim: parent lvl/trm/cfg index
;               xjpa()         :  quantum number (jp) for parent level 'ipa()'
;                                 note: (2*xjpa)+1 = statistical weight
;                                   dim: parent lvl/trm/cfg index
;               wpa()          :  energy relative to parent level 1 (cm-1)
;                                 for parent level 'ipa()'
;                                   dim: parent lvl/trm/cfg index
;               nlvl           :  number of energy levels (lvl/trm/cfg) of
;                                 recombined ion - replaced ncfg for hybrid forms
;               bwnr           :  ionisation potential (cm-1) of lowest level
;                                 of recombined ion
;               ia()           :  recombined ion energy level index number
;                                  dim: lvl/trm/cfg index
;               ip()           :  associated parent of recombined level
;                                  dim: lvl/trm/cfg index
;               cstrga()       :  nomencl./config. for recombined ion level 'ia()'
;                                  dim: lvl/trm/cfg index
;               isa()          :  multiplicity for recombined level 'ia()'
;                                 note: (isa-1)/2 = quantum number (s)
;                                  dim: lvl/trm/cfg index
;               ila()          :  quantum number (l) for recombined level 'ia()'
;                                  dim: lvl/trm/cfg index
;               xja()          :  quantum number (j) for recombined level 'ia()'
;                                 note: (2*xja)+1 = statistical weight
;                                  dim: lvl/trm/cfg index
;               wa()           :  energy relative to recombined level 1 (cm-1)
;                                 for recombined level 'ia()'
;                                  dim: lvl/trm/cfg index
;               auga_res(,)    :  specific auger rates for resolved levels
;                                   dim: lvl/trm/cfg index
;                                   dim: parent pair index
;               lauga_res(,)   :  1 (true)  => auger rate present for lvl/trm/cfg
;                                 0 (false) => auger rate not present
;                                  dim: lvl/trm/cfg index
;                                  dim: parent pair index
;               nlrep          :  number of representative nl-shells
;               iaprs_nl       :  number of nl-shell auger rate initial
;                                 and final parent pairs
;               caprs_nl()     :  nl-shell auger rate parent pair string
;                                  dim: parent pair index
;               ipaug_nl(,)    :  initial and final parents index
;                                 for nl-shell auger breakups
;                                  dim: parent pair index
;                                  dim: 2 (for initial and final parent)
;               irepa_nl()     :  representative nl-shell index number
;                                   dim: representative nl-shell index
;               irepa_nl()     :  representative nl-shell index number
;                                   dim: representative nl-shell index
;               nlrepa_n()     :  n value for representative nl-shell
;                                   dim: representative nl-shell index
;               nlrepa_l()     :  l value for representative nl-shell
;                                   dim: representative nl-shell index
;               auga_nl(,)     :  auger rates (sec-1)
;                                  dim: representative nl-shell index
;                                  dim: parent pair index
;               lauga_nl(,)    :  1 (true)  => auger rate present for nl-shell
;                                 0 (false) => auger rate not present
;                                   dim: representative n-shell index
;                                   dim: parent pair index
;               nrep           :  number of representative n-shells
;               iaprs_n        :  number of n-shell auger rate initial
;                                 and final parent pairs
;               caprs_n()      :  auger rate parent pair string
;                                  dim: parent pair index
;               ipaug_n(,)     :  initial and final parents index
;                                 for n-shell auger breakups
;                                  dim: parent pair index
;                                  dim: 2 (for initial and final parent)
;               irepa_n()      :  representative n-shell index number
;                                   dim: representative n-shell index
;               nrepa()        :  n value for representative n-shell
;                                   dim: representative n-shell index
;               auga_n(,)      :  auger rates (sec-1)
;                                  dim: representative n-shell index
;                                  dim: parent pair index
;               lauga_n(,)     :  1 (true)  => auger rate present for n-shell
;                                 0 (false) => auger rate not present
;                                  dim: representative n-shell index
;                                  dim: parent pair index
;               iprti()        :  initial parent index from block header
;                                   dim: initial parent index number
;               diel_res(,,)   :  lvl/trm/cfg resol. diel. coeffts.(cm3 s-1)
;                                  dim: level index
;                                  dim: initial parent index
;                                  dim: temperature index
;               ldiel_res(,)   :  1 (true)  => diel. present for lvl/trm/cfg index
;                                 0 (false) => diel. not present for
;                                              lvl/trm/cfg index
;                                  dim: level index
;                                  dim: initial parent index
;               iprtf(,)       :  final parent index from block header
;                                      dim: initial parent index number
;                                      dim: final parent index number
;               nsysf(,)       :  no. of spin systems built on final parent
;                                 defaults to 1 for /ic/ca/
;                                   dim: initial parent index number
;                                   dim: final parent index number
;               isys(,,)       :  n-shell spin system index for final parent
;                                 defaults to 1 for /ic/ca/
;                                   dim: initial parent index number
;                                   dim: final parent index number
;                                   dim: spin system index number
;               ispsys(,,)     :  n-shell spin system for final parent
;                                 applicable to /ls/ only
;                                   dim: initial parent index number
;                                   dim: final parent index number
;                                   dim: spin system index number
;               cspsys(,,)     :  characterising string for final parent
;                                 system - universal for /ls/ic/ca/
;                                   dim: initial parent index number
;                                   dim: final parent index number
;                                   dim: spin system index number
;               diel_nl(,,,,)  :  nl-shell dielec. coeffts.(cm3 s-1)
;                                   dim: repr. nl-shell index
;                                   dim: initial parent index
;                                   dim: final parent index
;                                   dim: spin system index
;                                   dim: temperature index
;               ldiel_nl(,,,)  :  1 (true)  => diel. present for repr. nl-shell
;                                 0 (false) => diel. not present for  nl-shell
;                                  dim: repr. nl-shell index
;                                  dim: initial parent index
;                                  dim: final parent index
;                                  dim: spin system index
;               diel_n(,,,,)   :  n-shell dielec. coeffts.(cm3 s-1)
;                                  dim: repr. n-shell index
;                                  dim: initial parent index
;                                  dim: final parent index
;                                  dim: spin system index
;                                  dim: temperature index
;               ldiel_n(,,,,)  :  1 (true)  => diel. present for repr. n-shell
;                                 0 (false) => diel. not present for  n-shell
;                                  dim: repr. n-shell index
;                                  dim: initial parent index
;                                  dim: final parent index
;                                  dim: spin system index
;               diel_sum(,,,)  :  sum of n-shell dielec. coeffts.(cm3 s-1)
;                                   dim: initial parent index
;                                   dim: final parent index
;                                   dim: spin system index
;                                   dim: temperature index
;               ldiel_sum(,,)  :  1 (true)  => diel. sum present
;                                 0 (false) => diel. sum not present
;                                  dim: initial parent index
;                                  dim: final parent index
;                                  dim: spin system index
;               diel_tot(,)    :  total diel coefficients (cm3 s-1) tabulated
;                                 in dataset
;                                  dim: initial parent index
;                                  dim: electron temperature index
;               ldiel_tot()    :  1 (true)  => diel. total present in dataset
;                                 0 (false) => diel. total not present
;                                  dim: initial parent index
;               nte            :  no. of electron temperatures
;               tea()          :  electron temperatures (k)
;                                  dim: electron temperature index
;
;
; Additional fields for hybrid forms of adf09 files:
;
;               ncfg           : number of averaged energy levels (lvl/trm/cfg)
;                                of recombined ion for hybrid adf09
;               iscp()         : parent of IP straddling nl-level
;                                1st.dim: cfg index
;               iscn()         : n quantum number of IP straddling  nl-level
;                                1st.dim: cfg index
;               iscl()         : l quantum number of IP straddling  nl-level
;                                1st.dim: cfg index
;               fsc(,)         : fractional statistical weight of the levels
;                                of a config which lie below and above the IP
;                                1st.dim: index
;                                2nd.dim: 1 -> below, 2-> above
;               wsc(,)         : the statistical average energies of the levels
;                                of a config which lie below and above the IP
;                                1st.dim: index
;                                2nd.dim: 1 -> below, 2-> above
;               ipca()         : parent config averaged index number          
;                                1st dim: parent cfg index                    
;               cstrpca()      : nomencl./config. for parent config 'ipca()'
;                                1st dim: parent cfg index                    
;               xjpca()        : quantum number (jp) for parent level 'ipca()'
;                                note: (2*xjpa)+1 = statistical weight        
;                                1st dim: parent cfg index                    
;               wpca()         : energy relative to parent level 1 (cm-1)     
;                                for parent level 'ipca()'     
;                                1st dim: parent cfg index     
;
; NOTES      :  Calls fortran code, xxdata_09.for.
;
; AUTHOR     :  Allan Whiteford
;
; DATE       :  02/05/05
;
;
; MODIFIED:
;       1.1     Allan Whiteford
;               - First version.
;       1.2     Martin O'Mullane
;               - Add variables for latest version of xxdata_09.for.
;       1.3     Martin O'Mullane
;               - diel_tot is tabulated on initial, not total, parents.
;       1.4     Martin O'Mullane
;               - ldiel_tot tag name in fulldata was given as ildiel_tot.
;               - Increase dimensions for levels and Auger rates.
;               - Restrict number of parents to ndprt.
;       1.5     Martin O'Mullane
;               - Adjustments for hybrid adf09 forms. Some fields in the
;                 output structure are changed and there are some additional
;                 variables.
;       1.6     Martin O'Mullane
;               - Add ipca, cstrpca, xjpca and wpca for the parent
;                 config for types 5 and 6 (hybrid ls/ic).
;               - Use nprnti, rather than nprnt, for the initial
;                 parents in types 5 and 6.
;       1.7     Martin O'Mullane
;               - Increase ndprt to 100 to read in hybrid tin datasets.
;
; VERSION:
;       1.1    02-05-2005
;       1.2    17-08-2009
;       1.3    22-07-2010
;       1.4    10-06-2013
;       1.5    08-04-2016
;       1.6    08-01-2018
;       1.7    19-05-2019
;
;-
;----------------------------------------------------------------------

pro xxdata_09, file=file, fulldata=fulldata, help=help

; Return help if requested

    if keyword_set(help) then begin
        doc_library, 'xxdata_09'
        return
    endif

; Check the file is valid

    if n_elements(file) eq 0 then message,'A filename must be given'
    file_acc, file, exist, read, write, execute, filetype
    if exist ne 1 then message, 'adf09 file does not exist '+file
    if read ne 1 then message, 'adf09 file cannot be read from this userid '+file

    dnsfull=file

; Dimensions

    ndprt  = 100L
    ndrep  = 250L
    ndlrep = 500L
    ndlev  = 16000L
    ndaug  = 80L
    ndt    = 20L
    ndsc   = 1000L

; Initialise variables

    iz           =    0L
    iz0          =    0L
    iz1          =    0L
    iform        =    0L
    nprnt        =    0L
    nprnti       =    0L
    nprntf       =    0L
    bwnp         =    0.0D0
    ipa          =    lonarr(ndprt)
    ipca         =    lonarr(ndprt)
    ispa         =    lonarr(ndprt)
    ilpa         =    lonarr(ndprt)
    xjpa         =    dblarr(ndprt)
    xjpca        =    dblarr(ndprt)
    wpa          =    dblarr(ndprt)
    wpca         =    dblarr(ndprt)
    nlvl         =    0L
    ncfg         =    0L
    nippy        =    0L
    bwnr         =    0.0D0
    ia           =    lonarr(ndlev)
    ip           =    lonarr(ndlev)
    isa          =    lonarr(ndlev)
    ila          =    lonarr(ndlev)
    xja          =    dblarr(ndlev)
    wa           =    dblarr(ndlev)
    iscp         =    lonarr(ndsc)
    iscn         =    lonarr(ndsc)
    iscl         =    lonarr(ndsc)
    fsc          =    dblarr(ndsc,2)
    wnsc         =    dblarr(ndsc,2)
    auga_res     =    dblarr(ndlev,ndprt)
    ilauga_res   =    lonarr(ndlev,ndprt)
    nlrep        =    0L
    iaprs_nl     =    0L
    ipaug_nl     =    lonarr(ndaug,2)
    irepa_nl     =    lonarr(ndlrep)
    nlrepa_n     =    lonarr(ndlrep)
    nlrepa_l     =    lonarr(ndlrep)
    auga_nl      =    dblarr(ndlrep,ndaug)
    ilauga_nl    =    lonarr(ndlrep,ndaug)
    nrep         =    0L
    iaprs_n      =    0L
    ipaug_n      =    lonarr(ndaug,2)
    irepa_n      =    lonarr(ndrep)
    nrepa        =    lonarr(ndrep)
    auga_n       =    dblarr(ndrep,ndaug)
    ilauga_n     =    lonarr(ndrep,ndaug)
    iprti        =    lonarr(ndprt)
    diel_res     =    dblarr(ndlev,ndprt,ndt)
    ildiel_res   =    lonarr(ndlev,ndprt)
    iprtf        =    lonarr(ndprt,ndprt)
    nsysf        =    lonarr(ndprt,ndprt)
    isys         =    lonarr(ndprt,ndprt,2)
    ispsys       =    lonarr(ndprt,ndprt,2)
    diel_nl      =    dblarr(ndlrep,ndprt,ndprt,2,ndt)
    ildiel_nl    =    lonarr(ndlrep,ndprt,ndprt,2)
    diel_n       =    dblarr(ndrep,ndprt,ndprt,2,ndt)
    ildiel_n     =    lonarr(ndrep,ndprt,ndprt,2)
    diel_sum     =    dblarr(ndprt,ndprt,2,ndt)
    ildiel_sum   =    lonarr(ndprt,ndprt,2)
    diel_tot     =    dblarr(ndprt,ndt)
    ildiel_tot   =    lonarr(ndprt)
    nte          =    0L
    tea          =    dblarr(ndt)

; To reserve memory we need to fill these strings full of blanks

    ctype     = '  '
    ictype    = long(byte(ctype))
    seqsym    = '  '
    iseqsym   = long(byte(seqsym))

    cstrga    = strarr(ndlev) + '                    '
    icstrga   = long(byte(cstrga))
    cstrpa    = strarr(ndprt) + '                    '
    icstrpa   = long(byte(cstrpa))
    cstrpca   = strarr(ndprt) + '                    '
    icstrpca  = long(byte(cstrpa))
    cspsys    = strarr(ndprt,ndprt,2) + '                              '
    icspsys   = long(byte(cspsys))
    caprs_n   = strarr(ndaug) + '          '
    icaprs_n  = long(byte(caprs_n))
    caprs_nl  = strarr(ndaug) + '          '
    icaprs_nl = long(byte(caprs_nl))


; Get binary directory and setup inputs

    fortdir = getenv('ADASFORT')

; Call shared object

    dummy = call_external(fortdir+'/xxdata_09_if.so','xxdata_09_if',     $
                           ndprt    , ndrep    , ndlrep   ,              $
                           ndlev    , ndaug    , ndt      , ndsc     ,   $
                           dnsfull  ,                                    $
                           iseqsym  , iz       , iz0      , iz1      ,   $
                           ictype   , iform    ,                         $
                           nprnt    , nprnti   , nprntf   ,              $
                           bwnp     ,                                    $
                           ipa      , icstrpa  , ispa     , ilpa     ,   $
                           xjpa     , wpa      ,                         $
                           ipca     , icstrpca , xjpca    , wpca     ,   $
                           nlvl     , ncfg     , nippy    , bwnr     ,   $
                           ia       , ip       , icstrga  , isa      ,   $
                           ila      , xja      , wa       ,              $
                           iscp     , iscn     , iscl     ,              $
                           fsc      , wnsc     ,                         $
                           auga_res , ilauga_res,                        $
                           nlrep    , iaprs_nl , icaprs_nl, ipaug_nl ,   $
                           irepa_nl , nlrepa_n , nlrepa_l , auga_nl  ,   $
                           ilauga_nl ,                                   $
                           nrep     , iaprs_n  , icaprs_n , ipaug_n  ,   $
                           irepa_n  , nrepa    , auga_n   , ilauga_n  ,  $
                           iprti    ,                                    $
                           diel_res , ildiel_res,                        $
                           iprtf    ,                                    $
                           nsysf    , isys     , ispsys   , icspsys  ,   $
                           diel_nl  , ildiel_nl , diel_n   , ildiel_n  , $
                           diel_sum , ildiel_sum, diel_tot , ildiel_tot, $
                           nte      , tea )

; Restrict the number of final parents to ndprt

nprntf = min([ndprt, nprntf])


; Convert string and logical variables from integers

    seqsym   = string(byte(iseqsym))
    ctype    = string(byte(ictype))
    cstrga   = string(byte(icstrga))
    cstrpa   = string(byte(icstrpa))
    cstrpca  = string(byte(icstrpca))
    cspsys   = string(byte(icspsys))
    caprs_n  = string(byte(icaprs_n))
    caprs_nl = string(byte(icaprs_nl))


; Throw away unncessary data - note hybrid forms have fewer fields than
; the others but also hold extra information on the IP straddling
; configurations.

if (iform EQ 5 OR iform EQ 6 OR iform EQ 7) then begin

    ipa          =    ipa[0:nprnti-1]
    ispa         =    ispa[0:nprnti-1]
    ilpa         =    ilpa[0:nprnti-1]
    xjpa         =    xjpa[0:nprnti-1]
    wpa          =    wpa[0:nprnti-1]
    ia           =    ia[0:ncfg-1]
    ip           =    ip[0:ncfg-1]
    isa          =    isa[0:ncfg-1]
    ila          =    ila[0:ncfg-1]
    xja          =    xja[0:ncfg-1]
    wa           =    wa[0:ncfg-1]
    auga_res     =    auga_res[0:ncfg-1,0:nprnt-1]
    ilauga_res   =    ilauga_res[0:ncfg-1,0:nprnt-1]

    if nlrep GT 0 then begin
       irepa_nl     =    irepa_nl[0:nlrep-1]
       nlrepa_n     =    nlrepa_n[0:nlrep-1]
       nlrepa_l     =    nlrepa_l[0:nlrep-1]
       diel_nl      =    diel_nl[0:nlrep-1,0:nprnti-1,0:nprntf-1,*,0:nte-1]
       ildiel_nl    =    ildiel_nl[0:nlrep-1,0:nprnti-1,0:nprntf-1,*]
    endif else begin
       irepa_nl     =    0L
       nlrepa_n     =    0L
       nlrepa_l     =    0L
       diel_nl      =    0.0D0
       ildiel_nl    =    0L
    endelse

    if iaprs_nl GE 1 then begin
       ipaug_nl     =  ipaug_nl[0:iaprs_nl-1,*]
       auga_nl      =  auga_nl[0:nlrep-1,0:iaprs_nl-1]
       ilauga_nl    =  ilauga_nl[0:nlrep-1,0:iaprs_nl-1]
       caprs_nl     =  caprs_nl[0:iaprs_n-1]
    endif else begin
       ipaug_nl     =  0L
       auga_nl      =  0.0D0
       ilauga_nl    =  0L
       caprs_nl     =  ' '
    endelse

    if iaprs_n GE 1 then begin
       ipaug_n      =  ipaug_n[0:iaprs_n-1,*]
       auga_n       =  auga_n[0:nrep-1,0:iaprs_n-1]
       ilauga_n     =  ilauga_n[0:nrep-1,0:iaprs_n-1]
       caprs_n      =  caprs_n[0:iaprs_n-1]
    endif else begin
       ipaug_n      =  0L
       auga_n       =  0.0D0
       ilauga_n     =  0L
       caprs_n      =  ' '
    endelse

    irepa_n    = irepa_n[0:nrep-1]
    nrepa      = nrepa[0:nrep-1]
    iprti      = iprti[0:nprnt-1]
    diel_res   = diel_res[0:ncfg-1,0:nprnti-1,0:nte-1]
    ildiel_res = ildiel_res[0:ncfg-1,0:nprnt-1]
    iprtf      = iprtf[0:nprnti-1,0:nprntf-1]
    nsysf      = nsysf[0:nprnti-1,0:nprntf-1]
    isys       = isys[0:nprnti-1,0:nprntf-1,*]
    ispsys     = ispsys[0:nprnti-1,0:nprntf-1,*]
    diel_n     = diel_n[0:nrep-1,0:nprnti-1,0:nprntf-1,*,0:nte-1]
    ildiel_n   = ildiel_n[0:nrep-1,0:nprnti-1,0:nprntf-1,*]
    diel_sum   = diel_sum[0:nprnti-1,0:nprntf-1,*,0:nte-1]
    ildiel_sum = ildiel_sum[0:nprnti-1,0:nprntf-1,*]
    diel_tot   = diel_tot[0:nprnti-1,0:nte-1]
    ildiel_tot = ildiel_tot[0:nprnti-1]
    tea        = tea[0:nte-1]
    cstrpa     = cstrpa[0:nprnt-1]
    cstrga     = cstrga[0:ncfg-1]
    cspsys     = cspsys[0:nprnti-1,0:nprntf-1,*]

    ; specific hybrid data
    
    if nippy GT 0 then begin
    
       ipca    = ipca[0:nprntf-1]
       cstrpca = cstrpca[0:nprntf-1]
       xjpca   = xjpca[0:nprntf-1]
       wpca    = wpca[0:nprntf-1]
       iscp    = iscp[0:nippy-1]
       iscn    = iscn[0:nippy-1]
       iscl    = iscl[0:nippy-1]
       fsc     = fsc[0:nippy-1,*]
       wnsc    = wnsc[0:nippy-1, *]

    endif else begin
       
       ipca    = -1L
       cstrpca = ''
       xjpca   = 0.0D0
       wpca    = 0.0D0
       iscp    = -1L
       iscn    = -1L
       iscl    = -1L
       fsc     = 0.0D0
       wnsc    = 0.0D0
    
    endelse
    
    ; Assemble fulldata structure

    fulldata  = { iz           :   iz,              $
                  iz0          :   iz0,             $
                  iz1          :   iz1,             $
                  sym          :   seqsym,          $
                  ctype        :   ctype,           $
                  iform        :   iform,           $
                  nprnt        :   nprnt,           $
                  nprnti       :   nprnti,          $
                  nprntf       :   nprntf,          $
                  bwnp         :   bwnp,            $
                  ipa          :   ipa,             $
                  cstrpa       :   cstrpa,          $
                  ispa         :   ispa,            $
                  ilpa         :   ilpa,            $
                  xjpa         :   xjpa,            $
                  wpa          :   wpa,             $
                  ipca         :   ipca,            $
                  cstrpca      :   cstrpca,         $
                  xjpca        :   xjpca,           $
                  wpca         :   wpca,            $
                  ncfg         :   ncfg,            $
                  bwnr         :   bwnr,            $
                  ia           :   ia,              $
                  ip           :   ip,              $
                  cstrga       :   cstrga,          $
                  isa          :   isa,             $
                  ila          :   ila,             $
                  xja          :   xja,             $
                  wa           :   wa,              $
                  nippy        :   nippy,           $
                  iscp         :   iscp,            $
                  iscn         :   iscn,            $
                  iscl         :   iscl,            $
                  fsc          :   fsc,             $
                  wnsc         :   wnsc,            $
                  auga_res     :   auga_res,        $
                  lauga_res    :   ilauga_res,      $
                  nlrep        :   nlrep,           $
                  iaprs_nl     :   iaprs_nl,        $
                  caprs_nl     :   caprs_nl,        $
                  ipaug_nl     :   ipaug_nl,        $
                  irepa_nl     :   irepa_nl,        $
                  nlrepa_n     :   nlrepa_n,        $
                  nlrepa_l     :   nlrepa_l,        $
                  auga_nl      :   auga_nl,         $
                  lauga_nl     :   ilauga_nl,       $
                  nrep         :   nrep,            $
                  iaprs_n      :   iaprs_n,         $
                  caprs_n      :   caprs_n,         $
                  ipaug_n      :   ipaug_n,         $
                  irepa_n      :   irepa_n,         $
                  nrepa        :   nrepa,           $
                  auga_n       :   auga_n,          $
                  lauga_n      :   ilauga_n,        $
                  iprti        :   iprti,           $
                  diel_res     :   diel_res,        $
                  ldiel_res    :   ildiel_res,      $
                  iprtf        :   iprtf,           $
                  nsysf        :   nsysf,           $
                  isys         :   isys,            $
                  ispsys       :   ispsys,          $
                  cspsys       :   cspsys,          $
                  diel_nl      :   diel_nl,         $
                  ldiel_nl     :   ildiel_nl,       $
                  diel_n       :   diel_n,          $
                  ldiel_n      :   ildiel_n,        $
                  diel_sum     :   diel_sum,        $
                  ldiel_sum    :   ildiel_sum,      $
                  diel_tot     :   diel_tot,        $
                  ldiel_tot    :   ildiel_tot,      $
                  nte          :   nte,             $
                  tea          :   tea              }

endif else begin

    ipa          =    ipa[0:nprnt-1]
    ispa         =    ispa[0:nprnt-1]
    ilpa         =    ilpa[0:nprnt-1]
    xjpa         =    xjpa[0:nprnt-1]
    wpa          =    wpa[0:nprnt-1]
    ia           =    ia[0:nlvl-1]
    ip           =    ip[0:nlvl-1]
    isa          =    isa[0:nlvl-1]
    ila          =    ila[0:nlvl-1]
    xja          =    xja[0:nlvl-1]
    wa           =    wa[0:nlvl-1]
    auga_res     =    auga_res[0:nlvl-1,0:nprnt-1]
    ilauga_res   =    ilauga_res[0:nlvl-1,0:nprnt-1]

    if nlrep GT 0 then begin
       irepa_nl     =    irepa_nl[0:nlrep-1]
       nlrepa_n     =    nlrepa_n[0:nlrep-1]
       nlrepa_l     =    nlrepa_l[0:nlrep-1]
       diel_nl      =    diel_nl[0:nlrep-1,0:nprnti-1,0:nprntf-1,*,0:nte-1]
       ildiel_nl    =    ildiel_nl[0:nlrep-1,0:nprnti-1,0:nprntf-1,*]
    endif else begin
       irepa_nl     =    0L
       nlrepa_n     =    0L
       nlrepa_l     =    0L
       diel_nl      =    0.0D0
       ildiel_nl    =    0L
    endelse

    if iaprs_nl GE 1 then begin
       ipaug_nl     =  ipaug_nl[0:iaprs_nl-1,*]
       auga_nl      =  auga_nl[0:nlrep-1,0:iaprs_nl-1]
       ilauga_nl    =  ilauga_nl[0:nlrep-1,0:iaprs_nl-1]
       caprs_nl     =  caprs_nl[0:iaprs_n-1]
    endif else begin
       ipaug_nl     =  0L
       auga_nl      =  0.0D0
       ilauga_nl    =  0L
       caprs_nl     =  ' '
    endelse

    if iaprs_n GE 1 then begin
       ipaug_n      =  ipaug_n[0:iaprs_n-1,*]
       auga_n       =  auga_n[0:nrep-1,0:iaprs_n-1]
       ilauga_n     =  ilauga_n[0:nrep-1,0:iaprs_n-1]
       caprs_n      =  caprs_n[0:iaprs_n-1]
    endif else begin
       ipaug_n      =  0L
       auga_n       =  0.0D0
       ilauga_n     =  0L
       caprs_n      =  ' '
    endelse

    irepa_n    = irepa_n[0:nrep-1]
    nrepa      = nrepa[0:nrep-1]
    iprti      = iprti[0:nprnt-1]
    diel_res   = diel_res[0:nlvl-1,0:nprnt-1,0:nte-1]
    ildiel_res = ildiel_res[0:nlvl-1,0:nprnt-1]
    iprtf      = iprtf[0:nprnti-1,0:nprntf-1]
    nsysf      = nsysf[0:nprnti-1,0:nprntf-1]
    isys       = isys[0:nprnti-1,0:nprntf-1,*]
    ispsys     = ispsys[0:nprnti-1,0:nprntf-1,*]
    diel_n     = diel_n[0:nrep-1,0:nprnti-1,0:nprntf-1,*,0:nte-1]
    ildiel_n   = ildiel_n[0:nrep-1,0:nprnti-1,0:nprntf-1,*]
    diel_sum   = diel_sum[0:nprnti-1,0:nprntf-1,*,0:nte-1]
    ildiel_sum = ildiel_sum[0:nprnti-1,0:nprntf-1,*]
    diel_tot   = diel_tot[0:nprnti-1,0:nte-1]
    ildiel_tot = ildiel_tot[0:nprnti-1]
    tea        = tea[0:nte-1]
    cstrpa     = cstrpa[0:nprnt-1]
    cstrga     = cstrga[0:nlvl-1]
    cspsys     = cspsys[0:nprnti-1,0:nprntf-1,*]

    ; Assemble fulldata structure

    fulldata  = { iz           :   iz,              $
                  iz0          :   iz0,             $
                  iz1          :   iz1,             $
                  sym          :   seqsym,          $
                  ctype        :   ctype,           $
                  iform        :   iform,           $
                  nprnt        :   nprnt,           $
                  nprnti       :   nprnti,          $
                  nprntf       :   nprntf,          $
                  bwnp         :   bwnp,            $
                  ipa          :   ipa,             $
                  cstrpa       :   cstrpa,          $
                  ispa         :   ispa,            $
                  ilpa         :   ilpa,            $
                  xjpa         :   xjpa,            $
                  wpa          :   wpa,             $
                  nlvl         :   nlvl,            $
                  bwnr         :   bwnr,            $
                  ia           :   ia,              $
                  ip           :   ip,              $
                  cstrga       :   cstrga,          $
                  isa          :   isa,             $
                  ila          :   ila,             $
                  xja          :   xja,             $
                  wa           :   wa,              $
                  auga_res     :   auga_res,        $
                  lauga_res    :   ilauga_res,      $
                  nlrep        :   nlrep,           $
                  iaprs_nl     :   iaprs_nl,        $
                  caprs_nl     :   caprs_nl,        $
                  ipaug_nl     :   ipaug_nl,        $
                  irepa_nl     :   irepa_nl,        $
                  nlrepa_n     :   nlrepa_n,        $
                  nlrepa_l     :   nlrepa_l,        $
                  auga_nl      :   auga_nl,         $
                  lauga_nl     :   ilauga_nl,       $
                  nrep         :   nrep,            $
                  iaprs_n      :   iaprs_n,         $
                  caprs_n      :   caprs_n,         $
                  ipaug_n      :   ipaug_n,         $
                  irepa_n      :   irepa_n,         $
                  nrepa        :   nrepa,           $
                  auga_n       :   auga_n,          $
                  lauga_n      :   ilauga_n,        $
                  iprti        :   iprti,           $
                  diel_res     :   diel_res,        $
                  ldiel_res    :   ildiel_res,      $
                  iprtf        :   iprtf,           $
                  nsysf        :   nsysf,           $
                  isys         :   isys,            $
                  ispsys       :   ispsys,          $
                  cspsys       :   cspsys,          $
                  diel_nl      :   diel_nl,         $
                  ldiel_nl     :   ildiel_nl,       $
                  diel_n       :   diel_n,          $
                  ldiel_n      :   ildiel_n,        $
                  diel_sum     :   diel_sum,        $
                  ldiel_sum    :   ildiel_sum,      $
                  diel_tot     :   diel_tot,        $
                  ldiel_tot    :   ildiel_tot,      $
                  nte          :   nte,             $
                  tea          :   tea              }

endelse

end
