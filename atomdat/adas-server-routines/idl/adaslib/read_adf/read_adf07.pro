;----------------------------------------------------------------------
;+
; PROJECT    :  ADAS
;
; NAME       :  read_adf07
;
; PURPOSE    :  Reads adf07 (SZD) files from the IDL command line.
;               called from IDL using the syntax
;               read_adf07,file=...,block=...,te=... etc
;
; ARGUMENTS  :  All output arguments will be defined appropriately.
;
;               NAME      I/O    TYPE   DETAILS
; REQUIRED   :  file       I     str    full name of ADAS adf07 file
;               block      I     int    selected block
;               te         I     real() temperatures requested
; OPTIONAL      data       O      -     SZD data
;               info       O      -     information in a structure
;                                       info.i_ion  = initial ion <el>+z
;                                       info.f_ion  = final ion <el>+z
;                                       info.i_met  = initial metastable
;                                       info.f_met  = final metastable
;                                       info.ip     = ionisation potential
;               fulldata   O     str    all of the data from xxdata_07.
;                                        esym                : Element
;                                        iz0                 :  Nuclear charge
;                                        nblock              :  Number of ionisation rates
;                                        iz[nblock]          :  Initial charge
;                                        iz1 [nblock]        :  Final charge
;                                        cicode[nblock]      :  Initial state metastable index
;                                        cfcode[nblock]      :  Final   state metastable index
;                                        ciion[nblock]       :  Initial ion (as <esym>+(iz[nblock]> )
;                                        cfion[nblock]       :  Final   ion (as <esym>+<iz1[nblock]>)
;                                        bwno[nblock]        :  Effective ionization potential (cm-1)
;                                        nte[nblock]         :  Number of temperatures in each block
;                                        te[nte,nblock]      :  Temperatures
;                                        szd[max(nte),nblock]:  Ionisation rates
;                                        filename            :  Filename read
;
; KEYWORDS      kelvin            -     requested temperature in K (default eV)
;               help              -     displays help entry
;
; NOTES      :  This is part of a chain of programs - read_adf07.c and
;               readadf07.for are required.
;
; AUTHOR     :  Martin O'Mullane
;
; DATE       :  26-07-1999
;
; MODIFIED   :
;       1.2     Martin O'Mullane
;                 - No limit on number of Te/dens returned
;                 - Temperatures can be requested in K if /kelvin is specified.
;       1.3     Martin O'Mullane
;                 - Regularised comments.
;       1.4     Allan Whiteford
;                 - Added /help keyword
;       1.5     Martin O'Mullane
;                 - Add fulldata structure output.
;       1.6     Martin O'Mullane
;                 - Increase MAXVAL to 99.
;
; VERSION    :
;       1.1    26-07-1999
;       1.2    05-03-2001
;       1.3    15-03-2002
;       1.4    07-04-2005
;       1.5    23-05-2008
;       1.6    10-08-2018
;-
;----------------------------------------------------------------------

PRO read_adf07, file      = file,     $
                block     = block,    $
                te        = te,       $
                data      = data,     $
                info      = info,     $
                fulldata  = fulldata, $
                kelvin    = kelvin,   $
                help      = help

; If asked for help

if keyword_set(help) then begin
   doc_library, 'read_adf07'
   return
endif

; Check that we get all inputs and that they are correct. Otherwise print
; a message and return to command line

on_error, 2



; file name

if n_elements(file) eq 0 then message, 'A file name must be passed'
file_acc, file, exist, read, write, execute, filetype
if exist ne 1 then message, 'SZD file does not exist '+file
if read ne 1 then message, 'SZD file cannot be read from this userid '+file


; If all of the data required put it in fulldata and return

if arg_present(fulldata) then begin

   xxdata_07, file = file, fulldata = fulldata
   return

endif



; selection block

if n_elements(block) eq 0 then message, 'An SZD block must be selected'
parsize=size(block, /N_DIMENSIONS)
if parsize ne 0 then message,'Selection index cannot be an array'
partype=size(block, /TYPE)
if (partype ne 2) and (partype ne 3) then begin
   message,'Selection index must be numeric'
endif else ibsel = LONG(block)



; temperature

if n_elements(te) eq 0 then message, 'User requested temperatures are missing'

partype=size(te, /type)
if (partype lt 2) or (partype gt 5) then begin
   message,'Temperature must be numeric'
endif  else te = DOUBLE(te)

; Kelvin rather than eV

if keyword_set(kelvin) then te = te/11605.0


; set variables for call to C/fortran reading of data

itval = n_elements(te)
itval = LONG(itval)
data  = DBLARR(itval)

i_ion = 'ABCDE'
f_ion = 'abcde'
i_met = LONG(0)
f_met = LONG(0)
bwno  = DOUBLE(0.0)


t_in  = te

fortdir = getenv('ADASFORT')

; Get data in blocks of 99 to avoid altering xxspln for large sets.

data = 0.0D0

MAXVAL = 99L

n_call = numlines(itval, MAXVAL)

for j = 0L, n_call - 1 do begin

  ist = j*MAXVAL
  ifn = min([(j+1)*MAXVAL,itval])-1

  t_ca     = t_in[ist:ifn]
  data_ca  = dblarr(ifn-ist+1)
  itval_ca = ifn-ist+1

  dummy = CALL_EXTERNAL(fortdir+'/read_adf07.so','read_adf07', $
                        file, ibsel,                           $
                        itval_ca, t_ca, data_ca,               $
                        i_ion, f_ion, i_met, f_met, bwno)

  data = [data, data_ca]

endfor
data = data[1:*]


; make up the info structure - if one exists get rid of it.

if n_elements(info) ne 0 then xyz = temporary(info)

info = { i_ion : i_ion,       $
         f_ion : f_ion,       $
         i_met : i_met,       $
         f_met : f_met,       $
         ip    : bwno         }

; Reset temperature to K

if keyword_set(kelvin) then te = te*11605.0

END
