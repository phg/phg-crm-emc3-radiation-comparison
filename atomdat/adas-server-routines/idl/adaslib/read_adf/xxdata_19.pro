;----------------------------------------------------------------------
;+
; PROJECT    :  ADAS
;
; NAME       :  xxdata_19
;
; PURPOSE    :  Reads and ADF19 file into an IDL structure
;
; INPUTS     :  file - Fully path of ADF19 file to read
;
; OUTPUTS    :  fulldata: Structure containing contents of ADF19 file:
;               esym                 Element Symbol
;               iz0                  Nuclear charge
;               nblock               Number of ionisation rates
;               iz[nblock]           Initial charge
;               iz1 [nblock]         Final charge
;               ciion[nblock]        Radiating ion
;               citype[nblock]       Radiation type
;               ciinfo[nblock]       Information string
;               nte[nblock]          Number of temperatures in each block
;               te[nte,nblock]       Temperatures
;               pzd[max(nte),nblock] Ionisation rates
;               filename             Filename read
;
; NOTES      :  Calls fortran code via C wrapper, see:
;                       xxdata_19_if.c
;                       xxdata_19_calc.for
;
; AUTHOR     :  Martin O'Mullane
;
; DATE       :  03-07-2008
;
; MODIFIED:
;       1.1     Martin O'Mullane
;               - First version.
;
; VERSION:
;       1.1    03-07-2008
;
;-
;----------------------------------------------------------------------


PRO xxdata_19,file = file, fulldata = fulldata

if n_elements(file) EQ 0 then message,'Filename must be set'

; Convert filename to byte array

fileb = file
adas_string_pad, fileb, 120
dsnin=byte(fileb)

; Parameters for storage

nstore = 500L
ntdim  = 24L

; Allocate space

iz0    = 0L
esym   = bytarr(2)
nbsel  = 0L
isela  = lonarr(nstore)
iz     = lonarr(nstore)
iz1    = lonarr(nstore)
ciion  = bytarr(5, nstore)
citype = bytarr(5, nstore)
ciinfo = bytarr(20, nstore)
ita    = lonarr(nstore)
teta   = dblarr(ntdim, nstore)
pzd    = dblarr(ntdim, nstore)


; Get Path

adasfort = getenv('ADASFORT')
if adasfort eq '' then message, 'Cannot locate ADAS binaries'

; Call wrapper routine

res = call_external(adasfort+'/xxdata_19_if.so','xxdata_19',   $
                   dsnin,                                      $
                   nstore , ntdim  ,                           $
                   esym   , iz0    ,                           $
                   nbsel  , isela  ,                           $
                   iz     , iz1    ,                           $
                   ciion  , citype , ciinfo,                   $
                   ita    ,                                    $
                   teta   , pzd   )

; Convert output back to strings

ciion  = string(ciion)
citype = string(citype)
ciinfo = string(ciinfo)
esym   = string(esym)

; Return the valid stuff

isela  = isela[0:nbsel-1]
iz     = iz[0:nbsel-1]
iz1    = iz1[0:nbsel-1]
ciion  = ciion[0:nbsel-1]
citype = citype[0:nbsel-1]
ciinfo = ciinfo[0:nbsel-1]
ita    = ita[0:nbsel-1]

teta = teta[0:max(ita)-1, 0:nbsel-1]
pzd  = pzd[0:max(ita)-1, 0:nbsel-1]


; Put in structure

if arg_present(fulldata) then fulldata={ esym      :  esym,       $
                                         iz0       :  iz0,        $
                                         iz        :  iz,         $
                                         iz1       :  iz1,        $
                                         nblock    :  nbsel,      $
                                         ciion     :  ciion,      $
                                         citype    :  citype,     $
                                         ciinfo    :  ciinfo,     $
                                         nte       :  ita,        $
                                         te        :  teta,       $
                                         pzd       :  pzd,        $
                                         filename  :  file        }

END
