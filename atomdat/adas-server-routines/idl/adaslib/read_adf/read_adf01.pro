;----------------------------------------------------------------------
;+
; PROJECT    :  ADAS
;
; NAME       :  read_adf01
;
; PURPOSE    :  Reads adf01 (QCX) charge exchange cross section data files
;               from the IDL command line.
;               Called from IDL using the syntax
;                  read_adf01,file=..., energy=... etc
;
; ARGUMENTS  :  All output arguments will be defined appropriately.
;
;               NAME      I/O    TYPE    DETAILS
; REQUIRED   :  file       I     str     full name of ADAS adf01 file
;               energy     I     real()  energies requested in eV/amu.
; OPTIONAL      data       O      -      QCX data in cm^2.
;               n          I     integer return QCX for n shell
;               l          I     integer return QCX for nl shell
;               help       I             display help entry
;               fulldata   O      -      Structure containing all the
;                                        adf01 data. If requested the
;                                        other options are not used.
;
; NOTES      :  The default is the total cross section. For nl both
;               n and l must be set and for nlm all 3.
;
;               -  This is part of a chain of programs - read_adf01.c and
;                  readadf01.for are required.
;
; The fulldata structure is defined:
;
;    fulldata = { filename    :  file,        $
;                 symbr       :  symbr,       $
;                 symbd       :  symbd,       $
;                 izr         :  izr,         $
;                 izd         :  izd,         $
;                 indd        :  indd,        $
;                 nenrgy      :  nenrgy,      $
;                 nmin        :  nmin,        $
;                 nmax        :  nmax,        $
;                 lparms      :  lparms,      $
;                 lsetl       :  lsetl,       $
;                 lsetm       :  lsetm,       $
;                 enrgya      :  enrgya,      $
;                 alphaa      :  alphaa,      $
;                 lforma      :  lforma,      $
;                 xlcuta      :  xlcuta,      $
;                 pl2a        :  pl2a,        $
;                 pl3a        :  pl3a,        $
;                 sigta       :  sigta,       $
;                 signa       :  signa,       $
;                 sigla       :  sigla        }
;
;
; The n, nl and nlm cross sections are returned up to the maximum n
; in the dataset. However the lower n is always 1, not nmin. The
; appropriate cross section index can be found using the indexing
; functions i4idfl.pro and i4idfm.pro. Use the optional /idl_index
; keyword to keep the IDL first index is 0 convention.
;
;
; AUTHOR     :  Martin O'Mullane
;
; DATE       :  19-01-2001
;
; UPDATE     :
;       1.1     Martin O'Mullane
;                 - First version.
;       1.2     Allan Whiteford
;                 - Added /help keyword
;       1.3     Martin O'Mullane
;                 - Add the fulldata structure as an optional output.
;       1.4     Martin O'Mullane
;                 - Increase number of n-shells from 20 to 100.
;                 - Trim fulldata arrays to size of data in adf01
;                   dataset, not the maximum dimension.
;       1.5     Martin O'Mullane
;                 - Remove unused m-subshell data possibility.
;       1.6     Martin O'Mullane
;                 - Read data in blocks of 100 to avoid triggering
;                   xxspln error.
;       1.7     Martin O'Mullane
;                 - Increase number of energies permitted to 35.
;       1.8     Martin O'Mullane
;                 - Change arguments of xxdata_01.
;
; VERSION    :
;       1.1    19-01-2001
;       1.2    07-04-2005
;       1.3    10-10-2005
;       1.4    14-03-2007
;       1.5    22-05-2007
;       1.6    04-12-2008
;       1.7    22-09-2009
;       1.8    11-11-2018
;-
;----------------------------------------------------------------------

PRO read_adf01, file     = file,      $
                energy   = energy,    $
                n        = n,         $
                l        = l,         $
                data     = data,      $
                fulldata = fulldata,  $
                help     = help


; If asked for help

if keyword_set(help) then begin
   doc_library, 'read_adf01'
   return
endif

; Check that we get all inputs and that they are correct. Otherwise print
; a message and return to command line

on_error, 2



; Input file name

if n_elements(file) eq 0 then message, 'A file name must be passed'
file_acc, file, exist, read, write, execute, filetype
if exist ne 1 then message, 'adf01 file does not exist '+file
if read ne 1 then message, 'adf01 file cannot be read from this userid '+file



; Either return everything or proceed to extract the requested quantities

if arg_present(fulldata) then begin

   xxdata_01, file=file, fulldata=fulldata
   
   return

endif


; Input energies

if n_elements(energy) eq 0 then message, 'User requested energies are missing'

partype=size(energy, /type)
if (partype lt 2) or (partype gt 5) then begin
   message,'Temperature must be numeric'
endif  else energy = DOUBLE(energy)


; Which cross sections are required?

if n_elements(n) EQ 0 AND n_elements(l) GT 0 then message, 'Both n and l required'

if n_elements(n) EQ 0 then n = 0L else n = LONG(n)
if n_elements(l) EQ 0 then l = -1L else l = LONG(l)


; set variables for call to C/fortran reading of data

ieval = long(n_elements(energy))

fortdir = getenv('ADASFORT')


; Get data in blocks of 100 to avoid altering xxspln for large sets.

MAXVAL = 100L
data   = 0.0D0

n_call = numlines(ieval, MAXVAL)

for j = 0L, n_call - 1 do begin

  ist = j*MAXVAL
  ifn = min([(j+1)*MAXVAL,ieval])-1

  e_ca     = energy[ist:ifn]
  data_ca  = dblarr(ifn-ist+1)
  ieval_ca = ifn-ist+1

  dummy = CALL_EXTERNAL(fortdir+'/read_adf01.so','read_adf01',$
                        file,n,l,ieval_ca,e_ca,data_ca)

  data = [data, data_ca]

endfor
data = data[1:*]

END
