;----------------------------------------------------------------------
;+
; PROJECT    :  ADAS
;
; NAME       :  read_adf12
;
; PURPOSE    :  Reads adf12 (QEF) files from the IDL command line.
;               called from IDL using the syntax
;               read_adf12,file=...,block=...,ein=... etc
;
; ARGUMENTS  :  All output arguments will be defined appropriately.
;
;               NAME      I/O    TYPE   DETAILS
; REQUIRED   :  file       I     str    full name of ADAS adf12 file
;               block      I     int    selected block
;               ein        I     real() energies requested
;               tion       I     real() ion temperatures requested (eV)
;               dion       I     real() ion densities requested (cm-3)
;               zeff       I     real() Z-effective requested
;               bmag       I     real() B field requested (T)
;
; OPTIONAL      ttar       I     real() Maxwell temperature of target
;                                       particles (eV)
;               r_mass     I     real   Receiver mass (amu) if ttar present
;               d_mass     I     real   Donor mass (amu) if ttar present
;               data       O     real   QEF rate coefficient (cm3 s-1)
;               wlngth     O     real   wavelength (A)
;
; KEYWORDS   :  energy           EIN units are eV/amu
;               atomic           EIN units are atomic units
;               velocity         EIN units are cm s-1
;
;
; NOTES      :  Scans over energy, ion temperature, ion density, Zeff
;               or B are possible.
;
;               If a target temperature is given the donor and
;               receiver masses are also required. In this case an
;               effective Maxwell average rate coefficient of a
;               monoenergetic beam and thermal plasma is returned.
;
;               The cxsqef.pro routine is used to get the data. This
;               routine relies on compiled fortran.
;
; The fulldata structure is defined:
;
;    fulldata = { filename  :  file,      $
;                 nstore    :  nstore,    $
;                 nbsel     :  nbsel ,    $
;                 cwavel    :  cwavel,    $
;                 cdonor    :  cdonor,    $
;                 crecvr    :  crecvr,    $
;                 cfile     :  cfile,     $
;                 cpcode    :  cpcode,    $
;                 cindm     :  cindm,     $
;                 qefref    :  qefref,    $
;                 enref     :  enref,     $
;                 teref     :  teref,     $
;                 deref     :  deref,     $
;                 zeref     :  zeref,     $
;                 bmref     :  bmref,     $
;                 nenera    :  nenera,    $
;                 ntempa    :  ntempa,    $
;                 ndensa    :  ndensa,    $
;                 nzeffa    :  nzeffa,    $
;                 nbmaga    :  nbmaga,    $
;                 enera     :  enera,     $
;                 tempa     :  tempa,     $
;                 densa     :  densa,     $
;                 zeffa     :  zeffa,     $
;                 bmaga     :  bmaga,     $
;                 qenera    :  qenera,    $
;                 qtempa    :  qtempa,    $
;                 qdensa    :  qdensa,    $
;                 qzeffa    :  qzeffa,    $
;                 qbmaga    :  qbmaga     }
;
;
; AUTHOR     :  Martin O'Mullane
;
; DATE       :  21-07-2000
;
; MODIFIED   :
;
;         1.1   Martin O'Mullane
;                - First version.
;         1.2   Martin O'Mullane
;                - No limit on number of energies returned. It crashed
;                  if more than 24 were requesed.
;         1.3   Martin O'Mullane
;                - Remove limitation of varying just one parameter.
;                - When a target temperature is supplied deliver an
;                  effective coefficient.
;                - Add /help keyword.
;         1.4   Martin O'Mullane
;                - Add the fulldata structure as an optional output.
;         1.5   Martin O'Mullane
;                - Incorporate new xxdata_12 reading routine.
;                - Return wavelength as optional output.
;
; VERSION    :
;
;         1.1    21-07-2000
;         1.2    07-02-2003
;         1.3    28-02-2005
;         1.4    09-10-2005
;         1.5    05-06-2007
;-
;----------------------------------------------------------------------

PRO read_adf12, file      =  file,       $
                block     =  block,      $
                ein       =  ein,        $
                dion      =  dion,       $
                tion      =  tion,       $
                zeff      =  zeff,       $
                bmag      =  bmag,       $
                ttar      =  ttar,       $
                d_mass    =  d_mass,     $
                r_mass    =  r_mass,     $
                energy    =  energy,     $
                atomic    =  atomic,     $
                velocity  =  velocity,   $
                data      =  data,       $
                wlngth    =  wlngth,     $
                fulldata  = fulldata,    $
                help      =  help



; If asked for help

if keyword_set(help) then begin
   doc_library, 'read_adf12'
   return
endif

; Warn when an effective rate is returned

warning = 'Data is an effective coefficient for a monoenergetic beam' + $
          ' and a Maxwell distribution'



; Check that we get all inputs and that they are correct. Otherwise print
; a message and return to command line

on_error, 2



; file name

if n_elements(file) eq 0 then message, 'A file name must be passed'
file_acc, file, exist, read, write, execute, filetype
if exist ne 1 then message, 'adf12 file does not exist '+file
if read ne 1 then message, 'adf12 file cannot be read from this userid '+file



; Either return everything or proceed to extract the requested quantities

if arg_present(fulldata) then begin

   nstore = 500L

   xxdata_12,  file    ,                                         $
               nstore  , nbsel  ,                                $
               csymb   , czion  , cwavel , cdonor , crecvr  ,    $
               ctrans  , cfile  , ctype  , cindm  ,              $
               qefref  ,                                         $
               enref   , teref  , deref  , zeref   , bmref  ,    $
               nenera  , ntempa , ndensa , nzeffa  , nbmaga ,    $
               enera   , tempa  , densa  , zeffa   , bmaga  ,    $
               qenera  , qtempa , qdensa , qzeffa  , qbmaga

   fulldata = { filename  :  file,                 $
                nstore    :  nstore,               $
                nbsel     :  nbsel ,               $
                csymb     :  csymb[0:nbsel-1],     $  
                czion     :  czion[0:nbsel-1],     $  
                cwavel    :  cwavel[0:nbsel-1],    $ 
                cdonor    :  cdonor[0:nbsel-1],    $  
                crecvr    :  crecvr[0:nbsel-1],    $  
                ctrans    :  ctrans[0:nbsel-1],    $  
                cfile     :  cfile[0:nbsel-1] ,    $  
                ctype     :  ctype[0:nbsel-1] ,    $  
                cindm     :  cindm[0:nbsel-1],     $   
                qefref    :  qefref[0:nbsel-1],    $
                enref     :  enref[0:nbsel-1],     $
                teref     :  teref[0:nbsel-1],     $
                deref     :  deref[0:nbsel-1],     $
                zeref     :  zeref[0:nbsel-1],     $
                bmref     :  bmref[0:nbsel-1],     $
                nenera    :  nenera[0:nbsel-1],    $
                ntempa    :  ntempa[0:nbsel-1],    $
                ndensa    :  ndensa[0:nbsel-1],    $
                nzeffa    :  nzeffa[0:nbsel-1],    $
                nbmaga    :  nbmaga[0:nbsel-1],    $
                enera     :  enera[*,0:nbsel-1],   $
                tempa     :  tempa[*,0:nbsel-1],   $
                densa     :  densa[*,0:nbsel-1],   $
                zeffa     :  zeffa[*,0:nbsel-1],   $
                bmaga     :  bmaga[*,0:nbsel-1],   $
                qenera    :  qenera[*,0:nbsel-1],  $
                qtempa    :  qtempa[*,0:nbsel-1],  $
                qdensa    :  qdensa[*,0:nbsel-1],  $
                qzeffa    :  qzeffa[*,0:nbsel-1],  $
                qbmaga    :  qbmaga[*,0:nbsel-1]   }

   return

endif



; selection block

if n_elements(block) eq 0 then message, 'A QEF block must be selected'
parsize=size(block, /N_DIMENSIONS)
if parsize ne 0 then message,'Selection index cannot be an array'
partype=size(block, /TYPE)
if (partype ne 2) and (partype ne 3) then begin
   message,'Selection index must be numeric'
endif else ibsel = LONG(block)

; Units of input energy are keywords - default to eV/amu

ietyp = 1L

e1 = keyword_set(energy)
e2 = keyword_set(atomic)
e3 = keyword_set(velocity)
if (e1+e2+e3) gt 1 then message, 'Only one energy unit allowed'

if e1 eq 1 then ietyp = 1L
if e2 eq 1 then ietyp = 2L
if e3 eq 1 then ietyp = 3L



; Energy, ion temperature, ion density, Zeff and B

if n_elements(ein)  eq 0 then message, 'No user requested energies'
if n_elements(dion) eq 0 then message, 'No user requested ion density'
if n_elements(tion) eq 0 then message, 'No user requested ion temperature'
if n_elements(zeff) eq 0 then message, 'No user requested zeff'
if n_elements(bmag) eq 0 then message, 'No user requested magnetic field'

partype=size(ein, /type)
if (partype lt 2) or (partype gt 5) then begin
   message,'Energies must be numeric'
endif  else ein = DOUBLE(ein)
partype=size(dion, /type)
if (partype lt 2) or (partype gt 5) then begin
   message,'Energies must be numeric'
endif  else dion = DOUBLE(dion)
partype=size(tion, /type)
if (partype lt 2) or (partype gt 5) then begin
   message,'Energies must be numeric'
endif  else tion = DOUBLE(tion)
partype=size(zeff, /type)
if (partype lt 2) or (partype gt 5) then begin
   message,'Energies must be numeric'
endif  else zeff = DOUBLE(zeff)
partype=size(bmag, /type)
if (partype lt 2) or (partype gt 5) then begin
   message,'Energies must be numeric'
endif  else bmag = DOUBLE(bmag)


if n_elements(ttar) NE 0 then begin

   partype=size(ttar, /type)
   if (partype lt 2) or (partype gt 5) then begin
      message,'Maxwell target temperatures must be numeric'
   endif  else ttar = DOUBLE(ttar)
   partype=size(r_mass, /type)
   if (partype lt 2) or (partype gt 5) then begin
      message,'Receiver mass must be numeric'
   endif  else r_mass = DOUBLE(r_mass)
   partype=size(d_mass, /type)
   if (partype lt 2) or (partype gt 5) then begin
      message,'Donor mass must be numeric'
   endif  else d_mass = DOUBLE(d_mass)

endif

; Convert to eV/amu for energy

case ietyp of
   1 :
   2 : ein = (ein/6.32678D-03)^2.0
   3 : ein = (ein/1.38410D+06)^2.0
endcase


; For iteration all sizes must be the same or just one is allowed to vary

sz_ein  = size(ein,  /N_DIMENSIONS)
sz_dion = size(dion, /N_DIMENSIONS)
sz_tion = size(tion, /N_DIMENSIONS)
sz_zeff = size(zeff, /N_DIMENSIONS)
sz_bmag = size(bmag, /N_DIMENSIONS)
sz_ttar = size(ttar, /N_DIMENSIONS)
sz_tot  = sz_ein  + sz_dion + sz_tion + sz_zeff + sz_bmag + sz_ttar

n_ein   = n_elements(ein)
n_dion  = n_elements(dion)
n_tion  = n_elements(tion)
n_zeff  = n_elements(zeff)
n_bmag  = n_elements(bmag)
n_ttar  = n_elements(ttar)
n_tot   = n_ein + n_dion + n_tion + n_zeff + n_bmag + n_ttar



if (sz_tot EQ 1) then begin

   ; Only one varies

   case 1 of

       sz_ein EQ 1  : begin
                         ieval   = n_ein
                         eng     = ein
                         tion_in = replicate(tion, ieval)
                         dion_in = replicate(dion, ieval)
                         zeff_in = replicate(zeff, ieval)
                         bmag_in = replicate(bmag, ieval)
                         if n_elements(ttar) EQ 0 then begin
                            ttar_in = replicate(0.0D0, ieval)
                            em1     = 1.0
                            em2     = 1.0
                         endif else begin
                            ttar_in = replicate(ttar, ieval)
                            em1     = d_mass
                            em2     = r_mass
                            message, warning, /continue
                         endelse
                      end

       sz_tion EQ 1  : begin
                         ieval   = n_tion
                         eng     = replicate(ein, ieval)
                         tion_in = tion
                         dion_in = replicate(dion, ieval)
                         zeff_in = replicate(zeff, ieval)
                         bmag_in = replicate(bmag, ieval)
                         if n_elements(ttar) EQ 0 then begin
                            ttar_in = replicate(0.0D0, ieval)
                            em1     = 1.0
                            em2     = 1.0
                         endif else begin
                            ttar_in = replicate(ttar, ieval)
                            em1     = d_mass
                            em2     = r_mass
                            message, warning, /continue
                         endelse
                      end

       sz_dion EQ 1  : begin
                         ieval   = n_dion
                         eng     = replicate(ein, ieval)
                         tion_in = replicate(tion, ieval)
                         dion_in = dion
                         zeff_in = replicate(zeff, ieval)
                         bmag_in = replicate(bmag, ieval)
                         if n_elements(ttar) EQ 0 then begin
                            ttar_in = replicate(0.0D0, ieval)
                            em1     = 1.0
                            em2     = 1.0
                         endif else begin
                            ttar_in = replicate(ttar, ieval)
                            em1     = d_mass
                            em2     = r_mass
                            message, warning, /continue
                         endelse
                      end

       sz_zeff EQ 1  : begin
                         ieval   = n_zeff
                         eng     = replicate(ein, ieval)
                         tion_in = replicate(tion, ieval)
                         dion_in = replicate(dion, ieval)
                         zeff_in = zeff
                         bmag_in = replicate(bmag, ieval)
                         if n_elements(ttar) EQ 0 then begin
                            ttar_in = replicate(0.0D0, ieval)
                            em1     = 1.0
                            em2     = 1.0
                         endif else begin
                            ttar_in = replicate(ttar, ieval)
                            em1     = d_mass
                            em2     = r_mass
                            message, warning, /continue
                         endelse
                      end

       sz_bmag EQ 1  : begin
                         ieval   = n_bmag
                         eng     = replicate(ein, ieval)
                         tion_in = replicate(tion, ieval)
                         dion_in = replicate(dion, ieval)
                         zeff_in = replicate(zeff, ieval)
                         bmag_in = bmag
                         if n_elements(ttar) EQ 0 then begin
                            ttar_in = replicate(0.0D0, ieval)
                            em1     = 1.0
                            em2     = 1.0
                         endif else begin
                            ttar_in = replicate(ttar, ieval)
                            em1     = d_mass
                            em2     = r_mass
                            message, warning, /continue
                         endelse
                      end

       sz_ttar EQ 1 : begin
                         ieval   = n_ttar
                         eng     = replicate(ein, ieval)
                         tion_in = replicate(tion, ieval)
                         dion_in = replicate(dion, ieval)
                         zeff_in = replicate(zeff, ieval)
                         bmag_in = replicate(bmag, ieval)
                         ttar_in = ttar
                         em1     = d_mass
                         em2     = r_mass
                         message, warning, /continue
                      end

   endcase

   nener  = 24L
   iord   = 1L

   cxsqef, file    , ibsel   ,                    $
           ieval   , eng     , ttar_in ,          $
           em1     , em2     , iord    ,          $
           tion_in , dion_in , zeff_in , bmag_in, $
           nener   , ener    , qener   ,          $
           csymb   , czion   , cwavel  ,          $
           cdonor  , crecvr  , ctrans  ,          $
           cfile   , ctype   , cindm   ,          $
           qeff    , ircode

   if ircode NE 0 then message, 'Problem reading adf12 file : ' + file

   data = qeff

endif else begin

   ; They all vary with the possible exception of target temperature

   if NOT (n_tot EQ n_ein*6L OR n_tot EQ n_ein*5L) then  $
      message, 'All input vectors must have the same length'

   ieval = n_ein
   if n_elements(ttar) EQ 0 then begin
      ttar_in = replicate(0.0D0, ieval)
      em1     = 1.0
      em2     = 1.0
   endif else begin
      ttar_in = ttar
      em1     = d_mass
      em2     = r_mass
      message, warning, /continue
   endelse


   nener  = 24L
   iord   = 1L
   cxsqef, file    , ibsel  ,                   $
           n_ein   , ein    , ttar_in ,         $
           em1     , em2    , iord    ,         $
           tion    , dion   , zeff    , bmag,   $
           nener   , ener   , qener   ,         $
           csymb   , czion  , cwavel  ,         $
           cdonor  , crecvr , ctrans  ,         $
           cfile   , ctype  , cindm   ,         $
           qeff    , ircode

   if ircode NE 0 then message, 'Problem reading adf12 file : ' + file

   data = qeff

endelse


; Return the wavelength if required

if arg_present(wlngth) then begin

    error = num_chk(cwavel)
    if error EQ 0 then wlngth = float(cwavel) else wlngth = 0.0
   
endif

END
