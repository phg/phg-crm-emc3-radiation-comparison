;----------------------------------------------------------------------
;+
; PROJECT    :  ADAS
;
; NAME       :  xxdata_01
;
; PURPOSE    :  
;
; ARGUMENTS  :  All output arguments will be defined appropriately.
;               Inputs will be converted to correct type, if possible,
;               internally without changing calling type.
;
;                xxdata_01, file=file, fulldata=fulldata
;
;               NAME      I/O    TYPE    DETAILS
; REQUIRED   :  file       I     str     adf01 file name
; OPTIONAL      fulldata   O     struc   data in the adf01 file
;
; The fulldata structure is defined:
;
;               filename    str     adf01 file name
;               symbr       str     receiver ion element symbol.
;               symbd       str     donor ion elment symbol.
;               izr         long    ion charge of receiver.
;               izd         long    ion charge of donor.
;               indd        long    donor state index.
;               nenrgy      long    number of energies read.
;               nmin        long    lowest n-shell for which data read.
;               nmax        long    highest n-shell for which data read.
;               lparms      long    flags if l-splitting parameters present.
;                                   1  => l-splitting parameters present.
;                                   0  => l-splitting parameters absent.
;               lsetl       long    flags if l-resolved data present.
;                                   1  => l-resolved data present.
;                                   0  => l-resolved data absent.
;               enrgya()    double  collision energies (eV/amu).
;                                   dimension: energy index
;               alphaa()    double  extrapolation parameter alpha.
;                                   dimension: energy index
;               lforma()    long    parameters for calculating l-res x-sec.
;                                   dimension: energy index
;               xlcuta()    double  parameters for calculating l-res x-sec.
;                                   dimension: energy index
;               pl2a()      double  parameters for calculating l-res x-sec.
;                                   dimension: energy index
;               pl3a()      double  parameters for calculating l-res x-sec.
;                                   dimension: energy index
;               sigta()     double  total charge exchange cross-section (cm2).
;                                   dimension: energy index
;               signa(,)    double  n-resolved charge exchange cross-sections (cm2).
;                                   1st dimension: energy index
;                                   2nd dimension: n-shell
;               sigla(,)    double  l-resolved charge exchange cross-sections (cm2).
;                                   1st dimension: energy index
;                                   2nd dimension: indexed by i4idfl(n,l)
;
;
; NOTES      :  Calls fortran code.
;
;
; AUTHOR     :  Martin O'Mullane
;
; DATE       :  06/10/05
;
;
; MODIFIED:
;       1.1     Martin O'Mullane
;              	- First version.
;       1.2     Martin O'Mullane
;                 - Remove unused m-subshell data possibility.
;       1.3     Martin O'Mullane
;                 - Return data as a fulldata structure to bring
;                   in line with other xxdata routines.
;
;
; VERSION:
;       1.1    06-10-2005
;       1.2    22-05-2007
;       1.3    11-11-2018
;
;-
;----------------------------------------------------------------------

PRO xxdata_01, file=file, fulldata=fulldata

; Parameters for sizing

mxneng = 35L
mxnshl = 100L
iunit  = 67L

; Check that the adf01 file exists

file_acc, file, exist, read, write, execute, filetype
if exist ne 1 then message, 'adf01 file does not exist ' + file
if read ne 1 then message, 'adf01 file cannot be read from this userid ' + file

file_in = string(replicate(32B, 132))
strput, file_in, file, 0



; Define types of the output parameters

symbd     = 'DD'  
symbr     = 'RR'
izr       = -1L  
izd       = -1L      
indd      = -1L      
nenrgy    = -1L   
nmin      = -1L    
nmax      = -1L    
il_lparms = 0L   
il_lsetl  = 0L  
enrgya    = dblarr(mxneng)
alphaa    = dblarr(mxneng)   
lforma    = lonarr(mxneng) 
xlcuta    = dblarr(mxneng) 
pl2a      = dblarr(mxneng)
pl3a      = dblarr(mxneng)
sigta     = dblarr(mxneng)   
signa     = dblarr(mxneng,mxnshl)   
sigla     = dblarr(mxneng,(mxnshl*(mxnshl+1))/2)   


; Location of sources

fortdir = getenv('ADASFORT')

dummy = CALL_EXTERNAL(fortdir+'/xxdata_01_if.so','xxdata_01_if',      $
                      iunit, mxneng, mxnshl, izr, izd,                $
                      indd, nenrgy, nmin, nmax, il_lparms,            $
                      il_lsetl, enrgya, alphaa, lforma,               $
                      xlcuta, pl2a, pl3a, sigta, signa,               $
                      sigla, symbd, symbr, file_in)

last_n = nmax - 1
last_l = i4idfl(nmax, nmax-1, /idl_index)

fulldata = { filename    :  file,                             $
             symbr       :  symbr,                            $
             symbd       :  symbd,                            $
             izr         :  izr,                              $
             izd         :  izd,                              $
             indd        :  indd,                             $
             nenrgy      :  nenrgy,                           $
             nmin        :  nmin,                             $
             nmax        :  nmax,                             $
             lparms      :  il_lparms,                        $
             lsetl       :  il_lsetl,                         $
             enrgya      :  enrgya[0:nenrgy-1],               $
             alphaa      :  alphaa[0:nenrgy-1],               $
             lforma      :  lforma[0:nenrgy-1],               $
             xlcuta      :  xlcuta[0:nenrgy-1],               $
             pl2a        :  pl2a[0:nenrgy-1],                 $
             pl3a        :  pl3a[0:nenrgy-1],                 $
             sigta       :  sigta[0:nenrgy-1],                $
             signa       :  signa[0:nenrgy-1, 0:last_n],      $
             sigla       :  sigla[0:nenrgy-1, 0:last_l]       }

END
