;----------------------------------------------------------------------
;+
; PROJECT    :  ADAS
;
; NAME       :  read_adf20
;
; PURPOSE    :  Reads adf20 (GFT) files from the IDL command line.
;               called from IDL using the syntax
;               read_adf20,file=...,block=...,te=... etc
;
; ARGUMENTS  :  All output arguments will be defined appropriately.
;
;               NAME      I/O    TYPE   DETAILS
; REQUIRED   :  file       I     str    full name of ADAS adf20 file.
;               block      I     int    selected block.
;               te         I     real() temperatures requested.
; OPTIONAL      data       O      -     GFT data.
;               dens       O      -     interpolated model density at te.
;               pressure   O      -     interpolated model pressure at te.
;               h_ratio    O      -     interpolated nH/ne ratio at te.
;
; KEYWORDS      kelvin            -     requested temperature in K (default eV)
;
; NOTES      :  - This is part of a chain of programs - read_adf20.c and
;                 readadf20.for are required.
;               - Although GFTs are mainly used in astrophysics where
;                 kelvin is the preferred temperature unit the default
;                 assumption here is that Te is in eV to keep consistency
;                 between the various read_adfXX routines. There is the
;                 /kelvin keyword to switch.
;
; AUTHOR     :  Martin O'Mullane
;
; DATE       :  06-06-2003
;
; MODIFIED   :
;       1.1     Martin O'Mullane
;                 - First version
;       1.2     Martin O'Mullane
;                 - Added help keyword.
;
; VERSION    :
;       1.1    06-06-2003
;       1.2    06-04-2005
;-
;----------------------------------------------------------------------

PRO read_adf20, file     = file,      $
                block    = block,     $
                te       = te,        $
                data     = data,      $
                dens     = dens,      $
                pressure = pressure,  $
                h_ratio  = h_ratio,   $
                kelvin   = kelvin,    $
                help     = help

; If asked for help

if keyword_set(help) then begin
   doc_library, 'read_adf20'
   return
endif


; Check that we get all inputs and that they are correct. Otherwise print
; a message and return to command line

on_error, 2



; file name

if n_elements(file) eq 0 then message, 'A file name must be passed'
file_acc, file, exist, read, write, execute, filetype
if exist ne 1 then message, 'GFT file does not exist '+file
if read ne 1 then message, 'GFT file cannot be read from this userid '+file


; selection block

if n_elements(block) eq 0 then message, 'A GFT transition must be selected'
parsize=size(block, /N_DIMENSIONS)
if parsize ne 0 then message,'Selection index cannot be an array'
partype=size(block, /TYPE)
if (partype ne 2) and (partype ne 3) then begin
   message,'Selection index must be numeric'
endif else ibsel = LONG(block)



; temperature

if n_elements(te) eq 0 then message, 'User requested temperatures are missing'

partype=size(te, /type)
if (partype lt 2) or (partype gt 5) then begin
   message,'Temperature must be numeric'
endif  else te = DOUBLE(te)

; Kelvin rather than eV

if keyword_set(kelvin) then te = te/11605.0



; set variables for call to C/fortran reading of data

itval = n_elements(te)
itval = LONG(itval)

; By here te is in eV but, as GFTs are astrophysical things, we must
; convert to K for the interpolation.

t_in  = te * 11605.0

fortdir = getenv('ADASFORT')


; Get data in blocks of 30 to avoid altering xxspln for large sets.

data     = 0.0D0
dens     = 0.0D0
pressure = 0.0D0
h_ratio  = 0.0D0


MAXVAL = 30

n_call = numlines(itval, MAXVAL)

for j = 0, n_call - 1 do begin

  ist = j*MAXVAL
  ifn = min([(j+1)*MAXVAL,itval])-1

  t_ca     = t_in[ist:ifn]
  itval_ca = ifn-ist+1
  data_ca  = dblarr(ifn-ist+1)
  dn_ca    = dblarr(ifn-ist+1)
  p_ca     = dblarr(ifn-ist+1)
  rh_ca    = dblarr(ifn-ist+1)

  dummy = CALL_EXTERNAL(fortdir+'/read_adf20.so','read_adf20', $
                        file, ibsel,                           $
                        itval_ca, t_ca, data_ca,               $
                        dn_ca, p_ca, rh_ca)

  data     = [data, data_ca]
  dens     = [dens    , dn_ca]
  pressure = [pressure, p_ca]
  h_ratio  = [h_ratio , rh_ca]

endfor
data     = data[1:*]
dens     = dens[1:*]
pressure = pressure[1:*]
h_ratio  = h_ratio[1:*]



; Reset temperature to K

if keyword_set(kelvin) then te = te*11605.0

END
