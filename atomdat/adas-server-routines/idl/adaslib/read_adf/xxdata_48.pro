;----------------------------------------------------------------------
;+
; PROJECT    :  ADAS
;
; NAME       :  xxdata_48
;
; PURPOSE    :  Reads adf48 radiative recombination files from IDL
;
; ARGUMENTS  :  All output arguments will be defined appropriately.
;               Inputs will be converted to correct type, if possible,
;               internally without changing calling type.
;
;              NAME       I/O    TYPE    DETAILS
; REQUIRED   : file        I     str     adf48 file name
; OPTIONAL   : fulldata    O     -       Output structure (see below)
;              help        I     switch  Help required
;
; The fulldata structure is defined as:
;
; fulldata  = { iz             :  recombined ion charge
;               iz0            :  nuclear charge
;               iz1            :  recombining ion charge
;               sym            :  recombined ion seq
;               ctype          :  resolution type (ls/ic/ca)
;               iform          :  data set organisational form
;                                    1: LS 2005 Badnell data form
;                                    2: IC 2005 Badnell data form
;                                    3: unassigned
;                                    4: hybrid LS 2015 Badnell data form
;                                    5: hybrid IC 2015 Badnell data form
;                                    6: hybrid CA 2015 Badnell data form
;               nprnt          :  total number of parents
;               nprnti         :  number of parents which are initial parents
;               nprntf         :  number of parents which are final parents
;               bwnp           :  binding wave no. of ground parent (cm-1)
;               ipa            :  parent energy level index number
;                                   dim : parent lvl/trm/cfg index
;               cstrpa()       :  nomencl./config. for parent level 'ipa()'
;                                   dim: parent lvl/trm/cfg index
;               ispa()         :  multiplicity for parent level 'ipa()'
;                                 note: (ispa-1)/2 = quantum number (sp)
;                                   dim: parent lvl/trm/cfg index
;               ilpa()         :  quantum number (lp) for parent level 'ipa()'
;                                   dim: parent lvl/trm/cfg index
;               xjpa()         :  quantum number (jp) for parent level 'ipa()'
;                                 note: (2*xjpa)+1 = statistical weight
;                                   dim: parent lvl/trm/cfg index
;               wpa()          :  energy relative to parent level 1 (cm-1)
;                                 for parent level 'ipa()'
;                                   dim: parent lvl/trm/cfg index
;               bwnr           :  ionisation potential (cm-1) of lowest level
;                                 of recombined ion
;               ia()           :  recombined ion energy level index number
;                                  dim: lvl/trm/cfg index
;               ip()           :  associated parent of recombined level
;                                  dim: lvl/trm/cfg index
;               cstrga()       :  nomencl./config. for recombined ion level 'ia()'
;                                  dim: lvl/trm/cfg index
;               isa()          :  multiplicity for recombined level 'ia()'
;                                 note: (isa-1)/2 = quantum number (s)
;                                  dim: lvl/trm/cfg index
;               ila()          :  quantum number (l) for recombined level 'ia()'
;                                  dim: lvl/trm/cfg index
;               xja()          :  quantum number (j) for recombined level 'ia()'
;                                 note: (2*xja)+1 = statistical weight
;                                  dim: lvl/trm/cfg index
;               wa()           :  energy relative to recombined level 1 (cm-1)
;                                 for recombined level 'ia()'
;                                  dim: lvl/trm/cfg index
;               nlrep          :  number of representative nl-shells
;               iaprs_nl       :  number of nl-shell auger rate initial
;                                 and final parent pairs
;               irepa_nl()     :  representative nl-shell index number
;                                   dim: representative nl-shell index
;               irepa_nl()     :  representative nl-shell index number
;                                   dim: representative nl-shell index
;               nlrepa_n()     :  n value for representative nl-shell
;                                   dim: representative nl-shell index
;               nlrepa_l()     :  l value for representative nl-shell
;                                   dim: representative nl-shell index
;               nrep           :  number of representative n-shells
;                                  dim: 2 (for initial and final parent)
;               irepa_n()      :  representative n-shell index number
;                                   dim: representative n-shell index
;               nrepa()        :  n value for representative n-shell
;                                   dim: representative n-shell index
;               iprti()        :  initial parent index from block header
;                                   dim: initial parent index number
;               rrec_res(,,)   :  lvl/trm/cfg resol. rad. rec. coeffts.(cm3 s-1)
;                                  dim: level index
;                                  dim: initial parent index
;                                  dim: temperature index
;               lrrec_res(,)   :  1 (true)  => rad. rec. present for lvl/trm/cfg index
;                                 0 (false) => rad. rec. not present for
;                                              lvl/trm/cfg index
;                                  dim: level index
;                                  dim: initial parent index
;               iprtf(,)       :  final parent index from block header
;                                      dim: initial parent index number
;                                      dim: final parent index number
;               nsysf(,)       :  no. of spin systems built on final parent
;                                 defaults to 1 for /ic/ca/
;                                   dim: initial parent index number
;                                   dim: final parent index number
;               isys(,,)       :  n-shell spin system index for final parent
;                                 defaults to 1 for /ic/ca/
;                                   dim: initial parent index number
;                                   dim: final parent index number
;                                   dim: spin system index number
;               ispsys(,,)     :  n-shell spin system for final parent
;                                 applicable to /ls/ only
;                                   dim: initial parent index number
;                                   dim: final parent index number
;                                   dim: spin system index number
;               cspsys(,,)     :  characterising string for final parent
;                                 system - universal for /ls/ic/ca/
;                                   dim: initial parent index number
;                                   dim: final parent index number
;                                   dim: spin system index number
;               rrec_nl(,,,,)  :  nl-shell rad. rec. coeffts.(cm3 s-1)
;                                   dim: repr. nl-shell index
;                                   dim: initial parent index
;                                   dim: final parent index
;                                   dim: spin system index
;                                   dim: temperature index
;               lrrec_nl(,,,)  :  1 (true)  => rad. rec. present for repr. nl-shell
;                                 0 (false) => rad. rec. not present for  nl-shell
;                                  dim: repr. nl-shell index
;                                  dim: initial parent index
;                                  dim: final parent index
;                                  dim: spin system index
;               rrec_n(,,,,)   :  n-shell rad. rec. coeffts.(cm3 s-1)
;                                  dim: repr. n-shell index
;                                  dim: initial parent index
;                                  dim: final parent index
;                                  dim: spin system index
;                                  dim: temperature index
;               lrrec_n(,,,,)  :  1 (true)  => rad. rec. present for repr. n-shell
;                                 0 (false) => rad. rec. not present for  n-shell
;                                  dim: repr. n-shell index
;                                  dim: initial parent index
;                                  dim: final parent index
;                                  dim: spin system index
;               rrec_sum(,,,)  :  sum of n-shell rad. recec. coeffts.(cm3 s-1)
;                                   dim: initial parent index
;                                   dim: final parent index
;                                   dim: spin system index
;                                   dim: temperature index
;               lrrec_sum(,,)  :  1 (true)  => rad. rec. sum present
;                                 0 (false) => rad. rec. sum not present
;                                  dim: initial parent index
;                                  dim: final parent index
;                                  dim: spin system index
;               rrec_tot(,)    :  total rad. rec. coefficients (cm3 s-1) tabulated
;                                 in dataset
;                                  dim: initial parent index
;                                  dim: electron temperature index
;               lrrec_tot()    :  1 (true)  => rad. rec. total present in dataset
;                                 0 (false) => rad. rec. total not present
;                                  dim: initial parent index
;               nte            :  no. of electron temperatures
;               tea()          :  electron temperatures (k)
;                                  dim: electron temperature index
;
;
;
;
; NOTES      :  Calls fortran code, xxdata_48.for.
;
;
; MODIFIED:
;       1.1     Martin O'Mullane
;               - First version.
;       1.2     Martin O'Mullane
;               - No longer relies on xxdata_09.so. Calls xxdata_48.so.
;
; VERSION:
;       1.1    10-06-2013
;       1.2    15-04-2016
;
;-
;----------------------------------------------------------------------

pro xxdata_48, file=file, fulldata=fulldata, help=help

; Return help if requested

if keyword_set(help) then begin
    doc_library, 'xxdata_48'
    return
endif

; Check the file is valid

if n_elements(file) eq 0 then message,'A filename must be given'
file_acc, file, exist, read, write, execute, filetype
if exist ne 1 then message, 'adf48 file does not exist '+file
if read ne 1 then message, 'adf48 file cannot be read from this userid '+file

dnsfull=file

; Dimensions

    ndprt  = 100L
    ndrep  = 250L
    ndlrep = 500L
    ndlev  = 16000L
    ndt    = 20L

; Initialise variables

    iz           =    0L
    iz0          =    0L
    iz1          =    0L
    iform        =    0L
    nprnt        =    0L
    nprnti       =    0L
    nprntf       =    0L
    bwnp         =    0.0D0
    ipa          =    lonarr(ndprt)
    ispa         =    lonarr(ndprt)
    ilpa         =    lonarr(ndprt)
    xjpa         =    dblarr(ndprt)
    wpa          =    dblarr(ndprt)
    nlvl         =    0L
    ncfg         =    0L
    nippy        =    0L
    bwnr         =    0.0D0
    ia           =    lonarr(ndlev)
    ip           =    lonarr(ndlev)
    isa          =    lonarr(ndlev)
    ila          =    lonarr(ndlev)
    xja          =    dblarr(ndlev)
    wa           =    dblarr(ndlev)
    ilauga_res   =    lonarr(ndlev)       ; different to DR
    nlrep        =    0L
    iaprs_nl     =    0L
    irepa_nl     =    lonarr(ndlrep)
    nlrepa_n     =    lonarr(ndlrep)
    nlrepa_l     =    lonarr(ndlrep)
    nrep         =    0L
    iaprs_n      =    0L
    irepa_n      =    lonarr(ndrep)
    nrepa        =    lonarr(ndrep)
    iprti        =    lonarr(ndprt)
    rrec_res     =    dblarr(ndlev,ndprt,ndt)
    ilrrec_res   =    lonarr(ndlev,ndprt)
    iprtf        =    lonarr(ndprt,ndprt)
    nsysf        =    lonarr(ndprt,ndprt)
    isys         =    lonarr(ndprt,ndprt,2)
    ispsys       =    lonarr(ndprt,ndprt,2)
    rrec_nl      =    dblarr(ndlrep,ndprt,ndprt,2,ndt)
    ilrrec_nl    =    lonarr(ndlrep,ndprt,ndprt,2)
    rrec_n       =    dblarr(ndrep,ndprt,ndprt,2,ndt)
    ilrrec_n     =    lonarr(ndrep,ndprt,ndprt,2)
    rrec_sum     =    dblarr(ndprt,ndprt,2,ndt)
    ilrrec_sum   =    lonarr(ndprt,ndprt,2)
    rrec_tot     =    dblarr(ndprt,ndt)
    ilrrec_tot   =    lonarr(ndprt)
    nte          =    0L
    tea          =    dblarr(ndt)

; To reserve memory we need to fill these strings full of blanks

    ctype     = '  '
    ictype    = long(byte(ctype))
    seqsym    = '  '
    iseqsym   = long(byte(seqsym))

    cstrga    = strarr(ndlev) + '                    '
    icstrga   = long(byte(cstrga))
    cstrpa    = strarr(ndprt) + '                    '
    icstrpa   = long(byte(cstrpa))
    cspsys    = strarr(ndprt,ndprt,2) + '                              '
    icspsys   = long(byte(cspsys))

; Get binary directory and setup inputs

    fortdir = getenv('ADASFORT')

; Call shared object

    dummy = call_external(fortdir+'/xxdata_48_if.so','xxdata_48_if',     $
                           ndprt    , ndrep    , ndlrep   ,              $
                           ndlev    , ndt      ,                         $
                           dnsfull  ,                                    $
                           iseqsym  , iz       , iz0      , iz1      ,   $
                           ictype   , iform    ,                         $
                           nprnt    , nprnti   , nprntf   , bwnp     ,   $
                           ipa      , icstrpa  , ispa     , ilpa     ,   $
                           xjpa     , wpa      ,                         $
                           nlvl     , bwnr     ,                         $
                           ia       , ip       , icstrga  , isa      ,   $
                           ila      , xja      , wa       , ilauga_res,  $
                           nlrep    , irepa_nl , nlrepa_n , nlrepa_l ,   $
                           nrep     , irepa_n  , nrepa    ,              $
                           iprti    ,                                    $
                           rrec_res , ilrrec_res,                        $
                           iprtf    ,                                    $
                           nsysf    , isys      , ispsys   , icspsys   , $
                           rrec_nl  , ilrrec_nl , rrec_n   , ilrrec_n  , $
                           rrec_sum , ilrrec_sum, rrec_tot , ilrrec_tot, $
                           nte      , tea )

; Restrict the number of final parents to ndprt

nprntf = min([ndprt, nprntf])


; Convert string and logical variables from integers

    seqsym   = string(byte(iseqsym))
    ctype    = string(byte(ictype))
    cstrga   = string(byte(icstrga))
    cstrpa   = string(byte(icstrpa))
    cspsys   = string(byte(icspsys))


; Throw away unncessary data - note hybrid forms do not have resolved
; level/term data.

if (iform EQ 1 OR iform EQ 2) then begin

    ipa          =    ipa[0:nprnt-1]
    ispa         =    ispa[0:nprnt-1]
    ilpa         =    ilpa[0:nprnt-1]
    xjpa         =    xjpa[0:nprnt-1]
    wpa          =    wpa[0:nprnt-1]
    ia           =    ia[0:nlvl-1]
    ip           =    ip[0:nlvl-1]
    isa          =    isa[0:nlvl-1]
    ila          =    ila[0:nlvl-1]
    xja          =    xja[0:nlvl-1]
    wa           =    wa[0:nlvl-1]
    ilauga_res   =    ilauga_res[0:nlvl-1]

    if nlrep GT 0 then begin
       irepa_nl     =    irepa_nl[0:nlrep-1]
       nlrepa_n     =    nlrepa_n[0:nlrep-1]
       nlrepa_l     =    nlrepa_l[0:nlrep-1]
       rrec_nl      =    rrec_nl[0:nlrep-1,0:nprnti-1,0:nprntf-1,*,0:nte-1]
       ilrrec_nl    =    ilrrec_nl[0:nlrep-1,0:nprnti-1,0:nprntf-1,*]
    endif else begin
       irepa_nl     =    0L
       nlrepa_n     =    0L
       nlrepa_l     =    0L
       rrec_nl      =    0.0D0
       ilrrec_nl    =    0L
    endelse

    irepa_n    = irepa_n[0:nrep-1]
    nrepa      = nrepa[0:nrep-1]
    iprti      = iprti[0:nprnt-1]
    rrec_res   = rrec_res[0:nlvl-1,0:nprnti-1,0:nte-1]
    ilrrec_res = ilrrec_res[0:nlvl-1,0:nprnt-1]
    iprtf      = iprtf[0:nprnti-1,0:nprntf-1]
    nsysf      = nsysf[0:nprnti-1,0:nprntf-1]
    isys       = isys[0:nprnti-1,0:nprntf-1,*]
    ispsys     = ispsys[0:nprnti-1,0:nprntf-1,*]
    rrec_n     = rrec_n[0:nrep-1,0:nprnti-1,0:nprntf-1,*,0:nte-1]
    ilrrec_n   = ilrrec_n[0:nrep-1,0:nprnti-1,0:nprntf-1,*]
    rrec_sum   = rrec_sum[0:nprnti-1,0:nprntf-1,*,0:nte-1]
    ilrrec_sum = ilrrec_sum[0:nprnti-1,0:nprntf-1,*]
    rrec_tot   = rrec_tot[0:nprnti-1,0:nte-1]
    ilrrec_tot = ilrrec_tot[0:nprnti-1]
    tea        = tea[0:nte-1]
    cstrpa     = cstrpa[0:nprnt-1]
    cstrga     = cstrga[0:nlvl-1]
    cspsys     = cspsys[0:nprnti-1,0:nprntf-1,*]
    
    ; Assemble fulldata structure

    fulldata  = { iz           :   iz,              $
                  iz0          :   iz0,             $
                  iz1          :   iz1,             $
                  sym          :   seqsym,          $
                  ctype        :   ctype,           $
                  iform        :   iform,           $
                  nprnt        :   nprnt,           $
                  nprnti       :   nprnti,          $
                  nprntf       :   nprntf,          $
                  bwnp         :   bwnp,            $
                  ipa          :   ipa,             $
                  cstrpa       :   cstrpa,          $
                  ispa         :   ispa,            $
                  ilpa         :   ilpa,            $
                  xjpa         :   xjpa,            $
                  wpa          :   wpa,             $
                  nlvl         :   nlvl,            $
                  bwnr         :   bwnr,            $
                  ia           :   ia,              $
                  ip           :   ip,              $
                  cstrga       :   cstrga,          $
                  isa          :   isa,             $
                  ila          :   ila,             $
                  xja          :   xja,             $
                  wa           :   wa,              $
                  lauga_res    :   ilauga_res,      $
                  nlrep        :   nlrep,           $
                  irepa_nl     :   irepa_nl,        $
                  nlrepa_n     :   nlrepa_n,        $
                  nlrepa_l     :   nlrepa_l,        $
                  nrep         :   nrep,            $
                  irepa_n      :   irepa_n,         $
                  nrepa        :   nrepa,           $
                  iprti        :   iprti,           $
                  rrec_res     :   rrec_res,        $
                  lrrec_res    :   ilrrec_res,      $
                  iprtf        :   iprtf,           $
                  nsysf        :   nsysf,           $
                  isys         :   isys,            $
                  ispsys       :   ispsys,          $
                  cspsys       :   cspsys,          $
                  rrec_nl      :   rrec_nl,         $
                  lrrec_nl     :   ilrrec_nl,       $
                  rrec_n       :   rrec_n,          $
                  lrrec_n      :   ilrrec_n,        $
                  rrec_sum     :   rrec_sum,        $
                  lrrec_sum    :   ilrrec_sum,      $
                  rrec_tot     :   rrec_tot,        $
                  lrrec_tot    :   ilrrec_tot,      $
                  nte          :   nte,             $
                  tea          :   tea              }

endif

if iform EQ 5 OR iform EQ 6 then begin
    
    ; Hybrid
    
    ipa          =    ipa[0:nprnt-1]
    ispa         =    ispa[0:nprnt-1]
    ilpa         =    ilpa[0:nprnt-1]
    xjpa         =    xjpa[0:nprnt-1]
    wpa          =    wpa[0:nprnt-1]

    if nlrep GT 0 then begin
       irepa_nl     =    irepa_nl[0:nlrep-1]
       nlrepa_n     =    nlrepa_n[0:nlrep-1]
       nlrepa_l     =    nlrepa_l[0:nlrep-1]
       rrec_nl      =    rrec_nl[0:nlrep-1,0:nprnti-1,0:nprntf-1,*,0:nte-1]
       ilrrec_nl    =    ilrrec_nl[0:nlrep-1,0:nprnti-1,0:nprntf-1,*]
    endif else begin
       irepa_nl     =    0L
       nlrepa_n     =    0L
       nlrepa_l     =    0L
       rrec_nl      =    0.0D0
       ilrrec_nl    =    0L
    endelse

    irepa_n    = irepa_n[0:nrep-1]
    nrepa      = nrepa[0:nrep-1]
    iprti      = iprti[0:nprnt-1]
    iprtf      = iprtf[0:nprnti-1,0:nprntf-1]
    nsysf      = nsysf[0:nprnti-1,0:nprntf-1]
    isys       = isys[0:nprnti-1,0:nprntf-1,*]
    ispsys     = ispsys[0:nprnti-1,0:nprntf-1,*]
    rrec_n     = rrec_n[0:nrep-1,0:nprnti-1,0:nprntf-1,*,0:nte-1]
    ilrrec_n   = ilrrec_n[0:nrep-1,0:nprnti-1,0:nprntf-1,*]
    rrec_sum   = rrec_sum[0:nprnti-1,0:nprntf-1,*,0:nte-1]
    ilrrec_sum = ilrrec_sum[0:nprnti-1,0:nprntf-1,*]
    rrec_tot   = rrec_tot[0:nprnti-1,0:nte-1]
    ilrrec_tot = ilrrec_tot[0:nprnti-1]
    tea        = tea[0:nte-1]
    cstrpa     = cstrpa[0:nprnt-1]
    cspsys     = cspsys[0:nprnti-1,0:nprntf-1,*]

    ; Assemble fulldata structure

    fulldata  = { iz           :   iz,              $
                  iz0          :   iz0,             $
                  iz1          :   iz1,             $
                  sym          :   seqsym,          $
                  ctype        :   ctype,           $
                  iform        :   iform,           $
                  nprnt        :   nprnt,           $
                  nprnti       :   nprnti,          $
                  nprntf       :   nprntf,          $
                  bwnp         :   bwnp,            $
                  ipa          :   ipa,             $
                  cstrpa       :   cstrpa,          $
                  ispa         :   ispa,            $
                  ilpa         :   ilpa,            $
                  xjpa         :   xjpa,            $
                  wpa          :   wpa,             $
                  bwnr         :   bwnr,            $
                  nlrep        :   nlrep,           $
                  irepa_nl     :   irepa_nl,        $
                  nlrepa_n     :   nlrepa_n,        $
                  nlrepa_l     :   nlrepa_l,        $
                  nrep         :   nrep,            $
                  irepa_n      :   irepa_n,         $
                  nrepa        :   nrepa,           $
                  iprti        :   iprti,           $
                  iprtf        :   iprtf,           $
                  nsysf        :   nsysf,           $
                  isys         :   isys,            $
                  ispsys       :   ispsys,          $
                  cspsys       :   cspsys,          $
                  rrec_nl      :   rrec_nl,         $
                  lrrec_nl     :   ilrrec_nl,       $
                  rrec_n       :   rrec_n,          $
                  lrrec_n      :   ilrrec_n,        $
                  rrec_sum     :   rrec_sum,        $
                  lrrec_sum    :   ilrrec_sum,      $
                  rrec_tot     :   rrec_tot,        $
                  lrrec_tot    :   ilrrec_tot,      $
                  nte          :   nte,             $
                  tea          :   tea              }

endif


END
