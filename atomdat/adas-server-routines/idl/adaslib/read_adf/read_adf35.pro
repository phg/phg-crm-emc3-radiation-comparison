; Copyright (c) 2003 Strathclyde University.
;----------------------------------------------------------------------
;+
; PROJECT    :  ADAS
;
; NAME       :  read_adf35
;
; PURPOSE    :  Reads adf35 transmission filter files from IDL
;               Called from IDL using the syntax
;                  read_adf35,file=..., energy=... etc
;
; ARGUMENTS  :  All output arguments will be defined appropriately.
;
;               NAME      I/O    TYPE    DETAILS
; REQUIRED   :  file       I     str     full name of ADAS adf35 file
; OPTIONAL      fulldata   O      -      Structure containing the
;                                        adf35 details. If requested the
;                                        other options are not used.
;                                        The log10 of the transmission
;                                        fraction is returned here.
;               energy     I      ()     energies for which transmission
;                                        fractions are required (eV).
;               trans      O      ()     Transmission fraction.
;               help       I      ()     Display help entry
;
; KEYWORDS   :
;
; NOTES      :  This version reads the data using the xxdata_35.for
;               routine. Glue C code is required.
;
;
; AUTHOR     :  Martin O'Mullane
;
; DATE       :  19-07-2003
;
;
; MODIFIED:
;       1.1     Martin O'Mullane
;               - First version
;       1.2     Allan Whiteford
;               - Added /help keyword
;       1.3     Martin O'Mullane
;               - Replace all with fulldata to maintain consistency
;                 across read_adf routines.
;       1.4     Martin O'Mullane
;               - Increase the number of allowed edges to 10.
;       1.5     Martin O'Mullane
;               - Do not return edges in fulldata if there are none.
;
; VERSION:
;       1.1    19-07-2003
;       1.2    07-04-2005
;       1.3    25-05-2005
;       1.4    08-12-2011
;       1.5    17-02-2017
;-
;----------------------------------------------------------------------

PRO read_adf35, file       = file,       $
                energy     = energy,     $
                trans      = trans,      $
                fulldata   = fulldata,   $
                help       = help

; If asked for help

if keyword_set(help) then begin
   doc_library, 'read_adf35'
   return
endif

; file name

if n_elements(file) EQ 0 then message,'A filename must be given'

file_acc, file, exist, read, write, execute, filetype
if exist ne 1 then message, 'Filter file does not exist '+file
if read ne 1 then message, 'Filter file cannot be read from this userid '+file


; Read in the adf35 by calling the IDL versions of xxdata_35
; Set suitable dimensions

ndedge = 10L
ndpts  = 2500L

xxdata_35, file, ndedge, ndpts, iedge, ipts, xx_edge, xx_energy, xx_trans



; Either return everything or proceed to extract the requested quantities

if arg_present(fulldata) then begin

   if iedge EQ 0 then begin
      
      fulldata = { filename    :  file,                $
                   energy      :  xx_energy[0:ipts-1], $
                   trans       :  xx_trans[0:ipts-1]   }
   
   endif else begin

      edge = xx_edge[0:iedge-1]

      fulldata = { filename    :  file,                $
                   edge        :  edge,                $
                   energy      :  xx_energy[0:ipts-1], $
                   trans       :  xx_trans[0:ipts-1]   }
   
   endelse

endif else begin

   ; Use the d8tran.pro wrapper version of d8tran.for for interpolation

   ieval = n_elements(energy)
   trans = dblarr(ieval)

   for j = 0, ieval-1 do begin

      ein = energy[j]
      d8tran, ndpts, ndedge, ipts, iedge, xx_edge, xx_energy, xx_trans, $
              ein, fout
      trans[j] = fout

   endfor

endelse

LABELEND:

END
