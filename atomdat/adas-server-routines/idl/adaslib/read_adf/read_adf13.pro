;----------------------------------------------------------------------
;+
; PROJECT    :  ADAS
;
; NAME       :  read_adf13
;
; PURPOSE    :  Reads adf13 (SXB) files from the IDL command line.
;               called from IDL using the syntax
;               read_adf13,file=...,block=...,te=... etc
;
; ARGUMENTS  :  All output arguments will be defined appropriately.
;
;               NAME      I/O    TYPE   DETAILS
; REQUIRED   :  file       I     str    full name of ADAS adf13 file
;               block      I     int    selected block
;               te         I     real() temperatures requested
;               dens       I     real() densities requested
; OPTIONAL      data       O      -     SXB data
;               wlngtht    O      -     wavelength of transition
;               iz0        O     int    return guess of nuclear charge
;                                       (returns -1 if unable to guess)
;               izz        O     int    return guess of ion charge
;                                       (returns -1 if unable to guess)
;               iz1        O     int    return guess of ion charge+1
;                                       (returns -1 if unable to guess)
;               fulldata   O     str    all of the data from xxdata_13.
;                                         a13file : filename
;                                         esym    : element
;                                         iz0     : atomic number
;                                         iz      : ion charge
;                                         iz1     : recombined ion charge
;                                         ndens   : number of densities
;                                         nte     :           temperatures
;                                         dens    : densities (dim - dens, block)
;                                         te      : temperatures
;                                         cpcode  : production code
;                                         cindm   : metastable
;                                         cwavel  : wavelength
;                                         cfile   : adf04 file (fragment)
;                                         sxb     : ionisations per photon
;
;
; KEYWORDS      all               -     if specified data is 2D of
;                                       temperature and density.
;               kelvin            -     requested temperature in K (default eV)
;               help              -     displays help entry
;
; NOTES      :  This is part of a chain of programs - read_adf13.c and
;               readadf13.for are required.
;
; AUTHOR     :  Martin O'Mullane
;
; DATE       :  19-07-99
;
; MODIFIED:
;       1.2     Martin O'Mullane
;                 - No limit on number of Te/dens returned
;                 - Returns a 2D array if /all is specified.
;                 - Temperatures can be requested in K if /kelvin is specified.
;       1.3     Martin O'Mullane
;		  - Regularised comments.
;       1.4     Allan Whiteford
;                 - Added in functionality to guess at nuclear and ion charge.
;       1.5     Allan Whiteford
;                 - Added /help keyword.
;       1.6     Martin O'Mullane
;                 - Add fulldata output structure.
;       1.7     Martin O'Mullane
;                 - Increase number of allowed temperatures in fulldata to
;                   allow H0 sxb to be read (24 to 35).
;       1.8     Martin O'Mullane
;                 - Change ctype to cpcode.
;       1.9     Martin O'Mullane
;                 - Get iz0, izz and iz1 from fortran rather than
;                   re-opening the file.
;                 - Use longs for loop control when /all is set.
;
; VERSION:
;       1.1    19-07-1999
;       1.2    05-03-2001
;       1.3    15-03-2002
;       1.4    08-05-2003
;       1.5    07-04-2005
;       1.6    17-02-2006
;       1.7    20-11-2013
;       1.8    17-07-2017
;       1.9    21-08-2018
;-
;----------------------------------------------------------------------

PRO read_adf13, file     = file,     $
                block    = block,    $
                te       = te,       $
                dens     = dens,     $
                data     = data,     $
                wlngth   = wlngth,   $
                all      = all,      $
                kelvin   = kelvin,   $
                iz0      = iz0,      $
                izz      = izz,      $
                iz1      = iz1,      $
                fulldata = fulldata, $
                help     = help


; If asked for help

if keyword_set(help) then begin
   doc_library, 'read_adf13'
   return
endif

; Check that we get all inputs and that they are correct. Otherwise print
; a message and return to command line

on_error, 2



; file name

if n_elements(file) eq 0 then message, 'A file name must be passed'
file_acc, file, exist, read, write, execute, filetype
if exist ne 1 then message, 'SXB file does not exist '+file
if read ne 1 then message, 'SXB file cannot be read from this userid '+file

; If fulldata selected retrieve the data into a structure and return.

if arg_present(fulldata) then begin

   nstore = 500
   ntdim  = 35
   nddim  = 24

   xxdata_13, file    ,                             $
              nstore  , ntdim  , nddim  ,           $
              iz0     , iz     , iz1    , esym  ,   $
              nbsel   , isela  ,                    $
              cwavel  , cfile  , cpcode , cindm ,   $
              ita     , ida    ,                    $
              teta    , teda   , sxb

   ftrim    = sxb[0:max(ita)-1, 0:max(ida)-1, 0:nbsel-1]

   fulldata = { a13file : file,                             $
                esym    : esym,                             $
                iz0     : iz0,                              $
                iz      : iz ,                              $
                iz1     : iz1,                              $
                ndens   : ida[0:nbsel-1],                   $
                nte     : ita[0:nbsel-1],                   $
                dens    : teda[0:max(ida)-1, 0:nbsel-1],    $
                te      : teta[0:max(ita)-1, 0:nbsel-1],    $
                cpcode  : cpcode[0:nbsel-1],                $
                cindm   : cindm[0:nbsel-1],                 $
                cwavel  : cwavel[0:nbsel-1],                $
                cfile   : cfile[0:nbsel-1],                 $
                sxb     : ftrim                             }
   return

endif


; selection block

if n_elements(block) eq 0 then message, 'An SXB index must be selected'
parsize=size(block, /N_DIMENSIONS)
if parsize ne 0 then message,'Selection index cannot be an array'
partype=size(block, /TYPE)
if (partype ne 2) and (partype ne 3) then begin
   message,'Selection index must be numeric'
endif else ibsel = LONG(block)



; temperature and density

if n_elements(te) eq 0 then message, 'User requested temperatures are missing'
if n_elements(dens) eq 0 then message, 'User requested densities are missing'

partype=size(te, /type)
if (partype lt 2) or (partype gt 5) then begin
   message,'Temperature must be numeric'
endif  else te = DOUBLE(te)

partype=size(dens, /type)
if (partype lt 2) or (partype gt 5) then begin
   message,'densmperature must be numeric'
endif else dens = DOUBLE(dens)

; Kelvin rather than eV

if keyword_set(kelvin) then te = te/11605.0

if n_elements(wlngth) eq 0 then wlngth = DOUBLE(0.0) else wlngth = DOUBLE(wlngth)



; Get the data by calling the fortran routine

; Do we want a 2D array (/all set) or 1D on (te, dens) vector

len_te   = n_elements(te)
len_dens = n_elements(dens)

if keyword_set(all) then begin

   ; make a big 1D list

   t_in = dblarr(len_te*len_dens)
   d_in = dblarr(len_te*len_dens)

   k = 0L
   for i = 0, len_te-1 do begin
      for j = 0, len_dens-1 do begin
         t_in[k] = te[i]
         d_in[k] = dens[j]
         k = k + 1
       endfor
    endfor
    itval = k


endif else begin

   ; are te and dens the same length and one dimensional?
   ; if so define data array

   if len_dens ne len_te then print, 'TE and DENS size mismatch - smaller  used'
   itval = min([len_te,len_dens])

   t_in   = te
   d_in   = dens

endelse


itval = LONG(itval)


fortdir = getenv('ADASFORT')


; Get data in blocks of 99 to avoid altering xxspln for large sets.

data = 0.0D0
iz0  = -1L
izz  = -1L
iz1  = -1L

MAXVAL = 99L

n_call = numlines(itval, MAXVAL)

for j = 0L, n_call - 1 do begin

  ist = j*MAXVAL
  ifn = min([(j+1)*MAXVAL,itval])-1

  t_ca     = t_in[ist:ifn]
  d_ca     = d_in[ist:ifn]
  data_ca  = dblarr(ifn-ist+1)
  itval_ca = ifn-ist+1

  dummy = CALL_EXTERNAL(fortdir+'/read_adf13.so','read_adf13',  $
                        file, ibsel,                            $
                        itval_ca, t_ca, d_ca, data_ca,          $
                        iz0, izz, iz1, wlngth)

  data = [data, data_ca]

endfor
data = data[1:*]

if keyword_set(all) then begin

   d_out = dblarr(len_te, len_dens)
   k = 0L                                   ; there must be a better way!
   for i = 0, len_te-1 do begin
      for j = 0, len_dens-1 do begin
         d_out[i,j] = data[k]
         k = k + 1
      endfor
   endfor

   data = d_out

endif

; Reset temperature to K

if keyword_set(kelvin) then te = te*11605.0

END
