;----------------------------------------------------------------------
;+
; PROJECT    :  ADAS
;
; NAME       :  read_adf24
;
; PURPOSE    :  Reads adf24 (SCX) files from the IDL command line.
;               called from IDL using the syntax
;               read_adf24,file=...,block=...,te=... etc
;
; ARGUMENTS  :  All output arguments will be defined appropriately.
;
;               NAME      I/O    TYPE   DETAILS
; REQUIRED   :  file       I     str    full name of ADAS adf24 file
;               block      I     int    selected block
;               energy     I     real() energies requested (eV/amu).
; OPTIONAL      data       O      -     SCX data in cm**2
;               fulldata   O     str    all of the data from xxdata_13.
;                                         a24file : filename
;                                         esym    : element
;                                         iz0     : atomic number         
;                                         iz      : ion charge            
;                                         iz1     : recombined ion charge 
;                                         neng    : number of energies (block)
;                                         teea    : energies (eV/amu)
;                                         cdonor  : donor name
;                                         crecvt  : receiver name
;                                         cfstat  : final state
;                                         ctype   : type of cross-section
;                                         scx     : cross sections (cm2)
;
; KEYWORDS      help              -     displays help entry
;
; NOTES      :  This is part of a chain of programs - read_adf24.c and 
;               readadf24.for are required.
;
; AUTHOR     :  Martin O'Mullane
; 
; DATE       :  11-06-2001
; 
; MODIFIED:
;       1.2     Martin O'Mullane
;               - e9spln has a max of 24 points, not 30 as assumed here.
;       1.3     Allan Whiteford
;               - Added /help keyword
;       1.4     Martin O'Mullane
;                 - Add fulldata output structure.
;
; VERSION:
;       1.2    26-05-2002 
;       1.3    07-04-2005 
;       1.4    27-03-2006
;-
;----------------------------------------------------------------------

PRO read_adf24, file     = file,     $
                block    = block,    $
                energy   = energy,   $ 
                data     = data,     $
                fulldata = fulldata, $
                help     = help

; If asked for help

if keyword_set(help) then begin
   doc_library, 'read_adf24'
   return
endif

; Check that we get all inputs and that they are correct. Otherwise print
; a message and return to command line

on_error, 2



; file name

if n_elements(file) eq 0 then message, 'A file name must be passed'
file_acc, file, exist, read, write, execute, filetype
if exist ne 1 then message, 'SCX file does not exist '+file
if read ne 1 then message, 'SCX file cannot be read from this userid '+file


; If fulldata selected retrieve the data into a structure and return.

if arg_present(fulldata) then begin

   nstore = 50
   nedim  = 24

   xxdata_24, file    ,                             $ 
              nstore  , nedim  ,                    $
              iz0     , iz     , iz1    , esym  ,   $
              nbsel   , isela  ,                    $
              cdonor  , crecvr , cfstat , ctype ,   $
              alph0   ,iea     , teea   , scx


   ftrim    = scx[0:max(iea)-1, 0:nbsel-1]

   fulldata = { a24file : file,                             $
                esym    : esym,                             $
                iz0     : iz0,                              $
                iz      : iz ,                              $
                iz1     : iz1,                              $
                neng    : iea[0:nbsel-1],                   $
                alpha   : alph0[0:nbsel-1],                 $
                teea    : teea[0:max(iea)-1, 0:nbsel-1],    $
                cdonor  : cdonor[0:nbsel-1],                $
                crecvr  : crecvr[0:nbsel-1],                $
                cfstat  : cfstat[0:nbsel-1],                $
                ctype   : ctype[0:nbsel-1],                 $
                scx     : ftrim                             }
   return

endif


; selection block

if n_elements(block) eq 0 then message, 'An SCX block must be selected'
parsize=size(block, /N_DIMENSIONS)
if parsize ne 0 then message,'Selection index cannot be an array'
partype=size(block, /TYPE)
if (partype ne 2) and (partype ne 3) then begin 
   message,'Selection index must be numeric'
endif else ibsel = LONG(block)

        
                
; reaction energy

if n_elements(energy) eq 0 then message, 'User requested energies are missing'

partype=size(energy, /type)
if (partype lt 2) or (partype gt 5) then begin 
   message,'Energies must be numeric'
endif  else energy = DOUBLE(energy)


; set variables for call to C/fortran reading of data

ieval = n_elements(energy)
ieval = LONG(ieval)
data  = DBLARR(ieval)

i_ion = 'ABCDE'
f_ion = 'abcde'
i_met = LONG(0)
f_met = LONG(0)
bwno  = DOUBLE(0.0)


e_in  = energy

fortdir = getenv('ADASFORT')

; Get data in blocks of 30 to avoid altering xxspln for large sets.

data = 0.0D0

MAXVAL = 24

n_call = numlines(ieval, MAXVAL)

for j = 0, n_call - 1 do begin 
  
  ist = j*MAXVAL
  ifn = min([(j+1)*MAXVAL,ieval])-1
  
  e_ca     = e_in[ist:ifn]
  data_ca  = dblarr(ifn-ist+1)
  ieval_ca = ifn-ist+1
 
  dummy = CALL_EXTERNAL(fortdir+'/read_adf24.so','read_adf24', $
                        file, ibsel,                           $
                        ieval_ca, e_ca, data_ca                )
  
  data = [data, data_ca]
  
endfor
data = data[1:*]


                                            
END
