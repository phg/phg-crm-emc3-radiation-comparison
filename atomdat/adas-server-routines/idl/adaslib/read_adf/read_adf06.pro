;----------------------------------------------------------------------
;+
; PROJECT    :  ADAS
;
; NAME       :  read_adf06
;
; PURPOSE    :  Write an adf06 specific ion file using the
;               fulldata structure (from read_adf06 or elsewhere).
;
;
; ARGUMENTS  :  All output arguments will be defined appropriately.
;
;               NAME      I/O    TYPE    DETAILS
; REQUIRED   :  fulldata   I       -     Structure containing the
;                                        adf06 details.
;               outfile    O      str    name of new adf06 file.
;               comments   I      str()  write these comments at end.
;
; KEYWORDS      help               -     if specified this comment
;                                        section is written to screen.
;
;
; NOTES      :
;
; The fulldata structure is defined:
;
;                filename            :  adf06 filename
;                iz                  :  recombined ion charge
;                iz0                 :  nuclear charge
;                iz1                 :  recombining ion charge
;                ia()                :  energy level index number
;                                            dimension - level index
;                cstrga()            :  nomenclature/configuration for level 'ia()'
;                isa()               :  multiplicity for level 'ia()'
;                                       note: (isa-1)/2 = quantum number (s)
;                                            dimension - level index
;                ila()               :  quantum number (l) for level 'ia()'
;                                            dimension - level index
;                xja()               :  quantum number (j-value) for level 'ia()'
;                                       note: (2*xja)+1 = statistical weight
;                                            dimension - level index
;                wa()                :  energy relative to level 1 (cm-1) for level'ia()'
;                                            dimension - level index
;                zpla(,)             :  eff. zeta param. for contributions to ionis. of level
;                                           1st dimension - parent index
;                                           2nd dimension - level index
;                ipla(,)             :  parent index for contributions to ionis. of level
;                                            1st dimension - parent index
;                                            2nd dimension - level index
;                bwno                :  ionisation potential (cm-1) of lowest parent
;                bwnoa()             :  ionisation potential (cm-1) of parents
;                                            dimension - metastable index
;                cprta               :
;                qdorb()             :  quantum defects for orbitals
;                lqdorb()                 1  => source data available for qd.
;                                         0 => source data not availabe qd.=0.0
;                qdn()               :  quantum defect for n-shells.  non-zero only
;                                       for adf06 files with orbital energy data
;                iorb                :  number of orbital energies
;                te()                :  input data file: electron temperatures (K) type 3
;                                                        X = e/delta_E for type 1
;                                            dimension - temperature
;                lower()             :  upper index for transition
;                                            dimension - transition
;                upper()             :  lower index for transition
;                                            dimension - transition
;                aval()              :  E2 quadrupole A-value
;                                            dimension - transition
;                gamma(,,)           :  gamma values
;                                           effective collision strength for type 3
;                                           collision strength for type 1
;                                           scaled rate coefft.(cm3 sec-1)(case 'S')
;                                           1st dimension - transition number
;                                           2nd dimension - temperature 'te()'
;                                           3rd dimension - ion projectile case
;                lbeth               :  1 => Bethe data on transition lines
;                                       0 => not present in file
;                beth(,)             :  Born limit
;                                            1st dimension - transition
;                                            2nd dimension - ion projectile case
;                tcode()             : data type
;                                            ' ' => ion impact transition
;                                            's','S' => Ionisation from current ion
;                                            dimension - transition
;                projectile()        : name of projectile, eg 7^Li
;                                            dimension - projecties
;                mass_target()       : mass of target
;                                            dimension - projecties
;                charge_projectile() : charge of projectile
;                                            dimension - projecties
;                mass_projectile()   : mass of projectile
;                                            dimension - projecties
;                level_ion()         : Ionisation from level
;                                            set to -1 if no data present
;                                            dimension - ionisation transitions
;                parent_ion()        : Ionisation to parent
;                                            set to -1 if no data present
;                                            dimension - ionisation transitions
;                ion(,,)             :  ion values
;                                           effective collision strength for type 3
;                                           collision strength for type 1
;                                           scaled, mass-independent, rate coefft.(cm3 sec-1)(case 'S')
;                                           1st dimension - transition number
;                                           2nd dimension - temperature 'te()'
;                                           3rd dimension - ion projectile case
;                ltied()             :  1 => specified level tied
;                                       0 => specified level is untied
;                iadftyp             : as input
;
;
;
; AUTHOR     :  Martin O'Mullane
;
; DATE       :  31-10-2016
;
;
; MODIFIED:
;       1.1     Martin O'Mullane
;                 - First version.
;
; VERSION:
;       1.1    03-09-2018
;-
;----------------------------------------------------------------------

PRO read_adf06, file       = file,       $
                fulldata   = fulldata,   $
                te         = te,         $
                lower      = lower,      $
                upper      = upper,      $
                projectile = projectile, $
                gamma      = gamma,      $
                rate       = rate,       $
                dex_rate   = dex_rate,   $
                level      = level,      $
                parent     = parent,     $
                kelvin     = kelvin,     $
                help       = help

; If asked for help

if keyword_set(help) then begin
   doc_library, 'read_adf06'
   return
endif


; file name

if n_elements(file) EQ 0 then message,'A filename must be given'

file_acc, file, exist, read, write, execute, filetype
if exist ne 1 then message, 'adf06 file does not exist ' + file
if read ne 1 then message, 'adf06 file cannot be read from this userid ' + file

; fulldata structure requested

if arg_present(fulldata) then begin

   xxdata_06, file=file, fulldata=fulldata, itieactn=0L
   return

endif


; check inputs

xxdata_06, file=file, fulldata=all

if n_elements(te) EQ 0 then begin
   message, 'Using the temperature set in adf06 file', /continue
   if all.iadftyp EQ 3 then te = all.te / 11605.0 else te = all.te
endif
if keyword_set(kelvin) then begin
   te_in = te /11605.0
   if all.iadftyp EQ 1 then message, 'Kelvin is not meaningful for type 1 files'
endif else begin
   te_in = te
endelse

str_06 = strcompress(strupcase(all.projectile), /remove_all)
if n_elements(projectile) EQ 0 then begin
   if n_elements(str_06) EQ 1 then begin
      str_pj = str_06
      message, 'Using the only projectile in adf06 file', /continue
   endif else begin
      message, 'The adf06 file has more than one projectile. Specify which one to use.'
   endelse
endif else begin
   str_pj = strcompress(strupcase(projectile), /remove_all)
endelse
ind_p = where(strpos(str_06, str_pj) EQ 0, c1)
ind_p = ind_p[0]

if c1 EQ 0 then message, 'Select a projectile present in adf06 file'
if c1 GT 1 then message, 'Repeated projectiles in adf06 file'

i1 = n_elements(upper)
i2 = n_elements(lower)
i3 = n_elements(level)
i4 = n_elements(parent)




; Excitation

if (i1+i2) NE 0 then begin

   if i1 NE i2 then message, 'Set one upper and one lower level only'
   if i3 GT 0 then message, 'Cannot combine excitation and ionisation'
   if i4 GT 0 then message, 'Cannot combine excitation and ionisation'

   if lower GT upper then begin
      itmp  = lower
      lower = upper
      upper = itmp
      message, 'Using reversed lower and upper indices', /continue
   endif

   u = where(all.upper EQ upper, n_u)
   l = where(all.lower EQ lower, n_l)

   if (n_l GT 0) AND (n_u GT 0) then begin

      ind = setintersection(u,l)
      if n_elements(ind) GT 0 AND ind[0] GT -1 then begin
          scef  = all.te
          gamma = reform(all.gamma[ind,*,ind_p])
      endif else begin
          print, 'Transition is not available : ' + string(upper) + ' - '+ string(lower)
          gamma = -1
          return
      endelse

      xin  = scef
      yin  = gamma
      if all.iadftyp EQ 3 then xout = te_in * 11605.0 else xout = te_in

      xxsple, opt=0, xin=xin, yin=yin, xout=xout, yout=yout, /log

      wlower = 2.0*all.xja[lower-1] + 1.0
      elower = all.wa[lower-1]
      wupper = 2.0*all.xja[upper-1] + 1.0
      eupper = all.wa[upper-1]

      mt = all.mass_target[ind_p]
      mp = all.mass_projectile[ind_p]
      mu = mt * mp / (mt + mp)

      rate = 2.17161d-8 * mu^(-1.5) * yout * sqrt(157890.0 / xout) / $
             (wlower * exp(1.4388 * (eupper-elower) / xout))

      dex_rate = (wlower / wupper) * exp(1.4388 * (eupper-elower) / xout) * rate

      gamma = yout

   endif else begin

      print, 'Transition is not available : ' + string(upper) + ' - '+ string(lower)
      gamma = -1
      return

   endelse

endif

; ionisation

if (i3+i4) NE 0 then begin

   if i3 NE i4 then message, 'Set one level and one parent'
   if i1 GT 0 then message, 'Cannot combine excitation and ionisation'
   if i2 GT 0 then message, 'Cannot combine excitation and ionisation'

   u = where(all.level_ion EQ level, n_u)
   l = where(all.parent_ion EQ parent, n_l)

   if (n_l GT 0) AND (n_u GT 0) then begin

      ind = setintersection(u,l)
      if n_elements(ind) GT 0 AND ind[0] GT -1 then begin
          scef  = all.te
          gamma = reform(all.ion[ind,*,ind_p])
      endif else begin
          print, 'Transition is not available : ' + string(upper) + ' - ' + string(lower)
          gamma = -1
          return
      endelse

      xin  = scef
      yin  = gamma
      if all.iadftyp EQ 3 then xout = te_in * 11605.0 else xout = te_in

      xxsple, opt=0, xin=xin, yin=yin, xout=xout, yout=yout, /log

      mt = all.mass_target[ind_p]
      mp = all.mass_projectile[ind_p]
      mu = mt * mp / (mt + mp)

      ip_ev = (all.bwnoa[parent-1] - all.wa[level-1]) * 13.6048/109737.26
      rate  = yout * exp(ip_ev/xout) * mu^(-1.5)

      gamma = yout

   endif else begin

      print, 'Process is not available : ' + string(level) + ' - ' + string(parent)
      gamma = -1
      return

   endelse

endif

; Rates are only meaningful for type 3 adf06 files

errstr = string(all.iadftyp, format='(i1)')

if all.iadftyp NE 3 then begin

   if arg_present(rate) then message, 'Rates are not meaningful for type ' + errstr + ' files'
   if arg_present(dex_rate) then message, 'De-excitation rates are not meaningful for type ' + errstr + ' files'

endif

END
