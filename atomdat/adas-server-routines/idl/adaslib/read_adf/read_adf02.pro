; Copyright (c) 2001 Strathclyde University.
;----------------------------------------------------------------------
;+
; PROJECT    :  ADAS
;
; NAME       :  read_adf02
;
; PURPOSE    :  Reads adf02 (SIA ionatom) files from the IDL command line.
;               called from IDL using the syntax
;               read_adf02,file=...,block=...,te=... etc
;
; ARGUMENTS  :  All output arguments will be defined appropriately.
;
;               NAME      I/O    TYPE   DETAILS
; REQUIRED   :  file       I     str    full name of ADAS adf02 file
;               block      I     int    selected block
;               energy     I     real() energies requested (eV/amu).
; OPTIONAL      data       O      -     SIA data in cm**2
;               info       O     struct information on reaction
;                                          donor     : donor
;                                          receiver  : receiver
;                                          type      : type of interaction
;                                          alpha     : power law slope in energy
;                                          threshold : threshold energy
;                                          mass_d    : mass donor
;                                          mass_r    : mass receiver
;
;               help       I     Display help entry
; KEYWORDS
;
; NOTES      :  This is part of a chain of programs - read_adf02.c and
;               readadf02.for are required.
;
; The fulldata structure is defined:
;
;    fulldata = { filename  :  filename, $
;                 nstore    :  nstore,   $
;                 nedim     :  nedim,    $
;                 nbsel     :  nbsel,    $
;                 isela     :  isela,    $
;                 cprimy    :  cprimy,   $
;                 csecdy    :  csecdy,   $
;                 ctype     :  ctype,    $
;                 ampa      :  ampa,     $
;                 amsa      :  amsa,     $
;                 alpha     :  alpha,    $
;                 ethra     :  ethra,    $
;                 iea       :  iea,      $
;                 teea      :  teea,     $
;                 sia       :  sia       }
;
;
; AUTHOR     :  Martin O'Mullane
;
; DATE       :  11-07-2001
;
; MODIFIED:
;       1.2     Martin O'Mullane
;               - The spline routine only takes 24 values, not 30.
;       1.3     Allan Whiteford
;               - Added /help keyword
;       1.4     Martin O'Mullane
;                - Add the fulldata structure as an optional output.
;       1.5     Martin O'Mullane
;                - Calling arguments for xxdata_02 have changed.
;       1.6     Martin O'Mullane
;                - Add mass of interacting species to info.
;
; VERSION:
;       1.2    29-10-2004
;       1.3    07-04-2005
;       1.4    02-12-2005
;       1.5    22-05-2019
;       1.6    10-07-2019
;-
;----------------------------------------------------------------------

PRO read_adf02, file     = file,      $
                block    = block,     $
                energy   = energy,    $
                data     = data,      $
                info     = info,      $
                fulldata = fulldata,  $
                help     = help

; If asked for help

if keyword_set(help) then begin
   doc_library, 'read_adf02'
   return
endif


; Check that we get all inputs and that they are correct. Otherwise print
; a message and return to command line

on_error, 2



; file name

if n_elements(file) eq 0 then message, 'A file name must be passed'
file_acc, file, exist, read, write, execute, filetype
if exist ne 1 then message, 'SIA file does not exist '+file
if read ne 1 then message, 'SIA file cannot be read from this userid '+file


; Either return everything or proceed to extract the requested quantities

if arg_present(fulldata) then begin

   xxdata_02, file=file, fulldata=fulldata
   return

endif

; selection block

if n_elements(block) eq 0 then message, 'An SIA block must be selected'
parsize=size(block, /N_DIMENSIONS)
if parsize ne 0 then message,'Selection index cannot be an array'
partype=size(block, /TYPE)
if (partype ne 2) and (partype ne 3) then begin
   message,'Selection index must be numeric'
endif else ibsel = LONG(block)



; reaction energy

if n_elements(energy) eq 0 then message, 'User requested energies are missing'

partype=size(energy, /type)
if (partype lt 2) or (partype gt 5) then begin
   message,'Energies must be numeric'
endif  else energy = DOUBLE(energy)


; set variables for call to C/fortran reading of data

ieval = n_elements(energy)
ieval = LONG(ieval)
data  = DBLARR(ieval)

s_don  = 'ABCDE'
s_rec  = 'abcde'
s_type = 'ABC'
alpha  = 0.0D0
ethrsh = 0.0D0
massd  = 0.0D0
massr  = 0.0D0


e_in  = energy

file_in = string(replicate(32B, 132))
strput, file_in, file, 0

fortdir = getenv('ADASFORT')

; Get data in blocks of 30 to avoid altering xxspln for large sets.

data = 0.0D0

MAXVAL = 24L

n_call = numlines(ieval, MAXVAL)

for j = 0, n_call - 1 do begin

  ist = j*MAXVAL
  ifn = min([(j+1)*MAXVAL,ieval])-1

  e_ca     = e_in[ist:ifn]
  data_ca  = dblarr(ifn-ist+1)
  ieval_ca = ifn-ist+1

  dummy = CALL_EXTERNAL(fortdir+'/read_adf02.so','read_adf02', $
                        file_in, ibsel,                        $
                        ieval_ca, e_ca, data_ca,               $
                        alpha, ethrsh, massd, massr,           $
                        s_don, s_rec, s_type                   )

  data = [data, data_ca]

endfor
data = data[1:*]

info = { donor     :  s_don,   $
         receiver  :  s_rec,   $
         type      :  s_type,  $
         alpha     :  alpha,   $
         threshold :  ethrsh,  $
         mass_d    :  massd,   $
         mass_r    :  massr    }


END
