;----------------------------------------------------------------------
;+
; PROJECT    :  ADAS
;
; NAME       :  read_adf19
;
; PURPOSE    :  Reads adf19 (PZD) files from the IDL command line.
;               called from IDL using the syntax
;               read_adf19,file=...,block=...,te=... etc
;
; ARGUMENTS  :  All output arguments will be defined appropriately.
;
;               NAME      I/O    TYPE   DETAILS
; REQUIRED   :  file       I     str    full name of ADAS adf19 file
;               block      I     int    selected block
;               te         I     real() temperatures requested
; OPTIONAL      data       O      -     PZD data
;               info       O      -     information in a structure
;                                       info.i_ion  = initial ion <el>+z
;                                       info.i_type = radiation type
;                                       info.i_info = extra information
;               fulldata   O     str    all of the data from xxdata_19.
;                                        esym                : Element
;                                        iz0                 :  Nuclear charge
;                                        nblock              :  Number of ionisation rates
;                                        iz[nblock]          :  Initial charge
;                                        iz1 [nblock]        :  Final charge
;                                        ciion[nblock]       :  Radiating ion
;                                        citype[nblock]      :  Radiation type
;                                        ciinfo[nblock]      :  Information string
;                                        nte[nblock]         :  Number of temperatures in each block
;                                        te[nte,nblock]      :  Temperatures
;                                        pzd[max(nte),nblock]:  Ionisation rates
;                                        filename            :  Filename read
;
; KEYWORDS      kelvin            -     requested temperature in K (default eV)
;               help              -     displays help entry
;
; NOTES      :  This is part of a chain of programs - read_adf19.c and
;               readadf19.for are required.
;
; AUTHOR     :  Martin O'Mullane
;
; DATE       :  03-07-2008
;
; MODIFIED   :
;       1.1     Martin O'Mullane
;                 - First version.
;
; VERSION    :
;       1.1    03-07-2008
;-
;----------------------------------------------------------------------

PRO read_adf19, file      = file,     $
                block     = block,    $
                te        = te,       $
                data      = data,     $
                info      = info,     $
                fulldata  = fulldata, $
                kelvin    = kelvin,   $
                help      = help

; If asked for help

if keyword_set(help) then begin
   doc_library, 'read_adf19'
   return
endif

; Check that we get all inputs and that they are correct. Otherwise print
; a message and return to command line

on_error, 2



; file name

if n_elements(file) eq 0 then message, 'A file name must be passed'
file_acc, file, exist, read, write, execute, filetype
if exist ne 1 then message, 'PZD file does not exist '+file
if read ne 1 then message, 'PZD file cannot be read from this userid '+file


; If all of the data required put it in fulldata and return

if arg_present(fulldata) then begin

   xxdata_19, file = file, fulldata = fulldata
   return

endif



; selection block

if n_elements(block) eq 0 then message, 'An PZD block must be selected'
parsize=size(block, /N_DIMENSIONS)
if parsize ne 0 then message,'Selection index cannot be an array'
partype=size(block, /TYPE)
if (partype ne 2) and (partype ne 3) then begin
   message,'Selection index must be numeric'
endif else ibsel = LONG(block)



; temperature

if n_elements(te) eq 0 then message, 'User requested temperatures are missing'

partype=size(te, /type)
if (partype lt 2) or (partype gt 5) then begin
   message,'Temperature must be numeric'
endif  else te = DOUBLE(te)

; Kelvin rather than eV

if keyword_set(kelvin) then te = te/11605.0


; set variables for call to C/fortran reading of data

itval = n_elements(te)
itval = LONG(itval)
data  = DBLARR(itval)

i_ion  = 'ABCDE'
i_type = 'abcde'
i_info = '12345678901234567890'


t_in  = te

fortdir = getenv('ADASFORT')

; Get data in blocks of 30 to avoid altering xxspln for large sets.

data = 0.0D0

MAXVAL = 24L

n_call = numlines(itval, MAXVAL)

for j = 0L, n_call - 1 do begin

  ist = j*MAXVAL
  ifn = min([(j+1)*MAXVAL,itval])-1

  t_ca     = t_in[ist:ifn]
  data_ca  = dblarr(ifn-ist+1)
  itval_ca = ifn-ist+1

  dummy = CALL_EXTERNAL(fortdir+'/read_adf19.so','read_adf19', $
                        file, ibsel,                           $
                        itval_ca, t_ca, data_ca,               $
                        i_ion, i_type, i_info)

  data = [data, data_ca]

endfor
data = data[1:*]




; make up the info structure - if one exists get rid of it.

if arg_present(info) then begin

   info = { i_ion  : i_ion,    $
            i_type : i_type,   $
            i_info : i_info    }
endif

; Reset temperature to K

if keyword_set(kelvin) then te = te*11605.0

END
