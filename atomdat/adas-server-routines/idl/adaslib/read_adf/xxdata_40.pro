;----------------------------------------------------------------------
;+
; PROJECT    :  ADAS
;
; NAME       :  xxdata_40
;
; PURPOSE    :  Reads and ADF40 file into an IDL structure
;
; INPUTS     :  Filename - Fully path of ADF40 file to read
;
; OUTPUTS    :  data: Structure containing contents of ADF40 file:
;               esym               Element Symbol   
;               iz0                Nuclear Charge       
;               is                 Partition index (i.e. iz for standard data)  
;               is1                Partition index + 1  
;               nblock             Number of wavelength regions 
;               npix[nblock]       Number of pixels in each block       
;               wave_min[nblock]   Minimum wavelength of each block
;               wave_max[nblock]   Maximum wavelength of each block
;               nte[nblock]        Number of temperatures in each block
;               te[nte,nblock]     Temperatures 
;               ndens[nblock]      Number of densities in each block
;               dens[ndens,nblock] Densities    
;               fpec[max(npix),max(nte),max(ndens),nblock]
;                                  Feature Photon Emissiviy Coefficient 
;               type[nblock]       Type of transition {'excit','recom'} 
;               comments           Comments in ADF40 file
;               filename           Filename read
;                       
; NOTES      :  Calls fortran code via C wrapper, see:
;                       xxdata_40_if.c
;                       xxdata_40_calc.for
;
; AUTHOR     :  Allan Whiteford
;
; DATE       :  09/10/06
;
; MODIFIED:
;       1.1     Allan Whiteford
;               - First version, complete re-write of previous code.
;       1.2     Martin O'Mullane
;               - teda incorrectly dimensioned with ntdim (rather than nddim).
;       1.3     Martin O'Mullane
;                 - Change names of arguments to standard form.
;       1.4     Martin O'Mullane
;                 - Increase number of characters in filename to 132.
;
; VERSION:
;       1.1    09-10-2006
;       1.2    22-06-2012
;       1.3    30-07-2018
;-
;----------------------------------------------------------------------


pro xxdata_40, file=file, fulldata=fulldata

        if not keyword_set(file) then begin
                print,'Filename must be set'
                return
        endif

        ; Convert filename to byte array

        filename=file
        adas_string_pad,filename,132
        dsnin=byte(filename)        
        
        ; Parameters to match fortran
        
        nstore=10L
        ndpix=1024L
        ntdim=40L
        nddim=50L
        ndptnl=4L
        ndptn=128L
        ndptnc=256L
        ndcnct=100L
        ndstack=40L
        ndcmt=2000L
        
        ; Allocate space
                
        iz0=0L
        is=0L
        is1=0L
        esym=bytarr(2)
        nptnl=0L
        nptn=lonarr(ndptnl)
        nptnc=lonarr(ndptnl,ndptn)
        iptnla=lonarr(ndptnl)
        iptna=lonarr(ndptnl,ndptn)
        iptnca=lonarr(ndptnl,ndptn,ndptnc)
        ncnct=0L
        icnctv=lonarr(ndcnct)
        ncptn_stack=0L
        cptn_stack=bytarr(80,ndstack)
        lres=0
        lptn=0
        lcmt=0
        lsup=0
        nbsel=0L
        isela=lonarr(nstore)
        npixa=lonarr(nstore)
        cfile=bytarr(8,nstore)
        ctype=bytarr(8,nstore)
        cindm=bytarr(8,nstore)
        ispbr=lonarr(nstore)
        isppr=lonarr(nstore)
        isstgr=lonarr(nstore)
        ilzr=lonarr(nstore)
        ihzr=lonarr(nstore)
        wvmina=dblarr(nstore)
        wvmaxa=dblarr(nstore)
        ita=lonarr(nstore)
        ida=lonarr(nstore)
        teta=dblarr(ntdim,nstore)
        teda=dblarr(nddim,nstore)
        fpec=dblarr(ndpix,ntdim,nddim,nstore)
        fpec_max=dblarr(nstore)
        ncmt_stack=0L
        cmt_stack=bytarr(80,ndcmt)        

        ; Get Path
        
        adasfort=getenv('ADASFORT')
        if adasfort eq '' then begin
                print,'Cannot locate ADAS binaries'
                return
        endif

        ; Call wrapper routine

        dummy=call_external(    adasfort+'/xxdata_40_if.so','xxdata_40',    $
                                dsnin,                                      $
                                nstore , ndpix   , ntdim  , nddim  ,        $
                                ndptnl , ndptn   , ndptnc , ndcnct ,        $
                                ndstack, ndcmt   ,                          $
                                iz0    , is      , is1    , esym   ,        $
                                nptnl  , nptn    , nptnc  ,                 $
                                iptnla , iptna   , iptnca ,                 $
                                ncnct  , icnctv  ,                          $
                                ncptn_stack      , cptn_stack      ,        $
                                lres   , lptn    , lcmt   , lsup   ,        $
                                nbsel  , isela   ,                          $
                                npixa  , cfile   , ctype  , cindm  ,        $
                                ispbr  , isppr   , isstgr , ilzr   , ihzr , $
                                wvmina , wvmaxa  ,                          $
                                ita    , ida     ,                          $
                                teta   , teda    ,                          $
                                fpec   , fpec_max,                          $
                                ncmt_stack       , cmt_stack                )

        ; convert strings

        cmt_stack=string(cmt_stack)
        cptn_stack=string(cptn_stack)
        cfile=string(cfile)
        ctype=string(ctype)
        cindm=string(cindm)
        esym=string(esym)

        ; throw away unncessary stuff

        isela=isela[0:nbsel-1]
        npixa=npixa[0:nbsel-1]
        cfile=cfile[0:nbsel-1]
        ctype=ctype[0:nbsel-1]
        cindm=cindm[0:nbsel-1]
        ispbr=ispbr[0:nbsel-1]
        isppr=isppr[0:nbsel-1]
        isstgr=isstgr[0:nbsel-1]
        ilzr=ilzr[0:nbsel-1]
        ihzr=ihzr[0:nbsel-1]
        wvmina=wvmina[0:nbsel-1]
        wvmaxa=wvmaxa[0:nbsel-1]
        ita=ita[0:nbsel-1]
        ida=ida[0:nbsel-1]

        teda=teda[0:max(ida)-1,0:nbsel-1]
        teta=teta[0:max(ita)-1,0:nbsel-1]
       
        nptn=nptn[0:nptnl-1]
        iptna=iptna[0:nptnl-1,0:max(nptn)-1]
        nptnc=nptnc[0:nptnl-1,0:max(nptn)-1]
        iptnca=iptnca[0:nptnl-1,0:max(nptn)-1,max(nptnc)]
        iptnla=iptnla[0:nptnl-1]
                
        fpec=fpec[0:max(npixa)-1,0:max(ita)-1,0:max(ida)-1,0:nbsel-1]
        
        cmt_stack=cmt_stack[0:ncmt_stack-1]      
        cptn_stack=cptn_stack[0:ncptn_stack-1]      
        
        ; Put in structure

        fulldata={                                                      $
                        esym            :               esym,           $
                        iz0             :               iz0,            $
                        is              :               is,             $
                        is1             :               is1,            $
                        nblock          :               nbsel,          $
                        npix            :               npixa,          $
                        wave_min        :               wvmina,         $
                        wave_max        :               wvmaxa,         $
                        nte             :               ita,            $
                        te              :               teta,           $
                        ndens           :               ida,            $
                        dens            :               teda,           $
                        fpec            :               fpec,           $
                        type            :               ctype,          $
                        comments        :               cmt_stack,      $
                        filename        :               file            }      
        
end
