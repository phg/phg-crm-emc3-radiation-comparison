;----------------------------------------------------------------------
;+
; PROJECT    :  ADAS
;
; NAME       :  xxdata_08
;
; PURPOSE    :  Reads an ADF08 file into an IDL structure.
;
; INPUTS     :  file - Fully path of ADF08 file to read.
;
; OUTPUTS    :  fulldata: Structure containing contents of ADF08 file:
;                      iz       :  Recombined ion charge
;                      iz0      :  Nuclear charge
;                      iz1      :  Recombining ion charge
;                      nprnt    :  Total number of parents
;                      nprnti   :  Number of parents which are initial parents
;                      bwnp     :  Binding wave no. of ground parent (cm-1)
;                      ipa      :  Number of parent energy levels
;                      ispa     :  Multiplicity for parent level 'ipa()'
;                                  Note: (ispa-1)/2 = quantum number (sp)
;                      ilpa     :  Quantum number (lp) for parent level 'ipa()'
;                      xjpa     :  Quantum number (jp) for parent level 'ipa()'
;                      wpa      :  Energy relative to parent level 1 (cm-1)
;                                  for parent level 'ipa()'
;                      cstrpa   :  Nomencl./config. for parent level 'ipa()'
;                      il       :  Number of energy levels (terms) of
;                                  recombined ion
;                      bwnr     :  Ionisation potential (cm-1) of lowest level
;                                  of recombined ion
;                      ia       :  Recombined ion energy level index number
;                      isa      :  Multiplicity for recombined level 'ia()'
;                      ila      :  Quantum number (l) for recombined level 'ia()'
;                      xja      :  Quantum number (j) for recombined level 'ia()'
;                      wa       :  Energy relative to recombined level 1 (cm-1)
;                                  for recombined level 'ia()'
;                      cstrga   :  Nomencl./config. for recombined ion level 'ia()'
;                      iprti    :  Initial parent block index
;                      isprti   :  Initial parent block term
;                      tprti    :  Initial parent block spin multiplicity
;                      radr     :  Term selective dielec. coeffts.(cm3 s-1)
;                                     1st.dim: level index
;                                     2nd.dim: initial parent index
;                                     3rd.dim: temperature index
;                      nte      :  Number of temperatures
;                      tea      :  Electron temperatures (k)
;                      lradr    :  1 => diel. present for level index
;                                  0 => diel. not present for level index
;                                     1st.dim: level index
;                                     2nd.dim: initial parent index
;
; NOTES      :  Calls fortran code via C wrapper, see:
;                       xxdata_08_if.c
;                       xxdata_08_calc.for
;
; AUTHOR     :  Martin O'Mullane
;
; DATE       :  12-01-2011
;
; MODIFIED:
;       1.1     Martin O'Mullane
;               - First version.
;
; VERSION:
;       1.1    12-01-2011
;
;-
;----------------------------------------------------------------------


PRO xxdata_08,file = file, fulldata = fulldata

if n_elements(file) EQ 0 then message,'Filename must be set'

; Convert filename to byte array

fileb = file
adas_string_pad, fileb, 120
dsnin=byte(fileb)

; Parameters for storage

nstore = 500L
ntdim  = 24L

; Allocate space

ndprt    = 10L
ndlev    = 1000L
ndt      = 14L
iz       = -1L
iz0      = -1L
iz1      = -1L
nprnt    = -1L
nprnti   = -1L
bwnp     = 0.0D0
ipa      = lonarr(ndprt)
ispa     = lonarr(ndprt)
ilpa     = lonarr(ndprt)
xjpa     = dblarr(ndprt)
wpa      = dblarr(ndprt)
il       = -1L
bwnr     = 0.0D0
ia       = lonarr(ndlev)
isa      = lonarr(ndlev)
ila      = lonarr(ndlev)
xja      = dblarr(ndlev)
wa       = dblarr(ndlev)
iprti    = lonarr(ndprt)
isprti   = lonarr(ndprt)
radr     = dblarr(ndlev, ndprt, ndt)
nte      = -1L
tea      = dblarr(ndt)
il_lradr = lonarr(ndlev, ndprt)
str_vars = lonarr(18, 3*ndlev)



; Get Path

adasfort = getenv('ADASFORT')
if adasfort eq '' then message, 'Cannot locate ADAS binaries'

; Call wrapper routine

res = call_external(adasfort+'/xxdata_08_if.so','xxdata_08',   $
                    dsnin  ,                                   $
                    ndprt  , ndlev  , ndt   ,                  $
                    iz     , iz0    , iz1   ,                  $
                    nprnt  , nprnti , bwnp  ,                  $
                    ipa    , ispa   , ilpa  ,                  $
                    xjpa   , wpa    ,                          $
                    il     , bwnr   ,                          $
                    ia     , isa    , ila   ,                  $
                    xja    , wa     ,                          $
                    iprti  , isprti,                           $
                    radr   ,  nte   , tea   ,                  $
                    il_lradr  , str_vars                       )


; Convert output back to strings

res = byte(str_vars)
res = string(res)
res = reform(res,ndlev, 3)

cstrpa  = res[0:nprnt-1, 0]
cstrga  = res[0:il-1, 1]
tprti   = res[0:nprnt-1, 2]

; Return the valid stuff

ipa      = ipa[0:nprnt-1]
ispa     = ispa[0:nprnt-1]
ilpa     = ilpa[0:nprnt-1]
xjpa     = xjpa[0:nprnt-1]
wpa      = wpa[0:nprnt-1]
ia       = ia[0:il-1]
isa      = isa[0:il-1]
ila      = ila[0:il-1]
xja      = xja[0:il-1]
wa       = wa[0:il-1]
iprti    = iprti[0:nprnt-1]
isprti   = isprti[0:nprnt-1]
radr     = radr[0:il-1, 0:nprnt-1, 0:nte-1]
tea      = tea[0:nte-1]
il_lradr = il_lradr[0:il-1, 0:nprnt-1]


; Put in structure

if arg_present(fulldata) then fulldata={ iz       :  iz,       $
                                         iz0      :  iz0,      $
                                         iz1      :  iz1,      $
                                         nprnt    :  nprnt,    $
                                         nprnti   :  nprnti,   $
                                         bwnp     :  bwnp,     $
                                         ipa      :  ipa,      $
                                         ispa     :  ispa,     $
                                         ilpa     :  ilpa,     $
                                         xjpa     :  xjpa,     $
                                         wpa      :  wpa,      $
                                         cstrpa   :  cstrpa,   $
                                         il       :  il,       $
                                         bwnr     :  bwnr,     $
                                         ia       :  ia,       $
                                         isa      :  isa,      $
                                         ila      :  ila,      $
                                         xja      :  xja,      $
                                         wa       :  wa,       $
                                         cstrga   :  cstrga,   $
                                         iprti    :  iprti,    $
                                         isprti   :  isprti,   $
                                         tprti    :  tprti,    $
                                         radr     :  radr,     $
                                         nte      :  nte,      $
                                         tea      :  tea,      $
                                         lradr    :  il_lradr  }

END
