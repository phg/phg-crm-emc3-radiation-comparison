;----------------------------------------------------------------------
;+
; PROJECT    :  ADAS
;
; NAME       :  read_adf16
;
; PURPOSE    :  Reads adf16 (GTN) files from the IDL command line.
;               called from IDL using the syntax
;               read_adf16,file=...,block=...,te=... etc
;
; ARGUMENTS  :  All output arguments will be defined appropriately.
;
;               NAME      I/O    TYPE   DETAILS
; REQUIRED   :  file       I     str    full name of ADAS adf16 file
;               block      I     int    selected block
;               te         I     real() temperatures requested
;               dens       I     real() densities requested
; OPTIONAL      data       O      -     GTN data
;               wlngth     O      -     wavelength of transition
;               iz0        O     int    return guess of nuclear charge
;                                       (returns -1 if unable to guess)
;               izz        O     int    return guess of ion charge
;                                       (returns -1 if unable to guess)
;               iz1        O     int    return guess of ion charge+1
;                                       (returns -1 if unable to guess)
;
; KEYWORDS      all        I      -     if specified data is 2D of
;                                       temperature and density.
;               kelvin     I      -     requested temperature in K (default eV)
;
; NOTES      :  The read_adf15 and xxdata_15 routines are called
;               to read the data.
;
; AUTHOR     :  Martin O'Mullane
;
; DATE       :  27-04-2005
;
; MODIFIED   :
;       1.1     Martin O'Mullane
;                 - First version.
;
; VERSION    :
;       1.1    27-04-2005
;-
;----------------------------------------------------------------------

PRO read_adf16, file     = file,     $
                block    = block,    $
                te       = te,       $
                dens     = dens,     $
                data     = data,     $
                wlngth   = wlngth,   $
                all      = all,      $
                kelvin   = kelvin,   $
                iz0      = iz0,      $
                izz      = izz,      $
                iz1      = iz1,      $
                fulldata = fulldata, $
                help     = help

; If asked for help

if keyword_set(help) then begin
   doc_library, 'read_adf16'
   return
endif

; Check that we get all inputs and that they are correct. Otherwise print
; a message and return to command line

on_error, 2

; file name

if n_elements(file) eq 0 then message, 'A file name must be passed'
file_acc, file, exist, read, write, execute, filetype
if exist ne 1 then message, 'GTN file does not exist '+file
if read ne 1 then message, 'GTN file cannot be read from this userid '+file


; If fulldata selected retrieve the data into a structure and return.

if arg_present(fulldata) then begin

   nstore = 500
   ntdim  = 100
   nddim  = 24

   xxdata_15, file    ,                             $
              nstore  , ntdim  , nddim  ,           $
              iz0     , iz     , iz1    , esym  ,   $
              nbsel   , isela  ,                    $
              cwavel  , cfile  , ctype  , cindm ,   $
              ita     , ida    ,                    $
              teta    , teda   , pec

   ftrim    = pec[0:max(ita)-1, 0:max(ida)-1, 0:nbsel-1]

   fulldata = { a16file : file,                             $
                esym    : esym,                             $
                iz0     : iz0,                              $
                iz      : iz ,                              $
                iz1     : iz1,                              $
                ndens   : ida[0:nbsel-1],                   $
                nte     : ita[0:nbsel-1],                   $
                dens    : teda[0:max(ida)-1, 0:nbsel-1],    $
                te      : teta[0:max(ita)-1, 0:nbsel-1],    $
                cwavel  : cwavel[0:nbsel-1],                $
                gtn     : ftrim                             }
   return

endif


; selection block

if n_elements(block) eq 0 then message, 'A GTN index must be selected'
parsize=size(block, /N_DIMENSIONS)
if parsize ne 0 then message,'Selection index cannot be an array'
partype=size(block, /TYPE)
if (partype ne 2) and (partype ne 3) then begin
   message,'Selection index must be numeric'
endif else ibsel = LONG(block)



; temperature and density

if n_elements(te) eq 0 then message, 'User requested temperatures are missing'
if n_elements(dens) eq 0 then message, 'User requested densities are missing'

partype=size(te, /type)
if (partype lt 2) or (partype gt 5) then begin
   message,'Temperature must be numeric'
endif  else te = DOUBLE(te)

partype=size(dens, /type)
if (partype lt 2) or (partype gt 5) then begin
   message,'densmperature must be numeric'
endif else dens = DOUBLE(dens)


; Get the data by calling read_adf15


read_adf15, file     = file,     $
            block    = block,    $
            te       = te,       $
            dens     = dens,     $
            data     = data,     $
            wlngth   = wlngth,   $
            all      = all,      $
            kelvin   = kelvin,   $
            iz0      = iz0,      $
            izz      = izz,      $
            iz1      = iz1

END
