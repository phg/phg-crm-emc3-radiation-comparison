; Copyright (c) 2003 Strathclyde University.
;----------------------------------------------------------------------
;+
; PROJECT    :  ADAS
;
; NAME       :  xxdata_35
;
; PURPOSE    :  
;
; ARGUMENTS  :  All output arguments will be defined appropriately.
;               Inputs will be converted to correct type, if possible,
;               internally without changing calling type.
;
;               The argument list is the same as fortran except
;               that the file name is passed rather than a unit
;               number.
;
;               NAME      I/O    TYPE    DETAILS
; REQUIRED   :  a35file    I     str     adf35 file name
;               ndedge     I     long    maximum number of energy edges
;               ndpts      I     long    maximum number of energy points
; 
;               iedge      O     long    number of energy edges
;               ipts       O     long    number of energy points
;               edge()     O     double  energies of the edges (eV).
;               energy()   O     double  tabulated energies (eV).
;               trans()    O     double  tabulated transmission.
;
;
; NOTES      :  Calls fortran code.
;
;
; AUTHOR     :  Martin O'Mullane
;
; DATE       :  18/07/03
;
;
; MODIFIED:
;       1.1     Martin O'Mullane
;              	- First version.
;	1.3     Allan Whiteford
;		- Changed wrapper path to be just ADASFORT.
;
;
; VERSION:
;       1.1    18/07/03
;       1.2    10/08/04
;
;-
;----------------------------------------------------------------------

PRO xxdata_35, a35file, ndedge , ndpts  ,        $
                        iedge  , ipts   ,        $
                        edge   , energy , trans
                        
; Check number of parameters

if n_params() NE 8 then message, 'Not all parameters present', /continue
                        
; Check of mandatory inputs and convert to LONG type.

if ndedge EQ 0 then message, 'Set maximum number of edges'
if ndpts EQ 0 then message, 'Set maximum number of energy values'

ndedge = long(ndedge)
ndpts  = long(ndpts)

; Check that the adf35 file exists

file_acc, a35file, exist, read, write, execute, filetype
if exist ne 1 then message, 'adf35 file does not exist '+a04file
if read ne 1 then message, 'adf35 file cannot be read from this userid '+a35file


; Define types of the output parameters

iedge  = -1L
ipts   = -1L
edge   = dblarr(ndedge)
energy = dblarr(ndpts)
trans  = dblarr(ndpts)

file   = string(replicate(32B, 80))
strput, file, a35file, 0

; Location of sources

fortdir = getenv('ADASFORT')
fortdir = fortdir

dummy = CALL_EXTERNAL(fortdir+'/xxdata_35_if.so','xxdata_35_if', $
                      ndedge, ndpts, iedge, ipts,                $
                      edge, energy, trans, file)

END
