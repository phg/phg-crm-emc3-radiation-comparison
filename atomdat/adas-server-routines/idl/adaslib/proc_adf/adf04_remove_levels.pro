;----------------------------------------------------------------------
;+
; PROJECT    :  ADAS
;
; NAME       :  adf04_remove_levels
;
; PURPOSE    :  Removes the user supplied levels from an adf04 file. The
;               result is returned as an adf04 fulldata structure.
;
;
;               NAME        I/O    TYPE   DETAILS
; REQUIRED   :  file         I     str    full name of ADAS adf04 file
;               level_list() I     long   array of levels to be purged.
;                                         Not needed if /untied is set.
;               fulldata     O     struct reduced adf04 data.
;
; KEYWORDS      untied              -     removes untied levels without
;                                         needing to specify level_list.
;               help                -     displays help entry
;
;
; NOTES      :  Relies on read_adf04.pro and xxdata_04.pro which call a compiled
;               fortran routine.
;
; AUTHOR     :  Martin O'Mullane
; DATE       :  12-10-2016
;
; MODIFIED:
;       1.1     Martin O'Mullane
;                 - First version.
;
; VERSION:
;       1.1    12-10-2016
;-
;----------------------------------------------------------------------


PRO adf04_remove_levels, file       = file,       $
                         level_list = level_list, $
                         fulldata   = fulldata,   $
                         untied     = untied,     $
                         help       = help

; If asked for help

if keyword_set(help) then begin
   doc_library, 'adf04_remove_levles'
   return
endif

; file name

if n_elements(file) EQ 0 then message,'A filename must be given'

file_acc, file, exist, read, write, execute, filetype
if exist ne 1 then message, 'adf04 file does not exist '+ file
if read ne 1 then message, 'adf04 file cannot be read from this userid '+ file

; check and options

if keyword_set(untied) then use_untied_list = 1 else use_untied_list = 0
if use_untied_list EQ 0 AND n_elements(level_list) EQ 0 then message, 'Supply list of levels to be removed'
if use_untied_list EQ 1 AND n_elements(level_list) GT 0 then message, 'No need for list when /untied is set'


; Read in the adf04 file

if use_untied_list EQ 1 then begin

   read_adf04, file=file, fulldata=all, /ignore_untied
   ind_untied = where(all.ltied EQ 0, n_untied, complement=ind_tied, ncomplement=n_tied)
   
   if n_untied EQ 0 then begin
   
      message, 'There are no untied levels', /continue
      fulldata = all
      
      return
      
   endif
   
   level_list = ind_untied + 1
   
endif else begin

   read_adf04, file=file, fulldata=all

endelse

; index of levels to be retained - adf04, start at 1

ind_l = -1
map   = intarr(n_elements(all.ia))

k = 1
for j = 0L, n_elements(all.ia)-1 do begin

   res = where(level_list EQ all.ia[j], count)
   if count EQ 0 then begin
      ind_l = [ind_l, all.ia[j]]
      map[j] = k
      k = k + 1
   endif else map[j] = -1

endfor
if n_elements(ind_l) GT 1 then ind_l = ind_l[1:*] else message, 'No valid levels'


; index of transitions to be retained - IDL, start at 0

ind_t = -1

for j = 0L, n_elements(all.upper)-1 do begin

   r1 = where(ind_l EQ all.upper[j], c1)
   r2 = where(ind_l EQ all.lower[j], c2)

   if c1 EQ 1 AND c2 EQ 1 then ind_t = [ind_t, j]

endfor
if n_elements(ind_t) GT 1 then ind_t = ind_t[1:*] else message, 'No valid transitions'

; Re-number transitions

nlev  = n_elements(ind_l)
ntran = n_elements(ind_t)

ia    = indgen(nlev) + 1
lower = all.lower[ind_t]
upper = all.upper[ind_t]

for j = 0L, ntran-1 do begin

   upper[j] = map[upper[j]-1]
   lower[j] = map[lower[j]-1]

endfor

; Output results

fulldata = {iz        : all.iz,               $
            iz0       : all.iz0,              $
            ia        : ia,                   $
            cstrga    : all.cstrga[ind_l-1],  $
            isa       : all.isa[ind_l-1],     $
            ila       : all.ila[ind_l-1],     $
            xja       : all.xja[ind_l-1],     $
            wa        : all.wa[ind_l-1],      $
            bwnoa     : all.bwnoa,            $
            cprta     : all.cprta,            $
            zpla      : all.zpla[0,ind_l-1],  $
            ipla      : all.ipla[0,ind_l-1],  $
            te        : all.te,               $
            beth      : all.beth[ind_t],      $
            lower     : lower,                $
            upper     : upper,                $
            aval      : all.aval[ind_t],      $
            gamma     : all.gamma[ind_t, *],  $
            level_rec : -1,                   $
            level_cx  : -1,                   $
            level_ion : -1,                   $
            iadftyp   : all.iadftyp           }

END
