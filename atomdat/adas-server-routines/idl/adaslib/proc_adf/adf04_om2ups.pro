;-----------------------------------------------------------------------------
;+
; PROJECT    : ADAS
;
; NAME       : adf04_om2ups
;
; PURPOSE    : Converts a type 1 or 5 adf04 to type 3 or 4 for Maxwellian,
;              kappa, Druyvesteyn or numerical electron energy distribution
;              functions.
;
; ARGUMENTS  :  All output arguments will be defined appropriately.
;
;               NAME        I/O    TYPE      DETAILS
; REQUIRED   :  adf04_in     I     string    input type 1 or 5 adf04 file
;               adf04_out    I     string    name of output adf04 file
;               adf37        I     string    numerical electron energy distribution file
;               a04type      I     int       for numerical distributions -
;                                             set to 3 for Maxwellian
;                                                 or 4 for asymmetrical collision strengths
;               kappa        I     float     kappa parameter (1.5 to infinity)
;               x            I     float     Druyvesteyn x parameter
;               te()         I     float     temperatures for adf04 output file
;                                            nb. will be ignored if adf37 defines a set
;                                            of temperatures.
;               unit_te      I     string    K, eV or red for Kelvin, eV or reduced.
;
; KEYWORDS   :  /help     - show this header
;
; NOTES      : Calls the standalone offline_adas/adas7#3/bin/adf04_om2ups.x
;              executable via the instruction namelist option. It writes
;              a temporary file which is deleted at the end.
;
;              The only output is a new adf04 file.
;
; WRITTEN:
;       Martin O'Mullane
;
;
; MODIFIED:
;       1.1   Martin O'Mullane
;               - First version.
;
; VERSION:
;       1.1   27-03-2013
;
;-
;-----------------------------------------------------------------------------


PRO adf04_om2ups, adf04_in  = adf04_in,   $
                  adf04_out = adf04_out,  $
                  adf37     = adf37,      $
                  a04type   = a04type,    $
                  kappa     = kappa,      $
                  x         = x,          $
                  te        = te,         $
                  unit_te   = unit_te,    $
                  help      = help



; If asked for help

if keyword_set(help) then begin
   doc_library, 'adf04_om2ups'
   return
endif



; Validate inputs

if n_elements(adf04_in)  EQ 0 then  message,'An input (type 1 or 5) adf04 filename is required'
if n_elements(adf04_out) EQ 0 then  message,'An output (type 3 or 4) adf04 filename is required'

if n_elements(te) EQ 0 then message, 'User requested temperatures are missing'
if n_elements(te) GT 50 then message, 'There is a maximum of 50 temperatures allowed'

if n_elements(unit_te) EQ 0 then ifout = 2 else begin
   case strlowcase(strtrim(unit_te,2)) of
      'k'   : ifout = 1
      'ev'  : ifout = 2
      'red' : ifout = 3
      else  : message, 'Unknown temperature units'
   endcase
endelse


if n_elements(kappa) GT 0 then is_kappa = 1 else is_kappa = 0
if n_elements(x) GT 0 then is_dru = 1 else is_dru = 0
if n_elements(adf37) GT 0 then is_numer = 1 else is_numer = 0
if n_elements(a04type) GT 0 then begin
   is_04typ = 1
   if NOT (a04type EQ 3 OR a04type EQ 4) then message, 'adf04 output type must be 3 or 4'
endif else is_04typ = 0



if (is_kappa + is_dru + is_numer) EQ 0 then begin
   dtype   = 0
   iadftyp = 3
endif

if is_kappa EQ 1 then begin
   if is_dru + is_numer GT 0 then message, 'Supply just one distribution method'
   kappa = double(kappa)
   if kappa LT 1.5 then message, 'kappa must be large than 1.5'
endif

if is_dru EQ 1 then begin
   if is_kappa + is_numer GT 0 then message, 'Supply just one distribution method'
   x = double(x)
endif

if is_numer EQ 1 then begin
   if is_dru + is_kappa GT 0 then message, 'Supply just one distribution method'
   if is_04typ EQ 0 then message, 'Suppy type of output adf04 (3 or 4)'
endif


; Write instruction file - assign a random name and delete afterwards

all = [' &instruction', $
       ' adf04_in  = ''' + adf04_in + ''',', $
       ' adf04_out = ''' + adf04_out + ''',', $
       ' numte = ' + string(n_elements(te), format='(i2)') + ',', $
       ' te = ' + string(te, format='(50(e10.4,",", :))'), $
       ' ittype = ' + string(ifout, format='(i1)') + ',' ]

if is_kappa EQ 1 then all = [all, ' kappa = ' + string(kappa), ' &end']
if is_dru EQ 1 then all = [all, ' x = ' + string(x), ' &end']
if is_numer EQ 1 then all = [all, ' adf37 = ''' + adf37 + ''', i04type = ' + string(a04type), ' &end']

rand = strmid(strtrim(string(randomu(seed)), 2), 2, 4)
inst_file = 'a04_om2ups' + rand + '.run'
adas_writefile, file=inst_file, all=all

; Run the code

bindir = getenv('ADASHOME')

cmd = bindir + '/offline_adas/adas7#3/bin/adf04_om2ups.x -f' + inst_file

spawn, cmd, result, errresult

file_delete, inst_file, /allow_nonexistent

END
