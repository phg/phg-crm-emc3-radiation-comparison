;-----------------------------------------------------------------------------
;+
; PROJECT:
;       ADAS
;
; NAME:
;        split_adf07
;
; PURPOSE:
;        Generates metastable resolved splits for adf07 files
;        from H P Summers spreadsheet data. The final file is
;        written following a call to this procedure.
;
; EXPLANATION:
;        The data in the spreadsheet is well specified and contains
;        parameters to generate Burgess-Chidichimo ionisation rates
;        for the various ionisation pathways. These factors are then
;        used to split an unresolved adf07 file into metastables
;
; USE:
;        split_adf07, adf07 = adf07,            $
;                     blocks = blocks,          $
;                     spread_file=spread_file,  $
;                     outfile=outfile
;
; INPUTS:
;       adf07       -  adf07 filename for stage-to-stage ionisation rates.
;       blocks      -  blocks in adf07 file required to be split.
;       spread_file -  Spreadsheet file, deliminated by colons (:) or commas (,)
;                      Export/save as .csv in spreadsheet program.
;
; OPTIONAL INPUTS:
;        None.
;
; OUTPUTS:
;       outfile     -  metastable resolved adf07 output file.
;
;
; OPTIONAL OUTPUTS:
;        None.
;
; KEYWORD PARAMETERS:
;        print_path - prints the spreadsheet pathways to comments. Note this can
;                     be of excessive length.
;
; CALLS:
;        R8FBCH         Calculates Burgess-Chidichimo ionisation rate.
;        wheresubarray  Finds indices of a sub-array in a larger array.
;        GET_METATERM   Extracts the term from a metastable string.
;        SPLIT07_GET_F  Parses formula string.
;
; SIDE EFFECTS:
;
; CATEGORY:
;        ADAS supplementary codes.
;
; WRITTEN:
;       Martin O'Mullane
;
; MODIFIED:
;       1.1     Martin O'Mullane
;                 - First release
;       1.2     Martin O'Mullane
;                 - Check for commas and colons as separators since
;                   Excel cannot write a colon delimited file.
;                 - Last entry in neutral stage was missed.
;                 - Fix error writing single character elements.
;                 - Remove blanks at end of comment lines.
;       1.3     Martin O'Mullane
;                 - Make parsing of formula string more robust.
;
; VERSION:
;       1.1     02-09-2010
;       1.2     03-09-2010
;       1.3     08-03-2012
;
;-
;-------------------------------------------------------------------------------


PRO get_metaterm, str, term

; Gets the LS metastable term info from a string of form config (term)
; eg from "2s2 2p4(3P) 3s1 (4P)" we get 4P

p1   = strpos(str, '(',/reverse_search)
term = strupcase(strmid(str, p1+1,2))

END
;-------------------------------------------------------------------------------



PRO split07_get_f, str, f_number, f_mul, n_f

; Parses the formula string to extract the individual formula
;   f_number : f1, f3 etc.
;   f_mul    : +1, -1, +0.23 etc
;   n_f      : number of terms in formula
;
; Byte codes are   43 +
;                  45 -
;                  42 *
;                 102 f
;               48-57 0-9

; replace the ( by a + to get first one or put a + there if there is only one f.

string = strtrim(str,2)
if strmid(string,0,1) EQ 'f' then string = '+' + string
strput, string, '+', 0

ind = strpos(strlowcase(string), '*ratio')
string = strmid(string, 0, ind)

; position of tokens ('+' and '-') and f

bline  = byte(string)
ind_f  = where(bline EQ 102B, n_f)
ind_pm = where(bline EQ 43B OR bline EQ 45B, n_pm)

; Parse the groups

f_number = intarr(n_f)
f_mul    = fltarr(n_f) + 1.0

for j = 0, n_f-1 do begin

   i1 = ind_pm[j]
   if j EQ n_f-1 then i2 = strlen(string)-1 else i2 = ind_pm[j+1] - 1

   val = bline[i1:i2]

   if val[0] EQ 45B then f_mul[j] = -1.0 * f_mul[j]

   ind_st = where(val EQ 42B, n_star)  ; may be more than 1

   if n_star GT 0 then begin

      ind_m = [0, ind_st]
      for k = 0, n_star-1 do  $
         f_mul[j] = f_mul[j] * float(string(val[ind_m[k]+1:ind_m[k+1]-1]))

      val = val[ind_st[n_star-1]+1:*]

   endif

   ind = where(val GE 48B AND val LE 57B)
   f_number[j] = fix(string(val[ind]))

endfor

END
;-------------------------------------------------------------------------------



PRO read_spread_data, file=file, ss_data=ss_data

; Reads the spreadsheet data - assume it is in a tab separated format
; in a standard unix ASCII file.
;
; Open file and make ASCII_TEMPLATE structure - we need a sample location
; of the tab delimiter. Use the first dataline for convenience.

openr, lun, file, /get_lun

str    = ''
j      = 0
f_skip = -1
while (not eof(lun)) and (f_skip LT 0) do begin
  readf, lun, str
  if strpos(str,'Transition') NE -1 then f_skip = 1
  j = j +1
endwhile

readf, lun, str
readf, lun, str

free_lun, lun

; This is how RSI gets multiple instances of a substring (!) -
; stolen from /usr/local/rsi/idl/lib/ascii_template.pro
; colon is 58B, comma is 44B

if strpos(str, ':') NE -1 then delim = 58B else delim = 44B

bline = [byte(str),32B]
nptr  = where(bline eq delim, cnt)
if cnt EQ 0 then fptr = [-1,n_elements(bline)]$
            else fptr = [-1,nptr,n_elements(bline)]
f_loc = fptr + 1
f_loc = f_loc[0:n_elements(fptr)-2]


f_num   = n_elements(f_loc)
f_typ   = intarr(f_num) + 7   ; make them all strings
f_names = strarr(f_num)
for j=0, f_num-1 do f_names[j] = 'fd' + string(j, format='(i2.2)')
f_grp   = indgen(f_num)

tpl = {  VERSION        : 1.0,              $
         DATASTART      : 0,                $
         DELIMITER      : delim,            $
         MISSINGVALUE   : !values.f_nan,    $
         COMMENTSYMBOL  : '',               $
         FIELDCOUNT     : f_num,            $
         FIELDTYPES     : f_typ,            $
         FIELDNAMES     : f_names,          $
         FIELDLOCATIONS : f_loc,            $
         FIELDGROUPS    : f_grp             }


; Now read the file

data = read_ascii(file, template=tpl)


; Truncate to start with 'Transitions' and end with 'Scale factor' column

for j = 0, n_tags(data)-1 do begin

  ind = where(strpos(data.(j), 'Transition') NE -1, count)
  if count GT 0 then is = j
  ind = where(strpos(data.(j), 'Scale factor') NE -1, count)
  if count GT 0 then ie = j

endfor

cmd = 'ss_data = create_struct('
i = 0
for k = is, ie do begin
  cmd = cmd + '''col' + string(i, format='(i2.2)') + ''', data.(' + $
                        string(k, format='(i2.2)') + '),'
  i = i + 1
endfor

cmd = strmid(cmd, 0, strlen(cmd)-1) + ')'
res = execute(cmd)

; Strip " or ' from all entries

n_col = n_elements(ss_data.col00)
for j = 0, n_tags(ss_data)-1 do begin
   for ic = 0, n_col-1 do begin
      i1 = strpos(ss_data.(j)[ic], '"')
      if i1 NE -1 then ss_data.(j)[ic] = strmid(ss_data.(j)[ic], 1, strlen((ss_data.(j)[ic]))-2)
      i1 = strpos(ss_data.(j)[ic], "'")
      if i1 NE -1 then ss_data.(j)[ic] = strmid(ss_data.(j)[ic], 1, strlen((ss_data.(j)[ic]))-2)
   endfor
endfor

; Remove spaces from 'Transition' column

ss_data.col00 = strcompress(ss_data.col00, /remove_all)

END
;-------------------------------------------------------------------------------



PRO process_spread_data, data=data, esym=esym, stage=stage, te=te, output=output

; Get the indices of valid data

str = strcompress(esym + '+' + string(stage) + '->', /remove_all)
ind = where(strpos(strlowcase(data.col00), str) NE -1, count)
i1  = ind[0]
if stage EQ 0 then begin
   i2 = n_elements(data.col01)-1
endif else begin
   str = strcompress(esym + '+' + string(stage-1) + '->', /remove_all)
   ind = where(strpos(strlowcase(data.col00), str) NE -1, count)
   i2  = ind[0]-1
endelse

index = i1 + indgen(i2-i1+1)

; Format everything as the comment block

ncol     = n_tags(data)
nrow     = i2-i1
comments = strarr(nrow)
pos      = [0, 13, 48, 58, 103, 166, 172 + indgen(ncol-5)*12]

for ir = 0, nrow-1 do begin
   str = string(replicate(32B, max(pos)+12))
   for j = 0, ncol-1 do strput, str, data.(j)[i1+ir], pos[j]
   comments[ir] = 'C   ' + str
endfor
comments = [comments, 'C']


;---------------------------------------
; Get the ionisation stage from column 0
;---------------------------------------

str = strlowcase(data.col00[i1])
p1  = strpos(str,'+')
p2  = strpos(str,'-')
z   = -1
reads, strmid(str, p1, p2-p1), z
z1 = z + 1

; adf07 header ion format: XX+ii (XX=element, ii=ion stage)

str = strtrim(str, 2)
p1  = strpos(str,'+')
p2  = strpos(str,'+',/reverse_search)
p3  = strpos(str,'-')
p4  = strpos(str,'>')

z_str  = '  +'
strput, z_str, strmid(str,0,p1), 0
z_str = z_str + string(strmid(str, p1+1, p3-1),format='(i2)')

z1_str = '  +'
strput, z1_str, strmid(str,p4+1,p2-p4-1), 0
z1_str = z1_str + string(strmid(str, p2+1),format='(i2)')


; Element and isoelectronic sequence of the ionising and ionised ions

elem = strmid(str, 0, p1)
iz0  = xxeiz0(elem)
seq_ionised  = xxesym(iz0-z-1)
seq_ionising = xxesym(iz0-z)
if (iz0-z) EQ 1 then seq_ionised = 'FS'


;------------------------------------
; Get the output data - metastables,
; totals and #'s
;------------------------------------

; first eliminate '' then #

ind = where(data.col01[index] NE '',count)
if count EQ 0 then begin
   message, 'Cannot find metastables'
   goto, EARLYEND
endif
ind = ind + i1

hash_loc = where(data.col01[index] EQ '#',count)
if count EQ 0 then begin
   message, 'Cannot find outputs (#)'
   goto, EARLYEND
endif
hash_loc = hash_loc + i1



; Get adf07 outputs - # in col 1 and total in col 3

total_loc = where(strpos(data.col03[index],'total') NE '-1',count)
if count EQ 0 then begin
   message, 'No total outputs'
   goto, EARLYEND
endif
total_loc = total_loc + i1


; metastable names are the non-# in column 1

mind   = setdifference(ind, hash_loc)
meta   = data.col01[mind]
n_meta = n_elements(meta)


;-------------------------------
; Burgess-Chidichimo (f) factors
;-------------------------------

ind = where(strpos(data.col02[index],'f') NE -1,count)
if count EQ 0 then begin
   message, 'Cannot find f factors'
   goto, EARLYEND
endif
ind   = ind + i1
num_f = n_elements(ind)
f_loc = ind


;-----------------
; Get the formulae
;-----------------

ind       = [total_loc,hash_loc]
i_formula = ind(sort(ind))
formula   = data.col04[i_formula]
n_formula = n_elements(formula)



;-------------------------------------------
; Get the scale factors from the last column
;-------------------------------------------

nt  = n_tags(data)
cmd = 'scale = data.'+(tag_names(data))[nt-1]+'[f_loc]'
r = execute(cmd)

scale = float(scale)


;------------------------------------------------------
; Get the shell values - column 5 to penultimate column
; have energy and occupation
;------------------------------------------------------

nshell = ((nt-1) - 5)/2

eng = fltarr(num_f,nshell)
occ = fltarr(num_f,nshell)

fdum = fltarr(num_f)
i = 0
for j = 0, nshell-1 do begin

  cmd1 = 'tmp = data.'+(tag_names(data))[i+5]+ '[f_loc]'
  r = execute(cmd1)
  ind = where(tmp EQ '', count)
  if count NE 0 then tmp[ind] = '0.0'
  eng[*,j] = float(tmp)

  cmd2 = 'tmp = data.'+(tag_names(data))[i+6]+ '[f_loc]'
  r = execute(cmd2)
  ind = where(tmp EQ '', count)
  if count NE 0 then tmp[ind] = '0.0'
  occ[*,j] = float(tmp)

  i = i + 2

endfor


;---------------------------------------
; Evaluate the factors by calling rbchid
; and put them into fn for later use.
;---------------------------------------

tek    = te * 11605.0
nte    = n_elements(te)
factor = dblarr(num_f,nte)

for j = 0, num_f-1 do begin

  for i = 0, nshell-1 do begin
    factor[j,*] = factor[j,*] + r8fbch(iz=z1, xi=eng[j,i], zeta=occ[j,i], te=tek)
  endfor
  factor[j,*] = factor[j,*] * scale[j]

  cmd = 'f' + strtrim(string(j+1),2) + ' = factor['+string(j)+',*]'
  r   = execute(cmd)

endfor


;-------------------------------------
; And now we can evaluate the formulae
;-------------------------------------

ratio        = 1.0 / f1
output_ratio = fltarr(n_formula,nte)


for j = 0, n_formula-1 do begin

  cmd = 'output_ratio[' + string(j) + ',*] = ' + formula[j]
  r   = execute(cmd)

endfor

;---------------------------------------------------------------------------
; Extract the ionisation potentials
;
; We can get the ionisation potentials from the shell energies. Each formula
; relies on the f-factors. Ignore the fractional and negative contributions 
; and from the remaining f's find the lowest energy. The split07_get_f 
; subroutine parses the formula string to return which f's are in the formula. 
; Then loop over the number of shells and f's to find the minimum value.
;---------------------------------------------------------------------------

ionpt = fltarr(n_formula)

for j = 0, n_formula-1 do begin
  
  split07_get_f, formula[j], f_number, f_mul, n_f
  
  list = 0.0
  for k = 0, n_f-1 do begin
     for i = 0, nshell-1 do begin
       if abs(f_mul[k]) EQ 1.0 then list = [list, eng[f_number[k]-1,i]]
     endfor
  endfor
  list = list[1:*]
  ind = where(list NE 0.0)
  ionpt[j] = min(list[ind])

endfor

;---------------------------------------------------------------------------
; A structure for information
;
; I'll never understand this again as this is mostly voodoo....
; icode amd fcode are the metastables of the ionising and ionised ions. We
; can assume that icode are in sequence so range from 1 to n_met (number of
; metastables deduced from spreadsheet). But we need to determine the ionised
; ones - therefore use the 'metastable' procedure to get the (unique) LS
; terms. Then extract the term from column 3 - it should be the last item in ().
; A call to wheresubarray(list_of_ionised_met,term) should give the answer. Of
; course add 1 to give a sensible value. At the same time we get the config+term
; strings for the comments section.
;---------------------------------------------------------------------------

icode    = intarr(n_formula)
fcode    = intarr(n_formula)
z_desig  = strarr(n_formula)
z1_desig = strarr(n_formula)

metastable, seq=seq_ionising, term=term_ionising, config=config_ionising, /lower
metastable, seq=seq_ionised, term=term_ionised, config=config_ionised, /lower

for j = 0, n_meta-1 do begin

  for i = 0, n_formula-1 do begin
     if i_formula[i] ge total_loc[j] then begin

        icode[i] = j+1
        get_metaterm, meta[j], tm
        ind = wheresubarray(term_ionising,tm)
        z_desig[i] = config_ionising[ind] + ' (' + tm + ')'
        get_metaterm, data.col03[i_formula[i]], tm
        ind = wheresubarray(term_ionised,tm, count)
        fcode[i] = ind + 1
        if count GT 0 then  z1_desig[i] = config_ionised[ind] + ' (' + tm + ')'

     endif
  endfor

endfor


; overwrite the totals with fcode = 0

ind        = wheresubarray(i_formula, total_loc)
fcode[ind] = 0
z1_desig[ind] = 'Total'




;-----------------------------------------------
; Put it all into an output structure
;
; Put element and Te here also for completeness.
;-----------------------------------------------

elem = strupcase(elem)

output = { element   :  elem,                 $
           z_str     :  upcasefirst(z_str),   $
           z1_str    :  upcasefirst(z1_str),  $
           nte       :  nte,                  $
           nblock    :  n_formula,            $
           ionpt     :  ionpt,                $
           icode     :  icode,                $
           fcode     :  fcode,                $
           z_desig   :  z_desig,              $
           z1_desig  :  z1_desig,             $
           te        :  te,                   $
           ratio     :  output_ratio,         $
           comments  :  comments              }


EARLYEND:
END
;-------------------------------------------------------------------------------




PRO split_adf07, adf07       =  adf07,        $
                 blocks      =  blocks,       $
                 spread_file =  spread_file,  $
                 outfile     =  outfile,      $
                 print_path  = print_path


; Get data from spreadsheet

read_spread_data, file=spread_file, ss_data=ss_data

; The data

istart = 1
os     = 'LINE -1'
ib     = 0
notes  = 'LINE -1'
how    = 'LINE -1'

read_adf07, file=adf07, fulldata=a07

for k = 0, n_elements(blocks)-1 do begin

  iblk  = blocks[k] - 1
  te    = reform(a07.te[0:a07.nte[iblk]-1,iblk])
  data  = reform(a07.szd[0:a07.nte[iblk]-1,iblk])
  stage = a07.iz[iblk]
  esym  = strlowcase(a07.esym)

  print, 'stage...', stage
  process_spread_data, data=ss_data, esym=esym, stage=stage, te=te, output=op
  element = op.element

  for j = 0, op.nblock-1 do begin

     ind  = indgen(op.nte)
     nout = op.nte
     szd  = data*op.ratio[j,*]

; correct for negative rates - set these to 1.0e-60
;     and for infinities - remove these temperatures

     szd = szd > 1.0D-60

     r    = where(szd EQ !values.d_infinity, count)
     if count GT 0 then begin
        ind  = where(szd NE !values.d_infinity, count)
        nout = count
     endif

     ionpt = op.ionpt[j]*109737.0
     os = [os,  op.z_str + '/' + op.z1_str +  '/' +                  $
                string(nout,format='(i5)') +                         $
                '/I.P. =      ' + string(ionpt, format='(f13.1)') +  $
                '/ICODE = ' + string(op.icode[j],format='(i2)') +    $
                '/FCODE = ' + string(op.fcode[j],format='(i2)') +    $
                '/ISEL = ' + string(istart+j,format='(i3)') ]
     os  = [os, string(te[ind], format = '(6E10.3,:)')]
     os  = [os, string(szd[ind], format = '(6E10.3,:)')]

     st1 = '                            '
     strput, st1,op.z_desig[j]
     st2 = '                            '
     strput, st2,op.z1_desig[j]

     notes = [notes, 'C   ' + string(ib+1, format='(i3)') + '   ' +         $
                     op.z_str + '  ' + string(op.icode[j],format='(i3)') +  $
                     '     ' + st1 + op.z1_str + '  '+                      $
                     string(op.fcode[j],format='(i3)') +  '     ' +         $
                     strtrim(st2) ]



     ib = ib + 1

  endfor

;   format_comments, op.comments, ion_how
   how = [how, op.comments]

  istart = istart +  op.nblock

endfor

; The comments

os = [os, 'C----------------------------------------------------------------------------']

os = [os, 'C','C','C']

; how we did it

os = [os, 'C  Resolved adf07 :  ' +  $
           adf07,'C','C' ]


; If we want to archice the pathways

if keyword_set(print_path) then os = [os, how[1:*]]


; the level list

os = [os, 'C','C','C']
os = [os, 'C   ISEL  INITIAL MET.    DESIG.                     FINAL  MET.    DESIG.']
os = [os, 'C           ION   CODE                                ION   CODE']
os = [os, 'C   ----  ------- ---- ---------------              ------- ---- ---------------']

os = [os, notes[1:*]]

os = [os, 'C','C','C']

; who takes the blame

me  = xxuser()
now = xxdate()

os = [os, 'C  Code   : split_adf07.pro']
os = [os, 'C  Author : ' + me]
os = [os, 'C  Date   : ' + now[0]]
os = [os, 'C']

os = [os, 'C----------------------------------------------------------------------------']


; Now send this stuff to the output file

line1 = string(ib,format='(i5)') +  '    /    IONISATION RATE COEFFICIENTS   /'
strput, line1, element, 10
os[0] = line1

adas_writefile, file=outfile, all=os

END
