;-----------------------------------------------------------------------------
;+
; PROJECT    : ADAS
;
; NAME       : merge_partial_adf11
;
; PURPOSE    : Merges a set of partial plt coefficients from adas810 into
;              a single iso-nuclear adf11 file.
;
; ARGUMENTS  :  All output arguments will be defined appropriately.
;
;               NAME             I/O    TYPE    DETAILS
; REQUIRED   :  partial_files     I     string  Array of partial plt files
;               outfile           I     string  Merged adf11 file
;               project           I     string  Name of project for top line upper right.
;
; OPTIONAL      comments          I     string  array holding comments.
;
; KEYWORDS   :  /help     - show this header
;
; NOTES      :  - Limited check on whether the partial plt files are
;                 compatible for Z0 and Te/dens arrays.
;
;
; WRITTEN:
;       Martin O'Mullane
;
;
; MODIFIED:
;       1.1     Martin O'Mullane
;                 - First version.
;
; VERSION:
;       1.1   17-02-2017
;
;-
;-----------------------------------------------------------------------------
PRO merge_partial_adf11, partial_files = partial_files,  $
                         outfile       = outfile,        $
                         project       = project,        $
                         comments      = comments,       $
                         help          = help

; If asked for help

if keyword_set(help) then begin
   doc_library, 'merge_partial_adf11'
   return
endif

n_stage = n_elements(partial_files)

; Setup the size of output arrays from the first partial file

read_adf11, file=partial_files[0], fulldata=p0

itval = n_elements(p0.dtev)
idval = n_elements(p0.ddens)
iz0   = p0.iz0
ddens = p0.ddens
dtev  = p0.dtev

drcof  = dblarr(p0.iz0, itval, idval)
isstgr = lonarr(p0.iz0)

if n_stage NE p0.iz0 then message, 'Not all stages present for iso-nuclear set', /continue

if p0.is1min NE p0.is1max then message, 'Not a partial plt file : ' + partial_files[0]

; Read the files in turn and populate drcof

is1min = 999
is1max = -1

for j = 0, n_stage-1 do begin

   read_adf11, file=partial_files[j], fulldata=res

   ; check consistency between files

   if res.class NE 'plt' then message, 'Not a PLT file : ' + partial_files[j]
   if res.iz0 NE iz0 then message, 'Not all partial files have the same Z0'
   if res.idmax NE idval then message, 'Not all partial files have the same number of densities'
   if res.itmax NE itval then message, 'Not all partial files have the same number of temperatures'
   if total(ddens-res.ddens) NE 0 then message, 'Not all partial files have the same densities'
   if total(dtev-res.dtev) NE 0 then message, 'Not all partial files have the same temperatures'

   is1min = min([is1min, res.is1min])
   is1max = max([is1max, res.is1max])

   is1 = res.is1min - 1
   drcof[is1, *, *] = res.drcof

   isstgr[is1] = is1+1

endfor

; Assemble fulldata structure for writing output file

if n_stage NE p0.iz0 then begin
   drcof  = drcof[is1min-1:is1max-1, *, *]
   isstgr = isstgr[is1min-1:is1max-1]
endif

fulldata = { iz0    : iz0,     $
             class  : 'plt',   $
             idmax  : idval,   $
             itmax  : itval,   $
             is1min : is1min,  $
             is1max : is1max,  $
             ismax  : n_stage, $
             lres   : 0,       $
             ddens  : ddens,   $
             dtev   : dtev,    $
             isstgr : isstgr,  $
             drcof  : drcof    }

if n_elements(comments) EQ 0 then comments=''

write_adf11, outfile=outfile, project=project, fulldata=fulldata, comments=comments

END
