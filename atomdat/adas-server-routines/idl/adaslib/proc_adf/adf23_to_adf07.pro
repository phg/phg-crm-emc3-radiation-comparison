;-----------------------------------------------------------------------------
;+
; PROJECT    : ADAS
;
; NAME       : adf23_to_adf07
;
; PURPOSE    : Take a set of adf23 datasets and generate one adf07 file.
;
; ARGUMENTS  :  All output arguments will be defined appropriately.
;
;               NAME        I/O    TYPE      DETAILS
; REQUIRED   :  adf23_files I      string()  array of adf23 files
;               adf07       I      string    name of output adf07 file
;
; KEYWORDS   :  /help     - show this header
;               /progress - print name of each adf23 file as it is read and
;                           processed.
;
; NOTES      :  - Each adf07 will have Z0 (atomic number) of enteries. If an
;                 adf23 file is missing for any stage the adf07 entry will be
;                 set to zeros.
;
; WRITTEN:
;       Martin O'Mullane
;
;
; MODIFIED:
;       1.1   Martin O'Mullane
;               - First version.
;
; VERSION:
;       1.1   11-07-2010
;
;-
;-----------------------------------------------------------------------------


PRO adf23_to_adf07, adf23_files = adf23_files, $
                    adf07       = adf07,       $
                    progress    = progress,    $
                    help        = help



; If asked for help

if keyword_set(help) then begin
   doc_library, 'adf23_to_adf07'
   return
endif


nfiles   = n_elements(adf23_files)


; Setup a fulldata structure - get info from first adf23 file

read_adf23, file=adf23_files[0], fulldata=all

nblock   = all.iz0
a23_sort = strarr(nblock)
sym      = xxesym(all.iz0)
n_temp   = all.nte_ion[0]
cicode   = strarr(nblock) + '1'
cfcode   = strarr(nblock) + '1'
ciion    = strarr(nblock)
cfion    = strarr(nblock)
bwno     = fltarr(nblock)
nte      = intarr(nblock) + n_temp
iz       = intarr(nblock)
iz1      = intarr(nblock)
te       = fltarr(n_temp, nblock)
szd      = fltarr(n_temp, nblock)

iz0_test = all.iz0

fulldata = {iz0    : all.iz0,  $
            esym   : sym,      $
            nblock : nblock,   $
            iz     : iz,       $
            iz1    : iz1,      $
            cicode : cicode,   $
            cfcode : cfcode,   $
            ciion  : ciion,    $
            cfion  : cfion,    $
            bwno   : bwno,     $
            nte    : nte,      $
            te     : te,       $
            szd    : szd       }

for j = 0, nfiles-1 do begin

   adf23 = adf23_files[j]
   if keyword_set(progress) then print, 'Processing.... ' + adf23

   read_adf23, file=adf23, fulldata=all, szd_total=szd

   if all.iz0 NE iz0_test then message, 'Input files have different elements'

   te_23  = reform(szd.te) / 11605.0
   szd_23 = reform(szd.qtot*10.0^szd.is_qtot) > 1.0e-36

   ind = all.iz

   a23_sort[ind]       = adf23
   fulldata.te[*,ind]  = te_23
   fulldata.szd[*,ind] = szd_23
   fulldata.bwno[ind]  = all.bwno_i
   fulldata.iz[ind]    = all.iz
   fulldata.iz1[ind]   = all.iz1

   str = '  +  '
   strput, str, sym
   strput, str, string(all.iz, format='(i2)'), 3
   fulldata.ciion[ind]   = str

   str = '  +  '
   strput, str, sym
   strput, str, string(all.iz1, format='(i2)'), 3
   fulldata.cfion[ind]   = str

endfor


; Construct some sensible commnets

line = 'C' + string(replicate(45B, 79))
comments = [line, 'C', 'C  Electron impact ionisation rates from CADW', 'C', $
            'C   ISEL  INITIAL   FINAL    SOURCE', $
            'C   ----  -------   -----    ------']
for j = 0, nblock-1 do begin
   ind = fulldata.iz[j]
   str = 'C' + string(j+1, format='(i6)') + '   ' + fulldata.ciion[ind] + $
         '     ' + fulldata.cfion[ind] + '    ' + $
         a23_sort[ind]

   comments = [comments, str]
endfor

date = xxdate()
name = xxuser()

comments = [comments, 'C', $
            'C',$
            'C  Code     : adf23_to_adf07.pro',$
            'C  Producer : ' + name[0],$
            'C  Date     : ' + date[0],$
            'C',line]


; Write the adf07 file

write_adf07, outfile=adf07, fulldata=fulldata, comments=comments


END

