;----------------------------------------------------------------------
;+
; PROJECT    :  ADAS
;
; NAME       :  trim_cophps_adf04
;
; PURPOSE    :  Reduces the number of levels (and transitions) of an adf04
;               file to those below 105% of the ionisation potential,
;               changes the Eissner configuration to standard and
;               re-orders the transition list.
;
;               Designed to be used with the adf04/cophps#<el> baseline data
;               but can be applied to any adf04 file.
;
;               NAME        I/O    TYPE   DETAILS
; REQUIRED   :  infile       I     str    full name of ADAS adf04 file
;               outfile      I     str    name of output adf04 file
; OPTIONAL      ip_threshold I     float  Multipler (default=1.05) on the ionisation
;                                         potential to determine highest energy level
;                                         retained.
;
; KEYWORDS      keep_eissner        -     do not convert configurations to standard
;                                         notations (default is off)
;               filter              -     pass the resulting adf04 file through the
;                                         ADAS filter04.x program before writing outfile
;                                         default is not to call it.
;               help                -     displays help entry
;
;
; NOTES      :  Relies on read_adf04.pro and xxdata_04.pro which call a compiled
;               fortran routine.
;
; AUTHOR     :  Martin O'Mullane
; DATE       :  16-11-2012
;
; MODIFIED:
;       1.1     Martin O'Mullane
;                 - First version.
;       1.2     Martin O'Mullane
;                 - Add adf04 type to structure when calling write_adf04.
;       1.3     Martin O'Mullane
;                 - Pass through orbital energies to write_adf04.
;
; VERSION:
;       1.1    16-11-2012
;       1.2    12-11-2016
;       1.3    30-08-2017
;-
;----------------------------------------------------------------------


PRO trim_cophps_adf04, infile       = infile,       $
                       outfile      = outfile,      $
                       keep_eissner = keep_eissner, $
                       ip_threshold = ip_threshold, $
                       filter       = filter,       $
                       help         = help

; If asked for help

if keyword_set(help) then begin
   doc_library, 'trim_cophps_adf04'
   return
endif


; Defaults

if keyword_set(keep_eissner) then iconv = 0 else iconv = 1
if n_elements(ip_threshold) GT 0 then ip_mul = ip_threshold else ip_mul = 1.05
if keyword_set(filter) then ifilter = 1 else ifilter = 0

; File name

if n_elements(infile) eq 0 then message, 'An input file name must be passed'
file_acc, infile, exist, read, write, execute, filetype
if exist ne 1 then message, 'Input adf04 file does not exist ' + infile
if read ne 1 then message, 'Input adf04 file cannot be read from this userid ' + infile


; Read in the large adf04 file

read_adf04, file=infile, fulldata=all

; Convert configurations to standard if required

if iconv EQ 1 then begin

   n_cfg   = n_elements(all.cstrga)
   cfg_new = strarr(n_cfg)

   for j = 0, n_cfg-1 do begin
      xxcftr, in_cfg=all.cstrga[j], out_cfg=str, type=3
      cfg_new[j] = str
   endfor

endif else cfg_new = all.cstrga


; Limit the set of energy levels

e_thresh = ip_mul * all.bwno

ind_l    = where(all.wa LE e_thresh, n_lev)
if n_lev EQ 0 then message, 'No energy levels below threshold'

lev_max  = max(ind_l) + 1
ind_t    = where(all.upper LE lev_max, n_tran)
if n_tran EQ 0 then message, 'No suitable transitions found'


; Restrict the output set of levels
;  - assume original energies are in order

fulldata = {iz        : all.iz,               $
            iz0       : all.iz0,              $
            ia        : all.ia[ind_l],        $
            cstrga    : cfg_new[ind_l],       $
            isa       : all.isa[ind_l],       $
            ila       : all.ila[ind_l],       $
            xja       : all.xja[ind_l],       $
            wa        : all.wa[ind_l],        $
            bwnoa     : all.bwnoa,            $
            cprta     : all.cprta,            $
            zpla      : all.zpla,             $
            ipla      : all.ipla,             $
            te        : all.te,               $
            eorb      : all.eorb,             $
            beth      : all.beth[ind_t],      $
            lower     : all.lower[ind_t],     $
            upper     : all.upper[ind_t],     $
            aval      : all.aval[ind_t],      $
            gamma     : all.gamma[ind_t, *],  $
            level_rec : -1,                   $
            level_cx  : -1,                   $
            level_ion : -1,                   $
            iadftyp   : all.iadftyp           }

; Write output file

write_adf04, outfile=outfile, fulldata=fulldata, filter=ifilter

END
