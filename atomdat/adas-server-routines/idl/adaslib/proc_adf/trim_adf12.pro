;----------------------------------------------------------------------
;+
; PROJECT    :  ADAS
;
; NAME       :  trim_adf12
;
; PURPOSE    :  Reads an adf12 (QEF) file and writes a new version
;               limited to the most prominent lines or those from a
;               list supplied by the user.
;
; ARGUMENTS  :  All output arguments will be defined appropriately.
;
;               NAME      I/O    TYPE   DETAILS
; REQUIRED   :  adf12      I     str    full name of ADAS adf12 file
;               outfile    I     str    name of new adf12 file
;               n_lines    I     int    number of blocks required
;                                       ranked according to max qef.
;                                       default is 10 if not set.
;               list       I     str()  array of n-n' values,
;                                       eg, ['4-3', '8-7']
;
; OPTIONAL      use n_lines or list. If both given favour list.
;
; KEYWORDS   :  help              -     displays help entry
;
;
; NOTES      :  The rank is determined from the coefficients at
;               the reference conditions.
;
;
; AUTHOR     :  Martin O'Mullane
;
; DATE       :  21-11-2008
;
; MODIFIED   :
;
;         1.1   Martin O'Mullane
;                - First version.
;         1.2   Martin O'Mullane
;                - Make n_line 10 by default if not set by user.
;
; VERSION    :
;
;         1.1    21-11-2008
;         1.2    17-12-2008
;-
;----------------------------------------------------------------------


PRO trim_adf12, adf12   = adf12,   $
                outfile = outfile, $
                n_lines = n_lines, $
                list    = list,    $
                help    = help

; If asked for help

if keyword_set(help) then begin
   doc_library, 'trim_adf12'
   return
endif

; Test the inputs

if n_elements(adf12) eq 0 then message, 'An adf12 file name must be passed'
file_acc, adf12, exist, read, write, execute, filetype
if exist ne 1 then message, 'adf12 file does not exist '+ file
if read ne 1 then message, 'adf12 file cannot be read from this userid '+ file

if n_elements(n_lines) EQ 0 then begin
   n_lines = 10
   message, 'Considering top 10 lines', /continue
endif

; Get all the data

read_adf12, file=adf12, fulldata=all

; Make sure n_lines is not larger than number of blocks

if n_lines GT all.nbsel then begin
   n_lines = all.nbsel
   message, 'Setting number of lines in trimmed dataset to number in source dataset', /continue
endif


; Take top n_lines rates

rank = reverse(sort(all.qefref))
ind  = rank[0:n_lines-1]


; Or extract those in the list

if n_elements(list) GT 0 then begin

   list_12 = strcompress(all.ctrans, /remove_all)
   ind = -1
   for j = 0, n_elements(list)-1 do begin

      str = strcompress(list[j], /remove_all)
      i1  = where(list_12 EQ str, cnt)
      if cnt GT 0 then ind = [ind, i1[0]]

   endfor
   n_lines = n_elements(ind) - 1
   if n_lines GT 0 then ind = ind[1:*]

endif

; Message to screen

if n_lines GT 0 then begin
   for j = 0, n_lines-1 do print, all.cwavel[ind[j]], all.ctrans[ind[j]], all.qefref[ind[j]]
endif

; Trim to n_lines - start rank at 1 rather than 0

all_12        = all
all_12.nbsel  = n_lines
all_12.csymb  = all.csymb[ind]
all_12.czion  = all.czion[ind]
all_12.cwavel = all.cwavel[ind]
all_12.cdonor = all.cdonor[ind]
all_12.crecvr = all.crecvr[ind]
all_12.ctrans = all.ctrans[ind]
all_12.cfile  = all.cfile[ind]
all_12.ctype  = all.ctype[ind]
all_12.cindm  = all.cindm[ind]
all_12.qefref = all.qefref[ind]
all_12.enref  = all.enref[ind]
all_12.teref  = all.teref[ind]
all_12.deref  = all.deref[ind]
all_12.zeref  = all.zeref[ind]
all_12.bmref  = all.bmref[ind]
all_12.nenera = all.nenera[ind]
all_12.ntempa = all.ntempa[ind]
all_12.ndensa = all.ndensa[ind]
all_12.nzeffa = all.nzeffa[ind]
all_12.nbmaga = all.nbmaga[ind]
all_12.enera  = all.enera[*,ind]
all_12.tempa  = all.tempa[*,ind]
all_12.densa  = all.densa[*,ind]
all_12.zeffa  = all.zeffa[*,ind]
all_12.bmaga  = all.bmaga[*,ind]
all_12.qenera = all.qenera[*,ind]
all_12.qtempa = all.qtempa[*,ind]
all_12.qdensa = all.qdensa[*,ind]
all_12.qzeffa = all.qzeffa[*,ind]
all_12.qbmaga = all.qbmaga[*,ind]

comments = ['C-------------------------------------------------------------------------------', $
            'C                                                                               ', $
            'C  Effective coefficient list:                                                  ', $
            'C                                                                               ', $
            'C     isel    type      ion       transition   wavln.(A)   emis. rank           ', $
            'C     ----    ----      ---       ----------   ---------   ----------           ']

for j = 0, n_lines-1 do begin

   cs = string(j+1, format='(i3)')
   ct = strmid(all_12.ctype[j], 0, 4)
   cz = strmid(all_12.crecvr[j], 0, 5)
   cn = strmid(all_12.ctrans[j], 0, 7)
   cw = string(float(all_12.cwavel[j]), format='(f8.2)')
   cr = string(j+1,  format='(i4)')

   str = 'C     ' + cs + '.    ' + ct + '      ' + cz + '      n = ' + cn + '  ' + cw + '      ' + cr

   comments = [comments, str]

endfor

today = (xxdate())[0]
user  = xxuser()

comments = [comments, $
            'C', $
            'C  Producer : ' + user, $
            'C  Date     : ' + today, $
            'C', $
            'C-------------------------------------------------------------------------------']


write_adf12, outfile=outfile, fulldata=all_12, comments=comments


END
