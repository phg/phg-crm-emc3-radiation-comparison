;-----------------------------------------------------------------------------
;+
; PROJECT    : ADAS
;
; NAME       : bundle_adf04
;
; PURPOSE    : Bundles levels in an adf04 file according to a input map.
;
; ARGUMENTS  :  All output arguments will be defined appropriately.
;
;               NAME      I/O    TYPE    DETAILS
; REQUIRED   :  adf04      I     string  input adf04 
;               map        I     int     map of bundle
;               fulldata   O     stuct   adf04 fulldata structure of bundled
;
; KEYWORDS   :  /help     - show this header
;
; NOTES      :  - Output drops all S, R and H lines.
;               - The map vector entries are the indices of the bundled
;                 levels. Count from 1 not 0.
;
;
; WRITTEN:
;       Martin O'Mullane
;
;
; MODIFIED:
;       1.1     Martin O'Mullane
;                 - First version.
;       1.2     Martin O'Mullane
;                 - The energy of bundled levles with xja of 0.0 were not
;                   calculated correctly.
;       1.3     Martin O'Mullane
;                 - The ipla array requires a leading dimension of 1.
;       1.4     Martin O'Mullane
;                 - Special case handling for NIST energy-level only files.
;       1.5     Matthew Bluteau
;                 - Term re-normalisation corrected: the ground term energy
;                   needs to be subtracted from all term energies, but e_bnd[0]
;                   was previously overwritten (as zero).
;       1.6     Martin O'Mullane
;                 - Add iadftyp to the returned fulldata structure.
;
; VERSION:
;       1.1   08-08-2010
;       1.2   18-11-2010
;       1.3   21-03-2011
;       1.4   03-10-2016
;       1.5   10-05-2017
;       1.6   22-03-2018
;
;-
;-----------------------------------------------------------------------------
PRO bundle_adf04, adf04    =  adf04,      $
                  map      =  map,        $ 
                  fulldata =  fulldata,   $
                  help     = help


; If asked for help

if keyword_set(help) then begin
   doc_library, 'bundle_adf04'
   return
endif


; Test input

if NOT arg_present(fulldata) then message, 'Supply an output structure'


; Read the unbundled adf04 file

read_adf04, file=adf04, fulldata=all


; Bundle level data according to map

ind_min = min(map, max=ind_max)

n_bnd = ind_max - ind_min + 1

e_bnd = dblarr(n_bnd)
w_bnd = fltarr(n_bnd)  
c_bnd = strarr(n_bnd)
s_bnd = intarr(n_bnd)
l_bnd = intarr(n_bnd)

for j = ind_min, ind_max do begin

   ind = where(map EQ j, count)
   
   if count GT 0 then begin
   
      eng = all.wa[ind]
      xja = all.xja[ind]
      
      w_bnd[j-1] = (total(2.0*xja+1.0) - 1.0) / 2.0
      
      wt = (2.0 * xja + 1.0) / 2.0
      
      if total(xja) EQ 0.0 then e_bnd[j-1] = total(eng)  $
                           else e_bnd[j-1] = total(eng*wt) / total(wt)
      
      c_bnd[j-1] = all.cstrga[ind[0]]
      s_bnd[j-1] = all.isa[ind[0]]
      l_bnd[j-1] = all.ila[ind[0]]
      
   endif

endfor

; Re-normalise new ground energy level

e_grnd = e_bnd[0]
for j = 0, n_bnd-1 do e_bnd[j] = e_bnd[j] - e_grnd


; Bundle transition data - only excitation here - NIST energy level only 
;                                                 file are a special case.

if all.upper[0] NE -1 then begin

   n_tran = n_elements(all.upper)
   n_map  = n_elements(map)

   new_up  = intarr(n_tran)
   new_low = intarr(n_tran)


   for j = 0, n_tran-1 do begin

      new_up[j]  = map[all.upper[j]-1]
      new_low[j] = map[all.lower[j]-1]

   endfor

   index = 10000L*new_up + new_low

   itag      = sort(index)
   new_index = index[itag]
   last_elem = uniq(new_index)

   n_tran_b = n_elements(last_elem)
   n_te     = n_elements(all.te)
   aval_b   = fltarr(n_tran_b)
   gamma_b  = fltarr(n_tran_b, n_te)
   up_b     = intarr(n_tran_b)
   low_b    = intarr(n_tran_b)

   for j = 0, n_tran_b-1 do begin

      if j EQ 0 then i1 = 0 else i1 = last_elem[j-1]+1
      i2 = last_elem[j]

      i_up  = new_index[i2] / 10000L
      i_low = new_index[i2] - (i_up * 10000L)
      ind_b = itag[i1:i2]

      up_b[j]  = i_up
      low_b[j] = i_low

      gamma_b[j, *] = total(all.gamma[ind_b,*], 1)

      w_tmp     = (2.0 * all.xja[all.upper[ind_b]-1] + 1.0) / 2.0
      a_tmp     = all.aval[ind_b] 
      tmp       = total(a_tmp * w_tmp) / ((2.0 * w_bnd[i_up-1] + 1.0) / 2.0)
      aval_b[j] = tmp             

   endfor


   ; Combine repeated transitions - split over energies 
   ; Set unused indices to -1 and -1

   index = 10000L*up_b + low_b

   for j = 0, n_tran_b-1 do begin

      icmp = 10000L*up_b[j] + low_b[j]
      ind1 = where(index EQ icmp,c1)
      icmp = 10000L*low_b[j] + up_b[j]
      ind2 = where(index EQ icmp,c2)

      if c1+c2 GT 1 then begin

         ind = [ind1, ind2]
         up_b[ind[1:*]]  = -1
         low_b[ind[1:*]] = -1

         aval_b[ind[0]] = 0.0
         gamma_b[ind[0], *] = total(gamma_b[ind,*], 1)

      endif

   endfor



   ; Omit repeated transitions - intra-term transitions (and split ones from above)

   ind_k = -1
   for j = 0, n_tran_b-1 do if up_b[j] NE low_b[j] then ind_k = [ind_k, j]
   ind_k = ind_k[1:*]

   up_b    = up_b[ind_k]
   low_b   = low_b[ind_k]  
   gamma_b = gamma_b[ind_k,*]
   aval_b  = aval_b[ind_k] 



   ; Assemble the output structure

   fulldata = { filename    :  'bundle.pass',         $  ; default
                iz          :  all.iz,                $
                iz0         :  all.iz0,               $
                ia          :  all.ia,                $
                cstrga      :  c_bnd,                 $
                isa         :  s_bnd,                 $
                ila         :  l_bnd,                 $
                xja         :  w_bnd,                 $
                wa          :  e_bnd,                 $
                bwno        :  all.bwno,              $
                bwnoa       :  all.bwno,              $
                cprta       :  strarr(n_bnd) + '',    $
                zpla        :  fltarr(1,n_bnd) + 0.0, $
                ipla        :  intarr(1,n_bnd) + 0,   $
                te          :  all.te,                $
                lower       :  low_b,                 $
                upper       :  up_b,                  $
                aval        :  aval_b,                $
                gamma       :  gamma_b,               $
                level_rec   :  -1,                    $
                level_cx    :  -1,                    $
                level_ion   :  -1,                    $
                iadftyp     :  all.iadftyp            }

endif else begin

   fulldata = { filename    :  'bundle.pass',         $  ; default
                iz          :  all.iz,                $
                iz0         :  all.iz0,               $
                ia          :  all.ia,                $
                cstrga      :  c_bnd,                 $
                isa         :  s_bnd,                 $
                ila         :  l_bnd,                 $
                xja         :  w_bnd,                 $
                wa          :  e_bnd,                 $
                bwno        :  all.bwno,              $
                bwnoa       :  all.bwno,              $
                cprta       :  strarr(n_bnd) + '',    $
                zpla        :  fltarr(1,n_bnd) + 0.0, $
                ipla        :  intarr(1,n_bnd) + 0,   $
                te          :  -1,                    $
                lower       :  -1,                    $
                upper       :  -1,                    $
                aval        :  -1,                    $
                gamma       :  -1,                    $
                level_rec   :  -1,                    $
                level_cx    :  -1,                    $
                level_ion   :  -1,                    $
                iadftyp     :  all.iadftyp            }

endelse       

END
