;-----------------------------------------------------------------------------
;+
; PROJECT    : ADAS
;
; NAME       : merge_adf04
;
; PURPOSE    : merges two adf04 datasets.
;
; ARGUMENTS  :  All output arguments will be defined appropriately.
;
;               NAME      I/O    TYPE    DETAILS
; REQUIRED   :  target     I     string  target adf04
;               supp       I     string  supplementary adf04
;               map        I     int     map of supp into target
;               outfile    I     string  merged adf04
;
; KEYWORDS   :  /help     - show this header
;               /match_te - interpolate supplement gamma onto Te grid of
;                           target file
;
; NOTES      :  - Output drops all S, R and H lines.
;               - Interpolation is simple and should be replaced by adas215
;                 style method.
;               - The map vector entries are the indices of the supplementary
;                 level in the target file. Count from 1 not 0.
;               - If there are levels in the supplementary file not in the
;                 target give them numbers starting at last target level +1.
;
;
; WRITTEN:
;       Martin O'Mullane
;
;
; MODIFIED:
;       1.1     Martin O'Mullane
;                 - First version.
;       1.2     Martin O'Mullane
;                 - Add a check for compatible quantum numbers.
;
; VERSION:
;       1.1   23-10-2009
;       1.2   08-08-2010
;
;-
;-----------------------------------------------------------------------------
PRO merge_adf04, target   = target,   $
                 supp     = supp,     $
                 map      = map,      $
                 outfile  = outfile,  $
                 match_te = match_te, $
                 help     = help

; If asked for help

if keyword_set(help) then begin
   doc_library, 'merge_adf04'
   return
endif


; Test that map enteries are unique

if n_elements(uniq(map)) NE n_elements(map) then message, 'Error in map'


; File names

if n_elements(target) eq 0 then message, 'A target filename must be given'
file_acc, target, exist, read, write, execute, filetype
if exist ne 1 then message, 'Target file does not exist ' + target
if read ne 1 then message, 'Target file cannot be read from this userid ' + target

if n_elements(supp) eq 0 then message, 'A supplementary filename must be given'
file_acc, supp, exist, read, write, execute, filetype
if exist ne 1 then message, 'Supplementary file does not exist ' + supp
if read ne 1 then message, 'Supplementary file cannot be read from this userid ' + supp


; Document what we do

dashes = 'C' + string(replicate(45B, 79))
date   = xxdate()
user   = xxuser()
date   = date[0]


comments = [dashes, 'C', 'C Combined two specific ion files', 'C', $
            'C Target        : ' + target, $
            'C Supplementary : ' + supp, $
            'C', $
            'C Replace SUPPLEMENTARY level into TARGET level', $
            'C']
for j = 0, n_elements(map)-1 do $
   comments = [comments, 'C    ' + string([j+1, map[j]], format='(2i16)')]

comments = [comments, 'C', 'C', $
            'C CODE     : ADAS utility program, merge_adf04.pro', $
            'C DATE     : ' + date, $
            'C PRODUCER : ' + user, $
            'C', $
            dashes]

aval_cmt = ['C', $
            'C   For the following transitions supplement A values have been used.', $
            'C   Check the supplement values!', 'C', $
            'C     Tran.      Target       Supp', 'C']



; Read in adf04 files and comments

read_adf04, file=target, fulldata=all_t
read_adf04, file=supp, fulldata=all_s

xxcomments, file=target, comments=cmt_t
xxcomments, file=supp, comments=cmt_s


; Levels not in target list

ia_t_max = max(all_t.ia)

; Step through supplement levels replacing target data

cstrga = all_t.cstrga
wa     = all_t.wa
ia     = all_t.ia
isa    = all_t.isa
ila    = all_t.ila
xja    = all_t.xja
ipla   = all_t.ipla
zpla   = all_t.zpla

for j = 0, n_elements(all_s.ia)-1 do begin

   i1 = where(map[j] EQ all_t.ia, count)
   if count GT 0 then begin

      ; check if quantum numbers match
      
      mess = 'Target : ' + cstrga[i1] + ' and supplementary : ' + all_s.cstrga[j] + ' have different '
      
      if isa[i1] NE all_s.isa[j] then message, mess + 'spins'
      if ila[i1] NE all_s.ila[j] then message, mess + 'angular momenta'
      if xja[i1] NE all_s.xja[j] then message, mess + 'statistical weights'

      cstrga[i1]  = all_s.cstrga[j]
      wa[i1]      = all_s.wa[j]
      
   endif else begin

      if map[j] GT ia_t_max then begin

         cstrga = [cstrga, all_s.cstrga[j]]
         wa     = [wa, all_s.wa[j]]
         ia     = [ia, map[j]]
         isa    = [isa, all_s.isa[j]]
         ila    = [ila, all_s.ila[j]]
         xja    = [xja, all_s.xja[j]]
         ipla   = [[ipla], [all_s.ipla[j]]]
         zpla   = [[zpla], [all_s.zpla[j]]]

      endif

   endelse

endfor


; Tag upper and lower tranition labels in the new mapping

lower  = all_t.lower
upper  = all_t.upper

for j = 0, n_elements(upper)-1 do begin

  il = where(map EQ lower[j], cl)
  iu = where(map EQ upper[j], cu)

  if cl GT 0 then lower[j] = il + 1
  if cu GT 0 then upper[j] = iu + 1

endfor
itag   = upper * 1000 + lower


; Step through supplement transitions replacing target data

aval   = all_t.aval
gamma  = all_t.gamma
lower  = all_t.lower
upper  = all_t.upper


; Interpolate Te if requested

if keyword_set(match_te) then begin

   n_te = n_elements(all_t.te)
   te   = all_t.te
   n_gs = n_elements(all_s.upper)

   gamma_s = fltarr(n_gs, n_te)

   for j = 0, n_gs-1 do begin
      res = interpol(all_s.gamma[j,*], all_s.te, te)
      gamma_s[j,*] = reform(res)
   endfor

endif else gamma_s = all_s.gamma


for j = 0, n_elements(all_s.upper)-1 do begin

   ; Catch if mapping has re-ordered upper and lower indices

   index = all_s.lower[j] * 1000 + all_s.upper[j]
   i1    = where(itag EQ index, count)
   i1    = i1[0]

   if count GT 0 then begin

     if all_s.aval[j] GT 1.0e-4 then aval[i1] = all_s.aval[j]
     gamma[i1,*] = gamma_s[j,*]

   endif else begin

      ; Mapping has not changed upper and lower

      index = all_s.upper[j] * 1000 + all_s.lower[j]
      i1    = where(itag EQ index, count)
      i1    = i1[0]

      if count GT 0 then begin

         amin = min([all_s.aval[j], aval[i1]], max=amax)
         if all_s.aval[j] GT 1.0e-4 then begin
            if amax /amin GT 9 then begin
                str =  string([upper[i1], lower[i1]], format='(2i5)') + $
                       string([all_s.aval[j], aval[i1]], format = '(2e12.2)')
                aval_cmt = [aval_cmt, 'C ' + str]
            endif
            aval[i1] = all_s.aval[j]
         endif
         gamma[i1,*] = gamma_s[j,*]

      endif else begin

          aval   = [aval, all_s.aval[j]]
          gamma  = [gamma, gamma_s[j,*]]
          lower  = [lower, map[all_s.lower[j]-1]]
          upper  = [upper, map[all_s.upper[j]-1]]

      endelse

   endelse

endfor



; fulldata for output file

all_o  = { iz        : all_t.iz,      $
           iz0       : all_t.iz0,     $
           ia        : ia,            $
           cstrga    : cstrga,        $
           isa       : isa,           $
           ila       : ila,           $
           xja       : xja,           $
           wa        : wa,            $
           bwnoa     : all_t.bwnoa,   $
           cprta     : all_t.cprta,   $
           zpla      : zpla,          $
           ipla      : ipla,          $
           te        : all_t.te,      $
           lower     : lower,         $
           upper     : upper,         $
           aval      : aval,          $
           gamma     : gamma,         $
           level_rec : -1,            $
           level_cx  : -1,            $
           level_ion : -1             }

if n_elements(aval_cmt) EQ 6 then begin
   comments = [comments, 'C Comments from target file',  cmt_t, $
                         'C Comments from supplementary file', cmt_s]
endif else begin
   comments = [comments, aval_cmt, 'C', dashes, 'C Comments from target file',  cmt_t, $
                         'C Comments from supplementary file', cmt_s]
endelse


write_adf04, outfile=outfile, fulldata=all_o, comments=comments

END
