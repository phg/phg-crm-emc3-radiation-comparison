;----------------------------------------------------------------------
;+
; PROJECT    :  ADAS
;
; NAME       :  adf15_transition
;
; PURPOSE    :  Interrogates the comment section of adas208 generated
;               adf15 files and returns details of the wavelengths and 
;               configurations of the PECs. Indices of the upper and 
;               lower configuration and the transition in the adf04 file
;               are optionally returned.
;
;               NAME        I/O    TYPE     DETAILS
; REQUIRED   :  comments     I     str()    comments from ADAS adf04 file, use
;                                           xxcomments.pro to extract these.
;
; OPTIONAL   :  wave         O     float()  wavelength (A)
;               upper        O     str()    configuration of upper level
;               lower        O     str()                     lower
;               type         O     str()    type PEC, excit., recom. or cx.
;               tr_index     O     int()    index of transition in the adf04 file
;               pr_rank      O     int()    power index
;               up_index     O     int()    index of upper level in adf04 file
;               low_index    O     int()             lower
;
; KEYWORDS      standard            -     converts Eissner to standard form.
;               help                -     displays help entry
;
;
; NOTES      :  It can only reliably parse the output from the latest version
;               of the adf15 files. If there is a partition vector or a power
;               ranking index in the adf15 file it should work.
;
;
; AUTHOR     :  Martin O'Mullane
; DATE       :  31-05-2017
;
; MODIFIED:
;       1.1     Martin O'Mullane
;                 - First version.
;
; VERSION:
;       1.1    31-05-2017
;-
;----------------------------------------------------------------------

PRO adf15_transition, comments  = comments,  $
                      wave      = wave,      $
                      upper     = upper,     $
                      lower     = lower,     $
                      type      = type,      $
                      tr_index  = tr_index,  $
                      pr_rank   = pr_rank,   $
                      up_index  = up_index,  $
                      low_index = low_index, $
                      standard  = standard


if keyword_set(help) then begin
   doc_library, 'adf15_transition'
   return
endif


all = comments

; New style adf15 - transition index and power rank are available

is_new = 0

i0 = where(strpos(strlowcase(all), 'tg pr wr') NE -1, count)

if count GT 0 then begin
   is_new = 1
   ind_sz = strpos(strlowcase(all[i0]), 'sz')
endif

; extract configurations

i0 = where(strpos(strlowcase(all), 'configuration') NE -1, count)
if count EQ 0 then begin
   message, 'No configuration information present', /continue
   wave = 0.0
   upper = 'NULL'
   lower = 'NULL'
   return
endif
all = all[i0[0]+1:*]
if strpos(all[0], '----') NE -1 then all = all[1:*]

j = 0
while num_chk(strmid(all[j], 1, 7)) EQ 0 do j = j+1

if j GT 0 then begin
   n_config = j
endif else begin
   message, 'No configurations found', /continue
   wave = 0.0
   upper = 'NULL'
   lower = 'NULL'
   return
endelse

i1 = strpos(all[0], '1')
i2 = strpos(all[0], ')', /reverse_search)

for j = 0, n_config-1 do config = strmid(all[0:n_config-1], i1+1, i2-i1+1)

; Convert to standard form if required

if keyword_set(standard) then begin

   for j = 0, n_config-1 do begin

      i1 = strpos(config[j], '(')
      str = strcompress(strmid(config[j], 0, i1-1), /remove_all)
      xxdtes, in_cfg=str, is_eissner=is_eissner

      if is_eissner EQ 1 then begin

         xxcftr, in_cfg=str, out_cfg=tmp, type=3

         config[j] = strcompress(tmp) + ' ' + strmid(config[j], i1)

      endif

   endfor

endif else begin

   i1  = strpos(config[0], '(')
   len = max(strlen(strtrim(strmid(config, 0, i1-1),2)))

   for j = 0, n_config-1 do begin

      str = string(replicate(32B, len))
      strput, str, strtrim(strmid(config[j], 0, i1-1),2)

      config[j] = str + ' ' + strmid(config[j], i1)

   endfor


endelse



; Extract transitions

i0 = where(strpos(strlowcase(all), 'transition') NE -1)
all = all[i0[0]+1:*]

i1 = -1
for j = 0, 5 do begin
  res = strpos(all[j], '---')
  if res[0] NE -1 then i1 = j
endfor

if i1 EQ -1 then begin
   message, 'No transitions found', /continue
   wave = 0.0
   upper = 'NULL'
   lower = 'NULL'
   return
endif else all = all[i1+1:*]


j = 0
while num_chk(strmid(all[j], 1, 7)) EQ 0 do j = j+1
n_tran = j


lower     = strarr(n_tran)
upper     = strarr(n_tran)
type      = strarr(n_tran)
wave      = fltarr(n_tran)
tr_index  = intarr(n_tran)
pr_rank   = intarr(n_tran)
up_index  = intarr(n_tran)
low_index = intarr(n_tran)



for j = 0, n_tran-1 do begin

   str = all[j]

   reads, strmid(str,1), isel, wv
   wave[j] = wv

   i1  = strpos(str, '(') - 4
   i2  = strpos(str, '-') + 1
   i3  = strpos(strmid(str,i2), '(')

   lv1 = fix(strmid(str, i1, 4))
   lv2 = fix(strmid(str, i2, i3))

   upper[j] = config[lv1-1]
   lower[j] = config[lv2-1]

   if strpos(strlowcase(str), 'excit') NE -1 then type[j] = 'EXCIT'
   if strpos(strlowcase(str), 'recom') NE -1 then type[j] = 'RECOM'
   if strpos(strlowcase(str), 'chex') NE -1 then type[j] = 'CX'

   if is_new EQ 1 then begin
      reads, strmid(str, ind_sz+2), in_a, in_b, in_c
      tr_index[j] = in_a
      pr_rank[j]  = in_b
   endif

   up_index[j]  = lv1
   low_index[j] = lv2

endfor


END
