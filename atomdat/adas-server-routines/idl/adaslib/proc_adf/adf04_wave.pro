;----------------------------------------------------------------------
;+
; PROJECT    :  ADAS
;
; NAME       :  adf04_wave
;
; PURPOSE    :  Interrogates an adf04 file and returns tables of
;               permitted wavelengths with configuration, energy
;               and quantum numbers, which can be sent to a file,
;               screen (default) or returned as a structure.
;
;
;               NAME        I/O    TYPE   DETAILS
; REQUIRED   :  file         I     str    full name of ADAS adf04 file
;
; OPTIONAL   :  outfile      I     str    name of file for formatted output.
;               wmin         I     float  minimum wavelength (default 0.01A)
;               wmax         I     float  maximum wavelength (default 10000A)
;               amin         I     float  minimum transition probability (default 100.0/s)
;               wavedata     O     struct structure containing tabulated data.
;
; KEYWORDS      sort                -     sorts the output table in
;                                         ascending order.
;               energy              -     substitute energy for wavelength
;               standard            -     converts Eissner to standard form.
;               noscreen            -     do not output results to the screen.
;               oscillator          -     also write oscillator strength to file
;               gaunt               -     also write effective Gaunt factor to file
;               help                -     displays help entry
;
;
; NOTES      :  Relies on read_adf04.pro and xxdata_04.pro which call a compiled
;               fortran routine.
;
;               The default behaviour is to output results to the screen. This
;               can be changed with /noscreen. Output to a file or a structure
;               depend on whether outfile or wavedata are supplied.
;
;               A Gaunt factor is calculated for the closest temperature (in eV)
;               corrsponding to the ionisation potential (in eV). 
;
; AUTHOR     :  Martin O'Mullane
; DATE       :  25-05-2017
;
; MODIFIED:
;       1.1     Martin O'Mullane
;                 - First version.
;       1.2     Martin O'Mullane
;                 - Add Gaunt factor and oscillator strength options.
;
; VERSION:
;       1.1    25-05-2017
;       1.2    30-04-2019
;-
;----------------------------------------------------------------------


PRO adf04_wave, file       = file,       $
                outfile    = outfile,    $
                noscreen   = noscreen,   $
                wmin       = wmin,       $
                wmax       = wmax,       $
                amin       = amin,       $
                sort       = sort,       $
                energy     = energy,     $
                standard   = standard,   $
                oscillator = oscillator, $
                gaunt      = gaunt,      $
                wavedata   = wavedata,   $
                help       = help


if keyword_set(help) then begin
   doc_library, 'adf04_wave'
   return
endif

; file name

if n_elements(file) EQ 0 then message,'A filename must be given'

file_acc, file, exist, read, write, execute, filetype
if exist ne 1 then message, 'adf04 file does not exist '+ file
if read ne 1 then message, 'adf04 file cannot be read from this userid '+ file


; Check and set options

if n_elements(wmin) EQ 0 then wmin = 0.01
if n_elements(wmax) EQ 0 then wmax = 1.0e4
if n_elements(amin) EQ 0 then amin = 1.0e2


; Read in the data - ignoring untied levels is appropriate here

read_adf04, file=file, fulldata=all, /ignore_untied


; convert Eissner to standard if requested

cfg_new = all.cstrga_std
len_s   = max(strlen(strtrim(cfg_new,2))) + 2
cfg_new = strmid(cfg_new, 0, len_s)


; Generate the tabular data

nlmax     = n_elements(all.upper)

outstr    = strarr(nlmax)
wv        = fltarr(nlmax)
wv_e      = fltarr(nlmax)
aval      = fltarr(nlmax)
fval      = fltarr(nlmax)
gval      = fltarr(nlmax)
fval      = fltarr(nlmax)
cfg_lower = strarr(nlmax)
s_lower   = intarr(nlmax)
l_lower   = intarr(nlmax)
j_lower   = fltarr(nlmax)
e_lower   = fltarr(nlmax)
cfg_upper = strarr(nlmax)
s_upper   = intarr(nlmax)
l_upper   = intarr(nlmax)
j_upper   = fltarr(nlmax)
e_upper   = fltarr(nlmax)

; Evaluate Gaunt factor at Te closest to ionization potential in eV

itemp = i4indf(all.te/11605.0, all.bwno/8066.0)
tk    = all.te[itemp]
tev   = tk / 11605.0

k = 0
for j = 0L, nlmax-1 do begin

   i1   = all.upper[j] - 1
   i2   = all.lower[j] - 1
   wave = 1.0e8 / (all.wa[i1] - all.wa[i2])
   eng  = (all.wa[i1] - all.wa[i2]) / 8066.0
   wup  = 2.0 * all.xja[i1] + 1.0
   wlow = 2.0 * all.xja[i2] + 1.0
   de   = all.wa[i1] - all.wa[i2]


   if (wave GT wmin AND wave LE wmax) AND (all.aval[j] GT amin) then begin

      if keyword_set(energy) then val = eng else val = wave

      fik  = (all.aval[j] * wave^2 * wup) / (6.67e15 * wlow)
      rate = 2.17161e-8 * all.gamma[j, itemp] * sqrt(157890/tk) / (wlow * exp(de/tk))
      gik  = (1.6e-19 * eng * rate) * sqrt(tev) / (2.53e-24 * fik *exp(-eng/tev))

      wv[k]   = val
      fval[k] = fik
      gval[k] = gik

      str1 = string(val, format='(f13.3)') + '   |' + $
             string(all.aval[j], format='(e15.3)')   +  '    |'

      if keyword_set(oscillator) then str2 = string(fik, format='(f9.4)')  + '   |' else str2 = ''
      if keyword_set(gaunt) then str3 = string(gik, format='(f10.4)')  + '   |' else str3 = ''

      str4 = string(all.wa[i2], format='(f12.1)') + ' - ' + $
             string(all.wa[i1], format='(f12.1)') +         $
             '    |  ' + $
             cfg_new[i2] + '  (' + string(all.isa[i2], format='(i1)') + ')' + $
             string(all.ila[i2], format='(i1)') + $
             '(' + string(all.xja[i2], format='(f4.1)') + ')' +  ' - ' + $
             cfg_new[i1] + '  (' + string(all.isa[i1], format='(i1)') + ')' + $
             string(all.ila[i1], format='(i1)') + $
             '(' + string(all.xja[i1], format='(f4.1)') + ')'

      outstr[k] = str1 + str2 + str3 + str4

      wv_e[k]      = eng
      aval[k]      = all.aval[j]
      cfg_lower[k] = cfg_new[i2]
      s_lower[k]   = all.isa[i2]
      l_lower[k]   = all.ila[i2]
      j_lower[k]   = all.xja[i2]
      e_lower[k]   = all.wa[i2]
      cfg_upper[k] = cfg_new[i1]
      s_upper[k]   = all.isa[i1]
      l_upper[k]   = all.ila[i1]
      j_upper[k]   = all.xja[i1]
      e_upper[k]   = all.wa[i1]

      k = k + 1

   endif

endfor

if k GT 0 then begin

   number    = k

   wv        = wv[0:number-1]
   wv_e      = wv_e[0:number-1]
   aval      = aval[0:number-1]
   fval      = fval[0:number-1]
   gval      = gval[0:number-1]
   cfg_lower = cfg_lower[0:number-1]
   s_lower   = s_lower[0:number-1]
   l_lower   = l_lower[0:number-1]
   j_lower   = j_lower[0:number-1]
   e_lower   = e_lower[0:number-1]
   cfg_upper = cfg_upper[0:number-1]
   s_upper   = s_upper[0:number-1]
   l_upper   = l_upper[0:number-1]
   j_upper   = j_upper[0:number-1]
   e_upper   = e_upper[0:number-1]

   outstr    = outstr[0:number-1]

endif else begin

   number = 0

endelse


if keyword_set(sort) then ind = sort(wv) else ind = indgen(n_elements(wv))

if keyword_set(energy) then ew_str = '  E_trans (eV)  ' $
                       else ew_str = ' Wavelength (A) '

dashes = '----------------------------------------------------------------' + $
         '-----------------------------------------------------------------'
str5 = ew_str + '|   A-value (sec-1) |'

if keyword_set(oscillator) then begin
   str5   = str5   + '    f_ij    |'
   dashes = dashes + '-------------'
endif

if keyword_set(gaunt) then begin
   str5   = str5   + '   G(eff)    |'
   dashes = dashes + '-------------'
endif

header = [xxesym(all.iz0) + strcompress(string(all.iz), /remove_all) + '+' + $
          '      Ionisation potential : '  +                                 $
          string(all.bwno, format='(f12.1)') + ' cm-1' +                     $
          '    ADAS file : ' + file,                                         $
          ' ',                                                               $
          dashes,                                                            $
          str5 + '         Energies (cm-1)' +                                $
          '       |  Configuration  (2S+1)L(w-1/2)',                         $
          dashes]

if n_elements(outstr) EQ 1 then header = [header, 'No lines found']



if n_elements(outfile) GT 0 then begin

   openw, lun, outfile, /get_lun
   for j = 0, n_elements(header)-1 do printf, lun, header[j]
   for j = 0, n_elements(outstr)-1 do printf, lun, outstr[ind[j]]
   free_lun, lun

endif

if NOT keyword_set(noscreen) then begin

   for j = 0, n_elements(header)-1 do print, header[j]
   for j = 0, n_elements(outstr)-1 do print, outstr[ind[j]]

endif

if arg_present(wavedata) then begin

   if number GT 0 then begin
      wavedata = { number     : number,          $
                   wave       : wv[ind],         $
                   delta_e    : wv_e[ind],       $
                   aval       : aval[ind],       $
                   fval       : fval[ind],       $
                   gval       : gval[ind],       $
                   cfg_lower  : cfg_lower[ind],  $
                   is_lower   : s_lower[ind],    $
                   il_lower   : l_lower[ind],    $
                   xj_lower   : j_lower[ind],    $
                   eng_lower  : e_lower[ind],    $
                   cfg_upper  : cfg_upper[ind],  $
                   is_upper   : s_upper[ind],    $
                   il_upper   : l_upper[ind],    $
                   xj_upper   : j_upper[ind],    $
                   eng_upper  : e_upper[ind],    $
                   header     : header,          $
                   table      : outstr[ind]      }
   endif else begin
      wavedata = { number     : 0,               $
                   header     : header           }
   endelse

endif

END
