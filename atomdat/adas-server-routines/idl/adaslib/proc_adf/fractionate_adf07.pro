;----------------------------------------------------------------------
;
; PROJECT    : ADAS
;
; NAME       : fractionate_adf07
;
; PURPOSE    : to create the spreadsheet,containing the direct
;              ionisation and excitation-autoionisation pathway
;              information for metastable resolved, which is used
;              by split_adf07.pro to calculate the metastable
;              resolved adf07
;
; INPUT      : elem       string     element
;              ionch      intarr     ion charge
;                                   - to be used only with /isoseq
;                                   keyword. If the keyword /isoseq
;                                   is switched off, ionch is ignored.
;                                   COMMENTS: ion charges must be in a
;                                   progressively increasing order
;                                   e.g. ionch=[2,3,4] not [4,3,7]
;              pathway    file      calculated by pathways.pro of the type
;                                   pathways_<elem><z>_<elem><z+1>.dat
;                                   for all the ions of an element or for
;                                   the ions included in the ionch variable
;
; OUTPUT     : file with the final spreadsheet for fractionation
;              to be used by split_adf07.pro. The default
;              filename is fractionation_<elem>.csv
;
; KEYWORDS   : isoseq     if isoseq is set (e.g. fractionate_adf07,elem='si',/isoseq)
;                         the program creates the spreadsheet only for the
;                         ion/ions requested
;
; CALLS      : xxseqid.pro, metastable.pro, xxweight.pro,
;              cfg2occ.pro, xxorbocc.pro
;
; EXAMPLE    : Examples of how to run:
;                         1. Case of the whole set of ionisation stages
;                            of an element
;                         IDL> fractionate_adf07,elem='si'
;
;                         2. Case of one ionisation stage only
;                         IDL> fractionate_adf07,elem='si',ionch=[9],/isoseq
;
;                         3. Case of a selected set of ionisation stages
;                         IDL> fractionate_adf07,elem='si',ionch=[3,4,5],/isoseq
;
; AUTHOR     : Alessandra Giunta
;
; DATE       : 14-05-2012
;
;
; MODIFIED   : Alessandra Giunta
;       1.1
;              - First version.
;
; VERSION:
;       1.1    14-05-2012
;
;----------------------------------------------------------------------
;
pro fractionate_adf07, elem        =  elem,         $
                       ionch       =  ionch,        $
                       spread_file =  spread_file,  $
                       path_file   =  path_file,    $
                       isoseq      =  isoseq

;establish the atomic number from the element
elem=strtrim(strlowcase(elem),2)
z0=xxeiz0(elem)

;define the output file

if n_elements(spread_file) EQ 0 then fileout='fractionation_'+elem+'.csv' $
                                else fileout=spread_file


openw,lout,fileout,/get_lun

;print the heading of the spreadsheet
printf,lout,'"Transition","Metastable","Index","Purpose","Formula","Shell 1",,"Shell 2",,"Shell 3",,"Shell 4",,"Shell 5",,"Shell 6",,"Shell 7",,"Scale factor"'
printf,lout,',,,,,"I(1s) (Ryd)","z(1s)","I(2s) (Ryd)","z(2s)","I(2p) (Ryd)","z(2p)","I(3s)(Ryd)","z(3s)","I(3p)(Ryd)","z(3p)","I(4s)(Ryd)","z(4s)","I(3d)(Ryd)","z(3d)",'

;define the maximum number of shell to be considered
nshellavl=7

;check the keyword /isoseq
if not keyword_set(isoseq) then begin
   nfile=fix(z0)
   filepath=strarr(nfile)
   infl=0
   fnfl=nfile-1
endif else begin
   nfile=n_elements(ionch)
   filepath=strarr(nfile)
   infl=ionch[0]
   fnfl=ionch[nfile-1]
endelse

for i=infl,fnfl do begin
  filepath[i-infl]='pathways_'+elem+strtrim(string(i),2)+$
                   '_'+elem+strtrim(string(i+1),2)+'.dat'
endfor
filepath=reverse(filepath)


; User supplied pathway files

if n_elements(path_file) GT 0 then begin
   filepath = path_file
   nfile    = n_elements(filepath)
endif

;define the ionisation stage for the ionising ion (iz)
;and the ionised ion(iz1)
str=''
nfdef=n_elements(filepath)
;start file loop
for i=0,nfdef-1 do begin
  openr,lun,filepath[i],/get_lun
  readf,lun,str
  if strlen(elem) eq 1 then begin
     iz=fix(strmid(str,2,2))
  endif else begin
     iz=fix(strmid(str,3,2))
  endelse
  iz1=iz+1

  print,'       *****'
  print,'       ',strupcase(elem)+'+'+strtrim(string(iz),2), '   START'
  print,'       *****'

  ;set iso-electronic sequence for ionising ion
  seq=strtrim(xxseqid(z0,iz),2)
  ;set iso-electronic sequence for ionised ion
  if z0 eq iz1 then begin    ;case of bare nucleus (like H+)
     seq1='bare'
  endif else begin
     seq1=strtrim(xxseqid(z0,iz1),2)
  endelse

  ;find metastables for each isoelectronic sequence using the
  ; metastable.pro routine and compare their number with the one
  ; in the pathway files

  ;for ionising ion
  metastable, seq=seq, config=config_met,term=term_met,spin=spin_met
  nc_met=n_elements(config_met)
  test_term_met=term_met
  if n_elements(term_met) ne n_elements(spin_met) then begin
     xxweight,test_term_met,lterm_met,spin_met
     spin_met=fix(spin_met)
  endif
  ;for ionised ion
  if z0 eq iz1 then begin    ;case of bare nucleus (like H+)
     config_met1=['1s0']
     term_met1=['1S']
     spin_met1=[1]
  endif else begin
     metastable, seq=seq1, config=config_met1,term=term_met1,spin=spin_met1
     test_term_met1=term_met1
     if n_elements(term_met1) ne n_elements(spin_met1) then begin
        xxweight,test_term_met1,lterm_met1,spin_met1
        spin_met1=fix(spin_met1)
     endif

  endelse
  nc_met1=n_elements(config_met1)

  ;read the number of metastables in the pathway files
  readf,lun,str
  nmetiz=fix(strmid(str,24,4))
  readf,lun,str
  nmetiz1=fix(strmid(str,24,4))

  metiz1=strarr(nmetiz1)
  for im1=0,nmetiz1-1 do begin
    metiz1[im1]=strtrim(config_met1[im1],2)+'('+strtrim(term_met1[im1],2)+')'
  endfor

  if nc_met1 ne nmetiz1 or nc_met ne nmetiz then begin
     message,'*** ERROR IN THE NUMBER OF METASTABLES ***'
  endif

  ;read header information from the pathway files:
  ;orbital and their energies
  readf,lun,str
  norbiz=fix(strmid(str,24,4))
  orbiz=strarr(norbiz)
  enorbiz=fltarr(norbiz)

 ; read number of pathways for each metastable
  npath=intarr(nc_met)
  strpath=strarr(1)
  readf,lun,strpath,npath,format='(a24,'+strtrim(string(nc_met),2)+'i4)'

  for is=0,2 do readf,lun,str
  for io=0,norbiz-1 do begin
    readf,lun,str
    orbiz[io]=strtrim(strmid(str,0,2),2)
    enorbiz[io]=float(strmid(str,14,10))
  endfor
  readf,lun,str
  readf,lun,str
  transition=strtrim(str,2)

  for is=0,2 do readf,lun,str
  metiz=strarr(nmetiz)
  metizcfg=strarr(nmetiz)

  tnpath=total(npath)

  ;define pathway variables for each metastable of the ionising ion
  for im=0,nmetiz-1 do begin
    cmd1='shell_'+strtrim(string(im),2)+'=strarr(npath['+strtrim(string(im),2)+'])'
    res1=execute(cmd1)

    cmd2='confpath_'+strtrim(string(im),2)+'=strarr(npath['+strtrim(string(im),2)+'])'
    res2=execute(cmd2)

    cmd3='termpath_'+strtrim(string(im),2)+'=strarr(npath['+strtrim(string(im),2)+'])'
    res3=execute(cmd3)

    cmd4='energypath_'+strtrim(string(im),2)+'=fltarr(npath['+strtrim(string(im),2)+'])'
    res4=execute(cmd4)

    cmd5='energypath_ryd_'+strtrim(string(im),2)+'=fltarr(npath['+strtrim(string(im),2)+'])'
    res5=execute(cmd5)

    cmd5b='weightpath_'+strtrim(string(im),2)+'=fltarr(npath['+strtrim(string(im),2)+'])'
    res5b=execute(cmd5b)

    cmd5l='lpartpath_'+strtrim(string(im),2)+'=fltarr(npath['+strtrim(string(im),2)+'])'
    res5l=execute(cmd5l)

    cmd5s='spartpath_'+strtrim(string(im),2)+'=fltarr(npath['+strtrim(string(im),2)+'])'
    res5s=execute(cmd5s)
  endfor

  ;start iz metastable loop
  for im=0,nmetiz-1 do begin
    readf,lun,str
    metiz[im]=strtrim(strmid(str,0,42),2)+'('+strtrim(strmid(str,43,4),2)+')'
    metizcfg[im]=strtrim(strmid(str,0,42),2)

    ishell=strarr(npath[im])
    iconf=strarr(npath[im])
    iterm=strarr(npath[im])
    ien=fltarr(npath[im])
    ien_ryd=fltarr(npath[im])
    iweight=fltarr(npath[im])
    ilpart=intarr(npath[im])
    ispart=fltarr(npath[im])
    for imm=0,npath[im]-1 do begin
      readf,lun,str
      ishell[imm]=strtrim(strmid(str,0,55),2)
      iconf[imm]=strtrim(strmid(str,55,40),2)
      iterm[imm]=strtrim(strmid(str,95,4),2)
      ien[imm]=float(strmid(str,99,15))
      ien_ryd[imm]=float(strmid(str,114,15))
    endfor

    icount=0
    rorbiz=reverse(orbiz)
    for io=0,norbiz-1 do begin
      ww=where(ishell eq rorbiz[io])
      if ww[0] ne -1 then begin
         icount=icount+1
      endif
    endfor

    ictwgt=intarr(icount)

    icountf=0
    icountd=0
    ;calculate the weigths and assign them to each pathway
    for io=0,norbiz-1 do begin
      ww=where(ishell eq rorbiz[io])
      if ww[0] ne -1 then begin
         nww=n_elements(ww)
         xxweight,iterm[ww],lpart,spart,wdum,scfac
         icountf=icountf+n_elements(scfac)
         icountd=icountd+1
         ictwgt[icountd-1]=icountf-1
      endif
    endfor

    icounth=0
    for io=0,norbiz-1 do begin
      ww=where(ishell eq rorbiz[io])
      if ww[0] ne -1 then begin
         nww=n_elements(ww)
         xxweight,iterm[ww],lpart,spart,wdum,scfac
         icounth=icounth+1
         if (icounth-1)  ne 0 then begin
            iweight[ictwgt[icounth-2]+1:ictwgt[icounth-1]]=scfac
            ilpart[ictwgt[icounth-2]+1:ictwgt[icounth-1]]=lpart
            ispart[ictwgt[icounth-2]+1:ictwgt[icounth-1]]=spart
         endif else begin
            iweight[0:ictwgt[icounth-1]]=scfac
         endelse
      endif
    endfor

    ;assign values to the pathway variables
    cmd6='shell_'+strtrim(string(im),2)+'[*]=ishell'
    res6=execute(cmd6)

    cmd7='confpath_'+strtrim(string(im),2)+'[*]=iconf'
    res7=execute(cmd7)

    cmd8='termpath_'+strtrim(string(im),2)+'[*]=iterm'
    res8=execute(cmd8)

    cmd9='energypath_'+strtrim(string(im),2)+'[*]=ien'
    res9=execute(cmd9)

    cmd10='energypath_ryd_'+strtrim(string(im),2)+'[*]=ien_ryd'
    res10=execute(cmd10)

    cmd11='weightpath_'+strtrim(string(im),2)+'[*]=iweight'
    res11=execute(cmd11)

    cmd11l='lpartpath_'+strtrim(string(im),2)+'[*]=ilpart'
    res11l=execute(cmd11l)

    cmd11s='spartpath_'+strtrim(string(im),2)+'[*]=ispart'
    res11s=execute(cmd11s)
  endfor

  ;close the pathway files
  close,lun
  free_lun,lun

  ;define ionisation potentials for ground term
  ;removing one electron from each shell and
  ;following Burgess case A and B
  ;Note that as threshold has been chosen 25%
  ;in the different of energy to distinguish between
  ;Burgess case A and B. This is not a precise choice.

  ;ground term occupation numbers
  izoccground=cfg2occ(metizcfg[0],z0,iz)
  izoccground=float(izoccground(where(izoccground ne 0)))
  nizoccground=n_elements(izoccground)
  ;ground term orbitals
  orbground=strarr(nizoccground)
  for j=0,nizoccground-1 do begin
    orbground[j]=strmid(metizcfg[0],j*4,2)
  endfor

  morbground=strarr(nizoccground)
  for imog=0,nizoccground-1 do morbground[imog]=strmid(orbground[imog],1,1)
  ;max orbital occupation numbers
  maxoccground = xxorbocc(morbground)

  ;ionisation potential
  enrydground=fltarr(nizoccground)
  ;first and last shells
  enrydground[nizoccground-1]=energypath_ryd_0[0]
  if nizoccground-1 ne 0 then enrydground[0]=enorbiz[0]-enorbiz[1]
  ;middle shells
  if nizoccground gt 2 then begin
    for ig=1, nizoccground-2 do begin
      pos_shell_0=where(orbiz[ig] eq shell_0)
      npos_shell_0=n_elements(pos_shell_0)
      if pos_shell_0[0] ne -1 then pos_shell_0_uniq=pos_shell_0[0]

      if ig ne nizoccground-2 then begin
         if enorbiz[ig] lt (enorbiz[ig+1]+0.25*enorbiz[ig+1]) $
         and enorbiz[ig] gt (enorbiz[ig+1]-0.25*enorbiz[ig+1]) then begin
            enrydground[ig]=energypath_ryd_0[0] ;Burgess case B
         endif else begin
            if energypath_ryd_0[pos_shell_0_uniq] ne -1.0 and npos_shell_0 eq 1 then begin
               enrydground[ig]=energypath_ryd_0[pos_shell_0_uniq] ;Burgess case A
            endif else begin
               enrydground[ig]=enorbiz[ig]  ;Burgess case A
            endelse
         endelse
      endif else begin
         if enorbiz[ig] lt (enorbiz[ig+1]+1.5*enorbiz[ig+1]) $
         and enorbiz[ig] gt (enorbiz[ig+1]-1.5*enorbiz[ig+1]) then begin
            ;case of noble gas, where both the last two shells are full:
            ;in that case Burgess case A in used
            if fix(izoccground[nizoccground-1]) eq maxoccground[nizoccground-1] $
            and fix(izoccground[nizoccground-2]) eq maxoccground[nizoccground-2]$
            then begin
               if energypath_ryd_0[pos_shell_0_uniq] ne -1.0 and npos_shell_0 eq 1 then begin
                  enrydground[ig]=energypath_ryd_0[pos_shell_0_uniq] ;Burgess case A
               endif else begin
                  enrydground[ig]=enorbiz[ig]  ;Burgess case A
               endelse
            endif else begin
               enrydground[ig]=energypath_ryd_0[0] ;Burgess case B
            endelse
         endif else begin
            if energypath_ryd_0[pos_shell_0_uniq] ne -1.0 and npos_shell_0 eq 1 then begin
               enrydground[ig]=energypath_ryd_0[pos_shell_0_uniq] ;Burgess case A
            endif else begin
               enrydground[ig]=enorbiz[ig]  ;Burgess case A
            endelse
         endelse
      endelse

    endfor
  endif

  ;array which contains ionisation potential
  ;and occupation number (e.g. [14.6, 2., 1.8, 2.])
  ntorground=2*nizoccground
  wgroundarr=fltarr(ntorground)
  wgroundarr[0]=enrydground[0]
  wgroundarr[1]=izoccground[0]
  if nizoccground gt 1 then begin
     for k=1,nizoccground-1 do begin
       wgroundarr[2*k]=enrydground[k]
       wgroundarr[2*k+1]=izoccground[k]
     endfor
  endif

  newnpath=intarr(nmetiz)

  noccg=n_elements(izoccground)
  ;define the number of comas for writing the ground
  ;term lines in the spreadsheet
  ncomag=(nshellavl-noccg)*2
  icsg=strarr(ncomag)
  icsg[0]=','
  for icomag=1,ncomag-1 do begin
    icsg[icomag]=','
    icsg[icomag]=icsg[icomag-1]+icsg[icomag]
  endfor
  csg=icsg[ncomag-1]

  ;print the ground term values in the output file
  printf,lout,'"'+strtrim(transition,2)+'",,"f1",,,'$
         +string(strtrim(string(wgroundarr)+',',2),$
         format='('+strtrim(string(ntorground),2)+'a12)')$
         +csg+'1.000'


  ;print the comment line on the comparison with unresolved
  ;adf07 calculation
  printf,lout,',,,'+'"RATE comparison",'+'"ratio=RATE/f1"'+',,,,,,,,,,,,,,,'

  ;start to estimate and print the data for the othe lines
  ;in the output file: distinguish between the case of bare
  ; (iz1=z+1=z0) and the other ions (iz1=z+1<z0)
  if iz1  eq z0  then begin ;start of bare if
     ;case of the bare (e.g. Si+13->Si+14)
     printf,lout,',"1s(2S)",,"1s (2S) total","f1*ratio",,,,,,,,,,,,,,,'
     printf,lout,',"#",,"1s(2S)->(1S)","f1*ratio",,,,,,,,,,,,,,,'

     print,''
     print,'       ',strtrim(string(i),2),' pathway file: ',strtrim(filepath[i],2)
     print,''
     print,'       *****'
     print,'       ',strupcase(elem)+'+'+strtrim(string(iz),2), '   COMPLETED'
     print,'       *****'
     wait,1

  endif else begin   ;bare if
     ;count to provide the correct number to the formula indeces (f)
     findexcount=1

     for im=0,nmetiz-1 do begin  ;start metastable loop
       ;metastable occupation numbers
       mocc=cfg2occ(metizcfg[im],z0,iz)
       mocc=float(mocc(where(mocc ne 0)))
       nmocc=n_elements(mocc)
       ;metastable orbitals
       morb=strarr(nmocc)
       for j=0,nmocc-1 do begin
         morb[j]=strmid(metizcfg[im],j*4,2)
       endfor

       prtmorb=strarr(nmocc)
       for iprtmm=0,nmocc-1 do prtmorb[iprtmm]=strmid(morb[iprtmm],1,1)
       ;max orbital occupation numbers
       maxprtmorb = xxorbocc(prtmorb)
       nicount=0

       ;recognise the metastable for the ionised ion in the pathways
       aam1=intarr(npath[im])
       for ip=0,npath[im]-1 do begin
         cmdaam1='iaam1=where(confpath_'+strtrim(string(im),2)+'['+strtrim(string(ip),2)+$
                 ']+termpath_'+strtrim(string(im),2)+'['+strtrim(string(ip),2)+$
                 '] eq strtrim(config_met1,2)+strtrim(term_met1,2))'
         resaam1=execute(cmdaam1)

         aam1[ip]=iaam1[0]
       endfor
       paam12=aam1(where(aam1 ne -1))
       paam1=where(aam1 ne -1)
       naam1=n_elements(paam1)

       ;define the variable where the final formula will be put
       autoformula=strarr(100)
       directformula=strarr(100)
       termautoformula=strarr(100)
       spinautoformula=intarr(100)
       pathindex=intarr(100)
       pathindexauto=intarr(100)


       ;start of the loop on the pathways, with different
       ;consideration for the last shell (ip=0) and middle
       ;shells (ip>0), where the excitation-autoionisation
       ;is included when appropriate
       for ip=0,npath[im]-1 do begin ;start pathway loop

         ;As for the ground, if more pathways correspond to the
         ;removal of an electron from one shell then find
         ;the lowest ionisation potential
         cmdmig='mig=where(shell_'+strtrim(string(im),2)+$
                       '['+strtrim(string(ip),2)+'] eq orbiz)'
         rmig=execute(cmdmig)

         cmdposshellim='pos_shell_'+strtrim(string(im),2)+$
                       '=where(orbiz[mig[0]] eq shell_'+strtrim(string(im),2)+')'
         resposshellim=execute(cmdposshellim)

         cmdnumposshellim='npos_shell_'+strtrim(string(im),2)+$
                          '=n_elements(pos_shell_'+strtrim(string(im),2)+')'
         resnumposshellim=execute(cmdnumposshellim)

         cmdifshellim='if pos_shell_'+strtrim(string(im),2)+$
                      '[0] ne -1 then pos_shell_uniq_'+strtrim(string(im),2)+$
                      '=pos_shell_'+strtrim(string(im),2)+'[0]'
         resifshellim=execute(cmdifshellim)
         ;-------------------------------

         if ip eq 0 then begin  ;start of last/middle shell if
            nicount=nicount+1

            ;define the ionisation potential to be assigned when
            ;removing an electron from the last shell
            ;(the most external shell) and the other features
            ;related to that shell: shell,comments,configuration,
            ;ionisation potential,occupation number,weight

            ;cc='print,shell_'+strtrim(string(im),2)+'['+strtrim(string(ip),2)+'],'+$
            ;   '"  via  "'+  ',termpath_'+strtrim(string(im),2)+'['+strtrim(string(ip),2)+'],'+$
            ;   '"  direct",'+$
            ;   'energypath_ryd_'+strtrim(string(im),2)+'['+strtrim(string(ip),2)+'],'+$
            ;   'mocc(where(shell_'+strtrim(string(im),2)+'['+strtrim(string(ip),2)+'] eq morb)),'+$
            ;   'weightpath_'+strtrim(string(im),2)+'['+strtrim(string(ip),2)+']'
            ;rcc=execute(cc)

            findexcount=findexcount+1

            ;define formula index
            cs0a='"f'+strtrim(string(findexcount),2)+'"'
            lfa=strlen(cs0a)
            fa=strmid(cs0a,1,lfa-2)
            directformula[findexcount-1]=fa
            pathindex[findexcount-1]=ip

            ;prepare the string to print on the output file
            ccs1='cs1=string(shell_'+strtrim(string(im),2)+'['+strtrim(string(ip),2)+'],'+$
                 '"  via  "'+  ',termpath_'+strtrim(string(im),2)+'['+strtrim(string(ip),2)+'],'+$
                 '"  direct")'
            rccs1=execute(ccs1)
            ncoma=(nmocc-1)*2
            ncoma2=(nshellavl-nmocc)*2
            if ncoma eq 0 then begin
               ics2=strarr(1)
            endif else begin
               ics2=strarr(ncoma)
            endelse
            ics4=strarr(ncoma2)
            ics4[0]=','
            if ncoma eq 0 then begin
               ics2[0]=''
               cs2=ics2
            endif else begin
               ics2[0]=','
               for icoma=1,ncoma-1 do begin
                 ics2[icoma]=','
                 ics2[icoma]=ics2[icoma-1]+ics2[icoma]
               endfor
               cs2=ics2[ncoma-1]
            endelse
            for icoma2=1,ncoma2-1 do begin
              ics4[icoma2]=','
              ics4[icoma2]=ics4[icoma2-1]+ics4[icoma2]
            endfor
            cs4=ics4[ncoma2-1]
            ccs3='cs3=string(energypath_ryd_'+strtrim(string(im),2)+'['+strtrim(string(ip),2)+'])'
            rccs3=execute(ccs3)
            ccs6='cs6=string(mocc(where(shell_'+strtrim(string(im),2)+'['+strtrim(string(ip),2)+$
                 '] eq morb)))'
            rccs6=execute(ccs6)
            ccs5='cs5=string(weightpath_'+strtrim(string(im),2)+'['+strtrim(string(ip),2)+'])'
            rccs5=execute(ccs5)

            ;print the string on the output file
            printf,lout,','+'"'+metiz[im]+'",'+cs0a+','+'"'+strtrim(cs1,2)+'",,'+cs2+cs3+$
                        ','+cs6+','+cs4+strtrim(cs5,2)
         endif else begin ;last/middle shell if

            ;case of z0-iz=4 when for im=0 only 1s and 2s are available
            if z0-iz eq 4 and im eq 0 then begin  ; start z0-iz=4 if
              if z0 eq 5 then begin
                 ;print,'1s +auto',enorbiz[0]-enorbiz[1],mocc[0]
                 findexcount=findexcount+1
                 cs0b='"f'+strtrim(string(findexcount),2)+'"'
                 lfb=strlen(cs0b)
                 fb=strmid(cs0b,1,lfb-2)
                 autoformula[findexcount-1]=fb
                 termautoformula[findexcount-1]=''
                 spinautoformula[findexcount-1]=-1
                 cs1='1s +auto'
                 cs4=',,,,,,,,,,,,'
                 cs3=string(enorbiz[0]-enorbiz[1])+','+string(mocc[0])
                 cs5=string(1.00000)

                 ;print on the output file
                 printf,lout,',,'+cs0b+','+'"'+cs1+'",,'+cs3+','+cs4+strtrim(cs5,2)
               endif
            endif else begin  ;z0-iz=4 if

               ;define the variables which establish the ionisation
               ;potential to assign to each middle shell when removing
               ;an electron from that shell and to establish when
               ;direct ionisation and excitation-autoionisation are
               ;appropriate

               ;iaam1orb distinguishes between direct ionisation only and
               ;direct+excitation/autoionisation
               ;cmdaam1orb='iaam1orb=where(energypath_ryd_'+strtrim(string(im),2)+$
               ;           '['+strtrim(string(ip),2)+'] le 1.*enorbiz(where(shell_'+$
               ;           strtrim(string(im),2)+$
               ;           '['+strtrim(string(ip),2)+'] eq orbiz)) and energypath_ryd_'+$
               ;           strtrim(string(im),2)+$
               ;           '['+strtrim(string(ip),2)+'] ne -1)'

               cmdaam1orb='iaam1orb=where(energypath_ryd_'+strtrim(string(im),2)+$
                          '['+strtrim(string(ip),2)+'] le 1.*energypath_ryd_0[0] and energypath_ryd_'+$
                          strtrim(string(im),2)+$
                          '['+strtrim(string(ip),2)+'] ne -1)'

               resaam1orb=execute(cmdaam1orb)


               ;case of last shell for metastable pathways
               cmdilasts='ilasts=where(shell_'+strtrim(string(im),2)+$
                          '['+strtrim(string(ip),2)+'] eq morb[nmocc-1])'
               resilasts=execute(cmdilasts)
               ;case of the shell before the last one
               cmdilasts2='ilasts2=where(shell_'+strtrim(string(im),2)+$
                          '['+strtrim(string(ip),2)+'] eq morb[nmocc-2])'
               resilasts2=execute(cmdilasts2)

               ;iaam2orb distinguish between Burgess case A and B
               if ilasts[0] eq -1 then begin
                  ;if this is not the last shell but
                  ;the shell before the last one
                  if ilasts2[0] ne -1 then begin
                     cmdaam2orb='iaam2orb=where(enorbiz(where(shell_'+strtrim(string(im),2)+$
                                '['+strtrim(string(ip),2)+'] eq orbiz)) lt enorbiz(where(shell_'+$
                                strtrim(string(im),2)+$
                                '['+strtrim(string(ip),2)+'] eq orbiz)+1)+1.5*enorbiz(where(shell_'+$
                                strtrim(string(im),2)+$
                                '['+strtrim(string(ip),2)+'] eq orbiz)+1) and enorbiz(where(shell_'+$
                                strtrim(string(im),2)+$
                                '['+strtrim(string(ip),2)+'] eq orbiz)) gt enorbiz(where(shell_'+$
                                strtrim(string(im),2)+$
                                '['+strtrim(string(ip),2)+'] eq orbiz)+1)-1.5*enorbiz(where(shell_'+$
                                 strtrim(string(im),2)+$
                                '['+strtrim(string(ip),2)+'] eq orbiz)+1))'
                     resaam2orb=execute(cmdaam2orb)
                  endif else begin
                     cmdaam2orb='iaam2orb=where(enorbiz(where(shell_'+strtrim(string(im),2)+$
                                '['+strtrim(string(ip),2)+'] eq orbiz)) lt enorbiz(where(shell_'+$
                                strtrim(string(im),2)+$
                                '['+strtrim(string(ip),2)+'] eq orbiz)+1)+0.25*enorbiz(where(shell_'+$
                                strtrim(string(im),2)+$
                                '['+strtrim(string(ip),2)+'] eq orbiz)+1) and enorbiz(where(shell_'+$
                                strtrim(string(im),2)+$
                                '['+strtrim(string(ip),2)+'] eq orbiz)) gt enorbiz(where(shell_'+$
                                strtrim(string(im),2)+$
                               '['+strtrim(string(ip),2)+'] eq orbiz)+1)-0.25*enorbiz(where(shell_'+$
                                strtrim(string(im),2)+$
                               '['+strtrim(string(ip),2)+'] eq orbiz)+1))'
                     resaam2orb=execute(cmdaam2orb)
                  endelse
               endif else begin
                  cmdaam2orb='iaam2orb=where(enorbiz(where(shell_'+strtrim(string(im),2)+$
                             '['+strtrim(string(ip),2)+'] eq orbiz)) lt enorbiz(where(shell_'+$
                             strtrim(string(im),2)+$
                             '['+strtrim(string(ip),2)+'] eq orbiz)-1)+0.25*enorbiz(where(shell_'+$
                             strtrim(string(im),2)+$
                             '['+strtrim(string(ip),2)+'] eq orbiz)-1) and enorbiz(where(shell_'+$
                             strtrim(string(im),2)+$
                             '['+strtrim(string(ip),2)+'] eq orbiz)) gt enorbiz(where(shell_'+$
                             strtrim(string(im),2)+$
                             '['+strtrim(string(ip),2)+'] eq orbiz)-1)-0.25*enorbiz(where(shell_'+$
                             strtrim(string(im),2)+$
                             '['+strtrim(string(ip),2)+'] eq orbiz)-1))'
                  resaam2orb=execute(cmdaam2orb)
               endelse


               ;direct+auto
               if iaam1orb[0] ne -1 then begin  ;start direct+auto if
                 ;direct+auto part
                 nicount=nicount+2

                  cvb='shellcomp=shell_'+strtrim(string(im),2)+'['+strtrim(string(ip),2)+']'
                  rescvd=execute(cvb)

                  ;auto
                  if (iaam2orb[0] eq -1)  $
                  or (fix(mocc[nmocc-1]) eq maxprtmorb[nmocc-1] $
                  and fix(mocc[nmocc-2]) eq maxprtmorb[nmocc-2])$
                  then begin   ; start Burgess case if
                     ;Burgess case A: ionisation potential=enorb
                     ;                or pathway ion.pot.

                     cmdaam5orb='iaam5orb=where(pos_shell_'+strtrim(string(im),2)+'[0] eq -1)'
                     resaam5orb=execute(cmdaam5orb)


                     if iaam5orb[0] eq -1 then begin
                        cmdaam4orb='iaam4orb=where(energypath_ryd_'+strtrim(string(im),2)+$
                                   '[pos_shell_uniq_'+strtrim(string(im),2)+$
                                   '] ne -1.0 and npos_shell_'+strtrim(string(im),2)+' eq 1)'
                        resaam4orb=execute(cmdaam4orb)
                     endif else begin
                        iaam4orb=-1
                     endelse

                     ;as for the ground
                     if z0-iz ne 3 and z0-iz ne 2 then begin

                        if ilasts[0] eq -1 then begin

                           if iaam4orb[0] ne -1 then begin
                              cmdenvar='energyvariable=energypath_ryd_'+strtrim(string(im),2)+$
                                       '[pos_shell_uniq_'+strtrim(string(im),2)+']'
                              resenvar=execute(cmdenvar)
                           endif else begin
                              cmdenvar='energyvariable=enorbiz(where(shell_'+strtrim(string(im),2)+$
                                       '['+strtrim(string(ip),2)+'] eq morb))'
                              resenvar=execute(cmdenvar)
                           endelse
                        endif else begin
                              cmdenvar='energyvariable=energypath_ryd_'+strtrim(string(im),2)+'[0]'
                              resenvar=execute(cmdenvar)
                        endelse

                     endif else begin
                        energyvariable=enorbiz[0]-enorbiz[1]
                     endelse


                     ;cc='print,shell_'+strtrim(string(im),2)+'['+strtrim(string(ip),2)+'],'+$
                     ;   '"  via  "'+  ',termpath_'+strtrim(string(im),2)+$
                     ;   '['+strtrim(string(ip),2)+'],'+'"  +auto",'+$
                     ;   'energyvariable,'+$
                     ;   'mocc(where(shell_'+strtrim(string(im),2)+$
                     ;   '['+strtrim(string(ip),2)+'] eq morb)),'+$
                     ;   'weightpath_'+strtrim(string(im),2)+'['+strtrim(string(ip),2)+']'
                     ;rcc=execute(cc)

                     findexcount=findexcount+1
                     cs0c='"f'+strtrim(string(findexcount),2)+'"'
                     lfc=strlen(cs0c)
                     fc=strmid(cs0c,1,lfc-2)
                     autoformula[findexcount-1]=fc
                     itcmd='itermautoformula=termpath_'+strtrim(string(im),2)+$
                           '['+strtrim(string(ip),2)+']'
                     ritcmd=execute(itcmd)
                     spcmd='ispinautoformula=fix(spartpath_'+strtrim(string(im),2)+$
                           '['+strtrim(string(ip),2)+'])'
                     rspcmd=execute(spcmd)
                     termautoformula[findexcount-1]=itermautoformula
                     spinautoformula[findexcount-1]=ispinautoformula


                     ccs1='cs1=string(shell_'+strtrim(string(im),2)+$
                          '['+strtrim(string(ip),2)+'],'+$
                          '"  via  "'+  ',termpath_'+strtrim(string(im),2)+$
                          '['+strtrim(string(ip),2)+'],'+'"  +auto")'
                     rccs1=execute(ccs1)
                     ccs2='ncoma=(where(morb eq shell_'+strtrim(string(im),2)+$
                          '['+strtrim(string(ip),2)+']))*2.'
                     rccs2=execute(ccs2)
                     ncoma=ncoma[0]
                     ncoma2=(nshellavl-(ncoma/2+1))*2

                     if ncoma eq 0 then begin
                        ics2=strarr(1)
                     endif else begin
                        ics2=strarr(ncoma)
                     endelse
                     ics4=strarr(ncoma2)
                     ics4[0]=','
                     if ncoma eq 0 then begin
                        ics2[0]=''
                        cs2=ics2
                     endif else begin
                        ics2[0]=','
                        for icoma=1,ncoma-1 do begin
                          ics2[icoma]=','
                          ics2[icoma]=ics2[icoma-1]+ics2[icoma]
                        endfor
                        cs2=ics2[ncoma-1]
                     endelse
                     for icoma2=1,ncoma2-1 do begin
                       ics4[icoma2]=','
                       ics4[icoma2]=ics4[icoma2-1]+ics4[icoma2]
                     endfor
                     cs4=ics4[ncoma2-1]

                     ccs3='cs3=string(energyvariable)'
                     rccs3=execute(ccs3)
                     ccs6='cs6=string(mocc(where(shell_'+strtrim(string(im),2)+$
                          '['+strtrim(string(ip),2)+'] eq morb)))'
                     rccs6=execute(ccs6)
                     ccs5='cs5=string(weightpath_'+strtrim(string(im),2)+$
                          '['+strtrim(string(ip),2)+'])'
                     rccs5=execute(ccs5)

                     ;print on the output file
                     printf,lout,',,'+cs0c+','+'"'+strtrim(cs1,2)+'",,'+cs2+strtrim(cs3,2)+$
                                 ','+cs6+','+cs4+strtrim(cs5,2)


                  endif else begin   ;Burgess case if
                     ;Burgess case B: ionisation potential lowered except
                     ;for the case of noble gases where Burgess case
                     ;A is used instead

                     ;if shellcomp ne morb[nmocc-2] - ???

                     ;endif else begin
                        if ilasts2[0] ne -1 then begin
                           cmdvar2='energyvariable2=energypath_ryd_'+strtrim(string(im),2)+'[0]'
                           resvar2=execute(cmdvar2)
                        endif else begin
                           if ilasts[0] eq -1 then begin
                              cmdvar2='energyvariable2=enorbiz(where(shell_'+strtrim(string(im),2)+$
                                      '['+strtrim(string(ip),2)+'] eq morb)+1)'
                              resvar2=execute(cmdvar2)
                           endif else begin
                              cmdvar2='energyvariable2=energypath_ryd_'+strtrim(string(im),2)+'[0]'
                              resvar2=execute(cmdvar2)
                           endelse
                        endelse
                     ;endelse

                     ;cc='print,shell_'+strtrim(string(im),2)+'['+strtrim(string(ip),2)+'],'+$
                     ;   '"  via  "'+  ',termpath_'+strtrim(string(im),2)+$
                     ;   '['+strtrim(string(ip),2)+'],'+'"  +auto",'+$
                     ;   'energyvariable2,'+$
                     ;   'mocc(where(shell_'+strtrim(string(im),2)+'['+strtrim(string(ip),2)+$
                     ;   '] eq morb)),'+$
                     ;    'weightpath_'+strtrim(string(im),2)+'['+strtrim(string(ip),2)+']'
                     ;rcc=execute(cc)

                     findexcount=findexcount+1
                     cs0h='"f'+strtrim(string(findexcount),2)+'"'
                     lfh=strlen(cs0h)
                     fh=strmid(cs0h,1,lfh-2)
                     autoformula[findexcount-1]=fh
                     itcmd='itermautoformula=termpath_'+strtrim(string(im),2)+$
                           '['+strtrim(string(ip),2)+']'
                     ritcmd=execute(itcmd)
                     spcmd='ispinautoformula=fix(spartpath_'+strtrim(string(im),2)+$
                           '['+strtrim(string(ip),2)+'])'
                     rspcmd=execute(spcmd)
                     termautoformula[findexcount-1]=itermautoformula
                     spinautoformula[findexcount-1]=ispinautoformula

                     ccs1='cs1=string(shell_'+strtrim(string(im),2)+'['+strtrim(string(ip),2)+'],'+$
                          '"  via  "'+  ',termpath_'+strtrim(string(im),2)+$
                          '['+strtrim(string(ip),2)+'],'+'"  +auto")'
                     rccs1=execute(ccs1)
                     ccs2='ncoma=(where(morb eq shell_'+strtrim(string(im),2)+$
                          '['+strtrim(string(ip),2)+']))*2'
                     rccs2=execute(ccs2)

                     ncoma=ncoma[0]
                     ncoma2=(nshellavl-(ncoma/2+1))*2

                     if ncoma eq 0 then begin
                        ics2=strarr(1)
                     endif else begin
                        ics2=strarr(ncoma)
                     endelse
                     ics4=strarr(ncoma2)
                     ics4[0]=','
                     if ncoma eq 0 then begin
                        ics2[0]=''
                        cs2=ics2
                     endif else begin
                        ics2[0]=','
                        for icoma=1,ncoma-1 do begin
                          ics2[icoma]=','
                          ics2[icoma]=ics2[icoma-1]+ics2[icoma]
                        endfor
                        cs2=ics2[ncoma-1]
                     endelse
                     for icoma2=1,ncoma2-1 do begin
                       ics4[icoma2]=','
                       ics4[icoma2]=ics4[icoma2-1]+ics4[icoma2]
                     endfor
                     cs4=ics4[ncoma2-1]
                     ccs3='cs3=string(energyvariable2)'
                     rccs3=execute(ccs3)
                     ccs6='cs6=string(mocc(where(shell_'+strtrim(string(im),2)+$
                          '['+strtrim(string(ip),2)+'] eq morb)))'
                     rccs6=execute(ccs6)
                     ccs5='cs5=string(weightpath_'+strtrim(string(im),2)+$
                          '['+strtrim(string(ip),2)+'])'
                     rccs5=execute(ccs5)

                     ;print on the output file
                     printf,lout,',,'+cs0h+','+'"'+strtrim(cs1,2)+$
                                 '",,'+cs2+strtrim(cs3,2)+','+cs6+','+cs4+strtrim(cs5,2)
                  endelse   ;end Burgess case if

                  ;direct

                  ;cc='print,shell_'+strtrim(string(im),2)+'['+strtrim(string(ip),2)+'],'+$
                  ;   '"  via  "'+  ',termpath_'+strtrim(string(im),2)+$
                  ;   '['+strtrim(string(ip),2)+'],'+'"  direct",'+$
                  ;   'energypath_ryd_'+strtrim(string(im),2)+'['+strtrim(string(ip),2)+'],'+$
                  ;   'mocc(where(shell_'+strtrim(string(im),2)+$
                  ;   '['+strtrim(string(ip),2)+'] eq morb)),'+$
                  ;   'weightpath_'+strtrim(string(im),2)+'['+strtrim(string(ip),2)+']'
                  ;rcc=execute(cc)

                  findexcount=findexcount+1
                  cs0i='"f'+strtrim(string(findexcount),2)+'"'
                  lfi=strlen(cs0i)
                  fi=strmid(cs0i,1,lfi-2)
                  directformula[findexcount-1]=fi
                  pathindex[findexcount-1]=ip

                  ccs1='cs1=string(shell_'+strtrim(string(im),2)+$
                       '['+strtrim(string(ip),2)+'],'+$
                       '"  via  "'+  ',termpath_'+strtrim(string(im),2)+$
                       '['+strtrim(string(ip),2)+'],'+'"  direct")'
                  rccs1=execute(ccs1)
                  ccs2='ncoma=(where(morb eq shell_'+strtrim(string(im),2)+$
                       '['+strtrim(string(ip),2)+']))*2'
                  rccs2=execute(ccs2)
                  ncoma=ncoma[0]
                  ncoma2=(nshellavl-(ncoma/2+1))*2
                  if ncoma eq 0 then begin
                     ics2=strarr(1)
                  endif else begin
                     ics2=strarr(ncoma)
                  endelse
                  ics4=strarr(ncoma2)
                  ics4[0]=','
                  if ncoma eq 0 then begin
                     ics2[0]=''
                     cs2=ics2
                  endif else begin
                     ics2[0]=','
                     for icoma=1,ncoma-1 do begin
                       ics2[icoma]=','
                       ics2[icoma]=ics2[icoma-1]+ics2[icoma]
                     endfor
                     cs2=ics2[ncoma-1]
                  endelse
                  for icoma2=1,ncoma2-1 do begin
                    ics4[icoma2]=','
                    ics4[icoma2]=ics4[icoma2-1]+ics4[icoma2]
                  endfor
                  cs4=ics4[ncoma2-1]
                  ccs3='cs3=string(energypath_ryd_'+strtrim(string(im),2)+$
                       '['+strtrim(string(ip),2)+'])'
                  rccs3=execute(ccs3)
                  ccs6='cs6=string(mocc(where(shell_'+strtrim(string(im),2)+$
                       '['+strtrim(string(ip),2)+'] eq morb)))'
                  rccs6=execute(ccs6)
                  ccs5='cs5=string(weightpath_'+strtrim(string(im),2)+$
                       '['+strtrim(string(ip),2)+'])'
                  rccs5=execute(ccs5)

                  ;print onoutput file
                  printf,lout,',,'+cs0i+','+'"'+strtrim(cs1,2)+'",,'+cs2+cs3+$
                              ','+cs6+','+cs4+strtrim(cs5,2)

               endif else begin  ;direct+auto if
                 ;auto part only
                 nicount=nicount+1

                 cvb='shellcomp=shell_'+strtrim(string(im),2)+'['+strtrim(string(ip),2)+']'
                 rescvd=execute(cvb)

                 if (iaam2orb[0] eq -1)  $
                 or (fix(mocc[nmocc-1]) eq maxprtmorb[nmocc-1] $
                 and fix(mocc[nmocc-2]) eq maxprtmorb[nmocc-2])$
                 then begin   ; start Burgess case if
                    ;Burgess case A: ionisation potential=enorb
                    ;                or pathway ion.pot.

                     cmdaam5orb='iaam5orb=where(pos_shell_'+strtrim(string(im),2)+' eq -1)'
                     resaam5orb=execute(cmdaam5orb)

                     if iaam5orb[0] eq -1 then begin
                        cmdaam4orb='iaam4orb=where(energypath_ryd_'+strtrim(string(im),2)+$
                                   '[pos_shell_uniq_'+strtrim(string(im),2)+$
                                   '] ne -1.0 and npos_shell_'+strtrim(string(im),2)+' eq 1)'
                        resaam4orb=execute(cmdaam4orb)
                     endif else begin
                        iaam4orb=-1
                     endelse

                     ;as for the ground
                     if z0-iz ne 3 and z0-iz ne 2 then begin

                        if iaam4orb[0] ne -1 then begin
                           cmdenvar='energyvariable=energypath_ryd_'+strtrim(string(im),2)+$
                                    '[pos_shell_uniq_'+strtrim(string(im),2)+']'
                           resenvar=execute(cmdenvar)
                        endif else begin
                           cmdenvar='energyvariable=enorbiz(where(shell_'+strtrim(string(im),2)+$
                                    '['+strtrim(string(ip),2)+'] eq morb))'
                           resenvar=execute(cmdenvar)
                        endelse

                     endif else begin
                        if z0-iz eq 3 then begin
                           energyvariable=enorbiz[0]-enorbiz[1]
                        endif else begin
                           cmdenvar='energyvariable=energypath_ryd_'+strtrim(string(im),2)+$
                                    '[ip]'
                           resenvar=execute(cmdenvar)
                        endelse
                     endelse

                     ;cc='print,shell_'+strtrim(string(im),2)+'['+strtrim(string(ip),2)+'],'+$
                     ;   '"  via  "'+  ',termpath_'+strtrim(string(im),2)+$
                     ;   '['+strtrim(string(ip),2)+'],'+'"  +auto",'+$
                     ;   'energyvariable,'+$
                     ;   'mocc(where(shell_'+strtrim(string(im),2)+$
                     ;   '['+strtrim(string(ip),2)+'] eq morb)),'+$
                     ;   'weightpath_'+strtrim(string(im),2)+'['+strtrim(string(ip),2)+']'
                     ;rcc=execute(cc)

                     findexcount=findexcount+1
                     cs0d='"f'+strtrim(string(findexcount),2)+'"'
                     lfd=strlen(cs0d)
                     fd=strmid(cs0d,1,lfd-2)
                     autoformula[findexcount-1]=fd
                     itcmd='itermautoformula=termpath_'+strtrim(string(im),2)+$
                           '['+strtrim(string(ip),2)+']'
                     ritcmd=execute(itcmd)
                     spcmd='ispinautoformula=fix(spartpath_'+strtrim(string(im),2)+$
                           '['+strtrim(string(ip),2)+'])'
                     rspcmd=execute(spcmd)
                     termautoformula[findexcount-1]=itermautoformula
                     spinautoformula[findexcount-1]=ispinautoformula


                     ccs1='cs1=string(shell_'+strtrim(string(im),2)+$
                          '['+strtrim(string(ip),2)+'],'+$
                          '"  via  "'+  ',termpath_'+strtrim(string(im),2)+$
                          '['+strtrim(string(ip),2)+'],'+'"  +auto")'
                     rccs1=execute(ccs1)
                     ccs2='ncoma=(where(morb eq shell_'+strtrim(string(im),2)+$
                          '['+strtrim(string(ip),2)+']))*2.'
                     rccs2=execute(ccs2)

                     ncoma=ncoma[0]
                     ncoma2=(nshellavl-(ncoma/2+1))*2

                     if ncoma eq 0 then begin
                        ics2=strarr(1)
                     endif else begin
                        ics2=strarr(ncoma)
                     endelse
                     ics4=strarr(ncoma2)
                     ics4[0]=','
                     if ncoma eq 0 then begin
                        ics2[0]=''
                        cs2=ics2
                     endif else begin
                        ics2[0]=','
                        for icoma=1,ncoma-1 do begin
                          ics2[icoma]=','
                          ics2[icoma]=ics2[icoma-1]+ics2[icoma]
                        endfor
                        cs2=ics2[ncoma-1]
                     endelse
                     for icoma2=1,ncoma2-1 do begin
                       ics4[icoma2]=','
                       ics4[icoma2]=ics4[icoma2-1]+ics4[icoma2]
                     endfor
                     cs4=ics4[ncoma2-1]

                     ccs3='cs3=string(energyvariable)'
                     rccs3=execute(ccs3)
                     ccs6='cs6=string(mocc(where(shell_'+strtrim(string(im),2)+$
                          '['+strtrim(string(ip),2)+'] eq morb)))'
                     rccs6=execute(ccs6)
                     ccs5='cs5=string(weightpath_'+strtrim(string(im),2)+$
                          '['+strtrim(string(ip),2)+'])'
                     rccs5=execute(ccs5)

                     ;print on the output file
                     printf,lout,',,'+cs0d+','+'"'+strtrim(cs1,2)+'",,'+cs2+strtrim(cs3,2)+$
                                 ','+cs6+','+cs4+strtrim(cs5,2)

                 endif else begin   ;Burgess case if

                    ;Burgess case B: ionisation potential lowered

                        if ilasts2[0] ne -1 then begin
                           cmdvar2='energyvariable2=energypath_ryd_'+strtrim(string(im),2)+'[0]'
                           resvar2=execute(cmdvar2)
                        endif else begin
                           if ilasts[0] eq -1 then begin
                              cmdvar2='energyvariable2=enorbiz(where(shell_'+strtrim(string(im),2)+$
                                      '['+strtrim(string(ip),2)+'] eq morb)+1)'
                              resvar2=execute(cmdvar2)
                           endif else begin
                              cmdvar2='energyvariable2=energypath_ryd_'+strtrim(string(im),2)+'[0]'
                              resvar2=execute(cmdvar2)
                           endelse
                        endelse

                        ;cc='print,shell_'+strtrim(string(im),2)+'['+strtrim(string(ip),2)+'],'+$
                        ;   '"  via  "'+  ',termpath_'+strtrim(string(im),2)+$
                        ;   '['+strtrim(string(ip),2)+'],'+'"  +auto",'+$
                        ;   'energyvariable2,'+$
                        ;   'mocc(where(shell_'+strtrim(string(im),2)+'['+strtrim(string(ip),2)+$
                        ;   '] eq morb)),'+$
                        ;    'weightpath_'+strtrim(string(im),2)+'['+strtrim(string(ip),2)+']'

                        ;rcc=execute(cc)

                        findexcount=findexcount+1
                        cs0h='"f'+strtrim(string(findexcount),2)+'"'
                        lfh=strlen(cs0h)
                        fh=strmid(cs0h,1,lfh-2)
                        autoformula[findexcount-1]=fh
                        itcmd='itermautoformula=termpath_'+strtrim(string(im),2)+$
                              '['+strtrim(string(ip),2)+']'
                        ritcmd=execute(itcmd)
                        spcmd='ispinautoformula=fix(spartpath_'+strtrim(string(im),2)+$
                              '['+strtrim(string(ip),2)+'])'
                        rspcmd=execute(spcmd)
                        termautoformula[findexcount-1]=itermautoformula
                        spinautoformula[findexcount-1]=ispinautoformula

                        ccs1='cs1=string(shell_'+strtrim(string(im),2)+$
                             '['+strtrim(string(ip),2)+'],'+$
                             '"  via  "'+  ',termpath_'+strtrim(string(im),2)+$
                             '['+strtrim(string(ip),2)+'],'+'"  +auto")'
                        rccs1=execute(ccs1)
                        ccs2='ncoma=(where(morb eq shell_'+strtrim(string(im),2)+$
                             '['+strtrim(string(ip),2)+']))*2'
                        rccs2=execute(ccs2)

                        ncoma=ncoma[0]
                        ncoma2=(nshellavl-(ncoma/2+1))*2

                        if ncoma eq 0 then begin
                           ics2=strarr(1)
                        endif else begin
                           ics2=strarr(ncoma)
                        endelse
                        ics4=strarr(ncoma2)
                        ics4[0]=','
                        if ncoma eq 0 then begin
                           ics2[0]=''
                           cs2=ics2
                        endif else begin
                           ics2[0]=','
                           for icoma=1,ncoma-1 do begin
                             ics2[icoma]=','
                             ics2[icoma]=ics2[icoma-1]+ics2[icoma]
                           endfor
                           cs2=ics2[ncoma-1]
                        endelse
                        for icoma2=1,ncoma2-1 do begin
                          ics4[icoma2]=','
                          ics4[icoma2]=ics4[icoma2-1]+ics4[icoma2]
                        endfor
                        cs4=ics4[ncoma2-1]
                        ccs3='cs3=energyvariable2'
                        rccs3=execute(ccs3)
                        ccs6='cs6=string(mocc(where(shell_'+strtrim(string(im),2)+$
                             '['+strtrim(string(ip),2)+'] eq morb)))'
                        rccs6=execute(ccs6)
                        ccs5='cs5=string(weightpath_'+strtrim(string(im),2)+$
                             '['+strtrim(string(ip),2)+'])'
                        rccs5=execute(ccs5)

                        ;print on the output file
                        printf,lout,',,'+cs0h+','+'"'+strtrim(cs1,2)+$
                                    '",,'+cs2+strtrim(cs3,2)+','+cs6+','+cs4+strtrim(cs5,2)

                 endelse   ;end Burgess case if

                 ;case of iz1 metastables: if the pathway
                 ;corresponds to a metastable of the ionised
                 ;ion iz1 then add the direct part in any case.
                 ;The pathway ionisation potential is used if
                 ;it exists. If it is -1.0 then the orbital energy
                 ;is used instead

                 cmdmetiz1='conftermmetiz1=string(confpath_'$
                           +strtrim(string(im),2)+'['+strtrim(string(ip),2)+'])'+$
                           '+"("+'+'string(termpath_'+strtrim(string(im),2)+$
                           '['+strtrim(string(ip),2)+'])'+'+")"'
                 rmetiz1=execute(cmdmetiz1)

                 aametiz1eq=where(metiz1 eq conftermmetiz1)

                 if aametiz1eq[0] ne -1 then begin ;if  met direct
                    findexcount=findexcount+1 ;;;check if is correct

                    ;print,metiz1(aametiz1eq),conftermmetiz1,findexcount

                    cmdenmetiz1='enionpotmetiz1=energypath_ryd_'$
                                +strtrim(string(im),2)+'['+strtrim(string(ip),2)+']'
                    renmetiz1=execute(cmdenmetiz1)

                    ;if io.pot eq -1.0 then put orbital energy
                    if enionpotmetiz1 eq -1.0 then begin
                       ;cc='print,shell_'+strtrim(string(im),2)+'['+strtrim(string(ip),2)+'],'+$
                       ;   '"  via  "'+  ',termpath_'+strtrim(string(im),2)+$
                       ;   '['+strtrim(string(ip),2)+'],'+'"  direct",'+$
                       ;   'enorbiz(where(shell_'+strtrim(string(im),2)+$
                       ;   '['+strtrim(string(ip),2)+'] eq orbiz)),'+$
                       ;   'mocc(where(shell_'+strtrim(string(im),2)+$
                       ;   '['+strtrim(string(ip),2)+'] eq morb)),'+$
                       ;   'weightpath_'+strtrim(string(im),2)+'['+strtrim(string(ip),2)+']'
                       ;rcc=execute(cc)

                       if z0-iz gt 3 and enionpotmetiz1 gt 2.*energypath_ryd_0[0] $
                                     then print,' *** WARNING: CHECK IF METASTABLE '$
                                          +strtrim(metiz(aametiz1eq))+' IS AUTOIONISING ***'


                       cs0mi='"f'+strtrim(string(findexcount),2)+'"'
                       lfi=strlen(cs0mi)
                       fmi=strmid(cs0mi,1,lfi-2)
                       directformula[findexcount-1]=fmi
                       pathindex[findexcount-1]=ip


                       ccs1='cs1=string(shell_'+strtrim(string(im),2)+$
                            '['+strtrim(string(ip),2)+'],'+$
                            '"  via  "'+  ',termpath_'+strtrim(string(im),2)+$
                            '['+strtrim(string(ip),2)+'],'+'"  direct")'
                       rccs1=execute(ccs1)
                       ccs2='ncoma=(where(morb eq shell_'+strtrim(string(im),2)+$
                            '['+strtrim(string(ip),2)+']))*2'
                       rccs2=execute(ccs2)
                       ncoma=ncoma[0]
                       ncoma2=(nshellavl-(ncoma/2+1))*2

                       if ncoma eq 0 then begin
                          ics2=strarr(1)
                       endif else begin
                          ics2=strarr(ncoma)
                       endelse
                       ics4=strarr(ncoma2)
                       ics4[0]=','
                       if ncoma eq 0 then begin
                          ics2[0]=''
                          cs2=ics2
                       endif else begin
                          ics2[0]=','
                          for icoma=1,ncoma-1 do begin
                            ics2[icoma]=','
                            ics2[icoma]=ics2[icoma-1]+ics2[icoma]
                          endfor
                          cs2=ics2[ncoma-1]
                       endelse
                       for icoma2=1,ncoma2-1 do begin
                         ics4[icoma2]=','
                         ics4[icoma2]=ics4[icoma2-1]+ics4[icoma2]
                       endfor
                       cs4=ics4[ncoma2-1]

                       ccs3='cs3=string(enorbiz(where(shell_'$
                            +strtrim(string(im),2)+'['+strtrim(string(ip),2)+'] eq orbiz))'
                       rccs3=execute(ccs3)
                       ccs6='cs6=string(mocc(where(shell_'+strtrim(string(im),2)+$
                            '['+strtrim(string(ip),2)+'] eq morb)))'
                       rccs6=execute(ccs6)
                       ccs5='cs5=string(weightpath_'+strtrim(string(im),2)+$
                            '['+strtrim(string(ip),2)+'])'
                       rccs5=execute(ccs5)

                       ;print on the output file
                       printf,lout,',,'+cs0mi+','+'"'+strtrim(cs1,2)+'",,'$
                              +cs2+cs3+','+cs6+','+cs4+strtrim(cs5,2)


                    endif else begin
                       ;cc='print,shell_'+strtrim(string(im),2)+'['+strtrim(string(ip),2)+'],'+$
                       ;   '"  via  "'+  ',termpath_'+strtrim(string(im),2)+$
                       ;   '['+strtrim(string(ip),2)+'],'+'"  direct",'+$
                       ;   'energypath_ryd_'+strtrim(string(im),2)+$
                       ;   '['+strtrim(string(ip),2)+'],'+$
                       ;   'mocc(where(shell_'+strtrim(string(im),2)+$
                       ;   '['+strtrim(string(ip),2)+'] eq morb)),'+$
                       ;   'weightpath_'+strtrim(string(im),2)+'['+strtrim(string(ip),2)+']'
                       ;rcc=execute(cc)

                       if z0-iz gt 3 and enionpotmetiz1 gt 2.*energypath_ryd_0[0] $
                                     then print,' *** WARNING: CHECK IF METASTABLE '$
                                          +strtrim(metiz(aametiz1eq))+' IS AUTOIONISING ***'


                       cs0mei='"f'+strtrim(string(findexcount),2)+'"'
                       lfi=strlen(cs0mei)
                       fmei=strmid(cs0mei,1,lfi-2)
                       directformula[findexcount-1]=fmei
                       pathindex[findexcount-1]=ip

                       ccs1='cs1=string(shell_'+strtrim(string(im),2)+$
                            '['+strtrim(string(ip),2)+'],'+$
                            '"  via  "'+  ',termpath_'+strtrim(string(im),2)+$
                            '['+strtrim(string(ip),2)+'],'+'"  direct")'
                       rccs1=execute(ccs1)
                       ccs2='ncoma=(where(morb eq shell_'+strtrim(string(im),2)+$
                            '['+strtrim(string(ip),2)+']))*2'
                       rccs2=execute(ccs2)
                       ncoma=ncoma[0]
                       ncoma2=(nshellavl-(ncoma/2+1))*2

                       if ncoma eq 0 then begin
                          ics2=strarr(1)
                       endif else begin
                          ics2=strarr(ncoma)
                       endelse
                       ics4=strarr(ncoma2)
                       ics4[0]=','
                       if ncoma eq 0 then begin
                          ics2[0]=''
                          cs2=ics2
                       endif else begin
                          ics2[0]=','
                          for icoma=1,ncoma-1 do begin
                            ics2[icoma]=','
                            ics2[icoma]=ics2[icoma-1]+ics2[icoma]
                          endfor
                          cs2=ics2[ncoma-1]
                       endelse
                       for icoma2=1,ncoma2-1 do begin
                         ics4[icoma2]=','
                         ics4[icoma2]=ics4[icoma2-1]+ics4[icoma2]
                       endfor
                       cs4=ics4[ncoma2-1]

                       ccs3='cs3=string(energypath_ryd_'+strtrim(string(im),2)+$
                            '['+strtrim(string(ip),2)+'])'
                       rccs3=execute(ccs3)
                       ccs6='cs6=string(mocc(where(shell_'+strtrim(string(im),2)+$
                            '['+strtrim(string(ip),2)+'] eq morb)))'
                       rccs6=execute(ccs6)
                       ccs5='cs5=string(weightpath_'+strtrim(string(im),2)+$
                            '['+strtrim(string(ip),2)+'])'
                       rccs5=execute(ccs5)

                       ;print on output file
                       printf,lout,',,'+cs0mei+','+'"'+strtrim(cs1,2)+'",,'$
                             +cs2+cs3+','+cs6+','+cs4+strtrim(cs5,2)
                    endelse

                 endif ;end if met direct

               endelse  ;end direct+auto if
            endelse  ;end z0-iz=4 if
         endelse  ; end of last/middle shell if
       endfor ;end pathway loop

       ;print the ionisation potentials for 1s shell
       if z0 eq 5 and iz eq 1 then begin
          if im eq 0 then begin
             ;print,'1s direct',enorbiz[0],mocc[0]

             findexcount=findexcount+1
             cs0q='"f'+strtrim(string(findexcount),2)+'"'
             lfq=strlen(cs0q)
             fq=strmid(cs0q,1,lfq-2)
             directformula[findexcount-1]=fq
             pathindex[findexcount-1]=ip
             cs1='1s direct'
             cs4=',,,,,,,,,,,,'
             cs3=string(enorbiz[0])+','+string(mocc[0])
             cs5=string(1.00000)

             ;print on the output file
             printf,lout,',,'+cs0q+','+'"'+cs1+'",,'+cs3+','+cs4+strtrim(cs5,2)
          endif else begin

             ;print,'1s +auto',enorbiz[0]-enorbiz[1],mocc[0]

             findexcount=findexcount+1
             cs0r='"f'+strtrim(string(findexcount),2)+'"'
             lfr=strlen(cs0r)
             fr=strmid(cs0r,1,lfr-2)
             autoformula[findexcount-1]=fr
             termautoformula[findexcount-1]=''
             spinautoformula[findexcount-1]=-1
             cs1='1s +auto'
             cs4=',,,,,,,,,,,,'
             cs3=string(enorbiz[0]-enorbiz[1])+','+string(mocc[0])
             cs5=string(1.00000)

             ;print on the output file
             printf,lout,',,'+cs0r+','+'"'+cs1+'",,'+cs3+','+cs4+strtrim(cs5,2)

             ;print,'1s direct',enorbiz[0],mocc[0]

             findexcount=findexcount+1
             cs0s='"f'+strtrim(string(findexcount),2)+'"'
             lfs=strlen(cs0s)
             fs=strmid(cs0s,1,lfs-2)
             directformula[findexcount-1]=fs
             pathindex[findexcount-1]=ip
             cs1='1s direct'
             cs4=',,,,,,,,,,,,'
             cs3=string(enorbiz[0])+','+string(mocc[0])
             cs5=string(1.00000)

             ;print on the output file
             printf,lout,',,'+cs0s+','+'"'+cs1+'",,'+cs3+','+cs4+strtrim(cs5,2)
          endelse
       endif

       if (z0 gt 5 and z0-iz ne 3 and z0-iz ne 2 and z0-iz ne 1) or $
          (z0 eq 5 and iz eq 0) then begin
          ;print,'1s +auto',enorbiz[0]-enorbiz[1],mocc[0]

          findexcount=findexcount+1
          cs0t='"f'+strtrim(string(findexcount),2)+'"'
          lft=strlen(cs0t)
          ft=strmid(cs0t,1,lft-2)
          autoformula[findexcount-1]=ft
          termautoformula[findexcount-1]=''
          spinautoformula[findexcount-1]=-1
          cs1='1s +auto'
          cs4=',,,,,,,,,,,,'
          cs3=string(enorbiz[0]-enorbiz[1])+','+string(mocc[0])
          cs5=string(1.00000)

          ;print on the output file
          printf,lout,',,'+cs0t+','+'"'+cs1+'",,'+cs3+','+cs4+strtrim(cs5,2)

          ;print,'1s direct',enorbiz[0],mocc[0]

          findexcount=findexcount+1
          cs0u='"f'+strtrim(string(findexcount),2)+'"'
          lfu=strlen(cs0u)
          fu=strmid(cs0u,1,lfu-2)
          directformula[findexcount-1]=fu
          pathindex[findexcount-1]=ip
          cs1='1s direct'
          cs4=',,,,,,,,,,,,'
          cs3=string(enorbiz[0])+','+string(mocc[0])
          cs5=string(1.00000)

          ;print, on the output file
          printf,lout,',,'+cs0u+','+'"'+cs1+'",,'+cs3+','+cs4+strtrim(cs5,2)
       endif

       ;find the total and build up the formulas
       print,metiz[im], ' total'
       for oo=0,naam1-1 do print,'# ',metiz[im],'->',metiz1[paam12[oo]]

       iaf=where(autoformula ne '')
       idf=where(directformula ne '')

       ;for gg=0,n_elements(idf)-1 do print,pathindex[idf[gg]],' ',directformula[idf[gg]]
       ;print,'metiz1 index ',pathindex[idf[paam1]]

       ;numberof elements of the auto-part of the formula
       niaf=n_elements(iaf)
       autoformulat=strarr(niaf)

       if iaf[0] eq -1 then begin
          autoft=''
       endif else begin
          autoformulat[0]='+'+autoformula[iaf[0]]
          for ia=1,niaf-1 do begin
            autoformulat[ia]='+'+autoformula[iaf[ia]]
            autoformulat[ia]=autoformulat[ia-1]+autoformulat[ia]
          endfor
          autoft=autoformulat[niaf-1]
       endelse

       formula='"('+directformula[idf[0]]+autoft+')*ratio"'
       ;print,formula

       ;built formulas for metastables
       formulamet=strarr(naam1)
       autoformulamet=strarr(naam1)
       minusdirectmet=strarr(naam1)
       spinautoformet=strarr(naam1)
       fspinautoformet=strarr(naam1)
       spinsystmet1=spin_met1[paam12]
       termmet1=term_met1[paam12]
       nsuniq=n_elements(fix(uniq(spinsystmet1,sort(spinsystmet1))))

       if nsuniq eq naam1 then begin
          wspinsystmet1=spinsystmet1/total(spinsystmet1)
       endif else begin
          if naam1 eq 2 then begin
             xxweight,termmet1,lpartformula,spartformula,wdum,wspinsystmet1
          endif else begin
             wspinsystmet1=fltarr(naam1)
             unspinsystmet1=spinsystmet1(uniq(spinsystmet1,sort(spinsystmet1)))
             iwspinsystmet1=unspinsystmet1/total(unspinsystmet1)

             ;if two metastable iz1 terms have the same spin system,
             ;the proper statistical weight for the two met. must be calculated

             ;identify the terms with same spinsystem
             niu=intarr(nsuniq)
             for iu=0,nsuniq-1 do begin
               ua=where(unspinsystmet1[iu] eq spinsystmet1)
               niu[iu]=n_elements(ua)
             endfor
             sua=where(niu gt 1)

             if n_elements(sua) gt 1 then begin
                message,'*** ERROR: NO MORE THAT ONE SPIN SYSTEM CAN HAVE MULTIPLE TERMS ***'
             endif

             multspinterm=unspinsystmet1[sua]
             indtermmultspin=where(multspinterm[0] eq spinsystmet1)


             xxweight,termmet1[indtermmultspin],lpartformula,spartformula,wdum,scfacformula

             for dd=0,n_elements(indtermmultspin)-1 do begin
               wspinsystmet1[indtermmultspin[dd]]=iwspinsystmet1[sua]*scfacformula[dd]
             endfor

             wiw=where(wspinsystmet1 ne wspinsystmet1[niu])

             if wiw[0] ne -1 then begin
               wspinsystmet1[wiw]=iwspinsystmet1(where(iwspinsystmet1 ne iwspinsystmet1[sua[0]]))
             endif
          endelse
       endelse

       naam1a=naam1-1

       ;case of more than one metastable
       if naam1a gt 0 then begin
          autoformulamet0=strarr(naam1a)
          minusdirectmet0=strarr(naam1a)
          iiafa=idf[paam1]-1
          iafa=iiafa[1:naam1a]
          autoformulamet0[0]='+'+autoformula[iafa[0]]
          minusdirectmet0[0]='-'+directformula[idf[paam1[1]]]
          if naam1a gt 1 then begin
             for iamet=1,naam1a-1 do begin
               autoformulamet0[iamet]='+'+autoformula[iafa[iamet]]
               autoformulamet0[iamet]=autoformulamet0[iamet-1]+autoformulamet0[iamet]
               minusdirectmet0[iamet]='-'+directformula[idf[paam1[iamet+1]]]
               minusdirectmet0[iamet]=minusdirectmet0[iamet-1]+minusdirectmet0[iamet]
              endfor
          endif

          ;auto+direct for the first metastable
          autoformulamet[0]=autoformulamet0[naam1a-1]
          minusdirectmet[0]=minusdirectmet0[naam1a-1]

          metautf=autoformula[iafa]
          nys=n_elements(metautf)

          ;max number of metastables
          maxmetnum1=4
          if nys eq maxmetnum1 then ys=where(autoformula ne metautf[0]$
                                         and autoformula ne metautf[1]$
                                         and autoformula ne metautf[2]$
                                         and autoformula ne metautf[3])
          if nys eq maxmetnum1-1 then ys=where(autoformula ne metautf[0]$
                                         and autoformula ne metautf[1]$
                                         and autoformula ne metautf[2])
          if nys eq maxmetnum1-2 then ys=where(autoformula ne metautf[0]$
                                         and autoformula ne metautf[1])
          if nys eq maxmetnum1-3 then ys=where(autoformula ne metautf[0])

          ;statistical weights
          ispinautoformet0=strarr(naam1)
          termautoformet0=strarr(naam1)
          spinint=intarr(naam1)

          for oo=0,naam1-1 do begin
            ispinautoformet=autoformula(where(strmid(termautoformula[iaf[paam1[oo]]],0,1) eq $
                                              strmid(termautoformula,0,1) and $
                                              termautoformula ne ''))
            itermautoformula=termautoformula(where(strmid(termautoformula[iaf[paam1[oo]]],0,1) eq $
                                              strmid(termautoformula,0,1) and $
                                              termautoformula ne ''))

            ;for t=0,n_elements(ispinautoformet)-1 do print,oo,'   ',$
            ;             ispinautoformet[t],'   ',itermautoformula[t]

            niof=n_elements(ispinautoformet)
            spinauto=strarr(niof)

            eio0=where(ispinautoformet[0] eq autoformula[iafa])
            if eio0[0] eq -1 then begin
               spinauto[0]='+'+ispinautoformet[0]
            endif else begin
               spinauto[0]=''
            endelse
            for io=1,niof-1 do begin
              eio=where(ispinautoformet[io] eq autoformula[iafa])
              if eio[0] eq -1 then begin
                 spinauto[io]='+'+ispinautoformet[io]
              endif else begin
                 spinauto[io]=''
              endelse
              spinauto[io]=spinauto[io-1]+spinauto[io]
            endfor
            spinautoformet[oo]=strtrim(spinauto[niof-1],2)
            spinint[oo]=fix(strmid(itermautoformula[0],0,1))
          endfor

          spinintmet1=intarr(naam1)
          spintermmet1=strarr(naam1)
          nspinautoformet=strarr(naam1)
          finalform=strarr(naam1)
          nspinint=intarr(naam1)
          saa=intarr(naam1)
          for oo=0,naam1-1 do begin
            spinintmet1[oo]=fix(strmid(term_met1[paam12[oo]],0,1))
            spintermmet1[oo]=term_met1[paam12[oo]]
          endfor

          nisaa=intarr(naam1)
          for oo=0,naam1-1 do begin
            isaa=where(spinint eq spinintmet1[oo])
            nisaa1=n_elements(isaa)
            nisaa[oo]=nisaa1
            if nisaa1 gt 1 then begin
               isaa2=isaa
               saa[oo]=-1
            endif else begin
               saa[oo]=isaa
            endelse
          endfor
          msaa=where(saa eq -1)
          if msaa[0] ne -1 then begin
             saa[msaa]=isaa2
             xxweight,spintermmet1[msaa],lsaa,ssaa,wdum,scfsaa
          endif
          nspinautoformet=spinautoformet[saa]
          nspinint=spinint[saa]

          ;for oo=0,naam1-1 do print,nspinautoformet[oo],nspinint[oo],spinintmet1[oo]

          if naam1 eq n_elements(msaa) and msaa[0] ne -1 then begin
             for oo=0,naam1-1 do begin
               lennsp=strlen(nspinautoformet[oo])
               ilencount=0
               for ilen=0,lennsp-1 do begin
                 inplus=where(strmid(nspinautoformet[oo],ilen,1) eq '+')
                 if inplus[0] ne -1 then ilencount=ilencount+1
               endfor
               nplus=ilencount
               pplus=intarr(nplus)
               pform=intarr(nplus)
               lform=intarr(nplus)
               dform=strarr(nplus)
               pplus[0]=strpos(nspinautoformet[oo],'+')
               pform[0]=pplus[0]+1
               for ippl=1,nplus-1 do begin
                 pplus[ippl]=strpos(strmid(nspinautoformet[oo],pplus[ippl-1]+1,lennsp-1),'+')+1+pplus[ippl-1]
                 pform[ippl]=pplus[ippl]+1
               endfor
               for ippl=0,nplus-1 do begin
                 if ippl ne nplus-1 then begin
                    lform[ippl]=pplus[ippl+1]-pform[ippl]
                 endif else begin
                    lform[ippl]=lennsp-pform[ippl]
                 endelse
               endfor
               for ippl=0,nplus-1 do begin
                 idform=strmid(nspinautoformet[oo],pform[ippl],lform[ippl])
                 dform[ippl]=strtrim(string(scfsaa[oo]),2)+'*'+idform
               endfor
               fform=strarr(nplus)
               fform[0]='+'+dform[0]

               for ippl=1,nplus-1 do begin
                 fform[ippl]='+'+dform[ippl]
                 fform[ippl]=fform[ippl-1]+fform[ippl]
               endfor
               finalform[oo]=fform[nplus-1]
               fspinautoformet[oo]=finalform[oo]
             endfor
          endif else begin
             for oo=0,naam1-1 do begin
               fspinautoformet[oo]=nspinautoformet[oo]
               if msaa[0] ne -1 then begin
                  for ii=0,n_elements(msaa)-1 do begin
                    if term_met1[oo] eq spintermmet1[msaa[ii]] then begin
                       lennsp=strlen(nspinautoformet[oo])
                       ilencount=0
                       for ilen=0,lennsp-1 do begin
                         inplus=where(strmid(nspinautoformet[oo],ilen,1) eq '+')
                         if inplus[0] ne -1 then ilencount=ilencount+1
                       endfor
                       nplus=ilencount
                       pplus=intarr(nplus)
                       pform=intarr(nplus)
                       lform=intarr(nplus)
                       dform=strarr(nplus)
                       pplus[0]=strpos(nspinautoformet[oo],'+')
                       pform[0]=pplus[0]+1
                       for ippl=1,nplus-1 do begin
                         pplus[ippl]=strpos(strmid(nspinautoformet[oo],pplus[ippl-1]+1,lennsp-1),'+')+1+pplus[ippl-1]
                         pform[ippl]=pplus[ippl]+1
                       endfor
                       for ippl=0,nplus-1 do begin
                         if ippl ne nplus-1 then begin
                            lform[ippl]=pplus[ippl+1]-pform[ippl]
                         endif else begin
                            lform[ippl]=lennsp-pform[ippl]
                         endelse
                       endfor
                       for ippl=0,nplus-1 do begin
                         idform=strmid(nspinautoformet[oo],pform[ippl],lform[ippl])
                         dform[ippl]=strtrim(string(scfsaa[ii]),2)+'*'+idform
                       endfor
                       fform=strarr(nplus)
                       fform[0]='+'+dform[0]

                       for ippl=1,nplus-1 do begin
                         fform[ippl]='+'+dform[ippl]
                         fform[ippl]=fform[ippl-1]+fform[ippl]
                       endfor
                       finalform[oo]=fform[nplus-1]
                       fspinautoformet[oo]=finalform[oo]

                    endif
                  endfor
               endif
             endfor
          endelse

         ;for oo=0,naam1-1 do print,term_met1[oo],'   ',fspinautoformet[oo]

       endif

       for oo=0,naam1-1 do begin
         if z0-iz ne 3 and z0-iz ne 2 and z0-iz ne 1 then begin
            iformulamet=directformula[idf[paam1[oo]]]+autoformulamet[oo]+$
            minusdirectmet[oo]+fspinautoformet[oo]+'+'$
            +strtrim(string(wspinsystmet1[oo]),2)+'*'+autoformula[iaf[niaf-1]]
         endif else begin
            iformulamet=directformula[idf[paam1[oo]]]+autoformulamet[oo]+$
            minusdirectmet[oo]+fspinautoformet[oo]
         endelse
         formulamet[oo]='"('+iformulamet+')*ratio"'
       endfor

       ;print, on the output file
       printf,lout,',,,"'+string(metiz[im])+' total"'+','+formula+',,,,,,,,,,,,,,,'

       if naam1 eq 1 then begin
          printf,lout,','+'"#",,"'+string(metiz[im])+'->'+string(metiz1[paam12[0]])+$
                    '",'+formula+',,,,,,,,,,,,,,,'
       endif else begin
          for oo=0,naam1-1 do printf,lout,','+'"#",,"'+$
            string(metiz[im])+'->'+string(metiz1[paam12[oo]])+'",'+$
            formulamet[oo]+',,,,,,,,,,,,,,,'
       endelse

       newnpath[im]=naam1+1+nicount+2



     endfor  ;end metastable loop
     print,''
     print,'       ',strtrim(string(i),2),' pathway file: ',strtrim(filepath[i],2)
     print,''
     print,'       *****'
     print,'       ',strupcase(elem)+'+'+strtrim(string(iz),2), '   COMPLETED'
     print,'       *****'
     wait,1
  endelse  ;end of bare if
endfor ;end of file loop

free_lun,lout

THEEND:

end

