;----------------------------------------------------------------------
;
; PROJECT    : ADAS
;
; NAME       : pathways_adf07
;
; PURPOSE    : given the element and the ionisation stage (of the ionising ion),
;              the routine provides the possible pathways and the associated
;              energies (in Rydberg) for the terms which are found in the
;              adf04 data file (otherwise it puts -1). Also it gives the orbital
;              energy (in Rydberg), taken from the adf04s when
;              available there or calculate using Cowan.
;
; INPUT      : elem      string     element
;              filez     string     adf04 data file for the ionising ion
;              filez1    string     adf04 data file for the ionised ion
;              iz        integer    ionisation stage of the ionising ion
;              elem      string     name of element
;              pathfile  string     (optional) name of pathway file
;                                   default name is:
;                                     pathways_<elem><iz>_<elem><iz1>.dat
;                                   (e.g. pathways_si1_si2.dat).
;
; KEYWORD    : noadf04             if the keywork noadf04 is active
;                                  the routine build up the pathways
;                                  without associating their energies
;
; EXAMPLE    : Examples of how to run:
;              IDL> elem='b'
;              IDL> iz=1
;              IDL> filez='/home/adas/adas/adf04/belike/belike_nrb03#b1.dat'
;              IDL> filez1='/home/adas/adas/adf04/lilike/lilike_dvb02#b2.dat'
;              IDL> pathways_adf07,elem=elem,iz=iz,filez,filez1
;              or if the keywork noadf04 is used:
;              IDL> elem='b'
;              IDL> iz=1
;              IDL> pathways_adf07,elem=elem,iz=iz,/noadf04
;
; NOTES      : the adf04s must be in LS resolution and
;              their configurations must be in the form
;              of Eissner or Standard
;
; CALLS      : xxeiz0.pro, xxseqid.pro, metastable.pro,
;              config_orbital_energies.pro, read_adf04.pro, checkconfig.pro, xxterms.pro
;
; AUTHOR     : Alessandra Giunta
;
; DATE       : 14-05-2012
;
;
; MODIFIED   : Alessandra Giunta
;       1.1
;              - First version.
;
; VERSION:
;       1.1    14-05-2012
;
;----------------------------------------------------------------------
;
pro pathways_adf07, filez, filez1,      $
                    elem     = elem,    $
                    iz       = iz,      $
                    noadf04  = noadf04, $
                    pathfile = pathfile


if n_elements(pathfile) EQ 0 then pfile = strcompress('pathways_'+elem+string(iz)+'_'+elem+string(iz+1)+'.dat', /remove_all) $
                             else pfile = pathfile


;conversion factor from cm-1 to Rydberg
cm2ryd=109737.26

;return the isoelectronic sequence
elem=strtrim(elem,2)
z0=xxeiz0(elem)
iz=fix(iz)
iz1=iz+1
;for ionising ion
seq=strtrim(xxseqid(z0,iz),2)
;for ionised ion
if z0 eq iz1 then begin    ;case of bare nucleus (like H+)
   seq1='bare'
endif else begin
   seq1=strtrim(xxseqid(z0,iz1),2)
endelse

;open the file pathways.dat, which will be filled
; with energy orbital and pathways

store_pth = ''

;write the heading on file 'pathways.dat'
store_pth = [store_pth, strupcase(elem)+'+'+strtrim(string(iz),2)]

;find metastable for each isoelectronic sequence
;for ionising ion
metastable, seq=seq, config=config_met,term=term_met,spin=spin_met
nc_met=n_elements(config_met)
;for ionised ion
if z0 eq iz1 then begin    ;case of bare nucleus (like H+)
   config_met1=['1s0']
   term_met1=['1S']
   spin_met1=[1]
endif else begin
   metastable, seq=seq1, config=config_met1,term=term_met1,spin=spin_met1
endelse
nc_met1=n_elements(config_met1)

;find orbital energy for the ionising ion
config_orbital_energies,z0_nuc=z0,z_ion=iz,config=config_met[nc_met-1],$
       energy=energy

;write the orbital energy on file 'pathways.dat'
store_pth = [store_pth, string(strupcase(elem)+'+'+strtrim(string(iz),2)+' Metastable Number:',nc_met,format='(a24,i4)')]
store_pth = [store_pth, string(strupcase(elem)+'+'+strtrim(string(iz1),2)+' Metastable Number:',nc_met1,format='(a24,i4)')]

wconfig=config_met[nc_met-1]
wconfig=strlowcase(wconfig)
wconfig=strtrim(wconfig,2)+' '
wlconf=fix(strlen(wconfig))
wnorb=(wlconf+1)/4
worb=strarr(wnorb)
for i=0,wnorb-1 do begin
  worb[i]=strmid(wconfig,i*4,2)
endfor

;check noadf04 keyword to allow the use of the program without the adf04 input
if not keyword_set(noadf04) then begin

   print,'---  Read adf04s  ---'

   ;Call the adf04 for ionising and ionised ions, if requested
   ;read the adf04 for ionising ion
   read_adf04,file=filez,te=te,fulldata=fdiz
   ;check iz
   if fdiz.iz0 ne z0 and fdiz.iz ne iz then begin
      message,'*** ERROR: wrong adf04 for ionising ion ***'
   endif
   ionpotiz=fdiz.bwno
   cstrgaiz=fdiz.cstrga
   ;isa=2*S+1
   isaiz=fdiz.isa
   ;L
   ilaiz=fdiz.ila
   ;energy
   waiz=fdiz.wa
   neladf04iz=n_elements(fdiz.ia)
   eorbiz=fdiz.eorb
   if eorbiz[0] eq -1 then begin
      eorbiz=fltarr(wnorb)
      eorbiz=energy
   endif

   ;check configuration and in case build up a complete configuration:
   ;e.g. for Si+2 (Mg-like) 3p2 --> 1s2 2s2 2p6 3p2
   
;   checkconfig,cstrgaiz,neladf04iz,fdiz.iz0,fdiz.iz,cfgadf04iz
   cfgadf04iz = strarr(neladf04iz)
   for v = 0, neladf04iz-1 do $
      cfgadf04iz[v] = occ2cfg(cfg2occ(strlowcase(cstrgaiz[v]),fdiz.iz0,fdiz.iz))
   
   ltermsadf04iz=strarr(neladf04iz)
   for v=0,neladf04iz-1 do begin
     vv=xxlvals(cut=ilaiz[v])
     nvv=n_elements(vv)
     ltermsadf04iz[v]=strtrim(cfgadf04iz[v],2)+strtrim(string(isaiz[v]),2)+strtrim(strupcase(vv[nvv-1]),2)
   endfor


  ;read the adf04 for ionised ion
   if iz1 eq z0 then begin
   ;case of bare (so no adf04for iz1)
      ionpotiz1=0.
      cstrgaiz1=['1s0']
      ;isa=2*S+1
      isaiz1=[1]
      ;L
      ilaiz1=[0]
      ;energy
      waiz1=[0.]
      neladf04iz1=n_elements(waiz1)
      ltermsadf04iz1=['1s01S']

   endif else begin

      read_adf04,file=filez1,te=te,fulldata=fdiz1
      ;check iz1
      if fdiz1.iz0 ne z0 and fdiz1.iz ne iz1 then begin
        message,'*** ERROR: wrong adf04 for ionised ion ***'
      endif
      ionpotiz1=fdiz1.bwno
      cstrgaiz1=fdiz1.cstrga
      ;isa=2*S+1
      isaiz1=fdiz1.isa
      ;L
      ilaiz1=fdiz1.ila
      ;energy
      waiz1=fdiz1.wa
      neladf04iz1=n_elements(fdiz1.ia)

      ;check configuration and in case build up a complete configuration:
      ;e.g. for Si+2 (Mg-like) 3p2 --> 1s2 2s2 2p6 3p2

;      checkconfig,cstrgaiz1,neladf04iz1,fdiz1.iz0,fdiz1.iz,cfgadf04iz1
      cfgadf04iz1 = strarr(neladf04iz1)
      for v = 0, neladf04iz1-1 do $
           cfgadf04iz1[v] = occ2cfg(cfg2occ(strlowcase(cstrgaiz1[v]),fdiz1.iz0,fdiz1.iz))
           
      ltermsadf04iz1=strarr(neladf04iz1)
      for v1=0,neladf04iz1-1 do begin
        vv1=xxlvals(cut=ilaiz1[v1])
        nvv1=n_elements(vv1)
        ltermsadf04iz1[v1]=strtrim(cfgadf04iz1[v1],2)+strtrim(string(isaiz1[v1]),2)+strtrim(strupcase(vv1[nvv1-1]),2)
      endfor
   endelse
      ;if adf04 is selected then use orbital energy from there but the
      ;number of orbital used are only the one present in the
      ;configuration of the last metastable
      store_pth = [store_pth, string('Orbital Number:',wnorb,format='(a24,i4)')]
      store_pth = [store_pth, '----------------------------------']
      store_pth = [store_pth, 'Orbital      Energy(Ryd)']
      store_pth = [store_pth, '----------------------------------']
      for i=0,wnorb-1 do store_pth = [store_pth, string(worb[i],eorbiz[i],format='(a2,12x,f10.3)')]
      ;-------------------------------------------------------

endif else begin
   ;if adf04 is not selected then use orbital energy calculated by config_orbital_energies
   store_pth = [store_pth, string('Orbital Number:',wnorb,format='(a24,i4)')]
   store_pth = [store_pth, '----------------------------------']
   store_pth = [store_pth, 'Orbital      Energy(Ryd)']
   store_pth = [store_pth, '----------------------------------']
   for i=0,wnorb-1 do store_pth = [store_pth, string(worb[i],energy[i],format='(a2,12x,f10.3)')]
   ;-------------------------------------------------------

endelse



;Find the pathways

print,'--- Find pathways ---'

;print,'Transition: ',strupcase(elem)+'+'+strtrim(string(iz),2)+' -> '$
;      +strupcase(elem)+'+'+strtrim(string(iz1),2)


;write the pathway heading
store_pth = [store_pth, '']
store_pth = [store_pth, strupcase(elem)+'+'+strtrim(string(iz),2)+' -> '$
      +strupcase(elem)+'+'+strtrim(string(iz1),2)]
store_pth = [store_pth, '---------------------------------------------------------------------------------------------------------------------------------']
store_pth = [store_pth, string('Pathways','I.P.(cm-1)','I.P.(Ryd)',format='(a8,90x,2a15)')]
store_pth = [store_pth, '---------------------------------------------------------------------------------------------------------------------------------']


for i=0,nc_met-1 do begin
  config_orbital_energies,z0_nuc=z0,z_ion=iz,config=config_met[i],$
  n=n,l=l,q=q

  nq=n_elements(q)
  llet=strarr(nq)
  for ll=0,nq-1 do begin
    illet=xxlvals(cut=l[ll])
    nillet=n_elements(illet)
    llet[ll]=strtrim(illet[nillet-1],2)
  endfor
  if nq lt 3 then begin
     nnq=nq
  endif else begin
     nnq=nq-1
  endelse
  configpath=strarr(nnq)
  shellrem=strarr(nnq)
  qpath=intarr(nnq,nq)
  iqpath=intarr(nq)
  narrtermpath=1000
  CHANGEARR:
  termpath=strarr(nnq,narrtermpath)
  Ltotpath=intarr(nnq,narrtermpath)
  Stotpath=fltarr(nnq,narrtermpath)

  energy=fltarr(nnq,narrtermpath)
  energy_ryd=fltarr(nnq,narrtermpath)


  if narrtermpath ne 1000 and nq ge 3 then goto,CONTARR
  if narrtermpath ne 1000 and nq lt 3 then goto,CONTARRHHELI

  ;write the metastable terms of ionising ion
  store_pth = [store_pth, string(config_met[i],term_met[i],'->',format='(a40,a5,a5)')]

  ;starts to remove an electron from 2s except for H-like, He-like and Li-ke (1s instead)
  if nq lt 3 then begin
    inq=0
  endif else begin
    inq=1
  endelse

  if nq ge 3 then begin

    for j=inq,nq-1 do begin
         shellrem[j-1]=strtrim(string(n[j]),2)+strtrim(llet[j],2)
      for iq=0,nq-1 do begin
        if strtrim(string(n[iq]),2)+strtrim(llet[iq],2) eq shellrem[j-1] then begin
           iqpath[iq]=q[iq]-1
        endif else begin
           iqpath[iq]=q[iq]
        endelse
      endfor
      qpath[j-1,*]=iqpath
      configpath[j-1]=occ2cfg(iqpath)
      xxterms,configpath[j-1],terms,Ltot,Stot

      narrtermpath=n_elements(terms)

      ;check possible pathways
      sterm_met=float(strmid(strtrim(term_met[i],2),0,1))
      stotterm_met=(sterm_met-1.)/2.
      lterm_met=strlowcase(fix(strmid(strtrim(term_met[i],2),0,1)))

      icount=0
      for tt=0,narrtermpath-1 do begin
        if Stot[tt] eq abs(stotterm_met + 0.5) or Stot[tt] eq abs(stotterm_met - 0.5) $
           then begin
             icount=icount+1
        endif
      endfor

      if icount eq 0 then begin
         message,'*** ERROR: no pathways available ***'
      endif

      ntermsnew=icount

      itt=where(Stot eq abs(stotterm_met + 0.5) or Stot eq abs(stotterm_met - 0.5))


      narrtermpath=ntermsnew
      if narrtermpath ne 1000 then goto,CHANGEARR
      CONTARR:
      termpath[j-1,*]=terms[itt]
      Ltotpath[j-1,*]=Ltot[itt]
      Stotpath[j-1,*]=Stot[itt]

      ;assign energy I-E1+E2 to each pathway
      if not keyword_set(noadf04) then begin
         stringmet=strtrim(config_met[i],2)+strtrim(term_met[i],2)
         part1=where(ltermsadf04iz eq stringmet)
         if part1 eq -1 then begin
            message,'*** ERROR: NO ENERGY FOR METASTABLE '+stringmet+ 'IN THE ADF04 USED ***'
         endif
         en1part=ionpotiz-waiz(part1)

         entotp1p2=fltarr(narrtermpath)
         entotp1p2_ryd=fltarr(narrtermpath)
         for p2=0,narrtermpath-1 do begin
           stringpart2=strtrim(configpath[j-1],2)+strtrim(termpath[j-1,p2],2)
           part2=where(ltermsadf04iz1 eq stringpart2)
           if part2[0] ne -1 then begin

           ;case of seniority (max six terms)
              if n_elements(part2) ge 1 then begin
                  iii=0
                 for iiio=0,n_elements(part2)-1 do begin
                   for iiif=0,n_elements(entotp1p2)-1 do begin
                      if where(float(entotp1p2[iiif]) eq float(en1part+waiz1(part2[iiio]))) ne -1 then begin
                         iii=iii+1
                      endif
                   endfor
                 endfor

                 if iii eq 0 then begin
                     iii=0
                 endif else begin
                     iii=iii-1
                 endelse

                 en1wa=float(en1part+waiz1(part2[iii]))
                 aaelpart2=where(float(entotp1p2) eq en1wa[0])

                 if aaelpart2 ne -1 then begin
                    naaelpart2=iii+1
                    entotp1p2[p2]=en1part+waiz1(part2[naaelpart2])

                 endif else begin
                    entotp1p2[p2]=en1part+waiz1(part2[0])
                 endelse

              endif else begin
                 entotp1p2[p2]=en1part+waiz1(part2[0])
              endelse
              entotp1p2_ryd[p2]=entotp1p2[p2]/cm2ryd
           endif else begin
              entotp1p2[p2]=-1.
              entotp1p2_ryd[p2]=-1.
           endelse
         endfor

         energy[j-1,*]=entotp1p2
         energy_ryd[j-1,*]=entotp1p2_ryd


         print,'removing an electron from '+shellrem[j-1]

        ;write the pathways with the associate energy where
        ;possible on file 'pathways.dat'
         for nntt=0,narrtermpath-1 do begin
           store_pth = [store_pth, string(shellrem[j-1],configpath[j-1],termpath[j-1,nntt],$
           energy[j-1,nntt],energy_ryd[j-1,nntt],format='(a55,a40,a4,f15.1,f15.5)')  ]
         endfor

     endif else begin

         print,'removing an electron from '+shellrem[j-1]

         for nntt=0,narrtermpath-1 do begin
           store_pth = [store_pth, string(shellrem[j-1],configpath[j-1],termpath[j-1,nntt],$
           format='(a55,a40,a4)')  ]
         endfor
     endelse
    endfor

  endif else begin
    ;case of H-like, He-like and Li-like where we need
    ;to remove electrons also from 1
    for j=0,nq-1 do begin
         shellrem[j]=strtrim(string(n[j]),2)+strtrim(llet[j],2)
      for iq=0,nq-1 do begin
        if strtrim(string(n[iq]),2)+strtrim(llet[iq],2) eq shellrem[j] then begin
           iqpath[iq]=q[iq]-1
        endif else begin
           iqpath[iq]=q[iq]
        endelse
      endfor
      qpath[j,*]=iqpath

      if nq eq 1 and iqpath[0] eq 0 then begin
         configpath[j]=['1s0']
         terms=['1S']
         Ltot=[0]
         Stot=[0.]
      endif else begin
         configpath[j]=occ2cfg(iqpath)
         xxterms,configpath[j],terms,Ltot,Stot
      endelse

      narrtermpath=n_elements(terms)

      ;check possible pathways
      sterm_met=float(strmid(strtrim(term_met[i],2),0,1))
      stotterm_met=(sterm_met-1.)/2.
      lterm_met=strlowcase(fix(strmid(strtrim(term_met[i],2),0,1)))

      icount=0
      for tt=0,narrtermpath-1 do begin
        if Stot[tt] eq abs(stotterm_met + 0.5) or Stot[tt] eq abs(stotterm_met - 0.5) $
           then begin
             icount=icount+1
        endif
      endfor

      if icount eq 0 then begin
         message,'*** ERROR: no pathways availableg ***'
      endif

      ntermsnew=icount

      itt=where(Stot eq abs(stotterm_met + 0.5) or Stot eq abs(stotterm_met - 0.5))

      narrtermpath=ntermsnew
      if narrtermpath ne 1000 then goto,CHANGEARR
      CONTARRHHELI:
      termpath[j,*]=terms[itt]
      Ltotpath[j,*]=Ltot[itt]
      Stotpath[j,*]=Stot[itt]

      ;assign energy I-E1+E2 to each pathway
      if not keyword_set(noadf04) then begin
         stringmet=strtrim(config_met[i],2)+strtrim(term_met[i],2)
         part1=where(ltermsadf04iz eq stringmet)
         if part1 eq -1 then begin
            message,'*** ERROR: NO ENERGY FOR METASTABLE '+stringmet+ 'IN THE ADF04 USED ***'
         endif
         en1part=ionpotiz-waiz(part1)

         entotp1p2=fltarr(narrtermpath)
         entotp1p2_ryd=fltarr(narrtermpath)
         for p2=0,narrtermpath-1 do begin
           stringpart2=strtrim(configpath[j],2)+strtrim(termpath[j,p2],2)
           part2=where(ltermsadf04iz1 eq stringpart2)
           if part2[0] ne -1 then begin

              ;case of seniority
              if n_elements(part2) ge 1 then begin

                  iii=0
                 for iiio=0,n_elements(part2)-1 do begin
                   for iiif=0,n_elements(entotp1p2)-1 do begin
                      if where(float(entotp1p2[iiif]) eq float(en1part+waiz1(part2[iiio]))) ne -1 then begin
                         iii=iii+1
                      endif
                   endfor
                 endfor

                 if iii eq 0 then begin
                     iii=0
                 endif else begin
                     iii=iii-1
                 endelse

                 en1wa=float(en1part+waiz1(part2[iii]))
                 aaelpart2=where(float(entotp1p2) eq en1wa[0])

                 if aaelpart2 ne -1 then begin
                    naaelpart2=iii+1
                    entotp1p2[p2]=en1part+waiz1(part2[naaelpart2])

                 endif else begin
                    entotp1p2[p2]=en1part+waiz1(part2[0])
                 endelse

              endif else begin
                 entotp1p2[p2]=en1part+waiz1(part2[0])
              endelse
              entotp1p2_ryd[p2]=entotp1p2[p2]/cm2ryd

           endif else begin
              entotp1p2[p2]=-1
              entotp1p2_ryd[p2]=-1
           endelse

         endfor

         energy[j,*]=entotp1p2
         energy_ryd[j,*]=entotp1p2_ryd

         print,'removing an electron from '+shellrem[j]

        ;write the pathways with the associate energy where
        ;possible on file 'pathways.dat'
         for nntt=0,narrtermpath-1 do begin
           store_pth = [store_pth, string(shellrem[j],configpath[j],termpath[j,nntt],$
           energy[j,nntt],energy_ryd[j,nntt],format='(a55,a40,a4,f15.1,f15.5)')]
         endfor

      endif else begin
         print,'removing an electron from '+shellrem[j]
         for nntt=0,narrtermpath-1 do begin
           store_pth = [store_pth, string(shellrem[j],configpath[j],termpath[j,nntt],$
           format='(a55,a40,a4)')]
         endfor

      endelse
    endfor

  endelse

endfor

store_pth = [store_pth, '---------------------------------------------------------------------------------------------------------------------------------']

store_pth = store_pth[1:*]

; Write the pathways file

nmet=fix(strmid(store_pth[1],24,4))
norb=fix(strmid(store_pth[3],24,4))

ind = where(strpos(store_pth, '->') EQ 48, count)
ind = [ind, n_elements(store_pth)-1] - ind[0]

store_num = ind[1:*]

nobj  = nmet+2
nstr1 = norb+12

nstr2i=intarr(nmet)
nstr2=intarr(nmet)

for i=0,nmet-1 do nstr2i[i]=store_num[i]

for i=0,nmet-1 do begin
  if i eq 0 then begin
     nstr2[i]=nstr2i[i]-(i+1)
  endif else begin
     nstr2[i]=nstr2i[i]-(i+1)-(nstr2i[i-1]-((i-1)+1))
  endelse

endfor


str=''
openw, lunw1,pfile,/get_lun

for i=0,3 do  printf,lunw1,store_pth[i]

printf,lunw1,'Pathway Numbers:',nstr2,format='(a24,'+strtrim(string(nmet),2)+'i4)'

for i=0,nstr1-4-1 do printf,lunw1,store_pth[i+4]

ipt = nstr1-1
metstr=strarr(nmet)
for m=0,nmet-1 do begin
  ipt = ipt+1
  metstr[m]=store_pth[ipt]
  cmdc='entstrc_'+strtrim(string(m),2)+'=strarr(nstr2['+strtrim(string(m),2)+'])'
  resc=execute(cmdc)
  for mm=0, nstr2[m]-1 do begin
    ipt = ipt+1
    str=store_pth[ipt]
    cmdc1='entstrc_'+strtrim(string(m),2)+'['+strtrim(string(mm),2)+"]='"+str+"'"
    resc1=execute(cmdc1)
  endfor
  cmdc2='entstrc_'+strtrim(string(m),2)+'=reverse(entstrc_'+strtrim(string(m),2)+')'
  resc2=execute(cmdc2)
endfor

laststr=store_pth[n_elements(store_pth)-1]
for m=0,nmet-1 do begin
  printf,lunw1,metstr[m]
  for mm=0, nstr2[m]-1 do begin
   cmdc3='printf,lunw1,entstrc_'+strtrim(string(m),2)+'['+strtrim(string(mm),2)+']'
   resc3=execute(cmdc3)
  endfor
endfor
printf,lunw1,laststr

free_lun,lunw1

end
