;----------------------------------------------------------------------
;+
; PROJECT    :  ADAS
;
; NAME       :  xxdatm_02
;
; PURPOSE    :  Returns the data in an mdf02 dataset as an IDL structure.
;
; ARGUMENTS  :  xxdatm_02, file=file, fulldata=fulldata [, /help]
;
;               NAME      I/O    TYPE       DETAILS
; REQUIRED   :  file       I     string     mdf02 filename
;               fulldata   O     structure  contents of mdf02 file
;
; KEYWORDS      help       I     Display help entry
;
;
; The fulldata structure contains:
;
;   msymb      string     :  Molecular symbol
;   atnum      long       :  Number of atoms in the molecule
;   atsymb     string     :  Atomic symbols in the molecule
;   ind_s      long       :  Max. index of species that is different molecules atoms and their ions
;   symbs()    string     :  Array of the symbols for each ind_s
;                              1st dim. number of species 
;   ch_ion()   string     :  Ionisation channels
;                              1st dim. number of species
;   bwno_i()   double     :  Ionisation energy (measured from lowest electronic state minimum) (cm^-1) 
;                              1st dim. number of species
;   ch_dis(,)  string     :  Dissociation channels
;                              1st dim. number of species, 
;                              2nd dim. number of diss. channel
;   bwno_d()   double     :  Dissociation energy (measured from lowest electronic state minimum) (cm^-1)
;                              1st dim. number of species, 
;                              2nd dim. number of diss. channel
;   ndis()     long       :  Number of dissociation channels per specie
;                              1st dim. number of species
;   wno(,)     string     :  Bound energy of states
;                          ( measured from lowest electronic state  minimum) (cm^-1)
;                              1st dim. specie
;                              2nd dim. el. state
;   wnod(,)    string     :  Asymptotic energy of electronic states 
;                          ( measured from lowest electronic state minimum) (cm^-1)  
;                              1st dim. specie
;                              2nd dim. el. state
;   ind_p      long       :  Max, index of electron impact processes between 
;                            different species atoms and their ions
;   index_p()  log        :  Index of process
;                              1st dim. number of processes
;   path_p()   string     :  Path of the different process in ind_p
;                              1st dim. number of processes
;   desc_p()   string     :  Description of the different process in ind_p
;                              1st dim. number of processes
;   ind_e()    long       :  Max. index of electronic state for each ind_s
;                              1st dim. number of species, 
;   econua()   string     :  Electronic configuration in the united atoms limit
;                              1st dim. specie
;                              2nd dim. el. state
;   econsa()   string     :  Electronic configuration in the separated atoms limit
;                              1st dim. specie
;                              2nd dim. el. state
;   coupsta()  string     :  Coupled molecular state electronic configuration
;                               1st dim. specie
;                               2nd dim. el. state
;   wt(,)      double     :  Statistical weight 
;                               1st dim. specie
;                               2nd dim. el. state
;   rparam(,)  string     :  Parameters for each reaction values
;                               1st dim. el. state
;                               2nd dim. specie
;   parinfo    structure  :  Split the descriptions for each process into
;                            arrays - see below
;   ind_r      long       :  Max. number of reactions
;   in_e(,)    long       :  Index of electronic state for each ind_s
;                               1st dim. species index
;                               2nd dim. el. index
;   tparam(,)  double     :  Temperatures array for data
;                               1st dim. process index, 
;                               2nd dim. number of energies
;   sparam(,)  double     :  Cross section or rates data array 
;                               1st dim. process index, 
;                               2nd dim. number of energies
;   parval(,)  double     :  Parameters array for fitting formula
;                               1st dim. process index, 
;                               2nd dim. number of parameters
;   eparam()   double     :  Transition energy parameter
;                               1st dim. process index, 
;
;   parinfo = {s_in          :   incoming channel state
;              e_in          :                    electronic level
;              v_in          :                    vibrational level
;              process_index :   index of process           
;              s_out         :   outgoing channel state
;              e_out         :                    electronic level   
;              v_out         :                    vibrational level  
;              diss_channel  :   dissassociation channel 
;              categ         :   category of collision
;                                 1 => collision strength (X and omg tabulation)
;                                 2 => cross-section (erel(eV/amu) and sig(cm^2))
;                                 3 => Maxwell averaged collision strength
;                                 4 => Maxwell averaged rate coefficient (cm^3/s)
;              tcode         :   electronic transition sub-category
;                                 1 = dipole
;                                 2 = non-dipole, non-spin change
;                                 3 = spin change
;                                 4 = other (ambiguous)
;              formula       :   formula number for evaluation
;              num_param     :   number of fitting parameters
;              num_x         :   number of temperature or energies
;              delta_e       :   tansition energy
;              description   :   string description
;
;
; NOTES      :  Calls fortran code.
;
;
; AUTHOR     :  Martin O'Mullane
;
; DATE       :  11/10/12
;
;
; MODIFIED:
;       1.1     Martin O'Mullane
;              	- First version.
;
;
; VERSION:
;       1.1    11/10/12
;
;-
;----------------------------------------------------------------------

PRO xxdatm_02, file=file, fulldata=fulldata, help=help

; If asked for help

if keyword_set(help) then begin
   doc_library, 'xxdatm_02'
   return
endif

; Location of compiled routine

fortdir = getenv('ADASFORT')

; Check that the mdf02 file exists

file_acc, file, exist, read, write, execute, filetype
if exist ne 1 then message, 'mdf02 file does not exist ' + file
if read ne 1 then message, 'mdf02 file cannot be read from this userid ' + file


; Define types of the parameters

MNE   = 60
NCEST = 5000
MAXPR = 70
MNME  = 50
MNS   = 20
MDP   = 20
MNC   = 5

atnum   = -1L
in_e    = lonarr(mns,mne)
ind_e   = lonarr(mns)
ind_p   = -1L
index_p = lonarr(maxpr)
ind_r   = -1L
ind_s   = -1L
ndis    = lonarr(mns)

str = string(replicate(32B, 128))
strput, str, file
by_dsnin = long(byte(str))

bwno_d = dblarr(mns,mnc)
bwno_i = dblarr(mns)
eparam = dblarr(ncest)
parval = dblarr(ncest,mdp)
sparam = dblarr(ncest,mnme)
tparam = dblarr(ncest,mnme)
wno    = dblarr(mns,mne)
wnod   = dblarr(mns,mne)
wt     = dblarr(mns,mne)

str = string(replicate(32B, 2))
by_atsymb = long(byte(str))

str = string(replicate(32B, 7))
by_ch_dis = lonarr(MNS,MNC,7) + transpose(rebin(long(byte(str)),7,MNS,MNC))

str = string(replicate(32B, 10))
by_ch_ion = lonarr(MNS,10) + transpose(rebin(long(byte(str)),10,MNS))

str = string(replicate(32B, 145))
by_cmmt = lonarr(100,145) + transpose(rebin(long(byte(str)),145,100))

str = string(replicate(32B, 10))
by_coupsta = lonarr(MNS,MNE,10) + transpose(rebin(long(byte(str)),10,MNS,MNE))

str = string(replicate(32B, 40))
by_desc_p = lonarr(MAXPR,40) + transpose(rebin(long(byte(str)),40,MAXPR))

str = string(replicate(32B, 4))
by_econsa = lonarr(MNS,MNE,4) + transpose(rebin(long(byte(str)),4,MNS,MNE))

str = string(replicate(32B, 8))
by_econua = lonarr(MNS,MNE,8) + transpose(rebin(long(byte(str)),8,MNS,MNE))

str = string(replicate(32B, 10))
by_msymb = long(byte(str))

str = string(replicate(32B, 60))
by_path_p = lonarr(MAXPR,60) + transpose(rebin(long(byte(str)),60,MAXPR))

str = string(replicate(32B, 15))
by_rparam = lonarr(NCEST,MNS,15) + transpose(rebin(long(byte(str)),15,NCEST,MNS))

str = string(replicate(32B, 10))
by_symbs = lonarr(MNS,10) + transpose(rebin(long(byte(str)),10,MNS))


dummy = CALL_EXTERNAL(fortdir+'/xxdatm_02_if.so','xxdatm_02_if',    $
                      atnum, in_e, ind_e, ind_p, index_p, ind_r,    $
                      ind_s, by_dsnin, ndis, bwno_d, bwno_i,        $
                      eparam, parval, sparam, tparam, wno,          $
                      wnod, wt, by_atsymb, by_ch_dis, by_ch_ion,    $
                      by_cmmt, by_coupsta, by_desc_p, by_econsa,    $
                      by_econua, by_msymb, by_path_p, by_rparam,    $
                      by_symbs)

atsymb = string(byte(by_atsymb))

ch_dis = strarr(MNS,MNC)
for j = 0, MNS-1 do begin
   for i = 0, MNC-1 do begin
      ch_dis[j,i] = string(byte(reform(by_ch_dis[j,i,*])))
   endfor
endfor

ch_ion = strarr(MNS)
for j = 0, MNS-1 do begin
   ch_ion[j] = string(byte(reform(by_ch_ion[j,*])))
endfor

cmmt = strarr(100)
for j = 0, 100-1 do begin
   cmmt[j] = string(byte(reform(by_cmmt[j,*])))
endfor

coupsta = strarr(MNS,MNE)
for j = 0, MNS-1 do begin
   for i = 0, MNE-1 do begin
      coupsta[j,i] = string(byte(reform(by_coupsta[j,i,*])))
   endfor
endfor

desc_p = strarr(MAXPR)
for j = 0, MAXPR-1 do begin
   desc_p[j] = string(byte(reform(by_desc_p[j,*])))
endfor

econsa = strarr(MNS,MNE)
for j = 0, MNS-1 do begin
   for i = 0, MNE-1 do begin
      econsa[j,i] = string(byte(reform(by_econsa[j,i,*])))
   endfor
endfor

econua = strarr(MNS,MNE)
for j = 0, MNS-1 do begin
   for i = 0, MNE-1 do begin
      econua[j,i] = string(byte(reform(by_econua[j,i,*])))
   endfor
endfor

msymb = string(byte(by_msymb))

path_p = strarr(MAXPR)
for j = 0, MAXPR-1 do begin
   path_p[j] = string(byte(reform(by_path_p[j,*])))
endfor

rparam = strarr(NCEST,MNS)
for j = 0, NCEST-1 do begin
   for i = 0, MNS-1 do begin
      rparam[j,i] = string(byte(reform(by_rparam[j,i,*])))
   endfor
endfor

symbs = strarr(MNS)
for j = 0, MNS-1 do begin
   symbs[j] = string(byte(reform(by_symbs[j,*])))
endfor


; Return the results in a structure
;
; find max number of processes, energy/temperatures and parameters
; break out rparam into usable arrays

tmp = strtrim(reform(rparam[*,3]))
ind = where(strpos(tmp, '')  EQ 0, num)

nstates = max(ind_e)

if num NE ind_r then message, 'Incosistenct in mdf02 file ' + file

s_in          = long(reform(rparam[ind, 0]))
e_in          = long(reform(rparam[ind, 1]))
v_in          = reform(rparam[ind, 2])
process_index = long(reform(rparam[ind, 3]))
s_out         = long(reform(rparam[ind, 4]))
e_out         = long(reform(rparam[ind, 5]))
v_out         = reform(rparam[ind, 6])
diss_channel  = reform(rparam[ind, 7])
categ         = long(strmid(reform(rparam[ind, 8]),6))
tcode         = long(strmid(reform(rparam[ind, 9]),6))
formula       = long(strmid(reform(rparam[ind, 10]),5))
num_param     = long(strmid(reform(rparam[ind, 11]),4))
num_x         = long(strmid(reform(rparam[ind, 12]),6))
delta_e       = double(strmid(reform(rparam[ind, 13]),3))
description   = reform(rparam[ind, 14])


parinfo = {s_in          :  s_in           ,$         
           e_in          :  e_in           ,$         
           v_in          :  v_in           ,$         
           process_index :  process_index  ,$
           s_out         :  s_out          ,$        
           e_out         :  e_out          ,$        
           v_out         :  v_out          ,$        
           diss_channel  :  diss_channel   ,$ 
           categ         :  categ          ,$        
           tcode         :  tcode          ,$        
           formula       :  formula        ,$      
           num_param     :  num_param      ,$    
           num_x         :  num_x          ,$        
           delta_e       :  delta_e        ,$      
           description   :  description	    }

nx = max(num_x)
np = max(num_param)    

fulldata = { msymb   :  msymb,                           $
             atnum   :  atnum,                           $
             atsymb  :  atsymb,                          $
             ind_s   :  ind_s,                           $
             symbs   :  symbs[0:ind_s-1],                $
             ch_ion  :  ch_ion[0:ind_s-1],               $
             bwno_i  :  bwno_i[0:ind_s-1],               $
             ch_dis  :  ch_dis[0:ind_s-1, *],            $
             bwno_d  :  bwno_d[0:ind_s-1, *],            $
             ndis    :  ndis[0:ind_s-1],                 $
             wno     :  wno[0:ind_s-1, 0:nstates-1],     $
             wnod    :  wnod[0:ind_s-1, 0:nstates-1],    $
             ind_p   :  ind_p,                           $
             index_p :  index_p[0:ind_p-1],              $
             path_p  :  path_p,                          $
             desc_p  :  desc_p,                          $
             ind_e   :  ind_e[0:ind_s-1],                $
             econua  :  econua[0:ind_s-1, 0:nstates-1],  $
             econsa  :  econsa[0:ind_s-1, 0:nstates-1],  $
             coupsta :  coupsta[0:ind_s-1, 0:nstates-1], $
             wt      :  wt[0:ind_s-1, 0:nstates-1],      $
             rparam  :  rparam[ind,*],                   $
             parinfo :  parinfo,                         $
             ind_r   :  ind_r,                           $
             in_e    :  in_e[0:ind_s-1, 0:nstates-1],    $
             tparam  :  tparam[ind,0:nx-1],              $
             sparam  :  sparam[ind,0:nx-1],              $
             parval  :  parval[ind,0:np-1],              $
             eparam  :  eparam[ind]                      }

END
