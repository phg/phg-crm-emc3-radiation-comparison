;----------------------------------------------------------------------
;+
; PROJECT    :  ADAS
;
; NAME       :  xxdatm_00
;
; PURPOSE    :  Returns the data in an mdf02 dataset as an IDL structure.
;
; ARGUMENTS  :  xxdatm_00, file=file, fulldata=fulldata [, /help]
;
;               NAME      I/O    TYPE       DETAILS
; REQUIRED   :  file       I     string     mdf02 filename
;               fulldata   O     structure  contents of mdf02 file
;
; KEYWORDS      help       I     Display help entry
;
;
; There are two fulldata structures:
;    (1) A-value and Frank-Condon files:
;
;        fulldata = { species_l : lower state specie index
;                     state_l   :             electronic index
;                     max_vib_l :             highest quantum number
;                                             of bound vibrational levels
;                     species_u : upper state electronic index
;                     state_u   :             specie index
;                     max_vib_u :             highest quantum number
;                                             of bound vib
;                     data(,)   : FCF or A-value (vib_lower, vib_upper)
;
;    (2) Vibrational energies files:
;
;        fulldata = { species_l : lower state specie
;                     state_l   :             state
;                     ind_vib() : vibrational level index
;                     energy()  : energy (eV)
;                     l_over()  : = 0, bound level
;                                 = 1, double well state or a
;                                      state above the barrier
;
;
; NOTES      :
;
;
; AUTHOR     :  Martin O'Mullane
;
; DATE       :  11/10/12
;
;
; MODIFIED:
;       1.1     Martin O'Mullane
;               - First version.
;
;
; VERSION:
;       1.1    11/10/12
;
;-
;----------------------------------------------------------------------

PRO xxdatm_00, file=file, fulldata=fulldata, help=help


; If asked for help

if keyword_set(help) then begin
   doc_library, 'xxdatm_00'
   return
endif


; Check that the mdf00 file exists

file_acc, file, exist, read, write, execute, filetype
if exist ne 1 then message, 'mdf00 file does not exist ' + file
if read ne 1 then message, 'mdf00 file cannot be read from this userid ' + file


; Determine type (and size of data) from first line

adas_readfile, file=file, all=all

ind = strpos(strlowcase(all[0]), 'max_vib_l')

if ind NE -1 then begin

   str = strlowcase(strcompress(all[0], /remove_all))
   pts = strsplit(str, '=/', /extract)

   i1 = where(strpos(pts, 'max_vib_l') NE -1)
   max_vib_l = long(pts[i1+1])
   i1 = where(strpos(pts, 'max_vib_u') NE -1)
   max_vib_u = long(pts[i1+1])
   i1 = where(strpos(pts, 'species_l') NE -1)
   species_l = long(pts[i1+1])
   i1 = where(strpos(pts, 'state_l') NE -1)
   state_l = long(pts[i1+1])
   i1 = where(strpos(pts, 'species_u') NE -1)
   species_u = long(pts[i1+1])
   i1 = where(strpos(pts, 'state_u') NE -1)
   state_u = long(pts[i1+1])

   data = dblarr(max_vib_l+1, max_vib_u+1)

   reads, all[1:max_vib_u+1], data

   fulldata = { state_l   : state_l[0],    $
                species_l : species_l[0],  $
                max_vib_l : max_vib_l[0],  $
                state_u   : state_u[0],    $
                species_u : species_u[0],  $
                max_vib_u : max_vib_u[0],  $
                data      : data           }

endif else begin

   str = strlowcase(strcompress(all[0], /remove_all))
   pts = strsplit(str, '=/', /extract)

   i1 = where(strpos(pts, 'species_l') NE -1)
   species_l = long(pts[i1+1])
   i1 = where(strpos(pts, 'state_l') NE -1)
   state_l = long(pts[i1+1])

   i1  = where(strpos(all, '  -1') NE -1)
   num = i1 - 2

   vib    = lonarr(num)
   energy = dblarr(num)
   above  = intarr(num)

   idum = -1
   fdum = 0.0D0
   sdum = ' '

   for j = 0, num[0]-1 do begin

      str = all[j+2]
      i1  = strpos(str, '*')
      if i1 NE -1 then  above[j]  = 1

      reads, str, idum, fdum
      vib[j]    = idum
      energy[j] = fdum

   endfor

   fulldata = { species_l : species_l[0],  $
                state_l   : state_l[0],    $
                ind_vib   : vib,           $
                energy    : energy,        $
                l_over    : above          }

endelse

END
