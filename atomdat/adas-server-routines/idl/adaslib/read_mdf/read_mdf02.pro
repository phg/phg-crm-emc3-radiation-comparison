;----------------------------------------------------------------------
;+
; PROJECT    :  ADAS
;
; NAME       :  read_mdf02
;
; PURPOSE    :  Interrogrates the data in an mdf02 dataset.
;
; ARGUMENTS  :  read_mdf02, file=file, species_in=1, elect_in=1, vib_in='0',      $
;                           process=12, species_out=1, elect_out=18, vib_out='T', $
;                           energy=energy, data=data
;
;               NAME       I/O    TYPE       DETAILS
;            :  file        I     string     mdf02 filename
;               fulldata    O     structure  contents of mdf02 file - subroutine
;                                            will terminate and return
;               species_in  I     int        incoming channel species
;               elect_in    I     int                         electronic level
;               vib_in      I     str                         vibrational level
;               species_out I     int        outgoing channel species
;               elect_out   I     int                         electronic level
;               vib_out     I     str                         vibrational level
;               process     I     int        index of process
;               energy      I     double()   Energies (eV) or Temperatures (eV)
;               data        O     double()   Interpolated output data
;                                            cross sections (cm2), rate coefficients(cm3/s)
;                                            collision strengths or effective collision
;                                            strength according to what is stored
;               xtitle      O     string     Energy/Te as in file
;               ytitle      O     string     rate/cross-section/gamma/upsilon as in file
;
; KEYWORDS      help       I     Display help entry
;
; NOTES      :  the vibration level can be a numbered level, 'S' for summed over discrete states,
;               'D' for a dissociative cross section or 'T' for a total cross section.
;
;               The list of possible processes is
;                    1     e+H_2(^+q)(N,v)  -> e + H_2(+q)*(N',v')                     excitation
;                   11     e+H_2(N,v) -> e + H_2*(N',v')                               vib resolved exc.
;                   12     e+H_2(N) -> e + H_2*(N)                                     vib unresolved exc.
;                   13     e+H_2(N,v) -> e + H_2*(N,v**) -> e + H(1s) + H(nl)          diss. exc.
;                   14     e+H_2(v) -> H_2^-(X(2)S(+)(u),B(2)S(+)(u))-> e + H_2(v')    vib exc via e attach
;                   15                                                                 other excitation processes
;                    2     e+H_2+ -> H_2                                               recombination
;                   21     H+H_2+(N,v) -> H+ + H_2(N',v')                              vib resolved inverse CX
;                   22     H+H_2+(N)   -> H+ + H_2(N)                                  vib unresolved inverse CX
;                   23     e+H_2+(N,v) -> H_2(N',v')                                   vib resolved recombination
;                   24     e+H_2+(N) -> H_2(N')                                        vib unresolved recombination
;                   25     e+H_2+ -> H_2** -> H + H                                    dissociative recombination
;                    3     e+H_2(N,v) -> e + e + H_2+(2)                               ionization
;                   31     e+H_2(N,v) -> e + e + H_2+(N',v')                           vib resolved ion.
;                   32     e+H_2(N)   -> e + e + H_2+(N')                              vib unresolved ion.
;                   33     e+H_2(N,v) -> e + e + H_2+(N',v'**) ->  e + H + H+          dissociative ion.
;                   34     H_2(N**,v**) -> H_2+ (N',v') + e                            autoionizacion
;                   35                                                                 other ionization processes
;                    4     e+H_2(N,v)  ->(e + H_2(d)* ->)e + H(1s) + H(nl)             dissociation ( or exc. to dissociative)
;                   41     e+H_2(N,v)  -> e + H_2(d)* -> e + H(1s) + H(nl)             vib resolved diss.
;                   42     e+H_2(N)  -> e + H_2(d)* -> e + H(1s) + H(nl)               vib unresolved diss.
;                   43     e+H_2(N;v) -> H_2-(S(+)(u/g) -> H- + H(1s)                  dis. el. attachment and H- production
;                   44     e+H_2(X;v) -> H_2-(X/B(2)S(+)(u/g) -> e + H(1s) + H(1s)     dis. el. attachment X branch
;                   45     e+H_2(X;v) -> H_2-(B(2)S(+)(u/g) -> e + H(1s) + H(1s)       dis. el. attachment B branch
;                   46                                                                 other dissociation processes
;                    5     H^+ + H_2 -> H(1s) + H_2^+                                  direct CX
;                   51     H^+ + H_2(N,v) -> H(1s) + H_2^+(N',v')                      vib resolved direct CX
;                   52     H^+ + H_2(N) -> H(1s) + H_2^+(N')                           vib unresolved direct CX
;                   53     H^+ + H_2(N,v) -> H(1s) + H + H^+                           dissiociative CX
;                   54     H^+ + H_2(v) -> H(1s) + H^+ + H^+ + e                       transfer ion.
;                   55                                                                 other CX processes;
;                    6     H2^+ H                                                      heavy particle impact
;                   61     H2(N,v) + H+ -> H2(N',v') + H+                              excitation by ion impact
;                   62     H2^+(N,v) + H -> H2^+(N',v') + H                            excitation by neutral impact
;                   63     H2(N,v) + H+ -> H + H                                       dissociation by ion impact
;                   64     H2+(N,v) + H -> H+ + H                                      dissociation by neutral impact
;                   65     H+ + H_2(N,v) -> H+ + H_2+(N',v')                           ionization by ion impact
;                   66     H + H_2+(N,v) -> H + (H_2++)                                ionization by neutral impact
;                   67     H+ +H_2(N,v) -> H+ + H_2+(N',v'**) ->  H + H + H+           dissociative ion. by ion impact
;                   68     H+H_2(N,v) -> H + H_2+(N',v'**) ->  H + H + H+              dissociative ion. by neutral impact
;                   69                                                                 others
;
;
; AUTHOR     :  Martin O'Mullane
;
; DATE       :  11/10/12
;
;
; MODIFIED:
;       1.1     Martin O'Mullane
;              	- First version.
;
;
; VERSION:
;       1.1    11/10/12
;
;-
;----------------------------------------------------------------------

PRO read_mdf02, file        = file,        $
                fulldata    = fulldata,    $
                species_in  = species_in,  $
                elect_in    = elect_in,    $
                vib_in      = vib_in,      $
                species_out = species_out, $
                elect_out   = elect_out,   $
                vib_out     = vib_out,     $
                process     = process,     $
                energy      = energy,      $
                data        = data,        $
                xtitle      = xtitle,      $
                ytitle      = ytitle,      $
                help        = help


; If asked for help

if keyword_set(help) then begin
   doc_library, 'read_mdf02'
   return
endif

; file name

if n_elements(file) eq 0 then message, 'A file name must be passed'
file_acc, file, exist, read, write, execute, filetype
if exist ne 1 then message, 'mdf02 file does not exist ' + file
if read ne 1 then message, 'mdf02 file cannot be read from this userid ' + file


; Read in all the data

xxdatm_02, file = file, fulldata = fulldata

; If all of the data required put it in fulldata and return

if arg_present(fulldata) then return


; Find data and parameters

index = string(fulldata.parinfo.s_in, format='(i2)') +             $
        string(fulldata.parinfo.e_in, format='(i2)') +             $
        string(strtrim(fulldata.parinfo.v_in,2), format='(a3)') +  $
        string(fulldata.parinfo.s_out, format='(i2)') +            $
        string(fulldata.parinfo.e_out, format='(i2)') +            $
        string(strtrim(fulldata.parinfo.v_out,2), format='(a3)') + $
        string(fulldata.parinfo.process_index, format='(i3)')


seek  = string(species_in, format='(i2)') +         $
        string(elect_in, format='(i2)') +           $
        string(strtrim(vib_in,2), format='(a3)') +  $
        string(species_out, format='(i2)') +        $
        string(elect_out, format='(i2)') +          $
        string(strtrim(vib_out,2), format='(a3)') + $
        string(process, format='(i3)')

ind = where(strpos(index, seek) NE -1, count)

if count GT 1 then begin
   print, 'Duplicate entries for '
   print, string(ind)
   message, 'Ambiguous request'
endif

ind = ind[0]

; Check if selection is valis

if ind EQ -1 then message, 'Invalid selection'

; Evaluate requested process

formula = fulldata.parinfo.formula[ind]
delta_e = fulldata.parinfo.delta_e[ind]
weight  = fulldata.wt[species_in-1, elect_in-1]
i1      = fulldata.parinfo.num_param[ind]
if i1 GT 0 then param = fulldata.parval[ind, 0:i1-1] $
           else param = -1

if formula EQ 201 then begin

   if fulldata.parinfo.categ[ind] EQ 3 then begin
      rate = 0
      xtitle = 'Te (eV)'
      ytitle = 'Effective collision strength'
   endif else begin
      rate = 1
      xtitle = 'Te (eV)'
      ytitle = 'Rate coefficient (cm3/s)'
   endelse

   adas9xx_evaluate_formula, formula = formula, $
                             delta_e = delta_e, $
                             weight  = weight,  $
                             param   = param,   $
                             energy  = energy,  $
                             result  = data,    $
                             rate    = rate

endif else begin

   if fulldata.parinfo.categ[ind] EQ 1 then begin
      sigma = 0
      xtitle = 'Energy (eV)'
      ytitle = 'Collision strength'
   endif else begin
      sigma = 1
      xtitle = 'Energy (eV)'
      ytitle = 'Cross section (cm2)'
   endelse

   adas9xx_evaluate_formula, formula = formula, $
                             delta_e = delta_e, $
                             weight  = weight,  $
                             param   = param,   $
                             energy  = energy,  $
                             result  = data,    $
                             sigma   = sigma

endelse

END
