;+
; NAME:
; ffs_contain__define.pro
;
; PURPOSE:
; ffs component container object
;
; CATEGORY:
; FFS - Framework for Feature Synthesis (Object oriented model/fitting)
;
; CALLING SEQUENCE:
; .r ffs_contain__define.pro
; then run procedure 'test'
;
; ARGUMENTS:
;
; KEYWORDS:
;
; MODIFICATION HISTORY:
; 15-10-2007 CHN
;  * repaired 'get' method - name matching was done incorrectly. 
; 14/08/2008 CHN
;  * removed little test routine - pointless.
;  * removed unused keyword no_destroy in 'remove' method.
;  * added destroy keyword to remove method and implemented action to take if
;    set.   
;----------------------------------------------------------------------
;
; First version distributed with ADAS.
;
; MODIFIED   :
;       1.1     Martin O'Mullane
;                 - First central ADAS version.
;
; VERSION    :
;       1.1    04-01-2013
;
;----------------------------------------------------------------------
;-

FUNCTION ffs_contain::get, $
  ALL=all, $
  ISA=isa, $
  POSITION=position, $
  COUNT=count, $
  NAME=name

  ;; Loop through objlist and find matching indices
  IF N_ELEMENTS(name) GT 0 THEN BEGIN
    position = -1
    FOR i=0L, N_ELEMENTS(name)-1 DO BEGIN
       nameToFind = STRTRIM(name[i], 2)
       nameList = self->getNames()
       result  = LONARR(N_ELEMENTS(nameList))
       FOR j=0L, N_ELEMENTS(nameList)-1 DO result[j] = STRCMP(nameList[j], nameToFind, /FOLD_CASE)
       matches = WHERE(result EQ 1, count)
       IF count GT 0 THEN position = [ position, matches ] ELSE RETURN, OBJ_NEW()
       ;print, 'trying to find ', nametofind, ' in [',self->getNames(),']'
    ENDFOR
    position = position[1:*]
  ENDIF  
  
  RETURN, self->IDL_Container::Get (ALL=all, ISA=isa, POSITION=position, COUNT=count ) 
END


FUNCTION ffs_contain::getNames
   items = self->get(/all, count=count)
   IF count GT 0 THEN BEGIN
     names = strarr(count)
     FOR i=0, count-1 DO names[i] = items[i]->getName()
     RETURN, names
   ENDIF ELSE RETURN, -1
END


FUNCTION ffs_contain::nameToRef, $
  name
  
  listOfNames = STRLOWCASE(self->getNames())
  indices = WHERE(listOfNames EQ STRLOWCASE(name))
  RETURN, self->get(POSITION=indices)
END


FUNCTION ffs_contain::add, $
  objects, $
  POSITION=position
  
  self->idl_container::add, objects, $
    POSITION=position
  RETURN, 1
END

FUNCTION ffs_contain::remove, $
  objRef,    $
  name = name,     $
  position = position, $
  All = all,   $
  destroy=destroy
  
  if keyword_set(destroy) then begin
    elements = self->get(name=name,  position=position, all=all, count=count)
    for i=0, count-1 do if obj_valid(elements[i]) then obj_destroy, elements[i]
  endif
  
  
  IF KEYWORD_SET(all) THEN BEGIN
    self->idl_container::remove, /all
    RETURN, 1
  ENDIF ELSE IF N_ELEMENTS(position) EQ 1 THEN BEGIN
    self->idl_container::remove, position=position
    RETURN, 1
  ENDIF  
  
  IF N_Elements(name) GT 0 THEN objRef = self->nameToRef(name)
  
  IF N_Elements(position) GT 0 THEN objRef = self->idl_container::get(position)  
    
  self->idl_container::remove, objRef
  RETURN, 1
  
END

PRO ffs_contain::cleanup
  ;should this delete all objects contained?
  self->idl_container::cleanup
END


FUNCTION ffs_contain::init, $
  items
  IF self->idl_container::init() EQ 1 THEN BEGIN
    
    FOR i=0, N_ELEMENTS(items)-1 DO ok = self->add(items[i])
    RETURN,1 
  ENDIF ELSE RETURN, 0
END


PRO ffs_contain__define
    self= { ffs_contain, $
            inherits idl_container $
          }
END
