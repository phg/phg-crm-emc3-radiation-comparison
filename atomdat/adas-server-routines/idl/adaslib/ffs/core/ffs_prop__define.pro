;+
; PROJECT:
;  FFS - Framework for Feature Synthesis (Object oriented model/fitting)
;
; NAME:
;  FFS_PROP
;
; PURPOSE:
;  A 'property' of a spectral feature that will not vary as part of a fitting procedure.
;  Not to be confused with an element parameter that has been set fixed.
;
; EXPLANATION:
;  An ffs_prop can have a value, for example, that is a string containing a filename 
;  from which archived data is to be read. This obviously is important in performing 
;  feature generation and so, is a required pseudo parameter of a model element, but
;  is NOT a fitting parameter. 
; 
; USE:
;  Inherit this class as you would any other.
;
; INITIALISATION SYNTAX:
;  prop_obj = obj_new('ffs_prop', name='prop_name', value=1000.0, /debug)
;
;  PARAMETERS:
;   None.
;  KEYWORDS:
;   name    -	a string value for the name of this property.
;   value   -	the 'value' associated with this property, could be of any data type.
;   debug   -	set this keyword to 1 for diagnostic information to be printed to the terminal.
;
; PUBLIC METHODS:
;  In addtion to the methods listed below, this object inherits methods from ffs_primitive
;   - refer to this class' documentation for more details.
;
;  [setname]
;    PURPOSE:
;      Sets property name.
;    INPUTS:
;      String - the name to give to the property.
;    OUTPUTS:
;      Integer - Returns 1 if successful.
;                Returns 0 if unsuccessful.
;    SIDE EFFECTS:
;      None
;  [getname]
;    PURPOSE:
;      Returns the property's name.
;    INPUTS:
;      None.
;    OUTPUTS:
;      String - property name.
;    SIDE EFFECTS:
;      None
;  [setvalue]
;      Sets property value.
;    INPUTS:
;      Any data type.
;    OUTPUTS:
;      Integer - Returns 1 if successful.
;                Returns 0 if unsuccessful.
;    SIDE EFFECTS:
;      None
;  [getvalue]
;    PURPOSE:
;      Returns the property's value.
;    INPUTS:
;      None.
;    OUTPUTS:
;      Unknown - returns whatever data the value pointer refers to.
;
; CALLS:
;   None.
;
; SIDE EFFECTS:
;   None.
;	
; AUTHOR:
;   Christopher Nicholas, University of Strathclyde
;
; VERSION HISTORY:
;   1.1 CHN 11/09/2007
;   	* First version
;   1.2 ADW 20/12/2007
;   	* Fixed some documentation
;    	* Changed setvalue from a procedure to a function.
;   1.3 CHN 20/12/2007
;   	* Class now inherits from ffs_primitive - replacement for cn_primitive
;       * Re-write of methods to become functions rather than procedures
;   	  - setname
;   	  - setproperty
;         - getproperty
;   	* Added debug keyword to init and passed through to ffs_primitive.
;   	* Modified documentation further to adhere to new style.
;         - removed most of modification history documentation header as it was not 
;           actually for this file!
;     	  - removed 'calling sequence', 'arguments', 'keywords'
;     	  - modified 'purpose'
;     	  - added 'project', 'explanation', 'use', 'initialisation syntax', 'public methods'
;   1.4 CHN 20/12/2007
;   	* Neglected to include 'initialisation syntax' changes in last update. Amended.
;----------------------------------------------------------------------
;
; First version distributed with ADAS.
;
; MODIFIED   :
;       1.1     Martin O'Mullane
;                 - First central ADAS version.
;
; VERSION    :
;       1.1    04-01-2013
;
;----------------------------------------------------------------------
;-

;+
;name:
;setname
;validates the input keyword parname then sets the parname
;-
function ffs_prop::setname, name
  if size(name, /type) ne 7 then return, self->seterrmsg('invalid ffs_prop name - must be string.')
  self.name = name
  return, 1
end

;+
;name:
;getname
;returns self.name
;-
function ffs_prop::getname
   return, self.name
end


;+
;name:
;setvalue
;-
function ffs_prop::setvalue, value
  if ptr_valid(self->getvalueptr()) then ptr_free, self->getvalueptr()
  self.value = ptr_new(value)
  return, 1
end


;+
;name:
;getvalueptr
;returns self.value
;-
function ffs_prop::getvalueptr
  return, self.value
end

;+
;name:
;getvalue
;returns *self.value
;-
function ffs_prop::getvalue
     return, *self.value
end


;+
;name:
;setproperty
;this method sets specific object values via keywords.
;the set keywords call the applicable individual methods.
;-
function ffs_prop::setproperty, $
  name = name, $ 
  value = value
  
  failstring='Failed while setting parameter attributes - '
  
  if n_elements(name) gt 0 && self->setname(name) eq 0 then return,self->seterrmsg(failstring+self->geterrmsg())
  if n_elements(value) gt 0 && self->setvalue(value) eq 0 then return,self->seterrmsg(failstring+self->geterrmsg())  
  
  return, 1
end

;+
;name:
;getproperty
;this method extracts specific object values and returns them to the
;user. normally, the user should use the getstate() method, or more
;specific, getvalue(), to retrieve the object state in a usable form.
;-

function ffs_prop::getproperty, $
  name=name, $
  value=value

  if keyword_set(name) then result=self->getname()
  if keyword_set(value) then result=self->getvalue()
  if n_elements(result) gt 0 then return, result else return, !values.d_nan 

end


;+
;name:
;cleanup
;calls superclass ffs_primitive cleanup method
;-
pro ffs_prop::cleanup
  if ptr_valid(self.value) then ptr_free, self.value
  self->ffs_primitive::cleanup
end


;+
;name:
;init
;initialise object. checks any passed keyword values for initialisation.
;for any set keyword, the relevant set* method is called.
;-
function ffs_prop::init, $
  name = name, $
  value = value, $
  debug=debug

  if self->ffs_primitive::init(debug=debug) ne 1 then return, 0     
  if self->setproperty(name=name, value=value)eq 0 then return, 0

  return, 1
end


;+
;NAME:
;define
;standard class definition, inherits ffs_primitive
;-
PRO ffs_prop__define
                      
  self = {ffs_prop, $
          name:'', $  
          value:PTR_NEW(), $          
	  INHERITS ffs_primitive}  
				 
END


