;+
; PROJECT:
;       FFS
;
; NAME:
;	FFS_PARSER
;
; PURPOSE:
;	Parse an FFS model definition to give a list of required
;	elements and their names. Also provides method to evaluate
;	the result of the whole model.
;
; EXPLANATION:
;	This object parses a model string in a lisp-style format
;	such as:
;		
;	(model my model name	
;		(+
;			(gaussian (adas-zeeman hfs) broaden-hfs)
;			(gaussian (adas-zeeman lfs) broaden-lfs)
;			(gaussian (+ (line c-impurity1)
;				     (line c-impurity2) 
;						) c-broaden)
;			(gaussian (line n-impurity) n-broaden)
;		)
;	)
;
;	returning, if requested, the necessary elements to build
;	the model (in this case 4 gaussian broadeners, 3 lines and
;	1 ADAS element). It was also return the name associated
;	with each one. See user documentation for more information
;	on model syntax and construction. If required the opening
;	expression of model can be omitted but it is suggested that it
;	is used for forward comptability with possible extra functionallity.
;
;	The object will also accept a list of object references
;	corresponding to the elements it requires (these must be in the
;	same order as the order it asked for them in) and then call the
;	evaluate method on each element feeding in to it whatever
;	other outputs it requires from the other elements.
;
;	The model string can also include declerations to set initial
;	values via, e.g.:
;		(setval c-impurity1.pos 503.1)
;	where 'pos' in this case is one of the parameters of the
;	c-impurity1 element which is derived from a 'line' element.
;
;	Similarly, limits can be set via declerations like:
;		(setmin c-impurity1.pos 502.5)
;		(setmax c-impurity1.pos 504.0)
;	or as an alternative:
;		(setlim c-impurity1.pos 502.5 504.0)
;
;	Couplings can also be set via something like:
;		(couple c-impurity1.pos (+ c-impurity2.pos 3.1))
;	or in a non-prefix (but much more restricted) way via:
;		(couple c-impurity1.pos = c-impurity2.pos + 3.1)
;	the parsing/evaluation of the coupling string is done
;	by an ffs_couple object. The prefix notation allows
;	arbitrarly complex expressions such as:
;		(+ (* l1.pos 3.2) (sqrt (/ b1.fwhmg b2.fwhmg)))
;	but the infix notation is set to simply another parameter
;	followed by an operand followed by a number.
;
;       Parameters can be set to be fixed or free via
;       declarations like:
;               (fixed c-impurity1.pos n-impurity.pos)
;               (free c-impurity2.pos n-impurity.intensity)
;       or via a list/switch format such as:
;               (fixed c-impurity1.pos 1)
;               (fixed n-impurity.pos 1)
;               (fixed c-impurity2.pos n-impurity.intensity 0)
;       behaviour is unspecified if you set a parameter
;       to be both free and fixed.
;
;       Similarly log or linear dependence can be set via
;       declarations like:
;               (log c-impurity1.pos n-impurity.pos)
;               (linear c-impurity2.pos n-impurity.intensity)
;       or via a list/switch format such as:
;               (log c-impurity1.pos 1)
;               (log n-impurity.pos 1)
;               (linear c-impurity2.pos n-impurity.intensity 0)
;       behaviour is unspecified if you set a parameter
;       to be both log and linear.
;
; USE:
;	As an example (coupled with ffs_model):
;		parser=obj_new('ffs_parser')
;		model=obj_new('ffs_model')
;		checkme=parser->setfile('example.mdl')
;		checkme=parser->apply(model)
;	    then use:
;		result=model->evaluate()
;	    for evaluation.
;
;	As an example (stand alone):
;		parser=obj_new('ffs_parser')
;		checkme=parser->setfile('example.mdl')
;		information=parser->info()
;		elements=objarr(n_elements(information.elements))
;		for i=0,n_elements(information.elements)-1 do $
;			elements[i]=obj_new(information.elements[i],
;					information.optional[i])
;		checkme=parser->setelements(elements)
;		checkme=parser->setchildren()
;	    (and, optionally):
;		checkme=parser->setlimits()
;		checkme=parser->setvalues()
;		checkme=parser->setcouple()
;		checkme=parser->setfixedfree()
;	    then, for evaluation, use:
;		for i=0,n_elements(elements)-1 do $
;			checkme=elements[i]->evaluate()
;		result=elements[n_elements(elements)-1]->getresult()
;
; INITIALISATION ARGUMENTS:
;	(OPTIONAL KEYWORD) file       - Filename containing a model
;	(OPTIONAL KEYWORD) definition - String or stringarray with valid model
;
; PUBLIC ROUTINES:
;	setdefinition
;		PURPOSE:
;			Sets the model from a string.
;		INPUTS:
;			String or stringarray with valid model
;		OUTPUTS:
;			Returns 1 if model parsed and validated
;			Returns 0 if error occurred, use geterrmsg
;			method for details of the error.
;		SIDE EFFECTS:
;			None
;
;	setfile
;		PURPOSE:
;			Reads a model from a specified file.
;		INPUTS:
;			Filename to read from.
;		OUTPUTS:
;			Returns 1 if model is succesfully parsed etc.
;			Returns 0 if error occurred, use geterrmsg
;			method for details of the error.
;		SIDE EFFECTS:
;			None but note that if the file changes another
;			call to setfile must be done in order for the
;			model parser to realise.
;
;	apply
;		PURPOSE:
;			Applies information in parser to a model object.
;		INPUTS:
;			An ffs_model object to apply to.
;		OUTPUTS:
;			Returns 1 if model is succesfully parsed etc.
;			Returns 0 if error occurred, use geterrmsg
;			method for details of the error.
;		SIDE EFFECTS:
;			No direct side effects but creates elements
;			and calls methods of ffs_model which may
;			have side effects. 
;
;	Routines below this point are public but it is unlikely that
;	they will be required under normal usage of the parser. The
;	below routines make it possible to interface ffs_parser with
;	an element manager distinct from ffs_model.
;
;	setelements
;		PURPOSE:
;			Gives the parser the object references for
;			each individual element.
;		INPUTS:
;			elements - objarr of object references
;			in the same order as given out by the
;			required_elements method
;		OUTPUTS:
;			Always returns 1
;		SIDE EFFECTS:
;			None
;
;	info
;		PURPOSE:
;			Returns the elements required to generate
;			the model.
;		INPUTS:
;			None
;		OUTPUTS:
;			Returns a structure:
;				.title:		Overall model title
;				.elements:	Array of required elements
;				.optional:	Array of optional parameters
;						to these elements at
;						creation time.
;				.name:		User-supplied name of the
;						element
;		SIDE EFFECTS:
;			None
;
;	geterrmsg
;		PURPOSE:
;			Gets error state.
;		INPUTS:
;			None.
;		OUTPUTS:
;			Returns string of error explanation of last
;			error, typically set when a function returned
;			a zero.
;		SIDE EFFECTS:
;			None
;		INHERITED FROM:
;			ffs_primitive
;
;	getmodel
;		PURPOSE:
;			Returns model string
;		INPUTS:
;			Keyword /strip: Returns model as one string
;			with all whitespace reduced to one space.
;			Otherwise, model will be returned as it was
;			given to the object either as a strarr or as
;			it appeared in the file.
;		OUTPUTS:
;			The string representation of the model.
;		SIDE EFFECTS:
;			None
;
;	getfile
;		PURPOSE:
;			Gives the filename which was read.
;		INPUTS:
;			None
;		OUTPUTS:
;			Filename which was read.
;		SIDE EFFECTS:
;			None
;
;	setvalues
;		PURPOSE:
;			Sets the values of parameters if declerations
;			of their values were set within the original
;			model string.
;		INPUTS:
;			None (gets input from modelstring which was
;                       passed in via setfile or setdefinition methods)
;		OUTPUTS:
;			Returns 1 if succesful.
;			Returns 0 if unsuccesful.
;		SIDE EFFECTS:
;			Changes element parameter values.
;
;	setchildren
;		PURPOSE:
;			Sets the children of the various elements.
;		INPUTS:
;			None (gets input from modelstring which was
;                       passed in via setfile or setdefinition methods)
;		OUTPUTS:
;			Returns 1 if succesful.
;			Returns 0 if unsuccesful.
;		SIDE EFFECTS:
;			Changes the children of each element
;
;	setlimits
;		PURPOSE:
;			Sets the limits of parameters if declerations
;			of their limits were set within the original
;			model string.
;		INPUTS:
;			None (gets input from modelstring which was
;                       passed in via setfile or setdefinition methods)
;		OUTPUTS:
;			Returns 1 if succesful.
;			Returns 0 if unsuccesful.
;		SIDE EFFECTS:
;			Changes element parameter limits.
;
;	setcouple
;		PURPOSE:
;			Sets the coupling of parameters if declerations
;			of their coupling were set within the original
;			model string.
;		INPUTS:
;			None (gets input from modelstring which was
;                       passed in via setfile or setdefinition methods)
;		OUTPUTS:
;			Returns 1 if succesful.
;			Returns 0 if unsuccesful.
;		SIDE EFFECTS:
;			Creates coupling object and passes it's
;			reference to the targetted parameter.
;
;	setfixedfree
;		PURPOSE:
;			Sets the various parameters to fixed or
;			free if declerations of this was set
;			within the original model string.
;		INPUTS:
;			None (gets input from modelstring which was
;                       passed in via setfile or setdefinition methods)
;		OUTPUTS:
;			Returns 1 if succesful.
;			Returns 0 if unsuccesful.
;		SIDE EFFECTS:
;			Changes parameter switch via setfixed() method.
;
;	setloglinear
;		PURPOSE:
;			Sets the various parameters to have a
;                       log or linear dependence from the point
;                       of view of later fitting.
;		INPUTS:
;			None (gets input from modelstring which was
;                       passed in via setfile or setdefinition methods)
;		OUTPUTS:
;			Returns 1 if succesful.
;			Returns 0 if unsuccesful.
;		SIDE EFFECTS:
;			Changes parameter switch via setfixed() method.
;
;	docmds
;		PURPOSE:
;			Wrapper to setlimits, setvalues, setcouple,
;			setfixedfree and setloglinear. Also indirectly
;			sets a flag to say whether or not anything
;			was found.
;		INPUTS:
;			None (gets input from modelstring which was
;                       passed in via setfile or setdefinition methods)
;		OUTPUTS:
;			Returns 1 if succesful.
;			Returns 0 if unsuccesful.
;		SIDE EFFECTS:
;			Changed parameters via subordinate methods
;			Resets the number of expressions found.
;	
;	getexprfound
;		PURPOSE:
;			Gets whether or not an expression has been found
;			used to find if the last call to docmds actually
;			did anything or not.
;		INPUTS:
;			None
;		OUTPUTS:
;			Returns > 0 if expressions were found.
;			Returns 0 if no expressions were found.
;		SIDE EFFECTS:
;			None but note this is not an alternative
;			to checking the error state of docmds.
;			docmds will return 0 if a command
;			failed, whereas this variable will be
;			0 if no command was found at all.
;
;	getdefinition
;		PURPOSE:
;			Gets a string from the model.
;		INPUTS:
;			A model reference.
;			Tabstop (optional keyword) - The number of
;				spaces to use for indentation
;		OUTPUTS:
;			Returns a string or stringarray with a
;			valid model string.
;		SIDE EFFECTS:
;			None
;
;	writedefinition
;		PURPOSE:
;			Writes a model file from a model.
;		INPUTS:
;			A model reference.
;			A filename
;			Tabstop (optional keyword) - The number of
;				spaces to use for indentation
;		OUTPUTS:
;			Returns 1 if succesful.
;			Returns 0 if unsuccesful.
;		SIDE EFFECTS:
;			Writes a file
;
; CALLS:
;	Indirect calls when the evaluate method is invoked to an arbitrary
;	number of other evaluate methods of supplied objects.
;
; SIDE EFFECTS:
;	None
;	
; WRITTEN:
;       Allan Whiteford, University of Strathclyde
;
;          	Allan Whiteford    
;              	First test release to Andy Meigs and Chris Nicholas.
;		11/06/07
;
;          	Chris Nicholas
;              	Added in-line documentation to some of the code
;		04/07/07
;
;       	Allan Whiteford    
;              	Added proper documentation header
;		Threw away ability for functions to have multiple
;		parameter sets as arguments.
;		Renamed methods slightly.
;		Set single parameter to be the 'name' of the function
;		as returned in the required_elements substructure
;		of info.
;		Added simple validatation in the form of counting
;		open and close brackets.
;		05/07/07
;   
;       1.1     Chris Nicholas
;               File entered into repository for first time.
;               13/08/07
;
;       1.2   	Chris Nicholas 
;               Added cleanup method to this class.
;		13/08/07
;
;       1.3   	Allan Whiteford
;               Changed seterror so it returns 0 (makes using it nicer)
;		Added setvalues method (public)
;		Added setlimits method (public)
;		Added getexprs method (private)
;		16/10/07
;
;       1.4   	Allan Whiteford
;		Removed some commented out lines, print statments and whitespace.
;		Added setcouple method (public)
;		17/10/07
;
;       1.5   	Allan Whiteford
;		Added setfixedfree method (public)
;		18/10/07
;
;       1.6 	Chris Nicholas
;   	        setMin/setMax calls to parameters modified since these
;               are now function methods within ffs_par.
;   	    	Changed '*' conversion  in 'SymbolToString' mult->multiply
;		24/10/07
;
;       1.7 	Allan Whiteford
;   	        Passed /memo keyword through evaluate method to each
;   	        elements evaluate method.
;		Also passed object references to each evaluate method.
;		13/11/07
;
;   	1.8	Christopher Nicholas
;   	    	Changed call to setparvals method of elements to new format 
;    	    	- this means that errors are now handled.
;   	    	Small fix to setcouple method -'parref' is populated with obtained
;   	    	par references before call to par::setcoupled and call moved to
;   	    	correct place.
;		05/12/07
;
;       1.9 	Allan Whiteford
;   	        Allowed coupling syntax to handle individual elements without
;		operators as a special case.
;		10/12/07
;
;       1.10 	Allan Whiteford
;   	        Renamed to ffs_parser
;		Added applymodel method.
;		Added setchildren method.
;		Removed evaluate method.
;		Above method changes represent structural rechange to
;		evaluation code, the parser is no longer required at
;		evaluate time because it tells elements are parse time
;		who their children are.
;		11/12/07
;
;       1.11 	Allan Whiteford
;   	        Renamed setmodel method to setdefinition.
;		Added keywords file= and definition= to init method.
;		12/12/07
;
;       1.12 	Allan Whiteford
;   	        Removed inbuilt error handling (geterror method, seterror
;		method and error state variable).
;		Inherited ffs_primitive (for generic error handling)
;		Changed all seterror calls to seterrmsg.
;		Changed model->addelement call from procedure to function.
;		Changed call to par->setfixed from procedure to function.
;		Better error handling if par->setmin or par->setmax fail.
;		Create blank coupling object and then individually set
;		things to get better idea of error message.
;		20/12/07
;
;   	1.13	Chris Nicholas
;   	    	Added ffs_primitive init and cleanup calls.
;   	    	Re-arranged method order to maintain define-init-cleanup at
;               end of file.
;   	    	20/12/07
;
;       1.14    Chris Nicholas
;               Altered call to element::setname, as this has been changed from
;               from a procedure to a function to handle errors.
;               23/01/08
;
;       1.15    Chris Nicholas
;               Minor change to 'applymodel' - now perform 'setrootelement' on
;               model object.
;               19/02/08
;
;       1.16    Allan Whiteford
;               Applymodel method now sets model name
;		Added getdefinition method (public)
;		Added writedefinition method (public)
;		Added expandwrite method (private)
;		Fixed some documentation formatting
;               13/03/08
;
;       1.17    Allan Whiteford
;               Added support for fixed and free parameters to
;		getdefinition.
;               20/03/08
;
;       1.18    Chris Nicholas
;               Ammended cleanup method to free all pointers referenced
;               by instance data pointer 'exprs'.
;               Adjusted documentation header to keep version numbers
;               consistent with CVS repo.
;               16/05/08
;
;       1.19    Allan Whiteford
;               Added getroot and setroot element so parser can keep track
;		of the root element (removes assumption it's just the last
;		element on the stack).
;               Added getdummy method to find dummy variables.
;		Removed some redundant code (method calls where the result
;		wasn't used and commented out code).
;               20/10/08
;
;	1.20	Allan Whiteford
;		Renamed applymodel method to simply apply
;		Moved some of apply into docmds
;		11/11/08
;
;	1.21	Allan Whiteford
;		Keep track of how many commands are found by getexpr
;		between each call to docmd.
;		Added variable to state structure along with
;		get and set methods.
;		Changed model calls from getrootelement and setrootelement
;		to simply getroot and setroot
;		13/11/08
;
;	1.22	Allan Whiteford
;		Fixed two bugs in getdummy:
;		Routine was pulling out things like "c1d.fwhm" as
;		meaning d.fwhm was a dummy.
;		Routine was only finding the first dummy
;		parameter in each coupling expression. 
;		09/12/08
;
;	1.23	Allan Whiteford
;		Abstracted out setting of boolean type properties
;               from setfixedfree
;		Added setloglinear
;		17/06/09
;
;	1.24	Allan Whiteford
;		Removed debug print statement introduced in v1.23
;		19/06/09
;
;       1.25    Chris Nicholas
;               Fixed bug in getdummy (only finding first dummy) - 'fix' in 1.22
;               performed counter reset in wrong place.
;               Re-numbered erroneous version numbers in comments above.
;               23/08/10
;
;       1.26    Chris Nicholas
;               Modified 'setvalues' - ensure ffs_prop values are set before
;               ffs_par values.
;----------------------------------------------------------------------
;
; First version distributed with ADAS.
;
; MODIFIED   :
;       1.1     Martin O'Mullane
;                 - First central ADAS version.
;
; VERSION    :
;       1.1    04-01-2013
;
;----------------------------------------------------------------------
;-

function ffs_parser::apply,model,idx=idx
	
	info=self->info()
    
    	if model->setname(info.title) eq 0 then return,self->seterrmsg('Failed to set model name')
        
	for i = 0, n_elements(info.elements)-1 do begin 
     		element = obj_new(info.elements[i], info.optional[i])
       		if not obj_valid(element) then return,self->seterrmsg('Failed to create element of type: '+info.elements[i])
		if element -> setName(info.name[i]) eq 0 then return, self->seterrmsg('Error setting element name: ' + element->geterrmsg())
                if model->addElement(element) eq 0 then return,self->seterrmsg('Error adding element: ' + model->geterrmsg())
        end
        
        elements = model->getelements()
        
        
        if ptr_valid(self.elements) then ptr_free,self.elements
        self.elements=ptr_new(elements)
        if model->setroot(elements[self->getroot()]) eq 0 then $
            return, self->seterrmsg("error setting model's root element: " + model->geterrmsg())
	if self->setchildren() eq 0 then return,0
	
	return,self->docmds()
end

function ffs_parser::docmds
	if self->setexprfound(0) eq 0 then return,0
	if self->setlimits() eq 0 then return,0
	if self->setvalues() eq 0 then return,0
	if self->setcouple() eq 0 then return,0
	if self->setfixedfree() eq 0 then return,0
	if self->setloglinear() eq 0 then return,0
	return,1
end

function ffs_parser::setexprfound,val
	self.exprfound=val
	return,1
end

function ffs_parser::getexprfound
	return,self.exprfound
end

function ffs_parser::getdummy
	cmds=self->getexprs('dummy')
	if cmds[0] eq '' then return,1

	for i=0,n_elements(cmds)-1 do begin
		tmp=strsplit(cmds[i],' ',/extract)
		if n_elements(tmp) ne 2 then return,self->seterrmsg('Can''t understand '+cmds[i])

		list=['']
		couples=self->getexprs('couple')

                for j=0,n_elements(couples)-1 do begin
		    k=0
                    while k le strlen(couples[j]) do if ((a=stregex(strmid(couples[j],k++),' '+tmp[1]+'\.([a-z0-9]+)',/extract,/sub)))[0] ne ''  then list=[list,a[1]] else k=strlen(couples[j])+1
                endfor
                if n_elements(list) gt 1 then junk=self->store_expr({operator:'dummy',extra:strtrim(strjoin(list[uniq(list, sort(list))],' '),2),operands:-1,name:tmp[1]})
        end
	return,1
end

function ffs_parser::setchildren
        elements=*self.elements
        exprs=*self.exprs

        for i=1,n_elements(exprs)-1 do begin
		if (*exprs[i]).operands[0] ne -1 then begin
			idx=(*exprs[i]).operands
                        obj=elements[idx-1]
			if (elements[i-1])->setchildren(obj) eq 0 then return,self->seterrmsg('Problem setting up children for ' + info.name[i]);  + ': '(elements[i-1])->geterrmsg())
		endif
        end
	return,1
end

function ffs_parser::getexprs,operator

        model=self->getmodel(/strip)

	rval=''
	a=-1

        while ((a=strpos(strlowcase(model),'('+operator,a+1)) ne -1) do begin
        	i=a+1
        	cnt=1
		while((cnt ne 0) and (i++ ne strlen(model))) do begin
                	if (strmid(model,i,1) eq '(') then cnt=cnt+1
        		if (strmid(model,i,1) eq ')') then cnt=cnt-1
        	endwhile
		rval=[rval,strmid(model,a+1,i-a-2)]
	end

	if self->setexprfound((n_elements(rval) gt 1) > self->getexprfound()) eq 0 then return,0
	if n_elements(rval) gt 1 then rval=rval[1:*]
	return,rval
end

function ffs_parser::setlimits
	if not ptr_valid(self.elements) then return,self->seterrmsg('No elements loaded')

	elementnames = (self->info()).name
        model=self->getmodel(/strip)
	cmds=self->getexprs('setmin')
	cmds=[cmds,self->getexprs('setmax')]

	if cmds[0] ne '' or cmds[1] ne '' then begin

		cmds=cmds[where(cmds ne '')]

		for i=0,n_elements(cmds)-1 do begin
			tmp=strsplit(cmds[i],' ',/extract)
			if n_elements(tmp) ne 3 then return,self->seterrmsg('Can''t understand '+cmds[i])
			
	        	val=double(tmp[2])
		
			do_max=strpos(tmp[0],'max') ne -1 ? 1 : 0
		
			tmp=strsplit(tmp[1],'.',/extract)
			if n_elements(tmp) ne 2 then return,self->seterrmsg('Can''t understand '+cmds[i])
		
			idx=where(elementnames eq  tmp[0])
			if idx[0] eq -1 then return,self->seterrmsg('Can''t find element '+ tmp[0] +' for command '+cmds[i])

                	par=((*self.elements)[idx])->getpars(parname=tmp[1])
                        if not obj_valid(par) then return,self->seterrmsg('Couldn''t find '+tmp[1]+' inside of ' + tmp[0])
 
			if do_max eq 0 then begin
				if par->setmin(val) eq 0 then return,self->seterrmsg('Error setting minimum value: ' + par->geterrmsg())
			endif else begin
				if par->setmax(val) eq 0 then return,self->seterrmsg('Error setting maximum value: ' + par->geterrmsg())
			endelse
        	end
	end
	
	cmds=self->getexprs('setlim')

	if cmds[0] eq '' then return,1
	
	for i=0,n_elements(cmds)-1 do begin
		tmp=strsplit(cmds[i],' ',/extract)
		if n_elements(tmp) ne 4 then return,self->seterrmsg('Can''t understand '+cmds[i])
			
		val1=double(tmp[2])
		val2=double(tmp[3])
				
		tmp=strsplit(tmp[1],'.',/extract)
		if n_elements(tmp) ne 2 then return,self->seterrmsg('Can''t understand '+cmds[i])
		
		idx=where(elementnames eq  tmp[0])
		if idx[0] eq -1 then return,self->seterrmsg('Can''t find element '+ tmp[0] +' for command '+cmds[i])

		par=((*self.elements)[idx])->getpars(parname=tmp[1])
		if not obj_valid(par) then return,self->seterrmsg('Couldn''t find '+tmp[1]+' inside of ' + tmp[0])
	        
		if par->setmin(val1) eq 0 then return,self->seterrmsg('Error setting minimum value: ' + par->geterrmsg())
		if par->setmax(val2) eq 0 then return,self->seterrmsg('Error setting maximum value: ' + par->geterrmsg())
	end

	return,1
end

function ffs_parser::setvalues
	if not ptr_valid(self.elements) then return,self->seterrmsg('No elements loaded')

	elementnames = (self->info()).name
	cmds=self->getexprs('setval')

	if cmds[0] eq '' then return,1
        
        plist = objarr(n_elements(cmds))
	for i=0,n_elements(cmds)-1 do begin

		tmp=strsplit(cmds[i],' ',/extract)
		if n_elements(tmp) ne 3 then return,self->seterrmsg('Can''t understand '+cmds[i])
			
        	val=tmp[2]
		
		tmp=strsplit(tmp[1],'.',/extract)
		if n_elements(tmp) ne 2 then return,self->seterrmsg('Can''t understand '+cmds[i])
		
		idx=where(elementnames eq  tmp[0])
		if idx[0] eq -1 then return,self->seterrmsg('Can''t find element '+ tmp[0] +' for command '+cmds[i])
                plist[i] = ((*self.elements)[idx])->getpars(parname=tmp[1])
        endfor
        propidx=where(obj_isa(plist, 'ffs_prop'), comp=comp)
        if propidx[0] eq -1 then $
            cmds = cmds[comp] $
        else if comp[0] eq -1 then $
            cmds = cmds[propidx] $
        else cmds = [cmds[propidx], cmds[comp]]
        

	for i=0,n_elements(cmds)-1 do begin

		tmp=strsplit(cmds[i],' ',/extract)
		if n_elements(tmp) ne 3 then return,self->seterrmsg('Can''t understand '+cmds[i])
			
        	val=tmp[2]
		
		tmp=strsplit(tmp[1],'.',/extract)
		if n_elements(tmp) ne 2 then return,self->seterrmsg('Can''t understand '+cmds[i])
		
		idx=where(elementnames eq  tmp[0])
		if idx[0] eq -1 then return,self->seterrmsg('Can''t find element '+ tmp[0] +' for command '+cmds[i])
		if ((*self.elements)[idx])->setParVals(val,name=tmp[1]) eq 0 then $
		  return, self->seterrmsg(((*self.elements)[idx])->geterrmsg())
        end
	return,1
end




function ffs_parser::setcouple
	if not ptr_valid(self.elements) then return,self->seterrmsg('No elements loaded')

	elementnames = (self->info()).name
	cmds=self->getexprs('couple')

	if cmds[0] eq '' then return,1

	for i=0,n_elements(cmds)-1 do begin

		tmp=strsplit(cmds[i],' ',/extract)

		ttmp=strsplit(strlowcase(tmp[1]),'.',/extract)
		if n_elements(ttmp) ne 2 then return,self->seterrmsg('Can''t understand '+cmds[i])
		idx=where(elementnames eq  ttmp[0])
		if idx[0] eq -1 then return,self->seterrmsg('Can''t find element '+ ttmp[0] +' for command '+cmds[i])
                targetpar=((*self.elements)[idx])->getpars(parname=ttmp[1])
		if not obj_valid(targetpar) then return,self->seterrmsg('Can''t find parameter '+ ttmp[0] + '.' + ttmp[1] +' for command '+cmds[i])
                
                if n_elements(tmp) eq 6 and tmp[2] eq '=' then begin
			expr='('+tmp[4]+ ' ' + tmp[3] + ' '+ tmp[5]+')'
                endif else if n_elements(tmp) eq 4 and tmp[2] eq '=' then begin
			expr='(* '+ tmp[3] + ' 1.0)'
		endif else begin
                        expr=strmid(cmds[i],strpos(cmds[i],'('))
                endelse
		expr=strlowcase(expr)

		par=['']
                j=-1
                while ((a=stregex(expr,'([a-z][a-z0-9]*)\.([a-z0-9]+)',/extract,/sub)))[0] ne '' and ((par=[par,a]))[++j+1] ne '' do $
                        while ((k=strpos(expr,' '+a[0]+' '))) ne -1 || ((k=strpos(expr,' '+a[0]+')'))) ne -1 || ((k=strpos(expr,'('+a[0]+')'))) ne -1 do $
                        	expr=strmid(expr,0,k+1)+'$'+strtrim(string(j+1),2)+strmid(expr,k+strlen(a[0])+1)
		                
                parref=objarr(j)
                for k=0,j-1 do begin
			idx=where(elementnames eq par[3*k+2])
                        if idx[0] eq -1 then return,self->seterrmsg('Can''t find element '+ par[3*k+2] +' for command '+cmds[i])
			tmp=((*self.elements)[idx])->getpars(parname=par[3*k+3])
			if not obj_valid(tmp) then return,self->seterrmsg('Can''t find parameter '+ par[3*k+1] +' for command '+cmds[i])
                	parref[k] = tmp
                end

		if stregex(expr,'\( ?\$1 ?\)',/boolean) then expr = '(* $1 1.0)'
		couple=obj_new('ffs_couple')
                if couple->setpars(parref) eq 0 then return,self->seterrmsg('Error creating coupled parameter: ' + couple->geterrmsg() )
                if couple->setexpr(expr) eq 0 then return,self->seterrmsg('Error creating coupled parameter: ' + couple->geterrmsg() )
                if not obj_valid(couple) then return,self->seterrmsg('Error creating coupling object for command '+cmds[i])
                if targetpar->setcoupled(couple) eq 0 then return,self->seterrmsg('Error setting coupled parameter: ' + targetpar->geterrmsg() )
        end
	return,1
end

function ffs_parser::setboolean,methodname,truecmd,falsecmd
	if not ptr_valid(self.elements) then return,self->seterrmsg('No elements loaded')
	elementnames = (self->info()).name
	cmds=self->getexprs(falsecmd)

	cmds=[cmds,self->getexprs(truecmd)]

	if cmds[0] ne '' or cmds[1] ne '' then begin

		cmds=cmds[where(cmds ne '')]
		for i=0,n_elements(cmds)-1 do begin
                
	                tmp=strsplit(cmds[i],' ',/extract)
			if tmp[0] eq falsecmd then val=0
			if tmp[0] eq truecmd then val=1
			ignorelast=0
        	        if tmp[n_elements(tmp)-1] eq '0' then val=abs(val-1)
	                if tmp[n_elements(tmp)-1] eq '0' or tmp[n_elements(tmp)-1] eq '1' then ignorelast=1

			for j=1,n_elements(tmp)-1-ignorelast do begin
				ttmp=strsplit(strlowcase(tmp[j]),'.',/extract)
				if n_elements(ttmp) ne 2 then return,self->seterrmsg('Can''t understand '+cmds[i])
				idx=where(elementnames eq  ttmp[0])
				if idx[0] eq -1 then return,self->seterrmsg('Can''t find element '+ ttmp[0] +' for command '+cmds[i])
		                targetpar=((*self.elements)[idx])->getpars(parname=ttmp[1])
				if not obj_valid(targetpar) then return,self->seterrmsg('Can''t find parameter '+ ttmp[0] + '.' + ttmp[1] +' for command '+cmds[i])
                                if n_elements(targetpar) gt 1 then return,self->seterrmsg('Found multiple matches for parameter '+ ttmp[0] + '.' + ttmp[1] +' for command '+cmds[i])
                                if call_method(methodname,targetpar[0],val) eq 0 then return,self->seterrmsg('Error setting '+ ttmp[0] + '.' + ttmp[1] +' to ' + tmp[0] + ': ' + targetpar->geterrmsg())
	                end
		end
	end
	return,1
end

function ffs_parser::setfixedfree
	return,self->setboolean('setfixed','fixed','free')
end

function ffs_parser::setloglinear
	return,self->setboolean('setlogdep','log','linear')
end

function ffs_parser::setelements,elements
	if ptr_valid(self.elements) then ptr_free,self.elements
	self.elements=ptr_new(elements)
	return,1
end

function ffs_parser::getelements
	return,*self.elements
end

function ffs_parser::store_expr,expr
	if not ptr_valid(self.exprs) then begin
		exprs=ptr_new(ptrarr(1))
	endif else begin
		exprs=*self.exprs
		ptr_free,self.exprs
	endelse
	exprs=[exprs,ptr_new()]	
	self.exprs=ptr_new(exprs)	
	idx=n_elements(exprs)-1	
	if ptr_valid((*self.exprs)[idx]) then ptr_free,(*self.exprs)[idx]
	(*self.exprs)[idx]=ptr_new(expr)
	return,idx
end

function ffs_parser::info
	elements=['']
	optional=['']
	name=['']
	for i=1,n_elements(*self.exprs)-1 do begin
		elements=[elements,'ffs_'+(*(*self.exprs)[i]).operator]
		optional=[optional,(*(*self.exprs)[i]).extra]
		name=[name,(*(*self.exprs)[i]).name]
	end

	return,{title:self.title,elements:elements[1:*],optional:optional[1:*],name:name[1:*]}
end

function ffs_parser::symboltostring,sym
	if sym eq '+' then return,'add'
	if sym eq '*' then return,'multiply'
	return,sym
end

function ffs_parser::expand_s,expr
	name=''
        len=strlen(expr)
  	; bracket counters
	open=0
	close=0
	; flag that is set to true when a set of nested brackets has been entered
	opened=0
	operator=''
	buffer=''
	operands=[-1]
	inquote=0

	for p=0,len-1 do begin
	        ;extract the pth character.
		c=strmid(expr,p,1)
		
		if (c eq ' ' or c eq '(') and operator eq '' then begin
		  ; copy buffer to operator 
		  ; perform transform on any symbols e.g. '+', '*' etc.
		  ; reset buffer to empty string.
			operator=buffer
			operator=self->symboltostring(operator)
			buffer=''
		endif
		
		;if operator string empty, add pth character to buffer.
		if operator eq '' then buffer=buffer+c
		
		; if the pth character is a bracket, increment respective counter
		
                if not inquote then begin
        	        if c eq '(' then open=open+1
			if c eq ')' then close=close+1
		endif

		if c eq '''' then inquote=abs(1-inquote)

                ; if the opened flag is set and the number of '(' and ')' characters
		; encountered is equal, run this routine for new_expr
		if opened eq 1 and open eq close then begin
		
			operands=[operands,self->expand_s(new_expr)]
						
			opened=0
			close=0
			open=0
		endif
    
		; if opened flag set, add pth character to the new expression string
		if opened eq 1 then begin
			new_expr=new_expr+c
		endif
		
		; if a '(' has been encountered, but the opened flag not yet set;
		; then do so, to indicate entry to new bracket depth.
		; reset new_expr and buffer.
		if open eq 1 and opened eq 0 then begin
			new_expr=''
			opened=1
			buffer=''
		endif
		; if the opened flag isn't set and the pth character isn't a ')' and the operator string isn't empty...
		if opened eq 0 and (c ne ')'  or inquote eq 1) and operator ne '' then begin
			buffer=buffer+c
			if ((c eq ' ' and inquote eq 0) or p eq len-1) and buffer ne '' and buffer ne ' ' then begin
				name=strtrim(buffer,2)
				if strmid(name,0,1) eq '''' then name=strmid(name,1)
				if strmid(name,0,1,/reverse_offset) eq '''' then name=strmid(name,0,strlen(name)-1)
				name=strtrim(name,2)
                                buffer=''
			endif
		endif
	end
	
	if n_elements(operands) gt 1 then operands=operands[1:*] else operands=-1
	
	extra=''
	if strpos(operator,'-') ne -1 then begin
		a=strsplit(operator,'-',/extract)
		operator=a[0]
		extra=a[1]
	endif

	return,self->store_expr({operator:operator,extra:extra,operands:operands,name:name})
end


function ffs_parser::parse_model
	model=self->getmodel(/strip)
        
        a=strpos(strlowcase(model),'(model')
	b=strpos(model,'(',a+1)
        
        if a ne -1 then self.title=strtrim(strmid(model,a+6,b-a-6),2)
        	        
        cnt=1
        i=b
        while((cnt ne 0) and (i++ ne strlen(model))) do begin
                if (strmid(model,i,1) eq '(') then cnt=cnt+1
        	if (strmid(model,i,1) eq ')') then cnt=cnt-1
        endwhile
        
       	junk=self->expand_s(strmid(model,b+1,i-b-2))
	info=self->info()
        junk=self->setroot(n_elements(info.elements)-1)
        
        if self->getdummy() eq 0 then return,0
        return,1
end

function ffs_parser::setroot,root
	self.rootelement=root
end

function ffs_parser::getroot
	return,self.rootelement
end

function ffs_parser::writedefinition,model,filename,tabstop=tabstop
	
        towrite=self->getdefinition(model,tabstop=tabstop)
	if size(towrite,/tname) ne 'STRING' then return,0

	catch,err
	if err ne 0 then eturn,self->seterrmsg("Error writing to file: "+filename)
        openw,unit,filename,/get_lun
        catch,/cancel

	printf,unit,'Model written automatically by FFS'
	printf,unit,'----------------------------------'
	printf,unit,''
	printf,unit,'Editing this file by hand is ok but please'
	printf,unit,'note that FFS will not retain any comments'
	printf,unit,'or formatting changes which you may add if'
	printf,unit,'asked to read and then re-save this model.'
	printf,unit,''

	for i=0,n_elements(towrite)-1 do printf,unit,towrite[i]
        
        free_lun,unit
        return,1

end

function ffs_parser::getdefinition,model,tabstop=tabstop
	if not obj_valid(model) then return,-1
	if not keyword_set(tabstop) then tabstop=4

	val=['(model '+model->getname()]
	val=[val,string(bytarr(tabstop)+32b)+self->expandwrite(model->getroot(),tabstop=tabstop),')']

	elem=model->getelements(/all)

	sval=['']
        lval=['']
        cval=['']
        fval=''
        xval=''

	for i=0,n_elements(elem)-1 do begin
        	pars=(elem[i])->getpars(/fitting)
                if size(pars,/tname) eq 'OBJREF' then begin
                	for j=0,n_elements(pars)-1 do begin
		                sval=[sval,'(setval '+elem[i]->getname()+'.'+(pars[j])->getparname() + ' ' +strtrim(string((pars[j])->getvalue()),2)+')']
	        		lval=[lval,'(setmin '+elem[i]->getname()+'.'+(pars[j])->getparname() + ' ' +strtrim(string((pars[j])->getmin()),2)+')']
	        		lval=[lval,'(setmax '+elem[i]->getname()+'.'+(pars[j])->getparname() + ' ' +strtrim(string((pars[j])->getmax()),2)+')']
                        
                        	if (pars[j]->getfixed() eq 1) then begin
                                	xval = xval + ' ' + elem[i]->getname()+'.'+(pars[j])->getparname()
                                endif else begin
                                	fval = fval + ' ' + elem[i]->getname()+'.'+(pars[j])->getparname()
                                endelse
                        
                        	cobj=(pars[j])->getcoupled()
                                if obj_valid(cobj) then begin
                                	expr=cobj->getexpr()
					tpars=cobj->getpars()
                                        for k=0,n_elements(tpars)-1 do begin
						cparname=(tpars[k])->getparname()
                                        	celename=model->findpar(tpars[k])
                                                if size(celename,/tname) ne 'STRING' then begin
                                                	return,self->seterrmsg("Can't find parameter in model for coupled parameter")
                                                endif
                                        	while (pos=strpos(expr,'$'+strtrim(string(k+1),2)+' ')) ne -1 do $
                                                        expr=strmid(expr,0,pos)+celename+'.'+cparname+strmid(expr,pos+2+fix(alog10(k+1)))
                                        end
                                        cval=[cval,'(couple '+elem[i]->getname()+'.'+(pars[j])->getparname() + ' (' + expr + ') )']                            
                                end
                        
                        end
                endif
        end

	sval=[sval,'']

	for i=0,n_elements(elem)-1 do begin
        	pars=(elem[i])->getpars(/static)
                if size(pars,/tname) eq 'OBJREF' then begin
                	for j=0,n_elements(pars)-1 do begin
		                sval=[sval,'(setval '+elem[i]->getname()+'.'+(pars[j])->getname() + ' ' +strtrim(string((pars[j])->getvalue()),2)+')']
                        end
                endif
        end

	if strlen(fval) gt 0 then fval=['','(free'+fval+')']
	if strlen(xval) gt 0 then xval=['','(fixed'+xval+')']

	val=[val,sval,lval,cval,fval,xval]

	return,val

end

function ffs_parser::expandwrite,element,tabstop=tabstop
	if not keyword_set(tabstop) then tabstop=4
        val=strlowcase(strmid(obj_class(element),4))
        subtype=element->getsubtype()
        if strlen(subtype) gt 0 then val=val+'-'+subtype
        val='('+val+' '
        children=element->getchildren(count=count)
        
	for i=0,count-1 do expanded=n_elements(expanded) gt 0 ? [expanded,self->expandwrite(children[i],tabstop=tabstop)] : self->expandwrite(children[i],tabstop=tabstop)
        if count gt 0 then if n_elements(expanded) gt 1 then val=[val,string(bytarr(tabstop)+32b)+expanded] else val=val+expanded+' '
        if n_elements(val) eq 1 then val = val +element->getname()+')' else val = [val,(strlen(element->getname()) gt 0 ? ' ':'')+ element->getname()+')']

        return,val
end

function ffs_parser::getmodel,strip=strip

	if keyword_set(strip) then begin
	  ; call this routine again without 'strip' set to return de-referenced
		; 'modelString' pointer.
		model=self->getmodel()
		;'model' is a string array (element for each line) so combine into single
		; string.
		data=model[0]		
		for i=1,n_elements(model)-1 do begin
			data=data +  model[i]
		endfor
		; now split the string up, using the space character as a delimiter.
		data=strsplit(data,' '+string(9B),/extract)
		; finally recombine the elements of the array again adding a single space
		; character between each
		modelstring=data[0]
		for i=1,n_elements(data)-1 do begin
			modelstring = modelstring + ' ' + data[i]
		end
		; now we have the modelString with all occurences of whitespace reduced 
		; to single spaces. Return the pointer to the string.
		return,strtrim(modelstring,2)
	endif

	return,*self.modelstring
end

function ffs_parser::setdefinition,modelstring
	if ptr_valid(self.modelstring) then ptr_free,self.modelstring
	self.modelstring=ptr_new(modelstring)
	return,self->validate()
end

function ffs_parser::getfile
	return,self.filename
end

function ffs_parser::setfile,filename
	self.filename=filename
	
	openr,unit,filename,/get_lun
	
	a=''
	data=['']
	while not eof(unit) do begin
		readf,unit,a
		data=[data,a]
	end
	
	if n_elements(data) eq 1 then begin
		return,self->seterrmsg('Empty file')
	end
	
	data=data[1:*]
	
	free_lun,unit
	
	return,self->setdefinition(data)
end

function ffs_parser::validate

	mdl=self->getmodel(/strip)
	cnt=0
	a=-1
	while((a=strpos(mdl,'(',a+1)) ne -1) do cnt=cnt+1;
	a=-1
	while((a=strpos(mdl,')',a+1)) ne -1) do cnt=cnt-1;

	if cnt ne 0 then begin
		junk=self->setdefinition('')
		if cnt gt 0 then $
                	return,self->seterrmsg('Too many open brackets')
		if cnt lt 0 then $
                	return,self->seterrmsg('Too many close brackets')
	endif

	return,self->parse_model()
end

pro ffs_parser::cleanup
    if ptr_valid(self.modelstring) then ptr_free, self.modelstring
    if ptr_valid(self.exprs) then begin
        for i=0, n_elements(*self.exprs)-1 do $
            if ptr_valid((*self.exprs)[i]) then ptr_free, (*self.exprs)[i]
        ptr_free, self.exprs
    endif
    if ptr_valid(self.elements) then ptr_free, self.elements
    self->ffs_primitive::cleanup
end

function ffs_parser::init,file=file,definition=definition, debug=debug 

    	if self->ffs_primitive::init(debug=debug) ne 1 then return, 0

        if n_elements(file) gt 0 then return,self->setfile(file)
        if n_elements(definition) gt 0 then return,self->setdefinition(definition)
	return,1
end



pro ffs_parser__define
	self = {ffs_parser, 	$
		filename:'',		$
		modelstring:ptr_new(),	$
		exprs:ptr_new(),	$
		elements:ptr_new(),	$
		rootelement:-1,		$
                exprfound:0,		$
		title:'',		$
		inherits ffs_primitive	}

end
