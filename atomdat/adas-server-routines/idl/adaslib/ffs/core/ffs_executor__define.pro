;+
; PROJECT:
;       FFS
;
; NAME:
;	FFS_EXECUTOR
;
; PURPOSE:
;	Allow a parser-style command (e.g. setval, setlimits,
;	couple etc.) to be applied to a model.
;
; EXPLANATION:
;	This object internally creates a stripped down ffs_parser
;	object and uses it to act on a given model. It only loads
;	the parser with information when the model is given via
;	the setmodel command so if the model structure (e.g. a
;	new element is added) changes then subsequent use
;	of the execute method will produce undefined results
;
; USE:
;	As an example (given a model):
;		obj=obj_new('ffs_executor',model=model)
;		isok=obj->execute('setval l2.pos 9.3')
;		isok=obj->execute('(setval l1.pos 7.3)')
;		isok=obj->execute('couple b2.fwhm = b1.fwhm')
;
; INITIALISATION ARGUMENTS:
;	(OPTIONAL KEYWORD) model      - Model object to act on
;
; PUBLIC ROUTINES:
;	execute
;		PURPOSE:
;			Executes an FFS cmd on a model
;		INPUTS:
;			String containing command, may or
;			may not be surrounded by brackets
;		OUTPUTS:
;			Returns 1 if execution ok
;			Returns 0 if error occurred, use geterrmsg
;			method for details of the error.
;		SIDE EFFECTS:
;			None
;
;	setmodel
;		PURPOSE:
;			Sets the model to use
;		INPUTS:
;			The model
;		OUTPUTS:
;			Returns 1 if model is succesfully used etc.
;			Returns 0 if error occurred, use geterrmsg
;			method for details of the error.
;		SIDE EFFECTS:
;			If the model changes then the executor will
;			not notice, it needs to be explicitly
;			re-initalised via the setmodel method.
;
; CALLS:
;	Indirect calls via the parser which subsequently calls 
;	model methods.
;
; SIDE EFFECTS:
;	If the model changes then the executor will not notice, it
;	needs to be explicitly re-initalised via the setmodel
;	method.
;	
; WRITTEN:
;       Allan Whiteford, University of Strathclyde
;
;       1.1     Allan Whiteford
;               File entered into repository for first time.
;               11/11/08
;
;       1.2     Chris Nicholas
;               Made 'execute' exhibit slightly less pessimism.
;               13/11/08
;
;       1.2     Allan Whiteford
;               Made execute return zero if the parser fails to
;		do anything at all.
;               13/11/08
;
;----------------------------------------------------------------------
;
; First version distributed with ADAS.
;
; MODIFIED   :
;       1.1     Martin O'Mullane
;                 - First central ADAS version.
;
; VERSION    :
;       1.1    04-01-2013
;
;----------------------------------------------------------------------
;-


function ffs_executor::execute,cmd
	cmdtoset=cmd
	if strpos(cmd,'(') ne 0 then cmdtoset = '('+cmd+')'
	if self.parser->setdefinition(cmdtoset) eq 0 then return,self->seterrmsg('Issue with putting string into parser: '+self.parser->geterrmsg())
	if self.parser->docmds() eq 0 then return,self->seterrmsg('Issue with doing parser command: '+self.parser->geterrmsg())
	if self.parser->getexprfound() eq 0 then return,self->seterrmsg('No valid command found')
	return,1
end

function ffs_executor::setmodel,model
	if obj_valid(self.parser) then obj_destroy,self.parser
	self.parser=obj_new('ffs_parser')
	self.model=model
	
	elements=model->getelements()
	for i=0,n_elements(elements)-1 do if self.parser->store_expr({operator:strlowcase(strmid(obj_class(elements[i]),4)),extra:elements[i]->getsubtype(),name:elements[i]->getname()}) eq 0 then return,self->seterrmsg('Problem storing expressions in parser: '+ self.parser->geterrmsg())
	if self.parser->setelements(elements) eq 0 then return,self->seterrmsg('Problem setting elements in parser: ' + self.parser->geterrmsg())
	return,1
end

pro ffs_executor::cleanup
	if obj_valid(self.parser) then obj_destroy,self.parser	
end

function ffs_executor::init,model=model,debug=debug
    	if self->ffs_primitive::init(debug=debug) ne 1 then return, 0
	if n_elements(model) gt 0 then return,self->setmodel(model)
	return,1
end

pro ffs_executor__define
	self = {ffs_executor,		$
		model:obj_new(),	$
		parser:obj_new(),	$
		inherits ffs_primitive  }
end
