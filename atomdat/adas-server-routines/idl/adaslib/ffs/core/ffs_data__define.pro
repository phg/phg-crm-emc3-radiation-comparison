; 04-10-2004 AGM started data object to contain a data array
; (one-dimensional or n-dimensional not decided yet).
; 18-05-2007 AGM renamed to ffs_ (framework feature synthesis).
;
;+
;NAME:
;evaluate
;parameter "x", an array of independant variable values is passed in.
;the switch "ifunctoff" disables the convolution of the evaluated result with the instrument function.
;Accesses the parameters for current element and gets their respective values and
;adds them to an array before either calling the convolve method or if the ifunct is invalid/switched off,
;calls the particular "rule" procedure eg "agm_gaussian" for
;calculating the function, passing x and the array of parameter values as it does so.
;either way - returns an array of f(x) values.
;----------------------------------------------------------------------
;
; First version distributed with ADAS.
;
; MODIFIED   :
;       1.1     Martin O'Mullane
;                 - First central ADAS version.
;                 - Use ffs_primitive init and cleanup rather than the
;                   cn_primitive methods.
;
; VERSION    :
;       1.1    04-01-2013
;
;----------------------------------------------------------------------
;-
FUNCTION ffs_data::evaluate, x;;, IFUNCTOFF=ifunctoff
    RETURN, self -> getYdata()
 END
;+
;NAME:
; setXdata
;-
PRO ffs_data::setData, ydata, xdata, error, IER = ier
  ier = 1
  setYdata, ydata, ier = yier
  
  setXdata, xdata, ier = xier
  IF (self -> validateXdata()) EQ 0 THEN BEGIN
     ier = 0
     IF PTR_VALID(self.ptr_xdata) THEN PTR_FREE, self.ptr_xdata
  ENDIF
  
END

;+
;NAME:
;validateXdata
;-
FUNCTION ffs_data::validateXdata, xdata
  ;;
  data =  self -> getYdata()
  IF N_ELEMENTS(data) EQ 1 THEN return, 0 ;; no data input yet so ....
  IF N_ELEMENTS(xdata) EQ 1 THEN return, 0 ;; no xdata input yet so ....
  
  CASE 1 OF 
     N_ELEMENTS(xdata) LT N_ELEMENTS(data): BEGIN
        self -> ErrorMessage, 'Error: xdata size smaller than ydata'
        return, 0
     END
     N_ELEMENTS(xdata) GT N_ELEMENTS(data): BEGIN
        xdata = DOUBLE(xdata[0:N_ELEMENTS(data)-1]) ;; truncate and convert to double
        self -> setXdata, xdata
        return, -1
     END
     ELSE: return, 1
  ENDCASE
END

;+
;NAME:
; setXdata
;-
PRO ffs_data::setXdata, xdata,  IER = ier
  ier = 1
  IF N_ELEMENTS(xdata) EQ 0 THEN BEGIN
     self -> ErrorMessage, 'Error: X-data must be passed'
     ier = 0
     return  
  ENDIF
  IF NOT agm_isnumeric(xdata) THEN BEGIN
     self -> ErrorMessage, 'Error: X-data must be numeric (integer, float, double)'
     ier = 0
     return
  ENDIF
  IF SIZE(xdata, /n_dimensions) NE 1 THEN BEGIN
     self -> ErrorMessage,  'Error: X-data must be 1-dimensional'
     ier = 0
     return
  ENDIF ELSE BEGIN
     xdata = DOUBLE(xdata)
  ENDELSE
  ;; Next 
  IF PTR_VALID(self.ptr_xdata) THEN PTR_FREE, self.ptr_xdata

  ;;
  print, '(self -> validateXdata(xdata)) = ', (self -> validateXdata(xdata))
  IF (self -> validateXdata(xdata)) THEN BEGIN ;; 1 means ok...
     ier = 1
     IF PTR_VALID(self.ptr_xdata) THEN PTR_FREE, self.ptr_xdata
     self.ptr_xdata = PTR_NEW(xdata)
  ENDIF
  
END
;+
;NAME:
;getXdata
;-
FUNCTION ffs_data::getXdata
  IF PTR_VALID(self.ptr_xdata) THEN return, *self.ptr_xdata ELSE return, 0
END

;+
;NAME:
; setData
;-
PRO ffs_data::setYdata, data, IER = ier
  ier = 1
  IF N_ELEMENTS(data) EQ 0 THEN BEGIN
     self -> ErrorMessage, 'Error: data must be passed'
     ier = 0
     return
  ENDIF
  IF NOT agm_isnumeric(data) THEN BEGIN
     self -> ErrorMessage, 'Error: data must be numeric (integer, float, double)'
     ier = 0
     return
  ENDIF
  IF SIZE(data, /n_dimensions) NE 1 THEN BEGIN
     self -> ErrorMessage,  'Error: data must be 1-dimensional'
     ier = 0
     return
  ENDIF ELSE BEGIN
     data = DOUBLE(data)
  ENDELSE
  IF PTR_VALID(self.ptr_data) THEN PTR_FREE, self.ptr_data
  self.ptr_data = PTR_NEW(data)
  
END
;+
;NAME:
;getData
;-
FUNCTION ffs_data::getYData
  IF PTR_VALID(self.ptr_data) THEN return, *self.ptr_data ELSE return, 0
END

;+
;NAME:
;simply calls idl help on "self", with /obj switch set.
;-
PRO ffs_data::help
    help,self,/obj
END

;+
;NAME:
;cleanup
;This method frees all object values and destroys all objects stored
;in the container. This is already handled in cn_container::cleanup, so
;cleanup only needs to perform an obj_destroy on the parcontainer. Also
;makes the call to the superclass cn_primitive's cleanup.
;-
PRO ffs_data::cleanup
;;  OBJ_DESTROY, self.container ;;self.parcontainer
  OBJ_DESTROY, self.iFunct
  OBJ_DESTROY, self.plotstyle

  IF PTR_VALID(self.ptr_data) THEN PTR_FREE, self.ptr_data
  IF PTR_VALID(self.ptr_xdata) THEN PTR_FREE, self.ptr_xdata
  IF PTR_VALID(self.ptr_yerror) THEN PTR_FREE, self.ptr_yerror
  IF PTR_VALID(self.ptr_blind) THEN PTR_FREE, self.ptr_blind
  ;;
  self -> ff_primitive::cleanup

END

;+
;NAME:
;Init:
;This method initializes the object values (dataname/name, type, rule)
;calls the parent's Init method
;-
FUNCTION ffs_data::init, $
  data, $
  XDATA = xdata, $
  YERROR = error, $
  BLIND = blind, $
  DATANAME = dataname, $
  NAME = name, $ ;; 06-08-2004
  _Extra = extra                ; Extra keywords from inherited objects

  ;;PRINT,'In ffs_data::Init'
  If Self -> ffs_primitive::Init(_Extra = extra) NE 1 Then Return, 0
  ;; Error Handler
  Catch, theError
  IF theError NE 0 THEN BEGIN
     self -> ErrorMessage, 'Error initialising object'
     RETURN, 0
  ENDIF
  ;;
  self -> setYdata, data,  IER = ier
  IF ier NE 1 THEN return, 0
  IF N_ELEMENTS(xdata) EQ 0 THEN xdata = DINDGEN(N_ELEMENTS(self -> getData()))
  print, 'before setxdata init'
  self -> setXdata, xdata, IER = xier
  print, 'xier = ', xier
  IF xier EQ 0 THEN return, 0
  ;;
  IF N_ELEMENTS(dataname) GT 0 THEN BEGIN
     self.dataname = dataname
  ENDIF ELSE IF N_ELEMENTS(name) GT 0 THEN BEGIN
     self.dataname = name
  ENDIF ELSE self.dataname = 'data'
;  IF N_ELEMENTS(type) GT 0 THEN self.type = type ELSE self.type = 'constant'
;  IF N_ELEMENTS(rule) GT 0 THEN self.rule = rule ELSE self.rule = 'agm_constant'
;;  self.parContainer = OBJ_NEW('cn_container') ;(cn_container) containing the parameters
;  self.container = OBJ_NEW('cn_container') ;(cn_container) containing the parameters
  self.plotStyle = OBJ_NEW('ffs_plotstyle',  _Extra = extra) ;; 11-08-2004 AGM added
                                ;PRINT, 'here'
  RETURN, 1
END


;+
;NAME:
;define
;standard class definition, inherits cn_primitive.
;-
PRO ffs_data__define
    self = {ffs_data, $
            dataname:'', $
            ptr_data:PTR_NEW(), $
            ptr_xdata:PTR_NEW(), $
            ptr_yerror:PTR_NEW(), $
            ptr_blind:PTR_NEW(), $
;;            container:OBJ_NEW(), $   ;; will be cn_container containing the parameters
                                        ;; when initialised in init.
            plotstyle:OBJ_NEW(), $         ;; will contain a ffs_yplotstyle object when initialized
            iFunct:OBJ_NEW(), $
            INHERITS ffs_primitive};
END
