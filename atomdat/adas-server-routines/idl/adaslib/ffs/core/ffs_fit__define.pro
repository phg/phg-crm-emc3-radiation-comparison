;+
; PROJECT:
;   FFS - Framework for Feature Synthesis (Object oriented model/fitting).
;
; NAME:
;   FFS_FIT
;
; PURPOSE:
;   Performs parametric fitting of an FFS model to supplied data.
;
; EXPLANATION:
;   This object provides a method that searches the parameter space of the
;   supplied FFS model to find the best fit to the supplied data (attempts to
;   minimize chi-square). Further methods are provided to query the result of
;   the fit.
;
; USE:
;   model=obj_new('ffs_model')
;   parser=obj_new('ffs_parser',file='example.mdl')
;   some_as_yet_unknown_syntax_for_creating_populated_ffs_data_object
;   fitter=obj_new('ffs_fit')
;   res=fitter->apply(model, data)                                          
;                                                                              
;   Note that the 'debug' keyword (inherited from ffs_primitive') can be set to 
;   enable printing of error messages to the terminal.                          
;  
; INITIALISATION SYNTAX:
;   fitter=obj_new('ffs_fit', debug=debug) 
;   PARAMETERS: 
;     None.
;   KEYWORDS:
;     debug    	    	-   set this keyword to 1 for diagnostic information
;                           to be printed to the terminal.
;
; PUBLIC METHODS:
;   In addtion to the methods listed below, this object inherits methods from 
;   ffs_primitive - refer to this class' documentation for more details.
;   ............................................................................
;   [apply]
;     PURPOSE:
;       Main method to begin fitting model to data (uses a Levenberg-Marquardt
;       algorithm).
;     INPUTS:
;       model           -   object reference for the ffs_model to be used in
;                           fit.
;       data            -   object reference for the ffs_data to be used in fit
;                           (temporarily this will actually be a structure with
;                           three fields 'x', 'y' and 'error'.
;     OUTPUTS:
;       Returns         -   integer | returns a 1 or a 0 to signify success and
;                           failure respectively.
;     SIDE EFFECTS:
;       None.
;   ............................................................................
;   [getstatus]
;     PURPOSE:
;       Gets the fit status - indicates the manner in which the fit completed.
;     INPUTS:
;       None.
;     OUTPUTS:
;       Returns         -   integer |
;                           0   "Fail."
;                           1   "Reached relative tolerance (difference in 
;                               successive chisq) 'reltol'."
;                           2   "Reached max number of iterations 'maxiter'."
;     SIDE EFFECTS:
;       None.
;   ............................................................................
;   [getstatusmsg]
;     PURPOSE:
;       Returns the message associated with a given fit status.
;     INPUTS:
;       status          -   integer | the status number to get the message for.
;                           if none supplied, current status will be used.
;     OUTPUTS:
;       Returns         -   string | the requested status message.
;     SIDE EFFECTS:
;       None.
;   ............................................................................
;   [setmaxiter]
;     PURPOSE:
;       Sets maximum number of iterations the fitting routine will perform.
;     INPUTS:
;       maxiter         -   integer | maximum number of iterations.
;     OUTPUTS:
;       Returns         -   integer | returns a 1 or a 0 to signify success and
;                           failure respectively.
;     SIDE EFFECTS:
;       None.
;   ............................................................................ 
;   [getmaxiter]
;     PURPOSE:
;       Returns the maximum number of iterations that the fitting routine will
;       perform.
;     INPUTS:
;       None.
;     OUTPUTS:
;       Returns         -   integer | maximum number of iterations.
;     SIDE EFFECTS:
;       None.
;   ............................................................................ 
;   [geterrors]
;     PURPOSE:
;       Returns the errors in the fitted parameter values.
;     INPUTS:
;       None.
;     OUTPUTS:
;       Returns         -   double | errors in the fitted parameter values.
;                           Value of !values.d_nan signifies failure.
;     SIDE EFFECTS:
;       None.
;   ............................................................................ 
;   [getcor]
;     PURPOSE:
;       Returns the parameter correlation matrix.
;     INPUTS:
;       None.
;     OUTPUTS:
;       Returns         -   double | matrix of correlation between the parameters.
;                           Value of !values.d_nan signifies failure.
;     SIDE EFFECTS:
;       None.
;   ............................................................................ 
;   [getscovar]
;     PURPOSE:
;       Returns the scaled parameter covariance matrix.
;     INPUTS:
;       None.
;     OUTPUTS:
;       Returns         -   double | matrix of covariances between the (scaled)
;                           free parameters.
;                           Value of !values.d_nan signifies failure.
;     SIDE EFFECTS:
;       None.
;   ............................................................................
;   [getcurvm]
;     PURPOSE:
;       Returns the curvature matrix.
;     INPUTS:
;       None.
;     OUTPUTS:
;       Returns         -   double | curvature matrix.
;                           Value of !values.d_nan signifies failure.
;     SIDE EFFECTS:
;       None.
;   ............................................................................
;   [getchisqder]
;     PURPOSE:
;       Returns the matrix comprising of the values of the partial derivatives
;       (wrt each of the parameters) of chi-square.
;     INPUTS:
;       None.
;     OUTPUTS:
;       Returns         -   double | chisqder matrix.
;                           Value of !values.d_nan signifies failure.
;     SIDE EFFECTS:
;       None.
;   ............................................................................
;   [getdof]
;     PURPOSE:
;       Returns the number of degrees of freedom of the fit (number of data
;       points - number of free parameters).
;     INPUTS:
;       y               -   data (really just any array of number of data
;                           points in length though). OPTIONAL (required with p)
;       p               -   free parameters (really just any array with number
;                           of parameters in length though).OPTIONAL (required with y)
;     OUTPUTS:
;       Returns         -   double | degrees of freedom.
;                           Value of -1 signifies failure.
;       
;     SIDE EFFECTS:
;       None.
;   ............................................................................
;   [getnchisq]
;     PURPOSE:
;       Returns the normalised chi-square value (chi-square/degrees of freedom).
;     INPUTS:
;       None.
;     OUTPUTS:
;       Returns         -   double | normalised chi-square.
;                           Value of !values.d_nan signifies failure.
;     SIDE EFFECTS:
;       None.
;   ............................................................................
;   [getchisq]
;     PURPOSE:
;       Returns the chi-square value.
;     INPUTS:
;       None.
;     OUTPUTS:
;       Returns         -   double | chi-square.
;                           Value of !values.d_nan signifies failure.
;     SIDE EFFECTS:
;       None.
;   ............................................................................
;   [getp]
;     PURPOSE:
;       Returns the fitted parameter values.
;     INPUTS:
;       None.
;     OUTPUTS:
;       Returns         -   double | parameter values.
;                           Value of !values.d_nan signifies failure.
;     SIDE EFFECTS:
;       None.
;
; PRIVATE METHODS:
;   Although IDL does not support private routines, these are labelled as such
;   As it is not envisaged that users will typical call these methods
;   directly.
;   ............................................................................
;   [setsvals]
;     PURPOSE:
;       Sets the scaled parameter values.
;     INPUTS:
;       svals           -   double | scaled parameter values.
;     OUTPUTS:
;       Returns         -   integer | 1 (there is no error checking for this 
;                           method).
;     SIDE EFFECTS:
;       None.
;   ............................................................................
;   [getsvals]
;     PURPOSE:
;       Returns the scaled parameter values.
;     INPUTS:
;       None.
;     OUTPUTS:
;       Returns         -   double | scaled parameter values (there is no error
;                           checking for this method).
;                           
;     SIDE EFFECTS:
;       None.
;   ............................................................................
;   [setslimits]
;     PURPOSE:
;       Sets the scaled parameter value limits.
;     INPUTS:
;       svals           -   double | scaled parameter value limits.
;     OUTPUTS:
;       Returns         -   integer | 1 (there is no error checking for this 
;                           method).
;     SIDE EFFECTS:
;       None.
;   ............................................................................
;   [getslimits]
;     PURPOSE:
;       Returns the scaled parameter value limits.
;     INPUTS:
;       None.
;     OUTPUTS:
;       Returns         -   double | scaled parameter value limits (there is no
;                           error checking for this method).
;                           
;     SIDE EFFECTS:
;       None.
;   ............................................................................
;   [setivals]
;     PURPOSE:
;       Sets the initial parameter values.
;     INPUTS:
;       svals           -   double | initial parameter values.
;     OUTPUTS:
;       Returns         -   integer | 1 (there is no error checking for this 
;                           method).
;     SIDE EFFECTS:
;       None.
;   ............................................................................
;   [getivals]
;     PURPOSE:
;       Returns the initial parameter values.
;     INPUTS:
;       None.
;     OUTPUTS:
;       Returns         -   double | initial parameter values (there is no error
;                           checking for this method).
;                           
;     SIDE EFFECTS:
;       None.
;   ............................................................................
;   [scalepd]
;     PURPOSE:
;       Determines the partial derivative of the model w.r.t. the scaled
;       parameters, given the partial derivatives w.r.t. the real parameter 
;       values. Essentially applies the chain rule for derivatives.
;     INPUTS:
;       pd              -   partial derivatives of the model w.r.t. the real
;                           parameter values.
;       pars            -   object ref | array of ffs_par object references.
;     OUTPUTS:
;       spd             -   the scaled partial derivaties.
;     SIDE EFFECTS:
;       None.
;   ............................................................................
;   [unscale]
;     PURPOSE:
;       Recovers true parameter values from the scaled values used to improve
;       numerical stability of the fitting algorithm.
;     INPUTS:
;       pars            -   object ref | array of ffs_par object references.
;     OUTPUTS:
;       Returns         -   integer | a 1 or a 0 to signify success and failure
;                           respectively.
;     SIDE EFFECTS:
;       Calls 'setp' to store unscaled values.
;   ............................................................................
;   [scale]
;     PURPOSE:
;       Scales parameter values such all are of the same order to improve
;       numerical stability of the fitting algorithm.
;     INPUTS:
;       pars            -   object ref | array of ffs_par object references.
;     OUTPUTS:
;       Returns         -   integer | a 1 or a 0 to signify success and failure
;                           respectively.
;     SIDE EFFECTS:
;       Calls 'setsvals', 'setslimits' and 'setivals'. To store relevant
;       quantities.
;   ............................................................................
;   [setstatus]
;     PURPOSE:
;       Sets the fit status - the manner in which the fit completed.
;     INPUTS:
;       status          -   integer | the fit status.
;     OUTPUTS:
;       Returns         -   integer | a 1 or a 0 to signify success and failure
;                           respectively.
;     SIDE EFFECTS:
;       None.
;   ............................................................................
;   [setcurvm]
;     PURPOSE:
;       Stores the curvature matrix.
;     INPUTS:
;       curvm           -   double | curvature matrix.
;     OUTPUTS:
;       Returns         -   integer | a 1 or a 0 to signify success and failure
;                           respectively.
;     SIDE EFFECTS:
;       None.
;   ............................................................................
;   [calcmat]
;     PURPOSE:
;       Calculates the curvature matrix and the chi-square derivative matrix 
;       (w.r.t. each parameter).
;     INPUTS:
;       y               -   double | data intensity values.
;       yerror          -   double | error in data intensity values.
;       f               -   double | modelled intensity values.
;       nfree           -   integer | number of free parameters.
;       pd              -   double | partial derivatives of the model wrt free.
;                           free parameters, for each data point.
;     
;     OUTPUTS:
;       Returns         -   integer | a 1 or a 0 to signify success and failure
;                           respectively.
;       Keywords:
;       curvm           -   double | curvature matrix.
;       chisqder        -   double | chisqder matrix.
;     SIDE EFFECTS:
;       None.
;   ............................................................................      
;   [setdof]
;     PURPOSE:
;       Sets / calculates 
;     INPUTS:
;       y               -   double | data intensity values.
;       yerror          -   double | error in data intensity values.
;       f               -   double | modelled intensity values.
;       nfree           -   integer | number of free parameters.
;       pd              -   double | partial derivatives of the model wrt free.
;                           free parameters, for each data point.
;     
;     OUTPUTS:
;       Returns         -   integer | a 1 or a 0 to signify success and failure
;                           respectively.
;       Keywords:
;       curvm           -   double | curvature matrix.
;       chisqder        -   double | chisqder matrix.
;     SIDE EFFECTS:
;       None.  
;   ............................................................................ 
; AUTHOR:
;   Christopher Nicholas, University of Strathclyde
;
; MODIFICATION HISTORY:
;   1.1  CHN 10/11/2008
;        * Initial commit.
;   1.2  CHN 07/12/2008
;        * Added proper documentation header.
;        * More error checks in 'init'.
;        * Simplified 'getdof'.
;        * Added 'setdof'.
;        * 'getchisqder', 'getcurvm', 'getcovar', 'geterrors' use error message
;          setting and return NAN rather than -1 to indicate failure.
;        * Check for case that chi-sqaure becomes infinite for whatever reason
;          in 'apply'.
;        * dof manually calulated in 'apply' now - setdof method simply stores
;          the figure now.
;        * Made 'apply' carry out 'setxdata' operation on model.
;   1.3  CHN 19/06/2009
;        * Fitting now done using scaled parameter values (changes made to
;          'apply').
;        * New methods:
;          - setsvals
;          - getsvals
;          - setslimits
;          - getslimits
;          - setivals
;          - getivals
;          - scalepd
;          - unscale
;          - scale
;        * New state variables:
;          - svals
;          - slimits
;          - ivals
;        * Removed 'model' object reference from state structure - not used,
;          or required.
;        * Modified cleanup to deal with new state variables.
;        * Check status of la_invert routine in 'apply' and exit fitting
;          loop if failure occurs.
;   1.4  ADW 23/06/2009
;        * Efficiency increase (~ 5x faster) of calcmat method.
;   1.5  ADW 01/07/2009
;        * Efficiency increase by simplifying scalepd and unscale methods
;          by pre-calculating information in new setupscale procedure.
;   1.6  CHN 07/07/2009
;        * Final calculation of curvature matrix (to obtain errors on fitted
;          parameter values) now done using scaled (rather than 'real') 
;          values for stability.
;        * Added method 'unscaleerr'.
;        * Modified 'geterrors' to use 'unscaleerr' before returning result -
;          to deal with the fact that we now have a scaled curvature matrix
;          at this point.
;        * Modified 'setupscale':
;          - merged 'setupscale' and 'scale' methods as they were duplicating
;            work and renamed result to 'scalesetup'
;          - replaced direct setting of state variables in 'setupscale' with use
;            of accessor methods
;          - removed commented out line 'spd[*,i] = pd[*,i] * factor'.
;        * Corrected fault in 'geterrors' and 'getcovar' - was erroneously using
;          !values.d_nan in a conditional expression; now use IDL 'finite'.
;        * Perform cleanup of pointer state variables introduced in version 1.5
;          ('scale_factor', 'scale_add' and 'scale_logdep').
;        * Added accessor methods for 'scale_factor', 'scale_add' and
;          'scale_logdep'.         
;        * Added basic error check to 'setsvals', 'setslimits' and 'setivals'.
;        * Modified 'scalepd' to take input of values rather than pass through
;          state variable 'p'
;        * Modified 'unscale' to take input of values rather than pass through
;          state variable 'svals'
;        * Modified 'apply':
;          - removed duplicate 'getpars' call
;          - removed unecessary 'getnumpars' call
;          - removed 'stop' statement
;          - removed calls to 'setsvals' - pass directly to unscale
;          - modified calls to 'unscale' and 'scalepd' to take input of
;            variables to be scaled / unscaled
;          - enabled 'fast' switch in call to 'translatefitresults'.
;   1.7  CHN 07/08/2009
;        * Corrected erroneous setting of svals in scalesetup when par has log
;          dependance.
;   1.8  CHN 23/10/2009
;        * Re-named 'getcovar' to 'getscovar' to reflect the fact that covar is
;          covariance of scaled values.
;        * Added method 'getcor'.
;        * changed un-necessary 'for' blocks to single statements in
;          'unscaleerr' and 'scalepd'.
;        * Now store the number of iterations taken - methods:
;          - setitercount
;          - getitercount
;        * Improved error messages for default setting failure in init.
;        * Default values for'maxiter' and 'itercount' set in init.
;   1.9  CHN 18/02/2010
;        * corrected cleanup to free up pointers used by this class.
;----------------------------------------------------------------------
;
; First version distributed with ADAS.
;
; MODIFIED   :
;       1.1     Martin O'Mullane
;                 - First central ADAS version.
;
; VERSION    :
;       1.1    04-01-2013
;
;----------------------------------------------------------------------
;-

function ffs_fit::setscale_factor, factor
    if ptr_valid(self.scale_factor) then ptr_free,self.scale_factor
    self.scale_factor=ptr_new(factor)
    return, 1
end

function ffs_fit::getscale_factor
    return, *self.scale_factor
end

function ffs_fit::setscale_logdep, logdep
    if ptr_valid(self.scale_logdep) then ptr_free,self.scale_logdep
    self.scale_logdep=ptr_new(logdep)
    return, 1
end

function ffs_fit::getscale_logdep
    return, *self.scale_logdep
end

function ffs_fit::setscale_add, add
    if ptr_valid(self.scale_add) then ptr_free,self.scale_add    
    self.scale_add=ptr_new(add)
    return, 1
end

function ffs_fit::getscale_add
    return, *self.scale_add
end

function ffs_fit::setsvals, svals
    if n_elements(svals) eq 0 then return, $
        self->seterrmsg('no svals to set')
    if ptr_valid(self.svals) then ptr_free, self.svals
    self.svals = ptr_new(svals)
    return, 1
end

function ffs_fit::getsvals
    return, *self.svals
end

function ffs_fit::setslimits, slimits
    if n_elements(slimits) eq 0 then return, $
        self->seterrmsg('no slimits to set')
    if ptr_valid(self.slimits) then ptr_free, self.slimits
    self.slimits = ptr_new(slimits)
    return, 1
end

function ffs_fit::getslimits
    return, *self.slimits
end

function ffs_fit::setivals, ivals
    if n_elements(ivals) eq 0 then return, $
        self->seterrmsg('no ivals to set')
    if ptr_valid(self.ivals) then ptr_free, self.ivals
    self.ivals = ptr_new(ivals)
    return, 1
end

function ffs_fit::getivals
    return, *(self.ivals)
end

function ffs_fit::scalesetup,pars

    add=dblarr(n_elements(pars))
    factor=dblarr(n_elements(pars))
    logdep=intarr(n_elements(pars))
    ivals = dblarr(n_elements(pars))
    slimits = dblarr(2,n_elements(pars))
    
    for i=0, n_elements(pars)-1 do begin
        ivals[i] = pars[i]->getvalue()
        logdep[i] = pars[i]->getlogdep()
        limits = pars[i]->getefflimits()
        
        if limits[0] ne -!values.d_infinity and limits[1] ne !values.d_infinity then begin
            if logdep[i] eq 0 then begin
                factor[i] = (limits[1] - limits[0])
                add[i]=limits[0]
            endif else begin
                factor[i] = (alog10(limits[1]) - alog10(limits[0]))
		add[i]=ALOG10(limits[0])
            endelse
            slimits[*,i] = [0,1]
        endif else if limits[0] eq -!values.d_infinity and limits[1] ne !values.d_infinity then begin
            if logdep[i] eq 0 then begin
                factor[i] = 2.0d0 * (limits[1] - ivals[i])
                add[i]=-0.5d0*factor[i]+ivals[i]
            endif else begin
                factor[i] = 2.0d0 * (alog10(limits[1]) - alog10(ivals[i]))
                add[i]=-0.5d0 * factor[i]+alog10(ivals[i])
            endelse
            slimits[*,i] = [-!values.d_infinity,1]
        endif else if limits[0] ne -!values.d_infinity and limits[1] eq !values.d_infinity then begin
            if logdep[i] eq 0 then begin
                factor[i] = 2.0d0 * (ivals[i] - limits[0])
                add[i]=-0.5d0*factor[i]+ivals[i]
            endif else begin
                factor[i] = 2.0d0 * (alog10(ivals[i]) - alog10(limits[0]))
                add[i]=-0.5d0 * factor[i]+alog10(ivals[i])
            endelse
            slimits[*,i] = [0,!values.d_infinity]        
        endif else begin
            if logdep[i] eq 0 then begin
                factor[i] = ivals[i] / 0.5
            endif else begin
                factor[i] = alog10(ivals[i]) / 0.5
            endelse
            slimits[*,i] = [-!values.d_infinity,!values.d_infinity]
        endelse
    endfor    

    if self->setscale_factor(factor) eq 0 then return, 0
    if self->setscale_logdep(logdep) eq 0 then return, 0
    if self->setscale_add(add) eq 0 then return, 0
    if self->setivals(ivals) eq 0 then return, 0
    idx = where(logdep eq 1)
    if idx[0] ne -1 then ivals[idx] = alog10(ivals[idx])
    if self->setsvals((ivals-add)/factor) eq 0 then return, 0
    if self->setslimits(slimits) eq 0 then return, 0
    return,1
end


function ffs_fit::unscaleerr, serr
    vals = self->getp()
    err = dblarr(n_elements(serr))
    factor=*self.scale_factor
    logdep=*self.scale_logdep
       
    for i=0,n_elements(serr)-1 do $
    	if logdep[i] then $
        	err[i] = serr[i] * factor[i] * vals[i] $
	else $
		err[i] = serr[i] * factor[i]
    return, err
end


function ffs_fit::scalepd, pd, vals

    spd = dblarr((size(pd,/dim))[0],(size(pd,/dim))[1])
   
    factor=*self.scale_factor
    logdep=*self.scale_logdep
   
    for i=0, n_elements(vals)-1 do $
    	if logdep[i] then $
        	spd[*,i] = pd[*,i] * factor[i] * vals[i] $
	else $
		spd[*,i] = pd[*,i] * factor[i]
    return, spd
end

function ffs_fit::unscale, svals
    
   factor=*self.scale_factor
   add=*self.scale_add
   logdep=*self.scale_logdep

   vals=add + svals * factor
   idx=where(logdep eq 1)
   if idx[0] ne -1 then vals[idx]=10d0^vals[idx]
   return, vals
end

function ffs_fit::apply, model, data, num=num
    
    lambda=0.01

    ; scale data and error.
    y = data.y
    yerror = data.error
    
    ; catch for any NaN values for error.
     errnanidx = where(finite(yerror) eq 0)
    if errnanidx[0] ne -1 then yerror[errnanidx] = max(y)

    x = data.x
    if model->setxdata(x) eq 0 then return, $
        self->seterrmsg('failure in setting xdata for model - ' + $
        '+model->geterrmsg()')
    f = (model->evaluate(/memo)).intensity
    
    pars = model->getpars(/free, count=nfree)    
    self.nfree = nfree
    if self->scalesetup(pars) eq 0 then return, $
        self->seterrmsg('Failed to setup parameter scaling, ' + self->geterrmsg())
    if self->setp(model->getparvals(/free)) eq 0 then return, 0

    vals = self->getivals()
    svals = self->getsvals()
    slimits = self->getslimits()    
    
    reltol = 0.001
    complete = reltol    
    maxiter = self->getmaxiter()
    if maxiter lt 1 then return, $
        self->seterrmsg('maxiter is: '+strtrim(string(maxiter),2)+ $
        ' cannot perform fitting.')
    invertfail=0    
    oldchisq = !values.d_infinity
    
    ; set current chi-square
    if self->setchisq(self->calcchisq(y, f, yerror)) eq 0 then return, 0
    if model->pdsetup() eq 0 then return, $
        self->seterrmsg('failed while attempting to run model pdsetup - '+ $
            model->geterrmsg())
    if model->fastsetup() eq 0 then return, $
        self->seterrmsg('failed while attempting to run model fastsetup - '+ $
            model->geterrmsg())
    count=0    
    
    if self.debug eq 1 then begin
        print, 'Initial chi-square: '+ strtrim(string(self->getchisq()),2)
        print, 'Begin minimization...'
    endif
    
    repeat begin          
        if self->getchisq() ge oldchisq then $
            lambda *= 10.0 $
        else begin
            lambda *= 0.1
            oldsvals = svals
            complete = (abs(self->getchisq()-oldchisq)/ oldchisq)
            oldchisq = self->getchisq()
            if complete lt reltol then break 
            if self->calcmat(y, yerror, f, nfree, self->scalepd(model->getpd(num=num), vals), $
                curvm=curvm, chisqder=chisqder) eq 0 then return, 0
        endelse
        
        ; multiply diagonals by Marquardt factor
        for i=0, nfree-1 do curvm[i,i] *= (1.0+lambda)


        ; solve equations
        svals = oldsvals + la_invert(curvm, /double, status=la_inv_stat) # chisqder

        if la_inv_stat ne 0 then begin
            if self->setstatus(-1) eq 0 then return, 0
            svals = oldsvals
            invertfail=1
        endif
        
        ; keep pars in limits
        svals = reform(slimits[0,*]) > svals
        svals = reform(slimits[1,*]) < svals

        vals = self->unscale(svals)
        
        ; evaluate chi-square with these parameters
        if model->translatefitresults(vals, /fast) eq 0 then return, $
            self->seterrmsg('failed to pass fit parameters back to model - '+ $
                model->geterrmsg())
        f = (model->evaluate(/memo)).intensity
       
        if finite(self->calcchisq(y, f, yerror)) eq 0 then begin
            if self->setstatus(0) eq 0 then return, 0
            return, self->seterrmsg('Chi-square is infinite or NAN')
        endif
       
        if self->setchisq(self->calcchisq(y, f, yerror)) eq 0 then return, 0
        count++
        if self.debug eq 1 then print, '('+strtrim(string(count), 2)+') '+ $
            strtrim(string(self->getchisq()),2)
    endrep until ((invertfail eq 1) ||(complete lt reltol) || (count eq maxiter))
    
    if self->setp(vals) eq 0 then return, 0
    if self->setsvals(svals) eq 0 then return, 0
    if count eq maxiter then if self->setstatus(2) eq 0 then return, 0
    if complete lt reltol then if self->setstatus(1) eq 0 then return, 0
    if self->setdof(n_elements(y) - n_elements(p)) eq -1 then return, 0
    
    ; re-calculate curvature matrix and chi-square derivative matrices at final
    ; point    
    if self->calcmat(y, yerror, f, nfree, self->scalepd(model->getpd(num=num), vals), $
                curvm=curvm, chisqder=chisqder) eq 0 then return, 0
    if self->setcurvm(curvm) eq 0 then return, 0
    if self->setchisqder(chisqder) eq 0 then return, 0
    if self->setitercount(count) eq 0 then return, 0
    
    if self.debug eq 1 then begin
        print, '********************************************'
        print, 'fitting complete(status='+ $
            strtrim(string(self->getstatus()),2)+')'
        print, 'status message: '+self->getstatusmsg()
        print, 'iterations: ', self->getitercount()
        print, 'chi-square is: ', self->getchisq()
        print, 'degrees of freedom: ', self->getdof()
        print, 'normalised chi-square is: ', self->getnchisq()
        print, '********************************************'
        print, 'parameters have been determined to be: '
        print, self->getp()
        print, '********************************************'
        print, 'errors are:'
        print, self->geterrors()
        print, '********************************************'
    endif
    return, 1
end

function ffs_fit::setitercount, itercount
    self.itercount = itercount
    return, 1
end

function ffs_fit::getitercount
    return, self.itercount
end

function ffs_fit::getstatusmsg, status
    if n_elements(status) eq 0 then status = (self->getstatus())[0]
    errmsg = self->geterrmsg()
    if errmsg eq '' then errmsg = 'none'
    if status eq 0 then return, 'Failed to fit. Checking fitting object for '+ $
        'error message...' +errmsg $
        else if status eq 1 then return, $
        "Reached relative tolerance (difference in successive chisq) 'reltol'." $
        else if status eq 2 then return, "Reached max number of iterations 'maxiter'." $
        else return, "No message associated with this status."
end

function ffs_fit::getstatus
    return, self.status
end

function ffs_fit::setstatus, status
    if not (size(status, /type) eq 2 || size(status, /type) eq 3) then $ 
        return, self->seterrmsg('input must be type integer')
    self.status = status
    return, 1
end

function ffs_fit::setmaxiter, maxiter
    if not (size(maxiter, /type) eq 2 || size(maxiter, /type) eq 3) then $ 
        return, self->seterrmsg('input must be type integer')
    self.maxiter = maxiter
    return, 1
end

function ffs_fit::getmaxiter
    return, self.maxiter
end

function ffs_fit::geterrors
    if finite(((scovar = self->getscovar()))[0], /nan) eq 1 then return, $
        self->seterrmsg('Invalid covariance matrix - so cannot return errors', $
        retval=!values.d_nan)    
    errors = dblarr(self.nfree)
    for i=0, self.nfree-1 do errors[i] = sqrt(scovar[i,i])
    return, self->unscaleerr(errors)
end

function ffs_fit::getcor
    scovar = self->getscovar()
    if finite(scovar[0], /nan) eq 1 then return, 0
    cor = scovar
    for i=0, self.nfree-1 do $
        for j=0, self.nfree-1 do $
            cor[i,j] = scovar[i,j]/ sqrt(scovar[i,i]*scovar[j,j])
    return, cor
end

function ffs_fit::getscovar

    if finite((self->getcurvm())[0], /nan) eq 1 then return, $
        self->seterrmsg('Invalid curvature matrix - so cannot return scovar', $
        retval=!values.d_nan)
    res = la_invert(self->getcurvm(), /double, status=status)
    if status ne 0 then return, $
        self->seterrmsg('Error when attempting to invert curvature matrix - ' + $
            'LA_INVERT reports status:'+strtrim(string(status),2), $
            retval=!values.d_nan)
    return, res
end

function ffs_fit::setcurvm, curvm
    if ptr_valid(self.curvm) then ptr_free, self.curvm
    self.curvm = ptr_new(curvm)
    return, 1
end

function ffs_fit::getcurvm
    if ptr_valid(self.curvm) eq 0 then return, $
        self->seterrmsg('No curvature matrix to retrieve.', retval=!values.d_nan)
    return, *self.curvm
end

function ffs_fit::setchisqder, chisqder
    if ptr_valid(self.chisqder) then ptr_free, self.chisqder
    self.chisqder = ptr_new(chisqder)
    return, 1
end

function ffs_fit::getchisqder
    if ptr_valid(self.chisqder) eq 0 then return, !values.d_nan
    return, *self.chisqder
end

function ffs_fit::calcmat, $
    y, $
    yerror, $
    f, $
    nfree, $
    pd, $
    curvm=curvm, $
    chisqder=chisqder
    
    if ptr_valid(self.curvm) then ptr_free, self.curvm
    if ptr_valid(self.chisqder) then ptr_free, self.chisqder
    
    curvm = dblarr(nfree, nfree)

    yerror2=yerror^2
    pdn=pd/rebin(yerror2,n_elements(y),nfree)
    for i=0, nfree-1 do $
        for j=i, nfree-1 do $
            curvm[i,j] = (curvm[j,i] = total(pdn[*,i]*pd[*,j]))
    chisqder=total(rebin((y-f)/yerror2,n_elements(y),nfree)*pd,1)
    
    self.curvm = ptr_new(curvm)
    self.chisqder = ptr_new(chisqder)
    return, 1
end

function ffs_fit::setdof, dof  
    if size(dof, /type) ne 2 and size(dof, /type) ne 3 then return, $
        self->seterrmsg('input dof was not of type integer/long', retval=-1)
    self.dof = dof
    return, 1
end

function ffs_fit::getdof
    return, self.dof 
end

function ffs_fit::getnchisq
    return, self->getchisq() / self->getdof()
end

function ffs_fit::setchisq, chisq
    self.chisq = chisq
    return, 1
end

function ffs_fit::getchisq   
    return, self.chisq 
end

function ffs_fit::calcchisq, y, f, yerror
    if n_params() ne 3 then return, $
        self->seterrmsg('incorrect number of arguments', retval=-1)
    return, (self.chisq = total(((y - f)/yerror)^2))
end

function ffs_fit::setp, p
    if ptr_valid(self.p) then ptr_free, self.p
    if size(p, /type) ne 4 and size(p, /type) ne 5 then return, $
        self->seterrmsg('input p was not of type float/double')
    self.p = ptr_new(p)
    return, 1
end

function ffs_fit::getp
    if ptr_valid(self.p) eq 0 then return, $
        self->seterrmsg('No parameters stored in object - ' + $
            'perhaps need to run apply method?', retval=!values.d_nan)
    return, *self.p
end

pro ffs_fit::cleanup
    if ptr_valid(self.p) then ptr_free, self.p
    if ptr_valid(self.svals) then ptr_free, self.svals
    if ptr_valid(self.slimits) then ptr_free, self.slimits
    if ptr_valid(self.ivals) then ptr_free, self.ivals    
    if ptr_valid(self.curvm) then ptr_free, self.curvm
    if ptr_valid(self.chisqder) then ptr_free, self.chisqder
    if ptr_valid(self.scale_factor) then ptr_free, self.scale_factor
    if ptr_valid(self.scale_logdep) then ptr_free, self.scale_logdep
    if ptr_valid(self.scale_add) then ptr_free, self.scale_add
    
    self->ffs_primitive::cleanup
end

function ffs_fit::init, debug=debug
    if self->ffs_primitive::init(debug=debug) eq 0 then return, 0
    if self->setstatus(0) eq 0 then return, $
	self->seterrmsg('failed to set initialise status')
    if self->setchisq(!values.d_nan) eq 0 then return, $
	self->seterrmsg('failed to initialise chisq')
    if self->setdof(-1) eq 0 then return, $
	self->seterrmsg('failed to initialise dof')
    if self->setitercount(-1) eq 0 then return, $
	self->seterrmsg('failed to initialise itercount')
    if self->setmaxiter(300) eq 0 then return, $
        self->seterrmsg('failed to set default maxiter')
    return, 1
end

pro ffs_fit__define
    self = {ffs_fit, $
            p:ptr_new(), $
            svals:ptr_new(), $
            slimits:ptr_new(), $
            ivals:ptr_new(), $
            chisq:0.0d0, $
            dof:0, $
            nfree:0, $
            curvm:ptr_new(), $
            chisqder:ptr_new(), $
            itercount:0, $
            maxiter:0, $
            status:0, $
            scale_factor:ptr_new(), $
            scale_logdep:ptr_new(), $
            scale_add:ptr_new(), $
            inherits ffs_primitive}
end
