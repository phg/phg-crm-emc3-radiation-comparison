;+
; PROJECT:
;   FFS - Framework for Feature Synthesis (Object oriented model/fitting).
;
; NAME:
;   FFS_PAR
;
; PURPOSE:
;   Set up a fitting parameter of a spectral feature (an FFS element) e.g. the
;   full width at half maximum of a gaussian.
;
; EXPLANATION:
;   Represents and controls all the attributes of a parameter of a spectral model
;   element. These parameters control the shape of spectral features and are altered
;   during numerical fitting of a model against data. It is often necessary to place
;   constraints on how the parameter value can be varied (if at all) for a particular
;   fit. This class provides the methods required to do so, for example, controlling
;   the maximum step size utilised per iteration, placing bounds on the allowed values, 
;   coupling parameter values together etc. The data structure of this class has been
;   designed to mirror that which is required for the fitting program MPFIT 
;   (C. B. Markwardt) but should be general enough to be readily adapted for any fitting
;   routine.
;
; USE:
;   Example of stand alone use to follow at later date. Currently only used 
;   indirectly by FFS system parser.
;
; INITIALISATION SYNTAX:
;   par = obj_new('ffs_par', parname=parname, value=value, error=error, fixed=fixed, $
;       limited=limited, limits=limits, hardlimits=hardlimits, hardmin=hardmin, $
;       hardmax=hardmax, step=step, relstep=relstep, mpside=mpside, mpmaxstep=mpmaxstep, $
;       coupled=coupled, mpprint=mpprint, debug=debug)
;   PARAMETERS:
;     None.
;   KEYWORDS:
;     parname       -   passed to 'setparname', see below for method details.
;     value         -   passed to 'setvalue', see below for method details.
;     error         -   double; error asssociated with the parameter. ?????????????????????
;     fixed         -   passed to 'setfixed', see below for method details. 
;     limited       -   passed to 'setlimited', see below for method details.
;     limits        -   passed to 'setlimits', see below for method details.
;     hardlimits    -   passed to 'sethardlimits', see below for method details.
;     hardmin       -   passed to 'sethardmin', see below for method details.
;     hardmax       -   passed to 'sethardmax', see below for method details.
;     step          -   passed to 'setstep', see below for method details.
;     relstep       -   passed to 'setrelstep', see below for method details.
;     mpside        -   passed to 'setmpside', see below for method details.
;     mpmaxstep     -   passed to 'setmpmaxstep', see below for method details.
;     coupled       -   passed to 'setcoupled', see below for method details.
;     mpprint       -   passed to 'setmpprint', see below for method details.
;     debug         -   set this keyword to enable debug information to be printed to the terminal.
;
; PUBLIC ROUTINES:
;   In addtion to the methods listed below, this object inherits methods from ffs_primitive
;   - refer to this class' documentation for more details.
;   ............................................................................
;   [setmpfitstr]
;     PURPOSE:
;	Sets all of the necessary object state data via a structure
;       of the form required by MPFIT.
;     INPUTS:
;	None.
;     OUTPUTS:
;	Returns a 1 or a 0 to signify success and failure respectively.
;     SIDE EFFECTS:
;	None.
;   ............................................................................
;   [getmpfitstr]
;     PURPOSE:
;	Retrieves all of the necessary object state data and forms the structure
;       required by MPFIT.
;     INPUTS:
;	None.
;     OUTPUTS:
;	Returns an MPFIT parameter structure.
;     SIDE EFFECTS:
;	None.
;   ............................................................................
;   [setname]
;     PURPOSE:
;	Sets the parameter's name; used to identify par at higher level in FFS. 
;       Note that this knowingly duplicates setparname functionality.
;     INPUTS:
;	String; the name of the parameter, should be unique within a given element.
;     OUTPUTS:
;	Returns a 1 or a 0 to signify success and failure respectively.
;     SIDE EFFECTS:
;	None.
;   ............................................................................
;   [getname]
;     PURPOSE:
;	Retrieves the parameter's name.
;     INPUTS:
;	None.
;     OUTPUTS:
;       String; the name of the parameter.
;     SIDE EFFECTS:
;	None.
;   ............................................................................
;   [setparname]
;     PURPOSE:
;	Sets the parameter's name; used to identify par at higher level in FFS. 
;       Note that this knowingly duplicates setname functionality.
;     INPUTS:
;	String; the name of the parameter, should be unique within a given element.
;     OUTPUTS:
;	Returns a 1 or a 0 to signify success and failure respectively.
;     SIDE EFFECTS:
;	None.
;   ............................................................................
;   [setvalue]
;     PURPOSE:
;	Sets the parameter's value.
;     INPUTS:
;	double; the value of the parameter.
;     OUTPUTS:
;	Returns a 1 or a 0 to signify success and failure respectively.
;     SIDE EFFECTS:
;	None.
;   ............................................................................
;   [getvalue]
;     PURPOSE:
;	Retrieves the parameter's value.
;     INPUTS:
;	None.
;     OUTPUTS:
;	double; the value of the parameter.
;     SIDE EFFECTS:
;	None.
;   ............................................................................
;   [setfixed]
;     PURPOSE:
;	Specifies if the parameter value is allowed to vary during a fit.
;     INPUTS:
;	integer; 1 or 0 (true or false)
;     OUTPUTS:
;	Returns a 1 or a 0 to signify success and failure respectively.
;     SIDE EFFECTS:
;	None.
;   ............................................................................
;   [getfixed]
;     PURPOSE:
;	Retrieves the value of 'fixed'.
;     INPUTS:
;	None.
;     OUTPUTS:
;	integer; 1 or 0 (true or false)
;     SIDE EFFECTS:
;	None.
;   ............................................................................
;   [setlimited]
;     PURPOSE:
;	Sets whether parameter has lower/upper bounds.
;     INPUTS:
;       integer, two element array with possible values 0 or 1;
;       to signify if the parameter is bound on lower/upper limit values
;       or not. e.g [0,1] indicates no bound on the low side and a bound
;       on the upper side.
;     OUTPUTS:
;	Returns a 1 or a 0 to signify success and failure respectively.
;     SIDE EFFECTS:
;	None.
;   ............................................................................
;   [getlimited]
;     PURPOSE:
;	Retrieves the value of 'limited'
;     INPUTS:
;       None.
;     OUTPUTS:
;	integer, two element array with possible values 0 or 1;
;       to signify if the parameter is bound on lower/upper limit values
;       or not. e.g [0,1] indicates no bound on the low side and a bound
;       on the upper side.
;     SIDE EFFECTS:
;	None.
;   ............................................................................
;   [setmin]
;     PURPOSE:
;	Sets the parameter's lower (soft) limit.
;     INPUTS:
;       double; the value of the lower bound.
;     OUTPUTS:
;	Returns a 1 or a 0 to signify success and failure respectively.
;     SIDE EFFECTS:
;	None.
;   ............................................................................
;   [getmin]
;     PURPOSE:
;	Retrieves the value of the lower bound.
;     INPUTS:
;       None.
;     OUTPUTS:
;	double; the value of the lower bound.
;     SIDE EFFECTS:
;	None.
;   ............................................................................
;   [setmax]
;     PURPOSE:
;	Sets the parameter's upper (soft) limit.
;     INPUTS:
;       double; the value of the upper bound.
;     OUTPUTS:
;	Returns a 1 or a 0 to signify success and failure respectively.
;     SIDE EFFECTS:
;	None.
;   ............................................................................
;   [getmax]
;     PURPOSE:
;	Retrieves the value of the upper bound.
;     INPUTS:
;       None.
;     OUTPUTS:
;	double; the value of the upper bound.
;     SIDE EFFECTS:
;	None.
;   ............................................................................
;   [sethardlimits]
;     PURPOSE:
;	Sets the parameter's hard limits.
;     INPUTS:
;       double, two element array; specifies the lower/upper hard
;       limits for the parameter value. These values would normally
;       be set to prevent values being selected that will cause
;       evaluation of an FFS element to fail. These are, in a sense,
;       limits placed upon the other soft limits detailed above.
;     OUTPUTS:
;	Returns a 1 or a 0 to signify success and failure respectively.
;     SIDE EFFECTS:
;	None.
;   ............................................................................
;   [gethardlimits]
;     PURPOSE:
;	Sets the parameter's hard limits.
;     INPUTS:
;       None.
;     OUTPUTS:
;	double, two element array; specifies the lower/upper hard
;       limits for the parameter value. These values would normally
;       be set to prevent values being selected that will cause
;       evaluation of an FFS element to fail. These are, in a sense,
;       limits placed upon the other soft limits detailed above.
;     SIDE EFFECTS:
;	None.
;   ............................................................................
;   [sethardmin]
;     PURPOSE:
;	Sets the parameter's lower hard limit.
;     INPUTS:
;       double; the value for the lower hard limit.
;     OUTPUTS:
;	Returns a 1 or a 0 to signify success and failure respectively.
;     SIDE EFFECTS:
;	None.
;   ............................................................................
;   [gethardmin]
;     PURPOSE:
;	Retrieves the parameter's lower hard limit.
;     INPUTS:
;       None.
;     OUTPUTS:
;	double; the value for the lower hard limit.
;     SIDE EFFECTS:
;	None.
;   ............................................................................
;   [sethardmax]
;     PURPOSE:
;	Sets the parameter's upper hard limit.
;     INPUTS:
;       double; the value for the upper hard limit.
;     OUTPUTS:
;	Returns a 1 or a 0 to signify success and failure respectively.
;     SIDE EFFECTS:
;	None.
;   ............................................................................
;   [gethardmax]
;     PURPOSE:
;	Retrieves the parameter's upper hard limit.
;     INPUTS:
;       None.
;     OUTPUTS:
;	double; the value for the upper hard limit.
;     SIDE EFFECTS:
;	None.
;   ............................................................................
;   [setlimits]
;     PURPOSE:
;	Sets the parameter's (soft) limits.
;     INPUTS:
;       double, two element array; specifies the lower/upper limits
;       for the parameter value (the range in which the user wishes
;       the par to vary during a fit).'limited' detailed above must be 
;       specified together with these limits to enforce or 
;       disable them.
;     OUTPUTS:
;	Returns a 1 or a 0 to signify success and failure respectively.
;     SIDE EFFECTS:
;	None.
;   ............................................................................
;   [getlimits]
;     PURPOSE:
;	Retrieves the parameter's (soft) limits.
;     INPUTS:
;       None.
;     OUTPUTS:
;	double, two element array; specifies the lower/upper (soft) limits
;       for the parameter value (the range in which it is free to
;       vary during a fit). Note that this should not be confused with
;       'getefflimits' (see below).
;     SIDE EFFECTS:
;	None.
;   ............................................................................
;   [getefflimits]
;     PURPOSE:
;	Retrieves the parameter's (effective) limits.
;     INPUTS:
;       None.
;     OUTPUTS:
;	double, two element array; specifies the lower/upper limits
;       for the parameter value (the range in which it is free to
;       vary during a fit). This will be the same as the soft limits
;       when limited is '[1,1]'. If either soft limit is disabled, i.e.
;       limited is '[0,1]', '[1,0]', or '[0,0]' hard limit values will be
;       substituted.
;     SIDE EFFECTS:
;	None.
;   ............................................................................
;   [setstep]
;     PURPOSE:
;	Sets the step size used for this parameter in numerical fitting.
;     INPUTS:
;       double; the iteration step size by which to alter the
;       parameter value. Notes: Setting zero step size means that
;       the step size is selected  automatically. This value is
;       overridden when relstep (see below) is set.
;     OUTPUTS:
;	Returns a 1 or a 0 to signify success and failure respectively.
;     SIDE EFFECTS:
;	None.
;   ............................................................................
;   [getstep]
;     PURPOSE:
;	Retrieves the step size used for this parameter in numerical fitting.
;     INPUTS:
;       None.
;     OUTPUTS:
;	double; the iteration step size by which to alter the
;       parameter value. Note: zero step size means that the step size is selected
;       automatically. This value is overridden when relstep (see below) is set.
;     SIDE EFFECTS:
;	None.   
;   ............................................................................
;   [setrelstep]
;     PURPOSE:
;	Sets the step size used for this parameter in numerical fitting,
;       as a fraction of the parameter value.
;     INPUTS:
;       float; the iteration step size by which to alter the
;       parameter value, as a fraction of the value. Notes: This
;       setting overrides 'step' if also present. A default step is
;       selected if the par value is zero.
;     OUTPUTS:
;	Returns a 1 or a 0 to signify success and failure respectively.
;     SIDE EFFECTS:
;	None.
;   ............................................................................
;   [getrelstep]
;     PURPOSE:
;	Retrieves the step size (as a fraction of the parameter value) used for
;       this parameter in numerical fitting,
;     INPUTS:
;       None.
;     OUTPUTS:
;	float; the iteration step size by which to alter the
;       parameter value, as a fraction of the value. Note: A default step is
;       selected if the par value is zero.
;     SIDE EFFECTS:
;	None.
;   ............................................................................
;   [setmpside]
;     PURPOSE:
;	Specific to mpfit; the sidedness of the finite 
;       difference when computing numerical derivatives. 
;     INPUTS:
;       integer;  this field
;       can take four values:
;        0 - one-sided derivative computed automatically
;        1 - one-sided derivative (f(x+h) - f(x)  )/h
;       -1 - one-sided derivative (f(x)   - f(x-h))/h
;        2 - two-sided derivative (f(x+h) - f(x-h))/(2*h)
;       Where H is the STEP parameter described above.  The
;       "automatic" one-sided derivative method will chose a
;       direction for the finite difference which does not
;       violate any constraints.  The other methods do not
;       perform this check.  The two-sided method is in
;       principle more precise, but requires twice as many
;       function evaluations.
;     OUTPUTS:
;	Returns a 1 or a 0 to signify success and failure respectively.
;     SIDE EFFECTS:
;	None.
;   ............................................................................
;   [getmpside]
;     PURPOSE:
;	Retrieves the value of mpside.
;     INPUTS:
;       None.
;     OUTPUTS:
;	integer;  this field
;       can take four values:
;        0 - one-sided derivative computed automatically
;        1 - one-sided derivative (f(x+h) - f(x)  )/h
;       -1 - one-sided derivative (f(x)   - f(x-h))/h
;        2 - two-sided derivative (f(x+h) - f(x-h))/(2*h)
;       Where H is the STEP parameter described above.  The
;       "automatic" one-sided derivative method will chose a
;       direction for the finite difference which does not
;       violate any constraints.  The other methods do not
;       perform this check.  The two-sided method is in
;       principle more precise, but requires twice as many
;       function evaluations.
;     SIDE EFFECTS:
;	None.
;   ............................................................................
;   [setcoupled]
;     PURPOSE:
;	Set the parameter to utilise a coupling object to determine it's value.
;     INPUTS:
;       object reference; a coupling object.
;     OUTPUTS:
;	Returns a 1 or a 0 to signify success and failure respectively.
;     SIDE EFFECTS:
;	None.
;   ............................................................................
;   [getcoupled]
;     PURPOSE:
;	Retrieves the coupling object reference.
;     INPUTS:
;       None.
;     OUTPUTS:
;	object reference; a coupling object (null object signifies no coupling
;       or error with coupling object).
;     SIDE EFFECTS:
;	None.
;   ............................................................................
;   [uncouple]
;     PURPOSE:
;	Destroys coupling object - uncouples this parameter.
;     INPUTS:
;       None.
;     OUTPUTS:
;	Returns a 1 or a 0 to signify success and failure respectively.
;     SIDE EFFECTS:
;	None.
;   ............................................................................
;   [setmpmaxstep]
;     PURPOSE:
;	Sets the maximum change allowed in parameter value in one fitting 
;       iteration.
;     INPUTS:
;       double; the maximum change to be made in the parameter value.  During the
;       fitting process, the parameter will never be changed by more than this
;       value in one iteration. A value of 0 indicates no maximum.
;     OUTPUTS:
;	Returns a 1 or a 0 to signify success and failure respectively.
;     SIDE EFFECTS:
;	None.
;   ............................................................................
;   [getmpmaxstep]
;     PURPOSE:
;	Retrieves the maximum change allowed in parameter value in one fitting 
;       iteration.
;     INPUTS:
;       None.
;     OUTPUTS:
;	double; the maximum change to be made in the parameter value.  During the
;       fitting process, the parameter will never be changed by more than this
;       value in one iteration. A value of 0 indicates no maximum.
;     SIDE EFFECTS:
;	None.
;   ............................................................................
;   [setmpprint]
;     PURPOSE:
;	Specific to mpfit; sets whether this parameter value will be printed by
;       the default mpfit 'iterproc' that is executed each iteration. 
;     INPUTS:
;       integer, 0 or 1; if set to 1, then the default ITERPROC (see mpfit) will
;       print the parameter value. If set to 0, the parameter value will not be 
;       printed. This tag can be used to selectively print only a few parameter
;       values out of many.  
;     OUTPUTS:
;	Returns a 1 or a 0 to signify success and failure respectively.
;     SIDE EFFECTS:
;	None.
;   ............................................................................
;   [getmpprint]
;     PURPOSE:
;	Retrieves the value of mpprint.
;     INPUTS:
;       None.
;     OUTPUTS:
;	integer, 0 or 1; if set to 1, then the default ITERPROC (see mpfit) will
;       print the parameter value. If set to 0, the parameter value will not be 
;       printed. This tag can be used to selectively print only a few parameter
;       values out of many.  
;     SIDE EFFECTS:
;	None.
;   ............................................................................
;   [setproperty]
;     PURPOSE:
;	Provides a common method for setting all object properties.
;       The appropriate set method is called depending upon the keyword used.
;     INPUTS:
;       There are keywords for each of the properties to set (name, parname,
;       value, error, fixed, limited, hardlimits, hardmin, hardmax, limits,
;       step, relstep, mpside, mpmaxstep, coupled, mpprint). The appropriate
;       value should be supplied via these (see the individual set method for
;       details).
;     OUTPUTS:
;	Returns a 1 or a 0 to signify success and failure respectively.
;     SIDE EFFECTS:
;	None.
;   ............................................................................
;   [getproperty]
;     PURPOSE:
;	Provides a common method for setting all object properties.
;       The appropriate set method is called depending upon the keyword used.
;     INPUTS:
;       Set the appropriate keyword from the following list to retrieve the 
;       property: 
;           name, parname, value, error, fixed, limited, hardlimits, hardmin, 
;           hardmax, limits, step, relstep, mpside, mpmaxstep, coupled, mpprint
;     OUTPUTS:
;	The value of the requested property (see individual get methods for details).
;     SIDE EFFECTS:
;	None.
;   ............................................................................
;
; AUTHOR:
;   Christopher Nicholas, University of Strathclyde
;   (Based on Andrew Meigs' glvoop_par__define and data structure follows that
;   of MPFIT [C. B. Markwardt, http://cow.physics.wisc.edu/~craigm/idl/idl.html] parameters).
;
; VERSION HISTORY:
;        CHN 17/06/2004
;        * Copied from glvoop_par__define. Removed code in place for "parameter_template" method. 
;          This was a pointless method that was a middle man in calling                           
;          getState() this may as well be done directly. Therefore changed calls                  
;          to parameter_template to getState calls                                                
;        CHN 23/06/2004
;        * Added separate set methods for all variables, then changed setProperty to call these methods.
;        CHN 07/07/2004
;        * Documented methods
;        AGM 04/08/2004
;        * Changed annoying setValue, Value=value to setValue, value for all of these set*'
;          (not setProperty obviously). Ran the parinittest and parsetgettest with no obvious
;          problems. Will now run the glvoop_modelelement tests to verify the next level of 
;          behaviour.
;        AGM 27/08/2004
;        * "restored" all the functionality of glvoop_parresult__define.pro to reduce the 
;          number of objects being carried around by the project...
;        AGM 05/11/2004
;        * coupling data element added and get/setCoupling (just copies tied, but will be the
;          model/element/par space equivalent)
;        AGM  18/05/2007
;        * renamed to ffs_ (framework feature synthesis).
;        AGM 06/06/2007
;        * added evaluate method which just calls getValue (symmetry and maybe useful in the 
;          algebra/parsing that needs to be added
;        CHN 07/06/2007
;        * lots of changes to incorporate possibility of a par not used for fitting
;          complete with new pointer data member (pointing to a strucutre with any additional information
;          required) also works as a flag as to whether it is 'static' or 'fittable' (PTR_VALID test)
;        * cleaned up modification history comments to current ffs standard format :)
;        * extracted test procedures to seperate file (ffs_par_tests.pro) to keep this cleaner
;   1.1  ADW 10/08/2007
;        * Initial commit to CVS.
;   1.2  CHN 21/08/2007                                                                            
;        * added getName method                                                                    
;   1.3  CHN 20/09/2007                                                                            
;        * Removed catch block.                                                                    
;   1.4  CHN 27/09/2007                                                                            
;        * added the following methods for par limits:                                             
;          - getMin                                                                                
;          - setMin                                                                                
;          - getMax                                                                                
;          - setMax                                                                                
;        * removed remaining references to "staticpardata" pointer - now ffs_prop                  
;          class used for this purpose.                                                            
;        CHN 15/10/2007                                                                            
;        * setValue method now allows strings to be passed in, with conversion to type double.     
;        * removed'tied' data member - not going to use mpfit coupling                             
;        * renamed 'coupling' data member to 'coupled' - going to be new coupling object.          
;   1.5  ADW 16/10/2007                                                                            
;        * Set all parameters to be limited with limits of +/- infinity, will                      
;          be replaced when hard limits are implemeneted.                                          
;   1.6  ADW 17/10/2007                                                                            
;        * Caught for memory leak if the same parameter is coupled twice.                          
;   1.7  CHN 19/10/2007                                                                            
;        * Added the following data members:                                                       
;          hardlimits                                                                              
;        * Added the following methods:                                                            
;          - setHardLimits                                                                         
;          - getHardLimits                                                                         
;          - setHardMin                                                                            
;          - getHardMin                                                                            
;          - setHardMax                                                                            
;          - getHardMax                                                                            
;        * Modified setLimits to:                                                                  
;          - allow input of limits in either order (max,min) or (min/max)                          
;          - check validity with respect to hardLimits                                             
;          - check if par value is outside new limit range, if so, move value                      
;            to nearest limit                                                                      
;   1.8  CHN 24/10/2007                                                                            
;        * Altered setMin/setMax:                                                                  
;          - changed to functions (for error checking)                                             
;          - changed calls to setLimits - as these are also functions now                          
;   1.9  CHN 05/12/2007                                                                            
;        * Parameter now set fixed in 'setcouple'.                                                 
;        * Added 'unCouple' method.                                                                
;   1.10 CHN 07/12/2007                                                                            
;        * Removed 'evaluate' method - inappropriate for par - it has getvalue.                    
;        * Updated to include error essage setting within 'setlimits' method.                      
;        * Simplified sethardmin / hardmax return statements.                                      
;        * Changed default limits to use !values.d_infinity rather than                            
;          single-precision equivalent.                                                            
;   1.11 CHN 19/12/2007                                                                            
;        * Major cleanup of this class.                                                            
;        * Now inherits from ffs_primitive - replacement for cn_primitive.                         
;        * Removed the following methods:                                                          
;          - getstate                                                                              
;          - setstate                                                                              
;          - rangecheck                                                                            
;          - validatetagvalue                                                                      
;          - validateinputstructure                                                                
;          - help (this functionality remains from inherited object ffs_primitive)                 
;          - copy                                                                                  
;        * Added the following methods:                                                            
;          - setname                                                                               
;          - getname                                                                               
;        * Converted all procedure methods to functions to allow for error checking.               
;        * Any previous use of 'validatetagvalue' is now replaced with calls to 'size'             
;          function                                                                                
;        * Modified 'setproperty' to include 'hardlimits', 'hardmin' and 'hardmax' keywords.       
;        * Simplified 'setproperty' if blocks to single line if statements                         
;        * Changed implementation of 'getproperty' now set keyword switch for variable required.   
;        * Removed any superfluous in-line documentation.                                          
;        * Removed any unecessary use of '_Extra' keyword.                                         
;        * Added debug keyword to init - if set this will print any error messages                 
;          and/or other debug information to the terminal.                                         
;   1.12 ADW 20/12/2007                                                                            
;        * Fixed some documentation.                                                               
;   1.13 CHN 20/12/2007                                                                            
;        * Removed the concept of partype.
;   1.14 CHN 10/01/2007
;        * Re-write of documentation header.  
;        * Removed duplicate 'getname method'.
;   1.15 CHN 14/08/2008                                                       
;        * Renamed 'getmpfstr' to 'getmpfitstr'.
;        * Added 'setmpfitstr' method.
;        * Updated doc.
;   1.16 CHN 07/11/2008
;        * Improved error message in 'setlimits'.
;        * Added methods:
;          - setlogdep
;          - getlogdep
;        * re-formatted long lines in 'setproperty'.
;        * added 'logdep' to 'setproperty' & 'getproperty'.
;        * added 'logdep' to 'init' and state variables.
;   1.17 CHN 13/11/2008
;        * Improved error messages for failed 'setvalue'.
;   1.18 CHN 21/11/2008
;        * Added 'no_destroy' keyword and related functionality to
;          'uncouple'.
;   1.19 CHN 19/06/2009
;        * Added new method 'getefflimits'
;        * Re-arranged error checking in setvalue for readability.
;        * Modified 'sethardlimits'
;          - added error checking
;          - setting hard limits sets limited to [0,0] rather than [1,1]
;        * Added 'limited' keyword and associated functionality to setlimits.
;        * Fixed setlogdep - copy/paste error meant that this method was setting
;          self.mpprint instead!
;        * Modified init such that initial hardlimits are -infinity and 
;          +infinity rather than 'NAN'. Also utilised method calls rather
;          than directly altering the state variables.
;   1.20 ADW 02/07/2009
;        * Efficiency increases
;          - Significant speedup of getvalue by referencing self.coupled
;            directly instead of via self->getcoupled()
;          - Added /fast keyword to setvalue.
;   1.21 CHN 22/03/2011
;        * Modified 'sethardlimits':
;          - move soft limits within hardlimits when set.
;        * Modified 'setlimits':
;          - bug; hardlimited flag incorrectly set.
;----------------------------------------------------------------------
;
; First version distributed with ADAS.
;
; MODIFIED   :
;       1.1     Martin O'Mullane
;                 - First central ADAS version.
;
; VERSION    :
;       1.1    04-01-2013
;
;----------------------------------------------------------------------
;-
function ffs_par::setmpfitstr, mpfitstr
    if self->setname(mpfitstr.parname) eq 0 then return, 0
    if self->setvalue(mpfitstr.value) eq 0 then return, 0
    if self->setfixed(mpfitstr.fixed) eq 0 then return, 0
    if self->setlimited(mpfitstr.limited) eq 0 then return, 0
    if self->setlimits(mpfitstr.limits) eq 0 then return, 0
    if self->setstep(mpfitstr.step) eq 0 then return, 0
    if self->setrelstep(mpfitstr.relstep) eq 0 then return, 0
    if self->setmpside(mpfitstr.mpside) eq 0 then return, 0
    if self->setmpmaxstep(mpfitstr.mpmaxstep) eq 0 then return, 0
    if self->setmpprint(mpfitstr.mpprint) eq 0 then return, 0
    return, 1
end

;+
;NAME:
;getmpfitstr
;-
FUNCTION ffs_par::getmpfitstr
  return, {parname:self->getname(), $
    value:self->getvalue(), $
    fixed:self->getfixed(), $
    limited:self->getlimited(), $
    limits:self->getlimits(), $
    step:self->getstep(), $
    relstep:self->getrelstep(), $
    mpside:self->getmpside(), $
    mpmaxstep:self->getmpmaxstep(), $
    mpprint:self->getmpprint() $
    }

END

;+
;NAME:
;setName
;-
function ffs_par::setName, name
  return, self->setparname(name)
end


;+
;NAME:
;getName
;-
FUNCTION ffs_par::getName
   return, self->getparname()
END


;+
;NAME:
;setParName
;-
function ffs_par::setParName, parname
  if size(parname, /type) eq 7 then begin
    self.parname = parname
    return, 1
  endif else return, self->seterrmsg('Input parname not of type string.')
end


;+
;NAME:
;getParName
;-
FUNCTION ffs_par::getParName
   return, self.parname
END


;+
;NAME:
;setValue
;-
function ffs_par::setValue, value,fast=fast

  if keyword_set(fast) then begin
  	self.value=value
        return,1
  endif

  ON_IOERROR, jump
  
    if (size(value, /type) ge 2 and size(value, /type) le 4) $
      or size(value, /type) EQ 7 $
      then value = double(value) 
    
    if size(value, /type) ne 5 then return, self->seterrmsg('value set fail - must be a float / double')
    if value lt self->gethardmin() then return, self->seterrmsg('attempted to set value below lower hard limit')
    if value gt self->gethardmax() then return, self->seterrmsg('attempted to set value above upper hard limit')
    
    if (self->getlimited())[0] && value lt self->getmin() then if self->setmin(value) eq 0 then $
	return, self->seterrmsg('Input value(' +string(value) +') was outside lower soft limit,('+string(self->getmin())+') attempted to adjust, but failed: '+self->geterrmsg())
    if (self->getlimited())[1] && value gt self->getmax() then if self->setmax(value) eq 0 then $
	return, self->seterrmsg('Input value(' +string(value) +') was outside upper soft limit ('+string(self->getmax())+'), attempted to adjust, but failed: '+self->geterrmsg())
    
    self.value = value

  return, 1

jump: return, self->seterrmsg('String value passed to parameter setValue could not be converted to required type - float/double.')

end


;+
;NAME:
;getValue
;-
FUNCTION ffs_par::getValue
  IF OBJ_VALID(self.coupled) THEN $		
    RETURN, (self.coupled)->getValue() $
  ELSE RETURN, self.value
END


;+
;getError
;-
FUNCTION ffs_par::getError
   RETURN, self.error
END


;+
;NAME:
;setError
;-
function ffs_par::setError, error
  if size(error, /type) eq 4 or size(error, /type) eq 5  then begin
    self.error = error
    return, 1
  endif else return, self->seterrmsg('Input error - value not of type float/double.') 
end


;+
;NAME:
;setFixed
;-
function ffs_par::setFixed, fixed

  if size(fixed, /type) ge 2 and size(fixed, /type) le 5  then begin
    self.fixed = (fixed gt 0)
    return, 1
  endif else return, self->seterrmsg('Input error - value not of type integer.') 
end


;+
;NAME:
;getFixed
;-
FUNCTION ffs_par::getFixed
   return, self.fixed
END


;+
;NAME:
;setLimited
;-
function ffs_par::setLimited, limited
  if size(limited, /type) ge 2 and size(limited, /type) le 5  then begin
    self.limited = (limited gt 0)
    return, 1
  endif else return, self->seterrmsg('Input error - value not of type integer.')    
end


;+
;NAME:
;getLimited
;-
FUNCTION ffs_par::getLimited
   return, self.limited
END

;+
;NAME:
;getMin
;-
FUNCTION ffs_par::getMin
  RETURN, self.limits[0]
END

;+
;NAME:
;setMin
;-
FUNCTION ffs_par::setMin, minVal
  RETURN, self->setLimits([minVal, self->getMax()])
END

;+
;NAME:
;getMax
;-
FUNCTION ffs_par::getMax
  RETURN, self.limits[1]
END

;+
;NAME:
;setMax
;-
FUNCTION ffs_par::setMax, maxVal
  RETURN, self->setLimits([self->getMin(), maxVal])
END


;+
;NAME:
;setHardLimits
;-
function ffs_par::sethardlimits, hardlimits
    if ~(size(hardlimits, /type) eq 4 || size(hardlimits, /type) eq 5) then $
        return, self->seterrmsg('limits are of wrong type')        
    self.hardlimits = hardlimits
    if (where(self->getlimited() eq 1))[0] ne -1 then begin
        ;have to push any existing soft limits within new hardlimits
        softlimits = self->getlimits()
        newlimits = [(softlimits[0]>hardlimits[0])<hardlimits[1], (softlimits[1]<hardlimits[1])>hardlimits[0]]
    endif else newlimits = hardlimits
    if self->setlimits(newlimits, limited=[0,0]) eq 0 then return, 0
    return, 1
end


;+
;NAME:
;getHardLimits
;-
function ffs_par::gethardlimits, hardlimits
    return, self.hardlimits
end


;+
;NAME:
;setHardMin
;-
function ffs_par::sethardmin, minval
    return, self->sethardlimits([minval, self->gethardmax()])
end


;+
;NAME:
;getHardMin
;-
function ffs_par::gethardmin
    return, self.hardlimits[0]
end


;+
;NAME:
;setHardMax
;-
function ffs_par::sethardmax, maxval
    return, self->sethardlimits([self->gethardmin(), maxval])
end

;+
;NAME:
;getHardMax
;-
function ffs_par::gethardmax
    return, self.hardlimits[1]
end


;+
;NAME:
;setLimits
;-
FUNCTION ffs_par::setLimits, limits, limited=limited
    
  if size(limits, /type) eq 4 || size(limits, /type) eq 5 then begin
  
    ; re-order in case of [max, min] input instead of [min, max]
    limits = [(limits[0] < limits[1]), (limits[0] > limits[1])]

    ; check if hardlimits have been set
    hardLimited = FINITE(self->getHardLimits()) EQ 1
    
    IF hardLimited[0] EQ 1 THEN IF (limits[0] LT self->getHardMin()) THEN RETURN, self->seterrmsg('lower limit outside of hard minimum('+self->getname()+')')
    IF hardLimited[1] EQ 1 THEN IF (limits[1] GT self->getHardMax()) THEN RETURN, self->seterrmsg('upper limit outside of hard maximum('+self->getname()+')')

    ; check if the par value is now within these limits if not - move to closest limit
    IF NOT (limits[0] LE self->getValue() AND limits[1] GE self->getValue()) THEN BEGIN
      dummy =  MIN(limits-(self->getValue()), index)
      if self->setValue(limits[index]) eq 0 then return, 0 
    ENDIF
    
    self.limits = limits
    if n_elements(limited) eq 0 then limited = [1,1]
    if n_elements(limited) eq 1 then begin
        if limited[0] eq 0 then limited = [0,0] $
         else if limited[0] eq 1 then limited = [1,1]
    endif    
    if self->setlimited(limited) eq 0 then return, 0
    return, 1
  endif else return, self->seterrmsg('limits are of wrong type')
 
END


;+
;NAME:
;getLimits
;returns self.Limits
;-
FUNCTION ffs_par::getLimits
   return, self.limits
END

;+
;NAME:
;getefflimits
;returns effective limits
;-
function ffs_par::getefflimits
    limits = self->gethardlimits()
    idx = where(self->getlimited())
    if idx[0] ne -1 then limits[idx] = (self->getlimits())[idx]
    return, limits
end


;+
;NAME:
;setStep
;-
function ffs_par::setStep, step
  if size(step, /type) eq 4 || size(step, /type) eq 5 then begin
    self.step = step
    return, 1  
  endif else return, self->seterrmsg('Input error - value not of type float/double.') 
end

;+
;NAME:
;getStep
;returns self.step
;-
FUNCTION ffs_par::getStep
   return, self.step
END


;+
;NAME:
;setRelStep
;-
function ffs_par::setRelstep, relstep
  if size(relstep, /type) eq 4 || size(relstep, /type) eq 5 then begin
    self.relstep = relstep
    return, 1  
  endif else return, self->seterrmsg('Input error - value not of type float/double.') 
end


;+
;NAME:
;getRelStep
;returns self.relstep
;-
FUNCTION ffs_par::getRelstep
   return, self.relstep
END


;+
;NAME:
;setMpside
;-
function ffs_par::setMpside, mpside
  if size(mpside, /type) eq 2 then begin
    if mpside lt -1 or mpside gt 2 then return, self->seterrmsg('Input error - mpside must be -1,0,1 or 2.') 
    self.mpside = mpside
    return, 1  
  endif else return, self->seterrmsg('Input error - value not of type integer.') 
end


;+
;NAME:
;getMpside
;-
FUNCTION ffs_par::getMpside
   return, self.mpside
END

;+
;NAME:
;unCouple
;-
function ffs_par::unCouple, no_destroy=no_destroy
  if keyword_set(no_destroy) then self.coupled = obj_new() $
        else if obj_valid(self.coupled) then obj_destroy,self.coupled
  if self->setfixed(0) eq 0 then return, 0
  return, 1
end


;+
;NAME:
;setCoupled
;-
function ffs_par::setCoupled, coupled
  if obj_valid(coupled) eq 0 then return, self->seterrmsg('Input coupling object was an invalid object reference')
  if obj_valid(self.coupled) then obj_destroy,self.coupled 
  self.coupled = coupled 
  if self->setfixed(1) eq 0 then return, 0
  return, 1
end

;+
;name:
;getcoupled
;-
function ffs_par::getcoupled
   return, self.coupled
end


;+
;NAME:
;setMpmaxstep
;-
function ffs_par::setMpmaxstep, mpmaxstep  
  if size(mpmaxstep, /type) eq 4 || size(mpmaxstep, /type) eq 5 then begin
    self.mpmaxstep = mpmaxstep
    return, 1  
  endif else return, self->seterrmsg('Input error - value not of type float/double.')   
end

;+
;NAME:
;getMpmaxStep
;-
FUNCTION ffs_par::getMpmaxstep
   return, self.mpmaxstep
END


;+
;NAME:
;setMpprint
;-
function ffs_par::setMpprint, mpprint
  if size(mpprint, /type) eq 2 then begin
    self.mpprint = mpprint gt 0
    return, 1  
  endif else return, self->seterrmsg('Input error - value not of type integer.')    
end


;+
;NAME:
;getMpprint
;-
FUNCTION ffs_par::getMpprint
   return, self.mpprint
END


;+
;NAME:
;setlogdep
;-
function ffs_par::setlogdep, logdep
  if size(logdep, /type) eq 2 then begin
    self.logdep = logdep gt 0
    return, 1  
  endif else return, self->seterrmsg('Input error - value not of type integer.')    
end


;+
;NAME:
;getlogdep
;-
FUNCTION ffs_par::getlogdep
   return, self.logdep
END


;+
;NAME:
;setProperty
;This method sets specific object values via keywords.
;The set keywords call the applicable individual methods.
;-
function ffs_par::setProperty, $
    NAME = name, $ ;; 27-08-2004 AGM
    PARNAME = parname, $
    VALUE = value, $
    ERROR = error, $ ;; 27-08-2004 AGM
    FIXED = fixed, $
    LIMITED = limited, $
    HARDLIMITS = hardlimits, $
    HARDMIN = hardmin, $
    HARDMAX = hardmax, $
    LIMITS = limits, $
    STEP = step, $
    RELSTEP = relstep, $       
    MPSIDE = mpside, $
    MPMAXSTEP = mpmaxstep, $
    COUPLED = coupled, $
    MPPRINT = mpprint, $
    logdep = logdep

    failstring='Failed while setting parameter attributes - '

    IF N_ELEMENTS(parname) GT 0 && self->setParname(parname) eq 0 then $
        return,self->seterrmsg(failstring+self->geterrmsg())
    IF N_ELEMENTS(parname) GT 0 && self->setParname(parname) eq 0 then $
        return,self->seterrmsg(failstring+self->geterrmsg())
    IF N_ELEMENTS(name) GT 0 && self->setParname(name) eq 0 then $
        return,self->seterrmsg(failstring+self->geterrmsg())
    IF N_ELEMENTS(value) GT 0 && self->setValue(value) eq 0 then $
        return,self->seterrmsg(failstring+self->geterrmsg())
    IF N_ELEMENTS(error) GT 0 && self->setError(error) eq 0 then $
        return,self->seterrmsg(failstring+self->geterrmsg())
    IF N_ELEMENTS(fixed) GT 0 && self->setFixed(fixed) eq 0 then $
        return,self->seterrmsg(failstring+self->geterrmsg())
    IF N_ELEMENTS(limited) GT 0 && self->setLimited(limited) eq 0 then $
        return,self->seterrmsg(failstring+self->geterrmsg())
    IF N_ELEMENTS(hardlimits) GT 0 && self->sethardlimits(hardlimits) eq 0 then $
        return,self->seterrmsg(failstring+self->geterrmsg())
    IF N_ELEMENTS(hardmin) GT 0 && self->sethardlimits(hardmin) eq 0 then $
        return,self->seterrmsg(failstring+self->geterrmsg())
    IF N_ELEMENTS(hardmax) GT 0 && self->sethardlimits(hardmax) eq 0 then $
        return,self->seterrmsg(failstring+self->geterrmsg())
    IF N_ELEMENTS(limits) GT 0 && self->setLimits(limits) eq 0 then $
        return,self->seterrmsg(failstring+self->geterrmsg())
    IF N_ELEMENTS(step) GT 0 && self->setStep(step) eq 0 then $
        return,self->seterrmsg(failstring+self->geterrmsg())
    IF N_ELEMENTS(relstep) GT 0 && self->setRelstep(relstep) eq 0 then $
        return,self->seterrmsg(failstring+self->geterrmsg())
    IF N_ELEMENTS(mpside) GT 0 && self->setMpside(mpside) eq 0 then $
        return,self->seterrmsg(failstring+self->geterrmsg())
    IF N_ELEMENTS(mpmaxstep) GT 0 && self->setMpmaxstep(mpmaxstep) eq 0 then $
        return,self->seterrmsg(failstring+self->geterrmsg())
    IF N_ELEMENTS(coupled) GT 0 && self->setCoupled(coupled) eq 0 then $
        return,self->seterrmsg(failstring+self->geterrmsg())
    IF N_ELEMENTS(mpprint) GT 0 && self->setMpprint(mpprint) eq 0 then $
        return,self->seterrmsg(failstring+self->geterrmsg())
    IF N_ELEMENTS(logdep) GT 0 && self->setlogdep(logdep) eq 0 then $
        return,self->seterrmsg(failstring+self->geterrmsg())
        
    return, 1
END

;+
;NAME:
;getProperty
;This method will return the value of the specified state variable.
;To specify which variable, set appropriate keyword to 1.
;-

function ffs_par::getproperty, $
  parname=parname, $
  name=name, $
  value=value, $
  error = error, $
  fixed=fixed, $
  limited=limited, $
  hardlimits = hardlimits, $
  hardmin = hardmin, $
  hardmax = hardmax, $
  limits=limits,$
  step = step, $
  relstep = relstep, $    
  mpside = mpside, $
  mpmaxstep = mpmaxstep, $
  coupled = coupled, $
  mpprint = mpprint, $
  logdep = logdep

  if keyword_set(mpstate) then result = self->getmpstate()
  if keyword_set(parname) then result = self->getparname()
  if keyword_set(name) then result = self->getparname()  ;note that duplicate name variable is not redundant.
  if keyword_set(value) then result = self->getvalue()
  if keyword_set(error) then result = self->geterror()
  if keyword_set(fixed) then result = self->getfixed()
  if keyword_set(limited) then result = self->getlimited()
  if keyword_set(hardlimits) then result = self->gethardlimits()
  if keyword_set(hardmin) then result = self->gethardmin()
  if keyword_set(hardmax) then result = self->gethardmax()
  if keyword_set(limits) then result = self->getlimits()
  if keyword_set(step) then result = self->getstep()
  if keyword_set(relstep) then result = self->getrelstep()
  if keyword_set(mpside) then result = self->getmpside()
  if keyword_set(mpmaxstep) then result = self->getmpmaxstep()
  if keyword_set(coupled) then result =  self->getcoupled()
  if keyword_set(mpprint) then result = self->getmpprint()
  if keyword_set(logdep) then result = self->logdep()
  if n_elements(result) gt 0 then return, result else return, !values.d_nan 

end


;+
;NAME:
;Cleanup
;calls superclass ffs_primitive cleanup method
;-
pro ffs_par::cleanup
  if obj_valid(self.coupled) then obj_destroy, self.coupled  
  self->ffs_primitive::cleanup
end


;+
;NAME:
;Init
;initialise object. Checks any passed keyword values for initialisation.
;for any set keyword, the relevant set* method is called.
;-
function ffs_par::init, $
    parname = parname, $
    value = value, $
    error = error, $
    fixed = fixed, $
    limited = limited, $
    limits = limits, $
    hardlimits = hardlimits, $
    hardmin = hardmin, $
    hardmax = hardmax, $
    step = step, $
    relstep = relstep, $        ;;the *relative* step size to be used in calculating
    mpside = mpside, $
    mpmaxstep = mpmaxstep, $
    coupled = coupled, $
    mpprint = mpprint, $
    logdep = logdep, $
    debug = debug
    
    if self->ffs_primitive::init(debug=debug) ne 1 then return, 0    
    if self->sethardlimits([-!values.d_infinity,!values.d_infinity]) eq 0 then return, 0
    if self->setproperty(parname = parname, $
        name = name, $ 
        value = value, $
        error = error, $
        fixed = fixed, $
        limited = limited, $
        hardlimits = hardlimits, $
        hardmin = hardmin, $
        hardmax = hardmax, $   
        limits = limits, $
        step = step, $
        relstep = relstep, $      
        mpside = mpside, $
        mpmaxstep = mpmaxstep, $
        coupled = coupled, $
        mpprint = mpprint, $
        logdep = logdep) eq 0 then return, 0
        
    return, 1
end


;+
;NAME:
;define
;standard class definition, inherits ffs_primitive
;-
pro ffs_par__define
  self = {ffs_par, $
          parname:'', $          ;; the name of the parameter
          value:0.0d, $          ;; the value of the parameter
          error:0.0d, $   ;; error bar on the data from the fit
          fixed:0, $             ;; fixed=1 means the parameter is fixed, 0 = free.
          limited:[0, 0], $      ;; upper,lower limited or not
          limits:[0.0d, 0.0d], $
	  hardlimits:[0.0d, 0.0d], $
          step:0.0d, $           ;;the step size for calculating the num. derivatives.
          relstep:0.0d, $        ;;the *relative* step size
          mpside:0, $
          mpmaxstep:0.0d, $
          coupled:obj_new(), $
          mpprint:0, $
          logdep:0, $
	  inherits ffs_primitive}

end


