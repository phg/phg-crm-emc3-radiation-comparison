;+
; PROJECT:
;  FFS
;
; NAME:
;  FFS_PRIMITIVE
;
; PURPOSE:
;  Base class from which most (if not all) FFS classes should inherit.
;  Supplies methods / variables commonly required by most FFS classes.
;
; EXPLANATION:
;  This class provides methods for set/ get debug information
;  flag and also for set / get of error messages. 
;  This class can be upgraded to provide new functionality
;  to all inheriting FFS classes.
;  
; USE:
;  Inherit this class as you would any other.
;
; INITIALISATION SYNTAX:
;  Not intended to be instantiated alone. Only to be used for inheritance by other
;  FFS classes.
;
; PUBLIC ROUTINES:
;  [seterrmsg]
;    PURPOSE:
;      Sets state variable errmsg with input error message string
;    INPUTS:
;      errmsg   - String. The error message to set.
;      obj      - Keyword. Indicates object return type to be used.
;    OUTPUTS:
;      Output is different, depending upong keywords set in method
;      call, but will be a typical error value for that type of variable (this
;      is for convienent use of the routine in the return statement of the
;      originating method).
;      If the 'obj' keyword is set:
;        Object reference. Null object reference.        
;      Default:
;          Integer. returns 0.
;    SIDE EFFECTS:
;      None.
;
;  [geterrmsg]
;    PURPOSE:
;     Returns the currently set error message string.
;    INPUTS:
;      None
;    OUTPUTS:
;      String - returns the currently set error message string.
;    SIDE EFFECTS:
;      None
;
;  [setdebug]
;    PURPOSE:
;      Sets debug flag.
;    INPUTS:
;      Integer - 1 or 0.
;    OUTPUTS:
;      Returns 1 if successful.
;      Returns 0 if unsuccessful.
;    SIDE EFFECTS:
;      None
;
;  [getdebug]
;    PURPOSE:
;      Returns value of debug flag.
;    INPUTS:
;      None.
;    OUTPUTS:
;      Integer - 1 or 0.
;    SIDE EFFECTS:
;      None
;
; CALLS:
;   None.
;
; SIDE EFFECTS:
;   None.
;	
; AUTHOR:
;   Christopher Nicholas, University of Strathclyde
;
; VERSION HISTORY:
;   1.1 Chris Nicholas 19-12-2007
;   	* First commit.  
;
;   1.2 Allan Whiteford 20-12-2007
;   	* Removed DOS carriage returns. 
;
;   1.3 Allan Whiteford 20-12-2007
;   	* Fixed some documentation
;
;   1.4 Chris Nicholas 14-08-2008
;       * Allowed passing of 'obj' keyword to seterrmsg to specify different
;         return type
;       * Updated doc.
;   1.5 Chris Nicholas 15-09-2008
;       * Added 'retval' keyword to 'seterrmsg' for arbitrary return value.
;   1.6 Chris Nicholas 07-07-2009
;       * Corrected statement for returning 'retval' in 'seterrmsg'.
;----------------------------------------------------------------------
;
; First version distributed with ADAS.
;
; MODIFIED   :
;       1.1     Martin O'Mullane
;                 - First central ADAS version.
;                 - Add a containerCheck method.
;                 - Add _Extra to init.
;
; VERSION    :
;       1.1    04-01-2013
;
;----------------------------------------------------------------------
;-

function ffs_primitive::containerCheck, objToCheck

  Catch, theError
  IF theError NE 0 THEN BEGIN
     Catch, /Cancel
     RETURN, 3
  ENDIF
  c =  objToCheck -> getContainer() ;; if ok then goes to next line, if not then goes to the catch error code
  return, 1

END


function ffs_primitive::seterrmsg, $
    errmsg, $
    obj=obj, $
    retval=retval
  if round(10.0*float(!version.release)) ge 62 then $
    last_routine=((scope_traceback(/structure))[n_elements(scope_traceback(/structure))-2]).routine $
  else begin
    help, calls=calls
    last_routine=strmid(calls[1], 0, strpos(calls[1], '<')-1)
  endelse
  self.errmsg=errmsg
  if self.debug eq 1 then print, '--------------------------------------------------'
  if self.debug eq 1 then print, 'error has occured in ', last_routine
  if self.debug eq 1 then print, '--> ', errmsg
  if self.debug eq 1 then print, '--------------------------------------------------'  
  if keyword_set(obj) then return, obj_new()
  if n_elements(retval) gt 0 then return, retval
  return,0
end

function ffs_primitive::geterrmsg
  return, self.errmsg
end

pro ffs_primitive::help
  help, self, /obj
end

function ffs_primitive::setdebug, debug
  self.debug = debug
  return, 1
end

function ffs_primitive::getdebug
  return, self.debug
end

pro ffs_primitive::cleanup

end


function ffs_primitive::init, debug=debug,_Extra=extra
  if keyword_set(debug) then self.debug = 1
   return,1
end


pro ffs_primitive__define
    self= { ffs_primitive, $
    	    debug:0, $
    	    errmsg:'' $
          }
end





