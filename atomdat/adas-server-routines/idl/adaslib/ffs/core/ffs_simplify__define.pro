;+
; PROJECT:
;   FFS - Framework for Feature Synthesis (Object oriented model/fitting).
;
; NAME:
;   FFS_SIMPLIFY
;
; PURPOSE:
;   Takes an FFS model and determines a 'simplified' representation utilising
;   alternative FFS elements to those currently employed, with the intention
;   of optimising both evaluation time and calculation of partial derivatives.
;
; EXPLANATION:
;   This object provides methods for generating optimal replacement elements for
;   the various parent/child combinations that can occur in a model definition.
;   
;   As an example, if an FFS model has a definition string of the form:
;       ------------------------------------------
;       (model original
;           (+
;               (broaden_gauss 
;                   (+ 
;                       (gaussian g1) 
;                       (lorentzian l1) 
;                       (broaden_lorentz 
;                           (+  
;                               (line theline) 
;                               (adas-zeeman az) 
;                           ) 
;                       blorentz) 
;                   ) 
;               bgauss)
;           )
;       )
;       ------------------------------------------
;
;   then ffs_simplify will produce a new model which would have a definition
;   string of the form:
;
;       ------------------------------------------
;       (model simplified
;            (+ 
;                (gaussian new_gauss)
;                (voigtian new_voigt)
;                (voigtian new_voigt)
;                (broaden_voigt 
;                    (+ 
;                        (adas-zeeman new_az)
;                    )
;                 new_bvoigt)
;            )
;       )
;       ------------------------------------------
;   
;   All of the new model parameters will be coupled back to those of the original
;   model. This is done with the intention that in performing spectral fitting, 
;   the simplified model will be used for evaluation, but via alteration of the
;   original model's parameters.
;
; USE:
;   model=obj_new('ffs_model')
;   parser=obj_new('ffs_parser',file='original.mdl')
;   simplifier=obj_new('ffs_simplify')
;   newmodel=simplifier->apply(model)                                          
;                                                                              
;   Note that the 'debug' keyword (inherited from ffs_primitive') can be set to 
;   enable printing of error messages to the terminal.                          
;  
; INITIALISATION SYNTAX:
;   simplifier=obj_new('ffs_simplify', debug=debug) 
;   PARAMETERS: 
;     None.
;   KEYWORDS:
;     debug    	    	-   set this keyword to 1 for diagnostic information
;                           to be printed to the terminal.
;
; PUBLIC METHODS:
;   In addtion to the methods listed below, this object inherits methods from 
;   ffs_primitive - refer to this class' documentation for more details.
;   ............................................................................
;   [apply]
;     PURPOSE:
;       main method to invoke simplification on a model reference.
;     INPUTS:
;       ref         -   object reference for the ffs_model to be 'simplified'.
;     OUTPUTS:
;       A reference to the new simplified ffs_model.
;     SIDE EFFECTS:
;       Creates a new ffs_model. This will involve creation of (amongst others)
;       ffs_model, ffs_element, ffs_par ffs_contain, ffs_prop and ffs_couple 
;       objects.
;   ............................................................................
;
; PRIVATE METHODS:
;   Although IDL does not support private routines, these are labelled as such
;   As they are not envisaged that users will typical call these methods
;   directly.
;   ............................................................................
;   [getrules]
;     PURPOSE:
;       Returns the rule list.
;     INPUTS:
;       None.
;     OUTPUTS:
;       Array of structures that represent the individual rules.
;       The structures have the following fields:
;           parent      -   string | parent object class,
;           child       -   string | child object class,
;           replacement -   string | replacement object class,
;           coupling    -   pointer| to an 2 x 'number of replacement pars'
;                                    string array that details coupling
;                                    between the original parent & child pars
;                                    and the new replacement.
;     SIDE EFFECTS:
;       None.
;   ............................................................................ 
;   [setrules]
;     PURPOSE:
;       Sets up the rule list to be used for element combination replacement.
;     INPUTS:
;       None.
;     OUTPUTS:
;       returns         -   integer | returns a 1 or a 0 to signify success and
;                           failure respectively.
;     SIDE EFFECTS:
;   ............................................................................ 
;   [cleanuprules]
;     PURPOSE:
;       Frees up pointers created in defining rule list (Including the rule list
;       pointer itself).
;     INPUTS:
;       None.
;     OUTPUTS:
;       returns         -   integer | a 1 or a 0 to signify success and failure
;                           respectively.
;     SIDE EFFECTS:
;       None.
;   ............................................................................ 
;   [checkrules]
;     PURPOSE:
;       Checks the rule list for a particular parent/child combination.
;     INPUTS:
;       ref             -   object reference | parent element,
;       child           -   object reference | child element.
;     OUTPUTS:
;       returns         -   integer | the index of the rule list identified as
;                           corresponding to the input parent/child combination.
;                           If no rule is found, a value of '-1' is returned.
;     SIDE EFFECTS:
;   ............................................................................
;   [expand_add]
;     PURPOSE:
;       Takes an add element reference and checks through its evaluation 
;       hierarchy for further add elements and extracts their child elements
;       forming a new list of children i.e. removes unnecessary add elements.
;     INPUTS:
;       ref             -   object reference | ffs_add element to expand.
;     OUTPUTS:
;       returns         -   object reference | array of the new children of the 
;                           expanded ffs_add.
;       count           -   integer (keyword) | provides number of references 
;                           being returned.
;     SIDE EFFECTS:
;       None.
;   ............................................................................
;   [expand_broaden]
;     PURPOSE:
;       To deal with the case of an 'ffs_broaden_*' element operating on an
;       'ffs_add' element.
;     INPUTS:
;       ref             -   object reference | ffs_broaden_* element to expand.
;     OUTPUTS:
;       returns         -   object reference | new representation of the
;                           original 'ref'. obj_new() is returned to signify
;                           failure i.e. invalid object reference.
;     SIDE EFFECTS:
;       Creates new 'ffs_broaden_*' and 'ffs_add' objects.
;   ............................................................................
;   [duplicate]
;     PURPOSE:
;       To create a copy of the requested ffs_element. Including parameter 
;       values and coupling.
;     INPUTS:
;       ref             -   object reference | ref of the element to be
;                           duplicated.
;       no_children     -   keyword | if set then the child elements of ref will
;                           not be duplicated and set.
;     OUTPUTS:
;       returns         -   object reference | the duplicated element. obj_new()
;                           is returned to signify failure i.e. invalid object 
;                           reference.
;     SIDE EFFECTS:
;       Creates an object of the same class as 'ref'.
;   ............................................................................
;   [bumpcplexpr]
;     PURPOSE:
;       Increments the markers in a given coupling expression.
;     INPUTS:
;       expr            -   string | the couple expression to modify.
;       increment       -   integer | number by which to increase the markers.
;       exclude1st      -   keyword | if set, does not increment the first
;                           marker.
;     OUTPUTS:
;       returns         -   integer | a 1 or a 0 to signify success and failure
;                           respectively.
;     SIDE EFFECTS:
;       None.
;   ............................................................................
;   [couple]
;     PURPOSE:
;       Creates a coupling object and sets it for the input destination.
;     INPUTS:
;       dest            -   object reference | par to be coupled.  
;       source          -   object reference | pars to couple 'dest' to.
;       expr            -   string (keyword) | expression defining how the 'dest'
;                           par is to be coupled.
;     OUTPUTS:
;       returns         -   integer | returns a 1 or a 0 to signify success and
;                           failure respectively.
;     SIDE EFFECTS:
;       Creates an 'ffs_couple' object.
;   ............................................................................
;   [replace]
;     PURPOSE:
;       Provides an appropriate replacement object for the input reference.
;     INPUTS:
;       ref             -   object reference | element to be replaced.
;     OUTPUTS:
;       returns         -   object reference | replacement element
;     SIDE EFFECTS:
;       Potentially calls some / all of: 'expand_add', 'expand_broaden', 'duplicate'
;       and 'couple'. These have object creation side effects.
;   ............................................................................      
; 
; AUTHOR:
;   Christopher Nicholas, University of Strathclyde
;
; MODIFICATION HISTORY:
;   1.1  CHN 14/08/2008
;        * First Release
;   1.2  CHN 13/11/2008
;        * Changed 'setrootelement' and 'getrootelement' calls in 'apply' as
;          these 'ffs_model' methods have been renamed.
;   1.3  CHN 23/10/2009
;        * Enabled 'expand_broaden' to work for voigt broadener.
;        * Fixed 'duplicate' - element property values were not being copied.
;   1.4  CHN 01/03/2010
;        * Made corrections to rule list for following combinations:
;          - ffs_broaden_lorentz on ffs_lorentzian
;          - ffs_broaden_lorentz on ffs_gaussian
;          - ffs_broaden_lorentz on ffs_voigtian
;          - ffs_broaden_voigt on ffs_voigtian
;          - ffs_broaden_voigt on ffs_lorentzian
;        * Added an error check in 'apply' to catch 'null' returns from
;          replacement operations.
;        * Fixed bug - root element was not being added to the new model
;          container!
;        * 'xdata' of original model (if present) is now copied across to the
;          new model - previously this had to be done manually after
;          simplification.
;----------------------------------------------------------------------
;
; First version distributed with ADAS.
;
; MODIFIED   :
;       1.1     Martin O'Mullane
;                 - First central ADAS version.
;
; VERSION    :
;       1.1    04-01-2013
;
;----------------------------------------------------------------------
;-

function ffs_simplify::getrules
    return, *self.rules
end


function ffs_simplify::setrules
    if ptr_valid(self.rules) then ptr_free
    self.rules = ptr_new( [$
    ;=============================================
    ; FFS_BROADEN_GAUSSIAN
    ;=============================================
        {parent:'ffs_broaden_gauss', $
         child:'ffs_gaussian', $
         replacement:'ffs_gaussian', $
         coupling:ptr_new([ $
                        ['pos', '(child.pos)'], $
                        ['fwhm', '(^ (+ (^ parent.fwhm 2) (^ child.fwhm 2)) 0.5)'], $
                        ['area', '(child.area)'] $
                    ]) $
        }, $
        {parent:'ffs_broaden_gauss', $
         child:'ffs_lorentzian', $
         replacement:'ffs_voigtian', $
         coupling:ptr_new([ $
                        ['pos', '(child.pos)'], $
                        ['fwhmg', '(parent.fwhm)'], $ 
                        ['fwhml', '(child.fwhm)'], $                  
                        ['area', '(child.area)'] $
                    ]) $ 
        }, $
        {parent:'ffs_broaden_gauss', $
         child:'ffs_broaden_lorentz', $
         replacement:'ffs_broaden_voigt', $
         coupling:ptr_new([ $
                        ['fwhmg', '(parent.fwhm)'], $
                        ['fwhml', '(child.fwhm)'] $
                    ]) $ 
        }, $
        {parent:'ffs_broaden_gauss', $
         child:'ffs_voigtian', $
         replacement:'ffs_voigtian', $
         coupling:ptr_new([ $
                    ['pos', '(child.pos)'], $
                    ['fwhmg', '(^ (+ (^ parent.fwhm 2) (^ child.fwhmg 2)) 0.5)'], $
                    ['fwhml', '(child.fwhml)'], $
                    ['area', '(child.area)'] $
                ]) $  
        }, $
        {parent:'ffs_broaden_gauss', $
         child:'ffs_line', $
         replacement:'ffs_gaussian', $
         coupling:ptr_new([ $
                        ['pos', '(child.pos)'], $
                        ['fwhm', '(parent.fwhm)'], $
                        ['area', '(child.intensity)'] $
                    ]) $ 
        }, $ 
    ;=============================================
    ; FFS_BROADEN_LORENTZ
    ;=============================================        
        {parent:'ffs_broaden_lorentz', $
         child:'ffs_lorentzian', $
         replacement:'ffs_lorentzian', $
         coupling:ptr_new([ $
                        ['pos', '(child.pos)'], $
                        ['fwhm', '(+ parent.fwhm child.fwhm)'], $
                        ['area', '(child.area)'] $
                    ]) $ 
        }, $
        {parent:'ffs_broaden_lorentz', $
         child:'ffs_gaussian', $
         replacement:'ffs_voigtian', $
         coupling:ptr_new([ $
                        ['pos', '(child.pos)'], $
                        ['fwhmg', '(child.fwhm)'], $
                        ['fwhml', '(parent.fwhm)'], $
                        ['area', '(child.area)'] $
                    ]) $ 
        }, $        
        {parent:'ffs_broaden_lorentz', $
         child:'ffs_voigtian', $
         replacement:'ffs_voigtian', $
         coupling:ptr_new([ $
                        ['pos', '(child.pos)'], $
                        ['fwhmg', '(child.fwhmg)'], $
                        ['fwhml', '(+ parent.fwhm child.fwhml)'], $
                        ['area', '(child.area)'] $
                    ]) $ 
        }, $
        {parent:'ffs_broaden_lorentz', $
         child:'ffs_line', $
         replacement:'ffs_lorentzian', $
         coupling:ptr_new([ $
                        ['pos', '(child.pos)'], $
                        ['fwhm', '(parent.fwhm)'], $
                        ['area', '(child.intensity)'] $
                    ]) $ 
        }, $        
    ;=============================================
    ; FFS_BROADEN_VOIGTIAN
    ;=============================================   
        {parent:'ffs_broaden_voigt', $
         child:'ffs_voigtian', $
         replacement:'ffs_voigtian', $
         coupling:ptr_new([ $
                        ['pos', '(child.pos)'], $
                        ['fwhml', '(+ parent.fwhml child.fwhml)'], $
                        ['fwhmg', '(^ (+ (^ parent.fwhmg 2) (^ child.fwhmg 2)) 0.5)'], $
                        ['area', '(child.area)'] $
                    ]) $ 
        }, $
        {parent:'ffs_broaden_voigt', $
         child:'ffs_gaussian', $
         replacement:'ffs_voigtian', $
         coupling:ptr_new([ $
                        ['pos', '(child.pos)'], $
                        ['fwhml', '(parent.fwhml)'], $
                        ['fwhmg', '(^ (+ (^ parent.fwhmg 2) (^ child.fwhm 2)) 0.5)'], $                        
                        ['area', '(child.area)'] $
                    ]) $ 
        }, $
        {parent:'ffs_broaden_voigt', $
         child:'ffs_lorentzian', $
         replacement:'ffs_voigtian', $
         coupling:ptr_new([ $
                        ['pos', '(child.pos)'], $
                        ['fwhml', '(+ parent.fwhml child.fwhm)'], $
                        ['fwhmg', '(parent.fwhmg)'], $
                        ['area', '(child.area)'] $
                    ]) $ 
        }, $
        {parent:'ffs_broaden_voigt', $
         child:'ffs_line', $
         replacement:'ffs_voigtian', $
         coupling:ptr_new([ $
                        ['pos', '(child.pos)'], $
                        ['fwhml', '(parent.fwhml)'], $
                        ['fwhmg', '(parent.fwhmg)'], $
                        ['area', '(child.intensity)'] $
                    ]) $ 
        }, $ 
    ;=============================================
    ; FFS_MULTIPLY
    ;=============================================   
        {parent:'ffs_multiply', $
         child:'ffs_gaussian', $
         replacement:'ffs_gaussian', $
         coupling:ptr_new([ $
                        ['pos', '(child.pos)'], $
                        ['fwhm', '(child.fwhm)'], $
                        ['area', '(* child.area parent.factor)'] $
                    ]) $ 
        }, $
        {parent:'ffs_multiply', $
         child:'ffs_lorentzian', $
         replacement:'ffs_lorentzian', $
         coupling:ptr_new([ $
                        ['pos', '(child.pos)'], $
                        ['fwhm', '(child.fwhm)'], $
                        ['area', '(* child.area parent.factor)'] $
                    ]) $ 
        }, $
        {parent:'ffs_multiply', $
         child:'ffs_voigtian', $
         replacement:'ffs_voigtian', $
         coupling:ptr_new([ $
                    ['pos', '(child.pos)'], $
                    ['fwhmg', 'child.fwhmg)'], $
                    ['fwhml', '(child.fwhml)'], $
                    ['area', '(* child.area parent.factor)'] $
                ]) $  
        } $        
    ] $
    )
    return, 1
end

function ffs_simplify::cleanuprules
    if ptr_valid(self.rules) then begin
        for i=0, n_elements(*self.rules)-1 do $
        if ptr_valid((*self.rules)[i].coupling) then $
        ptr_free, (*self.rules)[i].coupling
        ptr_free, self.rules
    endif
    return, 1
end

function ffs_simplify::checkRules, ref, child
    rules = self->getrules()
    parentidx = where(obj_class(ref) eq strupcase(rules.parent))
    if parentidx[0] eq -1 then return, -1
    childidx = where(obj_class(child) eq strupcase(rules[parentidx].child)) 
    if childidx eq -1 then return, -1 else return, parentidx[childidx]
    
end

function ffs_simplify::expand_add, ref, count=rtncnt
    children = ref->getchildren(count=count)
    expidx=0
    temp=0
    
    ;for all of the child elements that are 'addition' elements,
    ;extract their children and insert them in place of that element.
    if count ge 2 then begin
        if obj_isa(children[0], 'FFS_ADD') then $
            children = [self->expand_add(children[0], count=temp), children[1:count-1]]
        expidx = 0 > (expidx + temp-1)
       
        for i=1, count-2 do begin 
            temp=0 
            j=i+expidx
                    
            if obj_isa(children[j], 'FFS_ADD') then begin
                children = [children[0:j-1], self->expand_add(children[j], count=temp), $
                children[j+1:count+expidx-1]]
                expidx = 0 > (expidx + temp-1)
            endif
        endfor
        if obj_isa(children[count+expidx-1], 'FFS_ADD') then $
            children = [children[0:count+expidx-2], self->expand_add( children[count+expidx-1], count=temp)]
        
    endif else if obj_isa(children[0], 'FFS_ADD') then children = self->expand_add(children[0])
    rtncnt = n_elements(children)
    return, children
end

function ffs_simplify::expand_broaden, ref

    ;note broaden should only have one child.
    child = (ref->getchildren())[0]
    
    ;get the add element's children
    grandchildren = child->getchildren(count=gc_count)
    main_add = obj_new('ffs_add')
    
    numeric_brd = self->duplicate(ref, /no_children)

    ; get width par structure
    if obj_isa(ref, 'ffs_broaden_gauss') or obj_isa(ref, 'ffs_broaden_lorentz') then begin
        reffwhm = ref->getpars(parname='fwhm', /fitting)
        par_str = reffwhm->getmpfitstr()

        ; create a duplicate width par for the elements to be 
        ; broadened numerically from the addition.
        numericfwhm = obj_new('ffs_par')
        if numericfwhm->setmpfitstr(par_str) eq 0 then return, $
            self->seterrmsg('failed to set mpfit structure for numerical ' + $
            'broadener width in expand_broaden - ' + numericfwhm->geterrmsg(), /obj)

        ; do coupling        
        if self->couple(numericfwhm, reffwhm) eq 0 then return, $
            self->seterrmsg('failed to couple numeric broadener width to ' + $
            ' original width' + self->geterrmsg(), /obj)
            
    endif else if obj_isa(ref, 'ffs_broaden_voigt') then begin
        reffwhml = ref->getpars(parname='fwhml', /fitting)
        reffwhmg = ref->getpars(parname='fwhmg', /fitting)
        fwhml_par_str = reffwhml->getmpfitstr()
        fwhmg_par_str = reffwhmg->getmpfitstr()
        
        ; create a duplicate width par for the elements to be 
        ; broadened numerically from the addition.
        numericfwhml = obj_new('ffs_par')
        if numericfwhml->setmpfitstr(fwhml_par_str) eq 0 then return, $
            self->seterrmsg('failed to set mpfit structure for numerical ' + $
            'broadener width in expand_broaden - ' + numericfwhm->geterrmsg(), /obj)

        ; do coupling        
        if self->couple(numericfwhml, reffwhml) eq 0 then return, $
            self->seterrmsg('failed to couple numeric broadener width to ' + $
            ' original width' + self->geterrmsg(), /obj)
               
        ; create a duplicate width par for the elements to be 
        ; broadened numerically from the addition.
        numericfwhmg = obj_new('ffs_par')
        if numericfwhmg->setmpfitstr(fwhmg_par_str) eq 0 then return, $
            self->seterrmsg('failed to set mpfit structure for numerical ' + $
            'broadener width in expand_broaden - ' + numericfwhm->geterrmsg(), /obj)

        ; do coupling        
        if self->couple(numericfwhmg, reffwhmg) eq 0 then return, $
            self->seterrmsg('failed to couple numeric broadener width to ' + $
            ' original width' + self->geterrmsg(), /obj)   
    endif else return, self->seterrmsg('expand_broaden has been run on non-broadener element', /obj)

    mainbuf = obj_new()
    nbuf = obj_new()
    for i=0, gc_count-1 do begin
        ; check whether known element
        if self->checkrules(ref, grandchildren[i]) ne -1 then begin

            newbrd = self->duplicate(ref, /no_children)
            if newbrd->setname(ref->getname()+'_'+strtrim(string(i),2)) eq 0 $
            then return, obj_new()
            
            ;reffwhm = ref->getpars(parname='fwhm', /fitting)
            ;par_str = reffwhm->getmpfitstr()


            if obj_isa(ref, 'ffs_broaden_gauss') or obj_isa(ref, 'ffs_broaden_lorentz') then begin

                ; create new broadener (with duplicate par
                ; attributes) for this known element.
                newbrdfwhm = newbrd->getpars(parname='fwhm', /fitting)

                if newbrdfwhm->setmpfitstr(par_str) eq 0 then return, $
                    self->seterrmsg('failed to set mpfit structure for analytic ' + $
                    'broadener width in expand_broaden - ' + newbrdfwhm->geterrmsg(), /obj)

                if self->couple(newbrdfwhm, reffwhm) eq 0 then return, $
                    self->seterrmsg('failed to couple analytic broadener width to ' + $
                    ' original width' + self->geterrmsg(), /obj)
                
            endif else if obj_isa(ref, 'ffs_broaden_voigt') then begin
                ; create new broadener (with duplicate par
                ; attributes) for this known element.
                newbrdfwhmg = newbrd->getpars(parname='fwhmg', /fitting)
                newbrdfwhml = newbrd->getpars(parname='fwhml', /fitting)

                if newbrdfwhmg->setmpfitstr(fwhmg_par_str) eq 0 then return, $
                    self->seterrmsg('failed to set mpfit structure for analytic ' + $
                    'broadener width in expand_broaden - ' + newbrdfwhmg->geterrmsg(), /obj)

                if self->couple(newbrdfwhmg, reffwhmg) eq 0 then return, $
                    self->seterrmsg('failed to couple analytic broadener width to ' + $
                    ' original width' + self->geterrmsg(), /obj)
                if newbrdfwhml->setmpfitstr(fwhml_par_str) eq 0 then return, $
                    self->seterrmsg('failed to set mpfit structure for analytic ' + $
                    'broadener width in expand_broaden - ' + newbrdfwhml->geterrmsg(), /obj)

                if self->couple(newbrdfwhml, reffwhml) eq 0 then return, $
                    self->seterrmsg('failed to couple analytic broadener width to ' + $
                    ' original width' + self->geterrmsg(), /obj)
            
            endif
                
            ; set child of new broadnener to extracted child
            ; and add broadener as child of main add                    
            if newbrd->setchildren(grandchildren[i]) eq 0 then return, $
                self->seterrmsg(newbrd->geterrmsg(), /obj)
                
            mainbuf = [mainbuf, newbrd]
        endif else begin
            ; otherwise this element is unknown - must broaden numerically
            nbuf = [nbuf, self->duplicate(grandchildren[i])]
        endelse
    endfor

    ; Escape case - all numerical - return duplicate.
    if n_elements(nbuf) eq gc_count+1 then begin
        obj_destroy, main_add
        obj_destroy, numeric_brd
        obj_destroy, mainbuf[0]
        obj_destroy, nbuf[0]
        return, self->duplicate(ref)
    endif

    mainbuf = mainbuf[1:*]    
    
    ; if some are numerical, then add the numeric broadener to the main add, 
    ; otherwise destroy the numerical broaden object.
    ; replace all but the numeric_brd - no point
    for i=0, n_elements(mainbuf)-1 do $
        mainbuf[i] = self->replace(mainbuf[i])
    if n_elements(nbuf) ge 2 then begin
        nbuf = nbuf[1:*]  
        if n_elements(nbuf) eq 2 then begin
            if numeric_brd->setchildren(nbuf) eq 0 then return, $
                self->seterrmsg(numeric_brd->geterrmsg(), /obj)
        endif else begin
            numeric_add = obj_new('ffs_add')
            if numeric_add->setchildren(nbuf) eq 0 then return, $
                self->seterrmsg(numeric_add->geterrmsg(), /obj)
            if numeric_brd->setchildren(numeric_add) eq 0 then return, $
                self->seterrmsg(numeric_brd->geterrmsg(), /obj)
        endelse
        mainbuf = [mainbuf, numeric_brd]        
    endif else begin
        obj_destroy, numeric_brd
    endelse

    if main_add->setchildren(mainbuf) eq 0 then return, $
        self->seterrmsg(main_add->geterrmsg(), /obj)
    return, main_add        
        
end

function ffs_simplify::duplicate, ref, no_children=no_children
    subtype = ref->getsubtype()     
    newref = obj_new(obj_class(ref), subtype)  
    if obj_valid(newref) eq 0 then return, $
        self->seterrmsg('Failed to create duplicate object of type '+obj_class(ref), /obj)
     
    if newref->setname('_'+ref->getname()) eq 0 then return, $
        self->seterrmsg(newref->geterrmsg(),/obj)
    pars = ref->getpars(/fitting, count=count)                                
    newpars = newref->getpars(/fitting, count=count)                          

    for i=0, count-1 do begin                                                 
        par_str = pars[i]->getmpfitstr()                                      
        if newpars[i]->setmpfitstr(par_str)    eq 0 then return, $
            self->seterrmsg(newpars[i]->geterrmsg(), /obj)
        if self->couple(newpars[i], pars[i]) eq 0 then return, obj_new()
    endfor                                                                    

    props = ref->getpars(/static, count=count)
    newprops = newref->getpars(/static, count=count)
    for i=0, count-1 do $
        if newprops[i]->setvalue(props[i]->getvalue()) eq 0 then return, 0    
    
    if not keyword_set(no_children) then begin
        children = ref->getchildren(count=count2) 
        if count2 gt 0 then begin                                                 
            for i=0, count2-1 do children[i] = self->duplicate(children[i])         
            if newref->setchildren(children)        eq 0 then return, $
                self->seterrmsg(newref->geterrmsg(), /obj)
        endif                                                                     
    endif                                                                            
    
    return, newref                                                            
end

function ffs_simplify::bumpcplexpr, expr, increment, exclude1st=exclude1st
    marker = 0
    while ((res = stregex(strmid(expr, marker), '\$[0-9]+', len=len)) ne -1) do begin
        replace = '$'+ strtrim(string(fix(strmid(expr, marker+res+1, len))+increment), 2)
        if marker ne 0 or not keyword_set(exclude1st) then $
            expr = strmid(expr, 0, marker+res) + replace + $
            strmid(expr, marker+res+len)
        marker = marker + res + strlen(replace)
    endwhile
    return, 1
end

function ffs_simplify::couple, dest, source, expr=expr
   
    nsource = n_elements(source)

    if n_elements(dest) ne 1 then return, $
        self->seterrmsg('Must supply ONE destination par for coupling')
    if nsource lt 1 then return, $
        self->seterrmsg('Must supply at least one source par for coupling')
    if nsource gt 1 && n_elements(expr) eq 0 then return, $
        self->seterrmsg('Must explicitly specify coupling expression for' + $
            'input multiple source pars')
    
    ; If no expression supplied 
    ; (and just one source and destination - checked above) then
    ; default to simple 1-1 coupling.
    if n_elements(expr) eq 0 then expr = '(* $1 1.0)'
    
    ; Need to check if source par already has coupling.
    ; If so, should extract it's coupling string and place within new sting.
    ; Will also need to add source coupling parlist items in place 
    ; to dest coupling parlist.
    if nsource eq 1 then begin        
        if obj_valid((oldcpl = source[0]->getcoupled())) eq 1 then begin
            if expr ne '(* $1 1.0)' then begin
                oldcplexpr = oldcpl->getexpr()
                splitexpr = strsplit(expr, '( \$1 )', /regex, /extract, count=cnt)
                expr = strjoin(splitexpr, ' ('+oldcplexpr+') ')
            endif else begin
                expr = '('+oldcpl->getexpr()+')'
            endelse
            source = oldcpl->getpars()
        endif
    endif
    if nsource gt 1 then begin   
        repidx = 0
        for i=0, nsource-1 do begin 
            if obj_valid((oldcpl = source[i]->getcoupled())) eq 1 then begin
                oldcplpars = oldcpl->getpars()
                oldcplexpr = oldcpl->getexpr()

                ; revise markers 
                ; i.e. find $(insert number here)
                ; increment these numbers by n_elements(oldcplpars)
                ; rebuild expression
                
                if self->bumpcplexpr(expr, n_elements(oldcplpars)-1, $
                    /exclude1st) eq 0 then return, 0

                
                ; add on the repidx (number of variable the expression is 
                ; replacing) to all indices of the old expression
                
                if self->bumpcplexpr(oldcplexpr, repidx-1) eq 0 then return, 0

                splitexpr = strsplit(expr, '( \$'+strtrim(string(repidx),2)+ $
                    ' )', /regex, /extract, count=cnt)

                expr = strjoin(splitexpr, ' ('+oldcplexpr+') ')
                if i eq 0 then $
                    source = [oldcplpars, source[1:*]] $
                else if i eq nsource-1 then $
                    source = [source[0:nsource-2], oldcplpars] $
                else $
                    source = [source[0:i-1], oldcplpars, source[i+1:*]] 
            endif 
            repidx = n_elements(oldcplpars) + repidx
       endfor
    endif
    
    coupler = obj_new('ffs_couple', expr, source)
     
    if obj_valid(coupler) ne 1 then $
        return, self->seterrmsg('failed to create coupling object')

    return, dest->setcoupled(coupler)
end

function ffs_simplify::replace, ref
    rules = self->getrules()
    child = ref->getchildren(count = count)

    ; if no children, just straight forward replacement of object
    ; of same class as original
    if count lt 1 then return, self->duplicate(ref)
    
    ; replace child objects first.
    if count gt 0 then for i=0, count-1 do child[i] = self->replace(child[i])
    
    ; assign the replacement child objects.    
    if ref->setchildren(child) eq 0 then return, $
        self->seterrmsg(ref->geterrmsg(), /obj)
    
    ; for those with children, first, attempt to use the rule list
    match = self->checkrules(ref, child)
    
    ; if rule list has failed to supply replacement details
    ; deal with any other special case not in list.
    if match eq -1 then begin
        if obj_class(ref) eq 'FFS_ADD' then begin        
            newref = obj_new('ffs_add')
            newchildren = self->expand_add(ref, count=expaddcnt) 
            ; ridiculous overhead here - duplicate all new add children
            ; this is because 'new' children actually comprises of previous
            ; children and any others gathered by the expand_add op.
            ; The setchildren op. would destroy some of the 'new' children's
            ; references.            
            for i=0, expaddcnt-1 do newchildren[i] = self->duplicate(newchildren[i])  
            if newref->setchildren(newchildren) eq 0 then return, obj_new()
            return, newref             
        endif else begin        
           if strmatch(obj_class(ref),'ffs_broaden_*', /fold_case) $
              && count eq 1 $
              && obj_class(child[0]) eq 'FFS_ADD' then $
               return, self->expand_broaden(ref)         
        endelse
        
        ; if the element has no children, or the combination is unknown, 
        ; then just return a copy of the current element.
        if count gt 0 then print, 'unknown case: ', obj_class(ref), $
            'operating on ', obj_class(child[0]) $
        else print, obj_class(ref), ' has no child objects, so just duplicating'
        
        res = self->duplicate(ref)
       return, res
    endif
    
    ; if reached this point, then rule list is being used.
    
    gcref = child->getchildren(count=count)
   
    ; use the information from the rule list to create the new objects. 
    newref = obj_new(rules[match].replacement)
    if newref->setname('_' + ref->getname()) eq 0 then return, $
        self->seterrmsg(ref->geterrmsg(), /obj)
    newpars = newref->getpars(/fitting)
    newparnames = newref->getparnames(/fitting)
        
    if count gt 0 then $
        if newref->setchildren(gcref) eq 0 then return, $
        self->seterrmsg(newref->geterrmsg(), /obj)
        
    ;create coupling objects
    for i=0, (newref->getnumpars(/fitting))-1 do begin
        parlist=obj_new()
        coupling = *rules[match].coupling

        while (idx=stregex(coupling[1,i], '(parent|child)\.[^( |\))]+', length=length)) ne -1 do begin
            extracted = stregex(coupling[1,i], '(parent|child)\.[^( |\))]+', /extract)
            offset = strmid(extracted, 0, 5) eq 'child' ? 6 : 7
            parname = strmid(extracted, offset)



            if offset eq 6 then parref = child->getpars(parname=parname)
            if offset eq 7 then parref = ref->getpars(parname=parname)

            if size(parref, /type) ne 11 then begin
                print, 'coupling string failure'
                stop
            endif
            listidx = where(parlist eq parref) 
    
            if listidx ne -1 then begin
                coupleidx = listidx    
            endif else begin
                parlist = [parlist, parref]
                coupleidx = n_elements(parlist)-1
            endelse

            coupling[1,i] = strmid(coupling[1,i], 0, idx)+'$'+strtrim(string(coupleidx),2)+strmid(coupling[1,i], idx+length)
        endwhile 
        parlist = parlist[1:*]

        if stregex(coupling[1,i],'\( ?\$1 ?\)',/boolean) then coupling[1,i] = '(* $1 1.0)'
        
        ; search the coupling information from the rulelist for the newparnames
        ; to obtain an index to ensure correct mapping of par refs to coupling
        ; strings.
        if (nameidx = where(newparnames eq coupling[0,i])) eq -1 then return, $
            self->seterrmsg('Cannot find match in new parnames for '+coupling[0,i]+' in rule list', /obj)
        if self->couple(newpars[nameidx], parlist, expr=coupling[1, i]) eq 0 then return, obj_new()
        
    endfor
    return, newref
end

function ffs_simplify::apply, ref

    if not obj_isa(ref, 'FFS_MODEL') then return, 0
    if self->setrules() eq 0 then return, $
    self->seterrmsg('Rule list failure')
    root = self->duplicate(ref->getroot())
    replacement = self->replace(root)
    allelements = replacement->flatten(count=count)
    newmodel = obj_new('ffs_model')
    for i=0, count-1 do begin
        if obj_valid(allelements[i]) eq 0 then return, $
            self->seterrmsg('simplification has failed - one of the ' $
                + 'replacement elements is null...' + self->geterrmsg())
        if newmodel->addelement(allelements[i]) eq 0 then return, $
        self->seterrmsg(newmodel->geterrmsg(), /obj)
    endfor
        
    if newmodel->setroot(replacement) eq 0 then return, $
        self->seterrmsg(newmodel->geterrmsg(),/obj)
    
    if self->getdebug() eq 1 then begin
        print, '################################################'
        print, '                Original model
        elems =ref->getelements(count=nelems)
        for i=0, nelems-1 do begin
            print, '------------------------------------------------'
            print, elems[i]
            print, 'with pars'
            print, (pars=elems[i]->getpars(/fitting) )
        endfor
        print, '################################################'
        print, '                New model
        elems =newmodel->getelements(count=nelems)
        for i=0, nelems-1 do begin
            print, '------------------------------------------------'
            print, elems[i]
            pars=elems[i]->getpars(count=npars, /fitting)
            for j=0, npars-1 do begin
                print, '................'
                print, pars[j]
                cpl = pars[j]->getcoupled()
                if obj_valid(cpl) then begin
                    print, 'coupled to:'
                    print, cpl->getpars()
                    print, 'expression:'
                    print, cpl->getexpr()
                endif else print, 'no coupling'
            endfor
        endfor
    endif
    
    if newmodel->addelement(replacement) eq 0 then return, $
        self->seterrmsg('error when adding in root element to new model' $
            +' container...'+newmodel->geterrmsg(),/obj)
    xdata = ref->getxdata()
    if not (STRUPCASE(SIZE(xdata, /tname)) eq 'STRING') then $
        if newmodel->setxdata(xdata) eq 0 then return, $
            self->seterrmsg('failed to copy across xdata', /obj)
    return, newmodel
end

pro ffs_simplify::cleanup
    if ptr_valid(self.rules) then $
        isok = self->cleanuprules()
    self->ffs_primitive::cleanup
end

function ffs_simplify::init, $
    debug=debug 
    if self->ffs_primitive::init(debug=debug) ne 1 then return, 0
    return,1
end

pro ffs_simplify__define
    self = {FFS_SIMPLIFY, $
            rules:ptr_new(), $
            inherits ffs_primitive}
end
