;+
; PROJECT:
;   FFS - Framework for Feature Synthesis (Object oriented model/fitting).
;
; NAME:
;   FFS_MODEL
;
; PURPOSE:
;
; EXPLANATION:
;
; USE:
;
; INITIALISATION SYNTAX:
;
; PUBLIC ROUTINES:
;   In addtion to the methods listed below, this object inherits methods from 
;   ffs_primitive - refer to this class' documentation for more details.
;   ............................................................................
;   [mpfunct]
;     PURPOSE:
;       function for mpfit to use for evaluating the function
;       since it can't access the methods of the model object.
;     INPUTS:
;       x           -   the independent variable.
;       p           -   parameter values.
;       modelobj    -   object reference of the model to be used for calculation.
;     OUTPUTS:
;       Returns the modelled intensity values.
;     SIDE EFFECTS:
;       None.
;   ............................................................................
;   [iselement]
;     PURPOSE:
;       Checks if input array elements are ffs_element object references. 
;     INPUTS:
;       Object reference to check.
;     OUTPUTS:
;       Returns a 1 or a 0 to signify success and failure respectively.
;     SIDE EFFECTS:
;   ............................................................................
;   [ispar]
;     PURPOSE:
;       Checks if input array elements are ffs_par object references. 
;     INPUTS:
;       Object reference to check.
;     OUTPUTS:
;       Returns a 1 or a 0 to signify success and failure respectively.
;     SIDE EFFECTS:
;   ............................................................................
;   [getxdata]
;     PURPOSE:
;       Retrieves the object's xdata.
;     INPUTS:
;       None.
;     OUTPUTS:
;       double; array comprising the x-axis data points.
;     SIDE EFFECTS:
;       None.
;   ............................................................................
;   [setxdata]
;     PURPOSE:
;       Sets the wavelength grid that this model is intended to be placed upon
;       (This is not the same as 'wavelength' field in calculate structure).
;     INPUTS:
;       double; array comprising the x-axis data points.
;     OUTPUTS:
;       Returns a 1 or a 0 to signify success and failure respectively.
;     SIDE EFFECTS:
;       None.
;   ............................................................................
;   [setparfixed]
;     PURPOSE:
;       Sets specified parameters in specified elements to be fixed.
;     INPUTS:
;       integer(boolean); set to fix parameter.
;       Keywords to be passed to getpars to specify pars: elementname, position
;       parname, parpos, count, free, fixed, fullname.
;     OUTPUTS:
;       Returns a 1 or a 0 to signify success and failure respectively.
;     SIDE EFFECTS:
;       None.
;   ............................................................................
;   [getparfixed]
;     PURPOSE:
;       Retrieves the array of pars via method getPars then uses their getFixed 
;       method to retrieve the value of 'fixed' for each par.
;     INPUTS:
;       Keywords to be passed to getpars to specify pars: elementname, position
;       parname, parpos, count, fullname.
;     OUTPUTS:
;       integer; array of 'fixed' flag values (0 or 1).
;     SIDE EFFECTS:
;       None.   
;   ............................................................................
;   [setparerrors]
;     PURPOSE:
;       Sets the error attribute for the specified parameters.
;     INPUTS:
;       double; the error associated with the parameter value.
;       Keywords to be passed to getpars to specify pars: elementname, position,
;       parname, parpos, count, free, fixed, fullname.
;     OUTPUTS:
;       Returns a 1 or a 0 to signify success and failure respectively.
;     SIDE EFFECTS:
;       None.
;   ............................................................................
;   [getparerrors]
;     PURPOSE:
;       Retrieves the array of pars via method getPars then uses their geterror
;       method to retrieve the value of 'error' for each par.
;     INPUTS:
;       Keywords to be passed to getpars to specify pars: elementname, position
;       parname, parpos, count, fullname=fullname
;     OUTPUTS:
;       Returns the errors for the specified pars.
;     SIDE EFFECTS:
;       None.
;   ............................................................................
;   [setparvals]
;     PURPOSE:
;       Retrieves the array of parameters via method getPars then uses their 
;       setValue methods to set appropriate value in the parameter value array 
;       passed in 'parVals'
;     INPUTS:
;       if for fitting ffs_pars (/fitting) - then double; array of par values.
;       if for non-fitting ffs_props (/static) - then any type can be set as value.
;       Keywords to be passed to getpars to specify pars: elementname, position, 
;       parname, parpos, static, fitting, fixed, free, fullname.
;     OUTPUTS:
;       Returns a 1 or a 0 to signify success and failure respectively.
;     SIDE EFFECTS:
;       None.
;   ............................................................................
;   [getparvals]
;     PURPOSE:
;       Retrieves the array of parameters via method getPars then uses their 
;       getValue methods.
;     INPUTS:
;       Keywords to be passed to getpars to specify pars: elementname, position
;       parname, parpos, static, fitting, fixed, free, fullname.
;     OUTPUTS:
;       If for fitting ffs_pars (/fitting) - then double; array of par values.
;       If for non-fitting ffs_props (/static) - then pointer array to various
;       types.
;       Keyword 'count' supplies the number of par values returned.
;     SIDE EFFECTS:
;       None. 
;   ............................................................................
;   [setpars]
;     PURPOSE:
;       Clears existing parameters and adds the new parameters passed in, for
;       specified elements.
;     INPUTS:
;       object reference; array of ffs_par objects.
;       Keywords 'elementname' and 'position' specify elements to setpars for;
;       default is for all elements.
;     OUTPUTS:
;       Returns a 1 or a 0 to signify success and failure respectively.
;     SIDE EFFECTS:
;       None.
;   ............................................................................
;   [getpars]
;     PURPOSE:
;       Retrieves the specified element's parameter objects.
;     INPUTS:
;       Keywords to specify which pars are required:
;           elementname -   string; array of elementnames.
;           position    -   integer; position of element in container.
;           parname     -   string; array of par names.
;           parpos      -   integer; position in par container.
;           count       -   integer; (outward); the number of elements returned.
;           static      -   integer(boolean); set to return only ffs_props in the element.
;                           i.e. properties of the element that are non-fitting 
;                           parameters. 
;           fitting     -   integer (boolean); set to only return the ffs_pars in
;                           the element i.e. the fitting parameters.
;       Setting no keywords will default to returning all parameters.
;     OUTPUTS:
;      object reference; array of ffs_par objects 
;     SIDE EFFECTS:
;       None.
;   ............................................................................
;   [getnumpars]
;     PURPOSE:
;       Returns the number of pars in the specified element (can filter by 
;       /static, /fitting).
;     INPUTS:
;       Keywords to be passed to getpars to specify pars: elementname, position,
;       static, fitting.
;     OUTPUTS:
;       integer; the number of pars in the element.
;     SIDE EFFECTS:
;       None.
;   ............................................................................
;   [getparindex]
;     PURPOSE:
;       Return the index of the parameter, specified by elementname and parname
;       (both are required), in the flattened list of all of the model's 
;       parameters.
;     INPUTS:
;       elementname     -   the element to which the specified parameter belongs.
;       parname         -   the parameter from which to obtain the index.
;     OUTPUTS:
;       integer; the index of the parameter in a flattened list of the model
;       parameters.
;     SIDE EFFECTS:
;       None.
;   ............................................................................
;   [getparnames]
;     PURPOSE:
;       Retrieves the names of the parameters for those specified by:
;       containing element's name or position and optionally by par position.
;       It is also possible to filter fitting pars and static properties.
;     INPUTS:
;       Keywords to specify pars: 
;           elementname -   string; array of elementnames.
;           position    -   integer; position of element in container.
;           parpos      -   integer; position in par container.
;           static      -   integer (boolean); set to return only ffs_props in the element.
;                           i.e. properties of the element that are non-fitting 
;                           parameters. 
;           fitting     -   integer (boolean); set to only return the ffs_pars in
;                           the element i.e. the fitting parameters.
;       Setting no keywords will default to returning all par names.
;     OUTPUTS:
;       string; array of parnames.
;     SIDE EFFECTS:
;       None.
;   ............................................................................
;   [getnumelements]
;     PURPOSE:
;       Returns the number of pars in the element (can filter by /static, /fitting).
;     INPUTS:
;       Keywords to be passed to getpars to specify pars: static, fitting.
;     OUTPUTS:
;       integer; the number of pars in the element.
;     SIDE EFFECTS:
;       None. 
;   ............................................................................
;   [getelements]
;     PURPOSE:
;       returns an array of the elements stored within the container.
;       setting the postion or name keywords allows retrieval of specific elements.
;       by default, without supplying any keywords, the method will return ALL 
;       elements. Outward keyword count supplies the number of parameters in the
;       container.
;     INPUTS:
;       Keywords to specify elements:
;           all         -   integer(switch); enable to return all element
;           position    -   integer; position of element in container.
;           elementname -   string; array of elementnames.
;           parname     -   string; name of a parameter that the sought element 
;                           should have.
;           count       -   integer; (outward); the number of elements returned.
;       Setting no keywords will default to returning all parameters.
;     OUTPUTS:
;       object reference; array of the specified element object references.
;     SIDE EFFECTS:
;       
;   ............................................................................
;   [getelementnames]
;     PURPOSE:
;       Retrieves the array of specified elements via method getelements then 
;       uses their getname methods.
;     INPUTS:
;       position    -   integer; keyword to be passed to getelements to specify
;                       elements.
;       count       -   integer; (outward); the number of elements returned.
;     OUTPUTS:
;       string; array of element names         
;     SIDE EFFECTS:
;       
;   ............................................................................
;   [getelementtypes]
;     PURPOSE:
;       Retrieves the class type of the element specified
;     INPUTS:
;       Retrieves the array of specified elements via method getelements then 
;       utilises 'obj_class' to get the type of object.
;     OUTPUTS:
;       string; the element object class type.
;     SIDE EFFECTS:
;       None.
;   ............................................................................
;   [addelement]
;     PURPOSE:
;       Adds an ffs_element object to the model, either by object reference or
;       by element name - if the latter, then a new 'ffs_element' with that name
;       is created and added to the element.
;     INPUTS:
;       in          -   object reference; an existing ffs_element.
;       elementname -   string; the name of the new element to be added in.
;     OUTPUTS:
;       Returns a 1 or a 0 to signify success and failure respectively.
;     SIDE EFFECTS:
;       None.
;   ............................................................................
;   [setmodelname]
;     PURPOSE:
;       Sets the name of the model. Used to identify model at higher level in FFS.
;       Note that this knowingly duplicates setname functionality.
;     INPUTS:
;       String; the name of the model.
;     OUTPUTS:
;       Returns a 1 or a 0 to signify success and failure respectively. 
;     SIDE EFFECTS:
;       None. 
;   ............................................................................
;   [getmodelname]
;     PURPOSE:
;       Retrieves the model's name. Note that this knowingly duplicates getname
;       functionality.
;     INPUTS:
;       None.
;     OUTPUTS:
;       String; the name of the model.
;     SIDE EFFECTS:
;       None.
;   ............................................................................
;   [setname]
;     PURPOSE:
;       Sets the name of the model. Used to identify model at higher level in FFS.
;       Note that this knowingly duplicates setmodelname functionality.
;     INPUTS:
;       String; the name of the model.
;     OUTPUTS:
;       Returns a 1 or a 0 to signify success and failure respectively. 
;     SIDE EFFECTS:
;       None.      
;   ............................................................................
;   [getname]
;     PURPOSE:
;       Retrieves the model's name. Note that this knowingly duplicates 
;       getmodelname functionality.
;     INPUTS:
;       None.
;     OUTPUTS:
;       String; the name of the model.
;     SIDE EFFECTS:
;       None.
;   ............................................................................
;   [evaluate]
;     PURPOSE:
;       Evaluates the model.
;     INPUTS:
;       memo    -   enables buffering of previous evaluation result and parameter 
;                   values - should improve performance during fitting.
;                   If there has not been a change in parameter values, then this
;                   eliminates the need for re-calculation.
;     OUTPUTS:
;       A structure containing the following fields:
;           wavelength  -   wavelength grid for intensity values.
;           intensity   -   intensity values.
;           gridded     -   whether the intensity values are mapped to a wavelength 
;                           grid or not: 1 or 0.
;     SIDE EFFECTS:
;       None.
;   ............................................................................
;   [getresult]
;     PURPOSE:
;       Returns the saved model evaluation result structure.
;     INPUTS:
;       None.
;     OUTPUTS:
;       A structure containing the following fields:
;           wavelength  -   wavelength grid for intensity values.
;           intensity   -   intensity values.
;           gridded     -   whether the intensity values are mapped to a wavelength 
;                           grid or not: 1 or 0.
;     SIDE EFFECTS:
;       None.
;   ............................................................................
;   [findpar]
;     PURPOSE:
;       Gives which element a par belongs to
;     INPUTS:
;       A par object reference.
;     OUTPUTS:
;       The name of the element which the par belongs to.
;       Returns -1 if the par is not found in the model.
;     SIDE EFFECTS:
;       None.
;   ............................................................................
;   [fastsetup]
;     PURPOSE:
;       Sets cacheing state variables up so routines can use
;       the /fast option
;     INPUTS:
;       None
;     OUTPUTS:
;       Returns 1 if succesful.
;     SIDE EFFECTS:
;       Use of this facility assumes the model will not change
;       between calls (i.e. setup of coupling, limits etc. etc.)
;   ............................................................................
; AUTHOR:
;   Christopher Nicholas, University of Strathclyde
;   (Based on Andrew Meigs' & Chris Nicholas' glvoop_model__define)
;
; MODIFICATION HISTORY:
;        AGM 06/08/2004                                                                      
;        * added setParFixed/getParFixed                                                      
;        * added PARTYPE keyword to setParVals/getParVals and setParFixed/getParFixed             
;        AGM 07/08/2004                                                                             
;        * modified getParVals, getParFixed and getParInfo to                                     
;          use getPars directly (keyword passing) instead of the massive if's                     
;          implemented earlier. Also, addressed keyword precedence (parname                       
;          over position over partype)                                                            
;        AGM 11/08/2004                                                                             
;        * added object glvoop_plotstyle as data member and added                                 
;          method to obtain the object reference to the glvoop_plotstyle object                   
;          instanced by glvoop_model                                                              
;        AGM 12/08/2004                                                                             
;        * added obj_destroy for plotstyle object to cleanup                                      
;          method                                                                                     
;        AGM 12/08/2004                                                                             
;        * added getName method (which is just getModelName)                                      
;        AGM 16/08/2004                                                                             
;        * changed references to modelelement to element                                          
;        AGM 16/08/2004                                                                             
;        * changed data element 'elementContainer' to container                                   
;        * added method 'getContainer' (modified getElementContainer use new                      
;          data element).                                                                         
;        * Fixed places where self.elementContainer was being                                     
;          referenced directly (replaced with "self->getElementContainer()").                     
;        AGM 17/08/2004                                                                             
;        * added wrapper methods for the glvoop_plotstyle object                                  
;          so that inheritance by composition was better.                                         
;        AGM 27/08/2004                                                                             
;        * incorporated all that glvoop_modelresult__define.pro                                   
;          did into here to reduce the number of objects floating around                          
;        AGM 13/09/2004                                                                             
;        * added setPlotStyle                                                                     
;        AGM 16/09/2004                                                                             
;        * added setName.                                                                         
;        AGM 23/09/2004                                                                             
;        * I think Chris cleaned up the code and broke getPars                                    
;          (see notes in that method). I fixed it and also added a check in                       
;          getParVals on whether getPars returned a valid object (at least one).                  
;        AGM 24/09/2004                                                                             
;        * added some more functionality to setParVals: now if                                    
;          the user says model->setParvals,[200.0,400.0],parname='ctr' it will                    
;          try to handle it.                                                                      
;        AGM 01/10/2004                                                                              
;        * Added getArea to glvoop_element__define.pro and                                        
;          glvoop_model__define.pro; both just do an "INT_TABULATED" on the                       
;          function given an array of x data.                                                     
;        AGM 06/10/2004                                                                             
;        * added error method to calculate the error of the                                       
;          model from the error in the elements                                                   
;        AGM 21/10/2004                                                                             
;        * added bars methods (error bars on/off get... like draw)                                
;        AGM 22/10/2004                                                                             
;        * modified setParErrors to behave just like setParVals                                   
;          (see change log 24-09-2004)                                                                
;        AGM 22/11/2004                                                                             
;        * added minmax keyword (output) to evaluate method.                                      
;        AGM 02/02/2005                                                                             
;        * added coupling methods to define coupling of                                           
;          parameters... So far only simple elementname.parname =                                 
;          elementname.parname operator constant operator                                         
;          elementname.parname... no "(" or "[" allowed so far                                    
;        AGM 02/09/2005                                                                             
;        * added p_xdata pointer to class data and modified                                       
;          init, cleanup to take it and evaluate, convolve, getCentroid, getArea                  
;          to allow the old behaviour (x passed in) and if x not passed then                      
;          check on validity of xdata in the pointer.                                             
;        * Also, of course, added the get/set methods for xdata.                                  
;        * changed all method usage of x to xdata.                                                
;        AGM 04/09/2005                                                                             
;        * added plot method                                                                      
;        AGM 05/09/2005                                                                             
;        * added getKeywords method                                                               
;        AGM 05/09/2005                                                                             
;        * added linesymbol flag (see glvoop_plotstyle__define.pro)                               
;        AGM 06/10/2005                                                                             
;        * changed access to glvoop_plotstyle to inherit                                          
;          from glvoop_plotstyle_composition__define.pro (Strategy pattern                        
;          implemented! I think)                                                                  
;        AGM 18/05/2007                                                                             
;        * renamed to ffs_ (framework feature synthesis).                                            
;        CHN 13/06/2007                                                                             
;        * re-organised this file a bit - grouped together                                        
;          methods operating on ffs_pars and likewise with those on ffs_elements                  
;          to make it a bit easier to work with this program.                                     
;        CHN 14/06/2007                                                                             
;        * massive code reduction by removing awful if-blocks and utilising the                   
;          getPars (or getElements) methods  - whichever is most appropriate -  instead           
;        * modified multiple methods accessing ffs_pars properties via getPars method             
;          to account for new 'fitting' and 'static' keywords.                                    
;        * fixed bug in setProperty/getProperty (see routine doc)                                 
;        * added missing keyword 'elementname' to setProperty.                                    
;        * begun to implement the parser.                                                         
;        * extracted test procedures to seperate file (ffs_model_tests.pro) to keep this cleaner  
;        AGM 20/06/2007                                                                             
;        * in getELEMENTNAMES changed call to getElementName to getName to                        
;          facilitate working with models within models (not just elements                        
;          in models)                                                                             
;        * Added element parameter to addElement method (element ==                               
;          aElement) so that a non-keyword element could be used.                                 
;        * In method translateFitResults removed the recursion, which was broken.                 
;        AGM 26/06/2007                                                                             
;        * In getElements added set to count = 0 if "No such element found".                      
;        * In getPars added "ELSE count=0" for no valid elements returned.                        
;        AGM 27/06/2007                                                                             
;        * To method superClassCheck added ismodel and iselement                                  
;          keywords... to help out getElements for model-within-model                             
;          element grabbing                                                                       
;        * Modifying getElements to dig deeper into models-within-models                          
;        CHN 04/07/2007                                                                             
;        * Cleaned up the comments such that they appear as element and par :)                    
;        * added 14-06-2007 changes entry                                                         
;        CHN 09/08/2007                                                                             
;        * setXData method altered to propagate calls to set xdata for all elements within model. 
;        * added saveResult method and new result state variable.                                 
;        * simplified evaluate to use the parser's evaluate method and now saves result.                                                                                     
;   1.1  ADW 10/08/2007
;        * Initial Commit to CVS.
;   1.2  CHN 13/08/2007
;        * Updated cleanup method to prevent memory leaks.
;        * Updated modification history to include comments made in duplicate
;          copy of file that had not been merged.
;   1.3  CHN 13/08/2007
;        * missing a single comma in prev. version!:)
;   1.4  CHN 13/08/2007
;        * modified cleanup method.
;        * added getResult method.
;   1.5  AGM 16/08/2007
;        * Removed ffs_plotstyle_composition and all references to it in both
;          ffs_model__define.pro and ffs_element__define.pro. 
;        * modified ffs_model::writeIDLObject such that if it is not the one
;          called first then it does not issue the "return,..." and "end" write 
;          statements.
;   1.6  AGM 16/08/2007
;        * finished (I think) removal of plotstyle_composition and plot
;          object
;        * also removed the unfinished coupling methods which are stored in
;          ffs_model_couplingstuff_160807.pro
;        * Untested as parser part interfers currently with my evaluation.
;   1.7  AGM 29/08/2007
;        * fixed getParNames methods for case where an element had no parameters.
;   1.8  CHN 11/09/2007
;        * removed error handler blocks as IDL provides more useful debug information
;          without them.
;        * changed some inappropriate keyword_set checks on method input
;          to more appropriate n_elements check.
;        * generateParserString method added
;        * removed parserstring generation at init
;   1.9  CHN 12/09/2007
;        * syntax error from performing previous version merge.
;   1.10  CHN 20/09/2007 
;        * Init: Changed use of cn_container to new ffs_contain object.
;        * getNumElements now uses method 'count' of ffs_contain
;        * removed the following methods:
;          - getCentroid
;          - getArea
;          - getIFunct
;          - IFunct
;          - getElementContainer
;        * changed all references to getElementContainer to getContainer
;   1.11 CHN 15/10/2007 
;        * added the following methods:
;          - createFromParser
;          - setCalcString
;        * init method:
;          - now accepts 'file' keyword to create model from ffs language readable txt file.
;          - utilises setCalcString / generateParserString methods to perform model creation
;          - only indicates successful completion upon success of setcalcstring.
;        * removed gofobservable inheritance.
;   1.12 ADW 16/10/2007 
;        * Removed unncessary stop statement.
;        * Changed init method so that even if nothing is passed in
;          it still returns 1 (this is an acceptable thing to want to do).
;        * Added calls to parser->setvalues() and parser->setlimits() in
;          setcalcstring method.
;   1.13 ADW 17/10/2007 
;        * Removed printing parameters every time evaluate is called.
;        * Added call to parser->setcouple() in setcalcstring method.
;   1.14 ADW 18/10/2007 
;        * Added call to parser->setfixedfree() in setcalcstring method.
;   1.15 ADW 19/10/2007 
;        * Limits should be set before values to reflect the way
;          values can't go outside limits, swapped order of calls
;          to parser->setvalues() and parser->setlimits().
;   1.16 ADW 13/11/2007 
;        * Added memo keyword to evaluate method and pushed this down
;          to the parsers evaluate method.
;        * Made the ffs_model_mpfunct routine use the memo keyword.
;   1.17 CHN 05/12/2007 
;        * Keyword 'createfrom' is now just 'create' in 'setcalcstring'
;        * References to 'createfrom' corrected.
;        * Re-write of setparvals:
;          - now a function rather than procedure
;          - removed '_extra' keyword
;          - returns 0:1 dependent on success and sets error message accordingly. 
;        * Added method 'iselement' (checks entire array unlike 'superclass'
;          method - 'superclass' method may be removed at a later date)
;        * changed 'setparvals' calls to reflect new method.
;   1.18 CHN 07/12/2007 
;        * Updated 'createfromparser' method to check element object validity after
;          creation. Failure sets error message.
;   1.19 ADW 11/12/2007 
;        * Re-write of evaluation code, elements now know who their children are
;          and aren't told at evaluation time. Hence, a model no longer requires
;          a parser. As such:
;	    - Removed parser tag in main structure
;	    - Re-wrote evaluate to not use the parser
;	    - Removed getparserinfo, createfromparser and setcalcstring
;	    - Saveresult now just stores a pointer to an elements result,
;	      this should perhaps be re-written so that model just knows
;	      which element stores the final result and dynamically calls
;	      that element.
;   1.20 CHN 19/12/2007 
;        * Now inherits from ffs_primitive - replacement for cn_primitive.
;        * Removed the following methods:
;          - changeelementtype
;          - getproperty
;          - setproperty
;          - generateparserstring.
;        * Added 'debug' keyword for init.
;        * Removed any unecessary use of '_Extra' keyword.
;        * Modified init to reflect previous changes(11/12/2007 ADW)
;          - removed use of setcalcstring/generateparserstring
;          - removed keywords 'calcstring' and 'file'.
;        * removed keyword 'type' from init.
;        * Modified 'addelement' - now an error status returning function.
;        * Modified 'getparinfo' - now utilises 'getmpfstr' method of ffs_par
;         in place of 'getstate'.
;        * Modified multiple 'for loops' from blocks to single line statements.
;        * Modified multiple 'if' blocks to single line statements.
;        * Removed many old commented out lines of code.
;        * Removed any obviously pointless in-line comments.
;          ADW 20/12/2007 
;        * Removed DOS carriage returns.
;          ADW 20/12/2007 
;        * Evaluate method now just calls an elemental evaluate method
;          for the master element (assumed for now to be the last one in the
;          container). The API should be changed such that elements can be in
;          and order and a state variable just points at the master element,
;          this limitation was also present before and  was also above 
;          in the comment of 11/12/2007.
;          CHN 20/12/2007 
;        * Removed the following methods:
;          - help (provided by ffs_primitive.)
;          - superclasscheck
;          - gettype
;          - findelement
;          - getpartypes
;          - gettied
;          - writeidlobject
;        * Removed any 'partype' keywords.
;        * Simplified iselement method and included error message setting.
;        * Simplified getelements method - no need to support models in models anymore.
;        * Further general cleanup as before:
;          - modified multiple 'for loops' from blocks to single line statements.
;          - modified multiple 'if' blocks to single line statements.
;          - removed many old commented out lines of code.
;          - removed any obviously pointless in-line comments.
;   1.21 ADW 20/12/2007
;        * Removed dos carriages returns
;   1.22 ADW 20/12/2007
;        * Evaluate method now just calls an elemental evaluate method
;          for the master element (assumed for now to be the last one in the
;          container). The API should be changed such that elements can be in
;          and order and a state variable just points at the master element
;
;   1.23 CHN 07/01/2008 
;        * Corrected call to "iselement" in getelements (syntax error - missed out 
;          "self->" prefix).
;   1.24 CHN 23/01/2008
;        * Converted the following procedural methods to functions as part of
;          error handling strategy (this could in some cases mean substantial
;          changes to the methods):
;          - setxdata
;          - setparfixed
;          - setparerrors
;          - setpars
;          - addpar
;          - removeelement
;          - translatefitresults
;          - saveresult
;        * Added ispar method
;        * Re-arranged order of methods.
;        * Changed getparindex keywords to parameters (as they are required
;          rather than optional.
;        * Added static, fitting and parpos keywords to getparnames and passed
;          passed through to appropriate method calls
;        * Modified getparindex to use /fitting in call to getparnames due to above
;          change.
;        * Removed method 'findpar' - unused.
;        * Removed keyword elementname in call to 'getelements' within method 
;          'getelementnames'.
;        * Added keyword 'count' to 'getelements'.
;        * Removed 'error' method.
;        * Re-write of documentation header.
;   1.25 CHN 19/02/2008
;        * added 'rootelement' state variable.
;        * added methods:
;          - setrootelement
;          - getrootelement.
;        * modified 'getxdata' to assign dereferenced 'self.p_xdata' before
;           return - otherwise this can result in unintended modification
;           of state data p_xdata externally.
;        * modified 'removeelement' method:
;          - to allow for removal using object reference
;          - added name keyword (duplicates elementname)
;        * modified evaluate method to start on root element, rather 
;          than simply the last element added.
;   1.26 CHN 25/02/2008
;        * added 'validateparentage' method.
;   1.27 ADW 13/03/2008
;        * Removed /all in call to getchildren (now the default behaviour)
;   1.27 ADW 14/03/2008
;        * Added 'findpar' method.
;          Note: This new method is unrelated to the 'findpar' method removed
;                by CHN in revision 1.24
;   1.28 CHN 14/08/2008
;        * Added the following methods:
;          - getpd
;          - setsimplified
;          - getsimplified
;          - flatten
;        * Added parameter to ffs_model_mpfunct for passing partial derivatives
;          and added call to getpd here.
;        * Changed references to 'getmpfstr' to 'getmpfitstr' due to
;          rename of this par method.
;        * Modified evaluate method to utilise simplifed model if keyword set.
;        * 'simplified' object reference added as state variable of object.
;   1.29 CHN 15-09-2008
;        * Re-write of 'getpd'.
;        * Added 'fixed' and 'free' options to 'getpars'.
;   1.30 CHN 07/11/2008
;        * Added the following methods:
;          - pdsetup
;          - calcpd
;        * Added 'free' and 'fixed' keywords to:
;          - getparvals
;          - getnumpars
;          - getparnames
;        * Added 'perelement' keyword to 'getnumpars' to preserve previous
;          behaviour. Now returns total number of pars by default.
;        * Added 'fullname' keyword to 'getparnames'.
;        * Method 'translatefitresults' now operates on 'free' parameters rather
;          than 'fitting'.
;   1.31 CHN 13/11/2008
;        * Substantial change to 'getpars' to allow use of 'fullname' selection specification.
;        * Added 'fullname' keyword functionality to the following methods:
;          - setparvals
;          - getparvals
;          - setparfixed
;          - getparfixed
;          - setparerrors
;          - getparerrors
;        * Methods which have had 'fullname' functionality added may have
;          changed significantly - now grabbing parameters and operating on them
;          directly, rather than going via ffs_element equivalent methods.
;        * 'cleaned up' above methods at the same time - 4 character indentation
;          throughout and re-considered error handling etc.
;        * Re-write of getparindex. Simpler implementation.
;        * Added the following methods:
;          - execute (function)
;          - execute (procedure)
;        * Methods 'getrootelement' & 'setrootelement' renamed to 'setroot' &
;          'getroot' respectively.
;   1.32 CHN 17/11/2008
;        * Fixed bug in new getpars: added error check after using ffs_element's
;          getpars - to make sure it returned references before attempting to 
;          use them!
;   1.33 CHN 21/11/2008
;        * Changes to 'calcpd':
;          - uncouple any coupled pars to allow value changes for finite
;            difference method, then recouple.
;          - removed keyword 'relstep'; instead get this from parameter's attribute
;          - get 'step' from parameter attribute.
;          - decreased default relstep from 0.001 to 0.0001
;   1.34 ADW 02/07/2009
;        * Efficiency increases:
;          - Added fastsetup method
;          - Added /fast keyword to translatefitresults
;   1.35 CHN 07/08/2009
;        * Modified 'getpd' to get the simplified model's root element,
;          if present i.e. use optimised pd calculation!
;        * Changed a tolerence setting in checksimplified test.
;        * Modified 'setsimplified' - run pdsetup for simplified root.
;        * Modified 'evaluate':
;          - removed simplify keyword - instead just check for check for
;            presence of simplified model object.
;          - fixed bug - result was not being saved to primary model.
;   1.36 CHN 23/10/2009
;        * Increased default step size in 'calcpd'.
;   1.37 CHN 09/02/2010
;        * Modified 'setparvals' - incorrect method call to 'seterror', when
;         'seterrmsg' was intended.
;   1.38 CHN 18/02/2010
;        * 'init' was missing debug keyword (and associated pass through to
;          ffs_primitive.
;   1.39 CHN 23/08/2010
;        * Added method 'getfitstring'.
;        * Added 'count' keyword to 'getparvals'.
;   1.40 CHN 22/03/2011
;        * IMPORTANT NOTE: effectively disabled 'checksimplified'. This method
;          will now return 1 regardless of input. This has been done because the
;          current test is reporting failure for cases that are deemed
;          acceptable under manual inspection. A new update is required to
;          restore true functionality of this method.
;----------------------------------------------------------------------
;
; First version distributed with ADAS.
;
; MODIFIED   :
;       1.1     Martin O'Mullane
;                 - First central ADAS version.
;
; VERSION    :
;       1.1    04-01-2013
;
;----------------------------------------------------------------------
;-

;###############################################################################
;                     EXTERNAL ROUTINES (NOT PART OF CLASS)                    
;###############################################################################
;+
;NAME:
;evaluate wrapper
;-
FUNCTION ffs_model_mpfunct, x, p, pd, MODELOBJ=modelobj, num=num
    IF OBJ_VALID(modelobj) NE 1 THEN RETURN,-1
    IF STRUPCASE(OBJ_CLASS(modelobj)) NE 'FFS_MODEL' THEN RETURN,-1
    if modelobj->translateFitResults(p) eq 0 then return, -1
    result = modelobj->evaluate(/memo)
    pd = modelobj->getpd(num=num)
    RETURN, result.intensity
END
;###############################################################################
;                          END EXTERNAL ROUTINES
;###############################################################################

;##############################################################################
;                               MISC METHODS
;##############################################################################
function ffs_model::getfitstring, $
    free=free, $
    fitting=fitting, $
    static=static

    vals = self->getparvals(free=free, fitting=fitting, static=static, count=countvals)
    names = self->getparnames(free=free, fitting=fitting, static=static, /full, count=countnames)
    
    res = strarr(2,countvals)
    for i=0, countvals-1 do begin
        res[*, i] = [names[i], strtrim(vals[i], 2)]
    endfor
    return, res
end

;+
;NAME:
;execute
;-
pro ffs_model::execute, expr
    if self->execute(expr) eq 0 then print, self->geterrmsg()
end

;+
;NAME:
;execute
;-
function ffs_model::execute, expr
    if obj_valid((executor = obj_new('ffs_executor'))) eq 0 then return, $
        self->seterrmsg('Failed to created executor object')
    if executor->setmodel(self) eq 0 then return, $
        self->seterrmsg("Failed in applying 'self' to executor - " + $
        executor->geterrmsg())
    if executor->execute(expr) eq 0 then return, $
        self->seterrmsg("Failed when attempting to execute input " + $
        "expression - " +executor->geterrmsg())
    if obj_valid(executor) then obj_destroy, executor
    return, 1
end

;+
;NAME:
;pdsetup
;-
function ffs_model::pdsetup
    ; some analytic partial derivatives depend upon the fact that the elements
    ; will have the latest evaluation result stored. This should be the case, 
    ; but ensure it is here.
    pars = self->getpars(/free)
    dummy = self->evaluate()
    root = self->getroot()
    res = root->pdsetup(pars)
    return, 1
end

function ffs_model::fastsetup
	elements=self->getelements()
	for i=0,n_elements(elements)-1 do begin
        	if elements[i]->fastsetup() eq 0 then return,self->seterrmsg('Error setting up fast:'+elements[i]->geterrmsg())
        end
        return,1
end

;+
;NAME:
;calcpd
;-
function ffs_model::calcpd, ref
    initial = ref->getvalue()  
    if self.debug eq 1 then print, 'ffs_model numerical deriv. : ', ref->getname()
    ; temporary - must utilise better system for choosing step
    ; cannot use relstep when value is zero. Makes more sense to
    ; base step size on limits anyway. 
    limits = ref->getlimits()
    if total([(limits eq !values.d_infinity) , (limits eq -1.0*!values.d_infinity)]) eq 0 then $
        step = 0.01*abs(limits[1]-limits[0]) $
    else begin
        if (relstep = ref->getrelstep()) eq 0 then relstep=0.01
        ;move value just off zero (if that is where it is)...
        if initial eq 0.0 then initial=1.0e-10
        if (step = ref->getstep()) eq 0 then step = initial*relstep
    endelse

    ;must temporarily uncouple ref if coupled
    if obj_valid((tempcpl = ref->getcoupled())) then $
        if ref->uncouple(/no_destroy) eq 0 then $
        return, self->seterrmsg(ref->geterrmsg())
    factor=0.0
    
    ; will not check error status here for speed.
    factor = factor + ref->setvalue(initial + step)    
    fa = (self->evaluate()).intensity           

    isok = ref->setvalue(initial)   

    factor = factor + ref->setvalue(initial - step)    
    fb = (self->evaluate()).intensity           

    isok = ref->setvalue(initial)   
    ;must re-couple if par previously uncoupled.
    if obj_valid(tempcpl) then $
        if ref->setcoupled(tempcpl) eq 0 then $
            return, self->seterrmsg(ref->geterrmsg())
    return, ((fa-fb) / (factor*step))
end

;+
;NAME:
;getpd
;-
function ffs_model::getpd, num=num, relstep=relstep
    if not obj_valid((ref = self->getsimplified())) then ref = self
    root = ref->getroot()    
    
    pars = self->getpars(/free, count=pcount)
    allpars = self->getpars(/fitting)
    pd = make_array(n_elements(self->getxdata()), pcount, /double)
    if keyword_set(num) then $
        for i=0, pcount-1 do $
            pd[*, i] = ref->calcpd(pars[i]) $
    else $
        for i = 0, pcount-1 do $
            pd[*, i] =  root->getpd(pars[i], num=num)
    return, pd
end

;+
;NAME:
;checksimplified
;-
function ffs_model::checksimplified, simplified
    ;disabled check - 14/09/2010 CHN
    return, 1
    if not obj_isa(simplified,'FFS_MODEL') then return, $
        self->seterrmsg('Cannot verify input - not of type FFS_MODEL')

    origres = (self->evaluate()).intensity
    newres = (simplified->evaluate()).intensity
    dif = origres-newres
    avg = (origres + newres) / 2.0d0
    ; For just now, select regions where the value is greater than 5% of the 
    ; peak.  This is to avoid comparison of 'dif' to values of 'avg' that
    ; are small enough to be considered zero. A better method than this
    ; may be required. CHN
    idx = where(avg gt 0.05*max(avg))  
    if idx[0] eq -1 then return, $
        self->seterrmsg('No values where average is greater than 1% of peak - unsuitable test')
    dif = dif[idx]
    avg = avg[idx]

    ;if (where(abs(dif) gt 0.01*avg))[0] eq -1 then return, 1 else return, 0
end

;+
;NAME:
;setsimplified
;-
function ffs_model::setsimplified, ref
    if not obj_isa(ref, 'FFS_MODEL') then $
        return, self->seterrmsg('Require a valid ffs_model object reference.')
;    if ref->setxdata(self->getxdata()) eq 0 then return, ref->geterrmsg()    
    if self->checksimplified(ref) eq 0 then return, $
        self->seterrmsg('failed in checksimplified: '+self->geterrmsg())
    pars = self->getpars(/free)
    root = ref->getroot()
    res = root->pdsetup(pars)
    self.simplified = ref
    return, 1
end

;+
;NAME:
;getsimplified
;-
function ffs_model::getsimplified
    return, self.simplified
end

;+
;NAME:
;flatten
;-
function ffs_model::flatten, count=rtncnt
    root = self->getroot()
    return, root->flatten(count=rtncnt)
end

;+
;NAME:
;iselement
;-
FUNCTION ffs_model::iselement, ref
  if size(ref, /type) ne 11 then return, self->seterrmsg('Wrong type - must be an object reference to check if valid ffs_element.')
  dummy = where(obj_isa(ref, 'FFS_ELEMENT') eq 1, count)
  return, count eq n_elements(ref) ? 1 : 0
END

;+
;NAME:
;ispar
;-
FUNCTION ffs_model::ispar, ref
    if n_elements(ref) eq 0 then return, self->seterrmsg('Tried to check zero element variable.')
    if size(ref, /type) ne 11 then return, self->seterrmsg('Wrong type - must be an object reference to check if valid ffs_par.')
    dummy = where(obj_isa(ref, 'FFS_PAR') eq 1, count)
    return, count eq n_elements(ref) ? 1 : 0
END

;+
;NAME:
;getXdata
;-
FUNCTION ffs_model::getXdata

  IF PTR_VALID(self.p_xdata) THEN BEGIN
    result=*self.p_xdata
    return, result
  ENDIF ELSE BEGIN
     return, 'invalid' ;; return a string (can't return a undefined variable...)
  ENDELSE

END

;+
;NAME:
;setXdata
;-
function ffs_model::setXdata, xdata

  IF N_ELEMENTS(xdata) EQ 0 THEN return, self->seterrmsg('No xdata passed in')
  IF ((SIZE(xdata, /TYPE) EQ 5) OR (SIZE(xdata, /TYPE) EQ 4)) EQ 0 THEN $
    return, self->seterrmsg('xdata passed in of wrong type')
  PTR_FREE, self.p_xdata
  self.p_xdata = PTR_NEW(xdata)
  
  elements = self->getElements(/all, COUNT=count)

  FOR i=0, count-1 DO $
    if elements[i]->setXData(xdata) eq 0 then $
        return, self->seterrmsg('Failure setting xdata for element'+elements[i]->geterrmsg())
  return, 1
end

;##############################################################################
;                               END MISC METHODS
;##############################################################################


	  ;@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
	  ;@                                                    @ 
	  ;@                   START [PAR]                      @
	  ;@  Below are the methods that act on ffs_par objects @
	  ;@                                                    @
	  ;@                                                    @
	  ;@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@


;+
;NAME:
;setParFixed
;-
function ffs_model::setparfixed, $
    parfixed, $
    elementname=elementname, $
    position=position, $
    parname=parname, $
    parpos=parpos, $
    count=count, $
    free=free, $
    fixed=fixed, $
    fullname=fullname

    pars=self->getpars(elementname=elementname, position=position, $
        parname=parname, parpos=parpos, count=count, /fitting, $
        free=free, fixed=fixed, fullname=fullname)
    
    if count eq 0 then return, 0
    if n_elements(parfixed) eq 1 then parfixed = parfixed + intarr(n_elements(pars))
    for i=0, count-1 do $
        if pars[i]->setfixed(parfixed[i]) eq 0 then return, $
            self->seterrmsg(pars[i]->geterrmsg())
    return, 1
end

;+
;NAME:
;getParFixed
;-
function ffs_model::getparfixed, $
    elementname=elementname, $
    position=position, $
    parname=parname, $
    parpos=parpos, $
    count=count, $
    fullname=fullname
    
    pars=self->getpars(elementname=elementname, position=position, $
        parname=parname, parpos=parpos, count=count, /fitting, $
        fullname=fullname)   

    if count eq 0 then return, 0

    values = intarr(count)
    for i=0, count-1 do values[i] = pars[i]->getfixed()
    return, values
end

;+
;NAME:
;setParErrors
;-
function ffs_model::setparerrors, $
    errors, $
    elementname=elementname, $
    position=position, $
    parname=parname, $
    parpos=parpos, $
    count=count, $
    free=free, $
    fixed=fixed, $
    fullname=fullname

    pars=self->getpars(elementname=elementname, position=position, $
        parname=parname, parpos=parpos, count=count, /fitting, $
        free=free, fixed=fixed, fullname=fullname)
    
    if count eq 0 then return, 0

    for i=0, count-1-1 do $
        if pars[i]->seterror(errors[i]) eq 0 then return, $
            self->seterrmsg(pars[i]->geterrmsg())
    return, 1
end

;+
;NAME:
;getParErrors
;-
function ffs_model::getparerrors, $
    elementname=elementname, $
    position=position, $
    parname=parname, $
    parpos=parpos, $
    count=count, $
    fullname=fullname
    
    pars = self->getpars(elementname=elementname,  position=position, $
        parname=parname, parpos=parpos, static=static, fitting=fitting, $
        free=free, fixed=fixed, fullname=fullname, count=count)
   
    if count eq 0 then return, 0

    errors = dblarr(count)
    for i=0, count-1 do errors[i] = pars[i]->geterror()
    return, errors

end

;+
;NAME:
;setParVals
;-
function ffs_model::setparvals, $
    parvals, $
    elementref = elementref, $
    elementname = elementname, $
    position = position, $
    parname = parname, $
    parpos = parpos, $
    static=static, $
    fitting=fitting, $
    free=free, $
    fixed=fixed, $
    fullname=fullname
  
    if n_elements(fullname) gt 0 then begin
        if size(fullname, /type) ne 7 then return, $
            self->seterrmsg("'fullname' must be of type string.")
        if n_elements((temp = strsplit(strtrim(fullname, 2), '.', /extract))) eq 2 then begin
            elementname = temp[0]
            parname = temp[1]
        endif else return, self->seterrmsg("'fullname' must be specified" + $
            " in format: 'elementname.parname'")
    endif
  
    count = -1
    
    if n_elements(elementname) gt 0 $  
      or n_elements(parname) gt 0 $
      or n_elements(position) gt 0 $
      or n_elements(parpos) gt 0 $
      or keyword_set(static) gt 0 $  
    then begin
        elements = self->getelements( $
        elementname = elementname, $ 
        position = position, $
        parname = parname, $
        count=count)
    endif else if n_elements(elementref) gt 0 && self->iselement(elementref) then begin
        elements = elementref	
        count = n_elements(elementref)
    endif
  
    if count eq 0 then return, self->seterrmsg('specified elements do not exist') $
        else if count eq -1 then begin
            ;case of no elements specified because parvals is the parameter 
            ;structure for the entire model
            ;in which case - translatefitresults provides the required 
            ;functionality - so call it!
        if self->translatefitresults(parvals) eq 0 then return, self->seterrmsg('') 
    endif else begin      
        ;note that element setparvals will handle the case where
        ;a single value is passed in for multiple parameters 
        ;(with the intention of them all being set to the same value)
    
        for i=0, count-1 do $
	    if elements[i]->setparvals(parvals, parname=parname, $
            position=parpos, static=static, fitting=fitting, fixed=fixed) eq 0 then return, $
            self->seterrmsg("operation failed for element "+"'" $
            +(elements[i]->getname())+"'. Reason: "+elements[i]->geterrmsg())     
    endelse
    return, 1
end

;+
;NAME:
;getParVals
;-
function ffs_model::getparvals, $
    elementname=elementname, $
    position=position, $
    parname=parname, $
    parpos=parpos, $
    static=static, $
    fitting=fitting, $
    free=free, $
    fixed=fixed, $
    fullname=fullname, $
    count=count
    
    pars = self->getpars(elementname=elementname,  position=position, $
        parname=parname, parpos=parpos, static=static, fitting=fitting, $
        free=free, fixed=fixed, fullname=fullname, count=count)
   
    if count eq 0 then return, 0

    values = dblarr(count)
    for i = 0, count-1 do values[i] = pars[i]->getvalue()
    return, values
end

;+
;NAME:
;setPars
;-
function ffs_model::setPars,pars, ELEMENTNAME=elementname, POSITION=position

    elements = self->getElements( POSITION=position, ELEMENTNAME=elementName, COUNT=count) 
    if count eq 0 then return, self->seterrmsg('None of the specified elements were found.')  
    success=0
    
    FOR i=0, count-1 DO if elements[i]->setPars(pars) ne 0 then success=success+1
    return, success gt 0 ? 1 : self->seterrmsg('None of the specified pars'+ $
        'were found within the specified elements.') 
end

;+
;NAME:
;getPars
;-
function ffs_model::getpars, $
    elementname = elementname, $
    position = position, $
    parname = parname, $
    parpos = parpos, $
    count = count, $
    static = static, $
    fitting = fitting, $
    free = free, $
    fixed = fixed, $
    fullname=fullname
    
    if n_elements(fullname) gt 0 then begin
        fullname = strsplit(strtrim(fullname,2), ' ', /extract)
        elements=obj_new()
        pars=obj_new()
        for i=0, n_elements(fullname)-1 do begin
            if size(fullname[i], /type) ne 7 then return, $
                self->seterrmsg("'fullname' must be of type string.")
            if n_elements((temp = strsplit(strtrim(fullname[i], 2), '.', /extract))) eq 2 then begin
                if temp[0] eq '*' then $
                    etemp=self->getelements(count=ecount)$
                else $
                    etemp=self->getelements(elementname=temp[0], count=ecount)
                for j=0, ecount-1 do $
                    if temp[1] eq '*' then begin
                        ptemp = etemp[j]->getpars(static=static, $
                            fitting=fitting, free=free, fixed=fixed, $
                            count=pcount)
                        if pcount gt 0 then pars = [pars, ptemp]
                    endif else begin
                        ptemp = etemp[j]->getpars(parname=temp[1], $
                            static=static, fitting=fitting, free=free, $
                            fixed=fixed, count=pcount) 
                        if pcount gt 0 then pars = [pars, ptemp]
                    endelse
            endif else return, $
                    self->seterrmsg("input 'fullname' (" + fullname[i] + $
                    ")must be specified in format:" + "elementname.parname'")
        endfor
        if (count = n_elements(pars)-1) le 0 then return, $
            self->seterrmsg('specified pars not found.')

        return, pars[1:*]
    endif else begin
        elements = self->getelements(elementname = elementname, position = position)
        if obj_valid(elements[0]) && self->iselement(elements) eq 1 then begin
            pars=obj_new()
            for i = 0, n_elements(elements)-1 do begin
                if n_elements(fullname) gt 0 && n_elements(elements) gt 1 then parname=parname[i]
                temp = elements[i]->getpars(count = countpar, $
                    parname = parname, $
                    position = parpos, $
                    static = static, $
                    fitting = fitting, $
                    free = free, $
                    fixed = fixed)

                if countpar ne 0 then $
                    pars = [pars, temp]
            endfor
            
            if (count = n_elements(pars)-1) eq 0 then return, $
                self->seterrmsg('specified pars not found.')
            return, pars[1:*]
        endif else begin
            count = 0 
            return, 'no valid elements corresponding to specified search criteria'
        endelse
    endelse
end

;+
;NAME:
;getNumPars
;-
function ffs_model::getnumpars, $
    elementname=elementname, $
    position=position, $ 
    static=static, $  
    fitting=fitting, $
    free=free, $
    fixed=fixed, $
    perelement=perelement

    container = self->getcontainer()  
    elements = self->getelements(elementname=elementname, position=position, count=count)
    if count eq 0 then return, self->seterrmsg(retval=-1, self->geterrmsg())
    result = lonarr(count)
    for i=0, count-1 do $
           result[i] = (elements[i]->getnumpars(static=static, fitting=fitting, free=free))

    if keyword_set(perelement) then return, result $
        else return, fix(total(result))
end

;+
;NAME:
;getParIndex
;-
FUNCTION ffs_model::getParIndex, $
    elementname=elementname, $
    parname=parname, $
    fullname=fullname, $
    fitting=fitting, $
    free=free, $
    ref=ref
    
    if not keyword_set(free) then fitting=1
    
    if n_elements(ref) eq 0 then $
        ref = self->getpars(elementname=elementname, parname=parname, $
            fullname=fullname, fitting=fitting, free=free, count=count)
  
    if count ne 1 then return, $
        self->seterrmsg('Must specify ONE parameter.', retval=-1)
    
    pars = self->getpars(fitting=fitting, free=free)
    return, where(pars eq ref[0])

END

;+
;NAME:
;getParNames
;-
FUNCTION ffs_model::getParNames, $
                 ELEMENTNAME=elementname, $
                 POSITION=position, $
                 PARPOS=parpos, $
                 static=static, $
                 fitting=fitting, $
                 free=free, $
                 fixed=fixed, $
                 fullname=fullname, $
                 count=count
		 
    elements = self->getElements(ELEMENTNAME=elementName, POSITION=position)
    
    parNames = STRARR(1)
    temp = elements[0]->getParNames(position=parpos, static=static, $
        fitting=fitting, free=free, fixed=fixed)
    IF STRUPCASE(SIZE(temp, /TNAME)) EQ 'STRING' THEN begin
      if keyword_set(fullname) then temp = elements[0]->getname() + '.' + temp   
      parNames = temp
    endif
    IF N_ELEMENTS(elements) GE 2 THEN BEGIN
       FOR i=1, N_ELEMENTS(elements)-1 DO BEGIN         
         temp = elements[i] -> getParNames(position=parpos, static=static, $
            fitting=fitting, free=free, fixed=fixed)
         IF STRUPCASE(SIZE(temp, /TNAME)) EQ 'STRING' THEN begin
           if keyword_set(fullname) then temp = elements[i]->getname() + '.' + temp   
           parNames = [parNames, temp]
         endif
       ENDFOR
    ENDIF
    count = n_elements(parnames)

    RETURN, parNames
END

;+
;NAME:
;getParInfo
;-
function ffs_model::getparinfo, $
    elementname=elementname, $
    position=position

    pars = self->getpars(elementname=elementname,  position=position, $
        parname=parname, parpos=parpos, /free, count=count)

    if count gt 0 then begin     
        parinfo = replicate(pars[0] -> getmpfitstr(), 1)
        if count ge 2 then $
            for i=1, count-1 do parinfo = [parinfo, (pars[i]->getmpfitstr())[0]]
    endif

  return, parinfo
end

;+
;NAME:
;addPar
;-
function ffs_model::addpar,par, parname=parname, elementname=elementname, position=position

    elements = self->getelements(elementname=elementname, position=position)

    if self->ispar(par) then $
        for i=0, n_elements(elements)-1 do $
            if elements[i]->addpar(par) eq 0 then $
                return, self->seterrmsg('could not add par to element - ' + elements[i]->geterrmsg()) $
    else if n_elements(parname) gt 0 and size(parname, /type) eq 7 then $
        for i=0, n_elements(elements)-1 do $
            if elements[i]->addpar(parname=parname) eq 0 then $
                return, self->seterrmsg('could not add par to element - ' + elements[i]->geterrmsg())

    return, 1
end



	  ;@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
	  ;@                                                    @ 
	  ;@                   END [PAR]                        @
	  ;@  Above are the methods that act on ffs_par objects @
	  ;@                                                    @
	  ;@                                                    @
	  ;@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@



	  ;@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
	  ;@                                                        @ 
	  ;@                   START [ELEMENT]                      @
	  ;@  Below are the methods that act on ffs_element objects @
	  ;@                                                        @
	  ;@                                                        @
	  ;@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@


;+
;NAME:
;validateParentage
;-
function ffs_model::validateparentage, who=who
    elements = self->getelements(/all, count=count)
    idx = intarr(count)
    for i=0, count-1 do begin
        children = elements[i]->getchildren(count=numchildren)
        if numchildren ge elements[i]->getminchildren() $
            && numchildren le elements[i]->getmaxchildren() then idx[i]=1 $
            else idx[i] = 0
    endfor
    check = where(idx eq 0, numzero)
    if arg_present(who) then who = idx
    return, numzero eq 0
end


;+
;NAME:
;getContainer
;-
FUNCTION ffs_model::getContainer
    RETURN, self.container
END

;+
;NAME:
;getNumElements
;returns the number of elements in the container.
;-
FUNCTION ffs_model::getNumElements
    RETURN, (self->getContainer())->count()
END

;+
;NAME:
;getElements
;-
FUNCTION ffs_model::getElements, $
  ALL = all, $
  POSITION = position, $
  ELEMENTNAME = elementname,  $
  PARNAME = parname, $
  COUNT = countElems

  RESULT = 'OK'
  numElements = self->getNumElements()
  IF numElements EQ 0 THEN BEGIN
     result = 'no elements'
     countElems = 0 
     RETURN, 0
  ENDIF

  IF N_ELEMENTS(position) GT 0 THEN BEGIN
     elements = ((self->getContainer()) -> get(POSITION = position, COUNT = countElems))
  ENDIF ELSE IF N_ELEMENTS(elementname) GT 0 THEN BEGIN
     elements = ((self->getContainer()) -> get(NAME = elementname, COUNT = countElems))
  ENDIF ELSE IF N_ELEMENTS(parname) GT 0 THEN BEGIN
     elems = ((self->getContainer()) -> get(/all))
     FOR i = 0, N_ELEMENTS(elems)-1 DO BEGIN
        pars = elems[i]->getPars(PARNAME = parname, COUNT = countpar)
        IF countpar NE 0 THEN IF N_ELEMENTS(temparr) GT 0 THEN temparr = [temparr, elems[i]] ELSE temparr = [elems[i]]
     ENDFOR
     elements = temparr
     countElems = N_ELEMENTS(elements)
  ENDIF ELSE BEGIN
     elements = ((self->getContainer()) -> get(/all, COUNT = countElems))
  ENDELSE
  
  IF self->iselement(elements) eq 0 THEN PRINT,'No such element found'
  IF N_ELEMENTS(elements) EQ 1 THEN RETURN, elements[0] ELSE RETURN, elements
END

;+
;NAME:
;getElementNames
;-
FUNCTION ffs_model::getElementNames, POSITION=position, count=count

  elements = self->getElements( POSITION=position, COUNT=count)
  elementNames = STRARR(count)
  FOR i=0,count-1 DO elementNames[i] = elements[i]->getName()

  RETURN, elementNames
END

;+
;NAME:
;getelementypes
;-
FUNCTION ffs_model::getelementtypes, $
              ELEMENTNAME=elementname, $
              POSITION=position
	      
  elements = self->getElements( POSITION=position, ELEMENTNAME=elementName, COUNT=count) 
  elementTypes = STRARR(count)
  FOR i=0,count-1 DO elementTypes[i] = obj_class(elements[i])
  RETURN, elementTypes
END

;+
;NAME:
;addElement
;-
function ffs_model::addElement, $
  in, $
  elementname = elementname
  
  if size(in, /type) eq 11 && self->iselement(in) eq 1 then $
    (self->getcontainer())->add, in[0] $
  else if size(in, /type) eq 7 then begin
    temp = obj_new(in, elementname = elementname)
    if self->iselement(temp) eq 1 then $
      (self->getcontainer())->add, temp $
    else return, self->seterrmsg('Failed to create element of type: '+in)
  endif else return, self->seterrmsg('Must either supply existing ffs_element object, or string describing type of element to add')

  return, 1
end

;+
;NAME:
;findpar
;-
function ffs_model::findpar,par
	
	elem = self->getelements(/all)

	for i=0, n_elements(elem)-1 do begin
        	pars=(elem[i])->getpars()
                if size(pars,/tname) eq 'OBJREF' then begin
                	for j=0,n_elements(pars)-1 do begin
				if pars[j] eq par then begin
                                	return,(elem[i])->getname()
                                endif
			end
		endif
	end
	return,-1
end

;+
;NAME:
;removeElement
;-
function ffs_model::removeElement, $
    ref, $
    name=name, $
    elementname = elementname, $
    position = position
  
    if n_elements(elementname) gt 0 then name=elementname

    if n_elements(ref) eq 0 then begin
        ref = self->getelements(position = position, elementname = name, count=count)
        if count eq 0 then return, $
            self->seterrmsg('Error; cannot delete specified element - it is not present in this model.')
    endif
        
    allelements = self->getelements(count=count)
    for i=0, count-1 do if allelements[i]->removechild(ref) eq 0 then return, $
        self->seterrmsg('Error attempting to remove the element from parent.')
 
    if (self->getcontainer())->remove(ref, name=name, position=position) eq 0 then $
        return, self->seterrmsg('Error, whilst removing element: '+(self->getcontainer())->geterrmsg())
    return, 1    
end

	  ;@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
	  ;@                                                        @ 
	  ;@                   END [ELEMENT]                        @
	  ;@  Above are the methods that act on ffs_element objects @
	  ;@                                                        @
	  ;@                                                        @
	  ;@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@


	  ;@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
	  ;@                                                        @ 
	  ;@                   START [MODEL]                        @
	  ;@  Below are the methods that act on this ffs_model      @
	  ;@                                                        @
	  ;@                                                        @
	  ;@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@

;+
;NAME:
;setModelName
;-
function ffs_model::setModelName,modelname
    self.modelName = modelName
    return, 1
end

;+
;NAME:
;getModelName
;-
function ffs_model::getmodelname
    return, self.modelname
end

;+
;NAME:
;setName
;-
function ffs_model::setName,modelname
    return, self->setmodelname(modelname)
end

;+
;NAME:
;getName
;-
function ffs_model::getname
    return, self.modelname
end


;+
;NAME:
;translateFitResults
;performs the translation from the array of all parameters for the model, that mpfit uses,
;to storing the parameters separately, in their respective elements.
;-
function ffs_model::translateFitResults,p,fast=fast

  elements = self->getElements(count=count)
  startindex = 0
  endindex = 0
  
  if count eq 0 then return, self->seterrmsg('No elements found.')
  
  FOR i=0, count-1 DO BEGIN
      nparams = elements[i]->getNumPars(/free)
      IF nparams GT 0 THEN BEGIN
	endindex = startindex + nparams-1
	if elements[i]->setParVals(p[startindex:endindex], /free,fast=fast) eq 0 then $
            return, self->seterrmsg('Error setting par values - ' + elements[i]->geterrmsg())
	startindex = endindex+1
      ENDIF   
  ENDFOR
  return, 1
end


;+
;NAME:
;evaluate
;-
FUNCTION ffs_model::evaluate, $
    memo=memo
    
    if not obj_valid((ref = self->getsimplified())) then ref = self
    elements=ref->getelements()
    ; deal with no root element - place all elements within a new add.
    if obj_valid(ref->getroot()) eq 0 then begin
        root = obj_new('ffs_add')
        if root->setchildren(self->getelements()) eq 0 then return, $
            self->seterrmsg("Failed when setting root's children - " + $
                root->geterrmsg())
        if self->addelement(root) eq 0 then return, 0
        if self->setroot(root) eq 0 then return, 0
    endif
    result=(ref->getroot())->evaluate(memo=memo)
    if self->saveResult(result) eq 0 then stop
    return, *result
END

;+
;NAME:
;saveResult
;-
function ffs_model::saveResult, result
    if ptr_valid(result) then begin 
        self.result=result
        return, 1
    endif else return, self->seterrmsg('Result should be of type pointer.')
    
end


;+
;NAME:
;getResult
;-
FUNCTION ffs_model::getResult
    RETURN, self.result  
END


;+
;NAME:
;error
;-
FUNCTION ffs_model::error, xdata, ELEMENTNAME = elementname, POSITION = position, IFUNCTOFF = ifunctoff
  IF N_ELEMENTS(xdata) EQ 0 THEN BEGIN
     xdata = self->getXdata()
     IF STRUPCASE(SIZE(xdata, /tname)) EQ 'STRING' THEN  return, -1
  ENDIF 

  result = 0.0D
  elements = self->getElements( POSITION=position, ELEMENTNAME=elementName, COUNT=count)
  IF OBJ_VALID(elements[0]) THEN BEGIN
     FOR i = 0, N_ELEMENTS(elements)-1 DO BEGIN
                                ;result = result + elements[i]->evaluate(xdata)
        result = result + (elements[i] -> error(xdata, IFUNCTOFF = ifunctoff))^2.0
     ENDFOR
  ENDIF

  RETURN, SQRT(result)
END

function ffs_model::setroot, ref
    if obj_valid(ref) then self.rootelement = ref else return, 0
    return, 1
end

function ffs_model::getroot
    return, self.rootelement
end


	  ;@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
	  ;@                                                        @ 
	  ;@                     END [MODEL]                        @
	  ;@  Above are the methods that act on this ffs_model      @
	  ;@                                                        @
	  ;@                                                        @
	  ;@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@

;+
;NAME:
;cleanup
;This method frees all object values and destroys all objects stored
;in the container. This is already handled in cn_container::cleanup, so
;cleanup only needs to perform an obj_destroy on the parcontainer. Also
;makes the call to the superclass ffs_primitive's cleanup.
;-
PRO ffs_model::cleanup

   IF OBJ_VALID(self->getContainer()) THEN OBJ_DESTROY,(self->getContainer())
   IF PTR_VALID(self.p_xdata) THEN PTR_FREE, self.p_xdata
   IF PTR_VALID(self.result) THEN PTR_FREE, self.result
   if obj_valid(self.rootelement) then obj_destroy, self.rootelement
   if obj_valid(self.simplified) then obj_destroy, self.simplified
   self->ffs_primitive::cleanup

END

;+
;NAME:
;Init
;This method initializes the object values (modelname/name, type, elements)
;calls the parent's Init method
;-
FUNCTION ffs_model::init, $
  MODELNAME = modelname, $
  NAME = name, $
  ELEMENTS = elements, $
  XDATA = xdata, $
  debug=debug

  If Self -> ffs_primitive::Init(debug=debug) NE 1 Then Return, 0  
  
  self.modelname = 'default'    
  if n_elements(modelname) gt 0 then if self->setmodelname(modelname) eq 0 then return, 0 $
    else if n_elements(name) gt 0 then if self->setname(name) eq 0 then return, 0
  
  self.container = OBJ_NEW('ffs_contain') 
  
  if n_elements(elements) gt 0 && self->iselement(elements) eq 1 then $
    for i=0,  n_elements(elements)-1 do $
      if self->addElement(elements[i]) eq 0 then return, 0     

  if n_elements(xdata) gt 0 then if self->setxdata(xdata) eq 0 then return, 0

  RETURN, 1
           
END

;+
;NAME:
;define
;standard class definition, inherits ffs_primitive.
;-
PRO ffs_model__define, class

  class = {ffs_model, $
          modelname:'', $
          container:OBJ_NEW(), $ 
          p_xdata:PTR_NEW(), $  
	  result:PTR_NEW(), $ 
          rootelement:OBJ_NEW(), $
          simplified:obj_new(), $
          INHERITS ffs_primitive}
END
