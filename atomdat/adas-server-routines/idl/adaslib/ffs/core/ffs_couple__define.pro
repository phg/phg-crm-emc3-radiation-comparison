;+
; PROJECT:
;  FFS - Framework for Feature Synthesis (Object oriented model/fitting)
;
; NAME:
;  FFS_COUPLE
;
; PURPOSE:
;  Parse a lisp style string expression defining how to calculate the value of
;  an ffs_par. Provides a method to obtain the result produced by this 
;  expression.
;
; EXPLANATION:
;  This object parses a couple definition string in a lisp-style format
;  such as:
;    '(* (+ 1 2) 3)'  [RESULT: 9]
;
;  Available operators are:
;    add:	    	+
;    subtract:   	-
;    multiply:		*
;    divide:	    	/
;    exponentiate:	^
;
; USE:
;  As an example:
;    par1 = obj_new('ffs_par', parname='test1', value=6.0)
;    par2 = obj_new('ffs_par', parname='test2', value=7.0)
;    par3 = obj_new('ffs_par', parname='test3', value=10.0)
;    parameters=[par1,par2,par3]
;    expression = '(/ (+ $1 $2 3 8) (- $3 7) 2)'  
;    couple_obj = obj_new('ffs_couple', expression, parameters)
;    par4 = obj_new('ffs_par', parname='test4', value=0.0, coupled=couple_obj)
;    print, par4->getValue()
;
;  this should give the value 4.0
;  
;  If the object should fail to instantiate, try creating the object with keyword
;  'debug' set. This prints information to the terminal that may help solve the
;  problem.
;
; INITIALISATION SYNTAX:
;   couple_obj = obj_new('ffs_couple', expression, parameters, debug=debug)
;
;   PARAMETERS: 
;     expression    	-   lisp style string expression defining par coupling.
;     parameters    	-   an array of ffs_par* object references (*really any object with a 'getvalue' method)
;   KEYWORDS:
;     debug    	    	-   set this keyword to 1 for diagnostic information to be printed to the terminal.
;
; PUBLIC ROUTINES:
;  In addtion to the methods listed below, this object inherits methods from ffs_primitive
;   - refer to this class' documentation for more details.
;
;  [getvalue]
;    PURPOSE:
;      Evaluates the coupling expression.
;    INPUTS:
;      None
;    OUTPUTS:
;      Returns the value obtained from evaluating the coupling expression.
;    SIDE EFFECTS:
;      None
;  [setexpr]
;    PURPOSE:
;      Sets the coupling expression.
;    INPUTS:
;      A string defining the coupling string, in the prescribed format 
;      (see 'EXPLANATION' above)
;    OUTPUTS:
;      Returns 1 if successful.
;      Returns 0 if unsuccessful.
;    SIDE EFFECTS:
;      None
;
; CALLS:
;   None.
;
; SIDE EFFECTS:
;   None.
;	
; AUTHOR:
;   Christopher Nicholas, University of Strathclyde
;   (Based on Allan Whiteford's ffs_model_parser)
;
; VERSION HISTORY:
;   1.1  Allan Whiteford 17/10/2007
;        * Placeholder, Chris will implement this properly
;   1.2  Christopher Nicholas 27/11/2007
;   	 * first real commit of ffs_couple - previous version was just a placeholder.
;   1.3  Christopher Nicholas 27/11/2007
;   	 * Added documentation.
;   	 * fixed typo in getvalue.
;        * fixed parse call in init method to act on validated self.expr rather than 
;          raw input expr.
;        * re-write of setpars.
;        * init method utilises setpars method.
;   1.4  Christopher Nicholas 29/11/2007   
;	 * Added method 'execop'. If supplied with just the operator will return 1 or 0.
;   	   however, if also passed argument list will return result of operator on that
;   	   argument list.
;    	 * 'getValue' now utilises 'execop' for execution of instructions in list.
;   	 * Modified 'parse' method to include both an error flag in the structure it returns
;   	   as well as a string containing the current nexted expression. This has been done
;   	   in order to detect / locate syntax errors in the input coupling string.
;   	 * 'parse' now utilises 'execop' to verify operator validity.
;   	 * Improved 'parse' diagnostic messages.
;   	 * Modified 'setexpr' method to utilise error flags to create error messages 
;   	   after parsing expression.
;   	 * Replaced erroneous call to 'setexpr' in 'validate' with simple assignment
;   	   of state variable 'expr'.
;   1.5  Allan Whiteford 20/11/2007   
;	 * Inherited ffs_primitve so removed inbuilt error handling
;	 * Changed calls to seterror to seterrmsg
;	 * Made passing in expr and pars at init time non-mandatory.
;   1.6  Chris Nicholas 20/11/2007
;   	 * Removed 'diag' keyword for init.
;   	 * Removed 'diag' state variable and all references to it.
;        * Added 'debug' keyword and passed to ffs_primitive.
;   1.7  Chris Nicholas 15/09/2008
;        * Added the following methods:
;          - flatten
;          - calcparpd
;          - getparpd
;        * 'Vectorized' execution of exponential operation in 'execop'
;   1.8  Chris Nicholas 07/11/2008
;        * Fixed regular expression in 'parse' to allow letters 'e' and 'd'
;          as part of extracted constants (to allow for exponent).
;   1.9  Allan Whiteford 11/11/2008
;        * Fixed typo
;   1.10 Chris Nicholas 01/12/2008
;        * replaced 'calcpd' with new (working, if not elegant) implementation.
;        * 'getpd' now 'packages' the value of the result of each nested
;          expression, as well as their derivatives, before passing to 'calcpd'.
;        * temporarily allow for 'internal' keyword in 'getvalue' method to get
;          values of all of the nested expression values.
;        * modified 'parse' to allow '+' and '-' characters in regular expression
;          for extracting constants in expressions - note that at this point, have
;          not allowed for a constant to begin with a '-' sign - this will have
;         to be dealt with in a future update.
;        * Note that another revision of the above changes, is expected to
;          follow shortly.
;        * Modified documentation header to allow for 4-character version numbers.
;   1.11 Chris Nicholas 02/12/2008
;        * Fixed typo in 'getparpd'.
;   1.11 Allan Whiteford 02/07/2009
;        * Added cacheing facility to getvalue method.
;----------------------------------------------------------------------
;
; First version distributed with ADAS.
;
; MODIFIED   :
;       1.1     Martin O'Mullane
;                 - First central ADAS version.
;
; VERSION    :
;       1.1    04-01-2013
;
;----------------------------------------------------------------------
;-       
function ffs_couple::flatten
    pars = self->getpars()
    buf = pars
    for i=0, n_elements(pars)-1 do begin
        cobj = pars[i]->getcoupled()
        if obj_valid(cobj) then buf = [buf, cobj->flatten()]
    endfor
    return, buf[uniq(buf, sort(buf))]
end

function ffs_couple::calcparpd, operator, arglist, ref, paridx, order, evalres, const
    paridxctr = 0
    constidxctr = 0
    pars = self->getpars()
    opList = ['+','-','*','/','^']
    opidx = where(opList eq operator)    
    temp = dblarr(n_elements(order))
    result = 0.0d0
    
    case opidx of
        0:  result= total(arglist)
        1:  result= total([arglist[0], -1.0*arglist[1:*]])
        2:  begin
        
                for i=0, n_elements(order)-1 do begin
                    others = where(indgen(n_elements(order)) ne i)
                    if others[0] eq -1 then return, 0.0d0
                    result+= product([arglist[i], evalres[others]])
                endfor
                ;return, result
            end
        3:  begin
        
                result = product([arglist[0], 1.0d0 / evalres[1:*]]) 
                temp=0.0d0
                for i=1, n_elements(order)-1 do begin
                    others = where(indgen(n_elements(order)) ne i and indgen(n_elements(order)) gt 0)
                    temp = product([evalres[0], (-1.0d0 / evalres[i]^2)*arglist[i]])
                    if others[0] ne -1 then temp = product([temp, 1.0d0 / evalres[others]])
                    result += temp
                endfor
                ;return, result
            end
        4:  begin
        
               ; return, $
                result=product(evalres[1:*])*evalres[0]^(product(evalres[1:*]) - 1.0d0)*arglist[0] + alog(evalres[0])*evalres[0]^(product(evalres[1:*]))*(self->calcparpd('*', arglist[1:*], ref, paridx, order[1:*], evalres[1:*], const))
                ;evalres[0]^evalres[1:*]*(product(arglist[1:*])*alog(evalres[0])+product(evalres[1:*])/evalres[0]))
            end
        
    endcase
return, result
  return, opidx eq -1 ? 0 : 1
end



function ffs_couple::getparpd, ref
    exprs = *self.exprs
    
    pars = *self.pars
    res=dblarr(n_elements(exprs))

    exprres = (self->getvalue(/internal))
    for i=1,n_elements(exprs)-1 do begin
        operator = (*(exprs[i])).operator
        paridx = (*(exprs[i])).paridx
        expridx = (*(exprs[i])).expridx
        const = (*(exprs[i])).const
        order = (*(exprs[i])).order

        arglist = dblarr(n_elements(order))
        paridxctr = 0
        expridxctr = 0
        constctr = 0
        
        evalres = dblarr(n_elements(order))
        for j=0, n_elements(order)-1 do begin
            if order[j] eq 0 then begin
                evalres[j] = pars[paridx[paridxctr]]->getvalue()
                if obj_valid(pars[paridx[paridxctr]]->getcoupled()) then begin
                    arglist[j] = (pars[paridx[paridxctr++]]->getcoupled())->getparpd(ref)
                endif else if pars[paridx[paridxctr]] eq ref then begin
                    arglist[j] = 1.0d0
                    paridxctr++
                endif else begin
                    arglist[j] = 0.0
                    paridxctr++
                endelse
            endif
            if order[j] eq 1 then begin
                arglist[j] = 0.0d0
                evalres[j] = const[constctr++]
            endif
            
            if order[j] eq 2 then begin
                evalres[j] = exprres[expridx[expridxctr]]
                arglist[j] = res[expridx[expridxctr++]]
             
            endif
        endfor
        
        res[i]=self->calcparpd(operator, arglist, ref, paridx, order, evalres)

    endfor
    
  return, res[n_elements(res)-1]
end



function ffs_couple::execop, operator, ARGLIST=arglist
  opList = ['+','-','*','/','^']
  opidx = where(opList eq operator)
  ; note - have opted for 'left to right' implentation of exponent operator.
  ; This is consistent with IDL, but differs from most other languages.
  ; To clarify (^ x y z) will be evaluated in the same manner as (^ x (* y z))
  ; and NOT as (^ x (^ y z))
  if n_elements(arglist) gt 0 then begin
    if opidx eq 0 then return, total(arglist) $
    else if opidx eq 1 then return, arglist[0] + total(-1.0d * arglist[1:*]) $
    else if opidx eq 2 then return, product(arglist) $
    else if opidx eq 3 then return, arglist[0] * product(1.0d / arglist[1:*]) $
    else if opidx eq 4 then return, arglist[0] ^ product(arglist[1:*]) $
    else begin
      junk = self->seterrmsg('Invalid Operator')
      return, !values.d_nan
    endelse 
  endif
  return, opidx eq -1 ? 0 : 1
  
end


function ffs_couple::getvalue, internal=internal, nocache=nocache
  exprs=*self.exprs
 
  pars=*self.pars
  res=dblarr(n_elements(exprs))
  parvals=dblarr(n_elements(pars))
  for i=0,n_elements(pars)-1 do parvals[i]=pars[i]->getvalue()

  if not keyword_set(nocache) && ptr_valid(self.cache_par) && array_equal(*self.cache_par,parvals) then begin
    res=*self.cache_res
  endif else begin
    for i=1,n_elements(exprs)-1 do begin
      operator = (*(exprs[i])).operator
      paridx = (*(exprs[i])).paridx
      expridx = (*(exprs[i])).expridx
      const = (*(exprs[i])).const
      order = (*(exprs[i])).order

      arglist = dblarr(n_elements(order))
      paridxctr = 0
      expridxctr=0
      constctr=0

      for j=0, n_elements(order)-1 do begin
        if order[j] eq 0 then arglist[j] = parvals[paridx[paridxctr++]]
        if order[j] eq 1 then arglist[j] = const[constctr++]
        if order[j] eq 2 then arglist[j] = res[expridx[expridxctr++]]
      endfor

      res[i]=self->execop(operator, arglist=arglist)

    endfor

    if ptr_valid(self.cache_res) then ptr_free,self.cache_res
    if ptr_valid(self.cache_par) then ptr_free,self.cache_par
    self.cache_res=ptr_new(res)
    self.cache_par=ptr_new(parvals)
  
  endelse
  
  if keyword_set(internal) then return, res
  return,res[n_elements(res)-1]
end


function ffs_couple::parse, expr

  opened=0
  lbrkt=0
  rbrkt=0
  operator=''
  paridx=-1
  expridx=[-1]
  opbuf=''
  parbuf=''
  parmarker=0
  const=-1.0d
  constbuf=''
  nested_expr=''
  order=-1
  error=0
  
  ;add a space at end of input string
  ; - spaces are used as markers in parsing.
  expr=expr+' '
  
  for i=0, strlen(expr)-1 do begin
    c=strmid(expr,i,1)
  
    ;check if reached a new level of nested brackets - or a space
    ;therefore we have read our operator - flush operator buffer
    if (c eq ' ' or c eq '(') and operator eq '' then begin
      operator=opbuf
      opbuf=''
    endif

    ;dump the parameter buffer
    if parmarker eq 1 and (c eq ' ' or i eq strlen(expr)-1) and lbrkt eq 0 and parbuf ne ''then begin
        paridx=[paridx,fix(parbuf)]
        parbuf=''
	order=[order, 0]
	parmarker=0
    endif
    
    ;dump the constant buffer
    if parmarker eq 0 and (c eq ' ' or c eq ')') and constbuf ne '' then begin
      const=[const,double(constbuf)]
      constbuf=''
      order=[order, 1]
    endif    
    
    ;if operator string empty, add ith character to operator buffer
    if operator eq '' then $
      if c ne '$' then opbuf = opbuf+c else error=1
    
    ;if par marker set, then add current character to parameter buffer
    if parmarker eq 1 and lbrkt eq 0 then $
      if stregex(c,'[0-9]',/boolean) then parbuf = parbuf+c else error=2
    
    ;if numerical character, add to constant buffer
    ;first check if decimal point already present in constant buffer
    dummy=strsplit(constbuf,'.',count=pointcnt)
    if parmarker eq 0 and lbrkt eq 0 and ((strlen(constbuf) gt 0 and stregex(c,'[0-9.ed\+-]',/boolean)) or stregex(c,'[0-9.]',/boolean)) then $
      if (c eq '.' and pointcnt ge 2) then error=3 else constbuf=constbuf+c
    
    if error ne 1 and c eq '$' then parmarker=1    
    if c eq '(' then lbrkt=lbrkt+1 
    if c eq ')' then rbrkt=rbrkt+1 
   
    if opened eq 1 and lbrkt eq rbrkt then begin
      expridx=[expridx,self->parse(nested_expr)]
      order=[order,2]
      parmarker=0
      opened=0
      lbrkt=0
      rbrkt=0
    endif	
    
    ; if opened flag set, add current character to the new expression string
    if opened eq 1 then begin
      nested_expr=nested_expr+c
    endif 
    
    ; if a '(' has been encountered, but the opened flag not yet set;
    ; then do so, to indicate entry to new bracket depth.
    ; reset new_expr and opbuf.
    if lbrkt eq 1 and opened eq 0 then begin
	    nested_expr=''
	    opened=1
	    opbuf=''
    endif  
 
  endfor
  
  if n_elements(expridx) gt 1 then expridx=expridx[1:*] else expridx=-1
  if n_elements(paridx) gt 1 then paridx=paridx[1:*]-1 else paridx=-1
  if n_elements(const) gt 1 then const=const[1:*] else const=-1
  if n_elements(order) gt 1 then order=order[1:*] else order=-1
  
  if n_elements(order) lt 2 then error=4
  if self->execop(operator) eq 0 then error=1
 
  if self.debug eq 1 then print, '##############################################################'
  if self.debug eq 1 then print, 'expression parsed: ', expr
  if self.debug eq 1 then print, 'operator: ' , operator
  if self.debug eq 1 then print, 'pars (index): ', paridx 
  if self.debug eq 1 then print, 'constants: ', const
  if self.debug eq 1 then print, 'other expressions (index): ', expridx  
  if self.debug eq 1 then print, 'order of evaluation (0:par, 1:const, 2:expr): ', order
  if self.debug eq 1 then print, 'error status (0:none, 1:operator, 2:par, 3:const, 4:num_expr): ', error
  if self.debug eq 1 then print, '##############################################################'

  return,self->store_expr({expr:expr, operator:operator,paridx:parIdx, expridx:expridx, const:const, order:order, error:error})
end


function ffs_couple::store_expr,expr
  if not ptr_valid(self.exprs) then begin
	  exprs=ptr_new(ptrarr(1))
  endif else begin
	  exprs=*self.exprs
	  ptr_free,self.exprs
  endelse
  exprs=[exprs,ptr_new()]	
  self.exprs=ptr_new(exprs)	
  idx=n_elements(exprs)-1	
  if ptr_valid((*self.exprs)[idx]) then ptr_free,(*self.exprs)[idx]
  (*self.exprs)[idx]=ptr_new(expr)
  return,idx
end


function ffs_couple::setpars, pars
  if size(pars, /type) eq 11 then $
    for i=0, n_elements(pars)-1 do if obj_valid(pars[i]) eq 0 then $
      return, self->seterrmsg('input pars must be valid ffs_par object references')
  if ptr_valid(self.pars) then ptr_free, self.pars
  self.pars = ptr_new(pars)
  return, 1
end


function ffs_couple::getpars, pars
  return, *self.pars
end

;taken from ffs_model_parser
;verify string has matching number of left and right brackets.
function ffs_couple::validate, expr

  cnt=0
  a=-1
  while((a=strpos(expr,'(',a+1)) ne -1) do cnt=cnt+1
  a=-1
  while((a=strpos(expr,')',a+1)) ne -1) do cnt=cnt-1

  if cnt ne 0 then begin
    self.expr=''
    return,self->seterrmsg('uneven number of brackets in input expression')
  endif

  return,1
end


function ffs_couple::setexpr, expr
  if size(expr, /type) eq 7 then begin 
    self.origexpr = expr
    if self->validate(expr) then begin
      temp = strtrim(expr,2)
      if strpos(temp, '(') eq 0 && strpos(temp, ')', /reverse_search) eq strlen(temp)-1 then $
	  self.expr = strmid(temp, 1, strlen(temp)-2)	  
      lastidx=self->parse(self.expr)
      for i=1, lastidx do $
        if (*((*self.exprs)[i])).error ne 0 then begin
	  if (*((*self.exprs)[i])).error eq 1 then culprit='operator'
	  if (*((*self.exprs)[i])).error eq 2 then culprit='parameters'
	  if (*((*self.exprs)[i])).error eq 3 then culprit='constants'
	  if (*((*self.exprs)[i])).error eq 4 then culprit='number of operands'
          return, self->seterrmsg("Syntax error in coupling string - check " + culprit + " in expression: '(" + (*((*self.exprs)[i])).expr + ")'")
        endif
      return, 1
    endif else return, 0
  endif else return, self->seterrmsg('input coupling expression must be a string')
end

function ffs_couple::getexprs
  return, self.exprs
end

function ffs_couple::getexpr
  return, self.expr
end


pro ffs_couple::cleanup
  if ptr_valid(self.exprs) then ptr_free, self.exprs
  if ptr_valid(self.pars) then ptr_free, self.pars
  if ptr_valid(self.cache_res) then ptr_free,self.cache_res
  if ptr_valid(self.cache_par) then ptr_free,self.cache_par
end


function ffs_couple::init, expr, pars, debug=debug
  if self->ffs_primitive::init(debug=debug) ne 1 then return, 0
  if n_params() eq 2 then begin
	  if self->setpars(pars) eq 0 then return, 0  
	  return, self->setexpr(expr)
  endif
  return,1
end


pro ffs_couple__define
  self = {ffs_couple, $
  	  origexpr:'', $
	  expr:'', $
	  exprs:ptr_new(), $
          pars:ptr_new(), $
          cache_res:ptr_new(), $
          cache_par:ptr_new(), $
	  inherits ffs_primitive }
end

