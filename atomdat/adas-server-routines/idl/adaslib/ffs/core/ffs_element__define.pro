;+
; PROJECT:
;   FFS - Framework for Feature Synthesis (Object oriented model/fitting).
;
; NAME:
;   FFS_ELEMENT
;
; PURPOSE:
;   An FFS element can be a spectral feature defined by a set of FFS parameters
;   e.g. simple lines such as a Gaussian, Lorentzian or more complicated - a 
;   Balmer series profile, emission from a heavy species element etc. These 
;   elements can also act as operators upon other FFS elements e.g. provide
;   simple arithmetic; add, multiply or more complex operations like broadening.
;
; EXPLANATION:
;   An abstract base class (i.e. not to be instantiated alone) from which concrete
;   FFS element implementations should inherit. These elements are intended to be
;   utilised as components of a spectral model within the FFS system.
;   This class provides a way of containing and controlling the ffs parameter 
;   objects which define it. Elements also, in fact, have ffs_prop objects 
;   which are very similar to ffs_pars and control the spectral feature produced,
;   but do not vary during a fit (note: this is not the same as a fitting 
;   parameter that has been set fixed). This class also manages any 'child' 
;   elements - applicable to operator elements.
;
; USE:
;   Not intended to be instantiated alone. Should be inherited by concrete 
;   implementations ofthis class. Inheriting classes should provide a calculate
;   method detailed as follows:
;     [calculate]
;       PURPOSE:
;         Evaluates the element profile.
;       INPUTS:
;         A structure containing the following fields:
;           wavelength  -   wavelength grid for intensity values.
;           intensity   -   intensity values.
;           gridded     -   whether the intensity values are mapped to a wavelength 
;                           grid or not: 1 or 0.
;       OUTPUTS:
;         Returns a structure of the same type as input (see above).
;       SIDE EFFECTS:
;         None.
;   The calculate method will be invoked by the evaulate method of this class.
; INITIALISATION SYNTAX:
;   Not intended to be initialised alone - to be inherited from.
;   PARAMETERS:
;     None.
;   KEYWORDS:
;     elementname   -   passed to 'setelementname', see below for method details.
;     name          -   passed to 'setname', see below for method details.
;     xdata         -   passed to 'setxdata', see below for method details.
;     debug         -   set this keyword to enable debug information to be printed to the terminal.
;
; PUBLIC ROUTINES:
;   In addtion to the methods listed below, this object inherits methods from ffs_primitive
;   - refer to this class' documentation for more details.
;   ............................................................................
;   [ispar]
;     PURPOSE:
;	Checks if input array elements are ffs_par object references. 
;     INPUTS:
;	Object reference to check.
;     OUTPUTS:
;	Returns a 1 or a 0 to signify success and failure respectively.
;     SIDE EFFECTS:
;	None.
;   ............................................................................
;   [isprop]
;     PURPOSE:
;	Checks if input array elements are ffs_prop object references. 
;     INPUTS:
;	Object reference; the references to check.
;     OUTPUTS:
;	Returns a 1 or a 0 to signify success and failure respectively.
;     SIDE EFFECTS:
;	None.
;   ............................................................................
;   [setxdata]
;     PURPOSE:
;	Sets the wavelength grid that this feature is intended to be placed upon
;       (This is not the same as 'wavelength' field in calculate structure).
;     INPUTS:
;	double; array comprising the x-axis data points.
;     OUTPUTS:
;	Returns a 1 or a 0 to signify success and failure respectively.
;     SIDE EFFECTS:
;	None.
;   ............................................................................
;   [getxdata]
;     PURPOSE:
;	Retrieves the object's xdata.
;     INPUTS:
;	None.
;     OUTPUTS:
;	double; array comprising the x-axis data points.
;     SIDE EFFECTS:
;	None.
;   ............................................................................
;   [setname]
;     PURPOSE:
;	Sets the element's name; used to identify element at higher level in FFS.
;       Note that this knowingly duplicates setelementname functionality.
;     INPUTS:
;	String; the name of the element, should be unique within a model.
;     OUTPUTS:
;	Returns a 1 or a 0 to signify success and failure respectively.
;     SIDE EFFECTS:
;	None.
;   ............................................................................
;   [getname]
;     PURPOSE:
;	Retrieves the element's name.
;     INPUTS:
;	None.
;     OUTPUTS:
;       String; the name of the element
;     SIDE EFFECTS:
;	None.
;   ............................................................................
;   [setelementname]
;     PURPOSE:
;	Sets the element's name; used to identify element at higher level in FFS. 
;       Note that this knowingly duplicates setname functionality.
;     INPUTS:
;	String; the name of the element, should be unique within a model.
;     OUTPUTS:
;	Returns a 1 or a 0 to signify success and failure respectively.
;     SIDE EFFECTS:
;	None.
;   ............................................................................
;   [setpars]
;     PURPOSE:
;	Clears the parcontainer and adds the new parameters passed in.
;     INPUTS:
;	object reference; array of ffs_par objects.
;     OUTPUTS:
;	Returns a 1 or a 0 to signify success and failure respectively.
;     SIDE EFFECTS:
;	None.
;   ............................................................................
;   [getpars]
;     PURPOSE:
;	Retrieves the element's parameter objects.
;     INPUTS:
;       Keywords to specify which pars are required:
;           all         -   integer (boolean); set to retrieve all pars for the 
;                           element (default).
;           parname     -   string; array of par names.
;           position    -   integer; position in par container
;           count       -   integer; (outward); the number of elements returned.
;           static      -   boolean; set to return only ffs_props in the element.
;                           i.e. properties of the element that are non-fitting 
;                           parameters. 
;           fitting     -   integer (boolean); set to only return the ffs_pars in
;                           the element i.e. the fitting parameters.
;     OUTPUTS:
;	object reference; array of ffs_par objects
;     SIDE EFFECTS:
;	None.
;   ............................................................................
;   [setparfixed]
;     PURPOSE:
;	Sets specified parameters in the element to be fixed.
;     INPUTS:
;       integer(boolean); set to fix parameter.
;       Keywords to be passed to getpars to specify pars: parname, position.
;     OUTPUTS:
;	Returns a 1 or a 0 to signify success and failure respectively.
;     SIDE EFFECTS:
;	None.
;   ............................................................................
;   [getparfixed]
;     PURPOSE:
;	Retrieves the array of pars via method getPars then uses their getFixed 
;       method to retrieve the value of 'fixed' for each par.
;     INPUTS:
;       Keywords to be passed to getpars to specify pars: parname, position, 
;     OUTPUTS:
;	integer; array of 'fixed' flag values (0 or 1).
;     SIDE EFFECTS:
;	None.   
;   ............................................................................
;   [setparlimits]
;     PURPOSE:
;	Sets the (soft) limits for specified parameters in the element.
;     INPUTS:
;       double, two element array; specifies the lower/upper limits
;       for the parameter value (the range in which it is free to
;       vary during a fit).'limited' detailed above must be 
;       specified together with these limits to enforce or 
;       disable them.
;       Keywords to be passed to getpars to specify pars: parname, position.
;     OUTPUTS:
;	Returns a 1 or a 0 to signify success and failure respectively.
;     SIDE EFFECTS:
;	None.
;   ............................................................................
;   [getparlimits]
;     PURPOSE:
;	Retrieves the specified parameter's (soft) limits.
;     INPUTS:
;       Keywords to be passed to getpars to specify pars: parname, position.
;     OUTPUTS:
;	double, two element array; specifies the lower/upper limits
;       for the parameter value (the range in which it is free to
;       vary during a fit).'
;     SIDE EFFECTS:
;	None.
;   ............................................................................
;   [setparhardlimits]
;     PURPOSE:
;	Sets the hard limits for specified parameters in the element.
;     INPUTS:
;       double, two element array; specifies the lower/upper hard
;       limits for the parameter value. These values would normally
;       be set to prevent values being selected that will cause
;       evaluation of an FFS element to fail. These are, in a sense,
;       limits placed upon the other soft limits detailed above.
;       Keywords to be passed to getpars to specify pars: parname, position.
;     OUTPUTS:
;	Returns a 1 or a 0 to signify success and failure respectively.
;     SIDE EFFECTS:
;	None.
;   ............................................................................
;   [getparhardlimits]
;     PURPOSE:
;	Sets the specified parameter's hard limits.
;     INPUTS:
;       Keywords to be passed to getpars to specify pars: parname, position.
;     OUTPUTS:
;	double, two element array; specifies the lower/upper hard
;       limits for the parameter value. These values would normally
;       be set to prevent values being selected that will cause
;       evaluation of an FFS element to fail. These are, in a sense,
;       limits placed upon the other soft limits detailed above.
;     SIDE EFFECTS:
;	None.
;   ............................................................................
;   [setparvals]
;     PURPOSE:
;	Retrieves the array of parameters via method getPars then uses their 
;       setValue methods to set appropriate value in the parameter value array 
;       passed in 'parVals'
;     INPUTS:
;       if for fitting ffs_pars (/fitting) - then double; array of par values.
;       if for non-fitting ffs_props (/static) - then any type can be set as value.
;       Keywords to be passed to getpars to specify pars: name/parname, position, 
;       static, fitting.
;     OUTPUTS:
;	Returns a 1 or a 0 to signify success and failure respectively.
;     SIDE EFFECTS:
;	None.   
;   ............................................................................
;   [getparvals]
;     PURPOSE:
;	Retrieves the array of parameters via method getPars then uses their 
;       getValue methods.
;     INPUTS:
;       Keywords to be passed to getpars to specify pars: name/parname, position, 
;       static, fitting.
;     OUTPUTS:
;       If for fitting ffs_pars (/fitting) - then double; array of par values.
;       If for non-fitting ffs_props (/static) - then pointer array to various types.
;       Keyword 'count' supplies the number of par values returned.
;     SIDE EFFECTS:
;	None.   
;   ............................................................................
;   [getparnames]
;     PURPOSE:
;       Retrieves the array of parameters via method getPars then uses their 
;       getname methods.
;     INPUTS:
;       Keywords to be passed to getpars to specify pars: position, static, fitting.
;     OUTPUTS:
;       Returns a string array of the parameter names of the specified positions or all
;       by default.
;     SIDE EFFECTS:
;	None. 
;   ............................................................................
;   [getnumpars]
;     PURPOSE:
;       Returns the number of pars in the element (can filter by /static, /fitting).
;     INPUTS:
;       Keywords to be passed to getpars to specify pars: static, fitting.
;     OUTPUTS:
;       integer; the number of pars in the element.
;     SIDE EFFECTS:
;	None. 
;   ............................................................................
;   [addpar]
;     PURPOSE:
;       If a parameter is passed in, validity of the object verified, then
;       added to the element. If instead keyword 'parname' is set, then a
;       new 'ffs_par' with that name is instantiated and added to the element.
;     INPUTS:
;       par     -   object reference; an existing ffs_par.
;       parname -   string; the name of a parameter to add.
;     OUTPUTS:
;       Returns a 1 or a 0 to signify success and failure respectively.
;     SIDE EFFECTS:
;	None.
;   ............................................................................
;   [addprop]
;     PURPOSE:
;       If a ffs_prop is passed in, validity of the object verified, then
;       added to the element. If instead keyword 'name' is set, then a
;       new 'ffs_prop' with that name is instantiated and added to the element.
;     INPUTS:
;       prop    -   object reference; an existing ffs_prop.
;       name    -   string; the name of a property to add.
;     OUTPUTS:
;       Returns a 1 or a 0 to signify success and failure respectively.
;     SIDE EFFECTS:
;	None.
;   ............................................................................
;   [setchildren]
;     PURPOSE:
;       Sets the child element objects for this element.
;     INPUTS:
;       obj     -   object reference; the child object(s) to set.
;     OUTPUTS:
;       Returns a 1 or a 0 to signify success and failure respectively.
;     SIDE EFFECTS:
;	None.
;   ............................................................................
;   [getchildren]
;     PURPOSE:
;       Retrieves the child element objects for this element.
;     INPUTS:
;       None.
;     OUTPUTS:
;       object reference; the child objects for this element.
;       count (keyword): Number of child objects
;     SIDE EFFECTS:
;	None.
;   ............................................................................
;   [evaluate]
;     PURPOSE:
;       Evaluates this element - generates the line profile. The result is stored
;       to the object as well as returned through this method.
;     INPUTS:
;       memo -  enables buffering of previous evaluation result and parameter 
;               values - should improve performance during fitting.
;               If there has not been a change in parameter values, then this
;               eliminates the need for re-calculation.
;     OUTPUTS:
;        A structure containing the following fields:
;           wavelength  -   wavelength grid for intensity values.
;           intensity   -   intensity values.
;           gridded     -   whether the intensity values are mapped to a wavelength 
;                           grid or not: 1 or 0.
;     SIDE EFFECTS:
;	None.
;   ............................................................................
;   [setsubtype]
;     PURPOSE:
;	Sets the element's subtype; used for elements which can have
;       multiple behaviours (e.g. ADAS, broaden)
;     INPUTS:
;	String; the subtype of the element.
;     OUTPUTS:
;	Returns a 1 or a 0 to signify success and failure respectively.
;     SIDE EFFECTS:
;	None.
;   ............................................................................
;   [getsubtype]
;     PURPOSE:
;	Gets the element's subtype; used for elements which can have
;       multiple behaviours (e.g. ADAS, broaden)
;     INPUTS:
;	None
;     OUTPUTS:
;	Scalar String specifying the subtype, blank string if not set.
;     SIDE EFFECTS:
;	None.
;   ............................................................................
;   [fastsetup]
;     PURPOSE:
;       Sets cacheing state variables up so routines can use
;       the /fast option
;     INPUTS:
;       None
;     OUTPUTS:
;       Returns 1 if succesful.
;     SIDE EFFECTS:
;       Use of this facility assumes the model will not change
;       between calls (i.e. setup of coupling, limits etc. etc.)
;   ............................................................................
;
; AUTHOR:
;   Christopher Nicholas, University of Strathclyde
;   (Based on Andrew Meigs' & Chris Nicholas' glvoop_element__define)
;
; VERSION HISTORY:
;       AGM 01/03/2004  
;       * started.
;       CHN 18/06/2004  
;       * added setupForType - creates parameters for element and adds them to the 
;         parContainer.
;       * modified the define and init methods to initialise the parContainer properly.
;       CHN 21/06/2004 
;       * changed case statement in setupForType to a "where" to make code more general
;         ie. allows other params to be added to list of functions.
;       CHN 22/06/2004  
;       * added getparameter method: allows, all or specific pars referenced by 
;         name or position
;       * changed setupForType to function and returning
;         appropriate structure.
;       AGM 06/08/2004 
;       * added setParFixed/getParFixed 
;       * added PARTYPE keyword to setParVals/getParVals and setParFixed/getParFixed
;       AGM 07/08/2004  
;       * modified getParVals, getParFixed and getParInfo to use getPars directly
;         (keyword passing) instead of the massive if's implemented earlier.
;       * addressed keyword precedence (parname over position over partype)
;       AGM 11/08/2004  
;       * added object glvoop_plotstyle as data member
;       * added method to obtain the object reference to the glvoop_plotstyle object
;         instanced by glvoop_model
;       AGM 12/08/2004  
;       *added obj_destroy for plotstyle object to cleanup method
;       AGM 12/08/2004 
;       * added getName method (which is just getElementName)
;       AGM 16/08/2004  
;       * changed references to modelelement to element
;       * changed data element 'parContainer' to container
;       * added method 'getContainer' (modified getParContainer use new
;         data element). 
;       * Fixed places where self.parContainer was being
;         referenced directly (replaced with "self->getParContainer()").
;       AGM 17/08/2004  
;       * added wrapper methods for the glvoop_plotstyle object
;         so that inheritance by composition was better.
;       AGM 27/08/2004  
;       * added error (ie put everything that
;         glvoop_elementresult__define.pro does into here).
;       AGM 02/09/2004  
;       * cleaned up the set* get* (vals, errors, fixed)
;       * fixed a couple of problems with them. All use getPars properly now.
;       AGM 16/09/2004  
;       * added setName.
;       AGM 20/09/2004  
;       * added check for lun and filename to writeIDLObject
;       AGM 01/10/2004  
;       * added getArea to glvoop_element__define.pro and
;         glvoop_model__define.pro; both just do an "INT_TABULATED" on the
;         function given an array of x data. 
;       * added a centroid routine (getCentroid).
;       AGM 21/10/2004  
;       * added bars methods (error bars on/off get... like
;       draw)
;       AGM 02/02/2005  
;       * modified convolve to use getCentroid instead of
;         getAvgCtr (all elements will have a getCentroid whether meaningful or
;         not and a concrete extension (gaussian...) can override this method...
;       AGM 04/02/2005  
;       * added getCoupling and getTied (following glvoop_model__define) to list of methods.
;       AGM 02/09/2005  
;       * added p_xdata pointer to class data and modifiedinit, cleanup to take it and 
;         evaluate, convolve, getCentroid, getArea to allow the old behaviour (x passed in)
;         and if x not passed then check on validity of xdata in the pointer...
;       * added the get/set methods for xdata. And changed all method usage of x to xdata.
;       AGM 04/09/2005  
;       * added plot method
;       AGM 05/09/2005  
;       * added getKeywords method
;       AGM 05/09/2005  
;       * added linesymbol flag (see glvoop_plotstyle__define.pro)
;       AGM 06/10/2005  
;       * changed access to glvoop_plotstyle to inherit from 
;       * glvoop_plotstyle_composition__define.pro (Strategy pattern implemented! I think)
;       AGM 18/05/2007  
;       * renamed to ffs_ (framework feature synthesis).
;       CHN 07/06/2007  
;       * modified getPars method to be able to filter. 
;         whether fitting pars / static pars / all pars (default) are returned.
;       CHN 12/06/2007 
;       * modified multiple methods accessing ffs_pars properties via getPars method
;         to account for new 'fitting' and 'static' keywords.
;       * added element_ws_test external routine for testing an element with static pars.
;       CHN 13/06/2007  
;       * most of the methods accessing/ modifying ffs_pars via getPars method
;         now include 'static' and 'fitting' keywords to return
;         data concerning static (feature 'settings') and fitting
;         parameters respectively.
;       * all references to 'parameters' have been renamed 'pars' for consistency.
;       * any method declaration with a long parameter list has been modified to 
;         be split across many lines to make the arguments appear in a column.
;       * cleaned up modification history comments to current ffs standard format :)
;       * extracted test procedures to seperate file (ffs_element_tests.pro) to keep this cleaner
;       AGM 20/06/2007 
;       * In getParnames method fixed parnames variable not being an array (so return
;         last value stored in it from the loop.;-
;       CHN 08/08/2007 
;       * Fixed addPar to allow for adding non-fitting pars.
;       * Removed some error handler blocks as they are v. annoying whilst testing code
;       CHN 09/08/2007 
;       * added saveResult method and new result state variable.
;       * simplified evaluate to use the parser's evaluate method and now saves result.
;   1.1 ADW 10/08/2007
;       * Initial commit to CVS.
;   1.2 CHN 13/08/2007
;       * updated cleanup method
;       * added getResult method
;   1.3 AGM 16/08/2007
;       * Removed ffs_plotstyle_composition and all references to it in both
;         ffs_model__define.pro and ffs_element__define.pro. 
;   1.4 AGM 29/08/2007
;       * fixed getParNames methods for case where an element had no parameters.
;   1.5 CHN 21-08-2007
;       * removed ffs_par_composition inheritance so brought self.container 
;         back into this class
;       * removed the following methods:
;         - convolve
;         - evaluate
;         - setStaticParData
;         - getStaticParData
;         - getCentroid
;         - getArea
;         - setRule
;         - getRule
;         - getIFunct
;         - IFunct
;         - getParContainer
;       * changed all references to getParContainer to getContainer
;       * removed the following state variables and their associated
;         init / cleanup:
;         - self.ifunct
;         - self.rule
;   1.6 CHN 20/09/2007 
;       * Init Changed use of cn_container to new ffs_contain object.
;       * getNumPars now uses method 'count' of ffs_contain
;   1.7 CHN 15/10/2007 
;       * removed superfluous code in setparvals
;   1.8 ADW 13/11/2007 
;       * Added evaluate method rather than having each individual element
;         supplying an evaluate. Individual elements now supply a calculate
;         method which is called by evaluate.
;       * Modified saveresult so that it's a function which returns a
;         getresult for code neatness. Also had it set a self.changed=1
;         anytime a new result is stored.
;       * Added a method to check changed and at the same time decrement it
;         this means changed will only ever be one and it's assumed that
;         after it's been checked that it's now 'seen'.
;       * Allowed evaluate function to do a poor man's memoization and
;         won't re-evaluate itself if the last call to it contained the
;         same parameters and each of it's sub-elements havn't changed.
;       * Added manual override to memoization with the memoclear method
;         to force a re-evaluation and a memocleared method to check it.
;   1.9 CHN 05/12/2007 
;       * Changed setparvals method to a function rather than procedure.
;       * setparvals returns error message if specified pars are not found.
;   1.10 CHN 07/12/2007 
;       * Added set/get parlimits methods note that:
;       - unlike most of the other methods, setparlimits will only allow
;         one set of limits to be passed for multiple pars - NOT, for example,
;         a pointer array to a set of limits for each par.
;       - getparlimits can returns a pointer array to two element 
;         arrays for each par (if multiple pars specified).
;       * Added similar methods for set/get hardlimits
;   1.11 ADW 11/12/2007 
;        * Added set/get children methods note that:
;          - This represents a change in how evaluate is done, elements
;            now know who their children are rather than the parser passing
;            around structures at evaluation time.
;          - A children tag has been added to the main data structure.
;        * Made change to evaluate code to reflect new style evaluation
;   1.12 CHN 19/12/2007 
;        * Now inherits from ffs_primitive - replacement for cn_primitive.
;        * Removed the following methods:
;          - getproperty
;          - setproperty
;          - setstate
;          - help (this functionality remains from inherited object ffs_primitive)
;        * Modified 'getparinfo' method to:
;          - set fitting keyword when calling getpars
;          - utilise new par method 'getmpfstr' instead of removed 'getstate'.
;        * Where required, changed calls to par methods that were previously
;          procedures to function calls.
;        * Added 'debug' keyword for init.
;        * Removed any unecessary use of '_Extra' keyword.
;        * Removed 'staticpardata' keyword in obj_new call made in 'addpar'.
;        * Modified multiple 'for loops' from blocks to single line statements.
;        * Moved methods added on 11-12-2007 by ADW above define-init-cleanup.
;   1.13 ADW 20/12/2007 
;        * Evaluate method now pre-calls other evaluate methods so now need to
;          do them outside anymore.
;   1.14 CHN 20/12/2007 
;        * Removed the following methods:
;          - ispartypevalid
;          - gettype
;          - settype
;          - gettype
;          - getptype
;          - getpartypes
;          - gettied
;          - writeidlobject
;        * Removed any other occurence of 'type' or 'partype' concept.
;        * Simplified removepar if block
;        * Simplified init if block
;   1.15 CHN 23/01/2008
;        * Added the following methods:
;          - ispar
;          - isprop
;        * Removed the following methods:
;          - 'findpar' (unused)
;          - 'setparinfo (ffs_par::setstate that it utilised no longer supported)
;        * New ispar and isprop methods utilised throughout in place of other
;          logic blocks - this has resulted in substantial changes in some places.
;        * Modified 'getparvals' to work for ffs_prop (enabled the static/fitting
;          keywords). Now, if /static - returns pointer array to values, /fitting
;          returns, as normal, an array of doubles. -1 is returned to signify 
;          failure.
;        * Added 'name' keyword to 'getparvals'
;        * Converted the following procedural methods to functions as part of
;          error handling strategy:
;          - setname
;          - setelementname
;          - setpars
;          - setparfixed
;          - setparerrors
;          - addpar
;          - addprop
;          - removepar
;        * Removed unused input 'obj' from method 'getchildren'.
;        * Re-write of documentation header.
;        * Cleaned up various inline documentation and old individual 
;          method headers.
;   1.16 CHN 19/02/2008
;        * self.children now an object reference (to an IDL_container) rather $
;          than pointer array.
;        * added 'maxchildren' state variable.
;        * maxchildren set to 0 in init.
;        * cleanup destroys new self.children object.;
;        * added methods:
;          - removechild
;          - addchild
;          - getmaxchildren
;        * modified setchildren & getchildren to fit with new container object
;          for children.
;   1.17 CHN 19/02/2008
;        * fixed bug in 'getpars' method - needed to check where statements for
;          return of -1.
;   1.18 CHN 25/02/2008
;        * Added 'minchildren' state variable.
;        * 'minchildren' set to 0 in init.
;        * Added methods:
;          - setmaxchildren
;          - setminchildren
;          - getminchildren
;   1.19 ADW 13/03/2008
;        * Made getchildren return all children and removed /all keyword
;        * Added a scalar string 'subtype' to state strucutre.
;        * Added methods:
;          - setsubtype
;          - getsubtype
;   1.20 CHN 14/08/2008
;        * Changed references to 'getmpfstr' to 'getmpfitstr' due to
;          rename of this par method.
;        * added the following methods:
;          - haschildren
;          - flatten
;        * self.children is now using an ffs_contain rather than IDL_container.
;        * Error checking added to addchild and removechild (possible due to
;          use of ffs_contain.
;   1.21 CHN 15/08/2008
;        * Provided 'free'/'fixed' options in getpars.
;        * Provided 'free'/'fixed' options in getnumpars.
;        * Re-write of 'getpars' to make better use of error checking.
;        * Added methods:
;          - getpd                                           
;          - calcpd                                          
;        * Fixed bug in getparfixed related to case of eleme nt having no pars.
;        * Changed calls to par method 'seterror' in 'setpar errors' and
;          'setxdata' in 'init' to reflect the fact that the se are now function
;           calls.                                           
;        * Utilised error handling in 'setparrerrors'.       
;   1.22 CHN 07/10/2008
;        * Corrected error in getpd algorithm. 
;   1.23 CHN 07/11/2008
;        * Replaced previous 'getpd' implementation.
;        * Added the following methods:
;          - pdsetup
;          - storedepwhat
;          - getdepwhat
;          - storedephow
;          - getdephow
;          - storedeplcl
;          - getdepcpl
;        * Improved error message returned for parname search failure in
;          'setparvals'.
;        * Allowed for keywords 'free' and 'fixed' in methods:
;          - getparvals
;          - getparnames
;        * New state variables and associated cleanup operations added:
;          - dephow
;          - depwhat
;          - deplcl
;   1.24 CHN 07/11/2008
;        * Setparfixed still had an old style setfixed (procedure) call in it.
;          Replaced with function call and error message setting.
;   1.25 CHN 21/11/2008
;        * Changes to 'calcpd':
;          - uncouple any coupled pars to allow value changes for finite
;            difference method, then recouple.
;          - removed keyword 'relstep'; instead get this from parameter's attribute
;          - get 'step' from parameter attribute.
;          - decreased default relstep from 0.001 to 0.0001 
;   1.26 ADW 02/07/2009
;        * Efficiency increases:
;          - New variable depcou to go with depwhat etc.
;          - Inline speedup of getpd
;          - New routines getnxdata and setnxdata to save an n_elements
;            call on xdata.
;          - Addition of fastsetup method to cache various things.
;          - Addition of /fast keyword to setparvals
;   1.27 CHN 08/07/2009
;        * Removed superfluous 'getnumpars' call / error check in getpars.
;        * Re-ordered code such that call to container's 'getcount', is only
;          done if necessary.
;   1.28 CHN 07/08/2009
;        * Corrections to pdsetup:
;          - lcl only stored single object reference - did not allow multiple
;            local parameters to depend on the independant variable in question
;            lcl was only saving last detected dependance.
;          - dephow assignment no longer incremental (resultant dephow is no
;            longer be a sum of all dephow).
;          - dependence through child  now indicated by (dephow) value of 3, 
;            rather than a 4.
;          - merged separate loop storing 'depcou' with main code block.
;        * Changes to 'getpd':
;          - allow for the element having more than one local parameter which
;            which is dependant upon the requested par.
;   1.29 CHN 17/08/2009
;        * Made the following changes to 'getpd':
;          - fixed 'deplcl' and 'dephow' indexing error introduced in last update.
;          - removed unused variable.
;   1.30 CHN 23/10/2009
;        * Changes to 'calcpd':
;          - Increased default step size in 'calcpd'.
;          - Catch for case when step will push value outside hardlimits and adjust
;            the dx to be used for numerical derivative accordingly.
;        * Complete re-write of 'flatten'.
;   1.31 CHN 18/02/2010
;        * Corrected significant issue with pdsetup / getpd system - dephow
;          was being overwritten where dependency on par is via two paths,
;          e.g. directly and via coupling, leading to ommision of certain terms
;          in the pd calculation. Rectified by storing 2D array of flags to
;          to indicate dependencies.
;        * Added two new methods:
;          - trim_wavegrid
;          - expand_wavegrid.
;   1.32 CHN 22/03/2011
;        * Altered 'expand_wavegrid':
;          - operate on child elements in reverse order.
;        * Changed 'getpd':
;          - error catch for no depwhat pointer (i.e. element with no pars).
;
;----------------------------------------------------------------------
;
; First version distributed with ADAS.
;
; MODIFIED   :
;       1.1     Martin O'Mullane
;                 - First central ADAS version.
;
; VERSION    :
;       1.1    04-01-2013
;
;----------------------------------------------------------------------
;-

function ffs_element::trim_wavegrid, wavelength, intensity
    if self.expandpix eq -1 then return, self->getxdata()
    return, {wavelength:wavelength[self.expandpix:n_elements(wavelength)-self.expandpix-1], $
                intensity:intensity[self.expandpix:n_elements(intensity)-self.expandpix-1]}
end


function ffs_element::expand_wavegrid, x0, intensity, interval

    ; deal with edge effects (must extend wavelength range of child branch
    ; of tree by half-width at upper and lower sides).
    ; determine and apply additional wavelength interval 
    x = self -> getxdata()
    pixelwidth = mean(x[1:*]-x)
    npix = ceil(interval / pixelwidth)
    self.expandpix = npix
    paddinglow = x[0]+(findgen(npix)-npix)*pixelwidth
    paddinghigh = x[n_elements(x)-1]+(findgen(npix)+1)*pixelwidth
    
    ; store current xdata for all elements in child branch
    child = self->getchildren()
    branch = reverse([child, child->flatten()])
    origx = ptrarr(n_elements(branch))
    for i=0, n_elements(branch)-1 do origx[i] = ptr_new(branch[i]->getxdata())

    ; set xdata to low padding region and evaluate   
    for i=0, n_elements(branch)-1 do $
        if branch[i]->setxdata(paddinglow) eq 0 then return, $
        self->seterrmsg('error setting branch xdata for low padding' $
            + branch[i]->geterrmsg())
    for i=0, n_elements(branch)-1 do $
        reslow = *(branch[i]->evaluate())
    
    ; set xdata to high padding region and evaluate 
    for i=0, n_elements(branch)-1 do $
        if branch[i]->setxdata(paddinghigh) eq 0 then return, $
        self->seterrmsg('error setting branch xdata for high padding' $
            + branch[i]->geterrmsg())
    for i=0, n_elements(branch)-1 do $
        reshigh = *(branch[i]->evaluate())
    
    ; reset xdata on all branch elements
    for i=0, n_elements(branch)-1 do $
        if branch[i]->setxdata(*(origx[i])) eq 0 then return, $
        self->seterrmsg('error resetting branch xdata after padding' $
            + branch[i]->geterrmsg())
            
    ; free all pointers holding original xdata
    for i=0, n_elements(origx)-1 do ptr_free, origx[i]

    ; expand this element's x, x0 and intensity arrays with the new data
    x = [paddinglow, x, paddinghigh]
    x0 = [reslow.wavelength, x0, reshigh.wavelength]
    intensity = [reslow.intensity, intensity, reshigh.intensity]
    return, {x:x, x0:x0, intensity:intensity}
end

function ffs_element::calcpd, ref, childres
    ;print, 'ffs_element numerical being used', ' ', self->getname()
    initial = ref->getvalue()               
    ; temporary - must utilise better system for choosing step
    ; cannot use relstep when value is zero. Makes more sense to
    ; base step size on limits anyway.
    limits = ref->getlimits()

;lines below to be removed 10/09/10
if (ref->getstep()) gt 0.0 then step = ref->getstep() else begin 
    if total([(limits eq !values.d_infinity) , (limits eq -1.0*!values.d_infinity)]) eq 0 then $
        step = 0.01*abs(limits[1]-limits[0]) $
    else begin
        if (relstep = ref->getrelstep()) eq 0 then relstep=0.01
        ;move value just off zero (if that is where it is)...
        if initial eq 0.0 then initial=1.0e-10
        if (step = ref->getstep()) eq 0 then step = initial*relstep
    endelse
;lines below to be removed 10/09/10
endelse
    ;must temporarily uncouple ref if coupled
    if obj_valid((tempcpl = ref->getcoupled())) then $
        if ref->uncouple(/no_destroy) eq 0 then $
        return, self->seterrmsg(ref->geterrmsg())
    
    
    ;qqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqq
    ; need to watch for step pushing value over hardlimits here.
    ; qqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqq
    
    val = initial+step
    dx1 = step
    if val gt ref->gethardmax() then begin
        val = ref->gethardmax()
        dx1 =  val-initial
    endif
    ; will not check error status here for speed.
    isok = ref->setvalue(val)    
    fa = (*(self->evaluate())).intensity           
    
    val = initial-step
    dx2 = step
    if val lt ref->gethardmin() then begin
        val = ref->gethardmin()
        dx2 = initial-val
    endif
    
    ; will not check error status here for speed.
    isok = ref->setvalue(val)    
    fb = (*(self->evaluate())).intensity           

    ; reset to original value
    isok = ref->setvalue(initial)   

    ;must re-couple if par previously uncoupled.
    if obj_valid(tempcpl) then $
        if ref->setcoupled(tempcpl) eq 0 then $
            return, self->seterrmsg(ref->geterrmsg())

        if finite(((fa-fb) / (dx1+dx2))[0]) eq 0 then stop
    return, ((fa-fb) / (dx1+dx2))
    
end

function ffs_element::getpd, ref, num=num, relstep=relstep
    if ptr_valid(self.depwhat) eq 0 then return, fltarr(self.nxdata)
    what = *self.depwhat
    idx = (where(what eq ref))[0]
    if idx eq -1 then return, fltarr(self.nxdata)
    res = 0.0d0    
    if (*self.dephow)[idx, 1] gt 0 then begin
    	lref=*((*self.deplcl)[idx])
        for i=0, n_elements(lref)-1 do $
            if keyword_set(num) then $
                res += ((*((*self.depcou)[idx]))[i]->getparpd(ref))*self->ffs_element::calcpd(lref[i], relstep=relstep) $
            else $
                res += ((*((*self.depcou)[idx]))[i]->getparpd(ref))*self->calcpd(lref[i])
        ;return, res
    endif
    
    if ((*self.dephow)[idx, 0] gt 0) or ((*self.dephow)[idx, 2] gt 0) then begin
        if keyword_set(num) then $
            res += self->ffs_element::calcpd(ref, relstep=relstep) $
        else $
            res += self->calcpd(ref)
    endif
    return, res
end

function ffs_element::pdsetup, ref
    res = intarr(n_elements(ref), 3)
    lcl = ptrarr(n_elements(ref))
    mult = ptrarr(n_elements(ref))
    cou = ptrarr(n_elements(ref))
    
    freepars = self->getpars(/free, count=owncount)
    fixedpars = self->getpars(/fixed, count=fixcount)
    
    idx=-1
    for i=0, owncount-1 do begin
        idx = where(ref eq freepars[i])
        if idx[0] ne -1 then res[idx, 0] = 1
    endfor
    for i=0, fixcount-1 do begin
            cobj = fixedpars[i]->getcoupled()
            if obj_valid(cobj) then begin
                cplto = cobj->flatten()
                for j=0, n_elements(cplto)-1 do begin
                    idx = (where(ref eq cplto[j]))[0]
                    if idx ne -1 then begin
                        res[idx, 1] = 1
                        if ptr_valid(lcl[idx]) then begin
                            templcl = lcl[idx]
                            tempcou = cou[idx]
                            lcl[idx] = ptr_new([*templcl, fixedpars[i]])
                            cou[idx] = ptr_new([*tempcou, cobj])
                            ptr_free, templcl
                            ptr_free, tempcou
                        endif else begin
                            lcl[idx] = ptr_new(fixedpars[i])
                            cou[idx] = ptr_new(cobj)
                        endelse
                    endif
                endfor
            endif
    endfor
    
    children = self->getchildren(count=childcount)
    
    for j=0, childcount-1 do begin
        idx = where(children[j]->pdsetup(ref) ne 0)
        if idx[0] ne -1 then res[idx, 2] = 1
    endfor

    temp = total(res, 2)
    idx = where(temp ne 0)
    
    if idx[0] ne -1 then begin
        isok = self->storedepwhat(ref[idx])
        isok = self->storedephow(res[idx, *])
        isok = self->storedeplcl(lcl[idx])
        isok = self->storedepcou(cou[idx])
    endif

    return, temp
end

function ffs_element::storedepcou, ref
    if ptr_valid(self.depcou) then ptr_free, self.depcou
    self.depcou = ptr_new(ref)
    return, 1
end

function ffs_element::getdepcou
    return, *self.depcou
end

function ffs_element::storedepwhat, ref
    if ptr_valid(self.depwhat) then ptr_free, self.depwhat
    self.depwhat = ptr_new(ref)
    return, 1
end

function ffs_element::getdepwhat
    return, *self.depwhat
end

function ffs_element::storedephow, list
    if ptr_valid(self.dephow) then ptr_free, self.dephow
    self.dephow = ptr_new(list)
    return, 1
end

function ffs_element::getdephow
    return, *self.dephow
end

function ffs_element::storedeplcl, lcl
    if ptr_valid(self.deplcl) then ptr_free, self.deplcl
    self.deplcl = ptr_new(lcl)
    return, 1
end

function ffs_element::getdeplcl
    if ptr_valid(self.deplcl) eq 0 then return, -1
    return, *self.deplcl
end

function ffs_element::fastsetup
    freepars = self->getpars(/free, count=owncount)    
    if ptr_valid(self.cache_pars) then ptr_free,self.cache_pars
    self.cache_pars=ptr_new(freepars)
    return,1
end

function ffs_element::setsubtype,in
    if n_elements(in) ne 1 then return,self->seterrmsg('Subtype must be a scalar')
    if size(in,/tname) ne 'STRING' then return,self->seterrmsg('Subtype must be a string')
    self.subtype=in
    return,1
end

function ffs_element::flatten, count=rtncnt

    if self->haschildren() eq 0 then begin
        rtncnt = 1
        return, self
    endif

    children = self->getchildren(count=count)
    
    expidx=0
    j=0
        
    for i=0, count-1 do begin
        if children[j]->haschildren() then begin
            temp = [children[0:j], children[j]->flatten(count=expidx)]
            if j+1 le n_elements(children)-1 then $
                children = [temp, children[j+1:n_elements(children)-1]] $
            else $
                children = temp
        endif else expidx=0
        j+=expidx+1
    endfor

    rtncnt = n_elements(children)
    return, children
end

;+
;NAME:
;ispar
;-
FUNCTION ffs_element::ispar, ref
    if n_elements(ref) eq 0 then return, self->seterrmsg('Tried to check zero element variable.')
    if size(ref, /type) ne 11 then return, self->seterrmsg('Wrong type - must be an object reference to check if valid ffs_par.')
    dummy = where(obj_isa(ref, 'FFS_PAR') eq 1, count)
    return, count eq n_elements(ref) ? 1 : 0
END

;+
;NAME:
;isprop
;-
FUNCTION ffs_element::isprop, ref
    if n_elements(ref) eq 0 then return, self->seterrmsg('Tried to check zero element variable.')
    if size(ref, /type) ne 11 then return, self->seterrmsg('Wrong type - must be an object reference to check if valid ffs_prop.')
    dummy = where(obj_isa(ref, 'FFS_PROP') eq 1, count)
    return, count eq n_elements(ref) ? 1 : 0
END


FUNCTION ffs_element::setnxdata,nx
     self.nxdata=nx
     return,1
END

FUNCTION ffs_element::getnxdata
     return,self.nxdata
END

;+
;NAME:
;getXdata
;-
FUNCTION ffs_element::getXdata
  IF PTR_VALID(self.p_xdata) THEN BEGIN
     return, *self.p_xdata
  ENDIF ELSE BEGIN
     return, 'invalid' ;; return a string (can't return a undefined variable...)
  ENDELSE
END

;+
;NAME:
;setXdata
;-
function ffs_element::setXdata, xdata
  IF N_ELEMENTS(xdata) EQ 0 THEN return, self->seterrmsg('No xdata input')
  IF ((SIZE(xdata, /TYPE) EQ 5) OR (SIZE(xdata, /TYPE) EQ 4)) EQ 0 THEN return, self->seterrmsg('xdata supplied of wrong data type')
  PTR_FREE, self.p_xdata
  self.p_xdata = PTR_NEW(xdata)
  if self->setnxdata(n_elements(xdata)) eq 0 then return,0
  return, 1
END

;+
;NAME:
;setElementName
;-
function ffs_element::setElementName,elementName
    if size(elementname, /type) eq 7 then begin
        self.elementname = elementName
        return, 1
    endif else return, self->seterrmsg('Input name not of type string.')
end

;+
;NAME:
;setName
;-
function ffs_element::setName,elementName
    return, self->setElementName(elementName)
end

;+
;NAME:
;getName (same as getElementName)
;returns the name/label of the element eg 'carbon3' etc
;note: duplicate funtionality of getName/getElementName is to meet the requirements of the container class
;while allowing distinction between getting element/par names when delving from higher level, ie model.
;-
FUNCTION ffs_element::getName
    RETURN, self.elementname
END


;+
;NAME:
;getElementName
;returns the name/label of the element eg 'carbon3' etc
;note: duplicate funtionality of getName/getElementName is to meet the requirements of the container class
;while allowing distinction between getting element/par names when delving from higher level, ie model.
;-
FUNCTION ffs_element::getElementName
    RETURN, self.elementname
END

;+
;NAME:
;setPars
;-
function ffs_element::setPars,pars
    if self->ispar(pars) then begin
        container = (self->getContainer())
        container->remove,/all
        for i=0,n_elements(pars)-1 do self->addpar,pars[i]
        return, 1
    endif else return, 0
end


;+
;NAME:
;getPars
;-
function ffs_element::getpars, $
  all = all, $
  parname = parname, $
  position = position, $
  count = count, $
  static = static, $ 
  fitting = fitting, $
  free = free, $
  fixed = fixed
  
  if keyword_set(static) && keyword_set(fitting) then return, $
    self->seterrmsg("'static' cannot be used in conjunction with " + $
        "'fitting' or 'free' keywords in call to getpars.", retval=-1)

  if keyword_set(parname) then begin
     pars = ((self->getcontainer())->get(name = parname, count = count)) ;; 06-08-2004 agm -- what happens here if parname is not a valid parname of the element?
  endif else if n_elements(position) gt 0 then begin
     pars = ((self->getcontainer())->get(position = position, count = count))
  endif else begin
     pars = ((self->getcontainer())->get(/all, count = count))
  endelse

  if count eq 0 then return, $
    self->seterrmsg('No pars that meet search criteria found', retval=-1)
    
  fitidx = where(obj_isa(pars, 'FFS_PAR') eq 1, nfit)
  staticidx = where(obj_isa(pars, 'FFS_PROP') eq 1, nstatic) 

  if (nfit+nstatic) ne count then begin
    count = 0
    return, $
        self->seterrmsg('One or more of the references obtained were not ' + $
        'ffs_par or ffs_prop', retval=-1)
  endif

    ; set default index list (all)
    idx = indgen(count)
  
  if keyword_set(static) then begin 
    idx = staticidx
    count = nstatic
  endif  
  if keyword_set(fitting) then begin
    idx = fitidx
    count = nfit
  endif

  if keyword_set(free) && nfit gt 0 then begin
    pars = pars[fitidx]
    idx = intarr(nfit)
    for i=0, nfit-1 do idx[i] = pars[i]->getfixed() ? 0 : 1
    idx = where(idx eq 1, count)
  endif
  
  if keyword_set(fixed) && nfit gt 0 then begin
    pars = pars[fitidx]
    idx = intarr(nfit)
    for i=0, nfit-1 do idx[i] = pars[i]->getfixed()
    idx = where(idx eq 1, count)
  
  endif
   
  if count gt 0 then return, pars[idx] else $
    return, self->seterrmsg('No pars that meet search criteria found', retval=-1)    
end

;+
;NAME:
;getParInfo
;-
FUNCTION ffs_element::getParInfo, $
  PARNAME=parname, $
  POSITION = position, $
  STATIC=static, $
  FITTING=fitting

  pars = self->getPars(PARNAME=parname, POSITION=position, $
    COUNT=count, /fitting)

  IF OBJ_VALID(pars[0]) THEN BEGIN
     parInfoArr = REPLICATE(pars[0]->getmpfitstr(), 1)
     IF count GE 2 THEN BEGIN
          FOR i=1,N_ELEMENTS(pars)-1 DO BEGIN
             parInfoArr = [parInfoArr,(pars[i]->getmpfitstr())[0]]
         ENDFOR
      ENDIF
  ENDIF ELSE parInfoArr = -1

  RETURN, parInfoArr
 END


;+
;NAME:
;setParFixed
;-
function ffs_element::setparfixed, $
  parfixed, $
  parname=parname, $
  position = position
  
  pars = self->getpars(parname=parname, position=position, count=count, /fitting)
 
  if self->ispar(pars) then begin
     if n_elements(parfixed) eq 1 then parfixed = parfixed + intarr(n_elements(pars))
     for i = 0, n_elements(pars)-1 do $
        if pars[i]->setfixed(parfixed[i]) eq 0 then return, $
            self->seterrmsg(pars[i]->geterrmsg())
     return, 1
  endif else return, 0
end


;+
;NAME:
;getParFixed
;-
FUNCTION ffs_element::getParFixed, $
  PARNAME=parname, $
  POSITION=position

  pars = self->getPars(PARNAME=parname, POSITION=position, COUNT=count, /FITTING)  
  IF count NE 0 THEN BEGIN
        results = INTARR(count)
        FOR i = 0, count-1 DO results[i] = pars[i]->getFixed()
  ENDIF ELSE results = -1
  RETURN, results
END

;+
;NAME:
;setParErrors
;-
function ffs_element::setParErrors, $
  parErrors, $
  PARNAME=parname, $
  POSITION=position

  pars = self->getPars(PARNAME = parname, POSITION = position, COUNT = count)
  IF self->ispar(pars) THEN BEGIN
     IF N_ELEMENTS(parErrors) EQ 1 THEN parerrors = parerrors + DBLARR(N_ELEMENTS(pars))
     FOR i = 0, N_ELEMENTS(pars)-1 DO $
        if pars[i]->setError(parErrors[i]) eq 0 then return, $
        self->seterrmsg('issue while setting par errors: '+pars[i]->geterrmsg())
     return, 1
  ENDIF else return, self->seterrmsg('no ffs_par objects to work on')
END



;+
;NAME:
;getParErrors
;-
FUNCTION ffs_element::getParErrors, $
  PARNAME=parname, $
  POSITION=position

  pars = self->getPars(PARNAME = parname, POSITION = position, COUNT = count)
  IF count NE 0 THEN BEGIN
     results = DBLARR(count)
     FOR i = 0, count-1 DO results[i] = pars[i]->getError()
  ENDIF ELSE results = -1
  RETURN, results
END



;+
;NAME:
;setparlimits
;-
function ffs_element::setparlimits, $
  limits, $
  parname=parname, $
  position = position
  
  pars = self->getpars(parname=parname, position=position, count=count, /fitting)
  if count ne 0 then begin
     for i=0, count-1 do $
       if pars[i]->setlimits(limits) eq 0 then return, self->seterrmsg(pars[i]->geterrmsg())
  endif else return, self->seterrmsg('specified pars not found')
  
  return, 1      
end


;+
;NAME:
;getparlimits
;-
function ffs_element::getparlimits, $
  parname=parname, $
  position=position

  pars = self->getpars(parname=parname, position=position, count=count, /fitting)
  if count ne 0 then begin
    limits = ptrarr(count)
    for i=0, count-1 do limits[i] = ptr_new(pars[i]->getlimits())
    return, limits
  endif else return, -1
end


;+
;NAME:
;setparhardlimits
;-
function ffs_element::setparhardlimits, $
  limits, $
  parname=parname, $
  position = position
  
  pars = self->getpars(parname=parname, position=position, count=count, /fitting)
  if count ne 0 then begin
     for i=0, count-1 do $
       if pars[i]->sethardlimits(limits) eq 0 then return, self->seterrmsg(pars[i]->geterrmsg())
  endif else return, self->seterrmsg('specified pars not found')
  
  return, 1      
end

;+
;NAME:
;getparhardlimits
;-
function ffs_element::getparhardlimits, $
  parname=parname, $
  position=position

  pars = self->getpars(parname=parname, position=position, count=count, /fitting)
  if count ne 0 then begin
    limits = ptrarr(count)
    for i=0, count-1 do limits[i] = ptr_new(pars[i]->gethardlimits())
    return, limits
  endif else return, -1
end

;+
;NAME:
;setParVals
;-
FUNCTION ffs_element::setParVals, $
  parVals, $
  NAME = name, $;chn 21-08-2007
  PARNAME=parname, $
  POSITION=position, $
  STATIC=static, $
  FITTING=fitting, $
  free=free, $
  fixed=fixed, $
  fast=fast

  if keyword_set(fast) then begin
        pars=*self.cache_pars
        FOR i = 0, N_ELEMENTS(pars)-1 DO junk=pars[i]->setValue(parVals[i],/fast)
        return,1
  endif

  IF N_ELEMENTS(name) GT 0 THEN parname = name ;chn 21-08-2007
  
  pars = self->getPars(PARNAME=parname, POSITION=position, $
    COUNT=count, STATIC=static, FITTING=fitting, free=free, fixed=fixed)
  IF count GT 0 THEN BEGIN  
	 
    IF N_ELEMENTS(parvals) EQ 1 $
      AND (STRLOWCASE(SIZE(parvals[0], /TNAME)) EQ 'double' $
      OR STRLOWCASE(SIZE(parvals[0], /TNAME)) EQ 'float') THEN $
        parvals = parvals + dblarr(N_ELEMENTS(pars))

    FOR i = 0, N_ELEMENTS(pars)-1 DO if pars[i]->setValue(parVals[i]) eq 0 then return, self->seterrmsg('Error in setting par value: '+pars[i]->geterrmsg())

  ENDIF ELSE begin
    elementname = self->getname()
    if n_elements(parname) gt 0 || n_elements(name) gt 0 then $
        additional = 'Please check names suplied ('+parname+') against' + $
        ' allowed values ('+strjoin(self->getparnames(), ', ')+').' $
    else additional=''
    return, self->seterrmsg('Specified pars (for '+elementname+') not found. ' $
        + additional) 
  endelse
  
  return, 1
END


;+
;NAME:
;getParVals
;-
FUNCTION ffs_element::getParVals, $
  NAME=name, $
  PARNAME = parname, $
  POSITION = position, $
  STATIC=static, $
  FITTING=fitting
  
  if n_elements(name) gt 0 then parname = name
  
  pars = self->getPars(PARNAME=parname, POSITION=position,  $
    COUNT=count, static=static, fitting=fitting)  
  IF count NE 0 THEN BEGIN
     if keyword_set(fitting) then begin
        results = dblarr(count) 
        FOR i = 0, count-1 DO results[i] = pars[i]->getValue()
     endif else begin
        results = ptrarr(count)
        FOR i = 0, count-1 DO results[i] = PTR_NEW(pars[i]->getValue())
     endelse   
  ENDIF ELSE results = -1
  RETURN, results
END


;+
;NAME:
;getContainer
;-
FUNCTION ffs_element::getContainer
    RETURN, self.container
END


;+
;NAME:
;isParnameValid
;-
FUNCTION ffs_element::isParnameValid, parname
    IF SIZE(parname,/tname) EQ 'STRING' THEN BEGIN
          validparnames = self->getParNames()
        index = WHERE(parname EQ validparnames, count)
        IF count NE 0 THEN return, 1 ELSE return, 0
    ENDIF ELSE return, self->seterrmsg('Must input a parname - of type string')
END


;+
;NAME:
;getParnames
;-
FUNCTION ffs_element::getParNames,$
    POSITION=position, $
    STATIC=static, $
    FITTING=fitting, $
    FREE=free, $
    fixed=fixed
   
    pars = self->getPars(POSITION=position, $
        COUNT=count, STATIC=static, FITTING=fitting, free=free, fixed=fixed)
      
    IF count GT 0 THEN BEGIN
        parnames = STRARR(count)
        FOR i=0, count-1 DO parnames[i] = pars[i]->getName()
    ENDIF ELSE RETURN, -1
 
    RETURN, parnames
END


;+
;NAME:
;getNumPars
;-
function ffs_element::getnumpars, $
    static=static, $
    fitting=fitting, $
    free=free, $
    fixed=fixed
    
    if (keyword_set(static) or keyword_set(fitting) or keyword_set(free) or keyword_set(fixed)) then begin
        pars = self->getpars(static=static, fitting=fitting, free=free, fixed=fixed, count=count)
        return, count
    endif else return,(self->getcontainer())->count()
end


;+
;NAME:
;saveResult
;-
function ffs_element::saveResult, result
  IF N_ELEMENTS(result) GT 0 THEN BEGIN
    IF PTR_VALID(self.result) THEN PTR_FREE, self.result
    self.result = PTR_NEW(result)
    self.changed=1
  ENDIF
  return,self->getresult()
END


;+
;NAME:
;getResult
;-
FUNCTION ffs_element::getResult
    RETURN, self.result  
END


;+
;NAME:
;addPar
;-
function ffs_element::addpar,par, parname=parname
    if self->ispar(par) then begin 
        (self->getcontainer())->add,par
        return, 1
    endif else if n_elements(parname) gt 0 && size(parname,/type) eq 7 then begin
        temp = obj_new('ffs_par',parname=parname)
        if self->ispar(temp) then (self->getcontainer())->add,temp else return, 0
        return, 1
    endif else return, self->seterrmsg("need to either supply valid ffs_par object, or a string via keyword 'parname'")
end

;+
;NAME:
;addProp
;-
function ffs_element::addprop,prop, name=name
    if self->isprop(prop) then begin
      (self->getcontainer())->add,prop
      return, 1
    endif else if n_elements(name) gt 0 && size(name,/type) eq 7 then begin
        temp = obj_new('ffs_prop', name=name)
        if self->isprop(temp) then (self->getcontainer())->add,temp else return, 0
        return, 1
    endif else return, self->seterrmsg("need to either supply valid ffs_prop object, or a string via keyword 'name'")
end

;+
;NAME:
;removePar
;-
function ffs_element::removePar,par,PARNAME=parname, POSITION=position
    IF self->ispar(par) THEN (self->getContainer())->remove,par else return, 0
    IF N_ELEMENTS(parname) GT 0 && self->isParnameValid(parname) THEN (self->getContainer())->remove,NAME=parname else return, 0
    IF N_ELEMENTS(position) GT 0 THEN (self->getContainer())->remove,POSITION=position else return, 0
    return, 1
END

;+
;NAME:
;memoclear
;-
function ffs_element::memoclear
	return,(self.clearmemo=1)
end

;+
;NAME:
;memocleared
;-
function ffs_element::memocleared
        return,self.clearmemo--
end

;+
;NAME:
;changed
;-
function ffs_element::changed
	return,self.changed--
end

function ffs_element::haschildren
    children = self->getchildren(count=count)
    return, count gt 0
end

;+
;NAME:
;setchildren
;-
function ffs_element::setchildren,obj
    if self.children->remove(/all, /destroy) eq 0 then return, $
        self->seterrmsg('Container caused error:'+self.children->geterrmsg())  
    if self.children->add(obj) eq 0 then return, $
        self->seterrmsg('Container caused error:'+self.children->geterrmsg())
    return,1
end

;+
;NAME:
;getchildren
;-
function ffs_element::getchildren, count=count
	return, self.children->get(/all, count=count)
end

;+
;NAME:
;removechild
;-
function ffs_element::removechild, ref
    if self.children->remove(ref) eq 0 then return, $
        self->seterrmsg('Container caused error:'+self.children->geterrmsg())  
end

;+
;NAME:
;addchild
;-
function ffs_element::addchild, ref
    if self.children->add(ref) eq 0 then return, $
        self->seterrmsg('Container caused error:'+self.children->geterrmsg())  
end

;+
;NAME:
;setmaxchildren
;-
function ffs_element::setmaxchildren, in
    if size(in, /type) lt 2 || size(in, /type) gt 3 then return, $
        self->seterrmsg('Error setting maxchildren - incorrect input type') 
    self.maxchildren = in
    return, 1
end

;+
;NAME:
;getmaxchildren
;-
function ffs_element::getmaxchildren
    return, self.maxchildren
end

;+
;NAME:
;setminchildren
;-
function ffs_element::setminchildren, in
    if size(in, /type) lt 2 || size(in, /type) gt 3 then return, $
        self->seterrmsg('Error setting minchildren - incorrect input type') 
    self.minchildren = in
    return, 1
end

;+
;NAME:
;getminchildren
;-
function ffs_element::getminchildren
    return, self.minchildren
end

;+
;NAME:
;evaluate
;-
function ffs_element::evaluate,memo=memo
	neweval=0
	obj=self->getchildren(count=count)
	if count gt 0 then begin
		for i=0,n_elements(obj)-1 do junk=(obj)[i]->evaluate(memo=memo)
        	for i=0,n_elements(obj)-1 do if ((obj)[i]->changed() eq 1) then neweval=1
        	in=ptrarr(n_elements(obj))
                for i=0,n_elements(obj)-1 do in[i]=(obj)[i]->getresult()
        endif

	if not keyword_set(memo) then return,self->saveResult(self->calculate(in))

	newpars=self->getparvals(/fitting)

	if self->memocleared() eq 1 then neweval=1
	if size(newpars,/type) ne 2 then if not (ptr_valid(self.memoparvals) && array_equal(newpars,*self.memoparvals)) then neweval=1

	if neweval eq 0 then return,self->getresult()
	if ptr_valid(self.memoparvals) then ptr_free,self.memoparvals
	self.memoparvals=ptr_new(newpars)
        return,self->saveResult(self->calculate(in))
end

function ffs_element::setsubtype,in
	if n_elements(in) ne 1 then return,self->seterrmsg('Subtype must be a scalar')
	if size(in,/tname) ne 'STRING' then return,self->seterrmsg('Subtype must be a string')
        self.subtype=in
	return,1
end

function ffs_element::getsubtype,in
	return,self.subtype
end


;+
;NAME:
;cleanup
;This method frees all object values and destroys all objects stored
;in the container. This is already handled in ffs_container::cleanup, so
;cleanup only needs to perform an obj_destroy on the parcontainer. Also
;makes the call to the superclass ffs_primitive's cleanup.
;-
PRO ffs_element::cleanup
  IF PTR_VALID(self.p_xdata) THEN PTR_FREE, self.p_xdata 
  IF PTR_VALID(self.result) THEN PTR_FREE, self.result
  IF OBJ_VALID(self.container) THEN BEGIN
    isok = self.container->remove(/all, /destroy)
    OBJ_DESTROY, self.container
  ENDIF  
  IF OBJ_VALID(self.children) THEN OBJ_DESTROY, self.children
  if ptr_valid(self.dephow) then ptr_free, self.dephow
  if ptr_valid(self.depwhat) then ptr_free, self.depwhat
  if ptr_valid(self.deplcl) then ptr_free, self.deplcl
  if ptr_valid(self.depcou) then ptr_free, self.depcou
  if ptr_valid(self.memoparvals) then ptr_free, self.memoparvals
  if ptr_valid(self.cache_pars) then ptr_free, self.cache_pars
  self->ffs_primitive::cleanup
END


;+
;NAME:
;Init
;This method initializes the object. Calls the parent's Init method
;-
FUNCTION ffs_element::init, $
  ELEMENTNAME = elementname, $
  NAME = name, $ 
  XDATA = xdata, $ 
  debug=debug

  if self->ffs_primitive::init() ne 1 then return, 0
  self.elementname = 'default'
  IF N_ELEMENTS(elementname) GT 0 THEN self.elementname = elementname $
    ELSE IF N_ELEMENTS(name) GT 0 THEN self.elementname = name
    
  IF N_ELEMENTS(xdata) GT 0 THEN return, self->setXdata(xdata) 
  self.container = OBJ_NEW('ffs_contain')
  self.children = obj_new('ffs_contain')
  self.maxchildren = 0
  self.minchildren = 0
  self.expandpix = -1
  RETURN, 1
END


;+
;NAME:
;define
;standard class definition, inherits ffs_primitive.
;-
PRO ffs_element__define, class
    class = {ffs_element, $
            elementname:'', $
	    subtype:'', $
	    container:OBJ_NEW(), $ ; brought back from par composition
            p_xdata:PTR_NEW(), $ 
            nxdata:0, $ 
	    result:PTR_NEW(), $
            memoparvals:ptr_new(), $
            changed:0, $
            clearmemo:0, $
            children:obj_new(), $
            minchildren:0, $
            maxchildren:0, $
            dephow:ptr_new(), $
            depwhat:ptr_new(), $
            deplcl:ptr_new(), $
            depcou:ptr_new(), $
            cache_pars:ptr_new(), $
            expandpix:0, $
            INHERITS ffs_primitive}
END


