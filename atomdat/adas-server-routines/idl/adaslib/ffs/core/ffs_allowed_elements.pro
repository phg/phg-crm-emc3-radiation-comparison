;+
; PROJECT:
;   FFS - Framework for Feature Synthesis (Object oriented model/fitting).
;
; NAME:
;   FFS_ALLOWED_ELEMENTS
;
; PURPOSE:
;   Provides a list of the available FFS elements that can be added to an FFS
;   model.
;
; EXPLANATION:
;   FFS models are comprised of FFS elements. When adding elements to a model
;   from a GUI (or any other external application) it is necessary to know
;   which elements are available to choose from - this routine supplies such
;   a list, as well as providing info on any possible input to the routines.
;
; USE:
;   allowedstr = ffs_allowed_elements() 
;
; PARAMETERS:
;   None.
;
; KEYWORDS:
;   None.
;
; OUTPUT:
;   an array of structures with the fields:
;       type    -   string; the name of the element type i.e. a file 
;                   'ffs_<type>__define.pro' exists.
;       inputs  -   pointer->string; list of possible string input values to
;                   pass to the init of the element.
;
; AUTHOR:
;   Christopher Nicholas, University of Strathclyde.
;
; VERSION HISTORY:
;   1.1  CHN 19/02/2008
;        * Initial Commit.                                              
;----------------------------------------------------------------------
;
; First version distributed with ADAS.
;
; MODIFIED   :
;       1.1     Martin O'Mullane
;                 - First central ADAS version.
;
; VERSION    :
;       1.1    04-01-2013
;
;----------------------------------------------------------------------
;-
function ffs_allowed_elements 
    temp = {type:'', inputs:ptr_new()}
    eleminfo = replicate(temp, 10) 
        
    eleminfo[0].type = 'adas'
    eleminfo[0].inputs = ptr_new(['balmer-archived', 'zeeman', 'envelope'])

    eleminfo[1].type = 'add'
        
    eleminfo[2].type = 'background'
    eleminfo[2].inputs = ptr_new(['linear', 'constant'])
   
    eleminfo[3].type = 'broaden'
    eleminfo[3].inputs = ptr_new(['gaussian', 'lorentzian', 'voigtian'])
    
    eleminfo[4].type = 'gaussian'
    
    eleminfo[5].type = 'line'
    
    eleminfo[6].type = 'lorentzian'
    
    eleminfo[7].type = 'multiply'
    
    eleminfo[8].type = 'shift'
    eleminfo[8].inputs = ptr_new(['lambda', 'pixel'])

    eleminfo[9].type = 'voigtian'

    return, eleminfo
end
