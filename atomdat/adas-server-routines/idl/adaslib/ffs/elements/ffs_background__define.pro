;+
; PROJECT:
;   FFS - Framework for Feature Synthesis (Object oriented model/fitting).
;
; NAME:
;   FFS_BACKGROUND
;
; PURPOSE:
;   An FFS element - provides linear background for a model.
;
; EXPLANATION:
;   Provides evaluation of a linear background level dependant upon the input
;   parameters. The resultant profile is utilised (commonly in conjuction with 
;   many other FFS elements) in building model spectra with the FFS system. 
;   Note that the background can be of type 'constant' - defined simply by an
;   offset 'c' (linear background with no gradient), or it can be of type 
;   'linear' where, in addition to parameter 'c', the gradient, 'm', is also 
;   specified. Note also that the 'c' is not a y-intercept value (i.e. at x=0).
;   It in fact specifies the intercept of the line at x=min(wavelength).
;  
; USE:
;   An example of creating a background with gradient of 2 and intensity offset
;   of 100.0:
;     bckgrnd = obj_new('ffs_background', m=2.0, c=100.0)
;   or alternatively:
;     bckgrnd = obj_new('ffs_background')
;     isok = bckgrnd->setparvals('m', 2.0)
;     isok = bckgrnd->setparvals('c', 100.0)
;
; INITIALISATION SYNTAX:
;   background_obj = obj_new('ffs_background', m=m, c=c, debug=debug)
;   PARAMETERS:
;     subtype -   specifies the type of background (constant, linear...).
;   KEYWORDS:
;     m     -	the value of the background gradient.
;     c     -	the value of the background offset (see note in 'explanation' above.
;     debug - 	set this keyword to enable debug information to be printed to the terminal.
;
; PUBLIC ROUTINES:
;   In addtion to the methods listed below, this object inherits methods from ffs_element
;   - refer to this class' documentation for more details.
;
;   [calculate]
;     PURPOSE:
;	Performs calculation of the profile.
;     INPUTS:
;	in   -  input required by the ffs system, but unused by this element.		
;     OUTPUTS:
;	Returns a structure with the following fields:
;	wavelength   -  double array of the wavelength grid.	 
;	intensity    -  double array of profile intensity values.
;	gridded     -	0 or 1 to signify if on a wavelength grid - 1 in this case.
;     SIDE EFFECTS:
;	None.
;
; AUTHOR:
;   Allan Whiteford, University of Strathclyde.
;
; VERSION HISTORY:
;   1.1 ADW 19/10/2007
;   	* Placeholder routine, Chris will tidy up.
;   1.2 ADW 13/11/2007
;   	* Changed evaluate function name to calculate and made it return a
;   	  results structure rather storing it and returning a getresults.
;   1.3 CHN 05/12/2007
;   	* Altered all calls to 'setparvals' to reflect update of this method
;    	  from procedure to function as part of error handling strategy.
;   1.4 CHN 08/01/2007
;   	* Added documentation block.
;   	* Corrected syntax error for 'setparvals' calls if bytpe set to 'linear'.
;   	* Added 'm' keyword to init.
;   	* Added debug keyword to init to pass thorugh to superclass.
;   1.5 CHN 10/11/2008
;       * Changed addpar method calls to functions rather than procedure.
;       * 4 character indentation throughout.
;   1.6 CHN 10/08/2009
;       * Removed local 'btype' state variable.
;       * Use 'subtype' attribute (and methods) from superclass in place of
;         'btype'. Important for the parser, when using 'writedefinition'.
;   1.7 CHN 09/06/2010
;       * Corrected incorrect formula for line with gradient!
;       * Added 'calcpd' method for analytic derivatives.
;----------------------------------------------------------------------
;
; First version distributed with ADAS.
;
; MODIFIED   :
;       1.1     Martin O'Mullane
;                 - First central ADAS version.
;
; VERSION    :
;       1.1    04-01-2013
;
;----------------------------------------------------------------------
;-


function ffs_background::calcpd, ref, childres  
    pars = self -> getPars() 
    if ((paridx = where(pars eq ref)))[0] eq -1 then stop
    if paridx eq 0 then $
        return, fltarr(n_elements(self->getxdata()))+1.0
    if paridx eq 1 then $
        return, (self->getxdata())-(self->getxdata())[0]
        
end

FUNCTION ffs_background::calculate, in
  
    x = self -> getXdata()
    parVals = self->getParVals(/FITTING)

    if self->getsubtype() eq 'constant' then begin
        c = parVals[0]
        output =  {wavelength:x,intensity:fltarr(n_elements(x))+c, gridded:1}
    endif
  
    if self->getsubtype() eq 'linear' then begin
        c = parVals[0]
	m = parVals[1]
        output =  {wavelength:x,intensity:(x-x[0])*m+c, gridded:1}
    endif

    return,output  
END  
  
FUNCTION ffs_background::init,$
    subtype, $
    m=m, $
    c=c, $
    debug=debug
  
    If Self->ffs_element::Init(debug=debug) NE 1 Then Return, 0
    if n_elements(subtype) ne 1 then return, self->seterrmsg('Must specify background type.')
    if self->setsubtype(subtype) eq 0 then return, self->seterrmsg('failed to set background type...' + $
        self->geterrmsg())

    if subtype eq 'constant' then begin
    	    if self->addPar(PARNAME='c') eq 0 then stop
    	    IF N_ELEMENTS(c) GT 0 THEN $
                if self->setParVals(c, parname='c') eq 0 then return, 0
    endif
   
    if subtype eq 'linear' then begin
    	if self->addPar(PARNAME='c') eq 0 then stop
    	if self->addPar(PARNAME='m') eq 0 then stop
    	IF N_ELEMENTS(c) GT 0 THEN $
            if self->setParVals( c, parname='c') eq 0 then return, 0
    	IF N_ELEMENTS(m) GT 0 THEN $
            if self->setParVals( m, parname='m') eq 0 then return, 0
    endif

    RETURN, 1
END

PRO ffs_background__define
    self = {	ffs_background, $
                inherits ffs_element}
END

