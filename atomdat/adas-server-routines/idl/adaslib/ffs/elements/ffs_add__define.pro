;+
; PROJECT:
;   FFS - Framework for Feature Synthesis (Object oriented model/fitting).
;
; NAME:
;   FFS_ADD
;
; PURPOSE:
;   An FFS operator element -  performs addition of operand element results.
;
; EXPLANATION:
;   Performs addition operation on other FFS element results. This has a different meaning
;   depending upon whether the operand intensity values are 'gridded' or not.
;
;   If the results are gridded, then takes input of the result of other FFS element
;   evaluations and returns the linear superposition of the profiles as the new result.
;
;   If the results are not gridded, then instead the operand intensity arrays are
;   simply concatenated.   
;  
; USE:
;   Example of stand alone use to follow at later date. Currently only used 
;   indirectly by FFS system parser.
;
; INITIALISATION SYNTAX:
;   add_obj = obj_new('ffs_add', debug=debug)
;
;   PARAMETERS:
;     unusedParserInput -   input required by the ffs_parser, but unused by this element;
;   KEYWORDS:
;     debug - 	set this keyword to enable debug information to be printed to the terminal.
;
; PUBLIC ROUTINES:
;   In addtion to the methods listed below, this object inherits methods from ffs_element
;   - refer to this class' documentation for more details.
;
;   [calculate]
;     PURPOSE:
;	Performs FFS add operation on operand elements.
;     INPUTS:
;	in   -  An array of other FFS element evaluation output structures i.e. a
;           	structure with the fields:
;   	    	  wavelength  	-   double array of the wavelength grid.	 
;	    	  intensity     -   double array of profile intensity values.
;	    	  gridded     	-   0 or 1 to signify if intensities mapped to 
;   	    	    	    	    wavelength grid.	
;     OUTPUTS:
;	Returns a structure with the following fields:
;	wavelength  -	double array of the wavelength grid.	 
;	intensity   -	double array of profile intensity values.
;	gridded     -	0 or 1 to signify if intensities mapped to 
;   	    	    	wavelength grid.
;     SIDE EFFECTS:
;	None.
;
; AUTHOR:
;   Christopher Nicholas, University of Strathclyde
;
; VERSION HISTORY:
;   1.1  ADW 10/08/2007
;    	 * Initial commit to CVS
;   1.2  CHN 13/08/2007
;    	 * modified evaluate method to save result before returning value
;   1.3  CHN 11/09/2007
;    	 * removed usage of obsolete setRule
;    	 * updated to support ffs_prop objects
;   1.4  ADW 22/10/2007
;    	 * Removed error catching and some older stuff which isn't necessary
;          any longer
;   1.5  ADW 13/11/2007
;    	 * Changed evaluate function name to calculate and made it return a
;          results structure rather storing it and returning a getresults.
;   1.6  CHN 08/01/2008
;   	 * Added debug keyword to init to pass thorugh to superclass.
;   	 * Added documentation block.
;   1.7  CHN 19/02/2008
;        * Set 'maxchildren' in init.
;   1.8  CHN 25/02/2008
;        * Set 'minchildren' in init.
;        * Added error message to calculate method for case of no input.
;   1.9  CHN 15/09/2008
;        * added 'calcpd' method.
;   1.10 CHN 10/11/2008
;        * Removed 'childres' input to 'calcpd' and use 'getchildren' internally
;          instead.
;        * Tidied 'for loop' syntax in 'calculate'.
;        * Altered documentation to allow for 4 character version number.
;----------------------------------------------------------------------
;
; First version distributed with ADAS.
;
; MODIFIED   :
;       1.1     Martin O'Mullane
;                 - First central ADAS version.
;
; VERSION    :
;       1.1    04-01-2013
;
;----------------------------------------------------------------------
;-
function ffs_add::calcpd, ref
    children=self->getchildren(count=count)
    if count eq 0 then return, $
        self->seterrmsg('No operand elements for operator element to work on')
    res=fltarr(n_elements(self->getxdata()))
    for i=0, count-1 do $
            res += children[i]->getpd(ref, num=num, relstep=relstep)
    return, res
end

FUNCTION ffs_add::calculate,in
    if n_elements(in) eq 0 then return, $
        self->seterrmsg('No operand elements for operator element to work on')
    gridded=(*in[0]).gridded

    if gridded eq 1 then begin
	wavelength=(*in[0]).wavelength
	intensity=fltarr(n_elements(wavelength))
        for i=0,n_elements(in)-1 do $
		  intensity += (*in[i]).intensity
    endif else begin
	wavelength=[0.0]
	intensity=[0.0]
        for i=0,n_elements(in)-1 do begin
            wavelength=[wavelength,(*in[i]).wavelength]
	    intensity=[intensity,(*in[i]).intensity]
	endfor
	wavelength=wavelength[1:*]
	intensity=intensity[1:*]
    endelse
  
   return,{wavelength:wavelength,intensity:intensity,gridded:gridded}
END


FUNCTION ffs_add::init,$
    unusedParserInput, $
    debug=debug

    if self->ffs_element::init(debug=debug) ne 1 then return, 0
    self.minchildren = 1
    self.maxchildren = 32767
    RETURN, 1
END


PRO ffs_add__define
    self = {ffs_add, $
         inherits ffs_element}
END

