;+
; PROJECT:
;   FFS - Framework for Feature Synthesis (Object oriented model/fitting).
;
; NAME:
;   FFS_POLYNOMIAL
;
; PURPOSE:
;   An FFS element - evaluates a polynomial of degree n
;      p(x;ai) = SUM ( ai*x^(i) ) with i->0 to n
;
; EXPLANATION:
;   Provides evaluation of a polynomial dependant upon the input 
;   parameters. 
;  
; USE:
;   An example of creating a polynomial 1 + 2*x + 3*x^2:
;     poly = obj_new('ffs_polyseries', coeffs=[1,2,3])

;
; INITIALISATION SYNTAX:
;   polynomial_obj = obj_new('ffs_polyseries', coeffs, pars=pars)
;   PARAMETERS:
;     unusedParserInput -   input required by the ffs_parser, but unused by this
;     element
;   KEYWORDS:
;     coeffs = input (required) of polynomial coefficients: a0, a1,
;     a2, ...
;     pars  -	input of pos, fwhm and area can be passed as a 'pars' array
;
;     debug - 	set this keyword to enable debug information to be printed to 
;               the terminal.
;
; PUBLIC ROUTINES:
;   In addtion to the methods listed below, this object inherits methods from 
;   ffs_element - refer to this class' documentation for more details.
;
;   [calculate]
;     PURPOSE:
;	Performs calculation of the profile.
;     INPUTS:
;	in   -  input required by the ffs system, but unused by this element.		
;     OUTPUTS:
;	Returns a structure with the following fields:
;	wavelength  -   double array of the wavelength grid.	 
;	intensity   -   double array of profile intensity values.
;	gridded     -	0 or 1 to signify if on a wavelength grid - 1 in this 
;                       case.
;     SIDE EFFECTS:
;	None.
;
; AUTHOR:
;   Andrew Meigs, CCFE The United Kingdom Atomic Energy Authority
;   (Based on Andrew Meigs' GLV gaussian)
;
; VERSION HISTORY:
; 1.0 AGM 08/10/2010 general polynomial element with no shifts.
;----------------------------------------------------------------------
;
; First version distributed with ADAS.
;
; MODIFIED   :
;       1.1     Martin O'Mullane
;                 - First central ADAS version.
;
; VERSION    :
;       1.1    04-01-2013
;
;----------------------------------------------------------------------
;-
function ffs_gaussian::calcpd, ref
    pars = self -> getPars()
    if ((paridx = where(pars eq ref[0])))[0] eq -1 then stop  
    
    ;;result = *(self->getresult())
    parvals = self -> getParVals(/fitting)
    x = self -> getXdata() ;; result.wavelength
    if paridx eq 0 then return, 0*x
    return, paridx*parvals[paridx]*x^(paridx-1)

end

FUNCTION ffs_polyseries::getHornerCoeffs, in
  ;; Calculate the Horner Coefficients 
  ;; (see http://en.wikipedia.org/wiki/Horner_scheme
  ;; or 
  ;;
  ;;http://rosettacode.org/wiki/Horner%27s_rule_for_polynomial_evaluation)
  ;;From this link:
  ;;coefficients : = [-19, 7, -4, 6] # list coefficients of all x^0..x^n in order
  ;;x : = 3
  ;;accumulator : = 0
  ;;for i in length(coefficients) downto 1 do
  ;;  # Assumes 1-based indexing for arrays
  ;;  accumulator := ( accumulator * x ) + coefficients[i]
  ;;done
  ;;# accumulator now has the answer


  x = self -> getXdata()      
  pars = self->getPars()
  parvals = self -> getParVals(/fitting) ;; 
  
  ncoeffs = N_ELEMENTS(parvals)

  hcoeffs = DBLARR(ncoeffs, N_ELEMENTS(x))

  hcoeffs[ncoeffs-1, *] = parvals[ncoeffs-1]
  FOR i = ncoeffs-2, 0, -1 DO BEGIN
     hcoeffs[i, *] = parvals[i] + hcoeffs[i+1, *]*x
  ENDFOR
  return, hcoeffs
END

FUNCTION ffs_polyseries::calculate, in, horner = horner
  x = self -> getXdata()      
  ;pars = self->getPars(/fitting)
  ;parvals = self -> getParVals(/fitting) ;; 
 ; 
  ;ncoeffs = N_ELEMENTS(parvals);
;
 ; fval = 0.0D*x
  ;FOR i = ncoeffs-1, 0, -1 DO BEGIN
   ;  fval = fval +  (parvals[i]*x
  
  ;;http://rosettacode.org/wiki/Horner%27s_rule_for_polynomial_evaluation)
  ;;From this link:
  ;;coefficients : = [-19, 7, -4, 6] # list coefficients of all x^0..x^n in order
  ;;x : = 3
  ;;accumulator : = 0
  ;;for i in length(coefficients) downto 1 do
  ;;  # Assumes 1-based indexing for arrays
  ;;  accumulator := ( accumulator * x ) + coefficients[i]
  ;;done
  ;;# accumulator now has the answer 

  ;IF keyword_set(horner) THEN BEGIN
  ;   hcoeffs = self -> getHornerCoeffs()
  ;   return, {wavelength:x, intensity:REFORM(hcoeffs[0, *]), gridded:1}
  ;ENDIF ELSE BEGIN
     parvals = self -> getParVals(/fitting)
     ncoeffs = N_ELEMENTS(parvals) ;
     accumulator = 0.0D
     FOR i = ncoeffs-1, 0, -1 DO BEGIN
        accumulator = accumulator*x + parvals[i]
     ENDFOR
     return, {wavelength:x, intensity:accumulator, gridded:1}
  ;ENDELSE

END

FUNCTION ffs_polyseries::addCoeffs, coeffs
  IF N_ELEMENTS(coeffs) EQ 0 THEN return, 0

  ncoeffs = N_ELEMENTS(coeffs)
  parnames = 'a'+strtrim(indgen(ncoeffs), 2)
  print, 'parnames = ', parnames
  FOR i = 0, N_ELEMENTS(coeffs)-1 DO BEGIN
     IF self -> addPar(PARNAME = parnames[i]) EQ 0 THEN return, 0
     if self -> setParVals(coeffs[i], parname = parnames[i]) eq 0 then return, 0
  ENDFOR
  return, 1
END

pro ffs_polyseries::cleanup
  self -> ffs_element::cleanup
end

FUNCTION ffs_polyseries::init,$
  unusedParserInput, $
  pars = pars, $ ;; order is a0,a1,a2...an for f(x,ai) = a0 + a1*x + a2*x^2 + a3*x^3...
  debug = debug
    
    If Self->ffs_element::Init(debug=debug) NE 1 Then Return, 0
    
    IF N_ELEMENTS(pars) NE 0 THEN IF self -> addCoeffs(pars) EQ 0 THEN return, 0

    RETURN, 1
END

PRO ffs_polyseries__define
    self = {ffs_polyseries, $
            inherits ffs_element}
END
