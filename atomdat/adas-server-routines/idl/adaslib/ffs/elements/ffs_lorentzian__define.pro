;+
; PROJECT:
;   FFS - Framework for Feature Synthesis (Object oriented model/fitting).
;
; NAME:
;   FFS_LORENTZIAN
;
; PURPOSE:
;   An FFS element - evaluates a lorentzian profile.
;
; EXPLANATION:
;   Provides evaluation of a lorentzian line profile dependant upon the input parameters. 
;   The resultant profile is utilised (commonly in conjuction with many other FFS elements) 
;   in building model spectra with the FFS system.
;  
; USE:
;   An example of creating a lorentzian profile at central wavelength 100.0; 
;   full width at half maximum intensity of 25.0 and with area 250.0:
;     lz = obj_new('ffs_lorentzian', pos=100.0, fwhm=25.0, area=250.0)
;   or alternatively:
;     lz = obj_new('ffs_lorentzian')
;     isok = lz->setparvals('pos', 100.0)
;     isok = lz->setparvals('fwhm', 25.0)
;     isok = lz->setparvals('area', 250.0)
;
; INITIALISATION SYNTAX:
;   lorentz_obj = obj_new('ffs_lorentzian',pos=pos, fwhm=fwhm, area=area, pars=pars, trap=trap)
;   PARAMETERS:
;     unusedParserInput  -   input required by the ffs_parser, but unused by this element
;   KEYWORDS:
;     pos	-   central wavelength position of lorentzian line profile
;     fwhm   	-   the full width at half maximum of the profile
;     area	-   profile area (intensity)
;     pars	-   input of pos, fwhm and area can be passed as a 'pars' array
;     trap	-   parameter at which evaluation truncated [abs(x-x0) = trap * fwhm]
;   debug - 	set this keyword to enable debug information to be printed to the terminal.
;
; PUBLIC ROUTINES:
;   In addtion to the methods listed below, this object inherits methods from ffs_element
;   - refer to this class' documentation for more details.
;
;   [calculate]
;     PURPOSE:
;	Performs calculation of the profile.
;     INPUTS:
;	in   -  input required by the ffs system, but unused by this element.		
;     OUTPUTS:
;	Returns a structure with the following fields:
;	wavelength   -  double array of the wavelength grid.	 
;	intensity    -  double array of profile intensity values.
;	gridded     -	0 or 1 to signify if on a wavelength grid - 1 in this case.
;     SIDE EFFECTS:
;	None.
;
; AUTHOR:
;   Christopher Nicholas, University of Strathclyde
;   (Based on Andrew Meigs' GLV lorentzian)
;
; VERSION HISTORY:
;   1.1  ADW 10/08/2007
;   	 * Initial commit to CVS.
;   1.2  CHN 13/08/2007
;   	 * Complete overhaul of this class. Utilises the main parts of the old 
;          lorentzian class' evaluate, but otherwise altered significantly to fit
;   	   into the new ffs_model_parser evaluation step. This class was based 
;          upon ffs_gaussian.
;   1.3  CHN 11/09/2007
;   	 * removed usage of obsolete setRule.
;   	 * updated to support ffs_prop objects.
;   1.4  CHN 16/10/2007
;   	 * removed print statement at the beginning of the evaluate method saying
;   	   '#featuretype# evaluate'.
;   1.5  CHN 22/10/2007
;   	 * changed 'fwhmg' keyword and par created to 'fwhm'
;   	 * removed extraneous comments and old commented out lines
;   	 * removed 'voigtian_evaluate' function - originally intended for
;    	   lorentzian broadened by gaussian instrument function. No longer
;    	   relevant to new FFS.
;   	 * inserted documentation
;   	 * removed 'extra' at init
;   1.6  ADW 13/11/2007
;   	 * Changed evaluate function name to calculate and made it return a
;    	   results structure rather storing it and returning a getresults.
;   1.7  CHN 05/12/2007
;   	 * Altered all calls to 'setparvals' to reflect update of this method
;    	   from procedure to function as part of error handling strategy.
;   	 * Altered keywords and references to 'ctr' -> 'pos' for consistency with
;    	   'pos' used in ffs_line.
;   1.8  CHN 07/12/2007
;   	 * Added setting of appropriate hard / soft limits on pars in init.
;   1.9  CHN 08/01/2008
;   	 * Added debug keyword to init to pass thorugh to superclass.
;   	 * Re-write of documentation.
;   	 * Removed obsolete partype keyword from addpar calls.
;   1.10 CHN 24/01/2008
;        * Altered documentation to allow for 4 character version number.
;        * changed addpar calls in init to reflect changes to ffs_element.
;        * changed addprop calls in init to reflect changes to ffs_element.
;   1.11 CHN 14/08/2008
;        * Redfined evaluation expression to define trap in terms of fwhm,
;          rather than hwhm.
;        * Defined default trap in init and removed default setting of trap
;          in calculate method.
;        * Added method 'getpd'.
;   1.12 CHN 15/09/2008
;        * Renamed 'getpd' method to 'calcpd'.         
;----------------------------------------------------------------------
;
; First version distributed with ADAS.
;
; MODIFIED   :
;       1.1     Martin O'Mullane
;                 - First central ADAS version.
;
; VERSION    :
;       1.1    04-01-2013
;
;----------------------------------------------------------------------
;-

function ffs_lorentzian::calcpd, ref, childres  
    c = sqrt(4.0d*alog(2.0d))
    pars = self -> getPars() 
    x0 = pars[0]->getValue()
    fw = pars[1]->getValue()
    area = pars[2]->getValue()
    if ((paridx = where(pars eq ref)))[0] eq -1 then stop
    result = *(self->getresult())
    x = result.wavelength
    if paridx eq 0 then return, $
        2.0d0*(x-x0)/((x-x0)^2+(fw/2.0d0)^2) * result.intensity
    ;if paridx eq 0 then return, $
    ;    (x-x0)/((x-x0)^2+(fw/2.0)^2) * result.intensity    
    if paridx eq 1 then return, $
        ((x-x0)^2 - (fw/2.0)^2) / ((x-x0)^2+(fw/2.0d0)^2) * result.intensity / fw
    if paridx eq 2 then return, $
        (1.0d0/area) * result.intensity
end

FUNCTION ffs_lorentzian::calculate,in

  x = self -> getXdata()
  pars = self -> getPars()
  x0 = pars[0]->getValue()
  fw = pars[1]->getValue()
  area = pars[2]->getValue()
  trap = pars[3]->getValue()

  IF fw NE 0.0D THEN BEGIN
     result=(abs(x-x0) le trap*fw) * (area*(fw/2.0D)/((x-x0)^2 + (fw/2.0D)^2)/!DPI)
  ENDIF ELSE BEGIN
     result=area*((0.*x)*(x ne x0)+(x eq x0)) ; if fw eq 0 return delta function
  ENDELSE

  return,{wavelength:x, intensity:result, gridded:1}

END


FUNCTION ffs_lorentzian::init,$
    unusedParserInput, $
    pos=pos, $
    FWHM=fwhm, $
    AREA=area, $
    PARS=pars, $
    TRAP=trap, $
    debug=debug

    If Self->ffs_element::Init(debug=debug) NE 1 Then Return, 0

    if self->addPar(PARNAME='pos') eq 0 then return, 0
    if self->addPar(PARNAME='fwhm') eq 0 then return, 0
    if self->addPar(PARNAME='area') eq 0 then return, 0
    if self->addProp(NAME='trap') eq 0 then return, 0
    
    if self->setparhardlimits(PARNAME='fwhm', [0.0d,!values.f_infinity]) eq 0 then return, 0    
    if self->setparlimits(PARNAME='area', [0.0d,!values.f_infinity]) eq 0 then return, 0

    if n_elements(pos) gt 0 then if self->setparvals(pos,parname='pos') eq 0 then return, 0
    if n_elements(fwhm) gt 0 then if self->setparvals(fwhm,parname='fwhm') eq 0 then return, 0
    if n_elements(area) gt 0 then if self->setparvals(area,parname='area') eq 0 then return, 0
    if n_elements(pars) eq 3 and size(pars,/tname) ne 'string' then if self->setparvals(pars) eq 0 then return, 0

    if n_elements(trap) gt 0 then begin
      if self->setParVals(trap, name='trap') eq 0 then return, 0
    endif else if self->setparvals(40.0d, name='trap') eq 0 then return, 0
  
    RETURN, 1
END


PRO ffs_lorentzian__define
    self = {ffs_lorentzian, $
            inherits ffs_element}
END

