;+
; PROJECT:
;   FFS - Framework for Feature Synthesis (Object oriented model/fitting).
;
; NAME:
;   FFS_GAUSSIAN
;
; PURPOSE:
;   An FFS element - evaluates a gaussian profile
;
; EXPLANATION:
;   Provides evaluation of a gaussian line profile dependant upon the input 
;   parameters. The resultant profile is utilised (commonly in conjuction with
;   many other FFS elements) in building model spectra with the FFS system.
;  
; USE:
;   An example of creating a gaussian profile at central wavelength 100.0; 
;   full width at half maximum intensity of 25.0 and with area 250.0:
;     gs = obj_new('ffs_gaussian', pos=100.0, fwhm=25.0, area=250.0)
;   or alternatively:
;     gs = obj_new('ffs_gaussian')
;     isok = gs->setparvals('pos', 100.0)
;     isok = gs->setparvals('fwhm', 25.0)
;     isok = gs->setparvals('area', 250.0)
;
; INITIALISATION SYNTAX:
;   gaussian_obj = obj_new('ffs_gaussian',pos=pos, fwhm=fwhm, area=area, $
;   pars=pars, trap=trap)
;   PARAMETERS:
;     unusedParserInput -   input required by the ffs_parser, but unused by this
;     element
;   KEYWORDS:
;     pos   -	central wavelength position of gaussian line profile
;     fwhm  -	the full width at half maximum of the profile
;     area  -	profile area (intensity)
;     pars  -	input of pos, fwhm and area can be passed as a 'pars' array
;     trap  -	parameter at which evaluation truncated[abs(x-x0) = trap * fwhm]
;     debug - 	set this keyword to enable debug information to be printed to 
;               the terminal.
;
; PUBLIC ROUTINES:
;   In addtion to the methods listed below, this object inherits methods from 
;   ffs_element - refer to this class' documentation for more details.
;
;   [calculate]
;     PURPOSE:
;	Performs calculation of the profile.
;     INPUTS:
;	in   -  input required by the ffs system, but unused by this element.		
;     OUTPUTS:
;	Returns a structure with the following fields:
;	wavelength  -   double array of the wavelength grid.	 
;	intensity   -   double array of profile intensity values.
;	gridded     -	0 or 1 to signify if on a wavelength grid - 1 in this 
;                       case.
;     SIDE EFFECTS:
;	None.
;
; AUTHOR:
;   Christopher Nicholas, University of Strathclyde
;   (Based on Andrew Meigs' GLV gaussian)
;
; VERSION HISTORY:
;   1.1  ADW 10/08/2007
;   	 * Initial commit to CVS.
;   1.2  CHN 13/08/2007
;   	 * modified evaluate method to save result before returning value.
;   1.3  CHN 11/09/2007
;   	 * removed usage of obsolete setRule
;   	 * updated to support ffs_prop objects
;   1.4  CHN 16/10/2007
;   	 * removed print statement at the beginning of the evaluate method
;   	   saying '#featuretype# evaluate'.
;   1.5  CHN 22/10/2007
;   	 * changed 'fwhmg' keyword and par created to 'fwhm'
;   	 * removed extraneous comments and old commented out lines
;   	 * removed 'extra' at init
;   	 * inserted documentation
;   1.6  ADW 13/11/2007
;   	 * Changed evaluate function name to calculate and made it return a
;     	   results structure rather storing it and returning a getresults.
;   1.7  CHN 05/12/2007
;   	 * Altered all calls to 'setparvals' to reflect update of this method
;    	   from procedure to function as part of error handling strategy.
;   	 * Altered keywords and references to 'ctr' -> 'pos' for consistency 
;          with 'pos' used in ffs_line.
;   1.8  CHN 07/12/2007
;   	 * Added setting of appropriate hard / soft limits on pars in init.
;   1.9  CHN 08/01/2008
;   	 * Added debug keyword to init to pass thorugh to superclass.
;   	 * Re-write of documentation.
;   	 * Removed obsolete partype keyword from addpar calls.
;   1.10 CHN 23/01/2008
;        * Adjusted doc to allow for 4 character version numbers.
;        * Reformatted some doc lines to keep 80 characters wide max.
;        * changed addpar calls in init to reflect changes to ffs_element.
;        * changed addprop calls in init to reflect changes to ffs_element.
;   1.11 CHN 14/08/2008
;        * Added method 'getpd'
;   1.12 CHN 15/09/2008
;        * Renamed 'getpd' method to 'calcpd'.
;   1.13 CHN 10/11/2008
;        * Minor re-ordering in 'calcpd'.
;   1.14 CHN 09/02/2010
;        * Modified area minimum hard limit  to 0.00001 to avoid a divide by zero
;          at calcpd.
;----------------------------------------------------------------------
;
; First version distributed with ADAS.
;
; MODIFIED   :
;       1.1     Martin O'Mullane
;                 - First central ADAS version.
;
; VERSION    :
;       1.1    04-01-2013
;
;----------------------------------------------------------------------
;-

function ffs_gaussian::calcpd, ref
    pars = self -> getPars()
    if ((paridx = where(pars eq ref[0])))[0] eq -1 then stop  
    c = sqrt(4.0d*alog(2.0d)) 
    x0 = pars[0]->getValue()
    fw = pars[1]->getValue()
    area = pars[2]->getValue()
    result = *(self->getresult())
    x = result.wavelength
    if paridx eq 0 then return, 2.0d0*c^2*(x-x0)/fw^2 * result.intensity
    if paridx eq 1 then return, (2.0d0*c^2*(x-x0)^2/fw^2 - 1.0d0)/fw * result.intensity
    if paridx eq 2 then return, (1.0d0/area) * result.intensity
end


FUNCTION ffs_gaussian::calculate, in 
  x = self -> getXdata()
  pars = self -> getPars()
  x0 = pars[0]->getValue()
  fw = pars[1]->getValue()
  area = pars[2]->getValue()
  trap = pars[3]->getValue() 

  IF N_ELEMENTS(trap) EQ 0 THEN trap = 5.0D ELSE TRAP=Trap ;; setup the under flow parameter
  const = sqrt(4.0D*Alog(2.0D))

  IF fw NE 0 THEN BEGIN
     ; effectively this is setting the maximum (x-x0) to trap*fwhm
     arg = const*(abs((x-x0)/fw) < trap)
     ; if the arg reaches the limited value then zero the function
     result = exp(-arg*arg)*(arg lt const*trap)*area*const/sqrt(!DPI)/fw
  ENDIF ELSE BEGIN
     result = area*((0.*x)*(x ne x0)+(x eq x0)) ; if fw eq 0 return delta function
  ENDELSE

  return,{wavelength:x, intensity:result, gridded:1}
END


FUNCTION ffs_gaussian::init,$
    unusedParserInput, $
    pos=pos, $
    FWHM=fwhm, $
    AREA=area, $
    PARS=pars, $
    TRAP=trap, $
    debug=debug
    
    If Self->ffs_element::Init(debug=debug) NE 1 Then Return, 0
    if self->addPar(PARNAME='pos') eq 0 then return, 0
    if self->addPar(PARNAME='fwhm') eq 0 then return, 0
    if self->addPar(PARNAME='area') eq 0 then return, 0
    if self->addProp(NAME='trap') eq 0 then return, 0
    if self->setparhardlimits(PARNAME='fwhm', [0.0d,!values.f_infinity]) eq 0 then return, 0
    if self->setparlimits(PARNAME='area', [0.00001d,!values.f_infinity]) eq 0 then return, 0
    IF N_ELEMENTS(pos) GT 0 THEN if self->setParVals(pos,parname='pos') eq 0 then return, 0
    IF N_ELEMENTS(fwhm) GT 0 THEN if self->setParVals(fwhm,parname='fwhm') eq 0 then return, 0
    IF N_ELEMENTS(area) GT 0 THEN if self->setParVals(area,parname='area') eq 0 then return, 0
    IF N_ELEMENTS(pars) EQ 3 AND SIZE(pars,/tname) NE 'STRING' THEN if self->setParVals(pars) eq 0 then return, 0
    IF N_ELEMENTS(trap) GT 0 THEN BEGIN
      if self->setParVals(trap, name='trap') eq 0 then return, 0 
    ENDIF ELSE if self->setParVals(5.0d, parname='trap') eq 0 then return, 0
    RETURN, 1
END


PRO ffs_gaussian__define
    self = {ffs_gaussian, $
            inherits ffs_element}
END

