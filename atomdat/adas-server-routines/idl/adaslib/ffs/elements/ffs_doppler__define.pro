;+
;;13-08-2004 AGM started
; AGM 02-09-2005 added if xdata not passed to the evaluate... method
; the check validity of xdata in the pointer
;----------------------------------------------------------------------
;
; First version distributed with ADAS.
;
; MODIFIED   :
;       1.1     Martin O'Mullane
;                 - First central ADAS version.
;
; VERSION    :
;       1.1    04-01-2013
;
;----------------------------------------------------------------------
;-

FUNCTION ffs_doppler_evaluate,x,dti, vshift,area, TRAP=trap, DERS=ders
;
  IF N_ELEMENTS(x0) GE 3 THEN BEGIN ;CHN 24/06/2004
     xtemp = x0
     dti = xtemp[0]
     vshift = xtemp[1]
     area = xtemp[2]
  ENDIF
  ;;help, x,x0,fw,area
  const = sqrt(4.0D*Alog(2.0D))
  
  fwhmg = self -> translateToFwhmg()
  ctr = self -> translateToCtr()
  y = 
;

;
; Now adding partial derivatives (A Meigs 23-10-98)
;
; Compute partial derivatives ( so far not treating the delta function case)
;if N_ELEMENTS(DERS) NE 0 AND ders ne 0 then begin
;  if fw ne 0 then begin
;     pderctr = 2.0D*y*const*const*(x-x0)/fw^2*(arg lt const*trap) ; zero the derivatives if beyond the function evaluation range.
;     pderfw = y*(2.0D*const*const*((x-x0)/fw)^2-1.0D)/fw*(arg lt const*trap)
;     if area ne 0  then pderarea = y/area*(arg lt const*trap)
;  ENDIF
;  result = DBLARR(N_elements(x),4)
;  result[*,0] = y
;  result[*,1] = pderctr
;  result[*,2] = pderfw
;  result[*,3] = pderarea
;endif else result = y
RETURN, y  ;result
END  ; gaussian

;+
;NAME:
;evaluate
;parameter x, an array of independant variable values is passed in.
;Then accesses the parameters for current element and gets their respective values and
;adds them to an array before calling the particular "rule" procedure eg "agm_gaussian" for
;calculating the function, passing x and the array of parameter values as it does so.
;returns an array of f(x) values.
;CHN 28/07/2004 OVERRIDES: the inherited evaluate method of element
; AGM 02-09-2005 added if xdata not passed to the evaluate... method
; the check validity of xdata in the pointer
;-
FUNCTION ffs_doppler::evaluate, x, IFUNCTOFF = ifunctoff

  ;; 02-09-05 AGM added
  IF N_ELEMENTS(x) EQ 0 THEN BEGIN
     x = self -> getXdata()
     IF STRUPCASE(SIZE(x, /tname)) EQ 'STRING' THEN return, -1
  ENDIF ;; question is do we now set xdata?
  ;;
  parVals = self -> getParVals()

  ifunct = self -> getiFunct()
  IF KEYWORD_SET(ifunctoff) THEN BEGIN
     result = ffs_doppler_evaluate(x, parVals, TRAP = self.trap)
  ENDIF ELSE BEGIN
     IF self -> superclassCheck(iFunct) THEN BEGIN
        result = self -> convolve(x)
     ENDIF ELSE BEGIN
        result = ffs_doppler_evaluate(x, parVals, TRAP = self.trap)
     ENDELSE
  ENDELSE

  RETURN, result

END
;+
;NAME:
;setIFunct
; overrides the ffs_element setIFunct in that it also sets the
; contained ffs_gaussians IFunct as well
;-
PRO ffs_doppler::setIFunct,iFunct
  self -> ffs_modelement::setIFunct, iFunct
  self.gaussian -> setIFunct, iFunct

END

;+
;NAME:
;translateToCtr
;-
FUNCTION ffs_doppler::translateToCtr
  cvac = 2.99792458d8 ;; m/s
ctr = self.lambda0*(1.0D + self.vshift/cvac) ;; assuming vshift in m/s
return, ctr
END

;+
;NAME:
;translateToFwhmg
;-
FUNCTION ffs_doppler::translateToFwhmg
;; assume dTi in eV, lambda0 in angstroms
cvac = 2.99792458d18 ;; 1d+10 A/m  2.99792458d8 ;; m/s
kboltz =  1.380658d-23 ;; J/K
amu = 1.6605402d-27 ;; kg
eV = 1.602117733d-19 ;; J/eV  
const = sqrt(4.0D*Alog(2.0D))

;;fwhmg =
;;const*(self.lambd0/cvac)*sqrt(2*kboltz*self.dTi/self.atmass*amu)
fwhmg = const*(self.lambd0/cvac)*sqrt(2.0*eV*self.dTi/self.atmass*amu)
return, fwhmg
END

;+
;NAME:
;setAtMass
;-
PRO ffs_doppler::setAtMass, atmass

END
;+
;NAME:
;setLambda0
;-
PRO ffs_doppler::setLambda0, lambda0
IF N_ELEMENTS(dispersion) EQ 0 THEN self.lambda0 = 0.0D
END
;+
;NAME:
;setDispersion
;-
PRO ffs_doppler::setDispersion, dispersion
IF N_ELEMENTS(dispersion) EQ 0 THEN self.dispersion = 1.0D

END

;+
;NAME:
;init
;-
FUNCTION ffs_doppler::init, $
  lambda0 = lambda0, $
  atmass = atmass, $
  dispersion = dispersion, $
  vshift = vshift, $
  dti = dti, $
  AREA = area, $
  PARS = pars, $
  ;;TRAP = trap, $
  _Extra = extra

  If Self -> ffs_element::Init(_Extra = extra) NE 1 Then Return, 0
  ;; Error Handler
  Catch, theError
  IF theError NE 0 THEN BEGIN
     self -> ErrorMessage, 'Error initialising object'
     RETURN, 0
  ENDIF

  ;;self -> addPar, PARNAME = 'lambda0', PARTYPE = 'pos'
  ;;self -> addPar, PARNAME = 'atmass', PARTYPE = 'mass'
  self -> addPar, PARNAME = 'dti', PARTYPE = 'temperature'
  self -> addPar, PARNAME = 'vshift', PARTYPE = 'velocity'
  self -> addPar, PARNAME = 'area', PARTYPE = 'area'

  self -> setAtMass, atmass
  self -> setLambda0, lambda0
  self -> setDispersion, dispersion

  IF N_ELEMENTS(dti) GT 0 THEN self -> setParVals, dti, parname = 'dti'
  IF N_ELEMENTS(vshift) GT 0 THEN self -> setParVals, vshift, parname = 'vshift'
  IF N_ELEMENTS(area) GT 0 THEN self -> setParVals, area, parname = 'area'
  IF N_ELEMENTS(pars) EQ 3 AND SIZE(pars, /tname) NE 'STRING' THEN self -> setParVals, pars
  ;;IF N_ELEMENTS(trap) GT 0 THEN self.trap = trap ELSE self.trap =5.0D

  ;; Create the gaussian needed
  fwhmg = self -> translateToFwhmg()
  ctr = self -> translateToCtr()
  self.gaussian = OBJ_NEW('ffs_gaussian', ctr = ctr, fwhmg = fwhmg, area = area, _EXTRA = e)

  self -> setRule, 'ffs_doppler_evaluate'
  self -> setType, 'ffs_doppler'

  RETURN, 1
END




PRO ffs_doppler__define
    self = {ffs_doppler, $
            lambda0:0.0D, $
            atmass:0.0D, $
            dispersion:0.0D, $
            gaussian:OBJ_NEW(), $
         inherits ffs_element}
END
