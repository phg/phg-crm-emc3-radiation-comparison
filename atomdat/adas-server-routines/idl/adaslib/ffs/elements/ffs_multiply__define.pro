;+
; PROJECT:
;   FFS - Framework for Feature Synthesis (Object oriented model/fitting).
;
; NAME:
;   FFS_MULTIPLY
;
; PURPOSE:
;   An FFS operator element - takes output of other FFS elements and multiplies them
;   by scalar parameter 'factor'.
;
; EXPLANATION:
;   Performs multiplication operation on other FFS element results. Each of the intensity
;   values is multiplied by the scalar 'factor'.  
;
; USE:
;   Example of stand alone use to follow at later date. Currently only used 
;   indirectly by FFS system parser.
;
; INITIALISATION SYNTAX:
;   mult_obj = obj_new('ffs_multiply', factor=factor, debug=debug)
;   PARAMETERS:
;     unusedParserInput -   input required by the ffs_parser, but unused by this element;
;   KEYWORDS:
;     factor 	-   scalar value to multiply operand intensities by.
;     debug 	-   set this keyword to enable debug information to be printed to the terminal.
;
; PUBLIC ROUTINES:
;   In addtion to the methods listed below, this object inherits methods from ffs_element
;   - refer to this class' documentation for more details.
;
;   [calculate]
;     PURPOSE:
;	Performs FFS multiply operation on operand elements.
;     INPUTS:
;	in   -  An array of other FFS element evaluation output structures i.e. a
;           	structure with the fields:
;   	    	  wavelength  	-   double array of the wavelength grid.	 
;	    	  intensity     -   double array of profile intensity values.
;	    	  gridded     	-   0 or 1 to signify if intensities mapped to 
;   	    	    	    	    wavelength grid.	
;     OUTPUTS:
;	Returns a structure with the following fields:
;	wavelength  -	double array of the wavelength grid.	 
;	intensity   -	double array of profile intensity values.
;	gridded     -	0 or 1 to signify if intensities mapped to 
;   	    	    	wavelength grid.
;     SIDE EFFECTS:
;	None.
;
; AUTHOR:
;   Christopher Nicholas, University of Strathclyde
;
; VERSION HISTORY:
;
;   1.1 CHN 24/10/2007
;   	* Initial commit
;   1.2 ADW 13/11/2007
;   	* Changed evaluate function name to calculate and made it return a
;    	  results structure rather storing it and returning a getresults.
;   1.3 CHN 05/12/2007
;   	* Altered all calls to 'setparvals' to reflect update of this method
;   	  from procedure to function as part of error handling strategy.
;   1.4 CHN 08/01/2008
;   	* Added debug keyword to init to pass thorugh to superclass.
;   	* Re-write of documentation.
;   	* Removed obsolete partype keyword from addpar calls. 
;   	* Removed setting of element type in init.
;   1.5 CHN 09/01/2008
;   	* Corrected passing of 'debug' to ffs_element::init as a 
;   	  parameter instead of a keyword as required.
;   1.6 CHN 24/01/2008
;       * Corrected previous mod entry dates 2007->2008!
;       * changed addpar calls in init to reflect changes to ffs_element.
;   1.7 CHN 19/01/2008
;       * Set 'maxchildren' in init.
;       * 'calculate' method adjusted to only use element 0 of 'in'
;   1.8 CHN 25/02/2008
;       * Set 'minchildren' in init.
;       * Added error message to calculate method for case of no input.
;   1.9 CHN 10/11/2008
;       * Added method 'calcpd'.
;----------------------------------------------------------------------
;
; First version distributed with ADAS.
;
; MODIFIED   :
;       1.1     Martin O'Mullane
;                 - First central ADAS version.
;
; VERSION    :
;       1.1    04-01-2013
;
;----------------------------------------------------------------------
;-

function ffs_multiply::calcpd, ref
    pars = self->getpars(/fitting)
    mult = pars[0]->getvalue()
    children=self->getchildren(count=count)
    if ref eq pars[0] then return, (*(children[0]->getresult())).intensity
    if count ne 1 then stop
    if count ne 1 then return, $
        self->seterrmsg('Multiply element must have one child element only')

    res=fltarr(n_elements(self->getxdata()))
    return, mult*children[0]->getpd(ref, num=num, relstep=relstep)
end

FUNCTION ffs_multiply::calculate,in
    if n_elements(in) eq 0 then return, $
        self->seterrmsg('No operand elements for operator element to work on')
    gridded=(*in[0]).gridded
    factor = (self->getParVals(/fitting))[0]
    wavelength=(*in[0]).wavelength
    intensity=fltarr(n_elements(wavelength))
    return, {wavelength:wavelength, $
      intensity:(*in[0]).intensity * factor,gridded:gridded}
END


FUNCTION ffs_multiply::init,$
    unusedParserInput, $
    FACTOR=factor, $
    debug=debug

    If Self->ffs_element::Init(debug=debug) NE 1 Then Return, 0
    
    if self->addPar(PARNAME='factor') eq 0 then return, 0
    IF N_ELEMENTS(factor) GT 0 THEN begin
      if self->setParVals(factor, parname='factor') eq 0 then return, 0
    endif ELSE if self->setParVals(1.0, parname='factor') eq 0 then return, 0
    self.minchildren = 1
    self.maxchildren = 1
    RETURN, 1
END


PRO ffs_multiply__define
    self = {ffs_multiply, $
         inherits ffs_element}
END

