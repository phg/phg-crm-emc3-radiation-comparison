;+
; PROJECT:
;   FFS - Framework for Feature Synthesis (Object oriented model/fitting).
;
; NAME:
;   FFS_LINE
;
; PURPOSE:
;   An FFS element - evaluates a simple 'un-broadened' line profile (delta function).
;
; EXPLANATION:
;   Provides evaluation of a gaussian line profile dependant upon the input parameters. 
;   The resultant profile is utilised (commonly in conjuction with many other FFS elements) 
;   in building model spectra with the FFS system. It is necessary to map this element to a
;   wavelength grid using ffs_broaden before plotting the model. 
;  
; USE:
;   An example of creating a line at wavelength 100.0, with intensity of 300.0:
;     line = obj_new('ffs_line', pos=100.0, intensity=300.0)
;   or alternatively:
;     line = obj_new('ffs_line')
;     isok = line->setparvals('pos', 100.0)
;     isok = line->setparvals('intensity', 300.0)
;
; INITIALISATION SYNTAX:
;   line_obj = obj_new('ffs_line', pos=pos, intensity=intensity, debug=debug)
;   PARAMETERS:
;     unusedParserInput -   input required by the ffs_parser, but unused by this element;
;   KEYWORDS:
;     pos   	    -   specifies the position of the line in terms of wavelength.
;     intensity     -   specifies the height of the line.
;     debug 	    - 	set this keyword to enable debug information to be printed to the terminal.
;
; PUBLIC ROUTINES:
;   In addtion to the methods listed below, this object inherits methods from ffs_element
;   - refer to this class' documentation for more details.
;
;   [calculate]
;     PURPOSE:
;	Performs calculation of the profile.
;     INPUTS:
;	in   -  input required by the FFS system, but unused by this element.		
;     OUTPUTS:
;	Returns a structure with the following fields:
;	wavelength  -  double array of the wavelength grid.	 
;	intensity   -  double array of profile intensity values.
;	gridded     -	0 or 1 to signify if on a wavelength grid - 0 in this case.
;     SIDE EFFECTS:
;	None.
;
; AUTHOR:
;   Christopher Nicholas, University of Strathclyde
;
; VERSION HISTORY:
;   1.1  CHN 10/08/2007
;   	 * Initial commit to CVS.
;   1.2  CHN 13/08/2007
;   	 * modified evaluate method to save result before returning value
;   1.3  CHN 11/09/2007
;   	 * removed usage of obsolete setRule
;   	 * updated to support ffs_prop objects
;   1.4  ADW 13/11/2007
;   	 * Changed evaluate function name to calculate and made it return a
;   	   results structure rather storing it and returning a getresults.
;   1.5  CHN 05/12/2007
;   	 * Altered all calls to 'setparvals' to reflect update of this method
;    	   from procedure to function as part of error handling strategy.
;   1.6  CHN 07/12/2007
;   	 * Added setting of appropriate hard / soft limits on pars in init.
;   1.7  CHN 08/01/2008
;   	 * Added debug keyword to init to pass thorugh to superclass.
;   	 * Re-write of documentation.
;   	 * Removed obsolete partype keyword from addpar calls.
;   	 * Removed setting of element type.
;   1.8  CHN 24/01/2008
;        * Altered documentation to allow for 4 character version number.
;        * changed addpar calls in init to reflect changes to ffs_element.
;----------------------------------------------------------------------
;
; First version distributed with ADAS.
;
; MODIFIED   :
;       1.1     Martin O'Mullane
;                 - First central ADAS version.
;
; VERSION    :
;       1.1    04-01-2013
;
;----------------------------------------------------------------------
;-
FUNCTION ffs_line::calculate, in
  parVals = self->getParVals(/FITTING)
  pos = parVals[0]
  intensity = parVals[1]  
  return,{wavelength:pos,intensity:intensity, gridded:0}
END  
  
FUNCTION ffs_line::init,$
    unusedParserInput, $
    POS=pos, $
    INTENSITY=intensity, $
    debug=debug

    If Self->ffs_element::Init(debug=debug) NE 1 Then Return, 0

    if self->addPar(PARNAME='pos') eq 0 then return, 0
    if self->addPar(PARNAME='intensity') eq 0 then return, 0

    if self->setparlimits(PARNAME='intensity', [0.0d,!values.f_infinity]) eq 0 then return, 0

    IF N_ELEMENTS(pos) GT 0 THEN if self->setParVals(pos, parname='pos') eq 0 then return, 0
    IF N_ELEMENTS(intensity) GT 0 THEN if self->setParVals(intensity, parname='intensity') eq 0 then return, 0
   
    RETURN, 1
END


PRO ffs_line__define
    self = {ffs_line, $
         inherits ffs_element}
END

