;+
; PROJECT:
;   FFS - Framework for Feature Synthesis (Object oriented model/fitting).
;
; NAME:
;   FFS_SHIFT
;
; PURPOSE:
;   An FFS operator element -  performs wavelength/pixel shift operand element results.
;
; EXPLANATION:
;   Performs shift operation on other FFS element results. The shift can be
;   specified in terms of pixel or wavelength.
;  
; USE:
;   Example of stand alone use to follow at later date. Currently only used 
;   indirectly by FFS system parser.
;
; INITIALISATION SYNTAX:
;   shift_obj = obj_new('ffs_shift', shifttype, debug=debug)
;   PARAMETERS:
;     shifttype - a string value that specifies whether shift is in terms of wavelength or pixels.   
;   KEYWORDS:
;     debug - 	set this keyword to enable debug information to be printed to the terminal.
;
; PUBLIC ROUTINES:
;   In addtion to the methods listed below, this object inherits methods from ffs_element
;   - refer to this class' documentation for more details.
;
;   [calculate]
;     PURPOSE:
;	Performs FFS shift operation on operand elements.
;     INPUTS:
;	in   -  An FFS element evaluation output structures i.e. a
;           	structure with the fields:
;   	    	  wavelength  	-   double array of the wavelength grid.	 
;	    	  intensity     -   double array of profile intensity values.
;	    	  gridded     	-   0 or 1 to signify if intensities mapped to 
;   	    	    	    	    wavelength grid.	
;     OUTPUTS:
;	Returns a structure with the following fields:
;	wavelength  -	double array of the wavelength grid.	 
;	intensity   -	double array of profile intensity values.
;	gridded     -	0 or 1 to signify if intensities mapped to 
;   	    	    	wavelength grid. In this case always 1.
;     SIDE EFFECTS:
;	None.
;
; AUTHOR:
;   Allan Whiteford, University of Strathclyde
;
; VERSION HISTORY:
;   1.1 ADW 22/10/2007
;    	* Initial commit to CVS
;   1.2 ADW 13/11/2007
;    	* Changed evaluate function name to calculate and made it return a
;   	  results structure rather storing it and returning a getresults.
;   1.3 CHN 05/12/2007
;   	* Altered all calls to 'setparvals' to reflect update of this method
;   	  from procedure to function as part of error handling strategy.
;   1.4 CHN 08/01/2008
;   	* Added debug keyword to init to pass thorugh to superclass.
;   	* Removed unused 'lambda' and 'pixel' keywords from init.
;   	* Added documentation block.   	
;   	* Removed obsolete partype keyword from addpar calls.
;   	* Removed setting of element type in init.
;   1.5 CHN 19/01/2008
;       * Set 'maxchildren' in init.
;   1.6 CHN 10/11/2008
;       * 'addpar' calls changed to functions rather than procedures.
;   1.7 CHN 22/03/2011
;       * Modified 'calculate':
;         - attempt to cope with elements which evaluate beyond model wavegrid
;           i.e. not a small component within the grid. See inline comments.
;           Also note that this update takes a sledgehammer approach and
;          recalculated all child elements regardless - this has decreased
;          performance and so, must be rectified.
;            
;----------------------------------------------------------------------
;
; First version distributed with ADAS.
;
; MODIFIED   :
;       1.1     Martin O'Mullane
;                 - First central ADAS version.
;
; VERSION    :
;       1.1    04-01-2013
;
;----------------------------------------------------------------------
;-

FUNCTION ffs_shift::calculate, in
  
  x = self -> getXdata()
  parVals = self->getParVals(/FITTING)

  if self.shifttype eq 'pixel' then begin
	  pixel = parVals[0]
          startpix=0-pixel > 0
          endpix=n_elements(x)-pixel-1 < n_elements(x)-1
          intens=(*in[0]).intensity[startpix:endpix]
          if startpix eq 0 then intens=[fltarr(pixel)+intens[0],intens]
          if endpix eq n_elements(x)-1 then intens=[intens,fltarr(pixel)+intens[n_elements(intens)-1]]
          output =  {wavelength:x,intensity:intens, gridded:1}  
  endif
  
  ; There are issues here with elements* that produce spectra beyond wavebounds
  ; of the model. They cannot simply have their x-values changed as for elements
  ; which evaluate to a region smaller than the model grid.
  ; * in fact it is worse because you must deal with child elements that have
  ; this problem. CHN
  if self.shifttype eq 'lambda' then begin
          lambda = parVals[0]
;          if (*in[0]).gridded eq 1 then begin
;            old = *in[0]
;            child = self->getchildren()
;              if child->setxdata(x-lambda) eq 0 then return, $
;                self->seterrmsg('failed to set child xdata for shift'+ $
;                    child->geterrmsg(), retval=-1)
;              newin = (self->getchildren())->evaluate()
;              output={wavelength:x, intensity:(*newin).intensity, gridded:1}
;          endif else begin
          output =  {wavelength:(*in[0]).wavelength+lambda,intensity:(*in[0]).intensity, gridded:0}  
;          endelse
  endif
  
  return,output
END  
  
FUNCTION ffs_shift::init,$
    shifttype, $
    debug=debug
  
    If Self->ffs_element::Init(debug=debug) NE 1 Then Return, 0

    if shifttype eq '' then shifttype='lambda'
    self.shifttype=shifttype

    if shifttype eq 'pixel' then begin
    	if self->addPar(PARNAME='pixel') eq 0 then return, 0
    	IF N_ELEMENTS(pixel) GT 0 THEN if self->setParVals(pixel, parname='pixel') eq 0 then return, 0
    endif
      
    if shifttype eq 'lambda' then begin
    	if self->addPar(PARNAME='lambda') eq 0 then return, 0
    	IF N_ELEMENTS(lambda) GT 0 THEN if self->setParVals(lambda, parname='lambda') eq 0 then return, 0
    endif
    
    self.maxchildren = 1
   
    RETURN, 1
END

PRO ffs_shift__define
    self = {	ffs_shift, $
                shifttype:'', $
                inherits ffs_element}
END

