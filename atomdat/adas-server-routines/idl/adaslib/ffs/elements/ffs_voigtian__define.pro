;+
; PROJECT:
;   FFS - Framework for Feature Synthesis (Object oriented model/fitting).
;
; NAME:
;   FFS_VOIGTIAN
;
; PURPOSE:
;   An FFS element - evaluates a voigtian profile.
;
; EXPLANATION:
;   Provides evaluation of a voigt line profile dependant upon the input parameters. 
;   The resultant profile is utilised (commonly in conjuction with many other FFS elements) 
;   in building model spectra with the FFS system.
;  
; USE:
;   An example of creating a voigtian profile at central wavelength 100.0; 
;   is a convolution of a gaussian with full width at half maximum intensity
;   of 25.0 with a lorentzian (fwhm 40.0) and has an area 250.0:
;     vt = obj_new('ffs_voigtian', pos=100.0, fwhmg=25.0, fwhml=40.0, area=250.0)
;   or alternatively:
;     vt = obj_new('ffs_voigtian')
;     isok = vt->setparvals('pos', 100.0)
;     isok = vt->setparvals('fwhmg', 25.0)
;     isok = vt->setparvals('fwhml', 40.0)
;     isok = vt->setparvals('area', 250.0)
;
; INITIALISATION SYNTAX:
;   voigt_obj = obj_new('ffs_voigtian',pos=pos, fwhm=fwhm, area=area, pars=pars, trap=trap)
;   PARAMETERS:
;     unusedParserInput  -   input required by the ffs_parser, but unused by this element
;   KEYWORDS:
;     pos   -   central wavelength position of gaussian line profile
;     fwhmg -   full width at half maximum value for gaussian component
;     fwhml -   full width at half maximum value for lorentzian component
;     area  -   profile area (intensity)
;     pars  -   input of pos, fwhmg, fwhml and area can be passed as a 'pars' array
;     trap  -   parameter at which evaluation truncated [abs(x-x0) = trap * fwhm]
;     debug -	set this keyword to enable debug information to be printed to the terminal.
;
; PUBLIC ROUTINES:
;   In addtion to the methods listed below, this object inherits methods from ffs_element
;   - refer to this class' documentation for more details.
;
;   [calculate]
;     PURPOSE:
;	Performs calculation of the profile.
;     INPUTS:
;	in   -  input required by the ffs system, but unused by this element.		
;     OUTPUTS:
;	Returns a structure with the following fields:
;	wavelength  -  double array of the wavelength grid.	 
;	intensity   -  double array of profile intensity values.
;	gridded     -	0 or 1 to signify if on a wavelength grid - 1 in this case.
;     SIDE EFFECTS:
;	Calls agm_humlicek.
; AUTHOR:
;   Christopher Nicholas, University of Strathclyde
;   (Based on Andrew Meigs' GLV voigtian)
;
; VERSION HISTORY:
;   1.1  ADW 10/08/2007
;   	 * Initial commit to CVS.
;   1.2  CHN 14/08/2007
;   	 * Complete overhaul of this class. Utilises the main parts of the old 
;   	   voigtian class' evaluate, but otherwise altered significantly to fit
;   	   into the new ffs_model_parser evaluation step. This class was based
;   	   upon ffs_gaussian.
;   1.3  CHN 11/09/2007
;   	 * removed usage of obsolete setRule
;   	 * updated to support ffs_prop objects
;   1.4  CHN 16/10/2007
;   	 * removed print statement at the beginning of the evaluate method
;   	   saying '#featuretype# evaluate'.
;   1.5  CHN 22/10/2007
;   	 * changed 'fwhmg' keyword and par created to 'fwhm'
;   	 * removed extraneous comments and old commented out lines
;   	 * removed 'extra' at init
;   	 * inserted documentation
;   1.6  ADW 13/11/2007
;   	 * Changed evaluate function name to calculate and made it return a
;    	   results structure rather storing it and returning a getresults.
;   1.7  CHN 05/12/2007
;   	 * Altered all calls to 'setparvals' to reflect update of this method
;    	   from procedure to function as part of error handling strategy.
;   	 * Altered keywords and references to 'ctr' -> 'pos' for consistency with
;    	   'pos' used in ffs_line.
;   1.8  CHN 07/12/2007
;   	 * Added setting of appropriate hard / soft limits on pars in init.
;   1.9  CHN 08/01/2008
;   	 * Added debug keyword to init to pass thorugh to superclass.
;   	 * Re-write of documentation.
;   	 * Removed obsolete partype keyword from addpar calls.
;   1.10 CHN 24/01/2008
;        * Altered documentation to allow for 4 character version number.
;        * changed addpar calls in init to reflect changes to ffs_element.
;        * changed addprop calls in init to reflect changes to ffs_element.
;   1.11 CHN 21/05/2009
;        * Added in 'calcpd' method i.e. included analytic partial derivatives.
;        * Fixed a typo in 'init' that meant hard limits for 'fwhmg' were
;          were not being set as intended.
;----------------------------------------------------------------------
;
; First version distributed with ADAS.
;
; MODIFIED   :
;       1.1     Martin O'Mullane
;                 - First central ADAS version.
;
; VERSION    :
;       1.1    04-01-2013
;
;----------------------------------------------------------------------
;-
function ffs_voigtian::calcpd, ref
    pars = self -> getPars()
    if ((paridx = where(pars eq ref[0])))[0] eq -1 then stop  
    c = sqrt(4.0d*alog(2.0d)) 
    
    x0 = pars[0]->getValue()
    fl = pars[1]->getValue()
    fg = pars[2]->getValue()
    area = pars[3]->getValue()
    trap = pars[4]->getValue()    
    result = *(self->getresult())
    k = double(*(self.cmplxerrfcn))
    l = imaginary(*(self.cmplxerrfcn))
    x = result.wavelength
    a = c * (x-x0)/fg
    b = c*fl / (2.0d0*fg)
    
    if paridx eq 0 then return, -1.0d0*c^2*area / (sqrt(!DPI)*fg^2) * 2.0d0*(l*b-k*a)
    if paridx eq 1 then return, c^2*area/(sqrt(!dpi)*fg^2)*(k*b+l*a-1.0d0/(sqrt(!dpi)))
    if paridx eq 2 then return, -c*area/(sqrt(!dpi)*fg^2)*k - 2.0d0*c^2*area/(sqrt(!dpi)*fg^3)*((l*b-k*a)*(x-x0)+(k*b+l*a-1.0d0/sqrt(!dpi))*(fl/2.0d0))
    if paridx eq 3 then return, (1.0d0/area) * result.intensity
    
end

FUNCTION ffs_voigtian::calculate, in

  x = self -> getXdata()      
  pars = self->getPars()
  x0 = pars[0]->getValue()
  fl = pars[1]->getValue()
  fg = pars[2]->getValue()
  area = pars[3]->getValue()
  trap = pars[4]->getValue()
                   ;
  ; Set up a trap variable to stop calculation of the function beyond abs(x-x0) = trap * full width
  IF N_ELEMENTS(trap) EQ 0 THEN trap = 80.0D ELSE TRAP=Trap ;; setup the under flow parameter

  fullwidth = fl > fg
  IF fg ne 0.0D THEN begin
     alpha = 2.0D*sqrt(Alog(2.0D))/fg 
  ENDIF ELSE alpha = 1.0D
  
  aa = alpha*(x-x0);;alpha*xprime
  bb = alpha*(fl/2.0D);;alpha*beta
  ; Call the Humlicek routine for the complex probability function
  if ptr_valid(self.cmplxerrfcn) then ptr_free, self.cmplxerrfcn
  self.cmplxerrfcn = ptr_new((cmplxerrfcn = agm_humlicek(aa,bb)))
  k = DOUBLE(*(self.cmplxerrfcn)) ; real part of the complex probability function (the scaled voigt) 
  result =  area*(alpha/sqrt(!DPI))*k*(abs(x-x0) le trap * fullwidth)
  return,{wavelength:x, intensity:result, gridded:1}
END

pro ffs_voigtian::cleanup
    if ptr_valid(self.cmplxerrfcn) then ptr_free, self.cmplxerrfcn
    self->ffs_element::cleanup
end

FUNCTION ffs_voigtian::init,$
    unusedParserInput, $
    pos=pos, $
    FWHML=fwhml, $
    FWHMG=fwhmg, $
    AREA=area, $
    PARS=pars, $
    TRAP=trap, $
    debug=debug

    If Self->ffs_element::Init(debug=debug) NE 1 Then Return, 0

    if self->addPar(PARNAME='pos') eq 0 then return, 0
    if self->addPar(PARNAME='fwhml') eq 0 then return, 0
    if self->addPar(PARNAME='fwhmg') eq 0 then return, 0
    if self->addPar(PARNAME='area') eq 0 then return, 0
    if self->addProp(NAME='trap') eq 0 then return, 0

    if self->setparhardlimits(PARNAME='fwhml', [0.0d,!values.f_infinity]) eq 0 then return, 0    
    if self->setparhardlimits(PARNAME='fwhmg', [0.0d,!values.f_infinity]) eq 0 then return, 0    
    if self->setparlimits(PARNAME='area', [0.0d,!values.f_infinity]) eq 0 then return, 0

    IF N_ELEMENTS(pos) GT 0 THEN if self->setParVals(pos,parname='pos') eq 0 then return, 0
    IF N_ELEMENTS(fwhml) GT 0 THEN if self->setParVals(fwhml,parname='fwhml') eq 0 then return, 0
    IF N_ELEMENTS(fwhmg) GT 0 THEN if self->setParVals(fwhmg,parname='fwhmg') eq 0 then return, 0
    IF N_ELEMENTS(area) GT 0 THEN if self->setParVals(area,parname='area') eq 0 then return, 0
    IF N_ELEMENTS(pars) EQ 3 AND SIZE(pars,/tname) NE 'STRING' THEN if self->setParVals(pars) eq 0 then return, 0
    IF N_ELEMENTS(trap) GT 0 THEN BEGIN
      if self->setParVals(trap, name='trap')eq 0 then return, 0
    ENDIF ELSE if self->setParVals(80.0d, name='trap') eq 0 then return, 0
    
    RETURN, 1
END


PRO ffs_voigtian__define
    self = {ffs_voigtian, $
            cmplxerrfcn:ptr_new(), $
            inherits ffs_element}
END

