;+
; PROJECT:
;   FFS - Framework for Feature Synthesis (Object oriented model/fitting).
;
; NAME:
;   FFS_ADAS
;
; PURPOSE:
;   An FFS element - provides access to ADAS features, utilising AFG.
;
; EXPLANATION:
;   The ADAS Feature Generation (AFG) API provides access to the various
;   special feature models available from ADAS. This class acts as an
;   access point for the FFS system to utilise these as FFS elements
;   within an FFS model.
;
; USE:
;   As an example, to create an element utilising the 'balmer_archived' feature
;   from AFG and set the filename, temperature and density parameter values:
;   	adas_obj = obj_new('ffs_adas', 'balmer_archived')
;   	isok = adas_obj->setparvals(name='filename', 'adf36.dat')
;   	isok = adas_obj->setparvals(name='te', 2.0d4)
;   	isok = adas_obj->setparvals(name='dens', '1.0d13)
;
; INITIALISATION SYNTAX:
;   gaussian_obj = obj_new('ffs_adas', afgtype, adasobj=adasobj)
;   PARAMETERS:
;     afgType	-   string dictating adas (AFG) feature type
;   KEYWORDS:
;     adasobj   -	object reference of existing adas (AFG) object
;     debug 	- 	set this keyword to enable debug information to be printed to the terminal.
;
; PUBLIC ROUTINES:
;   In addtion to the methods listed below, this object inherits methods from ffs_element
;   - refer to this class' documentation for more details.
;
;   [calculate]
;     PURPOSE:
;	Performs calculation of the profile (sets up and executes AFG evaluation).
;     INPUTS:
;	in   -  input required by the ffs system, but unused by this element.		
;     OUTPUTS:
;	Returns a structure with the following fields:
;	wavelength   -  double array of the wavelength grid.	 
;	intensity    -  double array of profile intensity values.
;	gridded     -	0 or 1 to signify if modelled on a wavelength grid.
;     SIDE EFFECTS:
;	None.
; AUTHOR:
;   Christopher Nicholas, University of Strathclyde
;
; MODIFICATION HISTORY:
;   1.1 ADW 10/08/2007
;   	* Initial commit to CVS
;   1.2 CHN 11/09/2007
;   	* removed usage of obsolete setRule
;   	* updated to support ffs_prop objects
;   1.3 CHN 16/10/2007
;   	* now interpolates 'gridded' adas features onto specified wavelength interval.
;   1.4 CHN 16/10/2007
;   	* removed print statement at the beginning of the evaluate method
;   	  saying '#featuretype# evaluate'.
;   1.5 CHN 19/10/2007
;   	* added support for ffs hard limits - values taken from sff description structure.
;   	  fixed interpolation bug.
;   1.6 CHN 22/10/2007
;   	* altered interpolation such that occurences of negative intensity are 'zeroed'
;   	* fixed case when self.wvresolved was 0, tried to return interwv, which was not
;    	  set.
;   	* removed 'extra keyword'
;   	* inserted documentation
;   1.7 CHN 22/10/2007
;   	* ammended small error introduced by last update
;   1.8 CHN 24/10/2007
;   	* added new setParVals method (over-rides ffs_element::setParVals)
;   	  this method is present to ensure afg refreshes description structure
;    	  if one of the parameters is a filename. This is important for
;    	  establishing limits on other pars.This method still calls the superclass
;    	  implementation at the end.
;   	* removed hardLimit setting in init method as this is taken care of
;    	  by the new setParVals method.
;   1.9 CHN 12/11/2007
;   	* all references to sff changed to new afg - esp. obj_new statements.
;   	* afg has new implementation of setpars - this allows testing of
;    	  any changes to limits.
;   	* added 'refreshLimits' method
;   	* removed setParVals method added in last update - limit changes
;    	  handled by afg now (via new afg_api::setpars return value)
;   1.10 ADW 13/11/2007
;   	* Changed evaluate function name to calculate and made it return
;    	  a results structure rather than a pointer to one.
;   1.11 CHN 05/12/2007
;   	* Altered all calls to 'setparvals' to reflect update of this method
;    	  from procedure to function as part of error handling strategy.
;   1.12 CHN 07/12/2007
;   	* Made refreshlimits convert values to type double before passing to 
;   	  sethardlimits
;   1.13 CHN 08/01/2008
;   	* Added debug keyword to init to pass thorugh to superclass.
;   	* Re-write of documentation.
;   	* Removed obsolete partype keyword from addpar calls.
;   1.14 CHN 09/01/2008
;   	* Corrected passing of 'debug' to ffs_element::init as a 
;   	  parameter instead of a keyword as required.
;   1.15 CHN 23/01/2008
;        * changed addpar calls in init to reflect changes to ffs_element.
;        * changed addprop calls in init to reflect changes to ffs_element.
;   1.16 ADW 13/03/2008
;        * Stored feature type via call to setsubtype.
;   1.17 CHN 11/10/2008
;        * Removed 'stop' in 'calculate' method.
;        * 4-space character indentation throughout.
;        * Utilised error message setting in 'refreshlimits'.
;        * Re-formatted various for & if statements.
;   1.18 CHN 22/03/2011
;        * Added 'setparvals' over-ride method.
;        * Modified 'calculate':
;          - catch for pointer fields of AFG parstructure.
;        * Modified 'init':
;          - added error message.
;          - catch for pointer fields of AFG parstructure.
;----------------------------------------------------------------------
;
; First version distributed with ADAS.
;
; MODIFIED   :
;       1.1     Martin O'Mullane
;                 - First central ADAS version.
;
; VERSION    :
;       1.1    04-01-2013
;
;----------------------------------------------------------------------
;-


function ffs_adas::setparvals, $
    parvals, $
    name = name, $
    parname=parname, $
    position=position, $
    static=static, $
    fitting=fitting, $
    free=free, $
    fixed=fixed, $
    fast=fast
print, "######"
print, parvals
print, "######"
    if self->ffs_element::setparvals(parvals, $
        name = name, $
        parname=parname, $
        position=position, $
        static=static, $
        fitting=fitting, $
        free=free, $
        fixed=fixed, $
        fast=fast) eq 0 then return, 0
        
    if not keyword_set(fast) then begin
        ;get parameter structure from adas umbrella (afg)
        parStructure = self.adasObj->getPars()
         ; Only deal with 'alterlimits' statics first (to establish limits)
         ; then copy these prop (par) values into the structure
        prop = self->getpars(/static, count=count)
        names = self->getparnames(/static)
        FOR i=0, count-1 DO begin
            ;if ptr_valid((parstructure.(where(tag_names(parstructure) eq strupcase(names[i]))))) then $
                ;ptr_free, (parstructure.(where(tag_names(parstructure) eq strupcase(names[i]))))
            if size(parstructure.(i), /type) eq 10 then $
                *(parstructure.(where(tag_names(parstructure) eq strupcase(names[i])))) = (prop[i])->getvalue() else parstructure.(i) = (prop[i])->getvalue()
        endfor
        ;set adas (afg) object to have this structure
        afgStatus = self.adasObj->setPars(PARS=parStructure)
        
        IF afgStatus EQ 2 THEN BEGIN
            fitParValsBefore = self->getParVals(/FITTING)
            IF self->refreshLimits() EQ 0 THEN RETURN, 0
            fitParValsAfter = self->getParVals(/FITTING)
            IF (WHERE(fitParValsBefore NE fitParValsAfter))[0] NE -1 THEN $
                PRINT, 'new limits were set and altered input par vals'
        ENDIF
    endif
    
    return, 1
end


FUNCTION ffs_adas::calculate, in
    ;get parameter structure from adas umbrella (afg)
    parStructure = self.adasObj->getPars()
  
    ;get pars from ffs
    pars = self->getPars()
      
    ;copy par values into the structure
    FOR i=0, N_TAGS(parStructure)-1 DO begin
        if size(parstructure.(i), /type) eq 10 then $
            *(parstructure.(i)) = (pars[i])->getvalue() $
            else parstructure.(i) = (pars[i])->getvalue()
    endfor
    ;set adas (afg) object to have this structure
    afgStatus = self.adasObj->setPars(PARS=parStructure)
  
    IF afgStatus EQ 2 THEN BEGIN
        fitParValsBefore = self->getParVals(/FITTING)
        IF self->refreshLimits() EQ 0 THEN RETURN, 0
        fitParValsAfter = self->getParVals(/FITTING)
        IF (WHERE(fitParValsBefore NE fitParValsAfter))[0] NE -1 THEN $
            PRINT, 'new limits were set and altered input par vals'
    ENDIF
  
;    desc=self.adasobj->getdesc()
;    print, 'par limits from afg:'
;    print, desc.parameters.te.min, desc.parameters.te.max
;    print, 'par limits from ffs:'
;    print, pars[1]->getlimits()
    ;execute calculation for that feature
    result =  self.adasObj->getCalc()

    IF self.adasObj->getwvresolved() EQ 1 THEN BEGIN
        x = self->getXData() ;should error check this
        interRes = INTERPOL(REFORM(result.intensity), result.wv/10.0, x)
        ;zero elements that are negative:
        negInd = where(interRes LT 0.0)
        IF negInd[0] NE -1 THEN interRes[negInd] = 0.0
        interWv = x
    ENDIF ELSE BEGIN
        interRes = result.intensity
        interWv = result.wv/10.0
    ENDELSE  
    RETURN, {WAVELENGTH:interWv, INTENSITY:interRes, GRIDDED:self.adasObj->getwvresolved()}
END


FUNCTION ffs_adas::refreshLimits

    desc = self.adasObj->getDesc()
    ;get all pars
    pars = self->getPars(/FITTING, count=count)
    idx = -1
    FOR i=0, N_TAGS(desc.parameters)-1 DO $
        IF desc.parameters.(i).disptype EQ 'continuous' THEN idx = [idx, i]
    idx = idx[1:*]

    FOR i=0, count-1 DO $
        if ((pars[i])->setHardLimits([double(desc.parameters.(idx[i]).min), $
            double(desc.parameters.(idx[i]).max)])) eq 0 then $
            return, self->seterrmsg('failed to set hard limits - '+pars[i]->geterrmsg()) 
    RETURN, 1
END


;+
;NAME:
;cleanup
;This method frees all object values and destroys all objects stored
;in the container. This is already handled in cn_container::cleanup, so
;cleanup only needs to perform an obj_destroy on the parcontainer. Also
;makes the call to the superclass cn_primitive's cleanup.
;-
PRO ffs_adas::cleanup
    IF OBJ_VALID(self.adasObj) THEN OBJ_DESTROY, self.adasObj
    self->ffs_element::cleanup
END


FUNCTION ffs_adas::init,$
    afgType, $
    ADASOBJ=adasObj, $
    debug=debug

    IF self->ffs_element::Init(debug=debug) NE 1 THEN Return, 0
    
    IF N_ELEMENTS(adasObj) GT 0 THEN BEGIN
      IF OBJ_VALID(adasObj) EQ 1 THEN self.adasObj = adasobj ELSE $ 
        IF STRLOWCASE(SIZE(adasObj, /TNAME)) EQ 'string' THEN self.adasObj = OBJ_NEW('afg_'+adasObj) $
        ELSE RETURN, self->seterrmsg('failed to initialise - unable to create AFG object')
    ENDIF

    IF N_ELEMENTS(afgType) GT 0 THEN BEGIN
      IF STRLOWCASE(SIZE(afgType, /TNAME)) EQ 'string' THEN begin
      		self.adasObj = OBJ_NEW('afg_'+afgType)
		if (self->setsubtype(afgtype)) eq 0 then return,0
      endif else begin
        	RETURN, 0
      endelse  
    ENDIF
    
    desc = self.adasObj->getDesc()
    parNames = STRLOWCASE(TAG_NAMES(desc.parameters))
    parVals = self.adasObj->getPars()
    
    FOR i=0, N_TAGS(desc.parameters)-1 DO BEGIN
      IF STRLOWCASE(desc.parameters.(i).disptype) EQ 'continuous' THEN BEGIN
        ;create ffs pars for each of the parameters required for fitting
        if self->addPar(PARNAME=parNames[i]) eq 0 then return, 0
	;should verify that addpar worked - need to change to function returning 1 or 0
	if self->setParVals(parVals.(i), parname=parNames[i]) eq 0 then return, 0
	IF self->refreshLimits() EQ 0 THEN RETURN, 0
      ENDIF ELSE BEGIN
        ;create ffs pars for each of the parameters that are settings
	;required for underlying routine execution, but will NEVER
	;be varied for fitting.
        if self->addProp(NAME=parNames[i] ) eq 0 then return, 0
        if desc.parameters.(i).type eq 'pointer' then val = *(parVals.(i)) else  val = parVals.(i)
        if self->setParVals(val, parname=parNames[i]) eq 0 then return, 0
	
      ENDELSE
    ENDFOR

    RETURN, 1
END


PRO ffs_adas__define
    self = {ffs_adas, $
             TRAP: 0.0D, $
	     adasObj: OBJ_NEW(), $
             inherits ffs_element}
END

