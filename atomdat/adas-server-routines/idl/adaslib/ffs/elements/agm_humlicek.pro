;The Optimized Humlicek w4 Algorithm
;
;
;************************************************************************
;*                                                                      *
;*     complex probability function for complex argument Z=X+iY         *
;*     real part = voigt function K(x,y)                                *
;*                                                                      *
;*     source:   j. humlicek, JQSRT 27, 437, 1982                       *
;*                                                                      *
;*     parameters:                                                      *
;*      NX     number of grid points = NX+1                         in  *
;*      X      array of grid points                                 in  *
;\***				A.MEIGS -- x = 2*sqrt(ln(2))*(lam-lam0)/fwhm-gaussian ?????????
;*      Y      Voigt function parameter, ratio of lorentz/doppler   in  *
;\*** 				not an array
;*      PRBFCT complex array of function values                     out *
;*                                                                      *
;*     the stated accuracy is claimed to be 1.0E-04 by the author.      *
;*     r h norton has checked the accuracy by comparing values          *
;*     computed using a program written by b.h.armstrong, and           *
;*     the accuracy claim seems to be warranted.                        *
;*                                                                      *
;**************************************************************fgs 12/91*
; A.G. Meigs (JET) 03&04-11-98 Modified from original fortran code from http://www.op.dlr.de/ne-oe/ir/voigt.html
; or paper by F. Schreier; J. Quant. Spectros. Radiat. Transfer 48,
; 743-762 (1992)
; 28-07-2004 and 29-07-2004 AGM and CN optimized for IDL array operations.
;
;      REAL      X(0:NX), Y, S
;      COMPLEX   T, U, PRBFCT(0:NX)
;      COMPLEX   APPROX1, APPROX2, APPROX3, APPROX4
;      COMPLEX   T, U, PRBFCT(0:NX)
;      COMPLEX   APPROX1, APPROX2, APPROX3, APPROX4
;----------------------------------------------------------------------
;
; First version distributed with ADAS.
;
; MODIFIED   :
;       1.1     Martin O'Mullane
;                 - First central ADAS version.
;
; VERSION    :
;       1.1    04-01-2013
;
;----------------------------------------------------------------------

Function  agm_HUMLICEK, X, Y    ; ,reprbfct,imprbfct
  T = DCOMPLEX(Y, -X)
  U      = T * T
  PRBFCT = DCOMPLEXARR(N_elements(x))
;---------------------------
; original code
;APPROX1   = (T * .5641896) / (.5 + (T * T))
;APPROX2 = (T * (1.410474 + U*.5641896))/ (.75 + (U *(3.+U)))
;APPROX3   = ( 16.4955 + T * (20.20933 + T * (11.96482 + T * (3.778987 + 0.5642236*T)))) $
;						/ ( 16.4955 + T * (38.82363 + T * $
;							(39.27121 + T * (21.69274 + T * (6.699398 + T)))))
;APPROX4 = (T * (36183.31 - U * (3321.99 - U * (1540.787 - $
;					U*(219.031 - U *(35.7668 - U *(1.320522 - U * .56419)))))) $
;						/ (32066.6 - U * (24322.8 - U * (9022.23 - U * (2186.18 $
;						- U * (364.219 - U * (61.5704 - U * (1.84144 - U))))))))
;C     ==================================================================
  Case 1 of
     (Y GT 15.0): BEGIN         ; all points are in region I
        ;;APPROX1   = (T * .5641896) / (.5 + (T * T))
        PRBFCT = (T * 0.5641896) / (0.5 + (T * T));;APPROX1
     END
     (Y LT 15.0 AND Y GE 5.5): BEGIN ; points are in region I or region II
        APPROX1   = (T * 0.5641896) / (0.5 + (T * T))
        APPROX2 = (T * (1.410474 + U*0.5641896))/ (0.75 + (U *(3.0 + U)))
        S  = ABS(X) + Y
        index1 = where(S GE 15.0, count1, COMPLEMENT = index2)
        IF count1 GT 0 THEN PRBFCT[index1] = APPROX1[index1]
        IF index2[0] NE -1  THEN PRBFCT[index2] = approx2[index2] 
;---------------------------
; original code
;              APPROX1   = (T * .5641896) / (.5 + (T * T))
;              APPROX2 = (T * (1.410474 + U*.5641896))/ (.75 + (U *(3.+U)))
;              For  I=0,N_ELEMENTS(X)-1 DO BEGIN
;                 S  = ABS(X[I]) + Y
;                 IF (S GE 15.0) THEN BEGIN
;                    PRBFCT[I] = APPROX1[I]
;                 ENDIF ELSE BEGIN
;                    PRBFCT[I] = APPROX2[I]
;                 ENDELSE
;              ENDFOR
     END
     (Y LT 5.5 AND Y GT 0.75): BEGIN
        APPROX1   = (T * 0.5641896) / (0.5 + (T * T))
        APPROX2 = (T * (1.410474 + U*0.5641896))/ (0.75 + (U *(3.0 + U)))
        APPROX3   = ( 16.4955 + T * (20.20933 + T * (11.96482 + T * (3.778987 + 0.5642236 * T)))) $
                    / ( 16.4955 + T * (38.82363 + T * $
                                       (39.27121 + T * (21.69274 + T * (6.699398 + T)))))
        S  = ABS(X) + Y
        ;;
        index1 = WHERE(S GE 15.0, count1);;, COMPLEMENT = complement_GE15)
        IF count1 GT 0 THEN prbfct[index1] = approx1[index1]
        index3 = WHERE(S LT 5.5, count3);;, COMPLEMENT = complement_LT5p5)
        IF count3 GT 0 THEN prbfct[index3] = approx3[index3]
        index2 = WHERE(S GE 5.5 AND S LT 15.0, count2)
        IF count2 NE 0 THEN prbfct[index2] = approx2[index2]
; another way
;              index1 = WHERE(S GE 15.0, count1, COMPLEMENT = complement_GE15)
;              IF count1 GT 0 THEN prbfct[index1] = approx1[index1]
;              IF complement_GE15[0] NE -1 THEN BEGIN   
;                 index3 = WHERE(S LT 5.5, count3, COMPLEMENT = complement_LT5p5)
;                 IF count3 GT 0 THEN prbfct[index3] = approx3[index3]
;                 IF complement_LT5p5[0] NE -1 THEN BEGIN
;                    index2 = CMSET_OP(complement_GE15, 'AND', complement_LT5p5, count2)
;                    IF count2 NE 0 THEN prbfct[index2] = approx2[index2]
;                 ENDIF
;              ENDIF
;---------------------------
; original code
;              APPROX1   = (T * .5641896) / (.5 + (T * T))
;              APPROX2 = (T * (1.410474 + U*.5641896))/ (.75 + (U *(3.+U)))
;              APPROX3   = ( 16.4955 + T * (20.20933 + T * (11.96482 + T * (3.778987 + 0.5642236*T)))) $
;                   / ( 16.4955 + T * (38.82363 + T * $
;                                      (39.27121 + T * (21.69274 + T * (6.699398 + T)))))
;              FOR I=0,N_ELEMENTS(X)-1 DO BEGIN
;                 S  = ABS(X[I]) + Y
;                 CASE 1 of
;                    (S GE 15.0): PRBFCT[I] = APPROX1[I]
;                    (S LT 5.5):  PRBFCT[I] = APPROX3[I]
;                    ELSE:        PRBFCT[I] = APPROX2[I]
;                 ENDCASE
;              ENDFOR
     END
     ELSE: BEGIN
        APPROX1   = (T * 0.5641896) / (0.5 + (T * T))
        APPROX2 = (T * (1.410474 + U*0.5641896))/ (0.75 + (U *(3.0 + U)))
        APPROX3   = ( 16.4955 + T * (20.20933 + T * (11.96482 + T * (3.778987 + 0.5642236*T)))) $
                    / ( 16.4955 + T * (38.82363 + T * $
                                       (39.27121 + T * (21.69274 + T * (6.699398 + T)))))
        APPROX4 = (T * (36183.31 - U * (3321.99 - U * (1540.787 - $
                                                       U*(219.031 - U *(35.7668 - U *(1.320522 - U *0.56419)))))) $
                   / (32066.6 - U * (24322.8 - U * (9022.23 - U * (2186.18 $
                                                                   - U * (364.219 - U * (61.5704 - U * (1.84144 - U))))))))
        ;;------------------------------------------------------
        ;; CN 29-07-2004 Optimized with correction at the index3
        ;; level by AGM
        AX = ABS(X)
        S  = AX + Y

        index1 = where(S GE 15.0, count1)
        IF count1 GT 0 THEN PRBFCT[index1] = APPROX1[index1] ; region I

        index2 = where(S LT 15.0 AND S GE 5.5, count2)
        IF count2 GT 0 THEN PRBFCT[index2] = APPROX1[index2] ; region II

        index3 = where(S LT 5.5, count3)
        IF count3 GT 0 THEN BEGIN
           ;;A = 0.195
           ;;B = -0.176
           ;;C = (Y-B)/A
           C = (Y + 0.176)/0.195
           index3a = WHERE(AX LE C,  count3, COMPLEMENT = index4)
           IF count3 NE 0 THEN PRBFCT[index3a] = APPROX3[index3a] ; region III
           IF index4[0] NE -1 THEN PRBFCT[index4] = EXP(U[index4]) - APPROX4[index4] ; region IV
        ENDIF
        
        ;;AGM 28-07-2004 not quite optimized
;        AX = ABS(x)
;        S  = AX + Y
;        For  I = 0, N_ELEMENTS(X)-1 DO BEGIN
;           CASE 1 of
;              (S[I] GE 15.0):  PRBFCT[I] = APPROX1[I] ; region I
;              (S[I] LT 15.0 AND S[i] GE 5.5): PRBFCT[I] = APPROX2[I] ; region II
;              (S[I] LT 5.5  AND  Y GE (0.195*AX[I]-0.176)): PRBFCT[I] = APPROX3[I] ; region III
;              ELSE: PRBFCT[I] = EXP(U[I]) - APPROX4[I] ; region IV
;           ENDCASE
;        ENDFOR
;---------------------------
; original code
;        For  I = 0, N_ELEMENTS(X)-1 DO BEGIN
;           AX = ABS(X[i])
;           S  = AX + Y
;           CASE 1 of
;              (S GE 15.0):  PRBFCT[I] = APPROX1[I] ; region I
;              (S LT 15.0 AND S GE 5.5): PRBFCT[I] = APPROX2[I] ; region II
;              (S LT 5.5  AND  Y GE (0.195*AX[I]-0.176)): PRBFCT[I] = APPROX3[I] ; region III
;              ELSE: PRBFCT[I] = EXP(U[I]) - APPROX4[I] ; region IV
;           ENDCASE
;        ENDFOR
  ENDELSE
ENDCASE
;C     ==================================================================
  IF (Y EQ 0.0) THEN BEGIN
     PRBFCT = DCOMPLEX(EXP(-X^2), Imaginary(PRBFCT))
  ENDIF
;C     ==================================================================
; adding the real and imaginary parts as return variable
;reprbfct = Double(PRBFCT)
;imprbfct = Imaginary(PRBFCT)
  RETURN, PRBFCT
END
;
; The Fortran code in its entirety:
;The Optimized Humlicek w4 Algorithm
;
;
;************************************************************************
;      SUBROUTINE HUMLICEK (NX,X,Y, PRBFCT)
;*                                                                      *
;*     complex probability function for complex argument Z=X+iY         *
;*     real part = voigt function K(x,y)                                *
;*                                                                      *
;*     source:   j. humlicek, JQSRT 27, 437, 1982                       *
;*                                                                      *
;*     parameters:                                                      *
;*      NX     number of grid points = NX+1                         in  *
;*      X      array of grid points                                 in  *
;*      Y      Voigt function parameter, ratio of lorentz/doppler   in  *
;*      PRBFCT complex array of function values                     out *
;*                                                                      *
;*     the stated accuracy is claimed to be 1.0E-04 by the author.      *
;*     r h norton has checked the accuracy by comparing values          *
;*     computed using a program written by b.h.armstrong, and           *
;*     the accuracy claim seems to be warranted.                        *
;*                                                                      *
;**************************************************************fgs 12/91*
;      REAL      X(0:NX), Y, S
;      COMPLEX   T, U, PRBFCT(0:NX)
;      COMPLEX   APPROX1, APPROX2, APPROX3, APPROX4
;      APPROX1(T)   = (T * .5641896) / (.5 + (T * T))
;      APPROX2(T,U) = (T * (1.410474 + U*.5641896))/ (.75 + (U *(3.+U)))
;      APPROX3(T)   = ( 16.4955 + T * (20.20933 + T * (11.96482 +
;     ^                                   T * (3.778987 + 0.5642236*T))))
;     ^              / ( 16.4955 + T * (38.82363 + T *
;     ^                (39.27121 + T * (21.69274 + T * (6.699398 + T)))))
;      APPROX4(T,U) = (T * (36183.31 - U * (3321.99 - U * (1540.787 - U
;     ^         *(219.031 - U *(35.7668 - U *(1.320522 - U * .56419))))))
;     ^       / (32066.6 - U * (24322.8 - U * (9022.23 - U * (2186.18
;     ^          - U * (364.219 - U * (61.5704 - U * (1.84144 - U))))))))
;C     ==================================================================
;      IF (Y.GT.15.) THEN
;C        ---------------------------------------------------------------
;C        all points are in region I
;         DO 100, I=0,NX
;           T         = CMPLX(Y,-X(I))
; 100       PRBFCT(I) = APPROX1(T)
;C        ---------------------------------------------------------------
;      ELSE IF (Y.LT.15. .AND. Y.GE.5.5) THEN
;C        ---------------------------------------------------------------
;C        points are in region I or region II
;         DO 200, I=0,NX
;           T  = CMPLX(Y,-X(I))
;           S  = ABS(X(I)) + Y
;           IF (S .GE. 15.) THEN
;              PRBFCT(I) = APPROX1(T)
;           ELSE
;              U      = T * T
;              PRBFCT(I) = APPROX2(T,U)
;           END IF
; 200       CONTINUE
;C       ----------------------------------------------------------------
;      ELSE IF (Y.LT.5.5 .AND. Y.GT.0.75) THEN
;C        ---------------------------------------------------------------
;         DO 300, I=0,NX
;           T  = CMPLX(Y,-X(I))
;           S  = ABS(X(I)) + Y
;           IF (S .GE. 15.) THEN
;              PRBFCT(I) = APPROX1(T)
;           ELSE IF (S.LT.5.5) THEN
;              PRBFCT(I) = APPROX3(T)
;           ELSE
;              U      = T * T
;              PRBFCT(I) = APPROX2(T,U)
;           END IF
; 300       CONTINUE
;C       ----------------------------------------------------------------
;      ELSE
;C        ---------------------------------------------------------------
;         DO 400, I=0,NX
;           T  = CMPLX(Y,-X(I))
;           AX = ABS(X(I))
;           S  = AX + Y
;           IF (S .GE. 15.) THEN
;C              region I
;               PRBFCT(I)= APPROX1(T)
;           ELSE IF (S.LT.15.0 .AND. S.GE.5.5) THEN
;C              region II
;               U = T * T
;               PRBFCT(I)= APPROX2(T,U)
;           ELSE IF (S.LT.5.5 .AND. Y.GE.(0.195*AX-0.176)) THEN
;C             region III
;              PRBFCT(I)= APPROX3(T)
;           ELSE
;C             region IV
;              U  = T * T
;              PRBFCT(I)= CEXP(U) - APPROX4(T,U)
;           END IF
; 400       CONTINUE
;C        ---------------------------------------------------------------
;      END IF
;C     ==================================================================
;      IF (Y .EQ. 0.0) THEN
;         DO 20, I=0,NX
;20         PRBFCT(I) = CMPLX(EXP(-X(I)**2), AIMAG(PRBFCT(I)))
;         END IF
;C     ==================================================================
;      RETURN
;      END
