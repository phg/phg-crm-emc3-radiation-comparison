;+
; PROJECT:
;   FFS - Framework for Feature Synthesis (Object oriented model/fitting).
;
; NAME:
;   FFS_BROADEN_GAUSS
;
; PURPOSE:
;   An FFS operator element - takes output of other ffs elements and performs
;   gaussian broadening.
;
; EXPLANATION:
;   Performs broadening operation on other FFS element results. 
;   If the results are gridded, then the input profile is broadened on each
;   pixel.
;
; USE:
;   Example of stand alone use to follow at later date. Currently only used 
;   indirectly by FFS system parser.
;
; INITIALISATION SYNTAX:
;   broaden_obj = obj_new('ffs_broaden_gauss', fwhm=fwhm, $
;       trap=trap, debug=debug)
;   PARAMETERS:
;     unusedparserinput -   input required by the ffs_parser, but unused by
;                           this element
;   KEYWORDS:
;     fwhm	-   full width at half maximum value
;     trap	-   parameter controlling point at which evaluation is truncated
;                   [abs(x-x0) = trap * fwhm].
;     debug     -   set this keyword to enable debug information to be printed 
;                   to the terminal.
;
; PUBLIC ROUTINES:
;   In addtion to the methods listed below, this object inherits methods from
;   ffs_element - refer to this class' documentation for more details.
;
;   [calculate]
;     PURPOSE:
;       Performs gaussian broadening operation on operand elements.
;     INPUTS:
;       in   -  An array of other FFS element evaluation output structures i.e.
;               a structure with the fields:
;                   wavelength  -   double array of the wavelength grid.
;	    	    intensity   -   double array of profile intensity values.
;	    	    gridded     -   0 or 1 to signify if intensities mapped to 
;   	    	    	            wavelength grid.	
;     OUTPUTS:
;	Returns a structure with the following fields:
;	wavelength  -	double array of the wavelength grid.	 
;	intensity   -	double array of profile intensity values.
;	gridded     -	0 or 1 to signify if intensities mapped to 
;   	    	    	wavelength grid. Always 1 in this case.
;     SIDE EFFECTS:
;	None.
;
; AUTHOR:
;   Chris Nicholas, University of Strathclyde
;   (Adaptation of 'ffs_broaden' which was based on Andrew Meigs' GLV).
;
; VERSION HISTORY:
;   1.1  CHN 14/08/2008
;        * Initial commit to CVS.
;   1.2  CHN 18/02/2010
;        * 'calculate' now expands evaluation range over a larger wavelength
;           interval to compensate for inaccuracy where child result has been
;           truncated at the wavelength bounds. After calculation, data is then
;           trimmed to orignal size.
;        * Removed duplicate calculation of 'const' in 'calculate'.
;        * Utilise 'total' and 'rebin' functions to avoid 'for-loop'. Provides
;          significant speed increase.
;   1.3  CHN 22/03/2011
;        * Modified 'calculate':
;          - increased expansion region to 2*fwhm.
;        * Modified 'init':
;          - increased default trap to 10.0.
;----------------------------------------------------------------------
;
; First version distributed with ADAS.
;
; MODIFIED   :
;       1.1     Martin O'Mullane
;                 - First central ADAS version.
;
; VERSION    :
;       1.1    04-01-2013
;
;----------------------------------------------------------------------
;-

function ffs_broaden_gauss::calculate, in 
    if n_elements(in) eq 0 then return, $
      self->seterrmsg('no operand elements for operator element to work on')
    x0 = (*in[0]).wavelength
    intensity = (*in[0]).intensity

    x = self -> getxdata()
    pars = self->getpars()
    result = 0.0d0

    fwhm = pars[0]->getvalue()
    trap = pars[1]->getvalue() 

    padded = 0
    if (*in[0]).gridded eq 1 then begin
        expanded = self->ffs_element::expand_wavegrid(x0, intensity, 2.0*fwhm)
        
        x = expanded.x
        x0 = expanded.x0
        intensity = expanded.intensity
        ; midpoint riemann sum   \
        area = intensity * [x0[1]-x0[0],0.5*(x0[2:*] - x0[0:n_elements(x0)-2]),x0[n_elements(x0)-1]-x0[n_elements(x0)-2]]
        padded = 1
    endif else area = intensity
    
    const = sqrt(4.0d0*alog(2.0d0))
    if fwhm ne 0 then begin
    
        ; both do the same thing, just with different efficincies depending
        ; on the input
        if n_elements(x0) lt 50 then begin
          result=dblarr(n_elements(x))	
          for i=0, n_elements(x0)-1 do begin
	      arg = const*(abs((x-x0[i])/fwhm))
	      idx = where(arg lt const*trap)
	      if idx[0] ne -1 then $         
	          result[idx] = result[idx] + exp(-1d0 * arg[idx]*arg[idx])*area[i]*const/sqrt(!dpi)/fwhm
          endfor

        endif else begin
            ewidth = fwhm/const
            
            if ptr_valid(self.buffer) && self.oldfwhm eq fwhm && array_equal(*self.oldx0,x0) && array_equal(*self.oldx,x) then begin
                exparray=*self.buffer
	    endif else begin
	        exparray=(abs(rotate(rebin(x,n_elements(x),n_elements(x0)),1)-rebin(x0,n_elements(x0),n_elements(x))))

	        idx=where(exparray lt trap*const*ewidth,comp=idx2)

                if idx[0] ne -1 then exparray[idx]=exp(-(exparray[idx]*exparray[idx])/(ewidth*ewidth))
                if idx2[0] ne -1 then exparray[idx2]=0.0

	        if ptr_valid(self.buffer) then begin
                    ptr_free,self.buffer
	            ptr_free,self.oldx0
                    ptr_free,self.oldx
	        endif

                self.buffer=ptr_new(exparray)
	        self.oldfwhm=fwhm
	        self.oldx0=ptr_new(x0)
	        self.oldx=ptr_new(x)
	    endelse
            ; old way - suitable for all but slow (old version was missing normalisation factor though!)
                ;result=dblarr(n_elements(x))	
	        ;for i=0,n_elements(x0)-1 do result=result+area[i]*exparray[i,*]/(ewidth*sqrt(!dpi))
            ; this is fine for gridded items
            result = total(rebin(area, n_elements(x0), n_elements(x)) * exparray, 1)/(ewidth*sqrt(!dpi))
            ; what if not gridded and n_elements(x) ne n_elements(x0)????
            ; this may be suitable but is untested...
            ;area_rebin = rebin(area, n_elements(x0), n_elements(x0))
            ;result = total(congrid(area_rebin, n_elements(x0), n_elements(x)) * exparray, 1)/(ewidth*sqrt(!dpi))
            
        endelse
    endif else begin
       result = area*((0.*x)*(x ne x0)+(x eq x0)) ; if fwhm eq 0 return delta function
    endelse
    
    if padded eq 1 then begin
        trimmed = self->ffs_element::trim_wavegrid(x, result)
        x = trimmed.wavelength
        result = trimmed.intensity
    endif

    return,{wavelength:x, intensity:result, gridded:1}
end

pro ffs_broaden_gauss::cleanup
    if ptr_valid(buffer) then ptr_free, self.buffer
    if ptr_valid(oldx0) then ptr_free, self.oldx0
    if ptr_valid(oldx) then ptr_free, self.oldx
    self->ffs_element::cleanup
end
    
function ffs_broaden_gauss::init, $
    unusedparserinput, $
    fwhm=fwhm, $
    trap=trap, $
    debug=debug
     ;trap is the under flow parameter
    ;(5.0 means that the exp(-trap^2*4*alog(2)) is 7.88859e-31 and 4.0==> 5.421e-20)
    deftrap=10.0d0
    
    if self->ffs_element::init(debug=debug) ne 1 then return, 0
    
    if self->addpar(parname='fwhm') eq 0 then return, 0
    if self->addprop(name='trap') eq 0 then return, 0
      
    if $
        self->setparhardlimits(parname='fwhm', [0.0d,!values.d_infinity]) eq 0 $
        then return, 0

    if n_elements(fwhm) gt 0 then $
        if self->setparvals(fwhm, parname='fwhm') eq 0 then return, 0
        
    if n_elements(trap) gt 0 then begin
      if self->setparvals(trap, name='trap') eq 0 then return, 0
    endif else if self->setparvals(deftrap, parname='trap') eq 0 then return, 0
    
    self.minchildren = 1
    self.maxchildren = 1
    
    return, 1

end


pro ffs_broaden_gauss__define
    self = {ffs_broaden_gauss, $
            buffer:ptr_new(), $
            oldfwhm:0.0d0, $
            oldx0:ptr_new(), $
            oldx:ptr_new(), $
            inherits ffs_element}
end
