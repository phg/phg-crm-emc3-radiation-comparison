;----------------------------------------------------------------------
;
; First version distributed with ADAS.
;
; MODIFIED   :
;       1.1     Martin O'Mullane
;                 - First central ADAS version.
;
; VERSION    :
;       1.1    04-01-2013
;
;----------------------------------------------------------------------


FUNCTION ffs_convolution, obj1, obj2, xdata, _EXTRA = e
  ;; 02-08-2007 AGM started operator to perform convolution of two
  ;; ffs_element and/or ffs_model type objects
  ;;

;;



END
;; From ffs_element__define:
;accepts an array of 'x' values to evaluate the function for.
;method calls evaluate for the instrument function to achieve "ifunctresult", then calls evaluate
;for the actual function itself by making a call to the evaluate method with the ifunctoff switch set.
;the final result returned is the convolution of the ifunctresult with the regular result (ie if the instrument function been ignored)
;-
; FUNCTION ffs_element::convolve, xdata
;   ;; BASE is numerical convolution only.
;   ;;
;   ;; 02-09-05 AGM added
;   IF N_ELEMENTS(xdata) EQ 0 THEN BEGIN
;      xdata = self -> getXdata()
;      IF STRUPCASE(SIZE(xdata, /tname)) EQ 'STRING' THEN return, -1
;   ENDIF ;; question is do we now set xdata?
;   ;;
;   ;; Recenter the instrument function over the center of the x array
;   ctr_x = TOTAL(xdata)/N_ELEMENTS(xdata)
;   avgctr_ifunct = (self -> getiFunct()) -> getCentroid(xdata) ;; 02-02-05 AGM modified from: getAvgCtr()
;   ctr_shift = ctr_x - avgctr_ifunct
;   (self -> getiFunct()) -> setShift, ctr_shift
;   iFunctResult = (self -> getiFunct()) -> Evaluate(xdata[0:N_ELEMENTS(xdata)-2])
;   ;;print,ifunctresult
;   result = self -> evaluate(xdata, IFUNCTOFF = 1)
;   ;;    result = convol(result,iFunctResult,CENTER=1,EDGE_TRUNCATE=1,/NAN) ;need to get better convolve method.
;   result = convol(result, iFunctResult, CENTER = 1, EDGE_TRUNCATE = 1, /NAN) ;need to get better convolve method.
;   ;; Reshift back so that the user never knows
  
;   (self -> getiFunct()) -> setShift, -ctr_shift
;   RETURN, result
; END
