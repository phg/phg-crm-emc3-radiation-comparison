;+
; PROJECT:
;   FFS - Framework for Feature Synthesis (Object oriented model/fitting).
;
; NAME:
;   FFS_DUMMY
;
; PURPOSE:
;   An FFS element - dummy element to just hold parameters
;
; EXPLANATION:
;   This element simply holds parameters, it cannot be evaluated itself
;   such parameters are useful where one wants coupling to an arbitrary
;   arithmetic expression with a parameter which isn't a parameter
;   of another element.
;
; USE:
;   obj = obj_new('ffs_dummy','p1 p2 p3')
;   will create a dummy element with three dummy parameters called
;   "p1", "p2" and "p3".
;
; AUTHOR:
;   Allan Whiteford, University of Strathclyde
;
; VERSION HISTORY:
;   1.1  ADW 20/10/2008
;   	 * Initial commit to CVS.
;----------------------------------------------------------------------
;
; First version distributed with ADAS.
;
; MODIFIED   :
;       1.1     Martin O'Mullane
;                 - First central ADAS version.
;
; VERSION    :
;       1.1    04-01-2013
;
;----------------------------------------------------------------------
;-

function ffs_dummy::init,sparnames,debug=debug
	If Self->ffs_element::Init(debug=debug) NE 1 Then Return, 0
	parnames=strsplit(sparnames,' ',/extract)
	for i=0,n_elements(parnames)-1 do if self->addPar(PARNAME=parnames[i]) eq 0 then return, 0
	return,1
end

pro ffs_dummy__define
    self = {ffs_dummy, $
            inherits ffs_element}
end
