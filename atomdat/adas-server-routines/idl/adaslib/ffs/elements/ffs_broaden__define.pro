;+
; PROJECT:
;   FFS - Framework for Feature Synthesis (Object oriented model/fitting).
;
; NAME:
;   FFS_BROADEN
;
; PURPOSE:
;   An FFS operator element - takes output of other ffs elements and performs
;   gaussian, lorentzian or voigt profile broadening as requested.
;
; EXPLANATION:
;   Performs broadening operation on other FFS element results. The desired 
;   broadening line shape is specified. If the results are gridded, then the
;   input profile is broadened on each pixel.
;
; USE:
;   Example of stand alone use to follow at later date. Currently only used 
;   indirectly by FFS system parser.
;
; INITIALISATION SYNTAX:
;   broaden_obj = obj_new('ffs_broaden', broaden_type, fwhm=fwhm, fwhmg=fwhmg, $
;     fwhml=fwhml, trap=trap, debug=debug)
;   PARAMETERS:
;     broaden_type  -	string dictating broadening type: possible values are
;   	    	    	gaussian, lorentzian and voigtian.
;   KEYWORDS:
;     fwhm	-   full width at half maximum value (used for gaussian / lorentzian profile).
;     fwhmg 	-   full width at half maximum value for gauss (used for voigt profile).
;     fwhml 	-   full width at half maximum value for lorentz (used for voigt profile).
;     trap	-   parameter at which evaluation truncated [abs(x-x0) = trap * fwhm].
;     debug - 	set this keyword to enable debug information to be printed to the terminal.
;
; PUBLIC ROUTINES:
;   In addtion to the methods listed below, this object inherits methods from ffs_element
;   - refer to this class' documentation for more details.
;
;   [calculate]
;     PURPOSE:
;       Performs FFS add operation on operand elements.
;     INPUTS:
;       in   -  An array of other FFS element evaluation output structures i.e. a
;           	structure with the fields:
;                   wavelength  -   double array of the wavelength grid.	 
;	    	    intensity   -   double array of profile intensity values.
;	    	    gridded     -   0 or 1 to signify if intensities mapped to 
;   	    	    	            wavelength grid.	
;     OUTPUTS:
;	Returns a structure with the following fields:
;	wavelength  -	double array of the wavelength grid.	 
;	intensity   -	double array of profile intensity values.
;	gridded     -	0 or 1 to signify if intensities mapped to 
;   	    	    	wavelength grid. Always 1 in this case.
;     SIDE EFFECTS:
;	Calls agm_humlicek.
;
; AUTHOR:
;   Christopher Nicholas, University of Strathclyde
;   (Based on Andrew Meigs' GLV gaussian, lorentzian, voigt).
;
; VERSION HISTORY:
;   1.1  ADW 10/08/2007
;        * Initial commit to CVS.
;   1.2  CHN 13/08/2007
;        * modified evaluate method to save result before returning value.
;   1.3  CHN 14/08/2007
;        * updated to allow for lorentzian broadening in addition to exisiting gaussian.
;   1.4  CHN 11/09/2007
;        * removed usage of obsolete setRule
;        * updated to support ffs_prop objects
;   1.5  CHN 22-10-2007
;        * updated to allow for voigt broaden option.
;        * inserted documentation
;   1.6  ADW 13-11-2007
;        * Changed evaluate function name to calculate and made it return a
;          results structure rather storing it and returning a getresults.
;        * Made the calculate method more efficient for Gaussians by giving two
;          possibilities one loop-based where exponentials which aren't
;          required (set via trap) aren't calculated and another where loops
;          aren't used and an intermediate array (function of grids and fwhm)
;          are stored between calls.
;        * Set default trap based on line type - previously was setting to 25
;          (good Lorentzian default for every line).
;   1.7  CHN 05-12-2007
;        * Altered all calls to 'setparvals' to reflect update of this method
;          from procedure to function as part of error handling strategy.
;   1.8  CHN 07-12-2007
;        * Added setting of appropriate hard / soft limits on pars in init.
;        * Fixed typos introduced in last update.
;   1.9  CHN 08/01/2008
;        * Added debug keyword to init to pass thorugh to superclass.
;        * Re-write of documentation.
;        * Removed obsolete partype keyword from addpar calls.
;   1.10 ADW 08/01/2008
;        * Fixed small bug.
;        * Realigned comments to take account of 4 character version.
;   1.11 CHN 09/01/2008
;        * debug keyword in init had escaped documentation.
;   1.12 CHN 23/01/2008
;        * changed addpar calls in init to reflect changes to ffs_element.
;   1.13 CHN 19/02/2008
;        * Set 'maxchildren' in init.
;   1.14 CHN 25/02/2008
;        * Set 'minchildren' in init.
;        * Added error message to calculate method for case of no input.
;   1.15 ADW 12/03/2008
;        * Changed internal broaden_type to inherited (general) subtype.
;   1.16 CHN 10/11/2008
;        * Lorentzian trap re-defined in terms of the fwhm rather than hwhm.
;        * Allow use of 'fwhml' and 'fwhmg' keywords in 'init' for gaussian
;          and lorentzian broadening.
;        * Default trap size increased to 40.0 for gaussian and lorentzian
;          broadeners.
;----------------------------------------------------------------------
;
; First version distributed with ADAS.
;
; MODIFIED   :
;       1.1     Martin O'Mullane
;                 - First central ADAS version.
;
; VERSION    :
;       1.1    04-01-2013
;
;----------------------------------------------------------------------
;-

FUNCTION ffs_broaden::calculate, in 
  if n_elements(in) eq 0 then return, $
    self->seterrmsg('No operand elements for operator element to work on')
  x0 = (*in[0]).wavelength
  area = (*in[0]).intensity

  x = self -> getXdata()
  pars = self -> getPars()
  result = 0.0d
  
  ;##########
  ; GAUSSIAN
  ;##########
  IF STRLOWCASE(self->getsubtype()) EQ 'gaussian' THEN BEGIN
    fwhm = pars[0]->getValue()
    trap = pars[1]->getValue() 
  
    IF N_ELEMENTS(trap) EQ 0 THEN trap = 5.0D ELSE TRAP=Trap ;; setup the under flow parameter
    ;;(5.0 means that the exp(-trap^2*4*alog(2)) is 7.88859e-31 and 4.0==> 5.421e-20)
    const = sqrt(4.0D*Alog(2.0D))
    IF fwhm NE 0 THEN BEGIN
      
        ;FOR i=0, N_ELEMENTS(x0)-1 DO BEGIN
        ;    arg = const*(abs((x-x0[i])/fwhm) < trap)
        ;    result = result +exp(-arg*arg)*(arg lt const*trap)*area[i]*const/sqrt(!DPI)/fwhm
        ;ENDFOR
      
        ; Both do the same thing, just with different efficincies depending
        ; on the input
        if n_elements(x0) lt 50 then begin
                result=dblarr(n_elements(x))	
		FOR i=0, N_ELEMENTS(x0)-1 DO BEGIN
			arg = const*(abs((x-x0[i])/fwhm))
			idx = where(arg lt const*trap)
			if idx[0] ne -1 then $         
			result[idx] = result[idx] +exp(-1d0 * arg[idx]*arg[idx])*area[i]*const/sqrt(!DPI)/fwhm
		ENDFOR
  
        endif else begin
                ewidth=fwhm / (2.0 * sqrt(alog(2)))

                if ptr_valid(self.buffer) && self.oldfwhm eq fwhm && array_equal(*self.oldx0,x0) && array_equal(*self.oldx,x) then begin
			exparray=*self.buffer
		endif else begin
			exparray=(abs(rotate(rebin(x,n_elements(x),n_elements(x0)),1)-rebin(x0,n_elements(x0),n_elements(x))))
			idx=where(exparray lt trap*const*ewidth,comp=idx2)
			
                        if idx[0] ne -1 then exparray[idx]=exp(-(exparray[idx]*exparray[idx])/(ewidth*ewidth))
                        if idx2[0] ne -1 then exparray[idx2]=0.0
			
			if ptr_valid(self.buffer) then begin
                        	ptr_free,self.buffer
				ptr_free,self.oldx0
                                ptr_free,self.oldx
			endif
			
                        self.buffer=ptr_new(exparray)
			self.oldfwhm=fwhm
			self.oldx0=ptr_new(x0)
			self.oldx=ptr_new(x)
		endelse
              
		result=dblarr(n_elements(x))	
		for i=0,n_elements(x0)-1 do result=result+area[i]*exparray[i,*]
                result = result / 1.0/(ewidth*sqrt(!pi))
	endelse
    ENDIF ELSE BEGIN
       result = area*((0.*x)*(x ne x0)+(x eq x0)) ; if fwhm eq 0 return delta function
    ENDELSE
  ;############
  ; LORENTZIAN
  ;############   
  ENDIF ELSE IF STRLOWCASE(self->getsubtype()) EQ 'lorentzian' THEN BEGIN
    fwhm = pars[0]->getValue()
    trap = pars[1]->getValue() 
   IF N_ELEMENTS(trap) EQ 0 THEN trap = 25.0D ELSE TRAP=Trap ;; setup the under flow parameter
 
   IF fwhm NE 0.0D THEN BEGIN
     FOR i=0, N_ELEMENTS(x0)-1 DO BEGIN
      ;arg=((x-x0)^2 + (fwhm/2.0D)^2)
      ;redefine trap in terms of full width (chn):
      result = result + (abs(x-x0[i]) le trap*fwhm) * (area[i]*(fwhm/2.0D)/((x-x0[i])^2 + (fwhm/2.0D)^2)/!DPI)
      ;result = result + (abs(x-x0[i]) le trap*(fwhm/2.0D)) * (area[i]*(fwhm/2.0D)/((x-x0[i])^2 + (fwhm/2.0D)^2)/!DPI)
      ;;result=(abs(x-x0) le trap*(fwhm/2.0D))*(area*(fwhm/2.0D)/arg/!DPI) ; page 1350 of notebook 8.
     ENDFOR
   ENDIF ELSE BEGIN
      result=area*((0.*x)*(x ne x0)+(x eq x0)) ; if fwhm eq 0 return delta function
      ;;result = result*area                 ; make the area of the delta function just the intensity
   ENDELSE
  ;#######
  ; VOIGT
  ;#######    
  ENDIF ELSE IF STRLOWCASE(self->getsubtype()) EQ 'voigtian' THEN BEGIN

    fwhmg = pars[0]->getValue()
    fwhml = pars[1]->getValue()
    trap = pars[2]->getValue()     
  
    ; Set up a trap variable to stop calculation of the function beyond abs(x-x0) = trap * full width
    IF N_ELEMENTS(trap) EQ 0 THEN trap = 80.0D ELSE TRAP=Trap ;; setup the under flow parameter

    fullwidth = fwhml > fwhmg ; pick the greater of the two widths
  
    IF fwhmg EQ 0.0D AND fwhml EQ 0.0D THEN result=area*((0.*x)*(x ne x0)+(x eq x0)) ELSE BEGIN
  
      IF fwhmg ne 0.0D THEN BEGIN
	 alpha = 2.0D*sqrt(Alog(2.0D))/fwhmg 
      ENDIF ELSE alpha = 1.0D

      bb = alpha*(fwhml/2.0D)

      FOR i=0, N_ELEMENTS(x0)-1 DO BEGIN
	aa = alpha*(x-x0[i])
	kay = DOUBLE(agm_humlicek(aa,bb))
	result =  result + area[i]*(alpha/sqrt(!DPI))*kay*(abs(x-x0[i]) le trap * fullwidth)
      ENDFOR
      
    ENDELSE 
  ENDIF
  
  
  return,{wavelength:x, intensity:result, gridded:1}

END


FUNCTION ffs_broaden::init, $
    broaden_type,$
    FWHM=fwhm, $
    FWHMG=fwhmg, $
    FWHML=fwhml, $
    TRAP=trap, $
    debug=debug
    
    IF self->ffs_element::init(debug=debug) NE 1 THEN RETURN, 0
    IF N_ELEMENTS(broaden_type) GT 0 THEN junk=self->setsubtype(broaden_type) ELSE RETURN, 0
    IF junk eq 0 then return, 0
    
    IF (broaden_type EQ 'gaussian') OR (broaden_type EQ 'lorentzian') THEN BEGIN
      if self->addPar(PARNAME='fwhm') eq 0 then return, 0
      if self->addProp(NAME='trap') eq 0 then return, 0
      
      if self->setparhardlimits(PARNAME='fwhm', [0.0d,!values.d_infinity]) eq 0 then return, 0

      IF N_ELEMENTS(fwhml) GT 0 THEN if self->setParVals(fwhml, parname='fwhm') eq 0 then return, 0
      IF N_ELEMENTS(fwhmg) GT 0 THEN if self->setParVals(fwhmg, parname='fwhm') eq 0 then return, 0
      IF N_ELEMENTS(fwhm) GT 0 THEN if self->setParVals(fwhm, parname='fwhm') eq 0 then return, 0
      deftrap=40.0d0
    ENDIF
    
    IF (broaden_type EQ 'voigtian') THEN BEGIN
      if self->addPar(PARNAME='fwhmg') eq 0 then return, 0
      if self->addPar(PARNAME='fwhml') eq 0 then return, 0
      if self->addProp(NAME='trap') eq 0 then return, 0
      
      if self->setparhardlimits(PARNAME='fwhmg', [0.0d,!values.d_infinity]) eq 0 then return, 0
      if self->setparhardlimits(PARNAME='fwhml', [0.0d,!values.d_infinity]) eq 0 then return, 0
      
      IF N_ELEMENTS(fwhmg) GT 0 THEN if self->setParVals(fwhmg, parname='fwhmg') eq 0 then return, 0
      IF N_ELEMENTS(fwhml) GT 0 THEN if self->setParVals(fwhml, parname='fwhml') eq 0 then return, 0      
      deftrap=40.0d0
    ENDIF   
    
    if N_ELEMENTS(trap) GT 0 then begin
      if self->setParVals(trap, name='trap') eq 0 then return, 0
    endif else if self->setParVals(deftrap, parname='trap') eq 0 then return, 0
    self.minchildren = 1
    self.maxchildren = 32767
    RETURN, 1
END


PRO ffs_broaden__define
    self = {ffs_broaden, $
            buffer:ptr_new(), $
            oldfwhm:0.0d0, $
            oldx0:ptr_new(), $
            oldx:ptr_new(), $
            inherits ffs_element}
END

