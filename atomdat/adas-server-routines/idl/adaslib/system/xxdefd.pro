; Copyright (c) 1995 Strathclyde University .
; SCCS info: Module @(#)$Header: /home/adascvs/idl/adaslib/system/xxdefd.pro,v 1.4 2004/07/06 15:34:06 whitefor Exp $ Date $Date: 2004/07/06 15:34:06 $
;
;+
; PROJECT:
;       ADAS IBM MVS to DEC UNIX conversion
;
; NAME:
;	XXDEFD
;
; PURPOSE:
;	To set up an array of 'standard' temperatures.
;
; EXPLANATION:
;	This routine performs the same function as the IBM ADAS
;	FORTRAN subroutine XXDEFT. It sets up in 'denr()' array
;	standard set of reduced densities used in constructing master
;	condensed collisional-dielectrionic and metastable population
;	files.
;
; USE:
;	Example use;
;	
;		ndtin = 8
;		xxdefd, ndden, ide, denr
;
; INPUTS:
;	NDDEN	- Maximum number of densities allowed
;
; OPTIONAL INPUTS:
;	None.
;
; OUTPUTS:
;	IDE	- Integer; Number of densities set
;
;	DENR	- Dblarr; Output set of reduced electron densities.(cm-3/z1**7)
;
; OPTIONAL OUTPUTS:
;	None.
;
; KEYWORD PARAMETERS:
;	None.
;
; CALLS:
;	None.
;
; SIDE EFFECTS:
;	None.
;
; CATEGORY:
;	Adas system.
;
; WRITTEN:
;       Andrew Bowen, Tessella Support Services plc, 20-Apr-1993
;
; MODIFIED:
;       Version 1       Andrew Bowen    1-Jun-1993
;                       First release.
;
; VERSION:
;       1       1-Jun-1993
;-
;----------------------------------------------------------------------------

PRO xxdefd, ndden, ide, denr

		;**** the number of default densities ****
  ndset = 8

		;**** the output density array ****
  denr = dblarr(ndden)

                ;**** Check number of temperatures ****
  ON_ERROR, 2
  if ndden lt ndset then message,'XXDEFT ERROR: NDDEN < NDSET. INCREASE NDDEN'

		;**** the default values ****
		;**** densities: (reduced - cm-3/z1**7) ****
  denr(0:ndset-1) = [ 1.0D-03 , 1.0D+00 , 1.0D+03 , 1.0D+06 , $
		      1.0D+09 , 1.0D+12 , 1.0D+15 , 1.0D+18 ]

		;**** return the number of values set in denr ****
  ide = ndset

END
