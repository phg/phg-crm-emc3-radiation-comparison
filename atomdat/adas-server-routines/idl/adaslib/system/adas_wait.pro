;----------------------------------------------------------------------
;+
; PROJECT:
;       ADAS
;
; NAME:
;       ADAS_WAIT
;
; PURPOSE: Waits until the fortran process is finished.
;
; EXPLANATION:
;       Some IDL routines terminate before the fortran is fully
;       finished. This can abruptly terminate the fortran. The
;       typical manifistation is missing lines at the end of
;       output files. This routine waits until the process is
;       over before returing.
;
; USE:
;       A FORTRAN process must have been spawned so that the process ID
;       can be passed to this routine.
;
; INPUTS:
;       PID   - Long integer; the process ID of the fortran.
;                             (use pid=pid in call to spawn).
;
; OUTPUTS:
;       None
;
; KEYWORD PARAMETERS:
;       DELAY - float; optional delay between checks of pipe (0.2s default).
;
; CALLS:
;       FIND_PROCESS - Checks to see if a given process is active.
;
; SIDE EFFECTS:
;       If the fortran does not terminate neither will this routine.
;
; CATEGORY:
;       Adas system.
;
; WRITTEN:
;       Martin O'Mullane
;
; MODIFIED:
;       1.1     Martin O'Mullane
;                - First release
;
; VERSION:
;       1.1     10-10-2005
;
;-
;----------------------------------------------------------------------

PRO adas_wait, pid, delay=delay

if n_elements(delay) EQ 0 then w_time = 0.2 else w_time = float(delay)

while find_process(pid) EQ 1 do begin
  wait, w_time
endwhile

END
