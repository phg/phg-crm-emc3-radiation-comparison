; Copyright (c) 1996 Strathclyde University .
; SCCS info: Module @(#)$Header: /home/adascvs/idl/adaslib/system/localize.pro,v 1.31 2012/12/02 23:29:29 mog Exp $ Date $Date: 2012/12/02 23:29:29 $
;+
; PROJECT:
;       ADAS IBM MVS to DEC UNIX conversion
;
; NAME:
;       LOCALIZE
;
; PURPOSE:
;       To set parameters for plotting screens dependent on machine.
;
; EXPLANATION:
;       This routine specifies the parameters:
;               {left, right, top, bottom, grtop, grright}
;       which set-up a graph in the Graphics Output Window. The addition of
;       the UNIX environment variable TARGET_MACHINE to ADAS allows for
;       the localization of the above parameters for specific machines.
;
; USE:
;       This routine is simply called from cw_adas_graph.pro
;       with the command
;               localize
;
; INPUTS:
;       None
;
; OPTIONAL INPUTS:
;       None
;
; OUTPUTS:
;       None
;
; OPTIONAL OUTPUTS:
;       None
;
; KEYWORD PARAMETERS:
;       None
;
; CALLS:
;       GETENV  To get the value of the environment variable TARGET_MACHINE
;
; SIDE EFFECTS:
;       This routine uses the COMMON block Global_adas_num which contains
;       numbers corresponding to the adas routine which is selected by the
;       user. It also uses a second global COMMON block Global_lw_data into
;       which it puts the values of the parameters left, right, top, bottom,
;       grtop, grright for reading by the plotting routines.

; CATEGORY:
;       Adas system.
;
; WRITTEN:
;       E. J. Mansky, Oak Ridge National Laboratory, 9-28-95.
;
; MODIFIED:
;       1.1     Tim Hammond
;               Added standard headers and put into S.C.C.S.
;       1.2     Tim Hammond
;               Added settings for adas 203.
;       1.3     Tim Hammond
;               Corrected minor syntax error.
;       1.4     Tim Hammond
;               Added settings for adas 505.
;       1.5     William Osborn
;               Added settings for adas 407.
;       1.6     William Osborn
;               Added settings for adas 509.
;       1.7     William Osborn
;               Added settings for adas 208.
;       1.8     William Osborn
;               Syntax error
;       1.9     William Osborn
;               Added settings for adas 601.
;       1.10    William Osborn
;               Added settings for adas 306.
;       1.11    William Osborn
;               Added settings for adas 206.
;       1.12    William Osborn
;               Added settings for adas 406.
;       1.13    William Osborn
;               Syntax error
;       1.14    William Osborn
;               Added settings for adas 105.
;       1.15    Tim Hammond
;               Modified settings for adas208
;       1.16    William Osborn
;               Added settings for adas 106
;       1.17    William Osborn
;               Added settings for adas 402
;       1.18    William Osborn
;               Added settings for adas 103
;       1.19    Hugh Summers
;               Added settings for adas 302
;       1.20    Hugh Summers
;               Added settings for adas 102
;       1.21    David Brooks
;               Added settings for adas 602
;       1.22    Richard Martin and Martin O'Mullane.
;               Added settings for adas410, adas412 and adas602 for HP's.
;       1.23    Richard Martin
;               Added settings for adas409 and adas411.
;       1.24    Richard Martin
;               Added settings for adas413, adas214, adas215.
;       1.25    Richard Martin
;               Added settings for adas314 and adas603.
;       1.26    Hugh Summers
;               Added settings for adas108.
;       1.27    Richard Martin
;               Added settings for adas414 and adas415.
;       1.28    Martin O'Mullane
;               Added settings for adas 507
;       1.29    Richard Martin
;               Added settings for adas 510
;       1.30    Hugh Summers
;               Added settings for adas 809
;       1.31    Martin O'Mullane
;               Added settings for adas 901
;
; VERSION:
;     1.1   27-02-96
;       1.2     01-04-96
;       1.3     01-04-96
;       1.4     02-04-96
;       1.5     19-04-96
;       1.6     30-04-96
;       1.7     08-05-96
;       1.8     08-05-96
;       1.9     08-05-96
;       1.10    08-05-96
;       1.11    07-06-96
;       1.12    12-06-96
;       1.13    12-06-96
;       1.14    01-10-96
;       1.15    04-10-96
;       1.16    07-10-96
;       1.17    31-10-96
;       1.18    05-11-96
;       1.19    12-11-96
;       1.20    15-11-96
;       1.21    17-04-97
;       1.22    31-10-97
;       1.23    09-03-98
;       1.24    23-06-98
;       1.25    04-12-98
;       1.26    ??-05-99
;       1.27    04-04-00
;       1.28    06-02-01
;       1.29    10-05-02
;       1.30    18-02-03
;       1.31    24-10-12
;
;-
;-----------------------------------------------------------------------------

PRO localize

    COMMON Global_adas_num, mainchoice, subchoice
    COMMON Global_lw_data, left, right, top, bottom, grtop, grright

    machine = GETENV('TARGET_MACHINE')

                ;*************************************************
                ;**** TARGET_MACHINE is an HP 700 workstation ****
                ;**** running HP-UX operating system.         ****
                ;*************************************************

    IF (machine EQ 'HPUX') THEN BEGIN
        CASE mainchoice OF
            0: CASE subchoice OF
                0: begin
                    left = 0.1
                    right = 0.9
                    top = 0.91
                    bottom = 0.1
                    grtop = 0.8
                    grright = 0.70
                end
                1: begin
                    left = 0.1
                    right = 0.9
                    top = 0.91
                    bottom = 0.1
                    grtop = 0.8
                    grright = 0.70
                end
                2: begin
                    left = 0.1
                    right = 0.9
                    top = 0.91
                    bottom = 0.1
                    grtop = 0.8
                    grright = 0.60
                end
                3:
                4: begin
                    left = 0.1
                    right = 0.9
                    top = 0.91
                    bottom = 0.1
                    grtop = 0.8
                    grright = 0.60
                end
                5: begin
                    left = 0.1
                    right = 0.9
                    top = 0.91
                    bottom = 0.1
                    grtop = 0.8
                    grright = 0.60
                end
                6:
                7: begin
                    left = 0.1
                    right = 0.9
                    top = 0.91
                    bottom = 0.1
                    grtop = 0.8
                    grright = 0.70
              end
                ELSE:
            ENDCASE
            1: CASE subchoice OF
                0: begin
                    left = 0.1
                    right = 0.9
                    top = 0.95
                    bottom = 0.15
                    grtop = 0.8
                    grright =0.65
                end
                1:
                2: begin
                    left = 0.10
                    right = 0.9
                    top = 0.91
                    bottom = 0.1
                    grtop = 0.8
                    grright = 0.65
                end
                3:
                4: begin
                    left = 0.11
                    right = 0.9
                    top = 0.95
                    bottom = 0.15
                    grtop = 0.8
                    grright = 0.7
                end
                5: begin
                    left = 0.11
                    right = 0.9
                    top = 0.95
                    bottom = 0.15
                    grtop = 0.8
                    grright = 0.7
                end
                6: begin
                    left = 0.12
                    right = 0.98
                    top = 0.95
                    bottom = 0.15
                    grtop = 0.8
                    grright = 0.7
                end
                7: begin
                    left = 0.11
                    right = 0.9
                    top = 0.95
                    bottom = 0.15
                    grtop = 0.8
                    grright = 0.7
                end
                8:              ;**** Not applicable ****
                9:              ;**** Not applicable ****
                10:             ;**** Not applicable ****
                11:             ;**** Not applicable ****
                12:             ;**** Not applicable ****
                13: begin
                     left = 0.11
                     right = 0.9
                     top = 0.95
                     bottom = 0.15
                     grtop = 0.8
                     grright = 0.7
                    end
                14: begin
                     left = 0.11
                     right = 0.9
                     top = 0.95
                     bottom = 0.15
                     grtop = 0.8
                     grright = 0.7
                    end
                ELSE:
            ENDCASE
            2: CASE subchoice OF
                0: begin
                    left = 0.15
                    right = 0.9
                    top = 0.95
                    bottom = 0.15
                    grtop = 0.8
                    grright = 0.7
                end
                1:
                2: begin
                    left = 0.1
                    right = 0.9
                    top = 0.95
                    bottom = 0.15
                    grtop = 0.8
                    grright = 0.65
                end
                3: begin
                    left = 0.10
                    right = 0.70
                    top = 0.90
                    bottom = 0.12
                    grtop = 0.8
                    grright = 0.70
                end
                4:
                5: begin
                    left = 0.05
                    right = 0.34
                    top = 0.90
                    bottom = 0.12
                    grtop = 0.8
                    grright = 0.34
                end
                6:
                7: begin
                    left = 0.05
                    right = 0.34
                    top = 0.90
                    bottom = 0.12
                    grtop = 0.8
                    grright = 0.34
                end
                8:              ;**** Not applicable ****
                9:              ;**** Not applicable ****
                10:
                ELSE:
            ENDCASE
            3: CASE subchoice OF
                0: begin
                    left = 0.10
                    right = 0.85
                    top = 0.91
                    bottom = 0.1
                    grtop = 0.8
                    grright = 0.65
                end
                1: begin
                    left = 0.10
                    right = 0.85
                    top = 0.91
                    bottom = 0.1
                    grtop = 0.8
                    grright = 0.65
                end
                2:
                3:
                4: begin
                    left = 0.10
                    right = 0.70
                    top = 0.90
                    bottom = 0.12
                    grtop = 0.8
                    grright = 0.70
                end
                5: begin
                    left = 0.10
                    right = 0.70
                    top = 0.90
                    bottom = 0.12
                    grtop = 0.8
                    grright = 0.70
                end
                6: begin
                    left = 0.10
                    right = 0.70
                    top = 0.90
                    bottom = 0.12
                    grtop = 0.8
                    grright = 0.70
                end
                7:
                8: begin
                    left = 0.10
                    right = 0.70
                    top = 0.90
                    bottom = 0.12
                    grtop = 0.8
                    grright = 0.70
                end
                9: begin
                    left = 0.1
                    right = 0.9
                    top = 0.86
                    bottom = 0.1
                    grtop = 0.8
                    grright = 0.65
                end
                10: begin
                    left = 0.10
                    right = 0.70
                    top = 0.90
                    bottom = 0.12
                    grtop = 0.8
                    grright = 0.70
                end
                11: begin
                    left = 0.10
                    right = 0.70
                    top = 0.90
                    bottom = 0.12
                    grtop = 0.8
                    grright = 0.70
                end
                12: begin
                    left = 0.10
                    right = 0.70
                    top = 0.90
                    bottom = 0.12
                    grtop = 0.8
                    grright = 0.70
                end
                13: begin
                    left = 0.1
                    right = 0.9
                    top = 0.95
                    bottom = 0.15
                    grtop = 0.8
                    grright = 0.65
                end
                14: begin
                    left = 0.1
                    right = 0.9
                    top = 0.95
                    bottom = 0.15
                    grtop = 0.8
                    grright = 0.65
                end
                ELSE:
            ENDCASE
            4: CASE subchoice OF
                0: begin
                    left = 0.1
                    right = 0.9
                    top = 0.95
                    bottom = 0.15
                    grtop = 0.8
                    grright = 0.65
                end
                1: begin
                    left = 0.1
                    right = 0.9
                    top = 0.86
                    bottom = 0.1
                    grtop = 0.8
                    grright = 0.65
                end
                2: begin
                    left = 0.1
                    right = 0.9
                    top = 0.95
                    bottom = 0.15
                    grtop = 0.8
                    grright = 0.65
                end
                3: begin
                    left = 0.1
                    right = 0.9
                    top = 0.86
                    bottom = 0.1
                    grtop = 0.8
                    grright = 0.65
                end
                4: begin
                    left = 0.1
                    right = 0.9
                    top = 0.86
                    bottom = 0.1
                    grtop = 0.8
                    grright = 0.65
                end
                5: begin
                    left = 0.1
                    right = 0.9
                    top = 0.86
                    bottom = 0.1
                    grtop = 0.8
                    grright = 0.65
                end
                6: begin
                    left = 0.1
                    right = 0.9
                    top = 0.86
                    bottom = 0.1
                    grtop = 0.8
                    grright = 0.65
                end
                7:
                8: begin
                    left = 0.1
                    right = 0.9
                    top = 0.86
                    bottom = 0.1
                    grtop = 0.8
                    grright = 0.65
                end
                9: begin
                    left = 0.1
                    right = 0.9
                    top = 0.86
                    bottom = 0.1
                    grtop = 0.8
                    grright = 0.65
                end
                ELSE:
            ENDCASE
            5: CASE subchoice OF
                0: begin
                  left = 0.1
                  right = 0.9
                  top = 0.86
                  bottom = 0.1
                  grtop = 0.8
                  grright = 0.65
                end
            1: begin
                  left = 0.1
                  right = 0.9
                  top = 0.86
                  bottom = 0.1
                  grtop = 0.9
                  grright = 0.9
                end
            2: begin
              left = 0.1
              right = 0.9
              top = 0.86
              bottom = 0.1
              grtop = 0.9
              grright = 0.9
            end
                ELSE:
            ENDCASE
            6: CASE subchoice OF
                0: begin
                    left = 0.1
                    right = 0.9
                    top = 0.95
                    bottom = 0.15
                    grtop = 0.8
                    grright =0.65
                   end
                1: begin
                    left = 0.1
                    right = 0.9
                    top = 0.95
                    bottom = 0.15
                    grtop = 0.8
                    grright =0.65
                   end
                2: begin
                    left = 0.10
                    right = 0.9
                    top = 0.91
                    bottom = 0.1
                    grtop = 0.8
                    grright = 0.65
                   end
                3: begin
                    left = 0.10
                    right = 0.9
                    top = 0.91
                    bottom = 0.1
                    grtop = 0.8
                    grright = 0.65
                   end
                4: begin
                    left = 0.11
                    right = 0.9
                    top = 0.95
                    bottom = 0.15
                    grtop = 0.8
                    grright = 0.7
                   end
                5: begin
                    left = 0.11
                    right = 0.9
                    top = 0.95
                    bottom = 0.15
                    grtop = 0.8
                    grright = 0.7
                   end
                6: begin
                    left = 0.12
                    right = 0.98
                    top = 0.95
                    bottom = 0.15
                    grtop = 0.8
                    grright = 0.7
                   end
                7: begin
                    left = 0.11
                    right = 0.9
                    top = 0.95
                    bottom = 0.15
                    grtop = 0.8
                    grright = 0.7
                   end
                ELSE:
            ENDCASE
            7: CASE subchoice OF
                0: begin
                    left = 0.1
                    right = 0.9
                    top = 0.95
                    bottom = 0.15
                    grtop = 0.8
                    grright =0.65
                   end
                1: begin
                    left = 0.1
                    right = 0.9
                    top = 0.95
                    bottom = 0.15
                    grtop = 0.8
                    grright =0.65
                   end
                2: begin
                    left = 0.10
                    right = 0.9
                    top = 0.91
                    bottom = 0.1
                    grtop = 0.8
                    grright = 0.65
                   end
                3: begin
                    left = 0.10
                    right = 0.9
                    top = 0.86
                    bottom = 0.1
                    grtop = 0.9
                    grright = 0.9
                   end
                4: begin
                    left = 0.11
                    right = 0.9
                    top = 0.95
                    bottom = 0.15
                    grtop = 0.8
                    grright = 0.7
                   end
                5: begin
                    left = 0.11
                    right = 0.9
                    top = 0.95
                    bottom = 0.15
                    grtop = 0.8
                    grright = 0.7
                   end
                6: begin
                    left = 0.12
                    right = 0.98
                    top = 0.95
                    bottom = 0.15
                    grtop = 0.8
                    grright = 0.7
                   end
                7: begin
                    left = 0.11
                    right = 0.9
                    top = 0.95
                    bottom = 0.15
                    grtop = 0.8
                    grright = 0.7
                   end
                8: begin
                    left = 0.11
                    right = 0.9
                    top = 0.95
                    bottom = 0.15
                    grtop = 0.8
                    grright = 0.7
                   end
                ELSE:
            ENDCASE
            8: CASE subchoice OF
                0: begin
                    left = 0.15
                    right = 0.9
                    top = 0.91
                    bottom = 0.1
                    grtop = 0.8
                    grright = 0.70
                end
                1:
                2:              ;**** Not applicable ****
                3:              ;**** Not applicable ****
                ELSE:
            ENDCASE
            ELSE:
        ENDCASE
    ENDIF ELSE BEGIN

                ;***********************************
                ;**** All other TARGET_MACHINES ****
                ;***********************************

        CASE mainchoice OF
            0: CASE subchoice OF
                0: begin
                    left = 0.1
                    right = 0.9
                    top = 0.91
                    bottom = 0.1
                    grtop = 0.8
                    grright = 0.70
                end
                1: begin
                    left = 0.1
                    right = 0.9
                    top = 0.91
                    bottom = 0.1
                    grtop = 0.8
                    grright = 0.70
                end
                2: begin
                    left = 0.1
                    right = 0.9
                    top = 0.91
                    bottom = 0.1
                    grtop = 0.8
                    grright = 0.60
                end
                3:
                4: begin
                    left = 0.1
                    right = 0.9
                    top = 0.91
                    bottom = 0.1
                    grtop = 0.8
                    grright = 0.60
                end
                5: begin
                    left = 0.1
                    right = 0.9
                    top = 0.91
                    bottom = 0.1
                    grtop = 0.8
                    grright = 0.60
                end
                6:
                7: begin
                 left = 0.1
                 right = 0.9
                 top = 0.91
                 bottom = 0.1
                 grtop = 0.8
                 grright = 0.70
            end
                ELSE:
            ENDCASE
            1: CASE subchoice OF
                0: begin
                    left = 0.15
                    right = 0.9
                    top = 0.91
                    bottom = 0.1
                    grtop = 0.8
                    grright = 0.70
                end
                1:
                2: begin
                    left = 0.10
                    right = 0.9
                    top = 0.91
                    bottom = 0.1
                    grtop = 0.8
                    grright = 0.65
                end
                3:
                4: begin
                    left = 0.15
                    right = 0.9
                    top = 0.9
                    bottom = 0.1
                    grtop = 0.8
                    grright = 0.7
                end
                5: begin
                    left = 0.15
                    right = 0.9
                    top = 0.9
                    bottom = 0.1
                    grtop = 0.8
                    grright = 0.7
                end
                6: begin
                    left = 0.12
                    right = 0.9
                    top = 0.9
                    bottom = 0.1
                    grtop = 0.8
                    grright = 0.7
                end
                7: begin
                    left = 0.15
                    right = 0.9
                    top = 0.9
                    bottom = 0.1
                    grtop = 0.8
                    grright = 0.7
                end
                8:              ;**** Not applicable ****
                9:              ;**** Not applicable ****
                10:             ;**** Not applicable ****
                11:             ;**** Not applicable ****
                12:             ;**** Not applicable ****
                13: begin
                    left = 0.15
                    right = 0.9
                    top = 0.9
                    bottom = 0.1
                    grtop = 0.8
                    grright = 0.7
                end
                14: begin
                    left = 0.15
                    right = 0.9
                    top = 0.9
                    bottom = 0.1
                    grtop = 0.8
                    grright = 0.7
                end
                ELSE:
            ENDCASE
            2: CASE subchoice OF
                0: begin
                    left = 0.15
                    right = 0.9
                    top = 0.91
                    bottom = 0.1
                    grtop = 0.8
                    grright = 0.7
                end
                1: begin
                    left = 0.1
                    right = 0.9
                    top = 0.91
                    bottom = 0.1
                    grtop = 0.8
                    grright = 0.65
                end
                2: begin
                    left = 0.1
                    right = 0.9
                    top = 0.91
                    bottom = 0.1
                    grtop = 0.8
                    grright = 0.65
                end
                3: begin
                    left = 0.10
                    right = 0.70
                    top = 0.90
                    bottom = 0.12
                    grtop = 0.8
                    grright = 0.70
                end
                4:
                5: begin
                    left = 0.05
                    right = 0.34
                    top = 0.90
                    bottom = 0.12
                    grtop = 0.8
                    grright = 0.34
                end
                6:
                7: begin
                    left = 0.05
                    right = 0.34
                    top = 0.90
                    bottom = 0.12
                    grtop = 0.8
                    grright = 0.34
                end
                8:              ;**** Not applicable ****
                9:              ;**** Not applicable ****
                10:
                ELSE:
            ENDCASE
            3: CASE subchoice OF
                0: begin
                    left = 0.10
                    right = 0.85
                    top = 0.91
                    bottom = 0.1
                    grtop = 0.8
                    grright = 0.65
                end
                1: begin
                    left = 0.10
                    right = 0.85
                    top = 0.91
                    bottom = 0.1
                    grtop = 0.8
                    grright = 0.65
                end
                2:
                3:
                4: begin
                    left = 0.10
                    right = 0.70
                    top = 0.90
                    bottom = 0.12
                    grtop = 0.8
                    grright = 0.70
                end
                5: begin
                    left = 0.10
                    right = 0.70
                    top = 0.90
                    bottom = 0.12
                    grtop = 0.8
                    grright = 0.70
                end
                6: begin
                    left = 0.10
                    right = 0.70
                    top = 0.90
                    bottom = 0.12
                    grtop = 0.8
                    grright = 0.70
                end
                7:
                8: begin
                    left = 0.10
                    right = 0.70
                    top = 0.90
                    bottom = 0.12
                    grtop = 0.8
                    grright = 0.70
                end
                9: begin
                    left = 0.1
                    right = 0.9
                    top = 0.86
                    bottom = 0.1
                    grtop = 0.8
                    grright = 0.65
                end
                10: begin
                    left = 0.10
                    right = 0.70
                    top = 0.90
                    bottom = 0.12
                    grtop = 0.8
                    grright = 0.70
                end
                11: begin
                    left = 0.10
                    right = 0.70
                    top = 0.90
                    bottom = 0.12
                    grtop = 0.8
                    grright = 0.70
                end
                12: begin
                    left = 0.10
                    right = 0.70
                    top = 0.90
                    bottom = 0.12
                    grtop = 0.8
                    grright = 0.70
                end
                13: begin
                    left = 0.1
                    right = 0.9
                    top = 0.86
                    bottom = 0.1
                    grtop = 0.8
                    grright = 0.65
                end
                14: begin
                    left = 0.1
                    right = 0.9
                    top = 0.86
                    bottom = 0.1
                    grtop = 0.8
                    grright = 0.65
                end
                ELSE:
            ENDCASE
            4: CASE subchoice OF
                0: begin
                    left = 0.1
                    right = 0.9
                    top = 0.86
                    bottom = 0.1
                    grtop = 0.8
                    grright = 0.65
                end
                1: begin
                    left = 0.1
                    right = 0.9
                    top = 0.86
                    bottom = 0.1
                    grtop = 0.8
                    grright = 0.65
                end
                2: begin
                    left = 0.1
                    right = 0.9
                    top = 0.86
                    bottom = 0.1
                    grtop = 0.8
                    grright = 0.65
                end
                3: begin
                    left = 0.1
                    right = 0.9
                    top = 0.86
                    bottom = 0.1
                    grtop = 0.8
                    grright = 0.65
                end
                4: begin
                    left = 0.1
                    right = 0.9
                    top = 0.86
                    bottom = 0.1
                    grtop = 0.8
                    grright = 0.65
                end
                5: begin
                    left = 0.1
                    right = 0.9
                    top = 0.86
                    bottom = 0.1
                    grtop = 0.8
                    grright = 0.65
                end
                6: begin
                    left = 0.1
                    right = 0.9
                    top = 0.86
                    bottom = 0.1
                    grtop = 0.8
                    grright = 0.65
                end
                7:
                8: begin
                    left = 0.1
                    right = 0.9
                    top = 0.86
                    bottom = 0.1
                    grtop = 0.8
                    grright = 0.65
                end
                9: begin
                    left = 0.1
                    right = 0.9
                    top = 0.86
                    bottom = 0.1
                    grtop = 0.8
                    grright = 0.65
                end
                ELSE:
            ENDCASE
            5: CASE subchoice OF
                0: begin
                  left = 0.1
                  right = 0.9
                  top = 0.86
                  bottom = 0.1
                  grtop = 0.8
                  grright = 0.65
                end
            1: begin
                  left = 0.1
                  right = 0.9
                  top = 0.86
                  bottom = 0.1
                  grtop = 0.9
                  grright = 0.9
                end
            2: begin
              left = 0.1
              right = 0.9
              top = 0.86
              bottom = 0.1
              grtop = 0.9
              grright = 0.9
            end
                ELSE:
            ENDCASE
            6: CASE subchoice OF
                0: begin
                    left = 0.1
                    right = 0.9
                    top = 0.86
                    bottom = 0.1
                    grtop = 0.8
                    grright = 0.65
                   end
                1: begin
                    left = 0.1
                    right = 0.9
                    top = 0.86
                    bottom = 0.1
                    grtop = 0.9
                    grright = 0.9
                   end
                2: begin
                    left = 0.1
                    right = 0.9
                    top = 0.86
                    bottom = 0.1
                    grtop = 0.9
                    grright = 0.9
                   end
                ELSE:
            ENDCASE
            7: CASE subchoice OF
                0: begin
                    left = 0.1
                    right = 0.9
                    top = 0.86
                    bottom = 0.1
                    grtop = 0.8
                    grright = 0.65
                   end
                1: begin
                    left = 0.1
                    right = 0.9
                    top = 0.86
                    bottom = 0.1
                    grtop = 0.9
                    grright = 0.9
                   end
                2: begin
                    left = 0.1
                    right = 0.9
                    top = 0.86
                    bottom = 0.1
                    grtop = 0.9
                    grright = 0.9
                   end
                3: begin
                    left = 0.1
                    right = 0.9
                    top = 0.86
                    bottom = 0.1
                    grtop = 0.9
                    grright = 0.9
                   end
                4: begin
                    left = 0.1
                    right = 0.9
                    top = 0.86
                    bottom = 0.1
                    grtop = 0.9
                    grright = 0.9
                   end
                5: begin
                    left = 0.1
                    right = 0.9
                    top = 0.86
                    bottom = 0.1
                    grtop = 0.9
                    grright = 0.9
                   end
                6: begin
                    left = 0.1
                    right = 0.9
                    top = 0.86
                    bottom = 0.1
                    grtop = 0.8
                    grright = 0.65
                   end
                7: begin
                    left = 0.1
                    right = 0.9
                    top = 0.86
                    bottom = 0.1
                    grtop = 0.9
                    grright = 0.9
                   end
                8: begin
                    left = 0.15
                    right = 0.9
                    top = 0.91
                    bottom = 0.1
                    grtop = 0.78
                    grright = 0.85
                   end
                ELSE:
            ENDCASE
            8: CASE subchoice OF
                0: begin
                    left = 0.15
                    right = 0.9
                    top = 0.91
                    bottom = 0.1
                    grtop = 0.8
                    grright = 0.70
                end
                1:
                2:              ;**** Not applicable ****
                3:              ;**** Not applicable ****
                ELSE:
            ENDCASE

            ELSE:
        ENDCASE
    ENDELSE

END
