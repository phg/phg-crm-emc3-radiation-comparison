; Copyright (c) 1999, Strathclyde University .
; SCCS INFO : Module @(#)$Header: /home/adascvs/idl/adaslib/system/adasplot.pro,v 1.2 2004/07/06 11:15:05 whitefor Exp $	Date $Date: 2004/07/06 11:15:05 $
;
;+
; PROJECT:
;       IDL ADAS
;
; NAME:
;	ADASPLOT
;
; PURPOSE:
;	Plot one graph for ADAS.
;
; EXPLANATION:
;	TO BE DOCUMENTED
;
;
; MODIFIED:
; 	1.1	Richard Martin
;		First Release
;	1.2	Richard Martin
;		Added support for 24-bit colour.
;
; VERSION:
;	1.1	15-10-99
;	1.2	09-01-02
;
;-
;----------------------------------------------------------------------------

PRO adasplot, x , y, itval, nplots, nmx, title, xtitle, xuindx,xunits, ytitle, 		$
             strg, head1, head2a,head2b, x1val, x2val, 			$
             ldef, xmin, xmax, ymin, ymax, lfsel, xlog=xlog, ylog=ylog,$
             nodata=nodata, multilabels=multilabels

    COMMON Global_lw_data, left, right, top, bottom, grtop, grright
    
    IF NOT (KEYWORD_SET(nodata)) THEN nodata = 0 ELSE nodata=1
    IF NOT (KEYWORD_SET(multilabels)) THEN  multilabels= 0 ELSE multilabels=1   

	;*************************************
	;**** Set up colour plot plotting ****
	;*************************************	

    if !d.name eq 'X' then begin
    	 TVLCT, red, green, blue, /get
    	  
    	 device,get_visual_depth=depth
    	  
    	 if depth eq 24 then begin
    	 	   colour = { black   : red(0)+256L*(green(0)+256L*blue(0)),	 $
    	 			    white	: red(1)+256L*(green(1)+256L*blue(1)),	 $
    	 			    red	: red(2)+256L*(green(2)+256L*blue(2)),	 $
    	 			    green	: red(3)+256L*(green(3)+256L*blue(3)),	 $
    	 			    blue	: red(4)+256L*(green(4)+256L*blue(4)),	 $
    	 			    yellow  : red(5)+256L*(green(5)+256L*blue(5)),	 $
    	 			    magenta : red(6)+256L*(green(6)+256L*blue(6)),	 $
    	 			    cyan	: red(7)+256L*(green(7)+256L*blue(7))	   }
    	 endif else begin
    	 	   colour = { black   : 0, $
    	 			    white	: 1,       $
    	 			    red	: 2,       $
    	 			    green	: 3,       $
    	 			    blue	: 4,       $
    	 			    yellow  : 5,       $
    	 			    magenta : 6,       $
    	 			    cyan	: 7		 }
    	 endelse  
     endif  else begin
     	 	   colour = { black   : 0, $
    	 			    white	: 1,       $
    	 			    red	: 2,       $
    	 			    green	: 3,       $
    	 			    blue	: 4,       $
    	 			    yellow  : 5,       $
    	 			    magenta : 6,       $
    	 			    cyan	: 7		 }    
     endelse                                               
            ;****************************************************
		;**** Suitable character size for current device ****
		;**** Aim for 60 characters in y direction.      ****
   		;*****************************************************

    charsize = (!d.y_vsize/!d.y_ch_size)/60.0 
    small_check = GETENV('VERY_SMALL')
    if small_check eq 'YES' then charsize=charsize*0.8

		;**** set makeplot counter ****

    makeplot = 1

            ;***************************************
		;**** Construct graph title         ****
		;**** "!C" is the new line control. ****
            ;***************************************
		
    if xuindx eq 0 then xunit=xunits(0,0)
    if xuindx eq 1 then xunit=xunits(0,1)
    if xuindx eq 2 then xunit=xunits(1,0)
    if xuindx eq 3 then xunit=xunits(1,1)   

    !p.font = -1
    if small_check eq 'YES' then begin
      if lfsel eq 1 then begin
        gtitle =  title(0)+ xunit + "!C!C" + title(1)
        if NOT multilabels then 	$
        	gtitle = gtitle  + "!C!C"+title(2)+"!C!C" + title(3) + "!C!C" + title(4)
      endif else begin
        gtitle =  title(0)+ xunit + "!C!C" + title(1) + "!C!C"+title(2)+"!C!C" + $
            strmid(title(4),0,57)       
      endelse    
    endif else begin
      if lfsel eq 1 then begin    
        gtitle =  title(0)+ xunit + "!C!C" + title(1)
        if NOT multilabels then 	$
        	gtitle = gtitle + "!C" + title(2) + "!C" + title(3) + "!C" + title(4)          
      endif else begin
        gtitle =  title(0)+ xunit + "!C!C" + title(1) + "!C"+title(2)+"!C" + $
          strmid(title(4),0,57)              
      endelse
    endelse

		;**** Find x and y ranges for auto scaling ****

    if ldef eq 0 then begin

                ;********************************************
		;**** identify values in the valid range ****
		;**** plot routines only work within     ****
		;**** single precision limits.	         ****
                ;********************************************

    	xvals = where(x gt 1.0e-37 and x lt 1.0e37)
    	yvals = where(y gt 1.0e-37 and y lt 1.0e37)
    	if xvals(0) gt -1 then begin
            xmax = max(x(xvals), min=xmin)
            makeplot = 1
    	endif else begin
            makeplot = 0
    	endelse
    	if yvals(0) gt -1 then begin
            ymax = max(y(yvals), min=ymin)
            makeplot = 1
    	endif else begin
            makeplot = 0
    	endelse
        style = 0
    endif else begin

		;Check that at least some data in in axes range ***

    	xvals = where(x ge xmin and x le xmax)
    	yvals = where(y ge ymin and y le ymax)
    	if xvals(0) eq -1 or yvals(0) eq -1 then begin
      	    makeplot = 0
    	endif
    	style = 1
    endelse
    
		;**** Set up log-log plotting axes ****
    
    dxtitle=xtitle+xunit
    plot, [xmin,xmax], [ymin,ymax], /nodata, ticklen=1.0, 		$
	     position=[left,bottom,grright,grtop], 			$
	     xtitle=dxtitle, ytitle=ytitle, xstyle=style, ystyle=style, 	$
	     charsize=charsize, xlog=xlog, ylog=ylog, color=colour.white

    if makeplot eq 1  then begin

		;**********************
		;****  Make plots  ****
		;**********************
      if nodata eq 0 then begin
        oplot, x(0,0:itval-1), y(0,0:itval-1), psym=-1, color=colour.white
        
        if (lfsel eq 1) then begin
            oplot, x(1,*), y(1,*), linestyle=5, color=colour.white
        endif
      endif 
    endif else begin
        print, "ADAS503 : No data found in these axes ranges"
    endelse

		;**** Annotation to plot ****

    rhs = grright + 0.02

    if small_check eq 'YES' then begin
	charsize = charsize * 0.9
    endif
		;**** plot title ****

    xyouts, (left-0.05), (top+0.05), gtitle, /normal, alignment=0.0, 	$
	    charsize = charsize, color=colour.white

		;**** right hand side labels ****
		
    if multilabels eq 0 then begin
       cpp = grtop - 0.05
       xyouts, rhs, cpp, head1, /normal, alignment=0.0, charsize=charsize*0.8, color=colour.white
       cpp = cpp - 0.02
       for i = 0, 6 do begin
           cpp = cpp - 0.02
           xyouts, rhs, cpp, strg(i), /normal,  			   $
           alignment= 0.0, charsize=charsize*0.8, color=colour.white
       endfor
       cpp = cpp - 0.05
       xyouts, rhs, cpp, head2a, /normal, charsize=charsize*0.8, color=colour.white
       cpp = cpp - 0.04
       xyouts, rhs, cpp, head2b, /normal, charsize=charsize*0.8, color=colour.white
       for i = 0, itval-1 do begin
           index  = strcompress(string(i+1))
           if (i le 8) then line=index + "       " else line=index + "      "
           line = line +						   $
           strcompress(string(x1val(i), format='(e10.3)')) + "  	 " +$
           strcompress(string(x2val(i),  format='(e10.3)'))
           cpp = cpp - 0.02
           xyouts, rhs, cpp, line, /normal, charsize=charsize*0.8, color=colour.white
       endfor
    endif
    
END
