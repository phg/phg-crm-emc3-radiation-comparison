; Copyright (c) 2000, Strathclyde University.
;+
; PROJECT:
;       ADAS
;
; NAME:
;       ADASLOADCT
;
; PURPOSE:
;
;       To set up a simple colour table for colour plot.
;       a colour table is loaded in (colour table 40) and
;       first eight colours are re-defined with their indices
;       stored in a structure.
;
;         For a 24-bit or 16-bit display, the structure is:
;
;                  colour = { black   : red(0)+256L*(green(0)+256L*blue(0)),   $
;                             white   : red(1)+256L*(green(1)+256L*blue(1)),   $
;                             red     : red(2)+256L*(green(2)+256L*blue(2)),   $
;                             green   : red(3)+256L*(green(3)+256L*blue(3)),   $
;                             blue    : red(4)+256L*(green(4)+256L*blue(4)),   $
;                             yellow  : red(5)+256L*(green(5)+256L*blue(5)),   $
;                             magenta : red(6)+256L*(green(6)+256L*blue(6)),   $
;                             cyan    : red(7)+256L*(green(7)+256L*blue(7))    }
;
;         For an 8-bit display (and the Postscript Device), the indices are:
;
;                  colour = { black   : 0, $
;                             white   : 1, $
;                             red     : 2, $
;                             green   : 3, $
;                             blue    : 4, $
;                             yellow  : 5, $
;                             magenta : 6, $
;                             cyan    : 7  }
;
;         The colours are therefore reference by structure name
;         e.g. COLOR = colour.white ...
;
; MODIFIED:
;       1.1     Richard Martin
;                 First release
;       1.2     Richard Martin
;                 Modifies to include support for 24-bit displays.
;       1.3     Alessandro Lanzafame
;                 Added 16-bit displays.
;       1.4     Martin O'Mullane
;                 Ensure decomposed mode is on for 24/16-bit colour.
;
; VERSION:
;       1.1     18-09-2000
;       1.2     12-12-2002
;       1.3     01-03-2005
;       1.4     01-07-2017
;
;-
;-----------------------------------------------------------------------------

pro adasloadct, colour=colour

loadct, 40
red   = [ 0, 255,255,  0,  0,255,255,  0]
green = [ 0, 255,  0,255,  0,255,  0,255]
blue  = [ 0, 255,  0,  0,255,  0,255,255]

TVLCT, red, green, blue

device, get_visual_depth=depth

if depth eq 24 or depth eq 16 then begin
          
          device, decomposed=1
          
          colour = { black   : red[0]+256L*(green[0]+256L*blue[0]),     $
                     white   : red[1]+256L*(green[1]+256L*blue[1]),     $
                     red     : red[2]+256L*(green[2]+256L*blue[2]),     $
                     green   : red[3]+256L*(green[3]+256L*blue[3]),     $
                     blue    : red[4]+256L*(green[4]+256L*blue[4]),     $
                     yellow  : red[5]+256L*(green[5]+256L*blue[5]),     $
                     magenta : red[6]+256L*(green[6]+256L*blue[6]),     $
                     cyan    : red[7]+256L*(green[7]+256L*blue[7])      }
endif else begin
          colour = { black   : 0,       $
                     white   : 1,       $
                     red     : 2,       $
                     green   : 3,       $
                     blue    : 4,       $
                     yellow  : 5,       $
                     magenta : 6,       $
                     cyan    : 7        }
endelse

end
