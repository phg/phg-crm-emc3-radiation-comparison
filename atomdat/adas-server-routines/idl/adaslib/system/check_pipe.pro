; Copyright (c) 1996, Strathclyde University.
; SCCS info: Module @(#)$Header: /home/adascvs/idl/adaslib/system/check_pipe.pro,v 1.1 2004/07/06 12:04:46 whitefor Exp $ Date $Date: 2004/07/06 12:04:46 $
;+
; PROJECT:
;       ADAS IBM MVS to UNIX conversion
;
; NAME:
;	CHECK_PIPE
;
; PURPOSE: Diagnostic check on the state of the FORTRAN-IDL pipe.
;
; EXPLANATION:
;  	The equivalent FORTRAN check_pipe procedure must be put in
;       the FORTRAN code to communicate with this fortran subroutine.
;       Both routines write a string, read a string and then check
;       whether that string is as expected. A message is then
;       output indicating whether the pipe was empty before the calls
;       were made, so telling the user whether pipe communications
;       between the IDL and FORTRAN had been successful up to that
;       point.
;
;       Rather than giving the 'error' message, the FORTRAN may crash
;       indicating that the IDL has written too much data to the pipe
;       before this call and that the data was not a string.
;
;       If the FORTRAN or IDL hangs in this routine then there is a
;       major (>128 character) blockage in the pipe.
;
; USE:
;	A FORTRAN process must have been spawned so that the pipe unit
;	can be passed to this routine.
;
; INPUTS:
;	PIPE - Long integer; the unit for pipe I/O.
;
; OUTPUTS:
;	None
;
; CATEGORY:
;	Adas system.
;	
; WRITTEN:
;       William Osborn, Tessella Support Services plc, 05/07/1996
;
; MODIFIED:
;	1.1	William Osborn	
;		First release
;
; VERSION:
;       1.1	05-07-96
;			
PRO check_pipe, pipe

    print, 'Pipe check (IDL), writing.'
    a = "I know I've made some very poor decisions recently, but I "+	$
     "can give you my complete assurance that my work will be back "+	$
     "to normal."
    printf, pipe, a, format = '(A)'
    b = ''
    readf, pipe, b
    if b eq a then print, 'Check_pipe (IDL): OK'			$
    else print, 'Check_pipe (IDL): Error'

END
