; Copyright (c) 1995 Strathclyde University.
; SCCS info: Module @(#)$Header: /home/adascvs/idl/adaslib/system/find_process.pro,v 1.17 2004/07/06 13:54:10 whitefor Exp $ Date $Date: 2004/07/06 13:54:10 $
;+
; PROJECT:
;       ADAS IBM MVS to DEC UNIX conversion
;
; NAME:
;	FIND_PROCESS()
;
; PURPOSE:
;	To check to see if a UNIX process is still active.
;
; EXPLANATION:
;	The input to this function is the Process Identification number,
;	PID, of a UNIX process.  The routine returns 1 if the process is
;	still active and 0 if it is not.  The routine works by spawning
;	a UNIX command to search for the process.  For example for a 
;	process PID=4593 the routine issues the command 'ps -4593'.  The
;	stdout output from this command is read by the routine to
;	determine whether or not the process is active.
;
;	Note: the syntax of the UNIX command and the output which is
;	returned may vary from one operating system to another.  The
;	operating system is checked and specific code executed for that
;	system.  Currently the routine has specific code for;
;
;		ULTRIX	'ps -123'
;		SUNOS	'ps -123'
;		OSF	'ps -p123'
;	   	SOLARIS 'ps -p123'
;		HP-UX   'ps -p123'
;		AIX	'ps -p123'
;		All others (same as UNIX code)
;
;	The UNIX specific code simply checks the number of lines in
;	the output from the spawned 'ps -#PID' command.  If the process
;	is still active two lines of output are returned, a header line
;	and a line describing the process.  If the process is not active
;	only the header line is output.
;
; USE:
;	When a process is spawned from IDL the process identification
;	number can be recorded.  Later the IDL program can use find_process()
;	to see if the process is still active.  For example;
;
;		spawn,'/usr/fred/a.out',/noshell,PID=pid,UNIT=pipe
;
;		act = find_process(pid)
;		if act eq 0 then begin
;		  print,'Spawned process is no longer active'
;		end else begin
;		  print,'Spawned process is still active'
;		end
;
; INPUTS:
;	PID	- Integer; The process identification number of the
;		  UNIX process to be searched for.
;
; OPTIONAL INPUTS:
;	None
;
; OUTPUTS:
;	This function returns 1 if the process exists, or 0 if is does
;	not exist.
;
; OPTIONAL OUTPUTS:
;	None
;
; KEYWORD PARAMETERS:
;	None
;
; CALLS:
;	None
;
; SIDE EFFECTS:
;	This routine 'Spawns' a unix command.
;
; CATEGORY:
;	UNIX system IDL utility.
;
; WRITTEN:
;       Andrew Bowen, Tessella Support Services plc, 4-May-1993
;
; MODIFIED:
;       1.1 	Andrew Bowen  
;               First release.
;       1.2 	Andrew Bowen  
;		Code added to make spawned unix commands OS sensitive.
;       1.3 	Andrew Bowen  
;		Header comments added.
;       1.4 	Andrew Bowen  
;		OSF specific spawn added.
;       1.5 	Andrew Bowen  
;		SUNOS specific code added.
;	1.6	Lalit Jalota
;	1.7	Lalit Jalota
;	1.8	Lalit Jalota
;		Added /SH keyword to spawn call to stop any user
;		aliases being used
;	1.9	Lalit Jalota	
;	1.10	Lalit Jalota
;		Inserted check to differentiate between Sunos and
;		Solaris which are both called Sunos in the system 
;		variable !version.os but which have different syntax
;		for ps call.
;	1.11	Tim Hammond	
;		Inserted check for HP-UX operating system.
;	1.12	Tim Hammond
;		Inserted check for AIX operating system and tidied code.
;	1.13	William Osborn
;		Added check for <defunct> processes
;	1.14	William Osborn
;		Changed sunos code so that output(1) is only accessed
;		if output has two members
;	1.15	William Osborn
;		Changed sunos code so that found is always set
;	1.16	Martin O'Mullane
;		Add linux specific code.
;	1.17	Richard Martin
;		Made the default the same as the Linux version.
;
; VERSION:
;       1.1 	07-05-93
;	1.2	24-05-93
;	1.3	26-05-93
;	1.4	08-06-93
;	1.5	11-06-93
;	1.6	13-03-95
;	1.7	13-03-95
;	1.8	13-03-95
;	1.9	28-03-95
;	1.10	28-03-95
;	1.11	22-06-95
;	1.12	02-04-96
;	1.13	23-08-96
;	1.14	21-11-96
;	1.15	21-11-96
;	1.16	07-09-99
;	1.17	27-03-03
;
;-
;----------------------------------------------------------------------------

FUNCTION find_process,pid

		;******************************
		;**** Look for the process ****
		;******************************

    if !version.os eq 'ultrix' then begin

		;**** UNIX version ****

        spawn, 'ps -' + strtrim(string(pid),2),output,/sh

		;**** One line of output if the process does not exist.  ****
		;**** Also return 0 if the process is defunct. ****

        outsize = size(output)
        if outsize(1) eq 1 or strpos(output(1),'<defunct>') ne -1 then $
		found = 0 else found = 1
    endif else if !version.os eq 'OSF' then begin

		;**** OSF version ****

        spawn, 'ps -p' + strtrim(string(pid),2),output,/sh

		;**** One line of output if the process does not exist.  ****
		;**** Also return 0 if the process is defunct. ****

        outsize = size(output)
        if outsize(1) eq 1 or strpos(output(1),'<defunct>') ne -1 then $
		found = 0 else found = 1
    endif else if !version.os eq 'hp-ux' then begin

		;*** HP-UX version ***

        spawn, 'ps -p' + strtrim(string(pid),2),output,/sh

                ;**** One line of output if the process does not exist.  ****
		;**** Also return 0 if the process is defunct. ****

        outsize = size(output)
        if outsize(1) eq 1 or strpos(output(1),'<defunct>') ne -1 then $
		found = 0 else found = 1

    endif else if !version.os eq 'AIX' then begin

                ;**** AIX version ****

        spawn, 'ps -p' + strtrim(string(pid),2),output,/sh

                ;**** One line of output if the process does not exist.  ****
		;**** Also return 0 if the process is defunct. ****

        outsize = size(output)
        if outsize(1) eq 1 or strpos(output(1),'<defunct>') ne -1 then $
		found = 0 else found = 1

    endif else if !version.os eq 'linux' then begin

                ;**** Linux version ****

        spawn, 'ps -p' + strtrim(string(pid),2),output,/sh

                ;**** One line of output if the process does not exist.  ****
		;**** Also return 0 if the process is defunct. ****

        outsize = size(output)
        if outsize(1) eq 1 or strpos(output(1),'<defunct>') ne -1 then $
		found = 0 else found = 1
                
    endif else if !version.os eq 'sunos' then begin

		;*** get os version number ***

        spawn, "uname -r", release, /sh
        release = float(release(0))
        if (release gt 5.0) then begin
	    command = 'ps -p'
        endif else begin
	    command = 'ps -'
        endelse 
        spawn, command + strtrim(string(pid),2),output,/sh

                ;**** Two lines of output if the process does exist.  ****
		;**** Also return 0 if the process is defunct. ****

        outsize = size(output)
        if outsize(1) eq 2 then begin
            if strpos(output(1),'<defunct>') eq -1 then $
              found = 1 else found = 0
        endif else found = 0
    endif else begin

		;**** Other version (same as Linux) ****

        spawn, 'ps -p' + strtrim(string(pid),2),output,/sh

		;**** One line of output if the process does not exist.  ****
		;**** Also return 0 if the process is defunct. ****

        outsize = size(output)
        if outsize(1) eq 1 or strpos(output(1),'<defunct>') ne -1 then $
		found = 0 else found = 1
    endelse
		
    RETURN, found

END
