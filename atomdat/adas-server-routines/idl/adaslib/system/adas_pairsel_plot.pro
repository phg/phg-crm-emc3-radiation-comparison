; Copyright (c) 1999, Strathclyde University.
; SCCS info: Module @(#)$Header: /home/adascvs/idl/adaslib/system/adas_pairsel_plot.pro,v 1.3 2004/07/06 11:12:10 whitefor Exp $ Date $Date: 2004/07/06 11:12:10 $
;+
; PROJECT:
;       ADAS 
;
; NAME:
;	ADAS_PAIRSEL_PLOT	
;
;
; EXPLANATION:
;	TO BE DOCUMENTED
;
; MODIFIED:
;	1.1	Richard Martin
;		First release 
;	1.2	Richard Martin
;		Introduced set_shading command to set upper limit of colours
;		required in shade_surf to !d.n_colors . This is to try to
;		stop IDL using a private colour map.
;	1.3	Richard Martin
;		Added support for 24-bit colour.
;
; VERSION:
;	1.1	15-10-99
;	1.2	18-09-2000
;	1.3	14-12-01
;
;-
;-----------------------------------------------------------------------------

PRO adas_pairsel_plot, errid, stats, VALUE=value, FONT=font, PRINT=print

   IF NOT (KEYWORD_SET(font))  THEN font = ''
   IF NOT (KEYWORD_SET(value)) THEN $  
           message,'VALUE structure must be passed to ADAS_PAIRSEL_PLOT'

	;************************
	;**** Set up colours ****
	;************************
		
  TVLCT, red, green, blue, /get

  device,get_visual_depth=depth

  if depth eq 24 then begin
  	  colour = { black   : red(0)+256L*(green(0)+256L*blue(0)),   $
  			 white   : red(1)+256L*(green(1)+256L*blue(1)),   $
  			 red     : red(2)+256L*(green(2)+256L*blue(2)),   $	  
  			 green   : red(3)+256L*(green(3)+256L*blue(3)),   $
  			 blue    : red(4)+256L*(green(4)+256L*blue(4)),   $
  			 yellow  : red(5)+256L*(green(5)+256L*blue(5)),   $
  			 magenta : red(6)+256L*(green(6)+256L*blue(6)),   $
  			 cyan    : red(7)+256L*(green(7)+256L*blue(7))      }
  endif else begin
  	  colour = { black   : 0, $
  			 white   : 1, $
  			 red     : 2, $			 
  			 green   : 3, $
  			 blue    : 4, $
  			 yellow  : 5, $
  			 magenta : 6, $
  			 cyan    : 7    }
  endelse

   if (keyword_set(print)) then begin
     
     grtype = print.devcode( where(print.dev eq print.devlist) )
     set_plot,grtype
     
     if print.paper eq 'A4' then begin
        xsize = 15.0
        ysize = 16.0
     endif else begin
        xsize = 15.0
        ysize = 16.0
     endelse
     
     ; unlike 2D plots we need to use Hershy fonts.
     device,/color,bits=8,filename=print.dsn,  	   $
             font_size=12, /portrait,              $
             xsize=xsize, ysize=ysize
   endif 

   ; Trap any errors from the fitting and report back to the user.
   ; Hopefully it will not crash the program as we jump to the end
   ; in the case of an error.

   catch, error_status
   
   if error_status ne 0 then begin
      print,'An error occurred in ADAS_PAIRSEL_PLOT'
      print, !err_string 
      widget_control, errid, set_value = 'Possible mathematical error'
      goto,LABELend
   endif
   
   
   ; get data from value structure
   
   
   x       	= value.x
   y      	= value.y
   z		= value.z
   nx		= value.nx
   ny		= value.ny   
   title    = value.title
   subtitle = value.subtitle
   
   
   ax       = value.ax
   az       = value.az
;   colour   = value.colour
   style    = value.style   
   xlog  	= value.xlog
   ylog	= value.ylog
   
   numte  = n_elements(te)
   numden = n_elements(den)
    
   ; Leave space for a title and footer
  
   !y.omargin=[1,1]



   ; what are we plotting and global options for the graph

   xtitle = 'T!De!N' 

   ytitle ='n!De!N' 
 
;   if ( x(nx-1)/x(0) ge 1e2) then xlog=1
;   if ( y(ny-1)/y(0) ge 1e2) then ylog=1
   
      
   ; now do the plotting
  
  set_shading,values=[8,!d.n_colors]
  if style eq 0 then begin
   	surface, z(0:nx-1,0:ny-1), x(0:nx-1), y(0:ny-1), xlog=xlog, ylog=ylog, $
      	xtitle=xtitle, ytitle=ytitle, ax=ax, az=az, xstyle=1,ystyle=1,	 $
      	charsize=1.1,min_value=0.0, max_value=2.0,/save,color=colour.white
      
   endif else begin
      shade_surf, z(0:nx-1,0:ny-1), x(0:nx-1), y(0:ny-1), xlog=xlog, ylog=ylog,	$
      	xtitle=xtitle, ytitle=ytitle, ax=ax, az=az, xstyle=1,ystyle=1, 		$
      	charsize=1.1, min_value=0.0, max_value=2.0,/save
   endelse   

   widget_control, value.win2, get_value = draw_win
   wset,  draw_win
   !p.region=[0.0,0.0,1.0,1.0]

   surface, z(0:nx-1,0:ny-1), x(0:nx-1), y(0:ny-1), xlog=xlog, ylog=ylog,		$
   	zstyle=4, xtitle=xtitle, ytitle=ytitle, ax=90, az=0, xstyle=1,ystyle=1,	$
   	charsize=1.1, min_value=0.0, max_value=2.0,color=colour.white
	
   widget_control, value.win1, get_value = draw_win
   wset,  draw_win   
   !p.region=[0.0,0.0,1.0,1.0]
   
   ; display 'useful' information
   
   xyouts, 0.5, 0.95, align=0.5, title, /normal, charsize=1.1,color=colour.white
   xyouts, 0.5, 0.91, align=0.5, subtitle, /normal, charsize=0.9,color=colour.white
   
  
LABELend:


   ; reset graphical parameters
   
   !p.multi = [0,1,1,0,0]               
   !y.omargin=[0, 0]


   if (keyword_set(print)) then begin
     device,/close
     set_plot,'X'
   endif

END
