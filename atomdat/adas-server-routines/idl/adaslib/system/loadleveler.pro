; Copyright (c) 2003, Strathclyde University.
; SCCS info: Module @(#)$Header: /home/adascvs/idl/adaslib/system/loadleveler.pro,v 1.2 2018/10/17 10:19:32 mog Exp $ Date $Date: 2018/10/17 10:19:32 $
;+
; PROJECT:
;       ADAS
; 
; NAME:
;	loadleveler
;
; PURPOSE:
;	Starts a batch job running on the batch queue system at JET.
;       The routine uses the LoadLeveler submit command.
;
; NOTES:
;       Extact random number identifer of input and control files from
;       jobname. Read information in control file to extract the input
;       file and executable  file names. Construct a LoadLeveler command
;       file -- command####.cmd  (dsll) and a script file script####.tmp
;       (dsscript). Note #### is rand. The routine returns the command
;       to execute and the message to pop up on screen.
;
;       
; WRITTEN:
;       Martin O'Mullane
;
; MODIFIED:
;	1.1	Martin O'Mullane    
;		 - First release
;       1.2     Martin O'Mullane
;                - Replace obsolete str_sep with strsplit.
;
; VERSION:
;       1.1	20-06-2003
;       1.2     17-10-2018
;
;-----------------------------------------------------------------------------
PRO LOADLEVELER_EVENT, event


Widget_Control, event.top, Get_UValue=info


case event.id of

  info.any_machineID : begin
                         
                          widget_control, info.any_machineID, get_value=tmp
                          help, tmp
                          print, tmp
                          if tmp[0] EQ 0 then begin
                             widget_control, info.machine_nameID, sensitive=1
                          endif else begin
                             widget_control, info.machine_nameID, sensitive=0
                          endelse
                       
                       end
  info.acceptID      : begin

                         widget_control, info.any_machineID, get_value=tmp 
                         widget_control, info.machine_nameID, get_value=name
                         widget_control, info.logfileID, get_value=logfile
                         widget_control, info.notify_userID, get_value=notify
                         
                         formdata = {any      : tmp[0],     $
                                     name     : name[0],    $
                                     logfile  : logfile[0], $
                                     notify   : notify[0],  $
                                     cancel   : 0           }
                          *info.ptrToFormData = formdata
                          widget_control, event.top, /destroy
                       
                       end
                       
   else     : message, 'Should not react to anything else!', /continue

endcase

END
;-----------------------------------------------------------------------------




FUNCTION loadleveler, input_command, command,  jobname=jobname


; Extract the random number from the jobname
     
     rand=strmid(jobname,4,strlen(jobname))

     dscontrol = 'control'+rand+'.tmp'
     dsscript  = 'script'+rand+'.tmp'
     dsll      = 'command'+rand+'.cmd'


; Read info and adasXXX executable files from control####.tmp.
; ADAS801 does not follow this scheme as there is just the
; executable. 

     openr, conunit, dscontrol, /GET_LUN
     res1 = ''
     readf, conunit, res1
     free_lun,conunit
     
     if strpos(res1, '<') NE -1 then begin
     
        res2 = strcompress(res1)
        res3 = strsplit(res2,'<', /extract)
        execfile = res3[0]
        dsinfo   = res3[1]
     
     endif else begin
     
        execfile = input_command
        dsinfo   = ''
        
     endelse
	
        
; Find relevant environment variables

     spawn,'pwd', whereami
     spawn,'domainname',dom_name
     spawn,'whoami',user_name

     font_small = getenv('ADASFNTSM')
     adas_c     = getenv("ADASCENT")
     adas_u     = getenv("ADASUSER")
     fortdir    = getenv("ADASFORT")


; Construct the script file to run adasXXX and to clean up afterwards
     
     loc_ksh = file_which(getenv('PATH'), 'ksh')
     
     openw, conunit, dsscript, /GET_LUN

     printf, conunit, '#!' + loc_ksh
     printf, conunit, '	'
     printf, conunit, 'export ADASUSER='+adas_u
     printf, conunit, 'export ADASCENT='+adas_c
     printf, conunit, '	'
     if dsinfo NE '' then printf, conunit, execfile+' < '+dsinfo  $
                     else printf, conunit, execfile
     printf, conunit, '	'
     printf, conunit, 'rm -f '+dsll
     printf, conunit, 'rm -f '+dsscript	
     printf, conunit, 'rm -f '+dscontrol
     if dsinfo NE '' then printf, conunit, 'rm -f '+dsinfo

     free_lun,conunit

     spawn, 'chmod u+x '+dsscript



; Top level base widget
     
     parent = widget_base(Column=1, Title='LoadLeveler Submit', $
                          xoffset=50, yoffset=1)

     rc     = widget_label(parent,value='  ',font=font_small)
     col    = widget_base(parent, /column)


; Elements
     
     any_machineID  = cw_bgroup(col, [' '],                           $     
                                /nonexclusive,                        $     
                                label_left = 'Any JAC computer',      $     
                                Uvalue='ANY',font=font_small)
                             
     machine_nameID = cw_field(col, value='',                         $
                               title='Machine  : ',                   $
                               fieldfont=font_small,                  $
                               font=font_small,                       $
                               Uvalue = 'MACHINE', /string)
     
     logfileID      = cw_field(col, value='',                         $
                               title='Log file : ',                   $
                               fieldfont=font_small,                  $
                               font=font_small,                       $
                               Uvalue = 'LOGFILE', /string)
     
     notify_userID  = cw_bgroup(col,['Notify on completion'],         $
                               exclusive=1,column=1,                  $
                               label_left = ' ',                      $
                               event_funct = 'ADAS_PROC_NULL_EVENTS', $
                               /no_release,  Uvalue='NOTIFY',         $
                               font=font_small)
     
     skip = widget_label(col,value=' ')
     info = widget_label(col,value='Type llq for queue usage', font=font_small)
     skip = widget_label(col,value=' ')
     
     acceptID = widget_button(col, value='Accept',UValue='Accept', $
                              font=font_small)

; Initialise to any JAC

     widget_control, any_machineID, set_value = 1
     widget_control, machine_nameID, sensitive = 0


; Realize the widget and create pointer to form data

     widget_control, parent, /realize

     ptrToFormData = Ptr_New({cancel:1, menu:0})

     info = { any_machineID   :   any_machineID,     $
              machine_nameID  :   machine_nameID,    $
              logfileID       :   logfileID,         $
              notify_userID   :   notify_userID,     $
              acceptID        :   acceptID,          $
              ptrToFormData   :   ptrToFormData      }

  widget_control,parent, Set_UValue=info

  XManager, 'LOADLEVELER', parent, Event_Handler='LOADLEVELER_EVENT'

 
; Back from event handling

  formdata = *ptrToFormData


  if formdata.any    EQ 1 then any_jac     = 'YES' else any_jac     = 'NO'
  if formdata.notify EQ 1 then notify_user = 'YES' else notify_user = 'NO'
  
  if any_jac EQ 'NO' then machine_name = formdata.name $
                     else machine_name = 'a system allocated JAC'

  if formdata.logfile EQ '' then log = '/dev/null' $
                            else log = formdata.logfile

; construct the LoadLeveler command file

     openw, conunit, dsll, /GET_LUN

     printf, conunit, '# @ executable = '+ dsscript
     printf, conunit, '# @ output = ' + log
     printf, conunit, '# @ error = ' + log
     printf, conunit, '# @ initialdir = '+whereami
     if notify_user eq 'YES' then printf, conunit, '# @ notify_user = '+user_name
     printf, conunit, '# @ notification = complete'
     if any_jac EQ 'NO' then begin
        printf, conunit, '# @ requirements = (Machine == "'+machine_name+'")'
     endif
     printf, conunit, '# @ queue'

     free_lun,conunit


; Construct command required to submit the job
     
     command = 'llsubmit  ' + dsll


; Return info message

     infolab = 'Your job has been submitted to LoadLeveler on ' + machine_name

     return, infolab
     
END
