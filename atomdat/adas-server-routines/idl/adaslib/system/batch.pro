; Copyright (c) 1996, Strathclyde University.
; SCCS info: Module @(#)$Header: /home/adascvs/idl/adaslib/system/batch.pro,v 1.6 2004/07/06 11:35:43 whitefor Exp $ Date $Date: 2004/07/06 11:35:43 $
;+
; PROJECT:
;       ADAS IBM MVS to UNIX conversion
;
; NAME:
;	BATCH
;
; PURPOSE:
;	Starts a batch job running according to the operating system
;   in use.
;
; EXPLANATION:
;   This routine will set a batch job going on a number of different
;   flavours of UNIX, currently OSF1, SunOs v4.x and 5.x, Ultrix and AIX.
;
; USE:
;   batch, command, time=time
;   This will run 'command' at time 'time'. If time is not specified
;   then the job will start after 2 minutes.
;
; INPUTS:
;   COMMAND  String; a UNIX command.
;
; OPTIONAL INPUTS:
;	None
;
; OUTPUTS:
;	None - Note this routine should leave all inputs un-modified.
;
; OPTIONAL OUTPUTS:
;	None
;
; KEYWORD PARAMETERS:
;   TIME    Integer; the number of minutes after which the job will start
;
;   TITLE   String; title for the information widget
;
;   JOBNAME String; Used in the label on the information widget if supplied.
;
; CALLS:
;
;
; SIDE EFFECTS:
;	This routine spawns a UNIX shell executable.
;
; CATEGORY:
;	Adas system.
;	
; WRITTEN:
;       William Osborn, Tessella Support Services plc, 18/10/1996
;
; MODIFIED:
;	1.1	    William Osborn	
;		    First release
;	1.2	    Martin O'Mullane	
;		    Added JET specific code to allow submission to
;                   the LoadLeveler system. 
;       1.3         Richard Martin
;                   Corrected spelling mistake in Loadleveller queue names.
;	1.4	    Martin O'Mullane	
;		    - Added Linux as an acceptable OS. Also permitted Linux
;                     to check for and use the JET specific Loadleveler 
;                     batch job system. 
;                   - Restructured Loadleveler code and moved it to a 
;                     separate subroutine.
;	1.5	    Martin O'Mullane	
;		    - JET's loadleveler system has changed significantly.
;                     Add changes to deal with it.
;	1.6	    Martin O'Mullane	
;                    - Move JET batch handling into a separate routine.
;                    - Add optional font parameter for output message. 
;
; VERSION:
;   1.1	    18-10-96
;   1.2	    13-02-98
;   1.3     14-04-98
;   1.4	    24-05-99
;   1.5	    30-04-2002
;   1.6	    20-06-2003
;			
;-----------------------------------------------------------------------------

PRO batch, dscontrol, time=time, title=title, jobname=jobname,     $
                  FONT_LARGE = font_large


; Set defaults for keywords

    IF n_elements(font_large) eq 0 THEN font_large = ''

    if not keyword_set(time) then time=string(2, format='(I1)')  $
                             else time=strtrim(string(time),2)
    if not keyword_set(title) then title='BATCH EXECUTION INFORMATION'
    if not keyword_set(jobname) then jobname = ''

    warn = 'echo "Warning: unrecognised OS; batch job not started"'
    warnmes="Warning: unrecognised OS. Batch job not started"



; Find out if we are at JET for Loadleveler batch option
          
    spawn,'domainname',dom_name
    if dom_name(0) eq 'jaclinux' then atJET = 1 else atJET = 0



; Build 'at' command according to operating system

    osname = 'uname'
    spawn, osname, osresult

    OS = strcompress(osresult(0), /remove_all)
    
    
    CASE OS OF
    
    'OSF1'  : begin
                command = 'at -s -f '+dscontrol+' now+'+time+'minute'
                infolab = 'Your batch job ' + jobname +		$
                         ' will commence in ' + time + ' minutes'
              end
             
    'SunOS' : begin
                 infolab = 'Your batch job ' + jobname +		$
                          ' will commence in ' + time + ' minutes'
                 osversion = 'uname -r'
                 spawn, osversion, vernum
                 verval = fix(strmid(strcompress(vernum(0),/remove_all),0,1))
                 if verval eq 5 then begin
                     command = 'at -s -f '+dscontrol+' now + '+time+' minute'
                 endif else if verval eq '4' then begin
                     command = 'at -s now + '+time+' minute  '+dscontrol
                 endif else begin
                     command = 'echo "Warning: Sun OS version not supported"'
                     infolab = 'Warning: Sun OS version not supported'
                 endelse
              end

    'ULTRIX' : begin
                  spawn, 'date', datetex
                  time2 = fix(strmid(datetex,14,2))+fix(time)
                  time3 = strmid(datetex, 11, 3)+strtrim(string(time2),2)
                  time  = string(time3(0))
                  command = 'at '+time+' '+dscontrol
                  infolab = 'Your batch job ' + jobname +		$
                            ' will commence at: ' + time
              end
    
    'HP-UX'  : begin
                command = 'echo "Warning: cannot run batch under HP"'
                infolab = 'HP-UX cannot run batch jobs'
              end


    'AIX'  : begin
                command = 'at -s -f '+dscontrol+' now+'+time+'minute'
                infolab = 'Your batch job ' + jobname +		$
                          ' will commence in ' + time + ' minutes'
              end
    
    'Linux' : begin
                if atJET eq 1 then begin
                   infolab = loadleveler(dscontrol, command, jobname=jobname)
                endif else begin
                   command = 'at -f '+dscontrol+' now + '+time+' minute'
                   infolab = 'Your batch job ' + jobname +		$
                             ' will commence in ' + time + ' minutes'
                endelse
              end
    ELSE   : begin
                command = warn
                infolab = warnmes
             end
    
    ENDCASE


; Now set the batch job going
    
    spawn, command


; Create a small information widget to inform user

    tempbase = widget_base(/column, xoffset=300, yoffset=200,	$
                           title = title)
    lab0 = widget_label(tempbase, value='', font=font_large)
    lab1 = widget_label(tempbase, value = infolab, font=font_large)
    lab2 = widget_label(tempbase, value='', font=font_large)
    widget_control, tempbase, /realize
    wait, 4
    
        ;**** Destroy the information widget ****
    
    widget_control, tempbase, /destroy

END
