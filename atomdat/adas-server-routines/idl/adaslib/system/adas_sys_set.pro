;+
; PROJECT:
;       ADAS
;
; NAME:
;	ADAS_SYS_SET
;
; PURPOSE:
;	Returns various system settings for the ADAS system.
;
; EXPLANATION:
;	There are a number of settings which must be set before the
;	ADAS system can run.  These settings are grouped together as
;	they may vary from site to site depending on the operating
;	system in use etc.  This routine also returns some settings
;	which will vary from user to user.  These last settings are
;	read from UNIX environment variables which are set in the
;	users .login script.
;
; USE:
;	This routine is only called from adas.pro.
;
; INPUTS:
;	None.
;
; OPTIONAL INPUTS:
;	None.
;
; OUTPUTS:
;	ADASREL    - A string; The name of the system and the release.
;		     e.g ' ADAS RELEASE: ADAS93 V1.1'
;
;	FORTDIR    - A string; The directory holding the ADAS FORTRAN
;		     executables.  This is read from the environment
;		     variable ADASFORT. e.g '/disk2/adas/fort'
;
;	USERROOT   - A string; The root directory the user's ADAS data.
;		     This is read from the environment variable ADASUSER.
;		     e.g '/disk2/user/adas'
;
;	CENTROOT   - A string; The root directory the ADAS central data.
;		     This is read from the environment variable ADASCENT.
;		     e.g '/disk2/adas'
;
;	DEVLIST    - A string array;  Each element of the array is the
;		     name of a hardcopy graphics output device.  This
;		     list must mirror the DEVCODE list.  DEVLIST is the
;		     list which the user will see.
;		     e.g ['Post-Script','HP-PCL','HP-GL']
;
;	DEVCODE    - A string array;  Each element of the array is the
;		     IDL name of hardcopy graphics output device as used
;		     in a SET_PLOT command.  This list must mirror the
;		     DEVLIST list.
;		     e.g ['PS','PCL','HP']
;
;	FONT_LARGE - A string; A larger font for use in widgets.  This
;		     is read from the environment variable ADASFNTLG.
;		     e.g 'courier_bold14'
;
;	FONT_SMALL - A string; A smaller font for use in widgets. This
;		     is read from the environment variable ADASFNTSM.
;		     e.g 'courier_bold12'
;
;	EDIT_FONTS - A structure giving two fonts used to differentiate
;		     editable and non-editable data in edit tables;
;		     {font_norm:'', font_input:''}.  These are read from
;		     the environment variables ADASFNTNM and ADASFNTIN.
;
; OPTIONAL OUTPUTS:
;	None.
;
; KEYWORD PARAMETERS:
;	None.
;
; CALLS:
;	None.
;
; SIDE EFFECTS:
;	The environment variables ADASFORT, ADASUSER, ADASCENT,
;	ADASFNTSM, ADASFNTLG, ADASFNTIN and ADASFNTNM must exist.
;	These environment variables are set in the 'source' script
;	adas_setup except for ADASUSER which should be set explicitly
;	in the user's .login script.  Hence the user's .login script
;	should be modified to include the extra source statement for
;	adas_setup and the setenv command for ADASUSER, e.g;
;
;	source /...../idl_setup
;	source /...../adas_setup
;	setenv ADASUSER /...../bowen/adas
;
;	where the path names are appropriate for the system.  Note the
;	source adas_setup MUST FOLLOW source idl_setup as it modifies
;	the environment variable IDL_PATH.
;
; CATEGORY:
;	Adas system.
;
; WRITTEN:
;       Andrew Bowen, Tessella Support Services plc, 24-May-1993
;
; MODIFIED:
;       1.1	Andrew Bowen    
;               First release.
;	1.2	Andrew Bowen
;		Header comments added.
;	1.3	Lalit Jalota
;	1.4	Lalit Jalota
;	1.5	Tim Hammond
;		Updated version number on first menu.
;	1.6	Tim Hammond
;		Increased version number to 1.3
;	1.7	Tim Hammond
;		Increased version number to 1.4
;	1.8	Tim Hammond
;		Increased ADAS version number to 1.5
;	1.9	William Osborn
;		Increased ADAS version number to 1.6
;	1.10	William Osborn
;		Increased ADAS version number to 1.7
;	1.11	William Osborn
;		Increased ADAS version number to 1.8
;	1.12	William Osborn
;		Increased ADAS version number to 1.9
;	1.13	Richard Martin
;		Increased ADAS version number to 1.10
;	1.14	Richard Martin
;		Increased ADAS version number to 1.11
;	1.15	Richard Martin
;		Increased ADAS version number to 1.12
;	1.16	Richard Martin
;		Increased ADAS version number to 1.13
;	1.17    Richard Martin
;		Changed release to ADAS98 R 2.0
;	1.18    Richard Martin
;		Increased ADAS version number to 2.1
;	1.19    Richard Martin
;		Increased ADAS version number to 2.2
;	1.20    Richard Martin
;		Increased ADAS version number to 2.3
;	1.21    Richard Martin
;		Increased ADAS version number to 2.4
;	1.22	Richard Martin
;		Increased ADAS version number to 2.5.1
;	1.23	Richard Martin
;		Increased ADAS version number to 2.5.2
;	1.24	Richard Martin
;		Increased ADAS version number to 2.5.3
;	1.25	Richard Martin
;		Increased ADAS version number to 2.5.4
;	1.26	Richard Martin
;		Increased ADAS version number to 2.5.5
;	1.27	Richard Martin
;		Increased ADAS version number to 2.5.6
;	1.28	Richard Martin
;		Increased ADAS version number to 2.6
;	1.29	Richard Martin
;		Increased ADAS version number to 2.7
;	1.30	Allan Whiteford
;		Increased ADAS version number to 2.7.1
;	1.31	Allan Whiteford
;		Increased ADAS version number to 2.8
;	1.32	Allan Whiteford
;		Increased ADAS version number to 2.9
;	1.33	Allan Whiteford
;		Increased ADAS version number to 2.10
;	1.34	Allan Whiteford
;		Increased ADAS version number to 2.11
;	1.35	Allan Whiteford
;		Increased ADAS version number to 2.12
;	1.36	Allan Whiteford
;		Increased ADAS version number to 2.13
;	1.37	Martin O'Mullane
;		Increased ADAS version number to 3.0
;	1.38	Martin O'Mullane
;		Increased ADAS version number to 3.1
;	1.39	Martin O'Mullane
;		  - Increased ADAS version number to 4.0
;                 - Deprecate PCL and HP output options.
;	1.40	Martin O'Mullane
;		  - Increased ADAS version number to 4.1
;
; VERSION:
;       1.1     24-05-93
;	1.2	02-06-93
;	1.3	13-03-95
;	1.4	13-03-95
;	1.5	05-06-95
;	1.6	14-07-95
;	1.7	15-11-95
;	1.8	16-02-96
;	1.9	14-05-96
;	1.10	22-07-96
;	1.11	17-10-96
;	1.12	25-11-96
;	1.13	08-04-97
;	1.14	17-07-97
;	1.15	??-11-97
;	1.16	13-03-98
;	1.17    19-06-98
;	1.18    04-12-98
;	1.19	19-03-99
;	1.20	18-10-99
;	1.21	04-04-00
;	1.22	14-11-00
;	1.23	14-03-2001
;	1.24	23-07-2001
;	1.25	20-11-2001
;	1.26	18-03-02
;	1.27	16-07-02
;	1.28	18-03-03
;	1.29	18-03-03
;	1.30	18-04-04
;	1.31	29-11-04
;	1.32	02-06-05
;	1.33	05-01-06
;	1.34	06-09-06
;	1.35	01-10-07
;	1.36	17-12-08
;	1.37	01-10-2009
;	1.38	11-02-2011
;	1.39	17-11-2012
;	1.40	09-01-2016
;-
;-----------------------------------------------------------------------------

PRO adas_sys_set, adasrel, fortdir, userroot, centroot, 		$
		  devlist, devcode, font_large, font_small, edit_fonts

    adasrel = ' ADAS RELEASE: v4.1'

    fortdir = getenv('ADASFORT')

    userroot = getenv('ADASUSER')
    centroot = getenv('ADASCENT')

    devlist = ['Post-Script']
    devcode = ['PS']
 
    font_large = getenv('ADASFNTLG')
    font_small = getenv('ADASFNTSM')
    font_norm  = getenv('ADASFNTNM')
    font_input = getenv('ADASFNTIN')
    edit_fonts = {font_norm:font_norm,font_input:font_input}

END
