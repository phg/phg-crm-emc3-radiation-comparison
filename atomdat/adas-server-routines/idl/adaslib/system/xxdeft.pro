; Copyright (c) 1995 Strathclyde University .
; SCCS info: Module @(#)$Header: /home/adascvs/idl/adaslib/system/xxdeft.pro,v 1.4 2004/07/06 15:34:18 whitefor Exp $ Date $Date: 2004/07/06 15:34:18 $
;
;+
; PROJECT:
;       ADAS IBM MVS to DEC UNIX conversion
;
; NAME:
;	XXDEFT
;
; PURPOSE:
;	To set up an array of 'standard' temperatures.
;
; EXPLANATION:
;	This routine performs the same function as the IBM ADAS
;	FORTRAN subroutine XXDEFT. It sets up in 'tr()' array
;	standard set of reduced temperatures used in constructing
;	master condensed collisional-dielectrionic and metastable
;	population files.
;
; USE:
;	Example use;
;	
;		ndtin = 12
;		xxdeft, ndtin, ite, tr
;
; INPUTS:
;	NDTIN	- Maximum number of temperatures allowed
;
; OPTIONAL INPUTS:
;	None.
;
; OUTPUTS:
;	ITE	- Integer; Number of temperatures set
;
;	TR	- Dblarr; Output set of reduced electron temps (kelvin/z1**7).
;
; OPTIONAL OUTPUTS:
;	None.
;
; KEYWORD PARAMETERS:
;	None.
;
; CALLS:
;	None.
;
; SIDE EFFECTS:
;	None.
;
; CATEGORY:
;	Adas system.
;
; WRITTEN:
;       Andrew Bowen, Tessella Support Services plc, 20-Apr-1993
;
; MODIFIED:
;       Version 1       Andrew Bowen    1-Jun-1993
;                       First release.
;
; VERSION:
;       1       1-Jun-1993
;-
;-----------------------------------------------------------------------------

PRO xxdeft, ndtin, ite, tr

		;**** the number of default temperatures ****
  ntset = 12

		;**** the output temperature array ****
  tr = dblarr(ndtin)

		;**** Check number of temperatures ****
  ON_ERROR, 2
  if ndtin lt ntset then message,'XXDEFT ERROR: NDTIN < NTSET. INCREASE NDTIN'

		;**** the default values ****
		;**** temperatures: (reduced - kelvin/z1**2) ****
  tr(0:ntset-1) = [ 5.0D+02 , 1.0D+03 , 2.0D+03 , 5.0D+03 , $
		    1.0D+04 , 2.0D+04 , 5.0D+04 , 1.0D+05 , $
		    2.0D+05 , 5.0D+05 , 1.0D+06 , 2.0D+06 ]

		;**** return the number of values set in tr ****
  ite = ntset


END
