; Copyright (c) 1995, Strathclyde University
; SCCS info: Module @(#)$Header: /home/adascvs/idl/adaslib/system/text_table.pro,v 1.16 2008/03/25 14:06:50 allan Exp $	Date $Date: 2008/03/25 14:06:50 $
;+
; PROJECT:
;       ADAS IBM MVS to UNIX conversion
;
; NAME:
;	TEXT_TABLE
;
; PURPOSE:
;	Produces a text version of an ADAS data table.
;
; EXPLANATION:
;	This routine is used to produce a text version of an ADAS data
;	table.  The text version is intended for display in a non-editable
;	text widget.  This function returns the text table in a string
;	array with one element for each line of the table.  The table
;	is formatted automatically.  The routine is designed to use
;	many of the same arguments and keywords as the routine ADAS_EDTAB
;	to simplify using these two routines together.
;
; USE:
;	The following code produces a text table. The table data is in
;	4 columns of 5 rows.  There are no units specified for the
;	numeric data.  The table has column headers.
;
;	data = strarr(4,5)
;	data(0,*) = 1000.0
;	data(1,*) = 100.0
;	data(2,*) = 1.0
;	data(3,*) = 500.0
;	value = strtrim(string(data),2)
;	text = text_table(value, $
;		colhead=[['Col 1' ,'Col 2' ,'Col 3'      ,'Col 4'      ], $
;			 ['Volume','Volume','Temperature','Temperature']])
;	print,text
;
;	See cw_adas_table.pro for a more comprehensive example of
;	text_table usage.
;
; INPUTS:
;	VALUE    - Array of initial data values, type STRARR(ncols,nrows).
;		   Each string value should represent a valid floating
;		   point number.  Elements of the string array may contain
;		   empty strings.
;
; OPTIONAL INPUTS:
;	(Note: either ALL or NONE of these optional inputs should be
;	       provided by the caller)
;
;	UNITS    - Index number of the current units in the arrays that
;		   follow.
;
;	UNITNAME - 1D/2D array of text strings giving the names of the
;		   units available.
;
;	UNITIND  - 2D array of column indicies which make up the table
;		   for the available units.  For example a 4 column
;		   table switchable between 3 possible units would have
;		   dimensions UNITIND(3,4).  A value of;
;		     [[0,0,0],[1,1,1],[2,3,4],[5,6,7]]
;		   would refer to the columns from a VALUE array with
;		   dimensions VALUE(8,nrows).  Columns 0 and 1 in the
;		   table always come from columns 0 and 1 in the VALUE
;		   array but column 2 in the table is taken from columns
;		   2, 3 and 4 in VALUE as the units are changed between
;		   0, 1 and 2.
;
; OUTPUTS:
;	None.
;
; OPTIONAL OUTPUTS:
;	None.
;
; KEYWORD PARAMETERS:
;	UNITSTITLE - A title to place above the units box.
;
;	COLHEAD  - Column headers, type STRARR(NCOLS,n).  Each of the n
;		   rows of header is placed one below another at the top
;		   of each column.
;
;	NOINDEX  - Set this keyword to remove the column of integer index
;		   values at the far left of the table.  The default is
;		   for the index column to appear.
;
;	MINCSIZE - This keyword is used to specify a minimum column
;		   width for the text table.
;	UNITS_PLUS - Allows an extra string to be placed at the end of the
;			units line
;
; CALLS:
;	None.
;
; SIDE EFFECTS:
;	None.
;
; CATEGORY:
;	Adas system.
;
; WRITTEN:
;       Andrew Bowen, Tessella Support Services plc, 5-Apr-1993
;
; MODIFIED:
;       Version 1       Andrew Bowen
;                       First release.
;
;	Version 1.1	Lalit Jalota
;			Added UNITS_PLUS keyword.
;	Version 1.10	Tim Hammond
;			Previous version history unknown.
;			Added capability for the array UNITNAME to
;			be 2-dimensional in line with changes
;			made by David Brooks, Univ. Strathclyde.
;	Version 1.11	Tim Hammond
;			Added default definitions for variables
;			namesize and data as above change had meant
;			that in some cases they were not getting
;			defined.
;	Version 1.12	Tim Hammond
;			Extra default definition included.
;	Version 1.13	Tim Hammond
;			Removed print statement.
;	Version 1.14   	William Osborn
;			Added check for 1D array and changed initialisation
; 			of nrows and ncols accordingly
;	Version 1.15	Hugh Summers
;			Changed number of pieces of data from a fixed value
;			of 3 to reflect the number of columns
;			(Note: Comment added by Allan Whiteford and is
;			       a best guess of this revision)
;	Version 1.16	Changed loop variable from int to long to allow
;			for more than 32,768 rows.
;			
;
; VERSION:
;       1       28-May-1993
;	1.1	10-Mar-1995
;	1.10	12-Jul-1995
;	1.11	13-Jul-1995
;	1.12	13-Jul-1995
;	1.13	13-Jul-1995
;	1.14	28-Mar-1996
;	1.15	05-03-2003
;	1.15	25-03-2008
;-
;-----------------------------------------------------------------------------


FUNCTION text_table,	value, units, unitname, unitind, 		$
			UNITSTITLE = unitstitle, COLHEAD = colhead, 	$
			NOINDEX = noindex, MINCSIZE = mincsize
			UNITS_PLUS = units_plus


  IF (N_PARAMS() EQ 0) THEN MESSAGE, 'Must specify VALUE for text_table'
  IF (N_PARAMS() GT 1 and N_PARAMS() NE 4) THEN MESSAGE, 		$
	'UNITS, UNITNAME, and UNITIND go together for text_table'

  ON_ERROR, 2					;return to caller

		;**** Set defaults for keywords ****

  IF NOT (KEYWORD_SET(unitstitle)) THEN unitstitle = 'Units in'
  IF NOT (KEYWORD_SET(noindex)) THEN noindex = 0
  IF NOT (KEYWORD_SET(mincsize)) THEN mincsize = 1
  IF NOT (KEYWORD_SET(units_plus)) THEN units_plus = ' ' 

		;**** Find size of VALUE ****

  valsize = size(value)

		;**** Check for consistency in units info ****

  IF (N_PARAMS() GT 1) THEN begin
    if units(0) gt -1 then begin
      namesize = size(unitname)
      indsize = size(unitind)
      ncols = indsize(2)
      if ((namesize(0) ne 1) and (namesize(0) ne 2)) then 		$
          MESSAGE, 'text_table; UNITNAME must be 1D/2D array' 
      if (indsize(0) ne 2) then 					$
          MESSAGE, 'text_table; UNITIND must be a 2D array'
      nunits = namesize(1)
      if (nunits ne indsize(1)) then MESSAGE, 				$
        'text_table; dimension of UNITNAME and first dimension of UNITIND'+ $
	' must be the same'
      if (indsize(2) gt valsize(1)) then MESSAGE, 			$
        'text_table; second dimension of UNITIND must be <= first dimension'+ $
	' of VALUE'
      if (namesize(0) eq 1) then begin
          if (units lt 0) or (units ge nunits) then MESSAGE, 		$
        'text_table; UNITS must point to an element in the array UNITNAME'
      endif else if (namesize(0) eq 2) then begin
;          if (units(0) lt 0) or (units(2) ge nunits) then MESSAGE,	$
;        'text_table; UNITS must point to an element in the array UNITNAME'
          if (units(0) lt 0) or (units(ncols-1) ge nunits) then MESSAGE, $
        'text_table; UNITS must point to an element in the array UNITNAME'
      endif
    endif else begin
      namesize = 0
    endelse
  endif else begin
    units = -1
    namesize = 0
  endelse

		;**** Check the input VALUE and get row/col sizes ****
  ndim = valsize(0)
  if ndim gt 2 then MESSAGE, 'adas_edtab; VALUE must be 1D or 2D only'
  if valsize(ndim+1) ne 7 then MESSAGE, $
				'adas_edtab; VALUE must be a string array'
  if units(0) eq -1 then begin
    if(ndim eq 1) then begin
       ncols = valsize(1)
    endif else ncols = valsize(ndim-1)
  end else begin
    ncols = indsize(2)
  end
  if(ndim eq 1) then begin
    nrows=1
  endif else begin
    nrows = valsize(ndim)
  endelse

		;**** extract value sub-set if units in operation ****

  if (namesize(0) eq 1) then begin
      if units eq -1 then begin
        data = value
      end else begin
        data = value(unitind(units,*),*)
      end
  endif else if (namesize(0) eq 2) then begin
;      data = strarr(3,valsize(2))
      data = strarr(ncols,valsize(2))
      if units(0) eq -1 then begin
	  data = value
      end else begin 
;          for i = 0,2 do begin
;              data(i,*) = value(unitind(units(i),i),*)
;          endfor
          for i = 0,ncols-1 do begin
              data(i,*) = value(unitind(units(i),i),*)
          endfor
      end
  endif else begin
      if units(0) eq -1 then data = value
  endelse

                ;**** How many rows of header are there? ****

  headsize = size(colhead)
  if headsize(0) eq 1 then numhrow = 1 else numhrow = headsize(2)

		;**** Declare output text array ****

  if units(0) gt -1 then begin
    text = strarr(numhrow+nrows+2)
  end else begin
    text = strarr(numhrow+nrows)
  end

		;**** Find maximum string length for each column ****

  tab = intarr(ncols + 1)
  if noindex eq 0 then tab(0) = 7 else tab(0) = 1
  for j = 1 , ncols do begin
    tab(j) = tab(j-1) + $
	max([max(strlen(data(j-1,*))),max(strlen(colhead(j-1,*))),mincsize])+2
  end

		;**** Spaces for formatting text ****
  blanks = '                                                        '

		;*******************************************
		;**** Put the table into a string array ****
		;*******************************************

		;**** Column headers ****

  if noindex eq 0 then text(0) = 'INDEX' else text(0) = ''
  for k = 0L , numhrow-1 do begin
    for j = 0L , ncols-1 do begin
      header = colhead(j,k)
      if header eq '' then header = ' '
      text(k) = text(k) + strmid(blanks,0,tab(j)-strlen(text(k)))
      text(k) = text(k) + header
    end
    text(k) = text(k) + strmid(blanks,0,tab(ncols)-2-strlen(text(k)))
  end

		;**** The numbers ****

  for k = 0L , nrows-1 do begin
    l = k+numhrow
    if noindex eq 0 then text(l) = ' '+strtrim(string(k+1),2) else text(l) = ''
    for j = 0 , ncols-1 do begin
      text(l) = text(l) + strmid(blanks,0,tab(j)-strlen(text(l)))
      if(nrows eq 1) then begin
         text(l) = text(l) + data(j)
      endif else begin
         text(l) = text(l) + data(j,k)
      endelse
    end
    text(l) = text(l) + strmid(blanks,0,tab(ncols)-2-strlen(text(l)))
  end

		;**** The units ****

  if (namesize(0) eq 1) then begin
      if units gt -1 then begin
          text(numhrow+nrows) = ' '
          text(numhrow+nrows+1) = unitstitle+': '+unitname(units) + 	$
  	  units_plus
      endif
  endif else if (namesize(0) eq 2) then begin
      if units(0) gt -1 then begin
          text(numhrow+nrows) = ' '
;          text(numhrow+nrows+1) = 					$
;          unitstitle(0)+':'+strtrim(unitname(units(0),0),2)+'.'+ 	$
;          units_plus +							$
;	  unitstitle(1)+':'+strtrim(unitname(units(1),1),2)+'.'+ 	$
;          units_plus +							$
;	  unitstitle(2)+':'+strtrim(unitname(units(2),2),2)
          for i=0 , ncols-1 do begin
            text(numhrow+nrows+1) = text(numhrow+nrows+1) + 		$
              unitstitle(i)+':'+strtrim(unitname(units(i),i),2)+'.'+ 	$
              units_plus
	  end
      endif
  endif


  RETURN, text

END
