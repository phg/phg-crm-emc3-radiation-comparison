; Copyright (c) 1995 Strathclyde University .
; SCCS info: Module @(#)$Header: /home/adascvs/idl/adaslib/system/xxdate.pro,v 1.7 2004/07/06 15:33:52 whitefor Exp $ Date $Date: 2004/07/06 15:33:52 $
;+
; PROJECT:
;       ADAS IBM MVS to DEC UNIX conversion
;
; NAME:
;	XXDATE()
;
; PURPOSE:
;	Returns the current system date and time.
;
; EXPLANATION:
;	This function spawns UNIX commands to get the current date and
;	time.  It is called XXDATE since it performs a similar function
;	to the FORTRAN routines XXDATE and XXTIME.
;
;	Note: the syntax of the UNIX command and the output which is
;	returned may vary from one operating system to another.  The
;	operating system is checked and specific code executed for that
;	system.  Currently the routine has specific code for;
;
;		UNIX		'date +"%d/%m/%y%n%H:%M"'
;		OSF		(same as UNIX)
;		SUNOS		(same as UNIX)
;		All others 	(same as UNIX)
;
; USE:
;	An example;
;		date = xxdate()
;		print,'The date is ',date(0)
;		print,'The time is ',date(1)
;
; INPUTS:
;	None.
;
; OPTIONAL INPUTS:
;	None.
;
; OUTPUTS:
;	This function returns a two element string array.  The first
;	element gives the current date and the second element give the
;	current time.
;
; OPTIONAL OUTPUTS:
;	None.
;
; KEYWORD PARAMETERS:
;	None.
;
; CALLS:
;	None.
;
; SIDE EFFECTS:
;	This function spawns UNIX commands.
;
; CATEGORY:
;	UNIX system IDL utility.
;
; WRITTEN:
;       Andrew Bowen, Tessella Support Services plc, 24-May-1993
;
; MODIFIED:
;       Version 1       Andrew Bowen    11-Jun-1993
;                       First release.
;	Version 1.1	Lalit Jalota	13-Mar-1995
;			Added /SH keyword to spawn call to avoid any user
;			aliases being used.
; VERSION:
;       1       11-Jun-1993
;	1.1	13-Mar-1995
;-
;-----------------------------------------------------------------------------

FUNCTION xxdate

		;********************************************
		;**** issue unix command to get the date ****
		;********************************************

  if !version.os eq 'ultrix' or !version.os eq 'OSF' or $
	!version.os eq 'sunos' then begin

		;**** %d/%m/%y	gives date in dd/mm/yy format	****
		;**** %n	causes a line feed		****
		;**** %H:%M	gives the time in HH:MM format	****
    spawn,'date +"%d/%m/%y%n%H:%M"',date,/sh

  end else begin

    spawn,'date +"%d/%m/%y%n%H:%M"',date,/sh

  end

		;**** date is returned as a two element string array ****
		;**** date(0) is the date and date(1) is the time.   ****
RETURN, date

END
