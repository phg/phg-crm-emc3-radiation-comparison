;-----------------------------------------------------------------------------
; Copyright (c) 200 Strathclyde University.
;+
; PROJECT : ADAS
;
; NAME    : XXUSER()
;
; PURPOSE : Returns the real name of the user.
;
; EXPLANATION:
;       This function spawns UNIX command, finger, to get the current
;       name of the user. It has a default return value if the
;       user's real name is not set by the system administrator.
;
;
; USE:
;       An example;
;               producer = xxuser()
;
; INPUTS:
;       None.
;
; OPTIONAL INPUTS:
;       None.
;
; OUTPUTS:
;       This function returns a string.
;
; OPTIONAL OUTPUTS:
;       None.
;
; KEYWORD PARAMETERS:
;       None.
;
; CALLS:
;       None.
;
; SIDE EFFECTS:
;       This function spawns UNIX commands.
;
; CATEGORY:
;       UNIX system IDL utility.
;
; WRITTEN:
;       Martin O'Mullane
;
; MODIFIED:
;       1.1   Martin O'Mullane
;               - First release.
;       1.2   Martin O'Mullane
;               - Check for missing LOGNAME environment variable.
;       1.3   Martin O'Mullane
;               - Use getent, then finger since the finger command is
;                 missing from recent RH/Fedora based systems.
;       1.4   Martin O'Mullane
;               - Replace obsolete str_sep routine.
;
; VERSION:
;       1.1     28-02-2000
;       1.2     07-09-2010
;       1.3     27-09-2013
;       1.4     17-10-2018
;-
;-----------------------------------------------------------------------------
FUNCTION xxuser

  uid = getenv("LOGNAME")

  if uid EQ '' then return, 'I made this'

  cmd = 'getent passwd ' + uid + '| cut -d '':'' -f 5'
  spawn, cmd, result, /stderr

  if strpos(result[0], 'command not found') EQ -1 then begin

     if strpos(result[0], 'command not found') EQ -1 then xxuser = result[0] $
                                                     else xxuser = 'I made this'

  endif else begin

     ; Try finger

     cmd = 'finger '+ uid
     spawn, cmd, result, /stderr

     if strpos(result[0], 'command not found') EQ -1 then begin

        case strlowcase(!version.os) OF
          'linux' : teststr = 'ame: '
          'aix'   : teststr = 'ife: '
          'sunos' : teststr = 'ife: '
           else   : teststr = 'ife: '
        endcase

        if result[0] NE '' then begin
           str = result[0]
           i1  = strpos(str, teststr)
           if i1 GT 0 then xxuser = strmid(str, i1+5) $
                      else xxuser = 'Unknown'
        endif else begin
           xxuser = 'I made this'
        endelse

     endif

  endelse

  return, xxuser

END
