;----------------------------------------------------------------------
;+
; PROJECT:
;       ADAS
;
; NAME:
;       xxcomments
;
; PURPOSE:
;       Reads the comment section of the input adf dataset.
;
; EXPLANATION:
;       Returns the lines starting with 'C' from the bottom of ADAS
;       adf datasets.
;
;
; INPUTS:
;       FILE     - The UNIX system name of the file. For example
;                  'file.dat' refers to the current directory and
;                  '/disk2/fred/data' is a full path name.
;
; OPTIONAL INPUTS:
;       None
;
; OUTPUTS:
;       COMMENTS - A string array of the comments in the dataset.
;
; OPTIONAL OUTPUTS:
;       NLINES   - Number of comment lines.
;
; KEYWORD PARAMETERS:
;       None
;
; CALLS:
;       None
;
; SIDE EFFECTS:
;       None
;
; CATEGORY:
;       UNIX system IDL utility.
;
; WRITTEN:
;       Martin O'Mullane, 12-04-2005
;
;
; MODIFIED:
;       1.1     Martin O'Mullane
;               - First release.
;       1.2     Martin O'Mullane
;               - Account for lower case indication of comment line.
;
; VERSION:
;       1.1     12-04-2005
;       1.2     12-03-2009
;
;-
;----------------------------------------------------------------------


PRO xxcomments, file     = file,     $
                comments = comments, $
                nlines   = nlines,   $
                help     = help


if keyword_set(help) then begin
   doc_library, 'xxcomments'
   return
endif

; Check it exits

if n_elements(file) eq 0 then message, 'A file name must be given'
file_acc, file, exist, read, write, execute, filetype
if exist ne 1 then message, 'file does not exist ' + file
if read ne 1 then message, 'file cannot be read from this userid ' + file

adas_readfile, file=file, all=all, nlines=nlines

if nlines LT 3 then message, 'Check the adf file - too short'



; Ignore the first line as adf15 Carbon datasets start with a 'C'

all = all[1:*]
ind = where(strupcase(strmid(all,0,1)) EQ 'C', ncomments)

if ncomments GT 0 then begin
   comments = all[ind]
   nlines   = ncomments
endif else begin
   message, 'No comments in file', /continue
   nlines   = -1
endelse


END
