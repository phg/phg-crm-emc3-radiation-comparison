;-----------------------------------------------------------------------------
;+
; PROJECT : ADAS
;
; NAME    : adas_check_file
;
; PURPOSE : Checks the existence and access rights of a file.
;
; INPUTS  :
;           file        - The UNIX system name of the file. For example
;                         'file.dat' refers to the current directory and
;                         '/home/fred/file.dat' is a full path name.
;           description - A description of the file used in the message,
;                         eg. 'adf01', 'adf01 (cross section)'. It defaults
;                         to 'file' it is not set.
;
; OUTPUTS : None.
;
; CALLS   : file_acc
;
;
; SIDE EFFECTS: If the file does not exist or the user cannot
;               access it, a message command is issued which
;               forces a return to the IDL prompt.
;
;
; WRITTEN:
;       Martin O'Mullane
;
; MODIFIED:
;       1.1   Martin O'Mullane
;               - First release.
;
; VERSION:
;       1.1     12-04-2019
;-
;-----------------------------------------------------------------------------


PRO adas_check_file, file=file, description=description

if n_elements(description) EQ 0 then description = 'file'

; Has a filename been passed in

if n_elements(file) eq 0 then message, 'A filename must be supplied'

file_acc, file, exist, read, write, execute, filetype

if exist NE 1 then message,  description + ' does not exist : ' + file

if read NE 1 then message, description + ' cannot be read from this userid : ' + file

END
