; Copyright (c) 2002, Strathclyde University .
; SCCS INFO : Module @(#)$Header: /home/adascvs/idl/adaslib/system/adasplot_surface.pro,v 1.1 2004/07/06 11:15:09 whitefor Exp $	Date $Date: 2004/07/06 11:15:09 $
;
;+
; PROJECT:
;       IDL ADAS
;
; NAME:
;	ADASPLOT_SURFACE
;
; PURPOSE:
;	Displays one surface plot for ADAS.
;
; EXPLANATION:
;	This routine produces a surface plot for display within an IDL graphics 
;	window. The input array z is a 2D array containing the data to be plotted
;	with abscissa values stored within the x, y arrays for the X and Y axes
;	respectively. The data can optionally be plotted on log scales on all 
;	axes, in colour and as a mesh or as a shaded surface.
;
; USE
;	This routine is a general routine, intended to be used whenever
;	it is necessary to display a surface plot from within adas.
;	For an example on its use, see adas510_plot.pro.
;
; INPUTS:
;	x:	Array containing X abscissa values
;
;	y:	Array containing Y abscissa values
;
;	z:	Data to be displayed as a surface
;
;	nsize:	Size of input X & Y arrays
;
;	title:	- String array; titles to be placed above graph
;
;	xtitle:	- String; user supplied comment appended to end of title
;
;	ytitle:	- String; title to display on the y-axis.
;
;	strg:		- String array; Information regarding current data selected
;
;	head1:	- String; header information for data source
;
;	head2a:	- String; Header information for temperature/density 
;
;	head2b:	- String; column titles for temp/density pair information
;
;	x1val:	- Double array; User entered electron temperatures.
;
;	x2val:	- Double array; User entered electron densities.
;
;	grpscal:	 Integer; 	0 - use user entered graph scales
;			   		1 - use default axes scaling
;
;	xmin:		- String; Lower limit for x-axis of graph, number as string.
;
;	xmax:		- String; Upper limit for x-axis of graph, number as string.
;
;	ymin:		- String; Lower limit for y-axis of graph, number as string.
;
;	ymax:		- String; Upper limit for y-axis of graph, number as string.
;
;	zmin:		- String; Lower limit for z-axis of graph, number as string.
;
;	zmax:		- String; Upper limit for z-axis of graph, number as string.
;
; OPTIONAL INPUTS:
;	NONE
;
; OUTPUTS:
;	NONE
;
; OPTIONAL OUTPUTS:
;	NONE
;
; KEYWORD PARAMETERS:
;
;	XLOG:	-  	Use log scaling on the X axis.
;
;	YLOG:	-  	Use log scaling on the Y axis.
;
;	ZLOG:	-  	Use log scaling on the Z axis.
;
;	AX:	-	The angle rotation of the surface plot about the X axis.
;
;	AZ:	-	The angle rotation of the surface plot about the Z axis.
;
;	NODATA:	-	Set up plot window, but do not plot data.
;
;	SURFACE:	-	Display as a shaded surface.
;
;	SCOLOUR:	-	Index of colour palette to use for shaded surface.
;
; CALLS:
;	
;	SURFACE	Internal IDL routine for displaying a mesh surface plot
;	SHADE_SURF	Internal IDL routine for displaying a shaded surface plot
;	LOADCT	Internal IDL routine for loading a colour table.
;
; SIDE EFFECTS:
;
;	NONE
;
; WRITTEN
;	Richard Martin, University of Strathclyde, 17-06-02
;	Based on adasplot.pro v1.2 .
;
; MODIFIED:
; 	1.1	Richard Martin
;		First Release
;
; VERSION:
;	1.1	17-06-02
;
;-
;----------------------------------------------------------------------------

PRO adasplot_surface, x , y, z, nsize, title, xtitle, ytitle, strg, 		$
				head1, head2a, head2b, x1val, x2val, 				$
				grpscal, xmin, xmax, ymin, ymax, zmin, zmax, 			$
				xlog=xlog, ylog=ylog, zlog=zlog, ax=ax, az=az, 			$
				nodata=nodata, surface=surface, scolour=scolour

    COMMON Global_lw_data, left, right, top, bottom, grtop, grright
    
    IF NOT (KEYWORD_SET(nodata)) THEN nodata = 0 ELSE nodata=1
    IF NOT (KEYWORD_SET(xlog)) THEN xlog=0 ELSE xlog=1
    IF NOT (KEYWORD_SET(ylog)) THEN ylog=0 ELSE ylog=1    
    IF NOT (KEYWORD_SET(zlog)) THEN zlog=0 ELSE zlog=1          
    IF NOT (KEYWORD_SET(ax)) THEN ax=45
    IF NOT (KEYWORD_SET(az)) THEN az=45
    IF NOT (KEYWORD_SET(surface)) THEN surface=0
    IF NOT (KEYWORD_SET(scolour)) THEN scolour=0    
         
	;*************************************
	;**** Set up colour plot plotting ****
	;*************************************	

    if !d.name eq 'X' then begin
    	 TVLCT, red, green, blue, /get
    	  
    	 device,get_visual_depth=depth
    	  
    	 if depth eq 24 then begin
    	 	   colour = { black   : red(0)+256L*(green(0)+256L*blue(0)),	 $
    	 			    white	: red(1)+256L*(green(1)+256L*blue(1)),	 $
    	 			    red	: red(2)+256L*(green(2)+256L*blue(2)),	 $
    	 			    green	: red(3)+256L*(green(3)+256L*blue(3)),	 $
    	 			    blue	: red(4)+256L*(green(4)+256L*blue(4)),	 $
    	 			    yellow  : red(5)+256L*(green(5)+256L*blue(5)),	 $
    	 			    magenta : red(6)+256L*(green(6)+256L*blue(6)),	 $
    	 			    cyan	: red(7)+256L*(green(7)+256L*blue(7))	   }
    	 endif else begin
    	 	   colour = { black   : 0, $
    	 			    white	: 1,       $
    	 			    red	: 2,       $
    	 			    green	: 3,       $
    	 			    blue	: 4,       $
    	 			    yellow  : 5,       $
    	 			    magenta : 6,       $
    	 			    cyan	: 7		 }
    	 endelse  
     endif  else begin
     	 	   colour = { black   : 0, $
    	 			    white	: 1,       $
    	 			    red	: 2,       $
    	 			    green	: 3,       $
    	 			    blue	: 4,       $
    	 			    yellow  : 5,       $
    	 			    magenta : 6,       $
    	 			    cyan	: 7		 }    
     endelse                                               
            ;****************************************************
		;**** Suitable character size for current device ****
		;**** Aim for 60 characters in y direction.      ****
   		;*****************************************************

    charsize = (!d.y_vsize/!d.y_ch_size)/60.0 
    small_check = GETENV('VERY_SMALL')
    if small_check eq 'YES' then charsize=charsize*0.8

		;**** set makeplot counter ****

    makeplot = 1

    if surface gt 0 then loadct, scolour, bottom=8

            ;***************************************
		;**** Construct graph title         ****
		;**** "!C" is the new line control. ****
            ;***************************************

    !p.font = -1
    if small_check eq 'YES' then begin
        gtitle =  title(0) + "!C!C" + title(1) + "!C!C"+title(2)+"!C!C" + $
            strmid(title(4),0,57)       
    endif else begin
        gtitle =  title(0) + "!C!C" + title(1) + "!C"+title(2)+"!C" + $
          strmid(title(4),0,57)                 
    endelse

		;**** Find x and y ranges for auto scaling ****

    if grpscal eq 0 then begin

            ;********************************************
		;**** identify values in the valid range ****
		;**** plot routines only work within     ****
		;**** single precision limits.	     ****
            ;********************************************

    	xvals = where(x gt 1.0e-37 and x lt 1.0e37)
    	yvals = where(y gt 1.0e-37 and y lt 1.0e37)
    	if xvals(0) gt -1 then begin
            xmax = max(x(xvals), min=xmin)
            makeplot = 1
    	endif else begin
            makeplot = 0
    	endelse
    	if yvals(0) gt -1 then begin
            ymax = max(y(yvals), min=ymin)
            makeplot = 1
    	endif else begin
            makeplot = 0
    	endelse
        style = 0
    endif else begin

		;Check that at least some data in in axes range ***

    	xvals = where(x ge xmin and x le xmax)
    	yvals = where(y ge ymin and y le ymax)
    	if xvals(0) eq -1 or yvals(0) eq -1 then begin
      	    makeplot = 0
    	endif
    	style = 1
    endelse
    
		;**** Set up log-log plotting axes ****
    
    dxtitle=xtitle
	
    xindex=where(x ge xmin and x le xmax)
    ximax=max(xindex,min=ximin)
    yindex=where(y ge ymin and y le ymax)
    yimax=max(yindex,min=yimin)
     
    if (yimax gt yimin) AND (ximax gt ximin) then begin
      if surface eq 0 then begin
       surface, z(ximin:ximax,yimin:yimax), x(ximin:ximax), y(yimin:yimax),	$
	     position=[left,bottom,grright,grtop], zrange=[zmin,zmax],			$
	     xtitle=dxtitle, ytitle=ytitle, xstyle=style, ystyle=style,			$
	     xlog=xlog, ylog=ylog, zlog=zlog, ax=ax, az=az, charsize=charsize, 		$
	     color=colour.white	
      endif else begin
        shade_surf, z(ximin:ximax,yimin:yimax), x(ximin:ximax), y(yimin:yimax),	$
	     position=[left,bottom,grright,grtop], zrange=[zmin,zmax],			$
	     xtitle=dxtitle, ytitle=ytitle, xstyle=style, ystyle=style, 			$
	     xlog=xlog, ylog=ylog, zlog=zlog, ax=ax, az=az, charsize=charsize, 		$
	     color=colour.white
	     
	     loadct, scolour, bottom=8   
      endelse
    endif else begin

    	xyouts,   0.15 , 0.15 , "NO DATA IN THIS RANGE: Click 'Done' and disable 'Explicit Scaling'."
	
    endelse

		;**** Annotation to plot ****

    rhs = grright + 0.02

    if small_check eq 'YES' then begin
	charsize = charsize * 0.9
    endif
		;**** plot title ****

    xyouts, (left-0.05), (top+0.05), gtitle, /normal, alignment=0.0, 	$
	    charsize = charsize, color=colour.white

    cpp = grtop - 0.05
    xyouts, rhs, cpp, head1, /normal, alignment=0.0, charsize=charsize*0.8, color=colour.white
    cpp = cpp - 0.02

    for i = 0, 6 do begin
           cpp = cpp - 0.02
           xyouts, rhs, cpp, strg(i), /normal,  			   $
           alignment= 0.0, charsize=charsize*0.8, color=colour.white
    endfor
    cpp = cpp - 0.05
    xyouts, rhs, cpp, head2a, /normal, charsize=charsize*0.8, color=colour.white
    cpp = cpp - 0.04
    xyouts, rhs, cpp, head2b, /normal, charsize=charsize*0.8, color=colour.white
    for i = 0, nsize-1 do begin
        index  = strcompress(string(i+1))
        if (i le 8) then line=index + "	    " else line=index + "	 "
        line = line +							$
        strcompress(string(x1val(i), format='(e10.3)')) + "     " +$
        strcompress(string(x2val(i),  format='(e10.3)'))
        cpp = cpp - 0.02
        xyouts, rhs, cpp, line, /normal, charsize=charsize*0.8, color=colour.white
    endfor

		;**** right hand side labels ****
		
    
END
