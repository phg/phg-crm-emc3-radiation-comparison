; Copyright (c) 1997 Strathclyde University .
; SCCS info: Module @(#)$Header: /home/adascvs/idl/adas6xx/adas602/mlcds.pro,v 1.9 2004/07/06 14:21:47 whitefor Exp $ Date $Date: 2004/07/06 14:21:47 $
;+
; PROJECT:
;       IDL-ADAS Development.
;
; NAME:
;       MLCDS
;
; PURPOSE:
;       IDL cursor controlled interface to FORTRAN maximum likelihood
;       spectral line gaussian profile fitting procedure.
;
; EXPLANATION:
;       This routine can be called interactively from the IDL prompt or
;       used as part of a larger window based interface (see ADAS602).
;       A cursor based interface is invoked to allow the user to 
;       decide how they wish to process the data, e.g. how many lines
;       to fit etc. When the user's interactions are complete the
;       information gathered with the user interface is written to 
;       a FORTRAN process via a bi-directional UNIX pipe. Communications
;       are to the FORTRAN subroutine ADAS602. The routine has a number
;       of keywords which allow control of the options and questions
;       that are asked of the user. A structure variable is returned
;       which contains information about the fit. In addition to fitting
;       individual spectra there is an option to allow the automatic
;       processing of many data sets using the same fit parameters (see
;       below).
;
; USE:
;       An example of usage;
;
;       IDL> mlcds, structure, data=spectrum, xmin=100, xmax=400, $
;       IDL> /noprompt, /nooutput
;
; INPUTS:
;       None.
;
; OPTIONAL INPUTS:
;
;       FITDATA       -    A variable to allow return of the structure.
;               { b0      :  0.0     ,      eseb0   :  0.0     ,      $
;                 eclb0   :  0.0     ,      b1      :  0.0     ,      $
;                 eseb1   :  0.0     ,      eclb1   :  0.0     ,      $
;                 b2      :  0.0     ,      eseb2   :  0.0     ,      $
;                 eclb2   :  0.0     ,      w       :  dblarr(10),    $
;                 esewl   :  0.0     ,      eclwl   :  0.0     ,      $
;                 xo      :  dblarr(10),    esexo   :  dblarr(10),    $
;                 eclxo   :  dblarr(10),    h       :  dblarr(10),    $
;                 eseh    :  dblarr(10),    eclh    :  dblarr(10),    $
;                 esewv   :  dblarr(10),    eclwv   :  dblarr(10),    $
;                 flux    :  dblarr(10),    esef    :  dblarr(10),    $
;                 eclf    :  dblarr(10),    getfit  :  intarr(6),     $
;                 exptime :  0.0     ,      info    :  0       ,      $
;                 header  :  '',            xmin    :  0,             $
;                 xmax    :  0,             x       :  indgen(),      $
;                 y       :  dblarr(),      ya      :  dblarr(),      $
;                 nsize   :  0.0,           numline :  0,             $
;                 nfxl    :  0,             bchl    :  0.0,           $
;                 bchr    :  0.0,           lineid  :  strarr(10)     $
;                }
;
;                 B0      Fitted Flat component of background
;                 ESEB0   Estimated standard error in b0
;                 ECLB0   Estimated 95% confidence limit in b0
;                 B1      Fitted Sloped component of background
;                 ESEB1   Estimated standard error in B1
;                 ECLB1   Estimated 95% confidence limit in B1
;                 B2      Fitted Curved component of background
;                 ESEB2   Estimated standard error in B2
;                 ECLB2   Estimated 95% confidence limit in B2
;                 W       Fitted Line Widths
;                 ESEWL   Estimated standard error in W(fixed)
;                 EClWL   Estimated 95% confidence limit in W(fixed)
;                 XO      Fitted Line centroid positions
;                 ESEXO   Estimated standard error in XO
;                 ECLXO   Estimated 95% confidence limit in XO
;                 H       Fitted Line heights at centroid
;                 ESEH    Estimated standard error in H 
;                 ECLH    Estimated 95% confidence limit in H 
;                 ESEWV   Estimated standard error in W(variable)
;                 EClWV   Estimated 95% confidence limit in W(variable)
;                 FLUX    Fitted Line flux (counts under profile)
;                 ESEF    Estimated standard error in FLUX
;                 ECLF    Estimated 95% confidence limit in FLUX
;                 GETFIT  Options selected (see code)
;                 EXPTIME The exposure time (1.0 if expval not set)
;                 INFO    Error from MINPACK routine (see XXDER1)
;                 HEADER  Comment
;                 XMIN    Minimum pixel for fitting range
;                 XMAX    Maximum pixel for fitting range
;                 X       Pixel values
;                 Y       Spectral data
;                 YA      Fit to data
;                 NSIZE   Spectral range width
;                 NUMLINE No. of fitted lines
;                 NFXL    No. of fitted lines with fixed positions
;                 BCHL    Initial estimate of background
;                 BCHR     "       "       "   "
;                 LINEID  Preliminary line identification labels.
;
; OUTPUTS:
;     
;       FITDATA       -    Structure; as mentioned above.
;
; OPTIONAL OUTPUTS:
;       None.
;
; KEYWORD PARAMETERS:
;       DATA          -    Double Array; the array containing the spectrum.
;                          
;       EXPVAL        -    Float; the value of the exposure time. Used only
;                          if the spectrum data is in counts.
; 
;       XMIN          -    Integer; the lower pixel range for graph scaling.
;
;       XMAX          -    Integer; the upper pixel range for graph scaling.
;
;       YMIN          -    Integer; the lower intensity range for graph
;                          scaling.
;
;       YMAX          -    Integer; the upper intensity range for graph
;                          scaling.
;
;       PSYM          -    Integer; the plotting symbol used (as for IDL).
;
;       AUTOLOOP      -    Integer; invoke the option for automatic cycling
;                          through data using the original fit parameters.
;
;       LOOPDATA      -    Double Array(,); the pre-prepared spectrum
;                          with a second dimension for cycling over.
;
;       HEADER        -    String; a comment that can be added to the
;                          structure.
;
;       NOBACK        -    Integer; flag to notify that the background
;                          has been subtracted from the data.
;
;       NOPROMPT      -    Integer; flag to stop 'Print data to screen...'
;                          question being asked.
;
;       POST          -    Integer; plot to postscript file in stand
;                          alone mode.
;
;       OLDSTR        -    Structure; a structure containing the results
;                          from an old fit that when present will be used
;                          for initial estimates of the new fit.
;
;       POSITION      -    Float Array; array giving the position and
;                          size of the plotting area (as for IDL).
;
;       CHARSIZE      -    Double; a value forcing the size of the
;                          plot characters (as for IDL).
;
;       WEIGHTFIT     -    Integer; flag notifying if weighted fit is to
;                          be used. 1-yes, 2-no. The user is prompted
;                          if this keyword is not set.
;
;       NLINES        -    Integer; number of lines to be fitted. The user 
;                          is prompted if this keyword is not set. 
;                          Maximum - 10.
;
;       VWIDTH        -    Integer; flag notifying whether to allow the
;                          line widths to vary. 1-yes, 2-no. The user is
;                          prompted if this keyword is not set.
;
;       FVWIDTH       -    Integer; fit variable widths to the lines.
;                          1-yes, 2-no. The user is prompted if this 
;                          keyword is not set.
;
;       FXWIDTH       -    Double; value of the width if it is to be fixed
;                          for all lines. the user is prompted if this 
;                          keyword is not set.
;
;       FXPOS         -    Double Array; fix the centroid positions of the 
;                          lines to be fitted. The user is prompted if this
;                          keyword is not set.
;
;       IDLINES       -    String Array; preliminary identification labels
;                          of the lines to be fitted. The user is prompted
;                          if this keyword is not set.
;
;       NOOUTPUT      -    Integer; suppresses creation of output fit.
;
;       BLIN          -    Integer; fit a linear component to the 
;                          background. 1-yes, 2-no. The user is prompted
;                          if this keyword is not set.
;
;       BQUAD         -    Integer; fit a quadratic component to the
;                          background. 1-yes, 2-no. The user is prompted
;                          if this keyword is not set.
;
;       ADASTITLE     -    String; header to expand title of graph, used
;                          for ADAS release information.
;
;       UTITLE        -    String; additional graph title information
;                          input by user.
;
;       PROC          -    Integer; flag to notify that previously processed
;                          data is being used.
;
;       WAIT_TIME     -    Double; value, as a fraction of a second, of 
;                          the pause time between plot displays in 
;                          automatic cycling mode when no screen prompt
;                          is present.
;       
;       MISSING       -    Double; value to be trated as a data drop
;                          out and therefore not fitted by FORTRAN.
;
; CALLS:
;       None.
;
; SIDE EFFECTS:
;       This routine spawns a FORTRAN executable.
;
; CATEGORY:
;       ADAS system.
;	
; WRITTEN:
;       David H.Brooks, Univ. of Strathclyde, 23-June-1997.
;
; VERSION:
;       1.0              23-June-1997
;
; MODIFIED:
;	1.1	Richard Martin
;		Put under SCCS control.
;	1.2	David H.Brooks
;		Corrected minor pixel range problem and altered
;               data drop out handling. Now omits drop outs and
;               fits the remaining data then places drop outs
;               back in their original positions. These can
;               subsequently be removed with 'where' statements.
;               This method preserves the original data structure
;               and makes the x-values correspond to the data pixel
;               numbers. Added MISSING keyword.
;	1.3	Richard Martin
;		Set zeros to 0.000001 instead of 0.01, to avoid zero counts
;		error and still see smaller peaks.
;	1.4	David H.Brooks
;		Added cycle counter 'on screen' to trace unavoidable 
;               crashes due to badly corrupted input data. Added error
;               handler to allow code to continue under any crash
;               circumstance and fill relevant parts of structure
;               with -100. Altered position of range check to differentiate
;               between mis-match with old structure and a bad but successful
;               fit. Tightened stringency of test on the latter. Corrected
;               minor error in using an old structure to fit new data,
;               in extreme cases the widths of the later lines were incorrect.
;	1.5	Richard Martin
;		Corrected error - clicking right mouse button did nothing.
;		goto statement poineted to wrong label.
;	1.6	Richard Martin & David Brooks
;		Improved i/o error handling to trap cases which have no fit data.
;	1.7	Richard Martin & David Brooks
;		Undid correction 1.5 . Replaced "goto label1" with "goto labelend"
;		and re-display original plot from pixmap window at this point.
;		Further improvements to error handling.
;	1.8	Richard Martin
;		Fixed error in loop copunter logic.
;	1.9	Richard Martin
;		Undid edits 1.7 & 1.8 (because of errors).
;		Added support for NIS & residual window.
;
; DATE:
;	1.1	27-06-97
;	1.2	18-09-97
;	1.3	24-02-98
;	1.4	14-04-98
;	1.5	16-03-99
;	1.6	08-02-00
;	1.7	18-09-00
;	1.8	14-03-01
;	1.9	20-07-01
;
;-----------------------------------------------------------------------------
pro mlcds, fitdata, graphid, residid, FORTDIR=fortdir, DATA=data, EXPVAL=expval, XMIN=xmin,  $
	   XMAX=xmax,YMIN=ymin, YMAX=ymax, PSYM = psym, AUTOLOOP=autoloop, $
           LOOPDATA=loopdata, HEADER=header, NOBACK=noback,                $
           NOPROMPT=noprompt, POST=post, OLDSTR=oldstr, POSITION=position, $
           CHARSIZE=charsize, WEIGHTFIT=weightfit, NLINES=nlines,          $
           VWIDTH=vwidth, FVWIDTH=fvwidth, FXWIDTH=fxwidth, FXPOS=fxpos,   $
           IDLINES=idlines, NOOUTPUT=nooutput, BLIN=blin, BQUAD=bquad,     $
           ADASTITLE=adastitle, UTITLE=utitle, PROC=proc,                  $
	   WAIT_TIME=wait_time, PARAMVAL = paramval, MAXPARAMS = maxparams,  $
	   BTYPE = btype, MISSING=missing

IF NOT (KEYWORD_SET(expval)) THEN BEGIN
  exptime = 1.0 
END ELSE BEGIN
  exptime=float(expval)
END
IF NOT (KEYWORD_SET(fortdir)) THEN fortdir=' '
IF NOT (KEYWORD_SET(xmin)) THEN xmin = !x.crange(0)
IF NOT (KEYWORD_SET(xmax)) THEN xmax = !x.crange(1)
IF NOT (KEYWORD_SET(ymin)) THEN ymin = !y.crange(0)
IF NOT (KEYWORD_SET(ymax)) THEN ymax = !y.crange(1)
IF NOT (KEYWORD_SET(psym)) THEN psym = 10          
IF NOT (KEYWORD_SET(autoloop)) THEN autoloop = 0          
IF NOT (KEYWORD_SET(loopdata)) THEN loopdata = 0          
IF NOT (KEYWORD_SET(data)) AND NOT (KEYWORD_SET(loopdata))THEN BEGIN
  message,'NEEDS DATA OR LOOPDATA SET',/informational
  stop
END ELSE IF NOT (KEYWORD_SET(data)) THEN BEGIN
  data = loopdata(*,0)
END
IF (KEYWORD_SET(autoloop)) AND NOT (KEYWORD_SET(loopdata)) THEN BEGIN
  message,'NEEDS EXTRA DATA FOR AUTO LOOP',/informational
  stop
END
IF NOT (KEYWORD_SET(header)) THEN header=' '
IF NOT (KEYWORD_SET(noback)) THEN noback=0   
IF NOT (KEYWORD_SET(noprompt)) THEN noprompt=0   
IF NOT (KEYWORD_SET(post)) THEN post=0   
IF NOT (KEYWORD_SET(oldstr)) THEN oldstr=0   
IF NOT (KEYWORD_SET(position)) THEN position=[0.1,0.1,0.9,0.9]
IF NOT (KEYWORD_SET(charsize)) THEN charsize=(!d.y_vsize/!d.y_ch_size)/60.0
IF NOT (KEYWORD_SET(weightfit)) THEN weightfit=0
IF NOT (KEYWORD_SET(nlines)) THEN nlines=0
IF NOT (KEYWORD_SET(vwidth)) THEN vwidth=0
IF NOT (KEYWORD_SET(fvwidth)) THEN fvwidth=0
IF NOT (KEYWORD_SET(fxwidth)) THEN BEGIN
  fxwidth=0
END ELSE BEGIN
  if vwidth eq 1 then begin
    message,'INCOMPATIBLE USE OF KEYWORDS VWIDTH AND FXWIDTH',/informational
    stop
  end else if vwidth eq 0 then begin
    vwidth=2
  end 
END
IF NOT (KEYWORD_SET(fxpos)) THEN fxpos=0
IF NOT (KEYWORD_SET(idlines)) THEN idlines=0         
IF NOT (KEYWORD_SET(nooutput)) THEN nooutput=0         
IF NOT (KEYWORD_SET(blin)) THEN blin=0
IF NOT (KEYWORD_SET(bquad)) THEN bquad=0
IF NOT (KEYWORD_SET(adastitle)) THEN adastitle=' '
IF NOT (KEYWORD_SET(utitle)) THEN utitle=' '
IF NOT (KEYWORD_SET(proc)) THEN proc=0
IF NOT (KEYWORD_SET(wait_time)) THEN wait_time=0.25
IF NOT (KEYWORD_SET(missing)) THEN missing=-100.
IF NOT (KEYWORD_SET(btype)) THEN btype=0

!quiet=0
fail_cycle1=0
label2=''
!error=0
CATCH, error_handle
if error_handle ne 0 then goto, label2

               ;**** check on pixel range ****

xrange=xmax-xmin
if xrange gt 2048 then begin
  message,'XMAX-XMIN > 2048, DECREASE RANGE',/informational
  stop
end

               ;**** check on dimensions ****

inloop = 0      ; make counter to determine whether the looping
                ; has started.
loop_counter = -1
cdsdata=data
if autoloop eq 1 then begin
  dimntest = size(loopdata)
  dimnchk = dimntest(2)
end
               ;**** check that floating point-probably redundant ****

exptest=size(exptime)
if exptest(0) eq 0 then begin
  if exptest(1) ne 4 then exptime=float(exptime)
end else if exptest(0) eq 1 then begin
  exptime=exptime(0)
  if exptest(2) ne 4 then exptime=float(exptime)
end

datatest=size(cdsdata)
if datatest(2) ne 4 then cdsdata=float(cdsdata)
if long(xmax-xmin) ge datatest(1) then begin
  message,'CURRENT PLOT IS TOO LARGE, SPECIFY XMIN/XMAX',/informational
  stop
end

  nsize = long(xmax-xmin)+1

               ;**** Create x-arrays with pixel nos.  ****
               ;**** instead of just integers         ****

  x = indgen(nsize)+xmin
  if autoloop eq 1 then begin
    xl = dblarr(n_elements(x),dimntest(2))
    for k = 0, dimntest(2)-1 do xl(*,k)=x
  end

               ;**** Check if structure request is present ****

IF N_PARAMS() LT 1 THEN BEGIN
  make_struct = 0
END ELSE BEGIN
  if autoloop eq 1 then begin
    dim = dimnchk
  end else begin
    dim=1
  end
  make_struct = 1
  ten_array = dblarr(10)
  str_array = strarr(10)
  int_array = intarr(6)
  if proc eq 1 then begin
    dim1 = n_elements(cdsdata(0:xmax-xmin))
  end else begin
    dim1 = n_elements(cdsdata(xmin:xmax))
  end
  y_array = dblarr(dim1)
  yafit=dblarr(dim1,10)
  fitdata = { b0      :  0.0     ,      eseb0   :  0.0     ,      $
              eclb0   :  0.0     ,      b1      :  0.0     ,      $
              eseb1   :  0.0     ,      eclb1   :  0.0     ,      $
              b2      :  0.0     ,      eseb2   :  0.0     ,      $
              eclb2   :  0.0     ,      w       :  ten_array,     $
              esewl   :  0.0     ,      eclwl   :  0.0     ,      $
              xo      :  ten_array,     esexo   :  ten_array,     $
              eclxo   :  ten_array,     h       :  ten_array,     $
              eseh    :  ten_array,     eclh    :  ten_array,     $
              esewv   :  ten_array,     eclwv   :  ten_array,     $
              flux    :  ten_array,     esef    :  ten_array,     $
              eclf    :  ten_array,     getfit  :  int_array,     $
              exptime :  0.0     ,      info    :  0       ,      $
              header  :  header,        xmin    :  xmin,          $
              xmax    :  xmax,          x       :  x,             $
              y       :  y_array,       ya      :  y_array,       $
		  yafit   :  yafit,						$
              nsize   :  nsize,         numline :  0,             $
              nfxl    :  0,             bchl    :  0.0,           $
              bchr    :  0.0,           lineid  :  str_array      $
             }
if autoloop eq 1 then fitdata=replicate(fitdata,dimnchk)
END

               ;**** copy header to structure ****

if make_struct eq 1 then begin
  fitdata(*).header=header
end

               ;**** open an output file ****

if nooutput eq 0 then begin
  openw,unitopen,'output.fit',/get_lun
end else unitopen=104

               ;**** create final plot with    ****
               ;**** ADAS options if requested ****

   if strtrim(adastitle,2) ne '' then begin
     title="ARBITRARY COUNTS VS. PIXEL NUMBER"
     if strtrim(utitle,2) ne '' then begin
       title = title + ': ' + strupcase(strtrim(utitle,2))
     end
     title = title + strtrim(adastitle,2)
     xtitle='Pixel Number'
     ytitle='Counts'
   end else begin
     title="ARBITRARY COUNTS VS. PIXEL NUMBER"
     if strtrim(utitle,2) ne '' then begin
       title = title + ': ' + strupcase(strtrim(utitle,2))
     end
     xtitle=''
     ytitle=''
   end

label1: 

  if inloop eq 0 then begin

               ;**** determine whether drop outs exist ****

    if proc eq 1 then begin
      xw = x(where(cdsdata(xmin-xmin:xmax-xmin) ne missing))
      yw = cdsdata(where(cdsdata(xmin-xmin:xmax-xmin) ne missing))
      y = cdsdata(xmin-xmin:xmax-xmin)
      plot,x,y,xrange=[xmin,xmax],/xstyle,yrange=[ymin,ymax],$
           /ystyle, psym=psym,position=position,charsize=charsize
    end else begin
      xw = x(where(cdsdata(xmin:xmax) ne missing))
      yw = cdsdata(where(cdsdata(xmin:xmax) ne missing)+xmin)
      y = cdsdata(xmin:xmax)
      plot,x,y,xrange=[xmin,xmax],/xstyle,yrange=[ymin,ymax],/ystyle,$
           psym=psym,position=position,charsize=charsize
    end
    yw = yw/exptime
    qsecs = make_array(n_elements(yw), /double, value = exptime*4)  

    xo=dblarr(10)
    w=xo
    getfit=intarr(6)
    numline=0
    lbw = numline & bslope=numline & bcrv=numline & lwv = numline & nfxl = numline
    bch=0.0
    lineid=strarr(10)

               ;**** copy old structure values to variables ****

IF (KEYWORD_SET(oldstr)) THEN BEGIN
  numline = oldstr.numline
  nfxl    = oldstr.nfxl
  xo      = oldstr.xo
  w       = oldstr.w
  if oldstr.getfit(1) eq 0 then begin
    for zz = 0, n_elements(w)-1 do w(zz) = oldstr.w(0)
  end
  getfit  = oldstr.getfit
  bchl    = oldstr.bchl
  bchr    = oldstr.bchr
  bch     = abs(oldstr.bchr-oldstr.bchl)/exptime
  lineid  = oldstr.lineid

               ;**** check old structure on same x-range ****

  for irng = 0,numline-1 do begin
    if xo(irng) gt max(x) then begin
        message,'OLD STRUCTURE FITTED TO A DIFFERENT RANGE',/informational
        stop
    end
  endfor
END ELSE BEGIN 
  if nlines eq 0 then begin
    read,'Enter total no. of lines to fit :',numline
  end else begin
    numline=nlines
  end

               ;**** check fxpos is an array and mark ****
               ;**** fix option flag for later        ****

chkfix=size(fxpos)
if chkfix(0) eq 0 then begin
  if fxpos eq 0 then begin
    fxopt = 0
    read,'Enter no. of lines with fixed positions :',nfxl
    if nfxl eq 0 then begin
      t=[0]
    end else t = indgen(nfxl)
    if numline eq nfxl then begin
      xt=[0]
    end else xt = nfxl+indgen(numline-nfxl)
  end else begin
    fxopt = 1
    nfxl = 1
    t = [0]
    xt=1+indgen(10)
  end
end else begin
  t=where(fxpos ne 0.0)
  xt=where(fxpos eq 0.0)
  if t(0) ne -1 then begin
    fxopt = 2
    nfxl=n_elements(where(fxpos ne 0.0))
  end else begin
    nfxl = 0
  end
end
icntline = 1

               ;**** check idlines is an array and ****
               ;**** mark ID option flag for later ****

chkid=size(idlines)
if chkid(0) eq 0 then begin
  if idlines eq 0 then begin
    idopt = 0
  end else idopt = 1
end else begin
  st=where(strtrim(idlines,2) ne '')
  if st(0) ne -1 then begin
    idopt = 2
  end else idopt = 3
end
resp=' '
oldstring='****************************************'

               ;**** get user requests and fill ****
               ;**** arrays for FORTRAN ****

if nfxl ne 0 then begin
for i = (numline-nfxl), numline-1 do begin
  if idopt eq 0 then begin
    read,'Do you wish to make a preliminary line identification ? ',resp
    if resp eq 'y' then begin
      read,'Enter an ID : ',resp
      lineid(i) = strtrim(resp)
    end else begin
      lineid(i) = 'u'
    end
  end else if idopt eq 1 then begin
    lineid(i) = idlines
  end else if idopt eq 2 then begin
    lineid(i) = idlines(t(icntline-1)) 
  end else if idopt eq 3 then begin
    lineid(i) = 'u'
  end
  sline = strtrim(icntline,2)
  if fxopt eq 0 then begin
    read,'Enter the fixed position for line '+sline+' :',a
    xo(i) = a
  end else if fxopt eq 1 then begin
    xo(i) = fxpos
  end else if fxopt eq 2 then begin
    xo(i) = fxpos(t(icntline-1))
  end
  plots,xo(i),[!y.crange]
   if fxwidth eq 0 then begin
     xyouts,0.1,0.95,strtrim(oldstring,2),/normal,color=0
     xyouts,0.1,0.95,'Move cursor to left side line at FWHM :',/normal
       oldstring='Move cursor to left side line at FWHM :'
     cursor,wl,b,/down
     oplot,[wl],[b],psym=6
     xyouts,0.1,0.95,strtrim(oldstring,2),/normal,color=0
     xyouts,0.1,0.95,'Move cursor to right side line at FWHM :',/normal
       oldstring='Move cursor to right side line at FWHM :'
     cursor,wr,b,/down
     oplot,[wr],[b],psym=6
     w(i) = abs(wr-wl)
   end
    icntline=icntline+1
endfor
end

if nfxl ne numline then begin
for i = 0,(numline-nfxl)-1 do begin
  if idopt eq 0 then begin
      read,'Do you wish to make a preliminary line identification ? ',resp
      if resp eq 'y' then begin
        read,'Enter an ID : ',resp
        lineid(i) = strtrim(resp)
      end else begin
        lineid(i) = 'u'
      end
  end else if idopt eq 1 then begin
    lineid(i) = idlines
  end else if idopt eq 2 then begin
    if strtrim(idlines(xt(i)),2) eq '' then begin
      lineid(i) = 'u'
    end else lineid(i) = idlines(xt(i))
  end else if idopt eq 3 then begin
    lineid(i) = 'u'
  end
  sline = strtrim(icntline,2)
  xyouts,0.1,0.95,strtrim(oldstring,2),/normal,color=0
  xyouts,0.1,0.95,'Move cursor to line peak for line '+sline+' :',/normal
    oldstring='Move cursor to line peak for line '+sline+' :'
  cursor,a,b,/down
  oplot,[a],[b],psym=6
  xo(i) = a
  if fxwidth eq 0 then begin
    xyouts,0.1,0.95,strtrim(oldstring,2),/normal,color=0
    xyouts,0.1,0.95,'Move cursor to left side line at FWHM :',/normal
      oldstring='Move cursor to left side line at FWHM :'
    cursor,wl,b,/down
    oplot,[wl],[b],psym=6
    xyouts,0.1,0.95,strtrim(oldstring,2),/normal,color=0
    xyouts,0.1,0.95,'Move cursor to right side line at FWHM :',/normal
      oldstring='Move cursor to right side line at FWHM :'
    cursor,wr,b,/down
    oplot,[wr],[b],psym=6
    w(i) = abs(wr-wl)
  end
  icntline=icntline+1
endfor
end

if noback eq 0 then begin
  xyouts,0.1,0.95,strtrim(oldstring,2),/normal,color=0
  xyouts,0.1,0.95,'Move cursor to LOWER background :',/normal
    oldstring='Move cursor to LOWER background :'
  cursor,a,b,/down
  oplot,[a],[b],psym=6
  bchl = b
  xyouts,0.1,0.95,strtrim(oldstring,2),/normal,color=0
  xyouts,0.1,0.95,'Move cursor to UPPER background :',/normal
    oldstring='Move cursor to UPPER background :'
  cursor,a,b,/down
  oplot,[a],[b],psym=6
  bchr = b
  bch = abs(bchr-bchl)/exptime
end else begin
  bchl=0.0
  bchr=0.0
end

               ;**** confirm all parameters ****

  xyouts,0.1,0.95,strtrim(oldstring,2),/normal,color=0
  xyouts,0.1,0.95,'Click LEFT/RIGHT mouse button to CONFIRM/DISMISS selections',/normal
  cursor,a,b,/down
  if !mouse.button eq 4 then begin
    goto,label1
  end

if vwidth eq 0 then begin
  read,'Line widths to vary (Y=1,N=0) ?',lbw
end else begin
  if vwidth eq 1 then lbw=1
end
if (lbw eq 1) then begin
  if fvwidth eq 0 then begin
    read,'Fit different widths to lines (Y=1,N=0) ?',lwv
  end else begin
    if fvwidth eq 1 then lwv=1
  end
end else begin
  if fxwidth eq 0 then begin
    read,'Input fixed width : ',widt
  end else begin
    widt = fxwidth
  end
  for i=0, n_elements(w)-1 do w(i)=widt
end

kwt=0
if weightfit eq 0 then begin
  read,'Weighted fit (Y=1,N=0) ?',kwt
end else begin
  if weightfit eq 1 then kwt=1 
end

if noback eq 0 then begin
  if blin eq 0 then begin
    read,'Base to have a slope (Y=1,N=0) ?',bslope
  end else if blin eq 1 then begin
    bslope=1
  end else if blin eq 2 then begin
    bslope=0
  end
  if bquad eq 0 then begin
    read,'Base to have a curve (Y=1,N=0) ?',bcrv
  end else if bquad eq 1 then begin
    bcrv=1
  end else if bquad eq 2 then begin
    bcrv=0
  end
end else begin
  bslope = 0
  bcrv = 0
end

getfit(0) = lbw
getfit(1) = lwv
getfit(2) = bslope
getfit(3) = bcrv
getfit(4) = kwt
if noback eq 0 then begin
  getfit(5) = 1     
end else begin
  getfit(5) = 0     
end
END

               ;**** make plot display ****

   plot,x,y,xrange=[xmin,xmax],/xstyle,yrange=[ymin,ymax],/ystyle,$
        psym=psym,position=position,charsize=charsize,xtitle=xtitle,$
        ytitle=ytitle
   xyouts,0.1,0.98,title,/normal

               ;**** copy values for auto-cycling ****

if autoloop eq 1 then begin
  autonumline = numline
  autonfxl = nfxl
  autoxo = xo
  autow = w
  autobch = bch
  autogetfit = getfit
  autolineid = lineid
  autoparamval = paramval
  autobtype = btype
end

  end else if inloop eq 1 then begin

    IF (KEYWORD_SET(oldstr)) THEN BEGIN
      numline = oldstr.numline
      nfxl    = oldstr.nfxl
      xo      = oldstr.xo
      w       = oldstr.w
      if oldstr.getfit(1) eq 0 then begin
        for zz = 0, n_elements(w)-1 do w(zz) = oldstr.w(0)
      end
      getfit  = oldstr.getfit
      bchl    = oldstr.bchl
      bchr    = oldstr.bchr
      bch     = abs(oldstr.bchr-oldstr.bchl)/exptime
      lineid  = oldstr.lineid

               ;**** check old structure on same x-range ****

      for irng = 0,numline-1 do begin
        if xo(irng) gt max(x) then begin
            message,'OLD STRUCTURE FITTED TO A DIFFERENT RANGE',/informational
            stop
        end
      endfor
    END 

    if proc eq 1 then begin
      xw = xl(where(loopdata(xmin-xmin:xmax-xmin,loop_counter) ne missing),loop_counter)
      yw = loopdata(where(loopdata(xmin-xmin:xmax-xmin,loop_counter) ne missing),loop_counter)
      y = loopdata(xmin-xmin:xmax-xmin,loop_counter)
      plot,x,y,xrange=[xmin,xmax],/xstyle,yrange=[ymin,ymax],/ystyle,$
           psym=psym,position=position,charsize=charsize,xtitle=xtitle,$
           ytitle=ytitle
    end else begin
      xw = xl(where(loopdata(xmin:xmax,loop_counter) ne missing),loop_counter)
      yw = loopdata(where(loopdata(xmin:xmax,loop_counter) ne missing)+xmin,loop_counter)
      y = loopdata(xmin:xmax,loop_counter)
      plot,x,y,xrange=[xmin,xmax],/xstyle,yrange=[ymin,ymax],/ystyle,$
           psym=psym,position=position,charsize=charsize,xtitle=xtitle,$
           ytitle=ytitle
    end
    xyouts,0.1,0.98,title,/normal
    yw = yw/exptime
    qsecs = make_array(n_elements(yw), /double, value = exptime*4)
  end


               ;**** declare other variables for ****
               ;**** receiving from UNIX pipe    ****
               ;**** after the calculation is    ****
               ;**** finished.                   ****
ya=yw
jf=0
ifail=jf
b0=bch & b1=bch & b2=bch & eseb0=bch & eseb1 =bch & eseb2=bch
eclb0=bch & eclb1=bch & eclb2=bch & esewl=bch & eclwl=bch
h=w & flux=w & esexo =w & eseh=w & esef=w 
eclxo=w & eclh=w & eclf=w
esewv=w & eclwv=w

               ;**** spawn FORTRAN process ****
spawn, fortdir + '/adas602.out', unit=pipe,/noshell,PID=pid

               ;**** print data to pipe ****

printf,pipe,format='(1x,i4)',min([nsize,n_elements(yw)])
for i = 0,min([nsize,n_elements(yw)])-1 do begin

               ;**** temporarily set all zeroes to 0.01 ****
               ;**** to deal with zero counts error ****

 if yw(i) eq 0 then yw(i)=0.000001

               ;**** should above line be 'eq -1' for miss=-1?****
               ;**** send drop out free data to the fitting code ****

  printf,pipe,format='(1x,e13.6,1x,e13.6,1x,e13.6)',xw(i),yw(i),qsecs(i)
endfor

if inloop eq 1 then begin
  printf,pipe,format='(1x,i2)',autonumline
  printf,pipe,format='(1x,i2)',autonfxl
  for i = 0,autonumline-1 do begin
    printf,pipe,format='(1x,e13.6,1x,e13.6)',autoxo(i),autow(i)
  endfor
  printf,pipe,format='(1x,e13.6)',autobch
  for i = 0,5 do begin
    printf,pipe,format='(1x,i1)',autogetfit(i)
  endfor
  for i=0,maxparams-1 do begin
  	printf,pipe,format='(1x,e13.6)',autoparamval(i)
  endfor
  printf,pipe,format='(1x,i2)',autobtype
  	
               ;**** copy initial position estimates **** 
  xoinit=autoxo           
end else begin
  printf,pipe,format='(1x,i2)',numline
  printf,pipe,format='(1x,i2)',nfxl
  for i = 0,numline-1 do begin
    printf,pipe,format='(1x,e13.6,1x,e13.6)',xo(i),w(i)
  endfor
  printf,pipe,format='(1x,e13.6)',bch
  for i = 0,5 do begin
    printf,pipe,format='(1x,i1)',getfit(i)
  endfor
  for i=0,maxparams-1 do begin
  	printf,pipe,format='(1x,e13.6)',paramval(i)
  endfor      
  printf,pipe,format='(1x,i2)',btype
  
               ;**** copy initial position estimates **** 
  xoinit=xo
end

               ;**** read data from pipe ****
;yafit = dblarr(min([nsize,n_elements(yw)]),numline)		   
for i = 0,min([nsize,n_elements(yw)])-1 do begin
  readf,pipe,format='(1x,e13.6)',yat 
  ya(i) = yat
  for k = 0, numline-1 do begin
  	readf,pipe,format='(1x,e13.6)',yat   
  	yafit(i,k)=yat
;	print,k,yafit(i,k)
  endfor
endfor
readf,pipe,format='(1x,i4,1x,i4)',jf,ifail
readf,pipe,format='(1x,e13.6,1x,e13.6,1x,e13.6)',b0,b1,b2
readf,pipe,format='(1x,e13.6,1x,e13.6,1x,e13.6,1x,e13.6)',$
           eseb0,eseb1,eseb2,esewl
readf,pipe,format='(1x,e13.6,1x,e13.6,1x,e13.6,1x,e13.6)',$
           eclb0,eclb1,eclb2,eclwl
for i = 0,numline-1 do begin
  readf,pipe,format='(1x,e13.6,1x,e13.6,1x,e13.6,1x,e13.6)',$
             xot,ht,wt,ft
             xo(i)=xot
             h(i)=ht
             w(i)=wt
             flux(i)=ft
  readf,pipe,format='(1x,e13.6,1x,e13.6,1x,e13.6)',$
             esexot,eseht,eseft
             esexo(i)=esexot
             eseh(i)=eseht
             esef(i)=eseft
  readf,pipe,format='(1x,e13.6,1x,e13.6,1x,e13.6)',$
             eclxot,eclht,eclft
             eclxo(i)=eclxot
             eclh(i)=eclht
             eclf(i)=eclft
  readf,pipe,format='(1x,e13.6,1x,e13.6)',$
             esewvt,eclwvt
             esewv(i)=esewvt
             eclwv(i)=eclwvt
endfor

               ;**** correct for division by exptime ****
               ;**** before passing to FORTRAN ****

yw = yw*exptime
ya = ya*exptime
yafit = yafit*exptime
b0=b0*exptime
b1=b1*exptime
b2=b2*exptime
h=h*exptime
eseb0=eseb0*exptime
eseb1=eseb1*exptime
eseb2=eseb2*exptime
eseh=eseh*exptime
eclb0=eclb0*exptime
eclb1=eclb1*exptime
eclb2=eclb2*exptime
eclh=eclh*exptime
flux=flux*exptime
esef=esef*exptime
eclf=eclf*exptime

               ;**** overplot results ****
oplot,xw,ya

               ;**** now overplot individual line contributions ****

;yafit = dblarr(min([nsize,n_elements(yw)]),numline)
bfit = dblarr(min([nsize,n_elements(yw)]))
for j = 0,min([nsize,n_elements(yw)])-1 do begin
  for i = 0,numline-1 do begin
    if xo(i) gt max(x) or w(0) lt 0 then begin
        message,'UNPHYSICAL FIT ACHIEVED - TREATED AS ERROR',/informational
        !error = -1
        goto,label2
    end
    dr = abs(xw(j)-xo(i))/w(i)
;    yafit(j,i) = h(i)*exp(-1.0*dr*dr)
  endfor
  bfit(j) = b0+b1*xw(j)+b2*xw(j)*xw(j)
endfor
oplot,xw,bfit,linestyle=1

               ;**** copy number of lines to structure ****

if make_struct eq 1 then begin
   fitdata(*).numline = numline
   fitdata(*).nfxl = nfxl
   fitdata(*).bchl = bchl
   fitdata(*).bchr = bchr
end

for i = 0, numline-1 do oplot,xw,yafit(*,i),linestyle=i+1

	; **** Plot residual ****

widget_control, residid, get_value=resval
!error=0		; **** Hack - don't know why above line returns error (RM 6/2001)

wset,resval.win
plot, xw, yw-ya, xrange=[xmin,xmax], /xstyle, xtitle=xtitle, ytitle=ytitle
rtotal=total(((yw-ya)/sqrt(yw))^2)

rstring="!4R!Xsquares="+strtrim(string(rtotal),2)
xyouts,0.8,0.05,rstring,/normal
widget_control, graphid, get_value=grval
wset,grval.win

if autoloop eq 1 and inloop eq 0 then begin
  yafit_st = dblarr(nsize,numline,dimnchk)
  bfit_st = dblarr(nsize,dimnchk)
end
               ;**** add cycle counter ****
if autoloop eq 1 then begin
  lnumber=loop_counter+1
  strnumber='CYCLE COUNTER (IDL INDEX) : '+strtrim(lnumber,2)
  gap=(!y.crange(1)-!y.crange(0))/20.*1.5
  xyouts,[!x.crange(0)],[!y.crange(0)-gap],[strnumber]
end
 
               ;**** make postscript plot ****

if post eq 1 then begin
  dpsname=' '
  read,'Enter a File Name :',dpsname
  set_plot,'ps'
  device,filename=strtrim(dpsname,2)
  plot,x,y,xrange=[xmin,xmax],/xstyle,yrange=[ymin,ymax],/ystyle,psym=10
  oplot,xw,ya
  device,/close
  set_plot,'x'
end

if inloop eq 0 and noprompt eq 1 then begin
  if nooutput eq 0 then print,'**** PRINTOUT FORCED TO OUTPUT.FIT FILE ****'
end

if (autoloop eq 1 and inloop eq 1) or (autoloop eq 0) then begin

               ;**** store individual line contributions ****
               ;**** and allow variable data length in   ****
               ;**** automatic fitting. Forced omission  ****
               ;**** of data drop outs as they cause     ****
               ;**** FORTRAN to crash.                   ****

if autoloop ne 0 then begin ; redundant?
  sz_yafit_st=size(yafit_st)
  sz_yafit=size(yafit)
  if sz_yafit_st(1) ne sz_yafit(1) then begin
    yt_st = dblarr(nsize,numline)
    bt_st = dblarr(nsize)
    for i=0,sz_yafit(1)-1 do yt_st(i,*)=yafit(i,*)
    for i=sz_yafit(1),sz_yafit_st(1)-1 do yt_st(i,*)=missing     
    for i=0,sz_yafit(1)-1 do bt_st(i)=bfit(i)
    for i=sz_yafit(1),sz_yafit_st(1)-1 do bt_st(i)=missing     
    for k = 0, autonumline-1 do begin
      yafit_st(*,k,loop_counter) = yt_st(*,k)
      bfit_st(*,loop_counter) = bt_st(*)
    endfor
  end else begin
    for k = 0, autonumline-1 do begin
      yafit_st(*,k,loop_counter) = yafit(*,k)
      bfit_st(*,loop_counter) = bfit(*)
    endfor
  end
end else begin
  loop_counter = loop_counter+1
end

               ;**** save fits to y to the structure ****
               ;**** replacing drop outs if they exist ****

if make_struct eq 1 then begin
    fitdata(loop_counter).y(*)=y
    fitdata(loop_counter).ya(*)=missing
    fitdata(loop_counter).ya(xw-xmin)=ya
    fitdata(loop_counter).yafit=yafit
end

if noprompt eq 1 or nooutput eq 1 then begin
  wait,wait_time
  dchoice=1
end else begin
  read,'Print data to screen (0) or output.fit file (1) ?',dchoice 
end
if dchoice eq 0 then begin
  unit = -1
end else begin
  unit = unitopen
end

               ;**** print data to file or structure ****

if nooutput eq 0 then begin
printf,unit,'FQV - Fitted Quantity Value'
printf,unit,'ESE - Estimated Standard Error expressed as a % of FQV'
printf,unit,'ECL - Estimated 95% Confidence Limit expressed as a % of FQV'
printf,unit,'                              FQV         ESE%        ECL%      '
printf,unit,'-------------------------------------------------------------------------------'
end
scr = '                        '
scrt = 'Base level b0:'
strput,scr,scrt
if b0 ne 0 then begin
   if nooutput eq 0 then begin
     printf,unit,format='(1a24,3(1x,e11.4))',$
                 scr,b0,abs(eseb0*100/b0),abs(eclb0*100/b0)
   end
   if make_struct eq 1 then begin
     fitdata(loop_counter).b0 = b0
     fitdata(loop_counter).eseb0 = abs(eseb0*100/b0)
     fitdata(loop_counter).eclb0 = abs(eclb0*100/b0)
   end
end else begin
   if nooutput eq 0 then begin
     printf,unit,format='(1a24,1x,e11.4)',scr,b0
   end
   if make_struct eq 1 then begin
     fitdata(loop_counter).b0 = b0
   end
end

scrt = 'Slope b1:     '
strput,scr,scrt
if b1 ne 0 then begin
   if nooutput eq 0 then begin
     printf,unit,format='(1a24,3(1x,e11.4))',$
                 scr,b1,abs(eseb1*100/b1),abs(eclb1*100/b1)
   end
   if make_struct eq 1 then begin
     fitdata(loop_counter).b1= b1
     fitdata(loop_counter).eseb1= abs(eseb1*100/b1)
     fitdata(loop_counter).eclb1 = abs(eclb1*100/b1)
   end
end else begin
   if nooutput eq 0 then begin
     printf,unit,format='(1a24,1x,e11.4)',scr,b1
   end
   if make_struct eq 1 then begin
     fitdata(loop_counter).b1 = b1
   end
end

scrt = 'Curve b2:     '
strput,scr,scrt
if b2 ne 0 then begin
   if nooutput eq 0 then begin
     printf,unit,format='(1a24,3(1x,e11.4))',$
                 scr,b2,abs(eseb2*100/b2),abs(eclb2*100/b2)
   end
   if make_struct eq 1 then begin
     fitdata(loop_counter).b2= b2
     fitdata(loop_counter).eseb2= abs(eseb2*100/b2)
     fitdata(loop_counter).eclb2= abs(eclb2*100/b2)
   end
end else begin
   if nooutput eq 0 then begin
     printf,unit,format='(1a24,1x,e11.4)',scr,b2
   end
   if make_struct eq 1 then begin
     fitdata(loop_counter).b2= b2
   end
end

if lwv eq 0 then begin
  scrt = 'Fitted width wh all lines:'
  strput,scr,scrt
  if w(0) ne 0 then begin
     if nooutput eq 0 then begin
       printf,unit,format='(1a24,3(1x,e11.4))',$
                   scr,w(0),abs(esewl*100/w(0)),abs(eclwl*100/w(0))
     end
     if make_struct eq 1 then begin
       fitdata(loop_counter).w(0) = w(0)
       fitdata(loop_counter).esewl= abs(esewl*100/w(0))
       fitdata(loop_counter).eclwl = abs(eclwl*100/w(0))
     end
  end else begin
     if nooutput eq 0 then begin
       printf,unit,format='(1a24,1x,e11.4)',scr,w(0)
     end
     if make_struct eq 1 then begin
       fitdata(loop_counter).w(0) = w(0)
     end
  end
end

for i = 0,numline-1 do begin
  scr1 = strtrim(string(i+1),2)
  scrt = 'line '+scr1+' positions, xo:   '
  strput,scr,scrt
   if xo(i) ne 0 then begin
     if nooutput eq 0 then begin
       printf,unit,format='(1a24,3(1x,e11.4))',$
                   scr,xo(i),abs(esexo(i)*100/xo(i)),abs(eclxo(i)*100/xo(i))
     end
     if make_struct eq 1 then begin
       fitdata(loop_counter).xo(i) = xo(i)
       fitdata(loop_counter).esexo(i) = abs(esexo(i)*100/xo(i))
       fitdata(loop_counter).eclxo(i) = abs(eclxo(i)*100/xo(i))
       fitdata(loop_counter).lineid(i) = lineid(i)
     end
   end else begin
     if nooutput eq 0 then begin
       printf,unit,format='(1a24,1x,e11.4)',scr,xo(i)
     end
     if make_struct eq 1 then begin
       fitdata(loop_counter).xo(i) = xo(i)
       fitdata(loop_counter).lineid(i) = lineid(i)
     end
   end

  scrt = 'line '+scr1+' heights, h:      '
  strput,scr,scrt
   if h(i) ne 0 then begin
     if nooutput eq 0 then begin
       printf,unit,format='(1a24,3(1x,e11.4))',$
                   scr,h(i),abs(eseh(i)*100/h(i)),abs(eclh(i)*100/h(i))
     end
     if make_struct eq 1 then begin
       fitdata(loop_counter).h(i) = h(i)
       fitdata(loop_counter).eseh(i) = abs(eseh(i)*100/h(i))
       fitdata(loop_counter).eclh(i) = abs(eclh(i)*100/h(i))
     end
   end else begin
     if nooutput eq 0 then begin
       printf,unit,format='(1a24,1x,e11.4)',scr,h(i)
     end
     if make_struct eq 1 then begin
       fitdata(loop_counter).h(i)= h(i)
     end
   end

  scrt = 'Line '+scr1+' fitted width wh:     '
  strput,scr,scrt
    if lwv eq 1 then begin
      if w(i) ne 0 then begin
        if nooutput eq 0 then begin
          printf,unit,format='(1a24,3(1x,e11.4))',$
                 scr,w(i),abs(esewv(i)*100.0/w(i)),abs(eclwv(i)*100.0/w(i))
        end
        if make_struct eq 1 then begin
          fitdata(loop_counter).w(i)= w(i)
          fitdata(loop_counter).esewv(i)= abs(esewv(i)*100/w(i))
          fitdata(loop_counter).eclwv(i)= abs(eclwv(i)*100/w(i))
        end
      end else begin
        if nooutput eq 0 then begin
          printf,unit,format='(1a24,1x,e11.4)',scr,w(i)
        end
        if make_struct eq 1 then begin
          fitdata(loop_counter).w(i)= w(i)
        end
      end
    end

  scrt = 'line '+scr1+' flux:            '     
  strput,scr,scrt
   if flux(i) ne 0 then begin
     if nooutput eq 0 then begin
       printf,unit,format='(1a24,3(1x,e11.4))',$
              scr,flux(i),abs(esef(i)*100/flux(i)),abs(eclf(i)*100/flux(i))
     end
     if make_struct eq 1 then begin
       fitdata(loop_counter).flux(i)= flux(i)
       fitdata(loop_counter).esef(i)= abs(esef(i)*100/flux(i))
       fitdata(loop_counter).eclf(i)= abs(eclf(i)*100/flux(i))
     end
   end else begin
     if nooutput eq 0 then begin
       printf,unit,format='(1a24,1x,e11.4)',scr,flux(i)*exptime
     end
     if make_struct eq 1 then begin
       fitdata(loop_counter).flux(i)= flux(i)
     end
   end
endfor

str1 = ['Widths free to vary, kwfree:',$
        'Variable line widths to be fitted, kindep:',$
        'Base able to have linear slope, kb1:',$
        'Base able to have parabolic shape, kb2:',$
        'Weighted Fit, kwt:',$
        'Fit to background disabled, kb0:'] 
str2 = strarr(6)
for i = 0,5 do begin
  if make_struct eq 1 then begin
    fitdata(*).getfit(i) = getfit(i)
  end
  if getfit(i) eq 1 then begin
    str2(i) = ' Y'
  end else begin
    str2(i) = ' N'
  end
  string='                                          '
  strput,string,str1(i)
  string = string+str2(i)
  if nooutput eq 0 then printf,unit,format='(1a44)',string
endfor 
if nooutput eq 0 then printf,unit,'Exptime:',exptime
if nooutput eq 0 then printf,unit,'INFO error flag from MINPACK routine:',ifail
 if make_struct eq 1 then begin
    fitdata(loop_counter).exptime = exptime
    fitdata(loop_counter).info  = ifail
 end
end

label2: free_lun, pipe
   
               ;**** If error occured fill appropriate parts ****
               ;**** of structure with -100's                ****

if !error ne 0 then begin
  if loop_counter eq -1 then begin
    loop_counter=0
    lnumber=0
    fail_cycle1=1
  end
  fitdata(loop_counter).b0 = -100  
  fitdata(loop_counter).eseb0 = -100  
  fitdata(loop_counter).eclb0 = -100  
  fitdata(loop_counter).b1 = -100  
  fitdata(loop_counter).eseb1 = -100  
  fitdata(loop_counter).eclb1 = -100  
  fitdata(loop_counter).b2 = -100  
  fitdata(loop_counter).eseb2 = -100  
  fitdata(loop_counter).eclb2 = -100  
  fitdata(loop_counter).exptime = -100  
  fitdata(loop_counter).info = -100  
  fitdata(loop_counter).y = -100  
  fitdata(loop_counter).ya = -100  
  fitdata(loop_counter).yafit = -100 
  if getfit(1) eq 0 then fitdata(loop_counter).esewl = -100  
  if getfit(1) eq 0 then fitdata(loop_counter).eclwl = -100  
  for i=0, numline-1 do begin
    fitdata(loop_counter).w(i) = -100  
    if getfit(1) eq 1 then fitdata(loop_counter).esewv(i) = -100  
    if getfit(1) eq 1 then fitdata(loop_counter).eclwv(i) = -100  
    fitdata(loop_counter).h(i) = -100  
    fitdata(loop_counter).eseh(i) = -100  
    fitdata(loop_counter).eclh(i) = -100  
    fitdata(loop_counter).xo(i) = -100  
    fitdata(loop_counter).esexo(i) = -100  
    fitdata(loop_counter).eclxo(i) = -100  
    fitdata(loop_counter).flux(i) = -100  
    fitdata(loop_counter).esef(i) = -100  
    fitdata(loop_counter).eclf(i) = -100  
  end
  message,'ERROR IN IDL INDEX : '+strtrim(loop_counter,2),/informational
  !error=0
end

if autoloop eq 0 then begin
  if noprompt eq 0 then begin
    read,'Do you want to continue(1-yes, 0-no):',cont

    if cont eq 1 then begin
     loop_counter = -1
     goto,label1
    end 
  end
end else begin
  if loop_counter lt dimnchk-1 then begin
    if fail_cycle1 ne 1 then begin
      loop_counter = loop_counter+1
      inloop = 1
      goto,label1
    end else begin
      message,'ERROR OCCURED BEFORE CYCLING VARIABLES DEFINED',/informational
      message,'EXITING TO PROCESSING SCREEN',/informational
      goto, labelend
    end
  end
end

if n_elements(unit) ne 0 then begin
 if unit ne -1 then begin
  free_lun,unit
 end
end

labelend:

end
 
