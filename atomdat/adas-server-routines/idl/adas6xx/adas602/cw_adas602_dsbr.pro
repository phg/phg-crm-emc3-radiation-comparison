; Copyright (c) 1995, Strathclyde University .
; SCCS info: Module @(#)$Header: /home/adascvs/idl/adas6xx/adas602/cw_adas602_dsbr.pro,v 1.1 2004/07/06 12:53:57 whitefor Exp $ Date $Date: 2004/07/06 12:53:57 $
;
;+
; PROJECT:
;       ADAS IBM MVS to   UNIX conversion
;
; NAME:
;	CW_ADAS602_DSBR()
;
; PURPOSE:
;	Dataset name and a button to browse the dataset comments.
;
; EXPLANATION:
;	This function produces a compound widget which allows the
;	user to browse the comment lines from a pre-defined text
;	file.  The widget consists of a label showing the name
;	of the file and a button 'Browse Comments' to initiate the
;	browsing process.
;
;	This widget handles its child events internally and does not generate
;	any events.
;
;	This widget does not change any of its inputs and so has no
;	'value' which can be retrieved.
;
; USE:
;	An example of how to use this function;
;
;		filename='/disk2/bowen/adas/file.dat'
;		base=widget_base()
;		id=cw_adas_dsbr(base,filename)
;		widget_control,base,/realize
;		rc=widget_event()
;
;	This example will not return from widget_event since cw_adas_dsbr
;	does not itself generate any events.  Kill the window to stop
;	the event processing.
;
; INPUTS:
;       PARENT   - The ID of the parent widget.
;
;	DATAFILE - String; Data file name, e.g '/disk2/bowen/adas/file.dat'
;		   Note: It is the responsibility of the caller to
;		   ensure that the file name is valid and readable.
; 
;	FLAG	 - 0 => input data in standard form
;		   1 => input data in processed form
;		   2 => input data in other form
;
; OPTIONAL INPUTS:
;	None.
;
; OUTPUTS:
;       The return value of this function is the ID of the compound
;       widget which is of type LONG integer.
;
; OPTIONAL OUTPUTS:
;       None
;
; KEYWORD PARAMETERS:
;       FONT     - A font to use for all text in this widget.
;
;	UVALUE	 - A user value for this widget.
;
;	BUTTON	 - A string to be used as the button label. The default
;		   is 'Browse Comments'
;
;	NROWS	 - Integer - if set to a value other than 1 then the
;		   file name and associated label are placed on separate
;		   rows (depending on the setting or not of the ROW
;		   keyword)
;
;	ROW	 - If this keyword is set then the layout of the widgets
;		   is in a row rather than a column.
;
; CALLS:
;	XXTEXT602		Popup window holding datafile comments for browsing.
;       See side effects for other related widget management routines.
;
; SIDE EFFECTS:
;	DSBR602_EVENT()
;
; CATEGORY:
;	Compound Widget
;
; WRITTEN:
;       David Brooks, University of Strathclyde, 1997
;
; MODIFIED:
;       1.0     David Brooks    
;               Based on cw_adas_dsbr.pro v1.11 .
;	1.1	Richard Martin
;               Changed documentation + put under SCCS control.
;
; VERSION:
;       1.0     ?
;	1.1	18-06-97
;
;-
;-----------------------------------------------------------------------------

FUNCTION dsbr602_event, event


		;**** Base ID of compound widget ****

    base=event.handler

                ;**********************************************
                ;**** Retrieve the user value state        ****
                ;**** Get id of first_child widget because ****
                ;**** user value "state" is stored there   ****
                ;**********************************************

    first_child = widget_info(base, /child)
    widget_control, first_child, get_uvalue=state,/no_copy

		;************************
		;**** Process Events ****
		;************************

    CASE event.id OF

        state.browseid: xxtext602, state.datafile, state.flag, font=state.font

        ELSE:

    ENDCASE

		;**** Save the new state structure ****

    widget_control, first_child, set_uvalue=state, /no_copy

    RETURN, 0L

END

;-----------------------------------------------------------------------------

FUNCTION cw_adas602_dsbr, parent, datafile, flag, FONT=font, UVALUE=uvalue,	$
                       BUTTON=button, FILELABEL=filelabel, NROWS=nrows,	$
                       ROW=row

    IF (N_PARAMS() LT 1) THEN MESSAGE, 'Must specify PARENT for cw_adas_reac2'

		;**** Set defaults for keywords ****

    IF NOT (KEYWORD_SET(font)) THEN font=''
    IF NOT (KEYWORD_SET(uvalue))  THEN uvalue=0
    IF NOT (KEYWORD_SET(button)) THEN button='Browse Comments'
    IF NOT (KEYWORD_SET(filelabel)) THEN filelabel='Data File Name: '
    IF NOT (KEYWORD_SET(nrows)) THEN nrows=1
    IF (KEYWORD_SET(row)) THEN rowcol=1 ELSE rowcol=0

		;**** Create the main base for the widget ****

    main = WIDGET_BASE(parent, UVALUE = uvalue,				$
		       EVENT_FUNC = "dsbr602_event", 			$
		       /COLUMN)

                ;******************************************************
                ;**** Create a dummy widget just to hold value of *****
                ;**** "state" variable so as not to get confused  *****
                ;**** with any other values. Adopt IDL practice   *****
                ;**** of using first child widget                 *****
                ;******************************************************

    first_child = widget_base(main)

    if rowcol eq 0 then begin
        cwid = widget_base(first_child, /column)
    endif else begin
	cwid = widget_base(first_child, /row)
    endelse

		;**** Dataset name ****

    if nrows ne 1 then begin
        rc  = widget_label(cwid, value=filelabel, font=font)
        rc2 = widget_label(cwid, value=datafile, font=font)
    endif else begin
        rc = widget_label(cwid, value=filelabel+datafile, font=font)
    endelse

		;**** Comments browse button ****

    browseid = widget_button(cwid, value=button, font=font)

		;**** Create state structure ****

    new_state =  { BROWSEID:browseid, DATAFILE:datafile, FONT:font, $
                   FLAG:flag }

		;**** Save initial state structure ****

    widget_control, first_child, set_uvalue=new_state,/no_copy

    RETURN, main

END
