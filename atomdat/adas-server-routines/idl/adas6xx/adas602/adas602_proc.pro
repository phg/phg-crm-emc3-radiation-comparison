; Copyright (c) 1995 Strathclyde University .
; SCCS info: Module @(#)$Header: /home/adascvs/idl/adas6xx/adas602/adas602_proc.pro,v 1.1 2004/07/06 11:02:06 whitefor Exp $ Date $Date: 2004/07/06 11:02:06 $
;+
; PROJECT:
;       ADAS IBM MVS to DEC UNIX conversion
;
; NAME:
;	ADAS602_PROC
;
; PURPOSE:
;	IDL ADAS user interface, processing options/input.
;
; EXPLANATION:
;	This routine creates and manages a pop-up window which allows
;	the user to select options and input data to control ADAS602
;	processing.
;
; USE:
;	This routine is ADAS602 specific, see f2ispf.pro for how it
;	is used.
;
; INPUTS:
;	VAL	- A structure which determines the initial settings of
;		  the processing options widget.  The value is passed
;		  unmodified into cw_adas602_proc.pro.
;
;		  See cw_adas602_proc.pro for a full description of this
;		  structure.
;
;	INVAL	- Structure; the input screen settings. See cw_adas602_in.pro
;
;	OLDSTR  - A structure derived from a processed template also
;		  containing previously used fitting parameters.
;
;	PROC_LSTR- Structure containing processed data.
;
;	HEADER1 - Header of the input data.
;
;	HEADER2 - Header of the processed template.
;
;	SPECTRUM- Array containing input data.
;
;       BITFILE - String; the path to the dirctory containing bitmaps
;                 for the 'escape to series menu' button.
;
; OPTIONAL INPUTS:
;	None
;
; OUTPUTS:
;	VAL	- On output the structure records the final settings of
;		  the processing selections widget if the user pressed the
;		  'Done' button, otherwise it is not changed from input.
;
;	ACT	- String; Either 'Done' or 'Cancel' for the button the
;		  user pressed to terminate the processing options
;		  window.
;
; OPTIONAL OUTPUTS:
;	None
;
; KEYWORD PARAMETERS:
;	FONT_LARGE - The name of a larger font.
;
;	FONT_SMALL - The name of a smaller font.
;
;	EDIT_FONTS - A structure of two fonts used in the table editor
;		     adas_edtab.pro, {font_norm:'',font_input:''}.
;
; CALLS:
;	CW_ADAS602_PROC	Declares the processing options widget.
;	DYNLABEL	Sets dynamic_resize keyword for label widgets.
;	ADAS602_PROC_EV Called indirectly during widget management,
;			routine included in this file.
;	XMAMAGER
;
; SIDE EFFECTS:
;	This routine uses a common block PROC602_BLK in the management
;	of the pop-up window.
;
; CATEGORY:
;	Widgets
;
; WRITTEN:
;       David Brooks, Strathclyde University, June 1997
;
; MODIFIED:
;       1.0 	David Brooks
;
;	1.1	Richard Martin
;		Changed documentation + put under SCCS control
;
; VERSION:
;       1.0     ?
;
;	1.1	12-06-97
;-
;-----------------------------------------------------------------------------


PRO adas602_proc_ev, event

  COMMON proc602_blk,action,value

		;**** Find the event type and copy to common ****
    action = event.action

    CASE action OF

		;**** 'Done' button ****
	'Done'  : begin

			;**** Get the output widget value ****
		widget_control,event.id,get_value=value 

		widget_control,event.top,/destroy

	   end


		;**** 'Cancel' button ****
	'Cancel': widget_control,event.top,/destroy

		;**** 'Menu' button ****
	'Menu': widget_control,event.top,/destroy

    END

END

;-----------------------------------------------------------------------------


PRO adas602_proc, val, act, bitfile, header1, header2,        $
                spectrum, proc_lstr, inval, oldstr,           $
                FONT_LARGE=font_large,                        $
		FONT_SMALL=font_small, EDIT_FONTS=edit_fonts

  COMMON proc602_blk,action,value

		;**** Copy value to common ****
  value = val

		;**** Set defaults for keywords ****
  IF NOT (KEYWORD_SET(font_large)) THEN font_large = ''
  IF NOT (KEYWORD_SET(font_small)) THEN font_small = ''
  IF NOT (KEYWORD_SET(edit_fonts)) THEN $
				edit_fonts = { font_norm:'', font_input:''}

                ;**** create base widget ****
  procid = widget_base(TITLE='ADAS602 PROCESSING OPTIONS', $
					XOFFSET=50,YOFFSET=0)

		;**** Declare processing widget ****
  cwid = cw_adas602_proc(procid, bitfile, header1, header2, spectrum,  $
                         proc_lstr, inval, oldstr, VALUE=value,        $
			 FONT_LARGE=font_large, FONT_SMALL=font_small, $
			 EDIT_FONTS=edit_fonts)

		;**** Realize the new widget ****
  dynlabel, procid
  widget_control,procid,/realize

		;**** make widget modal ****
  xmanager,'adas602_proc',procid,event_handler='adas602_proc_ev', $
					/modal,/just_reg
 
		;**** Return the output value from common ****
  act = action
  val = value

END

