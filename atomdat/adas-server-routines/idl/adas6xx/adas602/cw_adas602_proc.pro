; Copyright (c) 1997 Strathclyde University
; SCCS info: Module @(#)$Header: /home/adascvs/idl/adas6xx/adas602/cw_adas602_proc.pro,v 1.6 2004/07/06 12:54:34 whitefor Exp $ Date $Date: 2004/07/06 12:54:34 $
;+
; PROJECT:
;	ADAS UNIX.
;
; NAME :
;	CW_ADAS602_PROC
;
; PURPOSE:
;	Produces a widget for ADAS602 processing options/input.
;
; EXPLANATION:
;
; USE:  This widget is specific to ADAS602, see adas602_proc.pro
;	for use.
;
; INPUTS:
;       PARENT - The ID of the parent widget.
;
;	INVAL	- Structure; the input screen settings. See cw_adas602_in.pro		
;
;	OLDSTR  - A structure derived from a processed template also
;		  containing previously used fitting parameters.
;
;	PROC_LSTR- Structure containing processed data.
;
;	HEADER1 - Header of the input data.
;
;	HEADER2 - Header of the processed template.
;
;	SPECTRUM- Array containing input data.
;
;       BITFILE - String; the path to the dirctory containing bitmaps
;                 for the 'escape to series menu' button.
;
; KEYWORD PARAMETERS:
;
;	VALUE	- A structure which determines the initial settings of
;		  the processing options widget. 
;
;		{ ps602_set, NEW:0, NLINES:0, 			$
;	     		EXPFLAG:0, EXPVAL:0.0, 			$
;	     		WAITF:0, WAITVAL:0.0, 			$
;	     		GRPSCAL:0, GRPRMESS:'', 		$
;	     		XMIN:0.0, XMAX:0.0, 			$
;	     		YMIN:0.0, YMAX:0.0, 			$
;	     		GTIT:'', GTITFLAG:0, 			$
;	     		FIXFLAG:0, LIDFLAG:0, 			$	
;	     		FIXPOS:dblarr(10), LINEID:strarr(10), 	$
;	     		VWFLAG:1, FWFLAG:1, FIXWID:0.0, 	$
;	     		BFLAG:0, BFLAT:0, BSLOPE:2, BCURVE:2, 	$
;	     		WTFLAG:1, AUTOLOOP:0, TITLE:'', FITFLAG:0}
;	
;		EXPFLAG	- Tells whether exposure time button is pressed
;				(1-on,0-off)
;		EXPVAL	- Value of exposure time if entered
;		WAITF	- Tells whether wait time button is pressed
;				(1-on,0-off)
;		WAITVAL	- Value of wait time if entered
;		GRPSCAL	- Tells whether explicit scaling of graph is on
;				(1-on,0-off)
;		GRPRMESS- Graph scaling error message e.g. if a symbol entered
;		XMIN	- Minimum of graphs' scaling if entered
;		XMAX	- Maximum of graphs' scaling if entered
;		GTIT	- Title of graph if entered
;		GTITFLAG- Tells whether graph title entered (1-yes,0-no)
;		FIXFLAG	- Tells whether fixed positions have been chosen
;				(1-yes,0-no)
;		LIDFLAG	- Tells whether line identifications have been chosen
;				(1-yes,0-no)
;		FIXPOS	- Values of fixed positions of lines
;		LINEID	- Identifications of lines (preliminary usually)
;		VWFLAG	- Tells whether variable widths are allowed
;				(1-yes,2-no,0-don't set keyword in mlcds)
;		FWFLAG	- Tells whether to fit variable widths to lines (")
;		FIXWID	- Value of fixed width if chosen
;		BFLAG	- Tells whether background is to be fitted (1-y,0-n)
;		BFLAT	- Tells whether to fit constant component to background
;				(1-y,0-n)
;		BSLOPE	- Tells whether to fit linear component to background
;				(1-y,0-n)
;		BCURVE	- Tells whether to fit quadratic component to
;				background(1-y,0-n)
;		WTFLAG	- Tells whether to make a weighted fit
;				(1-yes,2-no,0-don't set keyword in mlcds)
;		AUTOLOOP- Do automatic cycling (1) , don't(0)
;		TITLE	- Do automatic cycling (1) , don't(0)
;		FITFLAG	- make the fit(1), don't (0)
;
;
;	FONT_LARGE - The name of a larger font.
;
;	FONT_SMALL - The name of a smaller font.
;
;	EDIT_FONTS - A structure of two fonts used in the table editor
;		     adas_edtab.pro, {font_norm:'',font_input:''}.
;
; OUTPUTS:
;       The ID of the created widget is returned.
;
; COMMON BLOCKS:
;	None.
;
; CALLS:
;
;	PLOT		Graph plot.
;	CW_ADAS_GRAPH	Widget for displaying a series of ADAS plots.
;	CW_BNDL602_GEN  Bundling selection list widget.
;
; SIDE EFFECTS:
;
;	The following widget management routines are included in this
;	file
;
;	CW_PROC602_PROC_GET_VAL()	Returns the current VALUE structure.
;	CW_PROC602_PROC_EVENT()		Process and issue events.
;
; CATEGORY:
;	Compound Widget
;
; MODIFIED:
; 	1.0	David Brooks
;	1.1	Richard Martin
;		Added documentation + put under SCCS control
;	1.2	David H.Brooks
;		Modified widget creation order to increase speed of
;               panel realisation.
;	1.3	David H.Brooks
;		Corrected error in handling of processed data.
;	1.4	David H.Brooks
;		Modified to add wait time entry box/button. 
;		Temporary delete 'all_events' in xmin/max ymin/max ranges 
;		to reduce effect of growing widget bug in IDL5.0.x     
;	1.5	Richard Martin
;		Stored original plot window to pixmap window, so that 
;		original plot can be shown when exiting from fitting
;		procedure in mlcds.pro. Consequently added, state.pwin
;		in call to mlcds.pro .  
;		Changed widget base name switch to switchb for IDL 5.4 compatibility.           
; 
;	1.6	Richard Martin
;		Added support for NIS profiles.
;		Added residual plot window, and moved graph controls.
; DATE:
;	1.0 	?
;	1.1	01-07-97
;	1.2	19-08-97
;	1.3	18-09-97
;	1.4	14-04-98
;	1.5	18-09-00
;	1.6   20-07-01
;-
;----------------------------------------------------------------------------

FUNCTION cw_adas602_proc_get_value, id

	; Return to caller.
  ON_ERROR, 2

	; Retrieve the structure from the child that contains the sub ids.
  stash = WIDGET_INFO(id, /CHILD)
  WIDGET_CONTROL, stash, GET_UVALUE=state, /NO_COPY


        ; get exposure info.
  widget_control, state.expid2, get_uvalue=uexp
  if uexp eq 1 then begin
    widget_control, state.expid2, get_value=temp
    expval=temp(0)
    expflag=1
  end else begin
    expval = 0.0
    expflag=0
  end

        ; get graph title info.
  widget_control, state.gtitid2, get_uvalue=ugtit
  if ugtit eq 1 then begin
    widget_control, state.gtitid2, get_value=temp
    gtit=temp(0)
    gtitflag=1
  end else begin
    gtit = ' '
    gtitflag=0
  end

	; Get ranges info.  
  widget_control, state.xminid, get_value=temp
  xmin=float(strtrim(temp(0),2))
  widget_control, state.xmaxid, get_value=temp
  xmax=float(strtrim(temp(0),2))
  widget_control, state.yminid, get_value=temp
  ymin=float(strtrim(temp(0),2))
  widget_control, state.ymaxid, get_value=temp
  ymax=float(strtrim(temp(0),2))

	; Get ranges button and mesage info.  
  widget_control, state.exscid, get_uvalue=temp
  scalbut=fix(temp)
  widget_control, state.messid, get_value=temp
  grprmess=temp(0)

	; Get the no. of lines to be fitted
  widget_control, state.nlineid, get_value=temp
  nlines=temp(0)

	; Get info. about fixed positions / line IDs
  widget_control, state.fixid, get_uvalue=temp
  if temp eq 1 then begin
    fixflag = 1
  end else begin
    fixflag = 0
  end
  widget_control, state.lidid, get_uvalue=temp
  if temp eq 1 then begin
    lidflag = 1
  end else begin
    lidflag = 0
  end
  widget_control, state.fixrow, get_value=temp
  fixpos=float(temp.bndls)
  widget_control, state.linerow, get_value=temp
  lineid=temp.bndls

	; Get width options 
  widget_control, state.vwidyes, get_uvalue=temp
  if temp(0) eq 1 then begin
    vwflag = 1
    widget_control, state.fwidyes, get_uvalue=temp
    if temp(0) eq 1 then begin
      fwflag = 1
    end else begin
      fwflag = 2
    end
    fixwid=0.0
  end else begin
    vwflag = 2
    fwflag = 2
    widget_control, state.inpwid, get_value=temp
    fixwid=float(temp(0))
  end
  
	; Get background options
  widget_control, state.boptid, get_uvalue=temp
  if temp(0) eq 1 then begin
    bflag = 1
    bflat = 1
    bslope = 2
    bcurve = 2
    widget_control, state.blinid, get_uvalue=temp
    if temp(0) eq 1 then bslope = 1
    widget_control, state.bquadid, get_uvalue=temp
    if temp(0) eq 1 then bcurve = 1
  end else begin
    bflag = 0
    bflat = 0
    bslope = 2
    bcurve = 2
  end

	; Get weighted option
  widget_control, state.wtid, get_uvalue=temp
  if temp(0) eq 1 then begin
    wtflag=1
  end else begin
    wtflag=2
  end
	; Get cycling option 
  widget_control, state.aid, get_uvalue=temp
  if temp(0) eq 1 then begin
    autoloop=1
  end else begin
    autoloop=0
  end
        ; get wait time info.
  widget_control, state.awtid, get_uvalue=uwt
  if uwt eq 1 then begin
    widget_control, state.awtid2, get_value=temp
    waitval=temp(0)
    waitf=1
  end else begin
    waitval = 0.0
    waitf=0
  end
	; Get the value here
  ps = { NEW:0, NLINES:nlines, $
         EXPFLAG:expflag, EXPVAL:float(expval), $
         WAITF:waitf, waitval:float(waitval), $
         GRPSCAL:scalbut, GRPRMESS:grprmess ,$
         XMIN:xmin, XMAX:xmax, $
         YMIN:ymin, YMAX:ymax, $
         GTIT:gtit, GTITFLAG:gtitflag, $
         FIXFLAG:fixflag, LIDFLAG:lidflag, $
         FIXPOS:fixpos, LINEID:lineid, $
         VWFLAG:vwflag, FWFLAG:fwflag, $
         FIXWID:fixwid, BFLAG:bflag, $
         BFLAT:bflat, BSLOPE:bslope, $
         BCURVE:bcurve, WTFLAG:wtflag, $
         AUTOLOOP:autoloop, TITLE:state.ps.title, $
         FITFLAG:state.ps.fitflag, PROFILES:state.ps.profiles, $
	   PINDEX: state.ps.pindex } 

	; Restore the state.
  WIDGET_CONTROL, stash, SET_UVALUE=state, /NO_COPY

        ; Return the value here.

  RETURN, ps

END

;-----------------------------------------------------------------------------

FUNCTION cw_adas602_proc_event, event

  parent=event.handler


	; Retrieve the structure from the child that contains the sub ids.
  stash = WIDGET_INFO(parent, /CHILD)
  WIDGET_CONTROL, stash, GET_UVALUE=state, /NO_COPY


                ;**** Clear existing error message ****
  widget_control,state.messid,set_value=' '


                ;**** process events ****

  CASE event.id OF

                ;**** Cancel button ****

    state.cancelid: new_event = {ID:parent, TOP:event.top, $
                                 HANDLER:0L, ACTION:'Cancel'}

                ;**** Done button ****

    state.doneid: begin
                ;**** check for error message ****
        error =0
        if state.ps.grpscal eq 1 then error =1
        if state.ps.expflag eq 1 then error =1
        if state.ps.fitflag eq 0 then begin   
            action=popup(message='FIT FAILED OR WAS NOT PERFORMED', $
                         buttons=[' OK '], font=state.font)
            new_event = 0L
        end else begin
          if error eq 1 and strtrim(state.ps.grprmess) ne '' then begin
            widget_control, state.messid, set_value= $
                                 '**** Error in Settings ****'
                    new_event = 0L
          end else begin      
                    new_event = {ID:parent, TOP:event.top, $
                                 HANDLER:0L, ACTION:'Done'}
          end
        end
    end

                ;**** Menu button ****

    state.outid: new_event = {ID:parent, TOP:event.top, $
                              HANDLER:0L, ACTION:'Menu'}
 
                ;**** Fit button ****
    state.fitid: begin                                        

                ;**** Get all flags and appropriate user choices ****
                ;**** and then call spectral line fitting code   ****

                ;********************************************
                ;*** Have to restore "state" before calls ***
                ;*** to cw_adas602_proc_get_value so it   ***
                ;*** can be used there.                   ***
                ;********************************************

      widget_control, stash, set_uvalue=state, /no_copy

      widget_control, event.handler, get_value=ps	 

                ;**** reset state variable ****
      widget_control, stash, get_uvalue=state, /no_copy
      xchk = ps.xmax-ps.xmin
      chksize = size(state.spectrum)

                ; **** check whether template is to be used ****
      chkoldstr = size(state.oldstr)
      fcancel=0
      if chkoldstr(0) eq 0 and chkoldstr(chkoldstr(0)+1) ne 8 then begin

                ; **** check at least one line is to be fitted ****
       if state.ps.nlines eq 0 then begin
          action=popup(message='MUST FIT AT LEAST ONE LINE',   $
                       buttons=[' OK '], font=state.font)
          fcancel=1
       end else begin
        if xchk gt 2047 or ps.xmax gt 2047 or $
           ps.xmax ge chksize(1)+ps.xmin then begin
                ; **** popup warning as max. no. of grating positions  ****
                ; **** is only 2048 (adequate for CDS/SUMER? pixels)   ****
         action=popup(message='X-RANGES LARGER THAN DATA SIZE: DECREASE XMAX',$
                      buttons=[' OK '], font=state.font)
         fcancel =1
        end else begin

                ; **** check x-ranges are set ok. ****
          if ps.xmin eq 0.0 then state.ps.xmin=0.01
          if ps.xmax eq 0.0 then begin
             if !x.crange(1) gt 2047 then begin
               state.ps.xmax = 2047         
                ; **** popup warning that spectrum truncated to 2048   ****
               action=popup(message='SPECTRUM TRUNCATED TO 2048 PIXELS', $
                            buttons=[' OK '], font=state.font)
             end else begin
               state.ps.xmax = !x.crange(1)
             end
          end
          
                ; **** check y-ranges are set ok. ****
          if ps.ymin eq 0.0 then state.ps.ymin=0.01
          if ps.ymax eq 0.0 then state.ps.ymax=!y.crange(1)
          if state.ps.ymax lt max(state.spectrum) then begin
                ; **** popup warning that spectrum truncated to 2048   ****
            action=popup(message='YMAX LOWER THAN DATA MAXIMUM', $
                         buttons=[' Continue ',' Cancel '],      $
                         font=state.font)
            if action eq ' Cancel ' then begin
              fcancel=1
            end
          end
                ; **** check fixed width is non-zero
          if state.ps.vwflag eq 2 and state.ps.fixwid eq 0.0 then begin
               action=popup(message='CANNOT HAVE 0.0 FIXED WIDTH', $
                            buttons=[' OK '], font=state.font)
               fcancel=1
          end
                ; **** set fixed position vector ****          
          if state.ps.fixflag eq 0 then begin
            fixpos = make_array(10, /double, value=0.0)
          end else begin
            fixpos = ps.fixpos
          end
                ; **** if expflag set and no value entered then unset ****
          if state.ps.expflag eq 1 and state.ps.expval eq 0.0 then begin
            state.ps.expflag=0
            widget_control, state.expid1, set_button=0
            widget_control, state.expid2, sensitive=0
            widget_control, state.expid2, set_uvalue=0
          end
                ; **** check no. of fixed lines < no. of lines ****
          if n_elements(where(fixpos ne 0.0)) gt state.ps.nlines $
            and state.ps.fixflag eq 1 then begin
               action=popup(message='CANNOT HAVE MORE FIXED POSITIONS THAN NO. OF LINES', $
                            buttons=[' OK '], font=state.font)
               fcancel=1
          end
                ; **** set line ID vector ****          
          if state.ps.lidflag eq 0 then begin
            idlines = make_array(10, /string, value=' ')
          end else begin
            idlines = ps.lineid
          end
                ; **** check no. of IDs < no. of lines ****
          if n_elements(where(strtrim(idlines,2) ne '')) gt state.ps.nlines $
            and state.ps.lidflag eq 1 then begin
               action=popup(message='CANNOT HAVE MORE IDENTIFIED LINES THAN NO. OF LINES', $
                            buttons=[' OK '], font=state.font)
               fcancel=1
          end
                ; **** get graph user title ****
          if ps.gtitflag eq 1 then begin
            utitle = ps.gtit
          end else utitle=''
        end
       end
      end


                ; **** check autoloop compatable with array size ****
          chkspect=size(state.spectrum)
          if chkspect(0) eq 1 and ps.autoloop eq 1 then begin
            action=popup(message='NEED MORE THAN 1 DIMENSION FOR AUTO '+$
                         '- CYCLING', buttons=[' OK '], font=state.font)
            fcancel = 1 
          end

          if fcancel ne 1 then begin
           state.ps.fitflag = 0
           noback = 1-ps.bflag
           if ps.autoloop eq 1 then begin
               mlcds, return_str, state.graphid, state.residid, fortdir = state.inval.fortdir, 	     $
               	      expval = ps.expval, xmin = xmin, xmax = xmax, 	     $
                      ymin = state.ps.ymin,ymax = state.ps.ymax, /autoloop,  $
                      loopdata = state.spectrum, header = state.header,      $
                      noback = noback, /noprompt, oldstr = state.oldstr,     $
                      position = state.position, charsize = state.charsize,  $
                      weightfit = state.ps.wtflag, nlines = state.ps.nlines, $
                      vwidth = state.ps.vwflag, fvwidth = state.ps.fwflag,   $
                      fxwidth = state.ps.fixwid, fxpos = fixpos,             $
                      idlines = idlines, /nooutput, blin = state.ps.bslope,  $
                      bquad = state.ps.bcurve, adastitle = state.ps.title,   $
                      utitle = utitle, proc = state.inval.dflag, wait_time = $
                      state.ps.waitval, paramval = state.paramval, 	     $
		      maxparams = state.maxparams, btype = state.ps.pindex
           end else begin
               mlcds, return_str, state.graphid, state.residid, fortdir = state.inval.fortdir, 	     $
                      data = state.spectrum,      			     $
               	      expval = ps.expval, xmin =   xmin, xmax =   xmax,      $
                      ymin = state.ps.ymin, ymax = state.ps.ymax,            $
                      header = state.header, noback = noback, /noprompt,     $
                      oldstr = state.oldstr, position = state.position,      $
                      charsize = state.charsize, weightfit = state.ps.wtflag,$
                      nlines = state.ps.nlines, vwidth = state.ps.vwflag,    $
                      fvwidth = state.ps.fwflag, fxwidth = state.ps.fixwid,  $
                      fxpos = fixpos, idlines = idlines, /nooutput,          $
                      blin = state.ps.bslope, bquad = state.ps.bcurve,       $
                      adastitle = state.ps.title, utitle = utitle,           $
                      proc = state.inval.dflag, paramval = state.paramval,   $
		      maxparams = state.maxparams, btype = state.ps.pindex
           end
          lstr=return_str
          if lstr(0).info ne 0 and lstr(0).info ne 4 and lstr(0).info ne 6 $
            and lstr(0).info ne 7 then begin
            state.ps.fitflag = 1
          end
          save,filename='temporary',lstr
          end
    end 

                ;**** Exposure time button ****
    state.expid1: begin
        widget_control, state.expid2, get_uvalue=uexp
        if uexp eq 0 then begin 
          state.ps.expflag=1
          widget_control, state.expid2, /sensitive
          widget_control, state.expid2, /input_focus
          widget_control, state.expid2, set_uvalue=1
        end else if uexp eq 1 then begin
          state.ps.expflag=0
          widget_control, state.expid2, sensitive=0
          widget_control, state.expid2, set_uvalue=0
          widget_control, state.expid2, set_value=''
          state.ps.expval=0.0
        end
    end
                ;**** exposure time selected ****
    state.expid2: begin
      widget_control, state.expid2, get_value=temp
                ;**** check input legal - only +ve exposure times ****
      if strtrim(temp(0)) ne '' then begin
        error = num_chk(temp(0),/sign)
        if error eq 1 then begin
          state.ps.grprmess = '**** Illegal Numeric Value ****'
          widget_control,state.messid,set_value=state.ps.grprmess
          widget_control,state.expid2,/input_focus
        end else begin
          state.ps.expval = float(temp(0))
        end
      end else begin
        state.ps.expval = 0.0
      end
 
    end

                ;**** graph title store for later ****
    state.gtitid1: begin
        widget_control, state.gtitid2, get_uvalue=ugtit
        if ugtit eq 0 then begin 
          widget_control, state.gtitid2, /sensitive
          widget_control, state.gtitid2, /input_focus
          widget_control, state.gtitid2, set_uvalue=1
        end else if ugtit eq 1 then begin
          widget_control, state.gtitid2, sensitive=0
          widget_control, state.gtitid2, set_uvalue=0
          widget_control, state.gtitid2, set_value='1.0'
        end
    end

    state.exscid: begin
        if state.ps.grpscal eq 0 then begin
          state.ps.grpscal = 1
          widget_control,state.exscid,set_button=1
          widget_control,state.exscid,set_uvalue=1
          widget_control,state.rngbasid,/sensitive
          widget_control,state.xminid,set_value=strtrim(state.ps.xmin,2)
          widget_control,state.xmaxid,set_value=strtrim(state.ps.xmax,2)
          widget_control,state.yminid,set_value=strtrim(state.ps.ymin,2)
          widget_control,state.ymaxid,set_value=strtrim(state.ps.ymax,2)
                
                ; **** replot graph ****
          plot, state.pixinit, state.spectrum,                     $
                xtitle='pixel no.',                                $
                ytitle='counts', xr=[state.ps.xmin,state.ps.xmax], $
                /xstyle, yr=[state.ps.ymin,state.ps.ymax], /ystyle

	    widget_control, state.residid, get_value=resval
          wset,resval.win
	    plot, state.pixinit, state.spectrum, xtitle='pixel no.', ytitle='counts',     $
	    xr=[state.ps.xmin,state.ps.xmax], /xstyle, yr=[state.ps.ymin,state.ps.ymax], /ystyle,/nodata	    
	    widget_control, state.graphid, get_value=grval
	    wset,grval.win
		    
        end else begin
          state.ps.grpscal = 0
          widget_control,state.exscid,set_button=0
          widget_control,state.exscid,set_uvalue=0
          widget_control,state.rngbasid,sensitive=0
          widget_control,state.xminid,get_value=xmin
          widget_control,state.xmaxid,get_value=xmax
          widget_control,state.yminid,get_value=ymin
          widget_control,state.ymaxid,get_value=ymax
          state.ps.xmin = float(xmin(0))
          state.ps.xmax = float(xmax(0))
          state.ps.ymin = float(ymin(0))
          state.ps.ymax = float(ymax(0))
          widget_control,state.xminid,set_value='        '
          widget_control,state.xmaxid,set_value='        '        
          widget_control,state.yminid,set_value='        '        
          widget_control,state.ymaxid,set_value='        '

                ; **** revert to original graph but retain ****
                ; **** ranges in memory                    ****
          plot, state.pixinit, state.spectrum,                 $
                xtitle='pixel no.', ytitle='counts'

	    widget_control, state.residid, get_value=resval
          wset,resval.win
	    plot, state.pixinit, state.spectrum, xtitle='pixel no.', ytitle='counts',/nodata	    
	    widget_control, state.graphid, get_value=grval
	    wset,grval.win
		    
        end
    end

    state.xminid: begin

                ; **** get range limit and check input is legal ****
                ; **** only +ve x-ranges allowed ****
      widget_control, state.xminid, get_value=temp
        if strtrim(temp(0)) ne '' then begin
          error = num_chk(temp(0),/sign)
          if error eq 1 then begin
            state.ps.grprmess = '**** Illegal Numeric Value ****'
            widget_control, state.messid, set_value=state.ps.grprmess
            widget_control, state.xminid, /input_focus
          end else begin
            state.ps.grprmess=''
            state.ps.xmin = float(temp(0))

                ; **** replot graph ****
            plot, state.pixinit, state.spectrum,                     $
                  xtitle='pixel no.',                                $
                  ytitle='counts', xr=[state.ps.xmin,state.ps.xmax], $
                  /xstyle, yr=[state.ps.ymin,state.ps.ymax], /ystyle
			
		widget_control, state.residid, get_value=resval
      	wset,resval.win
		plot, state.pixinit, state.spectrum, xtitle='pixel no.', ytitle='counts',	$
		xr=[state.ps.xmin,state.ps.xmax], /xstyle, yr=[state.ps.ymin,state.ps.ymax], /ystyle,/nodata		
		widget_control, state.graphid, get_value=grval
		wset,grval.win			
			
          end
        end
    end

    state.xmaxid: begin

                ; **** get range limit and check input is legal ****
                ; **** only +ve x-ranges allowed ****
      widget_control, state.xmaxid, get_value=temp
        if strtrim(temp(0)) ne '' then begin
          error = num_chk(temp(0),/sign)
          if error eq 1 then begin
            state.ps.grprmess = '**** Illegal Numeric Value ****'
            widget_control, state.messid, set_value=state.ps.grprmess
            widget_control, state.xmaxid, /input_focus
          end else begin
            state.ps.grprmess=''
            state.ps.xmax = float(temp(0))
                ; **** replot graph ****
            plot, state.pixinit, state.spectrum,                     $
                  xtitle='pixel no.',                                $
                  ytitle='counts', xr=[state.ps.xmin,state.ps.xmax], $
                  /xstyle, yr=[state.ps.ymin,state.ps.ymax], /ystyle

		widget_control, state.residid, get_value=resval
      	wset,resval.win
		plot, state.pixinit, state.spectrum, xtitle='pixel no.', ytitle='counts',	$
		xr=[state.ps.xmin,state.ps.xmax], /xstyle, yr=[state.ps.ymin,state.ps.ymax], /ystyle,/nodata		
		widget_control, state.graphid, get_value=grval
		wset,grval.win			
			
          end
        end
    end

    state.yminid: begin

                ; **** get range limit and check input is legal ****
                ; **** +/-ve y-ranges allowed ****
      widget_control, state.yminid, get_value=temp
        if strtrim(temp(0)) ne '' then begin
          error = num_chk(temp(0),sign=0)
          if error eq 1 then begin
            state.ps.grprmess = '**** Illegal Numeric Value ****'
            widget_control, state.messid, set_value=state.ps.grprmess
            widget_control, state.yminid, /input_focus
          end else begin
            state.ps.grprmess=''
            state.ps.ymin = float(temp(0))
                ; **** replot graph ****
            plot, state.pixinit, state.spectrum,                     $
                  xtitle='pixel no.',                                $
                  ytitle='counts', xr=[state.ps.xmin,state.ps.xmax], $
                  /xstyle, yr=[state.ps.ymin,state.ps.ymax], /ystyle			
          end
        end
    end

    state.ymaxid: begin

                ; **** get range limit and check input is legal ****
                ; **** +/-ve y-ranges allowed ****
      widget_control, state.ymaxid, get_value=temp
        if strtrim(temp(0)) ne '' then begin
          error = num_chk(temp(0),sign=0)
          if error eq 1 then begin
            state.ps.grprmess = '**** Illegal Numeric Value ****'
            widget_control, state.messid, set_value=state.ps.grprmess
            widget_control, state.ymaxid, /input_focus
          end else begin
            state.ps.grprmess=''
            state.ps.ymax = float(temp(0))
                ; **** replot graph ****
            plot, state.pixinit, state.spectrum,                     $
                  xtitle='pixel no.',                                $
                  ytitle='counts', xr=[state.ps.xmin,state.ps.xmax], $
                  /xstyle, yr=[state.ps.ymin,state.ps.ymax], /ystyle	
          end
        end
    end

    state.pbut: begin
    	widget_control, state.pbut, get_value=temp
    	if (temp eq 0) then begin
    		widget_control,state.right1,map=1
    		widget_control,state.right2,map=0
    	endif else begin
    		widget_control,state.right1,map=0
    		widget_control,state.right2,map=1    		
	endelse
    end

    state.nlineid: begin
      widget_control, state.nlineid, get_value=temp
      if temp(0) gt 10 then begin
                ; **** popup warning as max. of 10 lines can be fitted ****
        action=popup(message='A maximum of 10 lines can be fitted ', $
                     buttons=[' OK '], font=state.font)
      end else begin
        state.ps.nlines = temp(0)
      end
    end

    state.fixid: begin
      widget_control, state.fixid, get_uvalue=temp
      if temp eq 1 then begin
         widget_control, state.fixid, set_uvalue=0
         widget_control, state.fixid, set_button=0
         widget_control, state.swcol1, sensitive=0
         widget_control, state.swcol1, map = 1  
         widget_control, state.swcol2, map = 0  
         state.ps.fixflag=0
      end else if temp eq 0 then begin
         widget_control, state.fixid, set_uvalue=1
         widget_control, state.fixid, set_button=1
         widget_control, state.swcol2, map = 0  
         widget_control, state.swcol1, map = 1  
         widget_control, state.swcol1, /sensitive
         widget_control, state.fixrow, get_value=temp
;         widget_control, temp.groups(0), /input_focus
         state.ps.fixflag=1
      end
    end

    state.lidid: begin
      widget_control, state.lidid, get_uvalue=temp
      if temp eq 1 then begin
         widget_control, state.lidid, set_uvalue=0
         widget_control, state.lidid, set_button=0
         widget_control, state.swcol2, sensitive=0
         widget_control, state.swcol2, map = 1  
         widget_control, state.swcol1, map = 0  
         state.ps.lidflag=0
      end else if temp eq 0 then begin
         widget_control, state.lidid, set_uvalue=1
         widget_control, state.lidid, set_button=1
         widget_control, state.swcol2, /sensitive
         widget_control, state.swcol2, map = 1  
         widget_control, state.swcol1, map = 0  
         widget_control, state.linerow, get_value=temp
;         widget_control, temp.groups(0), /input_focus
         state.ps.lidflag=1
      end
    end

    state.wparbut: begin
	widget_control, state.wmenusel, get_value=index
	paramval=fltarr(state.maxparams)
	profiles=state.ps.profiles
    	wparbut = cw_adas602_param(profiles,index,state.maxparams,paramval,font=state.font)
	state.ps.profiles=profiles
	state.paramval=paramval
    end
    
    state.wmenusel: begin
      widget_control, state.wmenusel, get_value=temp
	state.ps.pindex=temp(0)
	if state.ps.pindex eq 0 then begin
		widget_control, state.wpbase, sensitive=0
	endif else begin
		widget_control, state.wpbase, sensitive=1
	endelse
  	for i=0,state.ps.profiles[state.ps.pindex].numberparams-1 do begin
 	    state.paramval[i]=state.ps.profiles[state.ps.pindex].params[i].value
  	endfor
    end

    state.vwidyes: begin
      state.ps.vwflag = 1           
      widget_control, state.rswitch1, map=1
      widget_control, state.rswitch1, /sensitive 
      widget_control, state.vwidyes, set_uvalue = 1
      widget_control, state.rswitch2, map=0
      widget_control, state.rswitch2, sensitive = 0
      widget_control, state.vwidno, set_uvalue = 0
      widget_control, state.inpwid, set_value=''
      state.ps.fixwid = 0.0
    end

    state.vwidno: begin
      state.ps.vwflag = 2
      widget_control, state.rswitch1, map=0
      widget_control, state.rswitch1, sensitive = 0
      widget_control, state.vwidyes, set_uvalue = 0
      widget_control, state.rswitch2, map=1
      widget_control, state.rswitch2, /sensitive
      widget_control, state.vwidno, set_uvalue = 1
      widget_control, state.inpwid, /input_focus
    end

    state.fwidyes: begin
      state.ps.fwflag = 1           
      widget_control, state.fwidyes, set_uvalue = 1
      widget_control, state.fwidno, set_uvalue = 0
    end

    state.fwidno: begin
      state.ps.fwflag = 2
      widget_control, state.fwidyes, set_uvalue = 0
      widget_control, state.fwidno, set_uvalue = 1
    end

    state.inpwid: begin
      widget_control, state.inpwid, get_value=temp
                ;**** check input legal - only +ve widths ****
      if strtrim(temp(0)) ne '' then begin
        error = num_chk(temp(0),/sign)
        if error eq 1 then begin
          state.ps.grprmess = '**** Illegal Numeric Value ****'
          widget_control,state.messid,set_value=state.ps.grprmess
          widget_control,state.inpwid,/input_focus
        end else begin
          state.ps.fixwid = float(temp(0))
        end
      end
    end

    state.boptid: begin
      widget_control, state.boptid, get_uvalue=temp
      if temp(0) eq 1 then begin
        state.ps.bflag = 0
        state.ps.bflat = 0
        widget_control, state.boptid, set_uvalue = 0
        widget_control, state.brow, sensitive = 0
        widget_control, state.bconid, set_button = 0
        widget_control, state.bconid, set_uvalue = 0
      end else begin
        state.ps.bflag = 1
        state.ps.bflat = 1
        widget_control, state.boptid, set_uvalue = 1
        widget_control, state.brow, /sensitive
        widget_control, state.bconid, /set_button
        widget_control, state.bconid, set_uvalue = 1
      end
    end

    state.bconid: begin
       widget_control, state.bconid, get_uvalue=temp
       if temp(0) eq 1 then begin
                ; **** popup warning - cannot switch off flat background****
         action=popup(message='BACKGROUND MUST HAVE CONSTANT COMPONENT', $
                      buttons=[' OK '], font=state.font)
         widget_control, state.bconid, set_button=1
       end 
    end

    state.blinid: begin
       widget_control, state.blinid, get_uvalue=temp
       if temp(0) eq 1 then begin
         state.ps.bslope = 2
         widget_control, state.blinid, set_uvalue=0
       end else begin
         state.ps.bslope = 1
         widget_control, state.blinid, set_uvalue=1
       end
    end

    state.bquadid: begin
       widget_control, state.bquadid, get_uvalue=temp
       if temp(0) eq 1 then begin
         state.ps.bcurve = 2
         widget_control, state.bquadid, set_uvalue=0
       end else begin
         state.ps.bcurve = 1
         widget_control, state.bquadid, set_uvalue=1
       end
    end

    state.wtid: begin
      widget_control, state.wtid, set_uvalue=1
      widget_control, state.nwtid, set_uvalue=0
      state.ps.wtflag=1
    end

    state.nwtid: begin
      widget_control, state.wtid, set_uvalue=0
      widget_control, state.nwtid, set_uvalue=1
      state.ps.wtflag=2
    end

    state.aid: begin
      widget_control, state.aid, get_uvalue = temp
      if temp(0) eq 1 then begin
        widget_control, state.aid, set_uvalue=0
        state.ps.autoloop=0
        widget_control, state.acol, sensitive = 0
        widget_control, state.awtid, set_button = 0
        widget_control, state.awtid, set_uvalue = 0
        widget_control, state.awtid2, sensitive = 0
      end else begin
        widget_control, state.aid, set_uvalue=1
        state.ps.autoloop=1
        widget_control, state.acol, /sensitive 
      end
    end

    state.awtid: begin
      widget_control, state.awtid, get_uvalue = temp
      if temp(0) eq 1 then begin
        widget_control, state.awtid, set_uvalue = 0
        widget_control, state.awtid, set_button = 0
        state.ps.waitf = 0
        widget_control, state.awtid2, sensitive = 0
        state.ps.waitval = 0.0
      end else begin 
        widget_control, state.awtid, set_uvalue = 1
        widget_control, state.awtid, set_button = 1
        state.ps.waitf = 1
        widget_control, state.awtid2, /sensitive
        widget_control, state.awtid2, /input_focus
        state.ps.waitval = 0.25
        widget_control, state.awtid2, set_value=strtrim(state.ps.waitval,2)
      end 
    end

    state.awtid2: begin
      widget_control, state.awtid2, get_value = temp
      state.ps.waitval = float(temp(0))
    end

    ELSE:       ;**** Do nothing ****

  ENDCASE

        ; Restore the state structure
  WIDGET_CONTROL, stash, SET_UVALUE=state, /NO_COPY

  RETURN, new_event                                  
END

;-----------------------------------------------------------------------------

FUNCTION cw_adas602_proc, parent, bitfile, header1, header2, spectrum,   $
                          proc_lstr, inval, oldstr, VALUE = value,       $
                          FONT_LARGE=font_large, FONT_SMALL=font_small,  $
                          EDIT_FONTS=edit_fonts 


  IF (N_PARAMS() EQ 0) THEN                                              $
  MESSAGE, 'Must specify a parent for CW_ADAS602_PROC'

  ON_ERROR, 2					;return to caller

  IF NOT (KEYWORD_SET(font_large))  THEN font_large = ''
  IF NOT (KEYWORD_SET(font_small))  THEN font_small = ''
  IF NOT (KEYWORD_SET(edit_fonts))  THEN $                
                                edit_fonts = {font_norm:'', font_input:''}
  IF NOT (KEYWORD_SET(value)) then begin

         ;**** set expval as 0.0 to use as keyword to mlcds****
	   
     maxparams=0
     adas602_profiles, profiles, maxparams	   
;     ps = { ps602_set, NEW:0, 		NLINES:0, 		$
     ps = {		     NEW:0, 		NLINES:0, 		$
                       EXPFLAG:0, 		EXPVAL:0.0, 	$
                       WAITF:0, 		WAITVAL:0.0, 	$
                       GRPSCAL:0, 		GRPRMESS:'', 	$
                       XMIN:0.0, 		XMAX:0.0, 		$
                       YMIN:0.0, 		YMAX:0.0, 		$
                       GTIT:'', 		GTITFLAG:0, 	$
                       FIXFLAG:0, 		LIDFLAG:0, 		$
                       FIXPOS:dblarr(10), LINEID:strarr(10),$
                       VWFLAG:1, 		FWFLAG:1, 		$
                       FIXWID:0.0,		BFLAG:0, 		$
                       BFLAT:0, 		BSLOPE:2,	 	$
                       BCURVE:2,		WTFLAG:1, 		$
                       AUTOLOOP:0, 		TITLE:'', 		$
                       FITFLAG:0,		PROFILES: profiles,$
			     PINDEX:0	}  
  ENDIF ELSE begin
;     ps = { ps602_set, NEW:value.new, NLINES:value.nlines,  $
         ps = { NEW:value.new, NLINES:value.nlines,  $
            EXPFLAG:value.expflag, EXPVAL:value.expval,     $
            WAITF:value.waitf, WAITVAL:value.waitval,       $
            GRPSCAL:value.grpscal, GRPRMESS:value.grprmess, $
            XMIN:value.xmin, XMAX:value.xmax,               $
            YMIN:value.ymin, YMAX:value.ymax,               $
            GTIT:value.gtit, GTITFLAG:value.gtitflag,       $
            FIXFLAG:value.fixflag, LIDFLAG:value.lidflag,   $
            FIXPOS:value.fixpos, LINEID:value.lineid,       $
            VWFLAG:value.vwflag, FWFLAG:value.fwflag,       $
            FIXWID:value.fixwid, BFLAG:value.bflag,         $
            BFLAT:value.bflat, BSLOPE:value.bslope,         $
            BCURVE:value.bcurve, WTFLAG:value.wtflag,       $
            AUTOLOOP:value.autoloop, TITLE:value.title,     $
            FITFLAG:value.fitflag,	 PROFILES:value.profiles,$
		PINDEX:value.pindex	}
  ENDELSE
        ; **** add common block for graph area sizes ****
  COMMON Global_lw_data, left, right, tp, bot, grtop, grright
  charsize = (!d.y_vsize/!d.y_ch_size)/60.0
  small_check = GETENV('VERY_SMALL')
  if small_check eq 'YES' then charsize=charsize*0.8

	; effected.  You can see that the event handler is installed here.
	; As events occur in the sub-components of the compound widgets,
	; the events are passed up the tree until they hit this base 
	; where the event handler you define above handles them.  Similarily
	; whenever WIDGET_CONTROL, SET/GET_VALUE is called on this base,
	; the routine defined by the FUNC_GET/PRO_SET_VALUE is called to
	; set the value of the compound widget.  None of the three keywords
	; that override the standard behaviour are not required so it 
	; depends on your usage whether they are needed.
  mainbase = WIDGET_BASE(parent, EVENT_FUNC = "cw_adas602_proc_event",    $
                         FUNC_GET_VALUE = "cw_adas602_proc_get_value", /column)

  row1 = widget_base(mainbase, /row)
  left_col = widget_base(row1, /column)
  right_col = widget_base(row1, /column)

        ;******************************************
        ; **** left hand column rows/draw area ****
        ;******************************************
;  graphid = cw_adas_graph(left_col, xsize=600, ysize=530, $
;                          /nodone, /nolowerbase)
;  gtit_row = widget_base(left_col, /row, /frame)
  graphbase = widget_base(left_col, /row, /frame)

;        ; **** set graph position ****
;  position=[left, bot, grright, grtop]

;        ; **** Graph Title ****
;  gtit_col = widget_base(gtit_row, /column, /exclusive)
;  gtitid1  = widget_button(gtit_col, value='Enter Graph Title : ',         $
;                           font=font_large)
;  gtitid2  = widget_text(gtit_row, value='', /edit, xsize=30, font=font_large)
;  if ps.gtitflag eq 1 then begin
;    widget_control, gtitid1, set_button = 1
;    widget_control, gtitid2, /sensitive
;    widget_control, gtitid2, set_uvalue = 1
;    widget_control, gtitid2, set_value = strtrim(ps.gtit,2)
;  end else begin
;    widget_control, gtitid2, sensitive = 0
;    widget_control, gtitid2, set_uvalue = 0
;  end

;        ; **** Cribbed from cw_adas_ranges to allow immediate ****
;        ; **** graph on-screen rescaling ****
;  gbase = widget_base(graphbase,/column,/nonexclusive)
;  exscid = widget_button(gbase,value='Graph Range Scaling',font=font_large)
;  rngbasid = widget_base(graphbase,/row)
;  minbase = widget_base(rngbasid,/column)
;  maxbase = widget_base(rngbasid,/column)
;  base = widget_base(minbase,/row)
;  rc = widget_label(base, value='X-min :',font=font_large)
;;  xminid = widget_text(base,/editable,xsize=8,font=font_large,/all_events)
;  xminid = widget_text(base,/editable,xsize=8,font=font_large)
;  base = widget_base(maxbase,/row)
;  rc = widget_label(base, value='X-max :',font=font_large)
;;  xmaxid = widget_text(base,/editable,xsize=8,font=font_large,/all_events)
;  xmaxid = widget_text(base,/editable,xsize=8,font=font_large)
;  base = widget_base(minbase,/row)
;  rc = widget_label(base, value='Y-min :',font=font_large)
;;  yminid = widget_text(base,/editable,xsize=8,font=font_large,/all_events)
;  yminid = widget_text(base,/editable,xsize=8,font=font_large)
;  base = widget_base(maxbase,/row)
;  rc = widget_label(base, value='Y-max :',font=font_large)
;;  ymaxid = widget_text(base,/editable,xsize=8,font=font_large,/all_events)
;  ymaxid = widget_text(base,/editable,xsize=8,font=font_large)


;  graphid = cw_adas_graph(left_col, xsize=600, ysize=530, $
;                          /nodone, /nolowerbase)
   graphid = cw_adas_graph(left_col, xsize=600, ysize=480, $
                          /nodone, /nolowerbase)

        ; **** set graph position ****
  position=[left, bot, grright, grtop]				  
				  
  residid= cw_adas_graph(left_col, xsize=600, ysize=125, $
                          /nodone, /nolowerbase)

                ;**** Set initial state according to value ****
  chkoldstr = size(oldstr)


	; **** Automatic/wait time option ****
	
  lbase = widget_base(left_col, /row)
  arow = widget_base(lbase, /row, /frame)
  acol = widget_base(arow, /row)
  aopt_row = widget_base(acol, /row, /nonexclusive)
  aid = widget_button(aopt_row, value='Automatic Cycling', font=font_large)  
  arow = widget_base(acol, /row, /frame)
  acol = widget_base(arow, /column, /nonexclusive)
  awtid = widget_button(acol, value='Wait Time : ', font=font_large)
  awtid2 = widget_text(arow, value='', /edit, xsize=8, font=font_large, $
                       /all_events)

  widget_control, aid, set_button=ps.autoloop
  widget_control, aid, set_uvalue=ps.autoloop
  if chkoldstr(0) ne 0 and chkoldstr(chkoldstr(0)+1) eq 8 then begin
    widget_control, aid, set_button = 1
    widget_control, aid, set_uvalue = 1
    ps.autoloop = 1
  end
  if ps.autoloop eq 0 then begin
    widget_control, acol, sensitive = 0
    widget_control, awtid, set_button = 0
    widget_control, awtid, set_uvalue = 0
    widget_control, awtid2, sensitive = 0
  end else begin
    widget_control, acol, /sensitive
    widget_control, awtid, set_button=ps.waitf
    widget_control, awtid, set_uvalue=ps.waitf
    if ps.waitf eq 1 then begin
      widget_control, awtid2, /sensitive
      widget_control, awtid2, set_value=strtrim(ps.waitval,2)
    end else begin
      widget_control, awtid2, /sensitive
      widget_control, awtid2, set_value=''
    end
  end

	; **** Start Fitting ****
  frow = widget_base(lbase, /row, /frame)
  flbl = widget_label(frow, value='Initiate Fit : ', font=font_large)
  fitfile= bitfile + '/fitbit.bmp'
  read_X11_bitmap, fitfile, bitmap2
  fitid = widget_button(frow, value=bitmap2)


                ;**** Message ****
  if strtrim(ps.grprmess) eq '' then begin
    message = ' '
  end else begin
    message = ps.grprmess
  end
  messid = widget_label(left_col,value=message,font=font_large)
 
        ; **** create initial plot arrays ****
    if inval.dflag eq 0 then begin
       chksize = size(spectrum)
       if chksize(0) eq 1 then begin
         pixinit = indgen(n_elements(spectrum))
       end else if chksize(0) ge 2 then begin
         pixinit = indgen(chksize(1))
       end
       header = header1
    end else if inval.dflag eq 1 then begin
       spectrum = proc_lstr.y
       chksize = size(spectrum)
       pixinit = proc_lstr(0).x
       header = header2
    end   ; can add other data here if necessary


        ;******************************************
        ; **** right hand column buttons etc.  ****
        ;******************************************

  pbutlab = ['Graph Controls','Lines']
  pbut = cw_bgroup(right_col,pbutlab,/exclusive,set_value=0,/row)
  
  right_col2 = widget_base(right_col)
  right1 = widget_base(right_col2,/frame,/column)
  right2 = widget_base(right_col2,/frame,/column)

  gtit_row = widget_base(right1, /row, /frame)

        ; **** Graph Title ****
  gtit_col = widget_base(gtit_row, /column, /exclusive)
  gtitid1  = widget_button(gtit_col, value='Enter Graph Title : ',         $
                           font=font_large)
  gtitid2  = widget_text(gtit_row, value='', /edit, xsize=20, font=font_large)
  if ps.gtitflag eq 1 then begin
    widget_control, gtitid1, set_button = 1
    widget_control, gtitid2, /sensitive
    widget_control, gtitid2, set_uvalue = 1
    widget_control, gtitid2, set_value = strtrim(ps.gtit,2)
  end else begin
    widget_control, gtitid2, sensitive = 0
    widget_control, gtitid2, set_uvalue = 0
  end  
  
        ; **** Cribbed from cw_adas_ranges to allow immediate ****
        ; **** graph on-screen rescaling ****

  gbase = widget_base(right1,/column,/nonexclusive)
  exscid = widget_button(gbase,value='Graph Range Scaling',font=font_large)
  rngbasid = widget_base(right1,/row)
  minbase = widget_base(rngbasid,/column)
  maxbase = widget_base(rngbasid,/column)

  base = widget_base(minbase,/row)
  rc = widget_label(base, value='X-min :',font=font_large)
  xminid = widget_text(base,/editable,xsize=8,font=font_large)

  base = widget_base(maxbase,/row)
  rc = widget_label(base, value='X-max :',font=font_large)
  xmaxid = widget_text(base,/editable,xsize=8,font=font_large)

  base = widget_base(minbase,/row)
  rc = widget_label(base, value='Y-min :',font=font_large)
  yminid = widget_text(base,/editable,xsize=8,font=font_large)

  base = widget_base(maxbase,/row)
  rc = widget_label(base, value='Y-max :',font=font_large)
  ymaxid = widget_text(base,/editable,xsize=8,font=font_large)  

	; **** no. of lines to fit option ****
  nlines_row = widget_base(right2, /frame, /row)
  nlines_l = widget_label(nlines_row, font=font_large, $
                          value='Enter No. of Lines to Fit : ')
  nlineid = widget_text(nlines_row, /edit, xsize=8, font=font_large)
  if ps.nlines eq 0 then begin
     val=''
  end else val=strtrim(ps.nlines,2)
  widget_control, nlineid, set_value=val

        ; **** exposure time option ****
  exp_row = widget_base(right2, /row, /frame)
  exp_col = widget_base(exp_row, /column, /exclusive)
  expid1  = widget_button(exp_col, value='Enter Exposure Time    : ',    $
                          font=font_large)
  expid2  = widget_text(exp_row, value='', /edit, xsize=8, /all_events,  $
                        font=font_large)
  widget_control, expid1, set_button=ps.expflag
  if ps.expflag eq 1 then begin
    widget_control, expid2, /sensitive
    widget_control, expid2, set_uvalue=1
    widget_control, expid2, set_value=strtrim(ps.expval,2)
  end else begin
    widget_control, expid2, sensitive=0
    widget_control, expid2, set_uvalue=0
  end

	  ; **** Container Base for line pos/label & widths ****
  
        ; **** fixed positions and line IDs base ****
  fxidbase = widget_base(right2, /row, /frame)
  fxidcol = widget_base(fxidbase, /column)
  br = widget_base(fxidcol, /row, /nonexclusive)
  
        ; **** switching base for fixed positions/ line IDs ****
  swbaser = widget_base(fxidcol, /row)
  swbase = widget_base(swbaser)
  swcol1 = widget_base(swbase, /column)
  swcol2 = widget_base(swbase, /column)

        ; **** add the buttons ****
  fixid = widget_button(br, value='Fix Line Positions', font=font_large)
  lidid = widget_button(br, value='Identify Lines', font=font_large)  

	; **** map correct screen forward ****
  if ps.fixflag eq 1 then begin
    widget_control, fixid, /set_button
    widget_control, fixid, set_uvalue=1
    widget_control, swcol1, map = 1
    widget_control, swcol1, /sensitive
    widget_control, swcol2, map=0
    if ps.lidflag eq 1 then begin
      widget_control, lidid, set_uvalue=1
      widget_control, lidid, set_button=1
      widget_control, swcol2, sensitive=1
    end else begin
      widget_control, lidid, set_uvalue=0
      widget_control, lidid, set_button=0
      widget_control, swcol2, sensitive=0
    end
  end else if ps.lidflag eq 1 then begin                
    widget_control, lidid, /set_button
    widget_control, lidid, set_uvalue=1
    widget_control, fixid, set_uvalue=0
    widget_control, swcol2, map =1 
    widget_control, swcol2, /sensitive
    widget_control, fixid, set_button=0
    widget_control, swcol1, map=0
    widget_control, swcol1, sensitive=0
  end else begin
    widget_control, fixid, set_uvalue=0
    widget_control, fixid, set_button=0
    widget_control, lidid, set_uvalue=0
    widget_control, lidid, set_button=0
       ; **** temporarily map both away ****
    widget_control, swcol1, map = 0
    widget_control, swcol2, map = 0
  end
        
        ; **** line ID boxes ****
  lbls=[' 1 :',' 2 :',' 3 :',' 4 :',' 5 :',$
        ' 6 :',' 7 :',' 8 :',' 9 :','10 :']
  if ps.fixflag eq 1 then begin
    os = { bndls:ps.fixpos, groups:lonarr(n_elements(ps.fixpos)) }
    fixrow=cw_bndl602_gen(swcol1, lbls, OUTSTR=os, font=font_large, ncols=2, $
			     /noframe, xsize=6, sizecol=5)
  end else begin
    fixrow=cw_bndl602_gen(swcol1, lbls, font=font_large, ncols=2, $
			     /noframe, xsize=6, sizecol=5)
  end
  if ps.lidflag eq 1 then begin
    os = { bndls:ps.lineid, groups:lonarr(n_elements(ps.lineid)) }
    linerow=cw_bndl602_gen(swcol2, lbls, OUTSTR=os, font=font_large, ncols=2,$
				/noframe, xsize=10, sizecol=5)
  end else begin
    linerow=cw_bndl602_gen(swcol2, lbls, font=font_large, ncols=2,$
				/noframe, xsize=10, sizecol=5)
  end

	; **** line width options ****
	
  wrow = widget_base(right2, /row, /frame)  
  wcol = widget_base(wrow, /column)
  wrow2 = widget_base(wcol,/row)
  wlabel = widget_label(wrow2,value='Line Profile:',font=font_large)
  profilenames=['Gaussian','CDS-NIS1','CDS-NIS2','CDS-SOHO']
  wmenusel=cw_bselector(wrow2,profilenames,set_value=ps.pindex, font=font_large) 
  wlabel = widget_label(wrow2,value='   ',font=font_large)
  wpbase=widget_base(wrow2,/row)
  wparbut = widget_button(wpbase,value='Parameters',font=font_large)
  maxparams=0
  adas602_profiles, profiles, maxparams
  paramval=fltarr(maxparams)
  for i=0,ps.profiles[ps.pindex].numberparams-1 do begin
 	paramval[i]=ps.profiles[ps.pindex].params[i].value
  endfor
     
  wr_but = widget_base(wcol, /row) 
  wc_but = widget_base(wr_but, /column)
  wr_but = widget_base(wr_but, /row, /exclusive) 
    vwidl = widget_label(wc_but, value='Allow Variable Line Widths : ', $
                        font=font_large)
    vwidyes = widget_button(wr_but, value='Yes', font=font_large, $
                            /no_release)
    vwidno = widget_button(wr_but, value='No', font=font_large, /no_release)

  wr_but = widget_base(wcol, /row)
  wc_but = widget_base(wr_but, /column)

        ; **** switching base ****
  switchb = widget_base(wc_but)

  rswitch1 = widget_base(switchb, /row, /frame)
  wc_but = widget_base(rswitch1, /column)
  wr_but = widget_base(rswitch1, /row, /exclusive) 
    fwidl = widget_label(wc_but, value='Fit Different Widths : ', $
                        font=font_large)
    fwidyes = widget_button(wr_but, value='Yes', font=font_large, $
                            /no_release)
    fwidno = widget_button(wr_but, value='No', font=font_large, /no_release)

        ; **** other base ****
  rswitch2 = widget_base(switchb, /row, /frame)
  inpwid_lbl = widget_label(rswitch2, value='Input Fixed Width : ',$
                            font=font_large)
  inpwid = widget_text(rswitch2, /edit, xsize=8, font=font_large)    

        ; **** initial set up ****
  if ps.vwflag eq 1 then begin
    widget_control, rswitch1, map=1
    widget_control, rswitch1, /sensitive 
    widget_control, vwidyes, set_uvalue = 1
    widget_control, vwidyes, set_button = 1
    widget_control, vwidno, set_uvalue = 0
    widget_control, vwidno, set_button = 0
    if ps.fwflag eq 1 then begin
      widget_control, fwidyes, set_button = 1
      widget_control, fwidno, set_button = 0
      widget_control, fwidyes, set_uvalue = 1
      widget_control, fwidno, set_uvalue = 0
    end else begin
      widget_control, fwidyes, set_button = 0
      widget_control, fwidno, set_button = 1
      widget_control, fwidyes, set_uvalue = 0
      widget_control, fwidno, set_uvalue = 1
    end
    widget_control, rswitch2, map=0
    widget_control, rswitch2, sensitive = 0
  end else begin
    widget_control, rswitch1, map=0
    widget_control, rswitch1, sensitive = 0
    widget_control, vwidyes, set_uvalue = 0
    widget_control, vwidyes, set_button = 0
    widget_control, vwidno, set_uvalue = 1
    widget_control, vwidno, set_button = 1
    widget_control, fwidyes, set_uvalue = 0
    widget_control, fwidyes, set_button = 0
    widget_control, fwidno, set_uvalue = 0
    widget_control, fwidno, set_button = 0
    widget_control, rswitch2, map=1
    widget_control, rswitch2, /sensitive 
;    widget_control, inpwid, /input_focus
  end

	; **** Background Options ****
  brow = widget_base(right2, /row, /frame)
  bcol = widget_base(brow, /column)
  bopt_row = widget_base(bcol, /row, /nonexclusive)
  brow = widget_base(bcol, /row, /frame, /nonexclusive)
  boptid = widget_button(bopt_row, value='Background Options', $
                         font=font_large)
  bconid = widget_button(brow, value='Constant', font=font_large)
  blinid = widget_button(brow, value='Linear', font=font_large)
  bquadid = widget_button(brow, value='Quadratic', font=font_large)

        ; **** initial set up ****
  if ps.bflag eq 1 then begin
    widget_control, boptid, set_button=1
    widget_control, bconid, set_button=1
    widget_control, boptid, set_uvalue=1
    widget_control, bconid, set_uvalue=1
    widget_control, brow, /sensitive
    if ps.bslope eq 1 then begin
      widget_control, blinid, set_button=1
      widget_control, blinid, set_uvalue=1
    end else begin
      widget_control, blinid, set_button=0
      widget_control, blinid, set_uvalue=0
    end
    if ps.bcurve eq 1 then begin
      widget_control, bquadid, set_button=1
      widget_control, bquadid, set_uvalue=1
    end else begin
      widget_control, bquadid, set_button=0
      widget_control, bquadid, set_uvalue=0
    end
  end else begin
    widget_control, boptid, set_button=0
    widget_control, bconid, set_button=0
    widget_control, blinid, set_button=0
    widget_control, bquadid, set_button=0
    widget_control, boptid, set_uvalue=0
    widget_control, bconid, set_uvalue=0
    widget_control, blinid, set_uvalue=0
    widget_control, bquadid, set_uvalue=0
    widget_control, brow, sensitive=0
  end

	; **** Weighting options ****
  wtrow = widget_base(right2, /row, /frame, /exclusive)
  wtid = widget_button(wtrow, value='Weighted Fit', font=font_large, $
                       /no_release)
  nwtid = widget_button(wtrow, value='Equal Weighted Fit', $
                        font=font_large, /no_release)

        ; **** initial set up **** 
  if ps.wtflag eq 1 then begin
    widget_control, wtid, set_button=1
    widget_control, wtid, set_uvalue=1
    widget_control, nwtid, set_uvalue=0
  end else begin
    widget_control, nwtid, set_button=1
    widget_control, wtid, set_uvalue=0
    widget_control, nwtid, set_uvalue=1
  end

        ; **** initital set up if oldstr set ****
  if chkoldstr(0) ne 0 and chkoldstr(chkoldstr(0)+1) eq 8 then begin
    widget_control, nlines_row, sensitive = 0 
    widget_control, exp_row, sensitive = 0 
    widget_control, fxidbase, sensitive = 0 
    widget_control, wrow, sensitive = 0 
    widget_control, brow, sensitive = 0 
    widget_control, bopt_row, sensitive = 0 
    widget_control, wtrow, sensitive = 0 
  end

;	; **** Automatic/wait time option ****
;	
;  lbase = widget_base(left_col, /row)
;  arow = widget_base(lbase, /row, /frame)
;  acol = widget_base(arow, /row)
;  aopt_row = widget_base(acol, /row, /nonexclusive)
;  aid = widget_button(aopt_row, value='Automatic Cycling', font=font_large)  
;  arow = widget_base(acol, /row, /frame)
;  acol = widget_base(arow, /column, /nonexclusive)
;  awtid = widget_button(acol, value='Wait Time : ', font=font_large)
;  awtid2 = widget_text(arow, value='', /edit, xsize=8, font=font_large, $
;                       /all_events)
;
;  widget_control, aid, set_button=ps.autoloop
;  widget_control, aid, set_uvalue=ps.autoloop
;  if chkoldstr(0) ne 0 and chkoldstr(chkoldstr(0)+1) eq 8 then begin
;    widget_control, aid, set_button = 1
;    widget_control, aid, set_uvalue = 1
;    ps.autoloop = 1
;  end
;  if ps.autoloop eq 0 then begin
;    widget_control, acol, sensitive = 0
;    widget_control, awtid, set_button = 0
;    widget_control, awtid, set_uvalue = 0
;    widget_control, awtid2, sensitive = 0
;  end else begin
;    widget_control, acol, /sensitive
;    widget_control, awtid, set_button=ps.waitf
;    widget_control, awtid, set_uvalue=ps.waitf
;    if ps.waitf eq 1 then begin
;      widget_control, awtid2, /sensitive
;      widget_control, awtid2, set_value=strtrim(ps.waitval,2)
;    end else begin
;      widget_control, awtid2, /sensitive
;      widget_control, awtid2, set_value=''
;    end
;  end
;
;	; **** Start Fitting ****
;  frow = widget_base(lbase, /row, /frame)
;  flbl = widget_label(frow, value='Initiate Fit : ', font=font_large)
;  fitfile= bitfile + '/fitbit.bmp'
;  read_X11_bitmap, fitfile, bitmap2
;  fitid = widget_button(frow, value=bitmap2)

	; **** map correct panel ***
  
  widget_control, right1, map=1
  widget_control, right2, map=0  


	; **** add the exit buttons ****
  base = widget_base(mainbase, /row)
  menufile = bitfile + '/menu.bmp'
  read_X11_bitmap, menufile, bitmap1
  outid = widget_button(base, value=bitmap1)      ;menu button
  cancelid = widget_button(base,value='Cancel',font=font_large)
  doneid = widget_button(base,value='Done',font=font_large)

        ;**** realize the graph area at the end ****  
  widget_control, graphid, /realize
  widget_control, graphid, get_value=grval
  wset, grval.win

        ;**** now plot on it and check on dimensionality ****
        ;**** to display first plot only if cycling ****
  if ps.grpscal ne 1 then begin
    if chksize(0) eq 1 then begin
      plot, pixinit, spectrum, xtitle='pixel no.', ytitle='counts'
	widget_control, residid, get_value=resval
      wset,resval.win
	plot, pixinit, spectrum, xtitle='pixel no.', ytitle='counts'
	widget_control, graphid, get_value=grval
	wset,grval.win
    end else if chksize(0) eq 2 then begin
      plot, pixinit, spectrum(*,0), xtitle='pixel no.', ytitle='counts'
	widget_control, residid, get_value=resval
      wset,resval.win
	plot, pixinit, spectrum(*,0), xtitle='pixel no.', ytitle='counts'
	widget_control, graphid, get_value=grval
	wset,grval.win	
    end
  end

  if chkoldstr(0) ne 0 and chkoldstr(chkoldstr(0)+1) eq 8 then ps.grpscal=1
  if inval.dflag eq 1 then ps.grpscal=1
  if ps.grpscal eq 1 then begin
    widget_control,exscid,set_button=1
    widget_control,exscid,set_uvalue=1
    if chkoldstr(0) eq 0 and chkoldstr(chkoldstr(0)+1) ne 8 then begin
      if inval.dflag eq 1 then begin
        ps.xmin=proc_lstr(0).xmin
        ps.xmax=proc_lstr(0).xmax
      end
      widget_control,xminid,set_value=strtrim(ps.xmin,2)	
      widget_control,xmaxid,set_value=strtrim(ps.xmax,2)	
      widget_control,yminid,set_value=strtrim(ps.ymin,2)	
      widget_control,ymaxid,set_value=strtrim(ps.ymax,2)	

                ; **** replot graph ****
            plot, pixinit, spectrum,                                 $
                  xtitle='pixel no.',                                $
                  ytitle='counts', xr=[ps.xmin,ps.xmax],             $
                  /xstyle, yr=[ps.ymin,ps.ymax], /ystyle
		widget_control, residid, get_value=resval
      	wset,resval.win
		plot, pixinit, spectrum, xtitle='pixel no.', ytitle='counts',	$
		xr=[ps.xmin,ps.xmax], /xstyle, yr=[ps.ymin,ps.ymax], /ystyle,/nodata		
		widget_control, graphid, get_value=grval
		wset,grval.win
			
    end else begin
      widget_control,xminid,set_value=strtrim(oldstr.xmin,2)	
      widget_control,xmaxid,set_value=strtrim(oldstr.xmax,2)	
      widget_control,yminid,set_value='0.0'
      widget_control,ymaxid,set_value='0.0'

                ; **** replot graph ****
            plot, pixinit, spectrum,                                 $
                  xtitle='pixel no.',                                $
                  ytitle='counts', xr=[oldstr.xmin,oldstr.xmax], /xstyle
			
		widget_control, residid, get_value=resval
      	wset,resval.win
		plot, pixinit, spectrum, xtitle='pixel no.', ytitle='counts',	$
		xr=[ps.xmin,ps.xmax], /xstyle, yr=[ps.ymin,ps.ymax], /ystyle,/nodata		
		widget_control, graphid, get_value=grval
		wset,grval.win
    end
  end else begin
    widget_control,rngbasid,sensitive=0
    widget_control,exscid,set_uvalue=0
  end

        ;**** realize the graph area at the end ****  
  widget_control,residid, /realize
;  widget_control, residid, get_value=rval
;  wset, rval.win
;  residid=rval.win
;  plot, pixinit, spectrum
;  wset, grval.win
  
	    ; *** create pixmap window of plot without multiplet. ***

;     pwidth  = !d.x_size
;     pheight = !d.y_size
;     gwin=!d.window
;     pwin=9
	pwin=long(0)
;     window,pwin,/pixmap,xsize=pwidth,ysize=pheight
;     device,copy=[0,0,pwidth,pheight,0,0,gwin]
;     wset,gwin
     
	; Save out the initial state structure into the first childs UVALUE.
 btype=0
  state = { OUTID:outid, CANCELID:cancelid, DONEID:doneid, $
            EXPID1:expid1, EXPID2:expid2, GTITID1:gtitid1, $
            GTITID2:gtitid2, GRAPHID:graphid, RESIDID:residid, EXSCID:exscid, $
            RNGBASID:rngbasid, MESSID:messid, NLINEID:nlineid, $
            XMINID:xminid, XMAXID:xmaxid, YMINID:yminid, $
            YMAXID:ymaxid, PIXINIT:pixinit, $
            PS:ps, FONT:font_large, FIXID:fixid, LIDID:lidid, $
            FIXROW:fixrow, LINEROW:linerow, SWCOL1:swcol1, $
            SWCOL2:swcol2, WMENUSEL	:wmenusel,	WPARBUT:wparbut,	$
		WPBASE: wpbase, PBUT: pbut, RIGHT1:right1, RIGHT2:right2, $							$
	      PARAMVAL: paramval, MAXPARAMS: maxparams, BTYPE: btype, $
		VWIDYES:vwidyes, VWIDNO:vwidno , $
            FWIDYES:fwidyes, FWIDNO:fwidno, INPWID:inpwid, $
            RSWITCH1:rswitch1, RSWITCH2:rswitch2, BOPTID:boptid, $
            BCONID:bconid, BLINID:blinid, BQUADID:bquadid, $
            BROW:brow, WTID:wtid, NWTID:nwtid, AID:aid, FITID:fitid, $
            ACOL:acol, AWTID:awtid, AWTID2:awtid2, $
            SPECTRUM:spectrum, HEADER:header, OLDSTR:oldstr, $
            POSITION:position, CHARSIZE:charsize, INVAL:inval, pwin:pwin}

  WIDGET_CONTROL, WIDGET_INFO(mainbase, /CHILD), SET_UVALUE=state, /NO_COPY

  RETURN, mainbase

END





