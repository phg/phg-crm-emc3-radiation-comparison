; Copyright (c) 1995 Strathclyde University .
; SCCS info: Module @(#)$Header: /home/adascvs/idl/adas6xx/adas602/adas602_in.pro,v 1.2 2010/11/30 07:58:00 mog Exp $ Date $Date: 2010/11/30 07:58:00 $
;+
; PROJECT:
;       ADAS IBM MVS to DEC UNIX conversion
;
; NAME:
;	ADAS602_IN
;
; PURPOSE:
;	IDL ADAS user interface, input file selection.
;
; EXPLANATION:
;	This routine creates and manages a pop-up window which allows
;	the user to select two files from the UNIX file system. It is
;	addapted from adas_in, hence the existence of the wintitle and 
;	title keywords.
;
; USE:
;	See the ADAS602 routine f2spf0.pro for an example of how to
;	use this routine.
;
; INPUTS:
;	VAL	- A structure which determines the initial settings of
;		  the dataset selection widget.  VALUE is passed un-modified
;		  through to cw_adas602_in.pro, see that routine for a full
;		  description.
;
; OPTIONAL INPUTS:
;	None
;
; OUTPUTS:
;	VAL	- On output the structure records the final settings of
;		  the dataset selection widget if the user pressed the
;		  'Done' button otherwise it is not changed from input.
;
;	ACT	- A string; indicates the user's action when the pop-up
;		  window is terminated, i.e which button was pressed to
;		  complete the input.  Possible values are 'Done' and
;		  'Cancel'.
;
; OPTIONAL OUTPUTS:
;	None
;
; KEYWORD PARAMETERS:
;	WINTITLE- A title to be used on the banner for the pop-up window.
;		  Intended to indicate which application is running,
;		  e.g 'ADAS 602 INPUT'
;
;	TITLE	- The title to be included in the input file widget, used
;		  to indicate exactly what the required input dataset is,
;		  e.g 'Input COPASE Dataset'
;
;	FONT	- Supplies the font to be used for the interface widgets.
;
; CALLS:
;	CW_ADAS602_IN	Dataset selection widget creation.
;	DYNLABEL	Sets dynamic_resize keyword for label widgets.
;	XMANAGER	Manages the pop=up window.
;	ADAS602_IN_EV	Event manager, called indirectly during XMANAGER
;			event management.
;
; SIDE EFFECTS:
;	XMANAGER is called in /modal mode. Any other widgets become
;	inactive.
;
; CATEGORY:
;	Adas system.
;
; WRITTEN:
;       David H.Brooks, Universiy of Strathclyde, date unknown
;
; MODIFIED:
;       Version 1.0     David Brooks
;			Written using adas_in.pro v1.4 as a template
;	Version 1.1    	Richard Martin
;			Changed documentation + put under SCCS control
;	1.3    	Martin O'Mullane
;		Rename common block in_blk to in602_blk.
;
; VERSION:
;	1.0	????????
;	1.1	12-06-97
;	1.3	30-11-2010
;-
;-----------------------------------------------------------------------------


PRO adas602_in_ev, event

  COMMON in602_blk,action,value

		;**** Find the event type and copy to common ****
    action = event.action

    CASE action OF

		;**** 'Done' button ****
	'Done'  : begin

			;**** Get the output widget value ****
		widget_control,event.id,get_value=value 
		widget_control,event.top,/destroy

	   end


		;**** 'Cancel' button ****
	'Cancel': widget_control,event.top,/destroy

    END

END

;-----------------------------------------------------------------------------


PRO adas602_in, val, act, WINTITLE = wintitle, TITLE = title, FONT = font

  COMMON in602_blk,action,value

		;**** Copy value to common ****
  value = val

		;**** Set defaults for keywords ****
  IF NOT (KEYWORD_SET(wintitle)) THEN wintitle = 'ADAS INPUT FILE'
  IF NOT (KEYWORD_SET(title)) THEN title = ''
  IF NOT (KEYWORD_SET(font)) THEN font = ''

		;***************************************
		;**** Pop-up a new widget if needed ****
		;***************************************

                ;**** create base widget ****
  inid = widget_base(TITLE=wintitle,XOFFSET=100,YOFFSET=0)

		;**** Declare output options widget ****
  cwid = cw_adas602_in(inid, VALUE=value, TITLE=title, FONT=font)

		;**** Realize the new widget ****
  dynlabel, inid
  widget_control,inid,/realize

		;**** make widget modal ****
  xmanager,'adas602_in',inid,event_handler='adas602_in_ev',/modal,/just_reg
 
		;**** Return the output value from common ****
  act = action
  val = value

END

