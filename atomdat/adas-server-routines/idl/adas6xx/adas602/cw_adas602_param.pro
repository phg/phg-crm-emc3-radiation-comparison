; Copyright (c) 2001 Strathclyde University .
; SCCS info: Module @(#)$Header: /home/adascvs/idl/adas6xx/adas602/cw_adas602_param.pro,v 1.1 2004/07/06 12:54:11 whitefor Exp $ Date $Date: 2004/07/06 12:54:11 $
;
; To be documented
;
; MODIFIED:
;       1.1     Richard Martin
;
; DATE
;	1.1	20-07-01
;
;-------------------------------------------------------------------------------------------------
PRO cw_adas602_param_event, event

    COMMON outvalblock, prof, outval
 
                ;**** Base ID of compound widget ****

    parent=event.handler

                ;**********************************************
                ;**** Retrieve the user value state        ****
                ;**** Get id of first_child widget because ****
                ;**** user value "state" is stored there   ****
                ;**********************************************

    first_child = widget_info(parent, /child)
    widget_control, first_child, get_uvalue=state, /no_copy

                ;************************
                ;**** Process events ****
                ;************************

    destroy = 0
    
    CASE event.id OF
    
      state.defbut: begin
	
		adas602_profiles, profiles, state.maxparams
		for i=0,state.maxparams-1 do begin
    		    if prof[state.index].params[i].name ne 'NULL' then begin
		      dummy=strtrim(string(profiles[state.index].params[i].value,format='(F5.3)'),2)			
		    	widget_control, state.inputval[i], set_value=dummy
			prof[state.index].params[i].value=profiles[state.index].params[i].value			
		    endif
		end	
	end
	
	state.cbut: begin
		widget_control, event.top, /destroy		
		destroy = 1			
	end
		
	state.dbut: begin
		message=''
		for i=0,state.maxparams-1 do begin
    		    if prof[state.index].params[i].name ne 'NULL' then begin	
		    	widget_control, state.inputval[i], get_value=temp
		    	error = num_chk(temp(0))
		
		    	if error gt 0 AND message eq '' then begin
				pdummy=prof[state.index].params[i].name
				message='Error: '+ pdummy + ' is not a number.'
		    	endif else begin
				outval(i)=temp(0)
				prof[state.index].params[i].value=temp(0)
			endelse
		    endif
		end
		
		widget_control, state.messrow, set_value=message
		
		if message eq '' then begin
			
			widget_control, event.top, /destroy		
			destroy = 1	
		endif
	end
	
	ELSE:			;do nothing

    ENDCASE

                ;*********************************************
                ;*** make "state" available to other files ***
                ;*********************************************

    if destroy eq 0 then widget_control, first_child, set_uvalue=state, /no_copy

END

;------------------------------------------------------------------------------
FUNCTION cw_adas602_param, profiles, index, maxparams, outputval, MESSAGE=message, TITLE=title, $
                XOFFSET=xoffset, YOFFSET=yoffset, FONT=font, VALUE = value_in    

    
    COMMON  outvalblock, prof, outval

    outval = outputval
    prof = profiles
    		    
    ON_ERROR, 2
	
            ;**** Set defaults for keywords ****
		;**** Default value is set later ***
		
    IF NOT (KEYWORD_SET(message)) THEN message = 'Please enter a value: '
    IF NOT (KEYWORD_SET(title)) THEN title = 'Slider values'
    IF NOT (KEYWORD_SET(xoffset)) THEN xoffset = 500
    IF NOT (KEYWORD_SET(yoffset)) THEN yoffset = 500
    IF NOT (KEYWORD_SET(font)) THEN font = ''
    
            ;**** create titled base widget ****

    topparent = widget_base(title=title,  EVENT_FUNC = "cw_adas602_param_event",	$
    				     FUNC_GET_VALUE = "cw_adas602_param_get_value",$
				     xoffset=xoffset, yoffset=yoffset, /column)
				    
	      ;******************************************************
            ;**** Create a dummy widget just to hold value of *****
            ;**** "state" variable so as not to get confused  *****
            ;**** with any other values. Adopt IDL practice   *****
            ;**** of using first child widget			  *****
            ;******************************************************

    first_child = widget_base(topparent)

		;**** Create parent for the rest of the widget ****

    parent = widget_base(first_child, /column)

    profbase = widget_base(parent, /column)
    dummy=prof[index].name
    wlabel = widget_label(profbase, value=dummy, font=font)
    wlabel = widget_label(profbase, value=' ', font=font)
    
    dummy=strtrim(string(prof[index].params[0].maximum),2)
    
    inputval=fltarr(maxparams)
    for i=0,maxparams-1 do begin
    	if prof[index].params[i].name ne 'NULL' then begin
		dummyrow = widget_base(profbase,/row)
		dummy='                :'
		pdummy=prof[index].params[i].name
		strput,dummy,pdummy
    		wlabel = widget_label(dummyrow, value=dummy, font=font)
		dummy=strtrim(string(prof[index].params[i].value,format='(F5.3)'),2)
		if index eq 3 then begin
			inputval(i) = widget_text(dummyrow, value=dummy,xsize=8, font=font, /editable)
		endif else begin
			inputval(i) = widget_text(dummyrow, value=dummy,xsize=8, font=font)
		endelse		 
      endif
    endfor
    
    		;**** Create defaults button ***

    wlabel = widget_label(profbase, value=' ', font=font)
    if index eq 3 then begin
    	defbut = widget_button(profbase,value='Default Values (NIS2)',font=font) 
    endif else begin
     	defbut = widget_button(profbase,value='Default Values',font=font)    
    endelse

    		;**** Create message line ***    

    rc = widget_label(profbase,value='',font=font)
    message = '                                        '	
    messrow = widget_label(profbase,value=message,font=font)
    
    
		;**** Create done button ****

    dcbutbase = widget_base(parent,/row)		
    dbut = widget_button(dcbutbase,value='Done',font=font)   
    cbut = widget_button(dcbutbase,value='Cancel',font=font)  
                 
    		    ;*************************************************
                ;**** create a state structure for the pop-up ****
                ;****                window.                  ****
                ;*************************************************

    new_state = { dbut		: 	dbut	,		$
    			cbut		:     cbut  ,		$
    			defbut	:	defbut,		$
    			messrow	:	messrow,		$
			index		:	index,		$
			maxparams	:	maxparams,		$
    			inputval	:	inputval	}

                 ;**** Save initial state structure ****

    widget_control, first_child, set_uvalue=new_state, /no_copy

                ;**** realize the widget ****

    widget_control, topparent, /realize
   
                ;**** make widget modal ****

    xmanager, 'cw_adas602_param', topparent, /modal, /just_reg

                ;**** Return the output value from common ****

    outputval = outval
    
    profiles = prof
    
    RETURN, topparent

END
