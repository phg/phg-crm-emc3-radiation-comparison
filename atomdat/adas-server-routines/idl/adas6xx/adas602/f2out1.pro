; Copyright (c) 1997 Strathclyde University .
; SCCS info: Module @(#)$Header: /home/adascvs/idl/adas6xx/adas602/f2out1.pro,v 1.2 2004/07/06 13:51:20 whitefor Exp $ Date $Date: 2004/07/06 13:51:20 $
;+
; PROJECT:
;       ADAS IBM MVS to DEC UNIX conversion
;
; NAME:
;	F2OUT1
;
; PURPOSE:
;	Text output for ADAS602.
;
; EXPLANATION:
;	This routine writes text output to a file.
;
; USE:
;	Use is specific to ADAS602. See adas602.pro for example.
;
; INPUTS:
;
;	INVAL	- Structure; the input screen settings. See cw_adas602_in.pro
;
;	PROCVAL	- Structure containg the processing options. 
;		  See cw_adas602_proc.pro
;
;	OLDSTR  - A structure derived from a processed template also
;		  containing previously used fitting parameters.
;
;	LSTR	- Structure containing processed data.
;
;	ADASHEADER- Full header for output (release:prog:date:time)
;
;	HEADER1 - Header of the input data.
;
;	HEADER2 - Header of the processed template.
;
;	DATEO	- String containing the dateo.
;
;	DSNINC	- String; The full system file name of the input 
;		  dataset selected by the user for processing.
;	
;	DSNINX	- String; The full system file name of the input processed
;		  template.
;
;
; OPTIONAL INPUTS:
;	None.
;
; OUTPUTS:
;	None.
;
; OPTIONAL OUTPUTS:
;	None.
;
; KEYWORD PARAMETERS:
;	None
;
; CALLS:
;	None.
;
; SIDE EFFECTS:
;	Outputs text to a file.
;
; CATEGORY:
;	Adas system.	
;
; WRITTEN:
;       David Brooks, University of Strathclyde 1997
;
; MODIFIED:
;       1.1    	Richard Martin
;              	Changed documentation + put under SCCS control
;	  1.2		Richard Martin
;			Changed date to dateo to be consistent with adas602.pro
;			v 1.6 and avoid clashes with idl5.0 routine date.
;
; VERSION:
;	1.1	17-06-97
;	1.2   19-06-98
;
;-
;----------------------------------------------------------------------------

PRO f2out1, inval, procval, outval, lstr, adasheader, header1, header2, $
            dateo, dsninc, dsninx

                ;**** Open file ****

  openw, unit, /get_lun, outval.texdsn

    str=' '
    printf, unit, adasheader, format='(1a80)'    
    printf, unit, '************* TABULAR OUTPUT FROM MAXIMUM LIKELIHOOD'+$
                  ' SPECTRAL LINE PROFILE FITTING PROGRAM: ADAS602 -'+$
                  ' DATE: '+strupcase(dateo(0))+' *************'
    printf, unit, str
    if inval.dflag eq 0 then begin
      printf, unit, ' DATA FILENAME : '+strtrim(dsninc,2)+$
                    ' DATA TYPE : STANDARD'
    end else if inval.dflag eq 1 then begin
      printf, unit, ' DATA FILENAME : '+strtrim(dsninc,2)+$
                    ' DATA TYPE : PROCESSED'
    end else if inval.dflag eq 2 then begin
      printf, unit, ' DATA FILENAME : '+strtrim(dsninc,2)+$
                    '               DATA TYPE : OTHER'
    end
    if strtrim(dsninx,2) ne '' then begin
      printf, unit, '      TEMPLATE : ',dsninx, format= '(1a17,1a50)'
    end else begin
      printf, unit, '      TEMPLATE : NONE '
    end
    hdchk = size(lstr(0).header)
    if hdchk(hdchk(0)+1) ne 8 then begin
      printf, unit, ' HEADER : ',strtrim(lstr(0).header,2)
    end
    printf, unit, str

                ;**** Now list fitting options ****
    printf, unit, '--------- LINE WIDTH OPTIONS ---------'
    printf, unit, '--------------------------------------'
    if lstr(0).getfit(0) eq 1 then begin
      printf, unit,   ' ALL LINE WIDTHS FIXED ON INPUT   : N '
      if lstr(0).getfit(1) eq 1 then begin
        printf, unit, ' VARIABLE LINE WIDTHS WERE FITTED : Y ' 
      end else begin
        printf, unit, ' VARIABLE LINE WIDTHS WERE FITTED : N ' 
      end
    end else begin
      printf, unit,   ' ALL LINE WIDTHS FIXED ON INPUT   : Y '+$
                      ' - CHOSEN WIDTH : '+strtrim(lstr(0).w(0),2)
    end
    printf, unit, str
    printf, unit, '--------- BACKGROUND OPTIONS ---------'
    printf, unit, '--------------------------------------'
    if lstr(0).getfit(5) ne 1 then begin
      printf, unit, ' NO BACKGROUND IN DATA - ALL OPTIONS DISABLED '
    end else begin
      printf, unit, ' CONSTANT COMPONENT  : Y '
      if lstr(0).getfit(2) eq 1 then begin
        printf, unit, ' LINEAR COMPONENT    : Y '
      end else printf, unit, ' LINEAR COMPONENT    : N '
      if lstr(0).getfit(3) eq 1 then begin
        printf, unit, ' QUADRATIC COMPONENT : Y '
      end else printf, unit, ' QUADRATIC COMPONENT : N '
    end
    printf, unit, str
    printf, unit, '------------- OTHER OPTIONS --------------'
    if lstr(0).getfit(4) eq 1 then begin
      printf, unit, ' WEIGHTED FIT       : Y '
    end else begin
      printf, unit, ' EQUAL WEIGHTED FIT : Y '
    end
    printf, unit, ' INFO ERROR FLAG FROM MINPACK ROUTINE : '+$
                  strtrim(lstr(0).info,2)
    printf, unit, ' EXPOSURE TIME : '+strtrim(lstr(0).exptime,2)
    printf, unit, ' NO. OF LINES  : '+strtrim(lstr(0).numline,2)+$
                  '             NO. OF LINES WITH FIXED POSITIONS : '+$
                    strtrim(lstr(0).nfxl,2)
    
               ;**** Now output table ****
    printf, unit, str
    printf, unit, str
    printf, unit, ' KEY '
    printf, unit, '-----'
    printf, unit, ' ESE - ESTIMATED STANDARD ERROR (%)'
    printf, unit, ' ECL - ESTIMATED 95% CONFIDENCE LIMIT (%)'
    printf, unit, '-----------------------------------------------------'+$
    ' OUTPUT TABLE -----------------------------------------------------'
    for i = 0, n_elements(lstr)-1 do begin
      wtst = 0
      printf, unit, str
      printf, unit, ' DATASET : '+strtrim(i+1,2)
      printf, unit, '--------------
      printf, unit, ' BACKGROUND '
      printf, unit, '------------'
      printf, unit, ' CONSTANT  : '+strtrim(lstr(i).b0,2)+'        ESE : '+$
              strtrim(lstr(i).eseb0,2)+'        ECL : '+$
              strtrim(lstr(i).eclb0,2)
      printf, unit, ' LINEAR    : '+strtrim(lstr(i).b1,2)+'        ESE : '+$
              strtrim(lstr(i).eseb1,2)+'        ECL : '+$
              strtrim(lstr(i).eclb1,2)
      printf, unit, ' QUADRATIC : '+strtrim(lstr(i).b2,2)+'        ESE : '+$
              strtrim(lstr(i).eseb2,2)+'        ECL : '+$
              strtrim(lstr(i).eclb2,2)
      printf, unit, ' INDEX  POSITION                        HEIGHT '+$
       '                         FLUX                               '+$
       '  WIDTH                         IDENTIFICATION  '
      printf, unit, '          (XO)      (ESEXO)    (ECLXO)     (H) '+$
       '      (ESEH)     (ECLH)                (ESEF)     (ECLF)    '+$
       '            (ESEW)     (ECLW)                   '
      printf, unit, '-----------------------------------------------'+$
       '------------------------------------------------------------'+$
       '------------------------------------------------'
      if lstr(i).getfit(0) ne 1 then wtst=1
      if lstr(i).getfit(0) eq 1 and lstr(i).getfit(1) ne 1 then wtst=1
      for j = 0, lstr(i).numline-1 do begin
       if wtst eq 1 then begin
         printf, unit, j+1, lstr(i).xo(j), lstr(i).esexo(j), lstr(i).eclxo(j),$
         lstr(i).h(j), lstr(i).eseh(j), lstr(i).eclh(j), lstr(i).flux(j),  $
         lstr(i).esef(j), lstr(i).eclf(j), lstr(i).w(0), lstr(i).esewl,    $
         lstr(i).eclwl, lstr(i).lineid(j),                                 $
         format='(2x,i2,12(1x,d10.3),1a10)' 
       end else begin
         printf, unit, j+1, lstr(i).xo(j), lstr(i).esexo(j), lstr(i).eclxo(j),$
         lstr(i).h(j), lstr(i).eseh(j), lstr(i).eclh(j), lstr(i).flux(j),  $
         lstr(i).esef(j), lstr(i).eclf(j), lstr(i).w(j), lstr(i).esewv(j), $
         lstr(i).eclwv(j), lstr(i).lineid(j),                              $
         format='(2x,i2,12(1x,d10.3),1a10)' 
       end
      end
    end
    printf, unit, '-----------------------------------------------'+$
    '------------------------------------------------------------'+$
    '------------------------------------------------'
  
  free_lun, unit

                ;**** If text output is requested enable append ****
                ;**** for next time and update the default to   ****
                ;**** the current text file.                    ****

  outval.texapp=0
  outval.texdef = outval.texdsn
  if outval.texrep ge 0 then outval.texrep = 0
  outval.texmes = 'Output written to file.'

END
