; Copyright (c) 2001 Strathclyde University .
; SCCS info: Module @(#)$Header: /home/adascvs/idl/adas6xx/adas602/adas602_profiles.pro,v 1.1 2004/07/06 11:02:09 whitefor Exp $    1.1 Date $Date: 2004/07/06 11:02:09 $
;
; To be documented
;
; MODIFIED:
;       1.1     Richard Martin
;
; DATE
;       1.1     20-07-01
;
;-------------------------------------------------------------------------------------------------
pro adas602_profiles,profiles,maxparams	

	maxparams=11
	maxprofiles=10

	paramstruct={	name:'NULL',		$
			minimum:double(0),	$
			maximum:double(0),	$
			format:'(F10.3)',		$
			value: double(0)		$
			}

	profilestruct={	name:'NULL',				 $
			params:replicate(paramstruct,maxparams), $
			numberparams:long(0), 		 	 $
			fortroutine:fix(0)			 $
		}

	profiles=replicate(profilestruct,maxprofiles)




	;stop

	
	; Gaussian
	
	profiles[0].name='Gaussian'
	profiles[0].numberparams=1
	profiles[0].fortroutine=0
	
		profiles[0].params[0].name='Half Width / Pixels'
		profiles[0].params[0].minimum=5
		profiles[0].params[0].maximum=50
		profiles[0].params[0].value='0.0'		

	; Doppler profile
	
	
	; Voigt
		
	; CDS NIS1
	
	profiles[1].name='CDS-NIS1'
	profiles[1].numberparams=2
	profiles[1].fortroutine=1
	
		profiles[1].params[0].name='Alpha Left'
		profiles[1].params[0].minimum=0
		profiles[1].params[0].maximum=1
		profiles[1].params[0].value='0.8'
	
		profiles[1].params[1].name='Alpha Right'
		profiles[1].params[1].minimum=0
		profiles[1].params[1].maximum=1
		profiles[1].params[1].value='0.8'

	; CDS NIS2
	
	profiles[2].name='CDS-NIS2'
	profiles[2].numberparams=2
	profiles[2].fortroutine=1
	
		profiles[2].params[0].name='Alpha Left'
		profiles[2].params[0].minimum=0
		profiles[2].params[0].maximum=1
		profiles[2].params[0].value='0.088'
	
		profiles[2].params[1].name='Alpha Right'
		profiles[2].params[1].minimum=0
		profiles[2].params[1].maximum=1
		profiles[2].params[1].value='0.317'		

	; CDS-SOHO
	
	profiles[3].name='CDS-SOHO'
	profiles[3].numberparams=3
	profiles[3].fortroutine=1
	
		profiles[3].params[0].name='Alpha Left'
		profiles[3].params[0].minimum=0
		profiles[3].params[0].maximum=1
		profiles[3].params[0].value='0.088'
	
		profiles[3].params[1].name='Alpha Right'
		profiles[3].params[1].minimum=0
		profiles[3].params[1].maximum=1
		profiles[3].params[1].value='0.317'		
		
		profiles[3].params[2].name='Sigma'
		profiles[3].params[2].minimum=0
		profiles[3].params[2].maximum=20
		profiles[3].params[2].value='2.0'

	;Natural
	

	
	;Pressure
	

	;Double Gaussian
	

	
	;Triple Gaussian
	

end
