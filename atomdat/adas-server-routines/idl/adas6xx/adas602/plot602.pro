; Copyright (c) 1997 Strathclyde University .
; SCCS info: Module @(#)$Header: /home/adascvs/idl/adas6xx/adas602/plot602.pro,v 1.2 2004/07/06 14:35:08 whitefor Exp $ Date $Date: 2004/07/06 14:35:08 $
;+
; PROJECT:
;       ADAS IBM MVS to DEC UNIX conversion
;
; NAME:
;	PLOT602
;
; PURPOSE:
;	Plot one graph for ADAS602.
;
; EXPLANATION:
;	This routine plots ADAS602 output for a single graph.
;	That includes the data for up to 7 ordinary levels for
;	a given metastable level.
;
; USE:
;	Use is specific to ADAS602. See adas602.pro for example.
;
; INPUTS:
;
;	PROCVAL	- Structure containing settings from processing widget.
;
;	OUTVAL	- Structure containing settingd from the output
;		  selection widget.
;
;	LSTR	- Structure containing processed data.	
;
;	DEVCODE	- A string array of hardcopy device code names used in
;		  the SET_PLOT statement, e.g ['PS', 'PCL', 'HP']
;		  This array must mirror DEVLIST.
;
; OPTIONAL INPUTS:
;	None.
;
; OUTPUTS:
;	None.
;
; OPTIONAL OUTPUTS:
;	None.
;
; KEYWORD PARAMETERS:
;	None.
;
; CALLS:
;	None.
;
; SIDE EFFECTS:
;	None.
;
; CATEGORY:
;	Adas system.	
;
; WRITTEN:
;       David Brooks, University of Strathclyde, 1997
;
; MODIFIED:
;       1.1    	Richard Martin
;              	Changed documentation + put under SCCS control.
;	  1.2		Richard Martin
;			Modified so that line profiles are plotted from
;			structure arrays. This allows support for other line profiles
;
; VERSION:
;	1.1	17-06-97
;	1.2	20-07-01
;
;-
;----------------------------------------------------------------------------

PRO plot602, procval, outval, lstr, devcode

  COMMON Global_lw_data, left, right, tp, bot, grtop, grright

                ;**** set graphics device ****
    if outval.devsel ge 0 then device = devcode(outval.devsel)
    set_plot,device 
    device, filename = outval.hardname
    device, /landscape
     

		;**************************************************** 
		;**** Suitable character size for current device ****
		;**** Aim for 60 characters in y direction.      ****
		;**************************************************** 

    charsize = (!d.y_vsize/!d.y_ch_size)/60.0
    small_check = GETENV('VERY_SMALL')
    if small_check eq 'YES' then charsize=charsize*0.8        
 
                ; **** set graph position ****
    position=[left, bot, grright, grtop]


		;**** Initialise titles ****
    xtitle = 'Pixel Number' 
    ytitle = 'Counts'

 		;**** Construct graph title ****
 		;**** '!C' is the new line control. ****
    title="ARBITRARY COUNTS VS. PIXEL NUMBER"
    if strtrim(procval.gtit,2) ne '' then begin
      title = title + ': ' + strupcase(strtrim(procval.gtit,2))
    end
    title = title + strtrim(procval.title,2)

                ;**** make and annotate plot ****
    plot, lstr.x, lstr.y*lstr.exptime,                                    $
          xrange = [procval.xmin, procval.xmax], /xstyle,                 $
          yrange = [procval.ymin, procval.ymax], /ystyle, psym = 10,      $
          position = position, charsize = charsize, xtitle = xtitle,      $
          ytitle = ytitle

                ;**** over plot fits ****
    oplot, lstr.x, lstr.ya*lstr.exptime

;    yafit = dblarr(min([lstr.nsize,n_elements(lstr.y)]),lstr.numline)
    bfit = dblarr(min([lstr.nsize,n_elements(lstr.y)]))

    for j = 0, min([lstr.nsize,n_elements(lstr.y)])-1 do begin
;      for i = 0, lstr.numline-1 do begin
;        if lstr.getfit(0) eq 0 or lstr.getfit(1) eq 0 then begin
;          dr = abs(lstr.x(j)-lstr.xo(i))/lstr.w(0)
;        end else begin
;          dr = abs(lstr.x(j)-lstr.xo(i))/lstr.w(i)
;        end
;        yafit(j,i) = lstr.h(i)*lstr.exptime*exp(-1.0*dr*dr)
;      endfor
      bfit(j) = lstr.b0*lstr.exptime+lstr.b1*lstr.exptime*lstr.x(j)+$
                lstr.b2*lstr.exptime*lstr.x(j)*lstr.x(j)
    endfor

    oplot,lstr.x,bfit,linestyle=1

    for i = 0, lstr.numline-1 do oplot,lstr.x,lstr.yafit(*,i),linestyle=i+1

		;**** Output title above graphs ****

    if small_check eq 'YES' then begin
        xyouts, left, grtop+0.08, title, /normal, charsize = charsize*0.9
    endif else begin
        xyouts, left, grtop+0.08, title, /normal, charsize = charsize
    endelse

		;**** Reset graphics to screen  ****
    device, /close_file
    set_plot, 'X'

END
