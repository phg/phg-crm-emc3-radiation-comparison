; Copyright (c) 1995, Strathclyde University
; SCCS info: Module @(#)$Header: /home/adascvs/idl/adas6xx/adas602/cw_adas602_in.pro,v 1.1 2004/07/06 12:54:00 whitefor Exp $ Date $Date: 2004/07/06 12:54:00 $
;+
; PROJECT:
;       UNIX IDL ADAS development
;
; NAME:
;	CW_ADAS602_IN
;
; PURPOSE:
;	Data file selection for two input datasets.
;
; EXPLANATION:
;	This function creates a compound widget consisting of two
;       occurences of the compound widget cw_adas4xx_infile.pro,
;	data form selection buttons, a 'Cancel' button, a 'Done' 
;	button and two buttons to allow the browsing of the selected 
;	data file comments. The browsing and done buttons are 
;	automatically de-sensitised until a valid input dataset has 
;	been selected.
;
;	The value of this widget is the settings structure of two
;	cw_adas4xx_infile widgets and the form selector. This widget 
;	only generates events when either the 'Done' or 'Cancel' buttons 
;	are pressed. The event structure returned is;
;	{ID:0L, TOP:0L, HANDLER:0L, ACTION:''}
;	ACTION has one of two values 'Done' or 'Cancel'.
;
; USE:
;	See routine adas_in.pro for an example.
;
; INPUTS:
;	PARENT	- Long integer; the ID of the parent widget.
;
; OPTIONAL INPUTS:
;	None
;
; OUTPUTS:
;	This function returns the ID of the compound widget created.
;
; OPTIONAL OUTPUTS:
;	None
;
; KEYWORD PARAMETERS:
;	VALUE	- A structure which determines the initial settings of
;                 the dataset selection widget cw_adas4xx_infile.  The
;		  structure must be;
;		  {ROOTPATH1:'', FILE1:'', ROOTPATH2:'', FILE2:'',
;                  CENTROOT1:'', USERROOT1:'', CENTROOT2:'', USERROOT2:'',
;                  ADASREL:'', FORTDIR:'', USERROOT:'', CENTROOT:'',
;                  DEVLIST:strarr, DEVCODE:0, FONTL:'', FONTS:'',
;                  EFONT:tfont_struct, EXPSEL:0, DFLAG:) }
;		  The elements of the structure are as follows;
;
;	  ROOTPATH1 - first data directory e.g '/usr/fred/adas/adf04/'
;	  FILE1     - first data file in ROOTPATH1 e.g 'input.dat'
;	  ROOTPATH2 - second data directory e.g '/usr/fred/adas/adf18/'
;	  FILE2     - second data file in ROOTPATH2 e.g 'input.dat'
;	  CENTROOT1 - Default central data store e.g '/usr/adas/adf04/'
;	  USERROOT1 - Default user data store e.g '/usr/fred/adas/adf04/'
;	  CENTROOT2 - Default central data store e.g '/usr/adas/adf18/'
;	  USERROOT2 - Default user data store e.g '/usr/fred/adas/adf18/'
;	  ADASREL, FORTDIR - main ADAS variables
;	  USERROOT  - Default user data store e.g '/usr/fred/adas/'
;	  CENTROOT  - Default central data store e.g '/usr/adas/'
;	  EXPSEL    - 1 => an expansion file has been selected
;		      0 => an expansion file has not been selected
;	  DFLAG	    - 0 => input data in standard form
;		      1 => input data in processed form
;		      2 => input data in other form
;	  OTHERS    - See adas_sys_set
;
;
;		  The first data file selected by the user is obtained by
;		  appending ROOTPATH1 and FILE1, and the second by
;                 appending ROOTPATH2 and FILE2.  In the above example
;		  the full name of both data files is;
;		  /usr/fred/adas/input.dat
;
;		  Path names may be supplied with or without the trailing
;		  '/'.  The underlying routines add this character where
;		  required so that USERROOT will always end in '/' on
;		  output.
;
;		  The default value is;
;		  {ROOTPATH1:'./', FILE1:'', ROOTPATH2:'./', FILE2:'',
;                  CENTROOT:'', USERROOT:'' ... (see below)}
;		  i.e ROOTPATH1 & ROOTPATH2 are set to the user's current
;                 directory.
;
;	TITLE	- The title to be included in the input file widget, used
;                 to indicate exactly what the required input dataset is,
;                 e.g 'Input COPASE Dataset'
;
;	UVALUE	- A user value for the widget.  Defaults to 0.
;
;	FONT	- Supplies the font to be used for the interface widgets.
;		  Defaults to the current system font.
;
; CALLS:
;	XXTEXT	          Pop-up window to browse dataset comments.
;	CW_ADAS602_INFILE Dataset selection widget.
;	FILE_ACC	  Determine filetype and access permissions.
;
;	See side effects for other related widget management routines.
;
; SIDE EFFECTS:
;	IN602_GET_VAL()	Widget management routine in this file.
;	IN602_EVENT()	Widget management routine in this file.
;
; CATEGORY:
;	Compound Widget
;
; WRITTEN:
;       David H.Brooks, Univ.of Strathclyde, 20-Nov-1995
;       Altered cw_adas_in.pro to allow double file selection.
;
; MODIFIED:
;	1.0	David H.Brooks
;		First version - written using cw_adas208.pro
;	1.1	Richard Martin
;		Changed Documentation + put under SCCS control
;
; VERSION:
;       1.0	?
;	1.1	16-06-96
;
;-
;-----------------------------------------------------------------------------

FUNCTION in602_get_val, id

		;***********************************
                ;**** Return to caller on error ****
		;***********************************
    ON_ERROR, 2

		;***********************************************
                ;**** Retrieve the structure from the child ****
		;***********************************************

    first_child = widget_info(id, /child)
    widget_control, first_child, get_uvalue=state, /no_copy
  
		;**********************
		;**** Get settings ****
		;**********************

    widget_control, state.file1id, get_value=inset1
    widget_control, state.file2id, get_value=inset2
    widget_control, state.file2id, get_uvalue=expsel
 
		;***************************************
                ;**** Copy values to main structure ****
		;***************************************

    inset = {	in602_set,                                              $ 
              	ROOTPATH1	:	inset1.rootpath, 		$
		FILE1   	:	inset1.file,      		$
              	ROOTPATH2	:	inset2.rootpath, 		$
		FILE2   	:	inset2.file,      		$
	      	CENTROOT1 	:	inset1.centroot, 		$
		USERROOT1	:	inset1.userroot,  		$
	      	CENTROOT2 	:	inset2.centroot, 		$
		USERROOT2	:	inset2.userroot,   		$
                ADASREL         :       state.inset.adasrel,            $
                FORTDIR         :       state.inset.fortdir,            $
                USERROOT        :       state.inset.userroot,           $
                CENTROOT        :       state.inset.centroot,           $
                DEVLIST         :       state.inset.devlist,            $
                DEVCODE         :       state.inset.devcode,            $
                FONTL           :       state.inset.fontl,              $
                FONTS           :       state.inset.fonts,              $
                EFONT           :       state.inset.efont,		$
                EXPSEL          :       expsel,                  	$
		DFLAG 		:	state.inset.dflag}

		;***************************
                ;**** restore the state ****
		;***************************

    widget_control, first_child, set_uvalue=state, /no_copy

		;*******************************
                ;**** return the value here ****
		;*******************************
  
   RETURN, inset

END

;-----------------------------------------------------------------------------

FUNCTION in602_event, event

		;************************************
                ;**** Base ID of compound widget ****
		;************************************

    parent=event.handler

		;***********************************************
                ;**** Retrieve the structure from the child ****
		;***********************************************

    first_child = widget_info(parent, /child)
    widget_control, first_child, get_uvalue=state, /no_copy

		;*********************************
		;**** Clear previous messages ****
		;*********************************

    widget_control, state.messid, set_value=''

		;*********************************
		;**** Default output no event ****
		;*********************************

    new_event = 0L

                ;************************
                ;**** Process events ****
                ;************************

    CASE event.id OF

		;***********************************
		;**** Event from file selection ****
		;***********************************

        state.file1id: begin
	    if event.action eq 'newfile' then begin
                widget_control, state.file1id, set_uvalue = 1
                widget_control, state.file2id, get_uvalue = picked
	        widget_control, state.browse1id, /sensitive
                  widget_control, state.doneid, /sensitive
	    endif else begin
                widget_control, state.file1id, set_uvalue = 0
	        widget_control, state.browse1id, sensitive=0
                widget_control, state.doneid, sensitive = 0
	    endelse
	end

		;***********************
		;**** Browse button ****
		;***********************

    	state.browse1id: begin

		;*****************************
		;**** Get latest filename ****
		;*****************************

	    widget_control, state.file1id, get_value=inset1
	    filename1 = inset1.rootpath + inset1.file

		;*************************************************
		;**** Invoke comments browsing with data type ****
		;*************************************************

            xxtext602, filename1, state.inset.dflag, font=state.font
	end
		;*******************************
		;**** Second file selection ****
		;*******************************

    	state.file2id: begin
	    if event.action eq 'newfile' then begin
                widget_control, state.file2id, set_uvalue = 1
                widget_control, state.file1id, get_uvalue = picked
	        widget_control, state.browse2id, /sensitive
                if picked eq 1 then begin
                  widget_control, state.doneid, /sensitive
                end
	    endif else begin
	        widget_control, state.file2id, set_uvalue = 0
	        widget_control, state.browse2id, sensitive=0
	    endelse
	end

		;***********************
		;**** Browse button ****
		;***********************

    	state.browse2id: begin

		;*****************************
		;**** Get latest filename ****
		;*****************************

	    widget_control, state.file2id, get_value=inset2
	    filename2 = inset2.rootpath + inset2.file

		;*************************************************
		;**** Invoke comments browsing with data type ****
		;*************************************************

            dflag=1
	    xxtext602, filename2, dflag, font=state.font
	end

		;*************************
		;**** Standard button ****
		;*************************

    	state.standid: begin
            state.inset.dflag=0
        end
              
		;*******************************
		;**** Processed data button ****
		;*******************************

    	state.procdid: begin
            state.inset.dflag=1
        end

		;***************************
		;**** Other data button ****
		;***************************

    	state.otherid: begin
            state.inset.dflag=2
        end

		;***********************
		;**** Cancel button ****
		;***********************

    	state.cancelid: new_event = {ID:parent, TOP:event.top, 		$
				     HANDLER:0L, ACTION:'Cancel'}

		;*********************
		;**** Done button ****
		;*********************

    	state.doneid: begin
                ;**** checkdata of right type ****
	    widget_control, state.file1id, get_value=inset1
	    filename1 = inset1.rootpath + inset1.file
	    widget_control, state.file2id, get_value=inset2
	    filename2 = inset2.rootpath + inset2.file
            tst1=''
            openr, unit, /get_lun, strtrim(filename1,2)
              readf, unit, tst1
            free_lun, unit
            tst2=''
            if strtrim(inset2.file,2) eq '' then begin
              tst2='SR'
            end else begin
              openr, unit, /get_lun, strtrim(filename2,2)
                readf, unit, tst2
              free_lun, unit
            end
            if tst1 ne 'SR' or tst2 ne 'SR' then begin
              action=popup(message='FILES MUST BE OF IDL S/R TYPE',     $
                     buttons=[' OK '], font=state.font)
              new_event = 0L
            end else begin
                new_event = {ID:parent, TOP:event.top, 			$
                             HANDLER:0L, ACTION:'Done'}
            end
        end
    	ELSE:			;**** Do nothing ****

    ENDCASE

		;***************************
                ;**** restore the state ****
		;***************************

    widget_control, first_child, set_uvalue=state, /no_copy

    RETURN, new_event

END

;-----------------------------------------------------------------------------

FUNCTION cw_adas602_in, parent, VALUE=value, TITLE=title, 		$
                        UVALUE=uvalue, FONT=font


    IF (N_PARAMS() EQ 0) THEN MESSAGE, 'Must specify parent for cw_adas_in'
    ON_ERROR, 2					;return to caller

		;***********************************
		;**** Set defaults for keywords ****
		;***********************************

    IF NOT (KEYWORD_SET(value)) THEN begin
        sarr = make_array(3,/string,value='')
        tsr = {tsr_set, font_norm:'helvetica-bold14',font_input:'helvetica-oblique14'}
        inset = {	in602_set,                                      $ 
                	ROOTPATH1	:	'./', 			$
			FILE1		:	'', 			$
			ROOTPATH2	:	'./', 			$
			FILE2		:	'', 			$
			CENTROOT1	:	'', 			$
			USERROOT1	:	'',			$
			CENTROOT2	:	'', 			$
			USERROOT2	:	'',			$
                        ADASREL         :       '',                     $
                        FORTDIR         :       '',                     $
                        USERROOT        :       '',                     $
                        CENTROOT        :       '',                     $
                        DEVLIST         :       sarr,                   $
                        DEVCODE         :       sarr,                   $
                        FONTL           :       'courier_bold14',       $
                        FONTS           :       'courier_bold12',       $
                        EFONT           :       tsr,			$
			EXPSEL		:	0,                      $
			DFLAG           :	0 }
    ENDIF ELSE BEGIN
        inset = {       in602_set,                                      $ 
                	ROOTPATH1	:	value.rootpath1, 	$
			FILE1		:	value.file1, 		$
                	ROOTPATH2	:	value.rootpath2, 	$
			FILE2		:	value.file2, 		$
			CENTROOT1	:	value.centroot1, 	$
			USERROOT1	:	value.userroot1,	$
			CENTROOT2	:	value.centroot2, 	$
			USERROOT2	:	value.userroot2,	$
                        ADASREL         :       value.adasrel,          $
                        FORTDIR         :       value.fortdir,          $
                        USERROOT        :       value.userroot,         $
                        CENTROOT        :       value.centroot,         $
                        DEVLIST         :       value.devlist,          $
                        DEVCODE         :       value.devcode,          $
                        FONTL           :       value.fontl,            $
                        FONTS           :       value.fonts,            $
                        EFONT           :       value.efont,		$
			EXPSEL		:	value.expsel,           $
			DFLAG 		:	value.dflag }

        if strtrim(inset.rootpath1) eq '' then begin
            inset.rootpath1 = './'
        endif else if 							$
	strmid(inset.rootpath1, strlen(inset.rootpath1)-1, 1) ne '/' 	$
        then begin
            inset.rootpath1 = inset.rootpath1 + '/'
        endif
        if strmid(inset.file1, 0, 1) eq '/' then begin
            inset.file1 = strmid(inset.file1, 1, strlen(inset.file1)-1)
        endif
        if strtrim(inset.rootpath2) eq '' then begin
          inset.rootpath2 = './'
        endif else if 							$
        strmid(inset.rootpath2, strlen(inset.rootpath2)-1, 1) ne '/' 	$
	then begin
            inset.rootpath2 = inset.rootpath2 + '/'
        endif
        if strmid(inset.file2, 0, 1) eq '/' then begin
            inset.file2 = strmid(inset.file2, 1, strlen(inset.file2)-1)
        endif
    END
    IF NOT (KEYWORD_SET(title)) THEN title = ''
    IF NOT (KEYWORD_SET(uvalue)) THEN uvalue = 0
    IF NOT (KEYWORD_SET(font)) THEN font = ''

		;*********************************
		;**** Create the Input widget ****
		;*********************************

		;****************************
		;**** create base widget ****
		;****************************

    topbase = widget_base( parent, UVALUE = uvalue,         		$
			   EVENT_FUNC = "in602_event",       		$
		       	   FUNC_GET_VALUE = "in602_get_val", 		$
			   /COLUMN)

                ;******************************************************
                ;**** Create a dummy widget just to hold value of *****
                ;**** "state" variable so as not to get confused  *****
                ;**** with any other values. Adopt IDL practice   *****
                ;**** of using first child widget                 *****
                ;******************************************************

    first_child = widget_base(topbase)
    cwid = widget_base(first_child, /column)

		;*******************************************
		;**** Data type selection buttons       ****
		;*******************************************
   
    butbase = widget_base(cwid, /frame, /row)
    label_col = widget_base(butbase, /column) 
    data_label = widget_label(label_col, value='Select Data Form : ', $
                              font=font)

    but_col = widget_base(butbase, /column)
    but_row = widget_base(but_col, /row, /exclusive)
    standid = widget_button(but_row, value='Standard', font=font,     $
                                     /no_release)
    procdid = widget_button(but_row, value='Processed', font=font,    $
                                     /no_release)
    otherid = widget_button(but_row, value='Other', font=font,        $
                                     /no_release)
                
		;*******************************************
		;**** Set Data Form as selected         ****
		;*******************************************

    if inset.dflag eq 0 then begin
       widget_control, standid, set_button = 1
       widget_control, procdid, set_button = 0
       widget_control, otherid, set_button = 0
    end else if inset.dflag eq 1 then begin
       widget_control, standid, set_button = 0
       widget_control, procdid, set_button = 1
       widget_control, otherid, set_button = 0
    end else if inset.dflag eq 2 then begin
       widget_control, standid, set_button = 0
       widget_control, procdid, set_button = 0
       widget_control, otherid, set_button = 1
    end

		;*******************************************
		;**** Set up first file structure       ****
		;*******************************************

    inset1 = {   ROOTPATH:inset.rootpath1, FILE:inset.file1, 		$
                 CENTROOT:inset.centroot1, USERROOT:inset.userroot1 	}

		;*******************************************
		;****  First file selection widget      ****
		;*******************************************

    filebase1 = widget_base(cwid, /frame, /column)
    file1id = cw_adas602_infile(filebase1, value=inset1, title=title,	$
                                font=font, uvalue = 0)

		;*****************
		;**** Buttons ****
		;*****************

    base1 = widget_base(filebase1, /row)

		;*******************************
		;**** Browse Dataset button ****
		;*******************************

    browse1id = widget_button(base1, value='Browse Comments', font=font)

		;*******************************************
		;**** Set up second file structure      ****
		;*******************************************

    inset2 = {   ROOTPATH:inset.rootpath2, FILE:inset.file2, 		$
                 CENTROOT:inset.centroot2, USERROOT:inset.userroot2 	}

		;********************************************
		;**** Second file selection widget       ****
		;********************************************
	
    title2 = 'Select PROCESSED Template'
    filebase2 = widget_base(cwid, /frame, /column)
    file2id = cw_adas602_infile(filebase2, value=inset2, title=title2,  $
                                font=font, uvalue = 0)

		;*****************
		;**** Buttons ****
		;*****************

    base2 = widget_base(filebase2,/row)

		;*******************************
		;**** Browse Dataset button ****
		;*******************************

    browse2id = widget_button(base2, value='Browse Comments', font=font)


		;***********************
		;**** Cancel Button ****
		;***********************

    base = widget_base(cwid, /row)

    cancelid = widget_button(base, value='Cancel', font=font)
  
		;*********************
		;**** Done Button ****
		;*********************

    doneid = widget_button(base, value='Done', font=font)

		;***********************
		;**** Error message ****
		;***********************

    messid = widget_label(parent, font=font, value='*')

		;********************************************************
		;**** Check default filenames and desensitise buttons****
		;**** if they are directories or have no read access ****
		;********************************************************

    filename1 = inset1.rootpath + inset1.file
    file_acc, filename1, fileexist, read, write, execute, filetype
    if filetype ne '-' then begin
        widget_control, browse1id, sensitive=0
        widget_control, doneid, sensitive=0
        widget_control, file1id, set_uvalue = 0
    endif else begin
        if read eq 0 then begin
            widget_control, browse1id, sensitive=0
            widget_control, doneid, sensitive=0
            widget_control, file1id, set_uvalue = 0
        end else begin
            widget_control, file1id, set_uvalue = 1
        end
    endelse

    filename2 = inset2.rootpath + inset2.file
    file_acc, filename2, fileexist, read, write, execute, filetype
    if filetype ne '-' then begin
        widget_control, browse2id, sensitive=0
        widget_control, file2id, set_uvalue = 0
    endif else begin
        if read eq 0 then begin
            widget_control, browse2id, sensitive=0
            widget_control, file2id, set_uvalue = 0
        endif else begin
	    widget_control, file2id, set_uvalue = 1
	endelse
    endelse


		;*************************************************
		;**** create a state structure for the widget ****
		;*************************************************

     new_state = { 	FILE1ID         : file1id         ,		$
                 	BROWSE1ID       : browse1id       ,		$
                 	FILE2ID         : file2id         ,		$
                 	BROWSE2ID       : browse2id       ,		$
	 	 	CANCELID        : cancelid        ,		$
                 	DONEID          : doneid          ,		$
                 	MESSID          : messid          ,		$
		 	FONT            : font            ,		$
                 	INSET           : inset           ,             $
                        STANDID         : standid         ,             $
                        PROCDID         : procdid         ,             $
                        OTHERID         : otherid         }

		;**************************************
                ;**** Save initial state structure ****
		;**************************************

    widget_control, first_child,  set_uvalue=new_state, /no_copy

    RETURN, cwid

END

