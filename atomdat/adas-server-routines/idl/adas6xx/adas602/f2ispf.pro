; Copyright (c) 1997 Strathclyde University .
; SCCS info: Module @(#)$Header: /home/adascvs/idl/adas6xx/adas602/f2ispf.pro,v 1.3 2004/07/06 13:51:15 whitefor Exp $ Date $Date: 2004/07/06 13:51:15 $
;+
; PROJECT:
;       ADAS IBM MVS to DEC UNIX conversion
;
; NAME:
;	F2ISPF
;
; PURPOSE:
;	Top IDL user interface routine for ADAS602 processing options.
;
; EXPLANATION:
;	The routine begins by creating the ADAS602 graph title
;	Then part of the ADAS602 IDL user interface is invoked to determine 
;	how the user wishes to process the input dataset.  
;
; USE:
;	The use of this routine is specific to ADAS602, see adas602.pro.
;
; INPUTS:
;
;	LPEND	- Integer, 0 if the user pressed the 'Done' button
;		  or 1 if the user pressed 'Cancel'.  Maps directly onto
;		  the logical variable LPEND in ADAS602 FORTRAN.
;
;	VALUE	- A structure which determines the initial settings of
;		  the processing options widget.  The initial value is
;		  set in adas602.pro.  If adas602.pro passes a blank 
;		  dummy structure of the form {NMET:-1} into VALUE then
;		  VALUE is reset to a default structure.
;
;		  The VALUE structure is;
;
;		     {	NEW:0,              NLINES:0,         $
;                	EXPFLAG:0,          EXPVAL:0.0,       $
;                 WAITF:0, 		  WAITVAL:0.0,      $
;                	GRPSCAL:0,          GRPRMESS:'',      $
;	  	 	XMIN:0.0,	    XMAX:0.0,	      $
;		 	YMIN:0.0,	    YMAX:0.0,	      $
;		 	GTIT:'',	    GTITFLAG:0,       $
;		 	FIXFLAG:0,	    LIDFLAG:0,        $
;		 	FIXPOS:dblarr(10), LINEID:strarr(10), $
;		 	VWFLAG:1,	    FWFLAG:1,	      $
;		 	FIXWID:0.0,	    BFLAG:0,	      $
;		 	BFLAT:0,	    BSLOPE:2,	      $
;		 	BCURVE:2,	    WTFLAG:1,	      $
;		 	AUTOLOOP:0,	    TITLE:'',	      $
;		 	FITFLAG:0}
;
;		  See cw_adas602_proc.pro for a full description of this
;		  structure.
;
;	ADASHEADER- Full header for output (release:prog:date:time)
;
;	HEADER1 - Header of the input data
;
;	HEADER2 - Header of the processed template.
;
;	SPECTRUM- Array containing input data.
;
;	PROC_LSTR-Structure containing processed data.
;
;       BITFILE - String; the path to the dirctory containing bitmaps
;                 for the 'escape to series menu' button.
;
;	INVAL   - Structure; the input screen settings. See cw_adas602_in.pro
;
;	OLDSTR  - A structure derived from a processed template also
;		  containing previously used fitting parameters.	
;
;	DSNINC	- String; The full system file name of the input 
;		  dataset selected by the user for processing.
;
;	DSNINX	- String; The full system file name of the input processed
;		  template.
;
; OPTIONAL INPUTS:
;	None
;
; OUTPUTS:
;	VALUE	- On output the structure records the final settings of
;		  the processing selections widget if the user pressed the
;		  'Done' button, otherwise it is not changed from input.
;
;       GOMENU  - Int; flag - set to 1 if user has selected 'escape direct
;                 to series menu , otherwise 0
;
; OPTIONAL OUTPUTS:
;	None
;
; KEYWORD PARAMETERS:
;	FONT_LARGE - The name of a larger font.
;
;	FONT_SMALL - The name of a smaller font.
;
;	EDIT_FONTS - A structure of two fonts used in the table editor
;		     adas_edtab.pro, {font_norm:'',font_input:''}.
;
; CALLS:
;	ADAS602_PROC	Invoke the IDL interface for ADAS602 data
;		 	processing options/input.
;
; CATEGORY:
;	Adas system.
;
;
; MODIFIED:
;       1.0	David Brooks
;               First release.
;
;       1.1     Richard Martin
;               Changed documentation + put under SCCS control
;	  1.2	    Richard Martin
;		    Added WAITF, WAITVAL to value structure 
;	  1.3	    Richard Martin
;		    Added support for NIS line profiles
;
; VERSION:
;       1.0	?
;	1.1 	16-06-97
;	1.2	26-06-98
;	1.3	20-07-01
;
;-
;-----------------------------------------------------------------------------


PRO f2ispf, lpend, value, header1, header2,        $
		adasheader, spectrum, proc_lstr,   $
		gomenu, bitfile, inval, oldstr,    $
                dsninc, dsninx,           $
		FONT_LARGE=font_large, FONT_SMALL=font_small, $
		EDIT_FONTS=edit_fonts


                ;**** Set defaults for keywords ****
  IF NOT (KEYWORD_SET(font_large)) THEN font_large = ''
  IF NOT (KEYWORD_SET(font_small)) THEN font_small = ''
  IF NOT (KEYWORD_SET(edit_fonts)) THEN $
                                edit_fonts = {font_norm:'',font_input:''}

		;*******************************************
		;**** Set default value if non provided ****
		;*******************************************
  if value.new lt 0 then begin
    maxparams=0
    adas602_profiles, profiles, maxparams
    value = {	NEW:0,             NLINES:0,         $
                EXPFLAG:0,         EXPVAL:0.0,       $
                WAITF:0, 	     WAITVAL:0.0,      $
                GRPSCAL:0,         GRPRMESS:'',      $
                XMIN:0.0,          XMAX:0.0,         $
                YMIN:0.0,          YMAX:0.0,         $
                GTIT:'',           GTITFLAG:0,       $
                FIXFLAG:0,         LIDFLAG:0,        $
                FIXPOS:dblarr(10), LINEID:strarr(10),$
                VWFLAG:1,          FWFLAG:1,         $
                FIXWID:0.0,        BFLAG:0,          $
                BFLAT:0,           BSLOPE:2,         $
                BCURVE:2,          WTFLAG:1,         $
                AUTOLOOP:0,        TITLE:'',         $
                FITFLAG:0,	     PROFILES:profiles,$
		    PINDEX:0	 }
  end

  if lpend eq 1 or value.new ge 0 then value.title=''
  lpend = 0 

		;*********************************
		;**** Create ADAS Graph title ****
		;*********************************
  small_check = GETENV('VERY_SMALL')
  if small_check eq 'YES' then begin
    value.title =  value.title + '!CADAS    : ' + strupcase(adasheader)
    if inval.dflag eq 0 then begin
      type = '  : STANDARD DATA '
    end else if inval.dflag eq 1 then begin
      type = '  : PROCESSED DATA '
    end else if inval.dflag eq 2 then begin
      type = '  : OTHER DATA '
    end 
    value.title =  value.title + '!C!CFILE    : ' + strcompress(dsninc) + type
    if strtrim(dsninx,2) ne '' then begin
      value.title =  value.title + '!C!CTEMPLATE: ' + strcompress(dsninx) 
    end
    value.title =  value.title + $
    '!C!CKEY    : (HISTOGRAM - ORIGINAL DATA) (FULL LINE - FIT) (DOTTED - COMPONENTS)'
  endif else begin
    value.title =  value.title + '!CADAS    : ' + strupcase(adasheader)
    if inval.dflag eq 0 then begin
      type = '  : STANDARD DATA '
    end else if inval.dflag eq 1 then begin
      type = '  : PROCESSED DATA '
    end else if inval.dflag eq 2 then begin
      type = '  : OTHER DATA '
    end 
    value.title =  value.title + '!CFILE    : ' + strcompress(dsninc) + type
    if strtrim(dsninx,2) ne '' then begin
      value.title =  value.title + '!CTEMPLATE: ' + strcompress(dsninx) 
    end
    value.title =  value.title + $
    '!CKEY    : (HISTOGRAM - ORIGINAL DATA) (FULL LINE - FIT) (DOTTED - COMPONENTS)'
  endelse


		;****************************************
		;**** Pop-up processing input widget ****
		;****************************************
  adas602_proc, value, action, bitfile, header1, header2,     $
                spectrum, proc_lstr, inval, oldstr,           $
                FONT_LARGE=font_large,                        $
		FONT_SMALL=font_small, EDIT_FONTS=edit_fonts


		;******************************************
		;**** Act on the event from the widget ****
		;******************************************
		;**** There are three    possible events ****
		;**** 'Done', 'Menu' and 'Cancel'.       ****
		;********************************************

  gomenu = 0
  if action eq 'Done' then begin
    lpend = 0
  endif else if action eq 'Menu' then begin
    lpend = 0
    gomenu = 1
  endif else begin
    lpend = 1
  end


END
