; Copyright (c) 1997 Strathclyde University .
; SCCS info: Module @(#)$Header: /home/adascvs/idl/adas6xx/adas602/xxtext602.pro,v 1.1 2004/07/06 15:40:20 whitefor Exp $ Date $Date: 2004/07/06 15:40:20 $
;
;+
; PROJECT:
;       ADAS IBM MVS to DEC UNIX conversion
;
; NAME:
;	XXTEXT
;
; PURPOSE:
;	A popup window for browsing ADAS dataset comments.
;
; EXPLANATION:
;	This routine pops up a window which uses a scrollable text widget to
;	display the comment records from a text file.  Comment records
;	all begin with 'C' in the first column.  A 'Done' button is
;	also included in the widget for the user to complete the browsing.
;	This version is based on XXTEXT.PRO and displays comments in different
;	forms determined by DFLAG.
;
; USE:
;	An example of how to use this routine;
;
;		datafile = '/disk2/adas/file.dat'
;		xxtext, datafile, dflag, xsize=132, ysize=10
;
; INPUTS:
;       DATAFILE - String; Data file name, e.g '/disk2/bowen/adas/file.dat'
;		   Note: It is the responsibility of the caller to
;		   ensure that the file name is valid and readable.
;
; OPTIONAL INPUTS:
;	None.
;
; OUTPUTS:
;       None
;
; OPTIONAL OUTPUTS:
;       None
;
; KEYWORD PARAMETERS:
;	XSIZE	- Integer; x size in characters of browse widget.
;		  Default is 80.
;
;	YSIZE	- Integer; y size in characters of browse widget.
;		  Default is 30.
;
;	FONT	- A font to use for all text in this widget.
;
; CALLS:
;
;	See side effects for other related widget management routines.
;
; SIDE EFFECTS:
;
;	One other routine is included which is used to manage the widget;
;
;	XXTEXT602_EVENT
;
; CATEGORY:
;	Widgets
;
; WRITTEN:
;       David Brooks, University of Strathclyde, 1997
;
; MODIFIED:
;	1.0	David Brooks
;		Based on xxtext.pro v1.6
;       1.1     Richard Martin
;               Changed documentation + put under SCCS control
;
; VERSION:
;       1.0    	?
;	1.1	18-06-97
;-
;-----------------------------------------------------------------------------

PRO xxtext602_event, event

		;**** Destroy the widget ****
    widget_control,event.top,/destroy

END

;-----------------------------------------------------------------------------


PRO xxtext602, datafile, dflag, XSIZE = xsize, YSIZE = ysize, FONT = font



		;**** Set defaults for keywords ****
  IF NOT (KEYWORD_SET(xsize)) THEN xsize = 80
  IF NOT (KEYWORD_SET(ysize)) THEN ysize = 30
  IF NOT (KEYWORD_SET(font)) THEN font = ''


		;**** create titled base widget ****
  parent = widget_base(title='Data File Comments', $
			xoffset=100,yoffset=100,/column)

		;**** Title ****
  rc = widget_label(parent,value=datafile,font=font)
  comments=0
  record=''
  tst=''

  if dflag eq 0 then begin
    openr, unit, /get_lun, strtrim(datafile,2)
      readf, unit, tst
    free_lun, unit
    if tst ne 'SR' then begin
    action=popup(message='CHOSEN FILE NOT STANDARD TYPE - MUST BE IDL S/R',$
                   buttons=[' OK '], font=font)
    end else begin
      restore,strtrim(datafile,2)
      comstr='**** NO COMMENTS FOUND ****'
      if n_elements(header) ne 0 then begin
        dtype = size(header)
        if dtype(dtype(0)+1) eq 8 then begin
          tmp=tag_names(header(0))
          if n_elements(header) eq 1 then begin
              for i = 0, n_elements(tmp)-1 do begin
                 z = execute("reads,header(0)."+tmp(i)+",record")
                 if z ne 1 then begin
                    STOP,'EXECUTE STATEMENT FAILED IN XXTEXT602.PRO'
                 end
                 record = 'C  '+strtrim(tmp(i),2)+'   :   '+strtrim(record,2)
                 comments = comments+1
                 comstr=[comstr,record]
              end
          end else begin
  		;**** Create Info. widget ****
              widget_control, /hourglass
              widgetbase = widget_base(/column, xoffset=300, yoffset=200, $
                         title = "ADAS602 : INFORMATION")
              lab0 = widget_label(widgetbase, value='')
              lab1 = widget_label(widgetbase, value='')
              lab2 = widget_label(widgetbase, font=font,              $
              value="      Reading First 3 Structure Headers - please wait      ")
              lab3 = widget_label(widgetbase, value='')
              lab4 = widget_label(widgetbase, value='')
              widget_control, widgetbase, /realize
;              for j = 0, n_elements(header)-1 do begin
              for j = 0, 2 do begin
                for i = 0, n_elements(tmp)-1 do begin
                   z = execute("reads,header(j)."+tmp(i)+",record")
                   if z ne 1 then begin
                      STOP,'EXECUTE STATEMENT FAILED IN XXTEXT602.PRO'
                   end
                   record = 'C  '+strtrim(tmp(i),2)+'   :   '+strtrim(record,2)
                   comments = comments+1
                   comstr=[comstr,record]
                end
              end
              widget_control, widgetbase, /destroy
          end
        end else begin  
          comments = 1
          comstr=[comstr,header]
        end
      end
    end 
  end else if dflag eq 1 then begin
    openr, unit, /get_lun, strtrim(datafile,2)
      readf, unit, tst
    free_lun, unit
    if tst ne 'SR' then begin
  action=popup(message='CHOSEN FILE NOT PROCESSED TYPE - MUST BE IDL S/R',$
               buttons=[' OK '], font=font)
    end else begin
      restore,strtrim(datafile,2)
      header=lstr(0).header
      comstr='**** NO COMMENTS FOUND ****'
      if n_elements(header) ne 0 then begin
        comments = comments + 1
        comstr=[comstr,header]
      end
    end
  end
;**** Other TYPE ****
;**** note to DB need a check as to whether file is IDL/ascii
;**** assumed IDL here - below part for ascii
;;;;		;**** Read comments into text widget ****
;;;;  openr, unit, datafile, /get_lun
;;;;  comstr='**** NO COMMENTS FOUND ****'
;;;;  while (not eof(unit)) do begin
;;;;    readf, unit, record
;;;;    if strmid(record,0,1) eq 'C' then begin
;;;;      comments = comments + 1
;;;;      comstr=[comstr,record]
;;;;    end
;;;;  end

;;;;  close, unit
;;;;  free_lun, unit
 if tst eq 'SR' then begin

		;**** Remove 'no comments' message ****
  if comments gt 0 then comstr=comstr(1:comments)

		;**** Text widget to display comments ****
  textid = widget_text(parent,value=comstr,/scroll,xsize=xsize,		$
			ysize=ysize,font=font)

		;**** done button ****
  rc = widget_button(parent,value='Done',font=font)
  
		;**** realize the widget ****
  widget_control,parent,/realize

		;**** make widget modal ****
  xmanager,'xxtext602',parent,event_handler='xxtext602_event',/modal,/just_reg

 end 

END

