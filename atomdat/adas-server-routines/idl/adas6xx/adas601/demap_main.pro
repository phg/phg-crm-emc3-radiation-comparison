; Copyright (c) 1996, Strathclyde University.
; Alessandro Lanzafame  acl@phys.strath.ac.uk    ?? March 1996
; SCCS info:
;+
; PROJECT:
;       ADAS support programs
;
; NAME:
;   DEMAP_MAIN
;
; PURPOSE:
;   The highest level routine for the
;   Differential Emission Measure Analysis Package.
;
; EXPLANATION:
;   This routine is called from demap.pro,
;   to start the DEM application.  Associated with demap_main.pro
;   is a FORTRAN executable.
;
;       Most of the precessing, input and output is done by the IDL
;       code. Only the integral inversion remains in the FORTRAN
;       counterpart.
;
;       The IDL code communicates with the FORTRAN
;   executable via a bi-directional UNIX pipe.  The unit number
;   used by the IDL for writing to and reading from this pipe is
;   allocated when the FORTRAN executable is 'spawned' (see code
;   below).  Pipe communications in the FORTRAN process are to
;   stdin and stdout, i.e streams 5 and 6.
;
;   The FORTRAN code is an independent process under the UNIX system.
;   The IDL process can only exert control over the FORTRAN in the
;   data which it communicates via the pipe.  The communications
;   between the IDL and FORTRAN must be exactly matched to avoid
;   input conversion errors.  The correct ammounts of data must be
;   passed so that neither process 'hangs' waiting for communications
;   which will never occur.
;
;   The FORTRAN code performs some error checking which is
;   independent of IDL.  In cases of error the FORTRAN may write
;   error messages.  To prevent these error messages from conflicting
;   with the pipe communications all FORTRAN errors are written to
;   output stream 0, which is stderr for UNIX.  These error messages
;   will appear in the window from which the ADAS session/IDL session
;   is being run.
;
;   In the case of severe errors the FORTRAN code may terminate
;   itself prematurely.  In order to detect this, and prevent the
;   IDL program from 'hanging' or crashing, the IDL checks to see
;   if the FORTRAN executable is still an active process before
;   each group of pipe communications.  The process identifier
;   for the FORTRAN process, PID, is recorded when the process is
;   first 'spawned'.  The system is then checked for the presence
;   of the FORTRAN PID.
;
; USE:
;   First the system settings must be established by calling
;   demap_sys_set.pro then dem.pro is called to start the
;   DEM application;
;
;   demap_sys_set, adasrel, fortdir, userroot, centroot, $
;           devlist, devcode, font_large, font_small, edit_fonts
;   demap,   adasrel, fortdir, userroot, centroot, devlist, $
;        devcode, font_large, font_small, edit_fonts
;
; INPUTS:
;   ADASREL - A string indicating the ADAS system version,
;       e.g ' ADAS SUPPORT: ADAS95 V1.0'.  The first
;       character should be a space.
;
;   FORTDIR - A string holding the path to the directory where the
;       FORTRAN executables are, e.g '~/demap/bin'
;
;   USERROOT - A string holding the path to the root directory of
;        the user's data. e.g '~/adas'
;        This root directory will be used by adas to construct
;        other path names.  For example the users default data
;        for adas201 should be in ~/adas/adf04.  In
;        particular the user's default interface settings will
;        be stored in the directory USERROOT+'/defaults'.  An
;        error will occur if the defaults directory does not
;        exist.
;
;   CENTROOT - Like USERROOT, but this directory points to the
;        central data area for the system.  User defaults are
;        not stored on CENTROOT.
;
;   DEVLIST - A string array of hardcopy device names, used for
;       graphical output. e.g ['Post-Script','HP-PCL','HP-GL']
;       This array must mirror DEVCODE.  DEVCODE holds the
;       actual device names used in a SET_PLOT statement.
;
;   DEVCODE - A string array of hardcopy device code names used in
;       the SET_PLOT statement, e.g ['PS', 'PCL', 'HP']
;       This array must mirror DEVLIST.
;
;   FONT_LARGE - The name of a larger font e.g 'courier_bold14'
;
;   FONT_SMALL - The name of a smaller font e.g 'courier_bold12'
;
;   EDIT_FONTS - A structure of two fonts used in the table editor
;          adas_edtab.pro, {font_norm:'',font_input:''}.
;          The two fonts are used to differentiate between
;          editable and non-editable parts of the table. You
;          may have to experiment to get two fonts which give
;          the same default spacing in the edit widget.
;          e.g {font_norm:'helvetica_bold14', $
;        font_input:'helvetica_oblique14'}
;
; OPTIONAL INPUTS:
;   None
;
; OUTPUTS:
;   None - Note this routine should leave all inputs un-modified.
;
; OPTIONAL OUTPUTS:
;   None
;
; KEYWORD PARAMETERS:
;   None
;
; CALLS:
;   FIND_PROCESS  Checks to see if a given process is active.
;
;
; SIDE EFFECTS:
;   This routine spawns a FORTRAN executable.  Note the pipe
;   communications routines listed above.  In addition to these
;   pipe communications there is one explicit communication of the
;   date to the FORTRAN code, search for 'printf,pipe' to find it.
;
; CATEGORY:
;   Adas support and applications.
;
; WRITTEN:
;       Alessandro Lanzafame, University of Strathclyde, aug22-95
;
; MODIFIED:
;   1.0   Alessandro Lanzafame
;     FIrst version
;   1.1   Tim Hammond
;     Put under S.C.C.S. control
;   1.2   William Osborn
;     Removed spaces from before @... macros
;   1.3   William Osborn
;     Minor corrections
;   1.4   William Osborn
;     Added header + changed window title + added SIGN to outval
;   1.5   William Osborn
;     Removed common statements before procedure mbar_event
;   1.6   Alessandro Lanzafame / William Osborn
;     Added intensity prediction for when run_dem is selected
;   1.7   William Osborn
;     Minor syntax error + definition of H was put it
;   1.8   Tim Hammond
;     Added extra error checking when running run_dem.pro
;     Changed default value of sigma to 0.4e-1 from 0.4e-3
;   1.9    William Osborn
;     Increased version number to 1.3
;   1.10  Alessandro Lanzafame
;     Added prior function in predict_intensity
;   1.11  Richard Martin
;     Increased version no to 1.4
;   1.12  Richard Martin
;     Increased version number to 1.5
;   1.13   Alessandro Lanzafame
;               Modified call to kernel_plot in order to plot
;     only those G(T)'s used in the integral inversion
;               MABUND increased from 20 to 30
;               Increased version number to 1.6
;   1.14  Alessandro Lanzafame
;     a) Linked structures substituted by pointing indeces.
;     b) Computations of line intensities are now separated
;        for lines included and not included in the integral
;        inversion.
;     c) Weighting control now operates through integer iweight
;        in routine run_dem only.
;     d) Structure demdat changed to get more output
;        quantities from FORTRAN
;     e) Structure demdat sorted by increasing temperature
;                  before spawning to fortran. This to avoid problems in
;       associating the fortran results to transition's
;        listing.
;   1.15    Alessandro Lanzafame
;     a) in output file (demap_output.dat) deviation is now multiplied
;        by the constant variance (sigma) to get the non-reduced value
;        (modification dated 06-12-00 in f01wout.pro)
;     b) included abundance analysis: after the DEM evaluation, lines used
;        for the integral inversion plus all lines
;        flagged 'a' in the intensity file are used to get the elemental
;        abundance which gives the minimum deviation for these lines;
;        error bars are evaluated by searching the abundance range
;        which produce deviation less than one (i.e. predicted intensity
;                  within observational uncertainties)
;     c) Removed reduntant input "mpre" in call to predict_intensity
;   1.16    Richard Martin
;        Increased version no to 1.9
;   1.17    Alessandro Lanzafame
;        Added keyword "ABUNDANCE" in rd_lines for abundance
;        analysis, which makes procedure rd_lines_abund.pro
;        obsolete.
;   2.0     Alessandro Lanzafame
;        Adapted for use with Windows
;        Increased version no to 2.0
;
;
; VERSION:
;   1.0   22-08-95
;   1.1   03-05-96
;   1.2   07-05-96
;   1.3   07-05-96
;   1.4   14-05-96
;   1.5   14-05-96
;   1.6   12-07-96
;   1.7   12-07-96
;   1.8   07-08-96
;   1.9   14-10-96
;   1.10  21-02-97
;   1.11  07-04-97
;   1.12  09-04-97
;   1.13  25-01-99
;   1.14  13-04-00
;   1.15  11-12-00
;   1.16  17-07-02
;   1.17  02-08-02
;   2.00  25-07-04
;
;-
;-----------------------------------------------------------------------------

; Common statements before procedure defn cause problems at Cadarache
;@params
;@cfiles
;@cwidg
;@cdemdat
;@cplot

PRO mbar_event, event

@params
@cfiles
@cwidg
@cdemdat
@cplot

H = dblarr(MTRA,MTEMP)

    WIDGET_CONTROL, event.id, GET_UVALUE = eventval


    title=' DEM input selection'
    head=' Input'

    CASE eventval OF

;-----------------------------------------------------------------------
; File submenu
;-----------------------------------------------------------------------

        'QUIT': BEGIN
        WIDGET_CONTROL,message_status,SET_VALUE='Quit'
            WIDGET_CONTROL, event.top, /Destroy
        END

        'EXIT': BEGIN
      WIDGET_CONTROL,message_status,SET_VALUE='saving defaults and exiting'
            save, inval, inpfls, abn, abnl, obs, gofts, goftsl, goftsp, $
              demdat, iweight, weight_default, outval, predint,    $
                  idsrt, ldob, ldol, msel,          $
         dlam, co, ncap7,              $
         smooth_default, ir1uss, sigma, filename=deffile
                  WIDGET_CONTROL, event.top, /Destroy
        END

        'inp_abundance': BEGIN
        WIDGET_CONTROL,message_status,SET_VALUE='reading abundance data'
            dem_in,inpfls, inval, action, title, head,FONT=font, SEL='abundance'
            if strlen(strtrim(inpfls.abundance,2)) ne 0 then begin
                gabund,inpfls.abundance,abn,MABUND
            WIDGET_CONTROL,file_abund,SET_VALUE=inpfls.abundance
                if strlen(strtrim(inpfls.intensity,2)) ne 0 then begin
                WIDGET_CONTROL,message_status,SET_VALUE='linking abundances'
                    link_abund,obs,abn,abnl,MAXBLND,MTRA
                endif
            endif else begin
                WIDGET_CONTROL,message_status,SET_VALUE='No abundance data read'
                wait,3
            endelse
        WIDGET_CONTROL,message_status,SET_VALUE='ready'
        END

        'inp_data': BEGIN
        WIDGET_CONTROL,message_status,SET_VALUE='reading intensity data'
            dem_in,inpfls,inval, action, title, head,FONT=font, SEL='intensity'
            if strlen(strtrim(inpfls.intensity,2)) ne 0 then begin

            ;------------------------------------
            ; read observational data from file
            ;------------------------------------

                rd_lines,inpfls.intensity,obs,MAXBLND,MTRA

            ;-----------------------------------
            ; convert units if necessary
            ;-----------------------------------

                intensity_units,obs

            ;-----------------------------------
            ; multiply weight by 4*pi
            ;-----------------------------------
            ; !!!!! ONLY RUN_DEM SHOULD TAKE CARE OF THIS !!!!!!!!!!!!!
            ;    weight = 12.56*obs.sg00

            ;-----------------------------------
            ; default weighting from intensity data file
            ;-----------------------------------

                iweight = 2

                WIDGET_CONTROL,file_intensity,SET_VALUE=inpfls.intensity
                if strlen(strtrim(inpfls.abundance,2)) ne 0 then begin
                WIDGET_CONTROL,message_status,        $
           SET_VALUE='linking abundances'
                    link_abund,obs,abn,abnl,MAXBLND,MTRA
                endif
                if strlen(strtrim(inpfls.kernel,2)) ne 0 then begin
                WIDGET_CONTROL,message_status,SET_VALUE='linking kernels'
                    link_kernel,obs,gofts,msel,goftsl,mpre,goftsp,MAXBLND
                endif
            endif else begin
                WIDGET_CONTROL,message_status,SET_VALUE='No intensity data read'
                wait,3
            endelse
        WIDGET_CONTROL,message_status,SET_VALUE='ready'
        END

        'inp_collection': BEGIN
        WIDGET_CONTROL,message_status,SET_VALUE='reading kernel functions'
            dem_in,inpfls, inval, action, title, head,FONT=font, SEL='kernel'
            if strlen(strtrim(inpfls.kernel,2)) ne 0 then begin
                ercoll,inpfls.kernel,gofts,MTRA,MTEMP
                WIDGET_CONTROL,file_kernel,SET_VALUE=inpfls.kernel
                if strlen(strtrim(inpfls.intensity,2)) ne 0 then begin
                WIDGET_CONTROL,message_status,SET_VALUE='linking kernels'
                    link_kernel,obs,gofts,msel,goftsl,mpre,goftsp,MAXBLND
                endif
            endif else begin
                WIDGET_CONTROL,message_status,SET_VALUE='No kernel data read'
                wait,3
            endelse
        WIDGET_CONTROL,message_status,SET_VALUE='ready'
        END

;-----------------------------------------------------------------------
; Temperature grid submenu
;-----------------------------------------------------------------------

       'tgrid': BEGIN
       WIDGET_CONTROL,message_status,$
                          SET_VALUE='Temperature grid selection'

           tmpval = { tmpval,                         $
                      ntemp     : demdat.ntemp,          $
                      temp  : demdat.temp,          $
                      grpmess   : ''                    }
           tgrid_sel, VALUE=tmpval, UVALUE=newgrid, FONT=font

           demdat.ntemp = newgrid.ntemp
           demdat.temp  = newgrid.temp

           if demdat.ntemp lt MTEMP then demdat.temp(demdat.ntemp:MTEMP-1) = 0.0

           WIDGET_CONTROL,ntemp_value,   $
                          SET_VALUE=string(demdat.ntemp,format='(i4)')
           WIDGET_CONTROL,tmin_value,   $
                          SET_VALUE=string(demdat.temp(0),format='(e12.4)')
           WIDGET_CONTROL,tmax_value,   $
           SET_VALUE=string(demdat.temp(demdat.ntemp-1),format='(e12.4)')
           WIDGET_CONTROL,step_value,                               $
           SET_VALUE= string(                                    $
                           alog10(                                  $
                                  (demdat.temp(demdat.ntemp-1)/     $
                                   demdat.temp(0))^                 $
                                  ( 1.0/(demdat.ntemp - 1))         $
                                                               ),   $
                                   format='(e12.4)'            )
           WIDGET_CONTROL,logtmin_value,   $
           SET_VALUE=string(alog10(demdat.temp(0)),format='(e12.4)')
           WIDGET_CONTROL,logtmax_value,   $
           SET_VALUE=string(   $
                    alog10(demdat.temp(demdat.ntemp-1)),format='(e12.4)')
           WIDGET_CONTROL,message_status,SET_VALUE='ready'
        END

;-----------------------------------------------------------------------
; Weight submenu
;-----------------------------------------------------------------------

        'weight_intensity': BEGIN
         WIDGET_CONTROL,message_status,SET_VALUE='weight intensity'
             if n_elements(obs) ne 0 then begin
             WIDGET_CONTROL,message_weight,SET_VALUE='intensity'
                 ;weight = 12.56 * obs.g00
                 iweight = 0
             endif else begin
                 WIDGET_CONTROL,message_status,          $
                                SET_VALUE='No intensity data read yet'
                 wait,3
             endelse
             WIDGET_CONTROL,message_status,SET_VALUE='ready'
        END

        'weight_squareroot': BEGIN
        WIDGET_CONTROL,message_status,          $
                           SET_VALUE='weight square root of intensity'
            if n_elements(obs) ne 0 then begin
                WIDGET_CONTROL,message_weight,          $
                               SET_VALUE='square root of intensity'
                ;weight = sqrt(12.56 * obs.g00)
                iweight = 1
            endif else begin
                WIDGET_CONTROL,message_status,          $
                               SET_VALUE='No intensity data read yet'
                wait,3
            endelse
        WIDGET_CONTROL,message_status,SET_VALUE='ready'
        END

        'weight_file': BEGIN
         WIDGET_CONTROL,message_status,          $
                            SET_VALUE='weight from intensity data file'
             if n_elements(obs) ne 0 then begin
                 WIDGET_CONTROL,message_weight,          $
                                SET_VALUE='from intensity data file'
                 ;weight = 12.56*obs.sg00
                 iweight = 2
             endif else begin
                 WIDGET_CONTROL,message_status,          $
                                SET_VALUE='No intensity data read yet'
                 wait,3
             endelse
         WIDGET_CONTROL,message_status,SET_VALUE='ready'
        END

;-----------------------------------------------------------------------
; smooth submenu
;-----------------------------------------------------------------------

        'r1uss1': BEGIN
            WIDGET_CONTROL,message_status,SET_VALUE='setting smooth method'
        WIDGET_CONTROL,message_smooth,          $
               SET_VALUE='residual sum of squares function equal noise variance'
            ir1uss=1
            WIDGET_CONTROL, smooth_b, SENSITIVE=1
            WIDGET_CONTROL,message_status,SET_VALUE='ready'
        END

        'r1uss2': BEGIN
            WIDGET_CONTROL,message_status,SET_VALUE='setting smooth method'
        WIDGET_CONTROL,message_smooth,          $
            SET_VALUE='EDF function equal noise variance'
            ir1uss=2
            WIDGET_CONTROL, smooth_b, SENSITIVE=1
            WIDGET_CONTROL,message_status,SET_VALUE='ready'
        END

        'r1uss3': BEGIN
            WIDGET_CONTROL,message_status,SET_VALUE='setting smooth method'
        WIDGET_CONTROL,message_smooth,          $
            SET_VALUE='automatic'
            ir1uss=3
            WIDGET_CONTROL, smooth_b, SENSITIVE=0
            WIDGET_CONTROL,message_status,SET_VALUE='ready'
        END

;-----------------------------------------------------------------------
; Noise submenu
;-----------------------------------------------------------------------

        'noise_input': BEGIN
            WIDGET_CONTROL,message_status,SET_VALUE='input noise varaince'
            messvalue = " Enter noise variance: - "
            titlvalue = "INPUT NOISE VARIANCE"
            action = popin(message=messvalue, title = titlvalue,    $
                            xoffset=200,yoffset=200, value = sigma)
        sigma = action
            WIDGET_CONTROL,message_sigma,             $
                           SET_VALUE=string(sigma,format='(e12.4)')
            WIDGET_CONTROL,message_status,SET_VALUE='ready'
        END

;-----------------------------------------------------------------------
; Plot submenu
;-----------------------------------------------------------------------

        'plot_kernel': BEGIN
            if ircode eq 1 then begin
           WIDGET_CONTROL,message_status,SET_VALUE='plotting kernel functions'
;              kernel_plot,gofts
;              kernel_plot,obs,goftsl
               kernel_plot,demdat
            endif else begin
               WIDGET_CONTROL,message_status,          $
         SET_VALUE='must execute inversion first'
               wait,3
            endelse
        WIDGET_CONTROL,message_status,SET_VALUE='ready'
        END

        'plot_dem': BEGIN
            if ircode eq 1 then begin
           WIDGET_CONTROL,message_status,SET_VALUE='plotting D.E.M.'
               DEM_PLOT
            endif else begin
               WIDGET_CONTROL,message_status,          $
         SET_VALUE='must execute inversion first'
               wait,3
            endelse
        WIDGET_CONTROL,message_status,SET_VALUE='ready'
        END

        'graph_sel': BEGIN
        WIDGET_CONTROL,message_status,SET_VALUE='graphic selection'
            GRAPH_SEL
        WIDGET_CONTROL,message_status,SET_VALUE='ready'
        END

;-----------------------------------------------------------------------
; Run submenu
;-----------------------------------------------------------------------

        'run_dem': BEGIN
            if strlen(strtrim(inpfls.abundance,2)) ne 0 and        $
            strlen(strtrim(inpfls.intensity,2)) ne 0 and      $
            strlen(strtrim(inpfls.kernel,2)) ne 0 then begin
            WIDGET_CONTROL,message_status,          $
                               SET_VALUE='executing integral inversion'
                run_dem, FORTRDIR, abn, abnl, obs, gofts, msel, goftsl, $
                         MAXBLND, ldob, ldol, demdat, idsrt, iweight, rss, $
                         ir1uss, rl, sigma, Hmax, ircode, dlam, co, ncap7
            if ircode eq 1 then begin
               WIDGET_CONTROL,message_status,$
                                  SET_VALUE='predicting line of sight intensity'
                   predict_intensity, abn, abnl, obs, gofts, goftsp, $
                           demdat, predint, H, dlam, co, ncap7, MAXBLND
                   ; pr_predict,obs,demdat,gofts,predint,rl,sigma,H,Hmax
               WIDGET_CONTROL,message_status,$
                                  SET_VALUE='writing output'
                   f01wout, arelease, adasprog, inpfls,         $
                            obs, demdat, gofts, goftsl, goftsp, $
                            ldob, ldol, idsrt, iweight,     $
                            predint, rl, sigma, H, Hmax, rss
               WIDGET_CONTROL,message_status,SET_VALUE='ready'
       endif else begin
           WIDGET_CONTROL,message_status,$
                                  SET_VALUE='ready (error occurred)'
       endelse
            endif else begin
                WIDGET_CONTROL,message_status,          $
         SET_VALUE='input set not complete'
                wait,3
                WIDGET_CONTROL,message_status,          $
         SET_VALUE='inversion not performed'
                wait,3
                WIDGET_CONTROL,message_status,SET_VALUE='ready'
            endelse
        END

        'abund_analysis': BEGIN
        if demdat.m ne 0 then begin
               ;___temporary instruction
               ;WIDGET_CONTROL,message_status,          $
       ; SET_VALUE='to be implemented'
               ;wait,3
               ;___temporary instructions ends
               ;___uncomment following lines when finished
           WIDGET_CONTROL,message_status,SET_VALUE='performing abundance analysis'
               rd_lines,inpfls.intensity,obs_abund,MAXBLND,MTRA,/ABUNDANCE
               intensity_units,obs_abund
               link_abund,obs_abund,abn,abnl_an,MAXBLND,MTRA
               link_kernel,obs_abund,gofts,msel_ab,goftsl_ab,$
                           mpre_ab,goftsp_ab,MAXBLND
               abundance_analysis,obs_abund,abn,abnl_an,gofts,goftsp_ab, $
                                  demdat,H,dlam,co,ncap7,sigma,iweight,MAXBLND
               ;___end of temporary comments
            endif else begin
               WIDGET_CONTROL,message_status,          $
         SET_VALUE='must execute inversion first'
               wait,3
            endelse
        WIDGET_CONTROL,message_status,SET_VALUE='ready'
        END

        ELSE: BEGIN
       ; Print the button's user value to the IDL window:
        PRINT, 'Widget User Value = ' + eventval
    END

    ENDCASE

END

;-----------------------------------------------------------------------
;-----------------------------------------------------------------------
;      MAIN PROGRAM
;-----------------------------------------------------------------------
;-----------------------------------------------------------------------

PRO DEMAP_MAIN, adasrel, fortdir, userroot, centroot,        $
            devlist, devcode, font_large, font_small, edit_fonts

COMMON header_blk, header

@params
@cfiles
@cwidg
@cdemdat
@cplot

;-----------------------------------------------------------------------
;            Initialisation
;-----------------------------------------------------------------------

    MTRA = 500
   MTEMP = 101
  MABUND = 30
 MAXBLND = 5

   ircode = 0

    lpend = 0
    deffile = userroot+'/defaults/adas601_defaults.dat'
    device = ''
    fortrdir=fortdir
    arelease = adasrel

    dvlist=devlist
    dvcode=devcode
    font=font_small

       ;**** Create header for output. ****
    adasprog = 'PROGRAM: ADAS 601 V2.0'
    date = xxdate()
    header = adasrel +' '+ adasprog + ' DATE: ' + date(0) + ' TIME: ' + date(1)

    status='ready'

    spawn,'date +"%d/%m/%y%n%H:%M"',date,/sh

;-----------------------------------------------------------------------
;      Search for user default settings
;      If not found create defaults
;-----------------------------------------------------------------------

    files = findfile(deffile)
    if files(0) eq deffile then begin

                ;**** Create an information widget ****

    widget_control, /hourglass
    widgetbase = widget_base(/column,xoffset=300,yoffset=200,     $
                            title = "ADAS601 INFORMATION")
    lab0 = widget_label(widgetbase, value='')
    lab1 = widget_label(widgetbase, value='')
    lab2 = widget_label(widgetbase, font=font_large,              $
                        value="       Restoring settings - please wait       ")
    lab3 = widget_label(widgetbase, value='')
    lab4 = widget_label(widgetbase, value='')
    widget_control, widgetbase, /realize
        restore,deffile
        inval.centroot = centroot
        inval.userroot = userroot
    widget_control, widgetbase, /destroy
    endif else begin
        inval = {   ROOTPATH  :    userroot,      $
             FILE     :  '',         $
             CENTROOT :   centroot,        $
             USERROOT :   userroot      }

        inpfls = {      ABUNDANCE  :    '',         $
             INTENSITY    :  '',       $
             KERNEL     :    ''            }

        outval = {  OUTBUT     :    0,          $
         GTIT1       : '',          $
             SCALBUT   :   0,       $
         XMIN     :  '',           $
         XMAX     :  '',         $
         YMIN     :  '',           $
         YMAX     :  '',         $
         SIGN     :      0,        $
         HRDOUT     :    0,          $
         HARDNAME :   '',        $
         GRPDEF     :    '',       $
         GRPFMESS :   '',        $
         GRPSEL     :    -1,       $
         GRPRMESS :   '',        $
         DEVSEL     :    -1,       $
         GRSELMESS    :  ''        }

        demdat = {      DEMDAT,             $
                    NTEMP        :     0,        $
             TEMP    :   dblarr(MTEMP),     $
             M    :   0,      $
             G    :   dblarr(MTRA),       $
             SG    :   dblarr(MTRA),       $
             NG    :   intarr(MTRA),       $
             T    :   dblarr(MTEMP,MTRA),  $
             GOFT    :   dblarr(MTEMP,MTRA),  $
             TM    :   dblarr(MTRA),       $
                        YS        :     dblarr(MTRA),   $
                    FHAT        :     dblarr(MTEMP),     $
                        F0        :     dblarr(MTEMP),     $
         IREC     :  dblarr(MTRA),       $
         EPS   :   dblarr(MTRA),     $
         SIG   :   dblarr(MTRA),     $
         DEV   :   dblarr(MTRA)   }


    abn = {     abn,             $
                   nabund      :    0,         $
                symbamb    :     strarr(MABUND),       $
                   eabund      :    dblarr(MABUND)      }

    gofts = {    gofts,                 $
                   ntra      :  0,       $
                elemc      :  strarr(MTRA),     $
                  izi      :  intarr(MTRA),     $
                   indx      :  intarr(MTRA),     $
                 ng      :  intarr(MTRA),     $
                  awl      :  fltarr(MTRA),     $
                  swl      :  fltarr(MTRA),     $
                filei      :  strarr(MTRA),     $
                    t      :  dblarr(MTEMP,MTRA), $
                   goft      :  dblarr(MTEMP,MTRA)      }

    TGRID_LOG   ;NB: this defines defaults demdat.ntemp and demdat.temp

        weight_default='from intensity data file'
        smooth_default='automatic'
        ir1uss=3
        sigma=0.4d-1
    endelse


;-----------------------------------------------------------------------
;  Menu Bar
;-----------------------------------------------------------------------

    base = WIDGET_BASE(TITLE = 'ADAS 601 V1.9 - BETA',          $
              EVENT_FUNC="mbar_event",          $
              MBAR=bar_base,/COLUMN)

;-----------------------------------------------------------------------
    file_menu  = WIDGET_BUTTON(bar_base, Value='File', FONT=font, /Menu)
    file_bttn1 = WIDGET_BUTTON(file_menu,          $
                Value='read abundance file',   $
                Uvalue='inp_abundance', FONT=font)
    file_bttn2 = WIDGET_BUTTON(file_menu,          $
                Value='read intensity data file',     $
                Uvalue='inp_data', FONT=font)
    file_bttn3 = WIDGET_BUTTON(file_menu,          $
                Value='read kernel data file',      $
                Uvalue='inp_collection', FONT=font)
    file_bttn4 = WIDGET_BUTTON(file_menu, Value='exit',        $
                Uvalue='EXIT', FONT=font)
    file_bttn5 = WIDGET_BUTTON(file_menu, Value='quit',        $
                Uvalue='QUIT', FONT=font)

;-----------------------------------------------------------------------

    tgrid_menu = WIDGET_BUTTON(bar_base, Value='Temperature', FONT=font, /Menu)
    tgrid_bttn1 = WIDGET_BUTTON(tgrid_menu,          $
                 Value='Temperature grid selection',  $
                 Uvalue='tgrid', FONT=font)

;-----------------------------------------------------------------------
    weight_menu  = WIDGET_BUTTON(bar_base, Value='Weight', FONT=font, /Menu)
    weight_bttn1 = WIDGET_BUTTON(weight_menu,           $
                  Value='use intensity',      $
                  Uvalue='weight_intensity', FONT=font)
    weight_bttn2 = WIDGET_BUTTON(weight_menu,           $
                  Value='use square root of the intensity',$
                  Uvalue='weight_squareroot', FONT=font)
    weight_bttn3 = WIDGET_BUTTON(weight_menu,           $
                                 Value='get weights from data file',    $
                  Uvalue='weight_file', FONT=font)

;-----------------------------------------------------------------------
    smooth_menu  = WIDGET_BUTTON(bar_base, Value='Smoothing', FONT=font, /Menu)
    smooth_bttn1 = WIDGET_BUTTON(smooth_menu,           $
    Value='residual sum of squares equal noise variance',    $
    Uvalue='r1uss1', FONT=font)
    smooth_bttn2 = WIDGET_BUTTON(smooth_menu,           $
                                 Value='EDF equal noise variance',  $
                                 Uvalue='r1uss2', FONT=font)
    smooth_bttn3 = WIDGET_BUTTON(smooth_menu, Value='automatic',    $
                             Uvalue='r1uss3', FONT=font)

;-----------------------------------------------------------------------
    noise_menu = WIDGET_BUTTON(bar_base, Value='Noise', FONT=font, /Menu)
    noise_bttn1 = WIDGET_BUTTON(noise_menu,          $
                 Value='input noise variance',       $
                 Uvalue='noise_input', FONT=font)

;-----------------------------------------------------------------------
    plot_menu  = WIDGET_BUTTON(bar_base, Value='Plot', FONT=font, /Menu)
    plot_bttn1 = WIDGET_BUTTON(plot_menu,          $
                Value='graphical selection',   $
                Uvalue='graph_sel', FONT=font)
    plot_bttn2 = WIDGET_BUTTON(plot_menu,          $
                Value='plot kernel',       $
                Uvalue='plot_kernel', FONT=font)
    plot_bttn3 = WIDGET_BUTTON(plot_menu,          $
                Value='plot D.E.M.',       $
                Uvalue='plot_dem', FONT=font)

;-----------------------------------------------------------------------
    run_menu = WIDGET_BUTTON(bar_base, Value='Run', FONT=font, /Menu)
    run_bttn1 = WIDGET_BUTTON(run_menu,           $
             Value='execute integral inversion',   $
               Uvalue='run_dem', FONT=font)
    run_bttn2 = WIDGET_BUTTON(run_menu,           $
             Value='abundance analysis',           $
               Uvalue='abund_analysis', FONT=font)

;-----------------------------------------------------------------------

;-----------------------------------------------------------------------
;-----------------------------------------------------------------------
;  Information on input files widgets
;-----------------------------------------------------------------------

    filesbase= WIDGET_BASE(base,/COLUMN)

    rowabund = WIDGET_BASE(filesbase, /ROW,/FRAME)
    name_abund=WIDGET_LABEL(rowabund,             $
             VALUE=' Abundance file:   ', FONT=font)
    file_abund = WIDGET_TEXT(rowabund,            $
              VALUE = inpfls.abundance,      $
              XSIZE=100,YSIZE=1, FONT=font)
    rowintensity = WIDGET_BASE(filesbase, /ROW,/FRAME)
    name_intensity =WIDGET_LABEL(rowintensity,          $
                  VALUE=' Intensity file:   ', FONT=font)
    file_intensity = WIDGET_TEXT(rowintensity,           $
                  VALUE = inpfls.intensity,     $
                  XSIZE=100,YSIZE = 1, FONT=font)
    rowkernel = WIDGET_BASE(filesbase, /ROW,/FRAME)
    name_kernel =WIDGET_LABEL(rowkernel,          $
               VALUE=' Kernel file:      ', FONT=font)
    file_kernel = WIDGET_TEXT(rowkernel,           $
               VALUE = inpfls.kernel,        $
               XSIZE=100,YSIZE = 1, FONT=font)

;-----------------------------------------------------------------------
;  End of information on input files widgets
;-----------------------------------------------------------------------

;-----------------------------------------------------------------------
;  Temperatures display widget
;-----------------------------------------------------------------------

    tempgrid_id = WIDGET_BASE(base, /COLUMN,/FRAME)
    tempgrid_a = WIDGET_BASE(tempgrid_id, /ROW,/FRAME)

    ntemp_label = WIDGET_LABEL(tempgrid_a,           $
                               VALUE=' No of Temperature Points: ', FONT=font)
    ntemp_value=WIDGET_TEXT(tempgrid_a,           $
             VALUE = string(demdat.ntemp,format='(i4)'),  $
             XSIZE=6,YSIZE = 1, FONT=font)

    tmin_label=WIDGET_LABEL(tempgrid_a,VALUE='       Tmin: ', FONT=font)
    tmin_value=WIDGET_TEXT(tempgrid_a,                         $
                       VALUE=string(demdat.temp(0),format='(e12.4)'),$
                       XSIZE=12,YSIZE = 1, FONT=font)

    tmax_label=WIDGET_LABEL(tempgrid_a,VALUE='       Tmax: ', FONT=font)
    tmax_value=WIDGET_TEXT(tempgrid_a,                              $
    VALUE = string(demdat.temp(demdat.ntemp-1),format='(e12.4)'),   $
    XSIZE=12,YSIZE = 1, FONT=font)

    tempgrid_b = WIDGET_BASE(tempgrid_id, /ROW,/FRAME)

    step_label = WIDGET_LABEL(tempgrid_b,           $
                              VALUE='     Step in Log(T): ', FONT=font)
    step_value=WIDGET_TEXT(tempgrid_b,            $
            VALUE = string(                          $
                           alog10(                           $
                           (demdat.temp(demdat.ntemp-1)/     $
                            demdat.temp(0))^                 $
                           ( 1.0/(demdat.ntemp - 1) )        $
                                                        ),   $
                           format='(e12.4)'                  $
                                                        ),   $
            XSIZE=12,YSIZE = 1, FONT=font)

    logtmin_label=WIDGET_LABEL(tempgrid_b,VALUE=' Log(T-min): ', FONT=font)
    logtmin_value=WIDGET_TEXT(tempgrid_b,                               $
    VALUE = string(alog10(demdat.temp(0)),format='(e12.4)'),        $
    XSIZE=12,YSIZE = 1, FONT=font)

    logtmax_label=WIDGET_LABEL(tempgrid_b,VALUE=' Log(T-max): ', FONT=font)
    logtmax_value=WIDGET_TEXT(tempgrid_b,                               $
    VALUE = string(alog10(demdat.temp(demdat.ntemp-1)),format='(e12.4)'),$
    XSIZE=12,YSIZE = 1, FONT=font)

;-----------------------------------------------------------------------
;  smooth display widget
;-----------------------------------------------------------------------

    rowsmooth = WIDGET_BASE(base, /ROW, /FRAME)

    smooth_a = WIDGET_BASE(rowsmooth,/ROW)
    name_smooth =WIDGET_LABEL(smooth_a,           $
               VALUE=' Smoothing: ', FONT=font)
    message_smooth = WIDGET_TEXT(smooth_a,           $
                  VALUE = smooth_default,     $
                  XSIZE=60,ysize=1, FONT=font)

    smooth_b = WIDGET_BASE(rowsmooth,/ROW)
    name_sigma =WIDGET_LABEL(smooth_b,            $
              VALUE='   Noise variance: ', FONT=font)
    message_sigma = WIDGET_TEXT(smooth_b,           $
                 VALUE = string(sigma,format='(e12.4)'),  $
                 XSIZE=12,ysize=1, FONT=font)

    WIDGET_CONTROL, smooth_b, SENSITIVE=0

;-----------------------------------------------------------------------
;  Weight display widget
;-----------------------------------------------------------------------

    rowweight = WIDGET_BASE(base, /ROW)
    name_weight =WIDGET_LABEL(rowweight,          $
               VALUE='  Weight:    ', FONT=font)
    message_weight = WIDGET_TEXT(rowweight,           $
         VALUE = weight_default,          $
         XSIZE=60,ysize=1, FONT=font)
;-----------------------------------------------------------------------
;  Status display widget
;-----------------------------------------------------------------------

    rowstatus = WIDGET_BASE(base, /ROW)
    name_status =WIDGET_LABEL(rowstatus,          $
               VALUE='  Status:    ', FONT=font)
    message_status = WIDGET_TEXT(rowstatus,           $
                  VALUE = status,        $
                  XSIZE=60,ysize=1, FONT=font)

;-----------------------------------------------------------------------
;  Realize base widget
;-----------------------------------------------------------------------

    WIDGET_CONTROL, base, /REALIZE
    XMANAGER, "mbar", base, GROUP_LEADER = GROUP

END
