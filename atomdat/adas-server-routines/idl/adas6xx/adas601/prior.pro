; Copyright (c) 1997, Alessandro Lanzafame, Universita' di Catania
; SCCS info: Module @(#)$Header: /home/adascvs/idl/adas6xx/adas601/prior.pro,v 1.2 2012/11/14 19:45:28 mog Exp $ Date $Date: 2012/11/14 19:45:28 $
;+
; PROJECT:
;	ADAS
;
; NAME: 
;	PRIOR
;
; NOTE:
;
; PURPOSE: 
;	This function is specific to ADAS601 (DEMAP). It defines the prior
;       for the estimate of the DEM according to the "data adaptive 
;       smoothing approach.
;
; EXPLANATION:
;	This function is derived from a FORTRAN function written by Alan
;	Thompson, University of Glasgow. It calculates the prior from the 
;	spline representation of a first approximation of the DEM. If the 
;	ends of the prior are extrapolated linearly from the ends of the 
;	valid spline region.
;	
; USE:
;	Given the abscissa t (Log of temperature) and the B-spline
;	representation of the first approximation to the DEM in terms
;       of knots dlam, B-spline coefficients co, and number of spline
;	intervals + 7 (ncap7),
;
;	x= prior(t,dlam,co,ncap7)
;
;	returns prior in x.
;
; INPUTS:
;	T     =  Log of temperature
;
;	DLAM  =  Values of the complete set of knots
;                LAMDA(J), J=1,NBAR+7.
;                CONSTRAINT: Must be in non-decreasing
;                order with LAMDA(NCAP7-4) > LAMDA(3)
;
;	CO    =  The coefficients of the B-spline N_i(x),
;                for i=0,1,2,...,nbar+2.
;
;	NCAP7 =  NBAR+7, where NBAR is the number of
;                intervals (one greater than the number
;                of interior knots, i.e. the knots 
;                strictly in the range LAMDA(3) to 
;                LAMDA(NCAP+3)) over which the spline
;                is defined.
;                CONSTRAINT: NCAP7 >= 8
;
; OPTIONAL INPUTS:
;	None
;
; OUTPUTS:
;	This functions returns the evaluation of the prior function at t.
;
; OPTIONAL OUTPUTS:
;	None
;
; KEYWORD PARAMETERS:
;	None
;
; CALLS:
;	None
;
; SIDE 	EFFECTS:
;	None
;
; CATEGORY:
;	ADAS
;
; WRITTEN:
;       A. C. Lanzafame, Istituto di Astronomia
;                        Universita' di Catania
;                        Citta' Universitaria
;                        Viale Andrea Doria, 6
;                        I-95125 Catania, Italy
;                        Tel: +39 95 7332 240
;                        Fax: +39 95 330592
;                        e-mail: alanzafame@ct.astro.it
; MODIFIED:
;	1.0 	Alessandro Lanzafame
;		First version
;
;
;       1.2     Alessandra Giunta
;               Changed extrapolation at low temperature:
;               linear instead of spline
;
; VERSION:
;	1.0	20-02-97
;
;
;       1.2     11-08-08
;

;-

function prior, t, dlam, co, ncap7

   wid  = 6.0d-2
   wid1 = 1.0d-2
   ns   = 4
   s    = dblarr(ns)

;------------------------------------------------------------------------
; convert T to base 10
;------------------------------------------------------------------------

   x=t/alog(10.d0)

;------------------------------------------------------------------------
; calculate log10 of the Prior
;------------------------------------------------------------------------

   if (x lt dlam(3)) then begin

      x0=dlam(3)
      left=0
      ifail=0
      f01bcf, ncap7, dlam, co, x0, left, s, ifail
      t0=10.d0^x0
      t1=10.d0^x

      if (s(1) gt 0.d0) then begin
         prior=s(0)+(x-x0)*s(1)
      endif else begin

;--------------------------------------------------
;         prior=s(0)+alog10(1.d0+(t1/t0-1.d0)*s(1))
         prior=s(0)+(x-x0)*s(1)
;--------------------------------------------------

      endelse
    endif else begin
      if(x lt dlam(ncap7-4)) then begin       
         ifail=0
         fdum = 0.d0
         f01bbf, ncap7, dlam, co, x, fdum, ifail
         s(0) = fdum
         prior=s(0)
      endif else begin
         x0=dlam(ncap7-4)
         left=1
         ifail=0
         f01bcf, ncap7, dlam, co, x0, left, s, ifail
         t0=10.d0^x0
         t1=10.d0^x
         if(s(1) lt 0.d0) then begin
            prior=s(0)+(x-x0)*s(1)
         endif else begin

            prior=s(0)+(x-x0)*s(1)

;            prior=s(0)+alog10(1.d0+(t1/t0-1)*s(1))
         endelse
      endelse
   endelse

;------------------------------------------------------------------------
; calculate the Prior
;------------------------------------------------------------------------
   prior=10.d0^prior
   return, prior
end
