; Copyright (c) 1996, Strathclyde University.
; SCCS info: Module @(#)kernel_plot.pro	1.4 Date 10/4/96
;+
; PROJECT:
;       ADAS support and applications
;
; NAME:
;	KERNEL_PLOT
;
; PURPOSE:
;	Generates DEM kernel functions graphical output.
;
; EXPLANATION:
;	This routine creates a window for the display of graphical
;	output. A separate routine PLOTKERNEL actually plots the
;	graph.
;
; USE:
;	This routine is specific to dem.pro.
;
; INPUTS:
;

; OPTIONAL INPUTS:
;	None.
;
; OUTPUTS:
;	None.
;
; OPTIONAL OUTPUTS:
;	None.
;
; KEYWORD PARAMETERS:
;
; CALLS:
;	CW_ADAS_GRAPH	Graphical output widget.
;	PLOTKERNEL	Make one plot to an output device.
;	XMANAGER
;
; SIDE EFFECTS:
;	This routine uses a common block to maintain its state KERNEL_BLK.
;
;	One other routine is included in this file;
;	ADAS506_PLOT_EV	Called via XMANAGER during widget management.
;
; CATEGORY:
;	Adas support and applications.
;
; WRITTEN:
;       Alessandro Lanzafame, University of Strathclyde, 21st November 1995
;
; MODIFIED:
;	1.0	Alessandro Lanzafame
;		First Release
;	1.1	William Osborn
;		Put under S.C.C.S. control
;	1.2	William Osborn
;		Changed title
;	1.3	Tim Hammond
;		Added dynamic resizing of the screen according to display.
;       1.4     William Osborn
;               Added dynlabel
;       1.5     Alessandro Lanzafame
;               Modified to plot only those G(T)'s that are used to build
;               the kernel functions instead of all those listed in the 
;		G(T) collection file
;       1.6     Alessandro Lanzafame
;               Plot the kernel going into the integral inversion
;               instead of the G(T)'s
;       1.7     Alessandro Lanzafame
;               added BACKSTORE=2 in call to cw_adas_graph               
;
; VERSION:
;	1.0	????????
;	1.1	07-05-96
;	1.2	14-05-96
;	1.3	07-08-96
;	1.4	07-08-96
;       1.5     02-09-97
;       1.6     23-02-99
;       1.7     01-11-02
;
;-
;----------------------------------------------------------------------------

PRO kernel_plot_ev, event

  COMMON kernel_blk, data, action, win, plotdev, plotfile, fileopen


  newplot = 0
  print = 0
  done = 0
  ;--------------------------------------------------------------------------
  ;	Set graph and device requested
  ;--------------------------------------------------------------------------
  CASE event.action OF

	'print'	   : begin
			newplot = 1
			print = 1
		     end

	'done'	   : begin
			if fileopen eq 1 then begin
			  set_plot,plotdev
			  device,/close_file
			end
			set_plot,'X'
			widget_control,event.top,/destroy
			done = 1
		     end
  END

  ;--------------------------------------------------------------------------
  ;	Make requested plot/s
  ;--------------------------------------------------------------------------

  if done eq 0 then begin

  ;--------------------------------------------------------------------------
  ;	Set graphics device
  ;--------------------------------------------------------------------------
    if print eq 1 then begin

      set_plot,plotdev
      if fileopen eq 0 then begin
        fileopen = 1
        device,filename=plotfile
	device,/landscape
      end

    end else begin

      set_plot,'X'
      wset,win
  
    end

  ;--------------------------------------------------------------------------
  ;	 Draw graphics
  ;--------------------------------------------------------------------------
    plotkernel, data.m, data.x , data.y ,$
	   data.title, data.xtitle, data.ytitle, $
	   data.ldef1, data.xmin, data.xmax, data.ymin, data.ymax 


	if print eq 1 then begin
	  message = 'Plot  written to print file.'
	  grval = {WIN:0, MESSAGE:message}
	  widget_control,event.id,set_value=grval
	end


  end

END

;----------------------------------------------------------------------------

PRO kernel_plot,demdat

;,  dsfull, $
;		    titlx, titlm, utitle, date, $
;		    temp, gofta, tosa, gftosa, toma, gftoma, $
;                    ldef1, xmin, xmax, ymin, ymax, $
;                    lfsel, losel, nmx, nener, npspl, $
;                    hrdout, hardname, device, header, FONT=font

;@cdemdat
@params
@cplot
@cfiles

COMMON kernel_blk, data, action, win, plotdev, plotfile, fileopen
COMMON header_blk, header

  ;--------------------------------------------------------------------------
  ;	Copy input values to common
  ;--------------------------------------------------------------------------
  if outval.devsel ge 0 then begin
     device = dvcode(outval.devsel)
     plotdev = device
  endif else begin
     device='x'
  endelse
  plotfile = outval.hardname
  fileopen = 0

  ;--------------------------------------------------------------------------
  ;	Create general graph title
  ;--------------------------------------------------------------------------
  title = strarr(6)
  title(0) = "KERNEL FUNCTIONS VERSUS TEMPERATURE "
  if ( strtrim(strcompress(outval.gtit1),2)  ne ' ' ) then begin
     title(0) = title(0) + ': ' + strupcase(strtrim(outval.gtit1,2))
  endif
  title(1) = 'ADAS    :' + strupcase(header)
  title(2) = 'INTENSITY DATA:  ' + inpfls.intensity
  title(3) = 'G(T) DATA     :  ' + inpfls.kernel
  title(4) =  ' '; +  $
;  'KEY     : (CROSSES - INPUT DATA) (FULL LINE - SPLINE FIT)'
  title(5) = ' ' 
  

  x=dblarr(MTEMP,MTRA)
  y=dblarr(MTEMP,MTRA)

  m  = demdat.m

  x=demdat.t
  y=demdat.goft

  xtitle = " TEMPERATURE (KELVIN)"
  ytitle = "G(T!lE!n)"

  ;--------------------------------------------------------------------------
  ;	if desired set up axis scales 
  ;--------------------------------------------------------------------------
  if (outval.scalbut eq 0) then begin
      xmin = min(x)
      xmin = xmin * 0.9
      xmax = max(x) * 1.1

      ymin = min(y)
      ymin = ymin * 0.9
      ymax = max(y) * 1.1
  endif else begin
      xmin=outval.xmin
      xmax=outval.xmax
      ymin=outval.ymin
      ymax=outval.ymax
  endelse

  ;--------------------------------------------------------------------------
  ;	Create graph display widget
  ;--------------------------------------------------------------------------
  graphid = widget_base(TITLE='ADAS 601 GRAPHICAL OUTPUT: KERNEL', $
					XOFFSET=1,YOFFSET=1)
  device, get_screen_size = scrsz
  xwidth=scrsz(0)*0.75
  yheight=scrsz(1)*0.75
  cwid = cw_adas_graph(graphid,print=outval.hrdout,FONT=font,		$
                       xsize=xwidth, ysize=yheight, BACKSTORE=2)

  ;--------------------------------------------------------------------------
  ;	Realize the new widget
  ;--------------------------------------------------------------------------
  dynlabel, graphid
  widget_control,graphid,/realize
  ;--------------------------------------------------------------------------

  ;--------------------------------------------------------------------------
  ;	Get the id of the graphics area 
  ;--------------------------------------------------------------------------
  widget_control,cwid,get_value=grval
  win = grval.win

  ;--------------------------------------------------------------------------
  ;	Put the graphing data into common 
  ;--------------------------------------------------------------------------
  data = {    	m:m, x:x, y:y,$
	        title:title,  xtitle:xtitle, ytitle:ytitle, $
                ldef1:outval.scalbut, $
		xmin:xmin,	xmax:xmax,	ymin:ymin,	ymax:ymax $
          }
 
  wset,win

  plotkernel, m, x , y, title, xtitle, ytitle, $
	      outval.scalbut, xmin, xmax, ymin, ymax 


  ;--------------------------------------------------------------------------
  ;	make widget modal
  ;--------------------------------------------------------------------------
  xmanager,'kernel_plot',graphid,event_handler='kernel_plot_ev', $
                                        /modal,/just_reg

END



