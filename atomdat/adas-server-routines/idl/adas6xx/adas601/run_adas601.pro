;----------------------------------------------------------------------
;+
; PROJECT    :  ADAS
;
; NAME       :  run_adas601
;
; PURPOSE    :  Run the differential emission measure (DEM) code
;               adas601 non-interactively.
;
; ARGUMENTS  :  All output arguments will be defined appropriately.
;
;               NAME      I/O    TYPE   DETAILS
; INPUTS     :  files      I     struc  files.abundance
;                                       files.intensity
;                                       files.kernel
;               te         I     struc  te.tmin - minimum temperature (K)
;                                       te.tmax - maximum temperature (K)
;                                       te.num  - number of points (max 100)
;               weight     I     int    1 => use intensity (default if not set)
;                                       2 => use square root of intensity
;                                       3 => get weights from data file
;               smoothing  I     int    1 => use residual sum of squares equal noise variance
;                                       2 => use EDF equal noise variance
;                                       3 => automatic (default if not set)
;               noise      I     float  variance (set if smoothing is 1 or 2)
;
; OUTPUTS    :  outfile    O     str    name of output file - defaults to demap_output.dat
;               kernel     O     struc  { num  : number of contributing lines
;                                         nte  : number of non-zero temepratures
;                                                  1st dim : numbe of lines
;                                         te   : electron temperature (K)
;                                                  1st dim : temperature
;                                                  2nd dim : numbe of lines
;                                         goft : contribution function}
;                                                  1st dim : temperature
;                                                  2nd dim : numbe of lines
;
;               dem        O     struc  { te       : temperature (K)
;                                         final    : converged DEM
;                                         prior    : starting DEM
;                                         num      : number of lines
;                                         tran_te  : temperature of line used for DEM
;                                         tran_dem : DEM of line
;
;
; KEYWORDS   :  help              -     displays help entry
;
;
; NOTES      :  run_adas601 uses the spawn command. Therefore IDL v5.3
;               cannot be used. Any other version should work.
;
; AUTHOR     :  Martin O'Mullane
;
; DATE       :  12-04-2019
;
; MODIFIED
;       1.1     Martin O'Mullane
;                 - First version.
;
; DATE
;       1.1     12-04-2019
;
;-
;----------------------------------------------------------------------



PRO run_adas601, files     = files,     $
                 te        = te,        $
                 weight    = weight,    $
                 smoothing = smoothing, $
                 noise     = noise,     $
                 outfile   = outfile,   $
                 kernel    = kernel,    $
                 dem       = dem,       $
                 help      = help


; If asked for help

if keyword_set(help) then begin
   doc_library, 'run_adas416'
   return
endif


; First check that we are not using IDL v5.3

if !version.release EQ '5.3' then begin
   message, 'This version of IDL (v5.3) will not work - ' + $
            'change to any other v5.x or above'
   return
endif


; Check that we get all inputs and that they are correct. Otherwise print
; a message and return to command line.

on_error, 2


; Check file names

if n_elements(files) EQ 0 then message, 'Supply the file names'

adas_check_file, file=files.abundance, description='Abundance file'


; Check other options and set defaults

if te.num GT 100 then message, '100 is the maximum number of temperatures'

if n_elements(weight) EQ 0 then weight = 1
if fix(weight) LT 1 or fix(weight) GT 3 then message, '1, 2 or 3 are the only options for weight'

if n_elements(smoothing) EQ 0 then smoothing = 3
if fix(smoothing) LT 1 or fix(smoothing) GT 3 then message, '1, 2 or 3 are the only options for smoothing'

if n_elements(noise) EQ 0 then begin
   if smoothing EQ 1 OR smoothing EQ 2 then message, 'Set a noise value for smoothing option'
   noise = 0.04
endif


; Populate the internal variables and structures

fortdir = getenv('ADASFORT')

arelease = 'v4.1'
adasprog = 'run_adas601'

MTRA    = 500
MTEMP   = 101
MABUND  = 30
MAXBLND = 5

demdat = { ntemp   :   0,                  $
           temp    :   dblarr(MTEMP),      $
           m       :   0,                  $
           g       :   dblarr(MTRA),       $
           sg      :   dblarr(MTRA),       $
           ng      :   intarr(MTRA),       $
           t       :   dblarr(MTEMP,MTRA), $
           goft    :   dblarr(MTEMP,MTRA), $
           tm      :   dblarr(MTRA),       $
           ys      :   dblarr(MTRA),       $
           fhat    :   dblarr(MTEMP),      $
           f0      :   dblarr(MTEMP),      $
           irec    :   dblarr(MTRA),       $
           eps     :   dblarr(MTRA),       $
           sig     :   dblarr(MTRA),       $
           dev     :   dblarr(MTRA)        }

abn    = { nabund  :   0,                  $
           symbamb :   strarr(MABUND),     $
           eabund  :   dblarr(MABUND)      }

gofts  = { ntra    :  0,                   $
           elemc   :  strarr(MTRA),        $
           izi     :  intarr(MTRA),        $
           indx    :  intarr(MTRA),        $
           ng      :  intarr(MTRA),        $
           awl     :  fltarr(MTRA),        $
           swl     :  fltarr(MTRA),        $
           filei   :  strarr(MTRA),        $
           t       :  dblarr(MTEMP,MTRA),  $
           goft    :  dblarr(MTEMP,MTRA)   }



; Read in data from files

gabund, files.abundance, abn, MABUND

rd_lines, files.intensity, obs, MAXBLND, MTRA
intensity_units, obs

ercoll, files.kernel, gofts, MTRA, MTEMP


; Link the abundances and kernel

link_abund, obs, abn, abnl, MAXBLND, MTRA
link_kernel, obs, gofts, msel, goftsl, mpre, goftsp, MAXBLND



; Set internal values from inputs

demdat.ntemp = te.num
dt           = (te.tmax / te.tmin)^(1.0/(te.num-1))
demdat.temp  = te.tmin * dt^(findgen(te.num))

iweight = fix(weight) - 1
ir1uss  = smoothing
sigma   = noise


; Run the fortran part

run_dem, fortdir, abn, abnl, obs, gofts, msel, goftsl,     $
         MAXBLND, ldob, ldol, demdat, idsrt, iweight, rss, $
         ir1uss, rl, sigma, Hmax, ircode, dlam, co, ncap7


if ircode EQ 1 then begin

   H = dblarr(MTRA,MTEMP)

   predict_intensity, abn, abnl, obs, gofts, goftsp, $
                      demdat, predint, H, dlam, co, ncap7, MAXBLND

   f01wout, arelease, adasprog, files,          $
            obs, demdat, gofts, goftsl, goftsp, $
            ldob, ldol, idsrt, iweight,         $
            predint, rl, sigma, H, Hmax, rss

endif else message, 'An error occured'


; Rename the output file if requested

if n_elements(outfile) EQ 1 then file_move, 'demap_output.dat', outfile, /overwrite


; Return structures for the kernel and the dem

num   = demdat.m
ntemp = max(demdat.ng)

kernel = { num  : num,                            $
           nte  : demdat.ng[0:num-1],             $
           te   : demdat.t[0:ntemp-1, 0:num-1],   $
           goft : demdat.goft[0:ntemp-1, 0:num-1] }

ntemp = te.num
dem = { te       : demdat.temp[0:ntemp-1],  $
        final    : demdat.fhat[0:ntemp-1],  $
        prior    : demdat.f0[0:ntemp-1],    $
        num      : num,                     $
        tran_te  : demdat.tm[0:demdat.m-1], $
        tran_dem : demdat.ys[0:demdat.m-1]  }

END
