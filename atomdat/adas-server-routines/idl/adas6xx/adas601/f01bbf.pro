; Copyright (c) 1997, Alessandro Lanzafame, Universita' di Catania
; SCCS info: Module @(#)$Header: /home/adascvs/idl/adas6xx/adas601/f01bbf.pro,v 1.1 2004/07/06 13:50:30 whitefor Exp $ Date $Date: 2004/07/06 13:50:30 $
;+
; PROJECT:
;	B-spline fitting and interpolating routines
;
; NAME:
;	F01BBF
; NOTE:
;
; PURPOSE: Evaluates a cubic spline from its B-spline representation
;          IDL version of routine F01BBF
;
; EXPLANATION:
;
; INPUTS:
;	  (I*4) NCAP7        = NBAR+7, where NBAR is the number of
;                              intervals (one greater than the number
;                              of interior knots, i.e. the knots 
;                              strictly in the range LAMDA(3) to 
;                              LAMDA(NCAP+3)) over which the spline
;                              is defined.
;                              CONSTRAINT: NCAP7 >= 8
;
;          (R*8) LAMDA()     = Values of the complete set of knots
;                              LAMDA(J), J=1,NBAR+7.
;                              CONSTRAINT: Must be in non-decreasing
;                              order with LAMDA(NCAP7-4) > LAMDA(3).
;
;          (R*8) C()         = The coefficients of the B-spline N_i(x),
;                              for i=0,1,2,...,nbar+2. The remaining 
;                              elements (from NBAR+3 to NBAR+6) are not
;                              used.
;
;          (R*8) X           = The argument x at which the cubic spline
;                              is to be evaluated.
;                              CONSTRAINT: 
;                                 LAMDA(3) <= X <= LAMDA(NCAP7-4)
;          (I*8) IFAIL       = 0 : stop if any error
;                            = 1 : continue if non-fatal error.
;
; OUTPUTS: (R*8) S           = The value of the spline, s(x)
;
;          (I*4) IFAIL       = 0 : no error detected
;                            = 1 : X does not satisfy
;                                   LAMDA(3) <= X <= LAMDA(NCAP7-4)
;                            = 2 : NCAP7 < 8
;
; OPTIONAL INPUTS:
;	None
;
; OPTIONAL OUTPUTS:
;	None
;
; KEYWORD PARAMETERS:
;       None
;
; CALLS:
;       None
;
; SIDE EFFECTS:
;
; CATEGORY:
;       Adas support.
;
; WRITTEN:
;       A. C. Lanzafame,  Istituto di Astronomia
;                         Universita' di Catania
;                         Citta' Universitaria
;                         Viale Andrea Doria 6
;                         95125 Catania, Italy
;                         e-mail: alanzafame@ct.astro.it
;
; MODIFIED:
;       1.1     Alessandro Lanzafame
;               First release.
; VERSION:
;       1.1     20-02-97
;
;-----------------------------------------------------------------------
;-

PRO F01BBF, NCAP7 , LAMDA , C , X ,S , IFAIL 

   V=DBLARR(4)

;-----------------------------------------------------------------------
;  INITIAL CHECKS
;-----------------------------------------------------------------------
   ON_ERROR, 2         ;Return to caller if an error occurs
   IF ((X LT LAMDA(3)) OR (X GT LAMDA(NCAP7-4))) THEN BEGIN
      CASE 1 OF
         (IFAIL EQ 0): BEGIN
            IFAIL = 1
            PRINT, '*** F01BBF ERROR: IFAIL = 1 ***'
            STOP
         END
         (IFAIL EQ 1): BEGIN
            STOP
         END
         ELSE:
      ENDCASE
   ENDIF

   IF (NCAP7 LT 8) THEN BEGIN
      CASE 1 OF
         (IFAIL EQ 0): BEGIN
            IFAIL = 2
            PRINT, '*** F01BBF ERROR: IFAIL = 2 ***'
            STOP
         END
         (IFAIL EQ 1): BEGIN
            IFAIL = 2
            STOP
         END
      ENDCASE
   ENDIF

;-----------------------------------------------------------------------
;  LOCATE X IN LAMDA ARRAY
;-----------------------------------------------------------------------

   KL = 3
   KU = NCAP7-4
LAB_10:
   IF ((KU-KL) GT 1) THEN BEGIN
      KM = (KU+KL)/2
      CASE 1 OF
         ((LAMDA(NCAP7-1) GT LAMDA(0)) AND (X GT LAMDA(KM))): KL = KM
         ((LAMDA(NCAP7-1) LE LAMDA(0)) AND (X LE LAMDA(KM))): KL = KM
         ELSE: KU = KM
      ENDCASE
      GOTO,LAB_10
   ENDIF

   K = KL + 1

;-----------------------------------------------------------------------
;  GENERATE AND EVALUATE B-SPLINES
;-----------------------------------------------------------------------

   E1 = X - LAMDA(K-1)
   E2 = LAMDA(K) - X
   V(0) = 1.0D0/(LAMDA(K)-LAMDA(K-1))
   FOR J = 1, 3 DO BEGIN
      V(J) = E1*V(J-1)/(LAMDA(K+J)-LAMDA(K-1))
   ENDFOR
   FOR J = 1, 3 DO BEGIN
      E3 = X - LAMDA(K-1-J)
      V(0) = E2*V(0)/(LAMDA(K)-LAMDA(K-1-J))
      IF (J LT 3) THEN BEGIN
         FOR L=1, 3-J DO BEGIN
            V(L) = (E3*V(L-1)+(LAMDA(K+L)-X)*V(L))/   $
                   (LAMDA(K+L)-LAMDA(K-1-J))
         ENDFOR
      ENDIF
   ENDFOR

   S=0.0D0
   FOR L=0, 3 DO BEGIN
      S = S + C(K+L-4)*V(L)*(LAMDA(K+L)-LAMDA(K+L-4))
   ENDFOR

;-----------------------------------------------------------------------

END



