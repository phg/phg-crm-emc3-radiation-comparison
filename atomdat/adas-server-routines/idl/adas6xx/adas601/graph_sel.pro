; Copyright (c) 1995, Strathclyde University.
; SCCS info: Module @(#)$Header: /home/adascvs/idl/adas6xx/adas601/graph_sel.pro,v 1.4 2004/07/06 13:59:53 whitefor Exp $ Date $Date: 2004/07/06 13:59:53 $
; Alessandro Lanzafame	acl@phys.strath.ac.uk	21 November 1995
;+
; PROJECT:
;       ADAS support and application programs
;
; NAME:
;       GRAPH_SEL
;
; NOTE:
;	This routine is not yet properly annotated.
;
; PURPOSE:
;
; EXPLANATION:
;
; USE: 
;
; INPUTS:
;
; OPTIONAL INPUTS:
;
; OUTPUTS:
;
; OPTIONAL OUTPUTS:
;
; KEYWORD PARAMETERS:
;
; CALLS:
; SIDE EFFECTS:
;
; CATEGORY:
;       Adas support.
;
; WRITTEN:
;       Alessandro Lanzafame, University of Strathclyde, nov21-95
;
; MODIFIED:
;       1       Alessandro Lanzafame
;               First release.
;       1.1     Tim Hammond
;               Put under S.C.C.S. Control
;	1.2	William Osborn
;		Removed spaces from before macros
;	1.3	Tim Hammond
;		Set font of button
;	1.3	William Osborn
;		Added dynlabel
;
; VERSION:
;       1       21-11-95
;       1.1     03-05-96
;	1.2	07-05-96
;	1.3	07-08-96
;	1.4	04-10-96
;
;-----------------------------------------------------------------------
;-

PRO graph_sel_ev, event

@cplot
@cwidg

    WIDGET_CONTROL, event.id, GET_UVALUE = eventval

    CASE eventval OF
       'graph_sel':$
                     BEGIN
                        WIDGET_CONTROL,grpid,get_value=outval
                     END
 
	'done'	   : begin
			WIDGET_CONTROL,grpid,get_value=outval
			widget_control,event.top,/destroy
                     end
    ENDCASE

END

;-----------------------------------------------------------------------

PRO graph_sel

@cplot
@cwidg

;-----------------------------------------------------------------------
;	Base for the popup widget
;-----------------------------------------------------------------------

    grsel_base=widget_base(TITLE='DEM GRAPHIC SELECTION', /COLUMN, $
					XOFFSET=50,YOFFSET=0)

;-----------------------------------------------------------------------
;	Widget for graphics selection
;	Note change in names for GRPOUT and GRPSCAL
;-----------------------------------------------------------------------

    gr_base = widget_base(grsel_base,/row,/frame)
    grpid = cw_adas_gr_sel(gr_base, OUTPUT='Graphical Output', $
			    DEVLIST=dvlist, VALUE=outval,$
			    UVALUE='graph_sel',FONT=font)

    button= WIDGET_BUTTON(grsel_base, UVALUE='done',VALUE='Done',font=font)

    
    dynlabel, grsel_base
    widget_control,grsel_base,/realize

;--------------------------------------------------------------------------
;	
;--------------------------------------------------------------------------

    xmanager,'graph_sel',grsel_base,event_handler='graph_sel_ev'

END
