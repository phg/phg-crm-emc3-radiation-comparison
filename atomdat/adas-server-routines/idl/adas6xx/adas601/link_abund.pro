; Copyright (c) 1995, Strathclyde University.
; Alessandro Lanzafame	acl@phys.strath.ac.uk	20 March 1996
; SCCS info: Module @(#)$Header: /home/adascvs/idl/adas6xx/adas601/link_abund.pro,v 1.3 2004/07/06 14:11:22 whitefor Exp $ Date $Date: 2004/07/06 14:11:22 $
;+
; PROJECT:
;       ADAS support programs
;
; NAME:
;	LINK_ABUND.PRO
;
; PURPOSE:
;       Link the abundance data to the observed intensities.
;
; EXPLANATION:
;       Given the structure 'abn' containing the list of chemical
;       abundances from a data file, this routine reorganise the 
;       abundance data in a new structure 'abnl' which maps the 
;       abundances to the observed intensities stored in the
;       structure 'obs'. 
;
; USE:
;       From the IDL prompt or within a program
;
;               link_abund,obs,abn,abnl
;
;       will return a structure 'abnl' containing the abundance
;       data mapped to the 'obs' structure.
;
; INPUTS:
;       OBS()    Array of structures containing the observed 
;                intensities and associated quantities. See
;                routine rd_lines for details.
;
;       ABN      Structure containing abundance data. See gabund.pro
;                for details.
;
; OPTIONAL INPUTS:
;	None
;
; OUTPUTS:
;       ABNL(,)  Array of indeces linking abn to structure 'obs'
;
; OPTIONAL OUTPUTS:
;	None
;
; KEYWORD PARAMETERS:
;	None
;
; CALLS:
;       None  
;
; SIDE EFFECTS:
;       None
;
; CATEGORY:
;	Adas support and applications.
;	
; WRITTEN:
;       Alessandro Lanzafame, University of Strathclyde, mar20-95
;
; MODIFIED:
;	1.0	Alessandro Lanzafame
;		First version
;	1.1	Tim Hammond
;		Put under S.C.C.S. control
;	1.2	William Osborn
;		Removed spaces from before macros
;	1.3	Alessandro Lanzafame
;		Subtituting the array of structures with array of indeces
;		Removed include statement @params
;
; VERSION:
;	1.0	20-03-95
;	1.1	03-05-96
;	1.2	07-05-96
;	1.3	02-02-99
;
;-
;-----------------------------------------------------------------------------

PRO link_abund,obs,abn,abnl,MAXBLND,MTRA

;;;;    abund = dblarr(MAXBLND)

;    abnl = intarr(MAXBLND,MTRA)
    abnl = intarr(MTRA,MAXBLND)

    print,'recognised abundance for      abundance'

    for l=0, n_elements(obs) - 1 do begin
        for k=0, obs(l).hmib - 1 do begin
            for j=0,abn.nabund-1 do begin
                if STRUPCASE(obs(l).delem(k)) eq 			$
		STRUPCASE(abn.symbamb(j)) then begin
                    abnl(l,k) = j
   
                    print,format='(2x,a2,a1,i2,1x,f12.2,1x,e12.3)',$
                    obs(l).delem(k),'+',obs(l).izi(k),$
                    obs(l).swvlen(k),$
                    abn.eabund(abnl(l,k))

                    goto, RECOGNIZED
                endif
            endfor

            print,' I dont recognise ',obs(l).delem(k),' as one of the elements',$
                  ' that i know the abundance of.'
RECOGNIZED:
        endfor
    endfor

END
