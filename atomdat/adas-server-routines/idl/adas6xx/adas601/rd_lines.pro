; Copyright (c) 1996, Strathclyde University.
;+
; PROJECT:
;       ADAS support programs
;
; NAME:
;	RD_LINES
;
; PURPOSE:
;	Read SOHO-CDS observed intensities data file for DEM analysis
;       package.
;
; EXPLANATION:
;       Reads in data file in the format specified in 
;       /ADAS/adas/arch601/intensity/intensity.format. It gives the
;       possibility of having blank lines or comment lines at any 
;       position in the file and to treat blends of lines.
;
;       Lines beginning with 3 spaces, any of the symbols
;
;            '0' '*' 'c' 'C' '=' '-' 'x' 'X' ' X' '|' '-1' 
;
;       or having a negative G-index (see below the OUTPUTS session)
;       are treated as comments or blanks.
;
;       If the keyword ABUNDANCE is set, then lines beginning with 'a'
;       are flagged for the abundance analysis. The output structure 
;       contains lines flagged for the integral inversion (positive 
;       G-index) plus lines flagged with 'a' in the first column.
;       In this case, G-indx are than converted to negative to
;       simplify the linking with kernels (see abundance_analysis.pro)
;
;       Lines can be lumped together in a blend by specifing a
;       blend index, unique for lines belonging to the same blend.
;       This is relevant to observations of unresolved
;       components of the same multiplet and is done to allow a 
;       subsequent link with contribution functions for the single 
;       components.
;
;       The blending can be extended to lines resolved by the
;       instrument, in which case the observed intensity of lines
;       belonging to the same blend are summed and the
;       uncertainties combined. Obviously, lines having a wide 
;       difference in wavelengths or conditions of formation should
;       not be lumped in the same blend.
;
;       Data is organised in an array of structures, each structure
;       containing data for a single blend. 
;
; USE:
;       From the IDL prompt or within a program
;
;               rd_lines,'file.dat',observed
;
;       will return the array of structures 'observed' containing
;       data read from 'file.dat'
;
; INPUTS:
;       INTFILE = Observed intensity data file name.
;
; OPTIONAL INPUTS:
;	None
;
; OUTPUTS:
;       OBSERVED() = Array of structure containing the observational
;                    quantities. Each structure in the array contains
;                    data for one blend of lines, which may consists
;                    of a single non-blended line.
;
;                  IBLND    Blend index. It can be not equal to the
;                           index of the array of structures itself
;
;                  G00      Total intensity of blend
;
;                  SG00     Uncertainty in the intensity measure of blend
;
;                  HMIB     Number of lines in blend
;
;                  DELEM()  Chemical element for each line in the
;                           blend
;
;                  ION()    Ion charge: roman numeral in 
;                           spectroscopic notation.
;
;                  IZI()    Ion charge of the element
;
;                  SWVLEN() Spectroscopic line wavelength
;
;                  REFSW()  Single character for spectroscopic line
;			    wavelength reference
;
;                  OWVLEN() Observed wavelwngth
;
;                  TRCONF() Configuration of levels
;
;                  INDX()   Line G-index, used to link with
;                           contribution function 
;
; OPTIONAL OUTPUTS:
;	None
;
; KEYWORD PARAMETERS:
;	ABUNDANCE If set,lines beginning with 'a' are flagged
;                 for the abundance analysis. The output structure 
;                 contains lines flagged for the integral inversion 
;                 (positive G-index) plus lines flagged with 'a' in 
;                 the first column. 
;
; CALLS:
;       roman2z   Converts the spectroscopic symbol for the ion 
;                 charge (roman numeral) into ion charge (arabic
;                 numeral)
;
;       z2roman   Converts the ion charge into spectroscopic symbol for
;                 the ion charge (roman numeral)
;  
;
; SIDE EFFECTS:
;       None known
;
; CATEGORY:
;	Adas support and applications.
;	
; WRITTEN:
;       Alessandro Lanzafame, University of Strathclyde
;
; MODIFIED:
;	1.0	Alessandro Lanzafame
;		First Release
;	1.1	William Osborn
;		Put under SCCS control
;	1.2	Alessandro Lanzafame, David Brooks
;		Added string trimming in notation conversion of ion charge
;       1.3     Alessandro Lanzafame
;               Added possibility of reference (a single character - REFSW)
;               for spectroscopic wavelength reference
;	1.4	Alessandro Lanzafame
;		Removed include statement @params
;		Read data with negative indx indicative of lines not 
;		included in the integral inversion for which the
;		observed intensity must be compared with prediction
;		from DEM
;       1.5     Alessandro Lanzafame. 
;               a) Bug fixed:
;               Substituted STRLEN to STRTRIM in setting g00 and sg00.
;               This may cause errors with IDL 5.5
;
;               b) Introduced UNITS description in structure OBSERVED
;               Default intensity units description is set to 
;               ERGS/SEC/CM^2/STERADIANS until an escape sequence
;               like:
;
;                    \UNITS=PHOTONS/SEC/CM^2/ARCSEC^2
;
;               is found in the input file, which sets the intensity
;               units description to this value until it is changed
;               again or the end of file is reached.
;       1.6     Alessandro Lanzafame
;               Added option for abundance analysis through keyword
;               ABUNDANCE. This was previously done by a separate
;               version of this procedure called rd_lines_abund.pro
;
; VERSION:
;	1.0	14-03-96
;	1.1	07-05-96
;	1.2	24-02-97
;       1.3     25-01-99
;	1.4	02-02-99
;       1.5     08-06-02
;       1.6     02-08-02
;
;-
;-----------------------------------------------------------------------------

pro rd_lines,intfile,observed,MAXBLND,MTRA,ABUNDANCE=ABUNDANCE

;-----------------------------------------------------------------------

mblnd = intarr(MTRA)  ;___blend index array
hmib  = lonarr(MTRA)  ;___hmib(i) = # of lines in ith blend

stblnd = REPLICATE( {   obs                                  ,$
     	                iblnd  : 0		   	     ,$
                        units  : 'ERGS/SEC/CM^2/STERADIANS'  ,$
			g00    : 0.d0			     ,$
			sg00   : 0.d0			     ,$
			hmib   : 0			     ,$
			delem  : strarr(MAXBLND)	     ,$
			ion    : strarr(MAXBLND) 	     ,$
			izi    : lonarr(MAXBLND)	     ,$
			swvlen : fltarr(MAXBLND)	     ,$
      			refsw  : strarr(MAXBLND)	     ,$
                        owvlen : fltarr(MAXBLND)             ,$
                        trconf : strarr(MAXBLND)             ,$
			indx   : lonarr(MAXBLND)	   } , MTRA)

openr,lu,intfile,/get_lun

m=0       ;___line counter
nblnd=0   ;___blend counter

units=''

while not eof(lu) do begin

;-----------------------------------------------------------------------
;   Trap blanks and comments
;-----------------------------------------------------------------------
   string= ''
   readf,lu,format='(a132)',string
   if strlen(strtrim(string))    eq 0     then goto, BLANK
   if strlen(strmid(string,0,3)) eq 0     then goto, BLANK
   if strmid(string,0,3)         eq '   ' then goto, BLANK
   if strmid(string,0,1)         eq '*'   then goto, BLANK
   if strmid(string,0,1)         eq 'c'   then goto, BLANK
   if strmid(string,0,1)         eq 'C'   then goto, BLANK
   if strmid(string,0,1)         eq '='   then goto, BLANK
   if strmid(string,0,1)         eq '-'   then goto, BLANK
   if strmid(string,0,1)         eq 'X'   then goto, BLANK
   if strmid(string,1,1)         eq 'X'   then goto, BLANK
   if strmid(string,0,1)         eq 'x'   then goto, BLANK
   if strmid(string,0,1)         eq '0'   then goto, BLANK
   if strmid(string,0,1)         eq '|'   then goto, BLANK
   if strmid(string,0,2)         eq '-1'  then goto, BLANK

;-----------------------------------------------------------------------
;   Trap escape sequence for UNITS description
;-----------------------------------------------------------------------

   if strmid(string,0,1) eq '\' then begin
       string=STRUPCASE(string)
       if STRPOS(string,'UNITS') ne -1 then begin
           units=STRMID(string,STRPOS(string,'=')+1)
       endif
       goto,BLANK
   endif


;-----------------------------------------------------------------------
;  Read in quantities from string
;-----------------------------------------------------------------------

;___G-indx
   if long(strmid(string,78,4)) eq 0     then goto, BLANK
   if keyword_set(ABUNDANCE) then begin
       if long(strmid(string,78,4)) le 0 and $
         strmid(string,0,1) ne 'a' then goto, BLANK
       indx  = -abs( long(strmid(string,78,4)) )       
   endif else begin
       indx  = long(strmid(string,78,4))
   endelse

;___blnd-indx

   iblnd = long(strmid(string,83,4))

;___element
   delem  = strmid(string,1,2)

;___ion charge 
   if strmid(string,3,1) eq '+' then begin
      izi = long(strmid(string,4,8))
      ion = z2roman(izi)
   endif else begin
      ion = strtrim(strmid(string,4,8),2)
      izi = roman2z(ion)
   endelse

;___wavelength
   refsw  = strmid(string,13,1)
   swvlen = float(strmid(string,14,11))
;   swvlen = float(strmid(string,13,12))
   if strlen(strtrim(strmid(string,88,12),2)) ne 0 then begin
      owvlen = float(strmid(string,88,12))
   endif else begin
      owvlen = 0.0
   endelse

;___intensity and uncertainty
   if strlen(strtrim(strmid(string,101,12),2)) ne 0 then begin
      g00 = float(strmid(string,101,12))
   endif else begin
      g00 = 0.0
   endelse
   if strlen(strtrim(strmid(string,114,12),2)) ne 0 then begin
      sg00  = float(strmid(string,114,12))
   endif else begin
      sg00 =0.0
   endelse

;___configurations
   trconf = strmid(string,26,51)

;-----------------------------------------------------------------------
;   group blends and store into structures
;-----------------------------------------------------------------------

;___no blend
   if iblnd eq 0 then begin

      stblnd(nblnd).iblnd     = iblnd
      stblnd(nblnd).g00       = g00
      stblnd(nblnd).sg00      = sg00
      stblnd(nblnd).units     = units
      stblnd(nblnd).delem(0)  = delem
      stblnd(nblnd).ion(0)    = ion
      stblnd(nblnd).izi(0)    = izi
      stblnd(nblnd).swvlen(0) = swvlen
      stblnd(nblnd).refsw(0)  = refsw
      stblnd(nblnd).owvlen(0) = owvlen
      stblnd(nblnd).trconf(0) = trconf
      stblnd(nblnd).indx(0)   = indx

      hmib(nblnd) = hmib(nblnd) + 1      ;___augment no of lines
      stblnd(nblnd).hmib = hmib(nblnd)   ;   in blend

      nblnd = nblnd + 1                  ;___augment blend counter

;__blend
   endif else begin

      i = where (mblnd eq iblnd)        ;___identify blend structure
                                        ;   index

;      print,'line ',m,' iblnd ',iblnd,' i ',i

      if i(0) gt -1 then begin          ;___if not first in blend

         if n_elements(i) eq 1 then begin
            ii=i(0) 
            mm = ii
         endif else begin
            print,'***************************************'
            print,'***         RD_LINES ERROR:         ***'
            print,'***     ARRAY OF BLEND INDECES      ***'
            print,'*** HAVE AT LEAST TWO EQUAL INDECES ***'
            print,'***************************************'
            print,'***       PRINTING DIAGNSTICS:      ***'
            print,'***************************************'
            print,'  index    mblnd'
            for k=0,n_elements(mblnd) do print, k, mblnd(k)
            print,'***************************************'
            goto, ERROR
         endelse

;___store data into structure

         if stblnd(mm).iblnd ne iblnd then begin
            print,'***********************************************'
            print,'***            RD_LINES ERROR:              ***'
            print,'*** ATTEMPT TO ASSIGN DATA TO WRONG ELEMENT ***'
            print,'***         IN ARRAY OF STRUCTURES          ***'
            print,'***********************************************'
            print,'***           PRINTING DIGNOSTICS:          ***'
            print,'***********************************************'
            print,' blend index:     ',iblnd
            print,' blend index in array: ',stblnd(mm).iblnd
            print,' array element:        ',mm
            print,'***********************************************'
            goto, ERROR
         endif

         stblnd(mm).g00      = stblnd(mm).g00 + g00 
         stblnd(mm).sg00     = SQRT(stblnd(mm).sg00^2 + sg00^2)

         stblnd(mm).delem(hmib(mm))  = delem
         stblnd(mm).ion(hmib(mm))    = ion
         stblnd(mm).izi(hmib(mm))    = izi
         stblnd(mm).swvlen(hmib(mm)) = swvlen
         stblnd(mm).refsw(hmib(mm))  = refsw
         stblnd(mm).owvlen(hmib(mm)) = owvlen
         stblnd(mm).trconf(hmib(mm)) = trconf
         stblnd(mm).indx(hmib(mm))   = indx

         hmib(mm) = hmib(mm) + 1            ;___augment no of lines
         stblnd(mm).hmib = hmib(mm)         ;   in blend

      endif else begin                      ;___if first

         mblnd(nblnd) = iblnd         
         mm = nblnd           

;___store data into structure

         stblnd(mm).iblnd    = iblnd
         stblnd(mm).g00      = g00 
         stblnd(mm).sg00     = sg00

         stblnd(mm).units    = units
	    
         stblnd(mm).delem(hmib(mm))  = delem
         stblnd(mm).ion(hmib(mm))    = ion
         stblnd(mm).izi(hmib(mm))    = izi
         stblnd(mm).swvlen(hmib(mm)) = swvlen
         stblnd(mm).refsw(hmib(mm))  = refsw
         stblnd(mm).owvlen(hmib(mm)) = owvlen
         stblnd(mm).trconf(hmib(mm)) = trconf
         stblnd(mm).indx(hmib(mm))   = indx

         hmib(mm) = hmib(mm) + 1        ;___augment no of lines
         stblnd(mm).hmib = hmib(mm)     ;   in blend

         nblnd = nblnd + 1              ;___if first line in
                                           ;   blend update
                                           ;   blend counter
      endelse

   endelse

   m = m + 1                      ;___update line counter

   goto,NOTBLANK

BLANK:
;   print,'BLANK:    ',strlen(strtrim(string)),' ',string
   goto, ENDCHECK

NOTBLANK:
;   print,'NOTBLANK: ',strlen(strtrim(string)),' ',string

ENDCHECK:

endwhile

;print, mblnd

;-----------------------------------------------------------------------
;  Return only non empty structures in the array
;-----------------------------------------------------------------------

observed = stblnd(0:nblnd-1)

ERROR:

;-----------------------------------------------------------------------
close,lu
free_lun,lu

end
