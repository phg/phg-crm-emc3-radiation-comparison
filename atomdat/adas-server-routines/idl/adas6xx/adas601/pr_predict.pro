; Copyright (c) 1996, Strathclyde University.
; SCCS info: Module @(#)$Header: /home/adascvs/idl/adas6xx/adas601/pr_predict.pro,v 1.1 2004/07/06 14:36:30 whitefor Exp $ Date $Date: 2004/07/06 14:36:30 $
;+
; PROJECT:
;       ADAS support and application programs
;
; NAME:
;       PR_PREDICT
; NOTE:
;	This routine is not yet properly annotated
;
; PURPOSE:
;
; EXPLANATION:
;
; USE:
;
; INPUTS:
;       None
;
; OPTIONAL INPUTS:
;       None
;
; OUTPUTS:
;       None
;
; OPTIONAL OUTPUTS:
;       None
;
; KEYWORD PARAMETERS:
;       None
;
; CALLS:
;       None
;
; SIDE EFFECTS:
;
; CATEGORY:
;       Adas support.
;
; WRITTEN:
;       Alessandro Lanzafame, University of Strathclyde, unknown
;
; MODIFIED:
;       1       Alessandro Lanzafame
;               First release.
;       1.1     William Osborn
;               Put under S.C.C.S. Control
; VERSION:
;       1       unknown
;       1.1     12-07-96
;
;-----------------------------------------------------------------------
;-
pro pr_predict,obs,demdat,gofts,predint,rl,sigma,H,Hmax

items=strarr(gofts.ntra)

rss=0.0
nset=0

print
print,"Constant variance =   ",sigma
print,"Smoothing parameter = ",rl
print

print,"       Ion      Sp. Wav.     Ir       Sum[H(i,*)]     ",$
      "Io        IoUnc    (Ir-Io)^2/Var Iblnd "  

for i=0,gofts.ntra-1 do begin
   for l = 0, n_elements(obs) - 1 do begin
      for k = 0, obs(l).hmib - 1 do begin 

         if STRUPCASE(obs(l).delem(k)) eq STRUPCASE(gofts.elemc(i)) and $
         obs(l).izi(k) eq gofts.izi(i) and $
         obs(l).indx(k) eq gofts.indx(i) then begin

            prunc=total(H(i,*)) / Hmax / 12.56

            print,(i+1),gofts.elemc(i),'+',gofts.izi(i),$
                  gofts.swl(i),$
                  predint(i),prunc,$
                  obs(l).g00,sqrt(sigma)*demdat.sg(l)/12.56,$
                  (12.56*predint(i)/demdat.sg(l) - $
                   12.56*obs(l).g00/demdat.sg(l))^2 / sigma, $
                  obs(l).iblnd,$
                  format='(1x,i3,2x,a2,a1,i2,1x,f12.4,5(1x,e11.3),3x,i3)'

            items(i)=STRING((i+1),gofts.elemc(i),'+',gofts.izi(i),$
                  gofts.swl(i),$
                  predint(i),prunc,$
                  obs(l).g00,sqrt(sigma)*demdat.sg(l)/12.56,$
                  (12.56*predint(i)/demdat.sg(l) - $
                   12.56*obs(l).g00/demdat.sg(l))^2 / sigma, $
                  obs(l).iblnd,$
                  format='(1x,i3,2x,a2,a1,i2,1x,f12.4,5(1x,e11.3),3x,i3)')

            rss = rss + $
                  (12.56*obs(l).g00/demdat.sg(l) - $
                   12.56*predint(i)/demdat.sg(l))^2 / $
                  sigma
            nset=nset+1
            goto,EXIT            

         endif

      endfor
   endfor
   print,(i+1),gofts.elemc(i),'+',gofts.izi(i),$
         gofts.swl(i),predint(i),$
         format='(1x,i3,2x,a2,a1,i2,1x,f12.4,1x,e11.3)'

   items(i)=STRING((i+1),gofts.elemc(i),'+',gofts.izi(i),$
         gofts.swl(i),predint(i),$
         format='(1x,i3,2x,a2,a1,i2,1x,f12.4,1x,e11.3)')

EXIT:
endfor
print
print,"Chisquare = ",rss
print,"Number of transition used for the integral inversion = ",nset 

if rss gt float(nset) then begin
   print,"WARNING: THEORETICAL DATA DO NOT FALL WITHIN ", $
         "ESTIMATED EXPERIMENTAL ERRORS"
   print
endif


headings = [$     
     '------------------------------------------------------'+$
     '---------------------------------------',$
     '       Ion      Sp. Wav.     Ir       Sum[H(i,*)]     '+$
     'Io        IoUnc    (Ir-Io)^2/Var Iblnd ',$
     '------------------------------------------------------'+$
     '---------------------------------------'$
      ]

      displaytit='Integral inversion results'
      toptitle='ADAS601 OUTPUT'
      footmessage=''
;      ['Constant variance =   '+string(sigma),$
;     'Smoothing parameter = '+string(rl),$
;     'Chisquare = '+string(rss),$
;     'Number of transition used for the integral inversion = '+string(nset)]
      action = -1

      cw_display_list, headings, items, action, 		$
                       font=font, yoffset=50,		$
		       title=displaytit, toptitle=toptitle,	$
		       footmessage=footmessage

end
