; Copyright (c) 1995, Strathclyde University.
; Alessandro Lanzafame  acl@phys.strath.ac.uk    21 November 1995
; SCCS info: Module @(#)dem_in.pro  1.1 Date 05/03/96
;+
; PROJECT:
;       ADAS support and application
;
; NAME:
;   dem_in
;
; PURPOSE:
;   Select input files
;
; EXPLANATION:
;
; USE:
;
; INPUTS:
;
;   INPFLS  - A structure containing old input file names.
;
;   VALUE - A structure which determines the initial settings of
;       the dataset selection widget.  The initial value is
;       set in adas506.pro.  VALUE is passed un-modified
;       through to cw_adas_in.pro, see that routine for a full
;       description.
;
; OPTIONAL INPUTS:
;   None
;
; OUTPUTS:
;
;   INPFLS  - On output the structure containing the new input
;       file names
;
;   VALUE - On output the structure records the final settings of
;       the dataset selection widget if the user pressed the
;       'Done' button otherwise it is not changed from input.
;
;   REP   - String; Indicates whether the user pressed the 'Done'
;       or 'Cancel' button on the interface.  The action is
;       converted to the strings 'NO' and 'YES' respectively.
;
; OPTIONAL OUTPUTS:
;   None
;
; KEYWORD PARAMETERS:
;   FONT  - Supplies the font to be used for the interface widgets.
;
; CALLS:
;   ADAS_IN     Pops-up the dataset selection widget.
;
; SIDE EFFECTS:
;
;
; CATEGORY:
;   Adas support and application.
;
; WRITTEN:
;       Alessandro Lanzafame, University of Strathclyde
;
; MODIFIED:
;   1.0   Alessandro Lanzafame
;     First version
;   1.1   Tim Hammond
;     Put under S.C.C.S. control
;   1.2   Alessandro Lanzafame
;        Adapted for use with Windows
;
; VERSION:
;   1.0   21-11-95
;   1.1   03-05-96
;   1.2   25-07-04
;
;-
;-----------------------------------------------------------------------------


PRO dem_in, inpfls, value, rep, title, head, FONT=font, SEL=sel

;-----------------------------------------------------------------------
;     Set defaults for keywords
;-----------------------------------------------------------------------

IF NOT (KEYWORD_SET(font)) THEN font = ''

;-----------------------------------------------------------------------
;     Set up specific defaults
;-----------------------------------------------------------------------
if !version.os eq 'Win32' then begin
   if sel eq 'abundance' then begin
      oval = {  ROOTPATH:value.rootpath+'\arch601\abundance\', $
                FILE:'', $
                CENTROOT:value.centroot+'\arch601\abundance\', $
                USERROOT:value.userroot+'\arch601\abundance\' }
   endif else if sel eq 'intensity' then begin
      oval = {  ROOTPATH:value.rootpath+'\arch601\intensity\', $
                FILE:'', $
                CENTROOT:value.centroot+'\arch601\intensity\', $
                USERROOT:value.userroot+'\arch601\intensity\' }
   endif else if sel eq 'kernel' then begin
      oval = {  ROOTPATH:value.rootpath+'\arch601\kernel\', $
                FILE:'', $
                CENTROOT:value.centroot+'\arch601\kernel\', $
                USERROOT:value.userroot+'\arch601\kernel\' }
   endif
   endif else begin
   if sel eq 'abundance' then begin
      oval = {  ROOTPATH:value.rootpath+'/arch601/abundance/', $
                FILE:'', $
                CENTROOT:value.centroot+'/arch601/abundance/', $
                USERROOT:value.userroot+'/arch601/abundance/' }
   endif else if sel eq 'intensity' then begin
      oval = {  ROOTPATH:value.rootpath+'/arch601/intensity/', $
                FILE:'', $
                CENTROOT:value.centroot+'/arch601/intensity/', $
                USERROOT:value.userroot+'/arch601/intensity/' }
   endif else if sel eq 'kernel' then begin
      oval = {  ROOTPATH:value.rootpath+'/arch601/kernel/', $
                FILE:'', $
                CENTROOT:value.centroot+'/arch601/kernel/', $
                USERROOT:value.userroot+'/arch601/kernel/' }
   endif
endelse

;-----------------------------------------------------------------------
;     Pop-up input file widget
;-----------------------------------------------------------------------

if !version.os eq 'Win32' then begin
   tfile=DIALOG_PICKFILE(PATH=oval.userroot, /read)
   if tfile ne '' then begin
      action='Done'
      strind=strsplit(tfile,'\',length=ilen)
      lastpos=n_elements(strind)-1
      oval.file=strmid(tfile,strind[lastpos],ilen[lastpos])
      oval.rootpath=strmid(tfile,0,strind[lastpos])
    endif else action=''
endif else begin
   adas_in, oval, action, WINTITLE = title, $
            TITLE = head+' '+sel, FONT = font
endelse

;-----------------------------------------------------------------------
;     Act on the event from the widget
;-----------------------------------------------------------------------

if action eq 'Done' then begin
   rep = 'NO'
endif else begin
   rep = 'YES'
endelse

;-----------------------------------------------------------------------
;     Construct datatset name
;-----------------------------------------------------------------------

if sel eq 'abundance' then begin
   if strlen(strtrim(oval.file,2)) ne 0 then begin
      inpfls.abundance = oval.rootpath+oval.file
   endif else begin
      inpfls.abundance = ''
   endelse
endif else if sel eq 'intensity' then begin
   if strlen(strtrim(oval.file,2)) ne 0 then begin
      inpfls.intensity = oval.rootpath+oval.file
   endif else begin
      inpfls.intensity = ''
   endelse
endif else if sel eq 'kernel' then begin
   if strlen(strtrim(oval.file,2)) ne 0 then begin
      inpfls.kernel = oval.rootpath+oval.file
   endif else begin
      inpfls.kernel = ''
   endelse
endif else begin
   print,'*** DEM_IN.PRO ERROR   ***'
   print,'*** SEL NOT RECOGNISED ***'
endelse


END
