; Copyright (c) 1996, Strathclyde University.
; SCCS info: Module @(#)$Header: /home/adascvs/idl/adas6xx/adas601/predict_intensity.pro,v 1.4 2004/07/06 14:36:42 whitefor Exp $	Date $Date: 2004/07/06 14:36:42 $
;+
; PROJECT:
;       ADAS support programs
;
; NAME:
;       PREDICT_INTENSITY
;
; PURPOSE:
;       Given a set of contribution functions and a differential
;       emission measure function this procedure calculates the
;       predicted line of sight intensities.
;
;
; EXPLANATION:
;       This procedure evaluates the integral which defines the differential
;       emission measure to predict line of sight intensities. The DEM
;       is assumed to be evaluated in advance using a subset of spectral
;       lines for which the contribution function is given. The predicted
;       line of sight intensity is evaluated for transitions
;       listed in the collection of contribution function G(T) and whose
;	G-index is flagged as negative in the intensity input file.
;
; USE:
;       The statement 
;
;               predict_intensity,abn,obs,gofts,demdat,predint
;
;       gives the predicted line of sight intensities in predint.
;
; INPUTS:
;       ABN   =      Structure containing abundance data.
;
;                  NABUND    Number of elements contained in the data file
;
;                  SYMBAMB() Symbol of elements listed in the data file
;
;                  EABUND()  Abundance for element 
;
;	ABNL  =   Array of pointing indeces linking ABN with obs
;
;       OBSERVED() = Array of structure containing the observational
;                    quantities. Each structure in the array contains
;                    data for one blend of lines, which may consists
;                    of a single non-blended line.
;
;                  IBLND    Blend index. It can be not equal to the
;                           index of the array of structures itself
;
;                  G00      Total intensity of blend
;
;                  SG00     Uncertainty in the intensity measure of blend
;
;                  HMIB     Number of lines in blend
;
;                  DELEM()  Chemical element for each line in the
;                           blend
;
;                  ION()    Ion charge: roman numeral in 
;                           spectroscopic notation.
;
;                  IZI()    Ion charge of the element
;
;                  SWVLEN() Spectroscopic line wavelength
;
;                  OWVLEN() Observed wavelwngth
;
;                  TRCONF() Configuration of levels
;
;                  INDX()   Line G-index, used to link with
;                           contribution function 
;
;       GOFTS =      Structure containing G(T) functions and associated
;                    quantities. 
;
;                  ELEMC()  Chemical element                  
;
;                  IZI()    Ion charge              
;
;                  INDX()   Transition index
;
;                  NG()     Number of temperature points 
;
;                  AWL()    Approximate wavelength
;
;                  SWL()    Spectroscopic wavelength
;
;                  FILEI()  ADF20 master file from which the function
;                           was extracted (using ADAS 506)
;
;                  T(,)     Log of temperature
;                             1st index: temperature
;                             2nd index: transition
;
;                  GOFT(,)  G(T) function
;                             1st index: temperature
;                             2nd index: transition
;
;	MPRE   =   Number of lines for which  only the predicted
;		   intensity is sought - OBSOLETE - NOT USED
;
;	GOFTSP =   Array of pointing indeces linking gofts with obs
;
;       DEMDAT =     Structure containing the differential emission
;                    measure and associated quantities
;                  NTEMP    Number of temperature points at which the
;                           DEM is calculated
;                  TEMP()   Temperature at which the DEM is calculated
;                  M        Number of transitions used in the calculation
;                           of the DEM
;                  G()      4 pi * intensity of lines used in the DEM
;                           calculation  
;                  SG()     4 pi * uncertainty of lines used in the DEM
;                           calculation  
;                  NG()     Number of temperature points at which the
;                           G(T) function is tabulated
;                  T(,)     Temperature at which G(T) function is tabulated
;                           1st index temperature
;                           2nd index transition
;                  GOFT(,)  Kernel used in the inversion (see run_dem.pro)
;                           1st index temperature
;                           2nd index transition
;                  TM()     Temperature at which kernel is at max
;                  YS()     Crude estimate of the DEM at TM obtained by
;                           apporximating G(T) by
;                              Integral[G(T)dT] * Dirac[T-TM]
;                  FHAT()   DEM
;                  F0()     Spline fit to YS (Data adaptive smoothing
;                           approach
;
; OPTIONAL INPUTS:
;	None
;
; OUTPUTS:
;       PREDINT(,) = vector containing the predicted intensities for lines
;                    flagged with negative G-indeces.
;                    Indeces are as in input structure OBSERVED.
;
; OPTIONAL OUTPUTS:
;	None
;
; KEYWORD PARAMETERS:
;	None
;
; CALLS:
;       None
;
; SIDE EFFECTS:
;       None
;
; CATEGORY:
;	Adas support and applications.
;	
; WRITTEN:
;       Alessandro Lanzafame, University of Strathclyde,  may07-96
;
; MODIFIED:
;	1.0	Alessandro Lanzafame
;		First written
;	1.1	William Osborn
;		Put into S.C.C.S
;       1.2	Alessandro Lanzafame
;		Numerical improvement. Introduced function
;		prior in building up matrix H
;	1.3	Alessandro Lanzafame
;		The array of structures goftsl is substituted by
;		the array of ponting indeces goftsp which points to
;		the elements of array of structures gofts which are
;		not included in the integral inversion and for which
;		an intensity prediction is sought.
;               A separate loop for computing the intensity is
;               avoided.
;		Got rid of include statement @params
;       1.4     Alessandro Lanzafame
;               Corrected comments section.
;               Got rid of redundant input  "mpre".
;               Set zero=1.d-99 to avoid problems if G(T)
;               contains zero values.
;
; VERSION:
;	1.0	07-05-96
;	1.1	12-07-96
;	1.2	21-02-97
;	1.3	03-02-99
;       1.4     15-12-00
;
;-
;-----------------------------------------------------------------------

pro predict_intensity, abn, abnl, obs, gofts, goftsp,$
                       demdat, predint, H, dlam, co, ncap7, MAXBLND

zero=1.d-99

predint=dblarr(n_elements(obs),MAXBLND)
logtemp=alog(demdat.temp(0:demdat.ntemp-1))

;-----------------------------------------------------------------------
; Build up matrix H. 
;
; In the inversion algorithm, H is divided by the line weights and Hmax. 
; These steps are omitted. 
;-----------------------------------------------------------------------

for i = 0, n_elements(obs)-1 do begin
   if obs(i).hmib gt 0 then begin
      for j = 0, obs(i).hmib-1 do begin

         ;___predict line intensity for lines flagged with
         ;___negative indeces 
         if (obs(i).indx(j) lt 0) then begin

         ;___predict line intensity for all lines
         ;if (obs(i).indx(j) ne 0) then begin

            w=gofts.t(0:gofts.ng(goftsp(i,j))-1,goftsp(i,j))
            ;___Build-up kernel vector to form the H matrix
            x=abn.eabund(abnl(i,j))*$
              gofts.goft(0:gofts.ng(goftsp(i,j))-1,goftsp(i,j))*w
            w=alog(w)
            for kk=0,gofts.ng(goftsp(i,j))-1 do begin
               ww=w(kk)
               prr=prior(ww,dlam,co,ncap7)
               x(kk)=x(kk)*prr
               if x(kk) le 0. then x(kk)=zero
            endfor
            ;___Form H matrix
            x=alog(x)
            ;print,i,x,format='(i5,1x,101e10.2)'
            ;wait,5
            HMAT, H, demdat.ntemp, logtemp, w, x, i
            ;___Predicted intensities
            ;print,i,H(i,0:demdat.ntemp-1),format='(i5,1x,101e10.2)'
            ;wait,5
            predint(i,j)=total( H(i,0:demdat.ntemp-1)*$
                    (demdat.fhat(0:demdat.ntemp-1)/$
                     demdat.f0(0:demdat.ntemp-1)) ) / 12.56
         endif
      endfor
   endif
endfor

end

