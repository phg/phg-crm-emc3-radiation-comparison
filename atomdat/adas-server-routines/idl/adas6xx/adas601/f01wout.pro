; Copyright (c) 1996, Strathclyde University.
; SCCS info:
;+
; PROJECT:
;       ADAS support and application programs
;
; NAME:
;       F01WOUT
; NOTE:
;
; PURPOSE:
;   Prints output to file demap_output.dat
;
; EXPLANATION:
;   This routine gathers information on the run and produces a
;   comprehensive output file.
;
; USE:
;   This routine is specific to ADAS601. See demap_main.pro for its use
;
; INPUTS:
;
;       adasrel:  ADAS release string used in header
;
;       adasprog: ADAS601 release string used in header
;
;       inpfls:   Structure which stores input files information -
;                 see demap_main.pro for details
;
;       obs:      Structure which stores the observational data -
;       see rd_lines.pro for details
;
;       demdat:   Structure storing input and outout of integral inversion -
;                 see demap_main.pro and run_dem.pro
;
;       gofts:    Structure storing G(T) functions read from collection file -
;                 see ercoll.pro
;
;   goftsl:     Pointing array linking obs to gofts - lines used in
;       inversion
;
;   goftsp:   Pointing array linking obs to gofts - lines not used in
;       inversion, prediction only
;
;       predint:  Double prescision array giving predicted intensity -
;       - see predict_intensity.pro
;
;       rl:       Smoothing parameter - see run_dem.pro
;
;       sigma:    Constant variance - see run_dem.pro
;
;       H:        H-matrix build up by predict_intensity.pro
;
;       Hmax:     Maximum of matrix H
;
;   iweight    = index selecting the weighting quantity for the
;                  inversion:
;                      0 - use intensity
;                      1 - use square root of intensity
;                      2 - use quantities specified in the intensity
;                          input file (e.g. measurement errors)
;
;       idsrt    = vector of indeces for sorting structure demdat
;                  for increasing temperature of max G
;
;   ldob     = vector of indeces linking demdat to obs (blend)
;
;       ldol     = array of indeces linking demdat to obs (line):
;                  indicated which transitions have been summed up
;                  in the blend
;
;
; OPTIONAL INPUTS:
;       None
;
; OUTPUTS:
;       None
;
; OPTIONAL OUTPUTS:
;       None
;
; KEYWORD PARAMETERS:
;       None
;
; CALLS:
;       None
;
; SIDE EFFECTS:
;
; CATEGORY:
;       Adas support.
;
; WRITTEN:
;       Alessandro Lanzafame, University of Strathclyde, 30 June 1996
;
; MODIFIED:
;       1       Alessandro Lanzafame
;               First release.
;       1.1     William Osborn
;               Put under S.C.C.S. Control
;       1.2     Alessandro Lanzafame
;               Fixed a problem in computing chisquare and improved
;               header
;       1.3     Alessandro Lanzafame
;               (Istituto di Astronomia, Universita' di Catania)
;               Introduced pointing arrays
;               Changed layout
;       1.4     Alessandro Lanzafame
;               Dip.to di Fisica e Astronomia, Universita' di Catania
;               Multiplied line deviation by the constant variance
;               sigma.
;               Revised comments
;       1.5     Alessandro Lanzafame
;               Dip.to di Fisica e Astronomia, Universita' di Catania
;               Fixed bug in printout of blends with no observed intensity
;       1.6     Martin O'Mullane
;                - Fix writing the DEM table when number of temperatues is
;                  less than 100.
;                - Increase the number of decimal palces to 2 for the DEM. 
;
; VERSION:
;       1       30-06-96
;       1.1     12-07-96
;       1.2     26-02-97
;       1.3     17-02-99
;       1.4     13-12-00
;       1.5     01-07-02
;       1.6     15-04-2019
;
;
;-----------------------------------------------------------------------
;-
pro f01wout, adasrel, adasprog, inpfls,          $
             obs, demdat, gofts, goftsl, goftsp, $
             ldob, ldol, idsrt, iweight,     $
             predint, rl, sigma, H, Hmax, rss

conv_factor = 5.034059191e7    ; 1 / (h*c*1.e8)
outfile='demap_output.dat'
openw,lou,outfile,/get_lun

;-----------------------------------------------------------------------
; Write ADAS header
;-----------------------------------------------------------------------
date=xxdate()
header = adasrel+' '+adasprog+' DATE: '+date(0)+' TIME: '+date(1)
printf,lou,header
printf,lou,'******************* '+$
           'TABULAR OUTPUT FROM INTEGRAL INVERSION - '+$
            adasprog+' - DATE: '+date(0)+$
           ' *******************'
printf,lou

;-----------------------------------------------------------------------
; Write information on input files for run
;-----------------------------------------------------------------------

printf,lou,'INTENSITY DATA FILE NAME: '+inpfls.intensity
printf,lou,'   KERNEL DATA FILE NAME: '+inpfls.kernel
printf,lou,'ABUNDANCE DATA FILE NAME: '+inpfls.abundance
printf,lou

printf,lou
printf,lou,'  CONSTANT VARIANCE = ',sigma
printf,lou,'SMOOTHING PARAMETER = ',rl
printf,lou

printf,lou,'----------------------------------------'+$
           '--------- SINGLE LINES ---------'+$
           '-------------------------------------------'
printf,lou,' ION   SPECTROSCOP. PEAK    RECONSTR.  OBSERVED           '+$
           'RECONSTR.   OBSERVED'
printf,lou,'        WAVELENGTH  TEMP    INTENSITY  INTENSITY   QFR    '+$
           'INTENSITY   INTENSITY    EPS         SIG         DEV'
printf,lou,'                    log(T)  erg/cm2/s  erg/cm2/s          '+$
           'ph/cm2/s    ph/cm2/s'
printf,lou,'------------------------------------------'+$
           '--------------------------------------------------'+$
           '------------------------'

for i=0,demdat.m-1 do begin
   l=idsrt(i)
   j=ldob(l)
   if (obs(j).hmib eq 1) then begin
      k=ldol(l,0)
      printf,lou,obs(j).delem(k),'+',obs(j).izi(k),$
            obs(j).swvlen(k),$
            alog10(demdat.tm(l)),$
            demdat.irec(l)/conv_factor/obs(j).swvlen(k),$
            obs(j).g00/conv_factor/obs(j).swvlen(k),$
            demdat.irec(l)/obs(j).g00,$
            demdat.irec(l), $
            obs(j).g00, $
            demdat.eps(l), $
            demdat.sig(l), $
            demdat.dev(l)*sigma, $
            format='(1x,a2,a1,i2,1x,f11.3,1x,f5.2,2(1x,f11.3),1x,f6.3,5(1x,e11.3))'
   endif
endfor

printf,lou,'----------------------------------------'+$
           '--------------------------------------------------'+$
           '------------------------'

printf,lou
printf,lou
printf,lou

printf,lou,'---------------------------------------------------- '+$
           'BLENDS '+$
           '--------------------------------------------------------------'
printf,lou,' ION   SPECTROSCOP. PEAK          RECONSTR.  OBSERVED           '+$
           'RECONSTR.   OBSERVED'
printf,lou,'        WAVELENGTH  TEMP   Iblnd  INTENSITY  INTENSITY   QFR    '+$
           'INTENSITY   INTENSITY    EPS         SIG         DEV'
printf,lou,'                    log(T)        erg/cm2/s  erg/cm2/s          '+$
           'ph/cm2/s    ph/cm2/s'
printf,lou,'----------------------------------------------------------------'+$
           '----------------------------------------------------------'

for i=0,demdat.m-1 do begin
   m=idsrt(i)
   j=ldob(m)
   if (obs(j).hmib gt 1) then begin
      ;___print blend components
      for l=0,obs(j).hmib-1 do begin
         k=ldol(m,l)
         printf,lou,$
               obs(j).delem(k), '+', $
               obs(j).izi(k), $
               obs(j).swvlen(k),$
               alog10(demdat.tm(m)), $
               format='(1x,a2,a1,i2,1x,f11.3,1x,f5.2)'
      endfor
      ;___print fluxes
      printf,lou,$
            ' - SUM - ',$
            obs(j).iblnd, $
            demdat.irec(idsrt(i))/conv_factor/obs(j).swvlen(0), $
            obs(j).g00/conv_factor/obs(j).swvlen(0), $
            demdat.irec(idsrt(i))/obs(j).g00, $
            demdat.irec(l), $
            obs(j).g00, $
            demdat.eps(m), $
            demdat.sig(m), $
            demdat.dev(m)*sigma, $
            format='(5x,a9,13x,i3,2(1x,f11.3),1x,f6.3,5(1x,e11.3))'
   endif
endfor
printf,lou,'------------------------------------------------------'+$
           '--------------------------------------------------------------------'

printf,lou
printf,lou,'--------------------'+$
           '--------------------------------'+$
           '--------------------'
printf,lou
printf,lou,'CHISQUARE = ',rss
printf,lou,'NUMBER OF TRANSITION USED FOR THE INTEGRAL INVERSION = ',demdat.m
printf,lou

if rss gt float(demdat.m) then begin
   printf,lou,"WARNING: CHISQUARE GREATER THAN NUMBER OF TRANSITIONS ", $
         "USED FOR THE INTEGRAL INVERSION"
   printf,lou
endif
printf,lou,'--------------------'+$
           '--------------------------------'+$
           '--------------------'
printf,lou
printf,lou
printf,lou
printf,lou,'--------------------------------'+$
           '---- PREDICTED  INTENSITIES ----'+$
           '--------------------------------'
printf,lou,' ION   SPECTROSCOP. PEAK    RECONSTR.  OBSERVED           '+$
           'RECONSTR.   OBSERVED'
printf,lou,'        WAVELENGTH  TEMP    INTENSITY  INTENSITY   QFR    '+$
           'INTENSITY   INTENSITY    DEV'
printf,lou,'                    log(T)  erg/cm2/s  erg/cm2/s          '+$
           'ph/cm2/s    ph/cm2/s'
printf,lou,'--------------------------------'+$
           '--------------------------------'+$
           '--------------------------------'

for i = 0, n_elements(obs)-1 do begin
   if obs(i).hmib eq 1 then begin
      j=0
      ;___predicted line intensity for lines flagged with negative indeces
      if (obs(i).indx(j) lt 0) then begin
         max_subscript = 0
         Gmax = MAX(gofts.goft(*,goftsp(i,j)),max_subscript)
         TGmax = gofts.t(max_subscript,goftsp(i,j))
         TGmax = alog10(TGmax)
         if (obs(i).g00 ne 0.0) then begin
            case iweight of
              ; multiplied by sigma (06-12-00)
              ; 0 : dev = (predint(i,j) - obs(i).g00)^2 / (sigma*(obs(i).g00)^2)
              ; 1 : dev = (predint(i,j) - obs(i).g00)^2 / (sigma* obs(i).g00)
              ; 2 : dev = (predint(i,j) - obs(i).g00)^2 / (sigma*(obs(i).sg00)^2)
               0 : dev = (predint(i,j) - obs(i).g00)^2 / ((obs(i).g00)^2)
               1 : dev = (predint(i,j) - obs(i).g00)^2 / ( obs(i).g00)
               2 : dev = (predint(i,j) - obs(i).g00)^2 / ((obs(i).sg00)^2)
            endcase
            printf,lou,$
                  obs(i).delem(j), '+', $
                  obs(i).izi(j), $
                  obs(i).swvlen(j), $
                  TGmax, $
                  predint(i,j)/conv_factor/obs(i).swvlen(j), $
                  obs(i).g00/conv_factor/obs(i).swvlen(j), $
                  predint(i,j)/obs(i).g00, $
                  predint(i,j), $
                  obs(i).g00, $
                  dev, $
                  format='(1x,a2,a1,i2,1x,f11.3,1x,f5.2,2(1x,f11.3),1x,f6.3,3(1x,e11.3))'
         endif else begin
            printf,lou,$
                  obs(i).delem(j), '+', $
                  obs(i).izi(j), $
                  obs(i).swvlen(j), $
                  TGmax, $
                  predint(i,j)/conv_factor/obs(i).swvlen(j), $
                  predint(i,j),$
                  format='(1x,a2,a1,i2,1x,f11.3,1x,f5.2,1x,f11.3,20x,e11.3)'
         endelse
      endif
   endif else if obs(i).hmib gt 1 then begin
      ;___predicted line intensity for lines flagged with negative indeces
      if (obs(i).indx(0) lt 0) then begin
         ;___sum predicted intensity for the multiplet
         predmult=predint(i,0:obs(i).hmib-1)
         predmult=total(predmult,2)
         ;___printout components
         printf,lou
         for j = 0, obs(i).hmib-1 do begin
            max_subscript = 0
            Gmax = MAX(gofts.goft(*,goftsp(i,j)),max_subscript)
            TGmax = gofts.t(max_subscript,goftsp(i,j))
            TGmax = alog10(TGmax)
            printf,lou,$
                  obs(i).delem(j), '+', $
                  obs(i).izi(j), $
                  obs(i).swvlen(j), $
                  TGmax, $
                  predint(i,j)/conv_factor/obs(i).swvlen(j), $
                  predint(i,j), $
                  format='(1x,a2,a1,i2,1x,f11.3,1x,f5.2,1x,f11.3,20x,e11.3)'
         endfor
         ;___printout sum
         if (obs(i).g00 ne 0.0) then begin
            case iweight of
              ; multipied by sigma (06-12-00)
              ; 0 : dev = (predmult - obs(i).g00)^2 / (sigma*(obs(i).g00)^2)
              ; 1 : dev = (predmult - obs(i).g00)^2 / (sigma* obs(i).g00)
              ; 2 : dev = (predmult - obs(i).g00)^2 / (sigma*(obs(i).sg00)^2)
               0 : dev = (predmult - obs(i).g00)^2 / ((obs(i).g00)^2)
               1 : dev = (predmult - obs(i).g00)^2 / (obs(i).g00)
               2 : dev = (predmult - obs(i).g00)^2 / ((obs(i).sg00)^2)
            endcase
            printf,lou,$
                   ' - SUM - ',$
                   obs(i).iblnd, $
                   predmult/conv_factor/obs(i).swvlen(0), $
                   obs(i).g00/conv_factor/obs(i).swvlen(0), $
                   predmult/obs(i).g00, $
                   predmult, $
                   obs(i).g00, $
                   dev, $
                   format='(5x,a9,1x,i3,6x,2(1x,f11.3),1x,f6.3,3(1x,e11.3))'
            printf,lou
         endif else begin
            printf,lou,$
                   ' - SUM - ',$
                   obs(i).iblnd, $
                   predmult/conv_factor/obs(i).swvlen(0),$
                   predmult, $
                   format='(5x,a9,1x,i3,7x,f11.3,20x,e11.3)' ; $

            printf,lou
         endelse
      endif
   ;endelse
   endif
endfor

;   if obs(i).hmib gt 0 then begin
;      for j = 0, obs(i).hmib-1 do begin
;         ;___predicted line intensity for lines flagged with
;         ;___negative indeces
;         if (obs(i).indx(j) lt 0) then begin
;            max_subscript = 0
;            Gmax = MAX(gofts.goft(*,goftsp(i,j)),max_subscript)
;            TGmax = gofts.t(max_subscript,goftsp(i,j))
;            TGmax = alog10(TGmax)
;            if (obs(i).g00 ne 0.0) then begin
;               case iweight of
;                  0 : dev = (predint(i,j) - obs(i).g00)^2 / (sigma*(obs(i).g00)^2)
;                  1 : dev = (predint(i,j) - obs(i).g00)^2 / (sigma* obs(i).g00)
;                  2 : dev = (predint(i,j) - obs(i).g00)^2 / (sigma*(obs(i).sg00)^2)
;               endcase
;               printf,lou,$
;                  obs(i).delem(j), '+', $
;                  obs(i).izi(j), $
;                  obs(i).swvlen(j), $
;                  TGmax, $
;                  predint(i,j)/conv_factor/obs(i).swvlen(j), $
;                  obs(i).g00/conv_factor/obs(i).swvlen(j), $
;                  predint(i,j)/obs(i).g00, $
;                  predint(i,j), $
;                  obs(i).g00, $
;                  dev, $
;                  format='(1x,a2,a1,i2,1x,f11.3,1x,f5.2,2(1x,f11.3),1x,f6.3,3(1x,e11.3))'
;            endif else begin
;                  printf,lou,$
;                  obs(i).delem(j), '+', $
;                  obs(i).izi(j), $
;                  obs(i).swvlen(j), $
;                  TGmax, $
;                  predint(i,j)/conv_factor/obs(i).swvlen(j), $
;                  predint(i,j),$
;                  format='(1x,a2,a1,i2,1x,f11.3,1x,f5.2,1x,f11.3,20x,e11.3)'
;            endelse
;         endif
;      endfor
;   endif
;endfor

printf,lou,'--------------------------------'+$
           '--------------------------------'+$
           '--------------------------------'

;-------------------------------------------------------------------------
; DEM printout
;-------------------------------------------------------------------------

printf,lou
printf,lou
printf,lou
printf,lou,'---------------------------------------'+$
           '  DIFFERENTIAL EMISSION MEASURE '+$
           '---------------------------------------'
printf,lou,'   N   TEMP     DEM      N   TEMP     DEM   '+$
           '   N   TEMP     DEM      N   TEMP     DEM   '+$
           '   N   TEMP     DEM'
printf,lou,'---------------------------------------'+$
           '---------------------------------------'+$
           '--------------------------------'
printf,lou
itabdim = 50
remain = demdat.ntemp mod itabdim
if remain eq 0 then begin
   ntables = long(demdat.ntemp/itabdim)
endif else begin
   ntables = long(demdat.ntemp/itabdim) - 1
endelse

ntables = numlines(demdat.ntemp, itabdim)

for itab=0,ntables - 1 do begin
   irmax=min([9,demdat.ntemp-1])
   for ir=0,irmax do begin
      item=''
      icmax=min([4,long((demdat.ntemp-1-ir)/10.0)])
      for ic=0,icmax do begin
         indx= (10*ic + ir) + (itabdim * itab)
         if demdat.temp(indx) GT 0.0 then begin
            item=item+string((indx+1),demdat.temp(indx),demdat.fhat(indx),$
                            format='(1x,i3,2(e9.2))')
         endif
      endfor
      printf,lou,item
   endfor
   printf,lou
endfor
printf,lou,'---------------------------------------'+$
           '---------------------------------------'+$
           '--------------------------------'

;-----------------------------------------------------------------------
; Find and printout temperatures at which G(T) are at maximum and
; mean T. Although the firts quantity have been already computed for
; the lines used in the inversion (run_dem.pro), we need this
; information for all the transitions listed in the collection file, so
; we do it again and compute a proper mean T.
;-----------------------------------------------------------------------


;printf,lou
;printf,lou
;printf,lou
;printf,lou,'----'+$
;           '  TEMPERATURE OF MAXIMUM CONTRIBUTION FUNCTION G(T)'+$
;           '----'
;
;printf,lou,'----'+$
;           '                  AND MEAN TEMPERATURE             '+$
;           '----'
;printf,lou,' ION   SPECTROSCOPIC  APPROXIMATE  TEMPERATURE     MEAN'
;printf,lou,'         WAVELENGTH   WAVELENGTH    OF G-MAX    TEMPERATURE'
;printf,lou,'------------------------------'+$
;           '------------------------------'
;
;for i=0, gofts.ntra - 1 do begin
;
;   T_Gmax = 0.0
;   max_subscript = 0
;   gmax=MAX(gofts.goft(*,i),max_subscript)
;   T_Gmax = gofts.t(max_subscript,i)
;   if T_Gmax le 0 then MESSAGE,'Cannot find temperature of Gmax'
;
;   T_mean = 0.0
;   tt = gofts.t(*,i)
;   indexx = where( tt lt demdat.temp(demdat.ntemp-1) )
;   w=gofts.goft(indexx,i)
;   tt=tt(indexx)
;   indxnoz = where (w gt 0.e+0)
;
;   IG  = INT_TABULATED(tt(indxnoz),w(indxnoz))
;   ITG = INT_TABULATED(tt(indxnoz),(tt(indxnoz)*w(indxnoz)))
;   if IG ne 0.0 then T_mean = ITG / IG
;
;   printf,lou,gofts.elemc(i),'+',gofts.izi(i),$
;         gofts.swl(i),gofts.awl(i),T_Gmax,T_mean,$
;         format='(1x,a2,a1,i2,1x,2(1x,f11.4),2(2x,e11.2))'
;endfor
;
;printf,lou,'------------------------------'+$
;           '------------------------------'

close,lou
free_lun,lou

end
