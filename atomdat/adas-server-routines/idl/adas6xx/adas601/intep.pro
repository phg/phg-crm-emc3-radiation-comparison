; Copyright (c) 1996, Strathclyde University.
; SCCS info: Module @(#)$Header: /home/adascvs/idl/adas6xx/adas601/intep.pro,v 1.1 2004/07/06 14:09:16 whitefor Exp $ Date $Date: 2004/07/06 14:09:16 $
;+
; PROJECT:
;       ADAS support and application programs
;
; NAME:
;       INTEP
;
; NOTE: 
;	This routine is not yet fully annotated
;
; PURPOSE:
;
; EXPLANATION:
; USE:
;
; INPUTS:
;
; OPTIONAL INPUTS:
;
; OUTPUTS:
;
; OPTIONAL OUTPUTS:
;
; KEYWORD PARAMETERS:
;
; CALLS:
; SIDE EFFECTS:
;
; CATEGORY:
;       Adas support.
;
; WRITTEN:
;       Alessandro Lanzafame, University of Strathclyde, unknown
;
; MODIFIED:
;       1       Alessandro Lanzafame
;               First release.
;       1.1     Tim Hammond
;               Put under S.C.C.S. Control
; VERSION:
;       1       unknown
;       1.1     03-05-96
;
;-----------------------------------------------------------------------
;-

PRO intep, x, y, xpin, ypout

;
;  REF: PUBLICATIONS OF THE DOMINION ASTROPHYSICAL OBSERVATORY, XVI,6,67
;       GRAHAM HILL: INTEP, AN EFFECTIVE INTERPOLATION SUBROUTINE
;

    if (n_params(0) ne 4) then begin
        print,'intep, x, y, xp, yp'
        return
    endif
    n = n_elements(x)
    dum = size(xpin)
    if(dum(0) eq 0) then begin
        np = 1
        xp = fltarr(1)
        yp = fltarr(1)
        xp(0) = xpin
    endif else begin
        np = dum(1)
        xp = xpin
        yp = fltarr(np)
    endelse
    
    IER=1
    IO=0
    IUP=0  
    IF(X(1) LT X(0)) then IUP=1
    N1=N-2
    for it=0, np-1 do begin
        IF ((XP(IT) GE X(N-1)) AND (IUP EQ 0)) OR 			$
        ((XP(IT) LE X(N-1)) AND (IUP EQ 1)) THEN begin
g5:         YP(IT)=Y(N-1)
            goto, g6
        endif ELSE begin
            IF ((XP(IT) LE X(0)) AND (IUP EQ 0)) OR 			$
               ((XP(IT) GE X(0)) AND (IUP EQ 1)) THEN begin
                   YP(IT)=Y(0)
g6:                if (xp(it) ne x(0)) and (xp(it) ne x(n-1)) then IER=2
                   GOTO, loop
            ENDIF
        endelse
g8:                        
        for I=IO,N-1 do begin
            IF(XP(IT) LT X(I)) AND (IUP EQ 0) then goto, g2
            IF(XP(IT) GT X(I)) AND (IUP EQ 1) then goto, g2
        endfor
        goto, g5
g2:     I=I-1
        IF(I EQ IO-1) then goto, g4
        IO = I+1                                                           
        LP1 = 1./(X(I)-X(I+1))
        LP2 = 1./(X(I+1)-X(I))
        IF(I EQ 0) then FP1=(Y(1)-Y(0))/(X(1)-X(0))
        IF(I EQ 0) then goto, g3
        FP1 = (Y(I+1)-Y(I-1))/(X(I+1)-X(I-1))
g3:     IF(I GE N1) then FP2=(Y(N-1)-Y(N-2))/(X(N-1)-X(N-2))
        IF(I GE N1) then goto,g4
        FP2 = (Y(I+2)-Y(I))/(X(I+2)-X(I))
g4:     XPI1 = XP(IT) - X(I+1)
        XPI = XP(IT) - X(I)
        L1 = XPI1 * LP1
        L2 = XPI * LP2
        YP(IT) = Y(I)*(1.-2.*LP1*XPI)*L1*L1+Y(I+1)*(1.-2.*LP2*XPI1)* 	$
                 L2*L2+FP2*XPI1*L2*L2+FP1*XPI*L1*L1
        GOTO, loop
loop:
    endfor

result:
    if(dum(0) eq 0) then begin
        ypout=yp(0)
    endif else begin
        ypout=yp
    endelse
    if (ier eq 2) then print,' INTEP warning: xp outside range, '+	$
			     'yp set to end-point value'
    return
END
