; Copyright (c) 1997, Alessandro Lanzafame, Universita' di Catania
; SCCS info: Module @(#)$Header: /home/adascvs/idl/adas6xx/adas601/chebev.pro,v 1.1 2004/07/06 12:04:39 whitefor Exp $ Date $Date: 2004/07/06 12:04:39 $
;+
; PROJECT:
;	Mathematics - utilities
;
; NAME:
;	CHEBEV
;
; NOTE:
;
; PURPOSE: 
;	Chebyshev evaluation
;	REFERENCES: Press, H. W. et al "Numerical Recipes", 1992
;
; EXPLANATION:
;	See reference
;
; USE:
;	x = chebev(a,b,c,m,x)
;
;	returns evaluation of Chebyshev series in x. See below
;       for explanation of other inputs
;
; INPUTS:
;	(R*8)     A   = Lower limit of the abscissa interval
;
;	(R*8)     B   = Upper limit of the abscissa interval
;
;	(R*8)     C() = Array of Chebyshev coefficients
;
;	(I*4)     M   = Order of the series (maximum degree of 
;                       polynomial expansion = M-1)
;
;	(R*8)	  X   = Abscissa
;
; OPTIONAL INPUTS:
;	None
;
; OUTPUTS:
; 	This function returns the evaluation of Chebyshev series of
;	order m at the abscissa x
;
; OPTIONAL OUTPUTS:
;	None
;
; KEYWORD PARAMETERS:
;	None
;
; CALLS:
;	None
;
; SIDE 	EFFECTS:
;	None
;
; CATEGORY:
;	Maths - general use
;
; WRITTEN:
;       A. C. Lanzafame, Istituto di Astronomia
;                        Universita' di Catania
;                        Citta' Universitaria
;                        Viale Andrea Doria, 6
;                        I-95125 Catania, Italy
;                        Tel: +39 95 7332 240
;                        Fax: +39 95 330592
;                        e-mail: alanzafame@ct.astro.it
; MODIFIED:
;	1.0 	Alessandro Lanzafame
;		First version
;
; VERSION:
;	1.0	18-02-97
;
;-

FUNCTION CHEBEV, A, B, C, M, X

;-----------------------------------------------------------------------

      IF ( (X-A)*(X-B) GT 0.0D0 ) THEN BEGIN
         PRINT, '*** ERROR CHEBEV: X NOT IN RANGE ***'
         STOP
      ENDIF

;-----------------------------------------------------------------------
      D  = 0.0D0
      DD = 0.0D0
;-----------------------------------------------------------------------
; Change of variable
;-----------------------------------------------------------------------
      Y  = (2.0D0*X-A-B)/(B-A)
      Y2 = 2.0D0*Y
;-----------------------------------------------------------------------
; Clenshaw's recurrence
;-----------------------------------------------------------------------

      FOR J=M-1,1,-1 DO BEGIN
         SV = D
         D  = Y2*D-DD+C(J)
         DD = SV
      ENDFOR

;-----------------------------------------------------------------------
; Last step: note that the first and last terms of the summation
; must be halved
;-----------------------------------------------------------------------
      CHEBEV = Y*D-DD + 0.5*C(0)
;-----------------------------------------------------------------------
      RETURN, CHEBEV
;-----------------------------------------------------------------------
END
