; Copyright (c) 1995, Strathclyde University.
; SCCS info: Module @(#)$Header: /home/adascvs/idl/adas6xx/adas601/readgft.pro,v 1.2 2004/07/06 14:49:22 whitefor Exp $ Date $Date: 2004/07/06 14:49:22 $
; Alessandro Lanzafame	acl@phys.strath.ac.uk	21 November 1995
;+
; PROJECT:
;       ADAS support and application programs
;
; NAME:
;       READGFT
;
; NOTE:
;  	This routine is not yet fully annotated.
;
; PURPOSE:
;
; EXPLANATION:
;
; USE:
;
; INPUTS:
;       None
;
; OPTIONAL INPUTS:
;       None
;
; OUTPUTS:
;       None
;
; OPTIONAL OUTPUTS:
;       None
;
; KEYWORD PARAMETERS:
;       None
;
; CALLS:
; SIDE EFFECTS:
;
; CATEGORY:
;       Adas support.
;
; WRITTEN:
;       Alessandro Lanzafame, University of Strathclyde, nov21-95
;
; MODIFIED:
;       1       Alessandro Lanzafame
;               First release.
;       1.1     Tim Hammond
;               Put under S.C.C.S. Control
;	1.2	William Osborn
;		Removed spaces from before macros
;
; VERSION:
;       1       21-11-95
;       1.1     03-05-96
;	1.2	07-05-96
;
;-----------------------------------------------------------------------

PRO readgft

@params
@cfiles
@cdemdat

    t = fltarr(MTEMP,MTRA)
    tm = fltarr(MTRA)
    goft = fltarr(MTEMP,MTRA)
    ng = lonarr(MTRA)

    spawn, fortdir + '/readgft.x', unit=pipe, /noshell, PID=pid

    printf, pipe, IGFT
    printf, pipe, inpfls.kernel
    printf, pipe, demdat.m
    for i=0, demdat.m-1 do begin
        printf, pipe, strupcase(strtrim(demdat.delem(i)))
        printf, pipe, strtrim(demdat.ion(i))
        printf, pipe, demdat.izi(i)
        printf, pipe, demdat.indx(i)
    endfor

    if find_process(pid) eq 0 then goto, LABELERROR

    for i=0, demdat.m - 1 do begin
        readf, pipe, scrng
        ng(i) = scrng
        gmax = 0.0
        for j=0, ng(i)-1 do begin
            readf, pipe, scrt
            readf, pipe, scrgoft
            t(j,i) = scrt
            goft(j,i) = scrgoft
        endfor
    endfor

;-----------------------------------------------------------------------
; Find temperatures at which G(T) are at maximum
;-----------------------------------------------------------------------

    for i=0, demdat.m - 1 do begin
        for j=0, ng(i)-1 do begin
            scrgoft = goft(j,i)*t(j,i)*demdat.dabund(i)
            if gmax lt scrgoft then begin
                gmax = scrgoft
	        tm(i) = scrt
            endif
        endfor
    endfor

;-----------------------------------------------------------------------
; store in structure demdat
;-----------------------------------------------------------------------

   demdat = {   NTEMP: demdat.ntemp		,$
		TEMP:  demdat.temp		,$
		M:     demdat.m			,$
		DELEM: demdat.delem		,$
		ION:   demdat.ion		,$
		IZI:   demdat.izi		,$
		DABUND:demdat.dabund		,$
		WVLEN: demdat.wvlen		,$
		G00:   demdat.g00		,$
		G:     demdat.g			,$
		SG00:  demdat.sg00		,$
		SG:    demdat.sg		,$
		INDX:  demdat.indx		,$
		NG:    ng			,$
		T:     t			,$
		GOFT:  goft			,$
		TM:    tm}

    goto,LABELEND

LABELERROR:

    print, 'READGFT.PRO ERROR - readgft.x stopped before readf.pro'

LABELEND:

END
