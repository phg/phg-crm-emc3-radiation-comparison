; Copyright (c) 1995, Strathclyde University.
; Alessandro Lanzafame	acl@phys.strath.ac.uk	21 November 1995
; SCCS info: Module @(#)$Header: /home/adascvs/idl/adas6xx/adas601/gelemt.pro,v 1.1 2004/07/06 13:58:24 whitefor Exp $ Date $Date: 2004/07/06 13:58:24 $
;+
; PROJECT:
;       ADAS support and application programs
;
; NAME:
;       GELEMT
;
; NOTE:
;	This routine is not yet properly annotated
;
; PURPOSE:

;
; EXPLANATION:
;
; USE:
;
; INPUTS:
;
; OPTIONAL INPUTS:
;
; OUTPUTS:
;
; OPTIONAL OUTPUTS:
;
; KEYWORD PARAMETERS:
;
; CALLS:
;
; SIDE EFFECTS:
;
; CATEGORY:
;       Adas support.
;
; WRITTEN:
;       Alessandro Lanzafame, University of Strathclyde, nov21-95
;
; MODIFIED:
;       1       Alessandro Lanzafame
;               First release.
;       1.1     Tim Hammond
;               Put under S.C.C.S. Control
; VERSION:
;       1       21-11-95
;       1.1     03-05-96
;
;------------------------------------------------------------------------
;-

PRO gelemt

    COMMON PARAMS, MTRA, MTEMP, MABUND, IGFT
    COMMON CFILES,inval,inpfls,fortdir,deffile
    COMMON CABUND, eabund, symbamb, nabund
    COMMON CDEMDAT, demdat

    dabund=fltarr(MTRA)

    delem = MAKE_ARRAY(MTRA,/STRING)
      ion = MAKE_ARRAY(MTRA,/STRING)
      izi = MAKE_ARRAY(MTRA,/LONG)
    wvlen = MAKE_ARRAY(MTRA,/FLOAT)
      g00 = MAKE_ARRAY(MTRA,/FLOAT)
     sg00 = MAKE_ARRAY(MTRA,/FLOAT)
     indx = MAKE_ARRAY(MTRA,/LONG)

    rd_lines_adas,inpfls.intensity,obs

    print,'recognised abundance for      abundance'

    m=0

    for l=0, obs.m - 1 do begin
        for j=0,nabund-1 do begin
            if STRUPCASE(obs.delem(l)) eq STRUPCASE(symbamb(j)) then begin

              dabund(m) = eabund(j)
               delem(m) = obs.delem(l)
                 ion(m) = obs.ion(l)
                 izi(m) = obs.izi(l)
               wvlen(m) = obs.wvlen(l)
                 g00(m) = obs.g00(l)
                sg00(m) = obs.sg00(l)
                indx(m) = obs.indx(l)

               print,format='(i4,2x,a2,a1,i2,1x,f12.2,1x,e12.3)',$
                     l,delem(l),'+',izi(l),wvlen(l),dabund(l)
               goto, RECOGNIZED
            endif
        endfor

        print,' I dont recognise ',delem(m),' as one of the elements',$
              ' that i know the abundance of.'
RECOGNIZED:
        m=m+1
    endfor

;-----------------------------------------------------------------------
;  Get IZI=charge of ion by running ion.x
;-----------------------------------------------------------------------
;lab=''
;seqsy=''
;spawn,fortdir+'/ion.x',unit=pipe,/noshell,PID=pid
;printf,pipe,m	
;for i=0,m-1 do begin
;   if STRMID(delem(i),1,1) eq ' ' then begin
;      printf,pipe,delem(i)+ion(i) 
;   endif else begin
;      printf,pipe,delem(i)+' '+ion(i)
;   endelse
;endfor
;for i=0,m-1 do begin
;   readf,pipe,lab
;   print,lab
;   readf,pipe,format='(2(i5),4x,a7)',IZN,IZIC,SEQSY
;   print,format='(2(i5),4x,a7)',IZN,IZIC,SEQSY
;   izi(i)=IZIC
;endfor
;close,pipe

;-----------------------------------------------------------------------
;  store in structure demdat
;  NOTE: set sg=sg00 (weights given in intensity data file) as
;  default. 
;-----------------------------------------------------------------------
    demdat = {   NTEMP : demdat.ntemp		,$
	 	  TEMP : demdat.temp		,$
		     M : m			,$
		 DELEM : delem(0:m-1)	  	,$
	   	   ION : ion(0:m-1)		,$
		   IZI : izi(0:m-1)		,$
	        DABUND : dabund(0:m-1)		,$
		 WVLEN : wvlen(0:m-1)		,$
		   G00 : g00(0:m-1)		,$
		     G : g00(0:m-1)*12.56	,$
		  SG00 : sg00(0:m-1)		,$
		    SG : sg00(0:m-1)		,$
		  INDX : indx(0:m-1)		}

END

