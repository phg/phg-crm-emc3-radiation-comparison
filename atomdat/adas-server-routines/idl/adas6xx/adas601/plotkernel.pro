; Copyright (c) 1996, Strathclyde University.
; SCCS info: Module @(#)$Header: /home/adascvs/idl/adas6xx/adas601/plotkernel.pro,v 1.4 2004/07/06 14:35:45 whitefor Exp $ Date $Date: 2004/07/06 14:35:45 $
;+
; PROJECT:
;       ADAS support and applications
;
; NAME:
;	PLOTKERNEL
;
; PURPOSE:
;	Plot one graph for DEM.
;
; EXPLANATION:
;	This routine plots output for a single graph.
;
; USE:
;	Use is specific to dem.pro.
;
; INPUTS:
;	X	- Double array; list of x values.
;
;	Y	- 2D double array; y values, 2nd index ordinary level,
;		  indexed by iord1 and iord2.
;
;	NPLOTS  - Integer : type of plots  1: Data from file only
;					   3: Data and spline fit
;					   5: Data and minmax fit
;					   7: Data, spline, & minimax fits.
;
;	TITLE	- String array : General title for program run. 6 lines.
;
;	XTITLE  - String : title for x-axis annotation
;
;	YTITLE  - String : title for y-axis annotation
;
;	LDEF1	- Integer; 1 - User specified axis limits to be used, 
;		  	   0 - Default scaling to be used.
;
;	XMIN	- String; Lower limit for x-axis of graph, number as string.
;
;	XMAX	- String; Upper limit for x-axis of graph, number as string.
;
;	YMIN	- String; Lower limit for y-axis of graph, number as string.
;
;	YMAX	- String; Upper limit for y-axis of graph, number as string.
;
; OPTIONAL INPUTS:
;	None.
;
; OUTPUTS:
;	None.
;
; OPTIONAL OUTPUTS:
;	None.
;
; KEYWORD PARAMETERS:
;	None.
;
; CALLS:
;	None.
;
; SIDE EFFECTS:
;	None.
;
; CATEGORY:
;	Adas system.	
;
; WRITTEN:
;       Alessandro Lanzafame, University of Strathclyde, 21st November 1995
;
; MODIFIED:
;	1.0	Alessandro Lanzafame
;		First Release
;	1.1	William Osborn
;		Put under SCCS control
;	1.2	William Osborn
;		Added window position common block + extra title line
;	1.3	Tim Hammond
;		Added dynamic repositioning of the title on small monitors.
;	1.4	Alessandro Lanzafame
;		Changed default threshold
; 
; VERSION:
;	1.0	????????
;	1.1	07-05-96
;	1.2	14-05-96
;	1.3	07-08-96
;	1.4	21-02-97
;
;
;----------------------------------------------------------------------------

PRO plotkernel, m, x , y, title, xtitle, ytitle, $
             ldef1, xmin, xmax, ymin, ymax

  COMMON Global_lw_data, left, right, top, bottom, grtop, grright
    
  ;-------------------------------------------------------------------------
  ;	Suitable character size for current device
  ;	 Aim for 60 characters in y direction.    
  ;	Also reduce slightly to fit on page        
  ;-------------------------------------------------------------------------
  charsize = (!d.y_vsize/!d.y_ch_size)/60.0 *0.8
  small_check = GETENV('VERY_SMALL')

  ;-------------------------------------------------------------------------
  ;	 set makeplot counter 
  ;-------------------------------------------------------------------------
  makeplot = 1

  ;-------------------------------------------------------------------------
  ;	Construct graph title
  ;	"!C" is the new line control.
  ;-------------------------------------------------------------------------
  !p.font=-1
  if small_check eq 'YES' then begin
      gtitle =  title(0) + "!C!C" + title(1) + "!C!C" + title(2) + "!C!C" + $
	        title(3) + "!C!C" + title(4) + "!C!C" + title(5)
  endif else begin
      gtitle =  title(0) + "!C" + title(1) + "!C" + title(2) + "!C" + $
	        title(3) + "!C" + title(4) + "!C" + title(5)
  endelse

  ;-------------------------------------------------------------------------
  ;	Find x and y ranges for auto scaling 
  ;-------------------------------------------------------------------------

  if ldef1 eq 0 then begin
  ;-------------------------------------------------------------------------
  ;	identify values in the valid range
  ;	plot routines only work within
  ;	single precision limits.
  ;-------------------------------------------------------------------------
    xvals = where(x gt 1.0e-37 and x lt 1.0e37)
    yvals = where(y gt 1.0e-37 and y lt 1.0e37)

    if xvals(0) gt -1 then begin
      xmax = max(x(xvals), min=xmin)
      makeplot = 1
    end else begin
      makeplot = 0
    end

    if yvals(0) gt -1 then begin
      ymax = max(y(yvals), min=ymin)
      makeplot = 1
    end else begin
      makeplot = 0
    end

    style = 0
  end else begin

  ;-------------------------------------------------------------------------
  ;	Check that at least some data in in axes range
  ;-------------------------------------------------------------------------
    xvals = where(x ge xmin and x le xmax)
    yvals = where(y ge ymin and y le ymax)
    if xvals(0) eq -1 or yvals(0) eq -1 then begin
      makeplot = 0
    endif 
    style = 1
  end


  ;-------------------------------------------------------------------------
  ;	Set up log-log plotting axes
  ;-------------------------------------------------------------------------
    plot_oo,[xmin,xmax],[ymin,ymax],/nodata,ticklen=1.0, $
		position=[left,bottom,grright,grtop], $
		xtitle=xtitle, ytitle=ytitle, xstyle=style, ystyle=style, $
		charsize=charsize

  if makeplot eq 1 then begin

  ;-------------------------------------------------------------------------
  ;	  Make plots
  ;-------------------------------------------------------------------------

  for i=0,m-1 do begin
     yvals=where(y(*,i) ge ymin)	;Extra check to make sure all yvals
     oplot,x(yvals,i),y(yvals,i)	;are in IDL's plotting range
  endfor

  endif else begin
     print, "plotkernel : No data found in these axes ranges"
  endelse

  ;-------------------------------------------------------------------------
  ;	plot title
  ;-------------------------------------------------------------------------
  if small_check eq 'YES' then begin
      xyouts, (left-0.02), top+0.05, gtitle, /normal, alignment=0.0, $
			charsize=charsize
  endif else begin
      xyouts, (left-0.02), top, gtitle, /normal, alignment=0.0, $
			charsize=charsize
  endelse


END
