;+
; PROJECT:
;       ADAS
;
; NAME:
;	GABUND_EBAR.PRO
;
; PURPOSE:
;       Read abundances from data file. This version reads error bars too.
;
; EXPLANATION:
;       Reads in a list of chemical elements and their abundances
;       as well as their associated error bars
;
; USE:
;       From the IDL prompt or within a program
;
;               gabund_ebar,'file.dat',abne
;
;       will return a structure 'abne' containing
;       data read from 'file.dat'
;
; INPUTS:
;       ABUNDFILE = Abundance file name.
;
; OPTIONAL INPUTS:
;	None
;
; OUTPUTS:
;       ABNE  =      Structure containing abundance data.
;
;                  NABUND    Number of elements contained in the data file
;
;                  SYMBAMB() Symbol of elements listed in the data file
;
;                  EABUND()  Abundance for element
;
;                  MINABUND() Minimum abundance
;
;                  MAXABUND() Maximum abundance
;
;
; OPTIONAL OUTPUTS:
;	None
;
; KEYWORD PARAMETERS:
;	None
;
; CALLS:
;       None  
;
; SIDE EFFECTS:
;       None
;
; CATEGORY:
;	Adas support and applications.
;	
; WRITTEN:
;       Alessandro Lanzafame, University of Strathclyde, mar19-95
;
; MODIFIED:
;	1.0	Alessandro Lanzafame
;		First version adapted from gabund
;
; VERSION:
;	1.0	25-02-01
;
;-
;-----------------------------------------------------------------------------

PRO gabund_ebar,abundfile,abne,MABUND

    openr,lu,abundfile,/get_lun

    nabund=0
    string=''

    sscr=strarr(MABUND)
    escr=dblarr(MABUND)
    abmins=dblarr(MABUND)
    abmaxs=dblarr(MABUND)

    while not eof(lu) do begin
;        readf,lu,format='(a2,3(1x,e10.3))',s,e,abmin,abmax
        readf,lu,string
        astrng=strsplit(string,/extract)
        sscr(nabund)=astrng(0)
        escr(nabund)=double(astrng(1))
        abmins(nabund)=double(astrng(2))
        abmaxs(nabund)=double(astrng(3))
        nabund=nabund+1
    endwhile

    close,lu
    free_lun,lu

;----------------------------------------------------------------------
; Convert if abundance is given in Log(abundance) with Log(H)=12.0
;----------------------------------------------------------------------

l=where(strtrim(sscr,2) eq 'H',counts)
if counts eq 1 then begin
   if escr(l(0)) eq 12.0 then begin
       escr=10^(escr-12.0)
       abmins=10^(abmins-12.0)
       abmaxs=10^(abmaxs-12.0)
   endif
endif

;-----------------------------------------------------------------------
; Set up structure abne
;-----------------------------------------------------------------------

;abn = {  abn                       ,$
;         nabund : nabund           ,$
;        symbamb : sscr(0:nabund-1) ,$
;         eabund : escr(0:nabund-1) }

    abne.nabund = nabund           
    abne.symbamb = sscr(0:nabund-1) 
    abne.eabund = escr(0:nabund-1) 
    abne.abmin = abmins(0:nabund-1)
    abne.abmax = abmaxs(0:nabund-1)

END

