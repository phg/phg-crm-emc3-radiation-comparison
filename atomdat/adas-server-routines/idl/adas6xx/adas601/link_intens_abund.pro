; Copyright (c) 1996, Strathclyde University.
; SCCS info: Module @(#)$Header: /home/adascvs/idl/adas6xx/adas601/link_intens_abund.pro,v 1.2 2004/07/06 14:11:28 whitefor Exp $ Date $Date: 2004/07/06 14:11:28 $
;+
; PROJECT:
;       ADAS support and application programs
;
; NAME:
;       LINK_INTENSITY_ABUNDANCE
;
; NOTE:
;	This routine is not yet properly annotated
;
; PURPOSE:
;
; EXPLANATION:
;
; USE:
;
; INPUTS:
;
; OPTIONAL INPUTS:
;
; OUTPUTS:
;
; OPTIONAL OUTPUTS:
;
; KEYWORD PARAMETERS:
;       None
;
; CALLS:
;
; SIDE EFFECTS:
;
; CATEGORY:
;       Adas support.
;
; WRITTEN:
;       Alessandro Lanzafame, University of Strathclyde, unknown
;
; MODIFIED:
;       1       Alessandro Lanzafame
;               First release.
;       1.1     Tim Hammond
;               Put under S.C.C.S. Control
;	1.2	William Osborn
;		Removed spaces from before macros
;
; VERSION:
;       1       unknown
;       1.1     03-05-96
;	1.2	07-05-96
;
;-----------------------------------------------------------------------
;-

PRO link_intensity_abund

@params
@cabund
@cdemdat

    abn = REPLICATE( {abund(MAXBLND) }, n_elements(obs) }

    print,'recognised abundance for      abundance'

    for l=0, n_elements(obs) - 1 do begin
        for k=0, obs(i).hmib - 1 do begin
            for j=0,nabund-1 do begin
                if STRUPCASE(obs(l).delem(k)) eq 			$
		STRUPCASE(symbamb(j)) then begin
                    abn(l).abund(k) = eabund(j)
                    print, format='(2x,a2,a1,i2,1x,f12.2,1x,e12.3)',	$
                    obs(l).delem(k),'+', obs(l).izi(k),			$
                    obs(l).swvlen(k), abn.abund(k)
                    goto, RECOGNIZED
                endif
            endfor
        print,' I dont recognise ',obs(l).delem(k),' as one of the elements',$
              ' that i know the abundance of.'
RECOGNIZED:

    endfor

END


