; Copyright (c) 1995, Strathclyde University.
; SCCS info: Module @(#)run_dem.pro 1.5 Date 03/26/97
; Alessandro Lanzafame  acl@phys.strath.ac.uk    21 November 1995
;+
; PROJECT:
;       ADAS support and application programs
;
; NAME:
;       RUN_DEM
;
; PURPOSE:
;       Spawn fortran code for integral inversion.
;
; EXPLANATION:
;       This routine performs I/O with a fortran code which performs
;       the integral inversion for the determination of the DEM.
;
; USE:
;   See demap_main.pro
;
; INPUTS:
;   fortrdir = directory where the fortran executable is placed
;       abn  = structure containing the abundance
;   abnl   = array of poiniting indeces linking abn with the
;        observations
;       obs  = structure containing the observational data
;   gofts    = structure contaning the kernel functions
;   msel   = number of lines selected for the integral inversion
;       goftsl   = array of ponting indeces linking gofts with
;        the observations
;   demdat     = structure containing input for and output from
;        the inversion algorithm
;       idsrt    = vector of indeces for sorting structure demdat
;                  for increasing temperature of max G
;   iweight    = index selecting the weighting quantity for the
;                  inversion:
;                      0 - use intensity
;                      1 - use square root of intensity
;                      2 - use quantities specified in the intensity
;                          input file (e.g. measurement errors)
;   ir1uss     = integer specifying inversion method to be used
;   sigma  = assumed constant variance in the observations
;       MAXBLND  = maximum number of allowed lines in blend
;
; OPTIONAL INPUTS:
;   None
;
; OUTPUTS:
;      rl = smoothing parameter
;   sigma = estimated variance if automatic smoothing is selected
;    Hmax = maximum of operator H from fortran code
;    ldob = vector of indeces linking demdat to obs (blend)
;    ldol = array of indeces linking demdat to obs (line):
;           indicated which transitions have been summed up
;           in the blend
;  demdat = structure containing input for and output from
;           the inversion algorithm
;    dlam = knots for B-spline representation of function prior
;      co = coefficient of B-spline representation of function prior
;   ncap7 = number of B-spline intervals + 7
;  ircode = error code - 1=OK, 0=error
;
; OPTIONAL OUTPUTS:
;   None
;
; KEYWORD PARAMETERS:
;   None
;
; CALLS:
;   None
;
; SIDE EFFECTS:
;   None
;
; CATEGORY:
;       Adas support.
;
; WRITTEN:
;       Alessandro Lanzafame, University of Strathclyde, nov21-95
;
; NOTE: The actual version aloows the inclusion of blends in the
;       integral inversion. However it assumes that the blend
;       components have similar temperature of formation. Furthermore,
;       it gives most accurate results when the cooler line is listed
;       first in the blend. This should be improved in future
;       versions.
;
;
; MODIFIED:
;       1       Alessandro Lanzafame
;               First release.
;       1.1     Tim Hammond
;               Put under S.C.C.S. Control
;       1.2     William Osborn
;               Added scrf = 0.0 to prevent conversion errors
;       1.3    Alessandro Lanzafame / William Osborn
;            Added comments + various new reads from fortran.
;            Made sure that values were defined as real before reading
;            into them.
;       1.4    Tim Hammond
;            Added further I/O error handling
;       1.5     Alessandro Lanzafame
;            Get B-spline representation of function prior
;            to be used in the line of sight intensity prediction
;            (routine 'predict_intensity.pro')
;       1.6    Alessandro Lanzafame
;            Changed array of structures goftsl with array
;            of pointing indeces goftsp. This gives the addresses
;               (indeces) in the gofts structure of transitions not
;               included in the integral inversion for which the
;               prediction is sought.
;	1.7	Allan Whiteford
;		Changed spawn call to launch adas601.out instead
;		of dem02.x
;       1.8     Alessandro Lanzafame
;               Added check: if intensity is zero do not use
;               the line in the integral inversion. No warning
;               is issued
;       1.9     Alessandro Lanzafame
;               Added compatibility with Windows operating system.
;               NOTE: Windows does not allow piping. Data are stored
;               in temporary files used to pass information from IDL
;               procedure and FORTRAN and viceversa.
;
; VERSION:
;       1       21-11-95
;       1.1     03-05-96
;       1.2     14-05-96
;       1.3     12-07-96
;       1.4     07-08-96
;       1.5     21-02-97
;       1.6     12-05-99
;	1.7	08-07-2004
;       1.8     07-07-02
;       1.9     24-07-04
;
;-----------------------------------------------------------------------
;-

PRO run_dem, fortrdir, abn, abnl, obs, gofts, msel, goftsl, $
             MAXBLDN, ldob, ldol, demdat, idsrt, iweight, rss, $
             ir1uss, rl, sigma, Hmax, ircode, dlam, co, ncap7

;-----------------------------------------------------------------------
; Set up error handling
;-----------------------------------------------------------------------

   ON_IOERROR, LABELERROR
   ircode = 1

;-----------------------------------------------------------------------
; Set up demdat structure. demdat.ntemp and demdat.temp are set up
; by tgrid_log.pro
;-----------------------------------------------------------------------

;   demdat.m  = msel
   ldob = intarr(msel)
   ldol = intarr(msel,MAXBLDN)
   ;___use index k to avoid zero element in demdat structure
   j=-1
   for i = 0, n_elements(obs)-1 do begin
      if obs(i).hmib gt 0 then begin
         ;___the following selects the lines to be included in the
         ;___integral inversion
         if (obs(i).indx(0) gt 0 AND obs(i).g00 gt 0.) then begin
            j=j+1
            ldob(j) = i
            ldol(j,0) = 0
            demdat.g(j)  = 12.56 * obs(i).g00
            case iweight of
               0 : demdat.sg(j) = 12.56 * obs(i).g00
               1 : demdat.sg(j) = sqrt(12.56 * obs(i).g00)
               2 : demdat.sg(j) = 12.56 * obs(i).sg00
            endcase
            demdat.ng(j) = gofts.ng(goftsl(i,0))
            demdat.t[*,j]=0.
            demdat.t(0:demdat.ng(j)-1,j) =           $
                                  gofts.t(0:demdat.ng(j)-1,goftsl(i,0))
            demdat.goft[*,j]=0.
            demdat.goft(0:demdat.ng(j)-1,j) =        $
                         gofts.goft(0:demdat.ng(j)-1,goftsl(i,0)) *    $
                  demdat.t(0:demdat.ng(j)-1,j) * abn.eabund(abnl(i,0))
            ;___if it is a blend sum up gofts:
            ;___THIS SHOULD BE IMPROVED !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
            if obs(i).hmib gt 1 then begin
               for k = 1, obs(i).hmib - 1 do begin
                  ldol(j,k) = k
                  intep, gofts.t(*,goftsl(i,k)),       $
                         gofts.goft(*,goftsl(i,k)),        $
          demdat.t(*,j),            $
          goftintp
                  goftintp = goftintp *                                 $
                             gofts.t(*,goftsl(i,k)) *      $
                             abn.eabund(abnl(i,k))
                  demdat.goft(0:demdat.ng(j)-1,j) =          $
                    demdat.goft(0:demdat.ng(j)-1,j) +      $
                             goftintp(0:demdat.ng(j)-1)
               endfor
            endif
         endif
      endif
   endfor
   demdat.m=j+1

;-----------------------------------------------------------------------
; Find temperatures at which G(T) are at maximum
;-----------------------------------------------------------------------

    for i=0, demdat.m - 1 do begin
        max_subscript = 0
        gmax=MAX(demdat.goft(*,i),max_subscript)
        demdat.tm(i) = demdat.t(max_subscript,i)
        if demdat.tm(i) le 0 then MESSAGE,'Cannot find temperature of Gmax'
    endfor

;-----------------------------------------------------------------------
; Dimensions of integral inversion output vectors
;-----------------------------------------------------------------------

    ys   = dblarr(demdat.m)
    fhat = dblarr(demdat.ntemp)
    f0   = dblarr(demdat.ntemp)

    irec = dblarr(demdat.m)
    eps  = dblarr(demdat.m)
    sig  = dblarr(demdat.m)
    dev  = dblarr(demdat.m)

;-----------------------------------------------------------------------
;  sort demap for increasing temperature
;-----------------------------------------------------------------------

    idsrt=sort(demdat.tm(0:demdat.m-1))

;-----------------------------------------------------------------------
; debugging ....
;-----------------------------------------------------------------------
;    openw, lo, 'temp.dat', /get_lun
;    printf, lo, ir1uss
;    printf, lo, sigma
;    printf, lo, demdat.ntemp
;    for i=0, demdat.ntemp-1 do printf, lo, demdat.temp(i)
;    printf, lo, demdat.m
;    for i=0, demdat.m-1 do begin
;        printf, lo, demdat.g(idsrt(i))
;        printf, lo, demdat.sg(idsrt(i))
;        printf, lo, demdat.ng(idsrt(i))
;        printf, lo, demdat.tm(idsrt(i))
;        for j=0, demdat.ng(idsrt(i))-1 do begin
;            printf, lo, demdat.t(j,idsrt(i))
;            printf, lo, demdat.goft(j,idsrt(i))
;        endfor
;    endfor
;    close, lo
;    free_lun, lo

;-----------------------------------------------------------------------


;-----------------------------------------------------------------------
;      Start fortran code (Unix) or open temp file (Window)
;-----------------------------------------------------------------------

    if !version.os eq 'Win32' then begin
        openw,pipe,'tempdemin.dat',/get_lun
    endif else begin
        spawn, fortrdir + '/adas601.out', unit=pipe, /noshell, PID=pid
    endelse

;-----------------------------------------------------------------------
;  pass intensities and kernel functions to fortran
;-----------------------------------------------------------------------

    printf, pipe, ir1uss
    printf, pipe, sigma
    printf, pipe, demdat.ntemp
    for i=0, demdat.ntemp-1 do printf, pipe, demdat.temp(i)
    printf, pipe, demdat.m
    for i=0, demdat.m-1 do begin
        printf, pipe, demdat.g(idsrt(i))
        printf, pipe, demdat.sg(idsrt(i))
        printf, pipe, demdat.ng(idsrt(i))
        printf, pipe, demdat.tm((idsrt(i)))
        for j=0, demdat.ng(idsrt(i))-1 do begin
            printf, pipe, demdat.t(j,idsrt(i))
            printf, pipe, demdat.goft(j,idsrt(i))
        endfor
    endfor

;-----------------------------------------------------------------------
;  close tempdemin.dat, start fortran code and open tempdemout.dat (Windows)
;  or check if process is still running (Unix)
;-----------------------------------------------------------------------

    if !version.os eq 'Win32' then begin
        cc=1
        printf,pipe,cc
        close, pipe
        free_lun,pipe
        spawn, fortrdir + '\adas601.out < tempdemin.dat > tempdemout.dat ', temp
        openr,pipe,'tempdemout.dat',/get_lun
    endif else begin
        if find_process(pid) eq 0 then goto, LABELERROR
    endelse

;-----------------------------------------------------------------------
; read results
;-----------------------------------------------------------------------

    fdum = 0.0d0
    readf,pipe,fdum     ; smoothing parameter
    rl=fdum
    readf,pipe,fdum     ; variance
    sigma=fdum
    readf,pipe,fdum             ; maximum of matrix H
    Hmax=fdum

    idum = 0         ; B-spline representation of function prior
    readf,pipe,idum
    ncap7 = idum
    dlam=dblarr(ncap7)
    co=dblarr(ncap7)
    for i=0, idum-1 do begin
        readf, pipe, fdum
        dlam(i) = fdum
        readf, pipe, fdum
        co(i) = fdum
    endfor

    for i=0, demdat.m-1 do begin
        readf, pipe, fdum
        demdat.ys(idsrt(i)) = fdum          ; first approximation to DEM
        readf, pipe, fdum
        demdat.irec(idsrt(i)) = fdum        ; reconstructed intensity
        readf, pipe, fdum
        demdat.eps(idsrt(i)) = fdum
        readf, pipe, fdum
        demdat.sig(idsrt(i)) = fdum
        readf, pipe, fdum
        demdat.dev(idsrt(i)) = fdum
    endfor

    readf, pipe, rss    ; chisquare

    for i=0, demdat.ntemp-1 do begin
        readf, pipe, fdum
        demdat.fhat(i) = fdum
        readf, pipe, fdum
        demdat.f0(i) = fdum
    endfor

;-----------------------------------------------------------------------
; tell the fortran that it can stop now
;-----------------------------------------------------------------------

    if !version.os ne 'Win32' then begin
       cc = 1
       printf, pipe, cc
    endif

;-----------------------------------------------------------------------

    goto, LABELEND

LABELERROR:
    print,'*******************ADAS 601 ERROR************************'
    print,'**                                                     **'
    print,'**  A FATAL ERROR OCCURRED WHILST COMMUNICATING WITH   **'
    print,'**  THE FORTRAN CHILD PROCESS adas601.out              **'
    print,'**  THE INTEGRAL INVERSION WAS NOT COMPLETED.          **'
    print,'**  It is possible this was caused by UNIX pipe        **'
    print,'**  problems - attempting execution again may succeed. **'
    print,'**                                                     **'
    print,'*********************************************************'
    ircode = 0

LABELEND:

;-----------------------------------------------------------------------
; zero all remaining demdat elements
;-----------------------------------------------------------------------

   for i=demdat.m, n_elements(demdat)-1 do begin
      demdat.g(i)=0.
      demdat.sg(i)=0.
      demdat.ng(i)=0
      demdat.t(*,i)=0.
      demdat.goft(*,i)=0.
      demdat.tm(i)=0.
      demdat.ys(i)=0.
      demdat.irec(i)=0.
      demdat.eps(i)=0.
      demdat.sig(i)=0.
      demdat.dev(i)=0.
   endfor


END
