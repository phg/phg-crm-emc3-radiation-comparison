; Copyright (c) 1996, Strathclyde University.
; SCCS info: Module @(#)$Header: /home/adascvs/idl/adas6xx/adas601/hmat.pro,v 1.2 2004/07/06 14:05:58 whitefor Exp $	Date $Date: 2004/07/06 14:05:58 $
;+
; PROJECT:
;       ADAS support and application programs
;
; NAME:
;       HMAT
; NOTE:
;	This routine is not yet properly annotated
;
; PURPOSE:
;
; EXPLANATION:
;
; USE:
;
; INPUTS:
;       None
;
; OPTIONAL INPUTS:
;       None
;
; OUTPUTS:
;       None
;
; OPTIONAL OUTPUTS:
;       None
;
; KEYWORD PARAMETERS:
;       None
;
; CALLS:
;       None
;
; SIDE EFFECTS:
;
; CATEGORY:
;       Adas support.
;
; WRITTEN:
;       Alessandro Lanzafame, University of Strathclyde, unknown
;
; MODIFIED:
;       1       Alessandro Lanzafame
;               First release.
;       1.1     William Osborn
;               Put under S.C.C.S. Control
;	1.2	Alessandro Lanzafame
;		Fixed bug: Introduced index lab_77
; VERSION:
;       1       unknown
;       1.1     12-07-96
;	1.2	21-02-97
;
;-----------------------------------------------------------------------
;-

pro HMAT, H, NTEMP, T0, T, DA, K
 
;-----------------------------------------------------------------------
; Find nonzero elements of kernel DA
;-----------------------------------------------------------------------

inozero=where(da,counts)
NMIN=inozero(0)
NMAX=inozero(counts-1)

;-----------------------------------------------------------------------
; Initialise
;-----------------------------------------------------------------------

H(K,*)=0.0d0
TMIN=T(NMIN)
TLOW=TMIN
HLOW=DA(NMIN)

IUP=NMIN+1

;-----------------------------------------------------------------------
; Build up H matrix
;-----------------------------------------------------------------------

for j = 0, ntemp-1 do begin

    if T0(j) gt TLOW then begin

lab_77:

       if IUP gt NMAX then goto, EXIT

         CASE 1 OF

            (T0(J) GT T(IUP)): BEGIN
               HUP=DA(IUP)
               TUP=T(IUP)
               ALP=(HUP-HLOW)/(TUP-TLOW)
               IUP=IUP+1
               IF ALP NE 0.0 THEN BEGIN
                  H(K,J)=H(K,J)+(EXP(HUP)-EXP(HLOW))/ALP
               ENDIF ELSE BEGIN
                  H(K,J)=H(K,J)+(EXP(HUP)*TUP-EXP(HLOW)*TLOW)
               ENDELSE
               TLOW=TUP
               HLOW=HUP
               GOTO, lab_77
            END

            (T0(J) EQ T(IUP)): BEGIN
               HUP=DA(IUP)
               TUP=T(IUP)
               ALP=(HUP-HLOW)/(TUP-TLOW)
               IUP=IUP+1
               IF ALP NE 0.0 THEN BEGIN
                  H(K,J)=H(K,J)+(EXP(HUP)-EXP(HLOW))/ALP
               ENDIF ELSE BEGIN
                  H(K,J)=H(K,J)+(EXP(HUP)*TUP-EXP(HLOW)*TLOW)
               ENDELSE
               TLOW=TUP
               HLOW=HUP
            END

            ELSE: BEGIN
               TUP=T0(J)
               HUP=DA(IUP-1)+(DA(IUP)-DA(IUP-1))*(TUP-T(IUP-1))/$
                   (T(IUP)-T(IUP-1))
               ALP=(HUP-HLOW)/(TUP-TLOW)
               IF ALP NE 0.0 THEN BEGIN
                  H(K,J)=H(K,J)+(EXP(HUP)-EXP(HLOW))/ALP
               ENDIF ELSE BEGIN
                  H(K,J)=H(K,J)+(EXP(HUP)*TUP-EXP(HLOW)*TLOW)
               ENDELSE
               TLOW=TUP
               HLOW=HUP
            END

         ENDCASE

   endif
endfor
EXIT:
END
