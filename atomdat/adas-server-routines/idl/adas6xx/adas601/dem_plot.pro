; Copyright (c) 1996, Strathclyde University.
; SCCS info:
;+
; PROJECT:
;       ADAS support and applications
;
; NAME:
;	DEM_PLOT
;
; PURPOSE:
;	Generates DEM functions graphical output.
;
; EXPLANATION:
;	This routine creates a window for the display of graphical
;	output. A separate routine PLOTDEM actually plots the
;	graph.
;
; USE:
;	This routine is specific to dem.pro.
;
; INPUTS:
;

; OPTIONAL INPUTS:
;	None.
;
; OUTPUTS:
;	None.
;
; OPTIONAL OUTPUTS:
;	None.
;
; KEYWORD PARAMETERS:
;
; CALLS:
;	CW_ADAS_GRAPH	Graphical output widget.
;	PLOTDEM	Make one plot to an output device.
;	XMANAGER
;
; SIDE EFFECTS:
;	This routine uses a common block to maintain its state KERNEL_BLK.
;
;	One other routine is included in this file;
;	DEM_PLOT_EV	Called via XMANAGER during widget management.
;
; CATEGORY:
;	Adas support and applications.
;
; WRITTEN:
;       Alessandro Lanzafame, University of Strathclyde, 21 November 1995
;
; MODIFIED:
;	1.1	William Osborn
;		Put under SCCS control
;	1.2	William Osborn
;		Changed window title + added title line
;	1.3	William Osborn
;		Changed window title
;	1.4	Alessandro Lanzafame
;		Fixed title position
;	1.4	Alessandro Lanzafame
;		added ranges in the definition of xs and ys to avoid
;		memory of previous runs
;       1.5     Alessandro Lanzafame
;               added XSIZE and YSIZE keyword in call to CW_ADAS_GRAPH
;               in order to pass the screen dimension and avoid
;               problems with small screens
;       1.6     Alessandro Lanzafame
;               added BACKSTORE=2 in call to cw_adas_graph
;
; VERSION:
;	1.1	07-05-96
;	1.2	14-05-96
;	1.3	14-05-96
;	1.4	21-02-97
;	1.4	11-04-00
;       1.5     08-06-02
;       1.6     01-11-01
;
;-
;----------------------------------------------------------------------------

PRO dem_plot_ev, event

  COMMON dem_blk, data, action, win, plotdev, plotfile, fileopen


  newplot = 0
  print = 0
  done = 0
  ;--------------------------------------------------------------------------
  ;	Set graph and device requested
  ;--------------------------------------------------------------------------
  CASE event.action OF

	'print'	   : begin
			newplot = 1
			print = 1
		     end

	'done'	   : begin
			if fileopen eq 1 then begin
			  set_plot,plotdev
			  device,/close_file
			end
			set_plot,'X'
			widget_control,event.top,/destroy
			done = 1
		     end
  END

  ;--------------------------------------------------------------------------
  ;	Make requested plot/s
  ;--------------------------------------------------------------------------

  if done eq 0 then begin

  ;--------------------------------------------------------------------------
  ;	Set graphics device
  ;--------------------------------------------------------------------------
    if print eq 1 then begin

      set_plot,plotdev
      if fileopen eq 0 then begin
        fileopen = 1
        device,filename=plotfile
	device,/landscape
      end

    end else begin

      set_plot,'X'
      wset,win
  
    end

  ;--------------------------------------------------------------------------
  ;	 Draw graphics
  ;--------------------------------------------------------------------------
    plotdem, data.m, data.ntemp, data.x , data.y ,$
	     data.xs, data.ys, data.f0, $
  	     data.title, data.xtitle, data.ytitle, $
	     data.ldef1, data.xmin, data.xmax, data.ymin, data.ymax 


	if print eq 1 then begin
	  message = 'Plot  written to print file.'
	  grval = {WIN:0, MESSAGE:message}
	  widget_control,event.id,set_value=grval
	end


  end

END

;----------------------------------------------------------------------------

PRO dem_plot

;,  dsfull, $
;		    titlx, titlm, utitle, date, $
;		    temp, gofta, tosa, gftosa, toma, gftoma, $
;                    ldef1, xmin, xmax, ymin, ymax, $
;                    lfsel, losel, nmx, nener, npspl, $
;                    hrdout, hardname, device, header, FONT=font

@cdemdat
@cplot
@cfiles

COMMON dem_blk, data, action, win, plotdev, plotfile, fileopen
COMMON header_blk, header

  ;--------------------------------------------------------------------------
  ;	Copy input values to common
  ;--------------------------------------------------------------------------
  if outval.devsel ge 0 then begin
     device = dvcode(outval.devsel)
     plotdev = device
  endif else begin
     device='x'
  endelse
  plotfile = outval.hardname
  fileopen = 0

  ;--------------------------------------------------------------------------
  ;	Create general graph title
  ;--------------------------------------------------------------------------
  title = strarr(6)
  title(0) = '          ' + 'DEM RECONSTRUCTION VERSUS TEMPERATURE '
  if ( strtrim(strcompress(outval.gtit1),2)  ne '' ) then begin
     title(0) = title(0) + ': ' + strupcase(strtrim(outval.gtit1,2))
  endif
  title(1) = '          ' + 'ADAS    :' + strupcase(header)
  title(2) = '          ' + 'INTENSITY DATA:  ' + inpfls.intensity
  title(3) = '          ' + 'G(T) DATA     :  ' + inpfls.kernel
  title(4) = '          ' + 'ABUNDANCE DATA:  ' + inpfls.abundance 
  title(5) = '          ' +  $
  'KEY : (CROSSES - FIRST APPROXIMATION) (FULL LINE - DEM) (DASH - PRIOR)'
  

  x=demdat.temp
  xtitle = " TEMPERATURE (KELVIN)"
  y=demdat.fhat
  ytitle = "D.E.M."
  xs=demdat.tm(0:demdat.m-1)
  ys=demdat.ys(0:demdat.m-1)
  f0=demdat.f0

  ;--------------------------------------------------------------------------
  ;	if desired set up axis scales 
  ;--------------------------------------------------------------------------
  if (outval.scalbut eq 0) then begin
      xmin = min(x)
      xmin = xmin * 0.9
      xmax = max(x) * 1.1

      ymin = min(y)
      ymin = ymin * 0.9
      ymax = max(y) * 1.1
  endif else begin
      xmin=outval.xmin
      xmax=outval.xmax
      ymin=outval.ymin
      ymax=outval.ymax
  endelse

  ;--------------------------------------------------------------------------
  ;	Create graph display widget
  ;--------------------------------------------------------------------------
  graphid = widget_base(TITLE='ADAS 601 GRAPHICAL OUTPUT: D.E.M.', $
					XOFFSET=50,YOFFSET=0)
  Device, GET_SCREEN_SIZE=scrsize
  xsize = 0.8*scrsize[0] & ysize = 0.8*scrsize[1]
  cwid = cw_adas_graph(graphid,print=outval.hrdout,FONT=font,$
                      XSIZE=xsize, YSIZE=ysize, BACKSTORE=2)

  ;--------------------------------------------------------------------------
  ;	Realize the new widget
  ;--------------------------------------------------------------------------
  widget_control,graphid,/realize
  ;--------------------------------------------------------------------------

  ;--------------------------------------------------------------------------
  ;	Get the id of the graphics area 
  ;--------------------------------------------------------------------------
  widget_control,cwid,get_value=grval
  win = grval.win

  ;--------------------------------------------------------------------------
  ;	Put the graphing data into common 
  ;--------------------------------------------------------------------------
  data = {    	m:demdat.m, ntemp:demdat.ntemp, x:x, y:y, $
		xs:xs, ys:ys, f0:f0,$
	        title:title,  xtitle:xtitle, ytitle:ytitle, $
                ldef1:outval.scalbut, $
		xmin:xmin,	xmax:xmax,	ymin:ymin,	ymax:ymax $
          }
 
  wset,win

  plotdem, demdat.m, demdat.ntemp, x , y, xs, ys, f0,$
	      title, xtitle, ytitle, $
	      outval.scalbut, xmin, xmax, ymin, ymax 


  ;--------------------------------------------------------------------------
  ;	make widget modal
  ;--------------------------------------------------------------------------
  xmanager,'dem_plot',graphid,event_handler='dem_plot_ev', $
                                        /modal,/just_reg

END



