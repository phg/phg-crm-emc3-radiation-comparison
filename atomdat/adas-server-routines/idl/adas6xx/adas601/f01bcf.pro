; Copyright (c) 1997, Alessandro Lanzafame, Universita' di Catania
; SCCS info: Module @(#)$Header: /home/adascvs/idl/adas6xx/adas601/f01bcf.pro,v 1.2 2012/11/14 19:46:34 mog Exp $ Date $Date: 2012/11/14 19:46:34 $
;+
; PROJECT:
;	B-spline fitting and interpolating routines
;
; NAME:
;	F01BCF
; 
; PURPOSE: EVALUATES A CUBIC SPLINE AND ITS FIRST THREE 
;          DERIVATIVES FROM ITS B-SPLINE REPRESENTATION.
;          IDL version of ADAS routine F01BBF     
;
; EXPLANATION:
;
; 
; INPUTS:  (I*4)  NCAP7 =  n-bar + 7, where n-bar is the number 
;                          of intervals of the spline (which is one greater 
;                          than the number of interior knots, i.e. the 
;                          knots strictly within the range lambda(4) to 
;                          lambda(n-bar+4) over which the spline is defined). 
; 
;                          CONSTRAINT: NCAP7 >= 8. 
; 
;          (R*8) LAMDA() = LAMDA(j) must be set to the value of the jth 
;                          member of the complete set of knots, lambda(j), 
;                          for j = 1,2,...,n-bar+7. 
; 
;                          CONSTRAINT: the LAMDA(j) must be in 
;                          non-decreasing order with 
;                          LAMDA(NCAP7-3) > LAMDA(4). 
; 
;          (R*8) C()     = The coefficient c(i) of the B-spline N(i)(x), 
;                          for i = 1,2,...,n-bar+3. The remaining elements 
;                          of the array are not used. 
; 
;          (R*8) X       = The argument x at which the cubic spline and its 
;                          derivatives are to be evaluated. 
; 
;                          CONSTRAINT: LAMDA(4) <= X <= LAMDA(NCAP7-3). 
; 
;          (I*4) LEFT    = Specifies whether left- or right-hand values 
;                          of the spline and its derivatives are to be 
;                          computed.  Left- or right-hand values are formed
;                          according to whether LEFT is equal or not equal 
;                          to 1. If x does not coincide with a knot, 
;                          the value of LEFT is immaterial. If x = LAMDA(4), 
;                          right-hand values are computed, and if 
;                          x = LAMDA(NCAP7-3), left-hand values are formed, 
;                          regardless of the value of LEFT. 
;
; OUTPUTS:
;          (I*4) IFAIL   = 0 Handling errors
;
;          (R*8) S(4)    = S(j) contains the value of the (j-1)th derivative
;                          of the spline at the argument x, for j = 1,2,3,4. 
;                          Note that S(1) contains the value of the spline. 
; 
;          (I*4) IFAIL   = 0 unless the routine detects an error 
;
;                        = 1: NCAP7 < 8, i.e. the number of intervals is
;                             not positive. 
; 
;                        = 2: Either LAMDA(4) >= LAMDA(NCAP7-3), i.e. the 
;                             range over which s(x) is defined is null or
;                             negative in length, or X is an  invalid 
;                             argument, i.e.
;                             X < LAMDA(4) or X > LAMDA(NCAP7-3). 
;
;
;          (I*4) NC7MAX  = PARAMETER: maximum NCAP7 allowed
;
;          (I*4) N       = PARAMETER: spline order (=4)
;                          (spline degree = N-1 = 3)
;
;          (R*8) PI      = PARAMETER
;
;          (R*8) ALFA()  = Chebyshev series coefficients for x.          
; 
;          (R*8) SJ      = Spline at cosine points (used as workspace)
;
;          (I*4) J       = INDEX lamda(j) <= x<= x(j+1)
;
;          (R*8) Y       = GENERAL REAL
;          (I*4) I       = GENERAL INTEGER
;          (I*4) K       = GENERAL INTEGER
;
;
; OPTIONAL INPUTS:
;	None
;
; OPTIONAL OUTPUTS:
;	None
;
; KEYWORD PARAMETERS:
;       None
;
; CALLS:
;       None
;
; SIDE EFFECTS:
;
; CATEGORY:
;       Adas support.
;
; WRITTEN:
;       A. C. Lanzafame, Istituto di Astronomia
;                        Universita' di Catania
;                        Citta' Universitaria
;                        Viale Andrea Doria, 6
;                        I-95125 Catania, Italy
;                        Tel: +39 95 7332 240
;                        Fax: +39 95 330592
;                        e-mail: alanzafame@ct.astro.it
;
; MODIFIED:
;       1.1     Alessandro Lanzafame
;               First release.
;                
;       1.2     Alessandra Giunta
;               Modified constraint lamda(3) ge lamda(ncap7-5) 
;               to lamda(3) gt lamda(ncap7-5)
;
;
; VERSION:
;       1.1     20-02-97
;       1.2     11-08-08
;
;-----------------------------------------------------------------------
;-

PRO F01BCF, NCAP7, LAMDA, C, X, LEFT, S, IFAIL

   ON_ERROR,2

   NC7MAX=200
   N=4
   PI = 3.141592653589793D0 
   ALFA=DBLARR(N)
   ADER=DBLARR(N)
   S=DBLARR(4)

;-----------------------------------------------------------------------
; Trap error NCAP7 > NC7MAX
;-----------------------------------------------------------------------

   NCAP = NCAP7 - 7

   IF (NCAP7 GT NC7MAX) THEN BEGIN
      PRINT,'** ERROR - F01BCF.PRO: NCAP7 GREATER THAN PARAM NC7MAX  **'
      PRINT,'** INCREASE NC7MAX                                      **'
      STOP
   ENDIF

;-----------------------------------------------------------------------
; Trap error NCAP7 < 8
;-----------------------------------------------------------------------

   IF (NCAP7 LT 8) THEN BEGIN
      IFAIL=1
      PRINT,'*** ERROR - F01BCF: IFAIL = 1 ***'
      STOP
   ENDIF

;-----------------------------------------------------------------------
; Trap errors  LAMDA(0) >= LAMDA(NCAP)
;      or     X < LAMDA(0) or X > LAMDA(NCAP)
;-----------------------------------------------------------------------

;------ MODIFIED BY ASG - 11-08-2008 -----------------------------------
;
;   IF ( ( LAMDA(3) GE LAMDA(NCAP7-5) ) OR $
   IF ( ( LAMDA(3) GT LAMDA(NCAP7-5) ) OR $
        ( X LT LAMDA(3)            )   OR $
        ( X GT LAMDA(NCAP7-4)      ) ) THEN BEGIN
      IFAIL=2
      PRINT, ' LAMDA(3) = ',LAMDA(3),$
             ' X = ',X,$
             ' LAMDA(NCAP7-5 = ',LAMDA(NCAP7-5),$
             FORMAT='(1X,A,F12.4,A,F12.4,A,F12.4)'
      PRINT, '*** ERROR - F01BCF.PRO: IFAIL = 2 ***'
      STOP
   ENDIF

;-----------------------------------------------------------------------

;-----------------------------------------------------------------------
; Locate X in array LAMDA, LAMDA(J) <= X <= LAMDA(J+1)
; Check that LAMDA(J) < LAMDA(J+1)
;-----------------------------------------------------------------------

   J=3
LAB_30:
   IF ((X GE LAMDA(J)) AND (J LE (NCAP7-5))) THEN BEGIN
      J = J + 1
      GOTO, LAB_30
   ENDIF
   J=J-1
LAB_40:
   IF ((LAMDA(J) EQ LAMDA(J+1)) AND (X LE LAMDA(J+1))) THEN BEGIN
      J=J+1
      GOTO, LAB_40
   ENDIF

;-----------------------------------------------------------------------
; Evaluate Chebyshev series coefficients
;-----------------------------------------------------------------------

   BMA = 0.5D0*(LAMDA(J+1)-LAMDA(J))
   BPA = 0.5D0*(LAMDA(J+1)+LAMDA(J))
   FAC = 2.D0 / N

   FOR I = 1, N DO BEGIN
      SUM = 0.0D0
      FOR K = 1, N DO BEGIN
         Y = COS( PI * (K-0.5D0) / N )
         YM = Y*BMA + BPA 
         F01BBF, NCAP7, LAMDA, C, YM, SJ, IFAIL
         SUM = SUM + SJ * COS( (PI * (I-1)) * ((K-0.5D0) / N) )
      ENDFOR
      ALFA(I-1) = FAC * SUM
   ENDFOR

;-----------------------------------------------------------------------
; Evaluate Chebyshev series
;-----------------------------------------------------------------------

   S(0) = CHEBEV( LAMDA(J), LAMDA(J+1), ALFA, N, X )

;-----------------------------------------------------------------------
; Evaluate derivatives of the Chebyshev-approximated function
;-----------------------------------------------------------------------

   FOR I=2,N DO BEGIN
      CHDER, LAMDA(J), LAMDA(J+1), ALFA, ADER, N
      S(I-1) = CHEBEV( LAMDA(J), LAMDA(J+1), ADER, N, X )
      IF (I  LT  N) THEN BEGIN
         FOR K=1,N DO BEGIN
            ALFA(K-1) = ADER(K-1)
         ENDFOR
      ENDIF
   ENDFOR

;-----------------------------------------------------------------------

END
