;
;
;+
; PROJECT:
;       ADAS support programs
;
; NAME:
;   ABUNDANCE_ANALYSIS
;
; PURPOSE:
;   Perform a post-processing abundance analysis from given
;       Differential Emission Measure (DEM)
;
; EXPLANATION:
;   From a given DEM, the elemental abundance which gives the
;   minimum deviation for selected lines is sought using the
;   function DPFMIN.
;   Error bars are evaluated by applying the chisquare equation
;   to lines chosen for the abundance evaluation of a given
;   element. The output is written in the routine demap_abund.dat.
;   Some diagnostics is written in the file demap_abund.log
;
; USE:
;   abundance_analysis, obs,abn,abnl,gofts,goftsp,$
;                       demdat,H,dlam,co,ncap7,sigma,iweight,MAXBLND
;
; INPUTS:
;   OBS() = Array of structure containing the observational
;      quantities to be used in the abundance analysis.
;      Each structure in the array contains
;      data for one blend of lines, which may consists
;      of a single non-blended line.
;
;                  IBLND    Blend index. It can be not equal to the
;                           index of the array of structures itself
;
;                  G00      Total intensity of blend
;
;                  SG00     Uncertainty in the intensity measure of blend
;
;                  HMIB     Number of lines in blend
;
;                  DELEM()  Chemical element for each line in the
;                           blend
;
;                  ION()    Ion charge: roman numeral in
;                           spectroscopic notation.
;
;                  IZI()    Ion charge of the element
;
;                  SWVLEN() Spectroscopic line wavelength
;
;                  OWVLEN() Observed wavelwngth
;
;                  TRCONF() Configuration of levels
;
;                  INDX()   Line G-index, used to link with
;                           contribution function
;
;       ABN   =      Structure containing abundance data.
;
;                  NABUND    Number of elements contained in the data file
;
;                  SYMBAMB() Symbol of elements listed in the data file
;
;                  EABUND()  Abundance for element
;
;   ABNL =   Array of pointing indeces linking ABN with obs
;
;       GOFTS =      Structure containing G(T) functions and associated
;                    quantities.
;
;                  ELEMC()  Chemical element
;
;                  IZI()    Ion charge
;
;                  INDX()   Transition index
;
;                  NG()     Number of temperature points
;
;                  AWL()    Approximate wavelength
;
;                  SWL()    Spectroscopic wavelength
;
;                  FILEI()  ADF20 master file from which the function
;                           was extracted (using ADAS 506)
;
;                  T(,)     Log of temperature
;                             1st index: temperature
;                             2nd index: transition
;
;                  GOFT(,)  G(T) function
;                             1st index: temperature
;                             2nd index: transition
;
;   GOFTSP = Array of pointing indeces linking gofts with obs
;
;       DEMDAT =     Structure containing the differential emission
;                    measure and associated quantities
;                  NTEMP    Number of temperature points at which the
;                           DEM is calculated
;                  TEMP()   Temperature at which the DEM is calculated
;                  M        Number of transitions used in the calculation
;                           of the DEM
;                  G()      4 pi * intensity of lines used in the DEM
;                           calculation
;                  SG()     4 pi * uncertainty of lines used in the DEM
;                           calculation
;                  NG()     Number of temperature points at which the
;                           G(T) function is tabulated
;                  T(,)     Temperature at which G(T) function is tabulated
;                           1st index temperature
;                           2nd index transition
;                  GOFT(,)  Kernel used in the inversion (see run_dem.pro)
;                           1st index temperature
;                           2nd index transition
;                  TM()     Temperature at which kernel is at max
;                  YS()     Crude estimate of the DEM at TM obtained by
;                           apporximating G(T) by
;                              Integral[G(T)dT] * Dirac[T-TM]
;                  FHAT()   DEM
;                  F0()     Spline fit to YS (Data adaptive smoothing
;                           approach
;
;       DLAM     = knots for B-spline representation of function prior
;        (see run_dem.pro)
;
;       CO       = coefficient of B-spline representation of function prior
;        (see run_dem.pro)
;
;       NCAP7    = number of B-spline intervals + 7
;        (see run_dem.pro)
;
;       SIGMA    = assumed constant variance in the observations
;
;   IWEIGHT    = index selecting the weighting quantity for the
;                  inversion:
;                      0 - use intensity
;                      1 - use square root of intensity
;                      2 - use quantities specified in the intensity
;                          input file (e.g. measurement errors)
;
;   MAXBLND  = maximum number of allowed lines in blend
;
; OPTIONAL INPUTS:
;   None
;
; OUTPUTS:
;   ABN: On output the structure storing the abundances, ABN,
;        is updated wiht the new values. This allows to
;        perform the DEM calculation with the new abundances.
;
;   demap_abund.dat: The fitted abundances are written in the
;        file demap_abund.dat. Error bars are also written, with
;        the values for the elements not fitted being equal
;        to the input values.
;
;   demap_abund.log: The diagnostic from this routine and the
;        correction factors applied to find the new abundances
;        are written in the file demap_abund.log
;
; OPTIONAL OUTPUTS:
;   None
;
; KEYWORD PARAMETERS:
;   None
;
; CALLS:
;   predict_intensity:  Given a set of contribution functions
;                           and a differential emission measure
;                           function this procedure calculates the
;                           predicted line of sight intensities.
;
;   DFPMIN: Minimizes a user-written function of two or more
;           independent variables using the
;           Broyden-Fletcher-Goldfarb-Shanno variant of the
;           Davidon-Fletcher-Powell method. DFPMIN is based
;           on the routine dfpmin described in section 10.7 of
;           Numerical Recipes in C: The Art of Scientific Computing
;           (Second Edition),
;
; SIDE EFFECTS:
;       None
;
; CATEGORY:
;   Adas support and applications.
;
; WRITTEN:
;       Alessandro Lanzafame, Universita' di Catania, 12 December 2000
;
; MODIFIED:
;   1.0   Alessandro Lanzafame
;         First version
;   1.1   Alessandro Lanzafame
;         Augment abundance in iteration fixed
;   1.2   Alessandro Lanzafame
;         Improved output
;   1.3   Alessandro Lanzafame
;         Completely rewritten. From a given DEM the
;         abundances are estimated by minimising the
;         chisquare of the selected lines.
;
; VERSION:
;   1.0   12-12-00
;   1.1   05-11-02
;   1.2   27-07-04
;   1.3   16-08-04
;
;-
;-----------------------------------------------------------------------

function abund_chisq,A
   COMMON CABAN,R,S,T
   VEC = R - 2.*A*S + A*A*T
   return,TOTAL(VEC,/DOUBLE)
end

function abund_dchisq,A
   COMMON CABAN,R,S,T
   VEC = 2.*A*T - 2.*S
   return,VEC
end

pro abund_chicoeff, abn, abnl, obs, gofts, goftsp, $
               demdat, predint, H, dlam, co, ncap7, MAXBLND,$
               iweight, Elem, R, S, T, Klines
j=0
R[*]=0.
S[*]=0.
T[*]=0.
Klines[*]=0L

N = N_ELEMENTS(obs)

PREDICT_INTENSITY, abn, abnl, obs, gofts, goftsp, $
                demdat, predint, H, dlam, co, ncap7, MAXBLND

for i=0,n-1 do begin
   if (obs[i].g00 ne 0.0) then begin
      if obs[i].hmib eq 1 then begin
         pred=predint[i,j]
      endif else if obs(i).hmib gt 1 then begin
         predmult=predint(i,0:obs(i).hmib-1)
         pred=total(predmult,2)
      endif
      case iweight of
         0 : weight = 1. / obs[i].g00
         1 : weight = 1. / SQRT(obs[i].g00)
         2 : weight = 1. / obs[i].sg00
      endcase
      Bscr = weight * pred ; / abn.eabund(abnl[i,j])
      Gscr = weight * obs[i].g00
      kk=WHERE(Elem eq obs[i].delem[j])
      if kk ne -1 and N_ELEMENTS(kk) eq 1 then begin
         k=kk[0]
         R[k] = R[k] + Gscr^2
         S[k] = S[k] + (Gscr*Bscr)
         T[k] = T[k] + Bscr^2
         Klines[k] = Klines[k] + 1
      endif
   endif
endfor

end

pro abundance_analysis,obs,abn,abnl,gofts,goftsp,$
                       demdat,H,dlam,co,ncap7,sigma,iweight,MAXBLND

COMMON CABAN,R,S,T

;___parameters
Gtol   = 1.d-7
j = 0
Confidence=[9.00,11.8,14.2,16.3,18.2,20.1]

N = N_ELEMENTS(obs)

screlem = obs[*].delem[j]
indelem=where(screlem ne 'Ne' and screlem ne 'Mg')
TElem=screlem[indelem]

;TElem = obs[*].delem[j]
lsort = SORT(TElem)
Elem=TElem[UNIQ(TElem,SORT(TElem))]

Kprime=N_ELEMENTS(Elem)

if Kprime gt 6 then begin
   print,'ABUDANCE_ANALYSYS ERROR:'
   print,'number of abundances to fit greater than maximum allowed (6)'
   goto, EXIT1
endif else begin
   DeltaChisq=Confidence[Kprime-1]
endelse

A = dblarr(Kprime)
R = dblarr(Kprime)
S = dblarr(Kprime)
T = dblarr(Kprime)
;Q = dblarr(Kprime)
Klines = lonarr(Kprime)
;errab=dblarr(2,abn.nabund)
errab=dblarr(2,Kprime)


A[*] = 1.
errab[*,*] = 1.

ABUND_CHICOEFF, abn, abnl, obs, gofts, goftsp, $
                demdat, predint, H, dlam, co, ncap7, MAXBLND,$
                iweight, Elem, R, S, T, Klines


;Q = -0.5 * (-2*S - SQRT(4*S^2 - 4*R*(T-Klines)))

;print,'Current value of dev=',abund_dev(A)
;print,'Current value of ddev=',abund_ddev(A)

;print,a,r,(2*s*a),(A^2*t),(r-(2*s*a)+(A^2*t))

;DFPMIN,A,Gtol,CHISQmin,'abund_chisq','abund_dchisq',/DOUBLE,$
;    ITER=DFPniter,STEPMAX=1.e2
;print,'CHISQMIN=',CHISQmin

;for k=0,Kprime-1 do begin
;   print, Elem[k], A[k], S[k]/T[k], S[k]/T[k]/A[k]
;endfor

A=S/T

CHISQmin=abund_chisq(A)

for i=0,abn.nabund-1 do begin
   for k=0,Kprime-1 do begin
        if STRUPCASE(STRTRIM(abn.symbamb[i],2)) eq $
           STRUPCASE(STRTRIM(Elem[k],2)) then begin
           ;errab[0,i] = (Q[k]/R[k]) * abn.eabund[i]
           ;errab[1,i] = ((T[k]-Klines[k])/Q[k]) * abn.eabund[i]
           abn.eabund[i] = A[k] * abn.eabund[i]
        endif
   endfor
endfor

; Confidence limits by Monte Carlo symulation

print,'STARTING MONTE CARLO SIMULATION'

obsp=obs
Ap=A

itmax=50

for im=1, itmax do begin
   print,'ITERATION ',IM,'/',itmax
   for i=0,n-1 do begin
      ;seed=0.1
      pert = RANDOMN(seed)
      obsp[i].g00=obs[i].g00+pert*obs[i].sg00
   endfor
   ABUND_CHICOEFF, abn, abnl, obsp, gofts, goftsp, $
                   demdat, predint, H, dlam, co, ncap7, MAXBLND,$
                   iweight, Elem, R, S, T, Klines
   DFPMIN,Ap,Gtol,CHISQ,'abund_chisq','abund_dchisq',/DOUBLE,$
             STEPMAX=1.e2
   print,'DELTA CHISQ=',(CHISQ-CHISQmin),'(',DeltaChisq,')'
   if (CHISQ-CHISQmin) lt DeltaChisq then begin
      for k=0,Kprime-1 do begin
         escr=errab[0,k]
         ascr=Ap[k]
         errab[0,k]=MIN([escr,ascr])
         escr=errab[1,k]
         errab[1,k]=MAX([escr,ascr])
       endfor
   endif
endfor

; end Monte Carlo symuation

outfile='demap_abund.log'
openw,lou,outfile,/get_lun

;printf,lou,'DFPMIN: number of iterations =',DFPniter
;printf,lou,'DFPMIN: CHISQUARE minimum    =',CHISQmin
;printf,lou,'DFPMIN: requirement on grad. =',Gtol

printf,lou,'CHISQUARE=',CHISQmin

printf,lou

for k=0, Kprime-1 do begin
;   printf,lou, Elem[k], A[k],(T[k]-Klines[k])/Q[k], Q[k]/R[k], $
;    format='(a2,3(2x,f5.2))'
   printf,lou, Elem[k], A[k],errab[0,k], errab[1,k], 3./S[k], $
    format='(a2,4(2x,f5.2))'
endfor

close,lou
free_lun,lou

outfile='demap_abund.dat'
openw,lou,outfile,/get_lun

for i=0,abn.nabund-1 do begin
   errmin=1.
   errmax=1.
   for k=0,Kprime-1 do begin
      if STRUPCASE(STRTRIM(abn.symbamb[i],2)) eq $
          STRUPCASE(STRTRIM(Elem[k],2)) then begin
          errmin=errab[0,k]
          errmax=errab[1,k]
          goto,FOUND
      endif
   endfor
FOUND:
   printf,lou, abn.symbamb[i], $
               alog10(abn.eabund[i])+12, $
               alog10(abn.eabund[i]*errmin)+12, $
               alog10(abn.eabund[i]*errmax)+12, $
               format='(a2,3(2x,f5.2))'
;   endif else begin
;   printf,lou, abn.symbamb[i], $
;               alog10(abn.eabund[i])+12, $
;               alog10(abn.eabund[i])+12, $
;               alog10(abn.eabund[i])+12, $
;               format='(a2,3(2x,f5.2))'
;   endelse
endfor
close,lou
free_lun,lou

EXIT1:

end

;pro abundance_analysis,obs,abn,abnl,gofts,goftsp,$
;                       demdat,H,dlam,co,ncap7,sigma,iweight,MAXBLND
;;___parameters
;
;deltaA=1.d-1
;niter=500
;
;;___initialization
;n=n_elements(obs)
;new_abund = dblarr(n)
;min_abund = dblarr(n)
;max_abund = dblarr(n)
;devst=dblarr(n)
;
;for i=0,n-1 do begin
;   new_abund(i)=abn.eabund(abnl(i,0))
;   min_abund(i)=abn.eabund(abnl(i,0))
;   max_abund(i)=abn.eabund(abnl(i,0))
;endfor
;
;;___first compute the deviation for the given abundances
;print,'first compute the deviation for the given abundances'
;predict_intensity, abn, abnl, obs, gofts, goftsp, $
;                demdat, predint, H, dlam, co, ncap7, MAXBLND
;
;iexit=1                         ;___exit switch
;for i=0,n-1 do begin
;    if (obs(i).g00 ne 0.0) then begin
;        if obs(i).hmib eq 1 then begin
;            j=0
;            case iweight of
;                0 : dev = (predint(i,j) - obs(i).g00)^2 / ((obs(i).g00)^2)
;                1 : dev = (predint(i,j) - obs(i).g00)^2 / ( obs(i).g00)
;                2 : dev = (predint(i,j) - obs(i).g00)^2 / ((obs(i).sg00)^2)
;            endcase
;        endif else if obs(i).hmib gt 1 then begin
;            predmult=predint(i,0:obs(i).hmib-1)
;            predmult=total(predmult,2)
;            case iweight of
;                0 : dev = (predmult - obs(i).g00)^2 / ((obs(i).g00)^2)
;                1 : dev = (predmult - obs(i).g00)^2 / (obs(i).g00)
;                2 : dev = (predmult - obs(i).g00)^2 / ((obs(i).sg00)^2)
;            endcase
;        endif
;        if dev(0) le 1. then iexit=0 ;___check that at least one line
;                                     ;   is within obs error
;        devst(i)=dev(0)          ;___store deviation in vector devst
;    endif
;endfor
;
;if iexit eq 1 then goto, EXIT
;
;print,'the abundance analysis will be performed on the following lines:'
;
;for i = 0, n-1 do print, obs(i).delem(j), '+', $
;           obs(i).izi(j), $
;           obs(i).swvlen(j), $
;           'I=',predint(i,0),$
;           'A(',abn.symbamb(abnl(i,0)),')=',$
;           alog10(abn.eabund(abnl(i,0)))+12,$
;           'dev=',devst(i),$
;           format='(1x,a2,a1,i2,1x,f11.3,1x,a2,e10.2,1x,3a2,1x,f5.2,1x,a4,e10.2)'
;
;
;;*************************
;;****** iteration up *****
;;*************************
;
;print,'iteration up'
;scrabn    = abn
;for iter=0,niter do begin
;print,'iter=',iter
;   iexit=1
;   scrabn.eabund = (scrabn.eabund)*(1.+alog10(2.711828)*deltaA) ;___augment abundance
;
;   ;___compute deviation for the new abundance
;   predict_intensity, scrabn, abnl, obs, gofts, goftsp, $
;                  demdat, predint, H, dlam, co, ncap7, MAXBLND
;
;   for i=0,n_elements(obs)-1 do begin
;      if (obs(i).g00 ne 0.0) then begin
;         if obs(i).hmib eq 1 then begin
;         j=0
;            case iweight of
;               0 : dev = (predint(i,j) - obs(i).g00)^2 / ((obs(i).g00)^2)
;               1 : dev = (predint(i,j) - obs(i).g00)^2 / ( obs(i).g00)
;               2 : dev = (predint(i,j) - obs(i).g00)^2 / ((obs(i).sg00)^2)
;            endcase
;            if dev(0) lt devst(i) then begin
;               new_abund(i)=scrabn.eabund(abnl(i,j))
;               iexit=0
;            endif
;            if dev(0) ge devst(i) and dev(0) le 1. then begin
;               max_abund(i)=scrabn.eabund(abnl(i,j))
;           iexit=0
;            endif
;         endif else if obs(i).hmib gt 1 then begin
;            predmult=predint(i,0:obs(i).hmib-1)
;            predmult=total(predmult,2)
;            case iweight of
;               0 : dev = (predmult - obs(i).g00)^2 / ((obs(i).g00)^2)
;               1 : dev = (predmult - obs(i).g00)^2 / (obs(i).g00)
;               2 : dev = (predmult - obs(i).g00)^2 / ((obs(i).sg00)^2)
;            endcase
;            if dev(0) lt devst(i) then begin
;               new_abund(i)=scrabn.eabund(abnl(i,j))
;               iexit=0
;            endif
;            if dev(0) ge devst(i) and dev(0) le 1. then begin
;               max_abund(i)=scrabn.eabund(abnl(i,j))
;               iexit=0
;            endif
;         endif
;         devst(i)=dev(0)
;      endif
;   endfor
;   if iexit ne 0 then goto,LABEL1
;
;   for l = 0, n-1 do print, obs(l).delem(0), '+', $
;           obs(l).izi(0), $
;           obs(l).swvlen(0), $
;           'A(',abn.symbamb(abnl(l,0)),')=',$
;           alog10(new_abund(l))+12,$
;           'dev=',devst(l),$
;           format='(1x,a2,a1,i2,1x,f11.3,1x,3a2,1x,f5.2,1x,a4,e15.2)'
;
;endfor
;
;print,'warning: iteration up truncated'
;
;LABEL1:
;
;;************************
;;**** iteration down ****
;;************************
;
;print,'iteration down'
;scrabn    = abn
;for iter=0,niter do begin
;print,'iter=',iter
;   iexit=1
;   scrabn.eabund = scrabn.eabund*(1.-alog10(2.711828)*deltaA)   ;___decrease abundance
;
;   ;___compute deviation for the new abundance
;   predict_intensity, scrabn, abnl, obs, gofts, goftsp, $
;                  demdat, predint, H, dlam, co, ncap7, MAXBLND
;
;   for i=0,n_elements(obs)-1 do begin
;      if (obs(i).g00 ne 0.0) then begin
;         if obs(i).hmib eq 1 then begin
;         j=0
;            case iweight of
;               0 : dev = (predint(i,j) - obs(i).g00)^2 / ((obs(i).g00)^2)
;               1 : dev = (predint(i,j) - obs(i).g00)^2 / ( obs(i).g00)
;               2 : dev = (predint(i,j) - obs(i).g00)^2 / ((obs(i).sg00)^2)
;            endcase
;            if dev(0) lt devst(i) then begin
;               new_abund(i)=scrabn.eabund(abnl(i,j))
;               iexit=0
;            endif
;            if dev(0) ge devst(i) and dev(0) le 1. then begin
;               min_abund(i)=scrabn.eabund(abnl(i,j))
;           iexit=0
;            endif
;         endif else if obs(i).hmib gt 1 then begin
;            predmult=predint(i,0:obs(i).hmib-1)
;            predmult=total(predmult,2)
;            case iweight of
;               0 : dev = (predmult - obs(i).g00)^2 / ((obs(i).g00)^2)
;               1 : dev = (predmult - obs(i).g00)^2 / (obs(i).g00)
;               2 : dev = (predmult - obs(i).g00)^2 / ((obs(i).sg00)^2)
;            endcase
;            if dev(0) lt devst(i) then begin
;               new_abund(i)=scrabn.eabund(abnl(i,j))
;               iexit=0
;            endif
;            if dev(0) ge devst(i) and dev(0) le 1. then begin
;               min_abund(i)=scrabn.eabund(abnl(i,j))
;               iexit=0
;            endif
;         endif
;         devst(i)=dev(0)
;      endif
;   endfor
;   if iexit ne 0 then goto,LABEL2
;   for l = 0, n-1 do print, obs(l).delem(0), '+', $
;           obs(l).izi(0), $
;           obs(l).swvlen(0), $
;           'A(',abn.symbamb(abnl(l,0)),')=',$
;           alog10(new_abund(l))+12,$
;           'dev=',devst(l),$
;           format='(1x,a2,a1,i2,1x,f11.3,1x,3a2,1x,f5.2,1x,a4,e10.2)'
;endfor
;
;print,'warning: iteration down truncated'
;
;LABEL2:
;
;;****************
;;****printout****
;;****************
;
;print,'printout'
;
;outfile='demap_abund.dat'
;openw,lou,outfile,/get_lun
;
;
;TElem_Array=obs[*].delem[j]
;lsort=SORT(TElem_Array)
;
;print,'ion','lambda','El''old A','new A','min A','max A',$
;    format='(1x,a5,1x,a11,1x,a2,1x,4a5)'
;printf,lou,'ion','lambda','El''old A','new A','min A','max A',$
;    format='(1x,a5,1x,a11,1x,a2,1x,4a5)'
;for i=0,n_elements(obs)-1 do begin
;   printf,lou,$
;           obs(lsort[i]).delem(j), '+', $
;           obs(lsort[i]).izi(j), $
;           obs(lsort[i]).swvlen(j), $
;           abn.symbamb(abnl(lsort[i],0)),$
;           alog10(abn.eabund(abnl(lsort[i],0)))+12,$
;           alog10(new_abund(lsort[i]))+12,$
;           alog10(min_abund(lsort[i]))+12,$
;           alog10(max_abund(lsort[i]))+12,$
;           format='(1x,a2,a1,i2,1x,f11.3,1x,a2,1x,4f5.2)'
;   print,$
;           obs(lsort[i]).delem(j), '+', $
;           obs(lsort[i]).izi(j), $
;           obs(lsort[i]).swvlen(j), $
;           abn.symbamb(abnl(lsort[i],0)),$
;           alog10(abn.eabund(abnl(lsort[i],0)))+12,$
;           alog10(new_abund(lsort[i]))+12,$
;           alog10(min_abund(lsort[i]))+12,$
;           alog10(max_abund(lsort[i]))+12,$
;           format='(1x,a2,a1,i2,1x,f11.3,1x,a2,1x,4f5.2)'
;endfor
;
;Elem_Array=TElem_Array[UNIQ(TElem_Array,SORT(TElem_Array))]
;Abund_Elem=fltarr(5,n_elements(Elem_Array))
;
;for i=0, n_elements(Elem_Array)-1 do begin
;   iuniq=where(TElem_Array eq Elem_Array[i])
;   Abund_Elem[0,i]=MEAN(new_abund[iuniq])
;   Abund_Elem[1,i]=MIN(min_abund[iuniq])
;   Abund_Elem[2,i]=MAX(min_abund[iuniq])
;   Abund_Elem[3,i]=MIN(max_abund[iuniq])
;   Abund_Elem[4,i]=MAX(max_abund[iuniq])
;endfor
;
;print,'Elem',' mean(A) ',' min(Amin) ',' max(Amin) ',' <-median->',' min(Amax) ',' max(Amax) ',$
;      format='(1x,a4,6a11)'
;printf,lou,'Elem',' mean(A) ',' min(Amin) ',' max(Amin) ',' <-median->',' min(Amax) ',' max(Amax) ',$
;      format='(1x,a4,6a11)'
;
;for i=0, n_elements(Elem_Array)-1 do begin
;   print, Elem_Array[i], alog10(Abund_Elem[0,i])+12,$
;                         alog10(Abund_Elem[1,i])+12,$
;                         alog10(Abund_Elem[2,i])+12,$
;                         alog10(MEDIAN([Abund_Elem[2,i],Abund_Elem[3,i]],/even))+12,$
;                         alog10(Abund_Elem[3,i])+12,$
;                         alog10(Abund_Elem[4,i])+12,$
;                         format='(2x,a2,2x,6(3x,f5.2,3x))'
;   printf,lou,$
;          Elem_Array[i], alog10(Abund_Elem[0,i])+12,$
;                         alog10(Abund_Elem[1,i])+12,$
;                         alog10(Abund_Elem[2,i])+12,$
;                         alog10(MEDIAN([Abund_Elem[2,i],Abund_Elem[3,i]],/even))+12,$
;                         alog10(Abund_Elem[3,i])+12,$
;                         alog10(Abund_Elem[4,i])+12,$
;                         format='(2x,a2,2x,6(3x,f5.2,3x))'
;endfor
;
;close,lou
;free_lun,lou
;
;goto,EXIT0
;;___end
;
;EXIT:
;print,'no line is within the observational error - quitting'
;
;EXIT0:
;
;end
;
