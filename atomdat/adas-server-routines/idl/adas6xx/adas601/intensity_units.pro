; Copyright (c) 1996, Strathclyde University.
; Alessandro Lanzafame	acl@phys.strath.ac.uk	24 April 1996
;+
; PROJECT:
;       ADAS support programs
;
; NAME:
;	INTENSITY_UNIT
;
; PURPOSE:
;	Converts observed intensity from [ergs cm-2 s-1 st-1] to
;       [photons cm-2 s-1 st-1].
;       
;
; EXPLANATION:
;       After reading observed line intensities data with rd_lines.
;       data is organised in an array of structures, each structure
;       containing data for a single blend. intens_unit.pro converts
;       units from [ergs cm-2 s-1 st-1] to [photons cm-2 s-1 st-1] if
;       intensity value is lower than 1e5. 
;       FROM VERSION 1.4 THIS IS DONE IF THE UNITS DESCRIPTOR IN
;       STRUCTURE "OBS" (OBS(#).UNITS) CONTAINS THE WORD "ERG"
;
; USE:
;       From the IDL prompt or within a program
;
;               intensity_unit,observed
;
;       will convert the data read by rd_lines.
;
; INPUTS:
;       OBSERVED() = Array of structure containing the observational
;                    quantities. Each structure in the array contains
;                    data for one blend of lines, which may consists
;                    of a single non-blended line.
;
;                  IBLND    Blend index. It can be not equal to the
;                           index of the array of structures itself
;
;                  G00      Total intensity of blend
;
;                  SG00     Uncertainty in the intensity measure of blend
;
;                  UNITS    Descriptor of intensity units
;
;                  HMIB     Number of lines in blend
;
;                  DELEM()  Chemical element for each line in the
;                           blend
;
;                  ION()    Ion charge: roman numeral in 
;                           spectroscopic notation.
;
;                  IZI()    Ion charge of the element
;
;                  SWVLEN() Spectroscopic line wavelength
;
;                  OWVLEN() Observed wavelwngth
;
;                  TRCONF() Configuration of levels
;
;                  INDX()   Line G-index, used to link with
;                           contribution function 
;;
; OPTIONAL INPUTS:
;	None
;
; OUTPUTS:
;       Same structure 'observed' as input in which the intensity units
;       are replaced if obs(#).units contains the word "erg".
;
; OPTIONAL OUTPUTS:
;	None
;
; KEYWORD PARAMETERS:
;	None
;
; CALLS:
;       None
;
; SIDE EFFECTS:
;       None
;
; CATEGORY:
;	Adas support and applications.
;	
; WRITTEN:
;       Alessandro Lanzafame, University of Strathclyde,  apr24-95
;
; MODIFIED:
;	1.0	Alessandro Lanzafame
;		First version
;	1.1	Tim Hammond
;		Put under S.C.C.S. control
;	1.2	William Osborn
;		Corrected minor syntax error
;	1.3	William Osborn
;		Commented-out printout of intensity unit information
;       1.4     Alessandro Lanzafame
;               Conversion is now done if the UNITS descriptor in
;               strucrure Obs contains the word 'ERG'
;
; VERSION:
;	1.0	24-04-95
;	1.1	03-05-96
;	1.2	07-05-96
;	1.3	15-07-96
;       1.4     08-06-02
;
;-----------------------------------------------------------------------
;-

PRO intensity_units,obs

    conv_factor = 5.034059191e7    ; 1 / (h*c*1.e8)
    eps = 1.e-2                    ; tolerance of deviation from the mean
                                   ; spectroscopic wavelength in blend

    for i=0, n_elements(obs) - 1 do begin

   ;-------------------------------------------------------
   ; Check that the multiplet intensity is to be converted
   ;-------------------------------------------------------

        ;if obs(i).g00 lt 1.e5 and obs(i).g00 gt 0.0 then begin
        if (STRPOS(obs(i).units,'ERG') ne -1) and obs(i).g00 gt 0.0 then begin

      ;---------------------------------------------------------------
      ; Check that spectroscopic wavelengths are present. 
      ; At least one value must be present
      ;---------------------------------------------------------------

            index = where(obs(i).swvlen,counts)
            if counts ne 0 then begin

         ;---------------------------------------
         ; Spectroscopic wavelengths were found
         ;---------------------------------------

         ;-----------------------------------
         ; Compute mean wavelength for blend
         ;-----------------------------------

                meanwave = TOTAL(obs(i).swvlen(index))/counts

         ;--------------------------------------------------------------
         ; Check that wavelengths do not differ by more than eps of the 
         ; mean
         ;--------------------------------------------------------------

                if max(abs((obs(i).swvlen(index)-meanwave)/meanwave)) lt eps $
		 then begin

            ;--------------------------------------
            ; Everything is alright, convert units
            ;--------------------------------------

                    obs(i).g00 = obs(i).g00 * conv_factor * meanwave

                    obs(i).sg00 = obs(i).sg00 * conv_factor * meanwave   

            ;--------------
            ; Info message
            ;--------------

;                    print,'INTENSITY_UNITS: Converted units for blend/line:'
;                    print,'|-----------------------------------------------------',$
;                          '-------------------------------------------------|'
;                    print,'                    spectr.      observed          ',$
;                          'configuration',				$
;                          '             configuration       G-indx '
;                    print,'                  | wavelength | wavelength |',$
;                          '                   ',			$
;                          '                                       |'
;                    print,'------------------|------------|------------|',$
;                          '-------------------',			$
;                          '---------------------------------------|
;                    for k=0, obs(i).hmib - 1 do begin
;                        print,obs(i).delem(k),'+',obs(i).izi(k),obs(i).ion(k),$
;                        obs(i).swvlen(k), obs(i).owvlen(k),		$
;                        obs(i).trconf(k),obs(i).indx(k),		$
;                        format='(1x,a2,a1,i2,1x,a8,1x,2f14.4,a51,4x,i3)'
;                    endfor
;                    print,'------------------------------------------------------',$
;                          '--------------------------------------------------'
;                    print

                endif else begin

            ;----------------------------------------------------------
            ; Wavelengths deviates from multiplet's mean by more then
            ; tollerance. Do not convert units and issue a warning
            ;-----------------------------------------------------------

                    print,"INTENSITY_UNITS WARNING: Wavelengths deviates from ",$
                          "multiplet's mean by more then tollerance: ",eps
                    print,'Cannot convert units for blend/line.'
;                    for k=0, obs(i).hmib - 1 do begin
;                        print, obs(i).delem(k),'+',obs(i).izi(k),obs(i).ion(k),$
;                        obs(i).swvlen(k), obs(i).owvlen(k),$
;                        obs(i).trconf(k),obs(i).indx(k),$
;                        format='(1x,a2,a1,i2,1x,a8,1x,2f14.4,a51,4x,i3)'
;                    endfor
                endelse
            endif else begin

      ;-------------------------------------------------------------------
      ; Spectroscopic wavelengths were not found. Issue a warning message.
      ;-------------------------------------------------------------------

                print,'INTENSITY_UNITS WARNING: Cannot convert units for blend/line.'
;                print, obs(i).delem(k),'+',obs(i).izi(k),obs(i).ion(k),$
;                       obs(i).swvlen(k), obs(i).owvlen(k),$
;                       obs(i).trconf(k),obs(i).indx(k),$
;                       format='(1x,a2,a1,i2,1x,a8,1x,2f14.4,a51,4x,i3)'

            endelse
        endif
    endfor
END
