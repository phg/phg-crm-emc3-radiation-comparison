; Copyright (c) 1996, Strathclyde University.
; SCCS info: Module @(#)$Header: /home/adascvs/idl/adas6xx/adas601/ercoll.pro,v 1.2 2004/07/06 13:49:21 whitefor Exp $ Date $Date: 2004/07/06 13:49:21 $
;+
; PROJECT:
;       ADAS support programs
;
; NAME:
;	ERCOLL.PRO
;
; PURPOSE:
;       Read a collection of G(T) functions from a collection file
;
; EXPLANATION:
;       Reads in G(T) functions for a set of transitions together 
;       with the element, ion charge, transition index, number
;       of temperature points at which each function is given,
;       the temperature points, approximate and spectroscopic 
;       wavelengths  
;
; USE:
;       From the IDL prompt or within a program
;
;               ercoll,'file.dat',gofts
;
;       will return a structure 'gofts' containing
;       data read from 'file.dat'
;
; INPUTS:
;       KERFILE = G(T) collection file name.
;
;       GOFTS =      Structure to cintain G(T) functions and associated
;                    quantities. 
;
;                  ELEMC()  Chemical element                  
;
;                  IZI()    Ion charge              
;
;                  INDX()   Transition index
;
;                  NG()     Number of temperature points 
;
;                  AWL()    Approximate wavelength
;
;                  SWL()    Spectroscopic wavelength
;
;                  FILEI()  ADF20 master file from which the function
;                           was extracted (using ADAS 506)
;
;                  T(,)     Log of temperature
;                             1st index: temperature
;                             2nd index: transition
;
;                  GOFT(,)  G(T) function
;                             1st index: temperature
;                             2nd index: transition
;
; OPTIONAL INPUTS:
;	None
;
; OUTPUTS:
;       GOFTS =      Structure containing G(T) functions and associated
;                    quantities. 
;
;                  ELEMC()  Chemical element                  
;
;                  IZI()    Ion charge              
;
;                  INDX()   Transition index
;
;                  NG()     Number of temperature points 
;
;                  AWL()    Approximate wavelength
;
;                  SWL()    Spectroscopic wavelength
;
;                  FILEI()  ADF20 master file from which the function
;                           was extracted (using ADAS 506)
;
;                  T(,)     Log of temperature
;                             1st index: temperature
;                             2nd index: transition
;
;                  GOFT(,)  G(T) function
;                             1st index: temperature
;                             2nd index: transition
;
;
; OPTIONAL OUTPUTS:
;	None
;
; KEYWORD PARAMETERS:
;	None
;
; CALLS:
;       xxhkey    Extract froma a line of text 'CTEXT' a response
;                 to a key in the form of '<CKEY> = <CANS>'. 
;  
;
; SIDE EFFECTS:
;       None known
;
; CATEGORY:
;	Adas support and applications.
;	
; WRITTEN:
;       Alessandro Lanzafame, University of Strathclyde, apr23-95
;
; MODIFIED:
;	1.0	Alessandro Lanzafame
;		First Release
;	1.1	William Osborn
;		Put under SCCS control
;	1.2	Alessandro Lanzafame
;		Removed include statements @params
; VERSION:
;	1.0	23-04-96
;	1.1	07-05-96
;	1.2	02-02-99
;
;-
;-----------------------------------------------------------------------------
pro ercoll,kerfile,gofts,MTRA,MTEMP

openr,lu,kerfile,/get_lun

 elemc = strarr(MTRA)
   izc = intarr(MTRA)
istrnc = intarr(MTRA)
   nga = intarr(MTRA)
   awl = fltarr(MTRA)
   swl = fltarr(MTRA)
 filei = strarr(MTRA)
    tx = dblarr(MTEMP,MTRA)
  gftx = dblarr(MTEMP,MTRA)

ntra = 0
while not eof(lu) do begin

;-----------------------------------------------------------------------
;  Read line identifying the element symbol, ion charge, number
;  of data points, approx wavelength and spectrscopic wavelength
;-----------------------------------------------------------------------

   if ntra ge MTRA then begin
      print,'ERCOLL ERROR:  MAX DIMENSION EXCEEDED'
      print,'MTRA =',MTRA,'NTRA =',ntra
      goto, QUIT
   endif

   cline = ' '
   readf,lu,format='(a80)',cline
   ionnam = strmid(cline,0,5)
   if strmid(ionnam,0,2) eq 'XX' then goto, EXIT
   if strmid(ionnam,1,1) eq '+' then begin
      elemc(ntra) = strmid(ionnam,0,1)+' '
   endif else begin
      elemc(ntra) = strmid(ionnam,0,2)
   endelse
   izc(ntra)=long(strtrim(strmid(ionnam,3,3),2)) 

   c4 = ' ' 
   xxhkey,cline,'ITRANS','/',c4
   istrnc(ntra)=long(c4)

   c4 = ' '
   xxhkey,cline,'NTEMP','/',c4
   nga(ntra)=long(c4)
   if nga(ntra) gt MTEMP then begin
      print,'ERCOLL ERROR:  MAX DIMENSION EXCEEDED'
      print,'MTEMP =',MTEMP,'NGA(',ntra,') =',NGA(ntra)
      goto, QUIT
   endif

   c7 = ' ' 
   xxhkey,cline,'APPWAV','/',c7
   awl(ntra)=float(c7)

   c7 = ' ' 
   xxhkey,cline,'SPWAV','/',c7
   swl(ntra)=float(c7)

;-----------------------------------------------------------------------
;  Read input data set name
;-----------------------------------------------------------------------

   readf,lu,format='(a80)',cline
   filei(ntra)=strtrim(cline,2)

;-----------------------------------------------------------------------
;  Read G(T) from collection file
;-----------------------------------------------------------------------

   tscr=dblarr(nga(ntra))
   gscr=dblarr(nga(ntra))

   readf,lu,tscr
   readf,lu,gscr
;   inozero=where(gscr ge 1e-30,count)
;   if count ne 0 then begin
;      tx(0:count-1,ntra)   = tscr(inozero)
;      gftx(0:count-1,ntra) = gscr(inozero)
;   endif
;   nga(ntra)=count

;   tx(0:nga(ntra)-1,ntra)    = tscr
   tx(0:nga(ntra)-1,ntra)    = 10^tscr
   gftx(0:nga(ntra)-1,ntra)  = gscr

   if nga(ntra) lt MTEMP then begin
      tx(nga(ntra):MTEMP-1,ntra)   = 0.0
      gftx(nga(ntra):MTEMP-1,ntra) = 0.0
   endif

   ntra = ntra +1
endwhile
EXIT:

;-----------------------------------------------------------------------

QUIT:

;-----------------------------------------------------------------------
; Close file and free unit
;-----------------------------------------------------------------------

close,lu
free_lun,lu

;-----------------------------------------------------------------------
; Set up structure gofts
;-----------------------------------------------------------------------

;gofts = { gofts                    ,$
;           ntra : ntra             ,$
;          elemc : elemc(0:ntra-1)  ,$ 
;            izi : izc(0:ntra-1)    ,$
;           indx : istrnc(0:ntra-1) ,$
;             ng : nga(0:ntra-1)    ,$
;            awl : awl(0:ntra-1)    ,$
;            swl : swl(0:ntra-1)    ,$
;          filei : filei(0:ntra-1)  ,$
;              t : tx               ,$
;           goft : gftx             }


           gofts.ntra = ntra
          gofts.elemc = elemc     
            gofts.izi = izc     
           gofts.indx = istrnc   
             gofts.ng = nga    
            gofts.awl = awl        
            gofts.swl = swl            
          gofts.filei = filei    
              gofts.t = tx                
           gofts.goft = gftx

          
end
