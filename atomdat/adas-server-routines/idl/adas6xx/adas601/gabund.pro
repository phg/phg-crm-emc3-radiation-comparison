; Copyright (c) 1995, Strathclyde University.
; Alessandro Lanzafame	acl@phys.strath.ac.uk	19 March 1996
; SCCS info: Module @(#)$Header: /home/adascvs/idl/adas6xx/adas601/gabund.pro,v 1.6 2004/07/06 13:57:39 whitefor Exp $ Date $Date: 2004/07/06 13:57:39 $
;+
; PROJECT:
;       ADAS support programs
;
; NAME:
;	GABUND.PRO
;
; PURPOSE:
;       Read abundances from data file
;
; EXPLANATION:
;       Reads in a list of chemical elements and their abundances
;
; USE:
;       From the IDL prompt or within a program
;
;               gabund,'file.dat',abn
;
;       will return a structure 'abn' containing
;       data read from 'file.dat'
;
; INPUTS:
;       ABUNDFILE = Abundance file name.
;
; OPTIONAL INPUTS:
;	None
;
; OUTPUTS:
;       ABN   =      Structure containing abundance data.
;
;                  NABUND    Number of elements contained in the data file
;
;                  SYMBAMB() Symbol of elements listed in the data file
;
;                  EABUND()  Abundance for element 
;
;
; OPTIONAL OUTPUTS:
;	None
;
; KEYWORD PARAMETERS:
;	None
;
; CALLS:
;       None  
;
; SIDE EFFECTS:
;       None
;
; CATEGORY:
;	Adas support and applications.
;	
; WRITTEN:
;       Alessandro Lanzafame, University of Strathclyde, mar19-95
;
; MODIFIED:
;	1.0	Alessandro Lanzafame
;		First version
;	1.1 	Tim Hammond
;		Put under S.C.C.S control
;	1.2	William Osborn
;		Removed spaces from before macro
;	1.3	Alessandro Lanzafame / William Osborn
;		Added possibility to read Log of abundance with 
;		Log(H)=12.0
;	1.4	William Osborn
;		S.C.C.S problem
;	1.5	Alessandro Lanzafame
;		Removed include statement @params
;	1.6   Richard Martin
;		Added check for empty line in input data file because
;		of SCCS data. 
;
; VERSION:
;	1.0	19-03-95
;	1.1	03-05-96
;	1.2	07-05-96
;	1.3	05-06-96
;	1.4	12-07-96
;	1.5	02-02-99
;	1.6	12-07-01
;
;-
;-----------------------------------------------------------------------------

PRO gabund,abundfile,abn,MABUND

    openr,lu,abundfile,/get_lun

    nabund=0
    s=strarr(1)
    e=dblarr(1)
    s=''
    dummy=''
    
    sscr=strarr(MABUND)
    escr=dblarr(MABUND)

    while not eof(lu) do begin
        readf,lu,dummy
	  if strtrim(dummy,2) eq '' then goto, endloop
        reads,dummy,format='(a2,e12.3)',s,e
        if s ne '' then begin
            sscr(nabund)=s
            escr(nabund)=e
            nabund=nabund+1
        endif
    endwhile

endloop:

    close,lu
    free_lun,lu

;----------------------------------------------------------------------
; Convert if abundance is given in Log(abundance) with Log(H)=12.0
;----------------------------------------------------------------------

l=where(strtrim(sscr,2) eq 'H',counts)
if counts eq 1 then begin
   if escr(l(0)) eq 12.0 then escr=10^(escr-12.0)
endif

;-----------------------------------------------------------------------
; Set up structure abn
;-----------------------------------------------------------------------

;abn = {  abn                       ,$
;         nabund : nabund           ,$
;        symbamb : sscr(0:nabund-1) ,$
;         eabund : escr(0:nabund-1) }

    abn.nabund = nabund           
    abn.symbamb = sscr(0:nabund-1) 
    abn.eabund = escr(0:nabund-1) 

END

