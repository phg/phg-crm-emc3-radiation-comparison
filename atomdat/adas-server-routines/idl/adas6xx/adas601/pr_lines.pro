; Copyright (c) 1996, Strathclyde University.
; SCCS info: Module @(#)$Header: /home/adascvs/idl/adas6xx/adas601/pr_lines.pro,v 1.1 2004/07/06 14:36:27 whitefor Exp $ Date $Date: 2004/07/06 14:36:27 $
;
;+
; PROJECT:
;       ADAS support programs
;
; NAME:
;	PR_LINES
;
; PURPOSE:
;	Prints on screen observed line intensities
;       read by rd_lines.
;       
;
; EXPLANATION:
;       After reading observed line intensities data with rd_lines.
;       data is organised in an array of structures, each structure
;       containing data for a single blend. pr_lines prints out
;       the array of structures on screen. 
;
; USE:
;       From the IDL prompt or within a program
;
;               pr_lines,observed
;
;       will print the data read by rd_lines on screen
;
; INPUTS:
;       OBSERVED() = Array of structure containing the observational
;                    quantities. Each structure in the array contains
;                    data for one blend of lines, which may consists
;                    of a single non-blended line.
;
;                  IBLND    Blend index. It can be not equal to the
;                           index of the array of structures itself
;
;                  G00      Total intensity of blend
;
;                  SG00     Uncertainty in the intensity measure of blend
;
;                  HMIB     Number of lines in blend
;
;                  DELEM()  Chemical element for each line in the
;                           blend
;
;                  ION()    Ion charge: roman numeral in 
;                           spectroscopic notation.
;
;                  IZI()    Ion charge of the element
;
;                  SWVLEN() Spectroscopic line wavelength
;
;                  OWVLEN() Observed wavelwngth
;
;                  TRCONF() Configuration of levels
;
;                  INDX()   Line G-index, used to link with
;                           contribution function 
;;
; OPTIONAL INPUTS:
;	None
;
; OUTPUTS:
;       Printout on screen
;
; OPTIONAL OUTPUTS:
;	None
;
; KEYWORD PARAMETERS:
;	None
;
; CALLS:
;       None
;
; SIDE EFFECTS:
;       None known
;
; CATEGORY:
;	Adas support and applications.
;	
; WRITTEN:
;       Alessandro Lanzafame, University of Strathclyde, mar14-95
;
; MODIFIED:
;	1.0	Alessandro Lanzafame
;		First Release
;	1.1	William Osborn
;		Put under SCCS control
; VERSION:
;	1.0	????????
;	1.1	07-05-96
;
;-

pro pr_lines,obs

lab=' '
outsrt=''
for i=0, n_elements(obs) - 1 do begin
   spawn,'clear'
   print
   print,'------------------------------------------------------',$
         '-------------------------------------------------'
   print,'            BLEND NUMBER: ',i+1
   print,'------------------------------------------------------',$
         '-------------------------------------------------'
   print
   print,'             BLEND INDEX: ',obs(i).iblnd
   print,'               INTENSITY: ',obs(i).g00
   print,'             UNCERTAINTY: ',obs(i).sg00
   print,'NUMBER OF LINES IN BLEND: ',obs(i).hmib
   print
   print,'------------------------------------------------------',$
         '--------------------------------------------------'
   print,'             LINES IN BLEND                            '
   print,'|-----------------------------------------------------',$
         '-------------------------------------------------|'
   print,'                    spectr.      observed          configuration',$
         '             configuration       G-indx '
   print,'                  | wavelength | wavelength |                   ',$
         '                                       |'
   print,'------------------|------------|------------|-------------------',$
         '---------------------------------------|


   for k=0, obs(i).hmib - 1 do begin

      print,obs(i).delem(k),'+',obs(i).izi(k),obs(i).ion(k),$
            obs(i).swvlen(k), obs(i).owvlen(k),$
            obs(i).trconf(k),obs(i).indx(k),$
            format='(1x,a2,a1,i2,1x,a8,1x,2f14.4,a51,4x,i3)'

   endfor
   print,'------------------------------------------------------',$
         '--------------------------------------------------'
   print
   print
   if i ne n_elements(obs) - 1 then begin
      print,'...return to continue, q to quit'
      read,lab
      if lab eq 'q' then goto, ERROR
   endif
endfor

ERROR:

end
