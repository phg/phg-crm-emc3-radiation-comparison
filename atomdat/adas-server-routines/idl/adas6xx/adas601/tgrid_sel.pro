; Copyright (c) 1995, Strathclyde University.
; SCCS info: Module @(#)$Header: /home/adascvs/idl/adas6xx/adas601/tgrid_sel.pro,v 1.2 2004/07/06 15:25:37 whitefor Exp $ Date $Date: 2004/07/06 15:25:37 $
; Alessandro Lanzafame	acl@phys.strath.ac.uk	16 April 1996
;---------------------------------------------------------------------
;
;+
; PROJECT:
;       ADAS support and application programs
;
; NAME:
;	tgrid_sel()
;
; PURPOSE:
;	User interface, processing options/input.
;
; EXPLANATION:
;	This routine creates and manages a pop-up window which allows
;	the user to select options and input data to control DEMAP
;	processing.
;
;       All I/O is through keywords parameters.
;
; USE:
;	This routine is DEMAP specific, see demap_main.pro for how it
;	is used.
;
; INPUTS:
;      None.
;
; OPTIONAL INPUTS:
;	None.
;
; OUTPUTS:
;       None.
;
; OPTIONAL OUTPUTS:
;       None
;
; KEYWORD PARAMETERS:
;	VALUE	 - A structure describing the value of the widget;
;		   {NTEMP:'', TMIN:''   , TMAX:'',
;                   STEP:'' , LOGTMIN:'', LOGTMAX:'', GRPMESS:''}
;
;		   NTEMP    Number of points
;		   TMIN     Minimum temperature, as a string.
;		   TMAX     Maximum temperature, as a string.
;                  STEP     Step.
;		   LOGTMIN  Log of minimum temperature, as a string.
;		   LOGTMAX  Log of maximum temperature, as a string.
;		   GRPMESS Error message for ranges input.
;
;       FONT     - A font to use for all text in this widget.
;
;	UVALUE	 - The output temperature grid structure as VALUE if 
;                  selection was made.
;
;
; CALLS:
;	CW_DEMAP_TGRIDSEL.
;
; SIDE EFFECTS:
;       None
;
; CATEGORY:
;	Compound Widget
;
; WRITTEN:
;       Alessandro Lanzafame, University of Strathclyde
;
; MODIFIED:
;	1.0	Alessandro Lanzafame   23 April 1996
;		First Release
;	1.1	Tim Hammond
;		Put under S.C.C.S. control
;	1.2	William Osborn
;		Changed name of structure grselval to tgrselval to avoid
;		conflict with cw_adas_gr_sel
;
; VERSION:
;	1.0	23-04-96
;	1.1	03-05-96
;	1.2	14-05-96
;
;-
;-----------------------------------------------------------------------------

PRO tgrid_sel_ev, event

    COMMON tgrid_sel_blk, action, grselval, newgrid

;---------------------------------------------------------------------------
;	Find the event type and copy to common 
;---------------------------------------------------------------------------

    action = event.action

    case action of

;---------------------------------------------------------------------------
;	'Done' button
;---------------------------------------------------------------------------

        'Done'  : begin

;---------------------------------------------------------------------------
;	Get the output widget value
;---------------------------------------------------------------------------

            child = widget_info(event.id, /child)
            widget_control, child, get_value=grselval
            newgrid.ntemp = long(grselval.ntemp)
            DT=(double(grselval.tmax)/double(grselval.tmin))^ 		$
                 (1.0/(long(grselval.ntemp)-1))
            newgrid.temp = grselval.tmin * DT^(FINDGEN(grselval.ntemp))
            widget_control, event.top, /destroy 
	end

;--------------------------------------------------------------------
;	'Cancel' button
;--------------------------------------------------------------------

        'Cancel': widget_control, event.top, /destroy
	
	else:	;Do nothing

    endcase

END

;-----------------------------------------------------------------------

PRO tgrid_sel,VALUE=value, UVALUE=uvalue, FONT=font

    COMMON tgrid_sel_blk, action, grselval, newgrid

    newgrid = value

;-----------------------------------------------------------------------
; Set defaults for keywords
;-----------------------------------------------------------------------


    IF NOT (KEYWORD_SET(value)) THEN begin
        grselval = { 	tgrselval, 					$
                     	NTEMP 	: 	'', 				$
			TMIN 	: 	'', 				$
			TMAX 	: 	'',				$
                     	STEP 	: 	'', 				$
			LOGTMIN : 	'', 				$
			LOGTMAX : 	'',				$
                     	GRPMESS	: 	'' 				}
    ENDIF ELSE begin
        DeltaT=(value.temp(value.ntemp-1)/value.temp(0))^(1.0/(value.ntemp-1))
        grselval={	tgrselval ,					$
             		NTEMP	:	strtrim(string(value.ntemp,	$
					format='(i4)')),		$
             		TMIN	:	strtrim(string(value.temp(0),	$
					format='(e12.4)')),		$
             		TMAX	:	strtrim(			$
					string(value.temp(value.ntemp-1),$
					format='(e12.4)')),		$
             		STEP	:	strtrim(string(alog10(DeltaT),	$
					format='(e12.4)')),		$
             		LOGTMIN	:	strtrim(			$
					string(alog10(value.temp(0)),	$
					format='(f12.6)')),		$
             		LOGTMAX	:	strtrim(			$
                			string(				$
					alog10(value.temp(value.ntemp-1)),$
					format='(f12.6)')),		$
             		GRPMESS	:	value.grpmess			}
    ENDELSE
    IF NOT (KEYWORD_SET(font)) THEN font = ''
    IF NOT (KEYWORD_SET(uvalue))  THEN uvalue = 0

;-----------------------------------------------------------------------
;	Base for the popup widget
;-----------------------------------------------------------------------

    outid = widget_base(TITLE='DEM TEMPERATURE GRID SELECTION', 	$
			XOFFSET=50, YOFFSET=0)

;--------------------------------------------------------------------------
; Declare compound widget for temperature grid selection
;--------------------------------------------------------------------------

    cwid = cw_demap_tgridsel(outid, VALUE=grselval, FONT=font)

;---------------------------------------------------------------------------
;	Realize the new widget
;---------------------------------------------------------------------------

    widget_control, outid, /realize

;---------------------------------------------------------------------------
;	make widget modal
;---------------------------------------------------------------------------

    xmanager, 'tgrid_sel', outid, event_handler='tgrid_sel_ev',		$
              /modal, /just_reg

;---------------------------------------------------------------------------
;	Return the output value from common
;---------------------------------------------------------------------------

    uvalue = newgrid

END
