; Copyright (c) 1995, Strathclyde University.
; Alessandro Lanzafame	acl@phys.strath.ac.uk	20 March 1996
; SCCS info: Module @(#)$Header: /home/adascvs/idl/adas6xx/adas601/link_kernel.pro,v 1.4 2004/07/06 14:11:38 whitefor Exp $ Date $Date: 2004/07/06 14:11:38 $
;+
; PROJECT:
;       ADAS support programs
;
; NAME:
;	LINK_KERNEL.PRO
;
; PURPOSE:
;       Link the G(T) functions to the observed intensities.
;
; EXPLANATION:
;       Given the structure 'gofts' containing the G(T) functions
;       a data file, this routine reorganise the G(T) functions
;       in a new structure 'goftsl' which maps the G(T) functions 
;       to the observed intensities stored in the structure 'obs'. 
;
; USE:
;       From the IDL prompt or within a program
;
;               link_kernel,obs,gofts,goftsl
;
;       will return a structure 'goftsl' containing the G(T) functions
;       mapped to the 'obs' structure.
;
; INPUTS:
;       MAXBLND  Parameter: maximum number of lines in blends.
;                Used as second dimension of goftsl and goftsp
;
;       OBS()    Array of structures containing the observed 
;                intensities and associated quantities. See
;                routine rd_lines for details.
;
;       GOFTS    Structure containing G(T) functions. See ercoll.pro
;                for details.
;
; OPTIONAL INPUTS:
;	None
;
; OUTPUTS:
;	MSEL	  Number of lines selected for integral inversion
;
;       GOFTSL(,) Array of indeces linking structure 'gofts' 
;		  (G-gunctions) with structure 'obs' for lines to be
;		  included in the integral inversion.
;
;	MPRE	  Number of lines for which the intensity prediction is
;		  sought
;
;       GOFTSP(,) Array of indeces linking structure 'gofts' 
;		  (G-gunctions) with structure 'obs' for lines not to be
;		  included in the integral inversion - prediction only
;
;
; OPTIONAL OUTPUTS:
;	None
;
; KEYWORD PARAMETERS:
;	None
;
; CALLS:
;       None  
;
; SIDE EFFECTS:
;       None
;
; CATEGORY:
;	Adas support and applications.
;	
; WRITTEN:
;       Alessandro Lanzafame, University of Strathclyde, mar20-95
;
; MODIFIED:
;	1.0	Alessandro Lanzafame
;		First version
;	1.1	Tim Hammond
;		Put under S.C.C.S. control
;	1.2	Alessandro Lanzafame
;		Changed G(T) threshold from 1.e-30 to 1.e-40 
;	1.3	Alessandro Lanzafame
;		Abolished G(T) threshold
;	1.4	Alessandro Lanzafame
;		Removed include statement @params
;		Array of structures goftsl now substituted with an array
;		of pointing indeces.
;		Added array of pointing indeces goftsp for predicted
;		only intensities to be compared with observations
;		indicated with negative G-indeces in the input intensity
;		file.
;
; VERSION:
;	1.0	20-03-95
;	1.1	03-05-96
;	1.2	21-02-97
;	1.3	26-01-99
;	1.4	02-02-99
;
;-
;-----------------------------------------------------------------------------

pro link_kernel,obs,gofts,msel,goftsl,mpre,goftsp,MAXBLND

goftsl=intarr(n_elements(obs),MAXBLND)
goftsp=intarr(n_elements(obs),MAXBLND)
msel=0
mpre=0

for l = 0, n_elements(obs) - 1 do begin
   for k = 0, obs(l).hmib - 1 do begin 
      for j = 0, gofts.ntra - 1 do begin
         if STRUPCASE(obs(l).delem(k)) eq STRUPCASE(gofts.elemc(j)) and $
         obs(l).izi(k) eq gofts.izi(j) and $
         abs(obs(l).indx(k)) eq gofts.indx(j) then begin
            inozero= where(gofts.goft(*,j) gt 0.0 ,counts)
            if counts ne 0 then begin
		if (obs(l).indx(k) gt 0) then begin
			msel=msel+1 
			goftsl(l,k) = j
		endif
		if (obs(l).indx(k) lt 0) then begin
			mpre=mpre+1
			goftsp(l,k) = j
                endif
            endif else begin
               print,'%link_kernel: GOFT for '+gofts.elemc(j)+gofts.izi(j)+$
                     ' index='+gofts.indx(j)+'is a nul vector - ignored'
            endelse
            goto, EXIT
         endif
      endfor
      print,'*** LINK_KERNEL.PRO ERROR:         ***'
      print,'*** Cannot find GOFT function for: ***'
      print,obs(l).delem(k),'+',obs(l).izi(k),'  INDEX=',obs(l).indx(k)
EXIT:
   endfor
endfor

end

