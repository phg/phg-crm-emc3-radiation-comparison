; Copyright (c) 1995, Strathclyde University.
; SCCS info: Module @(#)$Header: /home/adascvs/idl/adas6xx/adas601/tgrid_log.pro,v 1.2 2004/07/06 15:25:31 whitefor Exp $ Date $Date: 2004/07/06 15:25:31 $
; Alessandro Lanzafame	acl@phys.strath.ac.uk	21 November 1995
;+
; PROJECT:
;       ADAS support and application programs
;
; NAME:
;       TGRID_LOG
;
; PURPOSE:
;
; EXPLANATION:
;
; USE:
;
; INPUTS:
;       None
;
; OPTIONAL INPUTS:
;       None
;
; OUTPUTS:
;       None
;
; OPTIONAL OUTPUTS:
;       None
;
; KEYWORD PARAMETERS:
;       None
;
; CALLS:
;       None
;
; SIDE EFFECTS:
;
; CATEGORY:
;       Adas support.
;
; WRITTEN:
;       Alessandro Lanzafame, University of Strathclyde, 21 November 1995
;
; MODIFIED:
;       1       Alessandro Lanzafame
;               First release.
;       1.1     Tim Hammond
;               Put under S.C.C.S. Control
;	1.2	William Osborn
;		Removed spaces from before @cdemdat macro
;
; VERSION:
;       1       21-11-95
;       1.1     03-05-96
;       1.2     07-05-96
;
;-----------------------------------------------------------------------
;-

PRO tgrid_log

@cdemdat

    Ntemp  =100
    Tmin = 1.e4
    Tmax = 1.e7
    DT = (Tmax/Tmin)^(1.0/(Ntemp-1))
    Temp = Tmin * DT^(FINDGEN(Ntemp))

    demdat.ntemp = ntemp 
    demdat.temp  = Temp

END
