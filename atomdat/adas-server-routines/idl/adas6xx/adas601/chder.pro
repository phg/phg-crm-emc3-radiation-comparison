; Copyright (c) 1997, Alessandro Lanzafame, Universita' di Catania
; SCCS info: Module @(#)$Header: /home/adascvs/idl/adas6xx/adas601/chder.pro,v 1.1 2004/07/06 12:04:30 whitefor Exp $ Date $Date: 2004/07/06 12:04:30 $
;+
; PROJECT:
;	Mathematics - utilities
;
; NAME:
;	CHDER
;
; NOTE:
;
; PURPOSE: COMPUTE THE CHEBYSHEV COEFFICIENTS OF THE DERIVATIVE OF
;          A FUNCTION FROM ITS CHEBYSHEV COEFFICIENTS.
;          REFERENCES: Press, H. W. et al "Numerical Recipes", 1992
;
; CALLING PROGRAMS: VARIOUS
;
; EXPLANATION:
;
; INPUTS:
;	(R*8)     A   = Lower limit of the abscissa interval
;
;	(R*8)     B   = Upper limit of the abscissa interval
;
;	(R*8)     C() = Array of Chebyshev coefficients
;
;	(I*4)     N   = Order of the series (maximum degree of 
;                       polynomial expansion = M-1)
;
; OPTIONAL INPUTS:
;	None
;
; OUTPUTS: 
;	(R*8)  CDER() = Array of Chebishev coefficients of the 
;                       derivative   
;
; OPTIONAL OUTPUTS:
;	None
;
; KEYWORD PARAMETERS:
;       None
;
; CALLS:
;       None
;
; SIDE EFFECTS:
;
; CATEGORY:
;       Maths - general
;
; WRITTEN:
;       A. C. Lanzafame,  Istituto di Astronomia
;                         Universita' di Catania
;                         Citta' Universitaria
;                         Viale Andrea Doria 6
;                         95125 Catania, Italy
;                         e-mail: alanzafame@ct.astro.it
;
; MODIFIED:
;       1.1     Alessandro Lanzafame
;               First release.
; VERSION:
;       1.1     18-02-97
;
;-

PRO CHDER, A, B, C, CDER, N

      CDER = DBLARR(N)

      CDER(N-1)   = 0.0D0
      CDER(N-2) = 2*(N-1)*C(N-1)
      IF (N GE 3) THEN BEGIN
         FOR J=N-2,1,-1 DO BEGIN
            CDER(J-1) = CDER(J+1) + 2*J*C(J)
         ENDFOR
      ENDIF

      CON = 2. / (B-A)

      FOR J = 0, N-1 DO BEGIN
         CDER(J) = CDER(J)*CON
      ENDFOR

END
