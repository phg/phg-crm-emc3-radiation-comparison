; Copyright (c) 1995 Strathclyde University .
; SCCS info: Module @(#)$Header: /home/adascvs/idl/adas6xx/adaslib/popin_fslider.pro,v 1.1 2004/07/06 14:36:03 whitefor Exp $ Date $Date: 2004/07/06 14:36:03 $
;
;+
; PROJECT:
;       ADAS IBM MVS to DEC UNIX conversion
;
; NAME:
;       POPIN_FSLIDER()
;
; PURPOSE:
;       Produces a popup window with a warning message and a typein box.
;
; EXPLANATION:
;       This is simply a way of prompting the user to enter a numerical
;	value. A box pops up, with an optional message and title and
;	a typein text widget. When the user enters a value it is checked
;	to ensure that it is a good number and if it is the window is
;	closed and the value passed back to the calling routine, 
;	otherwise the user must try again.
;
; INPUTS:
;       None
;
; OPTIONAL INPUTS:
;       None
;
; OUTPUTS:
;       The return value of this function is the string of the number
;	entered by the user.
;
; OPTIONAL OUTPUTS:
;       None
;
; KEYWORD PARAMETERS:
;       MESSAGE - A string; A message which appears in the widget.
;                 Default 'Please enter a value: '
;
;       TITLE   - A string; The title of the popup window.  Default 'Warning!'
;
;       XOFFSET - Integer; X offset of pop-up window in pixels. Default 500.
;
;       YOFFSET - Integer; Y offset of pop-up window in pixels. Default 500.
;
;       FONT    - A font to use for all text in this widget.
;
; CALLS:
;       NUM_CHK - to check that the input is a valid number.
;	XMANAGER
;
; SIDE EFFECTS:
;       One other routine is included which ise used to manage the
;       widget;
;
;       POPIN_FSLIDER_EVENT
;
; CATEGORY:
;       Compound Widget
;
; WRITTEN:
;       Richard Martin, University of Strathclyde.
;	  Based on popin.pro v1.3 .
;
; MODIFIED:
;       1.1	Richard Martin
;		First release.
;
; VERSION:
;      1.1	04-12-98
;
;
;-
;-----------------------------------------------------------------------------

PRO popin_fslider_event, event


    COMMON popin_fslider_com, value


                ;**** Base ID of compound widget ****

    parent=event.handler

                ;**********************************************
                ;**** Retrieve the user value state        ****
                ;**** Get id of first_child widget because ****
                ;**** user value "state" is stored there   ****
                ;**********************************************

    first_child = widget_info(parent, /child)
    widget_control, first_child, get_uvalue=state, /no_copy

                ;*********************************
                ;**** Clear previous messages ****
                ;*********************************

    widget_control, state.messid, set_value='                                  '

                ;************************
                ;**** Process events ****
                ;************************

    destroy = 0
    CASE event.id OF
	state.minval: begin
	    widget_control, state.minval, get_value=userstring
	    if num_chk(userstring(0)) eq 0 then begin
		min =double(userstring(0))
		widget_control, state.maxval, get_value=userstring
	      max = double(userstring(0))
	      widget_control, state.slidval, get_value=userstring
	      slid = double(userstring(0))
	      if ( min ge max ) then begin
			messvalue = 'min > max, please try again'
			widget_control, state.messid, set_value=messvalue
	      endif else if ( min gt slid) then begin
			messvalue = 'min > slider value, please try again'
			widget_control, state.messid, set_value=messvalue	      	
	      endif else begin
	      	value.min=min
	     	endelse	
	    endif else begin
		messvalue = 'Invalid (min) entry, please try again'
		widget_control, state.messid, set_value=messvalue
	    endelse
	end

	state.maxval: begin	
	    widget_control, state.maxval, get_value=userstring
	    if num_chk(userstring(0)) eq 0 then begin
		max = double(userstring(0))
		widget_control, state.minval, get_value=userstring
	      min = double(userstring(0))
	      widget_control, state.slidval, get_value=userstring
	      slid = double(userstring(0))	      
	      if ( max lt min ) then begin
			messvalue = 'max < min, please try again'
			widget_control, state.messid, set_value=messvalue
	      endif else if (max lt slid) then begin
			messvalue = 'max < slider value, please try again'
			widget_control, state.messid, set_value=messvalue	      	
	      endif else begin
	      	value.max=max
	     	endelse			
	    endif else begin
		messvalue = 'Invalid (max) entry, please try again'
		widget_control, state.messid, set_value=messvalue
	    endelse
	end

	state.slidval: begin	
	    widget_control, state.slidval, get_value=userstring
	    if num_chk(userstring(0)) eq 0 then begin
		slid = double(userstring(0))
	      widget_control, state.maxval, get_value=userstring
	      max = double(userstring(0))
	      widget_control, state.minval, get_value=userstring
	      min =double(userstring(0))
	      if ( slid lt min OR slid gt max ) then begin
			messvalue = 'Value out of range, please try again'
			widget_control, state.messid, set_value=messvalue	
		endif else begin
			value.slid=slid
		endelse      	      		
	    endif else begin
		messvalue = 'Invalid entry, please try again'
		widget_control, state.messid, set_value=messvalue
	    endelse
	end
	
	state.dbut: begin
		widget_control, event.top, /destroy		
		destroy = 1	
	end
	
	ELSE:			;do nothing

    ENDCASE

                ;*********************************************
                ;*** make "state" available to other files ***
                ;*********************************************

    if destroy eq 0 then widget_control, first_child, set_uvalue=state, /no_copy

END

;-----------------------------------------------------------------------------

FUNCTION popin_fslider, MESSAGE=message, TITLE=title, XOFFSET=xoffset, 		$
                YOFFSET=yoffset, FONT=font, VALUE = value_in, NOMAX= nomax,	$
                NOMIN=nomin

    COMMON popin_fslider_com, value

    ON_ERROR, 2

                ;**** Set defaults for keywords ****
		;**** Default value is set later ***
    IF NOT (KEYWORD_SET(message)) THEN message = 'Please enter a value: '
    IF NOT (KEYWORD_SET(title)) THEN title = 'Slider values'
    IF NOT (KEYWORD_SET(xoffset)) THEN xoffset = 500
    IF NOT (KEYWORD_SET(yoffset)) THEN yoffset = 500
    IF NOT (KEYWORD_SET(font)) THEN font = ''
    IF NOT (KEYWORD_SET(nomax)) THEN nomax=0
    IF NOT (KEYWORD_SET(nomin)) THEN nomin=0
    
                ;**** create titled base widget ****

    topparent = widget_base(title=title, xoffset=xoffset,		$
                            yoffset=yoffset, /column)

                ;******************************************************
                ;**** Create a dummy widget just to hold value of *****
                ;**** "state" variable so as not to get confused  *****
                ;**** with any other values. Adopt IDL practice   *****
                ;**** of using first child widget                 *****
                ;******************************************************

    first_child = widget_base(topparent)

    parent = widget_base(first_child, /column)

                ;**** Message ****

    rc = widget_label(parent, value=message, font=font)
    rc = widget_label(parent, value='     ', font=font)

    minbase = widget_base(parent,/row)
    maxbase = widget_base(parent,/row)    
    slidvalbase= widget_base(parent,/row)
    
		;**** Typein ****

    IF NOT (KEYWORD_SET(value_in)) THEN begin
	value = {min: 0.0, max: 0.0, slid: 0.0}
	  rc = widget_label(minbase,value='         Min:', font=font)
        minval = widget_text(minbase, xsize=8, font=font, /editable,/all_events)
	  rc = widget_label(maxbase,value='         Max:', font=font)
        maxval = widget_text(maxbase, xsize=8, font=font, /editable,/all_events)
	  rc = widget_label(slidvalbase,value='Slider value:', font=font)
        slidval= widget_text(slidvalbase, xsize=8, font=font, /editable,/all_events)
    endif else begin
	value = value_in
	  rc = widget_label(minbase,value='         Min:', font=font)
        minval = widget_text(minbase, value=string(value.min,format='(e12.4)'), $
				 xsize=8, font=font, /editable,/all_events)
	  rc = widget_label(maxbase,value='         Max:', font=font)
        maxval = widget_text(maxbase, value=string(value.max,format='(e12.4)'), $
				 xsize=8, font=font, /editable,/all_events)
	  rc = widget_label(slidvalbase,value='Slider value:', font=font)
        slidval= widget_text(slidvalbase, value=string(value.slid,format='(e12.4)'), $
				 xsize=8, font=font, /editable,/all_events)  			    
    endelse
    rc = widget_label(parent, value='     ', font=font)
    dbut = widget_button(parent,value='Done')

		;**** warning message ****

    messid = widget_label(parent, value='                                        ', font=font)

    IF (KEYWORD_SET(nomax)) then widget_control, maxbase, sensitive=0
    IF (KEYWORD_SET(nomin)) then widget_control, minbase, sensitive=0
                ;*************************************************
                ;**** create a state structure for the pop-up ****
                ;****                window.                  ****
                ;*************************************************

    new_state = {minval		:	minval,			$
    		     maxval		:	maxval,			$
    		     slidval	:	slidval,			$
	         messid		:	messid,			$
	         dbut		: 	dbut				}

                 ;**** Save initial state structure ****

    widget_control, first_child, set_uvalue=new_state, /no_copy

                ;**** realize the widget ****

    widget_control, topparent, /realize
    widget_control, minval, /input_focus

                ;**** make widget modal ****

    xmanager, 'popin_fslider', topparent, /modal, /just_reg

                ;**** Return the output value from common ****

    RETURN, value

END
