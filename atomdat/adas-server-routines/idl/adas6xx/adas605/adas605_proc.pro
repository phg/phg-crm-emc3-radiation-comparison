;+
; PROJECT:
;       ADAS 
;
; NAME:
;       ADAS605_PROC
;
; PURPOSE: 
;       This function puts up a widget allowing the user to
;	adjust the parameters of an AFG feature and plots
;	the results in real time.
;
; EXPLANATION:
;       Self contained GUI code, uses pointers to get data back
;       from the form bypassing the old ADAS standard way of
;       generating an unnecessary compound widget. It comes
;       with an associated event handler. It also contains
;	adas605_proc_update which is eventually called by
;	parameter widgets (see cw_adas605_params and
;	cw_adas605_param) when something is updated.
;     
; INPUTS:
;        obj         - An AFG object - see afg_api__define for more
;                      details.
;
; OPTIONAL INPUTS:
;       None
;
; OUTPUTS:
;       The function returns a string rep
;           rep = 'CONTINUE' if everything is ok
;           rep = 'CANCEL'   if there is a problem or indeed if the user
;                            closes the widget with the mouse. 
;           rep = 'MENU'     go directly back to ADAS menu system
;
; OPTIONAL OUTPUTS:
;       None
;
; KEYWORD PARAMETERS:
;       FONT_LARGE - The name of a larger font e.g 'courier_bold14'
;       FONT_SMALL - The name of a smaller font e.g 'courier_bold12'
;	BITFILE	   - Full path to ADAS menu bitmap
;
;
; CALLS:
;                            
;       AFG                  : Indirectly via method calls to obj
;       cw_adas605_params    : A CW which displays a list of AFG
;                              parameters and a means to edit them.
;      
; SIDE EFFECTS:
;       None
;
; CATEGORY:
;       Widgets
;
; WRITTEN:
;       Allan Whiteford
;
; MODIFIED:
;       1.1     Allan Whiteford
;               First release 
;
; VERSION:
;       1.1     22-10-2008
;
;-
;-----------------------------------------------------------------------------

pro adas605_proc_update,wid,nocalc=nocalc,alters=alters
	widget_control,wid,get_uvalue=info
        widget_control,info.graph,get_value=w
        widget_control,info.message,set_value=''
        wset,w
	obj=info.obj
        
        if not keyword_set(docalc) then begin
	        result=obj->docalc()
        	if result eq 0 then begin
          		widget_control,info.message,set_value=obj->geterrmsg()
			erase
        		return
                endif
        endif
        
        widget_control,info.explicit,get_uvalue=explicit
        if explicit eq 1 then begin
        	widget_control,info.xmin,get_value=xmin
        	widget_control,info.xmax,get_value=xmax
        	widget_control,info.ymin,get_value=ymin
        	widget_control,info.ymax,get_value=ymax
                xrange=[xmin,xmax]
                yrange=[ymin,ymax]
        endif

	if keyword_set(alters) then begin
        	widget_control,info.compound,/set_value
        endif
        
	adas605_plot,obj,xrange=xrange,yrange=yrange
end

pro adas605_proc_event,event
	widget_control,event.top,get_uvalue=info
        
        if event.id eq info.graph then begin
        	; do rubber band drawing here
        endif
        
        if event.id eq info.cancelid then begin
		*info.ptrtoformdata={cancel:1,menu:0}
        	widget_control,event.top,/destroy
		return
        end
        if event.id eq info.menuid then begin
		*info.ptrtoformdata={cancel:0,menu:1}
        	widget_control,event.top,/destroy
		return
        end

        if event.id eq info.doneid then begin
		*info.ptrtoformdata={cancel:0,menu:0}
        	widget_control,event.top,/destroy
	end
        
        if event.id eq info.xmin or event.id eq info.xmax or event.id eq info.ymin or event.id eq info.ymax then begin
        	adas605_proc_update,event.top,/nocalc
        endif
        
        if event.id eq info.explicit then begin
		widget_control,info.explicit,set_uvalue=event.select
		widget_control,info.xmin,sensitive=event.select
		widget_control,info.xmax,sensitive=event.select
		widget_control,info.ymin,sensitive=event.select
		widget_control,info.ymax,sensitive=event.select
        	adas605_proc_update,event.top,/nocalc
        endif
        
        if event.id eq info.current then begin
        	adas605_proc_update,event.top
        	widget_control,info.graph,get_value=w
	        wset,w
                widget_control,info.explicit,set_uvalue=1,set_button=1
                widget_control,info.xmin,set_value=strtrim(string(!x.crange[0]),2),sensitive=1
                widget_control,info.xmax,set_value=strtrim(string(!x.crange[1]),2),sensitive=1
                widget_control,info.ymin,set_value=strtrim(string(!y.crange[0]),2),sensitive=1
                widget_control,info.ymax,set_value=strtrim(string(!y.crange[1]),2),sensitive=1
        	adas605_proc_update,event.top,/nocalc
        endif
end


function adas605_proc,obj,bitfile=bitfile,font_large=font_large,font_small = font_small
	parent = widget_base(column=1, title='ADAS605 PROCESSING', xoffset=100, yoffset=1)

	main=widget_base(parent,/column)

	holder=widget_base(main,/row)
        leftbase=widget_base(holder,/column)
        graph=widget_draw(leftbase,xsize=640,ysize=480,/motion_events,/button_events)
	tbase=widget_base(leftbase,/frame,column=3,/grid)
        ttbase=widget_base(tbase,/row,/nonexclusive)
        explicit=widget_button(ttbase,value='Explicit Scaling',uvalue=0)
        ttbase=widget_base(tbase,/row)
        current=widget_button(ttbase,value='Use current values')
        ttbase=widget_base(tbase,/row)
        label=widget_label(ttbase,value='X-min')
        xmin=widget_text(ttbase,value='',/editable,sensitive=0,/all_events)
        ttbase=widget_base(tbase,/row)
        label=widget_label(ttbase,value='Y-min')
        ymin=widget_text(ttbase,value='',/editable,sensitive=0,/all_events)
        ttbase=widget_base(tbase,/row)
        label=widget_label(ttbase,value='X-max')
        xmax=widget_text(ttbase,value='',/editable,sensitive=0,/all_events)
        ttbase=widget_base(tbase,/row)
	label=widget_label(ttbase,value='Y-max')
        ymax=widget_text(ttbase,value='',/editable,sensitive=0,/all_events)
       
	dummy1=widget_base()
	dummy2=cw_adas605_params(dummy1,obj,font_small=font_small,font_large=font_large)

	scrollbase=widget_base(holder,/scroll,/column,x_scroll_size=(widget_info(dummy1,/geom)).xsize+50)
        compound=cw_adas605_params(scrollbase,obj,update_event={rtn:'adas605_proc_update',wid:parent},font_small=font_small,font_large=font_large)

        widget_control,dummy1,/destroy
        
	menufile = bitfile + '/menu.bmp'
	read_X11_bitmap, menufile, bitmap1

        mrow     = widget_base(parent,/row)
        menuid      = widget_button(mrow,value=bitmap1,font=font_large)
        cancelid = widget_button(mrow,value='Cancel',font=font_large)
        doneid   = widget_button(mrow,value='Done',font=font_large)
        message = widget_label(mrow,value='                                                                             ')
        widget_control, parent, /realize
        ptrtoformdata = ptr_new({cancel:1})

	info = { obj		 :  obj,		$
        	 graph           :  graph,		$
                 doneID          :  doneID,             $
	         menuID          :  menuID,             $
	         cancelID        :  cancelID,           $
	         compound        :  compound,           $
	         xmin		 :  xmin,               $
	         xmax		 :  xmax,               $
	         ymin		 :  ymin,               $
	         ymax		 :  ymax,               $
	         message         :  message,	        $
                 explicit	 :  explicit,		$
	         current	 :  current,		$
                 ptrToFormData   :  ptrToFormData       }  

	widget_control, parent, Set_UValue=info

	adas605_proc_update,parent

	xmanager, 'adas605_proc', parent, event_handler='adas605_proc_event'

	formdata = *ptrtoformdata

	if formdata.menu eq 1 then return,'MENU'
	if formdata.cancel eq 1 then return,'CANCEL'

	return,'CONTINUE'	
end
