;+
; PROJECT:
;       ADAS 
;
; NAME:
;       CW_ADAS605_PICKFILE
;
; PURPOSE: 
;       Display an interface for a user to pick a file.
;
; EXPLANATION:
;       Display a file or list of files and provide a button
;       for the user to press for the selection to be
;       changed. Also optionally call a user-supplied
;       routine to notify that the value has changed.
;       The underlying AFG object determines whether one
;       or multiple files can be selected.
;     
; INPUTS:
;       tlb         - Top level base to store the CW in 
;       obj         - An AFG object - see afg_api__define for more
;                      details.
;	idx         - Index of the parameter inside the AFG object
;
; OPTIONAL INPUTS:
;       UPDATE_EVENT- Structure:
;			  RTN : Routine to call when a value
;                               is changed
;			  WID : Widget ID to pass to routine
;       FONT        - The name of a font to use.
;
; OUTPUTS:
;       Returns WID of created CW.
;
; CALLS:
;       
;      
; SIDE EFFECTS:
;       Will modify underyling AFG object directly.
;
; CATEGORY:
;       Widgets
;
; WRITTEN:
;       Allan Whiteford
;
; MODIFIED:
;       1.1     Allan Whiteford
;               First release 
;       1.2     Martin O'Mullane
;               Point file opening dialogue to ADASCENT.
;
; VERSION:
;       1.1     23-02-2009
;       1.2     19-12-2015
;
;-
;-----------------------------------------------------------------------------

pro cw_adas605_pickfile_set,id,val
	widget_control,id,get_uvalue=info
	if info.multi eq 0 then begin
		widget_control,info.file,set_value=(info.obj->getpars()).(info.idx)
	endif

end

pro cw_adas605_pickfile_event,event
	widget_control,event.handler,get_uvalue=info
        
        if event.id eq info.button then begin
                use_path = getenv('ADASCENT')
        	file=dialog_pickfile(multiple_files=info.multi, /must_exist, path=use_path)
		
                if info.multi eq 0 then begin
			pars=info.obj->getpars()
			pars.(info.idx)=file
			ok=info.obj->setpars(pars=pars)
			adas605_update,info.update_event,alters=(info.obj->getdesc()).parameters.(info.idx).alterslimits
		endif

        endif
	


end

function cw_adas605_pickfile,tlb,obj,idx,update_event=update_event,font_small=font_small,font_large=font_large
	pardesc=obj->getdesc()
	if n_elements(update_event) eq 0 then update_event=-1

	main=widget_base(tlb,/row)  
	
        multi=0
	if pardesc.parameters.(idx).disptype eq 'filelist' then multi=1

	if multi eq 0 then begin
        	file=widget_text(main,font=font_small)
                button=widget_button(main,font=font_small,value='Change...')
        endif

	info={file:file, $
              button:button, $
              multi:multi, $
              update_event:update_event, $
              obj:obj, idx:idx }

	widget_control,main,set_uvalue=info
        widget_control,main,event_pro='cw_adas605_pickfile_event',pro_set_value='cw_adas605_pickfile_set'
        widget_control,main,/set_value
        return,main
end
