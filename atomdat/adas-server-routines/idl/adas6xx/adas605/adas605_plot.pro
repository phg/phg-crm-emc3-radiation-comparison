;+
; PROJECT:
;       ADAS 
;
; NAME:
;       ADAS605_PLOT
;
; PURPOSE: 
;       Draws a plot to the current graphics device from the
;	contents of an AFG object.
;
; EXPLANATION:
;       This code takes an AFG object, gets the values back from
;	it and produces a plot. Either a standard plot or a
;	stick plot depending on the type of AFG object.
;     
; INPUTS:
;        obj         - An AFG object - see afg_api__define for more
;                      details.
; OPTIONAL INPUTS:
;       None
;
; OUTPUTS:
;       The function returns a string rep
;           rep = 'MENU'     To go back to the menu system
;           rep = 'CANCEL'   To go back to the processing panel
;
; OPTIONAL OUTPUTS:
;       None
;
; KEYWORD PARAMETERS:
;       title		- Title for the plot
;	xrange		- X-range for plot
;	yrange		- Y-range for plot
;       nocalc	        - Don't have AFG recalculate, just
;			  use the results stored in it.
;
; CALLS:
;       AFG		: Indirectly via method calls to obj
;
; SIDE EFFECTS:
;       Draws to the current graphics device.
;
; CATEGORY:
;       Plotting
;
; WRITTEN:
;       Allan Whiteford
;
; MODIFIED:
;       1.1     Allan Whiteford
;               First release 
;
; VERSION:
;       1.1     22-10-2008
;
;-
;-----------------------------------------------------------------------------

pro adas605_plot,obj,title=title,xrange=xrange,yrange=yrange

	output=obj->getres()

	if n_elements(xrange) gt 0 && total(abs(xrange)) gt 0 then xstyle=1
	if n_elements(yrange) gt 0 && total(abs(yrange)) gt 0 then ystyle=1

	if (obj->getwvresolved()) eq 0 then begin
		PLOT,output.wv, output.intensity, xtitle='Wavelength', ytitle="Intensity", /nodata,xrange=xrange,yrange=yrange,title=title,xstyle=xstyle,ystyle=ystyle
		for i=0, n_elements(output.wv)-1 do plots, [output.wv[i], output.wv[i]],[!y.crange[0],output.intensity[i]]
	endif else begin
                plot, output.wv, output.intensity, xtitle='Wavelength', ytitle="Intensity",/nodata,xrange=xrange,yrange=yrange,title=title,xstyle=xstyle,ystyle=ystyle
                oplot,  output.wv, output.intensity
        endelse

end
