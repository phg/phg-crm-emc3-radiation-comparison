;+
; PROJECT:
;       ADAS 
;
; NAME:
;       ADAS605_UPDATE
;
; PURPOSE: 
;       Helper routine to allow an updating of something when
;       a widget changes.
;
; EXPLANATION:
;       Receives a structure and simply calls rhe specified routine
;       on a specified widget.
;     
; INPUTS:
;        update_event - Structure:
;			  RTN : Routine to call
;			  WID : Widget ID to pass to routine
;
; OPTIONAL INPUTS:
;       None
;
; OUTPUTS:
;       None
;
; OPTIONAL OUTPUTS:
;       None
;
; KEYWORD PARAMETERS:
;       ALTERS - Specifies that this particular change should alter limits,
;                passed through to called subroutine.
;
;
; CALLS:
;       Arbitrary.
;      
; SIDE EFFECTS:
;       None
;
; CATEGORY:
;       Widgets
;
; WRITTEN:
;       Allan Whiteford
;
; MODIFIED:
;       1.1     Allan Whiteford
;               First release 
;
; VERSION:
;       1.1     20-02-2009
;
;-
;-----------------------------------------------------------------------------

pro adas605_update,update_event,alters=alters

	if size(update_event,/tname) eq 'STRUCT' then begin
        	tags=tag_names(update_event)
                if (where(tags eq 'RTN'))[0] ne -1 then begin
                	if (where(tags eq 'WID'))[0] ne -1 then begin
                        	call_procedure,update_event.rtn,update_event.wid,alters=alters
                        endif
                endif
        endif
end
