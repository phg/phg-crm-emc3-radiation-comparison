;+
; PROJECT:
;       ADAS 
;
; NAME:
;       ADAS605_IN
;
; PURPOSE: 
;       This function puts up a widget asking the user to select
;       an ADAS feature from the AFG system. A short description
;       of each feature is also displayed.
;
; EXPLANATION:
;       Self contained GUI code, uses pointers to get data back
;       from the form bypassing the old ADAS standard way of
;       generating an unnecessary compound widget. It comes
;       with an associated event handler.
;     
; INPUTS:
;        inval       - A structure holding the 'remembered' input data.
;                        FEATURE:       Name of the feature
;
; OPTIONAL INPUTS:
;       None
;
; OUTPUTS:
;       The function returns a string rep
;           rep = 'CONTINUE' if everything is ok
;           rep = 'CANCEL'   if there is a problem or indeed if the user
;                            closes the widget with the mouse. 
;
; OPTIONAL OUTPUTS:
;       None
;
; KEYWORD PARAMETERS:
;       FONT_LARGE - The name of a larger font e.g 'courier_bold14'
;
;       FONT_SMALL - The name of a smaller font e.g 'courier_bold12'
;
;
; CALLS:
;                            
;       AFG                  : Gets list of ADAS features
;                              and short descriptions.
;       CW_ADAS_MULTILABEL   : A CW which displays
;                              a multiple line label
;      
; SIDE EFFECTS:
;       None
;
; CATEGORY:
;       Widgets
;
; WRITTEN:
;       Allan Whiteford
;
; MODIFIED:
;       1.1     Allan Whiteford
;               First release 
;
; VERSION:
;       1.1     22-10-2008
;
;-
;-----------------------------------------------------------------------------

pro adas605_in_event,event
        widget_control,event.top,get_uvalue=info
        
        if event.id eq info.cancelid then begin
                widget_control,event.top,/destroy
        end

        if event.id eq info.doneid then begin
                feature=(afg(/list))[widget_info(info.droplist,/droplist_select)]
                *info.ptrtoformdata={cancel:0,feature:feature}
                widget_control,event.top,/destroy
        end

        if event.id eq info.droplist then begin
                feature=(afg(/list))[widget_info(info.droplist,/droplist_select)]
                desc=afg(feature,/desc)
                widget_control,info.name,set_value=desc.name
                widget_control,info.desc,set_value=desc.text
        endif
end

function adas605_in,inval,font_large=font_large,font_small=font_small
        if n_elements(font_large) eq 0 then font_large = ''
        if n_elements(font_small) eq 0 then font_small = ''

        parent = Widget_Base(column=1, Title='ADAS605 INPUT', xoffset=100, yoffset=1)
        top=widget_base(parent,/column,/frame)
        main=widget_base(top,/row)
        label=widget_label(main,value='Select ADAS Feature',font=font_large)

        droplist=widget_droplist(main,value=afg(/list),font=font_large)
        idx=where(afg(/list) eq inval.feature)
        if idx[0] ne -1 then widget_control,droplist,set_droplist_select=idx

        name=widget_label(top,value=string(bytarr(40)+(byte(' '))[0]),font=font_large)  
        desc=cw_adas_multilabel(top,value='',font=font_small,lines=10,cols=40,/frame) 

        mrow     = widget_base(parent,/row)
        cancelID = widget_button(mrow,value='Cancel',font=font_large)
        doneID   = widget_button(mrow,value='Done',font=font_large)
        
        widget_Control, parent, /realize
        ptrtoformdata = ptr_new({cancel:1})

        info={  doneID          :  doneID,              $
                cancelID        :  cancelID,            $
                droplist        :  droplist,            $
                name            :  name,                $
                desc            :  desc,                $
                ptrToFormData   :  ptrToFormData        }  


        widget_control, parent, set_uvalue=info

        adas605_in_event,{id:droplist,top:parent}

        xmanager, 'adas605_in', parent, event_handler='adas605_in_event'

  
        formdata = *ptrToFormData
        rep='CONTINUE'
        if n_elements(formdata) eq 0 then begin
                ptr_free, ptrtoformdata
                rep ='CANCEL'
        endif
 
        if formdata.cancel eq 1 then begin
                ptr_free, ptrtoformdata
                rep ='CANCEL'
        endif

        if rep eq 'CONTINUE' then begin
                inval={feature:formdata.feature}
        endif       

        return,rep
end
