;+
; PROJECT:
;       ADAS 
;
; NAME:
;       CW_ADAS605_TEXT
;
; PURPOSE: 
;       Display a text widget which will interact with a
;       particular parameter in an AFG object.
;
; EXPLANATION:
;       Display a text widget and when the value is modified
;       immediately change the underlying AFG object. Also
;       optionally call a user-supplied routine to notify
;       that the value has changed.
;     
; INPUTS:
;       tlb         - Top level base to store the CW in 
;       obj         - An AFG object - see afg_api__define for more
;                      details.
;	idx         - Index of the parameter inside the AFG object
;
; OPTIONAL INPUTS:
;       UPDATE_EVENT- Structure:
;			  RTN : Routine to call when a value
;                               is changed
;			  WID : Widget ID to pass to routine
;       FONT        - The name of a font to use.
;
;
; OUTPUTS:
;       Returns WID of created CW.
;
; CALLS:
;       
;      
; SIDE EFFECTS:
;       Will modify underyling AFG object directly.
;
; CATEGORY:
;       Widgets
;
; WRITTEN:
;       Allan Whiteford
;
; MODIFIED:
;       1.1     Allan Whiteford
;               First release 
;
;       1.2     Allan Whiteford
;               Use standard ADAS num_chk routine to check
;               if a string is a valid representation
;               of a floating point number.
;
; VERSION:
;       1.1     23-02-2009
;       1.2     09-10-2009
;
;-
;-----------------------------------------------------------------------------

pro cw_adas605_text_set,id,val
	widget_control,id,get_uvalue=info
	widget_control,info.text,set_value=strtrim(((info.obj->getpars()).(info.idx)),2),set_uvalue=strtrim(((info.obj->getpars()).(info.idx)),2)
end

pro cw_adas605_text_event,event
	widget_control,event.handler,get_uvalue=info
	if event.id eq info.text then begin
 		widget_control,event.id,get_value=a
		if (num_chk(a[0])) and a[0] ne '' then begin
			widget_control,event.id,get_uvalue=a
			widget_control,event.id,set_value=a
			widget_control,event.id,set_text_select=[event.offset-1]
			return
		endif else begin
			widget_control,event.id,get_uvalue=b
			widget_control,event.id,set_uvalue=a
                        if b ne a then begin
                        	pars=info.obj->getpars()
                                pars.(info.idx)=float(a)
                                ok=info.obj->setpars(pars=pars)
                                adas605_update,info.update_event,alters=(info.obj->getdesc()).parameters.(info.idx).alterslimits
                        endif
                        return
		endelse			
        endif
end

function cw_adas605_text,tlb,obj,idx,update_event=update_event,font=font
	pardesc=obj->getdesc()
	if n_elements(update_event) eq 0 then update_event=-1

	main=widget_base(tlb, /column)  
	text=widget_text(main,/editable,font=font,/all_events,/align_center)

	info={text:text, $
              update_event:update_event, $
              obj:obj, idx:idx }

	widget_control,main,set_uvalue=info
        widget_control,main,event_pro='cw_adas605_text_event',pro_set_value='cw_adas605_text_set'
	widget_control,main,/set_value
        return,main
end
