;+
; PROJECT:
;       ADAS 
;
; NAME:
;       CW_ADAS605_DROPLIST
;
; PURPOSE: 
;       Display a droplist widget which will interact with a
;       particular parameter in an AFG object.
;
; EXPLANATION:
;       Display a droplist widget and when the value is modified
;       immediately change the underlying AFG object. The list of
;       options for the droplist is also taken from AFG. Also
;       optionally call a user-supplied routine to notify
;       that the value has changed.
;     
; INPUTS:
;       tlb         - Top level base to store the CW in 
;       obj         - An AFG object - see afg_api__define for more
;                      details.
;	idx         - Index of the parameter inside the AFG object
;
; OPTIONAL INPUTS:
;       UPDATE_EVENT- Structure:
;			  RTN : Routine to call when a value
;                               is changed
;			  WID : Widget ID to pass to routine
;       FONT        - The name of a font to use.
;
; OUTPUTS:
;       Returns WID of created CW.
;
; CALLS:
;       
;      
; SIDE EFFECTS:
;       Will modify underyling AFG object directly.
;
; CATEGORY:
;       Widgets
;
; WRITTEN:
;       Allan Whiteford
;
; MODIFIED:
;       1.1     Allan Whiteford
;               First release 
;
; VERSION:
;       1.1     23-02-2009
;
;-
;-----------------------------------------------------------------------------

pro cw_adas605_droplist_set,id,val
	widget_control,id,get_uvalue=info
	pardesc=info.obj->getdesc()
        widget_control,info.combo,set_value=(pardesc.parameters.(info.idx).values)
	widget_control,info.combo,set_combobox_select=(info.obj->getpars()).(info.idx)
end

pro cw_adas605_droplist_event,event
	widget_control,event.handler,get_uvalue=info
	if event.id eq info.combo then begin
		pars=info.obj->getpars()
		pars.(info.idx)=event.index
		ok=info.obj->setpars(pars=pars)
		adas605_update,info.update_event,alters=(info.obj->getdesc()).parameters.(info.idx).alterslimits
	endif
end

function cw_adas605_droplist,tlb,obj,idx,update_event=update_event,font=font
	pardesc=obj->getdesc()
	if n_elements(update_event) eq 0 then update_event=-1

	main=widget_base(tlb,/column)  

	combo=widget_combobox(main,value=(pardesc.parameters.(idx).values),font=font)
	widget_control,combo,set_combobox_select=(obj->getpars()).(idx)

	info={combo:combo, $
              update_event:update_event, $
              obj:obj, idx:idx }

	widget_control,main,set_uvalue=info
        widget_control,main,event_pro='cw_adas605_droplist_event',pro_set_value='cw_adas605_droplist_set'
        widget_control,main,/set_value
        return,main
end
