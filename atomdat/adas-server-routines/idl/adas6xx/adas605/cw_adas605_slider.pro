;+
; PROJECT:
;       ADAS 
;
; NAME:
;       CW_ADAS605_SLIDER
;
; PURPOSE: 
;       Display a slider widget which will interact with a
;       particular parameter in an AFG object.
;
; EXPLANATION:
;       Display a droplist widget and when the value is modified
;       immediately change the underlying AFG object. Limits and
;       whether the value should ne manipulated in log-space are
;       also taken from AFG. Also optionally call a
;       user-supplied routine to notify that the value has
;       changed.
;     
; INPUTS:
;       tlb         - Top level base to store the CW in 
;       obj         - An AFG object - see afg_api__define for more
;                      details.
;	idx         - Index of the parameter inside the AFG object
;
; OPTIONAL INPUTS:
;       UPDATE_EVENT- Structure:
;			  RTN : Routine to call when a value
;                               is changed
;			  WID : Widget ID to pass to routine
;       FONT        - The name of a font to use.
;
; OUTPUTS:
;       Returns WID of created CW.
;
; CALLS:
;       
;      
; SIDE EFFECTS:
;       Will modify underyling AFG object directly.
;
; CATEGORY:
;       Widgets
;
; WRITTEN:
;       Allan Whiteford
;
; MODIFIED:
;       1.1     Allan Whiteford
;               First release 
;
;       1.2     Allan Whiteford
;               Use standard ADAS num_chk routine to check
;               if a string is a valid representation
;               of a floating point number.
;
;       1.3     Martin O'Mullane
;               Make limit boxes on either side of slider wider (8->11).
;
; VERSION:
;       1.1     23-02-2009
;       1.2     09-10-2009
;       1.3     11-08-2015
;
;-
;-----------------------------------------------------------------------------

pro cw_adas605_slider_setslider,info
	widget_control,info.value,get_value=value
	widget_control,info.upper,get_value=upper
	widget_control,info.lower,get_value=lower

	value=float(value[0])
	upper=float(upper[0])
	lower=float(lower[0])

	if info.log eq 1 then begin
		if lower le 0 then begin
			widget_control,info.slider,set_value=alog10(value+1.)*1000./alog10(upper+1.)
                endif else begin
	  		widget_control,info.slider,set_value=(alog10(value)-alog10(lower))*1000./(alog10(upper)-alog10(lower))
                endelse
        endif else begin
        	widget_control,info.slider,set_value=(value-lower)*1000./(upper-lower)
        endelse
end

pro cw_adas605_slider_set,id,val
	widget_control,id,get_uvalue=info
	pardesc=info.obj->getdesc()
        widget_control,info.lower,set_value=strtrim(string(pardesc.parameters.(info.idx).min),2),set_uvalue=strtrim(string(pardesc.parameters.(info.idx).min),2)
        widget_control,info.upper,set_value=strtrim(string(pardesc.parameters.(info.idx).max),2),set_uvalue=strtrim(string(pardesc.parameters.(info.idx).max),2)
        widget_control,info.value,set_value=strtrim(((info.obj->getpars()).(info.idx)),2),set_uvalue=strtrim(((info.obj->getpars()).(info.idx)),2)
        cw_adas605_slider_setslider,info
end

pro cw_adas605_slider_event,event
	widget_control,event.handler,get_uvalue=info
	if event.id eq info.value or event.id eq info.upper or event.id eq info.lower then begin
 		widget_control,event.id,get_value=a
		if (num_chk(a[0])) and a[0] ne '' then begin
			widget_control,event.id,get_uvalue=a
			widget_control,event.id,set_value=a
			widget_control,event.id,set_text_select=[event.offset-1]
			return
		endif else begin
			widget_control,event.id,get_uvalue=b
			widget_control,event.id,set_uvalue=a
                        cw_adas605_slider_setslider,info
                        if event.id eq info.value and b ne a then begin
                        	pars=info.obj->getpars()
                                pars.(info.idx)=float(a)
                                ok=info.obj->setpars(pars=pars)
                                adas605_update,info.update_event,alters=(info.obj->getdesc()).parameters.(info.idx).alterslimits
                        endif
                        return
		endelse			
        endif

	if event.id eq info.slider then begin
		widget_control,info.upper,get_value=upper
		widget_control,info.lower,get_value=lower
		upper=float(upper[0])
		lower=float(lower[0])
		
                if info.log eq 1 then begin
			if lower le 0 then begin
                        	if event.value eq 0 then begin
                                	value=0
                                endif else begin
					value = -1.+10.^((event.value)/(1000.0d) * (ALOG10(upper+1.)))
                                endelse
                        endif else begin
				value = 10.^(alog10(lower) + (event.value)/(1000.0d) * ((alog10(upper))-alog10((lower))))	
                        endelse
                endif else begin
			value = lower + (event.value)/(1000.0d) * (upper-lower)
                endelse

		widget_control,info.value,set_value=strtrim(string(value),2),set_uvalue=strtrim(string(value),2)
		pars=info.obj->getpars()
		pars.(info.idx)=value
		ok=info.obj->setpars(pars=pars)
		adas605_update,info.update_event,alters=(info.obj->getdesc()).parameters.(info.idx).alterslimits
        endif
end

function cw_adas605_slider,tlb,obj,idx,update_event=update_event,font=font
	pardesc=obj->getdesc()

	if n_elements(update_event) eq 0 then update_event=-1
	log=pardesc.parameters.(idx).log

	main = widget_base(tlb, /column)  
  	top = widget_base(main, /row, /align_center)
	value = widget_text(top, xsize=13, /align_center, /editable, /all_events,font=font)
	if pardesc.parameters.(idx).units ne 'none' then $
  		label=widget_label(top,value=(obj->getdesc()).parameters.(idx).units,font=font)
	
	bottom=widget_base(main,/row)
	lower=widget_text(bottom,xsize=11, /editable, /all_events,font=font)
	slider=widget_slider(bottom, /drag, /suppress_value, minimum=0, maximum=1000,font=font)
	upper=widget_text(bottom,xsize=11, /editable, /all_events,font=font)
        
	info={value:value, $
              slider:slider, $
              upper:upper, $
              lower:lower, $
              log:log, $
              update_event:update_event, $
              obj:obj, idx:idx }

	widget_control,main,set_uvalue=info
        widget_control,main,event_pro='cw_adas605_slider_event',pro_set_value='cw_adas605_slider_set'
	widget_control,main,/set_value

        return,main
end
