;+
; PROJECT:
;       ADAS 
;
; NAME:
;       CW_ADAS605_POPUP
;
; PURPOSE: 
;       Produces a button which in turn will produce a popup
;       window to adjust AFG parameters.
;
; EXPLANATION:
;       This code simply displays a button on screen, when the
;       button is pressed a group of AFG parameters will appear
;       in a popup to be adjusted in real time. The groups
;       of parameters are defined by AFG itself, this code
;       simply works on the index of a group (note: not the
;       index of a specific parameter).
;     
; INPUTS:
;       base        - Top level base to store the CW in 
;       obj         - An AFG object - see afg_api__define for more
;                     details.
;       idx         - The index of the group of parmaters in
;                     question.
; OPTIONAL INPUTS:
;       UPDATE_EVENT- Structure:
;			  RTN : Routine to call when a value
;                               is changed
;			  WID : Widget ID to pass to routine
;       FONT_LARGE - The name of a larger font e.g 'courier_bold14'
;       FONT_SMALL - The name of a smaller font e.g 'courier_bold12'
;
; OUTPUTS:
;       Returns WID of created CW.
;
; OPTIONAL OUTPUTS:
;       None
;
; KEYWORD PARAMETERS:
;       None
;
; CALLS:
;       AFG                  : Indirectly via method calls to obj
;       cw_adas605_param     : A CW which displays a particular
;                              paramter.
;      
; SIDE EFFECTS:
;       Alters underlying AFG object in real time.
;
; CATEGORY:
;       Widgets
;
; WRITTEN:
;       Allan Whiteford
;
; MODIFIED:
;       1.1     Allan Whiteford
;               First release 
;
; VERSION:
;       1.1     22-02-2009
;
;-
;-----------------------------------------------------------------------------

pro cw_adas605_popup_press,event
	widget_control,event.handler,get_uvalue=info
	obj=info.obj
        if not obj_valid(obj) then begin
        	widget_control,event.handler,/destroy
                return
        endif
        pardesc=obj->getdesc()
	
        tlb=widget_base(/column,title=pardesc.subordinate.(info.idx).title)

	label=widget_label(tlb,value=pardesc.subordinate.(info.idx).title,font=info.font_large)

        
        members=pardesc.subordinate.(info.idx).members

	for i=0,n_tags(pardesc.parameters)-1 do begin
        	if (where(strupcase((tag_names(pardesc.parameters))[i]) eq strupcase(members)))[0] ne -1 then begin
                	mybase=widget_base(tlb,/row,/frame)
			parvalwid=cw_adas605_param(mybase,obj,i,update_event=info.update_event,font_small=font_small,font_large=font_large)
                endif
        end

	close=widget_button(tlb,value='Close')

	widget_control,tlb,/realize
        widget_control,tlb,set_uvalue={obj:0},event_pro='cw_adas605_popup_press'
end

function cw_adas605_popup,base,obj,idx,update_event=update_event,font_small=font_small,font_large=font_large
	
        tlb=widget_base(base,/row)
        
        if not obj_valid(obj) then return,tlb
	
        pardesc=obj->getdesc()

	button=widget_button(base,value='Set '+pardesc.subordinate.(idx).title+'...',font=font_small)
	widget_control,base,set_uvalue={button:button,obj:obj,idx:idx,update_event:update_event,font_small:font_small,font_large:font_large}
	widget_control,base,event_pro='cw_adas605_popup_press'
        return,tlb

end
