;+
; PROJECT:
;       ADAS 
;
; NAME:
;       CW_ADAS605_PARAM
;
; PURPOSE: 
;       For a given AFG parameter, decides which underlying
;       compound widget should be displayed and displays
;	this along with an appropriate label.
;
; EXPLANATION:
;       Interogate an AFG object to find out the 'disptype'
;       of a given parameter and makes a decision based on this
;       as to the type of ADAS605 widget which should be
;       displayed.
;     
; INPUTS:
;       tlb         - Top level base to store the CW in 
;       obj         - An AFG object - see afg_api__define for more
;                      details.
;	idx         - Index of the parameter inside the AFG object
;
; OPTIONAL INPUTS:
;       UPDATE_EVENT- Structure:
;			  RTN : Routine to call when a value
;                               is changed
;			  WID : Widget ID to pass to routine
;       FONT_LARGE - The name of a larger font e.g 'courier_bold14'
;       FONT_SMALL - The name of a smaller font e.g 'courier_bold12'
;
; OUTPUTS:
;       Returns WID of created CW.
;
; KEYWORD PARAMETERS:
;       NOSUB     - Don't abstrct subordinate parameters to
;                   pop-ups
;       NOGROUP   - Don't group related parameters inside
;                   frames.
;
; CALLS:
;       cw_adas605_slider	: Underlying slider CW	
;       cw_adas605_droplist	; Underlying droplist CW
;	cw_adas605_pickfile	; Underlying file selection CW
;	cw_adas605_text		; Underlying text-field CW
;      
; SIDE EFFECTS:
;       None
;
; CATEGORY:
;       Widgets
;
; WRITTEN:
;       Allan Whiteford
;
; MODIFIED:
;       1.1     Allan Whiteford
;               First release 
;       1.2     Martin O'Mullane
;               Use case statement for choice selection.
;
; VERSION:
;       1.1     22-02-2009
;       1.2     10-08-2013
;
;-
;-----------------------------------------------------------------------------

function cw_adas605_param,tlb,obj,idx,update_event=update_event,font_small=font_small,font_large=font_large,labelsize=labelsize

	pardesc=obj->getdesc()

	tbase=widget_base(tlb,scr_xsize=labelsize,/row,/base_align_center)
	label=cw_adas_multilabel(tbase,value=pardesc.parameters.(idx).desc,cols=10,font=font_small)
    
	ttbase=widget_base(tlb,/row,/base_align_center,xpad=0,ypad=1)
        
        case strlowcase(pardesc.parameters.(idx).disptype) of
        
           'continuous' : parvalwid=cw_adas605_slider(ttbase,obj,idx,update_event=update_event,font=font_small)   
           'selection'  : parvalwid=cw_adas605_droplist(ttbase,obj,idx,update_event=update_event,font=font_small)
           'file'       : parvalwid=cw_adas605_pickfile(ttbase,obj,idx,update_event=update_event,font_small=font_small,font_large=font_large)
           'filelist'   : parvalwid=cw_adas605_pickfile(ttbase,obj,idx,update_event=update_event,font_small=font_small,font_large=font_large)
	   'integers'   : parvalwid=cw_adas605_integers(ttbase,obj,idx,update_event=update_event,font=font_small)

            else : parvalwid=cw_adas605_text(ttbase,obj,idx,update_event=update_event,font=font_small)
            
        endcase
        
	widget_control,ttbase,scr_ysize=(widget_info(tbase,/geom)).scr_ysize
	widget_control,tbase,scr_ysize=(widget_info(ttbase,/geom)).scr_ysize

	return,parvalwid
end
