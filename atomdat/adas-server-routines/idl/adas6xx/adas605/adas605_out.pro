;+
; PROJECT:
;       ADAS 
;
; NAME:
;       ADAS605_OUT
;
; PURPOSE: 
;       This function puts up a widget asking the user to select
;       output from ADAS605. Available outputs are a hard copy
;	of the selected feature, X-Y data or code to reproduce
;	the feature.
;
; EXPLANATION:
;       Self contained GUI code, uses pointers to get data back
;       from the form bypassing the old ADAS standard way of
;       generating an unnecessary compound widget. It comes
;       with an associated event handler.
;     
; INPUTS:
;        outval      - A structure holding the 'remembered' output data.
;			CODE_OUT:       Give code output
;                       CODE_FILE:      Filename for code output
;                       CODE_REPLACE:   Replace code output
;                       XY_OUT:         Give X-Y output
;                       XY_FILE:        Filename for X-Y output
;                       XY_REPLACE:     Replace X-Y output
;                       GRPOUT:         Give graphical output
;                       GTIT1:          Title for graph
;                       GRPSCAL:        Manual graph scaling
;                       XMIN:           Minimum value for X-range
;                       XMAX:           Maximum value for X-range
;                       YMIN:           Minimum value for Y-range
;                       YMAX:           Maximum value for Y-range
;                       HARDNAME:       Filename for hard copy
;                       DEVSEL:         Type of hard copy
;
;        obj         - An AFG object - see afg_api__define for more
;                      details.
; OPTIONAL INPUTS:
;       None
;
; OUTPUTS:
;       The function returns a string rep
;           rep = 'MENU'     To go back to the menu system
;           rep = 'CANCEL'   To go back to the processing panel
;
; OPTIONAL OUTPUTS:
;       None
;
; KEYWORD PARAMETERS:
;       FONT		- The name of the font to use e.g 'courier_bold14'
;	DEVLIST		- Device list for hard copy
;	BITFILE		- Full path to ADAS menu bitmap
;       MESSAGE_CODE	- Message to display in code output CW
;       MESSAGE_XY	- Message to display in X-Y output CW
;       MESSAGE_GRAPH	- Message to display in graphical output CW
;
; CALLS:
;       AFG		: Indirectly via method calls to obj
;       cw_adas_gr_sel	: Asks for graphical filename etc.
;	cw_adas_outfile	: Asks for output filename
;
; SIDE EFFECTS:
;       None
;
; CATEGORY:
;       Widgets
;
; WRITTEN:
;       Allan Whiteford
;
; MODIFIED:
;       1.1     Allan Whiteford
;               First release 
;
; VERSION:
;       1.1     22-10-2008
;
;-
;-----------------------------------------------------------------------------

pro adas605_out_event,event
	widget_control,event.top,get_uvalue=info

	if event.id eq info.menuid then begin
        	(*info.ptrtoformdata).action='MENU'
        	widget_control,event.top,/destroy
        endif

	if event.id eq info.cancelid then begin
        	(*info.ptrtoformdata).action='CANCEL'
        	widget_control,event.top,/destroy
        endif

	if event.id eq info.doneid then begin
        	
                widget_control,info.xyid,get_value=val
                (*info.ptrtoformdata).outval.xy_out=val.outbut
                (*info.ptrtoformdata).outval.xy_replace=val.repbut
                (*info.ptrtoformdata).outval.xy_file=val.filename
                widget_control,info.codeid,get_value=val
                (*info.ptrtoformdata).outval.code_out=val.outbut
                (*info.ptrtoformdata).outval.code_replace=val.repbut
                (*info.ptrtoformdata).outval.code_file=val.filename
                (*info.ptrtoformdata).action='DONE'
                widget_control,info.grpid,get_value=val

		(*info.ptrtoformdata).outval.grpout=val.OUTBUT
		(*info.ptrtoformdata).outval.gtit1=val.GTIT1
		(*info.ptrtoformdata).outval.grpscal=val.SCALBUT
		(*info.ptrtoformdata).outval.xmin=val.XMIN
		(*info.ptrtoformdata).outval.xmax=val.XMAX
		(*info.ptrtoformdata).outval.ymin=val.YMIN
		(*info.ptrtoformdata).outval.ymax=val.YMAX
		(*info.ptrtoformdata).outval.hardname=val.HARDNAME
		(*info.ptrtoformdata).outval.devsel=val.DEVSEL

        	widget_control,event.top,/destroy
        endif

end


function adas605_out,outval,obj,font=font,devlist=devlist,bitfile=bitfile,message_code=message_code,message_xy=message_xy,message_graph=message_graph


	IF n_elements(font) eq 0 THEN font = ''

	type=strlowcase(obj_class(obj))
        if strpos(type,'afg_') eq 0 then type=strmid(type,4)

	parent = Widget_Base(Column=1, Title='ADAS605 OUTPUT', XOFFSET=100, YOFFSET=1)

	main=widget_base(parent,/column)


	grselval={	OUTBUT:outval.grpout, GTIT1:outval.gtit1, SCALBUT:outval.grpscal, $
			XMIN:outval.xmin, XMAX:outval.xmax, $
			YMIN:outval.ymin, YMAX:outval.ymax, $
			HRDOUT:1, HARDNAME:outval.hardname, $
			GRPDEF:'adas605_'+type+'.ps', GRPFMESS:message_graph, $
			GRPSEL:-1, GRPRMESS:'', $
			DEVSEL:outval.devsel, GRSELMESS:'Note: Graphical output for hard copy only' }

	base = widget_base(parent,/row,/frame)
	grpid = cw_adas_gr_sel(base, OUTPUT='Graphical Output', GRPLIST=grplist, $
			DEVLIST=devlist, $
                        VALUE=grselval, FONT=font)


	xyfval = { OUTBUT:outval.xy_out, APPBUT:-1, REPBUT:outval.xy_replace, $
                    FILENAME:outval.xy_file, DEFNAME:'adas605_xy_'+type+'.dat', MESSAGE:message_xy }



	base = widget_base(parent,/row,/frame)
	xyid = cw_adas_outfile(base, OUTPUT='X-Y Output', $
                        VALUE=xyfval, FONT=font)


	codefval = { OUTBUT:outval.code_out, APPBUT:-1, REPBUT:outval.code_replace, $
                    FILENAME:outval.code_file, DEFNAME:'adas605_example_'+type+'.pro', MESSAGE:message_code }



	base = widget_base(parent,/row,/frame)
	codeid = cw_adas_outfile(base, OUTPUT='Code listing output', $
                        VALUE=codefval, FONT=font)

	

        
	base = widget_base(parent,/row)
	menufile = bitfile + '/menu.bmp'
	read_X11_bitmap, menufile, bitmap1
	menuid = widget_button(base, value=bitmap1)
	cancelid = widget_button(base,value='Cancel',font=font)
	doneid = widget_button(base,value='Done',font=font)

        widget_Control, parent, /realize
	ptrToFormData = Ptr_New({action:'CANCEL',outval:outval})

	info = { doneID          :  doneID,              $
		 cancelID        :  cancelID,            $
		 menuid          :  menuid,              $
                 codeid		 :  codeid,		 $
                 xyid            :  xyid,		 $
                 grpid           :  grpid,		 $
                 ptrToFormData   :  ptrToFormData        }  

	widget_control,parent,set_uvalue=info	

	xmanager,'adas605_out',parent,event_handler='adas605_out_event'
	
        outval = (*ptrToFormData).outval
        action = (*ptrToFormData).action
        
	return,action
end
