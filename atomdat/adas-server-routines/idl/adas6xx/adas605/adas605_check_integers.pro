;-----------------------------------------------------------------------------
;+
; PROJECT:
;       ADAS 
;
; NAME: 
;       adas605_check_integers
;
; PURPOSE: 
;       Check if a string is an integer, an array of integers or a list 
;       of integers and optionally return it as a string.
;
; EXPLANATION:
;     
; INPUTS:
;       inputstr    - String
;
; OUTPUTS:
;       0 - string is not a valid integer
;       1 = string is valid
;
; OPTIONAL OUTPUTS:
;       display_str - input string returned as a vector array,
;                     eg '6' -> '[6]'
;                        '[2,3,5]' -> '[2,3,5]'
;                        '1,2,4' -> '[1,2,4]'
;
; WRITTEN:
;       Martin O'Mullane
;
; MODIFIED:
;       1.1     Martin O'Mullane
;                - First release 
;
; VERSION:
;       1.1     10-08-2015
;
;-
;-----------------------------------------------------------------------------

FUNCTION adas605_check_integers, inputstr, display_str=display_str

str = strtrim(inputstr, 2)

; Simple integer

if num_chk(str) EQ 0 then begin

   display_str = '[' + str + ']'
   return, 1
   
endif

; Proper array eg, '[1,4,5]'

if strmid(str,0,1) EQ '[' AND strmid(str,0,1,/reverse_offset) EQ ']' then begin
    
    cmd = 'tmp_array = ' + str
    res = execute(cmd,1,1)
    if res EQ 1 then begin
    
       if size(tmp_array, /type) EQ 2 then begin
          display_str = str
          return, 1
       endif
       
    endif
    
endif

; A list eg, '3, 5, 7'

cmd = 'tmp_array = [' + str + ']'
res = execute(cmd,1,1)

if res EQ 1 then begin

   if size(tmp_array, /type) EQ 2 then begin
      display_str = '[' + str + ']'
      return, 1
   endif

endif


; End up here if inputstr is not valid

display_str = str + ' is an invalid integer, list or array'
return, 0

END
