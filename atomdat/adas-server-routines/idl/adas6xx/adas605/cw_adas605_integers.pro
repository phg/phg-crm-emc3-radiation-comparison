;+
; PROJECT:
;       ADAS
;
; NAME:
;       CW_ADAS605_INTEGERS
;
; PURPOSE:
;       Display a text widget which will interact with a
;       particular parameter in an AFG object.
;
; EXPLANATION:
;       Display a text widget and when the value is modified
;       immediately change the underlying AFG object. Also
;       optionally call a user-supplied routine to notify
;       that the value has changed.
;
; INPUTS:
;       tlb          - Top level base to store the CW in
;       obj          - An AFG object - see afg_api__define for more details.
;	idx          - Index of the parameter inside the AFG object
;
; OPTIONAL INPUTS:
;       UPDATE_EVENT - Structure:
;		           RTN : Routine to call when a value is changed
;		           WID : Widget ID to pass to routine
;       FONT         - The name of a font to use.
;
;
; OUTPUTS:
;       Returns WID of created CW.
;
; CALLS:
;
;
; SIDE EFFECTS:
;       Will modify underyling AFG object directly.
;
; CATEGORY:
;       Widgets
;
; WRITTEN:
;       Martin O'Mullane
;
; MODIFIED:
;       1.1     Martin O'Mullane
;                - First release
;
; VERSION:
;       1.1     10-08-2015
;
;-
;-----------------------------------------------------------------------------

pro cw_adas605_integers_set,id,val
	widget_control,id,get_uvalue=info
        res = (info.obj->getpars()).(info.idx)
        if size(res, /type) EQ 10 then str = strtrim(*res, 2) else str = strtrim(res ,2)
	widget_control,info.text,set_value=str,set_uvalue=str
end

pro cw_adas605_integers_event,event
	widget_control,event.handler,get_uvalue=info
	if event.id eq info.text then begin
 		widget_control,event.id,get_value=a

                ; check that it is an integer or an array of integers

                res = adas605_check_integers(a[0], display_str=display_str)

		if (res) then begin
			widget_control,event.id,get_uvalue=b
			widget_control,event.id,set_uvalue=a
                        if b ne a then begin
                        	pars=info.obj->getpars()
                                res = pars.(info.idx)
                                if size(res, /type) EQ 10 then *res = display_str else res=display_str

                                ok=info.obj->setpars(pars=pars)
                                adas605_update,info.update_event,alters=(info.obj->getdesc()).parameters.(info.idx).alterslimits
                        endif
                        return
		endif else begin
			widget_control,event.id,get_uvalue=a
			widget_control,event.id,set_value=a
			widget_control,event.id,set_text_select=[event.offset-1]
			return
		endelse
        endif
end

function cw_adas605_integers,tlb,obj,idx,update_event=update_event,font=font
	pardesc=obj->getdesc()
	if n_elements(update_event) eq 0 then update_event=-1

	main=widget_base(tlb, /column)
	text=widget_text(main,/editable,font=font,/align_center)

	info={text:text, $
              update_event:update_event, $
              obj:obj, idx:idx }

	widget_control,main,set_uvalue=info
        widget_control,main,event_pro='cw_adas605_integers_event',pro_set_value='cw_adas605_integers_set'
	widget_control,main,/set_value

        return,main
end
