;+
; PROJECT:
;       ADAS
;
; NAME:
;	ADAS605
;
; PURPOSE:
;	The highest level routine for the ADAS 605 program.
;
; EXPLANATION:
;	This routine is called from the main adas system routine, adas.pro,
;	to start the ADAS603 application.
;
; USE:
;	First the system settings must be established by calling
;	adas_sys_set.pro then adas605.pro is called to start the
;	ADAS 605 application;
;
;	adas_sys_set, adasrel, fortdir, userroot, centroot, $
;		      devlist, devcode, font_large, font_small, edit_fonts
;	adas605,   adasrel, fortdir, userroot, centroot, devlist, $
;		   devcode, font_large, font_small, edit_fonts
;
; INPUTS:
;	ADASREL - A string indicating the ADAS system version, 
;		  e.g ' ADAS RELEASE: ADAS93 V1.1.0'.  The first
;		  character should be a space.
;
;	FORTDIR - A string holding the path to the directory where the
;		  FORTRAN executables are, e.g '/disk/adas/idl_adas/fortran/bin'
;
;	USERROOT - A string holding the path to the root directory of
;		   the user's adas data. e.g '/disk/bowen/adas'
;		   This root directory will be used by adas to construct
;		   other path names.  For example the users default data
;		   for adas605 should be in /disk/bowen/adas/arch603.  In
;		   particular the user's default interface settings will
;		   be stored in the directory USERROOT+'/defaults'.  An
;		   error will occur if the defaults directory does not
;		   exist.
;
;	CENTROOT - Like USERROOT, but this directory points to the
;		   central data area for the system.  User defaults are
;		   not stored on CENTROOT.
;
;	DEVLIST - A string array of hardcopy device names, used for
;		  graphical output. e.g ['Post-Script','HP-PCL','HP-GL']
;		  This array must mirror DEVCODE.  DEVCODE holds the
;		  actual device names used in a SET_PLOT statement.
;
;	DEVCODE - A string array of hardcopy device code names used in
;		  the SET_PLOT statement, e.g ['PS', 'PCL', 'HP']
;		  This array must mirror DEVLIST.
;
;	FONT_LARGE - The name of a larger font e.g 'courier_bold14'
;
;	FONT_SMALL - The name of a smaller font e.g 'courier_bold12'
;
;	EDIT_FONTS - A structure of two fonts used in the table editor
;		     adas_edtab.pro, {font_norm:'',font_input:''}.
;		     The two fonts are used to differentiate between
;		     editable and non-editable parts of the table. You
;		     may have to experiment to get two fonts which give
;		     the same default spacing in the edit widget.
;		     e.g {font_norm:'helvetica_bold14', $
;			  font_input:'helvetica_oblique14'}
;
; OPTIONAL INPUTS:
;	None
;
; OUTPUTS:
;	None - Note this routine should leave all inputs un-modified.
;
; OPTIONAL OUTPUTS:
;	None
;
; KEYWORD PARAMETERS:
;	None
;
; CALLS:
;	
;
; SIDE EFFECTS:
;	
;
; CATEGORY:
;	Adas system.
;	
; WRITTEN:
;       Allan Whiteford
;       31/07/08
;
; MODIFIED:
;	1.1   Allan Whiteford
;	      First release		
;
; VERSION:
;	1.1	31-07-08
;-
;-----------------------------------------------------------------------------


PRO ADAS605,	adasrel, fortdir, userroot, centroot, 			$
		devlist, devcode, font_large, font_small, edit_fonts

		;************************
		;**** Initialisation ****
		;************************

  adasprog = ' PROGRAM: ADAS605 V1.0'
  lpend = 0
  gomenu = 0
  deffile = userroot+'/defaults/adas605_defaults.dat'
  bitfile = centroot+'/bitmaps'
  device = ''

		;******************************************
		;**** Search for user default settings ****
		;**** If not found create defaults     ****
		;******************************************

  files = findfile(deffile)
  if files[0] eq deffile then begin

    restore,deffile

  end else begin

    inval = {feature:''}
    procval = {NEW:-1}
    outval = {code_out:0,code_file:'',code_replace:0,xy_out:0,xy_file:'',xy_replace:0, $
              grpout:0,gtit1:'',grpscal:0,xmin:'',xmax:'',ymin:'', $
	      ymax:'',hardname:'',devsel:0 }

  end

		;******************
		;**** Get date ****
		;******************
  userid = '          '
  uid = getenv('USER')
  strput,userid,strmid(uid,0,10)

LABELSTART:

  rep=adas605_in(inval,font_large=font_large,font_small=font_small)

  if rep eq 'CANCEL' then goto,LABELEND

  obj=obj_new('afg_'+inval.feature)
  
  if (where(tag_names(procval) eq 'TYPE'))[0] ne -1 then begin
  	tpars=obj->getpars()
        if procval.type eq obj_class(obj) and n_tags(procval.pars) eq n_tags(tpars) then begin
        	junk=obj->setpars(pars=procval.pars)
        endif
  endif
  
  
LABELPROC:

  rep=adas605_proc(obj,bitfile=bitfile,font_large=font_large,font_small = font_small)

  procval={pars:obj->getpars(),type:obj_class(obj)}
  
  if rep eq 'MENU' then goto, LABELEND
  if rep eq 'CANCEL' then goto,LABELSTART

  message_graph=''
  message_code=''
  message_xy=''
  
LABELOUT:  
  
  rep=adas605_out(outval,obj,font=font_large,devlist=devlist,bitfile=bitfile,message_code=message_code,message_xy=message_xy,message_graph=message_graph)
  
  if rep eq 'CANCEL' then goto,LABELPROC
  if rep eq 'DONE' then begin
  	if outval.xy_out eq 1 and outval.xy_file ne '' then begin
        	
                file_acc,outval.xy_file,exist,read,write,execute,filetype
                if exist eq 1 and outval.xy_replace eq 0 then begin
                	message_xy='File already exists'
                endif else begin
                	data=obj->getcalc()
                	openw,unit,outval.xy_file,/get_lun
                	for i=0,n_elements(data.wv)-1 do begin
                		printf,unit,data.wv[i],data.intensity[i],format='(2E12.5)'
                	end
        		free_lun,unit
                	message_xy='Written to file'
                endelse
                
        endif
        
  	if outval.code_out eq 1 and outval.code_file ne '' then begin
                file_acc,outval.code_file,exist,read,write,execute,filetype
                if exist eq 1 and outval.code_replace eq 0 then begin
                	message_xy='File already exists'
                endif else begin
	                ok=obj->writecode(outval.code_file)
        	        if ok eq 1 then message_code='Written to file' else message_code='Error generating code'
        	endelse
        endif
        
   	if outval.grpout eq 1 and outval.hardname ne '' then begin
                file_acc,outval.hardname,exist,read,write,execute,filetype
		data=obj->getcalc()
		set_plot,'PS'
                device,file=outval.hardname
                if outval.grpscal eq 1 then begin
                	xrange=[float(outval.xmin),float(outval.xmax)]
                        yrange=[float(outval.ymin),float(outval.ymax)]
                endif else begin
                	if n_elements(xrange) ne 0 then junk=temporary(xrange)
                	if n_elements(yrange) ne 0 then junk=temporary(yrange)
                endelse
                adas605_plot,obj,title=outval.gtit1,xrange=xrange,yrange=yrange
                device,/close
                set_plot,'X'
                message_graph='Written to file' 
        endif
        
        goto,LABELOUT
  endif
LABELEND:

		;**** Save user defaults ****
save,inval,procval,outval,filename=deffile

END
