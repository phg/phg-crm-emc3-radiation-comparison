;+
; PROJECT:
;       ADAS 
;
; NAME:
;       CW_ADAS605_PARAMS
;
; PURPOSE: 
;       Produces a compound widget to interact with the parameters
;       of an AFG object.
;
; EXPLANATION:
;       This code takes an AFG object and self-generates a set of
;       widgets to interact with the code, changes to the widget
;       are passed immediately to the underlying object and
;       optionally a user-specified routine is called (e.g.
;       to re-plot the AFG object).
;     
; USE:
;       obj=obj_new('afg_zeeman')
;	tlb=widget_base(title='Parameter Adjustment',/col)
;	params=cw_adas605_params(tlb,obj)
;       widget_control,tlb,/realize
;       xmanager,'Parameter Adjustment',tlb,/no_block
;
; INPUTS:
;       tlb         - Top level base to store the CW in 
;       obj         - An AFG object - see afg_api__define for more
;                      details.
;
; OPTIONAL INPUTS:
;       UPDATE_EVENT- Structure:
;			  RTN : Routine to call when a value
;                               is changed
;			  WID : Widget ID to pass to routine
;       FONT_LARGE - The name of a larger font e.g 'courier_bold14'
;       FONT_SMALL - The name of a smaller font e.g 'courier_bold12'
;
; OUTPUTS:
;       Returns WID of created CW.
;
; OPTIONAL OUTPUTS:
;       None
;
; KEYWORD PARAMETERS:
;       NOSUB     - Don't abstrct subordinate parameters to
;                   pop-ups
;       NOGROUP   - Don't group related parameters inside
;                   frames.
;
; CALLS:
;       AFG                  : Indirectly via method calls to obj
;       cw_adas605_param     : A CW which displays a particular
;                              paramter.
;       cw_adas605_popup     : A CW which displays a button
;                              which generates a popup.
;      
; SIDE EFFECTS:
;       Alters underlying AFG object in real time.
;
; CATEGORY:
;       Widgets
;
; WRITTEN:
;       Allan Whiteford
;
; MODIFIED:
;       1.1     Allan Whiteford
;               First release 
;
; VERSION:
;       1.1     22-02-2009
;
;-
;-----------------------------------------------------------------------------

pro cw_adas605_params_sv,id,val
	widget_control,id,get_uvalue=info
	for i=0,n_elements(info.parValWids)-1 do widget_control,info.parvalwids[i],/set_value
end


function cw_adas605_params,tlb,obj,update_event=update_event,nosub=nosub,nogroup=nogroup,font_small=font_small,font_large=font_large

	if n_elements(update_event) eq 0 then update_event=-1

	topbase=widget_base(tlb,/column)
	base=topbase

	parDesc = obj->getDesc()
	npar=n_tags(pardesc.parameters)
	nodisp=intarr(npar)
        pardone=intarr(npar)
        
        if not keyword_set(nosub) and (where(tag_names(pardesc) eq 'SUBORDINATE'))[0] ne -1 then begin
		for j=0,n_tags(pardesc.subordinate)-1 do begin
			for i=0,n_elements(pardesc.subordinate.(j).members)-1 do begin
                        	idx=where(strupcase(tag_names(pardesc.parameters)) eq strupcase(pardesc.subordinate.(j).members[i]))
                                if idx[0] ne -1 then nodisp[idx]=1
                        end
		end
	end

	nogroup=0

	if not keyword_set(nogroup) and (where(tag_names(pardesc) eq 'GROUPS'))[0] ne -1 then begin
		groups=n_tags(pardesc.groups)
                doframe=0
        endif else begin
        	groups=0
        	doframe=1
        endelse
        
	miniBase = intarr(1)
	parValWids = intarr(1)

        dummy=widget_base(/col)
        for i=0, npar-1 do label=cw_adas_multilabel(dummy,value=pardesc.parameters.(i).desc,cols=15)
	xsize=(widget_info(dummy,/geom)).scr_xsize
        widget_control,dummy,/destroy

	for k=0,groups do begin

		if doframe eq 0 then begin
        		base=widget_base(topbase,/frame,/column)
                	if groups gt 0 and k ne groups then label=widget_label(base,value=pardesc.groups.(k).title,font=font_large)
                	if groups gt 0 and k eq groups then label=widget_label(base,value='Other',font=font_large)
        	endif

		for i=0, npar-1 do begin

			if nodisp[i] eq 1 then goto,nextnpar
			if pardone[i] eq 1 then goto,nextnpar

			if k ne groups then begin
                        	if (where(strupcase(pardesc.groups.(k).members) eq strupcase((tag_names(pardesc.parameters))[i])))[0] eq -1 then goto,nextnpar
			endif
                
			pardone[i]=1

	                minibase = [minibase,widget_base(base, /row,frame=doframe)]
			mybase=minibase[n_elements(minibase)-1]
                
	                parvalwid=cw_adas605_param(mybase,obj,i,update_event=update_event,font_small=font_small,font_large=font_large,labelsize=xsize)
      
      			parvalwids=[parvalwids,parvalwid]

			if not keyword_set(nosub) and (where(tag_names(pardesc) eq 'SUBORDINATE'))[0] ne -1 then begin
				for j=0,n_tags(pardesc.subordinate)-1 do begin
                        		if strupcase(pardesc.subordinate.(j).after) eq strupcase((tag_names(pardesc.parameters))[i]) then begin
                                		print,'do group',pardesc.subordinate.(j).title
						miniBase = [minibase,WIDGET_BASE(base, /row,frame=doframe)]
						mybase=minibase[n_elements(minibase)-1]
	
						tbase=widget_base(mybase,scr_xsize=xsize,/row,/base_align_center)
						label=cw_adas_multilabel(tbase,value=pardesc.subordinate.(j).title,cols=10,font=font_small)
    
						ttbase=widget_base(mybase,/row,/base_align_center,xpad=0,ypad=1)

    						button=cw_adas605_popup(ttbase,obj,j,update_event=update_event,font_small=font_small,font_large=font_large)

						widget_control,ttbase,scr_ysize=(widget_info(tbase,/geom)).scr_ysize
						widget_control,tbase,scr_ysize=(widget_info(ttbase,/geom)).scr_ysize
                	                endif
                        	end
			end
nextnpar:
		endfor
	endfor

	minibase=minibase[1:*]
	parValWids=parvalwids[1:*]

	info={obj:obj,parValWids:parValWids,update_event:update_event}
        
        widget_control,base,set_uvalue=info
	widget_control,base,pro_set_value='cw_adas605_params_sv'

	return,base
end
