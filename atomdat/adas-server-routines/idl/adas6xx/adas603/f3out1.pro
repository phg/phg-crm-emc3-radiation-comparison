; Copyright (c) 1998 Strathclyde University .
; SCCS info: Module @(#)$Header: /home/adascvs/idl/adas6xx/adas603/f3out1.pro,v 1.4 2004/07/06 13:51:48 whitefor Exp $ Date $Date: 2004/07/06 13:51:48 $
;+
; PROJECT:
;       ADAS IBM MVS to DEC UNIX conversion
;
; NAME:
;	F3OUT1
;
; PURPOSE:
;	Text output for ADAS603.
;
; EXPLANATION:
;	This routine writes text output to a file.
;
; USE:
;	Use is specific to ADAS603. See adas603.pro for example.
;
; INPUTS:
;
;	INVAL	- Structure; the input screen settings. See cw_adas603_in.pro
;
;	PROCVAL	- Structure containg the processing options. 
;		  See cw_adas603_proc.pro
;
;	OLDSTR  - A structure derived from a processed template also
;		  containing previously used fitting parameters.
;
;	LSTR	- Structure containing processed data.
;
;	ADASHEADER- Full header for output (release:prog:date:time)
;
;	HEADER1 - Header of the input data.
;
;	HEADER2 - Header of the processed template.
;
;	DATE	- String containing the date.
;
;	DSNINC	- String; The full system file name of the input 
;		  dataset selected by the user for processing.
;	
;	DSNINX	- String; The full system file name of the input processed
;		  template.
;
;
; OPTIONAL INPUTS:
;	None.
;
; OUTPUTS:
;	None.
;
; OPTIONAL OUTPUTS:
;	None.
;
; KEYWORD PARAMETERS:
;	None
;
; CALLS:
;	None.
;
; SIDE EFFECTS:
;	Outputs text to a file.
;
; CATEGORY:
;	Adas system.	
;
; WRITTEN:
;       Richard Martin, University of Strathclyde, 4-Dec-1998
;	  Based on f2out.pro.
;
; MODIFIED:
;	1.1   Richard Martin
;		Put under SCCS control          
;	1.2	Richard Martin
;		Modified printing of polarisation option and for 
;		fit with background only case.
;	1.3	Richard Martin 
;		Added entry for sigma option (both +,- components)        
;	1.4	Richard Martin
;		Changed format of ese & ecl for temperature.
;
; VERSION:
;	1.1	04-12-98
;	1.2	19-03-99
;	1.3	22-04-99
;	1.4	15-11-00
;
;-
;----------------------------------------------------------------------------

PRO f3out1, inval, procval, outval, lstr, adasheader, header1, header2, $
           date, dsninc, dsninx

;PRO f3out1
;restore,'f3out.dat'

                ;**** Open file ****

  openw, unit, /get_lun, outval.texdsn

    str=' '
    printf, unit, adasheader, format='(1a80)'    
    printf, unit, '************* TABULAR OUTPUT FROM MAXIMUM LIKELIHOOD'+$
                  ' SPECTRAL LINE PROFILE FITTING PROGRAM: ADAS603 -'+$
                  ' DATE: '+strupcase(date(0))+' *************'
    printf, unit, str
    if inval.dflag eq 0 then begin
      printf, unit, ' DATA FILENAME : '+strtrim(dsninc,2)+$
                    ' DATA TYPE : STANDARD'
    end else if inval.dflag eq 1 then begin
      printf, unit, ' DATA FILENAME : '+strtrim(dsninc,2)+$
                    ' DATA TYPE : PROCESSED'
    end else if inval.dflag eq 2 then begin
      printf, unit, ' DATA FILENAME : '+strtrim(dsninc,2)+$
                    '               DATA TYPE : OTHER'
    end
    if strtrim(dsninx,2) ne '' then begin
      printf, unit, '      TEMPLATE : ',dsninx, format= '(1a17,1a50)'
    end else begin
      printf, unit, '      TEMPLATE : NONE '
    end
    hdchk = size(lstr(0).header)
    if hdchk(hdchk(0)+1) ne 8 then begin
      printf, unit, ' HEADER : ',strtrim(lstr(0).header,2)
    end
    printf, unit, str

                ;**** Now list fitting options ****
    printf, unit, '--------- LINE WIDTH OPTIONS ---------'
    printf, unit, '--------------------------------------'
    if lstr(0).getfit(0) eq 1 then begin
      printf, unit,   ' ALL LINE WIDTHS FIXED ON INPUT   : N '
      if lstr(0).getfit(1) eq 1 then begin
        printf, unit, ' VARIABLE LINE WIDTHS WERE FITTED : Y ' 
      end else begin
        printf, unit, ' VARIABLE LINE WIDTHS WERE FITTED : N ' 
      end
    end else begin
      printf, unit,   ' ALL LINE WIDTHS FIXED ON INPUT   : Y '+$
                      ' - CHOSEN WIDTH : '+strtrim(lstr(0).w(0),2)
    end
    printf, unit, str
    printf, unit, '--------- BACKGROUND OPTIONS ---------'
    printf, unit, '--------------------------------------'
    if lstr(0).getfit(5) ne 1 then begin
      printf, unit, ' NO BACKGROUND IN DATA - ALL OPTIONS DISABLED '
    end else begin
      printf, unit, ' CONSTANT COMPONENT  : Y '
      if lstr(0).getfit(2) eq 1 then begin
        printf, unit, ' LINEAR COMPONENT    : Y '
      end else printf, unit, ' LINEAR COMPONENT    : N '
      if lstr(0).getfit(3) eq 1 then begin
        printf, unit, ' QUADRATIC COMPONENT : Y '
      end else printf, unit, ' QUADRATIC COMPONENT : N '
    end
    printf, unit, str
    printf, unit, '------------- OTHER OPTIONS --------------'
    if lstr(0).getfit(4) eq 1 then begin
      printf, unit, ' WEIGHTED FIT       : Y '
    end else begin
      printf, unit, ' EQUAL WEIGHTED FIT : Y '
    end
    printf, unit, ' INFO ERROR FLAG FROM MINPACK ROUTINE : '+$
                  strtrim(lstr(0).info,2)
    printf, unit, ' EXPOSURE TIME : '+strtrim(lstr(0).exptime,2)
    printf, unit, ' NO. OF LINES  : '+strtrim(lstr(0).numline,2)+$
                  '             NO. OF LINES WITH FIXED POSITIONS : '+$
                    strtrim(lstr(0).nfxl,2)
    
               ;**** Now output table ****
    printf, unit, str
    printf, unit, str
    printf, unit, ' KEY '
    printf, unit, '-----'
    printf, unit, ' ESE - ESTIMATED STANDARD ERROR (%)'
    printf, unit, ' ECL - ESTIMATED 95% CONFIDENCE LIMIT (%)'
    printf, unit, '-----------------------------------------------------'+$
    ' OUTPUT TABLE -----------------------------------------------------'
    for i = 0, n_elements(lstr)-1 do begin
      wtst = 0
      printf, unit, str
      printf, unit, ' DATASET : '+strtrim(i+1,2)
      printf, unit, '--------------
      printf, unit, ' BACKGROUND '
      printf, unit, '------------'
      printf, unit, ' CONSTANT  : '+strtrim(lstr(i).b0,2)+'        ESE : '+$
              strtrim(lstr(i).eseb0,2)+'        ECL : '+$
              strtrim(lstr(i).eclb0,2)
      printf, unit, ' LINEAR    : '+strtrim(lstr(i).b1,2)+'        ESE : '+$
              strtrim(lstr(i).eseb1,2)+'        ECL : '+$
              strtrim(lstr(i).eclb1,2)
      printf, unit, ' QUADRATIC : '+strtrim(lstr(i).b2,2)+'        ESE : '+$
              strtrim(lstr(i).eseb2,2)+'        ECL : '+$
              strtrim(lstr(i).eclb2,2)
              
    if (lstr(i).getfit(1) NE 2) then begin          
      printf, unit, ' INDEX  POSITION                        HEIGHT '+$
       '                         FLUX                               '+$
       '  WIDTH                         IDENTIFICATION  '
      printf, unit, '          (XO)      (ESEXO)    (ECLXO)     (H) '+$
       '      (ESEH)     (ECLH)                (ESEF)     (ECLF)    '+$
       '            (ESEW)     (ECLW)                   '
      printf, unit, '-----------------------------------------------'+$
       '------------------------------------------------------------'+$
       '------------------------------------------------'
      if lstr(i).getfit(0) ne 1 then wtst=1
      if lstr(i).getfit(0) eq 1 and lstr(i).getfit(1) ne 1 then wtst=1
      for j = 0, lstr(i).numline-1 do begin
       if wtst eq 1 then begin
         printf, unit, j+1, lstr(i).xo(j), lstr(i).esexo(j), lstr(i).eclxo(j),$
         lstr(i).h(j), lstr(i).eseh(j), lstr(i).eclh(j), lstr(i).flux(j),  $
         lstr(i).esef(j), lstr(i).eclf(j), lstr(i).w(0), lstr(i).esewl,    $
         lstr(i).eclwl, lstr(i).lineid(j),                                 $
         format='(2x,i2,12(1x,d10.3),1a10)' 
       end else begin
         printf, unit, j+1, lstr(i).xo(j), lstr(i).esexo(j), lstr(i).eclxo(j),$
         lstr(i).h(j), lstr(i).eseh(j), lstr(i).eclh(j), lstr(i).flux(j),  $
         lstr(i).esef(j), lstr(i).eclf(j), lstr(i).w(j), lstr(i).esewv(j), $
         lstr(i).eclwv(j), lstr(i).lineid(j),                              $
         format='(2x,i2,12(1x,d10.3),1a10)' 
       end
      end
      printf, unit, '-----------------------------------------------'+$
      '------------------------------------------------------------'+$
      '------------------------------------------------'
    endif

      printf, unit,' '
      printf, unit, ' FEATURE '
      printf, unit, '---------'
      printf, unit,' '
      printf, unit, 'SELECTED FEATURE          : '+ lstr(i).featstr
	if (lstr(i).gamma eq -1.0) then lineprof='DOPPLER'
	if (lstr(i).gamma eq 1.0)  then lineprof='LORENTZIAN1 WIDTH=1.0'
	if (lstr(i).gamma eq 2.0)  then lineprof='LORENTZIAN2 WIDTH=2.0'		
      printf, unit, 'LINE PROFILE              : ', lineprof
      if (lstr(i).pol eq 0) then key='NONE'
      if (lstr(i).pol eq 1) then key='PI+SIGMA' 
      if (lstr(i).pol eq 2) then key='PI'
      if (lstr(i).pol eq 3) then key='SIGMA' 
      if (lstr(i).pol eq 4) then key='SIGMA+' 
      if (lstr(i).pol eq 5) then key='SIGMA-'        
      printf, unit, 'POLARISATION  COMPONENETS : ',key                 
      printf, unit, 'VIEWING ANGLE             : ',strtrim(string(lstr(i).angxp,format='(1x,f5.2)'),2)
      printf, unit,' '            
      if ( lstr(i).getxp(0) eq 1) then xp_opt='Y' ELSE xp_opt='N'        
      printf, unit, 'FIT B FIELD      : '+xp_opt+'   B VALUE        :  '+ 		$
      		  strtrim(string(lstr(i).bxp,format='(f5.3)'),2) + '    '+		$
      		  '   ESE : '+ strtrim(string(lstr(i).esebxp,format='(f5.3)'),2)+	$
      		  '   ECL : '+ strtrim(string(lstr(i).eclbxp,format='(f5.3)'),2)

      if ( lstr(i).getxp(1) eq 1) then xp_opt='Y' ELSE xp_opt='N'   
      printf, unit, 'FIT PEAK HEIGHT  : '+xp_opt+'   PEAK HEIGHT    :  '+ 		$
      		  strtrim(string(lstr(i).phxp,format='(f5.3)'),2) + '    '+		$
      		  '   ESE : '+ strtrim(string(lstr(i).esephxp,format='(f5.3)'),2)+$
      		  '   ECL : '+ strtrim(string(lstr(i).eclphxp,format='(f5.3)'),2)
      if ( lstr(i).getxp(2) eq 1) then xp_opt='Y' ELSE xp_opt='N'    
      if ( lstr(i).cxp lt 0.0) then begin
      	printf, unit, 'FIT CENTROID POS : '+xp_opt+'   DELTA CENTROID : '+ 	$
      	   	  strtrim(string(lstr(i).cxp,format='(f6.3)'),2) + '    '+		$
      		  '   ESE : '+ strtrim(string(lstr(i).esecxp,format='(f5.3)'),2)+ $
      		  '   ECL : '+ strtrim(string(lstr(i).eclcxp,format='(f5.3)'),2)
	endif else begin
      	printf, unit, 'FIT CENTROID POS : '+xp_opt+'   DELTA CENTROID :  '+ 	$
      		  strtrim(string(lstr(i).cxp,format='(f5.3)'),2) + '    '+		$
      		  '   ESE : '+ strtrim(string(lstr(i).esecxp,format='(f5.3)'),2)+ $
      		  '   ECL : '+ strtrim(string(lstr(i).eclcxp,format='(f5.3)'),2)	
	endelse
      if ( lstr(i).getxp(3) eq 1) then xp_opt='Y' ELSE xp_opt='N'   
      printf, unit, 'FIT TEMPERATURE  : '+xp_opt+'   TEMPERATURE    :  '+ 		$
      		  strtrim(string(lstr(i).txp,format='(e9.3)'),2) + 			$
      		  '   ESE : '+ strtrim(string(lstr(i).esetxp,format='(f9.3)'),2)+ $
      		  '   ECL : '+ strtrim(string(lstr(i).ecltxp,format='(f9.3)'),2)
    end
    printf, unit, '-----------------------------------------------'+$
    '------------------------------------------------------------'+$
    '------------------------------------------------'

  free_lun, unit

                ;**** If text output is requested enable append ****
                ;**** for next time and update the default to   ****
                ;**** the current text file.                    ****

  outval.texapp=0
  outval.texdef = outval.texdsn
  if outval.texrep ge 0 then outval.texrep = 0
  outval.texmes = 'Output written to file.'

END
