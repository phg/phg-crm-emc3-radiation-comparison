; Copyright (c) 1998 Strathclyde University .
; SCCS info: Module @(#)$Header: /home/adascvs/idl/adas6xx/adas603/cw_adas603_infile.pro,v 1.1 2004/07/06 12:54:43 whitefor Exp $ Date $Date: 2004/07/06 12:54:43 $
;+
; PROJECT:
;       ADAS IBM MVS to DEC UNIX conversion
;
; NAME: CW_ADAS603_INFILE()
;
; PURPOSE:
;	Data file selection for an input dataset.
;
; EXPLANATION:
;	This function creates a compound widget consisting of the
;	compound widget cw_adas_root, compound widget cw_file_select,
;	a title and a message.  This widget provides its own error
;	messages.
;
;	This widget generates events as the user interacts with it.
;	The event structure returned is;
;	{ID:0L, TOP:0L, HANDLER:0L, ACTION:''}
;	ACTION has one of two values 'nofile' or 'newfile' indicating
;	the current state of the file selection process.
;
; USE:
;	See routine cw_adas603_in.pro for an example.
;
; INPUTS:
;       PARENT   - Long integer; the ID of the parent widget.
;
; OPTIONAL INPUTS:
;	None.
;
; OUTPUTS:
;       The return value of this function is the ID of the compound
;       widget which is of type LONG integer.
;
; OPTIONAL OUTPUTS:
;       None
;
; KEYWORD PARAMETERS:
;	VALUE	- A structure which determines the initial settings of
;		  widgets cw_adas_root and cw_file_select.
;		  The structure must be;
;		  {ROOTPATH:'', FILE:'', CENTROOT:'', USERROOT:'' }
;		  The elements of the structure are as follows;
;
;		  ROOTPATH - Current data directory e.g '/usr/fred/adas/'
;		  FILE     - Current data file in ROOTPATH e.g 'input.dat'
;		  CENTROOT - Default central data store e.g '/usr/adas/'
;		  USERROOT - Default user data store e.g '/usr/fred/adas/'
;
;		  The data file selected by the user is obtained by
;		  appending ROOTPATH and FILE.  In the above example
;		  the full name of the data file is;
;		  /usr/fred/adas/input.dat
;
;		  Path names may be supplied with or without the trailing
;		  '/'.  The widget cw_adas_root will add this where
;		  required so that USERROOT will always end in '/' on
;		  output.
;
;		  The default value is;
;		  {ROOTPATH:'./', FILE:'', CENTROOT:'', USERROOT:''}
;		  i.e ROOTPATH is set to the user's current directory.
;
;	TITLE	- The title to be included in this widget, used
;                 to indicate exactly what the required input dataset is,
;                 e.g 'Input COPASE Dataset'
;
;       FONT    - A font to use for all text in this widget.
;
;	UVALUE	- A user value for this widget.
;
; CALLS:
;	CW_ADAS_ROOT	Used to select root adas data directory.
;	CW_FILE_SELECT	Used to select a UNIX file.
;       See side effects for other related widget management routines.
;
; SIDE EFFECTS:
;	This widget uses a COMMON BLOCK: CW_INFILE4xx_BLK to hold the
;       widget state.
;
;       Three other routines are included which are used to manage the
;       widget;
;
;	INFILE603_SET_VAL()
;	INFILE603_GET_VAL()
;	INFILE603_EVENT()
;
; CATEGORY:
;	Compound Widget
;
; WRITTEN:
;       Richard Martin, University of Strathclyde, 4-Dec-1998
;	  Based on adas602.pro.
;
; MODIFIED:
;	1.1   Richard Martin
;		Put under SCCS control                    
;
; VERSION:
;	1.1	04-12-98
;
;-
;-----------------------------------------------------------------------------

PRO infile603_set_val, id, value

                ;**** Return to caller on error ****

    ON_ERROR, 2

                ;**** Retrieve the state ****

    first_child = widget_info(id,/child)
    widget_control, first_child, get_uvalue=state, /no_copy

		;**** Get latest root and file name ****

    widget_control, state.rootid, get_value=rootval
    widget_control, state.fileid, get_value=fselval

    fselval.file = value.file
    fselval.root = value.rootpath
    rootval.rootpath = value.rootpath
    rootval.centroot = value.centroot
    rootval.userroot = value.userroot

		;**** Set the root and file name ****
    widget_control, state.rootid, set_value=rootval
    widget_control, state.fileid, set_value=fselval

    state.infileval.file = value.file
    state.infileval.rootpath = value.rootpath
    state.infileval.centroot = value.centroot
    state.infileval.userroot = value.userroot

		;**** Reset the value of state variable ****

    widget_control, first_child, set_uvalue=state, /no_copy

END

;-----------------------------------------------------------------------

FUNCTION infile603_get_val, id

		;**** Return to caller on error ****

    ON_ERROR, 2

		;**** Retrieve the state ****

    first_child = widget_info(id, /child)
    widget_control, first_child, get_uvalue=state

		;**** Get latest root and file name ****

    widget_control, state.fileid, get_value=fselval

		;**** Update state ****

    state.infileval.file = fselval.file
    state.infileval.rootpath = fselval.root


    RETURN, state.infileval

END

;-----------------------------------------------------------------------------

FUNCTION infile603_event, event

		;**** Base ID of compound widget ****

    base=event.handler

		;**** Retrieve the state ****

    first_child = widget_info(base, /child)
    widget_control, first_child, get_uvalue = state, /no_copy

		;**** Clear any existing error message ****

    widget_control, state.messid, set_value=' '

		;************************
		;**** Process Events ****
		;************************

    CASE event.id OF

        state.rootid: begin
	    if event.action eq 'newroot' then begin

		;**** Reset the value of the file selection widget ****

	        widget_control, state.rootid, get_value=rootval
	        fselval = { ROOT:rootval.rootpath, FILE:'' }
	        widget_control, state.fileid, set_value=fselval

		;**** Update the state ****

	        state.infileval.file = ''
	        state.infileval.rootpath = rootval.rootpath

		;**** Resensitise file selection ****

	        widget_control, state.fileid, /sensitive

	    endif else if event.action eq 'rootedit' then begin

		;**** When path is being edited desensitise file selection ****

	        widget_control, state.fileid, sensitive=0

	    endif
	    action = 'nofile'
	end

        state.fileid: begin
	    if event.action eq 'nofile' then begin
	        action = event.action
	    endif else begin

		;**** Get latest root and file name ****

	        widget_control, state.fileid, get_value=fselval
	        filename=fselval.root+fselval.file

		;**** Test for file read access ****

	        file_acc, filename, exist, read, write, execute, filetype
	        if read eq 0 then begin
	            action = 'nofile'
	            widget_control, state.messid,			$
		    set_value='No read access for file'
	        endif else begin
	            action = 'newfile'
	        endelse
	    endelse
	end

        ELSE:

    ENDCASE

		;**** Save the new state structure ****

    widget_control, first_child, set_uvalue=state, /no_copy

    RETURN, {ID:base, TOP:event.top, HANDLER:0L, ACTION:action}

END

;-----------------------------------------------------------------------------

FUNCTION cw_adas603_infile, topparent, VALUE=value, TITLE=title,	$
                            FONT=font, UVALUE=uvalue

    IF (N_PARAMS() LT 1) THEN MESSAGE, 'Must specify PARENT for cw_adas_root'

		;**** Set defaults for keywords ****

    IF NOT (KEYWORD_SET(value)) THEN BEGIN
        infileval = {infileval, ROOTPATH:'./', 				$
			        FILE:'', 				$
			        CENTROOT:'', 				$
			        USERROOT:'' }
    ENDIF ELSE BEGIN
        infileval = {infileval, ROOTPATH:value.rootpath, 		$
			        FILE:value.file, 			$
			        CENTROOT:value.centroot, 		$
			        USERROOT:value.userroot }
        if strtrim(infileval.rootpath) eq '' then begin
            infileval.rootpath = './'
        endif else if 							$
        strmid(infileval.rootpath,strlen(infileval.rootpath)-1,1) 	$
        ne '/' then begin
            infileval.rootpath = infileval.rootpath+'/'
        endif
        if strmid(infileval.file,0,1) eq '/' then begin
            infileval.file = strmid(infileval.file,1,strlen(infileval.file)-1)
        endif
    ENDELSE

    IF NOT (KEYWORD_SET(title)) THEN title = ''
    IF NOT (KEYWORD_SET(font)) THEN font = ''
    IF NOT (KEYWORD_SET(uvalue))  THEN uvalue = 0

		;**** Create the main base for the widget ****

    parent = WIDGET_BASE(topparent, UVALUE = uvalue, 			$
		       EVENT_FUNC = "infile603_event", 			$
		       FUNC_GET_VALUE = "infile603_get_val", 		$
		       PRO_SET_VALUE = "infile603_set_val")

		;**** Create base to hold the value of state ****

    first_child = widget_base(parent)

    cwid = widget_base(first_child,/column)

		;**** Title for input file ****

    rc = widget_label(cwid, value=title, font=font)

		;**** Root path name widget ****

    rootval = {	ROOTPATH:infileval.rootpath, $
		CENTROOT:infileval.centroot, $
		USERROOT:infileval.userroot }
    rootid = cw_adas_root(cwid, value=rootval, font=font)

	 	;**** File selection widget ****

    machine = GETENV('TARGET_MACHINE')
    if machine eq 'HPUX' then begin
	y_size = 3
    endif else begin
	y_size = 4
    endelse
    fselval = {	ROOT:infileval.rootpath, FILE:infileval.file }
    fileid = cw_file_select(cwid, value=fselval, font=font, ysize=y_size)

		;**** Message ****

    messid = widget_label(cwid, value=' ', font=font)

		;**** Test for file read access ****

    filename = infileval.rootpath+infileval.file
    file_acc, filename, exist, read, write, execute, filetype
    if filetype eq '-' and read eq 0 then begin
        widget_control,messid,set_value='No read access for file'
    endif
  
		;**** Create state structure ****

    new_state = { ROOTID:rootid, FILEID:FILEID, MESSID:messid, 		$
		  INFILEVAL:infileval, FONT:font }

		;**** Save initial state structure ****

    widget_control, first_child, set_uvalue=new_state, /no_copy

    RETURN, parent

END
