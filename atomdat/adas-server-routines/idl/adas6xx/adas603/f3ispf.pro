; Copyright (c) 1998 Strathclyde University .
; SCCS info: Module @(#)$Header: /home/adascvs/idl/adas6xx/adas603/f3ispf.pro,v 1.5 2004/07/16 09:44:42 allan Exp $ Date $Date: 2004/07/16 09:44:42 $
;+
; PROJECT:
;       ADAS IBM MVS to DEC UNIX conversion
;
; NAME:
;	F3ISPF
;
; PURPOSE:
;	Top IDL user interface routine for ADAS603 processing options.
;
; EXPLANATION:
;	The routine begins by creating the ADAS603 graph title
;	Then part of the ADAS603 IDL user interface is invoked to determine 
;	how the user wishes to process the input dataset.  
;
; USE:
;	The use of this routine is specific to ADAS603, see adas603.pro.
;
; INPUTS:
;
;	LPEND	- Integer, 0 if the user pressed the 'Done' button
;		  or 1 if the user pressed 'Cancel'.  Maps directly onto
;		  the logical variable LPEND in ADAS603 FORTRAN.
;
;	VALUE	- A structure which determines the initial settings of
;		  the processing options widget.  The initial value is
;		  set in adas603.pro.  If adas603.pro passes a blank 
;		  dummy structure of the form {NMET:-1} into VALUE then
;		  VALUE is reset to a default structure.
;
;		  The VALUE structure is;
;
;		     {NEW:0,          NLINES:0,         $
;                	EXPFLAG:0,      EXPVAL:0.0,       $
;			WAITF:0, 	    WAITVAL:0.0, 	    $ 
;                	GRPSCAL:0,      GRPRMESS:'',      $
;	  	 	XMIN:0.0,	    XMAX:0.0,	    $
;		 	YMIN:0.0,	    YMAX:0.0,	    $
;		 	GTIT:'',	    GTITFLAG:0,       $
;			FITLINES:1,				    $
;		 	FIXFLAG:0,	    LIDFLAG:0,        $
;		 	FIXPOS:dblarr(10), LINEID:strarr(10), $
;		 	VWFLAG:1,	    FWFLAG:1,	        $
;		 	FIXWID:0.0,	    BFLAG:0,	        $
;		 	BFLAT:0,	    BSLOPE:2,	        $
;		 	BCURVE:2,	    WTFLAG:1,	        $
;		 	AUTOLOOP:0,	    TITLE:'',	        $
;		 	FITFLAG:0,	    FINDEX: 0,	        $
;		      HINDEX: 0,	    FSEL: 0,		  $
;                	FMASS: 10.81,				  $
;                	BMIN: 0.0,	    BMAX: 5.0,	        $
;                	BVAL: 0.0,	    PMIN: 0.0,	        $
;                	PMAX: 2.0,	    PVAL: 1.0,	        $
;                	CMIN: -25.0,    CMAX: 25.0, 		  $  
;                	CVAL: 0.00001,  TMIN: 1.0e+5,		  $
;                	TMAX: 2.0e+6,   TVAL: 1.0e+6,    	  $
;                	ANGLE: 0.0,	    PROF: 0,	    	  $
;;                SHIFT: 0.0,	      		     	  $
;                	GAMMA: -1.0,    POL:  0,	     	  $
;                	NCOMPS: 3710,   FFP: [1,1,1,1], 	  $
;                	ION: 0,	LOWER:1,	UPPER:2	 }
;
;		  See cw_adas603_proc.pro for a full description of this
;		  structure.
;
;	ADASHEADER- Full header for output (release:prog:date:time)
;
;	HEADER1 - Header of the input data
;
;	HEADER2 - Header of the processed template.
;
;	SPECTRUM- Array containing input data.
;
;	PROC_LSTR-Structure containing processed data.
;
;       BITFILE - String; the path to the dirctory containing bitmaps
;                 for the 'escape to series menu' button.
;
;	INVAL   - Structure; the input screen settings. See cw_adas603_in.pro
;
;	OLDSTR  - A structure derived from a processed template also
;		  containing previously used fitting parameters.	
;
;	DSNINC	- String; The full system file name of the input 
;		  dataset selected by the user for processing.
;
;	DSNINX	- String; The full system file name of the input processed
;		  template.
;
; OPTIONAL INPUTS:
;	None
;
; OUTPUTS:
;	VALUE	- On output the structure records the final settings of
;		  the processing selections widget if the user pressed the
;		  'Done' button, otherwise it is not changed from input.
;
;       GOMENU  - Int; flag - set to 1 if user has selected 'escape direct
;                 to series menu , otherwise 0
;
; OPTIONAL OUTPUTS:
;	None
;
; KEYWORD PARAMETERS:
;	FONT_LARGE - The name of a larger font.
;
;	FONT_SMALL - The name of a smaller font.
;
;	EDIT_FONTS - A structure of two fonts used in the table editor
;		     adas_edtab.pro, {font_norm:'',font_input:''}.
;
; CALLS:
;	ADAS603_PROC	Invoke the IDL interface for ADAS603 data
;		 	processing options/input.
;
; CATEGORY:
;	Adas system.
;
;
; MODIFIED:
;	1.1   Richard Martin
;		Put under SCCS control            
;	1.2	Richard Martin
;		Corrected temperature scale. 
;	1.3	Richard Martin
;		Added support for Hydrogenic Lines.  
;	1.4	Richard Martin
;		Improved Documentation       
;	1.5	Allan Whiteford
;		Changed spawning of programs compiled from C to just
;		look in fortdir
;
; VERSION:
;	1.1	04-12-98
;	1.2	19-03-98
;	1.3	13-10-9
;	1.4	15-09-2000
;	1.5	08-07-2004
;-
;-----------------------------------------------------------------------------


PRO f3ispf, lpend, value, header1, header2,        $
		adasheader, spectrum, proc_lstr,   $
		gomenu, bitfile, inval, oldstr,    $
                dsninc, dsninx,           $
		FONT_LARGE=font_large, FONT_SMALL=font_small, $
		EDIT_FONTS=edit_fonts


                ;**** Set defaults for keywords ****
  IF NOT (KEYWORD_SET(font_large)) THEN font_large = ''
  IF NOT (KEYWORD_SET(font_small)) THEN font_small = ''
  IF NOT (KEYWORD_SET(edit_fonts)) THEN $
                                edit_fonts = {font_norm:'',font_input:''}

		;*******************************************
		;**** Set default value if non provided ****
		;*******************************************
  if value.new lt 0 then begin
    value = {	NEW:0,             NLINES:0,         $
                EXPFLAG:0,         EXPVAL:0.0,       $
                WAITF:0, 		WAITVAL:0.0, 	$
                GRPSCAL:0,         GRPRMESS:'',      $
                XMIN:0.0,          XMAX:0.0,         $
                YMIN:0.0,          YMAX:0.0,         $
                GTIT:'',           GTITFLAG:0,       $
                FITLINES:1,					$
                FIXFLAG:0,         LIDFLAG:0,        $
                FIXPOS:dblarr(10), LINEID:strarr(10),$
                VWFLAG:1,          FWFLAG:1,         $
                FIXWID:0.0,        BFLAG:0,          $
                BFLAT:0,           BSLOPE:2,         $
                BCURVE:2,          WTFLAG:1,         $
                AUTOLOOP:0,        TITLE:'',         $
                FITFLAG:0,	     FINDEX: 0,	     $
		    HINDEX: 0,		FSEL: 0,		$
                FMASS: 10.81,				$
                BMIN: 0.0,	     BMAX: 5.0,	     $
                BVAL: 0.0,	     PMIN: 0.0,	     $
                PMAX: 2.0,	     PVAL: 1.0,	     $
                CMIN: -25.0,       CMAX: 25.0,       $
                CVAL: 0.00001,     TMIN: 1.0e+5,     $
                TMAX: 2.0e+6,      TVAL: 1.0e+6,    $
                ANGLE: 0.0,	     PROF: 0,	     $
                SHIFT: 0.0,					$
                GAMMA: -1.0,       POL:  0,		$
                NCOMPS: 3710,	     FFP: [1,1,1,1],	$
                ION: 0,	LOWER:1,	UPPER:2	 }  
  end

  if lpend eq 1 or value.new ge 0 then value.title=''
  lpend = 0 

		;*********************************
		;**** Create ADAS Graph title ****
		;*********************************
  small_check = GETENV('VERY_SMALL')
  if small_check eq 'YES' then begin
    value.title =  value.title + '!CADAS    : ' + strupcase(adasheader)
    if inval.dflag eq 0 then begin
      type = '  : STANDARD DATA '
    end else if inval.dflag eq 1 then begin
      type = '  : PROCESSED DATA '
    end else if inval.dflag eq 2 then begin
      type = '  : OTHER DATA '
    end 
    value.title =  value.title + '!C!CFILE    : ' + strcompress(dsninc) + type
    if strtrim(dsninx,2) ne '' then begin
      value.title =  value.title + '!C!CTEMPLATE: ' + strcompress(dsninx) 
    end
    value.title =  value.title + $
    '!C!CKEY    : (HISTOGRAM - ORIGINAL DATA) (FULL LINE - FIT) (DOTTED - COMPONENTS)'
  endif else begin
    value.title =  value.title + '!CADAS    : ' + strupcase(adasheader)
    if inval.dflag eq 0 then begin
      type = '  : STANDARD DATA '
    end else if inval.dflag eq 1 then begin
      type = '  : PROCESSED DATA '
    end else if inval.dflag eq 2 then begin
      type = '  : OTHER DATA '
    end 
    value.title =  value.title + '!CFILE    : ' + strcompress(dsninc) + type
    if strtrim(dsninx,2) ne '' then begin
      value.title =  value.title + '!CTEMPLATE: ' + strcompress(dsninx) 
    end
    value.title =  value.title + $
    '!CKEY    : (HISTOGRAM - ORIGINAL DATA) (FULL LINE - FIT) (DOTTED - COMPONENTS)'
  endelse
  
  
  		;***********************************************
  		;**** Obtain list of multiplets from C code ****
  		;***********************************************

  spawn, inval.fortdir + '/multiplets603',unit=c_pipe ,/noshell  		  		
  m_dummy='                                                              '
  nmlist=60					;no of lines in Xpaschen database
  multiplet_list=STRARR(nmlist)                                                        
  for i=0,nmlist-1 do begin
	readf,c_pipe,m_dummy
 	multiplet_list(i)=m_dummy
  endfor

  spawn, inval.fortdir +  '/hdlike603',unit=c_pipe ,/noshell  		  		
  nmlist=351					;no of lines in Xpaschen database
  hdlike_list=STRARR(nmlist)                                                        
  for i=0,nmlist-1 do begin
    m_dummy=' '
	readf,c_pipe,m_dummy
 	hdlike_list(i)=m_dummy
  endfor
 
		;****************************************
		;**** Pop-up processing input widget ****
		;****************************************
 
 val=value
  
  adas603_proc, value, action, bitfile, header1, header2,     $
                spectrum, proc_lstr, inval, oldstr,           $
                multiplet_list, hdlike_list,			$
                FONT_LARGE=font_large,                        $
		FONT_SMALL=font_small, EDIT_FONTS=edit_fonts


		;******************************************
		;**** Act on the event from the widget ****
		;******************************************
		;**** There are three    possible events ****
		;**** 'Done', 'Menu' and 'Cancel'.       ****
		;********************************************

  gomenu = 0
  if action eq 'Done' then begin
    lpend = 0
  endif else if action eq 'Menu' then begin
    lpend = 0
    gomenu = 1
  endif else begin
    lpend = 1
  end


END
