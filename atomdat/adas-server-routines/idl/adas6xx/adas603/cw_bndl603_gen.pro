; Copyright (c) 1998 Strathclyde University .
; SCCS info: Module @(#)$Header: /home/adascvs/idl/adas6xx/adas603/cw_bndl603_gen.pro,v 1.1 2004/07/06 13:04:42 whitefor Exp $ Date $Date: 2004/07/06 13:04:42 $
;+
; PROJECT:
;       IDL-ADAS development
;
; NAME:
;       CW_BNDL603_GEN()
;
; PURPOSE:
;       Unbundling selection list with text boxes for specification of
;       groupings and automatic 'next row' input focus.
;
; EXPLANATION:
;       This compound widget produces an interactive selection list 
;       which consists of a WIDGET_LABEL and an editable WIDGET_TEXT
;       box for each item in a given list of string labels. A title
;       can also be specified.
;
;       The list of labels is specified as a one dimensional string 
;       array of the items to be grouped. The procedure passes back
;       an integer array containing the numbers input by the user
;       at the index of the label they chose and zeros everywhere
;       else.
; USE:    
;       General purpose.
;
;       See the routine CW_ADAS603_PROC for a working example of how to 
;       use CW_BNDL603_GEN.
;
;       The user makes selections by entering a number in the text box
;       next to the one they choose, and can group selections together
;       by putting the same number next to each choice. The user MUST 
;       press return to register their selection with XMANAGER. Any 
;       number of selections can be made and choices can also be altered.
;       The selections are returned in bndls, with zeros where no
;       selections were made.
;  
;       The following code is an example of a popup selection widget
;       displaying a list of the numbers five to ten.
;         
;       x = ['five','six','seven','eight','nine','ten']
;       title = 'Select Prime Numbers'
;       nsizex = n_elements(x)
;       sels = intarr(nsizex)
;       val = { labels:x, title:title, bndls:sels }
;       wid = cw_bndl603_gen(parent,nsizex,value=val)
;
;       After selecting the prime numbers, val.bndls will contain;
;       [1,0,1,0,0,0]
;           
; INPUTS: 
;
;       PARENT     - The parent ID of the compound widget.
;
;       LABELS     - String; Array of labels.
;
; OUTPUTS:
;       None
;
; KEYWORD PARAMETERS:
;       FONT       - The name of a font.
;
;       OUTSTR      - The structure which contains the initial settings
;                    of the widget, and finally the user selections.
;                    The default value is;
;                    outstr = { outstr_set,                  $
;                                 bndls:str_arr,                $ 
;                                 groups:int_arr                  $
;                                }
;                    where str_arr = strarr(nlabels)
;		     and   int_arr = lonarr(nlabels)
;                    labels is an array containing the strings that will
;                    appear as labels next to the input boxes. Title, is
;                    the title that will appear on the header. Bndls
;                    is the selection vector recording the numbers entered
;                    by the user and zeros elsewhere. For a working example
;                    see cw_adas603_proc.pro. 
;
;	TITLE	-    Widget Title
;
;	NOFRAME -    Set if no frame is desired in the widget.
;
;	NCOLS	-    Number of columns.
;
;	XSIZE	-    X dimension of the widget.
;
;	SIZECOL -    Column size (default 13).
;	
; CALLS:
;        BNDL603_GEN_EVENT   Called indirectly during widget management
;                         to handle the widget events. Procedure is
;                         included in this file.
;        BNDL603_GEN_GET_VAL Widget management, included in this file.
;
; SIDE EFFECTS:
;        None
;
; CATEGORY:
;        Compound widget
;
; WRITTEN:
;       Richard Martin, University of Strathclyde, 4-Dec-1998
;	  Based on cw_bndl602_gen.pro.
;
; MODIFIED:
;	1.1   Richard Martin
;		Put under SCCS control                    
;
; VERSION:
;	1.1	04-12-98
;-
;-----------------------------------------------------------------------------

FUNCTION bndl603_gen_get_val, id

    ON_ERROR, 2

	;**** Retrieve the structure from the child ****
        ;**** that contains the sub ids.            ****

    stash = WIDGET_INFO(id, /CHILD)
    WIDGET_CONTROL, stash, GET_UVALUE=state, /NO_COPY

  
    for i = 0, (state.nlabels-1) do begin
        widget_control, state.groups(i), get_value=value
        state.bndls(i) = value
    endfor 

        ;**** copy selections to output structure ****

    outstr = { BNDLS:state.bndls, GROUPS:state.groups}

	;**** Restore the state. ****

    WIDGET_CONTROL, stash, SET_UVALUE=state, /NO_COPY

    RETURN, outstr

END

;-----------------------------------------------------------------------------

FUNCTION bndl603_gen_event, event

    parent=event.handler

	;**** Retrieve the structure from child ****

    stash = WIDGET_INFO(parent, /CHILD)

    WIDGET_CONTROL, stash, GET_UVALUE=state, /NO_COPY

    name = strmid(tag_names(event, /structure_name),7,1000)

           ;***************************************;
           ;******* process user selections *******;
           ;***************************************;

    case(name) of

    	"TEXT_CH":begin

            widget_control, event.id, get_uvalue = uvalue
            if (uvalue ne state.nlabels-1) then	                        $
            widget_control, /input_focus, state.groups(uvalue+1)
        end

	ELSE:				;**** Do nothing ****

     ENDCASE

        ;**** Restore the state structure ****

    WIDGET_CONTROL, stash, SET_UVALUE=state, /NO_COPY

    RETURN, new_event 

END

;-----------------------------------------------------------------------------

FUNCTION cw_bndl603_gen, parent, labels, OUTSTR=outstr, TITLE=title,$
                      UVALUE=uval, FONT=font, NOFRAME=noframe,   $
                      NCOLS=ncols, XSIZE=xsize, SIZECOL=sizecol
                
    IF (N_PARAMS() EQ 0) THEN MESSAGE, 'Must specify a parent for cw_bndl603_gen'

    ON_ERROR, 2					;return to caller
    IF NOT (KEYWORD_SET(title))  THEN title = ''
    IF NOT (KEYWORD_SET(uval))  THEN uval = 0
    IF NOT (KEYWORD_SET(xoffset)) THEN xoffset = 200
    IF NOT (KEYWORD_SET(font)) THEN font = ''
    IF NOT (KEYWORD_SET(noframe)) THEN noframe = 0  
    IF NOT (KEYWORD_SET(ncols)) THEN ncols = $
                                fix(1+n_elements(labels)/sizecol)
    IF NOT (KEYWORD_SET(xsize)) THEN xsize = 5   
    IF NOT (KEYWORD_SET(sizecol)) THEN sizecol = 13  
    IF NOT (KEYWORD_SET(OUTSTR)) then begin
      str_arr=strarr(n_elements(labels))
      int_arr=lonarr(n_elements(labels))
      outstr = { outstr_set, bndls:str_arr, groups:int_arr}
    ENDIF ELSE begin
      outstr = { outstr_set, bndls:outstr.bndls, groups:outstr.groups}
    END

           ;**** declare some variables ****

    nlabels=n_elements(labels)
    terms = indgen(nlabels)
    groups = lonarr(nlabels)
    space = terms
    rowtxt = intarr(nlabels)

    base = WIDGET_BASE(parent, UVALUE = uval, 				$
                       EVENT_FUNC = "bndl603_gen_event", 			$
                       FUNC_GET_VALUE = "bndl603_gen_get_val", 		$
                       /row, XOFFSET=1, YOFFSET=1)

    row  = widget_base(base, /row)
    row1 = widget_base(row, /column)
    row2 = widget_base(row1, /row)

           ;***************************************;
           ;*determine number of columns required *;
           ;* each column is length 13 but can be *;
           ;*************** altered ***************;
           ;***************************************;

    col = intarr(ncols)
 
    for j = 0, (ncols-1) do begin
        col(j) = widget_base(row2, /column)
        for i = (sizecol*j), min([(sizecol*j)+(sizecol-1),nlabels-1]) do begin
            rowtxt(i) = widget_base(col(j),/row)
            if noframe ne 0 then begin
              terms(i)  = widget_label(rowtxt(i), value = labels(i),	$
              font=font)
            end else begin
              terms(i)  = widget_label(rowtxt(i), value = labels(i),	$
              font=font, /frame)
            end
            chkstr=size(outstr.bndls(i))
            if chkstr(1) eq 7 then begin
              if strtrim(outstr.bndls(i),2) eq 'u' then begin
                val=''
              end else val = strtrim(outstr.bndls(i),2)
            end else begin
              if outstr.bndls(i) eq 0.0 then begin
                val = ''
              end else val = strtrim(outstr.bndls(i),2)
            end
            groups(i) = widget_text(rowtxt(i), value=val, xsize = xsize,$
                                    /edit, font=font)
            widget_control, groups(i), set_uvalue = i
        endfor
    endfor
;    widget_control, groups(0), /input_focus

    state = {GROUPS:groups, NLABELS:nlabels, BNDLS:outstr.bndls }

    WIDGET_CONTROL, WIDGET_INFO(base, /CHILD), SET_UVALUE=state, /NO_COPY

    RETURN, base

END





