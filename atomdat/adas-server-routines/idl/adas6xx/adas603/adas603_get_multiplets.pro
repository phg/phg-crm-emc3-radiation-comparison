;+
; PROJECT : ADAS 
;
; NAME    : ADAS603_GET_MULTIPLETS()
;
; PURPOSE : Returns the list of multiplets ADAS603 can calculate.
;
; EXPLANATION:
;       This function spawns multiplets603 and puts the output in
;       a string array with the leading indices removed.
;
; INPUTS:
;       None
;
; OPTIONAL INPUTS:
;       fortdir  : Location of central ADAS binaries (ADASFORT)
;		   Determined from environment variables if not specified
;
; OUTPUTS:
;       This function returns a string array of multiplets available
;	inside ADAS603 and from the adas603_get_zeeman procedure.
;
; OPTIONAL OUTPUTS:
;       None.
;
; KEYWORD PARAMETERS:
;       None.
;
; CALLS:
;       None.
;
; SIDE EFFECTS:
;       This function spawns UNIX commands.
;
; CATEGORY:
;       Part of ADAS603 and general utility.
;
; WRITTEN:
;       Allan Whiteford
;
; MODIFIED:
;       Version 1.1   Allan Whiteford
;                     First release.
;
; VERSION:
;       1.1    14-03-2008
;-
;-----------------------------------------------------------------------------


function adas603_get_multiplets,fortdir=fortdir
	
        if not keyword_set(fortdir) then fortdir=getenv("ADASFORT")
                
        if fortdir eq '' then begin
        	print,'Unable to ascertain location of ADAS binaries'
                return,['']
        endif 
        
	spawn, fortdir + '/multiplets603',output ,/noshell 
	
	if n_elements(output) lt 2 then begin
		print,'Unable to get multiplet list'
                return,['']
        endif

	for i=0,n_elements(output)-1 do output[i]=strtrim(strmid(output[i],3),2)
	return,output
end 		  		

