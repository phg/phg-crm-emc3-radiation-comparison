; Copyright (c) 2001 Strathclyde University .
; SCCS info: Module @(#)$Header: /home/adascvs/idl/adas6xx/adas603/adas603_get_hdlike.pro,v 1.5 2019/07/24 20:27:06 mog Exp $ Date $Date: 2019/07/24 20:27:06 $
;+
; PROJECT : ADAS 
;
; NAME    : ADAS603_GET_HDLIKE()
;
; PURPOSE : Returns a H-like ion's Zeeman feature.
;
; EXPLANATION:
;
;       This function spawns hdlikecomponents603, passes it observational
;       information and returns wavelength and line strength arrays.
;
;
;
; INPUTS:
;       fortdir (string)  : Location of central ADAS binaries (ADASFORT)
;       pol     (int)     : Polarisation
;                             pol =  0  NONE
;                                 =  1  PI+SIGMA
;                                 =  2  PI
;                                 =  3  SIGMA
;                                 =  4  SIGMA+
;                                 =  5  SIGMA-
;       obsangle (double) : Observation angle
;       bvalue   (double) : Magnetic field strength (T)
;       lower    (int)    : Lower n value of transition.
;       upper    (int)    : Upper n value of transition.
;       ion      (int)    : Key to element choice (see interactive 603 for list)
;                             ion =  0  H
;                                 =  1  D
;                                 =  2  T
;                                 =  3  He
;                                 =  4  C
;                                 =  5  Ne
;
; OPTIONAL INPUTS:
;	None
;
; OUTPUTS:
;       This function returns 2 arrays. 
;
;       m_wlength  : wavelength array
;       m_strength : line strength array
;
; OPTIONAL OUTPUTS:
;       err        : An error message is inserted in this
;                    string if it is specified, otherwise
;                    the code will stop when encoutering
;                    an error in the underlying evaluation. 
;
; KEYWORD PARAMETERS:
;       None.
;
; CALLS:
;       None.
;
; SIDE EFFECTS:
;       This function spawns UNIX commands.
;
; CATEGORY:
;       Part of ADAS603 and general utility.
;
; WRITTEN:
;       Martin O'Mullane
;
; MODIFIED:
;       Version 1.1   Martin O'Mullane
;                     First release.
;                      - Make sure variable passed to C routine have 
;                        the correct type.
;                      - Update the documentation.
;       Version 1.2   Allan Whiteford
;                     Changed spawning of programs compiled from C
;                     to just look in fortdir.
;       Version 1.3   Allan Whiteford
;                     Option to return error message if
;                     calculation fails.
;       Version 1.4   Martin O'Mullane
;                      - Replace obsolete str_sep with strsplit.
;       Version 1.5   Martin O'Mullane
;                      - Give actual error message rather than the useless
;                        'something has gone wrong'.
;
; VERSION:
;
;       1.1    09-01-2002
;       1.2    08-07-2004
;       1.3    24-02-2009
;       1.4    17-10-2018
;       1.5    24-07-2019
;-
;-----------------------------------------------------------------------------

PRO adas603_get_hdlike, fortdir, pol, obsangle, bvalue, lower, upper, ion, $
                        m_wlength, m_strength, err

ncomp = 0
fdum  = ''


; Spawn components603 to get the Zeeman pattern

  spawn, fortdir + '/hdlikecomponents603',unit=c_pipe ,/noshell
  

; Make sure the requested parameters match the C definitions 

   pol_in       = fix(pol)
   obsangle_in  = double(obsangle)
   bvalue_in    = double(bvalue)
   lower_in     = fix(lower)
   upper_in     = fix(upper)
   ion_in       = fix(ion)

; Send the requested parameters   

   printf,c_pipe,pol_in
   printf,c_pipe,obsangle_in
   printf,c_pipe,bvalue_in
   printf,c_pipe,lower_in
   printf,c_pipe,upper_in
   printf,c_pipe,ion_in

; Get stuff back
	 
   readf,c_pipe,fdum
   flag=strtrim(string(fdum),2)
   dummy=' '
   if flag eq 0 then begin
     readf, c_pipe,dummy
     if arg_present(err) then begin
       err=dummy
       close,c_pipe
       free_lun,c_pipe	
       return
     endif else begin
       message, dummy
     endelse
   endif   

; Read in no of line components

   readf,c_pipe,ncomp
  
   m_wlength  = fltarr(ncomp)        
   m_strength = fltarr(ncomp)          
    	
; Read in string, and separate into wlength and line strength arrays
	
   fdum = 0.0
   for i = 0, ncomp-1 do begin
      readf,c_pipe,dummy
      parts = strsplit(dummy,"#", /extract)
      sdum  = strlowcase(parts(3))
      if strpos(sdum,'nan') EQ -1 then fdum = float(parts(3)) else fdum = 0.0
      m_wlength(i)  = float(parts(2))
      m_strength(i) = fdum
   endfor
	
   close,c_pipe
   free_lun,c_pipe	

END
