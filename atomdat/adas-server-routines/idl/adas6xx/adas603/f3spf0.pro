; Copyright (c) 1998 Strathclyde University .
; SCCS info: Module @(#)$Header: /home/adascvs/idl/adas6xx/adas603/f3spf0.pro,v 1.1 2004/07/06 13:51:51 whitefor Exp $ Date $Date: 2004/07/06 13:51:51 $
;+
; PROJECT:
;       ADAS IBM MVS to DEC UNIX conversion
;
; NAME:
;	F3SPF0
;
; PURPOSE:
;	IDL user interface to obtain input datasets.
;
; EXPLANATION:
;	This routine invokes the part of the user interface
;	used to select the input dataset for ADAS603.
;
; USE:
;	The use of this routine is specific to ADAS603, see adas603.pro.
;
; INPUTS:
;
;	VALUE	- A structure which determines the initial settings of
;		  the dataset selection widget.  The initial value is
;		  set in adas603.pro.  VALUE is passed un-modified
;		  through to cw_adas_in.pro, see that routine for a full
;		  description.
;
; OPTIONAL INPUTS:
;	None
;
; OUTPUTS:
;	VALUE	- On output the structure records the final settings of
;		  the dataset selection widget if the user pressed the
;		  'Done' button otherwise it is not changed from input.
;
;	DSNINC	- String; The full system file name of the input
;		  dataset selected by the user for processing.
;
;	DSNINX	- String; The full system file name of the input processed
;		  template.
;
;	REP	- String; Indicates whether the user pressed the 'Done'
;		  or 'Cancel' button on the interface.  The action is
;		  converted to the strings 'NO' and 'YES' respectively
;		  to match up with the existing FORTRAN code.  In the
;		  original IBM ISPF interface REP was the reply to the
;		  question 'DO YOU WANT TO END PROGRAM EXECUTION'.
;
; OPTIONAL OUTPUTS:
;	None
;
; KEYWORD PARAMETERS:
;	FONT	- Supplies the font to be used for the interface widgets.
;
; CALLS:
;	ADAS603_IN		Pops-up the dataset selection widget.
;
; SIDE EFFECTS:
;	None
;
; CATEGORY:
;	Adas system.
;
; WRITTEN:
;       Richard Martin, University of Strathclyde, 4-Dec-1998
;	  Based on f3spf0.pro.
;
; MODIFIED:
;	1.1   Richard Martin
;		Put under SCCS control                    
;
; VERSION:
;	1.1	04-12-98
;
;-

;-----------------------------------------------------------------------------


PRO f3spf0, value, dsninc, dsninx, rep, FONT=font     


                ;***********************************
                ;**** Set defaults for keywords ****
                ;***********************************
  IF NOT (KEYWORD_SET(font)) THEN font = ''


		;**********************************
		;**** Pop-up input file widget ****
		;**********************************
  adas603_in, value, action, WINTITLE = 'ADAS 603 INPUT', $
              TITLE = 'Input Dataset', FONT = font

		;******************************************
		;**** Act on the event from the widget ****
		;******************************************
		;**** There are only two possible events ****
		;**** 'Done' and 'Cancel'.               ****
		;********************************************
  if action eq 'Done' then begin
    rep = 'NO'
  end else begin
    rep = 'YES'
  end

                ;***********************************
		;**** Construct dataset name    ****
                ;***********************************
  dsninc = strcompress(value.rootpath1+value.file1)
  dsninx = strcompress(value.rootpath2+value.file2)

                ;*************************************************
                ;**** Determine whether template  file exists ****
                ;*************************************************

  file_acc, dsninx, ex, re, wr, ex, ty
  if ty ne '-' then begin
    dsninx = ' '
  end

END
