; Copyright (c) 1998 Strathclyde University .
; SCCS info: Module @(#)$Header: /home/adascvs/idl/adas6xx/adas603/feature_plot.pro,v 1.6 2018/10/17 10:19:32 mog Exp $ Date $Date: 2018/10/17 10:19:32 $
;+
; PROJECT:
;       ADAS IBM MVS to DEC UNIX conversion
;
; NAME:
;	FEATURE_PLOT
;
; PURPOSE:
;	Overplots zeeman feature envelope and componenets onto an already 
;	existing plot window.
;
; EXPLANATION:
;	This routine is called to overplot the feature envelope and zeeman
;	components, after a parameter has been modifies, and thus the
;	envelope/components must be re-calculated. The C-modules 
;	"componenets603" and "hdlikecomponents603" are called and the 
;	envelope calculated.
;
; USE:	This routine is specific to adas603.
; 
; INPUTS:
;	STATE	- Input structure which contains all necessary variable and parameter
;		  values (see cw_adas603_proc.pro).
;	
; KEYWORD PARAMETERS:
;
;	FXPVAL	- Contains values of B field, Peak Height, Centriod position
;			  and Temperature.
;	NODCOPY	- Do not copy pixmap of previously stored graphics window to 
;			  current graphics window.
;	FEATURE	- Returns array containing feature envelope
;	WLENGTH	- Returns array containing wavelength range for feature envelope.
;
; CALLS:
;
;	COMPONENTS603 - C routine which returns zeeman multiplet components
;	HDLIKECOMPONENTS603 - C routine which returns components for Hydrogenic lines.
;
; WRITTEN:
;       Richard Martin, University of Strathclyde, 4-Dec-1998
;	  Based on adas602.pro.
;
; MODIFIED:
;	1.1   Richard Martin
;		Put under SCCS control
;	1.2	Richard Martin
;		Corrected temperature conversion   
;	1.3	Richard Martin
;		Added support for Hydrogenic lines.
;	1.4	Richard Martin
;		Improved documentation.       
;		Removed conversion of cvalue from %age of window to pixels.
;		cvalue now comes in as pixels.          
;	1.5	Allan Whiteford
;		Changed spawning of programs compiled from C to just
;		look in fortdir.
;       1.6   Martin O'Mullane
;               - Replace obsolete str_sep with strsplit.
;          
;
; VERSION:
;	1.1	04-12-98
;	1.2	19-03-99
;	1.3	13-10-99
;	1.4	15-09-2000
;	1.5	08-07-2004
;       1.6     17-10-2018
;-
;-----------------------------------------------------------------------------

PRO	feature_plot, state, FXPVAL = fxpval, NODCOPY = nodcopy, $
	FEATURE= feature2, WLENGTH = wlength2

;	********************************
;	** Obtain selected line index **
;	********************************

	if state.ps.fsel eq 0 then begin
    		widget_control, state.featureid, get_value=dummy		
	endif else begin		
    		widget_control, state.hdlikeid, get_value=dummy	
		!error=0		;	Temporary fix
	endelse
	findex=dummy(0)	

IF NOT (KEYWORD_SET(fxpval)) THEN BEGIN
;	********************************
;	**  Obtain values of sliders  **
;	********************************

      widget_control, widget_info(state.bslid,/CHILD), get_uvalue=bstruct
      bvalue=bstruct.slval

      widget_control, widget_info(state.cslid,/CHILD), get_uvalue=cstruct      
      cvalue=cstruct.slval		
		
      widget_control, widget_info(state.pslid,/CHILD), get_uvalue=pstruct      
      pvalue=pstruct.slval

      widget_control, widget_info(state.iopslid,/CHILD), get_uvalue=istruct        
      tvalue=istruct.slval
      
END ELSE BEGIN

	bvalue=fxpval(0)
	pvalue =fxpval(1)     
	cvalue=fxpval(2)
	tvalue=fxpval(3)
END

      widget_control, state.obstext, get_value=temp
      obsangle = float(temp(0))

      widget_control, state.shifttext, get_value=temp
      shift = float(temp(0))      

      widget_control, state.polbut, get_value=temp2

      pol = temp2(0) 

;	********************************
;	**  value for gamma     **
;	********************************

	gamma=state.ps.gamma

;	**************************************************************
;	** Spawn C program 'components' and write parameters to it  **
;	**************************************************************

    	widget_control, state.mbut, get_value=temp

    	if temp(0) eq 0 then begin
    	   spawn, state.inval.fortdir + '/components603',unit=c_pipe ,/noshell
	
	   printf,c_pipe,pol
	   printf,c_pipe,obsangle
	   printf,c_pipe,bvalue
    	   printf,c_pipe,findex

	endif else begin	

    	   spawn, state.inval.fortdir + '/hdlikecomponents603',unit=c_pipe ,/noshell
	
	   printf,c_pipe,pol
	   printf,c_pipe,obsangle
	   printf,c_pipe,bvalue
	   printf,c_pipe,state.lower	   
	   printf,c_pipe,state.upper 
    	   printf,c_pipe,state.ion	
		
	   readf,c_pipe,fdum
	   flag=strtrim(string(fdum),2)
	   dummy=' '
	   if flag eq 0 then begin
	     readf, c_pipe,dummy
           widget_control,state.messid,set_value=dummy	   
	     goto,labelend 	
	   endif   
	endelse
   	dummy='                                                              '

;	***********************************
;	** Read in no of line components **
;	***********************************

	readf,c_pipe,ncomp

	state.m_wlength(*)=0.0		
	state.m_strength(*)=0.0		
	
;	*************************************************************************
;	** Read in string, and separate into wlength and line stratngth arrays **
;	************************************************************************
	

	for i=0,ncomp-1 do begin
		readf,c_pipe,dummy
		parts=strsplit(dummy,"#", /extract)
		state.m_wlength(i)=float(parts(2))
		state.m_strength(i)=float(parts(3))
	endfor
	
	close,c_pipe
	free_lun,c_pipe	

;	****************************************************
;	** Find pixel/wlength scale for original spectrum **
;	****************************************************
	
	nwl=n_elements(state.spectrum)

	scale=state.header.nsize/(state.header.wavemax-state.header.wavemin)

	nwl=n_elements(state.spectrum)	
	wlength=fltarr(ncomp)
	strength=fltarr(ncomp)
	tmp_wlength	=fltarr(ncomp)
	tmp_strength=fltarr(ncomp)
	
;	******************
;	** Find x-range **
;	******************	  
	
	if ( state.ps.grpscal gt 0 ) then begin
		widget_control, state.xminid, get_value=temp
		xmin=float(strtrim(temp(0),2))
		widget_control, state.xmaxid, get_value=temp
		xmax=float(strtrim(temp(0),2))
      endif 

;	********************************************
;	** Scale wlengths of components to pixels **
;	********************************************	
	
	for i=0,ncomp-1 do begin
	   if (state.m_wlength(i) gt 0.0) then $
	     tmp_wlength(i)=(state.m_wlength(i)-state.header.wavemin)*scale + cvalue  		   
	endfor
		       
;	******************************************
;	** Copy 'feature-less' plot from memory **
;	******************************************	    

IF NOT (KEYWORD_SET(nodcopy)) THEN BEGIN
	wset,state.gwin
	width=!d.x_size
	height=!d.y_size
	device, copy=[0,0,width,height,0,0,9]
END

;	**********************************************
;	** Obtain wlength and strength arrays 	  **
;	** i.e. arrays of component wavelengths     **
;     ** & strength sorted accorded to wavelength **
;	**********************************************

      wlength=tmp_wlength(where(tmp_wlength ne 0.0))      
	indices=sort(wlength)
	wlength=wlength(indices)

	state.m_wlength=tmp_wlength
      strength=state.m_strength(where(tmp_wlength ne 0.0))
      strength=strength(indices)

;	***********************************************
;	** Obtain max & min wavelength of components **
;	***********************************************

	wmin=min(wlength(where(wlength ne 0.0)))
	wmax=max(wlength)

;	*************************************************
;	** Obtain array of x components (pixel no's)   **
;     ** for feature envelope + create feature array **
;	*************************************************

	tmp1=fix(wmin-50+1)
	tmp2=fix(wmax+50)
      w_length=indgen(tmp2-tmp1)+tmp1
	feature=fltarr(n_elements(w_length))
	
;	*************************************************
;	** Check selected feature lies within selected **
;     ** pixel range					     **
;	*************************************************

	if ( state.ps.grpscal gt 0 ) then begin	
		if ( wmin lt xmin OR wmax gt xmax ) then begin
		      state.ps.grprmess = '*** selection outside range: make another selection ***'		
          		widget_control,state.messid,set_value=state.ps.grprmess
			goto, labelend
		endif
	endif else begin
		if ( wmin lt 0.0 OR wmax gt float(nwl) ) then begin
		      state.ps.grprmess = '*** selection outside range: make another selection ***'		
          		widget_control,state.messid,set_value=state.ps.grprmess
		endif
	endelse
	
;	*******************************
;	** Build up feature envelope **
;	*******************************

	  M=state.ps.fmass*1.66057e-27
	  c=2.99e+8
	  k=1.346e-23
		   	
	  if (gamma ne 0.0) then begin		;finite linewidth
			
		for i=0,ncomp-1 do begin
	   	   index=where(w_length eq round(wlength(i))) ;wlength(i) not integer
		   if (gamma eq -1.0) then begin	;doppler broadening
		   	for j=-50,49,1 do begin
	      		line=exp(-M*c^2/(2*k*tvalue)*((float(w_length(index+j)-wlength(i)))/	$
	      		(wlength(i)+state.header.wavemin*scale))^2)*strength(i)
	      		feature(index+j)=feature(index+j)+line
	   		endfor
	   	   endif else begin
	   		for j=-50,49,1 do begin
	      		line=(gamma^2/(float(j)^2+gamma^2))*strength(i)
	      		feature(index+j)=feature(index+j)+line
	   		endfor
		   endelse
		endfor

;		***************************
;		** Plot feature envelope **
;		***************************
		fdum=where(feature gt 0.0)
		if (fdum(0) eq -1 ) then begin
			state.ps.grprmess = '**** No Feature available for these parameters ***'
            	widget_control, state.messid, set_value=state.ps.grprmess
			goto, labelend
		endif else begin
	   		oplot,w_length,feature*pvalue+shift,	$
	   	   		linestyle=1, thick=2
	   	   	oplot,[w_length,w_length],[shift,shift],linestyle=2
	   	   	IF (KEYWORD_SET(FEATURE2)) THEN BEGIN	
	   	   		if (n_elements(w_length) gt state.ps.ncomps) then begin
	   	   			nmax=state.ps.ncomps
	   	   		endif else begin
	   	   			nmax=n_elements(w_length)
	   	   		endelse
	   	   		feature2(0:nmax-1)=feature(0:nmax-1)	;return feature envelope to calling routine
	   	   		wlength2(0:nmax-1)=w_length(0:nmax-1)	   	
	   	   	ENDIF	   	   	
		endelse 
				
;		*********************
;		** Plot components **
;		*********************

		for i=0,ncomp-1 do begin
	   	  oplot,[wlength(i),wlength(i)],	$
	   	  		[0.0,strength(i)*pvalue]
		endfor

	  endif
	  
labelend:

trap=check_math()
if (trap eq 32) then !error=0 

end
