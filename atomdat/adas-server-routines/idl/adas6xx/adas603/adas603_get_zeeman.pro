; Copyright (c) 2001 Strathclyde University .
; SCCS info: Module @(#)$Header: /home/adascvs/idl/adas6xx/adas603/adas603_get_zeeman.pro,v 1.3 2018/10/17 10:19:32 mog Exp $ Date $Date: 2018/10/17 10:19:32 $    
;+
; PROJECT : ADAS 
;
; NAME    : ADAS603_GET_ZEEMAN()
;
; PURPOSE : Returns a Zeeman feature.
;
; EXPLANATION:
;       This function spawns components603, passes it observational
;       information and returns wavelength and line strength arrays.
;
;
;
; INPUTS:
;       fortdir  : Location of central ADAS binaries (ADASFORT)
;       pol      : Polarisation
;                  pol =  0  NONE
;                      =  1  PI+SIGMA
;                      =  2  PI
;                      =  3  SIGMA
;                      =  4  SIGMA+
;                      =  5  SIGMA-
;       obsangle : Observation angle
;       bvalue   : Magnetic field strength (T)
;       findex   : Feature index (see interactive 603 for list).
;
; OPTIONAL INPUTS:
;       None.
;
; OUTPUTS:
;       This function returns 2 arrays. 
;
;       m_wlength  : wavelength array
;       m_strength : line strength array
;
; OPTIONAL OUTPUTS:
;       None.
;
; KEYWORD PARAMETERS:
;       None.
;
; CALLS:
;       None.
;
; SIDE EFFECTS:
;       This function spawns UNIX commands.
;
; CATEGORY:
;       Part of ADAS603 and general utility.
;
; WRITTEN:
;       Martin O'Mullane
;
; MODIFIED:
;       Version 1.1   Martin O'Mullane
;                     First release.
;	Version 1.2   Allan Whiteford
;                     Changed spawning of programs compiled from C
;                     to just look in fortdir.
;       Version 1.3   Martin O'Mullane
;                      - Replace obsolete str_sep with strsplit.
;
; VERSION:
;       1.1    05-06-2000
;       1.2    08-07-2004
;       1.3    17-10-2018
;-
;-----------------------------------------------------------------------------

PRO adas603_get_zeeman, fortdir, pol, obsangle, bvalue, findex, $
                        m_wlength, m_strength


; Spawn components603 to get the Zeeman pattern

   spawn, fortdir + '/components603',unit=c_pipe ,/noshell
 

; Send the requested parameters   

   printf,c_pipe,pol
   printf,c_pipe,obsangle
   printf,c_pipe,bvalue
   printf,c_pipe,findex


; Read in no of line components 


    readf,c_pipe,ncomp

    m_wlength  = fltarr(ncomp)        
    m_strength = fltarr(ncomp)          
    
; Read in string, and separate into wavelength and line strength arrays 
        
   dummy=' ' 
   for i=0,ncomp-1 do begin
        readf,c_pipe,dummy
        parts=strsplit(dummy,"#",/extract)
        m_wlength(i)=float(parts(2))
        m_strength(i)=float(parts(3))
   endfor

   close,c_pipe
   free_lun,c_pipe 

END
