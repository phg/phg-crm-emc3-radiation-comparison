; Copyright (c) 1998 Strathclyde University .
; SCCS info: Module @(#)$Header: /home/adascvs/idl/adas6xx/adas603/adas603.pro,v 1.8 2004/07/06 11:02:40 whitefor Exp $ Date $Date: 2004/07/06 11:02:40 $
;+
; PROJECT:
;       ADAS IBM MVS to DEC UNIX conversion
;
; NAME:
;	ADAS603
;
; PURPOSE:
;	The highest level routine for the ADAS 603 program.
;
; EXPLANATION:
;	This routine is called from the main adas system routine, adas.pro,
;	to start the ADAS603 application.  Associated with adas603.pro
;	is a FORTRAN executable.  The IDL code provides the user
;	interface and output graphics whilst the FORTRAN code reads
;	in data files, performs numerical processing and creates the
;	output files.  The IDL code communicates with the FORTRAN
;	executable via a bi-directional UNIX pipe.  The unit number
;	used by the IDL for writing to and reading from this pipe is
;	allocated when the FORTRAN executable is 'spawned' (see code
;	below).  Pipe communications in the FORTRAN process are to
;	stdin and stdout, i.e streams 5 and 6.
;
;	The FORTRAN code is an independent process under the UNIX system.
;	The IDL process can only exert control over the FORTRAN in the
;	data which it communicates via the pipe.  The communications
;	between the IDL and FORTRAN must be exactly matched to avoid
;	input conversion errors.  The correct ammounts of data must be
;	passed so that neither process 'hangs' waiting for communications
;	which will never occur.
;
;	The FORTRAN code performs some error checking which is
;	independent of IDL.  In cases of error the FORTRAN may write
;	error messages.  To prevent these error messages from conflicting
;	with the pipe communications all FORTRAN errors are written to
;	output stream 0, which is stderr for UNIX.  These error messages
;	will appear in the window from which the ADAS session/IDL session
;	is being run.
;
;	In the case of severe errors the FORTRAN code may terminate
;	itself prematurely.  In order to detect this, and prevent the
;	IDL program from 'hanging' or crashing, the IDL checks to see
;	if the FORTRAN executable is still an active process before
;	each group of pipe communications.  The process identifier
;	for the FORTRAN process, PID, is recorded when the process is
;	first 'spawned'.  The system is then checked for the presence
;	of the FORTRAN PID.
;
; USE:
;	First the system settings must be established by calling
;	adas_sys_set.pro then adas603.pro is called to start the
;	ADAS 603 application;
;
;	adas_sys_set, adasrel, fortdir, userroot, centroot, $
;		      devlist, devcode, font_large, font_small, edit_fonts
;	adas603,   adasrel, fortdir, userroot, centroot, devlist, $
;		   devcode, font_large, font_small, edit_fonts
;
; INPUTS:
;	ADASREL - A string indicating the ADAS system version, 
;		  e.g ' ADAS RELEASE: ADAS93 V1.1.0'.  The first
;		  character should be a space.
;
;	FORTDIR - A string holding the path to the directory where the
;		  FORTRAN executables are, e.g '/disk/adas/idl_adas/fortran/bin'
;
;	USERROOT - A string holding the path to the root directory of
;		   the user's adas data. e.g '/disk/bowen/adas'
;		   This root directory will be used by adas to construct
;		   other path names.  For example the users default data
;		   for adas603 should be in /disk/bowen/adas/arch603.  In
;		   particular the user's default interface settings will
;		   be stored in the directory USERROOT+'/defaults'.  An
;		   error will occur if the defaults directory does not
;		   exist.
;
;	CENTROOT - Like USERROOT, but this directory points to the
;		   central data area for the system.  User defaults are
;		   not stored on CENTROOT.
;
;	DEVLIST - A string array of hardcopy device names, used for
;		  graphical output. e.g ['Post-Script','HP-PCL','HP-GL']
;		  This array must mirror DEVCODE.  DEVCODE holds the
;		  actual device names used in a SET_PLOT statement.
;
;	DEVCODE - A string array of hardcopy device code names used in
;		  the SET_PLOT statement, e.g ['PS', 'PCL', 'HP']
;		  This array must mirror DEVLIST.
;
;	FONT_LARGE - The name of a larger font e.g 'courier_bold14'
;
;	FONT_SMALL - The name of a smaller font e.g 'courier_bold12'
;
;	EDIT_FONTS - A structure of two fonts used in the table editor
;		     adas_edtab.pro, {font_norm:'',font_input:''}.
;		     The two fonts are used to differentiate between
;		     editable and non-editable parts of the table. You
;		     may have to experiment to get two fonts which give
;		     the same default spacing in the edit widget.
;		     e.g {font_norm:'helvetica_bold14', $
;			  font_input:'helvetica_oblique14'}
;
; OPTIONAL INPUTS:
;	None
;
; OUTPUTS:
;	None - Note this routine should leave all inputs un-modified.
;
; OPTIONAL OUTPUTS:
;	None
;
; KEYWORD PARAMETERS:
;	None
;
; CALLS:
;	FIND_PROCESS	Checks to see if a given process is active.
;	F3SPF0		Top level IDL interface routine for input data selection.
;	F3ISPF		Top level IDL interface routine for processing options.
;	BXDATE		Get date and time from operating system.
;	F3SPF1		Top level IDL interface routine for output options.
;	PLOT603		Graph plotting routine.
;	F3OUT1		Text output.
;
; SIDE EFFECTS:
;	This routine spawns a FORTRAN executable.  Note the pipe 
;	communications routines listed above.  In addition to these
;	pipe communications there is one explicit communication of the
;	date to the FORTRAN code, search for 'printf,pipe' to find it.
;
; CATEGORY:
;	Adas system.
;	
; WRITTEN:
;       Richard Martin, University of Strathclyde, 4-Dec-1998
;	  Based on adas602.pro.
;
; MODIFIED:
;	1.1   Richard Martin
;		Put under SCCS control  
;	1.2	Richard Martin
;		Increased version no. to 1.2  
;	1.3	Richard Martin
;		Increased version no. to 1.3   
;	1.4	Richard Martin
;		Increased version no. to 1.4       
;	1.5	Richard Martin
;		Increased version no. to 1.5   
;	1.6	Richard Martin
;		Increased version no. to 1.6  
;	1.7	Richard Martin
;		Increased version no. to 1.7 
;	1.8	Richard Martin
;		Increased version no. to 1.8 
;
; VERSION:
;	1.1	04-12-98
;	1.2	19-03-99
;	1.3	28-04-99
;	1.4	13-10-99
;	1.5	15-09-2000
;	1.6	14-03-2001
;	1.7	18-03-02
;	1.8	16-07-02
;-
;-----------------------------------------------------------------------------


PRO ADAS603,	adasrel, fortdir, userroot, centroot, 			$
		devlist, devcode, font_large, font_small, edit_fonts

		;************************
		;**** Initialisation ****
		;************************

  adasprog = ' PROGRAM: ADAS603 V1.8'
  lpend = 0
  gomenu = 0
  deffile = userroot+'/defaults/adas603_defaults.dat'
  bitfile = centroot+'/bitmaps'
  device = ''

		;******************************************
		;**** Search for user default settings ****
		;**** If not found create defaults     ****
		;******************************************

  files = findfile(deffile)
  if files(0) eq deffile then begin

    restore,deffile
    inval.centroot1 = centroot+'/arch603/'
    inval.userroot1 = userroot+'/arch603/'
    inval.centroot2 = centroot+'/pass/'
    inval.userroot2 = userroot+'/pass/'
    inval.adasrel   = adasrel
    inval.fortdir   = fortdir
    inval.userroot  = userroot
    inval.centroot  = centroot
    inval.devlist   = devlist
    inval.devcode   = devcode
    inval.fontl     = font_large
    inval.fonts     = font_small
    inval.dflag     = 0          
    tfont_struct    = { ef,                                             $
                        font_norm:edit_fonts.font_norm,                 $
                        font_input:edit_fonts.font_input}
    inval.efont     = tfont_struct

  end else begin

    tfont_struct    = { ef,                                             $
                        font_norm:edit_fonts.font_norm,                 $
                        font_input:edit_fonts.font_input}
    inval = {                                                           $
		ROOTPATH1:userroot+'/arch603/',                         $
		FILE1:'',                                               $
		ROOTPATH2:userroot+'/pass/',                            $
		FILE2:'',                                               $
		CENTROOT1:centroot+'/arch603/',                         $
		USERROOT1:userroot+'/arch603/',                         $
                CENTROOT2:centroot+'/pass/',                            $
                USERROOT2:userroot+'/pass/',                            $
                ADASREL:adasrel,                                        $
                FORTDIR:fortdir,                                        $
                USERROOT:userroot,                                      $
                CENTROOT:centroot,                                      $
                DEVLIST:devlist,                                        $
                DEVCODE:devcode,                                        $
                FONTL:font_large,                                       $
                FONTS:font_small,                                       $
                EFONT:tfont_struct,                                     $
                EXPSEL:0,                                               $
                DFLAG:0	                                                $
             }

    procval = {NEW:-1}
    outval = {          GRPOUT   :      0,                              $
                        GTIT1    :      '',                             $
                        GRPSCAL  :      -1,                             $
                        XMIN     :      '',                             $
                        XMAX     :      '',                             $
                        YMIN     :      '',                             $
                        YMAX     :      '',                             $
                        HRDOUT   :      1,                              $
                        HARDNAME :      '',                             $
                        GRPDEF   :      '',                             $
                        GRPFMESS :      '',                             $
                        GRPSEL   :      -1,                             $
                        GRPRMESS :      '',                             $
                        DEVSEL   :      -1,                             $
                        GRSELMESS:      '',                             $
                        TEXOUT   :      0,                              $
                        TEXAPP   :      -1,                             $
                        TEXREP   :      0,                              $
                        TEXDSN   :      '',                             $
                        TEXDEF   :      'paper.txt',                    $
                        TEXMES   :      '',                             $
                        SPSOUT   :      0,                              $
                        SPSAPP   :      -1,                             $
                        SPSREP   :      0,                              $
                        SPSDSN   :      '',                             $
                        SPSDEF   :      userroot+'/pass/adas603.pass',  $
                        SPSMES   :      ''                              }

  end

		;******************
		;**** Get date ****
		;******************
  date = bxdate()

		;**** Get user-id 
  userid = '          '
  uid = getenv('USER')
  strput,userid,strmid(uid,0,10)

LABEL100:
		;*********************************************************
                ;**** Invoke user interface widget for file selection ****
		;*********************************************************

  f3spf0, inval, dsninc, dsninx, rep, FONT=font_large

		;**** If cancel selected then end program ****
  if rep eq 'YES' then goto, LABELEND

		;**********************************
		;**** Options for data Read in ****
		;**********************************

  header=''
  proc_header=''
  spectrum=-1
  oldstr=0
  lstr=0 

  if inval.dflag eq 0 then begin

                ;**** Standard Data, is it IDL or ascii? ****

                ;**** IDL restored at main level ****

      restore,strtrim(dsninc,2)
                ;**** Standard data, spectrum, header ****
      chkspect=size(spectrum)
      if chkspect(0) eq 0 then begin
        action=popup(message='DATA IS NOT OF STANDARD FORM', $
                     buttons=[' CANCEL '], font=font_large)
        goto, LABEL100
      end
                ;**** Check on dimensionality and collapse array ****
     
      if chkspect(0) gt 2 then begin 
        if chkspect(0) gt 6 then begin
          action=popup(message='DIMENSIONS > 6 NOT YET SUPPORTED', $
                       buttons=[' CANCEL '], font=font_large)
          goto, LABEL100
        end
        sum = 1.
        for ijk = 0, chkspect(0)-2 do begin
          sum = chkspect(ijk+2) * sum
        end
        spectrum = reform(spectrum, chkspect(1), sum)
      end

                ;**** Is there a template? ****

      if strtrim(dsninx,2) ne '' then begin
        restore,strtrim(dsninx,2)   ;**** lstr - structure of fits ****
        proc_header=lstr.header
        oldstr = lstr    ;**** Only set oldstr if data template exists,
                         ;**** not used if data is processed as unnecessary
                         ;**** to re-process same data.
      end

  end else if inval.dflag eq 1 then begin

                ;**** IDL restored at main level ****

      restore,strtrim(dsninc,2)

                ;**** Processed Data ****
      chklstr=size(lstr)
      if chklstr(chklstr(0)+1) ne 8 then begin
        action=popup(message='DATA IS NOT OF PROCESSED FORM', $
                     buttons=[' CANCEL '], font=font_large)
        goto, LABEL100
      end
      
      restore,strtrim(dsninc,2)
      proc_header=lstr.header
      proc_lstr=lstr     ;**** copy structure

                ;**** Is there a template? ****

      if strtrim(dsninx,2) ne '' then begin
        restore,strtrim(dsninx,2)  
        proc_header=lstr.header
        oldstr = lstr    ;**** This option only to allow viewing of 
                         ;**** previous fit.             
      end

  end else if inval.dflag eq 2 then begin

                ;**** Other data here ****

  end


LABEL200:

		;**** Create header for output. ****
  adasheader = adasrel+adasprog+' DATE: '+date(0)+' TIME: '+date(1)

		;************************************************
		;**** Setup data variables structure and     ****
		;**** invoke user interface widget for       ****
		;**** Processing options                     ****
		;************************************************

  f3ispf, lpend, procval, header, proc_header, adasheader, spectrum,    $
	  proc_lstr, gomenu, bitfile, inval, oldstr, dsninc, dsninx,    $ 
           FONT_LARGE=font_large, 		         	$
          FONT_SMALL=font_small, EDIT_FONTS=edit_fonts

                ;**** If menu button clicked, terminate ****

  if gomenu eq 1 then begin
     goto, LABELEND
  end

		;**** If cancel selected then goto 100 ****
  if lpend eq 1 then goto, LABEL100

LABEL300:


		;************************************************
		;**** Invoke user interface widget for       ****
		;**** Output options                         ****
		;************************************************

  f3spf1, lpend, inval, outval, dsninc, bitfile, gomenu, $
          DEVLIST=devlist, FONT=font_large


                ;**** If menu button clicked, terminate ****

  if gomenu eq 1 then begin
     goto, LABELEND
  end

		;**** If cancel selected then erase output ****
		;**** messages and goto 200.		   ****
  if lpend eq 1 then begin
    outval.texmes = ''
    outval.spsmes = ''
    goto, LABEL200
  end

                ;**** temporary restore of passing file ****
  restore,'temporary

                ;**** if passing file requested make it ****
  if outval.spsout eq 1 then begin

                ;**** Check on dimensionality, reform structure ****
                ;**** and create fitted spectrum array          ****
 
    if inval.dflag eq 0 then begin
    
      if chkspect(0) gt 2 then begin 
        new_dimns = chkspect(2:chkspect(0))
        dimn1 = (lstr(0).xmax-lstr(0).xmin)+1
        lstr = reform(lstr, new_dimns) 
        new_dimns = [dimn1, new_dimns]
        fspectrum = make_array(/double, dimension = new_dimns)
        if chkspect(0) eq 3 then fspectrum(0:dimn1-1,*,*) = lstr(*,*).ya
        if chkspect(0) eq 4 then fspectrum(0:dimn1-1,*,*,*) = lstr(*,*,*).ya
        if chkspect(0) eq 5 then fspectrum(0:dimn1-1,*,*,*,*) = lstr(*,*,*,*).ya
        if chkspect(0) eq 6 then fspectrum(0:dimn1-1,*,*,*,*,*) = lstr(*,*,*,*,*).ya

        save, filename = outval.spsdsn, lstr, fspectrum

      end else begin

        save, filename = outval.spsdsn, lstr

      end

    end else begin

        save, filename = outval.spsdsn, lstr

    end

  end

         	;**** Hardcopy output of graph ****
  if outval.grpout eq 1 then begin
    if n_elements(lstr) ne 1 then begin
      plotstruct = lstr(0)
    end else plotstruct = lstr
    plot603, procval, outval, plotstruct, header, devcode
  end

	        ;**** Text output ****
  if outval.texout eq 1 then begin
;    header1=header
;    header2=proc_header
;	save,filename='f3out.dat',inval, procval, outval, lstr, adasheader, header1, header2, $
;            date, dsninc, dsninx
    f3out1, inval, procval, outval, lstr, adasheader, header, proc_header, $
            date, dsninc, dsninx
  end

GOTO, LABEL300

LABELEND:
                
                ;**** remove temporary file ****
  spawn,'rm -f temporary'
  

		;**** Ensure appending is not enabled for ****
		;**** text output at start of next run.   ****
outval.texapp = -1

		;**** Save user defaults ****
save,inval,procval,outval,filename=deffile

END
