; Copyright (c) 1998 Strathclyde University .
; SCCS info: Module @(#)$Header: /home/adascvs/idl/adas6xx/adas603/f3spf1.pro,v 1.1 2004/07/06 13:51:54 whitefor Exp $ Date $Date: 2004/07/06 13:51:54 $
;
;+
; PROJECT:
;       ADAS IBM MVS to UNIX conversion
;
; NAME:
;	F3SPF1
;
; PURPOSE:
;	IDL user interface for output options.
;
; EXPLANATION:
;	This routine invokes the 'Output Options' part of the IDL user
;	interface.
;
; USE:
;	The use of this routine is specific to ADAS603, See adas603.pro.
;
; INPUTS:
;
;	DSFULL  - The name of the data set being analyzed.
;
;	INVAL	- A structure which determines the initial settings of
;		  the dataset selection widget.  The initial value is
;		  set in adas603.pro.  INVAL is passed un-modified
;		  through to adas603_out.pro.
;
;       BITFILE - String; the path to the directory containing bitmaps
;                 for the 'escape to series menu' button.
;
; OPTIONAL INPUTS:
;	None.
;
; OUTPUTS:
;	LPEND	- Integer; indicates user action on leaving output
;		  selections.  lpend is set to 1 if the user exited
;		  with the 'Cancel' button and 0 if the user exited
;		  with 'Done'.
;
;	VALUE	- On output the structure records the final settings of
;		  the output selection widget if the user pressed the
;		  'Done' button otherwise it is not changed from input.
;
;       GOMENU  - Int; flag - set to 1 if user has selected 'escape direct
;                 to series menu , otherwise 0
;
; OPTIONAL OUTPUTS:
;	None.
;
; KEYWORD PARAMETERS:
;	DEVLIST	- A string array;  Each element of the array is the
;		  name of a hardcopy graphics output device.  This
;		  list must mirror the DEVCODE list.  DEVLIST is the
;		  list which the user will see.
;		  e.g ['Post-Script','HP-PCL','HP-GL']
;
;	FONT	- Supplies the font to be used for the interface widgets.
;
; CALLS:
;	ADAS603_OUT	User interface - output options.
;
; SIDE EFFECTS:
;	None.
;
; CATEGORY:
;	Adas system.
;
; WRITTEN:
;       Richard Martin, University of Strathclyde, 4-Dec-1998
;	  Based on f3spf1.pro.
;
; MODIFIED:
;	1.1   Richard Martin
;		Put under SCCS control                    
;
; VERSION:
;	1.1	04-12-98
;
;-
;-----------------------------------------------------------------------------

PRO f3spf1, lpend, inval, value, dsfull, bitfile, gomenu,        $
            DEVLIST=devlist, FONT=font

                ;**** Set defaults for keywords ****

    IF NOT (KEYWORD_SET(font)) THEN font = ''

		;**************************************
		;**** Pop-up output options widget ****
		;**************************************

    adas603_out, value, inval, dsfull, action, bitfile, $
                 DEVLIST=devlist, FONT=font

		;********************************************
		;**** Act on the output from the widget   ****
		;**** There are only two possible actions ****
                ;**** There are three    possible actions ****
                ;**** 'Done', 'Cancel' and 'Menu.         ****
                ;*********************************************

    if action eq 'Done' then begin
        lpend = 0
    endif else if action eq 'Menu' then begin
        lpend = 0
        gomenu = 1
    endif else begin
        lpend = 1
    endelse

		;**************************************************
		;**** Set messages and settings ready for when ****
		;**** output window is re-displayed.           ****
		;**************************************************

    if lpend eq 0 then begin
        if value.texout eq 1 then begin
            value.texdef = value.texdsn
            if value.texrep ge 0 then value.texrep = 0
            value.texmes = 'Output written to file.'
        endif
                ;**** same for passing file ****
        if value.spsout eq 1 then begin
            value.spsdef = value.spsdsn
            if value.spsrep ge 0 then value.spsrep = 0
            value.spsmes = 'Output written to file.'
        end
        value.grprmess = ' '
        value.grpfmess = ' '
        value.grselmess = ' '
        value.spsmes = ' '
    endif

END
