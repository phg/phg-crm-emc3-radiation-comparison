; (c) 1995, Strathclyde University .
; SCCS info: Module @(#)$Header: /home/adascvs/idl/adas5xx/adas509/adas509_proc.pro,v 1.2 2004/07/06 11:01:00 whitefor Exp $ Date $Date: 2004/07/06 11:01:00 $
;
;+
; PROJECT:
;       ADAS IBM MVS to UNIX conversion
;
; NAME:
;	ADAS509_PROC
;
; PURPOSE:
;	IDL ADAS user interface, processing options/input.
;
; EXPLANATION:
;	This routine creates and manages a pop-up window which allows
;	the user to select options and input data to control ADAS509
;	processing.
;
; USE:
;	This routine is ADAS509 specific, see e9ispf.pro for how it
;	is used.
;
; INPUTS:
;	PROCVAL	- A structure which determines the initial settings of
;		  the processing options widget.  The value is passed
;		  unmodified into cw_adas509_proc.pro.
;
;		  See cw_adas509_proc.pro for a full description of this
;		  structure.
;
;	DSFULL	- String; The full system file name of the input 
;		          dataset selected by the user for processing.
;
;	NSTORE  - Integer: maximum number of data blocks which can be
;			   read from the data set.
;
;	NEDIM	- Integer : Maximum number of energies allowed.
;
;	NDEIN	- Integer : Maximum number of user input energies allowed.
;
;	NDTIN	- Integer : Maximum number of user input temps. allowed.
;
;	NBSEL   - Integer : number of data blocks accepted and read-in
;
;	CDONOR  - String array : Donor symbol for each data blocks
;				 dimension data block index (NSTORE).
;	CRECVR  - String array : Receiver symbol for each data blocks.
;				 dimension data block index (NSTORE).
;	CFSTAT  - String array : Final state for each data block 
;				 dimension data block index (NSTORE) 
;	CTYPE   - String array : DATA type  for each data block
;				 dimension data block index (NSTORE)
;	IEA     - Integer array: Number of energies
;				 dimension data block index (NSTORE)
;	EVALS   - Double array : Energies - 
;				 1st dimension - energy index
;				 2nd dimension - units 1 - cm s-1
;						       2 - at. units
;						       3 - eV/amu
;				 3rd dimension - data block index
;
;       BITFILE - String; the path to the directory containing bitmaps
;                 for the 'escape to series menu' button.
;
; OPTIONAL INPUTS:
;	None
;
; OUTPUTS:
;	PROCVAL	- On output the structure records the final settings of
;		  the processing selections widget if the user pressed the
;		  'Done' button, otherwise it is not changed from input.
;
;	ACT	- String; 'Done', 'Cancel' or 'Menu' for the button the
;		  user pressed to terminate the processing options
;		  window.
;
; OPTIONAL OUTPUTS:
;	None
;
; KEYWORD PARAMETERS:
;	FONT_LARGE - The name of a larger font.
;
;	FONT_SMALL - The name of a smaller font.
;
;	EDIT_FONTS - A structure of two fonts used in the table editor
;		     adas_edtab.pro, {font_norm:'',font_input:''}.
;
; CALLS:
;	CW_ADAS503_PROC	Declares the processing options widget.
;	DYNLABEL	Recursively adds dynamic_resize keyword to label
;			widgets for version 4.0.1 and higher of IDL
;	ADAS503_PROC_EV Called indirectly during widget management,
;			routine included in this file.
;	XMANAGER
;
; SIDE EFFECTS:
;       This widget uses a common block PROC503_BLK, to pass the
;       variables VALUE and ACTION between the two routines. 
;
; CATEGORY:
;	Widgets
;
; WRITTEN:
;       H. P. Summers, University of Strathclyde, 08-Mar-1996
;
; MODIFIED:
;       1.1     Hugh Summers
;               First Release
;	1.2	William Osborn
;		Added dynlabel procedure
;
; VERSION:
;	1.1	08-03-96
;	1.2	William Osborn
;		Added dynlabel procedure
;
;-----------------------------------------------------------------------------

PRO adas509_proc_ev, event

    COMMON proc509_blk, action, value

    action = event.action
    CASE event.action OF

		;**** 'Done' button ****

	'Done'  : begin

		;**** Get the output widget value ****

	    widget_control, event.id, get_value=value 
            widget_control, event.top, /destroy
	end

		;**** 'Cancel' button ****

	'Cancel': widget_control, event.top, /destroy

                ;**** 'Menu' button ****

        'Menu': widget_control, event.top, /destroy

        ELSE:                   ;**** Do nothing ****

    ENDCASE

END

;-----------------------------------------------------------------------------

PRO adas509_proc, procval, dsfull, nstore, nedim, ndein, ndtin, nbsel, 	$
		  cdonor, crecvr, cfstat, ctype, iea, evals, 		$
		  act, bitfile, FONT_LARGE=font_large, 			$
                  FONT_SMALL=font_small, EDIT_FONTS=edit_fonts


		;**** declare common variables ****

    COMMON proc509_blk, action, value

		;**** Copy "procval" to common ****

    value = procval

		;**** Set defaults for keywords ****

    IF NOT (KEYWORD_SET(font_large)) THEN font_large = ''
    IF NOT (KEYWORD_SET(font_small)) THEN font_small = ''
    IF NOT (KEYWORD_SET(edit_fonts)) THEN 				$
    edit_fonts = { font_norm:'', font_input:''}
		
                ;**** create base widget ****

    procid = widget_base(TITLE='ADAS509 PROCESSING OPTIONS', 		$
			 XOFFSET=50, YOFFSET=1)

		;**** Declare processing widget ****

    cwid = cw_adas509_proc(procid, dsfull, act, nstore,			$
			   nedim,  ndein, ndtin, nbsel, 		$
			   cdonor, crecvr, cfstat, ctype, 		$
			   iea, evals, 					$
			   bitfile, PROCVAL=value, UVALUE=uvalue,	$
		 	   FONT_LARGE=font_large, FONT_SMALL=font_small,$
			   EDIT_FONTS=edit_fonts)

		;**** Realize the new widget ****

    dynlabel, procid
    widget_control, procid, /realize

		;**** make widget modal ****

    xmanager, 'adas509_proc', procid, event_handler='adas509_proc_ev', 	$
	      /modal, /just_reg

		;*** copy value back to procval for return to e9ispf ***

    act = action
    procval = value
 
END

