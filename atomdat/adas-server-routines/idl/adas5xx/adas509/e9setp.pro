; (c) 1995, Strathclyde University .
; SCCS info: Module @(#)$Header: /home/adascvs/idl/adas5xx/adas509/e9setp.pro,v 1.1 2004/07/06 13:47:04 whitefor Exp $ Date $Date: 2004/07/06 13:47:04 $
;
; PROJECT:
;       ADAS IBM MVS to UNIX conversion
;
; NAME:
;	E9SETP
;
; PURPOSE:
;	IDL communications with ADAS509 FORTRAN process via pipe.
;
; EXPLANATION:
;	This IDL routine reads data from the ADAS509 FORTRAN subroutine
;	E9SETP via a UNIX pipe.  In the original IBM version the FORTRAN
;	subroutine E9SETP put information needed by the ISPF interface
;	into the ISPF pool area for later use.  Now the same information
;	is written to IDL for use in its user unterface.
;
; USE:
;	The use of this routine is specific to ADAS509.  See
;	adas509.pro for an example.
;
; INPUTS:
;	PIPE	- The IDL unit number of the bi-directional pipe to the
;		  ADAS509 FORTRAN process.
;
; OPTIONAL INPUTS:
;	None
;
; OUTPUTS:
;       NBLKS        - Number of data sub-blocks read in.
;
; OPTIONAL OUTPUTS:
;	None
;
; KEYWORD PARAMETERS:
;	None
;
; CALLS:
;	None
;
; SIDE EFFECTS:
;	Reads data from a pipe.
;
; CATEGORY:
;	Adas system.
;
; WRITTEN:
;       Hugh Summers, University of Strathclyde, 06/03/1996
;
; MODIFIED:
;	1.1		Hugh Summers
;			First version
;
; VERSION:
;	1.1		06-03-96
;
;-----------------------------------------------------------------------------

PRO e9setp, pipe, nbsel

		;**********************************
		;**** Initialise new variables ****
		;**********************************

    idum  = 0

		;********************************
		;**** Read data from fortran ****
		;********************************

    readf, pipe, idum
    nbsel = idum

END
