; (c) 1995, Strathclyde University .
; SCCS info: Module @(#)$Header: /home/adascvs/idl/adas5xx/adas509/e9outg.pro,v 1.1 2004/07/06 13:46:58 whitefor Exp $ Date $Date: 2004/07/06 13:46:58 $
;
;+
; PROJECT:
;       ADAS IBM MVS to UNIX conversion
;
; NAME:
;	E9OUTG
;
; PURPOSE:
;	Communication with ADAS509 FORTRAN via UNIX pipe and
;	graphics output.
;
; EXPLANATION:
;	The routine begins by reading information from the ADAS509
;	FORTRAN process via a UNIX pipe.  Then the IDL graphical
;	output routine for ADAS509 is invoked.  Communications are to
;	the FORTRAN subroutine E9OUTG.
;
; USE:
;	The use of this routine is specific to ADAS509 see adas509.pro.
;
; INPUTS:
;	DSFULL   - Data file name
;	PIPE	 - The IDL unit number of the bi-directional pipe to the
;		   ADAS509 FORTRAN process.
;
;	UTITLE   - Optional comment by user
;
;	GRPSCAL	 - Integer; 0 if default scaling required 1 if user
;		   specified scaling to be used.
;
;	XMIN	 - String; User specified x-axis minimum, number as string.
;
;	XMAX	 - String; User specified x-axis maximum, number as string.
;
;	YMIN	 - String; User specified y-axis minimum, number as string.
;
;	YMAX	 - String; User specified y-axis maximum, number as string.
;
;	HRDOUT	 - Integer; 0 if no hardcopy required, 1 if hardcopy.
;
;	HARDNAME - String; file name for hardcopy output.
;
;	DEVICE	 - String; IDL name for hardcopy output device.
;
;	HEADER	 - ADAS version header information for inclusion in the
;		   graphical output.
;
; OPTIONAL INPUTS:
;	None.
;
; OUTPUTS:
;	None.
;
; OPTIONAL OUTPUTS:
;	None.
;
; KEYWORD PARAMETERS:
;	FONT	- String; Name of the font to be used for the graphical
;		  output widget.
;
; CALLS:
;	ADAS509_PLOT	ADAS504 graphical output.
;
; SIDE EFFECTS:
;	This routine reads information from FORTRAN via a UNIX pipe.
;
; CATEGORY:
;	Adas system.
;
; WRITTEN:
;       Hugh Summers, University of Strathclyde,  11/03/1996
;
; MODIFIED:
;	1.1	Hugh Summers
;               First edition				
;
; VERSION:
;	1.1	11/03/1995
;

;-----------------------------------------------------------------------------

PRO e9outg, dsfull, pipe, utitle, grpscal, xmin, xmax, ymin, ymax,     $
	    hrdout, hardname, device, header, bitfile, gomenu, FONT=font

		;***********************************
                ;**** Set defaults for keywords ****
		;***********************************

    IF NOT (KEYWORD_SET(font)) THEN font = ''

		;*************************************
		;**** Declare variables for input ****
		;*************************************

    sdum = " "
    idum = 0
    fdum = 0.0
    strg = make_array(9, /string, value=" ")
    nmx = 0

		;********************************
		;**** Read data from fortran ****
		;********************************

    readf, pipe, format = '(a40)' , sdum
    title = sdum
    readf, pipe, format = '(a120)', sdum
    titlx = sdum
    readf, pipe, format = '(a80)' , sdum
    titlm = sdum
    readf, pipe, format = '(a8)' , sdum
    date = sdum
    readf, pipe, format = '(a2)' , sdum
    esym = sdum
    readf, pipe, idum
    iz0 = idum
    readf, pipe, idum
    iz = idum
    readf, pipe, format = '(a9)' , sdum
    cdonor = sdum
    readf, pipe, format = '(a9)' , sdum
    crecvr = sdum
    readf, pipe, format = '(a12)' , sdum
    cfstat = sdum
    readf, pipe, format = '(a2)' , sdum
    ctype = sdum
    readf, pipe, idum
    ieval = idum

		;**************************************
		;**** now declare array dimensions ****
		;**************************************

    eeva = dblarr(ieval) 
    scxa = dblarr(ieval) 
    for i=0, ieval-1 do begin
        readf, pipe, fdum
        eeva(i) = fdum 
    endfor
    for i=0, ieval-1 do begin
        readf, pipe, fdum
        scxa(i) = fdum 
    endfor
    readf, pipe, idum
    ldef1 = idum
    readf, pipe, idum
    lfsel = idum
    if (lfsel eq 1) then begin
        readf, pipe, idum
        nmx = idum
        scxm = fltarr(nmx) 		;**** declare arrays ****
        efitm = fltarr(nmx)
        for i = 0, nmx-1 do begin
	    readf, pipe, fdum
            scxm(i) = fdum 
        endfor
        for i = 0, nmx-1 do begin
	    readf, pipe, fdum 
            efitm(i) = fdum
        endfor
    endif
    for i = 0, 5 do begin
        readf, pipe, sdum 
        strg(i) = sdum
    endfor
    readf, pipe, sdum, format = '(a32)'
    head1 = sdum
    readf, pipe, sdum, format = '(a16)'
    head2 = sdum
    readf, pipe, sdum, format = '(a16)'
    head3 = sdum

		;***********************
		;**** Plot the data ****
		;***********************

    adas509_plot, dsfull, 						$
  		  title , titlx, titlm , utitle,date, esym, iz0, iz, 	$
  		  cdonor, crecvr, cfstat, ctype, ieval,              	$
		  eeva  , scxa , scxm  , efitm,                       	$
                  ldef1 , xmin , xmax  , ymin , ymax,                 	$
   		  lfsel , nmx  ,                                      	$
   		  strg  , head1, head2, head3,                        	$
		  hrdout, hardname, device, header, bitfile, gomenu,  	$
                  FONT=font

END
