; (c) 1995, Strathclyde University .
; SCCS info: Module @(#)$Header: /home/adascvs/idl/adas5xx/adas509/cw_adas509_proc.pro,v 1.5 2004/07/06 12:53:44 whitefor Exp $ Date $Date: 2004/07/06 12:53:44 $
;
;+
; PROJECT:
;       ADAS IBM MVS to UNIX conversion
;
; NAME:
;	CW_ADAS509_PROC()
;
; PURPOSE:
;	Produces a widget for ADAS509 processing options/input.
;
; EXPLANATION:
;	This function creates a compound widget consisting of :-
;	   a text widget holding an edieable 'Run title', 
;	   the dataset name/browse widget ,
;	   a widget to select the transition for analysis,
;	   a widget to request a mimax fit and enter a tolerance for it,
;	   a table widget for temperature data ,
;	   a button to enter default values into the table, 
;	   a message widget, 
;	   an 'Escape to series menu', a 'Cancel' and a 'Done' button.
;
;	The compound widgets included in this widget are self managing.
;	This widget only manages events from the 'Defaults' button,
;	the 'Cancel' button and the 'Done' button and the 
;       'escape to series menu' button.
;
;
; USE:
;	This widget is specific to ADAS509, see adas509_proc.pro
;	for use.
;
; INPUTS:
;	TOPPARENT- Long integer, ID of parent widget.
;
;	DSFULL	- String; The full system file name of the input 
;		          dataset selected by the user for processing.
;
;	NSTORE	- Integer : Maximum number of data blocks which can be
;                           read from the data set.
;
;	NEDIM   - Integer : Maximum number of energies from data file.
;
;	NDEIN	- Integer : Maximum number of user energies allowed.
;
;	NBSEL	- Integer : Number of data blocks accepted and read in
;
;	CDONOR	- String array : Donor symbol for each block
;               
;	CRECVR	- String array : Receiver symbol for each block
;
;	CFSTAT	- String array : Final state for each block
;
;	CTYPE	- String array : Data type for each block
;               
;	IEA	- Integer array: Number of energies for each block
;
;	EVALS	- Double array : Electron energies for each block
;				 1st dimension - energy index
;				 2nd dimension - units 1 => cm s-1
;						       2 => at. units
;						       3 => eV/amu
;
;       BITFILE - String; the path to the directory containing bitmaps
;                 for the 'escape to series menu' button.
;	
;
; OPTIONAL INPUTS:
;	None
;
; OUTPUTS:
;	The return value of this function is the ID of the declared widget.
;
;	ACT 	- String; result of this widget, 'done' or 'cancel'
;
; OPTIONAL OUTPUTS:
;	None
;
; KEYWORD PARAMETERS:
;	PROCVAL	- A structure which determines the initial settings of
;		  the processing options widget.
;
;		  The default PROCVAL is created thus;
;
;   		 ps = {proc509_set, 			$
;				new   	: 	0, 			$
;				title 	: 	'',             	$
;				ibsel 	: 	0,			$
;				amd 	: 	2.0,			$
;				amr 	: 	2.0,			$
;				ieval 	: 	ndein,			$
;				ietyp 	: 	1,			$
;		                ein  	:	ener_arr,		$
;				itval 	: 	ndtin,			$
;				ittyp 	: 	3,			$
;		                tin  	:	temp_arr,		$
;		                lfsel 	:	0,              	$
;		                tolval	: 	'5',			$
;				losel 	:	0,			$
;				ltsel 	:	0			}
;
;		NEW     Flag which defines whether or not default values
;			exist or not. (< 0 if no values exist)
; 		TITLE	Entered general title for program run
;		IBSEL   Selected data block in source file
;		AMD     Donor atomic mass number
;		AMR     Receiver atomic mass number
;		IEVAL   Number of user energy values entered
;		IETYP   Index indicating which energy units are being used
;		EIN     User supplied energy values for fit.
;		ITVAL   Number of user temperature values entered
;		ITTYP   Index indicating which temp. units are being used
;		TIN     User supplied temperature values for fit.
;		LFSEL   Flag as to whether polynomial fit is chosen
;		TOLVAL  Tolerance required for goodness of fit if
;			polynomial fit is selected.
;		LOSEL   Flag whether energies for text output have been set
;		LTSEL	Flag whether temperatures for TCX file have been set
;
;		All of these structure elements map onto variables of
;		the same name in the ADAS509 FORTRAN program.
;
;
;	UVALUE	- A user value for the widget. Default 0.
;
;	FONT_LARGE - The name of a larger font.  Default current system
;		     font
;
;	FONT_SMALL - The name of a smaller font. Default current system
;		     font.
;
;	EDIT_FONTS - A structure of two fonts used in the table editor
;		     adas_edtab.pro, {font_norm:'',font_input:''}.  Defaults
;		     to current system font.
;
;	NUM_FORM   - String; Numeric format to use in tables.  Default
;			'(E10.3)'
;		     
;
; CALLS:
;	POPUP		Popup warning window with buttons.
;	CW_ADAS_DSBR	Dataset name and comments browsing button.
;	CW_ADAS_TABLE	Adas data table widget.
;	CW_ADAS_SEL	Adas multiple selection widget.
;	CW_SINGLE_SEL	Adas scrolling table and selection widget.
;	CW_OPT_VALUE    Adas option selection widget with required input 
;			value. 
;
;	See side effects for other related widget management roueins.
;
; SIDE EFFECTS:
;
;	The following widget management roueins are included in this file;
;	PROC509_GET_VAL()	Returns the current PROCVAL structure.
;	PROC509_EVENT()		Process and issue events.
;	
; CATEGORY:
;	Compound Widget
;
; WRITTEN:
;       Hugh Summers, University of Strathclyde, 08/03/1996 
;
; MODIFIED:
;	1.1	Hugh Summers
;		First version
;	1.2	William Osborn
;		Removed print statements in get_val routine
;	1.3	William Osborn
;		Trivial correction
;	1.4	Martin O'Mullane
;		Changed ps.ibsel to ibsel when assembling energies and 
;               temperatures for the widget display
;	1.5	Richard Martin
;		IDL 5.5 fixes.
;				
; VERSION:
;	1.1	20/12/95
;	1.2	30/04/96
;	1.3	30/04/96
;	1.4	30/10/97
;	1.5	28-01-02
;
;-----------------------------------------------------------------------------

FUNCTION proc509_get_val, id

COMMON adas509_proc_blk, nstore, nedim, ndein, ndtin, nbsel

                ;**** Return to caller on error ****

    ON_ERROR, 2

		 ;***************************************
                 ;****     Retrieve the state 	     ****
		 ;**** Get first_child widget id     ****
		 ;**** because state is stored there ****
		 ;***************************************

    first_child = widget_info(id, /child)
    widget_control, first_child, get_uvalue = state

		;***********************************
		;**** Get run title from widget ****
		;**** Then centre it in string  ****
		;**** of 40 characters 		****
		;***********************************

    widget_control, state.runid, get_value=title
    title = strcompress(title(0))
    title_len = strlen(title) 
    if (title_len gt 40 ) then begin 
	title = strmid(title,0,37)
        widget_control, state.messid, set_value="Title too long - truncated "
        widget_control, state.runid, set_value=title
	wait, 1
    endif
    pad = (40 - title_len)/2 
    spaces = '                                         '
    title(0) = strmid(spaces,0,(pad-1)) + title + strmid(spaces,0,(pad-1))
  
		;******************************************
		;**** Get reactant mass parameters     ****
		;******************************************

    widget_control, state.amdid, get_value=amdval
    if strcompress(amdval(0), /remove_all) eq '' then begin
  	amd =2.0
    endif else begin
	amd = float(amdval(0))
    endelse

    widget_control, state.amrid, get_value=amrval
    if strcompress(amrval(0), /remove_all) eq '' then begin
  	amr =2.0
    endif else begin
	amr = float(amrval(0))
    endelse


		;****************************************************
		;**** Get new energy data from table widget      ****
		;**** If losel is selected                       ****
		;****************************************************

    if (state.losel eq 1) then begin
        widget_control, state.enerid, get_value=enerval
        enerdata = enerval.value
        ietyp = enerval.units+1

		;**** Copy out energy values ****

        ein = dblarr(ndein)
        blanks = where(strtrim(enerdata(0,*),2) eq '')

                ;***********************************************
		;**** next line assumes that all blanks are ****
		;**** at the end of the columns             ****
                ;***********************************************

        if blanks(0) ge 0 then ieval=blanks(0) else ieval=ndein

                ;*************************************************
                ;**** Only perform following if there is 1 or ****
                ;**** more value present in the table         ****
                ;*************************************************
                                 
	if ieval ge 1 then begin
            ein(0:ieval-1) = double(enerdata(0,0:ieval-1))
	endif

		;**** Fill out the rest with zero ****

        if ieval lt ndein then begin
            ein(ieval:ndein-1) = double(0.0)
        endif
    endif else begin	; *** use existing values
        ietyp = 1
	ein = dblarr(ndein)
	if ndein gt nedim then begin
            ein(0:nedim-1) = state.evals(*,0,0)
	    ein(nedim:ndein-1) = double(0.0)
        endif else begin
	    ein = state.evals(0:ndein-1,0,0)
;	    print,'cw_adas509_proc: truncating energy list to length',ndein
	endelse
	ieval = state.neval
    endelse
   
		;****************************************************
		;**** Get new temperature data from table widget ****
		;**** If ltsel is selected                       ****
		;****************************************************

    if (state.ltsel eq 1) then begin
        widget_control, state.tempid, get_value=tempval
        tempdata = tempval.value
        ittyp = tempval.units+1

		;**** Copy out temperature values ****

        tin = dblarr(ndtin)
        blanks = where(strtrim(tempdata(0,*),2) eq '')

                ;***********************************************
		;**** next line assumes that all blanks are ****
		;**** at the end of the columns             ****
                ;***********************************************

        if blanks(0) ge 0 then itval=blanks(0) else itval=ndtin

                ;*************************************************
                ;**** Only perform following if there is 1 or ****
                ;**** more value present in the table         ****
                ;*************************************************
                                 
	if itval ge 1 then begin
            tin(0:itval-1) = double(tempdata(0,0:itval-1))
	endif

		;**** Fill out the rest with zero ****

        if itval lt ndtin then begin
            tin(itval:ndtin-1) = double(0.0)
        endif
    endif else begin	; *** use existing values
        ittyp = 1
	tin = dblarr(ndtin)
	if ndtin ge nedim then begin
            tin(0:nedim-1) = state.tvals(*,0,0)
	    tin(nedim:ndtin-1) = double(0.0)
        endif else begin
	    tin = state.tvals(0:ndtin-1,0,0)
;	    print,'cw_adas509_proc: truncating temperature list to length'$
;			,ndtin
	endelse
	itval = state.neval
    endelse

		;*************************************************
		;**** Get selection of polyfit from widget    ****
		;*************************************************

    widget_control, state.optid, get_uvalue=polyset, /no_copy
    lfsel = polyset.optionset.option[0]
    if (num_chk(polyset.optionset.value[0]) eq 0) then begin
        tolval = (polyset.optionset.value[0])
    endif else begin
        tolval = '-999'
    endelse
    widget_control, state.optid, set_uvalue=polyset, /no_copy

		;***********************************************
		;**** get new index for data block selected ****
		;***********************************************

    widget_control, state.indexid, get_value=select_block
    ibsel = select_block  
  
		;***********************************************
		;**** write selected values to PS structure ****
		;***********************************************

     ps = {	new   	:  	0, 					$
                title 	: 	title,          			$
		ibsel 	: 	ibsel,					$
		amd 	: 	amd,					$
		amr 	: 	amr,					$
		ieval 	: 	ieval,					$
		ietyp 	: 	ietyp,					$
                ein  	: 	ein,       				$
		itval 	: 	itval,					$
		ittyp 	: 	ittyp,					$
                tin  	: 	tin,       				$
                lfsel 	: 	lfsel,              			$
                tolval	: 	tolval,					$
		losel 	: 	state.losel,				$
		ltsel 	: 	state.ltsel				}
   
     widget_control, first_child, set_uvalue=state, /no_copy

     RETURN, ps

END

;-----------------------------------------------------------------------------

FUNCTION proc509_event, event

COMMON adas509_proc_blk, nstore, nedim, ndein, ndtin, nbsel

                ;**** Base ID of compound widget ****

    parent = event.handler

		;**********************************************
                ;**** Retrieve the user value state        ****
		;**** Get id of first_child widget because ****
		;**** user value "state" is stored there   ****
		;**********************************************

    first_child = widget_info(parent, /child)
    widget_control, first_child, get_uvalue=state, /no_copy

		;*********************************
		;**** Clear previous messages ****
		;*********************************

    widget_control, state.messid, set_value=' '

                ;************************
                ;**** Process events ****
                ;************************

    CASE event.id OF

		;*********************************
		;*** ener. selected for output ***
		;*********************************

        state.lebut : begin
	    if (state.losel eq 0) then state.losel = 1 else state.losel = 0 
	    widget_control, state.defeid, sensitive = state.losel
	    widget_control, state.enerid, sensitive = state.losel
        end

		;*********************************
		;*** temp. selected for output ***
		;*********************************

        state.ltbut : begin
	    if (state.ltsel eq 0) then state.ltsel = 1 else state.ltsel = 0 
	    widget_control, state.deftid, sensitive = state.ltsel
	    widget_control, state.tempid, sensitive = state.ltsel
        end

		;*************************************
		;****      Select index           ****
		;*************************************

        state.indexid: begin

		;**** Get current index select value ****

            widget_control, state.indexid, get_value=ibsel
            neval = state.iea(ibsel)
	    amd   = state.amd
	    amr   = state.amr

		;**** Get current energy table widget value ****

 	    widget_control, state.enerid, get_value=enerval

		;**** Copy new ibsel values into structure  ****
 
	    if (neval gt 0) then begin    
    	        enerval.value(1,0:neval-1 ) = 				$
	        strtrim(string(state.evals(0:neval-1,0,ibsel),   	$
	        format=state.num_form),2)
                enerval.value(2,0:neval-1) = 				$
                strtrim(string(state.evals(0:neval-1,1,ibsel),   	$
                format=state.num_form),2)
    	        enerval.value(3,0:neval-1) = 				$
                strtrim(string(state.evals(0:neval-1,2,ibsel), 		$
	        format=state.num_form),2)

		;**** fill rest of table with blanks ****

	        if (neval lt nedim ) then begin
    	            enerval.value(1,neval:nedim-1 ) = ' '
                    enerval.value(2,neval:nedim-1 ) = ' '
    	            enerval.value(3,neval:nedim-1 ) = ' '
	        endif

		;**** Copy new data to energy table widget ****
   
 	        widget_control, state.enerid, set_value=enerval
  	    endif

		;**** Get current temperature table widget value ****

 	    widget_control, state.tempid, get_value=tempval

		;**** Copy new ibsel values into structure  ****
 
	    if (neval gt 0) then begin    
    	        tempval.value(1,0:neval-1 ) = 				$
	        strtrim(string(amd*state.evals(0:neval-1,2,ibsel),   	$
	        format=state.num_form),2)
                tempval.value(2,0:neval-1) = 				$
                strtrim(string(amr*state.evals(0:neval-1,2,ibsel),   	$
                format=state.num_form),2)
    	        tempval.value(3,0:neval-1) = 				$
                strtrim(string(state.evals(0:neval-1,2,ibsel), 		$
	        format=state.num_form),2)

		;**** fill rest of table with blanks ****

	        if (neval lt ndtin ) then begin
    	            tempval.value(1,neval:ndtin-1 ) = ' '
                    tempval.value(2,neval:ndtin-1 ) = ' '
    	            tempval.value(3,neval:ndtin-1 ) = ' '
	        endif

		;**** Copy new data to table widget ****
   
 	        widget_control, state.tempid, set_value=tempval

  	    endif

	  	;**** reset state.ibsel             ****

	    state.ibsel = ibsel
        end

		;*************************************
		;****   select donor mass number  ****
		;*************************************

        state.amdid: begin
            widget_control, state.amdid, get_value=amdval
            if strcompress(amdval(0), /remove_all) eq '' then begin
  	        amd =2.0
            endif else begin
	        amd = float(amdval(0))
            endelse

    	    ibsel = state.ibsel
            neval = state.iea(ibsel)

		;**** Get current temperature table widget value ****

 	    widget_control, state.tempid, get_value=tempval

		;**** Copy new  values into structure  ****
 
	    if (neval gt 0) then begin    
    	        tempval.value(1,0:neval-1 ) = 				$
	        strtrim(string(amd*state.evals(0:neval-1,2,ibsel),   	$
	        format=state.num_form),2)

		;**** Copy new data to table widget ****
   
 	        widget_control, state.tempid, set_value=tempval

  	    endif

	  	;**** reset state.amd             ****

	    state.amd = amd
	end

		;****************************************
		;****   select receiver mass number  ****
		;****************************************

        state.amrid: begin
            widget_control, state.amrid, get_value=amrval
            if strcompress(amrval(0), /remove_all) eq '' then begin
  	        amr =2.0
            endif else begin
	        amr = float(amrval(0))
            endelse

    	    ibsel = state.ibsel
            neval = state.iea(ibsel)

		;**** Get current temperature table widget value ****

 	    widget_control, state.tempid, get_value=tempval

		;**** Copy new  values into structure  ****
 
	    if (neval gt 0) then begin    
                tempval.value(2,0:neval-1) = 				$
                strtrim(string(amr*state.evals(0:neval-1,2,ibsel),   	$
                format=state.num_form),2)

		;**** Copy new data to table widget ****
   
 	        widget_control, state.tempid, set_value=tempval

  	    endif

	  	;**** reset state.amr             ****

	    state.amr = amr
	end

		;*************************************
		;**** Default energies button     ****
		;*************************************

        state.defeid: begin

		;**** popup window to confirm overwriting current values ****

	    action= popup(message='Confirm Overwrite values with Defaults', $
			  buttons=['Confirm','Cancel'], font=state.font)

	    if action eq 'Confirm' then begin

		;**** Get current table widget value ****

 	   	widget_control,state.enerid,get_value=enerval

		;**** Copy defaults into value structure    ****
		;**** For default values use the existing   ****
		;**** table values and let user modify them ****

           	units = enerval.units+1
	   	neval = state.iea(state.ibsel)
 	   	if neval gt 0 then begin
   	            enerval.value(0,0:neval-1) =   			$
	            strtrim(string(enerval.value(units,0:neval-1), 	$
	            format=state.num_form),2)
 	   	endif

		;**** Fill out the rest with blanks ****

 	    	if neval lt ndein then begin
   	            enerval.value(0,neval:ndein-1) = ''
 	    	endif

		;**** Copy new data to table widget ****

 	        widget_control, state.enerid, set_value=enerval
	    endif
        end


		;*****************************************
		;**** Default temperatures button     ****
		;*****************************************

        state.deftid: begin

		;**** popup window to confirm overwriting current values ****

	    action= popup(message='Confirm Overwrite values with Defaults', $
			  buttons=['Confirm','Cancel'], font=state.font)

	    if action eq 'Confirm' then begin

		;**** Get current temp. widget value ****

 	   	widget_control,state.tempid,get_value=tempval

		;**** Copy defaults into value structure    ****
		;**** For default values use the existing   ****
		;**** table values and let user modify them ****

           	units = tempval.units+1
	   	neval = state.iea(state.ibsel)
 	   	if neval gt 0 then begin
   	            tempval.value(0,0:neval-1) =   			$
	            strtrim(string(tempval.value(units,0:neval-1), 	$
	            format=state.num_form),2)
 	   	endif

		;**** Fill out the rest with blanks ****

 	    	if neval lt ndtin then begin
   	            tempval.value(0,neval:ndtin-1) = ''
 	    	endif

		;**** Copy new data to table widget ****

 	        widget_control, state.tempid, set_value=tempval
	    endif
        end
		;***********************
		;**** Cancel button ****
		;***********************

    	state.cancelid: new_event = {ID:parent, TOP:event.top, 		$
				     HANDLER:0L, ACTION:'Cancel'}

		;*********************
		;**** Done button ****
		;*********************

    	state.doneid: begin

		;***************************************
		;**** Check all user input is legal ****
		;***************************************

	    error = 0

        	;********************************************
		;*** Have to restore "state" before calls ***
		;*** to proc509_get_val so it can be used ***
		;*** there.				  ***
		;********************************************
		
	    widget_control, first_child, set_uvalue=state, /no_copy

	    widget_control, event.handler, get_value=ps

		;*** reset state variable ***

	    widget_control, first_child, get_uvalue=state, /no_copy

		;**** check energy values entered ****

	    if (ps.losel eq 1) then begin
	        if error eq 0 and ps.ieval eq 0 then begin
	            error = 1
	            message='**** Error: No energies entered ****'
                endif
	    endif

		;*** Check to see if index has been selected ***

	    if error eq 0 and ps.ibsel lt 0 then begin
	        error = 1
	        message='**** Error: Invalid block selected ****'
	    endif

		;*** Check to see if sensible tolerance is selected.

	    if (error eq 0 and ps.lfsel eq 1) then begin 
	        if (float(ps.tolval) lt 0)  or 				$
   	        (float(ps.tolval) gt 100) then begin
	            error = 1
	            message='**** Error: Tolerance for polyfit must be'+$
                            ' 0-100% ****'
	        endif
	    endif

		;**** return value or flag error ****

	    if error eq 0 then begin
	        new_event = {ID:parent, TOP:event.top, 			$
			     HANDLER:0L, ACTION:'Done'}
            endif else begin
	        widget_control, state.messid, set_value=message
	        new_event = 0L
            endelse
        end

                ;**** Menu button ****

      	state.outid: begin
            new_event = {ID:parent, TOP:event.top, HANDLER:0L,          $
                         ACTION:'Menu'}
      	end


      	ELSE: new_event = 0L

    ENDCASE

		;*********************************************
		;*** make "state" available to other files ***
		;*********************************************

    widget_control, first_child, set_uvalue=state, /no_copy

    RETURN, new_event

END

;-----------------------------------------------------------------------------

FUNCTION cw_adas509_proc, topparent, dsfull, act, nstore, nedim, 	$
			  ndein, ndtin, nbsel, 				$
			  cdonor, crecvr, cfstat, ctype,                $
                	  iea, evals, bitfile, procval=procval, 	$
			  uvalue=uvalue, font_large=font_large, 	$
			  font_small=font_small, edit_fonts=edit_fonts, $
  			  num_form=num_form

COMMON adas509_proc_blk, nstorecom, nedimcom, ndeincom, ndtincom, nbselcom

    nstorecom = nstore
    nedimcom  = nedim
    ndeincom  = ndein
    ndtincom  = ndtin
    nbselcom  = nbsel

		;**** Set defaults for keywords ****

    if not (keyword_set(uvalue)) then uvalue = 0
    if not (keyword_set(font_large)) then font_large = ''
    if not (keyword_set(font_small)) then font_small = ''
    if not (keyword_set(edit_fonts)) then 				$
    edit_fonts = {font_norm:'',font_input:''}
    if not (keyword_set(num_form)) then num_form = '(E10.3)'
    if not (keyword_set(procval)) then begin
        ener_arr = fltarr(ndein)
        temp_arr = fltarr(ndtin)
        ps = { 	new   	: 	0, 					$
		title 	: 	'',             			$
		ibsel 	: 	0,					$
		amd 	: 	2.0,					$
		amr 	: 	2.0,					$
		ieval 	: 	ndein,					$
		ietyp 	: 	1,					$
                ein  	:	ener_arr,				$
		itval 	: 	ndtin,					$
		ittyp 	: 	3,					$
                tin  	:	temp_arr,				$
                lfsel 	:	0,              			$
                tolval	: 	'5',					$
		losel 	:	0,					$
		ltsel 	:	0					}
	if (nedim le ndein) then begin
	    ps.ein(0:nedim-1)=evals(*,0,0)
	endif else begin
	    ps.ein=evals(0:ndein-1,0,0)
	    print,'cw_adas509_proc.pro : truncating energy list for '+	$
		  'energy values, too many values'
	endelse
	if (nedim le ndtin) then begin
	    ps.tin(0:nedim-1)=evals(*,2,0)
	endif else begin
	    ps.tin = evals(0:ndtin-1,2,0)
	    print,'cw_adas509_proc.pro : truncating energy list for '+	$
		  'temperature values, too many values'
	endelse

    endif else begin
	 ps = {  new   	: 	procval.new,   				$
                 title 	: 	procval.title,  			$
                 ibsel 	: 	procval.ibsel,  			$
		 amd 	: 	procval.amd,				$
		 amr 	: 	procval.amr,				$
                 ieval 	: 	procval.ieval,  			$
                 ietyp 	: 	procval.ietyp,  			$
                 ein  	: 	procval.ein,   				$
                 itval 	: 	procval.itval,  			$
                 ittyp 	: 	procval.ittyp,  			$
                 tin  	: 	procval.tin,   				$
                 lfsel 	: 	procval.lfsel,  			$
                 tolval	: 	procval.tolval, 			$
		 losel	: 	procval.losel,   			$
		 ltsel	: 	procval.ltsel   			}
    endelse

		;****************************************************
		;****       Assemble energy table data           ****
		;****************************************************
		;**** The adas table widget requires data to be *****
		;**** input as strings, so all the numeric data *****
		;**** has to be written into a string array.    *****
		;****     Declare ener.table array              *****
		;**** col 1 has user energy values              *****
		;**** col 2-4 have energy values from files,    *****
		;**** which can have one of three possible units*****
		;****************************************************

    if ps.ibsel lt nbsel then userindex=ps.ibsel else userindex=0
    ieval = ps.ieval
    ibsel = userindex
    neval = iea(ibsel)
    enerdata = strarr(4,ndein)

		;**** Copy out energy values      ****
		;**** number of energy            ****
		;**** values for this data block  ****
 

    if (neval gt 0) then begin
        enerdata(0,*) = strtrim(string(ps.ein,format=num_form),2)
    	enerdata(1,0:neval-1 ) = 					$
	strtrim(string(evals(0:neval-1,0,ibsel),format=num_form),2)
        enerdata(2,0:neval-1) = 					$
	strtrim(string(evals(0:neval-1,1,ibsel),format=num_form),2)
        enerdata(3,0:neval-1) = 					$
        strtrim(string(evals(0:neval-1,2,ibsel),format=num_form),2)

		;**** fill rest of table with blanks ****

        blanks = where(enerdata eq 0.0)
	if (blanks(0) ge 0) then begin
	    enerdata(blanks) = ' '
	endif
    endif

		;****************************************************
		;****       Assemble temperature table data      ****
		;****************************************************
		;**** The adas table widget requires data to be *****
		;**** input as strings, so all the numeric data *****
		;**** has to be written into a string array.    *****
		;****     Declare temp.table array              *****
		;**** col 1 has user temperature values         *****
		;**** col 2-4 have temp. values from files,     *****
		;**** which can have one of three possible units*****
		;****************************************************

    if ps.ibsel lt nbsel then userindex=ps.ibsel else userindex=0
    itval = ps.ieval
    ibsel = userindex
    neval = iea(ibsel)
    tvals = dblarr(nedim,3)
    tempdata = strarr(4,ndtin)

		;**** Copy out temperature values      ****
		;**** number of temperature            ****
		;**** values for this data block       ****

	tvals(*,0)=ps.amd*evals(*,2,ibsel)	
	tvals(*,1)=ps.amr*evals(*,2,ibsel)	
	tvals(*,2)=evals(*,2,ibsel)	
 

    if (neval gt 0) then begin
        tempdata(0,*) = strtrim(string(ps.tin,format=num_form),2)
    	tempdata(1,0:neval-1 ) = 					$
	strtrim(string(tvals(0:neval-1,0),format=num_form),2)
        tempdata(2,0:neval-1) = 					$
	strtrim(string(tvals(0:neval-1,1),format=num_form),2)
        tempdata(3,0:neval-1) = 					$
        strtrim(string(tvals(0:neval-1,2),format=num_form),2)

		;**** fill rest of table with blanks ****

        blanks = where(tempdata eq 0.0)
	if (blanks(0) ge 0) then begin
	    tempdata(blanks) = ' '
	endif
    endif

		;********************************************************
		;**** Create the 509 Processing options/input window ****
		;********************************************************

		;**** create titled base widget ****

    parent = widget_base(topparent, UVALUE = uvalue, 			$
			 title = 'ADAS509 PROCESSING OPTIONS',		$
			 EVENT_FUNC = "proc509_event", 			$
			 FUNC_GET_VALUE = "proc509_get_val", 		$
			 /COLUMN, XOFFSET=1, YOFFSET=1)

		;******************************************************
		;**** Create a dummy widget just to hold value of *****
		;**** "state" variable so as not to get confused  *****
		;**** with any other values. Adopt IDL practice   *****
		;**** of using first child widget                 *****
		;******************************************************

    first_child = widget_base(parent)
    topbase = widget_base(first_child,/column)

		;***********************
		;**** add run title ****
		;***********************

    base = widget_base(topbase, /row)
    rc = widget_label(base, value='Title for Run', font=font_large)
    runid = widget_text(base, value=ps.title, xsize=38, 		$
		        font=font_large, /edit)

		;**** add dataset name and browse button ****

    rc = cw_adas_dsbr(topbase, dsfull, font=font_large)

		;***************************************************
		;**** add a window to display and select index   ***
		;**** first create data array for table          ***
		;**** Then convert to 1D string array(text_table)***
		;**** call cw_single_sel.pro for options choice  ***
		;***************************************************

    block_info = strarr(4, nbsel)
    block_info(0,*) = cdonor
    block_info(1,*) = crecvr
    block_info(2,*) = cfstat
    block_info(3,*) = ctype
    titles = [['Donor', 'Recvr', 'Final', 'Type'], 			$
   	      ['Ion','Ion','State', ' ']]
    select_data = text_table(block_info, colhead=titles)
    coltitles = select_data(0:1)
    select_data = select_data(2:nbsel+1)
    indexid = cw_single_sel( parent, select_data, value=userindex, 	$
			     title='Select data Block', 		$
			     coltitles = coltitles, ysize = 5, 		$
			     font=font_small, big_font=font_large)

		;**********************
		;**** Another base ****
		;**********************

    parambase = widget_base(parent, /row, /frame)

		;********************************
		;**** add parameter typeins ****
		;********************************

    paramhead = widget_label(parambase, font=font_large,		$
		value="Please enter the reactant mass numbers    ")
    amdlab    = widget_label(parambase, font=font_small, value="Donor: ")
    amdval    = " " + strtrim(string(ps.amd, format='(f5.1)'),2)
    amdid     = widget_text(parambase, font=font_small, /editable,	$
		value=amdval, xsize=10)		
    amrlab    = widget_label(parambase, font=font_small, value="Recvr: ")
    amrval    = " " + strtrim(string(ps.amr, format='(f5.1)'),2)
    amrid     = widget_text(parambase, font=font_small, /editable,	$
		value=amrval, xsize=10)
		
		;**********************
		;**** Another base ****
		;**********************

    tablebase = widget_base(parent, /row, /frame)

		;*******************************************************
		;**** base for the energy table and defaults button ****
		;**** and whether table used for text output        ****
		;*******************************************************

    base = widget_base(tablebase, /column, /frame)

		;***************************************
		;*** "LOSEL" parameter switch **********
		;***************************************

    lbase = widget_base(base, /row)
    llabel = widget_label(lbase, value="Select Energies for output file", $
			  font = font_large)
    lbbase = widget_base(lbase, /row, /nonexclusive)
    lebut = widget_button(lbbase, value=' ')
    widget_control, lebut, set_button=ps.losel

		;********************************
		;**** energy data table      ****
		;********************************

    colhead = [['Output','Input'], ['','']]

		;********************************************
		;**** convert FORTRAN index to IDL index ****
		;********************************************

    units = ps.ietyp-1 
    unitname = ['at. un.','cm s-1','eV/amu']

		;************************************************************
		;**** Two  columns in the table and three sets of units. ****
		;**** Column 1 has the same values for all three         ****
		;**** units but column 2 switches between sets 2,3 & 4   ****
		;**** in the input array enerdata as the units change.  ****
		;************************************************************

    unitind = [[0,0,0],[1,2,3]]
    table_title =  ["Output Collision Energies "] 

		;****************************
		;**** table of data   *******
		;****************************
 
    enerid = cw_adas_table(base, enerdata, units, unitname, unitind, 	$
			   UNITSTITLE = 'Energy Units', 		$
			   COLEDIT = [1,0], COLHEAD = colhead, 		$
			   TITLE = table_title, ORDER = [1,0], 		$ 
			   LIMITS = [1,1], CELLSIZE = 12, 		$
			   /SCROLL, YTABSIZE = 7, NUM_FORM = num_form,	$
			   FONTS = edit_fonts, FONT_LARGE = font_large, $
			   FONT_SMALL = font_small)

		;*************************
		;**** Default button  ****
		;*************************

    defeid = widget_button(base,value=' Default Energy Values  ',	$
		           font=font_large)

		;*** Make table non/sensitive as ps.losel ***

    widget_control, enerid, sensitive = ps.losel
    widget_control, defeid, sensitive = ps.losel

		;****************************
		;**** table of data   *******
		;****************************

    base = widget_base(tablebase, /column, /frame)
 
		;*******************************************************
		;**** base for the temp.  table and defaults button ****
		;**** and whether table used for text output        ****
		;*******************************************************

		;***************************************
		;*** "LTSEL" parameter switch **********
		;***************************************

    lbase = widget_base(base, /row)
    llabel = widget_label(lbase, value="Select Temperatures for TCX file", $
			  font = font_large)
    lbbase = widget_base(lbase, /row, /nonexclusive)
    ltbut = widget_button(lbbase, value=' ')
    widget_control, ltbut, set_button=ps.ltsel

		;********************************
		;**** temperature data table ****
		;********************************

    colhead = [['Output','Input'], ['','']]

		;********************************************
		;**** convert FORTRAN index to IDL index ****
		;********************************************

    units = ps.ittyp-1 
    unitname = ['eV(don.)','eV(rec.)','eV/amu']

		;************************************************************
		;**** Two  columns in the table and three sets of units. ****
		;**** Column 1 has the same values for all three         ****
		;**** units but column 2 switches between sets 2,3 & 4   ****
		;**** in the input array enerdata as the units change.  ****
		;************************************************************

    unitind = [[0,0,0],[1,2,3]]
    table_title =  ["Output Temperatures "] 

		;****************************
		;**** table of data   *******
		;****************************
    tempid = cw_adas_table(base, tempdata, units, unitname, unitind, 	$
			   UNITSTITLE = 'Temperature Units', 		$
			   COLEDIT = [1,0], COLHEAD = colhead, 		$
			   TITLE = table_title, ORDER = [1,0], 		$ 
			   LIMITS = [1,1], CELLSIZE = 12, 		$
			   /SCROLL, YTABSIZE = 7, NUM_FORM = num_form,	$
			   FONTS = edit_fonts, FONT_LARGE = font_large, $
			   FONT_SMALL = font_small)

		;*************************
		;**** Default button  ****
		;*************************

    deftid = widget_button(base,value=' Default Temperature Values  ',	$
		           font=font_large)

		;*** Make table non/sensitive as ps.ltsel ***

    widget_control, tempid, sensitive = ps.ltsel
    widget_control, deftid, sensitive = ps.ltsel

		;**************************************
		;**** Add polynomial fit selection ****
		;**************************************

    polyset = { option:intarr(1), val:strarr(1)}  ; defined thus because 
						  ; cw_opt_value.pro
                                                  ; expects arrays
    polyset.option = [ps.lfsel]
    polyset.val = [ps.tolval]
    options = ['Fit Polynomial']
    optbase = widget_base(topbase, /frame)
    optid   = cw_opt_value(optbase, options, title='Polynomial Fitting',$
			   limits = [0,100], value=polyset, 		$
			   font=font_large)

		;**** Error message ****

    messid = widget_label(parent,font=font_large, 			$
    value='Edit the processing options data and press Done to proceed')

		;**** add the exit buttons ****

    base = widget_base(parent, /row)
    menufile = bitfile + '/menu.bmp'
    read_X11_bitmap, menufile, bitmap1
    outid = widget_button(base, value=bitmap1)		;menu button
    cancelid = widget_button(base, value='Cancel', font=font_large)
    doneid = widget_button(base, value='Done', font=font_large)
  
		;**** create a state structure for the pop-up ****
		;**** 		     window.                  ****

    new_state = { 	runid		:	runid,  		$
			messid		:	messid, 		$
			amdid		:	amdid,			$
			amrid		:	amrid,			$
			defeid		:	defeid,			$
			deftid		:	deftid,			$
			enerid		:	enerid, 		$
			tempid		:	tempid, 		$
			losel 		: 	ps.losel, 		$
                	lebut  		: 	lebut,			$
			ltsel 		: 	ps.ltsel, 		$
                	ltbut  		: 	ltbut,			$
			optid		:	optid,			$
 			indexid		:	indexid,		$
			cancelid	:	cancelid,		$
			doneid		:	doneid, 		$
	        	outid		:	outid,			$
			parambase	:	parambase,		$
			dsfull		:	dsfull,			$
			ibsel		:	ibsel,			$
			amd 		:	ps.amd,			$
			amr 		:	ps.amr, 		$
			neval		:	neval,			$
			iea		:	iea,			$
			evals		:	evals,			$
			itval		:	itval, 			$
			tvals		:	tvals,			$
			font		:	font_large,		$
			num_form	:	num_form		}

                 ;**** Save initial state structure ****

    widget_control, first_child, set_uvalue=new_state, /no_copy

    RETURN, parent

END

