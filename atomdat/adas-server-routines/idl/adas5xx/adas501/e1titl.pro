; Copyright (c) 1995, Strathclyde University .
; SCCS info: Module @(#)$Header: /home/adascvs/idl/adas5xx/adas501/e1titl.pro,v 1.3 2004/07/06 13:37:22 whitefor Exp $	Date $Date: 2004/07/06 13:37:22 $
;+
; PROJECT:
;       ADAS IBM MVS to UNIX conversion
;
; NAME:
;	E1TITL
;
; PURPOSE:
;	IDL user interface and communications with ADAS501 FORTRAN
;	process via pipe.
;
; EXPLANATION:
;	This routine reads some information pertaining to the current data 
;	file being analysed and then constructs a descriptive title which is
;	used in the final graphical output.  It replaces the old E1TITL.FOR
;	
;
; USE:
;	The use of this routine is specific to ADAS501, see adas501.pro.
;
; INPUTS:
;	PIPE	- The IDL unit number of the bi-directional pipe to the
;		  ADAS501 FORTRAN process.
;
; OPTIONAL INPUTS:
;	None
;
; OUTPUTS:
;	None ( all outputs directly to FORTRAN E1TITL)
;
; OPTIONAL OUTPUTS:
;	None
;
; KEYWORD PARAMETERS:
;
; CALLS:
;
; SIDE EFFECTS:
;	This routine communicates with the ADAS501 FORTRAN process
;	via a UNIX pipe.
;
; CATEGORY:
;	Adas system.
;
; WRITTEN:
;       Lalit Jalota, Tessella Support Services plc, 2-Mar-1995
;
; MODIFIED:
;	1.1	Lalit Jalota
;		First Release
;	1.2	Lalit Jalota
;	1.3	Tim Hammond
;		Tidied up comments and code
;
; VERSION:
;	1.1	02-03-95	
;	1.2	03-03-95
;	1.3	06-02-96
;
;-
;-----------------------------------------------------------------------------

PRO e1titl, pipe

		;**** declare variables for input from pipe ****

    dsfull = ' '
    esym = ' ' 
    cindm = ' '
    cwavel = ' '
    ibsel = 0
    iz = 0
    sdum = ' '
    idum = 0

		;**** read data in from pipe ****

    readf, pipe, sdum, format='(A80)'
    dsfull = sdum
    readf, pipe, sdum, format='(A2)'
    esym = sdum
    readf, pipe, sdum, format='(A2)'
    cindm = sdum
    readf, pipe, sdum, format='(A10)'
    cwavel = sdum
    readf, pipe, idum
    ibsel = idum
    readf, pipe, idum
    iz = idum

		;**** construct titlx ****

    titlx = strcompress(dsfull) + 					$
            ' BLK =' + strcompress(string(ibsel)) + ' ' + 		$
	    strcompress(strupcase(esym)) + '+' + 			$
            strcompress(string(iz)) + '  WAVELENGTH = ' + 		$
            strcompress(cwavel) + '  MET = ' + strcompress(cindm)

		;**** Now return value of titlx to E1TITL ****

    printf, pipe, titlx

END
