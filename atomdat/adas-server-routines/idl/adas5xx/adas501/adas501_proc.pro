; Copyright (c) 1995, Strathclyde University .
; SCCS Info : Module @(#)$Header: /home/adascvs/idl/adas5xx/adas501/adas501_proc.pro,v 1.5 2004/07/06 10:52:23 whitefor Exp $ Date $Date: 2004/07/06 10:52:23 $
;+
; PROJECT:
;       ADAS IBM MVS to   UNIX conversion
;
; NAME:
;	ADAS501_PROC
;
; PURPOSE:
;	IDL ADAS user interface, processing options/input.
;
; EXPLANATION:
;	This routine creates and manages a pop-up window which allows
;	the user to select options and input data to control ADAS501
;	processing.
;
; USE:
;	This routine is ADAS501 specific, see e1ispf.pro for how it
;	is used.
;
; INPUTS:
;	PROCVAL	- A structure which determines the initial settings of
;		  the processing options widget.  The value is passed
;		  unmodified into cw_adas501_proc.pro.
;
;		  See cw_adas501_proc.pro for a full description of this
;		  structure.
;
;	DSFULL	- String; The full system file name of the input 
;		          dataset selected by the user for processing.
;
;	NSTORE  - Integer: maximum number of data blocks which can be
;			   read from the data set.
;
;	NTDIM	- Integer : Maximum number of temperatures allowed.
;
;	NDDIM	- Integer : Maximum number of densities allowed.
;
;	NDTIN	- Integer : Maximum number of temperature/density pairs
;		           allowed.
;
;	NBSEL   - Integer : number of data blocks accepted and read-in
;
;	CWAVEL  - String array : Wavelength (Angstroms) of data blocks
;				 dimension data block index (NSTORE).
;	CFILE   - String array : Specific Ion file source.
;				 dimension data block index (NSTORE).
;	CPCODE  - String array : processing code for each data block 
;				 dimension data block index (NSTORE) 
;	CINDM   - String array : Metastable index for each data block
;				 dimension data block index (NSTORE)
;	ITA     - Integer array: Number of electron temperatures
;				 dimension data block index (NSTORE)
;	IDA     - Integer array: Number of electron densities
;				 dimension data block index (NSTORE)
;	TVALS   - Double array : Electron temperatures - 
;				 1st dimension - temperature index
;				 2nd dimension - units 1 - Kelvin
;						       2 - eV
;						       3 - Reduced
;				 3rd dimension - data block index
;	TEDA	- Double array : Electron densities - 
;				 1st dimension - densities index
;				 2nd dimension - data block index.
;
;	SXB	- Double array : Ionisation per photon values  
;				 1st dimension - temperature index
;				 2nd dimension - densities index
;				 3rd dimension - data block index.
;
;       BITFILE - String; the path to the directory containing bitmaps
;                 for the 'escape to series menu' button.
;
; OPTIONAL INPUTS:
;	None
;
; OUTPUTS:
;	PROCVAL	- On output the structure records the final settings of
;		  the processing selections widget if the user pressed the
;		  'Done' button, otherwise it is not changed from input.
;
;	ACT	- String; Either 'Done' or 'Cancel' for the button the
;		  user pressed to terminate the processing options
;		  window.
;
; OPTIONAL OUTPUTS:
;	None
;
; KEYWORD PARAMETERS:
;	FONT_LARGE - The name of a larger font.
;
;	FONT_SMALL - The name of a smaller font.
;
;	EDIT_FONTS - A structure of two fonts used in the table editor
;		     adas_edtab.pro, {font_norm:'',font_input:''}.
;
; CALLS:
;	CW_ADAS501_PROC	Declares the processing options widget.
;	DYNLABEL	Recursively adds dynamic_resize keyword to label
;			widgets for version 4.0.1 and higher of IDL
;	ADAS501_PROC_EV Called indirectly during widget management,
;			routine included in this file.
;	XMANAGER
;
; SIDE EFFECTS:
;       This widget uses a common block PROC501_BLK, to pass the
;       variables VALUE and ACTION between the two routines. 
;
; CATEGORY:
;	Widgets
;
; WRITTEN:
;       Lalit Jalota, Tessella Support Services plc, 2-Dec-1995
;
; MODIFIED:
;	1.0	Lalit Jalota
;		First Release
;	1.3	Tim Hammond
;		Tidied up comments and code and added 'return to menu'
;		bitmapped button.
;	1.4	William Osborn
;		Added dynlabel procedure
;     1.5   Richard Martin
;           Add SXB array to input and output for interactive pair
;           selector
;
; VERSION:
;	1.0	21-02-95	
;	1.3	05-02-96
;	1.4	11-07-96
;	1.5	14-10-99
;
;-
;-----------------------------------------------------------------------------

PRO adas501_proc_ev, event

    COMMON proc501_blk, action, value

    action = event.action

    CASE event.action OF

		;**** 'Done' button ****

	'Done'  : begin

		;**** Get the output widget value ****

	    widget_control, event.id, get_value=value 
	    widget_control, event.top, /destroy
	end

		;**** 'Cancel' button ****

	'Cancel': widget_control, event.top, /destroy

                ;**** 'Menu' button ****

        'Menu': widget_control, event.top, /destroy

	ELSE:			;**** Do nothing ****

    ENDCASE

END

;-----------------------------------------------------------------------------

PRO adas501_proc, procval, dsfull, nstore, ntdim, nddim, ndtin, nbsel, 	$
		  cwavel, cfile, cpcode, cindm, ita, ida, tvals, teda, 	$
		  sxb, act, bitfile, FONT_LARGE=font_large, 			$
                  FONT_SMALL=font_small, EDIT_FONTS=edit_fonts


		;**** declare common variables ****

    COMMON proc501_blk, action, value

		;**** Copy "procval" to common ****

    value = procval

		;**** Set defaults for keywords ****

    IF NOT (KEYWORD_SET(font_large)) THEN font_large = ''
    IF NOT (KEYWORD_SET(font_small)) THEN font_small = ''
    IF NOT (KEYWORD_SET(edit_fonts)) THEN 				$
    edit_fonts = { font_norm:'', font_input:''}
		
                ;**** create base widget ****

    procid = widget_base(TITLE='ADAS501 PROCESSING OPTIONS', 		$
                         XOFFSET=50, YOFFSET=1)

		;**** Declare processing widget ****

    cwid = cw_adas501_proc(procid, dsfull, nstore,  			$
			   ntdim, nddim, ndtin, nbsel, 			$
			   cwavel, cfile, cpcode, cindm, 		$
			   ita, ida, tvals, teda, sxb, 			$
			   bitfile, PROCVAL=value, 			$
		 	   FONT_LARGE=font_large, FONT_SMALL=font_small,$
			   EDIT_FONTS=edit_fonts)

		;**** Realize the new widget ****

    dynlabel, procid
    widget_control, procid, /realize

		;**** make widget modal ****

    xmanager, 'adas501_proc', procid, event_handler='adas501_proc_ev', 	$
              /modal, /just_reg

		;*** copy value back to procval for return to e1ispf ***

    act = action
    procval = value
 
END

