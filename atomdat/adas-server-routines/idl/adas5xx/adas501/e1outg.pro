; Copyright (c) 1995, Strathclyde University.
; SCCS info: Module @(#)$Header: /home/adascvs/idl/adas5xx/adas501/e1outg.pro,v 1.5 2004/07/06 13:35:42 whitefor Exp $	Date $Date: 2004/07/06 13:35:42 $
;+
; PROJECT:
;       ADAS IBM MVS to UNIX conversion
;
; NAME:
;	E1OUTG
;
; PURPOSE:
;	Communication with ADAS501 FORTRAN via UNIX pipe and
;	graphics output.
;
; EXPLANATION:
;	The routine begins by reading information from the ADAS501
;	FORTRAN process via a UNIX pipe.  Then the IDL graphical
;	output routine for ADAS501 is invoked.  Communications are to
;	the FORTRAN subroutine E1OUTG.
;
; USE:
;	The use of this routine is specific to ADAS501 see adas501.pro.
;
; INPUTS:
;	DSFULL   - Data file name
;
;	PIPE	 - The IDL unit number of the bi-directional pipe to the
;		   ADAS501 FORTRAN process.
;
;       UTITLE   - Optional comment by user
;
;	GRPSCAL	 - Integer; 0 if default scaling required 1 if user
;		   specified scaling to be used.
;
;	XMIN	 - String; User sepcified x-axis minimum, number as string.
;
;	XMAX	 - String; User sepcified x-axis maximum, number as string.
;
;	YMIN	 - String; User specified y-axis minimum, number as string.
;
;	YMAX	 - String; User specified y-axis maximum, number as string.
;
;	FITFLAG - 1	= polynomial fit permitted
;               0 = polynomial fit not permitted
;
;	HRDOUT	 - Integer; 0 if no hardcopy required, 1 if hardcopy.
;
;	HARDNAME - String; file name for hardcopy output.
;
;	DEVICE	 - String; IDL name for hardcopy output device.
;
;	HEADER	 - ADAS version header information for inclusion in the
;		   graphical output.
;
;       BITFILE - String; the path to the directory containing bitmaps
;                 for the 'escape to series menu' button.
;
; OPTIONAL INPUTS:
;	None.
;
; OUTPUTS:
;       GOMENU  - Int; flag - set to 1 if user has selected 'escape direct
;                 to series menu , otherwise 0
;
; OPTIONAL OUTPUTS:
;	None.
;
; KEYWORD PARAMETERS:
;	FONT	- String; Name of the font to be used for the graphical
;		  output widget.
;
; CALLS:
;	ADAS501_PLOT	ADAS501 graphical output.
;
; SIDE EFFECTS:
;	This routine reads information from FORTRAN via a UNIX pipe.
;
; CATEGORY:
;	Adas system.
;
; WRITTEN:
;       Lalit Jalota, Tessella Support Services plc, 12-Jan-1995
;
; MODIFIED:
;	1.1	Lalit Jalota
;		First release
;	1.2	Lalit Jalota
;		Added utitle
;	1.3	Tim Hammond
;		Tidied up comments and code and added bitmapped return to
;		menu button functionality.
;	1.4	Tim Hammond
;		Corrected pipe reading of headers.
;     1.5   Richard Martin
;           Added fitflag, devcode & devlist to input & output.
;	
; VERSION:
;	1.1	21-02-95
;	1.2	03-03-95 	
;	1.3	05-02-96
;	1.4	27-02-96
;	1.5	14-10-99
;
;-
;-----------------------------------------------------------------------------

PRO e1outg, dsfull, pipe, utitle, grpscal, xmin, xmax, ymin, ymax, fitflag,	$
	    hrdout, hardname, device, header, bitfile, gomenu, devcode,devlist,FONT=font

                ;**** Set defaults for keywords ****

  IF NOT (KEYWORD_SET(font)) THEN font = ''


		;**** Declare variables for input ****

    title = " "
    titlx = " "
    titlm = " "
    date = " "
    esym = " "
    iz0 = 0
    iz = 0
    cwavel = " "
    cfile = " "
    cpcode = " " 
    cindm = " "
    itval = 0
    ldef1 = 0
    teva = 0
    din = 0
    xmin = 0.0
    xmax = 0.0
    ymin = 0.0
    ymax = 0.0
    tdfitm = 0
    lfsel = 0
    ldfit = 0
    nmx = 0
    head1 = " "
    head2a = " "
    head2b = " "
    strg = make_array(7, /string, value=" ")
    dummy = 0.0
    sdum = " "
    idum = 0

		;********************************
		;**** Read data from fortran ****
		;********************************

    readf, pipe, format = '(a40)' , sdum
    title = sdum
    readf, pipe, format = '(a120)' , sdum
    titlx = sdum
    readf, pipe, format = '(a80)' , sdum
    titlm = sdum
    readf, pipe, format = '(a8)' , sdum
    date = sdum
    readf, pipe, format = '(a2)' , sdum
    esym = sdum
    readf, pipe, idum
    iz0 = idum
    readf, pipe, idum
    iz = idum
    readf, pipe, format = '(a10)' , sdum
    cwavel = sdum
    readf, pipe, format = '(a8)' , sdum
    cfile = sdum
    readf, pipe, format = '(a8)' , sdum
    cpcode = sdum
    readf, pipe, format = '(a2)' , sdum
    cindm = sdum
    readf, pipe, idum 
    itval = idum

		;**** now declare array dimensions ****

    teva = dblarr(itval) 
    din = dblarr(itval) 
    sxba = dblarr(itval) 
    for i=0, itval-1 do begin
        readf, pipe, dummy
        teva(i) = dummy 
    endfor
    for i=0, itval-1 do begin
        readf, pipe, dummy
        din(i) = dummy 
    endfor
    for i=0, itval-1 do begin
        readf, pipe, dummy
        sxba(i) = dummy 
    endfor
    readf, pipe, idum
    ldef1 = idum
    if (ldef1 eq 1) then begin
        readf, pipe, dummy
	xmin = dummy
        readf, pipe, dummy
	xmax = dummy
        readf, pipe, dummy
	ymin = dummy
        readf, pipe, dummy
	ymax = dummy
    endif 
    readf, pipe, idum
    lfsel = idum
    if (lfsel eq 1) then begin
        readf, pipe, idum
	ldfit = idum
        readf, pipe, idum
	nmx = idum

 		;**** declare arrays ****

        sxbm = fltarr(nmx)
        tdfitm = fltarr(nmx)
        for i = 0, nmx-1 do begin
	    readf, pipe, dummy
            sxbm(i) = dummy 
        endfor
        for i = 0, nmx-1 do begin
 	    readf, pipe, dummy 
            tdfitm(i) = dummy
        endfor
    endif
    readf, pipe, idum
    ldfit = idum
    test = " "
    for i = 0, 6 do begin
        readf, pipe, test 
        strg(i) = test
    endfor
    readf, pipe, format = '(a32)', sdum
    head1 = sdum
    readf, pipe, format = '(a45)', sdum
    head2a = sdum
    readf, pipe, format = '(a45)', sdum
    head2b = sdum

		;***********************
		;**** Plot the data ****
		;***********************

    adas501_plot, dsfull, 						$
  		  title , titlx, titlm, utitle, date, esym, iz0, iz, 	$
  		  cwavel, cfile, cpcode, cindm, itval,  		$
		  teva, din, sxba, sxbm, tdfitm, 			$
                  ldef1, xmin, xmax, ymin, ymax, 			$
   		  lfsel, ldfit, fitflag, nmx, 				$
   		  strg, head1, head2a, head2b, 				$
		  hrdout, hardname, device, header, bitfile, gomenu,	$
                  devcode, devlist, FONT=font

END
