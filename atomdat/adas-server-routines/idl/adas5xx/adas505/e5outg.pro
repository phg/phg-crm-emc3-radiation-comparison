; Copyright (c) 1995, Strathclyde University.
;  SCCS info: Module @(#)$Header: /home/adascvs/idl/adas5xx/adas505/e5outg.pro,v 1.2 2004/07/06 13:43:17 whitefor Exp $ Date $Date: 2004/07/06 13:43:17 $
;+
; PROJECT:
;       ADAS IBM MVS to UNIX conversion
;
; NAME:
;	E5OUTG
;
; PURPOSE:
;	Communication with ADAS505 FORTRAN via UNIX pipe and
;	graphics output.
;
; EXPLANATION:
;	The routine begins by reading information from the ADAS505
;	FORTRAN process via a UNIX pipe.  Then the IDL graphical
;	output routine for ADAS505 is invoked.  Communications are to
;	the FORTRAN subroutine E5OUTG.
;
; USE:
;	The use of this routine is specific to ADAS505 see adas505.pro.
;
; INPUTS:
;	DSFULL   - Data file name
;
;	PIPE	 - The IDL unit number of the bi-directional pipe to the
;		   ADAS505 FORTRAN process.
;
;       UTITLE   - Optional comment by user
;
;	GRPSCAL	 - Integer; 0 if default scaling required 1 if user
;		   specified scaling to be used.
;
;	XMIN	 - String; User sepcified x-axis minimum, number as string.
;
;	XMAX	 - String; User sepcified x-axis maximum, number as string.
;
;	YMIN	 - String; User specified y-axis minimum, number as string.
;
;	YMAX	 - String; User specified y-axis maximum, number as string.
;
;	HRDOUT	 - Integer; 0 if no hardcopy required, 1 if hardcopy.
;
;	HARDNAME - String; file name for hardcopy output.
;
;	DEVICE	 - String; IDL name for hardcopy output device.
;
;	HEADER	 - ADAS version header information for inclusion in the
;		   graphical output.
;
;       BITFILE - String; the path to the directory containing bitmaps
;                 for the 'escape to series menu' button.
;
; OPTIONAL INPUTS:
;	None.
;
; OUTPUTS:
;       GOMENU  - Int; flag - set to 1 if user has selected 'escape direct
;                 to series menu , otherwise 0
;
; OPTIONAL OUTPUTS:
;	None.
;
; KEYWORD PARAMETERS:
;	FONT	- String; Name of the font to be used for the graphical
;		  output widget.
;
; CALLS:
;	ADAS505_PLOT	ADAS505 graphical output.
;
; SIDE EFFECTS:
;	This routine reads information from FORTRAN via a UNIX pipe.
;
; CATEGORY:
;	Adas system.
;
; WRITTEN:
;       William Osborn, Tessella Support Services plc, 21-Mar-1996
;	Owes much to e1outg.pro
;
; MODIFIED:
;	1.1	William Osborn
;		First Version
;	1.2	William Osborn
;		Added proper header information
; VERSION:
;	1.1	21-03-96
;	1.2	02-04-96
;-
;-----------------------------------------------------------------------------

PRO e5outg, dsfull, pipe, utitle, grpscal, xmin, xmax, ymin, ymax, 	$
	    hrdout, hardname, device, header, bitfile, gomenu, FONT=font

                ;**** Set defaults for keywords ****

  IF NOT (KEYWORD_SET(font)) THEN font = ''


		;**** Declare variables for input ****

    title = " "
    titlx = " "
    titlm = " "
    date = " "
    irz0 = 0
    irz1 = 0
    idz0 = 0
    cdonor = " "
    crecvr = " "
    cfstat = " " 
    itval = 0
    ldef1 = 0
    teva = 0
    din = 0
    xmin = 0.0
    xmax = 0.0
    ymin = 0.0
    ymax = 0.0
    tdfitm = 0
    lfsel = 0
    ldfit = 0
    nmx = 0
    head1 = " "
    head2a = " "
    head2b = " "
    strg = make_array(7, /string, value=" ")
    dummy = 0.0
    sdum = " "
    idum = 0

		;********************************
		;**** Read data from fortran ****
		;********************************

    readf, pipe, format = '(a40)' , sdum
    title = sdum
    readf, pipe, format = '(a120)' , sdum
    titlx = sdum
    readf, pipe, format = '(a80)' , sdum
    titlm = sdum
    readf, pipe, format = '(a8)' , sdum
    date = sdum
    readf, pipe, idum
    irz0 = idum
    readf, pipe, idum
    irz1 = idum
    readf, pipe, idum
    idz0 = idum
    readf, pipe, format = '(a9)' , sdum
    cdonor = sdum
    readf, pipe, format = '(a9)' , sdum
    crecvr = sdum
    readf, pipe, format = '(a10)' , sdum
    cfstat = sdum
    readf, pipe, dummy
    amsra = dummy
    readf, pipe, dummy
    amsda = dummy
    readf, pipe, dummy
    rmass = dummy
    readf, pipe, dummy
    dmass = dummy
    readf, pipe, idum 
    itval = idum

		;**** now declare array dimensions ****

    treva = dblarr(itval) 
    tdeva = dblarr(itval) 
    qtcxa = dblarr(itval)
    for i=0, itval-1 do begin
        readf, pipe, dummy
        treva(i) = dummy 
    endfor
    for i=0, itval-1 do begin
        readf, pipe, dummy
        tdeva(i) = dummy 
    endfor
    for i=0, itval-1 do begin
        readf, pipe, dummy
        qtcxa(i) = dummy 
    endfor
    readf, pipe, idum
    ldef1 = idum
    if (ldef1 eq 1) then begin
        readf, pipe, dummy
	xmin = dummy
        readf, pipe, dummy
	xmax = dummy
        readf, pipe, dummy
	ymin = dummy
        readf, pipe, dummy
	ymax = dummy
    endif 

    readf, pipe, idum
    lfsel = idum
    if (lfsel eq 1) then begin
        readf, pipe, idum
	ldfit = idum
        readf, pipe, idum
	nmx = idum

 		;**** declare arrays ****

        qtcxm = fltarr(nmx)
        tfevm = fltarr(nmx)
        for i = 0, nmx-1 do begin
	    readf, pipe, dummy
            qtcxm(i) = dummy 
        endfor
        for i = 0, nmx-1 do begin
 	    readf, pipe, dummy 
            tfevm(i) = dummy
        endfor
    endif
    readf, pipe, idum
    ldfit = idum
    for i = 0, 3 do begin
        readf, pipe, format='(a28)', sdum 
        strg(i) = sdum
    endfor
    readf, pipe, format = '(a32)', sdum
    head1 = sdum
    readf, pipe, format = '(a32)', sdum
    head2 = sdum
    readf, pipe, format = '(a45)', sdum
    head3a = sdum
    readf, pipe, format = '(a45)', sdum
    head3b = sdum
		;***********************
		;**** Plot the data ****
		;***********************

    adas505_plot, dsfull, 						$
  		  title , titlx, titlm, utitle, date, irz0, irz1, idz0,	$
  		  cdonor, crecvr, cfstat, amsra, amsda, rmass, dmass,   $
		  itval, treva, tdeva, qtcxa, qtcxm, tfevm, 		$
                  ldef1, xmin, xmax, ymin, ymax, 			$
   		  lfsel, ldfit, nmx, 					$
   		  strg, head1, head2, head3a, head3b,			$
		  hrdout, hardname, device, header, bitfile, gomenu,	$
                  FONT=font

END
