; Copyright (c) 1995, Strathclyde University 
; SCCS info: Module @(#)$Header: /home/adascvs/idl/adas5xx/adas505/e5ispf.pro,v 1.2 2004/07/06 13:42:52 whitefor Exp $	Date  $Date: 2004/07/06 13:42:52 $
;
;+
; PROJECT:
;       ADAS IBM MVS to   UNIX conversion
;
; NAME:
;	E5ISPF
;
; PURPOSE:
;	IDL user interface and communications with ADAS505 FORTRAN
;	process via pipe.
;
; EXPLANATION:
;	The routine begins by reading information from the ADAS505
;	FORTRAN process via a UNIX pipe.  Then part of the ADAS505
;	IDL user interface is invoked to determine how the user
;	wishes to process the input dataset.  When the user's
;	interactions are complete the information gathered with
;	the user interface is written back to the FORTRAN process
;	via the pipe.  Communications are to the FORTRAN subroutine
;	E5ISPF.
;
; USE:
;	The use of this routine is specific to ADAS505, see adas505.pro.
;
; INPUTS:
;	PIPE	- The IDL unit number of the bi-directional pipe to the
;		  ADAS505 FORTRAN process.
;
;	PROCVAL	- A structure which determines the initial settings of
;		  the processing options widget.  The initial value is
;		  set in adas505.pro.  If adas505.pro passes a blank 
;		  dummy structure of the form {NMET:-1} into PROCVAL then
;		  PROCVAL is reset to a default structure.
;
;		  The PROCVAL structure is;
;       procval = {	nmet  	: 	0 ,             		$
;			title 	: 	'',				$
;			nbsel 	: 	0 ,				$
;			ibsel 	: 	0 ,     			$
;			udonor 	: 	str_arr,			$
;			urecvr 	: 	str_arr,			$
;			ufstat 	: 	str_arr,			$
;			amsra   :       flt_arr,			$
;			amsda   :       flt_arr,			$
;			rmass   :       0.0,  				$
;			dmass   :       0.0,  				$
;                	ifout 	: 	1, 				$
;			ldfit 	: 	0,				$
;			maxtr  	: 	0,              		$
;                	maxtd  	: 	0, 				$
;			trin   	: 	rec_temp_arr,			$
;			tdin   	: 	don_temp_arr,			$
;			ifsel 	: 	0,				$
;			tolval	: 	5                		}
;
;		  See cw_adas505_proc.pro for a full description of this
;		  structure.
;	
;
;	DSFULL	- String; The full system file name of the input 
;		  dataset selected by the user for processing.
;
;       BITFILE - String; the path to the dirctory containing bitmaps
;                 for the 'escape to series menu' button.
;
; OPTIONAL INPUTS:
;	None
;
; OUTPUTS:
;	PROCVAL	- On output the structure records the final settings of
;		  the processing selections widget if the user pressed the
;		  'Done' button, otherwise it is not changed from input.
;
;	LPEND	- Integer, 0 if the user pressed the 'Done' button
;		  or 1 if the user pressed 'Cancel'.  Maps directly onto
;		  the logical variable LPEND in ADAS505 FORTRAN.
;
;       GOMENU  - Int; flag - set to 1 if user has selected 'escape direct
;                 to series menu , otherwise 0
;
; OPTIONAL OUTPUTS:
;	None
;
; KEYWORD PARAMETERS:
;	FONT_LARGE - The name of a larger font.
;
;	FONT_SMALL - The name of a smaller font.
;
;	EDIT_FONTS - A structure of two fonts used in the table editor
;		     adas_edtab.pro, {font_norm:'',font_input:''}.
;
; CALLS:
;	ADAS505_PROC	Invoke the IDL interface for ADAS505 data
;		 	processing options/input.
;
; SIDE EFFECTS:
;	Two way communications with ADAS505 FORTRAN via the
;	bi-directional UNIX pipe.
;
; CATEGORY:
;	Adas system.
;
; WRITTEN:
;       William Osborn, Tessella Support Services plc, 19-March-1996
;               (e1ispf.pro cannibalisation)
; MODIFIED:
;	1.1	William Osborn
;		First Version
;	1.2	William Osborn
;		Added proper header information
; VERSION:
;	1.1	19-03-96
;	1.2	02-04-96
;-
;-----------------------------------------------------------------------------

PRO e5ispf, pipe, lpend, procval, dsfull, gomenu, bitfile,		$
            FONT_LARGE=font_large, FONT_SMALL=font_small, 		$
            EDIT_FONTS=edit_fonts


                ;**** Set defaults for keywords ****

    IF NOT (KEYWORD_SET(font_large)) THEN font_large = ''
    IF NOT (KEYWORD_SET(font_small)) THEN font_small = ''
    IF NOT (KEYWORD_SET(edit_fonts)) THEN 				$
    edit_fonts = {font_norm:'', font_input:''}

		;********************************************
		;****     Declare variables for input    ****
		;**** arrays wil be declared after sizes ****
		;**** have been read.                    ****
                ;********************************************

    lpend = 0
    nstore= 0
    ntrdim = 0
    ntddim = 0
    ndtin = 0
    nbsel = 0
    input = 0
    rinput=0.0

		;********************************
		;**** Read data from fortran ****
		;********************************

    readf, pipe, input
    nstore = input
    readf, pipe, input
    ntrdim = input
    readf, pipe, input
    ntddim = input
    readf, pipe, input
    ndtin = input
    readf, pipe, input
    nbsel = input

		;******************************************
		;**** Now can define other array sizes ****
		;******************************************

    cdonor = strarr(nbsel)
    crecvr = strarr(nbsel)
    cfstat = strarr(nbsel)
    amsra  = dblarr(nbsel)
    amsda  = dblarr(nbsel)
    itra   = intarr(nbsel)
    itda   = intarr(nbsel)
    trvals = dblarr(ntrdim, 3, nbsel)
    tdvals = dblarr(ntddim, 3, nbsel)

		;********************************
                ;**** Read data from fortran ****
                ;********************************

    next_item = 0.0
    sdum = ' '
    for j=0, nbsel-1 do begin
        readf, pipe, sdum
        cdonor(j) = sdum
    endfor
    for j=0, nbsel-1 do begin
        readf, pipe, sdum
        crecvr(j) = sdum
    endfor
    for j=0, nbsel-1 do begin
        readf, pipe, sdum
        cfstat(j) = sdum
    endfor
    for j=0, nbsel-1 do begin
        readf, pipe, rinput
        amsra(j) = rinput
    endfor
    for j=0, nbsel-1 do begin
        readf, pipe, rinput
        amsda(j) = rinput
    endfor
    for j=0, nbsel-1 do begin
        readf, pipe, input
        itra(j) = input
    endfor
    for j=0, nbsel-1 do begin
        readf, pipe, input
        itda(j) = input
    endfor
    for k = 0, nbsel-1 do begin 
        for j = 0, 2 do begin
	    for i = 0, itra(k)-1 do begin
                readf, pipe, next_item
                trvals(i,j,k) = next_item
            endfor
        endfor
    endfor
    for k = 0, nbsel-1 do begin 
        for j = 0, 2 do begin
	    for i = 0, itda(k)-1 do begin
                readf, pipe, next_item
                tdvals(i,j,k) = next_item
            endfor
        endfor
    endfor

		;*******************************************
		;**** Set default value if non provided ****
		;*******************************************

    if (procval.nmet lt 0) then begin
        str_arr = strarr(nbsel)
	flt_arr = fltarr(nbsel)
        rec_temp_arr = fltarr(ndtin)
        don_temp_arr = fltarr(ndtin)
        procval = {	nmet  	: 	0 ,             		$
			title 	: 	'',				$
			nbsel 	: 	0 ,				$
			ibsel 	: 	0 ,     			$
			udonor 	: 	str_arr,			$
			urecvr 	: 	str_arr,			$
			ufstat 	: 	str_arr,			$
			amsra   :       flt_arr,			$
			amsda   :       flt_arr,			$
			rmass 	: 	amsra(0),			$
			dmass	:	amsda(0),			$
                	ifout 	: 	1, 				$
			ldfit 	: 	0,				$
			maxtr  	: 	0,              		$
                	maxtd  	: 	0, 				$
			trin   	: 	rec_temp_arr,			$
			tdin   	: 	don_temp_arr,			$
			ifsel 	: 	0,				$
			tolval	: 	5                		}
    endif

		;****************************************
		;**** Pop-up processing input widget ****
		;****************************************

  adas505_proc, procval, dsfull, nstore, 				$
		ntrdim, ntddim, ndtin, nbsel, 				$
		cdonor, crecvr, cfstat, amsra, amsda,			$
		itra, itda, trvals, tdvals, action, bitfile,		$
		FONT_LARGE=font_large, FONT_SMALL=font_small, 		$
		EDIT_FONTS=edit_fonts

		;*******************************************
		;****  Act on the event from the widget ****
                ;**** There are three  possible actions ****
		;**** 'Done', 'Cancel'and 'Menu'.       ****
		;*******************************************

    if action eq 'Done' then begin
        lpend = 0
    endif else if action eq 'Menu' then begin
        lpend = 0
        gomenu = 1
    endif else begin
        lpend = 1
    endelse

		;*******************************
		;**** Write data to fortran ****
		;*******************************

    printf, pipe, lpend   
    title_short = strmid(procval.title,0,39) 	
    printf, pipe, title_short, format='(a40)' 
    printf, pipe, procval.ibsel  			
    printf, pipe, procval.rmass
    printf, pipe, procval.dmass
    printf, pipe, procval.ifout
    printf, pipe, procval.ldfit
    if (procval.ldfit eq 0) then begin
        printf, pipe, procval.maxtr   
    endif else begin 
        printf, pipe, procval.maxtd
    endelse
    printf, pipe, procval.trin
    printf, pipe, procval.tdin
    printf, pipe, procval.ifsel
    printf, pipe, procval.tolval

END
