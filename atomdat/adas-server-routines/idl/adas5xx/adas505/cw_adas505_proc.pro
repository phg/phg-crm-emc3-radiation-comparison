; Copyright (c) 1995, Strathclyde University.
; SCCS info: Module @(#)$Header: /home/adascvs/idl/adas5xx/adas505/cw_adas505_proc.pro,v 1.7 2004/07/06 12:52:35 whitefor Exp $	Date  $Date: 2004/07/06 12:52:35 $
;+
; PROJECT:
;       ADAS IBM MVS to   UNIX conversion
;
; NAME:
;	CW_ADAS505_PROC()
;
; PURPOSE:
;	Produces a widget for ADAS505 processing options/input.
;
; EXPLANATION:
;	This function creates a compound widget consisting of :-
;	   a text widget holding an editable 'Run title', 
;	   the dataset name/browse widget cw_adas_dsbr,
;	   a widget to select the data block for analysis,
;	   a widget to request a mimax fit and enter a tolerance for it,
;	   a table widget for receiver/donor temperature data cw_adas_table,
;	   buttons to enter default values into the table, 
;	   a message widget, 
;	   an 'Escape to series menu', a 'Cancel' and a 'Done' button.
;
;	The compound widgets included in this widget are self managing.
;	This widget only manages events from the two 'Defaults' buttons,
;	the 'Cancel' button and the 'Done' button and the
;       'escape to series menu' button.
;
; USE:
;	This widget is specific to ADAS505, see adas505_proc.pro
;	for use.
;
; INPUTS:
;	TOPPARENT- Long integer, ID of parent widget.
;
;	DSFULL	- String; The full system file name of the input 
;		  dataset selected by the user for processing.
;
;	NSTORE  - Integer; maximum number of data blocks allowed.
;
;	NTRDIM	- Integer; maximum number of receiver temperatures allowed.
;
;	NTDDIM	- Integer; maximum number of donor temperatures allowed.
;
;	NDTIN	- Integer; number of donor/receiver pairs selected
;
;	NBSEL   - Integer; number of data blocks included in this file
;
;	CDONOR  - String;  Donor identification string
;
;	CRECVR	- String;  Receiver identification string
;
;	CFSTAT	- String;  Final state specification
;
;       AMSRA	- Double : INPUT FILE - RECEIVER ATOMIC MASS (NSTORE)
;
;  	AMSDA   - Double : INPUT FILE - DONOR    ATOMIC MASS (NSTORE)
;
;  	ITRA  	- Integer : INPUT FILE - NO. OF RECEIVER TEMPERATURES (NSTORE)
;
;    	ITDA   	- Integer : INPUT FILE - NO. OF DONOR    TEMPERATURES (NSTORE)
;
;	TRVALS   - Double array : Receiver electron temperatures - 
;				 1st dimension - temperature index
;				 2nd dimension - units 1 - Kelvin
;						       2 - eV
;						       3 - Reduced
;				 3rd dimension - data block index
;
;	TDVALS	 - Double array : Receiver electron temperatures - 
;				 1st dimension - temperature index
;				 2nd dimension - units 1 - Kelvin
;						       2 - eV
;						       3 - Reduced
;				 3rd dimension - data block index
;	The inputs map exactly onto variables of the same
;	name in the ADAS505 FORTRAN program.
;
;       BITFILE - String; the path to the directory containing bitmaps
;                 for the 'escape to series menu' button.
;
; OPTIONAL INPUTS:
;	None
;
; OUTPUTS:
;	The return value of this function is the ID of the declared widget.
;
; OPTIONAL OUTPUTS:
;	None
;
; KEYWORD PARAMETERS:
;	PROCVAL	- A structure which determines the initial settings of
;		  the processing options widget.
;
;		  The PROCVAL structure is;
;       procval = {	nmet  	: 	0 ,             		$
;			title 	: 	'',				$
;			nbsel 	: 	0 ,				$
;			ibsel 	: 	0 ,     			$
;			udonor 	: 	str_arr,			$
;			urecvr 	: 	str_arr,			$
;			ufstat 	: 	str_arr,			$
;			amsra   :       flt_arr,			$
;			amsda   :       flt_arr,			$
;			rmass   :       0,  				$
;			dmass   :       0,  				$
;                	ifout 	: 	1, 				$
;			ldfit 	: 	0,				$
;			maxtr  	: 	0,              		$
;                	maxtd  	: 	0, 				$
;			trin   	: 	rec_temp_arr,			$
;			tdin   	: 	don_temp_arr,			$
;			ifsel 	: 	0,				$
;			tolval	: 	5                		}
;
;
;		NMET    Flag which defines whether or not default values
;			exist or not. (< 0 if no values exist)
; 		TITLE	Entered general title for program run
;		IBSEL   Selected data block
;		NBSEL   Number of blocks in data file
;		UDONOR	Donor ion identification string
;		URECVR  Receiver ion identification string
;		UFSTAT  Final state specifiaction
;		AMSRA	Atomic mass of receiver
;		AMSDA   Atomic mass of donor
;		RMASS   Isotopic atomic mass of receiver
;		DMASS   Isotopic atomic mass of donor
;		IFOUT   Index indicating which units are being used
;		LDFIT   Flag for whether receiver or donor selected. 
;			0 = receiver, 1 = donor
;		MAXTR   Number of receiver temperature values selected
;		MAXTD   Number of donor temperature values selected
;		TRIN    User supplied receiver temperature values for fit.
;		TDIN 	User supplied donor temperature values for fit.
;		IFSEL   Flag as to whether polynomial fit is chosen
;		TOLVAL  Tolerance required for goodness of fit if
;			polynomial fit is selected.
;
;		All of these structure elements map onto variables of
;		the same name in the ADAS505 FORTRAN program.
;
;
;	UVALUE	- A user value for the widget. Default 0.
;
;	FONT_LARGE - The name of a larger font.  Default current system
;		     font
;
;	FONT_SMALL - The name of a smaller font. Default current system
;		     font.
;
;	EDIT_FONTS - A structure of two fonts used in the table editor
;		     adas_edtab.pro, {font_norm:'',font_input:''}.  Defaults
;		     to current system font.
;
;	NUM_FORM   - String; Numeric format to use in tables.  Default
;			'(E10.3)'
;		     
;
; CALLS:
;	POPUP		Popup warning window with buttons.
;	CW_ADAS_DSBR	Dataset name and comments browsing button.
;	CW_ADAS_TABLE	Adas data table widget.
;	CW_ADAS_SEL	Adas multiple selection widget.
;	CW_SINGLE_SEL	Adas scrolling table and selection widget.
;	CW_OPT_VALUE    Adas option selection widget with required input 
;			value. 
;
;	See side effects for other related widget management routines.
;
; SIDE EFFECTS:
;
;	The following widget management routines are included in this file;
;	PROC505_GET_VAL()	Returns the current PROCVAL structure.
;	PROC505_EVENT()		Process and issue events.
;	
; CATEGORY:
;	Compound Widget
;
; WRITTEN:
;	William Osborn, Tessella Support Services, plc, 19-Mar-1996
;        Addapted from cw_adas501_proc.pro
;
; MODIFIED:
;	1.1	William Osborn
;              	First Written
;	1.2     William Osborn
;		Minor changes to structure initialisation
;	1.3	Tim Hammond
;		COrrected minor syntax error
;	1.4	William Osborn
;		Added selector for receiver/donor dependant variable and
;		modified 'Default temperature' button accordingly
;       1.5     William Osborn
;               Changed layout of atomic mass selection boxes
;       1.6     Martin O'Mullane
;               Trapped failure when number of temperatures is same as 
;               the max number allowed.
;	1.7	Richard Martin
;		IDL 5.5 fixes
;               
;
; VERSION:
;	1.1	19-03-96
;	1.2	01-04-96
;	1.3	02-04-96
;	1.4	01-05-96
;       1.5	14-10-96
;       1.6	07-11-97
;	1.7	11-03-02
;
;-
;-----------------------------------------------------------------------------

FUNCTION proc505_get_val, id

                ;**** Return to caller on error ****

    ON_ERROR, 2

		 ;***************************************
                 ;****     Retrieve the state 	     ****
		 ;**** Get first_child widget id     ****
		 ;**** because state is stored there ****
		 ;***************************************

    first_child = widget_info(id, /child)
    widget_control, first_child, get_uvalue = state

		;***********************************
		;**** Get run title from widget ****
		;**** Then centre in in string  ****
		;**** of 40 characters 		****
		;***********************************

    widget_control, state.runid, get_value=title
    title(0)=strtrim(title(0))
    title_len = strlen(title(0))
    if (title_len gt 40 ) then begin 
	title(0) = strmid(title(0),0,38)
        widget_control, state.messid, set_value="Title too long - truncated "
        widget_control, state.runid, set_value=title
	wait ,1
    endif
    pad = (40 - title_len)/2 
    spaces = '                                         '
    title(0) = strmid(spaces,0,(pad-1)) + title(0) + strmid(spaces,0,(pad-1))

		;****************************************************
		;**** Get new receiver data from table widget ****
		;****************************************************

    widget_control, state.tempid, get_value=tempval
    tabledata = tempval.value
    ifout = tempval.units + 1

		;*******************************************
		;**** Get whether donor or receiver fit ****
		;*******************************************

    widget_control, state.ldfitid, get_value=ldfit

		;**** Copy out temperature values ****

    trin = dblarr(state.ntrdim)
    tdin = dblarr(state.ntddim)
    blanks = where(strtrim(tabledata(0,*),2) eq '')

		;***********************************************
		;**** next line assumes that all blanks are ****
		;**** at the end of the columns             ****
		;***********************************************

    if blanks(0) ge 0 then maxtr=blanks(0) else maxtr=state.ntrdim

		;*************************************************
                ;**** Only perform following if there is 1 or ****
                ;**** more value present in the table         ****
		;*************************************************

    if (maxtr ge 1) then begin
        trin(0:maxtr-1) = double(tabledata(0,0:maxtr-1))
    endif

		;**** Now do same for donors ****

    blanks = where(strtrim(tabledata(4,*),2) eq '')
    if blanks(0) ge 0 then maxtd=blanks(0) else maxtd=state.ntddim
    if (maxtd ge 1) then begin
        tdin(0:maxtd-1) = double(tabledata(4,0:maxtd-1))
    endif

		;**** Fill in the rest with zero ****

    if maxtr lt state.ntrdim then begin
        trin(maxtr:state.ntrdim-1) = double(0.0)
    endif
    if maxtd lt state.ntddim then begin
        tdin(maxtd:state.ntddim-1) = double(0.0)
    endif 

		;*************************************************
		;**** Get selection of polyfit from widget    ****
		;*************************************************

    widget_control, state.optid, get_uvalue=polyset, /no_copy
    ifsel = polyset.optionset.option[0]
    if (num_chk(polyset.optionset.value[0]) eq 0) then begin
       tolval = (polyset.optionset.value[0])
    endif else begin
       tolval = -999
    endelse
    widget_control, state.optid, set_uvalue=polyset, /no_copy

		;***********************************************
		;**** get new index for data block selected ****
		;***********************************************

    widget_control, state.indexid, get_value=select_block
    ibsel = select_block  

		;***********************************************
		;**** write selected values to PS structure ****
		;***********************************************
;		  The PROCVAL structure is;
       ps =	 {	nmet  	: 	0 ,             		$
			title 	: 	title,				$
			nbsel 	: 	state.nbsel,			$
			ibsel 	: 	ibsel ,     			$
			udonor 	: 	state.udonor,			$
			urecvr 	: 	state.urecvr,			$
			ufstat 	: 	state.ufstat,			$
			amsra   :       state.amsra,			$
			amsda   :       state.amsda,			$
			rmass   :	state.rmass,			$
			dmass   :	state.dmass,			$
                	ifout 	: 	ifout, 				$
			ldfit 	: 	ldfit,				$
			maxtr  	: 	maxtr,              		$
                	maxtd  	: 	maxtd, 				$
			trin   	: 	trin,				$
			tdin   	: 	tdin,				$
			ifsel 	: 	ifsel,				$
			tolval	: 	tolval                		}


    widget_control, first_child, set_uvalue=state, /no_copy

    RETURN, ps

END

;-----------------------------------------------------------------------------

FUNCTION proc505_event, event

                ;**** Base ID of compound widget ****

    parent=event.handler

		;**********************************************
                ;**** Retrieve the user value state        ****
		;**** Get id of first_child widget because ****
		;**** user value "state" is stored there   ****
		;**********************************************

    first_child = widget_info(parent, /child)
    widget_control, first_child, get_uvalue=state,/no_copy
  
		;*********************************
		;**** Clear previous messages ****
		;*********************************

    widget_control, state.messid, set_value=' '

                ;************************
                ;**** Process events ****
                ;************************

    CASE event.id OF

                ;*************************************
                ;****      Select index           ****
                ;*************************************

    	state.indexid: begin

                ;**** Get current index select value ****

            widget_control, state.indexid, get_value=ibsel
            ntrdim = state.ntrdim
            ntddim = state.ntddim
            ntrval = state.itra(ibsel)
            ntdval = state.itda(ibsel)

                ;**** Get current table widget value ****

            widget_control, state.tempid, get_value=tempval

                ;**** Copy new ibsel values into structure   ****
		;**** Don't multiply by sfact since we reset ****
		;**** rmass and dmass a few lines from here  ****

            if (ntrval gt 0) then begin
                tempval.value(1,0:ntrval-1 ) =                               $
                strtrim(string(state.trvals(0:ntrval-1,0,ibsel),              $
                format=state.num_form),2)
                tempval.value(2,0:ntrval-1) =                                $
                strtrim(string(state.trvals(0:ntrval-1,1,ibsel),              $
                format=state.num_form),2)
                tempval.value(3,0:ntrval-1) =                                $
                strtrim(string(state.trvals(0:ntrval-1,2,ibsel),              $
                format=state.num_form),2)
                if (ntdval gt 0) then begin
                    tempval.value(5,0:ntdval-1) =                            $
                    strtrim(string(state.tdvals(0:ntdval-1,0,ibsel),          $
                    format=state.num_form),2)
                    tempval.value(6,0:ntdval-1) =                            $
                    strtrim(string(state.tdvals(0:ntdval-1,1,ibsel),          $
                    format=state.num_form),2)
                    tempval.value(7,0:ntdval-1) =                            $
                    strtrim(string(state.tdvals(0:ntdval-1,2,ibsel),          $
                    format=state.num_form),2)

                ;**** fill rest of table with blanks ****

                    if (ntdval lt ntrdim) then begin
                        tempval.value(5,ntdval:ntrdim-1) = ' '
                        tempval.value(6,ntdval:ntrdim-1) = ' '
                        tempval.value(7,ntdval:ntrdim-1) = ' '
                    endif
                endif
                if (ntrval lt ntrdim ) then begin
                    tempval.value(1,ntrval:ntrdim-1 ) = ' '
                    tempval.value(2,ntrval:ntrdim-1 ) = ' '
                    tempval.value(3,ntrval:ntrdim-1 ) = ' '
                endif

                ;**** Copy new data to table widget   ****
                ;**** Reset state.ibsel               ****
		;**** Change maximum length of output ****
                widget_control, state.tempid, set_value=tempval
                state.ibsel = ibsel
		state.maxtr = ntrval
		state.maxtd = ntdval

		;**** Reset the atomic mass and isotopic mass ****
		widget_control, state.domaidla,				$
		set_value=string(state.amsda(ibsel),format=state.num_form)
		widget_control, state.remaidla,				$
		set_value=string(state.amsra(ibsel),format=state.num_form)
		state.rmass=state.amsra(ibsel)
		state.dmass=state.amsda(ibsel)
		widget_control, state.domaid,				$
			set_value=string(state.dmass,format=state.num_form)
		widget_control, state.remaid,				$
			set_value=string(state.rmass,format=state.num_form)
	
            endif
        end

		;****************************************
		;**** Receiver isotopic mass changed ****
		;****************************************

	state.remaid: begin

		;*** Check to see if isotopic mass is valid ***

	    widget_control, state.remaid, get_value=sget
	    ibsel=state.ibsel
	    if(num_chk(sget(0)) eq 0) then begin
		rmass = float(sget(0))
	    endif else begin
	  	error=1
		message='Error: Receiver isotopic atomic mass invalid'
		rmass=state.amsra(ibsel)
		widget_control, state.remaid, 				$
			set_value=string(rmass,format=state.num_form)
	    endelse
                ;**** Get current table widget value ****

            widget_control, state.tempid, get_value=tempval

            ntrval = state.itra(ibsel)
	    ntrdim = state.ntrdim
	    sfact=rmass/state.amsra(ibsel)
            if (ntrval gt 0) then begin
                tempval.value(1,0:ntrval-1 ) =                               $
                strtrim(string(state.trvals(0:ntrval-1,0,ibsel)*sfact,       $
                format=state.num_form),2)
                tempval.value(2,0:ntrval-1) =                                $
                strtrim(string(state.trvals(0:ntrval-1,1,ibsel)*sfact,       $
                format=state.num_form),2)
                tempval.value(3,0:ntrval-1) =                                $
                strtrim(string(state.trvals(0:ntrval-1,2,ibsel)*sfact,       $
                format=state.num_form),2)
                if (ntrval lt ntrdim ) then begin
                    tempval.value(1,ntrval:ntrdim-1 ) = ' '
                    tempval.value(2,ntrval:ntrdim-1 ) = ' '
                    tempval.value(3,ntrval:ntrdim-1 ) = ' '
                endif
                ;**** Copy new data to table widget   ****
                widget_control, state.tempid, set_value=tempval
	    endif
	    state.rmass=rmass

	end

		;*************************************
	     	;**** Donor Isotopic Mass Changed ****
		;*************************************

	state.domaid: begin

	    widget_control, state.domaid, get_value=sget
	    ibsel=state.ibsel
	    if(num_chk(sget(0)) eq 0) then begin
		dmass = float(sget(0))
	    endif else begin
	  	error=1
		message='Error: Donor isotopic atomic mass invalid'
		dmass=state.amsda(ibsel)
		widget_control, state.domaid,				$
			set_value=string(dmass,format=state.num_form)
	    endelse
                ;**** Get current table widget value ****

            widget_control, state.tempid, get_value=tempval

	    sfact=dmass/state.amsda(ibsel)
            ntdval = state.itda(ibsel)
	    ntddim = state.ntddim
            if (ntdval gt 0) then begin
                tempval.value(5,0:ntdval-1 ) =                               $
                strtrim(string(state.trvals(0:ntdval-1,0,ibsel)*sfact,       $
                format=state.num_form),2)
                tempval.value(6,0:ntdval-1) =                                $
                strtrim(string(state.trvals(0:ntdval-1,1,ibsel)*sfact,       $
                format=state.num_form),2)
                tempval.value(7,0:ntdval-1) =                                $
                strtrim(string(state.trvals(0:ntdval-1,2,ibsel)*sfact,       $
                format=state.num_form),2)
                if (ntdval lt ntddim ) then begin
                    tempval.value(5,ntdval:ntddim-1 ) = ' '
                    tempval.value(6,ntdval:ntddim-1 ) = ' '
                    tempval.value(7,ntdval:ntddim-1 ) = ' '
                endif
                ;**** Copy new data to table widget   ****
                widget_control, state.tempid, set_value=tempval
	    endif
	    state.dmass=dmass

	end

		;*************************************
		;**** Default temperature button ****
		;*************************************

    	state.deftid: begin

		;**** popup window to confirm overwriting current values ****

	    action= popup(message='Confirm Overwrite values with Defaults',$
		          buttons=['Confirm','Cancel'], font=state.font)

	    if action eq 'Confirm' then begin

		;**** Get current table widget value ****

 	       	widget_control, state.tempid, get_value=tempval

		;**** Get whether donor or receiver fit

		widget_control, state.ldfitid, get_value=ldfit

		;*** let user select the default donor /receiver value ****

               	units = tempval.units+1
		if ldfit eq 0 then begin
	    	    tvalues = strarr(state.maxtd)
	            for i = 0, state.maxtd-1 do begin
                   tvalues(i) =  strcompress(string(tempval.value(4+units,i)))
                    endfor
		    mes = 'Select Default Donor Temperature'
		endif else begin
	    	    tvalues = strarr(state.maxtr)
	            for i = 0, state.maxtr-1 do begin
                   tvalues(i) =  strcompress(string(tempval.value(units,i)))
                    endfor
		    mes = 'Select Default Receiver Temperature'
		endelse
	        action = popup(message=mes, $
			      buttons=tvalues, /column, font=state.font)
                deftemp = action
   
		;***********************************************
		;**** Copy defaults into value structure    ****
		;**** For default values use the existing   ****
		;**** table values and let user modify them ****
		;**** then set all donor values to same     ****
		;**** value  				    ****
		;***********************************************

                deft = tempval
		if ldfit eq 0 then begin
	            maxtr = state.maxtr
	            maxtd = state.maxtr
 	            if maxtr gt 0 then begin
   	           	tempval.value(0,0:maxtr-1) = 			$
		   	strtrim(string(deft.value(units,0:maxtr-1), 	$
                   	format=state.num_form),2)
   	           	tempval.value(4,0:maxtd-1) =  			$
		   	string(deftemp, format=state.num_form)
 	       	    endif
		endif else begin
	            maxtr = state.maxtd
	            maxtd = state.maxtd
 	            if maxtd gt 0 then begin
   	           	tempval.value(0,0:maxtr-1) = 			$
		   	string(deftemp, format=state.num_form)
   	           	tempval.value(4,0:maxtd-1) =  			$
		   	strtrim(string(deft.value(4+units,0:maxtr-1), 	$
                   	format=state.num_form),2)
 	       	    endif
		endelse

		;**** Fill in the rest with blanks ****

 	       if maxtr lt state.ntrdim then begin
                   tempval.value(0,maxtr:state.ntrdim-1) = ''
                   tempval.value(4,maxtd:state.ntddim-1) = ''
 	       endif

		;***************************************
		;**** Copy new data to table widget ****
		;***************************************

 	       widget_control,state.tempid, set_value=tempval
	   endif
      end

		;***********************
		;**** Cancel button ****
		;***********************

    	state.cancelid: new_event = {ID:parent, TOP:event.top, 		$
				     HANDLER:0L, ACTION:'Cancel'}

		;*********************
		;**** Done button ****
		;*********************

    	state.doneid: begin

		;***************************************
		;**** Check all user input is legal ****
		;***************************************

	    error = 0

        	;********************************************
		;*** Have to restore "state" before calls ***
		;*** to proc505_get_val so it can be used ***
		;*** there.				  ***
		;********************************************
		
	    widget_control, first_child, set_uvalue=state, /no_copy
	    widget_control, event.handler, get_value=ps

		;*** reset state variable ***

	    widget_control, first_child, get_uvalue=state, /no_copy

		;**** check temp/density values entered ****

	    if error eq 0 then begin
	 	widget_control, state.tempid, get_value=tabval
		tabvals = 						$
		where(strcompress(tabval.value(0,*), /remove_all) ne '')
		if tabvals(0) eq -1 then begin
		    error = 1
		    message = '**** Error: No donor/receiver temperatures'+ $
                              ' entered ****'
		endif
	    endif

		;*** Check to see if index has been selected ***

	    if error eq 0 and ps.ibsel lt 0 then begin
	        error = 1
	        message='Error: Invalid block selected'
	    endif

		;*** Check to see if sensible tolerance is selected.

	    if (error eq 0 and ps.ifsel eq 1) then begin
	        if (float(ps.tolval) lt 0)  or 				$
                (float(ps.tolval) gt 100) then begin
	            error = 1
	            message='Error: Tolerance for polyfit must be 0-100% '
	        endif
	    endif

		;**** return value or flag error ****

	    if error eq 0 then begin
	        new_event = {ID:parent, TOP:event.top, 			$
                             HANDLER:0L, ACTION:'Done'}
            endif else begin
	        widget_control, state.messid, set_value=message
	        new_event = 0L
            endelse
        end


                ;**** Menu button ****

        state.outid: begin
            new_event = {ID:parent, TOP:event.top, HANDLER:0L,          $
                         ACTION:'Menu'}
        end

    	ELSE: new_event = 0L

    ENDCASE

		;*********************************************
		;*** make "state" available to other files ***
		;*********************************************

    widget_control, first_child, set_uvalue=state,/no_copy

    RETURN, new_event

END

;-----------------------------------------------------------------------------

FUNCTION cw_adas505_proc, topparent, 					$
		dsfull, nstore, ntrdim, ntddim, ndtin, nbsel, 		$
		cdonor, crecvr, cfstat, amsra, amsda,			$
		itra, itda, trvals, tdvals, bitfile, 			$
		procval=procval, uvalue=uvalue, 			$
		font_large=font_large, font_small=font_small, 		$
		edit_fonts=edit_fonts, num_form=num_form

		;**** Set defaults for keywords ****

    if not (keyword_set(uvalue)) then uvalue=0
    if not (keyword_set(font_large)) then font_large='' 
    if not (keyword_set(font_small)) then font_small=''
    if not (keyword_set(edit_fonts)) then 				$
    edit_fonts = {font_norm:'', font_input:''}
    if not (keyword_set(num_form)) then num_form='(E10.3)'
    if not (keyword_set(procval)) then begin
        str_arr  = strarr(nbsel)
	flt_arr   = fltarr(nbsel)
        rec_temp_arr = fltarr(ndtin)
        don_temp_arr = fltarr(ndtin)
        ps = {  nmet  : 0, 						$
		title : '',             				$
		nbsel : 0 , 						$
                ibsel : 0 ,             				$
                udonor: str_arr,        				$
                urecvr: str_arr,        				$
                ufstat: str_arr,       				 	$
		amsra : flt_arr,					$
		amsda : flt_arr,					$
		rmass : 0.0,						$
		dmass : 0.0,						$
		ifout : 1,						$
		ldfit : 0,              				$
		maxtr : 0,						$
		maxtd : 0,						$
                trin  : rec_temp_arr,       				$
                tdin  : don_temp_arr,       				$
                ifsel : 0,              				$
                tolval: '5'						}
    endif else begin
	ps = {  nmet  : procval.nmet,      				$
		title : procval.title,     				$
		nbsel : procval.nbsel,     				$
                ibsel : procval.ibsel,     				$
        	udonor: strarr(n_elements(cdonor)),  			$
        	urecvr: strarr(n_elements(crecvr)),  			$
        	ufstat: strarr(n_elements(cfstat)),  			$
        	amsra : fltarr(n_elements(amsra)),  			$ 
        	amsda : fltarr(n_elements(amsda)),  			$ 
		rmass : procval.rmass,					$
		dmass : procval.dmass,					$
		ifout : procval.ifout,	   				$
		ldfit : procval.ldfit,     				$
		maxtr : procval.maxtr,	   				$
                maxtd : procval.maxtd,      				$
                trin  : procval.trin,       				$
                tdin  : procval.tdin,       				$
                ifsel : procval.ifsel,     				$
                tolval: procval.tolval     				}
    endelse
    ps.udonor = cdonor
    ps.urecvr = crecvr
    ps.ufstat = cfstat
    ps.amsra  = amsra
    ps.amsda  = amsda

                ;*********************************************************
                ;**** Modify certain parameters and results depending ****
                ;**** on the machine being used                       ****
                ;*********************************************************

    machine = GETENV('TARGET_MACHINE')
    if machine eq 'HPUX' then begin
        y_size = 4
        large_font = font_small
    endif else begin
        y_size = 6
        large_font = font_large
    endelse

		;****************************************************
		;*** Assemble receiver and donor temperature data ***
		;****************************************************
		;**** The adas table widget requires data to be *****
		;**** input as strings, so all the numeric data *****
		;**** has to be written into a string array.    *****
		;**** Declare temp. table arrays	        *****
		;**** col 1 has user receiver values         	*****
		;**** col 2 has receiver values from files, 	*****
		;**** which can have one of three possible units*****
		;**** col 5 has user donor values             	*****
		;**** col 6 has donor values from file.       	*****
		;**** which can also have three units		***** 
		;****************************************************

    tabledata = strarr(8,ndtin)

		;***********************************************
		;**** Copy out recvr and donor temperatures ****
		;**** number of temperature and density     ****
		;**** values for this data block            ****
		;***********************************************

    if (ps.ibsel gt nbsel-1) then ps.ibsel = 0
    maxtr = itra(ps.ibsel) 
    maxtd = itda(ps.ibsel)
    if (maxtr gt 0) then begin
        tabledata(0,*) = 						$
	strtrim(string(ps.trin(*),format=num_form),2)
	sfact=ps.rmass/amsra(ps.ibsel)
        tabledata(1,0:maxtr-1) = 					$
        strtrim(string(trvals(0:maxtr-1,0,ps.ibsel)*sfact,format=num_form),2)
        tabledata(2,0:maxtr-1) = 					$
	strtrim(string(trvals(0:maxtr-1,1,ps.ibsel)*sfact,format=num_form),2)
        tabledata(3,0:maxtr-1) = 					$
        strtrim(string(trvals(0:maxtr-1,2,ps.ibsel)*sfact,format=num_form),2)
        tabledata(4,*) = 						$
   	strtrim(string(ps.tdin(*),format=num_form),2)
	sfact=ps.dmass/amsda(ps.ibsel)
        tabledata(5,0:maxtd-1) = 					$
	strtrim(string(tdvals(0:maxtd-1,0,ps.ibsel)*sfact,format=num_form),2)
        tabledata(6,0:maxtd-1) = 					$
	strtrim(string(tdvals(0:maxtd-1,1,ps.ibsel)*sfact,format=num_form),2)
        tabledata(7,0:maxtd-1) = 					$
	strtrim(string(tdvals(0:maxtd-1,2,ps.ibsel)*sfact,format=num_form),2)

		;**** fill rest of table with blanks ****
        blanks = where(ps.trin eq 0.0,count) 
        if count ne 0 then tabledata(0,blanks) = ' ' 
        if maxtr ne ntrdim then begin
           tabledata(1:3,maxtr:ntrdim-1) = ' '
        endif
        blanks = where(ps.tdin eq 0.0) 
        if count ne 0 then tabledata(4,blanks) = ' ' 
        if maxtd ne ntddim then begin
           tabledata(5:7,maxtd:ntddim-1) = ' '
        endif
    endif

		;********************************************************
		;**** Create the 505 Processing options/input window ****
		;********************************************************

		;**** create titled base widget ****

    parent = widget_base(topparent, UVALUE = uvalue, 			$
			 title = 'ADAS505 PROCESSING OPTIONS', 		$
			 EVENT_FUNC = "proc505_event", 			$
			 FUNC_GET_VALUE = "proc505_get_val", 		$
			 /COLUMN)

		;******************************************************
		;**** Create a dummy widget just to hold value of *****
		;**** "state" variable so as not to get confused  *****
		;**** with any other values. Adopt IDL practice   *****
		;**** of using first child widget                 *****
		;******************************************************

    first_child = widget_base(parent)
    topbase = widget_base(first_child,/column)

		;***********************
		;**** add run title ****
		;***********************

    base = widget_base(topbase,/row)
    rc = widget_label(base,value='Title for Run',font=large_font)
    runid = widget_text(base,value=ps.title,xsize=38,font=large_font,/edit)

		;**** add dataset name and browse button ****

    rc = cw_adas_dsbr(topbase,dsfull,font=large_font)

		;**************************************************
		;**** Add a base for index and temp selections ****
		;**************************************************

    imid = widget_base(parent,/ROW)

		;**** Base for index selection and donor / receiver selection
    lbase = widget_base(imid,/column)

		;***************************************************
		;**** add a window to display and select index   ***
		;**** First create data array for table          ***
		;**** then convert to 1D string array(text_table)***
		;**** Call cw_single_sel.pro for options choice  ***
		;***************************************************

		;**** First add a frame ****
    frinid = widget_base(lbase,/column,/frame)

    block_info = strarr(3, nbsel)
    block_info(0,*) = crecvr
    block_info(1,*) = cdonor
    block_info(2,*) = cfstat
    titles = [['Receiver', 'Donor', 'Final State'], $
   	      ['Identity','Identity','Spec.']]
    select_data = text_table(block_info, colhead=titles)
    coltitles = select_data(0:1)
    select_data = select_data(2:nbsel+1)
    indexid = cw_single_sel( frinid, select_data, value=ps.ibsel, 	$
			     title='Select Data Block', 		$
			     coltitles=coltitles, 			$
			     ysize=(y_size+2), font=font_small,		$
			     big_font=large_font)


		;**************************************************
		;**** Add the plot vs. receiver/donor selector ****
		;**************************************************

		;**** Frame
    selbse = widget_base(lbase,/column,/frame)

    lab = widget_label(selbse, value = 'Plot and fit as a function of',	$
			font=large_font)
    ldfitid = cw_bgroup(selbse, ['receiver temperature','donor temperature'],$
			/exclusive, font=large_font, /column,     	$
                        set_value=ps.ldfit, label_left='          ')


                ;************************************************
		;**** Add a base to display and input masses ****
		;************************************************

    samsda  = string(amsda(ps.ibsel),format=num_form)
    samsra  = string(amsra(ps.ibsel),format=num_form)
    sdmass  = string(ps.dmass,format=num_form)
    srmass  = string(ps.rmass,format=num_form)

    massbase= widget_base(parent,/row,/frame)
    massid1 = widget_base(massbase,/column)
    massid2 = widget_base(massbase,/column)
    massid3 = widget_base(massbase,/column)
    massm1  = widget_label(massid1,font=font_large, value='Select Isotopic A.M.')
    massm3  = widget_text(massid2,font=font_small, value='Receiver')
    massm2  = widget_text(massid3,font=font_small, value='Donor')
    massm4  = widget_text(massid1,font=font_small, value='Atomic Mass')
    remaidla= widget_text(massid2,font=font_small, value=samsra)
    domaidla= widget_text(massid3,font=font_small, value=samsda)
    massm7  = widget_text(massid1,font=font_small, 			$
			   value='Isotopic Atomic Mass')
    remaid  = widget_text(massid2,font=font_small, /editable, value=srmass)
    domaid  = widget_text(massid3,font=font_small, /editable, value=sdmass)


		;**********************
		;**** Another base ****
		;**********************

    tablebase = widget_base(imid,/row)

		;************************************************
		;**** base for the table and defaults button ****
		;************************************************

    base = widget_base(tablebase,/column,/frame)

		;********************************
		;**** temperature data table ****
		;********************************

    colhead = [['Output','Input','Output','Input'], ['','','','']]

		;********************************************
		;**** convert FORTRAN index to IDL index ****
		;********************************************

    units = ps.ifout-1 
    unitname = ['Kelvin','eV','Reduced']

		;***********************************************************
		;**** Four columns in the table and three sets of units.****
		;**** Column 1 has the same values for all three        ****
		;**** units but column 2 switches between sets 2,3 & 4  ****
		;**** in the input array tabledata as the units change. ****
		;***********************************************************

    unitind = [[0,0,0],[1,2,3],[4,4,4],[5,6,7]]
    table_title = [ ["Receiver and Donor Temperature Values"],		$
		  ["Receiver             Donor"]]

		;****************************
		;**** table of data   *******
		;****************************

    tempid = cw_adas_table(base, tabledata, units, unitname, unitind, 	$
			   UNITSTITLE = 'Temperature Units', 		$
			   COLEDIT = [1,0,1,0], COLHEAD = colhead, 	$
			   TITLE = table_title, 			$
			   UNITS_PLUS = '',			 	$
			   ORDER = [1,0,0,0], 				$ 
			   LIMITS = [1,1,1,1], CELLSIZE = 12, 		$
			   /SCROLL, ytexsize=y_size, NUM_FORM=num_form, $
			   FONTS = edit_fonts, FONT_LARGE = large_font, $
			   FONT_SMALL = font_small)

		;*************************
		;**** Default buttons ****
		;*************************

    tbase = widget_base(base, /column)
    deftid = widget_button(tbase,value=' Default Temperature Values  ',	$
		           font=large_font)

		;**************************************
		;**** Add polynomial fit selection ****
		;**************************************

    polyset = { option:intarr(1), val:strarr(1)}  ; defined thus because 
						  ; cw_opt_value.pro
                                                  ; expects arrays
    polyset.option = [ps.ifsel]
    polyset.val = [ps.tolval]
    options = ['Fit Polynomial']
    optbase = widget_base(topbase,/frame)
    optid   = cw_opt_value(optbase, options, 				$
			       title='Polynomial Fitting',		$
			       limits = [0,100], 			$
			       value=polyset, font=large_font 		)

		;**** Error message ****

    messid = widget_label(parent,font=large_font, 			$
    value='Edit the processing options data and press Done to proceed')

		;**** add the exit buttons ****

    base = widget_base(parent,/row)
    menufile = bitfile + '/menu.bmp'
    read_X11_bitmap, menufile, bitmap1
    outid = widget_button(base, value=bitmap1)          ;menu button
    cancelid = widget_button(base, value='Cancel', font=large_font)
    doneid = widget_button(base, value='Done', font=large_font)
  
		;*************************************************
		;**** create a state structure for the pop-up ****
		;**** 		     window.                  ****
		;*************************************************

    new_state = { runid		:	runid,  			$
		  messid	:	messid, 			$
		  deftid	:	deftid,				$
		  tempid	:	tempid, 			$
		  optid		:	optid,				$
		  indexid	:	indexid,			$
		  cancelid	:	cancelid,			$
		  doneid	:	doneid, 			$
		  outid		:	outid,				$
		  remaid	:	remaid,				$
		  domaid        :	domaid,				$
		  ldfitid	: 	ldfitid,			$
		  remaidla	:	remaidla,			$
		  domaidla      :	domaidla,			$
		  dsfull	:	dsfull,				$
		  nbsel 	:	nbsel, 				$
		  nstore	:	nstore,				$
		  ntrdim	:	ntrdim,				$
		  ntddim	:	ntddim,				$
		  maxtr		:	maxtr, 				$
		  ibsel		:	ps.ibsel,			$
		  maxtd		:	maxtd, 				$
		  itra		:	itra,				$
		  itda		:	itda,				$
		  trvals	:	trvals,				$
		  tdvals	:	tdvals,				$
		  udonor	:	cdonor,				$
		  urecvr	:	crecvr,				$
		  ufstat	:	cfstat,				$
		  amsra		:	amsra, 				$
		  amsda		:	amsda, 				$
		  rmass   	: 	ps.rmass,			$
		  dmass   	: 	ps.dmass,			$
		  font		:	large_font,			$
		  num_form	:	num_form			}
  
                 ;**** Save initial state structure ****

    widget_control, first_child, set_uvalue=new_state,/no_copy

    RETURN, parent

END
