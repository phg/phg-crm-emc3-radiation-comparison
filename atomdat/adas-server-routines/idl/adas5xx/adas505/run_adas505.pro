;----------------------------------------------------------------------
;+
; PROJECT    :  ADAS
;
; NAME       :  run_adas505
;
; PURPOSE    :  Runs ADAS505 thermal charge exchange calculation.
;
; ARGUMENTS  :  All output arguments will be defined appropriately.
;
;               NAME      I/O    TYPE   DETAILS
; INPUTS     :  adf14      I     str    input cross section file.
;               block      I     long   required block in adf14 file.
;               t_donor    I     double donor temperatures
;               t_receiver I     double receiver temperatures
;               m_donor    I     double donor mass required (defaults to value
;                                                            in adf14 file)
;               m_receiver I     double receiver mass required.
;               adf04      O     str    output patch file for adf04 H-line.
;
; KEYWORDS   :  donor     - use the donor temperatures (default)
;               recevier  - use the receiver temperatures
;               kelvin    - requested temperature in K (default eV)
;               help      - shows this comment section.
;
; OUTPUTS    :  None
;
; NOTES      :  - run_adas505 used the spawn command. Therefore IDL v5.3
;                 cannot be used. Any other version will work.
;
; AUTHOR     :  Martin O'Mullane
;
; DATE       :  07-03-2006
;
; UPDATE     :
;       1.1     Martin O'Mullane
;                 - First version.
;
; VERSION    :
;       1.1    07-03-2006
;-
;----------------------------------------------------------------------

PRO run_adas505, adf14      = adf14,      $
                 block      = block,      $
                 t_donor    = t_donor,    $
                 t_receiver = t_receiver, $
                 m_donor    = m_donor,    $
                 m_receiver = m_receiver, $
                 donor      = donor,      $
                 receiver   = receiver,   $
                 adf04      = adf04,      $
                 kelvin     = kelvin,     $
                 help       = help


; If asked for help

if keyword_set(help) then begin
   doc_library, 'run_adas505'
   return
endif

; First check that we are not using IDL v5.3

if !version.release EQ '5.3' then begin
   message, 'This version of IDL (v5.3) will not work - ' + $
            'change to v5.0, v5.1, v5.2, v5.4, v6.0 or above'
   return
endif


; Check that we get all inputs and that they are correct. Otherwise print
; a message and return to command line.

on_error, 2

if n_elements(adf14) EQ 0 then begin
    message, 'A input cross section file (adf14) is required'
endif

if n_elements(block) eq 0 then message, 'An SCX block must be selected'
parsize=size(block, /N_DIMENSIONS)
if parsize ne 0 then message,'Selection index cannot be an array'
partype=size(block, /TYPE)
if (partype ne 2) and (partype ne 3) then begin
   message,'Selection index must be numeric'
endif else ibsel = LONG(block)


; Temperatures

if n_elements(t_donor) eq 0 then message, 'User requested donor temperatures are missing'

partype=size(t_donor, /type)
if (partype lt 2) or (partype gt 5) then begin
   message,'Temperature must be numeric'
endif else t_donor = DOUBLE(t_donor)

if n_elements(t_receiver) eq 0 then message, 'User requested receiver temperatures are missing'

partype=size(t_receiver, /type)
if (partype lt 2) or (partype gt 5) then begin
   message,'Temperature must be numeric'
endif else t_receiver = DOUBLE(t_receiver)


; Kelvin rather than eV

if keyword_set(kelvin) then t_donor    = t_donor/11605.0
if keyword_set(kelvin) then t_receiver = t_receiver/11605.0

maxtd = n_elements(t_donor)
maxtr = n_elements(t_receiver)

; Calculate with donor or receiver

is_d = keyword_set(donor)
is_r = keyword_set(receiver)

if (is_d + is_r) EQ 0 then begin
  message, 'Using donor temperatures for calculation', /continue
  ldfit = 1
endif

if is_d EQ 1 then ldfit = 1
if is_r EQ 1 then ldfit = 0

if (is_d + is_r) EQ 2 then begin
  message, 'Cannot set both - using donor temperatures for calculation', /continue
  ldfit = 1
endif


; Suitable defaults - force Te to be in eV

ittyp = 2L


; File outputs

if n_elements(adf04) EQ 0 then begin
   output_file = ''
   is_adf04 = 0
endif else begin
   output_file = adf04
   is_adf04 = 1
endelse



; Now run the ADAS505 code

; location of files and launch fortran.

fortdir = getenv("ADASFORT")
spawn, fortdir+'/adas505.out', unit=pipe, /noshell, PID=pid


; Input screen

date = xxdate()
printf, pipe, date[0]

rep = 'NO'
printf, pipe, rep
printf, pipe, adf14


; Data and options screen

; Run calculation and output results

input  = 0
nstore = 0
ntrdim = 0
ntddim = 0
ndtin  = 0
nbsel  = 0
input  = 0
rinput = 0.0


readf, pipe, input
nstore = input
readf, pipe, input
ntrdim = input
readf, pipe, input
ntddim = input
readf, pipe, input
ndtin = input
readf, pipe, input
nbsel = input


cdonor = strarr(nbsel)
crecvr = strarr(nbsel)
cfstat = strarr(nbsel)
amsra  = dblarr(nbsel)
amsda  = dblarr(nbsel)
itra   = intarr(nbsel)
itda   = intarr(nbsel)
trvals = dblarr(ntrdim, 3, nbsel)
tdvals = dblarr(ntddim, 3, nbsel)


next_item = 0.0
sdum = ' '
for j=0, nbsel-1 do begin
    readf, pipe, sdum
    cdonor[j] = sdum
endfor
for j=0, nbsel-1 do begin
    readf, pipe, sdum
    crecvr[j] = sdum
endfor
for j=0, nbsel-1 do begin
    readf, pipe, sdum
    cfstat[j] = sdum
endfor
for j=0, nbsel-1 do begin
    readf, pipe, rinput
    amsra[j] = rinput
endfor
for j=0, nbsel-1 do begin
    readf, pipe, rinput
    amsda[j] = rinput
endfor
for j=0, nbsel-1 do begin
    readf, pipe, input
    itra[j] = input
endfor
for j=0, nbsel-1 do begin
    readf, pipe, input
    itda[j] = input
endfor
for k = 0, nbsel-1 do begin
    for j = 0, 2 do begin
        for i = 0, itra[k]-1 do begin
            readf, pipe, next_item
            trvals[i,j,k] = next_item
        endfor
    endfor
endfor
for k = 0, nbsel-1 do begin
    for j = 0, 2 do begin
        for i = 0, itda[k]-1 do begin
            readf, pipe, next_item
            tdvals[i,j,k] = next_item
        endfor
    endfor
endfor

printf, pipe, 0L


; Use mass as input but default to one in dataset

if n_elements(m_donor) EQ 0 then dmass = amsda[ibsel-1] else dmass = m_donor
if n_elements(m_receiver) EQ 0 then rmass = amsra[ibsel-1] else rmass = m_receiver

t_r = dblarr(ntrdim)
t_d = dblarr(ntrdim)

t_r[0:maxtr-1] = t_receiver
t_d[0:maxtd-1] = t_donor

title_short = ' Generated by run_adas505'
printf, pipe, title_short, format='(a40)'
printf, pipe, ibsel - 1
printf, pipe, rmass
printf, pipe, dmass
printf, pipe, ittyp
printf, pipe, ldfit
if ldfit eq 0 then printf, pipe, maxtr else printf, pipe, maxtd
printf, pipe, t_r
printf, pipe, t_d
printf, pipe, 0L
printf, pipe, 10.0

printf, pipe, 0L


; Output/display screen


printf, pipe, 0L   ; lpend
printf, pipe, 0L   ; grpout

printf, pipe, is_adf04
if is_adf04 EQ 1 then begin
   printf, pipe, 1L
   printf, pipe, adf04
endif

printf, pipe, 0L   ; texout


printf, pipe, 0L
printf, pipe, 1L
printf, pipe, 1L


; Close the pipe once fortran is finished

adas_wait, pid

close, pipe

; Reset temperature to K

if keyword_set(kelvin) then begin
   t_donor    = t_donor * 11605.0
   t_receiver = t_receiver * 11605.0
endif

END
