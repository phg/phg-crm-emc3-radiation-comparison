; Copyright (c) 1995, Strathclyde University .
; SCCS info: Module @(#)$Header: /home/adascvs/idl/adas5xx/adas505/adas505_proc.pro,v 1.3 2004/07/06 10:58:11 whitefor Exp $	Date  $Date: 2004/07/06 10:58:11 $
;+
; PROJECT:
;       ADAS IBM MVS to   UNIX conversion
;
; NAME:
;	ADAS505_PROC
;
; PURPOSE:
;	IDL ADAS user interface, processing options/input.
;
; EXPLANATION:
;	This routine creates and manages a pop-up window which allows
;	the user to select options and input data to control ADAS505
;	processing.
;
; USE:
;	This routine is ADAS505 specific, see e5ispf.pro for how it
;	is used.
;
; INPUTS:
;	PROCVAL	- A structure which determines the initial settings of
;		  the processing options widget.  The value is passed
;		  unmodified into cw_adas505_proc.pro.
;
;		  See cw_adas505_proc.pro for a full description of this
;		  structure.
;
;	DSFULL	- String; The full system file name of the input 
;		          dataset selected by the user for processing.
;
;	NSTORE  - Integer: maximum number of data blocks which can be
;			   read from the data set.
;
;	NTDIM	- Integer : Maximum number of temperatures allowed.
;
;	NDDIM	- Integer : Maximum number of densities allowed.
;
;	NDTIN	- Integer : Maximum number of temperature/density pairs
;		           allowed.
;
;	NBSEL   - Integer : number of data blocks accepted and read-in
;
;	CDONOR  - String;  Donor identification string
;
;	CRECVR	- String;  Receiver identification string
;
;	CFSTAT	- String;  Final state specification
;
;       AMSRA	- Double : INPUT FILE - RECEIVER ATOMIC MASS (NSTORE)
;
;  	AMSDA   - Double : INPUT FILE - DONOR    ATOMIC MASS (NSTORE)
;
;  	ITRA  	- Integer : INPUT FILE - NO. OF RECEIVER TEMPERATURES (NSTORE)
;
;    	ITDA   	- Integer : INPUT FILE - NO. OF DONOR    TEMPERATURES (NSTORE)
;
;	TRVALS   - Double array : Receiver electron temperatures - 
;				 1st dimension - temperature index
;				 2nd dimension - units 1 - Kelvin
;						       2 - eV
;						       3 - Reduced
;				 3rd dimension - data block index
;
;	TDVALS	 - Double array : Receiver electron temperatures - 
;				 1st dimension - temperature index
;				 2nd dimension - units 1 - Kelvin
;						       2 - eV
;						       3 - Reduced
;				 3rd dimension - data block index
;
;       BITFILE - String; the path to the directory containing bitmaps
;                 for the 'escape to series menu' button.
;
; OPTIONAL INPUTS:
;	None
;
; OUTPUTS:
;	PROCVAL	- On output the structure records the final settings of
;		  the processing selections widget if the user pressed the
;		  'Done' button, otherwise it is not changed from input.
;
;	ACT	- String; Either 'Done' or 'Cancel' for the button the
;		  user pressed to terminate the processing options
;		  window.
;
; OPTIONAL OUTPUTS:
;	None
;
; KEYWORD PARAMETERS:
;	FONT_LARGE - The name of a larger font.
;
;	FONT_SMALL - The name of a smaller font.
;
;	EDIT_FONTS - A structure of two fonts used in the table editor
;		     adas_edtab.pro, {font_norm:'',font_input:''}.
;
; CALLS:
;	CW_ADAS505_PROC	Declares the processing options widget.
;	DYNLABEL	Recursively adds dynamic_resize keyword to label
;			widgets for version 4.0.1 and higher of IDL
;	ADAS505_PROC_EV Called indirectly during widget management,
;			routine included in this file.
;	XMANAGER
;
; SIDE EFFECTS:
;       This widget uses a common block PROC505_BLK, to pass the
;       variables VALUE and ACTION between the two routines. 
;
; CATEGORY:
;	Widgets
;
; WRITTEN:
;       William Osborn, Tessella Support Services plc, 19-Mar-1996
;	   Skeleton from adas501_proc
;
; MODIFIED:
;	1.1	William Osborn
;		First Version
;	1.2	William Osborn
;		Added proper header information
;	1.3	William Osborn
;		Added dynlabel procedure
; VERSION:
;	1.1	19-03-96
;	1.2	02-04-96
;	1.3	12-07-96
;-
;-----------------------------------------------------------------------------

PRO adas505_proc_ev, event

    COMMON proc505_blk, action, value

    action = event.action

    CASE event.action OF

		;**** 'Done' button ****

	'Done'  : begin

		;**** Get the output widget value ****

	    widget_control, event.id, get_value=value 
	    widget_control, event.top, /destroy
	end

		;**** 'Cancel' button ****

	'Cancel': widget_control, event.top, /destroy

                ;**** 'Menu' button ****

        'Menu': widget_control, event.top, /destroy

	ELSE:			;**** Do nothing ****

    ENDCASE

END

;-----------------------------------------------------------------------------

PRO adas505_proc, procval, dsfull, nstore, ntrdim, ntddim, ndtin, nbsel,$
		  cdonor,crecvr,cfstat, amsra, amsda,			$
		  itra, itda,						$
               	  trvals, tdvals, act, bitfile, FONT_LARGE=font_large, 	$
                  FONT_SMALL=font_small, EDIT_FONTS=edit_fonts


		;**** declare common variables ****

    COMMON proc505_blk, action, value

		;**** Copy "procval" to common ****

    value = procval

		;**** Set defaults for keywords ****

    IF NOT (KEYWORD_SET(font_large)) THEN font_large = ''
    IF NOT (KEYWORD_SET(font_small)) THEN font_small = ''
    IF NOT (KEYWORD_SET(edit_fonts)) THEN 				$
    edit_fonts = { font_norm:'', font_input:''}
		
                ;**** create base widget ****

    procid = widget_base(TITLE='ADAS505 PROCESSING OPTIONS', 		$
                         XOFFSET=50, YOFFSET=1)

		;**** Declare processing widget ****

    cwid = cw_adas505_proc(procid, dsfull, nstore,  			$
			   ntrdim, ntddim, ndtin, nbsel, 		$
			   cdonor, crecvr, cfstat, amsra, amsda,	$
			   itra, itda, trvals, tdvals, 			$
			   bitfile, PROCVAL=value, 			$
		 	   FONT_LARGE=font_large, FONT_SMALL=font_small,$
			   EDIT_FONTS=edit_fonts)

		;**** Realize the new widget ****

    dynlabel, procid
    widget_control, procid, /realize

		;**** make widget modal ****

    xmanager, 'adas505_proc', procid, event_handler='adas505_proc_ev', 	$
              /modal, /just_reg

		;*** copy value back to procval for return to e5ispf ***

    act = action
    procval = value
 
END

