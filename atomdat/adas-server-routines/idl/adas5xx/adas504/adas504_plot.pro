; Copyright (c) 1996, Strathclyde University.
; SCCS info: Module @(#)$Header: /home/adascvs/idl/adas5xx/adas504/adas504_plot.pro,v 1.2 2004/07/06 10:56:33 whitefor Exp $ Date $Date: 2004/07/06 10:56:33 $
;+
; PROJECT:
;       ADAS IBM MVS to UNIX conversion
;
; NAME:
;	ADAS504_PLOT
;
; PURPOSE:
;	Generates ADAS504 graphical output.
;
; EXPLANATION:
;	This routine creates a window for the display of graphical
;	output a separate routine PLOT504 actually plots a
;	graph.
;
; USE:
;	This routine is specific to ADAS504, see e4outg.pro for
;	example use.
;
; INPUTS:
;	(Most of these inputs map exactly onto variables of the same
;	 name in the FORTRAN code.)
;
;
;	DSFULL  - String; Name of data file 
;
;	TITLE   - String array; titles to be placed above graph
;
;	TITLX   - String; user supplied comment appended to end of title
;
;	TITLM   - String; Information about minimax fitting if selected.
;
;	UTITLE  - String; Optional comment by user
;
;	DATE	- String; Date of graph production
;
;	ESYM 	- String; Symbol of emitting ion
;
;	IZ0	- Integer; Nuclear charge of ionising ion
;
;	IZ1	- Integer; Charge of ionising ion + 1
;
;	CIION 	- String; Radiating ion
;
;	CITYPE	- String; Radiation type
;
;	CIINFO 	- String; Information string
;
;	ITVAL	- Integer; number of user entered temperatures
;
;	TEVA	- Double array; User entered electron temperatures, eV
;
;
;	PZDA	- Double array; Spline interpolated or extrapolated 
;				radiated power coeffts. for user entered 
;				temperatures.
;
;	PZDM	- Double array; Minimax fit values of radiated power  
;				coeffts. at 'tfitm()'
;
;	TFITM	- Double array; Selected temperature values for minmiax fit.
;
;	LDEF1	- Integer; 0 - use user entered graph scales
;			   1 - use default axes scaling
;
;	XMIN    - String; Lower limit for x-axis of graph, number as string.
;
;	XMAX    - String; Upper limit for x-axis of graph, number as string.
;
;	YMIN    - String; Lower limit for y-axis of graph, number as string.
;
;	YMAX    - String; Upper limit for y-axis of graph, number as string.
;
;	LFSEL	- Integer; 0 - No minimax fitting was selected 
;			   1 - Mimimax fitting was selected
;
;	NMX	- Integer; Number of temperatures used for minimax fit
;
;	STRG	- String array; Information regarding current data selected
;
;	HEAD1	- String; header information on radiation
;
;	HEAD2	- String; Header information on radiator 
;
;	HEAD3   - String; Header information on temperatures
;
;	HRDOUT - Integer; 1 if hardcopy output activated, 0 if not.
;
;	HARDNAME- String; Filename for harcopy output.
;
;	DEVICE	- String; IDL name of hardcopy output device.
;
;	HEADER	- String; ADAS version number header to include in graph.
;
; OPTIONAL INPUTS:
;	None.
;
; OUTPUTS:
;	None.
;
; OPTIONAL OUTPUTS:
;	None.
;
; KEYWORD PARAMETERS:
;	FONT	- String; The name of a font to use for text in the
;		  graphical output widget.
;
; CALLS:
;	CW_ADAS_GRAPH	Graphical output widget.
;	PLOT504		Make one plot to an output device for 504.
;	XMANAGER
;
; SIDE EFFECTS:
;	This routine uses a common block to maintain its state PLOT504_BLK.
;
;	One other routine is included in this file;
;	ADAS504_PLOT_EV	Called via XMANAGER during widget management.
;
; CATEGORY:
;	Adas system.
;
; WRITTEN:
;       Hugh Summers, University of Strathclyde,  20/12/1995
;
; MODIFIED:
;	1.1		Hugh Summers
;			First version
;       1.2             William Osborn
;                       Added dynlabel
;	
; VERSION:
;	1.1		20-12-95
;       1.2             04-10-96
;
;-
;----------------------------------------------------------------------------

PRO adas504_plot_ev, event

    COMMON plot504_blk, data, action, win, plotdev, plotfile, 		$
                        fileopen, gomenu

    newplot = 0
    print = 0
    done = 0
		;****************************************
		;**** Set graph and device requested ****
		;****************************************

    CASE event.action OF

	'print'	   : begin
	    newplot = 1
		print = 1
	end

	'done'	   : begin
	    if fileopen eq 1 then begin
	        set_plot, plotdev
	        device, /close_file
	    endif
	    set_plot, 'X'
	    widget_control, event.top, /destroy
	    done = 1
	end

	'bitbutton' : begin
            if fileopen eq 1 then begin
                set_plot, plotdev
                device, /close_file
            end
            set_plot,'X'
            widget_control, event.top, /destroy
            done = 1
            gomenu = 1
        end

	ELSE: 			;do nothing

    ENDCASE

		;*******************************
		;**** Make requested plot/s ****
		;*******************************

    if done eq 0 then begin 		;**** Set graphics device ****
        if print eq 1 then begin
            set_plot, plotdev
            if fileopen eq 0 then begin
                fileopen = 1
                device, filename=plotfile
	        device, /landscape
            endif
        endif else begin
            set_plot, 'X'
            wset, win  
        endelse

		;**** Draw graphics ****

      	plot504, data.x , data.y, data.itval, data.nplots, data.nmx,	$
	         data.title, data.xtitle, data.ytitle,                	$
	         data.strg, data.head1, data.head2, data.head3,        	$
	         data.teva, data.ldef1,                               	$
	         data.xmin, data.xmax, data.ymin, data.ymax 
        if print eq 1 then begin
	    message = 'Plot  written to print file.'
	    grval = {WIN:0, MESSAGE:message}
	    widget_control, event.id, set_value=grval
        endif
    endif

END

;----------------------------------------------------------------------------

PRO adas504_plot, dsfull, title , titlx, titlm, utitle, date, esym, 	$
		  iz0, iz1, ciion, citype, ciinfo, itval,               $
                  teva, pzda, pzdm, tfitm, ldef1, xmin, xmax, 		$
		  ymin, ymax, lfsel, nmx, strg, head1, head2, head3,    $
                  hrdout, hardname, device, header, bitfile, gomenu,	$
		  FONT=font

    COMMON plot504_blk, data, action, win, plotdev, plotfile,		$
		        fileopen, gomenucom

		;*************************************
		;**** Copy input values to common ****
		;*************************************

    plotdev = device
    plotfile = hardname
    fileopen = 0
    gomenucom = gomenu

		;************************************
		;**** Create general graph title ****
		;************************************

    title = strarr(5)
    type = 'ELECTRON TEMPERATURE '
    title(0) = "RADIATED POWER COEFFT. VS " + type  
    if ( strtrim(strcompress(utitle),2)  ne '' ) then begin
        title(0) = title(0) + ': ' + strupcase(strtrim(utitle,2))
    endif
    title(1) = 'ADAS    :' + header
    title(2) = 'FILE     :' + titlx
    if (lfsel eq 1) then begin
        title(3)  = 'MINIMAX : ' + strupcase(titlm)
    endif
    title(4) = 'KEY     : (CROSSES - INPUT DATA) (FULL LINE - SPLINE FIT)'
    if (lfsel eq 1) then title(4) = title(4) + "  (DASH LINE - MINIMAX) "

		;********************************
		;*** Create graph annotation ****
		;********************************        

    strg = strtrim(strg, 2)
    strg(0) = strg(0) +  ' ' + esym
    strg(1) = strg(1) +  strcompress(string(iz0)) 
    strg(2) = strg(2) +  ' ' + ciion
    strg(3) = strg(3) +  ' ' + citype
    strg(4) = strg(4) +  ' ' + ciinfo
    strg(5) = strg(5)

		;*********************************
		;**** Set up Y data for plots ****
		;*********************************

    if (lfsel eq 1) then ydim = nmx else ydim = itval
    y = make_array(2, ydim, /float)
    valid_data = where((pzda gt 1e-37) and (pzda lt 1e+37))
    if (valid_data(0) ge 0) then begin
        y(0, valid_data) = pzda(valid_data)
        if (lfsel eq 1) then begin
            valid_data = where((pzdm gt 1e-37) and (pzdm lt 1e+37))
	    if (valid_data(0) ge 0) then begin
                y(1, valid_data) = pzdm(valid_data)
            endif else begin
                print, "ADAS504 : unable to plot polynomial fit data"
            endelse
        endif
    endif else begin
        print, "ADAS504 : unable to plot spline fit data"
    endelse
    ytitle = "RADIATED POWER COEFFICIENT  (W cm!e3!n)"

		;**************************************
		;**** Set up x axis and x axis title ***
		;**************************************

    if (lfsel eq 1) then xdim = nmx else xdim = itval
    x = fltarr(2, xdim)
    if (lfsel eq 0) then begin
        x(0,0:itval-1) = teva
        nplots = 1
    endif else begin
        x(0,0:itval-1) = teva
        x(1,*)  = tfitm
        nplots = 2
    endelse
    xtitle = type +  " (eV) "

  		;******************************************
		;*** if desired set up user axis scales ***
		;******************************************

    if (ldef1 eq 0) then begin
        xmin = min(x, max = xmax)
        xmin = xmin * 0.9
        xmax = xmax * 1.1
        ymin = min( y, max = ymax)
        ymin = ymin * 0.9
        ymax = ymax *1.1
    endif



		;*************************************
		;**** Create graph display widget ****
		;*************************************

    graphid = widget_base(TITLE='ADAS504 GRAPHICAL OUTPUT', 		$
			  XOFFSET=1, YOFFSET=1)
    device, get_screen_size = scrsz
    xwidth = scrsz(0)*0.75
    yheight = scrsz(1)*0.75
    multiplot = 0
    bitval = bitfile + '/menu.bmp'
    cwid = cw_adas_graph(graphid, print=hrdout, FONT=font,		$
                         xsize=xwidth, ysize=yheight, 			$
                         multiplot=multiplot, bitbutton=bitval)

		;********************************
                ;**** Realize the new widget ****
		;********************************

    dynlabel, graphid
    widget_control, graphid, /realize

		;*****************************************
		;**** Get the id of the graphics area ****
		;*****************************************

    widget_control, cwid, get_value=grval
    win = grval.win

		;*******************************************
		;**** Put the graphing data into common ****
		;*******************************************

    data = { 	Y		:	y,				$
		X		:	x,   				$
		ITVAL		:	itval,    			$
		NPLOTS		:	nplots,  			$
		NMX		:	nmx, 				$
                TITLE		:	title,   			$
		XTITLE		:	xtitle,  			$
		YTITLE		:	ytitle,           		$
		STRG		:	strg,     			$
		HEAD1		:	head1,    			$
		HEAD2		:	head2,             		$
                HEAD3		:	head3,   			$
		TEVA		:	teva,     			$
		LDEF1		:	ldef1,                    	$
		XMIN		:	xmin,     			$
		XMAX		:	xmax,      			$
		YMIN		:	ymin,				$
		YMAX		:	ymax 				}
  
    wset, win
    plot504, x , y, itval, nplots, nmx, title, xtitle, ytitle, 		$
	     strg, head1, head2, head3, teva,              		$
	     ldef1, xmin, xmax, ymin, ymax 


		;***************************
                ;**** make widget modal ****
		;***************************

    xmanager, 'adas504_plot', graphid,					$
	      event_handler='adas504_plot_ev', /modal, /just_reg
    gomenu = gomenucom

END
