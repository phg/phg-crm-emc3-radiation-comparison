; Copyright (c) 1996, Strathclyde University.
; SCCS info: Module @(#)$Header: /home/adascvs/idl/adas5xx/adas504/cw_adas504_out.pro,v 1.2 2004/07/06 12:51:59 whitefor Exp $ Date $Date: 2004/07/06 12:51:59 $
;+
; PROJECT:
;       ADAS IBM MVS to UNIX conversion
;
; NAME:
;	CW_ADAS504_OUT()
;
; PURPOSE:
;	Produces a widget for ADAS504 output options.
;
; EXPLANATION:
;	This function declares a compound widget consisting of the
;	graphical output selection widget cw_adas_gr_sel.pro, and an 
;	output file widget cw_adas_outfile.pro.  
;	The compound widgets cw_adas_gr_sel.pro and cw_adas_outfile.pro
;	included in this file are self managing. The text output
;	file specified in this widget is for tabular (paper.txt)
;	output.  This widget also includes a button for browsing the comments
;       from the input dataset, a 'Cancel' button , 'Done' button and an 
;       'Escape to series menu' button.  This latter is represented by a
;       bitmapped button)
;	This widget only generates events for the 'Done', 'Cancel'
;	and 'Escape to series menu' buttons.
;
; USE:
;	This widget is specific to ADAS504, see adas504_out.pro for use.
;
; INPUTS:
;	PARENT	- Long integer; ID of parent widget.
;
;	DSFULL	- Name of input dataset for this application.
;
;       BITFILE - String; the path to the dirctory containing bitmaps       
;                 for the 'escape to series menu' button.
;
; OPTIONAL INPUTS:
;	None.
;
; OUTPUTS:
;	The return value of this function is the ID of the declared widget.
;
; OPTIONAL OUTPUTS:
;	None.
;
; KEYWORD PARAMETERS:
;	DEVLIST	- A string array;  Each element of the array is the
;		  name of a hardcopy graphics output device.  This
;		  list must mirror the DEVCODE list.  DEVLIST is the
;		  list which the user will see.
;		  e.g ['Post-Script','HP-PCL','HP-GL']
;
;	VALUE	- A structure which determines the initial settings of
;		  the output options widget.  The structure is made
;		  up of two parts.  Each part is the same as the value
;		  structure of one of the two main compound widgets
;		  included in this widget.  See cw_adas_gr_sel and
;		  cw_adas_outfile for more details.  The default value is;
;
;		      { GRPOUT	:	0, 			$
;			GTIT1	:	'', 			$
;			GRPSCAL	:	0, 			$
;			XMIN	:	'',  			$
;			XMAX	:	'',   			$
;			YMIN	:	'',  			$
;			YMAX	:	'',   			$
;			HRDOUT	:	0, 			$
;			HARDNAME:	'', 			$
;			GRPDEF	:	'', 			$
;			GRPFMESS:	'', 			$
;			GRPSEL	:	-1, 			$
;			GRPRMESS:	'', 			$
;			DEVSEL	:	-1, 			$
;			GRSELMESS:	'', 			$
;			TEXOUT	:	0, 			$
;			TEXAPP	:	-1, 			$
;			TEXREP	:	0, 			$
;			TEXDSN	:	'', 			$
;			TEXDEF	:	'',			$
;			TEXMES	:	''  		 	}
;
;		  For CW_ADAS_GR_SEL;
;			GRPOUT	   Integer; Activation button 1 on, 0 off
;			GTIT1	   String; Graph title
;			GRPSCAL	   Integer; Scaling activation 1 on, 0 off
;			XMIN	   String; x-axis minimum, string of number
;			XMAX	   String; x-axis maximum, string of number
;			YMIN	   String; y-axis minimum, string of number
;			YMAX	   String; y-axis maximum, string of number
;			HRDOUT	   Integer; Hard copy activ' 1 on, 0 off
;			HARDNAME   String; Hard copy output file name
;			GRPDEF	   String; Default output file name 
;			GRPFMESS   String; File name error message
;			GRPSEL	   Integer; index of selected graph in GRPLIST
;			GRPRMESS   String; Scaling ranges error message
;			DEVSEL	   Integer; index of selected device in DEVLIST
;			GRSELMESS  String; General error message
;
;		  For CW_ADAS_OUTFILE tabular output (paper.txt);
;			TEXOUT	Integer; Activation button 1 on, 0 off
;			TEXAPP	Integer; Append button 1 on, 0 off, -1 no button
;			TEXREP	Integer; Replace button 1 on, 0 off, 
;                               -1 no button
;			TEXDSN	String; Output file name
;			TEXDEF	String; Default file name
;			TEXMES	String; file name error message
;
;	UVALUE	- A user value for this widget.
;
;	FONT	- String; the name of a font to be used for all text in
;		  this widget.  Default to system default font.
;
; CALLS:
;	CW_ADAS_DSBR	Input dataset name and comments browse button.
;	CW_ADAS_GR_SEL	Graphical output selection widget.
;	CW_ADAS_OUTFILE	Output file name entry widget.
;	See side effects for other related widget management routines.
;
; SIDE EFFECTS:
;
; 	Two other routines are included in this file
;	OUT504_GET_VAL()
;	OUT504_EVENT()
;
; CATEGORY:
;	Compound Widget
;
; WRITTEN:
;       Hugh Summers, University of Strathclyde, 20/12/1995 
;
; MODIFIED:
;	1.1	Hugh Summers
;		First version
;	1.2	William Osborn
;		Removed xsize=n keyword from widget_label command.
;		The dynlabel procedure does not override these
;		assignments and they were taken as being values in pixels
;		on some machines.
;
; VERSION:
;	1.1	20-12-95
;	1.2	01-08-96
;
;-
;-----------------------------------------------------------------------------

FUNCTION out504_get_val, id

                ;**** Return to caller on error ****

    ON_ERROR, 2

                ;**** Retrieve the state ****

    parent = widget_info(id, /parent)
    widget_control, parent, get_uvalue=state, /no_copy

		;**** Get graphical output settings ****

    widget_control, state.grpid, get_value=grselval

		;**** Get text output settings ****

    widget_control, state.paperid, get_value=papos

    os = { 	out504_set, 						$
	   	GRPOUT		:	grselval.outbut, 		$
		GTIT1		:	grselval.gtit1, 	 	$
 		GRPSCAL		:	grselval.scalbut,		$
	   	XMIN		:	grselval.xmin,     		$
		XMAX		:	grselval.xmax,    	 	$
	   	YMIN		:	grselval.ymin,     		$
		YMAX		:	grselval.ymax,    	 	$
	   	HRDOUT		:	grselval.hrdout, 		$
		HARDNAME	:	grselval.hardname,   		$
	   	GRPDEF		:	grselval.grpdef, 		$
		GRPFMESS	:	grselval.grpfmess,   		$
	   	GRPSEL		:	grselval.grpsel, 		$
		GRPRMESS	:	grselval.grprmess,   		$
	   	DEVSEL		:	grselval.devsel, 		$
		GRSELMESS	:	grselval.grselmess, 		$
	   	TEXOUT		:	papos.outbut,  			$
		TEXAPP		:	papos.appbut, 		 	$
	   	TEXREP		:	papos.repbut,  			$
		TEXDSN		:	papos.filename, 	 	$
	   	TEXDEF		:	papos.defname, 			$
		TEXMES		:	papos.message 		 	}

                ;**** Return the state ***

    widget_control, parent, set_uvalue=state, /no_copy

    RETURN, os 

END

;-----------------------------------------------------------------------------

FUNCTION out504_event, event

                ;**** Base ID of compound widget ****

    parent = event.top

                ;**** Retrieve the state ****

    widget_control, parent, get_uvalue=state, /no_copy

		;*********************************
		;**** Clear previous messages ****
		;*********************************

    widget_control, state.messid, set_value=' '

                ;************************
                ;**** Process events ****
                ;************************

    CASE event.id OF

		;***********************
		;**** Cancel button ****
		;***********************

      	state.cancelid: new_event = {ID:parent, TOP:event.top,	 	$
  	  			     HANDLER:0L, ACTION:'Cancel'}

		;*********************
		;**** Done button ****
		;*********************

        state.doneid: begin

		;****************************************************
                ;**** Return the state before checking can start ****
		;**** with the get_value keyword.                ****
		;****************************************************

            widget_control, parent, set_uvalue=state, /no_copy

		;***************************************
		;**** Check for errors in the input ****
		;***************************************

	    error = 0

		;**** Get a copy of the widget value ****

	    widget_control, event.handler, get_value=os
	
		;**** Check for widget error messages ****

	    if os.grpout eq 1 and strtrim(os.grpfmess) ne '' then error=1
	    if os.grpout eq 1 and os.grpscal eq 1 and 			$
	    strtrim(os.grprmess) ne '' then error=1
	    if os.grpout eq 1 and strtrim(os.grselmess) ne '' then error=1
	    if os.texout eq 1 and strtrim(os.texmes) ne '' then error=1

                ;**** Retrieve the state   ****

            widget_control, parent, get_uvalue=state, /no_copy

	    if error eq 1 then begin
	        widget_control, state.messid, set_value= 		$
		'**** Error in output settings ****'
	        new_event = 0L
	    endif else begin
	        new_event = {ID:parent, TOP:event.top,			$
			     HANDLER:0L, ACTION:'Done'}
	    endelse
        end

		;**** Menu button ****

	state.outid: begin
	    new_event = {ID:parent, TOP:event.top, HANDLER:0L,      	$
                         ACTION:'Menu'}
	end

        ELSE: new_event = 0L

    ENDCASE

                ;**** Return the state   ****

          widget_control, parent, set_uvalue=state, /no_copy

    RETURN, new_event

END

;-----------------------------------------------------------------------------

FUNCTION cw_adas504_out, parent, dsfull, DEVLIST=devlist,		 $
		         VALUE=value, UVALUE=uvalue, FONT=font, bitfile

    IF (N_PARAMS() EQ 0) THEN MESSAGE, 'Must specify parent for cw_adas504_out'
    ON_ERROR, 2					;return to caller

		;**** Set defaults for keywords ****

    IF NOT (KEYWORD_SET(devlist)) THEN devlist = ''
    IF NOT (KEYWORD_SET(uvalue)) THEN uvalue = 0
    IF NOT (KEYWORD_SET(font)) THEN font = ''
    IF NOT (KEYWORD_SET(value)) THEN begin
	os = {	out504_set, 						$
		GRPOUT		:	0, 				$
		GTIT1		:	'', 				$
		GRPSCAL		:	0, 				$
		XMIN		:	'',  				$
		XMAX		:	'',   				$
		YMIN		:	'',  				$
		YMAX		:	'',   				$
		HRDOUT		:	0, 				$
		HARDNAME	:	'', 				$
		GRPDEF		:	'', 				$
		GRPFMESS	:	'', 				$
		GRPSEL		:	-1, 				$
		GRPRMESS	:	'', 				$
		DEVSEL		:	-1, 				$
		GRSELMESS	:	'', 				$
		TEXOUT		:	0, 				$
		TEXAPP		:	-1, 				$
		TEXREP		:	0, 				$
		TEXDSN		:	'', 				$
         	TEXDEF		:	'paper.txt', 			$
		TEXMES		:	''  				}
  ENDIF ELSE BEGIN
	os = {	out504_set, 						$
		GRPOUT		:	value.grpout, 			$
		GTIT1		:	value.gtit1, 			$
		GRPSCAL		:	value.grpscal, 			$
		XMIN		:	value.xmin, 			$
		XMAX		:	value.xmax,   			$
		YMIN		:	value.ymin,     		$
		YMAX		:	value.ymax,   			$
		HRDOUT		:	value.hrdout, 			$
		HARDNAME	:	value.hardname, 		$
		GRPDEF		:	value.grpdef, 			$
		GRPFMESS	:	value.grpfmess, 		$
		GRPSEL		:	value.grpsel, 			$
		GRPRMESS	:	value.grprmess, 		$
		DEVSEL		:	value.devsel, 			$
		GRSELMESS	:	value.grselmess, 		$
		TEXOUT		:	value.texout, 			$
		TEXAPP		:	value.texapp, 			$
		TEXREP		:	value.texrep, 			$
		TEXDSN		:	value.texdsn, 			$
		TEXDEF		:	value.texdef, 			$
		TEXMES		:	value.texmes 			}
    ENDELSE

		;**********************************************
		;**** Create the 504 Output options widget ****
		;**********************************************

		;**** create base widget ****

    cwid = widget_base( parent, UVALUE = uvalue,			$
			EVENT_FUNC = "out504_event", 			$
			FUNC_GET_VALUE = "out504_get_val", 		$
			/COLUMN)

		;**** Add dataset name and browse button ****

    rc = cw_adas_dsbr(cwid, dsfull, font=font)

		;*****************************************************
		;**** Widget for graphics selection               ****
		;**** Note change in names for GRPOUT and GRPSCAL ****
		;*****************************************************

    grselval = {  	OUTBUT		:	os.grpout, 		$
			GTIT1		:	os.gtit1, 		$
			SCALBUT		:	os.grpscal, 		$
			XMIN		:	os.xmin, 		$
			XMAX		:	os.xmax, 		$
			YMIN		:	os.ymin, 		$
			YMAX		:	os.ymax, 		$
			HRDOUT		:	os.hrdout, 		$
			HARDNAME	:	os.hardname, 		$
			GRPDEF		:	os.grpdef, 		$
			GRPFMESS	:	os.grpfmess, 		$
			GRPSEL		:	os.grpsel, 		$
			GRPRMESS	:	os.grprmess, 		$
			DEVSEL		:	os.devsel, 		$
			GRSELMESS	:	os.grselmess 		}
    base = widget_base(cwid, /row, /frame)
    grpid = cw_adas_gr_sel(base, /SIGN, OUTPUT='Graphical Output', 	$
			   DEVLIST=devlist, VALUE=grselval, FONT=font)

		;**** Widget for text output ****

    outfval = { 	OUTBUT		:	os.texout, 		$
			APPBUT		:	-1, 			$
			REPBUT		:	os.texrep, 		$
	        	FILENAME	:	os.texdsn, 		$
			DEFNAME		:	os.texdef, 		$
			MESSAGE		:	os.texmes 		}
    base = widget_base(cwid, /row, /frame)
    paperid = cw_adas_outfile(base, OUTPUT='Text Output', 		$
                              VALUE=outfval, FONT=font)

		;**** Error message ****

    messid = widget_label(cwid, value=' ', font=font)

		;**** add the exit buttons ****

    base = widget_base(cwid, /row)
    menufile = bitfile + '/menu.bmp'
    read_X11_bitmap, menufile, bitmap1
    outid = widget_button(base, value=bitmap1)		;menu button
    cancelid = widget_button(base, value='Cancel', font=font)
    doneid = widget_button(base, value='Done', font=font)

		;*************************************************  
		;**** create a state structure for the pop-up ****
		;**** window.                                 ****
		;*************************************************

    new_state = { 	GRPID		:	grpid, 			$
			PAPERID		:	paperid,          	$
		  	CANCELID	:	cancelid, 		$
			DONEID		:	doneid, 		$
			MESSID		:	messid,  		$
		  	OUTID		:	outid 			}

                ;**** Save initial state structure ****

    widget_control, parent, set_uvalue=new_state, /no_copy

    RETURN, cwid

END

