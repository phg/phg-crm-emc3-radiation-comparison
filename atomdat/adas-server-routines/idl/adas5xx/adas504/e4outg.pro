; Copyright (c) 1996, Strathclyde University.
; SCCS info: Module @(#)$Header: /home/adascvs/idl/adas5xx/adas504/e4outg.pro,v 1.3 2004/07/06 13:42:06 whitefor Exp $ Date $Date: 2004/07/06 13:42:06 $
;+
; PROJECT:
;       ADAS IBM MVS to UNIX conversion
;
; NAME:
;	E4OUTG
;
; PURPOSE:
;	Communication with ADAS504 FORTRAN via UNIX pipe and
;	graphics output.
;
; EXPLANATION:
;	The routine begins by reading information from the ADAS504
;	FORTRAN process via a UNIX pipe.  Then the IDL graphical
;	output routine for ADAS504 is invoked.  Communications are to
;	the FORTRAN subroutine E4OUTG.
;
; USE:
;	The use of this routine is specific to ADAS504 see adas504.pro.
;
; INPUTS:
;	DSFULL   - Data file name
;	PIPE	 - The IDL unit number of the bi-directional pipe to the
;		   ADAS504 FORTRAN process.
;
;	UTITLE   - Optional comment by user
;
;	GRPSCAL	 - Integer; 0 if default scaling required 1 if user
;		   specified scaling to be used.
;
;	XMIN	 - String; User specified x-axis minimum, number as string.
;
;	XMAX	 - String; User specified x-axis maximum, number as string.
;
;	YMIN	 - String; User specified y-axis minimum, number as string.
;
;	YMAX	 - String; User specified y-axis maximum, number as string.
;
;	HRDOUT	 - Integer; 0 if no hardcopy required, 1 if hardcopy.
;
;	HARDNAME - String; file name for hardcopy output.
;
;	DEVICE	 - String; IDL name for hardcopy output device.
;
;	HEADER	 - ADAS version header information for inclusion in the
;		   graphical output.
;
; OPTIONAL INPUTS:
;	None.
;
; OUTPUTS:
;	None.
;
; OPTIONAL OUTPUTS:
;	None.
;
; KEYWORD PARAMETERS:
;	FONT	- String; Name of the font to be used for the graphical
;		  output widget.
;
; CALLS:
;	ADAS504_PLOT	ADAS504 graphical output.
;
; SIDE EFFECTS:
;	This routine reads information from FORTRAN via a UNIX pipe.
;
; CATEGORY:
;	Adas system.
;
; WRITTEN:
;       Hugh Summers, University of Strathclyde,  20/12/1995
;
; MODIFIED:
;	1.1	Hugh Summers				
;		First version
;	1.2	Tim Hammond, Tessella Support Services plc
;		Removed superfluous reading of xmin, xmax etc from FORTRAN
;       1.3     Tim Hammond
;               Put in default value for nmx for when polynomial fit
;               not required.
;
; VERSION:
;	1.1	20/12/1995
;	1.2	18/01/1996
;       1.3     29/01/1996
;
;-
;-----------------------------------------------------------------------------

PRO e4outg, dsfull, pipe, utitle, grpscal, xmin, xmax, ymin, ymax,     $
	    hrdout, hardname, device, header, bitfile, gomenu, FONT=font

		;***********************************
                ;**** Set defaults for keywords ****
		;***********************************

    IF NOT (KEYWORD_SET(font)) THEN font = ''

		;*************************************
		;**** Declare variables for input ****
		;*************************************

    sdum = " "
    idum = 0
    fdum = 0.0
    strg = make_array(9, /string, value=" ")
    nmx = 0

		;********************************
		;**** Read data from fortran ****
		;********************************

    readf, pipe, format = '(a40)' , sdum
    title = sdum
    readf, pipe, format = '(a120)', sdum
    titlx = sdum
    readf, pipe, format = '(a80)' , sdum
    titlm = sdum
    readf, pipe, format = '(a8)' , sdum
    date = sdum
    readf, pipe, format = '(a2)' , sdum
    esym = sdum
    readf, pipe, idum
    iz0 = idum
    readf, pipe, idum
    iz1 = idum
    readf, pipe, format = '(a5)' , sdum
    ciion = sdum
    readf, pipe, format = '(a5)' , sdum
    citype = sdum
    readf, pipe, format = '(a20)' , sdum
    ciinfo = sdum
    readf, pipe, idum
    itval = idum

		;**************************************
		;**** now declare array dimensions ****
		;**************************************

    teva = dblarr(itval) 
    pzda = dblarr(itval) 
    for i=0, itval-1 do begin
        readf, pipe, fdum
        teva(i) = fdum 
    endfor
    for i=0, itval-1 do begin
        readf, pipe, fdum
        pzda(i) = fdum 
    endfor
    readf, pipe, idum
    ldef1 = idum
    readf, pipe, idum
    lfsel = idum
    if (lfsel eq 1) then begin
        readf, pipe, idum
        nmx = idum
        pzdm = fltarr(nmx) 		;**** declare arrays ****
        tfitm = fltarr(nmx)
        for i = 0, nmx-1 do begin
	    readf, pipe, fdum
            pzdm(i) = fdum 
        endfor
        for i = 0, nmx-1 do begin
	    readf, pipe, fdum 
            tfitm(i) = fdum
        endfor
    endif
    for i = 0, 5 do begin
        readf, pipe, sdum 
        strg(i) = sdum
    endfor
    readf, pipe, sdum, format = '(a32)'
    head1 = sdum
    readf, pipe, sdum, format = '(a16)'
    head2 = sdum
    readf, pipe, sdum, format = '(a16)'
    head3 = sdum

		;***********************
		;**** Plot the data ****
		;***********************

    adas504_plot, dsfull, 						$
  		  title , titlx, titlm , utitle,date, esym, iz0, iz1, 	$
  		  ciion , citype, ciinfo, itval,                      	$
		  teva  , pzda , pzdm  , tfitm,                       	$
                  ldef1 , xmin , xmax  , ymin , ymax,                 	$
   		  lfsel , nmx  ,                                      	$
   		  strg  , head1, head2, head3,                        	$
		  hrdout, hardname, device, header, bitfile, gomenu,  	$
                  FONT=font

END
