; Copyright (c) 1995, Strathclyde University.
; SCCS info: Module @(#)$Header: /home/adascvs/idl/adas5xx/adas504/adas504_proc.pro,v 1.3 2004/07/06 10:56:46 whitefor Exp $ Date $Date: 2004/07/06 10:56:46 $
;+
; PROJECT:
;       ADAS IBM MVS to UNIX conversion
;
; NAME:
;	ADAS504_PROC
;
; PURPOSE:
;	IDL ADAS user interface, processing options/input.
;
; EXPLANATION:
;	This routine creates and manages a pop-up window which allows
;	the user to select options and input data to control ADAS504
;	processing.
;
; USE:
;	This routine is ADAS504 specific, see e4ispf.pro for how it
;	is used.
;
; INPUTS:
;	PROCVAL	- A structure which determines the initial settings of
;		  the processing options widget.  The value is passed
;		  unmodified into cw_adas504_proc.pro.
;
;		  See cw_adas504_proc.pro for a full description of this
;		  structure.
;
;	DSFULL	- String; The full system file name of the input 
;		          dataset selected by the user for processing.
;
;	NSTORE	- Integer : Maximum number of data blocks which can be
;                           read from the data set.
;
;	NTDIM   - Integer : Maximum number of temperatures from data file.
;
;	NDTIN	- Integer : Maximum number of temperatures allowed.
;
;	NBSEL	- Integer : Number of data blocks accepted and read in
;
;	CIION	- String array : Radiating ion for each
;                                block
;               
;	CITYPE	- String array : Radiation type for each
;                                block
;
;	CIINFO	- String array : Information string for each block
;               
;	ITA	- Integer array: Number of temperatures for each block
;
;	TVALS	- Double array : Electron temperatures for each block
;				 1st dimension - temperature index
;				 2nd dimension - units 1 => Kelvin
;						       2 => eV
;						       3 => Reduced
;
; OPTIONAL INPUTS:
;	None
;
; OUTPUTS:
;	PROCVAL	- On output the structure records the final settings of
;		  the processing selections widget if the user pressed the
;		  'Done' button, otherwise it is not changed from input.
;
;	ACT	- String; 'Done' , 'Cancel' or 'Menu' for the button the
;		  user pressed to terminate the processing options
;		  window.
;
; OPTIONAL OUTPUTS:
;	None
;
; KEYWORD PARAMETERS:
;	FONT_LARGE - The name of a larger font.
;
;	FONT_SMALL - The name of a smaller font.
;
;	EDIT_FONTS - A structure of two fonts used in the table editor
;		     adas_edtab.pro, {font_norm:'',font_input:''}.
;
; CALLS:
;	CW_ADAS504_PROC	Declares the processing options widget.
;	DYNLABEL	Recursively adds dynamic_resize keyword to label
;			widgets for version 4.0.1 and higher of IDL
;	ADAS504_PROC_EV Called indirectly during widget management,
;			routine included in this file.
;	XMANAGER
;
; SIDE EFFECTS:
;       This widget uses a common block PROC504_BLK, to pass the
;       variables VALUE and ACTION between the two routines. 
;
; CATEGORY:
;	Widgets
;
; WRITTEN:
;       Hugh Summers, University of Strathclyde, 12/12/1995
;
; MODIFIED:
;	1.1	Hugh Summers
;		First version
;	1.2	Tim Hammond
;		Corrected offset of main base.
;	1.3	William Osborn
;		Added dynlabel procedure
;
; VERSION:
;	1.1	27-11-95
;	1.2	06-02-96
;	1.3	12-07-96
;
;
;-----------------------------------------------------------------------------

PRO adas504_proc_ev, event

    COMMON proc504_blk,action,value

    action = event.action

    CASE event.action OF

		;**** 'Done' button ****

	'Done'  : begin

		;**** Get the output widget value ****

   	          widget_control,event.id,get_value=value 
	          widget_control,event.top,/destroy

	          end


		;**** 'Cancel' button ****

	'Cancel': widget_control,event.top,/destroy


		;**** 'Menu' button ****

	'Menu': widget_control,event.top,/destroy

    ENDCASE

END

;-----------------------------------------------------------------------------

PRO adas504_proc, procval, dsfull, act, nstore, ntdim, ndtin, nbsel,	$
		  ciion, citype, ciinfo, ita, tvals, bitfile,		$
		  FONT_LARGE=font_large, FONT_SMALL=font_small,		$
		  EDIT_FONTS=edit_fonts

		;**** declare common variables ****

    COMMON proc504_blk, action, value

		;**** Copy "procval" to common ****

    value = procval

		;**** Set defaults for keywords ****

    IF NOT (KEYWORD_SET(font_large)) THEN font_large = ''
    IF NOT (KEYWORD_SET(font_small)) THEN font_small = ''
    IF NOT (KEYWORD_SET(edit_fonts)) THEN $
    edit_fonts = { font_norm:'', font_input:''}
		
                ;**** create base widget ****

    procid = widget_base(TITLE='ADAS504 PROCESSING OPTIONS', 		$
			 XOFFSET=50, YOFFSET=1)

		;**** Declare processing widget ****

    cwid = cw_adas504_proc(procid, dsfull, act, nstore, ntdim, ndtin,	$
			   nbsel, ciion, citype, ciinfo,                $
		           ita,tvals, bitfile, PROCVAL=value, 		$
			   UVALUE=uvalue, FONT_LARGE=font_large, 	$
			   FONT_SMALL=font_small, EDIT_FONTS=edit_fonts)

		;**** Realize the new widget ****

    dynlabel, procid
    widget_control, procid, /realize

		;**** make widget modal ****

    xmanager, 'adas504_proc', procid, event_handler='adas504_proc_ev', 	$
	      /modal, /just_reg
 
		;*** copy value back to procval for return to e4ispf ***

    act = action
    procval = value
 
END

