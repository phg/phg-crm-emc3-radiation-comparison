; Copyright (c) 1995, Strathclyde University 
; SCCS INFO : Module @(#)$Header: /home/adascvs/idl/adas5xx/adas503/e3ispf.pro,v 1.3 2004/07/06 13:39:24 whitefor Exp $ Date $Date: 2004/07/06 13:39:24 $
;
;+
; PROJECT:
;       ADAS IBM MVS to   UNIX conversion
;
; NAME:
;	E3ISPF
;
; PURPOSE:
;	IDL user interface and communications with ADAS503 FORTRAN
;	process via pipe.
;
; EXPLANATION:
;	The routine begins by reading information from the ADAS503
;	FORTRAN process via a UNIX pipe.  Then part of the ADAS503
;	IDL user interface is invoked to determine how the user
;	wishes to process the input dataset.  When the user's
;	interactions are complete the information gathered with
;	the user interface is written back to the FORTRAN process
;	via the pipe.  Communications are to the FORTRAN subroutine
;	E3ISPF.
;
; USE:
;	The use of this routine is specific to ADAS503, see adas503.pro.
;
; INPUTS:
;	PIPE	- The IDL unit number of the bi-directional pipe to the
;		  ADAS503 FORTRAN process.
;
;	LPEND	- Integer, 0 if the user pressed the 'Done' button
;		  or 1 if the user pressed 'Cancel'.  Maps directly onto
;		  the logical variable LPEND in ADAS503 FORTRAN.
;
;	PROCVAL	- A structure which determines the initial settings of
;		  the processing options widget.  The initial value is
;		  set in adas503.pro.  If adas503.pro passes a blank 
;		  dummy structure of the form {NMET:-1} into PROCVAL then
;		  PROCVAL is reset to a default structure.
;
;		  The PROCVAL structure is;
;			procval = {                        	$
;                			nmet  : 0 ,             $
;                			title : '',             $
;                			nbsel : 0 ,             $
;                			ibsel : 0 ,             $
;                			uwavel: str_arr,        $
;                			ufile : str_arr,        $
;                			utype : str_arr,        $
;                			uindm : str_arr,        $
;                			ifout : 1,              $
;					ldfit : 0,              $
;					maxt  : 0, 			$
;					maxd  : 0,              $
;                			tin   : temp_arr,       $
;                			din   : dens_arr,       $
;                			ifsel : 0,              $
;					selbase: 1,			$
;                			tolval: 5               $
;              			}
;
;
;		  See cw_adas503_proc.pro for a full description of this
;		  structure.
;	
;
;	DSFULL	- String; The full system file name of the input 
;		  dataset selected by the user for processing.
;
;       BITFILE - String; the path to the dirctory containing bitmaps
;                 for the 'escape to series menu' button.
;
; OPTIONAL INPUTS:
;	None
;
; OUTPUTS:
;	PROCVAL	- On output the structure records the final settings of
;		  the processing selections widget if the user pressed the
;		  'Done' button, otherwise it is not changed from input.
;
;       LPEND   - Integer, 0 if the user pressed the 'Done' button
;                 or 1 if the user pressed 'Cancel'.  Maps directly onto
;                 the logical variable LPEND in ADAS501 FORTRAN.
;
;       GOMENU  - Int; flag - set to 1 if user has selected 'escape direct
;                 to series menu , otherwise 0
;
; OPTIONAL OUTPUTS:
;	None
;
; KEYWORD PARAMETERS:
;	FONT_LARGE - The name of a larger font.
;
;	FONT_SMALL - The name of a smaller font.
;
;	EDIT_FONTS - A structure of two fonts used in the table editor
;		     adas_edtab.pro, {font_norm:'',font_input:''}.
;
; CALLS:
;	ADAS503_PROC	Invoke the IDL interface for ADAS503 data
;		 	processing options/input.
;
; SIDE EFFECTS:
;	Two way communications with ADAS503 FORTRAN via the
;	bi-directional UNIX pipe.
;
; CATEGORY:
;	Adas system.
;
; WRITTEN:
;       Lalit Jalota, Tessella Support Services plc, 23-Feb-1995
;
; MODIFIED:
;	1.1	Lalit Jalota
;               First release
;	1.2	Tim Hammond 
;		Tidied up code and comments and added bitmap related
;           variables allowing the use of a return to series menu
;           button.
;           Also replaced all readf's into direct variables with
;           dummy variables which are then copied to the relevant
;           important variables to avoid possible future problems.
;	1.3	Richard Martin
;		Added reads for pec data (for interactive pair selector). 
;		Added selbase to procval.
;		
;
; VERSION:
;	1.1	28-02-95
;	1.2	07-02-96
;	1.3	13-10-99
;
;-
;-----------------------------------------------------------------------------

PRO e3ispf, pipe, lpend, procval, dsfull, gomenu, bitfile,		$
	    FONT_LARGE=font_large, FONT_SMALL=font_small, 		$
	    EDIT_FONTS=edit_fonts


                ;**** Set defaults for keywords ****

    IF NOT (KEYWORD_SET(font_large)) THEN font_large = ''
    IF NOT (KEYWORD_SET(font_small)) THEN font_small = ''
    IF NOT (KEYWORD_SET(edit_fonts)) THEN 				$
    edit_fonts = {font_norm:'',font_input:''}

		;********************************************
		;****     Declare variables for input    ****
		;**** arrays will be declared after sizes****
		;**** have been read.                    ****
                ;********************************************

    lpend = 0
    nstore= 0
    ntdim = 0
    nddim = 0
    ndtin = 0
    nbsel = 0
    input = 0

		;********************************
		;**** Read data from fortran ****
		;********************************

    readf, pipe, input
    nstore = input
    readf, pipe, input
    ntdim = input
    readf, pipe, input
    nddim = input
    readf, pipe, input
    ndtin = input
    readf, pipe, input
    nbsel = input

		;******************************************
		;**** Now can define other array sizes ****
		;******************************************

    cwavel = strarr(nbsel)
    cfile  = strarr(nbsel)
    ctype  = strarr(nbsel)
    cindm  = strarr(nbsel)
    ita    = intarr(nbsel)
    ida    = intarr(nbsel)
    tvals  = dblarr(ntdim, 3, nbsel)
    teda   = dblarr(nddim, nbsel)
    pec    = dblarr(ntdim,nddim,nbsel)

		;********************************
                ;**** Read data from fortran ****
                ;********************************

    next_item = 0.0
    sdum = ' '
    for j=0, nbsel-1 do begin
        readf, pipe, sdum
        cwavel(j) = sdum
    endfor
    for j=0, nbsel-1 do begin
        readf, pipe, sdum
        cfile(j) = sdum
    endfor
    for j=0, nbsel-1 do begin
        readf, pipe, sdum
        ctype(j) = sdum
    endfor
    for j=0, nbsel-1 do begin
        readf, pipe, sdum
        cindm(j) = sdum
    endfor
    for j=0, nbsel-1 do begin
        readf, pipe, input
        ita(j) = input
    endfor
    for j=0, nbsel-1 do begin
        readf, pipe, input
        ida(j) = input
    endfor
    for k = 0, nbsel-1 do begin 
        for j =  0 , 2 do begin
	    for i = 0, ita(k)-1 do begin
                readf, pipe, next_item
                tvals(i,j,k) = next_item
            endfor
        endfor
    endfor
    for j = 0, nbsel-1 do begin 
        for i = 0, ida(j)-1 do begin
            readf, pipe, next_item
            teda(i,j) = next_item
        endfor 
    endfor
    for k = 0, nbsel-1 do begin 
        for j = 0, ida(k)-1 do begin
		for i = 0, ita(k)-1 do begin
               readf, pipe, next_item
               pec(i,j,k) = next_item
		endfor
        endfor 
    endfor   

		;*******************************************
		;**** Set default value if non provided ****
		;*******************************************

    if (procval.nmet lt 0) then begin
         str_arr = strarr(nbsel)
         temp_arr = fltarr(ntdim)
         dens_arr = fltarr(nddim)
         procval = {	nmet  	: 	0 ,             		$
			title 	: 	'',				$
			nbsel 	: 	0 ,				$
			ibsel 	: 	0 ,     			$
			uwavel	: 	str_arr,			$
			ufile 	: 	str_arr,        		$
			utype 	: 	str_arr,        		$
			uindm 	: 	str_arr,			$
                	ifout 	: 	1, 				$
			ldfit 	: 	0,				$
			maxt  	: 	0,              		$
                	maxd  	: 	0, 				$
			tin   	: 	temp_arr,			$
			din   	: 	dens_arr,			$
			ifsel 	: 	0,				$
			selbase	:	1,				$
			tolval	: 	5               		}
    endif

		;****************************************
		;**** Pop-up processing input widget ****
		;****************************************

    adas503_proc, procval, dsfull, nstore, 				$
		  ntdim, nddim, ndtin, nbsel, 				$
		  cwavel, cfile, ctype , cindm, 			$
		  ita, ida, tvals, teda, pec, action, bitfile,		$
		  FONT_LARGE=font_large, FONT_SMALL=font_small, 	$
		  EDIT_FONTS=edit_fonts

		;********************************************
		;****  Act on the event from the widget  ****
                ;**** There are three  possible  actions ****
                ;**** 'Done', 'Cancel'and 'Menu'.        ****
		;********************************************

    if action eq 'Done' then begin
        lpend = 0
    endif else if action eq 'Menu' then begin
        lpend = 0
        gomenu = 1
    endif else begin
        lpend = 1
    endelse

		;*******************************
		;**** Write data to fortran ****
		;*******************************

    printf, pipe, lpend   
    title_short = strmid(procval.title,0,39) 	
    printf, pipe, title_short, format='(a40)' 
    printf, pipe, procval.ibsel  			
    printf, pipe, procval.ifout
    printf, pipe, procval.ldfit
    if (procval.ldfit eq 0) then begin
        printf, pipe, procval.maxt   
    endif else begin 
        printf, pipe, procval.maxd
    endelse
    printf, pipe, procval.tin
    printf, pipe, procval.din
    if (procval.selbase eq 1 AND procval.ifsel eq 1) then begin
    		printf, pipe, 1
    endif else begin
    	    	printf, pipe, 0
    endelse
    
    printf, pipe, procval.tolval

END
