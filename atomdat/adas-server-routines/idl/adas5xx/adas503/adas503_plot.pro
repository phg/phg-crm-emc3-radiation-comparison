; Copyright (c) 1995, Strathclyde University .
; SCCS info: Module @(#)$Header: /home/adascvs/idl/adas5xx/adas503/adas503_plot.pro,v 1.9 2004/07/06 10:55:44 whitefor Exp $	Date $Date: 2004/07/06 10:55:44 $
;+
; PROJECT:
;       ADAS IBM MVS to UNIX conversion
;
; NAME:
;	ADAS503_PLOT
;
; PURPOSE:
;	Generates ADAS503 graphical output.
;
; EXPLANATION:
;	This routine creates a window for the display of graphical
;	output a separate routine PLOT503 actually plots a
;	graph.
;
; USE:
;	This routine is specific to ADAS503, see e3outg.pro for
;	example use.
;
; INPUTS:
;	(Most of these inputs map exactly onto variables of the same
;	 name in the FORTRAN code.)
;
;
;	DSFULL  - String; Name of data file 
;
;	TITLE   - String array; titles to be placed above graph
;
;	TITLX   - String; user supplied comment appended to end of title
;
;	TITLM   - String; Information about minimax fitting if selected.
;
;	UTITLE  - String; Optional comment by user
;
;	DATE	- String; Date of graph production
;
;	ESYM 	- String; Symbol of emitting ion
;
;	IZ0	- Integer; Nuclear charge of emitting ion
;
;	IZ	- Integer; Charge of emitting ion
;
;	CWAVEL 	- String; Wavelength of data block used
;
;	CFILE	- String; Specific ion file source
;
;	CTYPE  	- String; processing code from input data file
;
;	CINDM	- String; metastable index
;
;	ITVAL	- Integer; number of user entered temperature/density pairs
;
;	TEVA	- Double array; User entered electron temperatures, eV
;
;	DIN	- Double array; User entered electron densities, cm-3
;
;	PECA	- Double array; Spline interpolated or extrapolated 
;				ionizations per photon for user entered temp.
;				and density pairs.
;
;	PECM	- Double array; Minimax fit values of ionizations per photon  
;				at 'tdfitm()'
;
;	TDFITM	- Double array; Selected temperature (or density) values for minmiax fit.
;
;	LDEF1	- Integer; 0 - use user entered graph scales
;			   1 - use default axes scaling
;
;	XMIN    - String; Lower limit for x-axis of graph, number as string.
;
;	XMAX    - String; Upper limit for x-axis of graph, number as string.
;
;	YMIN    - String; Lower limit for y-axis of graph, number as string.
;
;	YMAX    - String; Upper limit for y-axis of graph, number as string.
;
;	LFSEL	- Integer; 0 - No minimax fitting was selected 
;			   1 - Mimimax fitting was selected
;
;	LDFIT 	- Integer; 0 - data fitted vs temperature
;			   1 - Data fitted vs Density
;
;	NMX	- Integer; Number of temp/density pairs used for minimax fit
;
;	STRG	- String array; Information regarding current data selected
;
;	HEAD1	- String; header information for data source
;
;	HEAD2A	- String; Header information for temperature/density 
;
;	HEAD2B  - String; column titles for temp/density pair information
;
;	HRDOUT - Integer; 1 if hardcopy output activated, 0 if not.
;
;	HARDNAME- String; Filename for harcopy output.
;
;	DEVICE	- String; IDL name of hardcopy output device.
;
;	HEADER	- String; ADAS version number header to include in graph.
;
;     BITFILE 	- String; the path to the directory containing bitmaps
;                   for the 'escape to series menu' button.
;
;	FITFLAG	- 1 = polynomial fit permitted
;			  2 = polynomial fit not permitted
;
;	DEVCODE	- (see adas503.pro) Passed on to cw_adas_multiplot.
;
;	DEVLIST	- (see adas503.pro) Passed on to cw_adas_multiplot.
;
; OPTIONAL INPUTS:
;	None.
;
; OUTPUTS:
;       GOMENU  - Int; flag - set to 1 if user has selected 'escape direct
;                 to series menu', otherwise 0
;
; OPTIONAL OUTPUTS:
;	None.
;
; KEYWORD PARAMETERS:
;	FONT	- String; The name of a font to use for text in the
;		  graphical output widget.
;
; CALLS:
;	CW_ADAS_GRAPH	Graphical output widget.
;	ADASPLOT		Make one plot to an output device for 503.
;	XMANAGER
;
; SIDE EFFECTS:
;	This routine uses a common block to maintain its state PLOT503_BLK.
;
;	One other routine is included in this file;
;	ADAS503_PLOT_EV	Called via XMANAGER during widget management.
;
; CATEGORY:
;	Adas system.
;
; WRITTEN:
;       Lalit Jalota, Tessella Support Services plc,  23-Feb-1995
;
; MODIFIED:
;       1.1     Lalit Jalota
;               First release
;       1.2     Lalit Jalota
;		Corrected graph annotation to match IBM version
;       1.3     Lalit Jalota
;       1.4     Lalit Jalota
;	1.5	Tim Hammond	
;		Updated graph labelling.
;	1.6	Tim Hammond
;               Tidied up comments and code and added 'return to series
;               menu' functionality.
;       1.7     William Osborn
;               Added dynlabel
;	1.8	Richard Martin
;		Added support for graph adjust widget, multiplot widget
;	1.9	Richard Martin
;		Initialised xmin1,xmin2,xmax1,xmax2 for case ldef1>0
;
; VERSION:
;	1.1	28-02-95
;	1.2	03-03-95
;	1.3	13-03-95
;	1.4	23-03-95
;	1.5	27-06-95
;	1.6	07-02-96
;	1.7	04-10-96
;	1.8	13-10-99
;	1.9	14-04-2000
;
;-
;----------------------------------------------------------------------------

PRO adas503_plot_ev, event

    COMMON plot503_blk, data, action, win, plotdev, plotfile, 		$
                        fileopen, gomenu

    newplot = 0
    print = 0
    done = 0

		;****************************************
		;**** Set graph and device requested ****
		;****************************************

    CASE event.action OF

	'print': begin
	    newplot = 1
	    print = 1
        end

	'done': begin
	    if fileopen eq 1 then begin
	        set_plot, plotdev
	        device, /close_file
	    endif
	    set_plot, 'X'
	    widget_control, event.top, /destroy
	    done = 1
        end

        'bitbutton' : begin
            if fileopen eq 1 then begin
                set_plot, plotdev
                device, /close_file
            endif
            set_plot,'X'
            widget_control, event.top, /destroy
            done = 1
            gomenu = 1
        end

	'adjust': begin
	      if data.fitflag eq 1 then tdsens=1 else tdsens=0
		adjustid=cw_adjust(data,tdsens=tdsens,/dsens,/ysens,font=data.font)
		data=adjustid.data	
        end     
        
        'retain': begin
        	retainid=cw_adas_multigraph(data,/print,/adjust,font=data.font)
        end   
     
        ELSE:   ;**** Do nothing ****
              
    ENDCASE

		;*******************************
		;**** Make requested plot/s ****
		;*******************************

    if done eq 0 then begin

		;**** Set graphics device ****

        if print eq 1 then begin
            set_plot, plotdev
            if fileopen eq 0 then begin
                fileopen = 1
                device, filename=plotfile
	        device, /landscape
            endif
        endif else begin
            set_plot,'X'
            wset,win
        endelse

		;**** Draw graphics ****

        adasplot, data.x , data.y, data.itval, data.nplots, data.nmx, 	$
	         data.title,data.xtitle, data.xunit, data.xunits,data.ytitle, 			$
	         data.strg, data.head1, data.head2a,data.head2b, 	$
	         data.teva, data.din, data.ldef, 			$
	         data.xmin, data.xmax, data.ymin, data.ymax, 		$
	         data.lfsel, xlog=data.xlog, ylog=data.ylog 
	if print eq 1 then begin
	    message = 'Plot  written to print file.'
	    grval = {WIN:0, MESSAGE:message}
	    widget_control, event.id, set_value=grval
	endif
    endif
    
    if print eq 1 then begin
	      device, /close 
            set_plot,'X'               
    endif
END

;----------------------------------------------------------------------------

PRO adas503_plot, dsfull, title , titlx, titlm, utitle, date, esym,    $
			iz0, iz, cwavel, cfile, ctype , cindm, itval,  $
			teva, din, peca, pecm, tdfitm, ldef1, xmin, xmax,    $
			ymin, ymax, lfsel, ldfit, fitflag, nmx, strg, head1, head2a,  $
			head2b, hrdout, hardname, device, header, bitfile,	$
			gomenu, devcode,devlist, FONT=font

    COMMON plot503_blk, data, action, win, plotdev, plotfile, 		$
                        fileopen, gomenucom

		;**** Copy input values to common ****

    plotdev = device
    plotfile = hardname
    fileopen = 0
    gomenucom = gomenu

		;************************************
		;**** Create general graph title ****
		;************************************

    title = strarr(6)
    maintitle=strarr(2)
    type = 'ELECTRON TEMPERATURE '
    maintitle(0) = "PHOTON EMISSIVITY COEFFICIENT VS " + type

    type = 'ELECTRON DENSITY '
    maintitle(1) = "PHOTON EMISSIVITY COEFFICIENT VS " + type

    if ( strtrim(strcompress(utitle),2)  ne '' ) then begin
        maintitle(0) = maintitle(0) + ': ' + strupcase(strtrim(utitle,2))        
        maintitle(1) = maintitle(1) + ': ' + strupcase(strtrim(utitle,2))        
    endif
    if ldfit eq 0 then title(0)=maintitle(0) else title(0)=maintitle(1)
    title(1) = 'ADAS    :' + header
    title(2) = 'FILE     :' + titlx
    if (lfsel eq 1) then begin
        title(3)  = 'MINIMAX : ' + strupcase(titlm)
    endif
    title(4) = 'KEY     : (CROSSES - INPUT DATA) (FULL LINE - SPLINE FIT)'
    if (lfsel eq 1) then  title(4) = title(4) + "  (DASH LINE - MINIMAX) "

		;********************************
		;*** Create graph annotation ****
		;********************************        

    strg = strtrim(strg, 2)
    strg(0) = strg(0) +  ' ' + esym
    strg(1) = strg(1) +  strcompress(string(iz0)) 
    strg(2) = strg(2) +  strcompress(string(iz))
    strg(3) = strg(3) +  strcompress(cwavel)
    strg(4) = strg(4) +  ' ' + cfile
    strg(5) = strg(5) +  ' ' + ctype 
    strg(6) = strg(6) +  ' ' + cindm

		;*********************************
		;**** Set up Y data for plots ****
		;*********************************

    if (lfsel eq 1) then ydim=nmx else ydim=itval
    nmx=100
    ydim=nmx
    y = make_array(2, ydim, /float)
    valid_data = where((peca gt 1e-37) and (peca lt 1e+37))
    if (valid_data(0) ge 0) then begin
        y(0, valid_data) = peca(valid_data)
        if (lfsel eq 1) then begin
            valid_data = where((pecm gt 1e-37) and (pecm lt 1e+37))
	    if (valid_data(0) ge 0) then begin
                y(1, valid_data) = pecm(valid_data)
            endif else begin
                print, "ADAS503 : unable to plot polynomial fit data"
            endelse
        endif
    endif else begin
        print, "ADAS503 : unable to plot spline fit data"
    endelse
    ytitle = "PHOTON EMISSIVITY COEFFICIENT  (PHOT. cm!e3!n s!e-1!n)"

		;**************************************
		;**** Set up x axis and xaxis title ***
		;**************************************

    if (lfsel eq 1) then xdim = nmx else xdim = itval
    nmx=100
    xdim=nmx
    x1 = fltarr(2, xdim)
    x2 = fltarr(2, xdim)   

            ;****************************************************
		;*** write x axis array depending on whether temp ***
		;*** or density values used 			  	  ***
            ;****************************************************

    xtitles=strarr(2)
    xunits=strarr(2,2)
    xtitles(0) = "ELECTRON TEMPERATURE "
    xtitles(1) = "ELECTRON DENSITY "	   
    xunits(0,*) =  [' (eV) ',' (K) ']	 
    xunits(1,0) =   ' (cm-3) ' 
    xunit  = 0

    yunits =   [' (cm3 s-1) '] 

    if (lfsel eq 0) then begin
    	x1(0,0:itval-1) = teva
    	nplots = 1
    endif else begin
    	x1(0,0:itval-1) = teva
    	x1(1,*) = tdfitm
    	nplots = 2
    endelse
    if (lfsel eq 0) then begin
    	x2(0,0:itval-1) = din
    	nplots = 1
    endif else begin
    	x2(0,0:itval-1) = din
    	x2(1,*) = tdfitm	    
    	nplots = 2
    endelse	
    
    if (fitflag eq 1 AND ldfit eq 1) then begin 
    	x=x2
        xtitle = xtitles(1) 
        xunit  = 2     
        title(0)=maintitle(1)                  
    endif else begin
     	x=x1
        xunit  = 0
        xtitle = xtitles(0) 
        title(0)=maintitle(0)      	          
    endelse
      
  		;******************************************
		;*** if desired set up user axis scales ***
		;******************************************

    if (ldef1 eq 0) then begin
	  xmin1 = min(x1, max = xmax1)
	  xmin2 = min(x2, max = xmax2)	  
	  xmax1 = xmax1 * 1.1
	  xmax2 = xmax2 * 1.1	 
	  ymin = min( y, max = ymax)
	  ymin = ymin * 0.9
	  ymax = ymax *1.1
    endif



		;*************************************
		;**** Create graph display widget ****
		;*************************************

    graphid = widget_base(TITLE='ADAS503 GRAPHICAL OUTPUT', 		$
			  XOFFSET=1, YOFFSET=1)
    device, get_screen_size=scrsz
    xwidth = scrsz(0)*0.75
    yheight = scrsz(1)*0.75
    multiplot = 0
    bitval = bitfile + '/menu.bmp'

    adjust=1
    retain=1
    cwid = cw_adas_graph(graphid, print=hrdout, FONT=font,              $
                         xsize=xwidth, ysize=yheight,                   $
                         multiplot=multiplot, bitbutton=bitval,		$
                         adjust=adjust,retain=retain)

                ;**** Realize the new widget ****

    dynlabel, graphid
    widget_control, graphid, /realize

		;**** Get the id of the graphics area ****

    widget_control, cwid, get_value=grval
    win = grval.win

    wset, win
    
    if (fitflag eq 1 AND ldfit eq 1) then begin
    	adasplot, x2, y, itval, nplots, nmx, title, xtitle, xunit,xunits, ytitle, 		$
             strg, head1, head2a,head2b, teva, din, 			$
	     ldef1, xmin, xmax, ymin, ymax, lfsel, xlog=1, ylog=1
    endif else begin
    	adasplot, x1, y, itval, nplots, nmx, title, xtitle, xunit,xunits, ytitle, 		$
             strg, head1, head2a,head2b, teva, din, 			$
	     ldef1, xmin, xmax, ymin, ymax, lfsel, xlog=1, ylog=1    
    endelse
    
    if (ldef1 eq 0) then begin
	  xmin1 = 10^(!x.crange(0))
	  xmin2 = min(x2, max = xmax2)	  
	  xmax1 = 10^(!x.crange(1))
	  xmax2 = xmax2 * 1.1	  
	  ymin = 10^(!y.crange(0))
	  ymax = 10^(!y.crange(1))
    endif else begin
 	  xmin1 = 10^(!x.crange(0))
	  xmin2 = min(x2, max = xmax2)	  
	  xmax1 = 10^(!x.crange(1))
	  xmax2 = xmax2 * 1.1	     	
    endelse
    
		;*******************************************
		;**** Put the graphing data into common ****
		;*******************************************

    data = {	Y		:	y,			$
    		X		:	x,			$
		X1		:	x1,   			$
		X2		:	x2,   			$		
		ITVAL		:	itval,    		$
		NPLOTS		:	nplots,  		$
		NMX		:	nmx, 			$
            	TITLE		:	title,   		$
            	MAINTITLE   	:	maintitle,		$
            	XTITLE		:	xtitle,			$
            	XUNIT		:	xunit,			$
		XTITLES		:	xtitles,  		$
		XUNITS		:	xunits,  		$			
		YTITLE		:	ytitle,           	$
		YUNITS	 	:	yunits,			$
		STRG		:	strg,     		$
		HEAD1		:	head1,    		$
		HEAD2A		:	head2a,           	$
            	HEAD2B		:	head2b, 		$
		TEVA		:	teva,      		$
		DIN		:	din,     		$
		LDEF		:	ldef1, 			$		
		LDEF1		:	ldef1, 			$
		LDEF2		:	0, 			$		
		XMIN		:	xmin,			$
		XMAX		:	xmax,			$		
		XMIN1		:	xmin1,			$
		XMAX1		:	xmax1,			$
		XMIN2		:	xmin2,			$
		XMAX2		:	xmax2,			$		
		YMIN		:	ymin,			$
		YMAX		:	ymax,			$
		XLOG		: 	1,			$
		YLOG		: 	1,			$
		LFSEL		: 	lfsel,			$
		LDSEL		:	ldfit,			$		
		LDFIT		:	ldfit,			$
		fitflag		:	fitflag,		$
		DEVCODE		:	devcode,		$
		DEVLIST		:     	devlist,		$
		FONT		: 	font 		}

		;***************************
                ;**** make widget modal ****
		;***************************

    xmanager, 'adas503_plot', graphid, event_handler='adas503_plot_ev', $
              /modal,/just_reg
    gomenu = gomenucom

END
