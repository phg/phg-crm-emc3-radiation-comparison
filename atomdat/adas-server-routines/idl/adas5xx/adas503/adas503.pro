; Copyright (c) 1995, Strathclyde University .
; SCCS info: Module @(#)$Header: /home/adascvs/idl/adas5xx/adas503/adas503.pro,v 1.14 2004/07/06 10:55:07 whitefor Exp $	Date $Date: 2004/07/06 10:55:07 $
;+
; PROJECT:
;       ADAS IBM MVS to UNIX conversion
;
; NAME:
;	ADAS503
;
; PURPOSE:
;	The highest level routine for the ADAS 503 program.
;
; EXPLANATION:
;	This routine is called from the main adas system routine, adas.pro,
;	to start the ADAS 503 application.  Associated with adas503.pro
;	is a FORTRAN executable.  The IDL code provides the user
;	interface and output graphics whilst the FORTRAN code reads
;	in data files, performs numerical processing and creates the
;	output files.  The IDL code communicates with the FORTRAN
;	executable via a bi-directional UNIX pipe.  The unit number
;	used by the IDL for writing to and reading from this pipe is
;	allocated when the FORTRAN executable is 'spawned' (see code
;	below).  Pipe communications in the FORTRAN process are to
;	stdin and stdout, i.e streams 5 and 6.
;
;	The FORTRAN code is an independent process under the UNIX system.
;	The IDL process can only exert control over the FORTRAN in the
;	data which it communicates via the pipe.  The communications
;	between the IDL and FORTRAN must be exactly matched to avoid
;	input conversion errors.  The correct ammounts of data must be
;	passed so that neither process 'hangs' waiting for communications
;	which will never occur.
;
;	The FORTRAN code performs some error checking which is
;	independent of IDL.  In cases of error the FORTRAN may write
;	error messages.  To prevent these error messages from conflicting
;	with the pipe communications all FORTRAN errors are written to
;	output stream 0, which is stderr for UNIX.  These error messages
;	will appear in the window from which the ADAS session/IDL session
;	is being run.
;
;	In the case of severe errors the FORTRAN code may terminate
;	itself prematurely.  In order to detect this, and prevent the
;	IDL program from 'hanging' or crashing, the IDL checks to see
;	if the FORTRAN executable is still an active process before
;	each group of pipe communications.  The process identifier
;	for the FORTRAN process, PID, is recorded when the process is
;	first 'spawned'.  The system is then checked for the presence
;	of the FORTRAN PID.
;
; USE:
;	First the system settings must be established by calling
;	adas_sys_set.pro then adas503.pro is called to start the
;	ADAS 503 application;
;
;	adas_sys_set, adasrel, fortdir, userroot, centroot, $
;		      devlist, devcode, font_large, font_small, edit_fonts
;	adas503,   adasrel, fortdir, userroot, centroot, devlist, $
;		   devcode, font_large, font_small, edit_fonts
;
; INPUTS:
;	ADASREL - A string indicating the ADAS system version, 
;		  e.g ' ADAS RELEASE: ADAS93 V1.1.0'.  The first
;		  character should be a space.
;
;	FORTDIR - A string holding the path to the directory where the
;		  FORTRAN executables are, e.g '/disk/adas/fortran/exec'
;
;	USERROOT - A string holding the path to the root directory of
;		   the user's adas data. e.g '/disk/adas/adas'
;		   This root directory will be used by adas to construct
;		   other path names.  For example the users default data
;		   for adas503 should be in /disk/adas/adas/adf15.  In
;		   particular the user's default interface settings will
;		   be stored in the directory USERROOT+'/defaults'.  An
;		   error will occur if the defaults directory does not
;		   exist.
;
;	CENTROOT - Like USERROOT, but this directory points to the
;		   central data area for the system.  User defaults are
;		   not stored on CENTROOT.
;
;	DEVLIST - A string array of hardcopy device names, used for
;		  graphical output. e.g ['Post-Script','HP-PCL','HP-GL']
;		  This array must mirror DEVCODE.  DEVCODE holds the
;		  actual device names used in a SET_PLOT statement.
;
;	DEVCODE - A string array of hardcopy device code names used in
;		  the SET_PLOT statement, e.g ['PS', 'PCL', 'HP']
;		  This array must mirror DEVLIST.
;
;	FONT_LARGE - The name of a larger font e.g 'courier_bold14'
;
;	FONT_SMALL - The name of a smaller font e.g 'courier_bold12'
;
;	EDIT_FONTS - A structure of two fonts used in the table editor
;		     adas_edtab.pro, {font_norm:'',font_input:''}.
;		     The two fonts are used to differentiate between
;		     editable and non-editable parts of the table. You
;		     may have to experiment to get two fonts which give
;		     the same default spacing in the edit widget.
;		     e.g {font_norm:'helvetica_bold14', $
;			  font_input:'helvetica_oblique14'}
;
; OPTIONAL INPUTS:
;	None
;
; OUTPUTS:
;	None - Note this routine should leave all inputs un-modified.
;
; OPTIONAL OUTPUTS:
;	None
;
; KEYWORD PARAMETERS:
;	None
;
; CALLS:
;	FIND_PROCESS	Checks to see if a given process is active.
;	E3SPF0		Pipe comms with FORTRAN E3SPF0 routine.
;	E3ISPF		Pipe comms with FORTRAN E3ISPF routine.
;	XXDATE		Get date and time from operating system.
;	E3SPF1		Pipe comms with FORTRAN E3SPF1 routine.
;	E3OUTG		Pipe comms with FORTRAN E3OUTG routine.
;	ADASLOADCT		Sets up colour table for plots.
;
; SIDE EFFECTS:
;	This routine spawns a FORTRAN executable.  Note the pipe 
;	communications routines listed above.  In addition to these
;	pipe communications there is one explicit communication of the
;	date to the FORTRAN code, search for 'printf,pipe' to find it.
;
; CATEGORY:
;	Adas system.
;	
; WRITTEN:
;       Lalit Jalota, Tessella Support Services plc,  23-Feb-1995
;
; MODIFIED:
;	1.1	Lalit Jalota
;		First version.
;	1.2	Lalit Jalota
;	1.3	Tim Hammond
;		Corrected adasprog.
;	1.4	Tim Hammond
;		Updated version number to 1.4
;	1.5	Tim Hammond
;		Tidied up comments and code and added 'return to series
;		menu' functionality.
;	1.6	Tim Hammond
;		Updated version number to 1.5
;	1.7	William Osborn
;		Updated version number to 1.6
;	1.8	William Osborn
;		Added S.C.C.S. comment
;	1.9	William Osborn
;		Updated version number to 1.7
;	1.10	William Osborn
;		Updated version number to 1.8
;	1.11  Richard Martin
;		Increased version number to 1.9.
;	1.12	Richard Martin
;		Various updates for inclusion of interactive pair 
;		selector. Increased version number to 1.10.
;	1.13	Richard Martin
;		Introduced call to adasloadct to set up colour table
;		Increased version number to 1.11.
;	1.14	Richard Martin
;		Increased version number to 1.12.
;
; VERSION: 
;	1.1	28-02-95
;	1.2	13-03-95
;	1.3	05-06-95
;	1.4	15-11-95
;	1.5	07-02-96
;	1.6	16-02-96
;	1.7	13-05-96
;	1.8	13-05-96
;	1.9	12-07-96
;     1.10	14-10-96
;	1.11  01-11-97
;	1.12	13-10-99
;	1.13	18-09-00
;	1.14	18-03-02
;
;-
;-----------------------------------------------------------------------------


PRO ADAS503,	adasrel, fortdir, userroot, centroot, 			$
		devlist, devcode, font_large, font_small, edit_fonts

		;************************
		;**** Initialisation ****
		;************************

    adasprog = ' PROGRAM: ADAS503 V1.12'
    lpend = 0
    gomenu = 0
    deffile = userroot + '/defaults/adas503_defaults.dat'
    device = ''
    bitfile = centroot + '/bitmaps'

		;******************************************
		;**** Search for user default settings ****
		;**** If not found create defaults     ****
		;******************************************

    files = findfile(deffile)
    if files(0) eq deffile then begin
        restore, deffile
        inval.centroot = centroot+'/adf15/'
        inval.userroot = userroot+'/adf15/'
    endif else begin
        inval =   { 	ROOTPATH	:	userroot+'/adf15/', 	$
		  	FILE		:	'', 			$
		  	CENTROOT	:	centroot+'/adf15/', 	$
		  	USERROOT	:	userroot+'/adf15/' 	}
        procval = {	NMET		:	-1			}
        outval =  { 	GRPOUT		:	0, 			$
			GTIT1		:	'', 			$
			GRPSCAL		:	0, 			$
			XMIN		:	'',  			$
			XMAX		:	'',   			$
			YMIN		:	'',  			$
			YMAX		:	'',   			$
			HRDOUT		:	0, 			$
			HARDNAME	:	'', 			$
			GRPDEF		:	'', 			$
			GRPFMESS	:	'', 			$
			GRPSEL		:	-1, 			$
			GRPRMESS	:	'', 			$
			DEVSEL		:	-1, 			$
			GRSELMESS	:	'', 			$
			TEXOUT		:	0, 			$
			TEXAPP		:	-1, 			$
			TEXREP		:	0, 			$
			TEXDSN		:	'paper.txt', 		$
			TEXDEF		:	'',			$
			TEXMES		:	'' 			}
    endelse

		;****************************
		;*** Set up colour table ****
		;****************************
		
adasloadct

		;****************************
		;**** Start fortran code ****
		;****************************

    spawn, fortdir+'/adas503.out', unit=pipe, /noshell, PID=pid

   
		;************************************************
		;**** Get date and write to fortran via pipe ****
		;************************************************

    date = xxdate()
    printf, pipe, date(0)

LABEL100:
		;**** Check FORTRAN still running ****
  
    if find_process(pid) eq 0 then goto, LABELEND

		;************************************************
		;**** Communicate with e3spf0 in fortran and ****
		;**** invoke user interface widget for       ****
		;**** Data file selection                    ****
		;************************************************

    e3spf0, pipe, inval, dsfull, rep, FONT=font_large

		;**** If cancel selected then end program ****

    if rep eq 'YES' then begin 
	goto, LABELEND
    endif

		;**** Check FORTRAN still running ****

    if (find_process(pid) eq 0) then begin 
	goto, LABELEND
    endif

LABEL200:

		;**** Check FORTRAN still running ****

    if find_process(pid) eq 0 then goto, LABELEND

		;************************************************
		;**** Communicate with e3ispf in fortran and ****
		;**** invoke user interface widget for       ****
		;**** Processing options                     ****
		;************************************************

    e3ispf, pipe, lpend, procval, dsfull, gomenu, bitfile,  		$
	    FONT_LARGE=font_large, FONT_SMALL=font_small, 		$
	    EDIT_FONTS=edit_fonts

                ;**** If menu button clicked, tell FORTRAN to stop ****

    if gomenu eq 1 then begin
        printf, pipe, 1
        goto, LABELEND
    endif else begin
        printf, pipe, 0
    endelse
     
		;**** If cancel selected then goto 100 ****

    if lpend eq 1 then goto, LABEL100

                ;*************************************************
		;**** Fortran processing now in progress. Get ****
		;**** date and time for this run.	      ****
                ;*************************************************

    date = xxdate()

		;**** Create header for output. ****

    header = adasrel + adasprog + ' DATE: ' + date(0) + ' TIME: ' + date(1)

LABEL300:
		;**** Check FORTRAN still running ****

    if find_process(pid) eq 0 then goto, LABELEND

		;************************************************
		;**** Communicate with e3spf1 in fortran and ****
		;**** invoke user interface widget for       ****
		;**** Output options                         ****
		;************************************************

    e3spf1, pipe, lpend, outval, dsfull, header, bitfile, gomenu, 	$
            DEVLIST=devlist, FONT=font_large


                ;**** Extra check to see whether FORTRAN is still there ****

    if find_process(pid) eq 0 then goto, LABELEND

                ;**** If menu button clicked, tell FORTRAN to stop ****

    if gomenu eq 1 then begin
        printf, pipe, 1
        outval.texmes = ''
        goto, LABELEND
    endif else begin
        printf, pipe, 0
    endelse
                   
                ;**********************************************
		;**** If cancel selected then erase output ****
		;**** messages and goto 200.		   ****
                ;**********************************************

    if lpend eq 1 then begin
        outval.texmes = ''
        goto, LABEL200
    endif else if lpend eq 2 then begin
         outval.texmes = ''
        goto, LABEL100   
    endif

		;************************************************
		;**** If graphical output requested          ****
		;**** Communicate with e3outg in fortran and ****
		;**** invoke widget Graphical output         ****
		;************************************************

    if outval.grpout eq 1 then begin

		;**** User explicit scaling ****

        grpscal = outval.grpscal
        xmin = outval.xmin
        xmax = outval.xmax
        ymin = outval.ymin
        ymax = outval.ymax

		;**** Hardcopy output ****

        hrdout = outval.hrdout
        hardname = outval.hardname
        if outval.devsel ge 0 then device = devcode(outval.devsel)

		;**** optional user title/comment ****

        utitle = outval.gtit1

	  fitflag=procval.selbase
        e3outg, dsfull, pipe, utitle, grpscal, xmin, xmax, ymin, ymax, 	$
                fitflag,	$
		    hrdout, hardname, device, header, bitfile, gomenu,      $
                devcode,devlist,FONT=font_large

                ;**** If menu button clicked, tell FORTRAN to stop ****

        if gomenu eq 1 then begin
            printf, pipe, 1
            outval.texmes = ''
            goto, LABELEND
        endif else begin
            printf, pipe, 0
        endelse
    endif

		;**** Back for more output options ****

    GOTO, LABEL300

LABELEND:

                ;*********************************************
		;**** Ensure appending is not enabled for ****
		;**** text output at start of next run.   ****
                ;*********************************************

    outval.texapp = -1
    
    path=GETENV('ADASUSER')
    file=findfile('$ADASUSER/pass/.multigraph')
    if file(0) ne '' then begin
    	command='rm -f ' + file(0)
    	spawn, command	
    endif
		;**** Save user defaults ****

    save, inval, procval, outval, filename=deffile

END
