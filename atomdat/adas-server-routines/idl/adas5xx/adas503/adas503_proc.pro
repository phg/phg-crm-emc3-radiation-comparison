; Copyright (c) 1995, Strathclyde University .
; SCCS Info : Module   @(#)$Header: /home/adascvs/idl/adas5xx/adas503/adas503_proc.pro,v 1.5 2004/07/06 10:55:58 whitefor Exp $ Date $Date: 2004/07/06 10:55:58 $
;+
; PROJECT:
;       ADAS IBM MVS to UNIX conversion
;
; NAME:
;	ADAS503_PROC
;
; PURPOSE:
;	IDL ADAS user interface, processing options/input.
;
; EXPLANATION:
;	This routine creates and manages a pop-up window which allows
;	the user to select options and input data to control ADAS503
;	processing.
;
; USE:
;	This routine is ADAS503 specific, see e3ispf.pro for how it
;	is used.
;
; INPUTS:
;	PROCVAL	- A structure which determines the initial settings of
;		  the processing options widget.  The value is passed
;		  unmodified into cw_adas503_proc.pro.
;
;		  See cw_adas503_proc.pro for a full description of this
;		  structure.
;
;	DSFULL	- String; The full system file name of the input 
;		          dataset selected by the user for processing.
;
;	NSTORE  - Integer: maximum number of data blocks which can be
;			   read from the data set.
;
;	NTDIM	- Integer : Maximum number of temperatures allowed.
;
;	NDDIM	- Integer : Maximum number of densities allowed.
;
;	NDTIN	- Integer : Maximum number of temperature/density pairs
;		           allowed.
;
;	NBSEL   - Integer : number of data blocks accepted and read-in
;
;	CWAVEL  - String array : Wavelength (Angstroms) of data blocks
;				 dimension data block index (NSTORE).
;	CFILE   - String array : Specific Ion file source.
;				 dimension data block index (NSTORE).
;	CTYPE   - String array : processing code for each data block 
;				 dimension data block index (NSTORE) 
;	CINDM   - String array : Metastable index for each data block
;				 dimension data block index (NSTORE)
;	ITA     - Integer array: Number of electron temperatures
;				 dimension data block index (NSTORE)
;	IDA     - Integer array: Number of electron densities
;				 dimension data block index (NSTORE)
;	TVALS   - Double array : Electron temperatures - 
;				 1st dimension - temperature index
;				 2nd dimension - units 1 - Kelvin
;						       2 - eV
;						       3 - Reduced
;				 3rd dimension - data block index
;	TEDA	- Double array : Electron densities - 
;				 1st dimension - densities index
;				 2nd dimension - data block index.
;
;	SXB	- Double array : Ionisation per photon values  
;				 1st dimension - temperature index
;				 2nd dimension - densities index
;				 3rd dimension - data block index.
;
;       BITFILE - String; the path to the directory containing bitmaps
;                 for the 'escape to series menu' button.
;
; OPTIONAL INPUTS:
;	None
;
; OUTPUTS:
;	PROCVAL	- On output the structure records the final settings of
;		  the processing selections widget if the user pressed the
;		  'Done' button, otherwise it is not changed from input.
;
;	ACT	- String; 'Done', 'Cancel' or 'Menu' for the button the
;		  user pressed to terminate the processing options
;		  window.
;
; OPTIONAL OUTPUTS:
;	None
;
; KEYWORD PARAMETERS:
;	FONT_LARGE - The name of a larger font.
;
;	FONT_SMALL - The name of a smaller font.
;
;	EDIT_FONTS - A structure of two fonts used in the table editor
;		     adas_edtab.pro, {font_norm:'',font_input:''}.
;
; CALLS:
;	CW_ADAS503_PROC	Declares the processing options widget.
;	DYNLABEL	Recursively adds dynamic_resize keyword to label
;			widgets for version 4.0.1 and higher of IDL
;	ADAS503_PROC_EV Called indirectly during widget management,
;			routine included in this file.
;	XMANAGER
;
; SIDE EFFECTS:
;       This widget uses a common block PROC503_BLK, to pass the
;       variables VALUE and ACTION between the two routines. 
;
; CATEGORY:
;	Widgets
;
; WRITTEN:
;       Lalit Jalota, Tessella Support Services plc, 23-Feb-1995
;
; MODIFIED:
;       1.1     Lalit Jalota
;               First Release
;	1.2	Lalit Jalota
;	1.3	Tim Hammond
;               Tidied up comments and code and added 'return to menu'
;               bitmapped button.
;	1.4	William Osborn
;		Added dynlabel procedure
;	1.5	Richard Martin
;		Add PEC array to input and output for interactive pair
;		selector
;
; VERSION:
;	1.1	28-02-95
;	1.2	23-03-95
;	1.3	07-02-96
;	1.4	12-07-96
;	1.5	13-10-99
;
;-
;-----------------------------------------------------------------------------

PRO adas503_proc_ev, event

    COMMON proc503_blk, action, value

    action = event.action
    CASE event.action OF

		;**** 'Done' button ****

	'Done'  : begin

		;**** Get the output widget value ****

	    widget_control, event.id, get_value=value 
            widget_control, event.top, /destroy
	end

		;**** 'Cancel' button ****

	'Cancel': widget_control, event.top, /destroy

                ;**** 'Menu' button ****

        'Menu': widget_control, event.top, /destroy

        ELSE:                   ;**** Do nothing ****

    ENDCASE

END

;-----------------------------------------------------------------------------

PRO adas503_proc, procval, dsfull, nstore, ntdim, nddim, ndtin, nbsel, 	$
		  cwavel, cfile, ctype, cindm, ita, ida, tvals, teda, pec,	$
		  act, bitfile, FONT_LARGE=font_large, 			$
                  FONT_SMALL=font_small, EDIT_FONTS=edit_fonts


		;**** declare common variables ****

    COMMON proc503_blk, action, value

		;**** Copy "procval" to common ****

    value = procval

		;**** Set defaults for keywords ****

    IF NOT (KEYWORD_SET(font_large)) THEN font_large = ''
    IF NOT (KEYWORD_SET(font_small)) THEN font_small = ''
    IF NOT (KEYWORD_SET(edit_fonts)) THEN 				$
    edit_fonts = { font_norm:'', font_input:''}
		
                ;**** create base widget ****

    procid = widget_base(TITLE='ADAS503 PROCESSING OPTIONS', 		$
			 XOFFSET=50, YOFFSET=1)

		;**** Declare processing widget ****

    cwid = cw_adas503_proc(procid, dsfull, nstore,  			$
			   ntdim, nddim, ndtin, nbsel, 			$
			   cwavel, cfile, ctype, cindm, 		$
			   ita, ida, tvals, teda, pec,			$
			   bitfile, PROCVAL=value, 			$
		 	   FONT_LARGE=font_large, FONT_SMALL=font_small,$
			   EDIT_FONTS=edit_fonts)

		;**** Realize the new widget ****

    dynlabel, procid
    widget_control, procid, /realize

		;**** make widget modal ****

    xmanager, 'adas503_proc', procid, event_handler='adas503_proc_ev', 	$
	      /modal, /just_reg

		;*** copy value back to procval for return to e3ispf ***

    act = action
    procval = value
 
END

