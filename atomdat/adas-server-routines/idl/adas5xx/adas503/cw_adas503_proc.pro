; Copyright (c) 1995, Strathclyde University.
; SCCS info: Module @(#)$Header: /home/adascvs/idl/adas5xx/adas503/cw_adas503_proc.pro,v 1.9 2006/05/24 11:26:41 allan Exp $	Date  $Date: 2006/05/24 11:26:41 $
;+
; PROJECT:
;       ADAS IBM MVS to UNIX conversion
;
; NAME:
;	CW_ADAS503_PROC()
;
; PURPOSE:
;	Produces a widget for ADAS503 processing options/input.
;
; EXPLANATION:
;	This function creates a compound widget consisting of :-
;	   a text widget holding an editable 'Run title', 
;	   the dataset name/browse widget cw_adas_dsbr,
;	   a widget to select the data block for analysis,
;	   a widget to request a mimax fit and enter a tolerance for it,
;	   a table widget for temperature/density data cw_adas_table,
;	   buttons to enter default values into the table, 
;	   a message widget, 
;          an 'Escape to series menu', a 'Cancel' and a 'Done' button.
;
;	The compound widgets included in this widget are self managing.
;	This widget only manages events from the two 'Defaults' buttons,
;	the 'Cancel' button and the 'Done' button and the
;       'escape to series menu' button.
;
; USE:
;	This widget is specific to ADAS503, see adas503_proc.pro
;	for use.
;
; INPUTS:
;	TOPPARENT- Long integer, ID of parent widget.
;
;	DSFULL	- String; The full system file name of the input 
;		  dataset selected by the user for processing.
;
;	NSTORE  - Integer; maximum number of data blocks allowed.
;
;	NTDIM	- Integer; maximum number of temperatures allowed.
;
;	NDDIM	- Integer; maximum number of densities allowed.
;
;	NDTIN	- Integer; number of temp/density pairs selected
;
;	NBSEL   - Integer; number of data blocks included in this file
;
;	CWAVEL  - String array; wavelength values from file
;				dimension - NBSEL
;
;	CFILE   - String array; specific ion source file
;				dimension - NBSEL
;
;	CTYPE  - String array; Data type code
;				dimension - NBSEL
;
;	CINDM   - String array; Metastable index
;				dimension - NBSEL
;
;	ITA     - Integer array; Number of temperatures per block
;				dimension - NBSEL
;
;	IDA     - Integer array; Number of densities per block
;				dimension - NBSEL
;
;	TVALS   - Float array; temperature values from data file
;				dimension - NBSEL, NTDIM
;
;	TEDA    - Float array; density values from data file
;				dimension - NBSEL, NDDIM
;
;	PEC	  - Float array; pec data from data file
;				dimension - NTDIM, NDDIM, NBSEL
;
;	The inputs map exactly onto variables of the same
;	name in the ADAS503 FORTRAN program.
;
;       BITFILE - String; the path to the directory containing bitmaps
;                 for the 'escape to series menu' button.
;
; OPTIONAL INPUTS:
;	None
;
; OUTPUTS:
;	The return value of this function is the ID of the declared widget.
;
; OPTIONAL OUTPUTS:
;	None
;
; KEYWORD PARAMETERS:
;	PROCVAL	- A structure which determines the initial settings of
;		  the processing options widget.
;
;		  The default PROCVAL is created thus;
;
;    		str_arr = strarr(nbsel)
;	     	temp_arr = fltarr(ntdim)
;     		dens_arr = fltarr(nddim)
;     		ps = {proc503_set, $
;			nmet  : 0,		$
;                	title : '',             $
;			nbsel : 0 ,		$
;                	ibsel : 1 ,             $
;                	uwavel: str_arr,        $
;                	ufile : str_arr,        $
;                	utype : str_arr,        $
;                	uindm : str_arr,        $
;			ifout : 1 ,             $
;			ldfit : 0,		$
;			maxt  : 0, 		$
;			maxd  : 0, 		$
;                	tin   : temp_arr,       $
;                	din   : dens_arr,       $
;                	ifsel : 0,              $
;			selbase: 1,			$
;                	tolval: '5'		$
;             }
;
;
;		NMET    Flag which defines whether or not default values
;			exist or not. (< 0 if no values exist)
; 		TITLE	Entered general title for program run
;		IBSEL   Selected data block
;		NBSEL   Number of blocks in data file
;		UWAVEL  Array of wavelengths from data file
;		UFILE   Ion source files from data file
;		UTYPE   data type codes from data file
;		UINDM	Metastable indices from data file
;		IFOUT   Index indicating which units are being used
;		LDFIT   Flag for whether temp. or desity selected. 
;			0 = temperature, 1 = density
;		MAXT    Number of temperature values selected
;		MAXD    Number of density values selected
;		TIN     User supplied temperature values for fit.
;		DIN 	User supplied density values for fit.
;		IFSEL   Flag as to whether polynomial fit is chosen
;		SELBASE Flag as to whether polynomial fit base is sensitive
;		TOLVAL  Tolerance required for goodness of fit if
;			polynomial fit is selected.
;
;		All of these structure elements map onto variables of
;		the same name in the ADAS503 FORTRAN program.
;
;
;	UVALUE	- A user value for the widget. Default 0.
;
;	FONT_LARGE - The name of a larger font.  Default current system
;		     font
;
;	FONT_SMALL - The name of a smaller font. Default current system
;		     font.
;
;	EDIT_FONTS - A structure of two fonts used in the table editor
;		     adas_edtab.pro, {font_norm:'',font_input:''}.  Defaults
;		     to current system font.
;
;	NUM_FORM   - String; Numeric format to use in tables.  Default
;			'(E10.3)'
;		     
;
; CALLS:
;	POPUP		Popup warning window with buttons.
;	CW_ADAS_DSBR	Dataset name and comments browsing button.
;	CW_ADAS_TABLE	Adas data table widget.
;	CW_ADAS_SEL	Adas multiple selection widget.
;	CW_SINGLE_SEL	Adas scrolling table and selection widget.
;	CW_OPT_VALUE    Adas option selection widget with required input 
;			value.
;	CW_ADAS_PAIRSEL Adas pair value selector. 
;
;	See side effects for other related widget management routines.
;
; SIDE EFFECTS:
;
;	The following widget management routines are included in this file;
;	PROC503_GET_VAL()	Returns the current PROCVAL structure.
;	PROC503_EVENT()		Process and issue events.
;	
; CATEGORY:
;	Compound Widget
;
; WRITTEN:
;       Lalit Jalota, Tessella Support Services plc, 23-Feb-1995
;
; MODIFIED:
;	1.1     Lalit Jalota
;               First release
;       1.2     Lalit Jalota
;	1.3	Tim Hammond, Tessella Support Services plc
;		(Previous modification history unknown)
;		Added updating of table values whenever user chooses
;		a new line index.
;	1.4	Tim Hammond
;               Tidied up comments and code and added bitmapped 'return
;               to series menu' button.
;               Also prevented crashing when user attempts to proceed
;               with an empty table.
;	1.5	Tim Hammond
;               Modified use of fonts and sizes of tables for use on
;               different platforms.
;	1.6	Tim Hammond
;		Corrected machine selection criterion
;	1.7	Richard Martin
;		Added 'Default Te' button and 'Interactive Pair Selector'
;		button.
;	1.8	Richard Martin
;		IDL 5.5 fixes.
;	1.9	Allan Whiteford
;		Removed necessity for temperatures to be in ascending order
;		Non-monotonically increasing tempertures may have a knock-on
;		effect to the plotting routines but there is no good reason
;		to limit the temperatures like this. Also fixes problem
;		report by Hugh where selecting default Ne and then trying
;		to edit the table resulted in an error.
;
; VERSION:
;	1.1	28-02-95
;	1.2	10-03-95
;	1.3	22-01-96
;	1.4	07-02-96
;	1.5	27-02-96
;	1.6	27-02-96
;	1.7	13-10-99
;	1.8	28-01-02
;	1.9	24-05-06
;
;-
;-----------------------------------------------------------------------------

FUNCTION proc503_get_val, id

                ;**** Return to caller on error ****

    ON_ERROR, 2

		 ;***************************************
                 ;****     Retrieve the state 	     ****
		 ;**** Get first_child widget id     ****
		 ;**** because state is stored there ****
		 ;***************************************

    first_child = widget_info(id, /child)
    widget_control, first_child, get_uvalue = state

		;***********************************
		;**** Get run title from widget ****
		;**** Then centre in in string  ****
		;**** of 40 characters 		****
		;***********************************

    widget_control, state.runid, get_value=title
    title_len = strlen(title(0)) 
    if (title_len gt 40 ) then begin 
	title = strmid(title,0,38)
        widget_control, state.messid, set_value="Title too long - truncated "
        widget_control, state.runid, set_value=title
	wait, 1
    endif
    pad = (40 - title_len)/2 
    spaces = '                                         '
    title(0) = strmid(spaces,0,(pad-1)) + title(0) + strmid(spaces,0,(pad-1))
  

		;****************************************************
		;**** Get new temperature data from table widget ****
		;****************************************************

    widget_control, state.tempid, get_value=tempval
    tabledata = tempval.value
    ifout = tempval.units + 1
    ldfit = state.ldfit

		;**** Copy out temperature values ****

    tin = dblarr(state.ntdim)
    din = dblarr(state.nddim)
    blanks = where(strtrim(tabledata(0,*),2) eq '')

                ;***********************************************
		;**** next line assumes that all blanks are ****
		;**** at the end of the columns             ****
                ;***********************************************

    if blanks(0) ge 0 then maxt=blanks(0) else maxt=state.ntdim

                ;*************************************************
                ;**** Only perform following if there is 1 or ****
                ;**** more value present in the table         ****
                ;*************************************************
  
    if (maxt ge 1) then begin
  	tin(0:maxt-1) = double(tabledata(0,0:maxt-1))
    endif

                ;**** Now do same for densities ****
  
    blanks = where(strtrim(tabledata(4,*),2) eq '')
    if blanks(0) ge 0 then maxd=blanks(0) else maxd=state.nddim
    if (maxd ge 1) then begin
  	din(0:maxd-1) = double(tabledata(4,0:maxd-1))
    endif

		;**** Fill in the rest with zero ****

    if maxt lt state.ntdim then begin
        tin(maxt:state.ntdim-1) = double(0.0)
    endif
    if maxd lt state.nddim then begin
        din(maxd:state.nddim-1) = double(0.0)
    endif 

		;*************************************************
		;**** Get selection of polyfit from widget    ****
		;*************************************************

    widget_control, state.optid, get_uvalue=polyset, /no_copy
    ifsel = polyset.optionset.option[0]
    if (num_chk(polyset.optionset.value[0]) eq 0) then begin
        tolval = (polyset.optionset.value[0])
    endif else begin
        tolval = -999
    endelse
    widget_control, state.optid, set_uvalue=polyset, /no_copy

		;***********************************************
		;**** get new index for data block selected ****
		;***********************************************

    widget_control, state.indexid, get_value=select_block
    ibsel = select_block  

		;***********************************************
		;**** write selected values to PS structure ****
		;***********************************************

     ps = {  	nmet  : 0, 						$
                title : title,          				$
		nbsel : state.nbsel, 					$
                ibsel : ibsel ,             				$
		uwavel: state.uwavel,					$
		ufile : state.ufile,					$
		utype : state.utype,					$
		uindm : state.uindm, 					$
		ifout : ifout,						$
		ldfit : ldfit, 						$
		maxt  : maxt,						$
		maxd  : maxd,						$
                tin   : tin,       					$
                din   : din,       					$
                ifsel : ifsel,              			$
                selbase: state.selbase,				$
                tolval: tolval						}
   
    widget_control, first_child, set_uvalue=state, /no_copy

    RETURN, ps

END

;-----------------------------------------------------------------------------

FUNCTION proc503_event, event

                ;**** Base ID of compound widget ****

    parent = event.handler

		;**********************************************
                ;**** Retrieve the user value state        ****
		;**** Get id of first_child widget because ****
		;**** user value "state" is stored there   ****
		;**********************************************

    first_child = widget_info(parent, /child)
    widget_control, first_child, get_uvalue=state,/no_copy

		;*********************************
		;**** Clear previous messages ****
		;*********************************

    widget_control, state.messid, set_value=' '

                ;************************
                ;**** Process events ****
                ;************************

    CASE event.id OF

                ;*************************************
                ;****      Select index           ****
                ;*************************************

    	state.indexid: begin

                ;**** Get current index select value ****

            widget_control, state.indexid, get_value=ibsel
            ntdim = state.ntdim
            ntval = state.ita(ibsel)
            ndval = state.ida(ibsel)

                ;**** Get current table widget value ****

            widget_control, state.tempid, get_value=tempval

                ;**** Copy new ibsel values into structure  ****

            if (ntval gt 0) then begin
                tempval.value(1,0:ntval-1 ) =                        	$
                strtrim(string(state.tvals(0:ntval-1,0,ibsel),        	$
                format=state.num_form),2)
                tempval.value(2,0:ntval-1) =                       	$
                strtrim(string(state.tvals(0:ntval-1,1,ibsel),     	$
                format=state.num_form),2)
                tempval.value(3,0:ntval-1) =                       	$
                strtrim(string(state.tvals(0:ntval-1,2,ibsel),    	$
                format=state.num_form),2)
                if (ndval gt 0) then begin
                    tempval.value(5,0:ndval-1) =                   	$
                    strtrim(string(state.teda(0:ndval-1,ibsel),     	$
                    format=state.num_form),2)
    
                ;**** fill rest of table with blanks ****

                    if (ndval lt ntdim) then begin
                        tempval.value(5,ndval:ntdim-1) = ' '
                    endif
	        endif
                if (ntval lt ntdim ) then begin
                    tempval.value(1,ntval:ntdim-1 ) = ' '
                    tempval.value(2,ntval:ntdim-1 ) = ' '
                    tempval.value(3,ntval:ntdim-1 ) = ' '
                endif

		;***************************************
                ;**** Copy new data to table widget ****
                ;**** reset state.ibsel             ****
		;***************************************

                widget_control, state.tempid, set_value=tempval
                state.ibsel = ibsel
            endif
        end

		;*************************************
		;**** Default temperature button ****
		;*************************************

    	state.deftid: begin

		;**** popup window to confirm overwriting current values ****

	    action= popup(message='Confirm Overwrite values with Defaults',$
			  buttons=['Confirm','Cancel'], font=state.font)
	    if action eq 'Confirm' then begin

		;**** Get current table widget value ****

 	   	widget_control,state.tempid,get_value=tempval

		;*** let user select the default density value ****

	   	dvalues = strarr(state.maxd)
	   	for i = 0, state.maxd-1 do begin
              	    dvalues(i) =  strcompress(string(tempval.value(5,i)))
           	endfor
	   	action= popup(message='Select Default density value', 	$
			      buttons=dvalues, /column, font=state.font)
           	defdensity = action
   
            ;***********************************************
		;**** Copy defaults into value structure    ****
		;**** For default values use the existing   ****
		;**** table values and let user modify them ****
		;**** then set all density values to same   ****
		;**** value  				    ****
            ;***********************************************

             	deft = tempval
             	units = tempval.units+1
	     	maxt = state.maxt
	     	maxd = state.maxd
 	     	if maxt gt 0 then begin
   	           tempval.value(0,0:maxt-1) = 				$
		   strtrim(string(deft.value(units,0:maxt-1), 		$
                   format=state.num_form),2)
   	           tempval.value(4,0:maxt-1) =  			$
		   string(defdensity, format=state.num_form)
 	  	endif

		;**** Fill in the rest with blanks ****

 	  	if maxt lt state.ntdim then begin
   	            tempval.value(0,maxt:state.ntdim-1) = ''
                    tempval.value(4,maxt:state.nddim-1) = ''
 	  	endif

                ;***************************************
		;**** Set ldfit variable to 0 i.e.  ****
		;**** temperature selected          ****
                ;***************************************

	  	state.ldfit = 0

		;**** Copy new data to table widget ****

 	  	widget_control,state.tempid, set_value=tempval
 	  	
 	  	;**** Make Polynomial fit option base sensitive ****
 	  	
 	  	widget_control,state.optid,sensitive=1
 	  	state.selbase=1
 	  	
	    endif
        end

		;*************************************
		;**** Default Density button ****
		;*************************************

    	state.defdid: begin

		;**** popup window to confirm overwriting current values ****

	    action= popup(message='Confirm Overwrite values with Defaults',$
			  buttons=['Confirm','Cancel'], font=state.font)
	    if action eq 'Confirm' then begin

		;**** Get current table widget value ****

 	   	widget_control,state.tempid,get_value=tempval

		;*** let user select the default density value ****

	   	tvalues = strarr(state.maxt)
	   	for i = 0, state.maxt-1 do begin
              	    tvalues(i) =  strcompress(string(tempval.value(1,i)))
           	endfor
	   	action= popup(message='Select Default temperature value', 	$
			      buttons=tvalues, /column, font=state.font)
           	deftemp = action
   		
            ;***********************************************
		;**** Copy defaults into value structure    ****
		;**** For default values use the existing   ****
		;**** table values and let user modify them ****
		;**** then set all density values to same   ****
		;**** value  				    ****
            ;***********************************************

            defd = tempval
            units = tempval.units+1
	     	maxt = state.maxt
	     	maxd = state.maxd
 	     	if maxd gt 0 then begin
   	           tempval.value(4,0:maxd-1) = 				$
		   strtrim(string(defd.value(5,0:maxd-1), 		$
                   format=state.num_form),2)
   	           tempval.value(0,0:maxd-1) =  			$
		   string(deftemp, format=state.num_form)
 	  	endif

		;**** Fill in the rest with blanks ****

 	  	if maxd lt state.nddim then begin
   	            tempval.value(0,maxd:state.ntdim-1) = ''
                    tempval.value(4,maxd:state.nddim-1) = ''
 	  	endif

                ;***************************************
		;**** Set ldfit variable to 0 i.e.  ****
		;**** density selected          ****
                ;***************************************

	  	state.ldfit = 1

		;**** Copy new data to table widget ****

 	  	widget_control,state.tempid, set_value=tempval
 	  	
 	  	;**** Make Polynomial fit option base sensitive ****
 	  	
 	  	widget_control,state.optid,sensitive=1
 	  	state.selbase=1
 	  	
	    endif
        end
	  
	state.tempid: begin
 	  	widget_control,state.optid,sensitive=1
 	  	state.selbase=1	
	 end
		
    	state.ipsid: begin   
 	  	  widget_control,state.tempid, get_value=tempval 
 	  	  units=tempval.units 			
		  newt=fltarr(state.ntdim)
		  newd=fltarr(state.nddim)	  		  
		  ntindex=intarr(state.ntdim)
		  ndindex=intarr(state.nddim)
		  inval = {   numte : state.ntdim,  	  $
		  		  numden: state.nddim,  	  $
		  		  nbsel : state.ibsel,  	  $
		  		  te    : state.ita,		  $
		  		  den   : state.ida,		  $
		  		  tval  : state.tvals(*,*,state.ibsel),$
		  		  dval  : state.teda(*,state.ibsel), $
		  		  newt  : newt,			  $
		  		  newd  : newd,			  $
				  ntindex: ntindex,		  $
				  ndindex: ndindex,		  $
		  		  units : units,			  $
		  		  pec   : state.pec(*,*,state.ibsel)	  }
		  cw_adas_pairsel, VALUE=inval, XSIZE = xsize, YSIZE = ysize, $
              FONT_LARGE = state.font

                ;**** Get current table widget value ****

            widget_control, state.tempid, get_value=tempval

                ;**** Copy new ibsel values into structure  ****

	      ntval1=where(inval.newt gt 0.0,count)
	      ntval1=count
	      ntval2= state.ita(state.ibsel)
            ndval1= ntval1
            ndval2 = state.ida(state.ibsel)
            ntdim=state.ntdim	      
            if (ntval1 gt 0) then begin
                tempval.value(0,0:ntval1-1 ) =                        	$
                	strtrim(string(inval.newt(0:ntval1-1),        	$
                	format=state.num_form),2)
                tempval.value(1,0:ntval2-1) =                       	$
                	strtrim(string(state.tvals(0:ntval2-1,0,state.ibsel),     	$
                	format=state.num_form),2)
                tempval.value(2,0:ntval2-1) =                       	$
                	strtrim(string(state.tvals(0:ntval2-1,1,state.ibsel),     	$
                	format=state.num_form),2)
                tempval.value(3,0:ntval2-1) =                       	$
                	strtrim(string(state.tvals(0:ntval2-1,2,state.ibsel),    	$
                	format=state.num_form),2)

                tempval.value(4,0:ndval1-1) =                        	$
                	strtrim(string(inval.newd(0:ntval1-1),        	$
                	format=state.num_form),2)
                if (ndval2 gt 0) then begin    
                    tempval.value(5,0:ndval2-1) =                   	$
                    strtrim(string(state.teda(0:ndval2-1,state.ibsel),     	$
                    format=state.num_form),2)
    
                ;**** fill rest of table with blanks ****

                    if (ndval2 lt ntdim) then begin
                        tempval.value(5,ndval2:ntdim-1) = ' '
                    endif
	          endif
                if (ndval1 lt ntdim) then begin
                    tempval.value(4,ndval1:ntdim-1) = ' '
                endif			
	          	          
                if (ntval1 lt ntdim ) then begin
                     tempval.value(0,ntval1:ntdim-1 ) = ' '               
                endif
                if (ntval2 lt ntdim ) then begin
                    tempval.value(1,ntval2:ntdim-1 ) = ' '               
                    tempval.value(2,ntval2:ntdim-1 ) = ' '
                    tempval.value(3,ntval2:ntdim-1 ) = ' '
                endif

		;***************************************
                ;**** Copy new data to table widget ****
                ;**** reset state.ibsel             ****
		;***************************************

                widget_control, state.tempid, set_value=tempval
                
 	  	
 	  	;**** Desensitise Polynomial fit option base ****
 	  	
 	  	widget_control,state.optid,sensitive=0
 	  	state.selbase=0
                
            endif
	end

		;***********************
		;**** Cancel button ****
		;***********************

    	state.cancelid: new_event = {ID:parent, TOP:event.top, 		$
				     HANDLER:0L, ACTION:'Cancel'}

		;*********************
		;**** Done button ****
		;*********************

    	state.doneid: begin

		;***************************************
		;**** Check all user input is legal ****
		;***************************************

	    error = 0

        	;********************************************
		;*** Have to restore "state" before calls ***
		;*** to proc503_get_val so it can be used ***
		;*** there.				  ***
		;********************************************
		
	    widget_control, first_child, set_uvalue=state, /no_copy
	    widget_control, event.handler, get_value=ps

		;*** reset state variable ***

	    widget_control, first_child, get_uvalue=state, /no_copy

		;**** check temp/density values entered ****

            if error eq 0 then begin
                widget_control, state.tempid, get_value=tabval
                tabvals =                                               $
                where(strcompress(tabval.value(0,*), /remove_all) ne '')
                if tabvals(0) eq -1 then begin
                    error = 1
                    message = '**** Error: No temperatures/densities'+  $
                              ' entered ****'
                endif
            endif

		;*** Check to see if index has been selected ***

	    if error eq 0 and ps.ibsel lt 0 then begin
	        error = 1
	        message='**** Error: Invalid block selected ****'
	    end

		;*** Check to see if sensible tolerance is selected.

	    if (error eq 0 and ps.ifsel eq 1) then begin 		$
	        if (float(ps.tolval) lt 0) or 				$
	        (float(ps.tolval) gt 100) then begin
	            error = 1
	            message='**** Error: Tolerance for polyfit must '+	$
                            'be 0-100% ****'
	        endif
	    endif

		;**** return value or flag error ****

	    if error eq 0 then begin
	        new_event = {ID:parent, TOP:event.top, 			$
                             HANDLER:0L, ACTION:'Done'}
            endif else begin
	        widget_control, state.messid, set_value=message
	        new_event = 0L
            endelse
        end

                ;**** Menu button ****

        state.outid: begin
            new_event = {ID:parent, TOP:event.top, HANDLER:0L,          $
                         ACTION:'Menu'}
        end

    ELSE: new_event = 0L

  ENDCASE

		;*********************************************
		;*** make "state" available to other files ***
		;*********************************************

    widget_control, first_child, set_uvalue=state, /no_copy

    RETURN, new_event

END

;-----------------------------------------------------------------------------

function cw_adas503_proc, topparent, dsfull, nstore, ntdim, nddim, 	$
                          ndtin, nbsel, cwavel, cfile, ctype, cindm, 	$
		          ita, ida, tvals, teda, pec, bitfile, 		$
                          procval=procval, uvalue=uvalue, 		$
                          font_large=font_large, font_small=font_small, $
                          edit_fonts=edit_fonts, num_form=num_form

		;**** Set defaults for keywords ****

    if not (keyword_set(uvalue)) then uvalue = 0
    if not (keyword_set(font_large)) then font_large = ''
    if not (keyword_set(font_small)) then font_small = ''
    if not (keyword_set(edit_fonts)) then 				$
    edit_fonts = {font_norm:'',font_input:''}
    if not (keyword_set(num_form)) then num_form = '(E10.3)'
    if not (keyword_set(procval)) then begin
         str_arr  = strarr(ntdim)
         temp_arr = fltarr(ntdim)
         dens_arr = fltarr(nddim)
         ps = { nmet  : 0, 						$
		title : '',             				$
		nbsel : 0 , 						$
                ibsel : 0 ,             				$
                uwavel: str_arr,        				$
                ufile : str_arr,        				$
                utype : str_arr,        				$
                uindm : str_arr,        				$
		ifout : 1,						$
		ldfit : 0,              				$
		maxt  : 0,						$
		maxd  : 0,						$
                tin   : temp_arr,       				$
                din   : dens_arr,       				$
                ifsel : 0,              				$
                selbase:1,						$
                tolval: '5'						}
    endif else begin
	ps = {  nmet  : procval.nmet,      				$
		title : procval.title,     				$
		nbsel : procval.nbsel,     				$
                ibsel : procval.ibsel,     				$
        	uwavel: strarr(n_elements(cwavel)),  			$
        	ufile : strarr(n_elements(cfile)),  			$
        	utype : strarr(n_elements(ctype )),  			$
        	uindm : strarr(n_elements(cindm)),  			$ 
		ifout : procval.ifout,	   				$
		ldfit : procval.ldfit,     				$
		maxt  : procval.maxt,	   				$
                maxd  : procval.maxd,      				$
                tin   : procval.tin,       				$
                din   : procval.din,       				$
                ifsel : procval.ifsel,     				$
                selbase:procval.selbase,					$
                tolval: procval.tolval     				}
    endelse
    uwavel= cwavel
    ufile = cfile
    utype = ctype 
    uindm = cindm

                ;*********************************************************
                ;**** Modify certain parameters and results depending ****
                ;**** on the machine being used                       ****
                ;*********************************************************

    machine = GETENV('TARGET_MACHINE')
    if machine eq 'HPUX' then begin
        y_size = 4
        large_font = font_small
    endif else begin
        y_size = 6
        large_font = font_large
    endelse

		;****************************************************
		;**** Assemble temperature  & density table data ****
		;****************************************************
		;**** The adas table widget requires data to be *****
		;**** input as strings, so all the numeric data *****
		;**** has to be written into a string array.    *****
		;**** Declare temp. & dens.table array          *****
		;**** col 1 has user temperature values         *****
		;**** col 2 has temperature values from files,  *****
		;**** which can have one of three possible units*****
		;**** col 5 has user density values             *****
		;**** col 6 has density values from file.       *****
		;****************************************************

    tabledata = strarr(6,ndtin)

                ;***********************************************
		;**** Copy out temperature & density values ****
		;**** number of temperature and density     ****
		;**** values for this data block            ****
                ;***********************************************

    if (ps.ibsel gt nbsel) then ps.ibsel = 0
    maxt = ita(ps.ibsel) 
    maxd = ida(ps.ibsel)
    if (maxt gt 0) then begin
        tabledata(0,*) = 						$
	strtrim(string(ps.tin(*),format=num_form),2)
        tabledata(1,0:maxt-1) = 					$
        strtrim(string(tvals(0:maxt-1,0,ps.ibsel),format=num_form),2)
        tabledata(2,0:maxt-1) = 					$
	strtrim(string(tvals(0:maxt-1,1,ps.ibsel),format=num_form),2)
        tabledata(3,0:maxt-1) = 					$
        strtrim(string(tvals(0:maxt-1,2,ps.ibsel),format=num_form),2)
        tabledata(4,*) = 						$
     	strtrim(string(ps.din(*),format=num_form),2)
        tabledata(5,0:maxd-1) = 					$
	strtrim(string(teda(0:maxd-1,ps.ibsel),format=num_form),2)

		;**** fill rest of table with blanks ****

        blanks = where(ps.tin eq 0.0) 
        tabledata(0,blanks) = ' ' 
        tabledata(1:3,maxt:ntdim-1) = ' '
        blanks = where(ps.din eq 0.0) 
        tabledata(4,blanks) = ' ' 
        tabledata(5,maxd:nddim-1) = ' '
    endif

		;********************************************************
		;**** Create the 503 Processing options/input window ****
		;********************************************************

		;**** create titled base widget ****

    parent = widget_base(topparent, UVALUE = uvalue, 			$
			 title = 'ADAS503 PROCESSING OPTIONS', 		$
			 EVENT_FUNC = "proc503_event", 			$
			 FUNC_GET_VALUE = "proc503_get_val", 		$
			 /COLUMN)

		;******************************************************
		;**** Create a dummy widget just to hold value of *****
		;**** "state" variable so as not to get confused  *****
		;**** with any other values. Adopt IDL practice   *****
		;**** of using first child widget                 *****
		;******************************************************

    first_child = widget_base(parent)
    topbase = widget_base(first_child,/column)

		;***********************
		;**** add run title ****
		;***********************

    base = widget_base(topbase, /row)
    rc = widget_label(base, value='Title for Run', font=large_font)
    runid = widget_text(base, value=ps.title, xsize=38, font=large_font, /edit)
  
		;**** add dataset name and browse button ****

    rc = cw_adas_dsbr(topbase, dsfull, font=large_font)

		;***************************************************
		;**** add a window to display and select index   ***
		;**** first create data array for table          ***
		;**** Then convert to 1D string array(text_table)***
		;**** call cw_single_sel.pro for options choice  ***
		;***************************************************

    block_info = strarr(4, nbsel)
    block_info(0,*) = cwavel
    block_info(1,*) = cfile
    block_info(2,*) = ctype 
    block_info(3,*) = cindm
    titles = [['Wavelength', 'Ion', 'Processing', 'Metastable'], 	$
   	      ['','Source','Code', 'Index']]
    select_data = text_table(block_info, colhead=titles)
    coltitles = select_data(0:1)
    select_data = select_data(2:nbsel+1)
    indexid = cw_single_sel( parent, select_data, value=ps.ibsel, 	$
			     title='Select data Block', 		$
			     coltitles=coltitles, ysize=(y_size-2),	$
			     font=font_small, big_font=large_font)

		;**********************
		;**** Another base ****
		;**********************

    tablebase = widget_base(parent, /row)

		;************************************************
		;**** base for the table and defaults button ****
		;************************************************

    base = widget_base(tablebase, /column, /frame)

		;********************************
		;**** temperature data table ****
		;********************************

    colhead = [['Output','Input','Output','Input'], 			$
	       ['','','','']]

		;********************************************
		;**** convert FORTRAN index to IDL index ****
		;********************************************

    units = ps.ifout - 1 
    unitname = ['Kelvin', 'eV', 'Reduced']

                ;***********************************************************
		;**** Four columns in the table and three sets of units.****
		;**** Column 2 has the same values for all three        ****
		;**** units but column 2 switches between sets 2,3 & 4  ****
		;**** in the input array tabledata as the units change. ****
                ;***********************************************************

    unitind = [[0,0,0],[1,2,3],[4,4,4],[5,5,5]]
    table_title = [ ["Temperature & Density Values"], 			$
		  ["Temperature             Density"]]

		;****************************
		;**** table of data   *******
		;****************************

    tempid = cw_adas_table(base, tabledata, units, unitname, unitind, 	$
			   UNITSTITLE = 'Temperature Units', 		$
			   COLEDIT = [1,0,1,0], COLHEAD = colhead, 	$
			   TITLE = table_title, 			$
			   ORDER = [0,0,0,0], 				$ 
			   UNITS_PLUS = '       Density Units : cm-3', 	$
			   LIMITS = [1,1,1,1], CELLSIZE = 12, 		$
			   /SCROLL, ytexsize=y_size, NUM_FORM=num_form, $
			   FONTS = edit_fonts, FONT_LARGE = large_font, $
			   FONT_SMALL = font_small)

		;*************************
		;**** Default buttons ****
		;*************************

    tbase = widget_base(base, /row)
    deftid = widget_button(tbase, value=' Default Te ',$
		           font=large_font)
    defdid = widget_button(tbase, value=' Default Ne ',$
		           font=large_font)			     
    ipsid = widget_button(tbase, value=' Value Selection by display  ',$
		           font=large_font)

		;**************************************
		;**** Add polynomial fit selection ****
		;**************************************

    polyset = { option:intarr(1), val:strarr(1)}  ; defined thus because 
						  ; cw_opt_value.pro
                                                  ; expects arrays
    polyset.option = [ps.ifsel]
    polyset.val = [ps.tolval]
    options = ['Fit Polynomial']
    optbase = widget_base(topbase,/frame)
    optid   = cw_opt_value(optbase, options, title='Polynomial Fitting',$
			   limits = [0,100], value=polyset, 		$
                           font=large_font)
    if ps.selbase eq 0 then widget_control, optid, sensitive=0
    
		;**** Error message ****

    messid = widget_label(parent, font=large_font, 			$
    value='Edit the processing options data and press Done to proceed')

		;**** add the exit buttons ****

    base = widget_base(parent, /row)
    menufile = bitfile + '/menu.bmp'
    read_X11_bitmap, menufile, bitmap1
    outid = widget_button(base, value=bitmap1)          ;menu button
    cancelid = widget_button(base, value='Cancel', font=large_font)
    doneid = widget_button(base, value='Done', font=large_font)
  
                ;*************************************************
		;**** create a state structure for the pop-up ****
		;**** 		     window.                  ****
                ;*************************************************

    new_state = { runid		:	runid,  			$
			messid	:	messid, 			$
			deftid	:	deftid,			$
			defdid	:	defdid,			$			
			ipsid		:	ipsid,			$
			tempid	:	tempid, 			$
			optid		:	optid,			$
			selbase	:	ps.selbase,			$
			indexid	:	indexid,			$
			cancelid	:	cancelid,			$
			doneid	:	doneid, 			$
                  outid   	:     outid,                  $
			dsfull	:	dsfull,			$
			nbsel 	:	nbsel, 			$
			nstore	:	nstore,			$
			ntdim		:	ntdim,			$
			nddim		:	nddim,			$
			ibsel		:	ps.ibsel,			$
			maxt		:	maxt, 			$
			maxd		:	maxd, 			$
			ita		:	ita,				$
			ida		:	ida,				$
			ldfit		:	ps.ldfit,			$
			tvals		:	tvals,			$
			teda		:	teda,				$
			pec		:	pec,				$
			uwavel	:	cwavel,			$
			ufile		:	cfile,			$
			utype 	:	ctype ,			$
			uindm		:	cindm, 			$
			font		:	large_font,			$
			num_form	:	num_form			}

                 ;**** Save initial state structure ****

    widget_control, first_child, set_uvalue=new_state, /no_copy

    RETURN, parent

END

