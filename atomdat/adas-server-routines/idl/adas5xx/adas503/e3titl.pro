; Copyright (c) 1996, Strathclyde University .
; SCCS info: Module @(#)$Header: /home/adascvs/idl/adas5xx/adas503/e3titl.pro,v 1.3 2004/07/06 13:41:25 whitefor Exp $	Date $Date: 2004/07/06 13:41:25 $
;+
; PROJECT:
;       ADAS IBM MVS to UNIX conversion
;
; NAME:
;	E3TITL
;
; PURPOSE:
;	IDL user interface and communications with ADAS503 FORTRAN
;	process via pipe.
;
; EXPLANATION:
;	This routine reads some information pertaining to the current data 
;	file being analysed and then constructs a descriptive title which is
;	used in the final graphical output.  It replaces the old E3TITL.FOR
;	
;
; USE:
;	The use of this routine is specific to ADAS503, see adas503.pro.
;
; INPUTS:
;	PIPE	- The IDL unit number of the bi-directional pipe to the
;		  ADAS503 FORTRAN process.
;
; OPTIONAL INPUTS:
;	None
;
; OUTPUTS:
;	None ( all outputs directly to FORTRAN E3TITL)
;
; OPTIONAL OUTPUTS:
;	None
;
; KEYWORD PARAMETERS:
;
; CALLS:
;
; SIDE EFFECTS:
;	This routine communicates with the ADAS503 FORTRAN process
;	via a UNIX pipe.
;
; CATEGORY:
;	Adas system.
;
; WRITTEN:
;       Lalit Jalota, Tessella Support Services plc, 24-Feb-1995
;
; MODIFIED:
;	2-Mar-1995	L.Jalota	Changed construction of titlx
;					to match IBM version
;       1.1     Lalit Jalota
;               First Release
;	1.2	Lalit Jalota
;               Changed construction of titlx
;               to match IBM version
;	1.3	Tim Hammond
;		Tidied up comments and code
;
; VERSION:
;	1.1	28-02-95 
;	1.2	03-03-95	
;	1.3	07-02-96
;
;-
;-----------------------------------------------------------------------------

PRO e3titl, pipe

		;**** declare variables for input from pipe ****

    dsfull = ' '
    esym = ' ' 
    cindm = ' '
    cwavel = ' '
    ibsel = 0
    iz = 0
    sdum = ' '
    idum = 0

		;**** read data in from pipe ****

    readf, pipe, sdum, format='(A80)'
    dsfull = sdum
    readf, pipe, sdum, format='(A2)'
    esym = sdum
    readf, pipe, sdum, format='(A2)'
    cindm = sdum
    readf, pipe, sdum, format='(A10)'
    cwavel = sdum
    readf, pipe, idum
    ibsel = idum
    readf, pipe, idum
    iz = idum

		;**** construct titlx ****

    titlx = strcompress(strlowcase(dsfull)) + 				$
            ' BLK =' + strcompress(string(ibsel)) + ' ' + 		$
	    strcompress(strupcase(esym)) + '+' + 			$
            strcompress(string(iz)) + '  WAVELENGTH = ' + 		$
            strcompress(cwavel) + '  MET = ' + strcompress(cindm)

		;**** Now return value of titlx to E3TITL ****

    printf, pipe, titlx

END
