; Copyright (c) 2001, Strathclyde University .
; SCCS info: Module @(#)$Header: /home/adascvs/idl/adas5xx/adas507/adas507_plot.pro,v 1.1 2004/07/06 10:59:44 whitefor Exp $    Date $Date: 2004/07/06 10:59:44 $
;+
; PROJECT:
;       ADAS
;
; NAME:
;	ADAS507_PLOT
;
; PURPOSE:
;	Generates ADAS507 graphical output.
;
; EXPLANATION:
;	This routine creates a window for the display of graphical
;	output a separate routine PLOT507 actually plots a
;	graph.
;
; USE:
;	This routine is specific to ADAS507, see e7outg.pro for
;	example use.
;
; INPUTS:
;	(Most of these inputs map exactly onto variables of the same
;	 name in the FORTRAN code.)
;
;
;	DSFULL  - String; Name of data file 
;
;	TITLE   - String array; titles to be placed above graph
;
;	TITLX   - String; user supplied comment appended to end of title
;
;	TITLM   - String; Information about minimax fitting if selected.
;
;	UTITLE  - String; Optional comment by user
;
;	DATE	- String; Date of graph production
;
;	ESYM 	- String; Symbol of emitting ion
;
;	IZ0	- Integer; Nuclear charge of ionising ion
;
;	IZ1	- Integer; Charge of ionising ion + 1
;
;       BWNO	- String; Ionisation potential (cm-1), number as string.
;
;	CIION 	- String; Initial ion
;
;	CICODE	- String; Initial state metastable index
;
;	CFION  	- String; Final ion
;
;	CINDM	- String; Final state metastable index
;
;	ITVAL	- Integer; number of user entered temperatures
;
;	TEVA	- Double array; User entered electron temperatures, eV
;
;
;	cfgA	- Double array; Spline interpolated or extrapolated 
;				ionization coefficients for user entered 
;				temperatures.
;
;	cfgM	- Double array; Minimax fit values of ionization coeffts.  
;				at 'tfitm()'
;
;	TFITM	- Double array; Selected temperature values for minmiax fit.
;
;	LDEF1	- Integer; 0 - use user entered graph scales
;			   1 - use default axes scaling
;
;	XMIN    - String; Lower limit for x-axis of graph, number as string.
;
;	XMAX    - String; Upper limit for x-axis of graph, number as string.
;
;	YMIN    - String; Lower limit for y-axis of graph, number as string.
;
;	YMAX    - String; Upper limit for y-axis of graph, number as string.
;
;	LFSEL	- Integer; 0 - No minimax fitting was selected 
;			   1 - Mimimax fitting was selected
;
;	NMX	- Integer; Number of temperatures used for minimax fit
;
;	STRG	- String array; Information regarding current data selected
;
;	HEAD1	- String; header information for ionising ion
;
;	HEAD2	- String; Header information for initial state 
;
;	HEAD3   - String; Header information for final state
;
;	HEAD4   - String; Header information for temperature list
;
;	HRDOUT - Integer; 1 if hardcopy output activated, 0 if not.
;
;	HARDNAME- String; Filename for harcopy output.
;
;	DEVICE	- String; IDL name of hardcopy output device.
;
;	HEADER	- String; ADAS version number header to include in graph.
;
; OPTIONAL INPUTS:
;	None.
;
; OUTPUTS:
;	None.
;
; OPTIONAL OUTPUTS:
;	None.
;
; KEYWORD PARAMETERS:
;	FONT	- String; The name of a font to use for text in the
;		  graphical output widget.
;
; CALLS:
;	CW_ADAS_GRAPH	Graphical output widget.
;	PLOT507		Make one plot to an output device for 503.
;	XMANAGER
;
; SIDE EFFECTS:
;	This routine uses a common block to maintain its state PLOT507_BLK.
;
;	One other routine is included in this file;
;	ADAS507_PLOT_EV	Called via XMANAGER during widget management.
;
; CATEGORY:
;	Adas system.
;
; VERSION:
;	1.1	Martin O'Mullane
;		First release			
; MODIFIED:
;	1.1	25-01-2001
;
;-
;----------------------------------------------------------------------------

PRO adas507_plot_ev, event

    COMMON plot507_blk, data, action, win, plotdev, plotfile, fileopen, gomenu

    newplot = 0
    print = 0
    done = 0
		;****************************************
		;**** Set graph and device requested ****
		;****************************************

    CASE event.action OF

	'print'	   : begin
			newplot = 1
			print = 1
		     end

	'done'	   : begin
			if fileopen eq 1 then begin
			  set_plot, plotdev
			  device, /close_file
			endif
			set_plot,'X'
			widget_control, event.top, /destroy
			done = 1
		     end

	'bitbutton' : begin
            if fileopen eq 1 then begin
                set_plot, plotdev
                device, /close_file
            endif
            set_plot,'X'
            widget_control, event.top, /destroy
            done = 1
            gomenu = 1
        end

    END

		;*******************************
		;**** Make requested plot/s ****
		;*******************************

    if done eq 0 then begin
		;**** Set graphics device ****
        if print eq 1 then begin
            set_plot, plotdev
            if fileopen eq 0 then begin
                fileopen = 1
                device, filename = plotfile
	        device, /landscape
            endif
        endif else begin
            set_plot, 'X'
            wset, win
        endelse

		;**** Draw graphics ****
 
        plot507, data.x , data.y, data.itval, data.nplots, data.nmx, 	$
	         data.title, data.xtitle, data.ytitle, 			$
	         data.strg, data.head1, 		$
                 data.teva, data.ldef1, data.xmin, 		$
                 data.xmax, data.ymin, data.ymax 
	if print eq 1 then begin
	    message = 'Plot written to print file.'
	    grval = {WIN:0, MESSAGE:message}
	    widget_control, event.id, set_value=grval
	endif
    endif

END

;----------------------------------------------------------------------------

PRO adas507_plot, dsfull, title, titlx, titlm, utitle, date, esym, 	  $
                  iz0, iz1, itval, cwavel, ciion, cicode, ciscrp, citype, $
                  teva, cfga, cfgm, tfitm, ldef1, xmin, xmax, 		  $
                  ymin, ymax, lfsel, nmx, strg, head1,			  $
                  hrdout, hardname, device, header, 			  $
                  bitfile, gomenu, FONT=font

    COMMON plot507_blk, data, action, win, plotdev, plotfile, 		$
                        fileopen, gomenucom

		;**** Copy input values to common ****

    plotdev = device
    plotfile = hardname
    fileopen = 0
    gomenucom = gomenu
    
		;************************************
		;**** Create general graph title ****
		;************************************

    title = strarr(5)
    type = 'ELECTRON TEMPERATURE '
    title(0) = "CONTRIBUTION FUNCTION VS " + type  
    if ( strtrim(strcompress(utitle),2)  ne '' ) then begin
        title(0) = title(0) + ': ' + strupcase(strtrim(utitle,2))
    endif
    title(1) = 'ADAS    :' + header
    title(2) = 'FILE     :' + titlx
    if (lfsel eq 1) then begin
        title(3)  = 'MINIMAX : ' + strupcase(titlm)
    endif
    title(4) = 'KEY     : (CROSSES - INPUT DATA) (FULL LINE - SPLINE FIT)'
    if (lfsel eq 1) then  title(4) = title(4) + "  (DASH LINE - MINIMAX) "

		;********************************
		;*** Create graph annotation ****
		;********************************        
    strg = strtrim(strg, 2)
    strg(0) = strg(0) +  ' ' + esym
    strg(1) = strg(1) +  strcompress(string(iz0)) 
    strg(2) = strg(2) +  strcompress(string(iz1-1))
    strg(3) = strg(3) +  ' ' + cwavel
    strg(4) = strg(4) +  ' ' + cicode 
    strg(5) = strg(5) +  ' ' + ciscrp
    strg(6) = strg(6) +  ' ' + citype

		;*********************************
		;**** Set up Y data for plots ****
		;*********************************

    if (lfsel eq 1) then ydim = nmx else ydim = itval
    y = make_array(2, ydim, /float)
    valid_data = where((cfga gt 1e-37) and (cfga lt 1e+37))
    if (valid_data(0) ge 0) then begin
        y(0, valid_data) = cfga(valid_data)
        if (lfsel eq 1) then begin
            valid_data = where((cfgm gt 1e-37) and (cfgm lt 1e+37))
	    if (valid_data(0) ge 0) then begin
                y(1, valid_data) = cfgm(valid_data)
            endif else begin
                print, "ADAS507 : unable to plot polynomial fit data"
            endelse
        endif
    endif else begin
        print, "ADAS507 : unable to plot spline fit data"
    endelse
    ytitle = "CONTRIBUTION FUNCTION (unspecified)"

		;**************************************
		;**** Set up x axis and x axis title ***
		;**************************************

    if (lfsel eq 1) then xdim = nmx else xdim = itval
    x = fltarr(2, xdim)
    if (lfsel eq 0) then begin
        x(0,0:itval-1) = teva
        nplots = 1
    endif else begin
        x(0,0:itval-1) = teva
        x(1,*) = tfitm
        nplots = 2
    endelse
    xtitle = type +  " (eV) "

  		;******************************************
		;*** if desired set up user axis scales ***
		;******************************************

    if (ldef1 eq 0) then begin
        xmin = min(x, max=xmax)
        xmin = xmin*0.9
        xmax = xmax*1.1
        ymin = min(y, max=ymax)
        ymin = ymin*0.9
        ymax = ymax*1.1
    endif



		;*************************************
		;**** Create graph display widget ****
		;*************************************

    graphid = widget_base(TITLE='ADAS507 GRAPHICAL OUTPUT', 		$
                          XOFFSET=1,YOFFSET=1)
    device, get_screen_size = scrsz
    xwidth=scrsz(0)*0.75
    yheight=scrsz(1)*0.75
    multiplot=0
    bitval = bitfile + '/menu.bmp'
    cwid = cw_adas_graph(graphid, print=hrdout, FONT=font,		$
                         xsize=xwidth, ysize=yheight, 			$
                         multiplot=multiplot, bitbutton=bitval)

                ;**** Realize the new widget ****

    dynlabel, graphid
    widget_control, graphid, /realize

		;**** Get the id of the graphics area ****

    widget_control, cwid, get_value=grval
    win = grval.win

		;*******************************************
		;**** Put the graphing data into common ****
		;*******************************************

    data = { 	Y		:	y,				$
		X		:	x,   				$
		ITVAL		:	itval,    			$
		NPLOTS		:	nplots,  			$
		NMX		:	nmx, 				$
                TITLE		:	title,   			$
		XTITLE		:	xtitle,  			$
		YTITLE		:	ytitle,           		$
		STRG		:	strg,     			$
		HEAD1		:	head1,    			$
		TEVA		:	teva,     			$
		LDEF1		:	ldef1,                    	$
		XMIN		:	xmin,     			$
		XMAX		:	xmax,      			$
		YMIN		:	ymin,				$
		YMAX		:	ymax 				}  
 
    wset, win
    plot507, x, y, itval, nplots, nmx, title, xtitle, ytitle, 		$
	     strg, head1,  teva,          		$
	     ldef1, xmin, xmax, ymin, ymax 

		;***************************
                ;**** make widget modal ****
		;***************************

    xmanager, 'adas507_plot', graphid, 					$
              event_handler='adas507_plot_ev', /modal, /just_reg
    gomenu = gomenucom

END
