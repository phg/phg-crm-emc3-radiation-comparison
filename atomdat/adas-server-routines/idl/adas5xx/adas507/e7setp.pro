; Copyright (c) 2001, Strathclyde University .
; SCCS info: Module @(#)$Header: /home/adascvs/idl/adas5xx/adas507/e7setp.pro,v 1.1 2004/07/06 13:46:06 whitefor Exp $    Date $Date: 2004/07/06 13:46:06 $
;+
; PROJECT:
;       ADAS
;
; NAME:
;	E7SETP
;
; PURPOSE:
;	IDL communications with ADAS507 FORTRAN process via pipe.
;
; EXPLANATION:
;	This IDL routine reads data from the ADAS507 FORTRAN subroutine
;	E7SETP via a UNIX pipe.  In the original IBM version the FORTRAN
;	subroutine E7SETP put information needed by the ISPF interface
;	into the ISPF pool area for later use.  Now the same information
;	is written to IDL for use in its user unterface.
;
; USE:
;	The use of this routine is specific to ADAS507.  See
;	adas507.pro for an example.
;
; INPUTS:
;	PIPE	- The IDL unit number of the bi-directional pipe to the
;		  ADAS507 FORTRAN process.
;
; OPTIONAL INPUTS:
;	None
;
; OUTPUTS:
;       NBSEL        - Number of ionising ion combinations read in.
;
; OPTIONAL OUTPUTS:
;	None
;
; KEYWORD PARAMETERS:
;	None
;
; CALLS:
;	None
;
; SIDE EFFECTS:
;	Reads data from a pipe.
;
; CATEGORY:
;	Adas system.
;
; WRITTEN:
;       Hugh Summers, University of Strathclyde, 27/11/1995
;
; VERSION:
;	1.1	Martin O'Mullane
;		First release			
; MODIFIED:
;	1.1	25-01-2001
;
;-
;-----------------------------------------------------------------------------

PRO e7setp, pipe, nbsel

		;**********************************
		;**** Initialise new variables ****
		;**********************************

    idum  = 0

		;********************************
		;**** Read data from fortran ****
		;********************************

    readf, pipe, idum
    nbsel = idum

END
