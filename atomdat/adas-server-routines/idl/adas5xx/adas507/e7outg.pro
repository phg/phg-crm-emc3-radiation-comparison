; Copyright (c) 2001, Strathclyde University .
; SCCS info: Module @(#)$Header: /home/adascvs/idl/adas5xx/adas507/e7outg.pro,v 1.1 2004/07/06 13:45:59 whitefor Exp $    Date $Date: 2004/07/06 13:45:59 $
;+
; PROJECT:
;       ADAS
;
; NAME:
;	E7OUTG
;
; PURPOSE:
;	Communication with ADAS507 FORTRAN via UNIX pipe and
;	graphics output.
;
; EXPLANATION:
;	The routine begins by reading information from the ADAS507
;	FORTRAN process via a UNIX pipe.  Then the IDL graphical
;	output routine for ADAS507 is invoked.  Communications are to
;	the FORTRAN subroutine E7OUTG.
;
; USE:
;	The use of this routine is specific to ADAS507 see adas507.pro.
;
; INPUTS:
;	DSFULL   - Data file name
;	PIPE	 - The IDL unit number of the bi-directional pipe to the
;		   ADAS507 FORTRAN process.
;
;	UTITLE   - Optional comment by user
;
;	GRPSCAL	 - Integer; 0 if default scaling required 1 if user
;		   specified scaling to be used.
;
;	XMIN	 - String; User sepcified x-axis minimum, number as string.
;
;	XMAX	 - String; User sepcified x-axis maximum, number as string.
;
;	YMIN	 - String; User specified y-axis minimum, number as string.
;
;	YMAX	 - String; User specified y-axis maximum, number as string.
;
;	HRDOUT	 - Integer; 0 if no hardcopy required, 1 if hardcopy.
;
;	HARDNAME - String; file name for hardcopy output.
;
;	DEVICE	 - String; IDL name for hardcopy output device.
;
;	HEADER	 - ADAS version header information for inclusion in the
;		   graphical output.
;
; OPTIONAL INPUTS:
;	None.
;
; OUTPUTS:
;	None.
;
; OPTIONAL OUTPUTS:
;	None.
;
; KEYWORD PARAMETERS:
;	FONT	- String; Name of the font to be used for the graphical
;		  output widget.
;
; CALLS:
;	ADAS507_PLOT	ADAS507 graphical output.
;
; SIDE EFFECTS:
;	This routine reads information from FORTRAN via a UNIX pipe.
;
; CATEGORY:
;	Adas system.
;
;
; VERSION:
;	1.1	Martin O'Mullane
;		First release			
; MODIFIED:
;	1.1	25-01-2001
;
;-
;-----------------------------------------------------------------------------

PRO e7outg, dsfull, pipe, utitle, grpscal, xmin, xmax, ymin, ymax,     $
	    hrdout, hardname, device, header, bitfile, gomenu, FONT=font


                ;**** Set defaults for keywords ****


    IF NOT (KEYWORD_SET(font)) THEN font = ''


		;**** Declare variables for input ****

    sdum = " "
    idum = 0
    fdum = 0.0d
    strg = make_array(9, /string, value=" ")
    nmx = 0

		;********************************
		;**** Read data from fortran ****
		;********************************

    readf, pipe, format = '(a40)' , sdum
    title = sdum
    readf, pipe, format = '(a120)', sdum
    titlx = sdum
    readf, pipe, format = '(a80)' , sdum
    titlm = sdum
    readf, pipe, format = '(a8)' , sdum
    date = sdum
    readf, pipe, format = '(a2)' , sdum
    esym = sdum
    readf, pipe, idum
    iz0 = idum
    readf, pipe, idum
    iz1 = idum
    readf, pipe, format = '(a10)' , sdum
    cwavel = sdum
    readf, pipe, format = '(a5)' , sdum
    ciion = sdum
    readf, pipe, format = '(a8)' , sdum
    cicode = sdum
    readf, pipe, format = '(a8)' , sdum
    ciscrp = sdum
    readf, pipe, format = '(a5)' , sdum
    citype = sdum
    
    readf, pipe, idum
    itval = idum

		;**** now declare array dimensions ****

    teva = dblarr(itval) 
    cfga = dblarr(itval) 
    for i=0, itval-1 do begin
        readf, pipe, fdum
	if (fdum lt 1d-36) then fdum=0d0
        teva(i) = fdum 
    endfor
    for i=0, itval-1 do begin
        readf, pipe, fdum
	if (fdum lt 1d-36) then fdum=0d0
        cfga(i) = fdum 
    endfor
    readf, pipe, idum
    ldef1 = idum
    if (ldef1 eq 1) then begin
        readf, pipe, fdum
	xmin = fdum
        readf, pipe, fdum
	xmax = fdum
        readf, pipe, fdum
	ymin = fdum
        readf, pipe, fdum
	ymax = fdum
    endif 
    readf, pipe, idum
    lfsel = idum
    
    sdum=''
    if (lfsel eq 1) then begin
        readf, pipe, idum
        nmx = idum
 		;**** declare arrays ****
        cfgm = fltarr(nmx)
        tfitm = fltarr(nmx)
        for i = 0, nmx-1 do begin
	    readf, pipe, sdum
            i_nan = strpos(strupcase(sdum), 'NAN')
            i_inf = strpos(strupcase(sdum), 'INF')
            if (i_nan GT -1 OR i_inf GT -1) then fdum=0.0 else reads, sdum, fdum
	    if (fdum lt 1d-36) then fdum=0d0
            cfgm(i) = fdum 
        endfor
        for i = 0, nmx-1 do begin
	    readf, pipe, fdum 
            tfitm(i) = fdum
        endfor
    endif
    
    
    for i = 0, 6 do begin
        readf, pipe, sdum, format = '(a28)'
        strg(i) = sdum
    endfor
    readf, pipe, sdum, format = '(a32)'
    head1 = sdum

		;***********************
		;**** Plot the data ****
		;***********************

    adas507_plot, dsfull, 						$
  		  title , titlx, titlm , utitle,date, esym, iz0, iz1, 	$
  		  itval , cwavel, ciion, cicode, ciscrp, citype,        $
		  teva  , cfga , cfgm  , tfitm,                       	$
                  ldef1 , xmin , xmax  , ymin , ymax,                 	$
   		  lfsel , nmx  ,                                      	$
   		  strg  , head1, 			            	$
		  hrdout, hardname, device, header, bitfile, gomenu,  	$
                  FONT=font

END
