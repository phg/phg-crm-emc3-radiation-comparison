; Copyright (c) 2001, Strathclyde University .
; SCCS info: Module @(#)$Header: /home/adascvs/idl/adas5xx/adas507/e7spf0.pro,v 1.1 2004/07/06 13:46:13 whitefor Exp $    Date $Date: 2004/07/06 13:46:13 $
;+
; PROJECT:
;       ADAS
;
; NAME:
;	E7SPF0
;
; PURPOSE:
;	IDL user interface and communications with ADAS507 FORTRAN
;	process via pipe.
;
; EXPLANATION:
;	Firstly this routine invokes the part of the user interface
;	used to select the input dataset for ADAS507.  When the user's
;	interactions are complete this routine communicates with the
;	ADAS507 FORTRAN application via a UNIX pipe.  Communications
;	are to the FORTRAN subroutine E7SPF0.
;
; USE:
;	The use of this routine is specific to ADAS507, see adas507.pro.
;
; INPUTS:
;	PIPE	- The IDL unit number of the bi-directional pipe to the
;		  ADAS507 FORTRAN process.
;
;	VALUE	- A structure which determines the initial settings of
;		  the dataset selection widget.  The initial value is
;		  set in adas507.pro.  VALUE is passed un-modified
;		  through to cw_adas_in.pro, see that routine for a full
;		  description.
;
; OPTIONAL INPUTS:
;	None
;
; OUTPUTS:
;	VALUE	- On output the structure records the final settings of
;		  the dataset selection widget if the user pressed the
;		  'Done' button otherwise it is not changed from input.
;
;	DSFULL	- String; The full system file name of the input 
;		  dataset selected by the user for processing.
;
;	REP	- String; Indicates whether the user pressed the 'Done'
;		  or 'Cancel' button on the interface.  The action is
;		  converted to the strings 'NO' and 'YES' respectively
;		  to match up with the existing FORTRAN code.  In the
;		  original IBM ISPF interface REP was the reply to the
;		  question 'DO YOU WANT TO END PROGRAM EXECUTION'.
;
; OPTIONAL OUTPUTS:
;	None
;
; KEYWORD PARAMETERS:
;	FONT	- Supplies the font to be used for the interface widgets.
;
; CALLS:
;	ADAS_IN		Pops-up the dataset selection widget.
;
; SIDE EFFECTS:
;	This routine communicates with the ADAS507 FORTRAN process
;	via a UNIX pipe.
;
; CATEGORY:
;	Adas system.
;
; WRITTEN:
;       Hugh Summers, University of Strathclyde, 27/11/95
;
; VERSION:
;	1.1	Martin O'Mullane
;		First release			
;
; MODIFIED:
;	1.1	25-01-2001
;
;-
;-----------------------------------------------------------------------------


PRO e7spf0, pipe, value, dsfull, rep, FONT=font


                ;**** Set defaults for keywords ****

    IF NOT (KEYWORD_SET(font)) THEN font = ''

		;**********************************
		;**** Pop-up input file widget ****
		;**********************************

    adas_in, value, action, WINTITLE='ADAS 507 INPUT', FONT=font,	$
	     TITLE='Input Dataset'

		;********************************************
		;**** Act on the event from the widget   ****
		;********************************************
		;**** There are only two possible events ****
		;**** 'Done' and 'Cancel'.               ****
		;********************************************

    if action eq 'Done' then begin
        rep = 'NO'
    endif else begin
        rep = 'YES'
    endelse
  
		;**** Construct dataset name ****

    dsfull = value.rootpath + value.file

		;*******************************
		;**** Write data to fortran ****
		;*******************************

    printf, pipe, rep
    printf, pipe, dsfull

END
