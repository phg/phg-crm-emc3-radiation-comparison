; Copyright (c) 1995, Strathclyde University.
; SCCS info: Module @(#)$Header: /home/adascvs/idl/adas5xx/adas506/cw_adas506_out.pro,v 1.4 2004/07/06 12:52:49 whitefor Exp $ Date $Date: 2004/07/06 12:52:49 $
;+
; PROJECT:
;       ADAS IBM MVS to UNIX conversion
;
; NAME:
;	CW_ADAS506_OUT()
;
; PURPOSE:
;	Produces a widget for ADAS506 output options.
;
; EXPLANATION:
;	This function declares a compound widget consisting of the
;	graphical output selection widget cw_adas_gr_sel.pro, and an 
;	output file widget cw_adas_outfile.pro. The two text output
;	files specified in this widget are for tabular (paper.txt)
;	output and G(TE) collection file. This widget also includes 
;	a button for browsing the comments
;       from the input dataset, a 'Cancel' button and a 'Done' button.
;	The compound widgets cw_adas_gr_sel.pro and cw_adas_outfile.pro
;	included in this file are self managing.  This widget only
;	handles events from the 'Done' and 'Cancel' buttons.
;
;	This widget only generates events for the 'Done' and 'Cancel'
;	buttons.
;
; USE:
;	This widget is specific to ADAS506, see adas506_out.pro	for use.
;
; INPUTS:
;	PARENT	- Long integer; ID of parent widget.
;
;	DSFULL	- Name of input dataset for this application.
;
;       BITFILE - String; the path to the dirctory containing bitmaps
;                 for the 'escape to series menu' button.
;
; OPTIONAL INPUTS:
;	None.
;
; OUTPUTS:
;	The return value of this function is the ID of the declared widget.
;
; OPTIONAL OUTPUTS:
;	None.
;
; KEYWORD PARAMETERS:
;	DEVLIST	- A string array;  Each element of the array is the
;		  name of a hardcopy graphics output device.  This
;		  list must mirror the DEVCODE list.  DEVLIST is the
;		  list which the user will see.
;		  e.g ['Post-Script','HP-PCL','HP-GL']
;
;	VALUE	- A structure which determines the initial settings of
;		  the output options widget.  The structure is made
;		  up of two parts.  Each part is the same as the value
;		  structure of one of the two main compound widgets
;		  included in this widget.  See cw_adas_gr_sel and
;		  cw_adas_outfile for more details.  The default value is;
;
;		      { GRPOUT:0, GTIT1:'', $
;			GRPSCAL:0, $
;			XMIN:'',  XMAX:'',   $
;			YMIN:'',  YMAX:'',   $
;			HRDOUT:0, HARDNAME:'', $
;			GRPDEF:'', GRPFMESS:'', $
;			GRPSEL:-1, GRPRMESS:'', $
;			DEVSEL:-1, GRSELMESS:'', $
;			TEXOUT:0, TEXAPP:-1, $
;			TEXREP:0, TEXDSN:'', $
;			TEXDEF:'',TEXMES:'', $
;			COLOUT:0, COLAPP:-1, $
;			COLREP:0, COLDSN:'', $
;			COLDEF:'',COLMES:'', $
;		      }
;
;		  For CW_ADAS_GR_SEL;
;			GRPOUT	   Integer; Activation button 1 on, 0 off
;			GTIT1	   String; Graph title
;			GRPSCAL	   Integer; Scaling activation 1 on, 0 off
;			XMIN	   String; x-axis minimum, string of number
;			XMAX	   String; x-axis maximum, string of number
;			YMIN	   String; y-axis minimum, string of number
;			YMAX	   String; y-axis maximum, string of number
;			HRDOUT	   Integer; Hard copy activ' 1 on, 0 off
;			HARDNAME   String; Hard copy output file name
;			GRPDEF	   String; Default output file name 
;			GRPFMESS   String; File name error message
;			GRPSEL	   Integer; index of selected graph in GRPLIST
;			GRPRMESS   String; Scaling ranges error message
;			DEVSEL	   Integer; index of selected device in DEVLIST
;			GRSELMESS  String; General error message
;
;		  For CW_ADAS_OUTFILE tabular output (paper.txt);
;			TEXOUT	Integer; Activation button 1 on, 0 off
;			TEXAPP	Integer; Append button 1 on, 0 off, -1 no button
;			TEXREP	Integer; Replace button 1 on, 0 off, -1 no button
;			TEXDSN	String; Output file name
;			TEXDEF	String; Default file name
;			TEXMES	String; file name error message
;
;		  For CW_ADAS_OUTFILE collection file
;			COLOUT  Integer; Activation button 1 on, 0 off
;			COLAPP	Integer; Append button 1 on, 0 off, -1 no button
;			COLREP	Integer; Replace but' 1 on, 0 off, -1 no button
;			COLDSN	String; Output file name
;			COLDEF	String; Default file name
;			COLMES	String; file name error message
;
;	UVALUE	- A user value for this widget.
;
;	FONT	- String; the name of a font to be used for all text in
;		  this widget.  Default to system default font.
;
; CALLS:
;	CW_ADAS_DSBR	Input dataset name and comments browse button.
;	CW_ADAS_GR_SEL	Graphical output selection widget.
;	CW_ADAS_OUTFILE	Output file name entry widget.
;	See side effects for other related widget management routines.
;
; SIDE EFFECTS:
;
; 	Two other routines are included in this file
;	OUT506_GET_VAL()
;	OUT506_EVENT()
;
; CATEGORY:
;	Compound Widget
;
; WRITTEN:
;       Alessandro Lanzafame, University of Strathclyde         
;
; MODIFIED:
;	1.1	Alessandro Lanzafame				26-06-95
;	1.2	William Osborn
;		Added menu button code
;	1.3	William Osborn
;		Removed xsize=n keyword from widget_label command.
;		The dynlabel procedure does not override these
;		assignments and they were taken as being values in pixels
;		on some machines.
;       1.4     Tim Hammond
;		Added /row keyword to cw_adas_dsbr to shorten screen
;
; VERSION:
;	1.1	First Release
;	1.2	10-07-96
;	1.3	01-08-96
;	1.4	06-08-96
;-
;-----------------------------------------------------------------------------

FUNCTION out506_get_val, id

  ;---------------------------------------------------------------------------
  ;	Return to caller on error
  ;---------------------------------------------------------------------------

  ON_ERROR, 2

  ;---------------------------------------------------------------------------
  ;	Retrieve the state
  ;---------------------------------------------------------------------------

  parent=widget_info(id,/parent)
  widget_control, parent, get_uvalue=state, /no_copy

  ;---------------------------------------------------------------------------
  ;	Get graphical output settings
  ;---------------------------------------------------------------------------

  widget_control,state.grpid,get_value=grselval

  ;---------------------------------------------------------------------------
  ;	Get text output settings
  ;---------------------------------------------------------------------------

  widget_control,state.paperid,get_value=papos

  ;---------------------------------------------------------------------------
  ;	Get collection file settings
  ;---------------------------------------------------------------------------

  widget_control,state.contid,get_value=colos

  os = { out506_set, 							$
	   GRPOUT:grselval.outbut, GTIT1:grselval.gtit1, 	 	$
	   GRPSCAL:grselval.scalbut, 			 	 	$
	   XMIN:grselval.xmin,     XMAX:grselval.xmax,    	 	$
	   YMIN:grselval.ymin,     YMAX:grselval.ymax,    	 	$
	   HRDOUT:grselval.hrdout, HARDNAME:grselval.hardname,   	$
	   GRPDEF:grselval.grpdef, GRPFMESS:grselval.grpfmess,   	$
	   GRPSEL:grselval.grpsel, GRPRMESS:grselval.grprmess,   	$
	   DEVSEL:grselval.devsel, GRSELMESS:grselval.grselmess, 	$
	   TEXOUT:papos.outbut,  TEXAPP:papos.appbut, 		 	$
	   TEXREP:papos.repbut,  TEXDSN:papos.filename, 	 	$
	   TEXDEF:papos.defname, TEXMES:papos.message,		 	$
	   COLOUT:colos.outbut,  COLAPP:colos.appbut,		 	$
	   COLREP:colos.repbut,  COLDSN:colos.filename,		 	$
	   COLDEF:colos.defname, COLMES:colos.message 		 	$
       }

  ;---------------------------------------------------------------------------
  ;	Return the state
  ;---------------------------------------------------------------------------

  widget_control, parent, set_uvalue=state, /no_copy

  RETURN, os 

END

;-----------------------------------------------------------------------------

FUNCTION out506_event, event

  COMMON cw506out_blk, cwoval

  ;---------------------------------------------------------------------------
  ;	Base ID of compound widget
  ;---------------------------------------------------------------------------

  parent=event.top

  ;---------------------------------------------------------------------------
  ;	Retrieve the state
  ;---------------------------------------------------------------------------

  widget_control, parent, get_uvalue=state, /no_copy

  ;---------------------------------------------------------------------------
  ;	Clear previous messages
  ;---------------------------------------------------------------------------

  widget_control,state.messid,set_value=' '

  ;---------------------------------------------------------------------------
  ;	Process events
  ;---------------------------------------------------------------------------

  CASE event.id OF

  ;---------------------------------------------------------------------------
  ;	Cancel button
  ;---------------------------------------------------------------------------

    state.cancelid: new_event = {ID:parent, TOP:event.top, 		$
				HANDLER:0L, ACTION:'Cancel'}

		;*********************
                ;**** Menu button ****
		;*********************

    state.outid: begin
          new_event = {ID:parent, TOP:event.top, HANDLER:0L,          $
                       ACTION:'Menu'}
      end
  ;---------------------------------------------------------------------------
  ;	Done button
  ;---------------------------------------------------------------------------

    state.doneid: begin

  ;---------------------------------------------------------------------------
  ;	Return the state before checking can start 
  ;	with the get_value keyword.                
  ;---------------------------------------------------------------------------

          widget_control, parent, set_uvalue=state, /no_copy

  ;---------------------------------------------------------------------------
  ;	Check for errors in the input
  ;---------------------------------------------------------------------------

	  error = 0

  ;---------------------------------------------------------------------------
  ;	Get a copy of the widget value
  ;---------------------------------------------------------------------------

	  widget_control,event.handler,get_value=os
	
  ;---------------------------------------------------------------------------
  ;	Check for widget error messages
  ;---------------------------------------------------------------------------

	  if os.grpout eq 1 and strtrim(os.grpfmess) ne '' then error=1
	  if os.grpout eq 1 and os.grpscal eq 1 and $
				strtrim(os.grprmess) ne '' then error=1
	  if os.grpout eq 1 and strtrim(os.grselmess) ne '' then error=1
	  if os.texout eq 1 and strtrim(os.texmes) ne '' then error=1
 	  if os.colout eq 1 and strtrim(os.colmes) ne '' then error=1

  ;---------------------------------------------------------------------------
  ;	Retrieve the state 
  ;---------------------------------------------------------------------------

          widget_control, parent, get_uvalue=state, /no_copy

	  if error eq 1 then begin
	    widget_control,state.messid,set_value= 			$

		'**** Error in output settings ****'
	    new_event = 0L
	  end else begin
	    new_event = {ID:parent, TOP:event.top, HANDLER:0L, ACTION:'Done'}
	  end

        end

    ELSE: new_event = 0L

  ENDCASE

  ;---------------------------------------------------------------------------
  ;	Return the state
  ;---------------------------------------------------------------------------

          widget_control, parent, set_uvalue=state, /no_copy

  RETURN, new_event
END

;-----------------------------------------------------------------------------

FUNCTION cw_adas506_out, parent, dsfull, bitfile, DEVLIST=devlist, 	$
		VALUE=value, UVALUE=uvalue, FONT=font

  COMMON cw506out_blk, cwoval

  IF (N_PARAMS() EQ 0) THEN MESSAGE, 'Must specify parent for cw_adas506_out'
  ON_ERROR, 2					;return to caller

  cwoval=value

  ;---------------------------------------------------------------------------
  ;	Set defaults for keywords
  ;---------------------------------------------------------------------------

  IF NOT (KEYWORD_SET(devlist)) THEN devlist = ''
  IF NOT (KEYWORD_SET(uvalue)) THEN uvalue = 0
  IF NOT (KEYWORD_SET(font)) THEN font = ''
  IF NOT (KEYWORD_SET(value)) THEN begin
	os = {out506_set, 						$
			GRPOUT:0, GTIT1:'', 				$
			GRPSCAL:0, 					$
			XMIN:'',  XMAX:'',   				$
			YMIN:'',  YMAX:'',   				$
			HRDOUT:0, HARDNAME:'', 				$
			GRPDEF:'', GRPFMESS:'',	 			$
			GRPSEL:-1, GRPRMESS:'', 			$
			DEVSEL:-1, GRSELMESS:'', 			$
			TEXOUT:0, TEXAPP:-1, 				$
			TEXREP:0, TEXDSN:'', 				$
                        TEXDEF:'paper.txt',TEXMES:'',			$
			COLOUT:0, COLAPP:-1, 				$
			COLREP:0, COLDSN:'', 				$
                        COLDEF:'',COLMES:''  				$
                  }
  END ELSE BEGIN
	os = {out506_set, 						$
			GRPOUT:value.grpout, GTIT1:value.gtit1, 	$
			GRPSCAL:value.grpscal, 				$
			XMIN:value.xmin,     XMAX:value.xmax,   	$
			YMIN:value.ymin,     YMAX:value.ymax,   	$
			HRDOUT:value.hrdout, HARDNAME:value.hardname, 	$
			GRPDEF:value.grpdef, GRPFMESS:value.grpfmess, 	$
			GRPSEL:value.grpsel, GRPRMESS:value.grprmess, 	$
			DEVSEL:value.devsel, GRSELMESS:value.grselmess, $
			TEXOUT:value.texout, TEXAPP:value.texapp, 	$
			TEXREP:value.texrep, TEXDSN:value.texdsn, 	$
			TEXDEF:value.texdef, TEXMES:value.texmes, 	$
			COLOUT:value.colout, COLAPP:value.colapp, 	$
			COLREP:value.colrep, COLDSN:value.coldsn, 	$
			COLDEF:value.coldef, COLMES:value.colmes  	$
		}
  END

  ;---------------------------------------------------------------------------
  ;	Create the 506 Output options widget
  ;---------------------------------------------------------------------------
  ;---------------------------------------------------------------------------
  ;	create base widget
  ;---------------------------------------------------------------------------

  cwid = widget_base( parent, UVALUE = uvalue, 				$
			EVENT_FUNC = "out506_event", 			$
			FUNC_GET_VALUE = "out506_get_val", 		$
			/COLUMN)

  ;---------------------------------------------------------------------------
  ;	Add dataset name and browse button
  ;---------------------------------------------------------------------------

  rc = cw_adas_dsbr(cwid,dsfull,font=font,/row)

  ;---------------------------------------------------------------------------
  ;	Widget for graphics selection
  ;	Note change in names for GRPOUT and GRPSCAL
  ;---------------------------------------------------------------------------

  grselval = {  OUTBUT:os.grpout, GTIT1:os.gtit1, SCALBUT:os.grpscal, 	$
		XMIN:os.xmin, XMAX:os.xmax, 				$
		YMIN:os.ymin, YMAX:os.ymax, 				$
		HRDOUT:os.hrdout, HARDNAME:os.hardname, 		$
		GRPDEF:os.grpdef, GRPFMESS:os.grpfmess, 		$
		GRPSEL:os.grpsel, GRPRMESS:os.grprmess, 		$
		DEVSEL:os.devsel, GRSELMESS:os.grselmess }
  base = widget_base(cwid,/row,/frame)
  grpid = cw_adas_gr_sel(base, /SIGN, OUTPUT='Graphical Output', 	$
			 DEVLIST=devlist, VALUE=grselval, FONT=font 	$
			)

  ;---------------------------------------------------------------------------
  ;	Widget for text output
  ;---------------------------------------------------------------------------

  outfval = { OUTBUT:os.texout, APPBUT:-1, REPBUT:os.texrep, 		$
  	      FILENAME:os.texdsn, DEFNAME:os.texdef, MESSAGE:os.texmes }
  base = widget_base(cwid,/row,/frame)
  paperid = cw_adas_outfile(base, OUTPUT='Text Output', 		$
                        VALUE=outfval, FONT=font)

  ;---------------------------------------------------------------------------
  ;	Widget for collection file
  ;---------------------------------------------------------------------------

  outfval = { OUTBUT:os.colout, APPBUT:os.colapp, REPBUT:os.colrep, 	$
	      FILENAME:os.coldsn, DEFNAME:os.coldef, MESSAGE:os.colmes }
  base = widget_base(cwid,/row,/frame)
  contid = cw_adas_outfile(base, OUTPUT='Collection File', 		$
                        VALUE=outfval, FONT=font)

  ;---------------------------------------------------------------------------
  ;	Error message
  ;---------------------------------------------------------------------------

  messid = widget_label(cwid,value=' ',font=font)

  ;---------------------------------------------------------------------------
  ;	add the exit buttons
  ;---------------------------------------------------------------------------

  base = widget_base(cwid,/row)
  menufile = bitfile + '/menu.bmp'
  read_X11_bitmap, menufile, bitmap1
  outid = widget_button(base, value=bitmap1)          ;menu button
  cancelid = widget_button(base,value='Cancel',font=font)
  doneid = widget_button(base,value='Done',font=font)
  
  ;---------------------------------------------------------------------------
  ;	create a state structure for the pop-up window.
  ;---------------------------------------------------------------------------

  new_state = { GRPID:grpid, PAPERID:paperid, CONTID:contid,  		$
		CANCELID:cancelid, DONEID:doneid, MESSID:messid, OUTID:outid }

  ;---------------------------------------------------------------------------
  ;	Save initial state structure
  ;---------------------------------------------------------------------------

  widget_control, parent, set_uvalue=new_state, /no_copy
  RETURN, cwid

END

