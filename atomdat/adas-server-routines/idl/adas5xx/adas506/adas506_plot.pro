; Copyright (c) 1995, Strathclyde University.
; SCCS info: Module @(#)$Header: /home/adascvs/idl/adas5xx/adas506/adas506_plot.pro,v 1.4 2004/07/06 10:59:24 whitefor Exp $ Date $Date: 2004/07/06 10:59:24 $
;+
; PROJECT:
;       ADAS IBM MVS to UNIX conversion
;
; NAME:
;	ADAS506_PLOT
;
; PURPOSE:
;	Generates ADAS506 graphical output.
;
; EXPLANATION:
;	This routine creates a window for the display of graphical
;	output. A separate routine PLOT506 actually plots the
;	graph.
;
; USE:
;	This routine is specific to ADAS506, see e6outg.pro for
;	example use.
;
; INPUTS:
;	(Most of these inputs map exactly onto variables of the same
;	 name in the FORTRAN code.)
;
;
;	DSFULL  - String; Name of data file 
;
;	TITLX   - String; user supplied comment appended to end of title
;
;	TITLM   - String; Information about minimax fitting if selected.
;
;	UTITLE  - String; Optional comment by user added to graph title.
;
;	TEMP	- Double array; temperatures from input data file.
;	
;	RATE    - Double array; Excitation rate coefficient of selected 
;				transition from input data file
;
;	TOSA	- Double array; Selected temperatures for spline fit
;
;	ROSA	- Double array; Excitation rate coefficients for spline fit.
;
;	TOMA	- Double array; Selected temperatures for minimax fit
;
;	DATE	- String; Date of graph production
;
;	LDEF1	- Integer; 1 - use user entered graph scales
;			   0 - use default axes scaling
;
;	XMIN    - String; Lower limit for x-axis of graph, number as string.
;
;	XMAX    - String; Upper limit for x-axis of graph, number as string.
;
;	YMIN    - String; Lower limit for y-axis of graph, number as string.
;
;	YMAX    - String; Upper limit for y-axis of graph, number as string.
;
;	LFSEL	- Integer; 0 - No minimax fitting was selected 
;			   1 - Mimimax fitting was selected
;
;	LOSEL   - Integer; 0 - No interpolated values for spline fit. 
;			   1 - Intepolated values for spline fit.
;
;	NMX	- Integer; Number of temperatures used for minimax fit
;
;	NENER   - Integer; Number of user entered temperature values
;
;	NPSPL	- Integer; Number of interpolated values for spline.
;
;	HRDOUT - Integer; 1 if hardcopy output activated, 0 if not.
;
;	HARDNAME- String; Filename for harcopy output.
;
;	DEVICE	- String; IDL name of hardcopy output device.
;
;	HEADER	- String; ADAS version number header to include in graph.
;
;       BITFILE - String; the path to the dirctory containing bitmaps
;                 for the 'escape to series menu' button.
;
; OPTIONAL INPUTS:
;	None.
;
; OUTPUTS:
;       GOMENU  - Int; flag - set to 1 if user has selected 'escape direct
;                 to series menu , otherwise 0
;
; OPTIONAL OUTPUTS:
;	None.
;
; KEYWORD PARAMETERS:
;	FONT	- String; The name of a font to use for text in the
;		  graphical output widget.
;
; CALLS:
;	CW_ADAS_GRAPH	Graphical output widget.
;	PLOT506		Make one plot to an output device for 506.
;	XMANAGER
;
; SIDE EFFECTS:
;	This routine uses a common block to maintain its state PLOT506_BLK.
;
;	One other routine is included in this file;
;	ADAS506_PLOT_EV	Called via XMANAGER during widget management.
;
; CATEGORY:
;	Adas system.
;
; WRITTEN:
;       Alessandro Lanzafame, University of Strathclyde          
;
; MODIFIED:
;	1.1	Alessandro Lanzafame				
;		First Release
;	1.2	Tim Hammond					
;		Altered graph axis labels
;	1.3	William Osborn
;		Added menu button code
;       1.4     William Osborn
;               Added 'done but keep' button, dynlabel call and set
;               size in relation to the screen size
; VERSION:
;	1.1	26-06-95
;	1.2	27-06-95
;	1.3	10-07-96
;	1.4	04-10-96
;-
;----------------------------------------------------------------------------

PRO adas506_plot_ev, event

  COMMON plot506_blk, data, action, win, plotdev, 			$
		      plotfile, fileopen, gomenu


  newplot = 0
  print = 0
  done = 0
  gomenu = 0

  ;--------------------------------------------------------------------------
  ;	Set graph and device requested
  ;--------------------------------------------------------------------------

  CASE event.action OF

	'print'	   : begin
			newplot = 1
			print = 1
		     end

	'done'	   : begin
			if fileopen eq 1 then begin
			  set_plot,plotdev
			  device,/close_file
			end
			set_plot,'X'
			widget_control,event.top,/destroy
			done = 1
		     end
	'done_but_keep'	   : begin
			if fileopen eq 1 then begin
			  set_plot,plotdev
			  device,/close_file
			end
			set_plot,'X'
			widget_control,event.top,/destroy
			done = 1
			gomenu = 2
		     end
	'bitbutton'   : begin
			if fileopen eq 1 then begin
			  set_plot,plotdev
			  device,/close_file
			end
			set_plot,'X'
			widget_control,event.top,/destroy
			gomenu = 1
			done = 1
		     end
  END

  ;--------------------------------------------------------------------------
  ;	Make requested plot/s
  ;--------------------------------------------------------------------------

  if done eq 0 then begin

  ;--------------------------------------------------------------------------
  ;	Set graphics device
  ;--------------------------------------------------------------------------

    if print eq 1 then begin

      set_plot,plotdev
      if fileopen eq 0 then begin
        fileopen = 1
        device,filename=plotfile
	device,/landscape
      end

    end else begin

      set_plot,'X'
      wset,win
  
    end

  ;--------------------------------------------------------------------------
  ;	 Draw graphics
  ;--------------------------------------------------------------------------

    plot506, data.x , data.y, data.nener, data.nmx, data.npspl, 	$
	   data.nplots, data.title, data.xtitle, data.ytitle, 		$
	   data.ldef1, data.xmin, data.xmax, data.ymin, data.ymax 


	if print eq 1 then begin
	  message = 'Plot  written to print file.'
	  grval = {WIN:0, MESSAGE:message}
	  widget_control,event.id,set_value=grval
	end


  end

END

;----------------------------------------------------------------------------

PRO adas506_plot,  dsfull, 						$
		    titlx, titlm, utitle, date, 			$
		    temp, gofta, tosa, gftosa, toma, gftoma, 		$
                    ldef1, xmin, xmax, ymin, ymax, 			$
                    lfsel, losel, nmx, nener, npspl, 			$
                    hrdout, hardname, device, header, bitfile, gomenu, 	$
		    FONT=font

  COMMON plot506_blk, data, action, win, plotdev, 			$
		      plotfile, fileopen, gomenucom

  modal = 1

  ;--------------------------------------------------------------------------
  ;	Copy input values to common
  ;--------------------------------------------------------------------------

  gomenucom = gomenu
  plotdev = device
  plotfile = hardname
  fileopen = 0

  ;--------------------------------------------------------------------------
  ;	Create general graph title
  ;--------------------------------------------------------------------------

  title = strarr(5)

  title(0) = "G(Te) FUNCTION VERSUS TEMPERATURE "
  if ( strtrim(strcompress(utitle),2)  ne '' ) then begin
     title(0) = title(0) + ': ' + strupcase(strtrim(utitle,2))
  endif
  title(1) = 'ADAS    :' + strupcase(header)
  title(2) = 'FILE     :' + 				$
		strcompress((titlx))
  if (lfsel eq 1) then begin
     title(3)  =  "MINIMAX : " + strupcase(titlm)
     title(4)  = 'KEY     : (CROSSES - INPUT DATA) ' + 			$ 
		'(FULL LINE - SPLINE FIT)' + '   (DASH LINE - MINIMAX) '
  endif else begin
     title(3) = 'KEY     : (CROSSES - INPUT DATA) (FULL LINE - SPLINE FIT)'
     title(4) = ' ' 
  endelse
  
  ;--------------------------------------------------------------------------
  ;	Set up Y data for plots     
  ;	dimension of y axis depends 
  ;	upon selected fitting options
  ;--------------------------------------------------------------------------

  if ((lfsel eq 1) and (losel eq 1)) then begin
     ydim = nmx > npspl
  endif else begin
      if (lfsel eq 1) then ydim= nmx  else 				$
          if (losel eq 1) then ydim= npspl else 			$ 
             ydim = nener
  endelse

  y = make_array(3, ydim, /float)
  valid_data = where((gofta gt 1e-37) and (gofta lt 1e+37))
  if (valid_data(0) ge 0) then begin
     y(0, valid_data) = gofta(valid_data)
     if (losel eq 1) then begin
        valid_data = where((gftosa gt 1e-37) and (gftosa lt 1e+37))
	if (valid_data(0) ge 0) then begin
           y(1, valid_data) = gftosa(valid_data)
        endif else begin
           print, "ADAS506 : unable to plot spline fit data"
        endelse
     endif
     if (lfsel eq 1) then begin
        valid_data = where((gftoma gt 1e-37) and (gftoma lt 1e+37))
	if (valid_data(0) ge 0) then begin
           y(2, valid_data) = gftoma(valid_data)
        endif else begin
           print, "ADAS506 : unable to plot polynomial fit data"
        endelse
     endif
  endif else begin
           print, "ADAS506 : unable to plot data"
  endelse

  ytitle = "G(Te) (cm!e3!n s!e-1!n)"

  ;--------------------------------------------------------------------------
  ;	Set up x axis and xaxis title 
  ;--------------------------------------------------------------------------

  if ((lfsel eq 1) and (losel eq 1)) then begin
     xdim = nmx > npspl
  endif else begin
      if (lfsel eq 1) then xdim= nmx  else 				$
          if (losel eq 1) then xdim= npspl else 			$ 
             xdim = nener
  endelse
  x = fltarr(3, xdim)

  ;--------------------------------------------------------------------------
  ;	write x axis array 
  ;--------------------------------------------------------------------------

  x(0,0:nener-1) = temp
  nplots = 1

  if (losel eq 1) then begin
     x(1,0:npspl-1) = tosa
     nplots = nplots +2
  endif 
  if (lfsel eq 1) then begin
     x(2,0:nmx-1)  = toma
     nplots = nplots + 4
  endif
  xtitle = " TEMPERATURE (K)"

  ;--------------------------------------------------------------------------
  ;	if desired set up axis scales 
  ;--------------------------------------------------------------------------

  if (ldef1 eq 0) then begin
      xmin = min(x(*,1)) 	; data has to be monotonically increasing.
      xmin = xmin * 0.9
      xmax = max(x) * 1.1

      ymin = min(y(*,1))	; data has to be monotonically increasing.
      ymin = ymin * 0.9
      ymax = max(y) * 1.1
  endif

LAB_AGAIN:

  ;--------------------------------------------------------------------------
  ;	Create graph display widget
  ;--------------------------------------------------------------------------

  graphid = widget_base(TITLE='ADAS506 GRAPHICAL OUTPUT', 		$
					XOFFSET=1,YOFFSET=1)
  bitval = bitfile + '/menu.bmp'
  device, get_screen_size = scrsz
  xwidth=scrsz(0)*0.75
  yheight=scrsz(1)*0.75
  cwid = cw_adas_graph(graphid,print=hrdout,FONT=font, bitbutton = bitval,$
			keep = 1, xsize=xwidth, ysize=yheight)

  ;--------------------------------------------------------------------------
  ;	Realize the new widget
  ;--------------------------------------------------------------------------
  dynlabel, graphid
  widget_control,graphid,/realize
  ;--------------------------------------------------------------------------

  ;--------------------------------------------------------------------------
  ;	Get the id of the graphics area 
  ;--------------------------------------------------------------------------

  widget_control,cwid,get_value=grval
  win = grval.win

  ;--------------------------------------------------------------------------
  ;	Put the graphing data into common 
  ;--------------------------------------------------------------------------

  data = {    	y:y,	x:x,   nener:nener,    nmx:nmx,  npspl:npspl, 	$
	        nplots:nplots, title:title,  xtitle:xtitle, ytitle:ytitle, $
                ldef1:ldef1, 						$
		xmin:xmin,	xmax:xmax,	ymin:ymin,	ymax:ymax $
          }
 
  wset,win

  plot506, x , y, nener, nmx, npspl, nplots, title, xtitle, ytitle, 	$
	      ldef1, xmin, xmax, ymin, ymax 


  ;--------------------------------------------------------------------------
  ;	make widget modal
  ;--------------------------------------------------------------------------

  if modal eq 0 then begin
    xmanager,'adas506_plot',graphid,event_handler='adas506_plot_ev', 	$
                                        /just_reg
  endif else begin
    xmanager,'adas506_plot',graphid,event_handler='adas506_plot_ev', 	$
                                        /modal, /just_reg
  endelse
  gomenu = gomenucom
  if gomenu eq 2 then begin
	modal=0
	gomenucom = 0
	goto, LAB_AGAIN
  endif

END



