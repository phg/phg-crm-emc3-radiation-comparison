; Copyright (c) 1995, Strathclyde University.
; SCCS info: Module @(#)$Header: /home/adascvs/idl/adas5xx/adas506/e6outg.pro,v 1.2 2004/07/06 13:45:06 whitefor Exp $ Date $Date: 2004/07/06 13:45:06 $
;+
; PROJECT:
;       ADAS IBM MVS to UNIX conversion
;
; NAME:
;	E6OUTG
;
; PURPOSE:
;	Communication with ADAS506 FORTRAN via UNIX pipe and
;	graphics output.
;
; EXPLANATION:
;	The routine begins by reading information from the ADAS506
;	FORTRAN process via a UNIX pipe.  Then the IDL graphical
;	output routine for ADAS506 is invoked.  Communications are to
;	the FORTRAN subroutine E6OUTG.
;
; USE:
;	The use of this routine is specific to ADAS506 see adas506.pro.
;
; INPUTS:
;	DSFULL   - Data file name
;
;	PIPE	 - The IDL unit number of the bi-directional pipe to the
;		   ADAS506 FORTRAN process.
;
;       UTITLE   - Optional comment by user
;
;	GRPSCAL	 - Integer; 0 if default scaling required 1 if user
;		   specified scaling to be used.
;
;	XMIN	 - String; User sepcified x-axis minimum, number as string.
;
;	XMAX	 - String; User sepcified x-axis maximum, number as string.
;
;	YMIN	 - String; User specified y-axis minimum, number as string.
;
;	YMAX	 - String; User specified y-axis maximum, number as string.
;
;	HRDOUT	 - Integer; 0 if no hardcopy required, 1 if hardcopy.
;
;	HARDNAME - String; file name for hardcopy output.
;
;	DEVICE	 - String; IDL name for hardcopy output device.
;
;	HEADER	 - ADAS version header information for inclusion in the
;		   graphical output.
;
;       BITFILE - String; the path to the dirctory containing bitmaps
;                 for the 'escape to series menu' button.
;
; OPTIONAL INPUTS:
;	None.
;
; OUTPUTS:
;       GOMENU  - Int; flag - set to 1 if user has selected 'escape direct
;                 to series menu , otherwise 0
;
;
; OPTIONAL OUTPUTS:
;	None.
;
; KEYWORD PARAMETERS:
;	FONT	- String; Name of the font to be used for the graphical
;		  output widget.
;
; CALLS:
;	ADAS506_PLOT	ADAS506 graphical output.
;
; SIDE EFFECTS:
;	This routine reads information from FORTRAN via a UNIX pipe.
;
; CATEGORY:
;	Adas system.
;
; WRITTEN:
;       Alessandro Lanzafame, University of Strathclyde        
;
; MODIFIED:
;	1.1	Alessandro Lanzafame				26-06-95
;	1.2	William Osborn
;		Added menu button code
; VERSION:
;	1.1	First Release
;	1.2	10-07-96
;-
;-----------------------------------------------------------------------------



PRO e6outg, dsfull, pipe, utitle, grpscal, xmin, xmax, ymin, ymax, 	$
		  hrdout, hardname, device, header, bitfile, gomenu, FONT=font


                ;**** Set defaults for keywords ****

  IF NOT (KEYWORD_SET(font)) THEN font = ''


		;**** Declare variables for input ****

  titlx = " "
  titlm = " "
  date = " "
  nener = 0
  npspl = 0
  nmx = 0
  ldef1 = 0
  xmin = 0.0
  xmax = 0.0
  ymin = 0.0
  ymax = 0.0
  lfsel = 0
  dummy = 0.0D

		;********************************
		;**** Read data from fortran ****
		;********************************
		
		;*** Read titles and date information ***

  readf, pipe, format = '(a120)' , titlx 
  readf, pipe, format = '(a80)' , titlm 
  readf, pipe, format = '(a8)' , date 
  readf, pipe, nener
  readf, pipe, npspl

		;**** now declare array dimensions ****

  temp = dblarr(nener)
  gofta = dblarr(nener)

		;*** Read data from file ***

  for i=0, nener-1 do begin
     readf, pipe, dummy
     temp(i) = dummy 
  endfor
  for i=0, nener-1 do begin
     readf, pipe, dummy
     gofta(i) = dummy 
  endfor

		;***************************************
		;*** read spline interpolated values ***
		;***************************************

  readf, pipe, losel
  if (losel eq 1) then begin
     tosa = dblarr(npspl)
     gftosa = dblarr(npspl)
     for i=0, npspl-1 do begin
        readf, pipe, dummy
        tosa(i) = dummy 
     endfor
     for i=0, npspl-1 do begin
        readf, pipe, dummy
        gftosa(i) = dummy 
     endfor
  endif

		;*********************************************
		;*** Read user selected axes ranges if any ***
		;*********************************************

  readf, pipe, ldef1 
  if (ldef1 eq 1) then begin
     readf, pipe, xmin
     readf, pipe, xmax
     readf, pipe, ymin
     readf, pipe, ymax
  endif 

		;***************************************
		;*** read minimax fit data if chosen ***
		;***************************************

  readf, pipe, lfsel 
  if (lfsel eq 1) then begin
     readf, pipe, nmx 
     gftoma = fltarr(nmx)
     toma = fltarr(nmx)
     for i = 0, nmx-1 do begin
        readf, pipe, dummy
        toma(i) = dummy
     endfor
     for i = 0, nmx - 1 do begin
        readf, pipe, dummy
        gftoma(i) = dummy
     endfor
  endif

		;***********************
		;**** Plot the data ****
		;***********************

  adas506_plot, dsfull, 						$
  		titlx, titlm, utitle, date, 				$
 		temp, gofta, tosa, gftosa, toma, gftoma, 		$
                ldef1, xmin, xmax, ymin, ymax, 				$
   		lfsel, losel, nmx, nener, npspl, 			$
		hrdout, hardname, device, header, bitfile, gomenu, FONT=font

END



