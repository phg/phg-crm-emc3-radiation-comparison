; Copyright (c) 1995, Strathclyde University .
; SCCS info: Module @(#)$Header: /home/adascvs/idl/adas5xx/adas506/adas506_proc.pro,v 1.2 2004/07/06 10:59:29 whitefor Exp $ Date $Date: 2004/07/06 10:59:29 $
;+
; PROJECT:
;       ADAS IBM MVS to UNIX conversion
;
; NAME:
;	ADAS506_PROC
;
; PURPOSE:
;	IDL ADAS user interface, processing options/input.
;
; EXPLANATION:
;	This routine creates and manages a pop-up window which allows
;	the user to select options and input data to control ADAS506
;	processing.
;
; USE:
;	This routine is ADAS506 specific, see e6ispf.pro for how it
;	is used.
;
; INPUTS:
;	PROCVAL	- A structure which determines the initial settings of
;		  the processing options widget.  The value is passed
;		  unmodified into cw_adas506_proc.pro.
;
;		  See cw_adas506_proc.pro for a full description of this
;		  structure.
;
;	STRGA() - String array  : Level designations
;
;	DSFULL	- String; The full system file name of the input 
;		          dataset selected by the user for processing.
;
;	NDTIN	- Integer : Maximum number of temperatures allowed.
;
;	IL	- Integer : Number of energy levels
;
;	NDTEM   - Integer : Maximum number of temperatures from data file.
;
;	NV	- Integer : Number of temperatures frominput data file
;
;	ITRAN	- Integer : Number of electron impact transitions.
;
;	TSCEF(,)- Double array : Electron temperatures from input data file
;				first dimension - ndtem
;				 Second dimension - 3
;				                  - 1 => Kelvin
;						    2 => eV
;						    3 => Reduced
;
;	I1A()	- Integer array : Electron impact transition, lower energy
;				  level index.
;				 
;	I2A()	- Integer array : Electron impact transition, upper energy
;				  level index.
;
;       BITFILE - String; the path to the dirctory containing bitmaps
;                 for the 'escape to series menu' button.
;
; OPTIONAL INPUTS:
;	None
;
; OUTPUTS:
;	PROCVAL	- On output the structure records the final settings of
;		  the processing selections widget if the user pressed the
;		  'Done' button, otherwise it is not changed from input.
;
;	ACT	- String; 'Done', 'Cancel' or 'Menu' for the button the
;		  user pressed to terminate the processing options
;		  window.
;
; OPTIONAL OUTPUTS:
;	None
;
; KEYWORD PARAMETERS:
;	FONT_LARGE - The name of a larger font.
;
;	FONT_SMALL - The name of a smaller font.
;
;	EDIT_FONTS - A structure of two fonts used in the table editor
;		     adas_edtab.pro, {font_norm:'',font_input:''}.
;
; CALLS:
;	CW_ADAS506_PROC	Declares the processing options widget.
;	DYNLABEL	Sets dynamic_resize keyword for label widgets
;	ADAS506_PROC_EV Called indirectly during widget management,
;			routine included in this file.
;	XMANAGER
;
; SIDE EFFECTS:
;       This widget uses a common block PROC506_BLK, to pass the
;       variables VALUE and ACTION between the two routines. 
;
; CATEGORY:
;	Widgets
;
; WRITTEN:
;       Alessandro Lanzafame, University of Strathclyde         
;
; MODIFIED:
;	1.1	Alessandro Lanzafame				26-06-95
;	1.2	William Osborn
;		Added menu button code and dynlabel
; VERSION:
;	1.1	First Release
;	1.2	10-07-96
;-
;-----------------------------------------------------------------------------


PRO adas506_proc_ev, event

  COMMON proc506_blk,action,value

  action = event.action
  CASE event.action OF

;------	'Done' button---------------------------------------
	'Done'  : begin

;------	Get the output widget value ------------------------
		      widget_control,event.id,get_value=value 
		      widget_control,event.top,/destroy

	           end

;------	'Cancel' button ------------------------------------
	'Cancel': widget_control,event.top,/destroy
;**** 'Menu' button ****
	'Menu': widget_control,event.top,/destroy

  END
END

;-----------------------------------------------------------------------------


PRO adas506_proc, procval, strga, dsfull, 				$
		  ndtin, il, ndtem, nv, itran, 				$
		  tscef, i1a, i2a, apwl, swl, act, bitfile,		$
		  FONT_LARGE=font_large, FONT_SMALL=font_small, 	$
		  EDIT_FONTS=edit_fonts

;----------------------------------------------------------
;      	declare common variables
;----------------------------------------------------------

  COMMON proc506_blk,action,value

;----------------------------------------------------------
;      	Copy "procval" to common
;----------------------------------------------------------

  value = procval

;----------------------------------------------------------
; 	Set defaults for keywords 
;----------------------------------------------------------

  IF NOT (KEYWORD_SET(font_large)) THEN font_large = ''
  IF NOT (KEYWORD_SET(font_small)) THEN font_small = ''
  IF NOT (KEYWORD_SET(edit_fonts)) THEN $
				edit_fonts = { font_norm:'', font_input:''}
		
;----------------------------------------------------------
;	create base widget
;----------------------------------------------------------

  procid = widget_base(TITLE='ADAS506 PROCESSING OPTIONS', $
					XOFFSET=100,YOFFSET=0)

;----------------------------------------------------------
;	Declare processing widget
;----------------------------------------------------------

  cwid = cw_adas506_proc(procid, strga, dsfull,  			$
                         ndtin, il, ndtem, nv, itran, 			$
                  	 tscef, i1a, i2a, apwl, swl, act, bitfile,	$
			 PROCVAL=value, 				$
		 	 FONT_LARGE=font_large, FONT_SMALL=font_small, 	$
			 EDIT_FONTS=edit_fonts)

;----------------------------------------------------------
;	Realize the new widget
;----------------------------------------------------------

  dynlabel, procid
  widget_control,procid,/realize

;----------------------------------------------------------	
;	make widget modal
;----------------------------------------------------------

  xmanager,'adas506_proc',procid,event_handler='adas506_proc_ev', 	$
					/modal,/just_reg

;----------------------------------------------------------
;	copy value back to procval for return to e6ispf 
;----------------------------------------------------------

  act = action
  procval = value
 
END

