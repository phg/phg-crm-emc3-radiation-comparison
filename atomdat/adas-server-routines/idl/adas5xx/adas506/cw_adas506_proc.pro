; Copyright (c) 1995, Strathclyde University.
; SCCS info: Module @(#)$Header: /home/adascvs/idl/adas5xx/adas506/cw_adas506_proc.pro,v 1.6 2004/07/06 12:53:07 whitefor Exp $ Date $Date: 2004/07/06 12:53:07 $
;+
; PROJECT:
;       ADAS IBM MVS to UNIX conversion
;
; NAME:
;	CW_ADAS506_PROC()
;
; PURPOSE:
;	Produces a widget for ADAS506 processing options/input.
;
; EXPLANATION:
;	This function creates a compound widget consisting of :-
;	   a text widget holding an editable 'Run title', 
;	   the dataset name/browse widget ,
;	   a widget to select the transition for analysis,
;	   a widget to request a mimax fit and enter a tolerance for it,
;	   a table widget for temperature/density data ,
;	   a button to enter default values into the table, 
;	   a message widget, and a 'Cancel' and a 'Done' button.
;
;	The compound widgets included in this widget are self managing.
;	This widget only manages events from the two 'Defaults' buttons,
;	the 'Cancel', the 'Done' and the 'Menu' buttons.
;
;	This widget only generates events for the 'Done', 'Cancel' and 'Menu'
;	buttons.
;
; USE:
;	This widget is specific to ADAS506, see adas506_proc.pro
;	for use.
;
; INPUTS:
;	TOPPARENT- Long integer, ID of parent widget.
;
;	DSFULL	- String; The full system file name of the input 
;		  dataset selected by the user for processing.
;
;	STRGA	- String array; designations for the levels
;
;	NDTIN	- Integer; number of temp/density pairs selected
;
;	IL	- Integer; Number of energy index levels
;
;	NDTEM	- Integer; maximum number of input data file temps.
;
;	NV	- Integer; number of gamma/temp pairs for the selected
;			   transition
;	
;	ITRAN	- Integer; Number of electron impact transitions
;
;	TSCEF	- Double array ; Temperature values from input data file.
;	
;	I1A	- Integer array; Lower energy level index
;
;	I2A	- Integer array; Upper energy level index
;	
;	The inputs map exactly onto variables of the same
;	name in the ADAS506 FORTRAN program.
;
;       BITFILE - String; the path to the dirctory containing bitmaps
;                 for the 'escape to series menu' button.
;
; OPTIONAL INPUTS:
;	None
;
; OUTPUTS:
;	The return value of this function is the ID of the declared widget.
;
;	ACT 	- String; result of this widget, 'done' or 'cancel'
;
; OPTIONAL OUTPUTS:
;	None
;
; KEYWORD PARAMETERS:
;	PROCVAL	- A structure which determines the initial settings of
;		  the processing options widget.
;
;		  The default PROCVAL is created thus;
;
;     		ps = {proc506_set, $
;			new   : 0,		$
;                	title : '',             $
;			istrn : 0, 		$
;			maxt  : 0, 		$
;			ifout : 1 ,             $
;                	tine  : temp_arr,       $
;                	lfsel : 0,              $
;                	tolval: '5', 		$
;			losel : 0		$
;             }
;
;
;		NEW     Flag which defines whether or not default values
;			exist or not. (< 0 if no values exist)
; 		TITLE	Entered general title for program run
;		ISTRN   Selected transition index
;		MAXT    Number of temperature values selected
;		IFOUT   Index indicating which units are being used
;		TINE    User supplied temperature values for fit.
;		LFSEL   Flag as to whether polynomial fit is chosen
;		TOLVAL  Tolerance required for goodness of fit if
;			polynomial fit is selected.
;		LOSEL   Flag whether or not interpolated values for spline
;			fit have been used.
;
;		All of these structure elements map onto variables of
;		the same name in the ADAS506 FORTRAN program.
;
;
;	UVALUE	- A user value for the widget. Default 0.
;
;	FONT_LARGE - The name of a larger font.  Default current system
;		     font
;
;	FONT_SMALL - The name of a smaller font. Default current system
;		     font.
;
;	EDIT_FONTS - A structure of two fonts used in the table editor
;		     adas_edtab.pro, {font_norm:'',font_input:''}.  Defaults
;		     to current system font.
;
;	NUM_FORM   - String; Numeric format to use in tables.  Default
;			'(E10.3)'
;		     
;
; CALLS:
;	POPUP		Popup warning window with buttons.
;	CW_ADAS_DSBR	Dataset name and comments browsing button.
;	CW_ADAS_TABLE	Adas data table widget.
;	CW_ADAS_SEL	Adas multiple selection widget.
;	CW_SINGLE_SEL	Adas scrolling table and selection widget.
;	CW_OPT_VALUE    Adas option selection widget with required input 
;			value. 
;
;	See side effects for other related widget management routines.
;
; SIDE EFFECTS:
;
;	The following widget management routines are included in this file;
;	PROC506_GET_VAL()	Returns the current PROCVAL structure.
;	PROC506_EVENT()		Process and issue events.
;	
; CATEGORY:
;	Compound Widget
;
; WRITTEN:
;       Alessandro Lanzafame, University od Strathclyde         
;
; MODIFIED:
;	1.1	Alessandro Lanzafame				
;		First version
;	1.2	Tim Hammond				
;		Minor spelling correction
;	1.3	Alessandro Lanzafame		
;		Fixed problem with tabledata
;               (when fills rest of table with blanks)
;	1.4	Tim Hammond
;		Modified use of fonts and sizes of tables for use on
;               different platforms.
;	1.5	William Osborn
;		Added menu button code
;	1.6	Richard Martin
;		IDL 5.5 fixes
;
; VERSION:
;	1.1	26-06-95
;	1.2	26-06-95
;	1.3	11-09-95
;	1.4	27-02-96
;	1.5	10-07-96
;	1.6	28-01-02
;
;-
;-----------------------------------------------------------------------------

FUNCTION proc506_get_val, id

;-------------------------------------------------------------------------
  ;-----------------------------------------------------------------------
  ;	Return to caller on error
  ;-----------------------------------------------------------------------

  ON_ERROR, 2

  ;-----------------------------------------------------------------------
  ;	Retrieve the state
  ;	Get first_child widget id because state is stored there
  ;-----------------------------------------------------------------------

  first_child = widget_info(id, /child)
  widget_control, first_child, get_uvalue = state

  ;-----------------------------------------------------------------------
  ;	Get run title from widget. 
  ;	Then centre in in string of 40 characters
  ;-----------------------------------------------------------------------

  widget_control,state.runid,get_value=title
  title = strcompress(title(0))
  title_len = strlen(title) 
  if (title_len gt 40 ) then begin 
	title = strmid(title,0,37)
        widget_control, state.messid, set_value="Title too long - truncated "
        widget_control, state.runid, set_value=title
	wait ,1
  endif
  pad = (40 - title_len)/2 
  spaces = '                                         '
  title(0) = strmid(spaces,0,(pad-1)) + title + strmid(spaces,0,(pad-1))
  

  ;-----------------------------------------------------------------------
  ;	Get new temperature data from table widget
  ;	If losel is selected                       
  ;-----------------------------------------------------------------------

  if (state.losel eq 1)  then begin
     widget_control,state.tempid,get_value=tempval
     tabledata = tempval.value
     ifout = tempval.units+1

  ;-----------------------------------------------------------------------
  ;	Copy out temperature values
  ;-----------------------------------------------------------------------

     tine = dblarr(state.ndtin)
     blanks = where(strtrim(tabledata(0,*),2) eq '')

  ;-----------------------------------------------------------------------
  ;	next line assumes that all blanks are
  ;	at the end of the columns
  ;-----------------------------------------------------------------------

     if blanks(0) ge 0 then maxt=blanks(0) else maxt=state.ndtin
     tine(0:maxt-1) = double(tabledata(0,0:maxt-1))

     if maxt lt state.ndtin then begin
       tine(maxt:state.ndtin-1) = double(0.0)
     endif
  endif else begin	; *** use existing values
     ifout = 1
     tine = state.tscef(0,*)
     maxt = state.maxt
  endelse
   

  ;-----------------------------------------------------------------------
  ;	Get selection of polyfit from widget
  ;-----------------------------------------------------------------------

  widget_control,state.optid,get_uvalue=polyset,/no_copy
  lfsel = polyset.optionset.option[0]
  if (num_chk(polyset.optionset.value[0]) eq 0) then begin
     tolval = (polyset.optionset.value[0])
  endif else begin
     tolval = -999
  endelse
  widget_control,state.optid,set_uvalue=polyset,/no_copy

  ;-----------------------------------------------------------------------
  ;	get new index for data block selected
  ;-----------------------------------------------------------------------

  widget_control, state.indexid, get_value=select_block
  istrn = select_block  
  
  ;-----------------------------------------------------------------------
  ;	write selected values to PS structure
  ;-----------------------------------------------------------------------

     ps = { 			 		$
	        new   : 0, 			$
                title : title,          	$
		istrn : istrn,			$
		ifout : ifout,			$
		maxt  : maxt,			$
                tine  : tine,       		$
                lfsel : lfsel,              	$
                tolval: tolval,			$
		losel : state.losel		$
              }

   
  widget_control, first_child, set_uvalue=state, /no_copy

  RETURN, ps

END

;-----------------------------------------------------------------------------

FUNCTION proc506_event, event

;-------------------------------------------------------------------------

  ;-----------------------------------------------------------------------
  ;	Base ID of compound widget
  ;-----------------------------------------------------------------------
  parent=event.handler

  ;-----------------------------------------------------------------------
  ;	Retrieve the user value state
  ;	Get id of first_child widget because 
  ;	user value "state" is stored there
  ;-----------------------------------------------------------------------

  first_child = widget_info(parent, /child)
  widget_control, first_child, get_uvalue=state,/no_copy

  ;-----------------------------------------------------------------------
  ;	Clear previous messages
  ;-----------------------------------------------------------------------

  widget_control,state.messid,set_value=' '

  ;-----------------------------------------------------------------------
  ;	Process events
  ;-----------------------------------------------------------------------

  CASE event.id OF

  ;-----------------------------------------------------------------------
  ;	temp. selected for output
  ;-----------------------------------------------------------------------

    state.lbut : begin
		   if (state.losel eq 0) then state.losel = 1 else $
			state.losel = 0 
		   widget_control, state.deftid, sensitive= state.losel
		   widget_control, state.tempid, sensitive = state.losel
		 end

  ;-----------------------------------------------------------------------
  ;	Default temperature button
  ;-----------------------------------------------------------------------
    state.deftid: begin

  ;-----------------------------------------------------------------------
  ;	popup window to confirm overwriting current values
  ;-----------------------------------------------------------------------

	action= popup(message='Confirm Overwrite values with Defaults', $
			buttons=['Confirm','Cancel'],font=state.font)

	if action eq 'Confirm' then begin

  ;-----------------------------------------------------------------------
  ;	Get current table widget value 
  ;-----------------------------------------------------------------------

 	   widget_control,state.tempid,get_value=tempval

  ;-----------------------------------------------------------------------
  ;	Copy defaults into value structure
  ;	For default values use the existing
  ;	table values and let user modify them
  ;	then set all density values to same
  ;	value
  ;-----------------------------------------------------------------------

             units = tempval.units+1
	     ndtem = state.ndtem
   	     nv = state.nv 
 	     if nv gt 0 then begin
   	       tempval.value(0,0:nv-1) = $
		   strtrim(string(tempval.value(units,0:nv-1), $
		                               format=state.num_form),2)
 	  end

  ;-----------------------------------------------------------------------
  ;	Fill out the rest with blanks
  ;-----------------------------------------------------------------------

 	  if nv lt state.ndtin then begin
   	    tempval.value(0,nv:state.ndtin-1) = ''
 	  end

  ;-----------------------------------------------------------------------
  ;	Copy new data to table widget
  ;-----------------------------------------------------------------------

 	  widget_control,state.tempid, set_value=tempval
	end

      end

  ;-----------------------------------------------------------------------
  ;	Cancel button
  ;-----------------------------------------------------------------------

    state.cancelid: new_event = {ID:parent, TOP:event.top, $
				HANDLER:0L, ACTION:'Cancel'}

  ;-----------------------------------------------------------------------
  ;	Done button
  ;-----------------------------------------------------------------------

    state.doneid: begin

  ;-----------------------------------------------------------------------
  ;	Check all user input is legal
  ;-----------------------------------------------------------------------

	error = 0

  ;-----------------------------------------------------------------------
  ;	Have to restore "state" before calls
  ;	to proc506_get_val so it can be used
  ;	there.
  ;-----------------------------------------------------------------------

	widget_control, first_child, set_uvalue=state, /no_copy

	widget_control,event.handler,get_value=ps

  ;-----------------------------------------------------------------------
  ;	reset state variable 
  ;-----------------------------------------------------------------------

	widget_control, first_child,get_uvalue=state, /no_copy

  ;-----------------------------------------------------------------------
  ;	check temp values entered 
  ;-----------------------------------------------------------------------

	if (ps.losel eq 1) then begin
	   if error eq 0 and ps.maxt eq 0 then begin
	      error = 1
	      message='Error: No temperatures entered.'
            endif
	endif

  ;-----------------------------------------------------------------------
  ;	Check to see if index has been selected
  ;-----------------------------------------------------------------------

	if error eq 0 and ps.istrn lt 0 then begin
	   error = 1
	   message='Error: Invalid block selected'
	end

  ;-----------------------------------------------------------------------
  ;	Check to see if sensible tolerance is selected.
  ;-----------------------------------------------------------------------

	if (error eq 0 and ps.lfsel eq 1) then begin $
	    if (float(ps.tolval) lt 0)  or $
		          (float(ps.tolval) gt 100) then begin
	       error = 1
	       message='Error: Tolerance for polyfit must be 0-100% '
	    endif
	endif

  ;-----------------------------------------------------------------------
  ;	return value or flag error
  ;-----------------------------------------------------------------------

	if error eq 0 then begin
	  new_event = {ID:parent, TOP:event.top, HANDLER:0L, ACTION:'Done'}
        end else begin
	  widget_control,state.messid,set_value=message
	  new_event = 0L
        end
	

      end

		;*********************
                ;**** Menu button ****
		;*********************

    state.outid: begin
          new_event = {ID:parent, TOP:event.top, HANDLER:0L,          $
                       ACTION:'Menu'}
      end

    ELSE: new_event = 0L

  ENDCASE

  ;-----------------------------------------------------------------------
  ;	make "state" available to other files
  ;-----------------------------------------------------------------------

  widget_control, first_child, set_uvalue=state,/no_copy

  RETURN, new_event
END

;-----------------------------------------------------------------------------

function cw_adas506_proc, topparent, strga, dsfull, $
 		ndtin, il, ndtem, nv, itran,        $
		tscef, i1a, i2a, apwl, swl, act, bitfile,    $
		procval=procval, uvalue=uvalue,     $
		font_large=font_large, font_small=font_small, $
		edit_fonts=edit_fonts, num_form=num_form


  ;-----------------------------------------------------------------------
  ;	Set defaults for keywords
  ;-----------------------------------------------------------------------

  if not (keyword_set(uvalue)) then uvalue = 0
  if not (keyword_set(font_large)) then font_large = ''
  if not (keyword_set(font_small)) then font_small = ''
  if not (keyword_set(edit_fonts)) then $
				edit_fonts = {font_norm:'',font_input:''}
  if not (keyword_set(num_form)) then num_form = '(E10.3)'

  if not (keyword_set(procval)) then begin
     ps = { $ 
		new   : 0, 		$
		title : '',             $
		istrn : 0,		$
		maxt  : ndtin,		$
		ifout : 1,		$
                tine  : tscef(1,*),     $
                lfsel : 0,              $
                tolval: '5',		$
		losel : 0		$
              }
  endif else begin
	ps = { $ 
		new   : procval.new,    $
                title : procval.title,  $
                istrn : procval.istrn,  $
                maxt  : procval.maxt,   $
                ifout : procval.ifout,  $
                tine  : procval.tine,   $
                lfsel : procval.lfsel,  $
                tolval: procval.tolval, $
		losel : procval.losel   $
	     }
  endelse

  ;---------------------------------------------------------
  ;---- Modify certain parameters and results depending ----
  ;---- on the machine being used                       ----
  ;---------------------------------------------------------

  machine = GETENV('TARGET_MACHINE')
  if machine eq 'HPUX' then begin
      y_size = 4
      large_font = font_small
  endif else begin
      y_size = 6
      large_font = font_large
  endelse

  ;-----------------------------------------------------------------------
  ;	Assemble temperature table data
  ;-----------------------------------------------------------------------
  ;	The adas table widget requires data to be 
  ;	input as strings, so all the numeric data 
  ;	has to be written into a string array.    
  ;	Declare temp. table array                 
  ;	col 1 has user temperature values         
  ;	col 2 has temperature values from files,  
  ;	which can have one of three possible units
  ;-----------------------------------------------------------------------

  maxt = ps.maxt
  tabledata = strarr(4,ndtin)

  ;-----------------------------------------------------------------------
  ;	Copy out temperature values
  ;	number of temperature      
  ;	values for this data block
  ;-----------------------------------------------------------------------

  if (maxt gt 0) then begin
    
    tabledata(0,0:maxt-1) = $
		strtrim(string(ps.tine(0:maxt-1),format=num_form),2)
    tabledata(1,0:nv-1 ) = $
	        strtrim(string(tscef(0,0:nv-1),format=num_form),2)
    tabledata(2,0:nv-1) = $
		strtrim(string(tscef(1,0:nv-1),format=num_form),2)
    tabledata(3,0:nv-1) = $
                strtrim(string(tscef(2,0:nv-1),format=num_form),2)

  ;-----------------------------------------------------------------------
  ;	fill rest of table with blanks
  ;-----------------------------------------------------------------------

    blanks = where(tabledata eq 0.0) 
    if (n_elements(blanks) gt 1) then tabledata(0,blanks) = ' '
    if (n_elements(blanks) eq 1) then begin
       if (blanks(0) ne -1) then tabledata(0,blanks) = ' '
    endif
    if (nv gt ndtin) then tabledata(1:3,nv:ndtin-1) = ' '
  end

  ;-----------------------------------------------------------------------
  ;	Create the 506 Processing options/input window
  ;-----------------------------------------------------------------------


  ;-----------------------------------------------------------------------
  ;	create titled base widget
  ;-----------------------------------------------------------------------

  parent = widget_base(topparent, UVALUE = uvalue, $
			title = 'ADAS506 PROCESSING OPTIONS', $
			EVENT_FUNC = "proc506_event", $
			FUNC_GET_VALUE = "proc506_get_val", $
			/COLUMN)

  ;-----------------------------------------------------------------------
  ;	Create a dummy widget just to hold value of
  ;	"state" variable so as not to get confused  
  ;	with any other values. Adopt IDL practice   
  ;	of using first child widget                 
  ;-----------------------------------------------------------------------

  first_child = widget_base(parent)

  topbase = widget_base(first_child,/column)

  ;-----------------------------------------------------------------------
  ;	add run title
  ;-----------------------------------------------------------------------

  base = widget_base(topbase,/row)
  rc = widget_label(base,value='Title for Run',font=large_font)
  runid = widget_text(base,value=ps.title,xsize=38,font=large_font,/edit)

  ;-----------------------------------------------------------------------
  ;	add dataset name and browse button
  ;-----------------------------------------------------------------------

  rc = cw_adas_dsbr(topbase,dsfull,font=large_font)

  ;-----------------------------------------------------------------------
  ;	add information from data file about data set
  ;-----------------------------------------------------------------------
;  file_info = 'Number of Electron transitions : ' + $
;			strtrim(strcompress(string(itran))) + $
;			'   Number of energy levels : ' + $
;			strtrim(strcompress(string(il)))
  rc = widget_label(topbase,value=$
			'Number of Transitions : ' + $
			strtrim(strcompress(string(itran)))  , $
			font=large_font $
		    )

  rc = widget_label(topbase,value=$
			'Number of Index Energy levels : ' + $
			strtrim(strcompress(string(il))) , $
			font=large_font $
		    )

  ;-----------------------------------------------------------------------
  ;	add a window to display and select index   
  ;	first create data array for table          
  ;	Then convert to 1D string array(text_table)
  ;	call cw_single_sel.pro for options choice 
  ;-----------------------------------------------------------------------

  block_info = strarr(5, itran)
  for i = 0, itran-1 do begin
     block_info(0,i) = strcompress(string(i+1))
  endfor

   for i = 0, itran-1 do begin
       block_info(1,i) = strcompress(i1a(i)) 
       if (i1a(i) lt 10) then  begin 
	  block_info(1,i) = block_info(1,i) + '  ' +strga(i1a(i)-1)
       endif else begin
          block_info(1,i) = block_info(1,i) + ' '  +strga(i1a(i)-1)
       endelse

       block_info(2,i) = strcompress(i2a(i)) 
       if (i2a(i) lt 10) then begin
          block_info(2,i) = block_info(2,i) + '  ' +strga(i2a(i)-1)
       endif else begin
          block_info(2,i) = block_info(2,i) + ' ' +strga(i2a(i)-1)
       endelse

      block_info(3,i) = string(apwl(i),format='(f10.3)')
      block_info(4,i) = string(swl(i),format='(f10.3)')

   endfor
      
  titles = [['TRANSITION','  ----- LOWER LEVEL ----- ',  $
			  '  ----- UPPER LEVEL ----- ',  $
             'APPROXIMATE','SPECTROSCOPIC'           ],  $
   	   [ '  INDEX   ','    INDEX  DESIGNATION    ',  $
			  '    INDEX  DESIGNATION    ',  $
             'WAVELENGTH' ,'WAVELENGTH']  ]

  select_data = text_table(block_info, colhead=titles, /noindex)
  coltitles = select_data(0:1)
  select_data = select_data(2:itran+1)
  if (ps.istrn gt itran) then select_index = 0 else select_index = ps.istrn
  indexid = cw_single_sel( parent, select_data, value=select_index, $
			   title='Select Transition', $
			   coltitles = coltitles, $
			   ysize = (y_size-2), $
			   font=font_small, $
			   big_font=large_font $
			 )

  ;-----------------------------------------------------------------------
  ;	Another base
  ;-----------------------------------------------------------------------

  tablebase = widget_base(parent,/row)

  ;-----------------------------------------------------------------------
  ;	base for the table and defaults button
  ;	and whether table used for text output
  ;-----------------------------------------------------------------------

  base = widget_base(tablebase,/column,/frame)

  ;-----------------------------------------------------------------------
  ;	"LOSEL" parameter switch
  ;-----------------------------------------------------------------------

  lbase = widget_base(base, /row)
  llabel = widget_label(lbase, value="Select Temperatures for output files", $
				font = large_font)
  lbbase = widget_base(lbase, /row, /nonexclusive)
  lbut = widget_button(lbbase, value=' ')
  widget_control, lbut, set_button=ps.losel

  ;-----------------------------------------------------------------------
  ;	temperature data table
  ;-----------------------------------------------------------------------

  colhead = [['Output','Input'], $
	     ['','']]

  ;-----------------------------------------------------------------------
  ;	convert FORTRAN index to IDL index
  ;-----------------------------------------------------------------------

  units = ps.ifout-1 
  unitname = ['Kelvin','eV','Reduced']

  ;-----------------------------------------------------------------------
  ;	Two  columns in the table and three sets of units.
  ;	Column 1 has the same values for all three 
  ;	units but column 2 switches between sets 2,3 & 4
  ;	in the input array tabledata as the units change.
  ;-----------------------------------------------------------------------

  unitind = [[0,0,0],[1,2,3]]
  table_title =  ["Output Electron Temperatures "] 

  ;-----------------------------------------------------------------------
  ;	table of data
  ;-----------------------------------------------------------------------

  tempid = cw_adas_table(base, tabledata, units, unitname, unitind, 	$
			UNITSTITLE = 'Temperature Units', 		$
			COLEDIT = [1,0], COLHEAD = colhead, 		$
			TITLE = table_title, 				$
			ORDER = [1,0], 					$ 
			LIMITS = [1,1], CELLSIZE = 12, 			$
			/SCROLL, YTABSIZE = 7, NUM_FORM = num_form, 	$
			FONTS = edit_fonts, FONT_LARGE = large_font, 	$
			FONT_SMALL = font_small, YTEXSIZE=y_size)

  ;-----------------------------------------------------------------------
  ;	Default buttons
  ;-----------------------------------------------------------------------

  deftid = widget_button(base,value=' Default Temperature Values  ',	$
		font=large_font)

  ;-----------------------------------------------------------------------
  ;	Make table non/sensitive as ps.losel 
  ;-----------------------------------------------------------------------

  widget_control, tempid, sensitive = ps.losel
  widget_control, deftid, sensitive = ps.losel

  ;-----------------------------------------------------------------------
  ;	Add polynomial fit selection
  ;-----------------------------------------------------------------------

  polyset = { option:intarr(1), val:strarr(1)}  ; defined thus because 
						; cw_opt_value.pro
                                                ; expects arrays
  polyset.option = [ps.lfsel]
  polyset.val = [ps.tolval]
  options = ['Fit Polynomial']
  optbase = widget_base(topbase,/frame)
  optid   = cw_opt_value(optbase, options, 				$
			       title='Polynomial Fitting',		$
			       limits = [0,100], 			$
			       value=polyset, font=large_font 		$
			     )
  ;-----------------------------------------------------------------------
  ;	Error message
  ;-----------------------------------------------------------------------

  messid = widget_label(parent,font=large_font, 			$
	value='Edit the processing options data and press Done to proceed')

  ;-----------------------------------------------------------------------
  ;	add the exit buttons
  ;-----------------------------------------------------------------------

  base = widget_base(parent,/row)
  menufile = bitfile + '/menu.bmp'
  read_X11_bitmap, menufile, bitmap1
  outid = widget_button(base, value=bitmap1)          ;menu button
  cancelid = widget_button(base,value='Cancel',font=large_font)
  doneid = widget_button(base,value='Done',font=large_font)
  
  ;-----------------------------------------------------------------------
  ;	create a state structure for the pop-up window.
  ;-----------------------------------------------------------------------

  new_state = { runid:runid,  		$
		messid:messid, 		$
		deftid:deftid,		$
		tempid:tempid, 		$
		losel : ps.losel, 	$
                lbut  : lbut,		$
		optid:optid,		$
 		indexid:indexid,	$
		cancelid:cancelid,	$
		doneid:doneid, 		$
		outid:outid,		$
		dsfull:dsfull,		$
		ndtin:ndtin,		$
		ndtem:ndtem,		$
 		nv:nv,			$
		maxt:maxt, 		$
		tscef:tscef,		$
		font:large_font,	$
		num_form:num_form	$
	      }

  ;-----------------------------------------------------------------------
  ;	Save initial state structure
  ;-----------------------------------------------------------------------

  widget_control, first_child, set_uvalue=new_state,/no_copy

  RETURN, parent

END

