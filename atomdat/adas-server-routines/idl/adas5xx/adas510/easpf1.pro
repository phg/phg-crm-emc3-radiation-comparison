; Copyright (c) 2002, Strathclyde University.
; SCCS Info: Module @(#)$Header: /home/adascvs/idl/adas5xx/adas510/easpf1.pro,v 1.1 2004/07/06 13:47:45 whitefor Exp $	Date  $Date: 2004/07/06 13:47:45 $
;+
; PROJECT:
;       ADAS IBM MVS to UNIX conversion
;
; NAME:
;	E3SPF1
;
; PURPOSE:
;	Invokes IDL user interface.
;
; EXPLANATION:
;	This routine calls the Output Options' part of the interface then
;	sets some flags depending on what exit button was clicked.
;
; USE:
;	The use of this routine is specific to ADAS510, See adas510.pro.
;
; INPUTS:
;	DSFULL  - The name of the data set being analyzed.
;
;	VALUE	- A structure which determines the initial settings of
;		  the dataset selection widget.  The initial value is
;		  set in adas510.pro.  VALUE is passed un-modified
;		  through to cw_adas_in.pro, see that routine for a full
;		  description.
;
;	HEADER  - Header information used for text output.
;
;       BITFILE - String; the path to the directory containing bitmaps
;                 for the 'escape to series menu' button.
;
; OPTIONAL INPUTS:
;	None.
;
; OUTPUTS:
;	LPEND	- Integer; indicates user action on leaving output
;		  selections.  lpend is set to 1 if the user exited
;		  with the 'Cancel' button and 0 if the user exited
;		  with 'Done'.
;
;	VALUE	- On output the structure records the final settings of
;		  the output selection widget if the user pressed the
;		  'Done' button otherwise it is not changed from input.
;
;       GOMENU  - Int; flag - set to 1 if user has selected 'escape direct
;                 to series menu , otherwise 0
;
; OPTIONAL OUTPUTS:
;	None.
;
; KEYWORD PARAMETERS:
;	DEVLIST	- A string array;  Each element of the array is the
;		  name of a hardcopy graphics output device.  This
;		  list must mirror the DEVCODE list.  DEVLIST is the
;		  list which the user will see.
;		  e.g ['Post-Script','HP-PCL','HP-GL']
;
;	FONT	- Supplies the font to be used for the interface widgets.
;
; CALLS:
;	ADAS510_OUT	User interface - output options.
;
; SIDE EFFECTS:
;	None.
;
; CATEGORY:
;	Adas system.
;
; WRITTEN:
;       Richard Martin, University of Strathclyde, 17-06-2002
;
; MODIFIED:
;	1.1	Richard Martin
;           First Release
;
; VERSION:
;	1.1	17-06-02
;
;-
;-----------------------------------------------------------------------------

PRO easpf1, lpend, value, dsfull, header, bitfile, gomenu,        $
            DEVLIST=devlist, FONT=font

                ;**** Set defaults for keywords ****

    IF NOT (KEYWORD_SET(font)) THEN font = ''

		;**************************************
		;**** Pop-up output options widget ****
		;**************************************

    adas510_out, value, dsfull, action, bitfile, DEVLIST=devlist, FONT=font

		;*********************************************
		;**** Act on the output from the widget   ****
                ;**** There are three    possible actions ****
                ;**** 'Done', 'Cancel' and 'Menu.         ****
		;*********************************************

    if action eq 'Done' then begin
        lpend = 0
    endif else if action eq 'Menu' then begin
        lpend = 0
        gomenu = 1
    endif else if action eq 'File' then begin
    	  lpend = 2
    endif else begin
        lpend = 1
    endelse


END
