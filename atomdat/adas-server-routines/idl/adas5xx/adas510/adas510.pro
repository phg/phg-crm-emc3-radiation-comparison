;+
; PROJECT:
;       ADAS
;
; NAME:
;	ADAS510
;
; PURPOSE:
;	The highest level routine for the ADAS 510 program.
;
; EXPLANATION:
;	This routine is called from the main adas system routine, adas.pro,
;	to start the ADAS 510 application. 
;
; USE:
;	First the system settings must be established by calling
;	adas_sys_set.pro then adas510.pro is called to start the
;	ADAS 510 application;
;
;	adas_sys_set, adasrel, fortdir, userroot, centroot, $
;		      devlist, devcode, font_large, font_small, edit_fonts
;	adas510,   adasrel, fortdir, userroot, centroot, devlist, $
;		   devcode, font_large, font_small, edit_fonts
;
; INPUTS:
;	ADASREL - A string indicating the ADAS system version, 
;		  e.g ' ADAS RELEASE: ADAS93 V1.1.0'.  The first
;		  character should be a space.
;
;	FORTDIR - A string holding the path to the directory where the
;		  FORTRAN executables are, e.g '/disk/adas/fortran/exec'
;
;	USERROOT - A string holding the path to the root directory of
;		   the user's adas data. e.g '/disk/adas/adas'
;		   This root directory will be used by adas to construct
;		   other path names.  For example the users default data
;		   for adas503 should be in /disk/adas/adas/adf15.  In
;		   particular the user's default interface settings will
;		   be stored in the directory USERROOT+'/defaults'.  An
;		   error will occur if the defaults directory does not
;		   exist.
;
;	CENTROOT - Like USERROOT, but this directory points to the
;		   central data area for the system.  User defaults are
;		   not stored on CENTROOT.
;
;	DEVLIST - A string array of hardcopy device names, used for
;		  graphical output. e.g ['Post-Script','HP-PCL','HP-GL']
;		  This array must mirror DEVCODE.  DEVCODE holds the
;		  actual device names used in a SET_PLOT statement.
;
;	DEVCODE - A string array of hardcopy device code names used in
;		  the SET_PLOT statement, e.g ['PS', 'PCL', 'HP']
;		  This array must mirror DEVLIST.
;
;	FONT_LARGE - The name of a larger font e.g 'courier_bold14'
;
;	FONT_SMALL - The name of a smaller font e.g 'courier_bold12'
;
;	EDIT_FONTS - A structure of two fonts used in the table editor
;		     adas_edtab.pro, {font_norm:'',font_input:''}.
;		     The two fonts are used to differentiate between
;		     editable and non-editable parts of the table. You
;		     may have to experiment to get two fonts which give
;		     the same default spacing in the edit widget.
;		     e.g {font_norm:'helvetica_bold14', $
;			  font_input:'helvetica_oblique14'}
;
; OPTIONAL INPUTS:
;	None
;
; OUTPUTS:
;	None - Note this routine should leave all inputs un-modified.
;
; OPTIONAL OUTPUTS:
;	None
;
; KEYWORD PARAMETERS:
;	None
;
; CALLS:
;	ADAS_IN		ADAS input file selection widget.
;	EAISPF		Reads in data and spawns the processing widget.
;	XXDATE		Get date and time from operating system.
;	EASPF1		Invokes the output options widget.
;	EAOUTG		Displays the output graph.
;	ADASLOADCT      Sets up colour table for plots.
;
; SIDE EFFECTS:
;	 
;
; CATEGORY:
;	Adas system.
;	
; WRITTEN:
;        Richard Martin, University of Strathclyde, 14-06-2002
;	  Based on adas503 v1.14
;
; MODIFIED:
;	1.1     Richard Martin
;		 - First version.
;	1.2     Martin O'Mullane
;		 - Replaced eadata with read_adf40.
;
; VERSION: 
;	1.1     14-06-2002
;	1.2     26-11-2004
;
;-
;-----------------------------------------------------------------------------


PRO ADAS510,	adasrel, fortdir, userroot, centroot, 			$
		devlist, devcode, font_large, font_small, edit_fonts

		;************************
		;**** Initialisation ****
		;************************

    adasprog = ' PROGRAM: ADAS510 V1.2'
    lpend    = 0
    gomenu   = 0
    deffile  = userroot + '/defaults/adas510_defaults.dat'
    device   = ''
    bitfile  = centroot + '/bitmaps'

		;******************************************
		;**** Search for user default settings ****
		;**** If not found create defaults     ****
		;******************************************

    files = findfile(deffile)
    if files(0) eq deffile then begin
        restore, deffile
        inval.centroot = centroot+'/adf40/'
        inval.userroot = userroot+'/adf40/'
    endif else begin
        inval =   { 	ROOTPATH	:	userroot+'/adf40/', 	$
		  	FILE		:	'', 			$
		  	CENTROOT	:	centroot+'/adf40/', 	$
		  	USERROOT	:	userroot+'/adf40/' 	}
        procval = {	NMET		:	-1			}
        outval =  { 	GRPOUT		:	0, 		        $
			GTIT1		:	'', 			$
			GRPSCAL		:	0, 			$
			XMIN		:	'',  	 		$
			XMAX		:	'',   			$
			XMIN1		:	'',  			$
			XMAX1		:	'',   			$
			XMIN2		:	'',  			$
			XMAX2		:	'',   			$
			YMIN		:	'',  			$
			YMAX		:	'',   			$
			ZMIN		:	'',  			$
			ZMAX		:	'',   			$
			HRDOUT		:	0, 			$
			HARDNAME	:	'', 			$
			GRPDEF		:	'', 			$
			GRPFMESS	:	'', 			$
			GRPSEL		:	-1, 			$
			GRPRMESS	:	'', 			$
			DEVSEL		:	-1, 			$
			GRSELMESS	:	'', 			$
			TEXOUT		:	0, 			$
			TEXAPP		:	-1, 			$
			TEXREP		:	0, 			$
			TEXDSN		:	'paper.txt',		$
			TEXDEF		:	'',			$
			TEXMES		:	'' 			}
	  pecdata =    { FPEC		: 	0.0 			}
	  
    endelse

		;****************************
		;*** Set up colour table ****
		;****************************
		
adasloadct

LABEL100:
		;**** Check FORTRAN still running ****
 

                ;************************************************
                ;**** Invoke user interface widget for       ****
                ;**** Data file selection                    ****
                ;************************************************

    adas_in, inval, action, WINTITLE = 'ADAS 510 INPUT',                $
             TITLE = 'Input Dataset', FONT = font_large


                ;********************************
                ;**** Construct dataset name ****
                ;********************************

    dsfull = inval.rootpath + inval.file
    
                ;********************************************
                ;**** Act on the event from the widget   ****
                ;**** There are only two possible events ****
                ;**** 'Done' and 'Cancel'.               ****
                ;********************************************

    if action eq 'Done' then begin
        rep = 'NO'
    endif else begin
        rep = 'YES'
    endelse


		;**** If cancel selected then end program ****

    if rep eq 'YES' then begin 
	goto, LABELEND
    endif

		;**** Check FORTRAN still running ****

LABEL200:

		;**** Check FORTRAN still running ****



		;******************************************
		;**** Read data from selected file and ****
		;**** invoke user interface widget for ****
		;**** Processing options               ****
		;*******************************************

    eaispf, lpend, procval, pecdata, dsfull, gomenu, bitfile,  		$
	    FONT_LARGE=font_large, FONT_SMALL=font_small, 			$
	    EDIT_FONTS=edit_fonts

                ;**** If menu button clicked goto end ****

    if gomenu eq 1 then begin
        goto, LABELEND
    endif
    
		;**** If cancel selected then goto 100 ****

    if lpend eq 1 then goto, LABEL100

            ;*************************************************
		;**** Fortran processing now in progress. Get ****
		;**** date and time for this run.	          ****
            ;*************************************************

    date = xxdate()

		;**** Create header for output. ****

    header = adasrel + adasprog + ' DATE: ' + date(0) + ' TIME: ' + date(1)

		;**************************************************
		;**** Now call fortran to interpolate the data ****
		;**** for each wavelength and store in fpec    ****
		;**************************************************

    ntdim = long(pecdata.ntdim)
    nddim = long(pecdata.nddim)   

    pix   = pecdata.pix

    dummy = where(procval.tin gt 0.0,itnum)
    itval = long(itnum)

    ita     = long(pecdata.ita[procval.ibsel])
    teta    = dblarr(ita)
    teta[*] = double(pecdata.tvals[0:ita-1,procval.ibsel])
    ida     = long(pecdata.ida[procval.ibsel])    	
    teda    = dblarr(ida)
    teda[*] = double(pecdata.teda[0:ida-1,procval.ibsel])

    teva    = double(procval.tin[0:itval-1])
    din     = double(procval.din[0:itval-1])

    ; convert form index 0 to block 1 etc.
    
    read_adf40, file=dsfull, block=procval.ibsel+1, te=teva, dens=din, data=tmp
    fpec = transpose(tmp)
	
LABEL300:

		;************************************************
		;**** invoke user interface widget for       ****
		;**** Output options                         ****
		;************************************************

    easpf1, lpend, outval, dsfull, header, bitfile, gomenu, 	$
            DEVLIST=devlist, FONT=font_large

                ;**** If menu button clicked, go to end ****

    if gomenu eq 1 then begin
        outval.texmes = ''
        goto, LABELEND
    endif 
                   
            ;**********************************************
	    ;**** If cancel selected then erase output ****
	    ;**** messages and goto 200.	       ****
            ;**********************************************

    if lpend eq 1 then begin
        outval.texmes = ''
        goto, LABEL200
    endif else if lpend eq 2 then begin
         outval.texmes = ''
        goto, LABEL100   
    endif

		;************************************************
		;**** If graphical output requested          ****
		;**** Communicate with e3outg in fortran and ****
		;**** invoke widget Graphical output         ****
		;************************************************

    if outval.texout eq 1 then begin

            ;***************************************************
	    ;**** If text output is requested enable append ****
	    ;**** for next time and update the default to   ****
	    ;**** the current text file.                    ****
            ;***************************************************

	    eaout0, header, dsfull, outval, procval, pecdata, itval, fpec

            outval.texapp = 0
            outval.texdef = outval.texdsn
            if outval.texrep ge 0 then outval.texrep = 0
            outval.texmes = 'Output written to file.'
    endif

    if outval.grpout eq 1 then begin

		;**** User explicit scaling ****

        grpscal = outval.grpscal
        xmin    = outval.xmin
        xmax    = outval.xmax
        xmin1   = outval.xmin1
        xmax1   = outval.xmax1
        xmin2   = outval.xmin2
        xmax2   = outval.xmax2	  	  
        ymin    = outval.ymin
        ymax    = outval.ymax
        zmin    = outval.zmin
        zmax    = outval.zmax
		
                ;**** Hardcopy output ****

        hrdout = outval.hrdout
        hardname = outval.hardname
        if outval.devsel ge 0 then device = devcode(outval.devsel)

		;**** optional user title/comment ****

        utitle = outval.gtit1
	ldsel  = procval.ldsel
	titlr  = procval.title
        
        eaoutg, fpec, pecdata, procval, dsfull, utitle, title, date, grpscal, ldsel, $
	   	xmin, xmax, xmin1, xmax1, xmin2, xmax2, ymin, ymax, zmin, zmax,     $
		hrdout, hardname, device, header,$
		bitfile, gomenu, FONT=font_large

	procval.ldsel  =ldsel
        outval.xmin    = strtrim(string(xmin),2)
        outval.xmax    = strtrim(string(xmax),2)
        outval.xmin1   = strtrim(string(xmin1),2)
        outval.xmax1   = strtrim(string(xmax1),2)
	outval.xmin2   = strtrim(string(xmin2),2)
        outval.xmax2   = strtrim(string(xmax2),2)    
        outval.ymin    = strtrim(string(ymin),2)
        outval.ymax    = strtrim(string(ymax),2) 
        outval.zmin    = strtrim(string(zmin),2)
        outval.zmax    = strtrim(string(zmax),2)
	outval.grpscal = grpscal
                
                ;**** If menu button clicked, stop ****

    endif

    if gomenu eq 1 then begin
        outval.texmes = ''
        goto, LABELEND
    endif 
		;**** Back for more output options ****

    GOTO, LABEL300

LABELEND:

            ;*********************************************
	    ;**** Ensure appending is not enabled for ****
	    ;**** text output at start of next run.   ****
            ;*********************************************

    outval.texapp = -1
    

		;**** Save user defaults ****

    save, inval, procval, outval, filename=deffile

END
