; Copyright (c) 2002, Strathclyde University.
; SCCS Info : Module @(#)$Header: /home/adascvs/idl/adas5xx/adas510/cw_adas510_out.pro,v 1.1 2004/07/06 12:53:48 whitefor Exp $  Date $Date: 2004/07/06 12:53:48 $
;+
; PROJECT:
;       ADAS IBM MVS to UNIX conversion
;
; NAME:
;	CW_ADAS510_OUT()
;
; PURPOSE:
;	Produces a widget for ADAS510 output options.
;
; EXPLANATION:
;	This function declares a compound widget consisting of the
;	graphical output selection widget cw_adas_gr_sel.pro, and an 
;	output file widget cw_adas_outfile.pro.  The text output
;	file specified in this widget is for tabular (paper.txt)
;	output.  This widget also includes a button for browsing the comments
;       from the input dataset, a 'Cancel' button , a 'Done' button
;       and an 'Escape to series menu' button. This latter is represented by
;       a bitmapped button.
;	The compound widgets cw_adas_gr_sel.pro and cw_adas_outfile.pro
;	included in this file are self managing.  This widget only
;       handles events from the 'Done', 'Cancel' and 'Escape to series
;       menu' buttons.
;
;
; USE:
;	This widget is specific to ADAS510, see adas510_out.pro	for use.
;
; INPUTS:
;	PARENT	- Long integer; ID of parent widget.
;
;	DSFULL	- Name of input dataset for this application.
;
;       BITFILE - String; the path to the dirctory containing bitmaps
;                 for the 'escape to series menu' button.
;
; OPTIONAL INPUTS:
;	None.
;
; OUTPUTS:
;	The return value of this function is the ID of the declared widget.
;
; OPTIONAL OUTPUTS:
;	None.
;
; KEYWORD PARAMETERS:
;	DEVLIST	- A string array;  Each element of the array is the
;		  name of a hardcopy graphics output device.  This
;		  list must mirror the DEVCODE list.  DEVLIST is the
;		  list which the user will see.
;		  e.g ['Post-Script','HP-PCL','HP-GL']
;
;	VALUE	- A structure which determines the initial settings of
;		  the output options widget.  The structure is made
;		  up of two parts.  Each part is the same as the value
;		  structure of one of the two main compound widgets
;		  included in this widget.  See cw_adas_gr_sel and
;		  cw_adas_outfile for more details.  The default value is;
;
;		      { GRPOUT:0, GTIT1:'', 				$
;			GRPSCAL:0, 					$
;			XMIN:'',  XMAX:'',   				$
;			YMIN:'',  YMAX:'',   				$
;			HRDOUT:0, HARDNAME:'', 				$
;			GRPDEF:'', GRPFMESS:'', 			$
;			GRPSEL:-1, GRPRMESS:'', 			$
;			DEVSEL:-1, GRSELMESS:'', 			$
;			TEXOUT:0, TEXAPP:-1, 				$
;			TEXREP:0, TEXDSN:'', 				$
;			TEXDEF:'',TEXMES:'', 				$
;		      }
;
;		  For CW_ADAS_GR_SEL;
;			GRPOUT	   Integer; Activation button 1 on, 0 off
;			GTIT1	   String; Graph title
;			GRPSCAL	   Integer; Scaling activation 1 on, 0 off
;			XMIN	   String; x-axis minimum, string of number
;			XMAX	   String; x-axis maximum, string of number
;			YMIN	   String; y-axis minimum, string of number
;			YMAX	   String; y-axis maximum, string of number
;			HRDOUT	   Integer; Hard copy activ' 1 on, 0 off
;			HARDNAME   String; Hard copy output file name
;			GRPDEF	   String; Default output file name 
;			GRPFMESS   String; File name error message
;			GRPSEL	   Integer; index of selected graph in GRPLIST
;			GRPRMESS   String; Scaling ranges error message
;			DEVSEL	   Integer; index of selected device in DEVLIST
;			GRSELMESS  String; General error message
;
;		  For CW_ADAS_OUTFILE tabular output (paper.txt);
;			TEXOUT	Integer; Activation button 1 on, 0 off
;			TEXAPP	Integer; Append button 1 on, 0 off, -1 no button
;			TEXREP	Integer; Replace button 1 on, 0 off, -1 no button
;			TEXDSN	String; Output file name
;			TEXDEF	String; Default file name
;			TEXMES	String; file name error message
;
;
;	UVALUE	- A user value for this widget.
;
;	FONT	- String; the name of a font to be used for all text in
;		  this widget.  Default to system default font.
;
; CALLS:
;	CW_ADAS_DSBR	Input dataset name and comments browse button.
;	CW_ADAS_GR_SEL	Graphical output selection widget.
;	CW_ADAS_OUTFILE	Output file name entry widget.
;	See side effects for other related widget management routines.
;
; SIDE EFFECTS:
;
; 	Two other routines are included in this file
;	OUT510_GET_VAL()
;	OUT510_EVENT()
;
; CATEGORY:
;	Compound Widget
;
; WRITTEN:
;       Richard Martin, University of Strathclyde, 14-06-02
;	  Based on cw_adas503_out.pro v1.5.
;
; MODIFIED:
;       1.1     Richard Martin
;               First release
;		
; VERSION:
;	1.1	14-06-02
;-
;-----------------------------------------------------------------------------

FUNCTION out510_get_val, id

                ;**** Return to caller on error ****

    ON_ERROR, 2

                ;**** Retrieve the state ****

    parent = widget_info(id, /parent)
    widget_control, parent, get_uvalue=state, /no_copy

		;**** Get graphical output settings ****

    widget_control, state.grpid, get_value=grselval

		;**** Get text output settings ****

    widget_control, state.paperid, get_value=papos
    os = { out510_set, 							$
	   GRPOUT	:	grselval.outbut, 			$
	   GTIT1	:	grselval.gtit1, 	 		$
	   GRPSCAL	:	grselval.scalbut, 			$
	   XMIN		:	grselval.xmin,     			$
	   XMAX		:	grselval.xmax,    	 		$
	   XMIN1		:	state.xmin1,     			$
	   XMAX1		:	state.xmax1,    	 		$
	   XMIN2		:	state.xmin2,     			$
	   XMAX2		:	state.xmax2,    	 		$	   	   
	   YMIN		:	grselval.ymin,     			$
	   YMAX		:	grselval.ymax,    	 		$
	   ZMIN		:	grselval.zmin,     			$
	   ZMAX		:	grselval.zmax,    	 		$	   
	   HRDOUT	:	grselval.hrdout, 			$
	   HARDNAME	:	grselval.hardname,   			$
	   GRPDEF	:	grselval.grpdef, 			$
	   GRPFMESS	:	grselval.grpfmess,   			$
	   GRPSEL	:	grselval.grpsel, 			$
	   GRPRMESS	:	grselval.grprmess,   			$
	   DEVSEL	:	grselval.devsel, 			$
	   GRSELMESS	:	grselval.grselmess, 			$
	   TEXOUT	:	papos.outbut,  				$
	   TEXAPP	:	papos.appbut, 		 		$
	   TEXREP	:	papos.repbut,  				$
	   TEXDSN	:	papos.filename, 	 		$
	   TEXDEF	:	papos.defname, 				$
	   TEXMES	:	papos.message 		 		}

                ;**** Return the state ****

    widget_control, parent, set_uvalue=state, /no_copy

    RETURN, os 

END

;-----------------------------------------------------------------------------

FUNCTION out510_event, event

                ;**** Base ID of compound widget ****

    parent = event.top

                ;**** Retrieve the state ****

    widget_control, parent, get_uvalue=state, /no_copy

		;*********************************
		;**** Clear previous messages ****
		;*********************************

    widget_control, state.messid, set_value=' '

                ;************************
                ;**** Process events ****
                ;************************

    CASE event.id OF

		;***********************
		;**** Cancel button ****
		;***********************

    state.cancelid: new_event = {ID:parent, TOP:event.top, 		$
				 HANDLER:0L, ACTION:'Cancel'}

		;*********************
		;**** Done button ****
		;*********************

        state.doneid: begin

                ;****************************************************
                ;**** Return the state before checking can start ****
		;**** with the get_value keyword.                ****
                ;****************************************************

            widget_control, parent, set_uvalue=state, /no_copy

		;***************************************
		;**** Check for errors in the input ****
		;***************************************

	    error = 0

		;**** Get a copy of the widget value ****

	    widget_control, event.handler, get_value=os
	
		;**** Check for widget error messages ****

	    if os.grpout eq 1 and strtrim(os.grpfmess) ne '' then error=1
	    if os.grpout eq 1 and os.grpscal eq 1 and 			$
	    strtrim(os.grprmess) ne '' then error=1
 	    if os.grpout eq 1 and strtrim(os.grselmess) ne '' then error=1
 	    if os.texout eq 1 and strtrim(os.texmes) ne '' then error=1

                ;**** Retrieve the state   ****

            widget_control, parent, get_uvalue=state, /no_copy
	    if error eq 1 then begin
	        widget_control, state.messid, set_value= 		$
		    '**** Error in output settings ****'
	        new_event = 0L
	    endif else begin
	        new_event = {ID:parent, TOP:event.top, HANDLER:0L, 	$
                             ACTION:'Done'}
	    endelse
        end

                ;**** Menu button ****

        state.outid: begin
            new_event = {ID:parent, TOP:event.top, HANDLER:0L,          $
                         ACTION:'Menu'}
        end

        state.fileid: begin
            new_event = {ID:parent, TOP:event.top, HANDLER:0L,          $
                         ACTION:'File'}
        end        

        ELSE: new_event = 0L

    ENDCASE

                ;**** Return the state   ****

    widget_control, parent, set_uvalue=state, /no_copy

    RETURN, new_event

END

;-----------------------------------------------------------------------------

FUNCTION cw_adas510_out, parent, dsfull, bitfile, DEVLIST=devlist, 	$
		         VALUE=value, UVALUE=uvalue, FONT=font

    IF (N_PARAMS() EQ 0) THEN MESSAGE, 'Must specify parent for cw_adas510_out'
    ON_ERROR, 2					;return to caller

		;**** Set defaults for keywords ****

    IF NOT (KEYWORD_SET(devlist)) THEN devlist = ''
    IF NOT (KEYWORD_SET(uvalue)) THEN uvalue = 0
    IF NOT (KEYWORD_SET(font)) THEN font = ''
    IF NOT (KEYWORD_SET(value)) THEN begin
	os = {	out510_set, 						$
		GRPOUT		:	0, 				$
		GTIT1		:	'', 				$
		GRPSCAL		:	0, 				$
		XMIN		:	'',  				$
		XMAX		:	'',   				$
		XMIN1		:	'',  				$
		XMAX1		:	'',   				$
		XMIN2		:	'',  				$
		XMAX2		:	'',   				$
		YMIN		:	'',  				$
		YMAX		:	'',   				$
		ZMIN		:	'',  				$
		ZMAX		:	'',   				$		
		HRDOUT		:	0, 				$
		HARDNAME	:	'', 				$
		GRPDEF		:	'', 				$
		GRPFMESS	:	'', 				$
		GRPSEL		:	-1, 				$
		GRPRMESS	:	'', 				$
		DEVSEL		:	-1, 				$
		GRSELMESS	:	'', 				$
		TEXOUT		:	0, 				$
		TEXAPP		:	-1, 				$
		TEXREP		:	0, 				$
		TEXDSN		:	'', 				$
                TEXDEF		:	'',				$
		TEXMES		:	'' 				}
    ENDIF ELSE BEGIN
	os = {	out510_set, 						$
		GRPOUT		:	value.grpout, 			$
		GTIT1		:	value.gtit1, 			$
		GRPSCAL		:	value.grpscal, 			$
		XMIN		:	value.xmin,     		$
		XMAX		:	value.xmax,   			$
		XMIN1		:	value.xmin1,     		$
		XMAX1		:	value.xmax1,   			$		
		XMIN2		:	value.xmin2,     		$
		XMAX2		:	value.xmax2,   			$	
		YMIN		:	value.ymin,     		$
		YMAX		:	value.ymax,   			$
		ZMIN		:	value.zmin,     		$
		ZMAX		:	value.zmax,   			$		
		HRDOUT		:	value.hrdout, 			$
		HARDNAME	:	value.hardname, 		$
		GRPDEF		:	value.grpdef, 			$
		GRPFMESS	:	value.grpfmess, 		$
		GRPSEL		:	value.grpsel, 			$
		GRPRMESS	:	value.grprmess, 		$
		DEVSEL		:	value.devsel, 			$
		GRSELMESS	:	value.grselmess, 		$
		TEXOUT		:	value.texout, 			$
		TEXAPP		:	value.texapp, 			$
		TEXREP		:	value.texrep, 			$
		TEXDSN		:	value.texdsn, 			$
		TEXDEF		:	value.texdef, 			$
		TEXMES		:	value.texmes 			}
    ENDELSE

		;**********************************************
		;**** Create the 510 Output options widget ****
		;**********************************************

		;**** create base widget ****

    cwid = widget_base( parent, UVALUE = uvalue, 			$
			EVENT_FUNC = "out510_event", 			$
			FUNC_GET_VALUE = "out510_get_val", 		$
			/COLUMN)

		;**** Add dataset name and browse button ****

    rc = cw_adas_dsbr(cwid, dsfull, font=font)

                ;*****************************************************
		;**** Widget for graphics selection               ****
		;**** Note change in names for GRPOUT and GRPSCAL ****
                ;*****************************************************

    grselval = {  	OUTBUT		:	os.grpout, 		$
			GTIT1		:	os.gtit1, 		$
			SCALBUT		:	os.grpscal, 		$
			XMIN		:	os.xmin, 		$
			XMAX		:	os.xmax, 		$		
			YMIN		:	os.ymin, 		$
			YMAX		:	os.ymax, 		$
			ZMIN		:	os.zmin, 		$
			ZMAX		:	os.zmax, 		$			
			HRDOUT		:	os.hrdout, 		$
			HARDNAME	:	os.hardname, 		$
			GRPDEF		:	os.grpdef, 		$
			GRPFMESS	:	os.grpfmess, 		$
			GRPSEL		:	os.grpsel, 		$
			GRPRMESS	:	os.grprmess, 		$
			DEVSEL		:	os.devsel, 		$
			GRSELMESS	:	os.grselmess }
    base = widget_base(cwid, /row, /frame)
    grpid = cw_adas_gr_sel(base, /SIGN, zranges=1, OUTPUT='Graphical Output', 	$
			   DEVLIST=devlist, VALUE=grselval, FONT=font )

		;**** Widget for text output ****

    outfval = { OUTBUT:os.texout, APPBUT:os.texapp, REPBUT:os.texrep, 		$
	        FILENAME:os.texdsn, DEFNAME:os.texdef, MESSAGE:os.texmes }
    base = widget_base(cwid, /row, /frame)
    paperid = cw_adas_outfile(base, OUTPUT='Text Output', $
                              VALUE=outfval, FONT=font)

		;**** Error message ****

    messid = widget_label(cwid, value=' ', font=font)

		;**** add the exit buttons ****

    base = widget_base(cwid, /row)
    menufile = bitfile + '/menu.bmp'
    read_X11_bitmap, menufile, bitmap1
    outid = widget_button(base, value=bitmap1)          ;menu button
    fileid = widget_button(base, value='Return to Input File Selection', font=font)
    cancelid = widget_button(base, value='Cancel', font=font)  
    doneid = widget_button(base, value='Done', font=font)  
  
                ;*************************************************
		;**** create a state structure for the pop-up ****
		;**** window.                                 ****
                ;*************************************************

    new_state = { 	GRPID		:	grpid, 	$
			PAPERID		:	paperid, 	$
		  	CANCELID		:	cancelid,	$
			DONEID		:	doneid,	$
                  OUTID           	:     outid,	$
                  FILEID		:	fileid,	$
			MESSID		:	messid,	$
			XMIN1			:	os.xmin1,	$
			XMAX1			:	os.xmax1,	$	
			XMIN2			:	os.xmin2,	$
			XMAX2			:	os.xmax2 }

                ;**** Save initial state structure ****

    widget_control, parent, set_uvalue=new_state, /no_copy

    RETURN, cwid

END

