;+
; PROJECT:
;       ADAS
;
; NAME:
;	EAISPF
;
; PURPOSE:
;	Obtains input data via eadata.pro and invokes IDL user interface.
;
; EXPLANATION:
;	The routine first of all obtains data from the input data file
;	via the routine eadata. Then the processing widget interface is
;	invoked to determine how the user wishes to process the input dataset.  
;	When the user's interactions are complete the information gathered with
;	the user interface is stored in the PECDATA structure, and passed back
;	to adas510.pro.
;
; USE:
;	The use of this routine is specific to ADAS510, see adas510.pro.
;
; INPUTS:
;	LPEND	- Integer, 0 if the user pressed the 'Done' button
;		  or 1 if the user pressed 'Cancel'.
;
;	PROCVAL	- A structure which determines the initial settings of
;		  the processing options widget.  The initial value is
;		  set in adas510.pro.  If adas510.pro passes a blank 
;		  dummy structure of the form {NMET:-1} into PROCVAL then
;		  PROCVAL is reset to a default structure.
;
;		  The PROCVAL structure is;
;			procval = {                        	$
;                			nmet    : 0 ,           $
;                			title   : '',           $
;                			nbsel   : 0 ,           $
;                			ibsel   : 0 ,           $
;                			uwavemin: str_arr,	$
;					uwavemax: str_arr,	$       
;                			ufile   : str_arr,      $
;                			utype   : str_arr,      $
;                			uindm   : str_arr,      $
;                			ifout   : 1,            $
;					ldsel   : 0,            $
;					maxt    : 0,            $
;					maxd    : 0,            $
;                			tin     : temp_arr,     $
;                			din     : dens_arr,     $
;                			ifsel   : 0,            $
;					selbase : 1	}
;
;
;		  See cw_adas510_proc.pro for a full description of this
;		  structure.
;
;	PECDATA =  Initial PECDATA structure of form  { FPEC: 0.0 }	
;
;	DSFULL	- String; The full system file name of the input 
;		  dataset selected by the user for processing.
;
;       BITFILE - String; the path to the dirctory containing bitmaps
;                 for the 'escape to series menu' button.
;
; OPTIONAL INPUTS:
;	None
;
; OUTPUTS:
;	PROCVAL	- On output the structure records the final settings of
;		  the processing selections widget if the user pressed the
;		  'Done' button, otherwise it is not changed from input.
;
;     PECDATA:	 - Structure containing all the relevant data for 
;			   numerical processing by the FORTRAN subroutines.
;
;			  pecdata={ esym:esym,	      $
;     	   		            iz0:iz0,	      $
;		   		    iz:iz,  	      $
;		   		    iz1:iz1,	      $
;     	   		            nbsel:nbsel,      $
;     	   		            npix:npix,	      $
;		   		    ntdim:ntdim,      $
;		   		    nddim:nddim,      $
;		   		    pix:pix,	      $
;     	   		            ida:ida,	      $
;		   		    ita:ita,	      $
;		   		    teda:teda,	      $
;		   		    tvals:tvals,      $
;		   		    cwavemin:cwavemin,$
;		   		    cwavemax:cwavemax,$
;		   		    cfile:cfile,      $
;		   		    ctype:ctype,      $
;		   		    cindm:cindm,      $
;		   		    fpec:fpec         }
;
;		  ESYM:     element name.	
;     	          IZ0:      nuclear charge.
;		  IZ:       ion charge.
;		  IZ1:      ion charge+1.
;     	          NBSEL:    no of data blocks in the datafile.
;     	          NPIX:	    max no of pixels(wavelengths) in the datfile.
;		  NTDIM:    max no of temperatures in the datafile.
;		  NDDIM:    max no of densities in the datafile.
;		  PIX:	    array containing no of pixels (wavelengths) for each datablock
;     	          IDA:	    array containing no of densities for each datablock
;		  ITA:	    array containing no of temperatures for each datablock
;		  TEDA:	    array containing selected densities 
;		  TVALS:    array containing selected temperatures 
;		  CWAVEMIN: array containing minimum of the wavelength range for each datablock
;		  CWAVEMAX: array containing maximum of the wavelength range for each datablock
;		  CFILE:    array containing the specific ion source file for each datablock
;		  CTYPE:    array containing the data type code for each datablock
;		  CINDM:    array containing the metastable index for each datablock
;		  FPEC:	    envelope feature photon emissivity coefficients.
;
;       LPEND   - Integer, 0 if the user pressed the 'Done' button
;                 or 1 if the user pressed 'Cancel'.  Maps directly onto
;                 the logical variable LPEND in ADAS501 FORTRAN.
;
;       GOMENU  - Int; flag - set to 1 if user has selected 'escape direct
;                 to series menu , otherwise 0
;
; OPTIONAL OUTPUTS:
;	None
;
; KEYWORD PARAMETERS:
;	FONT_LARGE - The name of a larger font.
;
;	FONT_SMALL - The name of a smaller font.
;
;	EDIT_FONTS - A structure of two fonts used in the table editor
;		     adas_edtab.pro, {font_norm:'',font_input:''}.
;
; CALLS:
;	ADAS510_PROC Invoke the IDL interface for ADAS510 data
;		 	processing options/input.
;
; SIDE EFFECTS:
;	None.
;
; CATEGORY:
;	Adas system.
;
; WRITTEN:
;       Richard Martin, University of Strathclyde, 14-06-02
;
; MODIFIED:
;	1.1	Richard Martin
;                - First release		
;	1.2     Martin O'Mullane
;		 - Replaced eadata with read_adf40.
;
; VERSION:
;	1.1	14-06-2002
;	1.2     26-11-2004
;
;-
;-----------------------------------------------------------------------------

PRO eaispf, lpend, procval, pecdata, dsfull, gomenu, bitfile,		$
	    FONT_LARGE=font_large, FONT_SMALL=font_small, 		$
	    EDIT_FONTS=edit_fonts


                ;**** Set defaults for keywords ****

    IF NOT (KEYWORD_SET(font_large)) THEN font_large = ''
    IF NOT (KEYWORD_SET(font_small)) THEN font_small = ''
    IF NOT (KEYWORD_SET(edit_fonts)) THEN 				$
    edit_fonts = {font_norm:'',font_input:''}

		;********************************************
		;****     Declare variables for input    ****
		;**** arrays will be declared after sizes****
		;**** have been read.                    ****
                ;********************************************

    lpend = 0
    nstore= 500
    ntdim = 0
    nddim = 0
    ndtin = 0
    nbsel = 0
    input = 0

		;********************************************
		;****     Declare variables for input    ****
		;**** arrays will be declared after sizes****
		;**** have been read.                    ****
                ;********************************************

    read_adf40, file=dsfull, fulldata=all
    
		;******************************************
		;**** Now can define other array sizes ****
		;******************************************

    nbsel    = n_elements(all.wmin)
    esym     = all.esym
    iz0      = all.iz0 
    iz       = all.iz  
    iz1      = all.iz1 
    cwavemin = fltarr(nbsel)
    cwavemax = fltarr(nbsel)
    cfile    = strarr(nbsel)
    ctype    = strarr(nbsel)
    cindm    = strarr(nbsel)
    ita      = intarr(nbsel)
    ida      = intarr(nbsel)
    pix      = intarr(nbsel)

    
    for i=0,nbsel-1 do begin
   	  cwavemin[i] = all.wmin[i]  
   	  cwavemax[i] = all.wmax[i]  
	  cfile[i]    = ' '          
	  ctype[i]    = all.ctype[i] 
	  cindm[i]    = all.cindm[i] 
	  ita[i]      = all.nte[i]   
	  ida[i]      = all.ndens[i] 
	  pix[i]      = all.npix[i]  
    endfor       

    ntmax  = max(ita(*))
    ndmax  = max(ida(*))
    ndtin  = max([ntmax,ndmax])
    ntdim  = ndtin
    nddim  = ndtin
    nmet   = max(cindm(*))
    npix   = max(pix(*))
    
    tvals  = dblarr(ntdim, nbsel)
    teda   = dblarr(nddim, nbsel)
    fpec   = dblarr(ntdim,nddim,npix,nbsel)
    
    for k = 0, nbsel-1 do begin 
	 for i = 0, ita[k]-1 do begin
                tvals[i,k] = all.te[i,k]
	 endfor
    endfor
    for k = 0, nbsel-1 do begin 
        for i = 0, ida[k]-1 do begin
            teda[i,k] = all.dens[i,k]
        endfor 
    endfor

    for l = 0, nbsel-1 do begin 
      for k = 0, pix[l]-1 do begin 
        for j = 0, ida[l]-1 do begin
	   for i = 0, ita[l]-1 do begin
              fpec[i,j,k,l] = all.fpec[k,i,j,l]
	   endfor
        endfor 
      endfor   
    endfor   
    
		;*******************************************
		;**** Set default value if non provided ****
		;*******************************************

    if (procval.nmet lt 0) then begin
         str_arr = strarr(nbsel)
         temp_arr = fltarr(ntdim)
         dens_arr = fltarr(nddim)
         procval = {	nmet  	   :    0 ,             $
			title 	   :    '',             $
			nbsel 	   :    0 ,             $
			ibsel 	   :    0 ,             $
			uwavemin   :    str_arr,        $
			uwavemax   :    str_arr,        $                       
			ufile 	   :    str_arr,        $
			utype 	   :    str_arr,        $
			uindm 	   :    str_arr,        $
                	ifout 	   :    1,              $
			ldsel 	   :    0,              $
			maxt  	   :    0,              $
                	maxd  	   :    0,              $
			tin   	   :    temp_arr,       $
			din   	   :    dens_arr,       $
			selbase	   :    1               }
    endif
    
    procval.nbsel=nbsel			; make sure nbsel is upated every time/.

		;****************************************
		;**** Pop-up processing input widget ****
		;****************************************

    adas510_proc, procval, dsfull, nstore, 				$
		  ntdim, nddim, ndtin, nbsel, 				$
		  cwavemin, cwavemax, cfile, ctype , cindm, 		$
		  ita, ida, tvals, teda, fpec, action, bitfile,		$
		  FONT_LARGE=font_large, FONT_SMALL=font_small, 	$
		  EDIT_FONTS=edit_fonts

		;****************************************
		;***** Write data to data structure *****
		;****************************************

    pecdata = {	esym     :  esym,       $
    		iz0      :  iz0,        $
		iz       :  iz,         $
		iz1      :  iz1,        $
    		nbsel    :  nbsel,      $
    		npix     :  npix,       $
		ntdim    :  ntdim,      $
		nddim    :  nddim,      $
		pix      :  pix,        $
    		ida      :  ida,        $
		ita      :  ita,        $
		teda     :  teda,       $
		tvals    :  tvals,      $
		cwavemin :  cwavemin,   $
		cwavemax :  cwavemax,   $
		cfile    :  cfile,      $
		ctype    :  ctype,      $
		cindm    :  cindm,      $
		fpec     :  fpec        }
					
		;********************************************
		;****  Act on the event from the widget  ****
                ;**** There are three  possible  actions ****
                ;**** 'Done', 'Cancel'and 'Menu'.        ****
		;********************************************

    if action eq 'Done' then begin
        lpend = 0
    endif else if action eq 'Menu' then begin
        lpend = 0
        gomenu = 1
    endif else begin
        lpend = 1
    endelse


END
