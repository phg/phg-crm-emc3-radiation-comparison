; Copyright (c) 2002, Strathclyde University .
; SCCS info: Module @(#)$Header: /home/adascvs/idl/adas5xx/adas510/adas510_plot.pro,v 1.1 2004/07/06 11:01:09 whitefor Exp $	Date $Date: 2004/07/06 11:01:09 $
;+
; PROJECT:
;       ADAS IBM MVS to UNIX conversion
;
; NAME:
;	ADAS510_PLOT
;
; PURPOSE:
;	Generates ADAS510 graphical output.
;
; EXPLANATION:
;	This routine creates a window for the display of graphical
;	output a separate routine adasplot_surface actually plots a
;	surface plot.
;
; USE:
;	This routine is specific to ADAS510, see eaoutg.pro for
;	example use.
;
; INPUTS:
;	(Most of these inputs map exactly onto variables of the same
;	 name in the FORTRAN code.)
;
;
;	DSFULL  - String; Name of data file 
;
;	TITLE   - String array; titles to be placed above graph
;
;	TITLX   - String; user supplied comment appended to end of title
;
;	TITLM   - String; Information about minimax fitting if selected.
;
;	UTITLE  - String; Optional comment by user
;
;	DATE	- String; Date of graph production
;
;	ESYM 	- String; Symbol of emitting ion
;
;	IZ0	- Integer; Nuclear charge of emitting ion
;
;	IZ	- Integer; Charge of emitting ion
;
;	CWAVEMIN - String; Minimum wavelength of data block used
;
;	CWAVEMAX - String; Maximum wavelength of data block used	
;
;	CFILE	- String; Specific ion file source
;
;	CTYPE  	- String; processing code from input data file
;
;	CINDM	- String; metastable index
;
;	ITVAL	- Integer; number of user entered temperature/density pairs
;
;	NPIX  - No of pixels (wavelength values) in PECA array.
;
;	TEVA	- Double array; User entered electron temperatures, eV
;
;	DIN	- Double array; User entered electron densities, cm-3
;
;	PECA	- Double array; Spline interpolated or extrapolated 
;				envelope feature photon emissivity coefficients 
;				for user entered temp. and density pairs.
;
;	GRPSCAL	- Integer; 0 - use user entered graph scales
;			   1 - use default axes scaling
;
;	LDSEL   - 0 - display temperature on x-axis
;		    1 - display density on x-axis
;
;	XMIN    - String; Lower limit for x-axis of graph, number as string.
;
;	XMAX    - String; Upper limit for x-axis of graph, number as string.
;
;	YMIN    - String; Lower limit for y-axis of graph, number as string.
;
;	YMAX    - String; Upper limit for y-axis of graph, number as string.
;
;	ZMIN    - String; Lower limit for z-axis of graph, number as string.
;
;	ZMAX    - String; Upper limit for z-axis of graph, number as string.
;
;	STRG	- String array; Information regarding current data selected
;
;	HEAD1	- String; header information for data source
;
;	HEAD2A	- String; Header information for temperature/density 
;
;	HEAD2B  - String; column titles for temp/density pair information
;
;	HRDOUT - Integer; 1 if hardcopy output activated, 0 if not.
;
;	HARDNAME- String; Filename for harcopy output.
;
;	DEVICE	- String; IDL name of hardcopy output device.
;
;	HEADER	- String; ADAS version number header to include in graph.
;
;     BITFILE 	- String; the path to the directory containing bitmaps
;                   for the 'escape to series menu' button.
;
; OPTIONAL INPUTS:
;	None.
;
; OUTPUTS:
;       GOMENU  - Int; flag - set to 1 if user has selected 'escape direct
;                 to series menu', otherwise 0
;
; OPTIONAL OUTPUTS:
;	None.
;
; KEYWORD PARAMETERS:
;	FONT	- String; The name of a font to use for text in the
;		  graphical output widget.
;
; CALLS:
;	CW_ADAS_GRAPH	Graphical output widget.
;	ADASPLOT_SURFACE	Make one plot to an output device for 510.
;	XMANAGER
;
; SIDE EFFECTS:
;	This routine uses a common block to maintain its state PLOT510_BLK.
;
;	One other routine is included in this file;
;	ADAS510_PLOT_EV	Called via XMANAGER during widget management.
;
; CATEGORY:
;	Adas system.
;
; WRITTEN:
;       Richard Martin, University of Strathclyde,  14-06-2002.
;	  Based on adas503_plot v1.9.
;
; MODIFIED:
;       1.1     Richard Martin
;               First release
;
; VERSION:
;	1.1	14-06-02
;
;-
;----------------------------------------------------------------------------

PRO adas510_plot_ev, event

    COMMON plot510_blk, data, action, win, plotdev, plotfile, 		$
                        fileopen, gomenu

    newplot = 0
    print = 0
    done = 0

		;****************************************
		;**** Set graph and device requested ****
		;****************************************

    CASE event.action OF

	'print': begin
	    newplot = 1
	    print = 1
        end

	'done': begin
	    if fileopen eq 1 then begin
	        set_plot, plotdev
	        device, /close_file
	    endif
	    set_plot, 'X'
	    widget_control, event.top, /destroy
	    done = 1
        end

        'bitbutton' : begin
            if fileopen eq 1 then begin
                set_plot, plotdev
                device, /close_file
            endif
            set_plot,'X'
            widget_control, event.top, /destroy
            done = 1
            gomenu = 1
        end

	'adjust': begin
		adjustid=cw_adjust_surface(data,font=data.font)
		data=adjustid.data
        end     
       
     
        ELSE:   ;**** Do nothing ****
              
    ENDCASE

		;*******************************
		;**** Make requested plot/s ****
		;*******************************

    if done eq 0 then begin

		;**** Set graphics device ****

        if print eq 1 then begin
            set_plot, plotdev
            if fileopen eq 0 then begin
                fileopen = 1
                device, filename=plotfile
	        device, /landscape
            endif
        endif else begin
            set_plot,'X'
            wset,win
        endelse

		;**** Draw graphics ****

        adasplot_surface, data.x , data.y, data.peca, data.itval, 	$
	         data.title,data.xtitle,data.ytitle, data.strg, 		$
		   data.head1, data.head2a, data.head2b, data.teva, 		$
		   data.din, data.grpscal, data.xmin, data.xmax, data.ymin, $
		   data.ymax, data.zmin, data.zmax, xlog=data.xlog, 		$
		   ylog=data.ylog, zlog=data.zlog, ax=data.ax, az=data.az, 	$
		   scolour=data.colour, surface=data.surface
		   
	if print eq 1 then begin
	    message = 'Plot  written to print file.'
	    grval = {WIN:0, MESSAGE:message}
	    widget_control, event.id, set_value=grval
	endif
    endif
    
    if print eq 1 then begin
	      device, /close 
            set_plot,'X'               
    endif
END

;----------------------------------------------------------------------------

PRO adas510_plot, dsfull, title , titlx, utitle, date, esym,    		$
			iz0, iz, cwavemin, cwavemax, cfile, ctype , cindm, 	$
			itval, npix, teva, din, peca, grpscal, ldsel, 		$
			xmin, xmax, xmin1, xmax1, xmin2, xmax2, 			$
			ymin, ymax, zmin, zmax, strg, head1, 			$
			head2a, head2b, hrdout, hardname, device, header, 	$
			bitfile, gomenu, FONT=font

    COMMON plot510_blk, data, action, win, plotdev, plotfile, 		$
                        fileopen, gomenucom

		;**** Copy input values to common ****

    plotdev = device
    plotfile = hardname
    fileopen = 0
    gomenucom = gomenu

		;************************************
		;**** Create general graph title ****
		;************************************

    title = strarr(6)
    maintitle=strarr(2)
    type = 'ELECTRON TEMPERATURE '
    maintitle(0) = "FEATURE PHOTON EMISSIVITY COEFFICIENT VS " + type

    type = 'ELECTRON DENSITY '
    maintitle(1) = "FEATURE PHOTON EMISSIVITY COEFFICIENT VS " + type

    if ( strtrim(strcompress(utitle),2)  ne '' ) then begin
        maintitle(0) = maintitle(0) + ': ' + strupcase(strtrim(utitle,2))        
        maintitle(1) = maintitle(1) + ': ' + strupcase(strtrim(utitle,2))        
    endif

    title(1) = 'ADAS    :' + header
    title(2) = 'FILE     :' + titlx


		;********************************
		;*** Create graph annotation ****
		;********************************        

    strg = strtrim(strg, 2)
    strg(0) = strg(0) +  ' ' + esym
    strg(1) = strg(1) +  strcompress(string(iz0)) 
    strg(2) = strg(2) +  strcompress(string(iz))
    uwavemin = string(cwavemin,format='(F6.1)')	
    uwavemax = string(cwavemax,format='(F6.1)')        
    strg(3) = strg(3) +  strcompress(uwavemin + ' - ' + uwavemax)
    strg(4) = strg(4) +  ' ' + cfile
    strg(5) = strg(5) +  ' ' + ctype 
    strg(6) = strg(6) +  ' ' + cindm

		;*********************************
		;**** Set up Y data for plots ****
		;*********************************

    ydim=npix
    ydum = findgen(ydim)
    y = cwavemin + (cwavemax-cwavemin)/float(ydim)*ydum   
    ytitle = "WAVELENGTH (A)"

		;**************************************
		;**** Set up x axis and xaxis title ***
		;**************************************

    xdim = itval
    x1 = fltarr( xdim)    
    x2 = fltarr( xdim)      

            ;****************************************************
		;*** write x axis array depending on whether temp ***
		;*** or density values used 			  	  ***
            ;****************************************************

    xtitles=strarr(2)
    xunits=strarr(2)
    xtitles(0) = "ELECTRON TEMPERATURE (eV)"
    xtitles(1) = "ELECTRON DENSITY (cm-3)"	   

    if ldsel eq 0 then begin
    	x1(0:itval-1) = teva(0:itval-1)
    	x2(0:itval-1) = din(0:itval-1)
     	x=x1
      xunit  = 0
      xtitle = xtitles(0) 
      title(0)=maintitle(0)
	xmin1=xmin 
	xmax1=xmax      	       
    endif else begin
    	x2(0:itval-1) = din(0:itval-1)
    	x1(0:itval-1) = teva(0:itval-1)
     	x=x2
      xunit  = 1
      xtitle = xtitles(1) 
      title(0)=maintitle(1)    
	xmin2=xmin 
	xmax2=xmax 
    endelse
    
  		;******************************************
		;*** if desired set up user axis scales ***
		;******************************************

    if (grpscal eq 0) then begin
	  xmin1 = min(x1, max = xmax1)
	  xmin2 = min(x2, max = xmax2)	  
	  xmax1 = xmax1 * 1.1
	  xmax2 = xmax2 * 1.1	
 
;	  ymin = min( y, max = ymax)
;	  ymin = ymin * 0.9
;	  ymax = ymax *1.1
	  zmin = min( peca, max = zmax)
	  zmin = zmin * 0.9
	  zmax = zmax *1.1	  
    endif else begin
     	  if ldsel eq 0 then begin   	
		xmin=xmin1 
		xmax=xmax1     
;	  	xmin2 = min(x2, max = xmax2)		
;		xmax2 = xmax2 * 1.1	 	  
        endif else begin	
		xmin=xmin2 
		xmax=xmax2    
;	      xmin1 = min(x1, max = xmax1)		   
;		xmax1 = xmax1 * 1.1    
	  endelse
	  
    endelse

		;*************************************
		;**** Create graph display widget ****
		;*************************************

    graphid = widget_base(TITLE='ADAS510 GRAPHICAL OUTPUT', 		$
			  XOFFSET=1, YOFFSET=1)
    device, get_screen_size=scrsz
    xwidth = scrsz(0)*0.75
    yheight = scrsz(1)*0.75
    bitval = bitfile + '/menu.bmp'

    adjust=1
    retain=0
    cwid = cw_adas_graph(graphid, print=hrdout, FONT=font,              $
                         xsize=xwidth, ysize=yheight,                   $
                         bitbutton=bitval,		$
                         adjust=adjust,retain=retain)

                ;**** Realize the new widget ****

    dynlabel, graphid
    widget_control, graphid, /realize

		;**** Get the id of the graphics area ****

    widget_control, cwid, get_value=grval
    win = grval.win

    wset, win

    	adasplot_surface, x, y, peca, itval, title, xtitle, 		$
		 ytitle, strg, head1, head2a, head2b, 	$
		 teva, din,  grpscal, xmin, xmax, ymin, ymax, 		$
		 zmin, zmax, ax=45, az=45,  xlog=1, ylog=1    
    
		;*******************************************
		;**** Put the graphing data into common ****
		;*******************************************

    data = {	Y		:	y,			$
    		X		:	x,		$
		PECA		:	peca,		$
		X1		:	x1,   	$
		X2		:	x2,   	$		
		ITVAL		:	itval,    	$
		NPIX		:	npix,		$
            TITLE		:	title,   	$
            MAINTITLE   :	maintitle,	$
            XTITLE	:	xtitle,	$
		XTITLES	:	xtitles,  	$
		YTITLE	:	ytitle,	$	
		STRG		:	strg,     	$
		HEAD1		:	head1,    	$
		HEAD2A	:	head2a,     $
            HEAD2B	:	head2b, 	$
		TEVA		:	teva,      	$
		DIN		:	din,     	$
		GRPSCAL	:	grpscal, 	$		
		LDEF1		:	grpscal, 	$
		LDEF2		:	0, 		$
		XMIN		:	xmin,		$
		XMAX		:	xmax,		$		
		XMIN1		:	xmin1,	$
		XMAX1		:	xmax1,	$
		XMIN2		:	xmin2,	$
		XMAX2		:	xmax2,	$		
		YMIN		:	ymin,		$
		YMAX		:	ymax,		$
		ZMIN		:	zmin,		$
		ZMAX		:	zmax,		$		
		XLOG		: 	1,		$
		YLOG		: 	1,		$
		ZLOG		: 	0,		$		
		AX		:	45,		$
		AZ		:	45,		$
		LDSEL		:	ldsel,		$
		SURFACE	:	0,		$
		COLOUR	:	0,		$		
		FONT		: 	font 	}

		;***************************
                ;**** make widget modal ****
		;***************************

    xmanager, 'adas510_plot', graphid, event_handler='adas510_plot_ev', $
              /modal,/just_reg

    xmin    = data.xmin
    xmax    = data.xmax
    xmin1   = data.xmin1
    xmax1   = data.xmax1
    xmin2   = data.xmin2
    xmax2   = data.xmax2	  
    ymin    = data.ymin
    ymax    = data.ymax
    zmin    = data.zmin
    zmax    = data.zmax
     
    ldsel = data.ldsel 
    grpscal = data.grpscal
    
    
    gomenu = gomenucom    






END
