; Copyright (c) 2002, Strathclyde University 
; SCCS INFO : Module @(#)$Header: /home/adascvs/idl/adas5xx/adas510/eaout0.pro,v 1.1 2004/07/06 13:47:41 whitefor Exp $ Date $Date: 2004/07/06 13:47:41 $
;+
;
; NAME:
;	EAOUT0
;
; PURPOSE:
;	Write output text file.
;
; EXPLANATION:
;	The routine first of all obtains data from the input data file
;	via the routine eadata. Then the processing widget interface is
;	invoked to determine how the user wishes to process the input dataset.  
;	When the user's interactions are complete the information gathered with
;	the user interface is stored in the PECDATA structure, and passed back
;	to adas510.pro.
;
; USE:
;	The use of this routine is specific to ADAS510, see adas510.pro.
;
; INPUTS:
;	header:	Header string containing program name, date and time.
;	dsfull:	String containing location and name of output text file.
;	outval:	Structure containing output options.
;	procval:	Structure containing processing options - see eaispf.pro
;	pecdata:	Structure contain input and processed data - see eaispf.pro
;	itval:	Number of selected temperature-density pairs
;	fpec:		Array containing the feature envelope photon emissivity coefficients.
;
; OPTIONAL INPUTS:
;	None
;
; OUTPUTS:
;	None
;
; OPTIONAL OUTPUTS:
;	None
;
; KEYWORD PARAMETERS:
;	None
;
; CALLS:
;	None
;
; SIDE EFFECTS:
;	Creates an output text file, the name and location of which is stored 
;	in the string dsfull.
;
; CATEGORY:
;	Adas system.
;
; WRITTEN:
;       Richard Martin, University of Strathclyde, 14-06-02
;
; MODIFIED:
;	1.1	Richard Martin
;           First release		
;
; VERSION:
;	1.1	14-06-2002
;
;-
;-----------------------------------------------------------------------------


PRO eaout0, header, dsfull, outval, procval, pecdata, itval, fpec

xxelem, pecdata.iz0, ename

if outval.texapp eq 1 then begin
	openu,unit,outval.texdsn,/append,/get_lun
endif else begin
	openw,unit,outval.texdsn,/get_lun
endelse

printf,unit,header
printf,unit,' ******************** TABULAR OUTPUT FROM FEATURE PHOTON EMISSIVITY ' $
	+'INTERROGATION PROGRAM ********************'
printf,unit,''
printf,unit,' -----------------------------------------------------------------------------'
printf,unit,' FEATURE PHOTON EMISSIVITIES AS A FUNCTION OF ELECTRON TEMPERATURE AND DENSITY'
printf,unit,' -----------------------------------------------------------------------------'
printf,unit,'                    DATA GENERATED USING PROGRAM: ADAS510'	
printf,unit,'                    -------------------------------------'
printf,unit,' FILE : '+ dsfull
printf,unit,''
printf,unit,' EMITTING ION INFORMATION:'
printf,unit,' -------------------------'
printf,unit,'ELEMENT NAME','=',ename,format='(1x,a12,18x,a1,1x,a12)'
printf,unit,'ELEMENT SYMBOL','=',pecdata.esym,format='(1x,a14,16x,a1,1x,a3)'
printf,unit,'NUCLEAR CHARGE (Z0)','=',strtrim(string(pecdata.iz0),2),format='(1x,a19,11x,a1,1x,I3)'
printf,unit,'CHARGE STATE (Z)','=',strtrim(string(pecdata.iz),2),format='(1x,a16,14x,a1,1x,I3)'
uwavemin = strtrim(string(pecdata.cwavemin(procval.ibsel),format='(F6.1)'),2)	
uwavemax = strtrim(string(pecdata.cwavemax(procval.ibsel),format='(F6.1)'),2)    
waverng = strcompress(uwavemin + ' - ' + uwavemax)
printf,unit,'WAVELENGTH RANGE (angstroms)','=',waverng ,format='(1x,a28,2x,a1,1x,a15)'
printf,unit,'SPECIFIC ION FILE SOURCE','=',strtrim(pecdata.cfile(procval.ibsel),2) ,format='(1x,a24,6x,a1,1x,a8)'
printf,unit,'DATA TYPE  CODE','=',strtrim(pecdata.ctype(procval.ibsel),2) ,format='(1x,a15,15x,a1,1x,a8)'
printf,unit,'METASTABLE INDEX','=',strtrim(pecdata.cindm(procval.ibsel),2) ,format='(1x,a16,14x,a1,1x,a2)'
printf,unit,'' 
printf,unit,' --- ELECTRON TEMPERATURE --- ELECTRON DENSITY'
printf,unit,'        eV                       cm-3'
	  
for i=0,itval-1 do begin
	printf,unit,procval.tin(i),procval.din(i),format='(5x,E10.3,15x,E10.3)'
endfor

printf,unit,'' 
printf,unit,' --- FPEC TEMPERATURE/DENSITY INDEX vs WAVELENGTH ---'

npix=pecdata.pix(procval.ibsel)
for i=0,npix-1 do begin
	printf, unit, fpec(*,i)
endfor

close,unit
free_lun, unit


END
