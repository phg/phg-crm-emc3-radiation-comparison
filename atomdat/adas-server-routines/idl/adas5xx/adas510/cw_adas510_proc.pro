; Copyright (c) 2002, Strathclyde University.
; SCCS info: Module @(#)$Header: /home/adascvs/idl/adas5xx/adas510/cw_adas510_proc.pro,v 1.2 2004/07/06 12:53:54 whitefor Exp $	Date  $Date: 2004/07/06 12:53:54 $
;+
; PROJECT:
;       ADAS IBM MVS to UNIX conversion
;
; NAME:
;	CW_ADAS510_PROC()
;
; PURPOSE:
;	Produces a widget for ADAS510 processing options/input.
;
; EXPLANATION:
;	This function creates a compound widget consisting of :-
;	   a text widget holding an editable 'Run title', 
;	   the dataset name/browse widget cw_adas_dsbr,
;	   a widget to select the data block for analysis,
;	   a table widget for temperature/density data cw_adas_table,
;	   buttons to enter default values into the table, 
;	   a button to invoke the interactive pair selector to select temperature-density pairs
;	   a message widget, 
;          an 'Escape to series menu', a 'Cancel' and a 'Done' button.
;
;	The compound widgets included in this widget are self managing.
;	This widget only manages events from the two 'Defaults' buttons,
;	the 'Cancel' button and the 'Done' button and the
;       'escape to series menu' button.
;
; USE:
;	This widget is specific to ADAS510, see adas510_proc.pro
;	for use.
;
; INPUTS:
;	TOPPARENT- Long integer, ID of parent widget.
;
;	DSFULL	- String; The full system file name of the input 
;		  dataset selected by the user for processing.
;
;	NSTORE  - Integer; maximum number of data blocks allowed.
;
;	NTDIM	- Integer; maximum number of temperatures allowed.
;
;	NDDIM	- Integer; maximum number of densities allowed.
;
;	NDTIN	- Integer; number of temp/density pairs selected
;
;	NBSEL   - Integer; number of data blocks included in this file
;
;	CWAVEMIN - String; Minimum wavelength of data block used
;				 dimension data block index (NSTORE).
;
;	CWAVEMAX - String; Maximum wavelength of data block used	
;				 dimension data block index (NSTORE).
;
;	CFILE   - String array; specific ion source file
;				dimension - NBSEL
;
;	CTYPE  - String array; Data type code
;				dimension - NBSEL
;
;	CINDM   - String array; Metastable index
;				dimension - NBSEL
;
;	ITA     - Integer array; Number of temperatures per block
;				dimension - NBSEL
;
;	IDA     - Integer array; Number of densities per block
;				dimension - NBSEL
;
;	TVALS   - Float array; temperature values from data file
;				dimension - NBSEL, NTDIM
;
;	TEDA    - Float array; density values from data file
;				dimension - NBSEL, NDDIM
;
;	PEC	  - Float array; evelope feature pec data from data file
;				dimension - NTDIM, NDDIM, NPIX, NBSEL
;
;	The inputs map exactly onto variables of the same
;	name in the ADAS510 FORTRAN program.
;
;       BITFILE - String; the path to the directory containing bitmaps
;                 for the 'escape to series menu' button.
;
; OPTIONAL INPUTS:
;	None
;
; OUTPUTS:
;	The return value of this function is the ID of the declared widget.
;
; OPTIONAL OUTPUTS:
;	None
;
; KEYWORD PARAMETERS:
;	PROCVAL	- A structure which determines the initial settings of
;		  the processing options widget.
;
;		  The default PROCVAL is created thus;
;
;    		str_arr = strarr(nbsel)
;	     	temp_arr = fltarr(ntdim)
;     		dens_arr = fltarr(nddim)
;     		ps = {proc510_set, $
;			nmet  : 0,		$
;                	title : '',             $
;			nbsel : 0 ,		$
;                	ibsel : 1 ,             $
;                	uwavemin: str_arr,        $
;			uwavemax: str_arr,        $
;                	ufile : str_arr,        $
;                	utype : str_arr,        $
;                	uindm : str_arr,        $
;			ifout : 1 ,             $
;			ldsel : 0,		$
;			maxt  : 0, 		$
;			maxd  : 0, 		$
;                	tin   : temp_arr,       $
;                	din   : dens_arr	}
;
;
;		NMET    Flag which defines whether or not default values
;			exist or not. (< 0 if no values exist)
; 		TITLE	Entered general title for program run
;		IBSEL   Selected data block
;		NBSEL   Number of blocks in data file
;		UWAVEMIN  Array of minimum wavelengths from data file
;		UWAVEMAX  Array of maximum wavelengths from data file
;		UFILE   Ion source files from data file
;		UTYPE   data type codes from data file
;		UINDM	Metastable indices from data file
;		IFOUT   Index indicating which units are being used
;		LDSEL   Flag for whether temp. or desity selected. 
;			0 = temperature, 1 = density
;		MAXT    Number of temperature values selected
;		MAXD    Number of density values selected
;		TIN     User supplied temperature values for fit.
;		DIN 	User supplied density values for fit.
;
;		All of these structure elements map onto variables of
;		the same name in the ADAS503 FORTRAN program.
;
;
;	UVALUE	- A user value for the widget. Default 0.
;
;	FONT_LARGE - The name of a larger font.  Default current system
;		     font
;
;	FONT_SMALL - The name of a smaller font. Default current system
;		     font.
;
;	EDIT_FONTS - A structure of two fonts used in the table editor
;		     adas_edtab.pro, {font_norm:'',font_input:''}.  Defaults
;		     to current system font.
;
;	NUM_FORM   - String; Numeric format to use in tables.  Default
;			'(E10.3)'
;		     
;
; CALLS:
;	POPUP		Popup warning window with buttons.
;	CW_ADAS_DSBR	Dataset name and comments browsing button.
;	CW_ADAS_TABLE	Adas data table widget.
;	CW_ADAS_SEL	Adas multiple selection widget.
;	CW_SINGLE_SEL	Adas scrolling table and selection widget.
;	CW_OPT_VALUE    Adas option selection widget with required input 
;			value.
;	CW_ADAS_PAIRSEL Adas pair value selector. 
;
;	See side effects for other related widget management routines.
;
; SIDE EFFECTS:
;
;	The following widget management routines are included in this file;
;	PROC510_GET_VAL()	Returns the current PROCVAL structure.
;	PROC510_EVENT()		Process and issue events.
;	
; CATEGORY:
;	Compound Widget
;
; WRITTEN:
;       Richard Martin, University of Strathclyde, 14-06-02
;	  Based on cw_adas503_proc.pro v1.8.
;
; MODIFIED:
;	1.1     Richard Martin
;             First release
;	1.2     Richard Martin
;             Added temporary fix for unitname, unitind dimensioning
;			problem.
;
; VERSION:
;	1.1	14-06-02
;	1.2	15-09-03
;
;-
;-----------------------------------------------------------------------------

FUNCTION proc510_get_val, id

                ;**** Return to caller on error ****

    ON_ERROR, 2

		 ;***************************************
                 ;****     Retrieve the state 	     ****
		 ;**** Get first_child widget id     ****
		 ;**** because state is stored there ****
		 ;***************************************

    first_child = widget_info(id, /child)
    widget_control, first_child, get_uvalue = state

		;***********************************
		;**** Get run title from widget ****
		;**** Then centre in in string  ****
		;**** of 40 characters 		****
		;***********************************

    widget_control, state.runid, get_value=title
    title_len = strlen(title(0)) 
    if (title_len gt 40 ) then begin 
	title = strmid(title,0,38)
        widget_control, state.messid, set_value="Title too long - truncated "
        widget_control, state.runid, set_value=title
	wait, 1
    endif
    pad = (40 - title_len)/2 
    spaces = '                                         '
    title(0) = strmid(spaces,0,(pad-1)) + title(0) + strmid(spaces,0,(pad-1))
  

		;****************************************************
		;**** Get new temperature data from table widget ****
		;****************************************************

    widget_control, state.tempid, get_value=tempval
    tabledata = tempval.value
    ifout = tempval.units + 1
    ldsel = state.ldsel

		;**** Copy out temperature values ****

    ntdim=state.ntdim
    nddim=state.nddim
    nmax=max(ntdim,nddim)
    maxt=state.maxt
    maxd=state.maxd
    
    tin = dblarr(nmax)
    din = dblarr(nmax)
    blanks = where(strtrim(tabledata(0,*),2) eq '')

                ;***********************************************
		;**** next line assumes that all blanks are ****
		;**** at the end of the columns             ****
                ;***********************************************

    if blanks(0) ge 0 then maxt=blanks(0) else maxt=state.ntdim

                ;*************************************************
                ;**** Only perform following if there is 1 or ****
                ;**** more value present in the table         ****
                ;*************************************************
  
    if (maxt ge 1) then begin
  	tin(0:maxt-1) = double(tabledata(0,0:maxt-1))
    endif

                ;**** Now do same for densities ****
  
    blanks = where(strtrim(tabledata(2,*),2) eq '')

    if blanks(0) ge 0 then maxd=blanks(0) else maxd=state.nddim
    
    if (maxd ge 1) then begin
  	din(0:maxd-1) = double(tabledata(2,0:maxd-1))
    endif

		;**** Fill in the rest with zero ****

    if maxt lt nmax then begin
        tin(maxt:nmax-1) = double(0.0)
    endif
    if maxd lt nmax then begin
        din(maxd:nmax-1) = double(0.0)
    endif 
	
		;***********************************************
		;**** get new index for data block selected ****
		;***********************************************

    widget_control, state.indexid, get_value=select_block
    ibsel = select_block  

		;***********************************************
		;**** write selected values to PS structure ****
		;***********************************************

     ps = { nmet  : 0, 						$
            title : title,          				$
		nbsel : state.nbsel, 					$
            ibsel : ibsel ,             				$
		uwavemin: state.uwavemin,				$
		uwavemax: state.uwavemax,				$		
		ufile : state.ufile,					$
		utype : state.utype,					$
		uindm : state.uindm, 					$
		ifout : ifout,						$
		ldsel : ldsel, 						$
		maxt  : maxt,						$
		maxd  : maxd,						$
            tin   : tin,       					$
            din   : din			}
   
    widget_control, first_child, set_uvalue=state, /no_copy

    RETURN, ps

END

;-----------------------------------------------------------------------------

FUNCTION proc510_event, event

                ;**** Base ID of compound widget ****

    parent = event.handler

		;**********************************************
                ;**** Retrieve the user value state        ****
		;**** Get id of first_child widget because ****
		;**** user value "state" is stored there   ****
		;**********************************************

    first_child = widget_info(parent, /child)
    widget_control, first_child, get_uvalue=state,/no_copy

		;*********************************
		;**** Clear previous messages ****
		;*********************************

    widget_control, state.messid, set_value=' '

                ;************************
                ;**** Process events ****
                ;************************

    CASE event.id OF

                ;*************************************
                ;****      Select index           ****
                ;*************************************

    	state.indexid: begin

                ;**** Get current index select value ****

            widget_control, state.indexid, get_value=ibsel
            ntdim = state.ntdim
		nddim = state.nddim
            ntval = state.ita(ibsel)
            ndval = state.ida(ibsel)
		
                ;**** Get current table widget value ****

            widget_control, state.tempid, get_value=tempval

                ;**** Copy new ibsel values into structure  ****

            if (ntval gt 0) then begin
                tempval.value(1,0:ntval-1 ) =                        	$
                strtrim(string(state.tvals(0:ntval-1,ibsel),        	$
                format=state.num_form),2)
                if (ndval gt 0) then begin
                    tempval.value(3,0:ndval-1) =                   	$
                    strtrim(string(state.teda(0:ndval-1,ibsel),     	$
                    format=state.num_form),2)
    
                ;**** fill rest of table with blanks ****

                    if (ndval lt nddim) then begin
                        tempval.value(3,ndval:ntdim-1) = ' '
                    endif
	          endif
                if (ntval lt ntdim ) then begin
                    tempval.value(1,ntval:ntdim-1 ) = ' '
                endif
                if (ndval lt ntdim ) then begin
			  tempval.value(3,ndval:nddim-1) = ' '
                endif		    

		;***************************************
                ;**** Copy new data to table widget ****
                ;**** reset state.ibsel             ****
		;***************************************

                widget_control, state.tempid, set_value=tempval
                state.ibsel = ibsel
            endif
        end

		;*************************************
		;**** Default temperature button ****
		;*************************************

    	state.deftid: begin

		;**** popup window to confirm overwriting current values ****

	    action= popup(message='Confirm Overwrite values with Defaults',$
			  buttons=['Confirm','Cancel'], font=state.font)
	    if action eq 'Confirm' then begin

		;**** Get current table widget value ****

 	   	widget_control,state.tempid,get_value=tempval

		;*** let user select the default density value ****

	     	maxt = state.ita(state.ibsel)
	     	maxd = state.ida(state.ibsel)
	   	dvalues = strarr(maxd)
	   	for i = 0, maxd-1 do begin
              	    dvalues(i) =  strcompress(string(tempval.value(3,i)))
           	endfor
	   	action= popup(message='Select Default density value', 	$
			      buttons=dvalues, /column, font=state.font)
           	defdensity = action
   
            ;***********************************************
		;**** Copy defaults into value structure    ****
		;**** For default values use the existing   ****
		;**** table values and let user modify them ****
		;**** then set all density values to same   ****
		;**** value  				    ****
            ;***********************************************

            deft = tempval
            units = tempval.units+1
 	     	if maxt gt 0 then begin
   	           tempval.value(0,0:maxt-1) = 				$
		   strtrim(string(deft.value(units,0:maxt-1), 		$
                   format=state.num_form),2)
   	           tempval.value(2,0:maxt-1) =  			$
		   string(defdensity, format=state.num_form)
		   state.maxd=maxt
		   state.maxt=maxt		   
 	  	endif

		;**** Fill in the rest with blanks ****

 	  	if maxt lt state.ntdim then begin
   	            tempval.value(0,maxt:state.ntdim-1) = ''
 	  	endif
 	  	if maxt lt state.nddim then begin
                   tempval.value(2,maxt:state.nddim-1) = ''		
 	  	endif
		
                ;***************************************
		;**** Set ldsel variable to 0 i.e.  ****
		;**** temperature selected          ****
                ;***************************************

	  	state.ldsel = 0

		;**** Copy new data to table widget ****

 	  	widget_control,state.tempid, set_value=tempval
 	  		
	    endif
        end

		;*************************************
		;**** Default Density button ****
		;*************************************

    	state.defdid: begin

		;**** popup window to confirm overwriting current values ****

	    action= popup(message='Confirm Overwrite values with Defaults',$
			  buttons=['Confirm','Cancel'], font=state.font)
	    if action eq 'Confirm' then begin

		;**** Get current table widget value ****

 	   	widget_control,state.tempid,get_value=tempval

		;*** let user select the default density value ****

	   	tvalues = strarr(state.ita(state.ibsel))
	   	for i = 0, state.ita(state.ibsel)-1 do begin
              	    tvalues(i) =  strcompress(string(tempval.value(1,i)))
           	endfor
	   	action= popup(message='Select Default temperature value', 	$
			      buttons=tvalues, /column, font=state.font)
           	deftemp = action
   		
            ;***********************************************
		;**** Copy defaults into value structure    ****
		;**** For default values use the existing   ****
		;**** table values and let user modify them ****
		;**** then set all density values to same   ****
		;**** value  				    ****
            ;***********************************************

            defd = tempval
            units = tempval.units+1
	     	maxt = state.ita(state.ibsel)
	     	maxd = state.ida(state.ibsel)
 	     	if maxd gt 0 then begin
   	           tempval.value(2,0:maxd-1) = 				$
		   strtrim(string(defd.value(3,0:maxd-1), 		$
                   format=state.num_form),2)
   	           tempval.value(0,0:maxd-1) =  			$
		   string(deftemp, format=state.num_form)
		   state.maxt=maxd
		   state.maxd=maxd  
 	  	endif
			
		;**** Fill in the rest with blanks ****

 	  	if maxd lt state.nddim then begin
                    tempval.value(2,maxd:state.nddim-1) = ''
 	  	endif
 	  	if maxd lt state.ntdim then begin
   	            tempval.value(0,maxd:state.ntdim-1) = ''
			tempval.value(2,maxd:state.ntdim-1) = ''
 	  	endif
		
                ;***************************************
		;**** Set ldsel variable to 0 i.e.  ****
		;**** density selected          ****
                ;***************************************

	  	state.ldsel = 1

		;**** Copy new data to table widget ****

 	  	widget_control,state.tempid, set_value=tempval
 	  	
	    endif
        end
	  
    	state.ipsid: begin   
 	  	  widget_control,state.tempid, get_value=tempval 
 	  	  units=1	
		  ntdim=state.ntdim
		  nddim=state.nddim
		  nmaxdim=max(ntdim,nddim)
		  newt=fltarr(nmaxdim)
		  newd=fltarr(nmaxdim)    
		  ntdim=max(state.ita(*))
		  tval = dblarr(ntdim,2)
		  tval(*,0)=state.tvals(*,state.ibsel)
		  tval(*,1)=state.tvals(*,state.ibsel)
		  ntindex=intarr(state.ntdim)
		  ndindex=intarr(state.nddim)
		  ipec = state.pec(*,*,*,state.ibsel)
		  spec = total(ipec,3)
		  inval = {   numte : state.ntdim,  	  $
		  		  numden: state.nddim,  	  $
		  		  nbsel : state.ibsel,  	  $
		  		  te    : state.ita,		  $
		  		  den   : state.ida,		  $
		  		  tval  : tval,			  $
		  		  dval  : state.teda(*,state.ibsel), $
		  		  newt  : newt,			  $
		  		  newd  : newd,			  $
				  ntindex: ntindex,		  $
				  ndindex: ndindex,		  $
		  		  units : units,			  $
		  		  pec   : spec	  }
				  
		  cw_adas_pairsel, VALUE=inval, XSIZE = xsize, YSIZE = ysize, $
              FONT_LARGE = state.font

                ;**** Get current table widget value ****

            widget_control, state.tempid, get_value=tempval

                ;**** Copy new ibsel values into structure  ****

	      ntval1=where(inval.newt gt 0.0,count)
	      ntval1=count
	      ntval2= state.ita(state.ibsel)
            ndval1= ntval1
            ndval2 = state.ida(state.ibsel)
            ntdim=state.ntdim	      
            if (ntval1 gt 0) then begin
                tempval.value(0,0:ntval1-1 ) =                        	$
                	strtrim(string(inval.newt(0:ntval1-1),        		$
                	format=state.num_form),2)
                tempval.value(1,0:ntval2-1) =                       	$
                	strtrim(string(state.tvals(0:ntval2-1,state.ibsel), $
                	format=state.num_form),2)

                tempval.value(2,0:ndval1-1) =                        	$
                	strtrim(string(inval.newd(0:ntval1-1),        		$
                	format=state.num_form),2)
                if (ndval2 gt 0) then begin    
                    tempval.value(3,0:ndval2-1) =                   	$
                    strtrim(string(state.teda(0:ndval2-1,state.ibsel),  $
                    format=state.num_form),2)
    
                ;**** fill rest of table with blanks ****

                    if (ndval2 lt ntdim) then begin
                        tempval.value(3,ndval2:ntdim-1) = ' '
                    endif
	          endif
                if (ndval1 lt ntdim) then begin
                    tempval.value(2,ndval1:ntdim-1) = ' '
                endif			
	          	          
                if (ntval1 lt ntdim ) then begin
                     tempval.value(0,ntval1:ntdim-1 ) = ' '               
                endif
                if (ntval2 lt ntdim ) then begin
                    tempval.value(1,ntval2:ntdim-1 ) = ' '               
                endif

		;***************************************
                ;**** Copy new data to table widget ****
                ;**** reset state.ibsel             ****
		;***************************************

                widget_control, state.tempid, set_value=tempval
                
            endif
	end

		;***********************
		;**** Cancel button ****
		;***********************

    	state.cancelid: new_event = {ID:parent, TOP:event.top, 		$
				     HANDLER:0L, ACTION:'Cancel'}

		;*********************
		;**** Done button ****
		;*********************

    	state.doneid: begin

		;***************************************
		;**** Check all user input is legal ****
		;***************************************

	    error = 0

        	;********************************************
		;*** Have to restore "state" before calls ***
		;*** to proc510_get_val so it can be used ***
		;*** there.				  ***
		;********************************************
		
	    widget_control, first_child, set_uvalue=state, /no_copy
	    widget_control, event.handler, get_value=ps

		;*** reset state variable ***

	    widget_control, first_child, get_uvalue=state, /no_copy

		;**** check temp/density values entered ****

            if error eq 0 then begin
                widget_control, state.tempid, get_value=tabval
                tabvals =                                               $
                where(strcompress(tabval.value(0,*), /remove_all) ne '')
                if tabvals(0) eq -1 then begin
                    error = 1
                    message = '**** Error: No temperatures/densities'+  $
                              ' entered ****'
                endif
            endif

		;*** Check to see if index has been selected ***

	    if error eq 0 and ps.ibsel lt 0 then begin
	        error = 1
	        message='**** Error: Invalid block selected ****'
	    end

		;**** return value or flag error ****

	    if error eq 0 then begin
	        new_event = {ID:parent, TOP:event.top, 			$
                             HANDLER:0L, ACTION:'Done'}
            endif else begin
	        widget_control, state.messid, set_value=message
	        new_event = 0L
            endelse
        end

                ;**** Menu button ****

        state.outid: begin
            new_event = {ID:parent, TOP:event.top, HANDLER:0L,          $
                         ACTION:'Menu'}
        end

    ELSE: new_event = 0L

  ENDCASE

		;*********************************************
		;*** make "state" available to other files ***
		;*********************************************

    widget_control, first_child, set_uvalue=state, /no_copy

    RETURN, new_event

END

;-----------------------------------------------------------------------------

function cw_adas510_proc, topparent, dsfull, nstore, ntdim, nddim, 	$
                          ndtin, nbsel, cwavemin, cwavemax, cfile, ctype, cindm, 	$
		              ita, ida, tvals, teda, pec, bitfile, 		$
                          procval=procval, uvalue=uvalue, 		$
                          font_large=font_large, font_small=font_small, $
                          edit_fonts=edit_fonts, num_form=num_form

		;**** Set defaults for keywords ****

    if not (keyword_set(uvalue)) then uvalue = 0
    if not (keyword_set(font_large)) then font_large = ''
    if not (keyword_set(font_small)) then font_small = ''
    if not (keyword_set(edit_fonts)) then 				$
    edit_fonts = {font_norm:'',font_input:''}
    if not (keyword_set(num_form)) then num_form = '(E10.3)'
    if not (keyword_set(procval)) then begin
         str_arr  = strarr(ntdim)
         temp_arr = fltarr(ntdim)
         dens_arr = fltarr(nddim)
         ps = { nmet  : 0, 						$
		title : '',             				$
		nbsel : 0 , 						$
                ibsel : 0 ,             				$
                uwavemin: str_arr,        			$
                uwavemax: str_arr,        			$		    
                ufile : str_arr,        				$
                utype : str_arr,        				$
                uindm : str_arr,        				$
		ifout : 1,						$
		ldsel : 0,              				$
		maxt  : 0,						$
		maxd  : 0,						$
                tin   : temp_arr,       				$
                din   : dens_arr	}
    endif else begin
	ps = {  nmet  : procval.nmet,      				$
		title : procval.title,     				$
		nbsel : procval.nbsel,     				$
            ibsel : procval.ibsel,     				$
        	uwavemin: strarr(n_elements(cwavemin)),  			$
        	uwavemax: strarr(n_elements(cwavemax)),  			$		
        	ufile : strarr(n_elements(cfile)),  			$
        	utype : strarr(n_elements(ctype)),  			$
        	uindm : strarr(n_elements(cindm)),  			$ 
		ifout : procval.ifout,	   				$
		ldsel : procval.ldsel,     				$
		maxt  : procval.maxt,	   				$
                maxd  : procval.maxd,      				$
                tin   : procval.tin,       				$
                din   : procval.din	}
    endelse

    ufile = cfile
    utype = ctype 
    uindm = cindm

                ;*********************************************************
                ;**** Modify certain parameters and results depending ****
                ;**** on the machine being used                       ****
                ;*********************************************************

    machine = GETENV('TARGET_MACHINE')
    if machine eq 'HPUX' then begin
        y_size = 4
        large_font = font_small
    endif else begin
        y_size = 6
        large_font = font_large
    endelse

		;****************************************************
		;**** Assemble temperature  & density table data ****
		;****************************************************
		;**** The adas table widget requires data to be *****
		;**** input as strings, so all the numeric data *****
		;**** has to be written into a string array.    *****
		;**** Declare temp. & dens.table array          *****
		;**** col 1 has user temperature values         *****
		;**** col 2 has temperature values from files,  *****
		;**** which can have one of three possible units*****
		;**** col 5 has user density values             *****
		;**** col 6 has density values from file.       *****
		;****************************************************
    
    tabledata = strarr(4,ndtin)

                ;***********************************************
		;**** Copy out temperature & density values ****
		;**** number of temperature and density     ****
		;**** values for this data block            ****
                ;***********************************************

    if (ps.ibsel+1 gt nbsel) then ps.ibsel = 0
    if (ps.maxt eq 0) then maxt = ita(ps.ibsel) else maxt=ps.maxt
    if (ps.maxd eq 0) then maxd = ida(ps.ibsel) else maxd=ps.maxd
    if (maxt gt 0) then begin
        tabledata(0,0:maxt-1) = 						$
	strtrim(string(ps.tin(0:maxt-1),format=num_form),2)
        tabledata(1,0:ita(ps.ibsel)-1) = 					$
        strtrim(string(tvals(0:ita(ps.ibsel)-1,ps.ibsel),format=num_form),2)
        tabledata(2,0:maxd-1) = 					$
     	strtrim(string(ps.din(0:maxd-1),format=num_form),2)
        tabledata(3,0:ida(ps.ibsel)-1) = 					$
	strtrim(string(teda(0:ida(ps.ibsel)-1,ps.ibsel),format=num_form),2)

		;**** fill rest of table with blanks ****

        blanks = where(ps.tin eq 0.0) 
        if blanks(0) gt -1 then tabledata(0,blanks) = ' ' 
        blanks = where(ps.din eq 0.0) 
	  if blanks(0) gt -1 then tabledata(2,blanks) = ' ' 
    endif
    
    

		;********************************************************
		;**** Create the 510 Processing options/input window ****
		;********************************************************

		;**** create titled base widget ****

    parent = widget_base(topparent, UVALUE = uvalue, 			$
			 title = 'ADAS510 PROCESSING OPTIONS', 		$
			 EVENT_FUNC = "proc510_event", 			$
			 FUNC_GET_VALUE = "proc510_get_val", 		$
			 /COLUMN)

		;******************************************************
		;**** Create a dummy widget just to hold value of *****
		;**** "state" variable so as not to get confused  *****
		;**** with any other values. Adopt IDL practice   *****
		;**** of using first child widget                 *****
		;******************************************************

    first_child = widget_base(parent)
    topbase = widget_base(first_child,/column)

		;***********************
		;**** add run title ****
		;***********************

    base = widget_base(topbase, /row)
    rc = widget_label(base, value='Title for Run', font=large_font)
    runid = widget_text(base, value=ps.title, xsize=38, font=large_font, /edit)
  
		;**** add dataset name and browse button ****

    rc = cw_adas_dsbr(topbase, dsfull, font=large_font)

		;***************************************************
		;**** add a window to display and select index   ***
		;**** first create data array for table          ***
		;**** Then convert to 1D string array(text_table)***
		;**** call cw_single_sel.pro for options choice  ***
		;***************************************************

    block_info = strarr(4, nbsel)
    uwavemin = string(cwavemin,format='(F6.1)')	
    uwavemax = string(cwavemax,format='(F6.1)')    
    block_info(0,*) = strcompress(uwavemin + ' - ' + uwavemax)
    block_info(1,*) = cfile
    block_info(2,*) = ctype 
    block_info(3,*) = cindm
    titles = [['  Wavelength', 'Ion', 'Processing', 'Metastable'], 	$
   	      ['  Range','Source','Code', 'Index']]
    select_data = text_table(block_info, colhead=titles)
    coltitles = select_data(0:1)
    select_data = select_data(2:nbsel+1)
    indexid = cw_single_sel( parent, select_data, value=ps.ibsel, 	$
			     title='Select data Block', 		$
			     coltitles=coltitles, ysize=(y_size-2),	$
			     font=font_small, big_font=large_font)

		;**********************
		;**** Another base ****
		;**********************

    tablebase = widget_base(parent, /row)

		;************************************************
		;**** base for the table and defaults button ****
		;************************************************

    base = widget_base(tablebase, /column, /frame)

		;********************************
		;**** temperature data table ****
		;********************************

    colhead = [['Output','Input','Output','Input'], 			$
	       ['','','','']]

		;********************************************
		;**** convert FORTRAN index to IDL index ****
		;********************************************

    units = ps.ifout - 1 
    unitname = [ 'eV','eV']

                ;***********************************************************
		;**** Four columns in the table and three sets of units.****
		;**** Column 2 has the same values for all three        ****
		;**** units but column 2 switches between sets 2,3 & 4  ****
		;**** in the input array tabledata as the units change. ****
                ;***********************************************************

    unitind = [[0,0],[1,1],[2,2],[3,3]] 
       
    table_title = [ ["Temperature & Density Values"], 			$
		  ["Temperature             Density"]]

		;****************************
		;**** table of data   *******
		;****************************

    tempid = cw_adas_table(base, tabledata,  units, unitname, unitind, 		$
			   UNITSTITLE = 'Temperature Units', 		$
			   COLEDIT = [1,0,1,0], COLHEAD = colhead, 	$
			   TITLE = table_title, 			$
			   UNITS_PLUS = '       Density Units : cm-3', 	$
			   LIMITS = [1,1,1,1], CELLSIZE = 12, 		$
			   /SCROLL, ytexsize=y_size, NUM_FORM=num_form, $
			   FONTS = edit_fonts, FONT_LARGE = large_font, $
			   FONT_SMALL = font_small)

		;*************************
		;**** Default buttons ****
		;*************************

    tbase = widget_base(base, /row)
    deftid = widget_button(tbase, value=' Default Te ',$
		           font=large_font)
    defdid = widget_button(tbase, value=' Default Ne ',$
		           font=large_font)			     
    ipsid = widget_button(tbase, value=' Value Selection by display  ',$
		           font=large_font)
    
		;**** Error message ****

    messid = widget_label(parent, font=large_font, 			$
    value='Edit the processing options data and press Done to proceed')

		;**** add the exit buttons ****

    base = widget_base(parent, /row)
    menufile = bitfile + '/menu.bmp'
    read_X11_bitmap, menufile, bitmap1
    outid = widget_button(base, value=bitmap1)          ;menu button
    cancelid = widget_button(base, value='Cancel', font=large_font)
    doneid = widget_button(base, value='Done', font=large_font)
  
                ;*************************************************
		;**** create a state structure for the pop-up ****
		;**** 		     window.                  ****
                ;*************************************************

    new_state = { runid		:	runid,  			$
			messid	:	messid, 			$
			deftid	:	deftid,			$
			defdid	:	defdid,			$			
			ipsid		:	ipsid,			$
			tempid	:	tempid, 			$
			indexid	:	indexid,			$
			cancelid	:	cancelid,			$
			doneid	:	doneid, 			$
                  outid   	:     outid,                  $
			dsfull	:	dsfull,			$
			nbsel 	:	nbsel, 			$
			nstore	:	nstore,			$
			ntdim		:	ntdim,			$
			nddim		:	nddim,			$
			ibsel		:	ps.ibsel,			$
			maxt		:	maxt, 			$
			maxd		:	maxd, 			$
			ita		:	ita,				$
			ida		:	ida,				$
			ldsel		:	ps.ldsel,			$
			tvals		:	tvals,			$
			teda		:	teda,				$
			pec		:	pec,				$
			uwavemin	:	uwavemin,			$
			uwavemax	:	uwavemax,			$			
			ufile		:	cfile,			$
			utype 	:	ctype ,			$
			uindm		:	cindm, 			$
			font		:	large_font,			$
			num_form	:	num_form			}

                 ;**** Save initial state structure ****

    widget_control, first_child, set_uvalue=new_state, /no_copy

    RETURN, parent

END

