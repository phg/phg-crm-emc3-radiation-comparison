; Copyright (c) 2003, Strathclyde University.
; SCCS info: Module @(#)$Header: /home/adascvs/idl/adas5xx/adas510/eaoutg.pro,v 1.1 2004/07/06 13:47:43 whitefor Exp $	Date $Date: 2004/07/06 13:47:43 $
;+
; PROJECT:
;       ADAS IBM MVS to UNIX conversion
;
; NAME:
;	E3OUTG
;
; PURPOSE:
;	Invokes adas510 graphics output.
;
; EXPLANATION:
;	The routine begins by setting up various strings and then calls the 
;	IDL graphical output routine for ADAS510.
;
; USE:
;	The use of this routine is specific to ADAS510 see adas510.pro.
;
; INPUTS:
;	PECA 	   - Array containing output F-PEC data.
;
;	PECDATA  - Straucture containing input and processed data.
;
;	PROCVAL  - Structure containing processing options.
;
;	DSFULL   - Data file name
;
;	UTITLE   - Optional comment by user
;	
;	TITLE    - Title for the run.
;
;	DATE	   - String containing the date.
;
;	GRPSCAL	 - Integer; 0 if default scaling required 1 if user
;		   specified scaling to be used.
;
;	LDSEL  - flag 0 - Temperature used for X-axis
;			  1 - Density used for X-axis.
;
;	XMIN	 - String; User sepcified x-axis minimum, number as string.
;
;	XMAX	 - String; User sepcified x-axis maximum, number as string.
;
;	YMIN	 - String; User specified y-axis minimum, number as string.
;
;	YMAX	 - String; User specified y-axis maximum, number as string.
;
;
;	HRDOUT	 - Integer; 0 if no hardcopy required, 1 if hardcopy.
;
;	HARDNAME - String; file name for hardcopy output.
;
;	DEVICE	 - String; IDL name for hardcopy output device.
;
;	HEADER	 - ADAS version header information for inclusion in the
;		   graphical output.
;
;     BITFILE - String; the path to the directory containing bitmaps
;                 for the 'escape to series menu' button.
;
;
; OPTIONAL INPUTS:
;	FONT:	 - Font to be used instead of default.
;
; OUTPUTS:
;       GOMENU  - Int; flag - set to 1 if user has selected 'escape direct
;                 to series menu , otherwise 0
;
; OPTIONAL OUTPUTS:
;	None.
;
; KEYWORD PARAMETERS:
;	FONT	- String; Name of the font to be used for the graphical
;		  output widget.
;
; CALLS:
;	ADAS510_PLOT	ADAS510 graphical output.
;
; SIDE EFFECTS:
;	None.
;
; CATEGORY:
;	Adas system.
;
; WRITTEN:
;       Richard Martin, University of Strathclyde, 17-06-02.
;	  Based on e3outg.pro, v1.4.
;
; MODIFIED:
;	1.1	Richard Martin
;		First release
;
; VERSION:
;	1.1	17-06-02
;
;-
;-----------------------------------------------------------------------------

PRO eaoutg, peca, pecdata, procval, dsfull, utitle, title, date, grpscal, ldsel, $
		xmin, xmax, xmin1, xmax1, xmin2, xmax2, ymin, ymax, zmin, zmax, $
		hrdout, hardname, device, header, $
		bitfile, gomenu, FONT=font


                ;**** Set defaults for keywords ****

    IF NOT (KEYWORD_SET(font)) THEN font = ''


		;**** Declare variables for input ****


 
    esym = " "
    iz0 = 0
    iz = 0

    ldef1 = 0
    teva = 0
    din = 0
    tdfitm = 0
    nmx = 0
    head1 = " "
    head2a = " "
    head2b = " "
    strg = make_array(7, /string, value=" ")
    dummy = 0.0
    sdum = " "
    idum = 0

		;********************************
		;**** Read data from fortran ****
		;********************************


    esym = pecdata.esym
    iz0 = pecdata.iz0
    iz = pecdata.iz
    npix= pecdata.pix(procval.ibsel)

    cwavemin = pecdata.cwavemin(procval.ibsel)
    cwavemax = pecdata.cwavemax(procval.ibsel)
    uwavemin = string(cwavemin,format='(F6.1)')	
    uwavemax = string(cwavemax,format='(F6.1)')    
    cwavel =  strcompress(uwavemin + ' - ' + uwavemax)   

    cfile = procval.ufile(procval.ibsel)
    ctype = procval.utype(procval.ibsel) 
    cindm = procval.uindm(procval.ibsel)
    dummy=where(procval.tin gt 0.0,itval)
    

    titlx = dsfull + ' BLK=' + strtrim(string(procval.ibsel),2)
    titlx = titlx + '; WVRNG=' + cwavel + ' A;'
    titlx = titlx + ' MET=' + strtrim(string(cindm),2) +';'
    titler= procval.title
    
		;**** now declare array dimensions ****

    teva=double(procval.tin)
    din=double(procval.din)


    strg(0) =' ELEMENT SYMBOL          = '
    strg(1) =' NUCLEAR CHARGE          = '    
    strg(2) =' CHARGE STATE             = '    
    strg(3) =' WAVELENGTH RANGE	      = '    
    strg(4) =' SPECIFIC ION FILE SOURCE = '   
    strg(5) =' DATA TYPE CODE           = '   
    strg(6) =' METASTABLE INDEX         = '
    
    head1  = '--- EMITTING ION INFORMATION ---'

    head2a = '- ELECTRON TEMPERATURE/DENSITY RELATIONSHIP -'

    head2b = 'INDEX    TEMPERATURE (eV)    DENSITY (cm-3)  '

		;***********************
		;**** Plot the data ****
		;***********************
		
    adas510_plot, dsfull,title , titlx, utitle, date, esym, iz0, iz,	$
		 cwavemin, cwavemax, cfile, ctype , cindm, itval, npix,	$
		 teva, din, peca, grpscal, ldsel, xmin, xmax, xmin1, xmax1, $
		 xmin2, xmax2, ymin, ymax, 	$
		 zmin, zmax, strg, head1, head2a, head2b, hrdout, hardname,	$
		 device, header, bitfile, gomenu, FONT=font
                  
END
