; Copyright (c) 2002, Strathclyde University .
; SCCS info: Module @(#)$Header: /home/adascvs/idl/adas5xx/adas510/adas510_out.pro,v 1.1 2004/07/06 11:01:06 whitefor Exp $	 Date $Date: 2004/07/06 11:01:06 $
;+
;
; NAME:
;	adas510_out
;
; PURPOSE:
;	IDL ADAS user interface, output options.
;
; EXPLANATION:
;	This routine creates and manages a pop-up window which allows
;	the user to select options and input data to control ADAS510
;	graphical and file output.
;
; USE:
;	This routine is ADAS510 specific, see e3spf1.pro for how it
;	is used.
;
; INPUTS:
;	VAL	- A structure which determines the initial settings of
;		  the output options widget.  The value is passed
;		  unmodified into cw_adas510_out.pro.
;
;		  See cw_adas510_out.pro for a full description of this
;		  structure.
;
;	DSFULL	- String; The full system file name of the input 
;		  dataset selected by the user for processing.
;
;       BITFILE - String; the path to the dirctory containing bitmaps
;                 for the 'escape to series menu' button.
;
; OPTIONAL INPUTS:
;	None.
;
; OUTPUTS:
;       ACT     - String; 'Done', 'Cancel' ,'File' or 'Menu' for the button the
;		  user pressed to terminate the output options window.
;
; OPTIONAL OUTPUTS:
;	None.
;
; KEYWORD PARAMETERS:
;	DEVLIST	- A string array;  Each element of the array is the
;		  name of a hardcopy graphics output device.  This
;		  list must mirror the DEVCODE list.  DEVLIST is the
;		  list which the user will see.
;		  e.g ['Post-Script','HP-PCL','HP-GL']
;
;	FONT	- Supplies the font to be used for the interface widgets.
;
; CALLS:
;	CW_adas510_out	Creates the output options widget.
;	DYNLABEL	Recursively adds dynamic_resize keyword to label
;			widgets for version 4.0.1 and higher of IDL
;	XMANAGER
;	See side effects for widget management routine.
;
; SIDE EFFECTS:
;	This routine uses a common block OUT510_BLK to maintain its state.
;	adas510_out_EV	is included in this file and is called
;			indirectly from XMANAGER during widget
;			management.
;
; CATEGORY:
;	Widgets
;
; WRITTEN:
;       Richard Martin, University of Strathclyde, 14-06-2002
;	  Based adas503_out.pro, v 1.4
;
; MODIFIED:
;       1.1     Richard Martin
;               First release
;
; VERSION:
;	1.1	14-06-02
;
;-
;-----------------------------------------------------------------------------

PRO adas510_out_ev, event

    COMMON out510_blk, action, value
	

		;**** Find the event type and copy to common ****

    action = event.action

    CASE action of

		;**** 'Done' button ****

	'Done'  : begin

		;**** Get the output widget value ****

     	    child = widget_info(event.id,/child)
	    widget_control, child, get_value=value 

                ;*****************************************
		;**** Kill the widget to allow IDL to ****
		;**** continue and interface with     ****
		;**** FORTRAN only if there is work   ****
		;**** for the FORTRAN to do.          ****
                ;*****************************************

     	    if (value.grpout eq 1) or (value.texout eq 1) then begin
	        widget_control, event.top, /destroy
	    endif 
	end

		;**** 'Cancel' button ****

	'Cancel': widget_control, event.top, /destroy

                ;**** 'Menu' button ****

        'Menu': widget_control, event.top, /destroy
        
        'File': widget_control, event.top, /destroy
        
        ELSE:   ;**** Do nothing ****

    ENDCASE

END

;-----------------------------------------------------------------------------

PRO adas510_out, val, dsfull, act, bitfile, devlist=devlist, font=font

    COMMON out510_blk, action, value

		;**** Copy value to common ****

    value = val

		;**** Set defaults for keywords ****

    if not (keyword_set(font)) then font = ''
    if not (keyword_set(devlist)) then devlist = ''

		;***************************************
		;**** Pop-up a new widget           ****
		;***************************************

                ;**** create base widget ****

    outid = widget_base(title='ADAS510 OUTPUT OPTIONS', xoffset=100,	$
                        yoffset=1)

		;**** Declare output options widget ****

    cwid = cw_adas510_out(outid, dsfull, bitfile, value=value,  	$
			  devlist=devlist, font=font )

		;**** Realize the new widget ****

    dynlabel, outid
    widget_control, outid, /realize

		;**** make widget modal ****

    xmanager, 'adas510_out', outid, event_handler='adas510_out_ev',	$
              /modal, /just_reg
 
		;**** Return the output value from common ****

    act = action
    val = value

END

