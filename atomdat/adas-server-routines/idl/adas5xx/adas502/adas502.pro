; Copyright (c) 1995, Strathclyde University.
; SCCS info: Module @(#)$Header: /home/adascvs/idl/adas5xx/adas502/adas502.pro,v 1.10 2004/07/06 10:53:13 whitefor Exp $ Date $Date: 2004/07/06 10:53:13 $
;+
; PROJECT:
;       ADAS IBM MVS to UNIX conversion
;
; NAME:
;	ADAS502
;
; PURPOSE:
;	The highest level routine for the ADAS502 program.
;
; EXPLANATION:
;	This routine is called from the main adas system routine, adas.pro,
;	to start the ADAS 502 application.  Associated with adas502.pro
;	is a FORTRAN executable.  The IDL code provides the user
;	interface and output graphics whilst the FORTRAN code reads
;	in data files, performs numerical processing and creates the
;	output files.  The IDL code communicates with the FORTRAN
;	executable via a bi-directional UNIX pipe.  The unit number
;	used by the IDL for writing to and reading from this pipe is
;	allocated when the FORTRAN executable is 'spawned' (see code
;	below).  Pipe communications in the FORTRAN process are to
;	stdin and stdout, i.e streams 5 and 6.
;
;	The FORTRAN code is an independent process under the UNIX system.
;	The IDL process can only exert control over the FORTRAN in the
;	data which it communicates via the pipe.  The communications
;	between the IDL and FORTRAN must be exactly matched to avoid
;	input conversion errors.  The correct ammounts of data must be
;	passed so that neither process 'hangs' waiting for communications
;	which will never occur.
;
;	The FORTRAN code performs some error checking which is
;	independent of IDL.  In cases of error the FORTRAN may write
;	error messages.  To prevent these error messages from conflicting
;	with the pipe communications all FORTRAN errors are written to
;	output stream 0, which is stderr for UNIX.  These error messages
;	will appear in the window from which the ADAS session/IDL session
;	is being run.
;
;	In the case of severe errors the FORTRAN code may terminate
;	itself prematurely.  In order to detect this, and prevent the
;	IDL program from 'hanging' or crashing, the IDL checks to see
;	if the FORTRAN executable is still an active process before
;	each group of pipe communications.  The process identifier
;	for the FORTRAN process, PID, is recorded when the process is
;	first 'spawned'.  The system is then checked for the presence
;	of the FORTRAN PID.
;
; USE:
;	First the system settings must be established by calling
;	adas_sys_set.pro then adas502.pro is called to start the
;	ADAS502 application;
;
;	adas_sys_set, adasrel, fortdir, userroot, centroot, $
;		      devlist, devcode, font_large, font_small, edit_fonts
;	adas502,   adasrel, fortdir, userroot, centroot, devlist, $
;		   devcode, font_large, font_small, edit_fonts
;
; INPUTS:
;	ADASREL - A string indicating the ADAS system version, 
;		  e.g ' ADAS RELEASE: ADAS93 V1.1.0'.  The first
;		  character should be a space.
;
;	FORTDIR - A string holding the path to the directory where the
;		  FORTRAN executables are, e.g '/disk/adas/fortran/exec'
;
;	USERROOT - A string holding the path to the root directory of
;		   the user's adas data. e.g '/disk/bowen/adas'
;		   This root directory will be used by adas to construct
;		   other path names.  For example the users default data
;		   for adas502 should be in /disk/bowen/adas/adf04.  In
;		   particular the user's default interface settings will
;		   be stored in the directory USERROOT+'/defaults'.  An
;		   error will occur if the defaults directory does not
;		   exist.
;
;	CENTROOT - Like USERROOT, but this directory points to the
;		   central data area for the system.  User defaults are
;		   not stored on CENTROOT.
;
;	DEVLIST - A string array of hardcopy device names, used for
;		  graphical output. e.g ['Post-Script','HP-PCL','HP-GL']
;		  This array must mirror DEVCODE.  DEVCODE holds the
;		  actual device names used in a SET_PLOT statement.
;
;	DEVCODE - A string array of hardcopy device code names used in
;		  the SET_PLOT statement, e.g ['PS', 'PCL', 'HP']
;		  This array must mirror DEVLIST.
;
;	FONT_LARGE - The name of a larger font e.g 'courier_bold14'
;
;	FONT_SMALL - The name of a smaller font e.g 'courier_bold12'
;
;	EDIT_FONTS - A structure of two fonts used in the table editor
;		     adas_edtab.pro, {font_norm:'',font_input:''}.
;		     The two fonts are used to differentiate between
;		     editable and non-editable parts of the table. You
;		     may have to experiment to get two fonts which give
;		     the same default spacing in the edit widget.
;		     e.g {font_norm:'helvetica_bold14', $
;			  font_input:'helvetica_oblique14'}
;
; OPTIONAL INPUTS:
;	None
;
; OUTPUTS:
;	None - Note this routine should leave all inputs un-modified.
;
; OPTIONAL OUTPUTS:
;	None
;
; KEYWORD PARAMETERS:
;	None
;
; CALLS:
;	FIND_PROCESS	Checks to see if a given process is active.
;	XXDATE		Get date and time from operating system.
;	E2SPF0		Pipe comms with FORTRAN E2SPF0 routine.
;	E2SETP		Pipe comms with FORTRAN E2SETP routine.
;	E2ISPF		Pipe comms with FORTRAN E2ISPF routine.
;	E2SPF1		Pipe comms with FORTRAN E2SPF1 routine.
;	E2OUTG		Pipe comms with FORTRAN E2OUTG routine.
;
; SIDE EFFECTS:
;	This routine spawns a FORTRAN executable.  Note the pipe 
;	communications routines listed above.  In addition to these
;	pipe communications there is one explicit communication of the
;	date to the FORTRAN code, search for 'printf, pipe' to find it.
;
; CATEGORY:
;	Adas system.
;	
; WRITTEN:
;       Hugh Summers, University of Strathclyde, 27/11/1995
;
; VERSION:
;	1.1	Hugh Summers
;		First release			
;	1.2	Tim Hammond
;		Removed superfluous references to graph_out
;	1.3	Tim Hammond
;		Ensured that text output message is cleared when 'Menu'
;		button is used.
;	1.4	Tim Hammond
;		Updated version number to 1.2
;	1.5	William Osborn
;		Updated version number to 1.3
;	1.6	William Osborn
;		Updated version number to 1.4
;	1.7	William Osborn
;		Updated version number to 1.5
;	1.8	Richard Martin
;		Updated version number to 1.6
;	1.9	Richard Martin
;		Updated version number to 1.7
;	1.10	Richard Martin
;		Updated version number to 1.8
;
; MODIFIED:
;	1.1	27/11/95
;	1.2	13/11/95
;	1.3	06/02/96
;	1.4	16/02/96
;	1.5	13/05/96
;	1.6	12/07/96
;       1.7	14-10-96
;	1.8	14-10-99
;	1.9	21-03-00
;	1.10	18-03-02
;
;-
;-----------------------------------------------------------------------------


PRO ADAS502,	adasrel, fortdir, userroot, centroot, 			$
		devlist, devcode, font_large, font_small, edit_fonts

		;************************
		;**** Initialisation ****
		;************************

    adasprog = ' PROGRAM: ADAS502 V1.8'
    lpend = 0
    gomenu = 0
    deffile = userroot+'/defaults/adas502_defaults.dat'
    device = ''
    bitfile = centroot+'/bitmaps'

		;******************************************
		;**** Search for user default settings ****
		;**** If not found create defaults     ****
		;******************************************

    files = findfile(deffile)
    if files(0) eq deffile then begin
        restore,deffile
        inval.centroot = centroot+'/adf07/'
        inval.userroot = userroot+'/adf07/'
    endif else begin
        inval   = { 	ROOTPATH	:	userroot+'/adf07/',	$
			FILE		:		'', 		$
			CENTROOT	:	centroot+'/adf07/', 	$
			USERROOT	:	userroot+'/adf07/' 	}
        procval = {	NEW		:	-1			}
    	outval  = { 	GRPOUT		:	0, 			$
			GTIT1		:	'', 			$
			GRPSCAL		:	0, 			$
			XMIN		:	'',  			$
			XMAX		:	'',   			$
			YMIN		:	'',  			$
			YMAX		:	'',   			$
			HRDOUT		:	0, 			$
			HARDNAME	:	'', 			$
			GRPDEF		:	'', 			$
			GRPFMESS	:	'', 			$
			GRPSEL		:	-1, 			$
			GRPRMESS	:	'', 			$
			DEVSEL		:	-1, 			$
			GRSELMESS	:	'', 			$
			TEXOUT		:	0, 			$
			TEXAPP		:	-1, 			$
			TEXREP		:	0, 			$
			TEXDSN		:	'', 			$
			TEXDEF		:	'paper.txt',		$
			TEXMES		:	'' 			}
    endelse

		;****************************
		;**** Start fortran code ****
		;****************************

    spawn, fortdir+'/adas502.out', unit=pipe, /noshell, PID=pid

		;************************************************
		;**** Get date and write to fortran via pipe ****
		;************************************************

    date = xxdate()
    printf, pipe, date(0)

LABEL100:
		;**** Check FORTRAN still running ****
  
    if find_process(pid) eq 0 then goto, LABELEND

		;************************************************
		;**** Communicate with e2spf0 in fortran and ****
		;**** invoke user interface widget for       ****
		;**** Data file selection                    ****
		;************************************************

    e2spf0, pipe, inval, dsfull, rep, FONT=font_large

		;**** If cancel selected then end program ****

    if rep eq 'YES' then goto, LABELEND
  
		;**** Check FORTRAN still running ****

    if find_process(pid) eq 0 then goto, LABELEND

		;*************************************
		;**** Read pipe comms from e2setp ****
		;*************************************

    e2setp, pipe, nbsel


LABEL200:
		;**** Check FORTRAN still running ****

    if find_process(pid) eq 0 then goto, LABELEND

		;************************************************
		;**** Communicate with e2ispf in fortran and ****
		;**** invoke user interface widget for       ****
		;**** Processing options                     ****
		;************************************************

    e2ispf, pipe, lpend, procval, dsfull, nbsel, gomenu, bitfile,	$
	    FONT_LARGE=font_large, FONT_SMALL=font_small,        	$
	    EDIT_FONTS=edit_fonts

		;**** If menu button clicked, tell FORTRAN to stop ****

    if gomenu eq 1 then begin
 	printf, pipe, 1
 	goto, LABELEND
    endif else begin
 	printf, pipe, 0
    endelse


 		;**** If cancel selected then goto 100 ****

    if lpend eq 1 then goto, LABEL100

		;**** Fortran processing now in progress. ****
		;**** Create header for output.           ****

    header = adasrel + adasprog + ' DATE: ' + date(0) + ' TIME: ' + date(1)

		;**** Create header for output. ****

    if find_process(pid) eq 0 then goto, LABELEND

                
LABEL300:
		;**** Check FORTRAN still running ****

    if find_process(pid) eq 0 then goto, LABELEND

		;************************************************
		;**** Communicate with e2spf1 in fortran and ****
		;**** invoke user interface widget for       ****
		;**** Output options                         ****
		;************************************************

    e2spf1, pipe, lpend, outval, dsfull, header, bitfile, gomenu, 	$
            pid, DEVLIST=devlist, FONT=font_large

                ;**** Extra check to see whether FORTRAN is still there ****

    if find_process(pid) eq 0 then goto, LABELEND

		;**** If menu button clicked, tell FORTRAN to stop ****

    if gomenu eq 1 then begin
	printf, pipe, 1
        outval.texmes = ''
	goto, LABELEND
    endif else begin
	printf, pipe, 0
    endelse

		;**** If cancel selected then erase output ****
		;**** messages and goto 200.		   ****

    if lpend eq 1 then begin
       outval.texmes = ''
       goto, LABEL200
    endif

		;************************************************
		;**** If graphical output requested          ****
		;**** Communicate with e2outg in fortran and ****
		;**** invoke widget Graphical output         ****
		;************************************************

    if outval.grpout eq 1 then begin

		;**** User explicit scaling ****

        grpscal = outval.grpscal
        xmin = outval.xmin
        xmax = outval.xmax
        ymin = outval.ymin
        ymax = outval.ymax

		 ;**** Hardcopy output ****

        hrdout = outval.hrdout
        hardname = outval.hardname
        if outval.devsel ge 0 then device = devcode(outval.devsel)
 
		 ;*** user title for graph ***

       utitle = outval.gtit1
 
       e2outg, dsfull, pipe, utitle, grpscal, xmin, xmax, ymin, ymax, 	$
	       hrdout, hardname, device, header, bitfile, gomenu,	$
               FONT=font_large

		;**** If menu button clicked, tell FORTRAN to stop ****

        if gomenu eq 1 then begin
            printf, pipe, 1
            outval.texmes = ''
            goto, LABELEND
        endif else begin
            printf, pipe, 0
        endelse
    endif
	
		;**** Back for more output options ****

GOTO, LABEL300

LABELEND:

		;**** Save user defaults ****

     save, inval, procval, outval, filename=deffile

END
