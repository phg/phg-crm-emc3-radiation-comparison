; Copyright (c) 1995, Strathclyde University.
; SCCS info: Module @(#)$Header: /home/adascvs/idl/adas5xx/adas502/e2ispf.pro,v 1.4 2004/07/06 13:37:54 whitefor Exp $ Date $Date: 2004/07/06 13:37:54 $
;+
; PROJECT:
;       ADAS IBM MVS to UNIX conversion
;
; NAME:
;	E2ISPF
;
; PURPOSE:
;	IDL user interface and communications with ADAS502 FORTRAN
;	process via pipe.
;
; EXPLANATION:
;	The routine begins by reading information from the ADAS502
;	FORTRAN process via a UNIX pipe.  Then part of the ADAS502
;	IDL user interface is invoked to determine how the user
;	wishes to process the input dataset.  When the user's
;	interactions are complete the information gathered with
;	the user interface is written back to the FORTRAN process
;	via the pipe.  Communications are to the FORTRAN subroutine
;	E2ISPF.
;
; USE:
;	The use of this routine is specific to ADAS502, see adas502.pro.
;
; INPUTS:
;	PIPE	- The IDL unit number of the bi-directional pipe to the
;		  ADAS502 FORTRAN process.
;
;	LPEND	- Integer, 0 if the user pressed the 'Done' button
;		  or 1 if the user pressed 'Cancel'.  Maps directly onto
;		  the logical variable LPEND in ADAS502 FORTRAN.
;
;	PROCVAL	- A structure which determines the initial settings of
;		  the processing options widget.  The initial value is
;		  set in adas502.pro.  If adas502.pro passes a blank 
;		  dummy structure of the form {NEW:-1} into PROCVAL then
;		  PROCVAL is reset to a default structure.
;
;		  The PROCVAL structure is;
;   			 procval = {			$
;					new   : 0 ,             $
;					title : '',		$
;					nbsel : 0 ,		$
;					ibsel : 0 ,     	$
;                			ifout : 1, 		$
;					itval : 0,              $
;					tine  : temp_arr,	$
;					lfsel : 0,		$
;					tolval: 5,              $
;                			losel : 0               $
;
;		  See cw_adas502_proc.pro for a full description of this
;		  structure.
;	
;
;	DSFULL	- String; The full system file name of the input 
;		  dataset selected by the user for processing.
;	BITFILE - String; the path to the dirctory containing bitmaps
;		  for the 'escape to series menu' button.
;
; OPTIONAL INPUTS:
;	None
;
; OUTPUTS:
;       LPEND   - Integer, 0 if the user pressed the 'Done' button
;                 or 1 if the user pressed 'Cancel'.  Maps directly onto
;                 the logical variable LPEND in ADAS502 FORTRAN.
;	PROCVAL	- On output the structure records the final settings of
;		  the processing selections widget if the user pressed the
;		  'Done' button, otherwise it is not changed from input.
;	GOMENU	- Int; flag - set to 1 if user has selected 'escape direct
;		  to series menu , otherwise 0
;
; OPTIONAL OUTPUTS:
;	None
;
; KEYWORD PARAMETERS:
;	FONT_LARGE - The name of a larger font.
;
;	FONT_SMALL - The name of a smaller font.
;
;	EDIT_FONTS - A structure of two fonts used in the table editor
;		     adas_edtab.pro, {font_norm:'',font_input:''}.
;
; CALLS:
;	ADAS502_PROC	Invoke the IDL interface for ADAS502 data
;		 	processing options/input.
;
; SIDE EFFECTS:
;	Two way communications with ADAS502 FORTRAN via the
;	bi-directional UNIX pipe.
;
; CATEGORY:
;	Adas system.
;
; WRITTEN:
;       Hugh Summers, University of Strathclyde, 27/11/1995
;
; MODIFIED:
;	1.1	Hugh Summers
;		First version
;	1.2	Tim Hammond
;		Corrected minor syntax errors in reading of ita 
;		and other variables from pipe
;	1.3	Tim Hammond
;		Removed line which always set the chosen index in
;		the list (procval.ibsel) to 0.
;	1.4	Tim Hammond
;		Altered default definition of output temperatures
;		so that table is never empty.
;
; VERSION:
;	1.1	27-11-95
;	1.2	14-12-95
;	1.3	14-12-95
;	1.4	06-02-95
;
;-
;-----------------------------------------------------------------------------

PRO e2ispf, pipe, lpend, procval, dsfull, nbsel, gomenu, bitfile,	$
            FONT_LARGE=font_large, FONT_SMALL=font_small, EDIT_FONTS=edit_fonts

                ;**** Set defaults for keywords ****

    IF NOT (KEYWORD_SET(font_large)) THEN font_large = ''
    IF NOT (KEYWORD_SET(font_small)) THEN font_small = ''
    IF NOT (KEYWORD_SET(edit_fonts)) THEN 				$
    edit_fonts = {font_norm:'',font_input:''}

		;********************************************
		;****     Declare variables for input    ****
		;**** arrays wil be declared after sizes ****
		;**** have been read.                    ****
                ;********************************************
    lpend = 0
    nstore = 0
    ntdim = 0
    ndtin = 0
    nbsel = 0
    itval = 0
    input = 0

		;********************************
		;**** Read data from fortran ****
		;********************************

    readf, pipe, input
    nstore = input
    readf, pipe, input
    ntdim = input
    readf, pipe, input
    ndtin = input
    readf, pipe, input
    nbsel = input
  
		;******************************************
		;**** Now can define other array sizes ****
		;******************************************

    cicode = strarr(nbsel)
    cfcode = strarr(nbsel)
    ciion  = strarr(nbsel)
    cfion  = strarr(nbsel)
    bwno   = dblarr(nbsel)
    ita    = intarr(nbsel)
    tvals  = dblarr(ntdim, 3, nbsel)

		;****************************************************
		;*** Define some variables used to read arrays in ***
		;****************************************************

    temp = 0.0D
    itemp = 0
    sdum = ' '
  
		;********************************
                ;**** Read data from fortran ****
                ;********************************

    for i=0, nbsel-1 do begin
	readf, pipe, sdum
	cicode(i) = sdum
    endfor
    for i=0, nbsel-1 do begin
	readf, pipe, sdum
	cfcode(i) = sdum
    endfor
    for i=0, nbsel-1 do begin
	readf, pipe, sdum
	ciion(i) = sdum
    endfor
    for i=0, nbsel-1 do begin
	readf, pipe, sdum
	cfion(i) = sdum
    endfor
    for i=0, nbsel-1 do begin
	readf, pipe, sdum
	bwno(i) = sdum
    endfor
    for i=0, nbsel-1 do begin
	readf, pipe, itemp
	ita(i) = itemp
    endfor
    for k = 0, nbsel-1 do begin 
        for j =  0 , 2 do begin
	    for i = 0, ita(k)-1 do begin
                readf, pipe, temp
                tvals(i,j,k) = temp
            endfor
        endfor
    endfor

		;*******************************************
		;**** Set default value if non provided ****
		;*******************************************

    if (procval.new lt 0) then begin
        str_arr  = strarr(nbsel)
        bwno_arr = dblarr(nbsel)
        procval = {	new   	: 	0 ,             		$
			title 	: 	'',				$
			nbsel 	: 	0 ,				$
			ibsel 	: 	0 ,     			$
                	ifout 	: 	1, 				$
			itval 	: 	0,              		$
			tine  	: 	tvals(*,0,0),			$
			lfsel 	: 	0,				$
			tolval	: 	5,              		$
                	losel 	: 	0               		}
    endif 

		;****************************************
		;**** Pop-up processing input widget ****
		;****************************************

    adas502_proc, procval, dsfull, action, nstore, ntdim, ndtin, 	$
                  nbsel, cicode, cfcode, ciion, cfion, bwno,    	$ 
		  ita, tvals, bitfile, FONT_LARGE=font_large, 		$
                  FONT_SMALL=font_small, EDIT_FONTS=edit_fonts

                ;*********************************************
                ;**** Act on the output from the widget   ****
                ;**** There are three    possible actions ****
                ;**** 'Done', 'Cancel' and 'Menu'.        ****
                ;*********************************************

    if action eq 'Done' then begin
        lpend = 0
    endif else if action eq 'Menu' then begin
        lpend = 0
        gomenu = 1
    endif else begin
        lpend = 1
    endelse

		;*******************************
		;**** Write data to fortran ****
		;*******************************

    printf, pipe, lpend   
    printf, pipe, procval.title, format='(a40)' 
    printf, pipe, procval.ibsel
    printf, pipe, procval.ifout
    printf, pipe, procval.itval   
    for i = 0, procval.itval-1 do begin
        printf, pipe, procval.tine(i)
    endfor
    printf, pipe, procval.lfsel
    printf, pipe, procval.tolval
    printf, pipe, procval.losel

END
