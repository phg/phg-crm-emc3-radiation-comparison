; Copyright (c) 1995, Strathclyde University.
; SCCS info: Module @(#)$Header: /home/adascvs/idl/adas5xx/adas502/e2spf1.pro,v 1.1 2004/07/06 13:38:34 whitefor Exp $ Date $Date: 2004/07/06 13:38:34 $
;+
; PROJECT:
;       ADAS IBM MVS to UNIX conversion
;
; NAME:
;	E2SPF1
;
; PURPOSE:
;	IDL user interface and communications with ADAS502 FORTRAN
;	process via pipe.
;
; EXPLANATION:
;	First this routine reads some information from ADAS502 FORTRAN
;	via the UNIX pipe.  Next the 'Output Options' part of the
;	interface is invoked.  Finally the results of the user's
;	interactions are written back to the FORTRAN via the pipe.
;	Communications are with the FORTRAN subroutine E2SPF1.
;
; USE:
;	The use of this routine is specific to ADAS502, See adas502.pro.
;
; INPUTS:
;	PIPE	- The IDL unit number of the bi-directional pipe to the
;		  ADAS502 FORTRAN process.
;
;	DSFULL  - The name of the data set being analyzed.
;
;	VALUE	- A structure which determines the initial settings of
;		  the dataset selection widget.  The initial value is
;		  set in adas502.pro.  VALUE is passed un-modified
;		  through to cw_adas_in.pro, see that routine for a full
;		  description.
;
;	HEADER  - Header information used for text output.
;
; OPTIONAL INPUTS:
;	None.
;
; OUTPUTS:
;	LPEND	- Integer; indicates user action on leaving output
;		  selections.  lpend is set to 1 if the user exited
;		  with the 'Cancel' button and 0 if the user exited
;		  with 'Done'.
;
;	VALUE	- On output the structure records the final settings of
;		  the output selection widget if the user pressed the
;		  'Done' button otherwise it is not changed from input.
;
; OPTIONAL OUTPUTS:
;	None.
;
; KEYWORD PARAMETERS:
;	DEVLIST	- A string array;  Each element of the array is the
;		  name of a hardcopy graphics output device.  This
;		  list must mirror the DEVCODE list.  DEVLIST is the
;		  list which the user will see.
;		  e.g ['Post-Script','HP-PCL','HP-GL']
;
;	FONT	- Supplies the font to be used for the interface widgets.
;
; CALLS:
;	ADAS502_OUT	User interface - output options.
;
; SIDE EFFECTS:
;	This routine communicates with the ADAS502 FORTRAN process
;	via a UNIX pipe.
;
; CATEGORY:
;	Adas system.
;
; WRITTEN:
;       Hugh Summers, University of Strathclyde, 27/11/1995 
;
; MODIFIED:
;	1.1		Hugh Summers
;			First version
;
; VERSION:
;	1.1		27-11-95
;
;-
;-----------------------------------------------------------------------------

PRO e2spf1, pipe, lpend, value, dsfull, header, bitfile, gomenu,  	$
            pid, DEVLIST=devlist, FONT=font


                ;**** Set defaults for keywords ****

    IF NOT (KEYWORD_SET(font)) THEN font = ''

		;********************************
		;**** Read data from fortran ****
		;********************************

		;**************************************
		;**** Pop-up output options widget ****
		;**************************************

    adas502_out, value, dsfull, action, bitfile, DEVLIST=devlist, FONT=font

		;*********************************************
		;**** Act on the output from the widget   ****
		;**** There are three    possible actions ****
		;**** 'Done', 'Cancel' and 'Menu.         ****
		;*********************************************

    if action eq 'Done' then begin
        lpend = 0
    endif else if action eq 'Menu' then begin
	lpend = 0
	gomenu = 1
    endif else begin
        lpend = 1
    endelse

		;*******************************
		;**** Write data to fortran ****
		;*******************************

    printf, pipe, lpend

    if lpend eq 0 then begin

        printf, pipe, value.grpout
        if (value.grpout eq 1) then begin
	    printf, pipe, value.grpscal
	    if (value.grpscal eq 1) then begin
	        printf,pipe, value.xmin
	        printf,pipe, value.xmax
	        printf,pipe, value.ymin
	        printf,pipe, value.ymax
            endif 
        endif 

		;**** Extra check to see whether FORTRAN is still there ****

        if find_process(pid) eq 0 then return

		;*** If text output requested tell FORTRAN ****
     
         printf, pipe, value.texout
         if (value.texout eq 1) then begin
	     printf, pipe, value.texdsn
         endif

    endif

		;**************************************************
		;**** Set messages and settings ready for when ****
		;**** output window is re-displayed.           ****
		;**************************************************

    if lpend eq 0 then begin
        if value.texout eq 1 then begin
		;**** If text output is requested enable append ****
		;**** for next time and update the default to   ****
		;**** the current text file.                    ****
            value.texapp=0
            if value.texrep ge 0 then value.texrep = 0
            value.texmes = 'Output written to file.'
        endif
    endif

		;*************************************************
		;**** If text output has been requested write ****
		;**** header to pipe for XXADAS to read.      ****
		;*************************************************

   if (lpend eq 0 and value.texout eq 1) then begin
     printf, pipe, header
   endif

END
