; Copyright (c) 1995, Strathclyde University.
; SCCS info: Module @(#)$Header: /home/adascvs/idl/adas5xx/adas502/e2outg.pro,v 1.5 2004/07/06 13:38:17 whitefor Exp $ Date $Date: 2004/07/06 13:38:17 $
;+
; PROJECT:
;       ADAS IBM MVS to UNIX conversion
;
; NAME:
;	E2OUTG
;
; PURPOSE:
;	Communication with ADAS502 FORTRAN via UNIX pipe and
;	graphics output.
;
; EXPLANATION:
;	The routine begins by reading information from the ADAS502
;	FORTRAN process via a UNIX pipe.  Then the IDL graphical
;	output routine for ADAS502 is invoked.  Communications are to
;	the FORTRAN subroutine E2OUTG.
;
; USE:
;	The use of this routine is specific to ADAS502 see adas502.pro.
;
; INPUTS:
;	DSFULL   - Data file name
;	PIPE	 - The IDL unit number of the bi-directional pipe to the
;		   ADAS502 FORTRAN process.
;
;	UTITLE   - Optional comment by user
;
;	GRPSCAL	 - Integer; 0 if default scaling required 1 if user
;		   specified scaling to be used.
;
;	XMIN	 - String; User sepcified x-axis minimum, number as string.
;
;	XMAX	 - String; User sepcified x-axis maximum, number as string.
;
;	YMIN	 - String; User specified y-axis minimum, number as string.
;
;	YMAX	 - String; User specified y-axis maximum, number as string.
;
;	HRDOUT	 - Integer; 0 if no hardcopy required, 1 if hardcopy.
;
;	HARDNAME - String; file name for hardcopy output.
;
;	DEVICE	 - String; IDL name for hardcopy output device.
;
;	HEADER	 - ADAS version header information for inclusion in the
;		   graphical output.
;
; OPTIONAL INPUTS:
;	None.
;
; OUTPUTS:
;	None.
;
; OPTIONAL OUTPUTS:
;	None.
;
; KEYWORD PARAMETERS:
;	FONT	- String; Name of the font to be used for the graphical
;		  output widget.
;
; CALLS:
;	ADAS502_PLOT	ADAS502 graphical output.
;
; SIDE EFFECTS:
;	This routine reads information from FORTRAN via a UNIX pipe.
;
; CATEGORY:
;	Adas system.
;
; WRITTEN:
;       Hugh Summers, University of Strathclyde,  04/12/1995
;
; MODIFIED:
;	1.1	Hugh Summers				
;		First version
;	1.2	Tim Hammond
;		Minor tidying to code and altered pipe reading so that
;		it always goes into dummy variables.
;	1.3	Tim Hammond
;		Put in default value for nmx for when polynomial fit
;		not required.
;	1.4	Tim Hammond
;		Put in checks for numbers being too small when read from
;		FORTRAN - if so zero is substituted.
;       1.5     William Osborn
;               S.C.C.S. mistake
;
; VERSION:
;	1.1	04/12/95
;	1.2	13/12/95
;	1.3	29/01/96
;	1.4	05/08/96
;	1.5	04/10/96
;
;-
;-----------------------------------------------------------------------------

PRO e2outg, dsfull, pipe, utitle, grpscal, xmin, xmax, ymin, ymax,     $
	    hrdout, hardname, device, header, bitfile, gomenu, FONT=font


                ;**** Set defaults for keywords ****


    IF NOT (KEYWORD_SET(font)) THEN font = ''


		;**** Declare variables for input ****

    sdum = " "
    idum = 0
    fdum = 0.0d
    strg = make_array(9, /string, value=" ")
    nmx = 0

		;********************************
		;**** Read data from fortran ****
		;********************************

    readf, pipe, format = '(a40)' , sdum
    title = sdum
    readf, pipe, format = '(a120)', sdum
    titlx = sdum
    readf, pipe, format = '(a80)' , sdum
    titlm = sdum
    readf, pipe, format = '(a8)' , sdum
    date = sdum
    readf, pipe, format = '(a2)' , sdum
    esym = sdum
    readf, pipe, idum
    iz0 = idum
    readf, pipe, idum
    iz1 = idum
    readf, pipe, idum
    bwno = idum
    readf, pipe, format = '(a5)' , sdum
    ciion = sdum
    readf, pipe, format = '(a2)' , sdum
    cicode = sdum
    readf, pipe, format = '(a5)' , sdum
    cfion = sdum
    readf, pipe, format = '(a2)' , sdum
    cfcode = sdum
    readf, pipe, idum
    itval = idum

		;**** now declare array dimensions ****

    teva = dblarr(itval) 
    szda = dblarr(itval) 
    for i=0, itval-1 do begin
        readf, pipe, fdum
	if (fdum lt 1d-36) then fdum=0d0
        teva(i) = fdum 
    endfor
    for i=0, itval-1 do begin
        readf, pipe, fdum
	if (fdum lt 1d-36) then fdum=0d0
        szda(i) = fdum 
    endfor
    readf, pipe, idum
    ldef1 = idum
    if (ldef1 eq 1) then begin
        readf, pipe, fdum
	xmin = fdum
        readf, pipe, fdum
	xmax = fdum
        readf, pipe, fdum
	ymin = fdum
        readf, pipe, fdum
	ymax = fdum
    endif 
    readf, pipe, idum
    lfsel = idum
    if (lfsel eq 1) then begin
        readf, pipe, idum
        nmx = idum
 		;**** declare arrays ****
        szdm = fltarr(nmx)
        tfitm = fltarr(nmx)
        for i = 0, nmx-1 do begin
	    readf, pipe, fdum
	    if (fdum lt 1d-36) then fdum=0d0
            szdm(i) = fdum 
        endfor
        for i = 0, nmx-1 do begin
	    readf, pipe, fdum 
            tfitm(i) = fdum
        endfor
    endif
    for i = 0, 5 do begin
        readf, pipe, sdum 
        strg(i) = sdum
    endfor
    readf, pipe, sdum, format = '(a32)'
    head1 = sdum
    readf, pipe, sdum, format = '(a16)'
    head2 = sdum
    readf, pipe, sdum, format = '(a16)'
    head3 = sdum
    readf, pipe, sdum, format = '(a24)'
    head4 = sdum

		;***********************
		;**** Plot the data ****
		;***********************

    adas502_plot, dsfull, 						$
  		  title , titlx, titlm , utitle,date, esym, iz0, iz1, 	$
  		  bwno  , ciion, cicode, cfion, cfcode, itval,        	$
		  teva  , szda , szdm  , tfitm,                       	$
                  ldef1 , xmin , xmax  , ymin , ymax,                 	$
   		  lfsel , nmx  ,                                      	$
   		  strg  , head1, head2, head3, head4,                 	$
		  hrdout, hardname, device, header, bitfile, gomenu,  	$
                  FONT=font

END
