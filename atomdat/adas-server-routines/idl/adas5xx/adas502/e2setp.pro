; Copyright (c) 1995, Strathclyde University.
; SCCS info: Module @(#)$Header: /home/adascvs/idl/adas5xx/adas502/e2setp.pro,v 1.1 2004/07/06 13:38:21 whitefor Exp $ Date $Date: 2004/07/06 13:38:21 $
;+
; PROJECT:
;       ADAS IBM MVS to UNIX conversion
;
; NAME:
;	E2SETP
;
; PURPOSE:
;	IDL communications with ADAS502 FORTRAN process via pipe.
;
; EXPLANATION:
;	This IDL routine reads data from the ADAS502 FORTRAN subroutine
;	E2SETP via a UNIX pipe.  In the original IBM version the FORTRAN
;	subroutine E2SETP put information needed by the ISPF interface
;	into the ISPF pool area for later use.  Now the same information
;	is written to IDL for use in its user unterface.
;
; USE:
;	The use of this routine is specific to ADAS502.  See
;	adas502.pro for an example.
;
; INPUTS:
;	PIPE	- The IDL unit number of the bi-directional pipe to the
;		  ADAS502 FORTRAN process.
;
; OPTIONAL INPUTS:
;	None
;
; OUTPUTS:
;       NBSEL        - Number of ionising ion combinations read in.
;
; OPTIONAL OUTPUTS:
;	None
;
; KEYWORD PARAMETERS:
;	None
;
; CALLS:
;	None
;
; SIDE EFFECTS:
;	Reads data from a pipe.
;
; CATEGORY:
;	Adas system.
;
; WRITTEN:
;       Hugh Summers, University of Strathclyde, 27/11/1995
;
; MODIFIED:
;	1.1		Hugh Summers
;			First version
;
; VERSION:
;	1.1		27-11-95
;
;-
;-----------------------------------------------------------------------------

PRO e2setp, pipe, nbsel

		;**********************************
		;**** Initialise new variables ****
		;**********************************

    idum  = 0

		;********************************
		;**** Read data from fortran ****
		;********************************

    readf, pipe, idum
    nbsel = idum

END
