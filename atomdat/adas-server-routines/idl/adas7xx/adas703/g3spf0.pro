; Copyright (c) 1998 Strathclyde University .
; SCCS info: Module @(#)$Header: /home/adascvs/idl/adas7xx/adas703/g3spf0.pro,v 1.1 2004/07/06 13:57:02 whitefor Exp $ Date $Date: 2004/07/06 13:57:02 $
;+
; PROJECT:
;       ADAS IBM MVS to   UNIX conversion
;
; NAME:
;	G3SPF0
;
; PURPOSE:
;	IDL user interface and communications with ADAS703 FORTRAN
;	process via pipe.
;
; EXPLANATION:
;	Firstly this routine invokes the part of the user interface
;	used to select the input dataset for ADAS703.  When the user's
;	interactions are complete this routine communicates with the
;	ADAS703 FORTRAN application via a UNIX pipe.  Communications
;	are to the FORTRAN subroutine G3SPF0.
;
; USE:
;	The use of this routine is specific to ADAS703, see adas703.pro.
;
; INPUTS:
;	PIPE	- The IDL unit number of the bi-directional pipe to the
;		  ADAS703 FORTRAN process.
;
;	VALUE	- A structure which determines the initial settings of
;		  the dataset selection widget.  The initial value is
;		  set in adas703.pro.  VALUE is passed un-modified
;		  through to cw_adas_in.pro, see that routine for a full
;		  description.
;
; OPTIONAL INPUTS:
;	None
;
; OUTPUTS:
;	VALUE	- On output the structure records the final settings of
;		  the dataset selection widget if the user pressed the
;		  'Done' button otherwise it is not changed from input.
;
;	REP	- String; Indicates whether the user pressed the 'Done'
;		  or 'Cancel' button on the interface.  The action is
;		  converted to the strings 'NO' and 'YES' respectively
;		  to match up with the existing FORTRAN code.  In the
;		  original IBM ISPF interface REP was the reply to the
;		  question 'DO YOU WANT TO END PROGRAM EXECUTION'.
;       DSNIC1  - String; The first input data set name.
;       DSNIC2  - String; The second input data set name.
;
; OPTIONAL OUTPUTS:
;	None
;
; KEYWORD PARAMETERS:
;    FONT_LARGE - Supplies the large font to be used for the interface widgets.
;    FONT_SMALL - Supplies the small font to be used for the interface widgets.
;
; CALLS:
;	ADAS703_IN		Pops-up the dataset selection widget.
;
; SIDE EFFECTS:
;	This routine communicates with the ADAS703 FORTRAN process
;	via a UNIX pipe.
;
; CATEGORY:
;	Adas system.
;
; WRITTEN:
;       Martin O'Mullane
;
; MODIFIED:
;	1.1	Martin O'Mullane
;
; VERSION:
;       1.1	28-04-98
; 
;-
;-----------------------------------------------------------------------------

PRO g3spf0, pipe, value, rep, dsnic1, dsnic2,				$
		FONT_LARGE=font_large, FONT_SMALL=font_small

		;***********************************
                ;**** Set defaults for keywords ****
		;***********************************

    IF NOT (KEYWORD_SET(font_large)) THEN font_large = ''
    IF NOT (KEYWORD_SET(font_small)) THEN font_small = ''


		;**********************************
		;**** Pop-up input file widget ****
		;**********************************


    adas703_in, value, action, WINTITLE='ADAS 703 INPUT', 		$
	    FONT_LARGE=font_large, FONT_SMALL=font_small

		;********************************************
		;**** Act on the event from the widget   ****
		;**** There are only two possible events ****
		;**** 'Done' and 'Cancel'.               ****
		;********************************************

    if action eq 'Done' then begin
        rep = 'NO'
    endif else begin
        rep = 'YES'
    endelse

		;********************************
		;**** Construct dataset name ****
		;********************************

    dsnic1= value.erootpath + value.efile
    dsnic2= value.crootpath + value.cfile


		;*******************************
		;**** Write data to fortran ****
		;*******************************

    printf, pipe, rep
    printf, pipe, dsnic1
    printf, pipe, dsnic2

END
