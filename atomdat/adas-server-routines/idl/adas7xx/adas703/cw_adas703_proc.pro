; Copyright (c) 1998 Strathclyde University .
; SCCS info: Module @(#)$Header: /home/adascvs/idl/adas7xx/adas703/cw_adas703_proc.pro,v 1.1 2004/07/06 12:55:32 whitefor Exp $ Date $Date: 2004/07/06 12:55:32 $
;+
; PROJECT:
;       ADAS IBM MVS to UNIX conversion
;
; NAME:
;	CW_ADAS703_PROC()
;
; PURPOSE:
;	Produces a widget for ADAS703 processing options/input.
;
; EXPLANATION:
;	This function creates a compound widget consisting of :-
;	   a text widget holding an editable 'Run title', 
;	   a table widget for temperature data cw_adas_table,
;	   buttons to enter default values into the table, 
;	   a message widget, 
;          an 'Escape to series menu', a 'Cancel' and a 'Done' button.
;
;	The compound widgets included in this widget are self managing.
;	This widget only manages events from the two 'Defaults' buttons,
;	the 'Cancel' button and the 'Done' button and the
;       'escape to series menu' button.
;
; USE:
;	This widget is specific to ADAS703, see adas703_proc.pro
;	for use.
;
; INPUTS:
;	TOPPARENT- Long integer, ID of parent widget.
;
;	NDTIN	- Integer; number of temperatures allowed
;
;	TVALS   - Float array; temperature values from data file
;				dimension - NDTIM
;
;
;	The inputs map exactly onto variables of the same
;	name in the ADAS703 FORTRAN program.
;
;       BITFILE - String; the path to the directory containing bitmaps
;                 for the 'escape to series menu' button.
;
; OPTIONAL INPUTS:
;	None
;
; OUTPUTS:
;	The return value of this function is the ID of the declared widget.
;
; OPTIONAL OUTPUTS:
;	None
;
; KEYWORD PARAMETERS:
;	PROCVAL	- A structure which determines the initial settings of
;		  the processing options widget.
;
;		  The default PROCVAL is created thus;
;
;    		str_arr = strarr(nbsel)
;	     	temp_arr = fltarr(ntdim)
;     		dens_arr = fltarr(nddim)
;     		ps = {proc703_set, 		$
;			nmet  : 0,		$
;                	title : '',             $
;			ifout : 1 ,             $
;			maxt  : 0, 		$
;                	tin   : temp_arr        }
;
;
;		NMET    Flag which defines whether or not default values
;			exist or not. (< 0 if no values exist)
; 		TITLE	Entered general title for program run
;		IFOUT   Index indicating which units are being used
;		MAXT    Number of temperature values selected
;		TIN     User supplied temperature values for fit.
;		All of these structure elements map onto variables of
;		the same name in the ADAS703 FORTRAN program.
;
;
;	UVALUE	- A user value for the widget. Default 0.
;
;	FONT_LARGE - The name of a larger font.  Default current system
;		     font
;
;	FONT_SMALL - The name of a smaller font. Default current system
;		     font.
;
;	EDIT_FONTS - A structure of two fonts used in the table editor
;		     adas_edtab.pro, {font_norm:'',font_input:''}.  Defaults
;		     to current system font.
;
;	NUM_FORM   - String; Numeric format to use in tables.  Default
;			'(E10.3)'
;		     
;
; CALLS:
;	POPUP		Popup warning window with buttons.
;	CW_ADAS_DSBR	Dataset name and comments browsing button.
;	CW_ADAS_TABLE	Adas data table widget.
;	CW_ADAS_SEL	Adas multiple selection widget.
;	CW_SINGLE_SEL	Adas scrolling table and selection widget.
;	CW_OPT_VALUE    Adas option selection widget with required input 
;			value. 
;
;	See side effects for other related widget management routines.
;
; SIDE EFFECTS:
;
;	The following widget management routines are included in this file;
;	PROC703_GET_VAL()	Returns the current PROCVAL structure.
;	PROC703_EVENT()		Process and issue events.
;	
; CATEGORY:
;	Compound Widget
;
; WRITTEN:
;       Martin O'Mullane
;
; MODIFIED:
;	1.1	Martin O'Mullane
;		First release 
;
; VERSION:
;       1.1	29-04-98
; 
;-
;-----------------------------------------------------------------------------

FUNCTION proc703_get_val, id

                ;**** Return to caller on error ****

    ON_ERROR, 2

		 ;***************************************
                 ;****     Retrieve the state 	     ****
		 ;**** Get first_child widget id     ****
		 ;**** because state is stored there ****
		 ;***************************************

    first_child = widget_info(id, /child)
    widget_control, first_child, get_uvalue = state

		;***********************************
		;**** Get run title from widget ****
		;**** Then centre in in string  ****
		;**** of 40 characters 		****
		;***********************************

    widget_control, state.runid, get_value=title
    title_len = strlen(title(0)) 
    if (title_len gt 40 ) then begin 
	title = strmid(title,0,38)
        widget_control, state.messid, set_value="Title too long - truncated "
        widget_control, state.runid, set_value=title
	wait, 1
    endif
    pad = (40 - title_len)/2 
    spaces = '                                         '
    title(0) = strmid(spaces,0,(pad-1)) + title(0) + strmid(spaces,0,(pad-1))
  

		;***************************************
		;**** Get new ionisation potentials ****
		;***************************************

    bwno = dblarr(state.ndp)
 
    for i = 0, state.np-1 do begin
      widget_control, state.ipselid(i), get_value=a_temp_value
      if (num_chk(a_temp_value(0)) eq 0) then begin
          bwno(i) = float(a_temp_value(0))
      endif else begin
          bwno(i) = -99.0
      endelse
      widget_control, state.ipselid(i), set_value=a_temp_value
    endfor


                ;*******************************
                ;**** Get energy level type ****
                ;*******************************

    widget_control, state.yesid, get_uvalue=temp
    if (temp(0) eq 1 ) then iopt = 0 
    widget_control, state.noid, get_uvalue=temp
    if (temp(0) eq 1 ) then iopt = 1 
    
    
		;****************************************************
		;**** Get new temperature data from table widget ****
		;****************************************************

    widget_control, state.tempid, get_value=tempval
    tabledata = tempval.value
    ifout = tempval.units + 1

		;**** Copy out temperature values ****

    tin = dblarr(state.ndtin)
    blanks = where(strtrim(tabledata(0,*),2) eq '')

                ;***********************************************
		;**** next line assumes that all blanks are ****
		;**** at the end of the columns             ****
                ;***********************************************

    if blanks(0) ge 0 then maxt=blanks(0) else maxt=state.ndtin
                ;*************************************************
                ;**** Only perform following if there is 1 or ****
                ;**** more value present in the table         ****
                ;*************************************************
  
    if (maxt ge 1) then begin
  	tin(0:maxt-1) = double(tabledata(0,0:maxt-1))
    endif

		;**** Fill in the rest with zero ****

    if maxt lt state.ndtin then begin
        tin(maxt:state.ndtin-1) = double(0.0)
    endif


		;***********************************************
		;**** write selected values to PS structure ****
		;***********************************************

     ps = {  	nmet  : 0, 						$
                title : title,          				$
                bwno  : bwno,                              		$
		iopt  : iopt,						$
		ifout : ifout,						$
		maxt  : maxt,						$
                tin   : tin       					}
   
    widget_control, first_child, set_uvalue=state, /no_copy

    RETURN, ps

END

;-----------------------------------------------------------------------------

FUNCTION proc703_event, event

                ;**** Base ID of compound widget ****

    parent = event.handler

		;**********************************************
                ;**** Retrieve the user value state        ****
		;**** Get id of first_child widget because ****
		;**** user value "state" is stored there   ****
		;**********************************************

    first_child = widget_info(parent, /child)
    widget_control, first_child, get_uvalue=state,/no_copy

		;*********************************
		;**** Clear previous messages ****
		;*********************************

    widget_control, state.messid, set_value=' '

                ;************************
                ;**** Process events ****
                ;************************

;	select focus for ion pot enteries    

    for i = 0, state.np-1 do begin
      if event.id eq state.ipselid(i) then begin
         if i+1 gt state.np-1 then iselect = 0 else iselect = i+1
         widget_control, state.ipselid(iselect),/input_focus
      endif
    endfor             

    CASE event.id OF

        state.yesid: begin
           widget_control, state.yesid, set_uvalue = 1
           widget_control, state.noid,  set_uvalue = 0
           state.iopt = 0
        end     
       
        state.noid: begin
           widget_control, state.yesid, set_uvalue = 0
           widget_control, state.noid,  set_uvalue = 1
           state.iopt = 1
        end     
        
		;*************************************
		;**** Default temperature button ****
		;*************************************

    	state.deftid: begin

		;**** popup window to confirm overwriting current values ****

	    action= popup(message='Confirm Overwrite values with Defaults',$
			  buttons=['Confirm','Cancel'], font=state.font)
	    if action eq 'Confirm' then begin

		;**** Get current table widget value ****

 	   	widget_control,state.tempid,get_value=tempval
   
                ;***********************************************
		;**** Copy defaults into value structure    ****
		;**** For default values use the existing   ****
		;**** table values and let user modify them ****
		;**** then set all density values to same   ****
		;**** value  				    ****
                ;***********************************************

             	deft = tempval
             	units = tempval.units+1
	     	maxt = state.maxt
 	     	if maxt gt 0 then begin
   	           tempval.value(0,0:maxt-1) = 				$
		   strtrim(string(deft.value(units,0:maxt-1), 		$
                   format=state.num_form),2)
 	  	endif

		;**** Fill in the rest with blanks ****

 	  	if maxt lt state.ndtin then begin
   	            tempval.value(0,maxt:state.ndtin-1) = ''
 	  	endif

		;**** Copy new data to table widget ****

 	  	widget_control,state.tempid, set_value=tempval
	    endif
        end

		;***********************
		;**** Cancel button ****
		;***********************

    	state.cancelid: new_event = {ID:parent, TOP:event.top, 		$
				     HANDLER:0L, ACTION:'Cancel'}

		;*********************
		;**** Done button ****
		;*********************

    	state.doneid: begin

		;***************************************
		;**** Check all user input is legal ****
		;***************************************

	    error = 0

        	;********************************************
		;*** Have to restore "state" before calls ***
		;*** to proc703_get_val so it can be used ***
		;*** there.				  ***
		;********************************************
		
	    widget_control, first_child, set_uvalue=state, /no_copy
	    widget_control, event.handler, get_value=ps

		;*** reset state variable ***

	    widget_control, first_child, get_uvalue=state, /no_copy

		;**** check temp/density values entered ****

            if error eq 0 then begin
                widget_control, state.tempid, get_value=tabval
                tabvals =                                               $
                where(strcompress(tabval.value(0,*), /remove_all) ne '')
                if tabvals(0) eq -1 then begin
                    error = 1
                    message = '**** Error: No temperatures'+  $
                              ' entered ****'
                endif
            endif


                ;**************************************************
                ;**** Check to see if sensible ion. potentials ****
                ;**************************************************

	    for j=0, state.np-1 do begin 
              if (error eq 0 and (ps.bwno(j) le 0.0) ) then begin
                  error = 1
                  message='Error: Invalid Ionisation Potential'
              endif
            endfor


		;**** return value or flag error ****

	    if error eq 0 then begin
	        new_event = {ID:parent, TOP:event.top, 			$
                             HANDLER:0L, ACTION:'Done'}
            endif else begin
	        widget_control, state.messid, set_value=message
	        new_event = 0L
            endelse
        end

                ;**** Menu button ****

        state.outid: begin
            new_event = {ID:parent, TOP:event.top, HANDLER:0L,          $
                         ACTION:'Menu'}
        end

    ELSE: new_event = 0L

  ENDCASE

		;*********************************************
		;*** make "state" available to other files ***
		;*********************************************

    widget_control, first_child, set_uvalue=state, /no_copy

    RETURN, new_event

END

;-----------------------------------------------------------------------------

function cw_adas703_proc, topparent,  					$
                          ndtin, tvals, ndp, np, ipstr,			$	 
                          bitfile, 					$
                          procval=procval, uvalue=uvalue, 		$
                          font_large=font_large, font_small=font_small, $
                          edit_fonts=edit_fonts, num_form=num_form

		;**** Set defaults for keywords ****

    if not (keyword_set(uvalue)) then uvalue = 0
    if not (keyword_set(font_large)) then font_large = ''
    if not (keyword_set(font_small)) then font_small = ''
    if not (keyword_set(edit_fonts)) then 				$
    edit_fonts = {font_norm:'',font_input:''}
    if not (keyword_set(num_form)) then num_form = '(E10.3)'
    if not (keyword_set(procval)) then begin
         temp1_arr = fltarr(ndp)
         temp2_arr = fltarr(ndtin)
         str_arr   = fltarr(ndp)
         ps = { nmet  : 0, 						$
		title : '',             				$
                bwno  :	temp1_arr,					$
		iopt  : 0,						$
		ifout : 1,						$
		maxt  : 0,						$
                tin   : temp2_arr        				}
    endif else begin
	ps = {  nmet  : procval.nmet,      				$
		title : procval.title,     				$
		bwno  : procval.bwno,	   				$
		iopt  : procval.iopt,	   				$
		ifout : procval.ifout,	   				$
		maxt  : procval.maxt,	   				$
                tin   : procval.tin        				}
    endelse

                ;*********************************************************
                ;**** Modify certain parameters and results depending ****
                ;**** on the machine being used                       ****
                ;*********************************************************

    machine = GETENV('TARGET_MACHINE')
    if machine eq 'HPUX' then begin
        y_size = 4
        large_font = font_small
    endif else begin
        y_size = 6
        large_font = font_large
    endelse

		;****************************************************
		;**** Assemble temperature table data            ****
		;****************************************************
		;**** The adas table widget requires data to be *****
		;**** input as strings, so all the numeric data *****
		;**** has to be written into a string array.    *****
		;**** Declare temperature table array           *****
		;**** col 1 has user temperature values         *****
		;**** col 2 has temperature values from files,  *****
		;**** which can have one of three possible units*****
		;****************************************************

    tabledata = strarr(4,ndtin)

                ;***********************************************
		;**** Copy out temperature & density values ****
		;**** number of temperature and density     ****
		;**** values for this data block            ****
                ;***********************************************

    maxt = ndtin    ; by definition all our defaults are ndtin 
    if (maxt gt 0) then begin
        tabledata(0,*) = 						$
	strtrim(string(ps.tin(*),format=num_form),2)
        tabledata(1,0:maxt-1) = 					$
        strtrim(string(tvals(0:maxt-1,0),format=num_form),2)
        tabledata(2,0:maxt-1) =                               		$
	strtrim(string(tvals(0:maxt-1,1),format=num_form),2)
        tabledata(3,0:maxt-1) =                                		$
        strtrim(string(tvals(0:maxt-1,2),format=num_form),2)

		;**** fill rest of table with blanks ****

        blanks = where(ps.tin eq 0.0, count) 
        if count ne 0 then tabledata(0,blanks) = ' ' 
        if (maxt ne ndtin) then tabledata(1:3,maxt:ndtin-1) = ' '
    endif

		;********************************************************
		;**** Create the 703 Processing options/input window ****
		;********************************************************

		;**** create titled base widget ****

    parent = widget_base(topparent, UVALUE = uvalue, 			$
			 title = 'ADAS703 PROCESSING OPTIONS', 		$
			 EVENT_FUNC = "proc703_event", 			$
			 FUNC_GET_VALUE = "proc703_get_val", 		$
			 /COLUMN)

		;******************************************************
		;**** Create a dummy widget just to hold value of *****
		;**** "state" variable so as not to get confused  *****
		;**** with any other values. Adopt IDL practice   *****
		;**** of using first child widget                 *****
		;******************************************************

    first_child = widget_base(parent)
    topbase = widget_base(first_child,/column)

		;***********************
		;**** add run title ****
		;***********************

    base = widget_base(topbase, /row)
    rc = widget_label(base, value='Title for Run', font=large_font)
    runid = widget_text(base, value=ps.title, xsize=38, font=large_font, /edit)



		;*******************************
		;**** Base for IP selection ****
		;*******************************

    ipselbase = widget_base(parent, /column,/frame)
    ipseltit = widget_label(ipselbase, font=large_font,                    $
                            value = "Input Ionisation Potentials for Parent :-")
    
    list1id  = lonarr(np)
    list2id  = lonarr(np)
    ipselid  = lonarr(np)
    for j = 0,(np)-1 do begin
      
      ; base for ip of j'th parent
            
      list1id(j) = widget_base(ipselbase, /row)
      list2id(j) = widget_label(list1id(j), font=font_small,   $
                               value = ipstr(j)+':')
      if (ps.bwno(j) ne 0.0) then begin
          fval = string (ps.bwno(j), format='(d13.3)')
      endif else begin
          fval = "             "
      endelse
      ipselid(j) = widget_text(list1id(j), xsize=13, /editable,    $
                                value = fval, font=large_font)
            
    endfor  


                ;***************************************************
                ;**** Add the Eisner energy level type selector ****
                ;***************************************************

  wrow = widget_base(parent, /row, /frame)
  asktext = widget_label(wrow,value = 'Eisner style energy levels :', $
                        font=large_font)

  wr_but = widget_base(wrow, /row, /exclusive) 
  yesid = widget_button(wr_but, value='Yes', font=font_large,$
                          /no_release)
  noid  = widget_button(wr_but, value='No', $
                          font=font_large, /no_release)


		;************************************************
		;**** base for the table and defaults button ****
		;************************************************

    tablebase = widget_base(parent, /row)

    base = widget_base(tablebase, /column, /frame)

		;********************************
		;**** temperature data table ****
		;********************************

    colhead = [['Output','Input'],['','']]

		;********************************************
		;**** convert FORTRAN index to IDL index ****
		;********************************************

    units = ps.ifout - 1 
    unitname = ['Kelvin', 'eV', 'Reduced']

                ;***********************************************************
		;**** Four columns in the table and three sets of units.****
		;**** Column 2 has the same values for all three        ****
		;**** units but column 2 switches between sets 2,3 & 4  ****
		;**** in the input array tabledata as the units change. ****
                ;***********************************************************

    unitind = [[0,0,0],[1,2,3]]
    table_title = [ ["Temperature  Values"], 			$
		  [  "      "]]

		;****************************
		;**** table of data   *******
		;****************************

    tempid = cw_adas_table(base, tabledata, units, unitname, unitind, 	$
			   UNITSTITLE = 'Temperature Units', 		$
			   COLEDIT = [1,0], COLHEAD = colhead,	 	$
			   TITLE = table_title, 			$
			   ORDER = [1,0], 				$ 
			   LIMITS = [1,1], CELLSIZE = 12, 		$
			   /SCROLL, ytexsize=y_size, NUM_FORM=num_form, $
			   FONTS = edit_fonts, FONT_LARGE = large_font, $
			   FONT_SMALL = font_small)

		;*************************
		;**** Default buttons ****
		;*************************

    tbase = widget_base(base, /column)
    deftid = widget_button(tbase, value=' Default Temperature Values  ',$
		           font=large_font)


		;**** Error message ****

    messid = widget_label(parent, font=large_font, 			$
    value='Edit the processing options data and press Done to proceed')

		;**** add the exit buttons ****

    base = widget_base(parent, /row)
    menufile = bitfile + '/menu.bmp'
    read_X11_bitmap, menufile, bitmap1
    outid = widget_button(base, value=bitmap1)          ;menu button
    cancelid = widget_button(base, value='Cancel', font=large_font)
    doneid = widget_button(base, value='Done', font=large_font)
  

                ;**************************************************
                ;**** set Eisner options to a determined state ****
                ;**************************************************

    case ps.iopt of
       0 : begin
              widget_control, yesid, set_uvalue = 1
              widget_control, noid,  set_uvalue = 0
              widget_control, yesid, set_button = 1
              widget_control, noid,  set_button = 0
           end
       1 : begin
              widget_control, yesid, set_uvalue = 0
              widget_control, noid,  set_uvalue = 1
              widget_control, yesid, set_button = 0
              widget_control, noid,  set_button = 1
           end
    endcase
           
           
           
                ;*************************************************
		;**** create a state structure for the pop-up ****
		;**** 		     window.                  ****
                ;*************************************************

    new_state = { 	runid	:	runid,  			$
			messid	:	messid, 			$
			deftid	:	deftid,				$
			tempid	:	tempid, 			$
			cancelid:	cancelid,			$
			doneid	:	doneid, 			$
                  	outid   :       outid,                          $
                  	ipselid :       ipselid,                        $
                  	yesid   :       yesid,                          $
                  	noid    :       noid,                           $
                        iopt    :       ps.iopt,                        $
                        ndp	:	ndp,				$
                        np	:	np,				$
                        bwno	:	ps.bwno,			$
			ndtin	:	ndtin,				$
			maxt	:	maxt, 				$
			tvals	:	tvals,				$
			font	:	large_font,			$
			num_form:	num_form			}

                 ;**** Save initial state structure ****

    widget_control, first_child, set_uvalue=new_state, /no_copy

    RETURN, parent

END

