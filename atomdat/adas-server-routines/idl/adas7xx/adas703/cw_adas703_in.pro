; Copyright (c) 1998 Strathclyde University .
; SCCS info: Module @(#)$Header: /home/adascvs/idl/adas7xx/adas703/cw_adas703_in.pro,v 1.1 2004/07/06 12:55:26 whitefor Exp $ Date $Date: 2004/07/06 12:55:26 $
;+
; PROJECT:
;       ADAS IBM MVS to UNIX conversion
;
; NAME:
;       CW_ADAS703_IN()
;
; PURPOSE:
;       Data file selection for adas 703.
;
; EXPLANATION:
;       This function creates a compound widget consisting of two file
;       input fields to select the AUTOSTRUCTURE general output (olg) 
;       and oic or oicu (or the ls equivalents).
;	The compound widget is completed with 'Cancel' and 'Done' buttons.
;
;	The value of this widget is contained in the VALUE structure.
;
; USE:
;       See routine adas703_in.pro for an example.
;
; INPUTS:
;       PARENT  - Long integer; the ID of the parent widget.
;
; OPTIONAL INPUTS:
;       None
;
; OUTPUTS:
;       This function returns the ID of the compound widget created.
;
; OPTIONAL OUTPUTS:
;       None
;
; KEYWORD PARAMETERS:
;       VALUE   - A structure which determines the initial settings of
;                 the entire compound widget. The structure must be:
;		 	{ 	EROOTPATH	:	'',		$
;				EFILE		:	'',		$
;				ECENTROOT	:	'',		$
;				EUSERROOT	:	'',		$
;				CROOTPATH	:	'',		$
;				CFILE		:	'',		$
;			  	CCENTROOT	:	'',		$
;			  	CUSERROOT	:	''		}
;
;		  Where the elements of the structure are as follows:
;
;                 EROOTPATH   - Current data directory for olg files
;
;		  EFILE	      - Current olg file
;
;                 ECENTROOT   - Default central data store for olg file
;
;                 EUSERROOT   - Default user data store for olg file
;
;                 CROOTPATH   - Current data directory for oic file
;
;		  CFILE	      - Current oic file
;
;                 CCENTROOT   - Default central data store for oic file
;
;                 CUSERROOT   - Default user data store for oic file
;
;
;                 Path names may be supplied with or without the trailing
;                 '/'.  The underlying routines add this character where
;                 required so that USERROOT will always end in '/' on
;                 output. This does not apply to DSNEX and DSNCX.
;
;       FONT_LARGE  - Supplies the large font to be used for the
;                     interface widgets.
;
;       FONT_SMALL  - Supplies the small font to be used for the
;                     interface widgets.
; CALLS:
;	CW_ADAS_INFILE	Data file selection widget.
;	FILE_ACC	Checks availability of files.
;	NUM_CHK		Checks validity of numerical data.
;
; SIDE EFFECTS:
;       IN703_GET_VAL() Widget management routine in this file.
;       IN703_EVENT()   Widget management routine in this file.
;
; CATEGORY:
;       Compound Widget
;
; WRITTEN:
;       Martin O'Mullane
;
; MODIFIED:
;       1.1     Martin O'Mullane 
;		First release. 
;
; VERSION:
;       1.1     29-04-98
;
;-
;-----------------------------------------------------------------------------

FUNCTION in703_get_val, id


                ;**** Return to caller on error ****

    ON_ERROR, 2

                 ;***************************************
                 ;****     Retrieve the state        ****
                 ;**** Get first_child widget id     ****
                 ;**** because state is stored there ****
                 ;***************************************

    first_child = widget_info(id, /child)
    widget_control, first_child, get_uvalue = state

		;**** Get the settings ****

    widget_control, state.expid, get_value=fileval
    widget_control, state.chxid, get_value=fileval2

    ps = {	EROOTPATH	:	fileval.rootpath,		$
		EFILE		:	fileval.file,			$
		ECENTROOT	:	fileval.centroot,		$
		EUSERROOT	:	fileval.userroot,		$
		CROOTPATH	:	fileval2.rootpath,		$
		CFILE		:	fileval2.file,			$
		CCENTROOT	:	fileval2.centroot,		$
		CUSERROOT	:	fileval2.userroot		}

    widget_control, first_child, set_uvalue=state, /no_copy

    RETURN, ps

END

;-----------------------------------------------------------------------------

FUNCTION in703_event, event

    COMMON adas703_inblock, userdir, centdir

                ;**** Base ID of compound widget ****

    parent = event.handler

                ;**** Default output no event ****

    new_event = 0L

                ;**********************************************
                ;**** Retrieve the user value state        ****
                ;**** Get id of first_child widget because ****
                ;**** user value "state" is stored there   ****
                ;**********************************************

    first_child = widget_info(parent, /child)
    widget_control, first_child, get_uvalue=state, /no_copy
    topparent = widget_info(parent, /parent)

		;**** Clear any messages away ****

    widget_control, state.messid, set_value= '   '

                ;************************
                ;**** Process events ****
                ;************************

    CASE event.id OF

		;**** Event from olg file selection ****
                
	state.expid: begin
        
	       widget_control, state.chxid, get_value=chxval
               if event.action eq 'newfile' then begin

		;**** Check if oic file. If so, select as
		;**** oic file.

		widget_control, state.expid, get_value=expval
		
                ;**** Check whether oic file exists and if so,
		;**** sensitize buttons

                if strcompress(chxval.file, /remove_all) ne '' then begin
		    filename = chxval.rootpath + chxval.file
                    file_acc, filename, fileexist, read, write, 	$
                              execute, filetype
		    if filetype eq '-' then begin
                        widget_control, state.doneid, /sensitive
		    endif
		endif
                widget_control, state.browseid, /sensitive


            endif else begin
                widget_control, state.doneid, sensitive=0
                if strcompress(chxval.file, /remove_all) eq '' then begin
                    widget_control, state.browseid, sensitive=0
		endif
            endelse                    
                    
        end
   
		;**** Event from oic file selection ****

	state.chxid: begin
        
	       widget_control, state.expid, get_value=expval
               if event.action eq 'newfile' then begin
		   if strcompress(expval.file, /remove_all) ne '' then begin
		       filename = expval.rootpath + expval.file
                       file_acc, filename, fileexist, read, write, 	$
                                 execute, filetype
		       if filetype eq '-' then begin
                           widget_control, state.doneid, /sensitive
		       endif
		   endif
                   widget_control, state.browseid, /sensitive
               endif else begin
                   widget_control, state.doneid, sensitive=0
		   if strcompress(expval.file, /remove_all) eq '' then begin
                       widget_control, state.browseid, sensitive=0
		   endif
               endelse      
                 
        end


                ;***********************
                ;**** Browse button ****
                ;***********************

        state.browseid: begin

                ;**** Invoke comments browsing ****

	    widget_control, state.expid, get_value=expval
	    if strcompress(expval.file, /remove_all) ne '' then begin
		filename = expval.rootpath + expval.file
                file_acc, filename, fileexist, read, write, execute, filetype
		if filetype eq '-' then begin
		    xxtext, filename, font=state.font_large
		endif
	    endif
	    widget_control, state.chxid, get_value=chxval
	    if strcompress(chxval.file, /remove_all) ne '' then begin
		filename = chxval.rootpath + chxval.file
                file_acc, filename, fileexist, read, write, execute, filetype
		if filetype eq '-' then begin
		    xxtext, filename, font=state.font_large
		endif
	    endif
        end

                ;***********************
                ;**** Cancel button ****
                ;***********************

        state.cancelid: begin
            new_event = {ID:parent, TOP:event.top,          		$
                         HANDLER:0L, ACTION:'Cancel'}
        end

		;*********************
		;**** Done Button ****
		;*********************

	state.doneid: begin
	    error = 0

		;************************************
                ;**** return value or flag error ****
		;************************************

            if error eq 0 then begin
                new_event = {ID:parent, TOP:event.top,                  $
                             HANDLER:0L, ACTION:'Done'}
            endif else begin
		widget_control,state.messid,set_value=message
                new_event = 0L
            endelse
	end

        ELSE: new_event = 0L

    ENDCASE


                ;*********************************************
                ;*** make "state" available to other files ***
                ;*********************************************

    widget_control, first_child, set_uvalue=state, /no_copy

    RETURN, new_event

END

;-----------------------------------------------------------------------------

FUNCTION cw_adas703_in, parent, VALUE=value, FONT_LARGE=font_large, 	$
                        FONT_SMALL=font_small

    COMMON adas703_inblock, userdir, centdir

    ON_ERROR, 2                                 ;return to caller on error

                ;**** Set defaults for keywords ****

    IF NOT (KEYWORD_SET(value)) THEN begin
        inset = { EROOTPATH	:	userroot+'/adf04/',		$
		  EFILE		:	'',				$
		  ECENTROOT	:	centroot+'/adf04/',		$
		  EUSERROOT	:	userroot+'/adf04/',		$	
		  CROOTPATH	:	userroot+'/adf04/',		$
		  CFILE		:	'',				$
	   	  CCENTROOT	:	centroot+'/adf04/',		$
		  CUSERROOT	:	userroot+'/adf04/'		}
    ENDIF ELSE BEGIN
	inset = {	EROOTPATH       :       value.erootpath,	$
                        EFILE           :       value.efile,            $
			ECENTROOT       :       value.ecentroot,	$
			EUSERROOT       :       value.euserroot,	$
                        CROOTPATH       :       value.crootpath,	$
                        CFILE           :       value.cfile,		$
                        CCENTROOT       :       value.ccentroot,	$
                        CUSERROOT       :       value.cuserroot		}

        if strtrim(inset.erootpath) eq '' then begin
            inset.erootpath = './'
        endif else if                                                   $
        strmid(inset.erootpath, strlen(inset.erootpath)-1,1) ne '/' then begin
            inset.erootpath = inset.erootpath+'/'
        endif
        if strtrim(inset.crootpath) eq '' then begin
            inset.crootpath = './'
        endif else if                                                   $
        strmid(inset.crootpath, strlen(inset.crootpath)-1,1) ne '/' then begin
            inset.crootpath = inset.crootpath+'/'
        endif
    ENDELSE
    IF NOT (KEYWORD_SET(font_large)) THEN font_large = ''
    IF NOT (KEYWORD_SET(font_small)) THEN font_small = ''

                ;*********************************
                ;**** Create the Input widget ****
                ;*********************************

                ;**** create base widget ****

    topbase = widget_base(parent, EVENT_FUNC = "in703_event",		$
                          FUNC_GET_VALUE = "in703_get_val",		$
		          /column)

                ;******************************************************
                ;**** Create a dummy widget just to hold value of *****
                ;**** "state" variable so as not to get confused  *****
                ;**** with any other values. Adopt IDL practice   *****
                ;**** of using first child widget                 *****
                ;******************************************************

    first_child = widget_base(topbase)

    cwid = widget_base(first_child, /column)

                ;*****************************************
                ;**** olg file selection widget       ****
                ;*****************************************

    expbase = widget_base(cwid, /column, /frame)
    expval = {	ROOTPATH	:	inset.erootpath,		$
		FILE		:	inset.efile,			$
		CENTROOT	:	inset.ecentroot,		$
		USERROOT	:	inset.euserroot			}
    exptitle = widget_label(expbase, font=font_large,			$
	                    value='AUTOSTRUCTURE General File (olg) Details:-')
    expid = cw_adas4xx_infile(expbase, value=expval, font=font_small)

                ;***********************************************
                ;**** oic file selection widget ****
                ;***********************************************

    chxbase = widget_base(cwid, /column, /frame)
    chxval = {	ROOTPATH	:	inset.crootpath,		$
		FILE		:	inset.cfile,			$
		CENTROOT	:	inset.ccentroot,		$
		USERROOT	:	inset.cuserroot			}
    chxtitle = widget_label(chxbase, font=font_large,			$
	                    value='AUTOSTRUCTURE Rate File (oic) Details:-')
    chxid = cw_adas4xx_infile(chxbase, value=chxval, font=font_small)

		;**** Error message ****

    messid = widget_label(cwid, font=font_large,			$
    value='Edit the processing options data and press Done to proceed')

                ;*****************
                ;**** Buttons ****
                ;*****************

    base = widget_base(cwid, /row)

                ;**** Browse Dataset button ****

    browseid = widget_button(base, value='Browse Comments',		$
                             font=font_large)

                ;**** Cancel Button ****

    cancelid = widget_button(base, value='Cancel', font=font_large)

                ;**** Done Button ****

    doneid = widget_button(base, value='Done', font=font_large)

                ;*************************************************
                ;**** Check filenames and desensitise buttons ****
                ;**** if it is a directory or it is a file    ****
                ;**** without read access.                    ****
                ;*************************************************

    browseallow = 0
    filename = inset.erootpath + inset.efile
    file_acc, filename, fileexist, read, write, execute, filetype
    if filetype ne '-' then begin
        widget_control, browseid, sensitive=0
        widget_control, doneid, sensitive=0
    endif else begin
        if read eq 0 then begin
            widget_control, browseid, sensitive=0
            widget_control, doneid, sensitive=0
        endif else begin
	    browseallow = 1
	endelse
    endelse
    filename = inset.crootpath + inset.cfile
    file_acc, filename, fileexist, read, write, execute, filetype
    if filetype ne '-' then begin
        if browseallow eq 0 then widget_control, browseid, sensitive=0
        widget_control, doneid, sensitive=0
    endif else begin
        if read eq 0 then begin
            if browseallow eq 0 then widget_control, browseid, sensitive=0
            widget_control, doneid, sensitive=0
        endif
    endelse

                ;*************************************************
                ;**** create a state structure for the pop-up ****
                ;****                window.                  ****
                ;*************************************************

    new_state = {	font_large	:	font_large,		$
			font_small	:	font_small,		$
			inval		:	inset,			$
			doneid		:	doneid,			$
			browseid	:	browseid,		$
			expid		:	expid,			$
			chxid		:	chxid,			$
			messid		:	messid,			$
			cancelid	:	cancelid		}

               ;**** Save initial state structure ****

    widget_control, first_child, set_uvalue=new_state, /no_copy
    
    RETURN, topbase

END
