; Copyright (c) 1998 Strathclyde University .
; SCCS info: Module @(#)$Header: /home/adascvs/idl/adas7xx/adas703/g3ispf.pro,v 1.1 2004/07/06 13:56:56 whitefor Exp $ Date $Date: 2004/07/06 13:56:56 $
;+
; PROJECT:
;       ADAS IBM MVS to   UNIX conversion
;
; NAME:
;	G3ISPF
;
; PURPOSE:
;	IDL user interface and communications with ADAS703 FORTRAN
;	process via pipe.
;
; EXPLANATION:
;	The routine begins by reading information from the ADAS703
;	FORTRAN process via a UNIX pipe.  Then part of the ADAS703
;	IDL user interface is invoked to determine how the user
;	wishes to process the input dataset.  When the user's
;	interactions are complete the information gathered with
;	the user interface is written back to the FORTRAN process
;	via the pipe.  Communications are to the FORTRAN subroutine
;	G3ISPF.
;
; USE:
;	The use of this routine is specific to ADAS703, see adas703.pro.
;
; INPUTS:
;	PIPE	- The IDL unit number of the bi-directional pipe to the
;		  ADAS703 FORTRAN process.
;
;	LPEND	- Integer, 0 if the user pressed the 'Done' button
;		  or 1 if the user pressed 'Cancel'.  Maps directly onto
;		  the logical variable LPEND in ADAS703 FORTRAN.
;
;	PROCVAL	- A structure which determines the initial settings of
;		  the processing options widget.  The initial value is
;		  set in adas703.pro.  If adas703.pro passes a blank 
;		  dummy structure of the form {NMET:-1} into PROCVAL then
;		  PROCVAL is reset to a default structure.
;
;		  The PROCVAL structure is;
;			procval = {                        	$
;                			nmet  : 0 ,             $
;                			title : '',             $
;                			bwno  : flt_arr,        $
;                			iopt  : 0,              $
;					ifout : 1,              $
;					maxt  : 0, 		$
;                			tin   : temp_arr        
;              			  }
;
;
;		  See cw_adas703_proc.pro for a full description of this
;		  structure.
;	
;
;       BITFILE - String; the path to the dirctory containing bitmaps
;                 for the 'escape to series menu' button.
;
; OPTIONAL INPUTS:
;	None
;
; OUTPUTS:
;	PROCVAL	- On output the structure records the final settings of
;		  the processing selections widget if the user pressed the
;		  'Done' button, otherwise it is not changed from input.
;
;       LPEND   - Integer, 0 if the user pressed the 'Done' button
;                 or 1 if the user pressed 'Cancel'.  Maps directly onto
;                 the logical variable LPEND in ADAS501 FORTRAN.
;
;       GOMENU  - Int; flag - set to 1 if user has selected 'escape direct
;                 to series menu , otherwise 0
;
; OPTIONAL OUTPUTS:
;	None
;
; KEYWORD PARAMETERS:
;	FONT_LARGE - The name of a larger font.
;
;	FONT_SMALL - The name of a smaller font.
;
;	EDIT_FONTS - A structure of two fonts used in the table editor
;		     adas_edtab.pro, {font_norm:'',font_input:''}.
;
; CALLS:
;	ADAS703_PROC	Invoke the IDL interface for ADAS703 data
;		 	processing options/input.
;
; SIDE EFFECTS:
;	Two way communications with ADAS703 FORTRAN via the
;	bi-directional UNIX pipe.
;
; CATEGORY:
;	Adas system.
;
; WRITTEN:
;       Martin O'Mullane
;
; MODIFIED:
;	1.1	Martin O'Mullane
;		First release 
;
; VERSION:
;       1.1	29-04-98
; 
;-
;-----------------------------------------------------------------------------

PRO g3ispf, pipe, lpend, procval, gomenu, bitfile,		$
	    FONT_LARGE=font_large, FONT_SMALL=font_small, 		$
	    EDIT_FONTS=edit_fonts


                ;**** Set defaults for keywords ****

    IF NOT (KEYWORD_SET(font_large)) THEN font_large = ''
    IF NOT (KEYWORD_SET(font_small)) THEN font_small = ''
    IF NOT (KEYWORD_SET(edit_fonts)) THEN 				$
    edit_fonts = {font_norm:'',font_input:''}

		;********************************************
		;****     Declare variables for input    ****
		;**** arrays will be declared after sizes****
		;**** have been read.                    ****
                ;********************************************

    lpend = 0
    ndtin = 0
    ndp   = 0
    np    = 0
    input = 0
    
    next_item = 0.0
    next_str  = '  '

		;********************************
		;**** Read data from fortran ****
		;********************************

    readf, pipe, input
    ndtin = input
    readf, pipe, input
    ndp = input
    readf, pipe, input
    np = input

		;******************************************
		;**** Now can define other array sizes ****
		;******************************************

    tvals  = dblarr(ndtin, 3)
    ipstr  = strarr(ndp)

		;********************************
                ;**** Read data from fortran ****
                ;********************************


    for j =  0 , 2 do begin
	for i = 0, ndtin-1 do begin
            readf, pipe, next_item
            tvals(i,j) = next_item
        endfor
    endfor

    for i = 0, ndp-1 do begin
        readf, pipe, next_str
        ipstr(i) = next_str
    endfor

		;*******************************************
		;**** Set default value if non provided ****
		;*******************************************

    if (procval.nmet lt 0) then begin
         temp1_arr = fltarr(ndp)
         temp2_arr = fltarr(ndtin)
         procval = {    nmet    :       0 ,                             $
           	        title 	: 	'',				$
                        bwno	:	temp1_arr,			$
                        iopt    :       0,                              $
                        ifout   :       1,                              $
			maxt  	: 	0,              		$
			tin   	: 	temp2_arr			}
    endif

		;****************************************
		;**** Pop-up processing input widget ****
		;****************************************

    adas703_proc, procval, ndtin, tvals, ndp, np, ipstr,		$ 
                  action, bitfile,					$
		  FONT_LARGE=font_large, FONT_SMALL=font_small, 	$
		  EDIT_FONTS=edit_fonts

		;********************************************
		;****  Act on the event from the widget  ****
                ;**** There are three  possible  actions ****
                ;**** 'Done', 'Cancel'and 'Menu'.        ****
		;********************************************

    if action eq 'Done' then begin
        lpend = 0
    endif else if action eq 'Menu' then begin
        lpend = 0
        gomenu = 1
    endif else begin
        lpend = 1
    endelse

		;*******************************
		;**** Write data to fortran ****
		;*******************************

    printf, pipe, lpend   
    title_short = strmid(procval.title,0,39) 	
    printf, pipe, title_short, format='(a40)' 
    printf, pipe, procval.bwno
    printf, pipe, procval.iopt
    printf, pipe, procval.ifout
    printf, pipe, procval.maxt
    printf, pipe, procval.tin
   

END
