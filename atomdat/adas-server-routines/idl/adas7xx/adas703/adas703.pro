; Copyright (c) 1998 Strathclyde University .
; SCCS info: Module @(#)$Header: /home/adascvs/idl/adas7xx/adas703/adas703.pro,v 1.2 2004/07/06 11:04:12 whitefor Exp $ Date $Date: 2004/07/06 11:04:12 $
;+
; PROJECT:
;       ADAS IBM MVS to UNIX conversion
;
; NAME:
;	ADAS703
;
; PURPOSE:
;	The highest level routine for the ADAS 703 program.
;
; EXPLANATION:
;	This routine is called from the main adas system routine, adas.pro,
;	to start the ADAS 703 application.  Associated with adas703.pro
;	is a FORTRAN executable.  The IDL code provides the user
;	interface and output graphics whilst the FORTRAN code reads
;	in data files, performs numerical processing and creates the
;	output files.  The IDL code communicates with the FORTRAN
;	executable via a bi-directional UNIX pipe.  The unit number
;	used by the IDL for writing to and reading from this pipe is
;	allocated when the FORTRAN executable is 'spawned' (see code
;	below).  Pipe communications in the FORTRAN process are to
;	stdin and stdout, i.e streams 5 and 6.
;
;	The FORTRAN code is an independent process under the UNIX system.
;	The IDL process can only exert control over the FORTRAN in the
;	data which it communicates via the pipe.  The communications
;	between the IDL and FORTRAN must be exactly matched to avoid
;	input conversion errors.  The correct ammounts of data must be
;	passed so that neither process 'hangs' waiting for communications
;	which will never occur.
;
;	The FORTRAN code performs some error checking which is
;	independent of IDL.  In cases of error the FORTRAN may write
;	error messages.  To prevent these error messages from conflicting
;	with the pipe communications all FORTRAN errors are written to
;	output stream 0, which is stderr for UNIX.  These error messages
;	will appear in the window from which the ADAS session/IDL session
;	is being run.
;
;	In the case of severe errors the FORTRAN code may terminate
;	itself prematurely.  In order to detect this, and prevent the
;	IDL program from 'hanging' or crashing, the IDL checks to see
;	if the FORTRAN executable is still an active process before
;	each group of pipe communications.  The process identifier
;	for the FORTRAN process, PID, is recorded when the process is
;	first 'spawned'.  The system is then checked for the presence
;	of the FORTRAN PID.
;
; USE:
;	First the system settings must be established by calling
;	adas_sys_set.pro then adas703.pro is called to start the
;	ADAS 703 application;
;
;	adas_sys_set, adasrel, fortdir, userroot, centroot, $
;		      devlist, devcode, font_large, font_small, edit_fonts
;	adas703,   adasrel, fortdir, userroot, centroot, devlist, $
;		   devcode, font_large, font_small, edit_fonts
;
; INPUTS:
;	ADASREL - A string indicating the ADAS system version, 
;		  e.g ' ADAS RELEASE: ADAS93 V1.4'.  The first
;		  character should be a space.
;
;	FORTDIR - A string holding the path to the directory where the
;		  FORTRAN executables are, e.g '/disk/adas/fortran/exec'
;
;	USERROOT - A string holding the path to the root directory of
;		   the user's adas data. e.g '/disk/bowen/adas'
;		   This root directory will be used by adas to construct
;		   other path names.  For example the users default data
;		   for adas205 should be in /disk/bowen/adas/adf04.  In
;		   particular the user's default interface settings will
;		   be stored in the directory USERROOT+'/defaults'.  An
;		   error will occur if the defaults directory does not
;		   exist.
;
;	CENTROOT - Like USERROOT, but this directory points to the
;		   central data area for the system.  User defaults are
;		   not stored on CENTROOT.
;
;	FONT_LARGE - The name of a larger font e.g 'courier_bold14'
;
;	FONT_SMALL - The name of a smaller font e.g 'courier_bold12'
;
;	EDIT_FONTS - A structure of two fonts used in the table editor
;		     adas_edtab.pro, {font_norm:'',font_input:''}.
;		     The two fonts are used to differentiate between
;		     editable and non-editable parts of the table. You
;		     may have to experiment to get two fonts which give
;		     the same default spacing in the edit widget.
;		     e.g {font_norm:'helvetica_bold14', $
;			  font_input:'helvetica_oblique14'}
;
; OPTIONAL INPUTS:
;	None
;
; OUTPUTS:
;	None - Note this routine should leave all inputs un-modified.
;
; OPTIONAL OUTPUTS:
;	None
;
; KEYWORD PARAMETERS:
;	None
;
; CALLS:
;	FIND_PROCESS	Checks to see if a given process is active.
;       XXDATE          Get date and time from operating system.
;       G3SPF0          Pipe comms with FORTRAN G3SPF0 routine.
;       G3ISPF          Pipe comms with FORTRAN G3ISPF routine.
;       G3SPF1          Pipe comms with FORTRAN G3SPF1 routine.
;	G3OTG1		Pipe comms with FORTRAN G3OTG1 routine.
;	G3OTG2		Pipe comms with FORTRAN G3OTG2 routine.
;	G3OTG3		Pipe comms with FORTRAN G3OTG3 routine.
;
; SIDE EFFECTS:
;	This routine spawns a FORTRAN executable.  Note the pipe 
;	communications routines listed above.  In addition to these
;	pipe communications there is one explicit communication of the
;	date to the FORTRAN code, search for 'printf, pipe' to find it.
;	There are also communications of the variable gomenu to the
;	FORTRAN which is used as a signal to stop the program in its
;	tracks and return immediately to the series menu. Do the same
;	search as above to find the instances of this.
;
; CATEGORY:
;	Adas system.
;	
; WRITTEN:
;       Martin O'Mullane
;
; MODIFIED:
;	1.1	Martin O'Mullane
;		First release 
;	1.2	Richard Martin
;		Increase version no to 1.2
;
; VERSION:
;       1.1	28-04-98
;	  18-03-03
; 
;-
;-----------------------------------------------------------------------------

PRO ADAS703,	adasrel, fortdir, userroot, centroot, 			$
		devlist, devcode, font_large, font_small, edit_fonts

		;************************
		;**** Initialisation ****
		;************************

    adasprog = ' PROGRAM: ADAS703 V1.2'
    lpend    = 0
    gomenu   = 0
    deffile  = userroot+'/defaults/adas703_defaults.dat'
    bitfile  = centroot+'/bitmaps'
    device   = ''

		;******************************************
		;**** Search for user default settings ****
		;**** If not found create defaults     ****
		;**** inval: settings for data files   ****
		;**** procval: settings for processing ****
		;**** outval: settings for output      ****
		;******************************************

    files = findfile(deffile)
    if files(0) eq deffile then begin
        restore, deffile
        inval.ecentroot = centroot+'/autos/'
        inval.ccentroot = centroot+'/autos/'
        inval.euserroot = userroot+'/autos/'
        inval.cuserroot = userroot+'/autos/'
    endif else begin
	inval =	{							$
		  EROOTPATH	:	userroot+'/autos/',		$
		  EFILE		:	'',				$
		  ECENTROOT	:	centroot+'/autos/',		$
		  EUSERROOT	:	userroot+'/autos/',		$
		  CROOTPATH	:	userroot+'/autos/',		$
		  CFILE		:	'',				$
	   	  CCENTROOT	:	centroot+'/autos/',		$
		  CUSERROOT	:	userroot+'/autos/'		}
  
        procval = {NMET:-1}
	copdefval = userroot + '/pass/adas703_adf04.pass'
        outval = {							$
                        TEXOUT          :       0,                      $
                        TEXAPP          :       -1,                     $
                        TEXREP          :       0,                      $
                        TEXDSN          :       '',                     $
                        TEXDEF          :       'paper.txt',            $
                        TEXMES          :       '',			$
                        COPOUT          :       0,                      $
                        COPAPP          :       -1,                     $
                        COPREP          :       0,                      $
                        COPDSN          :       '',                     $
                        COPDEF          :       copdefval,              $
                        COPMES          :       '',                     $
                        TITLE  		:	''                      }
    end


		;****************************
		;**** Start fortran code ****
		;****************************

    spawn, fortdir+'/adas703.out', unit=pipe, /noshell, PID=pid

		;************************************************
		;**** Get date and write to fortran via pipe ****
		;************************************************

    date = xxdate()
    printf, pipe, date(0)

LABEL100:

		;**** Check FORTRAN still running ****
  
    if find_process(pid) eq 0 then goto, LABELEND

		;************************************************
		;**** Communicate with g3spf0 in fortran and ****
		;**** invoke user interface widget for       ****
		;**** Data file selection                    ****
		;************************************************

    g3spf0, pipe, inval, rep, dsnic1, dsnic2,				$
	    FONT_LARGE=font_large, FONT_SMALL=font_small

		;**** If cancel selected then end program ****

    if rep eq 'YES' then goto, LABELEND

		;**** Check FORTRAN still running ****

    if find_process(pid) eq 0 then goto, LABELEND


LABEL200:

    if find_process(pid) eq 0 then goto, LABELEND

		;************************************************
		;**** Communicate with g3ispf in fortran and ****
		;**** invoke user interface widget for       ****
		;**** processing options                     ****
		;************************************************

    g3ispf, pipe, lpend, procval,					$
	    gomenu, bitfile, 						$
	    FONT_LARGE=font_large, FONT_SMALL=font_small, 		$
            EDIT_FONTS=edit_fonts

		;**** If menu button clicked, tell FORTRAN to stop ****

    if gomenu eq 1 then begin
 	printf, pipe, 1
 	goto, LABELEND
    endif else begin
 	printf, pipe, 0
    endelse


		;**** If cancel selected then goto 400 ****

    if lpend eq 1 then goto, LABEL100

                ;*************************************************
		;**** Fortran processing now in progress. Get ****
		;**** date and time for this run.	      ****
                ;*************************************************

    date = xxdate()

		;**** Create header for output. ****

    header = adasrel+adasprog+' DATE: '+date(0)+' TIME: '+date(1)


LABEL300:
		;**** Check FORTRAN still running ****

    if find_process(pid) eq 0 then goto, LABELEND

		;************************************************
		;**** Communicate with g3spf1 in fortran and ****
		;**** invoke user interface widget for       ****
		;**** Output options                         ****
		;************************************************
    
    ; put the title on the output screen also - it is not
    ; allowed to be edited.
    
    if procval.nmet ne -1 then outval.title = procval.title
    
    g3spf1, pipe, lpend, outval,			$ 
            header, bitfile, gomenu,			$
	    FONT=font_large

                ;**** Extra check to see whether FORTRAN is still there ****

    if find_process(pid) eq 0 then goto, LABELEND

		;**** If menu button clicked, tell FORTRAN to stop ****

    if gomenu eq 1 then begin
	printf, pipe, 1
        outval.texmes = ''
        outval.copmes = ''
	goto, LABELEND
    endif else begin
	printf, pipe, 0
    endelse

                    
                ;**********************************************
                ;**** If cancel selected then erase output ****
                ;**** messages and goto 200.               ****
                ;**********************************************

    if lpend eq 1 then begin
        outval.texmes = ''
        outval.copmes = ''
        goto, LABEL200
    endif


    ;wait for flag from fortran to ensure all text output is written 
    ;before program exits
 
    dummy=0
    readf,pipe,dummy

                ;**** Back for more output options ****

    GOTO, LABEL300


LABELEND:
 

                ;*********************************************
                ;**** Ensure appending is not enabled for ****
                ;**** text output at start of next run.   ****
                ;*********************************************

    outval.texapp = -1


		;**** Save user defaults ****

    save, inval, procval, outval, filename=deffile


END
