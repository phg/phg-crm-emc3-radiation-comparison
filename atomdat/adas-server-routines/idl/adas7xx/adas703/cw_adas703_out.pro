; Copyright (c) 1998 Strathclyde University .
; SCCS info: Module @(#)$Header: /home/adascvs/idl/adas7xx/adas703/cw_adas703_out.pro,v 1.1 2004/07/06 12:55:29 whitefor Exp $ Date $Date: 2004/07/06 12:55:29 $
;+
; PROJECT:
;       ADAS IBM MVS to UNIX conversion
;
; NAME:
;	CW_ADAS703_OUT()
;
; PURPOSE:
;	Produces a widget for ADAS703 output options.
;
; EXPLANATION:
;	This function declares a compound widget consisting of an
;       output passing file  selection widget and an output file widget 
;       cw_adas_outfile.pro.  The text output file specified in this 
;       widget is for tabular (paper.txt) output. This widget also includes 
;       a button for 'Cancel', a 'Done' button and an 'Escape to series menu' 
;       button. This latter is represented by a bitmapped button.
;       The compound widgets cw_adas_outfile.pro included in this file 
;       are self managing.  This widget only handles events from the 
;       'Cancel' and 'Escape to series
;       menu' buttons.
;     
; USE:
;	This routine is specific to adas703.
;
; INPUTS:
;	PARENT	- Long integer; ID of parent widget.
;
;       BITFILE - String; the path to the dirctory containing bitmaps
;                 for the 'escape to series menu' button.
;
; OPTIONAL INPUTS:
;	None.
;
; OUTPUTS:
;	The return value of this function is the ID of the declared widget.
;
; OPTIONAL OUTPUTS:
;	None.
;
; KEYWORD PARAMETERS:
;
;	VALUE	- A structure which determines the initial settings of
;		  the output options widget.  The structure is made
;		  up of two parts.  Each part is the same as the value
;		  structure of one of the two main compound widgets
;		  included in this widget.  See cw_adas_gr_sel and
;		  cw_adas_outfile for more details.  The default value is;
;
;		      {	TEXOUT:0, TEXAPP:-1, 				$
;			TEXREP:0, TEXDSN:'', 				$
;			TEXDEF:'',TEXMES:'', 				$
;                       DIRNAME:''					$
;		      }
;
;		  For CW_ADAS_OUTFILE tabular output (paper.txt);
;			TEXOUT	Integer; Activation button 1 on, 0 off
;			TEXAPP	Integer; Append button 1 on, 0 off, -1 no button
;			TEXREP	Integer; Replace button 1 on, 0 off, -1 no button
;			TEXDSN	String; Output file name
;			TEXDEF	String; Default file name
;			TEXMES	String; file name error message
;
;			DIRNAME String; output directory for iso-nuclear files
;
;	UVALUE	- A user value for this widget.
;
;	FONT	- String; the name of a font to be used for all text in
;		  this widget.  Default to system default font.
;
; CALLS:
;	CW_ADAS_OUTFILE	Output file name entry widget.
;	See side effects for other related widget management routines.
;
; SIDE EFFECTS:
;
; 	Two other routines are included in this file
;	OUT703_GET_VAL()
;	OUT703_EVENT()
;
; CATEGORY:
;	Compound Widget
;
; WRITTEN:
;       Martin O'Mullane
; MODIFIED:
;	1.1	Martin O'Mullane	
;		First release
;
; VERSION:
;       1.1	28-04-98
;
;-
;-----------------------------------------------------------------------------

FUNCTION out703_get_val, id

                ;**** Return to caller on error ****

    ON_ERROR, 2

                ;**** Retrieve the state ****

    parent=widget_info(id, /parent)
    widget_control, parent, get_uvalue=state, /no_copy

		;**** Get adf04 pass name ****

    widget_control, state.passid, get_value = pspos

		;**** Get text output settings ****

    widget_control, state.paperid, get_value=papos

		;***********************************
		;**** Get run title from widget ****
		;**** Then centre in in string  ****
		;**** of 40 characters 		****
		;***********************************

    widget_control, state.titid, get_value=title
    title = strcompress(title(0))
    title_len = strlen(title) 
    if (title_len gt 40 ) then begin 
        title = strmid(title, 0, 37)
        widget_control, state.messid, set_value="Title too long - truncated "
        widget_control, state.titid, set_value=title
	wait, 1
    endif
    pad = (40 - title_len)/2 
    spaces = '                                         '
    title(0) = strmid(spaces,0,(pad-1)) + title + strmid(spaces,0,(pad-1))

    os = { out703_set, 					$
	   TEXOUT	:	papos.outbut,		$
           TEXAPP	:	papos.appbut, 		$
	   TEXREP	:	papos.repbut,		$ 
           TEXDSN	:	papos.filename, 	$
	   TEXDEF	:	papos.defname, 		$
           TEXMES	:	papos.message, 		$
	   COPOUT	:	pspos.outbut,		$
           COPAPP	:	pspos.appbut, 		$
	   COPREP	:	pspos.repbut,		$ 
           COPDSN	:	pspos.filename, 	$
	   COPDEF	:	pspos.defname, 		$
           COPMES	:	pspos.message, 		$
	   TITLE	:	title			}

                ;**** Return the state ****

  widget_control, parent, set_uvalue=state, /no_copy

  RETURN, os 

END

;-----------------------------------------------------------------------------

FUNCTION out703_event, event

    COMMON outblock703, font

                ;**** Base ID of compound widget ****

    parent=event.top

                ;**** Retrieve the state ****

    widget_control, parent, get_uvalue=state, /no_copy

		;*********************************
		;**** Clear previous messages ****
		;*********************************

    widget_control, state.messid, set_value=' '

                ;************************
                ;**** Process events ****
                ;************************

    CASE event.id OF


		;***********************
		;**** Cancel button ****
		;***********************

        state.cancelid: new_event = {ID:parent, TOP:event.top, 			$
		                     HANDLER:0L, ACTION:'Cancel'}

		;*********************
		;**** Done button ****
		;*********************

        state.doneid: begin

                ;****************************************************
                ;**** Return the state before checking can start ****
                ;**** with the get_value keyword.                ****
                ;****************************************************

            widget_control, parent, set_uvalue=state, /no_copy

                ;***************************************
                ;**** Check for errors in the input ****
                ;***************************************

            error = 0

                ;**** Get a copy of the widget value ****

            widget_control, event.handler, get_value=os

                ;**** Check for widget error messages ****
           
            mess = ''
            if (os.texout eq 1 and strtrim(os.texmes) ne '')  		    $
            or (os.copout eq 1 and strtrim(os.copmes) ne '')  then error=1

            if (os.copout eq 0) then begin
                error = 1
                mess = 'Error: you must choose a Specific Ion file'
            endif
            
            
                ;**** Retrieve the state   ****

            widget_control, parent, get_uvalue=state, /no_copy
            if error eq 1 then begin
                widget_control, state.messid, set_value=                $
                    '**** Error in output settings ****'
                new_event = 0L
            endif else begin
                new_event = {ID:parent, TOP:event.top, HANDLER:0L,      $
                             ACTION:'Done'}
            endelse
        end

         	;*********************
                ;**** Menu button ****
		;*********************

        state.outid: begin
            new_event = {ID:parent, TOP:event.top, HANDLER:0L,          $
                         ACTION:'Menu'}
        end

    ELSE: new_event = 0L

  ENDCASE


                ;**** Return the state   ****

    widget_control, parent, set_uvalue=state, /no_copy

    RETURN, new_event

END

;-----------------------------------------------------------------------------

FUNCTION cw_adas703_out, parent,  bitfile,                       $
		         VALUE=value, UVALUE=uvalue, FONT=font


    COMMON outblock703, fontcom


    IF (N_PARAMS() EQ 0) THEN MESSAGE, 'Must specify parent for cw_adas703_out'
    ON_ERROR, 2					;return to caller

		;**** Set defaults for keywords ****

    IF NOT (KEYWORD_SET(uvalue)) THEN uvalue = 0
    IF NOT (KEYWORD_SET(font)) THEN font = ''
    IF NOT (KEYWORD_SET(value)) THEN begin
	os = {out703_set, 				$
	TEXOUT:0,                    TEXAPP:-1,             $
	TEXREP:0,                    TEXDSN:'',             $
        TEXDEF:'paper.txt',          TEXMES:'',             $
        COPOUT:0,                    COPAPP:0,              $
        COPREP:0,                    COPDSN:'',             $
        COPDEF:'adas703_adf04.pass', COPMES:'',             $
        TITLE:'' }
    ENDIF ELSE BEGIN
	os = {out703_set, 				$
	TEXOUT:value.texout, TEXAPP:value.texapp, 	$
	TEXREP:value.texrep, TEXDSN:value.texdsn, 	$
	TEXDEF:value.texdef, TEXMES:value.texmes, 	$
        COPOUT:value.copout, COPAPP:value.copapp,       $
        COPREP:value.coprep, COPDSN:value.copdsn,       $
        COPDEF:value.copdef, COPMES:value.copmes,       $
        TITLE:value.title }
    ENDELSE

    fontcom = font

		;**********************************************
		;**** Create the 703 Output options widget ****
		;**********************************************

		;**** create base widget ****

    cwid = widget_base( parent, UVALUE = uvalue, 			$
			EVENT_FUNC = "out703_event", 			$
			FUNC_GET_VALUE = "out703_get_val", 		$
			/COLUMN)


		;***********************
		;**** add run title ****
		;***********************
  base = widget_base(cwid, /row)
  rc = widget_label(base, value='Title for Run', font=font)
  titid = widget_text(base, value=value.title, 				$
		font=font)

                ;**** Widget for adf04 data output ****

  outfval = { OUTBUT:os.copout, APPBUT:os.copapp, REPBUT:os.coprep,     $
              FILENAME:os.copdsn, DEFNAME:os.copdef, MESSAGE:os.copmes }
  base1 = widget_base(cwid, /row, /frame)
  passid = cw_adas_outfile(base1, OUTPUT='Specific Ion Output', $
                           VALUE=outfval, FONT=font)

		;**** Widget for text output ****

    outfval = { OUTBUT:os.texout, APPBUT:-1, REPBUT:os.texrep, 		$
	        FILENAME:os.texdsn, DEFNAME:os.texdef, MESSAGE:os.texmes }
    base = widget_base(cwid,/row,/frame)
    paperid = cw_adas_outfile(base, OUTPUT='Text Output', 		$
                              VALUE=outfval, FONT=font)

		;**** Error message ****

    messid = widget_label(cwid,value=' ', font=font)

		;**** add the exit buttons ****

    base = widget_base(cwid, /row)
    menufile = bitfile + '/menu.bmp'
    read_X11_bitmap, menufile, bitmap1
    outid = widget_button(base, value=bitmap1)          ;menu button
    cancelid = widget_button(base, value='Cancel', font=font)
    doneid = widget_button(base, value='Done', font=font)
  
		;*************************************************
		;**** create a state structure for the pop-up ****
		;**** window.                                 ****
		;*************************************************

    new_state = { PAPERID	:	paperid,		$
                  PASSID        :       passid,                 $
		  CANCELID	:	cancelid, 		$
                  DONEID        :       doneid,                 $
                  OUTID         :       outid,                  $
                  MESSID        :       messid,			$
                  TITID         :       titid                   }

                ;**** Save initial state structure ****

    widget_control, parent, set_uvalue=new_state, /no_copy

    RETURN, cwid

END

