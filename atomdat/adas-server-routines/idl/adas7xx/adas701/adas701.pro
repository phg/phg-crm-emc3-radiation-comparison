; Copyright (c) 1998 Strathclyde University .
; SCCS info: Module @(#)$Header: /home/adascvs/idl/adas7xx/adas701/adas701.pro,v 1.6 2004/07/06 11:03:29 whitefor Exp $ Date $Date: 2004/07/06 11:03:29 $
;+
; PROJECT:
;       ADAS 
;
; NAME:
;	ADAS701
;
; PURPOSE:
;	The highest level routine for the ADAS 701 program.
;
; EXPLANATION:
;	This routine is called from the main adas system routine, adas.pro,
;	to start the ADAS 701 application.  Associated with adas701.pro
;	is a FORTRAN executable, adas701.out.  The IDL code provides the user
;	interface and output graphics whilst the FORTRAN code reads
;	in data files, performs numerical processing and creates the
;	output files.  The IDL code communicates with the FORTRAN
;	executable via a bi-directional UNIX pipe.  The unit number
;	used by the IDL for writing to and reading from this pipe is
;	allocated when the FORTRAN executable is 'spawned' (see code
;	below).  Pipe communications in the FORTRAN process are to
;	stdin and stdout, i.e streams 5 and 6.
;
;	The FORTRAN code is an independent process under the UNIX system.
;	The IDL process can only exert control over the FORTRAN in the
;	data which it communicates via the pipe.  The communications
;	between the IDL and FORTRAN must be exactly matched to avoid
;	input conversion errors.  The correct ammounts of data must be
;	passed so that neither process 'hangs' waiting for communications
;	which will never occur.
;
;	The FORTRAN code performs some error checking which is
;	independent of IDL.  In cases of error the FORTRAN may write
;	error messages.  To prevent these error messages from conflicting
;	with the pipe communications all FORTRAN errors are written to
;	output stream 0, which is stderr for UNIX.  These error messages
;	will appear in the window from which the ADAS session/IDL session
;	is being run. In the case of the batch job, the error messages
;	will appear as standard cron job output which is usually
;	emailed to the user. If the job has completed succesfully then
;	the user will get a message telling them so.
;
;	In the case of severe errors the FORTRAN code may terminate
;	itself prematurely.  In order to detect this, and prevent the
;	IDL program from 'hanging' or crashing, the IDL checks to see
;	if the FORTRAN executable is still an active process before
;	each group of pipe communications.  The process identifier
;	for the FORTRAN process, PID, is recorded when the process is
;	first 'spawned'.  The system is then checked for the presence
;	of the FORTRAN PID. This checking does not occur for the 
;	batch cases.
;
; USE:
;	First the system settings must be established by calling
;	adas_sys_set.pro then adas701.pro is called to start the
;	ADAS 701 application;
;
;	adas_sys_set, adasrel, fortdir, userroot, centroot, 		$
;		      devlist, devcode, font_large, font_small, edit_fonts
;	adas701,   adasrel, fortdir, userroot, centroot, devlist, 	$
;		   devcode, font_large, font_small, edit_fonts
;
; INPUTS:
;	ADASREL - A string indicating the ADAS system version, 
;		  e.g ' ADAS RELEASE: ADAS93 V1.1.0'.  The first
;		  character should be a space.
;
;	FORTDIR - A string holding the path to the directory where the
;		  FORTRAN executables are, e.g '/disk/adas/fortran/exec'
;
;	USERROOT - A string holding the path to the root directory of
;		   the user's adas data. e.g '/disk/bowen/adas'
;		   This root directory will be used by adas to construct
;		   other path names.  For example the users default data
;		   for adas205 should be in /disk/bowen/adas/adf04.  In
;		   particular the user's default interface settings will
;		   be stored in the directory USERROOT+'/defaults'.  An
;		   error will occur if the defaults directory does not
;		   exist.
;
;	CENTROOT - Like USERROOT, but this directory points to the
;		   central data area for the system.  User defaults are
;		   not stored on CENTROOT.
;
;	DEVLIST - A string array of hardcopy device names, used for
;		  graphical output. e.g ['Post-Script','HP-PCL','HP-GL']
;		  This array must mirror DEVCODE.  DEVCODE holds the
;		  actual device names used in a SET_PLOT statement.
;
;	DEVCODE - A string array of hardcopy device code names used in
;		  the SET_PLOT statement, e.g ['PS', 'PCL', 'HP']
;		  This array must mirror DEVLIST.
;
;	FONT_LARGE - The name of a larger font e.g 'courier_bold14'
;
;	FONT_SMALL - The name of a smaller font e.g 'courier_bold12'
;
;	EDIT_FONTS - A structure of two fonts used in the table editor
;		     adas_edtab.pro, {font_norm:'',font_input:''}.
;		     The two fonts are used to differentiate between
;		     editable and non-editable parts of the table. You
;		     may have to experiment to get two fonts which give
;		     the same default spacing in the edit widget.
;		     e.g {font_norm:'helvetica_bold14', $
;			  font_input:'helvetica_oblique14'}
;
; OPTIONAL INPUTS:
;	None
;
; OUTPUTS:
;	None - Note this routine should leave all inputs un-modified.
;
; OPTIONAL OUTPUTS:
;	None
;
; KEYWORD PARAMETERS:
;	None
;
; CALLS:
;	FIND_PROCESS	Checks to see if a given process is active.
;       XXDATE          Get date and time from operating system.
;       G1SPF0          Pipe comms with FORTRAN G1SPF0 routine.
;       G1SPF1          Pipe comms with FORTRAN G1SPF1 routine.
;	G1PROGBAR	Updates the batch files and variables for
;			the batch runs depending on the user selections.
;
; SIDE EFFECTS:
;	This routine spawns FORTRAN executables.  Note the pipe 
;	communications routines listed above. 
;
; CATEGORY:
;	Adas system.
;	
; WRITTEN:
;       Martin O'Mullane
;
; MODIFIED:
;	1.1	Martin O'Mullane	
;		First release, based very loosely on ADAS211
;	1.2	Richard Martin	
;		Increased version no. to 1.2
;	1.3	Richard Martin	
;		Increased version no. to 1.3
;	1.4	Richard Martin	
;		Increased version no. to 1.4
;	1.5	Richard Martin	
;		Increased version no. to 1.5
;	1.6	Martin O'Mullane	
;		Increased version no. to 1.6
;
; VERSION:
;       1.1     05-04-98
;	1.2 	04-12-98
;	1.3 	19-03-99
;	1.4	15-10-99
;	1.5	20-11-01
;	1.5	28-08-2003
;	
;-----------------------------------------------------------------------------


PRO ADAS701,	adasrel, fortdir, userroot, centroot, 			$
		devlist, devcode, font_large, font_small, edit_fonts

		;************************
		;**** Initialisation ****
		;************************

    adasprog = ' PROGRAM: ADAS701 V1.6'
    lpend    = 0
    ipset    = 0
    ipbatset = 0
    l2batch  = 0			;Marker for whether a batch file
					;has been created.
                                        
    deffile  = userroot+'/defaults/adas701_defaults.dat'
    device   = ''


		;************************************
                ;****  defaults for output files ****
		;************************************

   ; note 1 = keep (button on), 0 = do not write the file (button off)
   ; The default is a simple structure calculation - is it possible
   ; to determine from the input file what the user wants to do?
   ;
   ; need information of coupling and format type of the files
   ; write into a structure and pass through program 
        
    list      = ['olg','ols','oic','olsu','oicu','RESTART',		$
                 'radwin','radout','TERMS','LEVELS','opls','opic']
    selected  = [1,0,0,0,0,0,0,0,1,0,0,0]
    form_comp = ['X','F','F','U','U','X','X','X','X','X','X','X']
    cpl_comp  = ['X','L','I','L','I','X','X','X','X','X','L','I']
    form_tgl  = [ [1,3],[2,4] ]
    cpl_tgl   = [ [1,2],[3,4],[10,11] ]
    
    file_list = { file_list,			$
                  list	     :   list, 		$
                  selected   :   selected,	$
                  form_comp  :   form_comp,	$
                  cpl_comp   :   cpl_comp,	$
                  form_tgl   :   form_tgl,	$
                  cpl_tgl    :   cpl_tgl	}
        

		;******************************************
		;**** Search for user default settings ****
		;**** If not found create defaults     ****
		;******************************************

    files = findfile(deffile)
    if files(0) eq deffile then begin

        restore,deffile
        inval.centroot = centroot+'/adf27/'
        inval.userroot = userroot+'/adf27/'

    end else begin

        inval = { 							$
		 ROOTPATH  : userroot+'/adf27/',                 	$
		 FILE      : '',                                 	$
		 CENTROOT  : centroot+'/adf27/',                 	$
		 USERROOT  : userroot+'/adf27/'                  	}

       outval = { 							$
                 new_list  :  list,					$
                 new_sel   :  selected,					$
                 new_dir   :  userroot+'/autos/',			$
                 message   :  '', 					$
                 form      :  'F',					$
                 cpl       :  'L',					$
                 iopt      :  0, 					$
		 TEXOUT	   :  0, 					$
                 TEXAPP    : -1,					$
		 TEXREP    :  0, 					$	
                 TEXDSN    : '',					$
		 TEXDEF    : 'paper.txt', 				$
                 TEXMES    : '',					$
                 DIRNAME   : userroot+'/autos/',     		        $
		 TITLE     : ''						}

    end


		;****************************
		;**** Start fortran code ****
		;****************************

    spawn, fortdir+'/adas701.out', unit=pipe, /noshell, PID=pid

		;************************************************
		;**** Write to fortran via pipe that we are  ****
		;**** running interactively                  ****
		;************************************************

    ibatch = 0
    printf, pipe, ibatch

		;******************
		;**** Get date ****
		;******************

    date = xxdate()
    printf, pipe, date(0)
    
LABEL100:
		;**** Check FORTRAN still running ****
  
    if find_process(pid) eq 0 then goto, LABELEND

		;************************************************
		;**** Communicate with g1spf0 in fortran and ****
		;**** invoke user interface widget for       ****
		;**** Data file selection                    ****
		;************************************************

    g1spf0, pipe, inval, dsfull, rep, FONT=font_large


    ipset = 0
    ipbatset = 0

		;**** If cancel selected then end program ****

    if rep eq 'YES' then goto, LABELEND

LABEL300:
		;**** Check FORTRAN still running ****

    if find_process(pid) eq 0 then goto, LABELEND

		;************************************************
		;**** Communicate with g1spf1 in fortran and ****
		;**** invoke user interface widget for       ****
		;**** Output options                         ****
		;************************************************

    g1spf1, pipe, lpend, outval, dsfull, file_list,  $
            header, FONT=font_large

		;**** If cancel selected then erase output ****
		;**** messages and goto 100.		   ****

    if lpend eq 1 then begin
        outval.texmes  = ''
        outval.message = ''
        goto, LABEL100
    end


		;*************************************************
		;**** Fortran processing now in progress. Get ****
		;**** date and time for this run.	      ****
		;*************************************************

    date = xxdate()

		;**** Create header for output. ****

    header = adasrel+adasprog+' DATE: '+date(0)+' TIME: '+date(1)


		;***********************************************
		;**** Now either begin the calculation or   ****
		;**** set up the batch file depending on    ****
		;**** what action has been requested.  	    ****
                ;***********************************************

    if lpend eq 0 then begin			;'Run Now' selected
        ipset = ipset + 1			; Increase 'run number'
        g1progbar, pipe, FONT=font_large
    endif else if lpend eq 2 then begin		;'Run in batch' selected
        ipbatset = ipbatset + 1			;Increase run number

        ;**** Kick the batch job off ****

        rand = strmid(strtrim(string(randomu(seed)), 2), 2, 4)
        dscontrol = 'control'+rand+'.tmp'
        dsinfo = 'info'+rand+'.tmp'
        openw, conunit, dscontrol, /GET_LUN
        openw, infunit, dsinfo, /GET_LUN
        printf, conunit, fortdir+'/adas701.out < '+dsinfo 
        printf, conunit, 'rm -f '+dsinfo 
        printf, conunit, 'rm -f '+dscontrol

 	;**** Write stuff needed by adas701 - it will be read into stdin ****

        yesbatch = 1
	repn = 'NO'
	repy = 'YES'
	lpend = 0
        printf, infunit, yesbatch
        printf, infunit, date(0)
        
	;**** Input screen, g1spf0, pipe info ****
        printf, infunit, repn
        printf, infunit, dsfull

	;**** Output screen, g1spf1, pipe info ****
        printf, infunit, lpend
	printf, infunit, outval.dirname
        for j = 0,n_elements(file_list.list)-1 do begin
	   printf, infunit, outval.new_list(j)
 	   printf, infunit, outval.new_sel(j)
        endfor
	printf, infunit, outval.texdsn
	printf, infunit, outval.title
	printf, infunit, outval.texout
	printf, infunit, lpend

	;**** Input screen again - reply yes => 'cancel' ****
        printf, infunit, repy
        printf, infunit, dsfull

        close, infunit, conunit
        spawn, 'chmod u+x '+dscontrol

        batch, dscontrol, title='ADAS701: INFORMATION', jobname='NO. '+rand

        l2batch = 0

    endif


		;**** Back for more input options ****
                
    outval.texmes  = ''
    outval.message = ''

    GOTO, LABEL100

LABELEND:

		;**** Save user defaults ****

    outval.texmes  = ''
    outval.message = ''
    
    save, inval, outval, filename=deffile
    
    close, /all


END
