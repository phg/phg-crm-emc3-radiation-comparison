; Copyright (c) 1998 Strathclyde University .
; SCCS info: Module @(#)$Header: /home/adascvs/idl/adas7xx/adas701/test_file.pro,v 1.1 2004/07/06 15:24:20 whitefor Exp $ Date $Date: 2004/07/06 15:24:20 $
;+
; PROJECT:
;	ADAS 
;
; NAME:
;	TEST_FILE()
;
; PURPOSE:
;	Check whether the input is a valid filename.
;
; INPUTS:
;       directory - directory in which the file name to be tested resides
;       file_name - name of file to be tested
;
; OUTPUTS:
;       message   - returned error message  
;
; CALLS:
;	file_acc to check for existance of files.
;
; SIDE EFFECTS:
;	None - it is completely self contained.
;
; CATEGORY:
;	Utility procedure 
;
; WRITTEN:
;	Martin O'Mullane
;
; MODIFIED:
;	1.1	Martin O'Mullane	
;		First release.
;
; VERSION:
;       1.1     11/6/1998
;-
;-----------------------------------------------------------------------------

pro test_file, directory, file_name, message  

  IF (N_PARAMS() LT 3) THEN MESSAGE, $
		'Must specify a NAME, ERROR and MESSAGE for test_field'


	;*** construct filename and limit  to 80 characters ***

      dirname  = strtrim(directory,2)
      filename = strmid(dirname+file_name, 0,80)


		;**********************************************
		;**** Perform checks on the specified file ****
		;**********************************************

      error   = 0
      message = ' '

		;**** Check that a file name has been provided ****
                
      if error eq 0 then begin
        if strtrim(filename,2) eq '' then begin
	  fileexist = -1
	  error = 1
	  message = 'Error: No file name has been entered.'
        end
      end

		;**** Check if file already exists ****
                
      if error eq 0 then begin
        file_acc,filename,fileexist,read,write,execute,filetype
        if filetype eq 'd' then begin
	  error = 1
	  message = 'Error: The supplied name is a directory.'
        end
        if error eq 0 then begin
          if fileexist eq 1 then begin
	    error = 1
	    message = 'Warning: One or more files already exist.'
          end
        end
      end


		;**** If file doesn't exist check path name is correct ****
                
      if error eq 0 and fileexist eq 0 then begin

		  ;**** Find the end of the directory path ***
        pathend = -1
        i = strlen(filename) - 1
        while (i gt 0 and pathend eq -1) do begin
          if strmid(filename,i,1) eq '/' then pathend = i
          i = i - 1
        end

		  ;**** Extract directory path ****
        if pathend gt 0 then begin
          path = strmid(filename,0,pathend)
        end else begin
          path = '.'
        end

		  ;**** Check path ****
        file_acc,path,pathexist,read,write,execute,filetype
         if pathexist eq 0 or filetype ne 'd' then begin
           error = 1
	   message = 'Error: The path name is invalid.'
         end

      end

                ;*******************************************************
		;**** Check for write access                        ****
		;**** Look for write access to an existing file     ****
		;**** or for write and execute access on the path   ****
		;**** for a new file.  This will also trap any      ****
		;**** path which passes the previous test but isn't ****
		;**** really a directory.                           ****
                ;*******************************************************
                
      if error eq 0 then begin
        if fileexist eq 1 then begin
	  if write ne 1 then begin
	    error = 1
	    message = 'Error: No write permission for file.'
          end
        end else begin
	  if write ne 1 or execute ne 1 then begin
            error = 1
            message = 'Error: No write permission for directory.'
          end
        end
      end
    
    
 

  RETURN

END
