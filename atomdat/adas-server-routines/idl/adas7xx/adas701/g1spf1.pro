; Copyright (c) 1998 Strathclyde University .
; SCCS info: Module @(#)$Header: /home/adascvs/idl/adas7xx/adas701/g1spf1.pro,v 1.1 2004/07/06 13:56:15 whitefor Exp $ Date $Date: 2004/07/06 13:56:15 $
;+
; PROJECT:
;       ADAS 
;
; NAME:
;	G1SPF1
;
; PURPOSE:
;	IDL user interface and communications with ADAS701 FORTRAN
;	process via pipe.
;
; EXPLANATION:
;       First this routine reads some information from ADAS701 FORTRAN
;       via the UNIX pipe.  Next the 'Output Options' part of the
;	interface is invoked.  The resulting action of the user's
;	interactions is written to the FORTRAN via the pipe.
;	Communications are with the FORTRAN subroutine G1SPF1.
;
; USE:
;	The use of this routine is specific to ADAS701, See adas701.pro.
;
; INPUTS:
;	PIPE	- The IDL unit number of the bi-directional pipe to the
;		  ADAS701 FORTRAN process.
;
;	DSFULL  - The name of the data set being analyzed.
;
;	VALUE	- A structure which determines the initial settings of
;		  the dataset selection widget.  The initial value is
;		  set in adas701.pro.  VALUE is passed un-modified
;		  through to cw_adas_in.pro, see that routine for a full
;		  description.
;
;	HEADER  - Header information used for text output.
;
; OPTIONAL INPUTS:
;	None.
;
; OUTPUTS:
;	LPEND	- Integer; indicates user action on leaving output
;		  selections.  lpend is set to 1 if the user exited
;		  with the 'Cancel' button, 0 if the user exited
;		  with 'Run Now' and 2 if the user exited with 'Run in 
;		  Batch'.
;
;	VALUE	- On output the structure records the final settings of
;		  the output selection widget if the user pressed either
;		  of the 'Run' buttons otherwise it is not changed from input.
;
; OPTIONAL OUTPUTS:
;	None.
;
; KEYWORD PARAMETERS:
;
;	FONT	- Supplies the font to be used for the interface widgets.
;
; CALLS:
;	ADAS701_OUT	User interface - output options.
;
; SIDE EFFECTS:
;	This routine communicates with the ADAS701 FORTRAN process
;	via a UNIX pipe.
;
; CATEGORY:
;	Adas system.
;
; WRITTEN:
;       Martin O'Mullane
;
; MODIFIED:
;	1.1	Martin O'Mullane	
;		First release.
;
; VERSION:
;       1.1     22/04/1998
;
;-
;-----------------------------------------------------------------------------

PRO   g1spf1, pipe, lpend, value, dsfull, file_list,  $
              header, FONT=font

                ;**** Return to caller on error ****

    ON_ERROR, 2

                ;**** Set defaults for keywords ****

    IF NOT (KEYWORD_SET(font)) THEN font = ''


                ;********************************
                ;**** Read data from fortran ****
                ;********************************
  form = ''
  cpl  = ''
  ifout = 0
  readf,pipe,form
  readf,pipe,cpl
  
		;**************************************
		;**** Pop-up output options widget ****
		;**************************************

    outtitle = "ADAS701 OUTPUT OPTIONS"
    adas701_out, value, dsfull, file_list, form, cpl,   $
                 action, FONT=font

		;*************************************************
		;**** Act on the output from the widget       ****
		;**** There are three    possible actions     ****
		;**** 'Cancel', 'Run Now' and 'Run in Batch'. ****
		;*************************************************

    if action eq 'Run Now' then begin
        lpend = 0
    endif else if action eq 'Run In Batch' then begin
        lpend = 2
    endif else begin
        lpend = 1
    end

		;*******************************
		;**** Write data to fortran ****
		;*******************************

    printf, pipe, lpend

    if (lpend eq 0) or (lpend eq 2) then begin

	printf, pipe, value.dirname
        
        for j = 0,n_elements(file_list.list)-1 do begin
	   printf, pipe, value.new_list(j)
 	   printf, pipe, value.new_sel(j)
        endfor
        
	printf, pipe, value.texdsn
	printf, pipe, value.title
	printf, pipe, value.texout
        yesbatch = 1
        nobatch = 0
        if lpend eq 2 then printf, pipe, yesbatch	$
	else printf, pipe, nobatch

		;**************************************************
		;**** Set messages and settings ready for when ****
		;**** output window is re-displayed.           ****
		;**************************************************

	if value.texout eq 1 then begin
            if value.texrep ge 0 then value.texrep = 0
	    value.texmes = 'Output written to file.'
        endif
    endif

END
