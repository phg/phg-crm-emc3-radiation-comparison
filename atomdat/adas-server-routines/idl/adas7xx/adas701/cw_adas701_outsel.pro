; Copyright (c) 1998 Strathclyde University .
; SCCS info: Module @(#)$Header: /home/adascvs/idl/adas7xx/adas701/cw_adas701_outsel.pro,v 1.1 2004/07/06 12:55:14 whitefor Exp $ Date $Date: 2004/07/06 12:55:14 $
;+
; PROJECT:
;	ADAS 
;
; NAME:
;	CW_ADAS701_OUTSEL()
;
; PURPOSE:
;	Multiple selection and change list compound widget.
;       This particular version is specific to adas series 7 routines.
;
; EXPLANATION:
;	This compound widget function produces a multiple selection list.
;	The LIST is a one dimensional string array of filenames.
;	The selection list consists of toggle buttons, one for each item
;	in the list, and an edit field to allow renaming.  The buttons are 
;       stacked in columns the number of which is determined by the user 
;       supplied NROW keyword. The fully qualified filename is a 
;       concatenation of the LIST name (or the changed file value) and the
;       base directory which is entered in another compond widget. Thus 
;       while this widget does not generate any events it reacts to a 
;       send_event from the base directory compound widget. The fully 
;       qualified filenames are checked for legality.
;
;
; USE:
;	See the routine cw_adas701_out for a working example of how to use
;	cw_adas701_outsel.
;
;
; INPUTS:
;       PARENT   - The ID of the parent widget.
;
;	LIST     - One dimensional string array of filenames for the
;		   selection list.  Each list item will be assigned
;		   a button.
;
; OPTIONAL INPUTS:
;	None.  See the keywords for additional controls.
;
; OUTPUTS:
;	The return value of this function is the ID of the compound
;	widget which is of type LONG integer.
;
; OPTIONAL OUTPUTS:
;	None
;
; KEYWORDS:
;	NROW     -  Number of rows the list should occupy. The number of
;                   columns required is calculated from this.
;		  
;
;	UVALUE   - Supplies the user value for the widget.  This is 
;		   exclusively for the caller's use and has no effect on
;		   the operation of the widget.
;
;	FONT	 - Supplies the font to be used for the widget.
;
;	VALUE	- A structure which determines the initial settings of
;		  the cw_adas701_outsel options widget.  The structure is 
;
;               value = {   new_list : str_arr,   $
;                           new_sel  : int_arr,   $
;                           new_dir  : '',        $
;                           message  : '',        $
;                           iopt     : 0          }
;
;       new_list -  One dimensional string array corresponding to user entered
;                   new names for output filenames. Initially it is set to
;                   the LIST input.
;
;	new_sel  -  One dimensional integer array of the indices of the
;	            items in LIST which are to be set as new_sel. There is
;                   a one to one correspondance between new_sel and LIST.
;                   A 1 signifies keep (button on) and 0 not to write
;                   (button off).
;
;	new_dir  -  String of new target directory.
;
;	iopt     -  More than one LIST may be requird. This variable is
;                   for book keeping in the calling program and is not used
;                   within cw_adas701_outsel.
;
;       message  -  Holds the current warning message.
;
;
; CALLS:
;	None directly.  See SIDE EFFECTS below for a list of the widget
;	management routines.
;
; RESTRICTIONS:
;	None.
;
; SIDE EFFECTS:
;
;       Reacts to events from cw_adas701_out event handler.
;
;	Three other routines are included which are used to manage the
;	widget;
;
;	OUTSEL_SET_VAL
;	OUTSEL_GET_VAL()
;	OUTSEL_EVENT()
;
; CATEGORY:
;	Compound Widget 
;
; WRITTEN:
;	Martin O'Mullane
;
; MODIFIED:
;	1.1	Martin O'Mullane	
;		First release.
;
; VERSION:
;       1.1     12/6/1998
;-
;-----------------------------------------------------------------------------

PRO outsel_set_val, id, value


		;**** Return to caller on error ****
  ON_ERROR, 2

		;**** Retrieve the state ****
                
  first_child = widget_info(id, /child)
  widget_control, first_child, get_uvalue = state ,/no_copy


		;**** Set buttons for new selections ****

  for i = 0, state.numsel-1 do begin
    widget_control,state.list_button(i),set_button=value.new_sel(i)
  end

		;**** Set filenames for new selections ****

  for i = 0, state.numsel-1 do begin
    widget_control,state.list_text(i),set_value=value.new_list(i)
  end

		;**** Check validity of input ****
                
  error   = 0
  message = ' '
  for i = 0, state.numsel-1 do begin
    widget_control, state.list_text(i), get_value=filename
    dirname  = state.mcval.new_dir(0)
    filename = filename(0)

    if state.mcval.new_sel(i) eq 1 then test_file, dirname, filename, message

  end

		;**** Copy message to widget ****
                
  if strtrim(value.message) ne '' then begin
     message = value.message
  end else begin
     message = ' '
  end
  widget_control,state.messid,set_value=message

		;**** Copy the new values to state structure ****
                ;**** not necessary to save new_dir name?

  state.mcval.new_list = value.new_list
  state.mcval.new_sel  = value.new_sel
  state.mcval.new_dir  = value.new_dir   
  state.mcval.message  = value.message   
                
                
		;**** Save the new state ****
                
  widget_control, first_child, set_uvalue=state, /no_copy

END

;-----------------------------------------------------------------------------

FUNCTION outsel_get_val, id

		;**** Return to caller on error ****
  ON_ERROR, 2

		;**** Retrieve the state ****

  first_child = widget_info(id, /child)
  widget_control, first_child, get_uvalue = state
  
 
    error   = 0
    message = ' '
    for i = 0, state.numsel-1 do begin

      widget_control, state.list_text(i), get_value=filename
      dirname  = state.mcval.new_dir(0)
      filename = filename(0)

               ;**********************************************
               ;**** Perform checks on the specified file ****
               ;**********************************************

      if state.mcval.new_sel(i) eq 1 then test_file, dirname, filename, message


               ;**** set the message - non blank if error has occurred.  ****

       state.mcval.message = message

               ;**** This GET_VALUE routine can change the value ****
               ;**** of the widget so call SET_VALUE to update   ****
               ;**** the screen.                                 ****

      widget_control, state.list_text(i), set_value=filename

    endfor


  RETURN, state.mcval

END

;-----------------------------------------------------------------------------

FUNCTION outsel_event, event


		;**** Base ID of compound widget ****

  parent = event.handler
  
     ;***************************************************************
     ;**** automatically notified if output directory is changed ****
     ;**** via a send_event from cw_adas701_out event handler    ****
     ;***************************************************************

  out_dir = ''
  thisEvent = tag_names(event,/structure)
  if thisEvent eq 'RENAMENEW' then begin
     out_dir = event.name
  endif
    
 
		;**** Retrieve the state ****

  first_child = widget_info(parent, /child)
  widget_control, first_child, get_uvalue=state 

		;**** Remove existing error message ****
 
  widget_control, state.messid, set_value = ' '
  state.mcval.message = ' '
  widget_control, state.mess2id, set_value = ' '
  state.mcval.message_2 = ' '


		;***************************************************
		;**** Update the new_dir name in the state and  ****
		;**** check the filenames if it has changed.    ****
		;***************************************************
  
  if out_dir ne '' then begin
     state.mcval.new_dir = out_dir
     for i = 0, state.numsel-1 do begin
       if state.mcval.new_sel(i) eq 1 then begin
            
          widget_control,state.list_text(i),get_value=filename
          filename = filename(0)
          dirname  = out_dir

          test_file, dirname, filename,  message
	  state.mcval.message = message

	  widget_control,state.messid,set_value=message
            
       endif
     endfor   
  endif
   
		;************************
		;**** Process events ****
		;************************

		;**** Button events ****
                
    for i = 0, state.numsel-1 do begin
      if event.id eq state.list_button(i) then begin
         if state.mcval.new_sel(i) eq 0 then begin
          
            state.mcval.new_sel(i) = 1           ; button on
            
            ; and check if filename exists    
         
            widget_control,state.list_text(i),get_value=filename
            filename = filename(0)
            dirname  = state.mcval.new_dir(0)

            test_file, dirname, filename,  message
            
	    state.mcval.message = message
	    widget_control,state.messid,set_value=message
        
            ; check if filename is compatible with coupling/format
            
            if state.file_list.cpl_comp(i) ne 'X' and 			$
               (state.mcval.cpl ne state.file_list.cpl_comp(i)) then 	$
               m1 = 'coupling'  else  m1 = ''
            if state.file_list.form_comp(i) ne 'X' and 			$
               (state.mcval.form ne state.file_list.form_comp(i)) then 	$
               m2 = 'format' else  m2 = ''
            
            if (m1 eq 'coupling' and m2 eq '') then 			$
               message_2 = 'Incompatible coupling' else			$
            if (m1 eq 'coupling' and m2 eq 'format') then 		$
               message_2 = 'Incompatible coupling and format'  else	$
            if (m1 eq '' and m2 eq 'format') then 			$
               message_2 = 'Incompatible format'  			$       
            else							$
               message_2 = ''
            
            ; if ic coupling then ls output may be sensible
            
            if state.mcval.cpl eq 'I' and (i eq 1 or i eq 3) then begin
              if strpos(message_2,'format') ne -1 then $
                 message_2 = 'Wrong format and possible conflict in coupling' $ 
              else message_2 = 'Possible conflict in coupling choice'
            endif
              
	    state.mcval.message_2 = message_2
	    widget_control,state.mess2id,set_value=message_2
        
         endif else begin
         
            state.mcval.new_sel(i) = 0           ; button off
         
         endelse
         
         widget_control,state.list_button(i),set_button=state.mcval.new_sel(i)
         
      endif
    endfor             

		;**** Rename file edit_text events ****
                
    error   = 0
    message = ' '
    for i = 0, state.numsel-1 do begin
      if event.id eq state.list_text(i) and state.mcval.new_sel(i) eq 1 then begin
         widget_control,state.list_text(i),get_value=filename
         filename = filename(0)
         dirname  = state.mcval.new_dir(0)

         test_file, dirname, filename,  message
	 state.mcval.new_list(i) = filename
	 state.mcval.message = message
	 widget_control,state.messid,set_value=message
      endif
    endfor             

    new_event = 0L

		;*******************************************
		;*** make state available to other files ***
		;*******************************************

    widget_control, first_child, set_uvalue=state, /no_copy
    RETURN, new_event
    

END

;-----------------------------------------------------------------------------

FUNCTION cw_adas701_outsel, parent, file_list, 				$ 
                            OUTPUT=output, NROW=nrow, 			$
			    VALUE=value, FONT=font, UVALUE=uvalue


		;**** Flag error and return to caller ****
                
  IF (N_PARAMS() LT 2) THEN MESSAGE, $
		'Must specify a PARENT and LIST for cw_adas701_outsel'
 
  ON_ERROR, 2

		;**** Set defaults for keywords ****
                
  IF NOT (KEYWORD_SET(output)) THEN output = 'Alter the Following Output'

  numsel = n_elements(file_list.list)
 
  IF NOT (KEYWORD_SET(value)) THEN begin

      str_arr = strarr(numsel)
      int_arr = intarr(numsel)
      mcval = { mcval,                  $
                new_list   : str_arr,   $
                new_sel    : int_arr,   $
                new_dir    : '',        $
                message    : '',        $
                message_2  : '',        $
                form       : 'F',       $
                cpl        : 'L',       $
                iopt       : 0          }
 
  end else begin
 
      mcval = { mcval,                         $
                new_list   : value.new_list,   $
                new_sel    : value.new_sel,    $
                new_dir    : value.new_dir,    $
                message    : value.message,    $
                message_2  : value.message_2,  $
                form       : value.form,       $
                cpl        : value.cpl,        $
                iopt       : value.iopt        }
  end

  IF NOT (KEYWORD_SET(font))   THEN font = ''
  IF NOT (KEYWORD_SET(uvalue)) THEN uvalue = 0
  IF NOT (KEYWORD_SET(nrow))   THEN nrow = 20


		;**** Create the main base for the table ****

  cwid = WIDGET_BASE(parent, UVALUE = uvalue, 			$
		     EVENT_FUNC     = "outsel_event", 		$
		     FUNC_GET_VALUE = "outsel_get_val", 	$
		     PRO_SET_VALUE  = "outsel_set_val", 	$
		     /COLUMN					)
  
  
		;******************************************************
		;**** Create a dummy widget just to hold value of *****
		;**** "state" variable so as not to get confused  *****
		;**** with any other values. Adopt IDL practice   *****
		;**** of using first child widget                 *****
		;******************************************************

  first_child = widget_base(cwid)
  topbase     = widget_base(first_child,/row)


		;**** See how many columns of buttons are requried ****
                
  listdim = size(file_list.list)
  ncols   = fix((listdim(1)+nrow-1)/nrow)
  

		;**** Create selection list of toggle button widgets ****
                                
  list_base   = lonarr(listdim(1))         ; base for button
  list_button = lonarr(listdim(1))         ; actual button
  list_text   = lonarr(listdim(1))         ; entry field to change name
  
  for j = 0, ncols-1 do begin
    colbase = widget_base(topbase,/column)
    for i = (j*nrow), min([(j*nrow)+nrow-1,listdim(1)-1]) do begin
      colbase2       = widget_base(colbase,/row)
      list_base(i)   = widget_base(colbase2,/nonexclusive,/row)
      list_button(i) = widget_button(list_base(i),value=file_list.list(i),   $
                                     xsize=110,font=font)
      list_text(i)   = widget_text(colbase2, xsize=13, /editable,       $
                                   value = mcval.new_list(i), font=font)
    end
  end

; Note that xsize in widget_label is in pixels but is in characters in 
; widget_text. There is no obvious way of matching these. Therefore the
; value in widget_label is set by trial and error.

		;**** Set buttons for current selections ****

  for i = 0, numsel-1 do begin
     widget_control,list_button(i),set_button=mcval.new_sel(i)
  endfor

		;**** Message - general errors ****
                
  if strtrim(mcval.message,2) eq '' then begin
     message = ' '
  end else begin
     message = mcval.message
  end
  messid = widget_label(cwid,value=message,font=font)

		;**** Message on incompatible coupling/format  ****
                
  if strtrim(mcval.message_2,2) eq '' then begin
     message_2 = ' '
  end else begin
     message_2 = mcval.message_2
  end
  mess2id = widget_label(cwid,value=message_2,font=font)


		;**** Create state structure ****
                
  new_state =  { list_button	:       list_button,    $
                 list_text	:       list_text,      $
                 messid	        :       messid,         $
                 mess2id	:       mess2id,        $
		 file_list	:       file_list,      $
 		 mcval		:	mcval,		$
                 numsel	        :       numsel          }


		;**** Save initial state structure ****
                
  widget_control, first_child, set_uvalue=new_state, /no_copy

  RETURN, cwid

END
