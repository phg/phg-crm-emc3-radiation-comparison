; Copyright (c) 1998 Strathclyde University .
; SCCS info: Module @(#)$Header: /home/adascvs/idl/adas7xx/adas701/g1progbar.pro,v 1.1 2004/07/06 13:56:05 whitefor Exp $ Date $Date: 2004/07/06 13:56:05 $
;+
; PROJECT:
;       ADAS  
;
; NAME:
;	G1PROGBAR
;
; PURPOSE:
;	IDL user interface and communications with ADAS701 FORTRAN
;	process via pipe.
;
; EXPLANATION:
;	This routine creates a progress indicator which is updated as
;	the FORTRAN progresses. The FORTRAN communicates via a pipe
;	the total number of stages and the stage which has been reached.
;
; USE:
;	The use of this routine is specific to ADAS701. See adas701.pro.
;
; INPUTS:
;	PIPE	- The IDL unit number of the bi-directional pipe to the
;		  ADAS701 FORTRAN process.
;
; OPTIONAL INPUTS:
;	None.
;
; OUTPUTS:
;	None
;
; OPTIONAL OUTPUTS:
;	None.
;
; KEYWORD PARAMETERS:
;
;	FONT	- Supplies the font to be used for the interface widgets.
;
; CALLS:
;
; SIDE EFFECTS:
;	This routine communicates with the ADAS701 FORTRAN processes
;	via UNIX pipes.
;	It also pops up an information widget which keeps the user 
;	updated as the calculation progresses.
;
; CATEGORY:
;	Adas system.
;
; WRITTEN:
;       Martin O'Mullane
;
; MODIFIED:
;	1.1	Martin O'Mullane
;
; VERSION:
;	1.1	24-04-98
;
;-
;-----------------------------------------------------------------------------

PRO g1progbar, pipe, FONT=font


                ;**** If there is an io error caused ****
		;**** by the Fortran crashing handle it ****

    ON_IOERROR, PIPE_ERR

                ;**** Set defaults for keywords ****

    IF NOT (KEYWORD_SET(font)) THEN font = ''

		;**** Create an information widget ****

    widget_control, /hourglass
    base = widget_base(/column, xoffset=300, yoffset=200,               $
    title = "ADAS701: INFORMATION")
    lab0 = widget_label(base, value='')
    lab1 = widget_label(base,                                           $
    value="    AUTOSTRUCTURE COMPUTATION UNDERWAY - PLEASE WAIT    ")
    grap = widget_draw(base, ysize=20, xsize=480) ; Size of the bar is hardwired -
			; is there another way to get it to fit the window?
    lab2 = widget_label(base,                                           $
    value = "Unknown amount completed!!!")
    dynlabel, base

    widget_control, base, /realize

    num = 0
    readf, pipe, num

		;**** Update information widget ****
                
; without delving into AUTOSTRUCTURE we cannot estimate the elapsed time
; so we have put up a message saying this!
;   
;    next = 0
;    step = 1000.0/num
;		;**** Scale time in an ad hoc exponential fashion      ****
;		;**** to account for the larger number of missing      ****
;		;**** transitions as the parent index is increased.    ****
;		;**** b should be changed if this is giving rediculous ****
;		;**** rates but it works OK for clike/clike_o2ls.dat.  ****
;		;**** Its magnitude should be increased to slow down   ****
;		;**** the apparent processing time at later times.     ****
;    b = -4.0/num
;    c = 1.0/(1-exp(num*b))
;    for i=0,num-1 do begin
;    	readf, pipe, next
;	p = float(i)/num*1000	;**** This would give a linear increase in
;	q = p+step		;**** loop time but not processing time
;	p = (1-exp(i*b))*c*1000
;	q = (1-exp((i+1)*b))*c*1000
;        for j=fix(p),fix(q) do begin
;            x = (float(j)+1)/1000.0
;            plots, [x, x],[0.0,1.0], /normal
;        endfor
;        widget_control, lab2, set_value="PROCESSING"+			$
;		string(fix(q/10),format='(I2)')+"% COMPLETED"
;    endfor

    readf, pipe, next


    goto, DONE

PIPE_ERR:
    mess = ['AUTOSTRUCTURE (adas701) has terminated prematurely.',$
            '  ',$
            'See main output file (olg) for details.']
    action = popup(message=mess, title = '*** ADAS701 Error ***',$
		 buttons=['OK'])

DONE:
		;**** Destroy the information widget ****

    widget_control, base, /destroy

END
