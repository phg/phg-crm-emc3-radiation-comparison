; Copyright (c) 1998 Strathclyde University .
; SCCS info: Module @(#)$Header: /home/adascvs/idl/adas7xx/adas701/cw_adas701_out.pro,v 1.1 2004/07/06 12:55:11 whitefor Exp $ Date $Date: 2004/07/06 12:55:11 $
;+
; PROJECT:
;       ADAS 
;
; NAME:
;	CW_ADAS701_OUT()
;
; PURPOSE:
;	Produces a widget for ADAS701 output options.
;
; EXPLANATION:
;	This function declares a compound widget consisting of an
;       output passing file directory selection widget and an output
;	file widget cw_adas_outfile.pro.  The text output file
;	specified in this widget is for tabular (paper.txt) output.
;	This widget also includes a button for browsing the comments
;       from the input dataset, a 'Cancel' button and 'Run Now' and
;       'Run in batch' buttons. The compound widgets
;       cw_adas_outfile.pro included in this file  are self managing.
;       This widget only handles events from the  'Cancel' and
;       'Run...' buttons.
;     
; USE:
;	This routine is specific to adas701.
;
; INPUTS:
;	PARENT	- Long integer; ID of parent widget.
;
;	INFOVAL	- String array; information about the input datasets
;
;       BITFILE - String; the path to the dirctory containing bitmaps
;                 for the 'escape to series menu' button.
;
; OPTIONAL INPUTS:
;	None.
;
; OUTPUTS:
;	The return value of this function is the ID of the declared widget.
;
; OPTIONAL OUTPUTS:
;	None.
;
; KEYWORD PARAMETERS:
;
;	VALUE	- A structure which determines the initial settings of
;		  the output options widget.  The structure is made
;		  up of two parts.  Each part is the same as the value
;		  structure of one of the two main compound widgets
;		  included in this widget.  See cw_adas_gr_sel and
;		  cw_adas_outfile for more details.  The default value is;
;
;		      {	TEXOUT:0, TEXAPP:-1, 				$
;			TEXREP:0, TEXDSN:'', 				$
;			TEXDEF:'',TEXMES:'', 				$
;                       DIRNAME:''					$
;		      }
;
;		  For CW_ADAS_OUTFILE tabular output (paper.txt);
;			TEXOUT	Integer; Activation button 1 on, 0 off
;			TEXAPP	Integer; Append button 1 on, 0 off, -1 no button
;			TEXREP	Integer; Replace button 1 on, 0 off, -1 no button
;			TEXDSN	String; Output file name
;			TEXDEF	String; Default file name
;			TEXMES	String; file name error message
;
;			DIRNAME String; output directory for iso-nuclear files
;
;	UVALUE	- A user value for this widget.
;
;	FONT	- String; the name of a font to be used for all text in
;		  this widget.  Default to system default font.
;
; CALLS:
;	CW_ADAS_OUTFILE	Output file name entry widget.
;	See side effects for other related widget management routines.
;
; SIDE EFFECTS:
;
; 	Two other routines are included in this file
;	OUT701_GET_VAL()
;	OUT701_EVENT()
;
; CATEGORY:
;	Compound Widget
;
; WRITTEN:
;       Martin O'Mullane
; MODIFIED:
;	1.1	Martin O'Mullane	
;		First release
;
; VERSION:
;       1.1	22-04-98
;
;-
;-----------------------------------------------------------------------------

FUNCTION out701_get_val, id

                ;**** Return to caller on error ****

    ON_ERROR, 2

                ;**** Retrieve the state ****

    parent=widget_info(id, /parent)
    widget_control, parent, get_uvalue=state, /no_copy

		;**** Get directory name ****

    widget_control, state.dirnameid, get_value = dir
    

		;**** Get text output settings ****

    widget_control, state.paperid, get_value=papos

		;***********************************
		;**** Get run title from widget ****
		;**** Then centre in in string  ****
		;**** of 40 characters 		****
		;***********************************

    widget_control, state.titid, get_value=title
    title = strcompress(title(0))
    title_len = strlen(title) 
    if (title_len gt 40 ) then begin 
        title = strmid(title, 0, 37)
        widget_control, state.messid, set_value="Title too long - truncated "
        widget_control, state.titid, set_value=title
	wait, 1
    endif
    pad = (40 - title_len)/2 
    spaces = '                                         '
    title(0) = strmid(spaces,0,(pad-1)) + title + strmid(spaces,0,(pad-1))

		;***********************
		;**** Get run  type ****
		;***********************


    widget_control, state.iopt0id, get_uvalue=temp
    if (temp(0) eq 1 ) then iopt = 0 
    widget_control, state.iopt1id, get_uvalue=temp
    if (temp(0) eq 1 ) then iopt = 1 
    widget_control, state.iopt2id, get_uvalue=temp
    if (temp(0) eq 1 ) then iopt = 2 

		;*******************************************************
		;**** Get rename info from cw_adas701_outsel widget ****
		;*******************************************************

    widget_control, state.renameid, get_value=renameval

    os = { out701_set, 				      $
           new_list   :	      renameval.new_list,     $
           new_sel    :	      renameval.new_sel,      $
           new_dir    :	      renameval.new_dir,      $
           message    :	      renameval.message,      $
           form       :	      renameval.form,         $
           cpl        :	      renameval.cpl,          $
           iopt       :	      iopt,	      	      $
	   texout     :       papos.outbut,           $
           texapp     :       papos.appbut,           $
	   texrep     :       papos.repbut,           $
           texdsn     :       papos.filename,         $
	   texdef     :       papos.defname,          $
           texmes     :       papos.message,          $
	   dirname    :       dir(0),                 $
           title      :       title                   $
        }

               ;**** Return the state ****

  widget_control, parent, set_uvalue=state, /no_copy

  RETURN, os 

END

;-----------------------------------------------------------------------------

FUNCTION out701_event, event

                ;**** Base ID of compound widget ****

    parent=event.top

                ;**** Retrieve the state ****

    widget_control, parent, get_uvalue=state, /no_copy

		;*********************************
		;**** Clear previous messages ****
		;*********************************

    widget_control, state.messid, set_value=' '

                ;************************
                ;**** Process events ****
                ;************************

    CASE event.id OF

		;********************************************
		;**** Inform cw_adas701_outsel if output ****
                ;**** directory is changed               ****
		;********************************************

        state.dirnameid: begin	
        	
            widget_control, state.dirnameid, get_value = dir
            
            file_acc,dir(0),fileexist,read,write,execute,filetype
            
            if filetype eq 'd' then begin

               if widget_info(state.renameid, /valid_id) then begin
                   renameEvent = {renameNew,                $
                                  ID      : parent,         $
                                  TOP     : event.top,      $
                                  HANDLER : 0L,             $
                                  NAME    : dir(0)          }
                   widget_control, state.renameid, send_event=renameEvent
                endif
            
            endif else begin
            
              message = 'Not a valid directory'
	      widget_control,state.messid,set_value = message            
            
            endelse
         
        end
	
		;********************************************
		;**** new type of run selected - change  ****
                ;**** default output files               ****
		;********************************************

	state.iopt0id: begin

           widget_control, state.renameid, get_value = ren_val
	   widget_control, state.iopt0id, set_uvalue = 1
	   widget_control, state.iopt1id, set_uvalue = 0
	   widget_control, state.iopt2id, set_uvalue = 0
           if ren_val.cpl eq 'L' then begin
              ren_val.new_sel = [1,0,0,0,0,0,0,0,1,0,0,0]
           endif else begin
              ren_val.new_sel = [1,0,0,0,0,0,0,0,1,1,0,0]
           endelse
           ren_val.iopt    = 0
           ren_val.new_list= state.file_list.list
           widget_control, state.renameid, set_value = ren_val

	end	

	state.iopt1id: begin

           widget_control, state.renameid, get_value = ren_val
	   widget_control, state.iopt0id, set_uvalue = 0
	   widget_control, state.iopt1id, set_uvalue = 1
	   widget_control, state.iopt2id, set_uvalue = 0
           
           if ren_val.cpl eq 'L' then begin
              if ren_val.form eq 'F' then begin
                  ren_val.new_sel = [1,1,0,0,0,0,0,0,0,0,0,0]
               endif else begin
                  ren_val.new_sel = [1,0,0,1,0,0,0,0,0,0,0,0]
               endelse
           endif else begin
              if ren_val.form eq 'F' then begin
                  ren_val.new_sel = [1,0,1,0,0,0,0,0,0,0,0,0]
               endif else begin
                  ren_val.new_sel = [1,0,0,0,1,0,0,0,0,0,0,0]
               endelse
           endelse
           
           ren_val.iopt    = 1
           ren_val.new_list= state.file_list.list
           widget_control, state.renameid, set_value = ren_val

	end	

	state.iopt2id: begin

           widget_control, state.renameid, get_value = ren_val
	   widget_control, state.iopt0id, set_uvalue = 0
	   widget_control, state.iopt1id, set_uvalue = 0
	   widget_control, state.iopt2id, set_uvalue = 1
           
           if ren_val.cpl eq 'L' then begin
              if ren_val.form eq 'F' then begin
                  ren_val.new_sel = [1,1,0,0,0,0,0,0,0,0,0,0]
               endif else begin
                  ren_val.new_sel = [1,0,0,1,0,0,0,0,0,0,0,0]
               endelse
           endif else begin
              if ren_val.form eq 'F' then begin
                  ren_val.new_sel = [1,0,1,0,0,0,0,0,0,0,0,0]
               endif else begin
                  ren_val.new_sel = [1,0,0,0,1,0,0,0,0,0,0,0]
               endelse
           endelse
           
           ren_val.iopt    = 2
           ren_val.new_list= state.file_list.list
           widget_control, state.renameid, set_value = ren_val

	end	



		;***********************
		;**** Cancel button ****
		;***********************

        state.cancelid: new_event = {ID      : parent,    $
                                     TOP     : event.top, $
		                     HANDLER : 0L, 	  $
                                     ACTION  : 'Cancel'   }

		;*****************************
		;**** Run In Batch button ****
		;*****************************

       state.batid: begin

		;****************************************************
                ;**** Return the state before checking can start ****
		;**** with the get_value keyword.                ****
		;****************************************************

          widget_control, parent, set_uvalue=state, /no_copy

		;***************************************
		;**** Check for errors in the input ****
		;***************************************

	  error = 0

		;**** Get a copy of the widget value ****

	  widget_control, event.handler, get_value=os
	
		;**** Check for widget error messages ****

          mess = ''
	  if (os.texout eq 1 and strtrim(os.texmes) ne '') then error=1

		;**** Check for at least one AUTOSTRUCTURE file output ****
	   
           err = where(os.new_sel eq 1, count)
           if count eq 0 then begin
               error = 1
               mess  = 'Choose at least one AUTOSTRUCTURE output file'
           endif 

                ;**** Retrieve the state   ****

          widget_control, parent, get_uvalue=state, /no_copy

	  if error eq 1 then begin
	      if mess eq '' then mess = '**** Error in output settings ****'
	      widget_control,state.messid,set_value = mess
	      new_event = 0L
	  endif else begin
	      new_event = {ID      : parent, 		$
                           TOP     : event.top,		$ 
                           HANDLER : 0L, 	  	$
	                   ACTION  : 'Run In Batch'	}
	  endelse

        end

		;************************
		;**** Run Now button ****
		;************************

      state.runid: begin

		;****************************************************
                ;**** Return the state before checking can start ****
		;**** with the get_value keyword.                ****
		;****************************************************

          widget_control, parent, set_uvalue=state, /no_copy

		;***************************************
		;**** Check for errors in the input ****
		;***************************************

	  error = 0

		;**** Get a copy of the widget value ****

	  widget_control, event.handler, get_value=os

		;**** Check for widget error messages ****

	  mess=''
	  if (os.texout eq 1 and strtrim(os.texmes) ne '') then error=1

		;**** Check for at least one AUTOSTRUCTURE file output ****
	   
           err = where(os.new_sel eq 1, count)
           if count eq 0 then begin
               error = 1
               mess  = 'Choose at least one AUTOSTRUCTURE output file'
           endif 

                ;**** Retrieve the state   ****

          widget_control, parent, get_uvalue=state, /no_copy

	  if error eq 1 then begin
	      if mess eq '' then mess = '**** Error in output settings ****'
	      widget_control, state.messid, set_value= mess
	      new_event = 0L
	  endif else begin
	      new_event = {ID      : parent,	$ 
                           TOP     : event.top, $
                           HANDLER : 0L, 	$
	                   ACTION  : 'Run Now'  }
	  endelse

        end

    ELSE: new_event = 0L

  ENDCASE


                ;**** Return the state   ****

    widget_control, parent, set_uvalue=state, /no_copy

    RETURN, new_event

END

;-----------------------------------------------------------------------------

FUNCTION cw_adas701_out, parent, dsfull, file_list, form, cpl,    $
		         VALUE=value, UVALUE=uvalue, FONT=font


    IF (N_PARAMS() EQ 0) THEN MESSAGE, 'Must specify parent for cw_adas701_out'

    		;**** return to caller if there is an error ****

    ON_ERROR, 2					
		;**** Set number of AUTOSTRUCTURE output files ****

     nout = n_elements(file_list.list)

		;**** Set defaults for keywords ****

    IF NOT (KEYWORD_SET(devlist)) THEN devlist = ''
    IF NOT (KEYWORD_SET(uvalue))  THEN uvalue = 0
    IF NOT (KEYWORD_SET(font))    THEN font = ''
    IF NOT (KEYWORD_SET(value))   THEN begin
        str_arr = strarr(nout)
        int_arr = intarr(nout)
	os = { out701_set, 						$
               new_list  :  str_arr,					$
               new_sel   :  int_arr,					$
               new_dir   :  '',						$
               message   :  '',	  				 	$
	       form      :  'F',                                        $
	       cpl       :  'L',                                        $
	       iopt      :  0,                                          $
	       texout    :  0,                                          $
               texapp    : -1,                                          $
	       texrep    :  0,                                          $
               texdsn    : '',                                          $
               texdef    : 'paper.txt',                                 $
               texmes    : '',                                          $
               dirname   : '',                                          $
               title     : ''                                           }
    ENDIF ELSE BEGIN
	os = { out701_set, 						$
               new_list  : value.new_list,				$
               new_sel   : value.new_sel,				$
	       new_dir   : value.new_dir,				$
               message   : value.message,				$
	       form      : value.form,                                  $
	       cpl       : value.cpl,                                   $
	       iopt      : value.iopt,                                  $
	       texout    : value.texout,                                $
               texapp    : value.texapp,                                $
	       texrep    : value.texrep,                                $
               texdsn    : value.texdsn,                                $
	       texdef    : value.texdef,                                $
               texmes    : value.texmes,                                $
               dirname   : value.dirname,                               $
               title     : value.title                                  }
    ENDELSE

    fontcom = font

		;***************************************************
		;**** Modify format/coupling output if changed. ****
                ;**** Swap ls and ic and u and non-u filenames. ****
                ;**** (The new values are passed from FORTRAN). ****
		;***************************************************
    
                ; swap  1 with 2 ols  and oic
                ;       3      4 olsu and oicu
                ;      10     11 opls and opic
                
; recall    file_list.cpl_tgl  = [ [1,2],[3,4],[10,11] ]
;           file_list.form_tgl = [ [1,3],[2,4] ]
    
    if cpl ne value.cpl then begin
       
       for j = 0, 2 do begin
          itmp = os.new_sel(file_list.cpl_tgl(0,j))
          os.new_sel(file_list.cpl_tgl(0,j)) = os.new_sel(file_list.cpl_tgl(1,j))
          os.new_sel(file_list.cpl_tgl(1,j)) = itmp
       endfor
       
    endif
    os.cpl = cpl
       
    if form ne value.form then begin
       
       for j = 0, 1 do begin
          itmp = os.new_sel(file_list.form_tgl(0,j))
          os.new_sel(file_list.form_tgl(0,j)) = os.new_sel(file_list.form_tgl(1,j))
          os.new_sel(file_list.form_tgl(1,j)) = itmp
       endfor
       
    endif
    os.form = form
    
; if coupling is IC switch on TERMS if LEVELS is on 
; if coupling is LS and TERMS is on switch it off

    if  (cpl eq 'I') and (os.new_sel(8) eq 1) then os.new_sel(9) = 1   
    if  (cpl eq 'L') and (os.new_sel(9) eq 1) then os.new_sel(9) = 0   



		;**************************************************
		;**** Now check the files to put up a warning  ****
		;**** if they already exist.                   ****
		;**************************************************
                
; Note that the user can still continue -- the warning does not stop the 
; process. It is put up here to catch the case where cw_adas701_outsel is
; not interacted with at all. ie the user accepts the previous/default values,
; which may result in the overwriting of the files. 
;
; Note : stop after the first warning

    message = ' '
    i = 0
    while (strtrim(message,2) eq '') and (i le nout-1) do begin

       if os.new_sel(i) eq 1 then begin

          filename = os.new_list(i)
          dirname  = os.new_dir

          test_file, dirname, filename,  message
	  os.message = message                    ; message in cw_adas701_outsel

       endif
       
       i=i+1
             
    endwhile            

     






		;**********************************************
		;**** Create the 701 Output options widget ****
		;**********************************************

		;**** create base widget ****

  cwid = widget_base( parent, UVALUE = uvalue, 			$
		      EVENT_FUNC = "out701_event", 		$
		      FUNC_GET_VALUE = "out701_get_val", 	$
		      /COLUMN)

		;**** Add dataset name and browse button ****

  rc = cw_adas_dsbr(cwid, dsfull, font=font)

		;***********************
		;**** add run title ****
		;***********************

  base = widget_base(cwid, /row)
  rc = widget_label(base, value='Title for Run', font=font)
  titid = widget_text(base, value=value.title, font=font, /edit)


		;***********************************
   		;**** Widget for directory name ****
		;***********************************
                
  base = widget_base(cwid,/column,/frame)
  l = widget_label(base, value='Directory for AUTOSTRUCTURE file output:', $ 
                   font=font)
  dirnameid = widget_text(base,value=os.dirname,font=font,xsize=40,/editable)


		;************************************
		;**** Add the run type selector ****
		;************************************


  wrow = widget_base(base, /row)
  wcol = widget_base(wrow, /column)

  l0 = widget_label(wcol, value='- Default file choice -', $ 
                      font=font)
  wr_but = widget_base(wcol, /row)
  l1 = widget_label(wr_but, value='Select type of run :', $ 
                      font=font)
  wc_but = widget_base(wr_but, /column)
  wr_but = widget_base(wr_but, /row, /exclusive) 

                      
  iopt0id = widget_button(wr_but, value='Structure', font=font,$
                          /no_release)
  iopt1id = widget_button(wr_but, value='Satellite', $
                          font=font, /no_release)
  iopt2id = widget_button(wr_but, value='Rates', $
                           font=font, /no_release)


		;*************************************
		;**** Rename output files Options ****
		;*************************************

  l2 = widget_label(base, value='     ', $ 
                      font=font)
  l3 = widget_label(base, value='Activate the following files:', $ 
                      font=font)

        
    mcval = { new_list   : os.new_list,     $
              new_sel    : os.new_sel,      $
              new_dir    : os.new_dir,      $
              message    : os.message,      $
              message_2  : '',              $
              form       : os.form,         $
              cpl        : os.cpl,          $
              iopt       : os.iopt          }

    renameid = cw_adas701_outsel(base,file_list,                   $ 
                                 output='What to delete',	   $
                                 nrow=6,value=mcval,font=font)



		;********************************
		;**** Widget for text output ****
		;********************************

    outfval = { OUTBUT   : os.texout, 	$
                APPBUT   : -1, 		$
                REPBUT   : os.texrep, 	$
	        FILENAME : os.texdsn, 	$
                DEFNAME  : os.texdef, 	$
                MESSAGE  : os.texmes 	}
                
    base = widget_base(cwid,/row,/frame)
    paperid = cw_adas_outfile(base, OUTPUT='Text Output',   $
                              VALUE=outfval, FONT=font)


		;***********************
		;**** Error message ****
		;***********************

    messid = widget_label(cwid,value=' ', font=font)



		;******************************
		;**** add the exit buttons ****
		;******************************

    base     = widget_base(cwid, /row)
    cancelid = widget_button(base, value='Cancel', font=font)
    runid    = widget_button(base, value='Run Now', font=font)
    batid    = widget_button(base, value='Run in Batch', font=font)
  
  
; set buttons and options depending on the type of run
         	 
	case os.iopt of
	  0 : begin
	  	widget_control, iopt0id, set_uvalue = 1
	  	widget_control, iopt1id, set_uvalue = 0
	  	widget_control, iopt2id, set_uvalue = 0
	  	widget_control, iopt0id, set_button = 1
	  	widget_control, iopt1id, set_button = 0
	  	widget_control, iopt2id, set_button = 0
	      end
	  1 : begin
	  	widget_control, iopt0id, set_uvalue = 0
	  	widget_control, iopt1id, set_uvalue = 1
	  	widget_control, iopt2id, set_uvalue = 0
	  	widget_control, iopt0id, set_button = 0
	  	widget_control, iopt1id, set_button = 1
	  	widget_control, iopt2id, set_button = 0
	      end
	  2 : begin
	  	widget_control, iopt0id, set_uvalue = 0
	  	widget_control, iopt1id, set_uvalue = 0
	  	widget_control, iopt2id, set_uvalue = 1
	        widget_control, iopt0id, set_button = 0
	  	widget_control, iopt1id, set_button = 0
	  	widget_control, iopt2id, set_button = 1
	      end
	endcase
	
		;*************************************************
		;**** create a state structure for the pop-up ****
		;**** window.                                 ****
		;*************************************************

    new_state = { paperid       :       paperid,        $
		  renameid      :       renameid,       $
    		  batid         :       batid,          $
		  cancelid      :       cancelid,       $
                  runid         :       runid,          $
                  messid        :       messid,         $
                  titid         :       titid,          $
                  dirnameid     :       dirnameid,      $
		  iopt0id	: 	iopt0id,	$
		  iopt1id	: 	iopt1id,	$
		  iopt2id	: 	iopt2id,	$	
                  file_list	:	file_list	}

                ;**** Save initial state structure ****

    widget_control, parent, set_uvalue=new_state, /no_copy

    RETURN, cwid

END

