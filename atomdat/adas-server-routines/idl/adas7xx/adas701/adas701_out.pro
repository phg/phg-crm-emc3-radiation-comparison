; Copyright (c) 1998 Strathclyde University .
; SCCS info: Module @(#)$Header: /home/adascvs/idl/adas7xx/adas701/adas701_out.pro,v 1.2 2004/07/06 11:03:35 whitefor Exp $ Date $Date: 2004/07/06 11:03:35 $
;+
; PROJECT:
;       ADAS  
;
; NAME:
;	ADAS701_OUT
;
; PURPOSE:
;	IDL ADAS user interface, output options.
;
; EXPLANATION:
;	This routine creates and manages a pop-up window which allows
;	the user to select options and input data to control ADAS701
;	file output.
;
; USE:
;	This routine is specifically for use with adas701.             
;
; INPUTS:
;	VAL	- A structure which determines the initial settings of
;		  the output options widget.  The value is passed
;		  unmodified into cw_adas701_out.pro.
;
;		  See cw_adas701_out.pro for a full description of this
;		  structure.
;
;	DSFULL	- String; The full system file name of the input 
;		  dataset selected by the user for processing.
;
; OPTIONAL INPUTS:
;	None.
;
; OUTPUTS:
;	ACT	- String; Either 'Done' or 'Cancel' for the button the
;		  user pressed to terminate the output options window.
;
; OPTIONAL OUTPUTS:
;	None.
;
; KEYWORD PARAMETERS:
;	FONT	- Supplies the font to be used for the interface widgets.
;
; CALLS:
;	CW_ADAS701_OUT	Creates the output options widget.
;	XMANAGER
;	See side effects for widget management routine.
;
; SIDE EFFECTS:
;	This routine uses a common block OUT701_BLK to maintain its state.
;	ADAS701_OUT_EV	is included in this file and is called
;	indirectly from XMANAGER during widget management.
;
; CATEGORY:
;	Compound Widget.
;
; WRITTEN:
;       Martin O'Mullane 
;
; MODIFIED:
;	1.1	Martin O'Mullane         05/04/1998
;	1.2	Richard Martin	
;		Removed 'dynlabel, outid' statement.
;	
; VERSION:
;	1.1	First Release
;	1.2	24-11-98	
;-
;-----------------------------------------------------------------------------


pro adas701_out_ev, event

  common out701_blk,action,value
	

		;**** Find the event type and copy to common ****

    action = event.action

    case action of

		;**** 'Run Now' button ****

	'Run Now'  : begin

			;**** Get the output widget value ****

     		     child = widget_info(event.id, /child)
		     widget_control, child, get_value=value 

                        ;*****************************************
			;**** Kill the widget to allow IDL to ****
			;**** continue and interface with     ****
			;**** FORTRAN.                        ****
			;*****************************************
                        
                        widget_control, event.top, /destroy

	          end


                ;**** 'Run in Batch' button ****

        'Run In Batch'  : begin

                        ;**** Get the output widget value ****

                     child = widget_info(event.id, /child)
                     widget_control, child, get_value=value

                        ;*****************************************
                        ;**** Kill the widget to allow IDL to ****
                        ;**** continue and interface with     ****
                        ;**** FORTRAN.			      ****
                        ;*****************************************
                        
                        widget_control, event.top, /destroy
                  end

		;**** 'Cancel' button ****

	'Cancel': widget_control, event.top, /destroy

        ELSE: begin   				
			;do nothing 
              end
    endcase

end

;-----------------------------------------------------------------------------


pro adas701_out, val, dsfull, file_list, form, cpl, $ 
                 act,  font=font

  common out701_blk, action, value

		;**** Copy value to common ****

  value = val

		;**** Set defaults for keywords ****

  if not (keyword_set(font)) then font = ''

		;***************************************
		;**** Pop-up a new widget           ****
		;***************************************

                ;**** create base widget ****

  outid = widget_base(title='ADAS701 OUTPUT OPTIONS', xoffset=100, yoffset=100)

		;**** Declare output options widget ****

  cwid = cw_adas701_out(outid, dsfull, file_list, form, cpl,  $ 
                        value=value, font=font )

		;**** Realize the new widget ****

  widget_control, outid, /realize

		;**** make widget modal ****

  xmanager,'adas701_out', outid, event_handler='adas701_out_ev',	$
                          /modal, /just_reg
 
		;**** Return the output value from common ****

  act = action
  val = value

END

