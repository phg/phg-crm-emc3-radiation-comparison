; Copyright (c) 1998 Strathclyde University .
; SCCS info: Module @(#)$Header: /home/adascvs/idl/adas7xx/adas702/adas702.pro,v 1.6 2004/07/06 11:03:58 whitefor Exp $ Date $Date: 2004/07/06 11:03:58 $
;+
; PROJECT:
;       ADAS IBM MVS to UNIX conversion
;
; NAME:
;	ADAS702
;
; PURPOSE:
;	The highest level routine for the ADAS 702 program.
;
; EXPLANATION:
;	This routine is called from the main adas system routine, adas.pro,
;	to start the ADAS 702 application.  Associated with adas702.pro
;	is a FORTRAN executable, adas702.out.  The IDL code provides the user
;	interface and output graphics whilst the FORTRAN code reads
;	in data files, performs numerical processing and creates the
;	output files.  The IDL code communicates with the FORTRAN
;	executable via a bi-directional UNIX pipe.  The unit number
;	used by the IDL for writing to and reading from this pipe is
;	allocated when the FORTRAN executable is 'spawned' (see code
;	below).  Pipe communications in the FORTRAN process are to
;	stdin and stdout, i.e streams 5 and 6.
;
;	The FORTRAN code is an independent process under the UNIX system.
;	The IDL process can only exert control over the FORTRAN in the
;	data which it communicates via the pipe.  The communications
;	between the IDL and FORTRAN must be exactly matched to avoid
;	input conversion errors.  The correct ammounts of data must be
;	passed so that neither process 'hangs' waiting for communications
;	which will never occur.
;
;	The FORTRAN code performs some error checking which is
;	independent of IDL.  In cases of error the FORTRAN may write
;	error messages.  To prevent these error messages from conflicting
;	with the pipe communications all FORTRAN errors are written to
;	output stream 0, which is stderr for UNIX.  These error messages
;	will appear in the window from which the ADAS session/IDL session
;	is being run. In the case of the batch job, the error messages
;	will appear as standard cron job output which is usually
;	emailed to the user. If the job has completed succesfully then
;	the user will get a message telling them so.
;
;	In the case of severe errors the FORTRAN code may terminate
;	itself prematurely.  In order to detect this, and prevent the
;	IDL program from 'hanging' or crashing, the IDL checks to see
;	if the FORTRAN executable is still an active process before
;	each group of pipe communications.  The process identifier
;	for the FORTRAN process, PID, is recorded when the process is
;	first 'spawned'.  The system is then checked for the presence
;	of the FORTRAN PID. This checking does not occur for the 
;	batch cases.
;
; USE:
;	First the system settings must be established by calling
;	adas_sys_set.pro then adas702.pro is called to start the
;	ADAS 702 application;
;
;	adas_sys_set, adasrel, fortdir, userroot, centroot, 		$
;		      devlist, devcode, font_large, font_small, edit_fonts
;	adas702,   adasrel, fortdir, userroot, centroot, devlist, 	$
;		   devcode, font_large, font_small, edit_fonts
;
; INPUTS:
;	ADASREL - A string indicating the ADAS system version, 
;		  e.g ' ADAS RELEASE: ADAS93 V1.1.0'.  The first
;		  character should be a space.
;
;	FORTDIR - A string holding the path to the directory where the
;		  FORTRAN executables are, e.g '/disk/adas/fortran/exec'
;
;	USERROOT - A string holding the path to the root directory of
;		   the user's adas data. e.g '/disk/bowen/adas'
;		   This root directory will be used by adas to construct
;		   other path names.  For example the users default data
;		   for adas205 should be in /disk/bowen/adas/adf04.  In
;		   particular the user's default interface settings will
;		   be stored in the directory USERROOT+'/defaults'.  An
;		   error will occur if the defaults directory does not
;		   exist.
;
;	CENTROOT - Like USERROOT, but this directory points to the
;		   central data area for the system.  User defaults are
;		   not stored on CENTROOT.
;
;	DEVLIST - A string array of hardcopy device names, used for
;		  graphical output. e.g ['Post-Script','HP-PCL','HP-GL']
;		  This array must mirror DEVCODE.  DEVCODE holds the
;		  actual device names used in a SET_PLOT statement.
;
;	DEVCODE - A string array of hardcopy device code names used in
;		  the SET_PLOT statement, e.g ['PS', 'PCL', 'HP']
;		  This array must mirror DEVLIST.
;
;	FONT_LARGE - The name of a larger font e.g 'courier_bold14'
;
;	FONT_SMALL - The name of a smaller font e.g 'courier_bold12'
;
;	EDIT_FONTS - A structure of two fonts used in the table editor
;		     adas_edtab.pro, {font_norm:'',font_input:''}.
;		     The two fonts are used to differentiate between
;		     editable and non-editable parts of the table. You
;		     may have to experiment to get two fonts which give
;		     the same default spacing in the edit widget.
;		     e.g {font_norm:'helvetica_bold14', $
;			  font_input:'helvetica_oblique14'}
;
; OPTIONAL INPUTS:
;	None
;
; OUTPUTS:
;	None - Note this routine should leave all inputs un-modified.
;
; OPTIONAL OUTPUTS:
;	None
;
; KEYWORD PARAMETERS:
;	None
;
; CALLS:
;	FIND_PROCESS	Checks to see if a given process is active.
;       XXDATE          Get date and time from operating system.
;       G2SPF0          Pipe comms with FORTRAN G2SPF0 routine.
;       G2SPF1          Pipe comms with FORTRAN G2SPF1 routine.
;       G2SPF2          Pipe comms with FORTRAN G2SPF2 routine.
;	G2PROGBAR       Updates the batch files and variables for
;			the batch runs depending on the user selections.
;
; SIDE EFFECTS:
;	This routine spawns FORTRAN executables.  Note the pipe 
;	communications routines listed above. 
;
; CATEGORY:
;	Adas system.
;	
; WRITTEN:
;       Martin O'Mullane
;
; MODIFIED:
;	1.1	Martin O'Mullane	
;		First release
;	1.2	Richard Martin
;		Increased version no. to 1.2
;	1.3	Richard Martin
;		Increased version no. to 1.3
;	1.4	Richard Martin
;		Increased version no. to 1.4
;	1.5	Richard Martin
;		Increased version no. to 1.5
;	1.6	Martin O'Mullane	
;                - ADASDR opens input file differently. Try a temporary 
;                  solution before a more elegant method.
;		 - Increased version no. to 1.6
;
; VERSION:
;       1.1     16-06-98
;	1.2	19-03-99
;	1.3	15-10-99
;	1.4	20-22-01
;	1.5	16-07-02
;	1.6	28-08-2003
;	
;-----------------------------------------------------------------------------


PRO ADAS702,	adasrel, fortdir, userroot, centroot, 			$
		devlist, devcode, font_large, font_small, edit_fonts

		;************************
		;**** Initialisation ****
		;************************

    adasprog = ' PROGRAM: ADAS702 V1.6'
    lpend    = 0
    ipset    = 0
    ipbatset = 0
    l2batch  = 0			;Marker for whether a batch file
					;has been created.
                                        
    deffile  = userroot+'/defaults/adas702_defaults.dat'
    device   = ''
        
                ;**** Set number of AUTOSTRUCTURE input files ****

    ndas = 4

		;******************************************
		;**** Search for user default settings ****
		;**** If not found create defaults     ****
		;******************************************

    files = findfile(deffile)
    if files(0) eq deffile then begin

        restore,deffile
        inval.centroot = centroot+'/adf28/'
        inval.userroot = userroot+'/adf28/'

    end else begin

        str_arr = strarr(ndas)
        
        inval = { 							$
		 ROOTPATH  : userroot+'/adf28/',                 	$
		 FILE      : '',                                 	$
		 CENTROOT  : centroot+'/adf28/',                 	$
		 USERROOT  : userroot+'/adf28/'                  	}
       
       inval2 = {                                                       $
                 new_list  : str_arr,					$
                 new_dir   :  '',					$
                 message   :  '',                                       $
                 dirname   :  userroot+'/autos/',                       $
                 title     : ''                                         }
                 
       pasdefval = userroot + '/pass/adas702_adf09.pass'

       outval = { 							$
		 TEXOUT	   :  0, 					$
                 TEXAPP    : -1,					$
		 TEXREP    :  0, 					$	
                 TEXDSN    : '',					$
		 TEXDEF    : 'paper.txt', 				$
                 TEXMES    : '',					$
                 DIRNAME   : userroot+'/autos/',     		        $
                 PASOUT    :  0,                                        $
                 PASAPP    : -1,                                        $
                 PASREP    :  0,                                        $
                 PASDSN    : '',                                        $
                 PASDEF    : pasdefval,                                 $
                 PASMES    : '',                                        $
		 TITLE     : ''						}

    end


		;****************************
		;**** Start fortran code ****
		;****************************

    spawn, fortdir+'/adas702.out', unit=pipe, /noshell, PID=pid

		;************************************************
		;**** Write to fortran via pipe that we are  ****
		;**** running interactively                  ****
		;************************************************

    ibatch = 0
    printf, pipe, ibatch

		;******************
		;**** Get date ****
		;******************

    date = xxdate()
    printf, pipe, date(0)
    
LABEL100:
		;**** Check FORTRAN still running ****
  
    if find_process(pid) eq 0 then goto, LABELEND

		;************************************************
		;**** Communicate with g2spf0 in fortran and ****
		;**** invoke user interface widget for       ****
		;**** Data file selection                    ****
		;************************************************

    g2spf0, pipe, inval, dsfull, rep, FONT=font_large


    ipset    = 0
    ipbatset = 0

		;**** If cancel selected then end program ****

    if rep eq 'YES' then goto, LABELEND


LABEL200:

    if find_process(pid) eq 0 then goto, LABELEND

                ;************************************************
                ;**** Communicate with g2spf1 in fortran and ****
                ;**** invoke user interface widget for adsdr ****
                ;**** input file options.                    ****
                ;************************************************
    g2spf1, pipe, lpend, ndas, inval2, dsfull,                        $ 
            header, FONT=font_large



  ; Bit of a kludge to accommodate Nigel's new (v1.10) way
  ; of opening the adasdr input files
  
  ind = where(inval2.new_list NE '', count)
  
  for j = 0, count-1 do begin
     cmd = 'ln -s ' + inval2.new_dir + '/' + inval2.new_list[j]
     spawn, cmd
  endfor


                ;**** If cancel selected then go back ****

    if lpend eq 1 then goto, LABEL100


LABEL300:
		;**** Check FORTRAN still running ****

    if find_process(pid) eq 0 then goto, LABELEND

		;************************************************
		;**** Communicate with g2spf1 in fortran and ****
		;**** invoke user interface widget for       ****
		;**** Output options                         ****
		;************************************************

    
    g2spf2, pipe, lpend, outval, dsfull,                        $ 
            header, FONT=font_large

		;**** If cancel selected then erase output ****
		;**** messages and goto 100.		   ****

    if lpend eq 1 then begin
        outval.texmes  = ''
        outval.pasmes  = ''
        goto, LABEL100
    end


		;*************************************************
		;**** Fortran processing now in progress. Get ****
		;**** date and time for this run.	      ****
		;*************************************************

    date = xxdate()

		;**** Create header for output. ****

    header = adasrel+adasprog+' DATE: '+date(0)+' TIME: '+date(1)


		;***********************************************
		;**** Now either begin the calculation or   ****
		;**** set up the batch file depending on    ****
		;**** what action has been requested.  	    ****
                ;***********************************************

    if lpend eq 0 then begin			;'Run Now' selected
        ipset = ipset + 1			; Increase 'run number'
        g2progbar, pipe, FONT=font_large
    endif else if lpend eq 2 then begin		;'Run in batch' selected
        ipbatset = ipbatset + 1			;Increase run number

        ;**** Kick the batch job off ****

        rand = strmid(strtrim(string(randomu(seed)), 2), 2, 4)
        dscontrol = 'control'+rand+'.tmp'
        dsinfo = 'info'+rand+'.tmp'
        openw, conunit, dscontrol, /GET_LUN
        openw, infunit, dsinfo, /GET_LUN
        printf, conunit, fortdir+'/adas702.out < '+dsinfo 
        printf, conunit, 'rm -f '+dsinfo 
        printf, conunit, 'rm -f '+dscontrol

 	;**** Write stuff needed by adas702 - it will be read into stdin ****

        yesbatch = 1
	repn = 'NO'
	repy = 'YES'
	lpend = 0
        printf, infunit, yesbatch
        printf, infunit, date(0)
        
	;**** Input screen, g2spf0, pipe info ****
        printf, infunit, repn
        printf, infunit, dsfull

	;**** Input screen, g2spf1, pipe info ****
        printf, infunit, lpend
        printf, infunit, inval2.dirname
        for j = 0,n_elements(inval2.new_list)-1 do begin
           printf, infunit, inval2.new_list(j)
        endfor

	;**** Output screen, g2spf2, pipe info ****
        printf, infunit, lpend
	printf, infunit, outval.title
	printf, infunit, outval.texdsn
	printf, infunit, outval.pasdsn
	printf, infunit, outval.texout
	printf, infunit, outval.pasout
	printf, infunit, lpend

	;**** Input screen again - reply yes => 'cancel' ****
        printf, infunit, repy
        printf, infunit, dsfull

        close, infunit, conunit
        spawn, 'chmod u+x '+dscontrol

        batch, dscontrol, title='ADAS702: INFORMATION', jobname='NO. '+rand

        l2batch = 0

    endif


		;**** Back for more input options ****
                
    outval.texmes  = ''
    outval.pasmes  = ''

    GOTO, LABEL100

LABELEND:

		;**** Save user defaults ****

    outval.texmes = ''
    outval.pasmes = ''
    
    save, inval, inval2, outval, filename=deffile


; Remove linked files and close up all open units

    for j = 0, count-1 do begin
       cmd = 'rm -f ' + inval2.new_list[j]
       spawn, cmd
    endfor

    close, /all
END
