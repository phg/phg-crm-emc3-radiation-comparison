; Copyright (c) 1998 Strathclyde University .
; SCCS info: Module @(#)$Header: /home/adascvs/idl/adas7xx/adas702/g2spf2.pro,v 1.1 2004/07/06 13:56:37 whitefor Exp $ Date $Date: 2004/07/06 13:56:37 $
;+
; PROJECT:
;       ADAS  
;
; NAME:
;	G1SPF2
;
; PURPOSE:
;	IDL user interface and communications with ADAS702 FORTRAN
;	process via pipe.
;
; EXPLANATION:
;       First this routine reads some information from ADAS702 FORTRAN
;       via the UNIX pipe.  Next the 'Output Options' part of the
;	interface is invoked.  The resulting action of the user's
;	interactions is written to the FORTRAN via the pipe.
;	Communications are with the FORTRAN subroutine G2SPF2.
;
; USE:
;	The use of this routine is specific to ADAS702, See adas702.pro.
;
; INPUTS:
;	PIPE	- The IDL unit number of the bi-directional pipe to the
;		  ADAS702 FORTRAN process.
;
;	VALUE	- A structure which determines the initial settings of
;		  the dataset selection widget.  The initial value is
;		  set in adas702.pro.  VALUE is passed un-modified
;		  through to cw_adas_in.pro, see that routine for a full
;		  description.
;
;	HEADER  - Header information used for text output.
;
; OPTIONAL INPUTS:
;	None.
;
; OUTPUTS:
;	LPEND	- Integer; indicates user action on leaving output
;		  selections.  lpend is set to 1 if the user exited
;		  with the 'Cancel' button, 0 if the user exited
;		  with 'Run Now' and 2 if the user exited with 'Run in 
;		  Batch'.
;
;	VALUE	- On output the structure records the final settings of
;		  the output selection widget if the user pressed either
;		  of the 'Run' buttons otherwise it is not changed from input.
;
; OPTIONAL OUTPUTS:
;	None.
;
; KEYWORD PARAMETERS:
;
;	FONT	- Supplies the font to be used for the interface widgets.
;
; CALLS:
;	ADAS702_OUT	User interface - output options.
;
; SIDE EFFECTS:
;	This routine communicates with the ADAS702 FORTRAN process
;	via a UNIX pipe.
;
; CATEGORY:
;	Adas system.
;
; WRITTEN:
;       Martin O'Mullane
;
; MODIFIED:
;	1.1	Martin O'Mullane	
;		First release.
;
; VERSION:
;       1.1     16/05/1998
;
;-
;-----------------------------------------------------------------------------

PRO   g2spf2, pipe, lpend, value, dsfull, $
              header, FONT=font

                ;**** Return to caller on error ****

    ON_ERROR, 2

                ;**** Set defaults for keywords ****

    IF NOT (KEYWORD_SET(font)) THEN font = ''


  
		;**************************************
		;**** Pop-up output options widget ****
		;**************************************

    outtitle = "ADAS702 OUTPUT OPTIONS"
    adas702_out, value, action, dsfull, FONT=font

		;*************************************************
		;**** Act on the output from the widget       ****
		;**** There are three    possible actions     ****
		;**** 'Cancel', 'Run Now' and 'Run in Batch'. ****
		;*************************************************

    if action eq 'Run Now' then begin
        lpend = 0
    endif else if action eq 'Run In Batch' then begin
        lpend = 2
    endif else begin
        lpend = 1
    end

		;*******************************
		;**** Write data to fortran ****
		;*******************************

    printf, pipe, lpend

    if (lpend eq 0) or (lpend eq 2) then begin

	printf, pipe, value.title
        
	printf, pipe, value.texdsn
	printf, pipe, value.pasdsn
        
        printf, pipe, value.texout
        printf, pipe, value.pasout
        
        yesbatch = 1
        nobatch = 0
        if lpend eq 2 then printf, pipe, yesbatch	$
	else printf, pipe, nobatch

		;**************************************************
		;**** Set messages and settings ready for when ****
		;**** output window is re-displayed.           ****
		;**************************************************

	if value.pasout eq 1 then begin
            if value.pasrep ge 0 then value.pasrep = 0
	    value.pasmes = 'Output written to pass file.'
        endif
	if value.texout eq 1 then begin
            if value.texrep ge 0 then value.texrep = 0
	    value.texmes = 'Output written to file.'
        endif
    endif

END
