; Copyright (c) 1998 Strathclyde University .
; SCCS info: Module @(#)$Header: /home/adascvs/idl/adas7xx/adas702/cw_adas702_in.pro,v 1.1 2004/07/06 12:55:17 whitefor Exp $ Date $Date: 2004/07/06 12:55:17 $
;+
; PROJECT:
;       ADAS IBM MVS to UNIX conversion
;
; NAME:
;	CW_ADAS702_IN()
;
; PURPOSE:
;	Produces a widget for ADAS702 input options.
;
; EXPLANATION:
;       This function declares a compound widget consisting of an input
;       passing file directory selection widget and an input file widget
;       cw_adas_infile.pro.  Use this widget to select the directory for
;       the AUTOSTRUCTURE output files necessary for ADASDR (nominally
;       o1/o2/o3/o1u etc) This widget also includes a button for browsing
;       the comments from the input dataset, a 'Cancel' button and a
;       'Done' and ' button. The compound widgets cw_adas_infile.pro
;       included in this file  are self managing.  This widget only
;       handles events from the 'Cancel' and 'Done' buttons.
;
;     
; USE:
;	This routine is specific to adas702.
;
; INPUTS:
;	PARENT	- Long integer; ID of parent widget.
;
;	INFOVAL	- String array; information abin the input datasets
;
;       BITFILE - String; the path to the dirctory containing bitmaps
;                 for the 'escape to series menu' button.
;
; OPTIONAL INPUTS:
;	None.
;
; INPUTS:
;	The return value of this function is the ID of the declared widget.
;
; OPTIONAL INPUTS:
;	None.
;
; KEYWORD PARAMETERS:
;
;	VALUE	- A structure which determines the initial settings of
;		  the input options widget. 
;	UVALUE	- A user value for this widget.
;
;	FONT	- String; the name of a font to be used for all text in
;		  this widget.  Default to system default font.
;
; CALLS:
;	CW_ADAS_INFILE	AUTOSTRUCTURE output file name entry widget.
;	See side effects for other related widget management routines.
;
; SIDE EFFECTS:
;
; 	Two other routines are included in this file
;	IN702_GET_VAL()
;	IN702_EVENT()
;       
;       Sends events destined for the cw_adas702_insel event handler.
;       An event is sent when dirname is changed.
;
; CATEGORY:
;	Compound Widget
;
; WRITTEN:
;       Martin O'Mullane
; MODIFIED:
;	1.1	Martin O'Mullane	
;		First release
;
; VERSION:
;       1.1	16-06-98
;
;-
;-----------------------------------------------------------------------------

FUNCTION in702_get_val, id

                ;**** Return to caller on error ****

    ON_ERROR, 2

                ;**** Retrieve the state ****

    parent=widget_info(id, /parent)
    widget_control, parent, get_uvalue=state, /no_copy

		;**** Get directory name ****

    widget_control, state.dirnameid, get_value = dir
    

		;***********************************
		;**** Get run title from widget ****
		;**** Then centre in in string  ****
		;**** of 40 characters 		****
		;***********************************

    widget_control, state.titid, get_value=title
    title = strcompress(title(0))
    title_len = strlen(title) 
    if (title_len gt 40 ) then begin 
        title = strmid(title, 0, 37)
        widget_control, state.messid, set_value="Title too long - truncated "
        widget_control, state.titid, set_value=title
	wait, 1
    endif
    pad = (40 - title_len)/2 
    spaces = '                                         '
    title(0) = strmid(spaces,0,(pad-1)) + title + strmid(spaces,0,(pad-1))


                ;********************************************
                ;**** Get AUTOSTRUCTURE input files from **** 
                ;**** cw_adas701_insel widget            ****
                ;********************************************

    widget_control, state.fileselid, get_value=filesel
    
    os = { in702_set, 				      $
           new_list   :	      filesel.new_list,       $
           new_dir    :	      filesel.new_dir,        $
           message    :	      filesel.message,        $
	   dirname    :       dir(0),                 $
           title      :       title                   $
         }

               ;**** Return the state ****

  widget_control, parent, set_uvalue=state, /no_copy

  RETURN, os 

END

;-----------------------------------------------------------------------------

FUNCTION in702_event, event

                ;**** Base ID of compound widget ****

    parent=event.top

                ;**** Retrieve the state ****

    widget_control, parent, get_uvalue=state, /no_copy

		;*********************************
		;**** Clear previous messages ****
		;*********************************

    widget_control, state.messid, set_value=' '

                ;************************
                ;**** Process events ****
                ;************************

    CASE event.id OF

                ;********************************************
                ;**** Inform cw_adas702_insel if input   ****
                ;**** directory is changed               ****
                ;********************************************

        state.dirnameid: begin
           
            widget_control, state.dirnameid, get_value = dir
            
            file_acc,dir(0),fileexist,read,write,execute,filetype
            
            if filetype eq 'd' then begin

               if widget_info(state.fileselid, /valid_id) then begin
                   fileselEvent = {fileselNew,               $
                                   ID      : parent,         $
                                   TOP     : event.top,      $
                                   HANDLER : 0L,             $
                                   NAME    : dir(0)          }
                   widget_control, state.fileselid, send_event=fileselEvent
                endif
                
            endif else begin
            
              message = 'Not a valid directory'
	      widget_control,state.messid,set_value = message            
            
            endelse

        end
	


		;***********************
		;**** Cancel button ****
		;***********************

        state.cancelid: new_event = {ID      : parent,    $
                                     TOP     : event.top, $
		                     HANDLER : 0L, 	  $
                                     ACTION  : 'Cancel'   }


		;*********************
		;**** Done button ****
		;*********************

       state.doneid: begin

		;****************************************************
                ;**** Return the state before checking can start ****
		;**** with the get_value keyword.                ****
		;****************************************************

          widget_control, parent, set_uvalue=state, /no_copy

		;***************************************
		;**** Check for errors in the input ****
		;***************************************

	  error = 0

		;**** Get a copy of the widget value ****

	  widget_control, event.handler, get_value=os
	
		;**** Check for widget error messages ****

          mess = ''
          
                ;**** Check for at least one AUTOSTRUCTURE file output ****
           
           one = where(strtrim(os.new_list,2) ne '', count)
           if count eq 0 then begin
               error = 1
               mess  = 'Choose at least one AUTOSTRUCTURE output file'
           endif 

                ;**** Retrieve the state   ****

          widget_control, parent, get_uvalue=state, /no_copy

	  if error eq 1 then begin
	      if mess eq '' then mess = '**** Error in input settings ****'
	      widget_control,state.messid,set_value = mess
	      new_event = 0L
	  endif else begin
	      new_event = {ID      : parent, 		$
                           TOP     : event.top,		$ 
                           HANDLER : 0L, 	  	$
	                   ACTION  : 'Done'		}
	  endelse

        end

		;************************
		;**** Run Now button ****
		;************************


    ELSE: new_event = 0L

  ENDCASE


                ;**** Return the state   ****

    widget_control, parent, set_uvalue=state, /no_copy

    RETURN, new_event

END

;-----------------------------------------------------------------------------

FUNCTION cw_adas702_in, parent, ndas, dsfull,      			$
		        VALUE=value, UVALUE=uvalue, FONT=font


    IF (N_PARAMS() EQ 0) THEN MESSAGE, 'Must specify parent for cw_adas702_in'

    		;**** return to caller if there is an error ****

    ON_ERROR, 2					

		;**** Set defaults for keywords ****

    IF NOT (KEYWORD_SET(uvalue))  THEN uvalue = 0
    IF NOT (KEYWORD_SET(font))    THEN font = ''
    IF NOT (KEYWORD_SET(value))   THEN begin
        str_arr = strarr(ndas)
	os = { in702_set, 						$
               new_list  :  str_arr,                                    $
               new_dir   :  '',						$
               message   :  '',	  				 	$
               dirname   :  '',                                         $
               title     :  ''                                          }
    ENDIF ELSE BEGIN
	os = { in702_set, 						$
               new_list  : value.new_list,                              $
	       new_dir   : value.new_dir,				$
               message   : value.message,				$
               dirname   : value.dirname,                               $
               title     : value.title                                  }
    ENDELSE




		;**********************************************
		;**** Create the 702 Input options widget  ****
		;**********************************************

		;**** create base widget ****

  cwid = widget_base( parent, UVALUE = uvalue, 			$
		      EVENT_FUNC = "in702_event", 		$
		      FUNC_GET_VALUE = "in702_get_val", 	$
		      /COLUMN)

		;**** Add dataset name and browse button ****

  rc = cw_adas_dsbr(cwid, dsfull, font=font)

		;***********************
		;**** add run title ****
		;***********************

  base = widget_base(cwid, /row)
  rc = widget_label(base, value='Title for Run', font=font)
  titid = widget_text(base, value=value.title, font=font, /edit)


		;***********************************
   		;**** Widget for directory name ****
		;***********************************
                
  base = widget_base(cwid,/column,/frame)
  l = widget_label(base, value='Directory for AUTOSTRUCTURE (ADAS701) file output:', $ 
                   font=font)
  dirnameid = widget_text(base,value=os.dirname,font=font,xsize=40,/editable)


                ;*********************************************
                ;**** AUTOSTRUCTURE input files selection ****
                ;*********************************************
                
  mcval = { new_list   : os.new_list,     $
            new_dir    : os.dirname,      $
            message    : os.message       }

  fileselid = cw_adas702_insel(base,ndas,                   $ 
                               output='What to select',      $
                               value=mcval,font=font)



		;***********************
		;**** Error message ****
		;***********************

    messid = widget_label(cwid,value=' ', font=font)



		;******************************
		;**** add the exit buttons ****
		;******************************

    base     = widget_base(cwid, /row)
    cancelid = widget_button(base, value='Cancel', font=font)
    doneid   = widget_button(base, value='Done', font=font)
  
  
	
		;*************************************************
		;**** create a state structure for the pop-up ****
		;**** window.                                 ****
		;*************************************************

    new_state = { cancelid      :       cancelid,       $
                  doneid        :       doneid,         $
                  fileselid	:	fileselid,	$
                  messid        :       messid,         $
                  titid         :       titid,          $
                  dirnameid     :       dirnameid       }

                ;**** Save initial state structure ****

    widget_control, parent, set_uvalue=new_state, /no_copy

    RETURN, cwid

END

