; Copyright (c) 1998 Strathclyde University .
; SCCS info: Module @(#)$Header: /home/adascvs/idl/adas7xx/adas702/cw_adas702_out.pro,v 1.1 2004/07/06 12:55:23 whitefor Exp $ Date $Date: 2004/07/06 12:55:23 $
;+
; PROJECT:
;       ADAS 
;
; NAME:
;	CW_ADAS702_OUT()
;
; PURPOSE:
;	Produces a widget for ADAS702 output options.
;
; EXPLANATION:
;       This function declares a compound widget consisting of an
;       output passing file  selection widget and an output file widget 
;       cw_adas_outfile.pro.  The text output file specified in this 
;       widget is for tabular (paper.txt) output. This widget also includes 
;	a button for browsing the comments from the input dataset, a
;       'Cancel' button and 'Run Now' and 'Run in batch' buttons. The
;       compound widget cw_adas_outfile.pro included in this file is
;       self managing.  This widget only handles events from the
;       'Cancel' and 'Run...' buttons.
;
;     
; USE:
;	This routine is specific to adas702.
;
; INPUTS:
;	PARENT	- Long integer; ID of parent widget.
;
;	INFOVAL	- String array; information about the input datasets
;
;       BITFILE - String; the path to the dirctory containing bitmaps
;                 for the 'escape to series menu' button.
;
; OPTIONAL INPUTS:
;	None.
;
; OUTPUTS:
;	The return value of this function is the ID of the declared widget.
;
; OPTIONAL OUTPUTS:
;	None.
;
; KEYWORD PARAMETERS:
;
;	VALUE	- A structure which determines the initial settings of
;		  the output options widget.  The structure is made
;		  up of two parts.  Each part is the same as the value
;		  structure of one of the two main compound widgets
;		  included in this widget.  See cw_adas_gr_sel and
;		  cw_adas_outfile for more details.  The default value is;
;
;		      {	TEXOUT:0, TEXAPP:-1, 				$
;			TEXREP:0, TEXDSN:'', 				$
;			TEXDEF:'',TEXMES:'', 				$
;                       DIRNAME:''					$
;		      }
;
;		  For CW_ADAS_OUTFILE tabular output (paper.txt);
;			TEXOUT	Integer; Activation button 1 on, 0 off
;			TEXAPP	Integer; Append button 1 on, 0 off, -1 no button
;			TEXREP	Integer; Replace button 1 on, 0 off, -1 no button
;			TEXDSN	String; Output file name
;			TEXDEF	String; Default file name
;			TEXMES	String; file name error message
;
;			DIRNAME String; output directory for iso-nuclear files
;
;	UVALUE	- A user value for this widget.
;
;	FONT	- String; the name of a font to be used for all text in
;		  this widget.  Default to system default font.
;
; CALLS:
;	CW_ADAS_OUTFILE	Output file name entry widget.
;	See side effects for other related widget management routines.
;
; SIDE EFFECTS:
;
; 	Two other routines are included in this file
;	OUT702_GET_VAL()
;	OUT702_EVENT()
;
; CATEGORY:
;	Compound Widget
;
; WRITTEN:
;       Martin O'Mullane
; MODIFIED:
;	1.1	Martin O'Mullane	
;		First release
;
; VERSION:
;       1.1	22-04-98
;
;-
;-----------------------------------------------------------------------------

FUNCTION out702_get_val, id

                ;**** Return to caller on error ****

    ON_ERROR, 2

                ;**** Retrieve the state ****

    parent=widget_info(id, /parent)
    widget_control, parent, get_uvalue=state, /no_copy
    

                ;**** Get adf09 pass name ****

    widget_control, state.passid, get_value = pspos

		;**** Get text output settings ****

    widget_control, state.paperid, get_value = papos

		;***********************************
		;**** Get run title from widget ****
		;**** Then centre in in string  ****
		;**** of 40 characters 		****
		;***********************************

    widget_control, state.titid, get_value=title
    title = strcompress(title(0))
    title_len = strlen(title) 
    if (title_len gt 40 ) then begin 
        title = strmid(title, 0, 37)
        widget_control, state.messid, set_value="Title too long - truncated "
        widget_control, state.titid, set_value=title
	wait, 1
    endif
    pad = (40 - title_len)/2 
    spaces = '                                         '
    title(0) = strmid(spaces,0,(pad-1)) + title + strmid(spaces,0,(pad-1))



    os = { out702_set, 				      $
	   texout     :       papos.outbut,           $
           texapp     :       papos.appbut,           $
	   texrep     :       papos.repbut,           $
           texdsn     :       papos.filename,         $
	   texdef     :       papos.defname,          $
           texmes     :       papos.message,          $
	   pasout     :       pspos.outbut,           $
           pasapp     :       pspos.appbut,           $
	   pasrep     :       pspos.repbut,           $
           pasdsn     :       pspos.filename,         $
	   pasdef     :       pspos.defname,          $
           pasmes     :       pspos.message,          $
           title      :       title                   $
        }

               ;**** Return the state ****

  widget_control, parent, set_uvalue=state, /no_copy

  RETURN, os 

END

;-----------------------------------------------------------------------------

FUNCTION out702_event, event

                ;**** Base ID of compound widget ****

    parent=event.top

                ;**** Retrieve the state ****

    widget_control, parent, get_uvalue=state, /no_copy

		;*********************************
		;**** Clear previous messages ****
		;*********************************

    widget_control, state.messid, set_value=' '

                ;************************
                ;**** Process events ****
                ;************************

    CASE event.id OF

		;***********************
		;**** Cancel button ****
		;***********************

        state.cancelid: new_event = {ID      : parent,    $
                                     TOP     : event.top, $
		                     HANDLER : 0L, 	  $
                                     ACTION  : 'Cancel'   }

		;*****************************
		;**** Run In Batch button ****
		;*****************************

       state.batid: begin

		;****************************************************
                ;**** Return the state before checking can start ****
		;**** with the get_value keyword.                ****
		;****************************************************

          widget_control, parent, set_uvalue=state, /no_copy

		;***************************************
		;**** Check for errors in the input ****
		;***************************************

	  error = 0

		;**** Get a copy of the widget value ****

	  widget_control, event.handler, get_value=os
	
		;**** Check for widget error messages ****

          mess = ''
	  if (os.texout eq 1 and strtrim(os.texmes) ne '') 		   $
          or (os.pasout eq 1 and strtrim(os.pasmes) ne '')  then error=1

          if (os.pasout eq 0) then begin
              error = 1
              mess = 'Error: you must choose an adf09 output file'
          endif
          
                ;**** Retrieve the state   ****

          widget_control, parent, get_uvalue=state, /no_copy

	  if error eq 1 then begin
	      if mess eq '' then mess = '**** Error in output settings ****'
	      widget_control,state.messid,set_value = mess
	      new_event = 0L
	  endif else begin
	      new_event = {ID      : parent, 		$
                           TOP     : event.top,		$ 
                           HANDLER : 0L, 	  	$
	                   ACTION  : 'Run In Batch'	}
	  endelse

        end

		;************************
		;**** Run Now button ****
		;************************

      state.runid: begin

		;****************************************************
                ;**** Return the state before checking can start ****
		;**** with the get_value keyword.                ****
		;****************************************************

          widget_control, parent, set_uvalue=state, /no_copy

		;***************************************
		;**** Check for errors in the input ****
		;***************************************

	  error = 0

		;**** Get a copy of the widget value ****

	  widget_control, event.handler, get_value=os

		;**** Check for widget error messages ****

	  mess=''
	  if (os.texout eq 1 and strtrim(os.texmes) ne '')  		   $
          or (os.pasout eq 1 and strtrim(os.pasmes) ne '')  then error=1

          if (os.pasout eq 0) then begin
              error = 1
              mess = 'Error: you must choose an adf09 output file'
          endif


                ;**** Retrieve the state   ****

          widget_control, parent, get_uvalue=state, /no_copy

	  if error eq 1 then begin
	      if mess eq '' then mess = '**** Error in output settings ****'
	      widget_control, state.messid, set_value= mess
	      new_event = 0L
	  endif else begin
	      new_event = {ID      : parent,	$ 
                           TOP     : event.top, $
                           HANDLER : 0L, 	$
	                   ACTION  : 'Run Now'  }
	  endelse

        end

    ELSE: new_event = 0L

  ENDCASE


                ;**** Return the state   ****

    widget_control, parent, set_uvalue=state, /no_copy

    RETURN, new_event

END

;-----------------------------------------------------------------------------

FUNCTION cw_adas702_out, parent, dsfull, VALUE=value, UVALUE=uvalue, FONT=font


    IF (N_PARAMS() EQ 0) THEN MESSAGE, 'Must specify parent for cw_adas702_out'

    		;**** return to caller if there is an error ****

    ON_ERROR, 2					

		;**** Set defaults for keywords ****

    IF NOT (KEYWORD_SET(uvalue))  THEN uvalue = 0
    IF NOT (KEYWORD_SET(font))    THEN font = ''
    IF NOT (KEYWORD_SET(value))   THEN begin
	os = { out702_set, 						$
	       texout    :  0,                                          $
               texapp    : -1,                                          $
	       texrep    :  0,                                          $
               texdsn    : '',                                          $
               texdef    : 'paper.txt',                                 $
               texmes    : '',                                          $
	       pasout    :  0,                                          $
               pasapp    : -1,                                          $
	       pasrep    :  0,                                          $
               pasdsn    : '',                                          $
               pasdef    : 'adas702_adf09.pass',                        $
               pasmes    : '',                                          $
               title     : ''                                           }
    ENDIF ELSE BEGIN
	os = { out702_set, 						$
	       texout    : value.texout,                                $
               texapp    : value.texapp,                                $
	       texrep    : value.texrep,                                $
               texdsn    : value.texdsn,                                $
	       texdef    : value.texdef,                                $
               texmes    : value.texmes,                                $
	       pasout    : value.pasout,                                $
               pasapp    : value.pasapp,                                $
	       pasrep    : value.pasrep,                                $
               pasdsn    : value.pasdsn,                                $
	       pasdef    : value.pasdef,                                $
               pasmes    : value.pasmes,                                $
               title     : value.title                                  }
    ENDELSE



		;**********************************************
		;**** Create the 702 Output options widget ****
		;**********************************************

		;**** create base widget ****

  cwid = widget_base( parent, UVALUE = uvalue, 			$
		      EVENT_FUNC = "out702_event", 		$
		      FUNC_GET_VALUE = "out702_get_val", 	$
		      /COLUMN)

		;**** Add dataset name and browse button ****

  rc = cw_adas_dsbr(cwid, dsfull, font=font)

		;***********************
		;**** add run title ****
		;***********************

  base = widget_base(cwid, /row)
  rc = widget_label(base, value='Title for Run', font=font)
  titid = widget_text(base, value=value.title, font=font)


		;****************************************
   		;**** Widget for adf09 pass filename ****
		;****************************************
                
    outfval = { OUTBUT   : os.pasout, 	$
                APPBUT   : -1, 		$
                REPBUT   : os.pasrep, 	$
	        FILENAME : os.pasdsn, 	$
                DEFNAME  : os.pasdef, 	$
                MESSAGE  : os.pasmes 	}
                
    base   = widget_base(cwid, /row, /frame)
    passid = cw_adas_outfile(base, OUTPUT='Resolved DR coefficient output', $
                             VALUE=outfval, FONT=font)



		;********************************
		;**** Widget for text output ****
		;********************************

    outfval = { OUTBUT   : os.texout, 	$
                APPBUT   : -1, 		$
                REPBUT   : os.texrep, 	$
	        FILENAME : os.texdsn, 	$
                DEFNAME  : os.texdef, 	$
                MESSAGE  : os.texmes 	}
                
    base = widget_base(cwid,/row,/frame)
    paperid = cw_adas_outfile(base, OUTPUT='Text Output',   $
                              VALUE=outfval, FONT=font)


		;***********************
		;**** Error message ****
		;***********************

    messid = widget_label(cwid,value=' ', font=font)



		;******************************
		;**** add the exit buttons ****
		;******************************

    base     = widget_base(cwid, /row)
    cancelid = widget_button(base, value='Cancel', font=font)
    runid    = widget_button(base, value='Run Now', font=font)
    batid    = widget_button(base, value='Run in Batch', font=font)
  
  
	
		;*************************************************
		;**** create a state structure for the pop-up ****
		;**** window.                                 ****
		;*************************************************

    new_state = { passid        :       passid,         $
		  paperid       :       paperid,        $
    		  batid         :       batid,          $
		  cancelid      :       cancelid,       $
                  runid         :       runid,          $
                  messid        :       messid,         $
                  titid         :       titid           }

                ;**** Save initial state structure ****

    widget_control, parent, set_uvalue=new_state, /no_copy

    RETURN, cwid

END

