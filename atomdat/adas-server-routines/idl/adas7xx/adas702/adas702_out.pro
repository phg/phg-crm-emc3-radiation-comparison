; Copyright (c) 1998 Strathclyde University .
; SCCS info: Module @(#)$Header: /home/adascvs/idl/adas7xx/adas702/adas702_out.pro,v 1.1 2004/07/06 11:04:03 whitefor Exp $ Date $Date: 2004/07/06 11:04:03 $
;+
; PROJECT:
;       ADAS
;
; NAME:
;	ADAS702_OUT
;
; PURPOSE:
;	IDL ADAS user interface, output options.
;
; EXPLANATION:
;	This routine creates and manages a pop-up window which allows
;	the user to select options and input data to control ADAS702
;	file output.
;
; USE:
;	This routine is specifically for use with adas702.             
;
; INPUTS:
;	VAL	- A structure which determines the initial settings of
;		  the output options widget.  The value is passed
;		  unmodified into cw_adas702_out.pro.
;
;		  See cw_adas702_out.pro for a full description of this
;		  structure.
;
;
; OPTIONAL INPUTS:
;	None.
;
; OUTPUTS:
;	ACT	- String; Either 'Done' or 'Cancel' for the button the
;		  user pressed to terminate the output options window.
;
; OPTIONAL OUTPUTS:
;	None.
;
; KEYWORD PARAMETERS:
;	FONT	- Supplies the font to be used for the interface widgets.
;
; CALLS:
;	CW_ADAS702_OUT	Creates the output options widget.
;	XMANAGER
;	See side effects for widget management routine.
;
; SIDE EFFECTS:
;	This routine uses a common block OUT702_BLK to maintain its state.
;	ADAS702_OUT_EV	is included in this file and is called
;	indirectly from XMANAGER during widget management.
;
; CATEGORY:
;	Compound Widget.
;
; WRITTEN:
;       Martin O'Mullane 
;
; MODIFIED:
;	1.1	Martin O'Mullane         16/06/1998
; VERSION:
;	1.1	First Release	
;-
;-----------------------------------------------------------------------------


pro adas702_out_ev, event

  common out702_blk,action,value
	

		;**** Find the event type and copy to common ****

    action = event.action

    case action of

		;**** 'Run Now' button ****

	'Run Now'  : begin

			;**** Get the output widget value ****

     		     child = widget_info(event.id, /child)
		     widget_control, child, get_value=value 

                        ;*****************************************
			;**** Kill the widget to allow IDL to ****
			;**** continue and interface with     ****
			;**** FORTRAN.                        ****
			;*****************************************
                        
                        widget_control, event.top, /destroy

	          end


                ;**** 'Run in Batch' button ****

        'Run In Batch'  : begin

                        ;**** Get the output widget value ****

                     child = widget_info(event.id, /child)
                     widget_control, child, get_value=value

                        ;*****************************************
                        ;**** Kill the widget to allow IDL to ****
                        ;**** continue and interface with     ****
                        ;**** FORTRAN.			      ****
                        ;*****************************************
                        
                        widget_control, event.top, /destroy
                  end

		;**** 'Cancel' button ****

	'Cancel': widget_control, event.top, /destroy

        ELSE: begin   				
			;do nothing 
              end
    endcase

end

;-----------------------------------------------------------------------------


pro adas702_out, val, act,  dsfull, font=font

  common out702_blk, action, value

		;**** Copy value to common ****

  value = val

		;**** Set defaults for keywords ****

  if not (keyword_set(font)) then font = ''

		;***************************************
		;**** Pop-up a new widget           ****
		;***************************************

                ;**** create base widget ****

  outid = widget_base(title='ADAS702 OUTPUT OPTIONS', xoffset=100, yoffset=100)

		;**** Declare output options widget ****

  cwid = cw_adas702_out(outid, dsfull, value=value, font=font )

		;**** Realize the new widget ****
  dynlabel, outid
  widget_control, outid, /realize

		;**** make widget modal ****

  xmanager,'adas702_out', outid, event_handler='adas702_out_ev',	$
                          /modal, /just_reg
 
		;**** Return the output value from common ****

  act = action
  val = value

END

