; Copyright (c) 1998 Strathclyde University .
; SCCS info: Module @(#)$Header: /home/adascvs/idl/adas7xx/adas702/cw_adas702_insel.pro,v 1.1 2004/07/06 12:55:20 whitefor Exp $ Date $Date: 2004/07/06 12:55:20 $
;+
; PROJECT:
;	ADAS 
;
; NAME:
;	CW_ADAS702_INSEL()
;
; PURPOSE:
;	Multiple selection and change list compound widget.
;       This particular version is specific to adas series 7 routines.
;
; EXPLANATION:
;	This compound widget function puts up a file selector widget and
;       the user chooses up to 4 files from the base directory which is
;       entered in another compond widget. Thus  while this widget does
;       not generate any events it reacts to a  send_event from the base
;       directory compound widget. The chosen files are listed next to
;       the file selector. Files are chosen by clicking once and are
;       deselected by a further click.
;
;
; USE:
;	See the routine cw_adas702_in for a working example of how to use
;	cw_adas702_insel.
;
;
; INPUTS:
;       PARENT   - The ID of the parent widget.
;
;	NDAS     - Max number of AUTOSTRUCTURE files allowed for ADASDR
;                  (currently it is 4 - nominally o1/o2/o3/o4 or o1u etc) 
;
; OPTIONAL INPUTS:
;	None.  See the keywords for additional controls.
;
; OUTPUTS:
;	The return value of this function is the ID of the compound
;	widget which is of type LONG integer.
;
; OPTIONAL OUTPUTS:
;	None
;
; KEYWORDS:
;		 
;	UVALUE   - Supplies the user value for the widget.  This is 
;		   exclusively for the caller's use and has no effect on
;		   the operation of the widget.
;
;	FONT	 - Supplies the font to be used for the widget.
;
;	VALUE	- A structure which determines the initial settings of
;		  the cw_adas702_insel options widget.  The structure is 
;
;               value = {   new_list : str_arr,   $
;                           new_dir  : '',        $
;                           message  : '',        }
;
;       new_list -  One dimensional string array corresponding to user picked
;                   names for input filenames.
;
;	new_dir  -  String of new target directory.
;
;       message  -  Holds the current warning message.
;
;
; CALLS:
;	None directly.  See SIDE EFFECTS below for a list of the widget
;	management routines.
;
; RESTRICTIONS:
;	None.
;
; SIDE EFFECTS:
;
;       Reacts to events from cw_adas702_in event handler.
;
;	Three other routines are included which are used to manage the
;	widget;
;
;	INSEL_SET_VAL
;	INSEL_GET_VAL()
;	INSEL_EVENT()
;
; CATEGORY:
;	Compound Widget 
;
; WRITTEN:
;	Martin O'Mullane
;
; MODIFIED:
;	1.1	Martin O'Mullane	
;		First release.
;
; VERSION:
;       1.1     12/6/1998
;-
;-----------------------------------------------------------------------------

PRO insel_set_val, id, value


		;**** Return to caller on error ****
  ON_ERROR, 2

		;**** Retrieve the state ****
                
  first_child = widget_info(id, /child)
  widget_control, first_child, get_uvalue = state ,/no_copy



		;**** Copy message to widget ****
                
  if strtrim(value.message) ne '' then begin
     message = value.message
  end else begin
     message = ' '
  end
  widget_control,state.messid,set_value=message

		;**** Copy the new values to state structure ****
                ;**** not necessary to save new_dir name?

  state.mcval.new_list = value.new_list
  state.mcval.new_dir  = value.new_dir   
  state.mcval.message  = value.message   
                
                
		;**** Save the new state ****
                
  widget_control, first_child, set_uvalue=state, /no_copy

END

;-----------------------------------------------------------------------------

FUNCTION insel_get_val, id

		;**** Return to caller on error ****
  ON_ERROR, 2

		;**** Retrieve the state ****

  first_child = widget_info(id, /child)
  widget_control, first_child, get_uvalue = state
  
 


  RETURN, state.mcval

END

;-----------------------------------------------------------------------------

FUNCTION insel_event, event


		;**** Base ID of compound widget ****

  parent = event.handler
  
     ;***************************************************************
     ;**** automatically notified if input directory is changed ****
     ;**** via a send_event from cw_adas702_in event handler    ****
     ;***************************************************************

  in_dir = ''
  thisEvent = tag_names(event,/structure)
  if thisEvent eq 'FILESELNEW' then begin
     in_dir = event.name
  endif
    
 
		;**** Retrieve the state ****

  first_child = widget_info(parent, /child)
  widget_control, first_child, get_uvalue=state 

		;**** Remove existing error message ****
 
  widget_control, state.messid, set_value = ' '
  state.mcval.message = ' '


               ;***************************************************
               ;**** Update the new_dir name in the state, set ****
               ;**** new directory and reset selected files.   ****
               ;***************************************************
  
  if in_dir ne '' then begin
  
     state.mcval.new_dir = in_dir
     
     state.fsel.root = in_dir
     widget_control,state.fselid,set_value=state.fsel
     
     state.mcval.new_list(0:state.ndas-1) = ''
     
     for i = 0, state.ndas-1 do begin
         widget_control,state.list_name(i),set_value=''
     endfor 
       
  endif

 
          
		;************************
		;**** Process events ****
		;************************
                
    if event.id eq state.fselid then begin           
       
       ndas = state.ndas   
       widget_control,state.fselid,get_value=filename
       f_new = filename.file
       
       pos = where(state.mcval.new_list eq f_new, count)

       if count eq 0 then begin       				; not in list
       
         pos_empty = where(state.mcval.new_list eq '', count_empty)
         pos_empty = pos_empty(0)
         if count_empty eq 0 then begin			; list full
             message = 'Max files reached. Remove one to add this file' 
             widget_control,state.messid,set_value=message	
         endif else begin					; add it to end
             state.mcval.new_list(pos_empty) = f_new
             widget_control,state.list_name(pos_empty),set_value=f_new
         endelse
         
       endif else begin
       
          temp = state.mcval.new_list(where(state.mcval.new_list ne f_new))
          state.mcval.new_list(0:state.ndas-1) = ''   
          state.mcval.new_list(0:(n_elements(temp)-1)) = temp
          
          for j = 0, state.ndas-1 do begin
              widget_control,state.list_name(j),set_value = state.mcval.new_list(j)
          endfor   
          
       endelse
       
    endif

    new_event = 0L

		;*******************************************
		;*** make state available to other files ***
		;*******************************************

    widget_control, first_child, set_uvalue=state, /no_copy
    RETURN, new_event
    

END

;-----------------------------------------------------------------------------

FUNCTION cw_adas702_insel, parent, ndas, 				$ 
                           OUTPUT=output, 				$
			   VALUE=value, FONT=font, UVALUE=uvalue


		;**** Flag error and return to caller ****
                
  IF (N_PARAMS() LT 2) THEN MESSAGE, $
		'Must specify a PARENT and LIST for cw_adas702_insel'
 
  ON_ERROR, 2

		;**** Set defaults for keywords ****
                
  IF NOT (KEYWORD_SET(output)) THEN output = 'Alter the Following Output'

 
  IF NOT (KEYWORD_SET(value)) THEN begin

      str_arr = strarr(ndas)
      mcval = { mcval,                  $
                new_list   : str_arr,   $
                new_dir    : '',        $
                message    : ''         }
 
  end else begin
 
      mcval = { mcval,                         $
                new_list   : value.new_list,   $
                new_dir    : value.new_dir,    $
                message    : value.message     }
  end

  IF NOT (KEYWORD_SET(font))   THEN font = ''
  IF NOT (KEYWORD_SET(uvalue)) THEN uvalue = 0


		;**** Create the main base for the table ****

  cwid = WIDGET_BASE(parent, UVALUE = uvalue, 			$
		     EVENT_FUNC     = "insel_event", 		$
		     FUNC_GET_VALUE = "insel_get_val", 	$
		     PRO_SET_VALUE  = "insel_set_val", 	$
		     /COLUMN					)
  
  
		;******************************************************
		;**** Create a dummy widget just to hold value of *****
		;**** "state" variable so as not to get confused  *****
		;**** with any other values. Adopt IDL practice   *****
		;**** of using first child widget                 *****
		;******************************************************

  first_child = widget_base(cwid)
  c_base      = widget_base(cwid,/column)
  r_base      = widget_base(c_base,/row)

		;**** Instructions ****
                
  inst1 = widget_label(c_base,value='Choose AUTOSTRUCTURE output files',font=font)
  inst2 = widget_label(c_base,value='- click once to choose, again to remove -',font=font)

		;**** File selection ****
                
  fsel = { fsel,		      $
	   root  :  mcval.new_dir,    $
           file  :  ''		}

  base1  = widget_base(c_base,/row)
  fselid = cw_file_select( base1, VALUE=fsel, TITLE=' ',     $
                           XSIZE=30, YSIZE=15, FONT=font )     
                           
                           
		;**** File selected display ****
                
  base2  = widget_base(base1,/column)
  inst3  = widget_label(base2,value='  ',font=font)
  inst3a = widget_label(base2,value='  ',font=font)
  inst4  = widget_label(base2,value='Files selected (in order):',font=font)
  inst4a = widget_label(base2,value='--------------------------',font=font)
              
  fname = ''
  list_name = lonarr(ndas)         ; list of selected files
  for j = 0,ndas - 1 do begin
     fname = strtrim(mcval.new_list(j),2)
     list_name(j) = widget_label(base2,value=fname,font=font,/align_left)
  endfor                               


		;**** Message - general errors ****
                
  if strtrim(mcval.message,2) eq '' then begin
     message = ' '
  end else begin
     message = mcval.message
  end
  messid = widget_label(cwid,value=message,font=font)


		;**** Create state structure ****
                
  new_state =  { list_name      :       list_name,      $
		 fselid		:	fselid,	      	$
                 messid	        :       messid,         $
 		 fsel		:	fsel,		$
                 ndas	        :       ndas,         	$
                 mcval	        :       mcval          	}


		;**** Save initial state structure ****
                
  widget_control, first_child, set_uvalue=new_state, /no_copy

  RETURN, cwid

END
