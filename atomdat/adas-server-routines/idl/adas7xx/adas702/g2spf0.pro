; Copyright (c) 1998 Strathclyde University .
; SCCS info: Module @(#)$Header: /home/adascvs/idl/adas7xx/adas702/g2spf0.pro,v 1.1 2004/07/06 13:56:25 whitefor Exp $ Date $Date: 2004/07/06 13:56:25 $
;+
; PROJECT:
;       ADAS 
;
; NAME:
;	G2SPF0
;
; PURPOSE:
;	IDL user interface and communications with ADAS702 FORTRAN
;	process via pipe.
;
; EXPLANATION:
;	Firstly this routine invokes the part of the user interface
;	used to select the input dataset for ADAS702.  When the user's
;	interactions are complete this routine communicates with the
;	ADAS702 FORTRAN application via a UNIX pipe.  Communications
;	are to the FORTRAN subroutine G2SPF0.
;
; USE:
;	The use of this routine is specific to ADAS702, see adas702.pro.
;
; INPUTS:
;	PIPE	- The IDL unit number of the bi-directional pipe to the
;		  ADAS702 FORTRAN process.
;
;	VALUE	- A structure which determines the initial settings of
;		  the dataset selection widget.  The initial value is
;		  set in adas702.pro.  VALUE is passed un-modified
;		  through to cw_adas_in.pro, see that routine for a full
;		  description.
;
; OPTIONAL INPUTS:
;	None
;
; OUTPUTS:
;	VALUE	- On output the structure records the final settings of
;		  the dataset selection widget if the user pressed the
;		  'Done' button otherwise it is not changed from input.
;
;	DSFULL	- String; The full system file name of the emitting ion
;		  dataset selected by the user for processing.
;
;	REP	- String; Indicates whether the user pressed the 'Done'
;		  or 'Cancel' button on the interface.  The action is
;		  converted to the strings 'NO' and 'YES' respectively
;		  to match up with the existing FORTRAN code.  In the
;		  original IBM ISPF interface REP was the reply to the
;		  question 'DO YOU WANT TO END PROGRAM EXECUTION'.
;
; OPTIONAL OUTPUTS:
;	None
;
; KEYWORD PARAMETERS:
;	FONT	- Supplies the font to be used for the interface widgets.
;
; CALLS:
;	ADAS_IN		Pops-up the dataset selection widget.
;
; SIDE EFFECTS:
;	This routine communicates with the ADAS702 FORTRAN process
;	via a UNIX pipe.
;
; CATEGORY:
;	Adas system.
;
; WRITTEN:
;       Martin O'Mullane, 16/06/1998
;
; MODIFIED:
;	1.1	Martin O'Mullane
;		First Release, based on bbspf0.pro
; VERSION:
;	1.1	16-06-98
;
;-----------------------------------------------------------------------------


PRO g2spf0, pipe, value, dsfull, rep, FONT=font


                ;**** Return to caller on error ****

    ON_ERROR, 2

                ;**** Set defaults for keywords ****

    IF NOT (KEYWORD_SET(font)) THEN font = ''


		;**********************************
		;**** Pop-up input file widget ****
		;**********************************

    adas_in, value, action, WINTITLE = 'ADAS 702 INPUT', 		$
	     TITLE = 'Input File', FONT = font

		;********************************************
		;**** Act on the event from the widget   ****
		;********************************************
		;**** There are only two possible events ****
		;**** 'Done' and 'Cancel'.               ****
		;********************************************

    if action eq 'Done' then begin
        rep = 'NO'
    end else begin
        rep = 'YES'
    end

		;**** Construct dataset name ****

    dsfull = value.rootpath+value.file


		;*******************************
		;**** Write data to fortran ****
		;*******************************

    printf, pipe, rep
    printf, pipe, dsfull


END
