; Copyright (c) 1998 Strathclyde University .
; SCCS info: Module @(#)$Header: /home/adascvs/idl/adas7xx/adas702/g2spf1.pro,v 1.1 2004/07/06 13:56:31 whitefor Exp $ Date $Date: 2004/07/06 13:56:31 $
;+
; PROJECT:
;       ADAS 
;
; NAME:
;	G2SPF1
;
; PURPOSE:
;	IDL user interface and communications with ADAS702 FORTRAN
;	process via pipe.
;
; EXPLANATION:
;       First this routine reads some information from ADAS702 FORTRAN
;       via the UNIX pipe.  Next the 'Output Options' part of the
;	interface is invoked.  The resulting action of the user's
;	interactions is written to the FORTRAN via the pipe.
;	Communications are with the FORTRAN subroutine G1SPF1.
;
; USE:
;	The use of this routine is specific to ADAS702, See adas702.pro.
;
; INPUTS:
;	PIPE	- The IDL unit number of the bi-directional pipe to the
;		  ADAS702 FORTRAN process.
;
;	DSFULL  - The name of the data set being analyzed.
;
;	VALUE	- A structure which determines the initial settings of
;		  the dataset selection widget.  The initial value is
;		  set in adas702.pro.  VALUE is passed un-modified
;		  through to cw_adas_in.pro, see that routine for a full
;		  description.
;
;	HEADER  - Header information used for text output.
;
; OPTIONAL INPUTS:
;	None.
;
; OUTPUTS:
;	LPEND	- Integer; indicates user action on leaving output
;		  selections.  lpend is set to 1 if the user exited
;		  with the 'Cancel' button, 0 if the user exited
;		  with 'Run Now' and 2 if the user exited with 'Run in 
;		  Batch'.
;
;	VALUE	- On output the structure records the final settings of
;		  the output selection widget if the user pressed either
;		  of the 'Run' buttons otherwise it is not changed from input.
;
; OPTIONAL OUTPUTS:
;	None.
;
; KEYWORD PARAMETERS:
;
;	FONT	- Supplies the font to be used for the interface widgets.
;
; CALLS:
;	ADAS702_OUT	User interface - output options.
;
; SIDE EFFECTS:
;	This routine communicates with the ADAS702 FORTRAN process
;	via a UNIX pipe.
;
; CATEGORY:
;	Adas system.
;
; WRITTEN:
;       Martin O'Mullane
;
; MODIFIED:
;	1.1	Martin O'Mullane	
;		First release.
;
; VERSION:
;       1.1     16/06/1998
;
;-
;-----------------------------------------------------------------------------

PRO   g2spf1, pipe, lpend, ndas, value, dsfull, header, FONT=font

                ;**** Return to caller on error ****

    ON_ERROR, 2

                ;**** Set defaults for keywords ****

    IF NOT (KEYWORD_SET(font)) THEN font = ''

  
		;**************************************
		;**** Pop-up output options widget ****
		;**************************************

    outtitle = "ADAS702 INPUT OPTIONS"
    adas702_in, value, ndas, dsfull, action, FONT=font

		;*************************************************
		;**** Act on the output from the widget       ****
		;**** There are two possible actions          ****
		;**** 'Cancel' and 'Done'.                    ****
		;*************************************************

    if action eq 'Done' then begin
        lpend = 0
    endif else begin
        lpend = 1
    end

		;*******************************
		;**** Write data to fortran ****
		;*******************************

    printf, pipe, lpend

    if (lpend eq 0) then begin

	printf, pipe, value.dirname
        
        for j = 0,n_elements(value.new_list)-1 do begin
           printf, pipe, value.new_list(j)
        endfor
        
    endif

END
