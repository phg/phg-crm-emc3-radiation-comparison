; Copyright (c) 1998 Strathclyde University .
; SCCS info: Module @(#)$Header: /home/adascvs/idl/adas7xx/adas702/adas702_in.pro,v 1.1 2004/07/06 11:04:01 whitefor Exp $ Date $Date: 2004/07/06 11:04:01 $
;+
; PROJECT:
;       ADAS 
;
; NAME:
;       ADAS702_IN
;
; PURPOSE:
;       IDL ADAS user interface, ADASDR input file options.
;
; EXPLANATION:
;       This routine creates and manages a pop-up window which allows
;       the user to select options and input data to control ADAS702
;       file input.
;
; USE:
;       This routine is ADAS702 specific, see g2spf1.pro for how it
;       is used.
;
; INPUTS:
;       VAL     - A structure which determines the initial settings of
;                 the input options widget.  The value is passed
;                 unmodified into cw_adas702_in.pro.
;
;                 See cw_adas702_in.pro for a full description of this
;                 structure.
;
;
; OPTIONAL INPUTS:
;       None.
;
; INPUTS:
;       ACT     - String; 'Done', 'Cancel' for the button the
;                 user pressed to terminate the input options window.
;
; OPTIONAL INPUTS:
;       None.
;
; KEYWORD PARAMETERS
;       FONT    - Supplies the font to be used for the interface widgets.
;
; CALLS:
;       CW_ADAS702_In   Creates the input options widget.
;       DYNLABEL        Recursively adds dynamic_resize keyword to label
;                       widgets for version 4.0.1 and higher of IDL
;       XMANAGER
;       See side effects for widget management routine.
;
; SIDE EFFECTS:
;       This routine uses a common block IN702_BLK to maintain its state.
;       ADAS702_IN_EV  is included in this file and is called
;       indirectly from XMANAGER during widget management.
;
; CATEGORY:
;       Widgets
;
; WRITTEN:
;       Martin O'Mullane
;
; MODIFIED:
;	1.1	Martin O'Mullane
;               First Release.
;
; VERSION:
;	1.1	16-06-98
;
;-
;-----------------------------------------------------------------------------

PRO adas702_in_ev, event

    COMMON in702_blk, action, value
        

                ;**** Find the event type and copy to common ****

    action = event.action

    CASE action of

                ;**** 'Done' button ****

        'Done'  : begin

                ;**** Get the input widget value ****

            child = widget_info(event.id,/child)
            widget_control, child, get_value=value 

                ;*****************************************
                ;**** Kill the widget to allow IDL to ****
                ;**** continue and interface with     ****
                ;**** FORTRAN only if there is work   ****
                ;**** for the FORTRAN to do.          ****
                ;*****************************************

            if value.dirname ne ''  then begin
                widget_control, event.top, /destroy
            endif 
        end

                ;**** 'Cancel' button ****

        'Cancel': widget_control, event.top, /destroy


        ELSE:   ;**** Do nothing ****

    ENDCASE

END

;-----------------------------------------------------------------------------

PRO adas702_in, val, ndas, dsfull, act, font=font

    COMMON in702_blk, action, value

                ;**** Copy value to common ****

    value = val

                ;**** Set defaults for keywords ****

    if not (keyword_set(font)) then font = ''

                ;***************************************
                ;**** Pop-up a new widget           ****
                ;***************************************

                ;**** create base widget ****

    inid = widget_base(title='ADAS702 INPUT OPTIONS', xoffset=100,    $
                        yoffset=1)

                ;**** Declare input options widget ****

    cwid = cw_adas702_in(inid,  ndas, dsfull, value=value,          $
                         font=font )

                ;**** Realize the new widget ****

    dynlabel, inid
    widget_control, inid, /realize

                ;**** make widget modal ****

    xmanager, 'adas702_in', inid, event_handler='adas702_in_ev',     $
              /modal, /just_reg
 
                ;**** Return the input value from common ****

    act = action
    val = value

END
