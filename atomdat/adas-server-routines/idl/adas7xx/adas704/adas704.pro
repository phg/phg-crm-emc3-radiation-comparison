; Copyright (c) 2000 Strathclyde University .
; SCCS info: Module @(#)$Header: /home/adascvs/idl/adas7xx/adas704/adas704.pro,v 1.1 2004/07/06 11:04:27 whitefor Exp $ Date $Date: 2004/07/06 11:04:27 $
;+
; PROJECT:
;       ADAS
;
; NAME:
;	ADAS704
;
; PURPOSE:
;	The highest level routine for the ADAS 704 program.
;
; EXPLANATION:
;	This routine is called from the main adas system routine, adas.pro,
;	to start the ADAS 704 application.  Associated with adas704.pro
;	is a FORTRAN executable.  The IDL code provides the user
;	interface and output graphics whilst the FORTRAN code reads
;	in data files, performs numerical processing and creates the
;	output files.  The IDL code communicates with the FORTRAN
;	executable via a bi-directional UNIX pipe.  The unit number
;	used by the IDL for writing to and reading from this pipe is
;	allocated when the FORTRAN executable is 'spawned' (see code
;	below).  Pipe communications in the FORTRAN process are to
;	stdin and stdout, i.e streams 5 and 6.
;
;	The FORTRAN code is an independent process under the UNIX system.
;	The IDL process can only exert control over the FORTRAN in the
;	data which it communicates via the pipe.  The communications
;	between the IDL and FORTRAN must be exactly matched to avoid
;	input conversion errors.  The correct ammounts of data must be
;	passed so that neither process 'hangs' waiting for communications
;	which will never occur.
;
;	The FORTRAN code performs some error checking which is
;	independent of IDL.  In cases of error the FORTRAN may write
;	error messages.  To prevent these error messages from conflicting
;	with the pipe communications all FORTRAN errors are written to
;	output stream 0, which is stderr for UNIX.  These error messages
;	will appear in the window from which the ADAS session/IDL session
;	is being run.
;
;	In the case of severe errors the FORTRAN code may terminate
;	itself prematurely.  In order to detect this, and prevent the
;	IDL program from 'hanging' or crashing, the IDL checks to see
;	if the FORTRAN executable is still an active process before
;	each group of pipe communications.  The process identifier
;	for the FORTRAN process, PID, is recorded when the process is
;	first 'spawned'.  The system is then checked for the presence
;	of the FORTRAN PID.
;
; USE:
;	First the system settings must be established by calling
;	adas_sys_set.pro then adas704.pro is called to start the
;	ADAS 704 application;
;
;	adas_sys_set, adasrel, fortdir, userroot, centroot, $
;		      devlist, devcode, font_large, font_small, edit_fonts
;	adas704,   adasrel, fortdir, userroot, centroot, devlist, $
;		   devcode, font_large, font_small, edit_fonts
;
; INPUTS:
;	ADASREL - A string indicating the ADAS system version, 
;		  e.g ' ADAS RELEASE: ADAS93 V1.1.0'.  The first
;		  character should be a space.
;
;	FORTDIR - A string holding the path to the directory where the
;		  FORTRAN executables are, e.g '/disk/adas/fortran/exec'
;
;	USERROOT - A string holding the path to the root directory of
;		   the user's adas data. e.g '/disk/bowen/adas'
;		   This root directory will be used by adas to construct
;		   other path names.  For example the users default data
;		   for adas704 should be in /disk/bowen/adas/adf04.  In
;		   particular the user's default interface settings will
;		   be stored in the directory USERROOT+'/defaults'.  An
;		   error will occur if the defaults directory does not
;		   exist.
;
;	CENTROOT - Like USERROOT, but this directory points to the
;		   central data area for the system.  User defaults are
;		   not stored on CENTROOT.
;
;	DEVLIST - A string array of hardcopy device names, used for
;		  graphical output. e.g ['Post-Script','HP-PCL','HP-GL']
;		  This array must mirror DEVCODE.  DEVCODE holds the
;		  actual device names used in a SET_PLOT statement.
;
;	DEVCODE - A string array of hardcopy device code names used in
;		  the SET_PLOT statement, e.g ['PS', 'PCL', 'HP']
;		  This array must mirror DEVLIST.
;
;	FONT_LARGE - The name of a larger font e.g 'courier_bold14'
;
;	FONT_SMALL - The name of a smaller font e.g 'courier_bold12'
;
;	EDIT_FONTS - A structure of two fonts used in the table editor
;		     adas_edtab.pro, {font_norm:'',font_input:''}.
;		     The two fonts are used to differentiate between
;		     editable and non-editable parts of the table. You
;		     may have to experiment to get two fonts which give
;		     the same default spacing in the edit widget.
;		     e.g {font_norm:'helvetica_bold14', $
;			  font_input:'helvetica_oblique14'}
;
; OPTIONAL INPUTS:
;	None
;
; OUTPUTS:
;	None - Note this routine should leave all inputs un-modified.
;
; OPTIONAL OUTPUTS:
;	None
;
; KEYWORD PARAMETERS:
;	None
;
; CALLS:
;	FIND_PROCESS	Checks to see if a given process is active.
;	XXDATE		Get date and time from operating system.
;	XXADAS          Pipe comms with FORTRAN XXADAS routine.
;
; SIDE EFFECTS:
;	This routine spawns a FORTRAN executable.  Note the pipe 
;	communications routines listed above.  In addition to these
;	pipe communications there is one explicit communication of the
;	date to the FORTRAN code, search for 'printf,pipe' to find it.
;
; CATEGORY:
;	Adas system.
;	
; WRITTEN:
;       Martin O'Mullane, 16-09-2000
;
; MODIFIED:
;	1.1	Martin O'Mullane
;		First version
;
; VERSION:
;	1.1	16-09-2000
;
;-
;-----------------------------------------------------------------------------


PRO ADAS704,	adasrel, fortdir, userroot, centroot, $
		devlist, devcode, font_large, font_small, edit_fonts


; Initialise

  adasprog = ' PROGRAM: ADAS704 V1.1'

  deffile  = userroot+'/defaults/adas704_defaults.dat'
  device   = ''
  bitfile  = centroot + '/bitmaps'


; Need IDL v5 capabilities

  thisRelease = StrMid(!Version.Release, 0, 1)
  IF thisRelease LT '5' THEN BEGIN
     message = 'Sorry, ADAS801 requires IDL 5 or higher' 
     tmp = popup(message=message, buttons=['Accept'],font=font_large)
     goto, LABELEND
  ENDIF



  files = findfile(deffile)
  
  if files[0] EQ deffile then begin
    
    restore,deffile
    inval.centroot = centroot+'/autos/'
    inval.userroot = userroot+'/autos/'
 
  endif else begin
    
     inval   = {ROOTPATH    : userroot+'/autos/',        $
                FILE        : '',                        $
                CENTROOT    : centroot+'/autos/',        $
                USERROOT    : userroot+'/autos/'         }

     procval = {NEW:-1}

     passname = userroot + '/pass/adas704_auger.pass'

     outval  = { TEXOUT   : 0,                           $
                 TEXAPP   : -1,                          $
                 TEXREP   : 0,                           $
                 TEXDSN   : '',                          $
                 TEXDEF   : 'paper.txt',                 $
                 TEXMES   : '',                          $
                 PASSOUT  : 0,                           $
                 PASSAPP  : -1,                          $
                 PASSREP  : 0,                           $
                 PASSDSN  : '',                          $
                 PASSDEF  : passname,                    $
                 PASSMES  : ''                           }
 
  endelse
  
  
; Set the fortran going  
  
  spawn, fortdir+'/adas704.out',unit=pipe,/noshell,PID=pid


; and pass it the date

  date = xxdate()
  printf,pipe,date(0)



LABEL100:
  
; Input file selection
  
  if find_process(pid) eq 0 then goto, LABELEND


  adas_in, inval, action, WINTITLE = 'ADAS 704 INPUT', $
           TITLE = 'Input oic Dataset', FONT = font_large
  

  if action eq 'Cancel' then goto, LABELEND

  dsfull = inval.rootpath + inval.file

  if action eq 'Cancel' then printf, pipe, 'END ' else printf, pipe, 'CONT'
  printf, pipe, dsfull


LABEL200:

; Processing

  if find_process(pid) eq 0 then goto, LABELEND

  rep = adas704_proc(procval, bitfile, font_large=font_large)

  

  CASE rep OF
    'MENU'     : Begin
                    printf, pipe, 'MENU'
                    goto, LABELEND
                 End
    'CANCEL'   : Begin
                    printf, pipe, 'BACK'
                    goto, LABEL100
                 End
    'CONTINUE' : Begin
                    
                    printf, pipe, 'CONT'
                    printf, pipe, procval.spin
                    printf, pipe, procval.wgthn
                    printf, pipe, procval.emin
                    printf, pipe, procval.emax
                    
                 End
      
    ELSE : message, 'An error from ADAS704_OUT : reply is ' + rep
  ENDCASE
  


LABEL300:

; output file selection

  if find_process(pid) eq 0 then goto, LABELEND

  rep = adas704_out(outval, header, bitfile, font_large=font_large)


  CASE rep OF
    'MENU'     : Begin
                    printf, pipe, 'MENU'
                    goto, LABELEND
                 End
    'CANCEL'   : Begin
                    printf, pipe, 'BACK'
                    goto, LABEL200
                 End
    'CONTINUE' : Begin
                    
                    printf, pipe, 'CONT'
                    dsnout = outval.passdsn
                    printf, pipe, dsnout

                    printf, pipe, outval.texout
                    if outval.texout EQ 1 then printf, pipe, outval.texdsn
                    
                 End
      
    ELSE : message, 'An error from ADAS704_OUT : reply is ' + rep
  ENDCASE
  
  
     
; Back for more output options


GOTO, LABEL300



LABELEND:

; Ensure appending is not enabled for
; text output at start of next run.  
 
 outval.texapp = -1


; Save user defaults 

save,inval,procval,outval,filename=deffile

END
