; Copyright (c) 2000 Strathclyde University .
; SCCS info: Module @(#)$Header: /home/adascvs/idl/adas7xx/adas704/adas704_out.pro,v 1.1 2004/07/06 11:04:30 whitefor Exp $ Date $Date: 2004/07/06 11:04:30 $
;+
; PROJECT:
;       ADAS 
;
; NAME:
;	ADAS704_OUT
;
; PURPOSE: 
;       This function allows the selection of two output files 
;          - paper.text 
;          - patch file for adf18/a09_p204 driver
;
;
;
; NOTES:
;       The passing file must be seleted for the program to proceed.
; 
;
;
; INPUTS:
;        outval  - Structure of output files,
;        header  - Output header; version, date etc.
;        bitfile - Directory of bitmaps for menu button
;
;
; OPTIONAL INPUTS:
;       None
;
; OUTPUTS:
;       The function returns a string rep
;           rep = 'CONTINUE' if everything is ok
;           rep = 'CANCEL' if there is a problem or indeed if the user
;                          closes the widget with the mouse. 
;           rep = 'MENU' if user want to exit to menu at this point
;
; OPTIONAL OUTPUTS:
;       None
;
; KEYWORD PARAMETERS:
;       FONT_LARGE - The name of a larger font e.g 'courier_bold14'
;
;       FONT_SMALL - The name of a smaller font e.g 'courier_bold12'
;
;
; CALLS:
;                            
;       ADAS704_OUT_MENU   : React to menu button event. The standard IDL
;                            reaction to button events cannot deal with
;                            pixmapped buttons. Hence the special handler.
;       ADAS704_OUT_EVENT  : Reacts to cancel and Done. Does the file
;                            existence error checking.
;
; SIDE EFFECTS:
;
; CATEGORY:
;	Widgets
;
; WRITTEN:
;       Martin O'Mullane
;
; MODIFIED:
;	1.1	Martin O'Mullane
;		First release 
;
; VERSION:
;       1.1	18-09-2000
;
;-
;-----------------------------------------------------------------------------



PRO ADAS704_OUT_MENU, event

; The 'normal' get_value from a widget_button does not work with a
; bitmap button. Hence this specialised event handler.

   ; Get the info structure out of the user value of the top-level base.

Widget_Control, event.top, Get_UValue=info


; Something nefarious may cause an error also.
;  -- if so trap it and exit gracefully

CATCH, error
IF error NE 0 THEN BEGIN
   formdata = {cancel : 0, menu:1}
   *info.ptrToFormData = formdata
   Widget_Control, event.top, /Destroy
   message,!err_string
   RETURN
ENDIF


 formdata = {cancel : 0, menu:1}
 *info.ptrToFormData =formdata
 widget_Control, event.top, /destroy

END


;-----------------------------------------------------------------------------


PRO ADAS704_OUT_EVENT, event

; React to button events - Cancel and Done but not menu (this requires a
; specialised event handler ADAS704_OUT_MENU). Also deal with the passing
; directory output Default button here.

; On pressing Done check for the following


   ; Get the info structure out of the user value of the top-level base.

Widget_Control, event.top, Get_UValue=info

; Something nefarious may cause an error also.
;  -- if so trap it and exit gracefully

;CATCH, error
;IF error NE 0 THEN BEGIN
;   formdata = {cancel:1, menu : 0}
;   *info.ptrToFormData = formdata
;   Widget_Control, event.top, /Destroy
;   print,!err_string
;   RETURN
;ENDIF

Widget_Control, event.id, Get_Value=userEvent

CASE userEvent OF


  'Cancel' : begin
               formdata = {cancel:1, menu : 0}
               *info.ptrToFormData =formdata
               widget_Control, event.top, /destroy
             end 

      
  'Done'   : begin
                 
                ; gather the data for return to calling program
                
                err  = 0
                mess = ''
                
                widget_Control, info.paperID, Get_Value=pap
                widget_Control, info.passID, Get_Value=pass
                
                ; no need to set mess as it is flagged in the cw
                if pap.outbut EQ 1 AND strtrim(pap.message) NE '' then err=1
                if pass.outbut EQ 1 AND strtrim(pass.message) NE '' then err=1
                
                ; insist on a adf18/a09_p204 patch file
                
                if pass.outbut EQ 0 then begin
                   err  = 1
                   mess = 'You must choose a Auger rate pass file'
                endif                
                
                if err EQ 0 then begin
                   formdata = { paper    : pap,      $
                                pass     : pass,     $
                                cancel   : 0,        $
                                menu     : 0         }
                   *info.ptrToFormData = formdata
                   widget_control, event.top, /destroy
                endif else begin
                   widget_Control, info.messID, Set_Value=mess
                endelse

             end
             
             
  else : print,'ADAS704_OUT_EVENT : You should not see this message! ',userEvent
                   
ENDCASE

END
  
;-----------------------------------------------------------------------------



FUNCTION ADAS704_OUT, outval,                   $  
                      header,  bitfile,         $
                      FONT_LARGE = font_large,  $
                      FONT_SMALL = font_small


; Set defaults for keywords and extract info for paper.txt question

  IF n_elements(font_large) eq 0 THEN font_large = ''
  IF n_elements(font_small) eq 0 THEN font_small = ''


  paperval =  { outbut   : outval.TEXOUT,  $
                appbut   : outval.TEXAPP,  $
                repbut   : outval.TEXREP,  $
                filename : outval.TEXDSN,  $
                defname  : outval.TEXDEF,  $
                message  : outval.TEXMES   }

  passval   = { outbut   : outval.passout, $
                appbut   : outval.passapp, $
                repbut   : outval.passrep, $
                filename : outval.passdsn, $
                defname  : outval.passdef, $
                message  : outval.passmes  }


  
  


                ;********************************************
		;**** create modal top level base widget ****
                ;********************************************
                
  parent = Widget_Base(Column=1, Title='ADAS 704 OUTPUT', $
                       XOFFSET=100, YOFFSET=1)

                
  rc = widget_label(parent,value='  ',font=font_large)

                 
  
                ;**********************************
		;**** Ask for output files -   ****
                ;**** paper.txt and new adf10  ****
                ;**********************************

  base    = widget_base(parent, /column)
  
  mrow    = widget_base(base,/frame)
  paperID = cw_adas_outfile(mrow, OUTPUT='Text Output',   $
                                 VALUE=paperval, FONT=font_large)
  
  
  mlab  = widget_label(parent,value='  ',font=font_large)
  mcol  = widget_base(parent, /column)
 

  passID = cw_adas_outfile(mcol, OUTPUT='adf18/a09_p204 Auger rates',   $
                                VALUE=passval, FONT=font_large, xsize=50)

                       
                            
                ;************************************
		;**** Error/Instruction message. ****
                ;************************************
                
  messID = widget_label(parent,value='     Choose output files   ',font=font_large)
                      
                      
                            
                ;*****************
		;**** Buttons ****
                ;*****************
                
  menufile = bitfile + '/menu.bmp'
  read_X11_bitmap, menufile, bitmap1
                
  mrow     = widget_base(parent,/row)
  menuID   = widget_button(mrow,value=bitmap1,font=font_large, $
                           event_pro='ADAS704_OUT_MENU')
  cancelID = widget_button(mrow,value='Cancel',font=font_large)
  doneID   = widget_button(mrow,value='Done',font=font_large)




                ;***************************
		;**** Put up the widget ****
                ;***************************

; Realize the ADAS704_OUT input widget.

   dynlabel, parent
   widget_Control, parent, /realize

; Create a pointer to store the information collected from the form.
; The initial data stored here is set to CANCEL, so nothing needs to
; be done if the user kills the widget with the mouse.

   ptrToFormData = Ptr_New({cancel:1, menu:0})

; Create an info structure with program information.

  info = { messID          :  messID,             $
           paperID         :  paperID,            $
           passID          :  passID,             $
           font            :  font_large,         $
           ptrToFormData   :  ptrToFormData       }  
            
               
; Store the info structure in the user value of the top-level base and launch
; the widget into the world.

  widget_control, parent, Set_UValue=info

  XManager, 'ADAS704_OUT', parent, Event_Handler='ADAS704_OUT_EVENT'


; When the widget is destroyed, the block is released, and we
; return here. Get the form data that was collected by the form
; and stored in the pointer location.

formdata = *ptrToFormData


rep='CONTINUE'
IF N_Elements(formdata) EQ 0 THEN BEGIN
   Ptr_Free, ptrToFormData
   rep ='CANCEL'
ENDIF
 
IF formdata.cancel EQ 1 THEN BEGIN
   Ptr_Free, ptrToFormData
   rep ='CANCEL'
   RETURN,rep
ENDIF

IF formdata.menu EQ 1 THEN BEGIN
   Ptr_Free, ptrToFormData
   rep ='MENU
   RETURN,rep
ENDIF

if rep eq 'CONTINUE' then begin
   
    outval =  { TEXOUT      :   formdata.paper.outbut,      $
                TEXAPP      :   formdata.paper.appbut,      $
                TEXREP      :   formdata.paper.repbut,      $
                TEXDSN      :   formdata.paper.filename,    $
                TEXDEF      :   formdata.paper.defname,     $
                TEXMES      :   formdata.paper.message,     $
                passOUT     :   formdata.pass.outbut,       $
                passAPP     :   formdata.pass.appbut,       $
                passREP     :   formdata.pass.repbut,       $
                passDSN     :   formdata.pass.filename,     $
                passDEF     :   formdata.pass.defname,      $
                passMES     :   formdata.pass.message       }
 

   ; Free the pointer.
   Ptr_Free, ptrToFormData

endif       


; And tell ADAS704 that we are finished

RETURN,rep

END
