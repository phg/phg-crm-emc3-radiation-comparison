; Copyright (c) 2000 Strathclyde University .
; SCCS info: Module @(#)$Header: /home/adascvs/idl/adas7xx/adas704/adas704_proc.pro,v 1.1 2004/07/06 11:04:32 whitefor Exp $ Date $Date: 2004/07/06 11:04:32 $
;+
; PROJECT:
;       ADAS 
;
; NAME:
;	ADAS704_PROC
;
; PURPOSE: 
;       This function requests information to post process AUTOSTRUCTURE
;       oic file to produce Auger rates for adf18/a09_p204 mapping files.
;       We need a title, spin of the bound levels, n-shell weighting factor
;       alnd min and max energies.
;
;
;
; INPUTS:
;        procvalval  - Structure of stoted information
;        bitfile     - Directory of bitmaps for menu button
;
;
; OPTIONAL INPUTS:
;       None
;
; OUTPUTS:
;       The function returns a string rep
;           rep = 'CONTINUE' if everything is ok
;           rep = 'CANCEL' if there is a problem or indeed if the user
;                          closes the widget with the mouse. 
;           rep = 'MENU' if user want to exit to menu at this point
;
; OPTIONAL OUTPUTS:
;       None
;
; KEYWORD PARAMETERS:
;       FONT_LARGE - The name of a larger font e.g 'courier_bold14'
;
;       FONT_SMALL - The name of a smaller font e.g 'courier_bold12'
;
;
; CALLS:
;                            
;       ADAS704_PROC_MENU   : React to menu button event. The standard IDL
;                             reaction to button events cannot deal with
;                             pixmapped buttons. Hence the special handler.
;       ADAS704_PROC_EVENT  : Reacts to cancel and Done. Does the file
;                             existence error checking.
;
; SIDE EFFECTS:
;
; CATEGORY:
;	Widgets
;
; WRITTEN:
;       Martin O'Mullane
;
; MODIFIED:
;	1.1	Martin O'Mullane
;		First release 
;
; VERSION:
;       1.1	18-09-2000
;
;-
;-----------------------------------------------------------------------------



PRO ADAS704_PROC_MENU, event

; The 'normal' get_value from a widget_button does not work with a
; bitmap button. Hence this specialised event handler.

   ; Get the info structure out of the user value of the top-level base.

Widget_Control, event.top, Get_UValue=info


; Something nefarious may cause an error also.
;  -- if so trap it and exit gracefully

CATCH, error
IF error NE 0 THEN BEGIN
   formdata = {cancel : 0, menu:1}
   *info.ptrToFormData = formdata
   Widget_Control, event.top, /Destroy
   message,!err_string
   RETURN
ENDIF


 formdata = {cancel : 0, menu:1}
 *info.ptrToFormData =formdata
 widget_Control, event.top, /destroy

END


;-----------------------------------------------------------------------------


PRO ADAS704_PROC_EVENT, event

; React to button events - Cancel and Done but not menu (this requires a
; specialised event handler ADAS704_PROC_MENU). Also deal with the passing
; directory output Default button here.

; On pressing Done check for the following


   ; Get the info structure out of the user value of the top-level base.

Widget_Control, event.top, Get_UValue=info

; Something nefarious may cause an error also.
;  -- if so trap it and exit gracefully

;CATCH, error
;IF error NE 0 THEN BEGIN
;   formdata = {cancel:1, menu : 0}
;   *info.ptrToFormData = formdata
;   Widget_Control, event.top, /Destroy
;   print,!err_string
;   RETURN
;ENDIF

Widget_Control, event.id, Get_Value=userEvent

CASE userEvent OF


  'Cancel' : begin
               formdata = {cancel:1, menu : 0}
               *info.ptrToFormData = formdata
               widget_Control, event.top, /destroy
             end 

      
  'Done'   : begin
                 
                ; gather the data for return to calling program
                
                err  = 0
                mess = ''
                
                widget_Control, info.spinID,  Get_Value=spin
                widget_Control, info.wgthnID, Get_Value=wgthn
                widget_Control, info.eminID,  Get_Value=emin
                widget_Control, info.emaxID,  Get_Value=emax
                
                ; Some error checking
                
                if spin LE 0 then begin
                   err  = 1
                   mess = 'Possible problem with spin value'
                endif
                
                if err EQ 0 AND wgthn LE 0.0 then begin
                   err  = 1
                   mess = 'Possible problem with weight value'
                endif

                if err EQ 0 AND emin LT 0.0 then begin
                   err  = 1
                   mess = 'Possible problem with Emin value'
                endif
                
                if err EQ 0 AND emax LE 0.0 then begin
                   err  = 1
                   mess = 'Possible problem with Emax value'
                endif
                
                if err EQ 0 AND emax LE emin then begin
                   err  = 1
                   mess = 'Emax is less than Emin'
                endif
                
                                
                if err EQ 0 then begin
                   formdata = { spin   : spin,     $
                                wgthn  : wgthn,    $
                                emin   : emin,     $
                                emax   : emax,     $
                                cancel : 0,        $
                                menu   : 0         }
                   *info.ptrToFormData = formdata
                   widget_control, event.top, /destroy
                endif else begin
                   widget_Control, info.messID, Set_Value=mess
                endelse

             end
             
             
  else : print,'ADAS704_PROC_EVENT : You should not see this message! ',userEvent
                   
ENDCASE

END
  
;-----------------------------------------------------------------------------



FUNCTION ADAS704_PROC, procval, bitfile,        $
                      FONT_LARGE = font_large,  $
                      FONT_SMALL = font_small


; Set defaults for keywords and extract info for paper.txt question

  IF n_elements(font_large) eq 0 THEN font_large = ''
  IF n_elements(font_small) eq 0 THEN font_small = ''


; Do we need defaults

  if procval.new EQ -1 then begin
    
      spin  = 0
      wgthn = 0.0
      emin  = 0.0  
      emax  = 99.0 
      
  endif else begin
  
      spin  = procval.spin
      wgthn = procval.wgthn
      emin  = procval.emin
      emax  = procval.emax
  
  endelse 


                
; A modal top level base widget
                
  parent = Widget_Base(Column=1, Title='ADAS 704 PROCESS OPTIONS', $
                       XOFFSET=100, YOFFSET=1)

                
  rc = widget_label(parent,value='  ',font=font_large)


  base  = widget_base(parent, /column)
  mlab  = widget_label(base,value='Enter post proccessing data',font=font_large)
  

  
  mrow    = widget_base(base,/frame,/column)

  spinID  = cw_field(mrow, fieldfont=font_large, font=font_large, /integer, $
                     title='Spin (2S+1) of bound levels     : ', value=spin )

  wgthnID = cw_field(mrow, fieldfont=font_large, font=font_large, /floating, $
                     title='n-shell weigthing factor        : ', value=wgthn )

  eminID  = cw_field(mrow, fieldfont=font_large, font=font_large, /floating, $
                     title='Free state minimum energy (ryd) : ', value=emin )

  emaxID  = cw_field(mrow, fieldfont=font_large, font=font_large, /floating, $
                     title='Free state maximum energy (ryd) : ', value=emax )



                      
; Error message
                
  messID = widget_label(parent,value='                ',font=font_large)
                      
                      
                            
                ;*****************
		;**** Buttons ****
                ;*****************
                
  menufile = bitfile + '/menu.bmp'
  read_X11_bitmap, menufile, bitmap1
                
  mrow     = widget_base(parent,/row)
  menuID   = widget_button(mrow,value=bitmap1,font=font_large, $
                           event_pro='ADAS704_PROC_MENU')
  cancelID = widget_button(mrow,value='Cancel',font=font_large)
  doneID   = widget_button(mrow,value='Done',font=font_large)




                ;***************************
		;**** Put up the widget ****
                ;***************************

; Realize the ADAS704_PROC input widget.

   dynlabel, parent
   widget_Control, parent, /realize

; Create a pointer to store the information collected from the form.
; The initial data stored here is set to CANCEL, so nothing needs to
; be done if the user kills the widget with the mouse.

   ptrToFormData = Ptr_New({cancel:1, menu:0})

; Create an info structure with program information.

  info = { messID          :  messID,           $
           spinID          :  spinID,           $
           wgthnID         :  wgthnID,          $
           eminID          :  eminID,           $
           emaxID          :  emaxID,           $
           ptrToFormData   :  ptrToFormData     }  
            
               
; Store the info structure in the user value of the top-level base and launch
; the widget into the world.

  widget_control, parent, Set_UValue=info

  XManager, 'ADAS704_PROC', parent, Event_Handler='ADAS704_PROC_EVENT'


; When the widget is destroyed, the block is released, and we
; return here. Get the form data that was collected by the form
; and stored in the pointer location.

formdata = *ptrToFormData


rep='CONTINUE'
IF N_Elements(formdata) EQ 0 THEN BEGIN
   Ptr_Free, ptrToFormData
   rep ='CANCEL'
ENDIF
 
IF formdata.cancel EQ 1 THEN BEGIN
   Ptr_Free, ptrToFormData
   rep ='CANCEL'
   RETURN,rep
ENDIF

IF formdata.menu EQ 1 THEN BEGIN
   Ptr_Free, ptrToFormData
   rep ='MENU
   RETURN,rep
ENDIF

if rep eq 'CONTINUE' then begin
   
    procval =  { new      :   0,               $
                 spin     :   formdata.spin,   $
                 wgthn    :   formdata.wgthn,  $
                 emin     :   formdata.emin,   $
                 emax     :   formdata.emax    }
 

   ; Free the pointer.
   Ptr_Free, ptrToFormData

endif       


; And tell ADAS704 that we are finished

RETURN,rep

END
