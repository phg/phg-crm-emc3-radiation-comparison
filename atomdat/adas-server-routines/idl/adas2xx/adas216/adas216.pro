; Copyright (c) 1999, Strathclyde University.
; SCCS info: Module @(#)$Header: /home/adascvs/idl/adas2xx/adas216/adas216.pro,v 1.4 2004/07/06 10:25:55 whitefor Exp $ Date $Date: 2004/07/06 10:25:55 $
;+
; PROJECT:
;       ADAS 
;
; NAME:
;	ADAS216
;
; PURPOSE:
;	The highest level routine for the ADAS 216 program.
;
; EXPLANATION:
;	This routine is called from the main adas system routine, adas.pro,
;	to start the ADAS 216 application.  Associated with adas216.pro
;	is a FORTRAN executable.  The IDL code provides the user
;	interface and output graphics whilst the FORTRAN code reads
;	in data files, performs numerical processing and creates the
;	output files.  The IDL code communicates with the FORTRAN
;	executable via a bi-directional UNIX pipe.  The unit number
;	used by the IDL for writing to and reading from this pipe is
;	allocated when the FORTRAN executable is 'spawned' (see code
;	below).  Pipe communications in the FORTRAN process are to
;	stdin and stdout, i.e streams 5 and 6.
;
;	The FORTRAN code is an independent process under the UNIX system.
;	The IDL process can only exert control over the FORTRAN in the
;	data which it communicates via the pipe.  The communications
;	between the IDL and FORTRAN must be exactly matched to avoid
;	input conversion errors.  The correct ammounts of data must be
;	passed so that neither process 'hangs' waiting for communications
;	which will never occur.
;
;	The FORTRAN code performs some error checking which is
;	independent of IDL.  In cases of error the FORTRAN may write
;	error messages.  To prevent these error messages from conflicting
;	with the pipe communications all FORTRAN errors are written to
;	output stream 0, which is stderr for UNIX.  These error messages
;	will appear in the window from which the ADAS session/IDL session
;	is being run.
;
;	In the case of severe errors the FORTRAN code may terminate
;	itself prematurely.  In order to detect this, and prevent the
;	IDL program from 'hanging' or crashing, the IDL checks to see
;	if the FORTRAN executable is still an active process before
;	each group of pipe communications.  The process identifier
;	for the FORTRAN process, PID, is recorded when the process is
;	first 'spawned'.  The system is then checked for the presence
;	of the FORTRAN PID.
;
; USE:
;	First the system settings must be established by calling
;	adas_sys_set.pro then adas216.pro is called to start the
;	ADAS 216 application;
;
;	adas_sys_set, adasrel, fortdir, userroot, centroot, $
;		      devlist, devcode, font_large, font_small, edit_fonts
;	adas216,   adasrel, fortdir, userroot, centroot, devlist, $
;		   devcode, font_large, font_small, edit_fonts
;
; INPUTS:
;	ADASREL - A string indicating the ADAS system version, 
;		  e.g ' ADAS RELEASE: ADAS93 V1.4'.  The first
;		  character should be a space.
;
;	FORTDIR - A string holding the path to the directory where the
;		  FORTRAN executables are, e.g '/disk/adas/fortran/exec'
;
;	USERROOT - A string holding the path to the root directory of
;		   the user's adas data. e.g '/disk/bowen/adas'
;		   This root directory will be used by adas to construct
;		   other path names.  For example the users default data
;		   for adas205 should be in /disk/bowen/adas/adf04.  In
;		   particular the user's default interface settings will
;		   be stored in the directory USERROOT+'/defaults'.  An
;		   error will occur if the defaults directory does not
;		   exist.
;
;	CENTROOT - Like USERROOT, but this directory points to the
;		   central data area for the system.  User defaults are
;		   not stored on CENTROOT.
;
;	FONT_LARGE - The name of a larger font e.g 'courier_bold14'
;
;	FONT_SMALL - The name of a smaller font e.g 'courier_bold12'
;
;	EDIT_FONTS - A structure of two fonts used in the table editor
;		     adas_edtab.pro, {font_norm:'',font_input:''}.
;		     The two fonts are used to differentiate between
;		     editable and non-editable parts of the table. You
;		     may have to experiment to get two fonts which give
;		     the same default spacing in the edit widget.
;		     e.g {font_norm:'helvetica_bold14', $
;			  font_input:'helvetica_oblique14'}
;
; OPTIONAL INPUTS:
;	None
;
; OUTPUTS:
;	None - Note this routine should leave all inputs un-modified.
;
; OPTIONAL OUTPUTS:
;	None
;
; KEYWORD PARAMETERS:
;	None
;
; CALLS:
;	FIND_PROCESS	Checks to see if a given process is active.
;       XXDATE          Get date and time from operating system.
;
; SIDE EFFECTS:
;	This routine spawns a FORTRAN executable.  Note the pipe 
;	communications routines listed above.  In addition to these
;	pipe communications there is one explicit communication of the
;	date to the FORTRAN code, search for 'printf, pipe' to find it.
;	There are also communications of the variable gomenu to the
;	FORTRAN which is used as a signal to stop the program in its
;	tracks and return immediately to the series menu. Do the same
;	search as above to find the instances of this.
;
; CATEGORY:
;	Adas system.
;	
; WRITTEN:
;       Martin O'Mullane
;
; MODIFIED:
;	1.1	Martin O'Mullane
;		First release 
;	1.2	Richard Martin
;		Increased version no. to 1.2
;	1.3	Richard Martin
;		Increased version no. to 1.3
;	1.4	Richard Martin
;		Increased version no. to 1.4
;
; VERSION:
;       1.1	17-03-99
;	1.2	21-03-00
;	1.3	10-11-00
;	1.4	20-11-01
; 
;-
;-----------------------------------------------------------------------------

PRO ADAS216,	adasrel, fortdir, userroot, centroot, 			$
		devlist, devcode, font_large, font_small, edit_fonts

		;************************
		;**** Initialisation ****
		;************************

    adasprog = ' PROGRAM: ADAS216 V1.4'
    lpend    = 0
    gomenu   = 0
    deffile  = userroot+'/defaults/adas216_defaults.dat'
    bitfile  = centroot+'/bitmaps'
    device   = ''
  
    dumpfile = userroot+'/pass/adas216_dump.out'


    sorry = ['This option is currently unavailable.',  $
             'Perhaps in the next release!         '  ]


		;************************************************
		;**** We need a set of colours in this code. ****
		;************************************************
                
red  =[ 0, 255,255,  0,  0,255,255,  0,255,127,127,0,90,150,188,220,240,255]
green=[ 0, 255,  0,255,  0,255,  0,255,127,255,127,0,90,150,188,220,240,255]
blue =[ 0, 255,  0,  0,255,  0,255,255,127,127,255,0,90,150,188,220,240,255]
TVLCT, red, green, blue
		
                
                ;***************************************
		;**** We need the IDL version also. ****
		;***************************************
                
    idl_ver = strmid(!version.release,0,1)
    defsysv,'!except',1



		;******************************************
		;**** Search for user default settings ****
		;**** If not found create defaults     ****
		;**** inval: settings for data files   ****
		;**** procval: settings for processing ****
		;**** outval: settings for output      ****
		;******************************************
                
    tfont_struct    = { ef,                                         $
                        font_norm   :   edit_fonts.font_norm,       $
                        font_input  :   edit_fonts.font_input }

    files = findfile(deffile)
    
    if files(0) eq deffile then begin
    
        restore, deffile
        
        inval.centroot1 = centroot+'/adf04/'
        inval.userroot1 = userroot+'/adf04/'
        inval.centroot2 = centroot+'/adf18/'
        inval.userroot2 = userroot+'/adf18/'
        inval.adasrel   = adasrel
        inval.fortdir   = fortdir
        inval.userroot  = userroot
        inval.centroot  = centroot
        inval.devlist   = devlist
        inval.devcode   = devcode
        inval.fontl     = font_large
        inval.fonts     = font_small
        inval.efont     = tfont_struct
        
    endif else begin
    
        inval =	{ rootpath1	:	userroot+'/adf04/',		$
		  file1		:	'',				$
		  centroot1	:	centroot+'/adf04/',		$
		  userroot1	:	userroot+'/adf04/',		$
		  rootpath2	:	userroot+'/adf18/',		$
		  file2		:	'',				$
		  centroot2	:	centroot+'/adf18/',		$
		  userroot2	:	userroot+'/adf18/',		$
                  adasrel	:	adasrel,                        $
                  fortdir	:	fortdir,                        $
                  userroot	:	userroot,                       $
                  centroot	:	centroot,                       $
                  devlist	:	devlist,                        $
                  devcode	:	devcode,                        $
                  fontl		:	font_large,                     $
                  fonts		:	font_small,                     $
                  efont		:	tfont_struct,                   $
                  expsel	:	0                               }
  
        procval = {new:-1}

        errorprocval = {new:-1}
        
        outval = {      grpout          :       0,                      $
                        gtitle          :       '',                     $
                        texout          :       0,                      $
                        texapp          :       -1,                     $
                        texrep          :       0,                      $
                        texdsn          :       '',                     $
                        texdef          :       'paper.txt',            $
                        texmes          :       ''			}
    end



		;***********************************************
		;**** Inform user that this is a beta with  ****
		;**** some missing functionality.           ****
		;***********************************************
        
  mess = ['This is a beta release of ADAS216.  ',  $
          'Some features are missing and others',  $
          'do not work fully.                  ',  $
          '                                    ',  $
          'In particular :                     ',  $
          ' - only ONE metastable is considered',  $
          ' - only errors in excitation rates  ',  $      
          '   are allowed                      ',  $      
          ' - cannot specify error according to',  $      
          '   spin yet                         ',  $      
          ' - overview plotting is limited     ' ]      
 
 
 
  tell= popup(message=mess, buttons=['Accept'],font=font_large)

		
                
                
                ;****************************
		;**** Start fortran code ****
		;****************************

    spawn, fortdir+'/adas216.out', unit=pipe, /noshell, PID=pid

		;************************************************
		;**** Get date and write to fortran via pipe ****
		;************************************************

    date = xxdate()
    printf, pipe, date(0)



LABEL100:

		;**** Check FORTRAN still running ****
  
    if find_process(pid) eq 0 then goto, LABELEND

		;************************************************
		;**** Communicate with bgspf0 in fortran and ****
		;**** invoke user interface widget for       ****
		;**** Data file selection                    ****
		;************************************************

    bgspf0, pipe, inval, rep, dsninc, FONT=font_large

		;**** If cancel selected then end program ****

    if rep eq 'YES' then goto, LABELEND

		;**** Check FORTRAN still running ****

    if find_process(pid) eq 0 then goto, LABELEND

                ;******************************
                ;**** Get info from bgsetm ****
                ;******************************
                
    bgsetm, pipe, sz0, sz, scnte, sil, ndstr, strga,      $
            ndmet, npl, strgmi, strgmf



LABEL200:

    if find_process(pid) eq 0 then goto, LABELEND

		;************************************************
		;**** Communicate with bgispf in fortran and ****
		;**** invoke user interface widget for       ****
		;**** processing options                     ****
		;************************************************

    selem = ''
    bgispf, pipe, lpend, procval, 					  $
            sz, sz0, selem, strga, dsninc,                 			  $
            gomenu, bitfile, inval, npl, strgmi, strgmf, ndmet,           $
	    userroot,							  $
            FONT_LARGE=font_large, FONT_SMALL=font_small, 		  $
            EDIT_FONTS=edit_fonts
 
		;**** If menu button clicked, tell FORTRAN to stop ****

    if gomenu eq 1 then begin
 	printf, pipe, 1
 	goto, LABELEND
    endif else begin
 	printf, pipe, 0
    endelse


		;**** If cancel selected then goto previous panel ****

    if lpend eq 1 then goto, LABEL100

                ;*************************************************
		;**** Fortran processing now in progress. Get ****
		;**** date and time for this run.	      ****
                ;*************************************************

    date = xxdate()

		;**** Create header for output. ****

    header = adasrel+adasprog+' DATE: '+date(0)+' TIME: '+date(1)


LABEL300:
		;**** Check FORTRAN still running ****

    if find_process(pid) eq 0 then goto, LABELEND


		;*************************************************
		;**** Communicate with bgispf2 in fortran and ****
		;**** invoke user interface widget for error  ****
		;**** processing.                             ****
		;*************************************************


    tev     = procval.tine(0:procval.maxt-1)
    dens    = procval.dine(0:procval.maxd-1)
    iunit_t = procval.ifout 
    iunit_d = procval.idout 
    
    bgispf2, pipe, lpend, errorprocval, 					  $
             sz, sz0, selem,     	             			  $
             strga, tev, dens, iunit_t, iunit_d, 		   	  $
             dsninc, gomenu, bitfile, devlist, devcode, dumpfile,      	  $
	     userroot,							  $
             FONT_LARGE=font_large, FONT_SMALL=font_small, 		  $
             EDIT_FONTS=edit_fonts
 
		;**** If menu button clicked, tell FORTRAN to stop ****

    if gomenu eq 1 then begin
 	printf, pipe, 1
 	goto, LABELEND
    endif else begin
 	printf, pipe, 0
    endelse


		;**** If cancel selected then goto previous panel ****

    if lpend eq 1 then goto, LABEL200




LABEL400:
		;**** Check FORTRAN still running ****

    if find_process(pid) eq 0 then goto, LABELEND

		;************************************************
		;**** Communicate with bgspf1 in fortran and ****
		;**** invoke user interface widget for       ****
		;**** Output options                         ****
		;************************************************
    
    ; put the title on the output screen also - it is not
    ; allowed to be edited.
    
    if procval.new ne -1 then outval.gtitle = procval.title
    
    bgspf1, pipe, lpend, outval, dsninc, 		$ 
            header, bitfile, gomenu,			$
	    FONT=font_large

                ;**** Extra check to see whether FORTRAN is still there ****

    if find_process(pid) eq 0 then goto, LABELEND

		;**** If menu button clicked, tell FORTRAN to stop ****

    if gomenu eq 1 then begin
	printf, pipe, 1
        outval.texmes = ''
	goto, LABELEND
    endif else begin
	printf, pipe, 0
    endelse

                    
                ;***********************************************
                ;**** If cancel selected then erase output  ****
                ;**** messages and goto process window 300. ****
                ;***********************************************

    if lpend eq 1 then begin
        outval.texmes = ''
        goto, LABEL300
    endif
    
    
                ;****************************************************
                ;**** Here we write the results of the error     ****
                ;**** analysis - depends on the analysis option. ****
                ;****************************************************

    
    if (outval.texout eq 1) or (outval.grpout eq 1) then begin
    
       il     = where(strga eq ' *** END OF LEVELS *** ')    
       numlev = il(0)
       numte  = procval.maxt
       numden = procval.maxd
       
       if errorprocval.analysis eq 0 then begin
       
          mean_u = fltarr(numlev,numte,numden)
          sd_u   = fltarr(numlev,numte,numden)

          val = {dumpfile : dumpfile,	         $
                 numte	  : numte,               $
                 numden   : numden,              $
                 numlev   : numlev,              $
                 numbin   : errorprocval.ust.nb, $
                 xhist    : errorprocval.ust.xp  }
    
;          if idl_ver eq '4' then $
;             bgout_cum_calc_v4, mean_u, sd_u, VALUE=val, FONT=font_large $
;          else bgout_cum_calc, mean_u, sd_u, VALUE=val, FONT=font_large

	     bgout_cum_calc, mean_u, sd_u, VALUE=val, FONT=font_large
         
       endif
       
    endif
    
    if outval.texout eq 1 then begin
        
       openw, lun, outval.texdsn, /append, /get_lun

       if (errorprocval.analysis eq 0) then begin
          
          vpr = {numte	  : numte,               $
                 numden   : numden,              $
                 numlev   : numlev,              $
                 te       : tev,                 $
                 den      : dens,                $
                 mean_u   : mean_u,              $
                 sd_u     : sd_u,                $
                 slev     : strga(0:numlev-1),   $
                 iunit_t  : iunit_t,             $
                 iunit_d  : iunit_d              }
     
          
          bgout_cum, lun, value=vpr
                    
       endif else begin
         
         mess = ['Writing of 3D explore errors.',' ',sorry]
         tell= popup(message=sorry, buttons=['I understand'],font=font_large)

       endelse
      
       free_lun,lun
    
    endif
                ;***************************************************
                ;**** If graphical output requested communicate ****
                ;**** with bgoutg in fortran to get data.       ****     
                ;***************************************************

    if outval.grpout eq 1 then begin


                ;**** Optional user title/comment ****

       utitle = outval.gtitle
       
       ; The upper and lower error surfaces are mean =/- 1 standard deviation
       ; Set sd=0.0 to 99 and trap the excessive value in the 3D plotting 
       ; routine - bgout_gr_plot.
       
       ind_zero = where(sd_u eq 0.0, count)
       if count ne 0 then sd_u(ind_zero) = 99.9
       
       surface_l = mean_u-sd_u
       surface_u = mean_u+sd_u
       
       
          
       if (errorprocval.analysis eq 0) then begin
           val = {title      : utitle,              $
                  numte	     : numte,               $
                  numden     : numden,              $
                  numlev     : numlev,              $
                  te         : tev,                 $
                  den        : dens,                $
                  surface_l  : surface_l,           $
                  surface_u  : surface_u,           $
                  slev       : strga(0:numlev-1),   $
                  iunit_t    : iunit_t,             $
                  iunit_d    : iunit_d              }
       endif

       if (errorprocval.analysis eq 0) then begin
          
           bgout_gr, devlist, devcode, value=val,                     $
                     font_large=font_large,  font_small=font_small
       
       endif else begin
         
         mess = ['Plotting of 3D explore errors.',' ',sorry]
         tell= popup(message=sorry, buttons=['I understand'],font=font_large)

       endelse

        
         ; Let fortran know that it can continue
         printf, pipe, 0
    
    endif


                ;**************************************
                ;**** Back for more output options ****
                ;**************************************

    GOTO, LABEL400

LABELEND:

                ;*********************************************
                ;**** Ensure appending is not enabled for ****
                ;**** text output at start of next run.   ****
                ;*********************************************

    outval.texapp = -1


		;**** Save user defaults ****

    save, inval, procval, errorprocval, outval, filename=deffile


END
