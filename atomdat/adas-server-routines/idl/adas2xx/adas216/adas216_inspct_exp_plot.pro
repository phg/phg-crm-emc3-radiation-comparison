; Copyright (c) 1999, Strathclyde University.
; SCCS info: Module @(#)$Header: /home/adascvs/idl/adas2xx/adas216/adas216_inspct_exp_plot.pro,v 1.1 2004/07/06 10:26:14 whitefor Exp $ Date $Date: 2004/07/06 10:26:14 $
;+
; PROJECT:
;       ADAS 
;
; NAME:
;	ADAS216_INSPCT_EXP_PLOT
;
; PURPOSE:
;	Plots to screen (or a file) the inspected data from adas216_inspct_exp.
;       Errors are caught here and reported back to allow for graceful recovery.
;       If printing and a transition has been highlighted then more information
;       is printed than is plotted. In the plotting the extra information on
;       the errors and transitions responsible are indicated in the widget. 
;
; EXPLANATION:
;
; INPUTS:
;       VALUE - Structure holding the data and other information for plotting.
;       PRINT - Structure holding information for printing to a file.
;
; OPTIONAL INPUTS:
;	None.
;
; OUTPUTS:
;        
;
; OPTIONAL OUTPUTS:
;       None
;
; KEYWORD PARAMETERS:
;
; CALLS:
;
; SIDE EFFECTS:
;
; CATEGORY:
;
;
; WRITTEN:
;       Martin O'Mullane
;
; MODIFIED:
;	1.1	Martin O'Mullane
;		First release 
;
; VERSION:
;       1.1	17-03-99
;
;-
;-----------------------------------------------------------------------------

PRO adas216_inspct_exp_plot, errid,  VALUE=value, FONT=font, PRINT=print

   IF NOT (KEYWORD_SET(font))  THEN font = ''
   IF NOT (KEYWORD_SET(value)) THEN $  
           message,'VALUE structure must be passed to adas216_inspct_plot'

   if (keyword_set(print)) then begin
     
     grtype = print.devcode( where(print.dev eq print.devlist) )
     set_plot,grtype
     
     if print.paper eq 'A4' then begin
        xsize = 15.0
        ysize = 16.0
     endif else begin
        xsize = 15.0
        ysize = 16.0
     endelse
     
     !p.font=0
     device,/color,bits=8,filename=print.dsn,  		$
            /Helvetica, font_size=12, /portrait,        $
            xsize=xsize, ysize=ysize
    
     ; Leave space for information
     
     !y.omargin=[1, 3]
   
   
   endif else begin
   
     !y.omargin=[1, 1]
   
   endelse

   ; Trap any errors and report back to the user. Hopefully it will not 
   ; crash the program as we jump to the end in the case of an error.

   catch, error_status
   
   if error_status ne 0 then begin
      print,'An error occurred in ADAS216_INSPCT_EXP_PLOT'
      print, !err_string 
      widget_control, errid, set_value = 'Possible error'
      goto,LABELend
   endif
   
   
   ; get data from value structure
   
   
   numtran  = value.numtran
   te       = value.te
   den      = value.den
   slev     = value.slev
   iunit_t  = value.iunit_t
   iunit_d  = value.iunit_d
   lev_ind  = value.lev_ind
   te_ind   = value.te_ind
   den_ind  = value.den_ind
   ichoice  = value.ichoice
   high_tr  = value.high_tr
   err_tr   = value.err_tr
   ad_tr    = value.ad_tr
   index_tr = value.index_tr
   yp       = value.yp
      
   numte  = n_elements(te)
   numden = n_elements(den)
    


   ; what are we plotting 
   
   name_t  = ['K','eV','Red.']
   name_d  = ['cm!U-3!N','Red.']
 
   if ichoice eq 0 then  begin
      xtitle = 'T!De!N (' + name_t(iunit_t-1) + ')'
      xmin   = te(0)
      xmax   = te(numte-1)
      xp     = te
      atstr  = string(den(den_ind), format='(e8.2)') + $
               ' (' + name_d(iunit_d-1) + ')'
   endif else begin
      xtitle = 'n!De!N (' + name_d(iunit_d-1) + ')'
      xmin   = den(0)
      xmax   = den(numden-1)
      xp     = den
      atstr  = string(te(te_ind), format='(e8.2)') + $
               ' (' + name_t(iunit_t-1) + ')'
   endelse

   
   ; Get max and min for y-axis, put up a line at y=1.0 and determine
   ; whether it will be a log or a linear x-axis.

   
   ymax = 1.12 * max(yp)     ; to round it up a little bit
   ymin = 2.0 - ymax


   if ( xmax/xmin ge 1e2) then begin 
      plot_oi,[xmin,xmax],[ymin,ymax], xtitle=xtitle, ystyle=1, /nodata, $
              ytitle = 'Fractional deviation'
   endif else begin
      plot,[xmin,xmax],[ymin,ymax], xtitle=xtitle, ystyle=1, /nodata, $
              ytitle = 'Fractional deviation'
   endelse

   ; overplot a line of y=1 -  make sure it covers all the x axis!

   x1 = [0.001*xmin,100.0*xmax]
   y1 = [1.0,1.0]
   oplot,x1,y1, linestyle=1
      
         
   for j = 0, numtran-1 do begin
     
     y = yp(*,j)   
     ind_pl = where(y gt 0.0, count)
     if count ne 0 then oplot, xp(ind_pl), y(ind_pl)

     ; is there a highlighted transition ?

     if (count ne 0) and (j eq high_tr) then   $
           oplot, xp(ind_pl), y(ind_pl), psym=4, symsize=0.8
     
   endfor

   
   ; display 'useful' information
   
   xyouts, 0.5, 0.97, align=0.5, /normal, 'Level : ' + slev(lev_ind) +   $
           '   at ' + atstr, charsize=1.1
   
   date = xxdate()
   xyouts, 0.9, 0.02, align=0.5, /normal, 'ADAS216   ' + date(0), charsize=0.6

  
   ; Display relevant information which changes depending on 
   ; whether we are printing or plotting to the screen. 
   
   if high_tr ne -1 then begin

      i1     = index_tr/1000
      i2     = index_tr - (i1*1000)
      tr1str = slev(i1-1) + ' - '
      tr2str = slev(i2-1)

      adstr  = '    Abs. Difference : ' + string(ad_tr, format='(f5.2)')
      errstr = '    Error : ' + string(err_tr, format='(f5.2)')
      outstr = 'Transition : ' + tr1str + tr2str + adstr + errstr
   
      xyouts, 0.5,0.93,align=0.5, /normal, outstr, charsize=0.8
      
   endif


LABELend:


   ; reset graphical parameters
   
   !p.multi = [0,1,1,0,0]               
   !y.omargin=[0, 0]


   if (keyword_set(print)) then begin
     device,/close
     set_plot,'X'
     !p.font=-1
   endif

END
