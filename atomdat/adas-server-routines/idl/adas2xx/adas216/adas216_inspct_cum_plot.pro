; Copyright (c) 1999, Strathclyde University.
; SCCS info: Module @(#)$Header: /home/adascvs/idl/adas2xx/adas216/adas216_inspct_cum_plot.pro,v 1.1 2004/07/06 10:26:09 whitefor Exp $ Date $Date: 2004/07/06 10:26:09 $
;+
; PROJECT:
;       ADAS 
;
; NAME:
;	ADAS216_INSPCT_CUM_PLOT
;
; PURPOSE:
;	Plots to screen (or a file) the inspected data from adas216_inspct.
;       Errors in the fitting are (always?) caught here and reported back to
;       allow for graceful recovery.
;
; EXPLANATION:
;
; INPUTS:
;       VALUE - Structure holding the data and other information for plotting.
;       PRINT - Structure holding information for printing to a file.
;
; OPTIONAL INPUTS:
;	None.
;
; OUTPUTS:
;       STATS - Structure holding statistical data.
;
; OPTIONAL OUTPUTS:
;       None
;
; KEYWORD PARAMETERS:
;
; CALLS:
;
; SIDE EFFECTS:
;
; CATEGORY:
;
;
; WRITTEN:
;       Martin O'Mullane
;
; MODIFIED:
;	1.1	Martin O'Mullane
;		First release 
;
; VERSION:
;       1.1	17-03-99
;
;-
;-----------------------------------------------------------------------------

PRO adas216_inspct_cum_plot, errid, stats, VALUE=value, FONT=font, PRINT=print

   IF NOT (KEYWORD_SET(font))  THEN font = ''
   IF NOT (KEYWORD_SET(value)) THEN $  
           message,'VALUE structure must be passed to adas216_inspct_plot'

   
   line_col = 5

   if (keyword_set(print)) then begin
     
     grtype = print.devcode( where(print.dev eq print.devlist) )
     set_plot,grtype
     
     if print.paper eq 'A4' then begin
        xsize = 24.0
        ysize = 18.0
     endif else begin
        xsize = 23.0
        ysize = 17.0
     endelse
     
     line_col = 0 
     
     !p.font=0
     device,/color,bits=8,filename=print.dsn,  		$
            /Helvetica, font_size=12, /landscape,       $
            xsize=xsize, ysize=ysize
   endif 
   
   ; unfortunately gaussfit is IDL version specific
   
   idl_ver = strmid(!version.release,0,1)
   
   

   ; Trap any errors from the fitting and report back to the user.
   ; Hopefully it will not crash the program as we jump to the end
   ; in the case of an error.

   catch, error_status
   
   if error_status ne 0 then begin
      print,'An error occurred in ADAS216_INSPCT_PLOT'
      print, !err_string 
      widget_control, errid, set_value = 'Possible mathematical error'
      goto,LABELend
   endif
   
   
   ; get data from value structure
   
   
   num_nz  = value.num_nz
   ind_nz  = value.ind_nz
   te      = value.te
   den     = value.den
   slev    = value.slev
   iunit_t = value.iunit_t
   iunit_d = value.iunit_d   
   lev_ind = value.lev_ind
   te_ind  = value.te_ind
   den_ind = value.den_ind
   ichoice = value.ichoice
   numiter = value.numiter
   ihist_p = value.ihist_p
   xhist   = value.xhist
   pop_u   = value.pop_u
   
   
   stats.numind  = num_nz
   stats.numiter = numiter
   stats.slevel  = slev(lev_ind)
   stats.popu    = pop_u
   
    
   ; How many rows of 4 graphs do we need?
   ; Leave space for a title and footer
  
   !y.omargin=[7, 7]
   n_rows = numlines(num_nz+2,4)
   !p.multi = [0,4,n_rows,0,0]
 

   ; define statisical arrays
   
   meanx  = fltarr(num_nz)
   sd     = fltarr(num_nz)
   mean_u = fltarr(num_nz)
   sd_u   = fltarr(num_nz)



   ; what are we plotting and set correct units in stats structure
   ; note that we want stats.units to be of length 4 irrespective of units

   name_t  = ['K','eV','Red.']
   name_d  = ['cm!U-3!N','Red.']
   name_ds = ['cm-3','Red.']
 
   temp = '    '
   if ichoice eq 0 then  begin
      units = ' ' + name_t(iunit_t-1)
      strput, temp, name_t(iunit_t-1), 0
      atstr = string(den(den_ind), format='(e8.2)') + $
              ' (' + name_d(iunit_d-1) + ')'
   endif else begin
      units = ' ' + name_d(iunit_d-1)
      strput, temp, name_ds(iunit_d-1), 0
      atstr = string(te(te_ind), format='(e8.2)') + $
              ' (' + name_t(iunit_t-1) + ')'
   endelse
   stats.units = temp

   ; x is the bin axis and y holds the histogram values
   ; do the gaussian fitting here

   aa = fltarr(4)
   
   for i = 0, num_nz-1 do begin

     ind = ind_nz(i)

     y = ihist_p(i,*)
     y = reform(y)
     x = xhist

     if ichoice eq 0 then  begin
        str='Te : ' + string(te(ind),format='(e8.2)') + units
     endif else begin
        str='ne : ' + string(den(ind),format='(e8.2)') + units
     endelse

     if idl_ver eq '4' then  begin
      	getfit,x,y,aa
           	ez = (x-aa(1))/aa(2)
		yfit = aa(3) + aa(0) * exp(-(ez^2)/2.0)     
     endif else begin
		yfit = gaussfit(x,y,aa,nterms=4)
     end
                            
     plot,x,y, title=str, xtitle='Deviation from unperturbed value'
     oplot,x,yfit,linestyle=2, thick=1.5, color=line_col

     meanx(i)  = aa(1)*pop_u(i)
     sd(i)     = aa(2)*pop_u(i)
     mean_u(i) = aa(1)
     sd_u(i)   = aa(2)
     
     stats.a(*,i) = aa

   endfor


   if ichoice eq 0 then  begin
      x   = te(ind_nz)
      str = 'T!De!N (' + units + ')' 
   endif else begin
      x   = den(ind_nz)
      str = 'n!De!N (' + units + ')'
   endelse
   stats.xvar = x



   ; show error in the populations

   plot_oo,x,meanx+sd, linestyle=2,  $
   title = 'Population' ,$
   xtitle = str, ytitle = 'N!Di!N/N!Dmet!N' 
   oplot,x,meanx
   oplot,x,meanx-sd, linestyle=2


   ; and in the fractional populations

   plot_oi,x,mean_u+sd_u, linestyle=2, yrange=[0.8,1.2], $
   title='Fractional population', $
   xtitle = str, ytitle = 'Deviation from unperturbed value',  $
   symsize=0.5
   oplot,x,mean_u, psym=-6, symsize=0.5
   oplot,x,mean_u-sd_u, linestyle=2
   
   
   ; overplot a line of y=1 -  make sure it covers all the x axis!
   
   x1 = [0.001*x(1),100.0*x(num_nz-1)]
   y1 = [1.0,1.0]
   oplot,x1,y1, linestyle=1

   
   ; display 'useful' information
   
   xyouts, 0.5, 0.95, align=0.5, /normal, 'Level : ' + slev(lev_ind) + $
           '   at ' + atstr +  $
           '   :- ' + string(numiter,format='(i5)') + ' iterations',  $
           charsize=1.1
   
   date = xxdate()
   xyouts, 0.9, 0.05, align=0.5, /normal, 'ADAS216   ' + date(0), charsize=0.6

  
LABELend:


   ; reset graphical parameters
   
   !p.multi = [0,1,1,0,0]               
   !y.omargin=[0, 0]


   if (keyword_set(print)) then begin
     device,/close
     set_plot,'X'
     !p.font=-1
   endif

END
