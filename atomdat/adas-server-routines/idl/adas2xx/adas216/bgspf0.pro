; Copyright (c) 1999, Strathclyde University.
; SCCS info: Module @(#)$Header: /home/adascvs/idl/adas2xx/adas216/bgspf0.pro,v 1.1 2004/07/06 11:41:06 whitefor Exp $ Date $Date: 2004/07/06 11:41:06 $
;+
; PROJECT:
;       ADAS  
;
; NAME:
;	BGSPF0
;
; PURPOSE:
;	IDL user interface and communications with ADAS216 FORTRAN
;	process via pipe.
;
; EXPLANATION:
;	Firstly this routine invokes the part of the user interface
;	used to select the input dataset for ADAS216.  When the user's
;	interactions are complete this routine communicates with the
;	ADAS216 FORTRAN application via a UNIX pipe.  Communications
;	are to the FORTRAN subroutine BGSPF0.
;
; USE:
;	The use of this routine is specific to ADAS216, see adas216.pro.
;
; INPUTS:
;	PIPE	- The IDL unit number of the bi-directional pipe to the
;		  ADAS216 FORTRAN process.
;
;	VALUE	- A structure which determines the initial settings of
;		  the dataset selection widget.  The initial value is
;		  set in adas216.pro.  VALUE is passed un-modified
;		  through to cw_adas_in.pro, see that routine for a full
;		  description.
;
; OPTIONAL INPUTS:
;	None
;
; OUTPUTS:
;	VALUE	- On output the structure records the final settings of
;		  the dataset selection widget if the user pressed the
;		  'Done' button otherwise it is not changed from input.
;
;	DSFULL	- String; The full system file name of the emitting ion
;		  dataset selected by the user for processing.
;
;	REP	- String; Indicates whether the user pressed the 'Done'
;		  or 'Cancel' button on the interface.  The action is
;		  converted to the strings 'NO' and 'YES' respectively
;		  to match up with the existing FORTRAN code.  In the
;		  original IBM ISPF interface REP was the reply to the
;		  question 'DO YOU WANT TO END PROGRAM EXECUTION'.
;
; OPTIONAL OUTPUTS:
;	None
;
; KEYWORD PARAMETERS:
;	FONT	- Supplies the font to be used for the interface widgets.
;
; CALLS:
;	ADAS_IN		Pops-up the dataset selection widget.
;
; SIDE EFFECTS:
;	This routine communicates with the ADAS216 FORTRAN process
;	via a UNIX pipe.
;
; CATEGORY:
;	Adas system.
;
; WRITTEN:
;       Martin O'Mullane, 30/06/1998
;
; MODIFIED:
;	1.1	Martin O'Mullane
;		First Release, based on b8spf0.pro
; VERSION:
;	1.1	17-03-99
;
;-----------------------------------------------------------------------------


PRO bgspf0, pipe, value, rep, dsfull,  FONT=font


                ;**** Return to caller on error ****

    ON_ERROR, 2

                ;**** Set defaults for keywords ****

    IF NOT (KEYWORD_SET(font)) THEN font = ''


		;**********************************
		;**** Pop-up input file widget ****
		;**********************************

    adas208_in, value, action, WINTITLE = 'ADAS 216 INPUT', 		$
	     TITLE = 'Input SPECIFIC ION File', FONT = font

		;********************************************
		;**** Act on the event from the widget   ****
		;********************************************
		;**** There are only two possible events ****
		;**** 'Done' and 'Cancel'.               ****
		;********************************************

    if action eq 'Done' then begin
        rep = 'NO'
    end else begin
        rep = 'YES'
    end

		;**** Construct dataset names ****

    dsfull = value.rootpath1+value.file1
    dsnexp = value.rootpath2+value.file2

                ;**** Determine whether expansion file exists ****

    file_acc, dsnexp, ex, re, wr, ex, ty
    if ty ne '-' then begin
      lpdata = 1
    end else begin
      lpdata = 0
    end

		;*******************************
		;**** Write data to fortran ****
		;*******************************

    printf, pipe, rep
    printf, pipe, dsfull
    if lpdata eq 1 then begin
      dsnexp = ' ' 
      printf, pipe, dsnexp
    end else begin
      printf, pipe, dsnexp
    end


END
