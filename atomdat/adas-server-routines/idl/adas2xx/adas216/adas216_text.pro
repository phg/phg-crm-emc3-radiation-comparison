; Copyright (c) 1999, Strathclyde University.
; SCCS info: Module @(#)$Header: /home/adascvs/idl/adas2xx/adas216/adas216_text.pro,v 1.1 2004/07/06 10:26:25 whitefor Exp $ Date $Date: 2004/07/06 10:26:25 $
;+
; PROJECT:
;       ADAS 
;
; NAME:
;	ADAS216_TEXT
;
; PURPOSE:
;	A popup window for displaying adf04 error information.
;
; EXPLANATION:
;	This routine pops up a window which uses a scrollable text widget to
;	display the errors in an adf04 file.  'Done' and 'Write to file'
;       buttons are included in the widget for the user to complete the 
;       browsing and to save the results for later patching into an adf04 file.
;
; INPUTS:
;       VALUE - Structure holding error information.
;
; OPTIONAL INPUTS:
;	None.
;
; OUTPUTS:
;       The return value of this function is the ID of the compound
;       widget which is of type LONG integer.
;
; OPTIONAL OUTPUTS:
;       None
;
; KEYWORD PARAMETERS:
;	XSIZE	- Integer; x size in characters of browse widget.
;		  Default is 50.
;
;	YSIZE	- Integer; y size in characters of browse widget.
;		  Default is 20.
;
;	FONT	- A font to use for all text in this widget.
;
; CALLS:
;	CW_ADAS_OUTFILE    output dialog for patch file
;
; SIDE EFFECTS:
;	One other routine is included which is used to manage the widget;
;
;	ADAS216_TEXT_EVENT
;
; CATEGORY:
;	Widgets
;
; WRITTEN:
;       Martin O'Mullane
;
; MODIFIED:
;	1.1	Martin O'Mullane
;		First release 
;
; VERSION:
;       1.1	17-03-99
;
;-
;-----------------------------------------------------------------------------

PRO adas216_text_event, event

 Widget_Control, event.top, Get_UValue=state
 Widget_Control, event.id,  Get_UValue=buttonEvent

CASE buttonEvent OF
  'DONE'  : widget_control, event.top, /destroy
  'WRITE' : begin
              widget_control,state.patch, get_value=fval
              if fval.outbut eq 1 then begin
                 if fval.appbut ne 1 then begin
                    openw,lun,fval.filename,/get_lun
                    fval.appbut = 0
                 endif else begin
                    openw,lun,fval.filename,/get_lun,/append
                 endelse
                 
                 for j = 0,n_elements(state.text)-1 do begin
                    printf, lun, state.text(j)
                 endfor
                 
                 free_lun,lun                 
                 fval.message='Output written to file'
                 widget_control,state.patch, set_value=fval
              endif
 
            end
                   
ENDCASE

END

;-----------------------------------------------------------------------------


PRO adas216_text, value, XSIZE = xsize, YSIZE = ysize, FONT = font


		;**** Set defaults for keywords ****

  IF NOT (KEYWORD_SET(xsize)) THEN xsize = 50
  IF NOT (KEYWORD_SET(ysize)) THEN ysize = 20
  IF NOT (KEYWORD_SET(font))  THEN font = ''


                ;***********************************
		;**** create titled base widget ****
                ;***********************************
                
  parent = widget_base(title='Errors for adf04 analysis', $
			xoffset=100,yoffset=100,/column)


		;**** Title ****
                
  rc = widget_label(parent,value='Errors to be applied',font=font)


		;**** Default case in text widget ****
                
  errstr='**** NO ERRORS DEFINED ****'


		;**** Write errors into text widget ****
     
  adas216_text_sub,'Excitation', value.numexc, value.defexc, $   
                                 value.genexc, value.spfexc, $
                                 errstr   
 
  adas216_text_sub,'Recombination', value.numrec, value.defrec, $   
                                    value.genrec, value.spfrec, $
                                    errstr   

  adas216_text_sub,'Charge Exchange', value.numcxr, value.defcxr, $   
                                      value.gencxr, value.spfcxr, $
                                      errstr   

  adas216_text_sub,'Ionisation', value.numion, value.defion, $   
                                 value.genion, value.spfion, $
                                 errstr   
  
  
		;**** Remove 'no errors defined' message ****
                
  errs = n_elements(errstr)
  if errs gt 1 then errstr=errstr(1:errs-1)
  errs = n_elements(errstr)


		;**** Set up text for adf04 file patch ****
                
  textstr = ['C+++ERROR specification start +++',' ']

  exc_list = ['---','Def','Gen','Spe']         ; exclude these lines 
  nexclist = n_elements(exc_list)
  v        = intarr(errs,nexclist)
  for j = 0,nexclist-1 do begin
    v(*,j) = strpos(errstr,exc_list(j))
  endfor
  
  for j=0,errs-1 do begin
     if total(v(j,*)) eq -1*nexclist then textstr = [textstr,errstr(j)]
  endfor 

  textstr = [textstr,'C+++ERROR specification end +++']
                 
                 
                ;*****************************************
		;**** Text widget to display comments ****
                ;*****************************************

  textid = widget_text(parent,value=errstr,/scroll,xsize=xsize,		$
			ysize=ysize,font=font,/no_copy)
   

                ;**************************************
                ;**** Widget for patch file output ****
                ;**************************************

    outfval = { OUTBUT   : value.errout,   $
                APPBUT   : value.errapp,   $
                REPBUT   : value.errrep,   $
                FILENAME : value.errdsn,   $
                DEFNAME  : value.errdef,   $
                MESSAGE  : ' '             }
                
    base   = widget_base(parent,/row,/frame)
    patch  = cw_adas_outfile(base, OUTPUT='Patch file Output',   $
                             VALUE=outfval, FONT=font)


                ;*****************
		;**** Buttons ****
                ;*****************
                
  base    = widget_base(parent,/row)
  doneid  = widget_button(base,value='Done',font=font,Uvalue='DONE')
  writeid = widget_button(base,value='Write to file',font=font,Uvalue='WRITE')
  
                ;***************************************************
		;**** put text and patch file info into a state ****
                ;***************************************************
   
  state = {  patch  :  patch,    $ 
             text   :  textstr   }
  
  
                ;****************************
		;**** realize the widget ****
                ;****************************

  dynlabel, parent
  widget_control,parent,/realize, set_uvalue=state


		;**** make widget modal ****
                
  xmanager,'adas216_text',parent,event_handler='adas216_text_event',/modal,/just_reg
 

END
