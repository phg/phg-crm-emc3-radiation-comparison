; Copyright (c) 1999, Strathclyde University.
; SCCS info: Module @(#)$Header: /home/adascvs/idl/adas2xx/adas216/cw_adas216_entry.pro,v 1.1 2004/07/06 12:35:52 whitefor Exp $ Date $Date: 2004/07/06 12:35:52 $
;+
; PROJECT:
;       ADAS 
;
; NAME:
;	CW_ADAS216_ERROR()
;
; PURPOSE:
;	Produces a widget for ADAS216 processing options/input.
;
; EXPLANATION:
;	This function creates a compound widget consisting of a text
;	widget holding an editable 'Run title', the dataset name/browse
;	widget cw_adas_dsbr, label widgets with charge information for
;	the input data, a table widget for temperature data cw_adas_table,
;	a second cw_adas_table widget for density data, buttons to
;	enter default values into the temperature and density tables,
;	a multiple selection widget cw_adas_sel, a reaction settings
;	widget cw_adas_reac2, a message widget, a 'Cancel' button and
;	finally a 'Done' button.
;
;	The compound widgets included in this widget are self managing.
;	This widget only manages events from the two 'Defaults' buttons,
;	the 'Cancel' button and the 'Done' button.
;
;	This widget only generates events for the 'Done' and 'Cancel'
;	buttons.
;
; USE:
;	This widget is specific to ADAS216, see adas216_error.pro
;	for use.
;
; INPUTS:
;	TOPPARENT- Long integer, ID of parent widget.
;
;	SZ	- String, recombined ion charge read.
;
;	SZ0	- String, nuclear charge read.
;
;	STRGA	- String array, level designations.
;
;	DSNINC	- String; The full system file name of the input COPASE
;		  dataset selected by the user for processing.
;
;	NDTEM	- Integer; Maximum number of temperatures allowed.
;
;	NDDEN	- Integer; Maximum number of densities allowed.
;
;	NDMET	- Integer; Maximum number of metastables allowed.
;
;	IL	- Integer; Number of energy levels.
;
;	NV	- Integer; Number of termperatures.
;
;	TSCEF	- dblarr(8,3); Input electron temperatures in three units.
;
;	The inputs SZ to TSCEF map exactly onto variables of the same
;	name in the ADAS216 FORTRAN program.
;
;       BITFILE - String; the path to the directory containing bitmaps
;                 for the 'escape to series menu' button.
;
;	INVAL	- Structure; the input screen structure - see cw_adas216_in.pro
;
;	SELEM	- String; selected element symbol
;
; OPTIONAL INPUTS:
;	None
;
; OUTPUTS:
;	The return value of this function is the ID of the declared widget.
;
; OPTIONAL OUTPUTS:
;	None
;
; KEYWORD PARAMETERS:
;	VALUE	- A structure which determines the initial settings of
;		  the processing options widget.
;
;		  The default VALUE is created thus;
;
;			imetr = make_array(ndmet,/int,value=0)
;			temp = dblarr(ndtem)
;			dens = dblarr(ndden)
;			ps = { $
;		 	TITLE:'',	NMET:0, $
;			IMETR:imetr,	IFOUT:1, $
;			MAXT:0,		TINE:temp, $
;			TINP:temp,	TINH:temp, $
;			IDOUT:1,	MAXD:0, $
;			DINE:dens,	DINP:dens, $
;			RATHA:dens,	RATPIA:dens, $
;			ZEFF:'',	LPSEL:0, $
;			LZSEL:0,	LISEL:0, $
;			LHSEL:0,	LRSEL:0, $
;                       LIOSEL:0,       LNSEL:0, $
;			LNORM:0,		 $
;			VAL502:struct502,	 $
;			WVLS:0.0,	WVLL:0.0,$
;			AVLT:0.0  }
;
; 		TITLE	entered general title for program run
; 		NMET	number of metastables
; 		(IMETR(I),I=1,NMET)	index of metastables
; 		IFOUT	input temperature units (1,2,3)
; 		MAXT	number of input temperatures (1-20)
; 		(TINE(I),I=1,MAXT)	electron temperatures
; 		(TINP(I),I=1,MAXT)	proton temperatures
; 		(TINH(I),I=1,MAXT)	neutral hydrogen temperatures
; 		IDOUT	input density units (1,2)
; 		MAXD	number of input densities
; 		(DINE(I),I=1,MAXD)	electron densities
; 		(DINP(I),I=1,MAXD)	proton densities
; 		(RATHA(I),I=1,MAXD)	ratio (neut h dens/elec dens)
; 		(RATPIA(I),I=1,MAXD)	ratio (n(z+1)/n(z) stage abund)
; 		ZEFF	plasma z effective, a string holding a valid number.
; 		LPSEL	include proton collisions?
; 		LZSEL	scale proton collisions with plasma z effective?
; 		LISEL	include ionisation rates?
; 		LHSEL	include charge transfer from neutral hydrogen?
; 		LRSEL	include free electron recombination?
;               LIOSEL to be determined
;               LNSEL   include projected bundle-n data?
;
;		All of the above structure elements map onto variables of
;		the same name in the ADAS216 FORTRAN program.
;
;
;	UVALUE	- A user value for the widget. Default 0.
;
;	FONT_LARGE - The name of a larger font.  Default current system
;		     font
;
;	FONT_SMALL - The name of a smaller font. Default current system
;		     font.
;
;	EDIT_FONTS - A structure of two fonts used in the table editor
;		     adas_edtab.pro, {font_norm:'',font_input:''}.  Defaults
;		     to current system font.
;
;	NUM_FORM   - String; Numeric format to use in tables.  Default
;			'(E10.3)'
;		     
;
; CALLS:
;	POPUP		 Popup warning window with buttons.
;
;	See side effects for other related widget management routines.
;
; SIDE EFFECTS:
;
;	The following widget management routines are included in this
;	file;
;	ENTRY216_GET_VAL()	Returns the current VALUE structure.
;	ENTRY216_EVENT()		Process and issue events.
;
; CATEGORY:
;	Compound Widget
;
; WRITTEN:
;       Martin O'Mullane
;
; MODIFIED:
;	1.1	Martin O'Mullane
;		First release 
;
; VERSION:
;       1.1	17-03-99
;
;-
;-----------------------------------------------------------------------------

PRO entry216_set_val, id, value


                ;**** Return to caller on error ****
  ON_ERROR, 2

		 ;***************************************
                 ;****     Retrieve the state 	     ****
		 ;**** Get first_child widget id     ****
		 ;**** because state is stored there ****
		 ;***************************************

  first_child = widget_info(id, /child)
  widget_control, first_child, get_uvalue = state, /no_copy 



  val_field  = strarr(3)


		;**** default error entry ****

  val_field(0)  ='  *'
  val_field(1)  ='  *'
  if value.num_typ(0) eq 1 then begin
     val_field(2) = string(value.def_typ,format='(f8.2)')  
  endif else begin
     val_field(2) = '        '
  endelse

  for j = 0, 2 do begin

    widget_control, state.entryid(0,j), set_value = val_field(j)

  endfor


		;**** general error entry ****


  for i = 0, state.ndgen - 1 do begin
  
     
     val_field(0)  ='  *'
     if i le value.num_typ(1)-1 then begin
        val_field(1) = string(value.gen_typ(i,0),format='(i3)')  
        val_field(2) = string(value.gen_typ(i,1),format='(f8.2)')  
     endif else begin
        val_field(1) = '   '
        val_field(2) = '        '
     endelse
      
     for j = 0 ,2 do begin
       
       widget_control, state.entryid(i+1,j), set_value = val_field(j)
          
     endfor
     
  endfor

                ;**** Save the new state ****

  widget_control, first_child, set_uvalue=state, /no_copy



END

;-----------------------------------------------------------------------------


FUNCTION entry216_get_val, id


                ;**** Return to caller on error ****
  ON_ERROR, 2

		 ;***************************************
                 ;****     Retrieve the state 	     ****
		 ;**** Get first_child widget id     ****
		 ;**** because state is stored there ****
		 ;***************************************

  first_child = widget_info(id, /child)
  widget_control, first_child, get_uvalue = state, /no_copy


                ;***************************************
                ;**** Get values of changed entries ****
                ;***************************************
  
  blanks = '        '
  
                ;**** defaults first ****
  
  state.num_typ(0) = 0

  widget_control, state.entryid(0,2), get_value=str1

  if str1(0) ne blanks then state.def_typ(0) = float(str1(0))
  
  if  str1(0) ne blanks then state.num_typ(0) =  1  
  
  

                ;**** general errors next ****
  ind = 0            
  
  state.num_typ(1) = 0
   
  for i = 1, state.ndgen do begin   ; start at 1 for the general errors
                
   ; entry 1 should be integer but the array is a float - change when using it
   ; entry 0 is a '*' 

    widget_control, state.entryid(i,1), get_value=str1
    widget_control, state.entryid(i,2), get_value=str2

    if str1(0) ne '   '  then state.gen_typ(ind,0) = float(str1(0))
    if str2(0) ne blanks then state.gen_typ(ind,1) = float(str2(0))
    
       
    if ( str1(0) ne '   '     and      		$
         str2(0) ne blanks  ) then		$
        state.num_typ(1) = state.num_typ(1) + 1  
         
    ind = ind + 1
        
  endfor

		;***********************************************
		;**** write selected values to PS structure ****
		;***********************************************

  ps = {entry216_set,		            $ 			
            err_type  :  state.err_type,    $
            num_typ   :  state.num_typ,     $
            def_typ   :  state.def_typ,     $
            gen_typ   :  state.gen_typ,     $
            spf_typ   :  state.spf_typ      }

    widget_control, first_child, set_uvalue=state, /no_copy

  RETURN, ps

END

;-----------------------------------------------------------------------------

FUNCTION entry216_event, event


                ;**** Base ID of compound widget ****
                
   parent=event.handler

		;**********************************************
                ;**** Retrieve the user value state        ****
		;**** Get id of first_child widget because ****
		;**** user value "state" is stored there   ****
		;**********************************************

   first_child = widget_info(parent, /child)
   widget_control, first_child, get_uvalue=state,/no_copy


		;*********************************
		;**** Clear previous messages ****
		;*********************************
                
   widget_control,state.messid,set_value=' '

                ;***************************************
                ;**** Process events                ****
                ;**** Get values of changed entries ****
                ;***************************************

                ;**** defaults first ****
                
    
  if event.id eq state.entryid(0,2)  then begin

     widget_control, state.entryid(0,2), get_value=str
     if (num_chk(str(0)) ne 0) then begin
        widget_control,state.messid,set_value='Error is not a vaild number'
        widget_control, state.entryid(0,2), set_value='        '
     endif

  endif
       

                ;**** general errors next ****
  
  for i = 1, state.ndgen - 1 do begin   ; start at 1 for the general errors
                
    if event.id eq state.entryid(i,1)  then begin   ; entry 1 should be integer 
                                                    ; entry 0 is a '*' 

       widget_control, state.entryid(i,1), get_value=str
       if (num_chk(str(0),/integer) ne 0) then begin
          widget_control,state.messid,set_value='Level must be integer'
          widget_control, state.entryid(i,1), set_value='   '
       endif

    endif

    
    if event.id eq state.entryid(i,2)  then begin

       widget_control, state.entryid(i,2), get_value=str
       if (num_chk(str(0)) ne 0) then begin
          widget_control,state.messid,set_value='Error is not a vaild number'
          widget_control, state.entryid(i,2), set_value='        '
       endif

    endif
       
       
  endfor


  
		;*********************************************
		;*** make "state" available to other files ***
		;*********************************************

  widget_control, first_child, set_uvalue=state, /no_copy

  action = 'entry'

  RETURN, {ID:parent, TOP:event.top, HANDLER:0L, ACTION:action}
  
END

;-----------------------------------------------------------------------------

FUNCTION cw_adas216_entry, topparent, 					$
		sz, sz0, selem, il, strga, ndgen, ndspf, 		$
		VALUE=value, UVALUE=uvalue, 				$
		FONT_LARGE=font_large, FONT_SMALL=font_small, 		$
		EDIT_FONTS=edit_fonts, NUM_FORM=num_form

		;**** Set defaults for keywords ****
                
  IF NOT (KEYWORD_SET(uvalue))     THEN uvalue = 0
  IF NOT (KEYWORD_SET(font_large)) THEN font_large = ''
  IF NOT (KEYWORD_SET(font_small)) THEN font_small = ''
  IF NOT (KEYWORD_SET(edit_fonts)) THEN $
				edit_fonts = {font_norm:'',font_input:''}
  IF NOT (KEYWORD_SET(num_form))   THEN num_form = '(E10.3)'

  IF NOT (KEYWORD_SET(value)) THEN begin
        num_arr = intarr(3)
        gen_arr = fltarr(ndgen,3)
        spf_arr = fltarr(ndspf,4)
	ps = {entry216_set,	                          $
		        err_type  :    0,    		  $
                        num_typ   :    num_arr,           $
                        def_typ   :    0.0,               $
                        gen_typ   :    gen_arr,           $
                        spf_typ   :    spf_arr            }
  END ELSE BEGIN
	ps = {entry216_set,	                          $
		        err_type  :    value.err_type,    $
                        num_typ   :    value.num_typ,     $
                        def_typ   :    value.def_typ,     $
                        gen_typ   :    value.gen_typ,     $
                        spf_typ   :    value.spf_typ      }
  END
  

                ;*********************************************************
                ;**** Modify certain parameters and results depending ****
                ;**** on the machine being used                       ****
                ;*********************************************************

    machine = GETENV('TARGET_MACHINE')
    if machine eq 'HPUX' then begin
        y_size = 4
        large_font = font_small
    endif else begin
        y_size = 6
        large_font = font_large
    endelse




		;*********************************************************
		;**** Create the 216 Error entry options/input window ****
		;*********************************************************

		;**** create titled base widget ****
                
  parent = widget_base(topparent, UVALUE = uvalue, 		$
		       EVENT_FUNC     = "entry216_event", 	$
		       FUNC_GET_VALUE = "entry216_get_val", 	$
		       PRO_SET_VALUE  = "entry216_set_val", 	$
		       /COLUMN)

		;******************************************************
		;**** Create a dummy widget just to hold value of *****
		;**** "state" variable so as not to get confused  *****
		;**** with any other values. Adopt IDL practice   *****
		;**** of using first child widget                 *****
		;******************************************************

  first_child = widget_base(parent)
  topbase = widget_base(first_child,/column)



		;*********************************
		;**** Error entry information ****
		;*********************************
                
  dum   = size(ps.gen_typ,/dimensions) 
  ndgen = dum(0)             
  dum   = size(ps.spf_typ,/dimensions) 
  ndspf = dum(0)             
  
  entryid = lonarr(ndgen+1,3)   ; number of general + the default
  
                
  base   = widget_base(topbase,/row)
  
  base1  = widget_base(base,/column)
  
  
  line = widget_label(base1,font=font_large, value='              -/+%')

  val_field  = strarr(3)
  size_field = intarr(3)
  edit_field = intarr(3)

  size_field(0) = 3
  size_field(1) = 3
  size_field(2) = 8


		;**** default error entry ****

  line = widget_base(base1, /row)

  val_field(0)  ='  *'
  val_field(1)  ='  *'
  edit_field(0) = 0
  edit_field(1) = 0
  edit_field(2) = 1
  if ps.num_typ(0) eq 1 then begin
     val_field(2) = string(ps.def_typ,format='(f8.2)')  
  endif else begin
     val_field(2) = '        '
  endelse

  for j = 0, 2 do begin

    entryid(0,j) = widget_text(line, 				$
                               xsize    = size_field(j), 	$
                               editable = edit_field(j),    	$
                               value    = val_field(j),     	$
                               font     = font_small         	)

    if j eq 0 then $
         gap = widget_label(line,font=font_small, value=' - ')   $
    else gap = widget_label(line,font=font_small, value='  ') 

  endfor


		;**** general error entry ****


  for i = 0, ndgen - 1 do begin
  
     line = widget_base(base1, /row)
     
     val_field(0)  ='  *'
     edit_field(0) = 0
     edit_field(1) = 1
     edit_field(2) = 1
     if i le ps.num_typ(1)-1 then begin
        val_field(1) = string(ps.gen_typ(i,0),format='(i3)')  
        val_field(2) = string(ps.gen_typ(i,1),format='(f8.2)')  
     endif else begin
        val_field(1) = '   '
        val_field(2) = '        '
     endelse
      
     for j = 0 ,2 do begin
       
       entryid(i+1,j) = widget_text(line, 			$
                                    xsize    = size_field(j), 	$
                                    editable = edit_field(j),   $
                                    value    = val_field(j),    $
                                    font     = font_small       )
                                  
       if j eq 0 then $
            gap = widget_label(line,font=font_small, value=' - ')   $
       else gap = widget_label(line,font=font_small, value='  ') 
          
     endfor
     
  endfor
  
 
 
  skip  = widget_label(base1,font=font_large, value=' ')
                       
  setid = widget_button(base1, value = 'Set Individual Level', $
                        /align_left,font=font_large)

		;***********************
		;**** Error message ****
		;***********************
                
  messid = widget_label(parent,font=font_large, value='  ')


 
		;*************************************************
		;**** create a state structure for the pop-up ****
		;**** window.                                 ****
		;*************************************************
                
  new_state = { sz              :       sz,             $
                selem           :       selem,          $
	        messid          :       messid,         $
	        entryid         :       entryid,        $
                font            :       font_large,     $
                ndgen           :       ndgen,    	$
                err_type        :       ps.err_type,    $
                num_typ         :       ps.num_typ,     $
                def_typ         :       ps.def_typ,     $
                gen_typ         :       ps.gen_typ,     $
                spf_typ         :       ps.spf_typ      }

                ;**** Save initial state structure ****

    widget_control, first_child, set_uvalue=new_state, /no_copy

  RETURN, parent

END
