; Copyright (c) 1999, Strathclyde University.
; SCCS info: Module @(#)$Header: /home/adascvs/idl/adas2xx/adas216/adas216_inspct_exp.pro,v 1.1 2004/07/06 10:26:12 whitefor Exp $ Date $Date: 2004/07/06 10:26:12 $
;+
; PROJECT:
;       ADAS 
;
; NAME:
;	ADAS216_INSPCT_EXP
;
; PURPOSE:
;	A popup window for displaying fits to iterated data. Slices
;       corresponding to temperature or density for a particular  level
;       can be viewed. The statistics of the fit can be viewed and
;       printed and/or the plot can be printed to a file.
;
; EXPLANATION:
;	This routine allows the user to navigate through the cumulative
;       error data. Slices of gaussian fits corresponding to a range of
;       temperatures or densities for a particular  level are plotted.
;       Only those Te/ne which have sufficient data to fit are plotted.
;       ie if the unperturbed population is 0.0  (ie too small) nothing
;       is shown. The data is read from a file. The size of the array
;       should be small enough for most systems to read into memory as it
;       is stored as integer*2. It is 4-D though as there are
;       numden*numte*numlev histograms each with numbin bins!
;     
;
; INPUTS:
;       VALUE - Structure holding necessary information.
;
;               numte    : number of temperatures  
;               numden   : number of densities 
;               numlev   : number of levels
;               numiter  : number of iterations
;               numbin   : number of bins for histograms
;               te       : temperatures  
;               den      : densities
;               slev     : levels (string)
;               iunit_t  : temperature units (integer) (K, eV, reduced)   
;               iunit_d  : density units (integer) (cm**-3, reduced)   
;               xhist    : x values of histogram
;               dumpfile : file where histograms are stored
;
; OPTIONAL INPUTS:
;	None.
;
; OUTPUTS:
;       The return value of this function is the ID of the compound
;       widget which is of type LONG integer.
;
; OPTIONAL OUTPUTS:
;       None
;
; KEYWORD PARAMETERS:
;	XSIZE	- Integer; x size in characters of browse widget.
;		  Default is 400.
;
;	YSIZE	- Integer; y size in characters of browse widget.
;		  Default is 340.
;
;	FONT	- A font to use for all text in this widget.
;
; CALLS:
;	ADAS_TEXT            to browse/print an array of strings
;       ADAS216_INSPCT_PLOT  plots to screen or file the fits 
;       ADAS_FILE_GR         popup widget to select a file for 
;                            graphics output. 
;
; SIDE EFFECTS:
;	One other routine is included which is used to manage the widget;
;
;	ADAS216_INSPCT_EVENT
;
; CATEGORY:
;	Widgets
;
; WRITTEN:
;       Martin O'Mullane
;
; MODIFIED:
;	1.1	Martin O'Mullane
;		First release 
;
; VERSION:
;       1.1	17-03-99
;
;-
;-----------------------------------------------------------------------------

PRO inspct_exp_advance, state, advance
    
  last_tr = state.last_tr
  lev_ind = state.lev_ind
  te_ind  = state.te_ind
  den_ind = state.den_ind
   

  ; get previouly highlighted transition

  if last_tr ne -1 then begin
  
    if state.ichoice eq 0 then begin
       numtran = state.teplot.ind_t(lev_ind,den_ind)
       yl = state.teplot.rt(lev_ind,*,den_ind,last_tr)
       xp = state.te
    endif else begin
       numtran = state.neplot.ind_d(lev_ind,te_ind)
       yl = state.neplot.rd(lev_ind,te_ind,*,last_tr)
       xp = state.den
    endelse 
    yl = reform(yl)
    
  endif else begin
  
    if state.ichoice eq 0 then begin
       numtran = state.teplot.ind_t(lev_ind,den_ind)
       xp = state.te
    endif else begin
       numtran = state.neplot.ind_d(lev_ind,te_ind)
       xp = state.den
    endelse 
    yl = -1
    
  endelse


  ; which way are we going

  if advance eq 1 then begin
 
     this_tr = last_tr + 1 
     if this_tr gt numtran-1 then this_tr = 0
     
  endif else begin
  
     this_tr = last_tr - 1 
     if this_tr lt 0 then this_tr = numtran-1
     
  endelse


  ; this is the transition to highlight

  if state.ichoice eq 0 then begin
     yn = state.teplot.rt(lev_ind,*,den_ind,this_tr)
  endif else begin
     yn = state.neplot.rd(lev_ind,te_ind,*,this_tr)
  endelse 
  yn = reform(yn)

  ; highlight in yellow

  ind_pl = where(yl gt 0.0, count)
  if count ne 0 then oplot, xp(ind_pl), yl(ind_pl), color=1

  ind_pl = where(yn gt 0.0, count)
  if count ne 0 then oplot, xp(ind_pl), yn(ind_pl), color=5

  state.last_tr = this_tr


  ; tell us what it is

  if state.ichoice eq 0 then begin
     err  = state.teplot.err_t(lev_ind,den_ind,this_tr)
     ad   = state.teplot.adiff_t(lev_ind,den_ind,this_tr)
     idum = state.teplot.index_t(lev_ind,den_ind,this_tr)
  endif else begin
     err  = state.neplot.err_d(lev_ind,te_ind,this_tr)
     ad   = state.neplot.adiff_d(lev_ind,te_ind,this_tr)
     idum = state.neplot.index_d(lev_ind,te_ind,this_tr)
  endelse 

  i1     = idum/1000
  i2     = idum - (i1*1000)
  tr1str = state.slev(i1-1) + ' - '
  tr2str = state.slev(i2-1)

  adstr  = 'Abs. Difference : ' + string(ad, format='(f5.2)')
  errstr = 'Error           : ' + string(err, format='(f5.2)')

  widget_control, state.adid,  set_value=adstr
  widget_control, state.errid, set_value=errstr
  widget_control, state.tr1id, set_value=tr1str
  widget_control, state.tr2id, set_value=tr2str



END

;-----------------------------------------------------------------------------

PRO adas216_inspct_exp_event, event

 Widget_Control, event.top, Get_UValue=state, /no_copy
 Widget_Control, event.id,  Get_UValue=userEvent

CASE userEvent OF

  'CHOICE' : begin
	     
             widget_control, state.choiceid, get_value = bpress
             state.ichoice = bpress
             
             if bpress eq 0 then begin
                widget_control, state.neid, map=1
                widget_control, state.teid, map=0
             endif
  
             if bpress eq 1 then begin
                widget_control, state.teid, map=1
                widget_control, state.neid, map=0
             endif
               
             widget_control, state.printid, sensitive=0
	
            end
            
  'LEVS'  : begin
              widget_control, state.printid, sensitive=0
              state.lev_ind = event.index
              state.last_tr = -1
            end
            
  'TE'    : begin
              widget_control, state.printid, sensitive=0
              state.te_ind = event.index
              state.last_tr = -1
            end
            
  'DEN'   : begin
              widget_control, state.printid, sensitive=0
              state.den_ind = event.index
              state.last_tr = -1
            end
	
  'RIGHT' : inspct_exp_advance, state, 1
             
  'LEFT'  : inspct_exp_advance, state, -1 
             
  'DONE'   : begin
               widget_control, event.top, /destroy
             end
  
  'PLOT'  : begin

               widget_control,state.updateid,set_value =  '                           '
               widget_control, state.go_l_id, sensitive=1
               widget_control, state.go_r_id, sensitive=1
               
               lev_ind = state.lev_ind
               te_ind  = state.te_ind
               den_ind = state.den_ind
               
               widget_control, state.drawid, get_value = draw_win
               wset,  draw_win
               erase
               
               
               if state.ichoice eq 0 then begin
                  yp = state.teplot.rt(lev_ind,*,den_ind,*)
                  numtran = state.teplot.ind_t(lev_ind,den_ind)
               endif else begin
                  yp = state.neplot.rd(lev_ind,te_ind,*,*)
                  numtran = state.neplot.ind_d(lev_ind,te_ind)
               endelse
               yp = reform(yp)

               
               ; only relevant if we want to print!
               err   = -999.9
               ad    = -999.9
               index = -999
              
               plot_stuff = { numtran  :  numtran,          $
                              te       :  state.te,         $
                              den      :  state.den,        $
                              slev     :  state.slev,       $
                              lev_ind  :  lev_ind,          $
                              te_ind   :  te_ind,           $
                              den_ind  :  den_ind,          $
                              iunit_t  :  state.iunit_t,    $
                              iunit_d  :  state.iunit_d,    $
                              ichoice  :  state.ichoice,    $
                              high_tr  :  state.last_tr,    $
                              err_tr   :  err,              $
                              ad_tr    :  ad,               $
                              index_tr :  index,            $
                              yp       :  yp                }
                              
               adas216_inspct_exp_plot, state.updateid, value = plot_stuff 
                                                    
                
               widget_control, state.printid, sensitive=1
                                                    
     LABEL_bailout:                 
               !p.multi = [0,1,1,0,0]               
               
            end
            
  'PRINT'  : begin
	
               widget_control,state.updateid,set_value =  '                           '

               print_val = { dsn     : '',                  $
                             defname : 'adas216_slice.ps',  $
                             replace :  1,                  $
                             devlist :  state.devlist,      $
                             dev     :  state.devlist(0),   $
                             paper   :  'A4',               $
                             write   :  0,                  $
                             message :  'Note: cannot append to graphics files'}
               act = 0
               adas_file_gr, print_val, act, font=state.font, $
                             title='File name for graphical output'
               
                              
               print_stuff = { dsn     : print_val.dsn,   $
                               devcode : state.devcode,   $
                               devlist : state.devlist,   $
                               dev     : print_val.dev,   $
                               paper   : print_val.paper  }
               
               lev_ind = state.lev_ind
               te_ind  = state.te_ind
               den_ind = state.den_ind
               this_tr = state.last_tr
                              
               
               if state.ichoice eq 0 then begin
                  yp = state.teplot.rt(lev_ind,*,den_ind,*)
                  numtran = state.teplot.ind_t(lev_ind,den_ind)
               endif else begin
                  yp = state.neplot.rd(lev_ind,te_ind,*,*)
                  numtran = state.neplot.ind_d(lev_ind,te_ind)
               endelse
               yp = reform(yp)

               ; when printing we need to put the highlighted transition
               ; (if any) information on the plot
               
               if this_tr ne -1 then begin
               
                  if state.ichoice eq 0 then begin
                     err   = state.teplot.err_t(lev_ind,den_ind,this_tr)
                     ad    = state.teplot.adiff_t(lev_ind,den_ind,this_tr)
                     index = state.teplot.index_t(lev_ind,den_ind,this_tr)
                  endif else begin
                     err   = state.neplot.err_d(lev_ind,te_ind,this_tr)
                     ad    = state.neplot.adiff_d(lev_ind,te_ind,this_tr)
                     index = state.neplot.index_d(lev_ind,te_ind,this_tr)
                  endelse 
               
               endif else begin
               
                  err   = -999.9
                  ad    = -999.9
                  index = -999

               endelse

               
               plot_stuff = { numtran  :  numtran,          $
                              te       :  state.te,         $
                              den      :  state.den,        $
                              slev     :  state.slev,       $
                              lev_ind  :  lev_ind,          $
                              te_ind   :  te_ind,           $
                              den_ind  :  den_ind,          $
                              iunit_t  :  state.iunit_t,    $
                              iunit_d  :  state.iunit_d,    $
                              ichoice  :  state.ichoice,    $
                              high_tr  :  this_tr,          $
                              err_tr   :  err,              $
                              ad_tr    :  ad,               $
                              index_tr :  index,            $
                              yp       :  yp                }
                              
                              
               if print_val.write eq 1 then begin               
                              
                  adas216_inspct_exp_plot, state.updateid,          $
                                           value = plot_stuff,      $
                                           print = print_stuff 
	
                  widget_control, state.updateid, $
                                  set_value = 'Plot written to file'
                                  
               endif else begin
               
                  widget_control, state.updateid, $
                                  set_value = 'No file selected'
                                  
               endelse
               
             end
             
             
  else : print,'ADAS216_INSPCT_EXP_EVENT : You should not see this message! ',userEvent
                   
ENDCASE

                ;*****************************************
		;**** Update state but only if we are ****
                ;**** NOT leaving the widget.         ****
                ;*****************************************

 if userEvent ne 'DONE' then $
    widget_control, event.top, set_uvalue=state, /no_copy

END

;-----------------------------------------------------------------------------


PRO adas216_inspct_exp, devlist, devcode, $
                        VALUE=value, XSIZE = xsize, YSIZE = ysize, FONT = font


		;**** Set defaults for keywords ****

  IF NOT (KEYWORD_SET(xsize)) THEN xsize = 400
  IF NOT (KEYWORD_SET(ysize)) THEN ysize = 340
  IF NOT (KEYWORD_SET(font))  THEN font = ''
  IF NOT (KEYWORD_SET(value)) THEN $  
          message,'A structure must be passed to adas216_inspct'



  dumpfile = value.dumpfile
  
  numte    = value.numte
  numden   = value.numden
  numlev   = value.numlev
  numstr   = value.nstr
  
  te       = value.te
  den      = value.den
  slev     = value.slev
    
  iunit_t  = value.iunit_t
  iunit_d  = value.iunit_d
  
 
 
                ;***********************************
		;**** create titled base widget ****
                ;***********************************
                
  parent = widget_base(title='Results of Exploration', $
			xoffset=20,yoffset=20,/column)
                
  rc = widget_label(parent,value='  ',font=font)

                 
  
                ;*****************************************
		;**** Choice of slice to plot section ****
                ;*****************************************

  mrow     = widget_base(parent, /row, /frame, /align_center)
 
  choiceid = cw_bgroup(mrow,['Temperature','Density    '],  $
                             exclusive=1,column=1,          $
                             label_left = 'Plot :',	    $
                             /no_release,  Uvalue='CHOICE',font=font)
                             
  mlab   = widget_label(mrow, value=' at ', font=font)
  
  base   = widget_base(mrow,/align_center)
  trow   = widget_base(base)
  drow   = widget_base(base)
  

  temps  = string(te,format='(e8.2)')
  teid   = widget_droplist(trow, value=temps, font=font, Uvalue='TE')

  denss  = string(den,format='(e8.2)')
  neid   = widget_droplist(drow, value=denss, font=font, Uvalue='DEN')

  mlab   = widget_label(mrow, value=' for level ', font=font)
  
  levs   = slev(0:numlev-1)
  levid  = widget_droplist(mrow, value=levs,/align_center,  $
                          font=font,  Uvalue='LEVS')



 		;***********************************
		;**** set up initial conditions ****
		;***********************************
 
 
  widget_control, levid, set_droplist_select = 0
  widget_control, teid,  set_droplist_select = 0
  widget_control, neid,  set_droplist_select = 0

  widget_control, teid, map=0
  widget_control, choiceid, set_value = 0



                ;*****************************************
		;**** Draw widget to display results. ****
                ;*****************************************

  device, get_screen_size=scrsz
  xwidth  = scrsz(0)*0.40
  yheight = scrsz(1)*0.40

  drow   = widget_base(parent,/row,/frame)
  dcol   = widget_base(drow,/column,xpad=10,ypad=10)
  
  ; leave some space before info column
  
  skip   = widget_label(drow, value='  ', font=font)
  mcol   = widget_base(drow,/column,/base_align_left)
  
  
  drawid = widget_draw(dcol, xsize=xwidth, ysize=yheight) 
                
               
                ;**************************************************
                ;**** Set advance / retract transition buttons ****
                ;**************************************************
  bittmp=getenv('ADASCENT')         
  bitfile=bittmp+'/bitmaps'     
  go_l_file = bitfile + '/go_l.bmp'
  read_X11_bitmap, go_l_file, go_l_bmp
  go_r_file = bitfile + '/go_r.bmp'
  read_X11_bitmap, go_r_file, go_r_bmp

  base    = widget_base(dcol, /row, /align_center)
  go_l_id = widget_button(base, value=go_l_bmp, Uvalue='LEFT')
  message = '    ' 
  panelid = widget_label(base, value=message, font=font)
  go_r_id = widget_button(base, value=go_r_bmp, Uvalue='RIGHT')


                ;**** Highlighted transition info column ****
  
  mlab   = widget_label(mcol, value='Transition : ', font=font)

  tr1id  = widget_label(mcol, value=' ', font=font)
  tr2id  = widget_label(mcol, value=' ', font=font)
               
  skip   = widget_label(mcol, value='  ', font=font)

  adid   = widget_label(mcol, value='Abs. Difference : ', font=font)
  errid  = widget_label(mcol, value='Error           : ', font=font)


                ;*****************
		;**** Buttons ****
                ;*****************
                
  base    = widget_base(parent,/row)
  doneid  = widget_button(base,value='Done',font=font,Uvalue='DONE')
  
  mlab   = widget_label(base, value='     ', font=font)
  plotid = widget_button(base, value='Plot',font=font,/align_center,Uvalue='PLOT')

  printid = widget_button(base,value='Print',font=font,Uvalue='PRINT')
  
  mlab   = widget_label(base, value='     ', font=font)
  

                ;**************************
		;**** Read data update ****
                ;**************************
  
  updateid = widget_label(base,value='                                      ',$
                          font=font,/align_right)
  

  
                ;**************************
		;**** initial settings ****
                ;**************************
  
  ; Only allow printing  when there is a plot on the screen.
  ; If the level/temperature/density is changed before 
  ; printing then de-sensitise the print button immediately.
  ; In this way what is printed is the same as on the screen.
                
  widget_control, printid, sensitive=0            
  widget_control, go_l_id, sensitive=0            
  widget_control, go_r_id, sensitive=0            
  
  
                ;**********************************************
                ;**** read in data - from unformatted file ****
                ;**********************************************

   ind_t   = lonarr(numlev,numden,/nozero)
   index_t = lonarr(numlev,numden,numstr,/nozero)
   err_t   = fltarr(numlev,numden,numstr,/nozero)
   adiff_t = fltarr(numlev,numden,numstr,/nozero)
   rt      = fltarr(numlev,numte,numden,numstr,/nozero)

   ind_d   = lonarr(numlev,numte,/nozero)
   index_d = lonarr(numlev,numte,numstr,/nozero)
   err_d   = fltarr(numlev,numte,numstr,/nozero)
   adiff_d = fltarr(numlev,numte,numstr,/nozero)
   rd      = fltarr(numlev,numte,numden,numstr,/nozero)
   


   openr, lun, dumpfile, /get_lun, /f77_unformatted

   readu, lun, ind_t
   readu, lun, index_t
   readu, lun, err_t
   readu, lun, adiff_t
   readu, lun, rt

   readu, lun, ind_d
   readu, lun, index_d
   readu, lun, err_d
   readu, lun, adiff_d
   readu, lun, rd
   
   free_lun, lun
  
 
   teplot = {  ind_t     :   ind_t,     $
               index_t   :   index_t,   $
               err_t     :   err_t,     $
               adiff_t   :   adiff_t,   $
               rt        :   rt         }
               
   neplot = {  ind_d     :   ind_d,     $
               index_d   :   index_d,   $
               err_d     :   err_d,     $
               adiff_d   :   adiff_d,   $
               rd        :   rd         }
               
               
                ;****************************************************
		;**** put all needed info into a state structure ****
                ;****************************************************
   
   state = {  devlist  :  devlist,   $
              devcode  :  devcode,   $   
              dumpfile :  dumpfile,  $
              numte    :  numte,     $
              numden   :  numden,    $
	      numlev   :  numlev,    $
	      numstr   :  numstr,    $
              choiceid :  choiceid,  $
              teid     :  teid,      $
              neid     :  neid,      $
	      levid    :  levid,     $
              tr1id    :  tr1id,     $
              tr2id    :  tr2id,     $
              errid    :  errid,     $
              adid     :  adid,      $
              plotid   :  plotid,    $
              printid  :  printid,   $
              go_l_id  :  go_l_id,   $
              go_r_id  :  go_r_id,   $
              te       :  te,        $
              den      :  den,       $
	      slev     :  slev,      $
	      iunit_t  :  iunit_t,   $
	      iunit_d  :  iunit_d,   $
	      drawid   :  drawid,    $
	      te_ind   :  0,         $
	      den_ind  :  0,         $
              lev_ind  :  0,         $
	      ichoice  :  0,         $
	      updateid :  updateid,  $
              last_tr  :  -1,        $
	      font     :  font,      $
              teplot   :  teplot,    $
              neplot   :  neplot     }
             
  
  
                ;****************************
		;**** realize the widget ****
                ;****************************

  ; must allow resizing in this one 
  dynlabel, parent
  widget_control,parent, /realize, set_uvalue=state, /no_copy


		;**** and make it modal ****
                
  xmanager,'adas216_inspct_exp',parent, $
            event_handler='adas216_inspct_exp_event',/modal,/just_reg
 

END
