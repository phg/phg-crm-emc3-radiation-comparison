; Copyright (c) 1999, Strathclyde University.
; SCCS info: Module @(#)$Header: /home/adascvs/idl/adas2xx/adas216/cw_adas216_error.pro,v 1.2 2004/07/06 12:35:59 whitefor Exp $ Date $Date: 2004/07/06 12:35:59 $
;+
; PROJECT:
;       ADAS 
;
; NAME:
;	CW_ADAS216_ERROR()
;
; PURPOSE:
;	Produces a widget for ADAS216 processing options/input.
;
; EXPLANATION:
;	This function creates a compound widget consisting of a text
;	widget holding an editable 'Run title', the dataset name/browse
;	widget cw_adas_dsbr, label widgets with charge information for
;	the input data, a table widget for temperature data cw_adas_table,
;	a second cw_adas_table widget for density data, buttons to
;	enter default values into the temperature and density tables,
;	a multiple selection widget cw_adas_sel, a reaction settings
;	widget cw_adas_reac2, a message widget, a 'Cancel' button and
;	finally a 'Done' button.
;
;	The compound widgets included in this widget are self managing.
;	This widget only manages events from the two 'Defaults' buttons,
;	the 'Cancel' button and the 'Done' button.
;
;	This widget only generates events for the 'Done' and 'Cancel'
;	buttons.
;
; USE:
;	This widget is specific to ADAS216, see adas216_error.pro
;	for use.
;
; INPUTS:
;	TOPPARENT- Long integer, ID of parent widget.
;
;	SZ	- String, recombined ion charge read.
;
;	SZ0	- String, nuclear charge read.
;
;	STRGA	- String array, level designations.
;
;	DSNINC	- String; The full system file name of the input COPASE
;		  dataset selected by the user for processing.
;
;	NDTEM	- Integer; Maximum number of temperatures allowed.
;
;	NDDEN	- Integer; Maximum number of densities allowed.
;
;	NDMET	- Integer; Maximum number of metastables allowed.
;
;	IL	- Integer; Number of energy levels.
;
;	NV	- Integer; Number of termperatures.
;
;	TSCEF	- dblarr(8,3); Input electron temperatures in three units.
;
;	The inputs SZ to TSCEF map exactly onto variables of the same
;	name in the ADAS216 FORTRAN program.
;
;       BITFILE - String; the path to the directory containing bitmaps
;                 for the 'escape to series menu' button.
;
;	INVAL	- Structure; the input screen structure - see cw_adas216_in.pro
;
;	SELEM	- String; selected element symbol
;
; OPTIONAL INPUTS:
;	None
;
; OUTPUTS:
;	The return value of this function is the ID of the declared widget.
;
; OPTIONAL OUTPUTS:
;	None
;
; KEYWORD PARAMETERS:
;	VALUE	- A structure which determines the initial settings of
;		  the processing options widget.
;
;		  The default VALUE is created thus;
;
;			imetr = make_array(ndmet,/int,value=0)
;			temp = dblarr(ndtem)
;			dens = dblarr(ndden)
;			ps = { $
;		 	TITLE:'',	NMET:0, $
;			IMETR:imetr,	IFOUT:1, $
;			MAXT:0,		TINE:temp, $
;			TINP:temp,	TINH:temp, $
;			IDOUT:1,	MAXD:0, $
;			DINE:dens,	DINP:dens, $
;			RATHA:dens,	RATPIA:dens, $
;			ZEFF:'',	LPSEL:0, $
;			LZSEL:0,	LISEL:0, $
;			LHSEL:0,	LRSEL:0, $
;                       LIOSEL:0,       LNSEL:0, $
;			LNORM:0,		 $
;			VAL502:struct502,	 $
;			WVLS:0.0,	WVLL:0.0,$
;			AVLT:0.0  }
;
; 		TITLE	entered general title for program run
; 		NMET	number of metastables
; 		(IMETR(I),I=1,NMET)	index of metastables
; 		IFOUT	input temperature units (1,2,3)
; 		MAXT	number of input temperatures (1-20)
; 		(TINE(I),I=1,MAXT)	electron temperatures
; 		(TINP(I),I=1,MAXT)	proton temperatures
; 		(TINH(I),I=1,MAXT)	neutral hydrogen temperatures
; 		IDOUT	input density units (1,2)
; 		MAXD	number of input densities
; 		(DINE(I),I=1,MAXD)	electron densities
; 		(DINP(I),I=1,MAXD)	proton densities
; 		(RATHA(I),I=1,MAXD)	ratio (neut h dens/elec dens)
; 		(RATPIA(I),I=1,MAXD)	ratio (n(z+1)/n(z) stage abund)
; 		ZEFF	plasma z effective, a string holding a valid number.
; 		LPSEL	include proton collisions?
; 		LZSEL	scale proton collisions with plasma z effective?
; 		LISEL	include ionisation rates?
; 		LHSEL	include charge transfer from neutral hydrogen?
; 		LRSEL	include free electron recombination?
;               LIOSEL to be determined
;               LNSEL   include projected bundle-n data?
;
;		All of the above structure elements map onto variables of
;		the same name in the ADAS216 FORTRAN program.
;
;
;	UVALUE	- A user value for the widget. Default 0.
;
;	FONT_LARGE - The name of a larger font.  Default current system
;		     font
;
;	FONT_SMALL - The name of a smaller font. Default current system
;		     font.
;
;	EDIT_FONTS - A structure of two fonts used in the table editor
;		     adas_edtab.pro, {font_norm:'',font_input:''}.  Defaults
;		     to current system font.
;
;	NUM_FORM   - String; Numeric format to use in tables.  Default
;			'(E10.3)'
;		     
;
; CALLS:
;	POPUP		 Popup warning window with buttons.
;	XXDEFT		 Returns default temperature table.
;	XXDEFD		 Returns default density table.
;	CW_ADAS_DSBR	 Dataset name and comments browsing button.
;	CW_ADAS_TABLE	 Adas data table widget.
;	CW_ADAS_SEL	 Adas mulitple selection widget.
;	CW_ADAS208_REAC2 Reaction settings for ADAS216.
;
;	See side effects for other related widget management routines.
;
; SIDE EFFECTS:
;
;	The following widget management routines are included in this
;	file;
;	ERROR216_GET_VAL()	Returns the current VALUE structure.
;	ERROR216_EVENT()		Process and issue events.
;
; CATEGORY:
;	Compound Widget
;
; WRITTEN:
;       Martin O'Mullane
;
; MODIFIED:
;	1.1	Martin O'Mullane
;		First release 
;	1.2	Richard Martin
;		Changed widget base name from switch to switchb for IDL 5.4
;		compatibility.
;
; VERSION:
;     1.1	17-03-99
;	1.2	10-11-00
;
;-
;-----------------------------------------------------------------------------

FUNCTION error216_get_val, id


                ;**** Return to caller on error ****
  ON_ERROR, 2

		 ;***************************************
                 ;****     Retrieve the state 	     ****
		 ;**** Get first_child widget id     ****
		 ;**** because state is stored there ****
		 ;***************************************

  first_child = widget_info(id, /child)
  widget_control, first_child, get_uvalue = state,/no_copy


		;***********************************
		;**** Get run title from widget ****
		;***********************************
                
  widget_control,state.runid,get_value=title


                ;************************
                ;**** Get Error Type ****
                ;************************

  widget_control, state.typeid, get_value=bpress   ; returns index of button
  errtype = bpress 

                ;**************************
                ;**** Get Error Values ****
                ;**************************

  widget_control, state.excid, get_value=tmp  
  state.errval.numexc = tmp.num_typ 
  state.errval.defexc = tmp.def_typ 
  state.errval.genexc = tmp.gen_typ 
  state.errval.spfexc = tmp.spf_typ 
  print,'exc',tmp.def_typ

  widget_control, state.recid, get_value=tmp    
  state.errval.numrec = tmp.num_typ 
  state.errval.defrec = tmp.def_typ 
  state.errval.genrec = tmp.gen_typ 
  state.errval.spfrec = tmp.spf_typ 
  print,'rec',tmp.def_typ

  widget_control, state.cxrid, get_value=tmp    
  state.errval.numcxr = tmp.num_typ 
  state.errval.defcxr = tmp.def_typ 
  state.errval.gencxr = tmp.gen_typ 
  state.errval.spfcxr = tmp.spf_typ 

  widget_control, state.ionid, get_value=tmp    
  state.errval.numion = tmp.num_typ 
  state.errval.defion = tmp.def_typ 
  state.errval.genion = tmp.gen_typ 
  state.errval.spfion = tmp.spf_typ 

    
		;***********************************************
		;**** write selected values to PS structure ****
		;***********************************************

  ps = {error216_set,		      $ 			
	    title   :  title(0),      $	
            errtype :  errtype,       $
            errval  :  state.errval   }
            

    widget_control, first_child, set_uvalue=state, /no_copy

  RETURN, ps

END

;-----------------------------------------------------------------------------

FUNCTION error216_event, event


                ;**** Base ID of compound widget ****
                
  parent=event.handler

		;**********************************************
                ;**** Retrieve the user value state        ****
		;**** Get id of first_child widget because ****
		;**** user value "state" is stored there   ****
		;**********************************************

  first_child = widget_info(parent, /child)
  widget_control, first_child, get_uvalue=state,/no_copy

		;*********************************
		;**** Clear previous messages ****
		;*********************************
                
  widget_control,state.messageid,set_value=' '

                ;************************
                ;**** Process events ****
                ;************************
                
                
  CASE event.id OF

    state.typeid: begin
       widget_control,state.typeid,get_value=bpress

; temp is title,errtype,errval : do we need to set values????
;       widget_control, first_child, set_uvalue=state, /no_copy
;       widget_control, event.handler,get_value=temp
;       widget_control, first_child, get_uvalue=state, /no_copy


       case bpress of
       
         0 : begin
               widget_control, state.exc_base, map = 1 
               widget_control, state.cxr_base, map = 0
               widget_control, state.ion_base, map = 0
               widget_control, state.rec_base, map = 0
             end 

         1 : begin
               widget_control, state.exc_base, map = 0 
               widget_control, state.rec_base, map = 1
               widget_control, state.cxr_base, map = 0
               widget_control, state.ion_base, map = 0
             end 

         2 : begin
               widget_control, state.exc_base, map = 0 
               widget_control, state.cxr_base, map = 0
               widget_control, state.ion_base, map = 1
               widget_control, state.rec_base, map = 0
             end 

         3 : begin
               widget_control, state.exc_base, map = 0
               widget_control, state.rec_base, map = 0
               widget_control, state.cxr_base, map = 0
               widget_control, state.ion_base, map = 1
             end 
             
       endcase

    end
    

		;***********************
		;**** Cancel button ****
		;***********************
                
    state.cancelid: new_event = {ID:parent, TOP:event.top, $
				HANDLER:0L, ACTION:'Cancel'}


		;*********************
		;**** Done button ****
		;*********************
                
    state.doneid: begin
    
		;***************************************
		;**** Check all user input is legal ****
		;***************************************
                
	error = 0
                ;*************************************************
                ;*** Have to restore state before call to     ****
                ;*** proc216_get_val so it can be used there. ****
                ;*************************************************
                
;        widget_control, first_child, set_uvalue=state, /no_copy
;	
;        widget_control,event.handler,get_value=abc
; 
;                ;*** reset state variable ***
;
;        widget_control, first_child, get_uvalue=state, /no_copy
            
            
		;******************************************
		;**** now we can begin to check input. ****
		;******************************************


		;**** return value or flag error ****
                
	if error eq 0 then begin
	  new_event = {ID:parent, TOP:event.top, HANDLER:0L, ACTION:'Done'}
        end else begin
	  widget_control,state.messageid,set_value=message
	  new_event = 0L
        end

      end
                ;**** Menu button ****
                
    state.outid: begin
      new_event = {ID:parent, TOP:event.top, HANDLER:0L, ACTION:'Menu'}
    end




    ELSE: new_event = 0L

  ENDCASE
  
		;*********************************************
		;*** make "state" available to other files ***
		;*********************************************
    widget_control, first_child, set_uvalue=state, /no_copy

  RETURN, new_event
  
END

;-----------------------------------------------------------------------------

FUNCTION cw_adas216_error, topparent, 					$
		sz, sz0, selem, il, strga, ndgen, ndspf,		$
                dsninc,  bitfile,  					$
		VALUE=value, UVALUE=uvalue, 				$
		FONT_LARGE=font_large, FONT_SMALL=font_small, 		$
		EDIT_FONTS=edit_fonts, NUM_FORM=num_form

		;**** Set defaults for keywords ****
                
  IF NOT (KEYWORD_SET(uvalue)) THEN uvalue = 0
  IF NOT (KEYWORD_SET(font_large)) THEN font_large = ''
  IF NOT (KEYWORD_SET(font_small)) THEN font_small = ''
  IF NOT (KEYWORD_SET(edit_fonts)) THEN $
				edit_fonts = {font_norm:'',font_input:''}
  IF NOT (KEYWORD_SET(num_form)) THEN num_form = '(E10.3)'

  IF NOT (KEYWORD_SET(value)) THEN begin
	message,'A structure must be passed'
  END ELSE BEGIN
	ps = {error216_set,	                          $
		        title     :       value.title,    $
                        errtype   :       0,  		  $
                        errval    :       value.errval    }
  END


                ;*********************************************************
                ;**** Modify certain parameters and results depending ****
                ;**** on the machine being used                       ****
                ;*********************************************************

    machine = GETENV('TARGET_MACHINE')
    if machine eq 'HPUX' then begin
        y_size = 4
        large_font = font_small
    endif else begin
        y_size = 6
        large_font = font_large
    endelse




		;*********************************************************
		;**** Create the 216 Error entry options/input window ****
		;*********************************************************

		;**** create titled base widget ****
                
  parent = widget_base(topparent, UVALUE = uvalue, 		$
		       EVENT_FUNC     = "error216_event", 	$
		       FUNC_GET_VALUE = "error216_get_val", 	$
		       /COLUMN)

		;******************************************************
		;**** Create a dummy widget just to hold value of *****
		;**** "state" variable so as not to get confused  *****
		;**** with any other values. Adopt IDL practice   *****
		;**** of using first child widget                 *****
		;******************************************************

  first_child = widget_base(parent)
  topbase = widget_base(first_child,/column)


		;***********************
		;**** add run title ****
		;***********************
                
  base  = widget_base(topbase,/row)
  rc    = widget_label(base,value='Title for Run',font=font_large)
  runid = widget_text(base,value=ps.title,xsize=40,font=font_large,/edit)

		;**** add dataset name and browse button ****
                
  rc = cw_adas_dsbr(topbase,dsninc,font=font_large,/row)


		;**** add charge information ****
                
  skip = widget_label(topbase,font=font_large, value=' ')

  rc = widget_label(topbase,font=font_large, /align_left,$
			value='Nuclear Charge:'+sz0+'  Ion Charge:'+sz)


		;*********************************
		;**** Error entry information ****
		;*********************************
                
  base   = widget_base(topbase,/row,/frame)
  
  base1  = widget_base(base,/column)
  
 		;**** Type buttons ****
  
  base2  = widget_base(base1,/row)
  
  query_names = ['Excitation','Recombination','Charge Exchange','Ionisation']
  
  typeid = cw_bgroup(base2,query_names,exclusive=1,column=2,    $
                    label_left='Error Type:',/no_release,font=font_large)
                    
  skip  = widget_label(base2,font=font_large, value=' ')
  
  
 		;**** bases for the possible types ****
  
  switchb   = widget_base(base1)
  exc_base = widget_base(switchb, /row)
  rec_base = widget_base(switchb, /row)
  cxr_base = widget_base(switchb, /row)
  ion_base = widget_base(switchb, /row)
  

 		;**** excitation ****
  
  
  entval = { dogone , 			      $
             err_type :  0, 		      $
             num_typ  :  ps.errval.numexc,    $
             def_typ  :  ps.errval.defexc,    $
             gen_typ  :  ps.errval.genexc,    $
             spf_typ  :  ps.errval.spfexc     }
             
  excid = cw_adas216_entry(exc_base, sz, sz0, selem, il, strga,		 $ 
  			    ndgen,ndspf,   				 $ 
                            VALUE=entval, 				 $
                            FONT_LARGE=font_large, FONT_SMALL=font_small,$
                            EDIT_FONTS=edit_fonts,uvalue='ERR_ENTRY')
 
 		;**** recombination ****
  
  
  entval = { dogoneit , 		      $
             err_type :  1, 		      $
             num_typ  :  ps.errval.numrec,    $
             def_typ  :  ps.errval.defrec,    $
             gen_typ  :  ps.errval.genrec,    $
             spf_typ  :  ps.errval.spfrec     }
             
   recid = cw_adas216_entry(rec_base, sz, sz0, selem, il, strga,          $ 
  			    ndgen,ndspf,   				  $ 
                            VALUE=entval, 				  $
                            FONT_LARGE=font_large, FONT_SMALL=font_small, $
                            EDIT_FONTS=edit_fonts,uvalue='ERR_ENTRY')


 		;**** charge exchange ****
  
  
  entval = { dogooone , 		      $
             err_type :  2, 		      $
             num_typ  :  ps.errval.numcxr,    $
             def_typ  :  ps.errval.defcxr,    $
             gen_typ  :  ps.errval.gencxr,    $
             spf_typ  :  ps.errval.spfcxr     }
             
   cxrid = cw_adas216_entry(cxr_base, sz, sz0, selem, il, strga,          $ 
  			    ndgen,ndspf,   				  $ 
                            VALUE=entval, 				  $
                            FONT_LARGE=font_large, FONT_SMALL=font_small, $
                            EDIT_FONTS=edit_fonts,uvalue='ERR_ENTRY')


		;**** ionisation ****
  
  
  entval = { dogoooneit , 		      $
             err_type :  3, 		      $
             num_typ  :  ps.errval.numion,    $
             def_typ  :  ps.errval.defion,    $
             gen_typ  :  ps.errval.genion,    $
             spf_typ  :  ps.errval.spfion     }
             
   ionid = cw_adas216_entry(ion_base, sz, sz0, selem, il, strga,          $ 
  			    ndgen,ndspf,   				  $ 
                            VALUE=entval, 				  $
                            FONT_LARGE=font_large, FONT_SMALL=font_small, $
                            EDIT_FONTS=edit_fonts,uvalue='ERR_ENTRY')


 
		;***********************
		;**** Error message ****
		;***********************
                
  messageid = widget_label(parent,font=font_large,  $
	value='Edit the error information and press Done to proceed')



		;**********************************
		;**** Finally the exit buttons ****
		;**********************************
                
  base     = widget_base(parent,/row)
  menufile = bitfile + '/menu.bmp'
  read_X11_bitmap, menufile, bitmap1
  outid    = widget_button(base, value = bitmap1)
  cancelid = widget_button(base,value='Cancel',font=font_large)
  doneid   = widget_button(base,value='Done',font=font_large)
  
  
; set buttons and map error entry options depending on error type
                 
        widget_control, typeid, set_value = ps.errtype
        case ps.errtype of
          0 : begin
                widget_control, exc_base, map = 1
                widget_control, rec_base, map = 0
                widget_control, cxr_base, map = 0
                widget_control, ion_base, map = 0
              end
          1 : begin
                widget_control, exc_base, map = 0
                widget_control, rec_base, map = 1
                widget_control, cxr_base, map = 0
                widget_control, ion_base, map = 0
              end
          2 : begin
                widget_control, exc_base, map = 0
                widget_control, rec_base, map = 0
                widget_control, cxr_base, map = 1
                widget_control, ion_base, map = 0
              end
          3 : begin
                widget_control, exc_base, map = 0
                widget_control, rec_base, map = 0
                widget_control, cxr_base, map = 0
                widget_control, ion_base, map = 1
              end
         endcase
  
  
		;*************************************************
		;**** create a state structure for the pop-up ****
		;**** window.                                 ****
		;*************************************************
                
  new_state = { sz              :       sz,             $
                selem           :       selem,          $
                runid           :       runid,          $
                outid           :       outid,          $
                cancelid        :       cancelid,       $
                doneid          :       doneid,         $
	        messageid       :       messageid,      $
	        typeid          :       typeid,         $
	        excid           :       excid,          $
	        recid           :       recid,          $
	        cxrid           :       cxrid,          $
	        ionid           :       ionid,          $
                font            :       font_large,     $
	        exc_base        :       exc_base,       $
	        rec_base        :       rec_base,       $
	        cxr_base        :       cxr_base,       $
	        ion_base        :       ion_base,       $
                errtype         :       ps.errtype,   	$
                errval          :       ps.errval    	}

                ;**** Save initial state structure ****

    widget_control, first_child, set_uvalue=new_state, /no_copy

  RETURN, parent

END

