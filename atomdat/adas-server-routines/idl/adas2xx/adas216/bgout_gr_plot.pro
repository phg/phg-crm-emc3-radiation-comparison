; Copyright (c) 1999, Strathclyde University.
; SCCS info: Module @(#)$Header: /home/adascvs/idl/adas2xx/adas216/bgout_gr_plot.pro,v 1.1 2004/07/06 11:40:37 whitefor Exp $ Date $Date: 2004/07/06 11:40:37 $
;+
; PROJECT:
;       ADAS 
;
; NAME:
;	BGOUT_GR_PLOT	
;
; PURPOSE:
;	Plots to screen (or a file) the inspected data from bgout_gr.
;       Errors are caught here and reported back to allow for graceful recovery.
;
; EXPLANATION:
;
; INPUTS:
;       VALUE - Structure holding the data and other information for plotting.
;       PRINT - Structure holding information for printing to a file.
;
; OPTIONAL INPUTS:
;	None.
;
; OUTPUTS:
;
; OPTIONAL OUTPUTS:
;       None
;
; KEYWORD PARAMETERS:
;
; CALLS:
;
; SIDE EFFECTS:
;
; CATEGORY:
;
;
; WRITTEN:
;       Martin O'Mullane
;
; MODIFIED:
;	1.1	Martin O'Mullane
;		First release 
;
; VERSION:
;       1.1	17-03-99
;
;-
;-----------------------------------------------------------------------------

PRO bgout_gr_plot, errid, stats, VALUE=value, FONT=font, PRINT=print

   IF NOT (KEYWORD_SET(font))  THEN font = ''
   IF NOT (KEYWORD_SET(value)) THEN $  
           message,'VALUE structure must be passed to BGOUT_GR_PLOT'

   if (keyword_set(print)) then begin
     
     grtype = print.devcode( where(print.dev eq print.devlist) )
     set_plot,grtype
     
     if print.paper eq 'A4' then begin
        xsize = 15.0
        ysize = 16.0
     endif else begin
        xsize = 15.0
        ysize = 16.0
     endelse
     
     ; unlike 2D plots we need to use Hershy fonts.
     device,/color,bits=8,filename=print.dsn,  	   $
             font_size=12, /portrait,              $
             xsize=xsize, ysize=ysize
   endif 

   ; Trap any errors from the fitting and report back to the user.
   ; Hopefully it will not crash the program as we jump to the end
   ; in the case of an error.

   catch, error_status
   
   if error_status ne 0 then begin
      print,'An error occurred in BGOUT_GR_PLOT'
      print, !err_string 
      widget_control, errid, set_value = 'Possible mathematical error'
      goto,LABELend
   endif
   
   
   ; get data from value structure
   
   
   te       = value.te
   den      = value.den
   s_l      = value.s_l
   s_u      = value.s_u
   
   title    = value.title
   subtitle = value.subtitle
   
   iunit_t  = value.iunit_t
   iunit_d  = value.iunit_d
   
   ax       = value.ax
   az       = value.az
   colour   = value.colour
   style    = value.style
   ichoice  = value.ichoice
   
   
   numte  = n_elements(te)
   numden = n_elements(den)
    
   ; Leave space for a title and footer
  
   !y.omargin=[1,1]



   ; what are we plotting and global options for the graph

   name_t  = ['K','eV','Red.']
   name_d  = ['cm!U-3!N','Red.']

   xtitle = 'T!De!N (' + name_t(iunit_t-1) + ')'
   xmin   = te(0)
   xmax   = te(numte-1)

   ytitle = 'n!De!N (' + name_d(iunit_d-1) + ')'
   ymin   = den(0)
   ymax   = den(numden-1)
 
   if ( xmax/xmin ge 1e2) then xlog=1
   if ( ymax/ymin ge 1e2) then ylog=1
   
   
   
   ; now do the plotting
   
   if ichoice eq 0 then sp=s_l else sp=s_u
 
   
   
   if style eq 0 then begin
   
      surface, sp, te, den, xrange=[xmin,xmax], yrange=[ymin,ymax], $
                            xlog=xlog, ylog=ylog,                   $
                            xtitle=xtitle, ytitle=ytitle,           $
                            ax=ax, az=az,		            $
                            charsize=1.1,                           $
                            min_value=0.0, max_value=2.0
                         
   endif else begin
   
      shade_surf, sp, te, den, xrange=[xmin,xmax], yrange=[ymin,ymax], $
                               xlog=xlog, ylog=ylog,                   $
                               xtitle=xtitle, ytitle=ytitle,           $
                               ax=ax, az=az,			       $
                               charsize=1.1,                           $ 
                               min_value=0.0, max_value=2.0
   
   
   endelse
   
   
   ; display 'useful' information
   
   xyouts, 0.5, 0.95, align=0.5, title, /normal, charsize=1.1
   xyouts, 0.5, 0.91, align=0.5, subtitle, /normal, charsize=0.9
   
   date = xxdate()
   xyouts, 0.9, 0.02, align=0.5, /normal, 'ADAS216   ' + date(0), charsize=0.6

  
LABELend:


   ; reset graphical parameters
   
   !p.multi = [0,1,1,0,0]               
   !y.omargin=[0, 0]


   if (keyword_set(print)) then begin
     device,/close
     set_plot,'X'
   endif

END
