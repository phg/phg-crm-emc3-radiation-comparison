; Copyright (c) 1999, Strathclyde University.
; SCCS info: Module @(#)$Header: /home/adascvs/idl/adas2xx/adas216/bgout_cum_calc.pro,v 1.1 2004/07/06 11:40:30 whitefor Exp $ Date $Date: 2004/07/06 11:40:30 $
;+
; PROJECT:
;       ADAS 
;
; NAME:
;	BGOUT_CUM_CALC
;
; PURPOSE:
;	Calculated 3D error surface from cumulative error analysis.
;
; EXPLANATION:
;	Later!
;     
;
; INPUTS:
;       VALUE - Structure holding necessary information.
;
;               dumpfile : file where histograms are stored
;               numte    : number of temperatures  
;               numden   : number of densities 
;               numlev   : number of levels
;               numbin   : number of bins for histograms
;               xhist    : x values of histogram
;
; OPTIONAL INPUTS:
;	None.
;
; OUTPUTS:
;       
;
; OPTIONAL OUTPUTS:
;       None
;
; KEYWORD PARAMETERS:
;
; CALLS:
;
; SIDE EFFECTS:
;	File output
;
;
; CATEGORY:
;	ADAS system
;
; WRITTEN:
;       Martin O'Mullane
;
; MODIFIED:
;	1.1	Martin O'Mullane
;		First release 
;
; VERSION:
;       1.1	17-03-99
;
;-
;-----------------------------------------------------------------------------


PRO bgout_cum_calc,  mean_u, sd_u, VALUE=value, FONT=font

  IF NOT (KEYWORD_SET(value)) THEN $
	message,'BGOUT_CUM_CALC : A structure must be passed'
  IF NOT (KEYWORD_SET(font)) THEN font = ''

  idl_ver = strmid(!version.release,0,1)
  old_except = !except
  !except = 0
  
  catch, error_status

  if error_status ne 0 then begin
     print,'Possible mathematical in BGOUT_CUM_CALC'
     print, !err_string 
  endif

                ;**************************************
		;**** Create an information widget ****
                ;**************************************

  widget_control, /hourglass
  base  = widget_base(/column, xoffset=300, yoffset=200,           $
                      title = "ADAS216: INFORMATION")
  lab0  = widget_label(base, value='')
  lab1  = widget_label(base,  font=font,                                         $
            value='   ADAS216 Calculating surface - Please Wait   ')

 ; As size of the bar is hardwired, centre it in its own base

  barbase = widget_base(base,/align_center)

  grap  = widget_draw(barbase, ysize=20, xsize=480) 

  ubase = widget_base(base,/row,/align_center)
  lab1  = widget_label(ubase, value = 'PROCESSING ')
  lab2  = widget_label(ubase, value = '  0')
  lab3  = widget_label(ubase, value = '% COMPLETED')



  widget_control, base, /realize



                ;********************
                ;**** Set values ****
                ;********************

  dumpfile = value.dumpfile
  
  numte    = value.numte
  numden   = value.numden
  numlev   = value.numlev
  numbin   = value.numbin
  
  x        = value.xhist
    
  
  
                ;**********************************************
                ;**** read in data - from unformatted file ****
                ;**********************************************
   
   popun = fltarr(numlev,numte,numden,/nozero)
   ihist = intarr(numlev,numte,numden,numbin,/nozero)

   openr, lun, dumpfile, /get_lun, /f77_unformatted

   readu, lun, popun
   readu, lun, ihist
   
   free_lun, lun
  
  
   
   if idl_ver eq 4 then aa=fltarr(4)
  
                ;*******************************
                ;**** cycle over Te/ne grid ****
                ;*******************************
                
  
  ; The time scales linearly in this  calculation. Update 
  ; the information widget in stages.                  
   
  ; Is there a more efficient way of traversing these for loops??? 
   
   numstep = (numlev-1)*numte*numden
                
   step = 1000.0/numstep
   
   i = 0
   for kd = 0, numden-1 do begin
     for kt = 0, numte-1 do begin
       for kl = 1, numlev-1 do begin

         p = float(i)/numstep*1000
         q = p+step

         for j=fix(p),fix(q) do begin
             xpb = (float(j)+1)/1000.0
             plots, [xpb, xpb],[0.0,1.0], /normal, color=1
         endfor
 
         i1 = fix(q/10)
         if (i1 mod 10) eq 0 then $
            widget_control, lab2, set_value=string(i1,format='(I3)')

         i = i+1

         y = ihist(kl,kt,kd,*)
         y = reform(y)

	   if idl_ver eq 4 then begin
	   	 getfit, x,y,aa
	   endif else begin
	   	yfit = gaussfit(x,y,aa,nterms=4)     
	   endelse 
	   
         mean_u(kl,kt,kd) = aa(1)
         sd_u(kl,kt,kd)   = aa(2)

       endfor
     endfor
   endfor
      

 
LABELend:

  !except = old_except 
    
		;**** Destroy the information widget ****

   widget_control, base, /destroy


END
