; Copyright (c) 1999, Strathclyde University.
; SCCS info: Module @(#)$Header: /home/adascvs/idl/adas2xx/adas216/cw_adas216_out.pro,v 1.1 2004/07/06 12:36:15 whitefor Exp $ Date $Date: 2004/07/06 12:36:15 $
;+
; PROJECT:
;       ADAS 
;
; NAME:
;	CW_ADAS216_OUT()
;
; PURPOSE:
;	Produces a widget for ADAS216 output options.
;
; EXPLANATION:
;	This function declares a compound widget consisting of the
;	graphical output selection widget cw_adas_gr_sel.pro, and an 
;	output file widget cw_adas_outfile.pro.  The text output
;	file specified in this widget is for tabular (paper.txt)
;	output.  This widget also includes a button for browsing the comments
;       from the input dataset, a 'Cancel' button , a 'Done' button
;       and an 'Escape to series menu' button. This latter is represented by
;       a bitmapped button.
;	The compound widgets cw_adas_gr_sel.pro and cw_adas_outfile.pro
;	included in this file are self managing.  This widget only
;       handles events from the 'Done', 'Cancel' and 'Escape to series
;       menu' buttons.
;
;
; USE:
;	This widget is specific to ADAS216, see adas216_out.pro	for use.
;
; INPUTS:
;	PARENT	- Long integer; ID of parent widget.
;
;	DSFULL	- Name of input dataset for this application.
;
;       BITFILE - String; the path to the dirctory containing bitmaps
;                 for the 'escape to series menu' button.
;
; OPTIONAL INPUTS:
;	None.
;
; OUTPUTS:
;	The return value of this function is the ID of the declared widget.
;
; OPTIONAL OUTPUTS:
;	None.
;
; KEYWORD PARAMETERS:
;
;	VALUE	- A structure which determines the initial settings of
;		  the output options widget.  The structure is made
;		  up of two parts.  Each part is the same as the value
;		  structure of one of the two main compound widgets
;		  included in this widget.  See cw_adas_outfile for 
;                 more details.  The default value is;
;
;		      { GRPOUT:0, gtitle:'', 				$
;			TEXOUT:0, TEXAPP:-1, 				$
;			TEXREP:0, TEXDSN:'', 				$
;			TEXDEF:'',TEXMES:'', 				$
;		      }
;
;			GRPOUT	Integer; Activation button 1 on, 0 off
;			GTITLE  String; Graph title
;			TEXOUT	Integer; Activation button 1 on, 0 off
;			TEXAPP	Integer; Append button 1 on, 0 off, -1 no button
;			TEXREP	Integer; Replace button 1 on, 0 off, -1 no button
;			TEXDSN	String; Output file name
;			TEXDEF	String; Default file name
;			TEXMES	String; file name error message
;
;
;	UVALUE	- A user value for this widget.
;
;	FONT	- String; the name of a font to be used for all text in
;		  this widget.  Default to system default font.
;
; CALLS:
;	CW_ADAS_DSBR	Input dataset name and comments browse button.
;	CW_ADAS_OUTFILE	Output file name entry widget.
;	See side effects for other related widget management routines.
;
; SIDE EFFECTS:
;
; 	Two other routines are included in this file
;	OUT216_GET_VAL()
;	OUT216_EVENT()
;
; CATEGORY:
;	Compound Widget
;
; WRITTEN:
;       Martin O'Mullane
;
; MODIFIED:
;	1.1	Martin O'Mullane
;		First release 
;
; VERSION:
;       1.1	17-03-99
;
;-
;-----------------------------------------------------------------------------

FUNCTION out216_get_val, id

                ;**** Return to caller on error ****

    ON_ERROR, 2

                ;**** Retrieve the state ****

    parent = widget_info(id, /parent)
    widget_control, parent, get_uvalue=state, /no_copy

		;**** Get graphical output settings ****

    widget_control, state.grtitid, get_value=title

		;**** Get text output settings ****

    widget_control, state.paperid, get_value=papos
    
    
    os = { out216_set, 				       $
	   GRPOUT	:	state.grsel, 	       $
	   GTITLE	:	title(0),              $
	   TEXOUT	:	papos.outbut,  	       $
	   TEXAPP	:	papos.appbut, 	       $
	   TEXREP	:	papos.repbut,  	       $
	   TEXDSN	:	papos.filename,        $
	   TEXDEF	:	papos.defname, 	       $
	   TEXMES	:	papos.message 	       }

                ;**** Return the state ****

    widget_control, parent, set_uvalue=state, /no_copy

    RETURN, os 

END

;-----------------------------------------------------------------------------

FUNCTION out216_event, event

                ;**** Base ID of compound widget ****

    parent = event.top

                ;**** Retrieve the state ****

    widget_control, parent, get_uvalue=state, /no_copy

		;*********************************
		;**** Clear previous messages ****
		;*********************************

    widget_control, state.messid, set_value=' '

                ;************************
                ;**** Process events ****
                ;************************

    CASE event.id OF

		;***********************
		;**** Cancel button ****
		;***********************

        state.cancelid: new_event = {ID:parent, TOP:event.top, 		$
				     HANDLER:0L, ACTION:'Cancel'}
                                     
		;**********************************
		;**** Graphic selection button ****
		;**********************************

        state.grselid: begin
        
              if state.grsel eq 0 then state.grsel = 1 else state.grsel = 0
              
        end 

		;*********************
		;**** Done button ****
		;*********************

        state.doneid: begin

                ;****************************************************
                ;**** Return the state before checking can start ****
		;**** with the get_value keyword.                ****
                ;****************************************************

            widget_control, parent, set_uvalue=state, /no_copy

		;***************************************
		;**** Check for errors in the input ****
		;***************************************

	    error = 0

		;**** Get a copy of the widget value ****

	    widget_control, event.handler, get_value=os
	
		;**** Check for widget error messages ****

 	    if os.texout eq 1 and strtrim(os.texmes) ne '' then error=1

                ;**** Retrieve the state   ****

            widget_control, parent, get_uvalue=state, /no_copy
	    if error eq 1 then begin
	        widget_control, state.messid, set_value= 		$
		    '**** Error in output settings ****'
	        new_event = 0L
	    endif else begin
	        new_event = {ID:parent, TOP:event.top, HANDLER:0L, 	$
                             ACTION:'Done'}
	    endelse
        end

                ;**** Menu button ****

        state.outid: begin
            new_event = {ID:parent, TOP:event.top, HANDLER:0L,          $
                         ACTION:'Menu'}
        end

        ELSE: new_event = 0L

    ENDCASE

                ;**** Return the state   ****

    widget_control, parent, set_uvalue=state, /no_copy

    RETURN, new_event

END

;-----------------------------------------------------------------------------

FUNCTION cw_adas216_out, parent, dsfull, bitfile,         	$
		         VALUE=value, UVALUE=uvalue, FONT=font

    IF (N_PARAMS() EQ 0) THEN MESSAGE, 'Must specify parent for CW_ADAS216_OUT'
    ON_ERROR, 2					

		;**** Set defaults for keywords ****

    IF NOT (KEYWORD_SET(uvalue)) THEN uvalue = 0
    IF NOT (KEYWORD_SET(font))   THEN font = ''
    IF NOT (KEYWORD_SET(value))  THEN begin
	message,'A structure must be passed to CW_ADAS216_OUT'
    ENDIF ELSE begin
	os = {  out216_set,                                             $
	        grpout          :       value.grpout,                   $
	        gtitle          :       value.gtitle,                   $
	        texout          :       value.texout,                   $
	        texapp          :       value.texapp,                   $
	        texrep          :       value.texrep,                   $
	        texdsn          :       value.texdsn,                   $
	        texdef          :       value.texdef,                   $
	        texmes          :       value.texmes                    }
    ENDELSE


		;**********************************************
		;**** Create the 216 Output options widget ****
		;**********************************************

		;**** create base widget ****

    cwid = widget_base( parent, UVALUE = uvalue, 			$
			EVENT_FUNC = "out216_event", 			$
			FUNC_GET_VALUE = "out216_get_val", 		$
			/COLUMN)

		;**** Add dataset name and browse button ****

    rc = cw_adas_dsbr(cwid, dsfull, font=font, /row)


		;**** Graphical output selection ****

   
    base   = widget_base(cwid, /column, /frame)
    sbase  = widget_base(base, /row)
    sbbase = widget_base(sbase, /nonexclusive)
    
    grselid  = widget_button(sbbase, value='', font=font)
    grsellab = widget_label(sbase,value='Graphical Output', font=font)
    
    tbase    = widget_base(base, /row)
    grtitlab = widget_label(tbase,value='Graph Title ',font=font)
    grtitid  = widget_text(tbase, xsize=40,/editable, value=os.gtitle, font=font)
    



		;**** Widget for text output ****


    outfval = { OUTBUT    :  os.texout,     $
                APPBUT    :  os.texapp,     $
                REPBUT    :  os.texrep,     $
	        FILENAME  :  os.texdsn,     $
                DEFNAME   :  os.texdef,     $ 
                MESSAGE   :  os.texmes      }
                
    base = widget_base(cwid, /row, /frame)
    paperid = cw_adas_outfile(base, OUTPUT='Text Output', $
                              VALUE=outfval, FONT=font)


		;**** Error message ****

    messid = widget_label(cwid, value='Display/print 3D overview of errors', $
                          font=font)



		;**** add the exit buttons ****

    base = widget_base(cwid, /row)
    menufile = bitfile + '/menu.bmp'
    read_X11_bitmap, menufile, bitmap1
    outid = widget_button(base, value=bitmap1)          
    cancelid = widget_button(base, value='Cancel', font=font)
    doneid = widget_button(base, value='Done', font=font)
  
  
                ;*************************************************
		;**** create a state structure for the pop-up ****
		;**** window.                                 ****
                ;*************************************************

    new_state = {       grselid         :       grselid,                $
		        grtitid         :       grtitid,                $
		        grsel           :       0,                      $
		        paperid         :       paperid,                $
		        cancelid        :       cancelid,               $
		        doneid          :       doneid,                 $
                        outid           :       outid,                  $
		        messid          :       messid                  }


                ;**** Save initial state structure ****

    widget_control, parent, set_uvalue=new_state, /no_copy

    RETURN, cwid

END

