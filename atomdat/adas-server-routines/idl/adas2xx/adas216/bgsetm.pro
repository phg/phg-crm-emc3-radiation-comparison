; Copyright (c) 1999, Strathclyde University.
; SCCS info: Module @(#)$Header: /home/adascvs/idl/adas2xx/adas216/bgsetm.pro,v 1.1 2004/07/06 11:41:00 whitefor Exp $ Date $Date: 2004/07/06 11:41:00 $
;+
; PROJECT:
;       ADAS 
;
; NAME:
;	BGSETM
;
; PURPOSE:
;	IDL communications with ADAS216 FORTRAN process via pipe.
;
; EXPLANATION:
;	This IDL routine reads data from the ADAS216 FORTRAN subroutine
;	BGSETM via a UNIX pipe. 
;
; USE:
;	The use of this routine is specific to ADAS216.  See
;	adas216.pro for an example.
;
; INPUTS:
;	PIPE	- The IDL unit number of the bi-directional pipe to the
;		  ADAS216 FORTRAN process.
;
; OPTIONAL INPUTS:
;	None
;
; OUTPUTS:
;	SZ0	- String, nuclear charge read.
;
;	SZ	- String, recombined ion charge read.
;
;	SCNTE	- Number of electron impact transitions.
;
;	SIL	- Number of energy levels.
;
;	NDSTR	- Number of level strings. i.e dimension of strga
;
;	STRGA	- String array, level designations.
;
;	NDMET	- Number of metastables
;
;	NPL	- Number of metastables in input file (see adas216.for)
;
;	STRGMI	- String of LSJ info for ordinary levels. 
;
;	STRGMF	- String of LSJ info for metastable levels.
;
; OPTIONAL OUTPUTS:
;	None
;
; KEYWORD PARAMETERS:
;	None
;
; CALLS:
;	None
;
; SIDE EFFECTS:
;	Reads data from a pipe.
;
; CATEGORY:
;	Adas system.
;
;
; WRITTEN:
;       Martin O'Mullane
;
; MODIFIED:
;	1.1	Martin O'Mullane
;		First release 
;
; VERSION:
;       1.1	17-03-99
; 
;-
;-----------------------------------------------------------------------------



PRO bgsetm, pipe, sz0, sz, scnte, sil, ndstr, strga, ndmet, npl, strgmi, strgmf

		;**********************************
		;**** Initialise new variables ****
		;**********************************
  sz0   = ''
  sz    = ''
  scnte = ''
  sil   = ''
  ndstr = 0
  input = ''
  idum  = 0
  jdum  = 0
  npl   = 0 

		;********************************
		;**** Read data from fortran ****
		;********************************
                
  readf, pipe, input
  sz0 = input
  readf, pipe, input
  sz = input
  readf, pipe, input
  scnte = input
  readf, pipe, input
  sil = input

  readf, pipe, idum
  ndstr  = idum
  strga  = strarr(ndstr)
  strgmi = strarr(ndstr)

  for i = 0, ndstr-1 do begin
    readf, pipe, input
    strga(i) = input
  end
  
  for i = 0, ndstr-1 do begin
    readf, pipe, input
    strgmi(i) = input
  end

  readf, pipe, input
  ndmet = input
  readf, pipe, jdum
  npl    = jdum
  strgmf = strarr(ndmet)

  for i = 0, ndmet-1 do begin
    readf, pipe, input
    strgmf(i) = input
  end
  
end
