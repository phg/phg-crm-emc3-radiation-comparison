; Copyright (c) 1999, Strathclyde University.
; SCCS info: Module @(#)$Header: /home/adascvs/idl/adas2xx/adas216/bgrwer.pro,v 1.1 2004/07/06 11:40:50 whitefor Exp $ Date $Date: 2004/07/06 11:40:50 $
;+
; PROJECT:
;       ADAS 
;
; NAME:
;	BGRWER
;
; PURPOSE:
;	IDL user interface and communications with ADAS216 FORTRAN
;	process via pipe.
;
; EXPLANATION:
;	This routine reads/writes error data from/to fortran.
;
; USE:
;	The use of this routine is specific to ADAS216, see bgispf.pro.
;
; INPUTS:
;	PIPE	- The IDL unit number of the bi-directional pipe to the
;		  ADAS216 FORTRAN process.
;
;	IRW	- Integer, 0 for read, 1 to write
;
; OPTIONAL INPUTS:
;	None
;
; OUTPUTS:
;	VALUE	- arrarys are filled with data.
;
; OPTIONAL OUTPUTS:
;	None
;
; KEYWORD PARAMETERS:
;	None
;
; CALLS:
;	ADAS216_PROC	Invoke the IDL interface for ADAS216 data
;		 	processing options/input.
;
; SIDE EFFECTS:
;	Two way communications with ADAS216 FORTRAN via the
;	bi-directional UNIX pipe.
;
; CATEGORY:
;	Adas system.
;
; WRITTEN:
;       Martin O'Mullane
;
; MODIFIED:
;	1.1	Martin O'Mullane
;		First release 
;
; VERSION:
;       1.1	17-03-99
;
;-
;-----------------------------------------------------------------------------


PRO bgrwer, pipe, irw, narr, def, gen, spf

  idum = 0
  ddum = 0.0
  
  if irw eq 0 then begin
  
     for i=0,2 do begin
        readf,pipe,idum
        narr(i)=idum
     endfor
     
     if narr(0) eq 1 then begin
        readf,pipe,ddum
        def=ddum
     endif
     
     if narr(1) ge 1 then begin
        for i=0,narr(1)-1 do begin
            for j=0,1 do begin
                readf,pipe,ddum
                gen(i,j)=ddum
            endfor
        endfor
     endif
     
     if narr(2) ge 1 then begin
        for i=0,narr(2)-1 do begin
            for j=0,2 do begin
                readf,pipe,ddum
                spf(i,j)=ddum
            endfor
        endfor
     endif
     
  endif else begin

     for i=0,2 do begin
        printf,pipe,narr(i)
     endfor
     
     if narr(0) eq 1 then begin
        printf,pipe,def 
     endif
     
     if narr(1) ge 1 then begin
        for i=0,narr(1)-1 do begin
            for j=0,1 do begin
                printf,pipe,gen(i,j)
            endfor
        endfor
     endif

     if narr(2) ge 1 then begin
        for i=0,narr(2)-1 do begin
            for j=0,2 do begin
                printf,pipe,spf(i,j)
            endfor
        endfor
     endif
     
  endelse
        
     

END
