; Copyright (c) 1999, Strathclyde University.
; SCCS info: Module @(#)$Header: /home/adascvs/idl/adas2xx/adas216/adas216_error.pro,v 1.1 2004/07/06 10:25:58 whitefor Exp $ Date $Date: 2004/07/06 10:25:58 $
;+
; PROJECT:
;       ADAS 
;
; NAME:
;	ADAS216_ERROR
;
; PURPOSE:
;	IDL ADAS user interface, processing options/input.
;
; EXPLANATION:
;	This routine creates and manages a pop-up window which allows
;	the user to select error data to in ADAS216.
;
; USE:
;	This routine is ADAS216 specific, see bgispf.pro for how it
;	is used.
;
; INPUTS:
;	VAL	- A structure which determines the initial settings of
;		  the processing options widget.  The value is passed
;		  unmodified into cw_adas216_proc.pro.
;
;		  See cw_adas216_proc.pro for a full description of this
;		  structure.
;
;	SZ	- String, recombined ion charge read.
;
;	SZ0	- String, nuclear charge read.
;
;	STRGA	- String array, level designations.
;
;	DSNINC	- String; The full system file name of the input COPASE
;		  dataset selected by the user for processing.
;
;       BITFILE - String; the path to the directory containing bitmaps
;                 for the 'escape to series menu' button.
;
;	IL	- Integer; Number of energy levels.
;
;	SELEM	- String; The element symbol, passed from Fortran
;
; OPTIONAL INPUTS:
;	None
;
; OUTPUTS:
;	VAL	- On output the structure records the final settings of
;		  the processing selections widget if the user pressed the
;		  'Done' button, otherwise it is not changed from input.
;
;	ACT	- String; Either 'Done' or 'Cancel' for the button the
;		  user pressed to terminate the processing options
;		  window.
;
; OPTIONAL OUTPUTS:
;	None
;
; KEYWORD PARAMETERS:
;	FONT_LARGE - The name of a larger font.
;
;	FONT_SMALL - The name of a smaller font.
;
;	EDIT_FONTS - A structure of two fonts used in the table editor
;		     adas_edtab.pro, {font_norm:'',font_input:''}.
;
; CALLS:
;	CW_ADAS216_ERRO   Declares the processing options widget.
;	DYNLABEL	  Sets dynamic_resize keyword for label widgets.
;	ADAS216_ERROR_EV  Called indirectly during widget management,
;			  routine included in this file.
;	XMAMAGER
;
; SIDE EFFECTS:
;
; CATEGORY:
;	Widgets
;
; WRITTEN:
;       Martin O'Mullane
;
; MODIFIED:
;	1.1	Martin O'Mullane
;		First release 
;
; VERSION:
;       1.1	17-03-99
;
;-
;-----------------------------------------------------------------------------


PRO adas216_error_ev, event

    COMMON err216_blk, action, value


		;**** Find the event type and copy to common ****
                
    action = event.action

    CASE action OF

		;**** 'Done' button   ****
	'Done'  : begin

			;**** Get the output widget value ****
		widget_control,event.id,get_value=e_val
                value.errtype = e_val.errtype
                value.errval  = e_val.errval
		widget_control,event.top,/destroy

	   end


		;**** 'Cancel' button ****
	'Cancel': widget_control,event.top,/destroy

                ;**** 'Menu' button   ****

        'Menu'  : widget_control, event.top, /destroy

        ELSE:   ;**** do nothing ****

    END

END

;-----------------------------------------------------------------------------


PRO adas216_error, val, sz, sz0, selem, il, strga, ndgen, ndspf, 	$
		   dsninc, bitfile, act, 			 	$
		   FONT_LARGE=font_large, FONT_SMALL=font_small, 	$
		   EDIT_FONTS=edit_fonts

  COMMON err216_blk, action, value

                ;**** Copy value to common ****

  value = val
    
		;**** Set defaults for keywords ****
                
  IF NOT (KEYWORD_SET(font_large)) THEN font_large = ''
  IF NOT (KEYWORD_SET(font_small)) THEN font_small = ''
  IF NOT (KEYWORD_SET(edit_fonts)) THEN $
				edit_fonts = { font_norm:'', font_input:''}

                ;**** create base widget ****
                
  procid = widget_base(TITLE='ADAS216 ERRORS ENTRY', $
					XOFFSET=50,YOFFSET=0)

		;**** Declare processing widget ****
                
  errval = {a_subset,	                      $
	    title     :       value.title,    $
            errtype   :       value.errtype,  $
            errval    :       value.errval    }
            
  cwid = cw_adas216_error(procid, sz, sz0, selem, il, strga,ndgen, ndspf, $ 
                          dsninc, bitfile, 				  $
			  VALUE=errval, 				   	  $
			  FONT_LARGE=font_large, FONT_SMALL=font_small,   $
			  EDIT_FONTS=edit_fonts,uvalue='ERR_ENTRY')

		;**** Realize the new widget ****
                
;  dynlabel, procid   ; dynamic resize is fatal to the speed of the widget
  widget_control,procid,/realize

		;**** make widget modal ****
                
  xmanager,'adas216_error',procid,event_handler='adas216_error_ev', $
					/modal,/just_reg
 
		;**** Return the output value ****
  act = action
  val = value
  

END

