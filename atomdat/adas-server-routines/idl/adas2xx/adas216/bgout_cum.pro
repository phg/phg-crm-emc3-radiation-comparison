; Copyright (c) 1999, Strathclyde University.
; SCCS info: Module @(#)$Header: /home/adascvs/idl/adas2xx/adas216/bgout_cum.pro,v 1.1 2004/07/06 11:40:26 whitefor Exp $ Date $Date: 2004/07/06 11:40:26 $
;+
; PROJECT:
;       ADAS 
;
; NAME:
;	BGOUT_CUM
;
; PURPOSE:
;	Outputs the results of cumulative error analysis to paper.text.
;
; EXPLANATION:
;	Later!
;     
;
; INPUTS:
;       VALUE - Structure holding necessary information.
;
;               numte    : number of temperatures  
;               numden   : number of densities 
;               numlev   : number of levels
;               te       : temperatures  
;               den      : densities
;               slev     : levels (string)
;               iunit_t  : temperature units (integer) (K, eV, reduced)   
;               iunit_d  : density units (integer) (cm**-3, reduced)   
;
; OPTIONAL INPUTS:
;	None.
;
; OUTPUTS:
;       
;
; OPTIONAL OUTPUTS:
;       None
;
; KEYWORD PARAMETERS:
;
; CALLS:
;
; SIDE EFFECTS:
;	File output
;
;
; CATEGORY:
;	ADAS system
;
; WRITTEN:
;       Martin O'Mullane
;
; MODIFIED:
;	1.1	Martin O'Mullane
;		First release 
;
; VERSION:
;       1.1	17-03-99
;
;-
;-----------------------------------------------------------------------------


PRO bgout_cum, lun, VALUE=value

  old_except = !except
  !except = 0
 


                ;********************
                ;**** Set values ****
                ;********************

  
  numte    = value.numte
  numden   = value.numden
  numlev   = value.numlev
  
  te       = value.te
  den      = value.den
  slev     = value.slev
    
  mean_u   = value.mean_u
  sd_u     = value.sd_u
  
  iunit_t  = value.iunit_t
  iunit_d  = value.iunit_d   
 
  name_t  = ['K','eV','Red.']
  name_d  = ['cm-3','Red.']
  
  
   
  
                ;*******************************
                ;**** cycle over Te/ne grid ****
                ;*******************************
                
       
   format_str0 = '(a,i4,3x,a,/,a,/a)'
   format_str1 = '(3(10x,8(e10.2),:,/))'
   format_str2 = '(e10.2,8(e10.2),:,/,2(10x,8(e10.2),:,/))'
  
   unitstr = 'Te (' + name_t(iunit_t-1) + ') / ne (' + name_d(iunit_d-1) + ')'
   
   
   for kl = 1, numlev-1 do begin
   
     printf, lun,  'Level :', kl+1, slev(kl), '-----------', $
                   unitstr, format=format_str0
   
     printf, lun, den, format=format_str1
     
     for kt = 0,numte-1 do begin
       
       printf, lun, te(kt), mean_u(kl,kt,*), format=format_str2 
       printf, lun, sd_u(kl,kt,*), format=format_str1 
   
     endfor
     
     printf, lun, ' '
     
   endfor

 
LABELend:
 
  !except = old_except 


END
