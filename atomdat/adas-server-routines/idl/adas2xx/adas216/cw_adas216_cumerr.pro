; Copyright (c) 1999, Strathclyde University.
; SCCS info: Module @(#)$Header: /home/adascvs/idl/adas2xx/adas216/cw_adas216_cumerr.pro,v 1.2 2004/07/06 12:35:48 whitefor Exp $ Date $Date: 2004/07/06 12:35:48 $
;+
; PROJECT:
;       ADAS 
;
; NAME:
;	CW_ADAS216_CUMERR()
;
; PURPOSE:
;	Produces a widget for ADAS216 processing options/input.
;
; EXPLANATION:
;	This function creates a compound widget consisting of a text
;	widget holding an editable 'Run title', the dataset name/browse
;	widget cw_adas_dsbr, label widgets with charge information for
;	the input data, a table widget for temperature data cw_adas_table,
;	a second cw_adas_table widget for density data, buttons to
;	enter default values into the temperature and density tables,
;	a multiple selection widget cw_adas_sel, a reaction settings
;	widget cw_adas_reac2, a message widget, a 'Cancel' button and
;	finally a 'Done' button.
;
;	The compound widgets included in this widget are self managing.
;	This widget only manages events from the two 'Defaults' buttons,
;	the 'Cancel' button and the 'Done' button.
;
;	This widget only generates events for the 'Done' and 'Cancel'
;	buttons.
;
; USE:
;	This widget is specific to ADAS216, see adas216_errorproc.pro
;	for use.
;
; INPUTS:
;	TOPPARENT- Long integer, ID of parent widget.
;
;	SZ	- String, recombined ion charge read.
;
;	SZ0	- String, nuclear charge read.
;
;	STRGA	- String array, level designations.
;
;	DSNINC	- String; The full system file name of the input COPASE
;		  dataset selected by the user for processing.
;
;	NDTEM	- Integer; Maximum number of temperatures allowed.
;
;	NDDEN	- Integer; Maximum number of densities allowed.
;
;	NDMET	- Integer; Maximum number of metastables allowed.
;
;	IL	- Integer; Number of energy levels.
;
;	NV	- Integer; Number of termperatures.
;
;	TSCEF	- dblarr(8,3); Input electron temperatures in three units.
;
;	The inputs SZ to TSCEF map exactly onto variables of the same
;	name in the ADAS216 FORTRAN program.
;
;       BITFILE - String; the path to the directory containing bitmaps
;                 for the 'escape to series menu' button.
;
;	INVAL	- Structure; the input screen structure - see cw_adas216_in.pro
;
;	SELEM	- String; selected element symbol
;
; OPTIONAL INPUTS:
;	None
;
; OUTPUTS:
;	The return value of this function is the ID of the declared widget.
;
; OPTIONAL OUTPUTS:
;	None
;
; KEYWORD PARAMETERS:
;	VALUE	- A structure which determines the initial settings of
;		  the processing options widget.
;
;		  The default VALUE is created thus;
;
;			imetr = make_array(ndmet,/int,value=0)
;			temp = dblarr(ndtem)
;			dens = dblarr(ndden)
;			ps = { $
;		 	TITLE:'',	NMET:0, $
;			IMETR:imetr,	IFOUT:1, $
;			MAXT:0,		TINE:temp, $
;			TINP:temp,	TINH:temp, $
;			IDOUT:1,	MAXD:0, $
;			DINE:dens,	DINP:dens, $
;			RATHA:dens,	RATPIA:dens, $
;			ZEFF:'',	LPSEL:0, $
;			LZSEL:0,	LISEL:0, $
;			LHSEL:0,	LRSEL:0, $
;                       LIOSEL:0,       LNSEL:0, $
;			LNORM:0,		 $
;			VAL502:struct502,	 $
;			WVLS:0.0,	WVLL:0.0,$
;			AVLT:0.0  }
;
; 		TITLE	entered general title for program run
; 		NMET	number of metastables
; 		(IMETR(I),I=1,NMET)	index of metastables
; 		IFOUT	input temperature units (1,2,3)
; 		MAXT	number of input temperatures (1-20)
; 		(TINE(I),I=1,MAXT)	electron temperatures
; 		(TINP(I),I=1,MAXT)	proton temperatures
; 		(TINH(I),I=1,MAXT)	neutral hydrogen temperatures
; 		IDOUT	input density units (1,2)
; 		MAXD	number of input densities
; 		(DINE(I),I=1,MAXD)	electron densities
; 		(DINP(I),I=1,MAXD)	proton densities
; 		(RATHA(I),I=1,MAXD)	ratio (neut h dens/elec dens)
; 		(RATPIA(I),I=1,MAXD)	ratio (n(z+1)/n(z) stage abund)
; 		ZEFF	plasma z effective, a string holding a valid number.
; 		LPSEL	include proton collisions?
; 		LZSEL	scale proton collisions with plasma z effective?
; 		LISEL	include ionisation rates?
; 		LHSEL	include charge transfer from neutral hydrogen?
; 		LRSEL	include free electron recombination?
;               LIOSEL to be determined
;               LNSEL   include projected bundle-n data?
;
;		All of the above structure elements map onto variables of
;		the same name in the ADAS216 FORTRAN program.
;
;
;	UVALUE	- A user value for the widget. Default 0.
;
;	FONT_LARGE - The name of a larger font.  Default current system
;		     font
;
;	FONT_SMALL - The name of a smaller font. Default current system
;		     font.
;
;	EDIT_FONTS - A structure of two fonts used in the table editor
;		     adas_edtab.pro, {font_norm:'',font_input:''}.  Defaults
;		     to current system font.
;
;	NUM_FORM   - String; Numeric format to use in tables.  Default
;			'(E10.3)'
;		     
;
; CALLS:
;	POPUP		 Popup warning window with buttons.
;	XXDEFT		 Returns default temperature table.
;	XXDEFD		 Returns default density table.
;	CW_ADAS_DSBR	 Dataset name and comments browsing button.
;	CW_ADAS_TABLE	 Adas data table widget.
;	CW_ADAS_SEL	 Adas mulitple selection widget.
;	CW_ADAS208_REAC2 Reaction settings for ADAS216.
;
;	See side effects for other related widget management routines.
;
; SIDE EFFECTS:
;
;	The following widget management routines are included in this
;	file;
;	cumerr216_GET_VAL()  Returns the current VALUE structure.
;	cumerr216_EVENT()            Process and issue events.
;
;       Talks to fortran adas216.out a lot!
;
; CATEGORY:
;	Compound Widget
;
; WRITTEN:
;       Martin O'Mullane
;
; MODIFIED:
;	1.1	Martin O'Mullane
;		First release 
;	1.2	Richard Martin
;		Changed name of widget base from switch to switchb for
;		IDL 5.4 compatibility
;
; VERSION:
;       1.1	17-03-99
;	  1.2	10-11-00
;
;-
;-----------------------------------------------------------------------------

FUNCTION cumerr216_get_val, id


                ;**** Return to caller on error ****
  ON_ERROR, 2

		 ;***************************************
                 ;****     Retrieve the state 	     ****
		 ;**** Get first_child widget id     ****
		 ;**** because state is stored there ****
		 ;***************************************

  first_child = widget_info(id, /child)
  widget_control, first_child, get_uvalue = state



		;**********************************
		;**** Get number of iterations ****
		;**********************************
                
  widget_control,state.iterid,get_value=num
  numiterset = num


		;***********************************************
		;**** write selected values to PS structure ****
		;***********************************************

  ps = {cumerr216_set, 		 			$
        level_ind       :       state.level_ind,  	$
        tev_ind         :       state.tev_ind,    	$
        dens_ind        :       state.dens_ind,   	$
        numiterset      :       numiterset,           	$
        ust             :       state.ust       	}

  widget_control, first_child, set_uvalue=state, /no_copy

  RETURN, ps

END

;-----------------------------------------------------------------------------

FUNCTION cumerr216_event, event

                ;**** Base ID of compound widget ****
                
   parent=event.handler

		;**********************************************
                ;**** Retrieve the user value state        ****
		;**** Get id of first_child widget because ****
		;**** user value "state" is stored there   ****
		;**********************************************

   first_child = widget_info(parent, /child)
   widget_control, first_child, get_uvalue=state,/no_copy


   
   widget_control, state.graphid  , get_value = update_win
   wset,update_win
   
   if state.ust.ifirstgo eq 0 then begin 
      plot,[state.ust.umin,state.ust.umax],[-5,state.ust.iumax], $
          xtitle='Deviation from unperturbed value',ystyle=1,/nodata
   endif
		;*********************************
		;**** Clear previous messages ****
		;*********************************
                
  widget_control,state.messid,  $
                 set_value='                                               '


                ;************************
                ;**** Process events ****
                ;************************
                
  CASE event.id OF

                ;**************************************************
                ;**** GO button - now communicate with fortran ****
                ;**************************************************
                
    state.goid: begin                                        

                ;****************************************************
                ;**** Get all flags and appropriate user choices ****
                ;**** and then start the analysis                ****
                ;****************************************************

                ;*** Have to restore "state" before calls 
                ;*** to get_value so it  can be used there.  
      
      widget_control, first_child, set_uvalue=state, /no_copy

      widget_control, event.handler, get_value=ps

                ;**** reset state variable ****
                
      widget_control, first_child, get_uvalue=state, /no_copy
      
     
     
                ;**** restore progress bar labels  ****
                ;**** if timing info was displayed ****
     
      widget_control, state.timeid, map=0
      widget_control, state.update1id, map=1
      widget_control, state.update2id, map=1
      widget_control, state.update3id, map=1
    
    
      pipe = state.pipe
      
      
      printf, pipe, -999        ; tells fortran that it's a GO event
      state.ust.ifirstgo = 1    ; tells event handler not to write a new
                                ; update window for a second (or greater)
                                ; GO event.
     
      printf, pipe, ps.numiterset
      printf, pipe, ps.level_ind + 1     ; set to fortran indexing
      printf, pipe, ps.tev_ind   + 1
      printf, pipe, ps.dens_ind  + 1
       
      ; Check if these have changed since last iteration - if so get
      ; the new monitored py data from fortran. Erase the old and plot
      ; the new. Then continue!
      
      newmon = -1
      readf, pipe, newmon 
      
      if newmon eq 1 then begin
      
        new_y = intarr(state.ust.nb)
        idum  = 0
        for  i=0, state.ust.nb-1 do begin
          readf, pipe, idum
          new_y(i) = idum
        endfor 
        
        ; erase old
        plot,[state.ust.umin,state.ust.umax],[-5,state.ust.iumax], $
              xtitle='Deviation from unperturbed value',ystyle=1,/nodata

        state.ust.yp(*) = 0
         
        ; plot new
        er_ind = where(new_y ne 0, count)
        if count ne 0 then begin
          er_x = state.ust.xp(er_ind)
          er_y = new_y(er_ind)
          oplot,er_x,er_y,psym=2,symsize=0.2
          state.ust.yp(er_ind) = er_y
        endif

      endif 

      widget_control, state.barid    , get_value = bar_win
      widget_control, state.graphid  , get_value = update_win
      
      wset,bar_win
      erase

      ddum = 0.0d0
      
      step = 1000.0/ps.numiterset
      for i=0,ps.numiterset-1 do begin

         readf, pipe, ddum               ; read the ratio from fortran

         wset, bar_win
         p = float(i)/ps.numiterset*1000   
         q = p+step              

         for j=fix(p),fix(q) do begin
             x = (float(j)+1)/1000.0
             plots, [x, x],[0.0,1.0], color=1, /normal
         endfor
         widget_control, state.update2id, 			$
                  set_value = string(fix(q/10),format='(I3)')
                           
	
	 ; if a valid ratio then update the histogram		   
         if ddum lt 99.99 then begin  
            
            wset,update_win
	    
	    yind=where(state.ust.xp gt ddum-state.ust.ubin/2.0 and   $
                       state.ust.xp lt ddum+state.ust.ubin/2.0)
            yind=yind(0) + 1
            yind=min([yind,state.ust.nb-1])
            state.ust.yp(yind) = state.ust.yp(yind)+1


            px = yind*state.ust.ubin + state.ust.umin-state.ust.ubin
            py = state.ust.yp(yind)

            if (py-1) gt 0 then $
              plots,px,py-1,psym=2,color=0,symsize=0.2     ; erase old total

            plots,px,py,psym=2,symsize=0.2                 ; update it    
	 endif
                   
      endfor
      
      readf, pipe, ddum
      elapsed_time = ddum
      
      
                ;**** an iteration has passed - say how long it took, ****
                ;**** ask for more, calculate how many and allow      ****
                ;**** inspection of data                              ****
      
      widget_control, state.messid, set_value = 'Extra iterations can be set' 
      
      widget_control, state.timeid, map=1
      widget_control, state.update1id, map=0
      widget_control, state.update2id, map=0
      widget_control, state.update3id, map=0
      el_str = string(elapsed_time,format='(f8.2)') + ' sec' 
      widget_control, state.timeid, set_value = 'Time of Calculation' + el_str
     
      state.info.numiter = state.info.numiter + ps.numiterset
     
      widget_control, state.inspctid, sensitive=1
    
    end
      
      
		;***********************************************
                ;**** inspect the fits to the iterated data ****
		;***********************************************
                
    state.inspctid : begin
     
      inspct_val = { inspct216c,          		$
           	     numte    : state.info.numte,       $
                     numden   : state.info.numden,      $
                     numlev   : state.info.numlev,      $
                     numiter  : state.info.numiter,     $
                     numbin   : state.ust.nb,      	$
                     te       : state.info.tev,      	$
                     den      : state.info.dens,     	$
                     slev     : state.info.slev,    	$
                     iunit_t  : state.info.iunit_t,     $
                     iunit_d  : state.info.iunit_d,     $
                     xhist    : state.ust.xp,      	$
                     dumpfile : state.info.dumpfile     }
   	   
      adas216_inspct_cum, state.devlist, state.devcode, $
                      value = inspct_val, font=state.font
    
    end
    
        
    
		;**********************
                ;**** monitor info ****
		;**********************
                
    state.levid  : state.level_ind  = event.index  
    state.tevid  : state.tev_ind    = event.index  
    state.densid : state.dens_ind   = event.index  
   


                ;**** Could we have missed something? ****
                

    ELSE: new_event = 0L
    
    

  ENDCASE
  
		;****************************
		;*** make state available ***
		;****************************

    widget_control, first_child, set_uvalue=state, /no_copy

  RETURN, new_event
  
END

;-----------------------------------------------------------------------------

FUNCTION cw_adas216_cumerr, topparent, 					$
                strga, tev, dens, iunit_t, iunit_d, 			$ 
                dumpfile, bitfile, devlist, devcode, pipe,	        $
		VALUE=value, UVALUE=uvalue, 				$
		FONT_LARGE=font_large, FONT_SMALL=font_small, 		$
		EDIT_FONTS=edit_fonts

		;**** Set defaults for keywords ****
                
  IF NOT (KEYWORD_SET(uvalue))     THEN uvalue = 0
  IF NOT (KEYWORD_SET(font_large)) THEN font_large = ''
  IF NOT (KEYWORD_SET(font_small)) THEN font_small = ''
  IF NOT (KEYWORD_SET(edit_fonts)) THEN $
				   edit_fonts = {font_norm:'',font_input:''}


  IF NOT (KEYWORD_SET(value)) THEN begin
	message,'CW_ADAS216_CUMERR structure must be passed'
  END ELSE BEGIN
	ps = {cumerr216_set,	                          $
                        level_ind   :   value.level_ind,  $
                        tev_ind     :   value.tev_ind,    $
                        dens_ind    :   value.dens_ind,   $
                        numiterset  :   value.numiterset, $
                        ust         :   value.ust         }
  END



  ; find number of levels in adf04 file and don't set level_ind to 
  ; the ground value
  
  il = where(strga eq ' *** END OF LEVELS *** ')    
  il = il(0)
  if ps.level_ind eq 0 or ps.level_ind gt il then ps.level_ind = 1 



                ;*********************************************************
                ;**** Modify certain parameters and results depending ****
                ;**** on the machine being used                       ****
                ;*********************************************************

    machine = GETENV('TARGET_MACHINE')
    if machine eq 'HPUX' then begin
        y_size = 4
        large_font = font_small
    endif else begin
        y_size = 6
        large_font = font_large
    endelse




		;****************************************************
		;**** Create the Processing options/input window ****
		;****************************************************

		;**** create base widget ****
                
  parent = widget_base(topparent, UVALUE = uvalue, 		$
		       EVENT_FUNC     = "cumerr216_event", 	$
		       FUNC_GET_VALUE = "cumerr216_get_val",    $
		       /COLUMN)

		;******************************************************
		;**** Create a dummy widget just to hold value of *****
		;**** "state" variable so as not to get confused  *****
		;**** with any other values. Adopt IDL practice   *****
		;**** of using first child widget                 *****
		;******************************************************

  first_child = widget_base(parent)
  topbase = widget_base(first_child,/column)


                
  base  = widget_base(topbase,/column,/align_left)


	
  
		;************************************
                ;**** Parts of processing screen ****
		;************************************
                
  base      = widget_base(base, /row,/frame)
  left_col  = widget_base(base, /column)
  skip      = widget_label(base, value = '  ')
  right_col = widget_base(base, /column)



        ; **** graph of monitored level ****

  graphid = widget_draw(left_col, xsize=400, ysize=340)
                     
  skip    = widget_label(left_col, value = ' ')
  
  
        ; **** progress bar                                 ****
        ; **** split into 3 parts so that % only is updated ****
        ; **** replace with time of calculation at end      ****
        ; **** by mapping/unmapping the labels              ****
        
  barbase   = widget_base(left_col,/align_center)

  barid     = widget_draw(barbase, ysize=20, xsize=340) 

  switchb    = widget_base(left_col,/align_center)
  prow      = widget_base(switchb, /row,/align_center)
  trow      = widget_base(switchb, /row,/align_center)
  update1id = widget_label(prow, value = 'PROCESSING')
  update2id = widget_label(prow, value = '  0')
  update3id = widget_label(prow, value = ' % COMPLETED          ')

  timeid    = widget_label(trow, value = '                                ')
  widget_control, timeid, map=0



	; **** monitored selection ****

  name_t = ['Kelvin','eV','Reduced']
  name_d = ['cm-3','Reduced']
 
  mcol   = widget_base(right_col, /column, /frame)
  mlab   = widget_label(mcol, value='Choose parameters to monitor: ',  $ 
                        font=font_large,/align_left)
  skip   = widget_label(mcol, value = ' ')

  mrow   = widget_base(mcol, /row)
  mlab   = widget_label(mrow, value='Level       : ', font=font_small)
  slev   = strga(0:il-1)
  levid  = widget_droplist(mrow, value=slev, font=font_small)

  mrow   = widget_base(mcol, /row)
  mlab   = widget_label(mrow, value='Temperature : ', font=font_small)
  temps  = string(tev,format='(e8.2)')
  tevid  = widget_droplist(mrow, value=temps, font=font_small)
  mlab   = widget_label(mrow, value=' '+name_t(iunit_t-1), font=font_small)

  mrow   = widget_base(mcol, /row)
  mlab   = widget_label(mrow, value='Density     : ', font=font_small)
  denss  = string(dens,format='(e8.2)')
  densid = widget_droplist(mrow, value=denss, font=font_small)
  mlab   = widget_label(mrow, value=' '+name_d(iunit_d-1), font=font_small)


	; **** Number of iterations ****

  skip   = widget_label(right_col, value = ' ')
  
  iterid = cw_field(right_col, title = 'Enter number of iterations :',  $
                               value = ps.numiterset, /integer,		$
                               font  = font_large,			$
                               fieldfont = font_large, 			$
                               xsize=7, /column )
                                                          
                               
	
        ; **** Start the analysis with a big GO button ****
        
  frow    = widget_base(right_col, /row)
  flbl    = widget_label(frow, value='Proceed with Analysis : ', 	$
                         font=font_large)

  gofile = bitfile + '/go.bmp'
  read_X11_bitmap, gofile, gobitmap
  goid   = widget_button(frow, value=gobitmap,/align_left, font=font_large)



        ; **** Inspect results after an iteration ****

  inspctid = widget_button(right_col, value='Inspect Results',      $
                           /align_left, font=font_large)

  widget_control,inspctid,sensitive=0
  
  

 	; **** If GO is pressed for a second time put up a ****
 	; **** message asking for extra iterations. Leave  ****
 	; **** space for this message here.                ****
        
                
  messid = widget_label(parent,font=font_large, $
	value='Choose a level to monitor and press GO to start calculation')



  
  
 		;***********************************
		;**** set up initial conditions ****
		;***********************************
 
   widget_control, levid,  set_droplist_select = ps.level_ind
   widget_control, tevid,  set_droplist_select = ps.tev_ind
   widget_control, densid, set_droplist_select = ps.dens_ind
 
 
		;*********************************
		;**** assemble info structure ****
		;*********************************
  
  numte  = n_elements(tev)
  numte  = numte(0) 
  numden = n_elements(dens)
  numden = numden(0) 
  numlev = il

  info = { info216c, 					$
 	        numte		:	numte,		$
                numden          :       numden,		$	
                numlev          :       il,		$
                numiter         :       0,		$
                slev		:	slev,		$
	        tev		:	tev,		$
                dens		:       dens,		$
                iunit_t		:	iunit_t,	$	
                iunit_d		:       iunit_d,	$
	        dumpfile        :       dumpfile  	}
  
    
    
    
		;**********************************
		;**** create a state structure ****
		;**********************************
                
  new_state = { messid          :       messid,		$
	        graphid         :       graphid,	$
	        barid           :       barid, 	 	$
	        update1id       :       update1id,	$
	        update2id       :       update2id,	$
	        update3id       :       update3id,	$
	        timeid          :       timeid,		$
	        levid           :       levid,		$
	        tevid           :       tevid,		$
	        densid          :       densid,		$
                iterid          :       iterid,		$
	        goid            :       goid,	 	$
	        inspctid        :       inspctid,	$
	        level_ind       :       ps.level_ind,	$
	        tev_ind         :       ps.tev_ind,	$
	        dens_ind        :       ps.dens_ind, 	$
	        devlist         :       devlist, 	$
	        devcode         :       devcode, 	$
	        pipe            :       pipe, 	  	$
	        font            :       font_large, 	$
	        info            :       info, 		$
                ust		:	ps.ust		}

                ;**** Save initial state structure ****

    widget_control, first_child, set_uvalue=new_state, /no_copy

  RETURN, parent

END

