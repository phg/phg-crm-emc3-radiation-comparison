; Copyright (c) 1999, Strathclyde University.
; SCCS info: Module @(#)$Header: /home/adascvs/idl/adas2xx/adas216/getfit.pro,v 1.1 2004/07/06 13:58:42 whitefor Exp $ Date $Date: 2004/07/06 13:58:42 $
;+
; PROJECT:
;       ADAS 
;
; NAME:
;	GETFIT
;
; PURPOSE:
;	Calls a modified version of gaussfit in IDL v4 installations. 
;       There is a problem with the RSI supplied curvefit procedure in
;       that it can fail to converge if CHISQ is NaN. This file contains
;       the IDL version 5 copyrighted  curvefit and gaussfit - renamed to 
;       mod_curvefit and mod_gaussfit in order not to conflict with
;       the installed versions. Redistribution should not be an 
;       issue as the ADAS sites have IDL licensed. 
;
; EXPLANATION:
;	Rather than using yfit = gaussfit(x,y,a) call the procedure
;       getfit, x, y, a. Note than a must be defined as fltarr(4)
;       before calling.     
;     
;
; INPUTS:
;
; OPTIONAL INPUTS:
;	None.
;
; OUTPUTS:
;       
;
; OPTIONAL OUTPUTS:
;       None
;
; KEYWORD PARAMETERS:
;
; CALLS:
;
; SIDE EFFECTS:
;
; CATEGORY:
;	ADAS system
;
; WRITTEN:
;       Martin O'Mullane
;
; MODIFIED:
;	1.1	Martin O'Mullane
;		First release 
;
; VERSION:
;       1.1	17-04-99
;
;-
;-----------------------------------------------------------------------------



; $Id: getfit.pro,v 1.1 2004/07/06 13:58:42 whitefor Exp $
;
; Copyright (c) 1982-1998, Research Systems, Inc.  All rights reserved.
;       Unauthorized reproduction prohibited.
;
FUNCTION MOD_CURVEFIT, x, y, weights, a, sigma, FUNCTION_NAME = Function_Name, $
                        ITMAX=itmax, ITER=iter, TOL=tol, CHI2=chi2, $
                        NODERIVATIVE=noderivative, CHISQ=chisq
;+
; NAME:
;       MOD_CURVEFIT
;
; PURPOSE:
;       Non-linear least squares fit to a function of an arbitrary 
;       number of parameters.  The function may be any non-linear 
;       function.  If available, partial derivatives can be calculated by 
;       the user function, else this routine will estimate partial derivatives
;       with a forward difference approximation.
;
; CATEGORY:
;       E2 - Curve and Surface Fitting.
;
; CALLING SEQUENCE:
;       Result = MOD_CURVEFIT(X, Y, Weights, A, SIGMA, FUNCTION_NAME = name, $
;                         ITMAX=ITMAX, ITER=ITER, TOL=TOL, /NODERIVATIVE)
;
; INPUTS:
;       X:  A row vector of independent variables.  This routine does
;           not manipulate or use values in X, it simply passes X
;           to the user-written function.
;
;       Y:  A row vector containing the dependent variable.
;
;  Weights:  A row vector of weights, the same length as Y.
;            For no weighting,
;                 Weights(i) = 1.0.
;            For instrumental (Gaussian) weighting,
;                 Weights(i)=1.0/sigma(i)^2
;            For statistical (Poisson)  weighting,
;                 Weights(i) = 1.0/y(i), etc.
;
;       A:  A vector, with as many elements as the number of terms, that 
;           contains the initial estimate for each parameter.  IF A is double-
;           precision, calculations are performed in double precision, 
;           otherwise they are performed in single precision. Fitted parameters
;           are returned in A.
;
; KEYWORDS:
;       FUNCTION_NAME:  The name of the function (actually, a procedure) to 
;       fit.  IF omitted, "FUNCT" is used. The procedure must be written as
;       described under RESTRICTIONS, below.
;
;       ITMAX:  Maximum number of iterations. Default = 20.
;       ITER:   The actual number of iterations which were performed
;       TOL:    The convergence tolerance. The routine returns when the
;               relative decrease in chi-squared is less than TOL in an 
;               interation. Default = 1.e-3.
;       CHI2:   The value of chi-squared on exit (obselete)
;     
;       CHISQ:   The value of reduced chi-squared on exit
;       NODERIVATIVE:   IF this keyword is set THEN the user procedure will not
;               be requested to provide partial derivatives. The partial
;               derivatives will be estimated in CURVEFIT using forward
;               differences. IF analytical derivatives are available they
;               should always be used.
;
; OUTPUTS:
;       Returns a vector of calculated values.
;       A:  A vector of parameters containing fit.
;
; OPTIONAL OUTPUT PARAMETERS:
;       Sigma:  A vector of standard deviations for the parameters in A.
;
; COMMON BLOCKS:
;       NONE.
;
; SIDE EFFECTS:
;       None.
;
; RESTRICTIONS:
;       The function to be fit must be defined and called FUNCT,
;       unless the FUNCTION_NAME keyword is supplied.  This function,
;       (actually written as a procedure) must accept values of
;       X (the independent variable), and A (the fitted function's
;       parameter values), and return F (the function's value at
;       X), and PDER (a 2D array of partial derivatives).
;       For an example, see FUNCT in the IDL User's Libaray.
;       A call to FUNCT is entered as:
;       FUNCT, X, A, F, PDER
; where:
;       X = Variable passed into CURVEFIT.  It is the job of the user-written
;           function to interpret this variable.
;       A = Vector of NTERMS function parameters, input.
;       F = Vector of NPOINT values of function, y(i) = funct(x), output.
;       PDER = Array, (NPOINT, NTERMS), of partial derivatives of funct.
;               PDER(I,J) = DErivative of function at ith point with
;               respect to jth parameter.  Optional output parameter.
;               PDER should not be calculated IF the parameter is not
;               supplied in call. IF the /NODERIVATIVE keyword is set in the
;               call to CURVEFIT THEN the user routine will never need to
;               calculate PDER.
;
; PROCEDURE:
;       Copied from "CURFIT", least squares fit to a non-linear
;       function, pages 237-239, Bevington, Data Reduction and Error
;       Analysis for the Physical Sciences.  This is adapted from:
;       Marquardt, "An Algorithm for Least-Squares Estimation of Nonlinear
;       Parameters", J. Soc. Ind. Appl. Math., Vol 11, no. 2, pp. 431-441,
;       June, 1963.
;
;       "This method is the Gradient-expansion algorithm which
;       combines the best features of the gradient search with
;       the method of linearizing the fitting function."
;
;       Iterations are performed until the chi square changes by
;       only TOL or until ITMAX iterations have been performed.
;
;       The initial guess of the parameter values should be
;       as close to the actual values as possible or the solution
;       may not converge.
;
; EXAMPLE:  Fit a function of the form f(x) = a * exp(b*x) + c to
;           sample pairs contained in x and y.
;           In this example, a=a(0), b=a(1) and c=a(2).
;           The partials are easily computed symbolicaly:
;           df/da = exp(b*x), df/db = a * x * exp(b*x), and df/dc = 1.0
;
;           Here is the user-written procedure to return F(x) and
;           the partials, given x:
;
;       pro gfunct, x, a, f, pder      ; Function + partials
;         bx = exp(a(1) * x)
;         f= a(0) * bx + a(2)         ;Evaluate the function
;         IF N_PARAMS() ge 4 THEN $   ;Return partials?
;         pder= [[bx], [a(0) * x * bx], [replicate(1.0, N_ELEMENTS(f))]]
;       end
;
;         x=findgen(10)                  ;Define indep & dep variables.
;         y=[12.0, 11.0,10.2,9.4,8.7,8.1,7.5,6.9,6.5,6.1]
;         Weights=1.0/y            ;Weights
;         a=[10.0,-0.1,2.0]        ;Initial guess
;         yfit=curvefit(x,y,Weights,a,sigma,function_name='gfunct')
;         print, 'Function parameters: ', a
;         print, yfit
;       end
;
; MODIFICATION HISTORY:
;       Written, DMS, RSI, September, 1982.
;       Does not iterate IF the first guess is good.  DMS, Oct, 1990.
;       Added CALL_PROCEDURE to make the function's name a parameter.
;              (Nov 1990)
;       12/14/92 - modified to reflect the changes in the 1991
;            edition of Bevington (eq. II-27) (jiy-suggested by CreaSo)
;       Mark Rivers, U of Chicago, Feb. 12, 1995
;           - Added following keywords: ITMAX, ITER, TOL, CHI2, NODERIVATIVE
;             These make the routine much more generally useful.
;           - Removed Oct. 1990 modification so the routine does one iteration
;             even IF first guess is good. Required to get meaningful output
;             for errors. 
;           - Added forward difference derivative calculations required for 
;             NODERIVATIVE keyword.
;           - Fixed a bug: PDER was passed to user's procedure on first call, 
;             but was not defined. Thus, user's procedure might not calculate
;             it, but the result was THEN used.
;
;      Steve Penton, RSI, June 1996.
;            - Changed SIGMAA to SIGMA to be consistant with other fitting 
;              routines.
;            - Changed CHI2 to CHISQ to be consistant with other fitting 
;              routines.
;            - Changed W to Weights to be consistant with other fitting 
;              routines.
;            _ Updated docs regarding weighing.
;           
;-
       ON_ERROR,2              ;Return to caller IF error

       ;Name of function to fit

       IF n_elements(function_name) LE 0 THEN function_name = "FUNCT"

       IF n_elements(tol) EQ 0 THEN tol = 1.e-3      ;Convergence tolerance
       IF n_elements(itmax) EQ 0 THEN itmax = 20     ;Maximum # iterations
       type = size(a)
       type = type(type(0)+1)
       double = type EQ 5

       IF (type ne 4) AND (type ne 5) THEN a = float(a)  ;Make params floating

       ; IF we will be estimating partial derivatives THEN compute machine
       ; precision

       IF keyword_set(NODERIVATIVE) THEN BEGIN
          res = machar(DOUBLE=double)
          eps = sqrt(res.eps)
       ENDIF

       nterms = n_elements(a)         ; # of parameters
       nfree = n_elements(y) - nterms ; Degrees of freedom

       IF nfree LE 0 THEN message, 'MOD_Curvefit - not enough data points.'

       flambda = 0.001                   ;Initial lambda
       diag = lindgen(nterms)*(nterms+1) ; Subscripts of diagonal elements

;      Define the partial derivative array

       IF double THEN pder = dblarr(n_elements(y), nterms) $
       ELSE pder = fltarr(n_elements(y), nterms)
;
       FOR iter = 1, itmax DO BEGIN      ; Iteration loop

;         Evaluate alpha and beta matricies.

          IF keyword_set(NODERIVATIVE) THEN BEGIN

;            Evaluate function and estimate partial derivatives
             CALL_PROCEDURE, Function_name, x, a, yfit

             FOR term=0, nterms-1 DO BEGIN

                p = a       ; Copy current parameters

                ; Increment size for forward difference derivative
                inc = eps * abs(p(term))    
                IF (inc EQ 0.) THEN inc = eps
                p(term) = p(term) + inc
                CALL_PROCEDURE, function_name, x, p, yfit1
                pder(0,term) = (yfit1-yfit)/inc

             ENDFOR
          ENDIF ELSE BEGIN

             ; The user's procedure will return partial derivatives
             call_procedure, function_name, x, a, yfit, pder 

          ENDELSE

          IF nterms EQ 1 THEN pder = reform(pder, n_elements(y), 1)

          beta = (y-yfit)*Weights # pder
          alpha = transpose(pder) # (Weights # (fltarr(nterms)+1)*pder)

          ; save current values of return parameters

          sigma1 = sqrt( 1.0 / alpha(diag) )           ; Current sigma.
          sigma  = sigma1

          chisq1 = total(Weights*(y-yfit)^2)/nfree     ; Current chi squared.
          chisq = chisq1

          yfit1 = yfit                                 

          done_early = chisq1 LT total(abs(y))/1e7/NFREE 
          IF done_early THEN GOTO, done

          c = sqrt(alpha(diag))
          c = c # c

          lambdaCount = 0

          REPEAT BEGIN

             lambdaCount = lambdaCount + 1

             ; Normalize alpha to have unit diagonal.

             array = alpha / c

             ; Augment the diagonal.

             array(diag) = array(diag)*(1.+flambda) 

             ; Invert modified curvature matrix to find new parameters.

             IF n_elements(array) EQ 1 THEN array = (1.0 / array) $
             ELSE array = invert(array)

             b = a + array/c # transpose(beta)          ; New params

             call_procedure, function_name, x, b, yfit  ; Evaluate function
             chisq = total(Weights*(y-yfit)^2)/nfree    ; New chisq
             sigma = sqrt(array(diag)/alpha(diag))      ; New sigma

             IF (finite(chisq) EQ 0) OR $
                  (lambdaCount GT 30 AND chisq GE chisq1) THEN BEGIN

                ; Reject changes made this iteration, use old values.

                yfit  = yfit1
                sigma = sigma1
                chisq = chisq1

                message, 'Failed to converge', /INFORMATIONAL

                GOTO, done 

             ENDIF             

             flambda = flambda*10.               ; Assume fit got worse

          ENDREP UNTIL chisq LE chisq1

          flambda = flambda/100.  

          a=b                                    ; Save new parameter estimate.

          IF ((chisq1-chisq)/chisq1) LE tol THEN GOTO,done   ;Finished?
       ENDFOR                        ;iteration loop
;
       MESSAGE, 'Failed to converge', /INFORMATIONAL
;
done:  chi2 = chisq         ; Return chi-squared (chi2 obsolete-still works)
       IF done_early THEN iter = iter - 1
       return,yfit          ; return result
END



; $Id: getfit.pro,v 1.1 2004/07/06 13:58:42 whitefor Exp $
;
; Copyright (c) 1982-1998, Research Systems, Inc.  All rights reserved.
;       Unauthorized reproduction prohibited.
;

PRO     MOD_GAUSS_FUNCT,X,A,F,PDER
; NAME:
;       MOD_GAUSS_FUNCT
;
; PURPOSE:
;       EVALUATE THE SUM OF A GAUSSIAN AND A 2ND ORDER POLYNOMIAL
;       AND OPTIONALLY RETURN THE VALUE OF IT'S PARTIAL DERIVATIVES.
;       NORMALLY, THIS FUNCTION IS USED BY CURVEFIT TO FIT THE
;       SUM OF A LINE AND A VARYING BACKGROUND TO ACTUAL DATA.
;
; CATEGORY:
;       E2 - CURVE AND SURFACE FITTING.
; CALLING SEQUENCE:
;       FUNCT,X,A,F,PDER
; INPUTS:
;       X = VALUES OF INDEPENDENT VARIABLE.
;       A = PARAMETERS OF EQUATION DESCRIBED BELOW.
; OUTPUTS:
;       F = VALUE OF FUNCTION AT EACH X(I).
;
; OPTIONAL OUTPUT PARAMETERS:
;       PDER = (N_ELEMENTS(X),6) ARRAY CONTAINING THE
;               PARTIAL DERIVATIVES.  P(I,J) = DERIVATIVE
;               AT ITH POINT W/RESPECT TO JTH PARAMETER.
; COMMON BLOCKS:
;       NONE.
; SIDE EFFECTS:
;       NONE.
; RESTRICTIONS:
;       NONE.
; PROCEDURE:
;       F = A(0)*EXP(-Z^2/2) + A(3) + A(4)*X + A(5)*X^2
;       Z = (X-A(1))/A(2)
;       Elements beyond A(2) are optional.
; MODIFICATION HISTORY:
;       WRITTEN, DMS, RSI, SEPT, 1982.
;       Modified, DMS, Oct 1990.  Avoids divide by 0 if A(2) is 0.
;       Added to Gauss_fit, when the variable function name to
;               Curve_fit was implemented.  DMS, Nov, 1990.
;
        n = n_elements(a)
        ON_ERROR,2                      ;Return to caller if an error occurs
        if a(2) ne 0.0 then begin
            Z = (X-A(1))/A(2)   ;GET Z
            EZ = EXP(-Z^2/2.)   ;GAUSSIAN PART
        endif else begin
            z = 100.
            ez = 0.0
        endelse

        case n of
3:      F = A(0)*EZ
4:      F = A(0)*EZ + A(3)
5:      F = A(0)*EZ + A(3) + A(4)*X
6:      F = A(0)*EZ + A(3) + A(4)*X + A(5)*X^2 ;FUNCTIONS.
        ENDCASE

        IF N_PARAMS(0) LE 3 THEN RETURN ;NEED PARTIAL?
;
        PDER = FLTARR(N_ELEMENTS(X),n) ;YES, MAKE ARRAY.
        PDER(*,0) = EZ          ;COMPUTE PARTIALS
        if a(2) ne 0. then PDER(*,1) = A(0) * EZ * Z/A(2)
        PDER(*,2) = PDER(*,1) * Z
        if n gt 3 then PDER(*,3) = 1.
        if n gt 4 then PDER(*,4) = X
        if n gt 5 then PDER(*,5) = X^2
        RETURN
END



Function MOD_Gaussfit, x, y, a, NTERMS=nt, ESTIMATES = est
;+
; NAME:
;       MOD_GAUSSFIT
;
; PURPOSE:
;       Fit the equation y=f(x) where:
;
;               F(x) = A0*EXP(-z^2/2) + A3 + A4*x + A5*x^2
;                       and
;               z=(x-A1)/A2
;
;       A0 = height of exp, A1 = center of exp, A2 = sigma (the width).
;       A3 = constant term, A4 = linear term, A5 = quadratic term.
;       Terms A3, A4, and A5 are optional.
;       The parameters A0, A1, A2, A3 are estimated and then CURVEFIT is 
;       called.
;
; CATEGORY:
;       ?? - fitting
;
; CALLING SEQUENCE:
;       Result = GAUSSFIT(X, Y [, A])
;
; INPUTS:
;       X:      The independent variable.  X must be a vector.
;       Y:      The dependent variable.  Y must have the same number of points
;               as X.
; KEYWORD INPUTS:
; KEYWORD INPUTS:
;       ESTIMATES = optional starting estimates for the parameters of the 
;               equation.  Should contain NTERMS (6 if NTERMS is not
;               provided) elements.
;       NTERMS = Set NTERMS to 3 to compute the fit: F(x) = A0*EXP(-z^2/2).
;          Set it to 4 to fit:  F(x) = A0*EXP(-z^2/2) + A3
;          Set it to 5 to fit:  F(x) = A0*EXP(-z^2/2) + A3 + A4*x
;
; OUTPUTS:
;       The fitted function is returned.
;
; OPTIONAL OUTPUT PARAMETERS:
;       A:      The coefficients of the fit.  A is a three to six
;               element vector as described under PURPOSE.
;
; COMMON BLOCKS:
;       None.
;
; SIDE EFFECTS:
;       None.
;
; RESTRICTIONS:
;       The peak or minimum of the Gaussian must be the largest
;       or smallest point in the Y vector.
;
; PROCEDURE:
;       The initial estimates are either calculated by the below procedure
;       or passed in by the caller.  Then the function CURVEFIT is called
;       to find the least-square fit of the gaussian to the data.
;
;  Initial estimate calculation:
;       If the (MAX-AVG) of Y is larger than (AVG-MIN) then it is assumed
;       that the line is an emission line, otherwise it is assumed there
;       is an absorbtion line.  The estimated center is the MAX or MIN
;       element.  The height is (MAX-AVG) or (AVG-MIN) respectively.
;       The width is found by searching out from the extrema until
;       a point is found less than the 1/e value.
;
; MODIFICATION HISTORY:
;       DMS, RSI, Dec, 1983.
;       DMS, RSI, Jun, 1995, Added NTERMS keyword.  Result is now float if 
;                               Y is not double.
;       DMS, RSI, Added ESTIMATES keyword.
;-
;
on_error,2                      ;Return to caller if an error occurs
csave = !c
if n_elements(nt) eq 0 then nt = 6
if nt lt 3 or nt gt 6 then $
   message,'NTERMS must have values from 3 to 6.'
n = n_elements(y)               ;# of points.
s = size(y)

if n_elements(est) eq 0 then begin      ;Compute estimates?
    c = poly_fit(x,y,1,yf)              ;Fit a straight line
    yd = y - yf
    if s(s(0)+1) ne 5 then begin        ;If Y is not double, use float
    yd = float(yd) & c = float(c) & endif

    ymax=max(yd) & xmax=x(!c) & imax=!c ;x,y and subscript of extrema
    ymin=min(yd) & xmin=x(!c) & imin=!c

    if abs(ymax) gt abs(ymin) then i0=imax else i0=imin ;emiss or absorp?
    i0 = i0 > 1 < (n-2)         ;never take edges
    dy=yd(i0)                   ;diff between extreme and mean
    del = dy/exp(1.)            ;1/e value
    i=0
    while ((i0+i+1) lt n) and $ ;guess at 1/2 width.
    ((i0-i) gt 0) and $
    (abs(yd(i0+i)) gt abs(del)) and $
    (abs(yd(i0-i)) gt abs(del)) do i=i+1
    a = [yd(i0), x(i0), abs(x(i0)-x(i0+i))]
    if nt gt 3 then a = [a, c(0)]       ;estimates
    if nt gt 4 then a = [a, c(1)]
    if nt gt 5 then a = [a, 0.]
endif else begin
    if nt ne n_elements(est) then message, 'ESTIMATES must have NTERM elements'
    a = est
endelse

!c=csave                        ;reset cursor for plotting
return,mod_curvefit(x,y,replicate(1.,n),a,sigmaa, $
                function_name = "MOD_GAUSS_FUNCT") ;call curvefit
end



PRO getfit, x,y, aa

  on_error, 2
      
  aa(1) = 0.0
  aa(2) = 0.0
    
  if total(y) ne 0.0 then begin

    yfit = mod_gaussfit(x,y,aa,nterms=4)
     
  endif
  
  
end
