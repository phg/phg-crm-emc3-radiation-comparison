; Copyright (c) 1999, Strathclyde University.
; SCCS info: Module @(#)$Header: /home/adascvs/idl/adas2xx/adas216/adas216_text_sub.pro,v 1.1 2004/07/06 10:26:28 whitefor Exp $ Date $Date: 2004/07/06 10:26:28 $
pro adas216_text_sub, title, num, def, gen, spf, errstr

  form_str = '(f7.2)'
  
  case title of
  
    'Excitation'      : pre =''
    'Recombination'   : pre ='+'
    'Charge Exchange' : pre ='+'
    'Ionisation'      : pre ='-'
  
  endcase

  if fix(total(num)) ge 1 then begin
     
     dash = ''
     for j=0,strlen(title)-1 do begin
       dash = dash+'-'
     endfor
     
     errstr = [errstr,title,dash,' ']
     
     if num(0) eq 1 then begin
        errstr = [errstr,'Defaults:',' ']
        record = '* - ' + pre + '* ' +                        $
                 string(def(0),format=form_str) 
        errstr = [errstr,record,' ']
     endif
  
     if num(1) ge 1 then begin
        errstr = [errstr,'General:',' ']
        for i=0,num(1)-1 do begin
          record = pre + string(gen(i,0),format='(i3)') + ' - * ' +  $
                   string(gen(i,1),format=form_str) 
          errstr = [errstr,record]
        endfor
        errstr = [errstr,' ']
     endif
     
     if num(2) ge 1 then begin
        errstr = [errstr,'Specific:',' ']
        for i=0,num(2)-1 do begin
          record = string(spf(i,0),format='(i3)') + ' - ' +          $ 
                   pre + string(spf(i,1),format='(i3)') +            $
                   string(spf(i,2),format=form_str) 
          errstr = [errstr,record]
        endfor
        errstr = [errstr,' ']
     endif

  endif

end
