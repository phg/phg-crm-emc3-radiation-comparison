; Copyright (c) 1999, Strathclyde University.
; SCCS info: Module @(#)$Header: /home/adascvs/idl/adas2xx/adas216/bgispf2.pro,v 1.1 2004/07/06 11:40:21 whitefor Exp $ Date $Date: 2004/07/06 11:40:21 $
;+
; PROJECT:
;       ADAS 
;
; NAME:
;	BGISPF2
;
; PURPOSE:
;	IDL user interface and communications with ADAS216 FORTRAN
;	process via pipe.
;
; EXPLANATION:
;	The routine begins by reading information from the ADAS216
;	FORTRAN process via a UNIX pipe.  Then part of the ADAS216
;	IDL user interface is invoked to determine how the user
;	wishes to process the input dataset.  When the user's
;	interactions are complete the information gathered with
;	the user interface is written back to the FORTRAN process
;	via the pipe.  Communications are to the FORTRAN subroutine
;	BGISPF.
;
; USE:
;	The use of this routine is specific to ADAS216, see adas216.pro.
;
; INPUTS:
;	PIPE	- The IDL unit number of the bi-directional pipe to the
;		  ADAS216 FORTRAN process.
;
;	LPEND	- Integer, 0 if the user pressed the 'Done' button
;		  or 1 if the user pressed 'Cancel'.  Maps directly onto
;		  the logical variable LPEND in ADAS216 FORTRAN.
;
;	VALUE	- A structure which determines the initial settings of
;		  the processing options widget.  The initial value is
;		  set in adas216.pro.  If adas216.pro passes a blank 
;		  dummy structure of the form {NMET:-1} into VALUE then
;		  VALUE is reset to a default structure.
;
;		  The VALUE structure is;
;		     {  TITLE:'',	NMET:0, 	$
;			IMETR:intarr(),	IFOUT:0, 	$
;			MAXT:0,		TINE:dblarr(), 	$
;			TINP:dblarr(),	TINH:dblarr(), 	$
;			IDOUT:1,	MAXD:0, 	$
;			MAXRP:0,        MAXRM:0,       	$
;			DINE:dblarr(),	DINP:dblarr(), 	$
;			RATHA:dblarr(),	RATPIA:dblarr(),$
;                       RATMIA:dblarr(,4), IMROUT:0, 	$
;			ZEFF:'',	LPSEL:0, 	$
;			LZSEL:0,	LISEL:0, 	$
;			LHSEL:0,	LRSEL:0,	$
;			LIOSEL:0,       LNSEL:0,        $
;			LNORM:0,		 	$
;	                VAL502:val502  }
;
;		  See cw_adas216_proc.pro for a full description of this
;		  structure and related variables ndmet, ndtem and ndden.
;
;	SZ	- String, recombined ion charge read.
;
;	SZ0	- String, nuclear charge read.
;
;	STRGA	- String array, level designations.
;
;	DSNINC	- String; The full system file name of the input COPASE
;		  dataset selected by the user for processing.
;
;	GOMENU  - Integer; 1=> menu button pressed
;			   0=> not pressed
;	BITFILE - String; file where bimap for menu button is held
;	INVAL 	- Structure; the structure returned by the input screen widget
;	NPL	- No. of metastables (see adas216.f)
;	STRGMI	- String; information written in bgsetp.f
;	STRGMF	- String; information written in bgsetp.f
;	NDMET	- Number of metastables
;
; OPTIONAL INPUTS:
;	None
;
; OUTPUTS:
;	VALUE	- On output the structure records the final settings of
;		  the processing selections widget if the user pressed the
;		  'Done' button, otherwise it is not changed from input.
;
; OPTIONAL OUTPUTS:
;	None
;
; KEYWORD PARAMETERS:
;	FONT_LARGE - The name of a larger font.
;
;	FONT_SMALL - The name of a smaller font.
;
;	EDIT_FONTS - A structure of two fonts used in the table editor
;		     adas_edtab.pro, {font_norm:'',font_input:''}.
;
; CALLS:
;	BGRWER     - read/writes error info from/to fortran.
;
; SIDE EFFECTS:
;	Two way communications with ADAS216 FORTRAN via the
;	bi-directional UNIX pipe.
;
; CATEGORY:
;	Adas system.
;
; WRITTEN:
;       Martin O'Mullane
;
; MODIFIED:
;	1.1	Martin O'Mullane
;		First release 
;
; VERSION:
;       1.1	17-03-99
;
;-
;-----------------------------------------------------------------------------


PRO bgispf2, pipe, lpend, value, 					$
		sz, sz0, selem,  					$
                strga, tev, dens, iunit_t, iunit_d,  			$
		dsninc, gomenu, bitfile, devlist, devcode, dumpfile, 	$
                userroot,						$
		FONT_LARGE=font_large, FONT_SMALL=font_small, 		$
		EDIT_FONTS=edit_fonts

                ;**** Set defaults for keywords ****
                
  IF NOT (KEYWORD_SET(font_large)) THEN font_large = ''
  IF NOT (KEYWORD_SET(font_small)) THEN font_small = ''
  IF NOT (KEYWORD_SET(edit_fonts)) THEN $
                                edit_fonts = {font_norm:'',font_input:''}


		;**** Declare variables for input ****
  idum  = 0
  ddum  = 0.0d0
  sdum  = ''

		;********************************
		;**** Read data from fortran ****
		;********************************
                

  readf, pipe, ddum                 ; settings for cumulative analysis
  umin = ddum
  readf, pipe, ddum
  umax = ddum
  readf, pipe, ddum
  ubin = ddum
  readf, pipe, idum
  iumax = idum                      ; max y of the monitored transition
                                    ; set to 100 (in fortran) at moment.
  
  readf, pipe, idum                 ; number of transitions for explore
  itran = idum                      ; error analysis                      
  readf, pipe, idum
  nstr = idum                       ; max number of transitions to inspect 
  
  
		;**** set up structure for update plot (cumerr) ****
  
  nb   = fix((umax-umin)/ubin) + 1 
  xp   = findgen(nb)
  xp   = (xp+1.0)*ubin + (umin-ubin)
  yp   = make_array(nb,/int,value=0)

  ust  = { ust216,               $
           umin       :   umin,  $         ; ust = Update STructure
   	   umax       :   umax,  $
   	   ubin       :   ubin,  $
   	   nb         :   nb,    $
   	   xp         :   xp,    $
   	   yp         :   yp,    $
   	   iumax      :   iumax, $
           ifirstgo   :   0      }
  
  
      

		;*******************************************
		;**** Set default value if not provided ****
		;*******************************************
          
  if value.new lt 0 then begin
            
    value = {	new        :   0,      $
                title	   :   '',     $
                analysis   :   0,      $
                level_ind  :   1,      $
                tev_ind    :   0,      $
                dens_ind   :   0,      $
                numiterset :   300,    $
                ust        :   ust,    $
                iexpgo     :   0,      $
                numtran    :   itran,  $
                nstr       :   nstr,   $
                level_exp  :   1,      $
                type_exp   :   0,      $
                mont_exp   :   0,      $
                mond_exp   :   0,      $
                sign_err   :   0       }

  endif

		;**** Set ifirstgo to 0 and zero yp display ****
		;**** vector at this stage always.          ****

  value.ust.ifirstgo = 0
  value.ust.yp       = yp
  value.iexpgo       = 0

                
                ;**** send name of dump file and current ****
                ;**** analysis method to fortran         ****
      
  printf, pipe, dumpfile
  printf, pipe, value.analysis
      

		;****************************************
		;**** Pop-up processing input widget ****
		;****************************************



  adas216_errorproc, value, sz, sz0, selem,				$
                     strga, tev, dens, iunit_t, iunit_d,		$
                     dsninc, dumpfile, bitfile, devlist, devcode,	$
                     pipe, action,					$
		     FONT_LARGE=font_large, FONT_SMALL=font_small, 	$
		     EDIT_FONTS=edit_fonts

		;******************************************
		;**** Act on the event from the widget ****
		;**** process cancel/done/menu options ****
		;******************************************
   
  if action eq 'Done' then begin
    
    lpend = 0
    printf, pipe, lpend

  end else if action eq 'Menu' then begin
  
    lpend = 2
    gomenu  = 1
    printf, pipe, lpend
    goto, LABELEND
    
  endif else begin
  
    lpend = 1
    printf, pipe, lpend
    goto, LABELEND
    
  end

		;*******************************
		;**** Write data to fortran ****
		;*******************************
                
  printf,pipe,value.title


LABELEND:

END
