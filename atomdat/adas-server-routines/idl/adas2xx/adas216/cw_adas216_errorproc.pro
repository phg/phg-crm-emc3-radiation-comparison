; Copyright (c) 1999, Strathclyde University.
; SCCS info: Module @(#)$Header: /home/adascvs/idl/adas2xx/adas216/cw_adas216_errorproc.pro,v 1.2 2004/07/06 12:36:04 whitefor Exp $ Date $Date: 2004/07/06 12:36:04 $
;+
; PROJECT:
;       ADAS 
;
; NAME:
;	CW_ADAS216_ERRORPROC()
;
; PURPOSE:
;	Produces a widget for ADAS216 processing options/input.
;
; EXPLANATION:
;	This function creates a compound widget consisting of a text
;	widget holding an editable 'Run title', the dataset name/browse
;	widget cw_adas_dsbr, label widgets with charge information for
;	the input data, a table widget for temperature data cw_adas_table,
;	a second cw_adas_table widget for density data, buttons to
;	enter default values into the temperature and density tables,
;	a multiple selection widget cw_adas_sel, a reaction settings
;	widget cw_adas_reac2, a message widget, a 'Cancel' button and
;	finally a 'Done' button.
;
;	The compound widgets included in this widget are self managing.
;	This widget only manages events from the two 'Defaults' buttons,
;	the 'Cancel' button and the 'Done' button.
;
;	This widget only generates events for the 'Done' and 'Cancel'
;	buttons.
;
; USE:
;	This widget is specific to ADAS216, see adas216_errorproc.pro
;	for use.
;
; INPUTS:
;	TOPPARENT- Long integer, ID of parent widget.
;
;	SZ	- String, recombined ion charge read.
;
;	SZ0	- String, nuclear charge read.
;
;	STRGA	- String array, level designations.
;
;	DSNINC	- String; The full system file name of the input COPASE
;		  dataset selected by the user for processing.
;
;	NDTEM	- Integer; Maximum number of temperatures allowed.
;
;	NDDEN	- Integer; Maximum number of densities allowed.
;
;	NDMET	- Integer; Maximum number of metastables allowed.
;
;	IL	- Integer; Number of energy levels.
;
;	NV	- Integer; Number of termperatures.
;
;	TSCEF	- dblarr(8,3); Input electron temperatures in three units.
;
;	The inputs SZ to TSCEF map exactly onto variables of the same
;	name in the ADAS216 FORTRAN program.
;
;       BITFILE - String; the path to the directory containing bitmaps
;                 for the 'escape to series menu' button.
;
;	INVAL	- Structure; the input screen structure - see cw_adas216_in.pro
;
;	SELEM	- String; selected element symbol
;
; OPTIONAL INPUTS:
;	None
;
; OUTPUTS:
;	The return value of this function is the ID of the declared widget.
;
; OPTIONAL OUTPUTS:
;	None
;
; KEYWORD PARAMETERS:
;	VALUE	- A structure which determines the initial settings of
;		  the processing options widget.
;
;		  The default VALUE is created thus;
;
;			imetr = make_array(ndmet,/int,value=0)
;			temp = dblarr(ndtem)
;			dens = dblarr(ndden)
;			ps = { $
;		 	TITLE:'',	NMET:0, $
;			IMETR:imetr,	IFOUT:1, $
;			MAXT:0,		TINE:temp, $
;			TINP:temp,	TINH:temp, $
;			IDOUT:1,	MAXD:0, $
;			DINE:dens,	DINP:dens, $
;			RATHA:dens,	RATPIA:dens, $
;			ZEFF:'',	LPSEL:0, $
;			LZSEL:0,	LISEL:0, $
;			LHSEL:0,	LRSEL:0, $
;                       LIOSEL:0,       LNSEL:0, $
;			LNORM:0,		 $
;			VAL502:struct502,	 $
;			WVLS:0.0,	WVLL:0.0,$
;			AVLT:0.0  }
;
; 		TITLE	entered general title for program run
; 		NMET	number of metastables
; 		(IMETR(I),I=1,NMET)	index of metastables
; 		IFOUT	input temperature units (1,2,3)
; 		MAXT	number of input temperatures (1-20)
; 		(TINE(I),I=1,MAXT)	electron temperatures
; 		(TINP(I),I=1,MAXT)	proton temperatures
; 		(TINH(I),I=1,MAXT)	neutral hydrogen temperatures
; 		IDOUT	input density units (1,2)
; 		MAXD	number of input densities
; 		(DINE(I),I=1,MAXD)	electron densities
; 		(DINP(I),I=1,MAXD)	proton densities
; 		(RATHA(I),I=1,MAXD)	ratio (neut h dens/elec dens)
; 		(RATPIA(I),I=1,MAXD)	ratio (n(z+1)/n(z) stage abund)
; 		ZEFF	plasma z effective, a string holding a valid number.
; 		LPSEL	include proton collisions?
; 		LZSEL	scale proton collisions with plasma z effective?
; 		LISEL	include ionisation rates?
; 		LHSEL	include charge transfer from neutral hydrogen?
; 		LRSEL	include free electron recombination?
;               LIOSEL to be determined
;               LNSEL   include projected bundle-n data?
;
;		All of the above structure elements map onto variables of
;		the same name in the ADAS216 FORTRAN program.
;
;
;	UVALUE	- A user value for the widget. Default 0.
;
;	FONT_LARGE - The name of a larger font.  Default current system
;		     font
;
;	FONT_SMALL - The name of a smaller font. Default current system
;		     font.
;
;	EDIT_FONTS - A structure of two fonts used in the table editor
;		     adas_edtab.pro, {font_norm:'',font_input:''}.  Defaults
;		     to current system font.
;
;	NUM_FORM   - String; Numeric format to use in tables.  Default
;			'(E10.3)'
;		     
;
; CALLS:
;	POPUP		 Popup warning window with buttons.
;	XXDEFT		 Returns default temperature table.
;	XXDEFD		 Returns default density table.
;	CW_ADAS_DSBR	 Dataset name and comments browsing button.
;	CW_ADAS_TABLE	 Adas data table widget.
;	CW_ADAS_SEL	 Adas mulitple selection widget.
;	CW_ADAS208_REAC2 Reaction settings for ADAS216.
;
;	See side effects for other related widget management routines.
;
; SIDE EFFECTS:
;
;	The following widget management routines are included in this
;	file;
;	ERRORPROC216_GET_VAL()  Returns the current VALUE structure.
;	ERRORPROC216_EVENT()            Process and issue events.
;
; CATEGORY:
;	Compound Widget
;
; WRITTEN:
;       Martin O'Mullane
;
; MODIFIED:
;	1.1	Martin O'Mullane
;		First release;
;	1.2	Richard Martin
;		Changed widget name switch to switchb for IDL 5.4 compatibility. 
;
; VERSION:
;       1.1	17-03-99
;	1.2	10-11-2000
;
;-
;-----------------------------------------------------------------------------

FUNCTION errorproc216_get_val, id


                ;**** Return to caller on error ****
  ON_ERROR, 2

		 ;***************************************
                 ;****     Retrieve the state 	     ****
		 ;**** Get first_child widget id     ****
		 ;**** because state is stored there ****
		 ;***************************************

  first_child = widget_info(id, /child)
  widget_control, first_child, get_uvalue = state


		;***********************************
		;**** Get run title from widget ****
		;***********************************
                
  widget_control,state.runid,get_value=title


		;************************************
		;**** Get type of error analysis ****
		;************************************
                
  widget_control,state.analysisid,get_value=errset
  analysis = errset

		;**************************************
		;**** Get cumulative error results ****
		;**************************************
                
  widget_control,state.cumid,get_value=cum_err


		;*********************************** 
		;**** Get explore error results ****
		;*********************************** 
                
  widget_control,state.expid,get_value=exp_err


		;***********************************************
		;**** write selected values to PS structure ****
		;***********************************************

  ps = {errorproc216_set, 				$
        new             :       0,                      $
        title           :       title(0),               $
        analysis	:       analysis,               $
        level_ind       :       cum_err.level_ind,  	$
        tev_ind         :       cum_err.tev_ind,    	$
        dens_ind        :       cum_err.dens_ind,   	$
        numiterset      :       cum_err.numiterset,     $
        ust             :       cum_err.ust,       	$
        iexpgo          :       exp_err.iexpgo,         $
        numtran         :       exp_err.numtran,        $
        nstr            :       exp_err.nstr,           $
        level_exp       :       exp_err.level_exp,      $
        type_exp        :       exp_err.type_exp,       $
        mont_exp        :       exp_err.mont_exp,       $
        mond_exp        :       exp_err.mond_exp,       $
        sign_err        :       exp_err.sign_err        }

  widget_control, first_child, set_uvalue=state, /no_copy

  RETURN, ps

END

;-----------------------------------------------------------------------------

FUNCTION errorproc216_event, event

                ;**** Base ID of compound widget ****
                
   parent=event.handler

		;**********************************************
                ;**** Retrieve the user value state        ****
		;**** Get id of first_child widget because ****
		;**** user value "state" is stored there   ****
		;**********************************************

   first_child = widget_info(parent, /child)
   widget_control, first_child, get_uvalue=state,/no_copy


   
		;*********************************
		;**** Clear previous messages ****
		;*********************************
                
  widget_control,state.messid, $
    set_value='                                                              '


                ;************************
                ;**** Process events ****
                ;************************
                
  CASE event.id OF


		;****************************************
                ;**** type of errors analysis button ****
		;****************************************
                
    state.analysisid: begin
    
      widget_control, state.analysisid, get_value = bpress
      state.analysis = bpress
      
      case bpress of
    
        0 : begin 
              widget_control, state.cumbase_id, map=1
              widget_control, state.expbase_id, map=0
              widget_control, state.cumid, sensitive=1
              widget_control, state.expid, sensitive=0
            end

        1 : begin 
              widget_control, state.cumbase_id, map=0
              widget_control, state.expbase_id, map=1
              widget_control, state.cumid, sensitive=0
              widget_control, state.expid, sensitive=1
            end
        
      endcase

      
    end


		;***********************
		;**** Cancel button ****
		;***********************
                
    state.cancelid: new_event = {ID:parent, TOP:event.top, $
				HANDLER:0L, ACTION:'Cancel'}


		;*********************
		;**** Done button ****
		;*********************
                
    state.doneid: begin
    
		;********************************************
		;**** Check for at least one calculation ****
		;********************************************
                
	error = 0
        
		;**** restore state to check values ****

	widget_control, first_child, set_uvalue=state, /no_copy
	widget_control, event.handler, get_value=ps
	widget_control, first_child, get_uvalue=state, /no_copy
        
       if (ps.analysis eq 0) and (ps.ust.ifirstgo eq 0) then begin
          error=1
          message='An iteration must be performed before proceeding - Press GO'
       endif
       
       
       if (ps.analysis eq 1) and (ps.iexpgo eq 0) then begin
          error=1
          message='Run the exploration - Press GO'
       endif
		
                ;**** return value or flag error ****
                
	if error eq 0 then begin
	  new_event = {ID:parent, TOP:event.top, HANDLER:0L, ACTION:'Done'}
        end else begin
	  widget_control,state.messid,set_value=message
	  new_event = 0L
        end


   end
                ;**** Menu button ****
                
    state.outid: begin
      new_event = {ID:parent, TOP:event.top, HANDLER:0L, ACTION:'Menu'}
    end



                ;**** Switching base buttons ****
                

    ELSE: new_event = 0L
    
    

  ENDCASE
  
		;****************************
		;*** make state available ***
		;****************************

    widget_control, first_child, set_uvalue=state, /no_copy

  RETURN, new_event
  
END

;-----------------------------------------------------------------------------

FUNCTION cw_adas216_errorproc, topparent, 				$
		sz, sz0, selem,    					$
                strga, tev, dens, iunit_t, iunit_d, 			$ 
                dsninc,  dumpfile, bitfile, devlist, devcode, pipe,	$
		VALUE=value, UVALUE=uvalue, 				$
		FONT_LARGE=font_large, FONT_SMALL=font_small, 		$
		EDIT_FONTS=edit_fonts


		;**** Set defaults for keywords ****
                
  IF NOT (KEYWORD_SET(uvalue))     THEN uvalue = 0
  IF NOT (KEYWORD_SET(font_large)) THEN font_large = ''
  IF NOT (KEYWORD_SET(font_small)) THEN font_small = ''
  IF NOT (KEYWORD_SET(edit_fonts)) THEN $
				   edit_fonts = {font_norm:'',font_input:''}


  IF NOT (KEYWORD_SET(value)) THEN begin
	message,'A structure must be passed'
  END ELSE BEGIN
	ps = {errorproc216_set,	                          $
                        new         :   value.new,        $
		        title       :   value.title,      $
                        analysis    :   value.analysis,   $
                        level_ind   :   value.level_ind,  $
                        tev_ind     :   value.tev_ind,    $
                        dens_ind    :   value.dens_ind,   $
                        numiterset  :   value.numiterset, $
                        ust         :   value.ust,        $
                        iexpgo      :   value.iexpgo,     $
                        numtran     :   value.numtran,    $
                        nstr        :   value.nstr,       $
                        level_exp   :   value.level_exp,  $
                        type_exp    :   value.type_exp,   $
                        mont_exp    :   value.mont_exp,   $
                        mond_exp    :   value.mond_exp,   $
                        sign_err    :   value.sign_err    }
  END

  il = where(strga eq ' *** END OF LEVELS *** ')    ; note this is an array
  il = il(0)
  if ps.level_ind eq 0 or ps.level_ind gt il then ps.level_ind = 1 
  ; note that we don't set it to the ground value


                ;*********************************************************
                ;**** Modify certain parameters and results depending ****
                ;**** on the machine being used                       ****
                ;*********************************************************

    machine = GETENV('TARGET_MACHINE')
    if machine eq 'HPUX' then begin
        y_size = 4
        large_font = font_small
    endif else begin
        y_size = 6
        large_font = font_large
    endelse




		;****************************************************
		;**** Create the Processing options/input window ****
		;****************************************************

		;**** create titled base widget ****
                
  parent = widget_base(topparent, UVALUE = uvalue, 		$
		       EVENT_FUNC     = "errorproc216_event", 	$
		       FUNC_GET_VALUE = "errorproc216_get_val", $
		       /COLUMN)

		;******************************************************
		;**** Create a dummy widget just to hold value of *****
		;**** "state" variable so as not to get confused  *****
		;**** with any other values. Adopt IDL practice   *****
		;**** of using first child widget                 *****
		;******************************************************

  first_child = widget_base(parent)
  topbase = widget_base(first_child,/column)


		;***********************
		;**** add run title ****
		;***********************
                
  base  = widget_base(topbase,/row)
  rc    = widget_label(base,value='Title for Run',font=font_large)
  runid = widget_text(base,value=ps.title,xsize=40,font=font_large,/edit)

		;**** add dataset name and browse button ****
                
  rc = cw_adas_dsbr(topbase,dsninc,font=font_large,/row)

		;**** add charge information ****
                
  rc = widget_label(base,font=font_large, $
			value='Nuclear Charge:'+sz0+'  Ion Charge:'+sz)


	
      
		;**********************************
                ;**** Error Analysis selection ****
		;**********************************
                  
  base_a = widget_base(topbase,/column,/align_left)

  analysis_names = ['Cumulative Error ',  $
                    'Explore Errors '    ]
  analysisid = cw_bgroup(base_a,analysis_names,exclusive=1,row=1,		$
                       label_left = 'Select type of Error Analysis :',	$
                       /no_release,  font=font_large)
                     
  
  
  
		;****************************************
                ;**** Base for error analysis choice ****
		;****************************************
 
  switchb     = widget_base(base_a)
  cumbase_id = widget_base(switchb,/row)
  expbase_id = widget_base(switchb,/row)


                ;**** Cumulative Error Analysis ****
 
  cval = {level_ind   :  ps.level_ind,   $
          tev_ind     :  ps.tev_ind,     $
          dens_ind    :  ps.dens_ind,    $
          numiterset  :  ps.numiterset,  $
          ust         :  ps.ust          }
          
  cumid = cw_adas216_cumerr(cumbase_id,					$
                strga, tev, dens, iunit_t, iunit_d, 			$ 
                dumpfile, bitfile, devlist, devcode, pipe,	        $
		VALUE=cval, UVALUE=uvalue, 				$
		FONT_LARGE=font_large, FONT_SMALL=font_small, 		$
		EDIT_FONTS=edit_fonts)


                ;**** Explore Error Analysis ****
                
                
  eval = {iexpgo      :  ps.iexpgo,      $
          numtran     :  ps.numtran,     $
          nstr        :  ps.nstr,        $
          level_exp   :  ps.level_exp,   $
          type_exp    :  ps.type_exp,    $
          mont_exp    :  ps.mont_exp,    $
          mond_exp    :  ps.mond_exp,    $
          sign_err    :  ps.sign_err     }
          
  expid = cw_adas216_experr(expbase_id,					$
                strga, tev, dens, iunit_t, iunit_d, 			$ 
                dumpfile, bitfile, devlist, devcode, pipe,	        $
		VALUE=eval, UVALUE=uvalue, 				$
		FONT_LARGE=font_large, FONT_SMALL=font_small, 		$
		EDIT_FONTS=edit_fonts)

                

		;***********************
		;**** Error message ****
		;***********************
                
  messid = widget_label(parent,font=font_large, $
	value='                                                            ')




		;**********************************
		;**** Finally the exit buttons ****
		;**********************************
                
  base     = widget_base(parent,/row)
  menufile = bitfile + '/menu.bmp'
  read_X11_bitmap, menufile, bitmap1
  outid    = widget_button(base, value = bitmap1)
  cancelid = widget_button(base,value='Cancel',font=font_large)
  doneid   = widget_button(base,value='Done',font=font_large)
  
  
  
		;**************************
		;**** Initial settings ****
		;**************************
                
  widget_control, analysisid, set_value = ps.analysis
  
  case ps.analysis of
    
    0 : begin 
          widget_control, cumbase_id, map=1
          widget_control, expbase_id, map=0
          widget_control, cumid, sensitive=1
          widget_control, expid, sensitive=0
        end
  
    1 : begin 
          widget_control, cumbase_id, map=0
          widget_control, expbase_id, map=1
          widget_control, cumid, sensitive=0
          widget_control, expid, sensitive=1
        end
        
  endcase
  
  
   
		;*************************************************
		;**** create a state structure for the pop-up ****
		;**** window.                                 ****
		;*************************************************
                
  new_state = { runid           :       runid,          $
	        analysisid      :       analysisid,     $
	        analysis        :       ps.analysis,    $
                cumid           :       cumid,          $
                expid           :       expid,          $
                cumbase_id      :       cumbase_id,     $
                expbase_id      :       expbase_id,     $
                outid           :       outid,          $
                cancelid        :       cancelid,       $
                doneid          :       doneid,         $
	        messid          :       messid          }

                ;**** Save initial state structure ****

    widget_control, first_child, set_uvalue=new_state, /no_copy

  RETURN, parent

END
