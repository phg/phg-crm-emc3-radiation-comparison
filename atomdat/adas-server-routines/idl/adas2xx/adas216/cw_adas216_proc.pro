; Copyright (c) 1999, Strathclyde University.
; SCCS info: Module @(#)$Header: /home/adascvs/idl/adas2xx/adas216/cw_adas216_proc.pro,v 1.1 2004/07/06 12:36:18 whitefor Exp $ Date $Date: 2004/07/06 12:36:18 $
;+
; PROJECT:
;       ADAS 
;
; NAME:
;	CW_ADAS216_PROC()
;
; PURPOSE:
;	Produces a widget for ADAS216 processing options/input.
;
; EXPLANATION:
;	This function creates a compound widget consisting of a text
;	widget holding an editable 'Run title', the dataset name/browse
;	widget cw_adas_dsbr, label widgets with charge information for
;	the input data, a table widget for temperature data cw_adas_table,
;	a second cw_adas_table widget for density data, buttons to
;	enter default values into the temperature and density tables,
;	a multiple selection widget cw_adas_sel, a reaction settings
;	widget cw_adas_reac2, a message widget, a 'Cancel' button and
;	finally a 'Done' button.
;
;	The compound widgets included in this widget are self managing.
;	This widget only manages events from the two 'Defaults' buttons,
;	the 'Cancel' button and the 'Done' button.
;
;	This widget only generates events for the 'Done' and 'Cancel'
;	buttons.
;
; USE:
;	This widget is specific to ADAS216, see adas216_proc.pro
;	for use.
;
; INPUTS:
;	TOPPARENT- Long integer, ID of parent widget.
;
;	SZ	- String, recombined ion charge read.
;
;	SZ0	- String, nuclear charge read.
;
;	STRGA	- String array, level designations.
;
;	DSNINC	- String; The full system file name of the input COPASE
;		  dataset selected by the user for processing.
;
;	NDTEM	- Integer; Maximum number of temperatures allowed.
;
;	NDDEN	- Integer; Maximum number of densities allowed.
;
;	NDMET	- Integer; Maximum number of metastables allowed.
;
;	IL	- Integer; Number of energy levels.
;
;	NV	- Integer; Number of termperatures.
;
;	TSCEF	- dblarr(8,3); Input electron temperatures in three units.
;
;	The inputs SZ to TSCEF map exactly onto variables of the same
;	name in the ADAS216 FORTRAN program.
;
;       BITFILE - String; the path to the directory containing bitmaps
;                 for the 'escape to series menu' button.
;
;	INVAL	- Structure; the input screen structure - see cw_adas216_in.pro
;
;	SELEM	- String; selected element symbol
;
; OPTIONAL INPUTS:
;	None
;
; OUTPUTS:
;	The return value of this function is the ID of the declared widget.
;
; OPTIONAL OUTPUTS:
;	None
;
; KEYWORD PARAMETERS:
;	VALUE	- A structure which determines the initial settings of
;		  the processing options widget.
;
;		  The default VALUE is created thus;
;
;			imetr = make_array(ndmet,/int,value=0)
;			temp = dblarr(ndtem)
;			dens = dblarr(ndden)
;			ps = { $
;		 	TITLE:'',	NMET:0, $
;			IMETR:imetr,	IFOUT:1, $
;			MAXT:0,		TINE:temp, $
;			TINP:temp,	TINH:temp, $
;			IDOUT:1,	MAXD:0, $
;			DINE:dens,	DINP:dens, $
;			RATHA:dens,	RATPIA:dens, $
;			ZEFF:'',	LPSEL:0, $
;			LZSEL:0,	LISEL:0, $
;			LHSEL:0,	LRSEL:0, $
;                       LIOSEL:0,       LNSEL:0, $
;			LNORM:0,		 $
;			VAL502:struct502,	 $
;			WVLS:0.0,	WVLL:0.0,$
;			AVLT:0.0  }
;
; 		TITLE	entered general title for program run
; 		NMET	number of metastables
; 		(IMETR(I),I=1,NMET)	index of metastables
; 		IFOUT	input temperature units (1,2,3)
; 		MAXT	number of input temperatures (1-20)
; 		(TINE(I),I=1,MAXT)	electron temperatures
; 		(TINP(I),I=1,MAXT)	proton temperatures
; 		(TINH(I),I=1,MAXT)	neutral hydrogen temperatures
; 		IDOUT	input density units (1,2)
; 		MAXD	number of input densities
; 		(DINE(I),I=1,MAXD)	electron densities
; 		(DINP(I),I=1,MAXD)	proton densities
; 		(RATHA(I),I=1,MAXD)	ratio (neut h dens/elec dens)
; 		(RATPIA(I),I=1,MAXD)	ratio (n(z+1)/n(z) stage abund)
; 		ZEFF	plasma z effective, a string holding a valid number.
; 		LPSEL	include proton collisions?
; 		LZSEL	scale proton collisions with plasma z effective?
; 		LISEL	include ionisation rates?
; 		LHSEL	include charge transfer from neutral hydrogen?
; 		LRSEL	include free electron recombination?
;               LIOSEL to be determined
;               LNSEL   include projected bundle-n data?
;
;		All of the above structure elements map onto variables of
;		the same name in the ADAS216 FORTRAN program.
;
;
;	UVALUE	- A user value for the widget. Default 0.
;
;	FONT_LARGE - The name of a larger font.  Default current system
;		     font
;
;	FONT_SMALL - The name of a smaller font. Default current system
;		     font.
;
;	EDIT_FONTS - A structure of two fonts used in the table editor
;		     adas_edtab.pro, {font_norm:'',font_input:''}.  Defaults
;		     to current system font.
;
;	NUM_FORM   - String; Numeric format to use in tables.  Default
;			'(E10.3)'
;		     
;
; CALLS:
;	POPUP		 Popup warning window with buttons.
;	XXDEFT		 Returns default temperature table.
;	XXDEFD		 Returns default density table.
;	CW_ADAS_DSBR	 Dataset name and comments browsing button.
;	CW_ADAS_TABLE	 Adas data table widget.
;	CW_ADAS_SEL	 Adas mulitple selection widget.
;	CW_ADAS208_REAC2 Reaction settings for ADAS216.
;
;	See side effects for other related widget management routines.
;
; SIDE EFFECTS:
;
;	The following widget management routines are included in this
;	file;
;	PROC216_GET_VAL()	Returns the current VALUE structure.
;	PROC216_EVENT()		Process and issue events.
;
; CATEGORY:
;	Compound Widget
;
; WRITTEN:
;       Martin O'Mullane
;
; MODIFIED:
;	1.1	Martin O'Mullane
;		First release, based on cw_adas208_proc.pro
;
; VERSION:
;       1.1	17-03-99
;
;-
;-----------------------------------------------------------------------------

FUNCTION proc216_get_val, id


                ;**** Return to caller on error ****
  ON_ERROR, 2

		 ;***************************************
                 ;****     Retrieve the state 	     ****
		 ;**** Get first_child widget id     ****
		 ;**** because state is stored there ****
		 ;***************************************

  first_child = widget_info(id, /child)
  widget_control, first_child, get_uvalue = state


		;***********************************
		;**** Get run title from widget ****
		;***********************************
                
  widget_control,state.runid,get_value=title

		;**************************************************
		;**** Get new selections from selection widget ****
		;**************************************************
                
  widget_control,state.selid,get_value=selected
  
		;****************************************************
		;**** selected is IDL index, from 0. imetr needs ****
		;**** to be fortran index, from 1.               ****
		;****************************************************
                
  imetr = selected + 1
  sels  = where(imetr gt 0, count)
  if count ne 0 then begin
    selsize = size(sels)
    nmet    = selsize(1)
  end else begin
    nmet = 0
  end

		;****************************************************
		;**** Get new temperature data from table widget ****
		;****************************************************
                
  widget_control,state.tempid,get_value=tempval
  tempdata = tempval.value
  ifout    = tempval.units+1
  
		;**** Copy out temperature values ****
                
  blanks = where(strtrim(tempdata(0,*),2) eq '')
  if blanks(0) ge 0 then maxt=blanks(0) else maxt=state.ndtem
  tine = dblarr(state.ndtem)
  tinp = dblarr(state.ndtem)
  tinh = dblarr(state.ndtem)
  if maxt gt 0 then begin
    tine(0:maxt-1) = double(tempdata(0,0:maxt-1))
    tinp(0:maxt-1) = double(tempdata(1,0:maxt-1))
    tinh(0:maxt-1) = double(tempdata(2,0:maxt-1))
  end

		;**** Fill out the rest with zero ****
                
  if maxt lt state.ndtem then begin
    tine(maxt:state.ndtem-1) = double(0.0)
    tinp(maxt:state.ndtem-1) = double(0.0)
    tinh(maxt:state.ndtem-1) = double(0.0)
  end

		;******************************************************
		;**** Get new density/ratio data from table widget ****
		;******************************************************

                ;**** switching table one ****
                
  widget_control,state.densid1,get_value=densval1
  densdata1 = densval1.value
  idout     = densval1.units+1

                ;**** switching table one ****
                
  widget_control,state.densid2,get_value=densval2
  densdata2 = densval2.value

                ;**** switching table one ****
                
  widget_control,state.densid3,get_value=densval3
  densdata3 = densval3.value

		;**** Copy out densities/ratios ****

  blanks1 = where(strtrim(densdata1(0,*),2) eq '')
  blanks2 = where(strtrim(densdata2(0,*),2) eq '')
  blanks3 = where(strtrim(densdata3(0,*),2) eq '')
  if blanks1(0) ge 0 then maxd=blanks1(0) else maxd=state.ndden
  if blanks2(0) ge 0 then maxrp=blanks2(0) else maxrp=state.ndden
  if blanks3(0) ge 0 then maxrm=blanks3(0) else maxrm=state.ndden

  dine   = dblarr(state.ndden)
  dinp   = dblarr(state.ndden)
  ratha  = dblarr(state.ndden)
  ratpia = dblarr(state.ndden,4)
  ratmia = dblarr(state.ndden,4)

  if maxd gt 0 then begin
    dine(0:maxd-1) = double(densdata1(0,0:maxd-1))
    dinp(0:maxd-1) = double(densdata1(1,0:maxd-1))
    ratha(0:maxd-1) = double(densdata1(2,0:maxd-1))
  end
  if maxrp gt 0 then begin
    ratpia(0:maxrp-1,0) = double(densdata2(0,0:maxrp-1))
    ratpia(0:maxrp-1,1) = double(densdata2(1,0:maxrp-1))
    ratpia(0:maxrp-1,2) = double(densdata2(2,0:maxrp-1))
    ratpia(0:maxrp-1,3) = double(densdata2(3,0:maxrp-1))
  end
  if maxrm gt 0 then begin
    ratmia(0:maxrm-1,0) = double(densdata3(0,0:maxrm-1))
    ratmia(0:maxrm-1,1) = double(densdata3(1,0:maxrm-1))
    ratmia(0:maxrm-1,2) = double(densdata3(2,0:maxrm-1))
    ratmia(0:maxrm-1,3) = double(densdata3(3,0:maxrm-1))
  end

		;**** Fill out the rest with zeros ****
                
  if maxd lt state.ndden then begin
    dine(maxd:state.ndden-1)  = double(0.0)
    dinp(maxd:state.ndden-1)  = double(0.0)
    ratha(maxd:state.ndden-1) = double(0.0)
  end
  if maxrp lt state.ndden then begin
    ratpia(maxrp:state.ndden-1,0) = double(0.0)
    ratpia(maxrp:state.ndden-1,1) = double(0.0)
    ratpia(maxrp:state.ndden-1,2) = double(0.0)
    ratpia(maxrp:state.ndden-1,3) = double(0.0)
  end
  if maxrm lt state.ndden then begin
    ratmia(maxrm:state.ndden-1,0) = double(0.0)
    ratmia(maxrm:state.ndden-1,1) = double(0.0)
    ratmia(maxrm:state.ndden-1,2) = double(0.0)
    ratmia(maxrm:state.ndden-1,3) = double(0.0)
  end

		;*************************************************
		;**** Get new reaction selections from widget ****
		;*************************************************
                
  widget_control,state.reactid,get_value=reacset

  zeff   = 0    ;not used at present
  lzsel  = 0
  
  liosel = reacset(0)
  lhsel  = reacset(1)
  lrsel  = reacset(2)
  lisel  = reacset(3)
  lnsel  = reacset(4)
  lpsel  = reacset(5)

		;***************************************
		;**** Get source of error - errtype ****
		;***************************************
                
  widget_control,state.errid,get_value=errset
  errtype = errset


		;***********************************************
		;**** write selected values to PS structure ****
		;***********************************************

  ps = {proc216_set, 					$
        new             :       0,             	  	$
        title           :       title(0),               $
        nmet            :       nmet,                   $
        imetr           :       imetr,                  $
        ifout           :       ifout,                  $
        maxt            :       maxt,                   $
        tine            :       tine,                   $
        tinp            :       tinp,                   $
        tinh            :       tinh,                   $
        idout           :       idout,                  $
        maxd            :       maxd,                   $
        maxrp           :       maxrp,                  $
        maxrm           :       maxrm,                  $
        dine            :       dine,                   $
        dinp            :       dinp,                   $
        ratha           :       ratha,                  $
        ratpia          :       ratpia,                 $
        ratmia          :       ratmia,                 $
        imrout          :       state.ps.imrout,        $
        zeff            :       zeff,                   $
        lpsel           :       lpsel,                  $
        lzsel           :       lzsel,                  $
        lisel           :       lisel,                  $
        lhsel           :       lhsel,                  $
        lrsel           :       lrsel,                  $
        liosel          :       liosel,                 $
        lnsel           :       lnsel,                  $
        errval          :       state.viewerr,		$
        errtype	        :       errtype                 }

    widget_control, first_child, set_uvalue=state, /no_copy

  RETURN, ps

END

;-----------------------------------------------------------------------------

FUNCTION proc216_event, event

                ;**** Base ID of compound widget ****
                
   parent=event.handler

		;**********************************************
                ;**** Retrieve the user value state        ****
		;**** Get id of first_child widget because ****
		;**** user value "state" is stored there   ****
		;**********************************************

   first_child = widget_info(parent, /child)
   widget_control, first_child, get_uvalue=state,/no_copy


		;*********************************
		;**** Clear previous messages ****
		;*********************************
                
  widget_control,state.messid,set_value=' '

                ;************************
                ;**** Process events ****
                ;************************
                
  CASE event.id OF

		;*************************************
		;**** Default temperatures button ****
		;*************************************
                
    state.deftid: begin

		;**** popup window to confirm overwriting current values ****
                
	action= popup(message='Confirm Overwrite Temperatures with Defaults', $
			buttons=['Confirm','Cancel'],font=state.font)

	if action eq 'Confirm' then begin
        
		;**** Get current table widget value ****
                
 	  widget_control,state.tempid,get_value=tempval

		;**** Get the default temperatures and set new units ****
                
 	  xxdeft, state.ndtem, maxt, deft
	  tempval.units = 2

		;**** Copy defaults into value structure ****
                
 	  if maxt gt 0 then begin
   	    tempval.value(0,0:maxt-1) = $
		strtrim(string(deft(0:maxt-1),format=state.num_form),2)
   	    tempval.value(1,0:maxt-1) = $
		strtrim(string(deft(0:maxt-1),format=state.num_form),2)
   	    tempval.value(2,0:maxt-1) = $
		strtrim(string(deft(0:maxt-1),format=state.num_form),2)
 	  endif

		;**** Fill out the rest with blanks ****
                
 	  if maxt lt state.ndtem then begin
   	    tempval.value(0:2,maxt:state.ndtem-1) = ''
 	  endif

		;**** Copy new temperature data to table widget ****
                
 	  widget_control,state.tempid,set_value=tempval
          
	endif

      end

		;**********************************
		;**** Default densities button ****
		;**********************************
                
    state.defdid: begin

		;**** popup window to confirm overwriting current values ****
                
	action = popup(message='Confirm Overwrite Densities/Ratios with Defaults', $
			buttons=['Confirm','Cancel'],font=state.font)

	if action eq 'Confirm' then begin
        
		;**** Get current table widget value ****
                
 	  widget_control,state.densid1,get_value=densval1
 	  widget_control,state.densid2,get_value=densval2
 	  widget_control,state.densid3,get_value=densval3

		;**** Get the default densities and set new units ****
                
 	  xxdefd, state.ndden, maxd, defd
	  densval1.units = 1

		;**** Copy defaults into value structure ****
                
 	  if maxd gt 0 then begin
   	    densval1.value(0,0:maxd-1) = $
		strtrim(string(defd(0:maxd-1),format=state.num_form),2)
   	    densval1.value(1:2,0:maxd-1) = '0.0000'
 	  end

                ;**** Copy default ratios into value structure. ****
                ;**** No. of ratios currently set to maxd  i.e. ****
                ;**** the same as the no. of density defaults   ****
                
          maxrp = maxd
          maxrm = maxd

          if maxrp gt 0 then begin
   	    densval2.value(0,0:maxrp-1) = '1.0000'
   	    densval2.value(1:3,0:maxrp-1) = '0.0000'
          endif
          if maxrm gt 0 then begin
   	    densval3.value(0,0:maxrm-1) = '1.0000'
   	    densval3.value(1:3,0:maxrm-1) = '0.0000'
          endif

		;**** Fill out the rest with blanks ****
 	  if maxd lt state.ndden then begin
   	    densval1.value(*,maxd:state.ndden-1) = ''
 	  endif
          if maxrp lt state.ndden then begin
   	    densval2.value(*,maxrp:state.ndden-1) = ''
          endif
          if maxrm lt state.ndden then begin
   	    densval3.value(*,maxrm:state.ndden-1) = ''
          endif

		;**** Copy new density/ratio data to table widget ****
                
 	  widget_control,state.densid1,set_value=densval1
 	  widget_control,state.densid2,set_value=densval2
 	  widget_control,state.densid3,set_value=densval3

	endif

      end
      
		;************************************
		;**** View errors in file button ****
		;************************************
                
    state.fileid: adas216_text, state.fileerr, font=state.font
      
		;************************************
		;**** View file integrity report ****
		;************************************
                
    state.reportid: begin
              
           def_name = 'adas216_integrity.txt'
           adas_text, state.reporterr,  def_name, font = state.font, $
                      xsize = 80, wintitle = 'ADAS216 Statistics'   
    
    end
		
                
                ;************************************
		;**** View current errors button ****
		;************************************
                
    state.viewid: adas216_text, state.viewerr, font=state.font

                ;**** source of errors button ****
                
    state.errid:begin
      widget_control, state.errid, get_value = bpress
      state.err_source = bpress
    end

		;***********************
		;**** Cancel button ****
		;***********************
                
    state.cancelid: new_event = {ID:parent, TOP:event.top, $
				HANDLER:0L, ACTION:'Cancel'}


		;*********************
		;**** Done button ****
		;*********************
                
    state.doneid: begin
    
		;***************************************
		;**** Check all user input is legal ****
		;***************************************
                
	error = 0
        
                ;*************************************************
                ;*** Have to restore state before call to     ****
                ;*** proc216_get_val so it can be used there. ****
                ;*************************************************
                
        widget_control, first_child, set_uvalue=state, /no_copy
	
        widget_control,event.handler,get_value=ps

                ;*** reset state variable ***

        widget_control, first_child, get_uvalue=state, /no_copy
            
            
		;******************************************
		;**** now we can begin to check input. ****
		;******************************************
                
		;**** check metastable states selected ****
                
	if error eq 0 and ps.nmet eq 0 then begin
	  error = 1
	  message='Error: No Metastable States selected.'
        end

		;**** check temperatures entered ****
                
	if error eq 0 and ps.maxt eq 0 then begin
	  error = 1
	  message='Error: No temperatures entered.'
        end

		;**** check densities entered ****
                
	if error eq 0 and ps.maxd eq 0 then begin
	  error = 1
	  message='Error: No densities entered.'
        end

		;**** check zeff value ****
                
;	if error eq 0 and ps.lzsel eq 1 then begin
;	  error = num_chk(ps.zeff)
;	  if error gt 0 then message='Error: Z-effective should be a number.'
;        end

		;**** return value or flag error ****
                
	if error eq 0 then begin
	  new_event = {ID:parent, TOP:event.top, HANDLER:0L, ACTION:'Done'}
        end else begin
	  widget_control,state.messid,set_value=message
	  new_event = 0L
        end

      end
                ;**** Menu button ****
                
    state.outid: begin
      new_event = {ID:parent, TOP:event.top, HANDLER:0L, ACTION:'Menu'}
    end



                ;**** Switching base buttons ****
                
    state.ienhid:begin
      widget_control, state.izpdid, set_button = 0
      widget_control, state.izmdid, set_button = 0
      widget_control, state.col1, map = 1
      widget_control, state.col2, map = 0
      widget_control, state.col3, map = 0
      state.ps.imrout = 0
    end
    state.izpdid:begin
      widget_control, state.ienhid, set_button = 0
      widget_control, state.izmdid, set_button = 0
      widget_control, state.col1, map = 0
      widget_control, state.col2, map = 1
      widget_control, state.col3, map = 0
      state.ps.imrout = 1
    end
    state.izmdid:begin
      widget_control, state.ienhid, set_button = 0
      widget_control, state.izpdid, set_button = 0
      widget_control, state.col1, map = 0
      widget_control, state.col2, map = 0
      widget_control, state.col3, map = 1
      state.ps.imrout = 2
    end
    

    ELSE: new_event = 0L
    
    

  ENDCASE
  
		;*********************************************
		;*** make "state" available to other files ***
		;*********************************************

    widget_control, first_child, set_uvalue=state, /no_copy

  RETURN, new_event
  
END

;-----------------------------------------------------------------------------

FUNCTION cw_adas216_proc, topparent, 					$
		sz, sz0, strga, dsninc, ndtem, ndden, ndmet, il, nv, 	$
                tscef, bitfile, inval, selem, fileerr, reporterr,	$
		VALUE=value, UVALUE=uvalue, 				$
		FONT_LARGE=font_large, FONT_SMALL=font_small, 		$
		EDIT_FONTS=edit_fonts, NUM_FORM=num_form


		;**** Set defaults for keywords ****
                
  IF NOT (KEYWORD_SET(uvalue)) THEN uvalue = 0
  IF NOT (KEYWORD_SET(font_large)) THEN font_large = ''
  IF NOT (KEYWORD_SET(font_small)) THEN font_small = ''
  IF NOT (KEYWORD_SET(edit_fonts)) THEN $
				edit_fonts = {font_norm:'',font_input:''}
  IF NOT (KEYWORD_SET(num_form)) THEN num_form = '(E10.3)'

  IF NOT (KEYWORD_SET(value)) THEN begin
	message,'A structure must be passed'
  END ELSE BEGIN
	ps = {proc216_set,	                          $
		        new       :       value.new,      $
		        title     :       value.title,    $
                        nmet      :       value.nmet,     $
		        imetr     :       value.imetr,    $
                        ifout     :       value.ifout,    $
		        maxt      :       value.maxt,     $
                        tine      :       value.tine,     $
		        tinp      :       value.tinp,     $
                        tinh      :       value.tinh,     $
		        idout     :       value.idout,    $
                        maxd      :       value.maxd,     $
		        maxrp     :       value.maxrp,    $
                        maxrm     :       value.maxrm,    $
		        dine      :       value.dine,     $
                        dinp      :       value.dinp,     $
		        ratha     :       value.ratha,    $
                        ratpia    :       value.ratpia,   $
		        ratmia    :       value.ratmia,   $
                        imrout    :       value.imrout,   $
		        zeff      :       value.zeff,     $
                        lpsel     :       value.lpsel,    $
		        lzsel     :       value.lzsel,    $
                        lisel     :       value.lisel,    $
		        lhsel     :       value.lhsel,    $
                        lrsel     :       value.lrsel,    $
		        liosel    :       value.liosel,   $
                        lnsel     :       value.lnsel,    $
                        errval    :       value.errval,   $
                        errtype   :       value.errtype   }
  END


                ;**** set up reaction list ****

  react_list    = intarr(6)
  react_list(0) = ps.liosel
  react_list(1) = ps.lhsel
  react_list(2) = ps.lrsel
  react_list(3) = ps.lisel
  react_list(4) = ps.lnsel
  react_list(5) = ps.lpsel


                ;*********************************************************
                ;**** Modify certain parameters and results depending ****
                ;**** on the machine being used                       ****
                ;*********************************************************

    machine = GETENV('TARGET_MACHINE')
    if machine eq 'HPUX' then begin
        y_size = 4
        large_font = font_small
    endif else begin
        y_size = 6
        large_font = font_large
    endelse


		;***************************************************
		;****      Assemble temperature table data      ****
		;***************************************************
		;**** The adas table widget requires data to be ****
		;**** input as strings, so all the numeric data ****
		;**** has to be written into a string array.    ****
		;**** Declare temp table array                  ****
		;**** Three editable columns and input values   ****
		;**** in three different units.                 ****
		;***************************************************
                
  tempdata = strarr(6,ndtem)

		;**** Copy out temperature values ****
                
  if ps.maxt gt 0 then begin
    tempdata(0,0:ps.maxt-1) = $
		strtrim(string(ps.tine(0:ps.maxt-1),format=num_form),2)
    tempdata(1,0:ps.maxt-1) = $
		strtrim(string(ps.tinp(0:ps.maxt-1),format=num_form),2)
    tempdata(2,0:ps.maxt-1) = $
		strtrim(string(ps.tinh(0:ps.maxt-1),format=num_form),2)
  end
		;**** input values in three units ****
                
  tempdata(3,0:nv-1) = strtrim(string(tscef(0:nv-1,0),format=num_form),2)
  tempdata(4,0:nv-1) = strtrim(string(tscef(0:nv-1,1),format=num_form),2)
  tempdata(5,0:nv-1) = strtrim(string(tscef(0:nv-1,2),format=num_form),2)

		;**** Fill out the rest with blanks ****
                
  if ps.maxt lt ndtem then begin
    tempdata(0:2,ps.maxt:ndtem-1) = ''
  endif
  tempdata(3:5,nv:ndtem-1) = ''

		;***************************************************
		;****        Assemble density table data        ****
		;***************************************************
		;**** The adas table widget requires data to be ****
		;**** input as strings, so all the numeric data ****
		;**** has to be written into a string array.    ****
		;***************************************************
                
		;**** Declare dens table array ****
                
  densdata1 = strarr(3,ndden)
  densdata2 = strarr(4,ndden)
  densdata3 = strarr(4,ndden)

		;**** Copy out density values ****
                
  if ps.maxd gt 0 then begin
  
    densdata1(0,0:ps.maxd-1) = $
		strtrim(string(ps.dine(0:ps.maxd-1),format=num_form),2)
    densdata1(1,0:ps.maxd-1) = $
		strtrim(string(ps.dinp(0:ps.maxd-1),format=num_form),2)
    densdata1(2,0:ps.maxd-1) = $
		strtrim(string(ps.ratha(0:ps.maxd-1),format=num_form),2)
    densdata2(0,0:ps.maxd-1) = $
		strtrim(string(ps.ratpia(0:ps.maxd-1,0),format=num_form),2)
    densdata2(1,0:ps.maxd-1) = $
		strtrim(string(ps.ratpia(0:ps.maxd-1,1),format=num_form),2)
    densdata2(2,0:ps.maxd-1) = $
		strtrim(string(ps.ratpia(0:ps.maxd-1,2),format=num_form),2)
    densdata2(3,0:ps.maxd-1) = $
		strtrim(string(ps.ratpia(0:ps.maxd-1,3),format=num_form),2)
    densdata3(0,0:ps.maxd-1) = $
		strtrim(string(ps.ratmia(0:ps.maxd-1,0),format=num_form),2)
    densdata3(1,0:ps.maxd-1) = $
		strtrim(string(ps.ratmia(0:ps.maxd-1,1),format=num_form),2)
    densdata3(2,0:ps.maxd-1) = $
		strtrim(string(ps.ratmia(0:ps.maxd-1,2),format=num_form),2)
    densdata3(3,0:ps.maxd-1) = $
		strtrim(string(ps.ratmia(0:ps.maxd-1,3),format=num_form),2)
                
  endif

		;**** Fill out the rest with blanks ****
                
  if ps.maxd lt ndden then begin
    densdata1(*,ps.maxd:ndden-1) = ''
    densdata2(*,ps.maxd:ndden-1) = ''
    densdata3(*,ps.maxd:ndden-1) = ''
  endif


		;********************************************************
		;**** Create the 216 Processing options/input window ****
		;********************************************************

		;**** create titled base widget ****
                
  parent = widget_base(topparent, UVALUE = uvalue, 		$
		       EVENT_FUNC = "proc216_event", 		$
		       FUNC_GET_VALUE = "proc216_get_val", 	$
		       /COLUMN)

		;******************************************************
		;**** Create a dummy widget just to hold value of *****
		;**** "state" variable so as not to get confused  *****
		;**** with any other values. Adopt IDL practice   *****
		;**** of using first child widget                 *****
		;******************************************************

  first_child = widget_base(parent)
  topbase = widget_base(first_child,/column)


		;*****************************
		;**** Information options ****
		;*****************************
                
  base  = widget_base(topbase,/row)
  rc    = widget_label(base,value='Title for Run',font=font_large)
  runid = widget_text(base,value=ps.title,xsize=40,font=font_large,/edit)

		;**** add dataset name and browse button ****
                
  tbase  = widget_base(topbase,/row)
  rc     = cw_adas_dsbr(tbase,dsninc,font=font_large,/row)

		;**** buttons to browse errors and to test ****

  fileid = widget_button(tbase,value='View errors in file',   $
                         font=font_large,/align_center)
  
  reportid = widget_button(tbase,value='File integrity',   $
                         font=font_large,/align_center)

		;**** add charge information ****
                
  rc = widget_label(base,font=font_large, $
			value='Nuclear Charge:'+sz0+'  Ion Charge:'+sz)





		;*************************************************
		;**** Base for temperature and density tables ****
		;*************************************************
                
  tablebase = widget_base(parent,/row)

		;**** base for the temperature table and defaults button ****
                
  base = widget_base(tablebase,/column,/frame)

                ;**** a row for the option switches ****
                
  row1 = widget_base(base,/row)
  row2 = widget_base(base,/row)
  col1 = widget_base(row2,/column)

                ;**** Two spaces to keep the widget looking even ****
                
  sp1 = widget_label(row1,value = '          ')
  sp2 = widget_label(col1,value = '          ')

		;**** Temperature data table ****
                
  colhead = [['Electron','Ion','Neutral','Input'], $
	     ['','','Hydrogen','Value']]
             
		;**** convert FORTRAN index to IDL index ****
                
  units = ps.ifout-1
  unitname = ['Kelvin','eV','Reduced']
  
		;**** Four columns in the table and three sets of units. ****
		;**** Columns 1,2 & 3 have the same values for all three ****
		;**** units but column 4 switches between sets 3, 4 & 5 ****
		;**** in the input array tempdata as the units change.   ****
                
  unitind = [[0,0,0],[1,1,1],[2,2,2],[3,4,5]]
  tempid = cw_adas_table(col1, tempdata, units, unitname, unitind, $
			UNITSTITLE = 'Temperature Units', /scroll, $
			COLEDIT = [1,1,1,0], COLHEAD = colhead, $
			TITLE = 'Temperatures', ORDER = [1,0,0,0], $
			LIMITS = [2,2,2,2], CELLSIZE = 12, $
			FONTS = edit_fonts, FONT_LARGE = font_large, $
			FONT_SMALL = font_small)

		;**** Default temperatures button ****
                
  base = widget_base(base,/column)
  deftid = widget_button(base,value='Default Temperatures',font=font_large)



		;********************************************************
		;**** base for the density table and defaults button ****
		;********************************************************
                
  base = widget_base(tablebase,/column,/frame)

                ;**** a row for the option switches ****
                
  row1  = widget_base(base,/row)
  row1l = widget_base(row1,/row)
  row1b = widget_base(row1,/row,/exclusive)
  row2  = widget_base(base,/row)

                ;**** add the switching base and build on it ****
                
  sw_base = widget_base(row2)
  col1    = widget_base(sw_base,/column)
  col2    = widget_base(sw_base,/column)
  col3    = widget_base(sw_base,/column)

                ;**** the options switches ****
                
  inpid  = widget_label(row1l, value = 'Input densities:', $
                        font = font_large)
  ienhid = widget_button(row1b, value = 'NE & NH', $
                         /no_release, font = font_large)
  izpdid = widget_button(row1b, value = 'N(z+1)', $
                         /no_release, font = font_large)
  izmdid = widget_button(row1b, value = 'N(z-1)', $
                         /no_release, font = font_large)

		;**** density data table one ****
                
  colhead1 = [['Electron', 'Ion',      'NH/NE'], $
	     ['Densities','Densities','Ratio']]
             
		;**** convert FORTRAN index to IDL index ****
                
  units = ps.idout-1
  unitname = ['cm-3','Reduced']
  
		;**** Three columns in the table and two sets of units.    ****
		;**** The values do not change when the units are switched ****
                
  unitind = [[0,0],[1,1],[2,2]]
  densid1 = cw_adas_table(col1, densdata1, units, unitname, unitind, $
			UNITSTITLE = 'Density Units', $
			COLHEAD = colhead1, /scroll, $
			TITLE = 'Densities', ORDER = [1,0,0], $
			LIMITS = [2,1,1], CELLSIZE = 12, $
			FONTS = edit_fonts, FONT_LARGE = font_large, $
			FONT_SMALL = font_small)

		;**** Default densities button ****
                
  base = widget_base(base,/column)
  defdid = widget_button(base,value='Default Densities/Ratios',font=font_large)

		;**** Density data table two ****
                
  colhead2 = [['Met 1', 'Met 2','Met 3','Met 4'], $
	     ['Ratio','Ratio','Ratio','Ratio']]

  densid2 = cw_adas_table(col2, densdata2, $
			COLHEAD = colhead2, /scroll, $
			TITLE = 'N(Z+1,M)/N(Z,1)', ORDER = [0,0,0,0], $
			LIMITS = [1,1,1,1], CELLSIZE = 12, $
			FONTS = edit_fonts, FONT_LARGE = font_large, $
			FONT_SMALL = font_small)

		;**** Density data table three ****
                
  colhead3 = [['Met 1', 'Met 2','Met 3','Met 4'], $
	     ['Ratio','Ratio','Ratio','Ratio']]

  densid3 = cw_adas_table(col3, densdata3, $
			COLHEAD = colhead3, /scroll, $
			TITLE = 'N(Z-1,M)/N(Z,1)', ORDER = [0,0,0,0], $
			LIMITS = [1,1,1,1], CELLSIZE = 12, $
			FONTS = edit_fonts, FONT_LARGE = font_large, $
			FONT_SMALL = font_small)

  botbase = widget_base(parent,/row)
  base = widget_base(botbase,/row,/frame)

                ;**** map the base to the screen ****    
                ;**** taken from defaults file   ****

  if ps.imrout eq 0 then begin
    widget_control, col1, map = 1
    widget_control, col2, map = 0
    widget_control, col3, map = 0
    widget_control, ienhid, set_button = 1
    widget_control, izpdid, set_button = 0
    widget_control, izmdid, set_button = 0
  end else if ps.imrout eq 1 then begin
    widget_control, col1, map = 0
    widget_control, col2, map = 1
    widget_control, col3, map = 0
    widget_control, ienhid, set_button = 0
    widget_control, izpdid, set_button = 1
    widget_control, izmdid, set_button = 0
  end else begin
    widget_control, col1, map = 0
    widget_control, col2, map = 0
    widget_control, col3, map = 1
    widget_control, ienhid, set_button = 0
    widget_control, izpdid, set_button = 0
    widget_control, izmdid, set_button = 1
  end
  
  
 
 		;*****************************************
                ;**** Base for metastable, error and  **** 
                ;**** reaction selection.             ****
		;*****************************************

  base   = widget_base(parent,/row)


		;**************************************************
		;**** metastables selection list               ****
		;**** imetr is fortran index, from 1, selected ****
		;**** needs to be IDL index, from 0.	       ****
		;**************************************************
                
  base_m = widget_base(base,/frame)
                
  selected = ps.imetr - 1
  levend   = where(strtrim(strga,2) eq '*** END OF LEVELS ***')
  listsize = size(strga)
  if levend(0) ge 0 then listend=levend(0)-1 else listend=listsize(1)-1
;  selid = cw_adas_sel(base_m,strga(0:listend),SELECTED=selected, $
;			MAXSEL=ndmet, /ORDERED, TITLE='Metastable States :', $
;			YTEXSIZE=5,FONT=font_large)
 
  
  ; only allow ONE metastable state for now
  
  ndmetallow = 1
  selid = cw_adas_sel(base_m,strga(0:listend),SELECTED=selected, $
			MAXSEL=ndmetallow, /ORDERED, TITLE='Metastable States :', $
			YTEXSIZE=5,FONT=font_large)
        
  
		;************************************
                ;**** Reaction selection buttons ****
		;************************************
                                  

  base_r = widget_base(base,/column,/frame,/align_left)

  react_names = ['Ionisation Rates ',  $
                 'Charge Exchange  ',  $
                 'Recombination    ', $
                 'Inner Shell Ionisation  ',  $
                 'Include Projection Data ',  $
                 'Proton Impact Collisions']
  reactid = cw_bgroup(base_r,react_names,nonexclusive=1,column=2,		$
                     label_top='Select Reactions to be Included :',	$
                     font=font_large)


  widget_control, reactid, set_value = react_list
	
      
		;*************************
                ;**** Error selection ****
		;*************************
                  
  base_e = widget_base(base,/column,/frame,/align_left)

  err_names = ['Use errors in adf04 file   ',  $
               'Modify errors in adf04 file',  $
               'Define errors              ',  $
               'Use previous run settings  '   ]
  errid = cw_bgroup(base_e,err_names,exclusive=1,row=4,		$
                       label_top = 'Select source of errors :',	$
                       /no_release, font=font_large)
                     
  junk   = widget_label(base_e,value='    ')
  viewid = widget_button(base_e,value='View current errors set',font=font_large,xsize=50)
  
  widget_control, errid, set_value = ps.errtype
  
                       

		;***********************
		;**** Error message ****
		;***********************
                
  messid = widget_label(parent,font=font_large, $
	value='Edit the processing options data and press Done to proceed')



		;**********************************
		;**** Finally the exit buttons ****
		;**********************************
                
  base     = widget_base(parent,/row)
  menufile = bitfile + '/menu.bmp'
  read_X11_bitmap, menufile, bitmap1
  outid    = widget_button(base, value = bitmap1)
  cancelid = widget_button(base,value='Cancel',font=font_large)
  doneid   = widget_button(base,value='Done',font=font_large)
  
  
  
  
		;*************************************************
		;**** create a state structure for the pop-up ****
		;**** window.                                 ****
		;*************************************************
                
  new_state = { runid           :       runid,          $
                inval           :       inval,          $
                ps              :       ps,             $
                sz              :       sz,             $
                selem           :       selem,          $
	        selid           :       selid,          $
                tempid          :       tempid,         $
                densid1         :       densid1,        $
                densid2         :       densid2,        $
                densid3         :       densid3,        $
	        deftid          :       deftid,         $
                defdid          :       defdid,         $
                outid           :       outid,          $
	        reactid         :       reactid,        $
	        errid           :       errid,          $
	        err_source      :       ps.errtype,     $
                cancelid        :       cancelid,       $
                doneid          :       doneid,         $
	        messid          :       messid,         $
                num_form        :       num_form,       $
	        ndtem           :       ndtem,          $
                ndden           :       ndden,          $
                ndmet           :       ndmet,          $
	        il              :       il,             $
                nv              :       nv,             $
                tscef           :       tscef,          $
                font            :       font_large,     $
                ienhid          :       ienhid,         $
                izpdid          :       izpdid,         $
                izmdid          :       izmdid,         $
                col1            :       col1,           $
                col2            :       col2,           $
                col3            :       col3,           $
                viewid		:	viewid,		$
                viewerr		:	ps.errval,	$
                fileid		:	fileid,		$
                fileerr		:	fileerr,	$
                reportid	:	reportid,	$
                reporterr	:	reporterr	}

                ;**** Save initial state structure ****

    widget_control, first_child, set_uvalue=new_state, /no_copy

  RETURN, parent

END

