; Copyright (c) 1999, Strathclyde University.
; SCCS info: Module @(#)$Header: /home/adascvs/idl/adas2xx/adas216/bgout_gr.pro,v 1.1 2004/07/06 11:40:33 whitefor Exp $ Date $Date: 2004/07/06 11:40:33 $
;+
; PROJECT:
;       ADAS 
;
; NAME:
;	BGOUT_GR
;
; PURPOSE:
;	A popup window for displaying fits to iterated data. Slices
;       corresponding to temperature or density for a particular  level
;       can be viewed. The statistics of the fit can be viewed and
;       printed and/or the plot can be printed to a file.
;
; EXPLANATION:
;	This routine allows the user to navigate through the cumulative
;       error data. Slices of gaussian fits corresponding to a range of
;       temperatures or densities for a particular  level are plotted.
;       Only those Te/ne which have sufficient data to fit are plotted.
;       ie if the unperturbed population is 0.0  (ie too small) nothing
;       is shown. The data is read from a file. The size of the array
;       should be small enough for most systems to read into memory as it
;       is stored as integer*2. It is 4-D though as there are
;       numden*numte*numlev histograms each with numbin bins!
;     
;
; INPUTS:
;       VALUE - Structure holding necessary information.
;
;               numte    : number of temperatures  
;               numden   : number of densities 
;               numlev   : number of levels
;               numiter  : number of iterations
;               numbin   : number of bins for histograms
;               te       : temperatures  
;               den      : densities
;               slev     : levels (string)
;               iunit_t  : temperature units (integer) (K, eV, reduced)   
;               iunit_d  : density units (integer) (cm**-3, reduced)   
;               xhist    : x values of histogram
;               dumpfile : file where histograms are stored
;
; OPTIONAL INPUTS:
;	None.
;
; OUTPUTS:
;       The return value of this function is the ID of the compound
;       widget which is of type LONG integer.
;
; OPTIONAL OUTPUTS:
;       None
;
; KEYWORD PARAMETERS:
;	XSIZE	- Integer; x size in characters of browse widget.
;		  Default is 400.
;
;	YSIZE	- Integer; y size in characters of browse widget.
;		  Default is 340.
;
;	FONT	- A font to use for all text in this widget.
;
; CALLS:
;	ADAS_TEXT                to browse/print an array of strings
;       bgout_gr_cum_PLOT  plots to screen or file the fits 
;       ADAS_FILE_GR             popup widget to select a file for 
;                                graphics output. 
;
; SIDE EFFECTS:
;	One other routine is included which is used to manage the widget;
;
;	bgout_gr_cum_EVENT
;
; CATEGORY:
;	Widgets
;
; WRITTEN:
;       Martin O'Mullane
;
; MODIFIED:
;	1.1	Martin O'Mullane
;		First release 
;
; VERSION:
;       1.1	17-03-99
;
;-
;-----------------------------------------------------------------------------

PRO bgout_gr_event, event


 Widget_Control, event.top, Get_UValue=state, /no_copy
 Widget_Control, event.id,  Get_UValue=userEvent

 ; cannot do both yet!
 widget_control, state.ch_ids(2), sensitive=0

 
 ; All events are connected with plotting - set the structure on 
 ; entering the event handler. All fields are part of state so
 ; the values are stored there permanently. 
 
 widget_control, state.drawid, get_value = draw_win
 wset,  draw_win
 ;erase

 plot_stuff = {  te       : state.te,         $
                 den      : state.den,        $
                 s_l      : state.sp_l,       $
                 s_u      : state.sp_u,       $
                 title    : state.title,      $
                 subtitle : state.subtitle,   $ 
                 iunit_t  : state.iunit_t,    $
                 iunit_d  : state.iunit_d,    $
                 ax       : state.ax,         $
                 az       : state.az,         $
                 colour   : state.col_ind,    $
                 style    : state.sty_ind,    $
                 ichoice  : state.ichoice     }
               


CASE userEvent OF

            
  'LEVS'  :  begin
               
               widget_control, state.xrotid,   sensitive = 1
               widget_control, state.zrotid,   sensitive = 1
               widget_control, state.styleid,  sensitive = 1
               widget_control, state.colourid, sensitive = 1
               widget_control, state.choiceid, sensitive = 1
                              
               widget_control, state.printid, sensitive=0
               state.lev_ind  = event.index
               
               str = 'Level ' + string((1+ state.lev_ind ), format='(i5)') + $
                     '  : ' + state.slev(state.lev_ind)
               state.subtitle = str
               
               state.sp_l = state.surface_l(state.lev_ind,*,*)
               state.sp_u = state.surface_u(state.lev_ind,*,*)
               
               
               plot_stuff.s_l = reform(state.sp_l,state.numte,state.numden)  
               plot_stuff.s_u = reform(state.sp_u,state.numte,state.numden) 
               plot_stuff.subtitle = str
               bgout_gr_plot, state.updateid, value = plot_stuff
             
               widget_control, state.printid, sensitive=1
             
             end
             
  'XROT'   : begin

               widget_control, state.printid, sensitive=0
               
               widget_control, state.xrotid, get_value=ax
               state.ax = ax 
               
               plot_stuff.ax = state.ax
               bgout_gr_plot, state.updateid, value = plot_stuff
               
               widget_control, state.printid, sensitive=1
             
             end
	
  'ZROT'   : begin

               widget_control, state.printid, sensitive=0
               
               widget_control, state.zrotid, get_value=az
               state.az = az  
              
               plot_stuff.az = state.az
               bgout_gr_plot, state.updateid, value = plot_stuff
             
               widget_control, state.printid, sensitive=1
             
             end

  'STYLES' : begin
  
               widget_control, state.printid, sensitive=0
               state.sty_ind = event.index 
               
               plot_stuff.style = state.sty_ind
               bgout_gr_plot, state.updateid, value = plot_stuff
               
               widget_control, state.printid, sensitive=1
               
             end
  
  'COLOURS': begin
   
               cvec = [0,3,1,8]  ; B-W,Red Temp,Blue,Green
                
               state.col_ind = event.index 
               loadct, cvec(state.col_ind)
               
               plot_stuff.colour = state.col_ind
               bgout_gr_plot, state.updateid, value = plot_stuff

             end
  
  'CHOICE' : begin
            
               widget_control, state.choiceid, get_value = bpress
               state.ichoice = bpress
                 
               widget_control, state.printid, sensitive=0
               
               plot_stuff.ichoice = state.ichoice
               bgout_gr_plot, state.updateid, value = plot_stuff

               widget_control, state.printid, sensitive=1
               
             end

  
  'DONE'   : begin
  
               ; restore original colour table before leaving
               TVLCT, state.red, state.green, state.blue

               widget_control, event.top, /destroy
               
             end
  
  'PRINT'  : begin
	
               widget_control,state.updateid,set_value =  '                           '

               print_val = { dsn     : '',                  $
                             defname : 'adas216_3D.ps',     $
                             replace :  1,                  $
                             devlist :  state.devlist,      $
                             dev     :  state.devlist(0),   $
                             paper   :  'A4',               $
                             write   :  0,                  $
                             message :  'Note: cannot append to graphics files'}
               act = 0
               adas_file_gr, print_val, act, font=state.font, $
                             title='File name for graphical output'
               
               
               lev_ind = state.lev_ind
               
               
               print_stuff = { dsn     : print_val.dsn,   $
                               devcode : state.devcode,   $
                               devlist : state.devlist,   $
                               dev     : print_val.dev,   $
                               paper   : print_val.paper  }
                              
               if print_val.write eq 1 then begin               
                              
	          bgout_gr_plot, state.updateid,      $ 
                                 value = plot_stuff,  $
                                 print = print_stuff
                                 
                  widget_control, state.updateid, $
                                  set_value = 'Plot written to file'
                                  
               endif else begin
               
                  widget_control, state.updateid, $
                                  set_value = 'No file selected'
                                  
               endelse
               
             end
             
             
  else : print,'BGOUT_GR_EVENT : You should not see this message! ',userEvent
                   
ENDCASE

                ;*****************************************
		;**** Update state but only if we are ****
                ;**** NOT leaving the widget.         ****
                ;*****************************************

 if userEvent ne 'DONE' then $
    widget_control, event.top, set_uvalue=state, /no_copy

END

;-----------------------------------------------------------------------------


PRO bgout_gr, devlist, devcode, $
              VALUE=value, XSIZE = xsize, YSIZE = ysize, $
              FONT_LARGE = font_large, FONT_SMALL = font_small


		;**** Set defaults for keywords ****

  IF NOT (KEYWORD_SET(xsize)) THEN xsize = 400
  IF NOT (KEYWORD_SET(ysize)) THEN ysize = 340
  IF NOT (KEYWORD_SET(font_large))  THEN font_large = ''
  IF NOT (KEYWORD_SET(font_small))  THEN font_small = ''
  IF NOT (KEYWORD_SET(value)) THEN $  
          message,'A structure must be passed to BGOUT_GR_CUM'


  
  numte     = value.numte
  numden    = value.numden
  numlev    = value.numlev
 
  te        = value.te
  den       = value.den
  surface_l = value.surface_l
  surface_u = value.surface_u
  slev      = value.slev
 
  iunit_t   = value.iunit_t
  iunit_d   = value.iunit_d
 
  title     = value.title
  
 
  
                ;***********************************
		;**** create titled base widget ****
                ;***********************************
                
  parent = widget_base(title='3D Overview of Error Analysis', $
			xoffset=20,yoffset=20,/column)
                
  rc = widget_label(parent,value='  ',font=font_large)

                 
  
                ;*****************************************
                ;**** Draw widget to display results. ****
                ;*****************************************

  device, get_screen_size=scrsz
  xwidth  = scrsz(0)*0.50
  yheight = scrsz(1)*0.60


  drow   = widget_base(parent, /row, /frame)
  dcol   = widget_base(drow, /column, xpad=10,ypad=10)  

  ; leave some space before user's choice column
  
  skip   = widget_label(drow, value='  ', font=font_large)
  
 
  drawid = widget_draw(dcol, xsize=xwidth, ysize=yheight) 
 
 
                ;*******************************************************
		;**** User choices - level, colour, style, position ****
                ;*******************************************************

  mcol     = widget_base(drow,/column,/base_align_left)
     
  mlab     = widget_label(mcol, value='Choose options ', font=font_large)
  mlab     = widget_label(mcol, value='  ', font=font_large)
  mlab     = widget_label(mcol, value='Level: ', font=font_large)
  
  levs     = slev(0:numlev-1)
  levid    = widget_droplist(mcol, value=levs,/align_left,  $
                          font=font_small,  Uvalue='LEVS')



  mlab     = widget_label(mcol, value='  ', font=font_large)
  
  mlab     = widget_label(mcol, value='Graph Style: ', font=font_large)
  ocol     = widget_base(mcol,/column,/frame)
  

  xrotid   = widget_slider(ocol,title='X rotation', font=font_small,     $
                           minimum=-90, maximum=90, scroll=10, value=30, $
                           Uvalue='XROT')

  zrotid   = widget_slider(ocol,title='Z rotation', font=font_small,     $
                           minimum=-90, maximum=90, scroll=10, value=30, $
                           Uvalue='ZROT')

  styles   = ['Mesh   ','Surface']
  styleid  = widget_droplist(ocol, value=styles,/align_left,  $
                          font=font_small,  Uvalue='STYLES')
  
  ; correspond to loadct 0, 3, 1, 8
  colours   = ['B-W     ','Red Temp','Blue    ','Green   ']
  colourid  = widget_droplist(ocol, value=colours,/align_left,  $
                          font=font_small,  Uvalue='COLOURS')



 		;**************************************
		;**** Upper or lower error surface ****
		;**************************************
             
  mlab     = widget_label(mcol, value='  ', font=font_large)
  mlab     = widget_label(mcol, value='Error Surface: ', font=font_large)
             
  
  choiceid = cw_bgroup(mcol,['Lower','Upper','Both '],      $
                             exclusive=1,column=1,          $
                             label_left = '',/frame,        $
                             ids=ch_ids,                    $
                             /no_release,  Uvalue='CHOICE',font=font_small)
             
             
             
 		;***********************************
		;**** set up initial conditions ****
		;***********************************
 
  widget_control, levid,    set_droplist_select = 0
  widget_control, styleid,  set_droplist_select = 0
  widget_control, colourid, set_droplist_select = 0

  widget_control, xrotid,   sensitive = 0
  widget_control, zrotid,   sensitive = 0
  widget_control, styleid,  sensitive = 0
  widget_control, colourid, sensitive = 0
  widget_control, choiceid, sensitive = 0






                ;*****************
		;**** Buttons ****
                ;*****************
                
  base    = widget_base(parent,/row)
  doneid  = widget_button(base,value='Done',font=font_large,Uvalue='DONE')
  
  mlab   = widget_label(base, value='     ', font=font_large)

  printid = widget_button(base,value='Print',font=font_large,Uvalue='PRINT')
  
  

                ;**************************
		;**** Read data update ****
                ;**************************
  
  updateid = widget_label(base,value='                                      ',$
                          font=font_large,/align_right)
  

  
                ;**************************
		;**** initial settings ****
                ;**************************
  
  ; Only allow printing when there is a plot on the screen.
  ; If the level is changed before printing or
  ; then de-sensitise the print button immediately.
  ; In this way what is printed is the same as on the screen.
                
  widget_control, printid, sensitive=0            
   
  widget_control, choiceid, set_value = 0

  subtitle = 'Level     2  :' + slev(1)

  sp_l = surface_l(1,*,*)
  sp_u = surface_u(1,*,*)
  
                ;**************************************
		;**** Get the current colour table ****
                ;**************************************
                
  TVLCT, r,g,b,/get
  
               
               
                ;****************************************************
		;**** put all needed info into a state structure ****
                ;****************************************************
   
  state = {  devlist    :  devlist,     $
             devcode    :  devcode,     $
             numte      :  numte,       $
             numden     :  numden,      $
	     numlev     :  numlev,      $
	     levid      :  levid,       $
	     xrotid     :  xrotid,      $
	     zrotid     :  zrotid,      $
	     styleid    :  styleid,     $
	     colourid   :  colourid,    $
	     choiceid   :  choiceid,    $
             printid    :  printid,     $
             te         :  te,          $
             den        :  den,         $
             surface_l  :  surface_l,   $
             surface_u  :  surface_u,   $
             sp_l       :  sp_l,        $
             sp_u       :  sp_u,        $
	     slev       :  slev,        $
	     iunit_t    :  iunit_t,     $
	     iunit_d    :  iunit_d,     $
	     drawid     :  drawid,      $
             lev_ind    :  1,           $         ; default values if
	     sty_ind    :  0,           $         ; plot is pressed first
	     col_ind    :  0,           $
	     ichoice    :  0,           $
	     ch_ids     :  ch_ids,      $
	     ax         :  30,          $
	     az         :  30,          $
	     title      :  title,       $
	     subtitle   :  subtitle,    $
	     updateid   :  updateid,    $
	     font       :  font_large,  $
             red        :  r,           $
             green      :  g,           $
             blue       :  b            }
  
  
                ;****************************
		;**** realize the widget ****
                ;****************************

;  dynlabel, parent
  widget_control,parent, /realize, set_uvalue=state, /no_copy


		;**** and make it modal ****
                
  xmanager,'bgout_gr', parent, $
           event_handler = 'bgout_gr_event', /modal, /just_reg
 

END
