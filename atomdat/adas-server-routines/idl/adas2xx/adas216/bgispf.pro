; Copyright (c) 1999, Strathclyde University.
; SCCS info: Module @(#)$Header: /home/adascvs/idl/adas2xx/adas216/bgispf.pro,v 1.1 2004/07/06 11:40:15 whitefor Exp $ Date $Date: 2004/07/06 11:40:15 $
;+
; PROJECT:
;       ADAS 
;
; NAME:
;	BGISPF
;
; PURPOSE:
;	IDL user interface and communications with ADAS216 FORTRAN
;	process via pipe.
;
; EXPLANATION:
;	The routine begins by reading information from the ADAS216
;	FORTRAN process via a UNIX pipe.  Then part of the ADAS216
;	IDL user interface is invoked to determine how the user
;	wishes to process the input dataset.  When the user's
;	interactions are complete the information gathered with
;	the user interface is written back to the FORTRAN process
;	via the pipe.  Communications are to the FORTRAN subroutine
;	BGISPF.
;
; USE:
;	The use of this routine is specific to ADAS216, see adas216.pro.
;
; INPUTS:
;	PIPE	- The IDL unit number of the bi-directional pipe to the
;		  ADAS216 FORTRAN process.
;
;	LPEND	- Integer, 0 if the user pressed the 'Done' button
;		  or 1 if the user pressed 'Cancel'.  Maps directly onto
;		  the logical variable LPEND in ADAS216 FORTRAN.
;
;	VALUE	- A structure which determines the initial settings of
;		  the processing options widget.  The initial value is
;		  set in adas216.pro.  If adas216.pro passes a blank 
;		  dummy structure of the form {NMET:-1} into VALUE then
;		  VALUE is reset to a default structure.
;
;		  The VALUE structure is;
;		     {  TITLE:'',	NMET:0, 	$
;			IMETR:intarr(),	IFOUT:0, 	$
;			MAXT:0,		TINE:dblarr(), 	$
;			TINP:dblarr(),	TINH:dblarr(), 	$
;			IDOUT:1,	MAXD:0, 	$
;			MAXRP:0,        MAXRM:0,       	$
;			DINE:dblarr(),	DINP:dblarr(), 	$
;			RATHA:dblarr(),	RATPIA:dblarr(),$
;                       RATMIA:dblarr(,4), IMROUT:0, 	$
;			ZEFF:'',	LPSEL:0, 	$
;			LZSEL:0,	LISEL:0, 	$
;			LHSEL:0,	LRSEL:0,	$
;			LIOSEL:0,       LNSEL:0,        $
;			LNORM:0,		 	$
;	                VAL502:val502  }
;
;		  See cw_adas216_proc.pro for a full description of this
;		  structure and related variables ndmet, ndtem and ndden.
;
;	SZ	- String, recombined ion charge read.
;
;	SZ0	- String, nuclear charge read.
;
;	STRGA	- String array, level designations.
;
;	DSNINC	- String; The full system file name of the input COPASE
;		  dataset selected by the user for processing.
;
;	GOMENU  - Integer; 1=> menu button pressed
;			   0=> not pressed
;	BITFILE - String; file where bimap for menu button is held
;	INVAL 	- Structure; the structure returned by the input screen widget
;	NPL	- No. of metastables (see adas216.f)
;	STRGMI	- String; information written in bgsetp.f
;	STRGMF	- String; information written in bgsetp.f
;	NDMET	- Number of metastables
;
; OPTIONAL INPUTS:
;	None
;
; OUTPUTS:
;	VALUE	- On output the structure records the final settings of
;		  the processing selections widget if the user pressed the
;		  'Done' button, otherwise it is not changed from input.
;
; OPTIONAL OUTPUTS:
;	None
;
; KEYWORD PARAMETERS:
;	FONT_LARGE - The name of a larger font.
;
;	FONT_SMALL - The name of a smaller font.
;
;	EDIT_FONTS - A structure of two fonts used in the table editor
;		     adas_edtab.pro, {font_norm:'',font_input:''}.
;
; CALLS:
;	BGRWER     - read/writes error info from/to fortran.
;
; SIDE EFFECTS:
;	Two way communications with ADAS216 FORTRAN via the
;	bi-directional UNIX pipe.
;
; CATEGORY:
;	Adas system.
;
; WRITTEN:
;       Martin O'Mullane
;
; MODIFIED:
;	1.1	Martin O'Mullane
;		First release 
;
; VERSION:
;       1.1	17-03-99
;
;-
;-----------------------------------------------------------------------------


PRO bgispf, pipe, lpend, value, 					$
		sz, sz0, selem, strga, dsninc,				$
		gomenu, bitfile, inval, npl, strgmi, strgmf, ndmet, 	$
                userroot,						$
		FONT_LARGE=font_large, FONT_SMALL=font_small, 		$
		EDIT_FONTS=edit_fonts

                ;**** Set defaults for keywords ****
                
  IF NOT (KEYWORD_SET(font_large)) THEN font_large = ''
  IF NOT (KEYWORD_SET(font_small)) THEN font_small = ''
  IF NOT (KEYWORD_SET(edit_fonts)) THEN $
                                edit_fonts = {font_norm:'',font_input:''}


		;**** Declare variables for input ****
  idum  = 0
  ddum  = 0.0d0
  sdum  = ''

		;********************************
		;**** Read data from fortran ****
		;********************************
                
  readf,pipe,idum
  lpend=idum
  
  readf,pipe,sdum
  selem = sdum
  readf,pipe,idum
  ndtem=idum
  readf,pipe,idum
  ndden=idum
  readf,pipe,idum
  il=idum
  readf,pipe,idum
  nvmax=idum
  readf,pipe,idum
  nv=idum
  
  tscef = dblarr(nvmax,3)
  for i=0,2 do begin
      for j=0,nvmax-1 do begin            
          readf,pipe,ddum
          tscef(j,i)=ddum
      endfor
  endfor


  readf,pipe,idum
  numcom=idum
  reporterr = strarr(numcom)
  
  for j=0,numcom-1 do begin
    readf,pipe,sdum
    reporterr(j)=sdum
  endfor 
  
  
		;**** now for the errors ****

  readf,pipe,idum
  ndgen=idum
  readf,pipe,idum
  ndspf=idum
  
  numexc = intarr(3)
  defexc = 0.0
  genexc = fltarr(ndgen,2)
  spfexc = fltarr(ndspf,3)
  
  
  bgrwer, pipe, 0, numexc, defexc, genexc, spfexc
 
  numrec = intarr(3)
  defrec = 0.0
  genrec = fltarr(ndgen,2)
  spfrec = fltarr(ndspf,3)
  
  bgrwer, pipe, 0, numrec, defrec, genrec, spfrec

  numcxr = intarr(3)
  defcxr = 0.0
  gencxr = fltarr(ndgen,2)
  spfcxr = fltarr(ndspf,3)
  
  bgrwer, pipe, 0, numcxr, defcxr, gencxr, spfcxr

  numion = intarr(3)
  defion = 0.0
  genion = fltarr(ndgen,2)
  spfion = fltarr(ndspf,3)
  
  bgrwer, pipe, 0, numion, defion, genion, spfion


  
  errdefval = userroot + '/pass/adas216_patch.pass'
  errval = {  errval,                         $
              errout  :       0,              $
              errapp  :       -1,             $
              errrep  :       0,              $
              errdsn  :       '',             $
              errdef  :       errdefval,      $
              errmes  :       '',             $
              numexc  :       numexc,	      $
              defexc  :       defexc,	      $
              genexc  :       genexc,	      $
              spfexc  :       spfexc,	      $
              numrec  :       numrec,	      $
              defrec  :       defrec,	      $
              genrec  :       genrec,	      $
              spfrec  :       spfrec,	      $
              numcxr  :       numcxr,	      $
              defcxr  :       defcxr,	      $
              gencxr  :       gencxr,	      $
              spfcxr  :       spfcxr,	      $
              numion  :       numion,	      $
              defion  :       defion,	      $
              genion  :       genion,	      $
              spfion  :       spfion	      }

                
		;*******************************************
		;**** Set default value if not provided ****
		;*******************************************
                
  if value.new lt 0 then begin
        
    imetr  = make_array(ndmet,/int,value=0)
    temp   = dblarr(ndtem)
    dens   = dblarr(ndden)
    ratios = dblarr(ndden,4)
    
    value = {	new        :    0,              $
                TITLE	   :    '',             $
                NMET	   :    0,              $
		IMETR	   :    imetr,          $
                IFOUT	   :    1,              $
		MAXT	   :    0,              $
                TINE	   :    temp,           $
		TINP	   :    temp,           $
                TINH	   :    temp,           $
		IDOUT	   :    1,              $
                MAXD	   :    0,              $
		MAXRP	   :    0,              $
                MAXRM	   :    0,              $
		DINE	   :    dens,           $
                DINP	   :    dens,           $
		RATHA	   :    dens,           $
                RATPIA	   :    ratios,         $
                RATMIA	   :    ratios,         $
                IMROUT	   :    0,              $
		ZEFF	   :    '',             $
                LPSEL	   :    0,              $
		LZSEL	   :    0,              $
                LISEL	   :    0,              $
		LHSEL	   :    0,              $
                LRSEL	   :    0,              $
		LIOSEL	   :    0,              $
                LNSEL	   :    0,              $
                errval     :    errval,         $
                errtype    :    0               }
                
  endif

		;****************************************
		;**** Pop-up processing input widget ****
		;****************************************
LABEL100:
                
  adas216_proc, value, sz, sz0, strga, dsninc, bitfile, 		$
		ndtem, ndden, ndmet, il, nv, tscef, action, inval, 	$
                selem, errval, reporterr,				$
		FONT_LARGE=font_large, FONT_SMALL=font_small, 		$
		EDIT_FONTS=edit_fonts

		;******************************************
		;**** Act on the event from the widget ****
		;******************************************
		;**** process cancel/done/menu options ****
		;******************************************
   
  if action eq 'Done' then begin
  
; value.errtype = 0  Read from file and no modifications.
;               = 1  Modify data read from file - launch error entry panel.
;               = 2  Define new errors - blank errval structure and
;                    launch error entry panel.
;               = 3  Use previous value.errval - launch error entry panel.

    if value.errtype eq 0 then begin       ; set errval to that in file
       
       value.errval = errval
       
    endif else begin
    
       case value.errtype of
       
          1 : value.errval = errval
          
          2 : begin
          
                num_arr = intarr(3)
                def_arr = 0.0
                gen_arr = fltarr(ndgen,2)
                spf_arr = fltarr(ndspf,3)
  
                errval = {  errval,                         $
                            errout  :       0,              $
                            errapp  :       -1,             $
                            errrep  :       0,              $
                            errdsn  :       '',             $
                            errdef  :       errdefval,      $
                            errmes  :       '',             $
                            numexc  :       num_arr,        $
                            defexc  :       def_arr,        $
                            genexc  :       gen_arr,        $
                            spfexc  :       spf_arr,        $
                            numrec  :       num_arr,        $
                            defrec  :       def_arr,        $
                            genrec  :       gen_arr,        $
                            spfrec  :       spf_arr,        $
                            numcxr  :       num_arr,        $
                            defcxr  :       def_arr,        $
                            gencxr  :       gen_arr,        $
                            spfcxr  :       spf_arr,        $
                            numion  :       num_arr,        $
                            defion  :       def_arr,        $
                            genion  :       gen_arr,        $
                            spfion  :       spf_arr         }
          
                value.errval = errval
                
              end
          
          3 : ; keep the previous definition
          
        endcase
      
        adas216_error, value, sz, sz0, selem, il, strga, ndgen, ndspf,  $
		       dsninc,bitfile, action,                          $
		       FONT_LARGE=font_large, FONT_SMALL=font_small,    $
		       EDIT_FONTS=edit_fonts
                   
        if action eq 'Cancel' then begin
           goto, label100
        endif else if action eq 'Menu' then begin
           lpend  = 0
           gomenu = 1
           printf, pipe, lpend
           goto, LABELEND
        end 

        
    endelse
    
    lpend = 0
    printf, pipe, lpend
    
  end else if action eq 'Menu' then begin
  
    lpend = 0
    gomenu = 1
    printf, pipe, lpend
    goto, LABELEND
    
  endif else begin
  
    lpend = 1
    printf, pipe, lpend
    goto, LABELEND
    
  end

		;*******************************
		;**** Write data to fortran ****
		;*******************************
                
  printf,pipe,value.title
  printf,pipe,value.nmet
  printf,pipe,value.imetr
  printf,pipe,value.ifout
  printf,pipe,value.maxt
  printf,pipe,value.tine
  printf,pipe,value.tinp
  printf,pipe,value.tinh
  printf,pipe,value.idout
  printf,pipe,value.maxd
  printf,pipe,value.dine
  printf,pipe,value.dinp
  printf,pipe,value.ratha
  
  for j = 0, 3 do begin
      for i = 0, ndden-1 do begin
         printf, pipe, value.ratpia(i,j)
      endfor
  endfor
  for j = 0, 3 do begin
      for i = 0, ndden-1 do begin
         printf, pipe, value.ratmia(i,j)
      endfor
  endfor
  if value.lzsel eq 1 then begin
    zeff = float(value.zeff)
  end else begin
    zeff = 0.0
  end
  printf,pipe,zeff
  printf,pipe,value.lpsel
  printf,pipe,value.lzsel
  printf,pipe,value.lisel
  printf,pipe,value.lhsel
  printf,pipe,value.lrsel
  printf,pipe,value.liosel
  printf,pipe,value.lnsel


		;**** now for the errors ****
  
  num = value.errval.numexc
  def = value.errval.defexc
  gen = value.errval.genexc 
  spf = value.errval.spfexc
  bgrwer, pipe, 1, num, def, gen, spf
  
  num = value.errval.numrec
  def = value.errval.defrec
  gen = value.errval.genrec 
  spf = value.errval.spfrec
  bgrwer, pipe, 1, num, def, gen, spf
  
  num = value.errval.numcxr
  def = value.errval.defcxr
  gen = value.errval.gencxr 
  spf = value.errval.spfcxr
  bgrwer, pipe, 1, num, def, gen, spf

  num = value.errval.numion
  def = value.errval.defion
  gen = value.errval.genion 
  spf = value.errval.spfion
  bgrwer, pipe, 1, num, def, gen, spf


LABELEND:

END
