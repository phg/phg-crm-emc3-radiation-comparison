; Copyright (c) 1999, Strathclyde University.
; SCCS info: Module @(#)$Header: /home/adascvs/idl/adas2xx/adas216/adas216_inspct_cum.pro,v 1.1 2004/07/06 10:26:05 whitefor Exp $ Date $Date: 2004/07/06 10:26:05 $
;+
; PROJECT:
;       ADAS 
;
; NAME:
;	ADAS216_INSPCT_CUM
;
; PURPOSE:
;	A popup window for displaying fits to iterated data. Slices
;       corresponding to temperature or density for a particular  level
;       can be viewed. The statistics of the fit can be viewed and
;       printed and/or the plot can be printed to a file.
;
; EXPLANATION:
;	This routine allows the user to navigate through the cumulative
;       error data. Slices of gaussian fits corresponding to a range of
;       temperatures or densities for a particular  level are plotted.
;       Only those Te/ne which have sufficient data to fit are plotted.
;       ie if the unperturbed population is 0.0  (ie too small) nothing
;       is shown. The data is read from a file. The size of the array
;       should be small enough for most systems to read into memory as it
;       is stored as integer*2. It is 4-D though as there are
;       numden*numte*numlev histograms each with numbin bins!
;     
;
; INPUTS:
;       VALUE - Structure holding necessary information.
;
;               numte    : number of temperatures  
;               numden   : number of densities 
;               numlev   : number of levels
;               numiter  : number of iterations
;               numbin   : number of bins for histograms
;               te       : temperatures  
;               den      : densities
;               slev     : levels (string)
;               iunit_t  : temperature units (integer) (K, eV, reduced)   
;               iunit_d  : density units (integer) (cm**-3, reduced)   
;               xhist    : x values of histogram
;               dumpfile : file where histograms are stored
;
; OPTIONAL INPUTS:
;	None.
;
; OUTPUTS:
;       The return value of this function is the ID of the compound
;       widget which is of type LONG integer.
;
; OPTIONAL OUTPUTS:
;       None
;
; KEYWORD PARAMETERS:
;	XSIZE	- Integer; x size in characters of browse widget.
;		  Default is 400.
;
;	YSIZE	- Integer; y size in characters of browse widget.
;		  Default is 340.
;
;	FONT	- A font to use for all text in this widget.
;
; CALLS:
;	ADAS_TEXT                to browse/print an array of strings
;       ADAS216_INSPCT_CUM_PLOT  plots to screen or file the fits 
;       ADAS_FILE_GR             popup widget to select a file for 
;                                graphics output. 
;
; SIDE EFFECTS:
;	One other routine is included which is used to manage the widget;
;
;	ADAS216_INSPCT_CUM_EVENT
;
; CATEGORY:
;	Widgets
;
; WRITTEN:
;       Martin O'Mullane
;
; MODIFIED:
;	1.1	Martin O'Mullane
;		First release 
;
; VERSION:
;       1.1	17-03-99
;
;-
;-----------------------------------------------------------------------------

PRO adas216_inspct_cum_event, event

 Widget_Control, event.top, Get_UValue=state, /no_copy
 Widget_Control, event.id,  Get_UValue=userEvent

CASE userEvent OF

  'CHOICE' : begin
	     
             widget_control, state.choiceid, get_value = bpress
             state.ichoice = bpress
             
             if bpress eq 0 then begin
                widget_control, state.neid, map=1
                widget_control, state.teid, map=0
             endif
  
             if bpress eq 1 then begin
                widget_control, state.teid, map=1
                widget_control, state.neid, map=0
             endif
               
             widget_control, state.printid, sensitive=0
             widget_control, state.statsid, sensitive=0
	
            end
            
  'LEVS'  :  begin
               widget_control, state.printid, sensitive=0
               widget_control, state.statsid, sensitive=0
               state.lev_ind = event.index
             end
  'TE'    :  begin
               widget_control, state.printid, sensitive=0
               widget_control, state.statsid, sensitive=0
               state.te_ind = event.index
             end
  'DEN'   :  begin
               widget_control, state.printid, sensitive=0
               widget_control, state.statsid, sensitive=0
               state.den_ind = event.index
             end
	
  'PLOT'  : begin

               widget_control,state.updateid,set_value =  '                           '
               
               lev_ind = state.lev_ind
               te_ind  = state.te_ind
               den_ind = state.den_ind
               
               widget_control, state.drawid, get_value = draw_win
               wset,  draw_win
               erase
               
               xyouts, 300,300,'Reading data', charsize = 3,/device

                 
               if state.ichoice eq 0 then begin
                  pop_u_all = state.popun(lev_ind,*,den_ind) 
               endif else begin
                  pop_u_all = state.popun(lev_ind,te_ind,*)
               endelse
               
               pop_u_all  = reform(pop_u_all)
               ind_nz = where(pop_u_all ne 0.0, count)
               
               ; jump to end if there is no valid data
               if count eq 0 then begin
                  
                  erase
                  xyouts, 300,300,'No valid data', charsize = 3, /device
                  goto, LABEL_bailout
                  
               endif
                
               num_nz = n_elements(ind_nz)
               num_nz = num_nz(0)
               pop_u  = pop_u_all(ind_nz)
               
               
               if state.ichoice eq 0 then begin
                  ihist_p = state.ihist(lev_ind,ind_nz,den_ind,*)
               endif else begin
                  ihist_p = state.ihist(lev_ind,te_ind,ind_nz,*)
               endelse
               ihist_p = reform(ihist_p,num_nz,state.numbin) 

               
               plot_stuff = { num_nz   :  num_nz,           $
                              ind_nz   :  ind_nz,           $
                              te       :  state.te,         $
                              den      :  state.den,        $
                              slev     :  state.slev,       $
                              lev_ind  :  lev_ind,          $
                              te_ind   :  te_ind,           $
                              den_ind  :  den_ind,          $
                              iunit_t  :  state.iunit_t,    $
                              iunit_d  :  state.iunit_d,    $
                              ichoice  :  state.ichoice,    $
                              numiter  :  state.numiter,    $
                              xhist    :  state.xhist,      $
                              ihist_p  :  ihist_p,          $
                              pop_u    :  pop_u             }
                              
               temp = state.stats
               adas216_inspct_cum_plot, state.updateid, temp, $
                                        value = plot_stuff 
                                                    
               state.stats=temp
               
               widget_control, state.printid, sensitive=1
               widget_control, state.statsid, sensitive=1
                                                    
     LABEL_bailout:                 
               !p.multi = [0,1,1,0,0]               
               
            end
            
  'DONE'   : begin
               widget_control, event.top, /destroy
             end
  
  'PRINT'  : begin
	
               widget_control,state.updateid,set_value =  '                           '

               print_val = { dsn     : '',                  $
                             defname : 'adas216_slice.ps',  $
                             replace :  1,                  $
                             devlist :  state.devlist,      $
                             dev     :  state.devlist(0),   $
                             paper   :  'A4',               $
                             write   :  0,                  $
                             message :  'Note: cannot append to graphics files'}
               act = 0
               adas_file_gr, print_val, act, font=state.font, $
                             title='File name for graphical output'
               
               
               lev_ind = state.lev_ind
               te_ind  = state.te_ind
               den_ind = state.den_ind
               
               widget_control, state.drawid, get_value = draw_win
               wset,  draw_win
                 
               if state.ichoice eq 0 then begin
                  pop_u_all = state.popun(lev_ind,*,den_ind) 
               endif else begin
                  pop_u_all = state.popun(lev_ind,te_ind,*)
               endelse
               
               pop_u_all  = reform(pop_u_all)
               ind_nz = where(pop_u_all ne 0.0, count)
               
               ; jump to end if there is no valid data - just in case we
               ; are asked to print before plotting
               
               if count eq 0 then begin
                  
                  erase
                  xyouts, 300,300,'No valid data', charsize = 3, /device
                  goto, LABEL_bailout
                  
               endif
                
               num_nz = n_elements(ind_nz)
               num_nz = num_nz(0)
               pop_u  = pop_u_all(ind_nz)
               
               
               if state.ichoice eq 0 then begin
                  ihist_p = state.ihist(lev_ind,ind_nz,den_ind,*)
               endif else begin
                  ihist_p = state.ihist(lev_ind,te_ind,ind_nz,*)
               endelse
               ihist_p = reform(ihist_p,num_nz,state.numbin) 

               
               plot_stuff = { num_nz   :  num_nz,           $
                              ind_nz   :  ind_nz,           $
                              te       :  state.te,         $
                              den      :  state.den,        $
                              slev     :  state.slev,       $
                              iunit_t  :  state.iunit_t,    $
                              iunit_d  :  state.iunit_d,    $
                              lev_ind  :  lev_ind,          $
                              te_ind   :  te_ind,           $
                              den_ind  :  den_ind,          $
                              ichoice  :  state.ichoice,    $
                              numiter  :  state.numiter,    $
                              xhist    :  state.xhist,      $
                              ihist_p  :  ihist_p,          $
                              pop_u    :  pop_u             }
                              
               
               print_stuff = { dsn     : print_val.dsn,   $
                               devcode : state.devcode,   $
                               devlist : state.devlist,   $
                               dev     : print_val.dev,   $
                               paper   : print_val.paper  }
                              
               if print_val.write eq 1 then begin               
                              
                  temp = state.stats
                  adas216_inspct_cum_plot, state.updateid, temp, $
                                       value = plot_stuff,       $
                                       print = print_stuff 
                  state.stats=temp
	
                  widget_control, state.updateid, $
                                  set_value = 'Plot written to file'
                                  
               endif else begin
               
                  widget_control, state.updateid, $
                                  set_value = 'No file selected'
                                  
               endelse
               
             end
             
  'STATS'  : begin
	       
               date = xxdate()
               
               numind  = state.stats.numind
               numiter = state.stats.numiter
               slevel  = state.stats.slevel
               units   = state.stats.units
               xvar    = state.stats.xvar
               a       = state.stats.a
               
               ; construct statistics information - it's fairly messy
               ; but it must be passed to adas_text (then to widget_text)
               ; as an array of strings.
               
               textstr = ['Statistics for cumulative monte-carlo' + $ 
                          ' error analysis', ' ',                   $
                          'ADAS216  ' + date(0)+ ' ' + date(1),     $
                          ' ',  'Level : ' + slevel + '   with ' +  $
                          string(numiter,format='(i5)') + ' iterations', $
                          ' ', 'Fit function : a3 + a0 * exp(-z**2/2) ;'+  $
                          ' z = (x-a1)/a2', ' ', ' ',                 $
                          'Fit data :' , ' ' ,                     $
                          'Index (' + units + ') a0          ' +$
                          'a1          a2          a3',' ']
               
               for j= 0, numind-1 do begin
                          
                 addstr  = string(xvar(j), a(*,j), format = '(5(e10.3,2x))')
                 textstr =  [textstr, addstr]
                 
               endfor
               
               textstr = [textstr, ' ', ' ',  'Population :' , ' ' , $
                          'Index (' + units + ') pop         ' +     $
                          'sigma (1 s.d.)  %error',' ']
 
               for j= 0, numind-1 do begin
                      
                 pop = state.stats.popu(j)
                 per = a(2,j) / a(1,j) * 100.0         
                 addstr  = string(xvar(j), a(1,j)*pop, a(2,j)*pop, per,    $
                           format = '(3(e10.3,2x),5x,f6.2)')
                 textstr =  [textstr, addstr]
                 
               endfor
               
               
               textstr = [textstr, ' ', ' ',  'Fractional population :' , ' ' , $
                          'Index (' + units + ') deviation   ' +     $
                          'sigma (1 s.d.)  %error',' ']
 
               for j= 0, numind-1 do begin
                      
                 per = a(2,j) / a(1,j) * 100.0         
                 addstr  = string(xvar(j), a(1,j), a(2,j), per,    $
                           format = '(3(e10.3,2x),5x,f6.2)')
                 textstr =  [textstr, addstr]
                 
               endfor
               
               
               ; In a small sample the absolute difference is probably
               ; a better measure of scatter than the variance. We
               ; know that the mean SHOULD BE 1.0 so let's get the
               ; scatter from that rather than computing the mean.
               
               arr = a(1,0:numind-1)
               arr=reform(arr)
               abs_diff = total(abs(arr-1.0))/numind
               s_ad = string(abs_diff,format = '(f9.4)')
               
               textstr = [textstr, ' ', ' ', 'Absolute difference : ' + s_ad]
 
               def_name = 'adas216_stats.txt'
               adas_text, textstr,  def_name, font = state.font, xsize = 60, $
                                    wintitle = 'ADAS216 Statistics'   
        
             end
             
  else : print,'ADAS216_ISPCT_EVENT : You should not see this message! ',userEvent
                   
ENDCASE

                ;*****************************************
		;**** Update state but only if we are ****
                ;**** NOT leaving the widget.         ****
                ;*****************************************

 if userEvent ne 'DONE' then $
    widget_control, event.top, set_uvalue=state, /no_copy

END

;-----------------------------------------------------------------------------


PRO adas216_inspct_cum, devlist, devcode, $
                    VALUE=value, XSIZE = xsize, YSIZE = ysize, FONT = font


		;**** Set defaults for keywords ****

  IF NOT (KEYWORD_SET(xsize)) THEN xsize = 400
  IF NOT (KEYWORD_SET(ysize)) THEN ysize = 340
  IF NOT (KEYWORD_SET(font))  THEN font = ''
  IF NOT (KEYWORD_SET(value)) THEN $  
          message,'A structure must be passed to adas216_inspct'



  dumpfile = value.dumpfile
  
  numte    = value.numte
  numden   = value.numden
  numlev   = value.numlev
  numiter  = value.numiter
  numbin   = value.numbin
  
  te       = value.te
  den      = value.den
  slev     = value.slev
  xhist    = value.xhist
    
  iunit_t  = value.iunit_t
  iunit_d  = value.iunit_d
  
 
  ; define statistics structure

  maxnum  = max([numte,numden])
  stats_a = fltarr(4,maxnum,/nozero)
  xvar    = fltarr(maxnum,/nozero)
  popu    = fltarr(maxnum,/nozero)
  
  stats = {  numind   :  0,         $
	     numiter  :  numiter,   $
             xvar     :  xvar,      $
	     slevel   :  '',        $
	     units    :  '',        $
	     popu     :  popu,      $
             a        :  stats_a    }
  
  
                ;***********************************
		;**** create titled base widget ****
                ;***********************************
                
  parent = widget_base(title='Results of Iteration', $
			xoffset=20,yoffset=20,/column)
                
  rc = widget_label(parent,value='  ',font=font)

                 
  
                ;*****************************************
		;**** Choice of slice to plot section ****
                ;*****************************************

  mrow     = widget_base(parent, /row, /frame, /align_center)
 
  choiceid = cw_bgroup(mrow,['Temperature','Density    '],  $
                             exclusive=1,column=1,          $
                             label_left = 'Plot :',	    $
                             /no_release,  Uvalue='CHOICE',font=font)
                             
  mlab   = widget_label(mrow, value=' at ', font=font)
  
  base   = widget_base(mrow,/align_center)
  trow   = widget_base(base)
  drow   = widget_base(base)
  

  temps  = string(te,format='(e8.2)')
  teid   = widget_droplist(trow, value=temps, font=font, Uvalue='TE')

  denss  = string(den,format='(e8.2)')
  neid   = widget_droplist(drow, value=denss, font=font, Uvalue='DEN')

  mlab   = widget_label(mrow, value=' for level ', font=font)
  
  levs   = slev(0:numlev-1)
  levid  = widget_droplist(mrow, value=levs,/align_center,  $
                          font=font,  Uvalue='LEVS')



 		;***********************************
		;**** set up initial conditions ****
		;***********************************
 
 
  widget_control, levid, set_droplist_select = 0
  widget_control, teid,  set_droplist_select = 0
  widget_control, neid,  set_droplist_select = 0

  widget_control, teid, map=0
  widget_control, choiceid, set_value = 0



                ;*****************************************
		;**** Draw widget to display results. ****
                ;*****************************************

  device, get_screen_size=scrsz
  xwidth  = scrsz(0)*0.80
  yheight = scrsz(1)*0.70

  drawid = widget_draw(parent, xsize=xwidth, ysize=yheight) 



                ;*****************
		;**** Buttons ****
                ;*****************
                
  base    = widget_base(parent,/row)
  doneid  = widget_button(base,value='Done',font=font,Uvalue='DONE')
  
  mlab   = widget_label(base, value='     ', font=font)
  plotid = widget_button(base, value='Plot',font=font,/align_center,Uvalue='PLOT')

  printid = widget_button(base,value='Print',font=font,Uvalue='PRINT')
  statsid = widget_button(base,value='Statistics',font=font,Uvalue='STATS')
  
  

                ;**************************
		;**** Read data update ****
                ;**************************
  
  updateid = widget_label(base,value='                                      ',$
                          font=font,/align_right)
  

  
                ;**************************
		;**** initial settings ****
                ;**************************
  
  ; Only allow printing or statistics when there is a plot on the screen.
  ; If the level/temperature/density is changed before printing or
  ; viewing statistics then de-sensitise the print/stats immediately.
  ; In this way what is printed/browsed is the same as on the screen.
                
  widget_control, printid, sensitive=0            
  widget_control, statsid, sensitive=0            
  
  
                ;**********************************************
                ;**** read in data - from unformatted file ****
                ;**********************************************
   
   popun = fltarr(numlev,numte,numden,/nozero)
   ihist = intarr(numlev,numte,numden,numbin,/nozero)

   openr, lun, dumpfile, /get_lun, /f77_unformatted

   readu, lun, popun
   readu, lun, ihist
   
   free_lun, lun
  
 
               
               
                ;****************************************************
		;**** put all needed info into a state structure ****
                ;****************************************************
   
  state = {  devlist  :  devlist,   $
             devcode  :  devcode,   $   
             dumpfile :  dumpfile,  $
             numte    :  numte,     $
             numden   :  numden,    $
	     numlev   :  numlev,    $
	     numiter  :  numiter,   $
             choiceid :  choiceid,  $
             teid     :  teid,      $
             neid     :  neid,      $
	     levid    :  levid,     $
             plotid   :  plotid,    $
             printid  :  printid,   $
             statsid  :  statsid,   $
             te       :  te,        $
             den      :  den,       $
	     slev     :  slev,      $
	     iunit_t  :  iunit_t,   $
	     iunit_d  :  iunit_d,   $
	     drawid   :  drawid,    $
	     te_ind   :  0,         $
	     den_ind  :  0,         $
             lev_ind  :  0,         $
	     ichoice  :  0,         $
	     updateid :  updateid,  $
	     font     :  font,      $
             popun    :  popun,     $
             numbin   :  numbin,    $
             xhist    :  xhist,     $
             ihist    :  ihist,     $
             stats    :  stats      }
  
  
                ;****************************
		;**** realize the widget ****
                ;****************************

;  dynlabel, parent
  widget_control,parent, /realize, set_uvalue=state, /no_copy


		;**** and make it modal ****
                
  xmanager,'adas216_inspct_cum',parent, $
            event_handler='adas216_inspct_cum_event',/modal,/just_reg
 

END
