; Copyright (c) 1996, Strathclyde University
; SCCS info: Module @(#)$Header: /home/adascvs/idl/adas2xx/adas212/bcspf0.pro,v 1.2 2005/03/16 11:17:22 allan Exp $ Date $Date: 2005/03/16 11:17:22 $
;+
; PROJECT:
;       ADAS IBM MVS to UNIX conversion
;
; NAME:
;	BCSPF0
;
; PURPOSE:
;	IDL user interface and communications with ADAS212 FORTRAN
;	process via pipe.
;
; EXPLANATION:
;	Firstly this routine invokes the part of the user interface
;	used to select the input dataset for ADAS212.  When the user's
;	interactions are complete this routine communicates with the
;	ADAS212 FORTRAN application via a UNIX pipe.  Communications
;	are to the FORTRAN subroutine BCSPF0.
;
; USE:
;	The use of this routine is specific to ADAS212, see adas212.pro.
;
; INPUTS:
;	PIPE	- The IDL unit number of the bi-directional pipe to the
;		  ADAS212 FORTRAN process.
;
;	VALUE	- A structure which determines the initial settings of
;		  the dataset selection widget.  The initial value is
;		  set in adas212.pro.  VALUE is passed un-modified
;		  through to cw_adas_in.pro, see that routine for a full
;		  description.
;
; OPTIONAL INPUTS:
;	None
;
; OUTPUTS:
;	VALUE	- On output the structure records the final settings of
;		  the dataset selection widget if the user pressed the
;		  'Done' button otherwise it is not changed from input.
;
;	DSFULL	- String; The full system file name of the emitting ion
;		  dataset selected by the user for processing.
;
;	REP	- String; Indicates whether the user pressed the 'Done'
;		  or 'Cancel' button on the interface.  The action is
;		  converted to the strings 'NO' and 'YES' respectively
;		  to match up with the existing FORTRAN code.  In the
;		  original IBM ISPF interface REP was the reply to the
;		  question 'DO YOU WANT TO END PROGRAM EXECUTION'.
;
; OPTIONAL OUTPUTS:
;	None
;
; KEYWORD PARAMETERS:
;	FONT	- Supplies the font to be used for the interface widgets.
;
; CALLS:
;	ADAS_IN		Pops-up the dataset selection widget.
;
; SIDE EFFECTS:
;	This routine communicates with the ADAS212 FORTRAN process
;	via a UNIX pipe.
;
; CATEGORY:
;	Adas system.
;
; WRITTEN:
;       William Osborn, Tessella Support Services plc, 31/07/1996
;
; MODIFIED:
;	1.1	William Osborn
;		First Release
;	1.2	Allan Whiteford
;		Call adas2xx_in instead of adas_in. adas2xx_in has
;               the option to give back non-Maxwellian parameters.
;               These parameters are then passed to the FORTRAN.
;
; VERSION:
;	1.1	31-07-96
;	1.2	27-01-05
;
;-----------------------------------------------------------------------------


PRO bcspf0, pipe, value, dsfull, rep, FONT=font


                ;**** Return to caller on error ****

    ON_ERROR, 2

                ;**** Set defaults for keywords ****

    IF NOT (KEYWORD_SET(font)) THEN font = ''


		;**********************************
		;**** Pop-up input file widget ****
		;**********************************

AGAIN:
    adas2xx_in, value, action, WINTITLE = 'ADAS 212 INPUT', 		$
	     TITLE = 'Input File', FONT = font

		;******************************************
		;**** Act on the event from the widget ****
		;******************************************
		;**** There are only two possible events ****
		;**** 'Done' and 'Cancel'.               ****
		;********************************************

    if action eq 'Done' then begin
        rep = 'NO'
    end else begin
        rep = 'YES'
    end

		;**** Construct dataset name ****

    dsfull = value.rootpath+value.file
    ds37   = value.root37+value.file37
    dist   = value.dist
    dparam = value.dparam

		;**** Check whether the x-reference file is OK

    if rep eq 'NO' then if check_xref_212(dsfull) eq 0 then goto, AGAIN

		;*******************************
		;**** Write data to fortran ****
		;*******************************

    printf, pipe, rep
    printf, pipe, dsfull
    printf, pipe, dist
    printf, pipe, dparam
    printf, pipe, ds37


END
