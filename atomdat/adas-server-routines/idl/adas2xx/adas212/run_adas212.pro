;----------------------------------------------------------------------
;+
; PROJECT    :  ADAS
;
; NAME       :  run_adas212
;
; PURPOSE    :  Add DR contribution to R-lines in an adf04 dataset.
;               The adf04 file and mapping to adf09 are set in an
;               adf18/a09_a04/ cross-reference file.
;
; ARGUMENTS  :  All output arguments will be defined appropriately.
;
;               NAME      I/O   TYPE    DETAILS
; INPUTS     :  adf18      I    str     driver file.
;               adf04_r    I    str     file of augmented R-lines for
;                                       editing into adf04 specific
;                                       ion file.
;               log        I    str     log file (paper.txt)
;
; KEYWORDS   :  help       I      -     Display header as help.
;
; OUTPUTS    :  all information is returned in ADAS datasets.
;
;
; NOTES      :  run_adas212 used the spawn command. Therefore IDL v5.3
;               cannot be used. Any other version will work.
;
;
; AUTHOR     :  Martin O'Mullane
;
; DATE       :  31-08-2009
;
;
; MODIFIED:
;       1.1     Martin O'Mullane
;                 - First version.
;
; VERSION:
;       1.1     31-08-2009
;-
;----------------------------------------------------------------------



PRO run_adas212, adf18   = adf18,   $
                 adf04_r = adf04_r, $
                 log     = log,     $
                 help    = help


; If asked for help

if keyword_set(help) then begin
   doc_library, 'run_adas212'
   return
endif


; First check that we are not using IDL v5.3

if !version.release EQ '5.3' then begin
   message, 'This version of IDL (v5.3) will not work - ' + $
            'change to v5.0, v5.1, v5.2 or v5.4 or v6 and above'
   return
endif


; Check that we get all inputs and that they are correct. Otherwise print
; a message and return to command line.

on_error, 2

if n_elements(adf04_r) EQ 0 then message,'An output file for R-lines is required'

; Check the input driver file

if n_elements(adf18) EQ 0 then begin

    message,'An adf18 driver file is required'

endif else begin

   a18 = adf18
   file_acc, a18, exist, read, write, execute, filetype
   if exist ne 1 then message, 'adf18 file does not exist '+ a18
   if read ne 1 then message, 'adf18 file cannot be read from this userid '+ a18

endelse


if n_elements(log) NE 0 then log = log else log = 'NULL'

; Run the fortran code

date = xxdate()

fortdir = getenv('ADASFORT')
spawn, fortdir+'/adas212.out', unit=pipe, /noshell, PID=pid

; Input screen

rep = 'NO'
dist = 0L
dparam = 1.0D-30
dsn37 = 'NULL'

printf, pipe, rep
printf, pipe, a18
printf, pipe, dist
printf, pipe, dparam
printf, pipe, dsn37


; Processing screen

printf, pipe, 0L
printf, pipe, adf04_r
printf, pipe, log
printf, pipe, 1L
if log EQ 'NULL' then printf, pipe, 0L else printf, pipe, 1L

; Calculation

n_adf09 = -1L
idum = -1L

readf, pipe, n_adf09
for j = 0, n_adf09-1 do readf, pipe, idum

printf, pipe, date[0], format='(A)'


; Finish

rep = 'YES'
printf, pipe, rep
printf, pipe, a18
printf, pipe, dist
printf, pipe, dparam
printf, pipe, dsn37

; Free up allocated units

idum = -1L
readf, pipe, idum

close, /all


END
