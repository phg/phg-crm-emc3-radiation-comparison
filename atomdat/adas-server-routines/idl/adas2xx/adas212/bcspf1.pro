; Copyright (c) 1996, Strathclyde University.
;
;+
; PROJECT:
;       ADAS IBM MVS to UNIX conversion
;
; NAME:
;       BBSPF1
;
; PURPOSE:
;       IDL user interface and communications with ADAS212 FORTRAN
;       process via pipe.
;
; EXPLANATION:
;       In this routine the 'Output Options' part of the
;       interface is invoked.  The resulting action of the user's
;       is written to the FORTRAN via the pipe.
;       Communications are with the FORTRAN subroutine BbSPF1.
;       Note that the usual large amount of FORTRAN-IDL pipe
;       communications in these routines has been removed due to the
;       fact that there are two FORTRAN routines rather than the usual
;       one and the communications are therefore handled elsewhere.
;
; USE:
;       The use of this routine is specific to ADAS212, See adas212.pro.
;
; INPUTS:
;       PIPE    - The IDL unit number of the bi-directional pipe to the
;                 ADAS212 FORTRAN process.
;
;       DSFULL  - The name of the data set being analyzed.
;
;       VALUE   - A structure which determines the initial settings of
;                 the dataset selection widget.  The initial value is
;                 set in adas212.pro.  VALUE is passed un-modified
;                 through to cw_adas_in.pro, see that routine for a full
;                 description.
;
;       HEADER  - Header information used for text output.
;
; OPTIONAL INPUTS:
;       None.
;
; OUTPUTS:
;       LPEND   - Integer; indicates user action on leaving output
;                 selections.  lpend is set to 1 if the user exited
;                 with the 'Cancel' button, 0 if the user exited
;                 with 'Run Now' and 2 if the user exited with 'Run in
;                 Batch'.
;
;       VALUE   - On output the structure records the final settings of
;                 the output selection widget if the user pressed either
;                 of the 'Run' buttons otherwise it is not changed from input.
;
; OPTIONAL OUTPUTS:
;       None.
;
; KEYWORD PARAMETERS:
;
;       FONT    - Supplies the font to be used for the interface widgets.
;
; CALLS:
;       ADAS212_OUT     User interface - output options.
;
; SIDE EFFECTS:
;       This routine communicates with the ADAS212 FORTRAN process
;       via a UNIX pipe.
;
; CATEGORY:
;       Adas system.
;
; WRITTEN:
;       William Osborn, Tessella Support Services plc, 04-jul-1996
;
; MODIFIED:
;       1.1     William Osborn                          04/07/96
; VERSION:
;       1.1     First release.
;-
;-----------------------------------------------------------------------------

PRO   bcspf1, pipe, lpend, value, dsfull, header,                       $
              FONT=font

                ;**** Return to caller on error ****

    ON_ERROR, 2

                ;**** Set defaults for keywords ****

    IF NOT (KEYWORD_SET(font)) THEN font = ''

                ;**** Define some variables before read from pipe ****


                ;**************************************
                ;**** Pop-up output options widget ****
                ;**************************************

    outtitle = "ADAS212 OUTPUT OPTIONS"
    adas212_out, value, dsfull, action, FONT=font

                ;*************************************************
                ;**** Act on the output from the widget       ****
                ;**** There are three    possible actions     ****
                ;**** 'Cancel', 'Run Now' and 'Run in Batch'. ****
                ;*************************************************

    if action eq 'Run Now' then begin
        lpend = 0
    endif else begin
        lpend = 1
    end

                ;*******************************
                ;**** Write data to fortran ****
                ;*******************************

    printf, pipe, lpend

    if (lpend eq 0) then begin

        printf, pipe, value.pasdsn
        printf, pipe, value.texdsn
        printf, pipe, value.pasout
        printf, pipe, value.texout

                ;**************************************************
                ;**** Set messages and settings ready for when ****
                ;**** output window is re-displayed.           ****
                ;**************************************************

        if value.pasout eq 1 then begin
            if value.pasrep ge 0 then value.pasrep = 0
            value.pasmes = 'Output written to file.'
        endif
        if value.texout eq 1 then begin
            if value.texrep ge 0 then value.texrep = 0
            value.texmes = 'Output written to file.'
        endif
    endif

END
