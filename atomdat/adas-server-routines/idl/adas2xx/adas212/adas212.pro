; Copyright (c) 1996, Strathclyde University.
;+
; PROJECT:
;       ADAS IBM MVS to UNIX conversion
;
; NAME:
;       ADAS212
;
; PURPOSE:
;       The highest level routine for the ADAS 212 program.
;
; EXPLANATION:
;       This routine is called from the main adas system routine, adas.pro,
;       to start the ADAS 212 application.  Associated with adas212.pro
;       is a FORTRAN executable, adas212.out.  The IDL code provides the user
;       interface and output graphics whilst the FORTRAN code reads
;       in data files, performs numerical processing and creates the
;       output files.  The IDL code communicates with the FORTRAN
;       executable via a bi-directional UNIX pipe.  The unit number
;       used by the IDL for writing to and reading from this pipe is
;       allocated when the FORTRAN executable is 'spawned' (see code
;       below).  Pipe communications in the FORTRAN process are to
;       stdin and stdout, i.e streams 5 and 6.
;
;       The FORTRAN code is an independent process under the UNIX system.
;       The IDL process can only exert control over the FORTRAN in the
;       data which it communicates via the pipe.  The communications
;       between the IDL and FORTRAN must be exactly matched to avoid
;       input conversion errors.  The correct ammounts of data must be
;       passed so that neither process 'hangs' waiting for communications
;       which will never occur.
;
;       The FORTRAN code performs some error checking which is
;       independent of IDL.  In cases of error the FORTRAN may write
;       error messages.  To prevent these error messages from conflicting
;       with the pipe communications all FORTRAN errors are written to
;       output stream 0, which is stderr for UNIX.  These error messages
;       will appear in the window from which the ADAS session/IDL session
;       is being run. In the case of the batch job, the error messages
;       will appear as standard cron job output which is usually
;       emailed to the user. If the job has completed succesfully then
;       the user will get a message telling them so.
;
;       In the case of severe errors the FORTRAN code may terminate
;       itself prematurely.  In order to detect this, and prevent the
;       IDL program from 'hanging' or crashing, the IDL checks to see
;       if the FORTRAN executable is still an active process before
;       each group of pipe communications.  The process identifier
;       for the FORTRAN process, PID, is recorded when the process is
;       first 'spawned'.  The system is then checked for the presence
;       of the FORTRAN PID. This checking does not occur for the
;       batch cases.
;
; USE:
;       First the system settings must be established by calling
;       adas_sys_set.pro then adas212.pro is called to start the
;       ADAS 212 application;
;
;       adas_sys_set, adasrel, fortdir, userroot, centroot,             $
;                     devlist, devcode, font_large, font_small, edit_fonts
;       adas212,   adasrel, fortdir, userroot, centroot, devlist,       $
;                  devcode, font_large, font_small, edit_fonts
;
; INPUTS:
;       ADASREL - A string indicating the ADAS system version,
;                 e.g ' ADAS RELEASE: ADAS93 V1.1.0'.  The first
;                 character should be a space.
;
;       FORTDIR - A string holding the path to the directory where the
;                 FORTRAN executables are, e.g '/disk/adas/fortran/exec'
;
;       USERROOT - A string holding the path to the root directory of
;                  the user's adas data. e.g '/disk/bowen/adas'
;                  This root directory will be used by adas to construct
;                  other path names.  For example the users default data
;                  for adas205 should be in /disk/bowen/adas/adf04.  In
;                  particular the user's default interface settings will
;                  be stored in the directory USERROOT+'/defaults'.  An
;                  error will occur if the defaults directory does not
;                  exist.
;
;       CENTROOT - Like USERROOT, but this directory points to the
;                  central data area for the system.  User defaults are
;                  not stored on CENTROOT.
;
;       DEVLIST - A string array of hardcopy device names, used for
;                 graphical output. e.g ['Post-Script','HP-PCL','HP-GL']
;                 This array must mirror DEVCODE.  DEVCODE holds the
;                 actual device names used in a SET_PLOT statement.
;
;       DEVCODE - A string array of hardcopy device code names used in
;                 the SET_PLOT statement, e.g ['PS', 'PCL', 'HP']
;                 This array must mirror DEVLIST.
;
;       FONT_LARGE - The name of a larger font e.g 'courier_bold14'
;
;       FONT_SMALL - The name of a smaller font e.g 'courier_bold12'
;
;       EDIT_FONTS - A structure of two fonts used in the table editor
;                    adas_edtab.pro, {font_norm:'',font_input:''}.
;                    The two fonts are used to differentiate between
;                    editable and non-editable parts of the table. You
;                    may have to experiment to get two fonts which give
;                    the same default spacing in the edit widget.
;                    e.g {font_norm:'helvetica_bold14', $
;                         font_input:'helvetica_oblique14'}
;
; OPTIONAL INPUTS:
;       None
;
; OUTPUTS:
;       None - Note this routine should leave all inputs un-modified.
;
; OPTIONAL OUTPUTS:
;       None
;
; KEYWORD PARAMETERS:
;       None
;
; CALLS:
;       FIND_PROCESS    Checks to see if a given process is active.
;       XXDATE          Get date and time from operating system.
;       BBSPF0          Pipe comms with FORTRAN BbSPF0 routine.
;       BBSPF1          Pipe comms with FORTRAN BbSPF1 routine.
;       BBSPF2          Updates the batch files and variables for
;                       the batch runs depending on the user selections.
;
; SIDE EFFECTS:
;       This routine spawns FORTRAN executables.  Note the pipe
;       communications routines listed above.
;
; CATEGORY:
;       Adas system.
;
; WRITTEN:
;       William Osborn, Tessella Support Services plc, 04/07/1996
;
; MODIFIED:
;       1.1     William Osborn
;               First release
;       1.2     William Osborn
;               Changed SunOS batch job behaviour
;       1.3     William Osborn
;               Added call to batch.pro rather than having code for
;               each UNIX flavour present in this routine.
;       1.4     William Osborn
;               Increased version number to 1.2
;       1.5     Richard Martin
;               Added 'printf' statement for date
;       1.6     Richard Martin
;               Increased version number to 1.3
;       1.7     Richard Martin
;               Increased version number to 1.4
;       1.8     Richard Martin
;               Added date to 'info####.tmp' batch file. Increased
;               version no. to 1.5 .
;       1.9     Richard Martin
;               Increased version no. to 1.6 .
;       1.10    Richard Martin
;               Increased version no. to 1.7 .
;       1.11    Martin O'Mullane
;               Change output default names to adas_adf04.pass from data04.pass
;               and adas_adf08.pass from data08.pass.
;               Increased version no to 1.8
;       1.12    Richard Martin
;               Increased version no. to 1.9 .
;       1.13    Allan Whiteford
;               Added fields in inval to handle Non-Maxwellian modelling.
;       1.14    Martin O'Mullane
;               Change workflow to mirror adas211.
;
; VERSION:
;       1.1     04-07-96
;       1.2     11-10-96
;       1.3     18-10-96
;       1.4     25-11-96
;       1.5     14-02-97
;       1.6     04-04-97
;       1.7     04-04-97
;       1.8     30-10-97
;       1.9     11-03-98
;       1.10    15-11-2000
;       1.11    18-12-2001
;       1.12    18-03-2003
;       1.13    27-01-2005
;       1.14    01-09-2009
;
;-----------------------------------------------------------------------------


PRO ADAS212,    adasrel, fortdir, userroot, centroot,                   $
                devlist, devcode, font_large, font_small, edit_fonts

                ;************************
                ;**** Initialisation ****
                ;************************

    adasprog = ' PROGRAM: ADAS212 V1.14'
    lpend = 0
    ipset = 0
    deffile = userroot+'/defaults/adas212_defaults.dat'
    device = ''

                ;******************************************
                ;**** Search for user default settings ****
                ;**** If not found create defaults     ****
                ;******************************************

    files = findfile(deffile)
    if files(0) eq deffile then begin

        restore,deffile
        inval.centroot = centroot+'/adf18/a09_a04/'
        inval.userroot = userroot+'/adf18/a09_a04/'
        inval.cent37 = centroot+'/adf37/'
        inval.user37 = userroot+'/adf37/'

    end else begin

        inval = {                                                       $
                ROOTPATH:userroot+'/adf18/a09_a04/',                    $
                FILE:'',                                                $
                CENTROOT:centroot+'/adf18/a09_a04/',                    $
                USERROOT:userroot+'/adf18/a09_a04/',                    $
                DIST:0,                                                 $
                DPARAM:0.0,                                             $
                ROOT37:userroot+'/adf37/',                              $
                FILE37:'',                                              $
                CENT37:centroot+'/adf37/',                              $
                USER37:userroot+'/adf37/' }

    outval = {                                                          $
                PASOUT:0, PASAPP:-1,                                    $
                PASREP:0, PASDSN:'',                                    $
                PASDEF:userroot+'/pass/adas212_adf04.pass', PASMES:'',  $
                TEXOUT:0, TEXAPP:-1,                                    $
                TEXREP:0, TEXDSN:'',                                    $
                TEXDEF:'paper.txt', TEXMES:'',                          $
                TITLE:''                                                $
             }

    end


                ;****************************
                ;**** Start fortran code ****
                ;****************************

    spawn, fortdir+'/adas212.out', unit=pipe, /noshell, PID=pid

                ;******************
                ;**** Get date ****
                ;******************

    date = xxdate()

LABEL100:
                ;**** Check FORTRAN still running ****

    if find_process(pid) eq 0 then goto, LABELEND

                ;************************************************
                ;**** Communicate with bbspf0 in fortran and ****
                ;**** invoke user interface widget for       ****
                ;**** Data file selection                    ****
                ;************************************************

    bcspf0, pipe, inval, dsfull, rep, FONT=font_large

;    check_pipe, pipe


                ;**** If cancel selected then end program ****

    if rep eq 'YES' then goto, LABELEND

LABEL300:
                ;**** Check FORTRAN still running ****

    if find_process(pid) eq 0 then goto, LABELEND

                ;************************************************
                ;**** Communicate with bbspf1 in fortran and ****
                ;**** invoke user interface widget for       ****
                ;**** Output options                         ****
                ;************************************************

    bcspf1, pipe, lpend, outval, dsfull, header, FONT=font_large

;    check_pipe, pipe
                ;**** If cancel selected then erase output ****
                ;**** messages and goto 100.               ****

    if lpend eq 1 then begin
        outval.pasmes = ''
        outval.texmes = ''
        goto, LABEL100
    end


                ;**** Create header for output. ****

    header = adasrel+adasprog+' DATE: '+date(0)+' TIME: '+date[1]


                ;***********************************************
                ;**** Now either begin the calculation or   ****
                ;**** set up the batch file depending on    ****
                ;**** what action has been requested.       ****
                ;***********************************************

    if lpend eq 0 then begin
        bcspf2, pipe, FONT=font_large
        printf, pipe, date[0], format='(a)'
    endif


    outval.pasapp = -1

                ;**** Back for more input options ****

    outval.pasmes=''
    outval.texmes=''

    GOTO, LABEL100

LABELEND:

                ;**** Save user defaults ****

    save, inval, outval, filename=deffile


END
