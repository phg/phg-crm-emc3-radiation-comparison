; Copyright (c) 1996, Strathclyde University.
; SCCS Info: Module @(#)$Header: /home/adascvs/idl/adas2xx/adas212/bcspf2.pro,v 1.4 2004/07/06 11:37:14 whitefor Exp $ Date $Date: 2004/07/06 11:37:14 $
;
;+
; PROJECT:
;       ADAS IBM MVS to UNIX conversion
;
; NAME:
;	BCSPF2
;
; PURPOSE:
;	IDL user interface and communications with ADAS212 FORTRAN
;	process via pipe.
;
; EXPLANATION:
;	This routine creates a progress indicator which is updated as
;	the FORTRAN progresses. The FORTRAN communicates via a pipe
;	the total number of stages and the stage which has been reached.
;
; USE:
;	The use of this routine is specific to ADAS212. See adas212.pro.
;
; INPUTS:
;	PIPE	- The IDL unit number of the bi-directional pipe to the
;		  ADAS212 FORTRAN process.
;
; OPTIONAL INPUTS:
;	None.
;
; OUTPUTS:
;	None
;
; OPTIONAL OUTPUTS:
;	None.
;
; KEYWORD PARAMETERS:
;
;	FONT	- Supplies the font to be used for the interface widgets.
;
; CALLS:
;
; SIDE EFFECTS:
;	This routine communicates with the ADAS212 FORTRAN processes
;	via UNIX pipes.
;	It also pops up an information widget which keeps the user 
;	updated as the calculation progresses.
;
; CATEGORY:
;	Adas system.
;
; WRITTEN:
;       William Osborn, Tessella Support Services plc, 31-July-1996
;
; MODIFIED:
;	1.1	William Osborn
;		First release
;	1.2	William Osborn
;		Added check for io error
;	1.3	Richard Martin.
;		Removed email address from popup warning.
;       1.4     Martin O'Mullane
;                - Added font keyword to widget_label statements.
;                - Centre progress bar in window.
;
; VERSION:
;	1.1	31-07-96
;	1.2	23-08-96
;	1.3	30-10-97
;       1.4     21-12-2001
;-
;-----------------------------------------------------------------------------

PRO bcspf2, pipe, FONT=font


                ;**** If there is an io error caused ****
		;**** by the Fortran crashing handle it ****

    ON_IOERROR, PIPE_ERR

                ;**** Set defaults for keywords ****

    IF NOT (KEYWORD_SET(font)) THEN font = ''

		;**** Create an information widget ****

    widget_control, /hourglass
    base   = widget_base(/column, xoffset=300, yoffset=200,               $
                          title = "ADAS212: INFORMATION")
    lab0   = widget_label(base, value='', font=font)
    lab1   = widget_label(base,                                           $
             value="    ADAS212 COMPUTATION UNDERWAY - PLEASE WAIT    " , $
             font=font)
    base_c = widget_base(base,/row,/align_center)
    grap   = widget_draw(base, ysize=20, xsize=480)
    lab2   = widget_label(base,                                           $
                          value = "READING DATA", font=font)
                          
    dynlabel, base

    widget_control, base, /realize

    num = 0
    readf, pipe, num

		;**** Update information widget ****

    next = 0
    step = 1000.0/num
		;**** Scale time in an ad hoc exponential fashion      ****
		;**** to account for the larger number of missing      ****
		;**** transitions as the parent index is increased.    ****
		;**** b should be changed if this is giving rediculous ****
		;**** rates but it works OK for clike/clike_o2ls.dat.  ****
		;**** Its magnitude should be increased to slow down   ****
		;**** the apparent processing time at later times.     ****
;    b = -4.0/num
;    c = 1.0/(1-exp(num*b))
    for i=0,num-1 do begin
    	readf, pipe, next
	p = float(i)/num*1000	;**** This would give a linear increase in
	q = p+step		;**** loop time but not processing time
;	p = (1-exp(i*b))*c*1000
;	q = (1-exp((i+1)*b))*c*1000
        for j=fix(p),fix(q) do begin
            x = (float(j)+1)/1000.0
            plots, [x, x],[0.0,1.0], /normal
        endfor
        widget_control, lab2, set_value="PROCESSING"+			$
		string(fix(q/10),format='(I3)')+"% COMPLETED"
    endfor

    goto, DONE

PIPE_ERR:
    mess = ['Error communicating with adas212 Fortran.',$
	    'The Fortran calculation has crashed.']
    action = popup(message=mess, title = '*** ADAS212 Error ***',$
		 buttons=['OK'])

DONE:
		;**** Destroy the information widget ****

    widget_control, base, /destroy

END
