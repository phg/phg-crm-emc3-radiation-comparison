; Copyright (c) 1996, Strathclyde University.
; SCCS info: Module @(#)$Header: /home/adascvs/idl/adas2xx/adas203/b3otg3.pro,v 1.1 2004/07/06 11:20:24 whitefor Exp $ Date @(#)$Header: /home/adascvs/idl/adas2xx/adas203/b3otg3.pro,v 1.1 2004/07/06 11:20:24 whitefor Exp $
;+
; PROJECT:
;       ADAS IBM MVS to UNIX conversion
;
; NAME:
;       B3OTG3
;
; PURPOSE:
;       Communication with ADAS203 FORTRAN via UNIX pipe and
;       graphics output.
;
; EXPLANATION:
;       The routine begins by reading information from the ADAS203
;	FORTRAN routine B3OTG3.FOR via a UNIX pipe.  Then the IDL graphical
;       output routine for ADAS203 scaled rate parameter plots is invoked.
;
; USE:
;       This routine is specific to adas203.
;
; INPUTS:
;       DSFULL   - Script file name
;
;       PIPE     - The IDL unit number of the bi-directional pipe to the
;                  ADAS203 FORTRAN process.
;
;       UTITLE   - Optional comment by user
;
;       LDEF3    - Integer; 0 if default scaling required 1 if user
;                  specified scaling to be used.
;
;       XMIN     - String; User specified x-axis minimum, number as string.
;
;       XMAX     - String; User specified x-axis maximum, number as string.
;
;       YMIN     - String; User specified y-axis minimum, number as string.
;
;       YMAX     - String; User specified y-axis maximum, number as string.
;
;       HRDOUT   - Integer; 0 if no hardcopy required, 1 if hardcopy.
;
;       HARDNAME - String; file name for hardcopy output.
;
;       DEVICE   - String; IDL name for hardcopy output device.
;
;       HEADER   - ADAS version header information for inclusion in the
;                  graphical output.
;
;       BITFILE - String; the path to the directory containing bitmaps
;                 for the 'escape to series menu' button.
;
;       ILABEL  - Integer; 0=Do not label graphs, 1=label them
;
; OPTIONAL INPUTS:
;       None.
;
; OUTPUTS:
;       GOMENU  - Int; flag - set to 1 if user has selected 'escape direct
;                 to series menu', otherwise 0
;
; OPTIONAL OUTPUTS:
;       None.
;
; KEYWORD PARAMETERS:
;       FONT    - String; Name of the font to be used for the graphical
;                 output widget.
;
; CALLS:
;       ADAS203_3_PLOT	ADAS203 Scaled A-value graphical output.
;
; SIDE EFFECTS:
;       This routine reads information from FORTRAN via a UNIX pipe.
;
; CATEGORY:
;       Adas system.
;
; WRITTEN:
;       Tim Hammond, Tessella Support Services plc, 28th March 1996
;
; MODIFIED:
;       1.1     Tim Hammond
;               First version
;
; VERSION:
;       1.1     01-04-96
;
;-
;-----------------------------------------------------------------------------

PRO b3otg3, dsfull, pipe, utitle, ldef3, xmin, xmax, ymin, ymax,	$
            hrdout, hardname, device, header, bitfile,			$
            gomenu, ilabel, FONT=font

                ;**** Set defaults for keywords ****

    IF NOT (KEYWORD_SET(font)) THEN font = ''

                ;**** Declare variables for input ****

    input = 0
    fdum = 0.0
    sdum = ' '
                                                        
                ;********************************
                ;**** Read data from fortran ****
                ;********************************

    readf, pipe, input
    izmax = input	
    readf, pipe, input
    itemp = input
    xvals = fltarr(itemp)
    yvals = fltarr(itemp,izmax)
    nuc = intarr(izmax+1)
    recd = intarr(izmax+1)
    elem = strarr(izmax+1)
    for i=0, (izmax-1) do begin
	for j=0, (itemp-1) do begin
 	    readf, pipe, fdum
 	    yvals(j,i) = fdum
	endfor
    endfor
    for i=0, (itemp-1) do begin
    	readf, pipe, fdum
        xvals(i) = fdum
    endfor
    readf, pipe, idum
    maxt = idum
    xo = fltarr(maxt)
    z = fltarr(maxt)
    for i=0, maxt-1 do begin
    	readf, pipe, fdum
    	xo(i) = fdum
    	readf, pipe, fdum
    	z(i) = fdum
    endfor
    readf, pipe, sdum
    species = sdum
    readf, pipe, sdum
    seq = sdum
    readf, pipe, sdum
    seq = seq + '  ' + sdum
    for i=0, (izmax-1) do begin
        readf, pipe, idum
 	nuc(i) = idum
        readf, pipe, idum
 	recd(i) = idum
        readf, pipe, sdum
 	elem(i) = strupcase(sdum)
    endfor
    readf, pipe, idum
    nuc(izmax) = idum
    readf, pipe, idum
    recd(izmax) = idum
    readf, pipe, sdum
    elem(izmax) = strupcase(sdum)
    readf, pipe, idum
    libpt = idum

                 ;***********************
                 ;**** Plot the data ****
                 ;***********************
 
     adas203_3_plot, dsfull, utitle, ldef3, xvals, yvals, xo, z,  	$
                     species, seq, hrdout, hardname, device, header,	$
 		     izmax, xmin, xmax, ymin, ymax, nuc, recd, elem,	$
                     bitfile, gomenu, itemp, libpt, izmax, ilabel,	$
	             FONT=font
                                                    
END
