; Copyright (c) 1996, Strathclyde University.
; SCCS info: Module @(#)$Header: /home/adascvs/idl/adas2xx/adas203/adas203.pro,v 1.9 2004/07/06 10:12:04 whitefor Exp $ Date $Date: 2004/07/06 10:12:04 $
;+
; PROJECT:
;       ADAS IBM MVS to UNIX conversion
;
; NAME:
;       ADAS203
;
; PURPOSE:
;       The highest level routine for the ADAS 203 program.
;
; EXPLANATION:
;       This routine is called from the main adas system routine, adas.pro,
;       to start the ADAS 203 application.  Associated with adas203.pro
;	is a FORTRAN executable. The IDL code provides the user
;       interface and output graphics whilst the FORTRAN code reads
;       in data files, performs numerical processing and creates the
;       output files. The IDL code communicates with the FORTRAN
;       executable via a bi-directional UNIX pipe.  The unit number
;       used by the IDL for writing to and reading from this pipe is
;       allocated when the FORTRAN executable is 'spawned' (see code
;       below).  Pipe communications in the FORTRAN process are to
;       stdin and stdout, i.e streams 5 and 6.
;
;       The FORTRAN code is an independent process under the UNIX system.
;       The IDL process can only exert control over the FORTRAN in the
;       data which it communicates via the pipe.  The communications
;       between the IDL and FORTRAN must be exactly matched to avoid
;       input conversion errors.  The correct ammounts of data must be
;       passed so that neither process 'hangs' waiting for communications
;       which will never occur.
;
;       The FORTRAN code performs some error checking which is
;       independent of IDL.  In cases of error the FORTRAN may write
;       error messages.  To prevent these error messages from conflicting
;       with the pipe communications all FORTRAN errors are written to
;       output stream 0, which is stderr for UNIX.  These error messages
;       will appear in the window from which the ADAS session/IDL session
;       is being run. 
;
;       In the case of severe errors the FORTRAN code may terminate
;       itself prematurely.  In order to detect this, and prevent the
;       IDL program from 'hanging' or crashing, the IDL checks to see
;       if the FORTRAN executable is still an active process before
;       each group of pipe communications.  The process identifier
;       for the FORTRAN process, PID, is recorded when the process is
;       first 'spawned'.  The system is then checked for the presence
;       of the FORTRAN PID. 
;
; USE:
;       First the system settings must be established by calling
;       adas_sys_set.pro then adas203.pro is called to start the
;       ADAS 203 application;
;
;       adas_sys_set, adasrel, fortdir, userroot, centroot,             $
;                     devlist, devcode, font_large, font_small, edit_fonts
;       adas203,   adasrel, fortdir, userroot, centroot, devlist,       $
;                  devcode, font_large, font_small, edit_fonts
;
; INPUTS:
;       ADASREL -  A string indicating the ADAS system version,
;                  e.g ' ADAS RELEASE: ADAS93 V1.4'.  The first
;                  character should be a space.
;
;       FORTDIR -  A string holding the path to the directory where the
;                  FORTRAN executables are, e.g '/disk/adas/fortran/exec'
;
;       USERROOT - A string holding the path to the root directory of
;                  the user's adas data. e.g '/disk/bowen/adas'
;                  This root directory will be used by adas to construct
;                  other path names.  For example the users default data
;                  for adas205 should be in /disk/bowen/adas/adf04.  In
;                  particular the user's default interface settings will
;                  be stored in the directory USERROOT+'/defaults'.  An
;                  error will occur if the defaults directory does not
;                  exist.
;
;       CENTROOT - Like USERROOT, but this directory points to the
;                  central data area for the system.  User defaults are
;                  not stored on CENTROOT.
;
;       DEVLIST -  A string array of hardcopy device names, used for
;                  graphical output. e.g ['Post-Script','HP-PCL','HP-GL']
;                  This array must mirror DEVCODE.  DEVCODE holds the
;                  actual device names used in a SET_PLOT statement.
;
;       DEVCODE -  A string array of hardcopy device code names used in
;                  the SET_PLOT statement, e.g ['PS', 'PCL', 'HP']
;                  This array must mirror DEVLIST.
;
;       FONT_LARGE - The name of a larger font e.g 'courier_bold14'
;
;       FONT_SMALL - The name of a smaller font e.g 'courier_bold12'
;
;       EDIT_FONTS - A structure of two fonts used in the table editor
;                    adas_edtab.pro, {font_norm:'',font_input:''}.
;                    The two fonts are used to differentiate between
;                    editable and non-editable parts of the table. You
;                    may have to experiment to get two fonts which give
;                    the same default spacing in the edit widget.
;                    e.g {font_norm:'helvetica_bold14', 		$
;                         font_input:'helvetica_oblique14'}
;
; OPTIONAL INPUTS:
;       None
;
; OUTPUTS:
;       None - Note this routine should leave all inputs un-modified.
;
; OPTIONAL OUTPUTS:
;       None
;
; KEYWORD PARAMETERS:
;       None
;
; CALLS:
;       FIND_PROCESS    Checks to see if a given process is active.
;	B3SPF0		Controls the input options screen.
;	B3ISPF		Controls the processing options screen.
;	B3SPF1		Controls the output options screen.
;	B3OTG1		Controls transition wave no. plot.
;	B3OTG2		Controls A-value plot.
;	B3OTG3		Controls rate parameter plot.
;       XXDATE          Get date and time from operating system.
;
; SIDE EFFECTS:
;       This routine spawns a FORTRAN executable.
;
; CATEGORY:
;       Adas system.
;
; WRITTEN:
;       Tim Hammond, Tessella Support Services plc, 28th February 1996
;
; MODIFIED:
;       1.1     Tim Hammond 
;		First version
;	1.2	Tim Hammond
;		Added extra i/o error check for b3spf1
;	1.3	Tim Hammond
;		Ensured that information widget is always destroyed (even
;		if there is an error).
;	1.4	Tim Hammond
;		Added default definition for information widget base
;		'infobase'.
;	1.5	William Osborn
;		Increased version number to 1.2
;	1.6	William Osborn
;		Increased version number to 1.3
;	1.7	Richard Martin
;		Increased version number to 1.4
;	1.8	Richard Martin
;		Increased version number to 1.5
;	1.9	Richard Martin
;		Increased version number to 1.6
;
; VERSION:
;       1.1     01-04-96
;	1.2	01-04-96
;	1.3	02-04-96
;	1.4	04-04-96
;	1.5	11-07-96
;	1.6	14-10-96
;	1.7	04-04-97
;	1.8	23-06-98
;	1.9	10-11-00
;
;-
;-----------------------------------------------------------------------------

PRO adas203, adasrel, fortdir, userroot, centroot, devlist, devcode, 	$
             font_large, font_small, edit_fonts

                ;************************
                ;**** Initialisation ****
                ;************************

    ON_IOERROR, LABELERROR
    adasprog = ' PROGRAM: ADAS203 V1.6'
    lpend = 0
    gomenu = 0
    deffile = userroot + '/defaults/adas203_defaults.dat'
    bitfile = centroot + '/bitmaps'
    device = ''
    infobase = 0L

                ;******************************************
                ;**** Search for user default settings ****
                ;**** If not found create defaults     ****
                ;******************************************

    files = findfile(deffile)
    if files(0) eq deffile then begin
        restore, deffile
        inval.centroot = centroot + '/adf05/'
        inval.userroot = userroot + '/adf05/'
    endif else begin
	inval   = {	ROOTPATH	:	userroot + '/adf05/',	$
			FILE		:	'',			$
			CENTROOT	:	centroot + '/adf05/',	$
			USERROOT	:	userroot + '/adf05/' 	}
	procval = {	NEW		:	-1			}
	copdefval = userroot + '/pass/copase.pass'
 	outval  = {	ITOGGLE		:	0,			$
			ILABEL		:	1,			$
                        LGRAPHNO        :       0,                      $
                        XMIN1           :       '',                     $
                        XMAX1           :       '',                     $
                        YMIN1           :       '',                     $
                        YMAX1           :       '',                     $
                        XMIN2           :       '',                     $
                        XMAX2           :       '',                     $
                        YMIN2           :       '',                     $
                        YMAX2           :       '',                     $
                        XMIN3           :       '',                     $
                        XMAX3           :       '',                     $
                        YMIN3           :       '',                     $
                        YMAX3           :       '',                     $
                        LDEF1           :       0,                      $
                        LDEF2           :       0,                      $
                        LDEF3           :       0,                      $
                        GRPOUT          :       0,                      $
                        GTIT1           :       '',                     $
                        HRDOUT          :       0,                      $
                        HARDNAME        :       '',                     $
                        GRPDEF          :       '',                     $
                        GRPFMESS        :       '',                     $
                        DEVSEL          :       -1,                     $
                        TEXOUT          :       0,                      $
                        TEXAPP          :       -1,                     $
                        TEXREP          :       0,                      $
                        TEXDSN          :       '',                     $
                        TEXDEF          :       'paper.txt',            $
                        TEXMES          :       '',                     $
			COPOUT		:	0,			$
			COPAPP		:	-1,			$
			COPREP		:	0,			$
			COPDSN		:	'',			$
			COPDEF		:	copdefval,		$
			COPMES		:	''			}
    endelse

                ;****************************
                ;**** Start fortran code ****
                ;****************************

    spawn, fortdir+'/adas203.out', unit=pipe, /noshell, PID=pid
     
                ;************************************************
                ;**** Get date and write to FORTRAN via pipe ****
                ;************************************************

    date = xxdate()
    printf, pipe, date(0)

		;***********************************
                ;**** Create header for output. ****
		;***********************************

    header = adasrel + adasprog + ' DATE: ' + date(0) + ' TIME: ' + date(1)

LABEL100:

                ;************************************************
                ;**** Invoke user interface widget for       ****
                ;**** Data file selection                    ****
                ;************************************************

    b3spf0, pipe, inval, dsfull, rep, FONT=font_large
    firstrun = 0

		;*********************************************
                ;**** If cancel selected then end program ****
		;*********************************************

    if rep eq 'YES' then goto, LABELEND

LABEL200:

                ;************************************************
                ;**** Invoke user interface widget for       ****
                ;**** processing options selection           ****
                ;************************************************

    b3ispf, pipe, lpend, inval, procval, dsfull, gomenu, bitfile,  	$
            infobase, firstrun, FONT_LARGE=font_large, 			$
	    ioerror, FONT_SMALL=font_small, EDIT_FONTS=edit_fonts 

		;***********************************
		;**** First check for IO-errors ****
		;***********************************

    if ioerror eq 1 then goto, LABELEND

		;*************************************************
                ;**** If menu button clicked, end the program ****
		;*************************************************

    if gomenu eq 1 then begin
        printf, pipe, 1
        goto, LABELEND
    endif else begin
        printf, pipe, 0
    endelse

		;******************************************
                ;**** If cancel selected then goto 100 ****
		;******************************************

    if lpend eq 1 then begin
        firstrun = 0
        goto, LABEL100
    endif

		;*************************************************
		;**** Wait for signal to proceed from FORTRAN ****
		;**** and then destroy the information widget ****
		;*************************************************

    idum = 0
    readf, pipe, idum
    widget_control, infobase, /destroy

LABEL300:
		;******************************************
		;**** Invoke user interface widget for ****
		;**** output options screen 	       ****
		;******************************************

    b3spf1, pipe, inval, procval, outval, bitfile, pid, lpend, 		$
	    gomenu, header, ioerror, FONT_LARGE=font_large, 		$
	    FONT_SMALL=font_small, DEVLIST=devlist

		;***********************************
		;**** First check for IO-errors ****
		;***********************************

    if ioerror eq 1 then goto, LABELEND

		;*************************************************
                ;**** If menu button clicked erase any output ****
		;**** and then end the program                ****
		;*************************************************

    if lpend eq 0 then begin
	outval.texmes = ''
	outval.copmes = ''
        goto, LABELEND
    endif else if lpend eq 1 then begin

		;***************************************
                ;**** If cancel selected then erase ****
		;**** output messages and goto 200  ****
		;***************************************

	outval.texmes = ''
	outval.copmes = ''
	goto, LABEL200
    endif else if lpend eq 2 then begin

		;*************************************************
		;**** If graphing selected then call plotting ****
		;**** routine                                 ****
		;*************************************************

		;**** Hardcopy output ****

        hrdout = outval.hrdout
        hardname = outval.hardname
        if outval.devsel ge 0 then device = devcode(outval.devsel)

                ;*** user title for graph ***

        utitle = outval.gtit1

        if outval.lgraphno eq 0 then begin
	    b3otg1, dsfull, pipe, utitle, outval.ldef1, outval.xmin1,   $
                    outval.xmax1, outval.ymin1, outval.ymax1, hrdout,   $
                    hardname, device, header, bitfile, gomenu, 		$
		    FONT=font_large
	endif else if outval.lgraphno eq 1 then begin
	    b3otg2, dsfull, pipe, utitle, outval.ldef2, outval.xmin2,   $
                    outval.xmax2, outval.ymin2, outval.ymax2, hrdout,   $
                    hardname, device, header, bitfile, gomenu, 		$
		    FONT=font_large
	endif else begin
	    b3otg3, dsfull, pipe, utitle, outval.ldef3, outval.xmin3,   $
                    outval.xmax3, outval.ymin3, outval.ymax3, hrdout,   $
                    hardname, device, header, bitfile, gomenu, 		$
		    outval.ilabel, FONT=font_large
	endelse

                ;**** If menu button clicked, tell FORTRAN to stop ****

        if gomenu eq 1 then begin
            outval.texmes = ''
            outval.copmes = ''
            printf, pipe, 1
            goto, LABELEND
        endif else begin
            printf, pipe, 0
        endelse

		;************************************************
		;**** If output and exit selected then erase ****
		;**** output messages, reset replace buttons ****
		;**** and end.                               ****
		;************************************************

    endif else if lpend eq 3 then begin
	outval.texmes = ''
	outval.copmes = ''
	outval.texrep = 0
	outval.coprep = 0
	goto, LABELEND
    endif 

		;**************************************
		;**** Back for more output options ****
		;**************************************

    GOTO, LABEL300

LABELERROR:
    print, '*********************** ADAS203 ERROR *************************'
    print, '*****                                                     *****'
    print, '*****     A FATAL ERROR OCCURRED WHILST COMMUNICATING     *****'
    print, '*****            WITH THE FORTRAN CHILD PROCESS           *****'
    print, '*****                                                     *****'
    print, '********************* ADAS203 TERMINATING *********************'

LABELEND:

		;*****************************************************
		;**** Test if information widget is still present ****
		;**** If it is then destroy it                    ****
		;*****************************************************

    test = widget_info(infobase, /valid_id)
    if test eq 1 then widget_control, infobase, /destroy

		;****************************
                ;**** Save user defaults ****
		;****************************

    save, inval, procval, outval, filename=deffile

END
