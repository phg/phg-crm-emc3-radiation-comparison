; Copyright (c) 1996 Strathclyde University .
; SCCS info: Module @(#)$Header: /home/adascvs/idl/adas2xx/adas203/plot203_3.pro,v 1.2 2004/07/06 14:29:33 whitefor Exp $ Date $Date: 2004/07/06 14:29:33 $
;+
; PROJECT:
;       ADAS IBM MVS to DEC UNIX conversion
;
; NAME:
;       PLOT203_3
;
; PURPOSE:
;       Plot A-value graphs for ADAS203.
;
; EXPLANATION:
;       This routine plots ADAS203 output for one plot
;
; USE:
;       Use is specific to ADAS203. See adas203_3_plot.pro for
;       example.
;
; INPUTS:
;
;	LDEF3	-	Integer; 1 if user specified axis limits to 	
;			be used, 0 if default scaling to be used.
;
;	X	-	Fltarr; x-values to plot
;
;	Y	-	Fltarr; y-values to plot 
;
;	XO	-	Fltarr; x-value of interpolation point
;
;	Z	-	Fltarr; y-value of interpolation point
;
;	RIGHTSTRING -	String; first column of right hand label
;
;	RIGHTSTRING2- 	String; second column of right hand label
;
;	RIGHTSTRING3- 	String; third column of right hand label
;
;	RIGHTSTRING4- 	String; fourth column of right hand label
;
;	TITLE	-	String; heading to go above graph
;
;	XMIN3	-	Float; user-defined x-axis minimum
;
;	XMAX3	-	Float; user-defined x-axis maximum
;
;	YMIN3	-	Float; user-defined y-axis minimum
;
;       YMAX3   -       Float; user-defined y-axis maximum
;
;       LIBPT   - Integer; flag = 0 when bad point option not selected
;                                 1 when bad point option is selected
;
;       ITEMP   - Integer; the number of input temps. for the selected
;                 transition.
;
;       IZMAX   - Integer; the number of sequence members in the input file
;
;	ILABEL	- Integer; 0=Do not label graphs, 1=label them
;
; OPTIONAL INPUTS:
;       None.
;
; OUTPUTS:
;       None.
;
; OPTIONAL OUTPUTS:
;       None.
;
; KEYWORD PARAMETERS:
;       FONT    - String; Name of font passed to graphical output
;                 widget.
;
; CALLS:
;       None.
;
; SIDE EFFECTS:
;       None.
;
; CATEGORY:
;       Adas system.
;
; WRITTEN:
;       Tim Hammond, Tessella Support Services plc,  28th March 1996
;
; MODIFIED:
;       1.1     Tim Hammond      
;		First version
;	1.2     William Osborn
;               Added check for new environment variable VERY_SMALL
;               If this is set to the string "YES" (preferably in the
;               .adas_setup file) then the font size used on the graphs
;               is reduced and the positioning of labels adjusted
;               slightly to make things fit on a very small screen.
;
; VERSION:
;	1.1	01-04-96
;	1.2	11-10-96
;
;-
;----------------------------------------------------------------------------

PRO plot203_3, ldef3, x, y, xo, z, title, rightstring, rightstring2,   	$
               rightstring3, rightstring4, xmin3, xmax3, ymin3, ymax3,	$
	       libpt, izmax, itemp, ilabel

    COMMON Global_lw_data, left, right, tp, bot, grtop, grright

                ;****************************************************
                ;**** Suitable character size for current device ****
                ;**** Aim for 60 characters in y direction.      ****
                ;****************************************************

    charsize = (!d.y_vsize/!d.y_ch_size)/60.0
    small_check = GETENV('VERY_SMALL')
    if small_check eq 'YES' then charsize=charsize*0.8

                ;**** Initialise titles ****

    ytitle = 'SCALED RATE PARAMETER'
    xtitle = 'Z-SCALED ELECTRON TEMPERATURE (K)'
    righthead = '----- SEQUENCE MEMBERS  -----!C!C'+			$
                'INDX  NUC.CHG.  RECD.ION.  ELEM'
    erase
		;*****************************************************
		;**** Set xo and z back to arrays if they've been ****
		;**** erroneously converted to scalars by IDL     ****
		;*****************************************************

    xonew = fltarr(1)
    znew = fltarr(1)
    xonew(0) = xo(0)
    znew(0) = z(0)
    xosize = size(xo)
    zsize = size(z)
    if xosize(0) ne 1 then xo=xonew
    if zsize(0) ne 1 then z=znew

                ;**** How many points to plot ****

    npts = size(x)
    npts = npts(1)

		;**** Find x and y ranges for auto scaling,        ****
                ;**** check x and y in range for explicit scaling. ****

    makeplot = 1
    style = 0
    ystyle = 0

    if ldef3 eq 0 then begin

                ;**** identify values in the valid range ****
                ;**** plot routines only work within 	 ****
                ;**** single precision limits.       	 ****

	xvals = where (x gt 1.0e-37 and x lt 1.0e37)
	yvals = where (y gt 1.0e-37 and y lt 1.0e37)
	if xvals(0) gt -1 then begin
	    maxx = max(x(xvals))
	    minx = min(x(xvals))
	endif else begin
	    makeplot = 0
	endelse
	if yvals(0) gt -1 then begin
	    maxy = max(y(yvals))
	    miny = min(y(yvals))
	endif else begin
	    makeplot = 0
	endelse
	if makeplot eq 1 then begin
	    if miny le 1.0e-36 then begin
		ystyle = 1
		miny = 1.0e-36
	    endif else begin
		ystyle = 0
	    endelse
	endif
	style = 0
	if miny eq max(y) then begin
	    maxy=miny*10.0
	    miny=miny/10.0
	    ystyle = 1
	endif
    endif else begin
        minx = xmin3
	maxx = xmax3
	miny = ymin3
	maxy = ymax3
	xvals = where(x gt minx and x lt maxx)
	yvals = where(y gt miny and y lt maxy)
	if xvals(0) eq -1 or yvals(0) eq -1 then begin
	    makeplot = 0
	endif else begin
	    makeplot = 1
	endelse
	style = 1
        ystyle = 1
    endelse

    if makeplot eq 1 then begin

                ;**** Set up log-log plotting axes ****

	plot_oo, [minx, maxx], [miny, maxy], /nodata, 			$
                 position=[left, bot, grright, grtop],			$
                 xtitle=xtitle, ytitle=ytitle, xstyle=style, 		$
		 ystyle=ystyle, charsize=charsize
	xtop = 10^(!x.crange(1))
	xbot = 10^(!x.crange(0))
	ytop = 10^(!y.crange(1))
	ybot = 10^(!y.crange(0))

                ;*********************************
                ;**** Make and annotate plots ****
                ;*********************************

	for i=0, (izmax-1) do begin
	    oplot, x, y(*,i), linestyle=2

		;**** Find suitable annotation point ****

	    if ilabel eq 1 then begin
	        istart = fix(itemp/2)
	        xco = -0.0909
	        yco = -0.0909
	        if (x(istart) lt xtop) and (x(istart) gt xbot) and 	$
	        (y(istart,i) lt ytop) and (y(istart,i) gt ybot) then begin
		    xco = x(istart)
		    yco = y(istart,i)
	        endif else begin
		    if (x(istart) lt xbot) or (y(istart,i) lt ybot) then begin
		        for j=istart, (izmax-1) do begin
	                    if (x(j) lt xtop) and (x(j) gt xbot) and 	$
                            (y(j,i) lt ytop) and (y(j,i) gt ybot) then begin
                	        xco = x(j)
                	        yco = y(j,i)
            		    endif
		        endfor
		    endif else if (x(istart) gt xtop) or 		$
	            (y(istart,i) gt ytop) then begin
                        for j=istart, 0, -1 do begin
                            if (x(j) lt xtop) and (x(j) gt xbot) and  	$
                            (y(j,i) lt ytop) and (y(j,i) gt ybot) then begin
                                xco = x(j)
                                yco = y(j,i)
                            endif
                        endfor
		    endif
	        endelse
	        if (xco ge 0 and yco ge 0) then begin
		    xlab = (0.05 + (0.05*i)) * (alog10(xtop) - alog10(xbot))
		    xlab = 10^(xlab + alog10(xbot))
		    if yco lt (0.9*ytop) then begin
		        ylab = 0.9 * (alog10(ytop) - alog10(ybot))
		    endif else begin
		        ylab = 0.05 * (alog10(ytop) - alog10(ybot))
		    endelse
		    ylab = 10^(ylab + alog10(ybot))
		    labelstring = strcompress(string(i+1), /remove_all)
		    xyouts, xlab, ylab, labelstring, alignment=1.0
		    oplot, [xlab, xco], [ylab,yco]
	        endif
	    endif
	endfor
	oplot, xo, z, linestyle=0
    endif else begin			;no plot possible
        xyouts, 0.2, 0.5, /normal, charsize=charsize*1.5,               $
                '---- No data lies within range ----'
    endelse

                ;**** Output title above graphs ****
    
    xyouts, 0.1, 0.9, title, charsize=charsize, /normal

                ;**** Output titles to right of graphs ****

    badstring = '#     BAD POINTS OPTION SELECTED'
    if libpt eq 1 then begin
    	rightstring = rightstring + '!C' + badstring
    endif
    xyouts, 0.69, 0.8, righthead, charsize=charsize, /normal
    xyouts, 0.69, 0.72, rightstring, charsize=charsize, /normal
    xyouts, 0.75, 0.72, rightstring2, charsize=charsize, /normal
    xyouts, 0.83, 0.72, rightstring3, charsize=charsize, /normal
    xyouts, 0.89, 0.72, rightstring4, charsize=charsize, /normal

END
