; Copyright (c) 1996, Strathclyde University.
; SCCS Info: Module @(#)$Header: /home/adascvs/idl/adas2xx/adas203/b3spf1.pro,v 1.2 2004/07/06 11:21:18 whitefor Exp $ Date $Date: 2004/07/06 11:21:18 $
;+
; PROJECT:
;       ADAS IBM MVS to UNIX conversion
;
; NAME:
;	B3SPF1
;
; PURPOSE:
;	IDL user interface and communications with ADAS203 FORTRAN
;	process via pipe.
;
; EXPLANATION:
;	First this routine invokes the 'Output Options' part of the
;	user interface for adas 203. Secondly the results of the user's
;	interactions are written to the FORTRAN process via the UNIX pipe.
;	Communications are with the FORTRAN subroutine B3SPF1.
;
; USE:
;	The use of this routine is specific to ADAS203, See adas203.pro.
;
; INPUTS:
;	PIPE	- The IDL unit number of the bi-directional pipe to the
;		  ADAS203 FORTRAN process.
;
;	INVAL	- A structure holding the final settings of the input
;		  options widget.
;
;	PROCVAL	- A structure holding the final settings of the processing
;		  options widget.
;
;	OUTVAL	- A structure which determines the initial settings of
;		  the output options widget.  The initial value is
;		  set in adas203.pro.  OUTVAL is passed un-modified
;		  through to cw_adas203_out.pro, see that routine for a full
;		  description.
;
;       BITFILE - String; the path to the dirctory containing bitmaps
;                 for the 'escape to series menu' button.
;
;	PID	- Integer; the process id of the FORTRAN part of the code.
;
;       HEADER  - Header information used for text output.
;
; OPTIONAL INPUTS:
;	None.
;
; OUTPUTS:
;	LPEND	- Integer; indicates user action on leaving output
;		  selections.  
;			0 = 'Return to series menu'
;			1 = 'Return to processing screen' = 'Cancel'
;			2 = 'View graphs'
;			3 = 'Produce output and exit adas 203'
;			4 = 'Produce output and return to output screen'
;
;	OUTVAL	- On output the structure records the final settings of
;		  the output selection widget if the user pressed 
;		  either button 3 or buttons 2 or 4 followed by buttons
;		  0 or 1.
;
;       GOMENU  - Int; flag - set to 1 if user has selected 'escape direct
;                 to series menu' , otherwise 0
;
;       IOERROR - 0 = no problems during pipe comms.
;                 1 = there was a problem during pipe comms.
;
; OPTIONAL OUTPUTS:
;	None.
;
; KEYWORD PARAMETERS:
;	DEVLIST	- A string array;  Each element of the array is the
;		  name of a hardcopy graphics output device.  This
;		  list must mirror the DEVCODE list.  DEVLIST is the
;		  list which the user will see.
;		  e.g ['Post-Script','HP-PCL','HP-GL']
;
;	FONT_LARGE- Supplies the large font to be used.
;
;	FONT_SMALL- Supplies the small font to be used.
;
; CALLS:
;	ADAS203_OUT	User interface - output options.
;
; SIDE EFFECTS:
;	This routine communicates with the ADAS203 FORTRAN process
;	via a UNIX pipe.
;
; CATEGORY:
;	Adas system.
;
; WRITTEN:
;       Tim Hammond, Tessella Support Services plc, 07-03-96
;
; MODIFIED: 
;	1.1	Tim Hammond
;		First version
;       1.2     Tim Hammond
;               Added extra error checking for FORTRAN
;
; VERSION:
;	1.1	21-03-96
;	1.2	01-04-96
;
;-
;-----------------------------------------------------------------------------

PRO b3spf1, pipe, inval, procval, outval, bitfile, pid, lpend,  	$
            gomenu, header, ioerror, DEVLIST=devlist, 			$
	    FONT_LARGE=font_large, FONT_SMALL=font_small

    ON_IOERROR, LABELERROR

                ;**** Set defaults for keywords ****

    IF NOT (KEYWORD_SET(font_large)) THEN font_large = ''
    IF NOT (KEYWORD_SET(font_small)) THEN font_small = ''

		;**************************************
		;**** Pop-up output options widget ****
		;**************************************

    adas203_out, inval, procval, outval, action, bitfile,  		$
                 DEVLIST=devlist, FONT_LARGE=font_large, 		$
                 FONT_SMALL=font_small

		;*******************************************
		;**** Act on the output from the widget ****
		;**** There are five possible actions   ****
		;*******************************************

    if action eq 'Menu' then begin
	lpend = 0
    endif else if action eq 'Cancel' then begin
	lpend = 1
    endif else if action eq 'View' then begin
	lpend = 2
    endif else if action eq 'Exit' then begin
	lpend = 3
    endif else if action eq 'Return' then begin
	lpend = 4
    endif

		;*******************************
		;**** Write data to fortran ****
		;*******************************

    printf, pipe, lpend
    if lpend eq 2 then begin
	printf, pipe, outval.lgraphno
    endif
    if (lpend eq 3 or lpend eq 4) then begin
	printf, pipe, outval.copout
	if outval.copout eq 1 then begin
	    outval.copdef = outval.copdsn
	    printf, pipe, outval.copdsn
	    spawn, 'whoami', userid
	    printf, pipe, userid
	endif
 	printf, pipe, outval.texout
	if outval.texout eq 1 then begin
	    outval.texdef = outval.texdsn
	    printf, pipe, outval.texdsn
	    printf, pipe, header
	endif

		;**** Wait for 'Finished' signal from FORTRAN ****

        idum = 0
        while idum ne 1 do begin
	    readf, pipe, idum
        endwhile
    endif
    if lpend eq 4 then begin
        if outval.texout eq 1 then begin
	    outval.texdef = outval.texdsn
            if outval.texrep ge 0 then outval.texrep = 0
      	    outval.texmes = 'Output written to file.'
        endif
	if outval.copout eq 1 then begin
	    outval.copdef = outval.copdsn
	    if outval.coprep ge 0 then outval.coprep = 0
	    outval.copmes = 'Output written to file.'
	endif
    endif
    ioerror = 0
    goto, LABELEND

LABELERROR:
    print, '************************ B3SPF1 ERROR *************************'
    print, '*****                                                     *****'
    print, '*****     A FATAL ERROR OCCURRED WHILST COMMUNICATING     *****'
    print, '*****            WITH THE FORTRAN CHILD PROCESS           *****'
    print, '*****                                                     *****'
    print, '********************** B3SPF1 TERMINATING *********************'
    ioerror = 1

LABELEND:

END
