; Copyright (c) 1996, Strathclyde University 
; SCCS INFO : Module @(#)$Header: /home/adascvs/idl/adas2xx/adas203/b3ispf.pro,v 1.3 2004/07/06 11:19:47 whitefor Exp $ Date $Date: 2004/07/06 11:19:47 $
;+
; PROJECT:
;       ADAS IBM MVS to UNIX conversion
;
; NAME:
;	B3ISPF
;
; PURPOSE:
;	IDL user interface and communications with ADAS203 FORTRAN
;	process via pipe.
;
; EXPLANATION:
;	The routine begins by reading information from the ADAS203
;	FORTRAN process via a UNIX pipe.  Then part of the ADAS203
;	IDL user interface is invoked to determine how the user
;	wishes to process the input dataset.  When the user's
;	interactions are complete the information gathered with
;	the user interface is written back to the FORTRAN process
;	via the pipe.  Communications are to the FORTRAN subroutine
;	B3ISPF.
;
; USE:
;	The use of this routine is specific to ADAS203, see adas203.pro.
;
; INPUTS:
;	PIPE	- The IDL unit number of the bi-directional pipe to the
;		  ADAS203 FORTRAN process.
;
;	LPEND	- Integer, 0 if the user pressed the 'Done' or 'Menu' button
;		  or 1 if the user pressed 'Cancel'.  Maps directly onto
;		  the logical variable LPEND in ADAS203 FORTRAN.
;
;	PROCVAL	- A structure which determines the initial settings of
;		  the processing options widget.  The initial value is
;		  set in adas203.pro.  If adas203.pro passes a blank 
;		  dummy structure of the form {NEW:-1} into PROCVAL then
;		  PROCVAL is reset to a default structure.
;
;		  The PROCVAL structure is;
;		procval = {   	NEW   	: 	0,             		$
;              		      	TITLE 	: 	'',            		$
;				ITOGGLE1:	0,			$
;				IZS	:	0,			$
;				IZ0 	:	0,			$
;				IZ0VAL	:  	intarr(izdimd),		$
;				ITRAN	:	0,			$
;				IL	:	0,			$
;				I1A	:	intarr(ndtrn),		$
;				I2A	:	intarr(ndtrn),		$
;				STRGA	:	strarr(ndlev),		$
;				NDLEV	:	0,			$
;				NDTRN	:	0,			$
;				NDTIN	:	0,			$
;				NDTEM	:	0,			$
;				IZMAX	:	0,			$
;				IZDIMD	:	0,			$
;				ZSTRGA	:	strarr(izdimd),		$
;				NVA	:	intarr(ndtrn),		$
;				SCEFA	:	fltarr(ndtem,ndtrn),	$
;				NV	:	0,			$
;				SCEF	:	intarr(ndtem),		$
;				TSCEF	:	intarr(ndtem,3),	$
;				ISTRN	:	0,			$
;				IEC1A	:	intarr(ndtrn),		$
;				IAC1A	:	intarr(ndtrn),		$
;				IAC2A	:	intarr(ndtrn),		$
;				FAC2A	:	fltarr(ndtrn),		$
;				IGC1A	:	intarr(ndtrn),		$
;				FGC2A	:	fltarr(ndtrn),		$
;				LIBPT	:	0,			$
;				IFOUT	:	1,			$
;				MAXT	:	0,			$
;				TINE	:	fltarr(ndtin) 		}
;
;		  Where the elements represent the following:
;
;               NEW     - Flag which defines whether or not default values
;                         exist or not. (< 0 if no values exist)
;		TITLE   - String; user-entered title for the run.
;		ITOGGLE1- Int; Flag used to makr which base is visible on
;			  the processing screen: 0="Settings and scalings"
;						 1="Temperature table"
;		IZS	- Int; Nuclear charge of neutral sequence member
;		IZ0	- Int; Index of selected ion (gives nuclear charge).
;		IZ0VAL	- Intarray; Nuclear charges of all ions.
;		ITRAN	- Int; Number of electron impact transitions
;		IL	- Int; Number of index energy levels
;		I1A	- Intarray; Indices of lower levels for transitions
;		I2A	- Intarray; Indices of upper levels for transitions
;		STRGA	- Strarray; Energy level designations
;		NDLEV	- Int; max. number of levels allowed
;		NDTRN	- Int; Max. no. of transitions
;		NDTIN	- Int; Max. no. of temperatures allowed
;		NDTEM	- Int; Input file: Max. no. of temperatures
;		IZMAX	- Int; Actual no. of members in input file
;		IZDIMD	- Int; Max. allowed number of members in file
;		ZSTRGA	- Strarr; List of members in general-z file
;		NVA	- Intarray; Input file: No. of Gamma/temp values
;			  for a given transition.
;		SCEFA	- Fltarray; Input file: Z-scaled electron temps. (K)
;				1st dimension - temperature
;				2nd dimension - transition index
;		NV	- Int; Number of temps for selected transition
;		SCEF	- Intarray; Selected transition: Z-scaled temps. (K)
;		TSCEF	- Intarray; Selected transition: Electron temps
;				1st dimension - temperature
;				2nd dimension - IFOUT
;		ISTRN	- Int; Selected electron impact transition index
;		IEC1A	- Intarray; Flags transition energy settings:
;				1 => Z1 ; 2 => 1/Z1
;		IAC1A	- Intarray; Flags Einstein coeff. settings:
;				1 => Z1 ; 2 => 1/Z1
;		IAC2A	- Intarray; Flags transition type:
;				1 => DIPOLE ; 2 => NON-DIPOLE ;
;				3 => SPIN CHANGE ; 4 => OTHER
;		FAC2A	- Fltarray; Z1 power for scaling factor
;		IGC1A	- Intarray; Flags upsilon coeff. settings
;				1 => Z1 ; 2 => 1/Z1
;		FGC2A	- Fltarray; Z1 power for scaling factor
;		LIBPT	- Int; Flag for use of bad-point option
;				0 = Yes ; 1 = No
;		IFOUT	- Int; Flag for temperature units
;				1 = Input temperatures in Kelvin
;				2 = Input temperatures in eV
;				3 = Input temperatures in Reduced form
;		MAXT	- Int; No. of input temperatures (1-20)
;		TINE	- Fltarray; Electron temperatures (units see IFOUT)
;
;	DSFULL	- String; The full system file name of the input 
;		  dataset selected by the user for processing.
;
;       BITFILE - String; the path to the directory containing bitmaps
;                 for the 'escape to series menu' button.
;
;	FIRSTRUN- Int; 0=This is the first run through the code for a 
;		  given file, 1=Not the first run.
;
; OPTIONAL INPUTS:
;	None
;
; OUTPUTS:
;	PROCVAL	- On output the structure records the final settings of
;		  the processing selections widget if the user pressed the
;		  'Done' button, otherwise it is not changed from input.
;
;	INFOBASE- Long int; the widget id. of the information widget which
;		  is created when the 'Done' button is pressed.
;	
;	FIRSTRUN- Set to 1 after the first run through to prevent user
;		  settings being overwritten by the default ones
;
;	IOERROR	- 0 = no problems during pipe comms.
;		  1 = there was a problem during pipe comms.
;
; OPTIONAL OUTPUTS:
;	None
;
; KEYWORD PARAMETERS:
;	FONT_LARGE - The name of a larger font.
;
;	FONT_SMALL - The name of a smaller font.
;
;	EDIT_FONTS - A structure of two fonts used in the table editor
;		     adas_edtab.pro, {font_norm:'',font_input:''}.
;
; CALLS:
;	ADAS203_PROC	Invoke the IDL interface for ADAS203 data
;		 	processing options/input.
;	I4Z0IE		Converts atomic number into element symbol
;
; SIDE EFFECTS:
;	Two way communications with ADAS203 FORTRAN via the
;	bi-directional UNIX pipe. Communications are with both B3SETP.FOR
;	and B3ISPF.FOR.
;
; CATEGORY:
;	Adas system.
;
; WRITTEN:
;       Tim Hammond, Tessella Support Services plc, 29-02-96
;
; MODIFIED:
;	1.1     Tim Hammond
;		First version
;	1.2	Tim Hammond
;		Modified value of iz0 passed through to FORTRAN
;	1.3	Tim Hammond
;		Added extra error checking for FORTRAN
;
; VERSION:
;	1.1	21-03-96
;	1.2	29-03-96
;	1.3	29-03-96
;
;-
;-----------------------------------------------------------------------------

PRO b3ispf, pipe, lpend, inval, procval, dsfull, gomenu, bitfile, 	$
	    infobase, firstrun, FONT_LARGE=font_large, 			$
	    ioerror, FONT_SMALL=font_small, EDIT_FONTS=edit_fonts


    ON_IOERROR, LABELERROR

                ;**** Set defaults for keywords ****

    IF NOT (KEYWORD_SET(font_large)) THEN font_large = ''
    IF NOT (KEYWORD_SET(font_small)) THEN font_small = ''
    IF NOT (KEYWORD_SET(edit_fonts)) THEN 				$
        edit_fonts = {font_norm:'',font_input:''}


    lpend = 0				;Cancel/done flag
    idum = 0				;dummy integer variable
    fdum = 0.0				;dummy float variable
    sdum = ''				;dummy string variable

		;***********************************************
		;**** If re-entering processing screen from ****
		;**** output screen take fast route through ****
		;**** the code                              ****
		;***********************************************

    if firstrun eq 0 then begin

		;**************************************
		;**** Create an information widget ****
		;**************************************

        widget_control, /hourglass
        widgetbase = widget_base(/column, xoffset=300, yoffset=200,	$
        title = "ADAS203: INFORMATION")
        lab0 = widget_label(widgetbase, value='')
        lab1 = widget_label(widgetbase, value='')
        lab2 = widget_label(widgetbase, font=font_large,		$
               value="    Accessing general-z file - please wait    ")
        lab3 = widget_label(widgetbase, value='')
        lab4 = widget_label(widgetbase, value='')
        widget_control, widgetbase, /realize
    
		;*****************************************
		;**** Read in parameters from FORTRAN ****
		;*****************************************

		;**** Read from b3setp ****

        readf, pipe, idum
        izmax = idum
        readf, pipe, idum
        izdimd = idum
        readf, pipe, idum
        ndlev = idum
        zstrga = strarr(izdimd)
        iz0val = intarr(izdimd)
        for i=0, izmax-1 do begin
	    zstrga(i) = '  '+ strcompress(string(i+1), /remove_all) + 	$
                        '       '
   	    readf, pipe, idum
            iz0val(i) = idum
	    if idum lt 10 then begin
	        zstrga(i) = zstrga(i) +       				$
                strcompress(string(idum), /remove_all) + '            '
	    endif else begin
	        zstrga(i) = zstrga(i) + 				$
	        strcompress(string(idum), /remove_all) + '           '
	    endelse
	    i4z0ie, idum, element
   	    readf, pipe, idum
	    if idum lt 10 then begin
	        zstrga(i) = zstrga(i) +       				$
	        strcompress(string(idum), /remove_all) + '            '
	    endif else begin
                zstrga(i) = zstrga(i) +                                     $
                strcompress(string(idum), /remove_all) + '           '
	    endelse
	    zstrga(i) = zstrga(i) + element
        endfor
        readf, pipe, idum
        il = idum
        strga = strarr(ndlev)
        for i=0, il-1 do begin
	    readf, pipe, sdum
	    strga(i) = sdum
        endfor
        readf, pipe, idum
        izs = idum
        readf, pipe, idum
        itran = idum

		;**** Read from b3ispf ****

        readf, pipe, idum
        ndtrn = idum
        i1a = intarr(ndtrn)
        i2a = intarr(ndtrn)
        if firstrun eq 1 then itran=procval.itran
        for i=0, itran-1 do begin
	    readf, pipe, idum
	    i1a(i) = idum - 1
	    readf, pipe, idum
	    i2a(i) = idum - 1
        endfor
        readf, pipe, idum
        ndtem = idum
        readf, pipe, idum
        ndtin = idum
        nva = intarr(ndtrn)
        scefa = fltarr(ndtem, ndtrn)
        for i=0, itran-1 do begin
	    readf, pipe, idum
	    nva(i) = idum
        endfor
        for i=0, itran-1 do begin
	    for j=0, (nva(i)-1) do begin
	        readf, pipe, fdum
	        scefa(j,i) = fdum
	    endfor
        endfor
        iec1a = intarr(ndtrn)
        iac1a = intarr(ndtrn)
        iac2a = intarr(ndtrn)
        fac2a = fltarr(ndtrn)
        igc1a = intarr(ndtrn)
        fgc2a = fltarr(ndtrn)
        for i = 0, ndtrn-1 do begin
	    readf, pipe, idum
	    if idum eq 0 then idum=1
	    iec1a(i) = idum - 1
	    readf, pipe, idum
	    if idum eq 0 then idum=1
	    iac1a(i) = idum - 1
	    readf, pipe, idum
	    if idum eq 0 then idum=1
	    iac2a(i) = idum - 1
	    readf, pipe, fdum
	    fac2a(i) = fdum
	    readf, pipe, idum
	    if idum eq 0 then idum=1
	    igc1a(i) = idum - 1
	    readf, pipe, fdum
	    fgc2a(i) = fdum
        endfor

		;*******************************************
		;**** Set default value if non provided ****
		;*******************************************

        if (procval.new lt 0) then begin
            procval = {	NEW   	: 	0 ,             		$
		  	TITLE 	: 	'', 		  		$
			ITOGGLE1:	0,				$
			IZS	:	izs,				$
			IZ0	:	0,				$
			IZ0VAL	:	iz0val,				$
			ITRAN	:	itran, 				$
			IL	:	il,				$
			I1A	:	i1a,				$
			I2A	:	i2a,				$
			STRGA	:	strga,				$
			NDLEV	:	ndlev,				$
			NDTRN	:	ndtrn,				$
			NDTIN	:	ndtin,				$
			NDTEM	:	ndtem,				$
			IZMAX	:	izmax,				$
			IZDIMD	:	izdimd,				$
			ZSTRGA	:	zstrga,				$
			NVA	:	nva,				$
			SCEFA	:	scefa,				$
			NV	:	0,				$
			SCEF	:	fltarr(ndtem),			$
			TSCEF	:	fltarr(ndtem,3),		$
			ISTRN	:	0,				$
			IEC1A	:	iec1a,				$
			IAC1A	:	iac1a,				$
			IAC2A	:	iac2a,				$
			FAC2A	:	fac2a,				$
			IGC1A	:	igc1a,				$
			FGC2A	:	fgc2a,				$
			LBPTS	:	intarr(ndtrn),			$
			LIBPT	:	0,				$
			IFOUT	:	1,				$
			MAXT	:	0,				$
			TINE	:	fltarr(ndtin)			$
	            }
        endif

		;**************************************************
		;**** Set those values that must always be set ****
		;**************************************************

        procval.izs = izs
        procval.itran = itran
        procval.il = il
        procval.izmax = izmax
        procval.zstrga = zstrga
        procval.strga = strga
        procval.iz0val = iz0val
        procval.i1a = i1a
        procval.i2a = i2a
        procval.scefa = scefa
        procval.nva = nva
        procval.iec1a = iec1a
        procval.iac1a = iac1a
        procval.iac2a = iac2a
        procval.fac2a = fac2a
        procval.igc1a = igc1a
        procval.fgc2a = fgc2a
    endif

		;****************************************
		;**** Pop-up processing input widget ****
		;****************************************

    adas203_proc, inval, procval, bitfile, dsfull, action, widgetbase, 	$
 		  firstrun, FONT_LARGE=font_large, 			$
		  FONT_SMALL=font_small, EDIT_FONTS=edit_fonts

                ;*********************************************
                ;**** Act on the output from the widget   ****
                ;**** There are three    possible actions ****
                ;**** 'Done', 'Cancel' and 'Menu'.        ****
                ;*********************************************

    if action eq 'Done' then begin
        lpend = 0

		;**************************************
		;**** Create an information widget ****
		;**************************************

        widget_control, /hourglass
        widgetbase = widget_base(/column, xoffset=300, yoffset=200,	$
        title = "ADAS203: INFORMATION")
	infobase = widgetbase
        lab0 = widget_label(widgetbase, value='')
        lab1 = widget_label(widgetbase, value='')
        lab2 = widget_label(widgetbase, font=font_large,		$
	value="       Processing information - please wait       ")
        lab3 = widget_label(widgetbase, value='')
        lab4 = widget_label(widgetbase, value='')
        widget_control, widgetbase, /realize
    endif else if action eq 'Menu' then begin
        lpend = 1
        gomenu = 1
    endif else begin
        lpend = 1
    endelse

		;*******************************
		;**** Write data to fortran ****
		;*******************************

    printf, pipe, lpend
    if lpend eq 0 then begin
	printf, pipe, procval.nv
	printf, pipe, (procval.istrn + 1)
	printf, pipe, procval.iz0
        printf, pipe, procval.title, format='(a40)'
 	printf, pipe, procval.maxt
	printf, pipe, procval.ifout
	for i=0, (procval.ndtin-1) do begin
	    printf, pipe, procval.tine(i)
	endfor
	printf, pipe, procval.libpt
	for i=0, (procval.ndtrn-1) do begin
	    printf, pipe, (procval.iec1a(i) + 1)
	    printf, pipe, (procval.iac1a(i) + 1)
	    printf, pipe, (procval.iac2a(i) + 1)
	    printf, pipe, procval.fac2a(i)
	    printf, pipe, (procval.igc1a(i) + 1)
	    printf, pipe, procval.fgc2a(i)
	endfor
    endif
    ioerror = 0
    goto, LABELEND

LABELERROR:
    print, '*********************** B3ISPF ERROR *************************'
    print, '*****                                                    *****'
    print, '*****    A FATAL ERROR OCCURRED WHILST COMMUNICATING     *****'
    print, '*****           WITH THE FORTRAN CHILD PROCESS           *****'
    print, '*****                                                    *****'
    print, '******************** ADAS203 TERMINATING *********************'
    ioerror = 1

LABELEND:

END
