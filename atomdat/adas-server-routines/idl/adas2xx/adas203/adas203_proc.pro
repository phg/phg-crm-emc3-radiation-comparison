; Copyright (c) 1996, Strathclyde University .
; SCCS Info : Module @(#)$Header: /home/adascvs/idl/adas2xx/adas203/adas203_proc.pro,v 1.5 2004/07/06 10:12:37 whitefor Exp $ Date $Date: 2004/07/06 10:12:37 $
;+
; PROJECT:
;       ADAS IBM MVS to UNIX conversion
;
; NAME:
;       ADAS203_PROC
;
; PURPOSE:
;       IDL ADAS user interface, processing options/input.
;
; EXPLANATION:
;       This routine creates and manages a pop-up window which allows
;       the user to select options and input data to control ADAS203
;       processing.
;
; USE:
;       This routine is ADAS203 specific, see b3ispf.pro for how it
;       is used.
;
; INPUTS:
;       INVAL   - A structure containing the final settings of the input
;                 options screen.
;
;       PROCVAL - A structure which determines the initial settings of
;                 the processing options widget.  The value is passed
;                 unmodified into cw_adas203_proc.pro.
;
;                 See b3ispf.pro for a full description of this structure.
;
;       BITFILE - String; the path to the directory containing bitmaps
;                 for the 'escape to series menu' button.
;
;	DSFULL	- String; the full UNIX name of the general-z file used
;
;	INFOBASE- Long int; the widget i.d. of the 'Please wait' widget
;		  which appears until the processing screen is ready
;		  for display.
;
;	FIRSTRUN- Int; marker showing whether this is the first run
;		  through the code (1) or not (0).
;
; OPTIONAL INPUTS:
;       None
;
; OUTPUTS:
;       PROCVAL - On output the structure records the final settings of
;                 the processing selections widget if the user pressed the
;                 'Done' button, otherwise it is not changed from input.
;
;       ACTION  - String; 'Done', 'Menu' or 'Cancel' for the button the
;                 user pressed to terminate the processing options
;                 window.
;
; OPTIONAL OUTPUTS:
;       None
;
; KEYWORD PARAMETERS:
;       FONT_LARGE - The name of a larger font.
;
;       FONT_SMALL - The name of a smaller font.
;
;       EDIT_FONTS - A structure of two fonts used in the table editor
;                    adas_edtab.pro, {font_norm:'',font_input:''}.
;
; CALLS:
;       CW_ADAS203_PROC Declares the processing options widget.
;       ADAS203_PROC_EV Called indirectly during widget management,
;                       routine included in this file.
;	DYNLABEL	Recursively adds dynamic_resize keyword to label
;			widgets for version 4.0.1 and higher of IDL
;       XMANAGER
;
; SIDE EFFECTS:
;       This widget uses a common block PROC203_BLK, to pass the
;       variables VALUE and ACTION between the two routines.
;
; CATEGORY:
;       Widgets
;
; WRITTEN:
;       Tim Hammond, Tessella Support Services plc, 1st March 1996
;
; MODIFIED:
;       1.1     Tim Hammond
;               First version
;	1.2	Tim Hammond
;		Added infobase input parameter and destroyed associated
;		information widget at correct time.
;	1.3	Tim Hammond
;		Added firstrun input variable.
;	1.4	Tim Hammond
;		Ensured information widget is always destroyed.
;	1.5	William Osborn
;		Added dynlabel procedure
;
; VERSION:
;       1.1     05-03-96
;	1.2	06-03-96
;	1.3	08-03-96
;	1.4	08-03-96
;	1.5	04-06-96
;
;-
;-----------------------------------------------------------------------------

PRO adas203_proc_ev, event

    COMMON proc203_blk, action, value

    action = event.action

    CASE event.action OF

                ;**** 'Done' button ****
                                            
        'Done'  : begin

                ;**** Get the output widget value ****

            widget_control, event.id, get_value=value
	    widget_control, event.top, /destroy
	
	end

                ;**** 'Cancel' button ****


        'Cancel': widget_control, event.top, /destroy

                ;**** 'Menu' button ****

        'Menu': widget_control, event.top, /destroy

 	ELSE:		;Do nothing

    ENDCASE

END

;-----------------------------------------------------------------------------

PRO adas203_proc, inval, procval, bitfile, dsfull, action, infobase,	$
                  firstrun, FONT_LARGE=font_large, 			$
		  FONT_SMALL=font_small, EDIT_FONTS=edit_fonts

                ;**** Declare common variables ****

    COMMON proc203_blk, actioncom, value

                ;**** Copy "procval" to common ****

    value = procval

                ;**** Set defaults for keywords ****

    IF NOT (KEYWORD_SET(font_large)) THEN font_large = ''
    IF NOT (KEYWORD_SET(font_small)) THEN font_small = ''
    IF NOT (KEYWORD_SET(edit_fonts)) THEN                               $
    edit_fonts = { font_norm:'', font_input:''}

                ;**** Create base widget ****

    procid = widget_base(TITLE='ADAS203 PROCESSING OPTIONS',            $
                         XOFFSET=1, YOFFSET=1)

                ;**** Declare processing widget ****

    cwid = cw_adas203_proc(procid, bitfile, inval, dsfull, 		$
                           PROCVAL=value,				$
                           FONT_LARGE=font_large, FONT_SMALL=font_small,$
                           EDIT_FONTS=edit_fonts)

		;**** Destroy the 'Please wait' widget ****

    if firstrun eq 0 then begin
        firstrun = 1
        widget_control, infobase, /destroy
    endif

                ;**** Realize the new widget ****

    dynlabel, procid
    widget_control, procid, /realize

                ;**** Make widget modal ****

    xmanager, 'adas203_proc', procid, event_handler='adas203_proc_ev',  $
              /modal, /just_reg

                ;*** Copy value back to procval for return to b3ispf ***

    action = actioncom
    procval = value

END
