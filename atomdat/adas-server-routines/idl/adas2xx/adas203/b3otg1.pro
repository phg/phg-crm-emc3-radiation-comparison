; Copyright (c) 1996, Strathclyde University.
; SCCS info: Module @(#)$Header: /home/adascvs/idl/adas2xx/adas203/b3otg1.pro,v 1.1 2004/07/06 11:20:05 whitefor Exp $ Date $Date: 2004/07/06 11:20:05 $
;+
; PROJECT:
;       ADAS IBM MVS to UNIX conversion
;
; NAME:
;       B3OTG1
;
; PURPOSE:
;       Communication with ADAS203 FORTRAN via UNIX pipe and
;       graphics output.
;
; EXPLANATION:
;       The routine begins by reading information from the ADAS203
;	FORTRAN routine B3OTG1.FOR via a UNIX pipe.  Then the IDL graphical
;       output routine for ADAS203 transition wave no. plots is invoked.
;
; USE:
;       This routine is specific to adas203.
;
; INPUTS:
;       DSFULL   - Script file name
;
;       PIPE     - The IDL unit number of the bi-directional pipe to the
;                  ADAS401 FORTRAN process.
;
;       UTITLE   - Optional comment by user
;
;       LDEF1    - Integer; 0 if default scaling required 1 if user
;                  specified scaling to be used.
;
;       XMIN     - String; User specified x-axis minimum, number as string.
;
;       XMAX     - String; User specified x-axis maximum, number as string.
;
;       YMIN     - String; User specified y-axis minimum, number as string.
;
;       YMAX     - String; User specified y-axis maximum, number as string.
;
;       HRDOUT   - Integer; 0 if no hardcopy required, 1 if hardcopy.
;
;       HARDNAME - String; file name for hardcopy output.
;
;       DEVICE   - String; IDL name for hardcopy output device.
;
;       HEADER   - ADAS version header information for inclusion in the
;                  graphical output.
;
;       BITFILE - String; the path to the directory containing bitmaps
;                 for the 'escape to series menu' button.
;
; OPTIONAL INPUTS:
;       None.
;
; OUTPUTS:
;       GOMENU  - Int; flag - set to 1 if user has selected 'escape direct
;                 to series menu', otherwise 0
;
; OPTIONAL OUTPUTS:
;       None.
;
; KEYWORD PARAMETERS:
;       FONT    - String; Name of the font to be used for the graphical
;                 output widget.
;
; CALLS:
;       ADAS203_1_PLOT	ADAS203 Transition wave no. graphical output.
;
; SIDE EFFECTS:
;       This routine reads information from FORTRAN via a UNIX pipe.
;
; CATEGORY:
;       Adas system.
;
; WRITTEN:
;       Tim Hammond, Tessella Support Services plc, 21st March 1996
;
; MODIFIED:
;       1.1     Tim Hammond
;               First version
;
; VERSION:
;       1.1     21-03-96
;
;-
;-----------------------------------------------------------------------------

PRO b3otg1, dsfull, pipe, utitle, ldef1, xmin, xmax, ymin, ymax,	$
            hrdout, hardname, device, header, bitfile,			$
            gomenu, FONT=font

                ;**** Set defaults for keywords ****

    IF NOT (KEYWORD_SET(font)) THEN font = ''

                ;**** Declare variables for input ****

    input = 0
    fdum = 0.0
    sdum = ' '
                                                        
                ;********************************
                ;**** Read data from fortran ****
                ;********************************

    readf, pipe, input
    izmax = input	
    xvals = fltarr(izmax)
    yvals = fltarr(izmax)
    nuc = intarr(izmax+1)
    recd = intarr(izmax+1)
    elem = strarr(izmax+1)
    for i=0, (izmax-1) do begin
	readf, pipe, fdum
 	xvals(i) = fdum
	readf, pipe, fdum
	yvals(i) = fdum
    endfor
    xo = fltarr(1)
    z = fltarr(1)
    readf, pipe, fdum
    xo(0) = fdum
    readf, pipe, fdum
    z(0) = fdum
    readf, pipe, sdum
    species = sdum
    readf, pipe, sdum
    seq = sdum
    readf, pipe, sdum
    seq = seq + '  ' + sdum
    for i=0, (izmax-1) do begin
        readf, pipe, idum
	nuc(i) = idum
        readf, pipe, idum
 	recd(i) = idum
        readf, pipe, sdum
	elem(i) = strupcase(sdum)
    endfor
    readf, pipe, idum
    nuc(izmax) = idum
    readf, pipe, idum
    recd(izmax) = idum
    readf, pipe, sdum
    elem(izmax) = strupcase(sdum)

                 ;***********************
                 ;**** Plot the data ****
                 ;***********************
 
     adas203_1_plot, dsfull, utitle, ldef1, xvals, yvals, xo, z,  	$
                     species, seq, hrdout, hardname, device, header,	$
 		     izmax, xmin, xmax, ymin, ymax, nuc, recd, elem,	$
                     bitfile, gomenu, FONT=font
                                                    
END
