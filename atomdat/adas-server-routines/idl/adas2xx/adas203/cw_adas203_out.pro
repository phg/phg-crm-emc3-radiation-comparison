; Copyright (c) 1996, Strathclyde University.
; SCCS info: Module @(#)$Header: /home/adascvs/idl/adas2xx/adas203/cw_adas203_out.pro,v 1.6 2004/07/06 12:29:39 whitefor Exp $ Date $Date: 2004/07/06 12:29:39 $
;+
; PROJECT:
;       ADAS IBM MVS to UNIX conversion
;
; NAME:
;       CW_ADAS203_OUT()
;
; PURPOSE:
;       Produces a widget for ADAS203 output options.
;
; EXPLANATION:
;       This function declares a compound widget consisting of:
;        - a button for selecting graphical or text output options
;	 - a selection button for choosing graphical output
;        - an exclusive selection widget to choose output graph
;        - for each graph a 'select axis limits' button
;        - for each graph a set of typeins for max and min axis values
;        - the associated hard copy widget comprising a filename and
;          list of output devices
;        - output file widgets cw_adas_outfile for text and passing
;          file output
;        - a button for browsing comments in the script file
;        - a text typein for the user to assign a title to the graph
;        - buttons for 'Escape to series menu', 'Return to processing
;	   screen', 'View graphical output', 'Produce data output and
;	   return to output screen', and 'Produce data output and exit
;	   to series menu'.
;
;	This widget only generates events for the last five buttons.
;
; USE:
;       This routine is specific to adas203.
;
; INPUTS:
;       PARENT  - Long integer; ID of parent widget.
;
;	INVAL	- A structure containing the settings of the input options
;		  compound widget.
;
;	PROCVAL	- A structure containing the settings of the processing
;		  options compound widget.
;
;       BITFILE - String; the path to the directory containing bitmaps
;                 for the 'escape to series menu' button.
;
; OPTIONAL INPUTS:
;       None.
;
; OUTPUTS:
;       The return value of this function is the ID of the declared widget.
;
; OPTIONAL OUTPUTS:
;       None.
;
; KEYWORD PARAMETERS:
;       DEVLIST - A string array;  Each element of the array is the
;                 name of a hardcopy graphics output device.  This
;                 list must mirror the DEVCODE list.  DEVLIST is the
;                 list which the user will see.
;                 e.g ['Post-Script','HP-PCL','HP-GL']
;
;       VALUE   - A structure which determines the initial settings of
;                 the output options widget.  The structure has the
;                 following form:
;		{	ITOGGLE		:	0,			$
;			ILABEL		:	1,			$
;			LGRAPHNO	:	0,			$
;                       XMIN1           :       '',                     $
;                       XMAX1           :       '',                     $
;                       YMIN1           :       '',                     $
;                       YMAX1           :       '',                     $
;                       XMIN2           :       '',                     $
;                       XMAX2           :       '',                     $
;                       YMIN2           :       '',                     $
;                       YMAX2           :       '',                     $
;                       XMIN3           :       '',                     $
;                       XMAX3           :       '',                     $
;                       YMIN3           :       '',                     $
;                       YMAX3           :       '',                     $
;                       LDEF1           :       0,                      $
;                       LDEF2           :       0,                      $
;                       LDEF3           :       0,     
;	      		GRPOUT          :       0, 			$
;                       GTIT1           :       '',                     $
;                       HRDOUT          :       0,                      $
;                       HARDNAME        :       '',                     $
;                       GRPDEF          :       '',                     $
;                       GRPFMESS        :       '',                     $
;                       DEVSEL          :       -1,                     $
;                       TEXOUT          :       0,                      $
;                       TEXAPP          :       -1,                     $
;                       TEXREP          :       0,                      $
;                       TEXDSN          :       '',                     $
;                       TEXDEF          :       'paper.txt',            $
;                       TEXMES          :       '',                     $
;                       COPOUT          :       0,                      $
;                       COPAPP          :       -1,                     $
;                       COPREP          :       0,                      $
;                       COPDSN          :       '',                     $
;                       COPDEF          :       copdefval,              $
;                       COPMES          :       ''                      }
;
;  Where the elements represent:
;
;			ITOGGLE	- Int; Flag showing whether graphics options
;                                 should be displayed (1) or text (0)
;  For Graphics output:
;			LGRAPHNO- Int; current graph chosen
;			ILABEL	- Int; 0=Do not label lines on graph,
;				       1=Do label lines on graph
;				       [Applies only to rate param. graph]
;			XMINn	- Float; min of x-axis for graph n
;			XMAXn	- Float; max of x-axis for graph n
;			YMINn	- Float; min of y-axis for graph n
;			YMAXn	- Float; max of y-axis for graph n
;			LDEFn	- Int; flag whether user has selected
;                                 explicit axes for graph n
;			GRPOUT	- Int; Flag whether graphical output chosen
;			GTIT1	- String; Graph title
;			HRDOUT	- Int; Hard copy active: 1 on, 0 off
;			HARDNAME- String; Hard copy output file name
;			GRPDEF	- String; Default output file name
;			GRPFMESS- String; File name error message
;			DEVSEL	- Int; index of selected device in DEVLIST
;
;  For Text output:
;			TEXOUT	- Int; Activation button 1 on, 0 off
;			TEXAPP	- Int; Append button 1 on, 0 off,
;                                 -1 no button
;			TEXREP  - Int; Replace button 1 on, 0 off,
;                                 -1 no button
;			TEXDSN	- String; Output file name
;			TEXDEF	- String; Default file name
;			TEXMES	- String; file name error message
;  For COPASE output:
;			COPOUT  - Int; Activation button 1 on, 0 off
;			COPAPP  - Int; Append button 1 on, 0 off,
;                                 -1 no button
;			COPREP  - Int; Replace button 1 on, 0 off,
;                                 -1 no button
;			COPDSN  - String; Output file name
;			COPDEF  - String; Default file name
;			COPMES  - String; file name error message
;
;  			End of VALUE elements description.
;
;       UVALUE  - A user value for this widget.
;
;       FONT_LARGE- Supplies the large font to be used.
;
;       FONT_SMALL- Supplies the small font to be used.
;
; CALLS:
;       CW_ADAS_DSBR    Input dataset name and comments browse button.
;       CW_ADAS_OUTFILE Output file name entry widget.
;       See side effects for other related widget management routines.
;
; SIDE EFFECTS:
;
;       Two other routines are included in this file
;       OUT203_GET_VAL()
;       OUT203_EVENT()
;
; CATEGORY:
;       Compound Widget
;
; WRITTEN:
;       Tim Hammond, Tessella Support Services plc, 7th March 1996
;
; MODIFIED:
;       1.1     Tim Hammond
;               First version - all widgets in place
;	1.2	Tim Hammond
;		Added functionality to text output buttons and added
;		get_value routine.
;	1.3	Tim Hammond
;		Completed functionality.
;	1.4	Tim Hammond
;		Added common block out203_block to store initial
;		settings of text and passing file widgets. These are
;		then used to switch the text output options off when
;		graphical output is requested. This is quite a 
;		cumbersome solution (to avoid warning messages about
;		existing text files when no text output will be produced),
;		but I have not been able to devise a better way of
;		doing it. The only drawback of this method is if the user
;		modifies the text output settings and then decides to 
;		look at the graph, the changes will be lost.
;	1.5	Tim Hammond
;		Ensured that sensitivity of TOGBASE is always set correctly.
;	1.6	William Osborn
;		Removed xsize=n keyword from widget_label command.
;		The dynlabel procedure does not override these
;		assignments and they were taken as being values in pixels
;		on some machines.
;
; VERSION:
;       1.1     08-03-96
;	1.2	21-03-96
;	1.3	01-04-96
;	1.4	01-04-96
;	1.5	02-04-96
;	1.6	01-08-96
;
;-
;-----------------------------------------------------------------------------

FUNCTION out203_get_val, id

                ;**** Return to caller on error ****

    ON_ERROR, 2

                ;**** Retrieve the state ****

    parent=widget_info(id, /parent)
    widget_control, parent, get_uvalue=state, /no_copy

		;**** Get toggle setting ****

    widget_control, state.flipid, get_value=itoggle

		;**** Get graphical output settings ****

    widget_control, state.titleid, get_value=gtit1
    widget_control, state.grshown, get_value=lgraphno
    widget_control, state.tyminbut, get_value=ymin1
    widget_control, state.tymaxbut, get_value=ymax1
    widget_control, state.txminbut, get_value=xmin1
    widget_control, state.txmaxbut, get_value=xmax1
    widget_control, state.ayminbut, get_value=ymin2
    widget_control, state.aymaxbut, get_value=ymax2
    widget_control, state.axminbut, get_value=xmin2
    widget_control, state.axmaxbut, get_value=xmax2
    widget_control, state.ryminbut, get_value=ymin3
    widget_control, state.rymaxbut, get_value=ymax3
    widget_control, state.rxminbut, get_value=xmin3
    widget_control, state.rxmaxbut, get_value=xmax3
    widget_control, state.labid, get_value=ilabel

		;**** Get hard copy settings ****

    widget_control, state.hardid, get_value=hardval
    widget_control, state.devid, get_value=devsel

                ;**** Get text output settings ****

    widget_control, state.textid, get_value=papos

                ;**** Get COPASE passing file settings ****

    widget_control, state.passid, get_value=pasos

    os = { out203_set,                                           	$
		ITOGGLE		:	itoggle,			$
		ILABEL		:	ilabel,				$
		LGRAPHNO	:	lgraphno,			$
		XMIN1		:	xmin1,				$
		XMAX1		:	xmax1,				$
		YMIN1		:	ymin1,				$
		YMAX1		:	ymax1,				$
		XMIN2		:	xmin2,				$
		XMAX2		:	xmax2,				$
		YMIN2		:	ymin2,				$
		YMAX2		:	ymax2,				$
		XMIN3		:	xmin3,				$
		XMAX3		:	xmax3,				$
		YMIN3		:	ymin3,				$
		YMAX3		:	ymax3,				$
		LDEF1		:	state.ldef1,			$
		LDEF2		:	state.ldef2,			$
		LDEF3		:	state.ldef3,			$
		GRPOUT		:	state.grpout,			$
		GTIT1		:	gtit1,				$
		HRDOUT		:	hardval.outbut,			$
		HARDNAME	:	hardval.filename,		$
		GRPDEF		:	hardval.defname,		$
		GRPFMESS	:	hardval.message,		$
		DEVSEL		:	devsel,				$
		TEXOUT		:	papos.outbut, 			$
		TEXAPP		:	papos.appbut,  			$
		TEXREP		:	papos.repbut,			$
		TEXDSN		:	papos.filename,			$
		TEXDEF		:	papos.defname,			$
		TEXMES		:	papos.message,			$
		COPOUT		:	pasos.outbut, 			$
		COPAPP		:	pasos.appbut,  			$
		COPREP		:	pasos.repbut,			$
		COPDSN		:	pasos.filename,			$
		COPDEF		:	pasos.defname,			$
		COPMES		:	pasos.message			$
									}

                ;**** Return the state ****

    widget_control, parent, set_uvalue=state, /no_copy
    
    RETURN, os
 
END

;-----------------------------------------------------------------------------

FUNCTION out203_event, event

    COMMON out203_block, textvalcom, passvalcom

                ;**** Base ID of compound widget ****

    parent = event.top

                ;**** Retrieve the state ****

    widget_control, parent, get_uvalue=state, /no_copy

                ;*********************************
                ;**** Clear previous messages ****
                ;*********************************

    widget_control, state.messid, set_value=' '

                ;************************
                ;**** Process events ****
                ;************************

    CASE event.id OF

                ;*****************************************************
                ;**** Change between graphics/text output display ****
                ;*****************************************************

        state.flipid: begin
            widget_control, state.flipid, get_value=itoggle
            widget_control, state.graphbase, map=(1-itoggle(0))
            widget_control, state.textbase, map=itoggle(0)
        end

		;*********************************************
		;**** Do we want graphical output? button ****
		;*********************************************

        state.grapflag: begin
            if state.grpout eq 1 then state.grpout=0 else state.grpout=1
            widget_control, state.plotbase, sensitive=state.grpout
            widget_control, state.graphbase2, sensitive=state.grpout
            widget_control, state.hardbase, sensitive=state.grpout
	    widget_control, state.viewid, sensitive=state.grpout
	    widget_control, state.grshown, get_value=shown
	    if shown eq 2 then begin
		widget_control, state.togbase, sensitive=state.grpout
	    endif else begin
		widget_control, state.togbase, sensitive=0
	    endelse
        end

		;********************************************
		;**** Explicit scaling for Wave no. plot ****
		;********************************************

	state.trandef: begin
	    if state.ldef1 eq 1 then state.ldef1=0 else state.ldef1=1
	    widget_control, state.tranbase3b, sensitive=state.ldef1
	end
		;*******************************************
		;**** Explicit scaling for A-Value plot ****
		;*******************************************

	state.avadef: begin
	    if state.ldef2 eq 1 then state.ldef2=0 else state.ldef2=1
	    widget_control, state.avabase3b, sensitive=state.ldef2
	end
		;**************************************************
		;**** Explicit scaling for Rate parameter plot ****
		;**************************************************

	state.rpdef: begin
	    if state.ldef3 eq 1 then state.ldef3=0 else state.ldef3=1
	    widget_control, state.rpbase3b, sensitive=state.ldef3
	end

		;*********************************************
		;**** Min/max buttons for Transition plot ****
		;*********************************************

	state.txminbut: widget_control, state.txmaxbut, /input_focus
	state.txmaxbut: widget_control, state.tyminbut, /input_focus
	state.tyminbut: widget_control, state.tymaxbut, /input_focus
	state.tymaxbut: widget_control, state.txminbut, /input_focus

		;******************************************
		;**** Min/max buttons for A-value plot ****
		;******************************************

	state.axminbut: widget_control, state.axmaxbut, /input_focus
	state.axmaxbut: widget_control, state.ayminbut, /input_focus
	state.ayminbut: widget_control, state.aymaxbut, /input_focus
	state.aymaxbut: widget_control, state.axminbut, /input_focus

		;*************************************************
		;**** Min/max buttons for Rate parameter plot ****
		;*************************************************

	state.rxminbut: widget_control, state.rxmaxbut, /input_focus
	state.rxmaxbut: widget_control, state.ryminbut, /input_focus
	state.ryminbut: widget_control, state.rymaxbut, /input_focus
	state.rymaxbut: widget_control, state.rxminbut, /input_focus

		;********************************
		;**** Choice of graph button ****
		;********************************

        state.grshown: begin
            widget_control, state.grshown, get_value=shown
            if (shown eq 0) then begin
                widget_control, state.tranbase, map=1
                widget_control, state.avabase, map=0
                widget_control, state.rpbase, map=0
		widget_control, state.togbase, sensitive=0
            endif else if (shown eq 1) then begin
                widget_control, state.tranbase, map=0
                widget_control, state.avabase, map=1
                widget_control, state.rpbase, map=0           
		widget_control, state.togbase, sensitive=0
	    endif else begin
                widget_control, state.tranbase, map=0
                widget_control, state.avabase, map=0
                widget_control, state.rpbase, map=1
		widget_control, state.togbase, sensitive=1
            endelse
	end

		;********************************************
		;**** Return to processing screen button ****
		;********************************************

	state.procid: begin
            new_event = {ID:parent, TOP:event.top, HANDLER:0L,          $
                         ACTION:'Cancel'}
	end
		;*********************
		;**** Menu button ****
		;*********************

        state.outid: begin
            new_event = {ID:parent, TOP:event.top, HANDLER:0L,          $
                         ACTION:'Menu'}
        end

		;*************************
		;**** Output and exit ****
		;*************************

	state.exitid: begin

		;***********************************************
		;**** First switch off any graphical output ****
		;***********************************************

	    state.grpout = 0
	    widget_control, state.grapflag, set_button=state.grpout
            widget_control, state.plotbase, sensitive=state.grpout
            widget_control, state.graphbase2, sensitive=state.grpout
            widget_control, state.hardbase, sensitive=state.grpout
            widget_control, state.togbase, sensitive=state.grpout
	    widget_control, state.viewid, sensitive=state.grpout
 	    widget_control, state.hardid, get_value=hardval
	    hardval.outbut = state.grpout
	    widget_control, state.hardid, set_value=hardval

		;**************************
		;**** Check for errors ****
		;**************************

	    widget_control, state.passid, get_value=passval
	    widget_control, state.textid, get_value=textval
	    mess1 = strcompress(passval.message, /remove_all)
	    mess2 = strcompress(textval.message, /remove_all)
	    if (passval.outbut eq 0) and (textval.outbut eq 0) then begin
		messval = '**** Error: You have not selected any '+	$
                          'files for output ****'
		widget_control, state.messid, set_value=messval
		widget_control, state.flipid, set_value=1
                widget_control, state.graphbase, map=0
                widget_control, state.textbase, map=1
	    endif else if (mess1 ne '') or (mess2 ne '') then begin
		messval = '**** Error in output file settings ****'
                widget_control, state.messid, set_value=messval
		widget_control, state.flipid, set_value=1
                widget_control, state.graphbase, map=0
                widget_control, state.textbase, map=1
	    endif else begin
                new_event = {ID:parent, TOP:event.top, HANDLER:0L,  	$
                             ACTION:'Exit'}
	    endelse
	end

		;***************************
		;**** Output and return ****
		;***************************

	state.dataid: begin

		;***********************************************
		;**** First switch off any graphical output ****
		;***********************************************

	    state.grpout = 0
	    widget_control, state.grapflag, set_button=state.grpout
            widget_control, state.plotbase, sensitive=state.grpout
            widget_control, state.graphbase2, sensitive=state.grpout
            widget_control, state.hardbase, sensitive=state.grpout
            widget_control, state.togbase, sensitive=state.grpout
	    widget_control, state.viewid, sensitive=state.grpout
 	    widget_control, state.hardid, get_value=hardval
	    hardval.outbut = state.grpout
	    widget_control, state.hardid, set_value=hardval

		;**************************
		;**** Check for errors ****
		;**************************

	    widget_control, state.passid, get_value=passval
	    widget_control, state.textid, get_value=textval
	    mess1 = strcompress(passval.message, /remove_all)
	    mess2 = strcompress(textval.message, /remove_all)
	    if (passval.outbut eq 0) and (textval.outbut eq 0) then begin
		messval = '**** Error: You have not selected any '+	$
                          'files for output ****'
		widget_control, state.messid, set_value=messval
		widget_control, state.flipid, set_value=1
                widget_control, state.graphbase, map=0
                widget_control, state.textbase, map=1
	    endif else if (mess1 ne '') or (mess2 ne '') then begin
		messval = '**** Error in output file settings ****'
                widget_control, state.messid, set_value=messval
		widget_control, state.flipid, set_value=1
                widget_control, state.graphbase, map=0
                widget_control, state.textbase, map=1
	    endif else begin
                new_event = {ID:parent, TOP:event.top, HANDLER:0L,  	$
                             ACTION:'Return'}
	    endelse
	end

		;*****************************
		;**** View selected graph ****
		;*****************************

	state.viewid: begin

		;***************************************************
		;**** First switch off all text output to avoid ****
		;**** unnecessary warning messages              ****
		;***************************************************

	    passval = passvalcom
	    textval = textvalcom
	    passval.outbut = 0
	    textval.outbut = 0
	    widget_control, state.passid, set_value=passval
            widget_control, state.textid, set_value=textval

                ;****************************************************
		;**** Check for errors in the graphics settings  ****
                ;**** Return the state before checking can start ****
                ;**** with the get_value keyword.                ****
                ;****************************************************

            widget_control, parent, set_uvalue=state, /no_copy
            error = 0

                ;**** Get a copy of the widget value ****

            widget_control, event.handler, get_value=os

                ;**** Check for widget error messages ****

            if os.grpout eq 1 and strtrim(os.grpfmess) ne '' then error=1
            if error eq 1 then warnmess = '**** Error in output settings ****'

		;*********************************************
                ;**** Check a device has been selected if ****
                ;**** hard copy requested                 ****
		;*********************************************

            if error ne 1 and os.grpout eq 1 and os.hrdout eq 1 and     $
            os.devsel eq -1 then begin
                warnmess = '**** Error: You must select an output device ****'
              error = 1
            endif

                ;**** Check for errors in scalings if relevant ****

            if os.lgraphno eq 0 and os.ldef1 eq 1 and error ne 1 then begin
                serror = 0
                serror = serror + num_chk(os.xmin1, sign=1)
                serror = serror + num_chk(os.xmax1, sign=1)
                serror = serror + num_chk(os.ymin1, sign=1)
                serror = serror + num_chk(os.ymax1, sign=1)
                if serror eq 0 then begin
                    if (float(os.xmin1) ge float(os.xmax1)) or          $
                    (float(os.ymin1) ge float(os.ymax1))                $
                    then serror = 1
                endif
                if serror gt 0 then begin
                    error = 1
                    warnmess = '**** Error: Tran'+   			$
                               'sition wave no. axis limits are inv'+	$
      			       'alid ****'
                endif
            endif else if os.lgraphno eq 1 and os.ldef2 eq 1            $
            and error ne 1 then begin
                serror = 0
                serror = serror + num_chk(os.xmin2, sign=1)
                serror = serror + num_chk(os.xmax2, sign=1)
                serror = serror + num_chk(os.ymin2, sign=1)
                serror = serror + num_chk(os.ymax2, sign=1)
                if serror eq 0 then begin
                    if (float(os.xmin2) ge float(os.xmax2)) or          $
                    (float(os.ymin2) ge float(os.ymax2))                $
                    then serror = 1
                endif
                if serror gt 0 then begin
                    error = 1
                    warnmess = '**** Error: '+       			$
                               'Scaled A-value axis limits'+            $
                               ' are invalid ****'
                endif
            endif else if os.lgraphno eq 2 and os.ldef3 eq 1            $
            and error ne 1 then begin
                serror = 0
                serror = serror + num_chk(os.xmin3, sign=1)
                serror = serror + num_chk(os.xmax3, sign=1)
                serror = serror + num_chk(os.ymin3, sign=1)
                serror = serror + num_chk(os.ymax3, sign=1)
                if serror eq 0 then begin
                    if (float(os.xmin3) ge float(os.xmax3)) or          $
                    (float(os.ymin3) ge float(os.ymax3))                $
                    then serror = 1
                endif
                if serror gt 0 then begin
                    error = 1
                    warnmess = '**** Error: '+       			$
                               'Scaled rate parameter axis limits'+     $
                               ' are invalid ****'
                endif
            endif

                ;**** Retrieve the state   ****

            widget_control, parent, get_uvalue=state, /no_copy

            if error eq 1 then begin
                widget_control, state.messid, set_value=warnmess
                new_event = 0L
            endif else begin
                new_event = {ID:parent, TOP:event.top, HANDLER:0L,      $
                             ACTION:'View'}
            endelse
	end

        ELSE: begin
            new_event = 0L
        end
    ENDCASE

                ;**** Return the state   ****

    widget_control, parent, set_uvalue=state, /no_copy

    RETURN, new_event

END

;-----------------------------------------------------------------------------

FUNCTION cw_adas203_out, parent, inval, procval, bitfile, 		$
                         VALUE=value, UVALUE=uvalue, FONT_LARGE=font,   $
			 FONT_SMALL=font_small, DEVLIST=devlist

    COMMON out203_block, textvalcom, passvalcom

    IF (N_PARAMS() EQ 0) THEN MESSAGE, 'Must specify parent for cw_adas203_out'
    ON_ERROR, 2                                 ;return to caller

                ;**** Set defaults for keywords ****

    IF NOT (KEYWORD_SET(devlist)) THEN devlist = ''
    IF NOT (KEYWORD_SET(uvalue)) THEN uvalue = 0
    IF NOT (KEYWORD_SET(font)) THEN font = ''
    IF NOT (KEYWORD_SET(font_small)) THEN font_small = ''

                ;**** Add flexible font-sizing ****

    machine = GETENV('TARGET_MACHINE')
    if machine eq 'HPUX' then begin
        large_font = font_small
    endif else begin
        large_font = font
    endelse

                ;**********************************************
                ;**** Create the 203 Output options widget ****
                ;**********************************************

                ;**** create base widget ****

    cwid = widget_base( parent, UVALUE=uvalue,                        	$
                        EVENT_FUNC="out203_event",                    	$
                        FUNC_GET_VALUE="out203_get_val",              	$
                        /COLUMN)

                ;**** Add dataset name and browse button ****

    dsfull = inval.rootpath + inval.file
    rc = cw_adas_dsbr(cwid, dsfull, font=font_small,                    $
                      filelabel='Script file: ', /row)

                ;****************************************************
                ;**** Add another base for the switching between ****
                ;**** Graphics and text output options           ****
                ;****************************************************

    flipbase = widget_base(cwid, /row)
    labelval = 'Select output option settings for display: '
    flipid = cw_bgroup(flipbase, ['Graphics','Text'], /exclusive,       $
                     font=large_font, label_left=labelval, /row,     	$
                     set_value=value.itoggle)

		;**** More bases for text and graphics options ****

    flipbase2 = widget_base(cwid)
    graphbase = widget_base(flipbase2, /column, /frame)
    textbase = widget_base(flipbase2, /column)

		;**** Add graphics output options ****

                ;**** Button to activate graphing ****

    graphbase1 = widget_base(graphbase, /row, /nonexclusive)
    grapflag = widget_button(graphbase1, value='Graphical output',      $
                             font=large_font)
    widget_control, grapflag, set_button=value.grpout

                ;**** Graph title ****

    titlebase = widget_base(graphbase, /row)
    rc = widget_label(titlebase, value='Graph Title', font=font_small)
    titleid = widget_text(titlebase, value=value.gtit1, font=font_small,$
                          /editable, xsize=30)

		;**** The 3 graphics options ****

    graphbase2 = widget_base(graphbase, /column, /frame)
    groptions = ['Scaled transition wave number vs. ion charge',	$
                 'Scaled A-value vs. ion charge',			$
		 'Scaled rate parameter vs. z-scaled electron temp'+	$
		 'erature']
    grshown = cw_bgroup(graphbase2, groptions, font=font_small,         $
                        set_value=value.lgraphno, /exclusive)
    widget_control, graphbase2, sensitive=value.grpout

		;**** Base for all plots ****

    plotbase = widget_base(graphbase, /frame)

                ;**** Transition wave number plot base ****

    if (value.lgraphno eq 0) then trmap=1 else trmap=0
    tranbase = widget_base(plotbase, /column, map=trmap)
    tranhead = widget_label(tranbase, font=font_small,                  $
                            value='Scaled transition wave number plot:-')
    tranbase3 = widget_base(tranbase, /row)
    tranbase3a = widget_base(tranbase3, /row, /nonexclusive)
    trandef = widget_button(tranbase3a, value='Explicit scaling  ',     $
                            font=font_small)
    widget_control, trandef, set_button=value.ldef1
    tranbase3b = widget_base(tranbase3, /column, /frame)
    tranbase3c = widget_base(tranbase3b, /row)
    tranxmin = widget_label(tranbase3c, font=font_small, value='X-min: ')
    txminbut = widget_text(tranbase3c, font=font_small, 		$
			   value=value.xmin1, xsize=8, /editable)
    tranxmax = widget_label(tranbase3c, font=font_small, value='X-max: ')
    txmaxbut = widget_text(tranbase3c, font=font_small, 		$
			   value=value.xmax1, xsize=8, /editable)
    tranbase3d = widget_base(tranbase3b, /row)
    tranxmin = widget_label(tranbase3d, font=font_small, value='Y-min: ')
    tyminbut = widget_text(tranbase3d, font=font_small, 		$
			   value=value.ymin1, xsize=8, /editable)
    tranxmax = widget_label(tranbase3d, font=font_small, value='Y-max: ')
    tymaxbut = widget_text(tranbase3d, font=font_small, 		$
			   value=value.ymax1, xsize=8, /editable)
    widget_control, tranbase3b, sensitive=value.ldef1

	 	;**** Rate parameter plot base ****

    if (value.lgraphno eq 2) then rpmap=1 else rpmap=0
    rpbase = widget_base(plotbase, /column, map=rpmap)
    rphead = widget_label(rpbase, font=font_small,                      $
                          value='Scaled rate parameter plot:-')
    rpbase3 = widget_base(rpbase, /row)
    rpbase3a = widget_base(rpbase3, /row, /nonexclusive)
    rpdef = widget_button(rpbase3a, value='Explicit scaling  ',         $
                           font=font_small)
    widget_control, rpdef, set_button=value.ldef3
    rpbase3b = widget_base(rpbase3, /column, /frame)
    rpbase3c = widget_base(rpbase3b, /row)
    rpxmin = widget_label(rpbase3c, font=font_small, value='X-min: ')
    rxminbut = widget_text(rpbase3c, font=font_small, 			$
			   value=value.xmin3, xsize=8, /editable)
    rpxmax = widget_label(rpbase3c, font=font_small, value='X-max: ')
    rxmaxbut = widget_text(rpbase3c, font=font_small, 			$
			   value=value.xmax3, xsize=8, /editable)
    rpbase3d = widget_base(rpbase3b, /row)
    rpymin = widget_label(rpbase3d, font=font_small, value='Y-min: ')
    ryminbut = widget_text(rpbase3d, font=font_small, 			$
			   value=value.ymin3, xsize=8, /editable)
    rpymax = widget_label(rpbase3d, font=font_small, value='Y-max: ')
    rymaxbut = widget_text(rpbase3d, font=font_small, 			$
			   value=value.ymax3, xsize=8, /editable)
    widget_control, rpbase3b, sensitive=value.ldef3

		;**** A-value plot base ****

    if (value.lgraphno eq 1) then avmap=1 else avmap=0
    avabase = widget_base(plotbase, /column, map=avmap)
    avahead = widget_label(avabase, font=font_small,                    $
                           value='Scaled A-value plot:-')
    avabase3 = widget_base(avabase, /row)
    avabase3a = widget_base(avabase3, /row, /nonexclusive)
    avadef = widget_button(avabase3a, value='Explicit scaling  ',       $
                           font=font_small)
    widget_control, avadef, set_button=value.ldef2
    avabase3b = widget_base(avabase3, /column, /frame)
    avabase3c = widget_base(avabase3b, /row)
    avaxmin = widget_label(avabase3c, font=font_small, value='X-min: ')
    axminbut = widget_text(avabase3c, font=font_small, 			$
			   value=value.xmin2, xsize=8, /editable)
    avaxmax = widget_label(avabase3c, font=font_small, value='X-max: ')
    axmaxbut = widget_text(avabase3c, font=font_small, 			$
			   value=value.xmax2, xsize=8, /editable)
    avabase3d = widget_base(avabase3b, /row)
    avaymin = widget_label(avabase3d, font=font_small, value='Y-min: ')
    ayminbut = widget_text(avabase3d, font=font_small, 			$
			   value=value.ymin2, xsize=8, /editable)
    avaymax = widget_label(avabase3d, font=font_small, value='Y-max: ')
    aymaxbut = widget_text(avabase3d, font=font_small, 			$
			   value=value.ymax2, xsize=8, /editable)
    widget_control, avabase3b, sensitive=value.ldef2

		;**** Base for label toggle ****

    togbase = widget_base(graphbase, /row, /frame)
    togval = 'Label lines with indices on rate parameter plot :'
    toglabel = widget_label(togbase, value=togval, font=font_small)
    labid = cw_bgroup(togbase, ['No','Yes'], /exclusive,       		$
                      font=font_small, /row, set_value=value.ilabel)
    widget_control, togbase, sensitive=value.grpout
    if value.lgraphno ne 2 then widget_control, togbase, sensitive=0

                ;**** Base for hard-copy widgets ****

    if value.hrdout ge 0 and strtrim(devlist(0)) ne '' then begin
        hardbase = widget_base(graphbase, /row, /frame)
        outfval = { 	OUTBUT		:	value.hrdout, 		$
			APPBUT		:	-1, 			$
			REPBUT		:	0,              	$
                    	FILENAME	:	value.hardname, 	$
			DEFNAME		:	value.grpdef,        	$
                    	MESSAGE		:	value.grpfmess }
        hardid = cw_adas_outfile(hardbase, OUTPUT='Enable Hard Copy',   $
                                 VALUE=outfval, FONT=font_small)
        hardbase2 = widget_base(hardbase, /column)
        devid = cw_single_sel(hardbase, devlist, title='Select Device', $
                              value=value.devsel, font=font_small)
    endif else begin
        devid = 0L
        hardid = 0L
        hardbase = 0L
        hardbase2 = 0L
    endelse

    		;**** Set the overall sensitivity of the bases ****

    widget_control, plotbase, sensitive=value.grpout
    if hardbase ne 0L then widget_control, hardbase, sensitive=value.grpout

		;**** Map/unmap graphics base according to toggle ****

    widget_control, graphbase, map=(1-value.itoggle)

		;**** Widget for passing file output ****

    passval = {		OUTBUT		:	value.copout,		$
			APPBUT		:	value.copapp,		$
			REPBUT		:	value.coprep,		$
			FILENAME	:	value.copdsn,		$
			DEFNAME		:	value.copdef,		$
			MESSAGE		:	value.copmes		}
    passvalcom = passval
    base = widget_base(textbase, /row, /frame)
    passid = cw_adas_outfile(base, OUTPUT='COPASE Passing File',	$
                             VALUE=passval, FONT=large_font)

                ;**** Widget for text output ****

    textval = {		OUTBUT		:	value.texout,		$
			APPBUT		:	value.texapp,		$
			REPBUT		:	value.texrep,		$
			FILENAME	:	value.texdsn,		$
			DEFNAME		:	value.texdef,		$
			MESSAGE		:	value.texmes		}
    textvalcom = textval
    base = widget_base(textbase, /row, /frame)
    textid = cw_adas_outfile(base, OUTPUT='Text Output',		$
                             VALUE=textval, FONT=large_font)

		;**** Map or unmap text base according to toggle ****

    widget_control, textbase, map=value.itoggle

                ;**** Error message ****

    messid = widget_label(cwid, value=' ', font=large_font)

                ;**** add the action buttons ****

    base = widget_base(cwid, /row)
    menufile = bitfile + '/menu.bmp'
    read_X11_bitmap, menufile, bitmap1
    outid = widget_button(base, value=bitmap1)          ;menu button
    procid = widget_button(base, value='Back to Processing',		$
			   font=font_small)
    viewid = widget_button(base, value='View Graph', font=font_small)
    widget_control, viewid, sensitive=value.grpout
    exitid = widget_button(base, value='Output and Exit',		$
			   font=font_small)
    dataid = widget_button(base, value='Output and Return Here',	$
			   font=font_small)

                ;*************************************************
                ;**** create a state structure for the pop-up ****
                ;**** window.                                 ****
                ;*************************************************

    new_state = {       MESSID		:	messid,			$
			FLIPID		:	flipid,			$
			PASSID		:	passid,			$
			TEXTID		:	textid,			$
			PROCID		:	procid,			$
			VIEWID		:	viewid,			$
			EXITID		:	exitid,			$
			DATAID		:	dataid,			$
			TEXTBASE	:	textbase,		$
			GRAPHBASE	:	graphbase,		$
			GRAPHBASE2	:	graphbase2,		$
			GRAPFLAG	:	grapflag,		$
			GRPOUT		:	value.grpout,		$
                        GRSHOWN         :       grshown,                $
			PLOTBASE	:	plotbase,		$
			TRANBASE	:	tranbase,		$
			AVABASE		:	avabase,		$
			RPBASE		:	rpbase,			$
			LDEF1		:	value.ldef1,		$
                        TRANDEF         :       trandef,                $
                        TRANBASE3B      :       tranbase3b,             $
                        TYMINBUT        :       tyminbut,               $
                        TYMAXBUT        :       tymaxbut,               $
                        TXMINBUT        :       txminbut,               $
                        TXMAXBUT        :       txmaxbut,               $
			LDEF2		:	value.ldef2,		$
                        AVADEF          :       avadef,                 $
                        AVABASE3B       :       avabase3b,              $
                        AYMINBUT        :       ayminbut,               $
                        AYMAXBUT        :       aymaxbut,               $
                        AXMINBUT        :       axminbut,               $
                        AXMAXBUT        :       axmaxbut,               $
			LDEF3		:	value.ldef3,		$
                        RPDEF           :       rpdef,                  $
                        RPBASE3B        :       rpbase3b,               $
                        RYMINBUT        :       ryminbut,               $
                        RYMAXBUT        :       rymaxbut,               $
                        RXMINBUT        :       rxminbut,               $
                	RXMAXBUT        :       rxmaxbut,               $
                        HARDBASE        :       hardbase,               $
                        HARDBASE2       :       hardbase2,              $
                        HARDID          :       hardid,                 $
                        DEVID           :       devid,                  $
			LABID		:	labid,			$
			TOGBASE		:	togbase,		$
			TITLEID		:	titleid,		$
			OUTID		:	outid			}

                ;**** Save initial state structure ****

    widget_control, parent, set_uvalue=new_state, /no_copy

    RETURN, cwid

END
