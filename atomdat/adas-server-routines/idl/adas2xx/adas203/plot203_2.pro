; Copyright (c) 1996 Strathclyde University .
; SCCS info: Module @(#)$Header: /home/adascvs/idl/adas2xx/adas203/plot203_2.pro,v 1.2 2004/07/06 14:29:29 whitefor Exp $ Date $Date: 2004/07/06 14:29:29 $
;+
; PROJECT:
;       ADAS IBM MVS to DEC UNIX conversion
;
; NAME:
;       PLOT203_2
;
; PURPOSE:
;       Plot A-value graphs for ADAS203.
;
; EXPLANATION:
;       This routine plots ADAS203 output for one plot
;
; USE:
;       Use is specific to ADAS203.  See adas203_2_plot.pro for
;       example.
;
; INPUTS:
;
;	LDEF2	-	Integer; 1 if user specified axis limits to 	
;			be used, 0 if default scaling to be used.
;
;	X	-	Fltarr; x-values to plot
;
;	Y	-	Fltarr; y-values to plot 
;
;	XO	-	Fltarr; x-value of interpolation point
;
;	Z	-	Fltarr; y-value of interpolation point
;
;	RIGHTSTRING -	String; first column of right hand label
;
;	RIGHTSTRING2- 	String; second column of right hand label
;
;	RIGHTSTRING3- 	String; third column of right hand label
;
;	RIGHTSTRING4- 	String; fourth column of right hand label
;
;	TITLE	-	String; heading to go above graph
;
;	XMIN2	-	Float; user-defined x-axis minimum
;
;	XMAX2	-	Float; user-defined x-axis maximum
;
;	YMIN2	-	Float; user-defined y-axis minimum
;
;       YMAX2   -       Float; user-defined y-axis maximum
;
; OPTIONAL INPUTS:
;       None.
;
; OUTPUTS:
;       None.
;
; OPTIONAL OUTPUTS:
;       None.
;
; KEYWORD PARAMETERS:
;       FONT    - String; Name of font passed to graphical output
;                 widget.
;
; CALLS:
;       None.
;
; SIDE EFFECTS:
;       None.
;
; CATEGORY:
;       Adas system.
;
; WRITTEN:
;       Tim Hammond, Tessella Support Services plc,  28th March 1996
;
; MODIFIED:
;       1.1     Tim Hammond      
;		First version
;	1.2     William Osborn
;               Added check for new environment variable VERY_SMALL
;               If this is set to the string "YES" (preferably in the
;               .adas_setup file) then the font size used on the graphs
;               is reduced and the positioning of labels adjusted
;               slightly to make things fit on a very small screen.
;
; VERSION:
;	1.1	01-04-96
;	1.2	11-10-96
;
;-
;----------------------------------------------------------------------------

PRO plot203_2, ldef2, x, y, xo, z, title, rightstring, rightstring2,   	$
               rightstring3, rightstring4, xmin2, xmax2, ymin2, ymax2

    COMMON Global_lw_data, left, right, tp, bot, grtop, grright

                ;****************************************************
                ;**** Suitable character size for current device ****
                ;**** Aim for 60 characters in y direction.      ****
                ;****************************************************

    charsize = (!d.y_vsize/!d.y_ch_size)/60.0
    small_check = GETENV('VERY_SMALL')
    if small_check eq 'YES' then charsize=charsize*0.8

                ;**** Initialise titles ****

    ytitle = 'SCALED A-VALUE (s!E-1!N)'
    xtitle = 'ION CHARGE + 1'
    righthead = '----- SEQUENCE MEMBERS  -----!C!C'+			$
                'INDX  NUC.CHG.  RECD.ION.  ELEM'
    erase
		;*****************************************************
		;**** Set xo and z back to arrays if they've been ****
		;**** erroneously converted to scalars by IDL     ****
		;*****************************************************

    xonew = fltarr(1)
    znew = fltarr(1)
    xonew(0) = xo(0)
    znew(0) = z(0)
    xosize = size(xo)
    zsize = size(z)
    if xosize(0) ne 1 then xo=xonew
    if zsize(0) ne 1 then z=znew

                ;**** How many points to plot ****

    npts = size(x)
    npts = npts(1)

		;**** Find x and y ranges for auto scaling,        ****
                ;**** check x and y in range for explicit scaling. ****

    makeplot = 1
    style = 0
    ystyle = 0

    if ldef2 eq 0 then begin

                ;**** identify values in the valid range ****
                ;**** plot routines only work within 	 ****
                ;**** single precision limits.       	 ****

	xvals = where (x gt 1.0e-37 and x lt 1.0e37)
	yvals = where (y gt 1.0e-37 and y lt 1.0e37)
	if xvals(0) gt -1 then begin
	    maxx = max(x(xvals))
	    minx = min(x(xvals))
	endif else begin
	    makeplot = 0
	endelse
	if yvals(0) gt -1 then begin
	    maxy = max(y(yvals))
	    miny = min(y(yvals))
	endif else begin
	    makeplot = 0
	endelse
	if makeplot eq 1 then begin
	    if miny le 1.0e-36 then begin
		ystyle = 1
		miny = 1.0e-36
	    endif else begin
		ystyle = 0
	    endelse
	endif
	style = 0
	if miny eq max(y) then begin
	    maxy=miny*10.0
	    miny=miny/10.0
	    ystyle = 1
	endif
    endif else begin
        minx = xmin2
	maxx = xmax2
	miny = ymin2
	maxy = ymax2
	xvals = where(x gt minx and x lt maxx)
	yvals = where(y gt miny and y lt maxy)
	if xvals(0) eq -1 or yvals(0) eq -1 then begin
	    makeplot = 0
	endif else begin
	    makeplot = 1
	endelse
	style = 1
        ystyle = 1
    endelse

    if makeplot eq 1 then begin

                ;**** Set up log-log plotting axes ****

	plot_oo, [minx, maxx], [miny, maxy], /nodata, 			$
                 position=[left, bot, grright, grtop],			$
                 xtitle=xtitle, ytitle=ytitle, xstyle=style, 		$
		 ystyle=ystyle, charsize=charsize

                ;*********************************
                ;**** Make and annotate plots ****
                ;*********************************

                oplot, x, y, linestyle=2
		oplot, xo, z, psym=7, symsize=charsize*1.3

    endif else begin			;no plot possible
        xyouts, 0.2, 0.5, /normal, charsize=charsize*1.5,               $
                '---- No data lies within range ----'
    endelse

                ;**** Output title above graphs ****
    
    xyouts, 0.1, 0.9, title, charsize=charsize, /normal

                ;**** Output titles to right of graphs ****

    xyouts, 0.69, 0.8, righthead, charsize=charsize, /normal
    xyouts, 0.69, 0.72, rightstring, charsize=charsize, /normal
    xyouts, 0.75, 0.72, rightstring2, charsize=charsize, /normal
    xyouts, 0.83, 0.72, rightstring3, charsize=charsize, /normal
    xyouts, 0.89, 0.72, rightstring4, charsize=charsize, /normal

END
