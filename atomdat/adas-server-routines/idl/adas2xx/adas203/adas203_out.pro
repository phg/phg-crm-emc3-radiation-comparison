; Copyright (c) 1996, Strathclyde University.
; SCCS info: Module @(#)$Header: /home/adascvs/idl/adas2xx/adas203/adas203_out.pro,v 1.2 2004/07/06 10:12:26 whitefor Exp $ Date $Date: 2004/07/06 10:12:26 $
;+
; PROJECT:
;       ADAS IBM MVS to UNIX conversion
;
; NAME:
;	ADAS203_OUT
;
; PURPOSE:
;	IDL ADAS user interface, output options.
;
; EXPLANATION:
;	This routine creates and manages a pop-up window which allows
;	the user to select options and input data to control ADAS203
;	graphical and file output.
;
; USE:
;	This routine is specific to adas203.                  
;
; INPUTS:
;	INVAL	- A structure containing the final settings of the input
;		  options widget.
;
;	PROCVAL	- A structure containing the current settings of the 
;		  processing options widget.
;
;	OUTVAL	- A structure which determines the initial settings of
;		  the output options widget.  The value is passed
;		  unmodified into cw_adas203_out.pro.
;
;		  See cw_adas203_out.pro for a full description of this
;		  structure.
;
;       BITFILE - String; the path to the dirctory containing bitmaps
;                 for the 'escape to series menu' button.
;
; OPTIONAL INPUTS:
;	None.
;
; OUTPUTS:
;	ACT	- String; value depends on which button the
;		  user pressed to terminate the output options window.
;
; OPTIONAL OUTPUTS:
;	None.
;
; KEYWORD PARAMETERS:
;	DEVLIST	- A string array;  Each element of the array is the
;		  name of a hardcopy graphics output device.  This
;		  list must mirror the DEVCODE list.  DEVLIST is the
;		  list which the user will see.
;		  e.g ['Post-Script','HP-PCL','HP-GL']
;
;       FONT_LARGE- Supplies the large font to be used.
;
;       FONT_SMALL- Supplies the small font to be used.
;
; CALLS:
;	CW_ADAS203_OUT	Creates the output options widget.
;	DYNLABEL	Recursively adds dynamic_resize keyword to label
;			widgets for version 4.0.1 and higher of IDL
;	XMANAGER
;	See side effects for widget management routine.
;
; SIDE EFFECTS:
;	This routine uses a common block OUT203_BLK to maintain its state.
;	ADAS203_OUT_EV	is included in this file and is called
;			indirectly from XMANAGER during widget
;			management.
;
; CATEGORY:
;	Widgets
;
; WRITTEN:
;       Tim Hammond, Tessella Support Services plc, 7-Mar-1996 
;
; MODIFIED:
;	1.1	Tim Hammond	
;		First version
;	1.2	William Osborn
;		Added dynlabel procedure
;
; VERSION:
;	1.1	01-04-96
;	1.2	04-06-96
; 
;-
;-----------------------------------------------------------------------------

PRO adas203_out_ev, event

    COMMON out203_blk, action, value
	
		;**** Find the event type and copy to common ****

    action = event.action

    CASE action of

		;**** 'View graph' button ****
	'View' : begin
                        ;**** Get the output widget value ****

             child = widget_info(event.id, /child)
             widget_control, child, get_value=value

               	;*****************************************
               	;**** Kill the widget to allow IDL to ****
               	;**** continue                        ****
               	;*****************************************

            widget_control, event.top, /destroy
	end

		;**** 'Output and exit' button ****

	'Exit' : begin

		;**** Get the output widget value ****

     	     child = widget_info(event.id, /child)
	     widget_control, child, get_value=value 

                ;*****************************************
		;**** Kill the widget to allow IDL to ****
		;**** continue                        ****
		;*****************************************

	    widget_control, event.top, /destroy
	end

		;**** 'Output and return' button ****

	'Return' : begin

               	;**** Get the output widget value ****

             child = widget_info(event.id, /child)
             widget_control, child, get_value=value

                ;*****************************************
                ;**** Kill the widget to allow IDL to ****
                ;**** continue                        ****
                ;*****************************************

            widget_control, event.top, /destroy
        end

		;**** 'Cancel' button ****

	'Cancel': widget_control, event.top, /destroy

		;**** 'Menu' button ****

	'Menu': widget_control, event.top, /destroy

    ENDCASE

END

;-----------------------------------------------------------------------------

PRO adas203_out, inval, procval, outval, act, bitfile, 			$
                 devlist=devlist, font_large=font, font_small=font_small

    COMMON out203_blk, action, value

		;**** Copy value to common ****

    value = outval

		;**** Set defaults for keywords ****

    if not (keyword_set(font)) then font = ''
    if not (keyword_set(font_small)) then font_small = ''
    if not (keyword_set(devlist)) then devlist = ''

		;***************************************
		;**** Pop-up a new widget           ****
		;***************************************

                ;**** create base widget ****

    outid = widget_base(title='ADAS203 OUTPUT OPTIONS', xoffset=100,	$
                        yoffset=1)

		;**** Declare output options widget ****

    cwid = cw_adas203_out(outid, inval, procval, bitfile, 		$
                          value=outval, devlist=devlist, 		$
		          font_large=font, font_small=font_small)

		;**** Realize the new widget ****

    dynlabel, outid
    widget_control, outid, /realize

		;**** make widget modal ****

    xmanager, 'adas203_out', outid, event_handler='adas203_out_ev',	$
              /modal, /just_reg
 
		;**** Return the output value from common ****

    act = action
    outval = value

END

