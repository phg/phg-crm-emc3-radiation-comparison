; Copyright (c) 1996, Strathclyde University.
; SCCS info: Module @(#)$Header: /home/adascvs/idl/adas2xx/adas203/cw_adas203_proc.pro,v 1.9 2004/07/06 12:30:08 whitefor Exp $ Date $Date: 2004/07/06 12:30:08 $
;+
; PROJECT:
;       ADAS IBM MVS to UNIX conversion
;
; NAME:
;       CW_ADAS203_PROC()
;
; PURPOSE:
;       Produces a widget for ADAS203 processing options/input.
;
; EXPLANATION:
;       This function creates a compound widget consisting of :-
;		a typein box for the title of the run,
;		the filename and a Browse Comments button,
;		an information box containing data on the members in the file
;		and a list widget to select one,
;		an information box containing data on the electron impact
;		transitions in the file and a list widget to select one,
;		A toggle to select between:
;			A table widget with associated buttons containing
;			the output electron temperatures,
;			A set of toggles and typeins defining the settings
;			and scalings for the selected transition and member,
;           	a message widget, and 'Cancel', 'Menu' and 'Done' buttons.
;
;       The compound widgets included in this widget are self managing.
;
;       This widget only generates events for the 'Done', 'Menu' and
;       'Cancel' buttons.
;
; USE:
;       This widget is specific to ADAS203, see adas203_proc.pro
;       for use.
;
; INPUTS:
;       TOPPARENT  - Long integer, ID of parent widget.
;
;       BITFILE    - String; the path to the directory containing bitmaps
;                    for the 'escape to series menu' button.
;
;       INVAL      - A structure containing the final settings of the input
;                    options screen.
;
;	DSFULL	   - A string containing the full UNIX name of the 
;		     general-z file being interrogated.
; OPTIONAL INPUTS:
;       None
;
; OUTPUTS:
;       The return value of this function is the ID of the declared widget.
;
; OPTIONAL OUTPUTS:
;       None
;
; KEYWORD PARAMETERS:
;       PROCVAL - A structure which determines the initial settings of
;                 the processing options widget.
;
;                 The default PROCVAL is created in b3ispf.pro - see that
;                 routine for a detailed description.
;
;       UVALUE  - A user value for the widget. Default 0.
;
;       FONT_LARGE - The name of a larger font.  Default current system
;                    font
;
;       FONT_SMALL - The name of a smaller font. Default current system
;                    font.
;
;       EDIT_FONTS - A structure of two fonts used in the table editor
;                    adas_edtab.pro, {font_norm:'',font_input:''}.  Defaults
;                    to current system font.
;
;       NUM_FORM   - String; Numeric format to use in tables.  Default
;                       '(E10.3)'
;
;
; CALLS:
;       CW_ADAS_TABLE   Adas data table widget.
;       READ_X11_BITMAP Reads in the bitmap for the 'escape to series
;                       menu' button.
;       NUM_CHK         Checks validity of a user's numbers
;       CW_BGROUP       Button list compound widget
;       CW_BSELECTOR    Pull-down menu compound widget
;       I4Z0IE          Converts atomic number into element symbol
;	POPUP		Simple warning message widget
;	XXTCON		Temperature conversion routine
;
;       See side effects for other related widget management routines.
;
; SIDE EFFECTS:
;
;       The following widget management routines are included in this file;
;       PROC203_GET_VAL()       Returns the current PROCVAL structure.
;       PROC203_EVENT()         Process and issue events.
;
; CATEGORY:
;       Compound Widget
;
; WRITTEN:
;       Tim Hammond, Tessella Support Services plc, 1st March 1996
;
; MODIFIED:
;       1.1     Tim Hammond
;               First version - incomplete
;	1.2	Tim Hammond
;		Added all event handling except for Done button.
;	1.3	Tim Hammond
;		Added event handling for the Done button.
;	1.4	Tim Hammond
;		Completed get_val routine.
;	1.5	Tim Hammond
;		Added auto-updating of parameters when new transition
;		chosen.
;	1.6	Tim Hammond
;		Added 'Are you sure?' popup when user hits Cancel.
;	1.7	Tim Hammond
;		Modified form of member selection input widget and 
;		changed associated checking and functionality.
;	1.8   Richard Martin
;		Increased no. of temps from 12 to 14 in popup 'only first
;		14 temperatures used' warning message.
;	1.9	Richard Martin
;		Changed name of widgetlabel from switch to lswitch for
;		IDL 5.4 compatibility.
;
; VERSION:
;       1.1     04-03-96
;	1.2	05-03-96
;	1.3	05-03-96
;	1.4	05-03-96
;	1.5	05-03-96
;	1.6	06-03-96
;	1.7	29-03-96
;	1.8	08-04-98
;	1.9	10-11-00
;
;-
;-----------------------------------------------------------------------------

FUNCTION proc203_get_val, id

    COMMON proc203_block, il, itran, izs, scefa, iz0val, i1a, 		$
                          i2a, strga, ndlev, ndtin, ndtrn, ndtem,	$
			  izmax, izdimd, zstrga, nva, iec1a, iac1a,	$
			  iac2a, fac2a, igc1a, fgc2a, lbpts

                 ;***************************************
                 ;****     Retrieve the state        ****
                 ;**** Get first_child widget id     ****
                 ;**** because state is stored there ****
                 ;***************************************

    first_child = widget_info(id, /child)
    widget_control, first_child, get_uvalue = state

                ;***********************************
                ;**** Get run title from widget ****
                ;**** Then centre in in string  ****
                ;**** of 40 characters          ****
                ;***********************************

    widget_control, state.runid, get_value=title
    title = strcompress(title(0))
    title_len = strlen(title)
    if (title_len gt 40 ) then begin
        title = strmid(title,0,37)
        widget_control, state.messid, set_value="Title too long - truncated "
        widget_control, state.runid, set_value=title
        wait ,1
    endif
    pad = (40 - title_len)/2
    spaces = '                                         '
    title(0) = strmid(spaces,0,(pad-1)) + title + strmid(spaces,0,(pad-1))

		;**************************
		;**** Get toggle value ****
		;**************************

    widget_control, state.toggleid, get_value=itoggle1

		;******************************
		;**** Get parameter values ****
		;******************************

    widget_control, state.index1id, get_value=iz0string
    iz0 = fix(iz0string)
    widget_control, state.index2id, get_value=istrn
    scef = fltarr(ndtem)
    scef(*) = scefa(*,istrn)
    widget_control, state.iec1aid, get_value=iec1aval
    widget_control, state.iac1aid, get_value=iac1aval
    widget_control, state.iac2aid, get_value=iac2aval
    widget_control, state.fac2aid, get_value=fac2aval
    widget_control, state.igc1aid, get_value=igc1aval
    widget_control, state.fgc2aid, get_value=fgc2aval
    iec1a(istrn) = iec1aval
    iac1a(istrn) = iac1aval
    iac2a(istrn) = iac2aval
    fac2a(istrn) = fac2aval
    igc1a(istrn) = igc1aval
    fgc2a(istrn) = fgc2aval
    widget_control, state.libptid, get_value=libptval
    lbpts(istrn) = libptval

		;*********************************
		;**** Get output temperatures ****
		;*********************************

    widget_control, state.tempid, get_value=tempval
    tscef = fltarr(ndtem,3)
    tempvals = where(strcompress(tempval.value(3,*), /remove_all) ne '')
    if tempvals(0) ne -1 then begin
        num_temps = n_elements(tempvals)
	tscef(0:num_temps-1,*) = float(tempval.value(1:3, 0:num_temps-1))
    endif else begin
	num_temps = 0
    endelse
    tine = fltarr(ndtin)
    tempvals = where(strcompress(tempval.value(0,*), /remove_all) ne '')
    if tempvals(0) ne -1 then begin
        maxt  = n_elements(tempvals)
        tine(0:maxt-1) = float(tempval.value(0,0:maxt-1))
    endif else begin
	maxt = 0
    endelse
    ifout = tempval.units + 1

                ;***********************************************
                ;**** write selected values to PS structure ****
                ;***********************************************

     ps = {             NEW             :       0,			$
			TITLE		:	title,			$
			ITOGGLE1	:	itoggle1,		$
			IZS		:	izs,			$
			IZ0		:	iz0,			$
			IZ0VAL		:	iz0val,			$
			ITRAN		:	itran,			$
			IL		:	il,			$
			I1A		:	i1a,			$
			I2A		:	i2a,			$
			STRGA		:	strga,			$
			NDLEV		:	ndlev,			$
			NDTRN		:	ndtrn,			$
			NDTIN		:	ndtin,			$
			NDTEM		:	ndtem,			$
			IZMAX		:	izmax,			$
			IZDIMD		:	izdimd,			$
			ZSTRGA		:	zstrga,			$
			NVA		:	nva,			$
			SCEFA		:	scefa,			$
			NV		:	nva(istrn),		$
			SCEF		:	scef,			$
			TSCEF		:	tscef,			$
			ISTRN		:	istrn,			$
			IEC1A		:	iec1a,			$
			IAC1A		:	iac1a,			$
			IAC2A		:	iac2a,			$
			FAC2A		:	fac2a,			$
			IGC1A		:	igc1a,			$
			FGC2A		:	fgc2a,			$
			LBPTS		:	lbpts,			$
			LIBPT		:	libptval,		$
			IFOUT		:	ifout,			$
			MAXT		:	maxt,			$
			TINE		:	tine			}

    widget_control, first_child, set_uvalue=state, /no_copy

    RETURN, ps

END

;-----------------------------------------------------------------------------

FUNCTION proc203_event, event

    COMMON proc203_block, il, itran, izs, scefa, iz0val, i1a,		$
			  i2a, strga, ndlev, ndtin, ndtrn, ndtem,	$
			  izmax, izdimd, zstrga, nva, iec1a, iac1a,	$
			  iac2a, fac2a, igc1a, fgc2a, lbpts

                ;************************************
                ;**** Base ID of compound widget ****
                ;************************************

    parent = event.handler

                ;**********************************************
                ;**** Retrieve the user value state        ****
                ;**** Get id of first_child widget because ****
                ;**** user value "state" is stored there   ****
                ;**********************************************

    first_child = widget_info(parent, /child)
    widget_control, first_child, get_uvalue=state, /no_copy

                ;*********************************
                ;**** Clear previous messages ****
                ;*********************************

    widget_control, state.messid, set_value=' '

                ;************************
                ;**** Process events ****
                ;************************

    CASE event.id OF

		;*********************
		;**** Base switch ****
		;*********************

   	state.toggleid: begin
	    widget_control, state.toggleid, get_value=itoggle1
	    if itoggle1 eq 0 then begin
                widget_control, state.setbase, map=1
                widget_control, state.setbase3, map=0
	    endif else if itoggle1 eq 1 then begin
                widget_control, state.setbase, map=0
                widget_control, state.setbase3, map=1
	    endif 
	end

		;****************************
                ;**** Clear table button ****
		;****************************

        state.clearid: begin

		;*********************************
                ;**** popup window to confirm ****
		;*********************************

	    clearmess = 'Are you sure you want to clear the table?'
            action = popup(message=clearmess, font=state.font_large,	$
                           buttons=['Confirm','Cancel'], 		$
                           title='ADAS203 Warning:-')

            if action eq 'Confirm' then begin

		;****************************************
                ;**** Get current table widget value ****
		;****************************************

                widget_control, state.tempid, get_value=tempval

		;***********************************
                ;**** Zero the relevant columns ****
		;***********************************

                empty = strarr(ndtin)
                tempval.value(0,*) = empty

		;***************************************
                ;**** Copy new data to table widget ****
		;***************************************

                widget_control, state.tempid, set_value=tempval
            endif
        end

                ;*************************************
                ;**** Default Energies button     ****
                ;*************************************

        state.deftid: begin

		;************************************************************
                ;**** popup window to confirm overwriting current values ****
		;************************************************************

	    deftmess = 'Confirm Overwrite values with Defaults'
            action = popup(message=deftmess, font=state.font_large,	$
                           buttons=['Confirm','Cancel'],		$
		    	   title='ADAS203 Warning:-')
            if action eq 'Confirm' then begin

		;****************************************
                ;**** Get current table widget value ****
		;****************************************

                widget_control, state.tempid, get_value=tempval

                ;***********************************************
                ;**** Copy defaults into value structure    ****
                ;***********************************************

                units = tempval.units+1
		valcheck = strcompress(tempval.value(units,*), /remove_all)
		values = where(valcheck ne '')
	 	num_values = n_elements(values)
		if values(0) ne -1 then begin
                    tempval.value(0,0:num_values-1) =                	$
                    strtrim(string(tempval.value(units,0:num_values-1),	$
                    format=state.num_form),2)
		endif

		;***************************************
                ;**** Copy new data to table widget ****
		;***************************************

                widget_control, state.tempid, set_value=tempval
            endif
        end

		;**************************************
		;**** New sequence member selected ****
		;**************************************

	state.index1id: begin
	    widget_control, state.index1id, get_value=i1val
     	    if num_chk(i1val(0), sign=1, /integer) eq 0 then begin
	        iz1 = fix(i1val(0)) - izs + 1

		;*************************************
		;**** Get temperatures from table ****
		;*************************************

	        widget_control, state.tempid, get_value=tempval

		;************************************************
		;**** Convert the Kelvin and eV temperatures ****
		;************************************************

	        tempvals = strcompress(tempval.value(3,*), /remove_all)
	        vals = where(tempvals ne '')
	        num_vals = n_elements(vals)
	        if vals(0) ne -1 then begin
	            redtemps = float(tempval.value(3,0:num_vals-1))
	            int1 = 1
	            int2 = 2
	            int3 = 3
	            xxtcon, int3, int1, iz1, num_vals, redtemps, keltemps
	            xxtcon, int3, int2, iz1, num_vals, redtemps, evtemps
	            tempval.value(1,0:num_vals-1) =			$
	            strtrim(string(keltemps, format=state.num_form), 2)
	            tempval.value(2,0:num_vals-1) =			$
	            strtrim(string(evtemps, format=state.num_form), 2)

		;***************************************
                ;**** Copy new data to table widget ****
		;***************************************
    
                    widget_control, state.tempid, set_value=tempval
	        endif
	    endif
	end

                ;*********************************
                ;**** New transition selected ****
                ;*********************************

	state.index2id: begin
	    widget_control, state.index2id, get_value=i2val

		;************************************
		;**** Get member information too ****
		;************************************

	    widget_control, state.index1id, get_value=i1val
     	    if num_chk(i1val(0), sign=1, /integer) eq 0 then begin
	        iz1 = fix(i1val(0)) - izs + 1

		;*************************************
		;**** Get temperatures from table ****
		;*************************************

	        widget_control, state.tempid, get_value=tempval

		;******************************************************
		;**** Replace reduced temps. with appropriate ones ****
		;**** for the selected transition                  ****
		;******************************************************

	        redtemps = scefa(*,i2val)
	        redvals = strcompress(redtemps(*), /remove_all)
	        vals = where(redvals ne '')
	        num_vals = n_elements(vals)
	        if vals(0) ne -1 then begin
	            redfloat = float(redtemps(0:num_vals-1))
	            int1 = 1
	            int2 = 2
	            int3 = 3
	            xxtcon, int3, int1, iz1, num_vals, redtemps, keltemps
	            xxtcon, int3, int2, iz1, num_vals, redtemps, evtemps
		    tempval.value(3,0:num_vals-1) =			$
		    strtrim(string(redfloat, format=state.num_form), 2)
	            tempval.value(1,0:num_vals-1) =			$
	            strtrim(string(keltemps, format=state.num_form), 2)
	            tempval.value(2,0:num_vals-1) =			$
	            strtrim(string(evtemps, format=state.num_form), 2)

		;***************************************
                ;**** Copy new data to table widget ****
		;***************************************

                    widget_control, state.tempid, set_value=tempval
	        endif
	    endif

		;************************************************
		;**** Reset all parameters to correct values ****
		;************************************************

	    widget_control, state.iec1aid, set_value=iec1a(i2val)
	    widget_control, state.iac1aid, set_value=iac1a(i2val)
	    widget_control, state.iac2aid, set_value=iac2a(i2val)
	    fac2aval = string(fac2a(i2val), format='(f5.2)')
	    widget_control, state.fac2aid, set_value=fac2aval
	    widget_control, state.igc1aid, set_value=igc1a(i2val)
	    fgc2aval = string(fgc2a(i2val), format='(f5.2)')
	    widget_control, state.fgc2aid, set_value=fgc2aval
	    widget_control, state.libptid, set_value=lbpts(i2val)
	end

                ;***********************
                ;**** Cancel button ****
                ;***********************

        state.cancelid: begin
	    messval = 'Are you sure you want to cancel?'
	    line2val = 'Any modified parameters will be lost.'
	    buttonval = [' Proceed with Cancel ',			$
			 ' Remain on Processing Screen ']
	    action = popup(message=messval, line2=line2val,		$
		           font=state.font_large,			$
		           buttons=buttonval,				$
		           title='ADAS203 Warning:-',			$
		           YOFFSET=400, XOFFSET=200)
	    if action eq ' Proceed with Cancel ' then begin
		new_event = {ID:parent, TOP:event.top,              	$
                             HANDLER:0L, ACTION:'Cancel'}
	    endif else begin
		new_event = 0L
	    endelse
	end

                ;*********************
                ;**** Menu button ****
                ;*********************

        state.outid: new_event = {ID:parent, TOP:event.top,                 $
                                  HANDLER:0L, ACTION:'Menu'}

                ;*********************
                ;**** Done button ****
                ;*********************

        state.doneid: begin

                ;***************************************
                ;**** Check all user input is legal ****
                ;***************************************

            error = 0

		;****************************************
		;**** Check entered ion for analysis ****
		;****************************************

	    widget_control, state.index1id, get_value=index1val
	    er_check = num_chk(index1val(0), sign=1, /integer)
  	    if er_check ne 0 then begin
		error = 1
		message = '**** Error: You have entered an invalid nu'+	$
		          'clear charge for the required member ****'
		widget_control, state.index1id, /input_focus
	    endif else begin
		if fix(index1val(0)) eq 0 then begin
		    error = 1
		    message = '**** Error: The nuclear charge for req'+	$
			      'uired member must be > 0 ****'
		    widget_control, state.index1id, /input_focus
		endif
	    endelse

                ;******************************
                ;**** Check parameters (I) ****
                ;******************************

	    if error eq 0 then begin
                widget_control, state.fac2aid, get_value=fac2aval
	        fac2achk = strcompress(fac2aval(0), /remove_all)
	        if fac2achk eq '' then begin
		    error = 1
		    message = '**** Error: You must enter a value for the'+$
		              ' Z1 power (Einstein coefficient settings) '+$
			      '****'
	        endif else if num_chk(fac2aval(0)) ne 0 then  begin
		    error = 1
		    message = '**** Error: The Z1 power (Einstein coeffic'+$
			      'ient settings) is invalid ****'
	        endif
	        if error eq 1 then begin
		    widget_control, state.fac2aid, /input_focus
		    widget_control, state.toggleid, set_value=0
		    widget_control, state.setbase, map=1
		    widget_control, state.setbase3, map=0
	        endif
	    endif

		;*******************************
		;**** Check parameters (II) ****
		;*******************************

	    if error eq 0 then begin
                widget_control, state.fgc2aid, get_value=fgc2aval
                fgc2achk = strcompress(fgc2aval(0), /remove_all)
                if fgc2achk eq '' then begin
                    error = 1
                    message = '**** Error: You must enter a value '+	$
			      'for the Z1 power (Upsilon coefficie'+	$
			      'nt settings) ****'
                endif else if num_chk(fgc2aval(0)) ne 0 then  begin
                    error = 1
                    message = '**** Error: The Z1 power (Upsilon c'+	$
			      'oefficient settings) is invalid ****'
                endif
                if error eq 1 then begin
                    widget_control, state.fgc2aid, /input_focus
                    widget_control, state.toggleid, set_value=1
                    widget_control, state.setbase, map=0
                    widget_control, state.setbase3, map=1
                endif
	    endif

		;***********************************
		;**** Check output temperatures ****
		;***********************************
	
	    if error eq 0 then begin
		widget_control, state.tempid, get_value=tempval
		tempvals = where(strcompress(tempval.value(0,*),	$
		/remove_all) ne '')
		if tempvals(0) eq -1 then begin
		    error = 1
		    message = '**** Error: No output electron temper'+	$
			      'atures entered ****'
		endif else begin
		    num_temps = n_elements(tempvals)
		    if num_temps gt 14 then begin

		;*************************************************
		;**** Only first 14 temperatures will be used ****
		;*************************************************

			messval = 'You have entered more than 14 ele'+	$
				  'ctron temperatures. '
			line2val = 'The specific ion passing file wi'+	$
				   'll only use the first 14.'
			action = popup(message=messval, line2=line2val,	$ 
				       font=state.font_large,		$
				       buttons=[' Cancel ',' OK '],	$
				       title='ADAS203 Warning:-',	$
				       XOFFSET=200)
			if action eq ' Cancel ' then begin
			    error = 1
			    message = ''
			endif
		    endif
		endelse
	    endif

                ;************************************
                ;**** return value or flag error ****
                ;************************************

            if error eq 0 then begin
		new_event = {ID:parent, TOP:event.top,                  $
                             HANDLER:0L, ACTION:'Done'}
            endif else begin
                widget_control, state.messid, set_value=message
                new_event = 0L
            endelse
        end

        ELSE: new_event = 0L

    ENDCASE

                ;*********************************************
                ;*** make "state" available to other files ***
                ;*********************************************

    widget_control, first_child, set_uvalue=state, /no_copy

    RETURN, new_event

END

;-----------------------------------------------------------------------------

FUNCTION cw_adas203_proc, topparent, bitfile, inval, dsfull,		$
                          PROCVAL=procval, FONT_LARGE=font_large,	$ 
                          FONT_SMALL=font_small, EDIT_FONTS=edit_fonts,	$
                          NUM_FORM=num_form, UVALUE=uvalue

    COMMON proc203_block, ilcom, itrancom, izscom, scefacom, iz0valcom,	$
			  i1acom, i2acom, strgacom, ndlevcom, ndtincom,	$
			  ndtrncom, ndtemcom, izmaxcom, izdimdcom,	$
			  zstrgacom, nvacom, iec1acom, iac1acom,	$
			  iac2acom, fac2acom, igc1acom, fgc2acom, 	$
			  lbptscom

                ;***********************************
                ;**** Return to caller on error ****
                ;***********************************

    ON_ERROR, 2

                ;***********************************
                ;**** Set defaults for keywords ****
                ;***********************************

    if not (keyword_set(uvalue)) then uvalue = 0
    if not (keyword_set(font_large)) then font_large = ''
    if not (keyword_set(font_small)) then font_small = ''
    if not (keyword_set(edit_fonts)) then                               $
    edit_fonts = {font_norm:'',font_input:''}
    if not (keyword_set(num_form)) then num_form = '(E10.3)'

		;*********************************
		;**** Initialise common block ****
		;*********************************

    ilcom = procval.il
    itrancom = procval.itran
    izscom = procval.izs
    scefacom = procval.scefa
    iz0valcom = procval.iz0val
    i1acom = procval.i1a
    i2acom = procval.i2a
    strgacom = procval.strga
    ndlevcom = procval.ndlev
    ndtrncom = procval.ndtrn
    ndtincom = procval.ndtin
    ndtemcom = procval.ndtem
    izmaxcom = procval.izmax
    izdimdcom = procval.izdimd
    zstrgacom = procval.zstrga
    nvacom = procval.nva
    iec1acom = procval.iec1a 
    iac1acom = procval.iac1a
    iac2acom = procval.iac2a
    fac2acom = procval.fac2a
    igc1acom = procval.igc1a
    fgc2acom = procval.fgc2a
    lbptscom = procval.lbpts

                ;*********************************************************
                ;**** Modify certain parameters and results depending ****
                ;**** on the machine being used                       ****
                ;*********************************************************

    machine = GETENV('TARGET_MACHINE')
    if machine eq 'HPUX' then begin
        large_font = font_small
	y_size = 5
    endif else begin
        large_font = font_large
	y_size = 6
    endelse

                ;********************************************************
                ;**** Create the 203 Processing options/input window ****
                ;********************************************************

                ;***********************************
                ;**** Create titled base widget ****
                ;***********************************

    parent = widget_base(topparent, UVALUE = uvalue,                    $
                         title = 'ADAS203 PROCESSING OPTIONS',          $
                         EVENT_FUNC = "proc203_event",                  $
                         FUNC_GET_VALUE = "proc203_get_val",            $
                         /COLUMN)

                ;******************************************************
                ;**** Create a dummy widget just to hold value of *****
                ;**** "state" variable so as not to get confused  *****
                ;**** with any other values. Adopt IDL practice   *****
                ;**** of using first child widget                 *****
                ;******************************************************

    first_child = widget_base(parent)
    topbase = widget_base(first_child, /column)

                ;***********************
                ;**** add run title ****
                ;***********************

    base = widget_base(topbase, /row)
    rc = widget_label(base, value='Title for Run', font=large_font)
    runid = widget_text(base, value=procval.title, xsize=38,		$
                        font=large_font, /edit)

		;********************************************
                ;**** add dataset name and browse button ****
		;********************************************

    rc = cw_adas_dsbr(topbase, dsfull, font=large_font,                 $
                      filelabel='General-z file: ', /row)

		;*********************************************************
		;**** Add a base for the transition/member selections ****
		;*********************************************************

    selectbase = widget_base(topbase, /row, /frame)

		;**************************************
		;**** Add member selection widgets ****
		;**************************************

    membase = widget_base(selectbase, /column, /frame)
    memheadval = 'Select Ion For Analysis:-'
    memhead = widget_label(membase, font=large_font,			$
			   value=memheadval)
    infolabel = 'Recombined ion isoelectronic sequence = '
    i4z0ie, procval.izs, element
    infolabel = infolabel + element
    memlabel = widget_label(membase, font=font_small, value=infolabel)

		;*****************************
		;**** Add the member list ****
		;*****************************

    mem_info = procval.zstrga(0:procval.izmax-1)
    titles = ['Index   Nuclear    Recombined    Element',		$
	      '        Charge     Ion Charge    Symbol ']
    index1title = '---------------- Members -----------------'
    infolabel = widget_label(membase, font=font_small, 			$
		             value=index1title)
    info = [titles, mem_info]
    list1id = widget_list(membase, value=info, font=font_small, 	$
                          ysize=y_size)
    membase2 = widget_base(membase, /row)
    if procval.iz0 gt 0 then begin
        index1val = strcompress(string(procval.iz0), /remove_all)
    endif else begin
	index1val = ' '
    endelse
    indexlabel = widget_label(membase2, font=font_small, value='Speci'+	$
                             'fy nuclear charge of member required :')
    index1id = widget_text(membase2, font=font_small, value=index1val,	$
			   /all_events, /editable, xsize=4)

		;*****************************************
		;**** Add electron transition widgets ****
		;*****************************************

    tranbase = widget_base(selectbase, /column, /frame)
    tranhead = widget_label(tranbase, font=large_font,          	$
                           value='Select Specific Electron Impact '+	$
                           'Transition:-')
    padding = '                          '
    infolabel1 = 'No. of electron impact transitions = ' +		$
    strcompress(string(procval.itran), /remove_all) + padding
    tranlabel1 = widget_label(tranbase, font=font_small, value=infolabel1)
    infolabel2 = 'No. of index energy levels         = ' +		$
    strcompress(string(procval.il), /remove_all) + padding
    tranlabel2 = widget_label(tranbase, font=font_small, value=infolabel2)

		;*********************************
		;**** Add the transition list ****
		;*********************************

    tran_info = strarr(3, procval.itran)
    tran_info(0,*) = strcompress(string(indgen(procval.itran) + 1), 	$
    /remove_all)
    for i=0, procval.itran-1 do begin
	if (procval.i2a(i) lt 9) then begin
	    tran_info(1,i) = 						$
	    strcompress(string(procval.i2a(i)+1), /remove_all) + 	$
	    '      ' + strtrim(procval.strga(procval.i2a(i)),2)
	endif else begin
	    tran_info(1,i) =                                            $
            strcompress(string(procval.i2a(i)+1), /remove_all) + 	$
            '     ' + strtrim(procval.strga(procval.i2a(i)),2)
	endelse
	if (procval.i1a(i) lt 9) then begin
	    tran_info(2,i) = 						$
	    strcompress(string(procval.i1a(i)+1), /remove_all) + 	$
	    '      ' + strtrim(procval.strga(procval.i1a(i)),2) 
	endif else begin
	    tran_info(2,i) =                                            $
            strcompress(string(procval.i1a(i)+1), /remove_all) + 	$
	    '     ' + strtrim(procval.strga(procval.i1a(i)),2)
	endelse
    endfor
    titles = [['Transition','----- Upper Level -----',			$
	       '----- Lower Level -----'],				$
	      ['Index',     'Index    Designation   ',			$
	       'Index    Designation   ']]
    select_data = text_table(tran_info, colhead=titles, /noindex)
    coltitles = select_data(0:1)
    select_data = select_data(2:*)
    if procval.istrn lt procval.itran then begin
	index2val = procval.istrn
    endif else begin
	index2val = 0
    endelse
    index2title = '-------------------- Transitions ---------------------'
    index2id = cw_single_sel(tranbase, select_data, value=index2val,	$
			     title=index2title, font=font_small,	$
			     big_font=large_font, coltitles=coltitles,	$
			     ysize=(y_size-3))

		;********************************************
		;**** Add toggle for remaining two bases ****
		;********************************************

    lowbase = widget_base(topbase, /row, /frame)
    lowbase1 = widget_base(lowbase, /column)
    switchbase = widget_base(lowbase1, /row)
    lswitch = widget_label(switchbase, font=large_font, 			$
    value='Select option for display : ')
    selections = ['Settings/scalings(I)','Settings/scalings(II)']
    toggleid = cw_bgroup(switchbase, selections, font=font_small,	$
			 set_value=procval.itoggle1, /exclusive)
    lowbase2 = widget_base(lowbase1)

		;**********************************************
		;**** Add settings and scalings widgets(I) ****
		;**********************************************

    setbase = widget_base(lowbase2, /column, /frame)
    sethead = widget_label(setbase, font=large_font,			$
    value='Please enter settings and scalings (I):-')
    setbase2 = widget_base(setbase, /column)
    label1 = 'Transition energy    - Z-spline independent variable : '
    label2 = 'Einstein coefficient - Z-spline independent variable : '
    label3 = 'Einstein coefficient - Transition type               : '
    label4 = 'Einstein coefficient - Z1 power for scaling factor   : '
    label5 = 'Upsilon coefficient  - Z-spline independent variable : '
    label6 = 'Upsilon coefficient  - Z1 power for scaling factor   : '
    label7 = 'Use bad point option - Linear interpolation          : '
    setbase2a = widget_base(setbase2, /row)
    setbase2b = widget_base(setbase2, /row)
    setbase2c = widget_base(setbase2, /row)
    setbase2d = widget_base(setbase2, /row)
    zspline=[' Z1 ','1/Z1']
    lab1 = widget_label(setbase2a, font=font_small, value=label1)
    iec1aid = cw_bselector(setbase2a, zspline, font=font_small, 	$
    set_value=procval.iec1a(procval.istrn))
    lab2 = widget_label(setbase2b, font=font_small, value=label2)
    iac1aid = cw_bselector(setbase2b, zspline, font=font_small,         $
    set_value=procval.iac1a(procval.istrn))
    transitions = ['Dipole','Non-dipole','Spin change','Other']
    lab3 = widget_label(setbase2c, font=font_small, value=label3)
    iac2aid = cw_bselector(setbase2c, transitions, font=font_small,    	$
    set_value=procval.iac2a(procval.istrn))
    lab4val = string(procval.fac2a(procval.istrn), format='(f6.2)')
    lab4 = widget_label(setbase2d, font=font_small, value=label4)
    fac2aid = widget_text(setbase2d, font=font_small, xsize=7, 		$
    value=lab4val, /editable)

                ;***************************************************
                ;**** Map or unmap base according to the toggle ****
                ;***************************************************

    if procval.itoggle1 ne 0 then begin
        widget_control, setbase, map=0
    endif

		;***********************************************
		;**** Add settings and scalings widgets(II) ****
		;***********************************************

    setbase3 = widget_base(lowbase2, /column, /frame)
    sethead3 = widget_label(setbase3, font=large_font,			$
    value='Please enter settings and scalings (II):-')
    setbase3a = widget_base(setbase3, /row)
    setbase3b = widget_base(setbase3, /row)
    setbase3c = widget_base(setbase3, /row)
    lab5 = widget_label(setbase3a, font=font_small, value=label5)
    igc1aid = cw_bselector(setbase3a, zspline, font=font_small, 	$
    set_value=procval.igc1a(procval.istrn))
    lab6 = widget_label(setbase3b, font=font_small, value=label6)
    lab6val = string(procval.fgc2a(procval.istrn), format='(f6.2)')
    fgc2aid = widget_text(setbase3b, font=font_small, xsize=7, 		$
    value=lab6val, /editable)
    lab7 = widget_label(setbase3c, font=font_small, value=label7)
    libptid = cw_bselector(setbase3c, ['Yes','No'], font=font_small,	$
    set_value=procval.lbpts(procval.istrn))

                ;***************************************************
                ;**** Map or unmap base according to the toggle ****
                ;***************************************************

    if procval.itoggle1 ne 1 then begin
        widget_control, setbase3, map=0
    endif
		;************************************************
		;**** Add output electron temperatures table ****
		;************************************************

    tembase = widget_base(lowbase, /column, /frame)

		;****************************************************
                ;**** Set up the data for the table              ****
                ;**** col 1 has user temperature values          ****
                ;**** col 2-4 have temperature values from files ****
                ;**** which can have one of three possible units ****
                ;****************************************************

    tabledata = strarr(4, procval.ndtin)
    tabledata(0,*) = strtrim(string(procval.tine(*), format=num_form),2)
    if procval.istrn lt procval.itran then userindex=procval.istrn else	$
    userindex=0
    istrn = userindex
    ntval = procval.nva(istrn)
    tvals = procval.scefa(*,istrn)			;Reduced temps.
    int1 = 1
    int2 = 2
    int3 = 3
    iz1 = procval.iz0 - procval.izs + 1
    xxtcon, int3, int1, iz1, ntval, tvals, tkelvals	;Kelvin temps.
    xxtcon, int3, int2, iz1, ntval, tvals, tevvals	;eV temps.
    if ntval gt 0 then begin
        tabledata(3,0:ntval-1 ) =                                       $
        strtrim(string(tvals(0:ntval-1),format=num_form),2)
        tabledata(1,0:ntval-1) =                                        $
        strtrim(string(tkelvals(0:ntval-1),format=num_form),2)
        tabledata(2,0:ntval-1) =                                        $
        strtrim(string(tevvals(0:ntval-1),format=num_form),2)
    endif

		;****************************************
                ;**** fill rest of table with blanks ****
		;****************************************

    blanks = where(tabledata eq 0.0)
    if (blanks(0) ge 0) then begin
        tabledata(blanks) = ' '
    endif

		;**************************
		;**** The actual table ****
		;**************************

    colhead = [['Output values','Input values'], ['','']]
    units = procval.ifout - 1
    unitname = ['Kelvin','eV','Reduced']
    unitind = [[0,0,0],[1,2,3]]
    table_title =  ["Output Electron Temperatures "]
    tempid = cw_adas_table(tembase, tabledata, units, unitname, unitind,$
                           UNITSTITLE='Temperature Units',            	$
                           COLEDIT=[1,0], COLHEAD=colhead,          	$
			   TITLE=table_title, ORDER=[1,0],          	$
                           LIMITS=[1,1], CELLSIZE=12,               	$
                           /SCROLL, NUM_FORM=num_form,  		$
                           FONTS=edit_fonts, FONT_LARGE=large_font, 	$
                           FONT_SMALL=font_small, YTEXSIZE=y_size)

                ;************************************
                ;**** Default and clear buttons  ****
                ;************************************

    tembase2 = widget_base(tembase, /row)
    deftid = widget_button(tembase2, font=large_font,			$
                           value='Default Temperatures')
    clearid = widget_button(tembase2, font=large_font,              	$
                           value='Clear Table')

                ;***********************
                ;**** Error message ****
                ;***********************

    messid = widget_label(parent, font=large_font, value='Edit the '+   $
                          'processing options data and press Done '+    $
                          'to proceed')

                ;******************************
                ;**** Add the exit buttons ****
                ;******************************

    base = widget_base(parent, /row)
    menufile = bitfile + '/menu.bmp'
    read_X11_bitmap, menufile, bitmap1
    outid = widget_button(base, value=bitmap1)          ;menu button
    cancelid = widget_button(base, value='Cancel', font=large_font)
    doneid = widget_button(base, value='Done', font=large_font)

                ;*************************************************
                ;**** Create a state structure for the pop-up ****
                ;****                window.                  ****
                ;*************************************************

    new_state = {       messid          :       messid,                 $
                        outid           :       outid,                  $
                        cancelid        :       cancelid,               $
                        doneid          :       doneid,			$
			runid		:	runid,			$
			tempid		:	tempid,			$
			toggleid	:	toggleid,		$
			deftid		:	deftid,			$
			clearid		:	clearid,		$
			index1id	:	index1id,		$
			index2id	:	index2id,		$
			setbase		:	setbase,		$
			setbase3	:	setbase3,		$
			tembase		:	tembase,		$
			iec1aid		:	iec1aid,		$
			iac1aid		:	iac1aid,		$
			iac2aid		:	iac2aid,		$
			fac2aid		:	fac2aid,		$
			igc1aid		:	igc1aid,		$
			fgc2aid		:	fgc2aid,		$
			libptid		:	libptid,		$
			font_large	:	large_font,		$
			font_small	:	font_small,		$
			num_form	:	num_form		}

                ;**************************************
                ;**** Save initial state structure ****
                ;**************************************

    widget_control, first_child, set_uvalue=new_state, /no_copy

    RETURN, parent

END
