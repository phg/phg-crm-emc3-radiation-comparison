; Copyright (c) 1996, Strathclyde University.
; SCCS info: Module @(#)$Header: /home/adascvs/idl/adas2xx/adas203/adas203_2_plot.pro,v 1.2 2004/07/06 10:12:16 whitefor Exp $ Date $Date: 2004/07/06 10:12:16 $
;+
; PROJECT:
;       ADAS IBM MVS to UNIX conversion
;
; NAME:
;       ADAS203_2_PLOT
;
; PURPOSE:
;       Generates ADAS203 Scaled A-value graphical output
;
; EXPLANATION:
;       This routine creates a window for the display of graphical
;       output. A separate routine PLOT203_2 actually plots the
;       graph.
;
; USE:
;       This routine is specific to the Scaled A-value plotting section
;	of ADAS203.
;
; INPUTS:
;       DSFULL  - String; Name of data file
;
;       UTITLE  - String; Optional comment by user added to graph title.
;
;       LDEF2   - Integer; 1 - use user entered graph scales
;                          0 - use default axes scaling
;
;	X	- Fltarr; ion charges(+1) - x-values
;
;	Y	- Fltarr; transition wave nos. - y-values
;
;       XO      - Fltarr; x-value of interpolation point
;
;       Z       - Fltarr; y-value of interpolation point
;
;	SPECIES - String; element name - used as part of header
;
;	SEQ	- String; isoelectronic sequence symbol + selected transition
;		          used as part of header
;
;       HRDOUT 	- Integer; 1 if hardcopy output activated, 0 if not.
;
;       HARDNAME- String; Filename for harcopy output.
;
;       DEVICE  - String; IDL name of hardcopy output device.
;	
;	NION	- Integer; number of ions in the input file.
;
;       HEADER  - String; ADAS version number header to include in graph.
;
;       XMIN2   - String; Lower limit for x-axis of graph, number as string.
;
;       XMAX2   - String; Upper limit for x-axis of graph, number as string.
;
;       YMIN2   - String; Lower limit for y-axis of graph, number as string.
;
;       YMAX2   - String; Upper limit for y-axis of graph, number as string.
;
;	BITFILE - String; the path to the directory containing bitmaps
;                 for the 'escape to series menu' button.
;
;	NUC	- Intarr; element nuclear charges
;
;	RECD	- Intarr; recombined ion charges
;
;	ELEM	- Strarr; the element symbols from the input file
;
; OPTIONAL INPUTS:
;       None.
;
; OUTPUTS:
;       GOMENU  - Int; flag - set to 1 if user has selected 'escape direct
;                 to series menu', otherwise 0
;
; OPTIONAL OUTPUTS:
;       None.
;
; KEYWORD PARAMETERS:
;       FONT    - String; The name of a font to use for text in the
;                 graphical output widget.
;
; CALLS:
;       CW_ADAS_GRAPH   Graphical output widget.
;       PLOT203_2       Make one plot to an output device for 203
;       XMANAGER
;
; SIDE EFFECTS:
;       This routine uses a common block to maintain its state 
;	PLOT203_2_BLK.
;
;       One other routine is included in this file;
;       ADAS203_2_PLOT_EV Called via XMANAGER during widget management.
;
; CATEGORY:
;       Adas system.
;
; WRITTEN:
;       Tim Hammond, Tessella Support Services plc, 28th March 1996
;
; MODIFIED:
;       1.1     Tim Hammond
;               First version
;	1.2     William Osborn
;               Added check for new environment variable VERY_SMALL
;               If this is set to the string "YES" (preferably in the
;               .adas_setup file) then the font size used on the graphs
;               is reduced and the positioning of labels adjusted
;               slightly to make things fit on a very small screen.
;
; VERSION:
;       1.1     01-04-96
;	1.2	11-10-96
;
;-
;----------------------------------------------------------------------------

PRO adas203_2_plot_ev, event

    COMMON plot203_2_blk, action, plotdev, plotfile, fileopen, win, 	$
                          data, gomenu

    newplot = 0
    print = 0
    done = 0
                ;****************************************
                ;**** Set graph and device requested ****
                ;****************************************

    CASE event.action OF

        'print'    : begin
            newplot = 1
            print = 1
        end

        'done'     : begin
            if fileopen eq 1 then begin
                set_plot, plotdev
                device, /close_file
            end
            set_plot,'X'
            widget_control, event.top, /destroy
            done = 1
        end

	'bitbutton' : begin
            if fileopen eq 1 then begin
                set_plot, plotdev
                device, /close_file
            end
            set_plot,'X'
            widget_control, event.top, /destroy
            done = 1
            gomenu = 1
        end

    END

                ;*******************************
                ;**** Make requested plot/s ****
                ;*******************************

    if done eq 0 then begin

                ;**** Set graphics device ****

        if print eq 1 then begin
            set_plot, plotdev
            if fileopen eq 0 then begin
                fileopen = 1
                device, filename=plotfile
                device, /landscape
            endif
        endif else begin
            set_plot,'X'
            wset, win
        endelse

                ;**** Draw graphics ****

        if newplot eq 1 then begin
            plot203_2, data.ldef2, data.x, data.y, data.xo, data.z,	$
		       data.title, data.rightstring, 			$
		       data.rightstring2, data.rightstring3,		$
		       data.rightstring4, data.xmin2, data.xmax2, 	$
		       data.ymin2, data.ymax2
	    if print eq 1 then begin
                message = 'Plot  written to print file.'
                grval = {WIN:0, MESSAGE:message}
                widget_control, event.id, set_value=grval
            endif
        endif
    endif

END

;----------------------------------------------------------------------------
                                                                
PRO adas203_2_plot, dsfull, utitle, ldef2, x, y, xo, z, species, seq,	$
                    hrdout, hardname, device, header, nion,   		$
                    xmin2, xmax2, ymin2, ymax2, nuc, recd, elem,	$
                    bitfile, gomenu, FONT=font

    COMMON plot203_2_blk, action, plotdev, plotfile, fileopen, win, 	$
                          data, gomenucom

                ;**** Copy input values to common ****

    plotdev = device
    plotfile = hardname
    fileopen = 0
    gomenucom = gomenu

                ;************************************
                ;**** Create general graph titles****
                ;************************************

    title = "SCALED A-VALUE vs ION CHARGE+1 "
    if ( strtrim(strcompress(utitle),2)  ne ' ' ) then begin
        title = title + ': ' + strupcase(strtrim(utitle,2))
    endif
    small_check = GETENV('VERY_SMALL')
    if small_check eq 'YES' then begin
        title =  title + '!C!CADAS    : ' + strupcase(header)
        species = strcompress(species, /remove_all)
        title =  title + '!C!CFILE     : ' + strcompress(dsfull) +	$
          '   SPECIES: ' + strupcase(species) + '   SEQ: ' + seq 
        title =  title + '!C!CKEY     :  (CROSS - INTERPOL.) (DASH LINE - SOURCE)'
    endif else begin
        title =  title + '!C!CADAS    : ' + strupcase(header)
        species = strcompress(species, /remove_all)
        title =  title + '!CFILE     : ' + strcompress(dsfull) +	$
          '   SPECIES: ' + strupcase(species) + '   SEQ: ' + seq 
        title =  title + '!CKEY     :  (CROSS - INTERPOL.) (DASH LINE - SOURCE)'
    endelse
    rightstring = ''
    rightstring2 = ''
    rightstring3 = ''
    rightstring4 = ''
    for i=0, (nion-1) do begin
        if (i+1) lt 10 then rightstring = rightstring + ' '
        rightstring = rightstring + strtrim(string(i+1),2) + '!C'
	if nuc(i) lt 10 then rightstring2 = rightstring2 + ' '
        rightstring2 = rightstring2 + strtrim(string(nuc(i)),2) + '!C'
	if recd(i) lt 10 then rightstring3 = rightstring3 + ' '
        rightstring3 = rightstring3 + strtrim(string(recd(i)),2) + '!C'
	rightstring4 = rightstring4 + 					$
		       strcompress(elem(i), /remove_all) + '!C'
    endfor
    rightstring = rightstring + '!C *!C'
    nucval = ''
    if nuc(nion) lt 10 then nucval = nucval + ' '
    nucval = nucval + strtrim(string(nuc(nion)),2)
    rightstring2 = rightstring2 + '!C' + nucval
    recdval = ''
    if recd(nion) lt 10 then recdval = recdval + ' '
    recdval = recdval + strtrim(string(recd(nion)),2)
    rightstring3 = rightstring3 + '!C' + recdval
    rightstring4 = rightstring4 + '!C' + strcompress(elem(nion), /remove_all)
    rightstring = rightstring + '!C* => ION SELECTED FOR INTERPOLATION'

                ;*************************************
                ;**** Create graph display widget ****
                ;*************************************

    graphid = widget_base(TITLE='ADAS203 GRAPHICAL OUTPUT', 		$
                          XOFFSET=1,YOFFSET=1)
    device, get_screen_size = scrsz
    xwidth=scrsz(0)*0.75
    yheight=scrsz(1)*0.75
    bitval = bitfile + '/menu.bmp'
    cwid = cw_adas_graph(graphid, print=hrdout, FONT=font,		$
                         xsize=xwidth, ysize=yheight, 			$
			 bitbutton=bitval)

                ;**** Realize the new widget ****

    widget_control, graphid, /realize

                ;**** Get the id of the graphics area ****

    widget_control, cwid, get_value=grval
    win = grval.win

                ;*******************************************
                ;**** Put the graphing data into common ****
                ;*******************************************

    data = {	X		:	x,				$
		Y		:	y,				$
		XO		:	xo,				$
		Z		:	z,				$
		XMIN2		:	xmin2,				$
		XMAX2		:	xmax2,				$
		YMIN2		:	ymin2,				$
		YMAX2		:	ymax2,				$
 		RIGHTSTRING	:	rightstring,			$
 		RIGHTSTRING2	:	rightstring2,			$
 		RIGHTSTRING3	:	rightstring3,			$
 		RIGHTSTRING4	:	rightstring4,			$
 		TITLE		:	title,				$
		LDEF2		:	ldef2				}

                ;**** Set plotting window ****

    wset, win

    plot203_2, ldef2, x, y, xo, z, title, rightstring, rightstring2,   	$
               rightstring3, rightstring4, xmin2, xmax2, ymin2, ymax2

                ;***************************
                ;**** make widget modal ****
                ;***************************

    xmanager, 'adas203_2_plot', graphid, /modal, /just_reg,		$
              event_handler='adas203_2_plot_ev'

    gomenu = gomenucom

END
