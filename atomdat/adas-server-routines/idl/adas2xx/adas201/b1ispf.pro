; Copyright (c) 1995, Strathclyde University 
; SCCS INFO : Module @(#)$Header: /home/adascvs/idl/adas2xx/adas201/b1ispf.pro,v 1.3 2004/07/06 11:17:58 whitefor Exp $	Date $Date: 2004/07/06 11:17:58 $
;
;+
; PROJECT:
;       ADAS IBM MVS to UNIX conversion
;
; NAME:
;	B1ISPF
;
; PURPOSE:
;	IDL user interface and communications with ADAS201 FORTRAN
;	process via pipe.
;
; EXPLANATION:
;	The routine begins by reading information from the ADAS201
;	FORTRAN process via a UNIX pipe.  Then part of the ADAS201
;	IDL user interface is invoked to determine how the user
;	wishes to process the input dataset.  When the user's
;	interactions are complete the information gathered with
;	the user interface is written back to the FORTRAN process
;	via the pipe.  Communications are to the FORTRAN subroutine
;	B1ISPF.
;
; USE:
;	The use of this routine is specific to ADAS201, see adas201.pro.
;
; INPUTS:
;	PIPE	- The IDL unit number of the bi-directional pipe to the
;		  ADAS201 FORTRAN process.
;
;	LPEND	- Integer, 0 if the user pressed the 'Done' button
;		  or 1 if the user pressed 'Cancel'.  Maps directly onto
;		  the logical variable LPEND in ADAS201 FORTRAN.
;
;	PROCVAL	- A structure which determines the initial settings of
;		  the processing options widget.  The initial value is
;		  set in adas201.pro.  If adas201.pro passes a blank 
;		  dummy structure of the form {NEW:-1} into PROCVAL then
;		  PROCVAL is reset to a default structure.
;
;		  The PROCVAL structure is;
;			procval = {                        	$
;                			new   : 0 ,             $
;                			title : '',             $
;                			ifout : 1,              $
;					ldfit : 0,              $
;					maxt  : 0, 		$
;                			tine  : temp_arr,       $
;                			lfsel : 0,              $
;                			tolval: 5,              $
;					losel : 0		$
;              			}
;
;
;		  See cw_adas201_proc.pro for a full description of this
;		  structure.
;	
;
;	STRGA 	- String array; Designations for levels
;
;	DSFULL	- String; The full system file name of the input 
;		  dataset selected by the user for processing.
;
;       BITFILE - String; the path to the dirctory containing bitmaps
;                 for the 'escape to series menu' button.
;
; OPTIONAL INPUTS:
;	None
;
; OUTPUTS:
;	PROCVAL	- On output the structure records the final settings of
;		  the processing selections widget if the user pressed the
;		  'Done' button, otherwise it is not changed from input.
;
;       GOMENU  - Int; flag - set to 1 if user has selected 'escape direct
;                 to series menu , otherwise 0
;
; OPTIONAL OUTPUTS:
;	None
;
; KEYWORD PARAMETERS:
;	FONT_LARGE - The name of a larger font.
;
;	FONT_SMALL - The name of a smaller font.
;
;	EDIT_FONTS - A structure of two fonts used in the table editor
;		     adas_edtab.pro, {font_norm:'',font_input:''}.
;
; CALLS:
;	ADAS201_PROC	Invoke the IDL interface for ADAS201 data
;		 	processing options/input.
;
; SIDE EFFECTS:
;	Two way communications with ADAS201 FORTRAN via the
;	bi-directional UNIX pipe.
;
; CATEGORY:
;	Adas system.
;
; WRITTEN:
;       Lalit Jalota, Tessella Support Services plc, 8-Mar-1995
;
; MODIFIED:
;	1.1	Lalit Jalota
;		First Release
;	1.2	Tim Hammond
;		Added further dummy variables when reading from
;		pipe.
;	1.3	William Osborn
;		Added menu button code
; VERSION:
;	1.1	08-03-95
;	1.2	06-07-95
;	1.3	04-06-96
;-
;-----------------------------------------------------------------------------


PRO b1ispf, pipe, lpend, procval, strga, dsfull, bitfile, gomenu,	$
		FONT_LARGE=font_large, FONT_SMALL=font_small, $
		EDIT_FONTS=edit_fonts


                ;**** Set defaults for keywords ****
  IF NOT (KEYWORD_SET(font_large)) THEN font_large = ''
  IF NOT (KEYWORD_SET(font_small)) THEN font_small = ''
  IF NOT (KEYWORD_SET(edit_fonts)) THEN $
                                edit_fonts = {font_norm:'',font_input:''}

		;********************************************
		;****     Declare variables for input    ****
		;**** arrays wil be declared after sizes ****
		;**** have been read.                    ****
                ;********************************************
  lpend = 0
  ndtin = 0
  ndtem = 0
  il = 0
  nv = 0
  itran = 0
  input = 0

		;********************************
		;**** Read data from fortran ****
		;********************************
  readf, pipe, input
  ndtin = input
  readf, pipe, input
  il = input
  readf, pipe, input
  ndtem = input
  readf, pipe, input
  nv = input
  readf, pipe, input
  itran = input

		;******************************************
		;**** Now can define other array sizes ****
		;******************************************
  tscef = dblarr(3,ndtem)
  i1a = intarr(itran)
  i2a = intarr(itran)

		;****************************************************
		;*** Define some variables used to read arrays in ***
		;****************************************************
  temp = 0.0D
  itemp = 0
  
		;********************************
                ;**** Read data from fortran ****
                ;********************************

  for i = 0, 2 do begin 
     for j = 0, ndtem-1 do begin
        readf, pipe, temp
        tscef(i,j) = temp
     endfor
  endfor	

  for i = 0, itran-1 do begin
     readf, pipe, itemp
     i1a(i)  = itemp
  endfor

  for i = 0, itran-1 do begin
     readf, pipe, itemp
     i2a(i) = itemp
  endfor

		;*******************************************
		;**** Set default value if non provided ****
		;*******************************************
  if (procval.new lt 0) then begin
     procval = {			$
		new   : 0 ,             $
		title : '',		$
		istrn : 0 ,     	$
		maxt  : nv,             $
                ifout : 1, 		$
		tine  : tscef(0,*),     $
		lfsel : 0,		$
		tolval: 5,              $
		losel : 0		$
	      }
  end

		;****************************************
		;**** Pop-up processing input widget ****
		;****************************************
  adas201_proc, procval, strga, dsfull,  $
		ndtin, il, ndtem, nv, itran, $ 
		tscef, i1a, i2a, action, bitfile, $
		FONT_LARGE=font_large, FONT_SMALL=font_small, $
		EDIT_FONTS=edit_fonts

		;**********************************************
		;****  Act on the event from the widget    ****
		;**** There are only three possible events ****
		;**** 'Done', 'Cancel' and 'Menu'.         ****
		;**********************************************
  gomenu = 0
  if action eq 'Done' then begin
    lpend = 0
  endif else if action eq 'Menu' then begin
    lpend = 1
    gomenu = 1
  endif else begin
    lpend = 1
  end

		;*******************************
		;**** Write data to fortran ****
		;*******************************
  printf,pipe,lpend   
  printf,pipe,procval.title,format='(a40)' 
  printf,pipe,procval.istrn
  printf,pipe,procval.ifout
  printf,pipe,procval.maxt   
  for i = 0, procval.maxt-1 do begin
     printf, pipe, procval.tine(i)
  endfor
  printf,pipe,procval.lfsel
  if (procval.lfsel eq 1) then begin
     printf,pipe,procval.tolval
  endif
  printf, pipe, procval.losel

END
