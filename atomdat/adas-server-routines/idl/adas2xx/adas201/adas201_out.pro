; Copyright (c) 1995, Strathclyde University.
; SCCS info: Module @(#)$Header: /home/adascvs/idl/adas2xx/adas201/adas201_out.pro,v 1.3 2004/07/06 10:11:15 whitefor Exp $	Date $Date: 2004/07/06 10:11:15 $
;
;+
; PROJECT:
;       ADAS IBM MVS to UNIX conversion
;
; NAME:
;	ADAS201_OUT
;
; PURPOSE:
;	IDL ADAS user interface, output options.
;
; EXPLANATION:
;	This routine creates and manages a pop-up window which allows
;	the user to select options and input data to control ADAS201
;	graphical and file output.
;
; USE:
;	This routine is ADAS201 specific, see b1spf1.pro for an example
;	of how it is used.
;
; INPUTS:
;	VAL	- A structure which determines the initial settings of
;		  the output options widget.  The value is passed
;		  unmodified into cw_adas201_out.pro.
;
;		  See cw_adas201_out.pro for a full description of this
;		  structure.
;
;	DSFULL	- String; The full system file name of the input 
;		  dataset selected by the user for processing.
;
;       BITFILE - String; the path to the dirctory containing bitmaps
;                 for the 'escape to series menu' button.
;
; OPTIONAL INPUTS:
;	None.
;
; OUTPUTS:
;	ACT	- String; Either 'Done' or 'Cancel' for the button the
;		  user pressed to terminate the output options window.
;
; OPTIONAL OUTPUTS:
;	None.
;
; KEYWORD PARAMETERS:
;	DEVLIST	- A string array;  Each element of the array is the
;		  name of a hardcopy graphics output device.  This
;		  list must mirror the DEVCODE list.  DEVLIST is the
;		  list which the user will see.
;		  e.g ['Post-Script','HP-PCL','HP-GL']
;
;	FONT	- Supplies the font to be used for the interface widgets.
;
; CALLS:
;	CW_ADAS201_OUT	Creates the output options widget.
;	XMANAGER
;	See side effects for widget management routine.
;
; SIDE EFFECTS:
;	This routine uses a common block OUT201_BLK to maintain its state.
;	ADAS201_OUT_EV	is included in this file and is called
;			indirectly from XMANAGER during widget
;			management.
;
; CATEGORY:
;	Widgets
;
; WRITTEN:
;       Lalit Jalota, Tessella Support Services plc, 8-Mar-1995 
;
; MODIFIED:
;	1.1	Lalit Jalota
;		First Release
;
;	1.3	William Osborn
;		Added menu button code and dynlabel procedure
; VERSION:
;	1.1	21-05-95
;
;	1.3	04-06-96
;-
;-----------------------------------------------------------------------------


pro adas201_out_ev, event

  common out201_blk,action,value
	

		;**** Find the event type and copy to common ****
    action = event.action

    case action of

		;**** 'Done' button ****
	'Done'  : begin

			;**** Get the output widget value ****
     		     child = widget_info(event.id,/child)
		     widget_control,child,get_value=value 

			;**** Kill the widget to allow IDL to ****
			;**** continue and interface with     ****
			;**** FORTRAN only if there is work   ****
			;**** for the FORTRAN to do.          ****
		     if (value.grpout eq 1) or (value.texout eq 1) then begin
		        widget_control,event.top,/destroy
		     endif 
	          end


		;**** 'Cancel' button ****
	'Cancel': widget_control,event.top,/destroy

		;**** 'Menu' button ****
	'Menu': widget_control,event.top,/destroy

    endcase

end

;-----------------------------------------------------------------------------


pro adas201_out, val, dsfull, act, bitfile, devlist=devlist, font=font

  common out201_blk,action,value

		;**** Copy value to common ****
  value = val

		;**** Set defaults for keywords ****
  if not (keyword_set(font)) then font = ''
  if not (keyword_set(devlist)) then devlist = ''

		;***************************************
		;**** Pop-up a new widget           ****
		;***************************************

                ;**** create base widget ****
  outid = widget_base(title='ADAS201 OUTPUT OPTIONS',xoffset=100,yoffset=0)

		;**** Declare output options widget ****
  cwid = cw_adas201_out(outid, dsfull, bitfile, value=value,  $
			devlist=devlist, font=font )

		;**** Realize the new widget ****
  dynlabel, outid
  widget_control,outid,/realize

		;**** make widget modal ****
  xmanager,'adas201_out',outid,event_handler='adas201_out_ev',/modal,/just_reg
 
		;**** Return the output value from common ****
  act = action
  val = value

END

