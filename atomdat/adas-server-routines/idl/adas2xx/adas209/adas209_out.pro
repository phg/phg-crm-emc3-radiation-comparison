;+
; Copyright (c) 1995, Strathclyde University.
; SCCS info: Module @(#)$Header: /home/adascvs/idl/adas2xx/adas209/adas209_out.pro,v 1.4 2004/07/06 10:21:45 whitefor Exp $ Date $Date: 2004/07/06 10:21:45 $
;
; PROJECT:
;       ADAS IBM MVS to UNIX conversion
;
; NAME:
;	ADAS209_OUT
;
; PURPOSE:
;	IDL ADAS user interface, output options.
;
; EXPLANATION:
;	This routine creates and manages a pop-up window which allows
;	the user to select options and input data to control ADAS209
;	graphical and file output.
;
; USE:
;	This routine is ADAS209 specific, see b9spf1.pro for an example
;	of how it is used.
;
; INPUTS:
;	VAL	- A structure which determines the initial settings of
;		  the output options widget.  The value is passed
;		  unmodified into cw_adas209_out.pro.
;
;		  See cw_adas291_out.pro for a full description of this
;		  structure.
;
;	DSFULL	- String; The full system file name of the input 
;		  dataset selected by the user for processing.
;
; OPTIONAL INPUTS:
;	None.
;
; OUTPUTS:
;	ACT	- String; Either 'Done' or 'Cancel' for the button the
;		  user pressed to terminate the output options window.
;
; OPTIONAL OUTPUTS:
;	None.
;
; KEYWORD PARAMETERS:
;	DEVLIST	- A string array;  Each element of the array is the
;		  name of a hardcopy graphics output device.  This
;		  list must mirror the DEVCODE list.  DEVLIST is the
;		  list which the user will see.
;		  e.g ['Post-Script','HP-PCL','HP-GL']
;
;	FONT	- Supplies the font to be used for the interface widgets.
;
; CALLS:
;	CW_ADAS209_OUT	Creates the output options widget.
;	DYNLABEL	Sets dynamic_resize keyword for label widgets.
;	XMANAGER
;	See side effects for widget management routine.
;
; SIDE EFFECTS:
;	This routine uses a common block OUT209_BLK to maintain its state.
;	ADAS209_OUT_EV	is included in this file and is called
;			indirectly from XMANAGER during widget
;			management.
;
; CATEGORY:
;	Widgets
;
; WRITTEN:
;       David H.Brooks, Univ.of Strathclyde, 10-May-1995
;       Altered naming conventions to be consistent with adas209, also
;       removed references to graphical output which are not required
;       for adas209.
;
; VERSION:
;       1.1	David H. Brooks					10-05-95
;	1.2	Tim Hammond (Tessella Support Services plc.)	27-06-95
;	1.3	Tim Hammond					27-06-95
;	1.4    	William Osborn					09-07-96
;		
; MODIFIED:
;	1.1	First release
;	1.2	Tidied code up
;	1.3	Corrected syntax error in comments
;	1.4	Added dynlabel procedure
;-
;-----------------------------------------------------------------------------


pro adas209_out_ev, event

    COMMON out209_blk, action, value
	

		;**** Find the event type and copy to common ****

    action = event.action

    case action of

		  ;**** 'Done' button ****

	'Done'  : begin

	          ;**** Get the output widget value ****

  		  child = widget_info(event.id,/child)
		  widget_control,child,get_value=value 

		  ;*****************************************
		  ;**** Kill the widget to allow IDL to ****
		  ;**** continue and interface with     ****
		  ;**** FORTRAN only if there is work   ****
		  ;**** for the FORTRAN to do.          ****
		  ;*****************************************

                  if value.texout eq 1 then begin
		      widget_control, event.top, /destroy
		  endif 
	          end


		  ;**** 'Cancel' button ****

	'Cancel': widget_control, event.top, /destroy

    endcase

end

;-----------------------------------------------------------------------------


pro adas209_out, val, dsfull, act, devlist=devlist, font=font

    COMMON out209_blk, action, value

		;**** Copy value to common ****

    value = val

		;**** Set defaults for keywords ****

    if not (keyword_set(font)) then font = ''
    if not (keyword_set(devlist)) then devlist = ''

		;***************************************
		;**** Pop-up a new widget           ****
		;***************************************

                ;**** create base widget ****

    outid = widget_base(title='ADAS209 OUTPUT OPTIONS',xoffset=200,yoffset=100)

		;**** Declare output options widget ****

    cwid = cw_adas209_out(outid, dsfull, value=value,  			$
			  devlist=devlist, font=font )

		;**** Realize the new widget ****

    dynlabel, outid
    widget_control, outid, /realize

		;**** make widget modal ****

    xmanager, 'adas209_out', outid, event_handler='adas209_out_ev',	$
              /modal, /just_reg
 
		;**** Return the output value from common ****

    act = action
    val = value

END

