; Copyright (c) 1995, Strathclyde University.
; SCCS info: Module @(#)$Header: /home/adascvs/idl/adas2xx/adas209/cw_adas209_out.pro,v 1.4 2004/07/06 12:34:14 whitefor Exp $ Date $Date: 2004/07/06 12:34:14 $
;+
; PROJECT:
;       ADAS IBM MVS to UNIX conversion
;
; NAME:
;	CW_ADAS209_OUT()
;
; PURPOSE:
;	Produces a widget for ADAS209 output options.
;
; EXPLANATION:
;	This function declares a compound widget consisting of the
;	output file widget cw_adas_outfile.pro.  The text output
;	file specified in this widget is for tabular (paper.txt)
;	output.  This widget also includes a button for browsing the comments
;       from the input dataset, a 'Cancel' button and a 'Done' button.
;	The compound widget cw_adas_outfile.pro included in this file
;       is self managing. This widget only handles events from the 'Done'
;       and 'Cancel' buttons.
;
;	This widget only generates events for the 'Done' and 'Cancel'
;	buttons.
;
; USE:
;	This widget is specific to ADAS209, see adas209_out.pro	for use.
;
; INPUTS:
;	PARENT	- Long integer; ID of parent widget.
;
;	DSFULL	- Name of input dataset for this application.
;
; OPTIONAL INPUTS:
;	None.
;
; OUTPUTS:
;	The return value of this function is the ID of the declared widget.
;
; OPTIONAL OUTPUTS:
;	None.
;
; KEYWORD PARAMETERS:
;	DEVLIST	- A string array;  Each element of the array is the
;		  name of a hardcopy graphics output device.  This
;		  list must mirror the DEVCODE list.  DEVLIST is the
;		  list which the user will see.
;		  e.g ['Post-Script','HP-PCL','HP-GL']
;
;	VALUE	- A structure which determines the initial settings of
;		  the output options widget.  The structure is made
;		  up of two parts.  Each part is the same as the value
;		  structure of one of the two main compound widgets
;		  included in this widget.  See cw_adas_outfile 
;		  for more details.  The default value is;
;
;                     { $
;			TEXOUT:0, TEXAPP:-1, $
;			TEXREP:0, TEXDSN:'', $
;			TEXDEF:'',TEXMES:'', $
;		      }
;
;		  For CW_ADAS_OUTFILE tabular output (paper.txt);
;			TEXOUT	Integer; Activation button 1 on, 0 off
;			TEXAPP	Integer; Append button 1 on, 0 off, -1 no button
;			TEXREP	Integer; Replace button 1 on, 0 off, -1
;                                        no button
;			TEXDSN	String; Output file name
;			TEXDEF	String; Default file name
;			TEXMES	String; file name error message
;
;
;	UVALUE	- A user value for this widget.
;
;	FONT	- String; the name of a font to be used for all text in
;		  this widget.  Default to system default font.
;
; CALLS:
;	CW_ADAS_DSBR	Input dataset name and comments browse button.
;	CW_ADAS_OUTFILE	Output file name entry widget.
;	See side effects for other related widget management routines.
;
; SIDE EFFECTS:
;
; 	Two other routines are included in this file
;	OUT209_GET_VAL()
;	OUT209_EVENT()
;
; CATEGORY:
;	Compound Widget
;
; WRITTEN:
;       David H.Brooks, Univ.of Strathclyde, 10-May-1995
;       Altered cw_adas201_out.pro to make cw_adas209_out.pro by
;       removing all references to graphical output, which are not required
;       for adas209, altering the state structure VALUE to make it 
;       compatable with other adas209 procedures, and modifying the widget
;       screen layout.
;
; MODIFIED:
;	1.1	David H. Brooks					10-05-95
;	1.2	Tim Hammond (Tessella Support Services plc)	27-06-95
;	1.3	Tim Hammond					27-06-95
;	1.4	William Osborn					01-08-96
; VERSION:
;       1.1     First release
;	1.2	Tidied code up
;	1.3	Changed label for output file
;	1.4	Removed xsize=n keyword from widget_label command.
;		The dynlabel procedure does not override these
;		assignments and they were taken as being values in pixels
;		on some machines.
;
;-
;-----------------------------------------------------------------------------

FUNCTION out209_get_val, id

                ;**** Return to caller on error ****
    ON_ERROR, 2

                ;**** Retrieve the state ****

    parent=widget_info(id, /parent)
    widget_control, parent, get_uvalue=state, /no_copy

		;**** Get text output settings ****

    widget_control, state.paperid, get_value=papos

    ost = { out209_set, 						$
            TEXOUT:papos.outbut, TEXAPP:papos.outbut, 			$
            TEXREP:papos.repbut  , TEXDSN:papos.filename,		$
            TEXDEF:papos.defname  , TEXMES:papos.message 		$
          }
          

                ;**** Return the state ****

    widget_control, parent, set_uvalue=state, /no_copy

    RETURN, ost 

END

;-----------------------------------------------------------------------------

FUNCTION out209_event, event


                ;**** Base ID of compound widget ****

    parent=event.top

                ;**** Retrieve the state ****

    widget_control, parent, get_uvalue=state, /no_copy

		;*********************************
		;**** Clear previous messages ****
		;*********************************

    widget_control, state.messid, set_value=' '

                ;************************
                ;**** Process events ****
                ;************************

    CASE event.id OF

		;***********************
		;**** Cancel button ****
		;***********************

    state.cancelid: new_event = {ID:parent, TOP:event.top, 		$
				 HANDLER:0L, ACTION:'Cancel'}

		;*********************
		;**** Done button ****
		;*********************

    state.doneid: begin

                ;**** Return the state before checking can start ****
		;**** with the get_value keyword.                ****

          widget_control, parent, set_uvalue=state, /no_copy

		;***************************************
		;**** Check for errors in the input ****
		;***************************************

	  error = 0

		;**** Get a copy of the widget value ****

	  widget_control, event.handler, get_value=ost
	
		;**** Check for widget error messages ****

	  if (ost.texout eq 1 and strtrim(ost.texmes) ne '') then error=1

                ;**** Retrieve the state   ****

          widget_control, parent, get_uvalue=state, /no_copy

	  if (error eq 1) then begin
	      widget_control,state.messid,set_value= 			$
	      '**** Error in output settings ****'
	      new_event = 0L
	  endif else begin
	      new_event = {ID:parent, TOP:event.top, HANDLER:0L, ACTION:'Done'}
	  endelse

        end

    ELSE: new_event = 0L

    ENDCASE

                ;**** Return the state   ****

    widget_control, parent, set_uvalue=state, /no_copy

    RETURN, new_event

END

;-----------------------------------------------------------------------------

FUNCTION cw_adas209_out, parent, dsfull, DEVLIST=devlist, 		$
		         VALUE=value, UVALUE=uvalue, FONT=font

    IF (N_PARAMS() EQ 0) THEN MESSAGE, 'Must specify parent for cw_adas209_out'
    ON_ERROR, 2					;return to caller

		;**** Set defaults for keywords ****

    IF NOT (KEYWORD_SET(devlist)) THEN devlist = ''
    IF NOT (KEYWORD_SET(uvalue)) THEN uvalue = 0
    IF NOT (KEYWORD_SET(font)) THEN font = ''
    IF NOT (KEYWORD_SET(value)) THEN begin
        ost = {out209_set, 						$
   	       TEXOUT:0, TEXAPP:-1, 					$
	       TEXREP:0, TEXDSN:'', 					$
	       TEXDEF:userroot+'/pass/bundle.pass',TEXMES:'' 		$
              }
    END ELSE BEGIN
        ost = {out209_set,						$
               TEXOUT:value.texout,   TEXAPP:value.texapp, 		$
               TEXREP:value.texrep  , TEXDSN:value.texdsn,		$
               TEXDEF:value.texdef  , TEXMES:value.texmes 		$
              }
    END


		;**********************************************
		;**** Create the 209 Output options widget ****
		;**********************************************

		;**** create base widget ****

    cwid = widget_base( parent, UVALUE = uvalue, 			$
			EVENT_FUNC = "out209_event", 			$
			FUNC_GET_VALUE = "out209_get_val", 		$
			/COLUMN)

		;**** Add dataset name and browse button ****

    rc = cw_adas_dsbr(cwid, dsfull, font=font)

		;**** Widget for text output ****

    outfval = { OUTBUT:ost.texout, APPBUT:-1, REPBUT:ost.texrep, 	$
      	        FILENAME:ost.texdsn, DEFNAME:ost.texdef, 		$
                MESSAGE:ost.texmes }
    base = widget_base(cwid, /row, /frame)
    paperid = cw_adas_outfile(base, OUTPUT='File Output', 		$
                              VALUE=outfval, FONT=font)

		;**** Error message ****

    messid = widget_label(cwid,value=' ',font=font)

		;**** add the exit buttons ****

    base = widget_base(cwid, /row)
    cancelid = widget_button(base, value='Cancel', font=font)
    doneid = widget_button(base, value='Done', font=font)
  
		;**** create a state structure for the pop-up ****
		;**** window.                                 ****

    new_state = { PAPERID:paperid,  					$
		  CANCELID:cancelid, DONEID:doneid, MESSID:messid }

                ;**** Save initial state structure ****

    widget_control, parent, set_uvalue=new_state, /no_copy

    RETURN, cwid

END

