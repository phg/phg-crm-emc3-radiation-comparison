; Copyright (c) 1995 Strathclyde University 
; SCCS info: Module @(#)$Header: /home/adascvs/idl/adas2xx/adas209/b9ispf.pro,v 1.9 2004/07/06 11:34:04 whitefor Exp $ Date $Date: 2004/07/06 11:34:04 $
;+
;
; PROJECT:
;         ADAS IBM MVS to UNIX conversion and development
;
; NAME:
;         ADAS209
;
; PURPOSE:
;         IDL interface and communications with ADAS209 FORTRAN
;         process via pipe.
;
; EXPLANATION:
;         This procedure does some preliminary independent checking of
;         array dimension and gathering of data from the input dataset.
;         It then reads information from the ADAS209 FORTRAN process 
;         via a bi-directional UNIX pipe. Then, the IDL user interface
;         adas209_proc.pro s invoked to determine how the user wishes to
;         process the input dataset. When the user's selections are 
;         complete the procedure processes them and establishes the values
;         of any outstanding arrays/variables. The information is then 
;         written back to the FORTRAN process via the pipe. Communications
;         are to the FORTRAN subroutine B9ISPF.
;
; USE:
;         ADAS209 specific, see adas209.pro
;
; INPUTS:
;         PIPE     - The IDL unit number of the bi-directional UNIX pipe
;                    to the ADAS209 FORTRAN process.
;
;         DSNAME   - String; The full system filename of the data 
;                    collection selected by the user for processing.
;
;         LEVELS   - Strarr(njlevx); Contains the original index, level
;                    configuration, Total spin/orbital angular momentum
;                    quantum numbers and ionisation potentials of the
;                    levels for which data is available in the input
;                    dataset.
;
;         ION      - String; The element and ion charge.
;
;         NCLC     - Integer; The elemental nuclear charge
;
;         IZ0      - String; The ion charge+1
;
;         PRNTS    - Strarr(njlevx); Contains the original index, term
;                    configuration, Total spin/orbital angular momentum
;                    quantum numbers and energies for the recombining ion
;                    parent metastables for which Dielectronic Recombination
;                    data is included in the dataset.
;
; OPTIONAL INPUTS:
;         None
;
; OUTPUTS:
;         LPEND    - Integer; 0 if the user pressed the 'Done' button
;                    or 1 if the user pressed 'Cancel'. Maps onto the 
;                    logical variable LPEND in the ADAS209 FORTRAN via 
;                    the integer ILOGIC.
;
;         GP_COUNT - Integer; The number of groupings selected by the user.
;                    maps directly onto NGP in the ADAS209 FORTRAN.
;
;         NJPRTX   - Integer; The number of J-resolved parent metastables.
;
;         NBPRTX   - Integer; The number of bundled parent metastables.
;
;         NJLEVX   - Integer; The number of J-resolved levels.
;
;         NBLEVX   - Integer; The number of bundled levels.
;
;         INDJP    - Integer; The index of the J-resolved parent.
;
;         BNDPR    - Intarr(njlevx); The parent bundling vector containing 
;                    the user selected group numbers and zero's for the
;                    unselected parents.
;
;         CNJP     - Strarr(njprtx); Contains the original parent
;                    parent metastable term configurations.
;
;         BNDLLS   - Intarr(njlevx); The level bundling vector containing
;                    the user selected group numbers and zero's for the
;                    unselected levels.
;
; OPTIONAL OUTPUTS:
;         None
;
; KEYWORD PARAMETERS:
;         FONT - The name of the font to be used on the widget.
;
; CALLS:
;         ADAS209_PROC   Invoke the IDL interface for ADAS209 data
;                        processing options/input.
;
; SIDE EFFECTS:
;         Bi-directional communication with ADAS209 FORTRAN 
;         via a UNIX pipe.
;
; CATEGORY:
;         ADAS system
;
; WRITTEN:
;       David H.Brooks, Univ.of Strathclyde, 	11-05-95
;
; MODIFIED:
;	1.1	David H. Brooks		
;		First release
;	1.2	Tim Hammond	
;		Tidied code up
;	1.3	Tim Hammond
;		Increased length of string prntst to 32 characters
;               in line with FORTRAN changes.
;       1.4     David H. Brooks  
;		Increased i1 to i3 in pipe communication of bndlLS
;               in line with FORTRAN changes.
;	1.5	David H. Brooks
;		Increased length of string prntst to 37 characters
;               & scr to 100 in line with FORTRAN changes.
;		Added gomenu and bitfile for menu button. Altered length
;               of scr & check1 and added space to levels. Increased
;               length of prntst again, to 49 characters. Introduced
;               structure value for new compound selection widget.
;               Altered method of finding gp_count.
;	1.6	Tim Hammond
;		Put in missing 'gomenu' in procedure definition.
;	1.7	David H. Brooks
;		Added a trap to catch error in not printing final level
;               under unusual indexing.
;       1.8     William Osborn
;		Increased dimension of head1 in line with ndlev
;       1.9     Martin O'Mullane
;		Increased dimension of head1 to 5000.
;
; VERSION:
;       1.1     11-05-95
;	1.2	27-06-95
;	1.3	17-07-95
;       1.4     23-10-95
;	1.5	19-01-95
;	1.6	19-01-95
;	1.7	09-02-96
;	1.8	18-04-96
;	1.9	14-04-2004
;               
;-
;---------------------------------------------------------------------

pro b9ispf, pipe, lpend, dsname, gomenu, bitfile, FONT=font

    IF NOT (KEYWORD_SET(font)) THEN font = ''

           ;***************************************;
           ;********Declare some variables ********;
           ;***************************************;

    check    = string(5)
    elhead   = string(40)
    head1    = strarr(5000)
    T_levels = elhead
    string   = string(15)
    scr      = string(100)
    check1   = string(5)
    check2   = '   -1'

           ;***************************************;
           ;open data set to check number of levels;
           ;***************************************;

    get_lun, unit
    openr, unit, dsname
    njlevx = 0
    repeat begin
        readf, unit, format='(1a100)', scr
        reads, scr, format='(1a5,95x)', check1
        head1(njlevx) = scr
        njlevx = njlevx+1
    endrep until check1 eq check2 
    free_lun, unit
    njlevx = njlevx-2        ; compensate for repeat loop reading then checking

           ;***************************************;
           ;* number established so create array  *;
           ;***************************************;

    levels  = strarr(njlevx)
    prnts = levels
    for i = 0, njlevx-1 do begin
        reads, head1(i+1), format = '(1a45)', T_levels
        levels(i) = T_levels + '   '
    endfor

           ;***************************************;
           ;read in ion charge & parent metastables;
           ;***************************************;

    iont = string(5)
    nclct = string(10)
    met1t = string(12) 
    met2t = met1t & met3t = met1t & met4t = met1t
    reads, head1(0), format='(1a5,5x,i5,1a10,75x)', iont, nclct, iz0t
    ion = iont
    nclc = nclct
    iz0 = iz0t

           ;***************************************;
           ; read pipe communications from fortran ;
           ;***************************************;

    prntst = ''
    readf, pipe, format='(1x,i1)', njprtx
    if njprtx eq 0 then njprtx = 1 
        for i = 0, njprtx-1 do begin
            readf, pipe, format='(1a49)', prntst
            prnts(i) = prntst
    endfor

           ;***************************************;
           ;*** establish no. of parent/bundles ***;
           ;***************************************;

    nbprtx = 0
    cnjp = strarr(njlevx)
    for i = 0, njprtx-1 do begin
        temp = strmid(prnts(i), 10, 9)
        space = '                  '
        strput, space, temp, 0
        cnjp(i) = space
    endfor

           ;***************************************;
           ;**** call widget creation function ****;
           ;***************************************;

    bndlLS = intarr(njlevx)
    bndl_prnts = bndlLS
    pmdflg = 1

           ;***************************************;
           ;*** create structure for processing ***;
           ;***************************************;

        value = { dsname       : dsname     ,				$
                  dtname       : dsname     ,				$
                  dpname       : dsname     ,				$
                  njlevx       : njlevx     ,				$
                  njprtx       : njprtx     ,				$
                  njprtx2      : njprtx     ,				$
                  levels       : levels     ,				$
                  levels2      : levels     ,				$
		  bndlLS       : bndlLS     ,				$
                  prnts1       : prnts      ,				$
                  prnts2       : prnts      ,				$
                  bndl_prnts   : bndl_prnts ,				$
                  ion          : ion        ,				$
                  nclc         : nclc       ,				$
                  pmdflg       : pmdflg      				}

    adas209_proc, njlevx, bitfile, value, action, FONT=font	

           ;***************************************;
           ;*** copy values back from widget    ***;
           ;***************************************;

    dsname = value.dsname
    njlevx = value.njlevx
    njprtx = value.njprtx
    njprtx2 = value.njprtx2
    levels = value.levels
    levels2 = value.levels2
    bndlLS = value.bndlLS
    prnts1 = value.prnts1
    prnts2 = value.prnts2
    bndl_prnts = value.bndl_prnts

           ;***************************************;
           ;***** process cancel/done options *****;           
           ;***************************************; 

    if action eq 'Done' then begin
        lpend = 0
        printf, pipe, format='(i1)', lpend
    endif else if action eq 'Menu' then begin
        lpend = 0
        gomenu = 1
        printf, pipe, format = '(i1)', lpend
        goto, labelend
    endif else begin
        lpend = 1
        printf, pipe, format='(i1)', lpend
        goto, labelend
    endelse

           ;***************************************;
           ;* create parent string arrays/indexes *;
           ;***************************************;

    indjp = intarr(njlevx)
    bndpr = indjp
    for i = 0, njprtx-1 do begin
        indjp(i) = i+1
        if bndl_prnts(i+1) gt bndl_prnts(i) then begin
            nbprtx = bndl_prnts(i+1)
        endif
    endfor
    if bndl_prnts(0) gt nbprtx then nbprtx = bndl_prnts(0) 
    for  i = 0, (njprtx-1) do begin
        if bndl_prnts(i) eq 0 then begin
            nbprtx = nbprtx+1
        endif
    endfor
    for i = 0, (njlevx-1) do begin
        bndpr(i) = -1
        if (i lt njprtx) then begin
            bndpr(i) = bndl_prnts(i)
        endif
    endfor

           ;***************************************;
           ;*** determine no. of bundled levels ***;
           ;***************************************;

;    gp_count = 0
;    for i = 0, (njlevx-2) do begin
;        if ( bndlLS(i+1) gt bndlLS(i) ) then begin
;            gp_count = gp_count + 1
;        endif
;    endfor
;    for i = 0, (njlevx-2) do begin
;        if bndlLS(i) gt gp_count and bndlLS(i+1) eq 0 then begin
;            gp_count = gp_count+1
;        endif
;    endfor
;    if bndlLS(0) gt gp_count then gp_count = bndlLS(0)
;    if bndlLS(njlevx-1) gt gp_count then gp_count = bndlLS(njlevx-1)


    gp_count =  n_elements(uniq(bndlLS,sort(bndlLS)))

    nblevx = gp_count
    for i = 0, (njlevx-1) do begin
        if (bndlLS(i) eq 0) then begin
            nblevx = nblevx+1
        endif
    endfor

           ;***************************************;
           ;******* fire selections to f77  *******;
           ;***************************************;

    printf, pipe, format = '(i5)', gp_count
    printf, pipe, format = '(i5)', iz0
    printf, pipe, format = '(i5)', njprtx
    printf, pipe, format = '(i5)', nbprtx
    printf, pipe, format = '(i5)', njlevx
    printf, pipe, format = '(i5)', nblevx
    for i = 0, (njprtx-1) do begin
        printf, pipe,format = '(i5,1x,i5,1x,1a18,1x)',			$
                indjp(i),bndpr(i),cnjp(i)
    endfor  
    for i = 0, (njlevx-1) do begin
        printf, pipe, format = '(1x,i3)', bndlLS(i)
    endfor

labelend:

END
