; Copyright (c) 1996, Strathclyde University.
; SCCS info: Module @(#)$Header: /home/adascvs/idl/adas2xx/adas204/adas204_out.pro,v 1.3 2005/05/24 15:37:39 mog Exp $ Date $Date: 2005/05/24 15:37:39 $
;+
; PROJECT:
;       ADAS 
;
; NAME:
;	ADAS204_OUT
;
; PURPOSE: 
;       This function puts up a widget to get the output file names.
;       The adf17 (was cbnm.pass) is obligatory. paper.txt is
;       optional. ADAS204 can also produce adf10 files but these
;       are now relegated to expert user only and can be selected
;       if required.
;
;
; NOTES:
;       This routine replaces the original adas204_out.pro and b4spf1.pro.
;
;
; INPUTS:
;        outval       - A structure holding the 'remembered' outout options.
;         
;        bitfile      - directory of bitmaps for menu button
;
;
; OPTIONAL INPUTS:
;       None
;
; OUTPUTS:
;       The function returns a string rep
;           rep = 'CANCEL' if there is a problem or indeed if the user
;                          closes the widget with the mouse. 
;           rep = 'MENU' if user want to exit to menu at this point.
;           rep = 'RUN'   for immediate execution.
;           rep = 'BATCH' for submission to batch queue.
;
;           file_names : list of output files.
;           file_open  : write switches corresponding to file_names.
;
; OPTIONAL OUTPUTS:
;       None
;
; KEYWORD PARAMETERS:
;       FONT_LARGE - The name of a larger font e.g 'courier_bold14'
;
;       FONT_SMALL - The name of a smaller font e.g 'courier_bold12'
;
;
; CALLS:
;                            
;       ADAS204_OUT_MENU         : React to menu button event. The standard IDL
;                                  reaction to button events cannot deal with
;                                  pixmapped buttons. Hence the special handler.
;       ADAS204_OUT_EVENT        : Reacts to cancel and Done. Does the file
;                                  existence error checking.
;
; SIDE EFFECTS:
;       Unlike the original this requires IDL v5. 
;
; CATEGORY:
;	Widgets
;
; WRITTEN:
;       William Osborn, Tessella Support Services plc, 9th August 1996
;
; MODIFIED:
;       1.1     William Osborn
;		  - First release 
;	1.2	Martin O'Mullane
;		  - Completely rewritten to make choice of output files
;                    more selective.  
;	1.3	Martin O'Mullane
;		  - Add another optional output file for populations.
;
; VERSION:
;       1.1	09-08-1996
;       1.2	06-09-2002
;	1.3	23-05-2005
;
;-
;-----------------------------------------------------------------------------


PRO ADAS204_OUT_MENU, event

; The 'normal' get_value from a widget_button does not work with a
; bitmap button. Hence this specialised event handler.

   ; Get the info structure out of the user value of the top-level base.

Widget_Control, event.top, Get_UValue=info


; Something nefarious may cause an error also.
;  -- if so trap it and exit gracefully

CATCH, error
IF error NE 0 THEN BEGIN
   formdata = {cancel : 0, menu:1}
   *info.ptrToFormData = formdata
   Widget_Control, event.top, /Destroy
   message,!err_string
   RETURN
ENDIF


 formdata = {cancel : 0, menu:1}
 *info.ptrToFormData =formdata
 widget_Control, event.top, /destroy

END


;-----------------------------------------------------------------------------


PRO ADAS204_OUT_EVENT, event

; React to button events - Cancel and Done but not menu (this requires a
; specialised event handler ADAS204_OUT_MENU). Also deal with the passing
; directory output Default button here.

; On pressing Done check for the following


   ; Get the info structure out of the user value of the top-level base.

Widget_Control, event.top, Get_UValue=info

; Something nefarious may cause an error also.
;  -- if so trap it and exit gracefully

CATCH, error
IF error NE 0 THEN BEGIN
   formdata = {cancel:1, menu : 0}
   *info.ptrToFormData = formdata
   Widget_Control, event.top, /Destroy
   print,!err_string
   RETURN
ENDIF

Widget_Control, event.id, Get_Value=userEvent

if userevent EQ 'Cancel' then begin

   formdata = {cancel:1, menu : 0}
   *info.ptrToFormData =formdata
   widget_Control, event.top, /destroy
               
endif 


if userEvent EQ 'Run Now' OR userEvent EQ 'Run in Batch' then begin    
          
          
     if userEvent EQ 'Run Now' then isnow = 1 else isnow = 0     
          
     ; gather the data for return to calling program
     
     err  = 0
     mess = ' '
     
     ; no need to set mess as it is flagged in the cw
     
     widget_Control, info.paperID, Get_Value=paper
     if paper.outbut EQ 1 AND strtrim(paper.message) NE '' then err=1

     widget_Control, info.a17ID, Get_Value=a17
     if a17.outbut EQ 1 AND strtrim(a17.message) NE '' then err=1
     
     ierr = 0
     for j = 0, 6 do begin 
       
       widget_control, info.opID[j], Get_Value=tmp
       if tmp.outbut EQ 1 AND strtrim(tmp.message) NE '' then begin
          ierr  = 1
          mess = tmp.message
       endif
       
       str = 'op' + string(j+1, format='(i1)') + ' = tmp'
       res = execute(str)
      
      endfor
      
      if ierr EQ 1 then begin
         err = 1
         widget_control, info.messID, set_value = mess
      endif
      
     
     if err EQ 0 then begin
        formdata = { paper    : paper,   $
                     a17      : a17,     $
                     op1      : op1,     $
                     op2      : op2,     $
                     op3      : op3,     $
                     op4      : op4,     $
                     op5      : op5,     $
                     op6      : op6,     $
                     op7      : op7,     $
                     runnow   : isnow,   $
                     cancel   : 0,       $
                     menu     : 0        }
        *info.ptrToFormData = formdata
        widget_control, event.top, /destroy
     endif else begin
        widget_Control, info.messID, Set_Value=mess
     endelse
  
endif
             

END
  
;-----------------------------------------------------------------------------



FUNCTION ADAS204_OUT, outval,                   $ 
                      bitfile,                  $ 
                      file_names, file_open,    $
                      FONT_LARGE = font_large,  $
                      FONT_SMALL = font_small


file_names = ''
file_open  = -1

; Set defaults for keywords and extract info for paper.txt question

  IF n_elements(font_large) eq 0 THEN font_large = ''
  IF n_elements(font_small) eq 0 THEN font_small = ''


  paperval =  { outbut   : outval.TEXOUT, $
                appbut   : outval.TEXAPP, $
                repbut   : outval.TEXREP, $
                filename : outval.TEXDSN, $
                defname  : outval.TEXDEF, $
                message  : outval.TEXMES  }
  
  a17val =    { outbut   : outval.a17OUT, $
                appbut   : outval.a17APP, $
                repbut   : outval.a17REP, $
                filename : outval.a17DSN, $
                defname  : outval.a17DEF, $
                message  : outval.a17MES  }
  


; Create modal top level base widget
                
  
  parent = Widget_Base(Title='ADAS204 OUTPUT', XOFFSET=100, YOFFSET=1, /column)

  label  = widget_label(parent,value='  ',font=font_large)



; Standard files

  base    = widget_base(parent,/frame)
  
  mcol    = widget_base(base,/column)
  label   = widget_label(mcol,value='Standard output files :-',font=font_large)
  paperID = cw_adas_outfile(mcol, OUTPUT='Text Output',   $
                                 VALUE=paperval, FONT=font_large)
  
  a17ID   = cw_adas_outfile(mcol, OUTPUT='Projection matrices',   $
                                  VALUE=a17val, FONT=font_large)
 
 
; Expert option output files 
 
  base    = widget_base(parent,/frame)
  mcol    = widget_base(mcol, xsize=0, ysize=170, /scroll, /column)

  label   = widget_label(mcol,value='Optional output files :-',$
                         font=font_large, /align_center)
  
  
  opID    = lonarr(7)
  opnames = ['cbnmpr', 'acd', 'scd', 'xcd', 'prb', 'pcasbin', 'population'] 
  
  for j = 0, 6 do begin
    
    value  =  { outbut   : outval.opfOUT[j], $
                appbut   : outval.opfAPP[j], $
                repbut   : outval.opfREP[j], $
                filename : outval.opfDSN[j], $
                defname  : outval.opfDEF[j], $
                message  : outval.opfMES[j]  }

    opID[j] = cw_adas_outfile(mcol, OUTPUT=opnames[j],   $
                                    VALUE=value, FONT=font_small)
  endfor  
                            
; Message area
                
  messID = widget_label(parent,value='     Choose output options   ',font=font_large)
                      
; Actions

                
  menufile = bitfile + '/menu.bmp'
  read_X11_bitmap, menufile, bitmap1
                
  mrow     = widget_base(parent,/row)
  menuID   = widget_button(mrow,value=bitmap1,font=font_large, $
                           event_pro='ADAS204_OUT_MENU')
  cancelID = widget_button(mrow,value='Cancel',font=font_large)
  runID    = widget_button(mrow,value='Run Now',font=font_large)
  batchID  = widget_button(mrow,value='Run in Batch',font=font_large)





                ;***************************
		;**** Put up the widget ****
                ;***************************

; Realize the ADAS204_OUT input widget.

   dynlabel, parent
   widget_Control, parent, /realize

; Create a pointer to store the information collected from the form.
; The initial data stored here is set to CANCEL, so nothing needs to
; be done if the user kills the widget with the mouse.

   ptrToFormData = Ptr_New({cancel:1, menu:0})

; Create an info structure with program information.

  info = { messID          :  messID,             $
           paperID         :  paperID,            $
           a17ID           :  a17ID,              $
           opID            :  opID,               $
           ptrToFormData   :  ptrToFormData       }  
            
               
; Store the info structure in the user value of the top-level base and launch
; the widget into the world.

  widget_control, parent, Set_UValue=info

  XManager, 'ADAS204_OUT', parent, Event_Handler='ADAS204_OUT_EVENT'


; When the widget is destroyed, the block is released, and we
; return here. Get the form data that was collected by the form
; and stored in the pointer location.

formdata = *ptrToFormData


rep = 'CONTINUE'
IF N_Elements(formdata) EQ 0 THEN BEGIN
   Ptr_Free, ptrToFormData
   rep = 'CANCEL'
   RETURN,rep
ENDIF
 
IF formdata.cancel EQ 1 THEN BEGIN
   Ptr_Free, ptrToFormData
   rep = 'CANCEL'
   RETURN,rep
ENDIF

IF formdata.menu EQ 1 THEN BEGIN
   Ptr_Free, ptrToFormData
   rep = 'MENU'
   RETURN,rep
ENDIF



if rep eq 'CONTINUE' then begin
   
    outval.TEXOUT  = formdata.paper.outbut   
    outval.TEXAPP  = formdata.paper.appbut   
    outval.TEXREP  = formdata.paper.repbut   
    outval.TEXDSN  = formdata.paper.filename 
    outval.TEXDEF  = formdata.paper.defname  
    outval.TEXMES  = formdata.paper.message  
    outval.a17OUT  = formdata.a17.outbut   
    outval.a17APP  = formdata.a17.appbut   
    outval.a17REP  = formdata.a17.repbut   
    outval.a17DSN  = formdata.a17.filename 
    outval.a17DEF  = formdata.a17.defname  
    outval.a17MES  = formdata.a17.message  
   
    file_names = [outval.TEXDSN, outval.a17DSN]
    file_open  = [outval.TEXOUT, outval.a17OUT] 
    
    str_left  = ['OUT', 'APP', 'REP', 'DSN', 'DEF', 'MES']
    str_right = ['outbut', 'appbut', 'repbut', 'filename', 'defname', $
                 'message']
    
    for j = 0, 6 do begin
      
      j_str   = string(j, format='(i1)')
      j1_str  = string(j+1, format='(i1)')
      
      for k = 0, 5 do begin
         k_str = string(k, format='(i1)')
         str   = 'outval.opf' + str_left[k] + '[' + j_str + '] = ' + $
                 'formdata.op' + j1_str + '.' + str_right[k]
         res   = execute(str)
      endfor
      
      file_names = [file_names, outval.opfdsn[j]]
      file_open  = [file_open, outval.opfout[j]]  
    
    endfor
    
    if formdata.runnow then rep='RUN' else rep = 'BATCH'
                
    Ptr_Free, ptrToFormData

endif       
  

; And tell ADAS204 that we are finished

RETURN,rep

END
