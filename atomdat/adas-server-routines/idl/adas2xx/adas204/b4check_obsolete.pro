; Copyright (c) 1999, Strathclyde University.
; SCCS info: Module @(#)$Header: /home/adascvs/idl/adas2xx/adas204/b4check_obsolete.pro,v 1.1 2004/07/06 11:21:38 whitefor Exp $ Date $Date: 2004/07/06 11:21:38 $
;+
; PROJECT:
;       ADAS
;
; NAME:
;	B4CHECK_OBSOLETE
;
; PURPOSE:
;	Checks whether an adf25 input file is the old format. 
;
;
; USE:
;	The use of this routine is specific to ADAS204, see adas204.pro.
;
; INPUTS:
;	DSFULL  - String; the input file name
;
; OPTIONAL INPUTS:
;	None
;
; OUTPUTS:
;	REP_OBS	- String; it's YES or NO
;
;
; OPTIONAL OUTPUTS:
;	None
;
; KEYWORD PARAMETERS:
;	FONT	- Supplies the font to be used for the interface widgets.
;
; CALLS:
;
; SIDE EFFECTS:
;	If the result is YES then ADAS204 must stop.
;
; CATEGORY:
;	Adas system.
;
; WRITTEN:
;       Martin O'Mullane, 26/07/1999
;
; MODIFIED:
;	1.1	Martin O'Mullane
;		First Release
; VERSION:
;	1.1	27-07-99
;
;-----------------------------------------------------------------------------


PRO b4check_obsolete, dsfull, rep_obs, FONT=font

                ;**** Return to caller on error ****

;    ON_ERROR, 2

                ;**** Set defaults for keywords ****

    IF NOT (KEYWORD_SET(font)) THEN font = ''

    
; open file and check if IONIZ and DRSUP are part of the namelist
; we may as well use grep

            
    spawn,'grep -i ioniz '+dsfull, res
    resu = STRUPCASE(res)
    if STRPOS(resu(0),'IONIZ') eq -1 then no_ion = 1 else no_ion = 0
    
    spawn,'grep -i drsup '+dsfull, res
    resu = STRUPCASE(res)
    if STRPOS(resu(0),'DRSUP') eq -1 then no_dr = 1 else no_dr = 0
    
        
    if no_ion eq 1 or no_dr eq 1 then begin
       message = ['The format of the adf25 input file has changed.', $
                  ' ',$
                  'The ionisation and DR supplement filenames are ',$
                  'now part of the adf25 input file.              ',$
                  ' ',$
                  'Include the following members in the namelist: ',$
                  'IONIZ  = ''"ADASCENT"/.....''',$
                  'DRSUP  = ''"ADASUSER"/.....''',$
                  ' ',$
                  'with your choice of USER or CENTral data.      ',$
                  ' ']
    endif
    
    
    if no_ion eq 0 and no_dr eq 0 then begin
       message = 'ok'
    endif
    
    if message(0) ne 'ok' then $
    action = popup(MESSAGE=message, TITLE='ADAS204 Obsolete input file',    $
                   BUTTONS=['Accept'],    FONT=font)
    
    
    if no_ion eq 1 or no_dr eq 1 then rep_obs = 'YES' else rep_obs = 'NO'
     
 
END
