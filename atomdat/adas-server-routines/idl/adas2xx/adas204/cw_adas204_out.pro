; Copyright (c) 1996, Strathclyde University.
; SCCS Info : Module @(#)$Header: /home/adascvs/idl/adas2xx/adas204/cw_adas204_out.pro,v 1.1 2004/07/06 12:30:15 whitefor Exp $ Date $Date: 2004/07/06 12:30:15 $
;+
; PROJECT:
;       ADAS IBM MVS to UNIX conversion
;
; NAME:
;	CW_ADAS204_OUT()
;
; PURPOSE:
;	Produces a widget for ADAS204 output options.
;
; EXPLANATION:
;	This function declares a compound widget consisting of one
;;	output file widget cw_adas_outfile.pro for text output and a
;	text widget for the passing file directory. This widget also
;	includes a button for browsing the 
;	comments from the input dataset, a 'Cancel' button and a 'Run'
;	button.
;	The compound widgets cw_adas_outfile.pro included in this file 
;	are self managing.  This widget only handles events from the 
;	'Cancel' and 'Run' buttons.
;
;	This widget only generates events for the 'Run' and 'Cancel'
;	buttons.
;
; USE:
;	This routine is unique to adas204.                              
;
; INPUTS:
;	PARENT	- Long integer; ID of parent widget.
;
;	DSFULL	- Name of input dataset for this application.
;
; OPTIONAL INPUTS:
;	None.
;
; OUTPUTS:
;	The return value of this function is the ID of the declared widget.
;
; OPTIONAL OUTPUTS:
;	None.
;
; KEYWORD PARAMETERS:
;	DEVLIST	- A string array;  Each element of the array is the
;		  name of a hardcopy graphics output device.  This
;		  list must mirror the DEVCODE list.  DEVLIST is the
;		  list which the user will see.
;		  e.g ['Post-Script','HP-PCL','HP-GL']
;
;	VALUE	- A structure which determines the initial settings of
;		  the output options widget.  The structure is made
;		  up of two parts.  Each part is the same as the value
;		  structure of one of the two main compound widgets
;		  included in this widget.  See 
;		  cw_adas_outfile for more details.  The default value is;
;
;		      { 						$
;			PASSDIR:'',					$
;;			TEXOUT:0, TEXAPP:0,  				$
;;			TEXREP:0, TEXDSN:'', 				$
;;			TEXDEF:'', TEXMES:''  	$
;		      }
;
;		PASSDIR	String; the directory for passing file output
;;	  For CW_ADAS_OUTFILE PAPER.TXT output
;;		TEXOUT  Integer; Activation button 1 on, 0 off
;;		TEXAPP  Integer; Append button 1 on, 0 off, -1 no button
;;		TEXREP  Integer; Replace button 1 on, 0 off, -1 no butn
;;		TEXDSN  String; Output file name
;;		TEXDEF  String; Default file name
;;		TEXMES  String; file name error message
;
;
;	UVALUE	- A user value for this widget.
;
;	FONT	- String; the name of a font to be used for all text in
;		  this widget.  Default to system default font.
;
; CALLS:
;	CW_ADAS_DSBR	Input dataset name and comments browse button.
;	CW_ADAS_OUTFILE	Output file name entry widget.
;	See side effects for other related widget management routines.
;
; SIDE EFFECTS:
;
; 	Two other routines are included in this file
;	OUT204_GET_VAL()
;	OUT204_EVENT()
;
; CATEGORY:
;	Compound Widget
;
; WRITTEN:
;       William Osborn, Tessella Support Services plc, 20th August 1996
;
; MODIFIED:
;	1.1	William Osborn
;		First Release
; VERSION:
;	1.1	20/08/96
;
;-
;-----------------------------------------------------------------------------

FUNCTION out204_get_val, id

                ;**** Return to caller on error ****

  ON_ERROR, 2

                ;**** Retrieve the state ****

  parent=widget_info(id, /parent)
  widget_control, parent, get_uvalue=state, /no_copy

;		;**** Get text output settings ****
;
;  widget_control, state.textid, get_value=ptpos

		;**** Get passing directory name ****
  widget_control, state.passid, get_value=passdir

  os = {   out204_set, 							$
	   PASSDIR:passdir(0)						$
;	   TEXOUT:ptpos.outbut,  TEXAPP:ptpos.appbut,		 	$
;	   TEXREP:ptpos.repbut,	 TEXDSN:ptpos.filename,		 	$
;	   TEXDEF:ptpos.defname, TEXMES:ptpos.message		 	$
        }

                ;**** Return the state ****

  widget_control, parent, set_uvalue=state, /no_copy

  RETURN, os 

END

;-----------------------------------------------------------------------------

FUNCTION out204_event, event


                ;**** Base ID of compound widget ****

  parent=event.top

                ;**** Retrieve the state ****

  widget_control, parent, get_uvalue=state, /no_copy

		;*********************************
		;**** Clear previous messages ****
		;*********************************

  widget_control, state.messid, set_value=' '

                ;************************
                ;**** Process events ****
                ;************************

  CASE event.id OF

		;***********************
		;**** Cancel button ****
		;***********************

    state.cancelid: new_event = {ID:parent, TOP:event.top, 		$
				 HANDLER:0L, ACTION:'Cancel'}

		;************************
		;**** Run Now button ****
		;************************

    state.runid: begin

		;****************************************************
                ;**** Return the state before checking can start ****
		;**** with the get_value keyword.                ****
		;****************************************************

          widget_control, parent, set_uvalue=state, /no_copy

		;***************************************
		;**** Check for errors in the input ****
		;***************************************

	  error = 0

		;**** Get a copy of the widget value ****

	  widget_control, event.handler, get_value=os
	
		;**** Check for widget error messages ****

	  mess=''
;	  if (os.texout eq 1 and strtrim(os.texmes) ne '') then error=1

		;**** Check that the passing directory exists ****
	  file_acc, os.passdir, exist, read, write, execute, filetype
	  if exist eq 0 then begin
	    error=1
	    mess="**** Error: Passing-file directory does not exist ****"
	  endif else if filetype ne 'd' then begin
	    error=1
	    mess="**** Error: Passing-file directory is not a directory ****"
	  endif else begin
		;**** Add a / to the end if there isn't one ****
	    if strmid(os.passdir, strlen(os.passdir)-1,1) ne '/' then $
		os.passdir=os.passdir+'/'
	  endelse

                ;**** Retrieve the state   ****

          widget_control, parent, get_uvalue=state, /no_copy

	  if error eq 1 then begin
	      if mess eq '' then mess = '**** Error in output settings ****'
	      widget_control, state.messid, set_value= mess
	      new_event = 0L
	  endif else begin
	      new_event = {ID:parent, TOP:event.top, HANDLER:0L, 	$
	      ACTION:'Run Now'}
	  endelse

        end

		;*****************************
		;**** Run In Batch button ****
		;*****************************

    state.batid: begin

		;****************************************************
                ;**** Return the state before checking can start ****
		;**** with the get_value keyword.                ****
		;****************************************************

          widget_control, parent, set_uvalue=state, /no_copy

		;***************************************
		;**** Check for errors in the input ****
		;***************************************

	  error = 0

		;**** Get a copy of the widget value ****

	  widget_control, event.handler, get_value=os
	
		;**** Check for widget error messages ****

	  mess=''
;	  if (os.texout eq 1 and strtrim(os.texmes) ne '') then error=1

		;**** Check that the passing directory exists ****
	  file_acc, os.passdir, exist, read, write, execute, filetype
	  if exist eq 0 then begin
	    error=1
	    mess="**** Error: Passing-file directory does not exist ****"
	  endif else if filetype ne 'd' then begin
	    error=1
	    mess="**** Error: Passing-file directory is not a directory ****"
	  endif else begin
		;**** Add a / to the end if there isn't one ****
	    if strmid(os.passdir, strlen(os.passdir)-1,1) ne '/' then $
		os.passdir=os.passdir+'/'
	  endelse

                ;**** Retrieve the state   ****

          widget_control, parent, get_uvalue=state, /no_copy

	  if error eq 1 then begin
	      if mess eq '' then mess = '**** Error in output settings ****'
	      widget_control, state.messid, set_value= mess
	      new_event = 0L
	  endif else begin
	      new_event = {ID:parent, TOP:event.top, HANDLER:0L, 	$
	      ACTION:'Run In Batch'}
	  endelse

        end

    ELSE: new_event = 0L

  ENDCASE

                ;**** Return the state   ****

          widget_control, parent, set_uvalue=state, /no_copy

  RETURN, new_event
END

;-----------------------------------------------------------------------------

FUNCTION cw_adas204_out, parent, dsfull, VALUE=value, UVALUE=uvalue, 	$
                         FONT=font

  IF (N_PARAMS() EQ 0) THEN MESSAGE, 'Must specify parent for cw_adas204_out'
  ON_ERROR, 2					;return to caller

		;**** Set defaults for keywords ****

  IF NOT (KEYWORD_SET(uvalue)) THEN uvalue = 0
  IF NOT (KEYWORD_SET(font)) THEN font = ''
  IF NOT (KEYWORD_SET(value)) THEN begin
	os = {out204_set, 						$
			PASSDIR:''					$
;			TEXOUT:0, TEXAPP:0,				$
;			TEXREP:0, TEXDSN:'',				$
;			TEXDEF:'paper.txt', TEXMES:''			$
              }
  END ELSE BEGIN
	os = {out204_set, 						$
			PASSDIR:value.passdir				$
;			TEXOUT:value.texout, TEXAPP:value.texapp,       $
;                        TEXREP:value.texrep, TEXDSN:value.texdsn,       $
;                        TEXDEF:value.texdef, TEXMES:value.texmes	$
	      }
  END

		;**********************************************
		;**** Create the 204 Output options widget ****
		;**********************************************

		;**** create base widget ****

  cwid = widget_base( parent, UVALUE = uvalue, 				$
		      EVENT_FUNC = "out204_event", 			$
		      FUNC_GET_VALUE = "out204_get_val", 		$
		      /COLUMN)

		;**** Add dataset name and browse button ****

  rc = cw_adas_dsbr(cwid, dsfull, font=font, filelabel='Input File Name: ')

;		;**** Widget for paper.txt output ****
;
;  outfval = { OUTBUT:os.texout, APPBUT:os.texapp, REPBUT:os.texrep, 	$
;	      FILENAME:os.texdsn, DEFNAME:os.texdef, MESSAGE:os.texmes }
;  base = widget_base(cwid, /row, /frame)
;  textid = cw_adas_outfile(base, OUTPUT='Text output', 		$
;                            VALUE=outfval, FONT=font)

		;**** Widget for passing file directory specification ****
  base = widget_base(cwid, /column, /frame)
  lab = widget_label(base, value = 'Directory for passing files:',font=font)
  passid = widget_text(base, value = os.passdir, font=font, /editable,$
		     xsize = 60)

		;**** Error message ****

  messid = widget_label(cwid, value=' ', font=font)

		;**** add the exit buttons ****

  base = widget_base(cwid, /row)
  cancelid = widget_button(base, value='Cancel', font=font)
  runid = widget_button(base, value='Run Now', font=font)
  batid = widget_button(base, value='Run in Batch', font=font)
  
		;*************************************************
		;**** create a state structure for the pop-up ****
		;**** window.                                 ****
		;*************************************************

  new_state = { PASSID:passid, 	$
;		TEXTID:textid,		$
		CANCELID:cancelid, RUNID:runid, BATID:batid, MESSID:messid}

                ;**** Save initial state structure ****

  widget_control, parent, set_uvalue=new_state, /no_copy
  RETURN, cwid

END

