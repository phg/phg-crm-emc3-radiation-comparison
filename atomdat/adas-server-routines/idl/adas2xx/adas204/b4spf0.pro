; Copyright (c) 1996, Strathclyde University
; SCCS info: Module @(#)$Header: /home/adascvs/idl/adas2xx/adas204/b4spf0.pro,v 1.3 2004/07/06 11:22:26 whitefor Exp $ Date $Date: 2004/07/06 11:22:26 $
;+
; PROJECT:
;       ADAS IBM MVS to UNIX conversion
;
; NAME:
;	B4SPF0
;
; PURPOSE:
;	IDL user interface and communications with ADAS204 FORTRAN
;	process via pipe.
;
; EXPLANATION:
;	Firstly this routine invokes the part of the user interface
;	used to select the input dataset for ADAS204. The resulting
;	action is written to the Fortran via the pipe. Communications
;	are with the FORTRAN subroutine B4SPFX.
;
; USE:
;	The use of this routine is specific to ADAS204, see adas204.pro.
;
; INPUTS:
;	PIPE	- The UNIX pipe unit number
;
;	VALUE	- A structure which determines the initial settings of
;		  the compound widget.  The initial value is
;		  set in adas204.pro.  VALUE is passed un-modified
;		  through to cw_adas204_in.pro, see that routine for a full
;		  description.
;
; OPTIONAL INPUTS:
;	None
;
; OUTPUTS:
;	VALUE	- On output the structure records the final settings of
;		  the dataset selection widget if the user pressed the
;		  'Done' button otherwise it is not changed from input.
;
;	DSFULL  - String; the input file name
;
;	REP	- String; whether the cancel button was pressed, YES or NO
;
;	NCALC   - Integer; the number of calculations that the input file
;		  specifies
;
; OPTIONAL OUTPUTS:
;	None
;
; KEYWORD PARAMETERS:
;	FONT	- Supplies the font to be used for the interface widgets.
;
; CALLS:
;	ADAS204_IN	Pops-up the dataset selection widget.
;
; SIDE EFFECTS:
;	This routine communicates with the ADAS204 FORTRAN process
;	via a UNIX pipe.
;
; CATEGORY:
;	Adas system.
;
; WRITTEN:
;       William Osborn, Tessella Support Services plc, 09/08/1996
;
; MODIFIED:
;	1.1	William Osborn
;		First Release
;       1.2     William Osborn
;               Added write of value.rootpath
;	1.3	Martin O'Mullane
;		Added construction of data & X-ref filenames.
;
; VERSION:
;	1.1	09-08-96
;	1.2	22-10-96
;	1.3 	04-12-98
;
;-----------------------------------------------------------------------------


PRO b4spf0, pipe, value, rep, adas_c, adas_u, dsfull, ncalc, FONT=font

                ;**** Return to caller on error ****

;    ON_ERROR, 2

                ;**** Set defaults for keywords ****

    IF NOT (KEYWORD_SET(font)) THEN font = ''

    ncalc=1     ; only one calculation 

		;**********************************
		;**** Pop-up input file widget ****
		;**********************************

    adas_in, value, action,  WINTITLE = 'ADAS 204 INPUT',         $
                             TITLE = 'Input File', FONT = font

		;********************************************
		;**** Act on the event from the widget   ****
		;**** There are only two possible events ****
		;**** 'Done' and 'Cancel'.               ****
		;********************************************

    if action eq 'Done' then begin
        rep = 'NO'
    end else begin
        rep = 'YES'
    end

		;**** Construct dataset name ****

    dsfull = value.rootpath+value.file
    
		;*************************************
		;**** Write data to fortran       ****
		;*************************************

    printf, pipe, rep
    printf, pipe, dsfull
    printf, pipe, adas_c
    printf, pipe, adas_u

END
