; Copyright (c) 1996, Strathclyde University.
; SCCS Info: Module @(#)$Header: /home/adascvs/idl/adas2xx/adas204/b4spf2.pro,v 1.3 2009/11/17 14:59:11 mog Exp $ Date $Date: 2009/11/17 14:59:11 $
;
;+
; PROJECT:
;       ADAS IBM MVS to UNIX conversion
;
; NAME:
;	B4SPF2
;
; PURPOSE:
;	IDL user interface and communications with ADAS204 FORTRAN
;	process via pipe.
;
; EXPLANATION:
;	This routine creates a progress indicator which is updated as
;	the FORTRAN progresses. The FORTRAN communicates via a pipe
;	the total number of stages and the stage which has been reached.
;
; USE:
;	The use of this routine is specific to ADAS204. See adas204.pro.
;
; INPUTS:
;	PIPE	- The IDL unit number of the bi-directional pipe to the
;		  ADAS204 FORTRAN process.
;
;	PID	- The Fortran process ID
;
;	NCALC   - Integer; the number of calculations specified in the
;		  input file.
;
; OPTIONAL INPUTS:
;	None.
;
; OUTPUTS:
;	None
;
; OPTIONAL OUTPUTS:
;	None.
;
; KEYWORD PARAMETERS:
;
;	FONT	- Supplies the font to be used for the interface widgets.
;
; CALLS:
;
; SIDE EFFECTS:
;	This routine communicates with the ADAS204 FORTRAN processes
;	via UNIX pipes.
;	It also pops up an information widget which keeps the user 
;	updated as the calculation progresses.
;
; CATEGORY:
;	Adas system.
;
; WRITTEN:
;       William Osborn, Tessella Support Services plc, 20-August-1996
;
; MODIFIED:
;	1.1	William Osborn
;		First release
;	1.2	Richard Martin
;		Added font keyword to widget_label statements
;		Removed "part 1/1" from string.
;	1.3	Martin O'Mullane
;		Change email to adas.ac.uk.
; VERSION:
;	1.1	20-08-96
;	1.2	20-09-99
;	1.3	17-11-2009
;-
;-----------------------------------------------------------------------------

PRO b4spf2, pipe, pid, ncalc, FONT=font


                ;**** If there is an io error caused ****
		;**** by the Fortran crashing handle it ****

    ON_IOERROR, PIPE_ERR

                ;**** Set defaults for keywords ****

    IF NOT (KEYWORD_SET(font)) THEN font = ''

		;**** Create an information widget ****

    widget_control, /hourglass
    base = widget_base(/column, xoffset=300, yoffset=200,               $
    title = "ADAS204: INFORMATION")
    lab0 = widget_label(base, value='',font=font)
    np = strcompress(string(ncalc),/remove_all)
    lab1 = widget_label(base,                                           $
;    value="    ADAS204 COMPUTATION UNDERWAY (PART 1/"+np+") - PLEASE WAIT    ")
    value="    ADAS204 COMPUTATION UNDERWAY - PLEASE WAIT    ",font=font)
    grap = widget_draw(base, ysize=20, xsize=560)
    lab2 = widget_label(base,                                           $
    value = "CALCULATING",font=font)
    dynlabel, base

    widget_control, base, /realize

    npass=0
    num = 0
AGAIN:
		;**** Read in number of signals we are expecting each pass ****
    readf, pipe, num
    if num lt 0 then goto, DONE

    npass=npass+1
;    widget_control, lab1, set_value="    ADAS204 COMPUTATION UNDERWAY (PART "$
;	+strcompress(string(npass),/remove_all)+"/"+np+") - PLEASE WAIT    "
    widget_control, lab1, set_value="    ADAS204 COMPUTATION UNDERWAY  - PLEASE WAIT    "	
    erase
		;**** Update information widget ****

    next = 0
    step = 1000.0/num
		;**** Scale time in an ad hoc exponential fashion      ****
		;**** to account for the larger number of missing      ****
		;**** transitions as the parent index is increased.    ****
		;**** b should be changed if this is giving rediculous ****
		;**** rates but it works OK for clike/clike_o2ls.dat.  ****
		;**** Its magnitude should be increased to slow down   ****
		;**** the apparent processing time at later times.     ****
;    b = -4.0/num
;    c = 1.0/(1-exp(num*b))
    for i=0,num-1 do begin
    	readf, pipe, next
	p = float(i)/num*1000	;**** This would give a linear increase in
	q = p+step		;**** loop time but not processing time
;	p = (1-exp(i*b))*c*1000
;	q = (1-exp((i+1)*b))*c*1000
        for j=fix(p),fix(q) do begin
            x = (float(j)+1)/1000.0
            plots, [x, x],[0.0,1.0], /normal
        endfor
        widget_control, lab2, set_value="PROCESSING "+		$
		string(fix(q/10),format='(I3)')+"% COMPLETED"
    endfor

    goto, AGAIN

PIPE_ERR:
    mess = ['Error communicating with adas204 Fortran.',$
	    'The Fortran calculation has crashed.',$
	    'If, after consulting the manual, you can think',$
	    'of no good reason why this should have happened',$
	    'then please report the error to adas@adas.ac.uk']
    action = popup(message=mess, title = '*** ADAS204 Error ***',$
		 buttons=['OK'])

DONE:
		;**** Destroy the information widget ****

    widget_control, base, /destroy

END
