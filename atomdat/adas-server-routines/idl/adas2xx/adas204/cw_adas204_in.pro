; Copyright (c) 1996, Strathclyde University
; SCCS info: Module @(#)$Header: /home/adascvs/idl/adas2xx/adas204/cw_adas204_in.pro,v 1.1 2004/07/06 12:30:11 whitefor Exp $ Date $Date: 2004/07/06 12:30:11 $
;+
; PROJECT:
;       ADAS IBM MVS to UNIX conversion
;
; NAME:
;	CW_ADAS204_IN()
;
; PURPOSE:
;	Data file selection for a single input dataset.
;
; EXPLANATION:
;	This function creates a compound widget consisting of the compound
;	widget cw_adas_infile.pro, an editable text widget giving the
;	corresponding cross reference file, a 'Cancel' button, a 'Done'
;	button and
;	a button to allow the browsing of the selected data file comments.
;	The browsing and done buttons are automatically de-sensitised
;	until a valid input dataset has been selected.
;
;	The value of this widget is a structure consisting of the x-ref
;	filename and the settings structure of the
;	cw_adas_infile widget.  This widget only generates events
;	when either the 'Done' or 'Cancel' buttons are pressed.
;	The event structure returned is;
;	{ID:0L, TOP:0L, HANDLER:0L, ACTION:''}
;	ACTION has one of two values 'Done' or 'Cancel'.
;
; USE:
;	See routine adas204_in.pro for an example.
;
; INPUTS:
;	PARENT	- Long integer; the ID of the parent widget.
;
; OPTIONAL INPUTS:
;	None
;
; OUTPUTS:
;	This function returns the ID of the compound widget created.
;
; OPTIONAL OUTPUTS:
;	None
;
; KEYWORD PARAMETERS:
;	VALUE	- A structure which determines the initial settings of
;                 the widget.  The
;		  structure must be;
;		  {DSNXR:'', ROOTPATH:'', FILE:'', CENTROOT:'', USERROOT:'' }
;		  The elements of the structure are as follows;
;
;		  DSNXR    - Name of the cross reference file
;		  ROOTPATH - Current data directory e.g '/usr/fred/adas/'
;		  FILE     - Current data file in ROOTPATH e.g 'input.dat'
;		  CENTROOT - Default central data store e.g '/usr/adas/'
;		  USERROOT - Default user data store e.g '/usr/fred/adas/'
;
;		  The data file selected by the user is obtained by
;		  appending ROOTPATH and FILE.  In the above example
;		  the full name of the data file is;
;		  /usr/fred/adas/input.dat
;
;		  Path names may be supplied with or without the trailing
;		  '/'.  The underlying routines add this character where
;		  required so that USERROOT will always end in '/' on
;		  output.
;
;		  The default value is;
;		  {DSNXR:'', ROOTPATH:'./', FILE:'', CENTROOT:'', USERROOT:''}
;		  i.e ROOTPATH is set to the user's current directory.
;
;	TITLE	- The title to be included in the input file widget, used
;                 to indicate exactly what the required input dataset is,
;                 e.g 'Input COPASE Dataset'
;
;	UVALUE	- A user value for the widget.  Defaults to 0.
;
;	FONT	- Supplies the font to be used for the interface widgets.
;		  Defaults to the current system font.
;
; CALLS:
;	XXTEXT		Pop-up window to browse dataset comments.
;	CW_ADAS_INFILE	Dataset selection widget.
;	FILE_ACC	Determine filetype and access permissions.
;
;	See side effects for other related widget management routines.
;
; SIDE EFFECTS:
;
;	IN204_GET_VAL()	Widget management routine in this file.
;	IN204_EVENT()	Widget management routine in this file.
;
; CATEGORY:
;	Compound Widget
;
; WRITTEN:
;       William Osborn, Tessella Support Services plc, 09-Aug-1996
;
; MODIFIED:
;       1.1     William Osborn
;               First release.
;
; VERSION:
;       1.1     09-08-96
;
;-
;-----------------------------------------------------------------------------

FUNCTION in204_get_val, id

		;**** Retrieve the state ****
  parent = widget_info(id, /parent)
  widget_control, parent, get_uvalue=state, /no_copy

                ;**** Return to caller on error ****
;  ON_ERROR, 2

		;**** Get settings ****
  widget_control,state.fileid,get_value=inset
  widget_control, state.dsnxrid, get_value=dsnxr

                ;**** Save the state ****
  widget_control, parent, set_uvalue=state, /no_copy

  os = {in204_set,							$
	dsnxr:dsnxr(0), ROOTPATH:inset.rootpath, FILE:inset.file,	$
	CENTROOT:inset.centroot, USERROOT:inset.userroot }

  RETURN, os

END

;-----------------------------------------------------------------------------

FUNCTION in204_event, event

                ;**** Base ID of compound widget ****
  parent=event.top

                ;**** Retrieve the state ****
  widget_control, parent, get_uvalue=state, /no_copy

		;*********************************
		;**** Clear previous messages ****
		;*********************************
  widget_control,state.messid,set_value=' '

		;**** Default output no event ****
  new_event = 0L
                ;************************
                ;**** Process events ****
                ;************************
  CASE event.id OF

		;***********************************
		;**** Event from file selection ****
		;***********************************
    state.fileid: begin
	  if event.action eq 'newfile' then begin
	    widget_control,state.doneid,/sensitive
	    widget_control,state.browseid,/sensitive
	    widget_control, state.fileid, get_value=val
		;**** Construct the cross reference filename ****
            z = strpos(val.file,'/')
	    a = strpos(val.file,'#',z+1)
	    b = strpos(val.file,'_',a+1)
	    c = strpos(val.rootpath,'adf')
	    openr, unit, val.rootpath+val.file, /get_lun
            s=''
	    readf, unit, s, format='(1A80)'
	    close, unit
	    d=strpos(s,"'",strpos(s,'XRMEMB'))
            e=strpos(s,"'",d+1)
	    if d ne -1 and e ne -1 then begin
            	dsnxr = strmid(val.rootpath,0,c-1)+'/adf18/a09_p204/nrb93#'+     $
	     	  strmid(val.file,a+1,b-a-1)+'/nrb93#'+strmid(val.file,a+1,b-a-1)+$
		  '_*.dat'
;		  '_'+strcompress(strmid(s,d+1,e-d-1),/remove_all)+'.dat'
	        widget_control, state.dsnxrid, set_value=dsnxr
	    endif else begin
		widget_control, state.messid, set_value="*** Error: Not a valid input file. ***"
	    	widget_control,state.doneid,sensitive=0
	    	widget_control,state.browseid,sensitive=0
	    endelse
	  end else begin
	    widget_control,state.doneid,sensitive=0
	    widget_control,state.browseid,sensitive=0
	  end
	end

		;***********************
		;**** Browse button ****
		;***********************
    state.browseid: begin
		;**** Get latest filename ****
	  widget_control,state.fileid,get_value=inset
	  filename = inset.rootpath+inset.file

		;**** Invoke comments browsing ****
	  xxtext, filename, font=state.font
	end

		;***********************
		;**** Cancel button ****
		;***********************
    state.cancelid: new_event = {ID:parent, TOP:event.top, $
				HANDLER:0L, ACTION:'Cancel'}

		;*********************
		;**** Done button ****
		;*********************
    state.doneid: begin

		;**** Check that the cross-reference file exists ****
	  widget_control, state.dsnxrid, get_value=dsnxr
	  file_acc, dsnxr(0), exist, read, write, execute, filetype
	  if exist eq 0 then begin
	    widget_control, state.messid, set_value="**** Error:"+$
			" Cross-reference file does not exist ****"
	  endif else if read eq 0 then begin
	    widget_control, state.messid, set_value="**** Error:"+$
			" No read permission for cross-reference file ****"
	  endif else if filetype eq 'd' then begin
	    widget_control, state.messid, set_value="**** Error:"+$
			" Cross-reference file is a directory ****"
	  endif else if strpos(dsnxr(0),'*') lt 0 then begin
	    widget_control, state.messid, set_value="**** Error:"+$
			" Cross-reference file name must have an * in it ****"
	  endif else begin
	    new_event = {ID:parent, TOP:event.top, HANDLER:0L, ACTION:'Done'}
	  endelse
        end

    ELSE:

  ENDCASE

		;**** Save the state ****

  widget_control, parent, set_uvalue=state, /no_copy

  RETURN, new_event
END

;-----------------------------------------------------------------------------

FUNCTION cw_adas204_in, parent, $
		VALUE=value, TITLE=title, UVALUE=uvalue, FONT=font


  IF (N_PARAMS() EQ 0) THEN MESSAGE, 'Must specify parent for cw_adas_in'
;  ON_ERROR, 2					;return to caller

		;**** Set defaults for keywords ****
  IF NOT (KEYWORD_SET(value)) THEN begin
    inset = {   ROOTPATH:'./', FILE:'', $
		CENTROOT:'', USERROOT:'' }
    dsnxr = ''
  END ELSE BEGIN
    inset = {   ROOTPATH:value.rootpath, FILE:value.file, $
		CENTROOT:value.centroot, USERROOT:value.userroot }
    dsnxr = value.dsnxr
    if strtrim(inset.rootpath) eq '' then begin
      inset.rootpath = './'
    end else if $
	    strmid(inset.rootpath,strlen(inset.rootpath)-1,1) ne '/' then begin
      inset.rootpath = inset.rootpath+'/'
    end
    if strmid(inset.file,0,1) eq '/' then begin
      inset.file = strmid(inset.file,1,strlen(inset.file)-1)
    end
  END
  IF NOT (KEYWORD_SET(title)) THEN title = ''
  IF NOT (KEYWORD_SET(uvalue)) THEN uvalue = 0
  IF NOT (KEYWORD_SET(font)) THEN font = ''

		;*********************************
		;**** Create the Input widget ****
		;*********************************

		;**** create base widget ****
  cwid = widget_base( parent, UVALUE = uvalue, $
			EVENT_FUNC = "in204_event", $
			FUNC_GET_VALUE = "in204_get_val", $
			/COLUMN)

		;*************************************
		;**** Input file selection widget ****
		;*************************************
  fileid = cw_adas_infile(cwid,value=inset,title=title,font=font)

		;******************************************
		;**** Cross reference file name widget ****
		;******************************************

  base = widget_base(cwid, /column, /frame)
  lab = widget_label(base, value='Cross-reference file name',font=font, xsize=800)
  dsnxrid = widget_text(base, value=dsnxr, /editable, font=font)

		;**** Error message ****
  messid = widget_label(cwid,font=font,value=' ')

		;*****************
		;**** Buttons ****
		;*****************
  base = widget_base(cwid,/row)

		;**** Browse Dataset button ****
  browseid = widget_button(base,value='Browse Comments',font=font)

		;**** Cancel Button ****
  cancelid = widget_button(base,value='Cancel',font=font)

		;**** Done Button ****
  doneid = widget_button(base,value='Done',font=font)

		;**** Check filename and desnsitise buttons if it ****
		;**** is a directory or it is a file without read ****
		;**** access.					  ****
  filename = inset.rootpath+inset.file
  file_acc,filename,fileexist,read,write,execute,filetype
  if filetype ne '-' then begin
    widget_control,browseid,sensitive=0
    widget_control,doneid,sensitive=0
  end else begin
    if read eq 0 then begin
      widget_control,browseid,sensitive=0
      widget_control,doneid,sensitive=0
    end
  end

		;**** create a state structure for the widget ****
  new_state = { FILEID:fileid, BROWSEID:browseid, $
		CANCELID:cancelid, DONEID:doneid, MESSID:messid, $
		DSNXRID:dsnxrid, FONT:font }

                ;**** Save initial state structure ****
  widget_control, parent, set_uvalue=new_state, /no_copy

  RETURN, cwid

END

