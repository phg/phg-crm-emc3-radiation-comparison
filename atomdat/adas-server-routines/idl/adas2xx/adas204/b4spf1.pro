; Copyright (c) 1996, Strathclyde University.
; SCCS Info: Module @(#)$Header: /home/adascvs/idl/adas2xx/adas204/b4spf1.pro,v 1.7 2004/07/06 11:22:47 whitefor Exp $ Date $Date: 2004/07/06 11:22:47 $
;
;+
; PROJECT:
;       ADAS IBM MVS to UNIX conversion
;
; NAME:
;	B4SPF1
;
; PURPOSE:
;	IDL user interface and communications with ADAS204 FORTRAN
;	process via pipe.
;
; EXPLANATION:
;	In this routine the 'Output Options' part of the
;	interface is invoked.  The resulting action of the user's
;	is written to the FORTRAN via the pipe.
;	Communications are with the FORTRAN subroutine B4SPFX.
;	Note that the usual large amount of FORTRAN-IDL pipe 
;	communications in these routines has been removed due to the
;	fact that there are two FORTRAN routines rather than the usual
;	one and the communications are therefore handled elsewhere.
;
; USE:
;	The use of this routine is specific to ADAS204, See adas204.pro.
;
; INPUTS:
;	PIPE	- The IDL unit number of the bi-directional pipe to the
;		  ADAS204 FORTRAN process.
;
;	DSFULL  - The name of the data set being analyzed.
;
;	VALUE	- A structure which determines the initial settings of
;		  the dataset selection widget.  The initial value is
;		  set in adas204.pro.  VALUE is passed un-modified
;		  through to cw_adas204_out.pro, see that routine for a full
;		  description.
;
; OPTIONAL INPUTS:
;	None.
;
; OUTPUTS:
;	LPEND	- Integer; indicates user action on leaving output
;		  selections.  lpend is set to 1 if the user exited
;		  with the 'Cancel' button, 0 if the user exited
;		  with 'Run Now' and 2 if the user exited with 'Run in 
;		  Batch'.
;
;	VALUE	- On output the structure records the final settings of
;		  the output selection widget if the user pressed either
;		  of the 'Run' buttons otherwise it is not changed from input.
;
; OPTIONAL OUTPUTS:
;	None.
;
; KEYWORD PARAMETERS:
;
;	FONT	- Supplies the font to be used for the interface widgets.
;
; CALLS:
;	ADAS204_OUT	User interface - output options.
;
; SIDE EFFECTS:
;	This routine communicates with the ADAS204 FORTRAN process
;	via a UNIX pipe.
;
; CATEGORY:
;	Adas system.
;
; WRITTEN:
;       William Osborn, Tessella Support Services plc, 09-Aug-1996
;
; MODIFIED:
;	1.1	William Osborn
;		First release.
;	1.2	William Osborn
;		Put in message giving the passing files
;	1.3	William Osborn
;		Minor error - adas203.pass1 -> adas204.pass1
;	1.4	Richard Martin
;		Removed 'adas204,pass?' from popup message.
;	1.5	Martin O'Mullane
;		Modified output settings
;	1.6	Richard Martin
;		Corrected SCCS error
;	1.7	Richard Martin
;		Added font keyword on call to popup.
;
; VERSION:
;	1.1	09-08-96
;	1.2	30-08-96
;	1.3	10-09-96
;	1.4	05-03-98
;	1.5   03-12-98
;	1.6   16-03-98
;	1.7	20-09-99
;-
;-----------------------------------------------------------------------------

PRO   b4spf1, pipe, lpend, value, dsfull, FONT=font

                ;**** Return to caller on error ****

    ON_ERROR, 2

                ;**** Set defaults for keywords ****

    IF NOT (KEYWORD_SET(font)) THEN font = ''


		;**************************************
		;**** Pop-up output options widget ****
		;**************************************

    adas204_out, value, dsfull, action, FONT=font

		;*************************************************
		;**** Act on the output from the widget       ****
		;**** There are three    possible actions     ****
		;**** 'Cancel', 'Run Now' and 'Run in Batch'. ****
		;*************************************************

    if action eq 'Run Now' then begin
        lpend = 0
    endif else if action eq 'Run In Batch' then begin
        lpend = 2
    endif else begin
        lpend = 1
    end

    if lpend ne 1 then begin

		;************************************************
		;**** Inform the user of which passing files ****
		;**** are being output                       ****
		;************************************************

        mess = ['The following passing files will be created in',	$
		'the directory '+value.passdir+'  :',$
		'cbnm.pass','cbnmpr.pass','acd.pass',	$
		'scd.pass','prb.pass','xcd.pass','pcasbin.pass']

	action = popup(message=mess, title = 'ADAS204 Message', buttons=['OK'],$
			font=font)

     endif
		;*******************************
		;**** Write data to fortran ****
		;*******************************

    printf, pipe, lpend
    if lpend ne 1 then begin
    	printf, pipe, value.passdir
    endif

END
