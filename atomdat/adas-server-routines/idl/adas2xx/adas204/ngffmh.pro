;----------------------------------------------------------------------
;+
; PROJECT    :  ADAS
;
; NAME       :  ngffmh
;
; PURPOSE    :  Evaluates electron temperature and frequency averaged
;               hydrogenic free free Gaunt factor.
;
; ARGUMENTS  :  All output arguments will be defined appropriately.
;               Inputs will be converted to correct type, if possible,
;               internally without changing calling type.
;                   gam2   = z^2*ih/te
;                   result = ngffmh(gam2)
;
;               NAME      I/O    TYPE    DETAILS
; REQUIRED   :  gam2       I     double  Z0*Z0*IH/KTE
;
; KEYWORDS      None
;
;
; NOTES      :  Calls fortran code.
;
;               Obtained from interpolation of Karzas & Latter (1959) Fig. 6
;               For -3<log10(z0*z0*ih/kte)<1. Outside this range a very
;               approximate extrapolation is performed with gffmh=1 in
;               the infinite limits.
;
;
; AUTHOR     :  Martin O'Mullane
;
; DATE       :  24-07-2006
;
;
; MODIFIED:
;         1.1    Martin O'Mullane
;                 - First version.
;         1.2    Martin O'Mullane
;                 - Improve comments.
;
; VERSION:
;         1.1   24-07-2006
;         1.2   03-12-2013
;-
;----------------------------------------------------------------------

FUNCTION ngffmh, gam2


fortdir = getenv('ADASFORT')
fortdir = fortdir

if n_elements(gam2) EQ 0 then message, 'argument is missing'

; inputs

num     = n_elements(gam2)
gam2_in = double(gam2)

; results

rate = dblarr(num)

dummy = CALL_EXTERNAL(fortdir+'/ngffmh_if.so','ngffmh_if', $
                      num, gam2_in, rate)

return, rate

END
