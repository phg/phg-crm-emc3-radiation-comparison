; Copyright (c) 2003 Strathclyde University.
;----------------------------------------------------------------------
;+
; PROJECT    :  ADAS
;
; NAME       :  run_adas204
;
; PURPOSE    :  Runs ADAS204 collisional-radiative population code as an IDL
;               subroutine.
;
; ARGUMENTS  :  All output arguments will be defined appropriately.
;
;               NAME      I/O    TYPE   DETAILS
; INPUTS     :  adf25      I     str    input driver file.
;               adf17      I     str    output projection file.
;               popfile    I     str    output population file.
;               log        I     str    name of output text file.
;                                       (defaults to no output).
;
; KEYWORDS   :  progress - put up a progesss bar if true
;
; OUTPUTS    :  None
;
; NOTES      :  - run_adas204 used the spawn command. Therefore IDL v5.3 
;                 cannot be used. Any other version will work.
;
; AUTHOR     :  Martin O'Mullane
; 
; DATE       :  31-03-2003
; 
; UPDATE     :  
;       1.2     Martin O'Mullane
;                 - Add popfile for optional population output. 
;                 - Added /help keyword.
;
; VERSION    :
;       1.1    31-03-2003
;       1.2    23-05-2005
;-
;----------------------------------------------------------------------

PRO run_adas204, adf25    = adf25,    $
                 adf17    = adf17,    $
                 popfile  = popfile,  $
                 log      = log,      $
                 progress = progress, $
                 help     = help


; If asked for help

if keyword_set(help) then begin
   doc_library, 'run_adas204'
   return
endif

; First check that we are not using IDL v5.3

if !version.release EQ '5.3' then begin
   message, 'This version of IDL (v5.3) will not work - ' + $
            'change to v5.0, v5.1, v5.2 or v5.4'
   return
endif


; Check that we get all inputs and that they are correct. Otherwise print
; a message and return to command line.

on_error, 2

if n_elements(adf25) EQ 0 then begin
    message,'A driver file (adf25) is required'
endif 

; Suitable defaults

if n_elements(adf17) EQ 0 then begin 
   adf17 = 'run_adas204_adf17.pass'
   message, 'A default projection file will be used : ' + adf17, /continue
endif


; File outputs

if n_elements(log) EQ 0 then begin
   output_file = '' 
   is_log = 0
endif else begin
   output_file = log
   is_log = 1
endelse

if n_elements(popfile) EQ 0 then begin
   pop_file = '' 
   is_popfile = 0
endif else begin
   pop_file = popfile
   is_popfile = 1
endelse


; Now run the ADAS204 code

; location of files etc.

    fortdir = getenv("ADASFORT")
    adas_c  = getenv("ADASCENT")
    adas_u  = getenv("ADASUSER")

; Input screen

    spawn, fortdir+'/adas204.out', unit=pipe, /noshell, PID=pid
    
    ibatch = 0
    printf, pipe, ibatch
    
    date = xxdate()
    printf, pipe, date[0]
      
    rep = 'NO'
    printf, pipe, rep
    printf, pipe, adf25
    printf, pipe, adas_c
    printf, pipe, adas_u
    
; Run calculation and output results   
    
    file_names    = strarr(9)
    file_names[0] = output_file
    file_names[1] = adf17
    file_names[8] = pop_file
    
    file_open = intarr(9)
    if is_log then file_open[0] = 1
    if is_popfile then file_open[8] = 1
    file_open[1] = 1
    
    printf, pipe, 0
    for j=0, n_elements(file_names)-1 do begin
       printf, pipe, file_names[j]
       printf, pipe, file_open[j]
    endfor

    if keyword_set(progress) then begin
    
       ncalc = 1
       b4spf2, pipe, pid, ncalc
 
    endif else begin
    
       AGAIN:
              readf, pipe, num
              if num lt 0 then goto, DONE
              for i=0,num-1 do readf, pipe, next
              goto, AGAIN
       DONE:       
       
    endelse
    
; Terminate FORTRAN process and close up everything open
    
    close, pipe
    close, /all               

END
