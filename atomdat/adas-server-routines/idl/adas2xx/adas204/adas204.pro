; Copyright (c) 1996, Strathclyde University.
;+
; PROJECT:
;       ADAS IBM MVS to UNIX conversion
;
; NAME:
;	ADAS204
;
; PURPOSE:
;	The highest level routine for the ADAS 204 program.
;
; EXPLANATION:
;	This routine is called from the main adas system routine, adas.pro,
;	to start the ADAS 204 application.  Associated with adas204.pro
;	is a FORTRAN executable, adas204.out. The IDL code provides the user
;	interface and output graphics whilst the FORTRAN code reads
;	in data files, performs numerical processing and creates the
;	output files.  The IDL code communicates with the FORTRAN
;	executable via a bi-directional UNIX pipe.  The unit number
;	used by the IDL for writing to and reading from this pipe is
;	allocated when the FORTRAN executable is 'spawned' (see code
;	below).  Pipe communications in the FORTRAN process are to
;	stdin and stdout, i.e streams 5 and 6.
;
;	The FORTRAN code is an independent process under the UNIX system.
;	The IDL process can only exert control over the FORTRAN in the
;	data which it communicates via the pipe.  The communications
;	between the IDL and FORTRAN must be exactly matched to avoid
;	input conversion errors.  The correct ammounts of data must be
;	passed so that neither process 'hangs' waiting for communications
;	which will never occur.
;
;	The FORTRAN code performs some error checking which is
;	independent of IDL.  In cases of error the FORTRAN may write
;	error messages.  To prevent these error messages from conflicting
;	with the pipe communications all FORTRAN errors are written to
;	output stream 0, which is stderr for UNIX.  These error messages
;	will appear in the window from which the ADAS session/IDL session
;	is being run. In the case of the batch job, the error messages
;	will appear as standard cron job output which is usually
;	emailed to the user. If the job has completed succesfully then
;	the user will get a message telling them so.
;
;	In the case of severe errors the FORTRAN code may terminate
;	itself prematurely.  In order to detect this, and prevent the
;	IDL program from 'hanging' or crashing, the IDL checks to see
;	if the FORTRAN executable is still an active process before
;	each group of pipe communications.  The process identifier
;	for the FORTRAN process, PID, is recorded when the process is
;	first 'spawned'.  The system is then checked for the presence
;	of the FORTRAN PID. This checking does not occur for the 
;	batch cases.
;
; USE:
;	First the system settings must be established by calling
;	adas_sys_set.pro then adas204.pro is called to start the
;	ADAS 204 application;
;
;	adas_sys_set, adasrel, fortdir, userroot, centroot, 		$
;		      devlist, devcode, font_large, font_small, edit_fonts
;	adas204,   adasrel, fortdir, userroot, centroot, devlist, 	$
;		   devcode, font_large, font_small, edit_fonts
;
; INPUTS:
;	ADASREL - A string indicating the ADAS system version, 
;		  e.g ' ADAS RELEASE: ADAS93 V1.1.0'.  The first
;		  character should be a space.
;
;	FORTDIR - A string holding the path to the directory where the
;		  FORTRAN executables are, e.g '/disk/adas/fortran/exec'
;
;	USERROOT - A string holding the path to the root directory of
;		   the user's adas data. e.g '/disk/bowen/adas'
;		   This root directory will be used by adas to construct
;		   other path names.  For example the users default data
;		   for adas205 should be in /disk/bowen/adas/adf04.  In
;		   particular the user's default interface settings will
;		   be stored in the directory USERROOT+'/defaults'.  An
;		   error will occur if the defaults directory does not
;		   exist.
;
;	CENTROOT - Like USERROOT, but this directory points to the
;		   central data area for the system.  User defaults are
;		   not stored on CENTROOT.
;
;	DEVLIST - A string array of hardcopy device names, used for
;		  graphical output. e.g ['Post-Script','HP-PCL','HP-GL']
;		  This array must mirror DEVCODE.  DEVCODE holds the
;		  actual device names used in a SET_PLOT statement.
;
;	DEVCODE - A string array of hardcopy device code names used in
;		  the SET_PLOT statement, e.g ['PS', 'PCL', 'HP']
;		  This array must mirror DEVLIST.
;
;	FONT_LARGE - The name of a larger font e.g 'courier_bold14'
;
;	FONT_SMALL - The name of a smaller font e.g 'courier_bold12'
;
;	EDIT_FONTS - A structure of two fonts used in the table editor
;		     adas_edtab.pro, {font_norm:'',font_input:''}.
;		     The two fonts are used to differentiate between
;		     editable and non-editable parts of the table. You
;		     may have to experiment to get two fonts which give
;		     the same default spacing in the edit widget.
;		     e.g {font_norm:'helvetica_bold14', $
;			  font_input:'helvetica_oblique14'}
;
; OPTIONAL INPUTS:
;	None
;
; OUTPUTS:
;	None - Note this routine should leave all inputs un-modified.
;
; OPTIONAL OUTPUTS:
;	None
;
; KEYWORD PARAMETERS:
;	None
;
; CALLS:
;	FIND_PROCESS	Checks to see if a given process is active.
;       XXDATE          Get date and time from operating system.
;       B4SPF0          Pipe comms with FORTRAN B4sPF0 routine.
;       B4SPF1          Pipe comms with FORTRAN B4sPF1 routine.
;	B4SPF2		Updates the batch files and variables for
;			the batch runs depending on the user selections.
;
; SIDE EFFECTS:
;	This routine spawns FORTRAN executables.  Note the pipe 
;	communications routines listed above. 
;
;       From v1.10 we require IDL v5. 
;
; CATEGORY:
;	Adas system.
;	
; WRITTEN:
;       William Osborn, Tessella Support Services plc, 31/07/1996
;
; MODIFIED:
;	1.1	William Osborn	
;		First release
;	1.2	William Osborn	
;		Added at command for SunOS version >= 5.3
;	1.3	William Osborn	
;		Added call to batch.pro rather than having code for
;		each UNIX flavour present in this routine.
;	1.4	William Osborn	
;		Added write of inval.rootpath to infunit for batch execution
;	1.5	William Osborn	
;		Increased version number to 1.2
;	1.6	Richard Martin
;		Increased version number to 1.3
;	1.7	Richard Martin
;		Increased version number to 1.4
;	1.8	Martin O'Mullane
;		Moved ionisation and DR cross reference files into the 
;               adf25 driver. Changes are b4spfx being replaced by b4spf0
;               and a general simplification of user input. The specialised
;               204 specific routines adas204_in and cw_adas204_in are no
;               longer needed. 
;		Increased version no. to 1.5
;	1.9	Martin O'Mullane
;               Added b4check_obsolete to check that the input adf25 files
;               conform to the new specification. A message is popped up
;               to tell the user what to change. The program is also
;               terminated.
;		Increased version no to 1.6
;	1.10	Martin O'Mullane
;		  - Make the choice of output files more selective. A new
;                   version of adas204_out.pro and some new logic was needed.
;		  - Increased version no to 1.7 .
;	1.11	Martin O'Mullane
;		  - Add another optional output file for populations.
;     
;
; VERSION:
;       1.1	31-07-96
;	1.2	11-09-96
;	1.3	18-10-96
;	1.4	22-10-96
;	1.5	25-11-96
;	1.6	04-04-97
;	1.7	05-03-98
;	1.8	07-12-98
;	1.9	27-07-99
;	1.10	10-09-2002
;	1.11	23-05-2005
;			
;-----------------------------------------------------------------------------


PRO ADAS204,	adasrel, fortdir, userroot, centroot, 			$
		devlist, devcode, font_large, font_small, edit_fonts



                ;***********************************************
                ;**** Make sure we are running v5 or above. ****
                ;***********************************************

    thisRelease = StrMid(!Version.Release, 0, 1)
    IF thisRelease LT '5' THEN BEGIN
       message = 'Sorry, ADAS807 requires IDL 5 or higher' 
       tmp = popup(message=message, buttons=['Accept'],font=font_large)
       goto, LABELEND
    ENDIF

		;************************
		;**** Initialisation ****
		;************************

    adasprog = ' PROGRAM: ADAS204 V1.7'
    lpend = 0
    ipset = 0
    ipbatset = 0
    l2batch = 0				;Marker for whether a batch file
					;has been created.
                                        
    deffile = userroot+'/defaults/adas204_defaults.dat'
    device  = ''
    bitfile = centroot + '/bitmaps'

		;**** Get environment for ioniz and DR supplement files ****
    
    adas_c     = getenv("ADASCENT")
    adas_u     = getenv("ADASUSER")

		;******************************************
		;**** Search for user default settings ****
		;**** If not found create defaults     ****
		;******************************************

    files = findfile(deffile)
    if files(0) eq deffile then begin

        restore,deffile
        inval.centroot = centroot+'/adf25/'
        inval.userroot = userroot+'/adf25/'

    endif else begin

        inval = { ROOTPATH   :   userroot+'/adf25/', 	$
		  FILE       :    '', 			$
		  CENTROOT   :   centroot+'/adf25/', 	$
		  USERROOT   :   userroot+'/adf25/'     }

        outval  = { TEXOUT      :   0,                                   $
                    TEXAPP      :   -1,                                  $
                    TEXREP      :   1,                                   $
                    TEXDSN      :   '',                                  $
                    TEXDEF      :   'paper.txt',                         $
                    TEXMES      :   ' ',                                 $
                    a17OUT      :   1,                                   $
                    a17APP      :   -1,                                  $
                    a17REP      :   1,                                   $
                    a17DSN      :   userroot+'/pass/adas204_adf17.pass', $
                    a17DEF      :   userroot+'/pass/adas204_adf17.pass', $
                    a17MES      :   ' ',                                 $
                    opfOUT      :   intarr(7),                           $
                    opfAPP      :   intarr(7) -1,                        $
                    opfREP      :   intarr(7) +1,                        $
                    opfDSN      :   strarr(7) + ' ',                     $
                    opfDEF      :   strarr(7) + ' ',                     $
                    opfMES      :   strarr(7) + ' '                      }
                    
       outval.opfdsn[0] = userroot+'/pass/cbnmpr.pass'
       outval.opfdsn[1] = userroot+'/pass/adas204_adf10_acd.pass'
       outval.opfdsn[2] = userroot+'/pass/adas204_adf10_scd.pass'
       outval.opfdsn[3] = userroot+'/pass/adas204_adf10_xcd.pass'
       outval.opfdsn[4] = userroot+'/pass/adas204_adf10_prb.pass'
       outval.opfdsn[5] = userroot+'/pass/adas204_adf10_pcasbin.pass'
       outval.opfdsn[6] = userroot+'/pass/adas204_pop.pass'
    
    endelse


		;****************************
		;**** Start fortran code ****
		;****************************
    
    spawn, fortdir+'/adas204.out', unit=pipe, /noshell, PID=pid


		;************************************************
		;**** Write to fortran via pipe that we are  ****
		;**** running interactively                  ****
		;************************************************

    ibatch = 0
    printf, pipe, ibatch

		;******************
		;**** Get date ****
		;******************

    date = xxdate()
    printf, pipe, date(0)

LABEL100:
		;**** Check FORTRAN still running ****
  
    if find_process(pid) eq 0 then goto, LABELEND

		;************************************************
		;**** Communicate with b4spfx in fortran and ****
		;**** invoke user interface widget for       ****
		;**** Data file selection                    ****
		;************************************************

    b4spf0, pipe, inval, rep, adas_c, adas_u, dsfull,  ncalc, FONT=font_large

    b4check_obsolete, dsfull, rep_obs, FONT=font_large
    if rep_obs eq 'YES' then goto, LABELEND
    
    ipset = 0
    ipbatset = 0
    
		;**** If cancel selected then end program ****

    if rep eq 'YES' then goto, LABELEND


LABEL300:
		;**** Check FORTRAN still running ****

    if find_process(pid) eq 0 then goto, LABELEND


    rep = adas204_out(outval, bitfile, FONT_LARGE = font_large, $
                  file_names, file_open,                        $
                  font_small=font_small)

    if rep EQ 'CANCEL' then begin
    
       printf, pipe, 1
       goto, LABEL100
    
    endif 
    
    if rep EQ 'MENU' then begin
    
       printf, pipe, 1
       goto, LABELEND
    
    endif 

    date   = xxdate()
    header = adasrel+adasprog+' DATE: '+date(0)+' TIME: '+date(1)
    
		;***********************************************
		;**** Now either begin the calculation or   ****
		;**** set up the batch file depending on    ****
		;**** what action has been requested.  	    ****
                ;***********************************************

    if rep EQ 'RUN' then begin
    
       printf, pipe, 0

       for j=0, n_elements(file_names)-1 do begin
          printf, pipe, file_names[j]
          printf, pipe, file_open[j]
       endfor
          
       ipset = ipset + 1			   ; Increase 'run number'
       b4spf2, pipe, pid, ncalc, FONT=font_large
       
    endif
    
    
    
    if rep EQ 'BATCH' then begin
    
       printf, pipe, 2
       for j=0, n_elements(file_names)-1 do begin
          printf, pipe, file_names[j]
          printf, pipe, file_open[j]
       endfor

       ipbatset = ipbatset + 1                 ;Increase run number

       ;*******************************************************************
       ;****                   Set the batch job up                    ****
       ;**** control####.tmp is a UNIX script file holding the commands****
       ;**** to set the batch job going.                               ****
       ;**** info####.tmp is the file used as standard input to        ****
       ;**** adas204.out.                                              ****
       ;*******************************************************************

       rand = strmid(strtrim(string(randomu(seed)), 2), 2, 4)
       dscontrol = 'control'+rand+'.tmp'
       dsinfo = 'info'+rand+'.tmp'
       openw, conunit, dscontrol, /GET_LUN
       openw, infunit, dsinfo, /GET_LUN
       printf, conunit, fortdir+'/adas204.out < '+dsinfo 
       printf, conunit, 'rm -f '+dsinfo 
       printf, conunit, 'rm -f '+dscontrol

       ;**** Write stuff needed by adas204 - it will be read into stdin ****

       yesbatch = 1
       repn = 'NO'
       repy = 'YES'
       lpend = 0
       printf, infunit, yesbatch

       ;**** Write the date ****
       printf, infunit, date(0)

       ;**** Input screen, b4spf0, pipe info ****
       printf, infunit, repn
       printf, infunit, dsfull
       printf, infunit, adas_c
       printf, infunit, adas_u

       ;**** Output screen adas204_out pipe info ****
      
       printf, infunit, lpend
       printf, infunit, outval.texdsn
       printf, infunit, outval.texout
       printf, infunit, outval.a17dsn
       printf, infunit, outval.a17out
       for j=0, n_elements(outval.opfdsn)-1 do begin
          printf, infunit, outval.opfdsn[j]
          printf, infunit, outval.opfout[j]
       endfor

       ;**** Input screen, again ****
       printf, infunit, repy

       close, infunit, conunit
       spawn, 'chmod u+x '+dscontrol


    ; Send off batch job
    
       batch, dscontrol, title = 'ADAS204: INFORMATION', jobname='NO. '+rand

       l2batch = 0

    endif

		;**** Back for more input options ****

    GOTO, LABEL100

LABELEND:

		;**** Save user defaults and close fortran code ****

    save, inval, outval, filename=deffile

    close, /all

END
