; Copyright (c) 1996, Strathclyde University
; SCCS info: Module @(#)$Header: /home/adascvs/idl/adas2xx/adas204/check_xref_204.pro,v 1.1 2004/07/06 12:04:49 whitefor Exp $ Date $Date: 2004/07/06 12:04:49 $
;+
; PROJECT:
;       ADAS IBM MVS to UNIX conversion
;
; NAME:
;	CHECK_XREF_204
;
; PURPOSE:
;	Checks the cross reference file used by ADAS204 to see if the
;	named files exist.
;
; EXPLANATION:
;	The filename is passed from b4spf0. The first four fields are checked
;	to see whether they are valid filenames. If they are not, then
;	a warning is given.
;
; USE:
;	The use of this routine is specific to ADAS204, see adas204.pro.
;
; INPUTS:
;	DSFULL  - The filename of the cross reference file
;
; OPTIONAL INPUTS:
;	None
;
; OUTPUTS:
;	The return value indicates whether there were no errors (1) or
;       if there were errors and the user wants to cancel (0). If the user
;	selects 'Continue' on the warning box then OK=1.
;
; OPTIONAL OUTPUTS:
;	None
;
; KEYWORD PARAMETERS:
;	FONT	- Supplies the font to be used for the interface widgets.
;
; CALLS:
;	CHECK_FILE Checks the file priorities using file_acc
;	FILE_ACC  Checks file access priorities
;	POPUP	  Popup query widget
;
; SIDE EFFECTS:
;	NONE
;
; CATEGORY:
;	Adas system.
;
; WRITTEN:
;       William Osborn, Tessella Support Services plc, 20/08/1996
;
; MODIFIED:
;	1.1	William Osborn
;		First Release
; VERSION:
;	1.1	20-08-96
;
;-----------------------------------------------------------------------------

PRO check_file, file, warning, nwarn

	;**** Check for "", an environment variable
    a=strpos(file,'"')
    b=strpos(file,'"',a+1)
    if a ne -1 then begin
	var = strmid(file,a+1,b-a-1)
	varval = getenv(var)
	file = strmid(file,0,a-1)+varval+strmid(file,b+1,strlen(file))
    endif
	;**** Check for comments
    c=strpos(file,":")
    if c ne -1 then begin
	file = strcompress(strmid(file,0,c-1), /remove_all)
    endif

    file_acc, file, exist, read, write,execute, filetype
    if exist eq 0 then begin
	warning(nwarn)=file+' : does not exist'
	nwarn=nwarn+1
    endif else if read eq 0 then begin
	warning(nwarn)=file+' : you do not have read permission'
	nwarn=nwarn+1
    endif else if filetype eq 'd' then begin
	warning(nwarn)=file+' : this is a directory'
	nwarn=nwarn+1
    endif
	
END

PRO check_dir, file, warning, nwarn

	;**** Check for "", an environment variable
    a=strpos(file,'"')
    b=strpos(file,'"',a+1)
    if a ne -1 then begin
	var = strmid(file,a+1,b-a-1)
	varval = getenv(var)
	file = strmid(file,0,a-1)+varval+strmid(file,b+1,strlen(file))
    endif
	;**** Check for comments
    c=strpos(file,":")
    if c ne -1 then begin
	file = strcompress(strmid(file,0,c-1), /remove_all)
    endif
    	;**** Extract the directory name
    v=0
    while v ne -1 do begin
	pos=v
	v = strpos(file,"/",pos+1)
    endwhile
    if pos eq 0 then dir='./' else dir = strmid(file,0,pos+1)

    file_acc, dir, exist, read, write,execute, filetype
    if exist eq 0 then begin
	warning(nwarn)='Directory ' + dir + ' : does not exist'
	nwarn=nwarn+1
    endif else if write eq 0 then begin
	warning(nwarn)= 'Directory ' + dir + ' : you do not have write permission'
	nwarn=nwarn+1
    endif else if filetype ne 'd' then begin
	warning(nwarn)= dir + ' : not a directory'
	nwarn=nwarn+1
    endif
	
END

FUNCTION check_xref_204, dsfull

    ON_IOERROR, ioerr

    warning = strarr(30)  ; Take 30 as the maximum number of warnings!
    warning(0) = 'References to the following files were found in a cross-reference file:'
    nwarn=1
    filetype=''
    file=''

    openr, unit, dsfull, /get_lun
	;***************************************
	;**** Check that input files exist ****
	;***************************************
; Read dsnmc
    readf, unit, file, format='(/,/,1A120,/,/,/)
    check_file, file, warning, nwarn

; Read dsnbd
LAB1:
    readf, unit, file
    if strlen(strcompress(file, /remove_all)) gt 0 then begin
        check_file, file, warning, nwarn
	goto, LAB1
    endif
	;***************************************************
	;**** Check that output files can be written to ****
	;***************************************************
; Read dsnmco
    readf, unit, file, format='(/,/,1A120)'
    check_dir, file, warning, nwarn

    close, unit

    goto, no_ioerr

ioerr:
    nwarn = 2
    warning=['IO error', 'Is the cross reference file of the proper format?']

no_ioerr:
    if nwarn gt 1 then begin
	message=warning(0:nwarn-1)
	action = popup(message=message, title = '!!! ADAS204: Warning !!!',$
		       buttons=['Cancel','Continue'])
	if action eq 'Cancel' then return, 0   else return, 1
    endif else return, 1

END

