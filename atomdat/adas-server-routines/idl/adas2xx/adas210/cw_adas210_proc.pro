; Copyright (c) 1996, Strathclyde University.
; SCCS info: Module @(#)$Header: /home/adascvs/idl/adas2xx/adas210/cw_adas210_proc.pro,v 1.3 2004/07/06 12:34:56 whitefor Exp $ Date $Date: 2004/07/06 12:34:56 $
;+
; PROJECT:
;         UNIX IDL-ADAS development
;
; NAME:
;         CW_ADAS210_PROC
;
; PURPOSE:
;         Produces a widget for ADAS210 processing options.
;
; EXPLANATION:
;	  This function declares a compound widget consisting of a processing
;         options widget cw_bndl_gen.pro. It also includes two buttons
;         for browsing the input data sets, some information about the first
;         dataset, a 'Cancel' button , a 'Return to series menu' button
;	  and a 'Done' button.
; USE:    
;         ADAS210 Specific.
;
; INPUTS: 
;	PARENT	- Long integer; ID of parent widget.
;
;	NJLEVX2	- Thge number of levels in the 2nd dataset.
;
; OPTIONAL INPUTS:
;       None
;
; OUTPUTS:
;       The return value of this function is the ID of the declared widget.
;
; OPTIONAL OUTPUTS:
;       None        
;
; KEYWORD PARAMETERS:
;       FONT   - The name of the font to be used on the widgets.
;
;       VALUE  - A structure which determines the initial settings of the
;                processing options widget.
;                The default value is;
;                value = { dsname           : ''               ,$
;                          dtname           : ''               ,$
;                          dpname           : ''               ,$
;                          njlevx           : 0                ,$
;                          njprtx           : 0                ,$
;                          njprtx2          : 0                ,$
;                          levels           : mult             ,$
;                          levels2          : mult             ,$
;                          bndlLS           : mult             ,$
;                          prnts1           : mult             ,$
;                          prnts2           : mult             ,$
;                          bndl_prnts       : mult             ,$
;                          ion              : 0                ,$
;                          nclc             : 0                 $
;                         }
;                where mult = make_array( njlevx2, /float, value = -1.0)
;
;                See baispf.pro for an explanation of the tags in this
;                structure.
;
; CALLS:
;         CW_ADAS210_PROC_EVENT 
;              Called indirectly during widget management to handle the widget 
;              events. Procedure is included in this file.
;         CW_ADAS210_PROC_GET_VALUE
;              Widget management routine included inthis file.
;         CW_ADAS_DSBR
;              Data set browse button function.
;         CW_BNDL_GEN
;              Selection widget
;
; SIDE EFFECTS:
;         None
;
; CATEGORY:
;         Compound Widget for ADAS210
;
; WRITTEN:
;         David H.Brooks, Univ.of Strathclyde, 21-Nov-1995
;
; MODIFIED:
;	1.1	David H. Brooks
;		First version
;	1.2	Tim Hammond
;		Added font keywords to calls to cw_bndl_gen to ensure
;		correct spacing of widgets
;       1.3     William Osborn
;		Changed x_scroll_size of col4 and col5 to 650
;
; VERSION:
; 	1.1	21-11-95
;	1.2	22-01-96
;	1.3	14-10-96
;
;-
;--------------------------------------------------------------------------

FUNCTION cw_adas210_proc_get_value, id

		;**** Return to caller on error. ****

    ON_ERROR, 2

		;***********************************************
		;**** Retrieve the structure from the child ****
        	;**** that contains the sub ids.            ****
		;***********************************************

    stash = WIDGET_INFO(id, /CHILD)
    WIDGET_CONTROL, stash, GET_UVALUE=state, /NO_COPY
  
    widget_control, state.stid,  get_value=temp
    widget_control, state.stpid, get_value=ptemp

        	;**** copy the values to the structure ****

    ps = { dsname           : state.ps.dsname     ,			$
           dtname           : state.ps.dtname     ,			$
           dpname           : state.ps.dpname     ,			$
           njlevx           : state.ps.njlevx     ,			$
           njprtx           : state.ps.njprtx     ,			$
           njprtx2          : state.ps.njprtx2    ,			$
           levels           : state.ps.levels     ,			$
           levels2          : state.ps.levels2    ,			$
           bndlLS           : temp                ,			$
           prnts1           : state.ps.prnts1     ,			$
           prnts2           : state.ps.prnts2     ,			$
           bndl_prnts       : ptemp               ,			$
           ion              : state.ps.ion        ,			$
           nclc             : state.ps.nclc       ,			$
           pmdflg           : state.ps.pmdflg      			}

	;**** Restore the state. ****

    WIDGET_CONTROL, stash, SET_UVALUE=state, /NO_COPY

        ;**** Return the value here. ****

    RETURN,ps

END

;-----------------------------------------------------------------------------

FUNCTION cw_adas210_proc_event, ev

    parent = ev.handler


		;***********************************************
		;**** Retrieve the structure from the child ****
        	;**** that contains the sub ids.            ****
		;***********************************************

    stash = WIDGET_INFO(parent, /CHILD)
    WIDGET_CONTROL, stash, GET_UVALUE=state, /NO_COPY

        	;**** process the events ****

    case ev.id of

        state.lid:begin
            widget_control, state.swcol2,  map = 0
            widget_control, state.swcol1,  map = 1
            widget_control, state.col3,  map = 0
            widget_control, state.col2,  map = 1
        end

        state.pbid:begin
            if state.ps.pmdflg eq 0 then begin
                buts = ['Cancel']
                title = 'ADAS210 ERROR!'
                message = 'There is no superstructure template'+	$
                          ' for the parent metastables'
                act = popup(message=message,title=title,buttons=buts)
                widget_control, state.lid, /set_button
                widget_control, state.pbid, set_button = 0
                widget_control, state.swcol2,  map = 0
                widget_control, state.swcol1,  map = 1
                widget_control, state.col3,  map = 0
                widget_control, state.col2,  map = 1
            endif else begin
                widget_control, state.swcol1,  map = 0
                widget_control, state.swcol2,  map = 1
                widget_control, state.col2,  map = 0
                widget_control, state.col3,  map = 1
            endelse
        end

        ;**** cancel button ****

        state.cid:begin
            new_event = {ID:parent, TOP:ev.top, HANDLER:0L, ACTION:'Cancel'}
        end

        ;**** done button ****

        state.did:begin
            widget_control, state.stpid, get_value = temp
            chk = 0
            for i = 0, n_elements(temp)-1 do begin
                if temp(i) ne 0 then chk = chk+1
            endfor
            if chk gt 5 then begin
                but = ['Cancel']
                titl = 'ADAS210 WARNING!'
                mess = 'Expansion to > 5 metastables is not supported'
                ac = popup(message=mess, title=titl, buttons=but)
            endif else begin
                new_event = {ID:parent, TOP:ev.top, HANDLER:0L, ACTION:'Done'}
            endelse
        end

        ;**** Menu button ****

        state.outid: begin
            new_event = {ID:parent, TOP:ev.top, HANDLER:0L, ACTION:'Menu'}
        end

	ELSE:			;**** Do nothing ****

    ENDCASE

        ;**** Restore the state structure ****

    WIDGET_CONTROL, stash, SET_UVALUE=state, /NO_COPY

    RETURN, new_event

END

;-----------------------------------------------------------------------------

FUNCTION cw_adas210_proc, parent, njlevx2, bitfile, VALUE=value, FONT=font

    IF (N_PARAMS() EQ 0) THEN MESSAGE, 'Must specify a parent '+	$
                                       'for cw_adas210_proc'

    ON_ERROR, 2					

    IF NOT (KEYWORD_SET(font))  THEN font=''
    IF NOT (KEYWORD_SET(value)) then begin
        mult = make_array( njlevx2, /float, value = -1.0)
        ps = { dsname           : ''               ,			$
               dtname           : ''               ,			$
               dpname           : ''               ,			$
               njlevx           : 0                ,			$
               njprtx           : 0                ,			$
               njprtx2          : 0                ,			$
               levels           : mult             ,			$
               levels2          : mult             ,			$
               bndlLS           : mult             ,			$
               prnts1           : mult             ,			$
               prnts2           : mult             ,			$
               bndl_prnts       : mult             ,			$
               ion              : 0                ,			$
               nclc             : 0                ,			$
               pmdflg           : 0                 			}
    endif else begin
        ps = { dsname           : value.dsname     ,			$
               dtname           : value.dtname     ,			$
               dpname           : value.dpname     ,			$
               njlevx           : value.njlevx     ,			$
               njprtx           : value.njprtx     ,			$
               njprtx2          : value.njprtx2    ,			$
               levels           : value.levels     ,			$
               levels2          : value.levels2    ,			$
               bndlLS           : value.bndlLS     ,			$
               prnts1           : value.prnts1     ,			$
               prnts2           : value.prnts2     ,			$
               bndl_prnts       : value.bndl_prnts ,			$
               ion              : value.ion        ,			$
               nclc             : value.nclc       ,			$
               pmdflg           : value.pmdflg      			}
    endelse

            	;**** define other variables ****

    ion  = strcompress(ps.ion, /remove_all)+'      '
    nclc = strcompress(ps.nclc, /remove_all)

            	;**** Create the base widget ***     

    base = WIDGET_BASE(parent, EVENT_FUNC = "cw_adas210_proc_event",	$
		       FUNC_GET_VALUE = "cw_adas210_proc_get_value", /row)
    col = widget_base(base, /column)
    sel_row   = widget_base(col, /row)
    info1_row = widget_base(col, /row)

            	;**** create the level/parent switch ****

    sel_col   = widget_base(sel_row,  /column)
    buttonlab = widget_label(sel_col, value = 'Click to edit:-',        $
                             font=font)
    sel_r1 = widget_base(sel_col, /row)
    sel_c1 = widget_base(sel_r1, /row, /exclusive)
    lid = widget_button(sel_c1, value = 'Level Unbundling ',		$
                       /no_release, font = font)
    pbid = widget_button(sel_c1, value = 'Parent Unbundling',		$
                       /no_release, font = font)

            	;**** create the switching base ****

    swbase = widget_base(info1_row)
    sw_col1 = widget_base(swbase, /column)

            	;*********************************
            	;**** build level info. on it ****
            	;*********************************

    titl_row  = widget_base(sw_col1, /row)
    info_row  = widget_base(sw_col1, /row)
    info_col  = widget_base(info_row, /column)
    info_r1   = widget_base(info_col, /row)
    info_r2   = widget_base(info_col, /row)
    labelval  = 'Specific ion file level list:-'
    listlab   = widget_label(info_col, value = labelval, font=font)
    listlab2  = widget_label(info_col, value = ' ', font=font)
    info_r3   = widget_base(info_col, /row) 
 
            	;**** add dataset name and browse button ****

    rc = cw_adas_dsbr(titl_row, ps.dsname, nrows = 2,                   $
                      font=font)

            	;**** add level list ****

    levelout  = ' Index  Configuration   Designation  Energy(cm-1)'
    levelout  = [levelout, ' ', ps.levels]
    iid1 = widget_label(info_r1, value = 'Ion:   ',            font=font)
    iid2 = widget_label(info_r1, value = ion,                  font=font)
    nid1 = widget_label(info_r1, value = 'Nuclear Charge:',    font=font)
    nid2 = widget_label(info_r1, value = nclc,                 font=font)
    list = widget_list(info_r3,  value = levelout, ysize = 22, font=font)

            	;*********************************
            	;**** build parent info. base ****
            	;*********************************

    sw_col2   = widget_base(swbase, /column)
    titl_row  = widget_base(sw_col2, /row)
    info_row  = widget_base(sw_col2, /row)
    info_col  = widget_base(info_row, /column)
    info_r1   = widget_base(info_col, /row)
    info_r2   = widget_base(info_col, /row)
    labelval  = 'Specific ion file parent metastable list:-'
    listlab   = widget_label(info_col, value = labelval, font=font)
    listlab2  = widget_label(info_col, value = ' ', font=font)
    info_r3   = widget_base(info_col, /row) 

            	;**** add dataset name and browse button ****

    rcp = cw_adas_dsbr(titl_row,  ps.dsname, nrows = 2,                 $
                       font=font)

            	;**** add parent list ****

    levelout=' Index  Configuration   St.Weight.   Energy(cm-1)'
    levelout=[levelout, ' ', ps.prnts1]
    iid1  = widget_label(info_r1, value = 'Ion:   ', font=font)
    iid2  = widget_label(info_r1, value = ion, font=font)
    nid1  = widget_label(info_r1, value = 'Nuclear Charge:', font=font)
    nid2  = widget_label(info_r1, value = nclc, font=font)
    list  = widget_list(info_r3, value = levelout, ysize = 22, font=font)

		;*******************************************
            	;**** always popup with levels chosen   ****
            	;**** to avoid problems with no dataset ****
		;*******************************************

    widget_control, sw_col1, /map 
    widget_control, lid, /set_button

            	;**** create a base to build unbundler upon ****

    actbase  = widget_base(base)

            	;**** create second switching bases ****

    col2     = widget_base(actbase, /column, /frame)
    col3     = widget_base(actbase, /column, /frame)
    col2_row = widget_base(col2, /row)
    col2_row2= widget_base(col2, /row)
    col2_row3= widget_base(col2, /row)
    col4     = widget_base(col2_row3, /column, /scroll,                 $
                           x_scroll_size = 650, y_scroll_size = 650)
    col3_row = widget_base(col3, /row)
    col3_row2= widget_base(col3, /row)
    col3_row3= widget_base(col3, /row)
    col5     = widget_base(col3_row3, /column, /scroll,                 $
                           x_scroll_size = 650, y_scroll_size = 650)


            ;**** add information label for level base ****

    infid = widget_label( col2_row2, font=font,                     	$
            value = '               < Press return after each entry >' )

            ;**** add browse button for superstructure dataset ****

    lbl2 = 'Data File:'
    rc2  = cw_adas_dsbr(col2_row, ps.dtname, filelabel = lbl2, font=font)

            ;*************************************************
            ;**** attach superstructure unbundling widget ****
            ;*************************************************

    stid = cw_bndl_gen( col4, njlevx2, ps.levels2, ps.bndlLS, 		$
                        title = ps.dtname, font=font)
            
            ;**** add information label for parent base ****

    infpid = widget_label( col3_row2, font=font,                	$
             value = '               < Press return after each entry >' )

            ;**** add browse button for parent superstructure dataset ****

    rcp2  = cw_adas_dsbr(col3_row, ps.dpname, filelabel = lbl2, font=font)

            ;********************************************************
            ;**** attach superstructure parent unbundling widget ****
            ;********************************************************

    stpid = cw_bndl_gen( col5, ps.njprtx2, ps.prnts2, ps.bndl_prnts, 	$
                         title = ps.dpname, font=font )
            
            ;**** Desensitize appropriate bases ****

    widget_control, col3, map = 0

            ;**** create bottom row buttons including  back to menu' ****

    but_row  = widget_base(col, /row)
    menufile = bitfile + '/menu.bmp'
    read_X11_bitmap, menufile, bitmap1
    outid = widget_button(but_row, value = bitmap1)
    cid = widget_button(but_row, value = 'Cancel', font=font)
    did = widget_button(but_row, value = 'Done', font=font)

            ;**** save out initial state structure ****

    state = { LID:lid, PBID:pbid, STID:stid, STPID:stpid, OUTID:outid, 	$
              CID:cid, DID:did, PS:ps, COL2:col2,        		$
              COL3:col3, SWCOL1:sw_col1, SWCOL2:sw_col2 }

    WIDGET_CONTROL, WIDGET_INFO(base, /CHILD), SET_UVALUE=state, /NO_COPY

    RETURN, base

END
