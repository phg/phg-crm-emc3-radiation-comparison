; Copyright (c) 1995 Strathclyde University .
; SCCS info: Module @(#)$Header: /home/adascvs/idl/adas2xx/adas210/adas210_proc.pro,v 1.2 2004/07/06 10:22:46 whitefor Exp $ Date $Date: 2004/07/06 10:22:46 $
;+
; PROJECT:
;       ADAS IBM MVS to UNIX IDL development
;
; NAME:
;	ADAS210_PROC
;
; PURPOSE:
;	IDL ADAS user interface, processing options.
;
; EXPLANATION:
;	This routine creates and manages the adas210 processing options
;       widget.
;
; USE:
;	See the ADAS210 routine baispf.pro for an example of how to
;	use this routine.
;
; INPUTS:
;	VAL	- A structure which determines the initial settings of
;		  the processing options compound widgets. The structure
;                 is passed unmodified through to cw_adas210_proc.pro,
;                 see that routine for a full description.
;
;       NJLEVX2 - The number of levels associated with the superstructure
;                 template data set, see cw_adas210_proc.pro.
;
; OPTIONAL INPUTS:
;	None
;
; OUTPUTS:
;	VAL	- On output the structure records the final settings of
;		  the processing selection widget if the user pressed the
;		  'Done' button otherwise it is not changed from input.
;
;	ACT	- A string; indicates the user's action when the pop-up
;		  window is terminated, i.e which button was pressed to
;		  complete the input.  Possible values are 'Done',  
;		  'Cancel' and 'Menu'.
;
; OPTIONAL OUTPUTS:
;	None
;
; KEYWORD PARAMETERS:
;	WINTITLE- A title to be used on the banner for the pop-up window.
;		  Intended to indicate which application is running,
;		  e.g 'ADAS 210 PROCESSING'
;
;	TITLE	- Another title to be included in the widget.
;
;	FONT	- Supplies the font to be used for the interface widgets.
;
; CALLS:
;	CW_ADAS210_PROC Processing options widget creation.
;	DYNLABEL	Sets dynamic_resize keyword for label widgets.
;	XMANAGER	Manages the pop=up window.
;	ADAS210_PROC_EV	Event manager, called indirectly during XMANAGER
;			event management.
;
; SIDE EFFECTS:
;	XMANAGER is called in /modal mode. Any other widget becomes
;	inactive.
;
; CATEGORY:
;	Adas system.
;
; WRITTEN:
;       David H.Brooks. Univ.of Strathclyde, 17-Nov-1995        
;
; MODIFIED:
;       1.1	David H.Brooks 
;               First release.
;	1.2    	William Osborn
;		Added dynlabel procedure
;
; VERSION:
;       1.1     17-11-95
;	1.2	09-07-96
;
;-
;-----------------------------------------------------------------------------

PRO adas210_proc_ev, event

    COMMON proc_blk, action, value

		;**** Find the event type and copy to common ****

    action = event.action

    CASE action OF

		;**** 'Done' button ****

	'Done'  : begin

			;**** Get the output widget value ****

	    widget_control, event.id, get_value=value 
	    widget_control, event.top, /destroy
	end


		;**** 'Cancel' button ****

	'Cancel': widget_control, event.top, /destroy

                ;**** 'Menu' button ****

        'Menu': widget_control, event.top, /destroy

	ELSE:			;**** Do nothing ****

    ENDCASE

END

;-----------------------------------------------------------------------------

PRO adas210_proc, njlevx2, bitfile, val, act, WINTITLE = wintitle, 	$
                   TITLE = title, FONT = font

    COMMON proc_blk, action, value

		;**** Set defaults for keywords ****

    IF NOT (KEYWORD_SET(wintitle)) THEN wintitle='ADAS210 PROCESSING OPTIONS'
    IF NOT (KEYWORD_SET(title)) THEN title = ''
    IF NOT (KEYWORD_SET(font)) THEN font = ''

		;**** Copy value to common ****

    value = val

		;***************************************
		;**** Pop-up a new widget if needed ****
		;***************************************

                ;**** create base widget ****

    procid = widget_base(TITLE=wintitle, XOFFSET=1, YOFFSET=1)

		;**** Declare output options widget ****

    cwid = cw_adas210_proc(procid, njlevx2, bitfile, VALUE=value, FONT=font)

		;**** Realize the new widget ****

    dynlabel, procid
    widget_control, procid, /realize

		;**** make widget modal ****

    xmanager,'adas210_proc', procid, event_handler='adas210_proc_ev',	$
             /modal, /just_reg
 
		;**** Return the output value from common ****

    act = action
    val = value

END

