; Copyright (c) 1995, Strathclyde University .
; SCCS info: Module @(#)$Header: /home/adascvs/idl/adas2xx/adas210/baspf0.pro,v 1.2 2004/07/06 11:35:19 whitefor Exp $ Date $Date: 2004/07/06 11:35:19 $
;+
; PROJECT:
;       ADAS IBM MVS to UNIX conversion
;
; NAME:
;	BASPF0
;
; PURPOSE:
;	IDL user interface and communications with ADAS210 FORTRAN
;	process via pipe.
;
; EXPLANATION:
;	Firstly this routine invokes the part of the user interface
;	used to select the input dataset for ADAS210.  When the user's
;	interactions are complete this routine communicates with the
;	ADAS210 FORTRAN application via a UNIX pipe.  Communications
;	are to the FORTRAN subroutine BASPF0.
;
; USE:
;	The use of this routine is specific to ADAS210, see adas210.pro.
;
; INPUTS:
;	PIPE	- The IDL unit number of the bi-directional pipe to the
;		  ADAS210 FORTRAN process.
;
;	VALUE	- A structure which determines the initial settings of
;		  the dataset selection widget.  The initial value is
;		  set in adas210.pro.  VALUE is passed un-modified
;		  through to cw_adas210_in.pro, see that routine for a full
;		  description.
;
; OPTIONAL INPUTS:
;	None
;
; OUTPUTS:
;	VALUE	- On output the structure records the final settings of
;		  the dataset selection widget if the user pressed the
;		  'Done' button otherwise it is not changed from input.
;
;	DSFULL	- String; The full system file name of the input 
;		  dataset selected by the user for processing.
;
;	REP	- String; Indicates whether the user pressed the 'Done'
;		  or 'Cancel' button on the interface.  The action is
;		  converted to the strings 'NO' and 'YES' respectively
;		  to match up with the existing FORTRAN code.  In the
;		  original IBM ISPF interface REP was the reply to the
;		  question 'DO YOU WANT TO END PROGRAM EXECUTION'.
;
; OPTIONAL OUTPUTS:
;	None
;
; KEYWORD PARAMETERS:
;	FONT	- Supplies the font to be used for the interface widgets.
;
; CALLS:
;	ADAS210_IN		Pops-up the dataset selection widget.
;
; SIDE EFFECTS:
;	This routine communicates with the ADAS210 FORTRAN process
;	via a UNIX pipe.
;
; CATEGORY:
;	Adas system.
;
; WRITTEN:
;       David Brooks, University of Strathclyde 27-11-95
;
; MODIFIED:
;       1.1	David Brooks
;		First version
;	1.2	Tim Hammond
;		Changed call to adas_in to modified adas210_in
;
; VERSION:
;	1.1	22-01-96
;	1.2	22-01-96
;
;-
;-----------------------------------------------------------------------------


PRO baspf0, pipe, value, dsfull1, dsfull2, dsfull3, rep, FONT=font

		;***********************************
                ;**** Set defaults for keywords ****
		;***********************************

    IF NOT (KEYWORD_SET(font)) THEN font = ''

		;**********************************
		;**** Pop-up input file widget ****
		;**********************************

    adas210_in, value, action, WINTITLE = 'ADAS 210 INPUT', 		$
	     TITLE = 'Input Dataset', FONT = font

		;********************************************
		;**** Act on the event from the widget   ****
		;**** There are only two possible events ****
		;**** 'Done' and 'Cancel'.               ****
		;********************************************

    if action eq 'Done' then begin
        rep = 'NO'
    endif else begin
        rep = 'YES'
    endelse

		;********************************
		;**** Construct dataset names****
		;********************************

    dsfull1 = strcompress(value.rootpath1+value.file1)
    dsfull2 = strcompress(value.rootpath2+value.file2)
    dsfull3 = ''

		;*******************************
		;**** Write data to fortran ****
		;*******************************

    printf, pipe, rep
    printf, pipe, dsfull1
    printf, pipe, dsfull2

		;*****************************************************
		;**** If parent file available send it to fortran ****
		;*****************************************************

    printf, pipe, format = '(1x,i1)', value.pmdflg
    if value.pmdflg eq 1 then begin
      dsfull3 = strcompress(value.rootpath3+value.file3)
      printf, pipe, dsfull3
    endif
                
END
