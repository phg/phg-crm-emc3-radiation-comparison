; Copyright (c) 1995, Strathclyde University
; SCCS info: Module @(#)$Header: /home/adascvs/idl/adas2xx/adas210/cw_adas210_in.pro,v 1.5 2004/07/06 12:34:39 whitefor Exp $ Date $Date: 2004/07/06 12:34:39 $
;+
; PROJECT:
;       UNIX IDL ADAS development
;
; NAME:
;	CW_ADAS210_IN()
;
; PURPOSE:
;	Data file selection for two input datasets.
;
; EXPLANATION:
;	This function creates a compound widget consisting of two
;       occurences of the compound widget cw_adas4xx_infile.pro
;	a 'Cancel' button, a 'Done' button and two buttons 
;	to allow the browsing of the selected data file comments.
;	The browsing and done buttons are automatically de-sensitised
;	until a valid input dataset has been selected.
;
;	The value of this widget is the settings structure of the
;	cw_adas4xx_infile widget.  This widget only generates events
;	when either the 'Done' or 'Cancel' buttons are pressed.
;	The event structure returned is;
;	{ID:0L, TOP:0L, HANDLER:0L, ACTION:''}
;	ACTION has one of two values 'Done' or 'Cancel'.
;
; USE:
;	See routine adas_in.pro for an example.
;
; INPUTS:
;	PARENT	- Long integer; the ID of the parent widget.
;
; OPTIONAL INPUTS:
;	None
;
; OUTPUTS:
;	This function returns the ID of the compound widget created.
;
; OPTIONAL OUTPUTS:
;	None
;
; KEYWORD PARAMETERS:
;	VALUE	- A structure which determines the initial settings of
;                 the dataset selection widget cw_adas4xx_infile.  The
;		  structure must be;
;		  {ROOTPATH1:'', FILE1:'', ROOTPATH2:'', FILE2:'',
;                  CENTROOT:'', USERROOT:'' }
;		  The elements of the structure are as follows;
;
;		  ROOTPATH1 - first data directory e.g '/usr/fred/adas/'
;		  FILE1     - first data file in ROOTPATH1 e.g 'input.dat'
;		  ROOTPATH2 - second data directory e.g '/usr/fred/adas/'
;		  FILE2     - second data file in ROOTPATH2 e.g 'input.dat'
;		  CENTROOT - Default central data store e.g '/usr/adas/'
;		  USERROOT - Default user data store e.g '/usr/fred/adas/'
;
;		  The first data file selected by the user is obtained by
;		  appending ROOTPATH1 and FILE1, and the second by
;                 appending ROOTPATH2 and FILE2.  In the above example
;		  the full name of both data files is;
;		  /usr/fred/adas/input.dat
;
;		  Path names may be supplied with or without the trailing
;		  '/'.  The underlying routines add this character where
;		  required so that USERROOT will always end in '/' on
;		  output.
;
;		  The default value is;
;		  {ROOTPATH1:'./', FILE1:'', ROOTPATH2:'./', FILE2:'',
;                  CENTROOT:'', USERROOT:''}
;		  i.e ROOTPATH1 & ROOTPATH2 are set to the user's current
;                 directory.
;
;	TITLE	- The title to be included in the input file widget, used
;                 to indicate exactly what the required input dataset is,
;                 e.g 'Input COPASE Dataset'
;
;	UVALUE	- A user value for the widget.  Defaults to 0.
;
;	FONT	- Supplies the font to be used for the interface widgets.
;		  Defaults to the current system font.
;
; CALLS:
;	XXTEXT	          Pop-up window to browse dataset comments.
;	CW_ADAS4XX_INFILE Dataset selection widget.
;	FILE_ACC	  Determine filetype and access permissions.
;
;	See side effects for other related widget management routines.
;
; SIDE EFFECTS:
;	IN210_GET_VAL()	Widget management routine in this file.
;	IN210_EVENT()	Widget management routine in this file.
;
; CATEGORY:
;	Compound Widget
;
; WRITTEN:
;       David H.Brooks, Univ.of Strathclyde, 20-Nov-1995
;       Altered cw_adas_in.pro to allow double file selection.
;
; MODIFIED:
;	1.0	David H.Brooks
;		First version
;	1.1	Tim Hammond (Tessella Support Services plc)
;		Considerable changes to allow smooth integration
;		into main body of ADAS routines:
;		- Renamed internal routines to avoid conflict with
;		  others of same name when adas running
;		- Tidied up code
;	1.2	Tim Hammond
;		Corrected minor syntax error
;	1.3	Tim Hammond
;		Renamed routines called by XMANAGER
;	1.4	William Osborn
;		Removed xsize=n keyword from widget_label command.
;		The dynlabel procedure does not override these
;		assignments and they were taken as being values in pixels
;		on some machines.
;	1.5	David H.Brooks 
;		Altered order of search for parent template file. Now
;               searches in user space first then central if not found.
;
; VERSION:
;       1.0	20-11-95
;	1.1	22-01-96
;	1.2	22-01-96
;	1.3	22-01-96
;	1.4	01-08-96
;	1.5	19-11-96
;
;-
;-----------------------------------------------------------------------------

FUNCTION in210_get_val, id

		;***********************************
                ;**** Return to caller on error ****
		;***********************************
    ON_ERROR, 2

		;***********************************************
                ;**** Retrieve the structure from the child ****
		;***********************************************

    first_child = widget_info(id, /child)
    widget_control, first_child, get_uvalue=state, /no_copy
  
		;**********************
		;**** Get settings ****
		;**********************

    widget_control, state.file1id, get_value=inset1
    widget_control, state.file2id, get_value=inset2
 
		;***************************************
                ;**** Copy values to main structure ****
		;***************************************

    inset = {	PMDFLG		:	state.inset.pmdflg,   		$
              	ROOTPATH1	:	inset1.rootpath, 		$
		FILE1   	:	inset1.file,      		$
              	ROOTPATH2	:	inset2.rootpath, 		$
		FILE2   	:	inset2.file,      		$
              	ROOTPATH3	:	state.inset.rootpath3,         	$
              	FILE3    	:	state.inset.file3,       	$
	      	CENTROOT 	:	inset1.centroot, 		$
		USERROOT	:	inset1.userroot   		}

		;***************************
                ;**** restore the state ****
		;***************************

    widget_control, first_child, set_uvalue=state, /no_copy

		;*******************************
                ;**** return the value here ****
		;*******************************
  
   RETURN, inset

END

;-----------------------------------------------------------------------------

FUNCTION in210_event, event

		;************************************
                ;**** Base ID of compound widget ****
		;************************************

    parent=event.handler

		;***********************************************
                ;**** Retrieve the structure from the child ****
		;***********************************************

    first_child = widget_info(parent, /child)
    widget_control, first_child, get_uvalue=state, /no_copy

		;*********************************
		;**** Clear previous messages ****
		;*********************************

    widget_control, state.messid, set_value=''

		;*********************************
		;**** Default output no event ****
		;*********************************

    new_event = 0L

                ;************************
                ;**** Process events ****
                ;************************

    CASE event.id OF

		;***********************************
		;**** Event from file selection ****
		;***********************************

        state.file1id: begin
	    if event.action eq 'newfile' then begin
	        widget_control, state.browse1id, /sensitive
	    endif else begin
	        widget_control, state.browse1id, sensitive=0
	    endelse
	end

		;***********************
		;**** Browse button ****
		;***********************

    	state.browse1id: begin

		;*****************************
		;**** Get latest filename ****
		;*****************************

	    widget_control, state.file1id, get_value=inset1
	    filename1 = inset1.rootpath + inset1.file

		;**********************************
		;**** Invoke comments browsing ****
		;**********************************

	  xxtext, filename1, font=state.font
	end
		;*******************************
		;**** Second file selection ****
		;*******************************

    	state.file2id: begin
	    if event.action eq 'newfile' then begin
	        widget_control, state.doneid, /sensitive
	        widget_control, state.browse2id, /sensitive
	    endif else begin
	        widget_control, state.doneid, sensitive=0
	        widget_control, state.browse2id, sensitive=0
	    endelse
	end

		;***********************
		;**** Browse button ****
		;***********************

    	state.browse2id: begin

		;*****************************
		;**** Get latest filename ****
		;*****************************

	    widget_control, state.file2id, get_value=inset2
	    filename2 = inset2.rootpath + inset2.file

		;**********************************
		;**** Invoke comments browsing ****
		;**********************************

	    xxtext, filename2, font=state.font
	end

		;***********************
		;**** Cancel button ****
		;***********************

    	state.cancelid: new_event = {ID:parent, TOP:event.top, 		$
				     HANDLER:0L, ACTION:'Cancel'}

		;*********************
		;**** Done button ****
		;*********************

    	state.doneid: begin

		;******************************************
		;**** search for availablity of parent ****
                ;****       superstructure file        ****
		;******************************************

            widget_control, state.file2id, get_value = inset2
            filechk = inset2.file
            charpos = make_array(strlen(filechk), /long, value = -1)
            i = 0
            cnt = 0
            while (i ne -1) do begin
                i = strpos(filechk,'#',i)
                if i ne -1 then begin cnt = cnt + 1
                    charpos(cnt-1) = i
                    i = i + 1
                endif
            endwhile

		;******************************
                ;**** partition the string ****
		;******************************

            uscore = strpos(filechk, '_')
            seq=strmid(filechk, charpos(cnt-2)+1,(uscore-(charpos(cnt-2)+1)))
            dot=strpos(filechk, '.')
            elc=strmid(filechk, charpos(cnt-1)+1, (dot-(charpos(cnt-1)+1)))
            nseq = ''

		;***********************************************
           	;**** create parent file name and directory ****
		;***********************************************

            seqnames = ['h','he','li','be','b','c','n','o','f','ne',	$
                        'na','mg','al','si','p','s','cl','ar']
            for i = 0, n_elements(seqnames)-1 do begin
                if seq eq seqnames(i) then begin
                    nseq = seqnames(i-1)
                endif
            endfor
            npos = strlen(elc)
            for i = 0,9 do begin
                if strpos(elc, strtrim(string(i), 2)) lt npos and 	$
                strpos(elc, strtrim(string(i), 2)) ne -1 then     	$
                npos = strpos(elc, strtrim(string(i), 2))           
            endfor
            jres = strpos(elc,'j')
            lres = strpos(elc,'l')
            chg = ''
            if jres gt lres then begin
                chg =  strmid(elc, npos, (strpos(elc,'j')-npos))
                chg = long(chg) + 1
                chg = strtrim(string(chg), 2) + 'j.dat'
            endif else if lres gt jres then begin
                chg =  strmid(elc, npos, (strpos(elc, 'l') - npos))
                chg = long(chg) + 1
                chg = strtrim(string(chg), 2) + 'l.dat'
            endif
            t1 = strmid(filechk, 0, charpos(0) + 1)
            len1 = (charpos(1) + 1) - (charpos(0) + 1 + strlen(seq))
            t2 = strmid(filechk, charpos(0) + 1 + strlen(seq), len1)
            filechk2 = t1 + nseq + t2
            if cnt eq 3 then begin
                len2 = (charpos(2) + 1) - (charpos(1) + 1 + strlen(seq))
                t3 = strmid(filechk, charpos(1) + 1 + strlen(seq), len2)
                len3 = (charpos(cnt - 1) + npos + 1) - (charpos(2) + 1)
                t4 = strmid(filechk, charpos(2) + 1, len3) + chg      
                filechk2 = filechk2 + nseq + t3 + t4
                srch1 = inset2.userroot + filechk2
                srch2 = inset2.centroot + filechk2
            endif else begin
                dir = strmid(filechk, 0, charpos(cnt - 2) + 1)
                dir = dir + nseq
                len3 = (charpos(cnt - 1) + npos + 1) - (charpos(1) + 1)
                t4 = strmid(filechk, charpos(1) + 1, len3) + chg      
                filechk2 = filechk2 + t4
                srch1 = inset2.userroot + dir + '/' + filechk2
                srch2 = inset2.centroot + dir + '/' + filechk2
            endelse

		;**************************************
                ;**** search to see if files exist ****
		;**************************************

            file_acc, srch1, exist1, read1, write1, exec1, type1
            if exist1 eq 0 or type1 ne '-' or read1 eq 0 then begin 
                file_acc, srch2, exist2, read2, write2, exec2, type2
                if exist2 eq 0 or type2 ne '-' or read2 eq 0 then begin
                    buts = ['Cancel','Continue']
                    title = 'ADAS210 SEARCH RESULTS'
                    message = 'There is no superstructure template'+	$
                              ' for the parent metastables'
                    act = popup(message=message, title=title, buttons=buts)
                    if act eq 'Continue' then begin
                        new_event = {ID:parent, TOP:event.top, 		$
                                     HANDLER:0L, ACTION:'Done'}
                    endif 
                endif else begin
                    if cnt eq 3 then begin
                     state.inset.rootpath3 = inset2.centroot
                     state.inset.file3 = filechk2 
                    endif else begin
                     state.inset.rootpath3 = inset2.centroot + dir + '/'
                     state.inset.file3 = filechk2 
                    endelse
                    state.inset.pmdflg = 1
                    new_event = {ID:parent, TOP:event.top, 		$
                                 HANDLER:0L, ACTION:'Done'}
                endelse
            endif else begin
                if cnt eq 3 then begin
                    state.inset.rootpath3 = inset2.userroot
                    state.inset.file3 = filechk2 
                endif else begin
                    state.inset.rootpath3 = inset2.userroot + dir + '/'
                    state.inset.file3 = filechk2 
                endelse
                state.inset.pmdflg = 1
                new_event = {ID:parent, TOP:event.top, 			$
                             HANDLER:0L, ACTION:'Done'}
            endelse
        end

    	ELSE:			;**** Do nothing ****

    ENDCASE

		;***************************
                ;**** restore the state ****
		;***************************

    widget_control, first_child, set_uvalue=state, /no_copy

    RETURN, new_event

END

;-----------------------------------------------------------------------------

FUNCTION cw_adas210_in, parent, VALUE=value, TITLE=title, 		$
                        UVALUE=uvalue, FONT=font


    IF (N_PARAMS() EQ 0) THEN MESSAGE, 'Must specify parent for cw_adas_in'
    ON_ERROR, 2					;return to caller

		;***********************************
		;**** Set defaults for keywords ****
		;***********************************

    IF NOT (KEYWORD_SET(value)) THEN begin
        inset = {	PMDFLG		:	0,       		$
                	ROOTPATH1	:	'./', 			$
			FILE1		:	'', 			$
			ROOTPATH2	:	'./', 			$
			FILE2		:	'', 			$
			ROOTPATH3	:	'./', 			$
			FILE3		:	'', 			$
			CENTROOT	:	'', 			$
			USERROOT	:	'' 			}
    ENDIF ELSE BEGIN
        inset = {   	PMDFLG		:	0,              	$
                	ROOTPATH1	:	value.rootpath1, 	$
			FILE1		:	value.file1, 		$
                	ROOTPATH2	:	value.rootpath2, 	$
			FILE2		:	value.file2, 		$
                	ROOTPATH3	:	value.rootpath3, 	$
			FILE3		:	value.file3, 		$
			CENTROOT	:	value.centroot, 	$
			USERROOT	:	value.userroot 		}
        if strtrim(inset.rootpath1) eq '' then begin
            inset.rootpath1 = './'
        endif else if 							$
	strmid(inset.rootpath1, strlen(inset.rootpath1)-1, 1) ne '/' 	$
        then begin
            inset.rootpath1 = inset.rootpath1 + '/'
        endif
        if strmid(inset.file1, 0, 1) eq '/' then begin
            inset.file1 = strmid(inset.file1, 1, strlen(inset.file1)-1)
        endif
        if strtrim(inset.rootpath2) eq '' then begin
          inset.rootpath2 = './'
        endif else if 							$
        strmid(inset.rootpath2, strlen(inset.rootpath2)-1, 1) ne '/' 	$
	then begin
            inset.rootpath2 = inset.rootpath2 + '/'
        endif
        if strmid(inset.file2, 0, 1) eq '/' then begin
            inset.file2 = strmid(inset.file2, 1, strlen(inset.file2)-1)
        endif
        if strtrim(inset.rootpath3) eq '' then begin
          inset.rootpath3 = './'
        endif else if 							$
        strmid(inset.rootpath3, strlen(inset.rootpath3)-1, 1) ne '/' 	$
	then begin
            inset.rootpath3 = inset.rootpath3+'/'
        endif
        if strmid(inset.file3, 0, 1) eq '/' then begin
            inset.file3 = strmid(inset.file3, 1, strlen(inset.file3)-1)
        endif
    END
    IF NOT (KEYWORD_SET(title)) THEN title = ''
    IF NOT (KEYWORD_SET(uvalue)) THEN uvalue = 0
    IF NOT (KEYWORD_SET(font)) THEN font = ''

		;*********************************
		;**** Create the Input widget ****
		;*********************************

		;****************************
		;**** create base widget ****
		;****************************

    topbase = widget_base( parent, UVALUE = uvalue,         		$
			   EVENT_FUNC = "in210_event",       		$
		       	   FUNC_GET_VALUE = "in210_get_val", 		$
			   /COLUMN)

                ;******************************************************
                ;**** Create a dummy widget just to hold value of *****
                ;**** "state" variable so as not to get confused  *****
                ;**** with any other values. Adopt IDL practice   *****
                ;**** of using first child widget                 *****
                ;******************************************************

    first_child = widget_base(topbase)
    cwid = widget_base(first_child, /column)

		;*******************************************
		;**** Set up first file structure       ****
		;*******************************************

    inset1 = {   ROOTPATH:inset.rootpath1, FILE:inset.file1, 		$
                 CENTROOT:inset.centroot, USERROOT:inset.userroot 	}

		;*******************************************
		;****  First file selection widget      ****
		;*******************************************

    filebase1 = widget_base(cwid, /frame, /column)
    file1id = cw_adas4xx_infile(filebase1, value=inset1, title=title,	$
                                font=font)

		;*****************
		;**** Buttons ****
		;*****************

    base1 = widget_base(filebase1, /row)

		;*******************************
		;**** Browse Dataset button ****
		;*******************************

    browse1id = widget_button(base1, value='Browse Comments', font=font)

		;*******************************************
		;**** Set up second file structure      ****
		;*******************************************

    inset2 = {   ROOTPATH:inset.rootpath2, FILE:inset.file2, 		$
                 CENTROOT:inset.centroot, USERROOT:inset.userroot 	}

		;********************************************
		;**** Second file selection widget       ****
		;********************************************
	
    title2 = 'Select Superstructure file for Template'
    filebase2 = widget_base(cwid, /frame, /column)
    file2id = cw_adas4xx_infile(filebase2, value=inset2, 		$
                                title=title2, font=font)

		;*****************
		;**** Buttons ****
		;*****************

    base2 = widget_base(filebase2,/row)

		;*******************************
		;**** Browse Dataset button ****
		;*******************************

    browse2id = widget_button(base2, value='Browse Comments', font=font)


		;***********************
		;**** Cancel Button ****
		;***********************

    base = widget_base(cwid, /row)

    cancelid = widget_button(base, value='Cancel', font=font)
  
		;*********************
		;**** Done Button ****
		;*********************

    doneid = widget_button(base, value='Done', font=font)

		;***********************
		;**** Error message ****
		;***********************

    messid = widget_label(parent, font=font, value='*')

		;********************************************************
		;**** Check default filenames and desensitise buttons****
		;**** if they are directories or have no read access ****
		;********************************************************

    filename1 = inset1.rootpath + inset1.file
    file_acc, filename1, fileexist, read, write, execute, filetype
    if filetype ne '-' then begin
        widget_control, browse2id, sensitive=0
        widget_control, doneid, sensitive=0
    endif else begin
        if read eq 0 then begin
            widget_control, browseid, sensitive=0
            widget_control, doneid, sensitive=0
        endif
    endelse

    filename2 = inset2.rootpath + inset2.file
    file_acc, filename2, fileexist, read, write, execute, filetype
    if filetype ne '-' then begin
        widget_control, browse2id, sensitive=0
        widget_control, doneid, sensitive=0
    endif else begin
        if read eq 0 then begin
            widget_control, browseid, sensitive=0
            widget_control, doneid, sensitive=0
        endif
    endelse

		;*************************************************
		;**** create a state structure for the widget ****
		;*************************************************

     new_state = { 	FILE1ID         : file1id         ,		$
                 	BROWSE1ID       : browse1id       ,		$
                 	FILE2ID         : file2id         ,		$
                 	BROWSE2ID       : browse2id       ,		$
	 	 	CANCELID        : cancelid        ,		$
                 	DONEID          : doneid          ,		$
                 	MESSID          : messid          ,		$
		 	FONT            : font            ,		$
                 	INSET           : inset            		}

		;**************************************
                ;**** Save initial state structure ****
		;**************************************

    widget_control, first_child,  set_uvalue=new_state, /no_copy

    RETURN, cwid

END

