; Copyright (c) 1995, Strathclyde University
; SCCS info: Module @(#)$Header: /home/adascvs/idl/adas2xx/adas210/baispf.pro,v 1.1 2004/07/06 11:35:00 whitefor Exp $ Date $Date: 2004/07/06 11:35:00 $
;+
;
; PROJECT:
;         ADAS IBM MVS to UNIX conversion and development
;
; NAME:
;         ADAS210
;
; PURPOSE:
;         IDL interface and communications with ADAS210 FORTRAN
;         process via pipe.
;
; EXPLANATION:
;         This procedure does some preliminary independent checking of
;         array dimension and gathering of data from the input dataset.
;         It then reads information from the ADAS210 FORTRAN process 
;         via a bi-directional UNIX pipe. Then, the IDL user interface
;         adas210_proc.pro is invoked to determine how the user wishes to
;         process the input dataset. When the user's selections are 
;         complete the procedure processes them and establishes the values
;         of any outstanding arrays/variables. The information is then 
;         written back to the FORTRAN process via the pipe. Communications
;         are to the FORTRAN subroutine BAISPF.
;
; USE:
;         ADAS210 specific, see adas210.pro
;
; INPUTS:
;         PIPE     - The IDL unit number of the bi-directional UNIX pipe
;                    to the ADAS210 FORTRAN process.
;
;         DSNAME   - String; The full system filename of the data 
;                    collection selected by the user for processing.
;
;         DTNAME   - String; The full system filename of the 
;                    superstructure data set selected to use as the
;                    unbundling template. 
;
;         LEVELS   - Strarr(njlevx); Contains the original index, level
;                    configuration, Total spin/orbital angular momentum
;                    quantum numbers and ionisation potentials of the
;                    levels for which data is available in the input
;                    dataset.
;
;         LEVELS2  - Strarr(njlevx2); Contains the original index, level
;                    configuration, Total spin/orbital angular momentum
;                    quantum numbers and ionisation potentials of the
;                    levels for which data is available in the
;                    superstructure template.
;
;         ION      - String; The element and ion charge.
;
;         NCLC     - Integer; The elemental nuclear charge
;
;         IZ0      - String; The ion charge+1
;
;         PRNTS    - Strarr(njlevx); Contains the original index, term
;                    configuration, Total spin/orbital angular momentum
;                    quantum numbers and energies for the recombining ion
;                    parent metastables for which Dielectronic Recombination
;                    data is included in the dataset.
;
; OPTIONAL INPUTS:
;         None
;
; OUTPUTS:
;         LPEND    - Integer; 0 if the user pressed the 'Done' button
;                    or 1 if the user pressed 'Cancel'. Maps onto the 
;                    logical variable LPEND in the ADAS210 FORTRAN via 
;                    the integer ILOGIC.
;
;         GP_COUNT - Integer; The number of groupings selected by the user.
;                    maps directly onto NGP in the ADAS210 FORTRAN.
;
;         NJPRTX   - Integer; The number of J-resolved parent metastables.
;
;         NBPRTX   - Integer; The number of bundled parent metastables.
;
;         NCBPRT   - Integer; The number of contributing parents from 
;		     superstructure.
;
;         NJLEVX   - Integer; The number of LS-resolved levels.
;
;         NJLEVX2  - Integer; The number of J-resolved levels.
;
;         NBLEVX   - Integer; The number of bundled levels.
;
;         INDJP    - Integer; The index of the J-resolved parent.
;
;         BNDPR    - Intarr(njlevx); The parent bundling vector containing 
;                    the user selected group numbers and zero's for the
;                    unselected parents.
;
;         CNJP     - Strarr(njprtx); Contains the original parent
;                    parent metastable term configurations.
;
;         BNDLLS   - Intarr(njlevx); The level bundling vector containing
;                    the user selected group numbers and zero's for the
;                    unselected levels.
;
;         VALUE    - Structure; Contains all the information required
;                    by the processing options widgets.
;                    value = {                            $
;                              dsname       : dsname     ,$
;                              dtname       : dtname     ,$
;                              njlevx       : njlevx     ,$
;                              njprtx       : njprtx     ,$
;                              levels       : levels     ,$
;                              levels2      : levels2    ,$
;                              bndlLS       : bndlLS     ,$
;                              prnts1       : prnts1     ,$
;                              prnts2       : prnts2     ,$
;                              bndl_prnts   : bndl_prnts ,$
;                              ion          : ion        ,$
;                              nclc         : nclc       ,$
;                             }
;
; OPTIONAL OUTPUTS:
;         None
;
; KEYWORD PARAMETERS:
;         FONT - The name of the font to be used on the widget.
;
; CALLS:
;         ADAS210_PROC   Invoke the IDL interface for ADAS210 data
;                        processing options/input.
;
; SIDE EFFECTS:
;         Bi-directional communication with ADAS210 FORTRAN 
;         via a UNIX pipe.
;
; CATEGORY:
;         ADAS system
;
; WRITTEN:
;       David H.Brooks, Univ.of Strathclyde, 	22-01-96
;
; MODIFIED:
;	1.1	David H. Brooks			
;		First version
; VERSION:
;       1.1     22-01-96
;		
;-
;------------------------------------------------------------------------

PRO baispf, pipe, lpend, dsname, dtname, dpname, pmdflg, gomenu, 	$
            bitfile, FONT=font

    IF NOT (KEYWORD_SET(font)) THEN font = ''

           	;***************************************;
           	;********Declare some variables ********;
           	;***************************************;

    check    = string(5)
    elhead   = string(40)
    head1 = strarr(100)
    T_levels = elhead
    string = string(15)
    scr = string(100)
    check1 = string(5)
    check2 = '   -1'

           	;***************************************;
           	;open data set to check number of levels;
           	;***************************************;

    get_lun, unit
    openr, unit, dsname
    njlevx = 0
    repeat begin
        readf, unit, format='(1a100)', scr
        reads, scr, format='(1a5,95x)', check1
        head1(njlevx) = scr
        njlevx = njlevx+1
    endrep until check1 eq check2 
    free_lun, unit
    njlevx = njlevx-2        ; compensate for repeat loop reading then checking

           	;***************************************;
           	;* number established so create array  *;
           	;***************************************;

    levels  = strarr(njlevx)
    for i = 0, njlevx-1 do begin
        reads, head1(i+1), format = '(1a45)', T_levels
        levels(i) = T_levels
    endfor

           	;***************************************;
           	;read in ion charge & parent metastables;
           	;***************************************;

    iont = string(5)
    nclct = string(10)
    met1t = string(12) 
    met2t = met1t & met3t = met1t & met4t = met1t
    reads, head1(0), format='(1a5,5x,i5,1a10,75x)', iont, nclct, iz0t
    ion = iont
    nclc = nclct
    iz0 = iz0t

           	;***************************************;
           	; read pipe communications from fortran ;
           	;***************************************;

    prntst = ''
    readf, pipe, format='(1x,i3)', njlevx2

           	;***************************************;
           	; levels determined so create array     ;
           	;***************************************;

    levels2 = strarr(njlevx2)
    prnts1 = levels2

    for i = 0,njlevx2-1 do begin
        readf, pipe, format='(1a48)', prntst
        levels2(i) = prntst
    endfor

    readf, pipe, format='(1x,i1)', njprtx
    if njprtx eq 0 then njprtx = 1 
    for i = 0, njprtx-1 do begin
        readf, pipe, format='(1a49)', prntst
        prnts1(i) = prntst
    endfor

		;***************************************************
           	;**** read parent superstructure dataset levels ****
		;***************************************************

    if pmdflg ne 0 then begin
      readf, pipe, format='(1x,i3)', njprtx2
      prnts2 = strarr(njprtx2)
        for i = 0, njprtx2-1 do begin
           readf, pipe, format='(1a48)', prntst
           prnts2(i) = prntst
        endfor
    endif else begin
      njprtx2 = njlevx
      prnts2 = strarr(njprtx2)
    endelse

           	;***************************************;
           	;*** establish no. of parent/bundles ***;
           	;***************************************;

    nbprtx = 0
    cnjp = strarr(njprtx2)
    for i = 0, njprtx2-1 do begin
        temp = strmid(prnts2(i), 5, 9)
        space = '                  '
        strput, space, temp, 0
        cnjp(i) = space
    endfor

           	;***************************************;
           	;**** call widget creation function ****;
           	;***************************************;

    bndlLS = intarr(njlevx2)
    bndl_prnts = intarr(njprtx2)

           	;***************************************;
           	;*** create structure for processing ***;
           	;***************************************;

        value = { dsname       : dsname     ,				$
                  dtname       : dtname     ,				$
                  dpname       : dpname     ,				$
                  njlevx       : njlevx     ,				$
                  njprtx       : njprtx     ,				$
                  njprtx2      : njprtx2    ,				$
                  levels       : levels     ,				$
                  levels2      : levels2    ,				$
                  bndlLS       : bndlLS     ,				$
                  prnts1       : prnts1     ,				$
                  prnts2       : prnts2     ,				$
                  bndl_prnts   : bndl_prnts ,				$
                  ion          : ion        ,				$
                  nclc         : nclc       ,				$
                  pmdflg       : pmdflg      				}

    adas210_proc, njlevx2, bitfile, value, action, FONT=font	

           	;***************************************;
           	;*** copy values back from widget    ***;
           	;***************************************;

    dsname = value.dsname
    dtname = value.dtname
    dpname = value.dpname
    njlevx = value.njlevx
    njprtx = value.njprtx
    njprtx2 = value.njprtx2
    levels = value.levels
    levels2 = value.levels2
    bndlLS = value.bndlLS
    prnts1 = value.prnts1
    prnts2 = value.prnts2
    bndl_prnts = value.bndl_prnts

           	;********************************************;
           	;***** process cancel/done/menu options *****;           
           	;********************************************; 

    if action eq 'Done' then begin
        lpend = 0
        printf, pipe, format='(i1)', lpend
    endif else if action eq 'Menu' then begin
        lpend = 0
        gomenu = 1
        printf, pipe, format = '(i1)', lpend
        goto, LABELEND
    endif else begin
        lpend = 1
        printf, pipe, format='(i1)', lpend
        goto, LABELEND
    endelse                    

           	;***************************************;
           	;* create parent string arrays/indexes *;
           	;***************************************;

    indjp = indgen(njprtx2)+1
    bndpr = indjp
    for i = 0, njprtx2-2 do begin
        if bndl_prnts(i+1) gt bndl_prnts(i) then begin
            nbprtx = bndl_prnts(i+1)
        endif
    endfor
    if bndl_prnts(0) gt nbprtx then nbprtx = bndl_prnts(0) 
    bndpr = bndl_prnts
    nbcprt = 0
    for i = 0, njprtx2-1 do begin
      if bndpr(i) ne 0 then nbcprt = nbcprt+1
    endfor
    for i = 0, nbcprt-1 do begin
      if bndpr(i) eq 0 then nbcprt = nbcprt+1
    endfor

           	;***************************************;
           	;*** determine no. of bundled levels ***;
           	;***************************************;
    
    gp_count = 0
    imk = 0
    nchk = make_array( njlevx2, /int, value = -1 )
    for i = 0, (njlevx2-1) do begin
        for k = 0, (njlevx2-1) do begin
            if bndlLS(i) eq bndlLS(k) and bndlLS(i) ne 0 and i ne k then begin
                lfnd = 0
                for j = 0, imk do begin
                    if bndlLS(i) eq nchk(j) then lfnd = 1
                endfor
                if lfnd eq 0 then begin
                    nchk(imk) = bndlLS(i)
                    imk = imk + 1
                    gp_count = gp_count + 1
                endif
            endif
        endfor
    endfor
    nblevx = gp_count
    for i = 0, (njlevx2-1) do begin
        lfnd = 0
        for j = 0, imk-1 do begin
            if bndlLS(i) eq nchk(j) then lfnd = 1 
        endfor
        if lfnd eq 0 then nblevx = nblevx+1
    endfor

           	;***************************************;
           	;******* fire selections to f77  *******;
           	;***************************************;

    printf, pipe, format = '(i5)', gp_count
    printf, pipe, format = '(i5)', iz0
    printf, pipe, format = '(i5)', njprtx
    printf, pipe, format = '(i5)', nbprtx
    printf, pipe, format = '(i5)', njlevx2
    printf, pipe, format = '(i5)', nblevx
    printf, pipe, format = '(i5)', nbcprt
    for i = 0, (nbcprt-1) do begin
        printf, pipe, format = '(i5,1x,i5,1x,1a18,1x)',			$
                indjp(i), bndpr(i), cnjp(i)
    endfor  
    for i = 0, (njlevx2-1) do begin
        printf, pipe, format = '(1x,i3)', bndlLS(i)
    endfor

LABELEND:

END
