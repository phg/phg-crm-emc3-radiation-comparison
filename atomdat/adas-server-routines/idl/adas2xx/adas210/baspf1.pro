; Copyright (c) 1995, Strathclyde University.
; SCCS info: Module @(#)$Header: /home/adascvs/idl/adas2xx/adas210/baspf1.pro,v 1.1 2004/07/06 11:35:26 whitefor Exp $ Date $Date: 2004/07/06 11:35:26 $
;+
;
; PROJECT:
;       ADAS IBM MVS to UNIX conversion
;
; NAME:
;	BASPF1
;
; PURPOSE:
;	IDL user interface and communications with ADAS210 FORTRAN
;	process via pipe.
;
; EXPLANATION:
;	First this routine reads some information from ADAS210 FORTRAN
;	via the UNIX pipe.  Next the 'Output Options' part of the
;	interface is invoked.  Finally the results of the user's
;	interactions are written back to the FORTRAN via the pipe.
;	Communications are with the FORTRAN subroutine BASPF1.
;
; USE:
;	The use of this routine is specific to ADAS210, See adas210.pro.
;
; INPUTS:
;	PIPE	- The IDL unit number of the bi-directional pipe to the
;		  ADAS210 FORTRAN process.
;
;	DSFULL  - The name of the data set being analyzed.
;
;	VALUE	- A structure which determines the initial settings of
;		  the dataset selection widget.  The initial value is
;		  set in adas210.pro.  VALUE is passed un-modified
;		  through to cw_adas_in.pro, see that routine for a full
;		  description.
;
;	HEADER  - Header information used for text output.
;
; OPTIONAL INPUTS:
;	None.
;
; OUTPUTS:
;	LPEND	- Integer; indicates user action on leaving output
;		  selections.  lpend is set to 1 if the user exited
;		  with the 'Cancel' button and 0 if the user exited
;		  with 'Done'.
;
;	VALUE	- On output the structure records the final settings of
;		  the output selection widget if the user pressed the
;		  'Done' button otherwise it is not changed from input.
;
; OPTIONAL OUTPUTS:
;	None.
;
; KEYWORD PARAMETERS:
;	DEVLIST	- A string array;  Each element of the array is the
;		  name of a hardcopy graphics output device.  This
;		  list must mirror the DEVCODE list.  DEVLIST is the
;		  list which the user will see.
;		  e.g ['Post-Script','HP-PCL','HP-GL']
;
;	FONT	- Supplies the font to be used for the interface widgets.
;
; CALLS:
;	ADAS210_OUT	User interface - output options.
;
; SIDE EFFECTS:
;	This routine communicates with the ADAS210 FORTRAN process
;	via a UNIX pipe.
;
; CATEGORY:
;	Adas system.
;
; WRITTEN:  
;       David H.Brooks, Univ.of Strathclyde, 20-Nov-1995
;       changed names in b9spf1.pro.
;
; VERSION:
;       1.1     David H. Brooks		
;		First version
;
; MODIFIED:
;	1.1	20-11-95
;
;-
;-----------------------------------------------------------------------------

PRO baspf1, pipe, lpend, value, dsfull, header, DEVLIST=devlist, FONT=font


		;***********************************
                ;**** Set defaults for keywords ****
		;***********************************

    IF NOT (KEYWORD_SET(font)) THEN font = ''

		;**************************************
		;**** Pop-up output options widget ****
		;**************************************

    adas210_out, value, dsfull, action, DEVLIST=devlist, FONT=font

		;*********************************************
		;**** Act on the output from the widget   ****
		;**** There are only two possible actions ****
		;**** 'Done' and 'Cancel'.                ****
		;*********************************************

    if action eq 'Done' then begin
        lpend = 0
    endif else begin
        lpend = 1
    endelse

		;*******************************
		;**** Write data to fortran ****
		;*******************************

    printf, pipe, lpend
    if lpend eq 0 then begin

		;**********************************************
		;*** If text output requested tell FORTRAN ****
		;**********************************************
     
       printf, pipe, value.texout
       if (value.texout eq 1) then begin
	   printf, pipe, value.texdsn
       endif

		;****************************************************
                ;** The next two lines are an alteration to clear  **    
                ;**  the widget dataset input fiel and deactivate  **
                ;** the text output request button, can be removed **
                ;** if defaults are preferred.                     **
		;****************************************************

        value.texdsn = ''    
        value.texout = 0     
    endif

		;**************************************************
		;**** Set messages and settings ready for when ****
		;**** output window is re-displayed.           ****
		;**************************************************

    if lpend eq 0 then begin
        if value.texout eq 1 then begin

		;***************************************************
		;**** If text output is requested enable append ****
		;**** for next time and update the default to   ****
		;**** the current text file.                    ****
		;***************************************************

            value.texapp=0
            if value.texrep ge 0 then value.texrep = 0
            value.texmes = 'Output written to file.'
        endif
    endif

END
