; Copyright (c) 1996 Strathclyde University .
; SCCS info: Module @(#)$Header: /home/adascvs/idl/adas2xx/adas210/adas210_in.pro,v 1.2 2004/07/06 10:22:36 whitefor Exp $ Date $Date: 2004/07/06 10:22:36 $
;+
; PROJECT:
;       ADAS IBM MVS to DEC UNIX conversion
;
; NAME:
;	ADAS210_IN
;
; PURPOSE:
;	IDL ADAS user interface, input file selection.
;
; EXPLANATION:
;	This routine creates and manages a pop-up window which allows
;	the user to select a file from the UNIX file system.  It is
;	intended as a general routine and hence the widget titles may
;	be passed into this routine using keywords.
;
; USE:
;	See the ADAS205 routine b5spf0.pro for an example of how to
;	use this routine.
;
; INPUTS:
;	VAL	- A structure which determines the initial settings of
;		  the dataset selection widget.  VALUE is passed un-modified
;		  through to cw_adas210_in.pro, see that routine for a full
;		  description.
;
; OPTIONAL INPUTS:
;	None
;
; OUTPUTS:
;	VAL	- On output the structure records the final settings of
;		  the dataset selection widget if the user pressed the
;		  'Done' button otherwise it is not changed from input.
;
;	ACT	- A string; indicates the user's action when the pop-up
;		  window is terminated, i.e which button was pressed to
;		  complete the input.  Possible values are 'Done' and
;		  'Cancel'.
;
; OPTIONAL OUTPUTS:
;	None
;
; KEYWORD PARAMETERS:
;	WINTITLE- A title to be used on the banner for the pop-up window.
;		  Intended to indicate which application is running,
;		  e.g 'ADAS 205 INPUT'
;
;	TITLE	- The title to be included in the input file widget, used
;		  to indicate exactly what the required input dataset is,
;		  e.g 'Input COPASE Dataset'
;
;	FONT	- Supplies the font to be used for the interface widgets.
;
; CALLS:
;	CW_ADAS210_IN	Dataset selection widget creation.
;	DYNLABEL	Sets dynamic_resize keyword for label widgets.
;	XMANAGER	Manages the pop=up window.
;	ADAS210_IN_EV	Event manager, called indirectly during XMANAGER
;			event management.
;
; SIDE EFFECTS:
;	XMANAGER is called in /modal mode. Any other widget become
;	inactive.
;
; CATEGORY:
;	Adas system.
;
; WRITTEN:
;       Andrew Bowen, Tessella Support Services plc, 17-May-1993
;
; MODIFIED:
;       Version 1.1     David H.Brooks 
;                       Changed YOFFSET to 1 & added call to 
;                       cw_adas210_in.pro.
;			Tim Hammond
;			Renamed COMMON block and all routines to
;			avoid clashes with same names in main adas_in.pro
;	Version 1.2    	William Osborn
;			Added dynlabel procedure
;
; VERSION:
;       1.1     14-Nov-1995
;	1.2	09-07-96
;
;-
;-----------------------------------------------------------------------------

PRO adas210_in_ev, event

    COMMON in210_blk, action, value

		;**** Find the event type and copy to common ****

    action = event.action

    CASE action OF

		;**** 'Done' button ****

	'Done'  : begin
			;**** Get the output widget value ****
		widget_control, event.id, get_value=value 
		widget_control, event.top, /destroy

	   end


		;**** 'Cancel' button ****

	'Cancel': widget_control, event.top, /destroy

    END

END

;-----------------------------------------------------------------------------

PRO adas210_in, val, act, WINTITLE=wintitle, TITLE=title, FONT=font

    COMMON in210_blk, action, value

		;**** Copy value to common ****

    value = val

		;**** Set defaults for keywords ****

    IF NOT (KEYWORD_SET(wintitle)) THEN wintitle = 'ADAS INPUT FILE'
    IF NOT (KEYWORD_SET(title)) THEN title = ''
    IF NOT (KEYWORD_SET(font)) THEN font = ''

		;***************************************
		;**** Pop-up a new widget if needed ****
		;***************************************

                ;**** create base widget ****

    inid = widget_base(TITLE=wintitle, XOFFSET=100, YOFFSET=1)

		;**** Declare output options widget ****

    cwid = cw_adas210_in(inid, VALUE=value, TITLE=title, FONT=font)

		;**** Realize the new widget ****
    dynlabel, inid
    widget_control, inid, /realize

		;**** make widget modal ****

    xmanager, 'adas210_in', inid, event_handler='adas210_in_ev',	$
              /modal, /just_reg
 
		;**** Return the output value from common ****

    act = action
    val = value

END

