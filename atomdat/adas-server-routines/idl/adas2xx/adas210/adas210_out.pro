; Copyright (c) 1995, Strathclyde University.
; SCCS info: Module @(#)$Header: /home/adascvs/idl/adas2xx/adas210/adas210_out.pro,v 1.2 2004/07/06 10:22:41 whitefor Exp $ Date $Date: 2004/07/06 10:22:41 $
;+
;
; PROJECT:
;       ADAS IBM MVS to UNIX conversion
;
; NAME:
;	ADAS210_OUT
;
; PURPOSE:
;	IDL ADAS user interface, output options.
;
; EXPLANATION:
;	This routine creates and manages a pop-up window which allows
;	the user to select options and input data to control ADAS210
;	graphical and file output.
;
; USE:
;	This routine is ADAS210 specific, see baspf1.pro for an example
;	of how it is used.
;
; INPUTS:
;	VAL	- A structure which determines the initial settings of
;		  the output options widget.  The value is passed
;		  unmodified into cw_adas210_out.pro.
;
;		  See cw_adas210_out.pro for a full description of this
;		  structure.
;
;	DSFULL	- String; The full system file name of the input 
;		  dataset selected by the user for processing.
;
; OPTIONAL INPUTS:
;	None.
;
; OUTPUTS:
;	ACT	- String; Either 'Done' or 'Cancel' for the button the
;		  user pressed to terminate the output options window.
;
; OPTIONAL OUTPUTS:
;	None.
;
; KEYWORD PARAMETERS:
;	DEVLIST	- A string array;  Each element of the array is the
;		  name of a hardcopy graphics output device.  This
;		  list must mirror the DEVCODE list.  DEVLIST is the
;		  list which the user will see.
;		  e.g ['Post-Script','HP-PCL','HP-GL']
;
;	FONT	- Supplies the font to be used for the interface widgets.
;
; CALLS:
;	CW_ADAS210_OUT	Creates the output options widget.
;	DYNLABEL	Sets dynamic_resize keyword for label widgets.
;	XMANAGER
;	See side effects for widget management routine.
;
; SIDE EFFECTS:
;	This routine uses a common block OUT210_BLK to maintain its state.
;	ADAS210_OUT_EV	is included in this file and is called
;			indirectly from XMANAGER during widget
;			management.
;
; CATEGORY:
;	Widgets
;
; WRITTEN:
;       David H.Brooks, Univ.of Strathclyde, 17-Nov-1995
;       Altered naming conventions in adas209_out to be consistent
;       with adas210. 
;
; VERSION:
;       1.1	David H. Brooks		
;		First version
;	1.2    	William Osborn
;		Added dynlabel procedure
;
; MODIFIED:
;	1.1	17-11-95
;	1.2	09-07-96
;
;-
;-----------------------------------------------------------------------------

PRO adas210_out_ev, event

    COMMON out210_blk, action, value
	

		;**** Find the event type and copy to common ****

    action = event.action

    case action of

		  ;**** 'Done' button ****

	'Done'  : begin

	          ;**** Get the output widget value ****

  		  child = widget_info(event.id,/child)
		  widget_control,child,get_value=value 

		  ;*****************************************
		  ;**** Kill the widget to allow IDL to ****
		  ;**** continue and interface with     ****
		  ;**** FORTRAN only if there is work   ****
		  ;**** for the FORTRAN to do.          ****
		  ;*****************************************

                  if value.texout eq 1 then begin
		      widget_control, event.top, /destroy
		  endif 
	          end


		  ;**** 'Cancel' button ****

	'Cancel': widget_control, event.top, /destroy

    endcase

end

;-----------------------------------------------------------------------------


pro adas210_out, val, dsfull, act, devlist=devlist, font=font

    COMMON out210_blk, action, value

		;**** Copy value to common ****

    value = val

		;**** Set defaults for keywords ****

    if not (keyword_set(font)) then font = ''
    if not (keyword_set(devlist)) then devlist = ''

		;***************************************
		;**** Pop-up a new widget           ****
		;***************************************

                ;**** create base widget ****

    outid = widget_base(title='ADAS210 OUTPUT OPTIONS',xoffset=200,yoffset=100)

		;**** Declare output options widget ****

    cwid = cw_adas210_out(outid, dsfull, value=value,  			$
			  devlist=devlist, font=font )

		;**** Realize the new widget ****

    dynlabel, outid
    widget_control, outid, /realize

		;**** make widget modal ****

    xmanager, 'adas210_out', outid, event_handler='adas210_out_ev',	$
              /modal, /just_reg
 
		;**** Return the output value from common ****

    act = action
    val = value

END

