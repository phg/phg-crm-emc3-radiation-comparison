;----------------------------------------------------------------------
;+
; PROJECT    :  ADAS
;
; NAME       :  run_adas218
;
; PURPOSE    :  Runs the ADAS218 collisional-radiative population code
;               as an IDL subroutine.
;
; ARGUMENTS  :  All output arguments will be defined appropriately.
;
;               NAME      I/O    TYPE   DETAILS
; INPUTS     :  adf04      I     str    specific ion file.
;               adf18      I     str    cross reference file for projection.
;               te         I     real() electron temperatures requested
;               tion       I     real() ion temperatures requested
;                                       (default set to te)
;               th         I     real() hydrogen temperaturesfor CX
;                                       (default to 0.0).
;               dens       I     real() electron densities requested
;               dion       I     real() ion densities requested
;                                       (default to 0.0).
;               denh       I     real() ion densities requested
;                                       (if /cx is used).
;               unit_te    I     str    'eV', 'K' or 'red' (default eV)
;               unit_dens  I     str    'cm-3' or 'red' (default cm-3)
;               meta       I     int()  metastable indices (start at 1)
;                                       (default set to [1])
;               zeff       I     real   for scaling ion collisions if dion
;               wmin       I     real   lower wavelength(A) for PEC/SXB
;                                       (default to 0.0).
;               wmax       I     real   upper wavelength(A) for PEC/SXB
;                                       (default to 9000.0).
;               amin       I     real   lower A value for PEC/SXB
;                                       (default to 1.0E8 s-1).
;               tr_upper   I     int()  upper indices for transitions in PEC file.
;               tr_lower   I     int()  lower indices for transitions in PEC file.
;               pass_dir   I     str    passing file directory for gcr etc.
;                                       (defaults to current directory).
;               log        I     str    name of output text file.
;                                       (defaults to no output).
;
; KEYWORDS   :  ion        I     int    =1 include ionisation from excited level
;                                       =0 do not (default)
;               cx         I     int    =1 use cx data
;                                       =0 no cx is included (default)
;               rec        I     int    =1 use recombination data
;                                       =0 none is included (default)
;               nonorm     I     int    =1 do not normalise pop to ground for single
;                                          metastable case
;                                       =0 normalise (default)
;               pec        I     int    =1 output pec data to pass_dir
;                                       =0 no pecs (default)
;               sxb        I     int    =1 output pec data to pass_dir
;                                       =0 no sxbs (default)
;               gcr        I     int    =1 output gcr set to pass_dir
;                                       =0 no gcr data output (default)
;               help       I     int    Returns help and exits if set
;
; OUTPUTS    :  pop        O     struc  pop.numlev - number of levels in the
;                                                    adf04 file
;                                       pop.levels - str array of level
;                                                    descriptors.
;                                       pop.nummet - number of metastables
;                                       pop.metastables - str array of
;                                                    metastable descriptors.
;                                       pop.dependence - populations of ordinary
;                                                        level dependence on
;                                                        the metastable population.
;                                                    fltarr(nord,nmet,nte,nden)
;                                                    nord - no. ordinary levels
;                                                    nmet - no. metastables
;                                                    nte  - no. temperatures
;                                                    nden - no. densities
;
;
; NOTES      :  - run_adas218 uses the spawn command. Therefore IDL v5.3
;                 cannot be used. Any other version will work.
;               - The equilibrium metastable split is not returned. Rerun
;                 with one metastable to get these values.
;
;
; MODIFIED :
;               1.1 Martin O'Mullane
;                    - First version
;               1.2 Martin O'Mullane
;                    - Allow user to directly specify which transitions are
;                      to be included in adf15 file.
;               1.3 Martin O'Mullane
;                    - If pass_dir is not given se the pass directory 
;                      to current directory (./).
;
; VERSION :
;
;               1.1   27-03-2013
;               1.2   27-09-2018
;               1.3   05-11-2018
;-
;----------------------------------------------------------------------


PRO  run_adas218_calc, adf04, adf18, te, tion, th, dens, dion, denh,       $
                       meta, ifout, idout,                                 $
                       zeff, norm, is_ion, is_cx, is_rec, is_proj,         $
                       pass, log, is_log, wmin, wmax, amin, itrlow, itrup, $
                       is_pec, is_sxb, is_gcr, is_pop, pop


; Now run the ADAS218 code - the input has been checked already.

; Input screen

    fortdir = getenv('ADASFORT')
    spawn, fortdir+'/adas218.out', unit=pipe, /noshell, PID=pid

    date = xxdate()
    printf, pipe, date[0]

    uid = getenv('USER')
    printf,pipe,strmid(uid,0,10)

    rep = 'NO'
    printf, pipe, rep
    printf, pipe, adf04
    printf, pipe, adf18

    b8setp, pipe, sz0, sz, scnte, sil, lfpool, strga, ndmet, ndwvl, ndsel, $
            npl, strgmi, strgmf, ndlev, iss04a

; Now move onto processing screen options


    idum  = 0
    ddum  = 0.0d0
    sdum  = ''
    tscef = dblarr(14,3)

    readf,pipe,idum
    lpend = idum
    readf,pipe,sdum
    selem  =  sdum
    readf,pipe,idum
    ndtem = idum
    readf,pipe,idum
    ndden = idum
    readf,pipe,idum
    il = idum
    readf,pipe,idum
    nv = idum
    for i = 0,2 do begin
        for j = 0,13 do begin
            readf,pipe,ddum
            tscef(j,i) = ddum
        endfor
    endfor


    lpend = 0
    printf, pipe, lpend

; fortran expects to get max sized arrays

    imetr_in = intarr(ndmet)
    imetr_in[0:n_elements(meta)-1] = meta

    te_in = dblarr(ndtem)
    te_in[0:n_elements(te)-1] = te
    tion_in = dblarr(ndtem)
    tion_in[0:n_elements(tion)-1] = tion
    th_in = dblarr(ndtem)
    th_in[0:n_elements(th)-1] = th

    dens_in = dblarr(ndden)
    dens_in[0:n_elements(dens)-1] = dens
    dion_in = dblarr(ndden)
    dion_in[0:n_elements(dion)-1] = dion
    rat_in = dblarr(ndden)
    rat_in[0:n_elements(dens)-1] = denh/dens

    printf, pipe, 'Produced by run_adas218'
    printf, pipe, n_elements(meta)
    printf, pipe, imetr_in
    printf, pipe, ifout
    printf, pipe, n_elements(te)
    printf, pipe, te_in
    printf, pipe, tion_in
    printf, pipe, th_in
    printf, pipe, idout
    printf, pipe, n_elements(dens)
    printf, pipe, dens_in
    printf, pipe, dion_in
    printf, pipe, rat_in

    ratio = dblarr(ndden,ndmet)
    ratio[*,0] = 1.0

    for j=0, ndmet-1 do begin
       for i=0, ndden-1 do begin
          printf, pipe, ratio[i,j]
       endfor
    endfor
    for j=0, ndmet-1 do begin
       for i=0, ndden-1 do begin
          printf, pipe, ratio[i,j]
       endfor
    endfor

    printf,pipe,zeff

    if zeff GT 0.0 then  printf, pipe, 1 else printf, pipe, 0
    if zeff GT 0.0 then  printf, pipe, 1 else printf, pipe, 0

    printf, pipe, 0
    printf, pipe, is_cx
    printf, pipe, is_rec
    printf, pipe, is_ion
    printf, pipe, is_proj
    printf, pipe, norm

    zero = intarr(ndmet,ndmet)
    for j=0, ndmet-1 do begin
       for i=0, ndmet-1 do begin
          printf, pipe, zero[i,j]
       endfor
    endfor
    zero = intarr(ndmet,ndmet)
    for j=0, ndmet-1 do begin
       for i=0, ndmet-1 do begin
          printf, pipe, zero[i,j]
       endfor
    endfor
    zero = dblarr(ndtem,ndmet,ndmet)
    for k=0, ndtem-1 do begin
       for i=0, ndmet-1 do begin
          for j=0, ndmet-1 do begin
            printf, pipe, zero[k,i,j]
          endfor
       endfor
    endfor

    printf, pipe, ' '

    printf, pipe, n_elements(wmin)
    for j = 0, n_elements(wmin)-1 do begin
       printf, pipe, wmin[j]
       printf, pipe, wmax[j]
       printf, pipe, amin[j]
    endfor

    printf, pipe, n_elements(itrup)
    for j = 0, n_elements(itrup)-1 do begin
       printf, pipe, itrlow[j]
       printf, pipe, itrup[j]
    endfor

    printf, pipe, 0


; Nearly there - output options
;  - get ion balance and power

    date   = bxdate()
    header = 'run_adas218 : DATE: '+ date(0) + ' TIME: ' +date(1)

    maxt  = 0
    idum  = 0
    readf, pipe, maxt
    tine = dblarr(maxt)
    readf, pipe, tine
    readf, pipe, idum

    printf, pipe, 0

    if is_pop then begin
       printf, pipe, 1
       printf, pipe, 1
       printf, pipe, 'Graph title'
    endif else begin
       printf, pipe, 0
    endelse

    if is_log then begin
       printf, pipe, 1
       printf, pipe, 0
       printf, pipe, log,format='(A)'
    endif else begin
       printf, pipe, 0
    endelse


    if is_pec then printf, pipe, 1 else printf, pipe, 0
    if is_sxb then printf, pipe, 1 else printf, pipe, 0
    if is_gcr then printf, pipe, 1 else printf, pipe, 0

    printf, pipe, pass + '/.pass',format='(A)'

    if is_log then printf, pipe, header

    printf, pipe, 0



    if (is_pop) then begin

       idum = 0
       fdum = 0.0
       ddum = 0.0d0
       sdum = ''

       readf,pipe,idum
       ndlev = idum
       readf,pipe,idum
       ndtem = idum
       readf,pipe,idum
       ndden = idum
       readf,pipe,idum
       ndmet = idum
       readf,pipe,sdum
       titled = sdum
       readf,pipe,sdum
       title = sdum
       readf,pipe,sdum
       gtit1 = sdum
       readf,pipe,sdum
       dsninc = sdum
       readf,pipe,idum
       iz = idum
       readf,pipe,idum
       itsel = idum
       readf,pipe,fdum
       tev = fdum
       readf,pipe,idum
       il = idum
       readf,pipe,idum
       nmet = idum
       readf,pipe,idum
       nord = idum
       readf,pipe,idum
       maxd = idum

       lmetr = intarr(nmet)
       imetr = intarr(nmet)
       iordr = intarr(nord)
       densa = dblarr(maxd)
       strga = strarr(il)
       stack = dblarr(il,nmet,ndtem,maxd)

       for i=0,nmet-1 do begin
         readf,pipe,idum
         lmetr(i)=idum
       endfor
       for i=0,nmet-1 do begin
         readf,pipe,idum
         imetr(i)=idum
       endfor
       for i=0,nord-1 do begin
         readf,pipe,idum
         iordr(i)=idum
       endfor
       for i=0,maxd-1 do begin
         readf,pipe,ddum
         if (ddum lt 1.0d-36) then ddum = 0.0d0
         densa(i)=ddum
       endfor
       input = ''
       for i = 0, il-1 do begin
         readf,pipe,input
         strga(i) = input
       end

       for l = 0, maxd-1 do begin
         for k = 0, ndtem-1 do begin
           for j=0,nmet-1 do begin
             for i=0,il-1 do begin
               readf,pipe,ddum
               if (ddum lt 1.0d-36) then ddum = 0.0d0
               stack(i,j,k,l)=ddum
             endfor
           endfor
         endfor
       endfor

 ; Now make up an output structure

       dependence = stack[0:il-nmet-1, 0:nmet-1, 0:maxt-1, 0:maxd-1]

       pop = { numlev      : nord,            $
               nummet      : nmet,            $
               levels      : strga[iordr-1],  $
               metastables : strga[imetr-1],  $
               dependence  : dependence       }

    endif



; Terminate FORTRAN process

    printf, pipe, 1
    close, pipe
    
END
;----------------------------------------------------------------------------



PRO run_adas218, adf04     = adf04     , adf18  = adf18 ,                    $
                 te        = te        , tion   = tion  , th     = th   ,    $
                 unit_te   = unit_te   ,                                     $
                 dens      = dens      , dion   = dion  , denh   = denh ,    $
                 unit_dens = unit_dens ,                                     $
                 meta      = meta      , zeff   = zeff  ,                    $
                 ion       = ion       , cx     = cx    , rec  = rec    ,    $
                 nonorm    = nonorm    ,                                     $
                 wmin      = wmin      , wmax   = wmax  , amin = amin   ,    $
                 tr_upper  = tr_upper  , tr_lower = tr_lower,                $
                 pass_dir  = pass_dir  , pec    = pec   , sxb  = sxb    ,    $
                 gcr       = gcr       ,                                     $
                 pop       = pop       , log    = log   ,                    $
                 help      = help


; First check that we are not using IDL v5.3

if !version.release EQ '5.3' then begin
   message, 'This version of IDL (v5.3) will not work - ' + $
            'change to any other version'
   return
endif

; If asked for help

if keyword_set(help) then begin
   doc_library, 'run_adas218'
   return
endif

; Check that we get all inputs and that they are correct. Otherwise print
; a message and return to command line.

on_error, 2

if n_elements(adf04) EQ 0 then begin
    message,'A specific ion file (adf04) is required'
endif

; Suitable defaults

if n_elements(adf18) EQ 0 then is_proj = 0 else is_proj = 1
if n_elements(adf18) EQ 0 then adf18 = ''
if adf18 EQ 'NULL' then is_proj = 0

if n_elements(zeff) EQ 0 then zeff = 0.0

if n_elements(meta) EQ 0 then meta = [1]

is_norm = 1
if keyword_set(nonorm) then is_norm = 0

if n_elements(wmin) EQ 0 then wmin = [0.0]
if n_elements(wmax) EQ 0 then wmax = [9000.0]
if n_elements(amin) EQ 0 then amin = [1.0e8]

if n_elements(wmin) NE n_elements(wmax) then message, 'Set wmin to be same size as wmax'
if n_elements(wmin) NE n_elements(amin) then message, 'Set wmin/wmax to be same size as amin'

if n_elements(tr_lower) NE n_elements(tr_upper) then message, 'Set tr_upper to be same as tr_lower'

if n_elements(tr_lower) GT 0 then begin

   message, 'transition selection overrides wmin/wmax/amin method', /continue
   itrup  = tr_upper
   itrlow = tr_lower

   if n_elements(itrup) GT 100 then message, 'Cannot exceed 100 user selected transitions'

endif

; Set suitable defaults for the keywords - turn them off

if keyword_set(ion) then is_ion = 1 else is_ion = 0
if keyword_set(rec) then is_rec = 1 else is_rec = 0
if keyword_set(cx)  then is_cx  = 1 else is_cx  = 0


; File outputs

if n_elements(log) EQ 0 then begin
   output_file = ''
   is_log = 0
endif else begin
   output_file = log
   is_log = 1
endelse

if n_elements(pass_dir) EQ 0 then pass = './' else pass = pass_dir

if keyword_set(pec) then is_pec = 1 else is_pec = 0
if keyword_set(sxb) then is_sxb = 1 else is_sxb = 0
if keyword_set(gcr) then is_gcr = 1 else is_gcr = 0




; Temperature and density

if n_elements(te) eq 0 then message, 'User requested temperatures are missing'
if n_elements(dens) eq 0 then message, 'User requested densities are missing'

partype=size(te, /type)
if (partype lt 2) or (partype gt 5) then begin
   message,'Temperature must be numeric'
endif  else te = DOUBLE(te)

partype=size(dens, /type)
if (partype lt 2) or (partype gt 5) then begin
   message,'density must be numeric'
endif else dens = DOUBLE(dens)

len_te   = n_elements(te)
len_dens = n_elements(dens)

if n_elements(tion) EQ 0 then tion = te
if n_elements(th) EQ 0 then th = fltarr(len_te)
if n_elements(dion) EQ 0 then dion = fltarr(len_dens)
if n_elements(denh) EQ 0 then denh = fltarr(len_dens)

len_tion = n_elements(tion)
len_th   = n_elements(th)
len_denh = n_elements(denh)

; are te and tion and dens and denh the same length and one dimensional?

if len_te ne len_tion then print, 'Temperature arrays size mismatch - smallest  used'
itval = min([len_te,len_tion])

if itval ne len_th then print, 'Temperature arrays size mismatch - smallest  used'
itval = min([itval,len_th])

if len_dens ne len_denh then print, 'DENS and DENH size mismatch - smaller  used'
idval = min([len_dens,len_denh])


; units of Te/dens - default to eV and cm**-3

if n_elements(unit_te) EQ 0 then ifout = 2 else begin
   case strlowcase(strtrim(unit_te,2)) of
      'k'   : ifout = 1
      'ev'  : ifout = 2
      'red' : ifout = 3
      else  : message, 'Unknown temperature units'
   endcase
endelse

if n_elements(unit_dens) EQ 0 then idout = 1 else begin
   case strlowcase(strtrim(unit_dens,2)) of
      'cm-3'  : idout = 1
      'red'   : idout = 2
      else    : message, 'Unknown desnity units'
   endcase
endelse



; Output options - if none exit

if arg_present(pop) then is_pop = 1 else is_pop = 0


; Run the calculation, but first check dimensions of Te, dens and meta

if n_elements(meta) GT 4  then message,'Only 4 metastables are allowed'
if n_elements(te)   GT 35 then message,'Only 35 temperatures are allowed'
if n_elements(dens) GT 24 then message,'Only 24 densities are allowed'


run_adas218_calc, adf04, adf18, te, tion, th, dens, dion, denh,       $
                  meta, ifout, idout,                                 $
                  zeff, is_norm, is_ion, is_cx, is_rec, is_proj,      $
                  pass, log, is_log, wmin, wmax, amin, itrlow, itrup, $         $
                  is_pec, is_sxb, is_gcr, is_pop, pop


END
