; Copyright (c) 1996 Strathclyde University .
; SCCS info: Module @(#)$Header: /home/adascvs/idl/adas2xx/adas206/adas206.pro,v 1.5 2004/07/06 10:17:22 whitefor Exp $ Date $Date: 2004/07/06 10:17:22 $
;+
; PROJECT:
;       ADAS IBM MVS to DEC UNIX conversion
;
; NAME:
;	ADAS206
;
; PURPOSE:
;	The highest level routine for the ADAS 206 program.
;
; EXPLANATION:
;	This routine is called from the main adas system routine, adas.pro,
;	to start the ADAS 206 application.  Associated with adas206.pro
;	is a FORTRAN executable.  The IDL code provides the user
;	interface and output graphics whilst the FORTRAN code reads
;	in data files, performs numerical processing and creates the
;	output files.  The IDL code communicates with the FORTRAN
;	executable via a bi-directional UNIX pipe.  The unit number
;	used by the IDL for writing to and reading from this pipe is
;	allocated when the FORTRAN executable is 'spawned' (see code
;	below).  Pipe communications in the FORTRAN process are to
;	stdin and stdout, i.e streams 5 and 6.
;
;	The FORTRAN code is an independent process under the UNIX system.
;	The IDL process can only exert control over the FORTRAN in the
;	data which it communicates via the pipe.  The communications
;	between the IDL and FORTRAN must be exactly matched to avoid
;	input conversion errors.  The correct ammounts of data must be
;	passed so that neither process 'hangs' waiting for communications
;	which will never occur.
;
;	The FORTRAN code performs some error checking which is
;	independent of IDL.  In cases of error the FORTRAN may write
;	error messages.  To prevent these error messages from conflicting
;	with the pipe communications all FORTRAN errors are written to
;	output stream 0, which is stderr for UNIX.  These error messages
;	will appear in the window from which the ADAS session/IDL session
;	is being run.
;
;	In the case of severe errors the FORTRAN code may terminate
;	itself prematurely.  In order to detect this, and prevent the
;	IDL program from 'hanging' or crashing, the IDL checks to see
;	if the FORTRAN executable is still an active process before
;	each group of pipe communications.  The process identifier
;	for the FORTRAN process, PID, is recorded when the process is
;	first 'spawned'.  The system is then checked for the presence
;	of the FORTRAN PID.
;
; USE:
;	First the system settings must be established by calling
;	adas_sys_set.pro then adas206.pro is called to start the
;	ADAS 206 application;
;
;	adas_sys_set, adasrel, fortdir, userroot, centroot, $
;		      devlist, devcode, font_large, font_small, edit_fonts
;	adas206,   adasrel, fortdir, userroot, centroot, devlist, $
;		   devcode, font_large, font_small, edit_fonts
;
; INPUTS:
;	ADASREL - A string indicating the ADAS system version, 
;		  e.g ' ADAS RELEASE: ADAS93 V1.1.0'.  The first
;		  character should be a space.
;
;	FORTDIR - A string holding the path to the directory where the
;		  FORTRAN executables are, e.g '/disk/adas/fortran/exec'
;
;	USERROOT - A string holding the path to the root directory of
;		   the user's adas data. e.g '/disk/bowen/adas'
;		   This root directory will be used by adas to construct
;		   other path names.  For example the users default data
;		   for adas206 should be in /disk/bowen/adas/adf04.  In
;		   particular the user's default interface settings will
;		   be stored in the directory USERROOT+'/defaults'.  An
;		   error will occur if the defaults directory does not
;		   exist.
;
;	CENTROOT - Like USERROOT, but this directory points to the
;		   central data area for the system.  User defaults are
;		   not stored on CENTROOT.
;
;	DEVLIST - A string array of hardcopy device names, used for
;		  graphical output. e.g ['Post-Script','HP-PCL','HP-GL']
;		  This array must mirror DEVCODE.  DEVCODE holds the
;		  actual device names used in a SET_PLOT statement.
;
;	DEVCODE - A string array of hardcopy device code names used in
;		  the SET_PLOT statement, e.g ['PS', 'PCL', 'HP']
;		  This array must mirror DEVLIST.
;
;	FONT_LARGE - The name of a larger font e.g 'courier_bold14'
;
;	FONT_SMALL - The name of a smaller font e.g 'courier_bold12'
;
;	EDIT_FONTS - A structure of two fonts used in the table editor
;		     adas_edtab.pro, {font_norm:'',font_input:''}.
;		     The two fonts are used to differentiate between
;		     editable and non-editable parts of the table. You
;		     may have to experiment to get two fonts which give
;		     the same default spacing in the edit widget.
;		     e.g {font_norm:'helvetica_bold14', $
;			  font_input:'helvetica_oblique14'}
;
; OPTIONAL INPUTS:
;	None
;
; OUTPUTS:
;	None - Note this routine should leave all inputs un-modified.
;
; OPTIONAL OUTPUTS:
;	None
;
; KEYWORD PARAMETERS:
;	None
;
; CALLS:
;	FIND_PROCESS	Checks to see if a given process is active.
;	B6SPF0		Pipe comms with FORTRAN B6SPF0 routine.
;	BXSETP		Pipe comms with FORTRAN BXSETP routine.
;	B6ISPF		Pipe comms with FORTRAN B6ISPF routine.
;	XXDATE		Get date and time from operating system.
;	B6SPF1		Pipe comms with FORTRAN B6SPF1 routine.
;	BXOUTG		Pipe comms with FORTRAN BXOUTG routine.
;
; SIDE EFFECTS:
;	This routine spawns a FORTRAN executable.  Note the pipe 
;	communications routines listed above.  In addition to these
;	pipe communications there is one explicit communication of the
;	date to the FORTRAN code, search for 'printf,pipe' to find it.
;
; CATEGORY:
;	Adas system.
;	
; WRITTEN:
;       William Osborn, Tessella Support Services plc, 06-Jun-1996
;
; MODIFIED:
;       1.1    	William Osborn
;              	First release. Written using adas205.pro as template.
;       1.2    	William Osborn
;              	Removed default saving from body of program
;       1.3    	William Osborn
;              	Increased release number to 1.2
;       1.4    	Richard Martin
;              	Increased release number to 1.3
;	1.5	Martin O'Mullane
;		Added progress bar between processing and output
;               screens.
;			Increased release number to 1.4
;
; VERSION:
;       1.1     06-06-96
;	1.2	17-06-96
;	1.3	14-10-96
;	1.4	18-03-03
;	1.5 	29-04-03
;
;-
;-----------------------------------------------------------------------------

PRO ADAS206,	adasrel, fortdir, userroot, centroot, 			$
		devlist, devcode, font_large, font_small, edit_fonts

		;************************
		;**** Initialisation ****
		;************************

  adasprog = ' PROGRAM: ADAS206 V1.4'
  lpend = 0
  gomenu = 0
  deffile = userroot+'/defaults/adas206_defaults.dat'
  device = ''
  bitfile = centroot + '/bitmaps'


		;******************************************
		;**** Search for user default settings ****
		;**** If not found create defaults     ****
		;******************************************

  files = findfile(deffile)
  if files(0) eq deffile then begin

    restore,deffile
    inval.centroot = centroot+'/adf04/'
    inval.userroot = userroot+'/adf04/'

  endif else begin

    inval = { $
		ROOTPATH:userroot+'/adf04/', 				$
		FILE:'', 						$
		CENTROOT:centroot+'/adf04/', 				$
		USERROOT:userroot+'/adf04/' }

    procval = {NMET:-1}

    outval = { 								$
		GRPOUT:0, GTIT1:'', GRPSCAL:0, 				$
		XMIN:'',  XMAX:'',   					$
		YMIN:'',  YMAX:'',   					$
		HRDOUT:0, HARDNAME:'', 					$
		GRPDEF:'', GRPFMESS:'', 				$
		GRPSEL:-1, GRPRMESS:'', 				$
		DEVSEL:-1, GRSELMESS:'', 				$
		TEXOUT:0, TEXAPP:-1, 					$
		TEXREP:0, TEXDSN:'paper.txt', 				$
		TEXDEF:'',TEXMES:'', 					$
		CONOUT:0, CONAPP:-1, 					$
		CONREP:0, CONDSN:'', 					$
		CONDEF:userroot+'/pass/contour.pass',CONMES:'', 	$
		TOTOUT:0, TOTAPP:-1, 					$
		TOTREP:0, TOTDSN:'', 					$
		TOTDEF:userroot+'/pass/plt206.pass',TOTMES:'',		$
		SPCOUT:0, SPCAPP:-1, 					$
		SPCREP:0, SPCDSN:'', 					$
		SPCDEF:userroot+'/pass/pls206.pass',SPCMES:'',		$
		FLIPVAL:0   						}

		;**** Default contour, total power and specfic line ****
		;**** power name above !!****

  end


		;****************************
		;**** Start fortran code ****
		;****************************

  spawn,fortdir+'/adas206.out',unit=pipe,/noshell,PID=pid

		;************************************************
		;**** Get date and write to fortran via pipe ****
		;************************************************
  date = xxdate()
  printf,pipe,date(0)

LABEL100:
		;**** Check FORTRAN still running ****
  
  if find_process(pid) eq 0 then goto, LABELEND

		;************************************************
		;**** Communicate with b6spf0 in fortran and ****
		;**** invoke user interface widget for       ****
		;**** Data file selection                    ****
		;************************************************

  b6spf0, pipe, inval, dsninc, rep, FONT=font_large

		;**** If cancel selected then end program ****

  if rep eq 'YES' then goto, LABELEND

		;**** Check FORTRAN still running ****

  if find_process(pid) eq 0 then goto, LABELEND

		;*************************************
		;**** Read pipe comms from bxsetp ****
		;*************************************

  bxsetp, pipe, sz0, sz, scnte, sil, lfpool, strga


LABEL200:
		;**** Check FORTRAN still running ****

  if find_process(pid) eq 0 then goto, LABELEND

		;************************************************
		;**** Communicate with b6ispf in fortran and ****
		;**** invoke user interface widget for       ****
		;**** Processing options                     ****
		;************************************************
  b6ispf, pipe, lpend, procval, sz, sz0, strga, dsninc, bitfile, gomenu,$
	  FONT_LARGE=font_large, FONT_SMALL=font_small, $
	  EDIT_FONTS=edit_fonts

                ;**** If menu button clicked, tell FORTRAN to stop ****

    if gomenu eq 1 then begin
        printf, pipe, 1
        goto, LABELEND
    endif else begin
        printf, pipe, 0
    endelse

		;**** If cancel selected then goto 100 ****
  if lpend eq 1 then goto, LABEL100

		;**** Fortran processing now in progress. Get ****
		;**** date and time for this run. Put up a    ****
		;**** progress bar until output screen.       ****
	     
  date = xxdate()

  if find_process(pid) EQ 0 then goto, LABELEND


  n_call = procval.maxt*procval.maxd
                                                    
  adas_progressbar, pipe=pipe, n_call=n_call, $
                    adasprog = 'ADAS206', font=font_large


		;**** Create header for output. ****
  
  header = adasrel+adasprog+' DATE: '+date(0)+' TIME: '+date(1)

LABEL300:
		;**** Check FORTRAN still running ****
  if find_process(pid) eq 0 then goto, LABELEND

		;************************************************
		;**** Communicate with b6spf1 in fortran and ****
		;**** invoke user interface widget for       ****
		;**** Output options                         ****
		;************************************************

  b6spf1, pipe, lpend, outval, dsninc, bitfile, gomenu,	$
		DEVLIST=devlist, FONT=font_large


                ;**** If menu button clicked, tell FORTRAN to stop ****

  if gomenu eq 1 then begin
      printf, pipe, 1
      outval.texmes = ''
      outval.conmes = ''
      outval.totmes = ''
      outval.spcmes = ''
      goto, LABELEND
  endif else begin
      printf, pipe, 0
  endelse

		;**** If cancel selected then erase output ****
		;**** messages and goto 200.		   ****
  if lpend eq 1 then begin
    outval.texmes = ''
    outval.conmes = ''
    outval.totmes = ''
    outval.spcmes = ''
    goto, LABEL200
  end

		;*************************************************
		;**** If text output has been requested write ****
		;**** header to pipe for XXADAS to read.      ****
		;*************************************************

  if outval.texout eq 1 then begin
    printf,pipe,header
  end

		;**** If graphical output requested ****

		;************************************************
		;**** Communicate with bxoutg in fortran and ****
		;**** invoke widget Graphical output         ****
		;************************************************
  if outval.grpout eq 1 then begin
		;**** User explicit scaling ****
    grpscal = outval.grpscal
    xmin = outval.xmin
    xmax = outval.xmax
    ymin = outval.ymin
    ymax = outval.ymax
		;**** Hardcopy output ****
    hrdout = outval.hrdout
    hardname = outval.hardname
    if outval.devsel ge 0 then device = devcode(outval.devsel)

    bxoutg, pipe, grpscal, xmin, xmax, ymin, ymax, $
		  hrdout, hardname, device, header, bitfile, gomenu,	$
	 	  FONT=font_large

                ;**** If menu button clicked, tell FORTRAN to stop ****

    if gomenu eq 1 then begin
        printf, pipe, 1
        outval.texmes = ''
   	outval.conmes = ''
    	outval.totmes = ''
    	outval.spcmes = ''
        goto, LABELEND
    endif else begin
        printf, pipe, 0
    endelse

  end

		;**** Back for more output options ****
GOTO, LABEL300

LABELEND:

		;**** Ensure appending is not enabled for ****
		;**** text output at start of next run.   ****
outval.texapp = -1

		;**** Save user defaults and free all units ****

save, inval, procval, outval, filename=deffile

close, /all

END
