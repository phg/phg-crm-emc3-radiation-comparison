; Copyright (c) 1996 Strathclyde University .
; SCCS info: Module @(#)$Header: /home/adascvs/idl/adas2xx/adas206/b6spf1.pro,v 1.1 2004/07/06 11:26:06 whitefor Exp $ Date $Date: 2004/07/06 11:26:06 $
;
;+
; PROJECT:
;       ADAS IBM MVS to DEC UNIX conversion
;
; NAME:
;	B6SPF1
;
; PURPOSE:
;	IDL user interface and communications with ADAS206 FORTRAN
;	process via pipe.
;
; EXPLANATION:
;	First this routine reads some information from ADAS206 FORTRAN
;	via the UNIX pipe.  Next the 'Output Options' part of the
;	interface is invoked.  Finally the results of the user's
;	interactions are written back to the FORTRAN via the pipe.
;	Communications are with the FORTRAN subroutine B6SPF1.
;
; USE:
;	The user of this routine is specific to ADAS206, See adas206.pro.
;
; INPUTS:
;	PIPE	- The IDL unit number of the bi-directional pipe to the
;		  ADAS206 FORTRAN process.
;
;	VALUE	- A structure which determines the initial settings of
;		  the dataset selection widget.  The initial value is
;		  set in adas206.pro.  VALUE is passed un-modified
;		  through to cw_adas_in.pro, see that routine for a full
;		  description.
;
;	DSNINC	- String; The full system file name of the input COPASE
;		  dataset selected by the user for processing.
;
;       BITFILE - String; the path to the dirctory containing bitmaps
;                 for the 'escape to series menu' button.
;
; OPTIONAL INPUTS:
;	None.
;
; OUTPUTS:
;	LPEND	- Integer; indicates user action on leaving output
;		  selections.  lpend is set to 1 if the user exited
;		  with the 'Cancel' button and 0 if the user exited
;		  with 'Done'.
;
;	VALUE	- On output the structure records the final settings of
;		  the output selection widget if the user pressed the
;		  'Done' button otherwise it is not changed from input.
;
;       GOMENU  - Int; flag - set to 1 if user has selected 'escape direct
;                 to series menu , otherwise 0
;
; OPTIONAL OUTPUTS:
;	None.
;
; KEYWORD PARAMETERS:
;	DEVLIST	- A string array;  Each element of the array is the
;		  name of a hardcopy graphics output device.  This
;		  list must mirror the DEVCODE list.  DEVLIST is the
;		  list which the user will see.
;		  e.g ['Post-Script','HP-PCL','HP-GL']
;
;	FONT	- Supplies the font to be used for the interface widgets.
;
; CALLS:
;	ADAS206_OUT	User interface - output options.
;
; SIDE EFFECTS:
;	This routine communicates with the ADAS206 FORTRAN process
;	via a UNIX pipe.
;
; CATEGORY:
;	Adas system.
;
; WRITTEN:
;       William Osborn, Tessella Support Services plc, 07-Jun-1996
;
; MODIFIED:
;       1.1     William Osborn
;               First release.
;
; VERSION:
;       1.1     07-06-93
;
;-
;-----------------------------------------------------------------------------


PRO b6spf1, pipe, lpend, value, dsninc, bitfile, gomenu,	$
		DEVLIST=devlist, FONT=font


                ;**** Set defaults for keywords ****
  IF NOT (KEYWORD_SET(font)) THEN font = ''

		;********************************
		;**** Read data from fortran ****
		;********************************
  maxt = 0
  ifout = 0
  readf,pipe,maxt

  tine = dblarr(maxt)
  readf,pipe,tine
  readf,pipe,ifout

		;*******************************
		;**** Make a list of graphs ****
		;*******************************
  if ifout eq 1 then units=' K' else $
  if ifout eq 2 then units=' eV' else $
	units=' Red.'
  grplist = strarr(maxt)
  for i = 0, maxt-1 do begin
    grplist(i) = string(tine(i),format='(E10.3)')+units
  end
  

		;**************************************
		;**** Pop-up output options widget ****
		;**************************************
  adas206_out, value, dsninc, action, bitfile, $
		GRPLIST=grplist, DEVLIST=devlist, FONT=font

		;***********************************************
		;**** Act on the output from the widget     ****
		;***********************************************
		;**** There are only three possible actions ****
		;**** 'Done', 'Cancel' and 'Menu'.          ****
		;***********************************************
  gomenu = 0
  if action eq 'Done' then begin
    lpend = 0
  endif else if action eq 'Menu' then begin
    lpend = 1
    gomenu = 1
  endif else begin
    lpend = 1
  end

		;*******************************
		;**** Write data to fortran ****
		;*******************************
  printf,pipe,lpend

  if lpend eq 0 then begin

    printf,pipe,value.grpout
    if value.grpout eq 1 then begin
		;**** add one to convert IDL array index to FORTRAN index ****
      printf,pipe,value.grpsel + 1
      printf,pipe,value.gtit1
    end

    printf,pipe,value.texout
    if value.texout eq 1 then begin
      printf,pipe,value.texapp
      printf,pipe,value.texdsn
    end

    printf,pipe,value.conout
    if value.conout eq 1 then printf,pipe,value.condsn

    printf,pipe,value.totout
    if value.totout eq 1 then printf,pipe,value.totdsn

    printf,pipe,value.spcout
    if value.spcout eq 1 then printf,pipe,value.spcdsn

  end

		;**************************************************
		;**** Set messages and settings ready for when ****
		;**** output window is re-displayed.           ****
		;**************************************************

  if lpend eq 0 then begin
    if value.texout eq 1 then begin
		;**** If text output is requested enable append ****
		;**** for next time and update the default to   ****
		;**** the current text file.                    ****
      value.texapp=0
      value.texdef = value.texdsn
      if value.texrep ge 0 then value.texrep = 0
      value.texmes = 'Output written to file.'
    end

    if value.conout eq 1 then begin
      value.conout = 0
      if value.conrep ge 0 then value.conrep = 0
      value.conmes = 'Output written to file.'
    end

    if value.totout eq 1 then begin
      value.totout = 0
      if value.totrep ge 0 then value.totrep = 0
      value.totmes = 'Output written to file.'
    end

    if value.spcout eq 1 then begin
      value.spcout = 0
      if value.spcrep ge 0 then value.spcrep = 0
      value.spcmes = 'Output written to file.'
    end

    if value.hrdout eq 1 then begin
    end
  end

END
