; Copyright (c) 1996 Strathclyde University .
; SCCS info: Module @(#)$Header: /home/adascvs/idl/adas2xx/adas206/cw_adas206_out.pro,v 1.2 2004/07/06 12:31:52 whitefor Exp $ Date $Date: 2004/07/06 12:31:52 $
;+
; PROJECT:
;       ADAS IBM MVS to DEC UNIX conversion
;
; NAME:
;	CW_ADAS206_OUT()
;
; PURPOSE:
;	Produces a widget for ADAS206 output options.
;
; EXPLANATION:
;	This function declares a compound widget consisting of the
;	graphical output selection widget cw_adas_gr_sel.pro, and three
;	output file widgets cw_adas_outfile.pro.  The three text output
;	files specified in this widget are for tabular (paper.txt)
;	output, countour passing file, total line power file and specific
;	line power file.  This
;	widget also includes a button for browsing the comments from
;	the input dataset, a 'Cancel' button, a 'Done' button and a
;	'Menu' button.
;
;	The compound widgets cw_adas_gr_sel.pro and cw_adas_outfile.pro
;	included in this file are self managing.  This widget only
;	handles events from the 'Done', 'Cancel' and 'Menu' buttons.
;
;	This widget only generates events for the 'Done', 'Cancel' and 'Menu'
;	buttons.
;
; USE:
;	This widget is specific to ADAS206, see adas206_out.pro	for use.
;
; INPUTS:
;	PARENT	- Long integer; ID of parent widget.
;
;	DSNINC	- Name of input dataset for this application.
;
;       BITFILE - String; the path to the dirctory containing bitmaps
;                 for the 'escape to series menu' button.
;
; OPTIONAL INPUTS:
;	None.
;
; OUTPUTS:
;	The return value of this function is the ID of the declared widget.
;
; OPTIONAL OUTPUTS:
;	None.
;
; KEYWORD PARAMETERS:
;	GRPLIST	- A string array; A list of graphs from which to choose.
;		  For ADAS206 this is a list of electron temperatures.
;		  One element of the array for each electron temperature.
;
;	DEVLIST	- A string array;  Each element of the array is the
;		  name of a hardcopy graphics output device.  This
;		  list must mirror the DEVCODE list.  DEVLIST is the
;		  list which the user will see.
;		  e.g ['Post-Script','HP-PCL','HP-GL']
;
;	VALUE	- A structure which determines the initial settings of
;		  the output options widget.  The structure is made
;		  up of four parts.  Each part is the same as the value
;		  structure of one of the four main compound widgets
;		  included in this widget.  See cw_adas_gr_sel and
;		  cw_adas_outfile for more details.  The default value is;
;
;		      { GRPOUT:0, GTIT1:'', 				$
;			GRPSCAL:0, 					$
;			XMIN:'',  XMAX:'',   				$
;			YMIN:'',  YMAX:'',   				$
;			HRDOUT:0, HARDNAME:'', 				$
;			GRPDEF:'', GRPFMESS:'', 			$
;			GRPSEL:-1, GRPRMESS:'', 			$
;			DEVSEL:-1, GRSELMESS:'', 			$
;			TEXOUT:0, TEXAPP:-1, 				$
;			TEXREP:0, TEXDSN:'', 				$
;			TEXDEF:'',TEXMES:'', 				$
;			CONOUT:0, CONAPP:-1, 				$
;			CONREP:0, CONDSN:'', 				$
;			CONDEF:'',CONMES:'', 				$
;			TOTOUT:0, TOTAPP:-1, 				$
;			TOTREP:0, TOTDSN:'', 				$
;			TOTDEF:'',TOTMES:'',				$
;			SPCOUT:0, SPCAPP:-1, 				$
;			SPCREP:0, SPCDSN:'', 				$
;			SPCDEF:'',SPCMES:'',				$
;			FLIPVAL:0   					}
;
;		  For CW_ADAS_GR_SEL;
;			GRPOUT	   Integer; Activation button 1 on, 0 off
;			GTIT1	   String; Graph title
;			GRPSCAL	   Integer; Scaling activation 1 on, 0 off
;			XMIN	   String; x-axis minimum, string of number
;			XMAX	   String; x-axis maximum, string of number
;			YMIN	   String; y-axis minimum, string of number
;			YMAX	   String; y-axis maximum, string of number
;			HRDOUT	   Integer; Hard copy activ' 1 on, 0 off
;			HARDNAME   String; Hard copy output file name
;			GRPDEF	   String; Default output file name 
;			GRPFMESS   String; File name error message
;			GRPSEL	   Integer; index of selected graph in GRPLIST
;			GRPRMESS   String; Scaling ranges error message
;			DEVSEL	   Integer; index of selected device in DEVLIST
;			GRSELMESS  String; General error message
;
;		  For CW_ADAS_OUTFILE tabular output (paper.txt);
;			TEXOUT	Integer; Activation button 1 on, 0 off
;			TEXAPP	Integer; Append button 1 on, 0 off, -1 no button
;			TEXREP	Integer; Replace but' 1 on, 0 off, -1 no button
;			TEXDSN	String; Output file name
;			TEXDEF	String; Default file name
;			TEXMES	String; file name error message
;
;		  For CW_ADAS_OUTFILE contour passing file;
;			CONOUT  Integer; Activation button 1 on, 0 off
;			CONAPP	Integer; Append button 1 on, 0 off, -1 no button
;			CONREP	Integer; Replace but' 1 on, 0 off, -1 no button
;			CONDSN	String; Output file name
;			CONDEF	String; Default file name
;			CONMES	String; file name error message
;
;		  FOR CW_ADAS_OUTFILE Totpop passing file;
;			TOTOUT	Integer; Activation button 1 on, 0 off
;			TOTAPP	Integer; Append button 1 on, 0 off, -1 no button
;			TOTREP	Integer; Replace but' 1 on, 0 off, -1 no button
;			TOTDSN	String; Output file name
;			TOTDEF	String; Default file name
;			TOTMES	String; file name error message
;		  FOR CW_ADAS_OUTFILE Spcpop passing file;
;			SPCOUT	Integer; Activation button 1 on, 0 off
;			SPCAPP	Integer; Append button 1 on, 0 off, -1 no button
;			SPCREP	Integer; Replace but' 1 on, 0 off, -1 no button
;			SPCDSN	String; Output file name
;			SPCDEF	String; Default file name
;			SPCMES	String; file name error message
;		  FOR OVERALL WIDGET;
;			FLIPVAL Integer; Flag showing whether graphics options
;					 should be displayed (1) or text (0)
;
;	UVALUE	- A user value for this widget.
;
;	FONT	- String; the name of a font to be used for all text in
;		  this widget.  Default to system default font.
;
; CALLS:
;	CW_ADAS_DSBR	Input dataset name and comments browse button.
;	CW_ADAS_GR_SEL	Graphical output selection widget.
;	CW_ADAS_OUTFILE	Output file name entry widget.
;	See side effects for other related widget management routines.
;
; SIDE EFFECTS:
;       A COMMON BLOCK: CW_OUT206_BLK is used which is private to
;       this widget.
;
;	OUT206_GET_VAL()
;	OUT206_EVENT()
;
; CATEGORY:
;	Compound Widget
;
; WRITTEN:
;       William Osborn, Tessella Support Services plc, 7-Jun-1996
;
; MODIFIED:
;       1.1	William Osborn   
;               First release.
;	1.2	William Osborn
;		Removed xsize=n keyword from widget_label command.
;		The dynlabel procedure does not override these
;		assignments and they were taken as being values in pixels
;		on some machines.
;
; VERSION:
;       1.1     07-06-96
;	1.2	01-08-96
;
;-
;-----------------------------------------------------------------------------

FUNCTION out206_get_val, id

                ;**** Return to caller on error ****
  ON_ERROR, 2

		;************************************
                ;**** Retrieve the state: it's   ****
		;**** stored as the uvalue of the****
		;**** first child base		 ****
		;************************************

  first_child = widget_info(id, /child)
  widget_control, first_child, get_uvalue=state

		;**** Get graphical output settings ****
  widget_control,state.grpid,get_value=grselval

		;**** Get text output settings ****
  widget_control,state.paperid,get_value=papos

		;**** Get contour file settings ****
  widget_control,state.contid,get_value=conos

		;**** Get total power file settings ****
  widget_control,state.totpid,get_value=totos

		;**** Get specific line power file settings ****
  widget_control,state.spcpid,get_value=spcos

		;**** Get flip-switch setting ****

  widget_control, state.flipid, get_value=flipval

  os = { 	GRPOUT:grselval.outbut, GTIT1:grselval.gtit1, 		$
		GRPSCAL:grselval.scalbut, 				$
		XMIN:grselval.xmin,     XMAX:grselval.xmax,   		$
		YMIN:grselval.ymin,     YMAX:grselval.ymax,   		$
		HRDOUT:grselval.hrdout, HARDNAME:grselval.hardname, 	$
		GRPDEF:grselval.grpdef, GRPFMESS:grselval.grpfmess, 	$
		GRPSEL:grselval.grpsel, GRPRMESS:grselval.grprmess, 	$
		DEVSEL:grselval.devsel, GRSELMESS:grselval.grselmess, 	$
		TEXOUT:papos.outbut,  TEXAPP:papos.appbut, 		$
		TEXREP:papos.repbut,  TEXDSN:papos.filename, 		$
		TEXDEF:papos.defname, TEXMES:papos.message, 		$
		CONOUT:conos.outbut,  CONAPP:conos.appbut, 		$
		CONREP:conos.repbut,  CONDSN:conos.filename, 		$
		CONDEF:conos.defname, CONMES:conos.message, 		$
		TOTOUT:totos.outbut,  TOTAPP:totos.appbut, 		$
		TOTREP:totos.repbut,  TOTDSN:totos.filename, 		$
		TOTDEF:totos.defname, TOTMES:totos.message,   		$
		SPCOUT:spcos.outbut,  SPCAPP:spcos.appbut, 		$
		SPCREP:spcos.repbut,  SPCDSN:spcos.filename, 		$
		SPCDEF:spcos.defname, SPCMES:spcos.message,   		$
		FLIPVAL:flipval(0)					}

  RETURN, os 

END

;-----------------------------------------------------------------------------

FUNCTION out206_event, event

                ;**** Base ID of compound widget ****
  parent=event.handler

		;************************************
                ;**** Retrieve the state: it's   ****
		;**** stored as the uvalue of the****
		;**** first child base		 ****
		;************************************

  first_child = widget_info(parent, /child)
  widget_control, first_child, get_uvalue=state, /no_copy

		;*********************************
		;**** Clear previous messages ****
		;*********************************
  widget_control,state.messid,set_value=' '

                ;************************
                ;**** Process events ****
                ;************************
  CASE event.id OF

		;*****************************************************
		;**** Change between graphics/text output display ****
		;*****************************************************

    state.flipid: begin
	widget_control, state.flipid, get_value=flipval
  	widget_control, state.graphbase, map=(1-flipval(0))
	widget_control, state.textbase, map=flipval(0)
    end
		;***********************
		;**** Cancel button ****
		;***********************

    state.cancelid: new_event = {ID:parent, TOP:event.top, 		$
				HANDLER:0L, ACTION:'Cancel'}

		;*********************
		;**** Done button ****
		;*********************

    state.doneid: begin

		;***************************************
		;**** Check for errors in the input ****
		;***************************************
	  error = 0

		;**** Get a copy of the widget value ****
		;**** Must restore state before calling get_value ****
          widget_control, first_child, set_uvalue=state, /no_copy
	  widget_control,event.handler,get_value=os
          widget_control, first_child, get_uvalue=state, /no_copy

		;**** Check for widget error messages ****
	  if os.grpout eq 1 and strtrim(os.grpfmess) ne '' then error=1
	  if os.grpout eq 1 and os.grpscal eq 1 and $
				strtrim(os.grprmess) ne '' then error=1
	  if os.grpout eq 1 and strtrim(os.grselmess) ne '' then error=1
	  if os.texout eq 1 and strtrim(os.texmes) ne '' then error=1
	  if os.conout eq 1 and strtrim(os.conmes) ne '' then error=1
	  if os.totout eq 1 and strtrim(os.totmes) ne '' then error=1
	  if os.spcout eq 1 and strtrim(os.spcmes) ne '' then error=1


	  if error eq 1 then begin
	    widget_control,state.messid,set_value= $
				'**** Error in output settings ****'
	    new_event = 0L
	  end else begin
	    new_event = {ID:parent, TOP:event.top, HANDLER:0L, ACTION:'Done'}
	  end

        end

                ;**** Menu button ****

    state.outid: begin
            new_event = {ID:parent, TOP:event.top, HANDLER:0L,          $
                         ACTION:'Menu'}
        end

    ELSE: new_event = 0L

  ENDCASE
		;*********************************************
		;*** make "state" available to other files ***
		;*********************************************

  widget_control, first_child, set_uvalue=state,/no_copy


  RETURN, new_event
END

;-----------------------------------------------------------------------------

FUNCTION cw_adas206_out, parent, dsninc, bitfile,		$
		GRPLIST=grplist, DEVLIST=devlist, $
		VALUE=value, UVALUE=uvalue, $
		FONT=font


  COMMON cw_out206_blk, state_base, state_stash, state

  IF (N_PARAMS() EQ 0) THEN MESSAGE, 'Must specify parent for cw_adas206_out'
  ON_ERROR, 2					;return to caller

		;**** Set defaults for keywords ****
  IF NOT (KEYWORD_SET(grplist)) THEN grplist = ''
  IF NOT (KEYWORD_SET(devlist)) THEN devlist = ''
  IF NOT (KEYWORD_SET(uvalue)) THEN uvalue = 0
  IF NOT (KEYWORD_SET(font)) THEN font = ''
  IF NOT (KEYWORD_SET(value)) THEN begin
	os = {out206_set, 						$
			GRPOUT:0, GTIT1:'', 				$
			GRPSCAL:0, 					$
			XMIN:'',  XMAX:'',   				$
			YMIN:'',  YMAX:'',   				$
			HRDOUT:0, HARDNAME:'', 				$
			GRPDEF:'', GRPFMESS:'', 			$
			GRPSEL:-1, GRPRMESS:'', 			$
			DEVSEL:-1, GRSELMESS:'', 			$
			TEXOUT:0, TEXAPP:-1, 				$
			TEXREP:0, TEXDSN:'', 				$
                        TEXDEF:'',TEXMES:'', 				$
			CONOUT:0, CONAPP:-1, 				$
			CONREP:0, CONDSN:'', 				$
                        CONDEF:'',CONMES:'', 				$
			TOTOUT:0, TOTAPP:-1, 				$
			TOTREP:0, TOTDSN:'', 				$
			TOTDEF:'',TOTMES:'',				$
			SPCOUT:0, SPCAPP:-1, 				$
			SPCREP:0, SPCDSN:'', 				$
			SPCDEF:'',SPCMES:'',				$
			FLIPVAL:0   					}
  END ELSE BEGIN
	os = {out206_set, 						$
			GRPOUT:value.grpout, GTIT1:value.gtit1, 	$
			GRPSCAL:value.grpscal, 				$
			XMIN:value.xmin,     XMAX:value.xmax,   	$
			YMIN:value.ymin,     YMAX:value.ymax,   	$
			HRDOUT:value.hrdout, HARDNAME:value.hardname, 	$
			GRPDEF:value.grpdef, GRPFMESS:value.grpfmess, 	$
			GRPSEL:value.grpsel, GRPRMESS:value.grprmess, 	$
			DEVSEL:value.devsel, GRSELMESS:value.grselmess, $
			TEXOUT:value.texout, TEXAPP:value.texapp, 	$
			TEXREP:value.texrep, TEXDSN:value.texdsn, 	$
			TEXDEF:value.texdef, TEXMES:value.texmes, 	$
			CONOUT:value.conout, CONAPP:value.conapp, 	$
			CONREP:value.conrep, CONDSN:value.condsn, 	$
			CONDEF:value.condef, CONMES:value.conmes, 	$
			TOTOUT:value.totout, TOTAPP:value.totapp, 	$
			TOTREP:value.totrep, TOTDSN:value.totdsn, 	$
			TOTDEF:value.totdef, TOTMES:value.totmes,	$
			SPCOUT:value.spcout, SPCAPP:value.spcapp, 	$
			SPCREP:value.spcrep, SPCDSN:value.spcdsn, 	$
			SPCDEF:value.spcdef, SPCMES:value.spcmes,	$
			FLIPVAL:value.flipval   			}
  END

		;**********************************************
		;**** Create the 206 Output options widget ****
		;**********************************************

		;**** create base widget ****
  cwid = widget_base( parent, UVALUE = uvalue, $
			EVENT_FUNC = "out206_event", $
			FUNC_GET_VALUE = "out206_get_val", $
			/COLUMN)
  first_child = widget_base(cwid)

		;**** Add dataset name and browse button ****
  rc = cw_adas_dsbr(cwid,dsninc,font=font)

		;****************************************************
		;**** Add another base for the switching between ****
		;**** Graphics and text output options           ****
		;****************************************************

  flipbase2 = widget_base(cwid, /row)
  flipbase = widget_base(cwid)
  labelval = 'Select output option settings for display: '
  flipid = cw_bgroup(flipbase2, ['Graphics','Text'], /exclusive,	$
		     font=font, label_left=labelval, /row,		$
		     set_value=os.flipval)

		;**** Widget for graphics selection ****
		;**** Note change in names for GRPOUT and GRPSCAL ****

  grselval = {  OUTBUT:os.grpout, GTIT1:os.gtit1, SCALBUT:os.grpscal, $
		XMIN:os.xmin, XMAX:os.xmax, $
		YMIN:os.ymin, YMAX:os.ymax, $
		HRDOUT:os.hrdout, HARDNAME:os.hardname, $
		GRPDEF:os.grpdef, GRPFMESS:os.grpfmess, $
		GRPSEL:os.grpsel, GRPRMESS:os.grprmess, $
		DEVSEL:os.devsel, GRSELMESS:os.grselmess }
  base = widget_base(flipbase,/row,/frame)
  graphbase = base
  grpid = cw_adas_gr_sel(base, OUTPUT='Graphical Output', GRPLIST=grplist, $
                        LISTTITLE='Graph Temperature', $
			DEVLIST=devlist, $
                        VALUE=grselval, FONT=font)
  widget_control, base, map=1-os.flipval

		;**** Overall base for text output ****

  textbase = widget_base(flipbase, /column)

		;**** Widget for text output ****

  outfval = { OUTBUT:os.texout, APPBUT:os.texapp, REPBUT:os.texrep, $
	      FILENAME:os.texdsn, DEFNAME:os.texdef, MESSAGE:os.texmes }
  base = widget_base(textbase,/row,/frame)
  paperid = cw_adas_outfile(base, OUTPUT='Text Output', $
                        VALUE=outfval, FONT=font)

		;**** Widget for contour passing file ****

  outfval = { OUTBUT:os.conout, APPBUT:os.conapp, REPBUT:os.conrep, $
	      FILENAME:os.condsn, DEFNAME:os.condef, MESSAGE:os.conmes }
  base = widget_base(textbase,/row,/frame)
  contid = cw_adas_outfile(base, OUTPUT='Contour File', $
                        VALUE=outfval, FONT=font)

		;**** Widget for total line power file ****

  outfval = { OUTBUT:os.totout, APPBUT:os.totapp, REPBUT:os.totrep, $
	      FILENAME:os.totdsn, DEFNAME:os.totdef, MESSAGE:os.totmes }
  base = widget_base(textbase,/row,/frame)
  totpid = cw_adas_outfile(base, OUTPUT='Total Line Power File', $
                        VALUE=outfval, FONT=font)

		;**** Widget for specific line power file ****

  outfval = { OUTBUT:os.spcout, APPBUT:os.spcapp, REPBUT:os.spcrep, $
	      FILENAME:os.spcdsn, DEFNAME:os.spcdef, MESSAGE:os.spcmes }
  base = widget_base(textbase,/row,/frame)
  spcpid = cw_adas_outfile(base, OUTPUT='Specific Line Power File', $
                        VALUE=outfval, FONT=font)

  widget_control, textbase, map=os.flipval

		;**** Error message ****

  messid = widget_label(cwid,value=' ',font=font)

		;**** add the exit buttons ****

  base = widget_base(cwid,/row)
  menufile = bitfile + '/menu.bmp'
  read_X11_bitmap, menufile, bitmap1
  outid = widget_button(base, value=bitmap1)          ;menu button
  cancelid = widget_button(base,value='Cancel',font=font)
  doneid = widget_button(base,value='Done',font=font)
  
		;*************************************************
		;**** create a state structure for the pop-up ****
		;**** window.                                 ****
		;*************************************************

  new_state = { GRPID		:	grpid, 				$
		OUTID		:	outid,				$
		PAPERID		:	paperid, 			$
		CONTID		:	contid, 			$
		TOTPID		:	totpid, 			$
		SPCPID		:	spcpid, 			$
		CANCELID	:	cancelid, 			$
		DONEID		:	doneid, 			$
		MESSID		:	messid,				$
		GRAPHBASE	:	graphbase,			$
		TEXTBASE	:	textbase,			$
		FLIPID		:	flipid				}

                ;**** Save initial state structure ****
		;**** using the widget's uvalue    ****

  widget_control, first_child, set_uvalue=new_state,/no_copy

  RETURN, cwid

END

