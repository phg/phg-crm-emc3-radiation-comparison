; Copyright (c) 1996 Strathclyde University .
; SCCS info: Module @(#)$Header: /home/adascvs/idl/adas2xx/adas206/adas206_out.pro,v 1.1 2004/07/06 10:17:25 whitefor Exp $ Date $Date: 2004/07/06 10:17:25 $
;
;+
; PROJECT:
;       ADAS IBM MVS to DEC UNIX conversion
;
; NAME:
;	ADAS206_OUT
;
; PURPOSE:
;	IDL ADAS user interface, output options.
;
; EXPLANATION:
;	This routine creates and manages a pop-up window which allows
;	the user to select options and input data to control ADAS206
;	graphical and file output.
;
; USE:
;	This routine is ADAS206 specific, see b6spf1.pro for how it
;	is used.
;
; INPUTS:
;	VAL	- A structure which determines the initial settings of
;		  the output options widget.  The value is passed
;		  unmodified into cw_adas206_out.pro.
;
;		  See cw_adas206_out.pro for a full description of this
;		  structure.
;
;	DSNINC	- String; The full system file name of the input COPASE
;		  dataset selected by the user for processing.
;
; OPTIONAL INPUTS:
;	None.
;
; OUTPUTS:
;	ACT	- String; Either 'Done' or 'Cancel' for the button the
;		  user pressed to terminate the output options window.
;
; OPTIONAL OUTPUTS:
;	None.
;
; KEYWORD PARAMETERS:
;	GRPLIST	- A string array; A list of graphs from which to choose.
;		  For ADAS206 this is a list of electron temperatures.
;		  One element of the array for each electron temperature.
;
;	DEVLIST	- A string array;  Each element of the array is the
;		  name of a hardcopy graphics output device.  This
;		  list must mirror the DEVCODE list.  DEVLIST is the
;		  list which the user will see.
;		  e.g ['Post-Script','HP-PCL','HP-GL']
;
;	FONT	- Supplies the font to be used for the interface widgets.
;
;       BITFILE - String; the path to the dirctory containing bitmaps
;                 for the 'escape to series menu' button.
;
; CALLS:
;	CW_ADAS206_OUT	Creates the output options widget.
;	XMANAGER
;	See side effects for widget management routine.
;
; SIDE EFFECTS:
;	This routine uses a common block OUT206_BLK to maintain its state.
;	ADAS206_OUT_EV	is included in this file and is called
;			indirectly from XMANAGER during widget
;			management.
;
; CATEGORY:
;	Widgets
;
; WRITTEN:
;       William Osborn, Tessella Support Services plc, 7-Jun-1996
;
; MODIFIED:
;       1.1     William Osborn
;               First release.
;
; VERSION:
;       1.1     07-06-96
;
;-

;-----------------------------------------------------------------------------


PRO adas206_out_ev, event

  COMMON out206_blk,action,value

		;**** Find the event type and copy to common ****
    action = event.action

    CASE action OF

		;**** 'Done' button ****
	'Done'  : begin

			;**** Get the output widget value ****
		widget_control,event.id,get_value=value 

			;**** Kill the widget to allow IDL to ****
			;**** continue and interface with     ****
			;**** FORTRAN only if there is work   ****
			;**** for the FORTRAN to do.          ****
		if (value.grpout eq 1) or $
		   (value.texout eq 1) or $
		   (value.conout eq 1) or $
		   (value.totout eq 1) or $
		   (value.spcout eq 1) then widget_control,event.top,/destroy

	   end


		;**** 'Cancel' button ****
	'Cancel': widget_control,event.top,/destroy

		;**** 'Menu' button ****
	'Menu': widget_control,event.top,/destroy

    END

END

;-----------------------------------------------------------------------------


PRO adas206_out, val, dsninc, act, bitfile,		$
	GRPLIST=grplist, DEVLIST=devlist, FONT = font

  COMMON out206_blk,action,value

		;**** Copy value to common ****
  value = val

		;**** Set defaults for keywords ****
  IF NOT (KEYWORD_SET(font)) THEN font = ''
  IF NOT (KEYWORD_SET(grplist)) THEN grplist = ''
  IF NOT (KEYWORD_SET(devlist)) THEN devlist = ''

		;***************************************
		;**** Pop-up a new widget if needed ****
		;***************************************

                ;**** create base widget ****
  outid = widget_base(TITLE='ADAS206 OUTPUT OPTIONS',XOFFSET=100,YOFFSET=0)

		;**** Declare output options widget ****
  cwid = cw_adas206_out(outid, dsninc, bitfile, VALUE=value, GRPLIST=grplist, $
			DEVLIST=devlist, FONT=font )

		;**** Realize the new widget ****
  dynlabel, outid
  widget_control,outid,/realize

		;**** make widget modal ****
  xmanager,'adas206_out',outid,event_handler='adas206_out_ev',/modal,/just_reg
 
		;**** Return the output value from common ****
  act = action
  val = value

END

