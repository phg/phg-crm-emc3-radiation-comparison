; Copyright (c) 1996 Strathclyde University .
; SCCS info: Module @(#)$Header: /home/adascvs/idl/adas2xx/adas206/b6ispf.pro,v 1.2 2008/03/25 14:02:23 allan Exp $ Date $Date: 2008/03/25 14:02:23 $
;+
; PROJECT:
;       ADAS IBM MVS to DEC UNIX conversion
;
; NAME:
;	B6ISPF
;
; PURPOSE:
;	IDL user interface and communications with ADAS206 FORTRAN
;	process via pipe.
;
; EXPLANATION:
;	The routine begins by reading information from the ADAS206
;	FORTRAN process via a UNIX pipe.  Then part of the ADAS206
;	IDL user interface is invoked to determine how the user
;	wishes to process the input dataset.  When the user's
;	interactions are complete the information gathered with
;	the user interface is written back to the FORTRAN process
;	via the pipe.  Communications are to the FORTRAN subroutine
;	B6ISPF.
;
; USE:
;	The use of this routine is specific to ADAS206, see adas206.pro.
;
; INPUTS:
;	PIPE	- The IDL unit number of the bi-directional pipe to the
;		  ADAS206 FORTRAN process.
;
;	LPEND	- Integer, 0 if the user pressed the 'Done' button
;		  or 1 if the user pressed 'Cancel'.  Maps directly onto
;		  the logical variable LPEND in ADAS206 FORTRAN.
;
;	VALUE	- A structure which determines the initial settings of
;		  the processing options widget.  The initial value is
;		  set in adas206.pro.  If adas206.pro passes a blank 
;		  dummy structure of the form {NMET:-1} into VALUE then
;		  VALUE is reset to a default structure.
;
;		  The VALUE structure is;
;		     {  TITLE:'',	IPROJ:0,    NMET:0, $
;			IMETR:intarr(),	ISTRN:0,    IFOUT:0, $
;			MAXT:0,		TINE:dblarr(), $
;			TINP:dblarr(),	TINH:dblarr(), $
;			IDOUT:1,	MAXD:0, $
;			DINE:dblarr(),	DINP:dblarr(), $
;			RATHA:dblarr(),	RATIA:dblarr(), $
;			ZEFF:'',	LLSEL:0,    LPSEL:0, $
;			LZSEL:0,	LISEL:0, $
;			LHSEL:0,	LNORM:0,    LRSEL:0, $
;			TDTOG:0  }
;
;		  See cw_adas206_proc.pro for a full description of this
;		  structure and related variables ndmet, ndtem and ndden.
;
;	SZ	- String, recombined ion charge read.
;
;	SZ0	- String, nuclear charge read.
;
;	STRGA	- String array, level designations.
;
;	DSNINC	- String; The full system file name of the input COPASE
;		  dataset selected by the user for processing.
;
;       BITFILE - String; the path to the dirctory containing bitmaps
;                 for the 'escape to series menu' button.
;
; OPTIONAL INPUTS:
;	None
;
; OUTPUTS:
;	VALUE	- On output the structure records the final settings of
;		  the processing selections widget if the user pressed the
;		  'Done' button, otherwise it is not changed from input.
;
;       GOMENU  - Int; flag - set to 1 if user has selected 'escape direct
;                 to series menu , otherwise 0
;
; OPTIONAL OUTPUTS:
;	None
;
; KEYWORD PARAMETERS:
;	FONT_LARGE - The name of a larger font.
;
;	FONT_SMALL - The name of a smaller font.
;
;	EDIT_FONTS - A structure of two fonts used in the table editor
;		     adas_edtab.pro, {font_norm:'',font_input:''}.
;
; CALLS:
;	ADAS206_PROC	Invoke the IDL interface for ADAS206 data
;		 	processing options/input.
;
; SIDE EFFECTS:
;	Two way communications with ADAS206 FORTRAN via the
;	bi-directional UNIX pipe.
;
; CATEGORY:
;	Adas system.
;
; WRITTEN:
;       William Osborn, Tessella Support Services plc, 6-Jun-1996
;
; MODIFIED:
;       1.1	William Osborn
;               First release.
;
;       1.2	Allan Whiteford
;               Changed loop variable from int to long to allow
;		for more than 32,768 levels.
;
; VERSION:
;       1.1	06-06-96
;       1.2	25-03-08
;
;-
;-----------------------------------------------------------------------------


PRO b6ispf, pipe, lpend, value, $
		sz, sz0, strga, $
		dsninc, bitfile, gomenu,	$
		FONT_LARGE=font_large, FONT_SMALL=font_small, $
		EDIT_FONTS=edit_fonts


                ;**** Set defaults for keywords ****
  IF NOT (KEYWORD_SET(font_large)) THEN font_large = ''
  IF NOT (KEYWORD_SET(font_small)) THEN font_small = ''
  IF NOT (KEYWORD_SET(edit_fonts)) THEN $
                                edit_fonts = {font_norm:'',font_input:''}


		;**** Declare variables for input ****
  lpend = 0
  ndtem = 0
  ndden = 0
  ndmet = 0
  il = 0
  nv = 0
  icnte = 0L
  tscef = dblarr(14,3)
  dtemp = 0.0
  itemp = 0
		;********************************
		;**** Read data from fortran ****
		;********************************
  readf,pipe,lpend
  readf,pipe,ndtem
  readf,pipe,ndden
  readf,pipe,ndmet
  readf,pipe,il
  readf,pipe,nv
  for i=0,13 do begin
     for j=0,2 do begin
  	readf,pipe,dtemp
	tscef(i,j)=dtemp
     end
  end
  readf,pipe,icnte
  ie1a = intarr(icnte)
  for i=0L,icnte-1 do begin
	readf,pipe,itemp
	ie1a(i)=itemp
  end
  ie2a = intarr(icnte)
  for i=0L,icnte-1 do begin
	readf,pipe,itemp
	ie2a(i)=itemp
  end

		;*******************************************
		;**** Set default value if not provided ****
		;*******************************************
  if value.nmet lt 0 then begin
    imetr = make_array(ndmet,/int,value=0)
    temp = dblarr(ndtem)
    dens = dblarr(ndden)
    value = {	TITLE:'',       IPROJ:0,    NMET:0, $
		IMETR:imetr,    ISTRN:0,    IFOUT:1, $
		MAXT:0,         TINE:temp, $
		TINP:temp,      TINH:temp, $
		IDOUT:1,        MAXD:0, $
		DINE:dens,      DINP:dens, $
		RATHA:dens,     RATIA:dens, $
		ZEFF:'',        LLSEL:0,  $
		LPSEL:0, 	LZSEL:0,        LISEL:0, $
		LHSEL:0,        LNORM:0,  	LRSEL:0, $
		TDTOG:0  }
  end

		;****************************************
		;**** Pop-up processing input widget ****
		;****************************************
  adas206_proc, value, sz, sz0, strga, dsninc, $
		ndtem, ndden, ndmet, il, nv, tscef, icnte, ie1a, ie2a, $
		action, bitfile, $
		FONT_LARGE=font_large, FONT_SMALL=font_small, $
		EDIT_FONTS=edit_fonts


		;**********************************************
		;**** Act on the event from the widget ********
		;**** There are only three possible events ****
		;**** 'Done', 'Cancel' and 'Menu'.         ****
		;**********************************************
  gomenu = 0
  if action eq 'Done' then begin
    lpend = 0
  endif else if action eq 'Menu' then begin
    lpend = 0
    gomenu = 1
  endif else begin
    lpend = 1
  end

		;*******************************
		;**** Write data to fortran ****
		;*******************************
  printf,pipe,lpend
  printf,pipe,value.title
		;**** For no extrapolation in Fortran, iproj > il ****
  if value.iproj eq 0 then iproj=il+1 else iproj = value.iproj
  printf,pipe,iproj
  printf,pipe,value.nmet
  for i=0,ndmet-1 do printf,pipe,value.imetr(i)
		;**** Convert istrn from an IDL to a Fortran index ****
  printf,pipe,value.istrn+1
  printf,pipe,value.ifout
  printf,pipe,value.maxt
  for i=0,ndtem-1 do   printf,pipe,value.tine(i)
  for i=0,ndtem-1 do   printf,pipe,value.tinp(i)
  for i=0,ndtem-1 do   printf,pipe,value.tinh(i)
  printf,pipe,value.idout
  printf,pipe,value.maxd
  for i=0,ndden-1 do printf,pipe,value.dine(i)
  for i=0,ndden-1 do printf,pipe,value.dinp(i)
  for i=0,ndden-1 do printf,pipe,value.ratha(i)
  for i=0,ndden-1 do printf,pipe,value.ratia(i)
  if value.lzsel eq 1 then begin
    zeff = float(value.zeff)
  end else begin
    zeff = 0.0
  end
  printf,pipe,zeff
  printf,pipe,value.llsel
  printf,pipe,value.lpsel
  printf,pipe,value.lzsel
  printf,pipe,value.lisel
  printf,pipe,value.lhsel
  printf,pipe,value.lnorm
  printf,pipe,value.lrsel


END
