; Copyright (c) 2003 Strathclyde University .
;----------------------------------------------------------------------
;+
; PROJECT    :  ADAS
;
; NAME       :  bxttyp
;
; PURPOSE    :  
;
; ARGUMENTS  :  All output arguments will be defined appropriately.
;               Inputs will be converted to correct type, if possible,
;               internally without changing calling type.
;
;                bxttyp
;
;               NAME      I/O    TYPE    DETAILS
; REQUIRED   :
;
;
; NOTES      :  Calls fortran code.
;
;
; AUTHOR     :  Martin O'Mullane
;
; DATE       :  05/06/03
;
;
; MODIFIED:
;       1.1     Martin O'Mullane
;              	- First version.
;	1.2	Allan Whiteford
;		- Changed wrapper path to be just ADASFORT.
;
; VERSION:
;       1.1    05/06/03
;       1.1    10/08/04
;
;-
;----------------------------------------------------------------------

PRO bxttyp, ndlev  , ndmet  , ndtrn  , nplr  , npli  , $
            itran  , tcode  , i1a    , i2a   , aval  , $
            icnte  , icntp  , icntr  , icnth , icnti , $ 
            icntl  , icnts  ,                          $
            ietrn  , iptrn  , irtrn  , ihtrn , iitrn , $
            iltrn  , istrn  ,                          $
                              ie1a   , ie2a  , aa    , $
                              ip1a   , ip2a  ,         $
                              ia1a   , ia2a  , auga  , $
                              il1a   , il2a  , wvla  , $
                              is1a   , is2a  , lss04a

; Check number of parameters

if n_params() NE 38 then message, 'Not all parameters present', /continue


; Alter mandatory inputs to proper types

if ndlev EQ 0 then message, 'Set maximum number of levels'
if ndtrn EQ 0 then message, 'Set maximum number of transitions'
if ndmet EQ 0 then message, 'Set maximum number of metastables'

ndlev  = long(ndlev)
ndtrn  = long(ndtrn)
ndmet  = long(ndmet)


itran  = long(itran)
i1a    = long(i1a)
i2a    = long(i2a)

aval   = double(aval)


; Set types of outputs
 
nplr      = 0L
npli      = 0L
icnte     = 0L
icntp     = 0L  
icntr     = 0L 
icnth     = 0L
icnti     = 0L
icntl     = 0L  
icnts     = 0L  
ietrn     = lonarr(ndtrn) 
iptrn     = lonarr(ndtrn) 
irtrn     = lonarr(ndtrn) 
ihtrn     = lonarr(ndtrn) 
iitrn     = lonarr(ndtrn)
iltrn     = lonarr(ndtrn) 
istrn     = lonarr(ndtrn) 
ie1a      = lonarr(ndtrn) 
ie2a      = lonarr(ndtrn)
aa        = dblarr(ndtrn)
ip1a      = lonarr(ndtrn) 
ip2a      = lonarr(ndtrn)
ia1a      = lonarr(ndtrn) 
ia2a      = lonarr(ndtrn) 
auga      = dblarr(ndtrn)
il1a      = lonarr(ndlev) 
il2a      = lonarr(ndlev) 
wvla      = dblarr(ndlev)
is1a      = lonarr(ndlev) 
is2a      = lonarr(ndlev) 
il_lss04a = lonarr(ndlev, ndmet)
            
; Put the tcode string array into a byte array

n_tcode  = n_elements(tcode)
if n_tcode EQ ndtrn then str_vars = tcode                             $
                    else str_vars = [tcode, strarr(ndtrn-n_tcode)]

str_vars = byte(str_vars) 
str_vars = long(str_vars)


; Location of sources

fortdir = getenv('ADASFORT')
fortdir = fortdir


dummy = CALL_EXTERNAL(fortdir+'/bxttyp_if.so','bxttyp_if',            $
                      i1a, i2a, ia1a, ia2a, icnte, icnth,             $
                      icnti, icntl, icntp, icntr, icnts, ie1a, ie2a,  $
                      ietrn, ihtrn, iitrn, il1a, il2a, iltrn, ip1a,   $
                      ip2a, iptrn, irtrn, is1a, is2a, istrn, itran,   $
                      ndlev, ndmet, ndtrn, npli, nplr, il_lss04a, aa, $
                      auga, aval, wvla, str_vars)
                      

END
