;+
; PROJECT:
;       ADAS
;
; NAME:
;	ADAS2XX_NONMAX
;
; PURPOSE:
;	IDL ADAS user interface, Non-Maxwellian parameters selection.
;
; EXPLANATION:
;	This routine creates and manages a pop-up window which allows
;	the user to select a distribution type from Maxwellian, Kappa,
;       numerical or Druvesteyn. x/kappa parameters and an input
;       file can also be chosen as appropriate for the distribution type.
;
; USE:
;	See the routine ADAS2xx_IN for an example of how to
;	use this routine.
;
; INPUTS:
;	NONMAXID- A widget ID containing a structure as it's UVALUE
;                 which should be used to initalise the widget and
;                 also to update the results to.
;
;		  The structure should have the form:
;			{	FILE37:'', ROOT37:'', $
;                		CENT37:'', USER37:'', $
;                		DIST:0, DPARAM:0.0 }
;			
;		  Where:
;		         FILE37 - Selected adf37 file
;		         ROOT37 - Path to selected adf37 file
;		         CENT37 - Location of central adf37 files
;		         USER37 - Location of user adf37 files
;		         DIST   - Distribution type:
;				  0 - Maxwellian
;				  1 - Kappa
;				  2 - Numerical
;				  3 - Druvesteyn
;		         DPARAM - Distribution parameter:
;				  Maxwellian: N/A
;				  Kappa:      kappa
;				  Numerical:  N/A
;				  Druvesteyn: x
;
; OPTIONAL INPUTS:
;	None
;
; OUTPUTS:
;	None - Although the UVALUE of the NONMAXID widget may be changed.
;
; OPTIONAL OUTPUTS:
;	None
;
; KEYWORD PARAMETERS:
;
;	FONT	- Supplies the font to be used for the interface widgets.
;
; CALLS:
;	CW_ADAS_IN		Dataset selection widget creation.
;	XMANAGER		Manages the pop=up window.
;	ADAS2XX_NONMAX_EV	Event manager, called indirectly during
;				XMANAGER event management.
;
; SIDE EFFECTS:
;	XMANAGER is called in /modal mode. Any other widget become
;	inactive.
;
; CATEGORY:
;	Adas system.
;
; WRITTEN:
;       Allan Whiteford
;
; MODIFIED:
;       Version 1.1     Allan Whiteford  26-Jan-2005
;                       First release.
;
; VERSION:
;       1.1       26-Jan-2005
;
;-
;-----------------------------------------------------------------------------

pro adas2xx_nonmax_ev,event

	widget_control,event.top,get_uvalue=state

	if event.id eq state.typeid then begin
        	if event.index eq 0 then begin
                	widget_control,state.parambase,map=0
                	widget_control,state.filebase,map=0
                endif
        
        	if event.index eq 1 then begin
			widget_control,state.paramlabel,set_value='Kappa'
                	widget_control,state.parambase,map=1
                	widget_control,state.filebase,map=0
                endif
        
        	if event.index eq 2 then begin
                 	widget_control,state.parambase,map=0
                	widget_control,state.filebase,map=1
                endif
        
        	if event.index eq 3 then begin
 			widget_control,state.paramlabel,set_value='  x  '
                	widget_control,state.parambase,map=1
                	widget_control,state.filebase,map=0
                endif
	end

        if event.id eq state.cancelid then begin
		widget_control,event.top,/destroy

	end
	
        if event.id eq state.okid then begin
        	                
                dist=widget_info(state.typeid,/droplist_select)
                widget_control,state.paramid,get_value=dparam
                dparam=float(dparam)
                widget_control,state.fileid,get_value=filedata
                file37=filedata.file
                root37=filedata.rootpath
                cent37=filedata.centroot
                user37=filedata.userroot
                
		nonmaxdata={	FILE37:file37, ROOT37:root37, $
                		CENT37:cent37, USER37:user37, $
                		DIST:dist, DPARAM:dparam }

                widget_control,state.nonmaxid,set_uvalue=nonmaxdata
		widget_control,event.top,/destroy
	end

end

pro adas2xx_nonmax,nonmaxid,font=font

  	IF NOT (KEYWORD_SET(font)) THEN font = ''
  	IF NOT (KEYWORD_SET(centroot)) THEN centroot = ''
  	IF NOT (KEYWORD_SET(userroot)) THEN userroot = ''

	widget_control,nonmaxid,get_uvalue=nonmaxdata
	tlb=widget_base(title='Select Non-Maxwellian Parameters',/column)

	options=['Maxwellian','Kappa','Numerical','Druvesteyn']

	typeid=widget_droplist(tlb,value=options,font=font)
        widget_control,typeid,set_droplist_select=nonmaxdata.dist
        
        
        paramlabel='     '
        if nonmaxdata.dist eq 1 then paramlabel='Kappa'
        if nonmaxdata.dist eq 3 then paramlabel='  x  '
        
        parambase=widget_base(tlb,/row)
        paramlabel=widget_label(parambase,value=paramlabel,font=font)
        paramid=widget_text(parambase,value=strtrim(string(nonmaxdata.dparam),2),uvalue=strtrim(string(nonmaxdata.dparam),2),font=font,/editable,/all_events)
        
	if nonmaxdata.dist eq 0 or nonmaxdata.dist eq 2 then widget_control,parambase,map=0

	filebase=widget_base(tlb,/column)
        
        filedata={ROOTPATH:nonmaxdata.root37, FILE:nonmaxdata.file37, $
		  CENTROOT:nonmaxdata.cent37, USERROOT:nonmaxdata.user37 }
        title='Select ADF37 File'
        
	fileid = cw_adas_infile(filebase,value=filedata,title=title,font=font)

	if nonmaxdata.dist ne 2 then widget_control,filebase,map=0

        bbase=widget_base(tlb,/row)
        okid=widget_button(bbase,value='Ok',font=font)
        cancelid=widget_button(bbase,value='Cancel',font=font)
	
        state={okid:okid,cancelid:cancelid,nonmaxid:nonmaxid, $
               typeid:typeid,paramlabel:paramlabel,paramid:paramid, $
               fileid:fileid,parambase:parambase,filebase:filebase}
        
        widget_control,tlb,set_uvalue=state
        widget_control,tlb,/realize
        
        xmanager,'adas2xx_nonmax',tlb,event_handler='adas2xx_nonmax_ev',/modal
end
