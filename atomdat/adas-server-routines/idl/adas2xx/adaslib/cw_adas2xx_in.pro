;+
; PROJECT:
;       ADAS
;
; NAME:
;	CW_ADAS2XX_IN()
;
; PURPOSE:
;	Data file selection for a single input dataset.
;
; EXPLANATION:
;	This function creates a compound widget consisting of the compound
;	widget cw_adas_infile.pro, a 'Cancel' button, a 'Done' button and
;	a button to allow the browsing of the selected data file comments.
;	The browsing and done buttons are automatically de-sensitised
;	until a valid input dataset has been selected. In addition a button
;       is present to pop up a widget asking for non-Maxwellian parameters.
;
;	The value of this widget is the settings structure of the
;	cw_adas2xx_infile widget.  This widget only generates events
;	when either the 'Done' or 'Cancel' buttons are pressed.
;	The event structure returned is;
;	{ID:0L, TOP:0L, HANDLER:0L, ACTION:''}
;	ACTION has one of two values 'Done' or 'Cancel'.
;
; USE:
;	See routine adas2xx_in.pro for an example.
;
; INPUTS:
;	topparent	- Long integer; the ID of the topparent widget.
;
; OPTIONAL INPUTS:
;	None
;
; OUTPUTS:
;	This function returns the ID of the compound widget created.
;
; OPTIONAL OUTPUTS:
;	None
;
; KEYWORD PARAMETERS:
;	VALUE	- A structure which determines the initial settings of
;                 the dataset selection widget cw_adas_infile.  The
;		  structure must be;
;		  {ROOTPATH:'', FILE:'', CENTROOT:'', USERROOT:''
;		   DIST:0, DPARAM:0.0, ROOT37:'', FILE37:'',
;		   CENT37:'', USER37:'' }
;		  The elements of the structure are as follows;
;
;		  ROOTPATH - Current data directory e.g '/usr/fred/adas/'
;		  FILE     - Current data file in ROOTPATH e.g 'input.dat'
;		  CENTROOT - Default central data store e.g '/usr/adas/'
;		  USERROOT - Default user data store e.g '/usr/fred/adas/'
;		  DIST     - Distribution type:
;				0 - Maxwellian
;				1 - Kappa
;				2 - Numerical
;				3 - Durvesteyn
;		  DPARAM    - Distribution parameter:
;				DIST = 0 : N/A
;				DIST = 1 : kappa
;				DIST = 2 : N/A
;				DIST = 3 : x
;		  FILE37   - adf37 file for Numerical-Distributions
;		  ROOT37   - adf37 directory for Numerical-Distributions
;		  CENT37   - adf37 central directory Numerical-Distributions
;		  USER37   - adf37 user directory for Numerical-Distributions
;
;		  The data file selected by the user is obtained by
;		  appending ROOTPATH and FILE.  In the above example
;		  the full name of the data file is;
;		  /usr/fred/adas/input.dat
;
;		  Path names may be supplied with or without the trailing
;		  '/'.  The underlying routines add this character where
;		  required so that USERROOT will always end in '/' on
;		  output.
;
;		  The default value is;
;		  {ROOTPATH:'./', FILE:'', CENTROOT:'', USERROOT:''}
;		   DIST:0, DPARAM:0.0, FILE37:'', ROOT37:'',
;		   CENT37:'', USER37:'' }
;		  i.e ROOTPATH is set to the user's current directory.
;
;	TITLE	- The title to be included in the input file widget, used
;                 to indicate exactly what the required input dataset is,
;                 e.g 'Input COPASE Dataset'
;
;	UVALUE	- A user value for the widget.  Defaults to 0.
;
;	FONT	- Supplies the font to be used for the interface widgets.
;		  Defaults to the current system font.
;
; CALLS:
;	CW_LOADSTATE	Recover compound widget state.
;	CW_SAVESTATE	Save compound widget state.
;	XXTEXT		Pop-up window to browse dataset comments.
;	CW_ADAS_INFILE	Dataset selection widget.
;	FILE_ACC	Determine filetype and access permissions.
;
;	See side effects for other related widget management routines.
;
; SIDE EFFECTS:
;       A COMMON BLOCK: CW_IN_BLK is used which is private to
;       this widget.
;
;	IN2XX_GET_VAL()	Widget management routine in this file.
;	IN2XX_EVENT()	Widget management routine in this file.
;
; CATEGORY:
;	Compound Widget
;
; WRITTEN:
;       Allan Whiteford
;       Based on cw_adas_in.pro
;
; MODIFIED:
;       Version 1.1     Allan Whiteford  26-Jan-2005
;                       First release.
;
; VERSION:
;       1.1       26-Jan-2005
;
;-
;-----------------------------------------------------------------------------

FUNCTION in2xx_get_val, id


                ;**** Return to caller on error ****
  ON_ERROR, 2

                ;**** Retrieve the state ****
   
  first_child = widget_info(id,/child)
  widget_control, first_child, get_uvalue=state, /no_copy

		;**** Get settings ****

  widget_control,state.fileid,get_value=filedata
  widget_control,state.nonmaxid,get_uvalue=nonmaxdata

    inset = {   ROOTPATH:filedata.rootpath, FILE:filedata.file, $
		CENTROOT:filedata.centroot, USERROOT:filedata.userroot, $
		FILE37:nonmaxdata.file37, ROOT37:nonmaxdata.root37, $
		CENT37:nonmaxdata.cent37, USER37:nonmaxdata.user37, $
                DIST:nonmaxdata.dist, DPARAM:nonmaxdata.dparam }

  widget_control, first_child, set_uvalue=state, /no_copy

  RETURN, inset

END

;-----------------------------------------------------------------------------

FUNCTION in2xx_event, event


                ;**** Base ID of compound widget ****
  topparent=event.handler

                ;**** Retrieve the state ****

  first_child = widget_info(topparent,/child)
  widget_control, first_child, get_uvalue=state, /no_copy

		;*********************************
		;**** Clear previous messages ****
		;*********************************
  widget_control,state.messid,set_value=''

		;**** Default output no event ****
  new_event = 0L
                ;************************
                ;**** Process events ****
                ;************************
  CASE event.id OF

		;***********************************
		;**** Event from file selection ****
		;***********************************
    state.fileid: begin
	  if event.action eq 'newfile' then begin
	    widget_control,state.doneid,/sensitive
	    widget_control,state.browseid,/sensitive
	  end else begin
	    widget_control,state.doneid,sensitive=0
	    widget_control,state.browseid,sensitive=0
	  end
	end

		;***********************
		;**** Browse button ****
		;***********************
    state.browseid: begin
		;**** Get latest filename ****
	  widget_control,state.fileid,get_value=inset
	  filename = inset.rootpath+inset.file

		;**** Invoke comments browsing ****
	  xxtext, filename, font=state.font
	end

		;***********************
		;**** Cancel button ****
		;***********************
    state.cancelid: new_event = {ID:topparent, TOP:event.top, $
				HANDLER:0L, ACTION:'Cancel'}

		;*********************
		;**** Done button ****
		;*********************
    state.doneid: begin

	  new_event = {ID:topparent, TOP:event.top, HANDLER:0L, ACTION:'Done'}

        end

    state.nonmaxid: begin
		widget_control,state.fileid,get_value=filedata
                userroot=filedata.userroot
                centroot=filedata.centroot
        	adas2xx_nonmax,state.nonmaxid,font=state.font
        end

    ELSE:

  ENDCASE

  widget_control, first_child, set_uvalue=state, /no_copy

  RETURN, new_event
END

;-----------------------------------------------------------------------------

FUNCTION cw_adas2xx_in, topparent, VALUE=value, TITLE=title, $
                                UVALUE=uvalue, FONT=font


  IF (N_PARAMS() EQ 0) THEN MESSAGE, 'Must specify topparent for cw_adas_in'
  ON_ERROR, 2					;return to caller

		;**** Set defaults for keywords ****
  IF NOT (KEYWORD_SET(value)) THEN begin
    inset = {   ROOTPATH:'./', FILE:'', $
		CENTROOT:'', USERROOT:'', $
                FILE37:'', ROOT37:'', $
                CENT37:'', USER37:'', $
                DIST:0, DPARAM:0.0 }
  END ELSE BEGIN
    inset = {   ROOTPATH:value.rootpath, FILE:value.file, $
		CENTROOT:value.centroot, USERROOT:value.userroot, $
		FILE37:value.file37, ROOT37:value.root37, $
		USER37:value.user37, CENT37:value.cent37, $
                DIST:value.dist, DPARAM:value.dparam }
                
    if strtrim(inset.rootpath) eq '' then begin
      inset.rootpath = './'
    end else if $
	    strmid(inset.rootpath,strlen(inset.rootpath)-1,1) ne '/' then begin
      inset.rootpath = inset.rootpath+'/'
    end
    if strtrim(inset.root37) eq '' then begin
      inset.root37 = './'
    end else if $
	    strmid(inset.root37,strlen(inset.root37)-1,1) ne '/' then begin
      inset.root37 = inset.root37+'/'
    end
    if strmid(inset.file,0,1) eq '/' then begin
      inset.file = strmid(inset.file,1,strlen(inset.file)-1)
    end
  END
  IF NOT (KEYWORD_SET(title)) THEN title = ''
  IF NOT (KEYWORD_SET(uvalue)) THEN uvalue = 0
  IF NOT (KEYWORD_SET(font)) THEN font = ''

		;*********************************
		;**** Create the Input widget ****
		;*********************************

		;**** create base widget ****
  parent = widget_base( topparent, UVALUE = uvalue, $
			EVENT_FUNC = "in2xx_event", $
			FUNC_GET_VALUE = "in2xx_get_val", $
			/COLUMN)

		;**** Create base to hold the value of state ****

    first_child = widget_base(parent)

    cwid = widget_base(first_child,/column)

		;*************************************
		;**** Input file selection widget ****
		;*************************************
  fileid = cw_adas_infile(cwid,value=inset,title=title,font=font)

		;*****************
		;**** Buttons ****
		;*****************
  base = widget_base(cwid,/row)

		;**** Browse Dataset button ****
  browseid = widget_button(base,value='Browse Comments',font=font)

		;**** Cancel Button ****
  cancelid = widget_button(base,value='Cancel',font=font)

		;**** Done Button ****
  doneid = widget_button(base,value='Done',font=font)

		;**** Non-Maxwellian Button ****
  nonmaxid = widget_button(base,value='Non-Maxwellian...',font=font)
  nonmaxdata = { FILE37:inset.file37, ROOT37:inset.root37, $
  		 CENT37:inset.cent37, USER37:inset.user37, $
                 DIST:inset.dist, DPARAM:inset.dparam }
  widget_control,nonmaxid,set_uvalue=nonmaxdata
  
		;**** Error message ****
  messid = widget_label(topparent,font=font,value='*')

		;**** Check filename and desnsitise buttons if it ****
		;**** is a directory or it is a file without read ****
		;**** access.					  ****
  filename = inset.rootpath+inset.file
  file_acc,filename,fileexist,read,write,execute,filetype
  if filetype ne '-' then begin
    widget_control,browseid,sensitive=0
    widget_control,doneid,sensitive=0
  end else begin
    if read eq 0 then begin
      widget_control,browseid,sensitive=0
      widget_control,doneid,sensitive=0
    end
  end

		;**** create a state structure for the widget ****
  new_state = { FILEID:fileid, BROWSEID:browseid, $
		CANCELID:cancelid, DONEID:doneid, $
                NONMAXID:nonmaxid, $
                MESSID:messid, FONT:font }

;                ;**** Save initial state structure ****

  widget_control, first_child, set_uvalue=new_state, /no_copy

  RETURN, parent

END

