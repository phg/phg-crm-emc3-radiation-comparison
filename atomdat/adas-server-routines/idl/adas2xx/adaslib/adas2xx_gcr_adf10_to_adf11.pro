;----------------------------------------------------------------------
;+
; PROJECT    :  ADAS
;
; NAME       :  adas2xx_gcr_adf10_to_adf11
;
; PURPOSE    :  Operates on adas208 produced partial adf10 files to make
;               adf11 files. Part of the integrated GCR workflow.
;
;
;               NAME        I/O    TYPE   DETAILS
; REQUIRED   :  files        I     str()  set of adf10 files
;               adf11        I     str    name of adf11 output file
;
; OPTIONAL      met_files    I     str()  set of adf10 met files if a
;                                         metastable un-resolved adf11
;                                         file is required
;               comments     I     str()  write these comments at end of adf11 file.
;               project      I     str    Name of project for top line upper right.
;
; KEYWORDS      unresolved     -          if set the output adf11 will be
;                                         metastable unresolved. met_files
;                                         are required for this option.
;
;
; NOTES      :  The partial adf10 files must all be of the same class.
;
;               The ionisation stages included in the adf11 file are
;               determined from the set of adf10 files supplied.
;
;               There is no 'met' class adf11 since these are only
;               required when generating unresolved adf11 files.
;
;
; AUTHOR     :  Martin O'Mullane
; DATE       :  27-07-2017
;
; MODIFIED:
;       1.1     Martin O'Mullane
;                 - First version.
;
; VERSION:
;       1.1    27-07-2017
;-
;----------------------------------------------------------------------

PRO adas2xx_gcr_adf10_to_adf11, files      = files,     $
                                met_files  = met_files, $
                                adf11      = adf11,     $
                                unresolved = unresolved

; Check inputs

num_met = n_elements(met_files)
num_10  = n_elements(files)

if num_met EQ 0 AND keyword_set(unresolved) then $
   message, '/unresolved requires a set of metastable files'

if keyword_set(unresolved) EQ 1 then unres = 1 else unres = 0

if n_elements(project) EQ 0 then project='Test project'


; Re-order set of files into ascending z1 first.
;
; Exclude empty datasets (usually qcd and xcd)

iz_list = -1
keep    = -1
for j = 0, num_10-1 do begin
   xxdata_10_p208, file=files[j], fulldata=all
   if all.class NE 'EMPTY' then begin
      iz_list = [iz_list, all.iz1]
      keep = [keep, j]
   endif
endfor
if n_elements(keep) EQ 1 then message, 'No usable files -- all are empty'

keep    = keep[1:*]
iz_list = iz_list[1:*]
files   = files[keep]
files   = files[sort(iz_list)]
num_10  = n_elements(files)



; Determine ranges, connection vector, indexing, limits and Te/dens for adf11 file

is1min  = 1000
is1max  = -1
class   = 'XXX'
ismax   = 0
icnctv  = -1
isstgr  = -1
index_1 = -1
index_2 = -1
ilast   = 0

for j = 0, num_10-1 do begin

   xxdata_10_p208, file=files[j], fulldata=all

   if j EQ 0 then begin
      class = all.class
   endif else begin
      if class NE all.class then message, 'Inconsistent class between adf10 files'
   endelse

   is1min = min([is1min, all.iz1])
   is1max = max([is1max, all.iz1])

   nblock  = n_elements(all.index_1)
   ismax   = ismax + nblock
   isstgr  = [isstgr, intarr(nblock) + all.iz1]   ; s1 for stage
   index_1 = [index_1, all.index_1]
   index_2 = [index_2, all.index_2]

   ; connection vector is the number of metastables in this stage
   ; plus the number of metastables in the last considered stage

   case class of
      'plt' : icnctv = [icnctv, max(all.index_1)]
      'prb' : icnctv = [icnctv, all.nmet]
      else  : icnctv = [icnctv, max(all.index_2)]
   endcase

   ilast  = max(all.index_1)

endfor

icnctv  = [icnctv[1:*], ilast]
isstgr  = isstgr[1:*]
index_1 = index_1[1:*]
index_2 = index_2[1:*]
iz0     = all.iz0
idval   = n_elements(all.dens)
itval   = n_elements(all.te)
dens    = all.dens
te      = all.te



; Default comments

units_str = 'cm**3 s**-1'
if class EQ 'plt' OR class EQ 'prb' OR class EQ 'prc' then units_str = 'W cm**-3'

today     = xxdate()
today     = today[0];  + ', ' + today[1]
user_name = xxuser()
dashes    = 'C-------------------------------------------------------------------------------'

a11_comments = ['C', $
                'C  Produced using the gcr_adf10_to_adf11 routine', $
                'C', $
                'C  Units  : ' + units_str, $
                'C', $
                'C  Author : ' + user_name, $
                'C  Date   : ' + today, $
                'C', $
                dashes]

if n_elements(comments) GT 0 then a11_comments = [comments, a11_comments]



; Make fulldata structure for adf11 resolved by metastable or not

if unres EQ 1 then begin

   lres = 0

   ; Read meta files to find max number of metastables/ion and perform some checks

   im1min   = 1000
   im1max   = -1
   class    = 'XXX'
   izm_list = -1
   nmet_max = 0

   for j = 0, num_met-1 do begin

      xxdata_10_p208, file=met_files[j], fulldata=all

      if j EQ 0 then begin
         class = all.class
      endif else begin
         if class NE all.class then message, 'Inconsistent class between adf10 met files'
      endelse

      im1min = min([im1min, all.iz1])
      im1max = max([im1max, all.iz1])

      nmet_max = max([nmet_max, all.nmet])
      izm_list = [izm_list, all.iz1]

   endfor

   izm_list  = izm_list[1:*]
   met_files = met_files[sort(izm_list)]
   nz        = is1max - is1min + 1


   ; read in metastable data and normalize to total

   meta = dblarr(nz, nmet_max, itval, idval)

   i1 = 0
   for j = 0, num_met-1 do begin

      xxdata_10_p208, file=met_files[j], fulldata=all
      num = all.nmet

      metfrc = all.data
      sum    = total(metfrc, 1)
      for im = 0, num-1 do metfrc[im,*,*] = metfrc[im,*,*] / sum

      meta[j, 0:all.nmet-1, *, *] = metfrc

   endfor

   meta = meta > 1.0D-40


   ; construct the unresolved coefficients

   info     = dashes
   coeff_ls = dblarr(nz, itval, idval)


   for j = 0, num_10-1 do begin

      xxdata_10_p208, file=files[j], fulldata=all

      ; acd/ccd

      if all.class EQ 'acd' OR all.class EQ 'ccd' OR $
         all.class EQ 'prb' OR all.class EQ 'prc' then begin

         acd_ls = dblarr(itval, idval)

         for ip = 0, n_elements(all.index_1)-1 do begin

            iprt = all.index_1[ip]

            izr = all.iz1+1
            if all.iz1 EQ all.iz0 then begin
               frac = 1.0
            endif else begin
               frac = reform(meta[izr-1, iprt-1, *, *])
            endelse

            acd_ls = acd_ls + reform(all.data[ip,*,*]) * frac

         endfor

         coeff_ls[all.iz1-1, *, *] = acd_ls > 1.0D-40

      endif


      ; scd

      if all.class EQ 'scd' then begin

         scd_ls = dblarr(itval, idval)

         for ig = 0, n_elements(all.index_1)-1 do begin

            igrd = all.index_2[ig]

            frac = reform(meta[all.iz1-1, igrd-1, *, *])

            scd_ls = scd_ls + reform(all.data[ig,*,*]) * frac

         endfor

         coeff_ls[all.iz1-1, *, *] = scd_ls > 1.0D-40

      endif


      ; plt

      if all.class EQ 'plt' then begin

         plt_ls = dblarr(itval, idval)

         for im = 0, n_elements(all.index_1)-1 do begin

            imet = all.index_1[im]

            frac = reform(meta[all.iz1-1, imet-1, *, *])

            plt_ls = plt_ls + reform(all.data[im,*,*]) * frac

         endfor

         coeff_ls[all.iz1-1, *, *] = plt_ls > 1.0D-40

      endif

      info = [info, all.comments, dashes]

   endfor

   ind = where(finite(coeff_ls) EQ 0, count)
   if count GT 0 then coeff_ls[ind] = 1.0D-40

   fulldata = {iz0    : all.iz0,          $
               class  : all.class,        $
               idmax  : idval,            $
               itmax  : itval,            $
               is1min : im1min,           $
               is1max : im1max,           $
               ismax  : nz,               $
               isstgr : izm_list,         $
               ddens  : alog10(dens),     $
               dtev   : alog10(te),       $
               drcof  : alog10(coeff_ls), $
               lres   : 0                 }


endif else begin


   lres = 1
   if num_met GT 0 then message, 'No need to supply met file for resolved data', /continue

   coeff = dblarr(ismax, itval, idval)

   info  = dashes

   i1 = 0
   for j = 0, num_10-1 do begin

      xxdata_10_p208, file=files[j], fulldata=all
      num = n_elements(all.index_1)
      i2  = i1+num-1

      coeff[i1:i2, *, *] = all.data
      i1 = i1 + num

      info = [info, all.comments, dashes]

   endfor

   coeff = coeff > 1.0D-40

   ind = where(finite(coeff) EQ 0, count)
   if count GT 0 then coeff[ind] = 1.0D-40

   s_1 = all.name_ind1
   s_2 = all.name_ind2

   if class EQ 'plt' OR class EQ 'prb' then begin

      fulldata = create_struct('iz0'    , iz0,           $
                               'class'  , class,         $
                               'idmax'  , idval,         $
                               'itmax'  , itval,         $
                               'is1min' , is1min,        $
                               'is1max' , is1max,        $
                               'ismax'  , ismax,         $
                               'isstgr' , isstgr,        $
                               'icnctv' , icnctv,        $
                               'ddens'  , alog10(dens),  $
                               'dtev'   , alog10(te),    $
                               'drcof'  , alog10(coeff), $
                               s_1      , index_1,       $
                               'lres'   , lres           )

   endif else begin

      fulldata = create_struct('iz0'    , iz0,           $
                               'class'  , class,         $
                               'idmax'  , idval,         $
                               'itmax'  , itval,         $
                               'is1min' , is1min,        $
                               'is1max' , is1max,        $
                               'ismax'  , ismax,         $
                               'isstgr' , isstgr,        $
                               'icnctv' , icnctv,        $
                               'ddens'  , alog10(dens),  $
                               'dtev'   , alog10(te),    $
                               'drcof'  , alog10(coeff), $
                               s_1      , index_1,       $
                               s_2      , index_2,       $
                               'lres'   , lres           )

   endelse

endelse


; Write adf11 file

write_adf11, outfile=adf11, fulldata=fulldata, project=project, comments=[info, dashes, a11_comments]

END
