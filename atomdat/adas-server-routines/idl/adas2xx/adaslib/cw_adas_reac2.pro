; Copyright (c) 1995 Strathclyde University .
; SCCS info: Module @(#)$Header: /home/adascvs/idl/adas2xx/adaslib/cw_adas_reac2.pro,v 1.6 2004/07/06 13:01:35 whitefor Exp $ Date $Date: 2004/07/06 13:01:35 $
;
;+
; PROJECT:
;       ADAS IBM MVS to DEC UNIX conversion
;
; NAME:
;	CW_ADAS_REAC2()
;
; PURPOSE:
;	Widget consisting of reaction options and input for ADAS205
;	and ADAS206.
;
; EXPLANATION:
;	This compound widget is for use in ADAS 205 and ADAS 206.  It
;	presents buttons for selecting reactions to be included in
;	subsequent calculations.  The upper portion of the widget
;	consists of three 'linked' widgets, two buttons and a text
;	entry box.  The lower portion of the widget consists of three
;	non-exclusive buttons.  The widgets are labeled;
;
;	'Proton Impact Collisions'		button 1
;	'Scale Proton Impact for Zeff'		button 2
;	'Enter Z-Effective for Collisions'	text input
;
;	'Ionisation Rates'			button3
;	'Neutral H Charge Exchange'		button4
;	'Free Electron Recombination'		button5
;
;	Button 2 is de-sensitized and unset unless button 1 is set.
;	The text input is de-sensitized and blank unless button 2 is set.
;
;	The widget-value for this compound widget is a structure which
;	represents the settings of all of the child widgets;
;
;	{reacset, ZEFF:'2.5', LPSEL:0,    LZSEL:0, $
;		 LISEL:0,     LHSEL:0,    LRSEL:0}
;
;	Zeff is the string from the text input, is should represent a
;	floating point number.
;
;	The settings of buttons 1-5 are represented by lpsel, lzsel, lisel,
;	lhsel and lrsel respectively.  A value of 0 means the button is
;	unset and a value of 1 indicates that the button is set.
;
;	The buttons in this widget are actively monitored and maintained
;	by the event procedures for this widget.
;
; USE:
;	This simple example creates a base and then creates this
;	reaction selection widget as the only child.  Note that in order
;	for the widget buttons to react properly and for the widget-value
;	to be correctly maintained this widget must be managed using
;	XMANAGER or widget_event()
;
;	base = widget_base()
;	rid = cw_adas_reac2(base,value=reacset,font='times_roman14')
;	widget_control,base,/realize
;
; INPUTS:
;       PARENT   - The ID of the parent widget.
;
; OPTIONAL INPUTS:
;	None.
;
; OUTPUTS:
;       The return value of this function is the ID of the compound
;       widget which is of type LONG integer.
;
; OPTIONAL OUTPUTS:
;       None
;
; KEYWORD PARAMETERS:
;	VALUE	 - The initial value of the widget representing the
;		   start-up settings of the buttons and the text input.
;		   The value is a structure defined in the Explanation
;		   section above.  The default is for all buttons to be
;		   off.
;
;       FONT     - A font to use for all text in this widget.
;
;	UVALUE	 - A user value for this widget.
;
; CALLS:
;       CW_LOADSTATE    Recover compound widget state.
;       CW_SAVESTATE    Save compound widget state.
;       See side effects for other related widget management routines.
;
; SIDE EFFECTS:
;	This widget uses a COMMON BLOCK: CW_ADASRC2_BLK to hold the
;       widget state.
;
;       Three other routines are included which are used to manage the
;       widget;
;
;	ADASRC2_SET_VAL
;	ADASRC2_GET_VAL()
;	ADASRC2_EVENT()
;
; CATEGORY:
;	Compound Widget
;
; WRITTEN:
;       Andrew Bowen, Tessella Support Services plc, 7-Apr-1993
;
; MODIFIED:
;       Version 1       Andrew Bowen    1-Jun-1993
;                       First release.
;	   1.6	Richard Martin
;			Removed obsolete cw_loadstate/savestate statements.
;
; VERSION:
;       1       1-Jun-1993
;	1.6	06-03-02
;-
;-----------------------------------------------------------------------------

PRO adasrc2_set_val, id, value


		;**** Return to caller on error ****
  ON_ERROR, 2

		;**** Retrieve the state ****
		
  first_child = widget_info(id,/child)
  widget_control, first_child, get_uvalue=state, /no_copy
  
		;*******************************
		;**** Set reaction settings ****
		;*******************************

		;**** Proton impact reactions ****
  if value.lpsel eq 1 then begin
    widget_control,state.picid,set_button=1
    widget_control,state.spiid,/sensitive

    if value.lzsel eq 1 then begin
      widget_control,state.spiid,set_button=1
      widget_control,state.zfbsid,/sensitive
      widget_control,state.pzeid,set_value=value.zeff
    end else begin
      widget_control,state.spiid,set_button=0
      widget_control,state.zfbsid,sensitive=0
      widget_control,state.pzeid,set_value=''
    end

  end else begin
    widget_control,state.picid,set_button=0
    widget_control,state.spiid,sensitive=0
    widget_control,state.zfbsid,sensitive=0
  end

		;**** Set other reaction buttons ****
  widget_control,state.irtid,set_button=value.lisel
  widget_control,state.hceid,set_button=value.lhsel
  widget_control,state.ferid,set_button=value.lrsel

		;**** Copy the new value to state structure ****
  state.reacset.zeff = value.zeff
  state.reacset.lpsel = value.lpsel
  state.reacset.lzsel = value.lzsel
  state.reacset.lisel = value.lisel
  state.reacset.lhsel = value.lhsel
  state.reacset.lrsel = value.lrsel

		;**** Save the new state ****

  widget_control, first_child, set_uvalue=state, /no_copy

END

;-----------------------------------------------------------------------------


FUNCTION adasrc2_get_val, id


		;**** Return to caller on error ****
  ON_ERROR, 2

		;**** Retrieve the state ****

  first_child = widget_info(id,/child)
  widget_control, first_child, get_uvalue=state, /no_copy

		;**** Get the latest Zeff value ****
  if state.reacset.lzsel eq 1 then begin
    widget_control,state.pzeid,get_value=value
    state.reacset.zeff=value(0)
  end

  reacset=state.reacset
  widget_control, first_child, set_uvalue=state, /no_copy  

  RETURN, reacset

END

;-----------------------------------------------------------------------------

FUNCTION adasrc2_event, event


		;**** Base ID of compound widget ****
  base=event.handler

		;**** Retrieve the state ****

  first_child = widget_info(base,/child)
  widget_control, first_child, get_uvalue=state, /no_copy

		;************************
		;**** Process Events ****
		;************************
  CASE event.id OF

    state.picid: begin
		if state.reacset.lpsel eq 1 then begin
		  state.reacset.lpsel = 0
		  widget_control,state.spiid,set_button=0
		  widget_control,state.spiid,sensitive=0
		  state.reacset.lzsel = 0
		  widget_control,state.pzeid,set_value=''
		  widget_control,state.zfbsid,sensitive=0
		  state.reacset.zeff = ''
		end else begin
		  state.reacset.lpsel = 1
		  widget_control,state.spiid,/sensitive
		end
    end

    state.spiid: begin
		if state.reacset.lzsel eq 1 then begin
		  state.reacset.lzsel = 0
		  widget_control,state.pzeid,set_value=''
		  widget_control,state.zfbsid,sensitive=0
		  state.reacset.zeff = ''
		end else begin
		  state.reacset.lzsel = 1
		  widget_control,state.zfbsid,/sensitive
		end
    end

    state.irtid: if state.reacset.lisel eq 1 then state.reacset.lisel=0 $
					     else state.reacset.lisel=1

    state.hceid: if state.reacset.lhsel eq 1 then state.reacset.lhsel=0 $
					     else state.reacset.lhsel=1

    state.ferid: if state.reacset.lrsel eq 1 then state.reacset.lrsel=0 $
					     else state.reacset.lrsel=1

    ELSE:

  ENDCASE

		;**** Save the new state structure ****

  widget_control, first_child, set_uvalue=state, /no_copy

      action = 'reaction'
  RETURN, {ID:base, TOP:event.top, HANDLER:0L, ACTION:action}
END

;-----------------------------------------------------------------------------

FUNCTION cw_adas_reac2, parent, VALUE=value, $
			FONT=font, UVALUE=uvalue


  IF (N_PARAMS() LT 1) THEN MESSAGE, 'Must specify PARENT for cw_adas_reac2'

		;**** Set defaults for keywords ****
  IF NOT (KEYWORD_SET(value)) THEN begin
		     reacset = {reacset, ZEFF:'', $
				LPSEL:0, LZSEL:0, $
				LISEL:0, LHSEL:0, $
				LRSEL:0}
		end else begin
		     reacset = {reacset, ZEFF:value.zeff, $
				LPSEL:value.lpsel, LZSEL:value.lzsel, $
				LISEL:value.lisel, LHSEL:value.lhsel, $
				LRSEL:value.lrsel}
		end
  IF NOT (KEYWORD_SET(font)) THEN font = ''
  IF NOT (KEYWORD_SET(uvalue))  THEN uvalue = 0


		;**** Create the main base for the widget ****
  cwid = WIDGET_BASE(parent, UVALUE = uvalue, $
		EVENT_FUNC = "adasrc2_event", $
		FUNC_GET_VALUE = "adasrc2_get_val", $
		PRO_SET_VALUE = "adasrc2_set_val", $
		/COLUMN)

		;**** Create base to hold the value of state ****

  first_child = widget_base(cwid)

		;**** Title ****
  rc = widget_label(first_child,value='Reaction Selection',font=font)

		;**** Proton impact ****
  protbase = widget_base(cwid,/column,/frame)
  protbut = widget_base(protbase,/column,/nonexclusive)
  picid = widget_button(protbut,value='Proton Impact Collisions',font=font)
  spiid = widget_button(protbut,value='Scale Proton Impact for Zeff',font=font)
  zfbsid = widget_base(protbase,/row)
  rc = widget_label(zfbsid,value='Enter Z-Effective for Collisions',font=font)
  pzeid = widget_text(zfbsid,/edit,xsize=5,font=font)

		;**** Other reactions ****
  othreac = widget_base(cwid,/column,/nonexclusive)
  irtid = widget_button(othreac,value='Ionisation Rates',font=font)
  hceid = widget_button(othreac,value='Neutral H Charge Exchange',font=font)
  ferid = widget_button(othreac,value='Free Electron Recombination',font=font)

		;*******************************
		;**** Set reaction settings ****
		;*******************************

		;**** Default all buttons off ****
  widget_control,protbase,set_button=0

		;**** Proton impact reactions ****
  if reacset.lpsel eq 1 then begin
    widget_control,picid,set_button=1

    if reacset.lzsel eq 1 then begin
      widget_control,spiid,set_button=1
      widget_control,pzeid,set_value=reacset.zeff
    end else begin
      widget_control,zfbsid,sensitive=0
    end

  end else begin
    widget_control,spiid,sensitive=0
    widget_control,zfbsid,sensitive=0
  end

		;**** Set other reaction buttons ****
  if reacset.lisel eq 1 then widget_control,irtid,set_button=1
  if reacset.lhsel eq 1 then widget_control,hceid,set_button=1
  if reacset.lrsel eq 1 then widget_control,ferid,set_button=1

		;**** Create state structure ****
  new_state =  {PICID:picid, SPIID:spiid, PZEID:pzeid, ZFBSID:zfbsid, $
		IRTID:irtid, HCEID:hceid, FERID:ferid, $
		REACSET:reacset}

		;**** Save initial state structure ****

  widget_control, first_child, set_uvalue=new_state, /no_copy

  RETURN, cwid

END
