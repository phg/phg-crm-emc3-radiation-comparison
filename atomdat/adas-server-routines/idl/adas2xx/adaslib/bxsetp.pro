; Copyright (c) 1995 Strathclyde University .
; SCCS info: Module @(#)$Header: /home/adascvs/idl/adas2xx/adaslib/bxsetp.pro,v 1.5 2004/07/06 11:46:32 whitefor Exp $ Date $Date: 2004/07/06 11:46:32 $
;+
; PROJECT:
;       ADAS IBM MVS to DEC UNIX conversion
;
; NAME:
;	BXSETP
;
; PURPOSE:
;	IDL communications with ADAS205 FORTRAN process via pipe.
;
; EXPLANATION:
;	This IDL routine reads data from the ADAS205/206 FORTRAN subroutine
;	BXSETP via a UNIX pipe.  In the original IBM version the FORTRAN
;	subroutine BXSETP put information needed by the ISPF interface
;	into the ISPF pool area for later use.  Now the same information
;	is written to IDL for use in its user unterface.
;
; USE:
;	The use of this routine is specific to ADAS205/206.  See
;	adas205.pro for an example.
;
; INPUTS:
;	PIPE	- The IDL unit number of the bi-directional pipe to the
;		  ADAS205/206 FORTRAN process.
;
; OPTIONAL INPUTS:
;	None
;
; OUTPUTS:
;	SZ0	- String, nuclear charge read.
;
;	SZ	- String, recombined ion charge read.
;
;	SCNTE	- Number of electron impact transitions.
;
;	SIL	- Number of energy levels.
;
;	LFPOOL	- Integer, number of level strings. i.e dimension of strga
;		  (In IBM version number of strings sent to function pool)
;
;	STRGA	- String array, level designations.
;
; OPTIONAL OUTPUTS:
;	None
;
; KEYWORD PARAMETERS:
;	None
;
; CALLS:
;	None
;
; SIDE EFFECTS:
;	Reads data from a pipe.
;
; CATEGORY:
;	Adas system.
;
; WRITTEN:
;       Andrew Bowen, Tessella Support Services plc, 6-Apr-1993
;
; MODIFIED:
;       Version 1.1       Andrew Bowen    27-May-1993
;	Version 1.4	  Tim Hammond	  30-Jun-1995
;	Version 1.5	  Tim Hammond	  30-Jun-1995
;
; VERSION:
;       1.1     First release
;	1.4	Made pipe reading clearer (direct reading into
;		variables can cause problems on certain machines)
;	1.5	Tidied up SC CS comments
;-
;-----------------------------------------------------------------------------



PRO bxsetp, pipe, sz0, sz, scnte, sil, lfpool, strga

		;**********************************
		;**** Initialise new variables ****
		;**********************************
  sz0 = ''
  sz = ''
  scnte = ''
  sil = ''
  lfpool = 0
  input = ''
  idum = 0

		;********************************
		;**** Read data from fortran ****
		;********************************
  readf, pipe, input
  sz0 = input
  readf, pipe, input
  sz = input
  readf, pipe, input
  scnte = input
  readf, pipe, input
  sil = input

  readf, pipe, idum
  lfpool = idum
  strga = strarr(lfpool)

  for i = 0, lfpool-1 do begin
    readf, pipe, input
    strga(i) = input
  end

END
