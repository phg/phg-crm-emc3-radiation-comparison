; Copyright (c) 1995 Strathclyde University .
; SCCS info: Module @(#)$Header: /home/adascvs/idl/adas2xx/adaslib/bxoutg.pro,v 1.11 2004/07/06 11:45:47 whitefor Exp $ Date $Date: 2004/07/06 11:45:47 $
;+
; PROJECT:
;       ADAS IBM MVS to DEC UNIX conversion
;
; NAME:
;	BXOUTG
;
; PURPOSE:
;	Communication with ADAS205/206/208 FORTRAN via UNIX pipe and
;	graphics output.
;
; EXPLANATION:
;	The routine begins by reading information from the ADAS205
;	FORTRAN process via a UNIX pipe.  Then the IDL graphical
;	output routine for ADAS205 is invoked.  Communications are to
;	the FORTRAN subroutine BXOUTG.
;
; USE:
;	The use of this routine is specific to ADAS205/206/208 see adas205.pro.
;
; INPUTS:
;	PIPE	 - The IDL unit number of the bi-directional pipe to the
;		   ADAS205 FORTRAN process.
;
;	GRPSCAL	 - Integer; 0 if default scaling required 1 if user
;		   specified scaling to be used.
;
;	XMIN	 - String; User sepcified x-axis minimum, number as string.
;
;	XMAX	 - String; User sepcified x-axis maximum, number as string.
;
;	YMIN	 - String; User sepcified y-axis minimum, number as string.
;
;	YMAX	 - String; User sepcified y-axis maximum, number as string.
;
;	HRDOUT	 - Integer; 0 if no hardcopy required, 1 if hardcopy.
;
;	HARDNAME - String; file name for hardcopy output.
;
;	DEVICE	 - String; IDL name for hardcopy output device.
;
;	HEADER	 - ADAS version header information for inclusion in the
;		   graphical output.
;
;       BITFILE - String; the path to the dirctory containing bitmaps
;                 for the 'escape to series menu' button.
;
; OPTIONAL INPUTS:
;	None.
;
; OUTPUTS:
;       GOMENU  - Int; flag - set to 1 if user has selected 'escape direct
;                 to series menu , otherwise 0
;
; OPTIONAL OUTPUTS:
;	None.
;
; KEYWORD PARAMETERS:
;	FONT	- String; Name of the font to be used for the graphical
;		  output widget.
;
; CALLS:
;	ADAS205_PLOT	ADAS205 graphical output.
;
; SIDE EFFECTS:
;	This routine reads information from FORTRAN via a UNIX pipe.
;
; CATEGORY:
;	Adas system.
;
; WRITTEN:
;       Andrew Bowen, Tessella Support Services plc, 4-May-1993
;
; MODIFIED:
;       1       	Andrew Bowen
;                       First release.
;	1.8		William Osborn
;			Added win_numb keyword for use with different routines
;	1.9		William Osborn
;			Added menu button code
;	1.10		William Osborn
;			Made pipe comm. loops explicit
;       1.11            William Osborn
;                       Made reads into specified variables
;
; VERSION:
;       1       8-Jun-1993
;	1.8	09-05-96
;	1.9	04-06-96
;	1.10	17-06-96
;	1.11	10-10-96
;-
;-----------------------------------------------------------------------------



PRO bxoutg, pipe, grpscal, xmin, xmax, ymin, ymax, hrdout, hardname,	$
	    device, header, bitfile, gomenu, FONT=font, WIN_NUMB=win_numb


                ;**** Set defaults for keywords ****
  IF NOT (KEYWORD_SET(font)) THEN font = ''
  IF NOT (KEYWORD_SET(win_numb)) THEN win_numb = '205'

		;**** Declare variables for input ****
  ndlev = 0
  ndtem = 0
  ndden = 0
  ndmet = 0
  titled = ''
  title = ''
  gtit1 = ''
  dsninc = ''
  iz = 0
  itsel = 0
  tev = 0.0
  il = 0
  nmet = 0
  nord = 0
  maxd = 0

		;********************************
		;**** Read data from fortran ****
		;********************************
  idum=0
  fdum=0.0
  ddum=0.0d0
  sdum=''
  readf,pipe,idum
  ndlev = idum
  readf,pipe,idum
  ndtem = idum
  readf,pipe,idum
  ndden = idum
  readf,pipe,idum
  ndmet = idum
  readf,pipe,sdum
  titled = sdum
  readf,pipe,sdum
  title = sdum
  readf,pipe,sdum
  gtit1 = sdum
  readf,pipe,sdum
  dsninc = sdum
  readf,pipe,idum
  iz = idum
  readf,pipe,idum
  itsel = idum
		;**** Convert FORTRAN itsel array index to IDL array index ****
  itsel = itsel - 1
  readf,pipe,fdum
  tev = fdum
  readf,pipe,idum
  il = idum
  readf,pipe,idum
  nmet = idum
  readf,pipe,idum
  nord = idum
  readf,pipe,idum
  maxd = idum

		;**** Declare arrays for input ****
  lmetr = intarr(nmet)
  imetr = intarr(nmet)
  iordr = intarr(nord)
  densa = dblarr(maxd)
  strga = strarr(il)
  stack = dblarr(il,nmet,ndtem,maxd)

		;********************************
		;**** Read data from fortran ****
		;********************************

  for i=0,nmet-1 do begin  
    readf,pipe,idum
    lmetr(i)=idum
  endfor
  for i=0,nmet-1 do begin
    readf,pipe,idum
    imetr(i)=idum
  endfor
  for i=0,nord-1 do begin
    readf,pipe,idum
    iordr(i)=idum
  endfor
  for i=0,maxd-1 do begin
    readf,pipe,ddum
    if (ddum lt 1.0d-36) then ddum = 0.0d0
    densa(i)=ddum
  endfor
  input = ''
  for i = 0, il-1 do begin
    readf,pipe,input
    strga(i) = input
  end

  for l = 0, maxd-1 do begin
    for k = 0, ndtem-1 do begin
      for j=0,nmet-1 do begin
        for i=0,il-1 do begin
	  readf,pipe,ddum
          if (ddum lt 1.0d-36) then ddum = 0.0d0
	  stack(i,j,k,l)=ddum
 	endfor
      endfor
    endfor
  endfor


		;****************************************
		;****************************************
  adas205_plot, $
		ndlev,  ndtem,  ndden,  ndmet,  $
		titled, title,  gtit1,  dsninc, $
		iz,     itsel,  tev,            $
		grpscal,			$
		xmin,	xmax,	ymin,	ymax,	$
		il,     nmet,   nord,   maxd,   $
		lmetr,  imetr,  iordr,  densa,  $
		strga,  stack, 			$
		hrdout,	hardname,device,header,	$
		bitfile, gomenu,		$
		FONT=font, WIN_NUMB=win_numb


END
