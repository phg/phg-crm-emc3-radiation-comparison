; Copyright (c) 1998, Strathclyde University.
; SCCS info: Module @(#)$Header: /home/adascvs/idl/adas2xx/adas213/check_xref_213.pro,v 1.1 2004/07/06 12:04:55 whitefor Exp $ Date $Date: 2004/07/06 12:04:55 $
;+
; PROJECT:
;       ADAS IBM MVS to UNIX conversion
;
; NAME:
;	CHECK_XREF_213
;
; PURPOSE:
;	Checks the cross reference file used by ADAS213 to see if the
;	named files exist.
;
; EXPLANATION:
;	The filename is passed from bdspf0. The first four fields are checked
;	to see whether they are valid filenames. If they are not, then
;	a warning is given.
;
; USE:
;	The use of this routine is specific to ADAS213, see adas213.pro.
;
; INPUTS:
;	DSFULL  - The filename of the cross reference file
;
; OPTIONAL INPUTS:
;	None
;
; OUTPUTS:
;	The return value indicates whether there were no errors (1) or
;       if there were errors and the user wants to cancel (0). If the user
;	selects 'Continue' on the warning box then OK=1.
;
; OPTIONAL OUTPUTS:
;	None
;
; KEYWORD PARAMETERS:
;	FONT	- Supplies the font to be used for the interface widgets.
;
; CALLS:
;	CHECK_FILE Included here - checks the file priorities using
;	FILE_ACC  Checks file access priorities
;	POPUP	  Popup query widget
;
; SIDE EFFECTS:
;	NONE
;
; CATEGORY:
;	Adas system.
;
; WRITTEN:
;       Hugh Summers, University of Strathclyde
;	JA8.08
;	Tel. 0141-553-4196
;
; DATE:	30-03-98
;
; MODIFIED:
;	1.1	Hugh Summers
;		First Release
; VERSION:
;	1.1	30-03-98
;
;-----------------------------------------------------------------------------

PRO check_file, file, warning, nwarn

	;**** Check for "", an environment variable
    a=strpos(file,'"')
    b=strpos(file,'"',a+1)
    if a ne -1 then begin
	var = strmid(file,a+1,b-a-1)
	varval = getenv(var)
	file = strmid(file,0,a-1)+varval+strmid(file,b+1,strlen(file))
    endif
	;**** Check for comments
    c=strpos(file,":")
    if c ne -1 then begin
	file = strcompress(strmid(file,0,c-1), /remove_all)
    endif

    file_acc, file, exist, read, write,execute, filetype
    if exist eq 0 then begin
	warning(nwarn)=file+' : does not exist'
	nwarn=nwarn+1
    endif else if read eq 0 then begin
	warning(nwarn)=file+' : you do not have read permission'
	nwarn=nwarn+1
    endif else if filetype eq 'd' then begin
	warning(nwarn)=file+' : this is a directory'
	nwarn=nwarn+1
    endif
	
END

PRO check_dir, file, warning, nwarn

	;**** Check for "", an environment variable
    a=strpos(file,'"')
    b=strpos(file,'"',a+1)
    if a ne -1 then begin
	var = strmid(file,a+1,b-a-1)
	varval = getenv(var)
	file = strmid(file,0,a-1)+varval+strmid(file,b+1,strlen(file))
    endif
	;**** Check for comments
    c=strpos(file,":")
    if c ne -1 then begin
	file = strcompress(strmid(file,0,c-1), /remove_all)
    endif
    	;**** Extract the directory name
    v=0
    while v ne -1 do begin
	pos=v
	v = strpos(file,"/",pos+1)
    endwhile
    if pos eq 0 then dir='./' else dir = strmid(file,0,pos+1)

    file_acc, dir, exist, read, write,execute, filetype
    if exist eq 0 then begin
	warning(nwarn)='Directory ' + dir + ' : does not exist'
	nwarn=nwarn+1
    endif else if write eq 0 then begin
	warning(nwarn)= 'Directory ' + dir + ' : you do not have write permission'
	nwarn=nwarn+1
    endif else if filetype ne 'd' then begin
	warning(nwarn)= dir + ' : not a directory'
	nwarn=nwarn+1
    endif
	
END

FUNCTION check_xref_213, dsfull

    warning = strarr(30)  ; Take 30 as the maximum number of warnings!
    warning(0) = 'References to the following files were found in the input file:'
    nwarn=1
    filetype=''
    file=''

    openr, unit, dsfull, /get_lun
	;***************************************
	;**** Check that input files exist ****
	;***************************************
; Read dsnsp
    readf, unit, file, format='(/,/,1A80,/,/,/)
    check_file, file, warning, nwarn

; Read dsnbd
LAB1:
    readf, unit, file
    if strlen(strcompress(file, /remove_all)) gt 0 then begin
        check_file, file, warning, nwarn
	goto, LAB1
    endif
	;***************************************************
	;**** Check that output files can be written to ****
	;***************************************************
; Read dsnspo
    readf, unit, file, format='(/,/,1A80)'
    check_dir, file, warning, nwarn

; Read dsnmco
    readf, unit, file
    check_dir, file, warning, nwarn

    close, unit

    if nwarn gt 1 then begin
	message=warning(0:nwarn-1)
	action = popup(message=message, title = '!!! ADAS213: Warning !!!',$
		       buttons=['Cancel','Continue'])
	if action eq 'Cancel' then return, 0   else return, 1
    endif else return, 1

END

