; Copyright (c) 1998, Strathclyde University.
; SCCS info: Module @(#)$Header: /home/adascvs/idl/adas2xx/adas213/bdspf1.pro,v 1.1 2004/07/06 11:37:54 whitefor Exp $ Date $Date: 2004/07/06 11:37:54 $
;
;+
; PROJECT:
;       ADAS IBM MVS to UNIX conversion
;
; NAME:
;	BDSPF1
;
; PURPOSE:
;	IDL user interface and communications with ADAS213 FORTRAN
;	process via pipe.
;
; EXPLANATION:
;	In this routine the 'Output Options' part of the
;	interface is invoked.  The resulting action of the user's
;	is written to the FORTRAN via the pipe.
;	Communications are with the FORTRAN subroutine BDSPF1.
;	Note that the usual large amount of FORTRAN-IDL pipe 
;	communications in these routines has been removed due to the
;	fact that there are two FORTRAN routines rather than the usual
;	one and the communications are therefore handled elsewhere.
;
; USE:
;	The use of this routine is specific to ADAS213, See adas213.pro.
;
; INPUTS:
;	PIPE	- The IDL unit number of the bi-directional pipe to the
;		  ADAS213 FORTRAN process.
;
;	DSFULL  - The name of the data set being analyzed.
;
;	VALUE	- A structure which determines the initial settings of
;		  the dataset selection widget.  The initial value is
;		  set in adas213.pro.  VALUE is passed un-modified
;		  through to cw_adas_in.pro, see that routine for a full
;		  description.
;
;	HEADER  - Header information used for text output.
;
; OPTIONAL INPUTS:
;	None.
;
; OUTPUTS:
;	LPEND	- Integer; indicates user action on leaving output
;		  selections.  lpend is set to 1 if the user exited
;		  with the 'Cancel' button, 0 if the user exited
;		  with 'Run Now' and 2 if the user exited with 'Run in 
;		  Batch'.
;
;	VALUE	- On output the structure records the final settings of
;		  the output selection widget if the user pressed either
;		  of the 'Run' buttons otherwise it is not changed from input.
;
; OPTIONAL OUTPUTS:
;	None.
;
; KEYWORD PARAMETERS:
;
;	FONT	- Supplies the font to be used for the interface widgets.
;
; CALLS:
;	ADAS213_OUT	User interface - output options.
;
; SIDE EFFECTS:
;	This routine communicates with the ADAS213 FORTRAN process
;	via a UNIX pipe.
;
; CATEGORY:
;	Adas system.
;
; WRITTEN:
;       Hugh Summers, University of Strathclyde
;	JA8.08
;	Tel. 0141-553-4196
;
; DATE:	30-03-98
;
; MODIFIED:
;	1.1	Hugh Summers
;		First release.
; VERSION:
;	1.1	30-03-98
;-
;-----------------------------------------------------------------------------

PRO   bdspf1, pipe, lpend, value, dsfull, header, 			$
              FONT=font

                ;**** Return to caller on error ****

    ON_ERROR, 2

                ;**** Set defaults for keywords ****

    IF NOT (KEYWORD_SET(font)) THEN font = ''

		;**** Define some variables before read from pipe ****


		;**************************************
		;**** Pop-up output options widget ****
		;**************************************

    adas213_out, value, dsfull, action, FONT=font

		;*************************************************
		;**** Act on the output from the widget       ****
		;**** There are three    possible actions     ****
		;**** 'Cancel', 'Run Now' and 'Run in Batch'. ****
		;*************************************************

    if action eq 'Run Now' then begin
        lpend = 0
    endif else begin
        lpend = 1
    end

		;*******************************
		;**** Write data to fortran ****
		;*******************************

    printf, pipe, lpend

    if (lpend eq 0) then begin

	yes="YES"
	no="NO "
	if value.yesno eq 0 then printf, pipe, no	$
	else printf, pipe, yes
	printf, pipe, value.texdsn
	printf, pipe, value.texout

    endif
		;**************************************************
		;**** Set messages and settings ready for when ****
		;**** output window is re-displayed.           ****
		;**************************************************

    if lpend eq 0 and value.texout eq 1 then begin
        if value.texrep ge 0 then value.texrep = 0
	value.texmes = 'Output written to file.'
    endif else value.texmes=' '

END

