; Copyright (c) 1998, Strathclyde University.
; SCCS info: Module @(#)$Header: /home/adascvs/idl/adas2xx/adas213/adas213_out.pro,v 1.1 2004/07/06 10:24:21 whitefor Exp $ Date $Date: 2004/07/06 10:24:21 $
;+
; PROJECT:
;       ADAS IBM MVS to UNIX conversion
;
; NAME:
;	ADAS213_OUT
;
; PURPOSE:
;	IDL ADAS user interface, output options.
;
; EXPLANATION:
;	This routine creates and manages a pop-up window which allows
;	the user to select options and input data to control ADAS213
;	graphical and file output.
;
; USE:
;	This routine is specifically for use with adas213.             
;
; INPUTS:
;	VAL	- A structure which determines the initial settings of
;		  the output options widget.  The value is passed
;		  unmodified into cw_adas213_out.pro.
;
;		  See cw_adas213_out.pro for a full description of this
;		  structure.
;
;	DSFULL	- String; The full system file name of the input 
;		  dataset selected by the user for processing.
;
; OPTIONAL INPUTS:
;	None.
;
; OUTPUTS:
;	ACT	- String; Either 'Done' or 'Cancel' for the button the
;		  user pressed to terminate the output options window.
;
; OPTIONAL OUTPUTS:
;	None.
;
; KEYWORD PARAMETERS:
;	FONT	- Supplies the font to be used for the interface widgets.
;
; CALLS:
;	CW_ADAS213_OUT	Creates the output options widget.
;	XMANAGER
;	See side effects for widget management routine.
;
; SIDE EFFECTS:
;	This routine uses a common block OUT213_BLK to maintain its state.
;	ADAS213_OUT_EV	is included in this file and is called
;	indirectly from XMANAGER during widget management.
;
; CATEGORY:
;	Compound Widget.
;
; WRITTEN:
;       Hugh Summers, University of Strathclyde
;	JA8.08
;	Tel. 0141-553-4196
;
; DATE:	30/03/98
;
; MODIFIED:
;	1.1	Hugh Summers		30/03/98
; VERSION:
;	1.1	First Release	
;-
;-----------------------------------------------------------------------------


pro adas213_out_ev, event

  common out213_blk,action,value
	

		;**** Find the event type and copy to common ****

    action = event.action

    case action of

		;**** 'Run Now' button ****

	'Run Now'  : begin

			;**** Get the output widget value ****

     		     child = widget_info(event.id, /child)
		     widget_control, child, get_value=value 

                        ;*****************************************
			;**** Kill the widget to allow IDL to ****
			;**** continue and interface with     ****
			;**** FORTRAN only if there is work   ****
			;**** for the FORTRAN to do.          ****
			;*****************************************

		     widget_control, event.top, /destroy
	          end

		;**** 'Cancel' button ****

	'Cancel': widget_control, event.top, /destroy

        ELSE: begin   				
			;do nothing 
              end
    endcase

end

;-----------------------------------------------------------------------------


pro adas213_out, val, dsfull, act,  font=font

  common out213_blk, action, value

		;**** Copy value to common ****

  value = val

		;**** Set defaults for keywords ****

  if not (keyword_set(font)) then font = ''

		;***************************************
		;**** Pop-up a new widget           ****
		;***************************************

                ;**** create base widget ****

  outid = widget_base(title='ADAS213 OUTPUT OPTIONS', xoffset=100, yoffset=100)

		;**** Declare output options widget ****

  cwid = cw_adas213_out(outid, dsfull, value=value,  			$
			 font=font )

		;**** Realize the new widget ****
  dynlabel, outid
  widget_control, outid, /realize

		;**** make widget modal ****

  xmanager,'adas213_out', outid, event_handler='adas213_out_ev',	$
                          /modal, /just_reg
 
		;**** Return the output value from common ****

  act = action
  val = value

END

