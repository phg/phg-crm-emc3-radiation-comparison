; Copyright (c) 1998, Strathclyde University.
; SCCS info: Module @(#)$Header: /home/adascvs/idl/adas2xx/adas213/bdspf2.pro,v 1.1 2004/07/06 11:37:57 whitefor Exp $ Date $Date: 2004/07/06 11:37:57 $
;
;+
; PROJECT:
;       ADAS IBM MVS to UNIX conversion
;
; NAME:
;	BDSPF2
;
; PURPOSE:
;	IDL user interface and communications with ADAS213 FORTRAN
;	process via pipe.
;
; EXPLANATION:
;	This routine creates a progress indicator which is updated as
;	the FORTRAN progresses. The FORTRAN communicates via a pipe
;	the total number of stages and the stage which has been reached.
;
; USE:
;	The use of this routine is specific to ADAS213. See adas213.pro.
;
; INPUTS:
;	PIPE	- The IDL unit number of the bi-directional pipe to the
;		  ADAS213 FORTRAN process.
;
; OPTIONAL INPUTS:
;	None.
;
; OUTPUTS:
;	None
;
; OPTIONAL OUTPUTS:
;	None.
;
; KEYWORD PARAMETERS:
;
;	FONT	- Supplies the font to be used for the interface widgets.
;
; CALLS:
;
; SIDE EFFECTS:
;	This routine communicates with the ADAS213 FORTRAN processes
;	via UNIX pipes.
;	It also pops up an information widget which keeps the user 
;	updated as the calculation progresses.
;
; CATEGORY:
;	Adas system.
;
; WRITTEN:
;       Hugh Summers, University of Strathclyde
;	JA8.08
;	Tel. 0141-553-4196
;	
; DATE:	30-03-98	
;
; MODIFIED:
;	1.1	Hugh Summers
;		First release
;
; VERSION:
;	1.1	30-03-98
;-
;-----------------------------------------------------------------------------

PRO bdspf2, pipe, FONT=font


                ;**** If there is an io error caused ****
		;**** by the Fortran crashing handle it ****

    ON_IOERROR, PIPE_ERR

                ;**** Set defaults for keywords ****

    IF NOT (KEYWORD_SET(font)) THEN font = ''

		;**** Create an information widget ****

    widget_control, /hourglass
    base = widget_base(/column, xoffset=300, yoffset=200,               $
    title = "ADAS213: INFORMATION")
    lab0 = widget_label(base, value='')
    lab1 = widget_label(base,                                           $
    value="    ADAS213 COMPUTATION UNDERWAY - PLEASE WAIT    ")
    grap = widget_draw(base, ysize=20, xsize=480)
    lab2 = widget_label(base,                                           $
    value = "READING DATA")
    dynlabel, base

    widget_control, base, /realize

    num = 0
    readf, pipe, num

		;**** Update information widget ****

    next = 0
    step = 1000.0/num
		;**** Scale time in an ad hoc exponential fashion      ****
		;**** to account for the larger number of missing      ****
		;**** transitions as the parent index is increased.    ****
		;**** b should be changed if this is giving rediculous ****
		;**** rates but it works OK for clike/clike_o2ls.dat.  ****
		;**** Its magnitude should be increased to slow down   ****
		;**** the apparent processing time at later times.     ****
;    b = -4.0/num
;    c = 1.0/(1-exp(num*b))
    for i=0,num-1 do begin
    	readf, pipe, next
	p = float(i)/num*1000	;**** This would give a linear increase in
	q = p+step		;**** loop time but not processing time
;	p = (1-exp(i*b))*c*1000
;	q = (1-exp((i+1)*b))*c*1000
        for j=fix(p),fix(q) do begin
            x = (float(j)+1)/1000.0
            plots, [x, x],[0.0,1.0], /normal
        endfor
        widget_control, lab2, set_value="PROCESSING"+			$
		string(fix(q/10),format='(I2)')+"% COMPLETED"
    endfor

    goto, DONE

PIPE_ERR:
    mess = ['Error communicating with adas213 Fortran.',$
	    'The Fortran calculation has crashed.']
    action = popup(message=mess, title = '*** ADAS213 Error ***',$
		 buttons=['OK'])

DONE:
		;**** Destroy the information widget ****

    widget_control, base, /destroy

END
