; Copyright (c) 1998, Strathclyde University.
; SCCS info: Module @(#)$Header: /home/adascvs/idl/adas2xx/adas214/cw_adas214_out.pro,v 1.2 2004/07/06 12:35:19 whitefor Exp $ Date $Date: 2004/07/06 12:35:19 $
;
;+
; PROJECT:
;       ADAS 
;
; NAME:
;	CW_ADAS214_OUT()
;
; PURPOSE:
;	Generates a widget used for graphical output selection.
;
; EXPLANATION:
;	This widget consists of a titled activation button, a text
;	widget for entering a graph title, a scaling ranges input
;	widget cw_adas_ranges.pro, an output file widget
;	cw_adas_outfile.pro, a single selection widget cw_single_sel.pro
;	with a list of available hardcopy devices.  A general error
;	message is also included in the widget.
;
;	cw_adas_ranges.pro and cw_adas_outfile.pro handle their own
;	events and perform their own error checking.  The general
;	activation button desensitises all other component widgets when
;	it is set off.  The only other events processed by this widget
;	are from the two single selection lists for graphs and devices.
;
;	Many of the elements of this widget are optional and controlled
;	by the caller.
;
;	This widget generates an event when the activation button is
;	pressed.  The event structure is;
;		{ID:0L, TOP:0L, HANDLER:0L, OUTBUT:0}
;	where OUTBUT is 0 if the activation button is off and 1 if
;	the activaion button is on.
;
; USE:
;	An example of use is given below.  Also see cw_adas205_out.pro
;	for another example.
;
;	grplist = ['Graph 1','Graph2','Graph3']
;	devlist = ['Device 1','Device 2','Device 3']
;	value = {$
;		OUTBUT:0 , GTIT1:'', $
;		SCALBUT:0, $
;		XMIN:'', XMAX:'', $
;		YMIN:'', YMAX:'', $
;		HRDOUT:0, HARDNAME:'', $
;		GRPDEF:'', GRPFMESS:'', $
;		GRPSEL:-1, GRPRMESS:'', $
;		DEVSEL:-1, GRSELMESS:'' }
;
;	base=widget_base(/column)
;	grpid=cw_adas_gr_sel(base,value=value,grplist=grplist,devlist=devlist)
;	rc=widget_button(base,value='Done')
;	widget_control,base,/realize
;	rc=widget_event()
;	widget_control,grpid,get_value=value
;
;	if strtrim(value.grselmess) eq '' and $
;	   strtrim(value.grpfmess) eq '' and $
;	   strtrim(value.grprmess) eq '' then begin
;
;	  if value.outbut eq 1 then begin
;	    print,'Graph selected: ',grplist(value.grpsel)
;
;	    if value.hrdout eq 1 then begin
;	      print,'Device selected: ',devlist(value.devsel)
;	      print,'Hard copy file: ',value.hardname
;	    end
;
;	    if value.scalbut eq 1 then begin
;	      print,'Scaling ranges:'
;	      print,'XMIN: ',value.xmin,'  XMAX: ',value.xmax
;	      print,'YMIN: ',value.ymin,'  YMAX: ',value.ymax
;	    end
;	  end else begin
;	    print,'No graphical output requested.'
;	  end
;
;	end else begin
;	  print,'Graphical selection error;'
;	  print,value.grselmess,value.grpfmess,value.grprmess
;	end
;
; INPUTS:
;       PARENT   - The ID of the parent widget.
;
; OPTIONAL INPUTS:
;	None.
;
; OUTPUTS:
;       The return value of this function is the ID of the compound
;       widget which is of type LONG integer.
;
; OPTIONAL OUTPUTS:
;       None
;
; KEYWORD PARAMETERS:
;	SIGN	  - Integer: < 0  => Graph ranges must be negative
;			     0	  => Graph ranges can be positive or negative
;			     > 0  => Graph ranges must be positive
;	OUTPUT	  - A string; Label to use for activate button.  Default
;		    'Graphical Output'.
;
;	GRPLIST	  - A string array; A list of graphs one array element per
;		    graph.  If the list is empty i.e grplist='' then the
;		    selection list widget will not appear.  Default value=''.
;
;	LISTTITLE - A string; title for the graph selection list.
;		    Default 'Select Graph'.
;
;	DEVLIST	  - A string array; A list of devices one array element per
;		    device.  If the list is empty i.e devlist='' then the
;		    selection list widget will not appear.  Default value=''.
;
;	VALUE	  - A structure which defines the value of this widget.
;		    The structure is made up of a number of parts each
;		    pertaining to one of the widgets in this compound
;		    widget.  Default value;
;
;		      { OUTBUT:0 , GTIT1:'', $
;			SCALBUT:0, $
;			XMIN:'', XMAX:'', $
;			YMIN:'', YMAX:'', $
;			HRDOUT:0, HARDNAME:'', $
;			GRPDEF:'', GRPFMESS:'', $
;			GRPSEL:-1, GRPRMESS:'', $
;			DEVSEL:-1, GRSELMESS:'' }
;
;		    Note these do not appear in the same order as in the
;		    value structure.
;		    General;
;			OUTBUT	   Integer; Activation button 1 on, 0 off
;			GTIT1	   String; Graph title
;			GRSELMESS  String; General error message
;
;		    For CW_ADAS_RANGES;
;			SCALBUT	   Integer; Scaling activation 1 on, 0 off,
;				   -1 no widget.
;			XMIN	   String; x-axis minimum, string of number
;			XMAX	   String; x-axis maximum, string of number
;			YMIN	   String; y-axis minimum, string of number
;			YMAX	   String; y-axis maximum, string of number
;			GRPRMESS   String; Scaling ranges error message
;
;		    For CW_ADAS_OUTFILE hard copy output file;
;			HRDOUT	   Integer; Hard copy activ' 1 on, 0 off,
;				   -1 no widget.
;			HARDNAME   String; Hard copy output file name
;			GRPDEF	   String; Default output file name 
;			GRPFMESS   String; File name error message
;			(Replace and append values are fixed)
;
;		    For CW_SINGLE_SEL graph selection;
;			DEVSEL	   Integer; index of selected device in DEVLIST
;				   -1 indicates no selection made.
;
;       FONT      - String; Name of a font to use for all text in this widget.
;
;	UVALUE	  - A user value for this widget.
;
; CALLS:
;	CW_ADAS_RANGES	Specify explict x and y axis ranges.
;	CW_ADAS_OUTFILE	Specify output file name.
;	CW_SINGLE_SEL	A single selection list widget.
;       See side effects for other related widget management routines.
;
; SIDE EFFECTS:
;       Three other routines are included which are used to manage the
;       widget;
;
;	GR_SEL_SET_VAL
;	GR_SEL_GET_VAL()
;	GR_SEL_EVENT()
;
; CATEGORY:
;	Compound Widget
;
; WRITTEN:
;       Stuart Loch, University of Strathclyde, 1998.
;	  Based on cw_adas312_out.pro v 1.1 .
;
; MODIFIED:
;                       
; 	1.1	Stuart Loch.
;		First version.
;	1.2 	Richard Martin
;		Changed grselval to be anonymous structure.
;
; VERSION:
;     1.1   18-06-98
;	1.2	04-12-98
;
;
;-
;-----------------------------------------------------------------------------

PRO gr_sel_set_val, id, value


		;**** Return to caller on error ****
  ON_ERROR, 2

               ;***************************************
               ;****     Retrieve the state        ****
               ;**** Get first_child widget id     ****
               ;**** because state is stored there ****
               ;***************************************

  first_child = widget_info(id, /child)
  widget_control, first_child, get_uvalue = state

		;**** Value for the file widget ****
  outfval = { OUTBUT:value.hrdout, APPBUT:-1, REPBUT:0, $
              FILENAME:value.hardname, DEFNAME:value.grpdef, $
	      MESSAGE:value.grpfmess }
	      
		;**** Value for the paper output widget ****
  outpval = { OUTBUT:value.texout, APPBUT:-1, REPBUT:0, $
              FILENAME:value.texdsn, DEFNAME:value.texdef, $
	      MESSAGE:value.texmes }
	      
		;**** Value for the ranges widget ****
  if state.scbasid gt 0 then begin
    rngval = {  SCALBUT:value.scalbut, $
		XMIN:value.xmin, XMAX:value.xmax, $
		YMIN:value.ymin, YMAX:value.ymax, $
		GRPRMESS:value.grprmess, SIGN:value.sign }
  end
		;**** Value for the text widget ****
   outtval = { OUTBUT:grselval.txtout, APPBUT:-1, REPBUT:0, $
	       FILENAME:grselval.txtname, DEFNAME:grselval.txtdfn,$
	       MESSAGE:grselval.grptmess }
	       	
		;**** Sensitise or desensitise with output button setting ****
		
  if value.outbut eq 0 then begin

    widget_control,state.actid,set_button=0

		;**** desensitise graph title ****
    widget_control,state.tibasid,sensitive=0

		;**** desensitise graph selection list ****
    if state.listid gt 0 then widget_control,state.lsbasid,sensitive=0

		;**** desensitise device selection list ****
;    if state.devbasid gt 0 then widget_control,state.devbasid,sensitive=0

		;**** desensitise filename and hide settings ****
    widget_control,state.devbasid,sensitive=0
    widget_control,state.fbaseid,sensitive=0
    outfval.outbut = 0
    widget_control,state.fileid,set_value=outfval
    widget_control,state.paperid,set_value=outtval
    widget_control,state.paperidtxt,set_value=outpval


		;**** desensitise ranges and hide settings ****
    if state.scbasid gt 0 then begin
      widget_control,state.scbasid,sensitive=0
      rngval.scalbut = 0
      widget_control,state.rangeid,set_value=rngval
    end

  end else begin

    widget_control,state.actid,set_button=1

    widget_control,state.tibasid,/sensitive

    if state.listid gt 0 then widget_control,state.lsbasid,/sensitive

   ; if state.devbasid gt 0 then widget_control,state.devbasid,/sensitive
    widget_control,state.devbasid,/sensitive
    widget_control,state.fbaseid,/sensitive
    widget_control,state.fileid,set_value=outfval
    widget_control,state.paperid,set_value=outtval
    widget_control,state.paperidtxt,set_value=outpval

    if state.scbasid gt 0 then begin
      widget_control,state.scbasid,/sensitive
      widget_control,state.rangeid,set_value=rngval
    end

  end

		;**** Update value of graph selection ****
  if state.listid gt 0 then widget_control,state.listid,set_value=value.grpsel

		;**** Update value of device selection ****
  if state.devid gt 0 then widget_control,state.devid,set_value=value.devsel

		;**** Update message ****
  if strtrim(value.grselmess) ne '' then begin
    message = value.grselmess
  end else begin
    message = ' '
  end
  widget_control,state.messid,set_value=message

		;**** Copy the new value to state structure ****
  state.grselval.outbut = value.outbut
  state.grselval.gtit1 = value.gtit1
  state.grselval.scalbut = value.scalbut
  state.grselval.xmin = value.xmin
  state.grselval.xmax = value.xmax
  state.grselval.ymin = value.ymin
  state.grselval.ymax = value.ymax
  state.grselval.hrdout = value.hrdout
  state.grselval.hardname = value.hardname
  state.grselval.txtout = value.txtout
  state.grselval.txtname = value.txtname
  state.grselval.txtdfn = value.txtdfn
  state.grselval.texout = value.texout
  state.grselval.texrep = value.texrep
  state.grselval.texdsn = value.txtname
  state.grselval.texdef = value.texdef
  state.grselval.texmes = value.texmes
  state.grselval.grpdef = value.grpdef
  state.grselval.grpfmess = value.grpfmess
  state.grselval.grptmess = value.grptmess
  state.grselval.grpsel = value.grpsel
  state.grselval.grprmess = value.grprmess
  state.grselval.devsel = value.devsel
  state.grselval.grselmess = value.grselmess
  state.grselval.new = value.new


		;**** Save the new state ****
  widget_control, first_child, set_uvalue=state, /no_copy

END

;-----------------------------------------------------------------------------


FUNCTION gr_sel_get_val, id


		;**** Return to caller on error ****
  ON_ERROR, 2

               ;***************************************
               ;****     Retrieve the state        ****
               ;**** Get first_child widget id     ****
               ;**** because state is stored there ****
               ;***************************************

  first_child = widget_info(id, /child)
  widget_control, first_child, get_uvalue = state

		;**** Get the graph title ****
		;*** truncate to 30 characters ***
  widget_control,state.titleid,get_value=gtit1
  if (strlen(gtit1(0)) gt 30 ) then begin
	gtit1(0) = strmid(gtit1(0),0,30)
        message = 'Warning : Title truncated to 30 characters'
        widget_control,state.messid,set_value=message
        wait, 1.0
	message = ' '
  endif
  state.grselval.gtit1 = gtit1(0)

		;**** Get the latest information ****
  widget_control,state.fileid,get_value=outfval
  state.grselval.hardname = outfval.filename
  state.grselval.grpfmess = outfval.message
  widget_control,state.paperid,get_value=outtval
  state.grselval.txtname = outtval.filename
  state.grselval.grptmess = outtval.message
  state.grselval.txtout = outtval.outbut
  if state.grselval.outbut eq 1 then state.grselval.hrdout = outfval.outbut
  if state.grselval.outbut eq 1 then state.grselval.txtout = outtval.outbut
  widget_control, state.paperidtxt, get_value=outpval
  state.grselval.texout = outpval.outbut
  state.grselval.texdsn = outpval.filename
  
		;**** Get scaling ranges and copy to value ****
  if state.scbasid gt 0 then begin
    widget_control,state.rangeid,get_value=rngval
    state.grselval.xmin = rngval.xmin
    state.grselval.xmax = rngval.xmax
    state.grselval.ymin = rngval.ymin
    state.grselval.ymax = rngval.ymax
    state.grselval.grprmess = rngval.grprmess
    if state.grselval.outbut eq 1 then state.grselval.scalbut = rngval.scalbut
  end

		;**** Clear any error message ****
  if state.grselval.hrdout ge 0 then begin
    state.grselval.grselmess = ''
    widget_control,state.messid,set_value=' '
  end

  if state.grselval.outbut eq 1 then begin

		;**** Check that a graph has been selected if appropriate ****
    if state.listid gt 0L then begin
      if state.grselval.grpsel lt 0 then begin
        state.grselval.grselmess = '**** You must select a graph and a'$
	  +' temperature ****'
        widget_control,state.messid,set_value=state.grselval.grselmess
      end
    end

		;**** Check that a device has been selected if appropriate ****
    if state.grselval.hrdout eq 1 and state.devid gt 0L then begin
      if state.grselval.devsel lt 0 then begin
        state.grselval.grselmess = '**** You must select a device ****'
        widget_control,state.messid,set_value=state.grselval.grselmess
      end
    end

  endif

  RETURN, state.grselval

END

;-----------------------------------------------------------------------------

FUNCTION gr_sel_event, event


		;**** Base ID of compound widget ****
  base=event.handler

               ;***************************************
               ;****     Retrieve the state        ****
               ;**** Get first_child widget id     ****
               ;**** because state is stored there ****
               ;***************************************

  first_child = widget_info(base, /child)
  widget_control, first_child, get_uvalue = state

  new_event = 0L
		;************************
		;**** Process Events ****
		;************************
  CASE event.id OF

    state.actid: begin
			;**** Sensitise and desensitise with toggle ****
			;**** of output button.			    ****
	if state.grselval.outbut eq 1 then begin

	  state.grselval.outbut = 0
	;  new_event = {ID:base, TOP:event.top, HANDLER:0L, OUTBUT:0}

			;**** Desensitise graph title ****
	  widget_control,state.tibasid,sensitive=0

			;**** Desensitise graph selection list ****
	  if state.listid gt 0 then $
				widget_control,state.lsbasid,sensitive=0

			;**** Desensitise device selection list ****
	  if state.devbasid gt 0 then widget_control,state.devbasid,sensitive=0

			;**** Desensitise filename and hide settings ****
	  outfval = {	OUTBUT:0, APPBUT:-1, REPBUT:0, $
                	FILENAME:state.grselval.hardname, $
			DEFNAME:state.grselval.grpdef, $
	        	MESSAGE:'' }
	 
	  widget_control,state.fbaseid,sensitive=0
	  widget_control,state.fileid,set_value=outfval

			;**** Desensitise paper output and hide settings ****
;          outpval = { OUTBUT:0,  APPBUT:-1, $
;	    	      REPBUT:0,  FILENAME:state.grselval.texdsn, $
;	              DEFNAME:state.grselval.texdef, MESSAGE:'' }
;	  widget_control,state.basetxt, sensitive=0
;          widget_control, state.paperidtxt, set_value=outpval

			;**** Desensitise scaling and hide settings ****
	  if state.scbasid gt 0 then begin
	  rngval = {	SCALBUT:0, $
			XMIN:state.grselval.xmin, XMAX:state.grselval.xmax, $
			YMIN:state.grselval.ymin, YMAX:state.grselval.ymax, $
			GRPRMESS:'' , SIGN:state.grselval.sign}
	    widget_control,state.scbasid,sensitive=0
	    widget_control,state.rangeid,set_value=rngval
	  end

	end else begin

	  state.grselval.outbut = 1
;	  new_event = {ID:base, TOP:event.top, HANDLER:0L, OUTBUT:1}


			;**** Sensitise graph title ****
	  widget_control,state.tibasid,/sensitive

			;**** Sensitise selection list ****
	  if state.listid gt 0 then widget_control,state.lsbasid,/sensitive

			;**** Sensitise device selection list ****
	  if state.devbasid gt 0 then widget_control,state.devbasid,/sensitive

			;**** Sensitise filename and restore settings ****
	  widget_control,state.fbaseid,/sensitive
	  widget_control,state.fileid,get_value=outfval
	  outfval.outbut = state.grselval.hrdout
	  widget_control,state.fileid,set_value=outfval

			;**** Sensitise paper output and restore settings ****
;	  widget_control,state.basetxt,/sensitive
;	  widget_control,state.paperidtxt,get_value=outpval
;	  outpval.outbut = state.grselval.texout
;	  widget_control,state.paperidtxt,set_value=outpval

			;**** Sensitise scaling and restore settings ****
	  if state.scbasid gt 0 then begin
	    widget_control,state.scbasid,/sensitive
	    widget_control,state.rangeid,get_value=rngval
	    rngval.scalbut = state.grselval.scalbut
	    widget_control,state.rangeid,set_value=rngval
	  end

	end
    end

    state.listid: state.grselval.grpsel = event.index

    state.devid: state.grselval.devsel = event.index
    
    state.doneid: begin
    			;**********************************;
    			;  First Check for error messages  ;
    			;**********************************
    			
    		widget_control,state.rangeid,get_value=rngval
    		widget_control,state.paperid,get_value=outtval
    		widget_control,state.fileid,get_value=outfval
    		widget_control,state.paperidtxt,get_value=outpval
    		if strtrim(rngval.grprmess) ne '' or $
    		   strtrim(outtval.message) ne '' or $
    		   strtrim(outpval.message) ne '' or $
    		   strtrim(outfval.message) ne '' then begin
    			new_event = 0L
    		end else begin
    			new_event = { ID:0L, TOP:event.top,$
 			HANDLER:0L,ACTION:'Done'}
    		end
    		
    		  end
    state.cancelid: begin
    			new_event = { ID:0L, TOP:event.top,$
 			HANDLER:0L,ACTION:'Cancel'}
    		    end
    		    
    state.outid : begin
    	
    			new_event = { ID:0L, TOP:event.top,$
 			HANDLER:0L,ACTION:'Menu'}
    		  end

    ELSE:

  ENDCASE

		;**** Save the new state structure ****
    widget_control, first_child, set_uvalue=state, /no_copy

  RETURN, new_event
END

;-----------------------------------------------------------------------------

FUNCTION cw_adas214_out, parent, dsfull, SIGN=sign, OUTPUT=output, $
	    GRPLIST=grplist, LISTTITLE=listtitle, DEVLIST=devlist, $
	    VALUE=value, FONT=font, UVALUE=uvalue,bitmap1



  IF (N_PARAMS() LT 1) THEN MESSAGE, 'Must specify PARENT for cw_adas_gr_sel'

		;**** Set defaults for keywords ****
  IF NOT (KEYWORD_SET(sign)) THEN sign = 0
  IF NOT (KEYWORD_SET(output)) THEN output = 'Graphical Output'
  IF NOT (KEYWORD_SET(grplist)) THEN grplist = ''
  IF NOT (KEYWORD_SET(listtitle)) THEN listtitle = 'Select Graph'
  IF NOT (KEYWORD_SET(devlist)) THEN devlist = ''
  IF NOT (KEYWORD_SET(value)) THEN begin
	  grselval = {new:0, 		$
			OUTBUT:0 , GTIT1:'', 	$
			SCALBUT:0, 		$
			XMIN:'', XMAX:'', 	$
			YMIN:'', YMAX:'', 	$
			SIGN:sign, 	  	$	
			HRDOUT:0, HARDNAME:'', 	$
			TXTOUT:0, TXTNAME:'',	$
		        TEXOUT:0, $
                        TEXREP:0, TEXDSN:'',   $
                        TEXDEF:userroot+'/pass/paper.txt', $
                        TEXMES:'',   $
			TXTDFN:'',	$
			GRPDEF:'', GRPFMESS:'', $
			GRPSEL:-1, GRPRMESS:'', $
			DEVSEL:-1, GRSELMESS:'', $
		        GRPTMESS:''}
	end else begin
	  grselval = {new:value.new, $
			OUTBUT:value.outbut, GTIT1:value.gtit1, $
			SCALBUT:value.scalbut, $
			XMIN:value.xmin, XMAX:value.xmax, $
			YMIN:value.ymin, YMAX:value.ymax, $
			SIGN:sign,		  $
			HRDOUT:value.hrdout, HARDNAME:value.hardname, $
			TXTOUT:value.txtout, TXTNAME:value.txtname,   $
		        TEXOUT:value.texout, $
                        TEXREP:value.texrep, TEXDSN:value.texdsn,   $
                        TEXDEF:value.texdef, $
                        TEXMES:value.texmes,   $
			TXTDFN:value.txtdfn, $
			GRPDEF:value.grpdef, GRPFMESS:value.grpfmess, $
			GRPSEL:value.grpsel, GRPRMESS:value.grprmess, $
			DEVSEL:value.devsel, GRSELMESS:value.grselmess,$
			GRPTMESS:value.grptmess}
	end
  IF NOT (KEYWORD_SET(font)) THEN font = ''
  IF NOT (KEYWORD_SET(uvalue))  THEN uvalue = 0

		;**** Create the main base for the widget ****
  main = WIDGET_BASE(parent, UVALUE = uvalue, $
		EVENT_FUNC = "gr_sel_event", $
		FUNC_GET_VALUE = "gr_sel_get_val", $
		PRO_SET_VALUE = "gr_sel_set_val", $
		/ROW)

                ;******************************************************
                ;**** Create a dummy widget just to hold value of *****
                ;**** "state" variable so as not to get confused  *****
                ;**** with any other values. Adopt IDL practice   *****
                ;**** of using first child widget                 *****
                ;******************************************************
  
  first_child = widget_base(main,/column)

  cwid = widget_base(first_child,/column)
  
  		;*** Add input filename and browse button ***
  rc = cw_adas_dsbr(cwid, dsfull, font=font)
  
		;**** Left hand base ****
  lfid = widget_base(cwid,/column,/frame)

		;**** Right hand base ****
  rgid = widget_base(cwid,/column)
  
  		;*****bottom base ******
  ttid = widget_base(cwid,/column,/frame)
  ctid = widget_base(cwid,/column,/frame)

		;**** Create button to activate graphing ****
  base = widget_base(lfid,/column,/nonexclusive)
  actid = widget_button(base,value=output,font=font)

		;**** Graph title ****
  tibasid = widget_base(lfid,/row)
  rc = widget_label(tibasid,value='Graph Title',font=font)
  titleid = widget_text(tibasid,value=grselval.gtit1, $
			/editable,xsize=30,font=font)

		;**** Scaling and ranges widget ****
  if grselval.scalbut ge 0 then begin
    tempid = widget_base(lfid,/row)
    scbasid = widget_base(tempid,/frame)
    rngval = {  SCALBUT:grselval.scalbut, $
		XMIN:grselval.xmin, XMAX:grselval.xmax, $
		YMIN:grselval.ymin, YMAX:grselval.ymax, $
		GRPRMESS:grselval.grprmess , SIGN:sign}
    rangeid = cw_adas_ranges(scbasid, SIGN=sign, VALUE=rngval, FONT=font) 
  end else begin
    scbasid = 0L
  end



		;**** Output file name widget ****
  if grselval.hrdout ge 0 then begin
    outfval = { OUTBUT:grselval.hrdout, APPBUT:-1, REPBUT:0, $
                FILENAME:grselval.hardname, DEFNAME:grselval.grpdef, $
	        MESSAGE:grselval.grpfmess }
	    
    fbaseid = widget_base(lfid,/row,/frame)
    fileid = cw_adas_outfile(fbaseid, OUTPUT='Enable Hard Copy', $
                              VALUE=outfval, FONT=font)
  end else begin
    fbaseid = 0L
    fileid = 0L
  end
  	;******** blank label used to help position select widget ****
     temp = widget_label(fbaseid,value='        ')

		;**** Add selection list if required ****
  if strtrim(grplist(0)) ne '' then begin
     lsbasid = widget_base(tempid,/frame)
    listid = cw_single_sel(lsbasid,grplist,title=listtitle, $
				value=grselval.grpsel, font=font)
  end else begin
    grselval.grpsel = -1
    lsbasid = 0L
    listid = 0L
  end

		;**** Add hardcopy device selection list ****
  if grselval.hrdout ge 0 and strtrim(devlist(0)) ne '' then begin
    devbasid = widget_base(lfid,/row)	
    devid = cw_single_sel(fbaseid,devlist,title='Select Device', $
				value=grselval.devsel, font=font)
  end else begin
    grselval.devsel = -1
    devbasid = 0L
    devid = 0L
  end

                ;**** Widget for text output ****

  outpval = { OUTBUT:grselval.texout,  APPBUT:-1, $
	      REPBUT:0,  FILENAME:grselval.texdsn, $
	      DEFNAME:grselval.texdef, MESSAGE:grselval.texmes }
  basetxt = widget_base(ttid,/row,/frame)
  paperidtxt = cw_adas_outfile(basetxt, OUTPUT='Text Output', $
                        VALUE=outpval, FONT=font)

		  ;**** Widget for text output ****

    outtval = { OUTBUT:grselval.txtout, APPBUT:-1, REPBUT:0, 		$
	        FILENAME:grselval.txtname, DEFNAME:grselval.txtdfn,	$
	        MESSAGE:grselval.grptmess }
    base = widget_base(ctid,/column)
    paperid = cw_adas_outfile(base, OUTPUT='Passing File      ', 		$
                              VALUE=outtval, FONT=font)

		;**** Add message widget ****
  if strtrim(grselval.grselmess) ne '' then begin
    message = grselval.grselmess
  end else begin
    message = ' '
  end
  messid = widget_label(lfid,value=message,font=font)

		;**** Set initial state according to value ****
  if grselval.outbut eq 1 then begin

    widget_control,actid,set_button=1

  end else begin

    widget_control,tibasid,sensitive=0

    if scbasid gt 0 then begin
      widget_control,scbasid,sensitive=0
      rngval.scalbut = 0
      widget_control,rangeid,set_value=rngval
    end
    widget_control,devbasid,sensitive=0
    widget_control,fbaseid,sensitive=0
    outfval.outbut = 0
    widget_control,fileid,set_value=outfval
 
    if listid gt 0 then widget_control,lsbasid,sensitive=0

  ;  if devbasid gt 0 then widget_control,devbasid,sensitive=0

  end
	base1 = widget_base(cwid, /row)

    	outid = widget_button(base1, value=bitmap1)		;menu button
    	cancelid = widget_button(base1, value='Cancel',font=font)
    	doneid = widget_button(base1, value='Done', font=font)

		;**** Create state structure ****
  state = { ACTID:actid, $
		TIBASID:tibasid, TITLEID:titleid, $
		SCBASID:scbasid, RANGEID:rangeid, $
		FBASEID:fbaseid, FILEID:fileid, $
		LSBASID:lsbasid, LISTID:listid, $
		DEVBASID:devbasid, DEVID:devid, MESSID:messid, $
		GRSELVAL:grselval, FONT:font ,PAPERID:paperid,$
		CANCELID:cancelid, OUTID:outid, DONEID:doneid,$
		PAPERIDTXT:paperidtxt, BASETXT:basetxt}

		;**** Save initial state structure ****
  widget_control, first_child, set_uvalue=state,/no_copy

  RETURN, main

END
