; Copyright (c) 1998, Strathclyde University.
; SCCS info: Module @(#)$Header: /home/adascvs/idl/adas2xx/adas214/adas214_proc.pro,v 1.1 2004/07/06 10:25:04 whitefor Exp $ Date $Date: 2004/07/06 10:25:04 $
;+
; 
; PROJECT:
;         ADAS IBM MVS to UNIX conversion and development
;
; NAME:
;         ADAS214_PROC
;
; PURPOSE:
;         IDL ADAS user interface, processing options/input
;
; EXPLANATION:
;	  This routine creates and manages a pop-up window which allows
;	  the user to select options and invokes the bundling selection
;         widget for ADAS214 dataset processing.
; USE:    
;         See the ADAS214 routine bdispf.pro for an example of how to
;         use this routine.
;
; INPUTS: 
;       VAL     - A structure which determines the initial settings of
;                 the processing options compound widgets. The structure
;                 is passed unmodified through to cw_adas214.pro,
;                 see that routine for a full description.
;
;       NJLEVX2 - The number of levels associated with the superstructure
;                 template data set, see cw_adas214.pro.
;
; OPTIONAL INPUTS:
;         None
;
; OUTPUTS:
;       VAL     - On output the structure records the final settings of
;                 the processing selection widget if the user pressed the
;                 'Done' button otherwise it is not changed from input.
;
;       ACT     - A string; indicates the user's action when the pop-up
;                 window is terminated, i.e which button was pressed to
;                 complete the input.  Possible values are 'Done' and
;                 'Cancel'.
;
; OPTIONAL OUTPUTS:
;         None
;
; KEYWORD PARAMETERS:
;         WINTITLE- A title to be used on the banner for the pop-up window.
;                   Intended to indicate which application is running,
;                   e.g 'ADAS 214 PROCESSING'
;
;         TITLE   - Another title to be included in the widget.
;
;         FONT    - The name of the font to be used on the widgets.
;
; CALLS:
;	  CW_ADAS214_PROC    Processing options compound widget
;	  DYNLABEL	     Sets dynamic_resize keyword for label widgets.
;         XMANAGER           Manages the pop-up window.
;         ADAS214_PROC_EV    Called indirectly during widget management
;                            to handle the widget events. Procedure is
;                            included in this file.
;
;
; SIDE EFFECTS:
;       XMANAGER is called in /modal mode. Any other widget becomes
;       inactive.
;
; CATEGORY:
;         ADAS214 widgets
;
; WRITTEN:
;         Stuart D. Loch, Univ.of Strathclyde, 15-12-1997
;
; MODIFIED:
;	1.1	Stuart D. Loch 			
;		First release
;	
; VERSION:
; 	1.1	18-06-98
;-
;---------------------------------------------------------------------------
PRO adas214_proc_ev, event

    COMMON proc_blk, action, value

                ;**** Find the event type and copy to common ****

    action = event.action

    CASE action OF

                ;**** 'Done' button ****

        'Done'  : begin

                        ;**** Get the output widget value ****

                widget_control, event.id, get_value=value
                widget_control, event.top, /destroy
           end

                ;**** 'Cancel' button ****

        'Cancel': widget_control, event.top, /destroy

                ;**** 'Menu' button ****

        'Menu': widget_control, event.top, /destroy

	ELSE:			;**** Do nothing ****

    ENDCASE

END

;---------------------------------------------------------------------------

PRO adas214_proc, njlevx2,  bitfile, val, act, WINTITLE=wintitle,$
TITLE=title, bndl_prnts, ion, nclc, FONT=font

    COMMON proc_blk, action, value

                ;**** Set defaults for keywords ****

    IF NOT (KEYWORD_SET(wintitle)) THEN wintitle = 			$
    'ADAS214 PROCESSING OPTIONS'
    IF NOT (KEYWORD_SET(title)) THEN title = ''
    IF NOT (KEYWORD_SET(font)) THEN font = ''

            ;****************************************;
            ; copy all necessary variables to common ;
            ;****************************************;

    value = val

                ;***************************************
                ;**** Pop-up a new widget if needed ****
                ;***************************************

                ;**** create base widget ****

    procid = widget_base(TITLE=wintitle, XOFFSET=1, YOFFSET=1)

                ;**** Declare output options widget ****

    cwid = cw_adas214_proc(procid,  njlevx2, $
                       bitfile, VALUE=value, FONT=font)

                ;**** Realize the new widget ****

    dynlabel, procid
    widget_control, procid, /realize

                ;**** make widget modal ****

    xmanager, 'adas214_proc', procid, event_handler='adas214_proc_ev',	$
             /modal, /just_reg

                ;**** Return the output value from common ****

    act = action
    val = value
END
