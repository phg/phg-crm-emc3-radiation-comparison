; Copyright (c) 1998, Strathclyde University.
; SCCS info: Module @(#)$Header: /home/adascvs/idl/adas2xx/adas214/adas214_plot.pro,v 1.2 2004/07/06 10:25:01 whitefor Exp $	Date $Date: 2004/07/06 10:25:01 $ 
;+
; PROJECT:
;       ADAS IBM MVS to UNIX conversion
;
; NAME:
;	ADAS214_PLOT
;
; PURPOSE:
;	Generates ADAS214 graphical output.
;
; EXPLANATION:
;	This routine creates a window for the display of graphical
;	output. A separate routine PLOT214 actually plots the
;	graph.
;
; USE:
;	This routine is specific to ADAS214, see bdoutg.pro for
;	example use.
;
; INPUTS:
;	(Most of these inputs map exactly onto variables of the same
;	 name in the FORTRAN code.)
;
;
;	DSFULL  - String; Name of data file 
;
;	TITLX   - String; user supplied comment appended to end of title
;
;	TITLM   - String; Information about minimax fitting if selected.
;
;	UTITLE  - String; Optional comment by user added to graph title.
;
;	TEMP	- Double array; temperatures from input data file.
;	
;	RATE    - Double array; Excitation rate coefficient of selected 
;				transition from input data file
;
;	TOSA	- Double array; Selected temperatures for spline fit
;
;	ROSA	- Double array; Excitation rate coefficients for spline fit.
;
;	TOMA	- Double array; Selected temperatures for minimax fit
;
;	DATE	- String; Date of graph production
;
;	LDEF1	- Integer; 1 - use user entered graph scales
;			   0 - use default axes scaling
;
;	XMIN    - String; Lower limit for x-axis of graph, number as string.
;
;	XMAX    - String; Upper limit for x-axis of graph, number as string.
;
;	YMIN    - String; Lower limit for y-axis of graph, number as string.
;
;	YMAX    - String; Upper limit for y-axis of graph, number as string.
;
;	LFSEL	- Integer; 0 - No minimax fitting was selected 
;			   1 - Mimimax fitting was selected
;
;	LOSEL   - Integer; 0 - No interpolated values for spline fit. 
;			   1 - Intepolated values for spline fit.
;
;	NMX	- Integer; Number of temperatures used for minimax fit
;
;	NENER   - Integer; Number of user entered temperature values
;
;	NPSPL	- Integer; Number of interpolated values for spline.
;
;	HRDOUT - Integer; 1 if hardcopy output activated, 0 if not.
;
;	HARDNAME- String; Filename for harcopy output.
;
;	DEVICE	- String; IDL name of hardcopy output device.
;
;	HEADER	- String; ADAS version number header to include in graph.
;
;       BITFILE - String; the path to the dirctory containing bitmaps
;                 for the 'escape to series menu' button.
;
; OPTIONAL INPUTS:
;	None.
;
; OUTPUTS:
;       GOMENU  - Int; flag - set to 1 if user has selected 'escape direct
;                 to series menu , otherwise 0
;
; OPTIONAL OUTPUTS:
;	None.
;
; KEYWORD PARAMETERS:
;	FONT	- String; The name of a font to use for text in the
;		  graphical output widget.
;
; CALLS:
;	CW_ADAS_GRAPH	Graphical output widget.
;	PLOT214		Make one plot to an output device for 214.
;	XMANAGER
;
; SIDE EFFECTS:
;	This routine uses a common block to maintain its state PLOT214_BLK.
;
;	One other routine is included in this file;
;	ADAS214_PLOT_EV	Called via XMANAGER during widget management.
;
; CATEGORY:
;	Adas system.
;
; WRITTEN:
;       Stuart Loch, University of Strathclyde, 1998.
;	  Based on adas201_plot.pro v1.4 .
;
; MODIFIED:
;	1.1	Stuart Loch
;		First release
;
; VERSION:
;	1.1	18-06-98
;
; VERSION:
;	1.2	Stuart Loch			26-11-98
;		Code altered to send sum2 to plot214 so that
;		line of sight escape factor is plotted.
;-
;----------------------------------------------------------------------------

PRO adas214_plot_ev, event

  COMMON plot214_blk, data, action, win, plotdev, $
		      plotfile, fileopen, gomenu


  newplot = 0
  print = 0
  done = 0
		;****************************************
		;**** Set graph and device requested ****
		;****************************************
  CASE event.action OF

	'print'	   : begin
			newplot = 1
			print = 1
		     end

	'done'	   : begin
			if fileopen eq 1 then begin
			  set_plot,plotdev
			  device,/close_file
			end
			set_plot,'X'
			widget_control,event.top,/destroy
			done = 1
		     end

        'bitbutton' : begin
            		if fileopen eq 1 then begin
                	  set_plot, plotdev
                	  device, /close_file
            		endif
            		set_plot,'X'
            		widget_control, event.top, /destroy
            		done = 1
            		gomenu = 1
        	      end

  END

		;*******************************
		;**** Make requested plot/s ****
		;*******************************

  if done eq 0 then begin
		;**** Set graphics device ****
    if print eq 1 then begin

      set_plot,plotdev
      if fileopen eq 0 then begin
        fileopen = 1
        device,filename=plotfile
	device,/landscape
      end

    end else begin

      set_plot,'X'
      wset,win
  
    end

		;**** Draw graphics ****

  plot214, data.x , data.y, data.nener,  data.nplots, data.title, $
	   data.xtitle, data.ytitle, $
	   data.ldef1, data.xmin, data.xmax, data.ymin, $
	   data.ymax, data.proc_out, data.numet, data.steps, $
	   data.proc_opt , data.modprof, data.optplot,data.x2, data.y2, data.sum2 

	if print eq 1 then begin
	  message = 'Plot  written to print file.'
	  grval = {WIN:0, MESSAGE:message}
	  widget_control,event.id,set_value=grval
	end


  end

END

;----------------------------------------------------------------------------

PRO adas214_plot,  dsfull, $
		    titlx, titlm, utitle, date, $
		    temp, rate, ratio, densarr, $
                    ldef1, xmin, xmax, ymin, ymax, $
                    nener,  modprof, optplot, x2, y2,$
                    hrdout, hardname, device, header, $
		    bitfile, proc_out, numet, steps, $
		    proc_opt, gomenu, sum2, FONT=font

  COMMON plot214_blk, data, action, win, plotdev, $
		      plotfile, fileopen, gomenucom

		;**** Copy input values to common ****
  plotdev = device
  plotfile = hardname
  fileopen = 0
  gomenucom = gomenu


		;************************************
		;**** Create general graph title ****
		;************************************
  title = strarr(5)

  title(0) = 'TITLE    '
  if ( strtrim(strcompress(utitle),2)  ne ' ' ) then begin
     title(0) = title(0) + ': ' + strupcase(strtrim(utitle,2))
  endif
  title(1) = 'ADAS    :' + strupcase(header)
  title(2) = 'FILE     :' + $
		strcompress((titlx))
  if proc_out.grpsel eq 0 then title(3) = 'KEY     : (SOLID LINE - POPULATION ESC FACT) (DOTTED LINE -  EMERGENT FLUX ESC FACT)'
  if proc_out.grpsel eq 1 then title(3) = 'KEY     : (TOP LINE - OPTICALLY THIN PROFILE) (LOWER LINES - OPTICALLY THICK PROFILES)'
  if proc_out.grpsel eq 2 then title(3) = 'KEY     : (SOLID LINE - EMERGENT FLUX RATIO)'
     title(4) = ' ' 
  
		;*************************************
		;***   Set up Y data for plots     ***
		;*** dimension of y axis depends   ***
		;*** upon selected fitting options ***
		;*************************************
   if proc_out.grpsel lt 2 then begin
      ydim = nener
      y=fltarr(ydim)
      y(0:nener-1) = rate
   end else begin
;      y=fltarr(numet, steps)
;      y(0:numet-1, 0:steps-1) = ratio
       y=ratio
   endelse

  if (proc_out.grpsel eq 0) then ytitle = "ESCAPE FACTOR"
  if (proc_out.grpsel eq 1) then ytitle = "LINE INTENSITY"
  if (proc_out.grpsel eq 2) then ytitle = "EMERGENT FLUX LINE RATIO"

		;**************************************
		;**** Set up x axis and xaxis title ***
		;**************************************
  if proc_out.grpsel lt 2 then begin
     xdim = nener
     x = fltarr(xdim)

		;*** write x axis array ***
     x(0:nener-1) = temp
     nplots = 1
  end else begin
     xdim = steps
     x=fltarr(steps)
     x(0:steps-1) = densarr*proc_opt.len
		;**** x is now column density
     nplots=numet
  endelse

  if (proc_out.grpsel eq 0) then xtitle = "OPTICAL DEPTH"
  if (proc_out.grpsel eq 1) then xtitle = "WAVELENGTH (pm)"
  if (proc_out.grpsel eq 2) then xtitle = "COLUMN DENSITY (cm^-2)"

  		;******************************************
		;*** if desired set up axis scales      ***
		;******************************************
  if (ldef1 eq 0) then begin
      xmin = min(x(*)) 	; data has to be monotonically increasing.
      xmin = xmin * 0.7
      xmax = max(x) * 1.3
;      print,'x=',x
;      print,'xmin=',xmin
;      print,'xmax=',xmax

      ymin = min(y(*,*)); data has to be monotonically increasing.
      ymin = ymin * 0.9
      ymax = max(y(*,*)) * 1.1
;      print,'y=',y
;      print,'ymin=',ymin
;      print,'ymax=',ymax
  endif

		;*************************************
		;**** Create graph display widget ****
		;*************************************
  graphid = widget_base(TITLE='ADAS214 GRAPHICAL OUTPUT', $
					XOFFSET=50,YOFFSET=0)
  bitval = bitfile + '/menu.bmp'
  cwid = cw_adas_graph(graphid,print=hrdout,FONT=font, bitbutton = bitval)

                ;**** Realize the new widget ****
  widget_control,graphid,/realize

		;**** Get the id of the graphics area ****
  widget_control,cwid,get_value=grval
  win = grval.win

		;*******************************************
		;**** Put the graphing data into common ****
		;*******************************************
  data = {    	y:y,	x:x,   nener:nener,     $
	        nplots:nplots, title:title,  xtitle:xtitle, ytitle:ytitle, $
                ldef1:ldef1, $
		xmin:xmin, xmax:xmax, ymin:ymin, ymax:ymax, $
          	proc_out:proc_out, numet:numet, steps:steps, $
		proc_opt:proc_opt, modprof:modprof, $
		optplot:optplot, x2:x2, y2:y2, sum2:sum2}
 
  wset,win

  
  if (proc_out.grpsel lt 2) then begin
     steps=0
     plot214, x , y, nener,  nplots, title, xtitle, ytitle, $
   	      ldef1, xmin, xmax, ymin, ymax, proc_out , numet,steps, $
	      proc_opt, modprof, optplot, x2, y2, sum2
  endif else begin
     plot214, x, y, nener, nplots, title, xtitle, ytitle, $
	      ldef1, xmin, xmax, ymin, ymax, proc_out, numet, steps, $
	      proc_opt, modprof, optplot, x2, y2, sum2
  endelse


		;***************************
                ;**** make widget modal ****
		;***************************
  xmanager,'adas214_plot',graphid,event_handler='adas214_plot_ev', $
                                        /modal,/just_reg

  gomenu = gomenucom

END
