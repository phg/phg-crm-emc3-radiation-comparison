; Copyright (c) 1998, Strathclyde University
; SCCS info: Module @(#)$Header: /home/adascvs/idl/adas2xx/adas214/beispf.pro,v 1.2 2004/07/06 11:38:16 whitefor Exp $ Date $Date: 2004/07/06 11:38:16 $
;+
;
; PROJECT:
;         ADAS IBM MVS to UNIX conversion and development
;
; NAME:
;         ADAS214
;
; PURPOSE:
;         IDL interface and communications with ADAS214 FORTRAN
;         process via pipe.
;
; EXPLANATION:
;         This procedure does some preliminary independent checking of
;         array dimension and gathering of data from the input dataset.
;         It then reads information from the ADAS214 FORTRAN process 
;         via a bi-directional UNIX pipe. Then, the IDL user interface
;         adas214_proc.pro s invoked to determine how the user wishes to
;         process the input dataset. When the user's selections are 
;         complete the procedure processes them and establishes the values
;         of any outstanding arrays/variables. The information is then 
;         written back to the FORTRAN process via the pipe. Communications
;         are to the FORTRAN subroutine BEISPF.
;
; USE:
;         ADAS214 specific, see adas214.pro
;
; INPUTS:
;         PIPE     - The IDL unit number of the bi-directional UNIX pipe
;                    to the ADAS214 FORTRAN process.
;
;         DSNAME   - String; The full system filename of the data 
;                    collection selected by the user for processing.
;
;         LEVELS   - Strarr(njlevx); Contains the original index, level
;                    configuration, Total spin/orbital angular momentum
;                    quantum numbers and ionisation potentials of the
;                    levels for which data is available in the input
;                    dataset.
;
;         ION      - String; The element and ion charge.
;
;         NCLC     - Integer; The elemental nuclear charge
;
;         IZ0      - String; The ion charge+1
;
;         PRNTS    - Strarr(njlevx); Contains the original index, term
;                    configuration, Total spin/orbital angular momentum
;                    quantum numbers and energies for the recombining ion
;                    parent metastables for which Dielectronic Recombination
;                    data is included in the dataset.
;
; OPTIONAL INPUTS:
;         None
;
; OUTPUTS:
;         LPEND    - Integer; 0 if the user pressed the 'Done' button
;                    or 1 if the user pressed 'Cancel'. Maps onto the 
;                    logical variable LPEND in the ADAS214 FORTRAN via 
;                    the integer ILOGIC.
;
;         GP_COUNT - Integer; The number of groupings selected by the user.
;                    maps directly onto NGP in the ADAS214 FORTRAN.
;
;         NJPRTX   - Integer; The number of J-resolved parent metastables.
;
;         NBPRTX   - Integer; The number of bundled parent metastables.
;
;         NJLEVX   - Integer; The number of J-resolved levels.
;
;         NBLEVX   - Integer; The number of bundled levels.
;
;         INDJP    - Integer; The index of the J-resolved parent.
;
;         BNDPR    - Intarr(njlevx); The parent bundling vector containing 
;                    the user selected group numbers and zero's for the
;                    unselected parents.
;
;         CNJP     - Strarr(njprtx); Contains the original parent
;                    parent metastable term configurations.
;
;         BNDLLS   - Intarr(njlevx); The level bundling vector containing
;                    the user selected group numbers and zero's for the
;                    unselected levels.
;
; OPTIONAL OUTPUTS:
;         None
;
; KEYWORD PARAMETERS:
;         FONT - The name of the font to be used on the widget.
;
; CALLS:
;         ADAS214_PROC   Invoke the IDL interface for ADAS214 data
;                        processing options/input.
;
; SIDE EFFECTS:
;         Bi-directional communication with ADAS214 FORTRAN 
;         via a UNIX pipe.
;
; CATEGORY:
;         ADAS system
;
; WRITTEN:
;       Stuart Loch, Univ.of Strathclyde, 	08-12-97.
;
; MODIFIED:
;	  1.1 Stuart Loch
;		First Release.
;	  1.2 Richard Martin
;		Corrected densarr dimensioning error.
;
; VERSION:
;	  1.1 18-06-98
;	  1.2 22-05-2002
;
;---------------------------------------------------------------------

pro beispf, pipe, value, setting, lpend, dsname, gomenu, bitfile, action,  FONT=font

    IF NOT (KEYWORD_SET(font)) THEN font = ''

           ;***************************************;
           ;********Declare some variables ********;
           ;***************************************;

    check    = string(5)
    elhead   = string(40)
    head1 = strarr(150)
    T_levels = elhead
    string = string(15)
    scr = string(100)
    check1 = string(5)
    check2 = '   -1'
 
           ;***************************************;
           ;open data set to check number of levels;
           ;***************************************;

    get_lun, unit
    openr, unit, dsname
    njlevx = 0
    repeat begin
        readf, unit, format='(1a100)', scr
        reads, scr, format='(1a5,95x)', check1
        head1(njlevx) = scr
        njlevx = njlevx+1
    endrep until check1 eq check2 
    free_lun, unit
    njlevx = njlevx-2        ; compensate for repeat loop reading then checking

           ;***************************************;
           ;* number established so create array  *;
           ;***************************************;

    levels  = strarr(njlevx)
    for i = 0, njlevx-1 do begin
        reads, head1(i+1), format = '(1a60)', T_levels
        levels(i) = T_levels + '   '
    endfor

           ;***************************************;
           ;read in ion charge & parent metastables;
           ;***************************************;

    iont = string(5)
    nclct = string(10)
    met1t = string(12) 
    met2t = met1t & met3t = met1t & met4t = met1t
    reads, head1(0), format='(1a5,5x,i5,1a10,75x)', iont, nclct, iz0t
    ion = iont
    nclc = nclct
    iz0 = iz0t

           ;***************************************;
           ;**** call widget creation function ****;
           ;***************************************;

    bndlLS = fltarr(njlevx)
    pmdflg = 1
  
           ;***************************************;
           ;*** create structure for processing ***;
           ;***************************************;

    if value.filechange eq 1 then begin
        value = { new	       : value.new  	,	$
		  dsname       : dsname   	,		$
		  filechange   : value.filechange,        $
              njlevx	   : njlevx 	  ,	  	$
              levels	   : levels 	  ,	  	$
              levels2	   : levels 	  ,	  	$
		  bndlLS       : bndlLS   	,		$
              ion 	   : ion		  ,	  	$
              nclc	   : nclc		  ,	  	$
              pmdflg	   : pmdflg 	  ,	  	$
              toggle_geo   : value.toggle_geo ,   	$
              toggle_den   : value.toggle_den ,   	$
              den 	   : value.den	  ,	  	$
              tempti	   : value.tempti   ,	  	$
              tempte	   : value.tempte   ,	  	$
              len 	   : value.len	  ,	  	$
              mm  	   : value.mm	  ,	  	$
              profile	   : value.profile  ,	  	$
              dmult	   : value.dmult    ,	  	$
              zlen	   : value.zlen	  ,	  	$
		  scanopt      : value.scanopt  ,		$
		  steps	       : value.steps  	,	$
		  minst	       : value.minst	,	$
		  maxst	       : value.maxst    ,	$
		  lev 	       : value.lev    }	
    endif else begin
        value = { new	       : value.new  	,	$
		  dsname       : dsname   	,		$
		  filechange   : value.filechange,        $
              njlevx       : njlevx   	,		$
              levels       : levels   	,		$
              levels2      : levels   	,		$
		  bndlLS       : value.bndlLS  	,	$
              ion 	   : ion		  ,		$
              nclc	   : nclc		  ,		$
              pmdflg	   : pmdflg 	  ,		$
              toggle_geo   : value.toggle_geo , 	$
              toggle_den   : value.toggle_den , 	$
              den 	   : value.den	  ,		$
              tempti	   : value.tempti   ,		$
              tempte	   : value.tempte   ,		$
              len 	   : value.len	  ,		$
              mm  	   : value.mm	  ,		$
              profile	   : value.profile  ,		$
              dmult	   : value.dmult    ,		$
              zlen	   : value.zlen	  ,		$
		  scanopt      : value.scanopt  ,		$
		  steps	       : value.steps  	,	$
		  minst	       : value.minst	,	$
		  maxst	       : value.maxst    ,	$
		  lev 	       : value.lev    }	
    endelse

    adas214_proc, njlevx,  bitfile, value, action, FONT=font	


           ;***************************************;
           ;*** copy values back from widget    ***;
           ;***************************************;

	   ;****calculates the number of transitions selected 
    numet=0
    for i = 0, 9 do begin
	if value.lev(0,i) ne 0.00000 then numet=numet+1
    endfor

	   ;****ensure steps is even
  if value.scanopt eq 0 then begin
    if (value.steps mod 2) ne 0 then begin
       value.steps=value.steps+1
    endif

    densarr=fltarr(value.steps+1)
    maxden=value.den*value.maxst
    minden=value.den*value.minst
    width=(maxden-minden)/value.steps
    for i=0,value.steps-1 do begin
	densarr(i)=minden+width*i
      setting.densarr(i)=densarr(i)
    endfor
  endif


           ;***************************************;
           ;***** process cancel/done options *****;           
           ;***************************************; 
    if action eq 'Done' then begin
    	  value.filechange=0
        lpend = 0
        printf, pipe, format='(i1)', lpend
    endif else begin
        lpend = 1
        printf, pipe, format='(i1)', lpend
        goto, labelend
    endelse

           ;***************************************;
           ;******* fire selections to f77  *******;
           ;***************************************;

    printf, pipe, value.mm
    printf, pipe, value.njlevx
    printf, pipe, value.toggle_geo
    printf, pipe, value.toggle_den
    printf, pipe, value.tempti
    printf, pipe, value.tempte
    printf, pipe, value.den
    printf, pipe, value.len
    printf, pipe, value.profile
    printf, pipe, value.dmult
    printf, pipe, value.zlen   


    for i = 0, (njlevx-1) do begin
        printf, pipe, value.bndlLS(i)
    endfor

    printf, pipe, numet
    for i=0,1 do begin
	for j=0,(numet-1) do begin
	    printf, pipe, value.lev(i,j)
	endfor
    endfor
    
    printf, pipe, value.scanopt
 if (value.scanopt eq 0) then begin
    printf, pipe, value.steps
    printf, pipe, value.minst
    printf, pipe, value.maxst
    for i=0,(value.steps-1) do begin
	printf, pipe, densarr(i)
    endfor
 endif

labelend:

END
