; Copyright (c) 1998, Strathclyde University.
; SCCS info: Module @(#)$Header: /home/adascvs/idl/adas2xx/adas214/cw_adas214_proc.pro,v 1.4 2004/07/06 12:35:32 whitefor Exp $ Date $Date: 2004/07/06 12:35:32 $
;+
; 
; PROJECT:
;         UNIX IDL-ADAS development
;
; NAME:
;         CW_ADAS214_PROC
;
; PURPOSE:
;         Produces a widget for ADAS214 processing options.
;
; EXPLANATION:
;	  This function declares a compound widget consisting of a processing
;         options widget cw_bndl_gen.pro. It also includes two buttons
;         for browsing the input data sets, some information about the first
;         dataset, a 'Cancel' button and a 'Done' button.
; USE:    
;         ADAS214 Specific.
;
; INPUTS: 
;	PARENT	- Long integer; ID of parent widget.
;
;	NJLEVX2	- The number of levels in the 2nd dataset.
;
; OPTIONAL INPUTS:
;       None
;
; OUTPUTS:
;       The return value of this function is the ID of the declared widget.
;
; OPTIONAL OUTPUTS:
;       None        
;
; KEYWORD PARAMETERS:
;       FONT   - The name of the font to be used on the widgets.
;
;       VALUE  - A structure which determines the initial settings of the
;                processing options widget.
;                The default value is;
;                value = { new		    : 0		       ,$
;			   dsname           : ''               ,$
;			   filechange       : 0                ,$
;                          njlevx           : 0                ,$
;                          levels           : mult             ,$
;                          levels2          : mult             ,$
;                          bndlLS           : mult             ,$
;                          ion              : 0                ,$
;                          nclc             : 0                ,$
;                          toggle_geo       : 0                ,$
;                          toggle_den       : 0                ,$
;                          den              : 0                ,$
;                          tempti           : 0                ,$
;                          tempte           : 0                ,$
;                          len              : 0                ,$
;                          mm               : 0                ,$
;                          profile          : 0                ,$
;                          dmult            : 0                ,$
;                          zlen             : 0  	       ,$
;		           scanopt    	    : 0                ,$
;		           steps	    : 0                ,$
;	      	           minst	    : 0                ,$
;	     	           maxst	    : 0                ,$
;			   lev		    : mult } 
;                where mult = make_array( njlevx2, /float, value = -1.0)
;
;                See baispf.pro for an explanation of the tags in this
;                structure.
;
; CALLS:
;         CW_ADAS214 
;              Called indirectly during widget management to handle the widget 
;              events. Procedure is included in this file.
;         CW_ADAS214
;              Widget management routine included inthis file.
;         CW_ADAS_DSBR
;              Data set browse button function.
;         CW_BNDL214_GEN
;              Selection widget
;
; SIDE EFFECTS:
;         None
;
; CATEGORY:
;         Compound Widget for ADAS214
;
; WRITTEN:
;         Stuart D. Loch, Univ.of Strathclyde, 15-Dec-1997
;
; MODIFIED:
;	1.1	Stuart D. Loch 
;		First version
;
; VERSION:
; 	1.1	18-06-98
;
; VERSION:
; 	1.2	MODIFIED: STUART LOCH		19-02-99
;		Put in a check to make sure that the minimum factor on the
;		scanning range was less than the maximum factor.
;		Also, no_release keyword added to button group for profile selector
;		(cw_bgroup), so that deselecting a button is not counted as an event.
;
; VERSION: 
;	1.3	MODIFIED: Richard Martin		16-11-2000
;		Corrected misplaced "end" corresponding to "state.did:begin" in 
;		cw_adas214_proc_event (shown up by IDL 5.4).
;	1.4	MODIFIED: Richard Martin		22-05-2002
;		Entered check to make sure nsteps le 100.
;-
;--------------------------------------------------------------------------

FUNCTION cw_adas214_proc_get_value, id

	;**** Return to caller. ****

    ON_ERROR, 2

	;**** Retrieve the structure from the child ****
        ;**** that contains the sub ids.            ****

    stash = WIDGET_INFO(id, /CHILD)
    WIDGET_CONTROL, stash, GET_UVALUE=state, /NO_COPY

    widget_control, state.stid,  get_value=temp

    bndlLS=strtrim(temp,2)
    bndlLS=float(bndlLS)
    state.ps.bndlLS=bndlLS
 	    
    widget_control, state.atm,get_value=dummy
    mm = strtrim(dummy(0),2)
    mm=float(mm)
    state.ps.mm=mm

    widget_control, state.lid,get_value=dummy1  	    
    toggle_geo=dummy1 
    state.ps.toggle_geo=toggle_geo
      	   
    widget_control, state.hoid,get_value=dummy2 
    toggle_den=dummy2 
    state.ps.toggle_den=toggle_den
            
    widget_control, state.prof, get_value=dummy2b
    profile=dummy2b
    state.ps.profile=profile

    widget_control, state.density, get_value=dummy3		   
    den=strtrim(dummy3(0),2)
    den=float(den)
    state.ps.den=den

    widget_control, state.length, get_value=dummy5
    len=strtrim(dummy5(0),2)
    len=float(len)
    state.ps.len=len

    widget_control, state.scan, get_value=dummy7
    scanopt=dummy7
    state.ps.scanopt=scanopt

    widget_control, state.scan2, get_value=dummy8
    steps=strtrim(dummy8(0),2)
    steps=float(steps)
    state.ps.steps=steps

    widget_control, state.scan3, get_value=dummy9
    minst=strtrim(dummy9(0),2)
    minst=float(minst)
    state.ps.minst=minst

    widget_control, state.scan4, get_value=dummy10
    maxst=strtrim(dummy10(0),2)
    maxst=float(maxst)
    state.ps.maxst=maxst

    widget_control, state.ostabid, get_value=dummy11
    lev=strtrim(dummy11.value,2)
    lev=float(lev)
    state.ps.lev=lev

    widget_control, state.tempion, get_value=dummy4
    tempti=strtrim(dummy4(0),2)
    tempti=float(tempti)
    state.ps.tempti=tempti
     

    widget_control, state.tempel, get_value=dummy4b
    tempte=strtrim(dummy4b(0),2)
    tempte=float(tempte)
    state.ps.tempte=tempte
       
    widget_control, state.zlength, get_value=dummy6
    zlen=strtrim(dummy6(0),2)
    zlen=float(zlen)
    state.ps.zlen=zlen

        ;**** copy the values to the structure ****


       ps = { new	       : 0           	     ,			$
	      dsname           : state.ps.dsname     ,			$
	      filechange       : state.ps.filechange ,			$
              njlevx           : state.ps.njlevx     ,			$
              levels           : state.ps.levels     ,			$
              levels2          : state.ps.levels2    ,			$
              bndlLS           : state.ps.bndlLS     ,		        $
              ion              : state.ps.ion        ,			$
              nclc             : state.ps.nclc       ,			$
              pmdflg           : state.ps.pmdflg     , 			$
              toggle_geo       : state.ps.toggle_geo ,                  $
              toggle_den       : state.ps.toggle_den ,                  $
              den              : state.ps.den        ,                  $
              tempti           : state.ps.tempti     ,                  $
              tempte           : state.ps.tempte     ,                  $
              len              : state.ps.len        ,                  $
              mm               : state.ps.mm         ,                  $
              profile          : state.ps.profile    ,                  $
              dmult            : state.ps.dmult      ,                  $
              zlen             : state.ps.zlen 	     ,			$
	      scanopt	       : state.ps.scanopt    ,			$
	      steps	       : state.ps.steps      ,			$
	      minst	       : state.ps.minst	     ,			$
	      maxst	       : state.ps.maxst      ,			$
	      lev	       : state.ps.lev} 


	;**** Restore the state. ****

    WIDGET_CONTROL, stash, SET_UVALUE=state, /NO_COPY

        ;**** Return the value here. ****

    RETURN, ps

END

;-----------------------------------------------------------------------------

FUNCTION cw_adas214_proc_event, ev

    parent=ev.handler

	;**** Retrieve the structure from the child ****
        ;**** that contains the sub ids.            ****

    stash = WIDGET_INFO(parent, /CHILD)
    WIDGET_CONTROL, stash, GET_UVALUE=state, /NO_COPY

        ;**** process the events ****


    case ev.id of
        
        ;**** select Atomic Mass Number****
 
        state.atm:begin
           widget_control, state.atm,get_value=dummy
	   widget_control, state.scan, get_value=dummy7
           if dummy7 eq 0 then begin
	      widget_control, /input_focus, state.scan2
           endif else begin
              widget_control, /input_focus, state.density
	   endelse
        end
        
        ;**** select plasma geometry****
        
        state.lid:begin
           widget_control, state.lid,get_value=dummy1
        end

        ;**** plasma density shape selector button****
        
        state.hoid:begin
           widget_control, state.hoid,get_value=dummy2
        end
       
        ;**** Line profile  selector ****
        
        state.prof:begin
           widget_control, state.prof, get_value=dummy2b
           if dummy2b gt 2 then begin
		trial={dpvalue:state.dmult, toggled:0}
		dmult=popin214( title='Line Width',value=trial)   
	        state.ps.dmult=dmult.dpvalue
  	   endif
        end
        
        
        ;**** Density text input ****
        
        state.density:begin
            widget_control, state.density, get_value=dummy3
            widget_control, /input_focus, state.length             
        end
        
        ;**** Length text input ****
        
        state.length:begin
            widget_control, state.length, get_value=dummy5
            widget_control, /input_focus, state.tempion
        end          
        
	;**** Scan option input ****

	state.scan:begin
	    widget_control, state.scan, get_value=dummy7
	    if (dummy7 eq 0) then begin
		widget_control, state.scan_row2, sensitive=1
		widget_control, state.scan_row3, sensitive=1
	    endif
	    if (dummy7 eq 1) then begin
		widget_control, state.scan_row2, sensitive=0
		widget_control, state.scan_row3, sensitive=0
	    endif
	end

        ;**** Ion Temperature text input ****
        
        state.tempion:begin
            widget_control, state.tempion, get_value=dummy4
            widget_control, /input_focus, state.tempel         
        end 
         
        
        ;**** Boltzmann Electron Temperature text input ****
        
        state.tempel:begin
            widget_control, state.tempel, get_value=dummy4b
            widget_control, /input_focus, state.zlength         
        end 


        ;**** Length along plasma to be integrated over input ****
        
        state.zlength:begin
            widget_control, state.zlength, get_value=dummy6
            widget_control, /input_focus, state.atm
        end          
        
        ;**** Scanning options ****          
	
        state.scan2:begin 
            widget_control, state.scan2, get_value=dummy8
            widget_control, /input_focus, state.scan3
        end
  
        state.scan3:begin
	    widget_control, state.scan3, get_value=dummy9
	    widget_control, /input_focus, state.scan4
        end

        state.scan4:begin
	    widget_control, state.scan4, get_value=dummy10
	    widget_control, /input_focus, state.density
        end

        ;**** cancel button ****

        state.cid:begin
             new_event = {ID:parent, TOP:ev.top, HANDLER:0L, ACTION:'Cancel'}
        end

        ;**** done button ****
        
        state.did:begin

	    check = 0
	    error=0

	    if check eq 0 then begin

		if error eq 0 then begin
            	widget_control, state.atm, get_value=dummy
            	error = num_chk(dummy(0),sign=1)
           		if (error eq 1) then message='*** Invalid atomic mass number. ***'
      	endif 
 
                      
		if error eq 0 then begin
            	widget_control, state.density, get_value=dummy3
            	error = num_chk(dummy3(0),sign=1)
            	if (error eq 1) then message='*** Invalid plasma density ***'
      	endif 


		if error eq 0 then begin
            	widget_control, state.length, get_value=dummy5
            	error = num_chk(dummy5(0),sign=1)
            	if (error eq 1) then message='*** Invalid plasma length ***'
      	endif 
      
		widget_control, state.scan, get_value=dummy7
		scanopt=dummy7
		if (scanopt eq 0 AND error eq 0) then begin
            	widget_control, state.scan2, get_value=dummy8
            	error = num_chk(dummy8(0),sign=1)
            	if (error eq 1) then begin
				message='*** Invalid number of steps entered ***'
			endif 
			nsteps=strtrim(dummy8(0),2)
			if nsteps gt 100 then begin
				message='*** Number of scan steps must be <= 100 ***'
				error = 1
			endif
      	endif


		if (scanopt eq 0 AND error eq 0) then begin
            	widget_control, state.scan3, get_value=dummy9
            	error = num_chk(dummy9(0),sign=1)
           	 	if (error eq 1) then message='*** Invalid minimum factor entered ***'
      	endif
      

		if (scanopt eq 0 AND error eq 0) then begin
            	widget_control, state.scan4, get_value=dummy10
            	error = num_chk(dummy10(0),sign=1)
            	if (error eq 1) then message='*** Invalid maximum factor entered ***'
      	endif


		if (scanopt eq 0 AND error eq 0) then begin
	    		if (dummy9(0) ge dummy10(0)) then begin
				error=1
				message='*** Minimum factor must be less then maximum factor ***'
	    		endif
        	endif

 
		if error eq 0 then begin
            	widget_control, state.tempion, get_value=dummy4
            	error = num_chk(dummy4(0),sign=1)
            	if (error eq 1) then message='*** Invalid ion temperature ***'
      	endif 
 
		if error eq 0 then begin
            	widget_control, state.tempel, get_value=dummy4b
            	error = num_chk(dummy4b(0),sign=1)
            	if (error eq 1) then message='*** Invalid electron temperature ***'
      	endif 


		if error eq 0 then begin
            	widget_control, state.zlength, get_value=dummy6
            	error = num_chk(dummy6(0),sign=1)
            	if (error eq 1) then message='*** Invalid aspect ratio ***'
      	endif 
          

		;*****************************************
		;**** Check all user input is legal ******
		;*****************************************


		if error eq 0 then begin   
            	widget_control, state.stid, get_value = tmp  
            	for i = 0, n_elements(tmp)-1 do begin 
              		temp2=strtrim(tmp(i),2)                    
		  		if (temp2 ne '') then error=num_chk(temp2,sign=1)
		  		if error gt 0 then message='**Invalid metastable multiplier entered.**'
		  		check=1
      		endfor
      	endif
	   
;		endif


		;**** check that numbers are in accepted range ****

		;**** if error then bring up error message ****

		if error eq 0 then begin	
           		new_event = {ID:parent, TOP:ev.top, HANDLER:0L, ACTION:'Done'}
		endif else begin
			widget_control,state.messid,set_value=message
			new_event=0L
			error=0
			check=0
		endelse

	  endif

	end
	
        ;**** Menu button ****

        state.outid: begin
            new_event = {ID:parent, TOP:ev.top, HANDLER:0L, ACTION:'Menu'}
        end

	ELSE:				;**** Do nothing ****


    ENDCASE

        ;**** Restore the state structure ****

    WIDGET_CONTROL, stash, SET_UVALUE=state, /NO_COPY

    RETURN, new_event

END

;-----------------------------------------------------------------------------

FUNCTION cw_adas214_proc, parent,  njlevx2, bitfile, $
                          VALUE=value, FONT=font


    IF (N_PARAMS() EQ 0) THEN 						$
    MESSAGE, 'Must specify a parent for cw_adas214_proc'

    ON_ERROR, 2					
    
    IF NOT (KEYWORD_SET(font))  THEN font=''
    IF NOT (KEYWORD_SET(value)) then begin
        mult = make_array( njlevx2, /float, value = 0)
        ps = { new		: 0		   ,			$
	       dsname           : ''               ,			$
	       filechange       : 0                ,			$
               njlevx           : 0                ,			$
               levels           : mult             ,			$
               levels2          : mult             ,			$
               bndlLS           : mult             ,			$
               ion              : 0                ,			$
               nclc             : 0                ,			$
               pmdflg           : 0                , 			$
               toggle_geo       : 0                ,                    $
               toggle_den       : 0                ,                    $
               den              : 0.                ,                    $
               tempti           : 0.                ,                    $
               tempte           : 0.                ,                    $
               len              : 0.                ,                    $
               mm               : 0                ,                    $
               profile          : 0                ,                    $
               dmult            : 0.               ,                    $
               zlen             : 0. 		   ,			$
	       scanopt	        : 0                ,			$
	       steps		: 0	           ,			$
	       minst		: 0.          	   ,			$
	       maxst		: 0.                ,			$
	       lev		: intarr(2,10) 	 } 

    ENDIF ELSE begin
        ps = { new		:value.new	   ,			$
	       dsname           : value.dsname     ,			$
	       filechange       : value.filechange ,			$
               njlevx           : value.njlevx     ,			$
               levels           : value.levels     ,			$
               levels2          : value.levels2    ,			$
               bndlLS           : value.bndlLS     ,			$
               ion              : value.ion        ,			$
               nclc             : value.nclc       ,			$
               pmdflg           : value.pmdflg     ,                    $
               toggle_geo       : value.toggle_geo ,                    $
               toggle_den       : value.toggle_den ,                    $
               den              : value.den        ,                    $
               tempti           : value.tempti     ,                    $
               tempte           : value.tempte     ,                    $
               len              : value.len        ,                    $
               mm               : value.mm         ,                    $
               profile          : value.profile    ,                    $
               dmult            : value.dmult      ,                    $
               zlen             : value.zlen  	   ,			$
	       scanopt	        : value.scanopt    ,			$
	       steps		: value.steps      ,			$
	       minst		: value.minst	   ,			$
	       maxst		: value.maxst 	   ,			$
	       lev		: value.lev }	
    ENDELSE

            ;**** define other variables ****

    ion  = strcompress(ps.ion, /remove_all) + '      '
    nclc = strcompress(ps.nclc, /remove_all)

            ;**** Create the base widget ***     

    base = WIDGET_BASE(parent, EVENT_FUNC = "cw_adas214_proc_event",	$
		       FUNC_GET_VALUE = "cw_adas214_proc_get_value", /row)
    col = widget_base(base, /column)
    ion_nam = widget_base(col, /row)
    dat_nam = widget_base(col, /row, /frame)
    atm_row = widget_base(col, /column, /frame)    
    den_row   = widget_base(col, /row, /frame)
    loop_row = widget_base(col, /row, /frame)
    loop1_col = widget_base(loop_row, /row, /frame)
    loop2_col = widget_base(loop_row, /column, /frame)
    var_col = widget_base(col, /row, /frame)
    var2_row = widget_base(var_col, /column, /frame)
    var3_row = widget_base(var_col, /column, /frame)
    tempi_row  = widget_base(var2_row, /column, /frame)
    tempe_row = widget_base(var2_row, /column, /frame)
    zlen_row   = widget_base(var2_row, /column, /frame)
    info1_row = widget_base(col, /row)

		;****set values to strings for text widgets*****

    mm=ps.mm
    mm=strtrim(mm,2)
    den=ps.den
    den=strtrim(den,2)
    tempti=ps.tempti
    tempti=strtrim(tempti,2)
    tempte=ps.tempte
    tempte=strtrim(tempte,2)
    len=ps.len
    len=strtrim(len,2)
    zlen=ps.zlen
    zlen=strtrim(zlen,2)
    steps=ps.steps
    steps=strtrim(steps,2)
    minst=ps.minst
    minst=strtrim(minst,2)
    maxst=ps.maxst
    maxst=strtrim(maxst,2)
    tabos=strarr(2,10)
    met=strarr(njlevx2)
    for i=0, 9 do begin
        if (ps.lev(0,i) ne 0) then begin
           tabos(0,i)="  "+strtrim(string(ps.lev(0,i), $
    				format='(i3)'),2)
	endif else begin
           tabos(0,i)="  "
	endelse
        if (ps.lev(1,i) ne 0) then begin
    	   tabos(1,i)="  "+strtrim(string(ps.lev(1,i),  $
				format='(i3)'),2)
	endif else begin
           tabos(1,i)="  "
	endelse
    endfor
    if ps.filechange eq 0 then begin
       for i=0, njlevx2-1 do begin
           if (ps.bndlLS(i) ne 0) then begin
              met(i)="  "+strtrim(string(ps.bndlLS(i), $
    	   			   format='(f6.3)'),2)
	   endif else begin
	      met(i)=" "
	   endelse
       endfor
    endif else begin
       for i=0,njlevx2-1 do begin
	   met(i)=" "
       endfor
    endelse


	    ;****create Atomic Mass No selector ****

    atm_col = widget_base(atm_row,  /row)	    
    atmlab = widget_label(atm_col, value='Atomic Mass Number:',$
                         font=font)
    atm = widget_text(atm_col, xsize = 20, font=font, $
                      value=mm, /edit)


            ;**** create line profile selector   ****

    prof_col = widget_base(den_row, /column, /frame)
    proflab = widget_label(prof_col,value='Select Line profile:',$
                           font=font)
    selections=['Doppler','Lorentzian','Holtsmark','Double Doppler',$
                'Voigt','Doppler-Holtsmark']
    prof = cw_bgroup(prof_col, selections, set_value=ps.profile,$
                     font=font, /exclusive, /column, /no_release)


           ;****create the plasma density switch ****
    
    den_col = widget_base(den_row,   /column, /frame)    
    hoidlab = widget_label(den_col,value='Select Density distribution:',$
                          font=font)
    selections=['Homogeneous','Linear','Parabolic']
    hoid=cw_bgroup(den_col,selections,set_value=ps.toggle_den, $
                      font=font,/exclusive,/column)
            

  
            ;**** create the slab/cyl. switch ****

    sel_col   = widget_base(den_row,  /column, /frame)
    lidlab = widget_label(sel_col,value='Select Plasma geometry:',font=font)
    selections=['Sphere','Disk','Cylinder']
    lid=cw_bgroup(sel_col,selections,set_value=ps.toggle_geo, $
                      font=font,/exclusive,/column)

	    ;**** looping options selections ****
  
    scan_row = widget_base(loop1_col, /column)
    scan_row1 = widget_base(scan_row, /row)
    scan_row2 = widget_base(scan_row, /row)
    scan_row3= widget_base(scan_row, /row)
    scan1_lab=widget_label(scan_row1,value='Scan?',font=font) 
    selections=['Yes','No']
    scan=cw_bgroup(scan_row1,selections, set_value=ps.scanopt, $
		   font=font, /exclusive, /row)
    scan2_lab=widget_label(scan_row2, value='No of steps',font=font)
    scan2 =widget_text(scan_row2,xsize=4, value=steps, font=font, /edit)
    scan_base=widget_base(scan_row3, /row)
    scan3_lab=widget_label(scan_base, value='Min factor ',font=font)
    scan3=widget_text(scan_base,xsize=4, value=minst, font=font, /edit)
    scan4_lab=widget_label(scan_base, value='Max factor',font=font)
    scan4=widget_text(scan_base,xsize=4, value=maxst, font=font, /edit)
 
            ;**** create density selector   ****
    
    dens_row  = widget_base(loop2_col, /column, /frame)
    dens_col = widget_base(dens_row, /row)
    den_lab = widget_label(dens_col,$
                           value='Density (cm^-3):    ',$
                           font=font)        
    density = widget_text(dens_col,xsize=20, value=den,$
                          font=font, /edit)
           
            ;**** create plasma radius selector ****
  
    len_row   = widget_base(loop2_col, /column, /frame)
    len_col = widget_base(len_row, /row)
    len_lab = widget_label(len_col,  $
                            value='Dimension(b) (cm):  ',$
                            font=font)        
    length = widget_text(len_col,xsize=20, font=font, $
                         value=len, /edit)            
           
            ;**** create ion temperature selector ****
  
    tempi_col = widget_base(tempi_row, /row)
    tempi_lab = widget_label(tempi_col,  $
                            value='Ion Temp (K):      ',$
                            font=font)        
    tempion = widget_text(tempi_col,xsize=20, font=font, $
                          value=tempti, /edit)

                
            ;**** create Boltzmann electron temperature selector ****
  
    tempe_col  = widget_base(tempe_row, /row)
    tempe_lab = widget_label(tempe_col,  $
              value='Boltz. E-Temp (K): ',$
                            font=font)        
    tempel = widget_text(tempe_col,xsize=20, font=font, $
                         value=tempte, /edit)            

                        
            ;**** create length along plasma to be integrated over selector ***

    zlen_col = widget_base(zlen_row, /row)
    zlen_lab = widget_label(zlen_col,  $
                   value='Aspect ratio (a/b):',$
                            font=font)        
    zlength = widget_text(zlen_col,xsize=20, font=font, $
                          value=zlen, /edit)

                        

	    ;**** add transition selector table ****

    osbase=widget_base(var3_row , /column)
    ostabid = cw_adas_table( osbase, tabos, coledit=[1,1],      $
                    colhead=[['Upper','Lower'],   $
                    ['level N','level N']], /scroll, $
                    title='Observed spectrum lines',limits=[2,2],   $
                    fltint=['(i4)','(i4)'],                 $
                    /rowskip, font_small=font_small, ytexsize=y_size)

    ostablab = widget_label(osbase, font=font_small, $
    value="Note: max allowed N quantum no. :") 	;+string(ntot))
    ostablab2 = widget_label(osbase, font=font_small, $
    value= "      min allowed N quantum no. :")	;+string(ngrnd))

            ;**** add dataset name and browse button ****

    rc = cw_adas_dsbr(dat_nam, ps.dsname, nrows = 2, font=font)


            ;**** create a base to select metastables ****

    actbase  = widget_base(base, /column)

            ;**** create switching bases ****

    col2     = widget_base(actbase, /column, /frame)

    col2_row2= widget_base(col2, /row)
    col2_row3= widget_base(col2, /row)
    col4     = widget_base(col2_row3, /column, /scroll,                 $
                           x_scroll_size = 450, y_scroll_size = 650)

            ;**** add information label for level base ****

    infid = widget_label( col2_row2, font=font,              		$
            value = '       Designate metastables and Boltzmann deviations' )


            ;*************************************************
            ;**** attach superstructure bundling widget   ****
            ;*************************************************

    levlab1 = widget_label( col4, $
                 value='Enter a multiplier for each metastable', font=font)
    levlab2 = widget_label(col4, $
                      value='<press return after each entry>', font=font)
    levels=ps.levels
    levels=strtrim(levels,2)
    stid = cw_bndl214_gen( col4, njlevx2, levels, met,$
                        title = ps.dsname, font=font)
            
            
            ;**** create bottom row buttons including  back to menu' ****

    colb=widget_base(col, /row)
    but_row  = widget_base(colb, /row)
    menufile = bitfile + '/menu.bmp'
    read_X11_bitmap, menufile, bitmap1
    outid = widget_button(but_row, value = bitmap1)
    cid = widget_button(but_row, value = 'Cancel', font=font)
    did = widget_button(but_row, value = 'Done', font=font)

	    ;**** widget label for error messages ****

    messid=widget_label(colb, value='')

	    ;**** desensitize scanning options of required ****
    
    if (ps.scanopt eq 1) then begin
	widget_control, scan_row2, sensitive=0
	widget_control, scan_row3, sensitive=0
    endif

            ;**** save out initial state structure ****
    
    state = { LID:lid, STID:stid, OUTID:outid, $
              CID:cid, DID:did, PS:ps,  $
              HOID:hoid, TEMPION:tempion, DENSITY:density, $
              TEMPEL:tempel, LENGTH:length,  ATM:atm,$
              PROF:prof,  DMULT:ps.dmult, ZLENGTH:zlength,$
              SCAN:scan, SCAN2:scan2, SCAN3:scan3,$
	      SCAN4:scan4, SCAN_ROW2:scan_row2, SCAN_ROW3:scan_row3, $
              OSBASE:osbase, OSTABID:ostabid , MESSID:messid}

    WIDGET_CONTROL, WIDGET_INFO(base, /CHILD), SET_UVALUE=state, /NO_COPY
  

    RETURN, base

END
