;----------------------------------------------------------------------
;+
; PROJECT    :  ADAS
;
; NAME       :  run_adas214
;
; PURPOSE    :  Runs ADAS214 opacity code as an IDL subroutine.
;
; ARGUMENTS  :  All output arguments will be defined appropriately.
;
;               NAME        I/O    TYPE   DETAILS
; INPUTS     :  adf04        I     str    specific ion file.
;               mass         I     real   atomic mass number
;               te           I     real   electron temperature (eV)
;               tion         I     real   ion temperature (eV)
;               density      I     real   electron density (cm-3)
;               geometry     I     str    Geometry: 'Sphere' (default)
;                                                   'Disk'/'Disc'
;                                                   'Cylinder'
;               distribution I     str    Dens. dist: 'Homogeneous' (default)
;                                                     'Linear'
;                                                     'Parabolic'
;               profile      I     str    Line profile: 'Doppler' (default)
;                                                       'Lorentzian'
;                                                       'Holtsmark'
;                                                       'Double-Doppler'
;                                                       'Voigt'
;                                                       'Doppler-Holtsmark'
;               dmul_2       I     real   width of second Doppler for
;                                         Double Doppler, Voight and
;                                         Doppler-Holtsmark profiles
;               dens_arr     I     real() density distribution
;               length       I     real   length of plasma along los(b) (cm)
;               aspect       I     real   aspect rato (a/b)
;               boltz_factor I     real() Deviation from Boltzman for each
;                                         level. Ground is set to 1.0 if this
;                                         is not specified.
;               upper_n      I     int()  upper index for transition
;               lower_n      I     int()  lower index for transition
;               log          I     str    name of output text file.
;                                         (defaults to no output).
;               pass         I     str    name of output modified adf04 file.
;               result
;                                       (defaults to no output).
;
; OUTPUTS    :  result       O     struc  optical_depth       :
;                                         pop_escape_factor   :
;                                         flux_escape_factor  :
;                                         trans_absorption    :
;                                         wave                :
;                                         profile_thin        :
;                                         profile_thick       :
;                                         dens_col            :
;                                         ratio               :
;                                         mid_dens            :
;
; KEYWORDS   :  kelvin       I     int    =1 interpret temperatures as Kelvin
;                                         =0 assume eV (default)
;               help                -     if specified this comment
;                                         section is written to screen.
;
;
; NOTES      :  - run_adas214 uses the spawn command. Therefore IDL v5.3
;                 cannot be used. Any other version will work.
;               - The equilibrium metastable split is not returned. Rerun
;                 with one metastable to get these values.
;
;
; WRITTEN:
;       Martin O'Mullane
;
; MODIFIED:
;       1.1     Martin O'Mullane
;                - First release
;       1.2     Martin O'Mullane
;                - Absorption coefficient should not be divided by length.
;
; VERSION:
;       1.1     17-03-2005
;       1.2     20-03-2005
;
;-
;----------------------------------------------------------------------


PRO run_adas214_calc, adf04, n_levels, mass, geometry, d_profile,          $
                      tion, te, dens, length, l_profile, second_doppler,   $
                      aspect,  boltz_factor, upper_n, lower_n,            $
                      is_scan, dens_arr, is_log, log, is_pass, pass,       $
                      od, ef_pop, ef_flux, tran_od, wave,                  $
                      profs_thin, profs_thick, column_dens, ratio


; Now run the ADAS214 code - the input has been checked already.

; Input screen

    fortdir = getenv('ADASFORT')
    spawn, fortdir+'/adas214.out', unit=pipe, /noshell, PID=pid

    date = xxdate()
    printf, pipe, date[0]

    rep = 'NO'
    printf, pipe, rep
    printf, pipe, adf04

    header = 'Run time version of ADAS214.  DATE: ' + date[0] + $
                                          ' TIME: ' + date[1]

; Now move onto processing screen options


    printf, pipe, 0L
    printf, pipe, mass
    printf, pipe, n_levels
    printf, pipe, geometry
    printf, pipe, d_profile
    printf, pipe, tion
    printf, pipe, te
    printf, pipe, dens
    printf, pipe, length
    printf, pipe, l_profile
    printf, pipe, second_doppler
    printf, pipe, aspect

    for j = 0L, n_levels-1 do printf, pipe, boltz_factor[j]

    n_tran = n_elements(upper_n)
    printf, pipe, n_tran
    for j = 0, n_tran-1 do printf, pipe, upper_n[j]
    for j = 0, n_tran-1 do printf, pipe, lower_n[j]

    if is_scan EQ 1 then printf, pipe, 0L $
                    else printf, pipe, 1L       ; Stuart's weird logic
    if is_scan EQ 1 then begin

       xmin    = min(dens_arr, max=xmax)
       n_steps = n_elements(dens_arr)

       printf, pipe, n_steps
       printf, pipe, xmin / dens
       printf, pipe, xmax / dens

       for j = 0L, n_steps-1 do printf, pipe, dens_arr[j]

    endif

    printf, pipe, 1L
    printf, pipe, 0L
    printf, pipe, 0L


; Nearly there - output options. Assume graphs are plotted to get data.

    fdum  = 0.0D
    fdum1 = 0.0D
    fdum2 = 0.0D
    idum  = 0L

    titlx = ' '
    titlm = ' '
    date  = ' '
    nener = 0L
    numet = 0L
    steps = 0L


    ; Normal escape factor and don't write the output files yet

    printf, pipe, 1L
    printf, pipe, 0L
    printf, pipe, 0L

    printf, pipe, 1L  ; force graphics
    printf, pipe, 0L  ; graph type - normal escape factor

    printf, pipe, 0L  ; passing files
    printf, pipe, 0L

    idum = 0
    readf, pipe, idum

    readf, pipe, format = '(a120)' , titlx
    readf, pipe, format = '(a80)' , titlm
    readf, pipe, format = '(a8)' , date
    readf, pipe, nener
    readf, pipe, numet
    readf, pipe, steps

    x2   = fltarr(numet,2)
    y2   = fltarr(numet,2)
    temp = dblarr(nener)
    rate = dblarr(nener)
    sum2 = dblarr(nener)

    for j = 0, nener-1 do begin
       readf, pipe, fdum
       temp[j] = fdum
    endfor
    for j = 0, nener-1 do begin
       readf, pipe, fdum
       rate[j] = fdum
    endfor

    for j = 0, numet-1 do begin
        for i = 0, 1 do begin
          readf, pipe, fdum1
          readf, pipe, fdum2
          x2[j,i] = fdum1
          y2[j,i] = fdum2
        endfor
    endfor
    for i = 0, nener-1 do begin
        readf, pipe, fdum
        sum2[i] = fdum
    endfor

    od       = temp
    ef_pop   = rate
    ef_flux  = sum2
    tran_od  = reform(x2[*,0])

    printf, pipe ,0L    ; back for next graph


    ; Modified profiles and don't write the output files yet

    printf, pipe, 1L
    printf, pipe, 0L
    printf, pipe, 0L

    printf, pipe, 1L  ; force graphics
    printf, pipe, 1L  ; graph type - modified profiles

    printf, pipe, 0L  ; passing files
    printf, pipe, 0L

    idum = 0
    readf, pipe, idum

    readf, pipe, format = '(a120)' , titlx
    readf, pipe, format = '(a80)' , titlm
    readf, pipe, format = '(a8)' , date
    readf, pipe, nener
    readf, pipe, numet
    readf, pipe, steps

    temp    = dblarr(nener)
    rate    = dblarr(nener)
    modprof = fltarr(30,nener)
    optplot = fltarr(10)

    for j = 0, nener-1 do begin
       readf, pipe, fdum
       temp[j] = fdum
    endfor
    for j = 0, nener-1 do begin
       readf, pipe, fdum
       rate[j] = fdum
    endfor

    for i = 0, 29 do begin
        for j = 0, nener-1 do begin
            readf, pipe, fdum
            modprof[i,j] = fdum
        endfor
    endfor


    wave        = temp
    profs_thin  = rate
    profs_thick = modprof

    printf, pipe ,0L    ; back for next graph



    ; Ratio plot and write the output files

    printf, pipe, 1L
    printf, pipe, 0L
    printf, pipe, 0L

    printf, pipe, 1L  ; force graphics
    printf, pipe, 2L  ; graph type - modified profiles

    ; passing files

    if is_log EQ 0 then begin
       printf, pipe, 0L
    endif else begin
       printf, pipe, 1L
       printf, pipe, log
       printf, pipe, header
    endelse
    if is_pass EQ 0 then begin
       printf, pipe, 0L
    endif else begin
       printf, pipe, 1L
       printf, pipe, pass
    endelse

    idum = 0
    readf, pipe, idum


    readf, pipe, format = '(a120)' , titlx
    readf, pipe, format = '(a80)' , titlm
    readf, pipe, format = '(a8)' , date
    readf, pipe, nener
    readf, pipe, numet
    readf, pipe, steps

    temp = dblarr(nener)
    rate = dblarr(nener)
    sum2 = dblarr(nener)

    if is_scan EQ 1 then begin
       ratio   = fltarr(numet,steps)
       densarr = fltarr(steps)
    endif else begin
       ratio   = -1
       densarr = -1
    endelse

    for i = 0, numet-1 do begin
        for j = 0, steps-1 do begin
            readf, pipe, fdum
            ratio[i,j] = fdum
        endfor
    endfor
    for i = 0, steps-1 do begin
        readf,pipe, fdum
        densarr[i] = fdum
    endfor



    column_dens = densarr*length

    printf, pipe ,1L    ; finish

; Terminate FORTRAN process

    close, pipe


END
;----------------------------------------------------------------------------




PRO run_adas214, adf04        = adf04,        $
                 mass         = mass,         $
                 geometry     = geometry,     $
                 distribution = distribution, $
                 profile      = profile,      $
                 dmul_2       = dmul_2,       $
                 te           = te,           $
                 tion         = tion ,        $
                 density      = density,      $
                 dens_arr     = dens_arr,     $
                 length       = length,       $
                 aspect       = aspect,       $
                 boltz_factor = boltz_factor, $
                 upper_n      = upper_n,      $
                 lower_n      = lower_n,      $
                 log          = log,          $
                 pass         = pass,         $
                 result       = result,       $
                 kelvin       = kelvin,       $
                 help         = help



; If asked for help

if keyword_set(help) then begin
   doc_library, 'run_adas214'
   return
endif



; First check that we are not using IDL v5.3

if !version.release EQ '5.3' then begin
   message, 'This version of IDL (v5.3) will not work - ' + $
            'change to v5.0, v5.1, v5.2, v5.4 or later'
   return
endif


; Check that we get all inputs and that they are correct. Otherwise print
; a message and return to command line.

on_error, 2

if n_elements(adf04) EQ 0 then message,'A specific ion file (adf04) is required'


read_adf04, file=adf04, fulldata=all
n_levels = n_elements(all.ia)

if n_elements(mass) EQ 0 then message, 'Supply an atomic mass' $
                         else mass_in = double(mass)

if n_elements(geometry) EQ 0 then str = 'x' $
   else str = strmid(strcompress(strlowcase(geometry),/remove_all),0,1)
case str of
  's'  : geom_in = 0L
  'd'  : geom_in = 1L
  'c'  : geom_in = 2L
  else : begin
            geom_in = 0L
            message, 'A sphere is assumed', /continue
         end
endcase

if n_elements(distribution) EQ 0 then str = 'x' $
   else str = strmid(strcompress(strlowcase(distribution),/remove_all),0,1)
case str of
  'h'  : d_profile = 0L
  'l'  : d_profile = 1L
  'p'  : d_profile = 2L
  else : begin
            d_profile = 0L
            message, 'A homogeneous distribution is assumed', /continue
         end
endcase

second_doppler = 0.0
if n_elements(profile) EQ 0 then str = 'x' $
   else str = strmid(strcompress(strlowcase(profile),/remove_all),0,1)
case str of
  'd'  : l_profile = 0L
  'l'  : l_profile = 1L
  'h'  : l_profile = 2L
  'v'  : begin
            l_profile = 4L
            if n_elements(dmul_2) EQ 0 then               $
                  message, 'Second Doppler width required'$
                  else second_doppler = double(dmul_2)
         end
  else : begin
            l_profile = 0L
            message, 'A Doppler line profile is assumed', /continue
         end
endcase


if n_elements(te) EQ 0 then message, 'Supply an electron temperature' $
                       else te_in = double(te)
if n_elements(tion) EQ 0 then message, 'Supply an ion temperature' $
                         else tion_in = double(tion)
if NOT keyword_set(kelvin) then begin
   tion_in = tion_in * 11605.0
   te_in   = te_in * 11605.0
endif

if n_elements(density) EQ 0 then message, 'Supply an electron density' $
                         else dens_in = double(density)

if n_elements(dens_arr) EQ 0 then begin
   is_scan = 0L
   dens_arr_in = dens_in
endif else begin
   is_scan = 1L
   dens_arr_in = double(dens_arr)
endelse

if n_elements(log) EQ 0 then begin
   log_in = ''
   is_log = 0L
endif else begin
   log_in = log
   is_log = 1L
endelse

if n_elements(pass) EQ 0 then begin
   pass_in = ''
   is_pass = 0L
endif else begin
   pass_in = pass
   is_pass = 1L
endelse


if n_elements(length) EQ 0 then message, 'Supply length (b)' $
                       else length_in = double(length)
if n_elements(aspect) EQ 0 then message, 'Supply aspect ratio (a/b)' $
                       else aspect_in = double(aspect)

b_factor = dblarr(n_levels)

if n_elements(boltz_factor) EQ 0 then begin
   message, 'Assume partition given by population calculation', /continue
   b_factor[0] = dblarr(n_levels)
endif else begin
   num = n_elements(boltz_factor) < n_levels
   b_factor[0:num-1] = double(boltz_factor[0:num-1])
endelse


if n_elements(upper_n) EQ 0 then upper_n_in = -1 else upper_n_in = long(upper_n)
if n_elements(lower_n) EQ 0 then lower_n_in = -1 else lower_n_in = long(lower_n)


run_adas214_calc, adf04, n_levels, mass_in, geom_in, d_profile,               $
                  tion_in, te_in, dens_in, length_in, l_profile,              $
                  second_doppler, aspect_in,  b_factor,                       $
                  upper_n_in, lower_n_in,                                     $
                  is_scan, dens_arr_in, is_log, log_in, is_pass, pass_in,     $
                  od, ef_pop, ef_flux, tran_od, wave,                         $
                  profs_thin, profs_thick, column_dens, ratio


; Output is a structure

if n_elements(dens_arr) EQ 1 then begin
   mid_dens = density
endif else begin
   ind = n_elements(dens_arr)/2
   mid_dens = column_dens[ind] / length
endelse

result = { optical_depth       :  od,           $
           pop_escape_factor   :  ef_pop,       $
           flux_escape_factor  :  ef_flux,      $
           trans_absorption    :  tran_od,      $
           wave                :  wave,         $
           profile_thin        :  profs_thin,   $
           profile_thick       :  profs_thick,  $
           dens_col            :  column_dens,  $
           ratio               :  ratio,        $
           mid_dens            :  mid_dens      }

END
