; Copyright (c) 1998, Strathclyde University
; SCCS info: Module @(#)$Header: /home/adascvs/idl/adas2xx/adas214/adas214.pro,v 1.7 2004/07/06 10:24:49 whitefor Exp $ Date $Date: 2004/07/06 10:24:49 $
;+
; PROJECT:
;       ADAS IBM MVS to UNIX conversion
;
; NAME:
;	ADAS214
;
; PURPOSE:
;	The highest level routine for the ADAS 214 program.
;
; EXPLANATION:
;	This routine is called from the main adas system routine, adas.pro,
;	to start the ADAS 214 application.  Associated with adas214.pro
;	is a FORTRAN executable.  The IDL code provides the user
;	interface and output graphics whilst the FORTRAN code reads
;	in data files, performs numerical processing and creates the
;	output files.  The IDL code communicates with the FORTRAN
;	executable via a bi-directional UNIX pipe.  The unit number
;	used by the IDL for writing to and reading from this pipe is
;	allocated when the FORTRAN executable is 'spawned' (see code
;	below).  Pipe communications in the FORTRAN process are to
;	stdin and stdout, i.e streams 5 and 6.
;
;	The FORTRAN code is an independent process under the UNIX system.
;	The IDL process can only exert control over the FORTRAN in the
;	data which it communicates via the pipe.  The communications
;	between the IDL and FORTRAN must be exactly matched to avoid
;	input conversion errors.  The correct ammounts of data must be
;	passed so that neither process 'hangs' waiting for communications
;	which will never occur.
;
;	The FORTRAN code performs some error checking which is
;	independent of IDL.  In cases of error the FORTRAN may write
;	error messages.  To prevent these error messages from conflicting
;	with the pipe communications all FORTRAN errors are written to
;	output stream 0, which is stderr for UNIX.  These error messages
;	will appear in the window from which the ADAS session/IDL session
;	is being run.
;
;	In the case of severe errors the FORTRAN code may terminate
;	itself prematurely.  In order to detect this, and prevent the
;	IDL program from 'hanging' or crashing, the IDL checks to see
;	if the FORTRAN executable is still an active process before
;	each group of pipe communications.  The process identifier
;	for the FORTRAN process, PID, is recorded when the process is
;	first 'spawned'.  The system is then checked for the presence
;	of the FORTRAN PID.
;
; USE:
;	First the system settings must be established by calling
;	adas_sys_set.pro then adas214.pro is called to start the
;	ADAS 214 application;
;
;	adas_sys_set, adasrel, fortdir, userroot, centroot, $
;		      devlist, devcode, font_large, font_small, edit_fonts
;	adas214,   adasrel, fortdir, userroot, centroot, devlist, $
;		   devcode, font_large, font_small, edit_fonts
;
; INPUTS:
;	ADASREL - A string indicating the ADAS system version, 
;		  e.g ' ADAS RELEASE: ADAS93 V1.1.0'.  The first
;		  character should be a space.
;
;	FORTDIR - A string holding the path to the directory where the
;		  FORTRAN executables are, e.g '/disk/adas/fortran/exec'
;
;	USERROOT - A string holding the path to the root directory of
;		   the user's adas data. e.g '/disk/bowen/adas'
;		   This root directory will be used by adas to construct
;		   other path names.  For example the users default data
;		   for adas214 should be in /disk/bowen/adas/adf04.  In
;		   particular the user's default interface settings will
;		   be stored in the directory USERROOT+'/defaults'.  An
;		   error will occur if the defaults directory does not
;		   exist.
;
;	CENTROOT - Like USERROOT, but this directory points to the
;		   central data area for the system.  User defaults are
;		   not stored on CENTROOT.
;
;	DEVLIST - A string array of hardcopy device names, used for
;		  graphical output. e.g ['Post-Script','HP-PCL','HP-GL']
;		  This array must mirror DEVCODE.  DEVCODE holds the
;		  actual device names used in a SET_PLOT statement.
;
;	DEVCODE - A string array of hardcopy device code names used in
;		  the SET_PLOT statement, e.g ['PS', 'PCL', 'HP']
;		  This array must mirror DEVLIST.
;
;	FONT_LARGE - The name of a larger font e.g 'courier_bold14'
;
;	FONT_SMALL - The name of a smaller font e.g 'courier_bold12'
;
;	EDIT_FONTS - A structure of two fonts used in the table editor
;		     adas_edtab.pro, {font_norm:'',font_input:''}.
;		     The two fonts are used to differentiate between
;		     editable and non-editable parts of the table. You
;		     may have to experiment to get two fonts which give
;		     the same default spacing in the edit widget.
;		     e.g {font_norm:'helvetica_bold14', $
;			  font_input:'helvetica_oblique14'}
;
; OPTIONAL INPUTS:
;	None
;
; OUTPUTS:
;	None - Note this routine should leave all inputs un-modified.
;
; OPTIONAL OUTPUTS:
;	None
;
; KEYWORD PARAMETERS:
;	None
;
; CALLS:
;	FIND_PROCESS	Checks to see if a given process is active.
;	BDSPF0		Pipe comms with FORTRAN B1SPF0 routine.
;	BXSETP		Pipe comms with FORTRAN BXSETP routine.
;	BDISPF		Pipe comms with FORTRAN B1ISPF routine.
;	XXDATE		Get date and time from operating system.
;
; SIDE EFFECTS:
;	This routine spawns a FORTRAN executable.  Note the pipe 
;	communications routines listed above.  In addition to these
;	pipe communications there is one explicit communication of the
;	date to the FORTRAN code, search for 'printf,pipe' to find it.
;
; CATEGORY:
;	Adas system.
;	
; WRITTEN:
;       Stuart Loch  Univ.of Strathclyde, 15-12-97
;       
;
; VERSION:
;       1.1  Stuart D. Loch 				
;		 First version
;	  1.2  Richard Martin
;		 Increased version no. to 1.2
;	  1.3  Richard Martin
;		 Increased version no. to 1.3
;	  1.4  Richard Martin
;		 Increased version no. to 1.4
;	  1.5  Richard Martin
;		 Increased version no. to 1.5
;	  1.6  Richard Martin
;		 Increased version no. to 1.6
;	  1.7  Richard Martin
;		 Increased version no. to 1.7
;
; MODIFIED:
;	1.1	18-06-98
;	1.2	02-12-98
;	1.3	18-03-99
;	1.4	24-09-99
;	1.5	15-11-00
;	1.5	16-07-02
;	1.6	16-07-02
;	1.7	08-09-03
;
;-
;-----------------------------------------------------------------------------


PRO ADAS214,	adasrel, fortdir, userroot, centroot, 			$
		devlist, devcode, font_large, font_small, edit_fonts

		;************************
		;**** Initialisation ****
		;************************

    adasprog = ' PROGRAM: ADAS214 V1.7'
    lpend = 0
    gomenu = 0
    deffile = userroot + '/defaults/adas214_defaults.dat'
    bitfile = centroot + '/bitmaps'
    menu = bitfile + '/menu.bmp'
    device = ''
    date = xxdate()
    header = adasrel+adasprog+' DATE: '+date(0)+' TIME: '+date(1)


    read_X11_bitmap,menu,bitmap1

		;******************************************
		;**** Search for user default settings ****
		;**** If not found create defaults     ****
		;******************************************

    files = findfile(deffile)
    if files(0) eq deffile then begin
        restore,deffile
        inval.centroot = centroot+'/adf04/'
        inval.userroot = userroot+'/adf04/'
    endif else begin
        inval = { 							$
		  ROOTPATH:userroot+'/adf04/', 				$
		  FILE:'', 						$
		  CENTROOT:centroot+'/adf04/', 				$
		  USERROOT:userroot+'/adf04/' }
                  proc_in = {new:-1}
                  proc_out = {new:-1}
		  proc_opt = {new:-1}
    endelse
    IF (proc_out.new lt 0) THEN BEGIN
        proc_out = {NEW:0,   OUTBUT:0,      $
                   GTIT1:'',               $
                   SCALBUT:0,              $
                   XMIN:'', XMAX:'',       $
                   YMIN:'', YMAX:'',       $
                   HRDOUT:0, HARDNAME:'',  $
                   TXTOUT:0, TXTNAME:'',   $
		   TEXOUT:0, $
                   TEXREP:0, TEXDSN:'',   $
                   TEXDEF:userroot+'/pass/paper.txt', $ 
		   TEXMES:'',   $
 		   TXTDFN:userroot+'/pass/adas214.pass', $
                   GRPDEF:userroot+'/pass/adas214.ps', $
                   GRPFMESS:'',            $
                   GRPSEL:-1, GRPRMESS:'', $
                   DEVSEL:-1, GRSELMESS:'', $
                   GRPTMESS:''            $
             }
    ENDIF
    
    IF (proc_opt.new lt 0) THEN BEGIN
        mult = make_array(100, /float, value = 0)
	proc_opt = {	NEW:0		,	$
			DSNAME: ''      ,	$
			FILECHANGE:0    ,	$
                        NJLEVX: 0       ,	$
                        LEVELS: mult    ,	$
                        LEVELS2: mult   , 	$
                        BNDLLS: mult    ,       $
                        ION: 0          ,	$
                        NCLC: 0         ,	$
			PMDFLG:0	,	$
                        TOGGLE_GEO: 0   ,	$
                        TOGGLE_DEN: 0   ,	$
                        DEN: 0.          ,	$
                        TEMPTI: 0.       ,	$
                        TEMPTE: 0.       ,	$
                        LEN: 0.          ,	$
                        MM: 0           ,	$
                        PROFILE: 0      ,	$
                        DMULT: 0.      ,	$
                        ZLEN: 0.  	,	$
			SCANOPT:0	,	$
			STEPS:0		,	$
			MINST:0.		,	$
			MAXST:0.		,	$
			LEV:intarr(2,10)}

    ENDIF

    
    setting = { sum1	:fltarr(35)	,	$
                sum2	:fltarr(35)	,	$
                sum3	:fltarr(35)	,	$
                sum4	:fltarr(35)	,	$
                x0  	:fltarr(35)	,	$
                x1  	:fltarr(35)	,	$
                y	:fltarr(100)	,	$
                densarr :fltarr(100)	,	$
                popar	:fltarr(30)	,	$
                axis	: 1		,	$
		xdata	:fltarr(40)	,	$
		ydata	:fltarr(40)	,	$
                x_title	:''		,	$
		y_title	:''		,	$
		main_title:''		,	$
		adas_info:adasrel+adasprog+' DATE : '+date(0)+'TIME:'+date(1),$
                file_info:'FILE :' 	,	$
		font	: ''		,	$
 		prtbut	: 0		,	$
		cfactor	:1.0}


		
;		*****************************
;               *			    * 
;		*    Start fortran code     *
;               *        		    *
;		*****************************

 
    spawn, fortdir+'/adas214.out', unit=pipe, /noshell, PID=pid

		;************************************************
		;**** Get date and write to fortran via pipe ****
		;************************************************
    printf, pipe, date(0)

LABEL100:
		;**** Check FORTRAN still running ****
  
    if find_process(pid) eq 0 then goto, LABELEND

		;************************************************
		;**** Communicate with bdspf0 in fortran and ****
		;**** invoke user interface widget for       ****
		;**** Data file selection                    ****
		;************************************************

;    bdspf0, pipe, inval, dsfull, rep, proc_opt, FONT=font_large
    bespf0, pipe, inval, dsfull, rep, proc_opt, FONT=font_large

		;**** If cancel selected then end program ****

    if rep eq 'YES' then goto, LABELEND

                ;*******Check FORTRAN still running*************

    if find_process(pid) eq 0 then goto, LABELEND
    setting.file_info = setting.file_info + dsfull

LABEL200:

	        ;**** Check FORTRAN still running ****

    if find_process(pid) eq 0 then goto, LABELEND

		;************************************************
		;**** Communicate with bdispf in fortran and ****
		;**** invoke user interface widget for       ****
		;**** Processing options                     ****
		;************************************************
;    bdispf, pipe, proc_opt, setting, lpend, dsfull, gomenu, bitfile, action, FONT=font_small
    beispf, pipe, proc_opt, setting, lpend, dsfull, gomenu, bitfile, action, FONT=font_small

                ;**** If menu button clicked tell fortran to stop ****

	if action eq 'Menu' then begin
		idone	= 0
		icancel	= 0
		imenu	= 1
		printf, pipe, idone
		printf, pipe, icancel
		printf, pipe, imenu
		goto, LABELEND
        endif
	if action eq 'Cancel' then begin
		idone	= 0
		icancel	= 1
		imenu	= 0
		printf, pipe, idone
		printf, pipe, icancel
		printf, pipe, imenu
                goto, LABEL100
        endif
	if action eq 'Done' then begin
		idone	= 1
		icancel	= 0
		imenu	= 0
		printf, pipe, idone
		printf, pipe, icancel
		printf, pipe, imenu
	endif

LABEL300:
		;**** Check FORTRAN still running ****

    if find_process(pid) eq 0 then goto, LABELEND

    adas214_out, pipe,proc_out, dsfull, devlist, proc_in, bitmap1, FONT=font_large, action, bitfile, proc_opt
		if action eq 'Menu' then begin
			idone	= 0
			icancel	= 0
			imenu	= 1
			printf, pipe, idone
			printf, pipe, icancel
			printf, pipe, imenu
			goto, LABELEND
                endif
		if action eq 'Cancel' then begin
			idone	= 0
			icancel	= 1
			imenu	= 0
			printf, pipe, idone
			printf, pipe, icancel
			printf, pipe, imenu
                        goto, LABEL200
                endif
		if action eq 'Done' then begin
			idone	= 1
			icancel	= 0
			imenu	= 0
			printf, pipe, idone
			printf, pipe, icancel
			printf, pipe, imenu
		endif

		;****output file names to fortran****

		printf, pipe, proc_out.outbut 
		if proc_out.outbut eq 1 then begin
  		   printf, pipe, proc_out.grpsel
		endif
		printf,pipe,proc_out.texout
		if proc_out.texout eq 1 then begin
		   printf, pipe, proc_out.texdsn
		   printf, pipe, header
		endif
		printf,pipe,proc_out.txtout
		if proc_out.txtout eq 1 then begin
		   printf, pipe, proc_out.txtname
		endif

                ;**** Create an information widget ****

  widget_control, /hourglass
  widgetbase = widget_base(/column,xoffset=300,yoffset=200,     $
                           title = "ADAS214 INFORMATION")
  lab0 = widget_label(widgetbase, value='')
  lab1 = widget_label(widgetbase, value='')
  lab2 = widget_label(widgetbase, font=font_large,              $
                      value="       Processing - please wait       ")
  lab3 = widget_label(widgetbase, value='')
  lab4 = widget_label(widgetbase, value='')
  widget_control, widgetbase, /realize

                ;*******Check FORTRAN still running*************

    if find_process(pid) eq 0 then begin
       widget_control, widgetbase, /destroy
       goto, LABELEND
    endif

                ;**** wait for signal from FORTRAN ****
                ;**** before continuing ****

  idum = 0
  readf, pipe, idum
  widget_control, widgetbase, /destroy

                ;*******Check FORTRAN still running*************

    if find_process(pid) eq 0 then goto, LABELEND

		;**** if graphing selected, continue ***

		if proc_out.outbut eq 1 then begin
		;**** user explicit scaling ****
		    grpscal=proc_out.outbut
		    gomenu=0			;????
		    xmin=proc_out.xmin
		    xmax=proc_out.xmax
		    ymin=proc_out.ymin
		    ymax=proc_out.ymax
		;**** Hardcopy output ****
		    hrdout=proc_out.hrdout
		    hardname=proc_out.hardname
                    if proc_out.devsel ge 0 then device=devcode(proc_out.devsel)
		;**** user title for graph ****
		    utitle=proc_out.gtit1

		    ldef1=proc_out.scalbut

		    beoutg, dsfull, pipe,utitle,grpscal,xmin,xmax,ymin,ymax,$
			    hrdout, hardname, ldef1, device, header, bitfile,$
			    proc_out, setting, proc_opt, gomenu, FONT=font

                ;*******Check FORTRAN still running*************

                    if find_process(pid) eq 0 then goto, LABELEND


			if gomenu eq 1 then begin
				imenu	= 1
				printf, pipe, idone
				goto, LABELEND
              		endif else begin
				printf, pipe,0
				goto, LABEL300
			endelse
		endif else begin
		        goto, LABEL300
		endelse
               
LABELEND:

		;**** Save user defaults ****
    save, inval, proc_in, proc_out, proc_opt, filename=deffile


                ;**********************************************
                ;********
END
