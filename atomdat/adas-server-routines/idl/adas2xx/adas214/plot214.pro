; Copyright (c) 1998, Strathclyde University .
; SCCS INFO : Module @(#)$Header: /home/adascvs/idl/adas2xx/adas214/plot214.pro,v 1.2 2004/07/06 14:30:46 whitefor Exp $	Date  $Date: 2004/07/06 14:30:46 $
;
;+
; PROJECT:
;       ADAS IBM MVS to UNIX conversion
;
; NAME:
;	PLOT214
;
; PURPOSE:
;	Plot one graph for ADAS214.
;
; EXPLANATION:
;	This routine plots ADAS214 output for a single graph.
;
; USE:
;	Use is specific to ADAS214.  See adas214_plot.pro for
;	example.
;
; INPUTS:
;	X	- Double array; list of x values.
;
;	Y	- 2D double array; y values, 2nd index ordinary level,
;		  indexed by iord1 and iord2.
;
;	NENER   - Integer : Number of data points and spline fit points
;
;	NMX     - Integer : Number of points used in polynomial fit.
;
;	NPSPL	- Integer : Number of intepolated points for spline fit.
;
;	NPLOTS  - Integer : type of plots  1: Data from file only
;					   3: Data and spline fit
;					   5: Data and minmax fit
;					   7: Data, spline, & minimax fits.
;
;	TITLE	- String array : General title for program run. 5 lines.
;
;	XTITLE  - String : title for x-axis annotation
;
;	YTITLE  - String : title for y-axis annotation
;
;	LDEF1	- Integer; 1 - User specified axis limits to be used, 
;		  	   0 - Default scaling to be used.
;
;	XMIN	- String; Lower limit for x-axis of graph, number as string.
;
;	XMAX	- String; Upper limit for x-axis of graph, number as string.
;
;	YMIN	- String; Lower limit for y-axis of graph, number as string.
;
;	YMAX	- String; Upper limit for y-axis of graph, number as string.
;
; OPTIONAL INPUTS:
;	None.
;
; OUTPUTS:
;	None.
;
; OPTIONAL OUTPUTS:
;	None.
;
; KEYWORD PARAMETERS:
;	None.
;
; CALLS:
;	None.
;
; SIDE EFFECTS:
;	None.
;
; CATEGORY:
;	Adas system.	
;
; WRITTEN:
;       Stuart loch, University of Strathclyde, 1998. 
;
; MODIFIED:
;	1.1	Stuart Loch
;		18-06-98
;
; VERSION:
;	1.1	18-06-98	
;
; VERSION:
;	1.2	Stuart Loch			26-11-98	
;		Plot changed to a log-log scale on the axes
;		for the line profiles.
;		Overplots emergent flux escape factor on population
;		escape factor plot.
;
;----------------------------------------------------------------------------

PRO plot214, x , y, nener, nplots, title, xtitle, ytitle, $
             ldef1, xmin, xmax, ymin, ymax, proc_out, numet, steps, $
	     proc_opt, modprof, optplot, x2, y2, sum2

COMMON Global_lw_data, left, right, top, bottom, grtop, grright

		;****************************************************
		;**** Suitable character size for current device ****
		;**** Aim for 60 characters in y direction.      ****
		;****************************************************
  charsize = (!d.y_vsize/!d.y_ch_size)/60.0 
  small_check = GETENV('VERY_SMALL')
  if small_check eq 'YES' then charsize=charsize*0.8

		;**** set makeplot counter ****

  makeplot = 1

		;**** Construct graph title ****
		;**** "!C" is the new line control. ****
  !p.font=-1
  if small_check eq 'YES' then begin
      gtitle =  title(0) + "!C!C" + title(1) + "!C!C" + title(2) + "!C!C" + $
        title(3) + "!C!C" + title(4)
  endif else begin
      gtitle =  title(0) + "!C!C" + title(1) + "!C" + title(2) + "!C" + $
        title(3) + "!C" + title(4)
  endelse

		;****construct run title and complete level list ****
  if (proc_out.grpsel eq 2) or (proc_out.grpsel eq 0) then begin
    strg1 = '---- Level Assignments ----'
    strg2 = 'INDEX       DESIGNATION       '
    list = '!C!C'+'!C!C'
    list = list +strg1+'!C'+strg2+'!C'
    for ilev = 1, proc_opt.njlevx do begin
  	strg3=string(proc_opt.levels2(ilev-1),format='(A35)')
;        list=list+'!C"
  	list = list+'!C'+strg3
    end
  end

		;*****make label list for ratios ****
  if (proc_out.grpsel eq 2 ) then begin
     labels=strarr(numet)
     for i=0,numet-1 do begin
       uplab=proc_opt.lev(0,i)
       uplab=string(uplab)
       uplab=strtrim(uplab,2)
       lowlab=proc_opt.lev(1,i)
       lowlab=string(lowlab)
       lowlab=strtrim(lowlab,2)
       if (i eq 0) then begin
	   up1=uplab
	   lo1=lowlab
       endif
       labels(i)=uplab+' - '+lowlab+'/'+up1+' - '+lo1
     endfor
  endif

  if (proc_out.grpsel eq 0) then begin
     labels=strarr(numet)
     for i=0,numet-1 do begin
	 uplab=proc_opt.lev(0,i)
	 uplab=string(uplab)
	 uplab=strtrim(uplab,2)
	 lowlab=proc_opt.lev(1,i)
	 lowlab=string(lowlab)
	 lowlab=strtrim(lowlab,2)
         labels(i)=uplab+'-'+lowlab
     endfor
  endif

		;**** Find x and y ranges for auto scaling ****

  if ldef1 eq 0 then begin
		;**** identify values in the valid range ****
		;**** plot routines only work within ****
		;**** single precision limits.	     ****
       xvals = where(x gt 1.0e-37 and x lt 1.0e37)
       yvals = where(y gt 1.0e-37 and y lt 1.0e37)


    if xvals(0) gt -1 then begin
      xmax = max(x(xvals), min=xmin)
      makeplot = 1
    end else begin
      makeplot = 0
    end

    if yvals(0) gt -1 then begin
      ymax = max(y(yvals), min=ymin)
      makeplot = 1
    end else begin
      makeplot = 0
    end

    style = 0
  end else begin

		;Check that at least some data in in axes range ***
    xvals = where(x ge xmin and x le xmax)
    yvals = where(y ge ymin and y le ymax)
    if xvals(0) eq -1 or yvals(0) eq -1 then begin
      makeplot = 0
    endif 
    style = 1
  end


		;**** Set up log-log plotting axes ****
  if (proc_out.grpsel eq 0) then begin
    plot_oo,[xmin,xmax],[ymin,ymax],/nodata,ticklen=1.0, $
		position=[left,bottom,grright,grtop], $
		xtitle=xtitle, ytitle=ytitle, xstyle=style, ystyle=style, $
		charsize=charsize
  endif
  if (proc_out.grpsel eq 1) then begin
    plot,[xmin,xmax],[ymin,ymax],/nodata,ticklen=1.0,/ylog, $
		position=[left,bottom,grright,grtop], $
		xtitle=xtitle, ytitle=ytitle, xstyle=style, ystyle=style, $
		charsize=charsize
  endif
  if (proc_out.grpsel eq 2) then begin
    plot,[xmin,xmax],[ymin,ymax],/nodata,ticklen=1.0, $
		position=[left,bottom,grright,grtop], $
		xtitle=xtitle, ytitle=ytitle, xstyle=style, ystyle=style, $
		charsize=charsize
  endif

  if makeplot eq 1 then begin

		;**********************
		;****  Make plots  ****
		;**********************
;  if (proc_out.grpsel eq 2) then begin
;     print,'numet=',numet
;     print,'steps=',steps
;     print,'x=',x
;     print,'y(0,*)=',y(0,*)
;  endif
;  if (proc_out.grpsel eq 0) then begin
;     print,'optplot=',optplot
;     yh=fltarr(10)
;     xh=fltarr(numet,10)
;     for j=0,numet-1 do begin
;       for i=0,9 do begin
;	   yh(i)=i/9.
;           xh(j,i)=optplot(j)
;       endfor
;     endfor
;     print,'yh=',yh
;     print,'xh=',xh
;   endif
     case proc_out.grpsel of 

	0 : begin
			oplot, x(0:nener-1), y(0:nener-1)
			oplot, x(0:nener-1), sum2(0:nener-1), linestyle=1
			for j=0,numet-1 do begin
			    oplot, x2(j,0:1),y2(j,0:1) 
		 	    xyouts,x2(j,1)*0.7,y2(j,0)*(1.1-j/numet),labels(j)
                            xyouts,grright+0.03, top-0.08,list, /normal, charsize=charsize*0.8
			endfor
            end
	1 : begin
			oplot, x(0:nener-1), y(0:nener-1)
			for i=0,29 do begin
	    		    oplot, x(0:nener-1), modprof(i,0:nener-1)
			endfor
	    end
	2 :  begin
			for i=1,(numet-1) do begin
		    		oplot, x(0:steps-1), y(i,0:steps-1)
  				xyouts,x(steps-1)*0.9,y(i,steps-1),labels(i)
	        	endfor
                 xyouts,grright+0.03, top-0.08,list, /normal, charsize=charsize*0.8
             end
        else : print, "ADAS214 : NO PLOTS"

      endcase

  endif else begin
     print, "ADAS214 : No data found in these axes ranges"
  endelse

		;**** plot title ****
      xyouts, (left-0.05), top, gtitle, /normal, alignment=0.0, $
			charsize=charsize*0.95

END
