; Copyright (c) 1998, Strathclyde University.
; SCCS info: Module @(#)$Header: /home/adascvs/idl/adas2xx/adas214/beoutg.pro,v 1.2 2004/07/06 11:38:28 whitefor Exp $	Date $Date: 2004/07/06 11:38:28 $
;+
; PROJECT:
;       ADAS IBM MVS to UNIX conversion
;
; NAME:
;	BEOUTG
;
; PURPOSE:
;	Communication with ADAS214 FORTRAN via UNIX pipe and
;	graphics output.
;
; EXPLANATION:
;	The routine begins by reading information from the ADAS214
;	FORTRAN process via a UNIX pipe.  Then the IDL graphical
;	output routine for ADAS214 is invoked.  Communications are to
;	the FORTRAN subroutine BEOUTG.
;
; USE:
;	The use of this routine is specific to ADAS201 see adas201.pro.
;
; INPUTS:
;	DSFULL   - Data file name
;
;	PIPE	 - The IDL unit number of the bi-directional pipe to the
;		   ADAS214 FORTRAN process.
;
;       UTITLE   - Optional comment by user
;
;	GRPSCAL	 - Integer; 0 if default scaling required 1 if user
;		   specified scaling to be used.
;
;	XMIN	 - String; User sepcified x-axis minimum, number as string.
;
;	XMAX	 - String; User sepcified x-axis maximum, number as string.
;
;	YMIN	 - String; User specified y-axis minimum, number as string.
;
;	YMAX	 - String; User specified y-axis maximum, number as string.
;
;	HRDOUT	 - Integer; 0 if no hardcopy required, 1 if hardcopy.
;
;	HARDNAME - String; file name for hardcopy output.
;
;	DEVICE	 - String; IDL name for hardcopy output device.
;
;	HEADER	 - ADAS version header information for inclusion in the
;		   graphical output.
;
;       BITFILE - String; the path to the dirctory containing bitmaps
;                 for the 'escape to series menu' button.
;
; OPTIONAL INPUTS:
;	None.
;
; OUTPUTS:
;       GOMENU  - Int; flag - set to 1 if user has selected 'escape direct
;                 to series menu , otherwise 0
;
; OPTIONAL OUTPUTS:
;	None.
;
; KEYWORD PARAMETERS:
;	FONT	- String; Name of the font to be used for the graphical
;		  output widget.
;
; CALLS:
;	ADAS214_PLOT	ADAS214 graphical output.
;
; SIDE EFFECTS:
;	This routine reads information from FORTRAN via a UNIX pipe.
;
; CATEGORY:
;	Adas system.
;
; WRITTEN:
;       Stuart Loch, University of Strathclyde, 1998.
;	  Based on b1oug.pro v1.3 
;
; MODIFIED:
;	1.1	Stuart Loch.
;		First Release.
;
; VERSION:
;	1.1	18-06-98
;
; VERSION:
;	1.2	Stuart Loch			26-11-98
;		Variable sum2 included in call to adas214_plot.pro
;		to allow plotting of emergent flux escape factor
;-
;-----------------------------------------------------------------------------



PRO beoutg, dsfull, pipe, utitle, grpscal, xmin, xmax, ymin, ymax, $
		  hrdout, hardname, ldef1, device, header, bitfile,$
		  proc_out, setting, proc_opt, gomenu, $
	  	  FONT=font


                ;**** Set defaults for keywords ****
  IF NOT (KEYWORD_SET(font)) THEN font = ''


		;**** Declare variables for input ****

  titlx = " "
  titlm = " "
  date = " "
  nener = 0
  dummy = 0.0D
  input = 0

		;********************************
		;**** Read data from fortran ****
		;********************************
		
		;*** Read titles and date information ***
  readf, pipe, format = '(a120)' , titlx 
  readf, pipe, format = '(a80)' , titlm 
  readf, pipe, format = '(a8)' , date 
  readf, pipe, input
  nener = input
  readf, pipe, numet
  readf, pipe, steps

  x2=fltarr(numet,2)
  y2=fltarr(numet,2)
		;**** now declare array dimensions ****
  temp = dblarr(nener)
  rate = dblarr(nener)
  sum2 = dblarr(nener)
  if proc_opt.scanopt eq 0 then begin
    ratio=fltarr(numet,steps)
    densarr=fltarr(steps)
  endif

  modprof=fltarr(30,nener)
  optplot=fltarr(10)


		;*** Read data from file ***
  if (proc_out.grpsel lt 2) then begin
     for i=0, nener-1 do begin
        readf, pipe, dummy
        temp(i) = dummy 
     endfor
     for i=0, nener-1 do begin
        readf, pipe, dummy
        rate(i) = dummy 
     endfor
  endif else begin
     for i=0, numet-1 do begin
         for j=0, steps-1 do begin
	     readf, pipe, dummy
	     ratio(i,j)=dummy
	 endfor
     endfor
     for i=0,(steps-1) do begin
	 readf,pipe, dummy
         densarr(i)=dummy
     endfor
   endelse

   if proc_out.grpsel eq 0 then begin
      for j=0,numet-1 do begin
	  for i=0,1 do begin
	    readf, pipe, dummy1
	    readf, pipe, dummy2
	    x2(j,i)=dummy1
	    y2(j,i)=dummy2
          endfor
      endfor
      for i=0,(nener-1) do begin
	  readf, pipe, dummy
	  sum2(i)=dummy
      endfor
   endif

   if proc_out.grpsel eq 1 then begin
      for i=0,29 do begin
	  for j=0, nener-1 do begin
	      readf, pipe, dummy
	      modprof(i,j)=dummy
	  endfor
      endfor
   endif


		;***********************
		;**** Plot the data ****
		;***********************

  adas214_plot, dsfull, $
  		titlx, titlm, utitle, date, $
 		temp, rate, ratio, densarr, $
                ldef1, xmin, xmax, ymin, ymax, $
   		nener, modprof, optplot, x2, y2,$
		hrdout, hardname, device, header, $
		bitfile, proc_out, numet, steps, $
		proc_opt, gomenu, sum2, FONT=font


END
