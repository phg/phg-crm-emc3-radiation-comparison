; Copyright (c) 1998, Strathclyde University.
; MODIFIED:
;
;       1.1     Stuart Loch
;               First Version
;       1.2     Richard Martin
;               Corrected spelling mistake.
;       1.3     Martin O'Mullane
;                 - Identify routine to xmanager as ADAS214_OUT an
;                   not ttcw_adas_gr_sel which was shared among a
;                   few routines.
;                 - Give the common block a unique name.
;
; VERSION:
;       1.1     18-06-98
;       1.2     18-03-98
;       1.3     13-05-2005
;-
;-----------------------------------------------------------------------------
pro adas214_out_event,event
        common general_214out, grpid , action , value_out
        action = event.action
        case action of
        'Done': begin
                 widget_control,grpid,get_value=value_out
                 widget_control,event.top,/destroy
                end
        'Cancel':widget_control,event.top,/destroy
        'Menu':widget_control,event.top,/destroy
        else: print,'Some other button has been pressed'
        endcase
end
;-----------------------------------------------------------------------------


pro adas214_out,pipe,value,dsfull,devlist,proc_in,bitmap1,font=font,act, bitfile, proc_opt
        common general_214out, grpid , action , value_out
        value_out = value
        if (proc_opt.scanopt eq 0) then grplist=['Norm Escape Factor','Modified Profile','Ratio Plot']
        if (proc_opt.scanopt eq 1) then grplist=['Norm Escape Factor','Modified Profile']
        parent=widget_base(title='ADAS214 OUTPUT OPTIONS',/column)
        grpid=cw_adas214_out(parent, dsfull, value=value, grplist=grplist,$
                                  devlist=devlist, font=font,bitmap1)

        state   = { grpid       : grpid }
        widget_control,parent,/realize
        widget_control,widget_info(parent,/child),set_uvalue=state,/no_copy
        xmanager,'ADAS214_OUT',parent,event_handler='adas214_out_event'
        act = action
        value = value_out
end
