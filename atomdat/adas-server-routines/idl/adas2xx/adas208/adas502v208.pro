; Copyright (c) 1995 Strathclyde University .
; SCCS info: Module @(#)$Header: /home/adascvs/idl/adas2xx/adas208/adas502v208.pro,v 1.4 2004/07/06 10:53:51 whitefor Exp $ Date $Date: 2004/07/06 10:53:51 $
;+
; PROJECT:
;       ADAS IBM MVS to UNIX conversion
;
; NAME:
;	ADAS502V208
;
; PURPOSE:
;	Starts ADAS502V208 which is ADAS502 changed for use in ADAS208
;
; EXPLANATION:
;	This routine is called from the adas routine, cw_adas208_proc.pro,
;	to start an ADAS 502-like application.  Associated with adas502V208.pro
;	is a FORTRAN executable.  The IDL code provides the user
;	interface and output graphics whilst the FORTRAN code reads
;	in data files, performs numerical processing and creates the
;	output files.  The IDL code communicates with the FORTRAN
;	executable via a bi-directional UNIX pipe.  The unit number
;	used by the IDL for writing to and reading from this pipe is
;	allocated when the FORTRAN executable is 'spawned' (see code
;	below).  Pipe communications in the FORTRAN process are to
;	stdin and stdout, i.e streams 5 and 6.
;
;	The FORTRAN code is an independent process under the UNIX system.
;	The IDL process can only exert control over the FORTRAN in the
;	data which it communicates via the pipe.  The communications
;	between the IDL and FORTRAN must be exactly matched to avoid
;	input conversion errors.  The correct ammounts of data must be
;	passed so that neither process 'hangs' waiting for communications
;	which will never occur.
;
;	The FORTRAN code performs some error checking which is
;	independent of IDL.  In cases of error the FORTRAN may write
;	error messages.  To prevent these error messages from conflicting
;	with the pipe communications all FORTRAN errors are written to
;	output stream 0, which is stderr for UNIX.  These error messages
;	will appear in the window from which the ADAS session/IDL session
;	is being run.
;
;	In the case of severe errors the FORTRAN code may terminate
;	itself prematurely.  In order to detect this, and prevent the
;	IDL program from 'hanging' or crashing, the IDL checks to see
;	if the FORTRAN executable is still an active process before
;	each group of pipe communications.  The process identifier
;	for the FORTRAN process, PID, is recorded when the process is
;	first 'spawned'.  The system is then checked for the presence
;	of the FORTRAN PID.
;
; USE:
;	First the system settings must be established and passed through
;	to adas502V208.pro which is called to start the ADAS502V208
;	application. Also, information from adas208 is passed through to
;	be used:
;
;	adas_sys_set, adasrel, fortdir, userroot, centroot, $
;		      devlist, devcode, font_large, font_small, edit_fonts
;
;		.... ADAS208 stuff. Definition of other params ...
;
;	adas502v208,   adasrel, fortdir, userroot, centroot, devlist, 	$
;		       devcode, font_large, font_small, edit_fonts,	$
;                      msize, strgmm, imetr, sz, selem, tine,           $
;                      ibsela, lsseta, sgrda, ndmet, itout
;
; INPUTS:
;	ADASREL - A string indicating the ADAS system version, 
;		  e.g ' ADAS RELEASE: ADAS93 V1.1.0'.  The first
;		  character should be a space.
;
;	FORTDIR - A string holding the path to the directory where the
;		  FORTRAN executables are, e.g '/disk/adas/fortran/exec'
;
;	USERROOT - A string holding the path to the root directory of
;		   the user's adas data. e.g '/disk/bowen/adas'
;		   This root directory will be used by adas to construct
;		   other path names.  For example the users default data
;		   for adas502 should be in /disk/bowen/adas/adf04.  In
;		   particular the user's default interface settings will
;		   be stored in the directory USERROOT+'/defaults'.  An
;		   error will occur if the defaults directory does not
;		   exist.
;
;	CENTROOT - Like USERROOT, but this directory points to the
;		   central data area for the system.  User defaults are
;		   not stored on CENTROOT.
;
;	DEVLIST - A string array of hardcopy device names, used for
;		  graphical output. e.g ['Post-Script','HP-PCL','HP-GL']
;		  This array must mirror DEVCODE.  DEVCODE holds the
;		  actual device names used in a SET_PLOT statement.
;
;	DEVCODE - A string array of hardcopy device code names used in
;		  the SET_PLOT statement, e.g ['PS', 'PCL', 'HP']
;		  This array must mirror DEVLIST.
;
;	FONT_LARGE - The name of a larger font e.g 'courier_bold14'
;
;	FONT_SMALL - The name of a smaller font e.g 'courier_bold12'
;
;	EDIT_FONTS - A structure of two fonts used in the table editor
;		     adas_edtab.pro, {font_norm:'',font_input:''}.
;		     The two fonts are used to differentiate between
;		     editable and non-editable parts of the table. You
;		     may have to experiment to get two fonts which give
;		     the same default spacing in the edit widget.
;		     e.g {font_norm:'helvetica_bold14', $
;			  font_input:'helvetica_oblique14'}
;	MSIZE	- Integer; length of metastable lists
;	STRGMM  - String; metastable info string
;	IMETR	- Integer array; selected metastable indices
;	SZ	- String; Ion charge
;	SELEM	- String; element symbol
;	TINE	- Double array; electron temperatures
;	IBSELA	- ?
;	LSSETA 	- ?
;	SGRDA	- ?
;	NDMET	- Integer; number of metastables chosen
;	ITOUT	- Integer; temperature units
;
; OPTIONAL INPUTS:
;	None
;
; OUTPUTS:
;	IBSELA	- ?
;	LSSETA 	- ?
;	SGRDA	- ?
;	DSFULL  - String; full filename from the 502 input screen
;
;
; OPTIONAL OUTPUTS:
;	None
;
; KEYWORD PARAMETERS:
;	None
;
; CALLS:
;	FIND_PROCESS	Checks to see if a given process is active.
;	XXDATE		Get date and time from operating system.
;	E2SPF0		Pipe comms with FORTRAN E2SPF0 routine.
;	E2SETP		Pipe comms with FORTRAN E2SETP routine.
;	E2B8ISPF	Pipe comms with FORTRAN E2ISPF routine.
;	E2SPF1		Pipe comms with FORTRAN E2SPF1 routine.
;	E2B8OUTG	Pipe comms with FORTRAN E2OUTG routine.
;
; SIDE EFFECTS:
;	This routine spawns a FORTRAN executable.  Note the pipe 
;	communications routines listed above.  In addition to these
;	pipe communications there is one explicit communication of the
;	date to the FORTRAN code, search for 'printf, pipe' to find it.
;
; CATEGORY:
;	Adas system.
;	
; WRITTEN:
;       David H.Brooks, Universiy of Strathclyde, date unknown
;
; MODIFIED:
;       1.0	     	David Brooks
;			Written using adas502.pro v1.4 as a template
;	1.1	    	William Osborn (Tessella Support Services plc)
;			Added documentation + put under SCCS control
;	1.2	    	William Osborn
;			Added dsfull in program parameters to return value
;			to ADAS 208
;	1.3	    	William Osborn
;			Reset variables to 0 rather than -1 at start
;	1.4	    	Hugh Summers
;			Adjustment to allow # marker on strgmm
;
; VERSION:
;	1.0	????????
;	1.1	10-05-96
;	1.2	13-05-96
;	1.3	10-07-96
;	1.4	15-09-99
;
;-
;-----------------------------------------------------------------------------


PRO ADAS502V208, adasrel, fortdir, userroot, centroot, 			$
		devlist, devcode, font_large, font_small, edit_fonts,   $
                msize, strgmm, imetr, sz, selem, tine,                  $
                ibsela, lsseta, sgrda, ndmet, itout, dsfull

		;************************
		;**** Initialisation ****
		;************************

    adasprog = ' PROGRAM: ADAS502V208 V1.1'
    lpend = 0
    gomenu = 0
    deffile = userroot+'/defaults/adas502v208_defaults.dat'
    device = ''
    bitfile = centroot+'/bitmaps'
                ;**** reset variables for a fresh run ****
    ibsela(*,*) = 0
    lsseta(*,*) = 0
    sgrda(*,*,*) = 0.0
    hlist_flag = -1

		;******************************************
		;**** Search for user default settings ****
		;**** If not found create defaults     ****
		;******************************************

    files = findfile(deffile)
    if files(0) eq deffile then begin
        restore,deffile
        inval.centroot = centroot+'/adf07/'
        inval.userroot = userroot+'/adf07/'
    endif else begin
        inval   = { 	ROOTPATH	:	userroot+'/adf07/',	$
			FILE		:		'', 		$
			CENTROOT	:	centroot+'/adf07/', 	$
			USERROOT	:	userroot+'/adf07/' 	}
        procval = {	NEW		:	-1			}
    	outval  = { 	GRPOUT		:	0, 			$
			GTIT1		:	'', 			$
			GRPSCAL		:	0, 			$
			XMIN		:	'',  			$
			XMAX		:	'',   			$
			YMIN		:	'',  			$
			YMAX		:	'',   			$
			HRDOUT		:	0, 			$
			HARDNAME	:	'', 			$
			GRPDEF		:	'', 			$
			GRPFMESS	:	'', 			$
			GRPSEL		:	-1, 			$
			GRPRMESS	:	'', 			$
			DEVSEL		:	-1, 			$
			GRSELMESS	:	'', 			$
			TEXOUT		:	0, 			$
			TEXAPP		:	-1, 			$
			TEXREP		:	0, 			$
			TEXDSN		:	'', 			$
			TEXDEF		:	'paper.txt2',		$
			TEXMES		:	'' 			}
    endelse

		;****************************
		;**** Start fortran code ****
		;****************************

    spawn, fortdir + '/adas502v208.out', unit=pipe, /noshell, PID=pid

		;************************************************
		;**** Get date and write to fortran via pipe ****
		;************************************************

    date = xxdate()
    printf, pipe, date(0)

LABEL100:
		;**** Check FORTRAN still running ****
  
    if find_process(pid) eq 0 then goto, LABELEND

		;************************************************
		;**** Communicate with e2spf0 in fortran and ****
		;**** invoke user interface widget for       ****
		;**** Data file selection                    ****
		;************************************************

    e2spf0, pipe, inval, dsfull, rep, FONT=font_large

		;**** If cancel selected then end program ****

    if rep eq 'YES' then goto, LABELEND
  
		;**** Check FORTRAN still running ****

    if find_process(pid) eq 0 then goto, LABELEND

		;*************************************
		;**** Read pipe comms from e2setp ****
		;*************************************

    e2setp, pipe, nbsel


LABEL200: 
		;**** Check FORTRAN still running ****

    if find_process(pid) eq 0 then goto, LABELEND

		;************************************************
		;**** Communicate with e2ispf in fortran and ****
		;**** invoke user interface widget for       ****
		;**** Processing options                     ****
		;************************************************

    e2b8ispf, pipe, lpend, procval, dsfull, nbsel, gomenu, bitfile,	$
            msize, strgmm, imetr, sz, selem, tine, hlist_flag, ndmet,   $
	    itout, FONT_LARGE=font_large, FONT_SMALL=font_small,      	$
	    EDIT_FONTS=edit_fonts

		;**** If menu button clicked, tell FORTRAN to stop ****

    if gomenu eq 1 then begin
 	printf, pipe, 1
 	goto, LABELEND
    endif else begin
 	printf, pipe, 0
    endelse


 		;**** If cancel selected then goto 100 ****

    if lpend eq 1 then goto, LABEL100

		;**** Fortran processing now in progress. ****
		;**** Create header for output.           ****

    header = adasrel + adasprog + ' DATE: ' + date(0) + ' TIME: ' + date(1)

		;**** Create header for output. ****

    if find_process(pid) eq 0 then goto, LABELEND

                
LABEL300:
		;**** Check FORTRAN still running ****

    if find_process(pid) eq 0 then goto, LABELEND

		;************************************************
		;**** Communicate with e2spf1 in fortran and ****
		;**** invoke user interface widget for       ****
		;**** Output options                         ****
		;************************************************

    e2spf1, pipe, lpend, outval, dsfull, header, bitfile, gomenu, 	$
            pid, DEVLIST=devlist, FONT=font_large

                ;**** Extra check to see whether FORTRAN is still there ****

    if find_process(pid) eq 0 then goto, LABELEND

		;**** If menu button clicked, tell FORTRAN to stop ****

    if gomenu eq 1 then begin
	printf, pipe, 1
        outval.texmes = ''
	goto, LABELEND
    endif else begin
	printf, pipe, 0
    endelse

		;**** If cancel selected then erase output ****
		;**** messages and goto 200.		   ****

    if lpend eq 1 then begin
       outval.texmes = ''
       goto, LABEL200
    endif
                 ;**** reset record button for every selection ****
    record = 0

		;************************************************
		;**** If graphical output requested          ****
		;**** Communicate with e2outg in fortran and ****
		;**** invoke widget Graphical output         ****
		;************************************************

    if outval.grpout eq 1 then begin

		;**** User explicit scaling ****

        grpscal = outval.grpscal
        xmin = outval.xmin
        xmax = outval.xmax
        ymin = outval.ymin
        ymax = outval.ymax

		 ;**** Hardcopy output ****

        hrdout = outval.hrdout
        hardname = outval.hardname
        if outval.devsel ge 0 then device = devcode(outval.devsel)
 
		 ;*** user title for graph ***

       utitle = outval.gtit1

                 ;**** create arrays in order to pass ionisation ****
                 ;**** coefficients and temperatures out ****
       info = size(tine)
       dimn = info(1)
       szda_sel = dblarr(dimn)
       teva = dblarr(dimn)
       itval_sel = 0

       e2b8outg, dsfull, pipe, utitle, grpscal, xmin, xmax, ymin, ymax,   $
	       hrdout, hardname, device, header, bitfile, gomenu, record, $
               szda_sel, teva_sel, itval_sel, FONT=font_large

		;**** If menu button clicked, tell FORTRAN to stop ****

        if gomenu eq 1 then begin
            printf, pipe, 1
            outval.texmes = ''
            goto, LABELEND
        endif else begin
            printf, pipe, 0
        endelse
    endif
	
		;**** Back for more output options - use done ****
		;**** Back to processing options - use record ****
    if record eq 1 then begin
      printf, pipe, 1
                ;**** locate the brackets in the string ****
      i = 0
      bcnt = 0
      pos = make_array(strlen(strgmm(procval.imsel)),/int,value = -1)
      while ( i ne -1 ) do begin
         i = strpos(strgmm(procval.imsel),'(',i)
         if ( i ne -1 ) then begin
           pos(bcnt) = i
           bcnt = bcnt+1
           i = i+1
         end
      endwhile

                ;**** fetch out the indices ****
                ;**** shift 1st position by 1 to allow for #  15-sep-99 ****

;      stri = strmid(strgmm(procval.imsel),0,pos(0))
      stri = strmid(strgmm(procval.imsel),1,pos(1))
      strf = strmid(strgmm(procval.imsel),pos(bcnt-1)-2,2) 
      indxinit = long(strtrim(stri,2))
      indxfin  = long(strtrim(strf,2))
      ibsela(indxinit-1,indxfin-1) = procval.ibsel+1
      lsseta(indxinit-1,indxfin-1) = 1
      for i = 0, itval_sel-1 do begin
         sgrda(i,indxinit-1,indxfin-1) = float(szda_sel(i))
      endfor

                ;**** fill the selection list highlighter array ****
      procval.hlist(procval.imsel) = 1
      hlist_flag = 1
      GOTO, LABEL200
    endif else begin
      printf, pipe, 0
      GOTO, LABEL300
    endelse

LABELEND:

		;**** Save user defaults ****

     save, inval, procval, outval, filename=deffile

END
