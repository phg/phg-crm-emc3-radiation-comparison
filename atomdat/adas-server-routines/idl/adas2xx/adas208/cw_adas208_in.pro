; Copyright (c) 1995, Strathclyde University
; SCCS info: Module @(#)$Header: /home/adascvs/idl/adas2xx/adas208/cw_adas208_in.pro,v 1.2 2004/07/06 12:32:55 whitefor Exp $ Date $Date: 2004/07/06 12:32:55 $
;+
; PROJECT:
;       UNIX IDL ADAS development
;
; NAME:
;	CW_ADAS208_IN
;
; PURPOSE:
;	Data file selection for two input datasets.
;
; EXPLANATION:
;	This function creates a compound widget consisting of two
;       occurences of the compound widget cw_adas4xx_infile.pro
;	a 'Cancel' button, a 'Done' button and two buttons 
;	to allow the browsing of the selected data file comments.
;	The browsing and done buttons are automatically de-sensitised
;	until a valid input dataset has been selected.
;
;	The value of this widget is the settings structure of two
;	cw_adas4xx_infile widgets.  This widget only generates events
;	when either the 'Done' or 'Cancel' buttons are pressed.
;	The event structure returned is;
;	{ID:0L, TOP:0L, HANDLER:0L, ACTION:''}
;	ACTION has one of two values 'Done' or 'Cancel'.
;
; USE:
;	See routine adas_in.pro for an example.
;
; INPUTS:
;	PARENT	- Long integer; the ID of the parent widget.
;
; OPTIONAL INPUTS:
;	None
;
; OUTPUTS:
;	This function returns the ID of the compound widget created.
;
; OPTIONAL OUTPUTS:
;	None
;
; KEYWORD PARAMETERS:
;	VALUE	- A structure which determines the initial settings of
;                 the dataset selection widget cw_adas4xx_infile.  The
;		  structure must be;
;		  {ROOTPATH1:'', FILE1:'', ROOTPATH2:'', FILE2:'',
;                  CENTROOT1:'', USERROOT1:'', CENTROOT2:'', USERROOT2:'',
;                  ADASREL:'', FORTDIR:'', USERROOT:'', CENTROOT:'',
;                  DEVLIST:strarr, DEVCODE:0, FONTL:'', FONTS:'',
;                  EFONT:tfont_struct }
;		  The elements of the structure are as follows;
;
;	  ROOTPATH1 - first data directory e.g '/usr/fred/adas/adf04/'
;	  FILE1     - first data file in ROOTPATH1 e.g 'input.dat'
;	  ROOTPATH2 - second data directory e.g '/usr/fred/adas/adf18/'
;	  FILE2     - second data file in ROOTPATH2 e.g 'input.dat'
;	  CENTROOT1 - Default central data store e.g '/usr/adas/adf04/'
;	  USERROOT1 - Default user data store e.g '/usr/fred/adas/adf04/'
;	  CENTROOT2 - Default central data store e.g '/usr/adas/adf18/'
;	  USERROOT2 - Default user data store e.g '/usr/fred/adas/adf18/'
;	  ADASREL, FORTDIR - main ADAS variables
;	  USERROOT  - Default user data store e.g '/usr/fred/adas/'
;	  CENTROOT  - Default central data store e.g '/usr/adas/'
;	  EXPSEL    - 1 => an expansion file has been selected
;		      0 => an expansion file has not been selected
;	  OTHERS    - See adas_sys_set
;
;
;		  The first data file selected by the user is obtained by
;		  appending ROOTPATH1 and FILE1, and the second by
;                 appending ROOTPATH2 and FILE2.  In the above example
;		  the full name of both data files is;
;		  /usr/fred/adas/input.dat
;
;		  Path names may be supplied with or without the trailing
;		  '/'.  The underlying routines add this character where
;		  required so that USERROOT will always end in '/' on
;		  output.
;
;		  The default value is;
;		  {ROOTPATH1:'./', FILE1:'', ROOTPATH2:'./', FILE2:'',
;                  CENTROOT:'', USERROOT:'' ... (see below)}
;		  i.e ROOTPATH1 & ROOTPATH2 are set to the user's current
;                 directory.
;
;	TITLE	- The title to be included in the input file widget, used
;                 to indicate exactly what the required input dataset is,
;                 e.g 'Input COPASE Dataset'
;
;	UVALUE	- A user value for the widget.  Defaults to 0.
;
;	FONT	- Supplies the font to be used for the interface widgets.
;		  Defaults to the current system font.
;
; CALLS:
;	XXTEXT	          Pop-up window to browse dataset comments.
;	CW_ADAS4XX_INFILE Dataset selection widget.
;	FILE_ACC	  Determine filetype and access permissions.
;
;	See side effects for other related widget management routines.
;
; SIDE EFFECTS:
;	IN208_GET_VAL()	Widget management routine in this file.
;	IN208_EVENT()	Widget management routine in this file.
;
; CATEGORY:
;	Compound Widget
;
; WRITTEN:
;       David H.Brooks, Univ.of Strathclyde, 20-Nov-1995
;       Altered cw_adas_in.pro to allow double file selection.
;
; MODIFIED:
;	1.0	David H.Brooks
;		First version - written using skeleton of adas210.pro v1.3
;		Removed references to 3rd file for 208. Sensitised
;               'done' button so that program will proceed without an
;               expansion file.
;	1.1	William Osborn
;		Wrote header comments + put under SCCS control
;	1.2	William Osborn
;		Removed xsize=n keyword from widget_label command.
;		The dynlabel procedure does not override these
;		assignments and they were taken as being values in pixels
;		on some machines.
;
; VERSION:
;       1.0	12-03-96
;	1.1	10-05-96
;	1.2	01-08-96
;
;-
;-----------------------------------------------------------------------------

FUNCTION in208_get_val, id

		;***********************************
                ;**** Return to caller on error ****
		;***********************************
    ON_ERROR, 2

		;***********************************************
                ;**** Retrieve the structure from the child ****
		;***********************************************

    first_child = widget_info(id, /child)
    widget_control, first_child, get_uvalue=state, /no_copy
  
		;**********************
		;**** Get settings ****
		;**********************

    widget_control, state.file1id, get_value=inset1
    widget_control, state.file2id, get_value=inset2
    widget_control, state.file2id, get_uvalue=expsel
 
		;***************************************
                ;**** Copy values to main structure ****
		;***************************************

    inset = {	in208_set,                                              $ 
              	ROOTPATH1	:	inset1.rootpath, 		$
		FILE1   	:	inset1.file,      		$
              	ROOTPATH2	:	inset2.rootpath, 		$
		FILE2   	:	inset2.file,      		$
	      	CENTROOT1 	:	inset1.centroot, 		$
		USERROOT1	:	inset1.userroot,  		$
	      	CENTROOT2 	:	inset2.centroot, 		$
		USERROOT2	:	inset2.userroot,   		$
                ADASREL         :       state.inset.adasrel,            $
                FORTDIR         :       state.inset.fortdir,            $
                USERROOT        :       state.inset.userroot,           $
                CENTROOT        :       state.inset.centroot,           $
                DEVLIST         :       state.inset.devlist,            $
                DEVCODE         :       state.inset.devcode,            $
                FONTL           :       state.inset.fontl,              $
                FONTS           :       state.inset.fonts,              $
                EFONT           :       state.inset.efont,		$
		EXPSEL		:	expsel}

		;***************************
                ;**** restore the state ****
		;***************************

    widget_control, first_child, set_uvalue=state, /no_copy

		;*******************************
                ;**** return the value here ****
		;*******************************
  
   RETURN, inset

END

;-----------------------------------------------------------------------------

FUNCTION in208_event, event

		;************************************
                ;**** Base ID of compound widget ****
		;************************************

    parent=event.handler

		;***********************************************
                ;**** Retrieve the structure from the child ****
		;***********************************************

    first_child = widget_info(parent, /child)
    widget_control, first_child, get_uvalue=state, /no_copy

		;*********************************
		;**** Clear previous messages ****
		;*********************************

    widget_control, state.messid, set_value=''

		;*********************************
		;**** Default output no event ****
		;*********************************

    new_event = 0L

                ;************************
                ;**** Process events ****
                ;************************

    CASE event.id OF

		;***********************************
		;**** Event from file selection ****
		;***********************************

        state.file1id: begin
	    if event.action eq 'newfile' then begin
                widget_control, state.file1id, set_uvalue = 1
                widget_control, state.file2id, get_uvalue = picked
	        widget_control, state.browse1id, /sensitive
;                if picked eq 1 then begin
                  widget_control, state.doneid, /sensitive
;                end
	    endif else begin
                widget_control, state.file1id, set_uvalue = 0
	        widget_control, state.browse1id, sensitive=0
                widget_control, state.doneid, sensitive = 0
	    endelse
	end

		;***********************
		;**** Browse button ****
		;***********************

    	state.browse1id: begin

		;*****************************
		;**** Get latest filename ****
		;*****************************

	    widget_control, state.file1id, get_value=inset1
	    filename1 = inset1.rootpath + inset1.file

		;**********************************
		;**** Invoke comments browsing ****
		;**********************************

	  xxtext, filename1, font=state.font
	end
		;*******************************
		;**** Second file selection ****
		;*******************************

    	state.file2id: begin
	    if event.action eq 'newfile' then begin
                widget_control, state.file2id, set_uvalue = 1
                widget_control, state.file1id, get_uvalue = picked
	        widget_control, state.browse2id, /sensitive
                if picked eq 1 then begin
                  widget_control, state.doneid, /sensitive
                end
	    endif else begin
	        widget_control, state.file2id, set_uvalue = 0
;	        widget_control, state.doneid, sensitive=0
	        widget_control, state.browse2id, sensitive=0
	    endelse
	end

		;***********************
		;**** Browse button ****
		;***********************

    	state.browse2id: begin

		;*****************************
		;**** Get latest filename ****
		;*****************************

	    widget_control, state.file2id, get_value=inset2
	    filename2 = inset2.rootpath + inset2.file

		;**********************************
		;**** Invoke comments browsing ****
		;**********************************

	    xxtext, filename2, font=state.font
	end

		;***********************
		;**** Cancel button ****
		;***********************

    	state.cancelid: new_event = {ID:parent, TOP:event.top, 		$
				     HANDLER:0L, ACTION:'Cancel'}

		;*********************
		;**** Done button ****
		;*********************

    	state.doneid: begin
;		;******************************************
;		;**** search for availablity of parent ****
;                ;****       superstructure file        ****
;		;******************************************
;
;            widget_control, state.file2id, get_value = inset2
;            filechk = inset2.file
;            charpos = make_array(strlen(filechk), /long, value = -1)
;            i = 0
;            cnt = 0
;            while (i ne -1) do begin
;                i = strpos(filechk,'#',i)
;                if i ne -1 then begin cnt = cnt + 1
;                    charpos(cnt-1) = i
;                    i = i + 1
;                endif
;            endwhile
;
;		;******************************
;                ;**** partition the string ****
;		;******************************
;
;            uscore = strpos(filechk, '_')
;            seq=strmid(filechk, charpos(cnt-2)+1,(uscore-(charpos(cnt-2)+1)))
;            dot=strpos(filechk, '.')
;            elc=strmid(filechk, charpos(cnt-1)+1, (dot-(charpos(cnt-1)+1)))
;            nseq = ''
;
;		;***********************************************
;           	;**** create parent file name and directory ****
;		;***********************************************
;
;            seqnames = ['h','he','li','be','b','c','n','o','f','ne',	$
;                        'na','mg','al','si','p','s','cl','ar']
;            for i = 0, n_elements(seqnames)-1 do begin
;                if seq eq seqnames(i) then begin
;                    nseq = seqnames(i-1)
;                endif
;            endfor
;            npos = strlen(elc)
;            for i = 0,9 do begin
;                if strpos(elc, strtrim(string(i), 2)) lt npos and 	$
;                strpos(elc, strtrim(string(i), 2)) ne -1 then     	$
;                npos = strpos(elc, strtrim(string(i), 2))           
;            endfor
;            jres = strpos(elc,'j')
;            lres = strpos(elc,'l')
;            chg = ''
;            if jres gt lres then begin
;                chg =  strmid(elc, npos, (strpos(elc,'j')-npos))
;                chg = long(chg) + 1
;                chg = strtrim(string(chg), 2) + 'j.dat'
;            endif else if lres gt jres then begin
;                chg =  strmid(elc, npos, (strpos(elc, 'l') - npos))
;                chg = long(chg) + 1
;                chg = strtrim(string(chg), 2) + 'l.dat'
;;            endif
;            t1 = strmid(filechk, 0, charpos(0) + 1)
;            len1 = (charpos(1) + 1) - (charpos(0) + 1 + strlen(seq))
;            t2 = strmid(filechk, charpos(0) + 1 + strlen(seq), len1)
;            filechk2 = t1 + nseq + t2
;            if cnt eq 3 then begin
;                len2 = (charpos(2) + 1) - (charpos(1) + 1 + strlen(seq))
;                t3 = strmid(filechk, charpos(1) + 1 + strlen(seq), len2)
;                len3 = (charpos(cnt - 1) + npos + 1) - (charpos(2) + 1)
;                t4 = strmid(filechk, charpos(2) + 1, len3) + chg      
;                filechk2 = filechk2 + nseq + t3 + t4
;                srch1 = inset2.centroot + filechk2
;                srch2 = inset2.userroot + filechk2
;            endif else begin
;                dir = strmid(filechk, 0, charpos(cnt - 2) + 1)
;                dir = dir + nseq
;                len3 = (charpos(cnt - 1) + npos + 1) - (charpos(1) + 1)
;                t4 = strmid(filechk, charpos(1) + 1, len3) + chg      
;                filechk2 = filechk2 + t4
;                srch1 = inset2.centroot + dir + '/' + filechk2
;                srch2 = inset2.userroot + dir + '/' + filechk2
;            endelse
;
;		;**************************************
;                ;**** search to see if files exist ****
;		;**************************************
;
;            file_acc, srch1, exist1, read1, write1, exec1, type1
;            if exist1 eq 0 or type1 ne '-' or read1 eq 0 then begin 
;                file_acc, srch2, exist2, read2, write2, exec2, type2
;                if exist2 eq 0 or type2 ne '-' or read2 eq 0 then begin
;                    buts = ['Cancel','Continue']
;                    title = 'ADAS210 SEARCH RESULTS'
;                    message = 'There is no superstructure template'+	$
;                              ' for the parent metastables'
;                    act = popup(message=message, title=title, buttons=buts)
;                    if act eq 'Continue' then begin
;                        new_event = {ID:parent, TOP:event.top, 	$
;                                     HANDLER:0L, ACTION:'Done'}
;                    endif 
;                endif else begin
;                    if cnt eq 3 then begin
;                     state.inset.rootpath3 = inset2.userroot
;                     state.inset.file3 = filechk2 
;                    endif else begin
;                     state.inset.rootpath3 = inset2.userroot + dir + '/'
;                     state.inset.file3 = filechk2 
;                    endelse
;                    state.inset.pmdflg = 1
;                    new_event = {ID:parent, TOP:event.top, 		$
;                                 HANDLER:0L, ACTION:'Done'}
;                endelse
;            endif else begin
;                if cnt eq 3 then begin
;                    state.inset.rootpath3 = inset2.centroot
;                    state.inset.file3 = filechk2 
;                endif else begin
;                    state.inset.rootpath3 = inset2.centroot + dir + '/'
;                    state.inset.file3 = filechk2 
;                endelse
;                state.inset.pmdflg = 1
                new_event = {ID:parent, TOP:event.top, 			$
                             HANDLER:0L, ACTION:'Done'}
;            endelse
        end
;
    	ELSE:			;**** Do nothing ****

    ENDCASE

		;***************************
                ;**** restore the state ****
		;***************************

    widget_control, first_child, set_uvalue=state, /no_copy

    RETURN, new_event

END

;-----------------------------------------------------------------------------

FUNCTION cw_adas208_in, parent, VALUE=value, TITLE=title, 		$
                        UVALUE=uvalue, FONT=font


    IF (N_PARAMS() EQ 0) THEN MESSAGE, 'Must specify parent for cw_adas_in'
    ON_ERROR, 2					;return to caller

		;***********************************
		;**** Set defaults for keywords ****
		;***********************************

    IF NOT (KEYWORD_SET(value)) THEN begin
        sarr = make_array(3,/string,value='')
        tsr = {tsr_set, font_norm:'helvetica-bold14',font_input:'helvetica-oblique14'}
        inset = {	in208_set,                                      $ 
                	ROOTPATH1	:	'./', 			$
			FILE1		:	'', 			$
			ROOTPATH2	:	'./', 			$
			FILE2		:	'', 			$
			CENTROOT1	:	'', 			$
			USERROOT1	:	'',			$
			CENTROOT2	:	'', 			$
			USERROOT2	:	'',			$
                        ADASREL         :       '',                     $
                        FORTDIR         :       '',                     $
                        USERROOT        :       '',                     $
                        CENTROOT        :       '',                     $
                        DEVLIST         :       sarr,                   $
                        DEVCODE         :       sarr,                   $
                        FONTL           :       'courier_bold14',       $
                        FONTS           :       'courier_bold12',       $
                        EFONT           :       tsr,			$
			EXPSEL		:	0 }
    ENDIF ELSE BEGIN
        inset = {       in208_set,                                      $ 
                	ROOTPATH1	:	value.rootpath1, 	$
			FILE1		:	value.file1, 		$
                	ROOTPATH2	:	value.rootpath2, 	$
			FILE2		:	value.file2, 		$
			CENTROOT1	:	value.centroot1, 	$
			USERROOT1	:	value.userroot1,	$
			CENTROOT2	:	value.centroot2, 	$
			USERROOT2	:	value.userroot2,	$
                        ADASREL         :       value.adasrel,          $
                        FORTDIR         :       value.fortdir,          $
                        USERROOT        :       value.userroot,         $
                        CENTROOT        :       value.centroot,         $
                        DEVLIST         :       value.devlist,          $
                        DEVCODE         :       value.devcode,          $
                        FONTL           :       value.fontl,            $
                        FONTS           :       value.fonts,            $
                        EFONT           :       value.efont,		$
			EXPSEL		:	value.expsel }

        if strtrim(inset.rootpath1) eq '' then begin
            inset.rootpath1 = './'
        endif else if 							$
	strmid(inset.rootpath1, strlen(inset.rootpath1)-1, 1) ne '/' 	$
        then begin
            inset.rootpath1 = inset.rootpath1 + '/'
        endif
        if strmid(inset.file1, 0, 1) eq '/' then begin
            inset.file1 = strmid(inset.file1, 1, strlen(inset.file1)-1)
        endif
        if strtrim(inset.rootpath2) eq '' then begin
          inset.rootpath2 = './'
        endif else if 							$
        strmid(inset.rootpath2, strlen(inset.rootpath2)-1, 1) ne '/' 	$
	then begin
            inset.rootpath2 = inset.rootpath2 + '/'
        endif
        if strmid(inset.file2, 0, 1) eq '/' then begin
            inset.file2 = strmid(inset.file2, 1, strlen(inset.file2)-1)
        endif
    END
    IF NOT (KEYWORD_SET(title)) THEN title = ''
    IF NOT (KEYWORD_SET(uvalue)) THEN uvalue = 0
    IF NOT (KEYWORD_SET(font)) THEN font = ''

		;*********************************
		;**** Create the Input widget ****
		;*********************************

		;****************************
		;**** create base widget ****
		;****************************

    topbase = widget_base( parent, UVALUE = uvalue,         		$
			   EVENT_FUNC = "in208_event",       		$
		       	   FUNC_GET_VALUE = "in208_get_val", 		$
			   /COLUMN)

                ;******************************************************
                ;**** Create a dummy widget just to hold value of *****
                ;**** "state" variable so as not to get confused  *****
                ;**** with any other values. Adopt IDL practice   *****
                ;**** of using first child widget                 *****
                ;******************************************************

    first_child = widget_base(topbase)
    cwid = widget_base(first_child, /column)

		;*******************************************
		;**** Set up first file structure       ****
		;*******************************************

    inset1 = {   ROOTPATH:inset.rootpath1, FILE:inset.file1, 		$
                 CENTROOT:inset.centroot1, USERROOT:inset.userroot1 	}

		;*******************************************
		;****  First file selection widget      ****
		;*******************************************

    filebase1 = widget_base(cwid, /frame, /column)
    file1id = cw_adas4xx_infile(filebase1, value=inset1, title=title,	$
                                font=font, uvalue = 0)

		;*****************
		;**** Buttons ****
		;*****************

    base1 = widget_base(filebase1, /row)

		;*******************************
		;**** Browse Dataset button ****
		;*******************************

    browse1id = widget_button(base1, value='Browse Comments', font=font)

		;*******************************************
		;**** Set up second file structure      ****
		;*******************************************

    inset2 = {   ROOTPATH:inset.rootpath2, FILE:inset.file2, 		$
                 CENTROOT:inset.centroot2, USERROOT:inset.userroot2 	}

		;********************************************
		;**** Second file selection widget       ****
		;********************************************
	
    title2 = 'Input EXPANSION File'
    filebase2 = widget_base(cwid, /frame, /column)
    file2id = cw_adas4xx_infile(filebase2, value=inset2, title=title2,  $
                                font=font, uvalue = 0)

		;*****************
		;**** Buttons ****
		;*****************

    base2 = widget_base(filebase2,/row)

		;*******************************
		;**** Browse Dataset button ****
		;*******************************

    browse2id = widget_button(base2, value='Browse Comments', font=font)


		;***********************
		;**** Cancel Button ****
		;***********************

    base = widget_base(cwid, /row)

    cancelid = widget_button(base, value='Cancel', font=font)
  
		;*********************
		;**** Done Button ****
		;*********************

    doneid = widget_button(base, value='Done', font=font)

		;***********************
		;**** Error message ****
		;***********************

    messid = widget_label(parent, font=font, value='*')

		;********************************************************
		;**** Check default filenames and desensitise buttons****
		;**** if they are directories or have no read access ****
		;********************************************************

    filename1 = inset1.rootpath + inset1.file
    file_acc, filename1, fileexist, read, write, execute, filetype
    if filetype ne '-' then begin
        widget_control, browse1id, sensitive=0
        widget_control, doneid, sensitive=0
        widget_control, file1id, set_uvalue = 0
    endif else begin
        if read eq 0 then begin
            widget_control, browse1id, sensitive=0
            widget_control, doneid, sensitive=0
            widget_control, file1id, set_uvalue = 0
        end else begin
            widget_control, file1id, set_uvalue = 1
        end
    endelse

    filename2 = inset2.rootpath + inset2.file
    file_acc, filename2, fileexist, read, write, execute, filetype
    if filetype ne '-' then begin
        widget_control, browse2id, sensitive=0
;        widget_control, doneid, sensitive=0
        widget_control, file2id, set_uvalue = 0
    endif else begin
        if read eq 0 then begin
            widget_control, browse2id, sensitive=0
;            widget_control, doneid, sensitive=0
            widget_control, file2id, set_uvalue = 0
        endif else begin
	    widget_control, file2id, set_uvalue = 1
	endelse
    endelse


		;*************************************************
		;**** create a state structure for the widget ****
		;*************************************************

     new_state = { 	FILE1ID         : file1id         ,		$
                 	BROWSE1ID       : browse1id       ,		$
                 	FILE2ID         : file2id         ,		$
                 	BROWSE2ID       : browse2id       ,		$
	 	 	CANCELID        : cancelid        ,		$
                 	DONEID          : doneid          ,		$
                 	MESSID          : messid          ,		$
		 	FONT            : font            ,		$
                 	INSET           : inset         }

		;**************************************
                ;**** Save initial state structure ****
		;**************************************

    widget_control, first_child,  set_uvalue=new_state, /no_copy

    RETURN, cwid

END

