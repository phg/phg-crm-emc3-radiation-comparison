; Copyright (c) 1995 Strathclyde University .
; SCCS info: Module @(#)$Header: /home/adascvs/idl/adas2xx/adas208/cw_adas208_reac2.pro,v 1.4 2004/07/06 12:34:01 whitefor Exp $ Date $Date: 2004/07/06 12:34:01 $
;
;+
; PROJECT:
;       ADAS IBM MVS to DEC UNIX conversion
;
; NAME:
;	CW_ADAS208_REAC2()
;
; PURPOSE:
;	Widget consisting of reaction options and input for ADAS208.
;
; EXPLANATION:
;	This compound widget is for use in ADAS 208.  It
;	presents buttons for selecting reactions to be included in
;	subsequent calculations.  The upper portion of the widget
;	consists of three 'linked' widgets, two buttons and a text
;	entry box.  The right-hand portion of the widget consists of five
;	non-exclusive buttons.  The widgets are labeled;
;
;	'Proton Impact Collisions'		button 1
;	'Scale Proton Impact for Zeff'		button 2
;	'Enter Z-Effective for Collisions'	text input
;
;	'Ionisation Rates'			button3
;	'Neutral H Charge Exchange'		button4
;	'Free Electron Recombination'		button5
;	'Inner Shell Ionisation'		button6
;	'Include Projection Data'		button7
;
;	Button 2 is de-sensitized and unset unless button 1 is set.
;	The text input is de-sensitized and blank unless button 2 is set.
;
;	Button 7 is de-sensitized if value.expsel = 0, i.e. no expansion
;	file was selected on the input screen
;
;	The widget-value for this compound widget is a structure which
;	represents the settings of all of the child widgets;
;
;	{reacset208, ZEFF:'2.5', LPSEL:0,    LZSEL:0, $
;		 LISEL:0,     LHSEL:0,    LRSEL:0, $
;		 LIOSEL:0,    LNSEL:0,    EXPSEL:0 }
;
;	Zeff is the string from the text input, it should represent a
;	floating point number.
;
;	The settings of buttons 1-7 are represented by lpsel, lzsel, lisel,
;	lhsel, lrsel, liosel and lnsel respectively.  A value of 0 means the
;	button is unset and a value of 1 indicates that the button is set.
;	Expsel is passed to the widget from cw_adas208_proc and is set to
;	1 if an expansion file was selected on the input screen, 0 if not.
;
;	The buttons in this widget are actively monitored and maintained
;	by the event procedures for this widget.
;
; USE:
;	This simple example creates a base and then creates this
;	reaction selection widget as the only child.  Note that in order
;	for the widget buttons to react properly and for the widget-value
;	to be correctly maintained this widget must be managed using
;	XMANAGER or widget_event()
;
;	base = widget_base()
;	rid = cw_adas_reac2(base,value=reacset,font='times_roman14')
;	widget_control,base,/realize
;
; INPUTS:
;       PARENT   - The ID of the parent widget.
;
; OPTIONAL INPUTS:
;	None.
;
; OUTPUTS:
;       The return value of this function is the ID of the compound
;       widget which is of type LONG integer.
;
; OPTIONAL OUTPUTS:
;       None
;
; KEYWORD PARAMETERS:
;	VALUE	 - The initial value of the widget representing the
;		   start-up settings of the buttons and the text input.
;		   The value is a structure defined in the Explanation
;		   section above.  The default is for all buttons to be
;		   off.
;
;       FONT     - A font to use for all text in this widget.
;
;	UVALUE	 - A user value for this widget.
;
; CALLS:
;       See side effects for other related widget management routines.
;
; SIDE EFFECTS:
;	This widget uses a COMMON BLOCK: CW_ADA208SRC2_BLK to hold the
;       widget state.
;
;       Three other routines are included which are used to manage the
;       widget;
;
;	ADAS208RC2_SET_VAL
;	ADAS208RC2_GET_VAL()
;	ADAS208RC2_EVENT()
;
; CATEGORY:
;	Compound Widget
;
; WRITTEN:
;       David H.Brooks, Universiy of Strathclyde, 16-Apr-1996
;
; MODIFIED:
;       1.0	     	David Brooks
;			Written using cw_adas_reac2.pro v1.5 as a template
;                       Added liosel & lnsel to allow inclusion of
;                       inner shell ionisation & projected bundle-n data.
;	1.1	    	William Osborn (Tessella Support Services plc)
;			Added documentation, removed CW_LOADSTATE and
;			CW_SAVESTATE + put under SCCS control
;	1.2		William Osborn
;			Put last five buttons to right of others
;	1.3		William Osborn
;			Changed reacset structure name to reacset208 so as
;			not to conflict with reacset in adas205
;	1.4		William Osborn
;			Swapped liosel and lisel which were the wrong way round
;
; VERSION:
;	1.0	16-04-96
;	1.1	10-05-96
;	1.2	10-05-96
;	1.3	24-05-96
;	1.4	05-07-96
;
;-
;-----------------------------------------------------------------------------

PRO adas208rc2_set_val, id, value

  COMMON cw_adas208rc2_blk, state_base, state_stash, state

		;**** Return to caller on error ****
  ON_ERROR, 2

		;**** Retrieve the state ****
  first_child = widget_info(id,/child)
  widget_control, first_child, get_uvalue = state, /no_copy

		;*******************************
		;**** Set reaction settings ****
		;*******************************

		;**** Proton impact reactions ****
  if value.lpsel eq 1 then begin
    widget_control,state.picid,set_button=1
    widget_control,state.spiid,/sensitive

    if value.lzsel eq 1 then begin
      widget_control,state.spiid,set_button=1
      widget_control,state.zfbsid,/sensitive
      widget_control,state.pzeid,set_value=value.zeff
    end else begin
      widget_control,state.spiid,set_button=0
      widget_control,state.zfbsid,sensitive=0
      widget_control,state.pzeid,set_value=''
    end

  end else begin
    widget_control,state.picid,set_button=0
    widget_control,state.spiid,sensitive=0
    widget_control,state.zfbsid,sensitive=0
  end

		;**** Set other reaction buttons ****
  widget_control,state.irtid,set_button=value.liosel
  widget_control,state.hceid,set_button=value.lhsel
  widget_control,state.ferid,set_button=value.lrsel
  widget_control,state.isiid,set_button=value.lisel
  widget_control,state.ipdid,set_button=value.lnsel

		;**** Copy the new value to state structure ****
  state.reacset.zeff = value.zeff
  state.reacset.lpsel = value.lpsel
  state.reacset.lzsel = value.lzsel
  state.reacset.lisel = value.lisel
  state.reacset.lhsel = value.lhsel
  state.reacset.lrsel = value.lrsel
  state.reacset.liosel = value.liosel
  state.reacset.lnsel = value.lnsel

		;**** Save the new state ****
  widget_control, first_child, set_uvalue=state, /no_copy

END

;-----------------------------------------------------------------------------


FUNCTION adas208rc2_get_val, id

  COMMON cw_adas208rc2_blk, state_base, state_stash, state

		;**** Return to caller on error ****
  ON_ERROR, 2

		;**** Retrieve the state ****
  first_child = widget_info(id,/child)
  widget_control, first_child, get_uvalue = state

		;**** Get the latest Zeff value ****
  if state.reacset.lzsel eq 1 then begin
    widget_control,state.pzeid,get_value=value
    state.reacset.zeff=value(0)
  end

  RETURN, state.reacset

END

;-----------------------------------------------------------------------------

FUNCTION adas208rc2_event, event

  COMMON cw_adas208rc2_blk, state_base, state_stash, state

		;**** Base ID of compound widget ****
  base=event.handler

		;**** Retrieve the state ****

  first_child=widget_info(base,/child)
  widget_control, first_child, get_uvalue = state, /no_copy

		;************************
		;**** Process Events ****
		;************************
  CASE event.id OF

    state.picid: begin
		if state.reacset.lpsel eq 1 then begin
		  state.reacset.lpsel = 0
		  widget_control,state.spiid,set_button=0
		  widget_control,state.spiid,sensitive=0
		  state.reacset.lzsel = 0
		  widget_control,state.pzeid,set_value=''
		  widget_control,state.zfbsid,sensitive=0
		  state.reacset.zeff = ''
		end else begin
		  state.reacset.lpsel = 1
		  widget_control,state.spiid,/sensitive
		end
    end

    state.spiid: begin
		if state.reacset.lzsel eq 1 then begin
		  state.reacset.lzsel = 0
		  widget_control,state.pzeid,set_value=''
		  widget_control,state.zfbsid,sensitive=0
		  state.reacset.zeff = ''
		end else begin
		  state.reacset.lzsel = 1
		  widget_control,state.zfbsid,/sensitive
		end
    end

    state.irtid: if state.reacset.liosel eq 1 then state.reacset.liosel=0 $
					     else state.reacset.liosel=1

    state.hceid: if state.reacset.lhsel eq 1 then state.reacset.lhsel=0 $
					     else state.reacset.lhsel=1

    state.ferid: if state.reacset.lrsel eq 1 then state.reacset.lrsel=0 $
					     else state.reacset.lrsel=1

    state.isiid: if state.reacset.lisel eq 1 then state.reacset.lisel=0 $
					     else state.reacset.lisel=1

    state.ipdid: if state.reacset.lnsel eq 1 then state.reacset.lnsel=0 $
					     else state.reacset.lnsel=1

    ELSE:

  ENDCASE

		;**** Save the new state structure ****
  widget_control, first_child, set_uvalue = state, /no_copy

  action = 'reaction'

  RETURN, {ID:base, TOP:event.top, HANDLER:0L, ACTION:action}
END

;-----------------------------------------------------------------------------

FUNCTION cw_adas208_reac2, topparent, VALUE=value, $
			FONT=font, UVALUE=uvalue


  COMMON cw_adas208rc2_blk, state_base, state_stash, state

  IF (N_PARAMS() LT 1) THEN MESSAGE, 'Must specify PARENT for cw_adas_reac2'

		;**** Set defaults for keywords ****
  IF NOT (KEYWORD_SET(value)) THEN begin
		     reacset = {reacset208, ZEFF:'', $
				LPSEL:0, LZSEL:0, $
				LISEL:0, LHSEL:0, $
				LRSEL:0, LIOSEL:0, $
                                LNSEL:0, EXPSEL:0 }
		end else begin
		     reacset = {reacset208, ZEFF:value.zeff, $
				LPSEL:value.lpsel, LZSEL:value.lzsel, $
				LISEL:value.lisel, LHSEL:value.lhsel, $
				LRSEL:value.lrsel, LIOSEL:value.liosel, $
                                LNSEL:value.lnsel, EXPSEL:value.expsel }
		end
  IF NOT (KEYWORD_SET(font)) THEN font = ''
  IF NOT (KEYWORD_SET(uvalue))  THEN uvalue = 0


		;**** Create the main base for the widget ****
  parent = WIDGET_BASE(topparent, UVALUE = uvalue, $
		EVENT_FUNC = "adas208rc2_event", $
		FUNC_GET_VALUE = "adas208rc2_get_val", $
		PRO_SET_VALUE = "adas208rc2_set_val")

		;**** Use a child base to store the widget value ****

  first_child = widget_base(parent)

  cwid = widget_base(first_child,/row)

  lbase = widget_base(cwid,/column)
		;**** Title ****
  rc = widget_label(lbase,value='Reaction Selection',font=font)

		;**** Proton impact ****
  protbase = widget_base(lbase,/column,/frame)
  protbut = widget_base(protbase,/column,/nonexclusive)
  picid = widget_button(protbut,value='Proton Impact Collisions',font=font)
  spiid = widget_button(protbut,value='Scale Proton Impact for Zeff',font=font)
  zfbsid = widget_base(protbase,/row)
  rc = widget_label(zfbsid,value='Enter Z-Effective for Collisions',font=font)
  pzeid = widget_text(zfbsid,/edit,xsize=5,font=font)

		;**** Other reactions ****
  othreac = widget_base(cwid,/column,/nonexclusive)
  irtid = widget_button(othreac,value='Ionisation Rates',font=font)
  hceid = widget_button(othreac,value='Neutral H Charge Exchange',font=font)
  ferid = widget_button(othreac,value='Free Electron Recombination',font=font)
  isiid = widget_button(othreac,value='Inner Shell Ionisation',font=font)
  ipdid = widget_button(othreac,value='Include Projection Data',font=font)

		;*******************************
		;**** Set reaction settings ****
		;*******************************

		;**** Default all buttons off ****
  widget_control,protbase,set_button=0

		;**** Proton impact reactions ****
  if reacset.lpsel eq 1 then begin
    widget_control,picid,set_button=1

    if reacset.lzsel eq 1 then begin
      widget_control,spiid,set_button=1
      widget_control,pzeid,set_value=reacset.zeff
    end else begin
      widget_control,zfbsid,sensitive=0
    end

  end else begin
    widget_control,spiid,sensitive=0
    widget_control,zfbsid,sensitive=0
  end

		;**** Set other reaction buttons ****
  if reacset.liosel eq 1 then widget_control,irtid,set_button=1
  if reacset.lhsel eq 1 then widget_control,hceid,set_button=1
  if reacset.lrsel eq 1 then widget_control,ferid,set_button=1
  if reacset.lisel eq 1 then widget_control,isiid,set_button=1

		;**** Desensitize the 'include projection data' button ****
		;**** if an expansion file was not selected (expsel=0) ****

  if reacset.expsel eq 0 then begin
     widget_control,ipdid,sensitive=0
     reacset.lnsel = 0
  endif else if reacset.lnsel eq 1 then widget_control,ipdid,set_button=1

		;**** Create state structure ****
  new_state =  {PICID:picid, SPIID:spiid, PZEID:pzeid, ZFBSID:zfbsid, $
		IRTID:irtid, HCEID:hceid, FERID:ferid, ISIID:isiid, $
		IPDID:ipdid, REACSET:reacset}

		;**** Save initial state structure ****
  widget_control, first_child, set_uvalue = new_state

  RETURN, parent

END
