; Copyright (c) 1996 Strathclyde University .
; SCCS info: Module @(#)$Header: /home/adascvs/idl/adas2xx/adas208/b8spf0.pro,v 1.1 2004/07/06 11:31:05 whitefor Exp $ Date $Date: 2004/07/06 11:31:05 $
;+
; PROJECT:
;       ADAS IBM MVS to DEC UNIX conversion
;
; NAME:
;	B8SPF0
;
; PURPOSE:
;	IDL user interface and communications with ADAS208 FORTRAN
;	process via pipe.
;
; EXPLANATION:
;	Firstly this routine invokes the part of the user interface
;	used to select the input dataset for ADAS208.  When the user's
;	interactions are complete this routine communicates with the
;	ADAS208 FORTRAN application via a UNIX pipe.  Communications
;	are to the FORTRAN subroutine B8SPF0.
;
; USE:
;	The use of this routine is specific to ADAS208, see adas208.pro.
;
; INPUTS:
;	PIPE	- The IDL unit number of the bi-directional pipe to the
;		  ADAS208 FORTRAN process.
;
;	VALUE	- A structure which determines the initial settings of
;		  the dataset selection widget.  The initial value is
;		  set in adas208.pro.  VALUE is passed un-modified
;		  through to cw_adas_in.pro, see that routine for a full
;		  description.
;
; OPTIONAL INPUTS:
;	None
;
; OUTPUTS:
;	VALUE	- On output the structure records the final settings of
;		  the dataset selection widget if the user pressed the
;		  'Done' button otherwise it is not changed from input.
;
;	DSNINC	- String; The full system file name of the input COPASE
;		  dataset selected by the user for processing.
;
;	DSNINX	- String; The full system file name of the input EXPANSION
;		  dataset selected by the user for processing.
;
;	REP	- String; Indicates whether the user pressed the 'Done'
;		  or 'Cancel' button on the interface.  The action is
;		  converted to the strings 'NO' and 'YES' respectively
;		  to match up with the existing FORTRAN code.  In the
;		  original IBM ISPF interface REP was the reply to the
;		  question 'DO YOU WANT TO END PROGRAM EXECUTION'.
;
; OPTIONAL OUTPUTS:
;	None
;
; KEYWORD PARAMETERS:
;	FONT	- Supplies the font to be used for the interface widgets.
;
; CALLS:
;	ADAS_IN		Pops-up the dataset selection widget.
;
; SIDE EFFECTS:
;	This routine communicates with the ADAS208 FORTRAN process
;	via a UNIX pipe.
;
; CATEGORY:
;	Adas system.
;
; WRITTEN:
;       David H.Brooks, Universiy of Strathclyde, date unknown
;
; MODIFIED:
;       Version 1.0     David Brooks
;			Written using b5spf0.pro v1.5 as a template
;                       Added capability to read two files and check on
;                       the existence of the 2nd.
;	Version 1.1    	William Osborn
;			Added documentation + put under SCCS control
;
; VERSION:
;	1.0	02-05-96
;	1.1	10-05-96
;
;-

;-----------------------------------------------------------------------------


PRO b8spf0, pipe, value, dsninc, dsninx, rep, FONT=font

                ;***********************************
                ;**** Set defaults for keywords ****
                ;***********************************
  IF NOT (KEYWORD_SET(font)) THEN font = ''


		;**********************************
		;**** Pop-up input file widget ****
		;**********************************
  adas208_in, value, action, WINTITLE = 'ADAS 208 INPUT', $
	TITLE = 'Input COPASE Dataset', FONT = font

		;******************************************
		;**** Act on the event from the widget ****
		;******************************************
		;**** There are only two possible events ****
		;**** 'Done' and 'Cancel'.               ****
		;********************************************
  if action eq 'Done' then begin
    rep = 'NO'
  end else begin
    rep = 'YES'
  end

                ;***********************************
		;**** Construct datatset name ****
                ;***********************************
  dsninc = strcompress(value.rootpath1+value.file1)
  dsninx = strcompress(value.rootpath2+value.file2)

                ;*************************************************
                ;**** Determine whether expansion file exists ****
                ;*************************************************

  file_acc, dsninx, ex, re, wr, ex, ty
  if ty ne '-' then begin
    lpdata = 1
  end else begin
    lpdata = 0
  end


		;*******************************
		;**** Write data to fortran ****
		;*******************************
  printf, pipe, rep
  printf, pipe, dsninc
  if lpdata eq 1 then begin
    dsninx = ' ' 
    printf, pipe, dsninx
  end else begin
    printf, pipe, dsninx
  end

END
