; Copyright (c) 1996 Strathclyde University .
; SCCS info: Module @(#)$Header: /home/adascvs/idl/adas2xx/adas208/b8spf1.pro,v 1.1 2004/07/06 11:31:14 whitefor Exp $ Date $Date: 2004/07/06 11:31:14 $
;
;+
; PROJECT:
;       ADAS IBM MVS to DEC UNIX conversion
;
; NAME:
;	B8SPF1
;
; PURPOSE:
;	IDL user interface and communications with ADAS208 FORTRAN
;	process via pipe.
;
; EXPLANATION:
;	First this routine reads some information from ADAS208 FORTRAN
;	via the UNIX pipe.  Next the 'Output Options' part of the
;	interface is invoked.  Finally the results of the user's
;	interactions are written back to the FORTRAN via the pipe.
;	Communications are with the FORTRAN subroutine B8SPF1.
;
; USE:
;	The user of this routine is specific to ADAS208, See adas208.pro.
;
; INPUTS:
;	PIPE	- The IDL unit number of the bi-directional pipe to the
;		  ADAS208 FORTRAN process.
;
;	VALUE	- A structure which determines the initial settings of
;		  the dataset selection widget.  The initial value is
;		  set in adas208.pro.  VALUE is passed un-modified
;		  through to cw_adas_in.pro, see that routine for a full
;		  description.
;
;	DSNINC	- String; The full system file name of the input COPASE
;		  dataset selected by the user for processing.
;
;	HEADER	- ADAS version header information for use in printed
;		  output.  Written to FORTRAN routine XXADAS via pipe.
;
;       BITFILE - String; the path to the directory containing bitmaps
;                 for the 'escape to series menu' button.
;
;	GOMENU  - Integer; 1=> exit to menu button pressed
;			   0=> not pressed
;
; OPTIONAL INPUTS:
;	None.
;
; OUTPUTS:
;	LPEND	- Integer; indicates user action on leaving output
;		  selections.  lpend is set to 1 if the user exited
;		  with the 'Cancel' button and 0 if the user exited
;		  with 'Done'.
;
;	VALUE	- On output the structure records the final settings of
;		  the output selection widget if the user pressed the
;		  'Done' button otherwise it is not changed from input.
;
; OPTIONAL OUTPUTS:
;	None.
;
; KEYWORD PARAMETERS:
;	DEVLIST	- A string array;  Each element of the array is the
;		  name of a hardcopy graphics output device.  This
;		  list must mirror the DEVCODE list.  DEVLIST is the
;		  list which the user will see.
;		  e.g ['Post-Script','HP-PCL','HP-GL']
;
;	FONT	- Supplies the font to be used for the interface widgets.
;
; CALLS:
;	ADAS208_OUT	User interface - output options.
;
; SIDE EFFECTS:
;	This routine communicates with the ADAS208 FORTRAN process
;	via a UNIX pipe.
;
; CATEGORY:
;	Adas system.
;
; WRITTEN:
;       David Brooks, University of Strathclyde, date unknown
;
; MODIFIED:
;       1.0    		David Brooks
;                       First written using b5spf1 as template.
;	1.1		William Osborn
;			Put under SCCS control
;
; VERSION:
;       1.0       	????????
;	1.1		10-05-96
;-
;-----------------------------------------------------------------------------


PRO b8spf1, pipe, lpend, value, dsninc, header, bitfile, gomenu, $
            DEVLIST=devlist, FONT=font


                ;**** Set defaults for keywords ****
  IF NOT (KEYWORD_SET(font)) THEN font = ''

		;********************************
		;**** Read data from fortran ****
		;********************************
  maxt = 0
  ifout = 0
  readf,pipe,maxt
  tine = dblarr(maxt)
  readf,pipe,tine
  readf,pipe,ifout

		;*******************************
		;**** Make a list of graphs ****
		;*******************************
  if ifout eq 1 then units=' K' else $
  if ifout eq 2 then units=' eV' else $
	units=' Red.'
  grplist = strarr(maxt)
  for i = 0, maxt-1 do begin
    grplist(i) = string(tine(i),format='(E10.3)')+units
  end
  

		;**************************************
		;**** Pop-up output options widget ****
		;**************************************
  adas208_out, value, dsninc, action,  bitfile, $
		GRPLIST=grplist, DEVLIST=devlist, FONT=font

		;*********************************************
		;**** Act on the output from the widget   ****
		;**** There are    three possible actions ****
		;**** 'Done' , 'Cancel' and 'Menu'.       ****
		;*********************************************
  if action eq 'Done' then begin
    lpend = 0
  endif else if action eq 'Menu' then begin
    lpend = 0
    gomenu = 1
  endif else begin
    lpend = 1
  endelse

		;*******************************
		;**** Write data to fortran ****
		;*******************************
  printf,pipe,lpend
  if lpend eq 0 then begin

    printf,pipe,value.grpout
    if value.grpout eq 1 then begin
		;**** add one to convert IDL array index to FORTRAN index ****
      printf,pipe,value.grpsel + 1
      printf,pipe,value.gtit1
    end
    printf,pipe,value.texout
    if value.texout eq 1 then begin
      printf,pipe,value.texapp
      printf,pipe,value.texdsn
    end

    printf,pipe,value.conout

    printf,pipe,value.metout

    printf,pipe,value.gcrout

    printf,pipe,value.passdir+'.pass',format='(A)'

  end

		;**************************************************
		;**** Set messages and settings ready for when ****
		;**** output window is re-displayed.           ****
		;**************************************************

  if lpend eq 0 then begin
    if value.texout eq 1 then begin
		;**** If text output is requested enable append ****
		;**** for next time and update the default to   ****
		;**** the current text file.                    ****
      value.texapp=0
      value.texdef = value.texdsn
      if value.texrep ge 0 then value.texrep = 0
      value.texmes = 'Output written to file.'
    end

    if value.conout eq 1 then begin
;      value.conout = 0
      value.mess = 'Output written for chosen files.'
    end

    if value.metout eq 1 then begin
;      value.metout = 0
      value.mess = 'Output written for chosen files.'
    end
   
    if value.gcrout eq 1 then begin
;      value.gcrout = 0
      value.mess = 'Output written for chosen files.'
    end

    if value.hrdout eq 1 then begin
    end
  end

		;*************************************************
		;**** If text output has been requested write ****
		;**** header to pipe for XXADAS to read.      ****
		;*************************************************
  if lpend eq 0 and value.texout eq 1 then begin
    printf,pipe,header
  end
END
