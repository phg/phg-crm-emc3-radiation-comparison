; Copyright (c) 1996, Strathclyde University .
; SCCS info: Module @(#)$Header: /home/adascvs/idl/adas2xx/adas208/cw_adas502v208_graph.pro,v 1.1 2004/07/06 12:50:55 whitefor Exp $ Date $Date: 2004/07/06 12:50:55 $
;
;+
; PROJECT:
;       ADAS IBM MVS to   UNIX conversion
;
; NAME:
;	CW_ADAS502v208_GRAPH()
;
; PURPOSE:
;	Widget for displaying a sequence of ADAS plots.
;
; EXPLANATION:
;	This widget is a convenient collection of a graphics window
;	and a number of graphical control buttons.  No actual graphics
;	are included with the widget.  This widget simply generates
;	events each time one of the control buttons is pressed.
;	The event returned is the structure;
;
;	{ID:0L, TOP:0L, HANDLER:0L, ACTION:''}
;
;	Where ACTION may have one of seven values;
;
;	'previous', 'next', 'print', 'printall', 'done', 'bitbutton' or
;	'record'.
;
; USE:
;	An example of how to use the widget.
;
;		base = widget_base()
;		graphid = cw_adas502v208_graph(base,/print,/multiplot)
;		widget_control,base,/realize
;		widget_control,graphid,get_value=value
;		win = value.win
;
;	Then use WSET,win to set the widget to receive graphics output.
;	Use XMANAGER or WIDGET_EVENT() to process the button events from
;	the widget.
;
; INPUTS:
;       PARENT   - The ID of the parent widget.
;
; OPTIONAL INPUTS:
;       None.  See the keywords for additional controls.
;
; OUTPUTS:
;       The return value of this function is the ID of the compound
;       widget which is of type LONG integer.
;
; OPTIONAL OUTPUTS:
;       None
;
; KEYWORD PARAMETERS:
;	VALUE	 - A structure which is the value of this widget;
;		   {WIN:0, MESSAGE:''}
;
;		   WIN	   An integer; The IDL window number of the graphical
;			   output area.
;
;		   MESSAGE A string; A message to be displayed in the widget.
;
;		   WIN is only relevant when the value of the widget
;		   is recovered using GET_VALUE.  The value of WIN is
;		   ignored when cw_adas_graph is first created or
;		   SET_VALUE is used.
;
;	MULTIPLOT- Set this keyword to get the 'Next' and 'Previous' buttons.
;		   By default MULTIPLOT is not set.
;
;	PRINT	 - Set this keyword to get the 'Print' button.  If multiplot
;		   is also set a 'Print All' button will also be included on
;		   the widget.  By default PRINT is not set.
;
;	XSIZE	 - The xsize of the graphics area in pixels.  Default
;		   850 pixels.
;
;	YSIZE	 - The ysize of the graphics area in pixels.  Default
;		   750 pixels.
;
;	TITLE	 - A title to appear at the top of the widget.
;
;	FONT	 - The font to be used for text in the widget.  Default
;		   to current screen font.
;
;	BITBUTTON - Filename of menu button pixmap. If present, add a menu
;		    button.
;
; CALLS:
;	LOCALIZE	Routine to set the various position parameters
;			used in output plotting.
;
; SIDE EFFECTS:
;	This widget uses a COMMON BLOCK: CW_ADASGR502V208_BLK to hold the
;       widget state.
;
;       Three other routines are included which are used to manage the
;       widget;
;
;	ADAS502V208_GR_SET_VAL
;	ADAS502V208_GR_GET_VAL()
;	ADAS502V208_GR_EVENT()
;
; CATEGORY:
;	Compound Widget
;
; WRITTEN:
;       David H.Brooks, Universiy of Strathclyde, 30-Apr-1996
;
; MODIFIED:
;       Version 1.0     David Brooks
;			Written using cw_adas_graph.pro v1.11 as a template
;                       Added record button.
;	Version 1.1    	William Osborn
;			Added documentation + put under SCCS control +
;			removed CW_LOADSTATE and CW_SAVESTATE
;
; VERSION:
;	1.0	30-04-96
;	1.1	10-05-96
;
;-
;-----------------------------------------------------------------------------

PRO adas502v208_gr_set_val, id, value

  COMMON cw_adasgr502v208_blk, state_base, state_stash, state

		;**** Return to caller on error ****
  ON_ERROR, 2

		;**** Retrieve the state ****
  first_child=widget_info(id,/child)
  widget_control, first_child, get_uvalue = state, /no_copy

		;**** Update value in state ****
  state.grval.message = value.message

		;**** Update screen message ****
  widget_control,state.messid,set_value=value.message

		;**** Save new state structure ****
  widget_control, first_child, set_uvalue = state, /no_copy

END

;-----------------------------------------------------------------------------


FUNCTION adas502v208_gr_get_val, id

  COMMON cw_adasgr502v208_blk, state_base, state_stash, state

		;**** Return to caller on error ****
  ON_ERROR, 2

		;**** Retrieve the state ****
  first_child=widget_info(id,/child)
  widget_control, first_child, get_uvalue = state

		;**** Get IDL window number of the drawing area ****
  widget_control,state.drawid,get_value=value

		;**** Update value ****
  state.grval.win = value

		;**** Return value ****
  RETURN, state.grval

END

;-----------------------------------------------------------------------------

FUNCTION adas502v208_gr_event, event

  COMMON cw_adasgr502v208_blk, state_base, state_stash, state

		;**** Base ID of compound widget ****
  base=event.handler

		;**** Retrieve the state ****
  first_child=widget_info(base,/child)
  widget_control, first_child, get_uvalue = state, /no_copy

		;**** Clear any message ****
  widget_control,state.messid,set_value=' '
  state.grval.message = ''

		;************************
		;**** Process Events ****
		;************************
  CASE event.id OF

    state.previd:	action = 'previous'

    state.nextid:	action = 'next'

    state.printid:	action = 'print'

    state.printallid:	action = 'printall'

    state.doneid:	action = 'done'

    state.bitid:	action = 'bitbutton'

    state.recordid:	action = 'record'

  ENDCASE

		;**** Save the state ****
  widget_control, first_child, set_uvalue = state, /no_copy

  RETURN, {ID:base, TOP:event.top, HANDLER:0L, ACTION:action}
END

;-----------------------------------------------------------------------------

FUNCTION cw_adas502v208_graph,topparent, VALUE=value, MULTIPLOT = multiplot, $
			PRINT = print, BITBUTTON = bitbutton,		$
			XSIZE = xsize, YSIZE = ysize, TITLE = title, 	$
			FONT = font

  COMMON cw_adasgr502v208_blk, state_base, state_stash, state

  IF (N_PARAMS() EQ 0) THEN MESSAGE, 'Must specify a parent for cw_adas_graph'
  ON_ERROR, 2					;return to caller

		;**** Set defaults for keywords ****
  IF NOT (KEYWORD_SET(value)) THEN begin
	grval = {grval, WIN:0, MESSAGE:''}
  end else begin
	grval = {grval, WIN:0, MESSAGE:value.message}
  end
  IF NOT (KEYWORD_SET(multiplot)) THEN multiplot = 0
  IF NOT (KEYWORD_SET(print)) THEN print = 0
  IF NOT (KEYWORD_SET(xsize)) THEN xsize = 850

		;**** Customize YSIZE for different TARGET_MACHINEs ****

  machine = GETENV('TARGET_MACHINE')
  IF (machine EQ 'HPUX') THEN BEGIN
    yheight = 650
  ENDIF ELSE BEGIN
    yheight = 750
  ENDELSE
  IF NOT (KEYWORD_SET(ysize)) THEN ysize = 750
  IF NOT (KEYWORD_SET(title)) THEN title = ' '
  IF NOT (KEYWORD_SET(font)) THEN font = ''
  IF NOT (KEYWORD_SET(bitbutton)) THEN BEGIN
      bitflag=0
  ENDIF ELSE BEGIN
      bitflag=1
  ENDELSE

		;**********************************************************
		;**** Assign parameters defining the size of the graph ****
		;**** in the graphics window in IDL procedure localize ****
		;**********************************************************

  localize

		;**** Create the main base for the graph widget ****

  uvalue = 0
  parent = WIDGET_BASE(topparent, UVALUE = uvalue, 			$
		EVENT_FUNC = "adas502v208_gr_event", 			$
		FUNC_GET_VALUE = "adas502v208_gr_get_val", 		$
		PRO_SET_VALUE = "adas502v208_gr_set_val")

  first_child = widget_base(parent)
  cwid = widget_base(first_child,/column)

		;**** Create upper base ****

  upperbase = widget_base(cwid,/row)

		;**** Create lower base ****

  lowerbase = widget_base(cwid,/row,space=20)

		;********************************
		;**** The Upper Area         ****
		;********************************

		;**** Graphics area ****

  leftupper = widget_base(upperbase,/column)
  rc = widget_label(leftupper,value=title)
  drawid = widget_draw(leftupper,/frame,xsize=xsize,ysize=ysize)


		;********************************
		;**** The lower Area         ****
		;********************************

		;**** Extra bitmapped button if specified ****

  if (bitflag eq 1) then begin
      read_X11_bitmap, bitbutton, bitmap1
      bitid = widget_button(lowerbase, value=bitmap1)
  endif else begin
      bitid = 0L
  endelse

		;**** Previous and Next only needed for multi-plots ****
  if multiplot ne 0 then begin
    previd = widget_button(lowerbase,value='Previous',FONT=font)
    nextid = widget_button(lowerbase,value='Next    ',FONT=font)
  end else begin
    previd = 0L
    nextid = 0L
  end

		;**** Print buttons if enabled ****
  if print eq 1 then begin
    printid = widget_button(lowerbase,value='Print   ',FONT=font)
		;**** Printall only needed for multi-plots ****
    if multiplot ne 0 then begin
      printallid = widget_button(lowerbase,value='Print All',FONT=font)
    end else begin
      printallid = 0L
    end
  end else begin
    printid = 0L
    printallid = 0L
  end

		;**** Done button ****
  recordid = widget_button(lowerbase,value='Record',FONT=font)
  doneid = widget_button(lowerbase,value='Done',FONT=font)

		;**** Message area ****
  if strtrim(grval.message) ne '' then begin
    message = grval.message
  end else begin
    message=' '
  end
  messid = widget_label(cwid,value=message,xsize=50,font=font)

		;**** Create state structure ****

  new_state =  {DRAWID:drawid, PREVID:previd, BITID:bitid, $
		NEXTID:nextid, PRINTID:printid, PRINTALLID:printallid, $
		DONEID:doneid, MESSID:messid, GRVAL:grval, $
                RECORDID:recordid }

		;**** Save initial state structure ****

  widget_control, first_child, set_uvalue = new_state

  RETURN, parent

END
