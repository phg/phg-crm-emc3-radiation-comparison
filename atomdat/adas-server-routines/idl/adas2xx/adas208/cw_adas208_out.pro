; Copyright (c) 1995 Strathclyde University .
; SCCS info: Module @(#)$Header: /home/adascvs/idl/adas2xx/adas208/cw_adas208_out.pro,v 1.5 2004/07/06 12:33:10 whitefor Exp $ Date $Date: 2004/07/06 12:33:10 $
;+
; PROJECT:
;       ADAS IBM MVS to DEC UNIX conversion
;
; NAME:
;	CW_ADAS208_OUT()
;
; PURPOSE:
;	Produces a widget for ADAS208 output options.
;
; EXPLANATION:
;	This function declares a compound widget consisting of the
;	graphical output selection widget cw_adas_gr_sel.pro, and three
;	output file widgets cw_adas_outfile.pro.  The three text output
;	files specified in this widget are for tabular (paper.txt)
;	output, countour passing file and METPOP passing file.  This
;	widget also includes a button for browsing the comments from
;	the input dataset, a 'Cancel' button and a 'Done' button.
;
;	The compound widgets cw_adas_gr_sel.pro and cw_adas_outfile.pro
;	included in this file are self managing.  This widget only
;	handles events from the 'Done' and 'Cancel' buttons.
;
;	This widget only generates events for the 'Done' and 'Cancel'
;	buttons.
;
; USE:
;	This widget is specific to ADAS208, see adas208_out.pro	for use.
;
; INPUTS:
;	PARENT	- Long integer; ID of parent widget.
;
;	DSNINC	- Name of input dataset for this application.
;
;       BITFILE - String; the path to the directory containing bitmaps
;                 for the 'escape to series menu' button.
;
; OPTIONAL INPUTS:
;	None.
;
; OUTPUTS:
;	The return value of this function is the ID of the declared widget.
;
; OPTIONAL OUTPUTS:
;	None.
;
; KEYWORD PARAMETERS:
;	GRPLIST	- A string array; A list of graphs from which to choose.
;		  For ADAS208 this is a list of electron temperatures.
;		  One element of the array for each electron temperature.
;
;	DEVLIST	- A string array;  Each element of the array is the
;		  name of a hardcopy graphics output device.  This
;		  list must mirror the DEVCODE list.  DEVLIST is the
;		  list which the user will see.
;		  e.g ['Post-Script','HP-PCL','HP-GL']
;
;	VALUE	- A structure which determines the initial settings of
;		  the output options widget.  The structure is made
;		  up of five parts.  The first four parts are the same as the
;		  value structure of one of the four main compound widgets
;		  included in this widget.  See cw_adas_gr_sel and
;		  cw_adas_outfile for more details. The fifth part is the
;		  passing-file directory. The default value is;
;
;		      { GRPOUT:0, GTIT1:'', $
;			GRPSCAL:0, $
;			XMIN:'',  XMAX:'',   $
;			YMIN:'',  YMAX:'',   $
;			HRDOUT:0, HARDNAME:'', $
;			GRPDEF:'', GRPFMESS:'', $
;			GRPSEL:-1, GRPRMESS:'', $
;			DEVSEL:-1, GRSELMESS:'', $
;			TEXOUT:0, TEXAPP:-1, $
;			TEXREP:0, TEXDSN:'', $
;			TEXDEF:'',TEXMES:'', $
;			CONOUT:0, CONAPP:-1, $
;			CONREP:0, CONDSN:'', $
;			CONDEF:'',CONMES:'', $
;			METOUT:0, METAPP:-1, $
;			METREP:0, METDSN:'', $
;			METDEF:'',METMES:'', $
;			PASSDIR:''   }
;
;		  For CW_ADAS_GR_SEL;
;			GRPOUT	   Integer; Activation button 1 on, 0 off
;			GTIT1	   String; Graph title
;			GRPSCAL	   Integer; Scaling activation 1 on, 0 off
;			XMIN	   String; x-axis minimum, string of number
;			XMAX	   String; x-axis maximum, string of number
;			YMIN	   String; y-axis minimum, string of number
;			YMAX	   String; y-axis maximum, string of number
;			HRDOUT	   Integer; Hard copy activ' 1 on, 0 off
;			HARDNAME   String; Hard copy output file name
;			GRPDEF	   String; Default output file name 
;			GRPFMESS   String; File name error message
;			GRPSEL	   Integer; index of selected graph in GRPLIST
;			GRPRMESS   String; Scaling ranges error message
;			DEVSEL	   Integer; index of selected device in DEVLIST
;			GRSELMESS  String; General error message
;
;		  For CW_ADAS_OUTFILE tabular output (paper.txt);
;			TEXOUT	Integer; Activation button 1 on, 0 off
;			TEXAPP	Integer; Append button 1 on, 0 off, -1 no button
;			TEXREP	Integer; Replace but' 1 on, 0 off, -1 no button
;			TEXDSN	String; Output file name
;			TEXDEF	String; Default file name
;			TEXMES	String; file name error message
;
;		  For CW_ADAS_OUTFILE contour passing file;
;			CONOUT  Integer; Activation button 1 on, 0 off
;			CONAPP	Integer; Append button 1 on, 0 off, -1 no button
;			CONREP	Integer; Replace but' 1 on, 0 off, -1 no button
;			CONDSN	String; Output file name
;			CONDEF	String; Default file name
;			CONMES	String; file name error message
;
;		  FOR CW_ADAS_OUTFILE Metpop passing file;
;			METOUT	Integer; Activation button 1 on, 0 off
;			METAPP	Integer; Append button 1 on, 0 off, -1 no button
;			METREP	Integer; Replace but' 1 on, 0 off, -1 no button
;			METDSN	String; Output file name
;			METDEF	String; Default file name
;			METMES	String; file name error message
;
;		  PASSDIR String; The directory in which to look for
;			  passing files
;
;	UVALUE	- A user value for this widget.
;
;	FONT	- String; the name of a font to be used for all text in
;		  this widget.  Default to system default font.
;
; CALLS:
;	CW_ADAS_DSBR	Input dataset name and comments browse button.
;	CW_ADAS_GR_SEL	Graphical output selection widget.
;	CW_ADAS_OUTFILE	Output file name entry widget.
;	See side effects for other related widget management routines.
;
; SIDE EFFECTS:
;       A COMMON BLOCK: CW_OUT208_BLK is used which is private to
;       this widget.
;
;	OUT208_GET_VAL()
;	OUT208_EVENT()
;
; CATEGORY:
;	Compound Widget
;
; WRITTEN:
;       David H.Brooks, Universiy of Strathclyde, date unknown
;
; MODIFIED:
;       1.0     David Brooks
;		Written using cw_adas205_out.pro v1.8 as a template
;	1.1    	William Osborn
;		Added documentation + put under SCCS control
;	1.2	William Osborn
;		Removed xsize=n keyword from widget_label command.
;		The dynlabel procedure does not override these
;		assignments and they were taken as being values in pixels
;		on some machines.
;       1.3     William Osborn / Tim Hammond
;               Added /row keyword to cw_adas_dsbr
;	1.4	Richard Martin
;		Removed obsolete cw_savesate/ladstate statements.
;	1.5	Martin O'Mullane
;		Problems with get_value of passdir part of the
;               widget returning a strarr of size 1 rather than a string.
;               Problem introduced in IDL 5.5. Also changed the way of
;               interacting with the PEC/SXB/GCR buttons - it was not
;               possible to de-select them once pressed.
;
; VERSION:
;	1.0	????????
;	1.1	10-05-96
;	1.2	01-08-96
;	1.3	10-10-96
;	1.4	30-01-02
;	1.5	10-04-02
;
;-

;-----------------------------------------------------------------------------

FUNCTION out208_get_val, id


                ;**** Return to caller on error ****
  ON_ERROR, 2

                ;**** Retrieve the state ****

  first_child = widget_info(id,/child)
  widget_control, first_child, get_uvalue=state, /no_copy

		;**** Get graphical output settings ****
  widget_control,state.grpid,get_value=grselval

		;**** Get text output settings ****
  widget_control,state.paperid,get_value=papos

		;**** Get the passing file directory ****
  widget_control, state.dirid, get_value=passdir
  passdir = passdir[0]
  
  widget_control, state.outpid,get_value=button
  
  os = { $
		GRPOUT:grselval.outbut, GTIT1:grselval.gtit1, $
		GRPSCAL:grselval.scalbut, $
		XMIN:grselval.xmin,     XMAX:grselval.xmax,   $
		YMIN:grselval.ymin,     YMAX:grselval.ymax,   $
		HRDOUT:grselval.hrdout, HARDNAME:grselval.hardname, $
		GRPDEF:grselval.grpdef, GRPFMESS:grselval.grpfmess, $
		GRPSEL:grselval.grpsel, GRPRMESS:grselval.grprmess, $
		DEVSEL:grselval.devsel, GRSELMESS:grselval.grselmess, $
		TEXOUT:papos.outbut,  TEXAPP:papos.appbut, $
		TEXREP:papos.repbut,  TEXDSN:papos.filename, $
		TEXDEF:papos.defname, TEXMES:papos.message, $
		CONOUT:button[0], METOUT:button[1], $
                GCROUT:button[2], MESS:state.os.mess, $
		PASSDIR:passdir}

  widget_control, first_child, set_uvalue=state, /no_copy

  RETURN, os 

END

;-----------------------------------------------------------------------------

FUNCTION out208_event, event


                ;**** Base ID of compound widget ****
  parent=event.handler

                ;**** Retrieve the state ****

  first_child = widget_info(parent,/child)
  widget_control, first_child, get_uvalue=state

		;*********************************
		;**** Clear previous messages ****
		;*********************************
  widget_control,state.messid,set_value=' '

                ;************************
                ;**** Process events ****
                ;************************
  CASE event.id OF

		;***********************
		;**** Cancel button ****
		;***********************
    state.cancelid: new_event = {ID:parent, TOP:event.top, $
				HANDLER:0L, ACTION:'Cancel'}

		;*********************
		;**** Done button ****
		;*********************
    state.doneid: begin

		;***************************************
		;**** Check for errors in the input ****
		;***************************************
	  error = 0
	  message = '**** Error in output settings ****'

		;**** Get a copy of the widget value ****
	  widget_control,event.handler,get_value=os

		;**** Check for widget error messages ****
	  if os.grpout eq 1 and strtrim(os.grpfmess) ne '' then error=1
	  if os.grpout eq 1 and os.grpscal eq 1 and $
				strtrim(os.grprmess) ne '' then error=1
	  if os.grpout eq 1 and strtrim(os.grselmess) ne '' then error=1
	  if os.texout eq 1 and strtrim(os.texmes) ne '' then error=1

	  	;**** Check that directory exists ****
	  file_acc, os.passdir, exist, read, write, execute, filetype
          if exist eq 1 and filetype eq 'd' then begin
	   	;**** Add a / if there isn't one ****
	    if strmid(os.passdir,strlen(os.passdir)-1,1) ne '/' then begin
	      os.passdir=os.passdir+'/'
	    endif
	    widget_control, state.dirid, set_value=os.passdir
	  endif else begin
	    error = 1
	    message = 'Passing file directory is not a directory'
	    widget_control, state.dirid, /input_focus
	  endelse

	  if error eq 1 then begin
	    widget_control,state.messid,set_value= message
	    new_event = 0L
	  end else begin
	    new_event = {ID:parent, TOP:event.top, HANDLER:0L, ACTION:'Done'}
	  end
    end
                ;**** Menu button ****

    state.outid: begin
          new_event = {ID:parent, TOP:event.top, HANDLER:0L,       $
                       ACTION:'Menu'}
    end


    ELSE: begin 
          new_event = 0L
    end

  ENDCASE

  RETURN, new_event
END

;-----------------------------------------------------------------------------

FUNCTION cw_adas208_out, parent, dsninc, bitfile, GRPLIST=grplist,	$
		DEVLIST=devlist, VALUE=value, UVALUE=uvalue, FONT=font


  IF (N_PARAMS() EQ 0) THEN MESSAGE, 'Must specify parent for cw_adas208_out'
  ON_ERROR, 2					;return to caller

		;**** Set defaults for keywords ****
  IF NOT (KEYWORD_SET(grplist)) THEN grplist = ''
  IF NOT (KEYWORD_SET(devlist)) THEN devlist = ''
  IF NOT (KEYWORD_SET(uvalue)) THEN uvalue = 0
  IF NOT (KEYWORD_SET(font)) THEN font = ''
  IF NOT (KEYWORD_SET(value)) THEN begin
	os = {out208_set, $
			GRPOUT:0, GTIT1:'', $
			GRPSCAL:0, $
			XMIN:'',  XMAX:'',   $
			YMIN:'',  YMAX:'',   $
			HRDOUT:0, HARDNAME:'', $
			GRPDEF:'', GRPFMESS:'', $
			GRPSEL:-1, GRPRMESS:'', $
			DEVSEL:-1, GRSELMESS:'', $
			TEXOUT:0, TEXAPP:-1, $
			TEXREP:0, TEXDSN:'', $
                        TEXDEF:'',TEXMES:'', $
			CONOUT:0, METOUT:0,  $
                        GCROUT:0, MESS:'',   $
			PASSDIR:'' }
  END ELSE BEGIN
	os = {out208_set, $
			GRPOUT:value.grpout, GTIT1:value.gtit1, $
			GRPSCAL:value.grpscal, $
			XMIN:value.xmin,     XMAX:value.xmax,   $
			YMIN:value.ymin,     YMAX:value.ymax,   $
			HRDOUT:value.hrdout, HARDNAME:value.hardname, $
			GRPDEF:value.grpdef, GRPFMESS:value.grpfmess, $
			GRPSEL:value.grpsel, GRPRMESS:value.grprmess, $
			DEVSEL:value.devsel, GRSELMESS:value.grselmess, $
			TEXOUT:value.texout, TEXAPP:value.texapp, $
			TEXREP:value.texrep, TEXDSN:value.texdsn, $
			TEXDEF:value.texdef, TEXMES:value.texmes, $
			CONOUT:value.conout, METOUT:value.metout, $
			GCROUT:value.gcrout, MESS:value.mess,	$
			PASSDIR:value.passdir }
  END

		;**********************************************
		;**** Create the 208 Output options widget ****
		;**********************************************

		;**** create base widget ****
  cwid = widget_base( parent, UVALUE = uvalue, $
			EVENT_FUNC = "out208_event", $
			FUNC_GET_VALUE = "out208_get_val", $
			/COLUMN)

		;**** Create base to hold the value of state ****

  first_child = widget_base(cwid)

		;**** Add dataset name and browse button ****
  rc = cw_adas_dsbr(cwid,dsninc,font=font, /row)

		;**** Widget for graphics selection ****
		;**** Note change in names for GRPOUT and GRPSCAL ****
  grselval = {  OUTBUT:os.grpout, GTIT1:os.gtit1, SCALBUT:os.grpscal, $
		XMIN:os.xmin, XMAX:os.xmax, $
		YMIN:os.ymin, YMAX:os.ymax, $
		HRDOUT:os.hrdout, HARDNAME:os.hardname, $
		GRPDEF:os.grpdef, GRPFMESS:os.grpfmess, $
		GRPSEL:os.grpsel, GRPRMESS:os.grprmess, $
		DEVSEL:os.devsel, GRSELMESS:os.grselmess }
  base = widget_base(cwid,/row,/frame)
  grpid = cw_adas_gr_sel(base, OUTPUT='Graphical Output', GRPLIST=grplist, $
                        LISTTITLE='Graph Temperature', $
			DEVLIST=devlist, $
                        VALUE=grselval, FONT=font)

		;**** Widget for text output ****
  outfval = { OUTBUT:os.texout, APPBUT:os.texapp, REPBUT:os.texrep, $
	      FILENAME:os.texdsn, DEFNAME:os.texdef, MESSAGE:os.texmes }
  base = widget_base(cwid,/row,/frame)
  paperid = cw_adas_outfile(base, OUTPUT='Text Output', $
                        VALUE=outfval, FONT=font)

		;**** Buttons for selecting output files ****
  base = widget_base(cwid,/row,/frame)
  col = widget_base(base,/column)
  base1 = widget_base(col,/row)
  label = widget_label(base1,value= 'Output passing files to directory',$
                      font=font)
  dirid = widget_text(base1,value=os.passdir,/editable,font=font,	$
		      xsize=strlen(os.passdir))

; Passing file choices

  base2m  = widget_base(col,/row)
  buttons = ['PEC    ', 'SXB    ', 'GCR    ']
  outpID  = cw_bgroup(base2m, buttons, nonexclusive=1,row=1, $
                      label_left = 'Passing files: ', $
                      font=font, /no_release)
                         
  
  base3 = widget_base(col,/row)
  if strtrim(os.mess,2) eq '' then begin
    message = ' '
  end else begin
    message = os.mess
  end
  messageid  = widget_label(base3, value = message, font=font)

                ;**** work out settings ****
  button_set = [os.conout, os.metout, os.gcrout]
  widget_control, outpid, set_value=button_set

		;**** Error message ****
  messid = widget_label(cwid,value=' ',font=font)

		;**** add the exit buttons ****
  base = widget_base(cwid,/row)
  menufile = bitfile + '/menu.bmp'
  read_X11_bitmap, menufile, bitmap1
  outid = widget_button(base, value=bitmap1)       ;menu button
  cancelid = widget_button(base,value='Cancel',font=font)
  doneid = widget_button(base,value='Done',font=font)
  
		;**** create a state structure for the pop-up ****
		;**** window.                                 ****
  new_state = { GRPID:grpid, PAPERID:paperid,  $
                OUTPID:outpid, OS:os, MESSAGEID:messageid, DIRID:dirid,       $
		CANCELID:cancelid, DONEID:doneid, MESSID:messid, OUTID:outid }

                ;**** Save initial state structure ****

  widget_control, first_child, set_uvalue=new_state, /no_copy		    

  RETURN, cwid

END

