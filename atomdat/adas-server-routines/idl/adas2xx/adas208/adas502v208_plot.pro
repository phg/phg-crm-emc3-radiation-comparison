; Copyright (c) 1995 Strathclyde University .
; SCCS info: Module @(#)$Header: /home/adascvs/idl/adas2xx/adas208/adas502v208_plot.pro,v 1.3 2004/07/06 10:54:00 whitefor Exp $ Date $Date: 2004/07/06 10:54:00 $
;+
; PROJECT:
;       ADAS IBM MVS to UNIX conversion
;
; NAME:
;	ADAS502V208_PLOT
;
; PURPOSE:
;	Generates ADAS502V208 graphical output.
;
; EXPLANATION:
;	This routine creates a window for the display of graphical
;	output; a separate routine PLOT502 actually plots a
;	graph.
;
; USE:
;	This routine is specific to ADAS502V208, see e2b8outg.pro for
;	example use.
;
; INPUTS:
;	(Most of these inputs map exactly onto variables of the same
;	 name in the FORTRAN code.)
;
;
;	DSFULL  - String; Name of data file 
;
;	TITLE   - String array; titles to be placed above graph
;
;	TITLX   - String; user supplied comment appended to end of title
;
;	TITLM   - String; Information about minimax fitting if selected.
;
;	UTITLE  - String; Optional comment by user
;
;	DATE	- String; Date of graph production
;
;	ESYM 	- String; Symbol of emitting ion
;
;	IZ0	- Integer; Nuclear charge of ionising ion
;
;	IZ1	- Integer; Charge of ionising ion + 1
;
;       BWNO	- String; Ionisation potential (cm-1), number as string.
;
;	CIION 	- String; Initial ion
;
;	CICODE	- String; Initial state metastable index
;
;	CFION  	- String; Final ion
;
;	CFCODE	- String; Final state metastable index
;
;	ITVAL	- Integer; number of user entered temperatures
;
;	TEVA	- Double array; User entered electron temperatures, eV
;
;
;	SZDA	- Double array; Spline interpolated or extrapolated 
;				ionization coefficients for user entered 
;				temperatures.
;
;	SZDM	- Double array; Minimax fit values of ionization coeffts.  
;				at 'tfitm()'
;
;	TFITM	- Double array; Selected temperature values for minmiax fit.
;
;	LDEF1	- Integer; 0 - use user entered graph scales
;			   1 - use default axes scaling
;
;	XMIN    - String; Lower limit for x-axis of graph, number as string.
;
;	XMAX    - String; Upper limit for x-axis of graph, number as string.
;
;	YMIN    - String; Lower limit for y-axis of graph, number as string.
;
;	YMAX    - String; Upper limit for y-axis of graph, number as string.
;
;	LFSEL	- Integer; 0 - No minimax fitting was selected 
;			   1 - Mimimax fitting was selected
;
;	NMX	- Integer; Number of temperatures used for minimax fit
;
;	STRG	- String array; Information regarding current data selected
;
;	HEAD1	- String; header information for ionising ion
;
;	HEAD2	- String; Header information for initial state 
;
;	HEAD3   - String; Header information for final state
;
;	HEAD4   - String; Header information for temperature list
;
;	HRDOUT - Integer; 1 if hardcopy output activated, 0 if not.
;
;	HARDNAME- String; Filename for harcopy output.
;
;	DEVICE	- String; IDL name of hardcopy output device.
;
;	HEADER	- String; ADAS version number header to include in graph.
;
;       BITFILE - String; the path to the directory containing bitmaps
;                 for the 'escape to series menu' button.
;
;	GOMENU	- Integer; 1=>exit to ADAS208
;			   0=>don't
;
;	RECORD	- Integer; 1=> this graph has been recorded
;			   0=> it hasn't yet
;
; OPTIONAL INPUTS:
;	None.
;
; OUTPUTS:
;	None.
;
; OPTIONAL OUTPUTS:
;	None.
;
; KEYWORD PARAMETERS:
;	FONT	- String; The name of a font to use for text in the
;		  graphical output widget.
;
; CALLS:
;	CW_ADAS_GRAPH	Graphical output widget.
;	PLOT502		Make one plot to an output device for 502.
;	XMANAGER
;
; SIDE EFFECTS:
;	This routine uses a common block to maintain its state PLOT502V208_BLK.
;
;	One other routine is included in this file;
;	ADAS502V208_PLOT_EV	Called via XMANAGER during widget management.
;
; CATEGORY:
;	Adas system.
;
; WRITTEN:
;       David H.Brooks, Universiy of Strathclyde, date unknown
;
; MODIFIED:
;       1.0     David Brooks
;		Written using adas502_plot.pro v1.1 as a template
;	1.1    	William Osborn
;		Added documentation + put under SCCS control
;	1.2	Tim Hammond
;		Added calls to localize.pro before and after every call to
;		plot502 in order to force the system to think it is
;		doing a 502 plot rather than a 208 one in order to get
;		the labels in the correct place. This is reset to 208
;		after each call so the 208 plot itself will still appear OK.
;       1.3     William Osborn
;               S.C.C.S. mistake
;
; VERSION:
;	1.0	????????
;	1.1	10-05-96
;	1.2	05-08-96
;	1.3	14-10-96
;
;-
;----------------------------------------------------------------------------

PRO adas502v208_plot_ev, event

    COMMON plot502v208_blk, data, action, win, plotdev, plotfile, 	$
			    fileopen, gomenu, recordcom
    COMMON Global_adas_num, mainchoice, subchoice

    newplot = 0
    print = 0
    done = 0
		;****************************************
		;**** Set graph and device requested ****
		;****************************************

    CASE event.action OF

	'print'	   : begin
			newplot = 1
			print = 1
		     end

	'done'	   : begin
			if fileopen eq 1 then begin
			  set_plot, plotdev
			  device, /close_file
			endif
			set_plot,'X'
			widget_control, event.top, /destroy
			done = 1
		     end
	'record'   : begin
			if fileopen eq 1 then begin
			  set_plot, plotdev
			  device, /close_file
			endif
			set_plot,'X'
			widget_control, event.top, /destroy
                        done = 1
			recordcom = 1
		     end

	'bitbutton' : begin
            if fileopen eq 1 then begin
                set_plot, plotdev
                device, /close_file
            endif
            set_plot,'X'
            widget_control, event.top, /destroy
            done = 1
            gomenu = 1
        end

    END

		;*******************************
		;**** Make requested plot/s ****
		;*******************************

    if done eq 0 then begin
		;**** Set graphics device ****
        if print eq 1 then begin
            set_plot, plotdev
            if fileopen eq 0 then begin
                fileopen = 1
                device, filename = plotfile
	        device, /landscape
            endif
        endif else begin
            set_plot, 'X'
            wset, win
        endelse

		;**** Draw graphics ****
		;**** Force localize.pro to think this is 502 running ****

        mainchoice=4
        subchoice=1
  	localize
        plot502, data.x , data.y, data.itval, data.nplots, data.nmx, 	$
	         data.title, data.xtitle, data.ytitle, 			$
	         data.strg, data.head1, data.head2, data.head3,		$
                 data.head4, data.teva, data.ldef1, data.xmin, 		$
                 data.xmax, data.ymin, data.ymax 
		;**** Reset localize.pro to think it is 208 running ****

        mainchoice=1
        subchoice=7
  	localize

	if print eq 1 then begin
	    message = 'Plot written to print file.'
	    grval = {WIN:0, MESSAGE:message}
	    widget_control, event.id, set_value=grval
	endif
    endif

END

;----------------------------------------------------------------------------

PRO adas502v208_plot, dsfull, title, titlx, titlm, utitle, date, esym, 	$
                  iz0, iz1, bwno, ciion, cicode, cfion, cfcode, itval, 	$
                  teva, szda, szdm, tfitm, ldef1, xmin, xmax, 		$
                  ymin, ymax, lfsel, nmx, strg, head1, head2, 		$
                  head3, head4, hrdout, hardname, device, header, 	$
                  bitfile, gomenu, record, FONT=font

    COMMON plot502v208_blk, data, action, win, plotdev, plotfile, 	$
                        fileopen, gomenucom, recordcom
    COMMON Global_adas_num, mainchoice, subchoice

		;**** Copy input values to common ****

    plotdev = device
    plotfile = hardname
    fileopen = 0
    gomenucom = gomenu
    recordcom = record

		;************************************
		;**** Create general graph title ****
		;************************************

    title = strarr(5)
    type = 'ELECTRON TEMPERATURE '
    title(0) = "IONISATION COEFFICIENT VS " + type  
    if ( strtrim(strcompress(utitle),2)  ne '' ) then begin
        title(0) = title(0) + ': ' + strupcase(strtrim(utitle,2))
    endif
    title(1) = 'ADAS    :' + header
    title(2) = 'FILE     :' + titlx
    if (lfsel eq 1) then begin
        title(3)  = 'MINIMAX : ' + strupcase(titlm)
    endif
    title(4) = 'KEY     : (CROSSES - INPUT DATA) (FULL LINE - SPLINE FIT)'
    if (lfsel eq 1) then  title(4) = title(4) + "  (DASH LINE - MINIMAX) "

		;********************************
		;*** Create graph annotation ****
		;********************************        
    strg = strtrim(strg, 2)
    strg(0) = strg(0) +  ' ' + esym
    strg(1) = strg(1) +  strcompress(string(iz0)) 
    strg(2) = strg(2) +  strcompress(bwno)
    strg(6) = strg(3) +  ' ' + cfion
    strg(7) = strg(4) +  ' ' + cfcode 
    strg(3) = strg(3) +  ' ' + ciion
    strg(4) = strg(4) +  ' ' + cicode

		;*********************************
		;**** Set up Y data for plots ****
		;*********************************

    if (lfsel eq 1) then ydim = nmx else ydim = itval
    y = make_array(2, ydim, /float)
    valid_data = where((szda gt 1e-37) and (szda lt 1e+37))
    if (valid_data(0) ge 0) then begin
        y(0, valid_data) = szda(valid_data)
        if (lfsel eq 1) then begin
            valid_data = where((szdm gt 1e-37) and (szdm lt 1e+37))
	    if (valid_data(0) ge 0) then begin
                y(1, valid_data) = szdm(valid_data)
            endif else begin
                print, "ADAS502 : unable to plot polynomial fit data"
            endelse
        endif
    endif else begin
        print, "ADAS502 : unable to plot spline fit data"
    endelse
    ytitle = "IONISATION COEFFICIENT  (cm!e3!n s!e-1!n)"

		;**************************************
		;**** Set up x axis and x axis title ***
		;**************************************

    if (lfsel eq 1) then xdim = nmx else xdim = itval
    x = fltarr(2, xdim)
    if (lfsel eq 0) then begin
        x(0,0:itval-1) = teva
        nplots = 1
    endif else begin
        x(0,0:itval-1) = teva
        x(1,*) = tfitm
        nplots = 2
    endelse
    xtitle = type +  " (eV) "

  		;******************************************
		;*** if desired set up user axis scales ***
		;******************************************

    if (ldef1 eq 0) then begin
        xmin = min(x, max=xmax)
        xmin = xmin*0.9
        xmax = xmax*1.1
        ymin = min(y, max=ymax)
        ymin = ymin*0.9
        ymax = ymax*1.1
    endif



		;*************************************
		;**** Create graph display widget ****
		;*************************************

    graphid = widget_base(TITLE='ADAS502(208) GRAPHICAL OUTPUT', 	$
                          XOFFSET=1,YOFFSET=1)
    device, get_screen_size = scrsz
    xwidth=scrsz(0)*0.75
    yheight=scrsz(1)*0.75
    multiplot=0
    bitval = bitfile + '/menu.bmp'
    cwid = cw_adas502v208_graph(graphid, print=hrdout, FONT=font,       $
                         xsize=xwidth, ysize=yheight, 			$
                         multiplot=multiplot, bitbutton=bitval)

                ;**** Realize the new widget ****

    widget_control, graphid, /realize

		;**** Get the id of the graphics area ****

    widget_control, cwid, get_value=grval
    win = grval.win

		;*******************************************
		;**** Put the graphing data into common ****
		;*******************************************

    data = { 	Y		:	y,				$
		X		:	x,   				$
		ITVAL		:	itval,    			$
		NPLOTS		:	nplots,  			$
		NMX		:	nmx, 				$
                TITLE		:	title,   			$
		XTITLE		:	xtitle,  			$
		YTITLE		:	ytitle,           		$
		STRG		:	strg,     			$
		HEAD1		:	head1,    			$
		HEAD2		:	head2,             		$
                HEAD3		:	head3,   			$
		HEAD4		:	head4,                  	$
		TEVA		:	teva,     			$
		LDEF1		:	ldef1,                    	$
		XMIN		:	xmin,     			$
		XMAX		:	xmax,      			$
		YMIN		:	ymin,				$
		YMAX		:	ymax 				}  
 
    wset, win

		;**** Force localize.pro to think this is 502 running ****

    mainchoice=4
    subchoice=1
    localize
    plot502, x, y, itval, nplots, nmx, title, xtitle, ytitle, 		$
	     strg, head1, head2, head3, head4, teva,          		$
	     ldef1, xmin, xmax, ymin, ymax 

		;**** Reset localize.pro to think it is 208 running ****

    mainchoice=1
    subchoice=7
    localize

		;***************************
                ;**** make widget modal ****
		;***************************

    xmanager, 'adas502_plot', graphid, 					$
              event_handler='adas502v208_plot_ev', /modal, /just_reg
    gomenu = gomenucom
    record = recordcom

END
