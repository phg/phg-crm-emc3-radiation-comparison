; Copyright (c) 1995 Strathclyde University .
; SCCS info: Module @(#)$Header: /home/adascvs/idl/adas2xx/adas208/b8setp.pro,v 1.4 2018/10/03 09:27:05 mog Exp $ Date $Date: 2018/10/03 09:27:05 $
;+
; PROJECT:
;       ADAS IBM MVS to DEC UNIX conversion
;
; NAME:
;	B8SETP
;
; PURPOSE:
;	IDL communications with ADAS205 FORTRAN process via pipe.
;
; EXPLANATION:
;	This IDL routine reads data from the ADAS208 FORTRAN subroutine
;	B8SETP via a UNIX pipe.  In the original IBM version the FORTRAN
;	subroutine B8SETP put information needed by the ISPF interface
;	into the ISPF pool area for later use.  Now the same information
;	is written to IDL for use in its user unterface.
;
; USE:
;	The use of this routine is specific to ADAS208.  See
;	adas208.pro for an example.
;
; INPUTS:
;	PIPE	- The IDL unit number of the bi-directional pipe to the
;		  ADAS208 FORTRAN process.
;
; OPTIONAL INPUTS:
;	None
;
; OUTPUTS:
;	SZ0	- String, nuclear charge read.
;
;	SZ	- String, recombined ion charge read.
;
;	SCNTE	- Number of electron impact transitions.
;
;	SIL	- Number of energy levels.
;
;	LFPOOL	- Integer, number of level strings. i.e dimension of strga
;		  (In IBM version number of strings sent to function pool)
;
;	STRGA	- String array, level designations.
;
;	NDMET	- Integer; number of metastables
;
;	NPL	- Integer; number of metastables in input file (?)(see adas208.f)
;
;	STRGMI	- String; info string (see b8setp.f)
;
;	STRGMF	- String; info string (see b8setp.f)
;
; OPTIONAL OUTPUTS:
;	None
;
; KEYWORD PARAMETERS:
;	None
;
; CALLS:
;	None
;
; SIDE EFFECTS:
;	Reads data from a pipe.
;
; CATEGORY:
;	Adas system.
;
; WRITTEN:
;       David H.Brooks, Universiy of Strathclyde, date unknown
;
; MODIFIED:
;       Version 1.0     David Brooks
;			Written using bxsetp.pro v1.5 as a template
;	Version 1.1    	William Osborn
;			Added documentation + put under SCCS control
;	Version 1.2    	Hugh Summers
;			Added acquisition of truth table for ionisation
;                       rate data availability from adf04 file
;	Version 1.3    	Martin O'Mullane
;			Acquire ndwvl value from fortran.
;	Version 1.4    	Martin O'Mullane
;			Acquire ndsel value from fortran.
;
; VERSION:
;	1.0	????????
;	1.1	10-05-96
;	1.2	14-09-99
;	1.3	18-03-13
;	1.4	27-09-18
;
;-
;-----------------------------------------------------------------------------



PRO b8setp, pipe, sz0, sz, scnte, sil, lfpool, strga, ndmet, $
            ndwvl, ndsel, npl, strgmi, strgmf, ndlev, iss04a

		;**********************************
		;**** Initialise new variables ****
		;**********************************
  sz0 = ''
  sz = ''
  scnte = ''
  sil = ''
  lfpool = 0
  input = ''
  idum = 0
  jdum = 0
  npl = 0
  ndmet = 0
  ndlev = 0 
  ndwvl = 0
  ndsel = 0

		;********************************
		;**** Read data from fortran ****
		;********************************
  readf, pipe, input
  sz0 = input
  readf, pipe, input
  sz = input
  readf, pipe, input
  scnte = input
  readf, pipe, input
  sil = input

  readf, pipe, idum
  lfpool = idum
  strga  = strarr(lfpool)
  strgmi = strarr(lfpool)

  for i = 0, lfpool-1 do begin
    readf, pipe, input
    strga(i) = input
  end
  for i = 0, lfpool-1 do begin
    readf, pipe, input
    strgmi(i) = input
  end

;  readf, pipe, input
;  ndmet = input
  readf, pipe, idum
  ndmet = idum
  
  readf, pipe, idum
  ndwvl = idum
  
  readf, pipe, idum
  ndsel = idum
  
  readf, pipe, jdum
  npl = jdum
  strgmf = strarr(ndmet)

  for i = 0, ndmet-1 do begin
    readf, pipe, input
    strgmf(i) = input
  end
  
  readf, pipe, idum
  ndlev = idum
  iss04a=intarr(ndlev,ndmet)
  for i = 0, ndlev-1 do begin
    for j = 0, ndmet-1 do begin
       readf, pipe, idum
       iss04a(i,j) = idum
    end
  end
 
END
