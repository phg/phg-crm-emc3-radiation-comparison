; Copyright (c) 2002, Strathclyde University.
;----------------------------------------------------------------------
;+
; PROJECT    :  ADAS
;
; NAME       :  r8necip
;
; PURPOSE    :  Calculates ECIP approximation for ionisation rate.
;
; ARGUMENTS  :  All output arguments will be defined appropriately.
;               Inputs will be converted to correct type, if possible,
;               internally without changing calling type.
;                   result = rsnecip(iz=iz, xi=xi, zeta=zeta)
;                   
;               r8necip, iz     = iz,    $
;                        xi     = xi,    $   
;                        zeta   = zeta,  $ 
;                        te     = te,    $   
;                        alfred = alfred
;              
;
;               NAME      I/O    TYPE    DETAILS
; REQUIRED   :  iz         I     integer recombined ion charge
;               xi         I     double  effective ionisation potential (Ryd)
;               zeta       I     double  effective number of equivalent electrons 
;               te         I     double  array of electron temperature (K)
; OPTIONAL      alfred     O     double  scaled 3-body recombination coefficient
;
; 
; KEYWORDS      None
; 
;
; NOTES      :  Calls the fortran code.
;               Units of result are cm^3/s
;
;
; AUTHOR     :  Martin O'Mullane
; 
; DATE       :  08-04-2002 
; 
;
; MODIFIED:
;         1.1       Martin O'Mullane
;              	        - First version.
;	  1.2	    Richard Martin
;			- Removed underscore in CALL_EXTERNAL statement.
;	  1.3	    Allan Whiteford
;			- Changed wrapper path to be just ADASFORT.
;         1.3       Martin O'Mullane
;              	        - Use long() rather than fix() to determine
;                         iz_in which is passed to fortran code.
;
; VERSION:
;         1.1      08-04-2002 
;	  1.2	   17-03-2003
;	  1.3	   10-08-2004
;	  1.4	   26-04-2010
;-
;----------------------------------------------------------------------

FUNCTION r8necip, iz     = iz,    $
                  xi     = xi,    $   
                  zeta   = zeta,  $ 
                  te     = te,    $   
                  alfred = alfred  


fortdir = getenv('ADASFORT')
fortdir = fortdir

if n_elements(iz) EQ 0 OR   $             
   n_elements(xi) EQ 0 OR   $             
   n_elements(zeta) EQ 0 OR $             
   n_elements(te) EQ 0      $
   then message, 'essential parameters missing'       

; inputs

n_tek = n_elements(te)

iz_in   = long(iz)
xi_in   = double(xi)
zeta_in = double(zeta)
te_in   = double(te)

; results

alfred_out = dblarr(n_tek) 
rate       = dblarr(n_tek) 


dummy = CALL_EXTERNAL(fortdir+'/r8necip_if.so','r8necip_if', $
                      n_tek, iz_in, xi_in, zeta_in, te_in, rate, alfred_out)

if arg_present(alfred) then alfred = alfred_out

return, rate

END
