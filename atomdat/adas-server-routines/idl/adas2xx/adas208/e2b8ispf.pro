; Copyright (c) 1995, Strathclyde University.
; SCCS info: Module @(#)$Header: /home/adascvs/idl/adas2xx/adas208/e2b8ispf.pro,v 1.2 2004/07/06 13:37:26 whitefor Exp $ Date $Date: 2004/07/06 13:37:26 $
;+
; PROJECT:
;       ADAS IBM MVS to UNIX conversion
;
; NAME:
;	E2B8ISPF
;
; PURPOSE:
;	IDL user interface and communications with ADAS502V208 FORTRAN
;	process via pipe.
;
; EXPLANATION:
;	The routine begins by reading information from the ADAS502V208
;	FORTRAN process via a UNIX pipe.  Then part of the ADAS502V208
;	IDL user interface is invoked to determine how the user
;	wishes to process the input dataset.  When the user's
;	interactions are complete the information gathered with
;	the user interface is written back to the FORTRAN process
;	via the pipe.  Communications are to the FORTRAN subroutine
;	E2ISPF.
;
; USE:
;	The use of this routine is specific to ADAS502V208, see adas502V208.pro.
;
; INPUTS:
;	PIPE	- The IDL unit number of the bi-directional pipe to the
;		  ADAS502V208 FORTRAN process.
;
;	LPEND	- Integer, 0 if the user pressed the 'Done' button
;		  or 1 if the user pressed 'Cancel'.  Maps directly onto
;		  the logical variable LPEND in ADAS502V208 FORTRAN.
;
;	PROCVAL	- A structure which determines the initial settings of
;		  the processing options widget.  The initial value is
;		  set in adas502v208.pro.  If adas502v208.pro passes a blank 
;		  dummy structure of the form {NEW:-1} into PROCVAL then
;		  PROCVAL is reset to a default structure.
;
;		  The PROCVAL structure is;
;   			 procval = {			$
;					new   : 0 ,             $
;					title : '',		$
;					ibsel : 0 ,     	$
;					imsel : 0 ,     	$
;                			ifout : 1, 		$
;					itval : 0,              $
;					tine  : temp_arr,	$
;					lfsel : 0,		$
;					tolval: 5,              $
;					hlist : hlist_arr
;                			losel : 1               $
;
;		  See cw_adas502v208_proc.pro for a full description of this
;		  structure.
;	
;
;	DSFULL	- String; The full system file name of the input 
;		  dataset selected by the user for processing.
;	BITFILE - String; the path to the dirctory containing bitmaps
;		  for the 'escape to series menu' button.
;
;	NBSEL	- Integer : Number of data blocks accepted and read in
;
;	MSIZE 	- Integer; size of the metastable list
;
;	STRGMM 	- String array; info strings
;
;	IMETR   - Integer array; metastable indices
;
;	SZ	- String; Ion charge
;
;	SELEM	- String; element symbol
;
;	NDMET	- Integer; Number of metastables
;
;	TINE	- Double array; input temperatures
;
;	HLIST_FLAG - Flag whether hlist should be reset
;
;	ITOUT   - Integer; temp unit flag - see ifout
;
; OPTIONAL INPUTS:
;	None
;
; OUTPUTS:
;       LPEND   - Integer, 0 if the user pressed the 'Done' button
;                 or 1 if the user pressed 'Cancel'.  Maps directly onto
;                 the logical variable LPEND in ADAS502 FORTRAN.
;	PROCVAL	- On output the structure records the final settings of
;		  the processing selections widget if the user pressed the
;		  'Done' button, otherwise it is not changed from input.
;	GOMENU	- Int; flag - set to 1 if user has selected 'escape direct
;		  to series menu , otherwise 0
;
; OPTIONAL OUTPUTS:
;	None
;
; KEYWORD PARAMETERS:
;	FONT_LARGE - The name of a larger font.
;
;	FONT_SMALL - The name of a smaller font.
;
;	EDIT_FONTS - A structure of two fonts used in the table editor
;		     adas_edtab.pro, {font_norm:'',font_input:''}.
;
; CALLS:
;	ADAS502V208_PROC Invoke the IDL interface for ADAS502V208 data
;		 	 processing options/input.
;
; SIDE EFFECTS:
;	Two way communications with ADAS502V208 FORTRAN via the
;	bi-directional UNIX pipe.
;
; CATEGORY:
;	Adas system.
;
; WRITTEN:
;       David H.Brooks, Universiy of Strathclyde, date unknown
;
; MODIFIED:
;       1.0	     David Brooks
;			Written using e2ispf.pro v1.4 as a template
;                       Added imsel to procval.
;	1.1	    	William Osborn
;			Added documentation + put under SCCS control
;	1.2		William Osborn
;			Set losel to be 1 always, rather than 0, since spline
;			fitting always needed with temps. from 208
;
; VERSION:
;	1.0	????????
;	1.1	10-05-96
;	1.2	13-05-96
;
;-
;-----------------------------------------------------------------------------

PRO e2b8ispf, pipe, lpend, procval, dsfull, nbsel, gomenu, bitfile,	$
            msize, strgmm,  imetr, sz, selem, tine, hlist_flag, ndmet,  $
            itout, FONT_LARGE=font_large, FONT_SMALL=font_small, 	$
	    EDIT_FONTS=edit_fonts

                ;**** Set defaults for keywords ****

    IF NOT (KEYWORD_SET(font_large)) THEN font_large = ''
    IF NOT (KEYWORD_SET(font_small)) THEN font_small = ''
    IF NOT (KEYWORD_SET(edit_fonts)) THEN 				$
    edit_fonts = {font_norm:'',font_input:''}

		;********************************************
		;****     Declare variables for input    ****
		;**** arrays wil be declared after sizes ****
		;**** have been read.                    ****
                ;********************************************
    lpend = 0
    nstore = 0
    ntdim = 0
    ndtin = 0
    nbsel = 0
    itval = 0
    input = 0

		;********************************
		;**** Read data from fortran ****
		;********************************

    readf, pipe, input
    nstore = input
    readf, pipe, input
    ntdim = input
    readf, pipe, input
    ndtin = input
    readf, pipe, input
    nbsel = input
  
		;******************************************
		;**** Now can define other array sizes ****
		;******************************************

    cicode = strarr(nbsel)
    cfcode = strarr(nbsel)
    ciion  = strarr(nbsel)
    cfion  = strarr(nbsel)
    bwno   = dblarr(nbsel)
    ita    = intarr(nbsel)
    tvals  = dblarr(ntdim, 3, nbsel)

		;****************************************************
		;*** Define some variables used to read arrays in ***
		;****************************************************

    temp = 0.0D
    itemp = 0
    sdum = ' '
  
		;********************************
                ;**** Read data from fortran ****
                ;********************************

    for i=0, nbsel-1 do begin
	readf, pipe, sdum
	cicode(i) = sdum
    endfor
    for i=0, nbsel-1 do begin
	readf, pipe, sdum
	cfcode(i) = sdum
    endfor
    for i=0, nbsel-1 do begin
	readf, pipe, sdum
	ciion(i) = sdum
    endfor
    for i=0, nbsel-1 do begin
	readf, pipe, sdum
	cfion(i) = sdum
    endfor
    for i=0, nbsel-1 do begin
	readf, pipe, sdum
	bwno(i) = sdum
    endfor
    for i=0, nbsel-1 do begin
	readf, pipe, itemp
	ita(i) = itemp
    endfor
    for k = 0, nbsel-1 do begin 
        for j =  0 , 2 do begin
	    for i = 0, ita(k)-1 do begin
                readf, pipe, temp
                tvals(i,j,k) = temp
            endfor
        endfor
    endfor
		;*******************************************
		;**** Set default value if non provided ****
                ;*** max size no of metastables is ndmet ***
                ;**** so use ndmet*ndmet for hlist_arr  ****
		;*******************************************
    if (procval.new lt 0) then begin
        nmettot = ndmet*ndmet
        str_arr  = strarr(nbsel)
        bwno_arr = dblarr(nbsel)
        hlist_arr = make_array(nmettot,/int,value=-1)
        procval = {	new   	: 	0 ,             		$
			title 	: 	'',				$
			ibsel 	: 	0 ,     			$
			imsel 	: 	0 ,     			$
                	ifout 	: 	itout, 				$
			itval 	: 	0,              		$
			tine  	: 	tine,                           $
			lfsel 	: 	0,				$
			tolval	: 	5,              		$
			hlist	: 	hlist_arr,            		$
                	losel 	: 	1               		}
    end else begin
        nmettot = ndmet*ndmet
        hlist_arr = make_array(nmettot,/int,value=-1)
        procval.tine = tine
        procval.ifout = itout
        if hlist_flag eq -1 then procval.hlist = hlist_arr
    end

		;****************************************
		;**** Pop-up processing input widget ****
		;****************************************

    adas502v208_proc, procval, dsfull, action, nstore, ntdim, ndtin, 	$
                  nbsel, cicode, cfcode, ciion, cfion, bwno,    	$ 
		  ita, tvals, bitfile, msize, strgmm,                   $
                  imetr, sz, selem, ndmet, FONT_LARGE=font_large,     	$
                  FONT_SMALL=font_small, EDIT_FONTS=edit_fonts

                ;*********************************************
                ;**** Act on the output from the widget   ****
                ;**** There are three    possible actions ****
                ;**** 'Done', 'Cancel' and 'Menu'.        ****
                ;*********************************************

    if action eq 'Done' then begin
        lpend = 0
    endif else if action eq 'Menu' then begin
        lpend = 0
        gomenu = 1
    endif else begin
        lpend = 1
    endelse

		;*******************************
		;**** Write data to fortran ****
		;*******************************

    printf, pipe, lpend   
    printf, pipe, procval.title, format='(a40)' 
    printf, pipe, procval.ibsel
;    printf, pipe, procval.imsel ;**** not sure if I need this yet
    printf, pipe, procval.ifout
    printf, pipe, procval.itval   
    for i = 0, procval.itval-1 do begin
        printf, pipe, procval.tine(i)
    endfor
    printf, pipe, procval.lfsel
    printf, pipe, procval.tolval
    printf, pipe, procval.losel

END
