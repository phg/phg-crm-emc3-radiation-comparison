; Copyright (c) 2003 Strathclyde University .
; SCCS info: Module @(#)$Header: /home/adascvs/idl/adas2xx/adas208/adas208_get_defaults.pro,v 1.1 2004/07/06 10:20:27 whitefor Exp $ Date $Date: 2004/07/06 10:20:27 $
;+
; PROJECT:
;       ADAS 
;
; NAME:
;       ADAS208_GET_DEFAULTS
;
; PURPOSE: 
;       This subroutine reads in default Te/dens data from both central
;       and user adas/defaults/adas208_te.dat and adas208_dens.dat files.
;       This allows a flexible and backward compatible extension of the 
;       defaults to more than one selection.
;
;
; EXPLANATION:
;       If the adas208_XX.dat files exist then these sets of defaults
;       are added to the adas208 processing button. The routine produces
;       the previous standard set to ensure that at least one is present.
;     
;
; NOTES:
;       The data file are strictly formatted - errors will caues probelms.
;       An example of the input data
; 
;                       2
;                       ---------
;                       Low Te set
;                       3
;                       ev
;                       1.0
;                       2.0
;                       3.0
;                       ----------
;                       Another set
;                       4
;                       K
;                       500.0
;                       1000.0
;                       4000.0
;                       8000.0
;
;        If the number of Te/dens in the file exceeds ndtem/ndden then
;        it is chopped at that value. 
;
; INPUTS:
;        type       -  'TE' or 'DENS' as appropriate
;        ndtem      -  max size of temperature vector (set in 208)
;        ndden      -  max size of density vector (set in 208)
;
;
; OPTIONAL INPUTS:
;       None
;
; OUTPUTS:
;       A structure is returned
;        
;                     value : num    - number of sets in both central
;                                      and user files
;                           : names  - string array of descriptors for
;                                      use in drop down list.
;                           : units  - 0,1,2 for Te = eV, K and reduced
;                                      0,1 for dens = cm**-3 and reduced
;                           : maxval - integer array of number of Te/dens  
;                           : data   - float array [num, max] where
;                                      max is ndtem or ndden.
;   
;
; OPTIONAL OUTPUTS:
;       None
;
; KEYWORD PARAMETERS:
;       None
;
;
; CALLS:
;       None
;                            
;
; SIDE EFFECTS:
;       Spawns system commands to check for file existance.
;
; CATEGORY:
;
; WRITTEN:
;       Martin O'Mullane
;
; MODIFIED:
;       1.1     Martin O'Mullane
;               First release 
;
; VERSION:
;       1.1     07-02-2001
;
;-
;-----------------------------------------------------------------------------


PRO adas208_get_defaults, type, ndtem, ndden, value
  
; Default is as before
  
  name = ['Standard Set']
  
  num  = 1
  
  if type EQ 'TE' then begin
     
     ext     = 'te'
     maxval  = [12]
     units   = ['red']
     data    = [ 5.0D+02 , 1.0D+03 , 2.0D+03 , 5.0D+03 , $
                 1.0D+04 , 2.0D+04 , 5.0D+04 , 1.0D+05 , $
                 2.0D+05 , 5.0D+05 , 1.0D+06 , 2.0D+06   ]
     max_all = ndtem
     
  endif else begin
  
     ext     = 'dens'
     maxval  = [8]
     units   = ['red']
     data    = [ 1.0D-03 , 1.0D+00 , 1.0D+03 , 1.0D+06 , $
                 1.0D+09 , 1.0D+12 , 1.0D+15 , 1.0D+18 ]
     max_all = ndden
     
  endelse
  
  
; We store the sets both centrally and in the user's adas/defaults directory 
  
  user_dir = getenv("ADASUSER")
  cent_dir = getenv("ADASCENT")
  
  file_c   = cent_dir + '/defaults/adas208_' + ext + '.dat'
  file_u   = user_dir + '/defaults/adas208_' + ext + '.dat'
  idum     = 0
  sdum     = ''
  
  ; First the central data
  
  command= 'if [ -r ' + file_c + ' ]; then  echo Found; fi'
  spawn, command, found, /sh

  if found(0) EQ 'Found' then begin
  
     openr, lun, file_c, /get_lun
     readf, lun, idum
     nset = idum
     num  = num + nset
     
     for j = 0, nset-1 do begin
        
        readf, lun, sdum                    ; -----
        readf, lun, sdum                    ; description
        name = [name, 'C : '+sdum] 
        readf, lun, idum
        maxval = [maxval, idum]
        readf, lun, sdum
        units = [units, sdum]
        
        tmp = fltarr(idum)
        readf, lun, tmp
        data = [data,tmp]
  
     endfor
     
     free_lun, lun
     
  endif
  
  ; Followed by the user's data
  
  command= 'if [ -r ' + file_u + ' ]; then  echo Found; fi'
  spawn, command, found, /sh

  if found(0) EQ 'Found' then begin
  
     openr, lun, file_u, /get_lun
     readf, lun, idum
     nset = idum
     num  = num + nset
     
     for j = 0, nset-1 do begin
        readf, lun, sdum                    ; -----
        readf, lun, sdum                    ; description
        name = [name, 'U : '+sdum] 
        readf, lun, idum
        maxval = [maxval, idum]
        readf, lun, sdum
        units = [units, sdum]
        
        tmp = fltarr(idum)
        readf, lun, tmp
        data = [data,tmp]
  
     endfor
     
     free_lun, lun
     
  endif
  
  
; Now put the data into a structure

units = strlowcase(units)
untxt = intarr(num)

if type EQ 'TE' then begin
   
   ind_red = where(strpos(units, 'red') NE -1, c_red)
   ind_ev  = where(strpos(units, 'ev') NE -1, c_ev) 
   ind_k   = where(strpos(units, 'k') NE -1, c_k) 

   if c_red GT 0 then untxt[ind_red] = 2
   if c_ev  GT 0 then untxt[ind_ev]  = 1
   if c_k   GT 0 then untxt[ind_k]   = 0
   
endif else begin

   ind_red = where(strpos(units, 'red') NE -1, c_red)
   ind_cm  = where(strpos(units, 'cm') NE -1, c_cm) 
  
   if c_red GT 0 then untxt[ind_red] = 1
   if c_cm  GT 0 then untxt[ind_cm]  = 0

endelse


; Restructure the data array

dout = fltarr(num, max_all)

istart = 0
for j = 0, num-1 do begin
  
  iadd = (maxval[j] < max_all) - 1
  ifin = istart + iadd
  dout[j,0:ifin-istart] = data[istart:ifin]
  istart = ifin + 1
  
endfor

ind = where(maxval GT max_all, count)
if count GT 0 then print, 'Too many items in default file - restricted to max value'

value = { num    : num,   $
          names  : name, $
          units  : untxt, $
          maxval : maxval < max_all, $
          data   : dout}
  
  
END
