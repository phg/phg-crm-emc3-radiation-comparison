; Copyright (c) 1995 Strathclyde University .
; SCCS info: Module @(#)$Header: /home/adascvs/idl/adas2xx/adas208/cw_adas208_proc.pro,v 1.15 2018/10/03 09:28:33 mog Exp $ Date $Date: 2018/10/03 09:28:33 $
;+
; PROJECT:
;       ADAS IBM MVS to DEC UNIX conversion
;
; NAME:
;	CW_ADAS208_PROC()
;
; PURPOSE:
;	Produces a widget for ADAS208 processing options/input.
;
; EXPLANATION:
;	This function creates a compound widget consisting of a text
;	widget holding an editable 'Run title', the dataset name/browse
;	widget cw_adas_dsbr, label widgets with charge information for
;	the input data, a table widget for temperature data cw_adas_table,
;	a second cw_adas_table widget for density data, buttons to
;	enter default values into the temperature and density tables,
;	a multiple selection widget cw_adas_sel, a reaction settings
;	widget cw_adas_reac2, a message widget, a 'Cancel' button and
;	finally a 'Done' button.
;
;	The compound widgets included in this widget are self managing.
;	This widget only manages events from the two 'Defaults' buttons,
;	the 'Cancel' button and the 'Done' button.
;
;	This widget only generates events for the 'Done' and 'Cancel'
;	buttons.
;
; USE:
;	This widget is specific to ADAS208, see adas208_proc.pro
;	for use.
;
; INPUTS:
;	TOPPARENT- Long integer, ID of parent widget.
;
;	SZ	- String, recombined ion charge read.
;
;	SZ0	- String, nuclear charge read.
;
;	STRGA	- String array, level designations.
;
;	DSNINC	- String; The full system file name of the input COPASE
;		  dataset selected by the user for processing.
;
;	NDTEM	- Integer; Maximum number of temperatures allowed.
;
;	NDDEN	- Integer; Maximum number of densities allowed.
;
;	NDMET	- Integer; Maximum number of metastables allowed.
;
;	IL	- Integer; Number of energy levels.
;
;	NV	- Integer; Number of termperatures.
;
;	TSCEF	- dblarr(8,3); Input electron temperatures in three units.
;
;	The inputs SZ to TSCEF map exactly onto variables of the same
;	name in the ADAS208 FORTRAN program.
;
;       BITFILE - String; the path to the directory containing bitmaps
;                 for the 'escape to series menu' button.
;
;	INVAL	- Structure; the input screen structure - see cw_adas208_in.pro
;
;	SELEM	- String; selected element symbol
;
; OPTIONAL INPUTS:
;	None
;
; OUTPUTS:
;	The return value of this function is the ID of the declared widget.
;
; OPTIONAL OUTPUTS:
;	None
;
; KEYWORD PARAMETERS:
;	VALUE	- A structure which determines the initial settings of
;		  the processing options widget.
;
;		  The default VALUE is created thus;
;
;			imetr = make_array(ndmet,/int,value=0)
;			temp = dblarr(ndtem)
;			dens = dblarr(ndden)
;			ps = { $
;		 	TITLE:'',	NMET:0, $
;			IMETR:imetr,	IFOUT:1, $
;			MAXT:0,		TINE:temp, $
;			TINP:temp,	TINH:temp, $
;			IDOUT:1,	MAXD:0, $
;			DINE:dens,	DINP:dens, $
;			RATHA:dens,	RATPIA:dens, $
;			ZEFF:'',	LPSEL:0, $
;			LZSEL:0,	LISEL:0, $
;			LHSEL:0,	LRSEL:0, $
;                       LIOSEL:0,       LNSEL:0, $
;			LNORM:0,		 $
;			VAL502:struct502,	 $
;			WVLS:0.0,	WVLL:0.0,$
;			NWVL:1, AVLT:0.0  }
;
; 		TITLE	entered general title for program run
; 		NMET	number of metastables
; 		(IMETR(I),I=1,NMET)	index of metastables
; 		IFOUT	input temperature units (1,2,3)
; 		MAXT	number of input temperatures (1-20)
; 		(TINE(I),I=1,MAXT)	electron temperatures
; 		(TINP(I),I=1,MAXT)	proton temperatures
; 		(TINH(I),I=1,MAXT)	neutral hydrogen temperatures
; 		IDOUT	input density units (1,2)
; 		MAXD	number of input densities
; 		(DINE(I),I=1,MAXD)	electron densities
; 		(DINP(I),I=1,MAXD)	proton densities
; 		(RATHA(I),I=1,MAXD)	ratio (neut h dens/elec dens)
; 		(RATPIA(I),I=1,MAXD)	ratio (n(z+1)/n(z) stage abund)
; 		ZEFF	plasma z effective, a string holding a valid number.
; 		LPSEL	include proton collisions?
; 		LZSEL	scale proton collisions with plasma z effective?
; 		LISEL	include ionisation rates?
; 		LHSEL	include charge transfer from neutral hydrogen?
; 		LRSEL	include free electron recombination?
;               LIOSEL to be determined
;               LNSEL   include projected bundle-n data?
;               LNORM   1 => if nmet=1 normalise total and
;                                        specific line pwr output files
;                                        plt & pls to stage tot populatn
;                       0 => if not normalise to identified
;                                        metastable populations.
;		NWVL    number of wavelength intervals
;		WVLS    short wavelength limit for pec & sxb (A)
;		WVLL    long wavelength limit for pec & sxb  (A)
;		AVLT    lower limit of A-values for pec & sxb
;
;		All of the above structure elements map onto variables of
;		the same name in the ADAS208 FORTRAN program.
;
;		VAL502	Structure to pass to the adas502v208 procedure - see
;			adas502v208.pro
;
;	UVALUE	- A user value for the widget. Default 0.
;
;	FONT_LARGE - The name of a larger font.  Default current system
;		     font
;
;	FONT_SMALL - The name of a smaller font. Default current system
;		     font.
;
;	EDIT_FONTS - A structure of two fonts used in the table editor
;		     adas_edtab.pro, {font_norm:'',font_input:''}.  Defaults
;		     to current system font.
;
;	NUM_FORM   - String; Numeric format to use in tables.  Default
;			'(E10.3)'
;		     
;
; CALLS:
;	POPUP		Popup warning window with buttons.
;	XXDEFT		Returns default temperature table.
;	XXDEFD		Returns default density table.
;	CW_ADAS_DSBR	Dataset name and comments browsing button.
;	CW_ADAS_TABLE	Adas data table widget.
;	CW_ADAS_SEL	Adas mulitple selection widget.
;	CW_ADAS208_REAC2 Reaction settings for ADAS208.
;	CW_SAVESTATE	Widget management routine.
;	CW_LOADSTATE	Widget management routine.
;
;	See side effects for other related widget management routines.
;
; SIDE EFFECTS:
;       A COMMON BLOCK: CW_PROC208_BLK is used which is private to
;       this widget.
;
;	The following widget management routines are included in this
;	file;
;	PROC208_GET_VAL()	Returns the current VALUE structure.
;	PROC208_EVENT()		Process and issue events.
;
; CATEGORY:
;	Compound Widget
;
; WRITTEN:
;       David H.Brooks, Universiy of Strathclyde, 02-May-1996
;
; MODIFIED:
;       Version 1.0     David Brooks
;			Written using adas205_out.pro v1.7 as a template
;                       Modified to use 3 switching tables.
;	1.1	    	William Osborn (Tessella Support Services plc)
;			Added documentation + put under SCCS control
;	1.2		William Osborn
;			Rearranged compound widgets to use the space better
;	1.3		William Osborn
;			Added dsfull to parameters of adas502v208 routine
;			and to procval.val205
;	1.4		William Osborn
;			Trap of val502.npl = 0 error. Occurs if data-set not
;			set up with parents on first line. Should be removed
;			when Fortran is modified to give defaults if no
;			parents present.
;	1.5		William Osborn
;			Changed reacset structure name to reacset208 so as
;			not to conflict with reacset in adas205
;	1.6		William Osborn
;			Removed xsize=30 from ionisation search message label.
;	1.7		William Osborn
;			Corrected metastable display array in ADAS502(208).
;			Didn't cross reference by the selected indices before,
;			just printed the first ps.val502.npl metastables.
;       1.8             William Osborn
;                       Added pec and sxb selection boxes.
;       1.9             William Osborn
;                       Corrected documentation mistake
;	1.10		Richard Martin
;			Added LNORM option (LNORM flag already exists in
;			the fortran code).
;	1.11		Richard Martin
;			Increased A-value edit box by 2 characters.
;	1.12    	Hugh Summers
;			Added  truth table for ionisation rate data
;                       availability from adf04 file for display
;	1.13		Richard Martin
;			Removed obsolete cw_loadstate/savestate statements
;	1.14 	        Martin O'Mullane
;		         - Wavelength limits and A-value selection criteria for
;                          PECs are now arrays.
;                        - Change metastable selection, reaction rate choice
;                          PEC selection criteria widget layout to match adas810.
;	1.15 	        Martin O'Mullane
;                        - Use ndmet, and not a hard-coded value of 3, to setup
;                          arrays depending on the number metastables.
;                        - Give a maximum x width of 40 characters to the table 
;                          widget for the ratpia and ratmia panels to force 
;                          them to scroll horizontally if nmet causes them to 
;                          be large than the Ne/NH/Nion overlay.
;
; VERSION:
;	1.0	02-05-96
;	1.1	10-05-96
;	1.2	10-05-96
;	1.3	13-05-96
;	1.4	14-05-96
;	1.5	24-05-96
;	1.6	10-09-96
;	1.7	24-09-96
;	1.8	14-10-96
;	1.9	14-10-96
;	1.10	01-07-97
;	1.11	24-11-98
;	1.12	14-09-99
;	1.13	31-01-02
;	1.14	18-03-13
;	1.15	02-10-18
;
;-
;-----------------------------------------------------------------------------

FUNCTION proc208_get_val, id


                ;**** Return to caller on error ****
  ON_ERROR, 2

                ;**** Retrieve the state ****

  first_child = widget_info(id,/child)
  widget_control, first_child, get_uvalue=state, /no_copy
  
		;***********************************
		;**** Get run title from widget ****
		;***********************************
  widget_control,state.runid,get_value=title

		;**************************************************
		;**** Get new selections from selection widget ****
		;**************************************************

  widget_control,state.selid,get_value=selected
		
                ;**** selected is IDL index, from 0. imetr needs ****
		;**** to be fortran index, from 1.               ****
  
  ind = where(selected EQ 1, nmet)
  imetr = intarr(state.ndmet)
  if nmet GT 0 AND nmet LE state.ndmet then imetr[0:nmet-1] = ind + 1
  

		;****************************************************
		;**** Get new temperature data from table widget ****
		;****************************************************
  widget_control,state.tempid,get_value=tempval
  tempdata = tempval.value
  ifout = tempval.units+1
		;**** Copy out temperature values ****
  blanks = where(strtrim(tempdata[0,*],2) eq '')
  if blanks[0] ge 0 then maxt=blanks[0] else maxt=state.ndtem
  tine = dblarr(state.ndtem)
  tinp = dblarr(state.ndtem)
  tinh = dblarr(state.ndtem)
  if maxt gt 0 then begin
    tine[0:maxt-1] = double(tempdata[0,0:maxt-1])
    tinp[0:maxt-1] = double(tempdata[1,0:maxt-1])
    tinh[0:maxt-1] = double(tempdata[2,0:maxt-1])
  end

		;**** Fill out the rest with zero ****
  if maxt lt state.ndtem then begin
    tine[maxt:state.ndtem-1] = double(0.0)
    tinp[maxt:state.ndtem-1] = double(0.0)
    tinh[maxt:state.ndtem-1] = double(0.0)
  end

		;******************************************************
		;**** Get new density/ratio data from table widget ****
		;******************************************************

                ;**** switching table one ****
  widget_control,state.densid1,get_value=densval1
  densdata1 = densval1.value
  idout = densval1.units+1

                ;**** switching table one ****
  widget_control,state.densid2,get_value=densval2
  densdata2 = densval2.value

                ;**** switching table one ****
  widget_control,state.densid3,get_value=densval3
  densdata3 = densval3.value

		;**** Copy out densities/ratios ****

  blanks1 = where(strtrim(densdata1[0,*],2) eq '')
  blanks2 = where(strtrim(densdata2[0,*],2) eq '')
  blanks3 = where(strtrim(densdata3[0,*],2) eq '')
  if blanks1[0] ge 0 then maxd=blanks1[0] else maxd=state.ndden
  if blanks2[0] ge 0 then maxrp=blanks2[0] else maxrp=state.ndden
  if blanks3[0] ge 0 then maxrm=blanks3[0] else maxrm=state.ndden

  dine = dblarr(state.ndden)
  dinp = dblarr(state.ndden)
  ratha = dblarr(state.ndden)
  ratpia = dblarr(state.ndden,state.ndmet)
  ratmia = dblarr(state.ndden,state.ndmet)

  if maxd gt 0 then begin
    dine[0:maxd-1] = double(densdata1[0,0:maxd-1])
    dinp[0:maxd-1] = double(densdata1[1,0:maxd-1])
    ratha[0:maxd-1] = double(densdata1[2,0:maxd-1])
  end
  
  if maxrp gt 0 then begin
    for j = 0, state.ndmet-1 do begin
       ratpia[0:maxrp-1,j] = double(densdata2[j,0:maxrp-1])
    endfor
  endif
  if maxrm gt 0 then begin
    for j = 0, state.ndmet-1 do begin
       ratmia[0:maxrm-1,j] = double(densdata3[j,0:maxrm-1])
    endfor
  endif

		;**** Fill out the rest with zeros ****
  if maxd lt state.ndden then begin
    dine[maxd:state.ndden-1] = double(0.0)
    dinp[maxd:state.ndden-1] = double(0.0)
    ratha[maxd:state.ndden-1] = double(0.0)
  endif
  if maxrp lt state.ndden then begin
    for j = 0, state.ndmet-1 do begin
       ratpia[maxrp:state.ndden-1,j] = double(0.0)
    endfor
  endif
  if maxrm lt state.ndden then begin
    for j = 0, state.ndmet-1 do begin
       ratmia[maxrm:state.ndden-1,j] = double(0.0)
    endfor
  endif

		;*************************************************
		;**** Get new reaction selections from widget ****
		;*************************************************
  widget_control,state.reacid,get_value=reacset
  widget_control, state.zeffID, get_value=zeff

  lisel  = reacset[0]
  lhsel  = reacset[1]
  lrsel  = reacset[2]
  liosel = reacset[3]
  lnsel  = reacset[4]
  lpsel  = reacset[5]
  zeff   = zeff[0]

  if zeff GT 0.0 then lzsel = 1 else lzsel = 0
  
		;*************************************************
		;**** Get lnorm value from widget             ****
		;*************************************************
		
  widget_control, state.lnormid, get_value=lnorm

		;**** Get pec and sxb limit values ****
  widget_control, state.wvlid, get_value=tempval

  temp   = double(tempval.value)
  wvls   = reform(temp[0,*])
  wvll   = reform(temp[1,*])
  avlt   = reform(temp[2,*])

  res = where(wvls GT 0.0, nwvl)

  ps = {proc208_set, $
		 	TITLE:title[0],	NMET:nmet,     $
			IMETR:imetr,	IFOUT:ifout,   $
			MAXT:maxt,	TINE:tine,     $
			TINP:tinp,	TINH:tinh,     $
			IDOUT:idout,	MAXD:maxd,     $
                        MAXRP:maxrp,    MAXRM:maxrm,   $
			DINE:dine,	DINP:dinp,     $
			RATHA:ratha,	RATPIA:ratpia, $
                        RATMIA:ratmia,  IMROUT:state.ps.imrout, $
			ZEFF:zeff,	LPSEL:lpsel,   $
			LZSEL:lzsel,	LISEL:lisel,   $
			LHSEL:lhsel,	LRSEL:lrsel,   $
			LIOSEL:liosel,	LNSEL:lnsel,   $
			LNORM:lnorm,		       $
                        VAL502:state.ps.val502,	       $
			WVLS:wvls,	WVLL:wvll,     $
			AVLT:avlt, nwvl:nwvl }

  widget_control, first_child, set_uvalue=state, /no_copy
  
  RETURN, ps

END

;-----------------------------------------------------------------------------

FUNCTION proc208_event, event

                ;**** Base ID of compound widget ****
  parent=event.handler

                ;**** Retrieve the state ****

  first_child = widget_info(parent,/child)
  widget_control, first_child, get_uvalue=state
  
		;*********************************
		;**** Clear previous messages ****
		;*********************************
  widget_control,state.messid,set_value=' '

                ;************************
                ;**** Process events ****
                ;************************
  CASE event.id OF

		;*************************************
		;**** Default temperatures button ****
		;*************************************
    state.deftid: begin

		;**** popup window to confirm overwriting current values ****
	action= popup(message='Confirm Overwrite Temperatures with Defaults', $
			buttons=['Confirm','Cancel'],font=state.font)

	if action eq 'Confirm' then begin
		;**** Get current table widget value ****
 	  widget_control,state.tempid,get_value=tempval

		;**** Get the default temperatures and set new units ****
 	  xxdeft, state.ndtem, maxt, deft
	  tempval.units = 2

		;**** Copy defaults into value structure ****
 	  if maxt gt 0 then begin
   	    tempval.value[0,0:maxt-1] = $
		strtrim(string(deft[0:maxt-1],format=state.num_form),2)
   	    tempval.value[1,0:maxt-1] = $
		strtrim(string(deft[0:maxt-1],format=state.num_form),2)
   	    tempval.value[2,0:maxt-1] = $
		strtrim(string(deft[0:maxt-1],format=state.num_form),2)
 	  end

		;**** Fill out the rest with blanks ****
 	  if maxt lt state.ndtem then begin
   	    tempval.value[0:2,maxt:state.ndtem-1] = ''
 	  end

		;**** Copy new temperature data to table widget ****
 	  widget_control,state.tempid,set_value=tempval
	end

      end

		;**********************************
		;**** Default densities button ****
		;**********************************
    state.defdid: begin

		;**** popup window to confirm overwriting current values ****
	action = popup(message='Confirm Overwrite Densities/Ratios with Defaults', $
			buttons=['Confirm','Cancel'],font=state.font)

	if action eq 'Confirm' then begin
		;**** Get current table widget value ****
 	  widget_control,state.densid1,get_value=densval1
 	  widget_control,state.densid2,get_value=densval2
 	  widget_control,state.densid3,get_value=densval3

		;**** Get the default densities and set new units ****
 	  xxdefd, state.ndden, maxd, defd
	  densval1.units = 1

		;**** Copy defaults into value structure ****
 	  if maxd gt 0 then begin
   	    densval1.value[0,0:maxd-1] = $
		strtrim(string(defd[0:maxd-1],format=state.num_form),2)
   	    densval1.value[1:2,0:maxd-1] = '0.0000'
 	  end

                ;**** Copy default ratios into value structure. ****
                ;**** No. of ratios currently set to maxd  i.e. ****
                ;**** the same as the no. of density defaults   ****
          maxrp = maxd
          maxrm = maxd

          if maxrp gt 0 then begin
   	    densval2.value[0,0:maxrp-1] = '1.0000'
   	    densval2.value[1:*,0:maxrp-1] = '0.0000'
          end
          if maxrm gt 0 then begin
   	    densval3.value[0,0:maxrm-1] = '1.0000'
   	    densval3.value[1:*,0:maxrm-1] = '0.0000'
          end

		;**** Fill out the rest with blanks ****
 	  if maxd lt state.ndden then begin
   	    densval1.value[*,maxd:state.ndden-1] = ''
 	  end
          if maxrp lt state.ndden then begin
   	    densval2.value[*,maxrp:state.ndden-1] = ''
          end
          if maxrm lt state.ndden then begin
   	    densval3.value[*,maxrm:state.ndden-1] = ''
          end

		;**** Copy new density/ratio data to table widget ****
 	  widget_control,state.densid1,set_value=densval1
 	  widget_control,state.densid2,set_value=densval2
 	  widget_control,state.densid3,set_value=densval3

	end

      end
      


		;***********************
		;**** Cancel button ****
		;***********************
    state.cancelid: new_event = {ID:parent, TOP:event.top, $
				HANDLER:0L, ACTION:'Cancel'}

		;*********************
		;**** Done button ****
		;*********************
    state.doneid: begin
			;***************************************
			;**** Check all user input is legal ****
			;***************************************
	error = 0
	widget_control,event.handler,get_value=ps

		;**** check metastable states selected ****
	if error eq 0 and ps.nmet eq 0 then begin
	  error = 1
	  message='Error: No Metastable States selected.'
        end

	if error eq 0 and ps.nmet gt 4 then begin
	  error = 1
	  message='Error: A maximum of 4 metastables only.'
        end

		;**** check temperatures entered ****
	if error eq 0 and ps.maxt eq 0 then begin
	  error = 1
	  message='Error: No temperatures entered.'
        end

		;**** check densities entered ****
	if error eq 0 and ps.maxd eq 0 then begin
	  error = 1
	  message='Error: No densities entered.'
        end

		;**** check zeff value ****
	if error eq 0 and ps.lzsel eq 1 then begin
	  error = num_chk(ps.zeff)
	  if error gt 0 then message='Error: Z-effective should be a number.'
        end

		;**** return value or flag error ****
	if error eq 0 then begin
	  new_event = {ID:parent, TOP:event.top, HANDLER:0L, ACTION:'Done'}
        end else begin
	  widget_control,state.messid,set_value=message
	  new_event = 0L
        end

      end
                ;**** Menu button ****
    state.outid: begin
      new_event = {ID:parent, TOP:event.top, HANDLER:0L, ACTION:'Menu'}
    end

                ;**** Spawn button ****
    state.spawnid: begin
      widget_control, state.selid, get_value = selected
      widget_control, event.handler, get_value = ps

      if ps.nmet gt 0 then begin
      if ps.maxt gt 0 then begin
      if ps.maxd gt 0 then begin
      if ps.val502.npl gt 0 then begin

        msize = ps.nmet*ps.val502.npl
        strgmm = strarr(1,msize)
        icnt = 0
        ciss04 = ' '
        for i = 0,ps.nmet-1 do begin
          for j = 0,ps.val502.npl-1 do begin
            ciss04 = ' '
            if ps.val502.iss04a[ps.imetr[i]-1,j] eq 1 then ciss04 = '#'
            strgmm[0,icnt] = ciss04+ps.val502.strgmi[selected[i]]+ps.val502.strgmf[j]
            icnt = icnt+1
          endfor
        endfor
;      npl    = ps.val502.npl
;      strgmf = ps.val502.strgmf
;      nimet  = ps.nmet
;      strgmi = ps.val502.strgmi
        imetr  = ps.imetr
        tine   = ps.tine
        ibsela = ps.val502.ibsela
        lsseta = ps.val502.lsseta
        sgrda  = ps.val502.sgrda
        idout = ps.ifout
                ;**** initiate modified 502 ****
        adas502v208, state.inval.adasrel, state.inval.fortdir, $
                   state.inval.userroot, state.inval.centroot, $
                   state.inval.devlist, state.inval.devcode,   $
                   state.inval.fontl, state.inval.fonts,       $
                   state.inval.efont, msize, strgmm,           $
                   imetr, state.sz, state.selem, tine,         $
                   ibsela, lsseta, sgrda, state.ndmet, idout,  $
		   dsfull

                ;**** copy out any alterations ****
;**** if any variables need to be carried out
;**** they can be put in val502

        state.ps.val502.ibsela = ibsela
        state.ps.val502.lsseta = lsseta
        state.ps.val502.sgrda  = sgrda

	state.ps.val502.dsfull = dsfull

;      state.ps.val502.npl    = npl
;      state.ps.val502.strgmf = strgmf
;      state.ps.val502.strgmi = strgmi

        message = '***Search Completed***'

      endif else message = '***Error: this COPASE file is not set up to be used by ADAS208***'
      endif else message = '***Error: no Densities Entered***'
      endif else message = '***Error: no Temperatures Entered***'
      endif else message = '***Error: no Metastables Selected***'

        widget_control, state.spawnid, set_button = 0
        widget_control, state.messid, set_value = message
    end

                ;**** Switching base buttons ****
    state.ienhid:begin
      widget_control, state.izpdid, set_button = 0
      widget_control, state.izmdid, set_button = 0
      widget_control, state.col1, map = 1
      widget_control, state.col2, map = 0
      widget_control, state.col3, map = 0
      state.ps.imrout = 0
    end
    state.izpdid:begin
      widget_control, state.ienhid, set_button = 0
      widget_control, state.izmdid, set_button = 0
      widget_control, state.col1, map = 0
      widget_control, state.col2, map = 1
      widget_control, state.col3, map = 0
      state.ps.imrout = 1
    end
    state.izmdid:begin
      widget_control, state.ienhid, set_button = 0
      widget_control, state.izpdid, set_button = 0
      widget_control, state.col1, map = 0
      widget_control, state.col2, map = 0
      widget_control, state.col3, map = 1
      state.ps.imrout = 2
    end

    ELSE: new_event = 0L

  ENDCASE

  widget_control, first_child, set_uvalue=state, /no_copy
  
  RETURN, new_event
END

;-----------------------------------------------------------------------------

FUNCTION cw_adas208_proc, topparent, $
		sz, sz0, strga, dsninc, ndtem, ndden, ndmet, ndwvl, il, nv, $
                tscef, bitfile, inval, selem, $
		VALUE=value, UVALUE=uvalue, $
		FONT_LARGE=font_large, FONT_SMALL=font_small, $
		EDIT_FONTS=edit_fonts, NUM_FORM=num_form


		;**** Set defaults for keywords ****
  IF NOT (KEYWORD_SET(uvalue)) THEN uvalue = 0
  IF NOT (KEYWORD_SET(font_large)) THEN font_large = ''
  IF NOT (KEYWORD_SET(font_small)) THEN font_small = ''
  IF NOT (KEYWORD_SET(edit_fonts)) THEN $
				edit_fonts = {font_norm:'',font_input:''}
  IF NOT (KEYWORD_SET(num_form)) THEN num_form = '(E10.3)'

  IF NOT (KEYWORD_SET(value)) THEN begin
	imetr = make_array(ndmet,/int,value=0)
	temp = dblarr(ndtem)
	dens = dblarr(ndden)
        ratios = dblarr(ndden,4)
        temp_struct =  { NPL:0 }
	ps = {proc208_set,                                           $
		 	TITLE:'',	NMET:0,                      $
			IMETR:imetr,	IFOUT:1,                     $
			MAXT:0,		TINE:temp,                   $
			TINP:temp,	TINH:temp,                   $
			IDOUT:1,	MAXD:0,                      $
			MAXRP:0,	MAXRM:0,                     $
			DINE:dens,	DINP:dens,                   $
			RATHA:dens,	RATPIA:ratios,               $
                        RATMIA:ratios,  IMROUT:0,                    $
			ZEFF:'',	LPSEL:0,                     $
			LZSEL:0,	LISEL:0,                     $
			LHSEL:0,	LRSEL:0,                     $
			LIOSEL:0,	LNSEL:0,                     $
			LNORM:0,				     $
                        VAL502:temp_struct,			     $
			WVLS:0.0,	WVLL:0.0,		     $
			AVLT:0.0, nwvl:0}
  END ELSE BEGIN
	ps = {proc208_set,                                           $
		 	TITLE:value.title,	NMET:value.nmet,     $
			IMETR:value.imetr,	IFOUT:value.ifout,   $
			MAXT:value.maxt,	TINE:value.tine,     $
			TINP:value.tinp,	TINH:value.tinh,     $
			IDOUT:value.idout,	MAXD:value.maxd,     $
			MAXRP:value.maxrp,	MAXRM:value.maxrm,   $
			DINE:value.dine,	DINP:value.dinp,     $
			RATHA:value.ratha,	RATPIA:value.ratpia, $
			RATMIA:value.ratmia,    IMROUT:value.imrout, $
			ZEFF:value.zeff,	LPSEL:value.lpsel,   $
			LZSEL:value.lzsel,	LISEL:value.lisel,   $
			LHSEL:value.lhsel,	LRSEL:value.lrsel,   $
			LIOSEL:value.liosel,	LNSEL:value.lnsel,   $
			LNORM:value.lnorm,			     $
                        VAL502:value.val502,			     $
			WVLS:value.wvls,	WVLL:value.wvll,     $
			AVLT:value.avlt, nwvl:value.nwvl }
  END


		;***************************************************
		;**** Assemble temperature table data           ****
		;***************************************************
		;**** The adas table widget requires data to be ****
		;**** input as strings, so all the numeric data ****
		;**** has to be written into a string array.    ****
		;**** Declare temp table array                  ****
		;**** Three editable columns and input values   ****
		;**** in three different units.                 ****
		;***************************************************
                
  tempdata = strarr(6,ndtem)

		;**** Copy out temperature values ****
  if ps.maxt gt 0 then begin
    tempdata[0,0:ps.maxt-1] = $
		strtrim(string(ps.tine[0:ps.maxt-1],format=num_form),2)
    tempdata[1,0:ps.maxt-1] = $
		strtrim(string(ps.tinp[0:ps.maxt-1],format=num_form),2)
    tempdata[2,0:ps.maxt-1] = $
		strtrim(string(ps.tinh[0:ps.maxt-1],format=num_form),2)
  end
		;**** input values in three units ****
  tempdata[3,0:nv-1] = strtrim(string(tscef[0:nv-1,0],format=num_form),2)
  tempdata[4,0:nv-1] = strtrim(string(tscef[0:nv-1,1],format=num_form),2)
  tempdata[5,0:nv-1] = strtrim(string(tscef[0:nv-1,2],format=num_form),2)

		;**** Fill out the rest with blanks ****
  if ps.maxt lt ndtem then begin
    tempdata[0:2,ps.maxt:ndtem-1] = ''
  end
  tempdata[3:5,nv:ndtem-1] = ''

		;***************************************************
		;**** Assemble density table data               ****
		;***************************************************
		;**** The adas table widget requires data to be ****
		;**** input as strings, so all the numeric data ****
		;**** has to be written into a string array.    ****
		;**** Declare dens table array                  ****
		;***************************************************
                
  densdata1 = strarr(3,ndden)
  densdata2 = strarr(ndmet,ndden)
  densdata3 = strarr(ndmet,ndden)

		;**** Copy out density values ****
  if ps.maxd gt 0 then begin
    densdata1[0,0:ps.maxd-1] = $
		strtrim(string(ps.dine[0:ps.maxd-1],format=num_form),2)
    densdata1[1,0:ps.maxd-1] = $
		strtrim(string(ps.dinp[0:ps.maxd-1],format=num_form),2)
    densdata1[2,0:ps.maxd-1] = $
		strtrim(string(ps.ratha[0:ps.maxd-1],format=num_form),2)
    
    for j = 0, ndmet-1 do begin
       densdata2[j,0:ps.maxd-1] = $
		strtrim(string(ps.ratpia[0:ps.maxd-1,j],format=num_form),2)
       densdata3[j,0:ps.maxd-1] = $
		strtrim(string(ps.ratmia[0:ps.maxd-1,j],format=num_form),2)
    endfor
    
  end

		;**** Fill out the rest with blanks ****
  if ps.maxd lt ndden then begin
    densdata1[*,ps.maxd:ndden-1] = ''
    densdata2[*,ps.maxd:ndden-1] = ''
    densdata3[*,ps.maxd:ndden-1] = ''
  end


  ; Spectral Intervals
  
  wvldata = strarr(3,ndwvl)
  
  if ps.nwvl gt 0 then begin
  
    wvldata[0,0:ps.nwvl-1] = $
		strtrim(string(ps.wvls[0:ps.nwvl-1],format='(f10.3)'),2)
    wvldata[1,0:ps.nwvl-1] = $
		strtrim(string(ps.wvll[0:ps.nwvl-1],format='(f10.3)'),2)
    wvldata[2,0:ps.nwvl-1] = $
		strtrim(string(ps.avlt[0:ps.nwvl-1],format='(e10.2)'),2)
                
  endif
  

		;********************************************************
		;**** Create the 208 Processing options/input window ****
		;********************************************************

		;**** create titled base widget ****
  parent = widget_base(topparent, UVALUE = uvalue, $
			EVENT_FUNC = "proc208_event", $
			FUNC_GET_VALUE = "proc208_get_val", $
			/COLUMN)

  first_child = widget_base(parent)

  topbase = widget_base(first_child,/column)

		;**** add run title ****
  base = widget_base(topbase,/row)
  rc = widget_label(base,value='Title for Run',font=font_large)
  runid = widget_text(base,value=ps.title,xsize=40,font=font_large,/edit)

		;**** add dataset name and browse button ****
  rc = cw_adas_dsbr(topbase,dsninc,font=font_large,/row)

		;**** add charge information ****
  rc = widget_label(base,font=font_large, $
			value='Nuclear Charge:'+sz0+'  Ion Charge:'+sz)

		;**** Another base ****
  tablebase = widget_base(parent,/row)

		;**** base for the temperature table and defaults button ****
  base = widget_base(tablebase,/column,/frame)

                ;**** a row for the option switches ****
  row1 = widget_base(base,/row)
  row2 = widget_base(base,/row)
  col1 = widget_base(row2,/column)

                ;**** a space to keep the widget looking even ****
  sp1 = widget_label(row1,value = '          ')

		;**** temperature data table ****
  colhead = [['Electron','Ion','Neutral','Input'], $
	     ['','','Hydrogen','Value']]
		;**** convert FORTRAN index to IDL index ****
  units = ps.ifout-1
  unitname = ['Kelvin','eV','Reduced']
		;**** Four columns in the table and three sets of units. ****
		;**** Columns 1,2 & 3 have the same values for all three ****
		;**** units but column 4 switches between sets 3, 4 & 5 ****
		;**** in the input array tempdata as the units change.  ****
  unitind = [[0,0,0],[1,1,1],[2,2,2],[3,4,5]]
  tempid = cw_adas_table(col1, tempdata, units, unitname, unitind, $
			UNITSTITLE = 'Temperature Units', /scroll, $
			COLEDIT = [1,1,1,0], COLHEAD = colhead, $
			TITLE = 'Temperatures', ORDER = [1,0,0,0], $
			LIMITS = [2,2,2,2], CELLSIZE = 12, $
			FONTS = edit_fonts, FONT_LARGE = font_large, $
			FONT_SMALL = font_small)

		;**** Default temperatures button ****
  base = widget_base(base,/column)
  deftid = widget_button(base,value='Default Temperatures',font=font_large)

		;**** base for the density table and defaults button ****
  base = widget_base(tablebase,/column,/frame)

                ;**** a row for the option switches ****
  row1 = widget_base(base,/row)
  row1l = widget_base(row1,/row)
  row1b = widget_base(row1,/row,/exclusive)
  row2 = widget_base(base,/row)

                ;**** add the switching base and build on it ****
  sw_base = widget_base(row2)
  col1 = widget_base(sw_base,/column)
  col2 = widget_base(sw_base,/column)
  col3 = widget_base(sw_base,/column)

                ;**** the options switches ****
  inpid  = widget_label(row1l, value = 'Input densities:', $
                        font = font_large)
  ienhid = widget_button(row1b, value = 'NE & NH', $
                         /no_release, font = font_large)
  izpdid = widget_button(row1b, value = 'N(z+1)', $
                         /no_release, font = font_large)
  izmdid = widget_button(row1b, value = 'N(z-1)', $
                         /no_release, font = font_large)

		;**** density data table one ****
  colhead1 = [['Electron', 'Ion',      'NH/NE'], $
	     ['Densities','Densities','Ratio']]
		;**** convert FORTRAN index to IDL index ****
  units = ps.idout-1
  unitname = ['cm-3','Reduced']
		;**** Three columns in the table and two sets of units. ****
		;**** The values do not change when the units are switched ****
  unitind = [[0,0],[1,1],[2,2]]
  densid1 = cw_adas_table(col1, densdata1, units, unitname, unitind, $
			UNITSTITLE = 'Density Units', $
			COLHEAD = colhead1, /scroll, $
			TITLE = 'Densities', ORDER = [1,0,0], $
			LIMITS = [2,1,1], CELLSIZE = 12, $
			FONTS = edit_fonts, FONT_LARGE = font_large, $
			FONT_SMALL = font_small)

		;**** Default densities button ****
  base = widget_base(base,/column)
  defdid = widget_button(base,value='Default Densities/Ratios',font=font_large)

		;**** metastable ratios data under the density data table *****
		;**** allow horizontial scroll if wider than density table ****
;  colhead2 = [['Met 1', 'Met 2','Met 3','Met 4'], $
;	     ['Ratio','Ratio','Ratio','Ratio']]

  colhead2 = strarr(ndmet, 2)
  colhead3 = strarr(ndmet, 2)
  for j = 0, ndmet-1 do begin
     colhead2[j,0] = 'Met ' + string(j+1, format='(i2)')
     colhead2[j,1] = 'Ratio'
     colhead3[j,0] = 'Met ' + string(j+1, format='(i2)')
     colhead3[j,1] = 'Ratio'
  endfor

  densid2 = cw_adas_table(col2, densdata2, $
			COLHEAD = colhead2, /scroll, $
			TITLE = 'N(Z+1,M)/N(Z,1)', ORDER = intarr(ndmet) , $
			LIMITS = intarr(ndmet) + 1, CELLSIZE = 12, $
			FONTS = edit_fonts, FONT_LARGE = font_large, $
			FONT_SMALL = font_small, xtexsize=40)

		;**** density data table three ****

  densid3 = cw_adas_table(col3, densdata3, $
			COLHEAD = colhead3, /scroll, $
			TITLE = 'N(Z-1,M)/N(Z,1)', ORDER = intarr(ndmet), $
			LIMITS = intarr(ndmet) + 1, CELLSIZE = 12, $
			FONTS = edit_fonts, FONT_LARGE = font_large, $
			FONT_SMALL = font_small, xtexsize=40)

  botbase = widget_base(parent,/row)
  base = widget_base(botbase,/row,/frame)

                ;**** map the base to the screen ****    
                ;**** taken from defaults file   ****

  if ps.imrout eq 0 then begin
    widget_control, col1, map = 1
    widget_control, col2, map = 0
    widget_control, col3, map = 0
    widget_control, ienhid, /set_button
    widget_control, izpdid, set_button = 0
    widget_control, izmdid, set_button = 0
  end else if ps.imrout eq 1 then begin
    widget_control, col1, map = 0
    widget_control, col2, map = 1
    widget_control, col3, map = 0
    widget_control, ienhid, set_button = 0
    widget_control, izpdid, /set_button
    widget_control, izmdid, set_button = 0
  end else begin
    widget_control, col1, map = 0
    widget_control, col2, map = 0
    widget_control, col3, map = 1
    widget_control, ienhid, set_button = 0
    widget_control, izpdid, set_button = 0
    widget_control, izmdid, /set_button
  end




;  metastables, reactions and spectral intervals               
                
                
  tablebase = widget_base(base,/row)


  ; Metastable selection list

  mbase =  widget_base(tablebase,/column,/frame)
  
  slen  = max(strlen(strga))
  longS = ''
  for j=0, slen-1+4 do longS = longS + 'H'
 
  bd = widget_base(map=0)
  lb = widget_label(bd, /dynamic_resize, value='',font=font_large)
  widget_control, lb, set_value=longS
  wd = (widget_info(lb, /geometry)).scr_xsize  
  xs = fix(wd)

  buttons = strga
  
  tmp     =  widget_label(mbase,value='Metastable State', font=font_large)
  selID   =  cw_bgroup(mbase,buttons,                 $
                       nonexclusive=1,column=1,       $
                       set_value = set_button,        $
                       scroll=1, y_scroll_size=120,   $
                       xsize=xs,x_scroll_size=xs,     $
                       /frame, font=font_large    )
                       
  ; Ask whether to normalise
  
  text0 = "For a single metastable"
  text1 = "normalize PLT and PEC ?"
  base  = widget_base(mbase, /row)
  base1 = widget_base(base, /column)
  l = widget_label(base1, value=text0, font=font_large)
  l = widget_label(base1, value=text1, font=font_large)
  lnormID = cw_bgroup(base1, ["NO","YES"], set_value=1,        $
                       /no_release, /exclusive,               $
                       event_funct = 'ADAS_PROC_NULL_EVENTS', $
                       font = font_large)
 

  ; Reactions to include
  
  mbase   = widget_base(tablebase,/column,/frame)


  react_names = ['Ionisation Rates ',         $
                 'Charge Exchange  ',         $
                 'Recombination    ',         $
                 'Inner Shell Ionisation  ',  $
                 'Include Projection Data ',  $
                 'Proton Impact Collisions']
                 
  tmp     =  widget_label(mbase,value='Include Reactions:', font=font_large)
  reacID  = cw_bgroup(mbase,react_names,nonexclusive=1,column=1, $
                     font=font_large, event_funct = 'ADAS_PROC_NULL_EVENTS')

  ; Ask for zeff for proton impact scaling

  zeffID  = cw_field(mbase, title='Zeff :', $
                     floating=1, fieldfont=font_small, font=font_small) 

                ;**** spawn ionisation search switch ****
  spawnid = widget_button(mbase, $
            value = 'Ionisation Coeff. Search  ',font=font_large)

                ;**** force it off always ****
  widget_control, spawnid, set_button = 0

  ; Spectral intervals
  
  mbase = widget_base(tablebase,/column,/frame)
  
  wvlID = cw_adas_table(mbase, wvldata,                                 $
              coledit=[1,1,1],		                                $
              colhead=['min wave','max wave', 'min A'],/scroll,         $
	      title='Spectral Intervals',limits=[2,2,2],CELLSIZE = 12, 	$
	      fltint=['(f10.2)','(f10.2)','(e10.2)'],                   $
              /rowskip,  /difflen, /gaps,                               $
              FONTS = edit_fonts, font_small=font_small,                $
              font_large=large_font,                                    $
              EVENT_FUNCT = 'ADAS_PROC_NULL_EVENTS')




		;**** Error message ****
  messid = widget_label(parent,font=font_large, $
  value='Edit the processing options data and press Done to proceed')

		;**** add the exit buttons ****
  base = widget_base(parent,/row)
  menufile = bitfile + '/menu.bmp'
  read_X11_bitmap, menufile, bitmap1
  outid = widget_button(base, value = bitmap1)
  cancelid = widget_button(base,value='Cancel',font=font_large)
  doneid = widget_button(base,value='Done',font=font_large)
  
		;**** create a state structure for the pop-up ****
		;**** window.                                 ****
  new_state = { RUNID:runid, SPAWNID:spawnid, $
                INVAL:inval, PS:ps, SZ:sz, SELEM:selem, $
		SELID:selid, TEMPID:tempid, DENSID1:densid1, $
                DENSID2:densid2, DENSID3:densid3, $
		DEFTID:deftid, DEFDID:defdid, OUTID:outid, $
		REACID:reacid, zeffID:zeffID, $
                CANCELID:cancelid, DONEID:doneid, $
		MESSID:messid, NUM_FORM:num_form, $
		NDTEM:ndtem, NDDEN:ndden, NDMET:ndmet, $
		IL:il, NV:nv, TSCEF:tscef, FONT:font_large, $
                IENHID:ienhid, IZPDID:izpdid, IZMDID:izmdid, $
                COL1:col1, COL2:col2, COL3:col3,		$
		WVLID:wvlid,    $
		LNORMID:lnormid,  BASE1:base1 }

                ;**** Save initial state structure ****

  widget_control, first_child, set_uvalue=new_state, /no_copy

  RETURN, parent

END

