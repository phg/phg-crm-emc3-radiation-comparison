; Copyright (c) 1996 Strathclyde University .
;----------------------------------------------------------------------
;+
; PROJECT:
;       ADAS
;
; NAME:
;	B8ISPF
;
; PURPOSE:
;	IDL user interface and communications with ADAS208 FORTRAN
;	process via pipe.
;
; EXPLANATION:
;	The routine begins by reading information from the ADAS208
;	FORTRAN process via a UNIX pipe.  Then part of the ADAS208
;	IDL user interface is invoked to determine how the user
;	wishes to process the input dataset.  When the user's
;	interactions are complete the information gathered with
;	the user interface is written back to the FORTRAN process
;	via the pipe.  Communications are to the FORTRAN subroutine
;	B5ISPF.
;
; USE:
;	The use of this routine is specific to ADAS208, see adas208.pro.
;
; INPUTS:
;	PIPE	- The IDL unit number of the bi-directional pipe to the
;		  ADAS208 FORTRAN process.
;
;	LPEND	- Integer, 0 if the user pressed the 'Done' button
;		  or 1 if the user pressed 'Cancel'.  Maps directly onto
;		  the logical variable LPEND in ADAS208 FORTRAN.
;
;	VALUE	- A structure which determines the initial settings of
;		  the processing options widget.  The initial value is
;		  set in adas208.pro.  If adas208.pro passes a blank 
;		  dummy structure of the form {NMET:-1} into VALUE then
;		  VALUE is reset to a default structure.
;
;		  The VALUE structure is;
;		     {  TITLE:'',	NMET:0, 	$
;			IMETR:intarr(),	IFOUT:0, 	$
;			MAXT:0,		TINE:dblarr(), 	$
;			TINP:dblarr(),	TINH:dblarr(), 	$
;			IDOUT:1,	MAXD:0, 	$
;			MAXRP:0,        MAXRM:0,       	$
;			DINE:dblarr(),	DINP:dblarr(), 	$
;			RATHA:dblarr(),	RATPIA:dblarr(),$
;                       RATMIA:dblarr(,4), IMROUT:0, 	$
;			ZEFF:'',	LPSEL:0, 	$
;			LZSEL:0,	LISEL:0, 	$
;			LHSEL:0,	LRSEL:0,	$
;			LIOSEL:0,       LNSEL:0,        $
;			LNORM:0,		 	$
;	                VAL502:val502  }
;
;		  See cw_adas208_proc.pro for a full description of this
;		  structure and related variables ndmet, ndtem and ndden.
;
;	SZ	- String, recombined ion charge read.
;
;	SZ0	- String, nuclear charge read.
;
;	STRGA	- String array, level designations.
;
;	DSNINC	- String; The full system file name of the input COPASE
;		  dataset selected by the user for processing.
;
;	GOMENU  - Integer; 1=> menu button pressed
;			   0=> not pressed
;	BITFILE - String; file where bimap for menu button is held
;	INVAL 	- Structure; the structure returned by the input screen widget
;	NPL	- No. of metastables (see adas208.f)
;	STRGMI	- String; information written in b8setp.f
;	STRGMF	- String; information written in b8setp.f
;	NDMET	- Number of metastables
;       NDLEV   - Maximum number of levels
;       ISS04A  - Integer truth table for adf04 ionisation rate availability
;
; OPTIONAL INPUTS:
;	None
;
; OUTPUTS:
;	VALUE	- On output the structure records the final settings of
;		  the processing selections widget if the user pressed the
;		  'Done' button, otherwise it is not changed from input.
;
; OPTIONAL OUTPUTS:
;	None
;
; KEYWORD PARAMETERS:
;	FONT_LARGE - The name of a larger font.
;
;	FONT_SMALL - The name of a smaller font.
;
;	EDIT_FONTS - A structure of two fonts used in the table editor
;		     adas_edtab.pro, {font_norm:'',font_input:''}.
;
; CALLS:
;	ADAS208_PROC	Invoke the IDL interface for ADAS208 data
;		 	processing options/input.
;
; SIDE EFFECTS:
;	Two way communications with ADAS208 FORTRAN via the
;	bi-directional UNIX pipe.
;
; CATEGORY:
;	Adas system.
;
; WRITTEN:
;       David H Brooks, University of Strathclyde, 12-Apr-96
;
; MODIFIED:
;	1.0		David H Brooks
;			Written using skeleton of b5ispf v1.7
;                       Altered value to include variables passed to and
;                       possibly changed by 502v208. These are slightly
;                       unusual for value in that they are reset by b8setp
;                       on every run and therefore it is not necessary to
;                       store them as defaults. However, it is easier to
;                       pass them as part of the structure that is already
;                       returned and it maintains the inval,procval,outval
;                       structure normally used for the ADAS project.
;	1.1		William Osborn
;			Minor changes + put under SCCS control
;	1.2		William Osborn
;			Added write of DSFULL to Fortran
;	1.3		William Osborn
;			Set ibsel_arr to 0 rather than -1 at startup
;	1.4		William Osborn
;			Added wavelength and A-value limits for PEC
;			and SXB files. Made reads into dummy variables.
;	1.5		Richard Martin
;			Added write of value.lnorm to pipe.
;	1.6    		Hugh Summers
;			Added acquisition of truth table for ionisation
;                       rate data availability from adf04 file
;	1.7  	        Martin O'Mullane
;		        Send number of wavelength/A-value selection critera
;                       to fortran.
;	1.8  	        Martin O'Mullane
;                        - Use ndmet, and not a hard-coded value of 3, to setup
;                          arrays depending on the number of metastables.
;                        - Add ndsel to argument list in anticipation of
;		           sending user supplied indices of transitions for
;                          inclusion in PEC file. Return that number as 0 now.
;                        - () to [] for arrays 
;
; VERSION:
;	1.0		12-04-96
;	1.1		10-05-96
;	1.2		13-05-96
;	1.3		15-07-96
;	1.4		30-09-96 / 10-10-96
;	1.5		02-07-97
;	1.6		14-09-99
;	1.7	        18-03-13
;	1.8	        02-10-18
;-
;-----------------------------------------------------------------------------


PRO b8ispf, pipe, lpend, value, 					$
		sz, sz0, strga, dsninc,					$
		gomenu, bitfile, inval, npl, strgmi, strgmf, ndmet, 	$
		ndwvl, ndsel, ndlev, iss04a,  		                $
		FONT_LARGE=font_large, FONT_SMALL=font_small, 		$
		EDIT_FONTS=edit_fonts


                ;**** Set defaults for keywords ****
  IF NOT (KEYWORD_SET(font_large)) THEN font_large = ''
  IF NOT (KEYWORD_SET(font_small)) THEN font_small = ''
  IF NOT (KEYWORD_SET(edit_fonts)) THEN $
                                edit_fonts = {font_norm:'',font_input:''}


		;**** Declare variables for input ****
  idum=0
  ddum=0.0d0
  sdum=''
  tscef = dblarr(14,3)

		;********************************
		;**** Read data from fortran ****
		;********************************
  readf,pipe,idum
  lpend=idum
  readf,pipe,sdum
  selem = sdum
  readf,pipe,idum
  ndtem=idum
  readf,pipe,idum
  ndden=idum
  readf,pipe,idum
  il=idum
  readf,pipe,idum
  nv=idum
  for i=0,2 do begin
      for j=0,13 do begin
          readf,pipe,ddum
          tscef[j,i]=ddum
      endfor
  endfor

                ;**** create structure for passing variables ****
                ;**** to the modified version of 502         ****


  ibsela_arr = make_array(ndmet,ndmet,/int,value=0)
  sgrda_arr  = make_array(ndtem,ndmet,ndmet,/double,value=0.0)
  val502 = {        val502,                         $
                    NPL    :npl,                    $
                    IBSELA :ibsela_arr,             $
                    LSSETA :ibsela_arr,             $
                    SGRDA  :sgrda_arr,              $
                    STRGMI :strgmi,                 $
                    STRGMF :strgmf,                 $
		    DSFULL :' ',		    $
		    ISS04A :iss04a		    $
            }
 

		;*******************************************
		;**** Set default value if non provided ****
		;*******************************************
  if value.nmet lt 0 then begin
    imetr = make_array(ndmet,/int,value=0)
    temp = dblarr(ndtem)
    dens = dblarr(ndden)
    ratios = dblarr(ndden,ndmet)
    value = {	TITLE:'',       NMET:0,        $
		IMETR:imetr,    IFOUT:1,       $
		MAXT:0,         TINE:temp,     $
		TINP:temp,      TINH:temp,     $
		IDOUT:1,        MAXD:0,        $
		MAXRP:0,        MAXRM:0,       $
		DINE:dens,      DINP:dens,     $
		RATHA:dens,     RATPIA:ratios, $
                RATMIA:ratios,  IMROUT:0,      $
		ZEFF:'',        LPSEL:0,       $
		LZSEL:0,        LISEL:0,       $
		LHSEL:0,        LRSEL:0,       $
		LIOSEL:0,       LNSEL:0,       $
		LNORM:0,		       $
                VAL502:val502,		       $
		WVLS:dblarr(ndwvl),WVLL:dblarr(ndwvl),	$
		NWVL:1,         AVLT:dblarr(ndwvl)  }
  end else begin
; Uncomment this line if dsfull should not be erased each time processing
; screen started.
;    val502.dsfull = value.val502.dsfull
    value.val502 = val502
  end

		;****************************************
		;**** Pop-up processing input widget ****
		;****************************************
  adas208_proc, value, sz, sz0, strga, dsninc, bitfile, $
		ndtem, ndden, ndmet, ndwvl, il, nv, tscef, action, inval, $
                selem, $
		FONT_LARGE=font_large, FONT_SMALL=font_small, $
		EDIT_FONTS=edit_fonts

		;******************************************
		;**** Act on the event from the widget ****
		;******************************************
		;**** process cancel/done/menu options ****
		;******************************************
  
  if action eq 'Done' then begin
    lpend = 0
    printf, pipe, lpend
  end else if action eq 'Menu' then begin
    lpend = 0
    gomenu = 1
    printf, pipe, lpend
    goto, LABELEND
  endif else begin
    lpend = 1
    printf, pipe, lpend
    goto, LABELEND
  end

		;*******************************
		;**** Write data to fortran ****
		;*******************************
  printf,pipe,value.title
  printf,pipe,value.nmet
  printf,pipe,value.imetr
  printf,pipe,value.ifout
  printf,pipe,value.maxt
  printf,pipe,value.tine
  printf,pipe,value.tinp
  printf,pipe,value.tinh
  printf,pipe,value.idout
  printf,pipe,value.maxd
  printf,pipe,value.dine
  printf,pipe,value.dinp
  printf,pipe,value.ratha
  
  for j = 0, ndmet-1 do begin
      for i = 0, ndden-1 do begin
         printf, pipe, value.ratpia[i,j]
      endfor
  endfor
  for j = 0, ndmet-1 do begin
      for i = 0, ndden-1 do begin
         printf, pipe, value.ratmia[i,j]
      endfor
  endfor
  if value.lzsel eq 1 then begin
    zeff = float(value.zeff)
  end else begin
    zeff = 0.0
  end
  printf,pipe,zeff
  printf,pipe,value.lpsel
  printf,pipe,value.lzsel
  printf,pipe,value.lisel
  printf,pipe,value.lhsel
  printf,pipe,value.lrsel
  printf,pipe,value.liosel
  printf,pipe,value.lnsel
  printf,pipe,value.lnorm
  for i = 0, ndmet-1 do begin
     for j = 0, ndmet-1 do begin
        printf, pipe, value.val502.ibsela[i,j]
     endfor
  endfor 

  for i = 0, ndmet-1 do begin
     for j = 0, ndmet-1 do begin
        printf, pipe, value.val502.lsseta[i,j]
     endfor
  endfor 

  for k = 0, ndtem-1 do begin
     for i = 0, ndmet-1 do begin
        for j = 0, ndmet-1 do begin
           printf, pipe, value.val502.sgrda[k,i,j]
        endfor
     endfor
  endfor 

  printf, pipe, value.val502.dsfull, format='(A)'

  printf, pipe, value.nwvl
  for i = 0, value.nwvl-1 do begin
     printf, pipe, value.wvls[i]
     printf, pipe, value.wvll[i]
     printf, pipe, value.avlt[i]
  endfor

; Ignore ability to set user supplied transition indices for PEC output file

  printf, pipe, 0L
;   printf, pipe, value.ntrsel
;   for i = 0, value.ntrsel-1 do begin
;      printf, pipe, value.itrlow[i]
;      printf, pipe, value.itrup[i]
;   endfor
  
  
LABELEND:

END
