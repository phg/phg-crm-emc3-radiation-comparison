; Copyright (c) 1996, Strathclyde University.
; SCCS info: Module @(#)$Header: /home/adascvs/idl/adas2xx/adas208/cw_adas502v208_proc.pro,v 1.6 2004/07/06 12:51:10 whitefor Exp $ Date $Date: 2004/07/06 12:51:10 $
;+
; PROJECT:
;       ADAS IBM MVS to UNIX conversion
;
; NAME:
;	CW_ADAS502V208_PROC()
;
; PURPOSE:
;	Produces a widget for ADAS502V208 processing options/input.
;
; EXPLANATION:
;	This function creates a compound widget consisting of :-
;	   a text widget holding an editable 'Run title', 
;	   the dataset name/browse widget ,
;	   a widget to select the transition for analysis,
;	   a widget to request a mimax fit and enter a tolerance for it,
;	   a table widget for temperature data ,
;	   a button to enter default values into the table, 
;	   a message widget, 
;	   an 'Escape to series menu', a 'Cancel' and a 'Done' button.
;
;	The compound widgets included in this widget are self managing.
;	This widget only manages events from the 'Defaults' button,
;	the 'Cancel' button and the 'Done' button and the 
;       'escape to series menu' button.
;
;
; USE:
;	This widget is specific to ADAS502V208, see adas502V208_proc.pro
;	for use.
;
; INPUTS:
;	TOPPARENT- Long integer, ID of parent widget.
;
;	DSFULL	- String; The full system file name of the input 
;		          dataset selected by the user for processing.
;
;	NSTORE	- Integer : Maximum number of data blocks which can be
;                           read from the data set.
;
;	NTDIM   - Integer : Maximum number of temperatures from data file.
;
;	NDTIN	- Integer : Maximum number of temperatures allowed.
;
;	NBSEL	- Integer : Number of data blocks accepted and read in
;
;	CICODE	- String array : Initial state metastable index of each
;                                block
;               
;	CFCODE	- String array : Final state metastable index of each
;                                blocks
;
;	CIION	- String array : Initial state ion of each block
;               
;	CFION	- String array : Final state ion of each block	
;
;	BWNO	- String array : Ionisation potential of each block	
;
;	ITA	- Integer array: Number of temperatures for each block
;
;	TVALS	- Double array : Electron temperatures for each block
;				 1st dimension - temperature index
;				 2nd dimension - units 1 => Kelvin
;						       2 => eV
;						       3 => Reduced
;
;       BITFILE - String; the path to the directory containing bitmaps
;                 for the 'escape to series menu' button.
;	
;	MSIZE 	- Integer; size of the metastable list
;	STRGMM 	- String array; info strings
;	IMETR   - Integer array; metastable indices
;	SZ	- String; Ion charge
;	SELEM	- String; element symbol
;	NDMET	- Integer; Number of metastables
;
;
; OPTIONAL INPUTS:
;	None
;
; OUTPUTS:
;	The return value of this function is the ID of the declared widget.
;
;	ACT 	- String; result of this widget, 'done' or 'cancel'
;
; OPTIONAL OUTPUTS:
;	None
;
; KEYWORD PARAMETERS:
;	PROCVAL	- A structure which determines the initial settings of
;		  the processing options widget.
;
;		  The default PROCVAL is created thus;
;
;   		 ps = {proc502v208_set, 		$
;				new   : 0 ,             $
;				title : '',		$
;				ibsel : 0 ,     	$
;				imsel : 0 ,     	$
;				itval : 0,              $
;                		ifout : 1, 		$
;				tine  : temp_arr,	$
;				lfsel : 0,		$
;				tolval: 5,              $
;				hlist : hlist_arr,	$
;                		losel : 0               $
;       	      }
; 
;
;		NEW     Flag which defines whether or not default values
;			exist or not. (< 0 if no values exist)
; 		TITLE	Entered general title for program run
;		IBSEL   Number of temperature value selected
;		IMSEL   Number of matching metastable selected
;		IFOUT   Index indicating which units are being used
;		ITVAL   Number of temps in tine
;		TINE    User supplied temperature values for fit.
;		LFSEL   Flag as to whether polynomial fit is chosen
;		TOLVAL  Tolerance required for goodness of fit if
;			polynomial fit is selected.
;		HLIST   Integer array. -1 => level not recorded yet
;		LOSEL   Flag whether or not interpolated values for spline
;			fit will be used.
;
;		Many of these structure elements map onto variables of
;		the same name in the ADAS502 FORTRAN program.
;
;
;	UVALUE	- A user value for the widget. Default 0.
;
;	FONT_LARGE - The name of a larger font.  Default current system
;		     font
;
;	FONT_SMALL - The name of a smaller font. Default current system
;		     font.
;
;	EDIT_FONTS - A structure of two fonts used in the table editor
;		     adas_edtab.pro, {font_norm:'',font_input:''}.  Defaults
;		     to current system font.
;
;	NUM_FORM   - String; Numeric format to use in tables.  Default
;			'(E10.3)'
;		     
;
; CALLS:
;	POPUP		Popup warning window with buttons.
;	CW_ADAS_DSBR	Dataset name and comments browsing button.
;	CW_ADAS_TABLE	Adas data table widget.
;	CW_ADAS_SEL	Adas multiple selection widget.
;	CW_SINGLE_SEL	Adas scrolling table and selection widget.
;	CW_OPT_VALUE    Adas option selection widget with required input 
;			value. 
;
;	See side effects for other related widget management routines.
;
; SIDE EFFECTS:
;
;	The following widget management routines are included in this file;
;	PROC502V208_GET_VAL()	Returns the current PROCVAL structure.
;	PROC502V208_EVENT()		Process and issue events.
;	
; CATEGORY:
;	Compound Widget
;
; WRITTEN:
;       David H.Brooks, Universiy of Strathclyde, date unknown
;
; MODIFIED:
;       1.0     David Brooks
;		Written using cw adas502_proc.pro v1.8 as a template
;	1.1    	William Osborn
;		Added documentation + put under SCCS control
;	1.2    	William Osborn
;		Commented-out code to change temperatures on selecting
;		index
;	1.3    	William Osborn
;		'tine' array set to value in temp. box passed from
;		208 rather than value in adf07 file.
;       1.4     William Osborn
;               Altered formatting of mblock
;	1.5    	Hugh Summers
;		Added display of truth table for ionisation
;               rate data availability from adf04 file
;	1.6    	Martin O'Mullane
;		Array of 1 problem with IDL v5.5 and above.
;
; VERSION:
;	1.0	????????
;	1.1	10-05-96
;	1.2	13-05-96
;	1.3	13-05-96
;	1.4	10-10-96
;	1.5	14-09-99
;	1.6	14-05-2003
;
;-
;-----------------------------------------------------------------------------

FUNCTION proc502v208_get_val, id

                ;**** Return to caller on error ****

    ON_ERROR, 2

		 ;***************************************
                 ;****     Retrieve the state 	     ****
		 ;**** Get first_child widget id     ****
		 ;**** because state is stored there ****
		 ;***************************************

    first_child = widget_info(id, /child)
    widget_control, first_child, get_uvalue = state

		;***********************************
		;**** Get run title from widget ****
		;**** Then centre in in string  ****
		;**** of 40 characters 		****
		;***********************************

    widget_control, state.runid, get_value=title
    title = strcompress(title(0))
    title_len = strlen(title) 
    if (title_len gt 40 ) then begin 
	title = strmid(title,0,37)
        widget_control, state.messid, set_value="Title too long - truncated "
        widget_control, state.runid, set_value=title
	wait, 1
    endif
    pad = (40 - title_len)/2 
    spaces = '                                         '
    title(0) = strmid(spaces,0,(pad-1)) + title + strmid(spaces,0,(pad-1))

		;****************************************************
		;**** Get new temperature data from table widget ****
		;**** If losel is selected                       ****
		;****************************************************

    widget_control, state.tempid, get_value=tempval
    for i=0,n_elements(tempval)-2 do begin
	tempval(i) = strmid(tempval(i+1),3,strlen(tempval(i+1))-3)
    endfor
    tempval(n_elements(tempval)-1)=' '
    ifout = state.ifout

		;**** Copy out temperature values ****
    tine = dblarr(state.ntdim)
    blanks = where(strtrim(tempval,2) eq '')

                ;***********************************************
		;**** next line assumes that all blanks are ****
		;**** at the end of the columns             ****
                ;***********************************************

    if blanks(0) ge 0 then itval=blanks(0) else itval=state.ntdim

                ;*************************************************
                ;**** Only perform following if there is 1 or ****
                ;**** more value present in the table         ****
                ;*************************************************

    if itval ge 1 then begin
        tine(0:itval-1) = double(tempval(0:itval-1))
    endif

		;**** Fill out the rest with zero ****

    if itval lt state.ntdim then begin
        tine(itval:state.ntdim-1) = double(0.0)
    endif

		;*************************************************
		;**** Get selection of polyfit from widget    ****
		;*************************************************

    widget_control, state.optid, get_uvalue=polyset, /no_copy
    lfsel = polyset.optionset.option[0]
    if (num_chk(polyset.optionset.value[0]) eq 0) then begin
        tolval = (polyset.optionset.value[0])
    endif else begin
        tolval = -999
    endelse
    widget_control, state.optid, set_uvalue=polyset, /no_copy

		;***********************************************
		;**** get new index for data block selected ****
		;***********************************************

    widget_control, state.indexid, get_value=select_block
    ibsel = select_block  

		;************************************************
		;**** get index of final metastable selected ****
		;************************************************

    widget_control, state.mindxid, get_value=mselect_block
    imsel = mselect_block
  
		;***********************************************
		;**** write selected values to PS structure ****
		;***********************************************

     ps = {	new   	: 	0, 					$
                title 	: 	title,          			$
		ibsel 	: 	ibsel,					$
		imsel 	: 	imsel,					$
		ifout 	: 	ifout,					$
		itval 	: 	itval,					$
                tine  	: 	tine,       				$
                lfsel 	: 	lfsel,              			$
                tolval	: 	tolval,					$
                hlist	: 	state.hlist,				$
		losel 	: 	state.losel				}

   
    widget_control, first_child, set_uvalue=state, /no_copy

    RETURN, ps

END

;-----------------------------------------------------------------------------

FUNCTION proc502v208_event, event

                ;**** Base ID of compound widget ****

    parent = event.handler

		;**********************************************
                ;**** Retrieve the user value state        ****
		;**** Get id of first_child widget because ****
		;**** user value "state" is stored there   ****
		;**********************************************

    first_child = widget_info(parent, /child)
    widget_control, first_child, get_uvalue=state, /no_copy

		;*********************************
		;**** Clear previous messages ****
		;*********************************

    widget_control, state.messid, set_value=' '

                ;************************
                ;**** Process events ****
                ;************************

    CASE event.id OF

		;*************************************
		;****      Select index           ****
		;*************************************

    state.indexid: begin

		;**** Get current index select value ****

        widget_control, state.indexid, get_value=ibsel
        ntdim = state.ntdim
        ntval = state.ita(ibsel)
;	ndtin=state.ndtin
;
;		;**** Get current table widget value ****
;
;	widget_control, state.tempid, get_value=tempval
;
;		;**** Copy new ibsel values into structure  ****
;
;  	if (ntval gt 0) then begin
;  	    tempval(0) = 'INDEX  Temp.'
;            tempval(1:ndtin) = string(findgen(ndtin)+1,format='(I2)') + '     '
;    	    tempval(1:ntval ) = tempval(1:ntval)+				$
;		strtrim(string(state.tvals(0:ntval-1,state.ifout-1,ibsel),	$
;	        format=state.num_form),2)
;
;		;**** fill rest of table with blanks ****
;
;	    print,ndtin
;	    if (ntval lt ndtin ) then begin
;    	   	tempval(ntval+1:ndtin) = tempval(ntval+1:ndtin)+' '
;    	    endif
;
;		;**** Copy new data to table widget ****
;		;**** reset state.ibsel             ****
;
; 	    widget_control, state.tempid, set_value=tempval
	    state.ibsel = ibsel
;  	endif
    end

    state.mindxid: begin
                ;**** get current metastable index select value ****
      
         widget_control, state.mindxid, get_value = imsel
         state.imsel = imsel
    end

		;***********************
		;**** Cancel button ****
		;***********************

    state.cancelid: new_event = {ID:parent, TOP:event.top, 		$
				 HANDLER:0L, ACTION:'Cancel'}

		;*********************
		;**** Done button ****
		;*********************

    state.doneid: begin

		;***************************************
		;**** Check all user input is legal ****
		;***************************************

	error = 0

        	;********************************************
		;*** Have to restore "state" before calls ***
		;*** to proc502V208_get_val so it can be  ***
		;*** used there.			  ***
		;********************************************
		
	widget_control, first_child, set_uvalue=state, /no_copy

	widget_control, event.handler, get_value=ps

		;*** reset state variable ***

	widget_control, first_child, get_uvalue=state, /no_copy

		;**** check temp values entered ****

	if (ps.losel eq 1) then begin
	    if error eq 0 and ps.itval eq 0 then begin
	        error = 1
	        message='**** Error: No temperatures entered ****'
            endif
	endif

		;*** Check to see if index has been selected ***

	if error eq 0 and ps.ibsel lt 0 then begin
	    error = 1
	    message='**** Error: Invalid block selected ****'
	endif

		;*** Check to see if sensible tolerance is selected.

	if (error eq 0 and ps.lfsel eq 1) then begin 
	    if (float(ps.tolval) lt 0)  or 				$
	    (float(ps.tolval) gt 100) then begin
	        error = 1
	        message='**** Error: Tolerance for polyfit must be '+	$
                        '0-100% ****'
	    endif
	endif

		;**** return value or flag error ****

	if error eq 0 then begin
	    new_event = {ID:parent, TOP:event.top, HANDLER:0L, ACTION:'Done'}
        endif else begin
	    widget_control, state.messid, set_value=message
	    new_event = 0L
        endelse
    end

                ;**** Menu button ****

    state.outid: begin
        new_event = {ID:parent, TOP:event.top, HANDLER:0L,          $
                     ACTION:'Menu'}
    end

    ELSE: new_event = 0L

    ENDCASE

		;*********************************************
		;*** make "state" available to other files ***
		;*********************************************

    widget_control, first_child, set_uvalue=state, /no_copy

    RETURN, new_event
END

;-----------------------------------------------------------------------------

FUNCTION cw_adas502v208_proc, topparent, dsfull, act, nstore, ntdim, 	$
                          ndtin, nbsel, cicode, cfcode, ciion, cfion, 	$
                          bwno, ita, tvals, bitfile, msize, strgmm,     $
                          imetr, sz, selem, ndmet, procval=procval,     $
                          uvalue=uvalue, font_large=font_large, 	$
                          font_small=font_small, edit_fonts=edit_fonts,	$
                          num_form=num_form

		;**** Set defaults for keywords ****

    if not (keyword_set(uvalue)) then uvalue = 0
    if not (keyword_set(font_large)) then font_large = ''
    if not (keyword_set(font_small)) then font_small = ''
    if not (keyword_set(edit_fonts)) then 				$
    edit_fonts = {font_norm:'',font_input:''}
    if not (keyword_set(num_form)) then num_form = '(E10.3)'
    if not (keyword_set(procval)) then begin
        nmettot = ndmet*ndmet
        hlist_arr = make_array(nmettot,/int,value=-1)
        ps = { 	new   	: 	0, 					$
		title 	: 	'',             			$
		ibsel 	: 	0,					$
		imsel 	: 	0,					$
		itval 	: 	ndtin,					$
		ifout 	: 	1,					$
                tine  	: 	tvals(*,0,0),     			$
                lfsel 	: 	0,              			$
                tolval	: 	'5',					$
                hlist	: 	hlist_arr,				$
		losel 	: 	1					}
    endif else begin
	ps = {  new   	: 	procval.new,    			$
                title 	: 	procval.title,  			$
                ibsel 	: 	procval.ibsel,  			$
                imsel 	: 	procval.imsel,  			$
                itval 	: 	procval.itval,  			$
                ifout 	: 	procval.ifout,  			$
                tine  	: 	procval.tine,   			$
                lfsel 	: 	procval.lfsel,  			$
                tolval	: 	procval.tolval, 			$
                hlist	: 	procval.hlist,  			$
		losel 	: 	1               			}
    endelse

                ;*********************************************************
                ;**** Modify certain parameters and results depending ****
                ;**** on the machine being used                       ****
                ;*********************************************************

    machine = GETENV('TARGET_MACHINE')
    if machine eq 'HPUX' then begin
        y_size = 4
        large_font = font_small
    endif else begin
        y_size = 6
        large_font = font_large
    endelse
              
		;****************************************************
		;****       Assemble temperature table data      ****
		;****************************************************
		;**** The adas table widget requires data to be *****
		;**** input as strings, so all the numeric data *****
		;**** has to be written into a string array.    *****
		;****     Declare temp.table array              *****
		;**** col 1 has user temperature values         *****
		;**** col 2-4 havetemperature values from files,*****
		;**** which can have one of three possible units*****
		;****************************************************

    if ps.ibsel lt nbsel then userindex=ps.ibsel else userindex=0
    itval = ps.itval
    ibsel = userindex
    ntval=ita(ibsel)
    tabledata = strarr(4,ndtin)

		;**** Copy out temperature values ****
		;**** number of temperature       ****
		;**** values for this data block  ****
;was ps.tine(*) but i am only taking first 24****
    if (ntval gt 0) then begin
        tabledata(0,*) = strtrim(string(ps.tine(0:ndtin-1),format=num_form),2)
        tabledata(1,0:ntval-1) = 					$
        strtrim(string(tvals(0:ntval-1,0,ibsel),format=num_form),2)
        tabledata(2,0:ntval-1) = 					$
        strtrim(string(tvals(0:ntval-1,1,ibsel),format=num_form),2)
        tabledata(3,0:ntval-1) = 					$
        strtrim(string(tvals(0:ntval-1,2,ibsel),format=num_form),2)

		;**** fill rest of table with blanks ****
    
        blanks = where(tabledata eq 0.0)
        if (blanks(0) ge 0) then begin
	    tabledata(blanks) = ' '
        endif
    endif

		;********************************************************
		;**** Create the 501 Processing options/input window ****
		;********************************************************

		;**** create titled base widget ****

    parent = widget_base(topparent, UVALUE = uvalue, 			$
			 title = 'ADAS502(208) PROCESSING OPTIONS', 	$
			 EVENT_FUNC = "proc502v208_event", 		$
			 FUNC_GET_VALUE = "proc502v208_get_val", 	$
			 /COLUMN, XOFFSET=1, YOFFSET=1)

		;******************************************************
		;**** Create a dummy widget just to hold value of *****
		;**** "state" variable so as not to get confused  *****
		;**** with any other values. Adopt IDL practice   *****
		;**** of using first child widget                 *****
		;******************************************************

    first_child = widget_base(parent)
    topbase = widget_base(first_child, /column)

		;***********************
		;**** add run title ****
		;***********************

    base = widget_base(topbase, /row)
    rc = widget_label(base, value='Title for Run', font=large_font)
    runid = widget_text(base, value=ps.title, xsize=38, font=large_font, /edit)

		;**** add dataset name and browse button ****

    rc = cw_adas_dsbr(topbase, dsfull, font=large_font)

		;**** create a new row parent for 502v208 ****

    row_prnt = widget_base( parent, /row, /frame )

		;***************************************************
		;**** add a window to display and select index   ***
		;**** first create data array for table          ***
		;**** Then convert to 1D string array(text_table)***
		;**** call cw_single_sel.pro for options choice  ***
		;***************************************************
    block_info = strarr(4, nbsel)
    block_info(0,*) = ciion
    block_info(1,*) = cicode
    block_info(2,*) = cfion
    block_info(3,*) = cfcode
    titles = [['Initial  ', 'Metastable', 'Final    ', 'Metastable'], 	$
   	      ['Ion','Index','Ion', 'Index']]
    select_data = text_table(block_info, colhead=titles)
    coltitles = select_data(0:1)
    select_data = select_data(2:nbsel+1)
    titl1 =  '          '
    titl2 =  'Select data Block'
    titl3 =  '          '
    titl_arr = strarr(3)
    titl_arr(0) = titl1
    titl_arr(1) = titl2
    titl_arr(2) = titl3
    indexid = cw_single_sel( row_prnt, select_data, value=userindex,	$
			     title= titl_arr,           		$
			     coltitles=coltitles, ysize = 5, 		$
			     font=font_small, big_font=large_font)
                ;***************************************************
		;**** add a window to display and select indices ***
		;**** for matching initial and final metastables ***
		;***************************************************
    if ps.imsel lt msize then mindex=ps.imsel else mindex=0
    mblock_info = strgmm + "       "
    for i = 0, msize-1 do begin
      if ps.hlist(i) ne -1 then begin
        dummy_str = strcompress(mblock_info(i), /remove_all) +'   Rec.'
        mblock_info(i) = dummy_str
      end
    endfor
    mtitles = [['Initial     Final     '],['metastable  metastable']]
    msel_data = text_table(mblock_info, colhead=mtitles)
    mcolt = msel_data(0:1)
    msel_data = msel_data(2:msize+1)
    sprnt = strtrim(string(long(sz)+1),2)
    mtitl1 = 'Initial ion: '+selem+strtrim(sz,0)+'  Final ion: '+$
             selem+' '+strtrim(sprnt,0)
    mtitl2 = 'Select matching states'
    mtitl3 = '# => data already found in adf04 file'
    mtitl_array = strarr(3) 
    mtitl_array(0) = mtitl1 
    mtitl_array(1) = mtitl2 
    mtitl_array(2) = mtitl3 
    
    mindxid = cw_single_sel( row_prnt, msel_data, value = mindex,       $
                             title=mtitl_array,                         $
                             coltitles=mcolt, ysize = 5,                $
                             font = font_small, big_font = large_font )

		;**********************
		;**** Another base ****
		;**********************

    tablebase = widget_base(parent,  /row)

		;************************************************
		;**** base for the table and defaults button ****
		;**** and whether table used for text output ****
		;************************************************

    base = widget_base(tablebase, /column, /frame)


		;********************************
		;**** temperature data table ****
		;********************************

    tab = strarr(ndtin+1)
    tab(0) = 'INDEX  Temp.'
    tab(1:ndtin)=string(findgen(ndtin)+1,format='(I2)') + '     '+	$
			tabledata(0,*)
    lab = widget_label(base, value='Output electron temperatures',	$
		       font=large_font)
    tempid = widget_text(base, /scroll, value = tab, ysize = y_size,	$
			 font=large_font)

		;********************************************
		;**** convert FORTRAN index to IDL index ****
		;********************************************

    units = ps.ifout-1 
    unitname = ['Kelvin','eV','Reduced']

    lab = widget_label(base,value='Temperature units: '+unitname(units),$
		       font=large_font)

		;**** Two  columns in the table and three sets of units. ****
		;**** Column 1 has the same values for all three ****
		;**** units but column 2 switches between sets 2,3 & 4 ****
		;**** in the input array tabledata as the units change.  ****

;		;*************************
;		;**** Default button  ****
;		;*************************

;    deftid = widget_button(base, value=' Default Temperature Values  ',	$
;		           font=large_font)

    deftid = 0L
		;*** Make table non/sensitive as ps.losel ***

;    widget_control, tempid, sensitive = ps.losel
;    widget_control, deftid, sensitive = ps.losel

		;**************************************
		;**** Add polynomial fit selection ****
		;**************************************

    polyset = {option:intarr(1), val:strarr(1)} ; defined thus because 
						; cw_opt_value.pro
                                                ; expects arrays
    polyset.option = [ps.lfsel]
    polyset.val = [ps.tolval]
    options = ['Fit Polynomial']
    optbase = widget_base(topbase, /frame)
    optid = cw_opt_value(optbase, options, title='Polynomial Fitting',	$
			 limits = [0,100], value=polyset, 		$
                         font=large_font)

		;**** Error message ****

    messid = widget_label(parent, font=large_font, 			$
    value='Edit the processing options data and press Done to proceed')

		;**** add the exit buttons ****

    base = widget_base(parent,/row)
    menufile = bitfile + '/menu.bmp'
    read_X11_bitmap, menufile, bitmap1
    outid = widget_button(base, value=bitmap1)		;menu button
    cancelid = widget_button(base, value='Cancel', font=large_font)
    doneid = widget_button(base, value='Done', font=large_font)
  
		;**** create a state structure for the pop-up ****
		;**** 		     window.                  ****

    new_state = { 	runid		:	runid,  		$
			messid		:	messid, 		$
			deftid		:	deftid,			$
			tempid		:	tempid, 		$
			losel 		: 	ps.losel, 		$
			hlist 		: 	ps.hlist, 		$
			optid		:	optid,			$
 			indexid		:	indexid,		$
 			mindxid		:	mindxid,		$
			cancelid	:	cancelid,		$
			doneid		:	doneid, 		$
	        	outid		:	outid,			$
			dsfull		:	dsfull,			$
			nbsel 		:	nbsel, 			$
			nstore		:	nstore,			$
			ntdim		:	ntdim,			$
                	ndtin		:	ndtin,            	$
			ibsel		:	ibsel,			$
			imsel		:	ps.imsel,		$
			itval		:	itval, 			$
			ifout		:	ps.ifout,		$
			ntval		:	ntval,			$
			ita		:	ita,			$
			tvals		:	tvals,			$
			font		:	large_font,		$
			num_form	:	num_form		}

                 ;**** Save initial state structure ****

    widget_control, first_child, set_uvalue=new_state, /no_copy

    RETURN, parent

END

