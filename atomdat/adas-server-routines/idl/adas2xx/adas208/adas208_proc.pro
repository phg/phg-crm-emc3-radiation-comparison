; Copyright (c) 1995 Strathclyde University .
; SCCS info: Module @(#)$Header: /home/adascvs/idl/adas2xx/adas208/adas208_proc.pro,v 1.3 2013/03/18 23:14:45 mog Exp $ Date $Date: 2013/03/18 23:14:45 $
;+
; PROJECT:
;       ADAS IBM MVS to DEC UNIX conversion
;
; NAME:
;	ADAS208_PROC
;
; PURPOSE:
;	IDL ADAS user interface, processing options/input.
;
; EXPLANATION:
;	This routine creates and manages a pop-up window which allows
;	the user to select options and input data to control ADAS208
;	processing.
;
; USE:
;	This routine is ADAS208 specific, see b8ispf.pro for how it
;	is used.
;
; INPUTS:
;	VAL	- A structure which determines the initial settings of
;		  the processing options widget.  The value is passed
;		  unmodified into cw_adas208_proc.pro.
;
;		  See cw_adas208_proc.pro for a full description of this
;		  structure.
;
;	SZ	- String, recombined ion charge read.
;
;	SZ0	- String, nuclear charge read.
;
;	STRGA	- String array, level designations.
;
;	DSNINC	- String; The full system file name of the input COPASE
;		  dataset selected by the user for processing.
;
;       BITFILE - String; the path to the directory containing bitmaps
;                 for the 'escape to series menu' button.
;
;	NDTEM	- Integer; Maximum number of temperatures allowed.
;
;	NDDEN	- Integer; Maximum number of densities allowed.
;
;	NDMET	- Integer; Maximum number of metastables allowed.
;
;	IL	- Integer; Number of energy levels.
;
;	NV	- Integer; Number of termperatures.
;
;	TSCEF	- dblarr(8,3); Input electron temperatures in three units.
;
;	INVAL	- Structure; the input screen settings. See cw_adas208_in.pro
;
;	SELEM	- String; The element symbol, passed from Fortran
;
; OPTIONAL INPUTS:
;	None
;
; OUTPUTS:
;	VAL	- On output the structure records the final settings of
;		  the processing selections widget if the user pressed the
;		  'Done' button, otherwise it is not changed from input.
;
;	ACT	- String; Either 'Done' or 'Cancel' for the button the
;		  user pressed to terminate the processing options
;		  window.
;
; OPTIONAL OUTPUTS:
;	None
;
; KEYWORD PARAMETERS:
;	FONT_LARGE - The name of a larger font.
;
;	FONT_SMALL - The name of a smaller font.
;
;	EDIT_FONTS - A structure of two fonts used in the table editor
;		     adas_edtab.pro, {font_norm:'',font_input:''}.
;
; CALLS:
;	CW_ADAS208_PROC	Declares the processing options widget.
;	DYNLABEL	Sets dynamic_resize keyword for label widgets.
;	ADAS208_PROC_EV Called indirectly during widget management,
;			routine included in this file.
;	XMAMAGER
;
; SIDE EFFECTS:
;	This routine uses a common block PROC208_BLK in the management
;	of the pop-up window.
;
; CATEGORY:
;	Widgets
;
; WRITTEN:
;       David H.Brooks, Universiy of Strathclyde, date unknown
;
; MODIFIED:
;       Version 1.0     David Brooks
;			Written using adas205_proc.pro v1.5 as a template
;	Version 1.1    	William Osborn
;			Added documentation + put under SCCS control
;	Version 1.2    	William Osborn
;			Added dynlabel procedure
;	Version 1.2    	Martin O'Mullane
;			Add ndwvl argument. 
;
; VERSION:
;	1.0	????????
;	1.1	10-05-96
;	1.2	09-07-96
;	1.3	18-03-13
;
;-
;-----------------------------------------------------------------------------


PRO adas208_proc_ev, event

  COMMON proc208_blk,action,value

		;**** Find the event type and copy to common ****
    action = event.action

    CASE action OF

		;**** 'Done' button   ****
	'Done'  : begin

			;**** Get the output widget value ****
		widget_control,event.id,get_value=value 
		widget_control,event.top,/destroy

	   end


		;**** 'Cancel' button ****
	'Cancel': widget_control,event.top,/destroy

                ;**** 'Menu' button   ****

        'Menu'  : widget_control, event.top, /destroy

        ELSE:   ;**** do nothing ****

    END

END

;-----------------------------------------------------------------------------


PRO adas208_proc, val, sz, sz0, strga, dsninc, bitfile, $
		ndtem, ndden, ndmet, ndwvl, il, nv, tscef, act, inval, selem, $
		FONT_LARGE=font_large, FONT_SMALL=font_small, $
		EDIT_FONTS=edit_fonts

  COMMON proc208_blk,action,value

		;**** Copy value to common ****
  value = val

		;**** Set defaults for keywords ****
  IF NOT (KEYWORD_SET(font_large)) THEN font_large = ''
  IF NOT (KEYWORD_SET(font_small)) THEN font_small = ''
  IF NOT (KEYWORD_SET(edit_fonts)) THEN $
				edit_fonts = { font_norm:'', font_input:''}

                ;**** create base widget ****
  procid = widget_base(TITLE='ADAS208 PROCESSING OPTIONS', $
					XOFFSET=50,YOFFSET=0)

		;**** Declare processing widget ****
  cwid = cw_adas208_proc(procid, sz, sz0, strga, dsninc, ndtem, ndden, $
			ndmet, ndwvl, il, nv, tscef, bitfile, inval, selem, $
			VALUE=value, $
			FONT_LARGE=font_large, FONT_SMALL=font_small, $
			EDIT_FONTS=edit_fonts)

		;**** Realize the new widget ****
  dynlabel, procid
  widget_control,procid,/realize

		;**** make widget modal ****
  xmanager,'adas208_proc',procid,event_handler='adas208_proc_ev', $
					/modal,/just_reg
 
		;**** Return the output value from common ****
  act = action
  val = value

END

