; Copyright (c) 1996, Strathclyde University.
; SCCS info: Module @(#)$Header: /home/adascvs/idl/adas2xx/adas208/e2b8outg.pro,v 1.1 2004/07/06 13:37:29 whitefor Exp $ Date $Date: 2004/07/06 13:37:29 $
;+
; PROJECT:
;       ADAS IBM MVS to UNIX conversion
;
; NAME:
;	E2B8OUTG
;
; PURPOSE:
;	Communication with ADAS502V208 FORTRAN via UNIX pipe and
;	graphics output.
;
; EXPLANATION:
;	The routine begins by reading information from the ADAS502V208
;	FORTRAN process via a UNIX pipe.  Then the IDL graphical
;	output routine for ADAS502V208 is invoked.  Communications are to
;	the FORTRAN subroutine E2OUTG.
;
; USE:
;	The use of this routine is specific to ADAS502V208 see adas502v208.pro.
;
; INPUTS:
;	DSFULL   - Data file name
;	PIPE	 - The IDL unit number of the bi-directional pipe to the
;		   ADAS502V208 FORTRAN process.
;
;	UTITLE   - Optional comment by user
;
;	GRPSCAL	 - Integer; 0 if default scaling required 1 if user
;		   specified scaling to be used.
;
;	XMIN	 - String; User sepcified x-axis minimum, number as string.
;
;	XMAX	 - String; User sepcified x-axis maximum, number as string.
;
;	YMIN	 - String; User specified y-axis minimum, number as string.
;
;	YMAX	 - String; User specified y-axis maximum, number as string.
;
;	HRDOUT	 - Integer; 0 if no hardcopy required, 1 if hardcopy.
;
;	HARDNAME - String; file name for hardcopy output.
;
;	DEVICE	 - String; IDL name for hardcopy output device.
;
;	HEADER	 - ADAS version header information for inclusion in the
;		   graphical output.
;
;       BITFILE  - String; the path to the directory containing bitmaps
;                  for the 'escape to series menu' button.
;
; OPTIONAL INPUTS:
;	None.
;
; OUTPUTS:
;	GOMENU   - 1 => menu button pressed
;		  0 => not pressed
;
;	RECORD   - 1 => record button pressed on graph plotting screen
;		  0 => not pressed
;
;       SZDA_SEL - spline interpolated or  extrapolated zero- density
;		   ionization rate-coefficients for the user entered electron
;		   temperatures.
;
;  	TEVA_SEL - user entered: electron temperatures (ev) dimension:
;		   electron temperature index
;
;	ITVAL_SEL- number of user-entered temperatures
;
; OPTIONAL OUTPUTS:
;	None.
;
; KEYWORD PARAMETERS:
;	FONT	- String; Name of the font to be used for the graphical
;		  output widget.
;
; CALLS:
;	ADAS502V208_PLOT	ADAS502V208 graphical output.
;
; SIDE EFFECTS:
;	This routine reads information from FORTRAN via a UNIX pipe.
;
; CATEGORY:
;	Adas system.
;
; WRITTEN:
;       David H.Brooks, Universiy of Strathclyde, date unknown
;
; MODIFIED:
;       Version 1.0     David Brooks
;			Written using e2outg.pro v1.3 as a template
;	Version 1.1    	William Osborn
;			Added documentation + put under SCCS control
;
; VERSION:
;	1.0	????????
;	1.1	10-05-96
;
;-
;-----------------------------------------------------------------------------

PRO e2b8outg, dsfull, pipe, utitle, grpscal, xmin, xmax, ymin, ymax,     $
	      hrdout, hardname, device, header, bitfile, gomenu, record, $
              szda_sel, teva_sel, itval_sel, FONT=font


                ;**** Set defaults for keywords ****


    IF NOT (KEYWORD_SET(font)) THEN font = ''


		;**** Declare variables for input ****

    sdum = " "
    idum = 0
    fdum = 0.0
    strg = make_array(9, /string, value=" ")
    nmx = 0

		;********************************
		;**** Read data from fortran ****
		;********************************

    readf, pipe, format = '(a40)' , sdum
    title = sdum
    readf, pipe, format = '(a120)', sdum
    titlx = sdum
    readf, pipe, format = '(a80)' , sdum
    titlm = sdum
    readf, pipe, format = '(a8)' , sdum
    date = sdum
    readf, pipe, format = '(a2)' , sdum
    esym = sdum
    readf, pipe, idum
    iz0 = idum
    readf, pipe, idum
    iz1 = idum
    readf, pipe, idum
    bwno = idum
    readf, pipe, format = '(a5)' , sdum
    ciion = sdum
    readf, pipe, format = '(a2)' , sdum
    cicode = sdum
    readf, pipe, format = '(a5)' , sdum
    cfion = sdum
    readf, pipe, format = '(a2)' , sdum
    cfcode = sdum
    readf, pipe, idum
    itval = idum

		;**** now declare array dimensions ****

    teva = dblarr(itval) 
    szda = dblarr(itval) 
    for i=0, itval-1 do begin
        readf, pipe, fdum
        teva(i) = fdum 
    endfor
    for i=0, itval-1 do begin
        readf, pipe, fdum
        szda(i) = fdum 
    endfor
    readf, pipe, idum
    ldef1 = idum
    if (ldef1 eq 1) then begin
        readf, pipe, fdum
	xmin = fdum
        readf, pipe, fdum
	xmax = fdum
        readf, pipe, fdum
	ymin = fdum
        readf, pipe, fdum
	ymax = fdum
    endif 
    readf, pipe, idum
    lfsel = idum
    if (lfsel eq 1) then begin
        readf, pipe, idum
        nmx = idum
 		;**** declare arrays ****
        szdm = fltarr(nmx)
        tfitm = fltarr(nmx)
        for i = 0, nmx-1 do begin
	    readf, pipe, fdum
            szdm(i) = fdum 
        endfor
        for i = 0, nmx-1 do begin
	    readf, pipe, fdum 
            tfitm(i) = fdum
        endfor
    endif
    for i = 0, 5 do begin
        readf, pipe, sdum 
        strg(i) = sdum
    endfor
    readf, pipe, sdum, format = '(a32)'
    head1 = sdum
    readf, pipe, sdum, format = '(a16)'
    head2 = sdum
    readf, pipe, sdum, format = '(a16)'
    head3 = sdum
    readf, pipe, sdum, format = '(a24)'
    head4 = sdum

		;***********************
		;**** Plot the data ****
		;***********************

    adas502v208_plot, dsfull, 						$
  		  title , titlx, titlm , utitle,date, esym, iz0, iz1, 	$
  		  bwno  , ciion, cicode, cfion, cfcode, itval,        	$
		  teva  , szda , szdm  , tfitm,                       	$
                  ldef1 , xmin , xmax  , ymin , ymax,                 	$
   		  lfsel , nmx  ,                                      	$
   		  strg  , head1, head2, head3, head4,                 	$
		  hrdout, hardname, device, header, bitfile, gomenu,  	$
                  record, FONT=font
    itval_sel = itval
    szda_sel = szda
    teva_sel = teva

END
