; Copyright (c) 1995 Strathclyde University .
; SCCS info: Module @(#)$Header: /home/adascvs/idl/adas2xx/adas208/adas208.pro,v 1.21 2018/10/03 09:25:18 mog Exp $ Date $Date: 2018/10/03 09:25:18 $
;+
; PROJECT:
;       ADAS IBM MVS to DEC UNIX conversion
;
; NAME:
;	ADAS208
;
; PURPOSE:
;	The highest level routine for the ADAS 208 program.
;
; EXPLANATION:
;	This routine is called from the main adas system routine, adas.pro,
;	to start the ADAS 208 application.  Associated with adas208.pro
;	is a FORTRAN executable.  The IDL code provides the user
;	interface and output graphics whilst the FORTRAN code reads
;	in data files, performs numerical processing and creates the
;	output files.  The IDL code communicates with the FORTRAN
;	executable via a bi-directional UNIX pipe.  The unit number
;	used by the IDL for writing to and reading from this pipe is
;	allocated when the FORTRAN executable is 'spawned' (see code
;	below).  Pipe communications in the FORTRAN process are to
;	stdin and stdout, i.e streams 5 and 6.
;
;	The FORTRAN code is an independent process under the UNIX system.
;	The IDL process can only exert control over the FORTRAN in the
;	data which it communicates via the pipe.  The communications
;	between the IDL and FORTRAN must be exactly matched to avoid
;	input conversion errors.  The correct ammounts of data must be
;	passed so that neither process 'hangs' waiting for communications
;	which will never occur.
;
;	The FORTRAN code performs some error checking which is
;	independent of IDL.  In cases of error the FORTRAN may write
;	error messages.  To prevent these error messages from conflicting
;	with the pipe communications all FORTRAN errors are written to
;	output stream 0, which is stderr for UNIX.  These error messages
;	will appear in the window from which the ADAS session/IDL session
;	is being run.
;
;	In the case of severe errors the FORTRAN code may terminate
;	itself prematurely.  In order to detect this, and prevent the
;	IDL program from 'hanging' or crashing, the IDL checks to see
;	if the FORTRAN executable is still an active process before
;	each group of pipe communications.  The process identifier
;	for the FORTRAN process, PID, is recorded when the process is
;	first 'spawned'.  The system is then checked for the presence
;	of the FORTRAN PID.
;
; USE:
;	First the system settings must be established by calling
;	adas_sys_set.pro then adas208.pro is called to start the
;	ADAS 208 application;
;
;	adas_sys_set, adasrel, fortdir, userroot, centroot, $
;		      devlist, devcode, font_large, font_small, edit_fonts
;	adas208,   adasrel, fortdir, userroot, centroot, devlist, $
;		   devcode, font_large, font_small, edit_fonts
;
; INPUTS:
;	ADASREL - A string indicating the ADAS system version, 
;		  e.g ' ADAS RELEASE: ADAS93 V1.1.0'.  The first
;		  character should be a space.
;
;	FORTDIR - A string holding the path to the directory where the
;		  FORTRAN executables are, e.g '/disk/adas/fortran/exec'
;
;	USERROOT - A string holding the path to the root directory of
;		   the user's adas data. e.g '/disk/bowen/adas'
;		   This root directory will be used by adas to construct
;		   other path names.  For example the users default data
;		   for adas208 should be in /disk/bowen/adas/adf04.  In
;		   particular the user's default interface settings will
;		   be stored in the directory USERROOT+'/defaults'.  An
;		   error will occur if the defaults directory does not
;		   exist.
;
;	CENTROOT - Like USERROOT, but this directory points to the
;		   central data area for the system.  User defaults are
;		   not stored on CENTROOT.
;
;	DEVLIST - A string array of hardcopy device names, used for
;		  graphical output. e.g ['Post-Script','HP-PCL','HP-GL']
;		  This array must mirror DEVCODE.  DEVCODE holds the
;		  actual device names used in a SET_PLOT statement.
;
;	DEVCODE - A string array of hardcopy device code names used in
;		  the SET_PLOT statement, e.g ['PS', 'PCL', 'HP']
;		  This array must mirror DEVLIST.
;
;	FONT_LARGE - The name of a larger font e.g 'courier_bold14'
;
;	FONT_SMALL - The name of a smaller font e.g 'courier_bold12'
;
;	EDIT_FONTS - A structure of two fonts used in the table editor
;		     adas_edtab.pro, {font_norm:'',font_input:''}.
;		     The two fonts are used to differentiate between
;		     editable and non-editable parts of the table. You
;		     may have to experiment to get two fonts which give
;		     the same default spacing in the edit widget.
;		     e.g {font_norm:'helvetica_bold14', $
;			  font_input:'helvetica_oblique14'}
;
; OPTIONAL INPUTS:
;	None
;
; OUTPUTS:
;	None - Note this routine should leave all inputs un-modified.
;
; OPTIONAL OUTPUTS:
;	None
;
; KEYWORD PARAMETERS:
;	None
;
; CALLS:
;	FIND_PROCESS	Checks to see if a given process is active.
;	B8SPF0		Pipe comms with FORTRAN B8SPF0 routine.
;	BXSETP		Pipe comms with FORTRAN BXSETP routine.
;	B8ISPF		Pipe comms with FORTRAN B8ISPF routine.
;	BXDATE		Get date and time from operating system.
;	XXGUID		Get user name from UNIX 'USER' varaible via IDL
;	B8SPF1		Pipe comms with FORTRAN B8SPF1 routine.
;	BXOUTG		Pipe comms with FORTRAN BXOUTG routine.
;
; SIDE EFFECTS:
;	This routine spawns a FORTRAN executable.  Note the pipe 
;	communications routines listed above.  In addition to these
;	pipe communications there is one explicit communication of the
;	date to the FORTRAN code, search for 'printf,pipe' to find it.
;
; CATEGORY:
;	Adas system.
;	
; WRITTEN:
;       David H.Brooks, Universiy of Strathclyde, 4-Dec-1995
;
; MODIFIED:
;       1.0     David Brooks
;		Written using adas205.pro v1.25 as a template
;	1.1    	William Osborn (Tessella Support Services plc)
;		Changed documentation + put under SCCS control
;	1.2    	William Osborn
;		Updated version number to 1.1
;	1.3	Tim Hammond
;		Updated version number to 1.2
;	1.4	William Osborn
;		Added bitfile and gomenu to bxoutg params
;	1.5	William Osborn
;		Added menu button code after bxoutg
;	1.6	William Osborn
;		Updated version number to 1.3
;	1.7	William Osborn
;		Replaced xxdate with bxdate
;       1.8     William Osborn
;               Increased version number to 1.4
;       1.9     William Osborn
;               Increased version number to 1.5
;       1.10    Richard Martin
;               Increased version number to 1.7
;		Number not incremented for r1.6 by mistake.
;       1.11    Richard Martin
;               Updated adasprog.
;	1.12    Richard Martin
;               Increased version number to 1.8
;	1.13    Hugh Summers
;		Added acquisition of truth table for ionisation
;               rate data availability from adf04 file.
;		Increased version number to 1.9
;	1.14	Richard Martin
;		Increase version no to 1.10
;	1.15	Richard Martin
;		Increase version no to 1.11
;	1.16	Richard Martin
;		Increase version no to 1.12
;	1.17	Richard Martin
;		Increase version no to 1.13
;	1.18	Richard Martin
;		Increase version no to 1.14
;	1.19	Richard Martin
;		Increase version no to 1.15
;	1.20	Martin O'Mullane
;		Increase version no to 1.16
;                 - Agruments to b8setp changed - wmin, wmax, amin now arrays.
;	1.21	Martin O'Mullane
;                 - Arguments to b8setp and b8ispf have changed - add ndsel.
;                   These are used to specify transitions in PEC file via
;                   run_adas208.pro but this functionality is not exposed
;                   in the interactive version.
;
; VERSION:
;	1.0	04-12-95
;	1.1	10-05-96
;	1.2	14-05-96
;	1.3	14-05-96
;	1.4	05-06-96
;	1.5	05-06-96
;	1.6	11-07-96
;	1.7	15-07-96
;	1.8	14-10-96
;	1.9	25-11-96
;	1.10	02-03-98
;	1.11	09-03-98
;	1.12	04-12-98
;	1.13	14/09/99
;	1.14	21-03-00
;	1.15	17-11-00
;	1.16	14-03-01
;	1.17	18-03-02
;	1.18	16-07-02
;	1.19	18-03-03
;	1.20	18-03-13
;	1.21	27-09-18
;
;-
;-----------------------------------------------------------------------------


PRO ADAS208,	adasrel, fortdir, userroot, centroot, 			$
		devlist, devcode, font_large, font_small, edit_fonts

		;************************
		;**** Initialisation ****
		;************************

  adasprog = ' PROGRAM: ADAS208 V1.16'
  lpend = 0
  gomenu = 0
  deffile = userroot+'/defaults/adas208_defaults.dat'
  bitfile = centroot+'/bitmaps'
  device = ''

		;******************************************
		;**** Search for user default settings ****
		;**** If not found create defaults     ****
		;******************************************

  files = findfile(deffile)
  if files(0) eq deffile then begin

    restore,deffile
    inval.centroot1 = centroot+'/adf04/'
    inval.userroot1 = userroot+'/adf04/'
    inval.centroot2 = centroot+'/adf18/'
    inval.userroot2 = userroot+'/adf18/'
    inval.adasrel   = adasrel
    inval.fortdir   = fortdir
    inval.userroot  = userroot
    inval.centroot  = centroot
    inval.devlist   = devlist
    inval.devcode   = devcode
    inval.fontl     = font_large
    inval.fonts     = font_small
    tfont_struct    = { ef,                                             $
                        font_norm:edit_fonts.font_norm,                 $
                        font_input:edit_fonts.font_input}
    inval.efont     = tfont_struct

  end else begin

    tfont_struct    = { ef,                                             $
                        font_norm:edit_fonts.font_norm,                 $
                        font_input:edit_fonts.font_input}
    inval = {                                                           $
		ROOTPATH1:userroot+'/adf04/',                           $
		FILE1:'',                                               $
		ROOTPATH2:userroot+'/adf18/',                           $
		FILE2:'',                                               $
		CENTROOT1:centroot+'/adf04/',                           $
		USERROOT1:userroot+'/adf04/',                           $
                CENTROOT2:centroot+'/adf18/',                           $
                USERROOT2:userroot+'/adf18/',                           $
                ADASREL:adasrel,                                        $
                FORTDIR:fortdir,                                        $
                USERROOT:userroot,                                      $
                CENTROOT:centroot,                                      $
                DEVLIST:devlist,                                        $
                DEVCODE:devcode,                                        $
                FONTL:font_large,                                       $
                FONTS:font_small,                                       $
                EFONT:tfont_struct,                                     $
                EXPSEL:0	                                        $
             }

    procval = {NMET:-1}

    passdir = userroot+'/pass/'
    outval = { $
		GRPOUT:0, GTIT1:'', GRPSCAL:0, $
		XMIN:'',  XMAX:'',   $
		YMIN:'',  YMAX:'',   $
		HRDOUT:0, HARDNAME:'', $
		GRPDEF:'', GRPFMESS:'', $
		GRPSEL:-1, GRPRMESS:'', $
		DEVSEL:-1, GRSELMESS:'', $
		TEXOUT:0, TEXAPP:-1, $
		TEXREP:0, TEXDSN:'paper.txt', $
		TEXDEF:'',TEXMES:'', $
		CONOUT:0, METOUT:0, $
                GCROUT:0, MESS:'',  $
		PASSDIR: passdir}
  end


		;****************************
		;**** Start fortran code ****
		;****************************

  spawn, fortdir + '/adas208.out', unit=pipe,/noshell,PID=pid

		;************************************************
		;**** Get date and write to fortran via pipe ****
		;************************************************
  date = bxdate()
  printf,pipe,date(0)

		;**** Pipe out user-id for Fortran program xxguid to use
  userid = '          '
  uid = getenv('USER')
  strput,userid,strmid(uid,0,10)
  printf,pipe,userid

LABEL100:
		;**** Check FORTRAN still running ****
  
  if find_process(pid) eq 0 then goto, LABELEND

		;************************************************
		;**** Communicate with b8spf0 in fortran and ****
		;**** invoke user interface widget for       ****
		;**** Data file selection                    ****
		;************************************************

  b8spf0, pipe, inval, dsninc, dsninx, rep, FONT=font_large

		;**** If cancel selected then end program ****

  if rep eq 'YES' then goto, LABELEND

		;**** Check FORTRAN still running ****

  if find_process(pid) eq 0 then goto, LABELEND

		;*************************************
		;**** Read pipe comms from b8setp ****
		;*************************************
  b8setp, pipe, sz0, sz, scnte, sil, lfpool, strga, ndmet, ndwvl, ndsel, $
          npl, strgmi, strgmf, ndlev, iss04a

LABEL200:
		;**** Check FORTRAN still running ****

  if find_process(pid) eq 0 then goto, LABELEND

		;************************************************
		;**** Communicate with b8ispf in fortran and ****
		;**** invoke user interface widget for       ****
		;**** Processing options                     ****
		;************************************************
  b8ispf, pipe, lpend, procval, sz, sz0, strga, dsninc,		 	$
	  gomenu, bitfile, inval, npl, strgmi, strgmf, ndmet, ndwvl,	$
	  ndsel, ndlev,  iss04a,					$
          FONT_LARGE=font_large, 					$
          FONT_SMALL=font_small, EDIT_FONTS=edit_fonts
  
                ;**** If menu button clicked, tell fortran to stop ****

  if gomenu eq 1 then begin
     printf, pipe, 1
     goto, LABELEND
  endif else begin
     printf, pipe, 0
  endelse

		;**** If cancel selected then goto 100 ****
  if lpend eq 1 then goto, LABEL100

		;**** Fortran processing now in progress. Get ****
		;**** date and time for this run.	      ****
  date = bxdate()
		;**** Create header for output. ****
  header = adasrel+adasprog+' DATE: '+date(0)+' TIME: '+date(1)

LABEL300:
		;**** Check FORTRAN still running ****
  if find_process(pid) eq 0 then goto, LABELEND
		;************************************************
		;**** Communicate with b8spf1 in fortran and ****
		;**** invoke user interface widget for       ****
		;**** Output options                         ****
		;************************************************

  b8spf1, pipe, lpend, outval, dsninc, header, bitfile, gomenu, $
          DEVLIST=devlist, FONT=font_large

                ;**** extra check to see whether FORTRAN is still there ****
  if find_process(pid) eq 0 then goto, LABELEND

                ;**** if menu button clicked, tell FORTRAN to stop ****
  if gomenu eq 1 then begin
      printf, pipe, 1
      outval.texmes = ''
      outval.mess = ''
      goto, LABELEND
  endif else begin
      printf, pipe, 0
  endelse


		;**** If cancel selected then erase output ****
		;**** messages and goto 200.		   ****
  if lpend eq 1 then begin
    outval.texmes = ''
    outval.mess = ''
    goto, LABEL200
  end

		;**** If graphical output requested ****

		;************************************************
		;**** Communicate with bxoutg in fortran and ****
		;**** invoke widget Graphical output         ****
		;************************************************
  if outval.grpout eq 1 then begin
		;**** User explicit scaling ****
    grpscal = outval.grpscal
    xmin = outval.xmin
    xmax = outval.xmax
    ymin = outval.ymin
    ymax = outval.ymax
	;**** Hardcopy output ****
    hrdout = outval.hrdout
    hardname = outval.hardname
    if outval.devsel ge 0 then device = devcode(outval.devsel)

    bxoutg, pipe, grpscal, xmin, xmax, ymin, ymax, $
		  hrdout, hardname, device, header, bitfile, gomenu,	$
		  FONT=font_large, WIN_NUMB = '208'

                ;**** if menu button clicked, tell FORTRAN to stop ****
  if gomenu eq 1 then begin
      printf, pipe, 1
      outval.texmes = ''
      outval.mess = ''
      goto, LABELEND
  endif else begin
      printf, pipe, 0
  endelse

  end

		;**** Back for more output options ****
GOTO, LABEL300

LABELEND:

		;**** Ensure appending is not enabled for ****
		;**** text output at start of next run.   ****
outval.texapp = -1

		;**** Save user defaults ****
save,inval,procval,outval,filename=deffile
 ;print, "deffile = "+deffile


;print,'ADAS 208 Ended'

END
