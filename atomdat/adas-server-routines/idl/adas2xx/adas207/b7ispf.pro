; Copyright (c) 1995 Strathclyde University .
; SCCS info: Module @(#)$Header: /home/adascvs/idl/adas2xx/adas207/b7ispf.pro,v 1.6 2004/07/06 11:27:24 whitefor Exp $ Date $Date: 2004/07/06 11:27:24 $
;+
; PROJECT:
;       ADAS IBM MVS to UNIX conversion
;
; NAME:
;	B7ISPF
;
; PURPOSE:
;	IDL user interface and communications with ADAS207 FORTRAN
;	process via pipe.
;
; EXPLANATION:
;	The routine begins by reading information from the ADAS207
;	FORTRAN process via a UNIX pipe.  Then part of the ADAS207
;	IDL user interface is invoked to determine how the user
;	wishes to process the input dataset.  When the user's
;	interactions are complete the information gathered with
;	the user interface is written back to the FORTRAN process
;	via the pipe.  Communications are to the FORTRAN subroutine
;	B5ISPF.
;
; USE:
;	The use of this routine is specific to ADAS207, see adas207.pro.
;
; INPUTS:
;	PIPE	- The IDL unit number of the bi-directional pipe to the
;		  ADAS207 FORTRAN process.
;
;	LPEND	- Integer, 0 if the user pressed the 'Done' button
;		  or 1 if the user pressed 'Cancel'.  Maps directly onto
;		  the logical variable LPEND in ADAS207 FORTRAN.
;
;	VALUE	- A structure which determines the initial settings of
;		  the processing options widget.  The initial value is
;		  set in adas207.pro.  If adas207.pro passes a blank 
;		  dummy structure of the form {LEQUIL:-1} into VALUE then
;		  VALUE is reset to a default structure.
;
;		  The VALUE structure is;
;		     {  LEQUIL:1,					$
;			XXMULT:dblarr(), 				$
;			NSTRN1:1,					$
;			ISTRN1:lonarr(), 				$
;			NSTRN1:0,					$
;			ISTRN1:lonarr(),				$
;			LINDEX:lonarr(20),			$
;			LRATHA:0,					$
;			LRATIA:0,					$
;			RATHA :strarr(maxd),			$
;			RATIA :strarr(maxd),			$
;			WVUPPER :	0.0,				$
;			WVLOWER :	0.0,				$
;			EMISSVAL :	0.0			}
;
;		  See cw_adas207_proc.pro for a full description of this
;		  structure and related variables.
;
;	TITLED	- String, Ion name.
;
;	SZ	- String, Recombined ion charge.
;
;	SZ0	- String, Nuclear charge.
;
;	SIONPT	- String, Ionisation potential.
;
;	DSNPAS	- String, The full system file name of the input
;			Contour Passing file.
;	DSNINC	- String, The full system file name of the input COPASE
;			dataset selected by the user for processing.
;
;	NDMET	- Integer, Maximum number of metastables allowed.
;
;	ILEV	- String array, Metastable level indices.
;
;	MSTRGA	- String array, Metastable level designations.
;
;       BITFILE - String; the path to the dirctory containing bitmaps
;                 for the 'escape to series menu' button.
;
; OPTIONAL INPUTS:
;	None
;
; OUTPUTS:
;	VALUE	- On output the structure records the final settings of
;		  the processing selections widget if the user pressed the
;		  'Done' button, otherwise it is not changed from input.
;
;       GOMENU  - Int; flag - set to 1 if user has selected 'escape direct
;                 to series menu , otherwise 0
;
; OPTIONAL OUTPUTS:
;	None
;
; KEYWORD PARAMETERS:
;	FONT_LARGE - The name of a larger font.
;
;	FONT_SMALL - The name of a smaller font.
;
;	EDIT_FONTS - A structure of two fonts used in the table editor
;		     adas_edtab.pro, {font_norm:'',font_input:''}.
;
; CALLS:
;	ADAS207_PROC	Invoke the IDL interface for ADAS207 data
;		 	processing options/input.
;
; SIDE EFFECTS:
;	Two way communications with ADAS207 FORTRAN via the
;	bi-directional UNIX pipe.
;
; CATEGORY:
;	Adas system.
;
; WRITTEN:
;       S.P.Bellamy, Tessella Support Services plc, 20/03/1995
;
; MODIFIED:
;	1.1	Lalit Jalota
;		Put under SCCS control
;	1.2	Tim Hammond
;		Added ratha, ratia, lratha and lratia to VALUE structure.
;		Tidied up comments and code.
;	1.3	Tim Hammond
;		Modified so that data is only passed to FORTRAN if the
;		user has selected 'Done' and not when 'Cancel'.
;	1.4	William Osborn
;		Added menu button code
;	1.5	Richard Martin
;		Added obtaining of watran, stckm, stack and avalue from fortran.
;		Added watran, stckm, stack and avalue to the adas207_proc call.
;		Added linex, wvupper, wvlower and emissval to the VALUE structure.
;		Passed stored lindex values to fortran instead of istrn1 or istrn2 values.
;	1.6	Martin O'Mullane & Allan Whiteford
;		 - Fix problem with rathha and ratia dimensions
;		
;
; VERSION:
;	1.1	30-03-95
;	1.2	13-02-96
;	1.3	03-04-96
;	1.4	04-06-96
;	1.5	14-11-01
;	1.6	19-11-02
;
;-
;-----------------------------------------------------------------------------

PRO b7ispf, pipe, lpend, value, titled, sz, sz0, sionpt, dsnpas, dsninc,$
	    ndmet, ilev, mstrga, bitfile, gomenu, FONT_LARGE=font_large, $
            FONT_SMALL=font_small, EDIT_FONTS=edit_fonts

                ;**** Set defaults for keywords ****

    IF NOT (KEYWORD_SET(font_large)) THEN font_large = ''
    IF NOT (KEYWORD_SET(font_small)) THEN font_small = ''
    IF NOT (KEYWORD_SET(edit_fonts)) THEN 				$
    edit_fonts = {font_norm:'',font_input:''}

		;**** Declare variables for input ****

    lpend = 0
    ndlne = 0
    il = 0
    nmet = 0
    maxd = 0
    icnte = 0
    icnth = 0
    icntr = 0
    idum = 0
    ldum = 0L
    sdum = ' '
    fdum = 0.0
  
		;********************************
		;**** Read data from fortran ****
		;********************************

    readf, pipe, idum
    lpend = idum
    readf, pipe, idum
    ndlne = idum
    readf, pipe, idum
    il = idum
    readf, pipe, idum
    ndlev = idum
    readf, pipe, idum
    nmet = idum
    readf, pipe, idum
    maxd = idum
    densa = fltarr(maxd)
    for i=0, (maxd-1) do begin
	readf, pipe, fdum
	densa(i) = fdum
    endfor
    readf, pipe, idum
    icnte = idum
    readf, pipe, idum
    icnth = idum
    readf, pipe, idum
    icntr = idum

		;***************************************************
		;**** Set up ratio flags depending on values of ****
		;**** icntr and icnth                           ****
		;***************************************************

    if icnth gt 0 then begin
	lratha = 1
    endif else begin
	lratha = 0
    endelse
    if icntr gt 0 then begin
	lratia = 1
    endif else begin
	lratia = 0
    endelse

		;*************************************
		;**** Set up and read into arrays ****
		;*************************************

    ie1a = lonarr(icnte)
    ie2a = lonarr(icnte)
    mtrga = strarr(il)
    strga = strarr(il)
    watran = fltarr(icnte)
    aval = fltarr(icnte)

    for i=0, (icnte-1) do begin
	readf, pipe, ldum
        ie1a(i) = ldum
    endfor
    for i=0, (icnte-1) do begin
	readf, pipe, ldum
        ie2a(i) = ldum
    endfor
    for i=0, (il-1) do begin
        readf, pipe, sdum
        mtrga(i) = sdum
    endfor
    for i=0, (il-1) do begin
        readf, pipe, sdum
        strga(i) = sdum
    endfor
    for i=0, (icnte-1) do begin
	readf, pipe, fdum
        watran(i) = fdum
    endfor    

    stckm=dblarr(nmet)
    stack=dblarr(ndlev,nmet)
    for i=0, (nmet-1) do begin     
	 readf, pipe, fdum
	 stckm(i)=fdum
	 for j=0, (ndlev-1) do begin     
	 	readf, pipe, fdum
		stack(j,i)=fdum
       endfor
    endfor    

    for i=0, (icnte-1) do begin
	readf, pipe, fdum
        aval(i) = fdum
    endfor    

		;*******************************************
		;**** Set default value if non provided ****
		;*******************************************

    if value.lequil lt 0 then begin
        mult = make_array( nmet, /DOUBLE, VALUE=1.0D )
        value = {	LEQUIL	:	1,				$
			XXMULT	:	mult, 			$
			NSTRN1	:	1,				$
			ISTRN1	:	[1], 				$
			NSTRN2	:	0,				$
			ISTRN2	:	[0],				$
			LINDEX	:     [20],				$
			LRATHA	:	0,				$
			LRATIA	:	0,				$
			RATHA		:	fltarr(maxd),		$
			RATIA		:	fltarr(maxd),		$
			WVUPPER	:	0,				$
			WVLOWER	:	0,				$
			EMISSVAL	:	0.0			}
    endif
    value.lratha = lratha
    value.lratia = lratia

    new_val = {	        LEQUIL	:   value.LEQUIL,    $
			XXMULT	:   value.XXMULT,    $
			NSTRN1	:   value.NSTRN1,    $
			ISTRN1	:   value.ISTRN1,    $
			NSTRN2	:   value.NSTRN2,    $
			ISTRN2	:   value.ISTRN2,    $
			LINDEX	:   value.LINDEX,    $
			LRATHA	:   value.LRATHA,    $
			LRATIA	:   value.LRATIA,    $
			RATHA	:   fltarr(maxd),    $
			RATIA	:   fltarr(maxd),    $
			WVUPPER	:   value.WVUPPER,   $
			WVLOWER	:   value.WVLOWER,   $
			EMISSVAL:   value.EMISSVAL   }

    imax = min([n_elements(value.ratha), maxd])
    new_val.ratha[0:imax-1] = value.ratha[0:imax-1]
    new_val.ratia[0:imax-1] = value.ratia[0:imax-1]
    
    value = new_val
    

		;****************************************
		;**** Pop-up processing input widget ****
		;****************************************

    adas207_proc, value, titled, sz, sz0, sionpt, dsnpas, dsninc, 	$
		  nmet, maxd, icnte, il, ndmet, ilev, mstrga, 		$
		  ie1a, ie2a, mtrga, strga, watran, stckm, stack, aval, action, densa, bitfile,	$
		  FONT_LARGE=font_large, FONT_SMALL=font_small, 	$
		  EDIT_FONTS=edit_fonts

		;**********************************************
		;**** Act on the event from the widget     ****
		;**********************************************
		;**** There are only three possible events ****
		;**** 'Done', 'Cancel' and 'Menu'.         ****
		;**********************************************

    if action eq 'Done' then begin
        lpend = 0
    endif else if action eq 'Menu' then begin
        lpend = 1
	gomenu = 1
    endif else begin
        lpend = 1
    endelse

		;*******************************
		;**** Write data to fortran ****
		;*******************************

    printf, pipe, lpend
    if lpend eq 0 then begin
        printf, pipe, value.lequil
        printf, pipe, value.xxmult
        if (value.lratia eq 1) or (value.lratha eq 1) then begin
  	    for i=0,(maxd-1) do begin
	        printf, pipe, value.ratha(i)
	    endfor
	    for i=0,(maxd-1) do begin
	        printf, pipe, value.ratia(i)
	    endfor
        endif else begin
            rats = dblarr(maxd)
            for i=0,(maxd-1) do begin
                printf, pipe, rats(i)
	    endfor
            for i=0,(maxd-1) do begin
                printf, pipe, rats(i)
	    endfor
        endelse
        printf, pipe, value.nstrn1
        if value.nstrn1 gt 0 then printf, pipe, value.lindex(value.istrn1-1)+1
        printf, pipe, value.nstrn2
        if value.nstrn2 gt 0 then printf, pipe, value.lindex(value.istrn2-1)+1
	  
    endif

END
