; Copyright (c) 1995 Strathclyde University .
; SCCS info: Module @(#)$Header: /home/adascvs/idl/adas2xx/adas207/adas207_proc.pro,v 1.5 2004/07/06 10:18:57 whitefor Exp $ Date $Date: 2004/07/06 10:18:57 $
;+
; PROJECT:
;       ADAS IBM MVS to UNIX conversion
;
; NAME:
;	ADAS207_PROC
;
; PURPOSE:
;	IDL ADAS user interface, processing options/input.
;
; EXPLANATION:
;	This routine creates and manages a pop-up window which allows
;	the user to select options and input data to control ADAS207
;	processing.
;
; USE:
;	This routine is ADAS207 specific, see b7ispf.pro for how it
;	is used.
;
; INPUTS:
;	VAL	- A structure which determines the initial settings of
;		  the processing options widget.  The value is passed
;		  unmodified into cw_adas207_proc.pro.
;
;		  See cw_adas207_proc.pro for a full description of this
;		  structure.
;
;	TITLED	- String, Ion name.
;
;	SZ	- String, Recombined ion charge.
;
;	SZ0	- String, Nuclear charge.
;
;	SIONPT	- String, Ionisation potential.
;
;	DSNPAS	- String, The full system file name of the input
;			Contour Passing file.
;	DSNINC	- String, The full system file name of the input COPASE
;			dataset selected by the user for processing.
;
;	NMET	- Integer, Number of metastables.
;
;	MAXD	- Integer, Unused - would be size of RATHA and RATIA arrays.
;
;	ICNTE	- Integer, Number of Electron Impact Transitions.
;
;	IL	- Integer, Number of energy levels.
;
;	NDMET	- Integer, Maximum number of metastables allowed.
;
;	ILEV	- String array, Metastable level indices.
;
;	MSTRGA	- String array, Metastable level designations.
;
;	IE1A	- Integer array, Lower level index of transition
;
;	IE2A	- Integer array, Upper level index of transition
;
;	MTARGA	- String array, Metastable marker
;
;	STRGA	- String array, Level designations
;
;	WATRAN- Float array. Transition energies (cm-1)
;
;	STCKM - Metastables population stack at median temperature and density.
;	
;	STACK - Population dependence at median temperature and density.
;
;	AVAL  - Electron impact transition: A-value (sec-1)
;
;	DENSA	- Float array, input densities (cm-3)
;
;       BITFILE - String; the path to the dirctory containing bitmaps
;                 for the 'escape to series menu' button.
;
; OPTIONAL INPUTS:
;	None
;
; OUTPUTS:
;	VAL	- On output the structure records the final settings of
;		  the processing selections widget if the user pressed the
;		  'Done' button, otherwise it is not changed from input.
;
;	ACT	- String; Either 'Done', 'Cancel' or 'Menu' for the button the
;		  user pressed to terminate the processing options
;		  window.
;
; OPTIONAL OUTPUTS:
;	None
;
; KEYWORD PARAMETERS:
;	FONT_LARGE - The name of a larger font.
;
;	FONT_SMALL - The name of a smaller font.
;
;	EDIT_FONTS - A structure of two fonts used in the table editor
;		     adas_edtab.pro, {font_norm:'',font_input:''}.
;
; CALLS:
;	CW_ADAS207_PROC	Declares the processing options widget.
;	ADAS207_PROC_EV Called indirectly during widget management,
;			routine included in this file.
;	XMAMAGER
;
; SIDE EFFECTS:
;	This routine uses a common block PROC207_BLK in the management
;	of the pop-up window.
;
; CATEGORY:
;	Widgets
;
; WRITTEN:
;       S.P.Bellamy, Tessella Support Services plc, 20/03/1995
;
; MODIFIED:
;	1.1	Lalit Jalota, Tessella Support Services plc
;		First version.
;	1.2	Tim Hammond, Tessella Support Services plc
;		Reduced X-Offset of processing widget to make the 
;		widget flush with the left-hand edge of the screen.
;	1.3	Tim Hammond
;		Added argument densa (the input densities) which is
;		passed straight through to cw_adas207_proc.pro
;	1.4	William Osborn
;		Added menu button code and dynlabel procedure
;	1.5	Richard Martin
;		Added watran, stckm, stack, aval to the subroutine call, and
;		in the call to cw_adas207_proc.pro
;
; VERSION:
;	1.1	30-03-95
;	1.2	12-02-96
;	1.3	12-02-96
;	1.4	04-06-96
;	1.5	14-11-01
;
;-----------------------------------------------------------------------------


PRO adas207_proc_ev, event

  COMMON proc207_blk,action,value

		;**** Find the event type and copy to common ****
    action = event.action

    CASE action OF

		;**** 'Done' button ****
	'Done'  : begin

			;**** Get the output widget value ****
		widget_control,event.id,get_value=value 

		widget_control,event.top,/destroy

	   end


		;**** 'Cancel' button ****
	'Cancel': widget_control,event.top,/destroy

		;**** 'Menu' button ****
	'Menu': widget_control,event.top,/destroy

    END

END

;-----------------------------------------------------------------------------

PRO adas207_proc, val, titled, sz, sz0, sionpt, dsnpas, dsninc, 	$
		  nmet, maxd, icnte, il, ndmet, ilev, mstrga, 		$
		  ie1a, ie2a, mtrga, strga, watran, stckm, stack,     $
		  aval, act, densa, bitfile,					$
		  FONT_LARGE=font_large, FONT_SMALL=font_small, 	$
		  EDIT_FONTS=edit_fonts

  COMMON proc207_blk,action,value

		;**** Copy value to common ****
  value = val

		;**** Set defaults for keywords ****

  IF NOT (KEYWORD_SET(font_large)) THEN font_large = ''
  IF NOT (KEYWORD_SET(font_small)) THEN font_small = ''
  IF NOT (KEYWORD_SET(edit_fonts)) THEN $
				edit_fonts = { font_norm:'', font_input:''}

                ;**** create base widget ****

  procid = widget_base(TITLE='ADAS207 PROCESSING OPTIONS', $
					XOFFSET=1,YOFFSET=0)

		;**** Declare processing widget ****

  cwid = cw_adas207_proc(procid, titled, sz, sz0, sionpt, dsnpas, 	$
                         dsninc, nmet, maxd, icnte, il, ndmet, ilev, 	$
                         mstrga, ie1a, ie2a, mtrga, strga, watran,      $
				 stckm, stack, aval, densa, bitfile,		$
			 	 VALUE=value, FONT_LARGE=font_large, 		$
                         FONT_SMALL=font_small, EDIT_FONTS=edit_fonts)

		;**** Realize the new widget ****

  dynlabel, procid
  widget_control,procid,/realize

		;**** make widget modal ****

  xmanager,'adas207_proc',procid,event_handler='adas207_proc_ev', $
					/modal,/just_reg
 
		;**** Return the output value from common ****

  act = action
  val = value

END

