; Copyright (c) 1995 Strathclyde University.
; SCCS info: Module @(#)$Header: /home/adascvs/idl/adas2xx/adas207/plot207.pro,v 1.5 2004/07/06 14:30:41 whitefor Exp $ Date $Date: 2004/07/06 14:30:41 $
;+
; PROJECT:
;       ADAS IBM MVS to UNIX conversion
;
; NAME:
;	PLOT207
;
; PURPOSE:
;	Plot graphs for ADAS207.
;
; EXPLANATION:
;	This routine plots ADAS207 output for the selected
;	graph type.
;
; USE:
;	Use is specific to ADAS207.  See adas207_plot.pro for
;	example.
;
; INPUTS:
;	IOPT	- Integer; Graphical output option number.
;
;	DATE	- String; Generation date of output.
;
;	TITLED  - String; Element symbol.
;
;	IZ      - Integer; Recombined ion charge.
;
;	GTIT1   - String; Title for graph.
;
;	DSNINC  - String; Input COPASE data set name.
;
;	ISEL	- Integer; Number of items in output option array.
;
;	CONTR	- Double array; Contour values for option 1.
;
;	IGSEL	- Integer array; Density or Temperature for option 2 or 3.
;
;	LDEF1	- Integer; Default scaling flag.
;
;	LOGINT	- Integer; Logarithmic contour interpolation flag.
;
;	XMIN	- Double; Minimum value for X axis.
;
;	XMAX	- Double; Maximum value for X axis.
;
;	YMIN	- Double; Minimum value for Y axis.
;
;	YMAX	- Double; Maximum value for Y axis.
;
;	NMET    - Integer; Number of metastable levels.
;
;	MAXD    - Integer; Number of input electron densities.
;
;	MAXT    - Integer; Number of input electron temperatures.
;
;	NSTRN1	- Integer; Number of transitions in first assembly.
;
;	NSTRN2	- Integer; Number of transitions in second assembly.
;
;	DENSA	- Double array; Electron densities (units: cm-3).
;
;	TEA	- Double array; Electron temperatures (units: K).
;
;	CSTRN1	- String array; Designations for first assembly.
;
;	CSTRN2	- String array; Designations for second assembly.
;
;	STRGM	- String array; Level designations.
;
;	RAT	- Double array; Spectrum line ratio.
;
;	HEADER	- String; ADAS version number header to include in graph.
;
; OPTIONAL INPUTS:
;	None.
;
; OUTPUTS:
;	None.
;
; OPTIONAL OUTPUTS:
;	None.
;
; KEYWORD PARAMETERS:
;
; CALLS:
;	None.
;
; SIDE EFFECTS:
;	None.
;
; CATEGORY:
;	Adas system.	
;
; WRITTEN:
;       S.P.Bellamy, Tessella Support Services plc, 28/03/1995
;
; MODIFIED:
;	1.1	S.P.Bellamy
;		First release
;	1.2	Tim Hammond
;		Changed plot symbol from cross to point.
;	1.3	Tim Hammond
;		Tidied up graph labelling       
;	1.4	Tim Hammond
;		Added new COMMON block Global_lw_data which contains the
;               values of left, right, top, bottom, grtop, grright
;       1.5     Tim Hammond
;               Added check for new environment variable VERY_SMALL
;               If this is set to the string "YES" (preferably in the
;               .adas_begin file) then the font sized used on the graphs
;               is reduced and the positioning of labels adjusted
;               slightly to make things fit on a very small screen.
;
; VERSION: 
;	1.1	28-03-95
;	1.2	07-04-95
;	1.3	14-07-95
;	1.4	26-02-96
;	1.5	06-08-96
;
;-
;----------------------------------------------------------------------------

PRO plot207, iopt, date, iz, titled, gtit1, dsninc, $
		isel, contr, igsel, ldef1, logint, $
		xmin, xmax, ymin, ymax, $
		nmet, maxd, maxt, nstrn1, nstrn2, $
		xmmult, densa, tea, cstrn1, cstrn2, strgm, rat, $
		header

  COMMON Global_lw_data, left, right, top, bottom, grtop, grright

                ;****************************************************
		;**** Suitable character size for current device ****
		;**** Aim for 60 characters in y direction.      ****
                ;****************************************************

  charsize = (!d.y_vsize/!d.y_ch_size)/60.0
  small_check = GETENV('VERY_SMALL')
  if small_check eq 'YES' then charsize=charsize*0.8

		;**** Initialise titles ****
		;**** '!E' shifts for exponent, !N returns to normal ****

  if iopt eq 1 then begin
	xtitle = 'ELECTRON TEMPERATURE (K)'
	ytitle = 'ELECTRON DENSITY (cm!E-3!N)'
	strg1 = '------- CONTOUR VALUES -------'
	strg2 = 'INDEX   SPECTRUM LINE RATIO'
  endif else if iopt eq 2 then begin
	xtitle = 'ELECTRON TEMPERATURE (K)'
	ytitle = 'SPECTRUM LINE RATIO (PH)'
	strg1 = '----- INDEX ASSIGNMENTS -----'
	strg2 = 'INDEX    DENSITY (cm!E-3!N)'
  endif else if iopt eq 3 then begin
	xtitle = 'ELECTRON DENSITY (cm!E-3!N)'
	ytitle = 'SPECTRUM LINE RATIO (PH)'
	strg1 = '----- INDEX ASSIGNMENTS -----'
	strg2 = 'INDEX    TEMPERATURE (K)'
  endif

  strg3 = '------ FIRST  LINE GROUP: TRANSITIONS ------'
  strg4 = '------ SECOND LINE GROUP: TRANSITIONS -----'
  strg5 = 'INDEX  -- UPPER LEVEL --   -- LOWER LEVEL --'
  strg6 = '-------- METASTABLE MULTIPLIERS --------'
  strg7 = 'INDEX  --- DESIGNATION ----   SCALING FACTOR'


		;**** Construct graph title ****
		;**** '!C' is the new line control. ****
  if iopt eq 1 then begin
	gtitle = 'SPECTRUM-LINE INTENSITY RATIO ON TEMPERATURE/DENSITY PLANE' $
			+' - DIAGNOSTIC CONTOUR PLOT'
  endif else if iopt eq 2 then begin
	gtitle = 'SPECTRUM-LINE INTENSITY RATIO VERSUS TEMPERATURE (KELVIN)' $
			+' FOR A SERIES OF DENSITIES '
  endif else if iopt eq 3 then begin
	gtitle = 'SPECTRUM-LINE INTENSITY RATIO VERSUS DENSITY (cm!E-3!N)' $
			+' FOR A SERIES OF TEMPERATURES    '
  endif

  gtitle = ' '+gtitle+': ION - '+titled+string(iz,format='(I2)')+'!C!C'
  gtitle = gtitle+header+'!C'
  if small_check eq 'YES' then gtitle=gtitle+'!C'
  gtitle = gtitle+' GRAPH TITLE:'+gtit1+'!C'
  if small_check eq 'YES' then gtitle=gtitle+'!C'
  gtitle = gtitle+' COPASE FILE:'+dsninc

		;**** Construct run information list ****
  if iopt eq 1 then begin
	if logint then begin
		list = '** LOGARITHMIC INTERPOLATED CONTOURS  **!C'
	endif else begin
		list = '***** LINEAR INTERPOLATED CONTOURS *****!C'
	endelse
  endif else begin
	list = '!C'
  endelse

  if small_check eq 'YES' then begin
    list = list+'!C'+strg1+'!C!C'+strg2+'!C'
  endif else begin
    list = list+'!C'+strg1+'!C'+strg2+'!C'
  endelse
  for i = 1, 20 do begin
	if i le isel then begin
		if iopt eq 1 then begin
			strgt = string(i,contr(i-1),format='(I3,8X,E10.4)')
		endif else if iopt eq 2 then begin
			strgt = string(i,densa(igsel(i-1)-1),format='(I3,8X,E10.4)')
		endif else if iopt eq 3 then begin
			strgt = string(i,tea(igsel(i-1)-1),format='(I3,8X,E10.4)')
		endif
  		if small_check eq 'YES' then list=list+'!C'
	endif else begin
		strgt = ''
	endelse
	list = list+'!C'+strgt
  endfor

  if small_check eq 'YES' then begin
    list = list+'!C!C'+strg3+'!C!C'+strg5+'!C'
  endif else begin
    list = list+'!C!C'+strg3+'!C'+strg5+'!C'
  endelse
  for i = 1, 14 do begin
	if i le nstrn1 then begin
		strgt = string(i,cstrn1( i-1 ),format='(I3,4X,A)')
  		if small_check eq 'YES' then list=list+'!C'
	endif else begin
		strgt = ''
	endelse
	list = list+'!C'+strgt
  endfor

  if small_check eq 'YES' then begin
    list = list+'!C!C'+strg4+'!C!C'+strg5+'!C'
  endif else begin
    list = list+'!C!C'+strg4+'!C'+strg5+'!C'
  endelse
  for i = 1, 14 do begin
	if i le nstrn2 then begin
		strgt = string(i,cstrn2( i-1 ),format='(I3,4X,A)')
  		if small_check eq 'YES' then list=list+'!C'
	endif else begin
		strgt = ''
	endelse
	list = list+'!C'+strgt
  endfor

  if small_check eq 'YES' then begin
    list = list+'!C!C'+strg6+'!C!C'+strg7+'!C'
  endif else begin
    list = list+'!C!C'+strg6+'!C'+strg7+'!C'
  endelse
  for i = 1, 5 do begin
	if i le nmet then begin
		strgt = string(i,strgm( i-1 ),xmmult(i-1), $
				format='(I3,4X,A,6X,E10.4)')
  		if small_check eq 'YES' then list=list+'!C'
	endif else begin
		strgt = ''
	endelse
	list = list+'!C'+strgt
  endfor


		;**** Find x and y ranges for auto scaling,	   ****
		;**** check x and y in range for explicit scaling. ****
  makeplot = 1

  if iopt eq 1 then begin
	xmin = min( tea )
	xmax = max( tea )
	ymin = min( densa )
	ymax = max( densa )

	style = 2
	ticklen = 0.02

  endif else begin
	if iopt eq 2 then begin
		x = tea
		y = rat( *, igsel-1 )
		npts = maxt
			
	endif else if iopt eq 3 then begin
		x = densa
		y = transpose( rat( igsel-1, * ) )
		npts = maxd

	endif

	if ldef1 then begin
		;**** identify values in the valid range ****
		;**** plot routines only work within ****
		;**** single precision limits.	     ****
		xvals = where(x gt 1.6e-36 and x lt 1.0e37)
		yvals = where(y gt 1.6e-36 and y lt 1.0e37)

		if xvals(0) gt -1 then begin
			xmax = max(x(xvals))
			xmin = min(x(xvals))
		end else begin
			makeplot = 0
		end

		if yvals(0) gt -1 then begin
			ymax = max(y(yvals))
			ymin = min(y(yvals))
		end else begin
			makeplot = 0
		end

		style = 0

	endif else begin
		xvals = where(x ge xmin and x le xmax)
		yvals = where(y ge ymin and y le ymax)
		if xvals(0) eq -1 or yvals(0) eq -1 then begin
			makeplot = 0
		endif

		style = 1

	endelse

	ticklen = 1.0

  endelse


  if makeplot eq 1 then begin

		;**** Set up log-log plotting axes ****
	plot_oo, [xmin,xmax], [ymin,ymax], /nodata, $
		position=[left,bottom,grright,grtop], $
		xtitle=xtitle, ytitle=ytitle, $
		xstyle=style, ystyle=style, ticklen=ticklen, $
		charsize=charsize

	if iopt eq 1 then begin

		;*********************************
		;**** Diagnostic contour plot ****
		;*********************************

		;**** Plot Density/Temperature points ****
		for i = 0, maxd-1 do begin
			yden = make_array( maxt, /double, value=densa(i) )
			oplot, tea, yden, psym=3

		endfor

		;**** Plot Dashed Border ****
		oplot, [xmin,xmax,xmax,xmin,xmin],[ymin,ymin,ymax,ymax,ymin], linestyle=2

		;**** Plot the Contours ****
		if logint then begin
			contour, alog10(rat), tea, densa, levels=alog10(contr), $
				/overplot, /follow, $
				c_annotation=string(indgen(isel)+1,format='(I2)')

		endif else begin
			contour, rat, tea, densa, levels=contr, $
				/overplot, /follow, $
				c_annotation=string(indgen(isel)+1,format='(I2)')
		endelse

	endif else begin

		;*********************************
		;**** Make and annotate plots ****
		;*********************************
		for i = 0, isel-1 do begin

		;**** Make plot ****
			oplot,x,y(*,i)

		;**** Find suitable point for annotation ****
			if (x(npts-1) ge xmin) and (x(npts-1) le xmax) and $
			(y(npts-1,i) ge ymin) and (y(npts-1,i) le ymax) then begin

				iplot = npts-1

			end else begin
				iplot = -1
				for id = 0, npts-2 do begin
					if (x(npts-1) ge xmin) and (x(npts-1) le xmax) and $
					(y(npts-1,i) ge ymin) and (y(npts-1,i) le ymax) then begin
						iplot = id
					endif
				endfor
			endelse

		;**** Annotate plots with level numbers ****
			if iplot ne -1 then begin
				xyouts,x(iplot),y(iplot,i),			$
				string(i+1,format='(1x,i2)'),			$
				charsize=charsize*0.7
			endif

		endfor

	endelse

		;**** Output title above graphs ****
    xyouts,(left-0.05),top,gtitle,/normal,charsize=charsize*0.95

		;**** Output information list at the side of the graph ****
    if small_check eq 'YES' then begin
        xyouts,grright+0.03,top-0.05,list,/normal,charsize=charsize*0.6
    endif else begin
        xyouts,grright+0.03,top-0.05,list,/normal,charsize=charsize*0.7
    endelse

  end else begin

		;**** Print a message to the screen ****
    erase
    mess = 'Data out of range.'
    xyouts,left,grtop*0.7,ytitle+'   V   '+xtitle+'!C'+mess, $
		/normal,charsize=charsize*1.2

		;**** Output title above graphs ****
    xyouts,(left-0.05),top,gtitle,/normal,charsize=charsize*0.95

		;**** Output level list at the side of the graph ****
    if small_check eq 'YES' then begin
      xyouts,grright+0.03,top-0.05,list,/normal,charsize=charsize*0.6
    endif else begin
      xyouts,grright+0.03,top-0.05,list,/normal,charsize=charsize*0.7
    endelse


  end

END
