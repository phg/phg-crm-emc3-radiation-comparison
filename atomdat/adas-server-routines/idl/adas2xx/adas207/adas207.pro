; Copyright (c) 1995, Strathclyde University.
; SCCS info: Module @(#)$Header: /home/adascvs/idl/adas2xx/adas207/adas207.pro,v 1.16 2004/07/06 10:18:31 whitefor Exp $ Date $Date: 2004/07/06 10:18:31 $
;+
; PROJECT:
;       ADAS IBM MVS to UNIX conversion
;
; NAME:
;	ADAS207
;
; PURPOSE:
;	The highest level routine for the ADAS 207 program.
;
; EXPLANATION:
;	This routine is called from the main adas system routine, adas.pro,
;	to start the ADAS 207 application.  Associated with adas207.pro
;	is a FORTRAN executable.  The IDL code provides the user
;	interface and output graphics whilst the FORTRAN code reads
;	in data files, performs numerical processing and creates the
;	output files.  The IDL code communicates with the FORTRAN
;	executable via a bi-directional UNIX pipe.  The unit number
;	used by the IDL for writing to and reading from this pipe is
;	allocated when the FORTRAN executable is 'spawned' (see code
;	below).  Pipe communications in the FORTRAN process are to
;	stdin and stdout, i.e streams 5 and 6.
;
;	The FORTRAN code is an independent process under the UNIX system.
;	The IDL process can only exert control over the FORTRAN in the
;	data which it communicates via the pipe.  The communications
;	between the IDL and FORTRAN must be exactly matched to avoid
;	input conversion errors.  The correct ammounts of data must be
;	passed so that neither process 'hangs' waiting for communications
;	which will never occur.
;
;	The FORTRAN code performs some error checking which is
;	independent of IDL.  In cases of error the FORTRAN may write
;	error messages.  To prevent these error messages from conflicting
;	with the pipe communications all FORTRAN errors are written to
;	output stream 0, which is stderr for UNIX.  These error messages
;	will appear in the window from which the ADAS session/IDL session
;	is being run.
;
;	In the case of severe errors the FORTRAN code may terminate
;	itself prematurely.  In order to detect this, and prevent the
;	IDL program from 'hanging' or crashing, the IDL checks to see
;	if the FORTRAN executable is still an active process before
;	each group of pipe communications.  The process identifier
;	for the FORTRAN process, PID, is recorded when the process is
;	first 'spawned'.  The system is then checked for the presence
;	of the FORTRAN PID.
;
; USE:
;	First the system settings must be established by calling
;	adas_sys_set.pro then adas207.pro is called to start the
;	ADAS 207 application;
;
;	adas_sys_set, adasrel, fortdir, userroot, centroot, $
;		      devlist, devcode, font_large, font_small, edit_fonts
;	adas207,   adasrel, fortdir, userroot, centroot, devlist, $
;		   devcode, font_large, font_small, edit_fonts
;
; INPUTS:
;	ADASREL - A string indicating the ADAS system version, 
;		  e.g ' ADAS RELEASE: ADAS93 V1.1.0'.  The first
;		  character should be a space.
;
;	FORTDIR - A string holding the path to the directory where the
;		  FORTRAN executables are, e.g '/disk/adas/fortran/exec'
;
;	USERROOT - A string holding the path to the root directory of
;		   the user's adas data. e.g '/disk/bowen/adas'
;		   This root directory will be used by adas to construct
;		   other path names.  For example the users default data
;		   for adas205 should be in /disk/bowen/adas/adf04.  In
;		   particular the user's default interface settings will
;		   be stored in the directory USERROOT+'/defaults'.  An
;		   error will occur if the defaults directory does not
;		   exist.
;
;	CENTROOT - Like USERROOT, but this directory points to the
;		   central data area for the system.  User defaults are
;		   not stored on CENTROOT.
;
;	DEVLIST - A string array of hardcopy device names, used for
;		  graphical output. e.g ['Post-Script','HP-PCL','HP-GL']
;		  This array must mirror DEVCODE.  DEVCODE holds the
;		  actual device names used in a SET_PLOT statement.
;
;	DEVCODE - A string array of hardcopy device code names used in
;		  the SET_PLOT statement, e.g ['PS', 'PCL', 'HP']
;		  This array must mirror DEVLIST.
;
;	FONT_LARGE - The name of a larger font e.g 'courier_bold14'
;
;	FONT_SMALL - The name of a smaller font e.g 'courier_bold12'
;
;	EDIT_FONTS - A structure of two fonts used in the table editor
;		     adas_edtab.pro, {font_norm:'',font_input:''}.
;		     The two fonts are used to differentiate between
;		     editable and non-editable parts of the table. You
;		     may have to experiment to get two fonts which give
;		     the same default spacing in the edit widget.
;		     e.g {font_norm:'helvetica_bold14', $
;			  font_input:'helvetica_oblique14'}
;
; OPTIONAL INPUTS:
;	None
;
; OUTPUTS:
;	None - Note this routine should leave all inputs un-modified.
;
; OPTIONAL OUTPUTS:
;	None
;
; KEYWORD PARAMETERS:
;	None
;
; CALLS:
;	FIND_PROCESS	Checks to see if a given process is active.
;	B7SPF0		Pipe comms with FORTRAN B7SPF0 routine.
;	B7SETP		Pipe comms with FORTRAN B7SETP routine.
;	B7ISPF		Pipe comms with FORTRAN B7ISPF routine.
;	XXDATE		Get date and time from operating system.
;	B7SETR		Pipe comms with FORTRAN B7SETR routine.
;	B7SPF1		Pipe comms with FORTRAN B7SPF1 routine.
;	B7OUTG		Pipe comms with FORTRAN B7OUTG routine.
;
; SIDE EFFECTS:
;	This routine spawns a FORTRAN executable.  Note the pipe 
;	communications routines listed above.  In addition to these
;	pipe communications there is one explicit communication of the
;	date to the FORTRAN code, search for 'printf,pipe' to find it.
;
; CATEGORY:
;	Adas system.
;	
; WRITTEN:
;       S.P.Bellamy, Tessella Support Services plc, 20/03/1995
;
; MODIFIED:
;	1.1	S.P.Bellamy	
;		First release
;	1.2	Tim Hammond		
;		Corrected paths for user/central data
;	1.3	Tim Hammond		
;		Used smaller font for output options
;	1.4	Tim Hammond				
;		Corrected adasprog.
;	1.5	Tim Hammond
;		Corrected adasprog.
;	1.6	Tim Hammond
;		Corrected adasprog - again.
;	1.7	Tim Hammond
;		Updated version number to 1.4
;	1.8	Tim Hammond
;		Updated version number to 1.5
;	1.9	Tim Hammond
;		Updated version number to 1.6
;	1.10	William Osborn
;		Added menu button code
;	1.11	William Osborn
;		Updated version number to 1.7
;	1.12	William Osborn
;		Updated version number to 1.8
;	1.13	Richard Martin
;		Increased version number to 1.9
;	1.14	Richard Martin
;		Increased version number to 1.10
;	1.15	Richard Martin
;		Increased version number to 1.11
;	1.16	Richard Martin
;		Increased version number to 1.12
;
; VERSION:
;	1.1	20-03-95
;	1.2	12-05-95
;	1.3	24-05-95
;	1.4	05-06-95
;	1.5	14-07-95
;	1.6	14-07-95
;	1.7	16-02-96
;	1.8	16-02-96
;	1.9	13-05-96
;	1.10	04-06-96
;	1.11	11-07-96
;     1.12	14-10-96
;	1.13	16-11-01
;	1.14	18-03-02
;	1.15	18-03-03
;	1.16	08-09-03
;
;-
;-----------------------------------------------------------------------------


PRO ADAS207,	adasrel, fortdir, userroot, centroot, $
		devlist, devcode, font_large, font_small, edit_fonts

		;************************
		;**** Initialisation ****
		;************************

  adasprog = ' PROGRAM: ADAS207 V1.12'
  lpend = 0
  gomenu = 0
  deffile = userroot+'/defaults/adas207_defaults.dat'
  device = ''
  bitfile = centroot + '/bitmaps'

		;******************************************
		;**** Search for user default settings ****
		;**** If not found create defaults     ****
		;******************************************

  files = findfile(deffile)
  if files(0) eq deffile then begin

    restore,deffile
    inval.centroot = centroot+'/pass/'
    inval.userroot = userroot+'/pass/'

  end else begin

    inval = { $
		ROOTPATH:userroot+'/pass/', $
		FILE:'', $
		CENTROOT:centroot+'/pass/', $
		USERROOT:userroot+'/pass/' }

    procval = {LEQUIL:-1}

    outval = { $
		GRPOUT:1, GRPOPT:1, $
		GRPMES:' ', $
		G1TITL:'', $
		G1DFCS:1, $
		G1CSLG:1, $
		G1NCNT:1, $
		G1CONT:[0.0D], $
		G1CILG:1, $
		G2TITL:'', $
		G2NDEN:1, $
		G2DENS:[1], $
		G2DFGS:1, $
		G2XMIN:0.0D, $
		G2XMAX:1.0D, $
		G2YMIN:0.0D, $
		G2YMAX:1.0D, $
		G2RMES:' ', $
		G3TITL:'', $
		G3NTMP:1, $
		G3TEMP:[1], $
		G3DFGS:1, $
		G3XMIN:0.0D, $
		G3XMAX:1.0D, $
		G3YMIN:0.0D, $
		G3YMAX:1.0D, $
		G3RMES:' ', $
		HRDOUT:0, $
		HRDNAM:'', $
		HRDDEF:'', $
		HRDMES:' ', $
		DEVSEL:0, $
		TEXOUT:0, TEXAPP:-1, $
		TEXREP:0, TEXDSN:'paper.txt', $
		TEXDEF:'',TEXMES:' '   }

  end


		;****************************
		;**** Start fortran code ****
		;****************************

  spawn,fortdir+'/adas207.out',unit=pipe,/noshell,PID=pid

		;************************************************
		;**** Get date and write to fortran via pipe ****
		;************************************************
  date = xxdate()
  printf,pipe,date(0)

LABEL100:
		;**** Check FORTRAN still running ****
  
  if find_process(pid) eq 0 then goto, LABELEND

		;************************************************
		;**** Communicate with b7spf0 in fortran and ****
		;**** invoke user interface widget for       ****
		;**** Data file selection                    ****
		;************************************************

  b7spf0, pipe, inval, dsnpas, rep, FONT=font_large

		;**** If cancel selected then end program ****

  if rep eq 'YES' then goto, LABELEND

		;**** Check FORTRAN still running ****

  if find_process(pid) eq 0 then goto, LABELEND

		;*************************************
		;**** Read pipe comms from b7setp ****
		;*************************************

  b7setp, pipe, titled, sz, sz0, sionpt, dsninc, ndmet, ilev, strga, $
		ndtem, tea, ndden, densa


LABEL200:
		;**** Check FORTRAN still running ****

  if find_process(pid) eq 0 then goto, LABELEND

		;************************************************
		;**** Communicate with b7ispf in fortran and ****
		;**** invoke user interface widget for       ****
		;**** Processing options                     ****
		;************************************************
  b7ispf, pipe, lpend, procval, $
          titled, sz, sz0, sionpt, dsnpas, dsninc, $
          ndmet, ilev, strga, bitfile, gomenu,	$
	  FONT_LARGE=font_large, FONT_SMALL=font_small, $
	  EDIT_FONTS=edit_fonts

                ;**** If menu button clicked, tell FORTRAN to stop ****

  if gomenu eq 1 then begin
      printf, pipe, 1
      goto, LABELEND
  endif else begin
      printf, pipe, 0
  endelse

		;**** If cancel selected then goto 100 ****
  if lpend eq 1 then goto, LABEL100

		;**** Fortran processing now in progress. Get ****
		;**** date and time for this run.	      ****
  date = xxdate()
		;**** Create header for output. ****
  header = adasrel+adasprog+' DATE: '+date(0)+' TIME: '+date(1)

		;**** Check FORTRAN still running ****

  if find_process(pid) eq 0 then goto, LABELEND

		;*************************************
		;**** Read pipe comms from b7setr ****
		;*************************************

  b7setr, pipe, srmin, srmax

LABEL300:
		;**** Check FORTRAN still running ****
  if find_process(pid) eq 0 then goto, LABELEND

		;************************************************
		;**** Communicate with b7spf1 in fortran and ****
		;**** invoke user interface widget for       ****
		;**** Output options                         ****
		;************************************************

  b7spf1, pipe, lpend, outval, dsnpas, dsninc, srmin, srmax, densa, tea, $
		bitfile, gomenu, DEVLIST=devlist, FONT=font_small


                ;**** If menu button clicked, tell FORTRAN to stop ****

  if gomenu eq 1 then begin
      printf, pipe, 1
      outval.texmes = ''
      goto, LABELEND
  endif else begin
      printf, pipe, 0
  endelse

		;**** If cancel selected then erase output ****
		;**** messages and goto 200.		   ****
  if lpend eq 1 then begin
    outval.texmes = ''
    goto, LABEL200
  end

		;*************************************************
		;**** If text output has been requested write ****
		;**** header to pipe for XXADAS to read.      ****
		;*************************************************

  if outval.texout eq 1 then begin
    printf,pipe,header
  end

		;**** If graphical output requested ****

		;************************************************
		;**** Communicate with b7outg in fortran and ****
		;**** invoke widget Graphical output         ****
		;************************************************
  if outval.grpout eq 1 then begin
		;**** Hardcopy output ****
    hrdout = outval.hrdout
    hrdnam = outval.hrdnam
    if outval.devsel ge 0 then device = devcode(outval.devsel)

    b7outg, pipe, hrdout, hrdnam, device, header, bitfile, gomenu,$
		 FONT=font_large

                ;**** If menu button clicked, tell FORTRAN to stop ****

    if gomenu eq 1 then begin
        printf, pipe, 1
        outval.texmes = ''
        goto, LABELEND
    endif else begin
        printf, pipe, 0
    endelse

  end

		;**** Back for more output options ****
GOTO, LABEL300

LABELEND:

		;**** Ensure appending is not enabled for ****
		;**** text output at start of next run.   ****
outval.texapp = -1

		;**** Save user defaults ****
save, inval, procval, outval, filename=deffile


END
