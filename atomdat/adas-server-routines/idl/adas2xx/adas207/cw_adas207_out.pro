; Copyright (c) 1995 Strathclyde University.
; SCCS info: Module @(#)$Header: /home/adascvs/idl/adas2xx/adas207/cw_adas207_out.pro,v 1.5 2004/07/06 12:32:15 whitefor Exp $ Date $Date: 2004/07/06 12:32:15 $
;+
; PROJECT:
;       ADAS IBM MVS to UNIX conversion
;
; NAME:
;	CW_ADAS207_OUT()
;
; PURPOSE:
;	Produces a widget for ADAS207 output options.
;
; EXPLANATION:
;	This function declares a compound widget consisting of the
;	graphical output selection widget cw_adas_gr_sel.pro, and three
;	output file widgets cw_adas_outfile.pro.  The three text output
;	files specified in this widget are for tabular (paper.txt)
;	output, countour passing file and METPOP passing file.  This
;	widget also includes a button for browsing the comments from
;	the input dataset, a menu button, a 'Cancel' button and a 
;	'Done' button.
;
;	The compound widgets cw_adas_gr_sel.pro and cw_adas_outfile.pro
;	included in this file are self managing.  This widget only
;	handles events from the 'Done', 'Cancel' and 'Menu' buttons.
;
;	This widget only generates events for the 'Done' and 'Cancel'
;	buttons.
;
; USE:
;	This widget is specific to ADAS207, see adas207_out.pro	for use.
;
; INPUTS:
;	PARENT	- Long integer; ID of parent widget.
;
;	DSNPAS	- Name of contour passing file for this application.
;
;	DSNINC	- Name of input dataset for this application.
;
;	SRMIN	- Minimum spectrum line intensity ratio
;
;	SRMAX	- Maximum spectrum line intensity ratio
;
;	MAXD	- Maximum density index
;
;	MAXT	- Maximum temperature index
;
;	DENSA	- Electron Densities
;
;	TEA	- Electron Temperatures
;
;       BITFILE - String; the path to the dirctory containing bitmaps
;                 for the 'escape to series menu' button.
;
; OPTIONAL INPUTS:
;	None.
;
; OUTPUTS:
;	The return value of this function is the ID of the declared widget.
;
; OPTIONAL OUTPUTS:
;	None.
;
; KEYWORD PARAMETERS:
;	DEVLIST	- A string array;  Each element of the array is the
;		  name of a hardcopy graphics output device.  This
;		  list must mirror the DEVCODE list.  DEVLIST is the
;		  list which the user will see.
;		  e.g ['Post-Script','HP-PCL','HP-GL']
;
;	VALUE	- A structure which determines the initial settings of
;		  the output options widget. The default value is;
;
;		      { GRPOUT:1,
;			GRPOPT:1,
;			GRPMES:' ',
;
;			G1TITL:'',
;			G1DFCS:1,
;			G1CSLG:1,
;			G1NCNT:1,
;			G1CONT:[0.0D],
;			G1CILG:1,
;
;			G2TITL:'',
;			G2NDEN:1,
;			G2DENS:[1],
;			G2DFGS:1,
;			G2XMIN:0.0D,
;			G2XMAX:1.0D,
;			G2YMIN:0.0D,
;			G2YMAX:1.0D,
;			G2RMES:' ',
;
;			G3TITL:'',
;			G3NTMP:1,
;			G3TEMP:[1],
;			G3DFGS:1,
;			G3XMIN:0.0D,
;			G3XMAX:1.0D,
;			G3YMIN:0.0D,
;			G3YMAX:1.0D,
;			G3RMES:' ',
;
;			HRDOUT:0,
;			HRDNAM:'',
;			HRDDEF:'',
;			HRDMES:' ',
;			DEVSEL:0,
;
;			TEXOUT:0, TEXAPP:-1, $
;			TEXREP:0, TEXDSN:'', $
;			TEXDEF:'',TEXMES:' ' }
;
;			GRPOUT	Integer; Activation button 1 on, 0 off
;			GRPOPT	Integer; Graph type, 1 Contour, 2 slr/den, 3 slr/tmp
;			GRPMES	String; Graph selection error message
;
;			G1TITL	String; Title for contour plot
;			G1DFCS	Integer; Default contour scaling, 1 Yes, 0 No
;			G1CSLG	Integer; Contour spacing, 1 Log, 0 Lin
;			G1NCNT	Integer; Number of Contours
;			G1CONT	Double array; Contour values
;			G1CILG	Integer; Contour Interpolation, 1 Log, 0 Lin
;
;			G2TITL	String; Title for SP-Line vs Temp plot
;			G2NDEN	Integer; Number of Densities selected
;			G2DENS	Integer Array; Density Indexes
;			G2DFGS	Integer; Default graph scaling, 1 Yes, 0 No
;			G2XMIN	Double;	Temp. Minimum
;			G2XMAX	Double; Temp. Maximum
;			G2YMIN	Double; SP-Line Minimum
;			G2YMAX	Double; SP-Line Maximum
;			G2RMES	String; Range setting error message
;
;			G3TITL	String; Title for SP-Line vs Density plot
;			G3NTMP	Integer; Number of Temperatures selected
;			G3TEMP	Integer array; Temp. Indexes
;			G3DFGS	Integer; Default graph scaling, 1 Yes, 0 No
;			G3XMIN	Double;	Density Minimum
;			G3XMAX	Double; Density Maximum
;			G3YMIN	Double; SP-Line Minimum
;			G3YMAX	Double; SP-Line Maximum
;			G3RMES	String; Range setting error message
;			
;			HRDOUT	Integer; Hard copy activ' 1 on, 0 off
;			HRDNAM	String; Hard copy output file name
;			HRDDEF	String; Default output file name 
;			HRDMES	String; File name error message
;			DEVSEL	Integer; Index of selected device in DEVLIST
;
;		  For CW_ADAS_OUTFILE tabular output (paper.txt);
;			TEXOUT	Integer; Activation button 1 on, 0 off
;			TEXAPP	Integer; Append button 1 on, 0 off, -1 no button
;			TEXREP	Integer; Replace button 1 on, 0 off, -1 no button
;			TEXDSN	String; Output file name
;			TEXDEF	String; Default file name
;			TEXMES	String; File name error message
;
;
;	UVALUE	- A user value for this widget.
;
;	FONT	- String; the name of a font to be used for all text in
;		  this widget.  Default to system default font.
;
; CALLS:
;	CW_ADAS_DSBR	Input dataset name and comments browse button.
;	CW_ADAS_OUTFILE	Output file name entry widget.
;	See side effects for other related widget management routines.
;
; SIDE EFFECTS:
;	The following widget management routines are included in this
;	file;
;	OUT207_GET_VAL()
;	OUT207_EVENT()
;
; CATEGORY:
;	Compound Widget
;
; WRITTEN:
;       S.P.Bellamy, Tessella Support Services plc, 21/03/1995
;
; MODIFIED:
;	1.1	Lalit Jalota, Tessella Support Services plc
;		First version
;	1.2	Tim Hammond, Tessella Support Services plc
;		Modified screen to make it shorter
;	1.3	William Osborn, Tessella Support Services plc
;		Added wait statement at end to give the window manager
;		time to set things up before calling xmanager
;	1.4	William Osborn, Tessella Support Services plc
;		Added menu button. Changed position of
;		widget_control, base, sensitive=... until after whole base has
;		been defined. Otherwise, if set to be insensitive, no further
;		widget_control statements have an effect on the sensitivity
;		of the items in the base.
;	1.5	William Osborn
;		Removed xsize=n keyword from widget_label command.
;		The dynlabel procedure does not override these
;		assignments and they were taken as being values in pixels
;		on some machines.
; VERSION:
;	1.1	30-03-95
;	1.2	26-02-96
;	1.3	16-05-96
;	1.4	04-06-96
;	1.5	01-08-96
;
;-

;-----------------------------------------------------------------------------

FUNCTION out207_get_val, id

                ;**** Return to caller on error ****
  ON_ERROR, 2

                ;**** Retrieve the state ****
  first_child = widget_info( id, /child )
  widget_control, first_child, get_uvalue=state, /no_copy

		;**** Get graphical output settings ****
  widget_control, state.groutid, get_uvalue=grpout

		;**** Clear graph messages ****
  graph_error = 0
  widget_control, state.grmesid, set_value=' '

		;**** Output settings for Contour Plot ****
  widget_control, state.g1ttlid, get_value=g1titl
  if( strlen( g1titl(0) ) gt 30 ) then begin
	g1titl = strmid( g1titl(0), 0, 30 )
        if not graph_error then begin
		widget_control, state.grmesid, $
			set_value='Warning : Title truncated to 30 characters'
		wait, 1.0
		widget_control, state.g1ttlid, set_value=g1titl
	endif
  endif else begin
	g1titl = g1titl(0)
  endelse
  widget_control, state.g1dcsid, get_uvalue=g1dfcs
  widget_control, state.g1cspid, get_value=g1cslg
  widget_control, state.g1conid, get_value=conos
  list = where( strlen( conos.value(0,*) ), g1ncnt )
  g1ncnt = fix( g1ncnt )
  if g1ncnt ne 0 then g1cont = double( conos.value(0,list) ) else begin
	g1cont = [0.0D]
	if not graph_error then begin
		graph_error = 1
		widget_control, state.grmesid, $
			set_value='**** No contour level defined ****'
	endif
  endelse
  widget_control, state.g1cinid, get_value=g1cilg

		;**** Output settings for SL-Ratio vs Temperature Plot ****
  widget_control, state.g2ttlid, get_value=g2titl
  if( strlen( g2titl(0) ) gt 30 ) then begin
	g2titl = strmid( g2titl(0), 0, 30 )
        if not graph_error then begin
		widget_control, state.grmesid, $
			set_value='Warning : Title truncated to 30 characters'
		wait, 1.0
		widget_control, state.g2ttlid, set_value=g2titl
	endif
  endif else begin
	g2titl = g2titl(0)
  endelse
  widget_control, state.g2denid, get_value=g2dens
  g2dens = g2dens + 1
  list = where( g2dens, g2nden )
  g2nden = fix( g2nden )
  if g2nden ne 0 then g2dens = g2dens( list ) else begin
	g2dens = [0]
	if not graph_error then begin
		graph_error = 1
		widget_control, state.grmesid, $
			set_value='**** No densities selected ****'
	endif
  endelse
  widget_control, state.g2rngid, get_value=g2rnge

		;**** Output settings for SL-Ratio vs Density Plot ****
  widget_control, state.g3ttlid, get_value=g3titl
  if( strlen( g3titl(0) ) gt 30 ) then begin
	g3titl = strmid( g3titl(0), 0, 30 )
        if not graph_error then begin
		widget_control, state.grmesid, $
			set_value='Warning : Title truncated to 30 characters'
		wait, 1.0
		widget_control, state.g3ttlid, set_value=g3titl
	endif
  endif else begin
	g3titl = g3titl(0)
  endelse
  widget_control, state.g3teaid, get_value=g3temp
  g3temp = g3temp + 1
  list = where( g3temp, g3ntmp )
  g3ntmp = fix( g3ntmp )
  if g3ntmp ne 0 then g3temp = g3temp( list ) else begin
	g3temp = [0]
	if not graph_error then begin
		graph_error = 1
		widget_control, state.grmesid, $
			set_value='**** No temperatures selected ****'
	endif
  endelse
  widget_control, state.g3rngid, get_value=g3rnge

		;**** Get graph messages ****
  if graph_error then widget_control, state.grmesid, get_value=grpmes $
	else grpmes = ' '

		;**** Get hardcopy settings ****
  widget_control, state.hrdcpid, get_value=hrdos
  widget_control, state.hcdevid, get_value=devsel


		;**** Get text output settings ****
  widget_control, state.paperid, get_value=papos

		;**** Get which graph is selected ****

  widget_control, state.gropval, get_value=grop
  grpopt = grop(0) + 1


  os = { 	GRPOUT:grpout, $
		GRPOPT:grpopt, $
		GRPMES:grpmes, $
		G1TITL:g1titl, $
		G1DFCS:g1dfcs, $
		G1CSLG:g1cslg, $
		G1NCNT:g1ncnt, $
		G1CONT:g1cont, $
		G1CILG:g1cilg, $
		G2TITL:g2titl, $
		G2NDEN:g2nden, $
		G2DENS:g2dens, $
		G2DFGS:(g2rnge.scalbut eq 0), $
		G2XMIN:double( g2rnge.xmin ), $
		G2XMAX:double( g2rnge.xmax ), $
		G2YMIN:double( g2rnge.ymin ), $
		G2YMAX:double( g2rnge.ymax ), $
		G2RMES:g2rnge.grprmess, $
		G3TITL:g3titl, $
		G3NTMP:g3ntmp, $
		G3TEMP:g3temp, $
		G3DFGS:(g3rnge.scalbut eq 0), $
		G3XMIN:double( g3rnge.xmin ), $
		G3XMAX:double( g3rnge.xmax ), $
		G3YMIN:double( g3rnge.ymin ), $
		G3YMAX:double( g3rnge.ymax ), $
		G3RMES:g3rnge.grprmess, $
		HRDOUT:hrdos.outbut, $
		HRDNAM:hrdos.filename, $
		HRDDEF:hrdos.defname, $
		HRDMES:hrdos.message, $
		DEVSEL:devsel, $
		TEXOUT:papos.outbut,  TEXAPP:papos.appbut, $
		TEXREP:papos.repbut,  TEXDSN:papos.filename, $
		TEXDEF:papos.defname, TEXMES:papos.message   }

  widget_control, first_child, set_uvalue=state, /no_copy

  RETURN, os 

END

;-----------------------------------------------------------------------------

FUNCTION out207_event, event

                ;**** Base ID of compound widget ****
  parent=event.handler

                ;**** Retrieve the state ****
  first_child = widget_info( parent, /child )
  widget_control, first_child, get_uvalue=state, /no_copy

		;*********************************
		;**** Clear previous messages ****
		;*********************************
  widget_control, state.messid, set_value=' '

                ;************************
                ;**** Process events ****
                ;************************
  CASE event.id OF

		;*********************************
		;**** Graph Activation button ****
		;*********************************
    state.groutid: begin
		widget_control, state.groutid, set_uvalue=event.select
		widget_control, state.actbase, sensitive=event.select
		widget_control, state.grmesid, sensitive=event.select
		widget_control, state.hrdbase, sensitive=event.select
		widget_control, state.gropval, sensitive=event.select

		;**** If graphs being de-activated then turn off hardcopy ****
		if not event.select then begin
			widget_control, state.hrdcpid, get_uvalue=hrdos
			hrdos.outbut = 0
			widget_control, state.hrdcpid, set_value=hrdos, /no_copy
		endif

	end

		;**** Graph option selector ****
    state.gropval: begin
	widget_control, state.gropval, get_value=gval
	if gval eq 0 then begin
	    widget_control, state.gr1base, map=1
	    widget_control, state.gr2base, map=0
	    widget_control, state.gr3base, map=0
	endif else if gval eq 1 then begin
	    widget_control, state.gr1base, map=0
	    widget_control, state.gr2base, map=1
	    widget_control, state.gr3base, map=0
	endif else begin
	    widget_control, state.gr1base, map=0
	    widget_control, state.gr2base, map=0
	    widget_control, state.gr3base, map=1
	endelse
    end

		;****************************************
		;**** Graph 1 Default Scaling button ****
		;****************************************
    state.g1dcsid: begin
		widget_control, state.g1dcsid, set_uvalue=event.select
		widget_control, state.g1cspid, sensitive=event.select
		widget_control, state.g1conid, sensitive=(event.select eq 0)
	end

		;***********************
		;**** Cancel button ****
		;***********************
    state.cancelid: new_event = {ID:parent, TOP:event.top, $
				HANDLER:0L, ACTION:'Cancel'}

		;*********************
		;**** Done button ****
		;*********************
    state.doneid: begin

		;***************************************
		;**** Check for errors in the input ****
		;***************************************
	  error = 0

		;**** Get a copy of the widget value ****
	  widget_control, first_child, set_uvalue=state, /no_copy
	  widget_control,event.handler,get_value=os
	  widget_control, first_child, get_uvalue=state, /no_copy

		;**** Check for widget error messages ****
	  if os.grpout eq 1 and strtrim(os.grpmes) ne '' then error=1
	  if os.grpout eq 1 and os.g2dfgs eq 1 and $
				strtrim(os.g2rmes) ne '' then error=1
	  if os.grpout eq 1 and os.g3dfgs eq 1 and $
				strtrim(os.g3rmes) ne '' then error=1
	  if os.texout eq 1 and strtrim(os.texmes) ne '' then error=1


	  if error eq 1 then begin
	    widget_control,state.messid,set_value= $
				'**** Error in output settings ****'
	    new_event = 0L
	  end else begin
	    new_event = {ID:parent, TOP:event.top, HANDLER:0L, ACTION:'Done'}
	  end

        end

		;*********************
                ;**** Menu button ****
		;*********************

    state.outid: begin
            new_event = {ID:parent, TOP:event.top, HANDLER:0L,          $
                         ACTION:'Menu'}
        end

    ELSE: new_event = 0L

  ENDCASE

		;**********************************
		;**** Save 'state' information ****
		;**********************************
  widget_control, first_child, set_uvalue=state, /no_copy

  RETURN, new_event
END

;-----------------------------------------------------------------------------

FUNCTION cw_adas207_out, parent, dsnpas, dsninc, srmin, srmax, $
		maxd, maxt, densa, tea, bitfile, $
		DEVLIST=devlist, $
		VALUE=value, UVALUE=uvalue, $
		NUM_FORM=num_form, FONT=font


		;**** Set defaults for keywords ****
  IF NOT (KEYWORD_SET(devlist)) THEN devlist = ['No Device']
  IF NOT (KEYWORD_SET(uvalue)) THEN uvalue = 0
  IF NOT (KEYWORD_SET(font)) THEN font = ''
  IF NOT (KEYWORD_SET(num_form)) THEN num_form = '(E8.2)'
  IF NOT (KEYWORD_SET(value)) THEN begin
	os = {		GRPOUT:1, $
			GRPOPT:1, $
			GRPMES:' ', $
			G1TITL:'', $
			G1DFCS:1, $
			G1CSLG:1, $
			G1NCNT:1, $
			G1CONT:[0.0D], $
			G1CILG:1, $
			G2TITL:'', $
			G2NDEN:1, $
			G2DENS:[1], $
			G2DFGS:1, $
			G2XMIN:0.0D, $
			G2XMAX:1.0D, $
			G2YMIN:0.0D, $
			G2YMAX:1.0D, $
			G2RMES:' ', $
			G3TITL:'', $
			G3NTMP:1, $
			G3TEMP:[1], $
			G3DFGS:1, $
			G3XMIN:0.0D, $
			G3XMAX:1.0D, $
			G3YMIN:0.0D, $
			G3YMAX:1.0D, $
			G3RMES:' ', $
			HRDOUT:0, $
			HRDNAM:'', $
			HRDDEF:'', $
			HRDMES:' ', $
			DEVSEL:0, $
			TEXOUT:0, TEXAPP:-1, $
			TEXREP:0, TEXDSN:'', $
			TEXDEF:'',TEXMES:' '   }
  END ELSE BEGIN
	if max( value.g2dens ) gt maxd then begin
		g2nden = 1
		g2dens = [1]
	endif else begin
		g2nden = value.g2nden
		g2dens = [value.g2dens]
	endelse
	if max( value.g3temp ) gt maxt then begin
		g3ntmp = 1
		g3temp = [1]
	endif else begin
		g3ntmp = value.g3ntmp
		g3temp = [value.g3temp]
	endelse
	devsiz = size( devlist )
	if value.devsel gt devsiz(1) then devsel = 0 else devsel = value.devsel
	os = {		GRPOUT:value.grpout, $
			GRPOPT:value.grpopt, $
			GRPMES:value.grpmes, $
			G1TITL:value.g1titl, $
			G1DFCS:value.g1dfcs, $
			G1CSLG:value.g1cslg, $
			G1NCNT:value.g1ncnt, $
			G1CONT:[value.g1cont], $
			G1CILG:value.g1cilg, $
			G2TITL:value.g2titl, $
			G2NDEN:g2nden, $
			G2DENS:g2dens, $
			G2DFGS:value.g2dfgs, $
			G2XMIN:value.g2xmin, $
			G2XMAX:value.g2xmax, $
			G2YMIN:value.g2ymin, $
			G2YMAX:value.g2ymax, $
			G2RMES:value.g2rmes, $
			G3TITL:value.g3titl, $
			G3NTMP:value.g3ntmp, $
			G3TEMP:value.g3temp, $
			G3DFGS:value.g3dfgs, $
			G3XMIN:value.g3xmin, $
			G3XMAX:value.g3xmax, $
			G3YMIN:value.g3ymin, $
			G3YMAX:value.g3ymax, $
			G3RMES:value.g3rmes, $
			HRDOUT:value.hrdout, $
			HRDNAM:value.hrdnam, $
			HRDDEF:value.hrddef, $
			HRDMES:value.hrdmes, $
			DEVSEL:devsel, $
			TEXOUT:value.texout, TEXAPP:value.texapp, $
			TEXREP:value.texrep, TEXDSN:value.texdsn, $
			TEXDEF:value.texdef, TEXMES:value.texmes   }
  END

		;**********************************************
		;**** Create the 207 Output options widget ****
		;**********************************************

		;**** Set various parameters depending on the machine ****

  machine = GETENV('TARGET_MACHINE')
  if machine eq 'HPUX' then begin
    yc_size = 2
    yt_size = 2
  endif else begin
    yc_size = 3
    yt_size = 3
  endelse
		;**** create base widget ****
  cwid = widget_base( parent, UVALUE = uvalue, $
			EVENT_FUNC = "out207_event", $
			FUNC_GET_VALUE = "out207_get_val", $
			/COLUMN)

		;********************************************************
		;**** Create a dummy widget just to hold value of    ****
		;**** "state" variable so as not to get confused     ****
		;**** with any other values. Adopt IDL practice      ****
		;**** of using first child widget                    ****
		;********************************************************

  first_child = widget_base( cwid )

  topbase = widget_base( first_child, /column )

		;**** add pass file name ****
  rc = widget_label( topbase, font=font, $
			value='Contour Passing File Name:'+dsnpas )

		;**** Add dataset name and browse button ****
  rc = cw_adas_dsbr(topbase,dsninc,font=font)

		;**** add ratio information ****
  rc = widget_label( topbase, font=font, $
		value='Spectrum Line Intensity Ratio range: '+srmin+' - '+srmax )

		;**** Base widget for graph selection ****
  base = widget_base( cwid, /column, /frame )

		;**** Base widget for graph activation ****
  base2 = widget_base(base, /row)
  actbase = widget_base( base2, /nonexclusive )

  		;**** Graph activation button ****
  groutid = widget_button( actbase, value='Graphical Output:', font=font, uvalue=os.grpout )
  widget_control, groutid, set_button=os.grpout
  
		;**** Base widget for graph option ****
  gropbase = widget_base(base2)

  		;**** Graph option buttons ****
  gropnames = ['Diagnostic Contour Plot of spectrum line ratios on T'+	$
	       'emp/Density Plane',					$
	       'Spectrum Line Ratios against Temperature for a given'+	$
	       ' Density',						$
	       'Spectrum Line Ratios against Density for a given '+	$
	       'Temperature']
  gropval = cw_bselector( gropbase, gropnames, set_value=(os.grpopt-1),	$
			  font=font)

		;**** Base widgets for graph sections ****
  actbase = widget_base( base, /frame )
  gr1base = widget_base( actbase, /column )
  if os.grpopt ne 1 then widget_control, gr1base, map=0
  gr2base = widget_base( actbase, /column )
  if os.grpopt ne 2 then widget_control, gr2base, map=0
  gr3base = widget_base( actbase, /column )
  if os.grpopt ne 3 then widget_control, gr3base, map=0

  grmesid = widget_label( base, value=os.grpmes, font=font )
  widget_control, grmesid, sensitive=os.grpout

		;**** Widgets for Contour Plot ****
  tbase = widget_base( gr1base, /row )
  rc = widget_label( tbase, value='Diagnostic Contour Plot Title', font=font )
  g1ttlid = widget_text( tbase, value=os.g1titl, /editable, font=font, xsize=40 )

  tbase = widget_base( gr1base, /nonexclusive )
  g1dcsid = widget_button( tbase, value='Default Contour Scaling', $
			font=font, uvalue=os.g1dfcs )
  widget_control, g1dcsid, set_button=os.g1dfcs
  
  tbase = widget_base( gr1base, /row )
  g1cspid = cw_single_sel( tbase, ['Linear','Logarithmic'], $
			value=os.g1cslg, title='Contour Spacing', ysize=2, $
			font=font, big_font=font )
  widget_control, g1cspid, sensitive=os.g1dfcs

  tabcon = strarr( 1, 20 )
  tabcon(0,0:os.g1ncnt-1) = string(os.g1cont,format=num_form)
  g1conid = cw_adas_table( tbase, tabcon, coledit=[1], $
			ytexsize=yc_size, ytabsize=10, /scroll, $
			order=[1], num_form=num_form, $
			font_small=font, font_large=font, $
			fonts={FONT_NORM:font,FONT_INPUT:font}, $
			title='Contour Values' )
  widget_control, g1conid, sensitive=(os.g1dfcs eq 0)

  g1cinid = cw_single_sel( tbase, ['Linear','Logarithmic'], $
			value=os.g1cilg, title='Contour Interpolation', ysize=2, $
			font=font, big_font=font )


		;**** Widgets for SLR/Temp. Plot ****
  tbase = widget_base( gr2base, /row )
  rc = widget_label( tbase, value='Spectrum Line Ratio vs. Temperature Plot Title', font=font )
  g2ttlid = widget_text( tbase, value=os.g2titl, /editable, font=font, xsize=40 )

  tbase = widget_base( gr2base, /row )

  selected=intarr(10)
  if os.g2nden gt 0 then selected(0:os.g2nden-1) = os.g2dens
  selected = selected - 1
  g2denid = cw_adas_sel( tbase, densa(0:maxd-1), selected=selected, $
		title='Electron Densities (cm-3)', /ordered, $
		font=font, ytexsize=yt_size )

  range = {	SCALBUT:(os.g2dfgs eq 0), $
		XMIN:string( os.g2xmin, format=num_form ), $
		XMAX:string(os.g2xmax, format=num_form ), $
		YMIN:string(os.g2ymin, format=num_form ), $
		YMAX:string(os.g2ymax, format=num_form ), $
		GRPRMESS:os.g2rmes }
  g2rngid = cw_adas_ranges( tbase, value=range, font=font )

		;**** Widgets for SLR/Dens. Plot ****
  tbase = widget_base( gr3base, /row )
  rc = widget_label( tbase, value='Spectrum Line Ratio vs. Density Plot Title', font=font )
  g3ttlid = widget_text( tbase, value=os.g3titl, /editable, font=font, xsize=40 )

  tbase = widget_base( gr3base, /row )

  selected=intarr(10)
  if os.g3ntmp gt 0 then selected(0:os.g3ntmp-1) = os.g3temp
  selected = selected - 1
  g3teaid = cw_adas_sel( tbase, tea(0:maxt-1), selected=selected, $
		title='Electron Temperatures (Kelvin)', /ordered, $
		font=font, ytexsize=yt_size )

  range = {	SCALBUT:(os.g3dfgs eq 0), $
		XMIN:string( os.g3xmin, format=num_form ), $
		XMAX:string(os.g3xmax, format=num_form ), $
		YMIN:string(os.g3ymin, format=num_form ), $
		YMAX:string(os.g3ymax, format=num_form ), $
		GRPRMESS:os.g3rmes }
  g3rngid = cw_adas_ranges( tbase, value=range, font=font )

  widget_control, actbase, sensitive=os.grpout

		;**** Base Widget for hardcopy ****
  hrdbase = widget_base( base, /row )

		;**** Widget for hardcopy file ****
  outfval = { OUTBUT:os.hrdout, APPBUT:-1, REPBUT:0, $
	      FILENAME:os.hrdnam, DEFNAME:os.hrddef, MESSAGE:os.hrdmes }
  tbase = widget_base( hrdbase, /frame )
  hrdcpid = cw_adas_outfile( tbase, OUTPUT='Enable Hard Copy', $
                        VALUE=outfval, UVALUE=outfval, FONT=font )

		;**** Widget for hardcopy device****
  hcdevid = cw_single_sel( hrdbase, devlist, value=os.devsel, $
			font=font, big_font=font, ysize=3, $
			title='Select Device' )

  widget_control, hrdbase, sensitive=os.grpout

		;**** Widget for text output ****
  outfval = { OUTBUT:os.texout, APPBUT:os.texapp, REPBUT:os.texrep, $
	      FILENAME:os.texdsn, DEFNAME:os.texdef, MESSAGE:os.texmes }
  base = widget_base( cwid, /row, /frame )
  paperid = cw_adas_outfile( base, OUTPUT='Text Output', $
                        VALUE=outfval, FONT=font )


		;**** Error message ****
  messid = widget_label(cwid,value=' ',font=font)

		;**** add the exit buttons ****
  base = widget_base(cwid,/row)
  menufile = bitfile + '/menu.bmp'
  read_X11_bitmap, menufile, bitmap1
  outid = widget_button(base, value=bitmap1)          ;menu button
  cancelid = widget_button(base,value='Cancel',font=font)
  doneid = widget_button(base,value='Done',font=font)
  
		;**** create a state structure for the pop-up ****
		;**** window.                                 ****
  new_state = { GROUTID:groutid, GRMESID:grmesid, $
		ACTBASE:actbase, HRDBASE:hrdbase, $
		GR1BASE:gr1base, GR2BASE:gr2base, GR3BASE:gr3base, $
		G1TTLID:g1ttlid, G2TTLID:g2ttlid, G3TTLID:g3ttlid, $
		G1DCSID:g1dcsid, G1CSPID:g1cspid, $
		G1CONID:g1conid, G1CINID:g1cinid, $
		G2DENID:g2denid, G2RNGID:g2rngid, $
		G3TEAID:g3teaid, G3RNGID:g3rngid, $
		HRDCPID:hrdcpid, HCDEVID:hcdevid, PAPERID:paperid, $
		CANCELID:cancelid, DONEID:doneid, MESSID:messid,	$
		GROPVAL:gropval, OUTID:outid			 	}

                ;**** Save initial state structure ****
  widget_control, first_child, set_uvalue=new_state, /no_copy

  wait,1.0
  RETURN, cwid

END

