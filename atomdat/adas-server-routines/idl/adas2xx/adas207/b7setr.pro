; Copyright (c) 1995, Strathclyde University.
; SCCS info: Module @(#)$Header: /home/adascvs/idl/adas2xx/adas207/b7setr.pro,v 1.1 2004/07/06 11:28:08 whitefor Exp $ Date $Date: 2004/07/06 11:28:08 $
;+
; PROJECT:
;       ADAS IBM MVS to UNIX conversion
;
; NAME:
;	B7SETP
;
; PURPOSE:
;	IDL communications with ADAS207 FORTRAN process via pipe.
;
; EXPLANATION:
;	This IDL routine reads data from the ADAS207 FORTRAN subroutine
;	B7SETR via a UNIX pipe.  In the original IBM version the FORTRAN
;	subroutine B7SETR put information needed by the ISPF interface
;	into the ISPF pool area for later use.  Now the same information
;	is written to IDL for use in its user unterface.
;
; USE:
;	The use of this routine is specific to ADAS207.  See
;	adas207.pro for an example.
;
; INPUTS:
;	PIPE	- The IDL unit number of the bi-directional pipe to the
;		  ADAS207 FORTRAN process.
;
; OPTIONAL INPUTS:
;	None
;
; OUTPUTS:
;	SRMIN	- Minimum spectrum line intensity ratio.
;
;	SRMAX	- Maximum spectrum line intensity ratio
;
;
; OPTIONAL OUTPUTS:
;	None
;
; KEYWORD PARAMETES:
;	None
;
; CALLS:
;	None
;
; SIDE EFFECTS:
;	Reads data from a pipe.
;
; CATEGORY:
;	Adas system.
;
; WRITTEN:
;       S.P.Bellamy, Tessella Support Services plc, 20/03/1995
;
; MODIFIED:
;
; VERSION:
;
;-----------------------------------------------------------------------------



PRO b7setr, pipe, srmin, srmax

		;**********************************
		;**** Initialise new variables ****
		;**********************************
  srmin = ''
  srmax = ''

		;********************************
		;**** Read data from fortran ****
		;********************************
  readf,pipe,srmin
  readf,pipe,srmax

END
