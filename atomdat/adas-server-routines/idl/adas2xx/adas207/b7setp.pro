; Copyright (c) 1995, Strathclyde University.
; SCCS info: Module @(#)$Header: /home/adascvs/idl/adas2xx/adas207/b7setp.pro,v 1.1 2004/07/06 11:28:02 whitefor Exp $ Date $Date: 2004/07/06 11:28:02 $
;+
; PROJECT:
;       ADAS IBM MVS to UNIX conversion
;
; NAME:
;	B7SETP
;
; PURPOSE:
;	IDL communications with ADAS207 FORTRAN process via pipe.
;
; EXPLANATION:
;	This IDL routine reads data from the ADAS207 FORTRAN subroutine
;	B7SETP via a UNIX pipe.  In the original IBM version the FORTRAN
;	subroutine B7SETP put information needed by the ISPF interface
;	into the ISPF pool area for later use.  Now the same information
;	is written to IDL for use in its user unterface.
;
; USE:
;	The use of this routine is specific to ADAS207.  See
;	adas207.pro for an example.
;
; INPUTS:
;	PIPE	- The IDL unit number of the bi-directional pipe to the
;		  ADAS207 FORTRAN process.
;
; OPTIONAL INPUTS:
;	None
;
; OUTPUTS:
;	TITLED	- ELEMENT SYMBOL.
;
;	SZ	- NUCLEAR CHARGE READ
;
;	SZ0	- RECOMBINED ION CHARGE READ
;
;	SIONPT	- IONISATION POTENTIAL (UNITS: WAVE NUMBERS)
;
;X	SCNTE	- NUMBER OF ELECTRON IMPACT TRANSITIONS
;X
;X	SIL	- NUMBER OF ENERGY LEVELS
;X
;X	SRAT1	- '  ' => NO CHARGE EXCHANGE OR FREE ELECTRON
;X                         RECOMBINATIONS
;X                 'H ' => CHARGE EXCHANGE RECOMBINATIONS BUT
;X                         NO FREE ELECTRON RECOMBINATIONS
;X                 ' R' => FREE ELECTRON RECOMBINATIONS BUT
;X                         NO CHARGE EXCHANGE RECOMBINATIONS
;X                 'HR' => CHARGE EXCHANGE AND FREE ELECTRON
;X                         RECOMBINATIONS
;
;	DSNINC	- DATA SET NAME OF ASSOCIATED COPASE FILE
;
;	NDMET	- MAXIMUM NUMBER OF METASTABLES ALLOWED
;
;	ILEV	- ARRAY COUNTER FOR LEVEL INDEX
;
;	STRGA	- LEVEL DESIGNATIONS
;                 DIMENSION: LEVEL INDEX
;
;	NDTEM	- MAXIUMUM NUMBER OF TEMPERATURES ALLOWED
;
;	TEA	- ELECTRON TEMPERATURES  (UNITS: KELVIN)
;
;	NDDEN	- MAXIUMUM NUMBER OF DENSITIES ALLOWED
;
;	DENSA	- ELECTRON DENSITIES  (UNITS: CM-3)
;
;	CRATHA	- PARAMETER = DEFAULT VALUE FOR (NEUTRAL H
;                             DENSITY/ELECTRON DENSITY) RATIO
;
;	CRATIA	- PARAMETER = DEFAULT VALUE FOR (STAGE ABUND-
;                             ANCIES) RATIO.
;
;
; OPTIONAL OUTPUTS:
;	None
;
; KEYWORD PARAMETERS:
;	None
;
; CALLS:
;	None
;
; SIDE EFFECTS:
;	Reads data from a pipe.
;
; CATEGORY:
;	Adas system.
;
; WRITTEN:
;       S.P.Bellamy, Tessella Support Services plc, 13/03/1995
;
; MODIFIED:
;
; VERSION:
;
;-----------------------------------------------------------------------------



PRO b7setp, pipe, titled, sz, sz0, sionpt, dsninc, ndmet, ilev, strga, $
			ndtem, tea, ndden, densa

		;**********************************
		;**** Initialise new variables ****
		;**********************************
  titled = ''
  sz = ''
  sz0= ''
  sionpt = ''
  scnte = ''
  sil = ''
  srat1 = ''
  dsninc = ''
  ndmet = 0
  ndtem = 0
  ndden = 0

  input = ''

		;********************************
		;**** Read data from fortran ****
		;********************************
  readf,pipe,titled
  readf,pipe,sz
  readf,pipe,sz0
  readf,pipe,sionpt
  readf,pipe,scnte
  readf,pipe,sil
  readf,pipe,srat1
  readf,pipe,input
  dsninc = strtrim( input )

  readf,pipe,ndmet
  ilev = strarr( ndmet )
  strga = strarr( ndmet )

  for i = 0, ndmet-1 do begin
    readf,pipe,input
    ilev(i) = input
    readf,pipe,input
    strga(i) = input
  end

  readf,pipe,ndtem
  tea = strarr( ndtem )

  for i = 0, ndtem-1 do begin
    readf,pipe,input
    tea(i) = input
  end

  readf,pipe,ndden
  densa = strarr( ndden )
  cratha = strarr( ndden )
  cratia = strarr( ndden )

  for i = 0, ndden-1 do begin
    readf,pipe,input
    densa(i) = input
    readf,pipe,input
    cratha(i) = input
    readf,pipe,input
    cratia(i) = input
  end

END
