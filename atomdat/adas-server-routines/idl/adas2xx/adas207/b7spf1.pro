; Copyright (c) 1995 Strathclyde University.
; SCCS info: @(#)$Header: /home/adascvs/idl/adas2xx/adas207/b7spf1.pro,v 1.3 2004/07/06 11:28:35 whitefor Exp $ Date $Date: 2004/07/06 11:28:35 $
;
;+
; PROJECT:
;       ADAS IBM MVS to UNIX conversion
;
; NAME:
;	B7SPF1
;
; PURPOSE:
;	IDL user interface and communications with ADAS207 FORTRAN
;	process via pipe.
;
; EXPLANATION:
;	First this routine reads some information from ADAS207 FORTRAN
;	via the UNIX pipe.  Next the 'Output Options' part of the
;	interface is invoked.  Finally the results of the user's
;	interactions are written back to the FORTRAN via the pipe.
;	Communications are with the FORTRAN subroutine B7SPF1.
;
; USE:
;	The use of this routine is specific to ADAS207, See adas207.pro.
;
; INPUTS:
;	PIPE	- The IDL unit number of the bi-directional pipe to the
;		  ADAS207 FORTRAN process.
;
;	VALUE	- A structure which determines the initial settings of
;		  the 'Output Options' widget.  The initial value is
;		  set in adas207.pro.  VALUE is passed un-modified
;		  through to cw_adas207_out.pro, see that routine for a full
;		  description.
;
;	DSNPAS	- Name of contour passing file for this application.
;
;	DSNINC	- Name of input dataset for this application.
;
;	SRMIN	- Minimum spectrum line intensity ratio
;
;	SRMAX	- Maximum spectrum line intensity ratio
;
;	DENSA	- Electron Densities
;
;	TEA	- Electron Temperatures
;
;	HEADER	- ADAS version header information for use in printed
;		  output.  Written to FORTRAN routine XXADAS via pipe.
;
;       BITFILE - String; the path to the dirctory containing bitmaps
;                 for the 'escape to series menu' button.
;
; OPTIONAL INPUTS:
;	None.
;
; OUTPUTS:
;	LPEND	- Integer; indicates user action on leaving output
;		  selections.  lpend is set to 1 if the user exited
;		  with the 'Cancel' button and 0 if the user exited
;		  with 'Done'.
;
;	VALUE	- On output the structure records the final settings of
;		  the output selection widget if the user pressed the
;		  'Done' button otherwise it is not changed from input.
;
;       GOMENU  - Int; flag - set to 1 if user has selected 'escape direct
;                 to series menu , otherwise 0
;
; OPTIONAL OUTPUTS:
;	None.
;
; KEYWORD PARAMETERS:
;	DEVLIST	- A string array;  Each element of the array is the
;		  name of a hardcopy graphics output device.  This
;		  list must mirror the DEVCODE list.  DEVLIST is the
;		  list which the user will see.
;		  e.g ['Post-Script','HP-PCL','HP-GL']
;
;	FONT	- Supplies the font to be used for the interface widgets.
;
; CALLS:
;	ADAS207_OUT	User interface - output options.
;
; SIDE EFFECTS:
;	This routine communicates with the ADAS207 FORTRAN process
;	via a UNIX pipe.
;
; CATEGORY:
;	Adas system.
;
; WRITTEN:
;       S.P.Bellamy, Tessella Support Services plc, 20/03/1995
;
; MODIFIED:
;	1.1	S.P.Bellamy
;		First Release
;	1.2	William Osborn
;		Added menu button code
;	1.3	Tim Hammond
;		Corrected reading in of pipe data
;
; VERSION:
;	1.1	20-03-95
;	1.2	04-06-96
;	1.3	06-08-96
;
;-
;-----------------------------------------------------------------------------


PRO b7spf1, pipe, lpend, value, dsnpas, dsninc, srmin, srmax, densa, tea, $
		bitfile, gomenu, DEVLIST=devlist, FONT=font


                ;**** Set defaults for keywords ****
  IF NOT (KEYWORD_SET(font)) THEN font = ''

		;********************************
		;**** Read data from fortran ****
		;********************************
  maxd = 0
  maxt = 0
  rmin = 0.0D
  rmax = 0.0D
  igmax = 0
  icmax = 0
  idum = 0
  ddum = 0.0d0

  readf, pipe, lpend
  lpend = idum
  readf, pipe, idum
  maxd = idum
  readf, pipe, idum
  maxt = idum
  readf, pipe, ddum
  rmin = ddum
  readf, pipe, ddum
  rmax = ddum
  readf, pipe, idum
  igmax = idum
  readf, pipe, idum
  icmax = idum

		;**************************************
		;**** Pop-up output options widget ****
		;**************************************
  adas207_out, value, dsnpas, dsninc, srmin, srmax, $
		maxd, maxt, densa, tea, action, bitfile, $
		DEVLIST=devlist, FONT=font

		;*******************************************
		;**** Act on the output from the widget ****
		;*******************************************
		;**** There are only two possible actions ****
		;**** 'Done' and 'Cancel'.                ****
  if action eq 'Done' then begin
    lpend = 0
  endif else if action eq 'Menu' then begin
    lpend = 1
    gomenu = 1
  endif else begin
    lpend = 1
  endelse

		;*******************************
		;**** Write data to fortran ****
		;*******************************
  printf,pipe,lpend

  if lpend eq 0 then begin

	printf, pipe, value.grpopt

	case value.grpopt of
		1:	begin
				printf, pipe, value.g1ncnt
				printf, pipe, value.grpout
				printf, pipe, value.g1dfcs
				printf, pipe, value.g1cilg
				printf, pipe, value.g1cslg
				printf, pipe, value.g1titl
				if value.g1ncnt gt 0 then printf, pipe, value.g1cont
			end
		2:	begin
				printf, pipe, value.g2nden
				printf, pipe, value.grpout
				printf, pipe, value.g2dfgs
				printf, pipe, value.g2titl
				if value.g2nden gt 0 then printf, pipe, value.g2dens
				if not value.g2dfgs then begin
					printf, pipe, value.g2xmin
					printf, pipe, value.g2xmax
					printf, pipe, value.g2ymin
					printf, pipe, value.g2ymax
				endif
			end
		3:	begin
				printf, pipe, value.g3ntmp
				printf, pipe, value.grpout
				printf, pipe, value.g3dfgs
				printf, pipe, value.g3titl
				if value.g3ntmp gt 0 then printf, pipe, value.g3temp
				if not value.g3dfgs then begin
					printf, pipe, value.g3xmin
					printf, pipe, value.g3xmax
					printf, pipe, value.g3ymin
					printf, pipe, value.g3ymax
				endif
			end
	endcase

	printf, pipe, value.texout

	if value.texout then begin
		printf, pipe, (value.texapp le 0)
		printf, pipe, value.texdsn
	endif

  end

		;**************************************************
		;**** Set messages and settings ready for when ****
		;**** output window is re-displayed.           ****
		;**************************************************

  if lpend eq 0 then begin
    if value.texout eq 1 then begin
		;**** If text output is requested enable append ****
		;**** for next time and update the default to   ****
		;**** the current text file.                    ****
      value.texapp=0
      value.texdef = value.texdsn
      if value.texrep ge 0 then value.texrep = 0
      value.texmes = 'Output written to file.'
    end

    if value.hrdout eq 1 then begin
    end
  end

END
