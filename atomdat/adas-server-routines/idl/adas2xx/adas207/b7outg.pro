; Copyright (c) 1995 Strathclyde University.
; SCCS info: Module @(#)$Header: /home/adascvs/idl/adas2xx/adas207/b7outg.pro,v 1.2 2004/07/06 11:27:56 whitefor Exp $ Date $Date: 2004/07/06 11:27:56 $
;+
; PROJECT:
;       ADAS IBM MVS to UNIX conversion
;
; NAME:
;	B7OUTG
;
; PURPOSE:
;	Communication with ADAS207 FORTRAN via UNIX pipe and
;	graphics output.
;
; EXPLANATION:
;	The routine begins by reading information from the ADAS207
;	FORTRAN process via a UNIX pipe.  Then the IDL graphical
;	output routine for ADAS207 is invoked.  Communications are to
;	the FORTRAN subroutine B7OUTG.
;
; USE:
;	The use of this routine is specific to ADAS207 see adas207.pro.
;
; INPUTS:
;	PIPE	 - The IDL unit number of the bi-directional pipe to the
;		   ADAS207 FORTRAN process.
;
;	HRDOUT	 - Integer; 0 if no hardcopy required, 1 if hardcopy.
;
;	HRDNAM   - String; file name for hardcopy output.
;
;	DEVICE	 - String; IDL name for hardcopy output device.
;
;	HEADER	 - ADAS version header information for inclusion in the
;		   graphical output.
;
;       BITFILE - String; the path to the dirctory containing bitmaps
;                 for the 'escape to series menu' button.
;
; OPTIONAL INPUTS:
;	None.
;
; OUTPUTS:
;       GOMENU  - Int; flag - set to 1 if user has selected 'escape direct
;                 to series menu , otherwise 0
;
; OPTIONAL OUTPUTS:
;	None.
;
; KEYWORD PARAMETERS:
;	FONT	- String; Name of the font to be used for the graphical
;		  output widget.
;
; CALLS:
;	ADAS207_PLOT	ADAS207 graphical output.
;
; SIDE EFFECTS:
;	This routine reads information from FORTRAN via a UNIX pipe.
;
; CATEGORY:
;	Adas system.
;
; WRITTEN:
;       S.P.Bellamy, Tessella Support Services plc, 27/03/1995
;
; MODIFIED:
;	1.1	S.P.Bellamy
;		First Release
;	1.2	William Osborn
;		Added menu button code
;
; VERSION:
;	1.1	20-03-95
;	1.2	04-06-96
;
;-
;-----------------------------------------------------------------------------



PRO b7outg, pipe, hrdout, hrdnam, device, header, bitfile, gomenu, FONT=font


                ;**** Set defaults for keywords ****
  IF NOT (KEYWORD_SET(font)) THEN font = ''


		;**** Declare variables for input ****
  iopt = 0
  date = ''
  ndden = 0
  ndtem = 0
  igmax = 0
  icmax = 0
  iz = 0
  titled = ''
  gtit1 = ''
  dsninc = ''
  isel = 0
  ldef1 = 0
  logint = 0
  xmin = 0.0D
  xmax = 0.0D
  ymin = 0.0D
  ymax = 0.0D
  nmet = 0
  maxd = 0
  maxt = 0
  nstrn1 = 0
  nstrn2 = 0

  inputc = ''
  inputd = 0.0D

		;********************************
		;**** Read data from fortran ****
		;********************************
  readf, pipe, iopt
  readf, pipe, date
  readf, pipe, ndden
  readf, pipe, ndtem
  readf, pipe, igmax
  readf, pipe, icmax
  readf, pipe, iz
  readf, pipe, titled
  readf, pipe, gtit1
  readf, pipe, dsninc
  readf, pipe, isel

  if iopt eq 1 then begin
	contr = dblarr( isel )
	igsel = [0L]
	readf, pipe, contr
  endif else begin
	contr = [0.0D]
	igsel = lonarr( isel )
	readf, pipe, igsel
  endelse

  readf, pipe, ldef1

  if iopt eq 1 then begin
	readf, pipe, logint
  endif else begin
	if ldef1 eq 0 then begin
		readf, pipe, xmin
		readf, pipe, xmax
		readf, pipe, ymin
		readf, pipe, ymax
	endif
  endelse

  readf, pipe, nmet
  readf, pipe, maxd
  readf, pipe, maxt
  readf, pipe, nstrn1
  readf, pipe, nstrn2

  if nmet gt 0 then begin
	xmmult = dblarr( nmet )
	readf, pipe, xmmult
  endif else begin
	xmmult = [0.0D]
  endelse

  if maxd gt 0 then begin
	densa = dblarr( maxd )
	readf, pipe, densa
  endif else begin
	densa = [0.0D]
  endelse

  if maxt gt 0 then begin
	tea = dblarr( maxt )
	readf, pipe, tea
  endif else begin
	tea = [0.0D]
  endelse

  if nstrn1 gt 0 then begin
	cstrn1 = strarr( nstrn1 )
	for i = 0, nstrn1-1 do begin
		readf, pipe, inputc
		cstrn1(i) = inputc
	endfor
  endif else begin
	cstrn1 = ['']
  endelse

  if nstrn2 gt 0 then begin
	cstrn2 = strarr( nstrn2 )
	for i = 0, nstrn2-1 do begin
		readf, pipe, inputc
		cstrn2(i) = inputc
	endfor
  endif else begin
	cstrn2 = ['']
  endelse

  if nmet gt 0 then begin
	strgm = strarr( nmet )
	for i = 0, nmet-1 do begin
		readf, pipe, inputc
		strgm(i) = inputc
	endfor
  endif else begin
	strgm = ['']
  endelse

  rat = dblarr( maxt, maxd )
  for j = 0, maxd-1 do begin
	for i = 0, maxt-1 do begin
		readf, pipe, inputd
		rat( i, j ) = inputd
	endfor
  endfor

		;**************************
		;**** ADAS207 plotting ****
		;**************************
  adas207_plot, iopt, date, iz, titled, gtit1, dsninc, $
		isel, contr, igsel, ldef1, logint, $
		xmin, xmax, ymin, ymax, $
		nmet, maxd, maxt, nstrn1, nstrn2, $
		xmmult, densa, tea, cstrn1, cstrn2, strgm, rat, $
		hrdout,	hrdnam, device, header, bitfile, gomenu,$
		FONT=font


END
