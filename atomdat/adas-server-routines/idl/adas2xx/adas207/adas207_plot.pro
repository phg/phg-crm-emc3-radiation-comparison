; Copyright (c) 1995 Strathclyde University.
; SCCS info: Module @(#)$Header: /home/adascvs/idl/adas2xx/adas207/adas207_plot.pro,v 1.3 2004/07/06 10:18:44 whitefor Exp $ Date $Date: 2004/07/06 10:18:44 $
;+
; PROJECT:
;       ADAS IBM MVS to UNIX conversion
;
; NAME:
;	ADAS207_PLOT
;
; PURPOSE:
;	Generates ADAS207 graphical output.
;
; EXPLANATION:
;	This routine creates a window for the display of graphical
;	output and displays output as the user directs by interaction
;	with the widget.
;
; USE:
;	This routine is specific to ADAS207, see b7outg.pro for
;	example use.
;
; INPUTS:
;	(Most of these inputs map exactly onto varaibles of the same
;	 name in the FORTRAN code.)
;
;	IOPT	- Integer; Graphical output option number.
;
;	DATE	- String; Generation date of output.
;
;	TITLED  - String; Element symbol.
;
;	IZ      - Integer; Recombined ion charge.
;
;	GTIT1   - String; Title for graph.
;
;	DSNINC  - String; Input COPASE data set name.
;
;	ISEL	- Integer; Number of items in output option array.
;
;	CONTR	- Double array; Contour values for option 1.
;
;	IGSEL	- Integer array; Density or Temperature for option 2 or 3.
;
;	LDEF1	- Integer; Default scaling flag.
;
;	LOGINT	- Integer; Logarithmic contour interpolation flag.
;
;	XMIN	- Double; Minimum value for X axis.
;
;	XMAX	- Double; Maximum value for X axis.
;
;	YMIN	- Double; Minimum value for Y axis.
;
;	YMAX	- Double; Maximum value for Y axis.
;
;	NMET    - Integer; Number of metastable levels.
;
;	MAXD    - Integer; Number of input electron densities.
;
;	MAXT    - Integer; Number of input electron temperatures.
;
;	NSTRN1	- Integer; Number of transitions in first assembly.
;
;	NSTRN2	- Integer; Number of transitions in second assembly.
;
;	DENSA	- Double array; Electron densities (units: cm-3).
;
;	TEA	- Double array; Electron temperatures (units: K).
;
;	CSTRN1	- String array; Designations for first assembly.
;
;	CSTRN2	- String array; Designations for second assembly.
;
;	STRGM	- String array; Level designations.
;
;	RAT	- Double array; Spectrum line ratio.
;
;	HRDOUT  - Integer; 1 if hardcopy output activated, 0 if not.
;
;	HRDNAM  - String; Filename for harcopy output.
;
;	DEVICE	- String; IDL name of hardcopy output device.
;
;	HEADER	- String; ADAS version number header to include in graph.
;
;       BITFILE - String; the path to the dirctory containing bitmaps
;                 for the 'escape to series menu' button.
;
; OPTIONAL INPUTS:
;	None.
;
; OUTPUTS:
;       GOMENU  - Int; flag - set to 1 if user has selected 'escape direct
;                 to series menu , otherwise 0
;
; OPTIONAL OUTPUTS:
;	None.
;
; KEYWORD PARAMETERS:
;	FONT	- String; The name of a font to use for text in the
;		  graphical output widget.
;
; CALLS:
;	CW_ADAS_GRAPH	Graphical output widget.
;	PLOT207		Make one plot to an output device for 205/206.
;	XMANAGER
;
; SIDE EFFECTS:
;	This routine uses a common block to maintain its state PLOT207_BLK.
;
;	One other routine is included in this file;
;	ADAS205_PLOT_EV	Called via XMANAGER during widget management.
;
; CATEGORY:
;	Adas system.
;
; WRITTEN:
;       Andrew Bowen, Tessella Support Services plc,  5-May-1993
;
; MODIFIED:
;	1.1	S.P.Bellamy
;		First Release
;	1.2	William Osborn
;		Added menu button and dynlabel procedure
;	1.3	Tim Hammond
;               Added "intelligent" resizing of the graphical output
;               window.
;
; VERSION:
;	1.1	05-05-95
;	1.2	04-06-96
;	1.3	06-08-96
;
;----------------------------------------------------------------------------

PRO adas207_plot_ev, event

  COMMON plot207_blk, data, action, win, plotdev, $
		      plotfile, fileopen, gomenu


  newplot = 0
  print = 0
  done = 0
		;****************************************
		;**** Set graph and device requested ****
		;****************************************
  CASE event.action OF

	'print'	   : begin
			newplot = 1
			print = 1
		end

	'done'	   : begin
			if fileopen eq 1 then begin
			  set_plot,plotdev
			  device,/close_file
			end
			set_plot,'X'
			widget_control,event.top,/destroy
			done = 1
		end
	'bitbutton'   : begin
			if fileopen eq 1 then begin
			  set_plot,plotdev
			  device,/close_file
			end
			set_plot,'X'
			widget_control,event.top,/destroy
			done = 1
			gomenu = 1
		end
  END

		;*******************************
		;**** Make requested plot/s ****
		;*******************************

  if done eq 0 then begin
		;**** Set graphics device ****
    if print eq 1 then begin

      set_plot,plotdev
      if fileopen eq 0 then begin
        fileopen = 1
        device,filename=plotfile
	device,/landscape
      endif

    endif else begin

      set_plot,'X'
      wset,win
  
    endelse

		;**** Draw graphics ****
    if newplot eq 1 then begin
	plot207,	data.iopt, data.date, data.iz, data.titled, data.gtit1, data.dsninc, $
			data.isel, data.contr, data.igsel, data.ldef1, data.logint, $
			data.xmin, data.xmax, data.ymin, data.ymax, $
			data.nmet, data.maxd, data.maxt, data.nstrn1, data.nstrn2, $
			data.xmmult, data.densa, data.tea, $
			data.cstrn1, data.cstrn2, data.strgm, data.rat, data.header

	if print eq 1 then begin
	  message = 'Plot written to print file.'
	  grval = {WIN:0, MESSAGE:message}
	  widget_control,event.id,set_value=grval
	endif

    endif

  endif

END

;----------------------------------------------------------------------------

PRO adas207_plot, iopt, date, iz, titled, gtit1, dsninc, $
		isel, contr, igsel, ldef1, logint, $
		xmin, xmax, ymin, ymax, $
		nmet, maxd, maxt, nstrn1, nstrn2, $
		xmmult, densa, tea, cstrn1, cstrn2, strgm, rat, $
		hrdout, hrdnam, device, header, bitfile, gomenu,$
		FONT=font



  COMMON plot207_blk, data, action, win, plotdev, $
		      plotfile, fileopen, gomenucom

		;**** Copy input values to common ****
  plotdev = device
  plotfile = hrdnam
  fileopen = 0
  gomenucom = gomenu

		;*************************************
		;**** Create graph display widget ****
		;*************************************
  graphid = widget_base( TITLE='ADAS207 GRAPHICAL OUTPUT', $
					XOFFSET=1, YOFFSET=1 )
  device, get_screen_size=scrsz
  xwidth = scrsz(0)*0.75
  yheight = scrsz(1)*0.75
  bitval = bitfile + '/menu.bmp'
  cwid = cw_adas_graph( graphid, print=hrdout, FONT=font, 		$
                        xsize=xwidth, ysize=yheight, bitbutton=bitval)

                ;**** Realize the new widget ****
  dynlabel, graphid
  widget_control, graphid, /realize

		;**** Get the id of the graphics area ****
  widget_control, cwid, get_value=grval
  win = grval.win

		;*******************************************
		;**** Put the graphing data into common ****
		;*******************************************
  data = {	IOPT:iopt,	DATE:date,	IZ:iz, $
		TITLED:titled,	GTIT1:gtit1,	DSNINC:dsninc, $
		ISEL:isel,	CONTR:contr,	IGSEL:igsel, $
		LDEF1:ldef1,	LOGINT:logint, $
		XMIN:xmin,	XMAX:xmax, $
		YMIN:ymin,	YMAX:ymax, $
		NMET:nmet,	MAXD:maxd,	MAXT:maxt, $
		NSTRN1:nstrn1,	NSTRN2:nstrn2, $
		XMMULT:xmmult,	DENSA:densa,	TEA:tea, $
		CSTRN1:cstrn1,	CSTRN2:cstrn2,	STRGM:strgm, $
		RAT:rat, $
		HEADER:header }

		;**** Initialise plot ****
  wset,win
  plot207,	data.iopt, data.date, data.iz, data.titled, data.gtit1, $
		data.dsninc, $
		data.isel, data.contr, data.igsel, data.ldef1, data.logint, $
		data.xmin, data.xmax, data.ymin, data.ymax, $
		data.nmet, data.maxd, data.maxt, data.nstrn1, data.nstrn2, $
		data.xmmult, data.densa, data.tea, $
		data.cstrn1, data.cstrn2, data.strgm, data.rat, data.header


		;***************************
                ;**** make widget modal ****
		;***************************
  xmanager,'adas207_plot',graphid,event_handler='adas207_plot_ev', $
                                        /modal,/just_reg

  gomenu = gomenucom

END
