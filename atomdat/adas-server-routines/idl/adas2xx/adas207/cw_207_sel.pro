; Copyright (c) 1995 Strathclyde University .
; SCCS info: Module @(#)$Header: /home/adascvs/idl/adas2xx/adas207/cw_207_sel.pro,v 1.4 2004/07/06 12:24:47 whitefor Exp $ Date $Date: 2004/07/06 12:24:47 $                    
;
;+
; PROJECT:
;       ADAS IBM MVS to DEC UNIX conversion
;
; NAME:
;	CW_207_SEL()
;
; PURPOSE:
;	Widget for viewing current selections and invoking a selection
;	list window.
;       This particular version of the widget has been altered specifically
;       for use with the adas207 processing options screen.
;
; EXPLANATION:
;	This compound widget provides a single entry point to the ADAS
;	multiple selection list features.  The widget consists of a
;	title, a text widget showing the items currently selected and
;	a button which can be used to invoke the multiple selection list
;	in order to modify the selections.
;
;	This widget generates an event whenever the 'Selections' button
;	has been pressed and a new set of selections completed.  The
;	event structure is;
;
;	{ID:0L, TOP:0L, HANDLER:0L, ACTION:''}
;
;	Where ACTION will have the value 'multisel'.
;
; USE:
;	The following code adds a selection list widget to a base.  The
;	list of selected items includes 'Red' and 'Blue' at realization.
;	The maximum number of selections allowed is set at 3 since that
;	is the length of the 'selected' array.  Press the 'Selections'
;	button to modify the selections.
;
;	    list = ['Red','Green','Blue','Black','Brown','Grey']
;	    selected = [0,2,-1]
;	    title = 'Colour Selections'
;	    base = widget_base(/column)
;	    selid = cw_207_sel(base,list,selected=selected,title=title)
;	    widget_control,base,/realize
;	    rc=widget_event()
;	    widget_control,selid,get_value=selected
;	    print,'Colours Selected are',list(selected(where(selected ge 0)))
;
; INPUTS:
;       PARENT   - The ID of the parent widget.
;
;	WIDGET_ID	 - Widget ID of the widget that has the transition list stored
;			    as a user value.
;
; OPTIONAL INPUTS:
;	None.
;
; OUTPUTS:
;       The return value of this function is the ID of the compound
;       widget which is of type LONG integer.
;
;		struct = { CWID: cwid, CURSELID: curselid }
;
;		CWID:	ID of the compound widget (long).
;	  CURSELID: ID of the text widget that displays current selections (long).
;
; OPTIONAL OUTPUTS:
;       None
;
; KEYWORD PARAMETERS:
;       SELECTED - One dimensional integer array of the indices of the
;                  items in LIST which are to be set as selected when
;                  first displayed.  The length of SELECTED is the
;                  maximum number of selections allowed.  Extra spaces
;                  can be reserved in SELECTED with values of -1.  All
;                  -1 values should be at the end of the array.
;                  For example SELECTED=[0,5,-1,-1] will give a maximum
;                  of 4 selections and items 0 and 5 from list are
;                  selected on start up.  The default is no selections
;                  set on startup.
;
;	  FLAG     - Allows distinction to be made between 1st compostie assembly
;			 and second composite assembly.
;
;       MAXSEL   - Maximum number of selections allowed.  The default is
;                  one unless the SELECTED keyword is specified in which
;                  case the value of MAXSEL is ignored.
;
;	ORDERED  - When the ordered keyword is non-zero the list of 
;		   selections in the SELECTED array is arranged in order
;		   of ascending value regardless of the order in which 
;		   the list items were chosen.  The -1 values used to
;		   pad the selected array still remain at the end.
;		   For example if the ordered keyword is not set the
;		   values in selected will match the order in which the
;		   items were selected from LIST e.g [3,1,5,-1,-1].
;		   With the ordered keyword non-zero the SELECTED array
;		   would be [1,3,5,-1,-1].
;
;       TITLE    - Title to appear on the selection list window banner.
;                  The default is 'Make your Selections'.
;
;       FONT     - The text font to be used for the window.  The default
;                  is the current X default.
;
;	UVALUE	 - User value for the compound widget.
;
; CALLS:
;       CW_LOADSTATE    Recover compound widget state.
;       CW_SAVESTATE    Save compound widget state.
;	MULTI_207	Pop-up multiple selection list window.
;       See side effects for other related widget management routines.
;
; SIDE EFFECTS:
;	This widget uses a COMMON BLOCK: CW_ADASSEL207_BLK to hold the
;       widget state.
;
;       Three other routines are included which are used to manage the
;       widget;
;
;	ADASSEL207_SET_VAL
;	ADASSEL207_GET_VAL()
;	ADASSEL207_EVENT()
;
; CATEGORY:
;	Compound Widget?
;
; WRITTEN:
;       Andrew Bowen, Tessella Support Services plc, 5-Apr-1993
;
; MODIFIED:
;       Version 1       Andrew Bowen    1-Jun-1993
;                       First release.
;	Version 1.1	Tim Hammond 	20-Apr-1995
;			Altered for use with adas207       
;       Version 1.2     Tim Hammond     20-Apr-1995
;                       Altered common block names.
;	 1.3	Richard Martin
;	 	Transition list obtained from widget user value, rather than being
;		passed in. This is so as to be consistent with introduction of 
;		wavelength and emissivity bounds introduced in cw_adas207_proc.pro.
;		Structure returned, rather than just CWID, to allow current selections
;		to be updated outwith cw_207_sel.
;	1.4	Richard Martin
;		Removed obsolete cw_loadstate/savestate statements.
;
; VERSION:
;       1.1       20-Apr-1995
;       1.2       20-Apr-1995
;	  1.3		14-11-01
;	  1.4		10-01-02
;-
;-----------------------------------------------------------------------------

PRO adassel207_set_val, id, selected


		;**** Return to caller on error ****
  ON_ERROR, 2

		;**** Retrieve the state ****

  first_child = widget_info(id,/child)
  widget_control, first_child, get_uvalue=state, /no_copy

		;**** The current selections ****

  widget_control, state.widgetid, get_uvalue=selstruct, /no_copy
  list = selstruct.trantab
  widget_control, state.widgetid, set_uvalue=selstruct, /no_copy
  
  selsind = where(selected gt -1)
  if selsind(0) ge 0 then currsels = list(selected(selsind)) $
							else currsels = ''
  widget_control,state.curselid,set_value=currsels

		;**** Copy the new value to state structure ****

  state.selected = selected

  widget_control, first_child, set_uvalue=state, /no_copy

END

;-----------------------------------------------------------------------------


FUNCTION adassel207_get_val, id


		;**** Return to caller on error ****
  ON_ERROR, 2

		;**** Retrieve the state ****

  first_child = widget_info(id,/child)
  widget_control, first_child, get_uvalue=state

  RETURN, state.selected

END

;-----------------------------------------------------------------------------

FUNCTION adassel207_event, event


		;**** Base ID of compound widget ****
  base=event.handler

		;**** Retrieve the state ****

  first_child = widget_info(base, /child)
  widget_control, first_child, get_uvalue = state, /no_copy

		;************************
		;**** Process Events ****
		;************************
  CASE event.id OF

    state.selectid: begin
	widget_control, state.widgetid, get_uvalue=selstruct,/no_copy
	list=selstruct.trantab
	if state.flag eq 1 then selected=selstruct.selected1 else selected=selstruct.secondsel
	
      action = 'multisel'
      selected = multi_207(list, SELECTED=selected, $
				MAXSEL=state.maxsel, ORDERED=state.ordered, $
				TITLE=state.title, FONT=state.font)

		;**** The current selections ****
		
      selsind = where(selected gt -1)
      if selsind(0) ge 0 then currsels = list(selected(selsind)) $
							 else currsels = ''
      widget_control,state.curselid,set_value=currsels

		;**** Copy the new value to state structure ****

	if state.flag eq 1 then begin
		selstruct.selected1 = selected 
	endif else begin
		selstruct.secondsel = selected
	endelse 
	
	widget_control, state.widgetid, set_uvalue=selstruct, /no_copy	
	
      state.selected = selected
	
      widget_control, first_child, set_uvalue=state, /no_copy

    end

  ENDCASE

  RETURN, {ID:base, TOP:event.top, HANDLER:0L, ACTION:action}
END

;-----------------------------------------------------------------------------

FUNCTION cw_207_sel, topparent, widget_id, SELECTED=selected, FLAG=flag, $
         MAXSEL=maxsel, ORDERED=ordered, TITLE=title, FONT=font, UVALUE=uvalue


  IF (N_PARAMS() LT 2) THEN MESSAGE, 'Must specify PARENT and LIST'+ $
						' for cw_207_sel'

                ;**** Set defaults for keywords ****
  IF NOT (KEYWORD_SET(maxsel)) THEN maxsel = 1
  IF NOT (KEYWORD_SET(selected)) THEN $
                        selected = make_array(maxsel,/int,value=-1)
  IF NOT (KEYWORD_SET(flag)) THEN flag = 1				
  IF NOT (KEYWORD_SET(ordered)) THEN ordered = 0
  IF NOT (KEYWORD_SET(title)) THEN title = ' '
  IF NOT (KEYWORD_SET(font)) THEN font = ''
  IF NOT (KEYWORD_SET(uvalue))  THEN uvalue = 0

                ;**** Over-ride MAXSEL with the length of SELECTED ****
                ;**** this is in case they are different.          ****
  IF (KEYWORD_SET(selected)) THEN begin
    seldim = size(selected)
    maxsel = seldim(1)
  end

		;**** Create the main base for the widget ****
  parent = WIDGET_BASE(topparent, UVALUE = uvalue, $
		EVENT_FUNC = "adassel207_event", $
		FUNC_GET_VALUE = "adassel207_get_val", $
		PRO_SET_VALUE = "adassel207_set_val", $
		/COLUMN)

		;**** Create base to hold the value of state ****

  first_child = widget_base(parent)

  cwid = widget_base(first_child,/column)

  ;**** get transition list from widget ****
  
  widget_control, widget_id, get_uvalue=selstruct, /no_copy
  
  list = selstruct.trantab

  widget_control, widget_id, set_uvalue=selstruct, /no_copy

		;**** Title ****
  rc = widget_label(cwid,value=title,font=font)

		;**** The current selections ****
  selsind = where(selected gt -1)
  if selsind(0) ge 0 then currsels = list(selected(selsind)) $
							else currsels = ''
  maxlen = max(strlen(list))
  curselid = widget_text(cwid,value=currsels,XSIZE=maxlen,YSIZE=maxsel, $
								font=font)

		;**** The edit button ****
  selectid = widget_button(cwid,value='Selections',font=font)

		;**** Create state structure ****
  new_state =  {CURSELID:curselid, SELECTID:selectid, $
		SELECTED:selected, FLAG:flag, MAXSEL:maxsel, ORDERED:ordered, $
		WIDGETID:widget_id, TITLE:title, FONT:font}

		;**** Save initial state structure ****

  widget_control, first_child, set_uvalue=new_state, /no_copy

  base = WIDGET_INFO(cwid, /CHILD)
  struct = { CWID: parent, curselid: curselid }

  RETURN, struct

END
