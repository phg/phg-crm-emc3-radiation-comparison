; Copyright (c) 1995 Strathclyde University 
; SCCS info: Module @(#)$Header: /home/adascvs/idl/adas2xx/adas207/multi_207.pro,v 1.4 2004/07/06 14:22:23 whitefor Exp $ Date $Date: 2004/07/06 14:22:23 $
;
;+
; PROJECT:
;	ADAS IBM MVS to DEC UNIX conversion.
;
; NAME:
;	MULTI_207()
;
; PURPOSE:
;	Produces a 'Pop-up' window with a multiple selection list.
;       This version of the widget has been altered specifically for use
;       with the adas207 processing options screen.
;
; EXPLANATION:
;	Produces an interactive multiple selection list in a 'Pop-up'
;	window.  The multiple-selection list consists of one toggle
;	button for each item in a given list (this is the compound
;	widget CW_MULTI_207) plus 'Cancel' and 'Done' buttons.
;	The caller may specify a title for the window.
;
;	The LIST is provided as	a one dimensional string array of
;	items for selection.  The function returns the selected items
;	to the caller in the form of an integer array giving the indices
;	of the items selected from the LIST.  The caller may specify the
;	maximum number of selections allowed from the list.  The user
;	can press the 'Done' button to confirm the new selections or
;	'Cancel' to retain the original selections if any.  The caller
;	may specify that items are marked as selected on startup using
;	the SELECTED keyword.
;
; USE:
;	The following invokes a pop-up window with a multiple selection
;	list of a number of colours.  The user is allowed to make a
;	maximum of three selections from the list.  The function returns
;	an integer array which gives the indices of the elements from
;	the LIST which have been selected.  If fewer than the maximum
;	number of selections are made the array is filled out with
;	-1 values.  When the user activates the 'Done' button the
;	selected items are printed to the screen in the order in which
;	they were selected.
;
;	list=['Black','Blue','Pink','Green','Purple','Red','Orange']
;	sels = MULTI_207(list,MAXSEL=3,TITLE='Select Colours')
;	print,list(sels(where(sels gt -1)))
;
;	The following code reactivates the selection list window with
;	the same set of selections already made on startup.  Because
;	sels has three elements the maximum number of allowed selections
;	is automatically set to three.
;
;	sels = MULTI_207(list,SELECTED=sels,TITLE='Select Colours')
;
; INPUTS:
;	LIST     - One dimensional string array of items for the
;		   selection list.
;
; OPTIONAL INPUTS:
;	None.  See the keywords for additional controls.
;
; OUTPUTS:
;	The return value of this function is a one dimensional array
;	of indices of the items selected from LIST (given in the same
;	form as the value for the SELECTED keyword).
;
; OPTIONAL OUTPUTS:
;	None.
;
; KEYWORDS:
;       SELECTED - One dimensional integer array of the indices of the
;                  items in LIST which are to be set as selected when
;		   first displayed.  The length of SELECTED is the
;                  maximum number of selections allowed.  Extra spaces
;                  can be reserved in SELECTED with values of -1.  All
;		   -1 values should be at the end of the array.
;                  For example SELECTED=[0,5,-1,-1] will give a maximum
;		   of 4 selections and items 0 and 5 from list are
;		   selected on start up.  The default is no selections
;		   set on startup.
;
;       MAXSEL   - Maximum number of selections allowed.  The default is
;                  one unless the SELECTED keyword is specified in which
;                  case the value of MAXSEL is ignored.
;
;	ORDERED  - When the ordered keyword is non-zero the list of 
;		   selections in the SELECTED array is arranged in order
;		   of ascending value regardless of the order in which 
;		   the list items were chosen.  The -1 values used to
;		   pad the selected array still remain at the end.
;		   For example if the ordered keyword is not set the
;		   values in selected will match the order in which the
;		   items were selected from LIST e.g [3,1,5,-1,-1].
;		   With the ordered keyword non-zero the SELECTED array
;		   would be [1,3,5,-1,-1].
;
;	TITLE	 - Title to appear on the selection list window banner.
;		   The default is 'Make your Selections'.
;
;	INSTR	 - Instructions on use of widget - default is blank.
;
;	FONT	 - The text font to be used for the window.  The default
;		   is the current X default.
;
; CALLS:
;	CW_MULTI_207()
;	XMANAGER
;
;	See side effects for other routines indirectly called.
;
; SIDE EFFECTS:
;	A COMMON BLOCK: MSEL_COM is used which is private to MULTI_207()
;	and MULTI_207_EVENT.
;
;	This routine uses XMANAGER to manage the multiple selection
;	window and hence via event generation and use of the
;	WIDGET_CONTROL procedure the following routines and functions are
;	indirectly called;
;
;	MULTI_207_EVENT		(included in this file)
;	MULSEL_SET_VAL		(see CW_MULTI_207.PRO)
;	MULSEL_GET_VAL()	(see CW_MULTI_207.PRO)
;	MULSEL_EVENT()		(see CW_MULTI_207.PRO)
;
;	The selection list widget uses a private common block: CW_MULSEL_BLK
;
; CATEGORY:
;	Widgets.
;
; WRITTEN:
;       Andrew Bowen, Tessella Support Services plc, 4-Mar-1993
;
; MODIFIED:
;       Version 1       Andrew Bowen    28-May-1993
;                       First release.
;	Version 1.1	Tim Hammond 	20-Apr-1995
;                       Altered for use with adas207
;       Version 1.2     Tim Hammond     20-Apr-1995
;                       Altered names of common blocks.
;       Version 1.3     Martin O'Mullane
;                       Add new INSTR keyword and changed multisel
;                       calling parameters for new style multipanel
;                       selection method.
;       Version 1.4     Martin Torney   13-Apr-2004
;                       Fixed bug where the index of the selected transition
;                       would decrease by 1 and the first transition would
;                       also be selected whenever the transition selection
;                       widget was opened.
;
; VERSION:
;       1.1       20-Apr-1995
;       1.2       20-Apr-1995
;       1.3       05-05-2003
;       1.4       13-04-2004
;-
;-----------------------------------------------------------------------------

PRO multi_207_event, event

  COMMON msel207_com, selid, sels

		;**** see which button generated the event ****
  widget_control,event.id,get_value=button

		;**** copy selections to common for 'Done' ****
  if button eq 'Done' then widget_control,selid,get_value=sels

		;**** for either button end the list window ****
  widget_control,event.top,/destroy

END


;-----------------------------------------------------------------------------



FUNCTION multi_207, list, SELECTED=selected, $
	                  MAXSEL=maxsel,     $
                          ORDERED=ordered,   $
                          TITLE=title,       $
                          INSTR=instr,       $
                          FONT=font 

		;**** private common, id of selection widget and ****
		;**** the list of selections.                    ****
  COMMON msel207_com, selid, sels

		;**** Set defaults for keywords ****
  IF NOT (KEYWORD_SET(ordered)) THEN ordered = 0
  IF NOT (KEYWORD_SET(title)) THEN  title = 'Make Your Selections'
  IF NOT (KEYWORD_SET(INSTR)) THEN  instr = ''
  IF NOT (KEYWORD_SET(font))  THEN font = ''

		;**** create titled base widget ****
  base207 = widget_base(title=title,XOFFSET=50,YOFFSET=50,/column)

		;**** add the multi_207 compound widget ****
  
;   selid = cw_multi_207(base207,list,selected=selected,maxsel=maxsel, $
; 					ordered=ordered,font=font)


; New stype step-through panels

  nlist  = n_elements(list)
  selval = intarr(nlist)
  ind    = where(selected NE -1, cnt)
  if cnt GT 0 then selval[selected(ind)] = 1
  value  = {cfg : list, sel : selval}
  selid = cw_multi_207(base207, value=value, maxsel=maxsel, $
                       ordered=ordered, font=font, title=instr)



		;**** copy inital selections to common ****
  widget_control,selid,get_value=sels
                
                ;**** informational message ****
  
  str    = 'Make a maximum of ' + string(maxsel,format='(i3)') + 'selections'
  messid = widget_label(base207, font=font, value=str)

		;**** add the exit buttons ****
  butbase = widget_base(base207,/row)
  rc = widget_button(butbase,value='Cancel',font=font)
  rc = widget_button(butbase,value='Done',font=font)
  
  widget_control,base207,/realize

		;**** make widget modal and wait until destroyed ****
  xmanager,'multi_207',base207,event_handler='multi_207_event',/modal,/just_reg
		;**** Is this a fudge? Will it work with future   ****
		;**** versions of xmanager? Using the MODAL and   ****
		;**** JUST_REG keywords has the effect of making  ****
		;**** xmanager wait until the widget is destroyed ****
		;**** and then returning from this call.          ****
 
		;**** return sels to the caller ****
  RETURN, sels

END
