; Copyright (c) 1995 Strathclyde University.
; SCCS info: Module @(#)$Header: /home/adascvs/idl/adas2xx/adas207/adas207_out.pro,v 1.2 2004/07/06 10:18:36 whitefor Exp $ Date $Date: 2004/07/06 10:18:36 $
;
;+
; PROJECT:
;       ADAS IBM MVS to UNIX conversion
;
; NAME:
;	ADAS207_OUT
;
; PURPOSE:
;	IDL ADAS user interface, output options.
;
; EXPLANATION:
;	This routine creates and manages a pop-up window which allows
;	the user to select options and input data to control ADAS207
;	graphical and file output.
;
; USE:
;	This routine is ADAS207 specific, see b7spf1.pro for how it
;	is used.
;
; INPUTS:
;	VAL	- A structure which determines the initial settings of
;		  the output options widget.  The value is passed
;		  unmodified into cw_adas207_out.pro.
;
;		  See cw_adas207_out.pro for a full description of this
;		  structure.
;
;	DSNPAS	- String; The full system file name of the input Contour
;		  Passing file selected by the user for processing.
;
;	DSNINC	- String; The full system file name of the input COPASE
;		  dataset selected by the user for processing.
;
;	SRMIN	- Minimum spectrum line intensity ratio
;
;	SRMAX	- Maximum spectrum line intensity ratio
;
;	MAXD	- Maximum density index
;
;	MAXT	- Maximum temperature index
;
;	DENSA	- Electron Densities
;
;	TEA	- Electron Temperatures
;
;       BITFILE - String; the path to the dirctory containing bitmaps
;                 for the 'escape to series menu' button.
;
; OPTIONAL INPUTS:
;	None.
;
; OUTPUTS:
;	ACT	- String; Either 'Done', 'Cancel' or 'Menu' for the button the
;		  user pressed to terminate the output options window.
;
; OPTIONAL OUTPUTS:
;	None.
;
; KEYWORD PARAMETERS:
;	DEVLIST	- A string array;  Each element of the array is the
;		  name of a hardcopy graphics output device.  This
;		  list must mirror the DEVCODE list.  DEVLIST is the
;		  list which the user will see.
;		  e.g ['Post-Script','HP-PCL','HP-GL']
;
;	FONT	- Supplies the font to be used for the interface widgets.
;
; CALLS:
;	CW_ADAS207_OUT	Creates the output options widget.
;	XMANAGER
;	See side effects for widget management routine.
;
; SIDE EFFECTS:
;	This routine uses a common block OUT207_BLK to maintain its state.
;	ADAS207_OUT_EV	is included in this file and is called
;			indirectly from XMANAGER during widget
;			management.
;
; CATEGORY:
;	Widgets
;
; WRITTEN:
;       S.P.Bellamy, Tessella Support Services plc, 20/03/1995
;
; MODIFIED:
;	1.1	S.P.Bellamy
;		First Release
;	1.2	William Osborn
;		Added menu button code and dynlabel procedure
;
; VERSION:
;	1.1	20-03-95
;	1.2	04-06-96
;
;-

;-----------------------------------------------------------------------------


PRO adas207_out_ev, event

  COMMON out207_blk,action,value

		;**** Find the event type and copy to common ****
    action = event.action

    CASE action OF

		;**** 'Done' button ****
	'Done'  : begin

			;**** Get the output widget value ****
		widget_control,event.id,get_value=value 

			;**** Kill the widget to allow IDL to ****
			;**** continue and interface with     ****
			;**** FORTRAN only if there is work   ****
			;**** for the FORTRAN to do.          ****
		if (value.grpout eq 1) or $
		   (value.texout eq 1) then widget_control,event.top,/destroy

	   end


		;**** 'Cancel' button ****
	'Cancel': widget_control,event.top,/destroy

		;**** 'Menu' button ****
	'Menu': widget_control,event.top,/destroy

    END

END

;-----------------------------------------------------------------------------


PRO adas207_out, val, dsnpas, dsninc, srmin, srmax, $
		maxd, maxt, densa, tea, $
		act, bitfile, DEVLIST=devlist, FONT=font

  COMMON out207_blk,action,value

		;**** Copy value to common ****
  value = val

		;**** Set defaults for keywords ****
  IF NOT (KEYWORD_SET(font)) THEN font = ''
  IF NOT (KEYWORD_SET(devlist)) THEN devlist = ''

		;***************************************
		;**** Pop-up a new widget if needed ****
		;***************************************

                ;**** create base widget ****
  outid = widget_base(TITLE='ADAS207 OUTPUT OPTIONS',XOFFSET=50,YOFFSET=0)

		;**** Declare output options widget ****
  cwid = cw_adas207_out(outid, dsnpas, dsninc, srmin, srmax, $
			maxd, maxt, densa, tea, bitfile, $
			VALUE=value, DEVLIST=devlist, FONT=font )

		;**** Realize the new widget ****
  dynlabel, outid
  widget_control,outid,/realize

		;**** make widget modal ****
  xmanager,'adas207_out',outid,event_handler='adas207_out_ev',/modal,/just_reg
 
		;**** Return the output value from common ****
  act = action
  val = value

END

