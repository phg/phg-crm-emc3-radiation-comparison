; Copyright (c) 1995 Strathclyde University .
; SCCS info: Module @(#)$Header: /home/adascvs/idl/adas2xx/adas207/cw_adas207_proc.pro,v 1.10 2004/07/06 12:32:48 whitefor Exp $ Date $Date: 2004/07/06 12:32:48 $
;+
; PROJECT:
;       ADAS IBM MVS to UNIX conversion
;
; NAME:
;	CW_ADAS207_PROC()
;
; PURPOSE:
;	Produces a widget for ADAS207 processing options/input.
;
; EXPLANATION:
;	This function creates a compound widget consisting of a label
;	widget holding contour pass file name, the dataset name/browse
;	widget cw_adas_dsbr, label widgets with charge information for
;	the input data, a single selection list widget cw_single_sel
;	to toggle the Equilibrium/Non-Equilibrium flag, a set of label and
;	text widgets for supplying Metastable Level Scalings for the
;	Non-Equilibrium case, two multiple selection widgets cw_207_sel
;	to set the first and second composite assemblies,
;	a message widget, a 'Cancel' button and finally a 'Done' button.
;
;	The compound widgets included in this widget are self managing.
;	This widget only manages events from the two 'Defaults' buttons,
;	the 'Cancel' button and the 'Done' button.
;
;	This widget only generates events for the 'Done' and 'Cancel'
;	buttons.
;
; USE:
;	This widget is specific to ADAS207, see adas207_proc.pro
;	for use.
;
; INPUTS:
;	TOPPARENT- Long integer, ID of parent widget.
;
;	TITLED	- String, Ion name.
;
;	SZ	- String, Recombined ion charge.
;
;	SZ0	- String, Nuclear charge.
;
;	SIONPT	- String, Ionisation potential.
;
;	DSNPAS	- String, The full system file name of the input
;			Contour Passing file.
;	DSNINC	- String, The full system file name of the input COPASE
;			dataset selected by the user for processing.
;
;	NMET	- Integer, Number of metastables.
;
;	MAXD	- Integer, Unused - would be size of RATHA and RATIA arrays.
;
;	ICNTE	- Integer, Number of Electron Impact Transitions.
;
;	IL	- Integer, Number of energy levels.
;
;	NDMET	- Integer, Maximum number of metastables allowed.
;
;	ILEV	- String array, Metastable level indices.
;
;	MSTRGA	- String array, Metastable level designations.
;
;	IE1A	- Integer array, Lower level index of transition
;
;	IE2A	- Integer array, Upper level index of transition
;
;	MTARGA	- String array, Metastable marker
;
;	STRGA	- String array, Level designations
;
;	WATRAN- Float array. Transition energies (cm-1)
;
;	STCKM - Metastables population stack at median temperature and density.
;	
;	STACK - Population dependence at median temperature and density.
;
;	AVAL  - Electron impact transition: A-value (sec-1)
;
;	The inputs TITLED to STRGA map onto variables of similar
;	name in the ADAS207 FORTRAN program.
;
;	DENSA	- Float array, electron densities (cm-3)
;
;       BITFILE - String; the path to the dirctory containing bitmaps
;                 for the 'escape to series menu' button.
;
; OPTIONAL INPUTS:
;	None
;
; OUTPUTS:
;	The return value of this function is the ID of the declared widget.
;
; OPTIONAL OUTPUTS:
;	None
;
; KEYWORD PARAMETERS:
;	VALUE	- A structure which determines the initial settings of
;		  the processing options widget.
;
;		  The default VALUE is created thus;
;
;			mult = make_array( nmet, /double, value=1.0D )
;			ps = {	LEQUIL:1, 			$
;				XXMULT:mult, 			$
;				NSTRN1:1, 				$
;				ISTRN1:[1], 			$
;				NSTRN2:0, 				$
;				ISTRN2:[0], 			$
;				LINDEX:[20],			$
;				LRATHA:0,				$
;				LRATIA:0,				$
;				RATHA :strarr(maxd),		$
;				RATIA :strarr(maxd),		$
;				WVUPPER:0.0,			$
;				WVLOWER:0.0,			$
;				EMISSVAL:0.0				}
;
; 		LEQUIL	Equilibrium/Non-equilibrium Toggle
; 		XXMULT	Scalings for metastables
;		NSTRN1	Number of transitions in first assembly
; 		(ISTRN1(I),I=1,NSTRN1)	index of transitions in subset array
;		NSTRN2	Number of transitions in second assembly
; 		(ISTRN2(I),I=1,NSTRN2)	index of transitions in subset array
;		LINDEX  Stored index of transitions from full transition list.
;		LRATHA  Flag - shows whether or not the input files used
;			contain any charge exchange recombinations.
;		LRATIA  Fkag - shows whether or not the input files used
;                       contain any free electron recombinations.
;		RATHA	Fltarr - ratios (neutral H density/electron density)
;		RATIA   Fltarr - ratios (N(Z+1)/N(Z) stage abundancies)
;		[Note that the values of RATHA and RATIA are only
;		used if the corresponding flag is set to one].
;		WVUPPER	upper wavelength bound.
;		WVLOWER	lower wavelength bound.
;		EMISSVAL	lower emissivity bound.
;
;		All of these structure elements (except lratia and lratha
;		map onto variables of the same name in the ADAS207 FORTRAN 
;		program.
;
;
;	UVALUE	- A user value for the widget. Default 0.
;
;	FONT_LARGE - The name of a larger font.  Default current system
;		     font
;
;	FONT_SMALL - The name of a smaller font. Default current system
;		     font.
;
;	EDIT_FONTS - A structure of two fonts used in the table editor
;		     adas_edtab.pro, {font_norm:'',font_input:''}.  Defaults
;		     to current system font.
;
;	NUM_FORM   - String; Numeric format to use in tables.  Default
;			'(E10.3)'
;		     
;
; CALLS:
;	CW_ADAS_DSBR	Dataset name and comments browsing button.
;	CW_207_SEL	Adas mulitple selection widget.
;	CW_SINGLE_SEL	Adas single selection widget.
;
;	See side effects for other related widget management routines.
;
; SIDE EFFECTS:
;	The following widget management routines are included in this
;	file;
;	PROC207_GET_VAL()	Returns the current VALUE structure.
;	PROC207_EVENT()		Process and issue events.
;
; CATEGORY:
;	Compound Widget
;
; WRITTEN:
;       S.P.Bellamy, Tessella Support Services plc, 20/03/1995
;
; MODIFIED:
;	1.1	Lalit Jalota, Tessella Support Services plc
;		First version
;	1.2	Tim Hammond, Tessella Support Services plc
;       	Changed names of selection routines.
;	1.3 	Tim Hammond
;       	Minor name changes for variables.
;	1.4 	Tim Hammond
;		Added pause before returning to allow time for
;	      	widgets to be sorted out before being realized
;	      	(this seemed to be a problem on certain machine).
;	1.5	Tim Hammond
;		Added extra functionality for possible ratio values
;		(neutral H density/electron density - RATHA ,
;		 N(Z+1)/N(Z) stage abundancies      - RATIA)
;	1.6	Tim Hammond
;		Changed label from N(ION)/N(1) to N+/N(1) in stage
;		abundancies table.
;	1.7	William Osborn
;		Added menu button
;	1.8	Tim Hammond
;		Added /row keyword to cw_adas_dsbr to shrink screen.
;	1.9	Richard Martin
;		Added wavelength selection range and emissivity lower bound to
;		reduce the number of transitions displayed.
;	1.10	Martin O'Mullane
;		Trap failure where stored wavelength limits give 
;               rise to no permitted lines in a new contour file. 
;
; VERSION:
;	1.1	30/03/95
;       1.2  	20/04/95
;       1.3 	20/04/95
;	1.4	24/05/95
;	1.5	13/02/96
;	1.6	16-02-96
;	1.7	04-06-96
;	1.8	08-08-96
;	1.9	14-11-01
;	1.10	06-05-2003
;
;-
;-----------------------------------------------------------------------------

FUNCTION proc207_get_val, id

                ;**** Return to caller on error ****

    ON_ERROR, 2

                ;**** Retrieve the state ****

    first_child = widget_info( id, /child )
    widget_control, first_child, get_uvalue=state, /no_copy

		;********************************************
		;**** Get Equilibrium Toggle from widget ****
		;********************************************

    widget_control, state.equilid, get_value=lequil

		;*****************************************************
		;**** Get Number of Metastable Levels from widget ****
		;*****************************************************

    widget_control, state.xmultid, get_uvalue=nmet

		;*********************************************
		;**** Get Metastable Scalings from widget ****
		;*********************************************

    mult = dblarr( nmet )
    for i = 0, nmet-1 do begin
	widget_control, state.scaleid(i), get_value=scale
	scale = scale(0)
	if num_chk( scale ) then begin
	    mult = i + 1
	    goto, ENDMET
	endif else begin
	    mult(i) = double( scale )
	endelse
    endfor

ENDMET:

		;**************************************************
		;**** Get First Composite Assembly from widget ****
		;**************************************************


    widget_control, state.second_child, get_uvalue=selstruct
    vcmpa=selstruct.selected1
    scmpa = where( vcmpa gt -1 )
    if scmpa(0) gt -1 then begin
	nstrn1 = size( scmpa )
        nstrn1 = fix( nstrn1(1) )
	istrn1 = vcmpa( scmpa ) + 1
    endif else begin
	nstrn1 = 0
	istrn1 = [0]
    endelse

		;***************************************************
		;**** Get Second Composite Assembly from widget ****
		;***************************************************

    vcmpa = selstruct.secondsel
    scmpa = where( vcmpa gt -1 )
    if scmpa(0) gt -1 then begin
	nstrn2 = size( scmpa )
        nstrn2 = nstrn2(1)
	istrn2 = vcmpa( scmpa ) + 1
    endif else begin
	nstrn2 = 0
	istrn2 = [0]
    endelse

		;***************************************
		;**** Get ratio values (if present) ****
		;***************************************

    if (state.lratha eq 1) or (state.lratia eq 1) then begin
	widget_control, state.ratid, get_value=ratval
	if (state.lratha eq 1) then begin
	    if (state.lratia eq 1) then begin		;Both ratios present
		ratha = fltarr(state.maxd)
		ratia = fltarr(state.maxd)
		rathemisss = where(strcompress(ratval.value(1,*),	$
		/remove_all) ne '')
		if rathemisss(0) ne -1 then ratha(rathemisss)=		$
		float(ratval.value(1,rathemisss))
		ratiemisss = where(strcompress(ratval.value(2,*),        $
                /remove_all) ne '')
                if ratiemisss(0) ne -1 then ratia(ratiemisss)=            $
                float(ratval.value(2,ratiemisss))
	    endif else begin				;Only ratha present
		ratia = state.ratia
		ratha = fltarr(state.maxd)
                rathemisss = where(strcompress(ratval.value(1,*),        $
                /remove_all) ne '')
                if rathemisss(0) ne -1 then ratha(rathemisss)=            $
                float(ratval.value(1,rathemisss))
	    endelse
	endif else begin				;Only ratia present
	    ratha = state.ratha
	    ratia = fltarr(state.maxd)
	    ratiemisss = where(strcompress(ratval.value(1,*),        	$
            /remove_all) ne '')
            if ratiemisss(0) ne -1 then ratia(ratiemisss)=            	$
            float(ratval.value(1,ratiemisss))
	endelse
    endif else begin
	ratha = state.ratha
	ratia = state.ratia
    endelse

		;********************************************
		;**** Get wavelength & emissivity limits ****
		;********************************************

    widget_control, state.wvltextid, get_value=tmp
    wvlower=fix(tmp(0))
    widget_control, state.wvutextid, get_value=tmp
    wvupper=fix(tmp(0))
    widget_control, state.atextid, get_value=tmp    
    emissval=float(tmp(0))
    
		;******************************************
		;**** Write selections to PS structure ****
		;******************************************

    ps = { 	LEQUIL:lequil, 						$
		XXMULT:mult, 						$
		NSTRN1:nstrn1, 						$
		ISTRN1:istrn1, 						$
		NSTRN2:nstrn2, 						$
		ISTRN2:istrn2,						$
		LINDEX:state.lindex,					$
		LRATHA:state.lratha,					$
		LRATIA:state.lratia,					$
		RATHA:ratha,						$
		RATIA:ratia,						$
		WVLOWER:wvlower,						$
		WVUPPER:wvupper,						$
		EMISSVAL:emissval 					}

    widget_control, first_child, set_uvalue=state, /no_copy

    RETURN, ps

END

;-----------------------------------------------------------------------------

FUNCTION proc207_event, event


                ;**** Base ID of compound widget ****

    parent = event.handler

                ;**** Retrieve the state ****

    first_child = widget_info( parent, /child )
    widget_control, first_child, get_uvalue=state, /no_copy

		;*********************************
		;**** Clear previous messages ****
		;*********************************

    widget_control, state.messid, set_value=' '

                ;************************
                ;**** Process events ****
                ;************************

    CASE event.id OF

		;****************************
		;**** Equilibrium Toggle ****
		;****************************

    	state.equilid: begin
	    widget_control, state.equilid, get_value=lequil
	    if lequil then begin
		widget_control, state.xmultid, sensitive=0
	    endif else begin
		widget_control, state.xmultid, sensitive=1		
          endelse
          new_event = 0L
        end
		 
		;***********************
		;**** Update Button ****
		;***********************
		
	state.updateid: begin	
	
		widget_control, state.wvltextid, get_value=wvlower
		widget_control, state.wvutextid, get_value=wvupper
		widget_control, state.atextid,   get_value=emissval		

		widget_control,/hourglass		

		message=''
 		lindex=intarr( state.icnte )
		lindex_old=state.lindex		
    		trantab=strarr( state.icnte )
		icntemp=0

    		mult = dblarr( state.nmet )
    		for i = 0, state.nmet-1 do begin
		  widget_control, state.scaleid(i), get_value=scale
		  scale = scale(0)
		  if num_chk( scale ) then begin
			message='Illegal value entered for metastable scaling fatctor'
		  endif else begin
			mult(i) = double( scale )
		  endelse
    		endfor

		;**** Calculate emissivity values ****

	      widget_control, state.equilid, get_value=lequil
    		for i = 0, state.icnte-1 do begin
    		  state.emiss(i)=0.0
		  sum=0.0
		  if lequil eq 1 then begin
			for j=0,state.nmet-1 do begin
			   sum=sum+state.stack(state.ie2a(i),j)*state.stckm(j)
			endfor
		  endif else begin
    		  	for j=0,state.nmet-1 do begin
    		  	   sum=sum+state.stack(state.ie2a(i),j)*state.stckm(j)*mult(j)
    		  	endfor
		  endelse
    		  state.emiss(i)=state.aval(i)*sum
    		endfor

		if wvlower(0) eq '' AND wvupper(0) eq '' AND emissval(0) eq '' then begin
    	 	   trantab=strarr( state.icnte )
    	 	   for i = 0, state.icnte-1 do begin
	 	   	 trantab(i) = string( i+1, format='(I4)' )     							$
	 	   		     +' '+string( state.ie2a(i), format='(I3)' )	     				$
	 	   		     +state.mtrga( state.ie2a(i)-1 )+' '+state.strga( state.ie2a(i)-1 )      	$
	 	   		     +' '+string( state.ie1a(i), format='(I3)' )	     				$
	         		     +state.mtrga( state.ie1a(i)-1 )+' '+state.strga( state.ie1a(i)-1 )
	 	   	 lindex(i)=i
    	 	   endfor
	 	   icntemp=state.icnte
	      endif else begin
		   
		   wvuval=0
		   wvlval=0
		   emval=0
		   if wvupper(0)  ne '' then begin
		      wvucheck   = num_chk(wvupper(0), /sign)
		   	if wvucheck eq 0 then begin
				wvuval = float(wvupper(0))
			endif else begin
				message='Illegal value entered for upper wavelength'
			endelse
		   endif 		   

		   if wvlower(0)  ne '' then begin
		      wvlcheck   = num_chk(wvlower(0), /sign)
		   	if wvlcheck eq 0 then begin
				wvlval = float(wvlower(0))
				if wvupper(0) eq '' then message='No upper wavelength bound entered'
				if wvupper(0) ne '' AND wvuval le wvlval then message='Illegal wavelength range entered'
			endif else begin
				message='Illegal value entered for lower wavelength'
			endelse
		   endif

		   if emissval(0)  ne '' then begin
		      emisscheck = num_chk(emissval(0), /sign)
		   	if emisscheck eq 0 then begin
				emval = float(emissval(0))
			endif else begin
				message='Illegal value entered for emissivity'
			endelse
		   endif
		
		   if message ne '' then begin
		   	   widget_control, state.messid, set_value=message
	         	   new_event = 0L
		   endif else begin			
			if emissval(0) eq '' AND wvupper(0) ne '' then begin
			    icntemp=0
			    for i = 0, state.icnte-1 do begin
		   	      wvlength=1.0/state.watran(i)*1.0e+8
		   	      if wvlength ge wvlval AND wvlength le wvuval then begin
		   	        trantab(icntemp) = string( i+1, format='(I4)' )				    $
		   	          + ' '+string( state.ie2a(i), format='(I3)' )				    $
		   	          + state.mtrga( state.ie2a(i)-1 )+' '+state.strga( state.ie2a(i)-1 )     $
		   	          + ' '+string( state.ie1a(i), format='(I3)' )				    $
		   	          + state.mtrga( state.ie1a(i)-1 )+' '+state.strga( state.ie1a(i)-1 )
    		   	          lindex(icntemp)=i
		   	          icntemp=icntemp+1
		   	      endif
		   	    endfor
		   	 endif
			 if emissval(0) ne '' AND wvupper(0) eq '' then begin
			    icntemp=0			 
			    for i = 0, state.icnte-1 do begin
		   	      if state.emiss(i) ge emval then begin
		   	        trantab(icntemp) = string( i+1, format='(I4)' )				    $
		   	          + ' '+string( state.ie2a(i), format='(I3)' )				    $
		   	          + state.mtrga( state.ie2a(i)-1 )+' '+state.strga( state.ie2a(i)-1 )     $
		   	          + ' '+string( state.ie1a(i), format='(I3)' )				    $
		   	          + state.mtrga( state.ie1a(i)-1 )+' '+state.strga( state.ie1a(i)-1 )
    		   	          lindex(icntemp)=i
		   	          icntemp=icntemp+1
		   	      endif
		   	    endfor
			 endif   
		   	 if emissval(0) ne '' AND wvupper(0) ne '' then begin
			    icntemp=0			 		 
			    for i = 0, state.icnte-1 do begin
		   	      wvlength=1.0/state.watran(i)*1.0e+8
		   	      if state.emiss(i) ge emval AND wvlength ge wvlval $
		   	      		    AND wvlength le wvuval then begin
		   	        trantab(icntemp) = string( i+1, format='(I4)' )				    $
		   	          + ' '+string( state.ie2a(i), format='(I3)' )				    $
		   	          + state.mtrga( state.ie2a(i)-1 )+' '+state.strga( state.ie2a(i)-1 )     $
		   	          + ' '+string( state.ie1a(i), format='(I3)' )				    $
		   	          + state.mtrga( state.ie1a(i)-1 )+' '+state.strga( state.ie1a(i)-1 )
    		   	          lindex(icntemp)=i
		   	          icntemp=icntemp+1
		   	      endif
		   	    endfor			
			endif
		   endelse
		endelse
		
		state.lindex=lindex

		if message eq '' AND icntemp gt 0 then begin

		   trantemp=strarr( icntemp )
		   trantemp(*)=trantab(0:icntemp-1)
		   traninfo ='       No. of Electron Impact Transitions: '	$
				+strtrim(string(icntemp),2)				$
				+'  No. of Index Energy Levels: '+  		$
            		strtrim(string(state.il),2)

		   widget_control, state.traninfo, set_value=traninfo
		   		   
		   ;**************************************************
		   ;**** Get First Composite Assembly from widget ****
		   ;**************************************************

		   widget_control, state.fcmpaid, get_value=vcmpa1
    		   scmpa = where( vcmpa1 gt -1 )
    		   if scmpa(0) gt -1 then begin
    		     nstrn1 = size( scmpa )
		     strn1 = fix( nstrn1(1) )
    		     istrn1 = vcmpa1( scmpa ) + 1
		     istrn1_new=intarr(strn1)
		     for i=0,strn1-1 do begin
			  dummy=lindex_old(istrn1(i)-1)
			  index=where(lindex(0:icntemp-1) eq dummy)
			  if index(0) ne -1 then begin
				istrn1_new(i)=index(0)
			  endif else begin
				istrn1_new(i)=-1
			  endelse
		     endfor

		     vcmpa1(*)=-1
		     vcmpa1(0:strn1-1)=istrn1_new(*)
		     index=where( vcmpa1 gt -1 )
		     
		     widget_control, state.fcmpaid, set_value=vcmpa1

		     if index(0) ne -1 then currsels=trantab(istrn1_new) else currsels=''
		     widget_control, state.curselid1, set_value=currsels
		   endif else begin
    		     nstrn1 =  0
		     istrn1 = [0]
		     vcmpa1(*) = -1
		   endelse

		   ;***************************************************
		   ;**** Get Second Composite Assembly from widget ****
		   ;***************************************************

		   widget_control, state.scmpaid, get_value=vcmpa2
    		   scmpa = where( vcmpa2 gt -1 )
    		   if scmpa(0) gt -1 then begin
    		     nstrn1 = size( scmpa )
		     strn1 = fix( nstrn1(1) )
    		     istrn1 = vcmpa2( scmpa ) + 1
		     istrn1_new=intarr(strn1)
		     for i=0,strn1-1 do begin
			  dummy=lindex_old(istrn1(i)-1)
			  index=where(lindex(0:icntemp-1) eq dummy)
			  if index(0) ne -1 then begin
				istrn1_new(i)=index(0)
			  endif else begin
				istrn1_new(i)=-1
			  endelse
		     endfor

		     vcmpa2(*)=-1
		     vcmpa2(0:strn1-1)=istrn1_new(*)

		     widget_control, state.scmpaid, set_value=vcmpa2
		   	
		     index=where( vcmpa2 gt -1 )
		     if index(0) ne -1 then currsels=trantab(istrn1_new) else currsels=''
		     widget_control, state.curselid2, set_value=currsels
		   endif else begin
    		     nstrn1 = 0
		     istrn1 = [0]
		     vcmpa2(*)  = -1
		   endelse
	
		   selstruct = { TRANTAB:trantemp, SELECTED1:vcmpa1, SECONDSEL:vcmpa2}
		   widget_control, state.second_child, set_uvalue=selstruct, /no_copy 

		endif else begin
			if message eq '' then message='No transitions exist for given bounds.'
			widget_control, state.messid, set_value=message
	      	new_event = 0L
		endelse
	end

		;****************************
		;**** Clear table button ****
		;****************************

	state.clearid: begin

                ;****************************************
                ;**** Get current table widget value ****
                ;****************************************
                                                              
	    widget_control, state.ratid, get_value=tempval
	    tempvals = where(strcompress(tempval.value(0,*),            $
                             /remove_all) ne '')
            if tempvals(0) ne -1 then begin

                ;*********************************
                ;**** Popup window to confirm ****
                ;*********************************

                messval = 'Are you sure you want to clear the table?'
                action = popup(message=messval, font=state.font,  	$
                               buttons=['Cancel','Confirm'],            $
                               title='ADAS207 Warning:-')

                if action eq 'Confirm' then begin

                ;***********************************
                ;**** Create a dummy zero array ****
                ;***********************************

                    empty = strarr(state.maxd)
		    tempval.value(1,*) = empty
		    tempsize = size(tempval.value)
		    if tempsize(1) eq 3 then begin
			tempval.value(2,*) = empty
		    endif

                ;***************************************
                ;**** Copy new data to table widget ****
                ;***************************************

                    widget_control, state.ratid, set_value=tempval
                endif
            endif
	end
		;***********************
		;**** Cancel button ****
		;***********************

    	state.cancelid: new_event = {ID:parent, TOP:event.top, 		$
				     HANDLER:0L, ACTION:'Cancel'}

		;*********************
		;**** Done button ****
		;*********************

    	state.doneid: begin

			;***************************************
			;**** Check all user input is legal ****
			;***************************************

	    error = 0
	    widget_control, first_child, set_uvalue=state, /no_copy
	    widget_control, event.handler, get_value=ps
	    widget_control, first_child, get_uvalue=state, /no_copy

		;**** check metastable scalings ****

	    metsiz = size( ps.xxmult )
	    if error eq 0 and metsiz(metsiz(0)+1) ne 5 then begin
		error = 1
		message = '**** Error: A Metastable Level Scaling is '+	$
                          'not a valid number ****'
		widget_control, state.scaleid(ps.xxmult-1), /input_focus
            endif

		;**** Check ratios table if present ****

	    if error eq 0 then begin
		if (state.lratha eq 1) or (state.lratia eq 1) then begin
		    widget_control, state.ratid, get_value=ratval
		    ratzeroes = where(ratval.value eq 0)
		    if ratzeroes(0) ne -1 then begin
			messval = 'There are zeroes in the ratio table'+$
				  ' - Do you still want to proceed?'
			action = popup(message=messval, font=state.font,$
			               buttons=['Cancel','Proceed'],	$
 			               title='ADAS207 Warning:-',	$
				       xoffset=200)
			if action eq 'Cancel' then begin
			    error = 1
			    message = ' '
			endif
		    endif
	 	endif
	    endif

		;**** return value or flag error ****

	    if error eq 0 then begin
	        new_event = {ID:parent, TOP:event.top, HANDLER:0L, 	$
                             ACTION:'Done'}
            endif else begin
	        widget_control, state.messid, set_value=message
	        new_event = 0L
            endelse
        end

                ;**** Menu button ****

    state.outid: begin
            new_event = {ID:parent, TOP:event.top, HANDLER:0L,          $
                         ACTION:'Menu'}
        end

    	ELSE: new_event = 0L

    ENDCASE

		;**********************************
		;**** Save 'state' information ****
		;**********************************

    widget_control, first_child, set_uvalue=state, /no_copy

    RETURN, new_event

END

;-----------------------------------------------------------------------------

FUNCTION cw_adas207_proc, topparent, titled, sz, sz0, sionpt, dsnpas, 	$
                          dsninc, nmet, maxd, icnte, il, ndmet, ilev, 	$
		          mstrga, ie1a, ie2a, mtrga, strga, watran, stckm, stack, aval,  $
		          densa, bitfile, VALUE=value, UVALUE=uvalue, 	$
		          FONT_LARGE=font_large, FONT_SMALL=font_small, $
		          EDIT_FONTS=edit_fonts, NUM_FORM=num_form

		;**** Set defaults for keywords ****

    IF NOT (KEYWORD_SET(uvalue)) THEN uvalue = 0
    IF NOT (KEYWORD_SET(font_large)) THEN font_large = ''
    IF NOT (KEYWORD_SET(font_small)) THEN font_small = ''
    IF NOT (KEYWORD_SET(edit_fonts)) THEN 				$
    edit_fonts = {font_norm:'',font_input:''}
    IF NOT (KEYWORD_SET(num_form)) THEN num_form = '(E10.3)'
    IF NOT (KEYWORD_SET(value)) THEN begin
        mult = make_array( nmet, /double, value=1.0D )
	ps = {	LEQUIL:1, 					$
		XXMULT:mult, 					$
		NSTRN1:1, 						$
		ISTRN1:[1], 					$
		NSTRN2:0, 						$
		ISTRN2:[0], 					$
		LINDEX:[0],						$
		LRATHA:0,						$
		LRATIA:0,						$
		RATHA :strarr(maxd),				$
		RATIA :strarr(maxd),				$
		WVUPPER:0,						$
		WVLOWER:0,						$
		emissval:0.0			}
    ENDIF ELSE BEGIN
	ps = {	LEQUIL:value.lequil, 			$
		XXMULT:[value.xxmult], 				$
		NSTRN1:value.nstrn1, 				$
		ISTRN1:[value.istrn1], 				$
		NSTRN2:value.nstrn2, 				$
		ISTRN2:[value.istrn2], 				$
		LINDEX:[value.lindex], 				$
		LRATHA:value.lratha,				$
		LRATIA:value.lratia,				$
		RATHA :value.ratha,				$
		RATIA :value.ratia,				$
		WVUPPER:value.wvupper,				$
		WVLOWER:value.wvlower,				$
		emissval:value.emissval			}
	mulsiz=size( ps.xxmult )
	if mulsiz(0) eq 0 then mulsiz=1 else mulsiz=fix( mulsiz(1) )
	if mulsiz ne nmet then begin
	    xxmult = make_array( nmet, /double, value=1.0D )
	    ps = {	LEQUIL:ps.lequil, 			$
			XXMULT:[xxmult], 				$
			NSTRN1:ps.nstrn1, 			$
			ISTRN1:[ps.istrn1], 			$
			NSTRN2:ps.nstrn2, 			$
			ISTRN2:[ps.istrn2], 			$
			LINDEX:[ps.lindex],			$
			LRATHA:ps.lratha,				$
			LRATIA:ps.lratia,				$
			RATHA :ps.ratha,				$
			RATIA :ps.ratia,				$
			WVUPPER:ps.wvupper,			$
			WVLOWER:ps.wvlower,			$
			emissval:ps.emissval	}
	endif
	if max( [ ps.istrn1 , ps.istrn2 ] ) gt icnte then begin
	    ps = {	LEQUIL:ps.lequil, 			$
			XXMULT:[ps.xxmult], 			$
			NSTRN1:1, 					$
			ISTRN1:[1], 				$
			NSTRN2:0, 					$
			ISTRN2:[0], 				$
			LINDEX:[0],					$
			LRATHA:ps.lratha,                   $
                  LRATIA:ps.lratia,                   $
                  RATHA :ps.ratha,                    $
                  RATIA :ps.ratia,				$
			WVUPPER:ps.wvupper,			$
			WVLOWER:ps.wvlower,			$
			emissval:ps.emissval           }
	endif
    ENDELSE


		;********************************************************
		;**** Create the 207 Processing options/input window ****
		;********************************************************

		;**** create titled base widget ****

    parent = widget_base( topparent, UVALUE = uvalue, 			$
			  EVENT_FUNC = "proc207_event", 		$
			  FUNC_GET_VALUE = "proc207_get_val", 		$
			  /COLUMN )

		;********************************************************
		;**** Create a dummy widget just to hold value of    ****
		;**** "state" variable so as not to get confused     ****
		;**** with any other values. Adopt IDL practice      ****
		;**** of using first child widget                    ****
		;********************************************************

    first_child = widget_base( parent )
    topbase = widget_base( first_child, /column )

		;**** add pass file name

    rc = widget_label( topbase, font=font_large, 			$
			value='Contour Passing File Name:'+dsnpas )

		;**** add dataset name and browse button ****

    rc = cw_adas_dsbr( topbase, dsninc, font=font_large, /row)

		;**** add charge information ****

    rc = widget_label( topbase, font=font_large, 			$
		       value='ION: '+titled+sz+'   Nuclear Charge: '+sz0+$
		       '  Ionisation Potential: '+sionpt+' (cm-1)' )



    metcolbase = widget_base( parent, /row )

		;**** Metastable Level base ****

    metabase = widget_base( metcolbase, /row, /frame )

		;**** Equilibruim Toggle ****

    equilid = cw_single_sel( metabase, ['Non-Equilibrium','Equilibrium'], $
			    value=ps.lequil, title='Transient Conditions',$
			    ysize=2, font=font_small, big_font=font_big )

		;**** Scaling Table ****

    xmultid = widget_base( metabase, /column, /frame )
    widget_control, xmultid, set_uvalue=nmet

    rc = widget_label( xmultid, font=font_big, 				$
		       value='Metastable Levels' )
    baseid = widget_base( xmultid, /row )
    rc = widget_label( baseid, font=font_small, 			$
		       value=' Met.  Level' )
    baseid = widget_base( xmultid, /row )
    rc = widget_label( baseid, font=font_small, 			$
		  value='Index  Index   Designation              Scaling' )
    scaleid = lonarr( nmet )
    for i = 0, nmet-1 do begin
	baseid = widget_base( xmultid, /row )
	rc = widget_label( baseid, font=font_small, 			$
		value=' '+string( i+1, format='(I2)' ) 			$
			+'    '+ilev(i)+'   '+mstrga(i)+'   ' )
	scaleid(i) = widget_text( baseid, 				$
			value=string( ps.xxmult(i), num_form ), 	$
			/editable, xsize=10, font=edit_fonts.font_input )
    endfor

    if ps.lequil then widget_control, xmultid, sensitive=0

		;**********************************************
		;**** Add ratio base and table as required ****
		;**********************************************

    if (ps.lratha eq 1) or (ps.lratia eq 1) then begin
        ratbase = widget_base(metabase, /column, /frame) 
	if ps.lratha eq 1 then begin
	    if ps.lratia eq 1 then begin

		;**** Both sets of ratios appropriate ****

		tabvals = strarr(3, maxd)
                non_blanks = where(ps.ratha ne 0.0)
                if non_blanks(0) ne -1 then begin
                    tabvals(1,*) = strtrim(string(0.0, format=num_form),2)
                    tabvals(1, non_blanks) =                            $
                    strtrim(string(ps.ratha(non_blanks),                $
                    format=num_form),2)
                endif
                non_blanks = where(ps.ratia ne 0.0)
  		if non_blanks(0) ne -1 then begin
                    tabvals(2,*) = strtrim(string(0.0, format=num_form),2)
                    tabvals(2, non_blanks) =                            $
                    strtrim(string(ps.ratia(non_blanks),                $
                    format=num_form),2)
                endif
		headings = [['Electron', '-Ratios-','-Ratios-'],	$
			    ['Dens. (cm-3)','N(H)/NE','N+/N(1)']]
                rattitle = 'Enter ratios for each density:-'
                columns = [0,1,1]
	    endif else begin

		;**** Only ratha ratios appropriate ****

		tabvals = strarr(2, maxd)
                non_blanks = where(ps.ratha ne 0.0)
                if non_blanks(0) ne -1 then begin
                    tabvals(1,*) = strtrim(string(0.0, format=num_form),2)
                    tabvals(1, non_blanks) =                    	$
                    strtrim(string(ps.ratha(non_blanks), 		$
                    format=num_form),2)
                endif
		headings = [['Electron','-Ratios-'],			$
                            ['Dens. (cm-3)','N(H)/NE']]
                rattitle = 'Enter ratio for each density:-'
                columns = [0,1]
	    endelse
	endif else begin

		;**** Only ratia ratios appropriate ****

	    tabvals = strarr(2, maxd)
	    non_blanks = where(ps.ratia ne 0.0)
	    if non_blanks(0) ne -1 then begin
		tabvals(1,*) = strtrim(string(0.0, format=num_form),2)	
		tabvals(1, non_blanks) = 				$
		strtrim(string(ps.ratia(non_blanks), format=num_form),2)
	    endif
	    headings = [['Electron','-Ratios-'],      			$
                        ['Dens. (cm-3)','N+/N(1)']]
            columns = [0,1]
            rattitle = 'Enter ratio for each density:-'
	endelse

		;**** Load in the density values ****

        tabvals(0,*) = strtrim(string(densa(*), format=num_form),2)
	ratid = cw_adas_table(ratbase, tabvals, /scroll, /gaps,		$
			      FONTS=edit_fonts, FONT_LARGE=font_small,	$
			      FONT_SMALL=font_small, COLEDIT=columns,	$
			      COLHEAD=headings, TITLE=rattitle,		$
                              XTEXSIZE=28, YTEXSIZE=4)
	clearid = widget_button(ratbase, font=font_small,		$
                                value='Clear Table')
    endif else begin

		;**** No ratios appropriate ****

	ratid = 0L
        clearid = 0L
    endelse

    lbase  = widget_base( parent, /column, /frame )

    lbase1 = widget_base(lbase,/row,/frame)
    limitbase = widget_base( lbase1, /row, /frame )    
    wvlbase = widget_base( lbase1 , /row )    
    wvllabel = widget_label(wvlbase, value='Lower Wavelength (A): ',font=font_small)
    if ps.wvlower ne 0 then wvlval=strtrim(string(ps.wvlower),2) else wvlval=''
    wvltextid = widget_text(wvlbase, value=wvlval ,/edit ,xsize=5, font=font_large)   
     
    wvubase = widget_base( lbase1 , /row )    
    wvulabel = widget_label(wvubase, value='Upper Wavelength (A): ',font=font_small)
    if ps.wvupper ne 0 then wvuval=strtrim(string(ps.wvupper),2) else wvuval=''
    wvutextid = widget_text(wvubase, value=wvuval ,/edit ,xsize=5, font=font_large)

    abase = widget_base( lbase1 , /row )    
    alabel = widget_label(abase, value='Emissivity lower bound (cm3 s-1): ',font=font_small)
    if ps.emissval ne 0.0 then emissval=strtrim(string(ps.emissval),2) else emissval=''
    atextid = widget_text(abase, value=emissval ,/edit ,xsize=5, font=font_large)

    alabel = widget_label(lbase1, value='   ',font=font_small)

    updateid = widget_button(lbase1, value='  update  ',font=font_small)


		;**** Calculate emissivity values ****

    emiss = fltarr(icnte)
    for i = 0, icnte-1 do begin
      emiss(i)=0.0
      sum=0.0
      if ps.lequil eq 1 then begin
          for j=0,nmet-1 do begin
      	 sum=sum+stack(ie2a(i),j)*stckm(j)
          endfor
      endif else begin
          for j=0,nmet-1 do begin
      	 sum=sum+stack(ie2a(i),j)*stckm(j)*ps.xxmult(j)
          endfor
      endelse
      emiss(i)=aval(i)*sum
    endfor

		;**** Build transition strings ****	  

    lindex=intarr( icnte )
    if ps.wvlower eq 0 AND ps.wvupper eq 0 AND ps.emissval eq 0.0 then begin

       trantab=strarr( icnte )
       for i = 0, icnte-1 do begin
           trantab(i) = string( i+1, format='(I4)' )  		    $
      		   +' '+string( ie2a(i), format='(I3)' )  	    $
      		   +mtrga( ie2a(i)-1 )+' '+strga( ie2a(i)-1 )	    $
      		   +' '+string( ie1a(i), format='(I3)' )  	    $
      		   +mtrga( ie1a(i)-1 )+' '+strga( ie1a(i)-1 )
           lindex(i)=i
       endfor
       icntemp=icnte

    endif else begin
          
          trantmp= strarr( icnte )
          
          if ps.emissval eq 0 AND ps.wvupper gt 0 then begin
      	     icntemp=0
      	     for i = 0, icnte-1 do begin
      	       wvlength=1.0/watran(i)*1.0e+8
      	       if wvlength ge ps.wvlower AND wvlength le ps.wvupper then begin
      		   trantmp(icntemp) = string( i+1, format='(I4)' )  $
      		     + ' '+string( ie2a(i), format='(I3)' )  	    $
      		     + mtrga( ie2a(i)-1 )+' '+strga( ie2a(i)-1 )    $
      		     + ' '+string( ie1a(i), format='(I3)' )  	    $
      		     + mtrga( ie1a(i)-1 )+' '+strga( ie1a(i)-1 )
      		     lindex(icntemp)=i
      		     icntemp=icntemp+1
      	       endif
      	     endfor
          endif
          
          if ps.emissval gt 0.0 AND ps.wvupper eq 0 then begin
      	     icntemp=0
      	     for i = 0, icnte-1 do begin
      	       if emiss(i) ge ps.emissval then begin
      		   trantmp(icntemp) = string( i+1, format='(I4)' )  $
      		     + ' '+string( ie2a(i), format='(I3)' )  	    $
      		     + mtrga( ie2a(i)-1 )+' '+strga( ie2a(i)-1 )    $
      		     + ' '+string( ie1a(i), format='(I3)' )  	    $
      		     + mtrga( ie1a(i)-1 )+' '+strga( ie1a(i)-1 )
      		     lindex(icntemp)=i
      		     icntemp=icntemp+1
      	       endif
      	     endfor
          endif
          
          if ps.emissval gt 0.0 AND ps.wvupper gt 0 then begin
      	     icntemp=0
      	     for i = 0, icnte-1 do begin
      	       wvlength=1.0/watran(i)*1.0e+8
      	       if emiss(i) ge ps.emissval AND wvlength ge ps.wvlower $
      				     AND wvlength le ps.wvupper then begin
      		   trantmp(icntemp) = string( i+1, format='(I4)' )     $
      		     + ' '+string( ie2a(i), format='(I3)' )  	    $
      		     + mtrga( ie2a(i)-1 )+' '+strga( ie2a(i)-1 )	    $
      		     + ' '+string( ie1a(i), format='(I3)' )  	    $
      		     + mtrga( ie1a(i)-1 )+' '+strga( ie1a(i)-1 )
      		     lindex(icntemp)=i
      		     icntemp=icntemp+1
      	       endif
      	     endfor
          endif

          if icntemp GT 0 then begin
             trantab=strarr(icntemp)
             trantab(*)=trantmp(0:icntemp-1)
          endif else begin
             trantab = string(replicate(32B, 62))
          endelse
    
    endelse

		;**** add electron information ****

    lbase2 = widget_base(lbase,/row)
    traninfo = widget_label( lbase2, font=font_large, 			$
			  value='       No. of Electron Impact Transitions: ' 	$
			+strtrim(string(icntemp),2) 			$
			+'  No. of Index Energy Levels: '+		$
                        strtrim(string(il),2) )

    ;**** Create second empty base to store trantab             ****
    ;**** This is to allow widget ID to be passed to cw_207_sel ****
    ;**** so that transition list can be dynamically updated    ****

    second_child = widget_base(parent) 

		;**** Composite Assembly base ****

    compbase = widget_base( parent, /row, /frame )
      

		;**** add first composite assembly ****		

    selected=intarr(20)
    if ps.nstrn1 gt 0 then selected(0:ps.nstrn1-1) = ps.istrn1
    selected = selected - 1   

    selstruct = { TRANTAB:trantab, SELECTED1:selected,  secondsel:selected}
    widget_control, second_child, set_uvalue=selstruct
       
    struct1 = cw_207_sel( compbase, second_child, selected=selected, flag=1,	$
		title='Lines for First Composite Assembly', /ordered, 	$
		font=font_small )

    fcmpaid = struct1.cwid

 
		;**** add second composite assembly ****

    selected=intarr(20)
    if ps.nstrn2 gt 0 then selected(0:ps.nstrn2-1) = ps.istrn2
    selected = selected - 1
    struct2 = cw_207_sel( compbase, second_child, selected=selected, flag=2,	$
		title='Lines for Second Composite Assembly', /ordered, 	$
		font=font_small )

    scmpaid = struct2.cwid

    selstruct.secondsel=selected
    widget_control, second_child, set_uvalue=selstruct, /no_copy

		;**** Error message ****

    messid = widget_label(parent,font=font_large, 			$
    value='Edit the processing options data and press Done to proceed')

		;**** add the exit buttons ****

    base207 = widget_base(parent,/row)
    menufile = bitfile + '/menu.bmp'
    read_X11_bitmap, menufile, bitmap1
    outid = widget_button(base207, value=bitmap1)          ;menu button
    cancelid = widget_button(base207,value='Cancel',font=font_large)
    doneid = widget_button(base207,value='Done',font=font_large)
  
		;*************************************************
		;**** create a state structure for the pop-up ****
		;**** window.                                 ****
		;*************************************************

    new_state = { EQUILID:equilid, 					$
		  OUTID:outid,						$
		  XMULTID:xmultid, 					$
		  SCALEID:scaleid, 					$
  		  FCMPAID:fcmpaid, 					$
		  SCMPAID:scmpaid, 					$
		  WVLTEXTID:wvltextid,					$
		  WVUTEXTID:wvutextid,					$
		  ATEXTID:atextid,					$
		  UPDATEID:updateid,					$
		  TRANINFO:traninfo,					$
		  LINDEX:lindex,						$		  
		  CANCELID:cancelid, 					$
		  DONEID:doneid, 						$
		  MESSID:messid, 						$
		  NUM_FORM:num_form, 					$
		  RATID:ratid,						$
		  LRATHA:ps.lratha,					$
		  LRATIA:ps.lratia,					$
		  RATHA:ps.ratha,						$
		  RATIA:ps.ratia,						$
		  ICNTE:icnte,						$
		  ICNTEMP:icntemp,					$
		  IE1A:ie1a,						$
		  IE2a:ie2a,						$
		  MTRGA:mtrga,						$
		  STRGA:strga,						$
		  WATRAN:watran,						$
		  STCKM:stckm,						$
		  STACK:stack,						$
		  EMISS:emiss,						$
		  AVAL:aval,						$
		  NMET:nmet,						$
		  IL:il,							$
		  CLEARID:clearid,					$
		  SECOND_CHILD:second_child,				$
		  MAXD:maxd,						$
		  CURSELID1:struct1.curselid,				$
		  CURSELID2:struct2.curselid,				$	  
		  FONT:font_large }

                ;**** Save initial state structure ****

    widget_control, first_child, set_uvalue=new_state, /no_copy

		;**** Short pause before returning ****

    wait, 1.0
    RETURN, parent

END

