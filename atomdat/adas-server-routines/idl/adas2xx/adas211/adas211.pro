; Copyright (c) 1996, Strathclyde University.
; SCCS info: Module @(#)$Header: /home/adascvs/idl/adas2xx/adas211/adas211.pro,v 1.13 2005/03/16 11:18:04 allan Exp $ Date $Date: 2005/03/16 11:18:04 $
;+
; PROJECT:
;       ADAS IBM MVS to UNIX conversion
;
; NAME:
;	ADAS211
;
; PURPOSE:
;	The highest level routine for the ADAS 211 program.
;
; EXPLANATION:
;	This routine is called from the main adas system routine, adas.pro,
;	to start the ADAS 211 application.  Associated with adas211.pro
;	is a FORTRAN executable, adas211.out.  The IDL code provides the user
;	interface and output graphics whilst the FORTRAN code reads
;	in data files, performs numerical processing and creates the
;	output files.  The IDL code communicates with the FORTRAN
;	executable via a bi-directional UNIX pipe.  The unit number
;	used by the IDL for writing to and reading from this pipe is
;	allocated when the FORTRAN executable is 'spawned' (see code
;	below).  Pipe communications in the FORTRAN process are to
;	stdin and stdout, i.e streams 5 and 6.
;
;	The FORTRAN code is an independent process under the UNIX system.
;	The IDL process can only exert control over the FORTRAN in the
;	data which it communicates via the pipe.  The communications
;	between the IDL and FORTRAN must be exactly matched to avoid
;	input conversion errors.  The correct ammounts of data must be
;	passed so that neither process 'hangs' waiting for communications
;	which will never occur.
;
;	The FORTRAN code performs some error checking which is
;	independent of IDL.  In cases of error the FORTRAN may write
;	error messages.  To prevent these error messages from conflicting
;	with the pipe communications all FORTRAN errors are written to
;	output stream 0, which is stderr for UNIX.  These error messages
;	will appear in the window from which the ADAS session/IDL session
;	is being run. In the case of the batch job, the error messages
;	will appear as standard cron job output which is usually
;	emailed to the user. If the job has completed succesfully then
;	the user will get a message telling them so.
;
;	In the case of severe errors the FORTRAN code may terminate
;	itself prematurely.  In order to detect this, and prevent the
;	IDL program from 'hanging' or crashing, the IDL checks to see
;	if the FORTRAN executable is still an active process before
;	each group of pipe communications.  The process identifier
;	for the FORTRAN process, PID, is recorded when the process is
;	first 'spawned'.  The system is then checked for the presence
;	of the FORTRAN PID. This checking does not occur for the 
;	batch cases.
;
; USE:
;	First the system settings must be established by calling
;	adas_sys_set.pro then adas211.pro is called to start the
;	ADAS 211 application;
;
;	adas_sys_set, adasrel, fortdir, userroot, centroot, 		$
;		      devlist, devcode, font_large, font_small, edit_fonts
;	adas211,   adasrel, fortdir, userroot, centroot, devlist, 	$
;		   devcode, font_large, font_small, edit_fonts
;
; INPUTS:
;	ADASREL - A string indicating the ADAS system version, 
;		  e.g ' ADAS RELEASE: ADAS93 V1.1.0'.  The first
;		  character should be a space.
;
;	FORTDIR - A string holding the path to the directory where the
;		  FORTRAN executables are, e.g '/disk/adas/fortran/exec'
;
;	USERROOT - A string holding the path to the root directory of
;		   the user's adas data. e.g '/disk/bowen/adas'
;		   This root directory will be used by adas to construct
;		   other path names.  For example the users default data
;		   for adas205 should be in /disk/bowen/adas/adf04.  In
;		   particular the user's default interface settings will
;		   be stored in the directory USERROOT+'/defaults'.  An
;		   error will occur if the defaults directory does not
;		   exist.
;
;	CENTROOT - Like USERROOT, but this directory points to the
;		   central data area for the system.  User defaults are
;		   not stored on CENTROOT.
;
;	DEVLIST - A string array of hardcopy device names, used for
;		  graphical output. e.g ['Post-Script','HP-PCL','HP-GL']
;		  This array must mirror DEVCODE.  DEVCODE holds the
;		  actual device names used in a SET_PLOT statement.
;
;	DEVCODE - A string array of hardcopy device code names used in
;		  the SET_PLOT statement, e.g ['PS', 'PCL', 'HP']
;		  This array must mirror DEVLIST.
;
;	FONT_LARGE - The name of a larger font e.g 'courier_bold14'
;
;	FONT_SMALL - The name of a smaller font e.g 'courier_bold12'
;
;	EDIT_FONTS - A structure of two fonts used in the table editor
;		     adas_edtab.pro, {font_norm:'',font_input:''}.
;		     The two fonts are used to differentiate between
;		     editable and non-editable parts of the table. You
;		     may have to experiment to get two fonts which give
;		     the same default spacing in the edit widget.
;		     e.g {font_norm:'helvetica_bold14', $
;			  font_input:'helvetica_oblique14'}
;
; OPTIONAL INPUTS:
;	None
;
; OUTPUTS:
;	None - Note this routine should leave all inputs un-modified.
;
; OPTIONAL OUTPUTS:
;	None
;
; KEYWORD PARAMETERS:
;	None
;
; CALLS:
;	FIND_PROCESS	Checks to see if a given process is active.
;       XXDATE          Get date and time from operating system.
;       BBSPF0          Pipe comms with FORTRAN BbSPF0 routine.
;       BBSPF1          Pipe comms with FORTRAN BbSPF1 routine.
;	BBSPF2		Updates the batch files and variables for
;			the batch runs depending on the user selections.
;
; SIDE EFFECTS:
;	This routine spawns FORTRAN executables.  Note the pipe 
;	communications routines listed above. 
;
; CATEGORY:
;	Adas system.
;	
; WRITTEN:
;       William Osborn, Tessella Support Services plc, 04/07/1996
;
; MODIFIED:
;	1.1	William Osborn	
;		First release
;	1.2	William Osborn	
;		Changed SunOS batch job behaviour
;	1.3	William Osborn	
;		Added call to batch.pro rather than having code for
;		each UNIX flavour present in this routine.
;	1.4	William Osborn	
;		Increased version number to 1.2
;	1.5	Richard Martin
;		Added 'printf' statement for date
;	1.6	Richard Martin
;		Increased version number to 1.3
;	1.7	Richard Martin
;		Increased version number to 1.4
;	1.8     Richard Martin
;		Added date to 'info####.tmp' batch file. Increased 
;		version no. to 1.5 .
;	1.9     Richard Martin
;		Increased version no. to 1.6 .
;	1.10    Richard Martin
;		Increased version no. to 1.7 .
;	1.11    Martin O'Mullane
;		Change output default names to adas_adf04.pass from data04.pass
;               and adas_adf08.pass from data08.pass.
;		Increased version no to 1.8
;	1.12    Richard Martin
;		Increased version no. to 1.9 .
;	1.13    Allan Whiteford
;		Added fields in inval to handle Non-Maxwellian modelling.
;
; VERSION:
;       1.1	04-07-96
;       1.2	11-10-96
;	1.3	18-10-96
;       1.4	25-11-96
;	1.5     14-02-97
;	1.6	04-04-97
;	1.7	04-04-97
;	1.8     30-10-97
;	1.9     11-03-98
;	1.10    15-11-2000
;	1.11    18-12-2001
;	1.12    18-03-2003
;	1.13    27-01-2005
;	
;-----------------------------------------------------------------------------


PRO ADAS211,	adasrel, fortdir, userroot, centroot, 			$
		devlist, devcode, font_large, font_small, edit_fonts

		;************************
		;**** Initialisation ****
		;************************

    adasprog = ' PROGRAM: ADAS211 V1.9'
    lpend = 0
    ipset = 0
    ipbatset = 0
    l2batch = 0				;Marker for whether a batch file
					;has been created.
    deffile = userroot+'/defaults/adas211_defaults.dat'
    device = ''

		;******************************************
		;**** Search for user default settings ****
		;**** If not found create defaults     ****
		;******************************************

    files = findfile(deffile)
    if files(0) eq deffile then begin

        restore,deffile
        inval.centroot = centroot+'/adf08/'
        inval.userroot = userroot+'/adf08/'
        inval.cent37 = centroot+'/adf37/'
        inval.user37 = userroot+'/adf37/'

    end else begin

        inval = { 							$
		ROOTPATH:userroot+'/adf08/', 				$
		FILE:'', 						$
		CENTROOT:centroot+'/adf08/', 				$
		USERROOT:userroot+'/adf08/', 				$
		DIST:0,							$
		DPARAM:0.0,						$
		ROOT37:userroot+'/adf37/',				$
		FILE37:'',						$
		CENT37:centroot+'/adf37/', 				$
		USER37:userroot+'/adf37/' }

    outval = { 								$
                CONOUT:0, CONAPP:-1, 					$
                CONREP:0, CONDSN:'', 					$
                CONDEF:userroot+'/pass/adas211_adf08.pass',CONMES:'',	$
		PASOUT:0, PASAPP:-1,					$
		PASREP:0, PASDSN:'',					$
		PASDEF:userroot+'/pass/adas211_adf04.pass', PASMES:'',	$
		TEXOUT:0, TEXAPP:-1,					$
		TEXREP:0, TEXDSN:'',					$
		TEXDEF:'paper.txt', TEXMES:'',				$
		TITLE:''						$
             }

    end


		;****************************
		;**** Start fortran code ****
		;****************************

    spawn, fortdir+'/adas211.out', unit=pipe, /noshell, PID=pid

		;************************************************
		;**** Write to fortran via pipe that we are  ****
		;**** running interactively                  ****
		;************************************************

    ibatch = 0
    printf, pipe, ibatch

		;******************
		;**** Get date ****
		;******************

    date = xxdate()
    printf, pipe, date(0)
    
LABEL100:
		;**** Check FORTRAN still running ****
  
    if find_process(pid) eq 0 then goto, LABELEND

		;************************************************
		;**** Communicate with bbspf0 in fortran and ****
		;**** invoke user interface widget for       ****
		;**** Data file selection                    ****
		;************************************************

    bbspf0, pipe, inval, dsfull, rep, FONT=font_large

;    check_pipe, pipe

    ipset = 0
    ipbatset = 0

		;**** If cancel selected then end program ****

    if rep eq 'YES' then goto, LABELEND

LABEL300:
		;**** Check FORTRAN still running ****

    if find_process(pid) eq 0 then goto, LABELEND

		;************************************************
		;**** Communicate with bbspf1 in fortran and ****
		;**** invoke user interface widget for       ****
		;**** Output options                         ****
		;************************************************

    if (l2batch ne 0) then begin
        outval.pasapp = 1
        outval.pasrep = 0
    endif
    bbspf1, pipe, lpend, outval, dsfull, header, FONT=font_large

;    check_pipe, pipe
		;**** If cancel selected then erase output ****
		;**** messages and goto 100.		   ****

    if lpend eq 1 then begin
        outval.conmes = ''
        outval.pasmes = ''
        outval.texmes = ''
        goto, LABEL100
    end


		;**** Fortran processing now in progress. Get ****
		;**** date and time for this run.	      ****

    date = xxdate()

		;**** Create header for output. ****

    header = adasrel+adasprog+' DATE: '+date(0)+' TIME: '+date(1)


		;***********************************************
		;**** Now either begin the calculation or   ****
		;**** set up the batch file depending on    ****
		;**** what action has been requested.  	    ****
                ;***********************************************

    if lpend eq 0 then begin			;'Run Now' selected
        ipset = ipset + 1			; Increase 'run number'
        if (outval.pasrep eq 1) then ipset = 1
        bbspf2, pipe, FONT=font_large
;	check_pipe, pipe
    endif else if lpend eq 2 then begin		;'Run in batch' selected
        ipbatset = ipbatset + 1			;Increase run number
;        if (l2batch eq 0) then l2batch = 1
;	bbspf3, pipe, outval, dsfull, header,		  		$
;        idz0, irz0, irz1, irz2, symbr, symbd, ipbatset, date, 		$
;	dsbatch, fortdir, l2batch

        ;**** Kick the batch job off ****

        rand = strmid(strtrim(string(randomu(seed)), 2), 2, 4)
        dscontrol = 'control'+rand+'.tmp'
        dsinfo = 'info'+rand+'.tmp'
        openw, conunit, dscontrol, /GET_LUN
        openw, infunit, dsinfo, /GET_LUN
        printf, conunit, fortdir+'/adas211.out < '+dsinfo 
        printf, conunit, 'rm -f '+dsinfo 
        printf, conunit, 'rm -f '+dscontrol

 	;**** Write stuff needed by adas211 - it will be read into stdin ****

        yesbatch = 1
	repn = 'NO'
	repy = 'YES'
	lpend = 0
        printf, infunit, yesbatch
        printf, infunit, date(0)
        
	;**** Input screen, bbspf0, pipe info ****
        printf, infunit, repn
        printf, infunit, dsfull

	;**** Output screen, bbspf1, pipe info ****
        printf, infunit, lpend
	printf, infunit, outval.condsn
	printf, infunit, outval.pasdsn
	printf, infunit, outval.texdsn
	printf, infunit, outval.title
	printf, infunit, outval.conout
	printf, infunit, outval.pasout
	printf, infunit, outval.texout
	printf, infunit, lpend

	;**** Input screen again - reply yes => 'cancel' ****
        printf, infunit, repy
        printf, infunit, dsfull

        close, infunit, conunit
        spawn, 'chmod u+x '+dscontrol

        batch, dscontrol, title='ADAS211: INFORMATION', jobname='NO. '+rand

        l2batch = 0

    endif

    outval.pasapp = -1

		;**** Back for more input options ****
    outval.conmes=''
    outval.pasmes=''
    outval.texmes=''

    GOTO, LABEL100

LABELEND:

		;**** Save user defaults ****

    save, inval, outval, filename=deffile


END
