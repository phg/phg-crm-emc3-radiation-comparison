; Copyright (c) 1996, Strathclyde University.
; SCCS info: Module @(#)$Header: /home/adascvs/idl/adas2xx/adas211/adas211_out.pro,v 1.1 2004/07/06 10:23:41 whitefor Exp $ Date $Date: 2004/07/06 10:23:41 $
;
;+
; PROJECT:
;       ADAS IBM MVS to UNIX conversion
;
; NAME:
;	ADAS211_OUT
;
; PURPOSE:
;	IDL ADAS user interface, output options.
;
; EXPLANATION:
;	This routine creates and manages a pop-up window which allows
;	the user to select options and input data to control ADAS211
;	graphical and file output.
;
; USE:
;	This routine is specifically for use with adas211.             
;
; INPUTS:
;	VAL	- A structure which determines the initial settings of
;		  the output options widget.  The value is passed
;		  unmodified into cw_adas211_out.pro.
;
;		  See cw_adas211_out.pro for a full description of this
;		  structure.
;
;	DSFULL	- String; The full system file name of the input 
;		  dataset selected by the user for processing.
;
; OPTIONAL INPUTS:
;	None.
;
; OUTPUTS:
;	ACT	- String; Either 'Done' or 'Cancel' for the button the
;		  user pressed to terminate the output options window.
;
; OPTIONAL OUTPUTS:
;	None.
;
; KEYWORD PARAMETERS:
;	FONT	- Supplies the font to be used for the interface widgets.
;
; CALLS:
;	CW_ADAS211_OUT	Creates the output options widget.
;	XMANAGER
;	See side effects for widget management routine.
;
; SIDE EFFECTS:
;	This routine uses a common block OUT211_BLK to maintain its state.
;	ADAS211_OUT_EV	is included in this file and is called
;	indirectly from XMANAGER during widget management.
;
; CATEGORY:
;	Compound Widget.
;
; WRITTEN:
;       William Osborn, Tessella Support Services plc, 4th July 1996
;
; MODIFIED:
;	1.1	William Osborn		04/07/96
; VERSION:
;	1.1	First Release	
;-
;-----------------------------------------------------------------------------


pro adas211_out_ev, event

  common out211_blk,action,value
	

		;**** Find the event type and copy to common ****

    action = event.action

    case action of

		;**** 'Run Now' button ****

	'Run Now'  : begin

			;**** Get the output widget value ****

     		     child = widget_info(event.id, /child)
		     widget_control, child, get_value=value 

                        ;*****************************************
			;**** Kill the widget to allow IDL to ****
			;**** continue and interface with     ****
			;**** FORTRAN only if there is work   ****
			;**** for the FORTRAN to do.          ****
			;*****************************************

		     if (value.pasout eq 1) or (value.conout eq 1) then begin
		        widget_control, event.top, /destroy
		     endif 
	          end


                ;**** 'Run in Batch' button ****

        'Run In Batch'  : begin

                        ;**** Get the output widget value ****

                     child = widget_info(event.id, /child)
                     widget_control, child, get_value=value

                        ;*****************************************
                        ;**** Kill the widget to allow IDL to ****
                        ;**** continue and interface with     ****
                        ;**** FORTRAN only if there is work   ****
                        ;**** for the FORTRAN to do.          ****
                        ;*****************************************

                     if (value.pasout eq 1) or (value.conout eq 1) then begin
                        widget_control, event.top, /destroy
                     endif
                  end

		;**** 'Cancel' button ****

	'Cancel': widget_control, event.top, /destroy

        ELSE: begin   				
			;do nothing 
              end
    endcase

end

;-----------------------------------------------------------------------------


pro adas211_out, val, dsfull, act,  font=font

  common out211_blk, action, value

		;**** Copy value to common ****

  value = val

		;**** Set defaults for keywords ****

  if not (keyword_set(font)) then font = ''

		;***************************************
		;**** Pop-up a new widget           ****
		;***************************************

                ;**** create base widget ****

  outid = widget_base(title='ADAS211 OUTPUT OPTIONS', xoffset=100, yoffset=100)

		;**** Declare output options widget ****

  cwid = cw_adas211_out(outid, dsfull, value=value,  			$
			 font=font )

		;**** Realize the new widget ****
  dynlabel, outid
  widget_control, outid, /realize

		;**** make widget modal ****

  xmanager,'adas211_out', outid, event_handler='adas211_out_ev',	$
                          /modal, /just_reg
 
		;**** Return the output value from common ****

  act = action
  val = value

END

