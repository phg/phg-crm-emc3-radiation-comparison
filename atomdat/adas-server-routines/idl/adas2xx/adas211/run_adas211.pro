;----------------------------------------------------------------------
;+
; PROJECT    :  ADAS
;
; NAME       :  run_adas211
;
; PURPOSE    :  Calculate state selective radiative recombination rates.
;               Generate RR contribution to R-lines in an adf04 dataset.
;
; ARGUMENTS  :  All output arguments will be defined appropriately.
;
;               NAME      I/O   TYPE    DETAILS
; INPUTS     :  driver     I    str     adf08 driver file.
;               adf04_r    I    str     file of augmented R-lines for
;                                       editing into adf04 specific
;                                       ion file.
;               log        I    str     log file (paper.txt)
;               adf08      I    str     outpur RR adf08 file
;
; KEYWORDS   :  help       I      -     Display header as help.
;
; OUTPUTS    :  all information is returned in ADAS datasets.
;
;
; NOTES      :  run_adas211 used the spawn command. Therefore IDL v5.3
;               cannot be used. Any other version will work.
;               The input driver file is in adf08 format but is called 
;               driver here to distinguish it from adf08 output.
;
;
; AUTHOR     :  Martin O'Mullane
;
; DATE       :  04-09-2009
;
;
; MODIFIED:
;       1.1     Martin O'Mullane
;                 - First version.
;
; VERSION:
;       1.1     04-09-2009
;-
;----------------------------------------------------------------------



PRO run_adas211, driver  = driver,  $
                 adf08   = adf08,   $
                 adf04_r = adf04_r, $
                 log     = log,     $
                 help    = help


; If asked for help

if keyword_set(help) then begin
   doc_library, 'run_adas211'
   return
endif


; First check that we are not using IDL v5.3

if !version.release EQ '5.3' then begin
   message, 'This version of IDL (v5.3) will not work - ' + $
            'change to v5.0, v5.1, v5.2 or v5.4 or v6 and above'
   return
endif


; Check that we get all inputs and that they are correct. Otherwise print
; a message and return to command line.

on_error, 2

if n_elements(adf04_r) EQ 0 then message,'An output file for R-lines is required'

; Check the input driver file

if n_elements(driver) EQ 0 then begin

    message,'A driver file (adf08 format) is required'

endif else begin

   a08 = driver
   file_acc, a08, exist, read, write, execute, filetype
   if exist ne 1 then message, 'driver file does not exist '+ a08
   if read ne 1 then message, 'driver file cannot be read from this userid '+ a08

endelse


if n_elements(log) NE 0 then log = log else log = 'NULL'
if n_elements(adf08) NE 0 then outfile = adf08 else outfile = 'NULL'

; Run the fortran code

date = xxdate()

fortdir = getenv('ADASFORT')
spawn, fortdir+'/adas211.out', unit=pipe, /noshell, PID=pid

; Preliminaries

ibatch = 0L
printf, pipe, ibatch
printf, pipe, date[0]


; Input screen

rep = 'NO'
dist = 0L
dparam = 1.0D-30
dsn37 = 'NULL'

printf, pipe, rep
printf, pipe, a08
printf, pipe, dist
printf, pipe, dparam
printf, pipe, dsn37


; Processing screen

printf, pipe, 0L
printf, pipe, outfile
printf, pipe, adf04_r
printf, pipe, log
printf, pipe, 'processed by run_adas211'
if outfile EQ 'NULL' then printf, pipe, 0L else printf, pipe, 1L
printf, pipe, 1L
if log EQ 'NULL' then printf, pipe, 0L else printf, pipe, 1L

printf, pipe, 0L


; Calculation

n_nlev = -1L
idum = -1L

readf, pipe, n_nlev
for j = 0, n_nlev-1 do readf, pipe, idum



; Finish

rep = 'YES'
printf, pipe, rep
printf, pipe, a08
printf, pipe, dist
printf, pipe, dparam
printf, pipe, dsn37

; Free up allocated units

idum = -1L
readf, pipe, idum

close, /all


END
