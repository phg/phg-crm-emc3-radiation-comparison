; Copyright (c) 1996, Strathclyde University.
; SCCS Info : Module @(#)$Header: /home/adascvs/idl/adas2xx/adas211/cw_adas211_out.pro,v 1.2 2004/07/06 12:35:03 whitefor Exp $ Date $Date: 2004/07/06 12:35:03 $
;+
; PROJECT:
;       ADAS IBM MVS to UNIX conversion
;
; NAME:
;	CW_ADAS211_OUT()
;
; PURPOSE:
;	Produces a widget for ADAS211 output options.
;
; EXPLANATION:
;	This function declares a compound widget consisting of three
;	output file widgets cw_adas_outfile.pro.  The text output
;	file specified in this widget is for tabular (paper.txt)
;	output. The data output files produce passing file data in the 
;	formats adf04 and adf12.  This widget also includes a button for
;	browsing the comments from the input dataset, a 'Cancel' button
;	and 'Run Now' and 'Run in batch' buttons.
;	The compound widgets cw_adas_outfile.pro included in this file 
;	are self managing.  This widget only handles events from the 
;	'Cancel' and 'Run...' buttons.
;
;	This widget only generates events for the 'Run..' and 'Cancel'
;	buttons.
;
; USE:
;	This routine is unique to adas211.                              
;
; INPUTS:
;	PARENT	- Long integer; ID of parent widget.
;
;	DSFULL	- Name of input dataset for this application.
;
; OPTIONAL INPUTS:
;	None.
;
; OUTPUTS:
;	The return value of this function is the ID of the declared widget.
;
; OPTIONAL OUTPUTS:
;	None.
;
; KEYWORD PARAMETERS:
;	DEVLIST	- A string array;  Each element of the array is the
;		  name of a hardcopy graphics output device.  This
;		  list must mirror the DEVCODE list.  DEVLIST is the
;		  list which the user will see.
;		  e.g ['Post-Script','HP-PCL','HP-GL']
;
;	VALUE	- A structure which determines the initial settings of
;		  the output options widget.  The structure is made
;		  up of two parts.  Each part is the same as the value
;		  structure of one of the two main compound widgets
;		  included in this widget.  See 
;		  cw_adas_outfile for more details.  The default value is;
;
;		      { 						$
;			CONOUT:0, CONAPP:0 , 				$
;			CONREP:0, CONDSN:'', 				$
;			CONDEF:'',CONMES:'', 				$
;			PASOUT:0, PASAPP:0,  				$
;			PASREP:0, PASDSN:'', 				$
;			PASDEF:'', PASMES:''  				$
;			TEXOUT:0, TEXAPP:0,  				$
;			TEXREP:0, TEXDSN:'', 				$
;			TEXDEF:'', TEXMES:'', TITLE:''  		$
;		      }
;
;	  For CW_ADAS_OUTFILE adf08 output;
;		CONOUT	Integer; Activation button 1 on, 0 off
;		CONAPP	Integer; Append button 1 on, 0 off, -1 no button
;		CONREP	Integer; Replace button 1 on, 0 off, -1 no button
;		CONDSN	String; Output file name
;		CONDEF	String; Default file name
;		CONMES	String; file name error message
;	  For CW_ADAS_OUTFILE adf04 output
;		PASOUT  Integer; Activation button 1 on, 0 off
;		PASAPP  Integer; Append button 1 on, 0 off, -1 no button
;		PASREP  Integer; Replace button 1 on, 0 off, -1 no butn
;		PASDSN  String; Output file name
;		PASDEF  String; Default file name
;		PASMES  String; file name error message
;	  For CW_ADAS_OUTFILE PAPER.TXT output
;		TEXOUT  Integer; Activation button 1 on, 0 off
;		TEXAPP  Integer; Append button 1 on, 0 off, -1 no button
;		TEXREP  Integer; Replace button 1 on, 0 off, -1 no butn
;		TEXDSN  String; Output file name
;		TEXDEF  String; Default file name
;		TEXMES  String; file name error message
;
;		TITLE 	String; user-input title for this run.
;
;	UVALUE	- A user value for this widget.
;
;	FONT	- String; the name of a font to be used for all text in
;		  this widget.  Default to system default font.
;
; CALLS:
;	CW_ADAS_DSBR	Input dataset name and comments browse button.
;	CW_ADAS_OUTFILE	Output file name entry widget.
;	See side effects for other related widget management routines.
;
; SIDE EFFECTS:
;
; 	Two other routines are included in this file
;	OUT211_GET_VAL()
;	OUT211_EVENT()
;
; CATEGORY:
;	Compound Widget
;
; WRITTEN:
;       William Osborn, Tessella Support Services plc, 4th July 1996
;
; MODIFIED:
;	1.1	William Osborn
;		First Release
;	1.2     Martin O'Mullane
;		Change output default names to adas_adf04.pass from data04.pass
;               and adas_adf08.pass from data08.pass.
; VERSION:
;	1.1	04/07/96
;	1.2     18-12-2001
;
;-
;-----------------------------------------------------------------------------

FUNCTION out211_get_val, id

                ;**** Return to caller on error ****

  ON_ERROR, 2

                ;**** Retrieve the state ****

  parent=widget_info(id, /parent)
  widget_control, parent, get_uvalue=state, /no_copy

		;**** Get text output settings ****

  widget_control, state.paperid, get_value=papos
  widget_control, state.passid, get_value=pspos
  widget_control, state.textid, get_value=ptpos

		;***********************************
		;**** Get run title from widget ****
		;**** Then centre in in string  ****
		;**** of 40 characters 		****
		;***********************************

    widget_control, state.titid, get_value=title
    title = strcompress(title(0))
    title_len = strlen(title) 
    if (title_len gt 40 ) then begin 
        title = strmid(title, 0, 37)
        widget_control, state.messid, set_value="Title too long - truncated "
        widget_control, state.titid, set_value=title
	wait, 1
    endif
    pad = (40 - title_len)/2 
    spaces = '                                         '
    title(0) = strmid(spaces,0,(pad-1)) + title + strmid(spaces,0,(pad-1))

  os = {   out211_set, 							$
	   CONOUT:papos.outbut,  CONAPP:papos.appbut, 		 	$
	   CONREP:papos.repbut,  CONDSN:papos.filename, 	 	$
	   CONDEF:papos.defname, CONMES:papos.message,		 	$
	   PASOUT:pspos.outbut,  PASAPP:pspos.appbut,		 	$
	   PASREP:pspos.repbut,	 PASDSN:pspos.filename,		 	$
	   PASDEF:pspos.defname, PASMES:pspos.message,		 	$
	   TEXOUT:ptpos.outbut,  TEXAPP:ptpos.appbut,		 	$
	   TEXREP:ptpos.repbut,	 TEXDSN:ptpos.filename,		 	$
	   TEXDEF:ptpos.defname, TEXMES:ptpos.message,		 	$
	   TITLE:title							$
        }

                ;**** Return the state ****

  widget_control, parent, set_uvalue=state, /no_copy

  RETURN, os 

END

;-----------------------------------------------------------------------------

FUNCTION out211_event, event


                ;**** Base ID of compound widget ****

  parent=event.top

                ;**** Retrieve the state ****

  widget_control, parent, get_uvalue=state, /no_copy

		;*********************************
		;**** Clear previous messages ****
		;*********************************

  widget_control, state.messid, set_value=' '

                ;************************
                ;**** Process events ****
                ;************************

  CASE event.id OF

		;***********************
		;**** Cancel button ****
		;***********************

    state.cancelid: new_event = {ID:parent, TOP:event.top, 		$
				 HANDLER:0L, ACTION:'Cancel'}

		;*****************************
		;**** Run In Batch button ****
		;*****************************

    state.batid: begin

		;****************************************************
                ;**** Return the state before checking can start ****
		;**** with the get_value keyword.                ****
		;****************************************************

          widget_control, parent, set_uvalue=state, /no_copy

		;***************************************
		;**** Check for errors in the input ****
		;***************************************

	  error = 0

		;**** Get a copy of the widget value ****

	  widget_control, event.handler, get_value=os
	
		;**** Check for widget error messages ****

          mess = ''
	  if (os.conout eq 1 and strtrim(os.conmes) ne '')		$
	  or (os.pasout eq 1 and strtrim(os.pasmes) ne '') 		$
	  or (os.texout eq 1 and strtrim(os.texmes) ne '') then error=1

	  if (os.conout eq 0) then begin
	      error = 1
	      mess = 'Error: you must choose an adf08 file'
	  endif

                ;**** Retrieve the state   ****

          widget_control, parent, get_uvalue=state, /no_copy

	  if error eq 1 then begin
	      if mess eq '' then mess = '**** Error in output settings ****'
	      widget_control,state.messid,set_value = mess
	      new_event = 0L
	  endif else begin
	      new_event = {ID:parent, TOP:event.top, HANDLER:0L, 	$
	      ACTION:'Run In Batch'}
	  endelse

        end

		;************************
		;**** Run Now button ****
		;************************

    state.runid: begin

		;****************************************************
                ;**** Return the state before checking can start ****
		;**** with the get_value keyword.                ****
		;****************************************************

          widget_control, parent, set_uvalue=state, /no_copy

		;***************************************
		;**** Check for errors in the input ****
		;***************************************

	  error = 0

		;**** Get a copy of the widget value ****

	  widget_control, event.handler, get_value=os
	
		;**** Check for widget error messages ****

	  mess=''
	  if (os.conout eq 1 and strtrim(os.conmes) ne '')		$
	  or (os.pasout eq 1 and strtrim(os.pasmes) ne '') 		$
	  or (os.texout eq 1 and strtrim(os.texmes) ne '') then error=1

	  if (os.conout eq 0) then begin
	      error = 1
	      mess = 'Error: you must choose an adf08 file'
	  endif

                ;**** Retrieve the state   ****

          widget_control, parent, get_uvalue=state, /no_copy

	  if error eq 1 then begin
	      if mess eq '' then mess = '**** Error in output settings ****'
	      widget_control, state.messid, set_value= mess
	      new_event = 0L
	  endif else begin
	      new_event = {ID:parent, TOP:event.top, HANDLER:0L, 	$
	      ACTION:'Run Now'}
	  endelse

        end

    ELSE: new_event = 0L

  ENDCASE

                ;**** Return the state   ****

          widget_control, parent, set_uvalue=state, /no_copy

  RETURN, new_event
END

;-----------------------------------------------------------------------------

FUNCTION cw_adas211_out, parent, dsfull, VALUE=value, UVALUE=uvalue, 	$
                         FONT=font

  IF (N_PARAMS() EQ 0) THEN MESSAGE, 'Must specify parent for cw_adas211_out'
  ON_ERROR, 2					;return to caller

		;**** Set defaults for keywords ****

  IF NOT (KEYWORD_SET(uvalue)) THEN uvalue = 0
  IF NOT (KEYWORD_SET(font)) THEN font = ''
  IF NOT (KEYWORD_SET(value)) THEN begin
	os = {out211_set, 						$
			CONOUT:0, CONAPP:0, 				$
			CONREP:0, CONDSN:'', 				$
                        CONDEF:'adas211_adf08.pass', CONMES:'', 	$
			PASOUT:0, PASAPP:0,				$
			PASREP:0, PASDSN:'',				$
			PASDEF:'adas211_adf04.pass', PASMES:'',		$
			TEXOUT:0, TEXAPP:0,				$
			TEXREP:0, TEXDSN:'',				$
			TEXDEF:'paper.txt', TEXMES:'',TITLE:''	$
              }
  END ELSE BEGIN
	os = {out211_set, 						$
			CONOUT:value.conout, CONAPP:value.conapp, 	$
			CONREP:value.conrep, CONDSN:value.condsn, 	$
			CONDEF:value.condef, CONMES:value.conmes, 	$
			PASOUT:value.pasout, PASAPP:value.pasapp,       $
                        PASREP:value.pasrep, PASDSN:value.pasdsn,       $
                        PASDEF:value.pasdef, PASMES:value.pasmes,	$
			TEXOUT:value.texout, TEXAPP:value.texapp,       $
                        TEXREP:value.texrep, TEXDSN:value.texdsn,       $
                        TEXDEF:value.texdef, TEXMES:value.texmes,	$
			TITLE:value.title				$
	      }
  END

		;**********************************************
		;**** Create the 211 Output options widget ****
		;**********************************************

		;**** create base widget ****

  cwid = widget_base( parent, UVALUE = uvalue, 				$
		      EVENT_FUNC = "out211_event", 			$
		      FUNC_GET_VALUE = "out211_get_val", 		$
		      /COLUMN)

		;**** Add dataset name and browse button ****

  rc = cw_adas_dsbr(cwid, dsfull, font=font)

		;***********************
		;**** add run title ****
		;***********************

  base = widget_base(cwid, /row)
  rc = widget_label(base, value='Title for Run', font=font)
  titid = widget_text(base, value=value.title, 				$
		font=font, /edit)

                ;**** Widget for adf04 data output ****

  outfval = { OUTBUT:os.pasout, APPBUT:os.pasapp, REPBUT:os.pasrep, 	$
              FILENAME:os.pasdsn, DEFNAME:os.pasdef, MESSAGE:os.pasmes }
  base1 = widget_base(cwid, /row, /frame)
  passid = cw_adas_outfile(base1, OUTPUT='Specific Ion Output',	$
                           VALUE=outfval, FONT=font)

		;**** Widget for adf08 data output ****

  outfval = { OUTBUT:os.conout, APPBUT:os.conapp, REPBUT:os.conrep, 	$
	      FILENAME:os.condsn, DEFNAME:os.condef, MESSAGE:os.conmes }
  base = widget_base(cwid, /row, /frame)
  paperid = cw_adas_outfile(base, OUTPUT='Resolved Recombination Output',$
                            VALUE=outfval, FONT=font)

		;**** Widget for paper.txt output ****

  outfval = { OUTBUT:os.texout, APPBUT:os.texapp, REPBUT:os.texrep, 	$
	      FILENAME:os.texdsn, DEFNAME:os.texdef, MESSAGE:os.texmes }
  base = widget_base(cwid, /row, /frame)
  textid = cw_adas_outfile(base, OUTPUT='Text output', 		$
                            VALUE=outfval, FONT=font)

		;**** Error message ****

  messid = widget_label(cwid, value=' ', font=font)

		;**** add the exit buttons ****

  base = widget_base(cwid, /row)
  cancelid = widget_button(base, value='Cancel', font=font)
  runid = widget_button(base, value='Run Now', font=font)
  batid = widget_button(base, value='Run in Batch', font=font)
  
		;*************************************************
		;**** create a state structure for the pop-up ****
		;**** window.                                 ****
		;*************************************************

  new_state = { PAPERID:paperid,  PASSID:passid, BATID:batid, TEXTID:textid,$
		CANCELID:cancelid, RUNID:runid, MESSID:messid, TITID:titid}

                ;**** Save initial state structure ****

  widget_control, parent, set_uvalue=new_state, /no_copy
  RETURN, cwid

END

