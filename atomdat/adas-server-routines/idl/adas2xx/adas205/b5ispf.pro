; Copyright (c) 1995 Strathclyde University .
; SCCS info: Module @(#)$Header: /home/adascvs/idl/adas2xx/adas205/b5ispf.pro,v 1.8 2004/07/06 11:23:50 whitefor Exp $ Date $Date: 2004/07/06 11:23:50 $
;+
; PROJECT:
;       ADAS IBM MVS to DEC UNIX conversion
;
; NAME:
;	B5ISPF
;
; PURPOSE:
;	IDL user interface and communications with ADAS205 FORTRAN
;	process via pipe.
;
; EXPLANATION:
;	The routine begins by reading information from the ADAS205
;	FORTRAN process via a UNIX pipe.  Then part of the ADAS205
;	IDL user interface is invoked to determine how the user
;	wishes to process the input dataset.  When the user's
;	interactions are complete the information gathered with
;	the user interface is written back to the FORTRAN process
;	via the pipe.  Communications are to the FORTRAN subroutine
;	B5ISPF.
;
; USE:
;	The use of this routine is specific to ADAS205, see adas205.pro.
;
; INPUTS:
;	PIPE	- The IDL unit number of the bi-directional pipe to the
;		  ADAS205 FORTRAN process.
;
;	LPEND	- Integer, 0 if the user pressed the 'Done' button
;		  or 1 if the user pressed 'Cancel'.  Maps directly onto
;		  the logical variable LPEND in ADAS205 FORTRAN.
;
;	VALUE	- A structure which determines the initial settings of
;		  the processing options widget.  The initial value is
;		  set in adas205.pro.  If adas205.pro passes a blank 
;		  dummy structure of the form {NMET:-1} into VALUE then
;		  VALUE is reset to a default structure.
;
;		  The VALUE structure is;
;		     {  TITLE:'',	NMET:0, $
;			IMETR:intarr(),	IFOUT:0, $
;			MAXT:0,		TINE:dblarr(), $
;			TINP:dblarr(),	TINH:dblarr(), $
;			IDOUT:1,	MAXD:0, $
;			DINE:dblarr(),	DINP:dblarr(), $
;			RATHA:dblarr(),	RATIA:dblarr(), $
;			ZEFF:'',	LPSEL:0, $
;			LZSEL:0,	LISEL:0, $
;			LHSEL:0,	LRSEL:0  }
;
;		  See cw_adas205_proc.pro for a full description of this
;		  structure and related variables ndmet, ndtem and ndden.
;
;	SZ	- String, recombined ion charge read.
;
;	SZ0	- String, nuclear charge read.
;
;	STRGA	- String array, level designations.
;
;	DSNINC	- String; The full system file name of the input COPASE
;		  dataset selected by the user for processing.
;
;       BITFILE - String; the path to the dirctory containing bitmaps
;                 for the 'escape to series menu' button.
;
; OPTIONAL INPUTS:
;	None
;
; OUTPUTS:
;	VALUE	- On output the structure records the final settings of
;		  the processing selections widget if the user pressed the
;		  'Done' button, otherwise it is not changed from input.
;
;       GOMENU  - Int; flag - set to 1 if user has selected 'escape direct
;                 to series menu , otherwise 0
;
; OPTIONAL OUTPUTS:
;	None
;
; KEYWORD PARAMETERS:
;	FONT_LARGE - The name of a larger font.
;
;	FONT_SMALL - The name of a smaller font.
;
;	EDIT_FONTS - A structure of two fonts used in the table editor
;		     adas_edtab.pro, {font_norm:'',font_input:''}.
;
; CALLS:
;	ADAS205_PROC	Invoke the IDL interface for ADAS205 data
;		 	processing options/input.
;
; SIDE EFFECTS:
;	Two way communications with ADAS205 FORTRAN via the
;	bi-directional UNIX pipe.
;
; CATEGORY:
;	Adas system.
;
; WRITTEN:
;       Andrew Bowen, Tessella Support Services plc, 6-Apr-1993
;
; MODIFIED:
;       1.0	Andrew Bowen
;               First release.
;
;       1.7     Tim Hammond
;               Changed array tscef to (14,3) in line with other
;               routines.
;	1.8	William Osborn
;		Added menu button code
;
; VERSION:
;       1.0	27-05-93
;
;       1.7     07-04-95
;       1.8     04-06-96
;-
;-----------------------------------------------------------------------------


PRO b5ispf, pipe, lpend, value, $
		sz, sz0, strga, $
		dsninc, bitfile, gomenu,	$
		FONT_LARGE=font_large, FONT_SMALL=font_small, $
		EDIT_FONTS=edit_fonts


                ;**** Set defaults for keywords ****
  IF NOT (KEYWORD_SET(font_large)) THEN font_large = ''
  IF NOT (KEYWORD_SET(font_small)) THEN font_small = ''
  IF NOT (KEYWORD_SET(edit_fonts)) THEN $
                                edit_fonts = {font_norm:'',font_input:''}


		;**** Declare variables for input ****
  lpend = 0
  ndtem = 0
  ndden = 0
  ndmet = 0
  il = 0
  nv = 0
  tscef = dblarr(14,3)

		;********************************
		;**** Read data from fortran ****
		;********************************
  readf,pipe,lpend
  readf,pipe,ndtem
  readf,pipe,ndden
  readf,pipe,ndmet
  readf,pipe,il
  readf,pipe,nv
  readf,pipe,tscef

		;*******************************************
		;**** Set default value if non provided ****
		;*******************************************
  if value.nmet lt 0 then begin
    imetr = make_array(ndmet,/int,value=0)
    temp = dblarr(ndtem)
    dens = dblarr(ndden)
    value = {	TITLE:'',       NMET:0, $
		IMETR:imetr,    IFOUT:1, $
		MAXT:0,         TINE:temp, $
		TINP:temp,      TINH:temp, $
		IDOUT:1,        MAXD:0, $
		DINE:dens,      DINP:dens, $
		RATHA:dens,     RATIA:dens, $
		ZEFF:'',        LPSEL:0, $
		LZSEL:0,        LISEL:0, $
		LHSEL:0,        LRSEL:0  }
  end

		;****************************************
		;**** Pop-up processing input widget ****
		;****************************************
  adas205_proc, value, sz, sz0, strga, dsninc, $
		ndtem, ndden, ndmet, il, nv, tscef, action, bitfile, $
		FONT_LARGE=font_large, FONT_SMALL=font_small, $
		EDIT_FONTS=edit_fonts


		;******************************************
		;**** Act on the event from the widget ****
		;******************************************
		;**** There are only two possible events ****
		;**** 'Done' and 'Cancel'.               ****
		;********************************************
  gomenu = 0
  if action eq 'Done' then begin
    lpend = 0
  endif else if action eq 'Menu' then begin
    lpend = 0
    gomenu = 1
  endif else begin
    lpend = 1
  end

		;*******************************
		;**** Write data to fortran ****
		;*******************************
  printf,pipe,lpend
  printf,pipe,value.title
  printf,pipe,value.nmet
  printf,pipe,value.imetr
  printf,pipe,value.ifout
  printf,pipe,value.maxt
  printf,pipe,value.tine
  printf,pipe,value.tinp
  printf,pipe,value.tinh
  printf,pipe,value.idout
  printf,pipe,value.maxd
  printf,pipe,value.dine
  printf,pipe,value.dinp
  printf,pipe,value.ratha
  printf,pipe,value.ratia
  if value.lzsel eq 1 then begin
    zeff = float(value.zeff)
  end else begin
    zeff = 0.0
  end
  printf,pipe,zeff
  printf,pipe,value.lpsel
  printf,pipe,value.lzsel
  printf,pipe,value.lisel
  printf,pipe,value.lhsel
  printf,pipe,value.lrsel


END
