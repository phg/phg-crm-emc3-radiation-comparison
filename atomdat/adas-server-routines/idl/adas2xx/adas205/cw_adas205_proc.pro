; Copyright (c) 1995 Strathclyde University .
; SCCS info: Module @(#)$Header: /home/adascvs/idl/adas2xx/adas205/cw_adas205_proc.pro,v 1.18 2004/07/06 12:31:45 whitefor Exp $ Date $Date: 2004/07/06 12:31:45 $
;+
; PROJECT:
;       ADAS IBM MVS to DEC UNIX conversion
;
; NAME:
;	CW_ADAS205_PROC()
;
; PURPOSE:
;	Produces a widget for ADAS205 processing options/input.
;
; EXPLANATION:
;	This function creates a compound widget consisting of a text
;	widget holding an editable 'Run title', the dataset name/browse
;	widget cw_adas_dsbr, label widgets with charge information for
;	the input data, a table widget for temperature data cw_adas_table,
;	a second cw_adas_table widget for density data, buttons to
;	enter default values into the temperature and density tables,
;	a multiple selection widget cw_adas_sel, a reaction settings
;	widget cw_adas_reac2, a message widget, a 'Cancel' button and
;	finally a 'Done' button.
;
;	The compound widgets included in this widget are self managing.
;	This widget only manages events from the two 'Defaults' buttons,
;	the 'Cancel' button and the 'Done' button.
;
;	This widget only generates events for the 'Done' and 'Cancel'
;	buttons.
;
; USE:
;	This widget is specific to ADAS205, see adas205_proc.pro
;	for use.
;
; INPUTS:
;	TOPPARENT- Long integer, ID of parent widget.
;
;	SZ	- String, recombined ion charge read.
;
;	SZ0	- String, nuclear charge read.
;
;	STRGA	- String array, level designations.
;
;	DSNINC	- String; The full system file name of the input COPASE
;		  dataset selected by the user for processing.
;
;	NDTEM	- Integer; Maximum number of temperatures allowed.
;
;	NDDEN	- Integer; Maximum number of densities allowed.
;
;	NDMET	- Integer; Maximum number of metastables allowed.
;
;	IL	- Integer; Number of energy levels.
;
;	NV	- Integer; Number of termperatures.
;
;	TSCEF	- dblarr(8,3); Input electron temperatures in three units.
;
;	The inputs SZ to TSCEF map exactly onto variables of the same
;	name in the ADAS205 FORTRAN program.
;
;       BITFILE - String; the path to the dirctory containing bitmaps
;                 for the 'escape to series menu' button.
;
; OPTIONAL INPUTS:
;	None
;
; OUTPUTS:
;	The return value of this function is the ID of the declared widget.
;
; OPTIONAL OUTPUTS:
;	None
;
; KEYWORD PARAMETERS:
;	VALUE	- A structure which determines the initial settings of
;		  the processing options widget.
;
;		  The default VALUE is created thus;
;
;			imetr = make_array(ndmet,/int,value=0)
;			temp = dblarr(ndtem)
;			dens = dblarr(ndden)
;			ps = { $
;		 	TITLE:'',	NMET:0, $
;			IMETR:imetr,	IFOUT:1, $
;			MAXT:0,		TINE:temp, $
;			TINP:temp,	TINH:temp, $
;			IDOUT:1,	MAXD:0, $
;			DINE:dens,	DINP:dens, $
;			RATHA:dens,	RATIA:dens, $
;			ZEFF:'',	LPSEL:0, $
;			LZSEL:0,	LISEL:0, $
;			LHSEL:0,	LRSEL:0  }
;
; 		TITLE	entered general title for program run
; 		NMET	number of metastables
; 		(IMETR(I),I=1,NMET)	index of metastables
; 		IFOUT	input temperature units (1,2,3)
; 		MAXT	number of input temperatures (1-20)
; 		(TINE(I),I=1,MAXT)	electron temperatures
; 		(TINP(I),I=1,MAXT)	proton temperatures
; 		(TINH(I),I=1,MAXT)	neutral hydrogen temperatures
; 		IDOUT	input density units (1,2)
; 		MAXD	number of input densities
; 		(DINE(I),I=1,MAXD)	electron densities
; 		(DINP(I),I=1,MAXD)	proton densities
; 		(RATHA(I),I=1,MAXD)	ratio (neut h dens/elec dens)
; 		(RATIA(I),I=1,MAXD)	ratio (n(z+1)/n(z) stage abund)
; 		ZEFF	plasma z effective, a string holding a valid number.
; 		LPSEL	include proton collisions?
; 		LZSEL	scale proton collisions with plasma z effective?
; 		LISEL	include ionisation rates?
; 		LHSEL	include charge transfer from neutral hydrogen?
; 		LRSEL	include free electron recombination?
;
;		All of these structure elements map onto variables of
;		the same name in the ADAS205 FORTRAN program.
;
;
;	UVALUE	- A user value for the widget. Default 0.
;
;	FONT_LARGE - The name of a larger font.  Default current system
;		     font
;
;	FONT_SMALL - The name of a smaller font. Default current system
;		     font.
;
;	EDIT_FONTS - A structure of two fonts used in the table editor
;		     adas_edtab.pro, {font_norm:'',font_input:''}.  Defaults
;		     to current system font.
;
;	NUM_FORM   - String; Numeric format to use in tables.  Default
;			'(E10.3)'
;		     
;
; CALLS:
;	POPUP		Popup warning window with buttons.
;	XXDEFT		Returns default temperature table.
;	XXDEFD		Returns default density table.
;	CW_ADAS_DSBR	Dataset name and comments browsing button.
;	CW_ADAS_TABLE	Adas data table widget.
;	CW_ADAS_SEL	Adas mulitple selection widget.
;	CW_ADAS_REAC2	Reaction settings for ADAS205/206.
;	CW_SAVESTATE	Widget management routine.
;	CW_LOADSTATE	Widget management routine.
;
;	See side effects for other related widget management routines.
;
; SIDE EFFECTS:
;       A COMMON BLOCK: CW_PROC205_BLK is used which is private to
;       this widget.
;
;	The following widget management routines are included in this
;	file;
;	PROC205_GET_VAL()	Returns the current VALUE structure.
;	PROC205_EVENT()		Process and issue events.
;
; CATEGORY:
;	Compound Widget
;
; WRITTEN:
;       Andrew Bowen, Tessella Support Services plc, 6-Apr-1993
;
; MODIFIED:
;       Version 1       Andrew Bowen    27-May-1993
;                       First release.
;	Version 1.1	Andrew Bowen	11-Oct-1993
;			Call to cw_adas_sel modified to include new
;			keyword OREDERED.
;	1.14		William Osborn
;			Changed maxt=state.ndden to maxt=state.ndtem
;	1.15		William Osborn
;			S.C.C.S. mistake
;	1.16		William Osborn
;			Added menu button code
;	1.17		Richard Martin
;			Remove obsolete cw_loadstate/savestate
;       1.18    Martin O'Mullane
;                - Change the metastable selection to not pop up
;                  a selection box which is better for large 
;                  adf04 files.
;
; VERSION:
;       1.1	11-Oct-1993
;	1.14	09-05-96
;	1.15	09-05-96
;	1.16	04-06-96
;	1.17	06-03-02
;       1.18	29-04-2003
;
;-
;-----------------------------------------------------------------------------

FUNCTION proc205_get_val, id


                ;**** Return to caller on error ****
  ON_ERROR, 2

                ;**** Retrieve the state ****
  first_child = widget_info(id,/child)
  widget_control, first_child, get_uvalue=state, /no_copy

		;***********************************
		;**** Get run title from widget ****
		;***********************************
  widget_control,state.runid,get_value=title

		;**************************************************
		;**** Get new selections from selection widget ****
		;**************************************************
  widget_control,state.selid,get_value=selected
  ind  = where(selected EQ 1, count)
  selected = intarr(state.ndmet)         
  selected[0:count-1] = ind + 1   
  
		;**** selected is IDL index, from 0. imetr needs ****
		;**** to be fortran index, from 1.               ****
  
  imetr = selected 
  sels  = where(imetr gt 0)
  if sels(0) gt -1 then begin
    selsize = size(sels)
    nmet = selsize(1)
  end else begin
    nmet = 0
  end

		;****************************************************
		;**** Get new temperature data from table widget ****
		;****************************************************
  widget_control,state.tempid,get_value=tempval
  tempdata = tempval.value
  ifout = tempval.units+1

		;**** Copy out temperature values ****
  blanks = where(strtrim(tempdata(0,*),2) eq '')
  if blanks(0) ge 0 then maxt=blanks(0) else maxt=state.ndtem
  tine = dblarr(state.ndtem)
  tinp = dblarr(state.ndtem)
  tinh = dblarr(state.ndtem)
  if maxt gt 0 then begin
    tine(0:maxt-1) = double(tempdata(0,0:maxt-1))
    tinp(0:maxt-1) = double(tempdata(1,0:maxt-1))
    tinh(0:maxt-1) = double(tempdata(2,0:maxt-1))
  end

		;**** Fill out the rest with zero ****
  if maxt lt state.ndtem then begin
    tine(maxt:state.ndtem-1) = double(0.0)
    tinp(maxt:state.ndtem-1) = double(0.0)
    tinh(maxt:state.ndtem-1) = double(0.0)
  end

		;************************************************
		;**** Get new density data from table widget ****
		;************************************************
  widget_control,state.densid,get_value=densval
  densdata = densval.value
  idout = densval.units+1

		;**** Copy out density values ****
  blanks = where(strtrim(densdata(0,*),2) eq '')
  if blanks(0) ge 0 then maxd=blanks(0) else maxd=state.ndden
  dine = dblarr(state.ndden)
  dinp = dblarr(state.ndden)
  ratha = dblarr(state.ndden)
  ratia = dblarr(state.ndden)
  if maxd gt 0 then begin
    dine(0:maxd-1) = double(densdata(0,0:maxd-1))
    dinp(0:maxd-1) = double(densdata(1,0:maxd-1))
    ratha(0:maxd-1) = double(densdata(2,0:maxd-1))
    ratia(0:maxd-1) = double(densdata(3,0:maxd-1))
  end

		;**** Fill out the rest with blanks ****
  if maxd lt state.ndden then begin
    dine(maxd:state.ndden-1) = double(0.0)
    dinp(maxd:state.ndden-1) = double(0.0)
    ratha(maxd:state.ndden-1) = double(0.0)
    ratia(maxd:state.ndden-1) = double(0.0)
  end

		;*************************************************
		;**** Get new reaction selections from widget ****
		;*************************************************
  widget_control,state.reacid,get_value=reacset
  zeff = reacset.zeff
  lpsel = reacset.lpsel
  lzsel = reacset.lzsel
  lisel = reacset.lisel
  lhsel = reacset.lhsel
  lrsel = reacset.lrsel

  ps = {proc205_set, $
		 	TITLE:title(0),	NMET:nmet, $
			IMETR:imetr,	IFOUT:ifout, $
			MAXT:maxt,	TINE:tine, $
			TINP:tinp,	TINH:tinh, $
			IDOUT:idout,	MAXD:maxd, $
			DINE:dine,	DINP:dinp, $
			RATHA:ratha,	RATIA:ratia, $
			ZEFF:zeff,	LPSEL:lpsel, $
			LZSEL:lzsel,	LISEL:lisel, $
			LHSEL:lhsel,	LRSEL:lrsel }

  widget_control, first_child, set_uvalue=state, /no_copy

  RETURN, ps

END

;-----------------------------------------------------------------------------

FUNCTION proc205_event, event


                ;**** Base ID of compound widget ****
  parent=event.handler

                ;**** Retrieve the state ****

  first_child = widget_info(parent,/child)
  widget_control, first_child, get_uvalue=state

		;*********************************
		;**** Clear previous messages ****
		;*********************************
  widget_control,state.messid,set_value=' '

                ;************************
                ;**** Process events ****
                ;************************
  CASE event.id OF

		;*************************************
		;**** Default temperatures button ****
		;*************************************
    state.deftid: begin

		;**** popup window to confirm overwriting current values ****
	action= popup(message='Confirm Overwrite Temperatures with Defaults', $
			buttons=['Confirm','Cancel'],font=state.font)

	if action eq 'Confirm' then begin
		;**** Get current table widget value ****
 	  widget_control,state.tempid,get_value=tempval

		;**** Get the default temperatures and set new units ****
 	  xxdeft, state.ndtem, maxt, deft
	  tempval.units = 2

		;**** Copy defaults into value structure ****
 	  if maxt gt 0 then begin
   	    tempval.value(0,0:maxt-1) = $
		strtrim(string(deft(0:maxt-1),format=state.num_form),2)
   	    tempval.value(1,0:maxt-1) = $
		strtrim(string(deft(0:maxt-1),format=state.num_form),2)
   	    tempval.value(2,0:maxt-1) = $
		strtrim(string(deft(0:maxt-1),format=state.num_form),2)
 	  end

		;**** Fill out the rest with blanks ****
 	  if maxt lt state.ndtem then begin
   	    tempval.value(0:2,maxt:state.ndtem-1) = ''
 	  end

		;**** Copy new temperature data to table widget ****
 	  widget_control,state.tempid,set_value=tempval
	end

      end

		;**********************************
		;**** Default densities button ****
		;**********************************
    state.defdid: begin

		;**** popup window to confirm overwriting current values ****
	action = popup(message='Confirm Overwrite Densities with Defaults', $
			buttons=['Confirm','Cancel'],font=state.font)

	if action eq 'Confirm' then begin
		;**** Get current table widget value ****
 	  widget_control,state.densid,get_value=densval

		;**** Get the default densities and set new units ****
 	  xxdefd, state.ndden, maxd, defd
	  densval.units = 1

		;**** Copy defaults into value structure ****
 	  if maxd gt 0 then begin
   	    densval.value(0,0:maxd-1) = $
		strtrim(string(defd(0:maxd-1),format=state.num_form),2)
   	    densval.value(1:3,0:maxd-1) = '0.0000'
 	  end

		;**** Fill out the rest with blanks ****
 	  if maxd lt state.ndden then begin
   	    densval.value(*,maxd:state.ndden-1) = ''
 	  end

		;**** Copy new temperature data to table widget ****
 	  widget_control,state.densid,set_value=densval

	end

      end

		;*************************************
		;**** Metastable selection widget ****
		;*************************************

    state.selid: begin

	widget_control, state.selid, get_value=selected
	sels = where(selected eq 1, count)
        
        ; only permit ndmet
        
        if count GT state.ndmet then begin
           
           str = 'A maximum of ' + strtrim(string(state.ndmet),2) + $
                 ' metastables are permitted'
           widget_control, state.messID, set_value=str
           widget_control, state.selID, get_value=sels
           sels[event.value] = 0
           widget_control, state.selID, set_value=sels 
        endif
    
    end

		;***********************
		;**** Cancel button ****
		;***********************
    state.cancelid: new_event = {ID:parent, TOP:event.top, $
				HANDLER:0L, ACTION:'Cancel'}

		;*********************
		;**** Done button ****
		;*********************
    state.doneid: begin
			;***************************************
			;**** Check all user input is legal ****
			;***************************************
	error = 0
	widget_control,event.handler,get_value=ps

		;**** check metastable states selected ****
	if error eq 0 and ps.nmet eq 0 then begin
	  error = 1
	  message='Error: No Metastable States selected.'
        end

		;**** check temperatures entered ****
	if error eq 0 and ps.maxt eq 0 then begin
	  error = 1
	  message='Error: No temperatures entered.'
        end

		;**** check densities entered ****
	if error eq 0 and ps.maxd eq 0 then begin
	  error = 1
	  message='Error: No densities entered.'
        end

		;**** check zeff value ****
	if error eq 0 and ps.lzsel eq 1 then begin
	  error = num_chk(ps.zeff)
	  if error gt 0 then message='Error: Z-effective should be a number.'
        end

		;**** return value or flag error ****
	if error eq 0 then begin
	  new_event = {ID:parent, TOP:event.top, HANDLER:0L, ACTION:'Done'}
        end else begin
	  widget_control,state.messid,set_value=message
	  new_event = 0L
        end

      end

                ;**** Menu button ****

      state.outid: begin
          new_event = {ID:parent, TOP:event.top, HANDLER:0L,          $
                       ACTION:'Menu'}
      end

    ELSE: new_event = 0L

  ENDCASE

  RETURN, new_event
END

;-----------------------------------------------------------------------------

FUNCTION cw_adas205_proc, topparent, $
		sz, sz0, strga, dsninc, ndtem, ndden, ndmet, il, nv, tscef, $
		bitfile, $
		VALUE=value, UVALUE=uvalue, $
		FONT_LARGE=font_large, FONT_SMALL=font_small, $
		EDIT_FONTS=edit_fonts, NUM_FORM=num_form


		;**** Set defaults for keywords ****
  IF NOT (KEYWORD_SET(uvalue)) THEN uvalue = 0
  IF NOT (KEYWORD_SET(font_large)) THEN font_large = ''
  IF NOT (KEYWORD_SET(font_small)) THEN font_small = ''
  IF NOT (KEYWORD_SET(edit_fonts)) THEN $
				edit_fonts = {font_norm:'',font_input:''}
  IF NOT (KEYWORD_SET(num_form)) THEN num_form = '(E10.3)'

  IF NOT (KEYWORD_SET(value)) THEN begin
	imetr = make_array(ndmet,/int,value=0)
	temp = dblarr(ndtem)
	dens = dblarr(ndden)
	ps = {proc205_set, $
		 	TITLE:'',	NMET:0, $
			IMETR:imetr,	IFOUT:1, $
			MAXT:0,		TINE:temp, $
			TINP:temp,	TINH:temp, $
			IDOUT:1,	MAXD:0, $
			DINE:dens,	DINP:dens, $
			RATHA:dens,	RATIA:dens, $
			ZEFF:'',	LPSEL:0, $
			LZSEL:0,	LISEL:0, $
			LHSEL:0,	LRSEL:0  }
  END ELSE BEGIN
	ps = {proc205_set, $
		 	TITLE:value.title,	NMET:value.nmet, $
			IMETR:value.imetr,	IFOUT:value.ifout, $
			MAXT:value.maxt,	TINE:value.tine, $
			TINP:value.tinp,	TINH:value.tinh, $
			IDOUT:value.idout,	MAXD:value.maxd, $
			DINE:value.dine,	DINP:value.dinp, $
			RATHA:value.ratha,	RATIA:value.ratia, $
			ZEFF:value.zeff,	LPSEL:value.lpsel, $
			LZSEL:value.lzsel,	LISEL:value.lisel, $
			LHSEL:value.lhsel,	LRSEL:value.lrsel }
  END


		;*****************************************
		;**** Assemble temperature table data ****
		;*****************************************
		;**** The adas table widget requires data to be ****
		;**** input as strings, so all the numeric data ****
		;**** has to be written into a string array.    ****
		;**** Declare temp table array ****
		;**** Three editable columns and input values ****
		;**** in three different units.               ****
  tempdata = strarr(6,ndtem)

		;**** Copy out temperature values ****
  if ps.maxt gt 0 then begin
    tempdata(0,0:ps.maxt-1) = $
		strtrim(string(ps.tine(0:ps.maxt-1),format=num_form),2)
    tempdata(1,0:ps.maxt-1) = $
		strtrim(string(ps.tinp(0:ps.maxt-1),format=num_form),2)
    tempdata(2,0:ps.maxt-1) = $
		strtrim(string(ps.tinh(0:ps.maxt-1),format=num_form),2)
  end
		;**** input values in three units ****
  tempdata(3,0:nv-1) = strtrim(string(tscef(0:nv-1,0),format=num_form),2)
  tempdata(4,0:nv-1) = strtrim(string(tscef(0:nv-1,1),format=num_form),2)
  tempdata(5,0:nv-1) = strtrim(string(tscef(0:nv-1,2),format=num_form),2)

		;**** Fill out the rest with blanks ****
  if ps.maxt lt ndtem then begin
    tempdata(0:2,ps.maxt:ndtem-1) = ''
  end
  tempdata(3:5,nv:ndtem-1) = ''

		;*************************************
		;**** Assemble density table data ****
		;*************************************
		;**** The adas table widget requires data to be ****
		;**** input as strings, so all the numeric data ****
		;**** has to be written into a string array.    ****
		;**** Declare dens table array ****
  densdata = strarr(4,ndden)

		;**** Copy out temperature values ****
  if ps.maxd gt 0 then begin
    densdata(0,0:ps.maxd-1) = $
		strtrim(string(ps.dine(0:ps.maxd-1),format=num_form),2)
    densdata(1,0:ps.maxd-1) = $
		strtrim(string(ps.dinp(0:ps.maxd-1),format=num_form),2)
    densdata(2,0:ps.maxd-1) = $
		strtrim(string(ps.ratha(0:ps.maxd-1),format=num_form),2)
    densdata(3,0:ps.maxd-1) = $
		strtrim(string(ps.ratia(0:ps.maxd-1),format=num_form),2)
  end

		;**** Fill out the rest with blanks ****
  if ps.maxd lt ndden then begin
    densdata(*,ps.maxd:ndden-1) = ''
  end

		;********************************************************
		;**** Create the 205 Processing options/input window ****
		;********************************************************

		;**** create titled base widget ****
  parent = widget_base(topparent, UVALUE = uvalue, $
			EVENT_FUNC = "proc205_event", $
			FUNC_GET_VALUE = "proc205_get_val", $
			/COLUMN)

		;**** Create base to hold the value of state ****

  first_child = widget_base(parent,/COLUMN)

  topbase = widget_base(first_child,/column)

		;**** add run title ****
  base = widget_base(topbase,/row)
  rc = widget_label(base,value='Title for Run',font=font_large)
  runid = widget_text(base,value=ps.title,xsize=40,font=font_large,/edit)

		;**** add dataset name and browse button ****
  rc = cw_adas_dsbr(topbase,dsninc,font=font_large)

		;**** add charge information ****
  rc = widget_label(topbase,font=font_large, $
			value='Nuclear Charge:'+sz0+'  Ion Charge:'+sz)

		;**** Another base ****
  tablebase = widget_base(parent,/row)

		;**** base for the temperature table and defaults button ****
  base = widget_base(tablebase,/column,/frame)

		;**** temperature data table ****
  colhead = [['Electron','Ion','Neutral','Input'], $
	     ['','','Hydrogen','Value']]
		;**** convert FORTRAN index to IDL index ****
  units = ps.ifout-1
  unitname = ['Kelvin','eV','Reduced']
		;**** Four columns in the table and three sets of units. ****
		;**** Columns 1,2 & 3 have the same values for all three ****
		;**** units but column 4 switches between sets 3, 4 & 5 ****
		;**** in the input array tempdata as the units change.  ****
  unitind = [[0,0,0],[1,1,1],[2,2,2],[3,4,5]]
  tempid = cw_adas_table(base, tempdata, units, unitname, unitind, $
			UNITSTITLE = 'Temperature Units', /scroll, $
			COLEDIT = [1,1,1,0], COLHEAD = colhead, $
			TITLE = 'Temperatures', ORDER = [1,0,0,0], $
			LIMITS = [2,2,2,2], CELLSIZE = 12, $
			FONTS = edit_fonts, FONT_LARGE = font_large, $
			FONT_SMALL = font_small)

		;**** Default temperatures button ****
  base = widget_base(base,/column)
  deftid = widget_button(base,value='Default Temperatures',font=font_large)

		;**** base for the density table and defaults button ****
  base = widget_base(tablebase,/column,/frame)

		;**** density data table ****
  colhead = [['Electron', 'Ion',      'NH/NE','N(Z1)/N(Z)'], $
	     ['Densities','Densities','Ratio','Ratio'     ]]
		;**** convert FORTRAN index to IDL index ****
  units = ps.idout-1
  unitname = ['cm-3','Reduced']
		;**** Four columns in the table and two sets of units. ****
		;**** The values do not change when the units are switched ****
  unitind = [[0,0],[1,1],[2,2],[3,3]]
  densid = cw_adas_table(base, densdata, units, unitname, unitind, $
			UNITSTITLE = 'Density Units', $
			COLHEAD = colhead, /scroll, $
			TITLE = 'Densities', ORDER = [1,0,0,0], $
			LIMITS = [2,1,1,1], CELLSIZE = 12, $
			FONTS = edit_fonts, FONT_LARGE = font_large, $
			FONT_SMALL = font_small)

		;**** Default densities button ****
  base = widget_base(base,/column)
  defdid = widget_button(base,value='Default Densities',font=font_large)

  botbase = widget_base(parent,/row)
  base = widget_base(botbase,/row,/frame)

		;**** metastables selection list ****
		;**** imetr is fortran index, from 1, selected ****
		;**** needs to be IDL index, from 0.	       ****
;   selected = ps.imetr - 1
;   levend = where(strtrim(strga,2) eq '*** END OF LEVELS ***')
;   listsize = size(strga)
;   if levend(0) ge 0 then listend=levend(0)-1 else listend=listsize(1)-1
;   selid = cw_adas_sel(base,strga(0:listend),SELECTED=selected, $
; 			MAXSEL=ndmet, /ORDERED, TITLE='Metastable States', $
; 			FONT=font_large)


  ; Metastable selection list

  mbase =  widget_base(base,/column,/frame)
  
  slen  = max(strlen(strga))
  longS = ''
  for j=0, slen-1+4 do longS = longS + 'H'
 
  bd = widget_base(map=0)
  lb = widget_label(bd, /dynamic_resize, value='',font=font_large)
  widget_control, lb, set_value=longS
  wd = (widget_info(lb, /geometry)).scr_xsize  
  xs = fix(wd)

  levend = where(strtrim(strga,2) eq '*** END OF LEVELS ***')
  listsize = size(strga)
  if levend(0) ge 0 then listend=levend(0)-1 else listend=listsize(1)-1  
  buttons = strga(0:listend)
  
  tmp     =  widget_label(mbase,value='Metastable State', font=font_large)
  selID   =  cw_bgroup(mbase,buttons,                 $
                       nonexclusive=1,column=1,       $
                       set_value = set_button,        $
                       scroll=1, y_scroll_size=120,   $
                       xsize=xs,x_scroll_size=xs,     $
                       /frame, font=font_large    )
  met_sel = intarr(listend+1)
  met_sel(ps.imetr-1) = 1
  widget_control, selID, set_value=met_sel


		;**** Another base ****
  base = widget_base(botbase,/column,/frame)

		;**** Reaction selection ****
  reacset ={reacset, 	ZEFF:strtrim(string(ps.zeff),2), $
			LPSEL:ps.lpsel, LZSEL:ps.lzsel, $
			LISEL:ps.lisel, LHSEL:ps.lhsel, $
			LRSEL:ps.lrsel}
  reacid = cw_adas_reac2(base,VALUE=reacset,FONT=font_large)

		;**** Error message ****
  messid = widget_label(parent,font=font_large, $
	value='Edit the processing options data and press Done to proceed')

		;**** add the exit buttons ****
  base = widget_base(parent,/row)
  menufile = bitfile + '/menu.bmp'
  read_X11_bitmap, menufile, bitmap1
  outid = widget_button(base, value=bitmap1)          ;menu button
  cancelid = widget_button(base,value='Cancel',font=font_large)
  doneid = widget_button(base,value='Done',font=font_large)
  
		;**** create a state structure for the pop-up ****
		;**** window.                                 ****
  new_state = { RUNID:runid, OUTID:outid,			$
		SELID:selid, TEMPID:tempid, DENSID:densid, 	$
		DEFTID:deftid, DEFDID:defdid, 			$
		REACID:reacid, CANCELID:cancelid, DONEID:doneid,$
		MESSID:messid, NUM_FORM:num_form, 		$
		NDTEM:ndtem, NDDEN:ndden, NDMET:ndmet, 		$
		IL:il, NV:nv, TSCEF:tscef, FONT:font_large }

  widget_control, first_child, set_uvalue=new_state, /no_copy

  RETURN, parent

END

