; Copyright (c) 1995 Strathclyde University .
; SCCS info: Module @(#)$Header: /home/adascvs/idl/adas2xx/adas205/adas205.pro,v 1.34 2004/07/06 10:15:49 whitefor Exp $ Date $Date: 2004/07/06 10:15:49 $
;+
; PROJECT:
;       ADAS IBM MVS to DEC UNIX conversion
;
; NAME:
;	ADAS205
;
; PURPOSE:
;	The highest level routine for the ADAS 205 program.
;
; EXPLANATION:
;	This routine is called from the main adas system routine, adas.pro,
;	to start the ADAS 205 application.  Associated with adas205.pro
;	is a FORTRAN executable.  The IDL code provides the user
;	interface and output graphics whilst the FORTRAN code reads
;	in data files, performs numerical processing and creates the
;	output files.  The IDL code communicates with the FORTRAN
;	executable via a bi-directional UNIX pipe.  The unit number
;	used by the IDL for writing to and reading from this pipe is
;	allocated when the FORTRAN executable is 'spawned' (see code
;	below).  Pipe communications in the FORTRAN process are to
;	stdin and stdout, i.e streams 5 and 6.
;
;	The FORTRAN code is an independent process under the UNIX system.
;	The IDL process can only exert control over the FORTRAN in the
;	data which it communicates via the pipe.  The communications
;	between the IDL and FORTRAN must be exactly matched to avoid
;	input conversion errors.  The correct ammounts of data must be
;	passed so that neither process 'hangs' waiting for communications
;	which will never occur.
;
;	The FORTRAN code performs some error checking which is
;	independent of IDL.  In cases of error the FORTRAN may write
;	error messages.  To prevent these error messages from conflicting
;	with the pipe communications all FORTRAN errors are written to
;	output stream 0, which is stderr for UNIX.  These error messages
;	will appear in the window from which the ADAS session/IDL session
;	is being run.
;
;	In the case of severe errors the FORTRAN code may terminate
;	itself prematurely.  In order to detect this, and prevent the
;	IDL program from 'hanging' or crashing, the IDL checks to see
;	if the FORTRAN executable is still an active process before
;	each group of pipe communications.  The process identifier
;	for the FORTRAN process, PID, is recorded when the process is
;	first 'spawned'.  The system is then checked for the presence
;	of the FORTRAN PID.
;
; USE:
;	First the system settings must be established by calling
;	adas_sys_set.pro then adas205.pro is called to start the
;	ADAS 205 application;
;
;	adas_sys_set, adasrel, fortdir, userroot, centroot, $
;		      devlist, devcode, font_large, font_small, edit_fonts
;	adas205,   adasrel, fortdir, userroot, centroot, devlist, $
;		   devcode, font_large, font_small, edit_fonts
;
; INPUTS:
;	ADASREL - A string indicating the ADAS system version, 
;		  e.g ' ADAS RELEASE: ADAS93 V1.1.0'.  The first
;		  character should be a space.
;
;	FORTDIR - A string holding the path to the directory where the
;		  FORTRAN executables are, e.g '/disk/adas/fortran/exec'
;
;	USERROOT - A string holding the path to the root directory of
;		   the user's adas data. e.g '/disk/bowen/adas'
;		   This root directory will be used by adas to construct
;		   other path names.  For example the users default data
;		   for adas205 should be in /disk/bowen/adas/adf04.  In
;		   particular the user's default interface settings will
;		   be stored in the directory USERROOT+'/defaults'.  An
;		   error will occur if the defaults directory does not
;		   exist.
;
;	CENTROOT - Like USERROOT, but this directory points to the
;		   central data area for the system.  User defaults are
;		   not stored on CENTROOT.
;
;	DEVLIST - A string array of hardcopy device names, used for
;		  graphical output. e.g ['Post-Script','HP-PCL','HP-GL']
;		  This array must mirror DEVCODE.  DEVCODE holds the
;		  actual device names used in a SET_PLOT statement.
;
;	DEVCODE - A string array of hardcopy device code names used in
;		  the SET_PLOT statement, e.g ['PS', 'PCL', 'HP']
;		  This array must mirror DEVLIST.
;
;	FONT_LARGE - The name of a larger font e.g 'courier_bold14'
;
;	FONT_SMALL - The name of a smaller font e.g 'courier_bold12'
;
;	EDIT_FONTS - A structure of two fonts used in the table editor
;		     adas_edtab.pro, {font_norm:'',font_input:''}.
;		     The two fonts are used to differentiate between
;		     editable and non-editable parts of the table. You
;		     may have to experiment to get two fonts which give
;		     the same default spacing in the edit widget.
;		     e.g {font_norm:'helvetica_bold14', $
;			  font_input:'helvetica_oblique14'}
;
; OPTIONAL INPUTS:
;	None
;
; OUTPUTS:
;	None - Note this routine should leave all inputs un-modified.
;
; OPTIONAL OUTPUTS:
;	None
;
; KEYWORD PARAMETERS:
;	None
;
; CALLS:
;	FIND_PROCESS	Checks to see if a given process is active.
;	B5SPF0		Pipe comms with FORTRAN B5SPF0 routine.
;	BXSETP		Pipe comms with FORTRAN BXSETP routine.
;	B5ISPF		Pipe comms with FORTRAN B5ISPF routine.
;	XXDATE		Get date and time from operating system.
;	B5SPF1		Pipe comms with FORTRAN B5SPF1 routine.
;	BXOUTG		Pipe comms with FORTRAN BXOUTG routine.
;
; SIDE EFFECTS:
;	This routine spawns a FORTRAN executable.  Note the pipe 
;	communications routines listed above.  In addition to these
;	pipe communications there is one explicit communication of the
;	date to the FORTRAN code, search for 'printf,pipe' to find it.
;
; CATEGORY:
;	Adas system.
;	
; WRITTEN:
;       Andrew Bowen, Tessella Support Services plc, 20-Apr-1993
;
; MODIFIED:
;       1.1    	Andrew Bowen    
;              	First release.
;       1.2    	Andrew Bowen    
;		Modifications for file output handling.
;       1.3    	Andrew Bowen    
;		Processing options window called under XMANAGER from
;		adas205_proc. Error trapping added for abnormal termination
;		of FORTRAN process.
;       1.4    	Andrew Bowen    
;		Modifications for graphical output.
;       1.5    	Andrew Bowen    
;		More graphical mods and saving of user default settings.
;       1.6    	Andrew Bowen    
;		Data set comments browsing buttons added.
;       1.7    	Andrew Bowen    
;		First complete working version.
;       1.8    	Andrew Bowen    
;		Output robustness.
;       1.9    	Andrew Bowen    
;		Various mods.  Adas version number/date/time etc added.  
;		Code prepared ready to be turned into a routine to be called
;		from main menu system.
;       1.10   	Andrew Bowen    
;		Modified to be called from controlling program.
;       1.11   	Andrew Bowen    
;		Header comments added.
;       1.12   	Andrew Bowen    
;		Modification to comments.
;       1.13   	Andrew Bowen    
;		Output messages for output options widget deleted when
;		output part of program completed.
;       1.14   	Andrew Bowen    
;	1.15	Lalit Jalota
;	1.16	Lalit Jalota
;	1.17	Lalit Jalota
;	1.18	Lalit Jalota
;	1.19	Lalit Jalota
;	1.20	Tim Hammond
;		Corrected pathnames for contour.pass and metpop.pass
;	1.21	Tim Hammond
;		Further corrected pathnames for passing files.
;	1.22	Tim Hammond
;		Corrected adasprog.
;	1.23	Tim Hammond
;		Corrected adasprog.
;	1.24	Tim Hammond
;		Updated overall version number to 1.4
;	1.25	Tim Hammond
;		Updated overall version number to 1.5
;	1.26	Tim Hammond
;		Added new FLIPVAL flag to outval structure
;	1.27	Tim Hammond
;		Updated version number to 1.6
;	1.28	William Osborn
;		Added menu buttons
;	1.29	William Osborn
;		Updated version number to 1.7
;	1.30	Tim Hammond / William Osborn
;		Added information widget whilst processing.
;               Increased version number to 1.8
;	1.31	Richard Martin
;		Increased version number to 1.9
;	1.32	Richard Martin
;		Increased version number to 1.10
;	1.33	Richard Martin
;		Increased version number to 1.11
;	1.34	Martin O'Mullane
;		Added progress bar between processing and output
;               screens.
;		Increased version number to 1.12
;             
;
; VERSION:
;       1.1     20-04-93
;       1.2     30-04-93
;       1.3     04-05-93
;       1.4     11-05-93
;       1.5     12-05-93
;       1.6     12-05-93
;       1.7     17-05-93
;       1.8     20-05-93
;       1.9     24-05-93
;       1.10    25-05-93
;       1.11    25-05-93
;       1.12    27-05-93
;       1.13    08-06-93
;       1.14    10-06-93
;       1.15    13-03-95
;       1.16    13-03-95
;       1.17    13-03-95
;       1.18    13-03-95
;       1.19    13-03-95
;	1.20	18-04-95
;	1.21	18-04-95
;	1.22	05-06-95
;	1.23	14-07-95
;	1.24	15-11-95
;	1.25	04-12-95
;	1.26	26-02-96
;	1.27	13-05-96
;	1.28	04-06-96
;	1.29	11-07-96
;	1.30	05-08-96 / 14-10-96
;	1.31	21-11-02
;	1.32	18-03-02
;	1.33	18-03-03
;	1.34	29-04-03
;
;-
;-----------------------------------------------------------------------------

PRO ADAS205,	adasrel, fortdir, userroot, centroot, 			$
		devlist, devcode, font_large, font_small, edit_fonts

		;************************
		;**** Initialisation ****
		;************************

  adasprog = ' PROGRAM: ADAS205 V1.12'
  lpend = 0
  gomenu = 0
  deffile = userroot+'/defaults/adas205_defaults.dat'
  device = ''
  bitfile = centroot + '/bitmaps'


		;******************************************
		;**** Search for user default settings ****
		;**** If not found create defaults     ****
		;******************************************

  files = findfile(deffile)
  if files(0) eq deffile then begin

    restore,deffile
    inval.centroot = centroot+'/adf04/'
    inval.userroot = userroot+'/adf04/'

  endif else begin

    inval = { $
		ROOTPATH:userroot+'/adf04/', 				$
		FILE:'', 						$
		CENTROOT:centroot+'/adf04/', 				$
		USERROOT:userroot+'/adf04/' }

    procval = {NMET:-1}

    outval = { 								$
		GRPOUT:0, GTIT1:'', GRPSCAL:0, 				$
		XMIN:'',  XMAX:'',   					$
		YMIN:'',  YMAX:'',   					$
		HRDOUT:0, HARDNAME:'', 					$
		GRPDEF:'', GRPFMESS:'', 				$
		GRPSEL:-1, GRPRMESS:'', 				$
		DEVSEL:-1, GRSELMESS:'', 				$
		TEXOUT:0, TEXAPP:-1, 					$
		TEXREP:0, TEXDSN:'paper.txt', 				$
		TEXDEF:'',TEXMES:'', 					$
		CONOUT:0, CONAPP:-1, 					$
		CONREP:0, CONDSN:'', 					$
		CONDEF:userroot+'/pass/contour.pass',CONMES:'', 	$
		METOUT:0, METAPP:-1, 					$
		METREP:0, METDSN:'', 					$
		METDEF:userroot+'/pass/metpop.pass',METMES:'',		$
		FLIPVAL:0   						}

		;**** Default METPOP and contour name above !!****

  end


		;****************************
		;**** Start fortran code ****
		;****************************

  spawn,fortdir+'/adas205.out',unit=pipe,/noshell,PID=pid

		;************************************************
		;**** Get date and write to fortran via pipe ****
		;************************************************
  date = xxdate()
  printf,pipe,date(0)

LABEL100:
		;**** Check FORTRAN still running ****
  
  if find_process(pid) eq 0 then goto, LABELEND

		;************************************************
		;**** Communicate with b5spf0 in fortran and ****
		;**** invoke user interface widget for       ****
		;**** Data file selection                    ****
		;************************************************

  b5spf0, pipe, inval, dsninc, rep, FONT=font_large

		;**** If cancel selected then end program ****

  if rep eq 'YES' then goto, LABELEND

		;**** Check FORTRAN still running ****

  if find_process(pid) eq 0 then goto, LABELEND

		;*************************************
		;**** Read pipe comms from bxsetp ****
		;*************************************

  bxsetp, pipe, sz0, sz, scnte, sil, lfpool, strga


LABEL200:
		;**** Check FORTRAN still running ****

  if find_process(pid) eq 0 then goto, LABELEND

		;************************************************
		;**** Communicate with b5ispf in fortran and ****
		;**** invoke user interface widget for       ****
		;**** Processing options                     ****
		;************************************************
  b5ispf, pipe, lpend, procval, sz, sz0, strga, dsninc, bitfile, gomenu,$
	  FONT_LARGE=font_large, FONT_SMALL=font_small, $
	  EDIT_FONTS=edit_fonts

                ;**** If menu button clicked, tell FORTRAN to stop ****

    if gomenu eq 1 then begin
        printf, pipe, 1
        goto, LABELEND
    endif else begin
        printf, pipe, 0
    endelse

		;**** If cancel selected then goto 100 ****

  if lpend eq 1 then goto, LABEL100

		;**** Fortran processing now in progress. Get ****
		;**** date and time for this run. Put up a    ****
		;**** progress bar until output screen.       ****

  date = xxdate()

  if find_process(pid) EQ 0 then goto, LABELEND


  n_call = procval.maxt*procval.maxd + 1          ; extra o/p in fortran
                                                  ; to make sure its over.
  adas_progressbar, pipe=pipe, n_call=n_call, $
                    adasprog = 'ADAS205', font=font_large



		;**** Create header for output. ****

  header = adasrel+adasprog+' DATE: '+date(0)+' TIME: '+date(1)

LABEL300:
		;**** Check FORTRAN still running ****
  if find_process(pid) eq 0 then goto, LABELEND

		;************************************************
		;**** Communicate with b5spf1 in fortran and ****
		;**** invoke user interface widget for       ****
		;**** Output options                         ****
		;************************************************

  b5spf1, pipe, lpend, outval, dsninc, bitfile, gomenu,	$
		DEVLIST=devlist, FONT=font_large


                ;**** If menu button clicked, tell FORTRAN to stop ****

  if gomenu eq 1 then begin
      printf, pipe, 1
      outval.texmes = ''
      outval.conmes = ''
      outval.metmes = ''
      goto, LABELEND
  endif else begin
      printf, pipe, 0
  endelse

		;**** If cancel selected then erase output ****
		;**** messages and goto 200.		   ****
  if lpend eq 1 then begin
    outval.texmes = ''
    outval.conmes = ''
    outval.metmes = ''
    goto, LABEL200
  end

		;*************************************************
		;**** If text output has been requested write ****
		;**** header to pipe for XXADAS to read.      ****
		;*************************************************

  if outval.texout eq 1 then begin
    printf,pipe,header
  end

		;**** If graphical output requested ****

		;************************************************
		;**** Communicate with bxoutg in fortran and ****
		;**** invoke widget Graphical output         ****
		;************************************************
  if outval.grpout eq 1 then begin
		;**** User explicit scaling ****
    grpscal = outval.grpscal
    xmin = outval.xmin
    xmax = outval.xmax
    ymin = outval.ymin
    ymax = outval.ymax
		;**** Hardcopy output ****
    hrdout = outval.hrdout
    hardname = outval.hardname
    if outval.devsel ge 0 then device = devcode(outval.devsel)

    bxoutg, pipe, grpscal, xmin, xmax, ymin, ymax, $
		  hrdout, hardname, device, header, bitfile, gomenu,	$
	 	  FONT=font_large

                ;**** If menu button clicked, tell FORTRAN to stop ****

    if gomenu eq 1 then begin
        printf, pipe, 1
        outval.texmes = ''
   	outval.conmes = ''
    	outval.metmes = ''
        goto, LABELEND
    endif else begin
        printf, pipe, 0
    endelse

  end

		;**** Back for more output options ****
GOTO, LABEL300

LABELEND:

		;**** Ensure appending is not enabled for ****
		;**** text output at start of next run.   ****
outval.texapp = -1

		;**** Save user defaults and free all units ****

save,inval,procval,outval,filename=deffile

close, /all


END
