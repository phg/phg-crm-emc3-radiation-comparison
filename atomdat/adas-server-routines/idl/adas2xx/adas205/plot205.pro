; Copyright (c) 1995 Strathclyde University .
; SCCS info: Module @(#)$Header: /home/adascvs/idl/adas2xx/adas205/plot205.pro,v 1.19 2004/07/06 14:30:26 whitefor Exp $ Date $Date: 2004/07/06 14:30:26 $
;+
; PROJECT:
;       ADAS IBM MVS to DEC UNIX conversion
;
; NAME:
;	PLOT205
;
; PURPOSE:
;	Plot one graph for ADAS205/206.
;
; EXPLANATION:
;	This routine plots ADAS205/206 output for a single graph.
;	That includes the data for up to 7 ordinary levels for
;	a given metastable level.
;
; USE:
;	Use is specific to ADAS205/206.  See adas205_plot.pro for
;	example.
;
; INPUTS:
;	X	- Double array; list of x values.
;
;	Y	- 2D double array; y values, 2nd index ordinary level,
;		  indexed by iord1 and iord2.
;
;	IORDR	- Integer array; List of ordinary level numbers.
;
;	IORD1	- Integer; First ordinary level to plot from array.
;
;	IORD2	- Integer; Last ordinary level to plot from array.
;
;	ISPEC	- String; Constructed title for plot including plot type
;		  charge and electron temperature information.
;
;	HEADER	- String; ADAS version number header to include in graph.
;
;	TITLE	- String; General title for program run.
;
;	GTIT1	- String; Title for graph.
;
;	DSNINC	- String; Input COPASE data set name.
;
;	STRGA	- String array; Level designations.
;
;	IL	- Integer; Number of energy levels.
;
;	IMET	- Integer; Metastable index.
;
;	GRPSCAL	- Integer; 1 if user specified axis limits to be used, 
;		  0 if default scaling to be used.
;
;	XMIN	- String; Lower limit for x-axis of graph, number as string.
;
;	XMAX	- String; Upper limit for x-axis of graph, number as string.
;
;	YMIN	- String; Lower limit for y-axis of graph, number as string.
;
;	YMAX	- String; Upper limit for y-axis of graph, number as string.
;
; OPTIONAL INPUTS:
;	None.
;
; OUTPUTS:
;	None.
;
; OPTIONAL OUTPUTS:
;	None.
;
; KEYWORD PARAMETERS:
;	FONT	- String; Name of font passed to graphical output
;		  widget.
;
; CALLS:
;	None.
;
; SIDE EFFECTS:
;	None.
;
; CATEGORY:
;	Adas system.	
;
; WRITTEN:
;       Andrew Bowen, Tessella Support Services plc,  5-May-1993
;
; MODIFIED:
;       1.1    	Andrew Bowen
;              	First release.
;       1.2    	Andrew Bowen
;		Lots of formatting changes and annotation added.
;       1.3    	Andrew Bowen
;		Bug fixes.
;       1.4    	Andrew Bowen
;		Adas version and date added.
;       1.5    	Andrew Bowen
;		Header comments added.
;       1.6    	Andrew Bowen
;		Limits checking changes to single precision range.
;       1.7    	Andrew Bowen
;		Release data modified.
;       1.8    	Andrew Bowen
;		Checks added for explicit scaling to check to data out 
;		of range.
;       1.9    	Andrew Bowen
;		Device independent character size selection added.
;	1.10	Andrew Bowen	
;		Adjustment to left edge position.  Due to
;		changes in adas205_plot.pro all plots should
;		always be in range.  "Out of Range" messages
;		should no longer be displayed but the code
;		has been left in as an error trap.
;	1.11	Lalit Jalota
;       1.12    Tim Hammond      
;               Made very minor adjustment to left hand position
;               of the plot in order to ensure that the axis title
;               is always visible (previous history unknown).
;	1.13	Tim Hammond
;		Updated comments
;	1.14	Tim Hammond
;		Tidied up the labelling of the graphs.
;	1.15	Tim Hammond
;		Moved up right hand label slightly.
;	1.16	Tim Hammond
;		Added new COMMON block Global_lw_data which contains the 
;		values of left, right, top, bottom, grtop, grright
;	1.17	William Osborn
;		Corrected error in annotation code so that labels are added
;		even when the last point is outside the plot range
;	1.18	Tim Hammond
;		Added check for new environment variable VERY_SMALL
;               If this is set to the string "YES" (preferably in the
;               .adas_begin file) then the font sized used on the graphs
;               is reduced and the positioning of labels adjusted
;               slightly to make things fit on a very small screen.
;	1.19	Hugh Summers
;               Only list levels plotted at side of graph.
;
; VERSION:
;	1.1	11-05-93
;	1.2	12-05-93
;	1.3	20-05-93
;	1.4	24-05-93
;	1.5	02-06-93
;	1.6	08-06-93
;	1.7	08-06-93
;	1.8	08-06-93
;	1.9	09-06-93
;       1.10	11-10-93
;	1.11	13-03-95
;	1.12	07-04-95
;       1.13    07-04-95
;	1.14	14-07-95
;	1.15	23-11-95
;	1.16	26-02-96
;	1.17	10-05-96
;	1.18	05-08-96
;	1.18	19-04-2003
;
;-
;----------------------------------------------------------------------------

PRO plot205, x, y, iordr, iord1, iord2, ispec, header, title, gtit1, $
		dsninc, strga, il, imet, grpscal, xmin, xmax, ymin, ymax

  COMMON Global_lw_data, left, right, top, bottom, grtop, grright

		;**************************************************** 
		;**** Suitable character size for current device ****
		;**** Aim for 60 characters in y direction.      ****
		;**************************************************** 

  charsize = (!d.y_vsize/!d.y_ch_size)/60.0
  small_check = GETENV('VERY_SMALL')
  if small_check eq 'YES' then charsize=charsize*0.8


		;**** Initialise titles ****
		;**** '!E' shifts for exponent, !N returns to normal ****
  xtitle = 'ELECTRON DENSITY (cm!E-3!N)'
  ytitle = 'N(I)/(NE*N(**))  (cm!E3!N)'
  strg1 = '---- Level  Assignments ----'
  strg2 = 'INDEX       DESIGNATION       '

		;**** Construct graph title ****
		;**** '!C' is the new line control. ****
  gtitle =' '+    ispec+'!C!C'
  if small_check eq 'YES' then begin
    gtitle = gtitle+header+'!C!C'
    gtitle = gtitle+' GRAPH TITLE:'+gtit1+'!C!C'
    gtitle = gtitle+' INPUT FILE :'+dsninc+'!C!C'
  endif else begin
    gtitle = gtitle+header+'!C'
    gtitle = gtitle+' GRAPH TITLE:'+gtit1+'!C'
    gtitle = gtitle+' INPUT FILE :'+dsninc+'!C'
  endelse

		;**** Add metastable index to ytitle ****
  strput, ytitle, string(imet,format='(I2)'), 11

		;**** Construct run title and complete level list ****
  list = '!C!C'+title+'!C!C'
  if small_check eq 'YES' then begin
    list = list+strg1+'!C!C'+strg2+'!C!C'
  endif else begin
    list = list+strg1+'!C'+strg2+'!C'
  endelse
;  for ilev = 1, il do begin
; ---------------------------replacement block for above line-----------
  strg3 = string(imet,strga(imet-1),format='(I3,5X,A22)')
  if small_check eq 'YES' then list=list+'!C'
  list = list+'!C'+strg3+'!C'
  for ilev = iordr(iord1), iordr(iord2) do begin
; ---------------------------replacement block end ---------------------
    strg3 = string(ilev,strga(ilev-1),format='(I3,5X,A22)')
    if small_check eq 'YES' then list=list+'!C'
    list = list+'!C'+strg3
  end

		;**** How many points to plot ****
  npts = size(x)
  npts = npts(1)

		;**** Find x and y ranges for auto scaling,	   ****
		;**** check x and y in range for explicit scaling. ****
  makeplot = 1
  yext = y(*,iord1:iord2)

  if grpscal eq 0 then begin
		;**** identify values in the valid range ****
		;**** plot routines only work within ****
		;**** single precision limits.	     ****
    xvals = where(x gt 1.6e-36 and x lt 1.0e37)
    yvals = where(yext gt 1.6e-36 and yext lt 1.0e37)

    if xvals(0) gt -1 then begin
      xmax = max(x(xvals))
      xmin = min(x(xvals))
    end else begin
      makeplot = 0
    end

    if yvals(0) gt -1 then begin
      ymax = max(yext(yvals))
      ymin = min(yext(yvals))
    end else begin
      makeplot = 0
    end

    style = 0
  end else begin
    xvals = where(x ge xmin and x le xmax)
    yvals = where(yext ge ymin and yext le ymax)
    if xvals(0) eq -1 or yvals(0) eq -1 then begin
      makeplot = 0
    end
    style = 1
  end

  if makeplot eq 1 then begin

		;**** Set up log-log plotting axes ****
    plot_oo,[xmin,xmax],[ymin,ymax],/nodata,ticklen=1.0, $
		position=[left,bottom,grright,grtop], $
		xtitle=xtitle, ytitle=ytitle, xstyle=style, ystyle=style, $
		charsize=charsize

		;*********************************
		;**** Make and annotate plots ****
		;*********************************
    for i = iord1, iord2 do begin
		;**** Make plot ****
      oplot,x,y(*,i)

		;**** Find suitable point for annotation ****
      if (x(npts-1) ge xmin) and (x(npts-1) le xmax) and $
         (y(npts-1,i) ge ymin) and (y(npts-1,i) le ymax) then begin

	  iplot = npts-1

      end else begin
         
        iplot = -1
        for id = 0, npts-2 do begin
          if (x(id) ge xmin) and (x(id) le xmax) and $
             (y(id,i) ge ymin) and (y(id,i) le ymax) then begin
		  iplot = id
          end
        end
      end

		;**** Annotate plots with level numbers ****
      if iplot ne -1 then begin
        xyouts,x(iplot),y(iplot,i),string(iordr(i),format='(1x,i3)')
      end

    end


		;**** Output title above graphs ****

    if small_check eq 'YES' then begin
        xyouts,left,top,gtitle,/normal,charsize=charsize*0.9
    endif else begin
        xyouts,left,top,gtitle,/normal,charsize=charsize
    endelse

		;**** Output level list at the side of the graph ****

    if small_check eq 'YES' then begin
        xyouts,grright+0.03,top-0.08,list,/normal,charsize=charsize*0.8
    endif else begin
        xyouts,grright+0.03,top-0.08,list,/normal,charsize=charsize
    endelse


  end else begin

		;**** Print a message to the screen ****
    erase
    format = "('Data for ordinary levels ',i2,' to ',i2,' out of range.')"
    mess = string(iord1+1,iord2+1,format=format)
    xyouts,left,grtop*0.7,ytitle+'   V   '+xtitle+'!C'+mess, $
		/normal,charsize=charsize

		;**** Output title above graphs ****

    xyouts,(left-0.18),top,gtitle,/normal,charsize=charsize*0.91

		;**** Output level list at the side of the graph ****

    if small_check eq 'YES' then begin
        xyouts,grright+0.03,top-0.02,list,/normal,charsize=charsize*0.8
    endif else begin
        xyouts,grright+0.03,top,list,/normal,charsize=charsize*0.95
    endelse


  end

END
