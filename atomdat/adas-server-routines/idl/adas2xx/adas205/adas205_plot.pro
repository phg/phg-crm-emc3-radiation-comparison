; Copyright (c) 1995 Strathclyde University .
; SCCS info: Module @(#)$Header: /home/adascvs/idl/adas2xx/adas205/adas205_plot.pro,v 1.16 2004/07/06 10:16:47 whitefor Exp $ Date $Date: 2004/07/06 10:16:47 $
;+
; PROJECT:
;       ADAS IBM MVS to DEC UNIX conversion
;
; NAME:
;	ADAS205_PLOT
;
; PURPOSE:
;	Generates ADAS205/206/208 graphical output.
;
; EXPLANATION:
;	This routine creates a window for the display of graphical
;	output and displays output as the user directs by interaction
;	with the widget.  The widget allows the user to step backwards
;	and forwards through the graphical output at will.  To make
;	this possible a separate routine PLOT205 actually plots a
;	graph.  This routine compiles a table of all possible graphs
;	and calls PLOT205 as each new graph is requested via an event
;	from the graphical output widget.
;
; USE:
;	This routine is specific to ADAS205/206/208, see bxoutg.pro for
;	example use.
;
; INPUTS:
;	(Most of these inputs map exactly onto varaibles of the same
;	 name in the FORTRAN code.)
;
;	NDLEV   - Integer; Maximum number of levels allowed.
;
;	NDTEM   - Integer; Maximum number of temperatures allowed.
;
;	NDDEN   - Integer; Maximum number of densities allowed.
;
;	NDMET   - Integer; Maximum number of metastables allowed.
;
;	TITLED  - String; Element symbol.
;
;	TITLE   - String; General title for program run.
;
;	GTIT1   - String; Title for graph.
;
;	DSNINC  - String; Input COPASE data set name.
;
;	IZ      - Integer; Recombined ion charge.
;
;	ITSEL   - Integer; Index of temperature selected for graph.
;
;	TEV     - Float; Selected electron temperature (eV) for graph.
;
;	GRPSCAL	- Integer; 1 indicates use user supplied axis limits
;		  for scaling, 0 use defaults.
;
;	XMIN    - String; Lower limit for x-axis of graph, number as string.
;
;	XMAX    - String; Upper limit for x-axis of graph, number as string.
;
;	YMIN    - String; Lower limit for y-axis of graph, number as string.
;
;	YMAX    - String; Upper limit for y-axis of graph, number as string.
;
;	IL      - Integer; Number of energy levels = 'NMET' + 'NORD'.
;
;	NMET    - Integer; Number of metastable levels.
;
;	NORD    - Integer; Number of ordinary levels.
;
;	MAXD    - Integer; Number of input electron densities.
;
;	LMETR	- Integer array; Array parallel to imetr.  1 indicates that
;		  transition exists to level given by same element in imetr.
;		  0 indicates that transitions do not exist. e.g if
;		  lmetr(2) is 1 then transition/s exist to level imetr(2).
;
;	IMETR	- Integer array; Index of metastables in complete level
;		  list.  Size intarr(ndmet).
;
;	IORDR	- Integer array; Index of ordinary excited levels in
;		  complete level list.
;
;	DENSA	- Double array; Electron densities (units: cm-3)
;
;	STRGA	- String array; Level designations.
;
;	STACK	- 4D Double array; Population dependence;
;			1st dimension: ordinary level index
;			2nd dimension: metastable index
;			3rd dimension: temperature index
;			4th dimension: density index
;
;	HARDOUT - Integer; 1 if hardcopy output activated, 0 if not.
;
;	HARDNAME- String; Filename for harcopy output.
;
;	DEVICE	- String; IDL name of hardcopy output device.
;
;	HEADER	- String; ADAS version number header to include in graph.
;
;       BITFILE - String; the path to the dirctory containing bitmaps
;                 for the 'escape to series menu' button.
;
; OPTIONAL INPUTS:
;	None.
;
; OUTPUTS:
;       GOMENU  - Int; flag - set to 1 if user has selected 'escape direct
;                 to series menu , otherwise 0
;
; OPTIONAL OUTPUTS:
;	None.
;
; KEYWORD PARAMETERS:
;	FONT	- String; The name of a font to use for text in the
;		  graphical output widget.
;
;	WIN_NUMB - String; Gives the number of the ADAS routine which is
;		   calling this procedure, '205,'206' or '208'. If omitted,
;		   '205' is the default.
;
; CALLS:
;	CW_ADAS_GRAPH	Graphical output widget.
;	PLOT205		Make one plot to an output device for 205/206/208.
;	XMANAGER
;
; SIDE EFFECTS:
;	This routine uses a common block to maintain its state PLOT205_BLK.
;
;	One other routine is included in this file;
;	ADAS205_PLOT_EV	Called via XMANAGER during widget management.
;
; CATEGORY:
;	Adas system.
;
; WRITTEN:
;       Andrew Bowen, Tessella Support Services plc,  5-May-1993
;
; MODIFIED:
;       1       Andrew Bowen    8-Jun-1993
;               First release.
;	1.1	Andrew Bowen	11-Oct-1993
;		Bug fixed. Cases with only a single graph to
;		plot caused a fatal error. Corrected.
;		Behaviour modification. Plotting "Out of Range"
;		messages suppressed.
;	1.12	Tim Hammond
;		Changed graph title to the standard upper case
;		(previous history unknown)
;	1.13	William Osborn
;		Added win_numb keyword to allow different window titles for
;		different routines + caught error for no points in plot range
;	1.14	William Osborn
;		Removed diagnostic print statement
;	1.15	William Osborn
;		Added menu button and dynlabel procedure
;	1.16	Tim Hammond
;		Added "intelligent" resizing of the graphical output
;		window.
;
; VERSION:
;       1.1	11-Oct-1993
;	1.12	14-Jul-1995
;	1.13	10-05-96
;	1.14	10-05-96
;	1.15	04-06-96
;	1.16	06-08-96
;
;----------------------------------------------------------------------------

PRO adas205_plot_ev, event

  COMMON plot205_blk, data, action, nplot, iplot, win, plotdev, $
		      plotfile, fileopen, gomenu


  newplot = 0
  print = 0
  done = 0
		;****************************************
		;**** Set graph and device requested ****
		;****************************************
  CASE event.action OF

	'previous' : begin
			if iplot gt 0 then begin
			  newplot = 1
			  iplot = iplot - 1
			  first = iplot
			  last = iplot
			end
		end

	'next'	   : begin
			if iplot lt nplot-1 then begin
			  newplot = 1
			  iplot = iplot + 1
			  first = iplot
			  last = iplot
			end
		end

	'print'	   : begin
			newplot = 1
			print = 1
			first = iplot
			last = iplot
		end

	'printall' : begin
			newplot = 1
			print = 1
			first = 0
			last = nplot-1
		end

	'done'	   : begin
			if fileopen eq 1 then begin
			  set_plot,plotdev
			  device,/close_file
			end
			set_plot,'X'
			widget_control,event.top,/destroy
			done = 1
		end
        'bitbutton' : begin
            		if fileopen eq 1 then begin
                	  set_plot, plotdev
                	  device, /close_file
            		endif
            		set_plot,'X'
            		widget_control, event.top, /destroy
            		done = 1
            		gomenu = 1
        end
  END

		;*******************************
		;**** Make requested plot/s ****
		;*******************************

  if done eq 0 then begin
		;**** Set graphics device ****
    if print eq 1 then begin

      set_plot,plotdev
      if fileopen eq 0 then begin
        fileopen = 1
        device,filename=plotfile
	device,/landscape
      end

    end else begin

      set_plot,'X'
      wset,win
  
    end

		;**** Draw graphics ****
    if newplot eq 1 then begin
      for i = first, last do begin
        im = data.im(i)
        iord1 = data.iord1(i)
        iord2 = data.iord2(i)
        plot205,data.densa, data.y(*,*,im), data.iordr, iord1, iord2, $
		data.ispec, data.header, data.title, data.gtit1, data.dsninc, $
		data.strga, data.il, data.imetr(im), data.grpscal, $
		data.xmin, data.xmax, data.ymin, data.ymax

	if print eq 1 then begin
	  message = 'Plot '+strtrim(string(i+1),2)+' written to print file.'
	  grval = {WIN:0, MESSAGE:message}
	  widget_control,event.id,set_value=grval
	end

      end
    end

  end

END

;----------------------------------------------------------------------------

PRO adas205_plot, $
		ndlev,	ndtem,	ndden,	ndmet,	$
		titled,	title,	gtit1,	dsninc,	$
		iz,	itsel,	tev,		$
		grpscal,			$
		xmin,	xmax,	ymin,	ymax,	$
		il,	nmet,	nord,	maxd,	$
		lmetr,	imetr,	iordr,	densa,	$
		strga,	stack,			$
		hrdout, hardname,device,header,	$
		bitfile, gomenu,		$
		FONT=font, WIN_NUMB = win_numb


  ngpic = 7
  if not keyword_set(win_numb) then win_numb='205'

  COMMON plot205_blk, data, action, nplot, iplot, win, plotdev, $
		      plotfile, fileopen, gomenucom

		;**** Copy input values to common ****
  plotdev = device
  plotfile = hardname
  fileopen = 0
  gomenucom = gomenu

		;************************************
		;**** Create general graph title ****
		;************************************
  format = '	("POPULATION DEPENDENCE ON METASTABLES:   ",'+ $
		 'a3,i2,3x,"ELECTRON TEMP. = ",e10.2," (eV)")    '
  ispec = string(titled,iz,tev,format=format)


		;*********************************
		;**** Set up Y data for plots ****
		;*********************************
  y = dblarr(maxd,nord,nmet)

  for im = 0, nmet-1 do begin
    for ilev = 0, nord-1 do begin
	y(*,ilev,im) = stack(ilev,im,itsel,*)/densa(*)
    end
  end

		;********************************************
		;**** Create table of all possible plots ****
		;********************************************
  nplot = 0

  for im = 0, nmet-1 do begin

		;**** Check for metastable levels without plots ****
    if lmetr(im) eq 0 then begin

      print,'********** ADAS'+win_numb+'_PLOT MESSAGE **********'
      print,imetr(im),iordr(0),iordr(nord-1), $
	format = '(1X,"Metastable Level: ",I3,4X," Ordinary Levels: ",I3,'+ $
		'" -> ",I3/1X,"No graph will be output because:")'
      print,'No input transitions are to the metastable level'
      print,'********** END OF MESSAGE **********'

    end else begin

		;**** Create table entries for each plot ****
		;**** for this metastable level.         ****
		;**** Three arrays make up the table.  There is one   ****
		;**** entry in each array for each plot.	      ****
		;**** imtab(i)		metastable for plot i 		****
		;**** iord1tab(i)	first ordinary level for plot i ****
		;**** iord2tab(i)	last ordinary level for plot i  ****
		;**** note iord1 and iord2 are indices through the list ****
		;**** of ordinary levels which is not the same as the   ****
		;**** level numbers themselves, imet offsets them.      ****
      for iord1 = 0, nord-1, ngpic do begin
        iord2 = iord1 + ngpic-1
        iord2 = min([iord2,nord-1])

		;**** check x and y in range for explicit or auto scaling. ****
		;**** identify values in the valid range ****
		;**** plot routines only work within ****
		;**** single precision limits.	     ****
	makeplot = 1
	yext = y(*,iord1:iord2,im)
	if grpscal eq 0 then begin
	  xvals = where(densa gt 1.0e-37 and densa lt 1.0e37)
	  yvals = where(yext gt 1.0e-37 and yext lt 1.0e37)
	end else begin
	  xvals = where(densa ge xmin and densa le xmax)
	  yvals = where(yext ge ymin and yext le ymax)
	end
	if xvals(0) eq -1 or yvals(0) eq -1 then begin
	  makeplot = 0
	end

	if makeplot eq 1 then begin
          if nplot eq 0 then begin
	    nplot = 1
            imtab = [im]
            iord1tab = [iord1]
            iord2tab = [iord2]
          endif else begin
	    nplot = nplot + 1
            imtab = [imtab,im]
            iord1tab = [iord1tab,iord1]
            iord2tab = [iord2tab,iord2]
          endelse
	endif
      end

    end

  end

		;**** Give an error if there's nothing to plot ****

  if nplot eq 0 then begin

    print,'********** ADAS'+win_numb+'_PLOT MESSAGE **********'
    if grpscal eq 0 then begin
      print,'* No points in x or y ranges : 1.0e-37 to 1.0e+37 *'
    endif else begin
      print,'************ No points in plot range **************'
    endelse
    print,'***************** END OF MESSAGE ******************'

  endif else begin
     
		;*************************************
		;**** Create graph display widget ****
		;*************************************
  graphid = widget_base(TITLE='ADAS'+win_numb+' GRAPHICAL OUTPUT', $
					XOFFSET=1,YOFFSET=1)
  device, get_screen_size=scrsz
  xwidth = scrsz(0)*0.75
  yheight = scrsz(1)*0.75
  if nplot gt 1 then multiplot = 1 else multiplot = 0
  bitval = bitfile + '/menu.bmp'
  cwid = cw_adas_graph(graphid,print=hrdout,multiplot=multiplot,FONT=font,$
		bitbutton=bitval, xsize=xwidth, ysize=yheight)

                ;**** Realize the new widget ****
  dynlabel, graphid
  widget_control,graphid,/realize

		;**** Get the id of the graphics area ****
  widget_control,cwid,get_value=grval
  win = grval.win

		;*******************************************
		;**** Put the graphing data into common ****
		;*******************************************
  data = {	IM:imtab,	IORDR:iordr,	$
		IORD1:iord1tab,	IORD2:iord2tab, $
		DENSA:densa,	Y:y,		ISPEC:ispec,	$
		HEADER:header,	TITLE:title,	GTIT1:gtit1,	$
		DSNINC:dsninc,	$
		STRGA:strga,	GRPSCAL:grpscal, $
		XMIN:xmin,	XMAX:xmax,	YMIN:ymin,	YMAX:ymax, $
		IL:il,		IMETR:imetr}

		;**** Initialise to plot 0 ****
  iplot = 0
  wset,win
  im = data.im(0)
  iord1 = data.iord1(0)
  iord2 = data.iord2(0)
  plot205,	data.densa, data.y(*,*,im), data.iordr, iord1, iord2, $
		data.ispec, data.header, data.title, data.gtit1, data.dsninc, $
		data.strga, data.il, data.imetr(im), data.grpscal, $
		data.xmin, data.xmax, data.ymin, data.ymax


		;***************************
                ;**** make widget modal ****
		;***************************
  xmanager,'adas205_plot',graphid,event_handler='adas205_plot_ev', $
                                        /modal,/just_reg

                ;**** Return the output value from common ****

  endelse

  gomenu = gomenucom

END
