; Copyright (c) 1995 Strathclyde University .
; SCCS info: Module @(#)$Header: /home/adascvs/idl/adas2xx/adas205/cw_adas205_out.pro,v 1.13 2004/07/06 12:30:52 whitefor Exp $ Date $Date: 2004/07/06 12:30:52 $
;+
; PROJECT:
;       ADAS IBM MVS to DEC UNIX conversion
;
; NAME:
;	CW_ADAS205_OUT()
;
; PURPOSE:
;	Produces a widget for ADAS205 output options.
;
; EXPLANATION:
;	This function declares a compound widget consisting of the
;	graphical output selection widget cw_adas_gr_sel.pro, and three
;	output file widgets cw_adas_outfile.pro.  The three text output
;	files specified in this widget are for tabular (paper.txt)
;	output, countour passing file and METPOP passing file.  This
;	widget also includes a button for browsing the comments from
;	the input dataset, a 'Cancel' button and a 'Done' button.
;
;	The compound widgets cw_adas_gr_sel.pro and cw_adas_outfile.pro
;	included in this file are self managing.  This widget only
;	handles events from the 'Done' and 'Cancel' buttons.
;
;	This widget only generates events for the 'Done' and 'Cancel'
;	buttons.
;
; USE:
;	This widget is specific to ADAS205, see adas205_out.pro	for use.
;
; INPUTS:
;	PARENT	- Long integer; ID of parent widget.
;
;	DSNINC	- Name of input dataset for this application.
;
;       BITFILE - String; the path to the dirctory containing bitmaps
;                 for the 'escape to series menu' button.
;
; OPTIONAL INPUTS:
;	None.
;
; OUTPUTS:
;	The return value of this function is the ID of the declared widget.
;
; OPTIONAL OUTPUTS:
;	None.
;
; KEYWORD PARAMETERS:
;	GRPLIST	- A string array; A list of graphs from which to choose.
;		  For ADAS205 this is a list of electron temperatures.
;		  One element of the array for each electron temperature.
;
;	DEVLIST	- A string array;  Each element of the array is the
;		  name of a hardcopy graphics output device.  This
;		  list must mirror the DEVCODE list.  DEVLIST is the
;		  list which the user will see.
;		  e.g ['Post-Script','HP-PCL','HP-GL']
;
;	VALUE	- A structure which determines the initial settings of
;		  the output options widget.  The structure is made
;		  up of four parts.  Each part is the same as the value
;		  structure of one of the four main compound widgets
;		  included in this widget.  See cw_adas_gr_sel and
;		  cw_adas_outfile for more details.  The default value is;
;
;		      { GRPOUT:0, GTIT1:'', 				$
;			GRPSCAL:0, 					$
;			XMIN:'',  XMAX:'',   				$
;			YMIN:'',  YMAX:'',   				$
;			HRDOUT:0, HARDNAME:'', 				$
;			GRPDEF:'', GRPFMESS:'', 			$
;			GRPSEL:-1, GRPRMESS:'', 			$
;			DEVSEL:-1, GRSELMESS:'', 			$
;			TEXOUT:0, TEXAPP:-1, 				$
;			TEXREP:0, TEXDSN:'', 				$
;			TEXDEF:'',TEXMES:'', 				$
;			CONOUT:0, CONAPP:-1, 				$
;			CONREP:0, CONDSN:'', 				$
;			CONDEF:'',CONMES:'', 				$
;			METOUT:0, METAPP:-1, 				$
;			METREP:0, METDSN:'', 				$
;			METDEF:'',METMES:'',				$
;			FLIPVAL:0   					}
;
;		  For CW_ADAS_GR_SEL;
;			GRPOUT	   Integer; Activation button 1 on, 0 off
;			GTIT1	   String; Graph title
;			GRPSCAL	   Integer; Scaling activation 1 on, 0 off
;			XMIN	   String; x-axis minimum, string of number
;			XMAX	   String; x-axis maximum, string of number
;			YMIN	   String; y-axis minimum, string of number
;			YMAX	   String; y-axis maximum, string of number
;			HRDOUT	   Integer; Hard copy activ' 1 on, 0 off
;			HARDNAME   String; Hard copy output file name
;			GRPDEF	   String; Default output file name 
;			GRPFMESS   String; File name error message
;			GRPSEL	   Integer; index of selected graph in GRPLIST
;			GRPRMESS   String; Scaling ranges error message
;			DEVSEL	   Integer; index of selected device in DEVLIST
;			GRSELMESS  String; General error message
;
;		  For CW_ADAS_OUTFILE tabular output (paper.txt);
;			TEXOUT	Integer; Activation button 1 on, 0 off
;			TEXAPP	Integer; Append button 1 on, 0 off, -1 no button
;			TEXREP	Integer; Replace but' 1 on, 0 off, -1 no button
;			TEXDSN	String; Output file name
;			TEXDEF	String; Default file name
;			TEXMES	String; file name error message
;
;		  For CW_ADAS_OUTFILE contour passing file;
;			CONOUT  Integer; Activation button 1 on, 0 off
;			CONAPP	Integer; Append button 1 on, 0 off, -1 no button
;			CONREP	Integer; Replace but' 1 on, 0 off, -1 no button
;			CONDSN	String; Output file name
;			CONDEF	String; Default file name
;			CONMES	String; file name error message
;
;		  FOR CW_ADAS_OUTFILE Metpop passing file;
;			METOUT	Integer; Activation button 1 on, 0 off
;			METAPP	Integer; Append button 1 on, 0 off, -1 no button
;			METREP	Integer; Replace but' 1 on, 0 off, -1 no button
;			METDSN	String; Output file name
;			METDEF	String; Default file name
;			METMES	String; file name error message
;		  FOR OVERALL WIDGET;
;			FLIPVAL Integer; Flag showing whether graphics options
;					 should be displayed (1) or text (0)
;
;	UVALUE	- A user value for this widget.
;
;	FONT	- String; the name of a font to be used for all text in
;		  this widget.  Default to system default font.
;
; CALLS:
;	CW_ADAS_DSBR	Input dataset name and comments browse button.
;	CW_ADAS_GR_SEL	Graphical output selection widget.
;	CW_ADAS_OUTFILE	Output file name entry widget.
;	See side effects for other related widget management routines.
;
; SIDE EFFECTS:
;       A COMMON BLOCK: CW_OUT205_BLK is used which is private to
;       this widget.
;
;	OUT205_GET_VAL()
;	OUT205_EVENT()
;
; CATEGORY:
;	Compound Widget
;
; WRITTEN:
;       Andrew Bowen, Tessella Support Services plc, 6-Apr-1993
;
; MODIFIED:
;       1.1	Andrew Bowen    
;               First release.
;       1.2	Andrew Bowen    
;		Modifications for graphical output.
;       1.3	Andrew Bowen    
;		Changes to allow correct titles for graphs.
;       1.4	Andrew Bowen    
;		Data set comments browsing buttons added.
;       1.5	Andrew Bowen    
;		Roubustness and output rationalisation.
;       1.6	Andrew Bowen    
;		Redundant set_value routine removed from widget.
;       1.7	Andrew Bowen    
;		Header comments added.
;	1.8	Lalit Jalota
;	1.9	Tim Hammond
;		Added switch to allow toggling between display of graphical
;		and text output settings enabling size of screen to be
;		considerably reduced.
;	1.10	William Osborn
;		Added menu button
;	1.11	William Osborn
;		Removed xsize=n keyword from widget_label command.
;		The dynlabel procedure does not override these
;		assignments and they were taken as being values in pixels
;		on some machines.
;       1.12    William Osborn
;               Ensured that a font setting is used for every widget
;	1.13	Richard Martin
;		Removed obsolete cw_loadstate/savestate.
;
; VERSION:
;       1.1     30-04-93
;	1.2	11-05-93
;	1.3	12-05-93
;	1.4	12-05-93
;	1.5	20-05-93
;	1.6	24-05-93
;	1.7	02-06-93
;	1.8	13-03-95
;	1.9	26-02-96
;	1.10	04-06-96
;	1.11	01-08-96
;	1.12	10-10-96
;	1.13	06-03-02
;
;-
;-----------------------------------------------------------------------------

FUNCTION out205_get_val, id

                ;**** Return to caller on error ****
  ON_ERROR, 2

                ;**** Retrieve the state ****

  first_child = widget_info(id,/child)
  widget_control, first_child, get_uvalue=state, /no_copy
  
		;**** Get graphical output settings ****
  widget_control,state.grpid,get_value=grselval

		;**** Get text output settings ****
  widget_control,state.paperid,get_value=papos

		;**** Get contour file settings ****
  widget_control,state.contid,get_value=conos

		;**** Get Metpop file settings ****
  widget_control,state.metpid,get_value=metos

		;**** Get flip-switch setting ****

  widget_control, state.flipid, get_value=flipval

  os = { 	GRPOUT:grselval.outbut, GTIT1:grselval.gtit1, 		$
		GRPSCAL:grselval.scalbut, 				$
		XMIN:grselval.xmin,     XMAX:grselval.xmax,   		$
		YMIN:grselval.ymin,     YMAX:grselval.ymax,   		$
		HRDOUT:grselval.hrdout, HARDNAME:grselval.hardname, 	$
		GRPDEF:grselval.grpdef, GRPFMESS:grselval.grpfmess, 	$
		GRPSEL:grselval.grpsel, GRPRMESS:grselval.grprmess, 	$
		DEVSEL:grselval.devsel, GRSELMESS:grselval.grselmess, 	$
		TEXOUT:papos.outbut,  TEXAPP:papos.appbut, 		$
		TEXREP:papos.repbut,  TEXDSN:papos.filename, 		$
		TEXDEF:papos.defname, TEXMES:papos.message, 		$
		CONOUT:conos.outbut,  CONAPP:conos.appbut, 		$
		CONREP:conos.repbut,  CONDSN:conos.filename, 		$
		CONDEF:conos.defname, CONMES:conos.message, 		$
		METOUT:metos.outbut,  METAPP:metos.appbut, 		$
		METREP:metos.repbut,  METDSN:metos.filename, 		$
		METDEF:metos.defname, METMES:metos.message,   		$
		FLIPVAL:flipval(0)					}

  widget_control, first_child, set_uvalue=state, /no_copy

  RETURN, os 

END

;-----------------------------------------------------------------------------

FUNCTION out205_event, event

                ;**** Base ID of compound widget ****
  parent=event.handler

                ;**** Retrieve the state ****

  first_child = widget_info(parent,/child)
  widget_control, first_child, get_uvalue=state

		;*********************************
		;**** Clear previous messages ****
		;*********************************
  widget_control,state.messid,set_value=' '

                ;************************
                ;**** Process events ****
                ;************************
  CASE event.id OF

		;*****************************************************
		;**** Change between graphics/text output display ****
		;*****************************************************

    state.flipid: begin
	widget_control, state.flipid, get_value=flipval
  	widget_control, state.graphbase, map=(1-flipval(0))
	widget_control, state.textbase, map=flipval(0)
    end
		;***********************
		;**** Cancel button ****
		;***********************

    state.cancelid: new_event = {ID:parent, TOP:event.top, 		$
				HANDLER:0L, ACTION:'Cancel'}

		;*********************
		;**** Done button ****
		;*********************

    state.doneid: begin

		;***************************************
		;**** Check for errors in the input ****
		;***************************************
	  error = 0

		;**** Get a copy of the widget value ****
	  widget_control,event.handler,get_value=os

		;**** Check for widget error messages ****
	  if os.grpout eq 1 and strtrim(os.grpfmess) ne '' then error=1
	  if os.grpout eq 1 and os.grpscal eq 1 and $
				strtrim(os.grprmess) ne '' then error=1
	  if os.grpout eq 1 and strtrim(os.grselmess) ne '' then error=1
	  if os.texout eq 1 and strtrim(os.texmes) ne '' then error=1
	  if os.conout eq 1 and strtrim(os.conmes) ne '' then error=1
	  if os.metout eq 1 and strtrim(os.metmes) ne '' then error=1


	  if error eq 1 then begin
	    widget_control,state.messid,set_value= $
				'**** Error in output settings ****'
	    new_event = 0L
	  end else begin
	    new_event = {ID:parent, TOP:event.top, HANDLER:0L, ACTION:'Done'}
	  end

        end

                ;**** Menu button ****

    state.outid: begin
            new_event = {ID:parent, TOP:event.top, HANDLER:0L,          $
                         ACTION:'Menu'}
        end

    ELSE: new_event = 0L

  ENDCASE  

  RETURN, new_event
END

;-----------------------------------------------------------------------------

FUNCTION cw_adas205_out, parent, dsninc, bitfile,		$
		GRPLIST=grplist, DEVLIST=devlist, $
		VALUE=value, UVALUE=uvalue, $
		FONT=font


  IF (N_PARAMS() EQ 0) THEN MESSAGE, 'Must specify parent for cw_adas205_out'
  ON_ERROR, 2					;return to caller

		;**** Set defaults for keywords ****
  IF NOT (KEYWORD_SET(grplist)) THEN grplist = ''
  IF NOT (KEYWORD_SET(devlist)) THEN devlist = ''
  IF NOT (KEYWORD_SET(uvalue)) THEN uvalue = 0
  IF NOT (KEYWORD_SET(font)) THEN font = ''
  IF NOT (KEYWORD_SET(value)) THEN begin
	os = {out205_set, 						$
			GRPOUT:0, GTIT1:'', 				$
			GRPSCAL:0, 					$
			XMIN:'',  XMAX:'',   				$
			YMIN:'',  YMAX:'',   				$
			HRDOUT:0, HARDNAME:'', 				$
			GRPDEF:'', GRPFMESS:'', 			$
			GRPSEL:-1, GRPRMESS:'', 			$
			DEVSEL:-1, GRSELMESS:'', 			$
			TEXOUT:0, TEXAPP:-1, 				$
			TEXREP:0, TEXDSN:'', 				$
                        TEXDEF:'',TEXMES:'', 				$
			CONOUT:0, CONAPP:-1, 				$
			CONREP:0, CONDSN:'', 				$
                        CONDEF:'',CONMES:'', 				$
			METOUT:0, METAPP:-1, 				$
			METREP:0, METDSN:'', 				$
			METDEF:'',METMES:'',				$
			FLIPVAL:0   					}
  END ELSE BEGIN
	os = {out205_set, 						$
			GRPOUT:value.grpout, GTIT1:value.gtit1, 	$
			GRPSCAL:value.grpscal, 				$
			XMIN:value.xmin,     XMAX:value.xmax,   	$
			YMIN:value.ymin,     YMAX:value.ymax,   	$
			HRDOUT:value.hrdout, HARDNAME:value.hardname, 	$
			GRPDEF:value.grpdef, GRPFMESS:value.grpfmess, 	$
			GRPSEL:value.grpsel, GRPRMESS:value.grprmess, 	$
			DEVSEL:value.devsel, GRSELMESS:value.grselmess, $
			TEXOUT:value.texout, TEXAPP:value.texapp, 	$
			TEXREP:value.texrep, TEXDSN:value.texdsn, 	$
			TEXDEF:value.texdef, TEXMES:value.texmes, 	$
			CONOUT:value.conout, CONAPP:value.conapp, 	$
			CONREP:value.conrep, CONDSN:value.condsn, 	$
			CONDEF:value.condef, CONMES:value.conmes, 	$
			METOUT:value.metout, METAPP:value.metapp, 	$
			METREP:value.metrep, METDSN:value.metdsn, 	$
			METDEF:value.metdef, METMES:value.metmes,	$
			FLIPVAL:value.flipval   			}
  END

		;**********************************************
		;**** Create the 205 Output options widget ****
		;**********************************************

		;**** create base widget ****
  cwid = widget_base( parent, UVALUE = uvalue, $
			EVENT_FUNC = "out205_event", $
			FUNC_GET_VALUE = "out205_get_val", $
			/COLUMN)

		;**** Create base to hold the value of state ****

  first_child = widget_base(cwid)

		;**** Add dataset name and browse button ****

  rc = cw_adas_dsbr(first_child,dsninc,font=font)

		;****************************************************
		;**** Add another base for the switching between ****
		;**** Graphics and text output options           ****
		;****************************************************

  flipbase2 = widget_base(cwid, /row)
  flipbase = widget_base(cwid)
  labelval = 'Select output option settings for display: '
  fliplab = widget_label(flipbase2, font=font, value=labelval)
  flipid = cw_bgroup(flipbase2, ['Graphics','Text'], /exclusive,	$
		     font=font, /row, set_value=os.flipval)

		;**** Widget for graphics selection ****
		;**** Note change in names for GRPOUT and GRPSCAL ****

  grselval = {  OUTBUT:os.grpout, GTIT1:os.gtit1, SCALBUT:os.grpscal, $
		XMIN:os.xmin, XMAX:os.xmax, $
		YMIN:os.ymin, YMAX:os.ymax, $
		HRDOUT:os.hrdout, HARDNAME:os.hardname, $
		GRPDEF:os.grpdef, GRPFMESS:os.grpfmess, $
		GRPSEL:os.grpsel, GRPRMESS:os.grprmess, $
		DEVSEL:os.devsel, GRSELMESS:os.grselmess }
  base = widget_base(flipbase,/row,/frame)
  graphbase = base
  grpid = cw_adas_gr_sel(base, OUTPUT='Graphical Output', GRPLIST=grplist, $
                        LISTTITLE='Graph Temperature', $
			DEVLIST=devlist, $
                        VALUE=grselval, FONT=font)
  widget_control, base, map=1-os.flipval

		;**** Overall base for text output ****

  textbase = widget_base(flipbase, /column)

		;**** Widget for text output ****

  outfval = { OUTBUT:os.texout, APPBUT:os.texapp, REPBUT:os.texrep, $
	      FILENAME:os.texdsn, DEFNAME:os.texdef, MESSAGE:os.texmes }
  base = widget_base(textbase,/row,/frame)
  paperid = cw_adas_outfile(base, OUTPUT='Text Output', $
                        VALUE=outfval, FONT=font)

		;**** Widget for contour passing file ****

  outfval = { OUTBUT:os.conout, APPBUT:os.conapp, REPBUT:os.conrep, $
	      FILENAME:os.condsn, DEFNAME:os.condef, MESSAGE:os.conmes }
  base = widget_base(textbase,/row,/frame)
  contid = cw_adas_outfile(base, OUTPUT='Contour File', $
                        VALUE=outfval, FONT=font)

		;**** Widget for METPOP file ****

  outfval = { OUTBUT:os.metout, APPBUT:os.metapp, REPBUT:os.metrep, $
	      FILENAME:os.metdsn, DEFNAME:os.metdef, MESSAGE:os.metmes }
  base = widget_base(textbase,/row,/frame)
  metpid = cw_adas_outfile(base, OUTPUT='METPOP File', $
                        VALUE=outfval, FONT=font)
  widget_control, textbase, map=os.flipval

		;**** Error message ****

  messid = widget_label(cwid,value=' ',font=font)

		;**** add the exit buttons ****

  base = widget_base(cwid,/row)
  menufile = bitfile + '/menu.bmp'
  read_X11_bitmap, menufile, bitmap1
  outid = widget_button(base, value=bitmap1)          ;menu button
  cancelid = widget_button(base,value='Cancel',font=font)
  doneid = widget_button(base,value='Done',font=font)
  
		;*************************************************
		;**** create a state structure for the pop-up ****
		;**** window.                                 ****
		;*************************************************

  new_state = { GRPID		:	grpid, 				$
		OUTID		:	outid,				$
		PAPERID		:	paperid, 			$
		CONTID		:	contid, 			$
		METPID		:	metpid, 			$
		CANCELID	:	cancelid, 			$
		DONEID		:	doneid, 			$
		MESSID		:	messid,				$
		GRAPHBASE	:	graphbase,			$
		TEXTBASE	:	textbase,			$
		FLIPID		:	flipid				}

                ;**** Save initial state structure ****

  widget_control, first_child, set_uvalue=new_state, /no_copy

  RETURN, cwid

END

