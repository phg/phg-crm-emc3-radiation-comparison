; Copyright (c) 1998, Strathclyde University .
; SCCS info: Module @(#)$Header: /home/adascvs/idl/adas2xx/adas215/adas215_out.pro,v 1.1 2004/07/06 10:25:30 whitefor Exp $ Date $Date: 2004/07/06 10:25:30 $
;
;+
; PROJECT:
;       ADAS IBM MVS to UNIX conversion
;
; NAME:
;	ADAS215_OUT
;
; PURPOSE:
;	IDL ADAS user interface, output options.
;
; EXPLANATION:
;	This routine creates and manages a pop-up window which allows
;	the user to select options and input data to control ADAS215
;	graphical and file output.
;
; USE:
;	This routine is ADAS215 specific, see e6spf1.pro for an example
;	of how it is used.
;
; INPUTS:
;	VAL	- A structure which determines the initial settings of
;		  the output options widget.  The value is passed
;		  unmodified into cw_adas509_out.pro.
;
;		  See cw_adas506_out.pro for a full description of this
;		  structure.
;
;	DSFULL	- String; The full system file name of the input 
;		  dataset selected by the user for processing.
;
;       BITFILE - String; the path to the dirctory containing bitmaps
;                 for the 'escape to series menu' button.
;
;	IOSEL	- Integer; 1 => Text output selection made on processing screen
;			   0 => Not made. Corresponds to LOSEL in Fortran.
;
; OPTIONAL INPUTS:
;	None.
;
; OUTPUTS:
;	ACT	- String; 'Done' , 'Cancel' or 'Menu' for the button the
;		  user pressed to terminate the output options window.
;
; OPTIONAL OUTPUTS:
;	None.
;
; KEYWORD PARAMETERS:
;	DEVLIST	- A string array;  Each element of the array is the
;		  name of a hardcopy graphics output device.  This
;		  list must mirror the DEVCODE list.  DEVLIST is the
;		  list which the user will see.
;		  e.g ['Post-Script','HP-PCL','HP-GL']
;
;	FONT	- Supplies the font to be used for the interface widgets.
;
; CALLS:
;	CW_ADAS215_OUT	Creates the output options widget.
;	DYNLABEL	Recursively adds dynamic_resize keyword to label
;			widgets for version 4.0.1 and higher of IDL
;	XMANAGER
;	See side effects for widget management routine.
;
; SIDE EFFECTS:
;	This routine uses a common block OUT509_BLK to maintain its state.
;	ADAS215_OUT_EV	is included in this file and is called
;			indirectly from XMANAGER during widget
;			management.
;
; CATEGORY:
;	Widgets
;
; WRITTEN:
;       Hugh Summers, University of Strathclyde, 28/05/98         
;
; MODIFIED:
;	1.1	Hugh Summers
;  		First version
;
; VERSION:
;	1.1	28-05-98
;	
;-----------------------------------------------------------------------------


pro adas215_out_ev, event

  common out215_blk,action,value
	

  ;---------------------------------------------------------------------------
  ;	Find the event type and copy to common 
  ;---------------------------------------------------------------------------

    action = event.action

    case action of

  ;---------------------------------------------------------------------------
  ;	'Done' button
  ;---------------------------------------------------------------------------

	'Done'  : begin

  ;---------------------------------------------------------------------------
  ;	Get the output widget value
  ;---------------------------------------------------------------------------

     		     child = widget_info(event.id,/child)
		     widget_control,child,get_value=value 

  ;---------------------------------------------------------------------------
  ;	Kill the widget to allow IDL to
  ;	continue and interface with     
  ;	FORTRAN only if there is work   
  ;	for the FORTRAN to do.          
  ;---------------------------------------------------------------------------

		     if (value.grpout eq 1) or (value.texout eq 1) or	$
			(value.tcxout eq 1) then begin
		        widget_control,event.top,/destroy
		     endif 
	          end


  ;---------------------------------------------------------------------------
  ;	'Cancel' button
  ;---------------------------------------------------------------------------

	'Cancel': widget_control,event.top,/destroy


  ;---------------------------------------------------------------------------
  ;	'Menu' button
  ;---------------------------------------------------------------------------

	'Menu': widget_control, event.top, /destroy

    endcase

end

;-----------------------------------------------------------------------------


pro adas215_out, val, dsfull, act, bitfile, iosel, devlist=devlist, font=font

  common out215_blk,action,value

  ;---------------------------------------------------------------------------
  ;	Copy value to common
  ;---------------------------------------------------------------------------

  value = val

  ;---------------------------------------------------------------------------
  ;	Set defaults for keywords
  ;---------------------------------------------------------------------------

  if not (keyword_set(font)) then font = ''
  if not (keyword_set(devlist)) then devlist = ''

  ;---------------------------------------------------------------------------
  ;	Pop-up a new widget           
  ;---------------------------------------------------------------------------

  ;---------------------------------------------------------------------------
  ;	create base widget
  ;---------------------------------------------------------------------------

  outid = widget_base(title='ADAS215 OUTPUT OPTIONS',xoffset=100,yoffset=0)

  ;---------------------------------------------------------------------------
  ;	Declare output options widget
  ;---------------------------------------------------------------------------

  cwid = cw_adas215_out(outid, dsfull, bitfile, iosel, value=value,  	$
			devlist=devlist, font=font )

  ;---------------------------------------------------------------------------
  ;	Realize the new widget
  ;---------------------------------------------------------------------------

  dynlabel, outid
  widget_control,outid,/realize

  ;---------------------------------------------------------------------------
  ;	make widget modal
  ;---------------------------------------------------------------------------

  xmanager, 'adas215_out',outid,event_handler='adas215_out_ev',		$
	    /modal,/just_reg
 
  ;---------------------------------------------------------------------------
  ;	Return the output value from common
  ;---------------------------------------------------------------------------

  act = action
  val = value

END

