; Copyright (c) 1998, Strathclyde University.
; SCCS info: Module @(#)$Header: /home/adascvs/idl/adas2xx/adas215/cw_multi_215.pro,v 1.4 2012/09/08 06:52:27 mog Exp $ Date $Date: 2012/09/08 06:52:27 $
;+
; PROJECT:
;	ADAS 
;
; NAME:
;	CW_MULTI_215()
;
; PURPOSE:
;	Multiple selection list and field input compound widget.
;       This particular version  is designed specifically for use 
;       with the adas215 routines.
;
; EXPLANATION:
;	This compound widget function produces a multiple selection list.
;       along with 2 input fields for the type of transition and the
;       Burgess C value. 
;	The selection list consists of toggle buttons, one for each item
;	in the list.  The buttons are stacked in columns of 10.  The
;	caller can specify the maximum number of selections which are
;	permitted.  This widget does not generate any events.
;
;	This file includes the event management and GET_VALUE (only)
;	routines.  There is no SET_VALUE as the initial state is set on
;       first calling the widget. The VALUE of this widget is a structure
;
;           value = { type  :  integer,
;                     cval  :  float,
;                     sel   :  integer      }
;         
;       where type is the Burgess definition (restricted 1,2,3 or 4),
;       cval is the user entered C-value and sel is an integer array,  
;       with 1 or 0 representing whether the transition is to be plotted
;       or not. A maximun, MAXSEL, number of selections is permitted.
;       WIDGET_CONTROL,GET_VALUE may be used to recover the
;	user's selections.  WIDGET_CONTROL,GET_VALUE will only give the
;	correct selections when the buttons have been managed with the
;	event handler MULSEL215_EVENT, ie. when WIDGET_EVENT or XMANAGER
;	have been used to trap widget events. 
;
; USE:
;	See the routine MULTI_215 for a working example of how to use
;	CW_MULTI_215.
;
;		WIDGET_CONTROL,selid,GET_VALUE=selected
;
;	The GET_VALUE keyword will only return the correct selections
;	if XMANAGER or WIDGET_EVENT have been in operation whilst the
;	user operated the selection list buttons.  This widget does not
;	generate any events, all button events are managed internally.
;
; INPUTS:
;       PARENT   - The ID of the parent widget.
;
;	LIST     - One dimensional string array of items for the
;		   selection list.  Each list item will be assigned
;		   a button.
;
; OPTIONAL INPUTS:
;	None.  See the keywords for additional controls.
;
; OUTPUTS:
;	The return value of this function is the ID of the compound
;	widget which is of type LONG integer.
;
; OPTIONAL OUTPUTS:
;	None
;
; KEYWORDS:
;	MAXSEL   - Maximum number of selections allowed.  The default is
;		   one.  If a SELECTED array is specified then the value
;		   of MAXSEL is set to the length of SELECTED.
;
;	UVALUE   - Supplies the user value for the widget.  This is 
;		   exclusively for the caller's use and has no effect on
;		   the operation of the widget.
;
;	FONT	 - Supplies the font to be used for the widget.
;
; CALLS:
;	None directly.  See SIDE EFFECTS below for a list of the widget
;	management routines.
;
; RESTRICTIONS:
;	None.
;
; SIDE EFFECTS:
;	Three other routines are included which are used to manage the
;	widget;
;
;	CW_MULTI215_ADVANCE()
;	MULSEL215_GET_VAL()
;	MULSEL215_EVENT()
;
; CATEGORY:
;	Compound Widget
;
; WRITTEN:
;	H. P. Summers, University of Strathclyde, 14-May-1998
;		Based on cw_multi_207.pro
;
; MODIFIED:
;       1.1     Hugh Summers	   14-May-1998
;               First release
;       1.2     Martin O'Mullane    25-Jan-1999
;               Total rewrite to display a panel through
;               which the data is moved. The comments have been
;               rewritten for the new behaviour. 
;       1.3     Martin O'Mullane    8-Oct-1999
;                - Change checking to allow for type 4.
;       1.4     Martin O'Mullane
;                - Change variable 'list' to 'thelist' to avoid conflict
;                  with the reserved function LIST introduced in IDL v8.0.

;
; VERSION:
;       1.1       14-May-1998
;       1.2       25-Jan-1999
;       1.3       8-Oct-1999 
;	1.4	  08-09-2012
;
;-----------------------------------------------------------------------------

FUNCTION mulsel215_get_val, id

		;**** Return to caller on error ****
  ON_ERROR, 2

		;**** Retrieve the state ****

  first_child = widget_info(id, /child)
  widget_control, first_child, get_uvalue = state


		;**** get present panel values ****
                
   iplus = state.npanel*state.nrow
   i2    = min([state.nrow, (state.listdim-iplus)])
   for i = 0, i2-1 do begin

     widget_control, state.list_type(i), get_value=itype
     if itype lt 1 or itype gt 4 then begin
        message = 'Type can only be in range 1 -- 4'
        widget_control,state.messid,set_value=message
     endif else begin
        state.tran.type(i+iplus) = itype
     endelse

     widget_control, state.list_cval(i), get_value=cval
     state.tran.cval(i+iplus) = cval

   endfor
   
		;**** A bit of a hack - make appropriate structure ****
		;**** to return to the calling program             ****

  sel = where(state.tran.sel eq 1)
  if n_elements(sel) lt state.maxsel then begin
    for i=n_elements(sel),state.maxsel-1 do begin
       sel = [sel,-1]
     endfor
  endif
  thelist = make_array(4,state.listdim,/string)
  for i=0,state.listdim-1 do begin
    if state.tran.sel[i] eq 1 then thelist[0,i]='p' else thelist[0,i]=' '
    thelist[1,i] = string(state.tran.type[i],format='(i1)')
    thelist[2,i] = string(state.tran.cval[i],format='(f5.1)')
    thelist[3,i] = state.tran.cfg[i]  
  endfor
  
  value = {SELECTED: sel, LIST:thelist}
  
  RETURN, value
  
;  RETURN, state.tran

END


;-----------------------------------------------------------------------------

PRO cw_multi215_advance, state, advance

		;**** store present panel before advancing ****
        
   iplus = state.npanel*state.nrow
   i2    = min([state.nrow, (state.listdim-iplus)])
   for i = 0, i2-1 do begin

     widget_control, state.list_type(i), get_value=itype
     state.tran.type(i+iplus) = itype

     widget_control, state.list_cval(i), get_value=cval
     state.tran.cval(i+iplus) = cval

   endfor


		;**** find out number of next panel    ****
		;**** cycle around in steps of advance ****

   nump = state.numpanels - 1    ; indexes start at 0

   state.npanel = state.npanel+advance
   if state.npanel gt nump then state.npanel = state.npanel - (nump+1)
   if state.npanel lt 0 then state.npanel = nump + state.npanel + 1


   iplus = state.npanel*state.nrow
   i2 = min([state.nrow, (state.listdim-iplus)])

   for i=0, i2-1 do begin

     widget_control,state.list_button(i),set_button=state.tran.sel(i+iplus)
     widget_control,state.list_type(i),set_value=state.tran.type(i+iplus)
     widget_control,state.list_cval(i),set_value=state.tran.cval(i+iplus)
     widget_control,state.list_cfg(i),set_value=state.tran.cfg(i+iplus)

   endfor

   if (i2 lt state.nrow) then begin
      for i=i2, state.nrow-1 do begin
         widget_control,state.list_button(i), map=0
         widget_control,state.list_type(i), map=0
         widget_control,state.list_cval(i), map=0
         widget_control,state.list_cfg(i), map=0
      endfor
   endif else begin
      for i=0,i2-1 do begin
         widget_control,state.list_button(i), map=1
         widget_control,state.list_type(i), map=1
         widget_control,state.list_cval(i), map=1
         widget_control,state.list_cfg(i), map=1
      endfor
   endelse

end



;-----------------------------------------------------------------------------

FUNCTION mulsel215_event, event

		;**** Base ID of compound widget ****
  parent=event.handler

		;**** Retrieve the state ****
  
  first_child = widget_info(parent, /child)
  widget_control, first_child, get_uvalue=state 
                

		;**** Remove existing error message ****
              
  str = string(replicate(32B, 40))
  widget_control,state.messid,set_value=str


		;************************
		;**** Process events ****
		;************************
                
		;***********************
		;**** Button events ****
		;***********************

                
    iplus = state.npanel*state.nrow
    i2    = min([state.nrow, (state.listdim-iplus)])
    
    for i = 0, i2-1 do begin
    
      if event.id eq state.list_button(i) then begin
        
        case state.tran.sel(i+iplus) of
                  
          1 : begin   ; turn it off
          
                state.numsel = state.numsel - 1
                state.tran.sel(i+iplus) = 0
                widget_control, state.list_button(i), set_button=0
             
              end
              
          0 : begin
          
                if state.numsel eq state.maxsel then begin
                  
                    message = 'Exceeded maximum number of selections'
                    widget_control,state.messid,set_value=message
                    widget_control, state.list_button(i), set_button=0
                    
                endif else begin
                
                   state.numsel = state.numsel + 1
                   state.tran.sel(i+iplus) = 1
                   widget_control, state.list_button(i), set_button=1
                   
                endelse
                
                end
                
           endcase
        endif
                 
    endfor
      
  
		;************************
		;**** Advance events ****
		;************************

  
  
  CASE event.id OF
  
    state.go_l_id  : cw_multi215_advance,state,-1
    state.go_ll_id : cw_multi215_advance,state,-3
    state.go_r_id  : cw_multi215_advance,state,1
    state.go_rr_id : cw_multi215_advance,state,3
        
  ELSE	:  
    
  ENDCASE
  
		;**** Update the panel number ****
                
  message   = 'Panel ' + string(state.npanel+1,format='(i3)') + $
              ' of '   + string(state.numpanels,format='(i3)')
  widget_control,state.panelid,set_value=message

		;**** Reset the value of the state variable ****
  widget_control, first_child, set_uvalue=state , /no_copy

  RETURN, 0L
  
END

;-----------------------------------------------------------------------------

FUNCTION cw_multi_215,	parent, value=value,  NROW=nrow,  $
			MAXSEL = maxsel,  $
			UVALUE = uvalue, FONT=font, _extra=eee

		;**** Flag error and return to caller ****
  IF (N_PARAMS() LT 1) THEN MESSAGE, $
		'Must specify a PARENT for cw_multi_215'
  ON_ERROR, 2

		;**** Set defaults for keywords ****
  IF NOT (KEYWORD_SET(maxsel)) THEN maxsel = 1
  IF NOT (KEYWORD_SET(nrow))   THEN nrow = 10
  IF NOT (KEYWORD_SET(uvalue)) THEN uvalue = 0
  IF NOT (KEYWORD_SET(font))   THEN font = ''

  IF NOT (KEYWORD_SET(value)) THEN begin
  
    message, 'cw_multi_215 cannot operate without an input'
    
  endif else begin
  
    tran = { type  :  value.type,     $       
    	     cval  :  value.cval,     $       
    	     cfg   :  value.cfg,      $       
    	     sel   :  value.sel       }
            
  endelse	

  ON_ERROR, 2




  cwid = WIDGET_BASE(parent, UVALUE = uvalue, 	$
		EVENT_FUNC     = "mulsel215_event", 	$
		FUNC_GET_VALUE = "mulsel215_get_val", 	$
		/COLUMN)
                
  
		;******************************************************
		;**** Create a dummy widget just to hold value of *****
		;**** "state" variable so as not to get confused  *****
		;**** with any other values. Adopt IDL practice   *****
		;**** of using first child widget                 *****
		;******************************************************

  first_child = widget_base(cwid,/COLUMN)
                


		;**** Create top and bottom areas ****
                
  topbase = widget_base(first_child,/column)
  botbase = widget_base(first_child,/column,/align_center)


		;**** Give some instructions ****
                
  instbase = widget_base(topbase,/column)
        
  str = 'Select transitions for plotting and Burgess C-value    ( max of' +$
        string(maxsel,format='(i3)')+')' 
       
  inst = widget_label(instbase, value=str,font=font)      
  inst = widget_label(instbase, value=' ',font=font)      
  str = 'Type 1: Electric Dipole'      
  inst = widget_label(instbase, value=str,font=font,/align_left)      
  str = '     2: Non-Electric Dipole, non-exchange'      
  inst = widget_label(instbase, value=str,font=font,/align_left)      
  str = '     3: Exchange'      
  inst = widget_label(instbase, value=str,font=font,/align_left)      


		;**** See how many blocks are required ****
                
  listdim = n_elements(tran.type)
  nblocks = fix((listdim+nrow-1)/nrow)

		;**** Create list of button and text widgets ****
                
  list_base   = lonarr(listdim)         ; base for button
  list_button = lonarr(listdim)         ; actual button
  list_type   = lonarr(listdim)         ; type of transition
  list_cval   = lonarr(listdim)         ; Burgess C value
  list_cfg    = lonarr(listdim)         ; configuration details

  listbase = widget_base(topbase,/column,/frame)
  
		;**** reserve space for nrows ****
  
  ;??? is it necessary?
  
		;**** display the first set at the beginning ****
  
  numbases  = min([listdim,nrow])
  numpanels = ceil(float(listdim)/nrow)
  npanel    = 0
  
  for i = 0, numbases-1 do begin
  
      listbase2      = widget_base(listbase,/row)
      list_base(i)   = widget_base(listbase2,/nonexclusive,/row)
      list_button(i) = widget_button(list_base(i), value='', font=font)

      list_type(i) = cw_field(listbase2, xsize=2, /integer, fieldfont=font, $
                              value=tran.type(i), title=' ')    

      list_cval(i) = cw_field(listbase2, xsize=5, /floating, fieldfont=font, $
                              value=tran.cval(i), title=' ')    
                                   
      list_cfg(i)  = widget_label(listbase2, value = tran.cfg(i), font=font)
                                   
  endfor

		;**** Set advance / retract frame buttons ****
  
  bitfile=getenv('ADASCENT')   + '/bitmaps/'             
  go_l_file = bitfile + 'go_l.bmp'
  read_X11_bitmap, go_l_file, go_l_bmp
  go_ll_file = bitfile + 'go_ll.bmp'
  read_X11_bitmap, go_ll_file, go_ll_bmp
  go_r_file = bitfile + 'go_r.bmp'
  read_X11_bitmap, go_r_file, go_r_bmp
  go_rr_file = bitfile + 'go_rr.bmp'
  read_X11_bitmap, go_rr_file, go_rr_bmp

  base = widget_base(botbase,/row)
  go_ll_id  = widget_button(base,value=go_ll_bmp)
  go_l_id   = widget_button(base,value=go_l_bmp)
  message   = 'Panel   1 of '+string(numpanels,format='(i3)')
  panelid   = widget_label(base,value=message,font=font)
  go_r_id   = widget_button(base,value=go_r_bmp)
  go_rr_id  = widget_button(base,value=go_rr_bmp)


		;**** Set buttons for current selections and ****
		;**** see how many selections exist.         ****
                
  for i= 0, numbases-1 do begin
    widget_control,list_button(i),set_button=tran.sel(i)
  endfor
  
  numsel = n_elements(where(tran.sel eq 1))

		;**** Create error message ****
                
  str = string(replicate(32B, 40))
  messid = widget_label(botbase, value=str, font=font)

		;**** Create state structure ****
                
  new_state =  {  list_button :  list_button,	$
		  list_type   :  list_type,	$
		  list_cval   :  list_cval,	$
		  list_cfg    :  list_cfg,	$
		  listdim     :  listdim,	$
		  messid      :  messid,	$
		  tran        :  tran,		$
		  maxsel      :  maxsel,	$
		  numsel      :  numsel,	$
		  numpanels   :  numpanels,	$
		  npanel      :  npanel,	$
		  nrow        :  nrow,   	$
		  panelid     :  panelid,	$
                  go_l_id     :  go_l_id,	$
                  go_ll_id    :  go_ll_id,	$
                  go_r_id     :  go_r_id,	$
                  go_rr_id    :  go_rr_id	}

		;**** Save initial state structure ****
                
  widget_control, first_child, set_uvalue=new_state, /no_copy

  RETURN, cwid

END
