; Copywrite (c) 1998, Strathclyde University .
; SCCS info: Module @(#)$Header: /home/adascvs/idl/adas2xx/adas215/bfoutg.pro,v 1.1 2004/07/06 11:39:15 whitefor Exp $ Date $Date: 2004/07/06 11:39:15 $
;
;+
; PROJECT:
;       ADAS IBM MVS to UNIX conversion
;
; NAME:
;	BFOUTG
;
; PURPOSE:
;	Communication with ADAS215 FORTRAN via UNIX pipe and
;	graphics output.
;
; EXPLANATION:
;	The routine begins by reading information from the ADAS215
;	FORTRAN process via a UNIX pipe.  Then the IDL graphical
;	output routine for ADAS5215 is invoked.  Communications are to
;	the FORTRAN subroutine BFOUTG.
;
; USE:
;	The use of this routine is specific to ADAS215 see adas215.pro.
;
; INPUTS:
;	DSFULL   - Data file name
;	PIPE	 - The IDL unit number of the bi-directional pipe to the
;		   ADAS215 FORTRAN process.
;
;	UTITLE   - Optional comment by user
;
;	GRPSCAL	 - Integer; 0 if default scaling required 1 if user
;		   specified scaling to be used.
;
;	XMIN	 - String; User specified x-axis minimum, number as string.
;
;	XMAX	 - String; User specified x-axis maximum, number as string.
;
;	YMIN	 - String; User specified y-axis minimum, number as string.
;
;	YMAX	 - String; User specified y-axis maximum, number as string.
;
;	HRDOUT	 - Integer; 0 if no hardcopy required, 1 if hardcopy.
;
;	HARDNAME - String; file name for hardcopy output.
;
;	DEVICE	 - String; IDL name for hardcopy output device.
;
;	HEADER	 - ADAS version header information for inclusion in the
;		   graphical output.
;
; OPTIONAL INPUTS:
;	None.
;
; OUTPUTS:
;	None.
;
; OPTIONAL OUTPUTS:
;	None.
;
; KEYWORD PARAMETERS:
;	FONT	- String; Name of the font to be used for the graphical
;		  output widget.
;
; CALLS:
;	ADAS509_PLOT	ADAS504 graphical output.
;
; SIDE EFFECTS:
;	This routine reads information from FORTRAN via a UNIX pipe.
;
; CATEGORY:
;	Adas system.
;
; WRITTEN:
;       Hugh Summers, University of Strathclyde,  01/06/1998
;
; MODIFIED:
;	1.1	Hugh Summers
;               First edition				
;
; VERSION:
;	1.1	01/06/1998
;

;-----------------------------------------------------------------------------

PRO bfoutg, dsfull, pipe, utitle, grpscal, xmin, xmax, ymin, ymax,     $
	    hrdout, hardname, device, header, bitfile, gomenu, FONT=font

		;***********************************
                ;**** Set defaults for keywords ****
		;***********************************

    IF NOT (KEYWORD_SET(font)) THEN font = ''

		;*************************************
		;**** Declare variables for input ****
		;*************************************

    sdum = " "
    idum = 0
    fdum = 0.0
    ddum = 0.0d0
    strg = make_array(9, /string, value=" ")

		;********************************
		;**** Read data from fortran ****
		;********************************

    readf, pipe, idum
    nvmax = idum
    readf, pipe, idum
    ndplot = idum
    
    readf, pipe, format = '(a40)' , sdum
    title = sdum
    readf, pipe, format = '(a120)', sdum
    titlx = sdum
    readf, pipe, format = '(a8)' , sdum
    date = sdum

    readf, pipe, idum
    iz0 = idum
    readf, pipe, idum
    iz = idum
    
    readf, pipe, idum
    nplot = idum
    
		;***************************************
		;**** declare some array dimensions ****
		;***************************************
		
    idxplt  = intarr(ndplot)
    typplt  = strarr(ndplot)
    cplt    = dblarr(ndplot)		

    for i=0, nplot-1 do begin
        readf, pipe, idum
        idxplt(i) = idum 
    endfor
    
    for i=0, nplot-1 do begin
        readf, pipe, sdum
        typplt(i) = sdum 
    endfor
    
    for i=0, nplot-1 do begin
        readf, pipe, ddum
        cplt(i) = ddum 
    endfor
    
		;**************************************
		;**** declare more array dimensions ****
		;**************************************

    tein   = dblarr(nvmax) 
    yinplt = dblarr(nvmax,ndplot)
    teout  = dblarr(nvmax) 
    youplt = dblarr(nvmax,ndplot)
    
    readf, pipe, idum
    nv = idum
     
    for i=0, nv-1 do begin
        readf, pipe, ddum
        tein(i) = ddum 
    endfor
    
    for k=0, nplot-1 do begin
      for i=0, nv-1 do begin
        readf, pipe, ddum
        yinplt(i,k) = ddum 
      endfor
    endfor
     
    readf, pipe, idum
    nvn = idum
     
    for i=0, nvn-1 do begin
        readf, pipe, ddum
        teout(i) = ddum 
    endfor
    
    for k=0, nplot-1 do begin
      for i=0, nvn-1 do begin
        readf, pipe, ddum
        youplt(i,k) = ddum 
      endfor
    endfor
    
    readf, pipe, idum
    ldef1 = idum
    
    for i = 0, 5 do begin
        readf, pipe, sdum 
        strg(i) = sdum
    endfor
    readf, pipe, sdum, format = '(a32)'
    head1 = sdum
    readf, pipe, sdum, format = '(a32)'
    head2 = sdum
    readf, pipe, sdum, format = '(a32)'
    head3 = sdum

		;***********************
		;**** Plot the data ****
		;***********************

    adas215_plot, dsfull, nvmax, ndplot, 				$
  		  title , titlx, utitle, date,  iz0, iz, 		$
  		  nplot , idxplt, typplt, cplt, 	              	$
		  tein  , yinplt, nv, teout, youplt, nvn,              	$
                  ldef1 , xmin , xmax  , ymin , ymax,                 	$
   		  strg  , head1, head2, head3,                        	$
		  hrdout, hardname, device, header, bitfile, gomenu,  	$
                  FONT=font

END
