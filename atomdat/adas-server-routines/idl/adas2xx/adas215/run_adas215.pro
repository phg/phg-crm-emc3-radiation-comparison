;----------------------------------------------------------------------
;+
; PROJECT    :  ADAS
;
; NAME       :  run_adas215
;
; PURPOSE    :  Runs ADAS215 adf04 Te extrapolation code as an IDL
;               subroutine.
;
; ARGUMENTS  :  All output arguments will be defined appropriately.
;
;               NAME      I/O    TYPE   DETAILS
; INPUTS     :  adf04      I     str    input driver file.
;               outfile    I     str    output projection file.
;               te         I     flt()  Required temperaures.
;               log        I     str    name of output text file.
;                                       (defaults to no output).
;
; KEYWORDS   :  kelvin     - use K not eV for temperature.
;               help       - displays this section.
;
; OUTPUTS    :  None
;
; NOTES      :  - run_adas215 used the spawn command. Therefore IDL v5.3
;                 cannot be used. Any other version will work.
;
; AUTHOR     :  Martin O'Mullane
;
; DATE       :  16-01-2007
;
; UPDATE     :
;       1.1     Martin O'Mullane
;                 - First version.
;
; VERSION    :
;       1.1    16-01-2007
;-
;----------------------------------------------------------------------

PRO run_adas215, adf04    = adf04,    $
                 outfile  = outfile,  $
                 log      = log,      $
                 te       = te,       $
                 kelvin   = kelvin,   $
                 help     = help


; If asked for help

if keyword_set(help) then begin
   doc_library, 'run_adas215'
   return
endif

; First check that we are not using IDL v5.3

if !version.release EQ '5.3' then begin
   message, 'This version of IDL (v5.3) will not work - ' + $
            'change to v5.0, v5.1, v5.2 or higher'
   return
endif


; Check that we get all inputs and that they are correct. Otherwise print
; a message and return to command line.

on_error, 2

if n_elements(adf04) EQ 0 then begin
    message,'An input adf04 file is required'
endif

; Suitable defaults

if n_elements(outfile) EQ 0 then begin
   outfile = 'run_adas215_adf04.pass'
   message, 'Output adf04 filename : ' + outfile, /continue
endif


; File outputs

if n_elements(log) EQ 0 then begin
   log_file = ''
   is_log = 0
endif else begin
   log_file = log
   is_log = 1
endelse

; Te input

if n_elements(te) eq 0 then message, 'User requested temperatures are missing'

partype=size(te, /type)
if (partype lt 2) or (partype gt 5) then begin
   message,'Temperature must be numeric'
endif  else te = DOUBLE(te)

; Kelvin rather than eV

if keyword_set(kelvin) then te = te/11605.0


; Now run the ADAS215 code

; location of files etc.

fortdir = getenv("ADASFORT")
adas_c  = getenv("ADASCENT")
adas_u  = getenv("ADASUSER")

; Input screen

spawn, fortdir+'/adas215.out', unit=pipe, /noshell, PID=pid


date = xxdate()
printf, pipe, date[0]

header = 'Produced by run_adas215'


; Input screen

rep = 'NO'
printf, pipe, rep
printf, pipe, adf04

titled = ''
sz     = ''
sz0    = ''
sionpt = ''
scnte  = ''
sil    = ''
dsninc = ''
ndtem  = 0
input  = ''

readf, pipe, titled
readf, pipe, sz
readf, pipe, sz0
readf, pipe, sionpt
readf, pipe, scnte
readf, pipe, sil
readf, pipe, input
dsninc = strtrim( input )

readf, pipe, ndtem
tea = strarr(ndtem)

for i = 0, ndtem-1 do begin
  readf, pipe, input
  tea[i] = input
end


; Processing screen

lpend = 0
ndlne = 0
nvmax = 14
il    = 0
nv    = 0
icnte = 0
icnth = 0
icntr = 0
idum  = 0
ldum  = 0L
sdum  = ' '
fdum  = 0.0

readf, pipe, idum
lpend = idum
readf, pipe, idum
ndlne = idum
readf, pipe, idum
nvmax = idum
readf, pipe, idum
il = idum
readf, pipe, idum
nv = idum
tscef = dblarr(nvmax,3)
for i = 0, (nvmax-1) do begin
    for j = 0, 2 do begin
        readf, pipe, fdum
        tscef[i,j] = fdum
    endfor
endfor
readf, pipe, idum
icnte = idum
readf, pipe, idum
icnth = idum
readf, pipe, idum
icntr = idum

ie1a   = lonarr(icnte)
ie2a   = lonarr(icnte)
strga  = strarr(il)
pecode = strarr(icnte)
tecode = strarr(icnte)
cea    = dblarr(icnte)

for i=0, (icnte-1) do begin
    readf, pipe, idum
    ie1a[i] = idum
endfor
for i=0, (icnte-1) do begin
    readf, pipe, idum
    ie2a[i] = idum
endfor
for i=0, (il-1) do begin
    readf, pipe, sdum
    strga[i] = sdum
endfor
for i=0, (icnte-1) do begin
    readf, pipe, sdum
    pecode[i] = sdum
endfor
for i=0, (icnte-1) do begin
    readf, pipe, sdum
    tecode[i] = sdum
endfor
for i=0, (icnte-1) do begin
    readf, pipe, fdum
    cea[i] = fdum
endfor

printf, pipe, 0L
printf, pipe, 0L

nte = n_elements(te)
printf, pipe, 2L
printf, pipe, nte
for j = 0, nte-1 do printf, pipe, te[j]


lpend = 0
printf, pipe, lpend


; Output screen

iosel = 0L
readf, pipe, iosel

lpend = 0
printf, pipe, lpend

printf, pipe, 0L    ; No graphics

if is_log then begin
   printf, pipe, 1L
   printf, pipe, log_file, format='(A)'
   printf, pipe, header
   printf, pipe, 1L
endif else begin
   printf, pipe, 0L
endelse

printf, pipe, 1L
printf, pipe, outfile, format='(A)'

printf, pipe, 1L

; Terminate FORTRAN process and close up everything open

wait, 0.9
close, /all

; Reset temperature to K

if keyword_set(kelvin) then te = te*11605.0

END
