; Copyright (c) 1998, University of Strathclyde.
; SCCS info: Module @(#)$Header: /home/adascvs/idl/adas2xx/adas215/bfspf0.pro,v 1.1 2004/07/06 11:39:32 whitefor Exp $ Date $Date: 2004/07/06 11:39:32 $
;+
; PROJECT:
;       ADAS IBM MVS to UNIX conversion
;
; NAME:
;	BFSPF0
;
; PURPOSE:
;	IDL user interface and communications with ADAS215 FORTRAN
;	process via pipe.
;
; EXPLANATION:
;	Firstly this routine invokes the part of the user interface
;	used to select the input dataset for ADAS215.  When the user's
;	interactions are complete this routine communicates with the
;	ADAS215 FORTRAN application via a UNIX pipe.  Communications
;	are to the FORTRAN subroutine BFSPF0.
;
; USE:
;	The use of this routine is specific to ADAS215, see adas215.pro.
;
; INPUTS:
;	PIPE	- The IDL unit number of the bi-directional pipe to the
;		  ADAS215 FORTRAN process.
;
;	VALUE	- A structure which determines the initial settings of
;		  the dataset selection widget.  The initial value is
;		  set in adas215.pro.  VALUE is passed un-modified
;		  through to cw_adas_in.pro, see that routine for a full
;		  description.
;
; OPTIONAL INPUTS:
;	None
;
; OUTPUTS:
;	VALUE	- On output the structure records the final settings of
;		  the dataset selection widget if the user pressed the
;		  'Done' button otherwise it is not changed from input.
;
;	DSNPAS	- String; The full system file name of the CONTOUR Pass
;		  dataset selected by the user for processing.
;
;	REP	- String; Indicates whether the user pressed the 'Done'
;		  or 'Cancel' button on the interface.  The action is
;		  converted to the strings 'NO' and 'YES' respectively
;		  to match up with the existing FORTRAN code.  In the
;		  original IBM ISPF interface REP was the reply to the
;		  question 'DO YOU WANT TO END PROGRAM EXECUTION'.
;
; OPTIONAL OUTPUTS:
;	None
;
; KEYWORD PARAMETERS:
;	FONT	- Supplies the font to be used for the interface widgets.
;
; CALLS:
;	ADAS_IN		Pops-up the dataset selection widget.
;
; SIDE EFFECTS:
;	This routine communicates with the ADAS207 FORTRAN process
;	via a UNIX pipe.
;
; CATEGORY:
;	Adas system.
;
; WRITTEN:
;       H. P. Summers, University of Strathlcyde, 06/05/1998
;
; MODIFIED:
;	  1.1	Hugh Summers
;		First version
;
; VERSION:
;	  1.1 06-05-98
;
;-----------------------------------------------------------------------------


PRO bfspf0, pipe, value, dsnpas, rep, FONT=font


                ;**** Set defaults for keywords ****
  IF NOT (KEYWORD_SET(font)) THEN font = ''


		;**********************************
		;**** Pop-up input file widget ****
		;**********************************
  adas_in, value, action, WINTITLE = 'ADAS 215 INPUT', $
	TITLE = 'Input CONTOUR Passing File', FONT = font

		;******************************************
		;**** Act on the event from the widget ****
		;******************************************
		;**** There are only two possible events ****
		;**** 'Done' and 'Cancel'.               ****
		;********************************************
  if action eq 'Done' then begin
    rep = 'NO'
  end else begin
    rep = 'YES'
  end

		;**** Construct datatset name ****
  dsnpas = value.rootpath+value.file


		;*******************************
		;**** Write data to fortran ****
		;*******************************
  printf,pipe,rep
  printf,pipe,dsnpas


END
