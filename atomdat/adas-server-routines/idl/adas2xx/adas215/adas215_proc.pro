; Copyright (c) 1998 Strathclyde University .
; SCCS info: Module @(#)$Header: /home/adascvs/idl/adas2xx/adas215/adas215_proc.pro,v 1.1 2004/07/06 10:25:35 whitefor Exp $ Date $Date: 2004/07/06 10:25:35 $
;+
; PROJECT:
;       ADAS IBM MVS to UNIX conversion
;
; NAME:
;	ADAS215_PROC
;
; PURPOSE:
;	IDL ADAS user interface, processing options/input.
;
; EXPLANATION:
;	This routine creates and manages a pop-up window which allows
;	the user to select options and input data to control ADAS215
;	processing.
;
; USE:
;	This routine is ADAS215 specific, see bfispf.pro for how it
;	is used.
;
; INPUTS:
;	VAL	- A structure which determines the initial settings of
;		  the processing options widget.  The value is passed
;		  unmodified into cw_adas215_proc.pro.
;
;		  See cw_adas215_proc.pro for a full description of this
;		  structure.
;
;	TITLED	- String, Ion name.
;
;	SZ	- String, Recombined ion charge.
;
;	SZ0	- String, Nuclear charge.
;
;	SIONPT	- String, Ionisation potential.
;
;	DSNPAS	- String, The full system file name of the input
;			Contour Passing file.
;	DSNINC	- String, The full system file name of the input COPASE
;			dataset selected by the user for processing.
;
;	NVMAX	- Integer, Max. no. of electron temperatures.
;
;	NV	- Integer, Number of input electron temperatures.
;
;	TSCEF	- float array, input electron temperatures.
;
;	ICNTE	- Integer, Number of Electron Impact Transitions.
;
;	IL	- Integer, Number of energy levels.
;
;	IE1A	- Integer array, Lower level index of transition
;
;	IE2A	- Integer array, Upper level index of transition
;
;	STRGA	- String array, Level designations
;
;	IFOUT	- Integer, Form of output temperatures.
;
;	NVN	- Integer, Number of output electron temperatures.
;
;	TOUT	- Float array, output temperatures (K)
;
;       BITFILE - String; the path to the dirctory containing bitmaps
;                 for the 'escape to series menu' button.
;
; OPTIONAL INPUTS:
;	None
;
; OUTPUTS:
;	VAL	- On output the structure records the final settings of
;		  the processing selections widget if the user pressed the
;		  'Done' button, otherwise it is not changed from input.
;
;	ACT	- String; Either 'Done', 'Cancel' or 'Menu' for the button the
;		  user pressed to terminate the processing options
;		  window.
;
; OPTIONAL OUTPUTS:
;	None
;
; KEYWORD PARAMETERS:
;	FONT_LARGE - The name of a larger font.
;
;	FONT_SMALL - The name of a smaller font.
;
;	EDIT_FONTS - A structure of two fonts used in the table editor
;		     adas_edtab.pro, {font_norm:'',font_input:''}.
;
; CALLS:
;	CW_ADAS215_PROC	Declares the processing options widget.
;	ADAS215_PROC_EV Called indirectly during widget management,
;			routine included in this file.
;	XMAMAGER
;
; SIDE EFFECTS:
;	This routine uses a common block PROC215_BLK in the management
;	of the pop-up window.
;
; CATEGORY:
;	Widgets
;
; WRITTEN:
;       H. P. Summers, University of Strathclyde, 7/5/98
;
; MODIFIED:
;	1.1	Hugh Summers
;		First version.
;
; VERSION:
;	1.1	07-05-98
;
;-----------------------------------------------------------------------------


PRO adas215_proc_ev, event

  COMMON proc215_blk,action,value

		;**** Find the event type and copy to common ****
    action = event.action

    CASE action OF

		;**** 'Done' button ****
	'Done'  : begin

			;**** Get the output widget value ****
		widget_control,event.id,get_value=value 

		widget_control,event.top,/destroy

	   end


		;**** 'Cancel' button ****
	'Cancel': widget_control,event.top,/destroy

		;**** 'Menu' button ****
	'Menu': widget_control,event.top,/destroy

    END

END

;-----------------------------------------------------------------------------

PRO adas215_proc, val, newflag, titled, sz, sz0, sionpt, dsnpas, dsninc, 	$
		  nvmax, nv, tscef, icnte, il, pecode, tecode, cea, 	$
		  ie1a, ie2a, strga, act, ifout, bitfile,	$
		  FONT_LARGE=font_large, FONT_SMALL=font_small, 	$
		  EDIT_FONTS=edit_fonts

  COMMON proc215_blk,action,value

		;**** Copy value to common ****
  value = val

		;**** Set defaults for keywords ****

  IF NOT (KEYWORD_SET(font_large)) THEN font_large = ''
  IF NOT (KEYWORD_SET(font_small)) THEN font_small = ''
  IF NOT (KEYWORD_SET(edit_fonts)) THEN $
				edit_fonts = { font_norm:'', font_input:''}

                ;**** create base widget ****

  procid = widget_base(TITLE='ADAS215 PROCESSING OPTIONS', $
					XOFFSET=1,YOFFSET=0)

		;**** Declare processing widget ****

  cwid = cw_adas215_proc(procid, titled, sz, sz0, sionpt, dsnpas, 	$
                         dsninc, nvmax, nv, tscef, icnte, il,		$
			 pecode, tecode, cea, newflag,			 	$
                         ie1a, ie2a, strga, ifout, bitfile,	$
			 VALUE=value, FONT_LARGE=font_large, 		$
                         FONT_SMALL=font_small, EDIT_FONTS=edit_fonts)

		;**** Realize the new widget ****

  dynlabel, procid
  widget_control,procid,/realize

		;**** make widget modal ****

  xmanager,'adas215_proc',procid,event_handler='adas215_proc_ev', $
					/modal,/just_reg
 
		;**** Return the output value from common ****

  act = action
  val = value

END

