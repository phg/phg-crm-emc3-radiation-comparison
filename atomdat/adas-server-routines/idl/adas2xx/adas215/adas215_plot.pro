; Copyright (c) 1998, Strathclyde University.
; SCCS info: Module @(#)$Header: /home/adascvs/idl/adas2xx/adas215/adas215_plot.pro,v 1.1 2004/07/06 10:25:32 whitefor Exp $ Date $Date: 2004/07/06 10:25:32 $
;+
; PROJECT:
;       ADAS IBM MVS to UNIX conversion
;
; NAME:
;       ADAS215_PLOT
;
; PURPOSE:
;       Generates ADAS215 interpolated upsilon graphical output.
;
; EXPLANATION:
;       This routine creates a window for the display of graphical
;       output. A separate routine PLOT215 actually plots the
;       graph.
;
; USE:
;       This routine is specific to the plotting section 
;	of ADAS215.
;
; INPUTS:
;       DSFULL  - String; Name of data file
;
;       NVMAX   - Integer; Maximum number of temperatures
;
;       NDPLOT  - Integer; Maximum number of distinct plots
;
;	TITLE   - String array; titles to be placed above graph
;
;	TITLX   - String; user supplied comment appended to end of title
;
;       UTITLE  - String; Optional comment by user added to graph title.
;
;	NCURV	- Integer; Number of upsilon output curves
;
;	IDXPLT	- Intarr; Index of plot in e-impact transition list
;
;	TYPPLT	- Strarr; Burgess-Tully type code for plot
;
;	CPLT	- Dblarr; Burgess-Tully C parameter of plot
;
;	TEIN	- Dblarr; electron temperatures from adf04 file
;
;	YINPLT	- Dblarr; upsilons from adf04 file for plotting
;
;	NV	- Integer; Number of temperatures from adf04 file
;
;	TEOUT	- Dblarr; electron temperatures for adf04 file
;
;	YOUPLT	- Dblarr; upsilons for adf04 file for plotting
;
;	NVN	- Integer; Number of temperatures for adf04 file
;
;       LDEF    - Integer; 1 - use user entered graph scales
;                          0 - use default axes scaling
;
;       XMIN    - String; Lower limit for x-axis of graph, number as string.
;
;       XMAX    - String; Upper limit for x-axis of graph, number as string.
;
;       YMIN    - String; Lower limit for y-axis of graph, number as string.
;
;       YMAX    - String; Upper limit for y-axis of graph, number as string.
;
;       STRG    - Strarr; Annotation strings.
;
;       HEAD1   - String; Annotation header string.
;
;       HEAD2   - String; Annotation header string.
;
;       HEAD3   - String; Annotation header string.
;
;       HRDOUT - Integer; 1 if hardcopy output activated, 0 if not.
;
;       HARDNAME- String; Filename for harcopy output.
;
;       DEVICE  - String; IDL name of hardcopy output device.
;
;       HEADER  - String; ADAS version number header to include in graph.
;
;	BITFILE - String; the path to the directory containing bitmaps
;                 for the 'escape to series menu' button.
;
; OPTIONAL INPUTS:
;       None.
;
; OUTPUTS:
;       GOMENU  - Int; flag - set to 1 if user has selected 'escape direct
;                 to series menu', otherwise 0
;
; OPTIONAL OUTPUTS:
;       None.
;
; KEYWORD PARAMETERS:
;       FONT    - String; The name of a font to use for text in the
;                 graphical output widget.
;
; CALLS:
;       CW_ADAS_GRAPH   Graphical output widget.
;       PLOT215         Make one plot to an output device for 215.
;       XMANAGER
;
; SIDE EFFECTS:
;       This routine uses a common block to maintain its state 
;	PLOT215_BLK.
;
;       One other routine is included in this file;
;       ADAS215_PLOT_EV Called via XMANAGER during widget management.
;
; CATEGORY:
;       Adas system.
;
; WRITTEN:
;       H. P. Summers, University of Strathclyde, 01/06/1998
;
; MODIFIED:
;       1.1     Hugh Summers
;               First version
;
; VERSION:
;       1.1     01-06-98
;
;
;----------------------------------------------------------------------------

PRO adas215_plot_ev, event

    COMMON plot215_blk, action, plotdev, plotfile, fileopen, win, 	$
                        iplot, nplot, data, nsum, gomenu

    newplot = 0
    print = 0
    done = 0
                ;****************************************
                ;**** Set graph and device requested ****
                ;****************************************

    CASE event.action OF

        'previous' : begin
            if iplot gt 0 then begin
                newplot = 1
                iplot = iplot - 1
                first = iplot
                last = iplot
            endif
	end

        'next'     : begin
            if iplot lt nplot-1 then begin
                newplot = 1
                iplot = iplot + 1
                first = iplot
                last = iplot
            endif
        end

        'print'    : begin
            newplot = 1
            print = 1
	    first = iplot
	    last = iplot
        end

        'printall' : begin
            newplot = 1
            print = 1
            first = 0
            last = nplot-1
        end

        'done'     : begin
            if fileopen eq 1 then begin
                set_plot, plotdev
                device, /close_file
            end
            set_plot,'X'
            widget_control, event.top, /destroy
            done = 1
        end

	'bitbutton' : begin
            if fileopen eq 1 then begin
                set_plot, plotdev
                device, /close_file
            end
            set_plot,'X'
            widget_control, event.top, /destroy
            done = 1
            gomenu = 1
        end

    END

                ;*******************************
                ;**** Make requested plot/s ****
                ;*******************************

    if done eq 0 then begin

                ;**** Set graphics device ****

        if print eq 1 then begin
            set_plot, plotdev
            if fileopen eq 0 then begin
                fileopen = 1
                device, filename=plotfile
                device, /landscape
            endif
        endif else begin
            set_plot,'X'
            wset, win
        endelse

                ;**** Draw graphics ****

        if newplot eq 1 then begin
            for i = first, last do begin
	        bottom = i * 7
	        top = bottom + 6
                if top gt (nsum-1) then top=nsum-1
                plot215, data.ldef, top, bottom,			$
			 data.idxplt, data.typplt, data.cplt,		$
			 data.tein, data.yinplt,			$
                	 data.teout, data.youplt,			$
                         data.strg, data.head1, data.head2, data.head3, $
			 data.title, data.xtitle, data.ytitle,		$
			 data.xmin, data.xmax, data.ymin, data.ymax
	        if print eq 1 then begin
                    message = 'Plot  written to print file.'
                    grval = {WIN:0, MESSAGE:message}
                    widget_control, event.id, set_value=grval
                endif
	    endfor
        endif
    endif

END

;----------------------------------------------------------------------------
                                                                
PRO adas215_plot, dsfull, nvmax, ndplot,				$
		  title, titlx,utitle, date, iz0, iz,			$
		  ncurv, idxplt, typplt, cplt, 				$
		  tein, yinplt,nv, teout, youplt,nvn,			$
		  ldef, xmin, xmax, ymin, ymax,				$
		  strg, head1, head2, head3,				$
                  hrdout, hardname, device, header, bitfile, gomenu, 	$
                  FONT=font

    COMMON plot215_blk, action, plotdev, plotfile, fileopen, win, 	$
                        iplot, nplot, data, nsum, gomenucom

    ngpic = 7			;max no. of lines per graph

                ;**** Copy input values to common ****

    plotdev = device
    plotfile = hardname
    fileopen = 0
    nsum = ncurv
    gomenucom = gomenu

		;*********************************************
		;**** Generate indices to be sent to plot ****
		;**** routine if there are to be multiple ****
		;**** plots drawn			  ****
		;*********************************************

    nplot = 0
    if ncurv le ngpic then begin
	bottom = 0
	top = (ncurv-1)
	nplot = 1
    endif else begin
	bottom = 0
	top = (ngpic-1)
	if ncurv mod ngpic ne 0 then begin
	    nplot = fix(ncurv/ngpic) + 1
	endif else begin
	    nplot = fix(ncurv/ngpic)
	endelse
    endelse

                ;************************************
                ;**** Create general graph titles****
                ;************************************

    title = strarr(4)
    title(0) = "UPSILON INTERPOLATION VS TEMPERATURE"  
    if ( strtrim(strcompress(utitle),2)  ne '' ) then begin
        title(0) = title(0) + ': ' + strupcase(strtrim(utitle,2))
    endif
    title(1) = 'ADAS    :' + header
    title(2) = 'FILE     :' + titlx
    title(3) = 'KEY     : (DASH LINE - INPUT DATA) (CROSSES - INTERPOLATION)'

    xtitle   = 'Electron temperature (K)'
    ytitle   = 'Upsilon'

;    title = "ION FRACTION VS. ELECTRON TEMPERATURE"
;    if ( strtrim(strcompress(utitle),2)  ne ' ' ) then begin
;        title = title + ': ' + strupcase(strtrim(utitle,2))
;    endif
;    small_check = GETENV('VERY_SMALL')
;    if small_check eq 'YES' then begin
;      title =  title + '!C!CADAS    : ' + strupcase(header)
;;      if strcompress(dyear, /remove_all) eq '' then defyear = 'NONE' else $
;      defyear = strcompress(dyear, /remove_all)
;      year = strcompress(year, /remove_all)
;      title =  title + '!C!CFILE     : ' + strcompress(dsfull) +             $
;      '   SPECIES: ' + strupcase(species) + '   YEAR: ' + year +             $
;      '   DEFAULT YEAR: '+ defyear
;      title =  title + '!C!CKEY     : (DASH LINE - PARTIAL)'
;    endif else begin
;      title =  title + '!C!CADAS    : ' + strupcase(header)
;      if strcompress(dyear, /remove_all) eq '' then defyear = 'NONE' else $
;      defyear = strcompress(dyear, /remove_all)
;      year = strcompress(year, /remove_all)
;      title =  title + '!CFILE     : ' + strcompress(dsfull) +               $
;      '   SPECIES: ' + strupcase(species) + '   YEAR: ' + year +             $
;      '   DEFAULT YEAR: '+ defyear
;      title =  title + '!CKEY     : (DASH LINE - PARTIAL)'
;    endelse

    rightstring = ''
    rightstring2 = ''
;    for i=0, (ncurv-1) do begin
;        if (i+1) lt 10 then rightstring = rightstring + ' '
;        rightstring = rightstring + strtrim(string(i+1),2) + '!C'
;        rightstring2 = rightstring2 + strupcase(poptit(i)) + '!C'
;  	if small_check eq 'YES' then begin
;	    rightstring = rightstring + '!C'
;	    rightstring2 = rightstring2 + '!C'
;	endif
;    endfor

                ;*************************************
                ;**** Create graph display widget ****
                ;*************************************

    graphid = widget_base(TITLE='ADAS215 GRAPHICAL OUTPUT', 		$
                          XOFFSET=1,YOFFSET=1)
    device, get_screen_size = scrsz
    xwidth=scrsz(0)*0.75
    yheight=scrsz(1)*0.75
    if nplot gt 1 then multiplot=1 else multiplot=0
    bitval = bitfile + '/menu.bmp'
    cwid = cw_adas_graph(graphid, print=hrdout, FONT=font,		$
                         xsize=xwidth, ysize=yheight, 			$
			 multiplot=multiplot, bitbutton=bitval)

                ;**** Realize the new widget ****

    widget_control, graphid, /realize

                ;**** Get the id of the graphics area ****

    widget_control, cwid, get_value=grval
    win = grval.win

                ;*******************************************
                ;**** Put the graphing data into common ****
                ;*******************************************

    data = {	STRG		:	strg,				$
		HEAD1		:	head1,				$
		HEAD2		:	head2,				$
		HEAD3		:	head3,				$
		IDXPLT		:	idxplt,				$
		TYPPLT		:	typplt,				$
		CPLT		:	cplt,				$
		TEIN		:	tein,				$
    		YINPLT		:	yinplt,				$
    		TEOUT		:	teout,				$
		YOUPLT		:	youplt,				$
		TITLE		:	title,				$
		XTITLE		:	xtitle,				$
		YTITLE		:	ytitle,				$
		XMIN		:	xmin,				$
		XMAX		:	xmax,				$
		YMIN		:	ymin,				$
		YMAX		:	ymax,				$
		LDEF		:	ldef				}

                ;**** Initialise to plot 0 ****
    iplot = 0
    wset, win

    plot215, ldef, top, bottom, 					$
	     idxplt, typplt, cplt,					$
	     tein, yinplt, teout, youplt,				$
    	     strg,head1,head2,head3, 					$
             title, xtitle, ytitle, xmin, xmax, ymin, ymax

                ;***************************
                ;**** make widget modal ****
                ;***************************

    xmanager, 'adas215_plot', graphid, /modal, /just_reg,		$
              event_handler='adas215_plot_ev'

    gomenu = gomenucom

END
