; Copyright (c) 1998, Strathclyde University.
; SCCS info: Module @(#)$Header: /home/adascvs/idl/adas2xx/adas215/adas215.pro,v 1.6 2009/09/29 22:46:54 mog Exp $ Date $Date: 2009/09/29 22:46:54 $
;+
; PROJECT:
;       ADAS IBM MVS to UNIX conversion
;
; NAME:
;	ADAS215
;
; PURPOSE:
;	The highest level routine for the ADAS 215 program.
;
; EXPLANATION:
;	This routine is called from the main adas system routine, adas.pro,
;	to start the ADAS 215 application.  Associated with adas215.pro
;	is a FORTRAN executable.  The IDL code provides the user
;	interface and output graphics whilst the FORTRAN code reads
;	in data files, performs numerical processing and creates the
;	output files.  The IDL code communicates with the FORTRAN
;	executable via a bi-directional UNIX pipe.  The unit number
;	used by the IDL for writing to and reading from this pipe is
;	allocated when the FORTRAN executable is 'spawned' (see code
;	below).  Pipe communications in the FORTRAN process are to
;	stdin and stdout, i.e streams 5 and 6.
;
;	The FORTRAN code is an independent process under the UNIX system.
;	The IDL process can only exert control over the FORTRAN in the
;	data which it communicates via the pipe.  The communications
;	between the IDL and FORTRAN must be exactly matched to avoid
;	input conversion errors.  The correct ammounts of data must be
;	passed so that neither process 'hangs' waiting for communications
;	which will never occur.
;
;	The FORTRAN code performs some error checking which is
;	independent of IDL.  In cases of error the FORTRAN may write
;	error messages.  To prevent these error messages from conflicting
;	with the pipe communications all FORTRAN errors are written to
;	output stream 0, which is stderr for UNIX.  These error messages
;	will appear in the window from which the ADAS session/IDL session
;	is being run.
;
;	In the case of severe errors the FORTRAN code may terminate
;	itself prematurely.  In order to detect this, and prevent the
;	IDL program from 'hanging' or crashing, the IDL checks to see
;	if the FORTRAN executable is still an active process before
;	each group of pipe communications.  The process identifier
;	for the FORTRAN process, PID, is recorded when the process is
;	first 'spawned'.  The system is then checked for the presence
;	of the FORTRAN PID.
;
; USE:
;	First the system settings must be established by calling
;	adas_sys_set.pro then adas215.pro is called to start the
;	ADAS215 application;
;
;	adas_sys_set, adasrel, fortdir, userroot, centroot, $
;		      devlist, devcode, font_large, font_small, edit_fonts
;	adas215,   adasrel, fortdir, userroot, centroot, devlist, $
;		   devcode, font_large, font_small, edit_fonts
;
; INPUTS:
;	ADASREL - A string indicating the ADAS system version, 
;		  e.g ' ADAS RELEASE: ADAS93 V1.1.0'.  The first
;		  character should be a space.
;
;	FORTDIR - A string holding the path to the directory where the
;		  FORTRAN executables are, e.g '/disk/adas/fortran/exec'
;
;	USERROOT - A string holding the path to the root directory of
;		   the user's adas data. e.g '/disk/bowen/adas'
;		   This root directory will be used by adas to construct
;		   other path names.  For example the users default data
;		   for adas205 should be in /disk/bowen/adas/adf04.  In
;		   particular the user's default interface settings will
;		   be stored in the directory USERROOT+'/defaults'.  An
;		   error will occur if the defaults directory does not
;		   exist.
;
;	CENTROOT - Like USERROOT, but this directory points to the
;		   central data area for the system.  User defaults are
;		   not stored on CENTROOT.
;
;	DEVLIST - A string array of hardcopy device names, used for
;		  graphical output. e.g ['Post-Script','HP-PCL','HP-GL']
;		  This array must mirror DEVCODE.  DEVCODE holds the
;		  actual device names used in a SET_PLOT statement.
;
;	DEVCODE - A string array of hardcopy device code names used in
;		  the SET_PLOT statement, e.g ['PS', 'PCL', 'HP']
;		  This array must mirror DEVLIST.
;
;	FONT_LARGE - The name of a larger font e.g 'courier_bold14'
;
;	FONT_SMALL - The name of a smaller font e.g 'courier_bold12'
;
;	EDIT_FONTS - A structure of two fonts used in the table editor
;		     adas_edtab.pro, {font_norm:'',font_input:''}.
;		     The two fonts are used to differentiate between
;		     editable and non-editable parts of the table. You
;		     may have to experiment to get two fonts which give
;		     the same default spacing in the edit widget.
;		     e.g {font_norm:'helvetica_bold14', $
;			  font_input:'helvetica_oblique14'}
;
; OPTIONAL INPUTS:
;	None
;
; OUTPUTS:
;	None - Note this routine should leave all inputs un-modified.
;
; OPTIONAL OUTPUTS:
;	None
;
; KEYWORD PARAMETERS:
;	None
;
; CALLS:
;	FIND_PROCESS	Checks to see if a given process is active.
;	BFSPF0		Pipe comms with FORTRAN BFSPF0 routine.
;	BFSETP		Pipe comms with FORTRAN BFSETP routine.
;	BFISPF		Pipe comms with FORTRAN BFISPF routine.
;	XXDATE		Get date and time from operating system.
;	BFSPF1		Pipe comms with FORTRAN BFSPF1 routine.
;	BFOUTG		Pipe comms with FORTRAN BFOUTG routine.
;
; SIDE EFFECTS:
;	This routine spawns a FORTRAN executable.  Note the pipe 
;	communications routines listed above.  In addition to these
;	pipe communications there is one explicit communication of the
;	date to the FORTRAN code, search for 'printf,pipe' to find it.
;
; CATEGORY:
;	Adas system.
;	
; WRITTEN:
;       H. P. Summers, University of Strathclyde, 6 May 1998
;
; MODIFIED:
;	1.1	Hugh Summers	
;		First release
;	1.2	Richard Martin
;		Increased version no. to 1.2
;	1.3	Richard Martin
;		Increased version no. to 1.3
;	1.4	Richard Martin
;		Increased version no. to 1.4
;	1.5	Richard Martin
;		Increased version no. to 1.5
;	1.6	Martin O'Mullane
;		  - Remove writing UID to fortran. xxname is used
;                   in fortran to determine the real name of the user.
;		  - Increased version no. to 1.6
;
; VERSION:
;	1.1	06-05-98
;	1.2	18-03-99
;	1.3	14-10-99
;	1.4	21-03-00
;	1.5	18-03-02
;	1.6	16-01-2007
;
;
;-----------------------------------------------------------------------------


PRO ADAS215,	adasrel, fortdir, userroot, centroot, $
		devlist, devcode, font_large, font_small, edit_fonts

		;************************
		;**** Initialisation ****
		;************************

  adasprog = ' PROGRAM: ADAS215 V1.6'
  lpend = 0
  gomenu = 0
  deffile = userroot+'/defaults/adas215_defaults.dat'
  device = ''
  bitfile = centroot + '/bitmaps'

		;******************************************
		;**** Search for user default settings ****
		;**** If not found create defaults     ****
		;******************************************

  files = findfile(deffile)
  if files(0) eq deffile then begin
    restore,deffile
    inval.centroot = centroot+'/adf04/'
    inval.userroot = userroot+'/adf04/'
    oldfile=inval.file
    newflag=0
    if (procval.new eq -1) then newflag=1

  end else begin

    inval = { $
		ROOTPATH:userroot+'/adf04/', $
		FILE:'', $
		CENTROOT:centroot+'/adf04/', $
		USERROOT:userroot+'/adf04/' }
    oldfile=''
    newflag=1
    
    procval = {NEW:-1}
    tcxname = userroot + '/pass/adas215_adf04.pass'
   	outval =  {	GRPOUT		:	0, 			$
			GTIT1		:	'', 			$
			GRPSCAL		:	0,			$
			XMIN		:	'',  			$
			XMAX		:	'', 			$
			YMIN		:	'',  			$
			YMAX		:	'',			$
			HRDOUT		:	0,			$
			HARDNAME	:	'', 			$
			GRPDEF		:	'',			$
			GRPFMESS	:	'', 			$
			GRPSEL		:	-1,			$
			GRPRMESS	:	'', 			$
			DEVSEL		:	-1, 			$
			GRSELMESS	:	'', 			$
			TEXOUT		:	0, 			$
			TEXAPP		:	-1, 			$
			TEXREP		:	0, 			$
			TEXDSN		:	'', 			$
			TEXDEF		:	'paper.txt',		$
			TEXMES		:	'', 			$
			TCXOUT		:	0, 			$
			TCXAPP		:	-1, 			$
			TCXREP		:	0, 			$
			TCXDSN		:	'', 			$
			TCXDEF		:	tcxname,		$
			TCXMES		:	'' 			}

  end

 		;**** Always set text output to not append since Fortran
		;**** won't have opened it

    outval.texapp = -1

		;****************************
		;**** Start fortran code ****
		;****************************

  spawn, fortdir+'/adas215.out',unit=pipe,/noshell,PID=pid

		;************************************************
		;**** Get date and write to fortran via pipe ****
		;************************************************

  date = xxdate()
  printf,pipe,date(0)


LABEL100:
		;**** Check FORTRAN still running ****
  
  if find_process(pid) eq 0 then goto, LABELEND

		;************************************************
		;**** Communicate with bfspf0 in fortran and ****
		;**** invoke user interface widget for       ****
		;**** Data file selection                    ****
		;************************************************

  bfspf0, pipe, inval, dsnpas, rep, FONT=font_large
  if ( inval.file eq oldfile AND procval.new eq 0) then newflag=0 else newflag=1

		;**** If cancel selected then end program ****

  if rep eq 'YES' then goto, LABELEND

		;**** Check FORTRAN still running ****

  if find_process(pid) eq 0 then goto, LABELEND

		;*************************************
		;**** Read pipe comms from bfsetp ****
		;*************************************

  bfsetp, pipe, titled, sz, sz0, sionpt, dsninc, ndtem, tea


LABEL200:
		;**** Check FORTRAN still running ****

  if find_process(pid) eq 0 then goto, LABELEND

		;************************************************
		;**** Communicate with bfispf in fortran and ****
		;**** invoke user interface widget for       ****
		;**** Processing options                     ****
		;************************************************
  bfispf, pipe, lpend, procval, 			$
          titled, sz, sz0, sionpt, dsnpas, dsninc, newflag,	$
          bitfile, gomenu,			$
	  FONT_LARGE=font_large, FONT_SMALL=font_small, $
	  EDIT_FONTS=edit_fonts

                ;**** If menu button clicked, tell FORTRAN to stop ****

  if gomenu eq 1 then begin
      printf, pipe, 1
      if (newflag eq 1) then procval.new=-1
      goto, LABELEND
  endif else begin
      printf, pipe, 0
  endelse

		;**** If cancel selected then goto 100 ****
  if lpend eq 1 then begin
  	if (newflag eq 1) then procval.new=-1
  	goto, LABEL100
  endif

		;**** Fortran processing now in progress. Get ****
		;**** date and time for this run.	      ****
  date = xxdate()
		;**** Create header for output. ****
  header = adasrel+adasprog+' DATE: '+date(0)+' TIME: '+date(1)

		;**** Check FORTRAN still running ****

  if find_process(pid) eq 0 then goto, LABELEND

LABEL300:
		;**** Check FORTRAN still running ****
  if find_process(pid) eq 0 then goto, LABELEND

		;************************************************
		;**** Communicate with bfspf1 in fortran and ****
		;**** invoke user interface widget for       ****
		;**** Output options                         ****
		;************************************************

  bfspf1, pipe, lpend, outval, dsninc, header, bitfile, gomenu, 	$
	  pid , DEVLIST=devlist, FONT=font_small

                ;**** Extra check to see whether FORTRAN is still there ****

    if find_process(pid) eq 0 then goto, LABELEND

                ;**** If menu button clicked, tell FORTRAN to stop ****

  if gomenu eq 1 then begin
      printf, pipe, 1
      outval.texmes = ''
      outval.tcxmes = ''
      goto, LABELEND
  endif else begin
      printf, pipe, 0
  endelse

		;**** If cancel selected then erase output ****
		;**** messages and goto 200.		   ****
  if lpend eq 1 then begin
    outval.texmes = ''
    goto, LABEL200
  end

		;**** If graphical output requested ****

		;************************************************
		;**** Communicate with bfoutg in fortran and ****
		;**** invoke widget Graphical output         ****
		;************************************************
  if outval.grpout eq 1 then begin

		;**** User explicit scaling ****

    grpscal = outval.grpscal
    xmin = outval.xmin
    xmax = outval.xmax
    ymin = outval.ymin
    ymax = outval.ymax

		;**** Hardcopy output ****
    hrdout = outval.hrdout
    hardname = outval.hardname
    if outval.devsel ge 0 then device = devcode(outval.devsel)

		 ;*** user title for graph ***

    utitle = outval.gtit1
 
    bfoutg, dsninc, pipe, utitle, grpscal, xmin, xmax, ymin, ymax, $
		  hrdout, hardname, device, header, bitfile, gomenu, $
		 FONT=font_large

                ;**** If menu button clicked, tell FORTRAN to stop ****

    if gomenu eq 1 then begin
        printf, pipe, 1
        outval.texmes = ''
	outval.tcxmes = ''
        goto, LABELEND
    endif else begin
        printf, pipe, 0
    endelse

  end

		;**** Back for more output options ****
GOTO, LABEL300

LABELEND:

		;**** Ensure appending is not enabled for ****
		;**** text output at start of next run.   ****
outval.texapp = -1

		;**** Save user defaults ****
save, inval, procval, outval, filename=deffile


END
