; Copyright (c) 1995, Strathclyde University.
; SCCS info: Module @(#)$Header: /home/adascvs/idl/adas2xx/adas215/bfsetp.pro,v 1.1 2004/07/06 11:39:22 whitefor Exp $ Date $Date: 2004/07/06 11:39:22 $
;+
; PROJECT:
;       ADAS IBM MVS to UNIX conversion
;
; NAME:
;	BFSETP
;
; PURPOSE:
;	IDL communications with ADAS215 FORTRAN process via pipe.
;
; EXPLANATION:
;	This IDL routine reads data from the ADAS215 FORTRAN subroutine
;	BFSETP via a UNIX pipe.  In the original IBM version the FORTRAN
;	subroutine BFSETP put information needed by the ISPF interface
;	into the ISPF pool area for later use.  Now the same information
;	is written to IDL for use in its user unterface.
;
; USE:
;	The use of this routine is specific to ADAS215.  See
;	adas215.pro for an example.
;
; INPUTS:
;	PIPE	- The IDL unit number of the bi-directional pipe to the
;		  ADAS215 FORTRAN process.
;
; OPTIONAL INPUTS:
;	None
;
; OUTPUTS:
;	TITLED	- ELEMENT SYMBOL.
;
;	SZ	- NUCLEAR CHARGE READ
;
;	SZ0	- RECOMBINED ION CHARGE READ
;
;	SIONPT	- IONISATION POTENTIAL (UNITS: WAVE NUMBERS)
;
; 	SCNTE	- NUMBER OF ELECTRON IMPACT TRANSITIONS
; 
; 	SIL	- NUMBER OF ENERGY LEVELS
;
;	DSNINC	- DATA SET NAME OF ASSOCIATED COPASE FILE
;
;	NDMET	- MAXIMUM NUMBER OF METASTABLES ALLOWED
;
;	ILEV	- ARRAY COUNTER FOR LEVEL INDEX
;
;	STRGA	- LEVEL DESIGNATIONS
;                 DIMENSION: LEVEL INDEX
;
;	NDTEM	- MAXIUMUM NUMBER OF TEMPERATURES ALLOWED
;
;	TEA	- ELECTRON TEMPERATURES  (UNITS: KELVIN)
;
;	NDDEN	- MAXIUMUM NUMBER OF DENSITIES ALLOWED
;
;	DENSA	- ELECTRON DENSITIES  (UNITS: CM-3)
;
;	CRATHA	- PARAMETER = DEFAULT VALUE FOR (NEUTRAL H
;                             DENSITY/ELECTRON DENSITY) RATIO
;
;	CRATIA	- PARAMETER = DEFAULT VALUE FOR (STAGE ABUND-
;                             ANCIES) RATIO.
;
;
; OPTIONAL OUTPUTS:
;	None
;
; KEYWORD PARAMETERS:
;	None
;
; CALLS:
;	None
;
; SIDE EFFECTS:
;	Reads data from a pipe.
;
; CATEGORY:
;	Adas system.
;
; WRITTEN:
;       Hugh Summers, University of Strathclyde, 1998
;
; MODIFIED: 
;	1.1	Hugh Summers
;		First version.
;
; VERSION:
;	1.1	09-06-98
;
;-----------------------------------------------------------------------------



PRO bfsetp, pipe, titled, sz, sz0, sionpt, dsninc, ndtem, tea

		;**********************************
		;**** Initialise new variables ****
		;**********************************
  titled = ''
  sz = ''
  sz0= ''
  sionpt = ''
  scnte = ''
  sil = ''
  dsninc = ''
  ndtem = 0

  input = ''

		;********************************
		;**** Read data from fortran ****
		;********************************
  readf,pipe,titled
  readf,pipe,sz
  readf,pipe,sz0
  readf,pipe,sionpt
  readf,pipe,scnte
  readf,pipe,sil
  readf,pipe,input
  dsninc = strtrim( input )

  readf,pipe,ndtem
  tea = strarr( ndtem )

  for i = 0, ndtem-1 do begin
    readf,pipe,input
    tea(i) = input
  end

END
