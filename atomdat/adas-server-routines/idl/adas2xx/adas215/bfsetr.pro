; Copyright (c) 1995, Strathclyde University.
; SCCS info: Module @(#)$Header: /home/adascvs/idl/adas2xx/adas215/bfsetr.pro,v 1.1 2004/07/06 11:39:25 whitefor Exp $ Date $Date: 2004/07/06 11:39:25 $
;+
; PROJECT:
;       ADAS IBM MVS to UNIX conversion
;
; NAME:
;	BFSETP
;
; PURPOSE:
;	IDL communications with ADAS215 FORTRAN process via pipe.
;
; EXPLANATION:
;	This IDL routine reads data from the ADAS215 FORTRAN subroutine
;	BFSETR via a UNIX pipe.  In the original IBM version the FORTRAN
;	subroutine BFSETR put information needed by the ISPF interface
;	into the ISPF pool area for later use.  Now the same information
;	is written to IDL for use in its user unterface.
;
; USE:
;	The use of this routine is specific to ADAS215.  See
;	adas215.pro for an example.
;
; INPUTS:
;	PIPE	- The IDL unit number of the bi-directional pipe to the
;		  ADAS215 FORTRAN process.
;
; OPTIONAL INPUTS:
;	None
;
; OUTPUTS:
;	SRMIN	- Minimum spectrum line intensity ratio.
;
;	SRMAX	- Maximum spectrum line intensity ratio
;
;
; OPTIONAL OUTPUTS:
;	None
;
; KEYWORD PARAMETES:
;	None
;
; CALLS:
;	None
;
; SIDE EFFECTS:
;	Reads data from a pipe.
;
; CATEGORY:
;	Adas system.
;
; WRITTEN:
;       Hugh Summers, University of strathclyde, 1998.
;
; MODIFIED:
;	  1.1 Hugh Summers
;		First version.
;
; VERSION:
;	  1.1 09-06-98
;
;-----------------------------------------------------------------------------



PRO bfsetr, pipe, srmin, srmax

		;**********************************
		;**** Initialise new variables ****
		;**********************************
  srmin = ''
  srmax = ''

		;********************************
		;**** Read data from fortran ****
		;********************************
  readf,pipe,srmin
  readf,pipe,srmax

END
