; Copyright (c) 1998 Strathclyde University .
; SCCS info: Module @(#)$Header: /home/adascvs/idl/adas2xx/adas215/plot215.pro,v 1.1 2004/07/06 14:30:48 whitefor Exp $ Date $Date: 2004/07/06 14:30:48 $
;+
; PROJECT:
;       ADAS IBM MVS to DEC UNIX conversion
;
; NAME:
;       PLOT215
;
; PURPOSE:
;       Plot upsilon graphs for ADAS215.
;
; EXPLANATION:
;       This routine plots ADAS215 output for one or more plots (each
;       can contain a maximum of seven lines).
;
; USE:
;       Use is specific to ADAS215.  See adas215_plot.pro for
;       example.
;
; INPUTS:
;
;	LDEF	-	Integer; 1 if user specified axis limits to 	
;			be used, 0 if default scaling to be used.
;
;	TOP	-	Integer; last line to plot
;
;	BOTTOM	-	Integer;first line to plot
;
;	X	-	Fltarr; x-values to plot
;
;	Y	-	2d Fltarr; y-values to plot (2nd dimension between
;			BOTTOM and TOP)
;
;	RIGHTSTRING  -  String; left hand column of title to side of graph
;			('INDEX')
;
;	RIGHTSTRING2 -  String; right hand column of title to side of graph
;			('DESIGNATION')
;
;	TITLE	-	String; heading to go above graph
;
;	xmin	-	Float; user-defined x-axis minimum
;
;	xmax	-	Float; user-defined x-axis maximum
;
;	ymin	-	Float; user-defined y-axis minimum
;
;       ymax   -       Float; user-defined y-axis maximum
;
; OPTIONAL INPUTS:
;       None.
;
; OUTPUTS:
;       None.
;
; OPTIONAL OUTPUTS:
;       None.
;
; KEYWORD PARAMETERS:
;       FONT    - String; Name of font passed to graphical output
;                 widget.
;
; CALLS:
;       None.
;
; SIDE EFFECTS:
;       None.
;
; CATEGORY:
;       Adas system.
;
; WRITTEN:
;       H. P. Summers, University of Strathclyde, 01/06/1998
;
; MODIFIED:
;       1.1     Hugh Summers      
;		First version
;
; VERSION:
;	1.1	01-06-98
;
;
;----------------------------------------------------------------------------

PRO plot215, ldef, top, bottom, 				$
	     idxplt, typplt, cplt,				$
	     tein, yinplt, teout, youplt, 			$
	     strg, head1, head2, head3, 				$
             title, xtitle, ytitle, xmin, xmax, ymin, ymax

    COMMON Global_lw_data, left, right, tp, bot, grtop, grright

                ;****************************************************
                ;**** Suitable character size for current device ****
                ;**** Aim for 60 characters in y direction.      ****
                ;****************************************************

    charsize = (!d.y_vsize/!d.y_ch_size)/60.0
    small_check = GETENV('VERY_SMALL')
    if small_check eq 'YES' then charsize=charsize*0.8

		;***************************************
		;**** Construct graph title         ****
		;**** "!C" is the new line control. ****
		;***************************************

    !p.font=-1
    if small_check eq 'YES' then begin
        gtitle =  title(0) + "!C!C" + title(1) + "!C!C" + title(2)+"!C!C" + $
          title(3)
    endif else begin
        gtitle =  title(0) + "!C!C" + title(1) + "!C" + title(2)+"!C" + $
          title(3) 
    endelse

                ;**** erase screen ****

    erase

                ;**** How many points to plot ****

    npts = size(tein)
    npts = npts(1)

		;**** Find x and y ranges for auto scaling,        ****
                ;**** check x and y in range for explicit scaling. ****

    makeplot = 1
    style = 0
    ystyle = 0
    yplot = yinplt(*, bottom:top)
    yploto = youplt(*, bottom:top)

    if ldef eq 0 then begin

                ;**** identify values in the valid range ****
                ;**** plot routines only work within ****
                ;**** single precision limits.       ****

	xvals = where (tein gt 1.0e-37 and tein lt 1.0e37)
	xvalo = where (teout gt 1.0e-37 and teout lt 1.0e37)
	yvals = where (yplot gt 1.0e-37 and yplot lt 1.0e37)
	yvalo = where (yploto gt 1.0e-37 and yploto lt 1.0e37)
	if xvals(0) gt -1 then begin
	    maxx = max(tein(xvals))
	    maxxo = max(teout(xvalo))
	    minx = min(tein(xvals))
	    minxo = min(teout(xvalo))
	    maxx = max([maxx,maxxo])
	    minx = min([minx,minxo])
	endif else begin
	    makeplot = 0
	endelse
	if yvals(0) gt -1 then begin
	    maxy = max(yplot(yvals))
	    maxyo = max(yploto(yvalo))
	    miny = min(yplot(yvals))
	    minyo = min(yploto(yvalo))
	    maxy = max([maxy,maxyo])
	    miny = min([miny,minyo])
	endif else begin
	    makeplot = 0
	endelse
	if makeplot eq 1 then begin
	    if miny le 1.0e-36 then begin
		ystyle = 1
		miny = 1.0e-36
	    endif else begin
		ystyle = 0
	    endelse
	endif
	style = 0
    endif else begin
        minx = xmin
	maxx = xmax
	miny = ymin
	maxy = ymax
	xvals = where(tein gt minx and tein lt maxx)
	yvals = where(yplot gt miny and yplot lt maxy)
	if xvals(0) eq -1 or yvals(0) eq -1 then begin
	    makeplot = 0
	endif else begin
	    makeplot = 1
	endelse
	style = 1
        ystyle = 1
    endelse

    if makeplot eq 1 then begin

                ;**** Set up log-log plotting axes ****


	plot_oo, [minx, maxx], [miny, maxy], /nodata, ticklen=1.0,	$
                 position=[left, bot, grright, grtop],			$
                 xtitle=xtitle, ytitle=ytitle, xstyle=style, 		$
		 ystyle=ystyle, charsize=charsize

                ;*********************************
                ;**** Make and annotate plots ****
                ;*********************************

 	for i=bottom,top do begin

	    ymaxchk = max(yinplt(*,i))		;check this line is okay to 
						;draw
	    if ymaxchk le 1.0e-36 then begin
		print, '******************************* BFOUTG MESSAGE '+$
                       '*******************************'
		print, 'NO GRAPH WILL BE OUTPUT ' +$
		       'BECAUSE:'
		print, 'ALL VALUES ARE BELOW THE CUTOFF OF 1.000E-36'
		print, '******************************* END OF MESSAGE '+$
                       '*******************************'
	    endif else begin
                oplot, tein, yinplt(*,i), linestyle=2
                oplot, teout, youplt(*,i), psym=1

                ;**** Find suitable point for annotation ****

                if (miny eq 10^(!y.crange(0))) then begin
	            if (tein(npts-1) ge minx) and (tein(npts-1) le maxx) and 	$
                    (yinplt(npts-1,i) gt miny) and 				$
                    (yinplt(npts-1,i) lt (10^(!y.crange(1)))) then begin
		        iplot = npts - 1
	            endif else begin
		        iplot = -1
		        for id = 0, npts-2 do begin
		            if (tein(id) ge minx) and (tein(id) le maxx) and	$
		            (yinplt(id,i) gt miny) and (yinplt(id,i) le maxy) then begin
			        iplot = id
		            endif
	                endfor
	            endelse
                endif else begin
                    if (tein(npts-1) ge minx) and (tein(npts-1) le maxx) and  $
                    (yinplt(npts-1,i) ge miny) and                           $
                    (yinplt(npts-1,i) lt (10^(!y.crange(1)))) then begin
                       iplot = npts - 1
                    endif else begin
                        iplot = -1
                        for id = 0, npts-2 do begin
                            if (tein(id) ge minx) and (tein(id) le maxx) and  $
                            (yinplt(id,i) ge miny) and                       $
                            (yinplt(id,i) le maxy) then begin
                               iplot = id
                            endif
                        endfor
                    endelse
                endelse
    
                ;**** Annotate plots with level numbers ****

 	        if iplot ne -1 then begin
		    xyouts, tein(iplot), yinplt(iplot,i), 		$
                    string(idxplt(i), format='(i2)'), alignment=0.0, 	$
		    charsize=charsize*0.9
	        endif
	    endelse
	endfor
    endif else begin			;no plot possible
        xyouts, 0.2, 0.5, /normal, charsize=charsize*1.5,               $
                '---- No data lies within range ----'
    endelse

		;********************
		;**** plot title ****
		;********************

    if small_check eq 'YES' then begin
	charsize = charsize * 0.9
    endif

    xyouts, (left-0.05), (tp+0.05), gtitle, /normal, alignment=0.0, 	$
	    charsize = charsize

		;********************************
		;**** right hand side labels ****
		;********************************

    rhs = grright + 0.02
    cpp = grtop - 0.02

    xyouts, rhs,  cpp, head1, /normal, alignment=0.0,charsize=charsize*0.8

    cpp = cpp -0.03
    xyouts, rhs,  cpp, head2, /normal, alignment=0.0,charsize=charsize*0.8
    cpp = cpp - 0.015
    xyouts, rhs,  cpp, head3, /normal, alignment=0.0,charsize=charsize*0.8

    for i = bottom,top  do begin
    	cpp = cpp - 0.02
  	xyouts, rhs, cpp, string(idxplt(i),strtrim(typplt(i),2),	$
	cplt(i),FORMAT='(1i5,11x,1a1,5x,1f5.2)'), 			$
	/normal, alignment= 0.0, charsize=charsize*0.8
    endfor

END
