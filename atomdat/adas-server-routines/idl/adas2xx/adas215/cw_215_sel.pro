; Copyright (c) 1998 Strathclyde University .
; SCCS info: Module @(#)$Header: /home/adascvs/idl/adas2xx/adas215/cw_215_sel.pro,v 1.4 2012/09/08 07:42:21 mog Exp $ Date $Date: 2012/09/08 07:42:21 $
;+
; PROJECT:
;       ADAS IBM MVS to DEC UNIX conversion
;
; NAME:
;	CW_215_SEL()
;
; PURPOSE:
;	Widget for viewing current selections and invoking a selection
;	list window.
;       This particular version of the widget has been altered specifically
;       for use with the adas215 processing options screen.
;
; EXPLANATION:
;	This compound widget provides a single entry point to the ADAS
;	multiple selection list features.  The widget consists of a
;	title, a text widget showing the items currently selected and
;	a button which can be used to invoke the multiple selection list
;	in order to modify the selections.
;
;	This widget generates an event whenever the 'Selections' button
;	has been pressed and a new set of selections completed.  The
;	event structure is;
;
;	{ID:0L, TOP:0L, HANDLER:0L, ACTION:''}
;
;	Where ACTION will have the value 'multisel'.
;
; USE:
;	The following code adds a selection list widget to a base.  The
;	list of selected items includes 'Red' and 'Blue' at realization.
;	The maximum number of selections allowed is set at 3 since that
;	is the length of the 'selected' array.  Press the 'Selections'
;	button to modify the selections.
;
;	    list = ['Red','Green','Blue','Black','Brown','Grey']
;	    selected = [0,2,-1]
;	    title = 'Colour Selections'
;	    base = widget_base(/column)
;	    selid = cw_215_sel(base,list,selected=selected,title=title)
;	    widget_control,base,/realize
;	    rc=widget_event()
;	    widget_control,selid,get_value=selected
;	    print,'Colours Selected are',list(selected(where(selected ge 0)))
;
; INPUTS:
;       PARENT   - The ID of the parent widget.
;
;	LIST	 - List of items for the selection list.  A string array
;		   with one element per list item.
;
; OPTIONAL INPUTS:
;	None.
;
; OUTPUTS:
;       The return value of this function is the ID of the compound
;       widget which is of type LONG integer.
;
; OPTIONAL OUTPUTS:
;       None
;
; KEYWORD PARAMETERS:
;       SELECTED - One dimensional integer array of the indices of the
;                  items in LIST which are to be set as selected when
;                  first displayed.  The length of SELECTED is the
;                  maximum number of selections allowed.  Extra spaces
;                  can be reserved in SELECTED with values of -1.  All
;                  -1 values should be at the end of the array.
;                  For example SELECTED=[0,5,-1,-1] will give a maximum
;                  of 4 selections and items 0 and 5 from list are
;                  selected on start up.  The default is no selections
;                  set on startup.
;
;       MAXSEL   - Maximum number of selections allowed.  The default is
;                  one unless the SELECTED keyword is specified in which
;                  case the value of MAXSEL is ignored.
;
;	ORDERED  - When the ordered keyword is non-zero the list of 
;		   selections in the SELECTED array is arranged in order
;		   of ascending value regardless of the order in which 
;		   the list items were chosen.  The -1 values used to
;		   pad the selected array still remain at the end.
;		   For example if the ordered keyword is not set the
;		   values in selected will match the order in which the
;		   items were selected from LIST e.g [3,1,5,-1,-1].
;		   With the ordered keyword non-zero the SELECTED array
;		   would be [1,3,5,-1,-1].
;
;       TITLE    - Title to appear on the selection list window banner.
;                  The default is 'Make your Selections'.
;
;       FONT     - The text font to be used for the window.  The default
;                  is the current X default.
;
;	UVALUE	 - User value for the compound widget.
;
; CALLS:
;       CW_LOADSTATE    Recover compound widget state.
;       CW_SAVESTATE    Save compound widget state.
;	MULTI_215	Pop-up multiple selection list window.
;       See side effects for other related widget management routines.
;
; SIDE EFFECTS:
;	This widget uses a COMMON BLOCK: CW_ADASSEL215_BLK to hold the
;       widget state.
;
;       Three other routines are included which are used to manage the
;       widget;
;
;	ADASSEL215_SET_VAL
;	ADASSEL215_GET_VAL()
;	ADASSEL215_EVENT()
;
; CATEGORY:
;	Compound Widget?
;
; WRITTEN:
;       Hugh Summers, University of Strathclyde, 1998
;	  Based on cw_207_sel.pro
;
; MODIFIED:
;       1.1	Hugh Summers
;		Firts version.
;	1.2	Richard Martin
;		Removed obsolete cw_loadstate/savestate statements.
;       1.3     Martin O'Mullane
;                - Change variable 'list' to 'thelist' to avoid conflict
;                  with the reserved function LIST introduced in IDL v8.0.
;
; VERSION:
;       1.1	09-06-98
;	1.2	30-01-02
;	1.3	08-09-2012
;-
;-----------------------------------------------------------------------------

PRO adassel215_set_val, id, selected

		;**** Return to caller on error ****
  ON_ERROR, 2

		;**** Retrieve the state ****

  first_child = widget_info(id,/child)
  widget_control, first_child, get_uvalue=state, /no_copy
  
		;**** The current selections ****
  selsind = where(selected gt -1)
  if selsind(0) ge 0 then currsels = state.list(3,selected(selsind)) $
							else currsels = ''
  widget_control,state.curselid,set_value=currsels

		;**** Copy the new value to state structure ****
  state.selected = selected

  widget_control, first_child, set_uvalue=state, /no_copy

END

;-----------------------------------------------------------------------------


FUNCTION adassel215_get_val, id


		;**** Return to caller on error ****
  ON_ERROR, 2

		;**** Retrieve the state ****
		
  first_child = widget_info(id,/child)
  widget_control, first_child, get_uvalue=state, /no_copy
  
  s_state = { selected: state.selected, list:  state.list}

  widget_control, first_child, set_uvalue=state, /no_copy 
  
  RETURN, s_state

END

;-----------------------------------------------------------------------------

FUNCTION adassel215_event, event


		;**** Base ID of compound widget ****
  base=event.handler

		;**** Retrieve the state ****

  first_child = widget_info(base,/child)
  widget_control, first_child, get_uvalue=state, /no_copy
  
		;************************
		;**** Process Events ****
		;************************
  CASE event.id OF

    state.selectid: begin

      action = 'multisel'
      msel = multi_215(state.list, SELECTED=state.selected, $
				MAXSEL=state.maxsel, ORDERED=state.ordered, $
				TITLE=state.title, FONT=state.font)

	selected=msel.selected
	thelist=msel.list
	
		;**** The current selections ****
      selsind = where(selected gt -1)
      if selsind(0) ge 0 then currsels = state.list(3,selected(selsind)) $
							 else currsels = ''
      widget_control,state.curselid,set_value=currsels

		;**** Copy the new value to state structure ****
      state.selected = selected
      state.list = thelist

    end

  ENDCASE

  widget_control, first_child, set_uvalue=state, /no_copy

  RETURN, {ID:base, TOP:event.top, HANDLER:0L, ACTION:action}
END

;-----------------------------------------------------------------------------

FUNCTION cw_215_sel, parent, thelist, SELECTED=selected, $
         MAXSEL=maxsel, ORDERED=ordered, TITLE=title, FONT=font, UVALUE=uvalue



  IF (N_PARAMS() LT 2) THEN MESSAGE, 'Must specify PARENT and LIST'+ $
						' for cw_215_sel'

                ;**** Set defaults for keywords ****
  IF NOT (KEYWORD_SET(maxsel)) THEN maxsel = 1
  IF NOT (KEYWORD_SET(selected)) THEN $
                        selected = make_array(maxsel,/int,value=-1)
  IF NOT (KEYWORD_SET(ordered)) THEN ordered = 0
  IF NOT (KEYWORD_SET(title)) THEN title = ' '
  IF NOT (KEYWORD_SET(font)) THEN font = ''
  IF NOT (KEYWORD_SET(uvalue))  THEN uvalue = 0

                ;**** Over-ride MAXSEL with the length of SELECTED ****
                ;**** this is in case they are different.          ****
  IF (KEYWORD_SET(selected)) THEN begin
    seldim = size(selected)
    maxsel = seldim(1)
  end

		;**** Create the main base for the widget ****
  cwid = WIDGET_BASE(parent, UVALUE = uvalue, $
		EVENT_FUNC = "adassel215_event", $
		FUNC_GET_VALUE = "adassel215_get_val", $
		PRO_SET_VALUE = "adassel215_set_val", $
		/COLUMN,/FRAME)

		;**** Create base to hold the value of state ****

  first_child = widget_base(cwid)

		;**** Title ****
  rc = widget_label(first_child,value=title,font=font)

		;**** The current selections ****
  selsind = where(selected gt -1)
  if selsind(0) ge 0 then currsels = thelist(3,selected(selsind)) $
							else currsels = ''
  maxlen = max(strlen(thelist(3,*)))
  curselid = widget_text(cwid,value=currsels,XSIZE=maxlen,YSIZE=maxsel, $
								font=font)
 
		;**** The edit button ****
  selectid = widget_button(cwid,value='Selections',font=font)

		;**** Create state structure ****
  new_state =  {CURSELID:curselid, SELECTID:selectid, LIST:thelist, $
		SELECTED:selected, MAXSEL:maxsel, ORDERED:ordered, $
		TITLE:title, FONT:font}

		;**** Save initial state structure ****

  widget_control, first_child, set_uvalue=new_state, /no_copy

  RETURN, cwid

END
