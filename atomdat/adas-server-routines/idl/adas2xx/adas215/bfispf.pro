; Copyright (c) 1998 Strathclyde University .
; SCCS info: Module @(#)$Header: /home/adascvs/idl/adas2xx/adas215/bfispf.pro,v 1.2 2004/07/06 11:38:56 whitefor Exp $ Date $Date: 2004/07/06 11:38:56 $
;+
; PROJECT:
;       ADAS IBM MVS to UNIX conversion
;
; NAME:
;	BFISPF
;
; PURPOSE:
;	IDL user interface and communications with ADAS215 FORTRAN
;	process via pipe.
;
; EXPLANATION:
;	The routine begins by reading information from the ADAS215
;	FORTRAN process via a UNIX pipe.  Then part of the ADAS215
;	IDL user interface is invoked to determine how the user
;	wishes to process the input dataset.  When the user's
;	interactions are complete the information gathered with
;	thbfispf.proe user interface is written back to the FORTRAN process
;	via the pipe.  Communications are to the FORTRAN subroutine
;	B5ISPF.
;
; USE:
;	The use of this routine is specific to ADAS215, see adas215.pro.
;
; INPUTS:
;	PIPE	- The IDL unit number of the bi-directional pipe to the
;		  ADAS215 FORTRAN process.
;
;	LPEND	- Integer, 0 if the user pressed the 'Done' button
;		  or 1 if the user pressed 'Cancel'.  Maps directly onto
;		  the logical variable LPEND in ADAS215 FORTRAN.
;
;	VALUE	- A structure which determines the initial settings of
;		  the processing options widget.  The initial value is
;		  set in adas215.pro.  If adas215.pro passes a blank 
;		  dummy structure of the form {NEW:-1} into VALUE then
;		  VALUE is reset to a default structure.
;
;		  The VALUE structure is;
;		     {  NSTRN1:1,					$
;			ISTRN1:lonarr(), 				$
;			IFOUT:1, 					$
;			NVMAX:0, 					$
;			NVN:0,						$
; 			TOUT:[0.0]					}
;
;		  See cw_adas215_proc.pro for a full description of this
;		  structure and related variables.
;
;	TITLED	- String, Ion name.
;
;	SZ	- String, Recombined ion charge.
;
;	SZ0	- String, Nuclear charge.
;
;	SIONPT	- String, Ionisation potential.
;
;	DSNPAS	- String, The full system file name of the input
;			Contour Passing file.
;	DSNINC	- String, The full system file name of the input COPASE
;			dataset selected by the user for processing.
;
;       BITFILE - String; the path to the dirctory containing bitmaps
;                 for the 'escape to series menu' button.
;
; OPTIONAL INPUTS:
;	None
;
; OUTPUTS:
;	VALUE	- On output the structure records the final settings of
;		  the processing selections widget if the user pressed the
;		  'Done' button, otherwise it is not changed from input.
;
;       GOMENU  - Int; flag - set to 1 if user has selected 'escape direct
;                 to series menu , otherwise 0
;
; OPTIONAL OUTPUTS:
;	None
;
; KEYWORD PARAMETERS:
;	FONT_LARGE - The name of a larger font.
;
;	FONT_SMALL - The name of a smaller font.
;
;	EDIT_FONTS - A structure of two fonts used in the table editor
;		     adas_edtab.pro, {font_norm:'',font_input:''}.
;
; CALLS:
;	ADAS215_PROC	Invoke the IDL interface for ADAS215 data
;		 	processing options/input.
;
; SIDE EFFECTS:
;	Two way communications with ADAS215 FORTRAN via the
;	bi-directional UNIX pipe.
;
; CATEGORY:
;	Adas system.
;
; WRITTEN:
;       H. P. Summers, University of Strathclyde, 7/5/98
;
; MODIFIED:
;	1.1	Hugh Summers
;		First edition
;	1.2	Martin OMullane
;		Corrected to accomodate modified level selection wiget.
;
; VERSION:
;	1.1	07-05-98
;	1.2	16-03-99
;
;
;-----------------------------------------------------------------------------

PRO bfispf, pipe, lpend, value, titled, sz, sz0, sionpt, dsnpas, dsninc,$
	    newflag, bitfile, gomenu, FONT_LARGE=font_large, $
            FONT_SMALL=font_small, EDIT_FONTS=edit_fonts

                ;**** Set defaults for keywords ****

    IF NOT (KEYWORD_SET(font_large)) THEN font_large = ''
    IF NOT (KEYWORD_SET(font_small)) THEN font_small = ''
    IF NOT (KEYWORD_SET(edit_fonts)) THEN 				$
    edit_fonts = {font_norm:'',font_input:''}

		;**** Declare variables for input ****

    lpend = 0
    ndlne = 0
    nvmax = 14
    il = 0
    nv = 0
    icnte = 0
    icnth = 0
    icntr = 0
    idum = 0
    ldum = 0L
    sdum = ' '
    fdum = 0.0
  
		;********************************
		;**** Read data from fortran ****
		;********************************

    readf, pipe, idum
    lpend = idum
    readf, pipe, idum
    ndlne = idum
    readf, pipe, idum
    nvmax = idum
    readf, pipe, idum
    il = idum
    readf, pipe, idum
    nv = idum
    tscef = dblarr(nvmax,3)
    for i=0, (nvmax-1) do begin
        for j=0, 2 do begin
	    readf, pipe, fdum
	    tscef(i,j) = fdum
        endfor
    endfor
    readf, pipe, idum
    icnte = idum
    readf, pipe, idum
    icnth = idum
    readf, pipe, idum
    icntr = idum

		;*************************************
		;**** Set up and read into arrays ****
		;*************************************

    ie1a   = lonarr(icnte)
    ie2a   = lonarr(icnte)
    strga  = strarr(il)
    pecode = strarr(icnte)
    tecode = strarr(icnte)
    cea    = dblarr(icnte)

    for i=0, (icnte-1) do begin
	readf, pipe, idum
        ie1a(i) = idum
    endfor
    for i=0, (icnte-1) do begin
	readf, pipe, idum
        ie2a(i) = idum
    endfor
    for i=0, (il-1) do begin
        readf, pipe, sdum
        strga(i) = sdum
    endfor
    for i=0, (icnte-1) do begin
	readf, pipe, sdum
        pecode(i) = sdum
    endfor
    for i=0, (icnte-1) do begin
	readf, pipe, sdum
        tecode(i) = sdum
    endfor
    for i=0, (icnte-1) do begin
	readf, pipe, fdum
        cea(i) = fdum
    endfor

		;*******************************************
		;**** Set default value if non provided ****
		;*******************************************

    if ( value.new lt 0 OR newflag eq 1) then begin
    	  temp = dblarr(nvmax)
        value = {	NEW:		0,				$
        		NSTRN1	:	1,				$
			ISTRN1	:	[1], 				$
			LIST	:	[''],				$
			IFOUT	:	1, 				$
			NVMAX	:	0, 				$
			NVN	:	0, 				$
			TOUT	:	temp				}
    endif
    
		;****************************************
		;**** Pop-up processing input widget ****
		;****************************************

    adas215_proc, value, newflag, titled, sz, sz0, sionpt, dsnpas, dsninc, $
		  nvmax, nv, tscef , icnte, il, pecode, tecode, cea, 	   $
		  ie1a, ie2a, strga, action, ifout, bitfile,	           $
		  FONT_LARGE=font_large, FONT_SMALL=font_small, 	   $
		  EDIT_FONTS=edit_fonts

		;**********************************************
		;**** Act on the event from the widget     ****
		;**********************************************
		;**** There are only three possible events ****
		;**** 'Done', 'Cancel' and 'Menu'.         ****
		;**********************************************

    if action eq 'Done' then begin
        lpend = 0
    endif else if action eq 'Menu' then begin
        lpend = 1
	gomenu = 1
    endif else begin
        lpend = 1
    endelse

    newflag=0		
    
		;*******************************
		;**** Write data to fortran ****
		;*******************************
    printf, pipe, lpend
    if lpend eq 0 then begin
        printf, pipe, value.nstrn1
        if ( value.nstrn1 gt 0 ) then begin
           printf, pipe, value.istrn1
        		
           for j=0,icnte-1 do begin
               printf, pipe, value.list(0,j)
               printf, pipe, value.list(1,j)
       	   endfor        		

       	   for j=0,icnte-1 do begin
        	dummy=double(value.list(2,j))
        	printf, pipe, dummy
	   endfor
        endif

	printf,pipe,value.ifout
	printf,pipe,value.nvn

	if ( value.nvn gt 0) then begin
       	    for j=0,value.nvn-1 do begin
        	dummy=double(value.tout(j))
        	printf, pipe, dummy
	    endfor
        endif

    endif


END
