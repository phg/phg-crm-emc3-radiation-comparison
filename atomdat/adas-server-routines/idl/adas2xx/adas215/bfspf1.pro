; Copyright (c) 1998 Strathclyde University.
; SCCS info: Module @(#)$Header: /home/adascvs/idl/adas2xx/adas215/bfspf1.pro,v 1.1 2004/07/06 11:39:38 whitefor Exp $ Date $Date: 2004/07/06 11:39:38 $
;+
; PROJECT:
;       ADAS IBM MVS to UNIX conversion
;
; NAME:
;	BFSPF1
;
; PURPOSE:
;	IDL user interface and communications with ADAS215 FORTRAN
;	process via pipe.
;
; EXPLANATION:
;	The 'Output Options' part of the
;	interface is invoked.  Finally the results of the user's
;	interactions are written back to the FORTRAN via the pipe.
;	Communications are with the FORTRAN subroutine BFSPF1.
;
; USE:
;	The use of this routine is specific to ADAS215, See adas215.pro.
;
; INPUTS:
;	PIPE	- The IDL unit number of the bi-directional pipe to the
;		  ADAS215 FORTRAN process.
;
;	DSFULL	- Name of input dataset for this application.
;
;	VALUE	- A structure which determines the initial settings of
;		  the 'Output Options' widget.  The initial value is
;		  set in adas215.pro.  VALUE is passed un-modified
;		  through to cw_adas215_out.pro, see that routine for a full
;		  description.
;
;	HEADER	- ADAS version header information for use in printed
;		  output.  Written to FORTRAN routine XXADAS via pipe.
;
;       BITFILE - String; the path to the dirctory containing bitmaps
;                 for the 'escape to series menu' button.
;
; OPTIONAL INPUTS:
;	None.
;
; OUTPUTS:
;	LPEND	- Integer; indicates user action on leaving output
;		  selections.  lpend is set to 1 if the user exited
;		  with the 'Cancel' button and 0 if the user exited
;		  with 'Done'.
;
;	VALUE	- On output the structure records the final settings of
;		  the output selection widget if the user pressed the
;		  'Done' button otherwise it is not changed from input.
;
;       GOMENU  - Int; flag - set to 1 if user has selected 'escape direct
;                 to series menu , otherwise 0
;
; OPTIONAL OUTPUTS:
;	None.
;
; KEYWORD PARAMETERS:
;	DEVLIST	- A string array;  Each element of the array is the
;		  name of a hardcopy graphics output device.  This
;		  list must mirror the DEVCODE list.  DEVLIST is the
;		  list which the user will see.
;		  e.g ['Post-Script','HP-PCL','HP-GL']
;
;	FONT	- Supplies the font to be used for the interface widgets.
;
; CALLS:
;	ADAS215_OUT	User interface - output options.
;
; SIDE EFFECTS:
;	This routine communicates with the ADAS215 FORTRAN process
;	via a UNIX pipe.
;
; CATEGORY:
;	Adas system.
;
; WRITTEN:
;       H. P. Summers, University of Strathclyde, 28/05/1998
;
; MODIFIED:
;	1.1	Hugh Summers
;		First Release
;
; VERSION:
;	1.1	28-05-98
;
;
;-----------------------------------------------------------------------------


PRO bfspf1, pipe, lpend, value, dsfull, header, bitfile, gomenu,	$ 
	    pid, DEVLIST=devlist, FONT=font


                ;**** Set defaults for keywords ****
  IF NOT (KEYWORD_SET(font)) THEN font = ''

		;********************************
		;**** Read data from fortran ****
		;********************************

  iosel = 0
  readf, pipe, iosel

		;**************************************
		;**** Pop-up output options widget ****
		;**************************************

  adas215_out, value, dsfull, action, bitfile, iosel,			$
                DEVLIST=devlist, FONT=font

		;*********************************************
		;**** Act on the output from the widget   ****
		;**** There are only two possible actions ****
		;**** 'Done', 'Cancel' and 'Menu'.        ****
		;*********************************************
  if action eq 'Done' then begin
    lpend = 0
  endif else if action eq 'Menu' then begin
    lpend = 1
    gomenu = 1
  endif else begin
    lpend = 1
  endelse

		;*******************************
		;**** Write data to fortran ****
		;*******************************
		;*******************************
		;**** Write data to fortran ****
		;*******************************

  printf, pipe, lpend

  if lpend eq 0 then begin

     printf, pipe, value.grpout
     if (value.grpout eq 1) then begin
	printf, pipe, value.grpscal
	if (value.grpscal eq 1) then begin
	    printf, pipe, value.xmin
	    printf, pipe, value.xmax
	    printf, pipe, value.ymin
	    printf, pipe, value.ymax
        endif
     endif

		;***************************************************
		;**** Extra check if     FORTRAN is still there ****
		;***************************************************
     if find_process(pid) eq 0 then return

		;***************************************************
		;***   If text output requested tell FORTRAN    ****
		;***************************************************
     
    printf, pipe, value.texout
    if (value.texout eq 1) then begin
	printf, pipe, strtrim(value.texdsn,2)
        printf, pipe, header
		;***********************************************************
		;**** Say whether replacment of old file is required.   ****
		;**** Yes if replace has been pressed or there is no    ****
		;**** append button, i.e. we are on the first run through **
		;***********************************************************
	if value.texrep gt 0 then begin
	    out = 1
	endif else if value.texapp lt 0 then begin
	    out = 1
	endif else begin
	    out = 0
	endelse
	printf, pipe, out
    endif

		;***************************************************
		;***   If tcx  output requested tell FORTRAN    ****
		;***************************************************
     
    printf, pipe, value.tcxout
    if (value.tcxout eq 1) then begin
        printf, pipe, value.tcxdsn
    endif

  endif

		;**************************************************
		;**** Set messages and settings ready for when ****
		;**** output window is re-displayed.           ****
		;**************************************************

 if lpend eq 0 then begin
   if value.texout eq 1 then begin

		;***************************************************
		;**** If text output is requested enable append ****
		;**** for next time 				****
		;***************************************************

     if value.texapp lt 0 and iosel eq 1 then value.texapp=1
     if value.texrep ge 0 then begin
	value.texrep = 0
	value.texapp = 1
     endif
     value.texmes = 'Output written to file.'
   endif

   if value.tcxout eq 1 then begin
		;********************************************************
		;**** If tcx  output is requested just give a messae ****
		;********************************************************
       value.tcxmes = 'File confirmed'
   endif

 endif

END
