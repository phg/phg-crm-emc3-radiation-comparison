; Copyright (c) 1998 Strathclyde University .
; SCCS info: Module @(#)$Header: /home/adascvs/idl/adas2xx/adas215/cw_adas215_proc.pro,v 1.2 2004/07/06 12:35:41 whitefor Exp $ Date $Date: 2004/07/06 12:35:41 $
;+
; PROJECT:
;       ADAS IBM MVS to UNIX conversion
;
; NAME:
;	CW_ADAS215_PROC()
;
; PURPOSE:
;	Produces a widget for ADAS215 processing options/input.
;
; EXPLANATION:
;	This function creates a compound widget consisting of a label
;	widget holding contour pass file name, the dataset name/browse
;	widget cw_adas_dsbr, label widgets with charge information for
;	the input data, a single selection list widget cw_single_sel
;	to toggle the Equilibrium/Non-Equilibrium flag, a set of label and
;	text widgets for supplying Metastable Level Scalings for the
;	Non-Equilibrium case, two multiple selection widgets cw_215_sel
;	to set the first and second composite assemblies,
;	a message widget, a 'Cancel' button and finally a 'Done' button.
;
;	The compound widgets included in this widget are self managing.
;	This widget only manages events from the two 'Defaults' buttons,
;	the 'Cancel' button and the 'Done' button.
;
;	This widget only generates events for the 'Done' and 'Cancel'
;	buttons.
;
; USE:
;	This widget is specific to ADAS215, see adas215_proc.pro
;	for use.
;
; INPUTS:
;	TOPPARENT- Long integer, ID of parent widget.
;
;	TITLED	- String, Ion name.
;
;	SZ	- String, Recombined ion charge.
;
;	SZ0	- String, Nuclear charge.
;
;	SIONPT	- String, Ionisation potential.
;
;	DSNPAS	- String, The full system file name of the input
;			Contour Passing file.
;	DSNINC	- String, The full system file name of the input COPASE
;			dataset selected by the user for processing.
;
;	NVMAX	- Integer, Max. no. of electron temperatures.
;
;	NV	- Integer, Number of input electron temperatures.
;
;	TSCEF	- Float array, input electron temperatures 
;
;	ICNTE	- Integer, Number of Electron Impact Transitions.
;
;	IL	- Integer, Number of energy levels.
;
;	IE1A	- Integer array, Lower level index of transition
;
;	IE2A	- Integer array, Upper level index of transition
;
;	STRGA	- String array, Level designations
;
;	The inputs TITLED to STRGA map onto variables of similar
;	name in the ADAS215 FORTRAN program.
;
;	IFOUT	- Integer, Form of output electron temperatures.
;
;	NVN	- Integer, Number of output electron temperatures.
;
;	TOUT	- Float array, output electron temperatures
;
;       BITFILE - String; the path to the dirctory containing bitmaps
;                 for the 'escape to series menu' button.
;
; OPTIONAL INPUTS:
;	None
;
; OUTPUTS:
;	The return value of this function is the ID of the declared widget.
;
; OPTIONAL OUTPUTS:
;	None
;
; KEYWORD PARAMETERS:
;	VALUE	- A structure which determines the initial settings of
;		  the processing options widget.
;
;		  The default VALUE is created thus;
;
;			ps = {	NSTRN1:1, 				$
;				ISTRN1:[1], 				$
;				IFOUT:1, 				$
;				NVMAX:0, 				$
;				NVN:0,					$
; 				TOUT:[0.0]				}
;
;		NSTRN1	Number of transitions in first assembly
; 		(ISTRN1(I),I=1,NSTRN1)	index of transitions
;
;		All of these structure elements 
;		map onto variables of the same name in the ADAS215 FORTRAN 
;		program.
;
;
;	UVALUE	- A user value for the widget. Default 0.
;
;	FONT_LARGE - The name of a larger font.  Default current system
;		     font
;
;	FONT_SMALL - The name of a smaller font. Default current system
;		     font.
;
;	EDIT_FONTS - A structure of two fonts used in the table editor
;		     adas_edtab.pro, {font_norm:'',font_input:''}.  Defaults
;		     to current system font.
;
;	NUM_FORM   - String; Numeric format to use in tables.  Default
;			'(E10.3)'
;		     
;
; CALLS:
;	CW_ADAS_DSBR	Dataset name and comments browsing button.
;	CW_215_SEL	Adas mulitple selection widget.
;	CW_SINGLE_SEL	Adas single selection widget.
;
;	See side effects for other related widget management routines.
;
; SIDE EFFECTS:
;	The following widget management routines are included in this
;	file;
;	PROC215_GET_VAL()	Returns the current VALUE structure.
;	PROC215_EVENT()		Process and issue events.
;
; CATEGORY:
;	Compound Widget
;
; WRITTEN:
;       H. P. Summers, University of Strathclyde, 7/5/98
;
; MODIFIED:
;	1.1	Hugh Summers
;		First version
;	1.2	Richard Martin
;		Initialised pecode(0) to 'p' for newflag=1 case
;
; VERSION:
;	1.1	07/05/98
;	1.2	04/04/00
;
;
;-----------------------------------------------------------------------------

FUNCTION proc215_get_val, id

                ;**** Return to caller on error ****

    ON_ERROR, 2

                ;**** Retrieve the state ****

    first_child = widget_info( id, /child )
    widget_control, first_child, get_uvalue=state, /no_copy


		;**************************************************
		;**** Get First Composite Assembly from widget ****
		;**************************************************

    widget_control, state.fcmpaid, get_value=vcmpa
    scmpa = where( vcmpa.selected gt -1 )
    if scmpa(0) gt -1 then begin
	nstrn1 = size( scmpa )
        nstrn1 = fix( nstrn1(1) )
	istrn1 = vcmpa.selected( scmpa ) + 1
    endif else begin
	nstrn1 = 0
	istrn1 = [0]
    endelse
    
    list = vcmpa.list

		;****************************************************
		;**** Get new temperature data from table widget ****
		;****************************************************

    widget_control,state.tempid,get_value=tempval
    tempdata = tempval.value
    ifout = tempval.units+1

		;**** Copy out temperature values ****
    blanks = where(strtrim(tempdata(0,*),2) eq '')
    if blanks(0) ge 0 then nvn=blanks(0) else nvn=state.nvmax
    tout = dblarr(state.nvmax)
    if nvn gt 0 then begin
       tout(0:nvn-1) = double(tempdata(0,0:nvn-1))
    end

		;**** Fill out the rest with zero ****
    if nvn lt state.nvmax then begin
      tout(nvn:state.nvmax-1) = double(0.0)
    end


		;******************************************
		;**** Write selections to PS structure ****
		;******************************************

    ps = { 	NEW: 0,							$
    		NSTRN1:nstrn1, 						$
		ISTRN1:istrn1, 						$
		LIST:	list,							$
		IFOUT:ifout, 						$
		NVMAX:state.nvmax, 					$
		NVN:nvn, 						$
		TOUT:tout						}


    widget_control, first_child, set_uvalue=state, /no_copy

    RETURN, ps

END

;-----------------------------------------------------------------------------

FUNCTION proc215_event, event


                ;**** Base ID of compound widget ****

    parent = event.handler

                ;**** Retrieve the state ****

    first_child = widget_info( parent, /child )
    widget_control, first_child, get_uvalue=state, /no_copy

		;*********************************
		;**** Clear previous messages ****
		;*********************************

    widget_control, state.messid, set_value=' '

                ;************************
                ;**** Process events ****
                ;************************

    CASE event.id OF

		;*************************************
		;**** Default temperatures button ****
		;*************************************
   	 state.deftid: begin

		;**** popup window to confirm overwriting current values ****
		action= popup(message='Confirm Overwrite Temperatures with Defaults', $
			buttons=['Confirm','Cancel'],font=state.font)
		if action eq 'Confirm' then begin
			;**** Get current table widget value ****
 	 		widget_control,state.tempid,get_value=tempval

			;**** Get the default temperatures and set new units ****
 	 		xxdeft, state.nvmax, nvn, deft
	  		tempval.units = 2

			;**** Copy defaults into value structure ****
 	  		if nvn gt 0 then begin
   	   			tempval.value(0,0:nvn-1) = $
				strtrim(string(deft(0:nvn-1),format=state.num_form),2)
 			end

			;**** Fill out the rest with blanks ****
 	 		if nvn lt state.nvmax then begin
   	  		 	tempval.value(0:0,nvn:state.nvmax-1) = ''
 	 		end

			;**** Copy new temperature data to table widget ****
 	  		widget_control,state.tempid,set_value=tempval
		end

     	 end

		;***********************
		;**** Cancel button ****
		;***********************

    	state.cancelid: new_event = {ID:parent, TOP:event.top, 		$
				     HANDLER:0L, ACTION:'Cancel'}

		;*********************
		;**** Done button ****
		;*********************

    	state.doneid: begin

			;***************************************
			;**** Check all user input is legal ****
			;***************************************
	    error = 0
	    widget_control, first_child, set_uvalue=state, /no_copy
	    widget_control, event.handler, get_value=ps
	    widget_control, first_child, get_uvalue=state, /no_copy

		;**** check temperatures entered ****
	if error eq 0 and ps.nvn eq 0 then begin
	  error = 1
	  message='Error: No temperatures entered.'
        end

		;**** return value or flag error ****
	if error eq 0 then begin
	  new_event = {ID:parent, TOP:event.top, HANDLER:0L, ACTION:'Done'}
        end else begin
	  widget_control,state.messid,set_value=message
	  new_event = 0L
        end

        end

                ;**** Menu button ****

    	state.outid: begin
            new_event = {ID:parent, TOP:event.top, HANDLER:0L,          $
                         ACTION:'Menu'}
        end

    	ELSE: new_event = 0L

    ENDCASE

		;**********************************
		;**** Save 'state' information ****
		;**********************************

    widget_control, first_child, set_uvalue=state, /no_copy

    RETURN, new_event

END

;-----------------------------------------------------------------------------

FUNCTION cw_adas215_proc, topparent, titled, sz, sz0, sionpt, dsnpas, 	$
                          dsninc, nvmax, nv, tscef, icnte, il,	 	$
			  pecode, tecode, cea, newflag,				$ 	
		          ie1a, ie2a, strga, ifout, bitfile,	$
		          VALUE=value, UVALUE=uvalue, 			$
		          FONT_LARGE=font_large, FONT_SMALL=font_small, $
		          EDIT_FONTS=edit_fonts, NUM_FORM=num_form

		;**** Set defaults for keywords ****

    IF NOT (KEYWORD_SET(uvalue)) THEN uvalue = 0
    IF NOT (KEYWORD_SET(font_large)) THEN font_large = ''
    IF NOT (KEYWORD_SET(font_small)) THEN font_small = ''
    IF NOT (KEYWORD_SET(edit_fonts)) THEN 				$
    edit_fonts = {font_norm:'',font_input:''}
    IF NOT (KEYWORD_SET(num_form)) THEN num_form = '(E10.3)'
    IF NOT (KEYWORD_SET(value)) THEN begin
	ps = {NEW:0	,						$	
		NSTRN1:1, 						$
		ISTRN1:[1], 						$
		LIST:[''],							$
		IFOUT:1, 						$
		NVMAX:0, 						$
		NVN:0, 							$
		TOUT:[0.0]						}
    ENDIF ELSE BEGIN
	ps = {NEW: value.new,						$
		NSTRN1:value.nstrn1, 					$
		ISTRN1:[value.istrn1], 					$
		LIST:[value.list],						$
		IFOUT:value.ifout, 					$
		NVMAX:value.nvmax, 					$
		NVN:value.nvn, 						$
		TOUT:[value.tout]					}
	if  ps.nstrn1  gt icnte then begin
	    ps = {	NEW:0,					$
	    		NSTRN1:1, 					$
			ISTRN1:[1], 					$
			LIST:[''],						$			
			IFOUT:1, 					$
			NVMAX:0, 					$
			NVN:0, 						$
			TOUT:[0.0]					}
	endif
    ENDELSE

		;***************************************************
		;**** Assemble temperature table data           ****
		;***************************************************
		;**** The adas table widget requires data to be ****
		;**** input as strings, so all the numeric data ****
		;**** has to be written into a string array.    ****
		;**** Declare temp table array                  ****
		;**** Three editable columns and input values   ****
		;**** in three different units.                 ****
		;***************************************************

  tempdata = strarr(4,nvmax)

		;**** Copy out temperature values ****
  if ps.nvn gt 0 then begin
    tempdata(0,0:ps.nvn-1) = $
		strtrim(string(ps.tout(0:ps.nvn-1),format=num_form),2)
  end
		;**** input values in three units ****
  tempdata(1,0:nv-1) = strtrim(string(tscef(0:nv-1,0),format=num_form),2)
  tempdata(2,0:nv-1) = strtrim(string(tscef(0:nv-1,1),format=num_form),2)
  tempdata(3,0:nv-1) = strtrim(string(tscef(0:nv-1,2),format=num_form),2)

		;**** Fill out the rest with blanks ****
  if ps.nvn lt nvmax then begin
    tempdata(0:0,ps.nvn:nvmax-1) = ''
  end
  if nv lt nvmax then begin
    tempdata(1:3,nv:nvmax-1) = ''
  end


		;********************************************************
		;**** Create the 215 Processing options/input window ****
		;********************************************************

		;**** create titled base widget ****

    parent = widget_base( topparent, UVALUE = uvalue, 			$
			  EVENT_FUNC = "proc215_event", 		$
			  FUNC_GET_VALUE = "proc215_get_val", 		$
			  /COLUMN )

		;********************************************************
		;**** Create a dummy widget just to hold value of    ****
		;**** "state" variable so as not to get confused     ****
		;**** with any other values. Adopt IDL practice      ****
		;**** of using first child widget                    ****
		;********************************************************

    first_child = widget_base( parent )
    topbase = widget_base( first_child, /column )

		;**** add dataset name and browse button ****

    rc = cw_adas_dsbr( topbase, dsninc, font=font_large, /row)

		;**** add charge information ****

    rc = widget_label( topbase, font=font_large, 			$
		       value='ION: '+titled+sz+'   Nuclear Charge: '+sz0+$
		       '  Ionisation Potential: '+sionpt+' (cm-1)' )

		;**** Metastable Level base ****

;    metabase = widget_base( parent, /row, /frame )

		;**** add electron information ****

    rc = widget_label( parent, font=font_large, 			$
			  value='No. of Electron Impact Transitions: ' 	$
			+strtrim(string(icnte),2) 			$
			+'  No. of Index Energy Levels: '+		$
                        strtrim(string(il),2) )

		;**** Build transition strings ****

    trantab=strarr(4,icnte)
    if ( newflag eq 1 or ps.new eq -1 ) then begin
      pecode(0)='p'
    	for i = 0, icnte-1 do begin
 		trantab(0,i) = strtrim(pecode(i),2)
 		trantab(1,i) = strtrim(tecode(i),2)
 		trantab(2,i) = strtrim(string(cea(i),format='(F5.2)'),2)
		trantab(3,i) = string( i+1, format='(I3)' ) 			$
			+'  '+string( ie2a(i), format='(I2)' ) 		$
			+' '+strga( ie2a(i)-1 ) 			$
			+' -- '+string( ie1a(i), format='(I2)' ) 	$
			+' '+strga( ie1a(i)-1 )

    	endfor
    endif else begin
    	for i = 0, icnte-1 do begin  
    		trantab(0,i) = ps.list(0,i)
		trantab(1,i) = ps.list(1,i)
		trantab(2,i) = ps.list(2,i)
		trantab(3,i) = ps.list(3,i)
	endfor
   endelse

		;**** Composite Assembly base ****

    compbase = widget_base( parent, /row, /frame )

		;**** add first composite assembly ****


    selected=intarr(20)
    if ps.nstrn1 gt 0 then selected(0:ps.nstrn1-1) = ps.istrn1
    selected = selected - 1
    fcmpaid = cw_215_sel( compbase, trantab, selected=selected, 	$
		title='Electron transitions for selection', /ordered, 	$
		font=font_small )

		;**** base for the temperature table and defaults button ****

  base = widget_base(compbase,/column,/frame)

		;**** temperature data table ****
  colhead = [['Electron','Input'], $
	     ['Temp.','Value']]
		;**** convert FORTRAN index to IDL index ****
  units = ps.ifout-1
  unitname = ['Kelvin','eV','Reduced']
		;**** Two columns in the table and three sets of units. ****
		;**** Column 1 has the same values for all three        ****
		;**** units but column 2 switches between sets 1, 2 & 3 ****
		;**** in the input array tempdata as the units change.  ****
  unitind = [[0,0,0],[1,2,3]]
  tempid = cw_adas_table(base, tempdata, units, unitname, unitind, $
			UNITSTITLE = 'Temperature Units', /scroll, $
			COLEDIT = [1,0], COLHEAD = colhead,        $
			TITLE = 'Select Temperatures', ORDER = [1,0], $
			LIMITS = [2,2], CELLSIZE = 12, $
			FONTS = edit_fonts, FONT_LARGE = font_large, $
			FONT_SMALL = font_small)

		;**** Default temperatures button ****
  base = widget_base(base,/column)
  deftid = widget_button(base,value='Default Temperatures',font=font_large)

		;**** Error message ****

    messid = widget_label(parent,font=font_large, 			$
    value='Edit the processing options data and press Done to proceed')

		;**** add the exit buttons ****

    base215 = widget_base(parent,/row)
    menufile = bitfile + '/menu.bmp'
    read_X11_bitmap, menufile, bitmap1
    outid = widget_button(base215, value=bitmap1)          ;menu button
    cancelid = widget_button(base215,value='Cancel',font=font_large)
    doneid = widget_button(base215,value='Done',font=font_large)
  
		;*************************************************
		;**** create a state structure for the pop-up ****
		;**** window.                                 ****
		;*************************************************

    new_state = { OUTID:outid,						$
		  FCMPAID:fcmpaid, 					$
		  TEMPID:tempid, 					$
		  DEFTID:deftid,					$
		  CANCELID:cancelid, 					$
		  DONEID:doneid, 					$
		  MESSID:messid, 					$
		  NUM_FORM:num_form, 					$
		  IFOUT:ps.ifout,					$
		  NVMAX:nvmax,						$
		  NVN:ps.nvn, 						$
		  IL:il, 						$
		  NV:nv, 						$
		  TSCEF:tscef, 						$
		  FONT:font_large }

                ;**** Save initial state structure ****

    widget_control, first_child, set_uvalue=new_state, /no_copy

		;**** Short pause before returning ****

    wait, 1.0
    RETURN, parent

END

