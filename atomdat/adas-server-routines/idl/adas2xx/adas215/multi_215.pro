; Copyright (c) 1995 Strathclyde University .
; SCCS info: Module @(#)$Header: /home/adascvs/idl/adas2xx/adas215/multi_215.pro,v 1.2 2004/07/06 14:22:28 whitefor Exp $ Date $Date: 2004/07/06 14:22:28 $
;+
; PROJECT:
;	ADAS IBM MVS to DEC UNIX conversion.
;
; NAME:
;	MULTI_215()
;
; PURPOSE:
;	Produces a 'Pop-up' window with a multiple selection list.
;       This version of the widget has been altered specifically for use
;       with the adas215 processing options screen.
;
; EXPLANATION:
;	Produces an interactive multiple selection list in a 'Pop-up'
;	window.  The multiple-selection list consists of one toggle
;	button for each item in a given list (this is the compound
;	widget CW_MULTI_215) plus 'Cancel' and 'Done' buttons.
;	The caller may specify a title for the window.
;
;	The LIST is provided as	a one dimensional string array of
;	items for selection.  The function returns the selected items
;	to the caller in the form of an integer array giving the indices
;	of the items selected from the LIST.  The caller may specify the
;	maximum number of selections allowed from the list.  The user
;	can press the 'Done' button to confirm the new selections or
;	'Cancel' to retain the original selections if any.  The caller
;	may specify that items are marked as selected on startup using
;	the SELECTED keyword.
;
; USE:
;	The following invokes a pop-up window with a multiple selection
;	list of a number of colours.  The user is allowed to make a
;	maximum of three selections from the list.  The function returns
;	an integer array which gives the indices of the elements from
;	the LIST which have been selected.  If fewer than the maximum
;	number of selections are made the array is filled out with
;	-1 values.  When the user activates the 'Done' button the
;	selected items are printed to the screen in the order in which
;	they were selected.
;
;	list=['Black','Blue','Pink','Green','Purple','Red','Orange']
;	sels = MULTI_215(list,MAXSEL=3,TITLE='Select Colours')
;	print,list(sels(where(sels gt -1)))
;
;	The following code reactivates the selection list window with
;	the same set of selections already made on startup.  Because
;	sels has three elements the maximum number of allowed selections
;	is automatically set to three.
;
;	sels = MULTI_215(list,SELECTED=sels,TITLE='Select Colours')
;
; INPUTS:
;	LIST     - One dimensional string array of items for the
;		   selection list.
;
; OPTIONAL INPUTS:
;	None.  See the keywords for additional controls.
;
; OUTPUTS:
;	The return value of this function is a one dimensional array
;	of indices of the items selected from LIST (given in the same
;	form as the value for the SELECTED keyword).
;
; OPTIONAL OUTPUTS:
;	None.
;
; KEYWORDS:
;       SELECTED - One dimensional integer array of the indices of the
;                  items in LIST which are to be set as selected when
;		   first displayed.  The length of SELECTED is the
;                  maximum number of selections allowed.  Extra spaces
;                  can be reserved in SELECTED with values of -1.  All
;		   -1 values should be at the end of the array.
;                  For example SELECTED=[0,5,-1,-1] will give a maximum
;		   of 4 selections and items 0 and 5 from list are
;		   selected on start up.  The default is no selections
;		   set on startup.
;
;       MAXSEL   - Maximum number of selections allowed.  The default is
;                  one unless the SELECTED keyword is specified in which
;                  case the value of MAXSEL is ignored.
;
;	ORDERED  - When the ordered keyword is non-zero the list of 
;		   selections in the SELECTED array is arranged in order
;		   of ascending value regardless of the order in which 
;		   the list items were chosen.  The -1 values used to
;		   pad the selected array still remain at the end.
;		   For example if the ordered keyword is not set the
;		   values in selected will match the order in which the
;		   items were selected from LIST e.g [3,1,5,-1,-1].
;		   With the ordered keyword non-zero the SELECTED array
;		   would be [1,3,5,-1,-1].
;
;	TITLE	 - Title to appear on the selection list window banner.
;		   The default is 'Make your Selections'.
;
;	FONT	 - The text font to be used for the window.  The default
;		   is the current X default.
;
; CALLS:
;	CW_MULTI_215()
;	XMANAGER
;
;	See side effects for other routines indirectly called.
;
; SIDE EFFECTS:
;	A COMMON BLOCK: MSEL_COM is used which is private to MULTI_215()
;	and MULTI_215_EVENT.
;
;	This routine uses XMANAGER to manage the multiple selection
;	window and hence via event generation and use of the
;	WIDGET_CONTROL procedure the following routines and functions are
;	indirectly called;
;
;	MULTI_215_EVENT		(included in this file)
;	MULSEL_SET_VAL		(see CW_MULTI_215.PRO)
;	MULSEL_GET_VAL()	(see CW_MULTI_215.PRO)
;	MULSEL_EVENT()		(see CW_MULTI_215.PRO)
;
;	The selection list widget uses a private common block: CW_MULSEL_BLK
;
; CATEGORY:
;	Widgets.
;
; WRITTEN:
;       Hugh Summers, University of Strathclyde, 1998
;
; MODIFIED:
;	  1.1	Hugh Summers.
;	  	First version.
;	  1.2	Martin O'Mullane
;	  	Changed to use new cw_multi_215 widget
;
; VERSION:
;       1.1 09-06-98
;       1.1 25-01-99
;-
;-----------------------------------------------------------------------------

PRO multi_215_event, event

  COMMON msel215_com, selid, sels

		;**** see which button generated the event ****
  widget_control,event.id,get_value=button

		;**** copy selections to common for 'Done' ****
  if button eq 'Done' then begin
  	widget_control,selid,get_value=sels
  endif 

		;**** for either button end the list window ****
  widget_control,event.top,/destroy

END


;-----------------------------------------------------------------------------



FUNCTION multi_215, list, MAXSEL=maxsel,  TITLE=title, FONT=font, _extra=eee 

		;**** private common, id of selection widget and ****
		;**** the list of selections.                    ****
 
  COMMON msel215_com, selid, sels
  
		;**** Set defaults for keywords ****
                
  IF NOT (KEYWORD_SET(title)) THEN title = 'Make Your Selections'
  IF NOT (KEYWORD_SET(font))  THEN font = ''

		;**** create titled base widget ****
  base215 = widget_base(title=title,XOFFSET=50,YOFFSET=50,/column)

		;**** add the multi_215 compound widget ****

		;**** A bit of a hack - make appropriate structure ****
		;**** to send to the calling program               ****

  ty=1
  idum=1
  listdim=size(list)
  listdim=listdim(2)
  if  list(0,0) eq 'p' then sl=1 else sl=0
  reads,list(1,0),ty
  reads,list(2,0),cv
  cf=list(3,0)
  for i=1,listdim-1 do begin
    if  list(0,i) eq 'p' then sl=[sl,1] else sl=[sl,0]
    reads,list(1,i),idum
    ty=[ty,idum]
    reads,list(2,i),dum
    cv=[cv,dum]
    cf=[cf,list(3,i)]
  endfor
  inlist = {type:ty, cval: cv, cfg:cf, sel:sl}
  
  selid = cw_multi_215(base215, value=inlist, maxsel=maxsel, font=font)

		;**** copy inital selections to common ****
                
  widget_control,selid,get_value=sels
  
                ;**** informational message ****
                
  messid = widget_label(base215,font=font, value=' ')

		;**** add the exit buttons ****
                
  butbase = widget_base(base215,/row)
  rc = widget_button(butbase,value='Cancel',font=font)
  rc = widget_button(butbase,value='Done',font=font)
  
  widget_control,base215,/realize

		;**** make widget modal and wait until destroyed ****
		;**** Is this a fudge? Will it work with future   ****
		;**** versions of xmanager? Using the MODAL and   ****
		;**** JUST_REG keywords has the effect of making  ****
		;**** xmanager wait until the widget is destroyed ****
		;**** and then returning from this call.          ****
 
  xmanager,'multi_215',base215,event_handler='multi_215_event',/modal,/just_reg
		
                ;**** return sels to the caller ****
  RETURN, sels

END
