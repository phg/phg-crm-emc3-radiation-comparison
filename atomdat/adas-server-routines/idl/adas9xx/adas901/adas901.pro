;+
; PROJECT:
;       ADAS
;
; NAME:
;       ADAS901
;
; PURPOSE:
;       Interrogate and plot data in mdf02 molecular data files.
;
; USE:
;       First the system settings must be established by calling
;       adas_sys_set.pro then adas901.pro is called.
;
;       adas_sys_set, adasrel, fortdir, userroot, centroot, $
;                     devlist, devcode, font_large, font_small, edit_fonts
;       adas901,   adasrel, fortdir, userroot, centroot, devlist, $
;                  devcode, font_large, font_small, edit_fonts
;
; INPUTS:
;       ADASREL - A string indicating the ADAS system version,
;                 e.g ' ADAS RELEASE: ADAS93 V1.4'.  The first
;                 character should be a space.
;
;       FORTDIR - Not applicable here.
;
;       USERROOT - A string holding the path to the root directory of
;                  the user's adas data. e.g '/disk/bowen/adas'
;                  This root directory will be used by adas to construct
;                  other path names.  For example the users default data
;                  for adas205 should be in /disk/bowen/adas/adf04.  In
;                  particular the user's default interface settings will
;                  be stored in the directory USERROOT+'/defaults'.  An
;                  error will occur if the defaults directory does not
;                  exist.
;
;       CENTROOT - Like USERROOT, but this directory points to the
;                  central data area for the system.  User defaults are
;                  not stored on CENTROOT.
;
;       DEVLIST - A string array of hardcopy device names, used for
;                 graphical output. e.g ['Post-Script','HP-PCL','HP-GL']
;                 This array must mirror DEVCODE.  DEVCODE holds the
;                 actual device names used in a SET_PLOT statement.
;
;       DEVCODE - A string array of hardcopy device code names used in
;                 the SET_PLOT statement, e.g ['PS', 'PCL', 'HP']
;                 This array must mirror DEVLIST.
;
;       FONT_LARGE - The name of a larger font e.g 'courier_bold14'
;
;       FONT_SMALL - The name of a smaller font e.g 'courier_bold12'
;
;       EDIT_FONTS - A structure of two fonts used in the table editor
;                    adas_edtab.pro, {font_norm:'',font_input:''}.
;                    The two fonts are used to differentiate between
;                    editable and non-editable parts of the table. You
;                    may have to experiment to get two fonts which give
;                    the same default spacing in the edit widget.
;                    e.g {font_norm:'helvetica_bold14', $
;                         font_input:'helvetica_oblique14'}
;
; OPTIONAL INPUTS:
;       None
;
; OUTPUTS:
;       None - Note this routine should leave all inputs un-modified.
;
; OPTIONAL OUTPUTS:
;       None
;
; KEYWORD PARAMETERS:
;       None
;
; CALLS:
;
; SIDE EFFECTS:
;
; CATEGORY:
;       Adas system.
;
; WRITTEN:
;       Martin O'Mullane
;
; MODIFIED:
;       1.1     Martin O'Mullane
;                - First release
;       1.2     Martin O'Mullane
;                - Add x and y axis titles set by mdf02 transition type.
;                - Set read_mdf02 parameters according to method chosen
;                  in the processing panel.
;
; VERSION:
;       1.1     17-10-2012
;       1.2     19-12-2012
;
;-
;-----------------------------------------------------------------------------

PRO adas901, adasrel, fortdir, userroot, centroot,                   $
             devlist, devcode, font_large, font_small, edit_fonts


; Initialisation

adasprog = ' PROGRAM: ADAS901 v1.0'
deffile  = userroot + '/defaults/adas901_defaults.dat'
bitfile  = centroot + '/bitmaps'
device   = ''


; Restore or set the defaults

files = findfile(deffile)
if files[0] eq deffile then begin

    restore, deffile
    inval.centroot = centroot + '/mdf02/'
    inval.userroot = userroot + '/mdf02/'

endif else begin

    inval = { ROOTPATH   : userroot+'/mdf02/',    $
              FILE       : '',                    $
              CENTROOT   : centroot+'/mdf02/',    $
              USERROOT   : userroot+'/mdf02/'     }

    procval = {NEW : -1}

    outval = { GRPOUT    : 0,           $
               GTIT1     : '',          $
               GRPSCAL   : 0,           $
               XMIN      : '',          $
               XMAX      : '',          $
               YMIN      : '',          $
               YMAX      : '',          $
               HRDOUT    : 0,           $
               HARDNAME  : '',          $
               GRPDEF    : '',          $
               GRPFMESS  : '',          $
               GRPSEL    : -1,          $
               GRPRMESS  : '',          $
               DEVSEL    : -1,          $
               GRSELMESS : '',          $
               TEXOUT    : 0,           $
               TEXAPP    : -1,          $
               TEXREP    : 0,           $
               TEXDSN    : '',          $
               TEXDEF    : 'paper.txt', $
               TEXMES    : ''           }

endelse


; Document the run

date   = xxdate()
header = adasrel + adasprog + ' DATE: ' + date[0] + ' TIME: ' + date[1]



LABEL100:

rep = adas901_in(inval, FONT_LARGE=font_large, FONT_SMALL = font_small)

if rep eq 'CANCEL' then goto, LABELEND


; Processing screen

LABEL200:


adas_file  = inval.rootpath + inval.file

rep = adas901_proc(adas_file, procval,         $
                   bitfile,                    $
                   FONT_LARGE = font_large,    $
                   FONT_SMALL = font_small)

if rep eq 'MENU'   then goto, LABELEND
if rep eq 'CANCEL' then goto, LABEL100


; Plotting screen

LABEL300:


rep = adas901_output(adas_file, outval,          $
                     bitfile,                    $
                     FONT_LARGE = font_large,    $
                     FONT_SMALL = font_small)

case rep of

   'MENU'     : goto, LABELEND
   'CANCEL'   : goto, LABEL200
   else       : begin

                   ; Get data for transition

                   x = procval.energy

                   if procval.itab EQ 0 then begin

                      species_in  = procval.s_in
                      elect_in    = procval.e_in
                      vib_in      = procval.v_in
                      species_out = procval.s_out
                      elect_out   = procval.e_out
                      vib_out     = procval.v_out
                      process     = procval.p_index

                   endif else begin

                      species_in  = -1
                      elect_in    = -1
                      vib_in      = 'X'
                      species_out = -1
                      elect_out   = -1
                      vib_out     = 'X'
                      process     = -1

                      reads, procval.trans, species_in, elect_in, vib_in,    $
                                            species_out, elect_out, vib_out, $
                                            process,                         $
                                            format='(i2, i2, a3, i2, i2, a3, i3)'

                   endelse

                   read_mdf02,  file        = adas_file,   $
                                species_in  = species_in,  $
                                elect_in    = elect_in,    $
                                vib_in      = vib_in,      $
                                species_out = species_out, $
                                elect_out   = elect_out,   $
                                vib_out     = vib_out,     $
                                process     = process,     $
                                energy      = x,           $
                                data        = y,           $
                                xtitle      = xtitle,      $
                                ytitle      = ytitle

                   sel_str = 'in : ' + string(procval.s_in, format='(i2)')     + ', ' +      $
                             string(procval.e_in, format='(i3)')               + ', ' +      $
                             procval.v_in                                      +             $
                             '; out : ' + string(procval.s_out, format='(i2)') + ', ' +      $
                             string(procval.e_out, format='(i3)')              + ', ' +      $
                             procval.v_out                                     +             $
                             '; process : ' + string(procval.p_index, format='(i2)') + $
                             ' ;  diss ch : ' ; + procval.diss_channel


                   title = ['MOLECULAR COEFFICIENT VERSUS ENERGY/TEMPERATURE :', $
                            '', $
                            'ADAS       : ' + header,    $
                            'FILE       : ' + adas_file, $
                            'TRANSITION : ' + sel_str,   $
                            'KEY        : (CROSSES - INPUT DATA) (FULL LINE - FORMULA FIT)']

                   graph_content = { x         : x,         $
                                     y         : y,         $
                                     xtitle    : xtitle,    $
                                     ytitle    : ytitle,    $
                                     header    : header,    $
                                     adas_file : adas_file, $
                                     sel_str   : sel_str,   $
                                     title     : title      }

                   if rep EQ 'GRAPH' or rep EQ 'BOTH' then begin
                      res = adas901_plot(graph_content, outval,      $
                                         bitfile,                    $
                                         FONT_LARGE = font_large,    $
                                         FONT_SMALL = font_small)
                   endif

                   if rep EQ 'PAPER' or rep EQ 'BOTH' then adas901_outfile, outval.texdsn, graph_content

                   goto, LABEL300

                end
endcase


LABELEND:

; Save user defaults

save, inval,  procval, outval, filename=deffile

close, /all

END
