;+
; PROJECT: ADAS
;
; NAME: ADAS901_OUTPUT
;
; PURPOSE: This procedure asks the user for output options for
;          plotting to screen and writing the output to a text file.
;
; INPUTS:
;         adas901_output, adas_file,  outval,         $
;                         bitfile, devlist, devcode,  $
;                         FONT_LARGE = font_large,    $
;                         FONT_SMALL = font_small
;
;          adas_file  - mdf02
;          outval     - structure holding output options
;          bitfile    - location of bitmaps
;          devlist    - printer devices
;
;
; OPTIONAL INPUTS:
;       None
;
; OUTPUTS:
;       The function returns a string rep
;           rep = 'CONTINUE' user is happy with choices made
;           rep = 'MENU'     to leave ADAS901
;           rep = 'CANCEL'   user want to go back a screen or the user
;                            closes the widget with the mouse.
;       The function may modify outval structure
;
; OPTIONAL OUTPUTS:
;       None
;
; KEYWORD PARAMETERS:
;       FONT_LARGE - The name of a larger font e.g 'courier_bold14'
;       FONT_SMALL - The name of a smaller font e.g 'courier_bold12'
;
;
; CALLS:
;
;       ADAS901_OUTPUT_NULL_EVENTS  : Black hole for events which we don't use.
;       ADAS901_OUTPUT_MENU         : Event handler for menu (bitmap) actions.
;       ADAS901_OUTPUT_EVENT        : Event handler for most actions.
;
;
; SIDE EFFECTS:
;
;
; CATEGORY:
;       Widgets
;
; WRITTEN:
;       Martin O'Mullane
;
; MODIFIED:
;       1.1     Martin O'Mullane
;                - First release
;
; VERSION:
;       1.1     18-10-2012
;
;-
;-----------------------------------------------------------------------------


FUNCTION ADAS901_output_NULL_EVENTS, event

   ; The purpose of this event handler is to do nothing
   ; and ignore all events that come to it.

   RETURN, 0

END
;-----------------------------------------------------------------------------



PRO ADAS901_output_MENU, event

; The 'normal' get_value from a widget_button does not work with a
; bitmap button. Hence this specialised event handler.

; Get the info structure out of the user value of the top-level base.

widget_control, event.top, get_uvalue=info


formdata = {cancel : 0, menu:1}
*info.ptrToFormData = formdata
widget_Control, event.top, /destroy

END
;-----------------------------------------------------------------------------


PRO ADAS901_OUTPUT_EVENTS, event

; Get the info structure out of the user value of the top-level base.

Widget_Control, event.handler, Get_UValue=info


; Clear any messages

widget_control, info.messID, set_value = ' '


; Process events

CASE event.id OF

  info.graphID   : 
  
  info.paperID   :
  
  info.cancelid  : begin
                      formdata = {cancel:1}
                      *info.ptrToFormData = formdata
                      widget_Control, event.top, /destroy
                   end


  info.doneID    : begin

                      err  = 0
                      mess = ' '

                      widget_control, info.graphID, get_value=graph
                      widget_control, info.paperID, get_value=paper
                      
                      if paper.outbut EQ 1 then begin
                      
                          file_acc, paper.filename, exist, read, write, execute, filetype
                          
                          if exist EQ 1 AND paper.repbut EQ 0 then begin
                             err  = 1
                             mess = 'Replace the existing file?'
                          endif
                          
                      endif

                      if err EQ 0 then begin
                         formdata = {test     : '901_output', $
                                     graph    : graph,        $
                                     paper    : paper,        $
                                     menu     : 0,            $
                                     cancel   : 0             }
                          *info.ptrToFormData = formdata
                          widget_control, event.top, /destroy
                      endif else begin
                         widget_control, info.messID, set_value=mess
                      endelse

                   end

  else : message, 'You should not see this message!', /continue

ENDCASE


END
;-----------------------------------------------------------------------------



FUNCTION adas901_output, adas_file, outval,          $
                         bitfile,                    $
                         FONT_LARGE = font_large,    $
                         FONT_SMALL = font_small

; Initial values

menufile = bitfile + '/menu.bmp'
read_X11_bitmap, menufile, bitmap1


; Set defaults for keywords

IF n_elements(font_large) eq 0 THEN font_large = ''
IF n_elements(font_small) eq 0 THEN font_small = ''


; Create modal top level base widget

parent = widget_base(column=1, title='ADAS901 : OUTPUT OPTIONS', $
                     xoffset=100, yoffset=1)
base   = widget_base(parent, /column)

; dataset name and browse button

rc = cw_adas_dsbr(base, adas_file, font=font_large)

; widget for graphics selection

grselval = { OUTBUT     :  outval.grpout,   $
             GTIT1      :  outval.gtit1,    $
             SCALBUT    :  outval.grpscal,  $
             XMIN       :  outval.xmin,     $
             XMAX       :  outval.xmax,     $
             YMIN       :  outval.ymin,     $
             YMAX       :  outval.ymax,     $
             HRDOUT     :  outval.hrdout,   $
             HARDNAME   :  outval.hardname, $
             GRPDEF     :  outval.grpdef,   $
             GRPFMESS   :  outval.grpfmess, $
             GRPSEL     :  outval.grpsel,   $
             GRPRMESS   :  outval.grprmess, $
             DEVSEL     :  outval.devsel,   $
             GRSELMESS  :  outval.grselmess }

tbase = widget_base(base,/row,/frame)

graphID = cw_adas_gr_sel(tbase, /SIGN, OUTPUT='Graphical Output', $
                         DEVLIST=devlist, VALUE=grselval, FONT=font_large)

; text file output

outfval = { OUTBUT    :  outval.texout, $
            APPBUT    :  outval.texapp, $
            REPBUT    :  outval.texrep, $
            FILENAME  :  outval.texdsn, $
            DEFNAME   :  outval.texdef, $
            MESSAGE   :  outval.texmes  }

tbase   = widget_base(base, /row, /frame)
paperID = cw_adas_outfile(tbase, OUTPUT='Text Output', VALUE=outfval, FONT=font_large)


; End of panel message and buttons

warn_message = '      '

messID   = widget_label(base, value=warn_message, font=font_large)

mrow     = widget_base(base,/row)
menuID   = widget_button(mrow,value=bitmap1,font=font_large, $
                         event_pro='ADAS901_output_MENU')
cancelID = widget_button(mrow,value='Cancel',font=font_large)
doneID   = widget_button(mrow,value='Done',font=font_large)


; Initial settings


; Realize the ADAS901_OUTPUT input widget.

dynlabel, parent

widget_control, parent, /realize


; Create a pointer to store the information collected from the form.
; The initial data stored here is set to CANCEL, so nothing needs to
; be done if the user kills the widget with the mouse.

ptrToFormData = Ptr_New({cancel:1, menu:0})


; Create an info structure with program information.

info = { graphID         :   graphID,        $
         paperID         :   paperID,        $
         messID          :   messID,         $
         cancelID        :   cancelID,       $
         doneID          :   doneID,         $
         adas_file       :   adas_file,      $
         procval         :   outval,         $
         font            :   font_large,     $
         ptrToFormData   :   ptrToFormData   }


; Store the info structure in the user value of the top-level base and launch
; the widget into the world.

widget_control, parent, Set_UValue=info

xmanager, 'ADAS901_OUTPUT', parent, event_handler='ADAS901_OUTPUT_EVENTS'


; When the widget is destroyed, the block is released, and we
; return here. Get the form data that was collected by the form
; and stored in the pointer location.

formdata = *ptrToFormData


IF N_Elements(formdata) EQ 0 THEN BEGIN
   Ptr_Free, ptrToFormData
   rep ='CANCEL'
ENDIF

IF formdata.cancel EQ 1 THEN BEGIN
   Ptr_Free, ptrToFormData
   rep ='CANCEL'
   RETURN,rep
ENDIF

IF formdata.menu EQ 1 THEN BEGIN
   Ptr_Free, ptrToFormData
   rep ='MENU'
   RETURN,rep
ENDIF

outval.gtit1     = formdata.graph.gtit1
outval.xmin      = formdata.graph.xmin
outval.xmax      = formdata.graph.xmax
outval.ymin      = formdata.graph.ymin
outval.ymax      = formdata.graph.ymax
outval.hardname  = formdata.graph.hardname
outval.hrdout    = formdata.graph.hrdout
outval.texdsn    = formdata.paper.filename

rep = 'CONTINUE'

if formdata.graph.outbut EQ 1 AND formdata.paper.outbut EQ 0 then rep = 'GRAPH'
if formdata.graph.outbut EQ 0 AND formdata.paper.outbut EQ 1 then rep = 'PAPER'
if formdata.graph.outbut EQ 1 AND formdata.paper.outbut EQ 1 then rep = 'BOTH'

RETURN, rep

END
