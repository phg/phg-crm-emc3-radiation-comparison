;+
; PROJECT: ADAS
;
; NAME: ADAS901_outfile
;
; PURPOSE: This procedure writes a record of the plot to a text file.
;
; INPUTS:
;         adas901_outfile, file, content
;
;          content - structure holding the contents of the graph
;          file    - name of output file
;
;
; OPTIONAL INPUTS:
;       None
;
; OUTPUTS:
;       None
;
; OPTIONAL OUTPUTS:
;       None
;
; KEYWORD PARAMETERS:
;       None
;
; CALLS:
;
;       ADAS_WRITEFILE  : routine to write ASCII files
;
; SIDE EFFECTS:
;
; WRITTEN:
;       Martin O'Mullane
;
; MODIFIED:
;       1.1     Martin O'Mullane
;                - First release
;
; VERSION:
;       1.1     18-10-2012
;
;-
;-----------------------------------------------------------------------------


PRO adas901_outfile, file, content

all = ['MOLECULAR COEFFICIENT VERSUS ENERGY/TEMPERATURE :', $
       '', $
       strtrim(content.header, 2), $
       '', $
       'mdf02 file : ' + content.adas_file, $
       'Transition : ' + content.sel_str, $
       '', $
       ' Energy/Te     Coefficient', $
       ' ---------     -----------'  ]

for j = 0, n_elements(content.x)-1 do begin
 
   str = string(content.x[j], format='(f10.2)') + $
         string(content.y[j], format='(e16.3)')
   all = [all, str]
   
endfor

adas_writefile, file=file, all=all

END
