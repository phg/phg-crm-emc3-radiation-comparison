;+
; PROJECT:
;       ADAS
;
; NAME:
;	PLOT502
;
; PURPOSE:
;	Plots one graph for ADAS901.
;
; EXPLANATION:
;	This routine plots ADAS901 output for a single graph either
;       to the screen or a postscript file.
;
; USE:
;	Use is specific to ADAS901.  See adas901_plot.pro for
;	example.
;
; INPUTS:
;	X	- Double array; list of x values.
;	Y	- Double array; y values
;	TITLE	- String array : General title for program run.
;       SEL_STR - string identifying the transition
;       OUTVAL  - structure holding the output options, including
;                 name of file, xmin, xmax, ymin, ymax. 
;
; KEYWORD PARAMETERS:
;	PRINT   - if set send output to a postscript file taking
;                 the name from the outval structure.
;
; CALLS:
;	None.
;
; SIDE EFFECTS:
;	None.
;
; WRITTEN:
;       Martin O'Mullane
;
; MODIFIED:
;       1.1     Martin O'Mullane
;                - First release
;       1.2     Martin O'Mullane
;                - Add x and y axis titles as parameters.
;
; VERSION:
;       1.1     18-10-2012
;       1.2     19-12-2012
;-
;----------------------------------------------------------------------------


PRO plot901, x, y, xtitle, ytitle, sel_str, outval, title, print=print

COMMON global_lw_data, left, right, top, bottom, grtop, grright

if keyword_set(print) then begin

     xsize = 24.0
     ysize = 18.0

     set_plot, 'PS'

     !p.font=0
     device,/color,bits=8,filename=outval.hardname,     $
            /Helvetica, font_size=12, /landscape,       $
            xsize=xsize, ysize=ysize

endif

xmin = min(x, max=xmax)
xmin = 0.9 * xmin
xmax = 1.1 * xmax
ymin = min(y, max=ymax)
ymin = 0.9 * ymin
ymax = 1.1 * ymax

if outval.xmin NE 0.0 AND outval.xmax NE 0.0 then begin
   xmin = outval.xmin
   xmax = outval.xmax
endif

if outval.ymin NE 0.0 AND outval.ymax NE 0.0 then begin
   ymin = outval.ymin
   ymax = outval.ymax
endif


charsize = (!d.y_vsize/!d.y_ch_size)/60.0 

!p.font=-1
small_check = GETENV('VERY_SMALL')

if small_check eq 'YES' then charsize=charsize*0.8

if small_check eq 'YES' then begin
    charsize=charsize*0.8
    gtitle =  title[0] + "!C!C" + title[1] + "!C!C" + title[2] + "!C!C" + $
              title[3] + "!C!C" + title[4] + "!C!C" + title[5]
endif else begin
    gtitle =  title[0] + "!C!C" + title[1] + "!C" + title[2] + "!C" + $
              title[3] + "!C" + title[4]+ "!C" + title[5]
endelse

plot_oo, [xmin,xmax], [ymin,ymax], /nodata, ticklen=1.0, $
         position=[left,bottom,grright,grtop], $
         xtitle=xtitle, ytitle=ytitle, xstyle=1, ystyle=1, $
         charsize=charsize

xyouts, (left-0.05), top, gtitle, /normal, alignment=0.0, $
                  charsize=charsize*0.95

oplot, x, y


if keyword_set(print) then begin

   device,/close
   set_plot,'X'
   !p.font=-1

endif


END
