;+
; PROJECT:
;       ADAS
;
; NAME:
;       ADAS901_IN
;
; PURPOSE: Acquires name of mdf02 file.
;
;
; EXPLANATION:
;
;
;
; NOTES: It entirely in IDL and uses v5 (and above) features such as
;        pointers to pass data.
;
;
;
; INPUTS:
;        inval - A structure holding the 'remembered' input data.
;
; OPTIONAL INPUTS:
;       None
;
; OUTPUTS:
;       The function returns a string rep
;           rep = 'CONTINUE' if everything is ok
;           rep = 'CANCEL' if there is a problem or indeed if the user
;                          closes the widget with the mouse.
;
; OPTIONAL OUTPUTS:
;       None
;
; KEYWORD PARAMETERS:
;       FONT_LARGE - The name of a larger font e.g 'courier_bold14'
;
;       FONT_SMALL - The name of a smaller font e.g 'courier_bold12'
;
;
; CALLS:
;
;       ADAS901_FILE_TEST       : Tests whether a file exists and if so
;                                 increments the allow to browse variable.
;       ADAS901_IN_NULL_EVENTS  : Black hole for events which we don't use.
;       ADAS901_IN_EVENT        : Event handler for all possible actions.
;
; SIDE EFFECTS:
;
; CATEGORY:
;       Widgets
;
; WRITTEN:
;       Martin O'Mullane
;
; MODIFIED:
;       1.1     Martin O'Mullane
;                - First release
;       1.2     Martin O'Mullane
;                - typo in using xxtext routine.
;
; VERSION:
;       1.1     17-10-2012
;       1.2     19-12-2012
;
;-
;-----------------------------------------------------------------------------

PRO ADAS901_FILE_TEST, filename, browse


file_acc, filename, fileexist, read, write, execute, filetype

if filetype EQ '-' AND read EQ 1 then browse = browse + 1

END
;-----------------------------------------------------------------------------

FUNCTION ADAS901_IN_NULL_EVENTS, event

   ; The purpose of this event handler is to do nothing
   ;and ignore all events that come to it.

   RETURN, 0

END

;-----------------------------------------------------------------------------



PRO ADAS901_IN_EVENT, event

; Get the info structure out of the user value of the top-level base.

Widget_Control, event.top, Get_UValue=info



Widget_Control, event.id, Get_Value=userEvent

; ADAS events

CASE userEvent OF

  'Cancel' : begin
               formdata = {cancel:1}
               *info.ptrToFormData =formdata
               widget_Control, event.top, /destroy
             end


  'Done'   : begin

                ; gather the data for return to calling program
                ; 'Done' is not sensitised until valid filenames
                ; have been chosen.

                widget_Control, info.flaID, Get_Value=vala

                err    = 0
                mess   = ''
                browse = 0
                n_sel  = 0

                file = vala.rootpath + vala.file
                adas901_file_test, file, browse

                if browse EQ 0 then begin
                   err  = 1
                   mess = 'File does not exist'
                endif

                if err EQ 0 then begin
                   formdata = {file    : vala,       $
                               cancel  : 0           }
                    *info.ptrToFormData = formdata
                    widget_control, event.top, /destroy
                endif else begin
                   widget_Control, info.messID, Set_Value=mess
                endelse

             end

    'Browse Comments' : Begin

                browse = 0

                Widget_Control, info.flaID, get_value=vala
                file = vala.rootpath + vala.file
                adas901_file_test, file, browse

                if browse EQ 0 then begin
                   mess = 'File does not exist'
                   widget_Control, info.messID, Set_Value=mess
                endif

                xxtext, file, font=info.font_large

             end

  else : message, 'You should not see this message!', /continue

ENDCASE

END

;-----------------------------------------------------------------------------


FUNCTION adas901_in, inval,                   $
                     FONT_LARGE = font_large, $
                     FONT_SMALL = font_small


; Set defaults for keywords

IF n_elements(font_large) eq 0 THEN font_large = ''
IF n_elements(font_small) eq 0 THEN font_small = ''


; Path names may be supplied with or without the trailing '/'.  Add
; this character where required so that USERROOT will always end in
; '/' on output.

if strtrim(inval.rootpath) eq '' then begin
    inval.rootpath = './'
endif else if                                                   $
strmid(inval.rootpath, strlen(inval.rootpath)-1,1) ne '/' then begin
    inval.rootpath = inval.rootpath+'/'
endif





; Modal top level base widget


parent = Widget_Base(Column=1, Title='ADAS901 INPUT', XOFFSET=100, YOFFSET=1)

rc     = widget_label(parent,value='  ', font=font_large)

base   = widget_base(parent, /column)



; ADAS style mdf02 file selection


flabase  = widget_base(base, /column, /frame)
flaval   = {  ROOTPATH   :  inval.rootpath,                $
              FILE       :  inval.file,                    $
              CENTROOT   :  inval.centroot,                $
              USERROOT   :  inval.userroot                 }
flatitle = widget_label(flabase, font=font_large, value='mdf04 file:')
flaID    = cw_adas_infile(flabase, value=flaval, font=font_large, $
                            ysize = 15, event_func='ADAS901_IN_NULL_EVENTS')


; Buttons

mrow     = widget_base(parent,/row,/align_center)
messID   = widget_label(mrow,font=font_large, $
                        value='          Enter File information             ')

mrow     = widget_base(parent,/row)

browseID = widget_button(mrow, value='Browse Comments', font=font_large)
cancelID = widget_button(mrow, value='Cancel',font=font_large)
doneID   = widget_button(mrow, value='Done',font=font_large)



; Initial settings - none! If file does not exist say so when
; attempting to continue or browse.



; Realize the adas901 input widget.

widget_Control, parent, /realize

; Create a pointer to store the information collected from the form.
; The initial data stored here is set to CANCEL, so nothing needs to
; be done if the user kills the widget with the mouse.

ptrToFormData = Ptr_New({cancel:1})

; Create an info structure with program information.

info = { flaID           :  flaID,              $
         doneID          :  doneID,             $
         browseID        :  browseID,           $
         messID          :  messID,             $
         parent          :  parent,             $
         font_large      :  font_large,         $
         ptrToFormData   :  ptrToFormData       }


; Store the info structure in the user value of the top-level base and launch
; the widget into the world.

widget_control, parent, Set_UValue=info

xmanager, 'adas901_in', parent, Event_Handler='ADAS901_IN_EVENT'


; When the widget is destroyed, the block is released, and we
; return here. Get the data that was collected by the widget
; and stored in the pointer location. Finally free the pointer.

formdata = *ptrToFormData

rep='CONTINUE'
IF N_Elements(formdata) EQ 0 THEN BEGIN
   Ptr_Free, ptrToFormData
   rep ='CANCEL'
ENDIF

IF formdata.cancel EQ 1 THEN BEGIN
   Ptr_Free, ptrToFormData
   rep ='CANCEL'
ENDIF

if rep eq 'CONTINUE' then begin
   inval.rootpath   = formdata.file.rootpath
   inval.file       = formdata.file.file
   inval.centroot   = formdata.file.centroot
   inval.userroot   = formdata.file.userroot
   Ptr_Free, ptrToFormData
endif


RETURN, rep

END
