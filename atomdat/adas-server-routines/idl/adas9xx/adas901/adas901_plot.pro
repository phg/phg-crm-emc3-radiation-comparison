;+
; PROJECT: ADAS
;
; NAME: ADAS901_plot
;
; PURPOSE: This procedure asks the user for output options for
;          plotting to screen and writing the output to a text file.
;
; INPUTS:
;         adas901_plot, graph_content, outval,     $
;                       bitfile,                   $
;                       FONT_LARGE = font_large,   $
;                       FONT_SMALL = font_small
;
;          graph_content - structure holding the contents of the graph
;          outval        - structure holding output options
;          bitfile       - location of bitmaps
;
;
; OPTIONAL INPUTS:
;       None
;
; OUTPUTS:
;       The function returns a string rep
;           rep = 'CONTINUE' user is happy with choices made
;           rep = 'MENU'     to leave ADAS901
;           rep = 'CANCEL'   user want to go back a screen or the user
;                            closes the widget with the mouse.
;
; OPTIONAL OUTPUTS:
;       None
;
; KEYWORD PARAMETERS:
;       FONT_LARGE - The name of a larger font e.g 'courier_bold14'
;       FONT_SMALL - The name of a smaller font e.g 'courier_bold12'
;
;
; CALLS:
;
;       ADAS901_PLOT_NULL_EVENTS  : Black hole for events which we don't use.
;       ADAS901_PLOT_MENU         : Event handler for menu (bitmap) actions.
;       ADAS901_PLOT_EVENT        : Event handler for most actions.
;
;
; SIDE EFFECTS:
;
;
; CATEGORY:
;       Widgets
;
; WRITTEN:
;       Martin O'Mullane
;
; MODIFIED:
;       1.1     Martin O'Mullane
;                - First release
;       1.2     Martin O'Mullane
;                - Add x and y axis titles to plot901 call.
;       1.3     Martin O'Mullane
;                - Axis titles were not passed to plot901 for 'print'
;                  option in event handler.
;                - Output a status update after printing plot to file.
;
; VERSION:
;       1.1     18-10-2012
;       1.2     19-12-2012
;       1.3     24-05-2013
;
;-
;-----------------------------------------------------------------------------


FUNCTION ADAS901_plot_NULL_EVENTS, EVENT

   ; The purpose of this event handler is to do nothing
   ; and ignore all events that come to it.

   RETURN, 0

END
;-----------------------------------------------------------------------------



PRO ADAS901_PLOT_MENU, event

; The 'normal' get_value from a widget_button does not work with a
; bitmap button. Hence this specialised event handler.

; Get the info structure out of the user value of the top-level base.

widget_control, event.top, get_uvalue=info


formdata = {cancel : 0, menu:1}
*info.ptrToFormData = formdata
widget_Control, event.top, /destroy

END
;-----------------------------------------------------------------------------


PRO ADAS901_PLOT_EVENTS, event

; Get the info structure out of the user value of the top-level base.

Widget_Control, event.handler, Get_UValue=info

widget_control, info.messID, set_value='               '

; Process events

CASE event.action OF

  'print' : begin
               plot901, info.graph_content.x, info.graph_content.y,          $
                        info.graph_content.xtitle, info.graph_content.ytitle,$ 
                        info.graph_content.sel_str, info.outval,             $
                        info.graph_content.title, /print
               widget_control, info.messID, set_value='Plot sent to file'
            end
            
  'done'  : begin
               formdata = {cancel:1}
               *info.ptrToFormData = formdata
               widget_Control, event.top, /destroy
            end

  else    : message, 'You should not see this message!', /continue

ENDCASE


END
;-----------------------------------------------------------------------------



FUNCTION adas901_plot, graph_content, outval,       $
                       bitfile,                     $
                       FONT_LARGE = font_large,     $
                       FONT_SMALL = font_small

COMMON global_lw_data, left, right, top, bottom, grtop, grright

; Set defaults for keywords

IF n_elements(font_large) eq 0 THEN font_large = ''
IF n_elements(font_small) eq 0 THEN font_small = ''


; Initial values

menufile = bitfile + '/menu.bmp'
read_X11_bitmap, menufile, bitmap1


; Create modal top level base widget

parent = widget_base(column=1, title='ADAS901 : GRAPHICAL OUTPUT', $
                     xoffset=50, yoffset=10)

graphID = cw_adas_graph(parent, print=outval.hrdout, FONT=font_large, bitbutton = bitval)

messID  = widget_label(parent, value='               ', FONT=font_large)

; Realize the ADAS901_PLOT input widget.

dynlabel, parent

widget_control, parent, /realize


; Plot the results and gather data for replotting/printing to a file

widget_control, graphID, get_value=grval
win = grval.win
wset,win

plot901, graph_content.x, graph_content.y, graph_content.xtitle, graph_content.ytitle, $
         graph_content.sel_str, outval, graph_content.title

; Create a pointer to store the information collected from the form.
; The initial data stored here is set to CANCEL, so nothing needs to
; be done if the user kills the widget with the mouse.

ptrToFormData = Ptr_New({cancel:1, menu:0})


; Create an info structure with program information.

info = { graphID         :   graphID,        $
         messID          :   messID,         $
         outval          :   outval,         $
         graph_content   :   graph_content,  $
         font            :   font_large,     $
         ptrToFormData   :   ptrToFormData   }


; Store the info structure in the user value of the top-level base and launch
; the widget into the world.

widget_control, parent, Set_UValue=info

xmanager, 'ADAS901_PLOT', parent, event_handler='ADAS901_PLOT_EVENTS'


; When the widget is destroyed, the block is released, and we
; return here. Get the form data that was collected by the form
; and stored in the pointer location.

formdata = *ptrToFormData

rep = 'CONTINUE'

IF N_Elements(formdata) EQ 0 THEN BEGIN
   Ptr_Free, ptrToFormData
   rep ='CANCEL'
ENDIF

IF formdata.cancel EQ 1 THEN BEGIN
   Ptr_Free, ptrToFormData
   rep ='CANCEL'
   RETURN,rep
ENDIF

IF formdata.menu EQ 1 THEN BEGIN
   Ptr_Free, ptrToFormData
   rep ='MENU'
   RETURN,rep
ENDIF

RETURN, rep

END
