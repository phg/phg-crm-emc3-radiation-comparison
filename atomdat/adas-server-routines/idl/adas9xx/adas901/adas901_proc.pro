;+
; PROJECT: ADAS
;
; NAME: ADAS901_PROC
;
; PURPOSE: Put up a widget panel requesting user selection of quantum
;          numbers of a transiton in an mdf02 file and a set of energies
;          or temperatures over which to plot the transiton data.
;
; NOTES:
;
;
; INPUTS:
;         adas901_proc, adas_file, procval,         $
;                       bitfile,                    $
;                       FONT_LARGE = font_large,    $
;                       FONT_SMALL = font_small
;
;          adas_file  - mdf02 file
;          bitfile    - location of bitmaps
;
;
; OPTIONAL INPUTS:
;       None
;
; OUTPUTS:
;       The function returns a string rep
;           rep = 'CONTINUE' to proceed onwards with ADAS901
;           rep = 'CANCEL'   user want to go back a screen or the user
;                            closes the widget with the mouse.
;           rep = 'MENU'     to leave ADAS901
;           procval = updated structure holding user entered settings.
;
; OPTIONAL OUTPUTS:
;       None
;
; KEYWORD PARAMETERS:
;       FONT_LARGE - The name of a larger font e.g 'courier_bold14'
;       FONT_SMALL - The name of a smaller font e.g 'courier_bold12'
;
;
; CALLS:
;
;       ADAS901_RANGE_STR         : Return a string holding the range of
;                                   quantum numbers possible,
;       ADAS901_PROC_NULL_EVENTS  : Black hole for events which we don't use.
;       ADAS901_PROC_MENU         : Event handler for menu (bitmap) actions.
;       ADAS901_PROC_EVENT        : Event handler for most actions.
;
;
; SIDE EFFECTS:
;
;       Calls routines from idl/read_adf/ and fortran routines from
;       the wrapper series.
;
; CATEGORY:
;       Widgets
;
; WRITTEN:
;       Martin O'Mullane
;
; MODIFIED:
;       1.1     Martin O'Mullane
;                - First release
;       1.2     Martin O'Mullane
;                - Restrict processes drop-down to those in mdf02 file.
;                - Add an alternative transition selection method by
;                  choosing from a list of data in the file.
;                - The transition selected, either by quantum numbers or
;                  transition index, is retained in the defaults.
;                - Check if the minimum energy for plotting is less than
;                  that in mdf02 file and give option to reconsider if so.
;                - Add a 'use mdf02 energy range' as a default when
;                  setting the energy.
;                - Rename 'Check' to 'Details'
;
; VERSION:
;       1.1     18-10-2012
;       1.2     19-12-2012
;
;-
;-----------------------------------------------------------------------------


FUNCTION adas901_range_str, array

; Extract the range of inputs

str     = ''
partype = size(array, /type)

if partype GT 0 AND partype LE 3 then begin

   if n_elements(array) EQ 1 then str = string(array[0]) $
                             else str = string(min(array[1:*])) + ' - ' + string(max(array[1:*]))
   str = strcompress('[ ' + str +  ' ]')

endif


if partype EQ 7 then begin

   ; Isolate integers from characters

   ival = -1
   sval = ''
   for j = 0, n_elements(array)-1 do begin

      if num_chk(array[j], /integer) EQ 0 then ival = [ival, long(array[j])] $
                                          else sval = [sval, strtrim(array[j], 2)]
   endfor

   case n_elements(ival) of
      1    : str_i = 0
      2    : str_i = string(ival[1])
      else : str_i = string(min(ival[1:*])) + ' - ' + string(max(ival[1:*]))
   endcase

   if n_elements(sval) EQ 1 then begin

      str = '[ ' + str_i + ' ]'

   endif else begin

      sval   = adas_uniq(sval)
      n_sval = n_elements(sval)
      sval   = sval + [strarr(n_sval-1) + ', ', '']

      str = '[ ' + str_i
      for j = 0, n_sval-1 do str = str + sval[j]
      str = str + ' ]'

   endelse

   str = strcompress(str)

endif

return, str

END
;---------------------------------------------------------------------------



FUNCTION ADAS901_PROC_NULL_EVENTS, event

   ; The purpose of this event handler is to do nothing
   ; and ignore all events that come to it.

   RETURN, 0

END
;-----------------------------------------------------------------------------



PRO ADAS901_PROC_MENU, event

; The 'normal' get_value from a widget_button does not work with a
; bitmap button. Hence this specialised event handler.

; Get the info structure out of the user value of the top-level base.

widget_control, event.top, get_uvalue=info


formdata = {cancel : 0, menu:1}
*info.ptrToFormData = formdata
widget_Control, event.top, /destroy

END
;-----------------------------------------------------------------------------


PRO ADAS901_PROC_EVENTS, event

; Get the info structure out of the user value of the top-level base.

Widget_Control, event.handler, Get_UValue=info


; Clear any messages

widget_control, info.messID, set_value = ' '


; Process events

CASE event.id OF

  info.cancelid  : begin
                      formdata = {cancel:1}
                      *info.ptrToFormData = formdata
                      widget_Control, event.top, /destroy
                   end

  info.speciesID : begin
                      adas_show_text, group_leader=info.gleader,    $
                                      notify = info.updateID,       $
                                      font = info.font, $
                                      text = info.list_species
                   end

  info.statesID  : begin
                      adas_show_text, group_leader=info.gleader,    $
                                      notify = info.updateID,       $
                                      font = info.font, $
                                      text = info.list_states
                   end

  info.clearID   : begin

                      widget_control, info.energyID, get_value=tval
                      sz = (size(tval.value, /dim))[1]
                      tval.value[0,*] = strarr(sz)
                      widget_control, info.energyID, set_value=tval

                   end

  info.defID   : begin

                      widget_control, info.energyID, get_value=tval
                      sz = (size(tval.value, /dim))[1]

                      sel = event.index

                      case sel of
                         0 : begin
                                widget_control, info.tabID, get_uvalue=itab

                                if itab EQ 0 then begin

                                   widget_control, info.processID, get_value=pr_all
                                   res = widget_info(info.processID, /combobox_gettext)
                                   i1 = where(strpos(pr_all, res) NE -1)
                                   reads, pr_all[i1], pr_index, format='(i2)'

                                   seek    = ''
                                   quantum = strarr(6)
                                   for j = 0, 5 do begin
                                      widget_control, info.selID[j], get_value=sc
                                      if j EQ 2 OR j EQ 5 then quantum[j] = string(strtrim(sc,2), format='(a3)') $
                                                          else quantum[j] = string(sc, format='(i2)')
                                      seek = seek + quantum[j]
                                   endfor
                                   seek = seek + string(pr_index, format='(i3)')

                                endif else begin

                                   widget_control, info.indexID, get_value=i1
                                   seek = info.index[i1]

                                endelse

                                ind = where(strpos(info.index, seek) NE -1, count)

                                if count GT 0 then begin

                                   i1    = ind[0]
                                   i2    = info.list_numx[i1]
                                   eng   = info.list_energy[i1, 0:i2-1]

                                endif

                                values = reform(eng)
                                sz     = n_elements(values)

                             end
                      
                         1 : values = adas_vector(low=1.0, high=100.0, num=sz)
                         2 : values = adas_vector(low=1.0, high=10.0, num=sz)
                      
                      endcase

                      tval.value[0,0:sz-1] = string(values, format=info.num_form)
                      widget_control, info.energyID, set_value=tval

                   end

  info.tabID     : begin
                       itab = event.tab
                       widget_control, info.tabID, set_uvalue=itab
                   end

  info.indexID   :  ; do nothing

  info.checkID   : begin

                      widget_control, info.tabID, get_uvalue=itab

                      if itab EQ 0 then begin

                         widget_control, info.processID, get_value=pr_all
                         res = widget_info(info.processID, /combobox_gettext)
                         i1 = where(strpos(pr_all, res) NE -1)
                         reads, pr_all[i1], pr_index, format='(i2)'

                         seek    = ''
                         quantum = strarr(6)
                         for j = 0, 5 do begin
                            widget_control, info.selID[j], get_value=sc
                            if j EQ 2 OR j EQ 5 then quantum[j] = string(strtrim(sc,2), format='(a3)') $
                                                else quantum[j] = string(sc, format='(i2)')
                            seek = seek + quantum[j]
                         endfor
                         seek = seek + string(pr_index, format='(i3)')

                      endif else begin

                         widget_control, info.indexID, get_value=i1
                         seek = info.index[i1]

                      endelse

                      ind = where(strpos(info.index, seek) NE -1, count)

                      if count EQ 0 then begin

                         mess = 'A transition for this set is not in mdf02 file'
                         widget_control, info.messID, set_value=mess

                      endif else begin

                         str = 'Formula : ' + string(info.list_formula[ind[0]], format='(i3)')
                         widget_control, info.formulaID, set_value=str

                         case info.list_categ[ind[0]] of
                            1 : begin
                                   str_x = 'x-type  : energy (eV/amu)'
                                   str_y = 'y-type  : collision strength'
                                end
                            2 : begin
                                   str_x = 'x-type  : energy (eV/amu)'
                                   str_y = 'y-type  : cross section (cm2)'
                                end
                            3 : begin
                                   str_x = 'x-type  : temperature (eV)'
                                   str_y = 'y-type  : effective collision strength'
                                end
                            4 : begin
                                   str_x = 'x-type  : temperature (eV)'
                                   str_y = 'y-type  : Maxwell averaged rate coefficient (cm3/s)'
                                end
                         endcase
                         widget_control, info.xID, set_value=str_x
                         widget_control, info.yID, set_value=str_y

                         i1    = ind[0]
                         i2    = info.list_numx[i1]
                         eng   = info.list_energy[i1, 0:i2-1]
                         str_r = 'Range   : ' + string(min(eng), format='(e8.2)') + ' - ' + string(max(eng), format='(e8.2)')
                         widget_control, info.rangeID, set_value=str_r

                      endelse

                   end

  info.doneID    : begin

                      err  = 0
                      mess = ' '

                      widget_control, info.runID, get_value=title

                      widget_control, info.tabID, get_uvalue=itab


                      widget_control, info.processID, get_value=pr_all
                      res = widget_info(info.processID, /combobox_gettext)
                      i1 = where(strpos(pr_all, res) NE -1)
                      reads, pr_all[i1], pr_index, format='(i2)'

                      seek    = ''
                      quantum = strarr(6)
                      for j = 0, 5 do begin
                         widget_control, info.selID[j], get_value=sc
                         if j EQ 2 OR j EQ 5 then quantum[j] = string(strtrim(sc,2), format='(a3)') $
                                             else quantum[j] = string(sc, format='(i2)')
                         seek = seek + quantum[j]
                      endfor
                      seek = seek + string(pr_index, format='(i3)')

                      widget_control, info.indexID, get_value=i1
                      select_index = i1

                      if itab EQ 1 then seek = info.index[i1]

                      ind = where(strpos(info.index, seek) NE -1, count)
                      if count EQ 0 then begin
                         mess = 'A transition for this set is not in mdf02 file'
                         err = 1
                      endif

                      i1      = ind[0]
                      i2      = info.list_numx[i1]
                      e02_min = min(info.list_energy[i1, 0:i2-1], max=e02_max)

                      if err EQ 0 then begin

                         widget_control, info.energyID, get_value=tempval

                         ind  = where(tempval.value NE '', neng)

                         if neng GT 0 then begin

                           energy = fltarr(neng)

                           for j = 0, neng-1 do begin
                              reads, tempval.value[0,j], tmp
                              energy[j] = tmp
                           endfor

                         endif else begin

                            err = 1
                            mess = 'At least one Energy/Temperature must be set'

                         endelse

                         ; if range is outside mdf02 minimun energy then re-consider

                         if min(energy) LT e02_min then begin

                            res = popup(message = ['There are energies below the minimium value', $
                                                   'in the mdf02 file.', $
                                                   '',$
                                                   'This may give an invalid extrapolation'], $
                                        title   = 'Numerical warning', $
                                        font    = info.font, $
                                        buttons = ['OK', 'Change energy'])
                            help,res

                         endif

                         if res EQ 'Change energy' then begin

                            err  = 1
                            mess = 'Change energy before continuing'

                         endif

                      endif


                      if err EQ 0 then begin
                         formdata = {title        : title[0],      $
                                     itab         : itab,          $
                                     pr_index     : pr_index[0],   $
                                     quantum      : quantum,       $
                                     select_index : select_index,  $
                                     energy       : energy,        $
                                     menu         : 0,             $
                                     cancel       : 0              }
                          *info.ptrToFormData = formdata
                          widget_control, event.top, /destroy
                      endif else begin
                         widget_control, info.messID, set_value=mess
                      endelse

                   end

  else : message, 'You should not see this message!', /continue

ENDCASE


END
;-----------------------------------------------------------------------------



FUNCTION adas901_proc, adas_file, procval,       $
                       bitfile,                  $
                       FONT_LARGE = font_large,  $
                       FONT_SMALL = font_small

; Initial values

menufile = bitfile + '/menu.bmp'
read_X11_bitmap, menufile, bitmap1


; Set defaults for keywords

IF n_elements(font_large) eq 0 THEN font_large = ''
IF n_elements(font_small) eq 0 THEN font_small = ''


; Read processes from the mdf02 file
;
; Set up indexing, ranges and initial conditions


xxdatm_02, file=adas_file, fulldata=all_02

index = string(all_02.parinfo.s_in, format='(i2)') +             $
        string(all_02.parinfo.e_in, format='(i2)') +             $
        string(strtrim(all_02.parinfo.v_in,2), format='(a3)') +  $
        string(all_02.parinfo.s_out, format='(i2)') +            $
        string(all_02.parinfo.e_out, format='(i2)') +            $
        string(strtrim(all_02.parinfo.v_out,2), format='(a3)') + $
        string(all_02.parinfo.process_index, format='(i3)')

processes = string(all_02.index_p, format='(i2)') + ' : ' + $
            all_02.desc_p[all_02.index_p-1] + '  [' + $
            all_02.path_p[all_02.index_p-1]

str_processes  = strtrim(processes) + ']'

; limit widget to processes which have data in the mdf02 file

i1  = adas_uniq(all_02.parinfo.process_index)
ind = i4indf(all_02.index_p, i1)
str_processes = str_processes[ind]

list_p_index   = all_02.index_p

list_formula   = all_02.parinfo.formula
list_categ     = all_02.parinfo.categ
list_energy    = all_02.tparam
list_numx      = all_02.parinfo.num_x

max_x  = max(all_02.parinfo.num_x, ind)
if max_x GT 0 then energy = all_02.tparam[ind,0:max_x-1]  $
              else energy = -1.0

num_species = all_02.ind_s
max_state   = max(all_02.in_e)
num_states  = max(all_02.ind_e)
num_tran    = all_02.ind_r

list_species = ['Species', $
                '-------', $
                'Index  Type  Ionis. channel  Dissoc. channel']
res = size(all_02.ch_dis, /dim)
n_s = res[0]
n_d = res[1]
for is = 0, n_s-1 do begin
   for id = 0, n_d-1 do begin
     if strlen(strcompress(all_02.ch_dis[is,id], /remove_all)) GT 0 then begin
        str = string(is+1, format='(i4)') + '   ' + all_02.symbs[is] + '  ' +$
              all_02.ch_ion[is] + '         ' + all_02.ch_dis[is,id]
        list_species = [list_species, str]
     endif
  endfor
endfor


list_states = ['States', $
               '-------', $
               '     Index             Electronic config limit      Molecular state', $
               'Species  State             united   separated']
res = size(all_02.wno, /dim)
n_s = res[0]
n_e = res[1]
for is = 0, n_s-1 do begin
   for ie = 0, n_e-1 do begin
     if all_02.in_e[is,ie] GT 0 then begin
        str = string(is+1, format='(i7)') + '     ' +                     $
              string(all_02.in_e[is,ie], format='(i2)') + '           ' + $
              all_02.econua[is,ie] + '   ' +                              $
              all_02.econsa[is,ie] + '            ' +                     $
              all_02.coupsta[is,ie]
        list_states = [list_states, str]
     endif
  endfor
endfor


ranges  = [adas901_range_str(all_02.parinfo.s_in),  $
           adas901_range_str(all_02.parinfo.e_in),  $
           adas901_range_str(all_02.parinfo.v_in),  $
           adas901_range_str(all_02.parinfo.s_out), $
           adas901_range_str(all_02.parinfo.e_out), $
           adas901_range_str(all_02.parinfo.v_out)  ]



; Create structure for processing screen if it is empty

if (procval.new lt 0) then begin

   energy  = adas_vector(low=1, high=100, num=20)
   procval = { new      : 0 ,                             $
               title    : '',                             $
               itab     : 0L,                             $
               s_in     : all_02.parinfo.s_in[0],         $
               e_in     : all_02.parinfo.e_in[0],         $
               v_in     : all_02.parinfo.v_in[0],         $
               s_out    : all_02.parinfo.s_out[0],        $
               e_out    : all_02.parinfo.e_out[0],        $
               v_out    : all_02.parinfo.v_out[0],        $
               p_index  : all_02.parinfo.process_index[0],$
               index    : 0L,                             $
               trans    : 'NULL',                         $
               max_X    : max_x,                          $
               energy   : energy                          }

endif


; Create modal top level base widget

parent = widget_base(column=1, title='ADAS901 : PROCESSING OPTIONS', $
                     xoffset=100, yoffset=1)
base   = widget_base(parent, /column)

; Title for run

trow  = widget_base(base, /row)
rc    = widget_label(trow, value='Title for Run: ', font=font_large)
runID = widget_text(trow, value=procval.title, xsize=38, font=font_large, /edit)

gap   = widget_label(base, value=' ', font=font_large)


; Info on file contents - species, states, no. transitions and processes available

str_n_species = 'Number of species           : ' + string(num_species, format='(i3)') + '   '
str_n_states  = 'Number of electronic states : ' + string(num_states, format='(i3)') + '   '
str_n_tran    = 'Number of transitions       : ' + string(num_tran, format='(i3)')

trow      = widget_base(base, /row)
rc        = widget_label(trow, value=str_n_species, font=font_large)
speciesID = widget_button(trow, value='Browse Species', font=font_large)

trow      = widget_base(base, /row)
rc        = widget_label(trow, value=str_n_states, font=font_large)
statesID  = widget_button(trow, value='Browse States', font=font_large)

trow      = widget_base(base, /row)
rc        = widget_label(trow, value=str_n_tran, font=font_large)

gap       = widget_label(base, value=' ', font=font_large)

trow      = widget_base(base, /row)
rc        = widget_label(trow, value='Choose Process              : ', font=font_large)
processID = widget_combobox(trow, value=str_processes,         $
                            event_func='ADAS_PROC_NULL_EVENTS',  $
                            font=font_small )

gap   = widget_label(base, value=' ', font=font_large)


; User enetered choice of energy/temperature and transition (via quantum numbers)

erow  = widget_base(base, /row)

; select transition

tlist = 0L

tcol    = widget_base(erow, /column)
trlab   = widget_label(tcol,font=font_large, value='Select Transition:')
tabID   = widget_tab(tcol, /align_top, font=font_large, uvalue=tlist)

; by quantum number

tybase  = widget_base(tabID, /column, /frame, title='By quantum number')

selID   = lonarr(6)

names  = ['Incoming channel - species     : ', $
          '                   elec. state : ', $
          '                   vib. level  : ', $
          'Outgoing channel - species     : ', $
          '                   elec. state : ', $
          '                   vib. level  : '  ]

for j = 0, 5 do begin

   isint = 1
   if j EQ 2 OR j EQ 5 then isint = 0

   trow      = widget_base(tybase, /row)
   selID[j]  = cw_field(trow, title=names[j], $
                        integer=isint, fieldfont=font_small, font=font_small)
   range     = widget_label(trow, value=ranges[j] + '  ', font=font_large)

endfor


trlab     = widget_label(tcol,font=font_large, value=' ')
tcol      = widget_base(tcol, /column, /frame)
formulaID = widget_label(tcol,font=font_large, value='Formula : ', /align_left)
xID       = widget_label(tcol,font=font_large, value='x-type  : ', /align_left)
yID       = widget_label(tcol,font=font_large, value='y-type  : ', /align_left)
rangeID   = widget_label(tcol,font=font_large, value='Range   : ', /align_left)
trow      = widget_base(tcol, /row)
checkID   = widget_button(trow,value='Details',font=font_large, /align_right)


; by scrolling through list of transitions

trbase      = widget_base(tabID, /column, /frame, title='By transition')

coltitles   = 'Process:  incoming channel - outgoing channel'

select_data = string(all_02.parinfo.process_index, format='(i3)') + '    : ' +    $
              string(all_02.parinfo.s_in, format='(i2)') +  '   ' +               $
              string(all_02.parinfo.e_in, format='(i2)') +  '   ' +               $
              string(strtrim(all_02.parinfo.v_in,2), format='(a3)') + '   -   ' + $
              string(all_02.parinfo.s_out, format='(i2)') + '   ' +               $
              string(all_02.parinfo.e_out, format='(i2)') + '   ' +               $
              string(strtrim(all_02.parinfo.v_out,2), format='(a3)') + ' '

select_index = -1

indexID      = cw_single_sel(trbase, select_data, value=select_index,      $
                          title='', coltitles = coltitles,                 $
                          ysize = 10, font=font_small, big_font=large_font )



gap = widget_label(erow, value='  ', font=font_large)


; select temperature/energies

tcol       = widget_base(erow, /column)
trlab      = widget_label(tcol,font=font_large, value='Select Temperatures/Energies:')
tybase     = widget_base(tcol, /column, /frame)

num_form   = '(E10.3)'
energy     = procval.energy
ind        = where(energy NE 0.0, nenergy)
nenergy    = n_elements(energy)
tabledata  = strarr(1,20)
if nenergy GT 0 then tabledata[0,0:nenergy-1] = strtrim(string(energy[0:nenergy-1],format=num_form),2)


table_title = ' '
table_head  = ['  Te/Energy (eV)    ']

energyID  = cw_adas_table(tybase, tabledata, coledit=[1],               $
                          limits=[2], title=table_title, font_large=font_small,    $
                          font_small=font_small, colhead=table_head,               $
                          ytexsize=15, num_form=num_form, order=[1],               $
                          fonts = edit_fonts,                                      $
                          event_func='ADAS_PROC_NULL_EVENTS')


def_str = ['mdf02 range', '1 - 100 eV ','1 - 10 eV']
defID   = widget_droplist(tybase,value = def_str,font=font_large, $
                          title='Default:');, event_pro = 'ADAS901_PROC_EVENTS')

clearID = widget_button(tybase, font=font_small, value=' Clear Table ')


; End of panel message and buttons

warn_message = '      '

messID   = widget_label(base, value=warn_message, font=font_large)

mrow     = widget_base(base,/row)
menuID   = widget_button(mrow,value=bitmap1,font=font_large, $
                         event_pro='ADAS901_PROC_MENU')
cancelID = widget_button(mrow,value='Cancel',font=font_large)
doneID   = widget_button(mrow,value='Done',font=font_large)



; Initial settings

if procval.new EQ 1 then begin

   if procval.itab EQ 0 then begin
      ind = where(long(strmid(str_processes,0,2)) EQ long(procval.p_index), count)
      if count GT 0 then widget_control, processID, set_combobox_select=ind
   endif else begin
      widget_control, indexID, set_value=procval.index
   endelse

   widget_control, tabID, set_tab_current=procval.itab
   widget_control, tabID, set_uvalue=procval.itab

   widget_control, selID[0], set_value=procval.s_in
   widget_control, selID[1], set_value=procval.e_in
   widget_control, selID[2], set_value=procval.v_in
   widget_control, selID[3], set_value=procval.s_out
   widget_control, selID[4], set_value=procval.e_out
   widget_control, selID[5], set_value=procval.v_out

endif


; Realize the ADAS901_PROC input widget.


dynlabel, parent

widget_Control, parent, /realize


; Create a pointer to store the information collected from the form.
; The initial data stored here is set to CANCEL, so nothing needs to
; be done if the user kills the widget with the mouse.

ptrToFormData = Ptr_New({cancel:1, menu:0})

; Create an info structure with program information.

info = { runID             :    runID,             $
         speciesID         :    speciesID,         $
         statesID          :    statesID,          $
         processID         :    processID,         $
         tabID             :    tabID,             $
         selID             :    selID,             $
         indexID           :    indexID,           $
         formulaID         :    formulaID,         $
         rangeID           :    rangeID,           $
         xID               :    xID,               $
         yID               :    yID,               $
         checkID           :    checkID,           $
         energyID          :    energyID,          $
         defID             :    defID ,            $
         clearID           :    clearID,           $
         messID            :    messID,            $
         cancelID          :    cancelID,          $
         doneID            :    doneID,            $
         gleader           :    parent,            $
         list_species      :    list_species,      $
         list_states       :    list_states,       $
         list_formula      :    list_formula,      $
         list_categ        :    list_categ,        $
         list_numx         :    list_numx,         $
         list_energy       :    list_energy,       $
         list_p_index      :    list_p_index,      $
         index             :    index,             $
         selection         :    -1,                $
         select_index      :    -1,                $
         num_form          :    num_form,          $
         updateID          :    parent,            $
         font              :    font_large,        $
         ptrToFormData     :    ptrToFormData      }


; Store the info structure in the user value of the top-level base and launch
; the widget into the world.

widget_control, parent, Set_UValue=info

XManager, 'ADAS901_PROC', parent, Event_Handler='ADAS901_PROC_EVENTS'


; When the widget is destroyed, the block is released, and we
; return here. Get the form data that was collected by the form
; and stored in the pointer location.

formdata = *ptrToFormData

rep = 'CONTINUE'

IF N_Elements(formdata) EQ 0 THEN BEGIN
   Ptr_Free, ptrToFormData
   rep ='CANCEL'
ENDIF

IF formdata.cancel EQ 1 THEN BEGIN
   Ptr_Free, ptrToFormData
   rep ='CANCEL'
   RETURN,rep
ENDIF

IF formdata.menu EQ 1 THEN BEGIN
   Ptr_Free, ptrToFormData
   rep ='MENU'
   RETURN,rep
ENDIF

if formdata.select_index GE 0 then tr_str = index[formdata.select_index] $
                              else tr_str = 'NULL'

procval = { new      : 1 ,                          $
            title    : formdata.title,              $
            itab     : long(formdata.itab),         $
            p_index  : formdata.pr_index,           $
            s_in     : long(formdata.quantum[0]),   $
            e_in     : long(formdata.quantum[1]),   $
            v_in     : formdata.quantum[2],         $
            s_out    : long(formdata.quantum[3]),   $
            e_out    : long(formdata.quantum[4]),   $
            v_out    : formdata.quantum[5],         $
            index    : long(formdata.select_index), $
            trans    : tr_str,                      $
            max_X    : -1.0,                        $
            energy   : formdata.energy              }


RETURN, rep

END
