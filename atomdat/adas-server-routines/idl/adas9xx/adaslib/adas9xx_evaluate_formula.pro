;----------------------------------------------------------------------
;+
; PROJECT    :  ADAS
;
; NAME       :  adas9xx_evaluate_formula
;
; PURPOSE    :  Evaluates the fitting formulae in the series 9 codes
;               and the mdf set of data.
;
; ARGUMENTS  :
;
;               NAME      I/O    TYPE       DETAILS
; REQUIRED   :  formula    I     int        number of formula
;
;
; KEYWORDS      help       I     Display help entry
;
;
; The list of formulae are:
;
;               1 :  vibrational excitation via H2- resonant states (Janev 4.2)
;             201 :  rate coefficient for e-impact vibrational excitation (Janev 5.25)
;
;
; NOTES      :  Calls fortran code.
;
;
; AUTHOR     :  Martin O'Mullane
;
; DATE       :  13/10/12
;
;
; MODIFIED:
;       1.1     Martin O'Mullane
;              	- First version.
;
;
; VERSION:
;       1.1    13-10-2012
;
;-
;----------------------------------------------------------------------

PRO adas9xx_evaluate_formula, formula = formula,  $
                              sigma   = sigma,    $
                              rate    = rate,     $
                              delta_e = delta_e,  $
                              weight  = weight,   $
                              param   = param,    $
                              energy  = energy,   $
                              result  = result,   $
                              help    = help
 ; If asked for help

if keyword_set(help) then begin
   doc_library, 'adas9xx_evaluate_formula'
   return
endif

; Some checks

valid_formula = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14,   $
                 101, 102, 103, 104, 105, 106, 201, 301, 302, 303 ]

res = where(valid_formula EQ formula, count)
if count EQ 0 then message, 'Invalid formula, ' + string(formula)

; Options

if formula EQ 201 then begin

   if keyword_set(sigma) then message, '/sigma is invalid for this formula'
   if keyword_set(rate) then ioutput = 1L else ioutput = 0L

endif else begin

   if keyword_set(rate) then message, '/rate is invalid for this formula'
   if keyword_set(sigma) then ioutput = 1L else ioutput = 0L

endelse

if formula EQ 0 then begin

   message, 'Not a valid formula ' + string(formula), /continue

   result = dblarr(n_elements(energy))

   return

endif

; Variables to pass to fortran

NENER = 200L
NPAR  = 20L

numform = long(formula)
ipar    = n_elements(param)
iener   = n_elements(energy)
de      = double(delta_e)
w       = double(weight)

par = dblarr(NPAR)
par[0:ipar-1] = param

ele = dblarr(NENER)
ele[0:iener-1] = energy

fform = dblarr(NENER)


; Location of compiled routine

fortdir = getenv('ADASFORT')

dummy = CALL_EXTERNAL(fortdir+'/ixeform_if.so','ixeform_if',    $
                      numform, ioutput, ipar   , iener ,        $
                      de     , w      , par    , ele   ,  fform )

result = fform[0:iener-1]

END
