; Copyright (c) 1999, Strathclyde University.
; SCCS info: Module @(#)$Header: /home/adascvs/idl/adas4xx/adas403/adas403_out.pro,v 1.3 2005/04/13 08:36:18 allan Exp $ Date $Date: 2005/04/13 08:36:18 $
;+
; PROJECT:
;       ADAS 
;
; NAME:
;	ADAS403_OUT
;
; PURPOSE: 
;       This function does the actual work. Initially it puts up a widget 
;       asking for the output directory/directories of the 'new' adf10
;       files. There are 3 choices - send all updated adf10 files to a
;       passing subdirectory, replace the old adf10 with the updated ones
;       or write to a different set of adf10 files. An option to write
;       a summary paper.txt is also offered.
;
;
; EXPLANATION:
;       The selection of adf10 directory structure output files is
;       similar to that of ADAS403_IN. However the root is forced to
;       'userroot+/adas/adf10/' and the sequence is taken from the adf10
;       input file. As in ADAS403_IN a template of the adf10
;       isoelectronic files, with XXX replacing the data class (acd,
;       plt etc.) is dynamically constructed and displayed. 
;       The output file destination is checked for permission to 
;       write and, importantly, whether it is a directory.
;
;
; NOTES:
;       The comments in the adf10 files are handled a little clumsily and
;       this is where the program is most likely to break. The assumption
;       is that the comments are in z1 order, and that they are seperated
;       by a line of '----' strating with a '-'- in the FIRST column. All
;       the comments in the adf10 file are read into a string array with
;       an index array holding the start position of cmment z1(i).
;       See the update procedures for an explanation of the algorithm
;       for positioning the supplementary data into the new adf10 file. 
; 
;
;
; INPUTS:
;        outval       - A structure holding the 'remembered' outout options.
;                       appbut      : append to end of paper.txt
;                       repbut      : replace paper.txt
;                       filename    : name of paper.txt
;                       defname     : 'paper.txt' - the default!
;                       message     : error message for paper.txt
;                       out_type    : 0,1,2 - pass directory, replace adf10
;                                             or new adf10 files.
;                       out_pass    : adf10 passing directory
;                       out_defpass : userroot+'/pass/' - default
;                       out_root    : root of new adf10
;                       out_year    : year of new adf10
;                       out_prefix  : prefix of new adf10
;                       out_seq     : sequence of new adf10
; 
;        Note : out_root, out_seq and out_appbut are set afresh for
;               each run. 
;
;        dataclasses  - A string array of the possible dataclasses
;                       'acd','scd','ccd','prb','prc','qcd','xcd',
;                       'plt','pls' and 'met'
;
;        header       - Output header; version, date etc.
;
;        match_report - string array from ADAS403_PROC reporting on
;                       files found for writing to paper.txt.
;         
;        bitfile      - directory of bitmaps for menu button
;
;
; OPTIONAL INPUTS:
;       None
;
; OUTPUTS:
;       The function returns a string rep
;           rep = 'CONTINUE' if everything is ok
;           rep = 'CANCEL' if there is a problem or indeed if the user
;                          closes the widget with the mouse. 
;           rep = 'MENU' if user want to exit to menu at this point
;
; OPTIONAL OUTPUTS:
;       None
;
; KEYWORD PARAMETERS:
;       FONT_LARGE - The name of a larger font e.g 'courier_bold14'
;
;       FONT_SMALL - The name of a smaller font e.g 'courier_bold12'
;
;
; CALLS:
;                            
;       ADAS403_OUT_ENTRY        : Reacts to events relevant to constructing
;                                  adf10 filenames.
;       ADAS403_OUT_NULL_EVENTS  : Captures events and does nothing (function)
;       ADAS403_OUT_NULLS        : Captures events and does nothing (procedure)
;       ADAS403_OUT_SWITCH       : Switches between 3 possible output options
;                                  which are mapped/unmapped here.
;       ADAS403_OUT_MENU         : React to menu button event. The standard IDL
;                                  reaction to button events cannot deal with
;                                  pixmapped buttons. Hence the special handler.
;       ADAS403_OUT_EVENT        : Reacts to cancel and Done. Does the file
;                                  existence error checking.
;       ADAS403_UPDATE_TYPE1     : Updates adf10 for datacalsses 0-6.
;       ADAS403_UPDATE_TYPE2     : Updates adf10 for datacalsses 7-9.
;
; SIDE EFFECTS:
;
; CATEGORY:
;	Widgets
;
; WRITTEN:
;       Martin O'Mullane
;
; MODIFIED:
;	1.1	Martin O'Mullane
;		First release 
;	1.2	Richard Martin
;		Changed widget name switch to switchb for IDL 5.4 compatibility. 
;	1.3	Allan Whiteford
;               Changed rstrpos to strpos with /reverse_search.
;
; VERSION:
;       1.1	16-08-99
;	1.2	10-11-00
;	1.3	13-04-05
;
;-
;-----------------------------------------------------------------------------

PRO ADAS403_OUT_ENTRY, event

; This event handler captures all text and button input relevant to 
; constructing a valid adf10 filename. The only events which come to 
; handler are button and text entries.

  Widget_Control, event.top, Get_UValue=info

; Remove instruction - we are not using dynamic resize so make blanks
; as large as any possible error string.

  Widget_Control, info.messID, Set_Value='                                     '


; recover strings for template identifer
   
   str_1 = info.basestr(0)
   str_2 = info.basestr(1)
   str_3 = info.basestr(2)
   str_4 = info.basestr(3)


; Handle text insertion events to year, prefix and sequence

   IF event.type EQ 0 THEN BEGIN

      Widget_Control, event.id, Get_Value=text
      text   = text(0)
      length = StrLen(text)

      ; only react if there are less than 2 characters there already
      ; and ignore RETURN (13B)

      if length LT 2 AND byte(event.ch) NE 13B then begin 
         selection = Widget_Info(event.id, /Text_Select)
         Widget_Control, event.id, /Use_Text_Select, Set_Value=String(event.ch)
         Widget_Control, event.id, Set_Text_Select=event.offset + 1
      endif

   ENDIF 

; Must also deal with deletions

   IF event.type EQ 2 THEN BEGIN

      Widget_Control, event.id, Get_Value=text
      text = text(0)
      length = StrLen(text)

      Widget_Control, event.id, Set_Value=StrMid(text, 0, length-event.length)
      Widget_Control, event.id, Set_Text_Select=event.offset

   ENDIF

; now change the template dynamically
       
   Widget_Control, event.id, Get_value=text
   Widget_Control, event.id, get_uvalue=part

   CASE part OF

     'year'   : begin 
                   str_2 = 'XXX'+text(0)
                   info.basestr(1) = str_2
                   loc = strpos(info.basestr(2),'XXX')
                   strput,str_3,text(0),loc+3
                end
     'prefix' : begin
                   if strlen(text(0)) GE 1  $
                      then str_3 = '/'+text(0)+'#XXX'+StrMid(info.basestr(1),3,2) $
                      else str_3 = '/XXX'+StrMid(info.basestr(1),3,2) 
                   info.basestr(2) = str_3
                end

      ELSE : print,'ADAS403_OUT_ENTRY - should not be seen',part

   ENDCASE
   Widget_Control, info.tempID, Set_value=str_1+str_2+str_3+str_4
   

   
   
; now fill in the adf10 template  
   
   Widget_Control, info.tempID, Set_value=str_1+str_2+str_3+str_4

   Widget_Control, event.top, Set_Uvalue=info

END 

;----------------------------------------------------------------------------

FUNCTION ADAS403_OUT_NULL_EVENTS, event

   ; The purpose of this event handler is to do nothing
   ;and ignore all events that come to it.
   
   ; need a function for cw_bgroup and a procedure for widget_test!!!! WHY???
   
   RETURN,0

END 

;-----------------------------------------------------------------------------

PRO ADAS403_OUT_NULLS, event

   ; The purpose of this event handler is to do nothing
   ;and ignore all events that come to it.
   
END 

;-----------------------------------------------------------------------------

FUNCTION ADAS403_OUT_SWITCH, event

   ; React to switching of the output options - it's a function because 
   ; cw_bgroup can only deal with these and not procedures.
   
  Widget_Control, event.top, Get_UValue=info
  Widget_Control, info.choiceID, Get_Value=choice
  
  
  CASE choice OF
    
    0 : begin 
          widget_control, info.passBaseID, map=1
          widget_control, info.replBaseID, map=0
          widget_control, info.newBaseID, map=0
        end
  
    1 : begin 
          widget_control, info.passBaseID, map=0
          widget_control, info.replBaseID, map=1
          widget_control, info.newBaseID, map=0
        end
        
    2 : begin 
          widget_control, info.passBaseID, map=0
          widget_control, info.replBaseID, map=0
          widget_control, info.newBaseID, map=1
        end
        
    else : print,'ADAS403_OUT_SWITCH: you should not see this ',choice
  
  ENDCASE  
 
  RETURN,0
  
END 

;-----------------------------------------------------------------------------

PRO ADAS403_OUT_MENU, event

; The 'normal' get_value from a widget_button does not work with a
; bitmap button. Hence this specialised event handler.

   ; Get the info structure out of the user value of the top-level base.

Widget_Control, event.top, Get_UValue=info


; Something nefarious may cause an error also.
;  -- if so trap it and exit gracefully

CATCH, error
IF error NE 0 THEN BEGIN
   formdata = {cancel : 0, menu:1}
   *info.ptrToFormData = formdata
   Widget_Control, event.top, /Destroy
   message,!err_string
   RETURN
ENDIF


 formdata = {cancel : 0, menu:1}
 *info.ptrToFormData =formdata
 widget_Control, event.top, /destroy

END


;-----------------------------------------------------------------------------


PRO ADAS403_OUT_EVENT, event

; React to button events - Cancel and Done but not menu (this requires a
; specialised event handler ADAS403_OUT_MENU). Also deal with the passing
; directory output Default button here.

; On pressing Done check for the following
;  - paper.txt is valid, handled by cw_adas_outfile
;  - if passing directory is blank ASSUME that they are to be sent
;    to the current directory.
;  - replace existing adf10 must be confirmed.
;  - new adf10 checks that directories exist.



   ; Get the info structure out of the user value of the top-level base.

Widget_Control, event.top, Get_UValue=info

; Something nefarious may cause an error also.
;  -- if so trap it and exit gracefully

CATCH, error
IF error NE 0 THEN BEGIN
   formdata = {cancel:1, menu : 0}
   *info.ptrToFormData = formdata
   Widget_Control, event.top, /Destroy
   print,!err_string
   RETURN
ENDIF

Widget_Control, event.id, Get_Value=userEvent

CASE userEvent OF

  'Default' : begin
                widget_control,info.passID, set_value=info.defpass
              end 

  'Cancel' : begin
               formdata = {cancel:1, menu : 0}
               *info.ptrToFormData =formdata
               widget_Control, event.top, /destroy
             end 

      
  'Done'   : begin
                 
                ; gather the data for return to calling program
                
                err  = 0
                mess = ''
                
                widget_Control, info.paperID, Get_Value=fval
                
                ; no need to set mess as it is flagged in the cw
                if fval.message NE ' ' then err  = 1
                
                widget_Control, info.choiceID,  Get_Value=choice
                widget_Control, info.passID,    Get_Value=pass
                widget_Control, info.replID,    Get_Value=repl
                widget_Control, info.yearID,    Get_Value=year
                widget_Control, info.prefixID,  Get_Value=prefix
                widget_Control, info.newreplID, Get_Value=newrepl
                widget_Control, info.tempID,    Get_Value=template
               
               
                ; have files been selected and can we write to them?
                
                CASE choice OF

                  0 : begin
                        adas_dircheck, pass, error, message
                        if error NE 0 then begin
                           err = 1
                           mess = message
                         endif
                      end
                      
                  1 : if repl EQ 0 then begin
                         err  = 1
                         mess = 'You must confirm this action'
                      endif
                      
                  2 : begin
                        for j=0, n_elements(info.valid_types)-1 do begin
                           if err EQ 0 then begin
                              
                              template = template[0]
                              p1 = strpos(template,'XXX')     
                              p2 = strpos(template,'/',/reverse_search)
                              outdir = template[0]
                              strput,outdir,info.valid_types[j],p1
                              outdir = strmid(outdir,0,p2+1)
                              
                              adas_dircheck, outdir, error, message
                              if error NE 0 then begin
                                 err = 1
                                 mess = info.valid_types[j] + ' - ' + message
                               endif
                            endif
                        endfor
                        if newrepl EQ 0 then begin
                          err = 1
                          mess = 'You must confirm replacement'
                        endif
                      end
                      
                  ELSE : 
                  
                ENDCASE
                      
                           
                if err EQ 0 then begin
                   formdata = { outbut   : fval.outbut,   $
                                appbut   : fval.appbut,   $
                                repbut   : fval.repbut,   $
                                filename : fval.filename, $
                                defname  : fval.defname,  $
                                message  : fval.message,  $
                                choice   : choice,        $
                                pass     : pass,          $
                                year     : year,          $
                                prefix   : prefix,        $
                                cancel   : 0,             $
                                menu     : 0              }
                   *info.ptrToFormData = formdata
                   widget_control, event.top, /destroy
                endif else begin
                   widget_Control, info.messID, Set_Value=mess
                endelse

             end
             
             
  else : print,'ADAS403_OUT_EVENT : You should not see this message! ',userEvent
                   
ENDCASE

END
  
;-----------------------------------------------------------------------------

PRO ADAS403_UPDATE_TYPE1, infile, suppfile, loc, outfile

; Updates/replaces in the adf10 isoelectronic file the partial gcr coefficient
; from the ADAS208 partial files - 
; ie adf10 types acd, scd, ccd, prb, prc, qcd and xcd

 
  str      = ''
  str1     = ''
  num      = -1
  idum     = 0
  nte      = 0
  ndens    = 0
  supp_z1  = 0.0
  supp_str = ''
  supp_com = ' '


; First the supplement file
    
  openr,lun,suppfile,/get_lun
  
  while (not EOF(lun)) do begin
    readf,lun,str1
    if (STRPOS(str1,'SSYSWT')) NE -1 then begin
       num = num + 1
       if num EQ loc then begin
          readf,lun,str
          readf,lun,idum,nte,ndens
          readf,lun,supp_z1
          for i=0,1 do readf,lun,str
          
          te        = DBLARR(nte)
          dens      = DBLARR(ndens)
          supp_data = DBLARR(ndens,nte)
          
          readf,lun,te
          readf,lun,dens
          readf,lun,supp_str
          readf,lun,supp_data
          
          readf,lun,str
          test = -1
          while (test NE 0 AND (not EOF(lun)) ) do begin
             readf,lun,str
             supp_com = [supp_com,str]
             test = STRPOS(str,'-',0)           ; -1 keep, 0 end of comments
          end 
          icom     = n_elements(supp_com)
          supp_com = supp_com[0:icom-1]  

       endif
    endif
  end   
   
  free_lun, lun



; The adf10 isoelectronic file
    
  str        = ''
  str1       = ''
  nz         = 0
  nte        = 0
  ndens      = 0
  npot       = 0
  adf10_com  = ''
  adf10_head = ''

    
  openr,lun,infile,/get_lun
  
  while (not EOF(lun)) do begin
    readf,lun,str1
    adf10_head = [adf10_head,str1]
    if (STRPOS(str1,'SSYSWT')) NE -1 then begin
       
       readf,lun,str
       adf10_head = [adf10_head,str]
       
       readf,lun,nz,nte,ndens,npot
       
       adf10_z1 = fltarr(nz)
       readf,lun,adf10_z1
       
       readf,lun,str
       if npot NE 0 then begin
          adf10_pot = fltarr(npot)
          readf,lun,adf10_pot
       endif
       readf,lun,str
          

       te         = DBLARR(nte)
       dens       = DBLARR(ndens)
       adf10_data = DBLARR(ndens,nte,nz)
       tmp        = DBLARR(ndens,nte)
       adf10_str  = STRARR(nz)
       
       readf,lun,te
       readf,lun,dens
       
       for j = 0, nz - 1 do begin
         readf,lun,str
         readf,lun,tmp
         adf10_str(j) = str
         adf10_data(*,*,j) = tmp
       endfor

       ; Assume the comments are in z1 order and are separated by '----'
       ; store in adf10_com everything except the seperators and index
       ; the start of each comment block in adf10_com_ind.
       ;
       ; This is a reasonably dangerous assumption but as long as the
       ; files are created automatically we will be ok.
       
       adf10_com_ind = intarr(nz+1) ; pointer to start of comment z1
       
       adf10_com_ind(0) = 1
       icom = 1
       j = 0
       adf10_com = ' '
       readf,lun,str ; seperator
       while (not EOF(lun))  do begin
          readf,lun,str
          test = STRPOS(str,'-',0)           ; 0 end of comments for this z
          if test EQ 0 then begin
             adf10_com_ind(icom) = j
             icom = icom + 1
          endif else begin
             adf10_com = [adf10_com,str]
             j = j+1
          endelse
       end   

    endif
  end   
   
  free_lun, lun




; Write out the new adf10 file - This is where the real work of 
; ADAS403 is done. 

  seperator = '----------------------------------------' + $
              '----------------------------------------'
              

; adf10_head has [' ', '===' etc.] We must start at element 1 not 0. 
; Not sure why but it works. 
              

   ; Make out_vec and out_typ for z1 and source of data, either supp or 
   ; adf10 file. The algorithm should work for insertion or replacement.
   ; If data comes from adf10 then type = 0 
   ; From supp file type is 1
   ; We also need to track the index of the data in the adf10 file. Put
   ; the sorted index into loc_adf10.
   ;
   ; eg if typ_z_out = 0 1 0 ie replace the second Z with supp data
   ;    then loc_adf10 = 0 1 2 so in the loop data block 1 is skipped.
   ;    if typ_z_out = 1 0 0 0 ie a new block at the beginning
   ;    then loc_adf10 = -1 0 1 2 and no blocks are skipped.
   
     ipt_adf10 = indgen( n_elements(adf10_z1))
     typ_a = intarr(n_elements(adf10_z1))
     typ_s = 1

     rep = where(adf10_z1 EQ supp_z1, count)
     if count EQ 0 then begin
        out_vec   = [adf10_z1,supp_z1] 
        out_typ   = [typ_a,typ_s]
        ipt_adf10 = [ipt_adf10,-1]
     endif else begin
        out_vec = adf10_z1
        out_typ = typ_a
        out_typ[rep(0)] = 1
     endelse


     tag=sort(out_vec)
     nz_out    = out_vec(tag)
     typ_z_out = out_typ(tag)
     loc_adf10 = ipt_adf10(tag)
          
          
   openw,lun,outfile,/get_lun

   for j=1, n_elements(adf10_head)-1 do printf, lun, adf10_head(j)
   printf,lun,n_elements(nz_out),nte,ndens,npot, format='(4I5)'
   printf,lun,nz_out, format='(10F10.5)'
   printf,lun,seperator
   if npot NE 0 then printf,lun, adf10_pot, format= '(6F12.1,:)'
   printf,lun,seperator

   printf,lun,te,format='(8E10.2,:)'
   printf,lun,dens,format='(8E10.2,:)'


;  The data

   for j = 0, n_elements(nz_out) - 1 do begin
     if typ_z_out[j] EQ 1 then begin
        printf,lun,supp_str
        printf,lun,supp_data,format='(8E10.2,:)'
     endif else begin
        idata = loc_adf10(j)
        printf,lun,adf10_str(idata)
        printf,lun,adf10_data(*,*,idata),format='(8E10.2,:)'
     endelse
   endfor


;  The comments
 
   printf,lun,seperator
 
   
   for j = 0, n_elements(nz_out) - 1 do begin
     if typ_z_out[j] EQ 1 then begin
        for k = 1, n_elements(supp_com)-2 do printf,lun,supp_com(k)
        printf,lun,seperator
     endif else begin
        idata = loc_adf10(j)
        for k = adf10_com_ind[idata]+1,adf10_com_ind[idata+1]  do begin
          printf,lun,adf10_com(k)
        endfor
        printf,lun,seperator
     endelse
   endfor
   
   
   free_lun, lun
 
 
END 

;-----------------------------------------------------------------------------

PRO ADAS403_UPDATE_TYPE2, infile, suppfile, loc, outfile

; Updates/replaces in the adf10 isoelectronic file the partial gcr coefficient
; from the ADAS208 partial files - 
; ie adf10 types pls, plt and met

 
  str      = ''
  str1     = ''
  nte      = 0
  ndens    = 0
  nmet     = 0
  supp_z1  = 0.0
  supp_str = ''
  supp_com = ' '


; First the supplement file
    
  openr,lun,suppfile,/get_lun
  
  while (not EOF(lun)) do begin
    readf,lun,str1
    if (STRPOS(str1,'NMET')) NE -1 then begin
       reads,str1,nmet,format='(14x,I2)'
    endif
    if (STRPOS(str1,'SPNSYS')) NE -1 then begin
       readf,lun,str
       readf,lun,idum,nte,ndens
       readf,lun,supp_z1
       for i=0,1 do readf,lun,str

       te        = DBLARR(nte)
       dens      = DBLARR(ndens)
       tmp       = DBLARR(ndens,nte)
       supp_data = DBLARR(ndens,nte,nmet)

       readf,lun,te
       readf,lun,dens
       readf,lun,supp_str
       
       for j = 0, nmet-1 do begin
         readf,lun,str
         readf,lun,tmp
         supp_data(*,*,j) = tmp
       endfor

       readf,lun,str
       test = -1
       while (test NE 0 AND (not EOF(lun)) ) do begin
          readf,lun,str
          supp_com = [supp_com,str]
          test = STRPOS(str,'-',0)           ; -1 keep, 0 end of comments
       end 
       icom     = n_elements(supp_com)
       supp_com = supp_com[0:icom-1]  

    endif
  end   
   
  free_lun, lun



; The adf10 isoelectronic file
    
  str        = ''
  str1       = ''
  nz         = 0
  nte        = 0
  ndens      = 0
  npot       = 0
  adf10_com  = ''
  adf10_head = ''

    
  openr,lun,infile,/get_lun
  
  while (not EOF(lun)) do begin
    readf,lun,str1
    adf10_head = [adf10_head,str1]
    if (STRPOS(str1,'NMET')) NE -1 then begin
       reads,str1,nmet,format='(14x,I2)'
    endif
    if (STRPOS(str1,'SPNSYS')) NE -1 then begin
       
       readf,lun,str
       adf10_head = [adf10_head,str]
       
       readf,lun,nz,nte,ndens,npot
       
       adf10_z1 = fltarr(nz)
       readf,lun,adf10_z1
       
       readf,lun,str
       if npot NE 0 then begin
          adf10_pot = fltarr(npot)
          readf,lun,adf10_pot
       endif
       readf,lun,str
          

       te         = DBLARR(nte)
       dens       = DBLARR(ndens)
       adf10_data = DBLARR(ndens,nte,nmet,nz)
       tmp        = DBLARR(ndens,nte)
       adf10_str  = STRARR(nz)
       
       readf,lun,te
       readf,lun,dens
       
       for j = 0, nz - 1 do begin
         readf,lun,str
         adf10_str(j) = str
         for k =0, nmet-1 do begin
            readf,lun,str
            readf,lun,tmp
            adf10_data(*,*,k,j) = tmp
         endfor
       endfor

       ; Assume the comments are in z1 order and are separated by '----'
       ; store in adf10_com everything except the seperators and index
       ; the start of each comment block in adf10_com_ind.
       ;
       ; This is a reasonably dangerous assumption but as long as the
       ; files are created automatically we will be ok.
       
       adf10_com_ind = intarr(nz+1) ; pointer to start of comment z1
       
       adf10_com_ind(0) = 1
       icom = 1
       j = 0
       adf10_com = ' '
       readf,lun,str ; seperator
       while (not EOF(lun))  do begin
          readf,lun,str
          test = STRPOS(str,'-',0)           ; 0 end of comments for this z
          if test EQ 0 then begin
             adf10_com_ind(icom) = j
             icom = icom + 1
          endif else begin
             adf10_com = [adf10_com,str]
             j = j+1
          endelse
       end   

    endif
  end   
   
  free_lun, lun




; Write out the new adf10 file - This is where the real work of 
; ADAS403 is done. 

  seperator = '----------------------------------------' + $
              '----------------------------------------'
              

; adf10_head has [' ', '===' etc.] We must start at element 1 not 0. 
; Not sure why but it works. 
              

   ; Make out_vec and out_typ for z1 and source of data, either supp or 
   ; adf10 file. The algorithm should work for insertion or replacement.
   ; If data comes from adf10 then type = 0 
   ; From supp file type is 1
   
     ipt_adf10 = indgen( n_elements(adf10_z1))
     typ_a = intarr(n_elements(adf10_z1))
     typ_s = 1

     rep = where(adf10_z1 EQ supp_z1, count)
     if count EQ 0 then begin
        out_vec = [adf10_z1,supp_z1] 
        out_typ = [typ_a,typ_s]
        ipt_adf10 = [ipt_adf10,-1]
     endif else begin
        out_vec = adf10_z1
        out_typ = typ_a
        out_typ[rep(0)] = 1
     endelse


     tag=sort(out_vec)
     nz_out    = out_vec(tag)
     typ_z_out = out_typ(tag)
     loc_adf10 = ipt_adf10(tag)

  
   
          
   openw,lun,outfile,/get_lun

   for j=1, n_elements(adf10_head)-1 do printf, lun, adf10_head(j)
   printf,lun,n_elements(nz_out),nte,ndens,npot, format='(4I5)'
   printf,lun,nz_out, format='(10F10.5)'
   printf,lun,seperator
   if npot NE 0 then printf,lun, adf10_pot, format= '(6F12.1,:)'
   printf,lun,seperator

   printf,lun,te,format='(8E10.2,:)'
   printf,lun,dens,format='(8E10.2,:)'


;  The data

   ia = 0
   for j = 0, n_elements(nz_out) - 1 do begin
     if typ_z_out[j] EQ 1 then begin
        printf,lun,supp_str
        for k =0, nmet-1 do begin
          str = '/'+string(k+1,format='(I1)')+'/'
          printf,lun,str
          printf,lun,supp_data(*,*,k),format='(8E10.2,:)'
        endfor
     endif else begin
        idata = loc_adf10(j)
        printf,lun,adf10_str(idata)
        for k =0, nmet-1 do begin
          str = '/'+string(k+1,format='(I1)')+'/'
          printf,lun,str
          printf,lun,adf10_data(*,*,k,idata),format='(8E10.2,:)'
        endfor
     endelse
   endfor


;  The comments
 
   printf,lun,seperator
 
   ia = 0
   
   for j = 0, n_elements(nz_out) - 1 do begin
     if typ_z_out[j] EQ 1 then begin
        for k = 1, n_elements(supp_com)-2 do printf,lun,supp_com(k)
        printf,lun,seperator
     endif else begin
        idata = loc_adf10(j)
        for k = adf10_com_ind[idata]+1,adf10_com_ind[idata+1]  do begin
          printf,lun,adf10_com(k)
        endfor
        printf,lun,seperator
     endelse
   endfor
   
   
   free_lun, lun
 
 
END 

;-----------------------------------------------------------------------------


FUNCTION ADAS403_OUT, procval, outval, dataclasses,                   $ 
                      header, match_report, bitfile,                  $
                      FONT_LARGE = font_large, FONT_SMALL = font_small


; Set defaults for keywords and extract info for paper.txt question

  IF n_elements(font_large) eq 0 THEN font_large = ''
  IF n_elements(font_small) eq 0 THEN font_small = ''


  paperval =  { outbut   : outval.outbut,    $
                appbut   : outval.appbut,    $
                repbut   : outval.repbut,    $
                filename : outval.filename,  $
                defname  : outval.defname,   $
                message  : outval.message    }


; Make a template name from the input - a reversal of before

  basestr    = strarr(4)
  basestr(0) = outval.out_root
  if outval.out_year EQ '' then basestr(1) = '' else basestr(1) = 'XXX'+outval.out_year
  if outval.out_prefix EQ '' then begin
     if outval.out_year EQ '' then basestr(2) = '' else basestr(2) = '/XXX'+outval.out_year
  endif else begin
     basestr(2) = '/'+outval.out_prefix+'#XXX'+outval.out_year
  endelse
  if outval.out_seq EQ '' then basestr(3) = '' else basestr(3) = '_'+outval.out_seq+'XX.dat'
  template = basestr(0)+basestr(1)+basestr(2)+basestr(3)
 

; Determine which dataclasses are to be written - need to test whether the
; subdirectories exist. 

  classes     = where(procval.type NE '')
  errors      = where(procval.error_mess NE '')
  sd          = setdifference(classes,errors)
  if sd(0) NE -1 then valid_types = dataclasses(sd) else $
                      valid_types = dataclasses(classes)




                ;********************************************
		;**** create modal top level base widget ****
                ;********************************************
                
  parent = Widget_Base(Column=1, Title='ADAS 403 OUTPUT', $
                       XOFFSET=100, YOFFSET=1)

                
  rc = widget_label(parent,value='  ',font=font_large)

                 
  
                ;**********************************
		;**** Ask for output files -   ****
                ;**** paper.txt and new adf10  ****
                ;**********************************

  base  = widget_base(parent, /column)
  
  mrow    = widget_base(base,/frame)
  paperID = cw_adas_outfile(mrow, OUTPUT='Text Output',   $
                                 VALUE=paperval, FONT=font_large)
  
  mlab  = widget_label(parent,value='  ',font=font_large)
  mrow  = widget_base(parent, /row)
 
  choices  = ['Passing Files', 'Replace Originals', 'New adf10 set']
  choiceID = cw_bgroup(mrow,choices,  $
                             exclusive=1,row=1,                  $
                             label_top='Location of new adf10 files:',  $
                             event_func='ADAS403_OUT_SWITCH',$
                             /no_release,  font=font_large)
 
 
                ;**** Base for output choice ****
                            
  switchb     = widget_base(parent)
  passBaseID = widget_base(switchb,/column)
  replBaseID = widget_base(switchb,/row)
  newBaseID  = widget_base(switchb,/column)
                            
  
  ; send all 'new' adf10 files to a passing directory                          
                            
  slab   = widget_label(passBaseID,value = 'Location of passing directory: ',  $
                        font=font_large)
  mrow  = widget_base(passBaseID,/row) 
  passID =  widget_text(mrow, value=outval.out_pass, font=font_large,   $
                            xsize=45,/editable,event_pro='ADAS403_OUT_NULLS')
  
  defpassID =  widget_button(mrow, value='Default', font=font_large)
   
                    
                            
  ; or replace existing adf10 files                          
                            
  question=['Replace anyway']
  replID = cw_bgroup(replBaseID,question,  $
                     nonexclusive=1,row=1,                  $
                     label_left='This may be dangerous:',   $
                     event_func='ADAS403_OUT_NULL_EVENTS',  $
                     /no_release,  font=font_large)
                            
  
  
   
  ; or put them in a new adf10 set in the users space 
  
  mrow  = widget_base(newBaseID,/row) 
  slab  = widget_label(mrow, value = 'New adf10 isoelectronic files:- '+  $
                       '     Year: ',  font=font_large)
  yearID   =  widget_text(mrow, value=outval.out_year, font=font_large,      $
                            xsize=3,event_pro='ADAS403_OUT_ENTRY',           $
                            all_events=1, editable=0,                        $
                            uvalue='year')
                            
  slab     = widget_label(mrow, value = '   Prefix: ', font=font_large)
  prefixID =  widget_text(mrow, value=outval.out_prefix, font=font_large,    $
                            xsize=3,event_pro='ADAS403_OUT_ENTRY',           $
                            all_events=1, editable=0,                        $
                            uvalue='prefix')
                            
  mrow     = widget_base(newBaseID,/row) 
  tempID   =  widget_text(mrow, value=template, font=font_small,   $
                            xsize=50,/editable,event_pro='ADAS403_OUT_NULLS')
  
  question=['Replace if exists']
  newreplID = cw_bgroup(mrow,question,  $
                     nonexclusive=1,row=1,                  $
                     event_func='ADAS403_OUT_NULL_EVENTS',$
                     /no_release,  font=font_large)
                            
                            
                            
                            
                ;************************************
		;**** Error/Instruction message. ****
                ;************************************
                
  messID = widget_label(parent,value='     Choose output options   ',font=font_large)
                            
                ;*****************
		;**** Buttons ****
                ;*****************
                
  menufile = bitfile + '/menu.bmp'
  read_X11_bitmap, menufile, bitmap1
                
  mrow     = widget_base(parent,/row)
  menuID   = widget_button(mrow,value=bitmap1,font=font_large, $
                           event_pro='ADAS403_OUT_MENU')
  cancelID = widget_button(mrow,value='Cancel',font=font_large)
  doneID   = widget_button(mrow,value='Done',font=font_large)



; Initial settings 

  widget_control, choiceID, set_value = outval.out_type
  
  CASE outval.out_type OF
    
    0 : begin 
          widget_control, passBaseID, map=1
          widget_control, replBaseID, map=0
          widget_control, newBaseID, map=0
        end
  
    1 : begin 
          widget_control, passBaseID, map=0
          widget_control, replBaseID, map=1
          widget_control, newBaseID, map=0
        end
        
    2 : begin 
          widget_control, passBaseID, map=0
          widget_control, replBaseID, map=0
          widget_control, newBaseID, map=1
        end
                
  ENDCASE


                ;***************************
		;**** Put up the widget ****
                ;***************************

; Realize the ADAS403_OUT input widget.

   dynlabel, parent
   widget_Control, parent, /realize

; Create a pointer to store the information collected from the form.
; The initial data stored here is set to CANCEL, so nothing needs to
; be done if the user kills the widget with the mouse.

   ptrToFormData = Ptr_New({cancel:1, menu:0})

; Create an info structure with program information.

  info = { paperID         :  paperID,            $
           choiceID        :  choiceID,           $
           passBaseID      :  passBaseID,         $
           replBaseID      :  replBaseID,         $
           newBaseID       :  newBaseID,          $
           passID          :  passID,             $
           defpassID       :  defpassID,          $
           replID          :  replID,             $
           yearID          :  yearID,             $
           prefixID        :  prefixID,           $
           newreplID       :  newreplID,          $
           tempID          :  tempID,             $
           messID          :  messID,             $
           basestr         :  basestr,            $        
           defpass         :  outval.out_defpass, $        
           valid_types     :  valid_types,        $        
           valid_root      :  outval.out_root,    $        
           ptrToFormData   :  ptrToFormData       }  
            
               
; Store the info structure in the user value of the top-level base and launch
; the widget into the world.

  widget_control, parent, Set_UValue=info

  XManager, 'ADAS403_OUT', parent, Event_Handler='ADAS403_OUT_EVENT'


; When the widget is destroyed, the block is released, and we
; return here. Get the form data that was collected by the form
; and stored in the pointer location.

formdata = *ptrToFormData


rep='CONTINUE'
IF N_Elements(formdata) EQ 0 THEN BEGIN
   Ptr_Free, ptrToFormData
   rep ='CANCEL'
ENDIF
 
IF formdata.cancel EQ 1 THEN BEGIN
   Ptr_Free, ptrToFormData
   rep ='CANCEL'
   RETURN,rep
ENDIF

IF formdata.menu EQ 1 THEN BEGIN
   Ptr_Free, ptrToFormData
   rep ='MENU
   RETURN,rep
ENDIF

if rep eq 'CONTINUE' then begin
   
   outval.out_type   =   formdata.choice
   outval.outbut     =   formdata.outbut
   outval.appbut     =   formdata.appbut
   outval.repbut     =   formdata.repbut
   outval.filename   =   formdata.filename
   outval.defname    =   formdata.defname
   outval.message    =   formdata.message
   outval.out_pass   =   formdata.pass
   outval.out_year   =   formdata.year
   outval.out_prefix =   formdata.prefix

   ; Free the pointer.
   Ptr_Free, ptrToFormData

endif       


                ;******************************************
		;**** Output new files if all is well. ****
                ;******************************************




; Do the work of copying the partial ADAS208 gcr files into the user
; selected output adf10 files.

; During writing of files put up a busy widget

  widget_control, /hourglass
  busybase = widget_base(/column,xoffset=300,yoffset=200,     $
                           title = "ADAS403 INFORMATION")
  lab0 = widget_label(busybase, value='')
  lab1 = widget_label(busybase, value='')
  lab2 = widget_label(busybase, font=font_large,              $
                      value='       Writing files - please wait       ')
  lab3 = widget_label(busybase, value='')
  lab4 = widget_label(busybase, value='')
  widget_control, busybase, /realize



  outfile_list = ' '
  
  for j = 0, n_elements(classes) - 1 do begin
     
     iclass = classes[j]

     if procval[iclass].error_mess EQ '' then begin
     
        for i = 0, procval[iclass].num_adf10-1 do begin

          ; which partial in gcr file
          
          ind_a = 10*procval[iclass].adf10_ind1(i) + procval[iclass].adf10_ind2(i)
          ind_s = 10*procval[iclass].supp_ind1 + procval[iclass].supp_ind2

          loc = where(ind_s EQ ind_a)
          loc = loc[0]

          CASE outval.out_type OF

             0 : begin
                    str1 = outval.out_pass
                    sloc = strpos(procval[iclass].adf10[i],'/',/reverse_search)
                    str2 = strmid(procval[iclass].adf10[i],sloc+1)
                    outfile = str1 + str2
                 end
             1 : outfile = procval[iclass].adf10[i]
             2 : begin
                    str1 = outval.out_root
                    if outval.out_year EQ '' then str2 = '' else $
                       str2 = procval[iclass].type + outval.out_year
                    if outval.out_prefix EQ '' then begin
                       if outval.out_year EQ '' then str3 = '' else $
                          str3 = '/' + procval[iclass].type + outval.out_year
                    endif else begin
                       str3 = '/'+outval.out_prefix+'#'+ procval[iclass].type + $
                               outval.out_year
                    endelse
                    str4 = '_'+outval.out_seq
                    if iclass LT 7 then str5 = string(ind_a,format='(I2)') else $
                                        str5 = '##'
                    outfile = str1+str2+str3+str4+str5+'.dat'
                 end
             ELSE : print,'ADAS403_OUT - outfile there should be nothing else'

          ENDCASE

          
          if iclass LT 7 then begin
             adas403_update_type1, procval[iclass].adf10[i],procval[iclass].supp,$ 
                                   loc, outfile
          endif else begin
             adas403_update_type2, procval[iclass].adf10[i],procval[iclass].supp,$ 
                                   loc, outfile
          endelse
        
          outfile_list = [outfile_list, outfile]
        
        endfor
        
     endif
  
  endfor


; Write the paper.txt file - if selected
   
  if outval.outbut EQ 1 then begin
                 
     if outval.appbut NE 1 then begin
        openw,lun,outval.filename,/get_lun
        outval.appbut = 0
     endif else begin
        openw,lun,outval.filename,/get_lun,/append
     endelse
        
     printf, lun, header
     printf, lun, ' '

     for j = 0,n_elements(match_report)-1 do begin
        printf, lun, match_report(j)
     endfor

     printf, lun, ' '
     printf, lun, ' '
     printf, lun, 'Write to output files:'
     printf, lun, ' '
     
     for j = 1, n_elements(outfile_list) - 1 do begin
        printf, lun, outfile_list[j]
     endfor
     
     free_lun,lun 
    
  endif              
    
     

  
    
; We have finished writing the data so destroy the busy widget

  widget_control, busybase, /destroy

; And tell ADAS403 that we are finished

RETURN,rep

END
