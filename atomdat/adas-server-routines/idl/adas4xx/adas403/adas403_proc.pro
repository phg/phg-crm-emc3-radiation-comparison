; Copyright (c) 1999, Strathclyde University.
;+
; PROJECT:
;       ADAS 
;
; NAME:
;	ADAS403_PROC
;
; PURPOSE: 
;       This function is probably misnamed in that it does not do any
;       user driven processing. It is more of a data checker and 
;       summarizer for ADAS403_IN. The input adf10 isoelectronic files 
;       and the ADAS208 partial gcr files are matched and some selective
;       checking is done. A report, in reality a string array, is produced
;       which can subsequently be written to paper.txt. Errors here may 
;       prevent the program from continuing. The source of the error is
;       popped up in a widget and the user has the option of continuing
;       or cancelling. Of course if there is no match the program only
;       allows the user to cancel. If continuing a text widget of the
;       report is presented for examination by the user.
;
;
; EXPLANATION:
;       A comprehensive matching of the partial and adf10 files is
;       not possible. In particular it is not possible to determine
;       the sequence from the ADAS208 gcr files. The number of enteries
;       in the gcr file should nmatch the number of adf10 files for
;       that class. Likewise the IPRT/IGRD/JPRT/JGRD labels should
;       match. This information, along with the Z in both, is written to
;       the report.     
;
; NOTES: 
; 
;
;
; INPUTS:
;        inval       - A structure holding the 'remembered' input data.
;                      ROOTPATHIN  : current selected adf10 root
;                      CENTROOTIN  : central ADAS adf10 location
;                      USERROOTIN  : user's ADAS adf10 location 
;                      SUPPROOTIN  : current location of ADAS208 files 
;                      YEAR        : year of adf10 
;                      PREFIX      : prefix of adf10     
;                      SEQ         : sequence of adf10           
;                      CLASS       : indices of selected classes
;        dataclasses - A string array of the possible dataclasses
;                      'acd','scd','ccd','prb','prc','qcd','xcd',
;                      'plt','pls' and 'met'
;                     
; OPTIONAL INPUTS:
;       None
;
; OUTPUTS:
;       An array of structures containing useful information for the 
;       output step is produced. 10 structures, corresponding to the
;       dataclasses are written. Some of these may well be empty but
;       the overhead of carrying them about is not great. Each 
;       structure is defined:
;        
;       partial ::  type       : dataclass (string)
;                   error_mess : error message from checking ('' if none)
;                   num_adf10  : number of adf10 files for the dataclass
;                   adf10      : string array of adf10 file names 
;                                (fully qualified)
;                   num_supp   : number of enteries in gcr file
;                                (should equal num_adf10)
;                   supp       : name of gcr file
;                   adf10_ind1 : integer array of first index (eg IPRT)
;                                (dimensioned to ndparts=12)
;                   adf10_ind2 : integer array of second index
;                   adf10_iz1  : array of z1 enteries in adf10 file
;                   supp_ind1  : integer array of first index
;                                (dimensioned to ndparts=12)
;                   supp_ind2  : integer array of second index
;                   supp_iz1   : z1 of supplementary gcr file
;
;       The function returns a string rep
;           rep = 'CONTINUE' if everything is ok
;           rep = 'CANCEL' if there is a problem or indeed if the user
;                          closes the widget with the mouse. 
;
; OPTIONAL OUTPUTS:
;       None
;
; KEYWORD PARAMETERS:
;       FONT_LARGE - The name of a larger font e.g 'courier_bold14'
;
;       FONT_SMALL - The name of a smaller font e.g 'courier_bold12'
;
;
; CALLS:
;                            
;       ADAS403_FILE_TYPE1  :  Gathers information from adf10 and gcr
;                              files for dataclasses acd, scd, ccd, prb, 
;                              prc, qcd and xcd 
;       ADAS403_FILE_TYPE1  :  Gathers information from adf10 and gcr
;                              files for dataclasses pls, plt and met
;
; SIDE EFFECTS:
;
; CATEGORY:
;	Widgets
;
; WRITTEN:
;       Martin O'Mullane
;
; MODIFIED:
;	1.1	Martin O'Mullane
;		 - First release 
;	1.2	Martin O'Mullane
;		 - Add a /sh to spawn command for Sun platforms.
;	1.3	Allan Whiteford
;		 - Changed rstrpos to strpos with /reverse_search.
;
; VERSION:
;       1.1	16-08-1999
;       1.2	05-02-2003
;       1.3	13-04-2005
;
;-
;-----------------------------------------------------------------------------

PRO ADAS403_FILE_TYPE1, value, match_report

; Probes the adf10 and ADAS208 gcr files for partial cases - 
; ie adf10 types acd, scd, ccd, prb, prc, qcd and xcd

  
  str  = ' '
  num  = 0
  ind1 = 0
  ind2 = 0
  iz1  = 0

; get IRRT/IGRD/JPRT/JGRD string here
  
  str_s1 = ''
  str_s2 = ''
  str_f1 = ''
  str_f2 = ''

; First the supplement file
    
  openr,lun,value.supp,/get_lun
  
  while (not EOF(lun)) do begin
    readf,lun,str
    if (STRPOS(str,'--/ I')) NE -1 then begin
       num = num + 1
       reads,str,str_s1,ind1,str_s2,ind2,iz1, format='(32x,a4,1x,i2,4x,a4,1x,i2,7x,i2)'
       value.supp_ind1[num-1] = ind1
       value.supp_ind2[num-1] = ind2
       value.supp_iz1  = iz1
    endif
  end   
   
  free_lun, lun
  value.num_supp = num


; Next the set of partial adf10 isoelectronic files - need to make some 
; assumptions here. The adf10 files are consistent in that they both
; have the same number of iz1 entries, temperatures etc.

  for j = 0, value.num_adf10-1 do begin
     
     nz = 0
     openr,lun,value.adf10[j],/get_lun
 
     while (not EOF(lun)) do begin
       readf,lun,str
       if (STRPOS(str,'--/ I')) NE -1 then begin
          reads,str,str_f1,ind1,str_f2,ind2,iz1, format='(32x,a4,1x,i2,4x,a4,1x,i2,7x,i2)'
          value.adf10_ind1[j] = ind1
          value.adf10_ind2[j] = ind2
          value.adf10_iz1[nz] = iz1 
          nz = nz + 1
       endif
     end   
     free_lun, lun
     
  endfor



; Extend the report for this dataclass
        
  match_report = [match_report,'adf10 isoelectronic files:',' ']
  
  
  for k = 0, value.num_adf10-1 do begin
    i_str = string(k+1,format='(1x,i2)')+': '
    match_report = [match_report,i_str+value.adf10(k)]
  endfor
  
  z_str = 'z1 = '
  loc_z = where(value.adf10_iz1 NE 0, count)
  for k = 0, count-1 do begin
     z_str = z_str + string(value.adf10_iz1(k),format='(1x,i2)')
  endfor
  match_report = [match_report,' ',z_str,' ','    '+str_f1 + '  ' +str_f2]   
  
  for k=0,value.num_adf10-1 do begin
    i_str = string(k+1,format='(1x,i2)')+': '
    j_str = string(value.adf10_ind1[k],format='(1x,i2)') + '   ' +$ 
            string(value.adf10_ind2[k],format='(1x,i2)')
    match_report = [match_report,i_str+j_str]
  endfor
  match_report = [match_report,' '] 
  
  
  z_str = 'z1 = ' + string(value.supp_iz1,format='(1x,i2)')
  match_report = [match_report,'ADAS208 gcr file:',' ', $
                  value.supp,' ',z_str,' ',str_s1 + '  ' +str_s2]
  for k=0,value.num_supp-1 do begin
    i_str = string(value.supp_ind1[k],format='(1x,i2)') + '   ' +$ 
            string(value.supp_ind2[k],format='(1x,i2)')
    match_report = [match_report,i_str]
  endfor


; Trap mismatches of indices and number of adf10 files vs number of blocks 
; in the gcr partial file - a potential indicator of whether the sequence 
; in the ADAS208 gcr and adf10 isoelectronic sets are different. There is 
; not enough information in gcr partial sets to determine the sequence.


  ind_a = 10*value.adf10_ind1 + value.adf10_ind2
  ind_s = 10*value.supp_ind1 + value.supp_ind2
  
  loc = setdifference(ind_s, ind_a)

  if loc NE -1 then value.error_mess = '[mis-match of indices]'
  
  if value.num_supp NE value.num_adf10 then $
       value.error_mess = value.error_mess + '[number of partials do not match]'


; Check if z1=0 in the ADAS208 files - an indication that they are empty.

  if value.supp_iz1 EQ 0 then $
       value.error_mess = value.error_mess + '[z1=0 in gcr file]'

  
END 

;-----------------------------------------------------------------------------


PRO ADAS403_FILE_TYPE2, value, match_report

; Probes the adf10 and ADAS208 gcr files for ioelectronic cases - 
; ie adf10 types plt, pls and met

  
  str  = ' '
  num  = 0
  iz1  = 0


; First the supplement file
    
  openr,lun,value.supp,/get_lun
  
  while (not EOF(lun)) do begin
    readf,lun,str
    if (STRPOS(str,'--/ Z')) NE -1 then begin
       num = num + 1
       reads,str,iz1, format='(59x,i2)'
       value.supp_iz1  = iz1
    endif
  end   
   
  free_lun, lun
  value.num_supp = num


; Next the adf10 isoelectronic file - need to make some 
; assumptions here. The adf10 files are consistent in that they both
; have the same number of iz1 entries, temperatures etc.

  nz = 0
  openr,lun,value.adf10[0],/get_lun

  while (not EOF(lun)) do begin
    readf,lun,str
    if (STRPOS(str,'--/ Z')) NE -1 then begin
       reads,str,iz1, format='(59x,i2)'
       value.adf10_iz1[nz] = iz1 
       nz = nz + 1
    endif
  end   
  free_lun, lun
     



;  Extend the report for this dataclass
        
  match_report = [match_report,'adf10 isoelectronic file:',' ']
  
  
  match_report = [match_report,value.adf10(0)]
  
  z_str = 'z1 = '
  loc_z = where(value.adf10_iz1 NE 0, count)
  for k = 0, count-1 do begin
     z_str = z_str + string(value.adf10_iz1(k),format='(1x,i2)')
  endfor
  match_report = [match_report,' ',z_str,' ']   
    
  
  z_str = 'z1 = ' + string(value.supp_iz1,format='(1x,i2)')
  match_report = [match_report,'ADAS208 gcr file:',' ', $
                  value.supp,' ',z_str,' ']
  


; Check if z1=0 in the ADAS208 files - an indication that they are empty.

  if value.supp_iz1 EQ 0 then $
       value.error_mess = value.error_mess + '[z1=0 in gcr file]'


END 

;-----------------------------------------------------------------------------


FUNCTION ADAS403_PROC, inval, dataclasses, procval,                     $
                       match_report,                                    $
                       FONT_LARGE = font_large, FONT_SMALL = font_small


; Set defaults for keywords

  IF n_elements(font_large) eq 0 THEN font_large = ''
  IF n_elements(font_small) eq 0 THEN font_small = ''





; During reading and checking of files put up a busy widget

  widget_control, /hourglass
  busybase = widget_base(/column,xoffset=300,yoffset=200,     $
                           title = "ADAS403 INFORMATION")
  lab0 = widget_label(busybase, value='')
  lab1 = widget_label(busybase, value='')
  lab2 = widget_label(busybase, font=font_large,              $
                      value='       Gathering files - please wait       ')
  lab3 = widget_label(busybase, value='')
  lab4 = widget_label(busybase, value='')
  widget_control, busybase, /realize




; define structures for storing data - partial and isolectronic storage

  ndparts = 12 
  ndz     = 20

  partial  = {partial403,                    $
              type       : '',               $
              error_mess : '',               $
              num_adf10  : 0,                $
              adf10      : strarr(ndparts),  $
              num_supp   : 0,                $
              supp       : '',               $
              adf10_ind1 : intarr(ndparts),  $
              adf10_ind2 : intarr(ndparts),  $
              adf10_iz1  : intarr(ndz),      $
              supp_ind1  : intarr(ndparts),  $
              supp_ind2  : intarr(ndparts),  $
              supp_iz1   : 0                 }
              
  
  partial_set = REPLICATE({partial403},10)



; extract template for filename selected from inval
 
  basestr    = strarr(4)
  basestr(0) = inval.rootpathin
  if inval.year EQ '' then basestr(1) = '' else basestr(1) = 'XXX'+inval.year
  if inval.prefix EQ '' then begin
     if inval.year EQ '' then basestr(2) = '' else basestr(2) = '/XXX'+inval.year
  endif else begin
     basestr(2) = '/'+inval.prefix+'#XXX'+inval.year
  endelse
  if inval.seq EQ '' then basestr(3) = '' else basestr(3) = '_'+inval.seq+'XX.dat'
  template = basestr(0)+basestr(1)+basestr(2)+basestr(3)
 


; A template file name looks like 
;         /u/adas/adas/adf10/XXX96/pj#XXX96_heXX.dat
;
; Replace the XXX with the dataclass and XX with wildcard '*'
; and pass to FINDFILE to check what isoelectronic files exist.
; Store all results into an array of structures for later processing.


  for j = 0, n_elements(inval.class)-1 do begin
  
     if inval.class(j) EQ 1 then begin
        
        error_mess = ''
        
; adf10 files

        p1 = strpos(template,'XXX')     
        p2 = strpos(template,'XXX',/reverse_search)     

        file = template
        strput,file,dataclasses(j),p1
        strput,file,dataclasses(j),p2
        
        p3 = strpos(file,'XX')
        s1 = strmid(file,0,p3)
        s2 = strmid(file,p3+2)

; Construct a wildcard version of the filename - note the [0-9,#] ensures
; that only numbers or hashes can occur after the sequence. This is to 
; allow the distinction between, eg b and be files.
         
        file = s1+'[0-9,#]*'+s2
        
        adf10_ind1 = intarr(ndparts)
        adf10_ind2 = intarr(ndparts)
        adf10_iz1  = intarr(ndz)
        adf10_files = strarr(ndparts)
        adf10_in = FINDFILE(file,count=num_adf10)
        if num_adf10 EQ 0 then begin
            error_mess = '[No adf10 files] '
        endif else begin
           adf10_files[0:num_adf10-1] = adf10_in
        endelse
        
        
; supplement gcr ADAS208 file 
       
        supp_ind1 = intarr(ndparts)
        supp_ind2 = intarr(ndparts)
        file = inval.supprootin+dataclasses(j)+'208.pass'
        partial_file = FINDFILE(file,count=count)
        if count EQ 0 then begin
           error_mess = error_mess+' [No gcr partial file]'
           partial_file=''
        endif
        
; occasionally ADAS208 gives files of length 0
        
        command = 'ls -s ' + file
        spawn,command,result, /sh         
        isize = 0
        reads,result,isize
        if isize EQ 0 then begin
           error_mess = error_mess+' [file is empty]'
           partial_file=''
        endif
          

; put all info into a structure
        
        partial_set[j] = {partial403,                    $
                          type       : dataclasses(j),   $
                          error_mess : error_mess,       $
                          num_adf10  : num_adf10,        $
                          adf10      : adf10_files,      $
                          num_supp   : 0,                $
                          supp       : partial_file(0),  $
                          adf10_ind1 : adf10_ind1,       $
                          adf10_ind2 : adf10_ind2,       $
                          adf10_iz1  : adf10_iz1,        $
                          supp_ind1  : supp_ind1,        $
                          supp_ind2  : supp_ind2,        $
                          supp_iz1   : 0                 }
     
      endif
      
   endfor




; For the partial sets the number of adf10 files must match the
; number of blocks in the ADAS208 gcr file. There are 10 files (0->9) 
; but 2 file types (0-6 and 7-9).
;
; As we are passing an element of a array of structures, even though 
; it's a structure, to a procedure it is passed by value and not by 
; reference because it's a subscripted variable. Hence the tmp bodge.
;
; After getting info from ADAS403_FILE_INDEX fill up the report array.


   seperator =   '------------------------------------------------------' + $
                 '-------------------'
   match_report=['Results of matching ADAS208 partial GCR data to adf10 ' + $
                  'isoelectronic sets',' ',seperator,' ']
                  
   for j = 0,9 do begin

     match_report = [match_report,dataclasses(j),'---',' ']
     ; match the gcr and adf10 files

     if partial_set[j].error_mess EQ '' AND inval.class(j) EQ 1 then begin
        tmp=partial_set[j]
        if j LE 6 then adas403_file_type1, tmp, match_report else $
                       adas403_file_type2, tmp, match_report
        partial_set[j]=tmp
     endif

     ; errors/omissions in match_report
     
     
     if inval.class(j) EQ 0 then match_report = [match_report,'Not Selected']
     
     if partial_set[j].error_mess NE '' AND inval.class(j) EQ 1 then begin
        match_report = [match_report,'Error with this gcr type',      $
                                     partial_set[j].error_mess,  '   ']
     endif
     
     match_report = [match_report,seperator,' ']
     
   endfor
    
    
    
; We have finished preparing the data so destroy the busy widget and replace
; with the viewable match_report.

  widget_control, busybase, /destroy
  
  procval = partial_set
  


; Determine which dataclasses have valid adf10 and ADAS208 file combinations
; by getting the intersection of partial_set error_mess and type. If there
; are only some invalid combinations then offer the choice continue.


  classes     = where(procval.type NE '',num_classes)
  errors      = where(procval.error_mess NE '',num_errors)
  sd          = setdifference(classes,errors)
  if sd(0) NE -1 then begin
     valid_types = dataclasses(sd)
     num_invalid = n_elements(valid_types)
     error_messages = '' 
     for j=0,num_errors - 1 do begin
       error_messages = [error_messages,dataclasses(errors(j))+'  '+ $
                         procval[errors(j)].error_mess]
     endfor
  endif
  if num_errors EQ 0 then num_invalid = 0
  if num_errors EQ num_classes then num_invalid = num_errors

  
  
  CASE 1 OF
  
    (num_invalid EQ 0) : begin
    
         adas_text, match_report,'q.q', font = font_small, $
                    xsize = 75, ysize=30, writefile='NO',  $
                    wintitle = 'ADAS403 RESULTS'
         rep='CONTINUE'
         
    end
                           
    (num_invalid GT 0 AND num_invalid LT num_classes) : begin
    
          message = [ 'Some adf10 and  ADAS208 gcr file combinations ',$
                      'selected are invalid.                         ',$
                      error_messages,' ']
          buttons = ['Cancel', 'Continue'] 
          answer = popup(message=message, buttons=buttons,font=font_large)
          
          if answer EQ 'Cancel' then begin
             rep='CANCEL' 
          endif else begin
             rep = 'CONTINUE'
             adas_text, match_report,'q.q', font = font_small, $
                        xsize = 75, ysize=30, writefile='NO',  $
                        wintitle = 'ADAS403 RESULTS'
          endelse
     end
                         
    (num_invalid EQ num_classes) : begin
    
          message = [ 'There are no valid adf10 and  ',$
                      'ADAS208 gcr file combinations ',$
                      'selected.                     ',$
                      ' ']
          buttons = ['Accept'] 
          answer = popup(message=message, buttons=buttons,font=font_large)
          rep='CANCEL'
         
     end
     
     ELSE : begin
              rep = 'CANCEL'
              print,'What do we do in this case????'
            end
            
  ENDCASE
                           

  RETURN,rep
  
END
