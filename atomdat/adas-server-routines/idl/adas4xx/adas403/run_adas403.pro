;----------------------------------------------------------------------
;+
; PROJECT    :  ADAS
;
; NAME       :  run_adas403
;
; PURPOSE    :  Runs ADAS403 adf10 post-processing/merging code as an
;               IDL subroutine.
;
; ARGUMENTS  :  All output arguments will be defined appropriately.
;
;               NAME      I/O    TYPE   DETAILS
; INPUTS     :  uid        I     str    username of adf11 location.
;                                       'adas' for central ADAS data.
;               year       I     int    year of adf11 data.
;               seq        I     str    isoelectonic sequence.
;               supp_dir   I     str    directory of supplementary adf10
;                                       fragments (default is current directory)
;               class      I     str()  array of classes to be updated.
;                                       full list is ['acd','scd','ccd','prb',$
;                                                     'prc','qcd','xcd','plt',$
;                                                     'pls','met']
;               out_dir    I     str    output directory for updated adf10 files
;                                       (default is to overwrite)
;
; OUTPUTS    :  report    O     str()   list of classes to be updated.
;
;
; NOTES      :  run_adas403 is based on ADAS403 but does not call any of
;               its routines.
;
; AUTHOR     :  Martin O'Mullane
;
; DATE       :  14-02-2001
;
;
; MODIFIED:
;       1.1     Martin O'Mullane
;                - Initial version.
;       1.2     Martin O'Mullane
;                - Use [] rather than () for array indexing.
;       1.3     Martin O'Mullane
;                - Write nz line in gropus of 8 rather than 10.
;                - Remove spaces at end of comment lines.
;
; VERSION:
;       1.1    14-02-2001
;       1.2    24-01-2011
;       1.3    11-06-2014
;-
;----------------------------------------------------------------------

PRO update_adf10_probe_TYPE1, value, match_report

; Probes the adf10 and ADAS208 gcr files for partial cases -
; ie adf10 types acd, scd, ccd, prb, prc, qcd and xcd


  str  = ' '
  num  = 0
  ind1 = 0
  ind2 = 0
  iz1  = 0

; get IRRT/IGRD/JPRT/JGRD string here

  str_s1 = ''
  str_s2 = ''
  str_f1 = ''
  str_f2 = ''

; First the supplement file

  openr,lun,value.supp,/get_lun

  while (not EOF(lun)) do begin
    readf,lun,str
    if (STRPOS(str,'--/ I')) NE -1 then begin
       num = num + 1
       reads,str,str_s1,ind1,str_s2,ind2,iz1, format='(32x,a4,1x,i2,4x,a4,1x,i2,7x,i2)'
       value.supp_ind1[num-1] = ind1
       value.supp_ind2[num-1] = ind2
       value.supp_iz1  = iz1
    endif
  end

  free_lun, lun
  value.num_supp = num


; Next the set of partial adf10 isoelectronic files - need to make some
; assumptions here. The adf10 files are consistent in that they both
; have the same number of iz1 entries, temperatures etc.

  for j = 0, value.num_adf10-1 do begin

     nz = 0
     openr,lun,value.adf10[j],/get_lun

     while (not EOF(lun)) do begin
       readf,lun,str
       if (STRPOS(str,'--/ I')) NE -1 then begin
          reads,str,str_f1,ind1,str_f2,ind2,iz1, format='(32x,a4,1x,i2,4x,a4,1x,i2,7x,i2)'
          value.adf10_ind1[j] = ind1
          value.adf10_ind2[j] = ind2
          value.adf10_iz1[nz] = iz1
          nz = nz + 1
       endif
     end
     free_lun, lun

  endfor



; Extend the report for this dataclass

  match_report = [match_report,'adf10 isoelectronic files:',' ']


  for k = 0, value.num_adf10-1 do begin
    i_str = string(k+1,format='(1x,i2)')+': '
    match_report = [match_report,i_str+value.adf10[k]]
  endfor

  z_str = 'z1 = '
  loc_z = where(value.adf10_iz1 NE 0, count)
  for k = 0, count-1 do begin
     z_str = z_str + string(value.adf10_iz1[k],format='(1x,i2)')
  endfor
  match_report = [match_report,' ',z_str,' ','    '+str_f1 + '  ' +str_f2]

  for k=0,value.num_adf10-1 do begin
    i_str = string(k+1,format='(1x,i2)')+': '
    j_str = string(value.adf10_ind1[k],format='(1x,i2)') + '   ' +$
            string(value.adf10_ind2[k],format='(1x,i2)')
    match_report = [match_report,i_str+j_str]
  endfor
  match_report = [match_report,' ']


  z_str = 'z1 = ' + string(value.supp_iz1,format='(1x,i2)')
  match_report = [match_report,'ADAS208 gcr file:',' ', $
                  value.supp,' ',z_str,' ',str_s1 + '  ' +str_s2]
  for k=0,value.num_supp-1 do begin
    i_str = string(value.supp_ind1[k],format='(1x,i2)') + '   ' +$
            string(value.supp_ind2[k],format='(1x,i2)')
    match_report = [match_report,i_str]
  endfor


; Trap mismatches of indices and number of adf10 files vs number of blocks
; in the gcr partial file - a potential indicator of whether the sequence
; in the ADAS208 gcr and adf10 isoelectronic sets are different. There is
; not enough information in gcr partial sets to determine the sequence.


  ind_a = 10*value.adf10_ind1 + value.adf10_ind2
  ind_s = 10*value.supp_ind1 + value.supp_ind2

  loc = setdifference(ind_s, ind_a)

  if loc NE -1 then value.error_mess = '[mis-match of indices]'

  if value.num_supp NE value.num_adf10 then $
       value.error_mess = value.error_mess + '[number of partials do not match]'


; Check if z1=0 in the ADAS208 files - an indication that they are empty.

  if value.supp_iz1 EQ 0 then $
       value.error_mess = value.error_mess + '[z1=0 in gcr file]'


END
;-----------------------------------------------------------------------------



PRO update_adf10_probe_TYPE2, value, match_report

; Probes the adf10 and ADAS208 gcr files for ioelectronic cases -
; ie adf10 types plt, pls and met

  str  = ' '
  num  = 0
  iz1  = 0


; First the supplement file

  openr,lun,value.supp,/get_lun

  while (not EOF(lun)) do begin
    readf,lun,str
    if (STRPOS(str,'--/ Z')) NE -1 then begin
       num = num + 1
       reads,str,iz1, format='(59x,i2)'
       value.supp_iz1  = iz1
    endif
  end

  free_lun, lun
  value.num_supp = num


; Next the adf10 isoelectronic file - need to make some
; assumptions here. The adf10 files are consistent in that they both
; have the same number of iz1 entries, temperatures etc.

  nz = 0
  openr,lun,value.adf10[0],/get_lun

  while (not EOF(lun)) do begin
    readf,lun,str
    if (STRPOS(str,'--/ Z')) NE -1 then begin
       reads,str,iz1, format='(59x,i2)'
       value.adf10_iz1[nz] = iz1
       nz = nz + 1
    endif
  end
  free_lun, lun




;  Extend the report for this dataclass

  match_report = [match_report,'adf10 isoelectronic file:',' ']


  match_report = [match_report,value.adf10[0]]

  z_str = 'z1 = '
  loc_z = where(value.adf10_iz1 NE 0, count)
  for k = 0, count-1 do begin
     z_str = z_str + string(value.adf10_iz1[k],format='(1x,i2)')
  endfor
  match_report = [match_report,' ',z_str,' ']


  z_str = 'z1 = ' + string(value.supp_iz1,format='(1x,i2)')
  match_report = [match_report,'ADAS208 gcr file:',' ', $
                  value.supp,' ',z_str,' ']



; Check if z1=0 in the ADAS208 files - an indication that they are empty.

  if value.supp_iz1 EQ 0 then $
       value.error_mess = value.error_mess + '[z1=0 in gcr file]'


END
;-----------------------------------------------------------------------------



PRO update_adf10_write_TYPE1, infile, suppfile, loc, outfile

; Updates/replaces in the adf10 isoelectronic file the partial gcr coefficient
; from the ADAS208 partial files -
; ie adf10 types acd, scd, ccd, prb, prc, qcd and xcd

  str      = ''
  str1     = ''
  num      = -1
  idum     = 0
  nte      = 0
  ndens    = 0
  supp_z1  = 0.0
  supp_str = ''
  supp_com = ' '


; First the supplement file

  openr,lun,suppfile,/get_lun

  while (not EOF(lun)) do begin
    readf,lun,str1
    if (STRPOS(str1,'SSYSWT')) NE -1 then begin
       num = num + 1
       if num EQ loc then begin
          readf,lun,str
          readf,lun,idum,nte,ndens
          readf,lun,supp_z1
          for i=0,1 do readf,lun,str

          te        = DBLARR(nte)
          dens      = DBLARR(ndens)
          supp_data = DBLARR(ndens,nte)

          readf,lun,te
          readf,lun,dens
          readf,lun,supp_str
          readf,lun,supp_data

          readf,lun,str
          test = -1
          while (test NE 0 AND (not EOF(lun)) ) do begin
             readf,lun,str
             supp_com = [supp_com,str]
             test = STRPOS(str,'-',0)           ; -1 keep, 0 end of comments
          end
          icom     = n_elements(supp_com)
          supp_com = supp_com[0:icom-1]

       endif
    endif
  end

  free_lun, lun



; The adf10 isoelectronic file

  str        = ''
  str1       = ''
  nz         = 0
  nte        = 0
  ndens      = 0
  npot       = 0
  adf10_com  = ''
  adf10_head = ''


  openr,lun,infile,/get_lun

  while (not EOF(lun)) do begin
    readf,lun,str1
    adf10_head = [adf10_head,str1]
    if (STRPOS(str1,'SSYSWT')) NE -1 then begin

       readf,lun,str
       adf10_head = [adf10_head,str]

       readf,lun,nz,nte,ndens,npot

       adf10_z1 = fltarr(nz)
       readf,lun,adf10_z1

       readf,lun,str
       if npot NE 0 then begin
          adf10_pot = fltarr(npot)
          readf,lun,adf10_pot
       endif
       readf,lun,str


       te         = DBLARR(nte)
       dens       = DBLARR(ndens)
       adf10_data = DBLARR(ndens,nte,nz)
       tmp        = DBLARR(ndens,nte)
       adf10_str  = STRARR(nz)

       readf,lun,te
       readf,lun,dens

       for j = 0, nz - 1 do begin
         readf,lun,str
         readf,lun,tmp
         adf10_str[j] = str
         adf10_data[*,*,j] = tmp
       endfor

       ; Assume the comments are in z1 order and are separated by '----'
       ; store in adf10_com everything except the seperators and index
       ; the start of each comment block in adf10_com_ind.
       ;
       ; This is a reasonably dangerous assumption but as long as the
       ; files are created automatically we will be ok.

       adf10_com_ind = intarr(nz+1) ; pointer to start of comment z1

       adf10_com_ind[0] = 0
       icom = 1
       j = 0
       adf10_com = ' '
       readf,lun,str ; seperator
       while (not EOF(lun))  do begin
          readf,lun,str
          test = STRPOS(str,'-',0)           ; 0 end of comments for this z
          if test EQ 0 then begin
             adf10_com_ind[icom] = j
             icom = icom + 1
          endif else begin
             adf10_com = [adf10_com,str]
             j = j+1
          endelse
       end

    endif
  end

  free_lun, lun



; Write out the new adf10 file - This is where the real work of
; ADAS403 is done.

  seperator = '----------------------------------------' + $
              '----------------------------------------'


; adf10_head has [' ', '===' etc.] We must start at element 1 not 0.
; Not sure why but it works.


   ; Make out_vec and out_typ for z1 and source of data, either supp or
   ; adf10 file. The algorithm should work for insertion or replacement.
   ; If data comes from adf10 then type = 0
   ; From supp file type is 1
   ; We also need to track the index of the data in the adf10 file. Put
   ; the sorted index into loc_adf10.
   ;
   ; eg if typ_z_out = 0 1 0 ie replace the second Z with supp data
   ;    then loc_adf10 = 0 1 2 so in the loop data block 1 is skipped.
   ;    if typ_z_out = 1 0 0 0 ie a new block at the beginning
   ;    then loc_adf10 = -1 0 1 2 and no blocks are skipped.

     ipt_adf10 = indgen( n_elements(adf10_z1))
     typ_a = intarr(n_elements(adf10_z1))
     typ_s = 1

     rep = where(adf10_z1 EQ supp_z1, count)
     if count EQ 0 then begin
        out_vec   = [adf10_z1,supp_z1]
        out_typ   = [typ_a,typ_s]
        ipt_adf10 = [ipt_adf10,-1]
     endif else begin
        out_vec = adf10_z1
        out_typ = typ_a
        out_typ[rep[0]] = 1
     endelse


     tag       = sort(out_vec)
     nz_out    = out_vec[tag]
     typ_z_out = out_typ[tag]
     loc_adf10 = ipt_adf10[tag]


   openw,lun,outfile,/get_lun

   for j=1, n_elements(adf10_head)-1 do printf, lun, adf10_head[j]
   printf,lun,n_elements(nz_out),nte,ndens,npot, format='(4I5)'
   printf,lun,nz_out, format='(8F10.5)'
   printf,lun,seperator
   if npot NE 0 then printf,lun, adf10_pot, format= '(6F12.1,:)'
   printf,lun,seperator

   printf,lun,te,format='(8E10.2,:)'
   printf,lun,dens,format='(8E10.2,:)'


;  The data

   for j = 0, n_elements(nz_out) - 1 do begin
     if typ_z_out[j] EQ 1 then begin
        printf,lun,supp_str
        printf,lun,supp_data,format='(8E10.2,:)'
     endif else begin
        idata = loc_adf10[j]
        printf,lun,adf10_str[idata]
        printf,lun,adf10_data[*,*,idata],format='(8E10.2,:)'
     endelse
   endfor


;  The comments

   printf,lun,seperator


   for j = 0, n_elements(nz_out) - 1 do begin
     if typ_z_out[j] EQ 1 then begin
        for k = 1, n_elements(supp_com)-2 do printf,lun,strtrim(supp_com[k],0)
        printf,lun,seperator
     endif else begin
        idata = loc_adf10[j]
        for k = adf10_com_ind[idata]+1,adf10_com_ind[idata+1]  do begin
          printf,lun,strtrim(adf10_com[k],0)
        endfor
        printf,lun,seperator
     endelse
   endfor


   free_lun, lun


END
;-----------------------------------------------------------------------------



PRO update_adf10_write_TYPE2, infile, suppfile, loc, outfile

; Updates/replaces in the adf10 isoelectronic file the partial gcr coefficient
; from the ADAS208 partial files -
; ie adf10 types pls, plt and met

  str      = ''
  str1     = ''
  nte      = 0
  ndens    = 0
  nmet     = 0
  supp_z1  = 0.0
  supp_str = ''
  supp_com = ' '


; First the supplement file

  openr,lun,suppfile,/get_lun

  while (not EOF(lun)) do begin
    readf,lun,str1
    if (STRPOS(str1,'NMET')) NE -1 then begin
       reads,str1,nmet,format='(14x,I2)'
    endif
    if (STRPOS(str1,'SPNSYS')) NE -1 then begin
       readf,lun,str
       readf,lun,idum,nte,ndens
       readf,lun,supp_z1
       for i=0,1 do readf,lun,str

       te        = DBLARR(nte)
       dens      = DBLARR(ndens)
       tmp       = DBLARR(ndens,nte)
       supp_data = DBLARR(ndens,nte,nmet)

       readf,lun,te
       readf,lun,dens
       readf,lun,supp_str

       for j = 0, nmet-1 do begin
         readf,lun,str
         readf,lun,tmp
         supp_data[*,*,j] = tmp
       endfor

       readf,lun,str
       test = -1
       while (test NE 0 AND (not EOF(lun)) ) do begin
          readf,lun,str
          supp_com = [supp_com,str]
          test = STRPOS(str,'-',0)           ; -1 keep, 0 end of comments
       end
       icom     = n_elements(supp_com)
       supp_com = supp_com[0:icom-1]

    endif
  end

  free_lun, lun



; The adf10 isoelectronic file

  str        = ''
  str1       = ''
  nz         = 0
  nte        = 0
  ndens      = 0
  npot       = 0
  adf10_com  = ''
  adf10_head = ''


  openr,lun,infile,/get_lun

  while (not EOF(lun)) do begin
    readf,lun,str1
    adf10_head = [adf10_head,str1]
    if (STRPOS(str1,'NMET')) NE -1 then begin
       reads,str1,nmet,format='(14x,I2)'
    endif
    if (STRPOS(str1,'SPNSYS')) NE -1 then begin

       readf,lun,str
       adf10_head = [adf10_head,str]

       readf,lun,nz,nte,ndens,npot

       adf10_z1 = fltarr(nz)
       readf,lun,adf10_z1

       readf,lun,str
       if npot NE 0 then begin
          adf10_pot = fltarr(npot)
          readf,lun,adf10_pot
       endif
       readf,lun,str


       te         = DBLARR(nte)
       dens       = DBLARR(ndens)
       adf10_data = DBLARR(ndens,nte,nmet,nz)
       tmp        = DBLARR(ndens,nte)
       adf10_str  = STRARR(nz)

       readf,lun,te
       readf,lun,dens

       for j = 0, nz - 1 do begin
         readf,lun,str
         adf10_str[j] = str
         for k =0, nmet-1 do begin
            readf,lun,str
            readf,lun,tmp
            adf10_data[*,*,k,j] = tmp
         endfor
       endfor

       ; Assume the comments are in z1 order and are separated by '----'
       ; store in adf10_com everything except the seperators and index
       ; the start of each comment block in adf10_com_ind.
       ;
       ; This is a reasonably dangerous assumption but as long as the
       ; files are created automatically we will be ok.

       adf10_com_ind = intarr(nz+1) ; pointer to start of comment z1

       adf10_com_ind[0] = 0
       icom = 1
       j = 0
       adf10_com = ' '
       readf,lun,str ; seperator
       while (not EOF(lun))  do begin
          readf,lun,str
          test = STRPOS(str,'-',0)           ; 0 end of comments for this z
          if test EQ 0 then begin
             adf10_com_ind[icom] = j
             icom = icom + 1
          endif else begin
             adf10_com = [adf10_com,str]
             j = j+1
          endelse
       end

    endif
  end

  free_lun, lun




; Write out the new adf10 file - This is where the real work of
; ADAS403 is done.

  seperator = '----------------------------------------' + $
              '----------------------------------------'


; adf10_head has [' ', '===' etc.] We must start at element 1 not 0.
; Not sure why but it works.


   ; Make out_vec and out_typ for z1 and source of data, either supp or
   ; adf10 file. The algorithm should work for insertion or replacement.
   ; If data comes from adf10 then type = 0
   ; From supp file type is 1

     ipt_adf10 = indgen( n_elements(adf10_z1))
     typ_a = intarr(n_elements(adf10_z1))
     typ_s = 1

     rep = where(adf10_z1 EQ supp_z1, count)
     if count EQ 0 then begin
        out_vec = [adf10_z1,supp_z1]
        out_typ = [typ_a,typ_s]
        ipt_adf10 = [ipt_adf10,-1]
     endif else begin
        out_vec = adf10_z1
        out_typ = typ_a
        out_typ[rep[0]] = 1
     endelse


     tag       = sort(out_vec)
     nz_out    = out_vec[tag]
     typ_z_out = out_typ[tag]
     loc_adf10 = ipt_adf10[tag]




   openw,lun,outfile,/get_lun

   for j=1, n_elements(adf10_head)-1 do printf, lun, adf10_head[j]
   printf,lun,n_elements(nz_out),nte,ndens,npot, format='(4I5)'
   printf,lun,nz_out, format='(8F10.5)'
   printf,lun,seperator
   if npot NE 0 then printf,lun, adf10_pot, format= '(6F12.1,:)'
   printf,lun,seperator

   printf,lun,te,format='(8E10.2,:)'
   printf,lun,dens,format='(8E10.2,:)'


;  The data

   ia = 0
   for j = 0, n_elements(nz_out) - 1 do begin
     if typ_z_out[j] EQ 1 then begin
        printf,lun,supp_str
        for k =0, nmet-1 do begin
          str = '/'+string(k+1,format='(I1)')+'/'
          printf,lun,str
          printf,lun,supp_data[*,*,k],format='(8E10.2,:)'
        endfor
     endif else begin
        idata = loc_adf10[j]
        printf,lun,adf10_str(idata)
        for k =0, nmet-1 do begin
          str = '/'+string(k+1,format='(I1)')+'/'
          printf,lun,str
          printf,lun,adf10_data[*,*,k,idata],format='(8E10.2,:)'
        endfor
     endelse
   endfor


;  The comments

   printf,lun,seperator

   ia = 0

   for j = 0, n_elements(nz_out) - 1 do begin
     if typ_z_out[j] EQ 1 then begin
        for k = 1, n_elements(supp_com)-2 do printf,lun,strtrim(supp_com[k],0)
        printf,lun,seperator
     endif else begin
        idata = loc_adf10[j]
        for k = adf10_com_ind[idata]+1,adf10_com_ind[idata+1]  do begin
          printf,lun,strtrim(adf10_com[k],0)
        endfor
        printf,lun,seperator
     endelse
   endfor


   free_lun, lun


END
;-----------------------------------------------------------------------------




PRO run_adas403, uid      = uid,      $
                 year     = year,     $
                 seq      = seq,      $
                 supp_dir = supp_dir, $
                 class    = class,    $
                 out_dir  = out_dir,  $
                 report   = report

dataclasses = ['acd','scd','ccd','prb','prc','qcd','xcd','plt','pls','met']


; Check that we get all inputs and that they are correct. Otherwise print
; a message and return to command line.

on_error, 2

if n_elements(uid) EQ 0 then message, 'Location for adf10 files required'
if n_elements(year) EQ 0 then message, 'Year for adf10 files required'
if n_elements(seq) EQ 0 then message, 'Sequence for adf10 files required'

seq = strlowcase(seq)

if n_elements(supp_dir) EQ 0 then supp_dir = './'

if n_elements(class) EQ 0 then begin
   message,'Indicate which classes are to be updated'
endif else begin

   ind_class = intarr(10)
   xclass    = strlowcase(class)

   for j = 0, n_elements(class)-1 do begin
     ind = where(dataclasses EQ xclass[j], count)
     if count NE 0 then ind_class[ind] = 1
   endfor

endelse



if n_elements(out_dir) EQ 0 then begin
   out_type = 1
   print, 'Input adf10 files will be updated'
endif else begin
   out_type = 0
endelse

; define structures for storing data - partial and isolectronic storage

  ndparts = 12
  ndz     = 20

  partial  = {partial403,                    $
              type       : '',               $
              error_mess : '',               $
              num_adf10  : 0,                $
              adf10      : strarr(ndparts),  $
              num_supp   : 0,                $
              supp       : '',               $
              adf10_ind1 : intarr(ndparts),  $
              adf10_ind2 : intarr(ndparts),  $
              adf10_iz1  : intarr(ndz),      $
              supp_ind1  : intarr(ndparts),  $
              supp_ind2  : intarr(ndparts),  $
              supp_iz1   : 0                 }


  partial_set = REPLICATE({partial403},10)




; A template file name looks like
;         /u/adas/adas/adf10/XXX96/pj#XXX96_heXX.dat
;
; Replace the XXX with the dataclass and XX with wildcard '*'
; and pass to FINDFILE to check what isoelectronic files exist.
; Store all results into an array of structures for later processing.

  log_name  = getenv("LOGNAME")
  adas_user = getenv("ADASUSER")
  adas_cent = getenv("ADASCENT")

  part_1 = strmid(adas_user,0,strpos(adas_user,log_name))
  part_2 = strcompress(uid, /remove_all)
  part_3 = strmid(adas_user,strpos(adas_user,log_name)+strlen(log_name))

  str_year = string(year,format='(i2)')
  template = part_1 + part_2 +part_3 + '/adf10/XXX' + str_year + $
             '/pj#XXX' + str_year + '_' + strcompress(seq) + 'XX.dat'

  for j = 0, n_elements(ind_class)-1 do begin

     if ind_class[j] EQ 1 then begin

        error_mess = ''

; adf10 files

        p1 = strpos(template,'XXX')
        p2 = strpos(template,'XXX', /reverse_search)

        file = template
        strput,file,dataclasses[j],p1
        strput,file,dataclasses[j],p2

        p3 = strpos(file,'XX')
        s1 = strmid(file,0,p3)
        s2 = strmid(file,p3+2)

; Construct a wildcard version of the filename - note the [0-9,#] ensures
; that only numbers or hashes can occur after the sequence. This is to
; allow the distinction between, eg b and be files.

        file = s1+'[0-9,#]*'+s2

        adf10_ind1 = intarr(ndparts)
        adf10_ind2 = intarr(ndparts)
        adf10_iz1  = intarr(ndz)
        adf10_files = strarr(ndparts)
        adf10_in = FINDFILE(file,count=num_adf10)
        if num_adf10 EQ 0 then begin
            error_mess = '[No adf10 files] '
        endif else begin
           adf10_files[0:num_adf10-1] = adf10_in
        endelse


; supplement gcr ADAS208 file

        supp_ind1 = intarr(ndparts)
        supp_ind2 = intarr(ndparts)
        file = supp_dir+dataclasses[j]+'208.pass'
        partial_file = FINDFILE(file,count=count)
        if count EQ 0 then begin
           error_mess = error_mess+' [No gcr partial file]'
           partial_file=''
        endif

; occasionally ADAS208 gives files of length 0

        command = 'ls -s ' + file
        spawn,command,result
        isize = 0
        reads,result,isize
        if isize EQ 0 then begin
           error_mess = error_mess+' [file is empty]'
           partial_file=''
        endif


; put all info into a structure

        partial_set[j] = {partial403,                    $
                          type       : dataclasses[j],   $
                          error_mess : error_mess,       $
                          num_adf10  : num_adf10,        $
                          adf10      : adf10_files,      $
                          num_supp   : 0,                $
                          supp       : partial_file(0),  $
                          adf10_ind1 : adf10_ind1,       $
                          adf10_ind2 : adf10_ind2,       $
                          adf10_iz1  : adf10_iz1,        $
                          supp_ind1  : supp_ind1,        $
                          supp_ind2  : supp_ind2,        $
                          supp_iz1   : 0                 }

      endif

   endfor




; For the partial sets the number of adf10 files must match the
; number of blocks in the ADAS208 gcr file. There are 10 files (0->9)
; but 2 file types (0-6 and 7-9).
;
; As we are passing an element of a array of structures, even though
; it's a structure, to a procedure it is passed by value and not by
; reference because it's a subscripted variable. Hence the tmp bodge.
;
; After getting info from ADAS403_FILE_INDEX fill up the report array.


   seperator =   '------------------------------------------------------' + $
                 '-------------------'
   match_report=['Results of matching ADAS208 partial GCR data to adf10 ' + $
                  'isoelectronic sets',' ',seperator,' ']

   for j = 0,9 do begin

     match_report = [match_report,dataclasses[j],'---',' ']
     ; match the gcr and adf10 files

     if partial_set[j].error_mess EQ '' AND ind_class[j] EQ 1 then begin
        tmp=partial_set[j]
        if j LE 6 then update_adf10_probe_type1, tmp, match_report else $
                       update_adf10_probe_type2, tmp, match_report
        partial_set[j]=tmp
     endif

     ; errors/omissions in match_report


     if ind_class[j] EQ 0 then match_report = [match_report,'Not Selected']

     if partial_set[j].error_mess NE '' AND ind_class[j] EQ 1 then begin
        match_report = [match_report,'Error with this gcr type',      $
                                     partial_set[j].error_mess,  '   ']
     endif

     match_report = [match_report,seperator,' ']

   endfor



; We have finished preparing the data so destroy the busy widget and replace
; with the viewable match_report.


  procval = partial_set



; Determine which dataclasses have valid adf10 and ADAS208 file combinations
; by getting the intersection of partial_set error_mess and type. If there
; are only some invalid combinations then offer the choice continue.


  classes     = where(procval.type NE '',num_classes)
  errors      = where(procval.error_mess NE '',num_errors)
  sd          = setdifference(classes,errors)
  if sd[0] NE -1 then begin
     valid_types = dataclasses[sd]
     num_invalid = n_elements(valid_types)
     error_messages = ''
     for j=0,num_errors - 1 do begin
       error_messages = [error_messages,dataclasses(errors[j])+'  '+ $
                         procval[errors[j]].error_mess]
     endfor
  endif
  if num_errors EQ 0 then num_invalid = 0
  if num_errors EQ num_classes then num_invalid = num_errors


; Determine which dataclasses are to be written - need to test whether the
; subdirectories exist.

  classes     = where(procval.type NE '')
  errors      = where(procval.error_mess NE '')
  sd          = setdifference(classes,errors)
  if sd[0] NE -1 then valid_types = dataclasses[sd] else $
                      valid_types = dataclasses[classes]


; Write out the updated files

  outfile_list = ' '

  for j = 0, n_elements(classes) - 1 do begin

     iclass = classes[j]

     if procval[iclass].error_mess EQ '' then begin

        for i = 0, procval[iclass].num_adf10-1 do begin

          ; which partial in gcr file

          ind_a = 10*procval[iclass].adf10_ind1[i] + procval[iclass].adf10_ind2[i]
          ind_s = 10*procval[iclass].supp_ind1 + procval[iclass].supp_ind2

          loc = where(ind_s EQ ind_a)
          loc = loc[0]

          CASE out_type OF

             0 : begin
                    str1 = out_dir
                    sloc = strpos(procval[iclass].adf10[i],'/', /reverse_search)
                    str2 = strmid(procval[iclass].adf10[i],sloc+1)
                    outfile = str1 + str2
                 end
             1 : outfile = procval[iclass].adf10[i]
             ELSE : print,'ADAS403_OUT - outfile there should be nothing else'

          ENDCASE


          if iclass LT 7 then begin
             update_adf10_write_type1, procval[iclass].adf10[i],procval[iclass].supp,$
                                   loc, outfile
          endif else begin
             update_adf10_write_type2, procval[iclass].adf10[i],procval[iclass].supp,$
                                   loc, outfile
          endelse

          outfile_list = [outfile_list, outfile]

        endfor

     endif

  endfor


; output a report if requested

if arg_present(report) then report = match_report

END
