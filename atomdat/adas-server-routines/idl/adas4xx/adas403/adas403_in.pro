; Copyright (c) 1999, Strathclyde University.
; SCCS info: Module @(#)$Header: /home/adascvs/idl/adas4xx/adas403/adas403_in.pro,v 1.1 2004/07/06 10:39:19 whitefor Exp $ Date $Date: 2004/07/06 10:39:19 $
;+
; PROJECT:
;       ADAS 
;
; NAME:
;	ADAS403_IN
;
; PURPOSE: 
;       This function puts up a widget asking for the directory of the
;       ADAS208 gcr files, a non-exclusive button group for selecting
;       which gcr data class are to be used and the source of the adf10
;       isoelectronic files to be updated.
;
;
; EXPLANATION:
;       No checking is done here. All information is passed onto the
;       ADAS403_PROC function for checking the data by updating the 
;       input inval structure.
;       A template of the adf10 isoelectronic files, with XXX replacing
;       the data class (acd, plt etc.) is dynamically constructed and
;       displayed. The user does not even have to press Enter in the
;       text entry boxes!     
;
; NOTES:
;       The way of returning information from the widget is different
;       from previous ADAS programs. There are no seperate driver and
;       compound widget parts. A pointer to the data we want returned 
;       from the widget is storeed in the user value of the top level
;       base of the realised widget. On exiting the widget the pointer
;       is used to reference the data stored on the heap. The pointer 
;       is then destroyed. 
; 
;
;
; INPUTS:
;        inval       - A structure holding the 'remembered' input data.
;                      ROOTPATHIN  : current selected adf10 root
;                      CENTROOTIN  : central ADAS adf10 location
;                      USERROOTIN  : user's ADAS adf10 location 
;                      SUPPROOTIN  : current location of ADAS208 files 
;                      YEAR        : year of adf10 
;                      PREFIX      : prefix of adf10     
;                      SEQ         : sequence of adf10           
;                      CLASS       : indices of selected classes
;        dataclasses - A string array of the possible dataclasses
;                      'acd','scd','ccd','prb','prc','qcd','xcd',
;                      'plt','pls' and 'met'
;                     
; OPTIONAL INPUTS:
;       None
;
; OUTPUTS:
;       The function returns a string rep
;           rep = 'CONTINUE' if everything is ok
;           rep = 'CANCEL' if there is a problem or indeed if the user
;                          closes the widget with the mouse. 
;
; OPTIONAL OUTPUTS:
;       None
;
; KEYWORD PARAMETERS:
;       FONT_LARGE - The name of a larger font e.g 'courier_bold14'
;
;       FONT_SMALL - The name of a smaller font e.g 'courier_bold12'
;
;
; CALLS:
;                            
;       ADAS403_IN_FILE         : Reacts to events relevant to constructing
;                                 adf10 filenames.
;       ADAS403_IN_NULLS        : Captures events and does nothing (procedure)
;       ADAS403_IN_NULL_EVENTS  : Captures events and does nothing (function)
;       ADAS403_IN_EVENT        : Reacts to Cancel and Done
;
; SIDE EFFECTS:
;
; CATEGORY:
;	Widgets
;
; WRITTEN:
;       Martin O'Mullane
;
; MODIFIED:
;	1.1	Martin O'Mullane
;		First release 
;
; VERSION:
;       1.1	16-08-99
;
;-
;-----------------------------------------------------------------------------

PRO ADAS403_IN_FILE, event

; This event handler captures all text and button input relevant to 
; constructing a valid adf10 filename. The only events which come to 
; handler are button and text entries.

  Widget_Control, event.top, Get_UValue=info
  Widget_Control, event.id, Get_Value=userEvent

; Remove instruction - we are not using dynamic resize so make blanks
; as large as any possible error string.

  Widget_Control, info.messID, Set_Value='                                     '


; recover strings for template identifer
   
   str_1 = info.basestr(0)
   str_2 = info.basestr(1)
   str_3 = info.basestr(2)
   str_4 = info.basestr(3)

; Is central or user ADAS accessed

   CASE userEvent(0) of

     'Central Data' : begin
                        str_1 = info.centroot
                        info.basestr(0) = str_1
                      end
     'User Data'    : begin
                         str_1 = info.userroot
                         info.basestr(0) = str_1
                      end

   ELSE:  begin

    ; Handle text insertion events to year, prefix and sequence

       IF event.type EQ 0 THEN BEGIN

          Widget_Control, event.id, Get_Value=text
          text   = text(0)
          length = StrLen(text)
          
          ; only react if there are less than 2 characters there already
          ; and ignore RETURN (13B)
          
          if length LT 2 AND byte(event.ch) NE 13B then begin 
             selection = Widget_Info(event.id, /Text_Select)
             Widget_Control, event.id, /Use_Text_Select, Set_Value=String(event.ch)
             Widget_Control, event.id, Set_Text_Select=event.offset + 1
          endif

       ENDIF 

    ; Must also deal with deletions

       IF event.type EQ 2 THEN BEGIN

          Widget_Control, event.id, Get_Value=text
          text = text(0)
          length = StrLen(text)

          Widget_Control, event.id, Set_Value=StrMid(text, 0, length-event.length)
          Widget_Control, event.id, Set_Text_Select=event.offset

       ENDIF

; now change the template dynamically
       
       Widget_Control, event.id, Get_value=text
       Widget_Control, event.id, get_uvalue=part

       CASE part OF

         'year'   : begin 
                       str_2 = 'XXX'+text(0)
                       info.basestr(1) = str_2
                       loc = strpos(info.basestr(2),'XXX')
                       strput,str_3,text(0),loc+3
                    end
         'prefix' : begin
                       if strlen(text(0)) GE 1  $
                          then str_3 = '/'+text(0)+'#XXX'+StrMid(info.basestr(1),3,2) $
                          else str_3 = '/XXX'+StrMid(info.basestr(1),3,2) 
                       info.basestr(2) = str_3
                    end
         'seq'    : begin 
                       str_4 = '_'+text(0)+'XX.dat'
                       info.basestr(3) = str_4
                    end
                    
          ELSE :

       ENDCASE
       Widget_Control, info.baseID, Set_value=str_1+str_2+str_3+str_4
   
   END
   
   ENDCASE


   
   
; now fill in the adf10 template  
   
   Widget_Control, info.baseID, Set_value=str_1+str_2+str_3+str_4

   Widget_Control, event.top, Set_Uvalue=info

END 

;----------------------------------------------------------------------------

FUNCTION ADAS403_IN_NULL_EVENTS, event

   ; The purpose of this event handler is to do nothing
   ;and ignore all events that come to it.
   
   ; need a function for cw_bgroup and a procedure for widget_test!!!! WHY???
   
   RETURN,0

END 

;-----------------------------------------------------------------------------

PRO ADAS403_IN_NULLS, event

   ; The purpose of this event handler is to do nothing
   ;and ignore all events that come to it.
   
END 

;-----------------------------------------------------------------------------


PRO ADAS403_IN_EVENT, event

   ; Get the info structure out of the user value of the top-level base.

Widget_Control, event.top, Get_UValue=info


; Something nefarious may cause an error also.
;  -- if so trap it and exit gracefully

CATCH, error
IF error NE 0 THEN BEGIN
   formdata = {cancel:1}
   *info.ptrToFormData = formdata
   Widget_Control, event.top, /Destroy
   message,!err_string
   RETURN
ENDIF

Widget_Control, event.id, Get_Value=userEvent


CASE userEvent OF

  'Cancel' : begin
               formdata = {cancel:1}
               *info.ptrToFormData =formdata
               widget_Control, event.top, /destroy
             end 

      
  'Done'   : begin
                 
                ; gather the data for return to calling program
                
                widget_Control, info.filenameID, Get_Value=file
                widget_Control, info.choiceID, Get_Value=buttons
                widget_Control, info.yearID, Get_Value=year
                widget_Control, info.prefixID, Get_Value=prefix
                widget_Control, info.seqID, Get_Value=seq
               
                err = 0
                
                ; have files been selected
                if total(buttons) eq 0.0 then begin
                   err = 1
                   mess = 'Choose at least ONE partial file'
                endif
                                
                if err EQ 0 then begin
                   formdata = {filename : file(0),         $
                               root     : info.basestr(0), $
                               year     : year,            $ 
                               prefix   : prefix,          $ 
                               seq      : seq,             $ 
                               classes  : buttons,         $ 
                               cancel   : 0                }
                   *info.ptrToFormData = formdata
                   widget_control, event.top, /destroy
                endif else begin
                   widget_Control, info.messID, Set_Value=mess
                endelse

             end
             
             
  else : print,'ADAS403_IN_EVENT : You should not see this message! ',userEvent
                   
ENDCASE

END
  
;-----------------------------------------------------------------------------


FUNCTION adas403_in, inval, dataclasses, $
                     FONT_LARGE = font_large, FONT_SMALL = font_small


		;**** Set defaults for keywords ****

  IF n_elements(font_large) eq 0 THEN font_large = ''
  IF n_elements(font_small) eq 0 THEN font_small = ''



; Extract template for filename selected from inval nad store fragments
; in basestr array.
 
  basestr    = strarr(4)
  basestr(0) = inval.rootpathin
  if inval.year EQ '' then basestr(1) = '' else basestr(1) = 'XXX'+inval.year
  if inval.prefix EQ '' then begin
     if inval.year EQ '' then basestr(2) = '' else basestr(2) = '/XXX'+inval.year
  endif else begin
     basestr(2) = '/'+inval.prefix+'#XXX'+inval.year
  endelse
  if inval.seq EQ '' then basestr(3) = '' else basestr(3) = '_'+inval.seq+'XX.dat'
  template = basestr(0)+basestr(1)+basestr(2)+basestr(3)
 
 
 
                ;********************************************
		;**** create modal top level base widget ****
                ;********************************************
                
  parent = Widget_Base(Column=1, Title='ADAS 403 INPUT', $
                       XOFFSET=100, YOFFSET=1)

                
  rc = widget_label(parent,value='  ',font=font_large)

                 
  
                ;*****************************************
		;**** Ask for input files -           ****
                ;**** adf10 and partials from ADAS208 ****
                ;*****************************************

  base  = widget_base(parent, /column)
  
  
  mrow  = widget_base(base, /row)
  
  mlab  = widget_label(mrow, value='ADAS208 passing files location :  ', font=font_large)
  filenameID = widget_text(mrow, value=inval.supprootin, font=font_large,   $
                            xsize=45,/editable,event_pro='ADAS403_IN_NULLS')
  
  
  mrow  = widget_base(parent, /row)
  mlab  = widget_label(mrow, value='Insert/Replace the following :    ', font=font_large)
 
  choiceID = cw_bgroup(mrow,dataclasses,  $
                             nonexclusive=1,column=4,          $
                             event_func='ADAS403_IN_NULL_EVENTS',$
                             /no_release, /frame, font=font_large)
                             
  mlab  = widget_label(mrow, value=' ', font=font_large)

  mrow  = widget_base(parent, /row)
  
  mlab  = widget_label(mrow, value='Isoelectronic adf10 files :       ', font=font_large)
  mcol  = widget_base(mrow, /column)

  mrow      = widget_base(mcol, /row)
  mlab      = widget_label(mrow, value='Source : ', font=font_large)
  centralID = widget_button(mrow,value='Central Data',font=font_large,$
                            event_pro='ADAS403_IN_FILE')
  userID    = widget_button(mrow,value='User Data',font=font_large,$
                            event_pro='ADAS403_IN_FILE')

  mrow      = widget_base(mcol, /row)
  mlab      = widget_label(mrow, value='Year   : ', font=font_large)
  yearID    = widget_text(mrow, value=inval.year, font=font_large,   $
                            all_events=1, editable=0,                $
                            xsize=4,event_pro='ADAS403_IN_FILE',     $
                            uvalue='year')

  mrow      = widget_base(mcol, /row)
  mlab      = widget_label(mrow, value='Prefix : ', font=font_large)
  prefixID  = widget_text(mrow, value=inval.year, font=font_large,   $
                            xsize=4,event_pro='ADAS403_IN_FILE',$
                            all_events=1, editable=0,                $
                            uvalue='prefix')
  mlab      = widget_label(mrow, value='  (usually pj)', font=font_small)

  mrow      = widget_base(mcol, /row)
  mlab      = widget_label(mrow, value='Seq.   : ', font=font_large)
  seqID     = widget_text(mrow, value=inval.year, font=font_large,   $
                            xsize=4,event_pro='ADAS403_IN_FILE',$
                            all_events=1, editable=0,                $
                            uvalue='seq')
                            
  mrow      = widget_base(mcol, /row)
  baseID    = widget_text(mrow, value=template, font=font_small,   $
                            xsize=50,event_pro='ADAS403_IN_NULLS')
                            
                            
                ;*****************
		;**** Buttons ****
                ;*****************
                
  mrow     = widget_base(parent,/row,/align_center)
  messID   = widget_label(mrow,font=font_large, $
                          value='          Enter File information             ')
                
  mrow     = widget_base(parent,/row)
  cancelID = widget_button(mrow,value='Cancel',font=font_large)
  
  doneID   = widget_button(mrow,value='Done',font=font_large)




; Initial settings 

  widget_control,choiceID, Set_Value=inval.class
  
  widget_control,baseID,set_value=template
  widget_control,yearID,set_value=inval.year
  widget_control,prefixID,set_value=inval.prefix
  widget_control,seqID,set_value=inval.seq




; Realize the ADAS403 input widget.

   widget_Control, parent, /realize

; Create a pointer to store the information collected from the form.
; The initial data stored here is set to CANCEL, so nothing needs to
; be done if the user kills the widget with the mouse.

   ptrToFormData = Ptr_New({cancel:1})

; Create an info structure with program information.

  info = { filenameID      :  filenameID,         $
           choiceID        :  choiceID,           $
           centralID       :  centralID,          $
           userID          :  userID,             $
           yearID          :  yearID,             $
           prefixID        :  prefixID,           $
           seqID           :  seqID,              $
           baseID          :  baseID,             $
           messID          :  messID,             $
           centroot        :  inval.centrootin,   $        
           userroot        :  inval.userrootin,   $        
           basestr         :  basestr,            $        
           dataclasses     :  dataclasses,        $        
           ptrToFormData   :  ptrToFormData       }  
            
               
; Store the info structure in the user value of the top-level base and launch
; the widget into the world.

  widget_control, parent, Set_UValue=info

  XManager, 'adas403_in', parent, Event_Handler='ADAS403_IN_EVENT'


; When the widget is destroyed, the block is released, and we
; return here. Get the form data that was collected by the form
; and stored in the pointer location.

formdata = *ptrToFormData

rep='CONTINUE'
IF N_Elements(formdata) EQ 0 THEN BEGIN
   Ptr_Free, ptrToFormData
   rep ='CANCEL'
ENDIF
 
IF formdata.cancel EQ 1 THEN BEGIN
   Ptr_Free, ptrToFormData
   rep ='CANCEL'
ENDIF

if rep eq 'CONTINUE' then begin
   inval.rootpathin  = formdata.root   
   inval.supprootin  = formdata.filename(0)   
   inval.year        = formdata.year
   inval.prefix      = formdata.prefix
   inval.seq         = formdata.seq
   inval.class       = formdata.classes 
endif       

   ; Free the pointer.

Ptr_Free, ptrToFormData

RETURN,rep

END
