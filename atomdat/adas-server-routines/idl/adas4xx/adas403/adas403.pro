; Copyright (c) 1999, Strathclyde University.
; SCCS info: Module @(#)$Header: /home/adascvs/idl/adas4xx/adas403/adas403.pro,v 1.3 2004/07/06 10:39:17 whitefor Exp $ Date $Date: 2004/07/06 10:39:17 $
;+
; PROJECT:
;       ADAS
;
; NAME:
;       ADAS403
;
; PURPOSE:
;       The highest level routine for the ADAS 403 program.
;
; EXPLANATION:
;       This routine is called from the main adas system routine,
;       adas.pro, to start the ADAS 403 application. This is the
;       first ADAS program to be fully written in IDL, which may
;       or may not be a good thing. 
;
;       The purpose of ADAS403 is to take the partial GCR passing
;       file output of ADAS208 and to insert, or replace, these
;       data into the isoelectronic adf10 datasets. There is a strict
;       naming convention for adf10 sets, eg helike
;           /home/adas/adas/adf10/acd96/pj#acd96_he12.dat
;       This file contains the helike 1-2 partial data. The pj
;       prefix is optional but is usually enforced. The directory
;       structure in adf10 is strictly adhered to in this program.
;       ie. to choose and adf10 file the year, prefix and root
;       (user or central ADAS) are given. The dataclasses are selected
;       from a button list and the passing directory of the ADAS208
;       output is also selected. All input is checked for existence
;       and some data consistency checks are performed.
;
;       There is no processing screen. However a summary of the file
;       checks and a summary of their contents are displayed.
;
;       For output a summary paper.txt and a choice of 3 output 
;       options are provided. The user can either replace the original
;       adf10 with the supplemented ones, write all the new adf10 files
;       to a passing directory or send them to the users adf10 
;       directory (a year and prefix are the inputs for this option).
;
;
; USE:
;       First the system settings must be established by calling
;       adas_sys_set.pro then adas403.pro is called to start the
;       ADAS 403 application;
;
;       adas_sys_set, adasrel, fortdir, userroot, centroot, $
;                     devlist, devcode, font_large, font_small, edit_fonts
;       adas403,   adasrel, fortdir, userroot, centroot, devlist, $
;                  devcode, font_large, font_small, edit_fonts
;
; INPUTS:
;       ADASREL - A string indicating the ADAS system version,
;                 e.g ' ADAS RELEASE: ADAS93 V1.4'.  The first
;                 character should be a space.
;
;       FORTDIR - A string holding the path to the directory where the
;                 FORTRAN executables are, e.g '/disk/adas/fortran/exec'
;                 Not used but retainned for compatibility.
;
;       USERROOT - A string holding the path to the root directory of
;                  the user's adas data. e.g '/disk/bowen/adas' This
;                  root directory will be used by adas to construct
;                  other path names.  In particular the user's default
;                  interface settings will be stored in the directory
;                  USERROOT+'/defaults'.  An error will occur if the
;                  defaults directory does not exist.
;
;       CENTROOT - Like USERROOT, but this directory points to the
;                  central data area for the system.  User defaults are
;                  not stored on CENTROOT.
;
;       DEVLIST - A string array of hardcopy device names, used for
;                 graphical output. e.g ['Post-Script','HP-PCL','HP-GL']
;                 This array must mirror DEVCODE.  DEVCODE holds the
;                 actual device names used in a SET_PLOT statement.
;                 Not used but retainned for compatibility.
;
;       DEVCODE - A string array of hardcopy device code names used in
;                 the SET_PLOT statement, e.g ['PS', 'PCL', 'HP']
;                 This array must mirror DEVLIST.
;                 Not used but retainned for compatibility.
;
;       FONT_LARGE - The name of a larger font e.g 'courier_bold14'
;
;       FONT_SMALL - The name of a smaller font e.g 'courier_bold12'
;
;       EDIT_FONTS - A structure of two fonts used in the table editor
;                    adas_edtab.pro, {font_norm:'',font_input:''}.
;                    The two fonts are used to differentiate between
;                    editable and non-editable parts of the table. You
;                    may have to experiment to get two fonts which give
;                    the same default spacing in the edit widget.
;                    e.g {font_norm:'helvetica_bold14', $
;                         font_input:'helvetica_oblique14'}
;                  Not used but retainned for compatibility.
;
; OPTIONAL INPUTS:
;       None
;
; OUTPUTS:
;       None - Note this routine should leave all inputs un-modified.
;
; OPTIONAL OUTPUTS:
;       None
;
; KEYWORD PARAMETERS:
;       None
;
; CALLS:
;       ADAS403_IN    Prompts for and gathers input files
;       ADAS403_PROC  Tests files for data consistency
;       ADAS403_OUT   Prompts for and writes output files
;       XXDATE        Get date and time from operating system.
;       POUP          Puts up a messahe on the screen.
;
; SIDE EFFECTS:
;       Some system calls in the called functions.
;
; NOTES:
;       This program requires IDL v5. This is tested for at the beginning.
;       The mode of operation is different to other ADAS programs in that
;       the 3 called routines are functions which return the variable 'rep'.
;       rep can be 'CONTINUE', 'CANCEL' or 'MENU' and subsequent action
;       depends on the returned value.
;       As a side note there are no common blocks!
;
; CATEGORY:
;       Adas system.
;
; WRITTEN:
;       Martin O'Mullane
;
; MODIFIED:
;       1.1     Martin O'Mullane
;               First release
;	1.2	Richard Martin
;		Increased version no. to 1.2
;	1.3	Richard Martin
;		Increased version no. to 1.3
;
; VERSION:
;       1.1     16-08-99
;	1.2	10-11-00
;	1.3	18-03-03
;
;-
;-----------------------------------------------------------------------------

PRO ADAS403,    adasrel, fortdir, userroot, centroot,                   $
                devlist, devcode, font_large, font_small, edit_fonts

                ;************************
                ;**** Initialisation ****
                ;************************

    adasprog = ' PROGRAM: ADAS403 V1.3'
    deffile  = userroot+'/defaults/adas403_defaults.dat'
    bitfile  = centroot+'/bitmaps'
    device   = ''
                
    dataclasses = ['acd','scd','ccd','prb','prc','qcd','xcd','plt','pls','met']
    

                ;******************************************
                ;**** Search for user default settings ****
                ;**** If not found create defaults     ****
                ;**** inval: settings for data files   ****
                ;**** outval: settings for output      ****
                ;******************************************

    files = findfile(deffile)
    if files(0) eq deffile then begin
    
        restore, deffile
        inval.centrootin = centroot+'/adf10/'
        inval.userrootin = userroot+'/adf10/'
    
    endif else begin
        
        classarr = intarr(10)
        inval = { ROOTPATHIN     :   userroot+'/adf10/',       $
                  CENTROOTIN     :   centroot+'/adf10/',       $
                  USERROOTIN     :   userroot+'/adf10/',       $
                  SUPPROOTIN     :   centroot+'/pass/',        $
                  YEAR           :   '',                       $
                  PREFIX         :   '',                       $
                  SEQ            :   '',                       $
                  CLASS          :   classarr                  }

        outval =  { OUTBUT      : 1,                $
                    APPBUT      : -1,               $
                    REPBUT      : 1,                $
                    FILENAME    : '',               $
                    DEFNAME     : 'paper.txt',      $
                    MESSAGE     : ' ',              $
                    out_type    : 0,                $
                    out_pass    : '',               $
                    out_defpass : userroot+'/pass/',$
                    out_root    : inval.userrootin, $
                    out_year    : '',               $
                    out_prefix  : '',               $
                    out_seq     : ''                }

    endelse
    procval = {NEW:-1}
    
    
                   
                ;***********************************************
                ;**** Make sure we are running v5 or above. ****
                ;***********************************************

    thisRelease = StrMid(!Version.Release, 0, 1)
    IF thisRelease LT '5' THEN BEGIN
       message = 'Sorry, ADAS403 requires IDL 5 or higher' 
       tmp = popup(message=message, buttons=['Accept'],font=font_large)
       goto, LABELEND
    ENDIF


LABEL100:

    ; These are fixed for each run
    
    outval.out_root = inval.userrootin
    outval.out_seq  = inval.seq
    outval.appbut   = -1
    
                ;********************************************
                ;**** Invoke user interface widget for   ****
                ;**** Data file selection                ****
                ;********************************************


    rep=adas403_in(inval, dataclasses, $
                   FONT_LARGE=font_large, FONT_SMALL = font_small)
 
    if rep eq 'CANCEL' then goto, LABELEND


                ;************************************************
                ;**** Probe the file selection and show what ****
                ;**** was found. Pass back the info for      ****
                ;**** possible later processing.             ****
                ;************************************************


    rep=adas403_proc(inval, dataclasses, procval,      $
                     match_report,                     $
                     FONT_LARGE=font_large, FONT_SMALL = font_small)
    
    
    if rep eq 'MENU'   then goto, LABELEND
    if rep eq 'CANCEL' then goto, LABEL100


LABEL300:

                ;************************************************
                ;**** Communicate with d4spf1 in fortran and ****
                ;**** invoke user interface widget for       ****
                ;**** Output options                         ****
                ;************************************************

                ;**** Create header for output. ****

    date   = xxdate()
    header = adasrel+adasprog+' DATE: '+date(0)+' TIME: '+date(1)

    rep=adas403_out(procval, outval,  dataclasses,                   $
                    header, match_report, bitfile,                   $
                    FONT_LARGE=font_large, FONT_SMALL = font_small)
    
   
    if rep eq 'MENU'   then goto, LABELEND
    if rep eq 'CANCEL' then goto, LABEL100

                ;**** Back for more output options ****

    GOTO, LABEL300

LABELEND:


                ;**** Save user defaults ****

    save, inval,  outval, filename=deffile


END
