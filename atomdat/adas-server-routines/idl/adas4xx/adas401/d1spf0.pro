; Copyright (c) 1995, Strathclyde University           
; SCCS info: Module @(#)$Header: /home/adascvs/idl/adas4xx/adas401/d1spf0.pro,v 1.3 2004/07/06 13:12:26 whitefor Exp $ Date $Date: 2004/07/06 13:12:26 $
;+
; PROJECT:
;       ADAS IBM MVS to UNIX conversion
;
; NAME:
;	D1SPF0
;
; PURPOSE:
;	IDL user interface and communications with ADAS401 FORTRAN
;	process via pipe.
;
; EXPLANATION:
;	Firstly this routine invokes the part of the user interface
;	used to select the input dataset for ADAS401.  When the user's
;	interactions are complete this routine communicates with the
;	ADAS401 FORTRAN application via a UNIX pipe.  Communications
;	are to the FORTRAN subroutine D1SPF0.
;
; USE:
;	The use of this routine is specific to ADAS401, see adas401.pro.
;
; INPUTS:
;	PIPE	- The IDL unit number of the bi-directional pipe to the
;		  ADAS401 FORTRAN process.
;
;	VALUE	- A structure which determines the initial settings of
;		  the dataset selection widget.  The initial value is
;		  set in adas401.pro.  VALUE is passed un-modified
;		  through to cw_adas401_in.pro, see that routine for a full
;		  description.
;
;	SEARCH	- A structure containing the initial settings of the
;		  file search widget. The initial value is set in
;		  adas401.pro. SEARCH is passed unmodified through to
;		  cw_adas401_in.pro, see that routine for a full
;		  description.
;
; OPTIONAL INPUTS:
;	None
;
; OUTPUTS:
;	VALUE	- On output the structure records the final settings of
;		  the dataset selection widget if the user pressed the
;		  'Done' button otherwise it is not changed from input.
;
;	SEARCH	- On output the structure records the final settings of
;                 the file search widget if the user pressed the
;                 'Done' button otherwise it is not changed from input.
;
;	DSFULL	- String; The full system file name of the emitting ion
;		  dataset selected by the user for processing.
;
;	REP	- String; Indicates whether the user pressed the 'Done'
;		  or 'Cancel' button on the interface.  The action is
;		  converted to the strings 'NO' and 'YES' respectively
;		  to match up with the existing FORTRAN code.  In the
;		  original IBM ISPF interface REP was the reply to the
;		  question 'DO YOU WANT TO END PROGRAM EXECUTION'.
;
;	CLASSVAL- String; Includes the class as extracted from the name
;		  of the file the user has selected. Used as a label on
;		  the processing screen.
;
;	YEARVAL - String; Includes the year of the data file as extracted
;		  from the the name of the file the user has selected.
;		  Used as a label on the processing screen.
;
;	ISOVAL  - String; Includes the element symbol (extracted from the
;		  file name) and the number of electrons which goes along
;		  with this symbol - calculated by routine i4eiz0.pro.
;                 Used as a label on the processing screen.
;
;	INDEXVAL- String; Includes the indices which are extracted from the
;		  name of partial data files. Used as a label on the
;		  processing screen.
;
;	FILEVAL - String; Shows whether file is partial or standard.
;                 Used as a label on the processing screen.
;	
;	NEL	- Int; Number of electrons - calculated from the symbol
;		  used in the file name by the routine i4eiz0.pro
;
;	IPSYSDUM- Int; The spin system index for a partial file.
;
;	CELEM	- String; The element symbol extracted from the file name.
;
;	CHEAD	- String; Heading to be used by FORTRAN and plotting.
;
;	LPARTL  - Int; Flag: 0=Standard file, 1=Partial file.
;
;	DATACLASS String; The three letter code representing the dataclass
;		  of the file - extracted from the file name.
;
;	YEAR	- String; The year of the data file - extracted from the file
;		  name.
;
;	IPRNTDUM- Int; The parent system index for a partial file.
;
;	ISWIT	- Int; Code showing which dataclass has been chosen. See
;		  FORTRAN programs for more details. A value of -1
;		  means that the file name is not valid.
;
; OPTIONAL OUTPUTS:
;	None
;
; KEYWORD PARAMETERS:
;	FONT	- Supplies the font to be used for the interface widgets.
;
; CALLS:
;	ADAS401_IN		Pops-up the dataset selection widget.
;	I4EIZ0		Converts element symbol into nuclear charge.
;
; SIDE EFFECTS:
;	This routine communicates with the ADAS401 FORTRAN process
;	via a UNIX pipe.
;
; CATEGORY:
;	Adas system.
;
; WRITTEN:
;       Tim Hammond, Tessella Support Services plc, 20/07/1995
;
; MODIFIED:
;	1.1	Tim Hammond	
;		First release
;	1.2	Tim Hammond 
;		Added a check for whether a member prefix is in
;		the filename when the filename is broken down
;		and used to extract information.
;	1.3	Tim Hammond
;		Modified check used to find member prefix as for
;		data classes PLS and PLT it is possible for there
;		to be 2 or 3 # symbols in the filename rather
;		than simply the one previously assumed.
;
; VERSION:
;	1.1	20-07-95      
;	1.2	24-10-95
;	1.3	15-02-96
;
;-
;-----------------------------------------------------------------------------

PRO d1spf0, pipe, value, search, dsfull, rep, classval, yearval, isoval,$
            indexval, fileval, nel, ipsysdum, celem, chead, lpartl, 	$
            dataclass, year, iprntdum, iswit,				$
            FONT=font


                ;**** Set defaults for keywords ****

    IF NOT (KEYWORD_SET(font)) THEN font = ''


		;**********************************
		;**** Pop-up input file widget ****
		;**********************************

    adas401_in, value, search, action, WINTITLE = 'ADAS 401 INPUT',	$
	        TITLE = 'Input Iso-electronic Master File', FONT = font,$
                outvalue

		;******************************************
		;**** Act on the event from the widget ****
		;******************************************
		;**** There are only two possible events ****
		;**** 'Done' and 'Cancel'.               ****
		;********************************************

    if action eq 'Done' then begin
        rep = 'NO'
    endif else begin
        rep = 'YES'
    endelse
    printf, pipe, rep

		;**** Construct dataset name ****

    if rep eq 'NO' then begin
        dsfull = outvalue.value.inset.rootpath+outvalue.value.inset.file
        value = outvalue.value.inset

		;**** Extract new value of search structure from outvalue ****

        search = outvalue.value.searchrange
	

		;**** Check datafile name is valid ****

        name401_check, dsfull, result
        printf, pipe, result


		;*******************************
		;**** Write data to fortran ****
		;*******************************

        if result eq 1 then begin

            printf, pipe, dsfull

		;***************************************************
		;**** Construct titles for processing screen    ****
		;***************************************************

            classval = 'Class     :                                         '+$
                       '                     '
            yearval  = 'Year      :                                         '+$
                       '                     '
            fileval  = 'File type :                                         '+$
                       '                     '
            isoval =   'Symbol              :          Number of electrons :'+$
                       '                     '

    		;**** What class is the data? ****

            iswit = 0
            dstart = strpos(dsfull, 'adf') + 6
            dataclass = strmid(dsfull, dstart, 3)
            printf, pipe, dataclass
            CASE dataclass OF
                'acd' : begin
                      classstring = 'ACD - Recombination coefficients'
                      chead = 'Recombination coefficients'
                      strput, classval, classstring, 12
                      iswit = 1
                 end
	        'scd' : begin
	              classstring = 'SCD - Ionisation coefficients'
	              chead = 'Ionisation coefficients'
                      strput, classval, classstring, 12
                      iswit = 2
 	         end
	        'ccd' : begin
	              classstring = 'CCD - Recombination coefficients: charge'+$
			            ' exchange'
	              chead = 'Recombination coefficients: Charge exchange'
                      strput, classval, classstring, 12
                      iswit = 3
                 end
                'prb' : begin
                      classstring = 'PRB - Power coefficients: recombination-'+$
			            'bremsstrahlung'
	              chead = 'Power coefficients: Recombination-Bremsstrahlung'
                      strput, classval, classstring, 12
                      iswit = 4
                 end
                'prc' : begin
                     classstring = 'PRC - Power coefficients: charge-exchange'+$
			            ' recombination'
	             chead = 'Power coefficients: Charge-exchange recombination'
                      strput, classval, classstring, 12
                      iswit = 5
                 end
                'qcd' : begin
                      classstring = 'QCD - Cross-coupling coefficients'
	              chead = 'Cross-coupling coefficients'
                      strput, classval, classstring, 12
	              iswit = 6
                 end
                'xcd' : begin
                      classstring = 'XCD - Cross-coupling coefficients: parent'
 	              chead = 'Cross-coupling coefficients: parent'
                      strput, classval, classstring, 12
	              iswit = 7
                 end
                'plt' : begin
                      classstring = 'PLT - Line power coefficients: total'
	              chead = 'Line power coefficients: total'
                      strput, classval, classstring, 12
                      iswit = 8
                 end
                'pls' : begin
                      classstring = 'PLS - Line power coefficients: specific'
	              chead = 'Line power coefficients: specific'
                      strput, classval, classstring, 12
                      iswit = 9
                 end
                ELSE  : begin
                      strput, classval, 'UNKNOWN', 12
                      chead = 'UNKNOWN'
                 end
            END
            printf, pipe, chead
    
		;**** What year is the data from? ****

            dstart = dstart + 3
            year = strmid(dsfull, dstart, 2)
            yearchk = byte(year)
            if (max(yearchk) gt 57) or (min(yearchk) lt 48) then begin
                strput, yearval, 'UNKNOWN', 12
            endif else begin
                strput, yearval, string(year), 12
            endelse
            printf, pipe, strtrim(string(year),2)

		;**** What sort of file is it? ****

            preftest = strmid(dsfull, dstart, 20)
            preflag = strpos(preftest, '#')
            if (preflag(0) ne 2) then begin
                dstart = dstart + 10
	    endif else begin
		dstart = dstart + 13
	    endelse
            filechk = strmid(dsfull, dstart, 2)
            filebyt = byte(filechk)
            if (filebyt(0) eq 46) or (filebyt(1) eq 46) then begin
                strput, fileval, 'Standard', 12
                lpartl = 0
            endif else begin
                strput, fileval, 'Partial', 12
                lpartl = 1
            endelse

		;**** What's the symbol? ****

            dstart = strpos(dsfull, '_') + 1
            symchk = strmid(dsfull, dstart, 2)
            symbt  = byte(symchk)
            if ((symbt(1) le 57) and (symbt(1) ge 48)) 			$
	    or (symbt(1) eq 46) or (symbt(1) eq 35) then begin
                symstring = strupcase(strmid(symchk, 0, 1));Single letter symbol
            endif else begin
                symstring = strupcase(strmid(symchk, 0, 1)) + 		$
                strmid(symchk, 1, 1)
            endelse
            printf, pipe, symstring
            strput, isoval, symstring, 22

		;**** Convert the symbol into number of electrons ****

            i4eiz0, symstring, nel
            strput, isoval, strcompress(string(nel), /remove_all), 53
            celem = strcompress(symstring, /remove_all)
    
		;**** What about the indices? ****

            if (lpartl eq 1) then begin
             indexval = '1st (upper) index   :          2nd (lower) index   :'+$
                        '                     '
                instart = strpos(dsfull, '.dat')
                indexchk = strmid(dsfull, (instart-2), 2)
                indexbyt = byte(indexchk)
                if ((indexbyt(0) le 57) and (indexbyt(0) ge 48)) then begin
                    iprntdum = fix(string(indexbyt(0)))
	        endif else begin
	            iprntdum = 0
	        endelse
	        if ((indexbyt(1) le 57) and (indexbyt(1) ge 48)) then begin
                    ipsysdum = fix(string(indexbyt(1)))
                endif else begin
                    ipsysdum = 0
                endelse
                strput, indexval, string(indexbyt(0)), 22
                strput, indexval, string(indexbyt(1)), 53
            endif else begin
                indexval = ''
                ipsysdum = 0
	        iprntdum = 0
            endelse
        endif else begin
	    iswit = -1
        endelse
                ;****************************************************
                ;**** Write other, associated information on the ****
		;**** data file to FORTRAN			 ****
		;****************************************************

        printf, pipe, iswit
        if (iswit ge 0) then begin		;only write if valid file
            printf, pipe, lpartl
            printf, pipe, iprntdum
            printf, pipe, ipsysdum
            printf, pipe, nel
        endif

    endif
             
END
