; Copyright (c) 1995, Strathclyde University
; SCCS info: Module @(#)$Header: /home/adascvs/idl/adas4xx/adas401/cw_adas401_in.pro,v 1.4 2004/07/06 12:43:03 whitefor Exp $	Date  $Date: 2004/07/06 12:43:03 $
;+
; PROJECT:
;       ADAS IBM MVS to UNIX conversion
;
; NAME:
;	CW_ADAS401_IN()
;
; PURPOSE:
;	Data file selection for a single input dataset.
;
; EXPLANATION:
;	This function creates a compound widget consisting of the compound
;	widget cw_adas4xx_infile.pro, the compound widget cw_dataset
;	a 'Cancel' button, a 'Done' button, a 'Search' button and
;	a button to allow the browsing of the selected data file comments.
;	The browsing and done buttons are automatically de-sensitised
;	until a valid input dataset has been selected.
;
;	The value of this widget is the settings structure of the
;	cw_adas4xx_infile widget plus the structure of the cw_dataset
;	widget. 
;
; USE:
;	See routine adas401_in.pro for an example.
;
; INPUTS:
;	PARENT	- Long integer; the ID of the parent widget.
;
; OPTIONAL INPUTS:
;	None
;
; OUTPUTS:
;	This function returns the ID of the compound widget created.
;
;	OUTVALUE- A structure which combines the final values of the
;		  two structures VALUE and SEARCHVAL.
;
; OPTIONAL OUTPUTS:
;	None
;
; KEYWORD PARAMETERS:
;	VALUE	- A structure which determines the initial settings of
;                 the dataset selection widget cw_adas4xx_infile.  The
;		  structure must be;
;		  {ROOTPATH:'', FILE:'', CENTROOT:'', USERROOT:'' }
;		  The elements of the structure are as follows;
;
;		  ROOTPATH - Current data directory e.g '/usr/fred/adas/'
;		  FILE     - Current data file in ROOTPATH e.g 'input.dat'
;		  CENTROOT - Default central data store e.g '/usr/adas/'
;		  USERROOT - Default user data store e.g '/usr/fred/adas/'
;
;		  The data file selected by the user is obtained by
;		  appending ROOTPATH and FILE.  In the above example
;		  the full name of the data file is;
;		  /usr/fred/adas/input.dat
;
;		  Path names may be supplied with or without the trailing
;		  '/'.  The underlying routines add this character where
;		  required so that USERROOT will always end in '/' on
;		  output.
;
;		  The default value is;
;		  {ROOTPATH:'./', FILE:'', CENTROOT:'', USERROOT:''}
;		  i.e ROOTPATH is set to the user's current directory.
;
;	SEARCHVAL A structure which determines the initial settings of the
;		  second dataset selection widget cw_dataset. The structure
;		  must be:
;		  {CLASS:0, YEAR:'          ', SYMBOL:'          ',	$
;		   FILE:0 , FIRST:0          , SECOND:0,		$
;		   MEMBER:'          '}
;                 The elements of the structure are as follows;
;
;                 CLASS    - The index of the class of the dataset being
;			     searched for (this corresponds to the FORTRAN
;			     variable ISWIT (-1) - see FORTRAN codes for
;			     details.
;		  YEAR	   - A string representing the year of datafile to
;			     be searched for.
;		  SYMBOL   - A string representing the element symbol of the
;			     datafiles to be searched for.
;		  FILE     - Flag indicating whether the file is of type
;			     STANDARD(1) or PARTIAL(0).
;		  FIRST	   - Integer: the first index for a partial file.
;		  SECOND   - Integer: the second index for a partial file.
;		  MEMBER   - String: An optional member prefix for the
;		       	     datafiles to be searched for.
;
;		  The default value of the structure is given above.
;
;	TITLE	- The title to be included in the input file widget, used
;                 to indicate exactly what the required input dataset is,
;                 e.g 'Input COPASE Dataset'
;
;	UVALUE	- A user value for the widget.  Defaults to 0.
;
;	FONT	- Supplies the font to be used for the interface widgets.
;		  Defaults to the current system font.
;
; CALLS:
;	XXTEXT		Pop-up window to browse dataset comments.
;	CW_ADAS4XX_INFILE	Dataset selection widget.
;	CW_DATASET	File search widget
;	FILE_ACC	Determine filetype and access permissions.
;	I4EIZ0		Converts element symbol to atomic number.
;	NAME401_CHECK	Checks whether a filename is valid or not
;	XXELEM		Converts atomic number to element symbol.
;	CW_LIST_SELECT  Pops up a list of files found when a search done.
;
;	See side effects for other related widget management routines.
;
; SIDE EFFECTS:
;	IN401_GET_VAL()	Widget management routine in this file.
;	IN401_EVENT()	Widget management routine in this file.
;
; CATEGORY:
;	Compound Widget
;
; WRITTEN:
;       Andrew Bowen, Tessella Support Services plc, 12-May-1993
;
; MODIFIED:
;	1.1	Tim Hammond (Tessella Support Services plc)
;		First release. Total rewrite of cw_adas205_in.pro
;		to include new search widget option.
;	1.2	Tim Hammond
;		Changed call of cw_adas401_infile to more general
;		cw_adas4xx_infile.
;	1.3	Tim Hammond
;		Removed calls to obsolete CW_LOADSTATE and CW_SAVESTATE
;		and added/modified sections of the code which now allow
;		it to deal with partial files of type PLS and PLT. This
;		was previously impossible due to the presence of '##'
;		in the filename rather than two indices which is the
;		case for all other data classes.
;	1.4	William Osborn
;		Removed xsize=n keyword from widget_label command.
;		The dynlabel procedure does not override these
;		assignments and they were taken as being values in pixels
;		on some machines.
;
; VERSION:
;       1.1	07-09-95
;	1.2	25-09-95
;	1.3	15-02-96
;	1.4	01-08-96
;
;-
;-----------------------------------------------------------------------------

FUNCTION in401_get_val, id

                ;**** Return to caller on error ****

    ON_ERROR, 2

                 ;***************************************
                 ;****     Retrieve the state        ****
                 ;**** Get first_child widget id     ****
                 ;**** because state is stored there ****
                 ;***************************************

    first_child = widget_info(id, /child)
    widget_control, first_child, get_uvalue = state

		;**** Get settings ****

    widget_control, state.fileid, get_value=inset
    widget_control, state.dataid, get_value=searchrange

    searchstruc = {inset	:	inset,				$
 	           searchrange	:	searchrange			}

    widget_control, first_child, set_uvalue=state, /no_copy
 
    RETURN, searchstruc      

END

;-----------------------------------------------------------------------------

FUNCTION in401_event, event

                ;**** Base ID of compound widget ****

    parent=event.handler

                ;**********************************************
                ;**** Retrieve the user value state        ****
                ;**** Get id of first_child widget because ****
                ;**** user value "state" is stored there   ****
                ;**********************************************

    first_child = widget_info(parent, /child)
    widget_control, first_child, get_uvalue=state, /no_copy

		;*********************************
		;**** Clear previous messages ****
		;*********************************

    widget_control, state.messid, set_value=''

		;**** Default output no event ****

    new_event = 0L

                ;************************
                ;**** Process events ****
                ;************************

    CASE event.id OF

		;***********************************
		;**** Event from file selection ****
		;***********************************

        state.fileid: begin
	    if event.action eq 'newfile' then begin
	        widget_control, state.doneid, /sensitive
	        widget_control, state.browseid, /sensitive
	    endif else begin
	        widget_control, state.doneid, sensitive=0
	        widget_control, state.browseid, sensitive=0
	    endelse
	end

		;***********************
		;**** Browse button ****
		;***********************

        state.browseid: begin

		;**** Get latest filename ****

	    widget_control, state.fileid, get_value=inset
	    filename = inset.rootpath+inset.file

		;**** Invoke comments browsing ****

	    xxtext, filename, font=state.font
	end

		;***********************
		;**** Cancel button ****
		;***********************

        state.cancelid: new_event = {ID:parent, TOP:event.top, 		$
				     HANDLER:0L, ACTION:'Cancel'}

		;*********************
		;**** Done button ****
		;*********************

        state.doneid: begin
            widget_control, state.dataid, get_value=searchrange
            state.outvalue.searchval.class = searchrange.class
            state.outvalue.searchval.year = searchrange.year
            state.outvalue.searchval.symbol = searchrange.symbol
	    state.outvalue.searchval.file = searchrange.file
	    state.outvalue.searchval.first = searchrange.first
	    state.outvalue.searchval.second = searchrange.second
	    state.outvalue.searchval.member = searchrange.member

	    new_event = {ID:parent, TOP:event.top, HANDLER:0L, ACTION:'Done'}

        end

		;***********************
		;**** Search button ****
		;***********************

        state.searchid: begin

		;**** Get values to be used in search ****

            widget_control, state.dataid, get_value=searchrange

		;**** Test validity of the values ****
    
	    errormess = ''
	
		;**** Check the year ****

	    year = searchrange.year
	    if (strlen(year) ne 2 and (year ne '')) then begin
	        errormess = '**** You have specified the year incorrectly ****'
	    endif else begin
	        yearbyt = byte(year)
	        if ((min(yearbyt) lt 48) or (max(yearbyt) gt 57) and 	$
                (year ne '')) then begin
		    errormess = 					$
		    '**** You have specified the year incorrectly ****'
	        endif
	    endelse

		;**** Check the symbol ****

	    if (errormess eq '') then begin
	        symb = strcompress(searchrange.symbol, /remove_all)
	        i4eiz0, symb, atnumber
	        if (atnumber eq 0 and symb ne '') then begin
		    errormess = 					$
		    '**** You have specified an invalid element ****'
	        endif
	    endif

		;**** Check the member prefix ****

	    if (errormess eq '') then begin
	        mem = strcompress(searchrange.member, /remove_all)
	        if (strlen(mem) ne 2 and (mem ne '')) then begin
		    errormess = 					$
		    '**** You have entered an invalid member prefix ****'
	        endif
	    endif

		;**** if error then give message ****

	    if errormess ne '' then begin
	        action = popup(message=errormess, buttons=[' OK '],	$
			       font=state.font)
	    endif else begin
	
		;**** No error so begin the search ****

	        fileflag = 1		;Set to 1 if there are files
					;that fit, otherwise 0

		;**** Switch to the appropriate directory ****

	        widget_control, state.fileid, get_value=inset
	        dirname = inset.rootpath
	        listcom = 'ls -l '+dirname+'.'
	        listcom2 = 'ls '+dirname+'.'
	        spawn, listcom, full_list		;get long-style list
	        spawn, listcom2, short_list		;get short-style list
	        no_files = n_elements(full_list)	;size of listing
	        if (no_files eq 1) then fileflag = 0;no files in the directory

		;**** Pick out only the directories ****

	        full_list = strcompress(full_list, /remove_all)
	        dir_indices = where(strpos(full_list, 'd') eq 0) - 1
	        if (dir_indices(0) eq -1) then begin
		    fileflag = 0
	        endif else begin
	            dir_list = short_list(dir_indices)
	            no_dirs = n_elements(dir_list)
	        endelse

		;*****************************************************
		;**** Compile a master file list by going through ****
		;**** each directory and getting a list of files  ****
		;**** therein.					  ****
		;*****************************************************

	        if fileflag eq 1 then begin
		    for i=0, (no_dirs-1) do begin
		        spawn, 'ls -l '+dirname+dir_list(i), all_files
		        spawn, 'ls '+dirname+dir_list(i), all_files_short
		        all_files = strcompress(all_files, /remove_all)
		        file_indices = where((strpos(all_files,'d') ne 0) $
				       and (strpos(all_files,'t') ne 0)) -1
		        if (file_indices(0) ne -1) then begin
		            file_list = all_files_short(file_indices)
			    if (i eq 0) then begin
			        files = file_list
			    endif else begin
			        files = [files, file_list]
			    endelse
		        endif
		    endfor
	        endif
	        no_files = n_elements(files)
	        if (no_files eq 0) then fileflag = 0

		;**** Remove all files with invalid names ****

	        if (fileflag eq 1) then begin
		    testfiles = strarr(no_files)
		    goodindices = replicate(1, no_files)
	            for i=0, (no_files-1) do begin
		        hashpoint = strpos(files(i), '#')
		        if hashpoint(0) ne 2 then begin
		            testfiles(i) = '/adf10/' + strmid(files(i),0,5) +  $
			    '/' + files(i)
		        endif else begin
			    testfiles(i) = '/adf10/' + strmid(files(i),3,5) +  $
			    '/' + files(i)
		        endelse
		        name401_check, testfiles(i), result
		        goodindices(i) = result
	            endfor
		    goodfiles = where(goodindices eq 1)
		    if (goodfiles(0) ne -1) then begin
		        files = files(goodfiles)
		    endif else begin
			fileflag = 0
		    endelse
	        endif

		;**** Start the wittling down of the list ****

	    	;**** Modify check positions depending on ****
		;**** whether a member prefix is assigned ****

	        if mem eq '' then begin
		    pos = [0,3,6]
		    shift = 3
	        endif else begin
		    pos = [3,6,9]
		    shift = 0
	        endelse
	
		;**** First check the dataclass ****

	        classes = ['acd','scd','ccd','prb','prc',		$
                           'qcd','xcd','plt','pls']
	        if (fileflag eq 1) then begin
	            class = searchrange.class
	            new_indices = where((strpos(files, classes(class)) 	$
                                         eq pos(0))			$
		    or ((strpos(files, classes(class)) eq pos(0)+shift)))
		    if (new_indices(0) ne -1) then begin
	                files = files(new_indices)
		    endif else begin
		        fileflag = 0
		    endelse
	        endif

		;**** Check the member ****

	        if (fileflag eq 1) then begin
		    if (mem ne '') then begin
		        new_indices = where(strpos(files, mem) eq 0)
		        if (new_indices(0) ne -1) then begin
			    files = files(new_indices)
		        endif else begin
			    fileflag = 0
		        endelse
		    endif
	        endif

		;**** Check the year ****

	        if (fileflag eq 1) then begin
		    if (year ne '') then begin
		        new_indices = where((strpos(files, year) eq pos(1))$
		        or ((strpos(files, year) eq pos(1)+shift)))
		        if (new_indices(0) ne -1) then begin
                            files = files(new_indices)
                        endif else begin
                            fileflag = 0
                        endelse
                    endif
	        endif

		;**** Check the symbol ****

	        if (fileflag eq 1) then begin
		    if (symb ne '') then begin
		        no_files = n_elements(files)
		        good_files = intarr(no_files)
		        checkstring = '_'+symb
		        for i=0, (no_files-1) do begin
		 	    pos_check = strpos(files(i), checkstring)
		    	    pos_len = 1+strlen(symb)
		            if pos_check gt -1 then begin
			        checkstring2 = strmid(files(i),pos_check+$
			                              pos_len,1)
			        chkbyte = byte(checkstring2)
			        if (min(chkbyte) gt 57) then begin
				    good_files(i) = 0
			        endif else begin
				    good_files(i) = 1
			        endelse
			    endif
		        endfor
		        new_indices=where(good_files eq 1)
                        if (new_indices(0) ne -1) then begin
                            files = files(new_indices)
                        endif else begin
                            fileflag = 0
                        endelse
                    endif
                endif

		;**** Check indices according to filetype ****
	
	        if (fileflag eq 1) then begin
	            if (searchrange.file eq 1) then begin	;standard type
		        if (symb ne '') then begin
		            checkstring=symb+'.'
		            new_indices = where((strpos(files, checkstring) $
                            eq pos(2))					$
		            or ((strpos(files, checkstring) eq pos(2)+shift)))
		        endif else begin
			    no_files = n_elements(files)
			    good_files = intarr(no_files)
			    for i=0, (no_files-1) do begin
			        pos_check=strpos(files(i),'.')
			        pos_strng=strmid(files(i),pos_check-2,2)	
			        pos_byte=byte(pos_strng)
			        if ((min(pos_byte) ge 48 and min(pos_byte) $
                                le 57) or (min(pos_byte) eq 35)) then begin
				    good_files(i)=0
			        endif else begin
				    good_files(i)=1
			        endelse
			    endfor
		            new_indices=where(good_files eq 1)
		        endelse
		        if (new_indices(0) ne -1) then begin
                            files = files(new_indices)
                        endif else begin
                            fileflag = 0
                        endelse
                    endif else begin			;partial type
		        first = searchrange.first
		        sec = searchrange.second
		        if (first ne 0 and sec ne 0) then begin
			    if (symb ne '') then begin
		                checkstring=symb+strtrim(string(first),2)    $
                                           +strtrim(string(sec),2)+'.'
                                new_indices = where((strpos(files, checkstring)$
                                eq pos(2)) or ((strpos(files, checkstring) eq  $
			        pos(2)+shift)))
		            endif else begin
			        no_files = n_elements(files)
			        good_files = intarr(no_files)
                                for i=0, (no_files-1) do begin
                                    pos_check=strpos(files(i),'.')
                                    pos_strng=strmid(files(i),pos_check-2,2)
				    checkstring=strtrim(string(first),2) +$
				    strtrim(string(sec),2)
				    if pos_strng eq checkstring then begin
				        good_files(i) = 1
				    endif else begin
				        good_files(i) = 0
				    endelse
			        endfor
			        new_indices=where(good_files eq 1)
			    endelse
                            if (new_indices(0) ne -1) then begin
                                files = files(new_indices)
                            endif else begin
                                fileflag = 0
                            endelse
		        endif else if (first ne 0 and sec eq 0) then begin
			    no_files = n_elements(files)
                            good_files = intarr(no_files)
                            for i=0, (no_files-1) do begin
                                pos_check=strpos(files(i),'.')
                                pos_strng=strmid(files(i),pos_check-2,1)
			        checkstring=strtrim(string(first),2)
			        if pos_strng eq checkstring then begin
                                    good_files(i) = 1
                                endif else begin
                                    good_files(i) = 0
                                endelse
                            endfor
                            new_indices=where(good_files eq 1)
                            if (new_indices(0) ne -1) then begin
                                files = files(new_indices)
                            endif else begin
                                fileflag = 0
                            endelse
		        endif else if (first eq 0 and sec ne 0) then begin
                            no_files = n_elements(files)
                            good_files = intarr(no_files)
                            for i=0, (no_files-1) do begin
                                pos_check=strpos(files(i),'.')
                                pos_strng=strmid(files(i),pos_check-1,1)
                                checkstring=strtrim(string(sec),2)
                                if pos_strng eq checkstring then begin
                                    good_files(i) = 1
                                endif else begin
                                    good_files(i) = 0
                                endelse
                            endfor
                            new_indices=where(good_files eq 1)
                            if (new_indices(0) ne -1) then begin
                                files = files(new_indices)
                            endif else begin
                                fileflag = 0
                            endelse
		        endif else begin
                            no_files = n_elements(files)
                            good_files = intarr(no_files)
                            for i=0, (no_files-1) do begin
                                pos_check=strpos(files(i),'.')
                                pos_strng=strmid(files(i),pos_check-2,2)
				if class lt 7 then begin
			            pos_byte=byte(pos_strng)
			            if (min(pos_byte) le 53 and 	$
				    max(pos_byte) ge 48) then begin
			                good_files(i) = 1
                                    endif else begin
                                        good_files(i) = 0
                                    endelse
				endif else begin
				    if pos_strng eq '##' then begin
                                        good_files(i) = 1
                                    endif else begin
                                        good_files(i) = 0
                                    endelse
				endelse
                            endfor
                            new_indices=where(good_files eq 1)
                            if (new_indices(0) ne -1) then begin
                                files = files(new_indices)
                            endif else begin
                                fileflag = 0
                            endelse
		        endelse
		    endelse
                endif

		;**** Search complete - popup information ****

	        if fileflag eq 0 then begin
		    action = popup(message='**** Sorry - No data files match'+$
			           ' the given parameters ****',	  $
			           buttons=['  OK  '], font=state.font,	  $
			           title='Information:-')
	        endif else begin

		;**** Construct information for the popup window ****
		
		    headings=['   SEQUENCE       ELECTRONS   TYPE     INDEX'+$
		              '      PREFIX   YEAR   ',			$
			      'Name      Symbol                     Parent '+$
			      'Spin               ']
		    no_files = n_elements(files)
		    choices = strarr(no_files)
		    for i=0, (no_files-1) do begin

		;**** Get the element symbol and name ****

		        elname = '            '
		        elpos = strpos(files(i),'_')
		        elcheck = strmid(files(i), elpos+2, 1)
		        elbyte = byte(elcheck)
		        if (elbyte(0) le 57 and elbyte(0) ge 48) or	$
                        elbyte(0) eq 46 or elbyte(0) eq 35 then begin
			    ellength = 1
		        endif else begin
			    ellength = 2
		        endelse
		        elsymb = strmid(files(i), elpos+1, ellength)
		        if ellength eq 1 then begin
			    choices(i) = choices(i) + elsymb + '        '
		        endif else begin
			    choices(i) = choices(i) + elsymb + '       '
		        endelse

		;**** Add the number of electrons ****

		        i4eiz0, elsymb, electrons
		        if electrons lt 10 then begin
			    choices(i) = choices(i) + 			$
                            strcompress(string(electrons), /remove_all)+'  '
		        endif else begin
			    choices(i) = choices(i) + 			$
                            strcompress(string(electrons), /remove_all)+' '
		        endelse

		;**** Convert number of electrons to element name ****

		        xxelem, electrons, element
		        strput, elname, element, 0
		        choices(i) = elname + choices(i)

		;**** Get the file type and indices ****

		        if searchrange.file eq 0 then begin
			    choices(i) = choices(i) + '    PARTIAL '
			    startpos= strpos(files(i), '.dat')
			    parent = strmid(files(i), startpos-2,1)
			    spin = strmid(files(i), startpos-1,1)
			    choices(i) = choices(i) +'   '+ 		$
			    strcompress(string(parent),/remove_all)	$
			    +'     '+strcompress(string(spin),/remove_all)
		        endif else begin
			    choices(i) = choices(i) + '     STANDARD   -    -' 
		        endelse

		;**** Get any member prefix ****

		        mempos = strpos(files(i), '#')
		        if mempos eq 2 then begin
			    choices(i) = choices(i) + '      '+		$
			    strmid(files(i),0,2)
		        endif else begin
			    choices(i) = choices(i) + '        '
		        endelse

		;**** Get the year ****

		        yrpos = strpos(files(i), '_')
		        yrval = strmid(files(i), yrpos-2, 2)
		        choices(i) = choices(i) + '     '+yrval
    
		    endfor

		;**** Pop up the selection window ****

		    index = 0
		    action = -1
		    cw_list_select, headings, choices, index, action,	$
				    font=state.font,			$
				    title='The following files match the'+$
				    ' given parameters:- ',		$
				    toptitle='Search results:-',	$
				    buttons=['Cancel','Select'], ysize=15
		    if action eq 1 then begin
		        widget_control, state.fileid, get_value=inset
		        memcheck = strpos(files(index), '#')
		        if memcheck ne 2 then begin
		            leader = strmid(files(index), 0, 5)
		        endif else begin
		            leader = strmid(files(index), 3, 5)
		        endelse
		        inset.file = leader+'/'+files(index)
		        widget_control, state.fileid, set_value=inset
	 	        widget_control, state.doneid, /sensitive
		        widget_control, state.browseid, /sensitive
		    endif
	        endelse
	    endelse
        end

        ELSE: new_event = 0L

    ENDCASE

                ;*********************************************
                ;*** make "state" available to other files ***
                ;*********************************************

    widget_control, first_child, set_uvalue=state, /no_copy
         
    RETURN, new_event

END

;-----------------------------------------------------------------------------

FUNCTION cw_adas401_in, parent, outvalue, VALUE=value, SEARCHVAL=search,  $
		        TITLE=title, UVALUE=uvalue, FONT=font

    IF (N_PARAMS() EQ 0) THEN MESSAGE, 'Must specify parent for cw_adas401_in'
    ON_ERROR, 2					;return to caller

		;**** Set defaults for keywords ****

    IF NOT (KEYWORD_SET(value)) THEN begin
        inset = {   ROOTPATH:'./', FILE:'', 				$
		    CENTROOT:'', USERROOT:'' }
    ENDIF ELSE BEGIN
        inset = {   ROOTPATH:value.rootpath, FILE:value.file, 		$
		    CENTROOT:value.centroot, USERROOT:value.userroot }
        if strtrim(inset.rootpath) eq '' then begin
            inset.rootpath = './'
        endif else if 							$
	strmid(inset.rootpath, strlen(inset.rootpath)-1,1) ne '/' then begin
            inset.rootpath = inset.rootpath+'/'
        endif
        if strmid(inset.file, 0, 1) eq '/' then begin
            inset.file = strmid(inset.file, 1, strlen(inset.file)-1)
        endif
    ENDELSE
    dumstr = strarr(1)
    dumstr(0) = '          '
    IF NOT (KEYWORD_SET(search)) THEN begin
	searchval = {class:0,						$
		     year:dumstr,					$
		     symbol:dumstr,					$
		     file:0,						$
		     first:0,						$
		     second:0,						$
		     member:dumstr}
    ENDIF ELSE BEGIN
	searchval = {class:search.class,				$
		     year:search.year,					$
		     symbol:search.symbol,				$
		     file:search.file,					$
		     first:search.first,				$
		     second:search.second,				$
		     member:search.member}
    ENDELSE
    outvalue = {value:inset,   searchval:searchval}
    IF NOT (KEYWORD_SET(title)) THEN title = ''
    IF NOT (KEYWORD_SET(uvalue)) THEN uvalue = 0
    IF NOT (KEYWORD_SET(font)) THEN font = ''

		;*********************************
		;**** Create the Input widget ****
		;*********************************

		;**** create base widget ****

    topbase = widget_base( parent, UVALUE = uvalue, 			$
			   EVENT_FUNC = "in401_event", 			$
			   FUNC_GET_VALUE = "in401_get_val", 		$
			   /COLUMN)

                ;******************************************************
                ;**** Create a dummy widget just to hold value of *****
                ;**** "state" variable so as not to get confused  *****
                ;**** with any other values. Adopt IDL practice   *****
                ;**** of using first child widget                 *****
                ;******************************************************

    first_child = widget_base(topbase)
    cwid = widget_base(first_child, /column)

		;*************************************
		;**** Input file selection widget ****
		;*************************************

    fileid = cw_adas4xx_infile(cwid,value=inset,                    	$
                               title=title,font=font)


		;**************************************
		;**** Second file selection widget ****
		;**************************************
 
    database = widget_base(cwid  ,/frame)
    cats = ['Iso-electronic master collisional-dielectronic class  :',	$
            '                                 Year of data         :',	$
            '               Iso-electronic sequence symbol         :',	$
	    '                                 Type of file         :',	$
	    '                                  First index         :',	$
            '                                 Second index         :',	$
            '                                Member prefix         :']
    values = strarr(7,9)
    values(0,*) = ['ACD - Recombination coefficients',			$
		   'SCD - Ionisation coefficients',			$
		   'CCD - Recombination coefficients: charge exchange',	$
	           'PRB - Power coefficients: recombination-Bremsstrahlung',$
		   'PRC - Power coefficients: charge-exchange recombination',$
		   'QCD - Cross-coupling coefficients',			$
		   'XCD - Cross-coupling coefficients: parent',		$
		   'PLT - Line power coefficients: total',		$
		   'PLS - Line power coefficients: specific']
    values(3,*) = ['Partial','Standard','','','','','','','']
    values(4,*) = ['Any','1','2','3','4','5','','','']
    values(5,*) = ['Any','1','2','3','4','5','','','']
    selectors = [1,0,0,1,1,1,0]
    dataid = cw_dataset(database, cats, values, selectors, font=font,	$
                        title="Search for input file from details:-")
    widget_control, dataid, set_value=searchval

		;*****************
		;**** Buttons ****
		;*****************

    base = widget_base(cwid,/row)

		;**** Browse Dataset button ****

    browseid = widget_button(base,value='Browse Comments',font=font)

		;**** Search button ****

    searchid = widget_button(base, value='Search',font=font)

		;**** Cancel Button ****

    cancelid = widget_button(base,value='Cancel',font=font)

		;**** Done Button ****

    doneid = widget_button(base,value='Done',font=font)


		;**** Error message ****

    messid = widget_label(parent,font=font,value='*')

		;******************************************************
		;**** Check filename and desensitise buttons if it ****
		;**** is a directory or it is a file without read  ****
		;**** access.					   ****
		;******************************************************

    filename = inset.rootpath+inset.file
    file_acc, filename, fileexist, read, write, execute, filetype
    if filetype ne '-' then begin
        widget_control, browseid, sensitive=0
        widget_control, doneid, sensitive=0
    endif else begin
        if read eq 0 then begin
            widget_control, browseid, sensitive=0
            widget_control, doneid, sensitive=0
        endif
    endelse

		;**** create a state structure for the widget ****

    new_state = { FILEID	:	fileid, 			$
		  BROWSEID	:	browseid, 			$
		  CANCELID	:	cancelid, 			$
		  DONEID	:	doneid, 			$
		  MESSID	:	messid, 			$
		  FONT		:	font, 				$
		  SEARCHID	:	searchid, 			$
		  DATAID	:	dataid, 			$
                  OUTVALUE	:	outvalue 			}

                ;**************************************
                ;**** Save initial state structure ****
                ;**************************************

    widget_control, first_child, set_uvalue=new_state, /no_copy

    RETURN, topbase

END

