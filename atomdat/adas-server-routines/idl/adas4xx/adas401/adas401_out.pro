; Copyright (c) 1995, Strathclyde University.
; SCCS info: Module @(#)$Header: /home/adascvs/idl/adas4xx/adas401/adas401_out.pro,v 1.3 2004/07/06 10:38:16 whitefor Exp $ Date $Date: 2004/07/06 10:38:16 $    
;+
; PROJECT:
;       ADAS IBM MVS to UNIX conversion
;
; NAME:
;	ADAS401_OUT
;
; PURPOSE:
;	IDL ADAS user interface, output options.
;
; EXPLANATION:
;	This routine creates and manages a pop-up window which allows
;	the user to select options and input data to control ADAS401
;	graphical and file output.
;
; USE:
;	This routine is the same as used for adas201.                  
;
; INPUTS:
;	VAL	- A structure which determines the initial settings of
;		  the output options widget.  The value is passed
;		  unmodified into cw_adas401_out.pro.
;
;		  See cw_adas401_out.pro for a full description of this
;		  structure.
;
;	DSFULL	- String; The full system file name of the input 
;		  dataset selected by the user for processing.
;
;       IZE     - Number of charge states in the input file.
;
;       IDE     - Number of reduced densities in the input file.
;
;       TEDA    - Temperatures in the input file.
;
;	Z1VALUES- String array containing all the possible charge
;		  states in the input data file - the user can
;		  select which are to be plotted.
;
;	Z1INDEX - Integer array containing flags for whether or not each
;		  set of input temperatures (for each charge state)
;		  should be plotted. A 1 means yes and a 0 no.
;	
;       BITFILE - String; the path to the dirctory containing bitmaps
;                 for the 'escape to series menu' button.
;
; OPTIONAL INPUTS:
;	None.
;
; OUTPUTS:
;	ACT	- String; Either 'Done', 'Cancel' or 'Menu' for the button the
;		  user pressed to terminate the output options window.
;
; OPTIONAL OUTPUTS:
;	None.
;
; KEYWORD PARAMETERS:
;	DEVLIST	- A string array;  Each element of the array is the
;		  name of a hardcopy graphics output device.  This
;		  list must mirror the DEVCODE list.  DEVLIST is the
;		  list which the user will see.
;		  e.g ['Post-Script','HP-PCL','HP-GL']
;
;	FONT	- Supplies the font to be used for the interface widgets.
;
; CALLS:
;	CW_ADAS401_OUT	Creates the output options widget.
;	DYNLABEL	Sets dynamic_resize keyword for label widgets.
;	XMANAGER
;	See side effects for widget management routine.
;
; SIDE EFFECTS:
;	This routine uses a common block OUT401_BLK to maintain its state.
;	ADAS401_OUT_EV	is included in this file and is called
;			indirectly from XMANAGER during widget
;			management.
;
; CATEGORY:
;	Widgets
;
; WRITTEN:
;       Tim Hammond, Tessella Support Services plc, 1-May-1995 
;
; MODIFIED:
;	1.1	Tim Hammond	
;		First release
;	1.2	Tim Hammond
;		Tidied code up
;	1.3	William Osborn
;		Added menu button code and dynlabel procedure
; VERSION:
;	1.1	08-09-95
;	1.2	08-09-95
;	1.3	09-07-96
;-
;-----------------------------------------------------------------------------


pro adas401_out_ev, event

    common out401_blk, action, value
	

		;**** Find the event type and copy to common ****

    action = event.action

    case action of

		;**** 'Done' button ****

	'Done'  : begin

			;**** Get the output widget value ****

     	     child = widget_info(event.id, /child)
	     widget_control, child, get_value=value 

                        ;*****************************************
			;**** Kill the widget to allow IDL to ****
			;**** continue and interface with     ****
			;**** FORTRAN only if there is work   ****
			;**** for the FORTRAN to do.          ****
			;*****************************************

	     if (value.grpout eq 1) or (value.texout eq 1) then begin
	        widget_control, event.top, /destroy
	     endif 
	end


		;**** 'Cancel' button ****

	'Cancel': widget_control, event.top, /destroy

                ;**** 'Menu' button ****

        'Menu': widget_control, event.top, /destroy

    endcase

END

;-----------------------------------------------------------------------------


pro adas401_out, val, dsfull, act, ide, teda, devlist=devlist, font=font, $
	         z1values, z1index, ize, bitfile

    common out401_blk, action, value

		;**** Copy value to common ****

    value = val

		;**** Set defaults for keywords ****

    if not (keyword_set(font)) then font = ''
    if not (keyword_set(devlist)) then devlist = ''

		;***************************************
		;**** Pop-up a new widget           ****
		;***************************************

                ;**** create base widget ****

    outid = widget_base(title='ADAS401 OUTPUT OPTIONS',xoffset=100,yoffset=1)

		;**** Declare output options widget ****

    cwid = cw_adas401_out(outid, dsfull, ide, teda, value=value,  	$
			devlist=devlist, font=font, z1values, z1index,	$
                        ize, bitfile )

		;**** Realize the new widget ****

    dynlabel, outid
    widget_control, outid, /realize

		;**** make widget modal ****

    xmanager, 'adas401_out', outid, event_handler='adas401_out_ev',	$
              /modal, /just_reg
 
		;**** Return the output value from common ****

    act = action
    val = value

END

