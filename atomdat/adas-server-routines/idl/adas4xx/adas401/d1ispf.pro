; Copyright (c) 1995, Strathclyde University 
; SCCS INFO : Module @(#)$Header: /home/adascvs/idl/adas4xx/adas401/d1ispf.pro,v 1.4 2004/07/06 13:11:45 whitefor Exp $ Date $Date: 2004/07/06 13:11:45 $                           
;
;+
; PROJECT:
;       ADAS IBM MVS to UNIX conversion
;
; NAME:
;	D1ISPF
;
; PURPOSE:
;	IDL user interface and communications with ADAS401 FORTRAN
;	process via pipe.
;
; EXPLANATION:
;	The routine begins by reading information from the ADAS401
;	FORTRAN process via a UNIX pipe.  Then part of the ADAS401
;	IDL user interface is invoked to determine how the user
;	wishes to process the input dataset.  When the user's
;	interactions are complete the information gathered with
;	the user interface is written back to the FORTRAN process
;	via the pipe.  Communications are to the FORTRAN subroutine
;	D1ISPF.
;
; USE:
;	The use of this routine is specific to ADAS401, see adas401.pro.
;
; INPUTS:
;	PIPE	- The IDL unit number of the bi-directional pipe to the
;		  ADAS401 FORTRAN process.
;
;	LPEND	- Integer, 0 if the user pressed the 'Done' button
;		  or 1 if the user pressed 'Cancel'.  Maps directly onto
;		  the logical variable LPEND in ADAS401 FORTRAN.
;
;	PROCVAL	- A structure which determines the initial settings of
;		  the processing options widget.  The initial value is
;		  set in adas401.pro.  If adas401.pro passes a blank 
;		  dummy structure of the form {NEW:-1} into PROCVAL then
;		  PROCVAL is reset to a default structure.
;
;		  The PROCVAL structure is;
;			procval = {                        	$
;                			new   : 0 ,             $
;                			title : '',             $
;                			lfsel : 0,              $
;                			tolval: 5,              $
;					ifout : 1,		$
;                                       tin   : temp_arr,       $
;                                       din   : dens_arr,       $
;                                       maxt  : 0,              $
;                                       maxd  : 0,              $
;					tvals : dblarr(3,ndtin),$
;					losel : 0,		$
;					z0val : '',		$
;					zval  : '',		$
;					z1val : ''		$
;              			}
;
;
;		  See cw_adas401_proc.pro for a full description of this
;		  structure.
;	
;
;	DSFULL	- String; The full system file name of the input 
;		  dataset selected by the user for processing.
;
;       BITFILE - String; the path to the dirctory containing bitmaps
;                 for the 'escape to series menu' button.
;
; OPTIONAL INPUTS:
;	None
;
; OUTPUTS:
;	PROCVAL	- On output the structure records the final settings of
;		  the processing selections widget if the user pressed the
;		  'Done' button, otherwise it is not changed from input.
;
;       GOMENU  - Int; flag - set to 1 if user has selected 'escape direct
;                 to series menu , otherwise 0
;
; OPTIONAL OUTPUTS:
;	None
;
; KEYWORD PARAMETERS:
;	FONT_LARGE - The name of a larger font.
;
;	FONT_SMALL - The name of a smaller font.
;
;	EDIT_FONTS - A structure of two fonts used in the table editor
;		     adas_edtab.pro, {font_norm:'',font_input:''}.
;
; CALLS:
;	ADAS401_PROC	Invoke the IDL interface for ADAS401 data
;		 	processing options/input.
;
; SIDE EFFECTS:
;	Two way communications with ADAS401 FORTRAN via the
;	bi-directional UNIX pipe.
;
; CATEGORY:
;	Adas system.
;
; WRITTEN:
;       Tim Hammond, Tessella Support Services plc, 20-Jul-1995
;
; MODIFIED:
;	1.1     Tim Hammond  
;		First release
;	1.2	Tim Hammond
;		Corrected value of ifout passed to FORTRAN
;	1.3	Tim Hammond
;		Ensured that new values of tvals are always put into
;		the procval structure (this could cause problems
;		for files with different numbers of temperatures
;		in them if omitted).
;	1.4	William Osborn
;		Added menu button code
;
; VERSION:
;	1.1	07-09-95
;	1.2	08-09-95
;	1.3	12-02-96
;	1.4	09-07-96
;
;-
;-----------------------------------------------------------------------------


PRO d1ispf, 	pipe, lpend, procval, dsfull,				$
		classval, yearval, isoval, indexval, fileval, 		$
		nel, ipsysdum, celem, 					$
		ide, teda, gomenu, bitfile,				$
		FONT_LARGE=font_large, FONT_SMALL=font_small, 		$
		EDIT_FONTS=edit_fonts


                ;**** Set defaults for keywords ****

    IF NOT (KEYWORD_SET(font_large)) THEN font_large = ''
    IF NOT (KEYWORD_SET(font_small)) THEN font_small = ''
    IF NOT (KEYWORD_SET(edit_fonts)) THEN 				$
    edit_fonts = {font_norm:'',font_input:''}

		;********************************************
		;****     Declare variables for input    ****
		;**** arrays wil be declared after sizes ****
		;**** have been read.                    ****
                ;********************************************
    lpend  = 0
    ntdmax = 0
    nblock = 0
    nenrgy = 0
    nener = 0
    lsetl = 0
    lsetm = 0
    nmin = 1
    nmax = 2
    input = 0

		;********************************
		;**** Read data from fortran ****
		;********************************

    readf, pipe, input
    ntdmax = input
    readf, pipe, input
    ndtin = input
    readf, pipe, input
    ndden = input
    readf, pipe, input
    ite = input
    readf, pipe, input
    ide = input
    tvals = dblarr(3, ndtin)
    teda  = dblarr(ndden)
    for j=0,(ndtin-1) do begin
        readf, pipe, fdum
        tvals(2,j) = fdum                       ;Input temperatures
    endfor					;(reduced units only)
    for j=0,(ndden-1) do begin
        readf, pipe, fdum
        teda(j) = fdum                          ;Input densities
    endfor


		;*******************************************
		;**** Set default value if non provided ****
		;*******************************************

    if (procval.new lt 0) then begin
        temp_arr = fltarr(ndtin)
        dens_arr = fltarr(ndden)
        procval = {			$
		   new   : 0 ,          $
 		   title : '',		$
 		   lfsel : 0,		$
 		   tolval: 5,           $
		   ifout : 1,		$
 		   losel : 0,		$
                   tin   : temp_arr,    $
                   din   : dens_arr,    $
                   maxt  : 0,           $
                   maxd  : 0,           $
	    	   tvals : tvals,	$
		   z0val : '',		$
		   zval  : '',		$
		   z1val : ''		$
	          }
    endif
    procval.tvals = tvals

		;****************************************
		;**** Pop-up processing input widget ****
		;****************************************

    adas401_proc, procval, dsfull,  					$
		  classval, yearval, isoval, indexval, fileval, action,	$
		  nel, ipsysdum, celem, 				$
		  ntdmax, teda, ndtin, ndden, 				$
		  ite, ide, bitfile,					$
		  FONT_LARGE=font_large, FONT_SMALL=font_small,		$
		  EDIT_FONTS=edit_fonts

		;********************************************
		;****  Act on the event from the widget  ****
		;**** There are three possible events    ****
		;**** 'Done', 'Cancel' and 'Menu'.       ****
		;********************************************

    gomenu = 0
    if action eq 'Done' then begin
        lpend = 0
    endif else if action eq 'Menu' then begin
        lpend = 0
        gomenu = 1
    endif else begin
        lpend = 1
    endelse

		;*******************************
		;**** Write data to fortran ****
		;*******************************

    printf, pipe, lpend
    if lpend eq 0 then begin
        printf, pipe, procval.ifout
        printf, pipe, procval.z1val
        printf, pipe, procval.maxt
        printf, pipe, procval.losel
        if procval.losel ne 0 then begin
            for i = 0, (procval.maxt - 1) do begin
	        printf, pipe, procval.tin(i)
            endfor
            for i = 0, (procval.maxt - 1) do begin
	        printf, pipe, procval.din(i)
            endfor
            printf, pipe, procval.lfsel
            if (procval.lfsel eq 1) then begin
	        printf, pipe, procval.tolval
            endif
        endif
    endif
    


END
