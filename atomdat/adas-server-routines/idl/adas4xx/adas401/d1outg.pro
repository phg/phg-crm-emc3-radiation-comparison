; Copyright (c) 1995, Strathclyde University.
; SCCS info: Module @(#)$Header: /home/adascvs/idl/adas4xx/adas401/d1outg.pro,v 1.4 2004/07/06 13:12:07 whitefor Exp $ Date $Date: 2004/07/06 13:12:07 $                              
;+
; PROJECT:
;       ADAS IBM MVS to UNIX conversion
;
; NAME:
;	D1OUTG
;
; PURPOSE:
;	Communication with ADAS401 FORTRAN via UNIX pipe and
;	graphics output.
;
; EXPLANATION:
;	The routine begins by reading information from the ADAS401
;	FORTRAN process via a UNIX pipe.  Then the IDL graphical
;	output routine for ADAS401 is invoked.  Communications are to
;	the FORTRAN subroutine D1OUTG.
;
; USE:
;	This routine is specific to adas401.                            
;
; INPUTS:
;	DSFULL   - Data file name
;
;	PIPE	 - The IDL unit number of the bi-directional pipe to the
;		   ADAS401 FORTRAN process.
;
;       UTITLE   - Optional comment by user
;
;	GRPSCAL	 - Integer; 0 if default scaling required 1 if user
;		   specified scaling to be used.
;
;	XMIN	 - String; User sepcified x-axis minimum, number as string.
;
;	XMAX	 - String; User sepcified x-axis maximum, number as string.
;
;	YMIN	 - String; User specified y-axis minimum, number as string.
;
;	YMAX	 - String; User specified y-axis maximum, number as string.
;
;	Z1VALUES - String array; String form of the charge states found
;		   in the input data file.
;
;	Z1INDEX  - Integer array; flags for which of the above z1values
;		   are to be plotted, a 1 means the corresponding
;		   value is to be plotted, a 0 means it is not.
;
;	HRDOUT	 - Integer; 0 if no hardcopy required, 1 if hardcopy.
;
;	HARDNAME - String; file name for hardcopy output.
;
;	DEVICE	 - String; IDL name for hardcopy output device.
;
;	HEADER	 - ADAS version header information for inclusion in the
;		   graphical output.
;
;	IZE	 - Integer; number of charge states (elements in z1values)
;
;	CHEAD	 - String; Heading to be used by FORTRAN and plotting.
;
;	Z1VAL	 - Integer; the recombining ion charge
;
;	LPARTL   - Integer; Flag: 0=Standard file, 1=Partial file.
;
;	CELEM	 - String; The element symbol extracted from the file name.
;
;	DATACLASS- String; The three letter code representing the dataclass.
;
;	YEAR     - String; The year of the data file - extracted from filename.
;
;	IPRNTDUM - Integer; The parent system index for a partial file.
;
;	IPSYSDUM - Integer; The spin system index for a partial file.
;
;	TEDA	 - Float array; actual temps read from input file
;
;	IDSEL    - Integer; index of density selected for plot.
;
;	Z0VAL	 - Integer; Element nuclear charge.
;
;	NEL      - Integer; Number of electrons.
;
;       BITFILE - String; the path to the dirctory containing bitmaps
;                 for the 'escape to series menu' button.
;
; OPTIONAL INPUTS:
;	None.
;
; OUTPUTS:
;       GOMENU  - Int; flag - set to 1 if user has selected 'escape direct
;                 to series menu , otherwise 0
;
; OPTIONAL OUTPUTS:
;	None.
;
; KEYWORD PARAMETERS:
;	FONT	- String; Name of the font to be used for the graphical
;		  output widget.
;
; CALLS:
;	ADAS401_PLOT	ADAS401 graphical output.
;
; SIDE EFFECTS:
;	This routine reads information from FORTRAN via a UNIX pipe.
;
; CATEGORY:
;	Adas system.
;
; WRITTEN:
;       Tim Hammond, Tessella Support Services plc, 1-Aug-1995 
;
; MODIFIED:
;	1.1	Tim Hammond		
;		First release
;	1.2	Tim Hammond
;		Tidied up comments and code
;	1.3	Tim Hammond
;		Added extra data classes (iswit gt 5)
;	1.4	William Osborn
;		Added menu button code
;
; VERSION:
;	1.1	01-08-95
;	1.2	08-09-95
;	1.3	24-10-95
;	1.4	09-07-96
;-
;-----------------------------------------------------------------------------



PRO d1outg, dsfull, pipe, utitle, grpscal, xmin, xmax, ymin, ymax, 	$
	    z1values, z1index, hrdout, hardname, device, header, 	$
            ize, chead, z1val, lpartl, celem, dataclass, year, 		$
            iprntdum, ipsysdum, teda, idsel, z0val, nel, gomenu, 	$
	    bitfile, FONT=font


                ;**** Set defaults for keywords ****

    IF NOT (KEYWORD_SET(font)) THEN font = ''


		;**** Declare variables for input ****

    titlx = " "
    titlm = " "
    titlf = " "
    date = " "
    nener = 0
    npspl = 0
    nmx = 0
    lfsel = 0
    sdum = ''
    idum = 0
    itdval = 0
    aout = 0
    aoutm = 0
    modtemp = 0
    moddens = 0
    mintemp = 0

		;********************************
		;**** Read data from fortran ****
		;********************************
		
		;*** Read titles and date information ***

    readf, pipe, idum
    iswit = idum
    CASE iswit OF
        1 : begin
	    ytitle = 'ACD (cm!E3!N s!E-1!N)'
	end
	2 : begin
	    ytitle = 'SCD (cm!E3!N s!E-1!N)'
	end
	3 : begin
	    ytitle = 'CCD (cm!E3!N s!E-1!N)'
	end
	4 : begin
	    ytitle = 'PRB (erg cm!E3!N s!E-1!N)'
	end
	5 : begin
	    ytitle = 'PRC (erg cm!E3!N s!E-1!N)'
        end
	6 : begin
	    ytitle = 'QCD (cm!E3!N s!E-1!N)'
	end
	7 : begin
	    ytitle = 'XCD (cm!E3!N s!E-1!N)'
	end
	8 : begin
	    ytitle = 'PLT (erg cm!E3!N s!E-1!N)'
	end
	9 : begin
	    ytitle = 'PLS (erg cm!E3!N s!E-1!N)'
	end
        10 : begin
	    ytitle = 'METASTABLE POPULATION'
	end
    ENDCASE
    readf, pipe, idum
    ite = idum					;number of input temperatures
    intemp = fltarr(ite)
    for i=0, (ite-1) do begin
	readf, pipe, fdum
        intemp(i) = fdum			;input temperatures 
    endfor					;(FORTRAN 'TR')
    readf, pipe, idum
    ize = idum					;no. of charge states
    aplot = fltarr(ite,ize)
    for i=0, (ite-1) do begin
	for j=0, (ize-1) do begin
	    readf, pipe, fdum
	    aplot(i,j) = fdum			;input 'coefficients'
	endfor
    endfor
    readf, pipe, idum
    losel = idum				;are model values wanted?
    if (losel eq 1) then begin
        readf, pipe, idum
        itdval = idum
        modtemp = fltarr(itdval)		;model temperatures
        moddens = fltarr(itdval)		;model densities
        for i=0, (itdval-1) do begin		;(FORTRAN 'TRED')
	    readf, pipe, fdum
	    modtemp(i) = fdum
	    readf, pipe, fdum
	    moddens(i) = fdum			;(FORTRAN 'DRED')
        endfor
        aout = fltarr(itdval)
        for i=0, (itdval-1) do begin  
            readf, pipe, fdum
            aout(i) = fdum			;model coefficients
        endfor
    endif
    readf, pipe, idum
    lfsel = idum				;is minimax wanted?
    if (lfsel eq 1) then begin
	readf, pipe, idum
	nmx = idum
	mintemp = fltarr(nmx)
	aoutm = fltarr(nmx)
	for i=0, (nmx-1) do begin
	    readf, pipe, fdum
	    mintemp(i) = fdum			;FORTRAN 'TREDM'
	    readf, pipe, fdum
	    aoutm(i) = fdum			;minimax coeffs.
	endfor
    endif
    
    readf, pipe, sdum
    titlm = sdum
    readf, pipe, sdum
    esymb = sdum


		;***********************
		;**** Plot the data ****
		;***********************

    adas401_plot, dsfull, chead, z1val,					$
  		  titlx, titlm, utitle, date, 				$
                  intemp, modtemp, moddens, mintemp,			$
		  aplot, aout, aoutm,					$
		  ytitle, z1values, z1index, ize,			$
                  grpscal, xmin, xmax, ymin, ymax,			$
   		  lfsel, losel, ite, itdval, 				$
                  celem, dataclass, year, iprntdum, ipsysdum,		$
		  hrdout, hardname, device, header, lpartl, teda, idsel,$
                  z0val, nel, esymb, gomenu, bitfile, FONT=font

END
