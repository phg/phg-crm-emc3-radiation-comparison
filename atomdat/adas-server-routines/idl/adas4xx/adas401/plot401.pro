; Copyright (c) 1995, Strathclyde University .
; SCCS INFO : Module @(#)$Header: /home/adascvs/idl/adas4xx/adas401/plot401.pro,v 1.6 2004/07/06 14:32:04 whitefor Exp $ Date $Date: 2004/07/06 14:32:04 $   
;
;+
; PROJECT:
;       ADAS IBM MVS to UNIX conversion
;
; NAME:
;	PLOT401
;
; PURPOSE:
;	Plot one graph for ADAS401.
;
; EXPLANATION:
;	This routine plots ADAS401 output for a single graph.
;
; USE:
;	This routine is very similar to that for adas201.    
;
; INPUTS:
;	TITLE	- String array : General title for program run. 5 lines.
;
;	XTITLE  - String : title for x-axis annotation
;
;	YTITLE  - String : title for y-axis annotation
;
;	LDEF1	- Integer; 1 - User specified axis limits to be used, 
;		  	   0 - Default scaling to be used.
;
;	XMIN	- String; Lower limit for x-axis of graph, number as string.
;
;	XMAX	- String; Upper limit for x-axis of graph, number as string.
;
;	YMIN	- String; Lower limit for y-axis of graph, number as string.
;
;	YMAX	- String; Upper limit for y-axis of graph, number as string.
;
;       Z1VALUES - String array; String form of the charge states found
;                  in the input data file.
;
;       Z1INDEX  - Integer array; flags for which of the above z1values
;                  are to be plotted, a 1 means the corresponding
;                  value is to be plotted, a 0 means it is not.
;
;       IZE      - Integer; number of charge states (elements in z1values)
;
;       APLOT   - Float array; input 'coefficients'
;
;       AOUT    - Float array; model coefficients
;
;       AOUTM   - Float array; minimax coefficients
;
;       INTEMP  - FLoat array; input temperatures
;
;       MODTEMP - Float array; model temperatures
;
;       MODDENS - Float array; model densities
;
;       MINTEMP - Float array; minimax temperatures
;
;       LFSEL   - Integer; 0 - No minimax fitting was selected
;                          1 - Mimimax fitting was selected
;
;       LOSEL   - Integer; 0 - No interpolated values for spline fit.
;                          1 - Intepolated values for spline fit.
;
;       Z1VAL    - Integer; the recombining ion charge
;
;       LPARTL   - Integer; Flag: 0=Standard file, 1=Partial file.
;
;       ITDVAL  - Integer; max. no of temp/dens pairs allowed
;
;       CELEM    - String; The element symbol extracted from the file name.
;
;       DATACLASS- String; The three letter code representing the dataclass.
;
;       YEAR     - String; The year of the data file - extracted from filename.
;
;       IPRNTDUM - Integer; The parent system index for a partial file.
;
;       IPSYSDUM - Integer; The spin system index for a partial file.
;
;	DENPLOT	 - Float; the reduced density used for the plot.
;
;       Z0VAL    - Integer; Element nuclear charge.
;
;       NEL      - Integer; Number of electrons.
;
;       EYSMB    - String; the element symbol.
;
;
; OPTIONAL INPUTS:
;	None.
;
; OUTPUTS:
;	None.
;
; OPTIONAL OUTPUTS:
;	None.
;
; KEYWORD PARAMETERS:
;	None.
;
; CALLS:
;	None.
;
; SIDE EFFECTS:
;	None.
;
; CATEGORY:
;	Adas system.	
;
; WRITTEN:
;       Tim Hammond, Tessella Support Services plc,  8-Sep-1995 
;
; MODIFIED:
;	1.1	Tim Hammond		
;		First release
;	1.2	Tim Hammond		
;		Tidied up comments and code
;	1.3	Tim Hammond
;		Added better positioning of graph labels for user-defined
;		axes ranges.
;	1.4	Tim Hammond
;		Added extra labelling check for PLS and PLT partial data.
;	1.5	im Hammond
;               Added new COMMON block Global_lw_data which contains the 
;		values of left, right, top, bottom, grtop, grright
;	1.6     William Osborn
;               Added check for new environment variable VERY_SMALL
;               If this is set to the string "YES" (preferably in the
;               .adas_setup file) then the font size used on the graphs
;               is reduced and the positioning of labels adjusted
;               slightly to make things fit on a very small screen.
;
; VERSION:
;	1.1	08-09-95
;	1.2	08-09-95
;	1.3	08-09-95
;	1.4	15-02-96
;	1.5	27-02-96
;	1.6	11-10-96
;
;-
;----------------------------------------------------------------------------

PRO plot401, title, xtitle, ytitle, 					$
             ldef1, xmin, xmax, ymin, ymax, z1values, z1index, ize, 	$
	     aplot, aout, aoutm, intemp, modtemp, moddens, mintemp, 	$
             lfsel, losel,						$
	     z1val, lpartl, itdval, celem, dataclass, year, iprntdum,	$
             ipsysdum, denplot, z0val, nel, esymb


    COMMON Global_lw_data, left, right, top, bottom, grtop, grright

                ;****************************************************
		;**** Suitable character size for current device ****
		;**** Aim for 60 characters in y direction.      ****
		;**** Also reduce slightly to fit on page        ****
		;****************************************************

    charsize = (!d.y_vsize/!d.y_ch_size)/60.0
    small_check = GETENV('VERY_SMALL')
    if small_check eq 'YES' then charsize=charsize*0.8

		;**** set makeplot counter ****

    makeplot = 0
    xhigh = 0.0
    xlow = 0.0
    yhigh = 0.0
    ylow = 0.0

		;**********************************************
		;**** Find x and y ranges for auto scaling ****
		;**** and for specified axes		   ****
		;**********************************************

    uppery = 1.0e37
    upperx = 1.0e37
    lowery = 1.0e-37
    lowerx = 1.0e-37

		;***********************************
		;**** find limits of input data ****
		;**** (if any requested) 	****
		;***********************************

    inputplots = 0				;no. of plots to construct
    for i=0, (ize-1) do begin
        if (z1index(i) eq 1) then begin
            aplotvals = where(aplot(*,i) gt lowery  and 		$
            aplot(*,i) lt uppery)
	    if aplotvals(0) gt -1 then begin
	        makeplot = 1
	        yhighdum = max(aplot(aplotvals,i))
	        ylowdum = min(aplot(aplotvals,i))
                if yhigh eq 0.0 then begin
		    yhigh = yhighdum
		endif else begin
		    yhigh = yhigh > yhighdum
		endelse
		if ylow eq 0.0 then begin
		    ylow = ylowdum
		endif else begin
		    ylow = ylow < ylowdum
		endelse
		inputplots = inputplots + 1
	    endif
	endif
    endfor
    intempvals = where(intemp gt lowerx  and intemp lt upperx) 
    if intempvals(0) gt -1 then begin
        makeplot = 1
        xhigh = max(intemp(intempvals))
        xlow = min(intemp(intempvals))
    endif else begin
        makeplot = 0
        inputplots = 0			;no input plots in range
    endelse

		;***************************************
		;**** Find limits of any model data ****
		;***************************************

    modplot = 0
    aoutvals = where(aout gt lowery  and aout lt uppery)
    if aoutvals(0) gt -1 then begin
        makeplot = 1
        yhighdum = max(aout(aoutvals))
        ylowdum = min(aout(aoutvals))
        if yhigh eq 0.0 then begin
            yhigh = yhighdum
        endif else begin
            yhigh = yhigh > yhighdum
        endelse
        if ylow eq 0.0 then begin
            ylow = ylowdum
        endif else begin
            ylow = ylow < ylowdum
        endelse
    endif
    modtempvals = where(modtemp gt lowerx  and modtemp lt upperx)
    if (modtempvals(0) gt -1) and makeplot eq 1 then begin
        modplot = 1
        xhighdum = max(modtemp(modtempvals))
        xhigh = xhigh > xhighdum
        xlowdum = min(modtemp(modtempvals))
        xlow = xlow < xlowdum
    endif

		;*************************************
		;**** Find limits of minimax data ****
		;*************************************

    minplot = 0
    if lfsel ne 0 then begin
        aoutmvals = where(aoutm gt lowery  and aoutm lt uppery)
        if aoutmvals(0) gt -1 then begin
	    makeplot = 1
	    yhighdum = max(aoutm(aoutmvals))
            ylowdum = min(aoutm(aoutmvals))
	    if yhigh eq 0.0 then begin
                yhigh = yhighdum
            endif else begin
                yhigh = yhigh > yhighdum
            endelse
            if ylow eq 0.0 then begin
                ylow = ylowdum
            endif else begin
                ylow = ylow < ylowdum
            endelse
        endif
        mintempvals = where(mintemp gt lowerx  and mintemp lt upperx)
        if (mintempvals(0) gt -1) and makeplot eq 1 then begin
            minplot = 1
            xhighdum = max(mintemp(mintempvals))
            xhigh = xhigh > xhighdum
            xlowdum = min(mintemp(mintempvals))
            xlow = xlow < xlowdum
        endif
    endif
    if ldef1 eq 1 then begin		;user-defined axes checks
	style = 1
	xlow = float(xmin)
	xhigh = float(xmax)
	ylow = float(ymin)
	yhigh = float(ymax)
	makeplot = 0			;check for data which is in range
	for i=0, (ize-1) do begin	;INPUT data
	    if (z1index(i) eq 1) then begin
		newapvals = where(aplot(*,i) gt ylow and 		$
		                  aplot(*,i) lt yhigh)
		if newapvals(0) gt -1 then makeplot = 1
	    endif
	endfor
	newinvals = where(intemp gt xlow and intemp lt xhigh)
	if (newinvals(0) gt -1 and makeplot eq 1) then begin
	    makeplot = 1
	endif else begin
	    makeplot = 0
	endelse
        if losel eq 1 then begin
	    newatvals = where(aout gt ylow and aout lt yhigh)	;MODEL data
	    if newatvals(0) gt -1 then makeplot = 1
	    newmdvals = where(modtemp gt xlow and modtemp lt xhigh)
	    if (newmdvals(0) gt -1 and makeplot eq 1) then begin
	        makeplot = 1
	    endif else begin
	        makeplot = 0
	    endelse
	endif
        if lfsel ne 0 then begin				;MINMAX data
	    newamvals = where(aoutm gt ylow and aoutm lt yhigh)	
	    if newamvals(0) gt -1 then makeplot = 1
	    newmivals = where(mintemp gt xlow and mintemp lt xhigh)
	    if (newmivals(0) gt -1 and makeplot eq 1) then begin
	        makeplot = 1
	    endif else begin
	        makeplot = 0
	    endelse
	endif
    endif else begin
	style = 0
    endelse

		;**** Set up log-log plotting axes ****

    if makeplot eq 1 then begin

        plot_oo, [xlow,xhigh], [ylow,yhigh], /nodata, ticklen=1.0,	$
 	         position=[left, bottom, grright, grtop],		$
 	         xtitle=xtitle, ytitle=ytitle, xstyle=style, 		$
                 ystyle=style, charsize=charsize


		;**********************
		;****  Make plots  ****
		;**********************

		;**********************
		;**** Input plots  ****
		;**********************

	if inputplots gt 0 then begin
            z1actual = strarr(ize)			;labels for plots
	    for i=0,(ize-1) do begin
                if z1index(i) eq 1 then begin
		    points = where(intemp ne 0)
		    endpoint = n_elements(points)
		    oplot, intemp, aplot(*,i), linestyle=5

			;**** Make sure labels fit on graph ****

		    if (intemp(endpoint-1) le 0.8*(10^(!x.crange(1)))) 	$
                    then begin
			alig = 0.0
		    endif else if (intemp(endpoint-1) gt		$
		    0.8*(10^(!x.crange(1))) and intemp(endpoint-1) le	$
		    0.9*(10^(!x.crange(1)))) then begin
			alig = 0.5
		    endif else begin
			alig = 1.0
                    endelse

		    if ldef1 eq 1 then begin
			points = where(intemp ge xmin and intemp le xmax)
			npoint = fix(n_elements(points)*0.8)
			endpoint = points(npoint)
			alig = 0.5
		    endif

		    z1actual(i) = "Z!D1!N="+strmid(z1values(i),4,	$
                    (strlen(z1values(i))-4))
		    xyouts, intemp(endpoint-1), aplot(endpoint-1,i),	$
		            ' '+strcompress(z1actual(i),/remove_all),	$
                            charsize=charsize*0.8, alignment=alig
		endif
	    endfor	
	endif

		;*********************
		;**** Model plot  ****
		;*********************

	if modplot gt 0 then begin
	    oplot, modtemp, aout, thick=1.5
	    oplot, modtemp, aout, psym=7
	    melements = n_elements(modtemp)
	    imel = fix(melements/3)
	    xyouts, modtemp(imel), aout(imel), 				$
                    ' Z!D1!N='+z1val+' (MODEL)', charsize=charsize*0.8,	$
                    alignment=0.0
	endif

		;**********************
		;**** Minimax plot ****
		;**********************

	if minplot gt 0 then begin
	    oplot, mintemp, aoutm, linestyle=1
	endif

    endif else begin
        xyouts, 0.1, 0.5, /normal, charsize=charsize*1.2,		$
        "-- ADAS401 : No data found in these axes ranges --"
    endelse

		;***************************************
		;**** Construct graph title         ****
		;**** "!C" is the new line control. ****
		;***************************************

    !p.font=-1
    if inputplots gt 0 then begin
        title(5) = "KEY     : (LONG DASH - INPUT DATA) "
    endif
    if losel eq 1 then begin
	if title(5) eq '' then begin
	    title(5) = "KEY     : (CROSSES/FULL LINE - SPLINE) "
	endif else begin
	    title(5) = title(5) + "(CROSSES/FULL LINE - SPLINE) "
	endelse
    endif
    if lfsel eq 1 then begin
	if title(5) eq '' then begin
	    title(5) = "KEY     : (DOTTED - MINIMAX)"
	endif else begin
	    title(5) = title(5) + "(DOTTED - MINIMAX)"
	endelse
    endif else begin
        title(4) = title(5)
  	title(5) = ' '
    endelse

    if small_check eq 'YES' then begin
        gtitle =  title(0) + "!C!C" + title(1) + "!C!C" + title(2)+"!C!C" + $
          title(3) + "!C!C" + title(4) + "!C!C" + title(5)
    endif else begin
        gtitle =  title(0) + "!C!C" + title(1) + "!C" + title(2) + "!C" + $
          title(3) + "!C" + title(4) + "!C" + title(5)
    endelse

		;**** Construct right hand labels ****

    if small_check eq 'YES' then begin
        inputlabel = "--- INPUT FILE INFORMATION ---!C!C!C"+	$
          "ISO-ELECTRONIC SEQUENCE !C!C"+				$
          "CLASS!C!CYEAR!C!CTYPE!C!C"
        if lpartl eq 1 then begin
            inputlabel = inputlabel+"PARENT REFERENCE!C!C"+		$
              "SPIN SYSTEM REFERENCE!C!C"
        endif
        inputlabel = inputlabel+"REDUCED DENSITY PLOTTED!C!C"
        inlab2 = "!C!C!C= "+celem+"!C!C"+				$
          "= "+strupcase(dataclass)+"!C!C"+				$
          "= "+year+"!C!C"+						$
          "= "
        if lpartl eq 1 then begin
            if iprntdum eq 0 then begin
                prntstring = '#'
            endif else begin
                prntstring = strtrim(string(iprntdum),2)
            endelse
            if ipsysdum eq 0 then begin
                psysstring = '#'
            endif else begin
                psysstring = strtrim(string(ipsysdum),2)
            endelse
            inlab2 = inlab2 + "PARTIAL!C!C"+				$
              "= "+prntstring+"!C!C"+"= "+psysstring+"!C!C"
        endif else begin
            inlab2 = inlab2 + "STANDARD!C!C"
        endelse
        inlab2 = inlab2 + "= "+strtrim(string(denplot, format='(E10.3)'),2)$
          +"!C!C"
        
        modlabel = "------ MODEL INFORMATION ------!C!C!C"+		$
          "ELEMENT SYMBOL!C!CZ!D0!N NUCLEAR CHARGE!C!C"+		$
          "Z!D1!N RECOMBINING ION CHARGE!C!C"+			$ 
          "NUMBER OF ELECTRONS!C!C!C!C"+				$
          "- MODEL TEMPERATURE/DENSITY RELATIONSHIP -!C!C!C"+	$ 
          "INDEX      REDUCED TEMP.    REDUCED DENSITY!C!C"	
        for i=1, itdval do begin
            modlabel = modlabel+strtrim(string(i),2)+"!C!C"
        endfor
        symlen = strlen(esymb)
        if symlen eq 1 then begin
            symname = strupcase(esymb)
        endif else begin
            symname = strupcase(strmid(esymb, 0, 1))+strmid(esymb, 1, 1)
        endelse
        modlab2 = "!C!C!C= "+symname+					$
          "!C!C= "+strtrim(string(z0val),2)+				$
          "!C!C= "+strtrim(string(z1val),2)+				$
          "!C!C= "+strtrim(string(nel),2)+"!C!C!C!C"
        modlab3 = ""
        modlab4 = ""
        for i=0, itdval-1 do begin
            modlab3 = modlab3+strtrim(string(modtemp(i),format='(E10.3)'),2)+$
              "!C!C"
            modlab4 = modlab4+strtrim(string(moddens(i),format='(E10.3)'),2)+$
              "!C!C"
        endfor
    endif else begin
        inputlabel = "--- INPUT FILE INFORMATION ---!C!C"+	$
          "ISO-ELECTRONIC SEQUENCE !C"+				$
          "CLASS!CYEAR!CTYPE!C"
        if lpartl eq 1 then begin
            inputlabel = inputlabel+"PARENT REFERENCE!C"+		$
              "SPIN SYSTEM REFERENCE!C"
        endif
        inputlabel = inputlabel+"REDUCED DENSITY PLOTTED!C"
        inlab2 = "!C!C= "+celem+"!C"+					$
          "= "+strupcase(dataclass)+"!C"+				$
          "= "+year+"!C"+						$
          "= "
        if lpartl eq 1 then begin
            if iprntdum eq 0 then begin
                prntstring = '#'
            endif else begin
                prntstring = strtrim(string(iprntdum),2)
            endelse
            if ipsysdum eq 0 then begin
                psysstring = '#'
            endif else begin
                psysstring = strtrim(string(ipsysdum),2)
            endelse
            inlab2 = inlab2 + "PARTIAL!C"+				$
              "= "+prntstring+"!C"+"= "+psysstring+"!C"
        endif else begin
            inlab2 = inlab2 + "STANDARD!C"
        endelse
        inlab2 = inlab2 + "= "+strtrim(string(denplot, format='(E10.3)'),2)$
          +"!C"
        
        modlabel = "------ MODEL INFORMATION ------!C!C"+		$
          "ELEMENT SYMBOL!CZ!D0!N NUCLEAR CHARGE!C"+		$
          "Z!D1!N RECOMBINING ION CHARGE!C"+			$ 
          "NUMBER OF ELECTRONS!C!C!C"+				$
          "- MODEL TEMPERATURE/DENSITY RELATIONSHIP -!C!C"+	$ 
          "INDEX      REDUCED TEMP.    REDUCED DENSITY!C"	
        for i=1, itdval do begin
            modlabel = modlabel+strtrim(string(i),2)+"!C"
        endfor
        symlen = strlen(esymb)
        if symlen eq 1 then begin
            symname = strupcase(esymb)
        endif else begin
            symname = strupcase(strmid(esymb, 0, 1))+strmid(esymb, 1, 1)
        endelse
        modlab2 = "!C!C= "+symname+					$
          "!C= "+strtrim(string(z0val),2)+				$
          "!C= "+strtrim(string(z1val),2)+				$
          "!C= "+strtrim(string(nel),2)+"!C!C!C"
        modlab3 = ""
        modlab4 = ""
        for i=0, itdval-1 do begin
            modlab3 = modlab3+strtrim(string(modtemp(i),format='(E10.3)'),2)+$
              "!C"
            modlab4 = modlab4+strtrim(string(moddens(i),format='(E10.3)'),2)+$
              "!C"
        endfor
    endelse

		;**** plot title **** 

    if small_check eq 'YES' then begin
	charsize = charsize * 0.9
    endif

    if small_check eq 'YES' then begin
        xyouts, left, top+0.02, gtitle, /normal, alignment=0.0, 	$
          charsize=charsize*0.95
    endif else begin
        xyouts, left, top, gtitle, /normal, alignment=0.0, 	$
          charsize=charsize*0.95
    endelse
   		;**** labels ****

    labtop = top-0.14
    xyouts, grright+0.03, labtop, inputlabel, /normal, alignment=0.0,	$
	    charsize=charsize*0.8
    xyouts, grright+0.22, labtop, inlab2, /normal, alignment=0.0,   	$
            charsize=charsize*0.8
    labtop = labtop - 0.16

    if losel eq 1 then begin
        if small_check eq 'YES' then begin
            xyouts, grright+0.03, labtop, modlabel, /normal,alignment=0.0,$
              charsize=charsize*0.8
            xyouts, grright+0.22, labtop, modlab2, /normal, alignment=0.0, $
              charsize=charsize*0.8
            xyouts, grright+0.11, labtop-0.17, modlab3, /normal,alignment=0.0,$
              charsize=charsize*0.8
            xyouts, grright+0.23, labtop-0.17, modlab4, /normal,alignment=0.0,$
              charsize=charsize*0.8
        endif else begin
            xyouts, grright+0.03, labtop, modlabel, /normal, alignment=0.0, $
              charsize=charsize*0.8
            xyouts, grright+0.22, labtop, modlab2, /normal, alignment=0.0, $
              charsize=charsize*0.8
            xyouts, grright+0.11, labtop-0.15, modlab3, /normal,alignment=0.0,$
              charsize=charsize*0.8
            xyouts, grright+0.23, labtop-0.15, modlab4, /normal,alignment=0.0,$
              charsize=charsize*0.8
        endelse
    endif

END
