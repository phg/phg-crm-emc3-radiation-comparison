; Copyright (c) 1995, Strathclyde University.
; SCCS info: Module @(#)$Header: /home/adascvs/idl/adas4xx/adas401/d1setp.pro,v 1.1 2004/07/06 13:12:16 whitefor Exp $ Date $Date: 2004/07/06 13:12:16 $
;+
; PROJECT:
;       ADAS IBM MVS to UNIX conversion
;
; NAME:
;	D1SETP
;
; PURPOSE:
;	IDL communications with ADAS401 FORTRAN process via pipe.
;
; EXPLANATION:
;	This IDL routine reads data from the ADAS401 FORTRAN subroutine
;	D1SETP via a UNIX pipe.  In the original IBM version the FORTRAN
;	subroutine D1SETP put information needed by the ISPF interface
;	into the ISPF pool area for later use.  Now the same information
;	is written to IDL for use in its user unterface.
;
; USE:
;	The use of this routine is specific to ADAS401.  See
;	adas401.pro for an example.
;
; INPUTS:
;	PIPE	- The IDL unit number of the bi-directional pipe to the
;		  ADAS401 FORTRAN process.
;
; OPTIONAL INPUTS:
;	None
;
; OUTPUTS:
;
;
; OPTIONAL OUTPUTS:
;	None
;
; KEYWORD PARAMETERS:
;	None
;
; CALLS:
;	None
;
; SIDE EFFECTS:
;	Reads data from a pipe.
;
; CATEGORY:
;	Adas system.
;
; WRITTEN:
;       Tim Hammond, Tessella Support Services plc, 01/05/1995
;
; MODIFIED:
;	1.1		Tim Hammond			07-09-95
; VERSION:
;	1.1		First release		
;
;-----------------------------------------------------------------------------



PRO d1setp, pipe, ize, zipt

		;**********************************
		;**** Initialise new variables ****
		;**********************************


    fdum = 0.0

		;********************************
		;**** Read data from fortran ****
		;********************************

    readf, pipe, ize
    zipt = fltarr(ize)
    for i=0, (ize-1) do begin
	readf, pipe, fdum
	zipt(i) = fdum
    endfor


END
