; Copyright (c) 1995 Strathclyde University .
; SCCS info: Module @(#)$Header: /home/adascvs/idl/adas4xx/adas401/cw_dataset.pro,v 1.2 2004/07/06 13:05:09 whitefor Exp $ Date $Date: 2004/07/06 13:05:09 $
;+
; PROJECT:
;       ADAS IBM MVS to DEC UNIX conversion
;
; NAME: CW_DATASET()
;
; PURPOSE:
;       Sets up parameters for a search of relevant directories for
;	datafiles for adas 401.
;
; EXPLANATION:
;       This function creates a compound widget consisting of seven
;       selection widgets: typeins for the year of data, element
;	symbol and member prefix and menu type selectors for the data
;	class, type of data file (partial or standard), and first and
;	second indices for a partial file.
;
;	No events are generated by this routine: it is simply interrogated
;	by cw_adas401_in.pro when the user selects the 'Search' button.
;
; USE:
;       See routine cw_adas401_in.pro for an example.
;
; INPUTS:
;    	PARENT	-	Long integer; the ID of the parent widget.
;
;	CATS	-	String array containing titles of the categories
;			to be searched (originally intended for use
;			with a more general routine so cats will always
;			contain the labels of the seven selection widgets.
;
;	VALUES	-	2D String array; For each element of cats it holds
;			the allowed options. For the typeins VALUES is
;			blank, whereas for example CATS(0,*) contains
;			all the data classes which are passed through
;			to the CW_BSELECTOR routine.
;
;	SELECTIONS	Integer array with elements corresponding to
;			the elements in cats. If selections is 1 then
;			the corresponding element in cats is a selection
;			menu. If selections is 0 then the cats element is a
;			typein. Again this feature is designed for a
;			more general routine and so will not be changed
;			here.
;
; OPTIONAL INPUTS:
;       None.
;
; OUTPUTS:
;       The return value of this function is the ID of the compound
;       widget which is of type LONG integer.
;
; OPTIONAL OUTPUTS:
;       None
;
; KEYWORD PARAMETERS:
;       FONT    - A font to use for all text in this widget.
;
;       TITLE   - The title to be included in this widget, used
;                 to indicate exactly what the required input dataset is.
;
; CALLS:
;       CW_BSELECTOR - Pull-down menu type compound widget.
;
; SIDE EFFECTS:
;       Three other routines are included which are used to manage the
;       widget:
;
;	DATASET_SET_VAL()
;	DATASET_GET_VAL()
;	DATASET_EVENT()
;
; CATEGORY:
;       Compound Widget
;
; WRITTEN:
;       Tim Hammond, Tessella Support Services plc, 7th September 1995
; MODIFIED:
;       1.1     Tim Hammond (Tessella Support Services plc)     
;		First release. This routine was originally intended for
;               more general use, but the complexity necessary has
;               meant that it is now specific to adas 401.
;	1.2	Tim Hammond
;		Modified functionality so that whenever partial data
;		of classes PLS or PLT is required, the indices buttons
;		are both set to zero ('Any') and are desensitized as
;		the indices must in this case be '##'
;
; VERSION:
;       1.1     07-09-95
;	1.2	15-02-96
;
;-
;-----------------------------------------------------------------------------

PRO dataset_set_val, id, value

                ;**** Return to caller on error ****

    ON_ERROR, 2
	
		;**** Retrieve the state ****
   
    first_child = widget_info(id, /child)
    widget_control, first_child, get_uvalue=state, /no_copy

		;**** Set data field values ****

    widget_control, long(state.buttons(0)), set_value=long(value.class)
    widget_control, state.buttons(1), set_value=value.year   
    widget_control, state.buttons(2), set_value=value.symbol 
    widget_control, long(state.buttons(3)), set_value=value.file   
    widget_control, long(state.buttons(4)), set_value=value.first  
    widget_control, long(state.buttons(5)), set_value=value.second 
    widget_control, state.buttons(6), set_value=value.member 

		;**** Set sensitivity of index buttons

    widget_control, long(state.buttons(4)), sensitive=(1-value.file)
    widget_control, long(state.buttons(5)), sensitive=(1-value.file)

		;**** Extra checks for data classes PLS and PLT ****

    if value.class ge 7 then begin
	if value.file eq 0 then begin
	    widget_control, long(state.buttons(4)), set_value=0,	$
	                    sensitive=0
            widget_control, long(state.buttons(5)), set_value=0,        $
                            sensitive=0
	endif
    endif

                ;**** Reset the value of state variable ****

    widget_control, first_child, set_uvalue=state, /no_copy

END

;-----------------------------------------------------------------------

function dataset_get_val, id

                ;**** Return to caller on error ****

    ON_ERROR, 2

		;*******************************
                ;**** Retrieve the state    ****
		;**** Get first_child id    ****
		;**** as state stored there ****
		;*******************************

    first_child = widget_info(id, /child)
    widget_control, first_child, get_uvalue=state

    widget_control, long(state.buttons(0)), get_value=class
    widget_control, long(state.buttons(1)), get_value=year 
    widget_control, long(state.buttons(2)), get_value=symbol
    widget_control, long(state.buttons(3)), get_value=file 
    widget_control, long(state.buttons(4)), get_value=first 
    widget_control, long(state.buttons(5)), get_value=second
    widget_control, long(state.buttons(6)), get_value=member

    datavalue = {class : class,					$
		 year  : year,					$
		 symbol: symbol,				$
		 file  : file,					$
		 first : first,					$
		 second: second,				$
		 member: member}

    widget_control, first_child, set_uvalue=state,/no_copy

    RETURN, datavalue

END

;------------------------------------------------------------------------

function dataset_event, event

    parent=event.handler

                ;**********************************************
                ;**** Retrieve the user value state        ****
                ;**** Get id of first_child widget because ****
                ;**** user value "state" is stored there   ****
                ;**********************************************

    first_child = widget_info(parent, /child)
    widget_control, first_child, get_uvalue=state, /no_copy

                ;************************
                ;**** Process events ****
                ;************************

    CASE event.id OF
	state.buttons(0) : begin
	    widget_control, long(state.buttons(0)), get_value=class_val
	    if class_val(0) ge 7 then begin
		widget_control, long(state.buttons(3)), get_value=filecheck
		if filecheck eq 0 then begin
		    widget_control, long(state.buttons(4)), 		$
		    set_value=0, sensitive=0
		    widget_control, long(state.buttons(5)), 		$
		    set_value=0, sensitive=0
		endif
	    endif else begin
                widget_control, long(state.buttons(3)), get_value=filecheck
		widget_control, long(state.buttons(4)), sensitive=(1-filecheck)
		widget_control, long(state.buttons(5)), sensitive=(1-filecheck)
	    endelse
	end

	state.buttons(1) : widget_control, state.buttons(2), /input_focus

	state.buttons(2) : widget_control, state.buttons(6), /input_focus

	state.buttons(6) : widget_control, state.buttons(1), /input_focus

        state.buttons(3) : begin		;partial/standard
	    widget_control, long(state.buttons(3)), get_value=filecheck
            widget_control, long(state.buttons(0)), get_value=class_val
	    if class_val lt 7 then begin
	        widget_control, long(state.buttons(4)),sensitive=(1-filecheck)
	        widget_control, long(state.buttons(5)),sensitive=(1-filecheck)
	    endif else begin
		if filecheck eq 0 then begin
                    widget_control, long(state.buttons(4)),             $
                    set_value=0, sensitive=0
                    widget_control, long(state.buttons(5)),             $
                    set_value=0, sensitive=0
                endif
	    endelse
	end

	else :        		;**** do nothing ****

    ENDCASE

                ;*********************************************
                ;*** make "state" available to other files ***
                ;*********************************************

    widget_control, first_child, set_uvalue=state, /no_copy

    RETURN, event

END

;-------------------------------------------------------------------------

function cw_dataset, parent, cats, values, selections, FONT=font, TITLE=title

    if (n_params() lt 3) then message, 					$
    "parent, cats, values and selections  must all be specified for cw_dataset"

		;**** Set defaults for keywords ****

    if (NOT KEYWORD_SET(font)) then font = ''
    if (NOT KEYWORD_SET(title)) then title = ''

		;**** How many categories are there? ****

    n_cats = n_elements(cats)
    rowid = intarr(n_cats)
    buttons = intarr(n_cats)
    head = intarr(n_cats)

		;**** Create main base for widget ****

    top = widget_base(parent, event_func='dataset_event',		$
                      func_get_val='dataset_get_val',			$
		      pro_set_val='dataset_set_val',			$
                      /column)

                ;******************************************************
                ;**** Create a dummy widget just to hold value of *****
                ;**** "state" variable so as not to get confused  *****
                ;**** with any other values. Adopt IDL practice   *****
                ;**** of using first child widget                 *****
                ;******************************************************

    first_child = widget_base(top) 

    cwid = widget_base(first_child, /column)

		;**** Title ****

    labelid = widget_label(cwid, value=title, font=font)

		;**** New base for buttons ****

    base = widget_base(cwid, /column, /frame)

		;**** Buttons ****

    for i=0, (n_cats-1) do begin
        rowid(i) = widget_base(base,/row)
        head(i) = widget_label(rowid(i),value=cats(i),font=font)
        if (selections(i) eq 1) then begin
            goodnames=where(values(i,*) ne '')
	    buttons(i) = cw_bselector(rowid(i), values(i, goodnames),	$
				      font=font)
        endif else begin
	    buttons(i) = widget_text(rowid(i), value='', xsize=7,	$
                                     font=font, /editable)
	endelse
    endfor

		;**** Create state structure ****

    state = {buttons   :    buttons}

		;**** Save initial state structure ****

    widget_control, first_child, set_uvalue=state, /no_copy

    RETURN, top

END
