; Copyright (c) 1995 Strathclyde University .
; SCCS info: Module @(#)$Header: /home/adascvs/idl/adas4xx/adas401/name401_check.pro,v 1.2 2004/07/06 14:23:13 whitefor Exp $ Date $Date: 2004/07/06 14:23:13 $
;+
; PROJECT:
;       ADAS IBM MVS to DEC UNIX conversion
;
; NAME: NAME401_CHECK()
;
; PURPOSE:
;       Checks whether a filename fits the rigid guidelines needed
;	for adas401.
;
; EXPLANATION:
;       This procedure accepts a filename as input and then checks
;	it against all valid filename structures for adas 401. If the
;	filename is a valid one then the procedure returns a 1, if
;	it is invalid then it returns a 0.
;
; USE:
;       See routine d1spf0.pro for an example.
;	Currently only used by adas 401 programs.
;
; INPUTS:
;       DSFULL	-	String: the filename to be tested.
;
; OPTIONAL INPUTS:
;       None.
;
; OUTPUTS:
;       RESULT	-	Integer: 1 for a valid name, 0 for invalid.
;
; OPTIONAL OUTPUTS:
;       None
;
; KEYWORD PARAMETERS:
;       None
;
; CALLS:
;       I4EIZ0	-	Converts elements symbol to number of electrons
;			(Used here to check the validity of element symbols
;			 in the file name).
;
; SIDE EFFECTS:
;       None
;
; WRITTEN:
;       Tim Hammond (Tessella Support Services plc), 7th September 1995
;
; MODIFIED:
;       1.1     Tim Hammond (Tessella Support Services plc)     
;		First release. Created specifically for adas401, but could
;               possibly be used by other routines later.
;	1.2	Tim Hammond
;		Updated routine to allow for case where the two indices
;		are of the form '##' which occurs for partial data files
;		of classes PLS and PLT.
;
; VERSION:
;       1.1     07-09-95
;	1.2	15-02-96
;		
;-
;-----------------------------------------------------------------------------

PRO name401_check, dsfull, result

    ON_ERROR, 2

		;**** Set up relevant variables ****

    result = 1			;assume initally the name is valid
    validstrings = ['acd','scd','ccd','prb','prc','qcd','xcd','plt','pls']
    start = strpos(dsfull, 'adf10')	;start position in string
    if start ne -1 then begin
	start = start + 6
    endif else begin
	start = 0
	result = 0
    endelse

		;**** Check first 3 letters ****

    if result eq 1 then begin
	first = strmid(dsfull, start, 3)
	firstchk = where(validstrings eq first)
	if (firstchk(0) eq -1) then result = 0
    endif

		;**** Check numbers ****

    if result eq 1 then begin
	second = strmid(dsfull, start+3, 3)
	secbyte = byte(second)
	if (max(secbyte) gt 57) or (min(secbyte) ne 47) then result = 0
    endif

		;**** Is there a prefix? ****

    if result eq 1 then begin
	pref = strpos(dsfull, '#')
	if pref(0) ne -1 then begin
	    if pref(0) eq start+8 then begin
		start = start+3
	    endif else if (pref(0) eq (start+13) or pref(0) 		$
	    eq (start+14)) and (firstchk(0) ge 7) then begin

		;*******************************************************
		;**** Do nothing - there is no prefix, but class is ****
		;**** type plt or pls and file is partial so 3 #'s  ****
		;**** are included in the file name                 ****
		;*******************************************************

	    endif else begin
		result = 0
	    endelse
	endif
    endif

		;**** Check next 5 equals first five ****

    if result eq 1 then begin
	third = strmid(dsfull, start+6, 5)
	if (third ne (first + strmid(second, 0,2))) then result = 0
    endif

		;**** Check underscore is next ****

    if result eq 1 then begin
	fourth = strmid(dsfull, start+11, 1)
	if fourth ne '_' then result = 0
    endif

		;**** Where is '.dat'? ****

    if result eq 1 then begin
	last = strpos(dsfull, '.dat')
	if last(0) eq -1 then result = 0
    endif

		;**** Check element symbol and any indices ****

    if result eq 1 then begin
	chars = last - (start+12)
	if chars gt 4 or chars lt 1 then begin
	    result = 0
	endif else begin			;Check element symbol
	    if chars lt 3 then begin
	        i4eiz0, strmid(dsfull, start+12, chars), nel
	         if nel eq 0 then result = 0
	    endif else begin
	        i4eiz0, strmid(dsfull, start+12, chars-2), nel
	        if nel eq 0 then result = 0
	        if result eq 1 then begin
		    indices = strmid(dsfull, start+12+chars-2, 2)
		    if firstchk(0) lt 7 then begin
		        indbyte = byte(indices)
		        if(min(indbyte) lt 49) or (max(indbyte) gt 53)	$
		        then result=0
		    endif else begin
			if indices ne '##' then result=0
		    endelse
	        endif
	    endelse
	endelse
    endif

		;**** If we get to here and result=1, file is valid ****

END
