; Copyright (c) 1995, Strathclyde University.
; SCCS info: Module @(#)$Header: /home/adascvs/idl/adas4xx/adas401/adas401_plot.pro,v 1.3 2004/07/06 10:38:24 whitefor Exp $ Date $Date: 2004/07/06 10:38:24 $     
;+
; PROJECT:
;       ADAS IBM MVS to UNIX conversion
;
; NAME:
;	ADAS401_PLOT
;
; PURPOSE:
;	Generates ADAS401 graphical output.
;
; EXPLANATION:
;	This routine creates a window for the display of graphical
;	output. A separate routine PLOT401 actually plots the
;	graph.
;
; USE:
;	This routine is very similar to that used for ADAS201.
;
; INPUTS:
;	(Most of these inputs map exactly onto variables of the same
;	 name in the FORTRAN code.)
;
;	DSFULL  - String; Name of data file 
;
;       CHEAD    - String; Heading to be used by FORTRAN and plotting.
;
;       Z1VAL    - Integer; the recombining ion charge
;
;	TITLX   - String; user supplied comment appended to end of title
;
;	TITLM   - String; Information about minimax fitting if selected.
;
;	UTITLE  - String; Optional comment by user added to graph title.
;
;       DATE    - String; Date of graph production
;
;	INTEMP  - FLoat array; input temperatures
;
;	MODTEMP - Float array; model temperatures
;
;	MODDENS - Float array; model densities
;
;	MINTEMP - Float array; minimax temperatures
;
;	APLOT   - Float array; input 'coefficients'
;
;	AOUT    - Float array; model coefficients
;
;	AOUTM   - Float array; minimax coefficients
;
;	YTITLE  - String; Title for y-axis
;
;       Z1VALUES - String array; String form of the charge states found
;                  in the input data file.
;
;       Z1INDEX  - Integer array; flags for which of the above z1values
;                  are to be plotted, a 1 means the corresponding
;                  value is to be plotted, a 0 means it is not.
;
;       IZE      - Integer; number of charge states (elements in z1values)
;
;	LDEF1	- Integer; 1 - use user entered graph scales
;			   0 - use default axes scaling
;
;	XMIN    - String; Lower limit for x-axis of graph, number as string.
;
;	XMAX    - String; Upper limit for x-axis of graph, number as string.
;
;	YMIN    - String; Lower limit for y-axis of graph, number as string.
;
;	YMAX    - String; Upper limit for y-axis of graph, number as string.
;
;	LFSEL	- Integer; 0 - No minimax fitting was selected 
;			   1 - Mimimax fitting was selected
;
;	LOSEL   - Integer; 0 - No interpolated values for spline fit. 
;			   1 - Intepolated values for spline fit.
;
;	ITE	- Integer - actual number of input temperatures
;
;	ITDVAL  - Integer; max. no of temp/dens pairs allowed
;
;       CELEM    - String; The element symbol extracted from the file name.
;
;       DATACLASS- String; The three letter code representing the dataclass.
;
;       YEAR     - String; The year of the data file - extracted from filename.
;
;       IPRNTDUM - Integer; The parent system index for a partial file.
;
;       IPSYSDUM - Integer; The spin system index for a partial file.
;
;	HRDOUT - Integer; 1 if hardcopy output activated, 0 if not.
;
;	HARDNAME- String; Filename for harcopy output.
;
;	DEVICE	- String; IDL name of hardcopy output device.
;
;	HEADER	- String; ADAS version number header to include in graph.
;
;       LPARTL   - Integer; Flag: 0=Standard file, 1=Partial file.
;
;       TEDA     - Float array; actual temps read from input file
;
;       IDSEL    - Integer; index of density selected for plot.
;
;       Z0VAL    - Integer; Element nuclear charge.
;
;       NEL      - Integer; Number of electrons.
;
;	EYSMB	 - String; the element symbol.
;
;       BITFILE - String; the path to the dirctory containing bitmaps
;                 for the 'escape to series menu' button.
;
; OPTIONAL INPUTS:
;	None.
;
; OUTPUTS:
;       GOMENU  - Int; flag - set to 1 if user has selected 'escape direct
;                 to series menu , otherwise 0
;
; OPTIONAL OUTPUTS:
;	None.
;
; KEYWORD PARAMETERS:
;	FONT	- String; The name of a font to use for text in the
;		  graphical output widget.
;
; CALLS:
;	CW_ADAS_GRAPH	Graphical output widget.
;	PLOT401		Make one plot to an output device for 401.
;	XMANAGER
;
; SIDE EFFECTS:
;	This routine uses a common block to maintain its state PLOT401_BLK.
;
;	One other routine is included in this file;
;	ADAS401_PLOT_EV	Called via XMANAGER during widget management.
;
; CATEGORY:
;	Adas system.
;
; WRITTEN:
;       Tim Hammond, Tessella Support Services plc,   8-Sep-1995
;
; MODIFIED:
;	1.1	Tim Hammond	
;		First release
;	1.2	Tim Hammond
;		Tidied up comments and code
;	1.3	William Osborn
;		Added menu button
; VERSION:
;	1.1	08-09-95
;	1.2	08-09-95
;	1.3	09-07-96
;-
;----------------------------------------------------------------------------

PRO adas401_plot_ev, event

    COMMON plot401_blk, data, action, win, plotdev, 			$
		        plotfile, fileopen, lfsel, 			$
                        losel, z1val, lpartl, itdval,			$
                        celem, dataclass, year, 			$
                        iprntdum, ipsysdum, teda, idsel, 		$
                        z0val, nel, esymb, gomenu


    newplot = 0
    print = 0
    done = 0
    gomenu = 0
		;****************************************
		;**** Set graph and device requested ****
		;****************************************

    CASE event.action OF

	'print'	   : begin
	    newplot = 1
	    print = 1
	end

	'done'	   : begin
	    if fileopen eq 1 then begin
	  	set_plot, plotdev
	  	device, /close_file
	    end
	    set_plot,'X'
	    widget_control, event.top, /destroy
	    done = 1
        end

        'bitbutton' : begin
            if fileopen eq 1 then begin
                set_plot, plotdev
                device, /close_file
            endif
            set_plot,'X'
            widget_control, event.top, /destroy
            done = 1
            gomenu = 1
        end

    END

		;*******************************
		;**** Make requested plot/s ****
		;*******************************

    if done eq 0 then begin

		;**** Set graphics device ****

        if print eq 1 then begin
            set_plot, plotdev
            if fileopen eq 0 then begin
                fileopen = 1
                device, filename=plotfile
	        device, /landscape
            endif
        endif else begin
            set_plot,'X'
            wset, win
        endelse

		;**** Draw graphics ****

        plot401, data.title, data.xtitle, data.ytitle, 			$
	         data.ldef1, data.xmin, data.xmax, data.ymin, data.ymax,$
                 data.z1values, data.z1index, data.ize, data.aplot, 	$
                 data.aout, data.aoutm, data.intemp,			$
                 data.modtemp, data.moddens, data.mintemp,		$
	         lfsel, losel, z1val, lpartl, itdval, celem, dataclass,	$
                 year, iprntdum, ipsysdum, teda(idsel), z0val, nel, esymb
    

        if print eq 1 then begin
	    message = 'Plot  written to print file.'
	    grval = {WIN:0, MESSAGE:message}
	    widget_control, event.id, set_value=grval
	endif

    endif

END

;----------------------------------------------------------------------------

PRO adas401_plot,   dsfull, chead, z1val, 				$
		    titlx, titlm, utitle, date, 			$
		    intemp, modtemp, moddens, mintemp,			$
		    aplot, aout, aoutm,      				$
		    ytitle, z1values, z1index, ize,   			$
                    ldef1,  xmin, xmax, ymin, ymax, 			$
                    lfsel, losel, ite, itdval,  			$
	            celem, dataclass, year, iprntdum, ipsysdum,		$
                    hrdout, hardname, device, header, lpartl, 		$
                    teda, idsel, z0val, nel, esymb, gomenu, bitfile,	$
		    FONT=font

    COMMON plot401_blk, data, action, win, plotdev, 			$
		        plotfile, fileopen, lfselcom, 			$
                        loselcom, z1valcom, lpartlcom,			$
		        itdvalcom, celemcom, classcom,			$
                        yearcom, prntcom, psyscom, tedacom, idselcom,	$
		        z0valcom, nelcom, esymbcom, gomenucom

		;**** Copy input values to common ****

    plotdev = device
    plotfile = hardname
    fileopen = 0
    lfselcom = lfsel
    loselcom = losel
    z1valcom = z1val
    lpartlcom = lpartl
    itdvalcom = itdval
    celemcom = celem
    classcom = dataclass
    yearcom = year
    prntcom = iprntdum
    psyscom = ipsysdum
    tedacom = teda
    idselcom = idsel
    z0valcom = z0val
    nelcom = nel
    esymbcom = esymb

		;************************************
		;**** Create general graph title ****
		;************************************

    title = strarr(6)

    title(0) = "COLLISIONAL-DIELECTRONIC COEFF. VS TEMPERATURE"
    if ( strtrim(strcompress(utitle),2)  ne ' ' ) then begin
        title(0) = title(0) + ': ' + strupcase(strtrim(utitle,2))
    endif
    title(1) =  'ADAS    :' + strupcase(header)
    title(2) =  'DATA    : '+strcompress(strupcase(chead))
    title(3) =  'FILE     : ' + strcompress(dsfull) 
    if (lfsel eq 1) then begin
        title(4)  = "MINIMAX : " + strupcase(titlm)
    endif 
  
		;**************************************
		;**** Set up x axis and xaxis title ***
		;**************************************

    xtitle = " REDUCED TEMPERATURE (K Z!D1!N!E-2!N) "

		;*************************************
		;**** Create graph display widget ****
		;*************************************

    graphid = widget_base(TITLE='ADAS401 GRAPHICAL OUTPUT', $
					XOFFSET=50,YOFFSET=0)
    bitval = bitfile + '/menu.bmp'
    cwid = cw_adas_graph(graphid,print=hrdout,FONT=font, bitbutton=bitval)

                ;**** Realize the new widget ****

    widget_control,graphid,/realize

		;**** Get the id of the graphics area ****

    widget_control,cwid,get_value=grval
    win = grval.win

		;*******************************************
		;**** Put the graphing data into common ****
		;*******************************************

    data = {title:title,  xtitle:xtitle, ytitle:ytitle, ldef1:ldef1, 	$
 	    xmin:xmin  ,  xmax:xmax    , ymin:ymin    ,	ymax:ymax  , 	$
            aplot:aplot,  aout:aout    , aoutm:aoutm  , intemp:intemp,	$
            modtemp:modtemp            , moddens:moddens             ,  $
            mintemp:mintemp            , z1values:z1values           ,  $
            z1index:z1index            , ize:ize		     }
 
    wset, win


    plot401, title, xtitle, ytitle, 					$
	     ldef1, xmin, xmax, ymin, ymax, z1values, z1index, ize,	$
	     aplot, aout, aoutm, intemp, modtemp, moddens, mintemp, 	$
             lfsel, losel,						$
	     z1val, lpartl, itdval, celem, dataclass, year, iprntdum,	$
             ipsysdum, teda(idsel), z0val, nel, esymb


		;***************************
                ;**** make widget modal ****
		;***************************

    xmanager,'adas401_plot', graphid, event_handler='adas401_plot_ev', $
             /modal, /just_reg

    gomenu = gomenucom

END
