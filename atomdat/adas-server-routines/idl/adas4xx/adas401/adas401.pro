; Copyright (c) 1995, Strathclyde University.
; SCCS info: Module @(#)$Header: /home/adascvs/idl/adas4xx/adas401/adas401.pro,v 1.10 2004/07/06 10:38:03 whitefor Exp $ Date $Date: 2004/07/06 10:38:03 $
;+
; PROJECT:
;       ADAS IBM MVS to UNIX conversion
;
; NAME:
;	ADAS401
;
; PURPOSE:
;	The highest level routine for the ADAS 401 program.
;
; EXPLANATION:
;	This routine is called from the main adas system routine, adas.pro,
;	to start the ADAS 401 application.  Associated with adas401.pro
;	is a FORTRAN executable.  The IDL code provides the user
;	interface and output graphics whilst the FORTRAN code reads
;	in data files, performs numerical processing and creates the
;	output files.  The IDL code communicates with the FORTRAN
;	executable via a bi-directional UNIX pipe.  The unit number
;	used by the IDL for writing to and reading from this pipe is
;	allocated when the FORTRAN executable is 'spawned' (see code
;	below).  Pipe communications in the FORTRAN process are to
;	stdin and stdout, i.e streams 5 and 6.
;
;	The FORTRAN code is an independent process under the UNIX system.
;	The IDL process can only exert control over the FORTRAN in the
;	data which it communicates via the pipe.  The communications
;	between the IDL and FORTRAN must be exactly matched to avoid
;	input conversion errors.  The correct ammounts of data must be
;	passed so that neither process 'hangs' waiting for communications
;	which will never occur.
;
;	The FORTRAN code performs some error checking which is
;	independent of IDL.  In cases of error the FORTRAN may write
;	error messages.  To prevent these error messages from conflicting
;	with the pipe communications all FORTRAN errors are written to
;	output stream 0, which is stderr for UNIX.  These error messages
;	will appear in the window from which the ADAS session/IDL session
;	is being run.
;
;	In the case of severe errors the FORTRAN code may terminate
;	itself prematurely.  In order to detect this, and prevent the
;	IDL program from 'hanging' or crashing, the IDL checks to see
;	if the FORTRAN executable is still an active process before
;	each group of pipe communications.  The process identifier
;	for the FORTRAN process, PID, is recorded when the process is
;	first 'spawned'.  The system is then checked for the presence
;	of the FORTRAN PID.
;
; USE:
;	First the system settings must be established by calling
;	adas_sys_set.pro then adas401.pro is called to start the
;	ADAS 401 application;
;
;	adas_sys_set, adasrel, fortdir, userroot, centroot, $
;		      devlist, devcode, font_large, font_small, edit_fonts
;	adas401,   adasrel, fortdir, userroot, centroot, devlist, $
;		   devcode, font_large, font_small, edit_fonts
;
; INPUTS:
;	ADASREL - A string indicating the ADAS system version, 
;		  e.g ' ADAS RELEASE: ADAS93 V1.1.0'.  The first
;		  character should be a space.
;
;	FORTDIR - A string holding the path to the directory where the
;		  FORTRAN executables are, e.g '/disk/adas/fortran/exec'
;
;	USERROOT - A string holding the path to the root directory of
;		   the user's adas data. e.g '/disk/bowen/adas'
;		   This root directory will be used by adas to construct
;		   other path names.  For example the users default data
;		   for adas205 should be in /disk/bowen/adas/adf04.  In
;		   particular the user's default interface settings will
;		   be stored in the directory USERROOT+'/defaults'.  An
;		   error will occur if the defaults directory does not
;		   exist.
;
;	CENTROOT - Like USERROOT, but this directory points to the
;		   central data area for the system.  User defaults are
;		   not stored on CENTROOT.
;
;	DEVLIST - A string array of hardcopy device names, used for
;		  graphical output. e.g ['Post-Script','HP-PCL','HP-GL']
;		  This array must mirror DEVCODE.  DEVCODE holds the
;		  actual device names used in a SET_PLOT statement.
;
;	DEVCODE - A string array of hardcopy device code names used in
;		  the SET_PLOT statement, e.g ['PS', 'PCL', 'HP']
;		  This array must mirror DEVLIST.
;
;	FONT_LARGE - The name of a larger font e.g 'courier_bold14'
;
;	FONT_SMALL - The name of a smaller font e.g 'courier_bold12'
;
;	EDIT_FONTS - A structure of two fonts used in the table editor
;		     adas_edtab.pro, {font_norm:'',font_input:''}.
;		     The two fonts are used to differentiate between
;		     editable and non-editable parts of the table. You
;		     may have to experiment to get two fonts which give
;		     the same default spacing in the edit widget.
;		     e.g {font_norm:'helvetica_bold14', $
;			  font_input:'helvetica_oblique14'}
;
; OPTIONAL INPUTS:
;	None
;
; OUTPUTS:
;	None - Note this routine should leave all inputs un-modified.
;
; OPTIONAL OUTPUTS:
;	None
;
; KEYWORD PARAMETERS:
;	None
;
; CALLS:
;	FIND_PROCESS	Checks to see if a given process is active.
;       XXDATE          Get date and time from operating system.
;       D1SPF0          Pipe comms with FORTRAN D1SPF0 routine.
;       D1SETP          Pipe comms with FORTRAN D1SETP routine.
;       D1ISPF          Pipe comms with FORTRAN D1ISPF routine.
;       D1SPF1          Pipe comms with FORTRAN D1SPF1 routine.
;       D1OUTG          Pipe comms with FORTRAN D1OUTG routine.
;
; SIDE EFFECTS:
;	This routine spawns a FORTRAN executable.  Note the pipe 
;	communications routines listed above.  In addition to these
;	pipe communications there is one explicit communication of the
;	date to the FORTRAN code, search for 'printf,pipe' to find it.
;
; CATEGORY:
;	Adas system.
;	
; WRITTEN:
;       Tim Hammond, Tessella Support Services plc, 20/07/1995
;
; MODIFIED:
;	1.1	Tim Hammond		
;		First release
;	1.2	Tim Hammond
;		Added extra input/output error check and useful
;		message for if FORTRAN crashes
;	1.3	Tim Hammond
;		Updated version number to 1.2
;	1.4	Tim Hammond
;		Added flexible use of fonts for input screen depending
;		on target platform
;	1.5	Tim Hammond
;		Added flexible use of fonts for output screen depending
;		on target platform
;	1.6	Tim Hammond
;		Updated version number to 1.3
;	1.7	William Osborn
;		Added menu buttons
;	1.8	William Osborn
;		Updated version number to 1.4
;	1.9	William Osborn
;		Updated version number to 1.5
;	1.10  Richard Martin
;		Increased version number to 1.6.
;
; VERSION:
;       1.1	20-07-95
;	1.2	15-02-96
;	1.3	16-02-96
;	1.4	27-02-96
;	1.5	27-02-96
;	1.6	13-05-96
;	1.7	09-07-96
;	1.8	11-07-96
;	1.9	14-10-96
;	1.10  01-11-97
;
;-
;-----------------------------------------------------------------------------

PRO ADAS401,	adasrel, fortdir, userroot, centroot, 			$
		devlist, devcode, font_large, font_small, edit_fonts

		;************************
		;**** Initialisation ****
		;************************

    ON_ERROR, 2
    adasprog = ' PROGRAM: ADAS401 V1.6'
    lpend = 0
    deffile = userroot+'/defaults/adas401_defaults.dat'
    device = ''
    gomenu = 0
    bitfile = centroot+'/bitmaps'

		;******************************************
		;**** Search for user default settings ****
		;**** If not found create defaults     ****
		;******************************************

    files = findfile(deffile)
    if files(0) eq deffile then begin
        restore, deffile
        inval.centroot = centroot+'/adf10/'
        inval.userroot = userroot+'/adf10/'
    end else begin
        inval = { 							$
		  ROOTPATH:userroot+'/adf10/', 				$
		  FILE:'', 						$
		  CENTROOT:centroot+'/adf10/', 				$
		  USERROOT:userroot+'/adf10/'}
	searchval = {							$
	          class:0,						$
		  year:'',						$
		  symbol:'',						$
		  file:0,						$
		  first:0,						$
		  second:0,						$
		  member:''}     
  
      procval = {NEW:-1}

      ones = replicate(1, 100)
      outval = { 							$
                 GRPOUT:0, GTIT1:'', GRPSCAL:0, 			$
 	  	  IDSEL:0, Z1INDEX:ones,				$
                  XMIN:'',  XMAX:'',   					$
                  YMIN:'',  YMAX:'',   					$
                  HRDOUT:0, HARDNAME:'', 				$
                  GRPDEF:'', GRPFMESS:'',				$
                  GRPSEL:-1, GRPRMESS:'', 				$
                  DEVSEL:-1, GRSELMESS:'', 				$
                  TEXOUT:0, TEXAPP:-1, 					$
                  TEXREP:0, TEXDSN:'', 					$
                  TEXDEF:'paper.txt',TEXMES:'' 				$
               }

    end


		;****************************
		;**** Start fortran code ****
		;****************************

    spawn, fortdir+'/adas401.out', unit=pipe, /noshell, PID=pid

		;************************************************
		;**** Get date and write to fortran via pipe ****
		;************************************************

    date = xxdate()
    printf, pipe, date(0)

LABEL100:
		;**** Check FORTRAN still running ****
  
    if find_process(pid) eq 0 then goto, LABELEND

		;************************************************
		;**** Communicate with d1spf0 in fortran and ****
		;**** invoke user interface widget for       ****
		;**** Data file selection                    ****
		;**** Select font depending on the platform  ****
		;************************************************

    machine = GETENV('TARGET_MACHINE')
    if machine eq 'HPUX' then begin
	infont = font_small
    endif else begin
	infont = font_large
    endelse
    outfont = infont
    d1spf0, pipe, inval, searchval, dsfull, rep, classval, yearval, 	$
            isoval, indexval, fileval, nel, ipsysdum, celem, chead, 	$
	    lpartl, dataclass, year, iprntdum, iswit, FONT=infont

		;**** If cancel selected then end program ****

    if rep eq 'YES' then goto, LABELEND

		;**** If filename was invalid then loop back ****

    if iswit eq -1 then goto, LABEL100

		;**** Check FORTRAN still running ****

    if find_process(pid) eq 0 then goto, LABELEND

		;**** Was there an error during input? ****

    idum = 0
    ON_IOERROR, LABELERROR
    readf, pipe, idum
    if idum eq 1 then goto, LABEL100		;was an error
   

		;*************************************
		;**** Read pipe comms from d1setp ****
		;*************************************

    d1setp, pipe, ize, zipt
    z1values = strarr(ize)
    for i=0, (ize-1) do begin
        z1values(i) = 'Z1 = '+strtrim(string(fix(zipt(i))),2)
    endfor



LABEL200:
		;**** Check FORTRAN still running ****

    if find_process(pid) eq 0 then goto, LABELEND

		;************************************************
		;**** Communicate with d1ispf in fortran and ****
		;**** invoke user interface widget for       ****
		;**** Processing options                     ****
		;************************************************

    d1ispf, pipe, lpend, procval, dsfull, 				$
	    classval, yearval, isoval, indexval, fileval, 		$
            nel, ipsysdum, celem, 					$
	    ide, teda, gomenu, bitfile,					$
	    FONT_LARGE=font_large, FONT_SMALL=font_small, 		$
            EDIT_FONTS=edit_fonts

		;**** If menu button clicked, tell FORTRAN to stop ****

    if gomenu eq 1 then begin
 	printf, pipe, 1
 	goto, LABELEND
    endif else begin
 	printf, pipe, 0
    endelse

		;**** If cancel selected then goto 100 ****

    if lpend eq 1 then goto, LABEL100

		;**** Fortran processing now in progress. Get ****
		;**** date and time for this run.	      ****

    date = xxdate()

		;**** Create header for output. ****

    header = adasrel+adasprog+' DATE: '+date(0)+' TIME: '+date(1)

		;**** Check FORTRAN still running ****

    if find_process(pid) eq 0 then goto, LABELEND

LABEL300:
		;**** Check FORTRAN still running ****

    if find_process(pid) eq 0 then goto, LABELEND

		;************************************************
		;**** Communicate with d1spf1 in fortran and ****
		;**** invoke user interface widget for       ****
		;**** Output options                         ****
		;************************************************



    d1spf1, pipe, lpend, outval, dsfull, header, ide, teda,		$
	    z1values, z1index, DEVLIST=devlist, FONT=outfont, ize,	$
	    gomenu, bitfile


		;**** If menu button clicked, tell FORTRAN to stop ****

    if gomenu eq 1 then begin
 	printf, pipe, 1
 	goto, LABELEND
    endif else begin
 	printf, pipe, 0
    endelse

		;**** If cancel selected then erase output ****
		;**** messages and goto 200.		   ****

    if lpend eq 1 then begin
        outval.texmes = ''
        outval.z1index = 1		;reset the indices
        goto, LABEL200
    end

		;*************************************************
		;**** If text output has been requested write ****
		;**** header to pipe for XXADAS to read.      ****
		;*************************************************

    if (lpend eq 0 and outval.texout eq 1) then begin
        printf, pipe, header
    endif

		;************************************************
                ;**** If graphical output requested          ****
		;**** Communicate with c1outg in fortran and ****
		;**** invoke widget Graphical output         ****
		;************************************************

    if outval.grpout eq 1 then begin

               ;**** User explicit scaling ****

        grpscal = outval.grpscal
        xmin = outval.xmin
        xmax = outval.xmax
        ymin = outval.ymin
        ymax = outval.ymax
  
		;**** Hardcopy output ****

        hrdout = outval.hrdout
        hardname = outval.hardname
        if outval.devsel ge 0 then device = devcode(outval.devsel)

                ;*** user title for graph ***

        utitle = outval.gtit1
  
        d1outg, dsfull, pipe, utitle, grpscal, xmin, xmax, ymin, ymax, 	$
                z1values,outval.z1index, hrdout, hardname, device, 	$
                header, ize, chead, procval.z1val, lpartl, 		$
                celem, dataclass, year, iprntdum, ipsysdum,		$
                teda, outval.idsel, procval.z0val, nel, gomenu, bitfile,$
                FONT=font_large		

		;**** If menu button clicked, tell FORTRAN to stop ****

    	if gomenu eq 1 then begin
 	    printf, pipe, 1
 	    goto, LABELEND
    	endif else begin
 	    printf, pipe, 0
    	endelse

    end

		;**** Back for more output options ****

    GOTO, LABEL300

LABELERROR:
    print, '*********************** ADAS401 ERROR *************************'
    print, '*****                                                     *****'
    print, '***** A FATAL ERROR OCCURRED IN THE FORTRAN CHILD PROCESS *****'
    print, '*****                                                     *****'
    print, '********************* ADAS401 TERMINATING *********************'

LABELEND:


		;**** Save user defaults ****

    save, inval, procval, outval, searchval, filename=deffile

END
