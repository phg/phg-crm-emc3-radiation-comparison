; Copyright (c) 1995 Strathclyde University .
; SCCS info: Module @(#)$Header: /home/adascvs/idl/adas4xx/adas401/adas401_in.pro,v 1.2 2004/07/06 10:38:08 whitefor Exp $ Date $Date: 2004/07/06 10:38:08 $
;+
; PROJECT:
;       ADAS IBM MVS to DEC UNIX conversion
;
; NAME:
;	ADAS401_IN
;
; PURPOSE:
;	IDL ADAS user interface, input file selection.
;
; EXPLANATION:
;	This routine creates and manages a pop-up window which allows
;	the user to select a file from the UNIX file system.  
;	This is an extension of the usual pop-up window for input file
;	selection due to the different ways in which files can be 
;	chosen for the series 4 routines. This routine is specific
;	to adas401. It includes two methods of file selection:
;	1 - The standard method where the user can move around
;	    all subdirectories of the data type adf10 and select
;	    the required file manually.
;	2 - A new method which allows the user to specify search parameters
;	    then search for a file which matches these parameters. Finally
;	    the user can select a file from a list which is produced
;	    by the search - assuming that matches are found.
;
; USE:
;	See d1spf0.pro for an example of how to
;	use this routine.
;
; INPUTS:
;	VAL	- A structure which determines the initial settings of
;		  the dataset selection widget.  VALUE is passed un-modified
;		  through to cw_adas401_in.pro, see that routine for a full
;		  description.
;
;	SEARCH	- A structure holding the value of the file selection
;		  widget - passed unmodifed through to cw_adas401_in.pro,
;		  see that routine for a full description.
;
; OPTIONAL INPUTS:
;	None
;
; OUTPUTS:
;	VAL	- On output the structure records the final settings of
;		  the dataset selection widget if the user pressed the
;		  'Done' button otherwise it is not changed from input.
;
;	SEARCH -  On output the structure records the final settings of
;                 the file search widget if the user pressed the
;                 'Done' button otherwise it is not changed from input.
;
;	ACT	- A string; indicates the user's action when the pop-up
;		  window is terminated, i.e which button was pressed to
;		  complete the input.  Possible values are 'Done' and
;		  'Cancel'.
;
;	OUTVALUE- Structure which holds VALUE and SEARCH structures.
;		  Used to simplify the method used to get hold of
;		  the final values from the input options screen
;		  as it is only possible to directly get a single
;		  structure from there. 
;
; OPTIONAL OUTPUTS:
;	None
;
; KEYWORD PARAMETERS:
;	WINTITLE- A title to be used on the banner for the pop-up window.
;		  Intended to indicate which application is running,
;		  e.g 'ADAS 205 INPUT'
;
;	TITLE	- The title to be included in the input file widget, used
;		  to indicate exactly what the required input dataset is,
;		  e.g 'Input COPASE Dataset'
;
;	FONT	- Supplies the font to be used for the interface widgets.
;
; CALLS:
;	CW_ADAS401_IN	Dataset selection widget creation.
;	XMANAGER	Manages the pop=up window.
;	ADAS401_IN_EV	Event manager, called indirectly during XMANAGER
;			event management.
;
; SIDE EFFECTS:
;	XMANAGER is called in /modal mode. Any other widget become
;	inactive.
;
; CATEGORY:
;	Adas system.
;
; WRITTEN:
;       Andrew Bowen, Tessella Support Services plc, 17-May-1993
;
; MODIFIED:
;	1.1	Tim Hammond (Tessella Support Services)		07-09-95
;	1.2	Tim Hammond					27-09-95
; VERSION:
;       1.1     First release. Created from skeleton of adas205_in
;               written by A. Bowen.
;	1.2	Changed name of common block to avoid possible clashes
;		with other routines.
;-
;-----------------------------------------------------------------------------


PRO adas401_in_ev, event

    COMMON in401_blk, action, value, searchval

		;**** Find the event type and copy to common ****

    action = event.action

    CASE action OF

		;**** 'Done' button ****

	'Done'  : begin

		;**** Get the output widget value ****

	    widget_control,event.id,get_value=value 
	    widget_control,event.top,/destroy

	end


		;**** 'Cancel' button ****

	'Cancel': widget_control,event.top,/destroy

    END

END

;-----------------------------------------------------------------------------


PRO adas401_in, val, search, act, WINTITLE = wintitle, 			$
                TITLE = title, FONT = font, outvalue

    COMMON in401_blk, action, value, searchval

		;**** Copy value to common ****

    value = val
    searchval = search

		;**** Set defaults for keywords ****

    IF NOT (KEYWORD_SET(wintitle)) THEN wintitle = 'ADAS INPUT FILE'
    IF NOT (KEYWORD_SET(title)) THEN title = ''
    IF NOT (KEYWORD_SET(font)) THEN font = ''

		;***************************************
		;**** Pop-up a new widget if needed ****
		;***************************************

                ;**** create base widget ****

    inid = widget_base(TITLE=wintitle, XOFFSET=100, YOFFSET=1)

		;**** Declare output options widget ****

    cwid = cw_adas401_in(inid, outvalue, VALUE=value, SEARCHVAL=searchval,$
                         TITLE=title, FONT=font)

		;**** Realize the new widget ****

    widget_control, inid, /realize

		;**** make widget modal ****

    xmanager, 'adas401_in', inid, event_handler='adas401_in_ev', /modal,$
              /just_reg
 
		;**** Return the output value from common ****

    act = action
    val = value
    search = searchval
    outvalue = {value:value,  searchval:searchval}

END

