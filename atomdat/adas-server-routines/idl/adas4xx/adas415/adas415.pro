; Copyright (c) 2000 Strathclyde University .
; SCCS info: Module @(#)$Header: /home/adascvs/idl/adas4xx/adas415/adas415.pro,v 1.3 2004/07/06 10:49:48 whitefor Exp $ Date $Date: 2004/07/06 10:49:48 $
;+
; PROJECT:
;       ADAS
;
; NAME:
;       ADAS415
;
; PURPOSE:
;       The highest level routine for the ADAS 415 program.
;
; EXPLANATION:
;       This routine is called from the main adas system routine,
;       adas.pro, to start the ADAS 415 application. This is the
;       first ADAS program to be fully written in IDL, which may
;       or may not be a good thing. 
;
;       There is no processing screen.
;
;       The purpose of ADAS415 is to plot the filter function. This
;       can be produced with ADAS414 or by other means. See the 
;       manual for file specification.
;
;
; USE:
;       First the system settings must be established by calling
;       adas_sys_set.pro then adas415.pro is called to start the
;       ADAS 415 application;
;
;       adas_sys_set, adasrel, fortdir, userroot, centroot, $
;                     devlist, devcode, font_large, font_small, edit_fonts
;       adas415,   adasrel, fortdir, userroot, centroot, devlist, $
;                  devcode, font_large, font_small, edit_fonts
;
; INPUTS:
;       ADASREL - A string indicating the ADAS system version,
;                 e.g ' ADAS RELEASE: ADAS93 V1.4'.  The first
;                 character should be a space.
;
;       FORTDIR - A string holding the path to the directory where the
;                 FORTRAN executables are, e.g '/disk/adas/fortran/exec'
;                 Not used but retainned for compatibility.
;
;       USERROOT - A string holding the path to the root directory of
;                  the user's adas data. e.g '/disk/bowen/adas' This
;                  root directory will be used by adas to construct
;                  other path names.  In particular the user's default
;                  interface settings will be stored in the directory
;                  USERROOT+'/defaults'.  An error will occur if the
;                  defaults directory does not exist.
;
;       CENTROOT - Like USERROOT, but this directory points to the
;                  central data area for the system.  User defaults are
;                  not stored on CENTROOT.
;
;       DEVLIST - A string array of hardcopy device names, used for
;                 graphical output. e.g ['Post-Script','HP-PCL','HP-GL']
;                 This array must mirror DEVCODE.  DEVCODE holds the
;                 actual device names used in a SET_PLOT statement.
;                 Not used but retainned for compatibility.
;
;       DEVCODE - A string array of hardcopy device code names used in
;                 the SET_PLOT statement, e.g ['PS', 'PCL', 'HP']
;                 This array must mirror DEVLIST.
;                 Not used but retainned for compatibility.
;
;       FONT_LARGE - The name of a larger font e.g 'courier_bold14'
;
;       FONT_SMALL - The name of a smaller font e.g 'courier_bold12'
;
;       EDIT_FONTS - A structure of two fonts used in the table editor
;                    adas_edtab.pro, {font_norm:'',font_input:''}.
;                    The two fonts are used to differentiate between
;                    editable and non-editable parts of the table. You
;                    may have to experiment to get two fonts which give
;                    the same default spacing in the edit widget.
;                    e.g {font_norm:'helvetica_bold14', $
;                         font_input:'helvetica_oblique14'}
;                  Not used but retainned for compatibility.
;
; OPTIONAL INPUTS:
;       None
;
; OUTPUTS:
;       None - Note this routine should leave all inputs un-modified.
;
; OPTIONAL OUTPUTS:
;       None
;
; KEYWORD PARAMETERS:
;       None
;
; CALLS:
;       ADAS415_IN    Prompts for and gathers input files
;       ADAS415_PROC  Tests files for data consistency
;       ADAS415_OUT   Prompts for and writes output files
;       XXDATE        Get date and time from operating system.
;       POUP          Puts up a messahe on the screen.
;
; SIDE EFFECTS:
;       Some system calls in the called functions.
;
; NOTES:
;       This program requires IDL v5. This is tested for at the beginning.
;       The mode of operation is different to other ADAS programs in that
;       the 3 called routines are functions which return the variable 'rep'.
;       rep can be 'CONTINUE', 'CANCEL' or 'MENU' and subsequent action
;       depends on the returned value.
;       As a side note there are no common blocks!
;
; CATEGORY:
;       Adas system.
;
; WRITTEN:
;       Martin O'Mullane
;
; MODIFIED:
;       1.1     Martin O'Mullane
;                - First release
;       1.2     Martin O'Mullane
;                - Files are strored in adf35 and not adf34.
;		     - Increased version no. to 1.2. 
;	  1.3		Richard Martin
;			Increased version no to 1.3
;
; VERSION:
;       1.1     16-08-1999
;       1.2     25-01-2001
;	  1.3		18-03-02
;
;-
;-----------------------------------------------------------------------------

PRO ADAS415,    adasrel, fortdir, userroot, centroot,                   $
                devlist, devcode, font_large, font_small, edit_fonts

                ;************************
                ;**** Initialisation ****
                ;************************

    adasprog = ' PROGRAM: ADAS415 V1.3'
    deffile  = userroot+'/defaults/adas415_defaults.dat'
    bitfile  = centroot+'/bitmaps'
    device   = ''
                
    

                ;******************************************
                ;**** Search for user default settings ****
                ;**** If not found create defaults     ****
                ;**** inval: settings for data files   ****
                ;**** outval: settings for output      ****
                ;******************************************

    files = findfile(deffile)
    if files(0) eq deffile then begin
    
        restore, deffile
        inval.centroot = centroot+'/adf35/'
        inval.userroot = userroot+'/adf35/'
    
    endif else begin
        
        inval =   { ROOTPATH    :   userroot+'/adf35/',   $
                    FILE        :   '',                   $
                    CENTROOT    :   centroot+'/adf35/',   $
                    USERROOT    :   userroot+'/adf35/'    }
                  
        procval = { NEW         :  -1                     }

        outval  = { TEXOUT      :   1,                    $
                    TEXAPP      :   -1,                   $
                    TEXREP      :   1,                    $
                    TEXDSN      :   '',                   $
                    TEXDEF      :   'paper.txt',          $
                    TEXMES      :   ' ',                  $
                    GRPOUT      :   0,                    $
                    GTIT1       :   '',                   $
                    GRPSCAL     :   0,                    $
                    XMIN        :   '',                   $
                    XMAX        :   '',                   $
                    YMIN        :   '',                   $
                    YMAX        :   '',                   $
                    HRDOUT      :   0,                    $
                    HARDNAME    :   '',                   $
                    GRPDEF      :   '',                   $
                    GRPFMESS    :   '',                   $
                    GRPSEL      :   -1,                   $
                    GRPRMESS    :   '',                   $
                    DEVSEL      :   -1,                   $
                    GRSELMESS   :   ''                    }

    endelse                                               
    

                   
                ;***********************************************
                ;**** Make sure we are running v5 or above. ****
                ;***********************************************

    thisRelease = StrMid(!Version.Release, 0, 1)
    IF thisRelease LT '5' THEN BEGIN
       message = 'Sorry, ADAS415 requires IDL 5 or higher' 
       tmp = popup(message=message, buttons=['Accept'],font=font_large)
       goto, LABELEND
    ENDIF


LABEL100:

    ; These are fixed for each run
    
    outval.texapp   = -1
    
                ;********************************************
                ;**** Invoke user interface widget for   ****
                ;**** Data file selection                ****
                ;********************************************


    rep=adas415_in(inval,  $
                   FONT_LARGE=font_large, FONT_SMALL = font_small)
 
    if rep eq 'CANCEL' then goto, LABELEND



                ;************************************************
                ;**** Probe the file selection and show what ****
                ;**** was found. Pass back the info for      ****
                ;**** possible later processing.             ****
                ;************************************************


LABEL200:


;    rep=adas415_proc(inval,  procval,                  $
;                     FONT_LARGE=font_large, FONT_SMALL = font_small)
    
    
    if rep eq 'MENU'   then goto, LABELEND
    if rep eq 'CANCEL' then goto, LABEL100


LABEL300:

                ;************************************************
                ;**** Communicate with d4spf1 in fortran and ****
                ;**** invoke user interface widget for       ****
                ;**** Output options                         ****
                ;************************************************

                ;**** Create header for output. ****

    date   = xxdate()
    header = adasrel+adasprog+' DATE: '+date(0)+' TIME: '+date(1)
    
    dsfull = inval.rootpath+inval.file
    
    rep=adas415_out(procval, outval,   dsfull,                $
                    header, bitfile,                   $
                    DEVLIST = devlist,                 $
                    FONT_LARGE=font_large, FONT_SMALL = font_small)
    
   
    if rep eq 'MENU'   then goto, LABELEND
    if rep eq 'CANCEL' then goto, LABEL100

    
    
                ;**** Read in the data from the file ****
    
    if (outval.grpout EQ 1 OR outval.texout EQ 1) then begin
    
       str   = ''
       npts  = 0
       nedge = 0
       
       openr,lun,dsfull, /get_lun
       readf,lun, npts, nedge
       readf,lun, str
       edges = fltarr(nedge)
       readf, lun, edges
       readf,lun, str
       xfile = fltarr(npts)
       yfile = fltarr(npts)
       readf,lun,xfile
       readf,lun,yfile
       
       yfile =10.0^yfile
    
    endif 
                ;**** Is graphical output required? ****
    
    if outval.grpout eq 1 then begin
    
       ; these from procval or file
       xdata=xfile
       ydata=yfile
       ival=n_elements(xdata)
       ldfit = 0
       
       ldef     = outval.grpscal
       xmin     = outval.xmin
       xmax     = outval.xmax
       ymin     = outval.ymin
       ymax     = outval.ymax
       hrdout   = outval.hrdout
       hardname = outval.hardname
       utitle   = outval.gtit1
       if outval.devsel ge 0 then device = devcode(outval.devsel)
       
       rep = adas415_plot(dsfull , utitle   ,                            $
                          ldfit  , ival     , xdata   , ydata  ,         $
                          ldef   , xmin     , xmax    , ymin   , ymax,   $
                          hrdout , hardname , device  ,                  $
                          header , bitfile  ,                            $
                          devcode, devlist  ,                            $
                          FONT_LARGE=font_large, FONT_SMALL = font_small)
       
       if rep eq 'MENU'   then goto, LABELEND

    endif 
    
                ;**** Back for more output options ****

    GOTO, LABEL300

LABELEND:


                ;**** Save user defaults ****

    save, inval,  outval, filename=deffile


END
