; Copyright (c) 2000 Strathclyde University .
; SCCS info: Module @(#)$Header: /home/adascvs/idl/adas4xx/adas415/adas415_plot.pro,v 1.1 2004/07/06 10:50:05 whitefor Exp $ Date $Date: 2004/07/06 10:50:05 $
;+
; PROJECT:
;       ADAS
;
; NAME:
;        ADAS415_PLOT
;
; PURPOSE:
;        Generates ADAS415 graphical output.
;
; EXPLANATION:
;        This routine creates a window for the display of graphical
;        output. A separate routine PLOT415 actually plots the
;        graph.
;
; USE:
;        This routine is specific to ADAS415, see b1outg.pro for
;        example use.
;
; INPUTS:
;        (Most of these inputs map exactly onto variables of the same
;         name in the FORTRAN code.)
;
;
;        DSFULL   - String; Name of data file
;
;        TITLX    - String; user supplied comment appended to end of title
;
;        UTITLE   - String; Optional comment by user added to graph title.
;
;        TEMP         - Double array; temperatures from input data file.
;
;        RATE     - Double array; Excitation rate coefficient of selected
;                                 transition from input data file
;
;        TOSA         - Double array; Selected temperatures for spline fit
;
;        ROSA         - Double array; Excitation rate coefficients for splinefit.
;
;        TOMA         - Double array; Selected temperatures for minimax fit
;
;        DATE         - String; Date of graph production
;
;        LDEF1         - Integer; 1 - use user entered graph scales
;                            0 - use default axes scaling
;
;        XMIN    .- String; Lower limit for x-axis of graph, number as string
;
;        XMAX    .- String; Upper limit for x-axis of graph, number as string
;
;        YMIN    .- String; Lower limit for y-axis of graph, number as string
;
;        YMAX    .- String; Upper limit for y-axis of graph, number as string
;
;        LFSEL         - Integer; 0 - No minimax fitting was selected
;                            1 - Mimimax fitting was selected
;
;        LOSEL    - Integer; 0 - No interpolated values for spline fit.
;                            1 - Intepolated values for spline fit.
;
;        NMX         - Integer; Number of temperatures used for minimax fit
;
;        NENER    - Integer; Number of user entered temperature values
;
;        NPSPL         - Integer; Number of interpolated values for spline.
;
;        HRDOUT   - Integer; 1 if hardcopy output activated, 0 if not.
;
;        HARDNAME - String; Filename for harcopy output.
;
;        DEVICE   - String; IDL name of hardcopy output device.
;
;        HEADER   - String; ADAS version number header to include in graph.
;
;        BITFILE  - String; the path to the dirctory containing bitmaps
;                   for the 'escape to series menu' button.
;
; OPTIONAL INPUTS:
;        None.
;
; OUTPUTS:
;       REP  -  'MENU' if user has selected 'escape direct to series menu , 
;                otherwise 'CONTINUE'
;
; OPTIONAL OUTPUTS:
;        None.
;
; KEYWORD PARAMETERS:
;        FONT        - String; The name of a font to use for text in the
;                  graphical output widget.
;
; CALLS:
;        CW_ADAS_GRAPH        Graphical output widget.
;        PLOT415                Make one plot to an output device for 415.
;        XMANAGER
;
; SIDE EFFECTS:
;
;        One other routine is included in this file;
;        ADAS415_PLOT_EV        Called via XMANAGER during widget management.
;
; CATEGORY:
;        Adas system.
;
; WRITTEN:
;       Martin O'Mullane
;
; MODIFIED:
;       1.1     Martin O'Mullane
;               First release
;
; VERSION:
;       1.1     09-11-99
;
;-
;----------------------------------------------------------------------------

PRO plot415, winNum, ival,  x, y, title,                 $
             xu_ind, xunits, xtitles, yunits, ytitle,    $
             ldef, xmin, xmax, ymin, ymax, xlog, ylog,   $
             PRINT = print

  
  COMMON Global_lw_data, left, right, top, bottom, grtop, grright


; Set up plot decoration

    charsize    = (!d.y_vsize/!d.y_ch_size)/60.0 
    small_check = GETENV('VERY_SMALL')
    if small_check eq 'YES' then charsize=charsize*0.8
    
    xunit  = xunits(xu_ind)
    xtitle = xtitles(xu_ind)
    gtitle = title(0) + "!C!C" + title(1) + "!C!C" + title(2) 
    
    if ldef eq 0 then begin
    	xvals = where(x gt 1.0e-37 and x lt 1.0e37, count_x)
    	yvals = where(y gt 1.0e-37 and y lt 1.0e37, count_y)
        if (count_x EQ 0 OR count_y EQ 0) then begin
            makeplot = 0 
        endif else begin
           makeplot = 1            
           xmax = max(x(xvals), min=xmin)
           ymax = max(y(yvals), min=ymin)
        endelse
        xstyle = 0
    endif else begin
    	xvals = where(x ge xmin and x le xmax, count_x)
    	yvals = where(y ge ymin and y le ymax, count_y)
        if (count_x EQ 0 OR count_y EQ 0) then makeplot = 0 else makeplot = 1
    	xstyle = 1
    endelse
    ystyle = 0
  
    
; Choose appropriate output device
    
    if (keyword_set(print)) then begin
     
       set_plot,print.dev
     
       if print.paper eq 'A4' then begin
          xsize = 24.0
          ysize = 18.0
       endif else begin
          xsize = 23.0
          ysize = 17.0
       endelse
       
       !p.font=0
       device,/color,bits=8,filename=print.dsn,           $
              /landscape,  xsize=xsize, ysize=ysize

     endif else begin
     
        wset, winNum
     
     endelse


     
; Let's do some plotting - default to log/lin


    
    dxtitle=xtitle+xunit
    plot, [xmin,xmax], [ymin,ymax], /nodata, ticklen=1.0, 		$
	     position=[left,bottom,grright,grtop], 			$
	     xtitle=dxtitle, ytitle=ytitle, xstyle=xstyle, ystyle=xstyle, 	$
	     charsize=charsize, xlog=xlog, ylog=ylog


    if makeplot eq 1  then begin
       oplot, x(0:ival-1), y(0:ival-1)
    endif else begin
       xyouts, grright,grtop,'ADAS415 : No data found in these axes ranges',/normal
       print,'ADAS415 : No data found in these axes ranges'
    endelse
    
    
    ; annotate with extra information on top
    
     xyouts, (left-0.05), (top+0.05), gtitle, /normal, alignment=0.0, 	$
	      charsize = charsize, color=17
    
    ; and RHS labels
    
    

; Reset device to X

     if (keyword_set(print)) then begin
       device,/close
       set_plot,'X'
       !p.font=-1
     endif

END
;----------------------------------------------------------------------------


PRO adas415_plot_event, event

; Get the info structure out of the user value of the top-level base.

Widget_Control, event.top, Get_UValue=info


; Now react to event.action as sent from cw_adas_graph
;    only accept 'bitbutton' - return to menu button pressed
;                'print'     
;                'done'
;                'adjust'
;                'mouse'  
;
; The 'update' event is sent from adas415_adjust.

 
   
CASE event.action OF
  
  'done'      : Begin
                   formdata = { cancel   : 0,      $
                                menu     : 0       }
                   *info.ptrToFormData = formdata
                   widget_control, event.top, /destroy
                End
  
  'print'     : Begin
                 
                  print_val = { dsn     :  info.hardname,      $
                                defname : 'adas415.ps',        $
                                replace :  1,                  $
                                devlist :  info.devlist,       $
                                dev     :  info.device,        $
                                paper   :  'A4',               $
                                write   :  0,                  $
                                message :  'Note: cannot append to graphics files'}

                   plot415, info.winNum, info.ival, info.x, info.y, info.title, $
                            info.xu_ind, info.xunits, info.xtitles,            $
                            info.yunits, info.ytitle,                          $
                            info.ldef, info.xmin, info.xmax,                   $
                            info.ymin, info.ymax,                              $
                            info.xlog, info.ylog,                              $
                            PRINT=print_val
                            
	           message = 'Plot  written to print file.'
	           grval   = {WIN:0, MESSAGE:message}
	           widget_control, event.id, set_value=grval
               
                End

  'adjust'    : Begin
                   adas415_adjust, group_leader=info.gleader,    $
                                   notify = info.updateID,       $
                                   font_large = info.font_large, $
                                   xunits = info.xunits
                End

  'update'    : Begin
  
                  CASE event.data.axes_type OF
                     0 : begin 
                            xlog = 0
                            ylog = 0 
                         end
                     1 : begin 
                            xlog = 1
                            ylog = 0 
                         end
                     2 : begin 
                            xlog = 0
                            ylog = 1 
                         end
                     3 : begin 
                            xlog = 1
                            ylog = 1 
                         end
                  ENDCASE
                  
                  CASE event.data.xunits_type OF 
                    -1 : begin
                           xu_ind = info.xu_ind
                           x      = info.x
                           xmin   = info.xmin
                           xmax   = info.xmax
                         end
                     0 : begin
                           xu_ind = info.xu_ind
                           x      = info.x
                           xmin   = info.xmin
                           xmax   = info.xmax
                         end
                     1 : begin
                           const  = 1.239842447e4
                           xu_ind = event.data.xunits_type
                           x      = const / info.x 
                           xmin   = const / info.xmax
                           xmax   = const / info.xmin
                         end
                  ENDCASE
                  
                  CASE event.data.yunits_type OF 
                    -1 : y = info.y
                     0 : y = info.y
                     1 : y = info.y
                  ENDCASE
                  
                  plot415, info.winNum, info.ival, x, y, info.title,   $
                           xu_ind, info.xunits, info.xtitles,          $
                           info.yunits, info.ytitle,                   $
                           info.ldef, xmin,  xmax,            $
                           info.ymin, info.ymax,                       $
                           xlog, ylog
                End


  'bitbutton' : Begin
                   formdata = {cancel:0, menu : 1}
                   *info.ptrToFormData = formdata
                   widget_Control, event.top, /destroy
                End
    
  'mouse'    : Begin
                   print,'Have got a mouse event'
               End
    
  else : print,'ADAS415_OUT_EVENT : You should not see this message! '
             
                   
ENDCASE
    
    
END

;----------------------------------------------------------------------------
   
FUNCTION adas415_plot, dsfull , utitle   ,                              $
                       ldfit  , ival     , xdata   , ydata  ,           $
                       ldef   , xmin     , xmax    , ymin   , ymax,     $
                       hrdout , hardname , device  ,                    $
                       header , bitfile  ,                              $
                       devcode, devlist  ,                              $
                       FONT_LARGE = font_large, FONT_SMALL = font_small


                ;************************************
                ;**** Create general graph title ****
                ;************************************

    title = strarr(3)
    maintitle=strarr(2)   
    type = 'PHOTON ENERGY '
    maintitle(0) = 'DETECTOR EFFICIENCY VS ' + type

    type = 'WAVELENGTH '
    maintitle(1) = "IONISATIONS PER PHOTON VS " + type
    if ldfit eq 0 then title(0)=maintitle(0) else title(0)=maintitle(1)
    
    title(0) = title(0) + ' : ' + utitle
    title(1) = 'ADAS     :' + strupcase(header)
    title(2) = 'FILE     : ' + dsfull

                ;********************************
                ;*** Create graph annotation ****
                ;********************************        


                ;*********************************
                ;**** Set up Y data for plots ****
                ;*********************************

    ydim = ival
    y = make_array(2, ydim, /float)
    valid_data = where((ydata gt 1e-37) and (ydata lt 1e+37), count)
    if (count GT 0) then begin
        y(0, valid_data) = ydata(valid_data)
    endif else begin
        print,'ADAS415 : unable to data'
    endelse
    ytitle = 'DETECTOR EFFICIENCY'

                ;**************************************
                ;**** Set up x axis and xaxis title ***
                ;**************************************

    xdim = ival
    x = fltarr(xdim)
    x(0:ival-1) = xdata
    y(0:ival-1) = ydata
 
                ;****************************************************
                ;*** write x axis array depending on whether temp ***
                ;*** or density values used                       ***
                ;****************************************************

    xtitles = strarr(2)
    xunits  = strarr(2)
    xtitles = ['PHOTON ENERGY','WAVELENGTH']          
    xunits  = [' (eV) ','  (A)  '] 
    xu_ind  = 0
    
    yunits = ['        ']


                ;******************************************
                ;*** if desired set up user axis scales ***
                ;******************************************

    if (ldef eq 0) then begin
          xmin  = min(x, max = xmax)
          xmax  = xmax * 1.1
          ymin  = min( y, max = ymax)
          ymin  = ymin * 0.9
          ymax  = ymax *1.1
    endif
    
                ;*************************************
                ;**** Create graph display widget ****
                ;*************************************

    parent = widget_base(TITLE='ADAS415 GRAPHICAL OUTPUT',               $
                          XOFFSET=1,YOFFSET=1)
    device, get_screen_size=scrsz
    xwidth = scrsz(0)*0.75
    yheight = scrsz(1)*0.75
    
    bitval = bitfile + '/menu.bmp'

    multiplot = 0
    adjust = 1
    cwid   = cw_adas_graph(parent, print=hrdout, FONT=font_large,         $
                           xsize=xwidth, ysize=yheight,                   $
                           multiplot=multiplot, bitbutton=bitval,         $
                           adjust=adjust, mouse_active=0, backstore=1)

                ;**** Realize the new widget ****

    dynlabel, parent
    widget_control, parent, /realize

                
                
; Get the id of the graphics area and plot an initial graph in log/lin space
    
    xlog = 1
    ylog = 0

    widget_control, cwid, get_value=grval
    winNum = grval.win

    wset, winNum

    plot415, winNum, ival, x, y, title,                $
             xu_ind, xunits, xtitles, yunits, ytitle,  $
             ldef, xmin, xmax, ymin, ymax, xlog, ylog
     
    
    
 
; Get the ranges from system variables 
                
    if (ldef eq 0) then begin
          xmin = 10^(!x.crange(0))
          xmax = 10^(!x.crange(1))
          ymin = 10^(!y.crange(0))
          ymax = 10^(!y.crange(1))
    endif
    


; Put all required data into a structure.

    ptrToFormData = Ptr_New({cancel:1, menu:0})

    info = { winNum          :  winNum,            $
             updateID        :  parent,            $
             gleader         :  parent,            $ 
             ival            :  ival,              $
             x               :  x,                 $
             y               :  y,                 $
             title           :  title,             $
             xu_ind          :  xu_ind,            $
             xunits          :  xunits,            $
             xtitles         :  xtitles,           $
             yunits          :  yunits,            $
             ytitle          :  ytitle,            $
             ldef            :  ldef,              $
             xmin            :  xmin,              $
             xmax            :  xmax,              $
             ymin            :  ymin,              $
             ymax            :  ymax,              $
             xlog            :  xlog,              $
             ylog            :  ylog,              $
             hardname        :  hardname,          $
             devlist         :  devlist,           $
             devcode         :  devcode,           $
             device          :  device,            $
             font_large      :  font_large,        $
             font_small      :  font_small,        $
             ptrToFormData   :  ptrToFormData      }

; Store the info structure in the user value of the top-level base and launch
; the widget into the world.

  widget_control, parent, Set_UValue=info

  XManager, 'ADAS415_PLOT', parent, Event_Handler='ADAS415_PLOT_EVENT'


; When the widget is destroyed, the block is released, and we
; return here. Get the form data that was collected by the form
; and stored in the pointer location.

formdata = *ptrToFormData

rep='CONTINUE'
IF N_Elements(formdata) EQ 0 THEN BEGIN
   Ptr_Free, ptrToFormData
   rep ='CANCEL'
ENDIF
 
IF formdata.cancel EQ 1 THEN BEGIN
   Ptr_Free, ptrToFormData
   rep ='CANCEL'
   RETURN,rep
ENDIF

IF formdata.menu EQ 1 THEN BEGIN
   Ptr_Free, ptrToFormData
   rep ='MENU
   RETURN,rep
ENDIF


; And tell ADAS415 that we are finished

RETURN,rep

END
