; Copyright (c) 2000 Strathclyde University .
; SCCS info: Module @(#)$Header: /home/adascvs/idl/adas4xx/adas415/adas415_adjust.pro,v 1.1 2004/07/06 10:49:51 whitefor Exp $ Date $Date: 2004/07/06 10:49:51 $
;+
; PROJECT:
;       ADAS 
;
; NAME:
;	ADAS415_ADJUST
;
; PURPOSE: 
;       This function puts up a widget asking for two input files for
;       the adas_driver program of Ralph Dux. Pairs of ionised/ionising
;       or recombined/recombining adf04 files are reuested.
;
;
; EXPLANATION:
;       Similar to ADAS407 input screen but without the automatic 
;       selection of the second file.       
;     
;
; NOTES:
;       Based on the techniques of ADAS403. It entirely in IDL and uses
;       v5 (and above) features such as pointers to pass data.
; 
;
;
; INPUTS:
;        inval       - A structure holding the 'remembered' input data.
;                        EROOTPATH     : root file 1
;                        EFILE         : filename 1
;                        ECENTROOT     : central adas of root 1
;                        EUSERROOT     : user path of file 1
;                        CROOTPATH     :
;                        CFILE         :  file 2!
;                        CCENTROOT     :
;                        CUSERROOT     :
; OPTIONAL INPUTS:
;       None
;
; OUTPUTS:
;       The function returns a string rep
;           rep = 'CONTINUE' if everything is ok
;           rep = 'CANCEL' if there is a problem or indeed if the user
;                          closes the widget with the mouse. 
;
; OPTIONAL OUTPUTS:
;       None
;
; KEYWORD PARAMETERS:
;       FONT_LARGE - The name of a larger font e.g 'courier_bold14'
;
;       FONT_SMALL - The name of a smaller font e.g 'courier_bold12'
;
;
; CALLS:
;                            
;       ADAS415_IN_FILEA     : Reacts to first file widget and sensitises
;                              'Done' and 'Browse' if 2 valid files have
;                              been selected.
;       ADAS415_IN_FILEB     : Ditto for second file.
;       ADAS415_IN_EVENT     : Reacts to Cancel, Done and Browse.
;
; SIDE EFFECTS:
;
; CATEGORY:
;	Widgets
;
; WRITTEN:
;       Martin O'Mullane
;
; MODIFIED:
;	1.1	Martin O'Mullane
;		First release 
;
; VERSION:
;       1.1	12-10-99
;
;-
;-----------------------------------------------------------------------------

PRO ADAS415_ADJUST_EVENT, event

; Get the info structure out of the user value of the top-level base.

Widget_Control, event.top, Get_UValue=info

; All events come here. Destroy the widget if cancelled. Otherwise grab
; all data to put into an event structure which is passed straight on 
; to a named notified widget. 
;
; These values are not necessarily strings. Hence the string conversion 
; to find if the cancel button was pressed.

Widget_Control, event.id, Get_Value=userEvent

userEventStr = string(userEvent)

CASE userEventStr OF

  'Cancel' : Begin
               widget_Control, event.top, /destroy
             End 

   else    : Begin
               if widget_info(info.notifyID, /valid_id) then begin
                 
                  Widget_Control,info.axesID, get_value = axes_type
                  if info.xunitsID NE -1L then  begin
                      Widget_Control,info.xunitsID, get_value = xunits_type
                   endif else begin
                      xunits_type = -1
                   endelse
                  if info.yunitsID NE -1L then  begin
                      Widget_Control,info.xunitsID, get_value = yunits_type
                   endif else begin
                      yunits_type = -1
                   endelse
            
                  adj_par = {  axes_type    : axes_type,    $
                               xunits_type  : xunits_type,  $
                               yunits_type  : yunits_type   }
                  
                  updateEvent = {ID      : event.id,       $
                                 TOP     : 0L,             $
                                 HANDLER : 0L,             $
                                 ACTION  : 'update',       $
                                 DATA    : adj_par         }
                  widget_control, info.notifyID, send_event=updateEvent
               
               endif
             End
                   
ENDCASE

END
  
;-----------------------------------------------------------------------------


PRO adas415_adjust, GROUP_LEADER = group_leader, NOTIFYID = notifyID,      $
                    FONT_LARGE = font_large, FONT_SMALL = font_small,      $
                    xunits = xunits, yunits = yunits,                              $
                    xmin = xmin, xmax = xmax, ymin = ymin, ymax = ymax 


; We only want one instance of this widget.  
  
  IF XREGISTERED('adas415_adjust') THEN RETURN
		
                
; Set defaults for keywords.

  IF n_elements(font_large) eq 0 THEN font_large = ''
  IF n_elements(font_small) eq 0 THEN font_small = ''

  IF n_elements(notifyID) eq 0 THEN notifyID = -1L

 
                ;********************************************
		;**** create modal top level base widget ****
                ;********************************************
                
  parent = Widget_Base(Column=1, Title='Graph Control')

  topbase = widget_base(parent,/column,/frame)
  label   = widget_label(topbase,value=' ')


                ;********************
		;**** Axis style ****
                ;********************

  style = ['X Linear/Y Linear', $
           'X Log/Y Linear   ', $
           'X Linear/Y Log   ', $
           'X Log/Y Log      ']        

  
  listval=0     ; initial setting

  stylbase = widget_base(topbase,/row)                            
  label    = widget_label(stylbase,value='   Axes: ',font=font_large)
  axesID   = cw_bselector(stylbase,style,set_value=listval,font=font_large)
                
                
                ;********************
		;**** Axis units ****
                ;********************
                
  
  IF n_elements(xunits) NE 0 then begin
  
     xunitbase = widget_base(topbase)
     base      = widget_base(xunitbase,/row)
     
     label     = widget_label(base,value='X-units: ',font=font_large)
     listval   = 0
     xunitsID  = cw_bselector(base,xunits,set_value=listval,font=font_large)
     
  endif else begin
  
    xunitsID   = -1L 
  
  endelse     
                
  IF n_elements(yunits) NE 0 then begin
  
     yunitbase = widget_base(topbase)
     base      = widget_base(yunitbase,/row)
     
     label     = widget_label(base,value='Y-units: ',font=font_large)
     listval   = 0
     yunitsID  = cw_bselector(base,yunits,set_value=listval,font=font_large)
     
  endif else begin
  
    yunitsID   = -1L 
  
  endelse     
  
                ;*****************
		;**** min/max ****
                ;*****************
                
                
                ;*****************
		;**** Buttons ****
                ;*****************
                
  cancelID   = widget_button(parent,value='Cancel',font=font_large)




; Initial settings 


; Realize the ADAS415 input widget.

   widget_Control, parent, /realize


; Create an info structure with program information.

  info = { cancelID        :  cancelID,           $
           notifyID        :  notifyID,           $
           axesID          :  axesID,             $
           xunitsID        :  xunitsID,           $
           yunitsID        :  yunitsID,           $
           font_large      :  font_large          }
            
               
; Store the info structure in the user value of the top-level base and launch
; the widget into the world.

  widget_control, parent, Set_UValue=info

  XManager, 'adas415_adjust', parent, Event_Handler='ADAS415_ADJUST_EVENT', $
                                  Group_Leader = group_leader



END
