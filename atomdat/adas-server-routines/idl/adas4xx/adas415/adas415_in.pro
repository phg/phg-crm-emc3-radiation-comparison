; Copyright (c) 2000 Strathclyde University .
; SCCS info: Module @(#)$Header: /home/adascvs/idl/adas4xx/adas415/adas415_in.pro,v 1.1 2004/07/06 10:49:53 whitefor Exp $ Date $Date: 2004/07/06 10:49:53 $
;+
; PROJECT:
;       ADAS 
;
; NAME:
;	ADAS415_IN
;
; PURPOSE: 
;       This function puts up a widget asking for two input files for
;       the adas_driver program of Ralph Dux. Pairs of ionised/ionising
;       or recombined/recombining adf04 files are reuested.
;
;
; EXPLANATION:
;       Similar to ADAS407 input screen but without the automatic 
;       selection of the second file.       
;     
;
; NOTES:
;       Based on the techniques of ADAS403. It entirely in IDL and uses
;       v5 (and above) features such as pointers to pass data.
; 
;
;
; INPUTS:
;        inval       - A structure holding the 'remembered' input data.
;                        EROOTPATH     : root file 1
;                        EFILE         : filename 1
;                        ECENTROOT     : central adas of root 1
;                        EUSERROOT     : user path of file 1
;                        CROOTPATH     :
;                        CFILE         :  file 2!
;                        CCENTROOT     :
;                        CUSERROOT     :
; OPTIONAL INPUTS:
;       None
;
; OUTPUTS:
;       The function returns a string rep
;           rep = 'CONTINUE' if everything is ok
;           rep = 'CANCEL' if there is a problem or indeed if the user
;                          closes the widget with the mouse. 
;
; OPTIONAL OUTPUTS:
;       None
;
; KEYWORD PARAMETERS:
;       FONT_LARGE - The name of a larger font e.g 'courier_bold14'
;
;       FONT_SMALL - The name of a smaller font e.g 'courier_bold12'
;
;
; CALLS:
;                            
;       ADAS415_IN_FILEA     : Reacts to first file widget and sensitises
;                              'Done' and 'Browse' if 2 valid files have
;                              been selected.
;       ADAS415_IN_FILEB     : Ditto for second file.
;       ADAS415_IN_EVENT     : Reacts to Cancel, Done and Browse.
;
; SIDE EFFECTS:
;
; CATEGORY:
;	Widgets
;
; WRITTEN:
;       Martin O'Mullane
;
; MODIFIED:
;	1.1	Martin O'Mullane
;		First release 
;
; VERSION:
;       1.1	12-10-99
;
;-
;-----------------------------------------------------------------------------

FUNCTION ADAS415_IN_FILE, event

   ; Sensitise 'Done' and 'Browse' if we are ready to continue 

   Widget_Control, event.top, Get_UValue=info
     
   Widget_Control, info.flID, Get_Value=file

   filename = file.rootpath + file.file
   file_acc, filename, exist, read, write, execute, filetype
     
   
   if event.action EQ 'newfile' AND filetype EQ '-' then begin
      Widget_Control, info.doneID, sensitive=1
      Widget_Control, info.browseID, sensitive=1
   endif 
   
   RETURN,0

END

;-----------------------------------------------------------------------------

PRO ADAS415_IN_EVENT, event

   ; Get the info structure out of the user value of the top-level base.

Widget_Control, event.top, Get_UValue=info


; Something nefarious may cause an error also.
;  -- if so trap it and exit gracefully

CATCH, error
IF error NE 0 THEN BEGIN
   formdata = {cancel:1}
   *info.ptrToFormData = formdata
   message,!err_string
   Widget_Control, event.top, /Destroy
   RETURN
ENDIF

Widget_Control, event.id, Get_Value=userEvent


CASE userEvent OF

  'Cancel' : begin
               formdata = {cancel:1}
               *info.ptrToFormData =formdata
               widget_Control, event.top, /destroy
             end 

      
  'Done'   : begin
                 
                ; gather the data for return to calling program
                ; 'Done' is not sensitised until valid filenames
                ; have been chosen.
               
                widget_Control, info.flID, Get_Value=val
                
                err  = 0
                mess = ''
                
               
                if err EQ 0 then begin
                   formdata = {val     : val,        $
                               cancel   : 0          }
                    *info.ptrToFormData = formdata
                    widget_control, event.top, /destroy
                endif else begin
                   widget_Control, info.messID, Set_Value=mess
                endelse

             end
             
    'Browse Comments' : Begin
    
                Widget_Control, info.flID, get_value=val
                filename = val.rootpath + val.file
                xxtext, filename, font=info.font_large
    
             end        

  else : print,'ADAS415_IN_EVENT : You should not see this message! ',userEvent
                   
ENDCASE

END
  
;-----------------------------------------------------------------------------


FUNCTION adas415_in, inval,  $
                     FONT_LARGE = font_large, FONT_SMALL = font_small


		;**** Set defaults for keywords ****

  IF n_elements(font_large) eq 0 THEN font_large = ''
  IF n_elements(font_small) eq 0 THEN font_small = ''


; Path names may be supplied with or without the trailing '/'.  Add
; this character where required so that USERROOT will always end in
; '/' on output.
  
  if strtrim(inval.rootpath) eq '' then begin
      inval.rootpath = './'
  endif else if                                                   $
  strmid(inval.rootpath, strlen(inval.rootpath)-1,1) ne '/' then begin
      inval.rootpath = inval.rootpath+'/'
  endif


 
                ;********************************************
		;**** create modal top level base widget ****
                ;********************************************
                
  parent = Widget_Base(Column=1, Title='ADAS 415 INPUT', $
                       XOFFSET=100, YOFFSET=1)

                
  rc = widget_label(parent,value='  ',font=font_large)


  base  = widget_base(parent, /column)
  
  
                ;*******************************
                ;**** File selection widget ****
                ;*******************************

  flbase = widget_base(base, /column, /frame)
  flval = {   ROOTPATH        :       inval.rootpath,                $
              FILE            :       inval.file,                    $
              CENTROOT        :       inval.centroot,                $
              USERROOT        :       inval.userroot                 }
  fltitle = widget_label(flbase, font=font_large,              $
                          value='Filter File Details:-')
  flID = cw_adas_infile(flbase, value=flval, font=font_large, ysize = 15 , $
                         event_funct='ADAS415_IN_FILE')



                            
                ;*****************
		;**** Buttons ****
                ;*****************
                
  mrow     = widget_base(parent,/row,/align_center)
  messID   = widget_label(mrow,font=font_large, $
                          value='          Enter File information             ')
                
  mrow     = widget_base(parent,/row)
  
  browseID = widget_button(mrow, value='Browse Comments', font=font_large)
  cancelID = widget_button(mrow,value='Cancel',font=font_large)
  doneID   = widget_button(mrow,value='Done',font=font_large)




; Initial settings 

                ;*************************************************
                ;**** Check filenames and desensitise buttons ****
                ;**** if it is a directory or it is a file    ****
                ;**** without read access.                    ****
                ;*************************************************

    browseallow = 0
    filename = inval.rootpath + inval.file
    file_acc, filename, fileexist, read, write, execute, filetype
    if filetype ne '-' then begin
        Widget_Control, browseID, sensitive=0
        Widget_Control, doneID, sensitive=0
    endif else begin
        if read eq 0 then begin
            Widget_Control, browseID, sensitive=0
            Widget_Control, doneID, sensitive=0
        endif else begin
            browseallow = 1
        endelse
    endelse



; Realize the ADAS415 input widget.

   widget_Control, parent, /realize

; Create a pointer to store the information collected from the form.
; The initial data stored here is set to CANCEL, so nothing needs to
; be done if the user kills the widget with the mouse.

   ptrToFormData = Ptr_New({cancel:1})

; Create an info structure with program information.

  info = { flID            :  flID,               $
           doneID          :  doneID,             $
           browseID        :  browseID,           $
           messID          :  messID,             $
           font_large      :  font_large,         $
           ptrToFormData   :  ptrToFormData       }  
            
               
; Store the info structure in the user value of the top-level base and launch
; the widget into the world.

  widget_control, parent, Set_UValue=info

  XManager, 'adas415_in', parent, Event_Handler='ADAS415_IN_EVENT'


; When the widget is destroyed, the block is released, and we
; return here. Get the data that was collected by the widget
; and stored in the pointer location. Finally free the pointer.

formdata = *ptrToFormData

rep='CONTINUE'
IF N_Elements(formdata) EQ 0 THEN BEGIN
   Ptr_Free, ptrToFormData
   rep ='CANCEL'
ENDIF
 
IF formdata.cancel EQ 1 THEN BEGIN
   Ptr_Free, ptrToFormData
   rep ='CANCEL'
ENDIF

if rep eq 'CONTINUE' then begin
   inval.rootpath   = formdata.val.rootpath
   inval.file       = formdata.val.file
   inval.centroot   = formdata.val.centroot
   inval.userroot   = formdata.val.userroot
   Ptr_Free, ptrToFormData
endif       



RETURN,rep

END
