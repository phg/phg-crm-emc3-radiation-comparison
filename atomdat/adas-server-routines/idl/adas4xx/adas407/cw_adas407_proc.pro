; Copyright (c) 1995 Strathclyde University 
; SCCS info: Module @(#)$Header: /home/adascvs/idl/adas4xx/adas407/cw_adas407_proc.pro,v 1.10 2004/07/06 12:47:50 whitefor Exp $ Date $Date: 2004/07/06 12:47:50 $
;+
; PROJECT:
;       ADAS IBM MVS to UNIX conversion
;
; NAME:
;	CW_ADAS407_PROC()
;
; PURPOSE:
;	Produces a widget for ADAS407 processing options/input.
;
; EXPLANATION:
;	This function creates a compound widget consisting of :-
;	   a text widget holding an editable 'Run title'; 
;	   for each dataset, a name/browse widget and a selection
;	   list to choose the metastables;
;	   a selection list to choose the temperature to match;
;	   a list of parameter forms and their assignments;
; 	   a selection list of the chosen metastables;
;	   a list of the allowed transitions allowing bundling
;	   a message widget, and a 'Cancel' and a 'Done' button.
;
;	The compound widgets included in this widget are self managing.
;
;	This widget only generates events for the 'Done' and 'Cancel'
;	buttons.
;
; USE:
;	This widget is specific to ADAS407, see adas407_proc.pro
;	for use.
;
; INPUTS:
;	TOPPARENT- Long integer, ID of parent widget.
;
;	DSNIC1	- String; The full system file name of the first input 
;		          dataset selected by the user for processing.
;
;	DSNIC2	- String; The full system file name of the second input 
;		          dataset selected by the user for processing.
;
;   	z0	- Double; nuclear charge from first copase file
;   	z1	- Double; ion charge from first copase file
;   	z02	- Double; nuclear charge from second copase file
;   	z12	- Double; ion charge from second copase file
;	ndlev 	- Integer; Max. number of levels
;	ndtrn 	- Integer; Max. number of transitions
;	ndtem 	- Integer; Max. number of temperatures
;	ndmet 	- Integer; Max. number of metastables
;	ndosc 	- Integer; Max. number of oscillator strengths
;	pipe	- Integer; The bi-directional pipe used for communication
;		  with Fortran in the routine d7bndl called when
;		  metastables have been selected.
;
;	Most inputs map exactly onto variables of the same
;	name in the ADAS407 FORTRAN program.
;
; OPTIONAL INPUTS:
;	None
;
; OUTPUTS:
;	The return value of this function is the ID of the declared widget.
;
;	ACT 	- String; result of this widget, 'Done', 'Cancel' or 'Menu'
;
; OPTIONAL OUTPUTS:
;	None
;
; KEYWORD PARAMETERS:
;	PROCVAL	- A structure which determines the initial settings of
;		  the processing options widget.
;
;		  The default PROCVAL is created thus;
;
;        procval = {	title	: 	'',,                		$
;			il	:	0,				$
;			ipl	:	0,				$
;			nv	:	0,				$
;			tscef	:	temp_arr,			$
;			nmet	:	0,				$
;			npmet	:	0,				$
;			imetr	:	int_arr,			$
;			ipmetr	:	int_arr,			$
;			imet	:	-1,				$
;			ifout	:	1,				$
;			itsel	:	0,				$
;			selr	:	0,				$
;			seld	:	0,				$
;			sels	:	0,				$
;			selp	:	0,				$
;			sell	:	0,				$
;			ltadj	:	0,				$
;			ictm	:	int_arr,			$
;			iuma	:	int_arr2,			$
;			cstgma	:	str_arr,			$
;			imaloca	:	int_arr2,			$
;			ispeca	:	int_arr,			$
;			strga   :	str_arr,			$
;			strga2	:	str_arr				$
;			}
;
;	title	A C*40 character string containing the run title
;	il	The number of ionising ion energy levels
;	ipl	The number of parent ion energy levels
;	nv	The number of temperatures
;	tscef	An array of the temperatures in three units (NDTEM,3)
;	nmet	Number of metastables chosen
;	npmet	Number of parent metastables chosen
;
;*****************************************************************************
;****				N.B.
;**** procval.imetr, procval.ipmetr and procval.imet are IDL
;**** indices between 0 and ndmet-1.
;**** 1 must be added to get back to Fortran indices
;*****************************************************************************
;
;	imetr	Index of metastables in complete level list (NDMET)
;	ipmetr	Index of parents in complete level list (NDMET)
;	imet	Metastable chosen for power fit: value= 0 to NDMET-1
;	ifout	Temperature unit selection (1 to 3)
;	itsel	Selected temperature index (IDL index, 0 to NDTEM-1)
;	*** CSEL# -> SEL#, 0 = 'A', 1 = 'B'
;	selr	Form for radiative recombination
;	seld	Form for dielectric recombination
;	sels	Form for collisional ionisation
;	selp	Form for total line power
;	sell	Form for specific line power	
;	ltadj	Adjust params from special tables? (0=no, 1=yes)
;	ictm	No. of included transitions for each metastable (NDMET)
;	iuma	Index of metastable assigned transition in full electron coll.
;		transition list (NDOSC,NDMET)
;	cstgma	String identifier for included transition (NDOSC,NDMET)
;	imaloca	Bundle allocation of allowed transition (NDOSC, NDMET)
;		= 0 initially when no selection has been made
;	ispeca	Specific line index in allowed transition
;	strga	String array of ionising ion level designation info (NDLEV)
;	strga2	String array of ionised ion level designation info (NDLEV)
;
;		Most of these structure elements map onto variables of
;		the same name in the ADAS407 FORTRAN program.
;
;
;	UVALUE	- A user value for the widget. Default 0.
;
;	FONT_LARGE - The name of a larger font.  Default current system
;		     font
;
;	FONT_SMALL - The name of a smaller font. Default current system
;		     font.
;
;	EDIT_FONTS - A structure of two fonts used in the table editor
;		     adas_edtab.pro, {font_norm:'',font_input:''}.  Defaults
;		     to current system font.
;
;	NUM_FORM   - String; Numeric format to use in tables.  Default
;			'(E10.3)'
;		     
;
; CALLS:
;	POPUP		Popup warning window with buttons.
;	CW_ADAS_DSBR	Dataset name and comments browsing button.
;	CW_ADAS_SEL	Multiple selection list widget.
;	CW_ADAS_SEL_SEL	Multiple selection list widget with further selection
;			from selected list.
;	CW_TEMP_SEL	Single selection list with a bselector for different
;			temperature units.
;	CW_BUNDLE	Single selection list with editable keys for each
;			member of the list.
;	XXTCON		General temperature conversion routine
;	I4EIZ0		Converts element symbol into atomic number
;
;	See side effects for other related widget management routines.
;
; SIDE EFFECTS:
;
;	The following widget management routines are included in this file;
;	PROC407_GET_VAL()	Returns the current PROCVAL structure.
;	PROC407_EVENT()		Process and issue events.
;	
; CATEGORY:
;	Compound Widget
;
; WRITTEN:
;       William Osborn, Tessella Support Services plc, 1-Apr-1996 
;
; MODIFIED:
;	1.1	William Osborn
;		First version.
;	1.2	William Osborn
;		Changed the way the charges are passed, sz, ...
;	1.3	William Osborn
;		Added message for no transitions present
;	1.4	William Osborn
;		Changed to use cw_bundle's 'commment' facility
;	1.5	William Osborn
;		Added no_parent flag for the case of the last two
;		members in an iso-electronic sequence. Also
;		desensitised the parent selection widget if no parents.
; 	1.6	Hugh Summers
;		Altered no_parent flag operation to correct bare nucleus case
;               cw_bundid(...size=20 -> size=50 ...)
;	1.7     Richard Martin
;	        Changed cw_bundid(...size=50 -> size=51 ...)
;	1.8	Richard Martin
;		Corrected error in proc407_event picked up by IDL 5.4 :
;		changed 'endelse' to 'end'.
;	1.9	Martin O'Mullane
;		Correct spelling mistake in A/B form instructions.
;               De-sensitise parent selection for H-like ion.
;	1.10	Martin O'Mullane
;		Do not pass ndmet through cw_adas_sel_sel as it 
;               changes its type and value which causes crashes
;               down the line.
;
; VERSION:
;	1.1	01-04-96
;	1.2 	23-04-96
;	1.3	02-04-96
;	1.4	02-04-96
;	1.5	19-11-96
;	1.6	24-06-97
;	1.7	24-02-98
;	1.8	17-11-00
;	1.9	11-05-2003
;	1.10	05-03-2004
;
;-
;-----------------------------------------------------------------------------

FUNCTION proc407_get_val, id

    COMMON cw_proc407_blk, ndlev, ndtrn, ndtem, ndmet, ndosc, ndbndl


                ;**** Return to caller on error ****

    ON_ERROR, 2

		 ;***************************************
                 ;****     Retrieve the state 	     ****
		 ;**** Get first_child widget id     ****
		 ;**** because state is stored there ****
		 ;***************************************

    first_child = widget_info(id, /child)
    widget_control, first_child, get_uvalue = state, /no_copy

		;***********************************
		;**** Get run title from widget ****
		;**** Then centre in in string  ****
		;**** of 40 characters 		****
		;***********************************

    widget_control, state.runid, get_value=title
    title = strcompress(title(0))
    title_len = strlen(title) 
    if (title_len gt 40 ) then begin 
	title = strmid(title,0,37)
        widget_control, state.messid, set_value="Title too long - truncated "
        widget_control, state.runid, set_value=title
	wait ,1
    endif
    pad = (40 - title_len)/2 
    spaces = '                                         '
    title(0) = strmid(spaces,0,(pad-1)) + title + strmid(spaces,0,(pad-1))


		;********************************************
		;**** Get selected temperature and units ****
		;********************************************

    widget_control, state.tempid, get_value=val
    itsel = val.index
    ifout = val.listix+1

		;****************************************
		;**** Get parameter form assignments ****
		;****************************************

    widget_control, state.selrid, get_value = val
    selr = val
    widget_control, state.seldid, get_value = val
    seld = val
    widget_control, state.selsid, get_value = val
    sels = val
    widget_control, state.selpid, get_value = val
    selp = val
    widget_control, state.sellid, get_value = val
    sell = val

    if state.imet ge 0 then begin

		;**********************************
		;**** Get bundling information ****
		;**********************************

    	widget_control, state.bundid, get_value=val
    	if state.ictm(state.imet) gt 0 then begin
	    for j=0,state.ictm(state.imet)-1 do begin
		if num_chk(val.key(j),/integer) eq 0 then begin
	            state.imaloca(j,state.imet)=fix(val.key(j))
		endif else begin
	            state.imaloca(j,state.imet)=0
		endelse			
	    endfor
        endif

		;*********************************
		;**** Specific line selection ****
		;*********************************

    	widget_control, state.specid, get_value=txt
    	txt=txt(0)
    	if num_chk(txt,/integer) eq 0 then begin
	   state.ispeca(state.imet)=fix(txt)
        endif else begin
	   state.ispeca(state.imet)=-1
     	endelse

    endif
		;***********************************************
		;**** write selected values to PS structure ****
		;***********************************************

        ps = 	{	title	: 	title(0),              		$
			il	:	state.il,			$
			ipl	:	state.il,			$
			nv	:	state.nv,			$
			tscef	:	state.tscef,			$
			nmet	:	state.nmet,			$
			npmet	:	state.npmet,			$
			imetr	:	state.imetr,			$
			ipmetr	:	state.ipmetr,			$
			imet	:	state.imet,			$
			ifout	:	ifout,				$
			itsel	:	itsel,				$
			selr	:	selr,				$
			seld	:	seld,				$
			sels	:	sels,				$
			selp	:	selp,				$
			sell	:	sell,				$
			ltadj	:	0,				$
			ictm	:	state.ictm,			$
			iuma	:	state.iuma,			$
			cstgma	:	state.cstgma,			$
			imaloca	:	state.imaloca,			$
			ispeca	:	state.ispeca,			$
			strga   :       state.strga,			$
			strga2	:	state.strga2			$
		}

   
    widget_control, first_child, set_uvalue=state, /no_copy

    RETURN, ps

END

;-----------------------------------------------------------------------------

FUNCTION proc407_event, event

    COMMON cw_proc407_blk, ndlev, ndtrn, ndtem, ndmet, ndosc, ndbndl


                ;**** Base ID of compound widget ****

    parent=event.handler

		;**********************************************
                ;**** Retrieve the user value state        ****
		;**** Get id of first_child widget because ****
		;**** user value "state" is stored there   ****
		;**********************************************

    first_child = widget_info(parent, /child)
    widget_control, first_child, get_uvalue=state, /no_copy

		;*********************************
		;**** Clear previous messages ****
		;*********************************

    widget_control, state.messid, set_value=' '

                ;************************
                ;**** Process events ****
                ;************************

    CASE event.id OF


		;******************************
		;**** Metastable selection ****
		;******************************

	state.selid1: begin

	    CASE event.action OF

		;**** User has chosen a metastable from the list of 5
		'singlesel': begin

		;**** If the index is valid, save last imaloca and set
		;**** bundling group widget
		    ix = event.index
		    if(ix ge 0 and ix lt state.nmet) then begin
			if state.imet ne -1 then begin
    			    widget_control, state.bundid, get_value=val
    			    if state.ictm(state.imet) gt 0 then begin
	    		    	for j=0,state.ictm(state.imet)-1 do begin
	        		    state.imaloca(j,state.imet)=fix(val.key(j))
	    		        endfor
			        widget_control, state.specid, get_value=txt
	        		txt=txt(0)
	        		if num_chk(txt,/integer) eq 0 then begin
		    		    f=fix(txt)
		    		    if f lt 1 or f gt ndbndl then begin
		        	        state.ispeca(state.imet)=-1
				    endif else begin
		        	    	ok=0
		        	    	for i=0,state.ictm(state.imet)-1 do $
				    	    if f eq fix(val.key(i)) then ok=1
		        	    	if ok eq 0 then begin
			    	    	    state.ispeca(state.imet)=-1
			            	endif else begin
					    state.ispeca(state.imet)=f
					endelse
		    	            endelse
			        endif else begin
				    state.ispeca(state.imet)=-1
			        endelse
        		    endif
		 	endif
		        state.imet = ix
			if(state.ictm(ix) gt 0) then begin
	text = string(fix(findgen(state.ictm(ix)))+1,format='(I2)') 	$
		 + '  ' + state.cstgma(0:state.ictm(ix)-1,ix)
			    key=strarr(state.ictm(ix))
			    for i=0,state.ictm(ix)-1 do begin
				if state.imaloca(i,ix) eq 0 then begin
				    key(i)=''
				endif else begin
			key(i) = strtrim(string(state.imaloca(i,ix)),2)
				endelse
			    endfor
			    if state.ispeca(ix) eq -1 then begin
				widget_control, state.specid, set_value=''
			    endif else begin
				widget_control, state.specid, set_value=$
			         strtrim(string(state.ispeca(ix)),2)
			    endelse
			endif else begin
			    text = 'No transitions above threshold'
			    key = 'comment'
			    widget_control, state.specid, set_value = ''
			endelse
		    	widget_control, state.bundid,			$
				set_value = {TEXT:text+'  ',KEY:key}
		    	metstr = 'Metastable Index: '+			$
				strtrim(string(state.imetr(ix)+1),2)
		    	widget_control, state.bumeid,set_value=metstr
		    endif

		end

		;**** User has pressed 'Selections' and selected the states
 	      'multisel': begin

	    	widget_control, state.selid1, get_value=selected

		oldsel=intarr(ndmet)
		if state.nmet gt 0 then oldsel(0:state.nmet-1) $
		   =state.imetr(0:state.nmet-1)
		if state.nmet lt ndmet then oldsel(	$
	 state.nmet:ndmet-1)=replicate(-1,ndmet-state.nmet)

		;**** If user didn't select Cancel or he changed something,
		;**** carry on

		flag = 0
		for i=0,ndmet-1 do if oldsel(i) ne selected(i) then $
		    flag = 1
		if flag eq 1 then begin

		;**** Ask if user really wants to change metastables ****
		    set = 1
		    if state.nmet ne 0 then begin
			action=popup(message='Changing the metastables will reset bundle selections',$
		          buttons=['Confirm','Cancel'], font=state.font)
			if action eq 'Cancel' then set = 0
		    endif

		;**** If so, call the bundling routine given that at least
		;**** one metastable has been chosen

		    if set eq 1 then begin
		    	ww = where(selected ne -1, count)
		    	if count ne 0 then begin

		            state.imetr(0:count-1) = selected(ww)
		            state.nmet = count
		            d7bndl, state

		;**** Reset bundling keys ****
			    for i=0,ndmet-1 do begin
				for j=0,ndosc-1 do begin
				    state.imaloca(j,i)=0
				endfor
				state.ispeca(i) = -1
			    endfor

		    	endif else begin
			    state.nmet = 0
			endelse

		;**** Reset the bundling widget ****

			state.imet = -1
			widget_control,state.bundid,set_value={		$
			 TEXT:'                             ', KEY:'comment'}
			widget_control, state.bumeid,			$
				set_value='Please choose a metastable'
			widget_control, state.specid, set_value=' '
		    endif else begin

		;**** If user selected Cancel then restore old values

			widget_control,state.selid1, set_value=oldsel

		    endelse
		endif

	      end

	    ENDCASE

	end

		;******************************
		;**** Parent selection ****
		;******************************

	state.selid2: begin
	    CASE event.action OF

		;**** User has pressed 'Selection' : store values in state
		'multisel': begin
		    widget_control, state.selid2, get_value=selected
		    ww = where(selected ne -1, count)
		    if count ne 0 then begin 
	      		state.ipmetr = selected(ww)
		    endif
		    state.npmet  = count
		end

		;**** User has selected a state: do nothing but give help
	    ELSE: widget_control,state.messid,				$
			set_value='For bundling, select from ionising ion list'

	    ENDCASE
	end

		;***********************
		;**** Cancel button ****
		;***********************

        state.cancelid: new_event = {ID:parent, TOP:event.top, 		$
				     HANDLER:0L, ACTION:'Cancel'}


		;*********************
		;**** Done button ****
		;*********************

        state.doneid: begin

		;**** Retrieve value of complete widget ****

	    widget_control, first_child, set_uvalue=state, /no_copy
  	    widget_control, event.handler, get_value = os
	    widget_control, first_child, get_uvalue=state, /no_copy

		;***************************************
		;**** Check all user input is legal ****
		;***************************************

	    error = 0

	    if os.nmet lt 1 then begin
		error = 1
		message = 'Error: No metastable(s) selected'
	    endif else if os.npmet lt 1 and state.no_parents eq 0 then begin
		error = 1
		message = 'Error: No parent metastable(s) selected'
	    endif else if os.imet lt 0 then begin
		error = 1
		message = 'Error: No metastable selected for power fit'
	    endif else if os.ictm(os.imet) eq 0 then begin
		error = 1
		message = 'Error: No transitions present for graphing'
	    endif else begin
		
		;**** Check whether keys are valid ****
	        if(os.ictm(os.imet) gt 0) then begin
	 	    ok=1
		    for i=0,os.ictm(os.imet)-1 do begin
			y=0
			for j=1,ndbndl do begin
			    if os.imaloca(i,os.imet) eq j then y=1
			endfor
		        if y eq 0 then ok = 0
		    endfor
		    if ok eq 0 then begin
		        error=1
		        message = 'Error: Bundling not completed'
		    endif
	        endif

		if error eq 0 then begin

		;**** Check that the specified key is valid ****
		f=os.ispeca(os.imet)
	        if f ne -1 then begin
		    if f lt 1 or f gt ndbndl then begin
		        error=1
		    message = 'Error: Index for Specific Line is out of'+$
			' range, 0<I<'+strtrim(string(ndbndl+1),2)
		    endif else begin
		        ok=0
		        for i=0,os.ictm(os.imet)-1 do 			$
			    if f eq os.imaloca(i,os.imet) then ok=1
		        if ok eq 0 then begin
			    error=1
	message = 'Error: Index for Specific Line is not a key being used'
		        endif
		    endelse
	        endif else begin
		    error=1
		    message = 'Error: Invalid Specific Line index'
	        endelse

		endif

	    endelse

		;**** return value or flag error ****

	    if error eq 0 then begin
                new_event = {ID:parent, TOP:event.top,                  $
                             HANDLER:0L, ACTION:'Done'}

            endif else begin
	        widget_control,state.messid,set_value=message
	        new_event = 0L
            endelse

        end

                ;**** Menu button ****

        state.outid: begin
            new_event = {ID:parent, TOP:event.top, HANDLER:0L,          $
                         ACTION:'Menu'}
        end

        ELSE: new_event = 0L

    ENDCASE

		;*********************************************
		;*** make "state" available to other files ***
		;*********************************************

    widget_control, first_child, set_uvalue=state, /no_copy

    RETURN, new_event

END

;-----------------------------------------------------------------------------

function cw_adas407_proc, topparent, dsnic1, dsnic2,			$
			  sz0, sz, sz1,					$
			  ndlev, ndtrn, ndtem, ndmet, ndosc, ndbndl,	$
			  bitfile, pipe,				$
		          procval=procval, uvalue=uvalue, 		$
		          font_large=font_large, font_small=font_small,	$
		          edit_fonts=edit_fonts, num_form=num_form

    COMMON cw_proc407_blk, ndlevcom, ndtrncom, ndtemcom, ndmetcom, 	$
			   ndosccom, ndbndlcom

    ndlevcom = ndlev
    ndtrncom = ndtrn
    ndtemcom = ndtem
    ndmetcom = ndmet
    ndosccom = ndosc
    ndbndlcom = ndbndl

		;**** Set defaults for keywords ****

    if not (keyword_set(uvalue)) then uvalue = 0
    if not (keyword_set(font_large)) then font_large = ''
    if not (keyword_set(font_small)) then font_small = ''
    if not (keyword_set(edit_fonts)) then 				$
    edit_fonts = {font_norm:'',font_input:''}
    if not (keyword_set(num_form)) then num_form = '(E10.3)'
    if not (keyword_set(procval)) then begin
	flt_arr  = fltarr(ndlev)
	temp_arr = fltarr(ndtem,3)
	str_arr  = strarr(ndosc,ndmet)
	str_arr2 = strarr(ndlev)
	int_arr  = intarr(ndmet)
	int_arr2 = intarr(ndosc,ndmet)
        temp_arr = fltarr(ndtem,3)
        ps = 	{	title	: 	'',                		$
			il	:	0,				$
			ipl	:	0,				$
			nv	:	0,				$
			tscef	:	temp_arr,			$
			nmet	:	0,				$
			npmet	:	0,				$
			imetr	:	int_arr,			$
			ipmetr	:	int_arr,			$
			imet	:	-1,				$
			ifout	:	0,				$
			itsel	:	0,				$
			selr	:	0,				$
			seld	:	0,				$
			sels	:	0,				$
			selp	:	0,				$
			sell	:	0,				$
			ltadj	:	0,				$
			ictm	:	int_arr,			$
			iuma	:	int_arr2,			$
			cstgma	:	str_arr,			$
			imaloca	:	int_arr2,			$
			ispeca	:	int_arr,			$
			strga   :       str_arr2,			$
			strga2	:	str_arr2			$
		}
    endif else begin
	ps = { 		title	: 	procval.title,    	      	$
			il	:	procval.il,			$
			ipl	:	procval.ipl,			$
			nv	:	procval.nv,			$
			tscef	:	procval.tscef,			$
			nmet	:	procval.nmet,			$
			npmet	:	procval.npmet,			$
			imetr	:	procval.imetr,			$
			ipmetr	:	procval.ipmetr,			$
			imet	:	procval.imet,			$
			ifout	:	procval.ifout,			$
			itsel	:	procval.itsel,			$
			selr	:	procval.selr,			$
			seld	:	procval.seld,			$
			sels	:	procval.sels,			$
			selp	:	procval.selp,			$
			sell	:	procval.sell,			$
			ltadj	:	procval.ltadj,			$
			ictm	:	procval.ictm,			$
			iuma	:	procval.iuma,			$
			cstgma	:	procval.cstgma,			$
			imaloca	:	procval.imaloca,		$
			ispeca	:	procval.ispeca,			$
			strga   :       procval.strga,			$
			strga2	:	procval.strga2 			$
		}
    endelse

                ;*********************************************************
                ;**** Modify certain parameters and results depending ****
                ;**** on the machine being used                       ****
                ;*********************************************************

    machine = GETENV('TARGET_MACHINE')
    if machine eq 'HPUX' then begin
        y_size = 3
        large_font = font_small
    endif else begin
        y_size = 6
        large_font = font_large
    endelse

		;********************************************************
		;**** Create the 407 Processing options/input window ****
		;********************************************************

		;**** create titled base widget ****

    parent = widget_base(topparent, UVALUE = uvalue, 			$
			 title = 'ADAS407 PROCESSING OPTIONS', 		$
 			 EVENT_FUNC = "proc407_event", 			$
			 FUNC_GET_VALUE = "proc407_get_val", 		$
			 /COLUMN)

		;******************************************************
		;**** Create a dummy widget just to hold value of *****
		;**** "state" variable so as not to get confused  *****
		;**** with any other values. Adopt IDL practice   *****
		;**** of using first child widget                 *****
		;******************************************************

    first_child = widget_base(parent)

    topbase = widget_base(first_child,/column)

		;***********************
		;**** add run title ****
		;***********************

    base = widget_base(topbase,/row)
    rc = widget_label(base,value='Title for Run  ',font=large_font)
    runid = widget_text(base,value=ps.title,xsize=38,font=large_font,/edit)

		;*******************************************************
		;**** Add the metastable selection lists            ****
		;*******************************************************

    ionbase = widget_base(topbase,column=2)
    
		;******************************
		;**** Metastable selection ****
		;******************************

    ibase1  = widget_base(ionbase,/column,/frame)

		;**** Add information labels ****
    iii1 = 'Nuclear charge: ' + strtrim(sz0,2) + ' Ion charge: '$
		+ strtrim(sz,2)
    lab1 = widget_label(ibase1,value='Ionising ion information',	$
			font=large_font)
    lab1 = widget_label(ibase1,value=iii1,font=font_small)
    
		;**** Add filename and browse button
    rc1 = cw_adas_dsbr(ibase1, dsnic1, font=font_small, filelabel='File: ')

    levend = where(strtrim(ps.strga,2) eq '*** END OF LEVELS ***')
    listsize = size(ps.strga)
    if levend(0) ge 0 then listend=levend(0)-1 else listend=listsize(1)-1
    nums = string(fix(findgen(listend+1))+1,format='(I2)')
    list = nums + '    ' + ps.strga(0:listend)
    sel=intarr(ndmet)
    
    if ps.nmet gt 0 then sel(0:ps.nmet-1)=ps.imetr(0:ps.nmet-1)
    if ps.nmet lt ndmet then sel(ps.nmet:ndmet-1)=replicate(-1,ndmet-ps.nmet)

		;**** Add the metastable selection list ****
    slab = widget_label(ibase1,value='Metastable States',font=large_font)
    idum = ndmet
    selid1 = cw_adas_sel_sel(ibase1, list, MAXSEL=idum, /ORDERED,	$
			TITLE='INDEX    LEVEL DESIGNATION           ',$
			SELECTED = sel, FONT=large_font, ytexsize=5)

		;**************************
		;**** Parent selection ****
		;**************************

    ; If ionising file is H-like do not give charge and adf04 file information
                
    if sz1 EQ sz0 then begin
       iii2  = 'Fully stripped'
       fname = ' not applicable'
    endif else begin
       iii2 = 'Nuclear charge: ' + strtrim(sz0,2) + ' Ion charge: '$
		   + strtrim(sz1,2)
       fname = dsnic2
    endelse
    
    
    ibase2  = widget_base(ionbase,/column,/frame)

		;**** Information labels ****
    lab2 = widget_label(ibase2,value='Ionised ion information',font=large_font)
    lab2 = widget_label(ibase2,value=iii2,font=font_small)

		;**** Add filename and browse button
    rc2 = cw_adas_dsbr(ibase2, fname, font=font_small, filelabel='File: ')

    if sz1 EQ sz0 then widget_control, rc2, sensitive=0 $
                  else widget_control, rc2, sensitive=1


		;**** Add the parent selection list ****
    levend = where(strtrim(ps.strga2,2) eq '*** END OF LEVELS ***')
    listsize = size(ps.strga2)
    if levend(0) ge 0 then listend=levend(0)-1 else listend=listsize(1)-1
    nums = string(fix(findgen(listend+1))+1,format='(I2)')
    if listend gt 0 then begin
        list = nums +'    ' + ps.strga2(0:listend)
        sel=intarr(ndmet)
        if ps.npmet gt 0 then sel(0:ps.npmet-1)=ps.ipmetr(0:ps.npmet-1)
        if ps.npmet lt ndmet then 					$
          sel(ps.npmet:ndmet-1)=replicate(-1,ndmet-ps.npmet)
    endif else begin
        list = ['** No parents for this member **']
        sel = [1,-1,-1,-1,-1]
    endelse
    slab = widget_label(ibase2, value='Parent States', font=large_font)
    idum = ndmet
    selid2 = cw_adas_sel_sel(ibase2, list, MAXSEL=idum, /ORDERED,	$
			 TITLE='INDEX    LEVEL DESIGNATION           ',	$
			 SELECTED = sel, FONT=large_font, ytexsize=5)

    ;**** Desensitise the parent selection widget if there are no
    ;**** parents (e.g. if last member of iso-nuclear sequence)
    ;**** Also, set flag to tell the 'Done' button code ****
    if listend lt 1 then begin
        widget_control, selid2, sensitive = 0
;        no_parents = 1
         no_parents = 0
         ps.npmet=1
    endif else no_parents = 0

    botbase = widget_base(topbase,/row)

		;********************************************************
		;**** Create string array of temperatures and its    ****
		;**** selection widget				     ****
		;********************************************************

    ttbase = widget_base(botbase,/column,/frame)
    nv = ps.nv
    table_data=strarr(nv,3)
    table_data(0:nv-1,0) = strtrim(string(procval.tscef(0:nv-1,0),	$
				   format=num_form),0)
    table_data(0:nv-1,1) = strtrim(string(procval.tscef(0:nv-1,1),	$
				   format=num_form),0)
    table_data(0:nv-1,2) = strtrim(string(procval.tscef(0:nv-1,2),	$
				   format=num_form),0)
    tempid = cw_temp_sel( ttbase, table_data,				$
			  value={index:ps.itsel,listix:ps.ifout-1},	$
			  title='Matching Temperature',			$
			  ysize=(y_size+2), font=font_small,		$
			  big_font=large_font,				$
			  bsel = ['Kelvin','eV','Reduced'],		$
			  blabel = 'Units: ')

		;****************************************************
		;**** Create Parameter Assignment Selection List ****
		;****************************************************

    sel = [' A ',' B ']
    pabase = widget_base(botbase,/column,/frame)
    palab1 = widget_label(pabase,value='Parameter Form',font=large_font)
    palab2 = widget_label(pabase,value='A : Van Maanen',font=font_small)
    palab3 = widget_label(pabase,value='B : O''Mullane/Summers',		$
			  font=font_small)

    rbase  = widget_base(pabase,/row)
    rlab   = widget_label(rbase,value='Radiative Recombination ',	$
			  font=font_small)
    selrid = cw_bselector(rbase,sel,set_value=ps.selr)
    
    dbase  = widget_base(pabase,/row)
    dlab   = widget_label(dbase,value='Dielectric Recombination',	$
			  font=font_small)
    seldid = cw_bselector(dbase,sel,set_value=ps.seld)
    
    sbase  = widget_base(pabase,/row)
    slab   = widget_label(sbase,value='Collisional Ionisation  ',	$
			  font=font_small)
    selsid = cw_bselector(sbase,sel,set_value=ps.sels)
    
    pbase  = widget_base(pabase,/row)
    plab   = widget_label(pbase,value='Total Line Power        ',	$
			  font=font_small)
    selpid = cw_bselector(pbase,sel,set_value=ps.selp)
    
    lbase  = widget_base(pabase,/row)
    llab   = widget_label(lbase,value='Specific Line Power     ',	$
			  font=font_small)
    sellid = cw_bselector(lbase,sel,set_value=ps.sell)
    
		;********************************
		;**** Create Bundling Widget ****
		;********************************

    valid_keys = strtrim(string(fix(findgen(ndbndl))+1),2)
    bubase = widget_base(botbase,/column,/frame)
    mes    = widget_label(bubase,value='Transition Assignment',font=large_font)
    bumeid = widget_label(bubase,value='Please choose a metastable.',	$
		 	  font=font_small)
    val ='Transition (Key:1-'+strtrim(string(ndbndl),2)+' = Bundling Group)'
    mes	   = widget_label(bubase,value=val,font=font_small)

		;**** Create an empty bundling box ****
;    bundid = cw_bundle(bubase, size = 20, font=font_small, ysize =y_size, $
;		  title='INDEX J   I   FIJ    WVLN ', valid_keys=valid_keys)
    bundid = cw_bundle(bubase, size = 51, font=font_small, ysize =y_size, $
		  title='INDEX J   I   FIJ    WVLN ', valid_keys=valid_keys)


		;*************************************************************
		;**** Set the value of the bundling widget if appropriate ****
		;*************************************************************

    ix = ps.imet
    if ix ne -1 then begin

    	if(ps.ictm(ix) gt 0) then begin
	    text = string(fix(findgen(ps.ictm(ix)))+1,format='(I2)') $
		 + '  ' + ps.cstgma(0:ps.ictm(ix)-1,ix)
	    key = strarr(ps.ictm(ix))
	    for i=0,ps.ictm(ix)-1 do begin
		if ps.imaloca(i,ix) eq 0 then begin
		    key(i)=''
		endif else begin
		    key(i) = strtrim(string(ps.imaloca(i,ix)),2)
		endelse
	    endfor
    	endif else begin
    	    text = 'No transitions above threshold'
	    key = 'comment'
    	endelse
        widget_control, bundid, set_value = {TEXT:text+'  ',KEY:key}
   	metstr = 'Metastable Index: '+ strtrim(string(ps.imetr(ix)+1),2)
    	widget_control, bumeid, set_value=metstr


    endif

		;**************************************************
		;**** Text widget to enter specific line index ****
		;**************************************************

    spbase = widget_base(bubase,/row)
    splab  = widget_label(spbase,value='Specific line index: ',font=font_small)
    specid = widget_text(spbase,/editable,font=font_small,xsize=2)
    if ix ne -1 then begin
	if ps.ispeca(ix) gt 0 and ps.ispeca(ix) lt ndbndl+1 then begin
      	   widget_control, specid, set_value = strtrim(string(ps.ispeca(ix)),2)
	endif
    endif
		;*************************
		;**** Default buttons ****
		;*************************

		;**** Error message ****

    messid = widget_label(parent,font=large_font, 			$
    value='Edit the processing options data and press Done to proceed')

		;**** add the exit buttons ****

    base = widget_base(parent,/row)
    menufile = bitfile + '/menu.bmp'
    read_X11_bitmap, menufile, bitmap1
    outid = widget_button(base, value=bitmap1)          ;menu button
    cancelid = widget_button(base, value='Cancel', font=large_font)
    doneid = widget_button(base, value='Done', font=large_font)
  

		;*************************************************
		;**** create a state structure for the pop-up ****
		;**** 		     window.                  ****
		;*************************************************

    new_state = { runid:runid,  		$
		  messid:messid, 		$
		  selid1:selid1,		$
		  selid2:selid2,		$
		  tempid:tempid,		$
		  selrid:selrid,		$
		  seldid:seldid,		$
		  selsid:selsid,		$
		  selpid:selpid,		$
		  sellid:sellid,		$
		  bumeid:bumeid,		$
		  bundid:bundid,		$
		  specid:specid,		$
		  outid:outid,			$
		  cancelid:cancelid,		$
		  doneid:doneid, 		$
		  il:ps.il,			$
		  ipl:ps.ipl,			$
		  nv:ps.nv,			$
		  tscef:ps.tscef,		$
		  nmet:ps.nmet,			$
		  npmet:ps.npmet,		$
		  imetr:ps.imetr,		$
		  ipmetr:ps.ipmetr,		$
		  imet:ps.imet,			$
		  ictm:ps.ictm,			$
		  iuma:ps.iuma,			$
		  cstgma:ps.cstgma,		$
		  imaloca:ps.imaloca,		$
		  ispeca:ps.ispeca,		$
		  strga:ps.strga,		$
		  strga2:ps.strga2,		$
		  font:font_large,		$
		  num_form:num_form, 		$
		  pipe:pipe,			$
                  no_parents:no_parents         $
	      }

		;************************************************************
		;**** Call d7bndl if needed so that the Fortran is happy ****
		;************************************************************

    if ps.imet ne -1 then d7bndl,new_state



                 ;**** Save initial state structure ****

    widget_control, first_child, set_uvalue=new_state,/no_copy

    RETURN, parent

END

