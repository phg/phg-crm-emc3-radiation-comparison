; Copyright (c) 1995, Strathclyde University 
; SCCS info: Module @(#)$Header: /home/adascvs/idl/adas4xx/adas407/d7bndl.pro,v 1.3 2004/07/06 13:21:23 whitefor Exp $	Date  $Date: 2004/07/06 13:21:23 $
;
;+
; PROJECT:
;       ADAS IBM MVS to   UNIX conversion
;
; NAME:
;	D7BNDL
;
; PURPOSE:
;	Communicates data to D7BNDL.FOR for bundling of transitions
;	and then reads back relevant results
;
; EXPLANATION:
;	First of all, the routine tells D7ISPF.FOR that we are doing
;	some bundling so that it can then call D7BNDL.FOR. Then information
;	from the input state is sent to the FORTRAN before reading
;	back the results via the pipe.
;
; USE:
;	The use of this routine is specific to ADAS407,
;	see cw_adas407_proc.pro.
;
; INPUTS:
;	PIPE	- The IDL unit number of the bi-directional pipe to the
;		  ADAS407 FORTRAN process.
;	STATE	- The state of a cw_adas407_proc widget. See that routine.
;
; OPTIONAL INPUTS:
;	None
;
; OUTPUTS:
;	None.
;
; OPTIONAL OUTPUTS:
;	None
;
; KEYWORD PARAMETERS:
;	None
;
; CALLS:
;	None
;
; SIDE EFFECTS:
;	Two way communications with ADAS407 FORTRAN via the
;	bi-directional UNIX pipe.
;
; CATEGORY:
;	Adas system.
;
; WRITTEN:
;       William Osborn, Tessella Support Services plc, 4-Apr-1996
;
; MODIFIED:
;	1.1	William Osborn
;		First version.
;	1.2 	William Osborn
;		Replaced ndmet by nmet in loop to print imetr
;	1.2 	William Osborn
;		Replaced nmet by state.nmet in loop to print imetr
;
; VERSION:
;	1.1	04-04-96
;	1.2	22-04-96
;-
;-----------------------------------------------------------------------------

PRO d7bndl, state

		;********************************************
		;**** Say to d7ispf that we are bundling ****
		;********************************************

    pipe = state.pipe
    ibundle = 1
    printf,pipe,ibundle
 
		;********************************
		;**** Write data to d7bndl   ****
		;********************************
		;**** state.imetr is an IDL index -> FORTRAN index

    printf,pipe,state.nmet
    for i=0,state.nmet-1 do printf,pipe,state.imetr(i)+1

		;********************************
		;**** Read data from d7bndl  ****
		;********************************

    input = 0
    readf,pipe,input
    ndmet = input
    readf,pipe,input
    ndosc = input

    cdum=''

    for i=0,ndmet-1 do begin
	readf, pipe, input
	state.ictm(i)=input
    endfor

    for i=0,ndmet-1 do begin
	for j=0,ndosc-1 do begin
	    readf, pipe, input
	    state.iuma(j,i)=input
	endfor
    endfor

    for i=0,ndmet-1 do begin
	for j=0,ndosc-1 do begin
	    readf, pipe, cdum
;	    print,cdum
	    state.cstgma(j,i)=cdum
	endfor
    endfor


END
