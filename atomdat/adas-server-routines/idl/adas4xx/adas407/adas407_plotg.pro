; Copyright (c) 1995, Strathclyde University .
; SCCS info: @(#)$Header: /home/adascvs/idl/adas4xx/adas407/adas407_plotg.pro,v 1.1 2004/07/06 10:45:50 whitefor Exp $ Date $Date: 2004/07/06 10:45:50 $
;
;+
; PROJECT:
;       ADAS IBM MVS to   UNIX conversion
;
; NAME:
;	ADAS407_PLOT
;
; PURPOSE:
;	Generates ADAS407/ graphical output.
;
; EXPLANATION:
;	This routine creates a window for the display of graphical
;	output a separate routine PLOT407 actually plots a
;	graph.
;
; USE:
;	This routine is specific to ADAS407, see e5outg.pro for
;	example use.
;
; INPUTS:
;	(Most of these inputs map exactly onto variables of the same
;	 name in the FORTRAN code.)
;
;
;	DSFULL  - String; Name of data file 
;
;	TITLE   - String array; titles to be placed above graph
;
;	TITLX   - String; user supplied comment appended to end of title
;
;	UTITLE  - String; Optional comment by user
;
;	DATE	- String; Date of graph production
;
;	NTEMP	- Integer; Number of temps.
;
;	TEMP()	- Double; Array of temps.(NTEMP)
;
;	PTOTA()	- Double; Array of selected transition power functions (NTEMP)
;
;	NPSPL	- Integer; Number of spline interpolated pow/temp pairs for
;			   graphical display
;
;	TOSA()	- Double; Spline: selected temperatures(NPSPL)
;
;	POWESA()- Double; Exact power coefficient(NPSPL)
;
;	POWISA()- Double; Init. fit power coeff.(NPSPL)
;
;	POWSSA()- Double; Simple power fit coeff.(NPSPL)
;
;	POWOSA()- Double; Opt. fit power coeff.(NPSPL)
;
;	LDEF1	- Integer; 0 - use user entered graph scales
;			   1 - use default axes scaling
;
;	XMIN    - String; Lower limit for x-axis of graph, number as string.
;
;	XMAX    - String; Upper limit for x-axis of graph, number as string.
;
;	YMIN    - String; Lower limit for y-axis of graph, number as string.
;
;	YMAX    - String; Upper limit for y-axis of graph, number as string.
;
;	HRDOUT - Integer; 1 if hardcopy output activated, 0 if not.
;
;	HARDNAME- String; Filename for harcopy output.
;
;	DEVICE	- String; IDL name of hardcopy output device.
;
;	HEADER	- String; ADAS version number header to include in graph.
;
;       BITFILE - String; the path to the directory containing bitmaps
;                 for the 'escape to series menu' button.
;
; OPTIONAL INPUTS:
;	None.
;
; OUTPUTS:
;       GOMENU  - Int; flag - set to 1 if user has selected 'escape direct
;                 to series menu', otherwise 0
;
; OPTIONAL OUTPUTS:
;	None.
;
; KEYWORD PARAMETERS:
;	FONT	- String; The name of a font to use for text in the
;		  graphical output widget.
;
; CALLS:
;	CW_ADAS_GRAPH	Graphical output widget.
;	PLOT407		Make one plot to an output device for 407.
;	XMANAGER
;
; SIDE EFFECTS:
;	This routine uses a common block to maintain its state PLOT407_BLK.
;
;	One other routine is included in this file;
;	ADAS407_PLOT_EV	Called via XMANAGER during widget management.
;
; CATEGORY:
;	Adas system.
;
; WRITTEN:
;       William Osborn, Tessella Support Services plc,  18-Apr-1996
;
; MODIFIED:
;	1.1	William Osborn
;		First Version
;
; VERSION:
;	1.1	18-04-96
;
;-
;----------------------------------------------------------------------------

PRO adas407_plotg_ev, event

    COMMON plot407_blk, data, action, win, plotdev, plotfile, 		$
                        fileopen, gomenu

    newplot = 0
    print = 0
    done = 0
    gomenu = 0
		;****************************************
		;**** Set graph and device requested ****
		;****************************************

    CASE event.action OF

	'print': begin
	    newplot = 1
	    print = 1
	end

	'done': begin
	    if fileopen eq 1 then begin
	        set_plot, plotdev
	        device, /close_file
	    endif
	    set_plot, 'X'
	    widget_control, event.top, /destroy
	    done = 1
	end

        'bitbutton' : begin
            if fileopen eq 1 then begin
                set_plot, plotdev
                device, /close_file
            endif
            set_plot,'X'
            widget_control, event.top, /destroy
            done = 1
            gomenu = 1
        end

	ELSE:	;**** Do nothing ****

    ENDCASE

		;*******************************
		;**** Make requested plot/s ****
		;*******************************

    if done eq 0 then begin

		;**** Set graphics device ****

        if print eq 1 then begin
            set_plot, plotdev
            if fileopen eq 0 then begin
                fileopen = 1
                device, filename=plotfile
	        device, /landscape
            endif
        endif else begin
            set_plot, 'X'
            wset, win
        endelse

		;**** Draw graphics ****

        plot407, data

	if print eq 1 then begin
	    message = 'Plot  written to print file.'
	    grval = {WIN:0, MESSAGE:message}
	    widget_control, event.id, set_value=grval
	endif
    endif

END

;----------------------------------------------------------------------------

PRO adas407_plotg, dsfull, title, titlx, utitle, date,    		$
		  ntemp, temp, ptota, npspl, tosa, powesa, powisa,	$
		  powssa, powosa,					$
 		  ldef1, xmin, xmax, ymin, ymax,			$
	   	  hrdout, hardname, device, header, 			$
		  bitfile,gomenu, FONT=font

    COMMON plot407_blk, data, action, win, plotdev, plotfile, 		$
                        fileopen, gomenucom

		;**** Copy input values to common ****

    plotdev = device
    plotfile = hardname
    fileopen = 0
    gomenucom = gomenu

		;************************************
		;**** Create general graph title ****
		;************************************

    title = strarr(4)

    title(0) = "TOTAL LINE POWER COEFFICIENT VERSUS TEMPERATURE"
    if ( strtrim(strcompress(utitle),2)  ne '' ) then begin
        title(0) = title(0) + ': ' + strupcase(strtrim(utitle,2))
    endif
    title(1) = 'ADAS    :' + strupcase(header)
    title(2) = titlx
    title(3) = 'KEY     : (FULL LINE - EXACT) (SHORT DASH - INITIAL)'+	$
		'(LONG DASH - SIMPLE) (DOT DASH - OPTIMISED)'

		;*********************************
		;**** Set up Y data for plots ****
		;*********************************

    y = dblarr(4, npspl)

		;**** This next line can produce divide by zero errors
		;**** with powesa approx.= 1e-300

    valid_data = where((powesa gt 1e-37) and (powesa lt 1e+37))

    if (valid_data(0) ge 0) then begin
        y(0, valid_data) = powesa(valid_data)
    endif else print,"ADAS407 : unable to plot exact data"
    valid_data = where((powisa gt 1e-37) and (powisa lt 1e+37))
    if (valid_data(0) ge 0) then begin
        y(1, valid_data) = powisa(valid_data)
    endif else print,"ADAS407 : unable to plot initial data"
    valid_data = where((powssa gt 1e-37) and (powssa lt 1e+37))
    if (valid_data(0) ge 0) then begin
        y(2, valid_data) = powssa(valid_data)
    endif else print,"ADAS407 : unable to plot simple data"
    valid_data = where((powosa gt 1e-37) and (powosa lt 1e+37))
    if (valid_data(0) ge 0) then begin
	y(3, valid_data) = powosa(valid_data)
    endif else print,"ADAS407 : unable to plot optimised data"

    ytitle = "POWER COEFFT. (W CM3)"

    nplots = 4

		;**************************************
		;**** Set up x axis and xaxis title ***
		;**************************************

    x = tosa
    xtitle = "TEMPERATURE (EV)"

  		;******************************************
		;*** if desired set up user axis scales ***
		;******************************************

    if (ldef1 eq 0) then begin
        xmin = min(x, max = xmax)
        xmin = xmin * 0.9
        xmax = xmax * 1.1
        ymin = min( y, max = ymax)
        ymin = ymin * 0.9
        ymax = ymax *1.1
    endif

		;*************************************
		;**** Create graph display widget ****
		;*************************************

    graphid = widget_base(TITLE='ADAS407 GRAPHICAL OUTPUT', 		$
			  XOFFSET=1,YOFFSET=1)
    device, get_screen_size=scrsz
    xwidth = scrsz(0)*0.75
    yheight = scrsz(1)*0.75
    multiplot = 0
    bitval = bitfile + '/menu.bmp'
    cwid = cw_adas_graph(graphid, print=hrdout, FONT=font,              $
                         xsize=xwidth, ysize=yheight,                   $
                         multiplot=multiplot, bitbutton=bitval)

                ;**** Realize the new widget ****

    widget_control, graphid, /realize

		;**** Get the id of the graphics area ****

    widget_control, cwid, get_value=grval
    win = grval.win

		;*******************************************
		;**** Put the graphing data into common ****
		;*******************************************

    data = {    Y		:	y,				$
		X		:	x,   				$
		NPSPL		:	npspl,    			$
		NPLOTS		:	nplots,				$
                TITLE		:	title,   			$
		XTITLE		:	xtitle,  			$
		YTITLE		:	ytitle,           		$
		LDEF1		:	ldef1, 				$
		XMIN		:	xmin,				$
		XMAX		:	xmax,				$
		YMIN		:	ymin,				$
		YMAX		:	ymax 				}
 
    wset, win

    plot407, data


		;***************************
                ;**** make widget modal ****
		;***************************

    xmanager, 'adas407_plotg', graphid, event_handler='adas407_plotg_ev',$
              /modal, /just_reg
    gomenu = gomenucom

END
