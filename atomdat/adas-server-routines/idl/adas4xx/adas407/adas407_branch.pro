; Copyright (c) 2002, Strathclyde University.
; SCCS info: Module @(#)$Header: /home/adascvs/idl/adas4xx/adas407/adas407_branch.pro,v 1.1 2004/07/06 10:45:35 whitefor Exp $ Date $Date: 2004/07/06 10:45:35 $
;+
; PROJECT:
;       ADAS
;
; NAME:
;       ADAS407_BRANCH
;
; PURPOSE:
;       IDL ADAS user interface, ADAS407 type of run selection.
;
; EXPLANATION:
;       This routine creates and manages a pop-up window which allows
;       the user to select between two options - Automatic or Interactive.
;       The option to cancel is also present.
;
; USE:
;       Only as part of ADAS407.
;
; INPUTS:
;       None
;
; OPTIONAL INPUTS:
;       None
;
; OUTPUTS:
;       REP     - A string; indicates the user's action when the pop-up
;                 window is terminated, i.e which button was pressed to
;                 complete the input.  Possible values are 'CANCEL',
;                 'AUTO' or 'INTR'.
;
; OPTIONAL OUTPUTS:
;       None
;
; KEYWORD PARAMETERS:
;
;    FONT_LARGE - Supplies the large font to be used for the interface widgets.
;    FONT_SMALL - Supplies the small font to be used for the interface widgets.
;
; CALLS:
;       XMANAGER        Manages the pop-up window.
;       ADAS407_BRANCH_EVENT   
;                       Event manager, called indirectly during XMANAGER
;                       event management.
;
; SIDE EFFECTS:
;       XMANAGER is called in /modal mode. Any other widgets become
;       inactive.
;
; CATEGORY:
;       Adas system.
;
; WRITTEN:
;       Martin O'Mullane
;
;
; MODIFIED:
;       1.1     Martin O'Mullane
;               First Version
;
; VERSION:
;       1.1     23-01-2002
;
;-
;-----------------------------------------------------------------------------


PRO adas407_branch_event, event

   ; Get the info structure out of the user value of the top-level base.

Widget_Control, event.top, Get_UValue=info

Widget_Control, event.id, Get_Value=userEvent


CASE userEvent OF

  'Cancel' : begin
               formdata = {cancel:1}
               *info.ptrToFormData =formdata
               widget_Control, event.top, /destroy
             end 

  'Automatic'  : begin
               formdata = {cancel:0, auto : 1, intr : 0}
               *info.ptrToFormData = formdata
               widget_Control, event.top, /destroy
            end 
                   
  'Interactive'  : begin
               formdata = {cancel:0, auto : 0, intr : 1}
               *info.ptrToFormData = formdata
               widget_Control, event.top, /destroy
            end 
                   
  else : print,'ADAS407_BRANCH_EVENT : You should not see this message! ',userEvent
                   
ENDCASE

END
;-----------------------------------------------------------------------------



FUNCTION adas407_branch, FONT_LARGE = font_large, FONT_SMALL = font_small


; Set defaults for keywords

  IF n_elements(font_large) eq 0 THEN font_large = ''
  IF n_elements(font_small) eq 0 THEN font_small = ''


 
                ;********************************************
		;**** create modal top level base widget ****
                ;********************************************
                
  parent = Widget_Base(Column=1, Title='ADAS 407 ANALYSIS CHOICE', $
                       XOFFSET=100, YOFFSET=1)

  rc    = widget_label(parent,value='Iso-nuclear parameter file generation', $
                       font=font_large)
  rc    = widget_label(parent,value='  ',font=font_large)
  rc    = widget_label(parent,value='Choose method of analysis:', $
                       font=font_large)

; action buttons
  
  base   = widget_base(parent,/row)
  autoID = widget_button(base,value='Automatic',font=font_large)
  intrID = widget_button(base,value='Interactive',font=font_large)
  skip   = widget_label(base,value='  ',font=font_large)
  
  base     = widget_base(parent,/row)
  cancelID = widget_button(base,value='Cancel',font=font_large)

; Realize the ADAS407_braqnch input widget.

   widget_Control, parent, /realize


; Create a pointer to store the information collected from the form.
; The initial data stored here is set to CANCEL, so nothing needs to
; be done if the user kills the widget with the mouse.

   ptrToFormData = Ptr_New({cancel:1})

; Create an info structure with program information.

  info = { autoID           :  autoID,             $
           intrID           :  intrID,             $
           font_large       :  font_large,         $
           ptrToFormData    :  ptrToFormData       }  
            
               
; Store the info structure in the user value of the top-level base and launch
; the widget into the world.

  widget_control, parent, Set_UValue=info

  XManager, 'adas407_branch', parent, Event_Handler='ADAS407_BRANCH_EVENT'


; When the widget is destroyed, the block is released, and we
; return here. Get the data that was collected by the widget
; and stored in the pointer location. Finally free the pointer.

formdata = *ptrToFormData

rep='CONTINUE'
IF N_Elements(formdata) EQ 0 THEN BEGIN
   Ptr_Free, ptrToFormData
   rep ='CANCEL'
ENDIF
 
IF formdata.cancel EQ 1 THEN BEGIN
   Ptr_Free, ptrToFormData
   rep ='CANCEL'
ENDIF

if rep eq 'CONTINUE' then begin
   if formdata.auto EQ 1 then rep = 'AUTO'
   if formdata.intr EQ 1 then rep = 'INTR'
   Ptr_Free, ptrToFormData
endif       


return, rep

END
