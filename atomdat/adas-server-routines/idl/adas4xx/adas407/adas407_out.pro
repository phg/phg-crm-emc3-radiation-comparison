; Copyright (c) 1995, Strathclyde University .
; SCCS info: Module @(#)$Header: /home/adascvs/idl/adas4xx/adas407/adas407_out.pro,v 1.2 2004/07/06 10:45:46 whitefor Exp $ Date $Date: 2004/07/06 10:45:46 $
;+
; PROJECT:
;       ADAS IBM MVS to   UNIX conversion
;
; NAME:
;	ADAS407_OUT
;
; PURPOSE:
;	IDL ADAS user interface, output options.
;
; EXPLANATION:
;	This routine creates and manages a pop-up window which allows
;	the user to select options and input data to control ADAS407
;	graphical and file output.
;
; USE:
;	This routine is ADAS407 specific, see d7spf1.pro for how it
;	is used.
;
; INPUTS:
;	INVAL	- The input widget structure
;
;	PROCVAL - The processing widget structure
;
;	VAL	- A structure which determines the initial settings of
;		  the output options widget.  The value is passed
;		  unmodified into cw_adas501_out.pro.
;
;		  See cw_adas501_out.pro for a full description of this
;		  structure.
;
;       BITFILE - String; the path to the dirctory containing bitmaps
;                 for the 'escape to series menu' button.
;
; OPTIONAL INPUTS:
;	None.
;
; OUTPUTS:
;	ACT	- String; 'Proc', 'Input', 'Cancel' or 'Menu' for the button the
;		  user pressed to terminate the output options window.
;
; OPTIONAL OUTPUTS:
;	None.
;
; KEYWORD PARAMETERS:
;	DEVLIST	- A string array;  Each element of the array is the
;		  name of a hardcopy graphics output device.  This
;		  list must mirror the DEVCODE list.  DEVLIST is the
;		  list which the user will see.
;		  e.g ['Post-Script','HP-PCL','HP-GL']
;
;	FONT	- Supplies the font to be used for the interface widgets.
;
; CALLS:
;	CW_ADAS501_OUT	Creates the output options widget.
;	DYNLABEL	Sets dynamic_resize keyword for label widgets.
;	XMANAGER
;	See side effects for widget management routine.
;
; SIDE EFFECTS:
;	This routine uses a common block OUT407_BLK to maintain its state.
;	ADAS407_OUT_EV	is included in this file and is called
;			indirectly from XMANAGER during widget
;			management.
;
; CATEGORY:
;	Widgets
;
; WRITTEN:
;       William Osborn, Tessella Support Services plc, 16-Apr-1996
;	Largely copied from adas505_out.pro v1.2
;	
; MODIFIED:
;	1.1	William Osborn
;		First Version
;	1.2    	William Osborn
;		Added dynlabel procedure
; VERSION:
;	1.1	16-05-96
;	1.2	09-07-96
;-
;-----------------------------------------------------------------------------

PRO adas407_out_ev, event

    COMMON out407_blk,action,value


		;**** Find the event type and copy to common ****

    action = event.action

    case action of

		;**** 'View Graph(s)' button ****

	'Graph'  : begin

		;**** Get the output widget value ****

	    widget_control,event.id,get_value=value 

            widget_control,event.top,/destroy
	    
        end
		;**** 'Output and back to file input' button ****

	'Input'  : begin

		;**** Get the output widget value ****

	    widget_control,event.id,get_value=value 

            widget_control,event.top,/destroy

        end

		;**** 'Back to Processing' button ****

	'Proc': widget_control, event.top, /destroy

                ;**** 'Menu' button ****

        'Menu': widget_control, event.top, /destroy
	
	ELSE:	;**** Do nothing ****

    ENDCASE

END

;-----------------------------------------------------------------------------

PRO adas407_out, inval, procval, val, act, bitfile, lsetp, devlist=devlist, $
	 font_large=font_large, font_small=font_small

    COMMON out407_blk,action,value

		;**** Copy value to common ****

    value = val

		;**** Set defaults for keywords ****

    if not (keyword_set(font_small)) then font_small = ''
    if not (keyword_set(font_large)) then font_large = ''
    if not (keyword_set(devlist)) then devlist = ''

		;***************************************
		;**** Pop-up a new widget           ****
		;***************************************

                ;**** create base widget ****

    outid = widget_base(title='ADAS407 OUTPUT OPTIONS', xoffset=100,	$
                        yoffset=1)

		;**** Declare output options widget ****

    cwid = cw_adas407_out(outid, inval, procval, bitfile, lsetp,	$
			  value=value,	$
			  devlist=devlist, font_large=font_large,	$
			  font_small=font_small)

		;**** Realize the new widget ****

    dynlabel, outid
    widget_control, outid, /realize

		;**** make widget modal ****

    xmanager, 'adas407_out', outid, event_handler='adas407_out_ev',	$
              /modal, /just_reg
 
		;**** Return the output value from common ****

    act = action
    val = value

END

