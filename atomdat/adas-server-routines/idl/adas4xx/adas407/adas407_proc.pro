; Copyright (c) 1995, Strathclyde University .
; SCCS info: Module @(#)$Header: /home/adascvs/idl/adas4xx/adas407/adas407_proc.pro,v 1.3 2004/07/06 10:46:02 whitefor Exp $	Date  $Date: 2004/07/06 10:46:02 $
;+
; PROJECT:
;       ADAS IBM MVS to   UNIX conversion
;
; NAME:
;	ADAS407_PROC
;
; PURPOSE:
;	IDL ADAS user interface, processing options/input.
;
; EXPLANATION:
;	This routine creates and manages a pop-up window which allows
;	the user to select options and input data to control ADAS407
;	processing.
;
; USE:
;	This routine is ADAS407 specific, see d7ispf.pro for how it
;	is used.
;
; INPUTS:
;	PROCVAL	- A structure which determines the initial settings of
;		  the processing options widget.  The value is passed
;		  unmodified into cw_adas407_proc.pro.
;
;		  See cw_adas407_proc.pro for a full description of this
;		  structure.
;
;	DSNIC1	- String; The full system file name of the first input 
;		          dataset selected by the user for processing.
;
;	DSNIC2	- String; The full system file name of the second input 
;		          dataset selected by the user for processing.
;
;   	z0	- Double; nuclear charge from first copase file
;   	z1	- Double; ion charge from first copase file
;   	z02	- Double; nuclear charge from second copase file
;   	z12	- Double; ion charge from second copase file
;	ndlev 	- Integer; Max. number of levels
;	ndtrn 	- Integer; Max. number of transitions
;	ndtem 	- Integer; Max. number of temperatures
;	ndmet 	- Integer; Max. number of metastables
;	ndosc 	- Integer; Max. number of oscillator strengths
;
;       BITFILE - String; the path to the directory containing bitmaps
;                 for the 'escape to series menu' button.
;
; OPTIONAL INPUTS:
;	None
;
; OUTPUTS:
;	PROCVAL	- On output the structure records the final settings of
;		  the processing selections widget if the user pressed the
;		  'Done' button, otherwise it is not changed from input.
;
;	ACT	- String; Either 'Done' or 'Cancel' for the button the
;		  user pressed to terminate the processing options
;		  window.
;
; OPTIONAL OUTPUTS:
;	None
;
; KEYWORD PARAMETERS:
;	FONT_LARGE - The name of a larger font.
;
;	FONT_SMALL - The name of a smaller font.
;
;	EDIT_FONTS - A structure of two fonts used in the table editor
;		     adas_edtab.pro, {font_norm:'',font_input:''}.
;
; CALLS:
;	DYNLABEL	Sets dynamic_resize keyword for label widgets.
;	CW_ADAS407_PROC	Declares the processing options widget.
;	ADAS407_PROC_EV Called indirectly during widget management,
;			routine included in this file.
;	XMANAGER
;
; SIDE EFFECTS:
;       This widget uses a common block PROC407_BLK, to pass the
;       variables VALUE and ACTION between the two routines. 
;
; CATEGORY:
;	Widgets
;
; WRITTEN:
;       William Osborn, Tessella Support Services plc, 1-Apr-1996
;	   Skeleton from adas505_proc
; MODIFIED:
;	1.1	William Osborn
;		First version
;	1.2	William Osborn
;		Changed the way the charges are passed, sz, ...
;	1.3    	William Osborn
;		Added dynlabel procedure
;
; VERSION:
;       1.1	02-04-96
;	1.2 	23-04-96
;	1.3	09-07-96
; 
;
;-
;-----------------------------------------------------------------------------

PRO adas407_proc_ev, event

    COMMON proc407_blk, action, value

    action = event.action

    CASE event.action OF

		;**** 'Done' button ****

	'Done'  : begin

		;**** Get the output widget value ****

	    widget_control, event.id, get_value=value 
	    widget_control, event.top, /destroy
	end

		;**** 'Cancel' button ****

	'Cancel': widget_control, event.top, /destroy

                ;**** 'Menu' button ****

        'Menu': widget_control, event.top, /destroy

	ELSE:			;**** Do nothing ****

    ENDCASE

END

;-----------------------------------------------------------------------------

PRO adas407_proc, procval, dsnic1, dsnic2, sz0, sz, sz1,		$
		ndlev, ndtrn, ndtem, ndmet, ndosc, ndbndl,		$
               	act, bitfile, pipe, FONT_LARGE=font_large, 		$
                FONT_SMALL=font_small, EDIT_FONTS=edit_fonts


		;**** declare common variables ****

    COMMON proc407_blk, action, value

		;**** Copy "procval" to common ****

    value = procval

		;**** Set defaults for keywords ****

    IF NOT (KEYWORD_SET(font_large)) THEN font_large = ''
    IF NOT (KEYWORD_SET(font_small)) THEN font_small = ''
    IF NOT (KEYWORD_SET(edit_fonts)) THEN 				$
    edit_fonts = { font_norm:'', font_input:''}
		
                ;**** create base widget ****

    procid = widget_base(TITLE='ADAS407 PROCESSING OPTIONS', 		$
                         XOFFSET=50, YOFFSET=1)

		;**** Declare processing widget ****

    cwid = cw_adas407_proc(procid, dsnic1, dsnic2,			$
			   sz0, sz, sz1,				$
			   ndlev, ndtrn, ndtem, ndmet, ndosc, ndbndl,	$
			   bitfile, pipe, PROCVAL=value,		$
		 	   FONT_LARGE=font_large, FONT_SMALL=font_small,$
			   EDIT_FONTS=edit_fonts)

		;**** Realize the new widget ****

    dynlabel, procid
    widget_control, procid, /realize

		;**** make widget modal ****

    xmanager, 'adas407_proc', procid, event_handler='adas407_proc_ev', 	$
              /modal, /just_reg

		;*** copy value back to procval for return to d7ispf ***

    act = action
    procval = value
 
END

