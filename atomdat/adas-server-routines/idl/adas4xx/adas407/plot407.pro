; Copyright (c) 1995, Strathclyde University .
; SCCS INFO : @(#)$Header: /home/adascvs/idl/adas4xx/adas407/plot407.pro,v 1.2 2004/07/06 14:33:12 whitefor Exp $ Date $Date: 2004/07/06 14:33:12 $
;+
; PROJECT:
;       ADAS IBM MVS to   UNIX conversion
;
; NAME:
;	PLOT407
;
; PURPOSE:
;	Plot one graph for ADAS407.
;
; EXPLANATION:
;	This routine plots ADAS407 output for a single graph.
;
; USE:
;	Use is specific to ADAS407.  See adas407_plot.pro for
;	example.
;
; INPUTS:
;	DATA	- Structure; the information needed for the plot. The structure
;		  is:
;    	{	Y		:	y,				$
;		X		:	x,   				$
;		NPSPL		:	npspl,    			$
;               TITLE		:	title,   			$
;		XTITLE		:	xtitle,  			$
;		YTITLE		:	ytitle,           		$
;		LDEF1		:	ldef1, 				$
;		XMIN		:	xmin,				$
;		XMAX		:	xmax,				$
;		YMIN		:	ymin,				$
;		YMAX		:	ymax 				}
; 
;	where the elements are,
;
;	X	- Double array; list of x values.
;
;	Y	- 2D double array; y values, 2nd index ordinary level,
;		  indexed by iord1 and iord2.
;
;	NPSPL   - Integer : Number of data points and spline fit points
;
;	TITLE	- String array : General title for program run. 5 lines.
;
;	XTITLE  - String : title for x-axis annotation
;
;	YTITLE  - String : title for y-axis annotation
;
;	LDEF1	- Integer; 1 if user specified axis limits to be used, 
;		  0 if default scaling to be used.
;
;	XMIN	- String; Lower limit for x-axis of graph, number as string.
;
;	XMAX	- String; Upper limit for x-axis of graph, number as string.
;
;	YMIN	- String; Lower limit for y-axis of graph, number as string.
;
;	YMAX	- String; Upper limit for y-axis of graph, number as string.
;
; OPTIONAL INPUTS:
;	None.
;
; OUTPUTS:
;	None.
;
; OPTIONAL OUTPUTS:
;	None.
;
; KEYWORD PARAMETERS:
;	None.
;
; CALLS:
;	None.
;
; SIDE EFFECTS:
;	None.
;
; CATEGORY:
;	Adas system.	
;
; WRITTEN:
;       William Osborn, Tessella Support Services plc, 19-Apr-1996
;
; MODIFIED:
;       1.1     William Osborn
;		First Release
;	1.2     William Osborn
;               Added check for new environment variable VERY_SMALL
;               If this is set to the string "YES" (preferably in the
;               .adas_setup file) then the font size used on the graphs
;               is reduced and the positioning of labels adjusted
;               slightly to make things fit on a very small screen.
;
; VERSION:
;	1.1	19-04-96
;	1.2	11-10-96
;
;-
;----------------------------------------------------------------------------

PRO plot407, data

    COMMON Global_lw_data, left, right, top, bottom, grtop, grright

		;****************************************************
		;**** Suitable character size for current device ****
		;**** Aim for 60 characters in y direction.      ****
		;****************************************************

    charsize = (!d.y_vsize/!d.y_ch_size)/60.0 
    small_check = GETENV('VERY_SMALL')
    if small_check eq 'YES' then charsize=charsize*0.8

		;**** set makeplot counter ****

    makeplot = 1

		;***************************************
		;**** Construct graph title         ****
		;**** "!C" is the new line control. ****
		;***************************************

    !p.font=-1
    if small_check eq 'YES' then begin
        gtitle = data.title(0) + "!C!C!C" + data.title(1) + "!C!C" + $
          data.title(2) + "!C!C" + data.title(3)
    endif else begin
        gtitle = data.title(0) + "!C!C" + data.title(1) + "!C" + $
          data.title(2) + "!C" + data.title(3)
    endelse

		;**** Find x and y ranges for auto scaling ****

    xmin=data.xmin
    ymin=data.ymin
    xmax=data.xmax
    ymax=data.ymax
    if data.ldef1 eq 0 then begin

		;********************************************
		;**** identify values in the valid range ****
		;**** plot routines only work within     ****
		;**** single precision limits.	         ****
		;********************************************

        xvals = where(data.x gt 1.0e-37 and data.x lt 1.0e37)
        yvals = where(data.y gt 1.0e-37 and data.y lt 1.0e37)
        if xvals(0) gt -1 then begin
            xmax = max(data.x(xvals), min=xmin)
            makeplot = 1
        endif else begin
            makeplot = 0
        endelse
        if yvals(0) gt -1 then begin
            ymax = max(data.y(yvals), min=ymin)
            makeplot = 1
        endif else begin
            makeplot = 0
        endelse
        style = 0
    endif else begin
	
		;Check that at least some data in in axes range ***
        xvals = where(data.x ge xmin and data.x le xmax)
        yvals = where(data.y ge ymin and data.y le ymax)
        if xvals(0) eq -1 or yvals(0) eq -1 then begin
            makeplot = 0
        endif
        style = 1
    endelse


		;**** Set up log-log plotting axes ****

    plot_oo, [xmin,xmax], [ymin,ymax], /nodata, ticklen=1.0, 		$
	     position=[left,bottom,grright,grtop], 			$
	     xtitle=data.xtitle, ytitle=data.ytitle, xstyle=style, 	$
	     ystyle=style, charsize=charsize

    if makeplot eq 1 then begin

		;**********************
		;****  Make plots  ****
		;**********************

	if data.nplots gt 0 then begin
            oplot, data.x, data.y(0,*), linestyle=0
	    if data.nplots gt 1 then begin
       	        oplot, data.x, data.y(1,*), linestyle=2
		if data.nplots gt 2 then begin
		    oplot, data.x, data.y(2,*), linestyle=5
		    if data.nplots gt 3 then begin
			oplot, data.x, data.y(3,*), linestyle=3
		    endif
		endif
	    endif
	endif

    endif else begin
        print, "ADAS407 : No data found in these axes ranges"
    endelse

		;**** Annotation to plot ****

    rhs = grright + 0.02

		;**** plot title ****

    xyouts, (left-0.05), (top+0.05), gtitle, /normal, alignment=0.0, 	$
	    charsize=charsize


END
