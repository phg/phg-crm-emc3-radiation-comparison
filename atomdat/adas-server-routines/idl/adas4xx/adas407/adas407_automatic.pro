;-
; PROJECT:
;       ADAS
;
; NAME:
;       ADAS407_AUTOMATIC
;
; PURPOSE:
;       Runs ADAS407 in automatic mode.
;
; EXPLANATION:
;       Given a minimum of input - location of adf04 files, element,
;       minimum and maximum ionisation stages along with adf03 and
;       paper.txt output files - this function tests the input and
;       runs the ADAS407 code. It puts up a progress bar during the
;       calculation.
;
; USE:
;
; INPUTS:
;       autoval  : structure for retained between runs information
;                     ROOTPATH  :  directory of adf04 files (fully qualified)
;                     CENTROOT  :  central ADAS location
;                     USERROOT  :  user's ADAS data
;                     A03OUT    :  }
;                     A03APP    :  }
;                     A03REP    :  } details of output adf03 file
;                     A03DSN    :  }
;                     A03DEF    :  }
;                     A03MES    :  }
;                     TEXOUT    :  }                              
;                     TEXAPP    :  }                              
;                     TEXREP    :  } details of output paper.txt file 
;                     TEXDSN    :  }                              
;                     TEXDEF    :  }                              
;                     TEXMES    :  }                              
;                     ELEM      :  element symbol (eg Fe)
;                     ION_LOW   :  minimum ion for adf03 output
;                     ION_HIGH  :  maximun ion stage
;
;       fortdir  : location of ADASFORT - fortran executables.
;       bitfile  : location of bitmaps for the menu button.
;
; OPTIONAL INPUTS:
;       None
;
; OUTPUTS:
;       REP     - A string; indicates the return status. Possible values 
;                 are 'CANCEL', 'MENU' or 'CONTINUE'.
;
; OPTIONAL OUTPUTS:
;       None
;
; KEYWORD PARAMETERS:
;
;    FONT_LARGE - Supplies the large font to be used for the interface widgets.
;    FONT_SMALL - Supplies the small font to be used for the interface widgets.
;
; CALLS:
;       XMANAGER                  Manages the pop-up window.
;       ADAS407_AUTOMATIC_FILE    Event manager for checking adf04 
;                                 directory selection.
;       ADAS407_AUTOMATIC_MENU    Manage bitmap button events
;       ADAS407_AUTOMATIC_EVENT   Event handler for everything else
;
;
;
; SIDE EFFECTS:
;       XMANAGER is called in /modal mode. Any other widgets become
;       inactive.
;
; CATEGORY:
;       Adas system.
;
; WRITTEN:
;       Martin O'Mullane
;
;
; MODIFIED:
;       1.1     Martin O'Mullane
;                - First Version
;       1.2     Martin O'Mullane
;                - Option for A or B parameters can be made.
;                - A prefix (normally ls or ic) can be used to
;                  construct file names.
;       1.3     Martin O'Mullane
;                - Send adf25 and metastable information to fortran.
;                  Not used here but added to permit extra functionality 
;                  in run_adas407.
;
; VERSION:
;       1.1     23-01-2002
;       1.2     23-05-2003
;       1.3     04-11-2009
;
;-
;-----------------------------------------------------------------------------


FUNCTION adas407_automatic_file, event

   ; Sensitise 'Done' if we are ready to continue 

   Widget_Control, event.top, Get_UValue=info
     
   Widget_Control, info.a04ID, Get_Value=file

   filename = file.rootpath
   file_acc, filename, exist, read, write, execute, filetype
        
   if event.action EQ 'newroot' AND exist EQ 1 then begin
      Widget_Control, info.doneID, sensitive=1
   endif else begin
      Widget_Control, info.doneID, sensitive=0
   endelse
   
   RETURN, 0

END

;-----------------------------------------------------------------------------



PRO adas407_automatic_menu, event

; The 'normal' get_value from a widget_button does not work with a
; bitmap button. Hence this specialised event handler.

   ; Get the info structure out of the user value of the top-level base.

Widget_Control, event.top, Get_UValue=info


 formdata = {cancel : 0, menu:1}
 *info.ptrToFormData =formdata
 widget_Control, event.top, /destroy

END
;-----------------------------------------------------------------------------



PRO adas407_automatic_event, event

   ; Get the info structure out of the user value of the top-level base.

Widget_Control, event.top, Get_UValue=info

Widget_Control, event.id, Get_Value=userEvent


CASE userEvent OF

  'Cancel' : begin
               formdata = {cancel:1, menu : 0}
               *info.ptrToFormData =formdata
               widget_Control, event.top, /destroy
             end 

  'Done'   : begin
                 
                ; gather the data for return to calling program
                ; 'Done' is not sensitised until valid filenames
                ; have been chosen.
               
                err  = 0
                mess = ''
                
                widget_Control, info.a04ID, Get_Value=a04dir
                widget_Control, info.elemID, Get_Value=elem
                widget_Control, info.ion_lowID, Get_Value=ion_low
                widget_Control, info.ion_highID, Get_Value=ion_high
                widget_Control, info.a03ID, Get_Value=a03
                widget_Control, info.paperID, Get_Value=paper
                
                ; does the adf04 directory exist?
            
                filename = a04dir.rootpath
                file_acc, filename, exist, read, write, execute, filetype
                if exist EQ 0 then begin
                   err  = 1
                   mess = 'Select a valid adf04 directory'
                endif 
               
                ; moan if upper is GT lower (rather than swapping silently)
               
                if err EQ 0 AND ion_low LT 0 then begin
                   err  = 1
                   mess = 'Lower ion stage cannot be negative'
                endif
                
                if err EQ 0 AND ion_high LT ion_low then begin
                   err  = 1
                   mess = 'Lower ion stage is bigger than upper!'
                endif
                
                ; Is it a valid element
               
                i4eiz0, elem[0], iz
                if err EQ 0 AND iz EQ 0 then begin
                   err  = 1
                   mess = 'This is not a real element'
                endif
                 
                if err EQ 0 AND iz LT ion_high+1 then begin
                   err  = 1
                   mess = 'Cannot have this many ion stages'
                endif
                
                ; are the ion selections consistent with adf04 selection?
               
                if err EQ 0 then begin
                
                   widget_Control, info.preID, Get_Value=tmp
                   prefix = tmp[0]
                   
                   root = a04dir.rootpath[0]
                   zz   = strcompress(elem[0],/remove_all)
                   srch = strcompress(root + '/' + prefix + '#' + zz + '*.dat',/remove_all)
                   list = findfile(srch, count=num_files)
                   
                   if num_files GT 0 then begin
                      
                      ; Find all adf04 files in iso-nuclear directory
                      
                      files = ''
                      for j = 0, num_files-1 do begin
                         lpos  = strpos(list[j], '/', /reverse_search)
                         files = [files, strmid(list[j],lpos+1)]
                      endfor
                      files = files[1:*]
                      
                      ; Construct requested file list into ascending 
                      ; ion order - not alphabetical order.
                      ;
                      ; add 2 to high-low to give proper number of 
                      ; ion stages plus one extra 
                      ;
                      ; eg, 0 - 4 requires files 0 - 5
                      
                      req = ion_low + indgen(ion_high-ion_low+2)
                      req = prefix + '#' + elem[0] + string(req) + '.dat'
                      req = strcompress(req,/remove_all)
                      
                      ; And determine which files are common 

                      commonfiles = ''
                      count       = 0
                      for j = 0, n_elements(req)-1 do begin
                         ind = where(files EQ req[j], cnt)
                         if cnt EQ 1 then begin
                            commonfiles = [commonfiles, files[ind]]
                            count = count + 1
                         endif
                      endfor
                      if count GT 0 then commonfiles = commonfiles[1:*]
                      
                      if count EQ 0 then begin
                         err  = 1
                         mess = 'No valid adf04 files in the directory'
                      endif else begin
                         if count NE n_elements(req) then begin
                            err  = 1
                            mess = 'Not all required adf04 files are present'
                         endif
                      endelse
                      
                   endif else begin
                      err  = 1
                      mess = 'No adf04 files in the directory'
                   endelse
                
                endif
             
                ; Get selections for parameterisations - no check
                ; as cw_bgroup defaults to 0 if nothing set.
                
                widget_control, info.ionID, get_value = ionval
                widget_control, info.rrID,  get_value = rrval
                widget_control, info.drID,  get_value = drval
                widget_control, info.powID, get_value = powval
                
                
                if err EQ 0 AND paper.outbut EQ 1 AND strtrim(paper.message) NE '' then err=1
                if err EQ 0 AND a03.outbut EQ 1 AND strtrim(a03.message) NE '' then err=1

                if err EQ 0 AND a03.outbut EQ 0 then begin
                   err  = 1
                   mess = 'An adf03 file is required output'
                endif
              
                if err EQ 0 then begin
                   formdata = {a04           : a04dir,    $
                               a03           : a03,       $
                               paper         : paper,     $
                               prefix        : prefix,    $
                               elem          : elem[0],   $
                               ion_low       : ion_low,   $
                               ion_high      : ion_high,  $
                               ionval        : ionval,    $
                               rrval         : rrval,     $
                               drval         : drval,     $
                               powval        : powval,    $
                               process_files : req,       $
                               cancel        : 0,         $
                               menu          : 0          }
                    *info.ptrToFormData = formdata
                    widget_control, event.top, /destroy
                endif else begin
                   widget_Control, info.messID, Set_Value=mess
                endelse

             end

                   
  else : print,'ADAS407_AUTOMATIC_EVENT : You should not see this message! ',userEvent
                   
ENDCASE

END
;-----------------------------------------------------------------------------



FUNCTION adas407_automatic, autoval, fortdir, bitfile,   $
                            FONT_LARGE = font_large,     $
                            FONT_SMALL = font_small


; Keyword defaults

  IF n_elements(font_large) eq 0 THEN font_large = ''
  IF n_elements(font_small) eq 0 THEN font_small = ''


; Path names may be supplied with or without the trailing '/'.  Add
; this character where required so that USERROOT will always end in
; '/' on output.
  
  if strtrim(autoval.rootpath) eq '' then begin
      autoval.rootpath = './'
  endif else if                                                   $
  strmid(autoval.rootpath, strlen(autoval.rootpath)-1,1) ne '/' then begin
      autoval.rootpath = autoval.rootpath+'/'
  endif



; Modal top level base widget


  parent = Widget_Base(Column=1, Title='ADAS 407 AUTOMATIC BRANCH CHOICES', $
                       XOFFSET=100, YOFFSET=1)

                
  rc = widget_label(parent,value='  ',font=font_large)


  base  = widget_base(parent, /column)



; Location of adf04 files
              

  fltitle = widget_label(base, font=font_large,              $
                          value='Location (directory) of adf04 files:-')
  flbase  = widget_base(base, /column, /frame)
  flval   = { ROOTPATH : autoval.rootpath,         $
              CENTROOT : autoval.centroot,         $
              USERROOT : autoval.userroot          }
  
  a04ID   = cw_adas_root(flbase, value=flval, font=font_large, $
                         event_funct='ADAS407_AUTOMATIC_FILE')


; Program options
;
;    Number of ionisation stages and file prefix

  optbase    = widget_base(base, /row, /frame)

  elem       = autoval.elem
  ion_low    = autoval.ion_low
  ion_high   = autoval.ion_high

  ionbase    =  widget_base(optbase, /column)
  mlab       =  widget_label(ionbase,value='Enter range of ionisation stages:-',$
                             font=font_large)
  mcol       = widget_base(ionbase,/frame,/column)
  preID      = cw_field(mcol, fieldfont=font_large, font=font_large, /string, $
                        title='Prefix (ls or ic) : ', value=elem ) 
  elemID     = cw_field(mcol, fieldfont=font_large, font=font_large, /string, $
                        title='Element           : ', value=elem ) 
  ion_lowID  = cw_field(mcol, fieldfont=font_large, font=font_large, /integer, $
                        title='Lower ion stage   : ', value=ion_low ) 
  ion_highID = cw_field(mcol, fieldfont=font_large, font=font_large, /integer, $
                        title='Upper ion stage   : ', value=ion_high ) 


;    A or B type approximations

  appbase = widget_base(optbase, /column)
  
  sel     = ['A', 'B']
 
  mlab    =  widget_label(appbase,value='Enter parameterisations:-',$
                             font=font_large)

  ionID   = cw_bgroup(appbase, sel, exclusive=1,row=1,                 $
                       label_left = 'Ionisation   :', font=font_large, $
                       event_func='ADAS_PROC_NULL_EVENTS')
  rrID    = cw_bgroup(appbase, sel, exclusive=1,row=1,                 $
                       label_left = 'Rad. recom.  :', font=font_large, $
                       event_func='ADAS_PROC_NULL_EVENTS')
  drID    = cw_bgroup(appbase, sel, exclusive=1,row=1,                 $
                       label_left = 'Diel. recom. :', font=font_large, $
                       event_func='ADAS_PROC_NULL_EVENTS')
  powID   = cw_bgroup(appbase, sel, exclusive=1,row=1,                 $
                       label_left = 'Power        :', font=font_large, $
                       event_func='ADAS_PROC_NULL_EVENTS')


   
; Output files

  a03val   =  { outbut   : autoval.A03OUT, $
                appbut   : autoval.A03APP, $
                repbut   : autoval.A03REP, $
                filename : autoval.A03DSN, $
                defname  : autoval.A03DEF, $
                message  : autoval.A03MES  }


  paperval   =  { outbut   : autoval.TEXOUT, $
                  appbut   : autoval.TEXAPP, $
                  repbut   : autoval.TEXREP, $
                  filename : autoval.TEXDSN, $
                  defname  : autoval.TEXDEF, $
                  message  : autoval.TEXMES  }


  base    =  widget_base(base, /column)
  mlab    =  widget_label(base,value=' ', font=font_large)
  mlab    =  widget_label(base,value='Output files:-', font=font_large)

  mcol    = widget_base(parent, /column,/frame)
  a03ID   = cw_adas_outfile(mcol, OUTPUT='ATOMPARS (adf03) file:',   $
                                  VALUE=a03val, FONT=font_large, xsize=50)
  paperID = cw_adas_outfile(mcol, OUTPUT='Text Output:',   $
                                  VALUE=paperval, FONT=font_large)
  



; Message/errors and controlling buttons

  menufile = bitfile + '/menu.bmp'
  read_X11_bitmap, menufile, bitmap1
                
  mrow     = widget_base(parent,/row,/align_center)
  messID   = widget_label(mrow,font=font_large, $
                          value='         Choose run options             ')                

  mrow     = widget_base(parent,/row)
  menuID   = widget_button(mrow,value=bitmap1,font=font_large, $
                           event_pro='ADAS407_AUTOMATIC_MENU')
  cancelID = widget_button(mrow,value='Cancel',font=font_large)
  doneID   = widget_button(mrow,value='Done',font=font_large)



; Initial settings 


    filename = autoval.rootpath
    file_acc, filename, exist, read, write, execute, filetype
    if exist ne 1 then begin
        Widget_Control, doneID, sensitive=0
    endif

    widget_control, preID, set_value = autoval.prefix

    widget_control, ionID, set_value = autoval.ionval
    widget_control, rrID,  set_value = autoval.rrval
    widget_control, drID,  set_value = autoval.drval
    widget_control, powID, set_value = autoval.powval



; Realize the ADAS407_AUTOMATIC input widget.

   dynlabel, parent
   widget_Control, parent, /realize

                            

; Create a pointer to store the information collected from the form.
; The initial data stored here is set to CANCEL, so nothing needs to
; be done if the user kills the widget with the mouse.

   ptrToFormData = Ptr_New({cancel:1})

; Create an info structure with program information.

  info = { a04ID           :  a04ID,              $
           preID           :  preID,              $
           elemID          :  elemID,             $
           ion_lowID       :  ion_lowID,          $
           ion_highID      :  ion_highID,         $
           ionID           :  ionID,              $
           rrID            :  rrID,               $
           drID            :  drID,               $
           powID           :  powID,              $
           a03ID           :  a03ID,              $
           paperID         :  paperID,            $
           doneID          :  doneID,             $
           messID          :  messID,             $
           font_large      :  font_large,         $
           ptrToFormData   :  ptrToFormData       }  
            
               
; Store the info structure in the user value of the top-level base and launch
; the widget into the world.

  widget_control, parent, Set_UValue=info

  XManager, 'adas407_automatic', parent, Event_Handler='ADAS407_AUTOMATIC_EVENT'


; When the widget is destroyed, the block is released, and we
; return here. Get the data that was collected by the widget
; and stored in the pointer location. Finally free the pointer.

formdata = *ptrToFormData

rep='CONTINUE'
IF N_Elements(formdata) EQ 0 THEN BEGIN
   Ptr_Free, ptrToFormData
   rep = 'CANCEL'
   return, rep
ENDIF
 
IF formdata.cancel EQ 1 THEN BEGIN
   Ptr_Free, ptrToFormData
   rep ='CANCEL'
   RETURN,rep
ENDIF

IF formdata.menu EQ 1 THEN BEGIN
   Ptr_Free, ptrToFormData
   rep ='MENU'
   RETURN,rep
ENDIF

IF rep EQ 'CONTINUE' THEN BEGIN
   
   Ptr_Free, ptrToFormData
   
   autoval.rootpath  =   formdata.a04.rootpath
   autoval.centroot  =   formdata.a04.centroot
   autoval.userroot  =   formdata.a04.userroot
   autoval.a03out    =   formdata.a03.outbut  
   autoval.a03app    =   formdata.a03.appbut  
   autoval.a03rep    =   formdata.a03.repbut  
   autoval.a03dsn    =   formdata.a03.filename
   autoval.a03def    =   formdata.a03.defname 
   autoval.a03mes    =   formdata.a03.message 
   autoval.texout    =   formdata.paper.outbut  
   autoval.texapp    =   formdata.paper.appbut  
   autoval.texrep    =   formdata.paper.repbut  
   autoval.texdsn    =   formdata.paper.filename
   autoval.texdef    =   formdata.paper.defname 
   autoval.texmes    =   formdata.paper.message 
   autoval.prefix    =   formdata.prefix    
   autoval.elem      =   formdata.elem    
   autoval.ion_low   =   formdata.ion_low 
   autoval.ion_high  =   formdata.ion_high
   autoval.ionval    =   formdata.ionval
   autoval.rrval     =   formdata.rrval 
   autoval.drval     =   formdata.drval 
   autoval.powval    =   formdata.powval
   files             =   formdata.process_files
   ion_low           =   formdata.ion_low 
   ion_high          =   formdata.ion_high
   ionval            =   formdata.ionval
   rrval             =   formdata.rrval 
   drval             =   formdata.drval 
   powval            =   formdata.powval
   
ENDIF


; Do the work - can only get to here if all is well
   
    date     = xxdate()
    name     = xxuser()
    rand     = strmid(strtrim(string(randomu(seed)), 2), 2, 4)
    
    tmpdir   = getenv('IDL_TMPDIR')
    a03_pass = tmpdir + 'a03.pass' + rand
    pap_pass = tmpdir + 'pap.txt' + rand
   
    if autoval.a03out EQ 1 then begin
       
       openw, lun_a03, autoval.a03dsn, /get_lun
       i4eiz0, autoval.elem, z
       printf, lun_a03, z, ion_low, ion_high,           $ 
                        '1    0    0', 'ADF03',         $
                        format='(3i5,14x,a11,35x,a5)'
    
    endif    

    if autoval.texout EQ 1 then openw, lun_pap, autoval.texdsn, /get_lun  


; During processing put up a busy widget

    widget_control, /hourglass
    busybase = widget_base(/column,xoffset=300,yoffset=200,     $
                             title = "ADAS407 INFORMATION")

    lab   = widget_label(busybase, value=' ')
    lab   = widget_label(busybase,  font=font_large,        $
              value='   ADAS407 Computation Underway - Please Wait   ')
    lab   = widget_label(busybase, value=' ')
    
    mrow  = widget_base(busybase,/row,/align_center)

    ; Size of the progress bar is hardwired
   
    barbase   = widget_base(busybase,/align_center)
    
    grap      = widget_draw(barbase, ysize=20, xsize=480) 
    
    rbase     = widget_base(busybase,/row,/align_center)
    lab       = widget_label(rbase, value='Ion stage : ', font=font_large)
    pupID     = widget_label(rbase, value=' ', font=font_large)
    mlab      = widget_label(busybase, value = ' ')
    
    
; Make labels resize dynamically and set it going.    
    
    dynlabel, busybase
    widget_control, busybase, /realize

    num = n_elements(files)-1

    next = 0
    step = 1000.0/num

    for i=0,num-1 do begin
    
      ON_IOERROR, jump_out      
   
      file_lower = autoval.rootpath + files[i]
      file_upper = autoval.rootpath + files[i+1]
                    
      spawn, fortdir+'/adas407_auto.out', unit=pipe, /noshell, PID=pid
            
      printf, pipe, date
      printf, pipe, file_lower
      printf, pipe, file_upper
      printf, pipe, a03_pass 
      printf, pipe, 'NULL' 
      printf, pipe, pap_pass 
      
      printf, pipe, ionval
      printf, pipe, rrval
      printf, pipe, drval
      printf, pipe, powval
      
      ; Read iz0, iz1 of ionised and ionising stage - used in run_adas407
      
      idum = -1L
      readf, pipe, idum
      readf, pipe, idum
      readf, pipe, idum
      readf, pipe, idum
      
      ; Assume one metastable for each - different in run_adas407
      
      printf, pipe, 1L    ; nmet
      printf, pipe, 1L    ; imetr(0..nmet-1)
      printf, pipe, 1L    ; npmet
      printf, pipe, 1L    ; ipmetr(0..npmet-1)
      
      p = float(i)/num*1000   
      q = p+step              
      
      for j=fix(p),fix(q) do begin
          x = (float(j)+1)/1000.0
          plots, [x, x],[0.0,1.0], /normal
      endfor
      widget_control, pupID, set_value = string(i+ion_low,format='(I2)')
          
      while find_process(pid) EQ 1 do temp = 0
      free_lun,pipe     
    
      if autoval.a03out EQ 1 then begin
      
         openr, lun_tmp, a03_pass, /get_lun
         
         str = ''
         while not EOF(lun_tmp) do begin
            readf, lun_tmp, str
            printf, lun_a03, str
         endwhile
         
         free_lun, lun_tmp
      
      endif
      
      if autoval.texout EQ 1 then begin
      
         openr, lun_tmp, pap_pass, /get_lun
         
         str = ''
         while not EOF(lun_tmp) do begin
            readf, lun_tmp, str
            printf, lun_pap, str
         endwhile
         
         free_lun, lun_tmp
      
      endif
    
    endfor
jump_out:


; Add comments, close up the files and remove the temporary ones

if autoval.a03out EQ 1 then begin
   
   printf, lun_a03, 'C--------------------------------------------------------------------'
   printf, lun_a03, 'C'
   printf, lun_a03, 'C  Data sets : ', autoval.rootpath 
   printf, lun_a03, 'C'
   printf, lun_a03, 'C  Code      : ADAS407'
   printf, lun_a03, 'C  Producer  : ',name(0)
   printf, lun_a03, 'C  Date      : ',date(0)
   printf, lun_a03, 'C'
   printf, lun_a03, 'C--------------------------------------------------------------------'
   
   free_lun, lun_a03

endif
if autoval.texout EQ 1 then free_lun, lun_pap

command = 'rm -f ' + a03_pass + ' ' + pap_pass
spawn, command

    

; We have finished writing the data so destroy the busy widget

  widget_control, busybase, /destroy


; Tell calling adas407 that we are finished - add AUTO to tell adas407
; how we got to the end.
  
   rep = 'CONTINUE - AUTO'
   RETURN, rep

END
