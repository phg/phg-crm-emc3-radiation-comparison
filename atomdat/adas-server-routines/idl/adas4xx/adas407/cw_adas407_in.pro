; Copyright (c) 1995, Strathclyde University.
; SCCS info: Module @(#)$Header: /home/adascvs/idl/adas4xx/adas407/cw_adas407_in.pro,v 1.2 2004/07/06 12:47:05 whitefor Exp $ Date $Date: 2004/07/06 12:47:05 $
;+
; PROJECT:
;       ADAS IBM MVS to UNIX conversion
;
; NAME:
;       CW_ADAS407_IN()
;
; PURPOSE:
;       Data file selection for adas 407.
;
; EXPLANATION:
;       This function creates a compound widget consisting of text typeins
;	for the beam species element symbol (default h) and the beam 
;	species ion charge (default 0).
;	The compound widget is completed with 'Cancel' and 'Done' buttons.
;
;	The value of this widget is contained in the VALUE structure.
;
; USE:
;       See routine adas407_in.pro for an example.
;
; INPUTS:
;       PARENT  - Long integer; the ID of the parent widget.
;
; OPTIONAL INPUTS:
;       None
;
; OUTPUTS:
;       This function returns the ID of the compound widget created.
;
; OPTIONAL OUTPUTS:
;       None
;
; KEYWORD PARAMETERS:
;       VALUE   - A structure which determines the initial settings of
;                 the entire compound widget. The structure must be:
;		 	{ 	EROOTPATH	:	'',		$
;				EFILE		:	'',		$
;				ECENTROOT	:	'',		$
;				EUSERROOT	:	'',		$
;				CROOTPATH	:	'',		$
;				CFILE		:	'',		$
;			  	CCENTROOT	:	'',		$
;			  	CUSERROOT	:	'',		$
;				FILE_CHANGED	:	0		}
;
;		  Where the elements of the structure are as follows:
;
;                 EROOTPATH   - Current data directory for ionising files
;
;		  EFILE	      - Current ionising file
;
;                 ECENTROOT   - Default central data store for ionising files
;
;                 EUSERROOT   - Default user data store for ionising files
;
;                 CROOTPATH   - Current data directory for ionised files
;
;		  CFILE	      - Current charge exchange data file
;
;                 CCENTROOT   - Default central data store for ionised files
;
;                 CUSERROOT   - Default user data store for ionised files
;
;		  FILE_CHANGED- 1 = Files have been changed in input widget
;				0 = Files the same
;
;                 Path names may be supplied with or without the trailing
;                 '/'.  The underlying routines add this character where
;                 required so that USERROOT will always end in '/' on
;                 output. This does not apply to DSNEX and DSNCX.
;
;       FONT_LARGE  - Supplies the large font to be used for the
;                     interface widgets.
;
;       FONT_SMALL  - Supplies the small font to be used for the
;                     interface widgets.
; CALLS:
;	CW_ADAS_INFILE	Data file selection widget.
;	I4Z0IE		Converts atomic number into element symbol.
;	I4EIZ0		Converts element symbol into atomic number.
;	FILE_ACC	Checks availability of files.
;	NUM_CHK		Checks validity of numerical data.
;
; SIDE EFFECTS:
;       IN407_GET_VAL() Widget management routine in this file.
;       IN407_EVENT()   Widget management routine in this file.
;       IN407_GET_Z()   Function to return iz, iz0 and iz1 from adf04 file.
;
; CATEGORY:
;       Compound Widget
;
; WRITTEN:
;       William Osborn, Tessella Support Services plc, 28th March 1996
;
; MODIFIED:
;       1.1     William Osborn 
;		First release. Created from skeleton of cw_adas310_in.
;       1.2     Martin O'Mullane 
;		If ionising file is H-like de-sensitise ionised file 
;               selection. Requires alterations to event handling 
;               and error checking.
;
; VERSION:
;       1.1     28-03-96
;       1.2     09-05-2003
;
;-
;-----------------------------------------------------------------------------

PRO in407_get_z, file, iz, iz0, iz1

iz1 = -1
iz0 = -1
iz  = -1

file_acc, file, exist, read, write, execute, filetype

if filetype EQ '-' then begin

   str = ''

   openr, lun, file, /get_lun
   readf, lun, str
   free_lun, lun


   pos1 = strpos(str,'+')
   reads, strmid(str, pos1+1), iz, iz0, iz1

endif

END
;-----------------------------------------------------------------------------


FUNCTION in407_get_val, id


                ;**** Return to caller on error ****

    ON_ERROR, 2

                 ;***************************************
                 ;****     Retrieve the state        ****
                 ;**** Get first_child widget id     ****
                 ;**** because state is stored there ****
                 ;***************************************

    first_child = widget_info(id, /child)
    widget_control, first_child, get_uvalue = state

		;**** Get the settings ****

    widget_control, state.expid, get_value=fileval
    widget_control, state.chxid, get_value=fileval2

    ps = {	EROOTPATH	:	fileval.rootpath,		$
		EFILE		:	fileval.file,			$
		ECENTROOT	:	fileval.centroot,		$
		EUSERROOT	:	fileval.userroot,		$
		CROOTPATH	:	fileval2.rootpath,		$
		CFILE		:	fileval2.file,			$
		CCENTROOT	:	fileval2.centroot,		$
		CUSERROOT	:	fileval2.userroot,		$
		FILE_CHANGED	:	state.file_changed		}

    widget_control, first_child, set_uvalue=state, /no_copy

    RETURN, ps

END

;-----------------------------------------------------------------------------

FUNCTION in407_event, event

    COMMON adas407_inblock, userdir, centdir

                ;**** Base ID of compound widget ****

    parent = event.handler

                ;**** Default output no event ****

    new_event = 0L

                ;**********************************************
                ;**** Retrieve the user value state        ****
                ;**** Get id of first_child widget because ****
                ;**** user value "state" is stored there   ****
                ;**********************************************

    first_child = widget_info(parent, /child)
    widget_control, first_child, get_uvalue=state, /no_copy
    topparent = widget_info(parent, /parent)

		;**** Clear any messages away ****

    widget_control, state.messid, set_value= '   '

                ;************************
                ;**** Process events ****
                ;************************

    CASE event.id OF

		;**** Event from ionising file selection ****
	
        state.expid: begin
	    
            widget_control, state.chxid, get_value=chxval
            if event.action eq 'newfile' then begin

		widget_control, state.expid, get_value=val
		file = strtrim(val.rootpath+val.file,2)
                in407_get_z, file, iz, iz0, iz1
                
                
                if iz1 EQ iz0 then begin
                   
                   ; If ionising file is H-like de-sensitise ionised 
                   ; file selection
                   
                   widget_control, state.chxid, sensitive=0
                
                endif else begin

                   widget_control, state.chxid, sensitive=1
		   
                   ;**** Check if file with charge +1 exists. If so, select as
		   ;**** ionised file.

		   widget_control, state.expid, get_value=expval
		   efile=strtrim(expval.file,2)
		   elen=strlen(efile)

		   ;** Find last #

		   i=0
		   epos=0
		   while(i ne -1) do begin
		       i=strpos(efile,'#',i)
		       if(i ne -1) then begin
			   epos=i
			   i=i+1
		       endif
		   endwhile

		   ;** If there was a #, look for the charge number after the
		   ;** element symbol and increase by one, creating a new filename
		   ;** Look for this filename, newfile, and if found change the
		   ;** state of the ionised ion file chooser

		   if(epos ne 0) then begin
		       newfile=strmid(efile,0,epos)
		       i=epos
		       while(num_chk(strmid(efile,i,1)) ne 0) do begin
			   newfile=newfile+strmid(efile,i,1)
			   i=i+1
		       endwhile
		       endstr=strmid(efile,i,elen-i)
		       enum=''
		       while(num_chk(strmid(efile,i,1),/integer) eq 0) do begin
			   enum=enum+strmid(efile,i,1)
			   i=i+1
		       endwhile
		       newfile=newfile+strtrim(string(fix(enum)+1),2)	$
			   +strmid(efile,i,elen-i)
		       file_acc, expval.rootpath+newfile, fileexist,	$
			   read, write, execute, filetype
		       if (fileexist eq 1) then begin
		    	   chxval.rootpath = expval.rootpath
		    	   chxval.file = newfile
		    	   widget_control, state.chxid, set_value=chxval
		       endif
		   endif

		   ;**** Check whether ionized file exists and if so,
		   ;**** sensitize buttons

                   if strcompress(chxval.file, /remove_all) ne '' then begin
		       filename = chxval.rootpath + chxval.file
                       file_acc, filename, fileexist, read, write, 	$
                                 execute, filetype
		       if filetype eq '-' then begin
                           widget_control, state.doneid, /sensitive
		       endif
		   endif
                   widget_control, state.browseid, /sensitive
                   
                endelse


            endif else begin
                widget_control, state.doneid, sensitive=0
                if strcompress(chxval.file, /remove_all) eq '' then begin
                    widget_control, state.browseid, sensitive=0
		endif
            endelse
        end

		;**** Event from ionised file selection ****

	state.chxid: begin
	    widget_control, state.expid, get_value=expval
            if event.action eq 'newfile' then begin
		if strcompress(expval.file, /remove_all) ne '' then begin
		    filename = expval.rootpath + expval.file
                    file_acc, filename, fileexist, read, write, 	$
                              execute, filetype
		    if filetype eq '-' then begin
                        widget_control, state.doneid, /sensitive
		    endif
		endif
                widget_control, state.browseid, /sensitive
            endif else begin
                widget_control, state.doneid, sensitive=0
		if strcompress(expval.file, /remove_all) eq '' then begin
                    widget_control, state.browseid, sensitive=0
		endif
            endelse
        end


                ;***********************
                ;**** Browse button ****
                ;***********************

        state.browseid: begin

                ;**** Invoke comments browsing ****

	    widget_control, state.expid, get_value=expval
	    if strcompress(expval.file, /remove_all) ne '' then begin
		filename = expval.rootpath + expval.file
                file_acc, filename, fileexist, read, write, execute, filetype
		if filetype eq '-' then begin
		    xxtext, filename, font=state.font_large
		endif
	    endif
            
            filename = expval.rootpath + expval.file
            in407_get_z, filename, iz, iz0, iz1
	    if iz1 NE iz0 then begin
               widget_control, state.chxid, get_value=chxval
	       if strcompress(chxval.file, /remove_all) ne '' then begin
		   filename = chxval.rootpath + chxval.file
                   file_acc, filename, fileexist, read, write, execute, filetype
		   if filetype eq '-' then begin
		       xxtext, filename, font=state.font_large
		   endif
	       endif
            endif
        
        end

                ;***********************
                ;**** Cancel button ****
                ;***********************

        state.cancelid: begin
            new_event = {ID:parent, TOP:event.top,          		$
                         HANDLER:0L, ACTION:'Cancel'}
        end

		;*********************
		;**** Done Button ****
		;*********************

	state.doneid: begin
	    
            error = 0
		
            ;**** Find the element and charge of each file from the 
	    ;**** file data and check that they are compatible

	    widget_control, state.expid, get_value=expval
	    widget_control, state.chxid, get_value=chxval
	    efile = expval.file
	    cfile = chxval.file
            
            in407_get_z, expval.rootpath+expval.file, iz, iz0, iz1

            if iz1 EQ iz0 then begin
            
               ; only check ionising file in this case
            
	       openr,eunit,expval.rootpath+expval.file,/get_lun
	       ename=''
	       readf,eunit,format='(a5)',ename
	       free_lun, eunit

	       enam = strlowcase(strcompress(strmid(ename,0,2),/remove_all))
	       enum = strcompress(strmid(ename,2,3),/remove_all)
               
	       if  num_chk(enum,/integer) ne 0 then begin
		   error=1
		   message='Charge is not an integer. Is this an adf04 file?'
	       endif
            
            endif else begin

               ; Check both
 
	       openr,eunit,expval.rootpath+expval.file,/get_lun
	       openr,cunit,chxval.rootpath+chxval.file,/get_lun

	       ename=''
	       cname=''
	       readf,eunit,format='(a5)',ename
	       readf,cunit,format='(a5)',cname
	       free_lun, eunit
               free_lun, cunit

	       enam = strlowcase(strcompress(strmid(ename,0,2),/remove_all))
	       cnam = strlowcase(strcompress(strmid(cname,0,2),/remove_all))
	       enum = strcompress(strmid(ename,2,3),/remove_all)
 	       cnum = strcompress(strmid(cname,2,3),/remove_all)

	       if((num_chk(enum,/integer) ne 0) or (num_chk(cnum,/integer) ne 0))$
			   then begin
		   error=1
		   message='Charge is not an integer. Are they both adf04 format files?'
	       endif else begin
		   if(fix(enum) ne fix(cnum)-1) then begin
		       error=1
		       message='Ionised ion charge must be one greater than ionising ion charge'
		   endif
   	           if(enam ne cnam) then begin
  		       error=1
   		       message='Must be the same element in each file'
		   endif
	       endelse

		   ;*************************************************
		   ;**** If rest of filename is different, query ****
		   ;*************************************************

	       ;** Find last #

	       i=0
	       epos=0
	       while(i ne -1) do begin
		   i=strpos(efile,'#',i)
		   if(i ne -1) then begin
		       epos=i
		       i=i+1
		   endif
	       endwhile
	       ehead=expval.rootpath+strmid(efile,0,epos)
	       i=0
	       cpos=0
	       while(i ne -1) do begin
		   i=strpos(cfile,'#',i)
		   if(i ne -1) then begin
		       cpos=i
		       i=i+1
		   endif
	       endwhile
	       chead=chxval.rootpath+strmid(cfile,0,cpos)
	       if(ehead ne chead) then begin
		   action=popup(message='Proceed with different file types?',$
		             buttons=['Confirm','Cancel'], font=state.font_large)
		   if action eq 'Cancel' then begin
		       error = 1
		       message = 'Charges are compatible but file types are different'
		   endif
	       endif
               
            endelse

		;************************************
                ;**** return value or flag error ****
		;************************************

            if error eq 0 then begin
                new_event = {ID:parent, TOP:event.top,                  $
                             HANDLER:0L, ACTION:'Done'}
            endif else begin
		widget_control,state.messid,set_value=message
                new_event = 0L
            endelse
	end

        ELSE: new_event = 0L

    ENDCASE


                ;*********************************************
                ;*** make "state" available to other files ***
                ;*********************************************

    widget_control, first_child, set_uvalue=state, /no_copy

    RETURN, new_event

END

;-----------------------------------------------------------------------------

FUNCTION cw_adas407_in, parent, VALUE=value, FONT_LARGE=font_large, 	$
                        FONT_SMALL=font_small

    COMMON adas407_inblock, userdir, centdir

    ON_ERROR, 2                                 ;return to caller on error

                ;**** Set defaults for keywords ****

    IF NOT (KEYWORD_SET(value)) THEN begin
        inset = { EROOTPATH	:	userroot+'/adf04/',		$
		  EFILE		:	'',				$
		  ECENTROOT	:	centroot+'/adf04/',		$
		  EUSERROOT	:	userroot+'/adf04/',		$	
		  CROOTPATH	:	userroot+'/adf04/',		$
		  CFILE		:	'',				$
	   	  CCENTROOT	:	centroot+'/adf04/',		$
		  CUSERROOT	:	userroot+'/adf04/',		$
		  FILE_CHANGED  :	1				}
    ENDIF ELSE BEGIN
	inset = {	EROOTPATH       :       value.erootpath,	$
                        EFILE           :       value.efile,            $
			ECENTROOT       :       value.ecentroot,	$
			EUSERROOT       :       value.euserroot,	$
                        CROOTPATH       :       value.crootpath,	$
                        CFILE           :       value.cfile,		$
                        CCENTROOT       :       value.ccentroot,	$
                        CUSERROOT       :       value.cuserroot,	$
			FILE_CHANGED	:	value.file_changed	}

        if strtrim(inset.erootpath) eq '' then begin
            inset.erootpath = './'
        endif else if                                                   $
        strmid(inset.erootpath, strlen(inset.erootpath)-1,1) ne '/' then begin
            inset.erootpath = inset.erootpath+'/'
        endif
        if strtrim(inset.crootpath) eq '' then begin
            inset.crootpath = './'
        endif else if                                                   $
        strmid(inset.crootpath, strlen(inset.crootpath)-1,1) ne '/' then begin
            inset.crootpath = inset.crootpath+'/'
        endif
    ENDELSE
    IF NOT (KEYWORD_SET(font_large)) THEN font_large = ''
    IF NOT (KEYWORD_SET(font_small)) THEN font_small = ''

                ;*********************************
                ;**** Create the Input widget ****
                ;*********************************

                ;**** create base widget ****

    topbase = widget_base(parent, EVENT_FUNC = "in407_event",		$
                          FUNC_GET_VALUE = "in407_get_val",		$
		          /column)

                ;******************************************************
                ;**** Create a dummy widget just to hold value of *****
                ;**** "state" variable so as not to get confused  *****
                ;**** with any other values. Adopt IDL practice   *****
                ;**** of using first child widget                 *****
                ;******************************************************

    first_child = widget_base(topbase)

    cwid = widget_base(first_child, /column)

                ;*****************************************
                ;**** Ionising file selection widget ****
                ;*****************************************

    expbase = widget_base(cwid, /column, /frame)
    expval = {	ROOTPATH	:	inset.erootpath,		$
		FILE		:	inset.efile,			$
		CENTROOT	:	inset.ecentroot,		$
		USERROOT	:	inset.euserroot			}
    exptitle = widget_label(expbase, font=font_large,			$
	                    value='Ionising Ion File Details:-')
    expid = cw_adas4xx_infile(expbase, value=expval, font=font_small)

                ;***********************************************
                ;**** Ionised file selection widget ****
                ;***********************************************

    chxbase = widget_base(cwid, /column, /frame)
    chxval = {	ROOTPATH	:	inset.crootpath,		$
		FILE		:	inset.cfile,			$
		CENTROOT	:	inset.ccentroot,		$
		USERROOT	:	inset.cuserroot			}
    chxtitle = widget_label(chxbase, font=font_large,			$
	                    value='Ionised Ion File Details:-')
    chxid = cw_adas4xx_infile(chxbase, value=chxval, font=font_small)

		;**** Error message ****

    messid = widget_label(cwid, font=font_large,			$
    value='Edit the processing options data and press Done to proceed')

                ;*****************
                ;**** Buttons ****
                ;*****************

    base = widget_base(cwid, /row)

                ;**** Browse Dataset button ****

    browseid = widget_button(base, value='Browse Comments',		$
                             font=font_large)

                ;**** Cancel Button ****

    cancelid = widget_button(base, value='Cancel', font=font_large)

                ;**** Done Button ****

    doneid = widget_button(base, value='Done', font=font_large)

                ;*************************************************
                ;**** Check filenames and desensitise buttons ****
                ;**** if it is a directory or it is a file    ****
                ;**** without read access.                    ****
                ;*************************************************

    browseallow = 0
    filename = inset.erootpath + inset.efile
    file_acc, filename, fileexist, read, write, execute, filetype
    if filetype ne '-' then begin
        widget_control, browseid, sensitive=0
        widget_control, doneid, sensitive=0
    endif else begin
        if read eq 0 then begin
            widget_control, browseid, sensitive=0
            widget_control, doneid, sensitive=0
        endif else begin
	    browseallow = 1
	endelse
    endelse
    filename = inset.crootpath + inset.cfile
    file_acc, filename, fileexist, read, write, execute, filetype
    if filetype ne '-' then begin
        if browseallow eq 0 then widget_control, browseid, sensitive=0
        widget_control, doneid, sensitive=0
    endif else begin
        if read eq 0 then begin
            if browseallow eq 0 then widget_control, browseid, sensitive=0
            widget_control, doneid, sensitive=0
        endif
    endelse

    
    ; If ionising file is H-like de-sensitise ionised file selection
                
    filename = inset.erootpath + inset.efile
    in407_get_z, filename, iz, iz0, iz1

    if iz1 EQ iz0 then widget_control, chxid, sensitive=0 $
                  else widget_control, chxid, sensitive=1


                ;*************************************************
                ;**** create a state structure for the pop-up ****
                ;****                window.                  ****
                ;*************************************************

    new_state = {	font_large	:	font_large,		$
			font_small	:	font_small,		$
			inval		:	inset,			$
			doneid		:	doneid,			$
			browseid	:	browseid,		$
			expid		:	expid,			$
			chxid		:	chxid,			$
			messid		:	messid,			$
			cancelid	:	cancelid,		$
			file_changed	:	inset.file_changed	}

               ;**** Save initial state structure ****

    widget_control, first_child, set_uvalue=new_state, /no_copy
    
    RETURN, topbase

END
