; Copyright (c) 1995, Strathclyde University.
;  SCCS info: Module @(#)$Header: /home/adascvs/idl/adas4xx/adas407/d7outh.pro,v 1.2 2004/07/06 13:23:15 whitefor Exp $ Date $Date: 2004/07/06 13:23:15 $
;+
; PROJECT:
;       ADAS IBM MVS to UNIX conversion
;
; NAME:
;	D7OUTG
;
; PURPOSE:
;	Communication with ADAS407 FORTRAN via UNIX pipe and
;	graphics output.
;
; EXPLANATION:
;	The routine begins by reading information from the ADAS407
;	FORTRAN process via a UNIX pipe.  Then the IDL graphical
;	output routine for ADAS407 is invoked.  Communications are to
;	the FORTRAN subroutine D7OUTG.
;
; USE:
;	The use of this routine is specific to ADAS407 see adas407.pro.
;
; INPUTS:
;	DSFULL   - Data file name
;
;	PIPE	 - The IDL unit number of the bi-directional pipe to the
;		   ADAS407 FORTRAN process.
;
;       UTITLE   - Optional comment by user
;
;	HRDOUT	 - Integer; 0 if no hardcopy required, 1 if hardcopy.
;
;	HARDNAME - String; file name for hardcopy output.
;
;	DEVICE	 - String; IDL name for hardcopy output device.
;
;	HEADER	 - ADAS version header information for inclusion in the
;		   graphical output.
;
;       BITFILE - String; the path to the directory containing bitmaps
;                 for the 'escape to series menu' button.
;
; OPTIONAL INPUTS:
;	None.
;
; OUTPUTS:
;       GOMENU  - Int; flag - set to 1 if user has selected 'escape direct
;                 to series menu , otherwise 0
;
; OPTIONAL OUTPUTS:
;	None.
;
; KEYWORD PARAMETERS:
;	FONT	- String; Name of the font to be used for the graphical
;		  output widget.
;
; CALLS:
;	ADAS407_PLOTH	ADAS407 graphical output.
;
; SIDE EFFECTS:
;	This routine reads information from FORTRAN via a UNIX pipe.
;
; CATEGORY:
;	Adas system.
;
; WRITTEN:
;       William Osborn, Tessella Support Services plc, 18-Apr-1996
;
; MODIFIED:
;	1.1	William Osborn
;		First Version
;	1.2	William Osborn
;		Increased size of titlx
; VERSION:
;	1.1	18-04-96
;	1.2	23-04-96
;-
;-----------------------------------------------------------------------------

PRO d7outh, dsfull, pipe, utitle,					$
	    hrdout, hardname, device, header, bitfile, gomenu, FONT=font

                ;**** Set defaults for keywords ****

  IF NOT (KEYWORD_SET(font)) THEN font = ''


		;**** Declare variables for input ****

    dummy = 0.0
    sdum = " "
    idum = 0

		;********************************
		;**** Read data from fortran ****
		;********************************

    readf, pipe, format = '(a40)' , sdum
    title = sdum
    readf, pipe, format = '(a120)' , sdum
    titlx = sdum
    readf, pipe, format = '(a8)' , sdum
    date = sdum

    readf, pipe, idum
    ntemp = idum

    temp=dblarr(ntemp)
    for i=0,ntemp-1 do begin
    	readf, pipe, dummy
	temp(i)=dummy
    endfor

    stota=dblarr(ntemp)
    for i=0,ntemp-1 do begin
    	readf, pipe, dummy
	stota(i)=dummy
    endfor
    
    readf, pipe, idum
    npspl = idum

    tosa=dblarr(npspl)
    for i=0,npspl-1 do begin
    	readf, pipe, dummy
	tosa(i)=dummy
    endfor
    
    sowesa=dblarr(npspl)
    for i=0,npspl-1 do begin
    	readf, pipe, dummy
	sowesa(i)=dummy
    endfor
    
    sowisa=dblarr(npspl)
    for i=0,npspl-1 do begin
    	readf, pipe, dummy
	sowisa(i)=dummy
    endfor
    
    sowssa=dblarr(npspl)
    for i=0,npspl-1 do begin
    	readf, pipe, dummy
	sowssa(i)=dummy
    endfor
    
    readf, pipe, idum
    ldef = idum
    if ldef eq 1 then begin
	readf, pipe, dummy
	xmin=dummy
        readf, pipe, dummy
	xmax = dummy
        readf, pipe, dummy
	ymin = dummy
        readf, pipe, dummy
	ymax = dummy
    endif 

		;***********************
		;**** Plot the data ****
		;***********************

    adas407_ploth, dsfull, title , titlx, utitle, date,			$
		  ntemp, temp, stota, npspl, tosa, sowesa, sowisa,	$
		  sowssa,						$
                  ldef, xmin, xmax, ymin, ymax, 			$
		  hrdout, hardname, device, header, bitfile, gomenu,	$
                  FONT=font

END
