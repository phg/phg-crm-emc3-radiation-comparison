; Copyright (c) 1996, Strathclyde University.
; SCCS info: Module @(#)$Header: /home/adascvs/idl/adas4xx/adas407/cw_adas407_out.pro,v 1.3 2004/07/06 12:47:15 whitefor Exp $ Date $Date: 2004/07/06 12:47:15 $
;+
; PROJECT:
;       ADAS IBM MVS to UNIX conversion
;
; NAME:
;       CW_ADAS407_OUT()
;
; PURPOSE:
;       Produces a widget for ADAS407 output options.
;
; EXPLANATION:
;       This function declares a compound widget consisting of:
;        - a button for selecting graphical or text output options
;	 - a 'select axis limits' button and max-min typeins for
;	   the total and sepcific line power graphs
;	 - a button to say whether a specific line power graph is required
;        - for each graph a set of typeins for max and min axis values
;        - the associated hard copy widget comprising a filename and
;          list of output devices
;        - output file widgets cw_adas_outfile for text and passing
;          file output
;        - a text typein for the user to assign a title to the graph
;        - buttons for 'Escape to series menu', 'Cancel', 'Graph and proceed'
;	   'Compute and back to input'
;
;	This widget only generates events for the last four buttons.
;
; USE:
;       This routine is specific to adas407.
;
; INPUTS:
;       PARENT  - Long integer; ID of parent widget.
;
;	INVAL	- A structure containing the settings of the input options
;		  compound widget.
;
;	PROCVAL	- A structure containing the settings of the processing
;		  options compound widget.
;
;       BITFILE - String; the path to the directory containing bitmaps
;                 for the 'escape to series menu' button.
;
; OPTIONAL INPUTS:
;       None.
;
; OUTPUTS:
;       The return value of this function is the ID of the declared widget.
;
; OPTIONAL OUTPUTS:
;       None.
;
; KEYWORD PARAMETERS:
;       DEVLIST - A string array;  Each element of the array is the
;                 name of a hardcopy graphics output device.  This
;                 list must mirror the DEVCODE list.  DEVLIST is the
;                 list which the user will see.
;                 e.g ['Post-Script','HP-PCL','HP-GL']
;
;       VALUE   - A structure which determines the initial settings of
;                 the output options widget.  The structure has the
;                 following form:
;		{	ITOGGLE		:	0,			$
;			LGRAPH		:	1,			$
;                       XMIN1           :       '',                     $
;                       XMAX1           :       '',                     $
;                       YMIN1           :       '',                     $
;                       YMAX1           :       '',                     $
;                       XMIN	        :       '',                     $
;                       XMAX            :       '',                     $
;                       YMIN            :       '',                     $
;                       YMAX            :       '',                     $
;                       LDEF1           :       0,                      $
;                       LDEF            :       0,                      $
;	      		LPLT            :       0, 			$
;                       GTIT1           :       '',                     $
;                       HRDOUT          :       0,                      $
;                       HARDNAME        :       '',                     $
;                       GRPDEF          :       '',                     $
;                       GRPFMESS        :       '',                     $
;                       DEVSEL          :       -1,                     $
;                       TEXOUT          :       0,                      $
;                       TEXAPP          :       -1,                     $
;                       TEXREP          :       0,                      $
;                       TEXDSN          :       '',                     $
;                       TEXDEF          :       'paper.txt',            $
;                       TEXMES          :       '',                     $
;                       MAIOUT          :       0,                      $
;                       MAIAPP          :       -1,                     $
;                       MAIREP          :       0,                      $
;                       MAIDSN          :       '',                     $
;                       MAIDEF          :       maidefval,              $
;                       MAIMES          :       '',                     $
;                       ATOOUT          :       0,                      $
;                       ATOAPP          :       -1,                     $
;                       ATOREP          :       0,                      $
;                       ATODSN          :       '',                     $
;                       ATODEF          :       atodefval,              $
;                       ATOMES          :       '',                     $
;			VIEWED		:	0			}

;
;  Where the elements represent:
;
;			ITOGGLE	- Int; Flag showing whether graphics options
;                                 should be displayed (1) or text (0)
;  For Graphics output:
;			LGRAPH  - Int; Graph power plot 1=yes,0=no. Currently
;				  automatically set to one and never changed
;			XMIN1	- Float; min of x-axis for power graph
;			XMAX1	- Float; max of x-axis for power graph
;			YMIN1	- Float; min of y-axis for power graph
;			YMAX1	- Float; max of y-axis for power graph
;			LDEF1	- Int; flag whether user has selected
;                                 explicit axes for power graph
;			XMIN	- Float; min of x-axis for spec. power graph
;			XMAX	- Float; max of x-axis for spec. power graph
;			YMIN	- Float; min of y-axis for spec. power graph
;			YMAX	- Float; max of y-axis for spec. power graph
;			LDEF	- Int; flag whether user has selected
;                                 explicit axes for spec. power graph
;			LPLT	- Int; Flag whether spec. power graph wanted
;			GTIT1	- String; Graph title
;			HRDOUT	- Int; Hard copy active: 1 on, 0 off
;			HARDNAME- String; Hard copy output file name
;			GRPDEF	- String; Default output file name
;			GRPFMESS- String; File name error message
;			DEVSEL	- Int; index of selected device in DEVLIST
;
;  For Text output:
;			TEXOUT	- Int; Activation button 1 on, 0 off
;			TEXAPP	- Int; Append button 1 on, 0 off,
;                                 -1 no button
;			TEXREP  - Int; Replace button 1 on, 0 off,
;                                 -1 no button
;			TEXDSN	- String; Output file name
;			TEXDEF	- String; Default file name
;			TEXMES	- String; file name error message
;  For MAINBN output:
;			MAIOUT  - Int; Activation button 1 on, 0 off
;			MAIAPP  - Int; Append button 1 on, 0 off,
;                                 -1 no button
;			MAIREP  - Int; Replace button 1 on, 0 off,
;                                 -1 no button
;			MAIDSN  - String; Output file name
;			MAIDEF  - String; Default file name
;			MAIMES  - String; file name error message
;  For ATOMPARS output:
;			ATOOUT  - Int; Activation button 1 on, 0 off
;			ATOAPP  - Int; Append button 1 on, 0 off,
;                                 -1 no button
;			ATOREP  - Int; Replace button 1 on, 0 off,
;                                 -1 no button
;			ATODSN  - String; Output file name
;			ATODEF  - String; Default file name
;			ATOMES  - String; file name error message
;
;			VIEWED  - Int; 0 if the graphs have been viewed
;				       1 if they haven't
;
;  			End of VALUE elements description.
;
;       UVALUE  - A user value for this widget.
;
;       FONT_LARGE- Supplies the large font to be used.
;
;       FONT_SMALL- Supplies the small font to be used.
;
; CALLS:
;       CW_ADAS_DSBR    Input dataset name and comments browse button.
;       CW_ADAS_OUTFILE Output file name entry widget.
;       See side effects for other related widget management routines.
;
; SIDE EFFECTS:
;
;       Two other routines are included in this file
;       OUT407_GET_VAL()
;       OUT407_EVENT()
;
; CATEGORY:
;       Compound Widget
;
; WRITTEN:
;       William Osborn, Tessella Support Services plc, 10th April 1996
;
; MODIFIED:
;       1.1     William Osborn
;               First version - modified cw_adas203_out.pro
;	1.2	William Osborn
;		Removed duplicate textbase definition from new_state
;	1.3	William Osborn
;		Removed xsize=n keyword from widget_label command.
;		The dynlabel procedure does not override these
;		assignments and they were taken as being values in pixels
;		on some machines.
;
; VERSION:
;       1.1     10-04-96
;       1.2     16-04-96
;	1.3	01-08-96
;
;-
;-----------------------------------------------------------------------------

FUNCTION out407_get_val, id

                ;**** Return to caller on error ****

    ON_ERROR, 2

                ;**** Retrieve the state ****

    first_child=widget_info(id, /child)
    widget_control, first_child, get_uvalue=state, /no_copy

		;**** Get toggle setting ****

    widget_control, state.flipid, get_value=itoggle

		;**** Get graphical output settings ****

    widget_control, state.titleid, get_value=gtit1
    widget_control, state.tyminbut, get_value=ymin1
    widget_control, state.tymaxbut, get_value=ymax1
    widget_control, state.txminbut, get_value=xmin1
    widget_control, state.txmaxbut, get_value=xmax1
    if state.lplt ne 0 then begin
    	widget_control, state.syminbut, get_value=ymin
    	widget_control, state.symaxbut, get_value=ymax
    	widget_control, state.sxminbut, get_value=xmin
    	widget_control, state.sxmaxbut, get_value=xmax
    endif else begin
	xmin =''
	ymin =''
	xmax =''
	ymax =''
    endelse

		;**** Get hard copy settings ****

    widget_control, state.hardid, get_value=hardval
    widget_control, state.devid, get_value=devsel

                ;**** Get text output settings ****

    widget_control, state.textid, get_value=papos

                ;**** Get MAINBN passing file settings ****

    widget_control, state.mainid, get_value=mainos

                ;**** Get ATOMPARS passing file settings ****

    widget_control, state.atomid, get_value=atomos

    os = { 		                                           	$
		ITOGGLE		:	itoggle,			$
		LGRAPH		:	1,				$
                XMIN1           :       xmin1,                     	$
                XMAX1           :       xmax1,                     	$
                YMIN1           :       ymin1,                     	$
                YMAX1           :       ymax1,                      	$
                XMIN	        :       xmin,                     	$
                XMAX            :       xmax,                     	$
                YMIN            :       ymin,                     	$
                YMAX            :       ymax,                     	$
                LDEF1           :       state.ldef1,                    $
                LDEF            :       state.ldef,                     $
    		LPLT            :       state.lplt, 			$
                GTIT1           :       gtit1,                     	$
		HRDOUT		:	hardval.outbut,			$
		HARDNAME	:	hardval.filename,		$
		GRPDEF		:	hardval.defname,		$
		GRPFMESS	:	hardval.message,		$
                DEVSEL          :       devsel,                     	$
		TEXOUT		:	papos.outbut, 			$
		TEXAPP		:	papos.appbut,  			$
		TEXREP		:	papos.repbut,			$
		TEXDSN		:	papos.filename,			$
		TEXDEF		:	papos.defname,			$
		TEXMES		:	papos.message,			$
		MAIOUT		:	mainos.outbut, 			$
		MAIAPP		:	mainos.appbut,  		$
		MAIREP		:	mainos.repbut,			$
		MAIDSN		:	mainos.filename,		$
		MAIDEF		:	mainos.defname,			$
		MAIMES		:	mainos.message,			$
		ATOOUT		:	atomos.outbut, 			$
		ATOAPP		:	atomos.appbut,  		$
		ATOREP		:	atomos.repbut,			$
		ATODSN		:	atomos.filename,		$
		ATODEF		:	atomos.defname,			$
		ATOMES		:	atomos.message,			$
		VIEWED		:	state.viewed			}

                ;**** Return the state ****

    widget_control, first_child, set_uvalue=state, /no_copy
    
    RETURN, os
 
END

;-----------------------------------------------------------------------------

PRO graph_err, procid, error, warnmess

                ;**** Get a copy of the widget value ****

            widget_control, procid, get_value=os

                ;**** Check for widget error messages ****

            if strtrim(os.grpfmess) ne '' then error=1
            if error eq 1 then warnmess = '**** Error in output settings ****'

		;*********************************************
                ;**** Check a device has been selected if ****
                ;**** hard copy requested                 ****
		;*********************************************

            if error ne 1 and os.hrdout eq 1 and os.devsel eq -1 then begin
                warnmess = '**** Error: You must select an output device ****'
              error = 1
            endif

                ;**** Check for errors in scalings if relevant ****

            if os.lgraph eq 1 and os.ldef1 eq 1 and error ne 1 then begin
                serror = 0
                serror = serror + num_chk(os.xmin1, sign=1)
                serror = serror + num_chk(os.xmax1, sign=1)
                serror = serror + num_chk(os.ymin1, sign=1)
                serror = serror + num_chk(os.ymax1, sign=1)
                if serror eq 0 then begin
                    if (float(os.xmin1) ge float(os.xmax1)) or          $
                    (float(os.ymin1) ge float(os.ymax1))                $
                    then serror = 1
                endif
                if serror gt 0 then begin
                    error = 1
                    warnmess = '**** Error: Total power graph axis limits'+$
			       ' are invalid ****'
                endif
            endif else if os.lplt eq 1 and os.ldef eq 1            $
            and error ne 1 then begin
                serror = 0
                serror = serror + num_chk(os.xmin, sign=1)
                serror = serror + num_chk(os.xmax, sign=1)
                serror = serror + num_chk(os.ymin, sign=1)
                serror = serror + num_chk(os.ymax, sign=1)
                if serror eq 0 then begin
                    if (float(os.xmin) ge float(os.xmax)) or          $
                    (float(os.ymin) ge float(os.ymax))                $
                    then serror = 1
                endif
                if serror gt 0 then begin
                    error = 1
                    warnmess = '**** Error: '+       			$
                               'Specific line power graph axis limits'+ $
                               ' are invalid ****'
                endif
            endif

END

;-----------------------------------------------------------------------------

PRO output_err, state, error, messval

    COMMON adas407_out_blk, lsetp

    widget_control, state.mainid, get_value=mainval
    widget_control, state.atomid, get_value=atomval
    widget_control, state.textid, get_value=textval
    mess1 = strcompress(mainval.message, /remove_all)
    mess2 = strcompress(atomval.message, /remove_all)
    mess3 = strcompress(textval.message, /remove_all)
    if (mess1 ne '') or (mess2 ne '') or (mess3 ne '') then begin
	error = 1
	messval = '**** Error in output file settings ****'
	widget_control, state.flipid, set_value=1
        widget_control, state.graphbase, map=0
        widget_control, state.textbase, map=1
    endif
    if atomval.outbut eq 1 and lsetp eq 0 then begin
	error=1
	messval = '**** Error: atompars cannot be written. Power from metastable 1 has not been analysed ****'
	widget_control, state.flipid, set_value=1
        widget_control, state.graphbase, map=0
        widget_control, state.textbase, map=1
    endif

END

;-----------------------------------------------------------------------------

FUNCTION out407_event, event

    COMMON adas407_out_blk, lsetp

                ;**** Base ID of compound widget ****

    parent = event.handler

                ;**** Retrieve the state ****

    first_child = widget_info(parent,/child)

    widget_control, first_child, get_uvalue=state, /no_copy

                ;*********************************
                ;**** Clear previous messages ****
                ;*********************************

    widget_control, state.messid, set_value=' '

                ;************************
                ;**** Process events ****
                ;************************

    CASE event.id OF

                ;*****************************************************
                ;**** Change between graphics/text output display ****
                ;*****************************************************

        state.flipid: begin
            widget_control, state.flipid, get_value=itoggle
            widget_control, state.graphbase, map=(1-itoggle(0))
            widget_control, state.textbase, map=itoggle(0)
        end


		;************************************************
		;**** Explicit scaling for total power graph ****
		;************************************************

	state.totpdef: begin
	    if state.ldef1 eq 1 then state.ldef1=0 else state.ldef1=1
	    widget_control, state.totpbase3b, sensitive=state.ldef1
	end

		;******************************************
		;**** Specific line power graph button ****
		;******************************************

	state.lpltid: begin
	    if state.lplt eq 1 then state.lplt=0 else state.lplt=1
	    widget_control, state.slpbase3, sensitive=state.lplt
	end

		;****************************************************
		;**** Explicit scaling for spec. line power plot ****
		;****************************************************

	state.slpdef: begin
	    if state.ldef eq 1 then state.ldef=0 else state.ldef=1
	    widget_control, state.slpbase3b, sensitive=state.ldef
	end

		;**********************************************
		;**** Min/max buttons for total power plot ****
		;**********************************************

	state.txminbut: widget_control, state.txmaxbut, /input_focus
	state.txmaxbut: widget_control, state.tyminbut, /input_focus
	state.tyminbut: widget_control, state.tymaxbut, /input_focus
	state.tymaxbut: widget_control, state.txminbut, /input_focus

		;***************************************************
		;**** Min/max buttons for spec. line power plot ****
		;***************************************************

	state.sxminbut: widget_control, state.sxmaxbut, /input_focus
	state.sxmaxbut: widget_control, state.syminbut, /input_focus
	state.syminbut: widget_control, state.symaxbut, /input_focus
	state.symaxbut: widget_control, state.sxminbut, /input_focus


		;***********************************
		;**** Back to Processing button ****
		;***********************************

	state.cancelid: begin
            new_event = {ID:parent, TOP:event.top, HANDLER:0L,          $
                         ACTION:'Proc'}
	end
		;*********************
		;**** Menu button ****
		;*********************

        state.outid: begin
            new_event = {ID:parent, TOP:event.top, HANDLER:0L,          $
                         ACTION:'Menu'}
        end

		;****************************************
		;**** Graph and return to processing ****
		;****************************************

	state.procid: begin

            error = 0

                ;****************************************************
		;**** Check for errors in the graphics settings  ****
                ;**** Return the state before checking can start ****
                ;**** with the get_value keyword.                ****
                ;****************************************************

            widget_control, first_child, set_uvalue=state, /no_copy

	    graph_err, event.handler, error, messval

                ;**** Retrieve the state   ****

            widget_control, first_child, get_uvalue=state, /no_copy

            if error eq 1 then begin
                widget_control, state.messid, set_value=messval
                new_event = 0L
            endif else begin
                new_event = {ID:parent, TOP:event.top, HANDLER:0L,  	$
                             ACTION:'Graph'}
	    endelse
	end

		;*************************************
		;**** Compute and return to input ****
		;*************************************

	state.inputid: begin

		;*************************************************
		;**** Check for errors in the output settings ****
		;*************************************************

            error = 0
	    output_err, state, error, messval

            if error eq 1 then begin
                widget_control, state.messid, set_value=messval
                new_event = 0L
            endif else begin
                new_event = {ID:parent, TOP:event.top, HANDLER:0L,  	$
                             ACTION:'Input'}
	    endelse
	end

        ELSE: begin
            new_event = 0L
        end
    ENDCASE

                ;**** Return the state   ****

    widget_control, first_child, set_uvalue=state, /no_copy

    RETURN, new_event

END

;-----------------------------------------------------------------------------

FUNCTION cw_adas407_out, topparent, inval, procval, bitfile, lsetp,	$
                         VALUE=value, UVALUE=uvalue, FONT_LARGE=font,   $
			 FONT_SMALL=font_small, DEVLIST=devlist

    COMMON adas407_out_blk, lsetpcom

    lsetpcom = lsetp

    IF (N_PARAMS() EQ 0) THEN MESSAGE, 'Must specify parent for cw_adas407_out'
    ON_ERROR, 2                                 ;return to caller

                ;**** Set defaults for keywords ****

    IF NOT (KEYWORD_SET(devlist)) THEN devlist = ''
    IF NOT (KEYWORD_SET(uvalue)) THEN uvalue = 0
    IF NOT (KEYWORD_SET(font)) THEN font = ''
    IF NOT (KEYWORD_SET(font_small)) THEN font_small = ''

                ;**** Add flexible font-sizing ****

    machine = GETENV('TARGET_MACHINE')
    if machine eq 'HPUX' then begin
        large_font = font_small
    endif else begin
        large_font = font
    endelse

                ;**********************************************
                ;**** Create the 407 Output options widget ****
                ;**********************************************

                ;**** create base widget ****

    parent = widget_base( topparent, UVALUE=uvalue,                     $
                        EVENT_FUNC="out407_event",                    	$
                        FUNC_GET_VALUE="out407_get_val")

    first_child = widget_base(parent)

    cwid = widget_base(first_child, /column)

                ;**** Add dataset names and browse buttons ****

    dsnic1 = inval.erootpath + inval.efile
    dsnic2 = inval.crootpath + inval.cfile
    rc = cw_adas_dsbr(cwid, dsnic1, font=font_small,                    $
                      filelabel='Input files: Parent ion:  ', /row)
    rc = cw_adas_dsbr(cwid, dsnic2, font=font_small,                    $
                      filelabel='             Initial ion: ', /row)

                ;****************************************************
                ;**** Add another base for the switching between ****
                ;**** Graphics and text output options           ****
                ;****************************************************

    flipbase = widget_base(cwid, /row)
    labelval = 'Select output option settings for display: '
    flipid = cw_bgroup(flipbase, ['Graphics','Text'], /exclusive,       $
                     font=large_font, label_left=labelval, /row,     	$
                     set_value=value.itoggle)

		;**** More bases for text and graphics options ****

    flipbase2 = widget_base(cwid)
    graphbase = widget_base(flipbase2, /column, /frame)
    textbase = widget_base(flipbase2, /column)

		;**** Add graphics output options ****

                ;**** Graph title ****

    titlebase = widget_base(graphbase, /row)
    rc = widget_label(titlebase, value='Graph Title', font=font_small)
    titleid = widget_text(titlebase, value=value.gtit1, font=font_small,$
                          /editable, xsize=30)

		;**** Base for all plots ****

    plotbase = widget_base(graphbase, /column)

                ;**** Total power graph base ****

    totpbase = widget_base(plotbase, /column,/frame)
    totphead = widget_label(totpbase, font=large_font,                  $
                            value='Total power fit graph :-')
    totpbase3 = widget_base(totpbase, /row)
    totpbase3a = widget_base(totpbase3, /row, /nonexclusive)
    totpdef = widget_button(totpbase3a, value='Explicit scaling  ',     $
                            font=font_small)
    widget_control, totpdef, set_button=value.ldef1
    totpbase3b = widget_base(totpbase3, /column, /frame)
    totpbase3c = widget_base(totpbase3b, /row)
    totpxmin = widget_label(totpbase3c, font=font_small, value='X-min: ')
    txminbut = widget_text(totpbase3c, font=font_small, 		$
			   value=value.xmin1, xsize=8, /editable)
    totpxmax = widget_label(totpbase3c, font=font_small, value='X-max: ')
    txmaxbut = widget_text(totpbase3c, font=font_small, 		$
			   value=value.xmax1, xsize=8, /editable)
    totpbase3d = widget_base(totpbase3b, /row)
    totpxmin = widget_label(totpbase3d, font=font_small, value='Y-min: ')
    tyminbut = widget_text(totpbase3d, font=font_small, 		$
			   value=value.ymin1, xsize=8, /editable)
    totpxmax = widget_label(totpbase3d, font=font_small, value='Y-max: ')
    tymaxbut = widget_text(totpbase3d, font=font_small, 		$
			   value=value.ymax1, xsize=8, /editable)
    widget_control, totpbase3b, sensitive=value.ldef1

	 	;**** Specific line power graph base ****

		;**** Only include if a key was set on processing screen

    if procval.ispeca(procval.imet) ne -1 then begin
    	slpbase = widget_base(plotbase, /column,/frame)
	slpbase1a = widget_base(slpbase, /row, /nonexclusive)
    	lpltid = widget_button(slpbase1a, font=large_font, 		$
			value='Specific line power fit graph')
	widget_control, lpltid, set_button=value.lplt
    	slpbase3 = widget_base(slpbase, /row)
    	slpbase3a = widget_base(slpbase3, /row, /nonexclusive)
    	slpdef = widget_button(slpbase3a, value='Explicit scaling  ',         $
                           font=font_small)
    	widget_control, slpdef, set_button=value.ldef
    	slpbase3b = widget_base(slpbase3, /column, /frame)
    	slpbase3c = widget_base(slpbase3b, /row)
    	slpxmin = widget_label(slpbase3c, font=font_small, value='X-min: ')
    	sxminbut = widget_text(slpbase3c, font=font_small, 		$
			   value=value.xmin, xsize=8, /editable)
    	slpxmax = widget_label(slpbase3c, font=font_small, value='X-max: ')
    	sxmaxbut = widget_text(slpbase3c, font=font_small, 		$
			   value=value.xmax, xsize=8, /editable)
    	slpbase3d = widget_base(slpbase3b, /row)
    	slpymin = widget_label(slpbase3d, font=font_small, value='Y-min: ')
    	syminbut = widget_text(slpbase3d, font=font_small, 		$
			   value=value.ymin, xsize=8, /editable)
    	slpymax = widget_label(slpbase3d, font=font_small, value='Y-max: ')
    	symaxbut = widget_text(slpbase3d, font=font_small, 		$
			   value=value.ymax, xsize=8, /editable)
    	widget_control, slpbase3b, sensitive=value.ldef
    	widget_control, slpbase3, sensitive=value.lplt
    endif else begin
	value.lplt = 0
	lpltid = 0L
	slpdef = 0L
	slpbase = 0L
	slpbase3 = 0L
	slpbase3b = 0L 
	sxminbut = 0L
	syminbut = 0L
	sxmaxbut = 0L
	symaxbut = 0L
    endelse

                ;**** Base for hard-copy widgets ****

    if value.hrdout ge 0 and strtrim(devlist(0)) ne '' then begin
        hardbase = widget_base(graphbase, /row, /frame)
        outfval = { 	OUTBUT		:	value.hrdout, 		$
			APPBUT		:	-1, 			$
			REPBUT		:	0,              	$
                    	FILENAME	:	value.hardname, 	$
			DEFNAME		:	value.grpdef,        	$
                    	MESSAGE		:	value.grpfmess }
        hardid = cw_adas_outfile(hardbase, OUTPUT='Enable Hard Copy',   $
                                 VALUE=outfval, FONT=font_small)
        devid = cw_single_sel(hardbase, devlist, title='Select Device', $
                              value=value.devsel, font=font_small)
    endif else begin
        devid = 0L
        hardid = 0L
        hardbase = 0L
    endelse

		;**** Map/unmap graphics base according to toggle ****

    widget_control, graphbase, map=(1-value.itoggle)

		;**** Widgets for passing file output ****

    mainval = {		OUTBUT		:	value.maiout,		$
			APPBUT		:	value.maiapp,		$
			REPBUT		:	value.mairep,		$
			FILENAME	:	value.maidsn,		$
			DEFNAME		:	value.maidef,		$
			MESSAGE		:	value.maimes		}
    mainvalcom = mainval
    base = widget_base(textbase, /row, /frame)
    mainid = cw_adas_outfile(base, OUTPUT='MAINBN Passing File',	$
                             VALUE=mainval, FONT=large_font)

    atomval = {		OUTBUT		:	value.atoout,		$
			APPBUT		:	value.atoapp,		$
			REPBUT		:	value.atorep,		$
			FILENAME	:	value.atodsn,		$
			DEFNAME		:	value.atodef,		$
			MESSAGE		:	value.atomes		}
    atomvalcom = atomval
    base = widget_base(textbase, /row, /frame)
    atomid = cw_adas_outfile(base, OUTPUT='ATOMPARS Passing File',	$
                             VALUE=atomval, FONT=large_font)

                ;**** Widget for text output ****

    textval = {		OUTBUT		:	value.texout,		$
			APPBUT		:	value.texapp,		$
			REPBUT		:	value.texrep,		$
			FILENAME	:	value.texdsn,		$
			DEFNAME		:	value.texdef,		$
			MESSAGE		:	value.texmes		}
    textvalcom = textval
    base = widget_base(textbase, /row, /frame)
    textid = cw_adas_outfile(base, OUTPUT='Text Output',		$
                             VALUE=textval, FONT=large_font)

		;**** Map or unmap text base according to toggle ****

    widget_control, textbase, map=value.itoggle

                ;**** Error message ****

    messid = widget_label(cwid, value=' ', font=large_font)

                ;**** add the action buttons ****

    base = widget_base(cwid, /row)
    menufile = bitfile + '/menu.bmp'
    read_X11_bitmap, menufile, bitmap1
    outid = widget_button(base, value=bitmap1)          ;menu button
    cancelid = widget_button(base, value='Back to processing', font=font_small)
    procid = widget_button(base,value='View graph(s)',$
			   font=font_small)
    inputid = widget_button(base, value='Output files and back to input',$
			   font=font_small)
    widget_control, inputid, sensitive = value.viewed

                ;*************************************************
                ;**** create a state structure for the pop-up ****
                ;**** window.                                 ****
                ;*************************************************

    new_state = {	FLIPID		:	flipid,			$
			TITLEID		:	titleid,		$
			TEXTBASE	:	textbase,		$
			GRAPHBASE	:	graphbase,		$
			TOTPDEF		:	totpdef,		$
			TOTPBASE	:	totpbase,		$
			TOTPBASE3B	:	totpbase3b,		$
			LDEF1		:	value.ldef1,		$
			TXMINBUT	:	txminbut,		$
			TYMINBUT	:	tyminbut,		$
			TXMAXBUT	:	txmaxbut,		$
			TYMAXBUT	:	tymaxbut,		$
			LPLTID		:	lpltid,			$
			LPLT		:	value.lplt,		$
			SLPDEF		:	slpdef,			$
			LDEF		:	value.ldef,		$
			SLPBASE		:	slpbase,		$
			SLPBASE3	:	slpbase3,		$
			SLPBASE3B	:	slpbase3b,		$
			SXMINBUT	:	sxminbut,		$
			SYMINBUT	:	syminbut,		$
			SXMAXBUT	:	sxmaxbut,		$
			SYMAXBUT	:	symaxbut,		$
			HARDID		:	hardid,			$
			DEVID		:	devid,			$
			HARDBASE	:	hardbase,		$
			MAINID		:	mainid,			$
			ATOMID		:	atomid,			$
			TEXTID		:	textid,			$
			ITOGGLE		:	value.itoggle,		$
			MESSID		:	messid,			$
			OUTID		:	outid,			$
			CANCELID	:	cancelid,		$
			PROCID		:	procid,			$
			INPUTID		:	inputid,		$
			VIEWED		:	value.viewed		}


                ;**** Save initial state structure ****

    widget_control, first_child, set_uvalue=new_state, /no_copy

    RETURN, parent

END
