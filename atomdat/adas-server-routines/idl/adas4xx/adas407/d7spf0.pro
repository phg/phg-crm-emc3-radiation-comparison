; Copyright (c) 1995, Strathclyde University .
; SCCS INFO Module: @(#)$Header: /home/adascvs/idl/adas4xx/adas407/d7spf0.pro,v 1.1 2004/07/06 13:23:39 whitefor Exp $  Data $Date: 2004/07/06 13:23:39 $
;+
; PROJECT:
;       ADAS IBM MVS to   UNIX conversion
;
; NAME:
;	D7SPF0
;
; PURPOSE:
;	IDL user interface and communications with ADAS407 FORTRAN
;	process via pipe.
;
; EXPLANATION:
;	Firstly this routine invokes the part of the user interface
;	used to select the input dataset for ADAS407.  When the user's
;	interactions are complete this routine communicates with the
;	ADAS407 FORTRAN application via a UNIX pipe.  Communications
;	are to the FORTRAN subroutine D7SPF0.
;
; USE:
;	The use of this routine is specific to ADAS407, see adas407.pro.
;
; INPUTS:
;	PIPE	- The IDL unit number of the bi-directional pipe to the
;		  ADAS407 FORTRAN process.
;
;	VALUE	- A structure which determines the initial settings of
;		  the dataset selection widget.  The initial value is
;		  set in adas407.pro.  VALUE is passed un-modified
;		  through to cw_adas_in.pro, see that routine for a full
;		  description.
;
; OPTIONAL INPUTS:
;	None
;
; OUTPUTS:
;	VALUE	- On output the structure records the final settings of
;		  the dataset selection widget if the user pressed the
;		  'Done' button otherwise it is not changed from input.
;
;	REP	- String; Indicates whether the user pressed the 'Done'
;		  or 'Cancel' button on the interface.  The action is
;		  converted to the strings 'NO' and 'YES' respectively
;		  to match up with the existing FORTRAN code.  In the
;		  original IBM ISPF interface REP was the reply to the
;		  question 'DO YOU WANT TO END PROGRAM EXECUTION'.
;       DSNIC1  - String; The first input data set name.
;       DSNIC2  - String; The second input data set name.
;
; OPTIONAL OUTPUTS:
;	None
;
; KEYWORD PARAMETERS:
;    FONT_LARGE - Supplies the large font to be used for the interface widgets.
;    FONT_SMALL - Supplies the small font to be used for the interface widgets.
;
; CALLS:
;	ADAS407_IN		Pops-up the dataset selection widget.
;
; SIDE EFFECTS:
;	This routine communicates with the ADAS407 FORTRAN process
;	via a UNIX pipe.
;
; CATEGORY:
;	Adas system.
;
; WRITTEN:
;       William Osborn, Tessella Support Services plc, 27-3-1996
;
; MODIFIED:
;	1.1	William Osborn
;		First release - created from adas405.pro
;
; VERSION:
;       1.1	27-03-96
; 

;-
;-----------------------------------------------------------------------------

PRO d7spf0, pipe, value, rep, dsnic1, dsnic2,				$
		FONT_LARGE=font_large, FONT_SMALL=font_small

		;***********************************
                ;**** Set defaults for keywords ****
		;***********************************

    IF NOT (KEYWORD_SET(font_large)) THEN font_large = ''
    IF NOT (KEYWORD_SET(font_small)) THEN font_small = ''

		;**** Store the filenames so that we can tell if they've
		;**** changed

    olde = value.erootpath+value.efile
    oldc = value.crootpath+value.cfile

		;**********************************
		;**** Pop-up input file widget ****
		;**********************************

    adas407_in, value, action, WINTITLE='ADAS 407 INPUT', 		$
	    FONT_LARGE=font_large, FONT_SMALL=font_small

		;********************************************
		;**** Act on the event from the widget   ****
		;**** There are only two possible events ****
		;**** 'Done' and 'Cancel'.               ****
		;********************************************

    if action eq 'Done' then begin
        rep = 'NO'
    endif else begin
        rep = 'YES'
    endelse

		;********************************
		;**** Construct dataset name ****
		;********************************

    dsnic1= value.erootpath + value.efile
    dsnic2= value.crootpath + value.cfile

    if olde ne dsnic1 or oldc ne dsnic2 then value.file_changed=1

		;*******************************
		;**** Write data to fortran ****
		;*******************************

    printf, pipe, rep
    printf, pipe, dsnic1
    printf, pipe, dsnic2

END
