;+
; PROJECT:
;       ADAS IBM MVS to UNIX conversion
;
; NAME:
;	ADAS407
;
; PURPOSE:
;	The highest level routine for the ADAS 407 program.
;
; EXPLANATION:
;	This routine is called from the main adas system routine, adas.pro,
;	to start the ADAS 407 application.  Associated with adas407.pro
;	is a FORTRAN executable.  The IDL code provides the user
;	interface and output graphics whilst the FORTRAN code reads
;	in data files, performs numerical processing and creates the
;	output files.  The IDL code communicates with the FORTRAN
;	executable via a bi-directional UNIX pipe.  The unit number
;	used by the IDL for writing to and reading from this pipe is
;	allocated when the FORTRAN executable is 'spawned' (see code
;	below).  Pipe communications in the FORTRAN process are to
;	stdin and stdout, i.e streams 5 and 6.
;
;	The FORTRAN code is an independent process under the UNIX system.
;	The IDL process can only exert control over the FORTRAN in the
;	data which it communicates via the pipe.  The communications
;	between the IDL and FORTRAN must be exactly matched to avoid
;	input conversion errors.  The correct ammounts of data must be
;	passed so that neither process 'hangs' waiting for communications
;	which will never occur.
;
;	The FORTRAN code performs some error checking which is
;	independent of IDL.  In cases of error the FORTRAN may write
;	error messages.  To prevent these error messages from conflicting
;	with the pipe communications all FORTRAN errors are written to
;	output stream 0, which is stderr for UNIX.  These error messages
;	will appear in the window from which the ADAS session/IDL session
;	is being run.
;
;	In the case of severe errors the FORTRAN code may terminate
;	itself prematurely.  In order to detect this, and prevent the
;	IDL program from 'hanging' or crashing, the IDL checks to see
;	if the FORTRAN executable is still an active process before
;	each group of pipe communications.  The process identifier
;	for the FORTRAN process, PID, is recorded when the process is
;	first 'spawned'.  The system is then checked for the presence
;	of the FORTRAN PID.
;
; USE:
;	First the system settings must be established by calling
;	adas_sys_set.pro then adas407.pro is called to start the
;	ADAS 407 application;
;
;	adas_sys_set, adasrel, fortdir, userroot, centroot, $
;		      devlist, devcode, font_large, font_small, edit_fonts
;	adas407,   adasrel, fortdir, userroot, centroot, devlist, $
;		   devcode, font_large, font_small, edit_fonts
;
; INPUTS:
;	ADASREL - A string indicating the ADAS system version, 
;		  e.g ' ADAS RELEASE: ADAS93 V1.4'.  The first
;		  character should be a space.
;
;	FORTDIR - A string holding the path to the directory where the
;		  FORTRAN executables are, e.g '/disk/adas/fortran/exec'
;
;	USERROOT - A string holding the path to the root directory of
;		   the user's adas data. e.g '/disk/bowen/adas'
;		   This root directory will be used by adas to construct
;		   other path names.  For example the users default data
;		   for adas205 should be in /disk/bowen/adas/adf04.  In
;		   particular the user's default interface settings will
;		   be stored in the directory USERROOT+'/defaults'.  An
;		   error will occur if the defaults directory does not
;		   exist.
;
;	CENTROOT - Like USERROOT, but this directory points to the
;		   central data area for the system.  User defaults are
;		   not stored on CENTROOT.
;
;	DEVLIST - A string array of hardcopy device names, used for
;		  graphical output. e.g ['Post-Script','HP-PCL','HP-GL']
;		  This array must mirror DEVCODE.  DEVCODE holds the
;		  actual device names used in a SET_PLOT statement.
;
;	DEVCODE - A string array of hardcopy device code names used in
;		  the SET_PLOT statement, e.g ['PS', 'PCL', 'HP']
;		  This array must mirror DEVLIST.
;
;	FONT_LARGE - The name of a larger font e.g 'courier_bold14'
;
;	FONT_SMALL - The name of a smaller font e.g 'courier_bold12'
;
;	EDIT_FONTS - A structure of two fonts used in the table editor
;		     adas_edtab.pro, {font_norm:'',font_input:''}.
;		     The two fonts are used to differentiate between
;		     editable and non-editable parts of the table. You
;		     may have to experiment to get two fonts which give
;		     the same default spacing in the edit widget.
;		     e.g {font_norm:'helvetica_bold14', $
;			  font_input:'helvetica_oblique14'}
;
; OPTIONAL INPUTS:
;	None
;
; OUTPUTS:
;	None - Note this routine should leave all inputs un-modified.
;
; OPTIONAL OUTPUTS:
;	None
;
; KEYWORD PARAMETERS:
;	None
;
; CALLS:
;	FIND_PROCESS	Checks to see if a given process is active.
;       XXDATE          Get date and time from operating system.
;       D7SPF0          Pipe comms with FORTRAN D7SPF0 routine.
;       D7ISPF          Pipe comms with FORTRAN D7ISPF routine.
;       D7SPF1          Pipe comms with FORTRAN D7SPF1 routine.
;	D7OTG1		Pipe comms with FORTRAN D7OTG1 routine.
;	D7OTG2		Pipe comms with FORTRAN D7OTG2 routine.
;	D7OTG3		Pipe comms with FORTRAN D7OTG3 routine.
;
; SIDE EFFECTS:
;	This routine spawns a FORTRAN executable.  Note the pipe 
;	communications routines listed above.  In addition to these
;	pipe communications there is one explicit communication of the
;	date to the FORTRAN code, search for 'printf, pipe' to find it.
;	There are also communications of the variable gomenu to the
;	FORTRAN which is used as a signal to stop the program in its
;	tracks and return immediately to the series menu. Do the same
;	search as above to find the instances of this.
;
; CATEGORY:
;	Adas system.
;	
; WRITTEN:
;       William Osborn, Tessella Support Services plc,
;	   27/03/1996
;
; MODIFIED:
;	1.1	William Osborn
;		First release - created from adas405.pro
;	1.2	William Osborn
;		Changed the way charges are passed to d7ispf
;	1.3	William Osborn
;		Updated version number to 1.1
;	1.4	William Osborn
;		Updated version number to 1.2
;       1.5     William Osborn
;               Increased version number to 1.3
;       1.6     William Osborn
;               Increased version number to 1.4
;	1.7	Richard Martin
;		Increased Version number to 1.5
;	1.8	Richard Martin
;		Increased Version number to 1.6
;	1.9	Richard Martin
;		Increased Version number to 1.7
;	1.10	Richard Martin
;		Increased Version number to 1.8
;	1.11	Richard Martin
;		Increased Version number to 1.9
;	1.12	Richard Martin
;		Increased Version number to 1.10
;	1.13	Martin O'Mullane
;		Add in automatic operation where ADAS407 acts
;               upon a directory of adf04 files.
;		Increased version no to 1.11
;	1.14	Martin O'Mullane
;		 - Change file selection to not require fake fully
;                  ionised input adf04 files.
;                - New enteries in autoval structure.
;                - Change text output defaults to adas707_adf03.pass
;                  and adas407_adf25.pass.
;		 - Increased Version number to 1.12
;	1.15	Martin O'Mullane
;		 - In interactive mode wait for fortran to send a
;                  terminating signal.
;
; VERSION:
;       1.1	27-03-96
;	1.2	23-04-96
;	1.3	14-05-96
;	1.4	11-07-96
;       1.5	14-10-96
;       1.6	25-11-96
;	1.7	08-04-97
;	1.8	27-02-98
;	1.9	11-03-98
;	1.10 	18-03-99
;	1.11	18-10-99
;	1.12	17-11-2000
;	1.13	22-01-2002
;	1.14	23-05-2003
;	1.15	04-11-2009
; 
;-
;-----------------------------------------------------------------------------

PRO ADAS407,	adasrel, fortdir, userroot, centroot, 			$
		devlist, devcode, font_large, font_small, edit_fonts

		;************************
		;**** Initialisation ****
		;************************

    adasprog = ' PROGRAM: ADAS407 V1.13'
    lpend    = 0
    gomenu   = 0
    deffile  = userroot+'/defaults/adas407_defaults.dat'
    bitfile  = centroot+'/bitmaps'
    device   = ''

		;******************************************
		;**** Search for user default settings ****
		;**** If not found create defaults     ****
		;**** inval: settings for data files   ****
		;**** procval: settings for processing ****
		;**** outval: settings for output      ****
		;******************************************

    files = findfile(deffile)
    if files(0) eq deffile then begin
        restore, deffile
        inval.ecentroot = centroot+'/adf04/'
        inval.ccentroot = centroot+'/adf04/'
        inval.euserroot = userroot+'/adf04/'
        inval.cuserroot = userroot+'/adf04/'
    endif else begin
	inval =	{							$
		  EROOTPATH	:	userroot+'/adf04/',		$
		  EFILE		:	'',				$
		  ECENTROOT	:	centroot+'/adf04/',		$
		  EUSERROOT	:	userroot+'/adf04/',		$
		  CROOTPATH	:	userroot+'/adf04/',		$
		  CFILE		:	'',				$
	   	  CCENTROOT	:	centroot+'/adf04/',		$
		  CUSERROOT	:	userroot+'/adf04/',		$
		  FILE_CHANGED	:	1				}
  
        procval = {NMET:-1}
	maidefval = userroot + '/pass/adas407_adf25.pass'
	atodefval = userroot + '/pass/adas407_adf03.pass'
        outval = {	ITOGGLE		:	0,			$
			LGRAPH	 	:	1,			$
                        XMIN1           :       '',                     $
                        XMAX1           :       '',                     $
                        YMIN1           :       '',                     $
                        YMAX1           :       '',                     $
                        XMIN            :       '',                     $
                        XMAX            :       '',                     $
                        YMIN            :       '',                     $
                        YMAX            :       '',                     $
                        LDEF1           :       0,                      $
                        LDEF            :       0,                      $
	      	 	LPLT            :       0, 			$
                        GTIT1           :       '',                     $
                        HRDOUT          :       0,                      $
                        HARDNAME        :       '',                     $
                        GRPDEF          :       '',                     $
                        GRPFMESS        :       '',                     $
                        DEVSEL          :       -1,                     $
                        TEXOUT          :       0,                      $
                        TEXAPP          :       -1,                     $
                        TEXREP          :       0,                      $
                        TEXDSN          :       '',                     $
                        TEXDEF          :       'paper.txt',            $
                        TEXMES          :       '',                     $
                        MAIOUT          :       0,                      $
                        MAIAPP          :       -1,                     $
                        MAIREP          :       0,                      $
                        MAIDSN          :       '',                     $
                        MAIDEF          :       maidefval,              $
                        MAIMES          :       '',                     $
                        ATOOUT          :       0,                      $
                        ATOAPP          :       -1,                     $
                        ATOREP          :       0,                      $
                        ATODSN          :       '',                     $
                        ATODEF          :       atodefval,              $
                        ATOMES          :       '',			$
			VIEWED		:	0                       }
    
        a03name = userroot + '/pass/adas407_adf03.pass'
        autoval = {     ROOTPATH        :       centroot+'/adf04/',     $
                        CENTROOT        :       centroot+'/adf04/',     $
                        USERROOT        :       userroot+'/adf04/',     $  
                        A03OUT          :       0,                      $
                        A03APP          :       -1,                     $
                        A03REP          :       0,                      $
                        A03DSN          :       '',                     $
                        A03DEF          :       A03name,                $
                        A03MES          :       '',                     $
                        TEXOUT          :       0,                      $
                        TEXAPP          :       -1,                     $
                        TEXREP          :       0,                      $
                        TEXDSN          :       '',                     $
                        TEXDEF          :       'paper.txt',            $
                        TEXMES          :       '',                     $
                        prefix          :       'ls',                   $
                        ELEM            :       '',                     $
                        ION_LOW         :       0,                      $
                        ION_HIGH        :       1,                      $
                        ionval          :       0,                      $
                        rrval           :       0,                      $
                        drval           :       0,                      $
                        powval          :       0                       }
   
    
    end



;------------ Add in option to automatically consider a set of
;             adf04 files. If not revert to the old method.

LABEL50:

   ; Determine which branch we want ****

   rep = adas407_branch(font_large=font_large)

   if rep EQ 'CANCEL' then return
   

LABEL60:

   if rep EQ 'AUTO' then begin
  
      rep=adas407_automatic(autoval, fortdir, bitfile, font_large=font_large)

      if rep EQ 'MENU' then return
      if rep EQ 'CANCEL' then goto, LABEL50 else goto, LABELSAVE
            
   endif

;------------ As we were before the automatic option was added




		;****************************
		;**** Start fortran code ****
		;****************************

    spawn, fortdir+'/adas407.out', unit=pipe, /noshell, PID=pid

		;************************************************
		;**** Get date and write to fortran via pipe ****
		;************************************************

    date = xxdate()
    printf, pipe, date[0]

LABEL100:

    ldout = 0
		;**** Check FORTRAN still running ****
  
    if find_process(pid) eq 0 then goto, LABELEND

		;************************************************
		;**** Communicate with d7spf0 in fortran and ****
		;**** invoke user interface widget for       ****
		;**** Data file selection                    ****
		;************************************************

    d7spf0, pipe, inval, rep, dsnic1, dsnic2,				$
	    FONT_LARGE=font_large, FONT_SMALL=font_small

		;**** If cancel selected then end program ****

    if rep eq 'YES' then goto, LABELEND

		;**** Check FORTRAN still running ****

    if find_process(pid) eq 0 then goto, LABELEND


		;************************************************
		;**** Communicate with d7setp in fortran and ****
		;**** get values for processing screen       ****
		;************************************************

   d7setp, pipe, sz0, sz, scnte, sil, lfpool, strga, sz1, sil2, 	$
	    lfpool2, strga2

LABEL200:

    if find_process(pid) eq 0 then goto, LABELEND

		;************************************************
		;**** Communicate with d7ispf in fortran and ****
		;**** invoke user interface widget for       ****
		;**** processing options                     ****
		;************************************************

    d7ispf, pipe, lpend, inval, procval, dsnic1, dsnic2, strga, strga2,	$
	    gomenu, bitfile, sz0, sz, sz1,				$
	    FONT_LARGE=font_large, FONT_SMALL=font_small, 		$
            EDIT_FONTS=edit_fonts

		;**** If menu button clicked, tell FORTRAN to stop ****

    if gomenu eq 1 then begin
 	printf, pipe, 1
 	goto, LABELEND
    endif else begin
 	printf, pipe, 0
    endelse


		;**** If cancel selected then goto 400 ****

    if lpend eq 1 then goto, LABEL400

		;**** Fortran processing now in progress. Get ****
		;**** date and time for this run.	      ****

    date = xxdate()

		;**** Create header for output. ****

    header = adasrel+adasprog+' DATE: '+date(0)+' TIME: '+date(1)

    outval.viewed = 0

LABEL300:
		;**** Check FORTRAN still running ****

    if find_process(pid) eq 0 then goto, LABELEND

		;************************************************
		;**** Communicate with d7spf1 in fortran and ****
		;**** invoke user interface widget for       ****
		;**** Output options                         ****
		;************************************************

    d7spf1, pipe, inval, procval, outval, bitfile, pid, lpend,  	$
            gomenu, header, DEVLIST=devlist, 				$
	    FONT_LARGE=font_large, FONT_SMALL=font_small

                ;**** Extra check to see whether FORTRAN is still there ****

    if find_process(pid) eq 0 then goto, LABELEND

		;**** If menu button clicked, tell FORTRAN to stop ****

    if gomenu eq 1 then begin
	printf, pipe, 1
        outval.texmes = ''
        outval.maimes = ''
        outval.atomes = ''
	goto, LABELEND
    endif else begin
	printf, pipe, 0
    endelse


		;**********************************************************
		;**** If back to processing selected then erase output ****
		;**** messages and goto 200.		               ****
		;**********************************************************

    if lpend eq 1 then begin
        outval.texmes = ''
        outval.maimes = ''
        outval.atomes = ''
        goto, LABEL200
    endif 

		;**********************************************************
		;**** If output and return to input selected, goto 400 ****
		;**********************************************************

    if lpend eq 3 then goto, LABEL400

		;**********************************************
		;**** Otherwise, Graphical Output Routines ****
		;**********************************************

        hrdout = outval.hrdout
        hardname = outval.hardname
        if outval.devsel ge 0 then device = devcode(outval.devsel)

		;**** Optional user title/comment ****
		;**** The other options from the output screen will ****
		;**** be passed from the Fortran pipe		    ****

        utitle = outval.gtit1

		;**** Total power graph ****

        d7outg, dsfull, pipe, utitle,				 	$
                hrdout, hardname, device, header, bitfile, gomenu,  	$
                FONT=font_large
	if gomenu eq 1 then begin
	    printf, pipe, 1
	    goto, LABELEND
        endif else begin
	    printf, pipe, 0
     	endelse

		;**** Specific line power graph ****

	if outval.lplt eq 1 then begin
            d7outh, dsfull, pipe, utitle,				 $
                hrdout, hardname, device, header, bitfile, gomenu,  	 $
                FONT=font_large
	    if gomenu eq 1 then begin
	        printf, pipe, 1
	        goto, LABELEND
            endif else begin
	        printf, pipe, 0
     	    endelse
	endif
    outval.viewed = 1
    ldout = 1
    goto, LABEL300

LABEL400:

		;*************************************************************
		;**** User has selected output and return to input screen ****
		;*************************************************************

    if ldout eq 0 then goto, LABEL100

    goto, LABEL100


LABELEND:
		;**** Pipe out user-id when closing files

    userid = '          '
    uid = getenv('USER')
    strput,userid,strmid(uid,0,10)
    printf,pipe,userid

LABELSAVE:
		;**** Save user defaults and free all units ****

    save, inval, procval, outval, autoval, filename=deffile
    
    ; How did we get here - automatic or interactive branch?
    
    if rep NE 'CONTINUE - AUTO' then begin
       idum = -1L
       readf, pipe, idum
       close, /all
    endif

END
