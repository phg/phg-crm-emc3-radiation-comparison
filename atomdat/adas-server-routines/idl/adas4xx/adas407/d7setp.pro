; Copyright (c) 1995 Strathclyde University .
; SCCS info: Module @(#)$Header: /home/adascvs/idl/adas4xx/adas407/d7setp.pro,v 1.1 2004/07/06 13:23:31 whitefor Exp $ Date $Date: 2004/07/06 13:23:31 $
;+
; PROJECT:
;       ADAS IBM MVS to DEC UNIX conversion
;
; NAME:
;	BXSETP
;
; PURPOSE:
;	IDL communications with ADAS407 FORTRAN process via pipe.
;
; EXPLANATION:
;	This IDL routine reads data from the ADAS407 FORTRAN subroutine
;	BXSETP via a UNIX pipe.  In the original IBM version the FORTRAN
;	subroutine BXSETP put information needed by the ISPF interface
;	into the ISPF pool area for later use.  Now the same information
;	is written to IDL for use in its user unterface.
;
; USE:
;	The use of this routine is specific to ADAS407.  See
;	adas407.pro for an example.
;
; INPUTS:
;	PIPE	- The IDL unit number of the bi-directional pipe to the
;		  ADAS407 FORTRAN process.
;
; OPTIONAL INPUTS:
;	None
;
; OUTPUTS:
;	SZ0	- String, nuclear charge read.
;
;	SZ	- String, recombined ion charge read.
;
;	SCNTE	- Number of electron impact transitions.
;
;	SIL	- Number of energy levels.
;
;	LFPOOL	- Integer, number of level strings. i.e dimension of strga
;		  (In IBM version number of strings sent to function pool)
;
;	STRGA	- String array, level designations.
;
; OPTIONAL OUTPUTS:
;	None
;
; KEYWORD PARAMETERS:
;	None
;
; CALLS:
;	None
;
; SIDE EFFECTS:
;	Reads data from a pipe.
;
; CATEGORY:
;	Adas system.
;
; WRITTEN:
;       William Osborn, Tessella Support Services plc, 2-Apr-1996
;
; MODIFIED:
;       1.1     William Osborn
;		First Version
;
; VERSION:
;       1.1     02-04-96
;
;-
;-----------------------------------------------------------------------------



PRO d7setp, pipe, sz0, sz, scnte, sil, lfpool, strga, sz1, sil2, 	$
	    lfpool2, strga2

		;**********************************
		;**** Initialise new variables ****
		;**********************************
  sz0 = ''
  sz = ''
  scnte = ''
  sil = ''
  lfpool = 0
  sz1 = ''
  sil2 = ''
  lfpool2 = 0
  input = ''
  idum = 0

		;********************************
		;**** Read data from fortran ****
		;********************************
  readf, pipe, input
  sz0 = input
  readf, pipe, input
  sz = input
  readf, pipe, input
  scnte = input
  readf, pipe, input
  sil = input

  readf, pipe, idum
  lfpool = idum
  strga = strarr(lfpool)

  for i = 0, lfpool-1 do begin
    readf, pipe, input
    strga(i) = input
  end

  readf, pipe, input
  sz1 = input
  readf, pipe, input
  sil2 = input

  readf, pipe, idum
  lfpool2 = idum
  strga2 = strarr(lfpool2)

  for i = 0, lfpool2-1 do begin
    readf, pipe, input
    strga2(i) = input
  end

END
