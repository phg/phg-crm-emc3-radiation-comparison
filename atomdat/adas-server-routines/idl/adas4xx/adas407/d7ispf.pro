; Copyright (c) 1995 Strathclyde University 
; SCCS info: Module @(#)$Header: /home/adascvs/idl/adas4xx/adas407/d7ispf.pro,v 1.4 2004/07/06 13:22:20 whitefor Exp $ Date $Date: 2004/07/06 13:22:20 $
;
;+
; PROJECT:
;       ADAS IBM MVS to   UNIX conversion
;
; NAME:
;	D7ISPF
;
; PURPOSE:
;	IDL user interface and communications with ADAS407 FORTRAN
;	process via pipe.
;
; EXPLANATION:
;	The routine begins by reading information from the ADAS407
;	FORTRAN process via a UNIX pipe.  Then part of the ADAS407
;	IDL user interface is invoked to determine how the user
;	wishes to process the input dataset.  When the user's
;	interactions are complete the information gathered with
;	the user interface is written back to the FORTRAN process
;	via the pipe.  Communications are to the FORTRAN subroutine
;	D7ISPF.
;
; USE:
;	The use of this routine is specific to ADAS407, see adas407.pro.
;
; INPUTS:
;	PIPE	- The IDL unit number of the bi-directional pipe to the
;		  ADAS407 FORTRAN process.
;
;	INVAL   - The structure returned from the input screen - see
;		
;
;	PROCVAL	- A structure which determines the initial settings of
;		  the processing options widget.  The initial value is
;		  set in adas407.pro.  If adas407.pro passes a blank 
;		  dummy structure of the form {NMET:-1} into PROCVAL then
;		  PROCVAL is reset to a default structure.
;
;		  The PROCVAL structure is;
;
;        procval = {	title	: 	'',,                		$
;			il	:	0,				$
;			ipl	:	0,				$
;			nv	:	0,				$
;			tscef	:	temp_arr,			$
;			nmet	:	0,				$
;			npmet	:	0,				$
;			imetr	:	0,				$
;			ipmetr	:	0,				$
;			imet	:	-1,				$
;			ifout	:	1,				$
;			itsel	:	0,				$
;			selr	:	0,				$
;			seld	:	0,				$
;			sels	:	0,				$
;			selp	:	0,				$
;			sell	:	0,				$
;			ltadj	:	0,				$
;			ictm	:	int_arr,			$
;			iuma	:	int_arr2,			$
;			cstgma	:	str_arr,			$
;			imaloca	:	int_arr2,			$
;			ispeca	:	int_arr,			$
;			}
;
;
;		  See cw_adas407_proc.pro for a full description of this
;		  structure.
;	
;
;	NDISC1	- String; the full system file name of the first input 
;		  dataset selected by the user for processing.
;
;	NDISC2	- String; the full system file name of the first input 
;		  dataset selected by the user for processing.
;
;	STRGA   - String array; the level designation information string
;		  for the ionising ion selection list
;
;	STRGA2  - String array; the level designation information string
;		  for the ionised ion selection list
;
;       BITFILE - String; the path to the dirctory containing bitmaps
;                 for the 'escape to series menu' button.
;
;	FILE_CHANGED
;		- 1 if the file has been changed on the input screen, 0 if
;		  not
;
; OPTIONAL INPUTS:
;	None
;
; OUTPUTS:
;	PROCVAL	- On output the structure records the final settings of
;		  the processing selections widget if the user pressed the
;		  'Done' button, otherwise it is not changed from input.
;
;	LPEND	- Integer, 0 if the user pressed the 'Done' button
;		  or 1 if the user pressed 'Cancel'.  Maps directly onto
;		  the logical variable LPEND in ADAS407 FORTRAN.
;
;       GOMENU  - Int; flag - set to 1 if user has selected 'escape direct
;                 to series menu , otherwise 0
;
; OPTIONAL OUTPUTS:
;	None
;
; KEYWORD PARAMETERS:
;	FONT_LARGE - The name of a larger font.
;
;	FONT_SMALL - The name of a smaller font.
;
;	EDIT_FONTS - A structure of two fonts used in the table editor
;		     adas_edtab.pro, {font_norm:'',font_input:''}.
;
; CALLS:
;	ADAS407_PROC	Invoke the IDL interface for ADAS407 data
;		 	processing options/input.
;
; SIDE EFFECTS:
;	Two way communications with ADAS407 FORTRAN via the
;	bi-directional UNIX pipe.
;
; CATEGORY:
;	Adas system.
;
; WRITTEN:
;       William Osborn, Tessella Support Services plc, 1-Apr-1996
;               (d1ispf.pro cannibalisation)
; MODIFIED:
;	1.1	William Osborn
;		First version
;	1.2	William Osborn
;		Changed the way the charges are passed, sz, ...
;	1.3	Hugh Summers
;		Corrected dimensioning of tscef: (nv,3) to (ndtem,3)
;	1.4	Martin O'Mullane
;		Change ipnput from integer to long to cope with 
;               the large number of transitions (more than int 
;               maximum size).
;
; VERSION:
;       1.1	01-04-96
;	1.2 	23-04-96
;	1.3	25-06-97
;	1.4	04-03-2004
; 
;-
;-----------------------------------------------------------------------------

PRO d7ispf, pipe, lpend, inval, procval, dsnic1, dsnic2, strga, strga2,	$
	    gomenu, bitfile, sz0, sz, sz1,				$
            FONT_LARGE=font_large, FONT_SMALL=font_small, 		$
            EDIT_FONTS=edit_fonts


                ;**** Set defaults for keywords ****

    IF NOT (KEYWORD_SET(font_large)) THEN font_large = ''
    IF NOT (KEYWORD_SET(font_small)) THEN font_small = ''
    IF NOT (KEYWORD_SET(edit_fonts)) THEN 				$
    edit_fonts = {font_norm:'', font_input:''}

		;********************************************
		;****     Declare variables for input    ****
		;**** arrays wil be declared after sizes ****
		;**** have been read.                    ****
                ;********************************************

    input  = 0
    idum   = 0L
    rinput = 0.0

		;********************************
		;**** Read data from fortran ****
		;********************************

    readf, pipe, input
    ndlev = input
    readf, pipe, idum
    ndtrn = idum
    readf, pipe, input
    ndtem = input
    readf, pipe, input
    ndmet = input
    readf, pipe, input
    ndosc = input
    readf, pipe, input
    ndbndl= input
    readf, pipe, input
    il = input
    readf, pipe, input
    ipl = input
    readf, pipe, input
    nv = input

		;******************************************
		;**** Now can define other array sizes ****
		;******************************************

;    tscef  = dblarr(nv,3)
    tscef  = dblarr(ndtem,3)
    ietrn  = intarr(ndtrn)
    ie1a   = intarr(ndtrn)
    ie2a   = intarr(ndtrn)
    aa     = dblarr(ndtrn)
;    scom   = dblarr(nv,ndtrn)
    scom   = dblarr(ndtem,ndtrn)
    ictm   = intarr(ndmet)
    iuma   = intarr(ndosc,ndmet)
    cstgma = strarr(ndosc,ndmet)
    

		;********************************
                ;**** Read data from fortran ****
                ;********************************

    sdum = ' '
    for j=0, 2 do begin
	for i=0,nv-1 do begin
            readf, pipe, rinput
  	    tscef(i,j) = rinput
	endfor
    endfor

    readf,pipe,input
    icnte = input

;    for j=0, ndtrn-1 do begin
;        readf, pipe, input
;        ietrn(j) = input
;    endfor
;    for j=0, ndtrn-1 do begin
;        readf, pipe, input
;        ie1a(j) = input
;    endfor
;    for j=0, ndtrn-1 do begin
;        readf, pipe, input
;        ie2a(j) = input
;    endfor
;    for j=0, ndtrn-1 do begin
;        readf, pipe, rinput
;        aa(j) = rinput
;    endfor
;    for j=0, ndtrn-1 do begin
;	for i=0,nv-1 do begin
;            readf, pipe, rinput
;  	    scom(i,j) = rinput
;	endfor
;    endfor


		;*******************************************
		;**** Set default value if not provided ****
		;*******************************************

    if (procval.nmet lt 0) then begin
	flt_arr = fltarr(ndlev)
	str_arr = strarr(ndosc,ndmet)
	int_arr = intarr(ndmet)
	int_arr2 = intarr(ndosc,ndmet)
        temp_arr = fltarr(ndtem,3)
        procval = {	title	: 	'',                		$
			il	:	il,				$
			ipl	:	ipl,				$
			nv	:	nv,				$
			tscef	:	tscef,				$
			nmet	:	0,				$
			npmet	:	0,				$
			imetr	:	int_arr,			$
			ipmetr	:	int_arr,			$
			imet	:	-1,				$
			ifout	:	1,				$
			itsel	:	0,				$
			selr	:	0,				$
			seld	:	0,				$
			sels	:	0,				$
			selp	:	0,				$
			sell	:	0,				$
			ltadj	:	0,				$
			ictm	:	int_arr,			$
			iuma	:	int_arr2,			$
			cstgma	:	str_arr,			$
			imaloca	:	int_arr2,			$
			ispeca	:	int_arr,			$
			strga   :       strga,				$
			strga2	:	strga2				$
			}
    endif

		;**** Set those values which need to be set

    procval.il = il
    procval.ipl = ipl
    procval.nv = nv
    procval.tscef = tscef
    procval.strga = strga
    procval.strga2 = strga2

		;*****************************************
		;**** Set those values which change   ****
		;**** with the file if it has changed ****
		;*****************************************

    if (inval.file_changed eq 1) then begin


		;**** Set selection lists to blank ****

    	procval.imetr = intarr(ndmet)
    	procval.ipmetr = intarr(ndmet)
    	procval.nmet = 0
    	procval.npmet = 0
    	procval.imet = -1
    	procval.itsel = 0

		;**** Reset file_changed flag

	inval.file_changed = 0

    endif

		;****************************************
		;**** Pop-up processing input widget ****
		;****************************************

    adas407_proc, procval, dsnic1, dsnic2, sz0, sz, sz1,	 	$
		ndlev, ndtrn, ndtem, ndmet, ndosc, ndbndl,		$
		action, bitfile, pipe,					$
		FONT_LARGE=font_large, FONT_SMALL=font_small, 		$
		EDIT_FONTS=edit_fonts

		;*******************************************
		;****  Act on the event from the widget ****
                ;**** There are three  possible actions ****
		;**** 'Done', 'Cancel'and 'Menu'.       ****
		;*******************************************

    if action eq 'Done' then begin
        lpend = 0
    endif else if action eq 'Menu' then begin
        lpend = 0
        gomenu = 1
    endif else begin
        lpend = 1
    endelse


		;*******************************
		;**** Write data to fortran ****
		;*******************************

		;First pipe-out a zero to say that we are not bundling
    ibundle = 0
    printf, pipe, ibundle

    printf, pipe, lpend   
    title_short = strmid(procval.title,0,39) 	
    printf, pipe, title_short, format='(a40)' 
    printf, pipe, procval.ifout			
		;**** Convert itsel to Fortran index
    printf, pipe, procval.itsel+1

		;*** csel# have been replaced by integers, sel#, 
	 	;*** in the idl and so need converting back to strings

    if procval.selr eq 0 then printf, pipe, 'A'	$
    else printf, pipe, 'B'
    if procval.seld eq 0 then printf, pipe, 'A' $
    else printf, pipe, 'B'
    if procval.sels eq 0 then printf, pipe, 'A' $
    else printf, pipe, 'B'
    if procval.selp eq 0 then printf, pipe, 'A' $
    else printf, pipe, 'B'
    if procval.sell eq 0 then printf, pipe, 'A' $
    else printf, pipe, 'B'

    printf, pipe, procval.ltadj
    for j=0,ndmet-1 do begin
  	for i=0,ndosc-1 do begin
	    printf, pipe, procval.imaloca(i,j)
	endfor
    endfor
    printf, pipe, procval.ispeca
    printf, pipe, procval.nmet
    printf, pipe, procval.npmet
; Convert imet, imetr and ipmetr to FORTRAN indices
    printf, pipe, procval.imetr+1
    printf, pipe, procval.ipmetr+1
    printf, pipe, procval.imet+1
END
