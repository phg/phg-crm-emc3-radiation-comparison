;----------------------------------------------------------------------
;+
; PROJECT    :  ADAS
;
; NAME       :  run_adas407
;
; PURPOSE    :  Runs ADAS407 to generate parameterised atomic data for
;               use in ADAS408 and ADAS204 codes.
;
; ARGUMENTS  :  All output arguments will be defined appropriately.
;
;               NAME      I/O    TYPE   DETAILS
; INPUTS     :  z_adf04    I     str    ionising specific ion file
;               z1_adf04   I     str    ionised specific ion file
;               ion_type   I     str    'A' or 'B' ionisation parameterisation
;               rr_type    I     str               radiative recomb.
;               dr_type    I     str               dielectronic reomb.
;               pow_type   I     str               line power
;               nmet       I     int    Number of metastables in ionising stage
;                                       If not given the code attempts to
;                                       deduce number (defaults to 1 if not possible)
;               npar       I     int    Number of metastables in ionised stage.
;               log        O     str    Output logfile name.
;               adf03      O     str    Fragment of adf03 atompars file.
;               adf25      O     str    Partial driver file for adas204.
;
;
; KEYWORDS      pbar               -    Display a progress bar when running.
;               help               -    If specified this comment section
;                                       is written to screen.
;
;
; OUTPUTS    :  The adf03 and adf25 outputs are not complete. The adf03 should
;               be embedded in an iso-nuclear set. The adf25 requires editing
;               to include supplementary ionisation and DR data.
;
;
; NOTES      :  run_adas407 uses the spawn command. Therefore IDL v5.3
;               cannot be used. Any other version should work.
;
;               If parameterisation types not given default to type 'A'
;
;               If number of metastables not given metastable.pro is called
;               to deduce number but this is only valid for H-like to Ar-like.
;               Otherwise it defaults to 1.
;
;
; AUTHOR     :  Martin O'Mullane
;
; DATE       :  05-11-2009
;
; MODIFIED
;       1.1     Martin O'Mullane
;                 - First version.
;       1.2     Martin O'Mullane
;                 - If npar is set copy it to internal variable (npmet).
;       1.3     Martin O'Mullane
;                 - Set seq to FS for fully stripped ion.
;       1.4     Martin O'Mullane
;                 - Incorrect metastable index for stages with more
;                   than one metastable.
;
; DATE
;       1.1     05-11-2009
;       1.2     21-12-2011
;       1.3     19-07-2012
;       1.4     15-10-2016
;
;-
;----------------------------------------------------------------------


PRO run_adas407, z_adf04   =  z_adf04,   $
                 z1_adf04  =  z1_adf04,  $
                 ion_type  =  ion_type,  $
                 rr_type   =  rr_type,   $
                 dr_type   =  dr_type,   $
                 pow_type  =  pow_type,  $
                 nmet      =  nmet,      $
                 npar      =  npar,      $
                 adf03     =  adf03,     $
                 adf25     =  adf25,     $
                 log       =  log,       $
                 help      =  help


; If asked for help

if keyword_set(help) then begin
   doc_library, 'run_adas407'
   return
endif



; First check that we are not using IDL v5.3

if !version.release EQ '5.3' then begin
   message, 'This version of IDL (v5.3) will not work - ' + $
            'change to any other version of IDL!'
   return
endif


; Check inputs

if n_elements(z_adf04) EQ 0 then message, 'An ionising adf04 file is necessary'
a407_adf04_z = z_adf04

if n_elements(z1_adf04) EQ 0 then message, 'An ionised adf04 file is necessary'
a407_adf04_z1 = z1_adf04


; Output files

if n_elements(adf03) EQ 0 then a03 = 'NULL' else a03 = adf03
if n_elements(adf25) EQ 0 then a25 = 'NULL' else a25 = adf25
if n_elements(log)   EQ 0 then log = 'NULL'


; case A or B

if n_elements(ion_type) EQ 0 then begin
   ion_case = 0
endif else begin
   case strupcase(ion_type) of
      'A'  : ion_case = 0
      'B'  : ion_case = 1
      else : message, 'Incorrect type for ionisation'
   endcase
endelse

if n_elements(rr_type) EQ 0 then begin
   rr_case = 0
endif else begin
   case strupcase(rr_type) of
      'A'  : rr_case = 0
      'B'  : rr_case = 1
      else : message, 'Incorrect type for radiative recombinaion'
   endcase
endelse

if n_elements(dr_type) EQ 0 then begin
   dr_case = 0
endif else begin
   case strupcase(dr_type) of
      'A'  : dr_case = 0
      'B'  : dr_case = 1
      else : message, 'Incodrect type for dielectronic recombinaion'
   endcase
endelse

if n_elements(pow_type) EQ 0 then begin
   pow_case = 0
endif else begin
   case strupcase(pow_type) of
      'A'  : pow_case = 0
      'B'  : pow_case = 1
      else : message, 'Incorrect type for power'
   endcase
endelse


;-----------------
; Now run the code
;-----------------

fortdir = getenv('ADASFORT')
spawn, fortdir+'/adas407_auto.out', unit=pipe, /noshell, PID=pid


date = xxdate()
printf, pipe, date[0]

printf, pipe, a407_adf04_z
printf, pipe, a407_adf04_z1
printf, pipe, a03
printf, pipe, a25
printf, pipe, log

printf, pipe, ion_case
printf, pipe, rr_case
printf, pipe, dr_case
printf, pipe, pow_case


; Number of metastables

idum = -1L
readf, pipe, iza
readf, pipe, iz0a
readf, pipe, izp
readf, pipe, iz0p

if iz0a NE iz0p then message, 'adf04 files are not the same element'

ff    =  iz0a - iza
seq_a = xxesym(ff)
ff    =  iz0p - izp
seq_p = xxesym(ff)

if abs(izp-iza) NE 1 then message, 'adf04 files are not adjacent ion stages'


; Figure out number of metastables if not given

if n_elements(nmet) EQ 0 then begin

  nmet = 1L
  if (iz0a - iza) LE 18 then begin
     if seq_a EQ '  ' then seq_a = 'FS'
     metastable, seq=seq_a, term=term_a
     nmet  = n_elements(term_a)
  endif

endif else nmet = long(nmet)


if n_elements(npar) EQ 0 then begin

  npmet = 1L
  if (iz0a - iza) LE 18 then begin
     if seq_p EQ '  ' then seq_p = 'FS'
     metastable, seq=seq_p, term=term_p
     npmet  = n_elements(term_p)
  endif

endif else npmet = long(npar)

printf, pipe, nmet
for j = 0, nmet-1 do printf, pipe, long(j+1)
printf, pipe, npmet
for j = 0, npmet-1 do printf, pipe, long(j+1)

idum = -1L
readf, pipe, idum

close, /all

END
