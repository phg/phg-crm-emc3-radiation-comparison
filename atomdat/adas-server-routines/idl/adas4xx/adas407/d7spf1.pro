; Copyright (c) 1996, Strathclyde University.
; SCCS Info: Module @(#)$Header: /home/adascvs/idl/adas4xx/adas407/d7spf1.pro,v 1.1 2004/07/06 13:23:47 whitefor Exp $ Date $Date: 2004/07/06 13:23:47 $
;+
; PROJECT:
;       ADAS IBM MVS to UNIX conversion
;
; NAME:
;	D7SPF1
;
; PURPOSE:
;	IDL user interface and communications with ADAS407 FORTRAN
;	process via pipe.
;
; EXPLANATION:
;	First this routine invokes the 'Output Options' part of the
;	user interface for adas 407. Secondly the results of the user's
;	interactions are written to the FORTRAN process via the UNIX pipe.
;	Communications are with the FORTRAN subroutine D7SPF1.
;
; USE:
;	The use of this routine is specific to ADAS407, See adas407.pro.
;
; INPUTS:
;	PIPE	- The IDL unit number of the bi-directional pipe to the
;		  ADAS407 FORTRAN process.
;
;	INVAL	- A structure holding the final settings of the input
;		  options widget.
;
;	PROCVAL	- A structure holding the final settings of the processing
;		  options widget.
;
;	OUTVAL	- A structure which determines the initial settings of
;		  the output options widget.  The initial value is
;		  set in adas407.pro.  OUTVAL is passed un-modified
;		  through to cw_adas407_out.pro, see that routine for a full
;		  description.
;
;       BITFILE - String; the path to the dirctory containing bitmaps
;                 for the 'escape to series menu' button.
;
;	PID	- Integer; the process id of the FORTRAN part of the code.
;
;       HEADER  - Header information used for text output.
;
; OPTIONAL INPUTS:
;	None.
;
; OUTPUTS:
;	LPEND	- Integer; indicates user action on leaving output
;		  selections.  
;			0 = 'Return to series menu'
;			1 = 'Return to processing screen' = 'Cancel'
;			2 = 'View graphs and return to processing'
;			3 = 'Produce output and return to input screen'
;
;	OUTVAL	- On output the structure records the final settings of
;		  the output selection widget if the user pressed 
;		  ????
;
;       GOMENU  - Int; flag - set to 1 if user has selected 'escape direct
;                 to series menu' , otherwise 0
;
; OPTIONAL OUTPUTS:
;	None.
;
; KEYWORD PARAMETERS:
;	DEVLIST	- A string array;  Each element of the array is the
;		  name of a hardcopy graphics output device.  This
;		  list must mirror the DEVCODE list.  DEVLIST is the
;		  list which the user will see.
;		  e.g ['Post-Script','HP-PCL','HP-GL']
;
;	FONT_LARGE- Supplies the large font to be used.
;
;	FONT_SMALL- Supplies the small font to be used.
;
; CALLS:
;	ADAS407_OUT	User interface - output options.
;
; SIDE EFFECTS:
;	This routine communicates with the ADAS407 FORTRAN process
;	via a UNIX pipe.
;
; CATEGORY:
;	Adas system.
;
; WRITTEN:
;       William Osborn, Tessella Support Services plc, 10-04-96
;
; MODIFIED: 
;	1.1	William Osborn
;		First version - made from b3spf1.pro v1.2
;
; VERSION:
;	1.1	10-04-96
;
;-
;-----------------------------------------------------------------------------

PRO d7spf1, pipe, inval, procval, outval, bitfile, pid, lpend,  	$
            gomenu, header, DEVLIST=devlist, 				$
	    FONT_LARGE=font_large, FONT_SMALL=font_small


                ;**** Set defaults for keywords ****

    IF NOT (KEYWORD_SET(font_large)) THEN font_large = ''
    IF NOT (KEYWORD_SET(font_small)) THEN font_small = ''

		;**** Read lsetp from Fortran ****
    idum = 0
    readf, pipe, idum
    lsetp = idum

		;**************************************
		;**** Pop-up output options widget ****
		;**************************************

    adas407_out, inval, procval, outval, action, bitfile, lsetp,	$
                 DEVLIST=devlist, FONT_LARGE=font_large, 		$
                 FONT_SMALL=font_small

		;*******************************************
		;**** Act on the output from the widget ****
		;**** There are five possible actions   ****
		;*******************************************

    if action eq 'Menu' then begin
	lpend = 0
	gomenu = 1
    endif else if action eq 'Proc' then begin
	lpend = 1
    endif else if action eq 'Graph' then begin
	lpend = 2
    endif else if action eq 'Input' then begin
	lpend = 3
    endif

		;*******************************
		;**** Write data to fortran ****
		;*******************************

    printf, pipe, lpend
    if lpend eq 2 then begin

	printf, pipe, outval.ldef1
	if outval.ldef eq 1 then begin
	    printf, pipe, outval.xmin1
	    printf, pipe, outval.xmax1
	    printf, pipe, outval.ymin1
	    printf, pipe, outval.ymax1
	endif

	printf, pipe, outval.lplt
	if outval.lplt eq 1 then begin
	    printf, pipe, outval.ldef
	    if outval.ldef eq 1 then begin
		printf, pipe, outval.xmin
		printf, pipe, outval.xmax
		printf, pipe, outval.ymin
		printf, pipe, outval.ymax
	    endif
	endif

    endif else if lpend eq 3 then begin

 	printf, pipe, outval.texout
	if outval.texout eq 1 then begin
	    outval.texdef = outval.texdsn
	    printf, pipe, outval.texdsn
	    printf, pipe, header
	endif

 	printf, pipe, outval.maiout
	if outval.maiout eq 1 then begin
	    outval.maidef = outval.maidsn
	    printf, pipe, outval.maidsn
	endif

 	printf, pipe, outval.atoout
	if outval.atoout eq 1 then begin
	    outval.atodef = outval.atodsn
	    printf, pipe, outval.atodsn
	endif

    endif

LABELEND:

END
