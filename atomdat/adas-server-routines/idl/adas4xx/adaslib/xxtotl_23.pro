;----------------------------------------------------------------------
;+
; PROJECT    :  ADAS
;
; NAME       :  xxtotl_23
;
; PURPOSE    :  Returns the total iionisation rates from an adf23 file.
;
; INPUTS     :  fulldata - Structure containing adf23 data
;
; OUTPUTS    :  szd_total : Structure containing ionisation rates:
;
;
; The fulldata structure is defined as:
;
;    seq          =  iso-electronic sequence symbol
;    iz0          =  nuclear charge
;    iz           =  ionising ion charge
;    iz1          =  ionised ion charge (=iz+1)
;    ctype        =  adf23 file resol. ('ca', 'ls' or 'ic')
;    bwno_f       =  ionis. poten. of ionised ion (cm-1)
;    nlvl_f       =  number of levels of ionised ion
;    lmet_f       =  1 - ionised metastables marked
;                    0 -                     unmarked
;    lcstrg_f     =  1 - standard config strings for ionised ion states
;                    0 - cannot be determined
;    ia_f()       =   index of ionised ion levels
;                       (ndlev) : ionised ion level index
;    code_f()     =  met. or excit. DR parent marker (* or #)
;                       (ndlev) : ionised ion level index
;    cstrga_f()   =  ionised ion configuration strings
;                       (ndlev) : ionised ion level index
;    isa_f()      =  ionised ion level multiplicity
;                       (ndlev) : ionised ion level index
;    ila_f()      =  ionised ion total orb. ang. mom.
;                       (ndlev) : ionised ion level index
;    xja_f()      =  ionised ion level (stat wt-1)/2
;                       (ndlev) : ionised ion level index
;    wa_f()       =  ionised ion level wave number (cm-1)
;                       (ndlev) : ionised ion level index
;    nmet_f       =  number of ionised ion metastables
;    imeta_f()    =  pointers to ionised metastables in full ionised ion state list
;                       (ndlev) : ionised ion level index
;    bwno_i       =  ionis. poten. of ionising ion (cm-1)
;    nlvl_i       =  number of levels of ionising ion
;    lmet_i       =  1 - ionising metastables marked
;                    0 -                     unmarked
;    lcstrg_i     =  1 - standard config strings for ionising ion states
;                    0 - cannot be determined
;    ia_i()       =   index of ionising ion levels
;                       (ndlev) : ionising ion level index
;    code_i()     =  met. or excit. DR parent marker (* or #)
;                       (ndlev) : ionising ion level index
;    cstrga_i()   =  ionising ion configuration strings
;                       (ndlev) : ionising ion level index
;    isa_i()      =  ionising ion level multiplicity
;                       (ndlev) : ionising ion level index
;    ila_i()      =  ionising ion total orb. ang. mom.
;                       (ndlev) : ionising ion level index
;    xja_i()      =  ionising ion level (stat wt-1)/2
;                       (ndlev) : ionising ion level index
;    wa_i()       =  ionising ion level wave number (cm-1)
;                       (ndlev) : ionising ion level index
;    nmet_i       =  number of ionising ion metastables
;    imeta_i()    =  pointers to ionising metastables in full ionising ion state list
;                       (ndlev) : ionised ion level index
;    nte_ion()    =  number of temperatures for direct ionisation
;                    data for initial metastable block
;                       (ndmet) : ionising ion metastable index
;    tea_ion(,)   =  temperatures (K) for direct ionisation
;                    data for initial metastable blockdblarr
;                       (ndmet) : ionising ion metastable index
;                       (ndtem) : temperature index
;    lqred_ion()  =  1 - direct ionisation data line present for ionised ion state
;                    0 - missing
;                       (ndmet) : ionising ion metastable index
;                       (ndlev) : ionised ion state index
;    qred_ion(,,) =  reduced direct ionisation rate coefficients
;                       (ndmet) : ionising ion metastable index
;                       (ndlev) : ionised ion state index
;                       (ndtem) : temperature index
;    nf_a()       =  number of Auger ionised ion final states
;                       (ndmet) : ionising ion metastable index
;    indf_a(,)    =  Auger ionised ion final state
;                       (ndmet) : ionising ion metastable index
;                       (ndlev) : final state index
;    lyld_a(,)    =  1 - Auger data for ionising ion excited state present
;                    0 - No Auger data
;                       (ndmet) : ionising ion metastable index
;                       (ndlev) : initial state index
;    yld_a(,,)    =  Auger yields
;                       (ndmet) : ionising ion metastable index
;                       (ndlev) : ionising ion excited state index
;                       (ndlev) : ionised ion excited state index
;    nte_exc()    =  number of temperatures for excitation
;                       (ndmet) : ionising ion metastable index
;    tea_exc(,)   =  temperatures (K) for direct excitation data for
;                    initial metastable block
;                       (ndmet) : ionising ion metastable index
;                       (ndtem) : temperature index
;    lqred_exc(,) =  1 - direct excitation data line present for excited ion state
;                    0 - no data
;                       (ndmet) : ionising ion metastable index
;                       (ndlev) : ionising ion excited state index
;    qred_exc(,,) =  reduced excitation rate coefficients
;                       (ndmet) : ionising ion metastable index
;                       (ndlev) : ionising ion excited state index
;                       (ndtem) : temperature index
;    l_ion()      =  1 - ionisation data present for metastable
;                    0 - not present
;                       (ndmet) : ionising ion metastable index
;    l_aug()      =  1 - Auger data present for metastable
;                    0 - not present
;                       (ndmet) : ionising ion metastable index
;    l_exc()      =  1 - excitation data present for metastable
;                    0 - not present
;                       (ndmet) : ionising ion metastable index
;
; The szd_total structure is defined as:
;
;    q_ion(,,)    = direct ionisation rate coefficients multipler
;                       (ndmet) : ionising ion metastable index
;                       (ndlev) : ionised ion state index
;                       (ndtem) : temperature index
;    is_q_ion(,,) = scaling exponent for direct ionisation coefficient
;                       (ndmet) : ionising ion metastable index
;                       (ndlev) : ionised ion state index
;                       (ndtem) : temperature index
;    q_exc(,,)    = excited ionisation rate coefficients multipler
;                       (ndmet) : ionising ion metastable index
;                       (ndlev) : excited ionising ion state index
;                       (ndtem) : temperature index
;    is_q_exc(,,) = scaling exponent for excited ionisation coefficient
;                       (ndmet) : ionising ion metastable index
;                       (ndlev) : excited ionising ion state index
;                       (ndtem) : temperature index
;    qtot(,,)     = total ionisation rate multipler
;                       (ndmet) : ionising ion metastable index
;                       (ndlev) : ionised ion metastable index
;                       (ndtem) : temperature index
;    is_qtot(,,)  = scaling exponent for total ionisation coefficient
;                       (ndmet) : ionising ion metastable index
;                       (ndlev) : ionised ion metastable index
;                       (ndtem) : temperature index
;
;
;
; NOTES      :  Calls fortran code via C wrapper, see:
;                       xxtotl_23_if.c
;                       xxtotl_23_calc.for
;
; AUTHOR     :  Martin O'Mullane
;
; DATE       :  22-05-2008
;
; MODIFIED:
;       1.1     Martin O'Mullane
;                - First version.
;
; VERSION:
;       1.1    22-05-2008
;
;-
;----------------------------------------------------------------------


PRO xxtotl_23, fulldata = fulldata, szd_total = szd_total

if n_elements(fulldata) EQ 0 then message, 'An input structure must be given'


; Parameters for storage

ndlev  = 100L
ndmet  = 10L
ndtem  = 50L
ndtext = 80L

; Allocate space - as in xxdata_23. Replace with fulldata value just for
; parameters used in xxtotl_23.
;
; Note that fulldata only fills arrays with meaningful data and trims
; their size from that expected by fortran.

seq       =  bytarr(2)
iz0       =  0L
iz        =  0L
iz1       =  0L
ctype     =  bytarr(2)
bwno_f    =  0.0D0
nlvl_f    =  0L
lmet_f    =  0L
lcstrg_f  =  0L
ia_f      =  lonarr(ndlev)
code_f    =  bytarr(1,ndlev)
cstrga_f  =  bytarr(28,ndlev)
isa_f     =  lonarr(ndlev)
ila_f     =  lonarr(ndlev)
xja_f     =  dblarr(ndlev)
wa_f      =  dblarr(ndlev)
nmet_f    =  0L
imeta_f   =  lonarr(ndmet)
bwno_i    =  0.0D0
nlvl_i    =  0L
lmet_i    =  0L
lcstrg_i  =  0L
ia_i      =  lonarr(ndlev)
code_i    =  bytarr(1,ndlev)
cstrga_i  =  bytarr(28,ndlev)
isa_i     =  lonarr(ndlev)
ila_i     =  lonarr(ndlev)
xja_i     =  dblarr(ndlev)
wa_i      =  dblarr(ndlev)
nmet_i    =  0L
imeta_i   =  lonarr(ndmet)
nte_ion   =  lonarr(ndmet)
tea_ion   =  dblarr(ndmet,ndtem)
lqred_ion =  lonarr(ndmet,ndlev)
qred_ion  =  dblarr(ndmet,ndlev,ndtem)
nf_a      =  lonarr(ndmet)
indf_a    =  lonarr(ndmet,ndlev)
lyld_a    =  lonarr(ndmet,ndlev)
yld_a     =  dblarr(ndmet,ndlev,ndlev)
nte_exc   =  lonarr(ndmet)
tea_exc   =  dblarr(ndmet,ndtem)
lqred_exc =  lonarr(ndmet,ndlev)
qred_exc  =  dblarr(ndmet,ndlev,ndtem)
l_ion     =  lonarr(ndmet)
l_aug     =  lonarr(ndmet)
l_exc     =  lonarr(ndmet)
ntext     =  0L
ctext     =  bytarr(80, ndtext)

is_q_ion  = lonarr(ndmet,ndlev,ndtem)
is_q_exc  = lonarr(ndmet,ndlev,ndtem)
is_qtot   = lonarr(ndmet,ndmet,ndtem)
q_ion     = dblarr(ndmet,ndlev,ndtem)
q_exc     = dblarr(ndmet,ndlev,ndtem)
qtot      = dblarr(ndmet,ndmet,ndtem)

; Replace data in allocated variables by the real values in fulldata
;  - but only fill variables used in xxtotl_23.for.

lmet_f  =  fulldata.lmet_f
lmet_i  =  fulldata.lmet_i
nmet_f  =  fulldata.nmet_f
nmet_i  =  fulldata.nmet_i
nlvl_f  =  fulldata.nlvl_f
nlvl_i  =  fulldata.nlvl_i

bwno_i = fulldata.bwno_i
nte_ion[0:nmet_i-1]                              = fulldata.nte_ion
tea_ion[0:nmet_i-1,0:max(nte_ion)-1]             = fulldata.tea_ion
l_exc[0:nmet_i-1]                                = fulldata.l_exc
nte_exc[0:nmet_i-1]                              = fulldata.nte_exc
imeta_i[0:nmet_i-1]                              = fulldata.imeta_i
imeta_f[0:nmet_f-1]                              = fulldata.imeta_f
wa_i[0:nlvl_i-1]                                 = fulldata.wa_i
wa_f[0:nlvl_f-1]                                 = fulldata.wa_f
xja_i[0:nlvl_i-1]                                = fulldata.xja_i
qred_ion[0:nmet_i-1,0:nlvl_f-1,0:max(nte_ion)-1] = fulldata.qred_ion
lyld_a[0:nmet_i-1,0:nlvl_i-1]                    = fulldata.lyld_a
nf_a[0:nmet_i-1]                                 = fulldata.nf_a
indf_a[0:nmet_i-1,0:nlvl_i-1]                    = fulldata.indf_a
yld_a[0:nmet_i-1,0:nlvl_i-1,0:nlvl_f-1]          = fulldata.yld_a

if total(fulldata.l_exc) GT 0 then begin
   tea_exc[0:nmet_i-1,0:max(nte_exc)-1]             = fulldata.tea_exc
   qred_exc[0:nmet_i-1,0:nlvl_i-1,0:max(nte_exc)-1] = fulldata.qred_exc
endif


; Get Path

adasfort = getenv('ADASFORT')
if adasfort eq '' then message, 'Cannot locate ADAS binaries'

; Call wrapper routine

res = call_external(adasfort+'/xxtotl_23_if.so','xxtotl_23',       $
                    ndlev    , ndmet     , ndtem     , ndtext   ,  $
                    seq      , iz0       , iz        , iz1      ,  $
                    ctype    ,                                     $
                    bwno_f   , nlvl_f    , lmet_f    , lcstrg_f ,  $
                    ia_f     , code_f    , cstrga_f  ,             $
                    isa_f    , ila_f     , xja_f     , wa_f     ,  $
                    nmet_f   , imeta_f   ,                         $
                    bwno_i   , nlvl_i    , lmet_i    , lcstrg_i ,  $
                    ia_i     , code_i    , cstrga_i  ,             $
                    isa_i    , ila_i     , xja_i     , wa_i     ,  $
                    nmet_i   , imeta_i   ,                         $
                    nte_ion  , tea_ion   , lqred_ion , qred_ion ,  $
                    nf_a     , indf_a    , lyld_a    , yld_a    ,  $
                    nte_exc  , tea_exc   , lqred_exc , qred_exc ,  $
                    l_ion    , l_aug     , l_exc     ,             $
                    ntext    , ctext     ,                         $
                    q_ion    , is_q_ion  , q_exc     , is_q_exc ,  $
                    qtot     , is_qtot                             $
                   )


; Return the totals - assume no regridding was required (xxtotl_23.for will
; fail if it is required).


if arg_present(szd_total) then begin

   nte = max(nte_ion)

   szd_total = { te        :  reform(tea_ion[0,0:nte-1]),                $
                 q_ion     :  q_ion[0:nmet_i-1, 0:nlvl_f-1, 0:nte-1],    $
                 is_q_ion  :  is_q_ion[0:nmet_i-1, 0:nlvl_f-1, 0:nte-1], $
                 q_exc     :  q_exc[0:nmet_i-1, 0:nlvl_i-1, 0:nte-1],    $
                 is_q_exc  :  is_q_exc[0:nmet_i-1, 0:nlvl_i-1, 0:nte-1], $
                 qtot      :  qtot[0:nmet_i-1, 0:nmet_f-1, 0:nte-1],     $
                 is_qtot   :  is_qtot[0:nmet_i-1, 0:nmet_f-1, 0:nte-1]   }


endif

END
