;-----------------------------------------------------------------------------
;+
; PROJECT    :  ADAS
;
; NAME       :  preview_natural_partition
;
; PURPOSE    :  Preview the natural partition for an element. Allows plots
;               of fractional change in ionisation potential, normalised
;               ionisation balance within a partition component, root
;               fractional abundance and superstage fractional abundance.
;
; ARGUMENTS  :  All output arguments will be defined appropriately.
;
;               NAME      I/O    TYPE    DETAILS
; REQUIRED   :  z0         I     int     nuclear charge of required element
;
; OPTIONAL      sigma      I     float   divisor of running mean of fractional
;                                        ionisation potential use for
;                                        superstage grouping (typically 1.5,
;                                        larger for more superstages).
;               te_min     I     float   minimum temperature for plot
;               te_max     I     float   maximum temperature for plot
;               te_plot    I     float   plot at this temperature
;               num_te     I     float   number of temperatures (default 400)
;               partition()O     string  array holding partition for inclusion
;                                        in adas416 script file.
;               plot_type  I     int     plot number:
;                                         1 = frac. ionis pot. curve
;                                         2 = normalised partition component
;                                             fractional abundances
;                                         3 = root fractional abundances
;                                         4 = natural partition frac. abunds.
;               frac       O     struct
;                                    te       : temperatures
;                                    ion_norm : normalised frac. abund.
;                                    ion_all  : frac. abund. of all ions
;                                    ion_part : frac. abund. of partition
;
; KEYWORDS   :  /kelvin  interpret temperatures as Kelvin
;               /help    if specified this comment
;                        section is written to screen.
;
;
; ROUTINES:
;          NAME                  TYPE       COMMENT
;          xxelem                adas       obtain an element name
;          xxesym                adas       returns an element chemical symbol
;          preview_fig           internal   plots figures of types 1-4
;
;
; CATEGORY:
;       Adas system.
;
; WRITTEN:
;       H. P. Summers, University of Strathclyde,29-06-07.

;
; MODIFIED:
;       1.1     H. P. Summers
;                 - First version.
;       1.2     Martin O'Mullane
;                 - Return fractional abundance if requested.
;
; VERSION:
;       1.1   03-07-07
;       1.2   14-10-2010
;
;-
;-----------------------------------------------------------------------------

PRO preview_fig,  z0            = z0,                              $
                  elem_name     = elem_name,                       $
                  elem_symb     = elem_symb,                       $
                  np            = np,                              $
                  ip0           = ip0,                             $
                  ip1           = ip1,                             $
                  ipm           = ipm,                             $
                  dfeiona       = dfeiona,                         $
                  avera         = avera,                           $
                  nte           = nte,                             $
                  tea           = tea,                             $
                  iprint        = iprint,                          $
                  abun_norm     = abun_norm,                       $
                  abun_base     = abun_base,                       $
                  abun_part     = abun_part,                       $
                  plot_no       = plot_no,                         $
                  mul           = mul

; Te units

if mul EQ 1 then te_str = 'Te (K)' else te_str = 'Te (eV)'

; Plot according to choice

case plot_no of

    1: begin
         xa=fltarr(2)
         ya=fltarr(2)

         ixa=indgen(z0-1)
         plot,ixa,dfeiona,xtitle='Ion charge',$
                         ytitle='Fractional ionization potential change', $
                         yrange=[0.0,1.5]

         for i=0,np-1 do begin
           xa[0]=ip0[i]
           xa[1]=ip1[i]
           ya[0]=1.25
           ya[1]=1.25
           oplot,xa,ya
         endfor
         xyouts,z0/2.5,1.4,elem_name + '  z0 = '+ string(z0,format='(i2)')

       end

    2: begin

         ixa=indgen(z0+1)

         ymin=1.0e-8
         ymax=10.0

         xval=fltarr(2)
         yval=fltarr(2)
         xval[0]=ixa(0)
         xval[1]=ixa(0)
         yval[0]=ymin
         yval[1]=max(abun_norm[0,iprint],ymin)

         te=tea[iprint]

         plot_io,xval,yval,xtitle='ion charge',ytitle='partition fractional abundances', $
                 xrange=[0,z0],yrange=[1.0e-8,10.0]
         xyouts,z0/3,10.0,elem_name + '  z0 = '+ string(z0,format='(i2)')
         xyouts,3*(z0/4),10.0, te_str + ' = ' + string(te,format='(e10.2)')

         for i=1,z0 do begin
           xval[0]=ixa[i]
           xval[1]=ixa[i]
           yval[0]=1.0e-8
           yval[1]=abun_norm[i,iprint]
           if yval[1] lt 1.0e-8 then yval[1]=1.0e-8

           oplot,xval,yval
         endfor

         for ip=0,np-1 do begin
           xval[0]=ixa[ip0[ip]]
           xval[1]=ixa[ip1[ip]]
           yval[0]=5.0
           yval[1]=5.0
           oplot,xval,yval
         endfor

       end

    3: begin

         ia=intarr(3)

         yval=fltarr(nte)
         frac=fltarr(z0+1)

         ixa=indgen(z0+1)

         yval=abun_base[0,*]
         plot_oo,tea/mul,yval,xtitle=te_str, $
         ytitle='fractional abundances',yrange=[1.0e-15,1.0e5]
         xyouts,tea[nte/3]/mul,3.0,elem_name + '  z0 = '+ string(z0,format='(i2)') + $
                         '  all stages'

         for i=1,z0 do begin
           yval=abun_base[i,*]
           oplot,tea/mul,yval
         endfor

       end

    4: begin

         ia=intarr(3)

         yval=fltarr(nte)
         frac=fltarr(np)

         ixa=indgen(np)


         yval=abun_part[0,*]
         plot_oo,tea/mul,yval,xtitle=te_str, $
         ytitle='fractional abundances',yrange=[1.0e-15,10.0]
         xyouts,tea[nte/3]/mul,3.0,elem_name + '  z0 = ' + string(z0,format='(i2)') + $
                          ' with '+ string(np,format='(i2)') + ' partitions'

         for i=1,np-1 do begin
           yval=abun_part[i,*]
           oplot,tea/mul,yval
         endfor

       end

endcase

END
;------------------------------------------------------------------------------



PRO preview_natural_partition,  z0        = z0,             $
                                sigma     = sigma,          $
                                plot_type = plot_type,      $
                                te_min    = te_min,         $
                                te_max    = te_max,         $
                                te_plot   = te_plot,        $
                                num_te    = num_te,         $
                                partition = partition,      $
                                frac      = frac,           $
                                kelvin    = kelvin,         $
                                help      = help

; If asked for help

if keyword_set(help) then begin 
   doc_library, 'preview_natural_partition' 
   return
endif

;----------------------------------------------------------------------
; Check inputs
;----------------------------------------------------------------------

    if n_elements(z0) EQ 0 then message, 'z0 must be assigned'
    if n_elements(z0) GT 1 then message, 'only one z0 allowed'
    if z0 LT 6 then message, 'z0 must be greater than 6'

    if n_elements(sigma) EQ 0 then begin
       sigma = 1.5d0
       message, 'sigma set to ' + string(sigma), /continue
    endif

    if n_elements(plot_type) GT 0 then  plot_no =  long(plot_type)

;----------------------------------------------------------------------
; set up te grid - code works in Kelvin
;----------------------------------------------------------------------

    if keyword_set(kelvin) then begin
        mul   = 1.0
        temin = 1.0d4
        temax = 1.0D10
     endif else begin
        mul   = 11605.0
        temin = 1.0d4 / mul
        temax = 1.0d10 / mul
     endelse

    if n_elements(te_min) NE 0 then temin=te_min * mul
    if n_elements(te_max) NE 0 then temax=te_max * mul
    if n_elements(num_te) EQ 0 then nte = 400 else nte = num_te

    tea = adas_vector(low=temin, high=temax, num=nte)

    if n_elements(te_plot) EQ 0 then iprint = nte / 2 $
                                else iprint = i4indf(tea, te_plot*mul)

;----------------------------------------------------------------------
; set up element
;----------------------------------------------------------------------

    xxelem, z0, elem_name
    elem_symb  = xxesym(z0, /lower)

;----------------------------------------------------------------------
; create base partition string with new line separator '*'
;----------------------------------------------------------------------

    p_string ="//#01/"

    for i=0,z0 do begin

      if i le 9 then begin
         p_string_cmpt="p0"+string(i,format='(i1)')+"/ 0"+ $
                  string(i,format='(i1)')+"/"
      endif else begin
         p_string_cmpt=+"p"+string(i,format='(i2)')+"/ "+ $
                  string(i,format='(i2)')+"/"

      endelse

      j=0
      j=i/9
      k=i-9*j

      if k eq 0 and j gt 0 then begin
         p_string=p_string+'*      '+p_string_cmpt
      endif else begin
         p_string=p_string+p_string_cmpt
      endelse

    endfor

;----------------------------------------------------------------------
; print base partition string tidly
;----------------------------------------------------------------------

    cptn_stack = strsplit(p_string,'*',/extract)
    ncptn_stack=n_elements(cptn_stack)

    ncptn_stack_root = ncptn_stack

;----------------------------------------------------------------------
; obtain element ionisation potentials
;----------------------------------------------------------------------

    read_adf00,z0=z0,z1=z1,ionpot=eiona,/all
    dfeiona = 2.0 * (eiona - shift(eiona,1)) / (shift(eiona,1) + eiona)
    dfeiona = dfeiona[1:*]

;----------------------------------------------------------------------
; try to find the 'natural partition in terms of deviations from a
; a running mean (excluding spike values) of the dfeiona.  Take sigma
; as the deviation -  i.e. individulise if (dfeion-mean) > mean/sigma
;----------------------------------------------------------------------

    avera_first=fltarr(z0-1)
    avera_first[0]=0.0
    avera_first[1]=0.0
    i=1
    while (i le z0-3) do begin
       i=i+1
       max_a=min([i+3,z0-2])
       min_a=max([max_a-6,0])

       aver=mean(dfeiona[min_a:max_a])
       avera_first[i]=aver
    endwhile
    avera_first[0]=avera_first[2]
    avera_first[1]=avera_first[2]
    index=where(dfeiona-avera_first gt avera_first/sigma,complement=index_cmpl)

    avera=fltarr(z0-1)
    avera[0]=0.0
    avera[1]=0.0
    i=1
    while (i le z0-3) do begin
       i=i+1
       max_a=min([i+3,z0-2])
       min_a=max([max_a-6,0])

       index1=where(index_cmpl ge min_a and index_cmpl le max_a)
       aver=mean(dfeiona[index_cmpl[index1]])
       avera[i]=aver
    endwhile
    avera[0]=avera[2]
    avera[1]=avera[2]

    ip_nat=[0,1]
    i=1
    while (i le z0-3) do begin
       i=i+1
       max_a=min([i+3,z0-2])
       min_a=max([max_a-6,0])
       aver=mean(dfeiona[min_a:max_a])
       if ((dfeiona(i)-avera(i)) ge avera(i)/sigma) then begin
          ip_nat=[ip_nat,i,i+1]
          i=i+1
       endif
    endwhile

    ip_nat=[ip_nat,z0-1,z0]
    np_nat=n_elements(ip_nat)


;----------------------------------------------------------------------
; now assemble the partition
;----------------------------------------------------------------------

    ip0=intarr(np_nat+20)
    ip1=intarr(np_nat+20)

    np=0
    ip0[np]=ip_nat[0]
    for i=0,np_nat-2 do begin
       if(ip_nat[i+1] eq ip_nat[i]+1) then begin
         ip1[np]=ip_nat[i]
         np=np+1
         ip0[np]=ip_nat[i+1]
       endif else begin
         ip1[np]=ip_nat[i]
         np=np+1
         ip0[np]=ip_nat[i]+1
         ip1[np]=ip_nat[i+1]-1
         np=np+1
         ip0[np]=ip_nat[i+1]
       endelse
    endfor
    ip1[np]=ip_nat[np_nat-1]
    np=np+1

    ip0=[ip0[0],ip0[where(ip0 gt 0)]]
    ip1=[ip1[0],ip1[where(ip1 gt 0)]]

;----------------------------------------------------------------------
; create natural partition string
;----------------------------------------------------------------------

    p_natural="//#02/"

    for i=0,np-1 do begin

      if i le 9 then begin
         p_natural=p_natural+"p0"+string(i,format='(i1)')+"/"
      endif else begin
         p_natural=p_natural+"p"+string(i,format='(i2)')+"/"
      endelse

      for k=ip0[i],ip1[i] do begin

        if k le 9 then begin
           p_natural=p_natural + " 0" +string(k,format='(i1)')
        endif else begin
           p_natural=p_natural + " " + string(k,format='(i2)')
        endelse

      endfor

      p_natural=p_natural +"/"

    endfor

;----------------------------------------------------------------------
; print natural partition string tidly
;----------------------------------------------------------------------

    maxlen = 68

    markers = strsplit(p_natural,'p')

    ilow = 1
    ihigh = ilow
    p_print = strmid(p_natural,0,markers[ilow]-1)
    for ip=1,np-1 do begin
      if (markers[ip]-markers[ilow] gt maxlen) then begin
          str_low=strmid(p_natural,markers[ilow]-1,markers[ip-1]-markers[ilow])
          for is=1,4 do begin
            if strlen(str_low) gt is*maxlen then begin
               mark = strpos(str_low,' ',is*maxlen)
               if mark gt -1 then begin
                  str_low=strmid(str_low,0,mark)+ '*          ' +       $
                          strmid(str_low,mark)
               endif
            endif
          endfor
          p_print=p_print + str_low+ "*      "
          ilow=ip-1
      endif
    endfor
    str_low=strmid(p_natural,markers[ilow]-1)
    for is=1,4 do begin
      if strlen(str_low) gt is*maxlen then begin
        mark = strpos(str_low,' ',is*maxlen)
        if mark gt -1 then begin
            str_low=strmid(str_low,0,mark)+ '*          ' + strmid(str_low,mark)
        endif
      endif
    endfor
    p_print=p_print + str_low

    cptn_stack = [cptn_stack,strsplit(p_print,'*',/extract)]
    ncptn_stack=n_elements(cptn_stack)

; Return partition as string array if required

    if arg_present(partition) then begin
       partition = [cptn_stack[ncptn_stack_root:ncptn_stack-1], $
                    cptn_stack[0:ncptn_stack_root-1]]
    endif

;----------------------------------------------------------------------
; set up vectors of ionisation and recombination rate coefficients
; sa(0) contains S(z=0-->z=1) aa(0) contains a(z=1-->z=0)
;----------------------------------------------------------------------

    sa_abs     = dblarr(z0,nte)
    aa_abs     = dblarr(z0,nte)
    sp_abs     = dblarr(z0,nte)
    ap_abs     = dblarr(z0,nte)
    abun_norm  = dblarr(z0+1,nte)
    abun_base  = dblarr(z0+1,nte)
    abun_part  = dblarr(np,nte)
    ipm        = intarr(np)

    for ite=0,nte-1 do begin
           te=tea[ite]
           im=0
           for i=0,z0-1 do begin

             sa_abs[i,ite]=2.0d-8*1.0d0*sqrt(te)*                       $
                           10.0d0^(-5040d0*eiona[i]/te)/eiona[i]^2
             aa_abs[i,ite]=2.1d-11*(float(i+1))*2/sqrt(te)
             if (sa_abs[i,ite] ge aa_abs[i,ite]) then im=im+1

           endfor

;-----------------------------------------------------------------------
;  Cycle through partitions to find dominant stage in each partition
;-----------------------------------------------------------------------

          for ip=0,np-1 do begin
            i0=ip0[ip]
            i1=ip1[ip]
            if (i0 lt i1) then begin
               im=i0
               for i=i0,i1-1 do begin
                 if (sa_abs[i,ite] ge aa_abs[i,ite]) then begin
                     im=im+1
                 endif
               endfor
               ipm[ip]=im
            endif else begin
                ipm[ip]=i0
            endelse
          endfor

;-----------------------------------------------------------------------
;  Cycle through partitions recurring up and down to find fractions
;  normalised within each partition group separately to unity
;-----------------------------------------------------------------------
          for ip=0,np-1 do begin
            i0=ip0[ip]
            i1=ip1[ip]
            im=ipm[ip]
            sum=1.0d0
            abun_norm(im,ite)=1.0d0
            if (i0 lt i1) then begin
               sum=0.0d0
               if ( im ne i0) then begin
                   for i=im-1,i0,-1 do begin
                       abun_norm[i,ite]=abun_norm[i+1,ite]*aa_abs[i,ite]/               $
                                   sa_abs[i,ite]
                   endfor
               endif
               if (im ne i1) then begin
                   for i=im+1,i1 do begin
                       abun_norm[i,ite]=abun_norm[i-1,ite]*sa_abs[i-1,ite]/     $
                                   aa_abs[i-1,ite]
                   endfor
               endif
               for i=i0,i1 do begin
                 sum=sum+abun_norm[i,ite]
               endfor
               for i=i0,i1 do begin
                 abun_norm[i,ite]=abun_norm[i,ite]/sum
                 if (abun_norm[i,ite] le 1.0d-72) then abun_norm[i,ite]=0.0d0
               endfor
            endif
          endfor

;-----------------------------------------------------------------------
;  Now fill the partition effective ionisation and  recombination coeffts
;-----------------------------------------------------------------------

          for ip=0,np-2 do begin
            i1=ip1[ip]
            i0=ip0[ip+1]
            sp_abs[ip,ite]=sa_abs[i1,ite]*abun_norm[i1,ite]
            ap_abs[ip,ite]=aa_abs[i1,ite]*abun_norm[i0,ite]
          endfor

      endfor


;-----------------------------------------------------------------------
;  Prepare the equilibrium fractional abundances
;-----------------------------------------------------------------------
      for ite=0,nte-1 do begin
;---------------------------------
;  Base fractional abundances
;---------------------------------
           im=0
           for i=0,z0-1 do begin
             if (sa_abs[i,ite] ge aa_abs[i,ite]) then begin
                 im=im+1
             endif
           endfor
           sum=0.0d0
           abun_base[im,ite]=1.0d0
           if (im ne 0) then begin
               for i=im-1,0,-1 do begin
                   abun_base[i,ite]=abun_base[i+1,ite]*             $
                                    aa_abs[i,ite]/sa_abs[i,ite]
               endfor
           endif
           if(im ne z0) then begin
               for i=im+1,z0 do begin
                   abun_base[i,ite]=abun_base[i-1,ite]*             $
                              sa_abs[i-1,ite]/aa_abs[i-1,ite]
               endfor
           endif
           for i=0,z0 do begin
             sum=sum+abun_base[i,ite]
           endfor
           for i=0,z0 do begin
             abun_base[i,ite]=abun_base[i,ite]/sum
             if (abun_base[i,ite] le 1.0d-72) then abun_base[i,ite]=0.0d0
           endfor

;---------------------------------
;  Partition fractional abundances
;---------------------------------
           im=0
           for i=0,np-2 do begin
             if (sp_abs[i,ite] ge ap_abs[i,ite]) then begin
                 im=im+1
             endif
           endfor
           sum=0.0d0
           abun_part[im,ite]=1.0d0
           if (im  ne 0) then begin
               for i=im-1,0,-1 do begin
                   abun_part[i,ite]=abun_part[i+1,ite]*             $
                                    ap_abs[i,ite]/sp_abs[i,ite]
               endfor
           endif
           if( im  ne np-1) then begin
               for i=im+1,np-1 do begin
                   abun_part[i,ite]=abun_part[i-1,ite]*             $
                             sp_abs[i-1,ite]/ap_abs[i-1,ite]
               endfor
           endif

           for i=0,np-1 do begin
             sum=sum+abun_part[i,ite]
           endfor
           for i=0,np-1 do begin
             abun_part[i,ite]=abun_part[i,ite]/sum
             if (abun_part[i,ite] le 1.0d-72) then abun_part[i,ite]=0.0d0
           endfor

      endfor

; Plot figure depending on chosen plot type

      if plot_no GT 0 then begin

         preview_fig,  z0            = z0,                              $
                       elem_name     = elem_name,                       $
                       elem_symb     = elem_symb,                       $
                       np            = np,                              $
                       ip0           = ip0,                             $
                       ip1           = ip1,                             $
                       ipm           = ipm,                             $
                       dfeiona       = dfeiona,                         $
                       avera         = avera,                           $
                       nte           = nte,                             $
                       tea           = tea,                             $
                       iprint        = iprint,                          $
                       abun_norm     = abun_norm,                       $
                       abun_base     = abun_base,                       $
                       abun_part     = abun_part,                       $
                       plot_no       = plot_no,                         $
                       mul           = mul
      endif

; Return the fractional abundances if requested

      if arg_present(frac) then begin
          
          frac = { te       : tea/mul,   $
                   ion_norm : abun_norm, $
                   ion_all  : abun_base, $
                   ion_part : abun_part  }
      endif


END

