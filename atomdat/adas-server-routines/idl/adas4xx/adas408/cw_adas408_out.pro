; Copyright (c) 1995, Strathclyde University.
; SCCS Info : Module @(#)$Header: /home/adascvs/idl/adas4xx/adas408/cw_adas408_out.pro,v 1.2 2004/07/06 12:47:55 whitefor Exp $ Date $Date: 2004/07/06 12:47:55 $
;+
; PROJECT:
;       ADAS IBM MVS to   UNIX conversion
;
; NAME:
;	CW_ADAS408_OUT()
;
; PURPOSE:
;	Produces a widget for ADAS408 output options.
;
; EXPLANATION:
;	This function declares a compound widget consisting of an input
;	file name comments browser widget, an
;	editable text widet for the passing file year, a label widget
;	diplaying the total master passing file name as it is changed and
;	an output file widget cw_adas_outfile.pro.  The text output
;	file specified in this widget is for tabular (paper.txt)
;	output.  This widget also includes a button for browsing the comments
;       from the input dataset, a 'Cancel' button , a 'Done' button
;	and an 'Escape to series menu' button. This latter is represented by 
;       a bitmapped button.
;  	This widget only
;	handles events from the 'Done', 'Cancel' and 'Escape to series 
;	menu' buttons.
;
; USE:
;	This widget is specific to ADAS408, see adas408_out.pro	for use.
;
; INPUTS:
;	PARENT	- Long integer; ID of parent widget.
;
;       BITFILE - String; the path to the dirctory containing bitmaps
;                 for the 'escape to series menu' button.
;
; OPTIONAL INPUTS:
;	None.
;
; OUTPUTS:
;	The return value of this function is the ID of the declared widget.
;
; OPTIONAL OUTPUTS:
;	None.
;
; KEYWORD PARAMETERS:
;
;	DSFULL	- The input file name
;
;	SYM	- The input file element symbol
;
;	VALUE	- A structure which determines the initial settings of
;		  the output options widget.
;		  The default value is;
;
;		      { 
;			YEAR:0,  PASDSN:'XXX00#'+SYM+'.pass',		$
;			TEXOUT:0, TEXAPP:-1, 				$
;			TEXREP:0, TEXDSN:'', 				$
;			TEXDEF:'',TEXMES:'', SYMBOL:''			$
;		      }
;			YEAR	Integer; Current entered year
;			PASDSN	String;  Master passing file name
;			TEXOUT	Integer; Activation button 1 on, 0 off
;			TEXAPP	Integer; Append button 1 on, 0 off, -1 no button
;			TEXREP	Integer; Replace button 1 on, 0 off, -1 no button
;			TEXDSN	String;  Output file name
;			TEXDEF	String;  Default file name
;			TEXMES	String;  file name error message
;
;
;
;	UVALUE	- A user value for this widget.
;
;	FONT	- String; the name of a font to be used for all text in
;		  this widget.  Default to system default font.
;
; CALLS:
;	CW_ADAS_DSBR	Input dataset name and comments browse button.
;	CW_ADAS_OUTFILE	Output file name entry widget.
;	See side effects for other related widget management routines.
;
; SIDE EFFECTS:
;
; 	Two other routines are included in this file
;	OUT408_GET_VAL()
;	OUT408_EVENT()
;
; CATEGORY:
;	Compound Widget
;
; WRITTEN:
;       William Osborn, Tessella Support Services plc, 15-Apr-1996
;
; MODIFIED:
;	1.1	William Osborn
;		First release
;	1.2	William Osborn
;		Removed xsize=n keyword from widget_label command.
;		The dynlabel procedure does not override these
;		assignments and they were taken as being values in pixels
;		on some machines.
;
; VERSION:
;	1.1	15-04-96
;	1.2	01-08-96
;
;-
;-----------------------------------------------------------------------------

FUNCTION out408_get_val, id

COMMON cw_out407_blk, sym, fname

                ;**** Return to caller on error ****

    ON_ERROR, 2

                ;**** Retrieve the state ****

    first_child=widget_info(id, /child)
    widget_control, first_child, get_uvalue=state

		;**** Get text output settings ****

    widget_control, state.paperid, get_value=papos

    os = { out408_set, 							$
	   YEAR		:	state.year,				$
	   DSNPAS	:	state.dsnpas,				$
	   TEXOUT	:	papos.outbut,  				$
	   TEXAPP	:	papos.appbut, 		 		$
	   TEXREP	:	papos.repbut,  				$
	   TEXDSN	:	papos.filename, 	 		$
	   TEXDEF	:	papos.defname, 				$
	   TEXMES	:	papos.message,				$
	   PASSPATH	:	state.passpath				}
                ;**** Return the state ****

    widget_control, first_child, set_uvalue=state, /no_copy

    RETURN, os 

END

;-----------------------------------------------------------------------------

FUNCTION out408_event, event

COMMON cw_out407_blk, sym, fname

                ;**** Base ID of compound widget ****

    parent = event.handler

		;**********************************************
                ;**** Retrieve the user value state        ****
		;**** Get id of first_child widget because ****
		;**** user value "state" is stored there   ****
		;**********************************************

    first_child = widget_info(parent, /child)
    widget_control, first_child, get_uvalue=state, /no_copy

		;*********************************
		;**** Clear previous messages ****
		;*********************************

    widget_control, state.messid, set_value=' '

                ;************************
                ;**** Process events ****
                ;************************

    CASE event.id OF

		;***********************
		;**** Cancel button ****
		;***********************

        state.cancelid: new_event = {ID:parent, TOP:event.top, 		$
				    HANDLER:0L, ACTION:'Cancel'}

		;*********************
		;**** Done button ****
		;*********************

    	state.doneid: begin

		;****************************************************
                ;**** Return the state before checking can start ****
		;**** with the get_value keyword.                ****
		;****************************************************

            widget_control, first_child, set_uvalue=state, /no_copy

		;***************************************
		;**** Check for errors in the input ****
		;***************************************

	    error = 0

		;**** Get a copy of the widget value ****

	    widget_control, event.handler, get_value=os
	
                ;**** Retrieve the state   ****

            widget_control, first_child, get_uvalue=state, /no_copy

		;**** Check for widget error messages ****

	    if os.texout eq 1 and strtrim(os.texmes) ne '' then begin
		error=1
		mess = '**** Error in text output settings ****'
	    endif

		;**** Check filename is OK ****
	    file_acc, os.passpath, fileexist
	    if fileexist eq 0 then begin
		error=1
		mess='**** Error: Passing file directory does not exist ****'
	    endif
	    if strpos(os.dsnpas,'XXX') eq -1 or strpos(os.dsnpas,'.pass')  $
			 ne strlen(os.dsnpas)-5 then begin
		error=1
		mess='**** Error: Passing file template not of correct form ****'
	    endif

		;**** Check the year ****
	    if error eq 0 then begin

	    	if os.year eq -1 then begin
		    error = 1
	  	    mess = '**** Error: Please enter a two-digit year ****'
		    widget_control, state.yearid, /input_focus
	    	endif else begin

		;**** Give an error if the file already exists with ****
		;**** XXX=ACD, unfiltered, for this year            ****

		    yearval = '00'+strtrim(string(os.year),2)
		    yearval = strmid(yearval,strlen(yearval)-2,2)

   	    	    dsnchk='ACD'+yearval+'#'+sym+'.pass'
		    file_acc, os.passpath+dsnchk, fileexist, read, write,$
		          execute, filetype
		    if fileexist eq 1 then begin
		    	error=1
		    	mess='**** Error: Passing files for this year'+ $
			     ' already exist ****'
		   	widget_control, state.yearid, /input_focus
		    endif

	    	endelse

	    endif

	    if error eq 1 then begin
	        widget_control,state.messid,set_value=mess
	        new_event = 0L
	    endif else begin
	        new_event = {ID:parent, TOP:event.top, 			$
                             HANDLER:0L, ACTION:'Done'}
	    endelse
	end

	state.yearid: begin

	    widget_control, state.yearid, get_value=yearval
	    yearval = yearval(0)
	    slen=strlen(yearval)
	    if num_chk(yearval,/integer) eq 0 and slen le 2 then begin
		if slen eq 0 then begin
		    yearval = '00'
		endif else if slen eq 1 then begin
		    yearval = '0'+yearval
		endif
		state.year = fix(yearval)
		state.dsnpas = 'XXX'+yearval+'#'+sym+'.'+fname+'.pass'
	    endif else begin
		state.year = -1
		state.dsnpas = 'XXX??#'+sym+'.'+fname+'.pass'
	    endelse
	    widget_control, state.dsnpasid, set_value=state.passpath+state.dsnpas

        end

	state.dsnpasid: begin
	    widget_control, state.dsnpasid, get_value=val
	    ;**** Find the name and path ****
	    val=strtrim(val(0),2)
	    i=0
	    while strpos(val,'/',i) ne -1 do i=i+1
	    if i ne 0 then begin
		state.passpath=strmid(val,0,i)
		state.dsnpas = strmid(val,i,strlen(val)-i)
	    endif else begin
		state.passpath=''
		state.dsnpas = val
	    endelse
	end

                ;**** Menu button ****

        state.outid: begin
            new_event = {ID:parent, TOP:event.top, HANDLER:0L,          $
                         ACTION:'Menu'}
        end

    	ELSE: new_event = 0L

    ENDCASE

                ;**** Return the state   ****

    widget_control, first_child, set_uvalue=state, /no_copy

    RETURN, new_event

END

;-----------------------------------------------------------------------------

FUNCTION cw_adas408_out, topparent, dsfull, fname, bitfile, sym,	$
		         VALUE=value, UVALUE=uvalue, FONT=font

COMMON cw_out407_blk, symcom, fnamecom

    IF (N_PARAMS() EQ 0) THEN MESSAGE, 'Must specify parent for cw_adas408_out'
    ON_ERROR, 2					;return to caller

		;**** Set defaults for keywords ****

    IF NOT (KEYWORD_SET(uvalue)) THEN uvalue = 0
    IF NOT (KEYWORD_SET(font)) THEN font = ''
    IF NOT (KEYWORD_SET(value)) THEN begin
	os = {out408_set,						$
			YEAR	 :	-1,				$
			DSNPAS	 :	'XXX??#'+sym+'.'+fname+'.pass',	$
			TEXOUT	 :	0, 				$
			TEXAPP	 :	-1, 				$
			TEXREP	 :	0, 				$
			TEXDSN	 :	'', 				$
                        TEXDEF	 :	'',				$
			TEXMES	 :	'',				$
			PASSPATH : 	'/'				}
    ENDIF ELSE BEGIN
	os = {out408_set,						$
			YEAR	 :  	value.year,			$
			DSNPAS	 : 	value.dsnpas,			$
			TEXOUT	 :	value.texout, 			$
			TEXAPP	 :	value.texapp, 			$
			TEXREP	 :	value.texrep, 			$
			TEXDSN	 :	value.texdsn, 			$
			TEXDEF	 :	value.texdef, 			$
			TEXMES	 :	value.texmes,			$
			PASSPATH :	value.passpath 			}
    ENDELSE

		;****    Reset os.dsnpas    ****

    if os.year ne -1 then begin
    	yearval = '00'+strtrim(string(os.year),2)
    	yearval = strmid(yearval,strlen(yearval)-2,2)
    	os.dsnpas = 'XXX'+yearval+'#'+sym+'.'+fname+'.pass'
    endif else os.dsnpas = 'XXX??#'+sym+'.'+fname+'.pass'

		;**** Set up the common block ****

    symcom = sym
    fnamecom = fname

		;**********************************************
		;**** Create the 408 Output options widget ****
		;**********************************************

		;**** create base widget ****

    parent = widget_base( topparent, UVALUE = uvalue, 			$
			EVENT_FUNC = "out408_event", 			$
			FUNC_GET_VALUE = "out408_get_val")

		;******************************************************
		;**** Create a dummy widget just to hold value of *****
		;**** "state" variable so as not to get confused  *****
		;**** with any other values. Adopt IDL practice   *****
		;**** of using first child widget                 *****
		;******************************************************

    first_child=widget_base(parent)

    cwid = widget_base(parent,/column)

		;**** Add dataset name and browse button ****

    rc = cw_adas_dsbr(cwid,dsfull,font=font)

		;**** Add a base for the year selection and passing filename

    dbase = widget_base(cwid,/column,/frame)

    ybase = widget_base(dbase,/row)
    y2base = widget_base(ybase,/column)
    lab = widget_label(y2base,value='Please enter the year number for ',  $
		font=font)
    lab = widget_label(y2base,value='master passing files (two-digits): ',$
			font = font)
    if os.year ne -1 then begin
	val=string(os.year,format='(I2)')
    endif else begin
	val =''
    endelse

    yearid = widget_text(ybase,/editable,value=val, xsize =3, ysize = 1,$
			 /all_events,font=font)

    lbase = widget_base(dbase,/row)
    lab = widget_label(lbase,value='Passing file template : ',$
			font=font)
    pfile = os.passpath+os.dsnpas
    dsnpasid = widget_text(lbase, /editable, /all_events, value=pfile, 	$
			   xsize=strlen(pfile), font=font) 

		;**** Widget for text output ****

    outfval = { OUTBUT:os.texout, APPBUT:-1, REPBUT:os.texrep, 		$
	        FILENAME:os.texdsn, DEFNAME:os.texdef, MESSAGE:os.texmes }
    base = widget_base(cwid,/row,/frame)
    paperid = cw_adas_outfile(base, OUTPUT='Text Output', 		$
                              VALUE=outfval, FONT=font)

		;**** Error message ****

    messid = widget_label(cwid,value=' ',font=font)

		;**** add the exit buttons ****

    base = widget_base(cwid,/row)
    menufile = bitfile + '/menu.bmp'
    read_X11_bitmap, menufile, bitmap1
    outid = widget_button(base, value=bitmap1)          ;menu button
    cancelid = widget_button(base, value='Cancel', font=font)
    doneid = widget_button(base, value='Done', font=font)
  
                ;*************************************************
		;**** create a state structure for the pop-up ****
		;**** window.                                 ****
                ;*************************************************

    new_state = { 	YEAR		:	os.year,		$
			DSNPAS		:	os.dsnpas,		$
			YEARID		:	yearid,			$
			DSNPASID	:	dsnpasid,		$
			PAPERID		:	paperid,		$
		  	CANCELID	:	cancelid, 		$
			DONEID		:	doneid, 		$
			OUTID		:	outid,			$
			MESSID		:	messid,			$
			PASSPATH	:	os.passpath		}

                ;**** Save initial state structure ****

    widget_control, first_child, set_uvalue=new_state, /no_copy
    RETURN, cwid

END

