; Copyright (c) 1995 Strathclyde University 
; SCCS info: Module @(#)$Header: /home/adascvs/idl/adas4xx/adas408/adas408_out.pro,v 1.5 2006/10/06 16:36:22 mog Exp $ Date $Date: 2006/10/06 16:36:22 $
;+
; PROJECT:
;       ADAS 
;
; NAME:
;	ADAS408_OUT
;
; PURPOSE: 
;
;
; EXPLANATION:
;       The user selects which adf11 output files are required. The
;       names are constructed from a template: year, element and
;       filter are chosen. 
;
;
; NOTES:
; 
;
;
; INPUTS:
;        outval  - Structure of output files,
;        bitfile - Directory of bitmaps for menu button
;
;
; OPTIONAL INPUTS:
;       None
;
; OUTPUTS:
;       The function returns a string rep
;           rep = 'CONTINUE' if everything is ok
;           rep = 'CANCEL' if there is a problem or indeed if the user
;                          closes the widget with the mouse. 
;           rep = 'MENU' if user want to exit to menu at this point
;
; OPTIONAL OUTPUTS:
;       None
;
; KEYWORD PARAMETERS:
;       FONT_LARGE - The name of a larger font e.g 'courier_bold14'
;
;       FONT_SMALL - The name of a smaller font e.g 'courier_bold12'
;
;
; CALLS:
;                            
;       ADAS408_OUT_MENU   : React to menu button event. The standard IDL
;                            reaction to button events cannot deal with
;                            pixmapped buttons. Hence the special handler.
;       ADAS408_OUT_EVENT  : Reacts to cancel and Done. Does the file
;                            existence error checking.
;
; SIDE EFFECTS:
;
; CATEGORY:
;	Widgets
;
; WRITTEN:
;       Martin O'Mullane
;
; MODIFIED:
;	1.1	Martin O'Mullane
;		 - First release 
;	1.2	Martin O'Mullane
;		 - Add 'Extra Options' button to choose processes
;                  included in the acd or prb files.
;	1.3	Martin O'Mullane
;		 - Add ZCD, YCD and ECD as output classes.
;
; VERSION:
;       1.1	07-08-2003
;       1.2	05-03-2004
;       1.3	06-10-2006
;
;-
;-----------------------------------------------------------------------------



PRO ADAS408_OUT_MENU, event

; The 'normal' get_value from a widget_button does not work with a
; bitmap button. Hence this specialised event handler.

   ; Get the info structure out of the user value of the top-level base.

Widget_Control, event.top, Get_UValue=info


 formdata = {cancel : 0, menu:1}
 *info.ptrToFormData =formdata
 widget_Control, event.top, /destroy

END


;-----------------------------------------------------------------------------


PRO ADAS408_OUT_EVENT, event

; React to button events - Cancel and Done but not menu (this requires a
; specialised event handler ADAS408_OUT_MENU). Also deal with the passing
; directory output Default button here.

; On pressing Done check for the following


   ; Get the info structure out of the user value of the top-level base.

Widget_Control, event.top, Get_UValue=info


Widget_Control, event.id, Get_Value=userEvent

CASE userEvent OF


  'Cancel' : begin
               formdata = {cancel:1, menu : 0}
               *info.ptrToFormData =formdata
               widget_Control, event.top, /destroy
             end 

      
  'Done'   : begin
                 
                ; gather the data for return to calling program
                
                err  = 0
                mess = ''
                
                widget_Control, info.paperID, Get_Value=pap
                widget_Control, info.yearID, Get_Value=tmp
                year = tmp[0]
                if year EQ 0 then begin
                   err  = 1
                   mess = 'Not a suitable year'
                endif
                
                if err EQ 0 then begin
                
                   widget_Control, info.elementID, Get_Value=tmp
                   element = tmp[0]
                   i4eiz0, element, isym
                   if isym EQ 0 then begin
                      err  = 1
                      mess = 'Choose a real element name'
                   endif
                
                endif

                if err EQ 0 AND info.dsn35 NE 'NULL' then begin
                
                   widget_Control, info.filterID, Get_Value=tmp
                   filter = tmp[0]
                   widget_Control, info.c3ID, Get_Value=tmp3
                   if filter EQ '' AND total(tmp3) GT 0 then begin
                      err  = 1
                      mess = 'Choose a filter name'
                   endif
                
                endif else filter = ''
                
                if err EQ 0 then begin
                
                   widget_Control, info.extID, Get_Value=tmp
                   extension = tmp[0]
                   if extension EQ '' then begin
                      err  = 1
                      mess = 'Choose an extension name'
                   endif
                
                endif


                if err EQ 0 then begin
                
                   widget_Control, info.dirID, Get_Value=tmp
                   dir = tmp[0]
                   file_acc, dir, ex, re, wr, ex, ty
                   
                   if ex EQ 0 then begin
                      err  = 1
                      mess = 'Directory does not exist'
                   endif
                   
                   if err EQ 0 AND ty NE 'd' then begin
                      err  = 1
                      mess = 'This is not a directory'
                   endif
                
                endif

                if err EQ 0 then begin
                
                   widget_Control, info.c1ID, Get_Value=tmp1
                   widget_Control, info.c2ID, Get_Value=tmp2
                   widget_Control, info.c3ID, Get_Value=tmp3

                   list = [tmp1, tmp2, tmp3, 0, 0, 0]
                   sum = total(list)
                   if info.dsn35 NE 'NULL' then begin
                      widget_Control, info.c4ID, Get_Value=tmp4
                      sum = sum + total(tmp4)
                      list[10:12] = tmp4
                   endif

                   if sum EQ 0 then begin
                      err  = 1
                      mess = 'Select at least one output file'
                   endif

                endif


                ; no need to set mess as it is flagged in the cw
                if pap.outbut  EQ 1 AND strtrim(pap.message)  NE '' then err=1
                
                                               
                if err EQ 0 then begin
                   formdata = { paper     : pap,          $
                                year      : year,         $
                                element   : element,      $
                                filter    : filter,       $
                                extension : extension,    $
                                directory : dir,          $
                                list      : list,         $
				itypacd   : info.itypacd, $
				itypprb   : info.itypprb, $
                                cancel    : 0,            $
                                menu      : 0             }
                   *info.ptrToFormData = formdata
                   widget_control, event.top, /destroy
                endif else begin
                   widget_Control, info.messID, Set_Value=mess
                endelse

             end


  'Extra Options...'  : Begin
                  
                st_in = { itypacd : 0, itypprb : 0}      
                rep=adas408_out_opt(st_in,                         $
                                    parent     = info.parent,      $
                                    FONT_SMALL = info.font_small,  $
                                    FONT_LARGE = info.font         )
		
		if rep EQ 'CONTINUE' then begin
                    info.itypacd = st_in.itypacd
                    info.itypprb = st_in.itypprb
                    Widget_Control, event.top, Set_UValue=info
                endif

             end                
  
             
             
  else : print,'ADAS408_OUT_EVENT : You should not see this message! ',userEvent
                   
ENDCASE

END
  
;-----------------------------------------------------------------------------



FUNCTION ADAS408_OUT, outval, dsn35, iz0 , template,     $  
                      bitfile, itypacd, itypprb,         $
                      FONT_LARGE = font_large,           $
                      FONT_SMALL = font_small

; Force special options to default each time

itypacd = 0
itypprb = 0

; Set defaults for keywords and extract info for paper.txt question

  IF n_elements(font_large) eq 0 THEN font_large = ''
  IF n_elements(font_small) eq 0 THEN font_small = ''


  paperval =  { outbut    : outval.TEXOUT, $
                appbut    : outval.TEXAPP, $
                repbut    : outval.TEXREP, $
                filename  : outval.TEXDSN, $
                defname   : outval.TEXDEF, $
                message   : outval.TEXMES  }


; Create top level base widget

                
  parent = Widget_Base(Column=1, Title='ADAS 408 OUTPUT', $
                       XOFFSET=100, YOFFSET=1)
                
  rc = widget_label(parent,value='  ',font=font_large)

  base    = widget_base(parent, /column)


 
; Template and user choices


  mcol       = widget_base(base, /column)
  
  tplab      = widget_label(mcol, value='Template : ' + template, font=font_large)
  mlab       = widget_label(mcol, value=' ', font=font_large)
  
  mrow       = widget_base(mcol, /row)

                       
  yearID     = cw_field(mcol, title='Year      : ', $
                        integer=1, fieldfont=font_small, font=font_small, $
                        xsize=2)
  elementID  = cw_field(mcol, title='Element   : ', $
                        fieldfont=font_small, font=font_small, xsize=2)
  filterID   = cw_field(mcol, title='Filter    : ', $
                        fieldfont=font_small, font=font_small)
  extID      = cw_field(mcol, title='Extension : ', $
                        fieldfont=font_small, font=font_small)
  dirID      = cw_field(mcol, title='Directory : ', $
                        fieldfont=font_small, font=font_small,$
                        xsize=40)

  mlab       = widget_label(mcol, value=' ', font=font_large)

; Select which adf11 files are required

  out1       = ['SCD', 'ACD', 'CCD'] 
  out2       = ['ZCD', 'YCD', 'ECD'] 
  out3       = ['PLT', 'PRB', 'PRC', 'PLS'] 
  out4       = ['PLT - filtered', 'PRB - filtered', 'PRC - filtered '] 

  mlab       = widget_label(mcol, value='Write adf11 class files : ', font=font_large)
  mrow       = widget_base(mcol, /row, /frame)
  c1ID       = cw_bgroup(mrow, out1,                         $
                         nonexclusive=1,column=1,            $
                         event_func='ADAS_PROC_NULL_EVENTS', $
                         /no_release,  font=font_large)
  c2ID       = cw_bgroup(mrow, out2,                         $
                         nonexclusive=1,column=1,            $
                         event_func='ADAS_PROC_NULL_EVENTS', $
                         /no_release,  font=font_large)
  c3ID       = cw_bgroup(mrow, out3,                         $
                         nonexclusive=1,column=1,            $
                         event_func='ADAS_PROC_NULL_EVENTS', $
                         /no_release,  font=font_large)
  c4ID       = cw_bgroup(mrow, out4,                         $
                         nonexclusive=1,column=1,            $
                         event_func='ADAS_PROC_NULL_EVENTS', $
                         /no_release,  font=font_large)
; paper.txt output


  mcol       = widget_base(mcol, /frame)
  paperID = cw_adas_outfile(mcol, OUTPUT='Text Output ',   $
                                  VALUE=paperval, FONT=font_large)


; End of panel message and buttons                

  message  = 'Choose output options'

  messID   = widget_label(parent, value=message, font=font_large)
                      
                
  menufile = bitfile + '/menu.bmp'
  read_X11_bitmap, menufile, bitmap1
                
  mrow     = widget_base(parent,/row)
  menuID   = widget_button(mrow,value=bitmap1,font=font_large, $
                           event_pro='ADAS408_OUT_MENU')
  cancelID = widget_button(mrow,value='Cancel',font=font_large)
  doneID   = widget_button(mrow,value='Done',font=font_large)
  extraID  = widget_button(mrow,value='Extra Options...',font=font_large)


; Initialise the widget

  if dsn35 EQ 'NULL' then begin
     
     widget_control, filterID, sensitive=0
     widget_control, c4ID, sensitive=0

  endif
  
  widget_control, yearID, set_value = outval.year
  widget_control, filterID, set_value = outval.filter
  widget_control, extID, set_value = outval.extension
  widget_control, dirID, set_value = outval.directory
 
  element = strcompress(strlowcase(xxesym(iz0)), /remove_all)
  widget_control, elementID, set_value = element
  
  
; Realize the ADAS408_OUT input widget.

   dynlabel, parent
   widget_Control, parent, /realize



; Create a pointer to store the information collected from the form.
; The initial data stored here is set to CANCEL, so nothing needs to
; be done if the user kills the widget with the mouse.

   ptrToFormData = Ptr_New({cancel:1, menu:0})


; Create an info structure with program information.

  info = { messID          :  messID,             $
           yearID          :  yearID,             $      
           elementID       :  elementID,          $   
           filterID        :  filterID,           $    
           extID           :  extID,              $       
           dirID           :  dirID,              $       
           c1ID            :  c1ID,               $
           c2ID            :  c2ID,               $
           c3ID            :  c3ID,               $
           c4ID            :  c4ID,               $
           paperID         :  paperID,            $
           dsn35           :  dsn35,              $
           font            :  font_large,         $
           font_small      :  font_small,         $
	   itypacd         :  itypacd,            $
	   itypprb         :  itypprb,            $
           parent          :  parent,             $  
           ptrToFormData   :  ptrToFormData       }  
            
               
; Store the info structure in the user value of the top-level base and launch
; the widget into the world.

  widget_control, parent, Set_UValue=info

  XManager, 'ADAS408_OUT', parent, Event_Handler='ADAS408_OUT_EVENT'


; When the widget is destroyed, the block is released, and we
; return here. Get the form data that was collected by the form
; and stored in the pointer location.

formdata = *ptrToFormData

rep='CONTINUE'
IF N_Elements(formdata) EQ 0 THEN BEGIN
   Ptr_Free, ptrToFormData
   rep ='CANCEL'
   RETURN, rep
ENDIF
 
IF formdata.cancel EQ 1 THEN BEGIN
   Ptr_Free, ptrToFormData
   rep ='CANCEL'
   RETURN, rep
ENDIF

IF formdata.menu EQ 1 THEN BEGIN
   Ptr_Free, ptrToFormData
   rep ='MENU'
   RETURN, rep
ENDIF

if rep eq 'CONTINUE' then begin

  outval.texout     = formdata.paper.outbut  
  outval.texapp     = formdata.paper.appbut    
  outval.texrep     = formdata.paper.repbut    
  outval.texdsn     = formdata.paper.filename   
  outval.texdef     = formdata.paper.defname    
  outval.texmes     = ''   
  outval.year       = formdata.year     
  outval.element    = formdata.element  
  outval.filter     = formdata.filter     
  outval.extension  = formdata.extension       
  outval.directory  = formdata.directory
  outval.list       = formdata.list   
  
  itypacd           = formdata.itypacd 
  itypprb           = formdata.itypprb 
  
endif

RETURN, rep

END
