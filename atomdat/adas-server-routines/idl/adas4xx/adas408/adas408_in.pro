; Copyright (c) 2003, Strathclyde University .
; SCCS info: Module @(#)$Header: /home/adascvs/idl/adas4xx/adas408/adas408_in.pro,v 1.1 2004/07/06 10:46:42 whitefor Exp $ Date $Date: 2004/07/06 10:46:42 $
;+
; PROJECT:
;       ADAS 
;
; NAME:
;	ADAS408_IN
;
; PURPOSE: 
;       This function puts up a widget asking for an adf03 input file 
;       and an (optional) adf35 filter file.
;
;
; EXPLANATION:
;       Similar to ADAS208 input screen but with a different
;       way of dealing with events.
;     
;
; NOTES:
;       
; 
;
;
; INPUTS:
;        inval       - A structure holding the 'remembered' input data.
;
; OPTIONAL INPUTS:
;       None
;
; OUTPUTS:
;       The function returns a string rep
;           rep = 'CONTINUE' if everything is ok
;           rep = 'CANCEL' if there is a problem or indeed if the user
;                          closes the widget with the mouse. 
;
; OPTIONAL OUTPUTS:
;       None
;
; KEYWORD PARAMETERS:
;       FONT_LARGE - The name of a larger font e.g 'courier_bold14'
;
;       FONT_SMALL - The name of a smaller font e.g 'courier_bold12'
;
;
; CALLS:
;       ADAS807_IN_FILE     : Reacts to adf04 file widget and sensitises
;                             'Done' and 'Browse' if a valid file has
;                             been selected.
;       ADAS408_IN_EVENT    : Reacts to Cancel, Done and Browse.
;
;
; SIDE EFFECTS:
;
; CATEGORY:
;	Widgets
;
; WRITTEN:
;       Martin O'Mullane
;
; MODIFIED:
;	1.1	Martin O'Mullane
;		 - First release 
;
; VERSION:
;       1.1	21-07-2003
;
;-
;-----------------------------------------------------------------------------

FUNCTION ADAS408_IN_FILE, event

   ; Sensitise 'Done' and 'Browse' if we are ready to continue 

   Widget_Control, event.top, Get_UValue=info
   Widget_Control, info.flaID, Get_Value=file
   
   filename = file.rootpath + file.file
   file_acc, filename, exist, read, write, execute, filetype
        
   if event.action EQ 'newfile' AND filetype EQ '-' then begin
      Widget_Control, info.doneID, sensitive=1
      Widget_Control, info.browseID, sensitive=1
   endif else begin
      Widget_Control, info.doneID, sensitive=0
      Widget_Control, info.browseID, sensitive=0
   endelse
        
   RETURN,0

END 

;-----------------------------------------------------------------------------



PRO ADAS408_IN_EVENT, event

   ; Get the info structure out of the user value of the top-level base.

Widget_Control, event.top, Get_UValue=info
Widget_Control, event.id, Get_Value=userEvent


CASE userEvent OF

  'Cancel' : begin
               formdata = {cancel:1}
               *info.ptrToFormData =formdata
               widget_Control, event.top, /destroy
             end 

                   
  'Done'   : begin
                 
                ; gather the data for return to calling program
                ; 'Done' is not sensitised until valid filenames
                ; have been chosen.
               
                widget_Control, info.flaID, Get_Value=vala
                widget_Control, info.flbID, Get_Value=valb

                
                err  = 0
                mess = ''
                               
                if err EQ 0 then begin
                   formdata = {vala    : vala,         $
                               valb    : valb,         $
                               cancel  : 0             }
                    *info.ptrToFormData = formdata
                    widget_control, event.top, /destroy
                endif else begin
                   widget_Control, info.messID, Set_Value=mess
                endelse

             end
             
    'Browse Comments' : Begin
    
                widget_Control, info.flaID, Get_Value=vala
                widget_Control, info.flbID, Get_Value=valb

                dsn03 = vala.rootpath + vala.file
                dsn35 = valb.rootpath + valb.file

                file_acc, dsn03, exist, read, write, execute, filetype
                if exist EQ 1 AND read EQ 1 AND filetype EQ '-' then $
                   xxtext, dsn03, font=info.font_large

                file_acc, dsn35, exist, read, write, execute, filetype
                if exist EQ 1 AND read EQ 1 AND filetype EQ '-' then $
                   xxtext, dsn35, font=info.font_large

             end     
                
  else : print,'ADAS408_IN_EVENT : You should not see this message! ',userEvent
                   
ENDCASE

END
  
;-----------------------------------------------------------------------------


FUNCTION adas408_in, inval,                   $
                     FONT_LARGE = font_large, $
                     FONT_SMALL = font_small


; Set defaults for keywords

  IF n_elements(font_large) eq 0 THEN font_large = ''
  IF n_elements(font_small) eq 0 THEN font_small = ''


; Path names may be supplied with or without the trailing '/'.  Add
; this character where required so that USERROOT will always end in
; '/' on output.
  
  if strtrim(inval.arootpath) eq '' then begin
      inval.arootpath = './'
  endif else if                                                   $
  strmid(inval.arootpath, strlen(inval.arootpath)-1,1) ne '/' then begin
      inval.arootpath = inval.arootpath+'/'
  endif
  
  if strtrim(inval.brootpath) eq '' then begin
      inval.brootpath = './'
  endif else if                                                   $
  strmid(inval.brootpath, strlen(inval.brootpath)-1,1) ne '/' then begin
      inval.brootpath = inval.brootpath+'/'
  endif



; Top level (modal) base widget                
  
  
  parent = Widget_Base(Column=1, Title='ADAS 408 INPUT', $
                       XOFFSET=100, YOFFSET=1)

                
  rc       = widget_label(parent,value='  ',font=font_large)
  base     = widget_base(parent,  /align_center,/column)
  
  
  filebase    = widget_base(base, /column)

  

; Ask for adf03 and adf35 input selections


  flabase = widget_base(filebase, /column, /frame)
  flaval = {  ROOTPATH        :       inval.arootpath,                $
              FILE            :       inval.afile,                    $
              CENTROOT        :       inval.acentroot,                $
              USERROOT        :       inval.auserroot                 }
  flatitle = widget_label(flabase, font=font_large,                   $
                          value='Atomic parameters File Details:-')
  flaID = cw_adas_infile(flabase, value=flaval, font=font_small, $
                         ysize = 5, event_func='ADAS408_IN_FILE')



  flbbase = widget_base(filebase, /column, /frame)
  flbval = {  ROOTPATH        :       inval.brootpath,                $
              FILE            :       inval.bfile,                    $
              CENTROOT        :       inval.bcentroot,                $
              USERROOT        :       inval.buserroot                 }
  flbtitle = widget_label(flbbase, font=font_large,                   $
                          value='Filter File Details:-')
  flbID = cw_adas_infile(flbbase, value=flbval, font=font_small, $
                         ysize = 5, event_funct='ADAS_PROC_NULL_EVENTS')



; Control buttons
                
  mrow     = widget_base(parent,/row,/align_center)
  messID   = widget_label(mrow,font=font_large, value=' ')
                
  mrow     = widget_base(parent,/row)
  
  browseID = widget_button(mrow,value='Browse Comments', font=font_large)
  cancelID = widget_button(mrow,value='Cancel',font=font_large)
  doneID   = widget_button(mrow,value='Done',font=font_large)


; Initial settings - depends on branch

  mess = 'Enter parameter and (optional) filter file details'

  widget_control, messID, set_value = mess


; Check filenames and desensitise 'Done' if adf03 selection is a 
; directory or if it is a file without read access.                   
  
  filename = inval.arootpath + inval.afile
  browseallow = 0
  file_acc, filename, fileexist, read, write, execute, filetype
  if filetype ne '-' then begin
      Widget_Control, browseID, sensitive=0
      Widget_Control, doneID, sensitive=0
  endif else begin
      if read eq 0 then begin
          Widget_Control, browseID, sensitive=0
          Widget_Control, doneID, sensitive=0
      endif else begin
          browseallow = 1
      endelse
  endelse


; Realize the ADAS408 input widget.
   
   dynlabel, parent
   widget_Control, parent, /realize

; Create a pointer to store the information collected from the form.
; The initial data stored here is set to CANCEL, so nothing needs to
; be done if the user kills the widget with the mouse.

   ptrToFormData = Ptr_New({cancel:1})

; Create an info structure with program information.

  info = { parent          :  parent,             $
           flaID           :  flaID,              $
           flbID           :  flbID,              $
           doneID          :  doneID,             $
           browseID        :  browseID,           $
           messID          :  messID,             $
           font_large      :  font_large,         $
           font_small      :  font_small,         $
           ptrToFormData   :  ptrToFormData       }  
            
               
; Store the info structure in the user value of the top-level base and launch
; the widget into the world.

  widget_control, parent, Set_UValue=info

  XManager, 'adas408_in', parent, Event_Handler='ADAS408_IN_EVENT'


; When the widget is destroyed, the block is released, and we
; return here. Get the data that was collected by the widget
; and stored in the pointer location. Finally free the pointer.

formdata = *ptrToFormData

rep='CONTINUE'
IF N_Elements(formdata) EQ 0 THEN BEGIN
   Ptr_Free, ptrToFormData
   rep ='CANCEL'
ENDIF
 
IF formdata.cancel EQ 1 THEN BEGIN
   Ptr_Free, ptrToFormData
   rep ='CANCEL'
ENDIF

if rep eq 'CONTINUE' then begin
   
   inval.arootpath   = formdata.vala.rootpath
   inval.afile       = formdata.vala.file
   inval.acentroot   = formdata.vala.centroot
   inval.auserroot   = formdata.vala.userroot
   inval.brootpath   = formdata.valb.rootpath
   inval.bfile       = formdata.valb.file
   inval.bcentroot   = formdata.valb.centroot
   inval.buserroot   = formdata.valb.userroot
      
   Ptr_Free, ptrToFormData
   
endif       


RETURN,rep


END
