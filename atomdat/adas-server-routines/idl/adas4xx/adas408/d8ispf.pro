; Copyright (c) 1996, Strathclyde University.
; SCCS info: Module @(#)$Header: /home/adascvs/idl/adas4xx/adas408/d8ispf.pro,v 1.2 2004/07/06 13:25:09 whitefor Exp $ Date $Date: 2004/07/06 13:25:09 $
;+
; PROJECT:
;       ADAS IBM MVS to UNIX conversion
;
; NAME:
;	D8ISPF
;
; PURPOSE:
;	IDL user interface and communications with ADAS408 FORTRAN
;	process via pipe.
;
; EXPLANATION:
;	The routine begins by reading information from the ADAS408
;	FORTRAN process via a UNIX pipe.  Then part of the ADAS408
;	IDL user interface is invoked to determine how the user
;	wishes to process the input dataset.  When the user's
;	interactions are complete the information gathered with
;	the user interface is written back to the FORTRAN process
;	via the pipe.  Communications are to the FORTRAN subroutine
;	D8ISPF.
;
; USE:
;	The use of this routine is specific to ADAS408, see adas408.pro.
;
; INPUTS:
;	PIPE	- The IDL unit number of the bi-directional pipe to the
;		  ADAS408 FORTRAN process.
;
;	LPEND	- Integer, 0 if the user pressed the 'Done' button
;		  or 1 if the user pressed 'Cancel'.  Maps directly onto
;		  the logical variable LPEND in ADAS408 FORTRAN.
;
;	PROCVAL	- A structure which determines the initial settings of
;		  the processing options widget.  The initial value is
;		  set in adas408.pro.  If adas408.pro passes a blank 
;		  dummy structure of the form {NEW:-1} into PROCVAL then
;		  PROCVAL is reset to a default structure.
;
;		  The PROCVAL structure is;
;   			 procval = {	NEW   	: 	0 ,             $
;					TITLE 	: 	'',		$
;					SYMBOL	: 	'',		$
;					AMSI	:	0,		$
;					AMSH	:	0,		$
;					LEVCUT	:	0,		$
;					IEVCUT	:	0,		$
;					THBE	:	0.0.		$
;					THSI	:	0.0,		$
;					FNAME	:	'',		$
;					TINMIN	:	0.0,		$
;					TINMAX	:	0.0,		$
;					NUMT	:	0,		$
;					DINMIN	:	0.0,		$
;					DINMAX	:	0.0,		$
;					NUMD	:	0,		$
;					ITTYP	:	0,		$
;					ITDIMD	:	0,		$
;					IDDIMD	:	0,		$
;					IZ0	:	0		}
;
;		  Where the elements of the structure represent:
;	
;               NEW     - Flag which defines whether or not default values
;                         exist or not. (< 0 if no values exist)
;               TITLE   - String; user-entered title for the run.
;		SYMBOL	- String; Impurity element symbol - used as label
;		AMSI	- Float; Impurity atomic mass
;		AMSH	- Float; Hydrogen atomic mass
;		LEVCUT	- Int; 0 => Do use simple cut-off energy
;			       1 => Do NOT use simple cut-off energy
;		IEVCUT	- Int; Energy for simpl cut-off (eV)
;		THBE	- Float; Beryllium filter thickness (micron)
;		THSI	- Float; Silicon filter thickness (mm)
;		FNAME	- String; Filter name (used in constructing 
;			  passing file names)
;		TINMIN	- Float; Min. electron temperature for scan
;		TINMAX  - Float; Max. electron temperature for scan
;		NUMT	- Int; No. of temperatures to be used in scan
;		DINMIN	- Float; Min. electron density for scan (cm-3)
;		DINMAX	- Float; Max. electron density for scan (cm-3)
;		NUMD	- Int; No. of densities to be used in scan
;		ITTYP	- Int; 0 => Use Kelvin as temperature units
;			       1 => Use eV as temperature units
;		ITDIMD	- Int; Max. no. of temperatures
;		IDDIMD	- Int; Max. no. of densities
;		IZ0	- Int; Nuclear charge from input file
;
;	DSFULL	- String; The full system file name of the input 
;		  dataset selected by the user for processing.
;
;	BITFILE - String; the path to the dirctory containing bitmaps
;		  for the 'escape to series menu' button.
;
; OPTIONAL INPUTS:
;	None
;
; OUTPUTS:
;       LPEND   - Integer, 0 if the user pressed the 'Done' button
;                 or 1 if the user pressed 'Cancel'.  Maps directly onto
;                 the logical variable LPEND in ADAS408 FORTRAN.
;	PROCVAL	- On output the structure records the final settings of
;		  the processing selections widget if the user pressed the
;		  'Done' button, otherwise it is not changed from input.
;	GOMENU	- Int; flag - set to 1 if user has selected 'escape direct
;		  to series menu , otherwise 0
;
; OPTIONAL OUTPUTS:
;	None
;
; KEYWORD PARAMETERS:
;	FONT_LARGE - The name of a larger font.
;
;	FONT_SMALL - The name of a smaller font.
;
;	EDIT_FONTS - A structure of two fonts used in the table editor
;		     adas_edtab.pro, {font_norm:'',font_input:''}.
;
; CALLS:
;	ADAS408_PROC	Invoke the IDL interface for ADAS408 data
;		 	processing options/input.
;
; SIDE EFFECTS:
;	Two way communications with ADAS408 FORTRAN via the
;	bi-directional UNIX pipe.
;
; CATEGORY:
;	Adas system.
;
; WRITTEN:
;       Tim Hammond, Tessella Support Services plc, 4th April 1996
;
; MODIFIED:
;	1.1	Tim Hammond 
;		First version
;
; VERSION:
;	1.1	04-04-96
;
; VERSION: 1.2 					DATE: 24-03-97
; MODIFIED:	M. O'Mullane
;		Changed fname from format='(a6)', to format='(a7)'
;
;-
;-----------------------------------------------------------------------------

PRO d8ispf, pipe, lpend, inval, procval, dsfull, gomenu, bitfile,       $
	    FONT_LARGE=font_large, FONT_SMALL=font_small, 		$
            EDIT_FONTS=edit_fonts

                ;**** Set defaults for keywords ****

    IF NOT (KEYWORD_SET(font_large)) THEN font_large = ''
    IF NOT (KEYWORD_SET(font_small)) THEN font_small = ''
    IF NOT (KEYWORD_SET(edit_fonts)) THEN 				$
    edit_fonts = {font_norm:'',font_input:''}

		;********************************************
		;****     Declare variables for input    ****
                ;********************************************
    lpend = 0
    idum = 0
    fdum = 0.0
    sdum = ''

		;********************************
		;**** Read data from fortran ****
		;********************************

    readf, pipe, idum
    itdimd = idum
    readf, pipe, idum
    iddimd = idum
    readf, pipe, idum
    iz0 = idum
  

		;*******************************************
		;**** Set default value if non provided ****
		;*******************************************

    if (procval.new lt 0) then begin
        procval = {	NEW   	: 	0 ,             		$
			TITLE 	: 	'',				$
			SYMBOL	: 	'',				$
			AMSI	:	0,				$
			AMSH	:	0,				$
			LEVCUT	:	0,				$
			IEVCUT	:	0,				$
			THBE	:	0,				$
			THSI	:	0,				$
			FNAME	:	'',				$
			TINMIN	:	0.0,				$
			TINMAX	:	0.0,				$
			NUMT	:	0,				$
			DINMIN	:	0.0,				$
			DINMAX	:	0.0,				$
			NUMD	:	0,				$
			ITTYP	:	0,				$
			ITDIMD	:	itdimd,				$
			IDDIMD	:	iddimd, 			$
			IZ0	:	iz0				}
    endif
    procval.iz0 = iz0
    if (procval.itdimd ne itdimd) or (procval.iddimd ne iddimd) then begin
	procval.itdimd = itdimd
	procval.iddimd = iddimd
    endif

		;****************************************
		;**** Pop-up processing input widget ****
		;****************************************

    adas408_proc, procval, dsfull, action, bitfile,			$
                  FONT_LARGE=font_large, FONT_SMALL=font_small, 	$
		  EDIT_FONTS=edit_fonts

                ;*********************************************
                ;**** Act on the output from the widget   ****
                ;**** There are three    possible actions ****
                ;**** 'Done', 'Cancel' and 'Menu'.        ****
                ;*********************************************

    if action eq 'Done' then begin
        lpend = 0
    endif else if action eq 'Menu' then begin
        lpend = 1
        gomenu = 1
    endif else begin
        lpend = 1
    endelse

		;*******************************
		;**** Write data to fortran ****
		;*******************************

    printf, pipe, lpend   
    if lpend eq 0 then begin
        printf, pipe, procval.title, format='(a40)'
	printf, pipe, procval.amsi
	printf, pipe, procval.amsh
	printf, pipe, procval.levcut
	printf, pipe, procval.ievcut
	printf, pipe, procval.thbe
	printf, pipe, procval.thsi
	printf, pipe, procval.fname, format='(a7)'
	printf, pipe, procval.numt
	printf, pipe, procval.numd
	printf, pipe, procval.ittyp+1
	printf, pipe, procval.tinmin
	printf, pipe, procval.tinmax
	printf, pipe, procval.dinmin
	printf, pipe, procval.dinmax
    endif

END
