; Copyright (c) 1996, Strathclyde University.
; SCCS info: Module @(#)$Header: /home/adascvs/idl/adas4xx/adas408/d8spf0.pro,v 1.1 2004/07/06 13:25:35 whitefor Exp $ Date $Date: 2004/07/06 13:25:35 $
;+
; PROJECT:
;       ADAS IBM MVS to UNIX conversion
;
; NAME:
;	D8SPF0
;
; PURPOSE:
;	IDL user interface and communications with ADAS408 FORTRAN
;	process via pipe.
;
; EXPLANATION:
;	Firstly this routine invokes the part of the user interface
;	used to select the input dataset for ADAS408.  When the user's
;	interactions are complete this routine communicates with the
;	ADAS408 FORTRAN application via a UNIX pipe.  Communications
;	are to the FORTRAN subroutine D8SPF0.
;
; USE:
;	The use of this routine is specific to ADAS408, see adas408.pro.
;
; INPUTS:
;	PIPE	- The IDL unit number of the bi-directional pipe to the
;		  ADAS408 FORTRAN process.
;
;	VALUE	- A structure which determines the initial settings of
;		  the dataset selection widget.  The initial value is
;		  set in adas408.pro.  VALUE is passed un-modified
;		  through to cw_adas_in.pro, see that routine for a full
;		  description.
;
; OPTIONAL INPUTS:
;	None
;
; OUTPUTS:
;	VALUE	- On output the structure records the final settings of
;		  the dataset selection widget if the user pressed the
;		  'Done' button otherwise it is not changed from input.
;
;	DSFULL	- String; The full system file name of the input 
;		  dataset selected by the user for processing.
;
;	REP	- String; Indicates whether the user pressed the 'Done'
;		  or 'Cancel' button on the interface.  The action is
;		  converted to the strings 'NO' and 'YES' respectively
;		  to match up with the existing FORTRAN code.  In the
;		  original IBM ISPF interface REP was the reply to the
;		  question 'DO YOU WANT TO END PROGRAM EXECUTION'.
;
;	FILE_CHANGED
;		- Integer; 1 => the file has been changed by the user
;			   0 => the file hasn't been changed
;
; OPTIONAL OUTPUTS:
;	None
;
; KEYWORD PARAMETERS:
;	FONT	- Supplies the font to be used for the interface widgets.
;
; CALLS:
;	ADAS_IN		Pops-up the dataset selection widget.
;
; SIDE EFFECTS:
;	This routine communicates with the ADAS408 FORTRAN process
;	via a UNIX pipe.
;
; CATEGORY:
;	Adas system.
;
; WRITTEN:
;       Tim Hammond, Tessella Support Services plc, 4th April 1996
;
; MODIFIED:
;	1.1	Tim Hammond
;		First version
;
; VERSION:
;	1.1	04-04-96
;
;-
;-----------------------------------------------------------------------------

PRO d8spf0, pipe, value, dsfull, rep, FONT=font


                ;**** Set defaults for keywords ****

    IF NOT (KEYWORD_SET(font)) THEN font = ''

		;**********************************
		;**** Pop-up input file widget ****
		;**********************************

    adas_in, value, action, WINTITLE='ADAS 408 INPUT', FONT=font,	 $
	     TITLE = 'Input Dataset'

		;******************************************
		;**** Act on the event from the widget ****
		;******************************************
		;**** There are only two possible events ****
		;**** 'Done' and 'Cancel'.               ****
		;********************************************

    if action eq 'Done' then begin
        rep = 'NO'
    endif else begin
        rep = 'YES'
    endelse
  
		;**** Construct datatset name ****

    dsfull = value.rootpath + value.file

		;*******************************
		;**** Write data to fortran ****
		;*******************************

    printf, pipe, rep
    printf, pipe, dsfull

END
