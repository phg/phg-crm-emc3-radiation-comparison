;----------------------------------------------------------------------
;+
; PROJECT    :  ADAS
;
; NAME       :  d8tran
;
; PURPOSE    :  
;
; ARGUMENTS  :  All output arguments will be defined appropriately.
;               Inputs will be converted to correct type, if possible,
;               internally without changing calling type.
;
;
;               NAME      I/O    TYPE    DETAILS
; REQUIRED   :
;
;
; NOTES      :  Calls fortran code.
;
;
; AUTHOR     :  Martin O'Mullane
;
; DATE       :  23/07/03
;
;
; MODIFIED:
;       1.1     Martin O'Mullane
;              	- First version.
;
;
; VERSION:
;       1.1    23-07-2003
;
;-
;----------------------------------------------------------------------

PRO d8tran, ndeng  , ndedge  ,             $
            ieng   , iedge   ,             $
            edge   , energy  , fraction ,  $
            ein    , fout


fortdir = getenv('ADASFORT')

; Set up variables to be of the correct type

ndeng    = long(ndeng)
ndedge   = long(ndedge)
ieng     = long(ieng)  
iedge    = long(iedge) 
edge     = double(edge)
energy   = double(energy)
fraction = double(fraction)
ein      = double(ein)
fout     = -1.0D0

dummy = CALL_EXTERNAL(fortdir+'/d8tran_if.so','d8tran_if',  $
                      ndeng, ndedge, ieng, iedge, edge,     $
                      energy, fraction, ein, fout)

END
