; Copyright (c) 1995 Strathclyde University 
;+
; PROJECT:
;       ADAS IBM MVS to UNIX conversion
;
; NAME:
;	ADAS408
;
; PURPOSE:
;	The highest level routine for the ADAS408 program.
;
; EXPLANATION:
;	This routine is called from the main adas system routine, adas.pro,
;	to start the ADAS 408 application.  Associated with adas408.pro
;	is a FORTRAN executable.  The IDL code provides the user
;	interface and output graphics whilst the FORTRAN code reads
;	in data files, performs numerical processing and creates the
;	output files.  The IDL code communicates with the FORTRAN
;	executable via a bi-directional UNIX pipe.  The unit number
;	used by the IDL for writing to and reading from this pipe is
;	allocated when the FORTRAN executable is 'spawned' (see code
;	below).  Pipe communications in the FORTRAN process are to
;	stdin and stdout, i.e streams 5 and 6.
;
;	The FORTRAN code is an independent process under the UNIX system.
;	The IDL process can only exert control over the FORTRAN in the
;	data which it communicates via the pipe.  The communications
;	between the IDL and FORTRAN must be exactly matched to avoid
;	input conversion errors.  The correct ammounts of data must be
;	passed so that neither process 'hangs' waiting for communications
;	which will never occur.
;
;	The FORTRAN code performs some error checking which is
;	independent of IDL.  In cases of error the FORTRAN may write
;	error messages.  To prevent these error messages from conflicting
;	with the pipe communications all FORTRAN errors are written to
;	output stream 0, which is stderr for UNIX.  These error messages
;	will appear in the window from which the ADAS session/IDL session
;	is being run.
;
;	In the case of severe errors the FORTRAN code may terminate
;	itself prematurely.  In order to detect this, and prevent the
;	IDL program from 'hanging' or crashing, the IDL checks to see
;	if the FORTRAN executable is still an active process before
;	each group of pipe communications.  The process identifier
;	for the FORTRAN process, PID, is recorded when the process is
;	first 'spawned'.  The system is then checked for the presence
;	of the FORTRAN PID.
;
; USE:
;	First the system settings must be established by calling
;	adas_sys_set.pro then adas408.pro is called to start the
;	ADAS408 application;
;
;	adas_sys_set, adasrel, fortdir, userroot, centroot, 		$
;		      devlist, devcode, font_large, font_small, edit_fonts
;	adas408,   adasrel, fortdir, userroot, centroot, devlist, 	$
;		   devcode, font_large, font_small, edit_fonts
;
; INPUTS:
;	ADASREL - A string indicating the ADAS system version, 
;		  e.g ' ADAS RELEASE: ADAS93 V1.1.0'.  The first
;		  character should be a space.
;
;	FORTDIR - A string holding the path to the directory where the
;		  FORTRAN executables are, e.g '/disk/adas/fortran/exec'
;
;	USERROOT - A string holding the path to the root directory of
;		   the user's adas data. e.g '/disk/bowen/adas'
;		   This root directory will be used by adas to construct
;		   other path names.  For example the users default data
;		   for adas408 should be in /disk/bowen/adas/adf03.  In
;		   particular the user's default interface settings will
;		   be stored in the directory USERROOT+'/defaults'.  An
;		   error will occur if the defaults directory does not
;		   exist.
;
;	CENTROOT - Like USERROOT, but this directory points to the
;		   central data area for the system.  User defaults are
;		   not stored on CENTROOT.
;
;	DEVLIST - A string array of hardcopy device names, used for
;		  graphical output. e.g ['Post-Script','HP-PCL','HP-GL']
;		  This array must mirror DEVCODE.  DEVCODE holds the
;		  actual device names used in a SET_PLOT statement.
;
;	DEVCODE - A string array of hardcopy device code names used in
;		  the SET_PLOT statement, e.g ['PS', 'PCL', 'HP']
;		  This array must mirror DEVLIST.
;
;	FONT_LARGE - The name of a larger font e.g 'courier_bold14'
;
;	FONT_SMALL - The name of a smaller font e.g 'courier_bold12'
;
;	EDIT_FONTS - A structure of two fonts used in the table editor
;		     adas_edtab.pro, {font_norm:'',font_input:''}.
;		     The two fonts are used to differentiate between
;		     editable and non-editable parts of the table. You
;		     may have to experiment to get two fonts which give
;		     the same default spacing in the edit widget.
;		     e.g {font_norm:'helvetica_bold14', $
;			  font_input:'helvetica_oblique14'}
;
; OPTIONAL INPUTS:
;	None
;
; OUTPUTS:
;	None - Note this routine should leave all inputs un-modified.
;
; OPTIONAL OUTPUTS:
;	None
;
; KEYWORD PARAMETERS:
;	None
;
; CALLS:
;	FIND_PROCESS	Checks to see if a given process is active.
;	XXDATE		Get date and time from operating system.
;	D8SPF0		Pipe comms with FORTRAN D8SPF0 routine.
;	D8ISPF		Pipe comms with FORTRAN D8ISPF routine.
;	D8SPF1		Pipe comms with FORTRAN D8SPF1 routine.
;
; SIDE EFFECTS:
;	This routine spawns a FORTRAN executable.  Note the pipe 
;	communications routines listed above.  In addition to these
;	pipe communications there is one explicit communication of the
;	date to the FORTRAN code, and other communications of user's
;	intentions to immediately leave the program by pressing the 
;	return to menu button, search for 'printf, pipe' to find them.
;
; CATEGORY:
;	Adas system.
;	
; WRITTEN:
;       Tim Hammond, Tessella Support Services plc, 04-04-96
;
; VERSION:
;	1.1	Tim Hammond + William Osborn
;               First version
;	1.2	William Osborn
;		Added title to 'Outputting passing files' message window
;	1.3	William Osborn
;		Updated version number to 1.2
;       1.4     William Osborn
;               Increased version number to 1.3
;       1.5     Richard Martin
;               Increased version number to 1.4
;       1.6     Richard Martin
;               Increased version number to 1.5
;       1.7     Martin O'Mullane
;                - Re-structure for using adf35 filter datasets.
;                - Increased version number to 1.6
;       1.8     Martin O'Mullane
;                - Allow user to choose processes included in the
;                  acd or prb file via an 'Extra Options' button in 
;                  the output screen.  
;                - Increased version number to 1.7
;       1.9     Martin O'Mullane
;                - Remove debug print statement.
;       1.10    Martin O'Mullane
;                - Add a read from fortran at end of output file
;                  writing. Otherwise IDL can exit (when invoked
;                  as run_adas408) and kill the fortran process 
;                  before the files are written.
;	1.11	Martin O'Mullane
;		 - Add ZCD, YCD and ECD as output classes.
;	1.12	Martin O'Mullane
;		 - Calculate te/dens vector from min/max pairs
;                  here since the fortran now expects a vector
;                  as input.
;
; MODIFIED:
;	1.1	04-04-96
;	1.2	23-04-96
;	1.3	11-07-96
;       1.4     14-10-96
;       1.5     14-10-96
;       1.6     11-03-98
;       1.7     21-07-2003
;       1.8     13-04-2004
;       1.9     06-10-2004
;       1.10    21-02-2005
;       1.11    06-10-2006
;       1.12    07-02-2010
;
;-
;-----------------------------------------------------------------------------

PRO ADAS408, adasrel, fortdir, userroot, centroot,                   $
	     devlist, devcode, font_large, font_small, edit_fonts


; Initialisation

    adasprog  = ' PROGRAM: ADAS408 V1.7'
    deffile   = userroot+'/defaults/adas408_defaults.dat'
    bitfile   = centroot+'/bitmaps'
    device    = ''
                
    template  =  '<class><year>#<element>.<filter>.<ext>'
    
    
    
; We rely on IDL v5 features - are there any v4 places left?

    thisRelease = StrMid(!Version.Release, 0, 1)
    IF thisRelease LT '5' THEN BEGIN
       message = 'Sorry, ADAS408 requires IDL 5 or higher' 
       tmp = popup(message=message, buttons=['Accept'],font=font_large)
       goto, LABELEND
    ENDIF



; Restore user default settings 


    files = findfile(deffile)

    if files[0] EQ deffile then begin

        restore, deffile
        inval.acentroot = centroot+'/adf03/'
        inval.auserroot = userroot+'/adf03/'
        inval.bcentroot = centroot+'/adf35/'
        inval.buserroot = userroot+'/adf35/'
    
    endif else begin

       inval   = {arootpath   :   userroot+'/adf03/',   $
                  afile       :   '',                   $
                  acentroot   :   centroot+'/adf03/',   $
                  auserroot   :   userroot+'/adf03/',   $
                  brootpath   :   userroot+'/adf35/',   $
                  bfile       :   '',                   $
                  bcentroot   :   centroot+'/adf35/',   $
                  buserroot   :   userroot+'/adf35/'    }
                   

       procval = {title       :   '',                   $
                  te_min      :   0.0D0,                $
                  te_max      :   0.0D0,                $
                  te_num      :   0,                    $
                  ifout       :   2,                    $
                  dens_min    :   0.0D0,                $
                  dens_max    :   0.0D0,                $
                  dens_num    :   0,                    $
                  mass_imp    :   0.0D0,                $
                  mass_hyd    :   0.0D0                 }

       
       outval  = {TEXOUT      :   0,                    $
                  TEXAPP      :   -1,                   $
                  TEXREP      :   0,                    $
                  TEXDSN      :   '',                   $
                  TEXDEF      :   'paper.txt',          $
                  TEXMES      :   '',                   $
                  directory   :   '',                   $
                  year        :   89,                   $
                  element     :   '',                   $
                  filter      :   '',                   $
                  extension   :   'pass',               $
                  list        :   intarr(13)            }

  endelse


; Start the fortran
  
  spawn, fortdir + '/adas408.out', unit=pipe,/noshell,PID=pid


; First task is to get date and time and pass to fortran

  date = bxdate()
  printf, pipe, date[0]

  header = adasrel + adasprog + ' DATE: ' + date[0] + ' TIME: ' + date[1]
  printf, pipe, header
	
; Start the main event loop - 3 screens; input, processing and output
; Check that the fortran code is running at each stage       


;-------------
; Input screen
;-------------

LABEL100:
  
    
    if find_process(pid) EQ 0 then goto, LABELEND

    rep = adas408_in(inval,                     $
                     font_large=font_large,     $ 
                     font_small = font_small    )

    if rep eq 'CANCEL' then goto, LABELEND
    

    if find_process(pid) EQ 0 then goto, LABELEND
    printf, pipe, rep

    if rep eq 'CONTINUE' then begin
    
       dsn03 = inval.arootpath + inval.afile
       dsn35 = inval.brootpath + inval.bfile

       file_acc, dsn35, exist, read, write, execute, filetype
       if NOT (exist EQ 1 AND read EQ 1 AND filetype EQ '-') then dsn35='NULL' 

       printf, pipe, dsn03
       printf, pipe, dsn35
      
    endif
  

;------------------
; Processing screen
;------------------

LABEL200:

    if find_process(pid) eq 0 then goto, LABELEND

    iz0 = -1L
    openr, lun, dsn03, /get_lun
    readf, lun, iz0
    free_lun, lun
    
    rep = adas408_proc(procval,                   $
                       iz0,                       $
                       dsn03,                     $
                       dsn35,                     $
                       bitfile,                   $
                       FONT_LARGE = font_large,   $    
                       FONT_SMALL = font_small    )    

    if find_process(pid) EQ 0 then goto, LABELEND
    printf, pipe, rep

    case rep of

      'MENU'     : goto, LABELEND

      'CANCEL'   : goto, LABEL100
     
      'CONTINUE' : begin
        
                      te = adas_vector(low=procval.te_min, $
                                       high=procval.te_max, $
                                       num=procval.te_num)
                      dens = adas_vector(low=procval.dens_min, $
                                         high=procval.dens_max, $
                                         num=procval.dens_num)
                      
                      printf, pipe, procval.title  
                      
                      printf, pipe, procval.ifout   
                      printf, pipe, procval.te_num
                      for j = 0, procval.te_num-1 do printf, pipe, te[j]
                      
                      printf, pipe, procval.dens_num
                      for j = 0, procval.dens_num-1 do printf, pipe, dens[j]
                      
                      printf, pipe, procval.mass_imp 
                      printf, pipe, procval.mass_hyd 
                      
                   end
    
    endcase
  
;-----------------
; The calculations
;-----------------

    if find_process(pid) EQ 0 then goto, LABELEND

    adas_progressbar, pipe     = pipe,             $
                      n_call   = procval.te_num,   $
                      adasprog = adasprog,         $
                      font     = font_large

;--------------
; Output screen
;--------------


LABEL300:

    if find_process(pid) eq 0 then goto, LABELEND


    rep = adas408_out(outval,  dsn35, iz0 , template,  $
                      bitfile, itypacd, itypprb,       $
                      FONT_LARGE = font_large,         $    
                      FONT_SMALL = font_small          )


    if find_process(pid) EQ 0 then goto, LABELEND
    printf, pipe, rep

    case rep of

      'MENU'     : goto, LABELEND

      'CANCEL'   : goto, LABEL200
     
      'CONTINUE' : begin
                      ; construct part of filename after classname
                      
                      file_ext = string(outval.year) + '_' + $
                                 outval.element      + '.' + $
                                 outval.extension
                                 
                      if dsn35 NE 'NULL' then begin
                          file_ext_filter = string(outval.year) + '_' + $
                                            outval.element      + '.' + $
                                            outval.filter       + '.' + $
                                            outval.extension
                      endif else file_ext_filter = ''
                      
                      file_ext_filter = strcompress(strlowcase(file_ext_filter), /remove_all)
                      file_ext        = strcompress(strlowcase(file_ext), /remove_all)
                      
                      printf, pipe, file_ext
                      printf, pipe, file_ext_filter
       
                      ; add trailing / to directory name

                      dir = outval.directory
                      if strlen(dir) GT 1 then dir = dir + '/'
                      printf, pipe, dir
		      
		      printf, pipe, itypacd
		      printf, pipe, itypprb

                      if dir EQ ' ' then dir  = ''       
                      
                      printf, pipe, outval.texout 
                      if outval.texout EQ 1 then printf, pipe, outval.texdsn
                      
                      ; convert from adas408_out.pro list to adas408.for list
                      ; idl     : scd, acd, ccd, zcd, ycd, ecd, plt, prb, prc, pls, plt - fil, prb - fil, prc - fil 
                      ; fortran : acd, scd, ccd, prb - fil, plt - fil, pls, prc - fil, prb, plt, prc, zcd, ycd, ecd  
                      
                      ind = [1, 0, 2, 11, 10, 9, 12, 7, 6, 8, 3, 4, 5]
                      for j = 0, 12 do printf, pipe, outval.list[ind[j]]

                  end
                   
    endcase
    
    GOTO, LABEL300


LABELEND:



; Save user defaults and free all units


readf, pipe, rep

if rep EQ 'EXIT NOW' then begin
   close, /all
   save, inval, procval, outval, filename=deffile
endif

END
