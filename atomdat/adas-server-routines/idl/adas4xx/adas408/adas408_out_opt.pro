; SCCS Info Module @(#)$Header: /home/adascvs/idl/adas4xx/adas408/adas408_out_opt.pro,v 1.1 2004/07/06 10:46:56 whitefor Exp $ Date: $Date: 2004/07/06 10:46:56 $ 
;
;----------------------------------------------------------------------
;+
; PROJECT:
;       ADAS 
;
; NAME:
;	ADAS408_IN_OPT
;
; PURPOSE: 
;       This function puts up a widget asking for optional parameters
;       such as scaling factors, temperature sets and E1/M1/E2 choices. 
;
;
; EXPLANATION:
;             
;     
;
; NOTES:
;       Based on the techniques of ADAS403. It entirely in IDL and uses
;       v5 (and above) features such as pointers to pass data.
; 
;
;
; INPUTS:
;        inval       - A structure holding the 'remembered' input data.
;
; OPTIONAL INPUTS:
;       None
;
; OUTPUTS:
;       The function returns a string rep
;           rep = 'CONTINUE' if everything is ok
;           rep = 'CANCEL' if there is a problem or indeed if the user
;                          closes the widget with the mouse. 
;
; OPTIONAL OUTPUTS:
;       None
;
; KEYWORD PARAMETERS:
;       FONT_LARGE - The name of a larger font e.g 'courier_bold14'
;
;       FONT_SMALL - The name of a smaller font e.g 'courier_bold12'
;
;
; CALLS:
;       ADAS408_IN_OPT_DEF   : Event handler for Temperature selection.                     
;       ADAS408_IN_OPT_UNIT  : Converts between red. eV and K temperatures.
;       ADAS408_IN_OPT_EVENT : General event handler.
;
; SIDE EFFECTS:
;
; CATEGORY:
;	Widgets
;
; WRITTEN:
;       Martin O'Mullane
;
; MODIFIED:
;	1.1	Martin O'Mullane
;		 - First release 
;
; VERSION:
;       1.1	12-04-2004
;
;-
;-----------------------------------------------------------------------------

PRO ADAS408_IN_OPT_EVENT, event

   ; Get the info structure out of the user value of the top-level base.

Widget_Control, event.top, Get_UValue=info


Widget_Control, event.id, Get_Value=userEvent

CASE userEvent OF

  'Cancel' : begin
               formdata = {cancel:1}
               *info.ptrToFormData =formdata
               widget_Control, event.top, /destroy
             end 

      
  'Done'   : begin
  
                err  = 0
                mess = ' '
                 
                ; gather the data for return to calling program
                ; 'Done' is not sensitised until valid filenames
                ; have been chosen. 
                
                
                ; Get the values
               
                widget_control, info.acdID, get_value=itypacd
                widget_control, info.prbID, get_value=itypprb
		
		
                
                formdata = { itypacd  : itypacd,	 $
		             itypprb  : itypprb,         $
                	     cancel   : 0		 }
                 *info.ptrToFormData = formdata
                 widget_control, event.top, /destroy

             end

  
  
  else : print,'ADAS408_IN_OPT_EVENT : You should not see this message! '
                   
ENDCASE

END
  
;-----------------------------------------------------------------------------


FUNCTION ADAS408_out_opt, procval,                   $
                          PARENT     = parent,       $
                          FONT_LARGE = font_large,   $
                          FONT_SMALL = font_small

; Set defaults for keywords 

  IF n_elements(font_large) eq 0 THEN font_large = ''
  IF n_elements(font_small) eq 0 THEN font_small = ''


; We want a modal top level base widget
  
  if n_elements(parent) eq 0 then begin
     widparent = Widget_Base(Column=1, Title='ADAS 408 OUTPUT - EXTRA OPTIONS', $
                             XOFFSET=200, YOFFSET=100   )
  endif else begin              
     widparent = Widget_Base(Column=1, Title='ADAS 408 OUTPUT - EXTRA OPTIONS', $
                             XOFFSET=200, YOFFSET=100,                 $
                             group_leader=parent, modal=1)
  endelse




  topbase = widget_base(widparent, /row)
  base    = widget_base(topbase, /column)


; ACD
  
  ltop    = 'ACD: Recombination rate'
  buttons = ['Both (default)', $
             'Radiative recombination only', $
	    'Dielectronic recombination only' ]
  
  acdID   = cw_bgroup(base,buttons,                          $
                      label_top=ltop,                        $
                      exclusive=1,column=1,                  $
                      event_func='ADAS_PROC_NULL_EVENTS',    $
                      font=font_small    )

  lab     = widget_label(base,font=font_large, value=' ')
  

; PRB

  ltop    = 'PRB: Continuum power'
  buttons = ['All (default)', $
             'Radiative recombination only', $
             'Dielectronic recombination only', $
	     'Bremsstrahlung only']
  
  prbID   = cw_bgroup(base,buttons,                          $
                      label_top=ltop,                        $
                      exclusive=1,column=1,                  $
                      event_func='ADAS_PROC_NULL_EVENTS',    $
                      font=font_small    )


; The usual buttons at the bottom
                
  mrow      = widget_base(widparent,/row,/align_center)
  messID    = widget_label(mrow,font=font_large, $
                          value='                         ')
                
  mrow      = widget_base(widparent,/row)
  
  cancelID  = widget_button(mrow,value='Cancel',font=font_large)
  doneID    = widget_button(mrow,value='Done',font=font_large)



; Initial settings
   
   widget_control, acdID, set_value = 0             
   widget_control, prbID, set_value = 0             
     



;------------------------------------------------
; Realize the ADAS408 extra options input widget.

   dynlabel, widparent
   widget_Control, widparent, /realize

; Create a pointer to store the information collected from the form.
; The initial data stored here is set to CANCEL, so nothing needs to
; be done if the user kills the widget with the mouse.

   ptrToFormData = Ptr_New({cancel:1})

; Create an info structure with program information.

  info = { doneID          :  doneID,             $
           messID          :  messID,             $
           acdID           :  acdID,              $
           prbID           :  prbID,              $
           font_large      :  font_large,         $
           procval         :  procval,            $
           ptrToFormData   :  ptrToFormData       }  
               
; Store the info structure in the user value of the top-level base and launch
; the widget into the world.

  widget_control, widparent, Set_UValue=info

  XManager, 'ADAS408_in_opt', widparent, Event_Handler='ADAS408_IN_OPT_EVENT'


; When the widget is destroyed, the block is released, and we
; return here. Get the data that was collected by the widget
; and stored in the pointer location. Finally free the pointer.

formdata = *ptrToFormData

rep='CONTINUE'
IF N_Elements(formdata) EQ 0 THEN BEGIN
   Ptr_Free, ptrToFormData
   rep ='CANCEL'
   RETURN,rep
ENDIF
 
IF formdata.cancel EQ 1 THEN BEGIN
   Ptr_Free, ptrToFormData
   rep ='CANCEL'
ENDIF

if rep eq 'CONTINUE' then begin
   procval.itypacd = formdata.itypacd
   procval.itypprb = formdata.itypprb
   Ptr_Free, ptrToFormData
endif       

RETURN,rep

END
