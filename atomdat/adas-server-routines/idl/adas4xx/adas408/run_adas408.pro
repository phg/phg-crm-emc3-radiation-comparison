;----------------------------------------------------------------------
;+
; PROJECT    :  ADAS
;
; NAME       :  run_adas408
;
; PURPOSE    :  Runs ADAS408 ionisation balance code as an IDL
;               subroutine.
;
; ARGUMENTS  :  All output arguments will be defined appropriately.
;
;               NAME      I/O    TYPE   DETAILS
; INPUTS     :  adf03      I     str    ADAS atompars data file.
;               adf35      I     str    ADAS filter file.
;               te_min     I     real   lower temperature requested.
;               te_max     I     real   higher temperature requested.
;               te_num     I     real   number of temperatures requested.
;               dens_min   I     real   lower density requested.
;               dens_max   I     real   higher density requested.
;               dens_num   I     real   number of densities requested.
;               te_list    I     real() temperature vector requested (optional)
;               dens_list  I     real() density vector requested (optional)
;               elem       I     str    element (default - read from adf03).
;               mh         I     real   Hydrogen isotope mass.
;               mz         I     real   Element isotope mass.
;               class      I     str()  adf11 classes required (default all).
;               passdir    I     str    Output file directory.
;               ext        I     str    Filename extension (default .pass).
;               filter     I     str    Filter extension.
;               year       I     int    Year of output adf11 data (default 89).
;               log        I     str    name of output text file (default none).
;               acdtype    I     int    0 : All processes (default)
;                                       1 : Radiative recombination only
;                                       2 : Dielectronic recombination only
;               prbtype    I     int    0 : All processes (default)
;                                       1 : Radiative recombination only
;                                       2 : Dielectronic recombination only
;                                       3 : Bremsstrahlung only
;
;
; KEYWORDS      pbar               -    Display a progress bar when running.
;               help               -    If specified this comment section
;                                       is written to screen.
;
; OUTPUTS    :
;
; NOTES      :  run_adas408 uses the spawn command. Therefore IDL v5.3
;               cannot be used. Any other version should work.
;
;               te_list and dens_list can be used to sepcify a vector
;               of temperatures and densities. If given they superceed
;               the min/max values.
;
; AUTHOR     :  Martin O'Mullane
;
; DATE       :  09-09-2003
;
; MODIFIED
;       1.1     Martin O'Mullane
;                 - First version.
;       1.2     Martin O'Mullane
;                 - Indexing error when setting classes.
;                 - Extra options for acd and prb process selection.
;                 - Option to not show the progress bar.
;                 - Add /help option.
;                 - Add a read from fortran at end of output file
;                   writing. Otherwise IDL can exit (when invoked
;                   as run_adas408) and kill the fortran process
;                   before the files are written.
;       1.3     Martin O'Mullane
;                 - Add ZCD, YCD and ECD classes.
;       1.4     Martin O'Mullane
;                 - Allow the option for Te and density to be
;                   given as vectors rather than  min/max pairs.
;       1.5     Martin O'Mullane
;                 - If a filter is supplied only output the (power)
;                   files in the class input list. Give an on-screen
;                   reminder that the filtered and unfiltered versions
;                   will be written.
;
; DATE
;       1.1     09-09-2003
;       1.2     07-02-2005
;       1.3     06-10-2006
;       1.4     07-02-2010
;       1.5     20-02-2017
;
;-
;----------------------------------------------------------------------


PRO run_adas408, adf03     =  adf03,     $
                 adf35     =  adf35,     $
                 te_min    =  te_min,    $
                 te_max    =  te_max,    $
                 te_num    =  te_num,    $
                 dens_min  =  dens_min,  $
                 dens_max  =  dens_max,  $
                 dens_num  =  dens_num,  $
                 te_list   =  te_list,   $
                 dens_list =  dens_list, $
                 elem      =  elem,      $
                 mh        =  mh,        $
                 mz        =  mz,        $
                 class     =  class,     $
                 passdir   =  passdir,   $
                 ext       =  ext,       $
                 year      =  year,      $
                 filter    =  filter,    $
                 acdtype   =  acdtype,   $
                 prbtype   =  prbtype,   $
                 log       =  log,       $
                 pbar      =  pbar,      $
                 help      =  help


; If asked for help

if keyword_set(help) then begin
   doc_library, 'run_adas408'
   return
endif



; First check that we are not using IDL v5.3

if !version.release EQ '5.3' then begin
   message, 'This version of IDL (v5.3) will not work - ' + $
            'change to v5.0, v5.1, v5.2 or v5.4'
   return
endif


; Check that we get all inputs and that they are correct. Otherwise print
; a message and return to command line.

on_error, 2


; Input files

if n_elements(adf03) EQ 0 then begin
    message,'An atompars data file (adf03) is required'
endif else begin

   file_acc, adf03, exist, read, write, execute, filetype
   if NOT (exist EQ 1 AND read EQ 1 AND filetype EQ '-') then $
      message, 'adf03 file does not exist : ' + adf03

endelse

if n_elements(adf35) EQ 0 then begin
   adf35 = 'NULL'
endif else begin

   file_acc, adf35, exist, read, write, execute, filetype
   if NOT (exist EQ 1 AND read EQ 1 AND filetype EQ '-') then $
      message, 'adf35 file does not exist : ' + adf35

   if n_elements(filter) EQ 0 then begin
      print, 'Using filtered as a default filter name'
      filter = 'filtered'
   endif

endelse

; Temperature and density

if n_elements(te_list) EQ 0 then begin

   if n_elements(te_min) eq 0 then message, 'A lower temperature value is required'
   if n_elements(te_max) eq 0 then message, 'An upper temperature value is required'
   if n_elements(te_num) eq 0 then message, 'The number of temperatures is required'

   te_list = adas_vector(low=te_min, high=te_max, num=te_num)

endif else te_num = n_elements(te_list)

if n_elements(dens_list) EQ 0 then begin

   if n_elements(dens_min) eq 0 then message, 'A lower density value is required'
   if n_elements(dens_max) eq 0 then message, 'An upper density value is required'
   if n_elements(dens_num) eq 0 then message, 'The number of densities is required'

   dens_list = adas_vector(low=dens_min, high=dens_max, num=dens_num)

endif else dens_num = n_elements(dens_list)

; Te/dens size limit

if dens_num GT 55 then message, 'More than 55 densities requested'
if te_num GT 55 then message, 'More than 55 temperatures requested'


; Element

if n_elements(elem) EQ 0 then begin

    iz0 = -1L
    openr, lun, adf03, /get_lun
    readf, lun, iz0
    free_lun, lun
    elem = strcompress(strlowcase(xxesym(iz0)), /remove_all)
    print, 'Element from adf03 file : ' + elem

endif else begin

    symbol = strcompress(elem, /remove_all)
    if (symbol eq '') then begin
        message, 'Enter a valid  isonuclear element symbol'
    endif else begin
        i4eiz0, symbol, atnumber
        if (atnumber eq 0) then  message, 'Enter a valid element'
    endelse

endelse


; Output options

if n_elements(acdtype) EQ 0 then iacdtype = 0L else iacdtype = long(acdtype)
if n_elements(prbtype) EQ 0 then iprbtype = 0L else iprbtype = long(prbtype)



if n_elements(year) EQ 0 then begin
    print, 'Using 89 as a default year'
    year = 89
endif

if n_elements(ext) EQ 0 then begin
    print, 'Using .pass as a default extension'
    extension = 'pass'
endif else extension = ext


if n_elements(passdir) EQ 0 then begin
    print, 'Output will be sent to current working directory'
    passdir = ''
endif

if n_elements(class) EQ 0 then begin

    print, 'All data classes will be produced'
    cl_in = lonarr(13) + 1
    if adf35 eq 'NULL' then cl_in[[3,4,6]] = 0L

endif else begin

    cl_in = lonarr(13)

    for j = 0, n_elements(class)-1 do begin

       case strlowcase(class[j]) of
          'acd' : cl_in[0]  = 1
          'scd' : cl_in[1]  = 1
          'ccd' : cl_in[2]  = 1
          'plt' : cl_in[8]  = 1
          'prb' : cl_in[7]  = 1
          'prc' : cl_in[9]  = 1
          'pls' : cl_in[5]  = 1
          'zcd' : cl_in[10] = 1
          'ycd' : cl_in[11] = 1
          'ecd' : cl_in[12] = 1
       endcase

    endfor
    
    if adf35 NE 'NULL' then begin
       
       for j = 0, n_elements(class)-1 do begin

          case strlowcase(class[j]) of
             'plt' : cl_in[4]  = 1
             'prb' : cl_in[3]  = 1
             'prc' : cl_in[6]  = 1
             else  :
          endcase

       endfor
       
       if total(cl_in[[3,4,6]]) GT 0 then begin
          message, 'Filtered and unfiltered power datasets will be written', /continue
       endif

    endif

endelse


if cl_in[2] EQ 1 OR cl_in[6] EQ 1 OR cl_in[9] EQ 1 then begin
   if n_elements(mh) EQ 0 then message, 'Hydrogen mass required'
   if n_elements(mZ) EQ 0 then message, 'Element mass required'
endif else begin
   mh = 1.0
   mZ = 1.0
endelse


; Now run the ADAS408 code - the input has been checked already.

    fortdir = getenv('ADASFORT')
    spawn, fortdir+'/adas408.out', unit=pipe, /noshell, PID=pid

    date = xxdate()
    printf, pipe, date[0]

    header = 'run_adas408 : ' + ' DATE: ' + date[0] + ' TIME: ' + date[1]
    printf, pipe, header

    rep = 'CONTINUE'
    printf, pipe, rep

    printf, pipe, adf03
    printf, pipe, adf35


    rep = 'CONTINUE'
    printf, pipe, rep

    te_num    = long(te_num)
    dens_num  = long(dens_num)
    te_list   = double(te_list)
    dens_list = double(dens_list)
    mh        = double(mh)
    mz        = double(mz)

    printf, pipe, 'IDL run_adas408 version'

    printf, pipe, 2L
    printf, pipe, te_num
    for j = 0, te_num-1 do printf, pipe, te_list[j]

    printf, pipe, dens_num
    for j = 0, dens_num-1 do printf, pipe, dens_list[j]

    printf, pipe, mz
    printf, pipe, mh


    if keyword_set(pbar) then begin
       adas_progressbar, pipe     = pipe,         $
                         n_call   = te_num,       $
                         adasprog = 'run_adas408'
    endif else begin
       idum = 0L
       for i=1, te_num do readf, pipe, idum
    endelse

    rep = 'CONTINUE'
    printf, pipe, rep


    file_ext = string(year) + '_' + $
               elem      + '.' + $
               extension

    if adf35 NE 'NULL' then begin
        file_ext_filter = string(year) + '_' + $
                          elem         + '.' + $
                          filter       + '.' + $
                          extension
    endif else file_ext_filter = ''

    file_ext_filter = strcompress(strlowcase(file_ext_filter), /remove_all)
    file_ext        = strcompress(strlowcase(file_ext), /remove_all)

    printf, pipe, file_ext
    printf, pipe, file_ext_filter

    ; add trailing / to directory name

    dir = passdir
    if strlen(dir) GT 1 then dir = dir + '/'
    if dir EQ ' ' then dir  = ''
    printf, pipe, dir

    printf, pipe, iacdtype
    printf, pipe, iprbtype

    if n_elements(log) GT 0 then begin
       printf, pipe, 1L
       printf, pipe, log
    endif else begin
        printf, pipe, 0L
    endelse


    for j = 0, 12 do printf, pipe, cl_in[j]


; For some reason it's necessary to pause here - otherwise the fortran
; completes without writing anything. Why? Buffer problems?

    wait, 5


; Finish up

    rep = 'MENU'
    printf, pipe, rep

    readf, pipe, rep
    if rep EQ 'EXIT NOW' then close, /all


END
