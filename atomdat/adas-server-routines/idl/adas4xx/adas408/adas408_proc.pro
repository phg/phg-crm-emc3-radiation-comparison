; Copyright (c) 2003, Strathclyde University .
; SCCS info: Module @(#)$Header: /home/adascvs/idl/adas4xx/adas408/adas408_proc.pro,v 1.3 2004/07/06 10:47:05 whitefor Exp $ Date $Date: 2004/07/06 10:47:05 $
;+
; PROJECT:
;       ADAS 
;
; NAME:
;	ADAS408_PROC
;
; PURPOSE:
;	IDL ADAS user interface, processing options/input.
;
; EXPLANATION:
;	This routine creates and manages a pop-up window which allows
;	the user to select options and input data to control ADAS408
;	processing.
;
; USE:
;	This routine is ADAS408 specific.
;
; INPUTS:
;	VALUE	- A structure which determines the initial settings of
;		  the processing options widget.
;
;
;       BITFILE - String; the path to the directory containing bitmaps
;                 for the 'escape to series menu' button.
;
;
; OPTIONAL INPUTS:
;	None
;
; OUTPUTS:
;	VALUE	- On output the structure records the final settings of
;		  the processing selections widget if the user pressed the
;		  'Done' button, otherwise it is not changed from input.
;
;	REP	- String; Either 'CANCEL' or 'MENU' or 'CONTINUE' depending
;		  on how the user terminated the processing options
;		  window.
;
; OPTIONAL OUTPUTS:
;	None
;
; KEYWORD PARAMETERS:
;	FONT_LARGE - The name of a larger font.
;
;	FONT_SMALL - The name of a smaller font.
;
;	EDIT_FONTS - A structure of two fonts used in the table editor
;		     adas_edtab.pro, {font_norm:'',font_input:''}.
;
; CALLS:
;	DYNLABEL	Sets dynamic_resize keyword for label widgets.
;	ADAS408_PROC_EVENT Called indirectly during widget management,
;			   routine included in this file.
;	XMAMAGER
;
; SIDE EFFECTS:
;
;
; CATEGORY:
;	Widgets
;
; WRITTEN:
;       Martin O'Mullane
;
; MODIFIED:
;       1.1     Tim Hammond
;                - First version
;       1.2     William Osborn
;                - Added dynlabel procedure
;	1.3	Martin O'Mullane
;		 - New version which shares nothing with the previous ones.
;
; VERSION:
;       1.1     04-04-96
;       1.2     09-07-96
;       1.3	21-07-2003
;
;-
;-----------------------------------------------------------------------------

PRO ADAS408_PROC_MENU, event

; The 'normal' get_value from a widget_button does not work with a
; bitmap button. Hence this specialised event handler.

; Get the info structure out of the user value of the top-level base.

Widget_Control, event.top, Get_UValue=info


 formdata = {cancel : 0, menu:1}
 *info.ptrToFormData = formdata
 widget_Control, event.top, /destroy

END

;-----------------------------------------------------------------------------


PRO adas408_proc_event, event

; Get the info structure out of the user value of the top-level base.

Widget_Control, event.top, Get_UValue=info


; Clear any messages 

widget_control, info.messID, set_value = ' '


; Process events


CASE event.id OF

  info.cancelID : begin
                    formdata = {cancel:1}
                    *info.ptrToFormData = formdata
                    widget_Control, event.top, /destroy
                  end 
   

   info.doneID  : begin

                   ; Gather the data for return to calling program
                   ; but do some consistency checks first
                
                    err  = 0
                    mess = ''
                    
                    ; Title of run
                    
                    widget_control, info.runID, get_value=title
                    
                    
                    ; Scan parameters
                    
                    widget_control, info.te_minID, get_value=tmp
                    te_min = tmp[0]
                    widget_control, info.te_maxID, get_value=tmp
                    te_max = tmp[0]
                    widget_control, info.te_numID, get_value=tmp
                    te_num = tmp[0]
                    
                    widget_control, info.te_untID, get_value=tmp
                    if tmp[0] EQ 0 then ifout = 2 else ifout = 1 
                                        
                    widget_control, info.dens_minID, get_value=tmp
                    dens_min = tmp[0]
                    widget_control, info.dens_maxID, get_value=tmp
                    dens_max = tmp[0]
                    widget_control, info.dens_numID, get_value=tmp
                    dens_num = tmp[0]
                                        
                    widget_control, info.mass_impID, get_value=tmp
                    mass_imp = tmp[0]
                    widget_control, info.mass_hydID, get_value=tmp
                    mass_hyd = tmp[0]
                                 
                                             
                    ; Return information
                                           
                    if err EQ 0 then begin
                    
                       formdata = {title    : title,        $  
                                   te_min   : te_min,       $    
                                   te_max   : te_max,       $    
                                   te_num   : te_num,       $    
                                   ifout    : ifout,        $  
                                   dens_min : dens_min,     $    
                                   dens_max : dens_max,     $    
                                   dens_num : dens_num,     $    
                                   mass_imp : mass_imp ,    $    
                                   mass_hyd : mass_hyd ,    $    
                                   menu     : 0,            $  
                                   cancel   : 0             }  
                        *info.ptrToFormData = formdata
                        widget_control, event.top, /destroy
                    
                    endif else begin
                       widget_Control, info.messID, Set_Value=mess
                    endelse

                  end
                   
  
   ELSE :  print,'ADAS408_PROC_EVENT : You should not see this message! '
  
ENDCASE


END

;-----------------------------------------------------------------------------


FUNCTION adas408_proc, value,                     $
                       iz0,                       $
                       dsn03,                     $
                       dsn35,                     $
                       bitfile,                   $
                       FONT_LARGE = font_large,   $    
                       FONT_SMALL = font_small

  

; Set defaults for keywords

  if not keyword_set(font_large) then font_large = ''
  if not keyword_set(font_small) then font_small = ''



; Worry about special HP case

  machine = GETENV('TARGET_MACHINE')  
  if machine eq 'HPUX' then begin     
      y_size = 4                      
      large_font = font_small         
  endif else begin                    
      y_size = 6                      
      large_font = font_large         
  endelse                             




; Create top level base widget

  parent = widget_base(Column=1, Title='ADAS408 PROCESSING OPTIONS', $
                       xoffset=50, yoffset=1)

  rc     = widget_label(parent,value='  ',font=font_small)

  base   = widget_base(parent, /column)


; Information: Run title and filenames

  mbase = widget_base(base, /row)
  rc    = widget_label(mbase, value='Title for Run', font=font_large)
  runID = widget_text(mbase,value=value.title, xsize=40, font=font_large,  $
                            /edit, EVENT_FUNC = 'ADAS_PROC_NULL_EVENTS')

  rc     = widget_label(base,value='  ',font=font_small)

 
  mbase  = widget_base(base,/column,/frame)
    
  lab = cw_adas_dsbr(mbase, dsn03, font=font_small, $
                            filelabel='adf03 file : ',/row)
  if dsn35 EQ 'NULL' then begin
     lab = widget_label(mbase, value='No filter file', font=font_small)
  endif else begin
     lab = cw_adas_dsbr(mbase, dsn35, font=font_small, $
                               filelabel='adf35 file : ',/row)
  endelse
  
  
  mbase   = widget_base(base,/row)


; Temperatures


  tebase   = widget_base(mbase,/column,/frame)

  mlab     = widget_label(tebase,value='Temperature limits',font=font_large)


  te_minID = cw_field(tebase, title='Lower limit   :', xsize=10, $
                     floating=1, fieldfont=font_small, font=font_small)
                      
  te_maxID = cw_field(tebase, title='Upper limit   :',  xsize=10, $
                     floating=1, fieldfont=font_small, font=font_small)
                      
  te_numID = cw_field(tebase, title='No. of temps. :',  xsize=10, $
                     integer=1, fieldfont=font_small, font=font_small)
                      
  te_untID = cw_bgroup(tebase, ['eV', 'K'], label_left='Units        :',   $
                       set_value=0,                                        $
                       /no_release, /exclusive,  /row,                     $
                       event_funct = 'ADAS_PROC_NULL_EVENTS',              $
                       font = font_large)

; Densities


  densbase   = widget_base(mbase,/column,/frame)

  mlab       = widget_label(densbase,value='Density limits',font=font_large)


  dens_minID = cw_field(densbase, title='Lower limit  :',  xsize=11, $
                     floating=1, fieldfont=font_small, font=font_small)
                      
  dens_maxID = cw_field(densbase, title='Upper limit  :',  xsize=11, $
                     floating=1, fieldfont=font_small, font=font_small)
                      
  dens_numID = cw_field(densbase, title='No. of dens. :',  xsize=11, $
                     integer=1, fieldfont=font_small, font=font_small)
                      

; Masses

  massbase   = widget_base(mbase,/column,/frame)

  mlab       = widget_label(massbase,value='Mass data',font=font_large)
  lab        = widget_label(massbase,font=font_small, $
                            value='Nuclear Charge : ' + strcompress(string(iz0)))


  mass_impID = cw_field(massbase, title='Impurity  :',  xsize=8, $
                        floating=1, fieldfont=font_small, font=font_small)
                      
  mass_hydID = cw_field(massbase, title='Neutral H :',  xsize=8, $
                        floating=1, fieldfont=font_small, font=font_small)
                      
                



; End of panel message and buttons                

  message  = 'Edit the scan information data and press Done to proceed'

  messID   = widget_label(parent, value=message, font=font_large)
                            
  menufile = bitfile + '/menu.bmp'
  read_X11_bitmap, menufile, bitmap1
                
  mrow     = widget_base(parent,/row)
  menuID   = widget_button(mrow,value=bitmap1,font=font_large, $
                           event_pro='ADAS408_PROC_MENU')
  cancelID = widget_button(mrow,value='Cancel',font=font_large)
  doneID   = widget_button(mrow,value='Done',font=font_large)




; Initialise the widget

  widget_control, runID, set_value=value.title
  
  if value.te_min NE 0.0 then widget_control, te_minID, set_value=value.te_min
  if value.te_max NE 0.0 then widget_control, te_maxID, set_value=value.te_max
  if value.te_num NE 0 then widget_control, te_numID, set_value=value.te_num
  
  if value.ifout EQ 1 then widget_control, te_untID, set_value=1
  if value.ifout EQ 2 then widget_control, te_untID, set_value=0

  if value.dens_min NE 0.0 then widget_control, dens_minID, set_value=value.dens_min
  if value.dens_max NE 0.0 then widget_control, dens_maxID, set_value=value.dens_max
  if value.dens_num NE 0 then widget_control, dens_numID, set_value=value.dens_num

  if value.mass_imp NE 0.0 then widget_control, mass_impID, set_value=value.mass_imp
  if value.mass_hyd NE 0.0 then widget_control, mass_hydID, set_value=value.mass_hyd


; Realize the ADAS408_PROC input widget.


   dynlabel, parent
   widget_Control, parent, /realize



; Create a pointer to store the information collected from the form.
; The initial data stored here is set to CANCEL, so nothing needs to
; be done if the user kills the widget with the mouse.

   ptrToFormData = Ptr_New({cancel:1, menu:0})

; Create an info structure with program information.

  info = { runID           :   runID,          $  
           te_minID        :   te_minID,       $    
           te_maxID        :   te_maxID,       $     
           te_numID        :   te_numID,       $     
           te_untID        :   te_untID,       $     
           dens_minID      :   dens_minID,     $   
           dens_maxID      :   dens_maxID,     $   
           dens_numID      :   dens_numID,     $   
           mass_impID      :   mass_impID ,    $  
           mass_hydID      :   mass_hydID ,    $  
           messID          :   messID,         $     
           menuID          :   menuID,         $     
           cancelID        :   cancelID,       $
           doneID          :   doneID,         $
           ptrToFormData   :   ptrToFormData   } 
  
           
; Store the info structure in the user value of the top-level base and launch
; the widget into the world.

  widget_control, parent, Set_UValue=info

  XManager, 'ADAS408_PROC', parent, Event_Handler='ADAS408_PROC_EVENT'


; When the widget is destroyed, the block is released, and we
; return here. Get the form data that was collected by the form
; and stored in the pointer location.

formdata = *ptrToFormData

rep='CONTINUE'
IF N_Elements(formdata) EQ 0 THEN BEGIN
   Ptr_Free, ptrToFormData
   rep ='CANCEL'
   RETURN,rep
ENDIF
 
IF formdata.cancel EQ 1 THEN BEGIN
   Ptr_Free, ptrToFormData
   rep ='CANCEL'
   RETURN,rep
ENDIF

IF formdata.menu EQ 1 THEN BEGIN
   Ptr_Free, ptrToFormData
   rep ='MENU
   RETURN,rep
ENDIF


if rep eq 'CONTINUE' then begin
   
   value.title    =  formdata.title  
   value.te_min   =  formdata.te_min   
   value.te_max   =  formdata.te_max   
   value.te_num   =  formdata.te_num   
   value.ifout    =  formdata.ifout   
   value.dens_min =  formdata.dens_min 
   value.dens_max =  formdata.dens_max 
   value.dens_num =  formdata.dens_num 
   value.mass_imp =  formdata.mass_imp 
   value.mass_hyd =  formdata.mass_hyd 
   
   Ptr_Free, ptrToFormData

endif       


RETURN, rep


END
