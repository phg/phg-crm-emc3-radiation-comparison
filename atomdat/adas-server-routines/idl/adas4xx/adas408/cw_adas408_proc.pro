; Copyright (c) 1996, Strathclyde University.
; SCCS info: Module @(#)$Header: /home/adascvs/idl/adas4xx/adas408/cw_adas408_proc.pro,v 1.2 2004/07/06 12:48:01 whitefor Exp $	Date  $Date: 2004/07/06 12:48:01 $
;+
; PROJECT:
;       ADAS IBM MVS to UNIX conversion
;
; NAME:
;	CW_ADAS408_PROC()
;
; PURPOSE:
;	Produces a widget for ADAS408 processing options/input.
;
; EXPLANATION:
;	This function creates a compound widget consisting of :-
;	   a text widget holding an editable 'Run title', 
;	   the dataset name/browse widget cw_adas_dsbr,
;	   a message widget, 
;          an 'Escape to series menu', a 'Cancel' and a 'Done' button.
;
;	The compound widgets included in this widget are self managing.
;	This widget only manages events from the 'Cancel' button, the 
;	'Done' button and the 'escape to series menu' button.
;
; USE:
;	This widget is specific to ADAS408, see adas408_proc.pro
;	for use.
;
; INPUTS:
;	TOPPARENT- Long integer, ID of parent widget.
;
;	DSFULL	- String; The full system file name of the input 
;		  dataset selected by the user for processing.
;
;       BITFILE - String; the path to the directory containing bitmaps
;                 for the 'escape to series menu' button.
;
; OPTIONAL INPUTS:
;	None
;
; OUTPUTS:
;	The return value of this function is the ID of the declared widget.
;
; OPTIONAL OUTPUTS:
;	None
;
; KEYWORD PARAMETERS:
;	PROCVAL	- A structure which determines the initial settings of
;		  the processing options widget. See d8ispf.pro for details.
;
;	UVALUE	- A user value for the widget. Default 0.
;
;	FONT_LARGE - The name of a larger font.  Default current system
;		     font
;
;	FONT_SMALL - The name of a smaller font. Default current system
;		     font.
;
;	EDIT_FONTS - A structure of two fonts used in the table editor
;		     adas_edtab.pro, {font_norm:'',font_input:''}.  Defaults
;		     to current system font.
;
;	NUM_FORM   - String; Numeric format to use in tables.  Default
;			'(E10.3)'
;		     
;
; CALLS:
;	CW_ADAS_DSBR	Dataset name and comments browsing button.
;	I4Z0IE		Converts atomic number to element symbol.
;	filter_name	Defined in this file. Constructs the filter
;			name from the state of the widget.
;
;	See side effects for other related widget management routines.
;
; SIDE EFFECTS:
;
;	The following widget management routines are included in this file;
;	PROC408_GET_VAL()	Returns the current PROCVAL structure.
;	PROC408_EVENT()		Process and issue events.
;	
; CATEGORY:
;	Compound Widget
;
; WRITTEN:
;       Tim Hammond, Tessella Support Services plc, 4th April 1996
;
; MODIFIED:
;	1.1	Tim Hammond + William Osborn
;		First version
;
; VERSION:
;	1.1	04-04-96
;
; VERSION: 1.2 					DATE: 23-03-97
; MODIFIED:	MARTIN O'MULLANE & RICHARD MARTIN
;		Changed filter name to allow Be in microns and Si in mm.
;		Corrected temperature units logic from (eV =0, Kelvin=1)
;		to (Kelvin=0, eV=1) which is as in all other routines.
;-
;-----------------------------------------------------------------------------

FUNCTION filter_name, state

		;***************************************************
		;**** Routine to construct and set the filter   ****
		;**** name from the current x-ray filter        ****
		;**** settings. Does what D8PAN2.FOR used to do	****
		;***************************************************

    widget_control, state.levcutid, get_value = levcutval

    if levcutval eq 0 then begin
		;**** If the simple cut-off is selected ... ****
    	widget_control, state.ievcutid, get_value=evval
	if num_chk(evval(0)) eq 0 then ev = fix(evval(0)) else ev=0
        evval='0000'+strtrim(string(ev),2)
	evval=strmid(evval,strlen(evval)-4,4)
	fname='ev'+evval
    endif else begin
		;**** Otherwise ... ****
        widget_control, state.thbeid, get_value=beval
    	widget_control, state.thsiid, get_value=sival
    	beval=strtrim(fix(beval(0)),2)
        cbe='000'
        i=0
        case strlen(beval) of 
          1  : i=2
          2  : i=1
        else : i=0
        endcase
        strput,cbe,beval,i
;
    	sival=strtrim(fix(sival(0)),2)
        csi='00'
        i=0
        if strlen(sival) lt 2 then i=1
        strput,csi,sival,i
;
   	fname='ft'+cbe+csi
    endelse

    widget_control, state.fnameid, set_value=fname

    RETURN, fname
END

;-----------------------------------------------------------------------------

FUNCTION proc408_get_val, id

    COMMON proc408_block, iz0, itdimd, iddimd, ievcut, thbe, thsi, symbol,$
			  fname

                ;**** Return to caller on error ****

    ON_ERROR, 2

		 ;***************************************
                 ;****     Retrieve the state 	     ****
		 ;**** Get first_child widget id     ****
		 ;**** because state is stored there ****
		 ;***************************************

    first_child = widget_info(id, /child)
    widget_control, first_child, get_uvalue = state

		;***********************************
		;**** Get run title from widget ****
		;**** Then centre in in string  ****
		;**** of 40 characters 		****
		;***********************************

    widget_control, state.runid, get_value=title
    title_len = strlen(title(0)) 
    if (title_len gt 40 ) then begin 
	title = strmid(title,0,38)
        widget_control, state.messid, set_value="Title too long - truncated "
        widget_control, state.runid, set_value=title
	wait, 1
    endif
    pad = (40 - title_len)/2 
    spaces = '                                         '
    title(0) = strmid(spaces,0,(pad-1)) + title(0) + strmid(spaces,0,(pad-1))

		;**** Get mass values ****

    widget_control, state.amsiid, get_value=amsival
    widget_control, state.amshid, get_value=amshval
    
		;**** Get X-ray values ****

    widget_control, state.levcutid, get_value=levcutval
    widget_control, state.ievcutid, get_value=ievcut
    if num_chk(ievcut(0)) eq 0 then ievcutval=fix(ievcut(0)) else ievcutval=0
    widget_control, state.thbeid, get_value=beval
    if num_chk(beval(0)) eq 0 then thbeval = fix(beval(0)) else thbeval=0
    widget_control, state.thsiid, get_value=sival
    if num_chk(sival(0)) eq 0 then thsival = fix(sival(0)) else thsival=0

    fname = filter_name(state)

		;**** Get temp and dens limits ****

    widget_control, state.tinminid, get_value=tinminval
    widget_control, state.tinmaxid, get_value=tinmaxval
    widget_control, state.dinminid, get_value=dinminval
    widget_control, state.dinmaxid, get_value=dinmaxval

    widget_control, state.numtid, get_value=numtval
    widget_control, state.numdid, get_value=numdval

    widget_control, state.unitid, get_value=ittyp

		;***********************************************
		;**** Write selected values to PS structure ****
		;***********************************************

     ps = {  	NEW  	: 	0, 					$
                TITLE 	: 	title,         				$
		SYMBOL	: 	symbol,					$
		AMSI	:	float(amsival(0)),			$
		AMSH	:	float(amshval(0)),			$
		LEVCUT	:	levcutval,				$
		IEVCUT	:	ievcutval,				$
		THBE	:	fix(thbeval(0)),			$
		THSI	:	fix(thsival(0)),			$
		FNAME	:	fname,				 	$
		TINMIN	:	float(tinminval(0)),			$
		TINMAX	:	float(tinmaxval(0)),			$
		NUMT	:	fix(numtval(0)),			$
		DINMIN	:	float(dinminval(0)),			$
		DINMAX	:	float(dinmaxval(0)),			$
		NUMD	:	fix(numdval(0)),			$
		ITTYP	:	ittyp,					$
		ITDIMD	:	itdimd,					$
		IDDIMD	:	iddimd,					$
		IZ0	:	iz0					}
   
    widget_control, first_child, set_uvalue=state, /no_copy

    RETURN, ps

END

;-----------------------------------------------------------------------------

FUNCTION proc408_event, event

    COMMON proc408_block, iz0, itdimd, iddimd, ievcut, thbe, thsi,	$
			  symbol, fname

                ;**** Base ID of compound widget ****

    parent = event.handler

		;**********************************************
                ;**** Retrieve the user value state        ****
		;**** Get id of first_child widget because ****
		;**** user value "state" is stored there   ****
		;**********************************************

    first_child = widget_info(parent, /child)
    widget_control, first_child, get_uvalue=state,/no_copy

		;*********************************
		;**** Clear previous messages ****
		;*********************************

    widget_control, state.messid, set_value=' '

                ;************************
                ;**** Process events ****
                ;************************

    CASE event.id OF

		;**********************
		;**** Mass typeins ****
		;**********************

	state.amsiid: widget_control, state.amshid, /input_focus
	state.amshid: begin
	    widget_control, state.levcutid, get_value=levcutval
	    if levcutval eq 0 then begin
		widget_control, state.ievcutid, /input_focus
	    endif else begin
		widget_control, state.thbeid, /input_focus
	    endelse
	end
		;*******************************
		;**** Temp and dens typeins ****
		;*******************************

	state.tinminid: widget_control, state.tinmaxid, /input_focus
	state.tinmaxid: widget_control, state.numtid, /input_focus
	state.numtid:	widget_control, state.dinminid, /input_focus
	state.dinminid: widget_control, state.dinmaxid, /input_focus
	state.dinmaxid: widget_control, state.numdid, /input_focus
	state.numdid:	widget_control, state.amsiid, /input_focus

		;**********************
		;**** X-ray toggle ****
		;**********************

	state.levcutid: begin
	    widget_control, state.levcutid, get_value=levcutval
	    widget_control, state.nobase, map=levcutval
  	    widget_control, state.yesbase, map=1-levcutval
	    fname = filter_name(state)
	    if levcutval eq 0 then begin
		widget_control, state.ievcutid, /input_focus
	    endif else begin
		widget_control, state.thbeid, /input_focus
	    endelse
	end

		;***********************
		;**** X-ray typeins ****	
		;***********************

	state.ievcutid: begin
	    	;**** Construct filter name ****
	    fname = filter_name(state)

		;**** Change input focus ****
	    if event.type eq 0 then if event.ch eq 10 then 		$
 	        widget_control, state.tinminid, /input_focus

	end
	    
	state.thbeid: begin

		;**** Construct filter name ****
	    fname = filter_name(state)

		;**** Change input focus ****
	    if event.type eq 0 then if event.ch eq 10 then 		$
 	        widget_control, state.thsiid, /input_focus

	end

	state.thsiid: begin
		;**** Construct filter name ****
	    fname = filter_name(state)

		;**** Change input focus ****
	    if event.type eq 0 then if event.ch eq 10 then 		$
 	        widget_control, state.tinminid, /input_focus

	end

		;***********************
		;**** Unit selector ****
		;***********************

	state.unitid: begin

	    widget_control, state.unitid, get_value = unit
	    widget_control, state.tinminid, get_value = tinminval
	    widget_control, state.tinmaxid, get_value = tinmaxval
	    if unit eq 0 then begin
		tval1 = 'Electron Temperature (Kelvin)'
		widget_control, state.tinminid,set_value=		$
		    string(float(tinminval)*11605.4,format = '(e8.2)')
		widget_control, state.tinmaxid,set_value=		$
		    string(float(tinmaxval)*11605.4,format = '(e8.2)')
	    endif else begin
		tval1 = 'Electron Temperature (eV)    '
		widget_control, state.tinminid,set_value=		$
		    string(float(tinminval)/11605.4,format = '(e8.2)')
		widget_control, state.tinmaxid,set_value=		$
		    string(float(tinmaxval)/11605.4,format = '(e8.2)')
	    endelse
	    widget_control, state.tlabid, set_value = tval1

	end

		;***********************
		;**** Cancel button ****
		;***********************

    	state.cancelid: new_event = {ID:parent, TOP:event.top, 		$
				     HANDLER:0L, ACTION:'Cancel'}

		;*********************
		;**** Done button ****
		;*********************

    	state.doneid: begin

		;***************************************
		;**** Check all user input is legal ****
		;***************************************

	    error = 0

		;**** Mass typeins ****

	    widget_control, state.amsiid, get_value=amsival
	    er_check = num_chk(amsival(0), sign=1)
	    if er_check ne 0 then begin
                error = 1
                message = '**** Error: You have entered an invalid im'+ $
                          'purity element isotopic mass ****'
                widget_control, state.amsiid, /input_focus
            endif else begin
                if float(amsival(0)) eq 0.0 then begin
                    error = 1
                    message = '**** Error: The impurity element isoto'+	$
			      'pic mass must be > 0 ****'
                    widget_control, state.amsiid, /input_focus
                endif
            endelse
	    if error eq 0 then begin
	        widget_control, state.amshid, get_value=amshval
	        er_check = num_chk(amshval(0), sign=1)
	        if er_check ne 0 then begin
                    error = 1
                    message = '**** Error: You have entered an invali'+ $
                              'd neutral hydrogen isotopic mass ****'
                    widget_control, state.amshid, /input_focus
                endif else begin
                    if float(amshval(0)) eq 0.0 then begin
                        error = 1
                        message = '**** Error: The neutral hydrogen i'+	$
			          'sotopic mass must be > 0 ****'
                        widget_control, state.amshid, /input_focus
                    endif
                endelse
	    endif

		;**** X-ray typeins ****

	    if error eq 0 then begin
	        widget_control, state.levcutid, get_value=levcut
		if levcut eq 0 then begin
		    widget_control, state.ievcutid, get_value=ievcutval
	            er_check = num_chk(ievcutval(0), sign=1)
		    if er_check ne 0 then begin
			error = 1
			message = '**** Error: You have entered an '+	$
                            'invalid energy cut-off  ****'
			widget_control, state.ievcutid, /input_focus
		    endif
		endif else begin
		    widget_control, state.thbeid, get_value=thbeval
		    er_check = num_chk(thbeval(0), /integer, sign=1) 
		    if er_check ne 0 then begin
                        error = 1
                        message = '**** Error: You have entered an '+   $
                                  'invalid Be thickness ****'
			widget_control, state.thbeid, /input_focus
		    endif else begin
			widget_control, state.thsiid, get_value=thsival
                        er_check = num_chk(thsival(0),/integer, sign=1)
                        if er_check ne 0 then begin
                            error = 1
                            message = '**** Error: You have entered an'+$
                                      ' invalid Si thickness ****'
                            widget_control, state.thsiid, /input_focus
			endif
                    endelse
		endelse
	    endif
    
		;**** Electron temp. limit typeins ****

	    if error eq 0 then begin
		widget_control,state.tinminid, get_value=tinminval
		er_check = num_chk(tinminval(0), sign=1)
		if er_check ne 0 then begin
                    error = 1
                    message = '**** Error: '+   $
                              'Invalid electron temperature lower limit ****'
		    widget_control, state.tinminid, /input_focus
		endif else begin
		    widget_control, state.tinmaxid, get_value=tinmaxval
                    er_check = num_chk(tinmaxval(0), sign=1)
                    if er_check ne 0 then begin
                        error = 1
                        message = '**** Error: '+   $
                            'Invalid electron temperature upper limit ****'
                        widget_control, state.tinmaxid, /input_focus
		    endif else begin
			tinmin=float(tinminval(0))
			tinmax=float(tinmaxval(0))
			if tinmax le tinmin then begin
			    error=1
			    message='**** Error: Electron temperature '+$
				'upper limit must be higher than lower limit'
			    widget_control, state.tinmaxid, /input_focus
			endif else begin
  		            widget_control, state.numtid, get_value=numtval
                            er_check = num_chk(numtval(0), /integer, sign=1)
                            if er_check ne 0 then begin
                            	error = 1
                            	message = '**** Error: '+   $
                                'Invalid number of electron temperatures ****'
                                widget_control, state.numtid, /input_focus
		            endif else if fix(numtval(0)) gt itdimd then begin
                            	error = 1
                            	message = '**** Error: Number of electron'+$
			              ' temperatures must be less than '+ $
			              strtrim(string(itdimd),2)+' ****'
                                widget_control, state.numtid, /input_focus
		            endif
		    	endelse
	            endelse
            	endelse
	    endif
		;**** Electron dens. limit typeins ****

	    if error eq 0 then begin
		widget_control,state.dinminid, get_value=dinminval
		er_check = num_chk(dinminval(0), sign=1)
		if er_check ne 0 then begin
                    error = 1
                    message = '**** Error: '+   $
                              'Invalid electron density lower limit ****'
		    widget_control, state.dinminid, /input_focus
		endif else begin
		    widget_control, state.dinmaxid, get_value=dinmaxval
                    er_check = num_chk(dinmaxval(0), sign=1)
                    if er_check ne 0 then begin
                        error = 1
                        message = '**** Error: '+   $
                            'Invalid electron density upper limit ****'
                        widget_control, state.dinmaxid, /input_focus
		    endif else begin
			dinmin=float(dinminval(0))
			dinmax=float(dinmaxval(0))
			if dinmax le dinmin then begin
			    error=1
			    message='**** Error: Electron density '+$
				'upper limit must be higher than lower limit'
			    widget_control, state.dinmaxid, /input_focus
			endif else begin
		    	    widget_control, state.numdid, get_value=numdval
                    	    er_check = num_chk(numdval(0), /integer, sign=1)
                    	    if er_check ne 0 then begin
                            	error = 1
                            	message = '**** Error: '+   $
                                'Invalid number of electron densities ****'
                            	widget_control, state.numdid, /input_focus
		    	    endif else if fix(numdval(0)) gt iddimd then begin
                            	error = 1
                            	message = '**** Error: Number of electron'+$
			              ' densities must be less than '+  $
			              strtrim(string(iddimd),2)+' ****'
                                widget_control, state.numdid, /input_focus
		    	    endif
		    	endelse
		    endelse
		endelse
	    endif

		;**** return value or flag error ****

	    if error eq 0 then begin
        	new_event = {ID:parent, TOP:event.top, 			$
                            HANDLER:0L, ACTION:'Done'}
            endif else begin
	        widget_control, state.messid, set_value=message
	        new_event = 0L
            endelse
        end

		;*********************
                ;**** Menu button ****
		;*********************

        state.outid: begin
            new_event = {ID:parent, TOP:event.top, HANDLER:0L,          $
                         ACTION:'Menu'}
        end

    ELSE: new_event = 0L

  ENDCASE

		;*********************************************
		;*** make "state" available to other files ***
		;*********************************************

    widget_control, first_child, set_uvalue=state, /no_copy

    RETURN, new_event

END

;-----------------------------------------------------------------------------

FUNCTION cw_adas408_proc, topparent, dsfull, bitfile, 			$
                          PROCVAL=procval, UVALUE=uvalue, 		$
                          FONT_LARGE=font_large, FONT_SMALL=font_small, $
                          EDIT_FONTS=edit_fonts, NUM_FORM=num_form

    COMMON proc408_block, iz0com, itdimdcom, iddimdcom, ievcutcom, 	$
                          thbecom, thsicom, symbol, fnamecom

		;**** Set defaults for keywords ****

    if not (keyword_set(uvalue)) then uvalue = 0
    if not (keyword_set(font_large)) then font_large = ''
    if not (keyword_set(font_small)) then font_small = ''
    if not (keyword_set(edit_fonts)) then 				$
    edit_fonts = {font_norm:'',font_input:''}
    if not (keyword_set(num_form)) then num_form = '(E10.3)'
    iz0com = procval.iz0
    itdimdcom = procval.itdimd
    iddimdcom = procval.iddimd
    ievcutcom = procval.ievcut
    thbecom = procval.thbe
    thsicom = procval.thsi
    fnamecom = procval.fname

                ;*********************************************************
                ;**** Modify certain parameters and results depending ****
                ;**** on the machine being used                       ****
                ;*********************************************************

    machine = GETENV('TARGET_MACHINE')
    if machine eq 'HPUX' then begin
        y_size = 4
        large_font = font_small
    endif else begin
        y_size = 6
        large_font = font_large
    endelse

		;********************************************************
		;**** Create the 408 Processing options/input window ****
		;********************************************************

		;**** create titled base widget ****

    parent = widget_base(topparent, UVALUE = uvalue, 			$
			 title = 'ADAS408 PROCESSING OPTIONS', 		$
			 EVENT_FUNC = "proc408_event", 			$
			 FUNC_GET_VALUE = "proc408_get_val", 		$
			 /COLUMN)

		;******************************************************
		;**** Create a dummy widget just to hold value of *****
		;**** "state" variable so as not to get confused  *****
		;**** with any other values. Adopt IDL practice   *****
		;**** of using first child widget                 *****
		;******************************************************

    first_child = widget_base(parent)
    topbase = widget_base(first_child,/column)

		;***********************
		;**** add run title ****
		;***********************

    base = widget_base(topbase, /row)
    rc = widget_label(base, value='Title for Run', font=large_font)
    runid = widget_text(base, value=procval.title, xsize=38, 		$
                        font=large_font, /edit)
  
		;**** add dataset name and browse button ****

    rc = cw_adas_dsbr(topbase, dsfull, font=large_font, /row)

		;**** Add a row base for the top two boxes ****

    topbase = widget_base(parent, /row)

		;**** Base for mass information ****

    massbase = widget_base(topbase, /column, /frame)
    massval = 'Please input mass information:-'
    masshead = widget_label(massbase, font=large_font, value=massval)
    massbuffer = widget_label(massbase, font=font_small, value=' ')
    massval2 = 'Impurity element symbol       :  '
    i4z0ie, procval.iz0, symbol
    massval2 = massval2 + symbol
    massbase1 = widget_base(massbase, /row)
    massinfo = widget_label(massbase1, font=font_small, value=massval2)
    massbase2 = widget_base(massbase, /row)
    massval3 = 'Impurity element isotopic mass: '
    masslabel1 = widget_label(massbase2, font=font_small, value=massval3)
    massbase3 = widget_base(massbase, /row)
    massval4 = 'Neutral hydrogen isotopic mass: '
    masslabel2 = widget_label(massbase3, font=font_small, value=massval4)
    if procval.amsi ne 0 then begin
  	amsval = strtrim(string(procval.amsi), 2)
    endif else begin
	amsval = ' '
    endelse
    amsiid = widget_text(massbase2, font=font_small, xsize=4,		$
			 value=amsval, /editable)
    if procval.amsh ne 0 then begin
 	amshval = strtrim(string(procval.amsh), 2)
    endif else begin
	amshval = ' '
    endelse
    amshid = widget_text(massbase3, font=font_small, xsize=4,           $
                         value=amshval, /editable)


		;**** Base for X-ray filter information ****

    filtbase = widget_base(topbase, /column, /frame)
    filtval = 'Please input soft X-ray filter information:-'
    filthead = widget_label(filtbase, font=large_font, value=filtval)
    filtbase1 = widget_base(filtbase, /row)
    filtval2 = 'Use a simple cut-off energy?  '
    filtlabel1 = widget_label(filtbase1, font=font_small, value=filtval2)
    choices = [' Yes ',' No  ']
    levcutid = cw_bselector(filtbase1, choices, set_value=procval.levcut)

		;**** Base for widgets depending on yes/no val of toggle ****

    yesnobase = widget_base(filtbase)

		;**** Base for energy cut-off filter ****

    yesbase = widget_base(yesnobase, /column, map=1-procval.levcut)
    yesbase1 = widget_base(yesbase, /row)
    yesheadval = 'Energy of cut-off (eV)   : '
    yeshead = widget_label(yesbase1, font=font_small, value=yesheadval)
    if procval.ievcut ne 0 then begin
	ievval = strtrim(string(procval.ievcut, format='(i7)'),2)
    endif else begin
	ievval = ' '
    endelse
    ievcutid = widget_text(yesbase1, font=font_small, /editable,	$
			   xsize=7, value=ievval, /all_events)

		;**** Base for Be/Si filter ****

    nobase = widget_base(yesnobase, /column, map=procval.levcut)
    nobase1 = widget_base(nobase, /row)
    noheadval = 'Beryllium thickness (microns) : '
    nohead = widget_label(nobase1, font=font_small, value=noheadval)
    if procval.thbe ne 0.0 then begin
	thbeval = strtrim(string(procval.thbe, format='(i5)'),2)
    endif else begin
	thbeval = ' '
    endelse
    thbeid = widget_text(nobase1, /editable, font=font_small, xsize=7,	$
			 value=thbeval, /all_events)
    nobase2 = widget_base(nobase, /row)
    noheadval2 = 'Silicon thickness (mm)        : '
    nohead2 = widget_label(nobase2, font=font_small, value=noheadval2)
    if procval.thsi ne 0.0 then begin
	thsival = strtrim(string(procval.thsi, format='(i5)'),2)
    endif else begin
	thsival = ' '
    endelse
    thsiid = widget_text(nobase2, /editable, font=font_small, xsize=7,	$
			 value=thsival, /all_events)

		;**** Base for filter name ****

    fnamebase = widget_base(filtbase, /row)
    fnamehead = 'Current filter name : '
    fnamelabel = widget_label(fnamebase, font=font_small, value=fnamehead)
    fnameval = strcompress(procval.fname, /remove_all)
    fnameid = widget_label(fnamebase, value=fnameval)
    state = {fnameid:fnameid,ievcutid:ievcutid,thsiid:thsiid,		$
	     thbeid:thbeid, levcutid:levcutid}
    fname = filter_name(state)

		;**** Base for temperature and densiy limit selection ****
    tdtopbase = widget_base(parent,/column,/frame)

    tdval = 'Please enter electron temperature and density limits for scans'
    tdlab = widget_label(tdtopbase,value=tdval, font=large_font)

		;**** Base for mass and filter info ****
    tdbase = widget_base(tdtopbase,/row)

		;**** Base for temperature limit input ****

    tempbase = widget_base(tdbase,/column,/frame)

		;**** Temperature limit fields ****

    if procval.ittyp eq 0 then begin
    	tval1 = 'Electron Temperature (Kelvin)'
    endif else begin
    	tval1 = 'Electron Temperature (eV)    '
    endelse
    tlab1 = widget_label(tempbase,value=tval1,font=large_font)
    tempbase1 = widget_base(tempbase,/row)
    tval1a = 'Lower limit :   '
    tlab1a = widget_label(tempbase1,value=tval1a,font=font_small)
    if procval.tinmin ne 0.0 then begin
	tval1b = strtrim(string(procval.tinmin, format='(e8.2)'),2)
    endif else begin
	tval1b = ' '
    endelse
    tinminid = widget_text(tempbase1, /editable, font=font_small, 	$
			   xsize=8, value=tval1b)
    tempbase2 = widget_base(tempbase,/row)
    tval2a = 'Upper limit :   '
    tlab2a = widget_label(tempbase2,value=tval2a,font=font_small)
    if procval.tinmax ne 0.0 then begin
	tval2b = strtrim(string(procval.tinmax, format='(e8.2)'),2)
    endif else begin
	tval2b = ' '
    endelse
    tinmaxid = widget_text(tempbase2, /editable, font=font_small, 	$
			   xsize=8, value=tval2b)
    tempbase3 = widget_base(tempbase,/row)
    tval3a = 'No. of temps. : '
    tlab3a = widget_label(tempbase3,value=tval3a,font=font_small)
    if procval.numt ne 0 then begin
	tval3b = strtrim(string(procval.numt, format='(i4)'),2)
    endif else begin
	tval3b = ' '
    endelse
    numtid = widget_text(tempbase3, /editable, font=font_small, 	$
			   xsize=8, value=tval3b)

		;**** Button for temperature unit selection ****

    unitbase = widget_base(tdbase,/column,/frame)
    blab = widget_label(unitbase,value='Temperature',font=large_font)
    blab = widget_label(unitbase,value='units:',font=large_font)
    unitid = cw_bgroup(unitbase,['Kelvin','eV'],/exclusive, 		$
		       font=font_small, /column, set_value=procval.ittyp)      

		;**** Base for density limit input ****

    densbase = widget_base(tdbase,/column,/frame)

		;**** Density limit fields ****

    dval1 = 'Electron Density (cm-3)'
    dlab1 = widget_label(densbase,value=dval1,font=large_font)
    densbase1 = widget_base(densbase,/row)
    dval1a = 'Lower limit :  '
    dlab1a = widget_label(densbase1,value=dval1a,font=font_small)
    if procval.dinmin ne 0.0 then begin
	dval1b = strtrim(string(procval.dinmin, format='(e8.2)'),2)
    endif else begin
	dval1b = ' '
    endelse
    dinminid = widget_text(densbase1, /editable, font=font_small, 	$
			   xsize=8, value=dval1b)
    densbase2 = widget_base(densbase,/row)
    dval2a = 'Upper limit :  '
    dlab2a = widget_label(densbase2,value=dval2a,font=font_small)
    if procval.dinmax ne 0.0 then begin
	dval2b = strtrim(string(procval.dinmax, format='(e8.2)'),2)
    endif else begin
	dval2b = ' '
    endelse
    dinmaxid = widget_text(densbase2, /editable, font=font_small, 	$
			   xsize=8, value=dval2b)
    densbase3 = widget_base(densbase,/row)
    dval3a = 'No. of dens. : '
    dlab3a = widget_label(densbase3,value=dval3a,font=font_small)
    if procval.numd ne 0 then begin
	dval3b = strtrim(string(procval.numd, format='(i4)'),2)
    endif else begin
	dval3b = ' '
    endelse
    numdid = widget_text(densbase3, /editable, font=font_small, 	$
			   xsize=8, value=dval3b)


		;**** Message at bottom of window ****

    noteval = '(Note: equal logarithmic scaling of temperatures and '+	$
		'densities is used)'
    tdmes = widget_label(tdtopbase,value=noteval,font=font_small)

		;**** Error message ****

    messid = widget_label(parent, font=large_font, 			$
    value='Edit the processing options data and press Done to proceed')

		;**** add the exit buttons ****

    base = widget_base(parent, /row)
    menufile = bitfile + '/menu.bmp'
    read_X11_bitmap, menufile, bitmap1
    outid = widget_button(base, value=bitmap1)          ;menu button
    cancelid = widget_button(base, value='Cancel', font=large_font)
    doneid = widget_button(base, value='Done', font=large_font)
  
                ;*************************************************
		;**** create a state structure for the pop-up ****
		;**** 		     window.                  ****
                ;*************************************************

    new_state = { 	RUNID	:	runid,  			$
			MESSID	:	messid, 			$
			CANCELID:	cancelid,			$
			DONEID	:	doneid, 			$
                  	OUTID   :       outid,                          $
			AMSIID	:	amsiid,				$
			AMSHID	:	amshid,				$
			LEVCUTID:	levcutid,			$
			FNAMEID	:	fnameid,			$
			THBEID	:	thbeid,				$
			THSIID	:	thsiid,				$
			IEVCUTID:	ievcutid,			$
			DSFULL	:	dsfull,				$
			NOBASE	:	nobase,				$
			YESBASE	:	yesbase,			$
			FONT	:	large_font,			$
			NUM_FORM:	num_form,			$
			TINMINID:	tinminid,			$
			TINMAXID:	tinmaxid,			$
			DINMINID:	dinminid,			$
			DINMAXID:	dinmaxid,			$
			NUMTID	:	numtid,				$
			NUMDID	:	numdid,				$
			UNITID	: 	unitid,				$
			TLABID	:	tlab1				}

                 ;**** Save initial state structure ****

    widget_control, first_child, set_uvalue=new_state, /no_copy

    RETURN, parent

END

