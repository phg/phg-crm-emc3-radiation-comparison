; Copyright (c) 1996 Strathclyde University.
; SCCS info: Module @(#)$Header: /home/adascvs/idl/adas4xx/adas404/cw_adas404_in.pro,v 1.3 2004/07/06 12:44:47 whitefor Exp $ Date $Date: 2004/07/06 12:44:47 $
;+
; PROJECT:
;       ADAS IBM MVS to UNIX conversion
;
; NAME:
;       CW_ADAS404_IN()
;
; PURPOSE:
;       Data file selection for adas 404.
;
; EXPLANATION:
;       This function creates a compound widget consisting of two
;       cw_adas_root compound widgets to select the input and output
;	directories, several selection widgets,
;	a 'Cancel' button, a 'Done' button, and a 'Search' button to
;	search for files with the given details. The 'Done'
;	button is automatically de-sensitised until appropriate times.
;
;	The value of this widget is contained in the VALUE structure.
;
; USE:
;       See routine adas404_in.pro for an example.
;
; INPUTS:
;       PARENT  - Long integer; the ID of the parent widget.
;
;	CLASSES - String array: the names of the isonuclear master classes
;
; OPTIONAL INPUTS:
;       None
;
; OUTPUTS:
;       This function returns the ID of the compound widget created.
;
; OPTIONAL OUTPUTS:
;       None
;
; KEYWORD PARAMETERS:
;       VALUE   - A structure which determines the initial settings of
;                 the entire compound widget. The structure must be:
;		{							                $
;		  ROOTPATHIN:userroot+'/adf10/', 			$
;		  CENTROOTIN:centroot+'/adf10/', 			$
;		  USERROOTIN:userroot+'/adf10/',			$
;		  ROOTPATHOUT:userroot+'/adf11/', 			$
;		  CENTROOTOUT:centroot+'/adf11/', 			$
;		  USERROOTOUT:userroot+'/adf11/',			$
;         FILE:'',                                  $
;         FILTR:'',                                 $
;		  IRESO:0,						            $
;         Z0:1,                                     $
;         ZE1:0,						            $
;         ZE2:0,					            	$
;		  YEAR:'',					              	$
;         CLASSES:intarr(9)				            $
;		}
;
;		  Where the elements of the structure are as follows:
;
;                 ROOTPATH   - Current data directory e.g '/usr/fred/adas/'
;
;                 CENTROOT   - Default central data store e.g '/usr/adas/'
;
;                 USERROOT   - Default user data store e.g '/usr/fred/adas/'
;
;                 FILE       - Current file name
;
;		  IRESO      -  0 = Standard -> Standard conversion
;                       1 = Resolved -> Unresolved
;                       2 = Resolved -> Resolved
;
;		  YEAR       - String: the year for the data to be taken from.
;
;		  CLASS()    - Int: 1 if the corresponding class in CLASSES is
;				     required
;
;                 Path names may be supplied with or without the trailing
;                 '/'.  The underlying routines add this character where
;                 required so that USERROOT will always end in '/' on
;                 output.
;
;       FONT_LARGE  - Supplies the large font to be used for the
;                     interface widgets.
;
;       FONT_SMALL  - Supplies the small font to be used for the
;                     interface widgets.
;
;       RNUM  	    - Supplies the number (as a string) to be used in the
;		      information widget title
;
; CALLS:
;	POPUP			Pops up simple message window
;
; SIDE EFFECTS:
;       IN404_GET_VAL() Widget management routine in this file.
;       IN404_EVENT()   Widget management routine in this file.
;
; CATEGORY:
;       Compound Widget
;
; WRITTEN:
;       William Osborn, Tessella Support Services plc, 12th November 1996
;
; MODIFIED:
;       1.1     William Osborn
;               First written using cw_adas402_in.pro
;       1.2     William Osborn
;               Removed diagnostic print statements
;       1.3     Lorne Horton
;               Fixed 'met' file naming
;               Corrected out-by-one error in number of electrons
;               Modified allowable range of nuclear charge
;               Fixed bug in file selection algorithm
;               - setting indices to -1 is incompatible with
;                 cw_choose.  I've reverted to 0 which means that
;                 if you go from unresolved to resolved targets
;                 then QCD and XCD wil be unselected unless explicitly
;                 reselected.  THIS REQUIRES MORE WORK.
;
; VERSION:
;       1.1     12-11-96
;       1.2     20-11-96
;       1.3     20-10-97
;
;-----------------------------------------------------------------------------
;-
FUNCTION in404_get_val, id

                ;**** Return to caller on error ****

    ON_ERROR, 2

                 ;***************************************
                 ;****     Retrieve the state        ****
                 ;**** Get first_child widget id     ****
                 ;**** because state is stored there ****
                 ;***************************************

    first_child = widget_info(id, /child)
    widget_control, first_child, get_uvalue = state

		;**** Get the settings ****

    widget_control, state.pathinid, get_value=pathin
;    widget_control, state.pathoutid, get_value=pathout
    widget_control, state.z0id, get_value=z0val
    if num_chk(z0val(0),/integer) eq 0 then z0 = fix(z0val(0)) else z0 = 1
    widget_control, state.ze1id, get_value=ze1val
    if num_chk(ze1val(0),/integer) eq 0 then ze1 = fix(ze1val(0)) else ze1 = 0
    widget_control, state.ze2id, get_value=ze2val
    if num_chk(ze2val(0),/integer) eq 0 then ze2 = fix(ze2val(0)) else ze2 = 0

    widget_control, state.yearid, get_value = yearval
    year = strtrim(yearval(0),2)
    sy = strlen(year)
    if sy gt 2 then year = strmid(year,sy-2,2)
    widget_control, state.prefixid, get_value = prefix
    prefix = strtrim(prefix(0),2)

    widget_control, state.iresoid, get_value=ireso

    ps = {ROOTPATHIN:pathin.rootpath, 		$
		  CENTROOTIN:pathin.centroot, 		$
		  USERROOTIN:pathin.userroot,		$
;		  DIROUT:pathout,					$
          FILEIN:state.filein,              $
		  IRESO:ireso,						$
          Z0:z0,                            $
          ZE1:ze1,						    $
          ZE2:ze2,						    $
    	  YEAR:year,						$
          CLASS:state.indices,				$
		  PREFIX:prefix						$
		}

    widget_control, first_child, set_uvalue=state, /no_copy

    RETURN, ps

END

;-----------------------------------------------------------------------------

FUNCTION in404_event, event

                ;**** Base ID of compound widget ****

    parent = event.handler

                ;**** Default output no event ****

    new_event = 0L

                ;**********************************************
                ;**** Retrieve the user value state        ****
                ;**** Get id of first_child widget because ****
                ;**** user value "state" is stored there   ****
                ;**********************************************

    first_child = widget_info(parent, /child)
    widget_control, first_child, get_uvalue=state, /no_copy
    topparent = widget_info(parent, /parent)

    widget_control, state.messid, set_value=' '

                ;************************
                ;**** Process events ****
                ;************************

    CASE event.id OF

	state.z0id: widget_control, state.ze1id, /input_focus
	state.ze1id: widget_control, state.ze2id, /input_focus
	state.ze2id: widget_control, state.yearid, /input_focus
	state.yearid: widget_control, state.z0id, /input_focus

		;***********************************
		;**** Event from root selection ****
		;***********************************

    state.pathinid: begin
	    widget_control, state.doneid, sensitive=0
	end

		;***********************************
		;**** Event from Search button  ****
		;***********************************

	state.classbut: begin
            choosetitle="Select master classes for processing:-"
            widget_control, state.iresoid, get_value=val
            if val ne 2 then begin
                ;**** Remove the QCD and XCD classes for ****
                ;**** conversion into unresolved files   ****
                css = strarr(7)
                css(0:4)=state.classes(0:4)
                css(5:6)=state.classes(7:8)
                indices = strarr(7)
                indices(0:4)=state.indices(0:4)
                indices(5:6)=state.indices(7:8)
            endif else begin
                indices = strarr(9)
                css = state.classes(0:8)
                indices = state.indices
            endelse
            cw_choose, strupcase(css), indices,	            $
                       font=state.font_large,	 			$
        		       title=choosetitle
            w = where(indices eq 1)
            if w(0) eq -1 then begin
                mes = ['You must choose at least one master class:',$
                       'ACD and SCD have been assumed to be selected.']
                but = ['OK']
                act = popup(message=mes,buttons=but,font=state.font_large,$
                            title='ADAS404 Message')
                indices(0:1)=[1,1]
            endif
            if val ne 2 then begin
                state.indices(0:4) = indices(0:4)
                state.indices(5:6) = [0,0]
                state.indices(7:8) = indices(5:6)
            endif else begin
                state.indices = indices
            endelse
        end
                ;***********************
                ;**** Cancel button ****
                ;***********************

        state.cancelid: begin
            new_event = {ID:parent, TOP:event.top,          		$
                         HANDLER:0L, ACTION:'Cancel'}
        end

	state.searchid: begin

		;**************************************************
		;**** Get the current settings of the widgets. ****
		;**************************************************

	    widget_control, first_child, set_uvalue=state, /no_copy
	    widget_control, event.handler, get_value=ps
	    widget_control, first_child, get_uvalue=state, /no_copy

		;**** Test the validity of the user's values ****

            errormess = ''

                ;**** Check the year ****

            if (errormess eq '') then begin
                year = strcompress(ps.year, /remove_all)
                if (year eq '') then begin
                    errormess = '**** You must enter a valid year '+    $
                    'for the data ****'
		    widget_control, state.yearid, /input_focus
                endif else if (strlen(year) ne 2 ) then begin
                    errormess = '**** You have specified the year '+    $
                    'incorrectly ****'
		    widget_control, state.yearid, /input_focus
                endif else begin
                    yearbyt = byte(year)
                    if ((min(yearbyt) lt 48) or                         $
                    (max(yearbyt) gt 57)) then begin
                        errormess =                                     $
                        '**** You have specified the year incorrectly ****'
			widget_control, state.yearid, /input_focus
                    endif
                endelse
            endif

                ;**** Check the charges ****

            if (errormess eq '') then begin
                z0 = ps.z0
		if z0 lt 1 or z0 gt 50 then begin
                    errormess = '**** You must enter a valid '+         $
                    'nuclear charge (0<=Z0<=50) ****'
		    widget_control, state.z0id, /input_focus
                endif
                ze2 = ps.ze2
		if ze2 lt 0 or ze2 ge z0 then begin
                    errormess = '**** You must enter a valid '+         $
                    'highest ion charge (0<=ZE2<Z0) ****'
		    widget_control, state.ze1id, /input_focus
                endif
                ze1 = ps.ze1
		if ze1 lt 0 or ze1 gt ze2 then begin
                    errormess = '**** You must enter a valid '+         $
                    'lowest ion charge (0<=ZE1<=ZE2) ****'
		    widget_control, state.ze2id, /input_focus
                endif
            endif

                ;**** if error then give message ****

            if errormess ne '' then begin
                widget_control, state.messid, set_value=errormess
		new_event = 0L
            endif else begin

		;**** No error so begin the search ****

	        fileflag = 1		;Set to 1 if there are files
					;that fit, otherwise 0

                ;**** Create an information widget ****

                widget_control, /hourglass
                widgetbase = widget_base(/column, xoffset=300, yoffset=200,$
                title = "ADAS"+state.rnum+": INFORMATION")
                lab0 = widget_label(widgetbase, value='')
                lab1 = widget_label(widgetbase, value='')
                lab2 = widget_label(widgetbase, font=state.font_large,  $
                value=" Searching for files - "+$
                "please wait      ")
                lab3 = widget_label(widgetbase, value='')
                lab4 = widget_label(widgetbase, value='')
                widget_control, widgetbase, /realize
		widget_control, topparent, sensitive=0

                ;**************************
                ;**** Search for files ****
                ;**************************


                ;**** Construct target directory names ****
                dirnames = state.classes(where(ps.class eq 1))
                if ps.ireso eq 1 then dirnames = [dirnames,'met']
                dirnames = dirnames + year
;                filtr_val = ps.filtr
;                filterstring = strcompress(filtr_val(0), /remove_all)
;                if filterstring ne '' then begin
;                    dirnames = dirnames + '(' + filterstring + ')'
;                endif

                ;**** Look for iso-electronic files ****

		nclass = n_elements(dirnames)
		nsweep = ze2-ze1+1
		infiles = strarr(nsweep,nclass)
		found = replicate(0,nclass)
		text = strarr(nclass+2)
		text(0) = '             Iso-electronic element'
		text(1) = 'Class'
		for j=0,nclass-1 do text(j+2)=strmid(dirnames(j),0,3)+'  '
		for i=0,nsweep-1 do begin
		    nelec = z0-(ze1+i)
		    i4z0ie, nelec, symbol
		    symbol = strlowcase(symbol)
            to_find = dirnames + '_' + symbol
		    if strlen(symbol) eq 1 then symbol = ' ' + symbol
		    text(1)=text(1)+' '+symbol+' '
		    if ps.ireso eq 0 then begin
    			to_find = to_find + '.dat'
		    endif else begin
	    		to_find = to_find + '??.dat'
		    endelse
		    if ps.prefix ne '' then begin
		    	to_find = ps.prefix+'#'+to_find
		    endif
		    for j=0,nclass-1 do begin
	            com = ['find',ps.rootpathin,'-name',to_find(j),'-print']
                        spawn, com, files, /noshell
                s=size(files)
                if s(0) gt 0 then begin
		    	    found(j) = found(j)+1
			        text(j+2)=text(j+2)+'YES '
			    endif else begin
			        text(j+2)=text(j+2)+' NO '
			    endelse
                infiles(i,j) = ps.rootpathin+dirnames(j)+'/'+to_find(j)
		    endfor
		endfor

		;**** Pop-up widget showing which files have been found ****

		w = where(found eq 0)
		if w(0) ne -1 then begin
                    mess = '**** Error: For some data classes there are no data files ****'
		endif else begin
		    w = where(found ne nsweep)
		    if w(0) ne -1 then begin
                    	mess = '**** Warning: For some data classes not all files exist ****'
		    endif else begin
			mess = '**** All requested input files exist ****'
		    endelse
		    widget_control, state.doneid, sensitive = 1
		endelse

		text = [text,mess]

		action = popup(message=text,buttons=['OK'],$
			font=state.font_large,title='Files found')

        w = where(ps.class eq 1)
        if ps.ireso eq 1 then nclass = nclass-1 ;MET files dealt with later
		for i=0,nsweep-1 do begin
		    for j=0,nclass-1 do begin
                        cl = w(j)
                        if ps.ireso eq 0 then begin
                            state.filein(i,cl) = infiles(i,j)
                        endif else begin
                            ;**** If resolved, remove met. indices and put ##
                            ;**** in place
                            p = strpos(infiles(i,j),'.dat')
                            if p gt 0 then begin
                                state.filein(i,cl)=strmid(infiles(i,j),0,p-2)+$
                                  '##.dat'
                            endif
                        endelse
                    endfor
		endfor

            ;**** Deal with MET files if ****
            ;**** Resolved->Unresolved   ****
            if ps.ireso eq 1 then begin
                for i=0,nsweep-1 do begin
                    p=strpos(infiles(i,nclass),'.dat')
                    if p gt 0 then begin
                        state.filein(i,9)=strmid(infiles(i,nclass),0,p-2)+$
                                  '##.dat'
                    endif
                endfor
                ;**** One extra member is needed for the MET files ****
                ;**** only if highest state is not hydrogen-like   ****
                ;**** If this is missing the fortran will stop!!   ****
                if ze2 lt z0-1 then begin
                    nelec = z0-(ze1+nsweep)
                    i4z0ie, nelec, symbol
                    symbol = strlowcase(symbol)
                    dir = 'met'+year
                    file = dir+'_'+symbol+'##.dat'
                    if ps.prefix ne '' then begin
                        file = ps.prefix+'#'+file
                    endif
                    file = dir+'/'+file
                    state.filein(nsweep,9) = ps.rootpathin+file
                endif else begin
                    state.filein(nsweep,9) = ''
                endelse
            endif

		widget_control, widgetbase,/destroy
		widget_control, topparent, sensitive=1

	    endelse
	end

                ;*********************
                ;**** Done button ****
                ;*********************

        state.doneid: begin
            new_event = {ID:parent, TOP:event.top,          $
                         HANDLER:0L, ACTION:'Done'}
        end

        ELSE: new_event = 0L

    ENDCASE


                ;*********************************************
                ;*** make "state" available to other files ***
                ;*********************************************

    widget_control, first_child, set_uvalue=state, /no_copy

    RETURN, new_event

END

;-----------------------------------------------------------------------------

FUNCTION cw_adas404_in, parent, classes, VALUE=value,			$
			FONT_LARGE=font_large, FONT_SMALL=font_small,	$
			RNUM = rnum


    IF (N_PARAMS() LT 2) THEN MESSAGE,					$
    'Must specify parent and classes for cw_adas404_in'

    ON_ERROR, 2                                 ;return to caller on error

                ;**** Set defaults for keywords ****

    IF NOT (KEYWORD_SET(value)) THEN begin
	classarr = intarr(9)
	classar(0)=1
	classar(1)=1
	fileinarr = strarr(50,10)
	inset = {							$
		  ROOTPATHIN:'./', 				$
		  CENTROOTIN:'./', 				$
		  USERROOTIN:'./',				$
;		  DIROUT:'./',					$
          FILEIN:fileinarr,             $
		  IRESO:0,						$
          Z0:1,                         $
          ZE1:0,						$
          ZE2:0,						$
		  YEAR:'',						$
          CLASS:classarr,		        $
		  PREFIX:''						$
		}
    ENDIF ELSE BEGIN
	inset = {							$
		  ROOTPATHIN:value.rootpathin,	$
		  CENTROOTIN:value.centrootin,	$
		  USERROOTIN:value.userrootin,	$
;		  DIROUT:value.dirout,			$
          FILEIN:value.filein,          $
          IRESO:value.ireso,			$
          Z0:value.z0,                  $
          ZE1:value.ze1,	            $
          ZE2:value.ze2,	            $
		  YEAR:value.year,				$
          CLASS:value.class,        	$
		  PREFIX:value.prefix			$
		}

        if strtrim(inset.rootpathin) eq '' then begin
            inset.rootpathin = './'
        endif else if                                                   $
       strmid(inset.rootpathin, strlen(inset.rootpathin)-1,1) ne '/' then begin
            inset.rootpathin = inset.rootpathin+'/'
        endif
    ENDELSE
    IF NOT (KEYWORD_SET(font_large)) THEN font_large = ''
    IF NOT (KEYWORD_SET(font_small)) THEN font_small = ''
    IF NOT (KEYWORD_SET(rnum)) THEN rnum = '404'

		;**** Modify font sizes depending on platform ****

    machine = GETENV('TARGET_MACHINE')
    if machine eq 'HPUX' then begin
	large_font = font_small
    endif else begin
	large_font = font_large
    endelse

                ;*********************************
                ;**** Create the Input widget ****
                ;*********************************

                ;**** create base widget ****

    topbase = widget_base(parent, EVENT_FUNC = "in404_event",		$
                          FUNC_GET_VALUE = "in404_get_val",		$
		          /column)

                ;******************************************************
                ;**** Create a dummy widget just to hold value of *****
                ;**** "state" variable so as not to get confused  *****
                ;**** with any other values. Adopt IDL practice   *****
                ;**** of using first child widget                 *****
                ;******************************************************

    first_child = widget_base(topbase)

    cwid = widget_base(first_child, /column)

		;*************************************
		;**** Input path selection widget ****
		;*************************************
    base = widget_base(cwid,/frame,/column)
    lab = widget_label(base,value='Path for isoelectronic input files:',$
		font=large_font)
    pathin = {   ROOTPATH:inset.rootpathin,	$
                 CENTROOT:inset.centrootin, USERROOT:inset.userrootin }
    pathinid = cw_adas_root(base,value=pathin,font=large_font)

		;******************************
		;**** File selection input ****
		;******************************

    base = widget_base(cwid, /frame, /column)
    b1 = widget_base(base, /row)
    lab1 = widget_label(b1,value='Nuclear charge Z0 (<=50):',font=large_font)
    z0id = widget_text(b1,value=string(inset.z0,format='(I3)'),$
	xsize = 4, font=large_font, /editable)
    b2 = widget_base(base, /row)
    lab2 = widget_label(b2,value='Lowest ion charge ZE1   :',font=large_font)
    ze1id = widget_text(b2,value=string(inset.ze1,format='(I3)'),$
	xsize=4, font=large_font, /editable)
    b3 = widget_base(base, /row)
    lab3 = widget_label(b3,value='Highest ion charge ZE2  :',font=large_font)
    ze2id = widget_text(b3,value=string(inset.ze2,format='(I3)'),$
	xsize=4, font=large_font, /editable)
    lab3a = widget_label(b3,value='(excluding bare nucleus)',font=large_font)
    b4 = widget_base(base, /row)
    lab4 = widget_label(b4,value='Year of data  :',font=large_font)
    yearid = widget_text(b4,value=inset.year,xsize=4,font=large_font,$
			 /editable)
    b6 = widget_base(base, /row)
    lab6 = widget_label(b6,value='File prefix (blank for none):',font=large_font)
    prefixid = widget_text(b6,value=inset.prefix,xsize=4,font=large_font,$
			 /editable)

    classbase = widget_base(base, /row)
    classval = "Select master collisional-dielectronic classes   :"
    classlabel = widget_label(classbase, font=large_font, value=classval)
    classbut = widget_button(classbase, value="  Select  ", font=large_font)

    ftype = ['Standard -> Standard','Partial Resolved -> Partial Unresolved',$
		'Partial Resolved -> Partial Resolved']
    b5 = widget_base(base, /row)
    lab5 = widget_label(b5,value='File types for extraction :',$
		font=large_font)
    iresoid = cw_bselector(b5,ftype,set_value=inset.ireso,font=large_font)

;		;**************************************
;		;**** Output path selection widget ****
;		;**************************************
;
;    base = widget_base(cwid,/frame,/column)
;    lab = widget_label(base,value='Directory for isonuclear output files:',$
;		font=large_font)
;    pathoutid = widget_text(base,value=inset.dirout,font=large_font)

    messid = widget_label(cwid,value='Please enter options and press Search to continue',font=large_font)

                ;*****************
                ;**** Buttons ****
                ;*****************

    base = widget_base(cwid, /row)

                ;**** Search button ****

    searchid = widget_button(base, value='Search',$
                             font=large_font)

                ;**** Cancel Button ****

    cancelid = widget_button(base, value='Cancel', font=large_font)

                ;**** Done Button ****

    doneid = widget_button(base, value='Done', font=large_font)

    widget_control, doneid, sensitive = 0

                ;*************************************************
                ;**** create a state structure for the pop-up ****
                ;****                window.                  ****
                ;*************************************************

    indices = inset.class
    filein = inset.filein

    new_state = {	font_large	:	large_font,		$
        			font_small	:	font_small,		$
		        	inval		:	inset,			$
	        		doneid		:	doneid,			$
			        cancelid	:	cancelid,		$
                    rnum		: 	rnum,			$
                    searchid    :   searchid,       $
                    messid      :   messid,  		$
        			z0id		:	z0id,			$
		        	ze1id		:	ze1id,			$
        			ze2id		:	ze2id,			$
		        	yearid		:	yearid,			$
        			iresoid		:	iresoid,		$
		        	pathinid	:	pathinid,		$
			        prefixid	:	prefixid,		$
        			classbut	:	classbut,		$
		        	indices		:	indices,		$
			        classes		:	classes,		$
			        filein		:	filein 			$
		        }

                 ;**** Save initial state structure ****

    widget_control, first_child, set_uvalue=new_state, /no_copy

    RETURN, topbase

END
