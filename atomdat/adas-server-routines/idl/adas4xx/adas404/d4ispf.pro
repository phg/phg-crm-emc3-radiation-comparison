; Copyright (c) 1996, Strathclyde University 
; SCCS INFO : Module @(#)$Header: /home/adascvs/idl/adas4xx/adas404/d4ispf.pro,v 1.1 2004/07/06 13:14:06 whitefor Exp $ Date $Date: 2004/07/06 13:14:06 $
;
;+
; PROJECT:
;       ADAS IBM MVS to UNIX conversion
;
; NAME:
;	D4ISPF
;
; PURPOSE:
;	IDL user interface and communications with ADAS404 FORTRAN
;	process via pipe.
;
; EXPLANATION:
;	The routine begins by reading information from the ADAS404
;	FORTRAN process via a UNIX pipe.  Then part of the ADAS404
;	IDL user interface is invoked to determine how the user
;	wishes to process the input dataset.  When the user's
;	interactions are complete the information gathered with
;	the user interface is written back to the FORTRAN process
;	via the pipe.  Communications are to the FORTRAN subroutine
;	D1ISPF.
;
; USE:
;	The use of this routine is specific to ADAS404, see adas404.pro.
;
; INPUTS:
;	PIPE	- The IDL unit number of the bi-directional pipe to the
;		  ADAS404 FORTRAN process.
;
;	LPEND	- Integer, 0 if the user pressed the 'Done' button
;		  or 1 if the user pressed 'Cancel'.  Maps directly onto
;		  the logical variable LPEND in ADAS404 FORTRAN.
;
;	PROCVAL	- A structure which determines the initial settings of
;		  the processing options widget.  The initial value is
;		  set in adas404.pro.  If adas404.pro passes a blank 
;		  dummy structure of the form {NEW:-1} into PROCVAL then
;		  PROCVAL is reset to a default structure.
;
;		  The PROCVAL structure is;
;			procval = {                        	$
;                			new   : 0 ,             $
;                			title : '',             $
;					ifout : 1,		$
;                                       tin   : temp_arr,       $
;                                       dout  : dens_arr,       $
;                                       maxt  : 0,              $
;                                       maxd  : 0               $
;              			}
;
;
;		  See cw_adas404_proc.pro for a full description of this
;		  structure.
;	
;
;       BITFILE - String; the path to the dirctory containing bitmaps
;                 for the 'escape to series menu' button.
;
; OPTIONAL INPUTS:
;	None
;
; OUTPUTS:
;	PROCVAL	- On output the structure records the final settings of
;		  the processing selections widget if the user pressed the
;		  'Done' button, otherwise it is not changed from input.
;
;       GOMENU  - Int; flag - set to 1 if user has selected 'escape direct
;                 to series menu , otherwise 0
;
; OPTIONAL OUTPUTS:
;	None
;
; KEYWORD PARAMETERS:
;	FONT_LARGE - The name of a larger font.
;
;	FONT_SMALL - The name of a smaller font.
;
;	EDIT_FONTS - A structure of two fonts used in the table editor
;		     adas_edtab.pro, {font_norm:'',font_input:''}.
;
; CALLS:
;	ADAS404_PROC	Invoke the IDL interface for ADAS404 data
;		 	processing options/input.
;
; SIDE EFFECTS:
;	Two way communications with ADAS404 FORTRAN via the
;	bi-directional UNIX pipe.
;
; CATEGORY:
;	Adas system.
;
; WRITTEN:
;       William Osborn, Tessella Support Services plc, 12-Nov-1996
;
; MODIFIED:
;	1.1     William Osborn
;		First release
;
; VERSION:
;	1.1	12-11-96
;
;-
;-----------------------------------------------------------------------------


PRO d4ispf, 	lpend, procval, infoval, ndtin, ndden,		$
		gomenu, bitfile,				        $
		FONT_LARGE=font_large, FONT_SMALL=font_small, 		$
		EDIT_FONTS=edit_fonts


                ;**** Set defaults for keywords ****

    IF NOT (KEYWORD_SET(font_large)) THEN font_large = ''
    IF NOT (KEYWORD_SET(font_small)) THEN font_small = ''
    IF NOT (KEYWORD_SET(edit_fonts)) THEN 				$
    edit_fonts = {font_norm:'',font_input:''}

		;*******************************************
		;**** Set default value if non provided ****
		;*******************************************

    if (procval.new lt 0) then begin
        temp_arr = fltarr(ndtin)
        dens_arr = fltarr(ndden)
        procval = {			$
		   new   : 0 ,          $
 		   title : '',		$
		   ifout : 1,		$
                   tin   : temp_arr,    $
                   din   : dens_arr,    $
                   maxt  : 0,           $
                   maxd  : 0            $
	          }
    endif
		;****************************************
		;**** Pop-up processing input widget ****
		;****************************************

    adas404_proc, procval, infoval, ndtin, ndden, action, bitfile,	$
		  FONT_LARGE=font_large, FONT_SMALL=font_small,		$
		  EDIT_FONTS=edit_fonts

		;********************************************
		;****  Act on the event from the widget  ****
		;**** There are three possible events    ****
		;**** 'Done', 'Cancel' and 'Menu'.       ****
		;********************************************

    gomenu = 0
    if action eq 'Done' then begin
        lpend = 0
    endif else if action eq 'Menu' then begin
        lpend = 0
        gomenu = 1
    endif else begin
        lpend = 1
    endelse

		;**** Output written to pipe in d4spf1 ****

END
