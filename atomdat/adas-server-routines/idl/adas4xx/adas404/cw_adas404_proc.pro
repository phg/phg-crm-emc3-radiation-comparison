; Copyright (c) 1996, Strathclyde University.
; SCCS info: Module @(#)$Header: /home/adascvs/idl/adas4xx/adas404/cw_adas404_proc.pro,v 1.2 2004/07/06 12:45:03 whitefor Exp $ Date $Date: 2004/07/06 12:45:03 $
;+
; PROJECT:
;       ADAS IBM MVS to UNIX conversion
;
; NAME:
;	CW_ADAS404_PROC()
;
; PURPOSE:
;	Produces a widget for ADAS404 processing options/input.
;
; EXPLANATION:
;	This function creates a compound widget consisting of :-
;	   a text widget holding an editable 'Run title',
;	   the input dataset information ,
;	   a table widget for temp/density data ,
;	   a button to enter default values into the table,
;	   a message widget, and a 'Cancel' and a 'Done' button.
;
;	The compound widgets included in this widget are self managing.
;	This widget only manages events from the  'Default' button,
;	the 'Cancel' button and the 'Done' button and the ion parameter
;	typeins.
;
;	This widget only generates events for the 'Done' and 'Cancel'
;	buttons.
;
; USE:
;	This widget is specific to ADAS404, see adas404_proc.pro
;	for use.
;
; INPUTS:
;	TOPPARENT- Long integer, ID of parent widget.
;
;       BITFILE - String; the path to the dirctory containing bitmaps
;                 for the 'escape to series menu' button.
;
;	Some inputs map exactly onto variables of the same
;	name in the ADAS404 FORTRAN program.
;
; OPTIONAL INPUTS:
;	None
;
; OUTPUTS:
;	The return value of this function is the ID of the declared widget.
;
;	ACT 	- String; result of this widget, 'done' or 'cancel'
;
; OPTIONAL OUTPUTS:
;	None
;
; KEYWORD PARAMETERS:
;	PROCVAL	- A structure which determines the initial settings of
;		  the processing options widget.
;
;		  The default PROCVAL is created thus;
;
;     		ps = {proc404_set, $
;			new   : 0,		$
;                	title : '',             $
;			ifout : 1,		$
;                       tin   : temp_arr,       $
;                       din   : dens_arr,       $
;                       maxt  : 0,              $
;                       maxd  : 0,              $
;             }
;
;
;		NEW     Flag which defines whether or not default values
;			exist or not. (< 0 if no values exist)
; 		TITLE	Entered general title for program run
;		IFOUT	Flag for the temperature units in use:
;			1=Kelvin, 2=eV, 3=Reduced
;		TIN  	Input temperatures entered by user
;		DIN	Input densities entered by user
;		MAXT	Number of temperatures in table entered by user
;		MAXD	Number of densities in table
;
;		Most of these structure elements map onto variables of
;		the same name in the ADAS404 FORTRAN program.
;
;
;	UVALUE	- A user value for the widget. Default 0.
;
;	FONT_LARGE - The name of a larger font.  Default current system
;		     font
;
;	FONT_SMALL - The name of a smaller font. Default current system
;		     font.
;
;	EDIT_FONTS - A structure of two fonts used in the table editor
;		     adas_edtab.pro, {font_norm:'',font_input:''}.  Defaults
;		     to current system font.
;
;	NUM_FORM   - String; Numeric format to use in tables.  Default
;			'(E10.3)'
;
;
; CALLS:
;	POPUP		Popup warning window with buttons.
;	CW_ADAS_TABLE   Adas data table widget.
;	CW_OPT_VALUE    Adas option selection widget with required input
;			value.
;	I4EIZ0		Converts element symbol into atomic number
;
;	See side effects for other related widget management routines.
;
; SIDE EFFECTS:
;
;	The following widget management routines are included in this file;
;	PROC404_GET_VAL()	Returns the current PROCVAL structure.
;	PROC404_EVENT()		Process and issue events.
;
; CATEGORY:
;	Compound Widget
;
; WRITTEN:
;       William Osborn, Tessella Support Services plc, 29-Oct-1996
;
; MODIFIED:
;	1.1	William Osborn
;		First version - written using cw_adas401_proc.pro
;	1.2	Lorne Horton
;		Various Modifications
;
; VERSION:
;	1.1	29-10-96
;	1.2	27-02-97
;
;-
;-----------------------------------------------------------------------------

FUNCTION proc404_get_val, id

    COMMON cw_proc404_blk, z0min, z0max, zmin, zmax, z1min, z1max


                ;**** Return to caller on error ****

    ON_ERROR, 2

		 ;***************************************
         ;****     Retrieve the state 	     ****
		 ;**** Get first_child widget id     ****
		 ;**** because state is stored there ****
		 ;***************************************

    first_child = widget_info(id, /child)
    widget_control, first_child, get_uvalue = state

		;***********************************
		;**** Get run title from widget ****
		;**** Then centre in in string  ****
		;**** of 40 characters          ****
		;***********************************

    widget_control, state.runid, get_value=title
    title = strcompress(title(0))
    title_len = strlen(title)
    if (title_len gt 40 ) then begin
	title = strmid(title,0,37)
        widget_control, state.messid, set_value="Title too long - truncated "
        widget_control, state.runid, set_value=title
	wait ,1
    endif
    pad = (40 - title_len)/2
    spaces = '                                         '
    title(0) = strmid(spaces,0,(pad-1)) + title + strmid(spaces,0,(pad-1))


		;****************************************************
		;**** Get new temp/densitydata from table widget ****
		;**** whether or not losel selected              ****
		;****************************************************

     widget_control, state.tempid, get_value=tempval
     tabledata = tempval.value
     ifout = tempval.units+1

 	        ;**** Copy out temperature values ****

     tin = dblarr(state.ntdmax)
     din = dblarr(state.ntdmax)
     blanks = where(strtrim(tabledata(0,*),2) eq '')
     blanksd = where(strtrim(tabledata(1,*),2) eq '')

        ;***********************************************
		;**** next line assumes that all blanks are ****
		;**** at the end of the columns             ****
		;***********************************************

    if blanks(0) ge 0 then maxt=blanks(0) else maxt=state.ntdmax
    if blanksd(0) ge 0 then maxd=blanksd(0) else maxd=state.ntdmax

                ;*************************************************
                ;**** Only perform following if there is 1 or ****
                ;**** more value present in the table         ****
                ;*************************************************

    if (maxt ge 1) then begin
    	tin(0:maxt-1) = double(tabledata(0,0:maxt-1))
    endif
    if (maxd ge 1) then begin
    	din(0:maxd-1) = double(tabledata(1,0:maxd-1))
    endif

		;**** Fill in the rest with zeroes ****

    if maxt lt state.ntdmax then begin
        tin(maxt:state.ntdmax-1) = double(0.0)
    endif
    if maxd lt state.ntdmax then begin
        din(maxd:state.ntdmax-1) = double(0.0)
    endif

    state.maxt = maxt
    state.maxd = maxd

		;***********************************************
		;**** write selected values to PS structure ****
		;***********************************************

     ps = { 			 		$
	        new   : 0, 			$
                title : title,          	$
		ifout : ifout,			$
		maxt  : maxt,			$
		maxd  : maxd,			$
		tin   : tin,			$
		din   : din			$
              }


    widget_control, first_child, set_uvalue=state, /no_copy

    RETURN, ps

END

;-----------------------------------------------------------------------------

FUNCTION proc404_event, event

    COMMON cw_proc404_blk, z0min, z0max, zmin, zmax, z1min, z1max

                ;**** Base ID of compound widget ****

    parent=event.handler

		;**********************************************
        ;**** Retrieve the user value state        ****
		;**** Get id of first_child widget because ****
		;**** user value "state" is stored there   ****
		;**********************************************

    first_child = widget_info(parent, /child)
    widget_control, first_child, get_uvalue=state, /no_copy

		;*********************************
		;**** Clear previous messages ****
		;*********************************

    widget_control, state.messid, set_value=' '

                ;************************
                ;**** Process events ****
                ;************************

    CASE event.id OF

		;*************************************
		;**** Default Energies button     ****
		;*************************************

        state.deftid: begin

 		;**** popup window to confirm overwriting current values ****

            action= popup(message='Confirm Overwrite values with Defaults', $
 		          buttons=['Confirm','Cancel'], font=state.font,$
			  title='ADAS404 Warning:-')

	    if action eq 'Confirm' then begin

		;**** Get current table widget value ****

 	        widget_control, state.tempid, get_value=tempval

;		;**** Force user to select default density value ****
;
;		dvalues = strarr(state.ide)
;		for i = 0, (state.ide - 1) do begin
;		    dvalues(i) =  strcompress(string(tempval.value(5,i)))
;		endfor
;		action= popup(message='Select Default density value', 	$
;		              buttons=dvalues, /column, font=state.font,$
;			      title = ' ')
;		defdensity = action

		;***********************************************
		;**** Copy defaults into value structure    ****
		;**** For default values use the existing   ****
		;**** table values and let user modify them ****
		;***********************************************

		deftemp = ['1e4','2e4','4e4','7e4','1e5','2e5','4e5','7e5','1e6','2e6','4e6','7e6','1e7','2e7','4e7','1e8','2e8','4e8','7e8','1e9']
		defdens = ['1e4','3e4','1e5','3e5','1e6','3e6','1e7','3e7','1e8','3e8','1e9','3e9','1e10','3e10','1e11','3e11','1e12','3e12','1e13','3e13']
		tempval.units = 0
   	        nv = 20
 	        if nv gt 0 then begin
   	            tempval.value(0,0:nv-1) = 				$
 	            strtrim(string(deftemp, 	$
 	            format=state.num_form),2)
                    tempval.value(1,0:nv-1) =  				$
                    string(defdens , format=state.num_form)
 		endif

		;**** Fill in the rest with blanks ****

 	        if nv lt state.ntdmax then begin
   	            tempval.value(0,nv:state.ntdmax-1) = ''
                    tempval.value(1,nv:state.ntdmax-1) = ''
 	        endif

		;**** Copy new data to table widget ****

 	        widget_control, state.tempid, set_value=tempval

 	    endif

     	end

		;***********************
		;**** Cancel button ****
		;***********************

        state.cancelid: new_event = {ID:parent, TOP:event.top, 		$
				     HANDLER:0L, ACTION:'Cancel'}

		;*********************
		;**** Done button ****
		;*********************

        state.doneid: begin

		;***************************************
		;**** Check all user input is legal ****
		;***************************************

	    error = 0

       	;********************************************
		;*** Have to restore "state" before calls ***
		;*** to proc404_get_val so it can be used ***
		;*** there.	              	              ***
		;********************************************

	    widget_control, first_child, set_uvalue=state, /no_copy

	    widget_control, event.handler, get_value=ps

		;*** reset state variable ***

	    widget_control, first_child, get_uvalue=state, /no_copy

		;**** check temp values entered ****

            if error eq 0 then begin
                widget_control, state.tempid, get_value=tabval
                tabvals =                                               $
                where(strcompress(tabval.value(0,*), /remove_all) ne '')
                if tabvals(0) eq -1 then begin
                    error = 1
                    message='**** Error: No temperatures/densities'+	$
                            ' entered ****'
                endif
            endif

		;**** return value or flag error ****

	    if error eq 0 then begin
                new_event = {ID:parent, TOP:event.top,                  $
                             HANDLER:0L, ACTION:'Done'}
            endif else begin
	        widget_control,state.messid,set_value=message
	        new_event = 0L
            endelse

        end

		;*********************
        ;**** Menu button ****
		;*********************

        state.outid: begin
            new_event = {ID:parent, TOP:event.top, HANDLER:0L,          $
                         ACTION:'Menu'}
        end

        ELSE: new_event = 0L

    ENDCASE

		;*********************************************
		;*** make "state" available to other files ***
		;*********************************************

    widget_control, first_child, set_uvalue=state, /no_copy

    RETURN, new_event

END

;-----------------------------------------------------------------------------

function cw_adas404_proc, topparent, infoval, ndtin, ndden, bitfile,	$
		          procval=procval, uvalue=uvalue, 		$
		          font_large=font_large, font_small=font_small,	$
		          edit_fonts=edit_fonts, num_form=num_form

		;**** Set defaults for keywords ****

    if not (keyword_set(uvalue)) then uvalue = 0
    if not (keyword_set(font_large)) then font_large = ''
    if not (keyword_set(font_small)) then font_small = ''
    if not (keyword_set(edit_fonts)) then 				$
    edit_fonts = {font_norm:'',font_input:''}
    if not (keyword_set(num_form)) then num_form = '(E10.3)'
    if not (keyword_set(procval)) then begin
        temp_arr = fltarr(ndtin)
        dens_arr = fltarr(ndden)
        ps = { 				$
		new   : 0, 		$
		title : '',             $
		ifout : 1,		$
                tin   : temp_arr,       $
                din   : dens_arr,       $
                maxt  : 0,              $
                maxd  : 0               $
              }
    endif else begin
	ps = { 				$
		new   : procval.new,    $
                title : procval.title,  $
		ifout : procval.ifout,	$
                tin   : procval.tin,    $
                din   : procval.din,    $
                maxt  : procval.maxt,   $
                maxd  : procval.maxd    $
	     }
    endelse

                ;*********************************************************
                ;**** Modify certain parameters and results depending ****
                ;**** on the machine being used                       ****
                ;*********************************************************

    machine = GETENV('TARGET_MACHINE')
    if machine eq 'HPUX' then begin
        y_size = 3
        large_font = font_small
    endif else begin
        y_size = 6
        large_font = font_large
    endelse

		;****************************************************
		;**** Assemble temp and density  table data      ****
		;****************************************************
		;**** The adas table widget requires data to be *****
		;**** input as strings, so all the numeric data *****
		;**** has to be written into a string array.    *****
		;**** Declare energies  table array             *****
		;**** col 1 has user temp   values              *****
		;**** col 2 has user density values             *****
		;****************************************************

    ntdmax = ndden > ndtin
    tabledata = strarr(2,ntdmax)

        ;*************************************
		;**** Copy out temp and density   ****
		;**** values                      ****
		;*************************************

    if (ps.maxt gt 0) then begin
        tabledata(0,0:ps.maxt-1) =					$
 	strtrim(string(ps.tin(0:ps.maxt-1),format=num_form),2)
    endif
    if (ps.maxd gt 0) then begin
        tabledata(1,0:ps.maxd-1) =					$
        strtrim(string(ps.din(0:ps.maxd-1),format=num_form),2)
    endif

		;**** fill rest of table with blanks ****

    blanks = where(tabledata eq 0.0)
    tabledata(blanks) = ' '

		;********************************************************
		;**** Create the 404 Processing options/input window ****
		;********************************************************

		;**** create titled base widget ****

    parent = widget_base(topparent, UVALUE = uvalue, 			$
			 title = 'ADAS404 PROCESSING OPTIONS', 		$
 			 EVENT_FUNC = "proc404_event", 			$
			 FUNC_GET_VALUE = "proc404_get_val", 		$
			 /COLUMN)

		;******************************************************
		;**** Create a dummy widget just to hold value of *****
		;**** "state" variable so as not to get confused  *****
		;**** with any other values. Adopt IDL practice   *****
		;**** of using first child widget                 *****
		;******************************************************

    first_child = widget_base(parent)

    topbase = widget_base(first_child,/column)

		;***********************
		;**** add run title ****
		;***********************

    base = widget_base(topbase,/row)
    rc = widget_label(base,value='Title for Run',font=large_font)
    runid = widget_text(base,value=ps.title,xsize=38,font=large_font,/edit)

		;********************************
		;**** Base for information   ****
		;********************************

    infomain = widget_base(topbase, /row)
    infobase = widget_base(infomain, /column, /frame)
    infolab  = widget_label(infobase, $
             value='Input iso-electronic file information:-',font=large_font)
    for i=0,n_elements(infoval)-1 do begin
        datalab = widget_label(infobase, value=infoval(i), font=font_small)
    endfor

		;************************************************
		;**** base for the table and defaults button ****
		;************************************************

    tablebase = widget_base(topbase, /row)
    base = widget_base(tablebase, /column, /frame)

		;********************************
		;**** Energy data table      ****
		;********************************

    colhead = ['Output','Output']

		;********************************************
		;**** convert FORTRAN index to IDL index ****
		;********************************************

    units = ps.ifout-1
    unitname = ['Kelvin','eV']

		;************************************************************
		;**** Two  columns in the table and one set of units. 	 ****
		;**** Column 1 has the same values for all three         ****
		;**** units as does column 2                             ****
		;************************************************************

    unitind = [[0,0],[1,1]]
    table_title =  [ ["Temperature & Density Values     "],		$
                   ["Temperature Density"]]

		;****************************
		;**** table of data   *******
		;****************************

    tempid = cw_adas_table(base, tabledata, units, unitname, unitind, 	$
 		           UNITSTITLE = 'Temperature Units', 		$
 		           COLEDIT = [1,1], COLHEAD = colhead, 	$
 		           TITLE = table_title, ORDER = [1,1],	$
			       UNITS_PLUS = '       Density Units : cm-3',	$
 		           LIMITS = [1,1], CELLSIZE = 12, 		$
 		           /SCROLL, ytexsize=y_size, NUM_FORM=num_form,	$
 		           FONTS = edit_fonts, FONT_LARGE = large_font,	$
 		           FONT_SMALL = font_small, /DIFFLEN)

		;*************************
		;**** Default buttons ****
		;*************************

    tbase = widget_base(base, /column)
    deftid = widget_button(tbase, font=large_font,                 	$
                           value='Default Temperature/Density Values')

		;**** Error message ****

    messid = widget_label(parent,font=large_font, 			$
    value='Edit the processing options data and press Done to proceed')

		;**** add the exit buttons ****

    base = widget_base(parent, /row)
    menufile = bitfile + '/menu.bmp'
    read_X11_bitmap, menufile, bitmap1
    outid = widget_button(base, value=bitmap1)          ;menu button
    cancelid = widget_button(base, value='Cancel', font=large_font)
    doneid = widget_button(base, value='Done', font=large_font)


		;*************************************************
		;**** create a state structure for the pop-up ****
		;**** 		     window.                      ****
		;*************************************************

    new_state = { runid:runid,  		$
		  outid:outid,			$
		  messid:messid, 		$
		  deftid:deftid,		$
		  tempid:tempid, 		$
		  cancelid:cancelid,		$
		  doneid:doneid, 		$
		  font:font_large,		$
                  maxt:ps.maxt,                 $
                  maxd:ps.maxd,                 $
                  ntdmax:ntdmax,                $
                  tablebase:tablebase,          $
		  num_form:num_form 		$
	      }

                 ;**** Save initial state structure ****

    widget_control, first_child, set_uvalue=new_state,/no_copy

    RETURN, parent

END
