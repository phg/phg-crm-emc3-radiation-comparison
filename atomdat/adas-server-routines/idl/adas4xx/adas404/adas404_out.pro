; Copyright (c) 1996, Strathclyde University.
; SCCS info: Module @(#)$Header: /home/adascvs/idl/adas4xx/adas404/adas404_out.pro,v 1.1 2004/07/06 10:40:01 whitefor Exp $ Date $Date: 2004/07/06 10:40:01 $
;+
; PROJECT:
;       ADAS IBM MVS to UNIX conversion
;
; NAME:
;	ADAS404_OUT
;
; PURPOSE:
;	IDL ADAS user interface, output options.
;
; EXPLANATION:
;	This routine creates and manages a pop-up window which allows
;	the user to select options and input data to control ADAS404
;	graphical and file output.
;
; USE:
;	This routine is the same as used for adas201.                  
;
; INPUTS:
;	VAL	- A structure which determines the initial settings of
;		  the output options widget.  The value is passed
;		  unmodified into cw_adas404_out.pro.
;
;		  See cw_adas404_out.pro for a full description of this
;		  structure.
;
;	INFOVAL	- String array; information about the input datasets
;
;       BITFILE - String; the path to the dirctory containing bitmaps
;                 for the 'escape to series menu' button.
;
; OPTIONAL INPUTS:
;	None.
;
; OUTPUTS:
;	ACT	- String; Either 'Done', 'Cancel' or 'Menu' for the button the
;		  user pressed to terminate the output options window.
;
; OPTIONAL OUTPUTS:
;	None.
;
; KEYWORD PARAMETERS:
;	DEVLIST	- A string array;  Each element of the array is the
;		  name of a hardcopy graphics output device.  This
;		  list must mirror the DEVCODE list.  DEVLIST is the
;		  list which the user will see.
;		  e.g ['Post-Script','HP-PCL','HP-GL']
;
;	FONT	- Supplies the font to be used for the interface widgets.
;
; CALLS:
;	CW_ADAS404_OUT	Creates the output options widget.
;	DYNLABEL	Sets dynamic_resize keyword for label widgets.
;	XMANAGER
;	See side effects for widget management routine.
;
; SIDE EFFECTS:
;	This routine uses a common block OUT404_BLK to maintain its state.
;	ADAS404_OUT_EV	is included in this file and is called
;			indirectly from XMANAGER during widget
;			management.
;
; CATEGORY:
;	Widgets
;
; WRITTEN:
;       William Osborn, Tessella Support Services plc, 13-Nov-1996
;
; MODIFIED:
;	1.1	William Osborn
;		First release - written using adas404_out.pro
;
; VERSION:
;	1.1	13-11-96
;
;-
;-----------------------------------------------------------------------------


pro adas404_out_ev, event

    common out404_blk, action, value
	

		;**** Find the event type and copy to common ****

    action = event.action

    case action of

		;**** 'Done' button ****

	'Done'  : begin

			;**** Get the output widget value ****

     	     child = widget_info(event.id, /child)
	     widget_control, child, get_value=value 

             widget_control, event.top, /destroy

	end


		;**** 'Cancel' button ****

	'Cancel': widget_control, event.top, /destroy

                ;**** 'Menu' button ****

        'Menu': widget_control, event.top, /destroy

    endcase

END

;-----------------------------------------------------------------------------


pro adas404_out, val, infoval, act, bitfile, devlist=devlist, font=font

    common out404_blk, action, value

		;**** Copy value to common ****

    value = val

		;**** Set defaults for keywords ****

    if not (keyword_set(font)) then font = ''
    if not (keyword_set(devlist)) then devlist = ''

		;***************************************
		;**** Pop-up a new widget           ****
		;***************************************

                ;**** create base widget ****

    outid = widget_base(title='ADAS404 OUTPUT OPTIONS',xoffset=100,yoffset=1)

		;**** Declare output options widget ****

    cwid = cw_adas404_out(outid, infoval, bitfile, value=value,  	$
			devlist=devlist, font=font )

		;**** Realize the new widget ****

    dynlabel, outid
    widget_control, outid, /realize

		;**** make widget modal ****

    xmanager, 'adas404_out', outid, event_handler='adas404_out_ev',	$
              /modal, /just_reg
 
		;**** Return the output value from common ****

    act = action
    val = value

END

