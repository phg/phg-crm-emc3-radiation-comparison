; Copyright (c) 1996 Strathclyde University .
; SCCS info: Module @(#)$Header: /home/adascvs/idl/adas4xx/adas404/d4spf0.pro,v 1.2 2004/07/06 13:14:33 whitefor Exp $ Date $Date: 2004/07/06 13:14:33 $
;+
; PROJECT:
;       ADAS IBM MVS to UNIX conversion
;
; NAME:
;       D4SPF0
;
; PURPOSE:
;       IDL user interface and communications with ADAS404 FORTRAN
;       process via pipe.
;
; EXPLANATION:
;       Firstly this routine invokes the part of the user interface
;       used to select the inputs for ADAS404. When the user's
;       interactions are complete this routine communicates with the
;       ADAS404 FORTRAN application via a UNIX pipe.  Communications
;       are to the FORTRAN subroutine D4SPF0.
;
; USE:
;       The use of this routine is specific to ADAS404, see adas404.pro.
;
; INPUTS:
;       INVAL	- A structure which determines the initial settings of
;                 the input screen widgets.
;                 INVAL is passed un-modified through to cw_adas404_in.pro,
;                 see that routine for a full description.
;
;       CLASSES - String array: the names of the isonuclear master classes
;
; OPTIONAL INPUTS:
;       None
;
; OUTPUTS:
;       INVAL	- On output the structure records the final settings of
;                 the input screen widgets if the user pressed the
;                 'Done' button, otherwise it is not changed from input.
;
;       REP     - String; Indicates whether the user pressed the 'Done'
;                 or 'Cancel' button on the interface.  The action is
;                 converted to the strings 'NO' and 'YES' respectively
;                 to match up with the existing FORTRAN code.  In the
;                 original IBM ISPF interface REP was the reply to the
;                 question 'DO YOU WANT TO END PROGRAM EXECUTION'.
;       DSFULL  - Full input filename
;
;       INFOVAL - Information string array for use in cw_adas404_proc
;
;
; OPTIONAL OUTPUTS:
;       None
;
; KEYWORD PARAMETERS:
;       FONT_LARGE	- Supplies the large font to be used for the 
;			  interface widgets.
;
;	FONT_SMALL	- Supplies the small font to be used for the 
;			  interface widgets.
; CALLS:
;       ADAS404_IN   	- Pops-up the input selections widget.
;
; SIDE EFFECTS:
;       This routine communicates with the ADAS404 FORTRAN process
;       via a UNIX pipe.
;
; CATEGORY:
;       Adas system.
;
; WRITTEN:
;       William Osborn, Tessella Support Services plc, 12th November 1996
;
; MODIFIED:
;       1.1     William Osborn
;		First release - written using d2spf0.pro
;       1.2     William Osborn
;               Removed diagnostic print statements
;
; VERSION:
;       1.1     12-11-96
;       1.2     20-11-96
;
;-----------------------------------------------------------------------------
;-

PRO d4spf0, inval, classes, rep, infoval, $
            FONT_LARGE=font_large, FONT_SMALL=font_small


                ;**** Set defaults for keywords ****

    IF NOT (KEYWORD_SET(font_large)) THEN font_large = ''
    IF NOT (KEYWORD_SET(font_small)) THEN font_small = ''

                ;**********************************
                ;**** Pop-up input file widget ****
                ;**********************************

    adas404_in, inval, classes, action, WINTITLE = 'ADAS 404 INPUT',	$
		FONT_LARGE=font_large, FONT_SMALL=font_small

                ;********************************************
                ;**** Act on the event from the widget   ****
                ;********************************************
                ;**** There are only two possible events ****
                ;**** 'Done' and 'Cancel'.               ****
                ;********************************************

    if action eq 'Done' then begin
        rep = 'NO'
    endif else begin
        rep = 'YES'
    endelse

    ;**** Prepare info strings for d4ispf.pro ****

    c = strupcase(classes(where(inval.class eq 1)))
    cs = ''
    for i=0,n_elements(c)-1 do cs = cs + c(i)+'  '
    ftype = ['Standard -> Standard','Partial Resolved -> Partial Unresolved',$
		'Partial Resolved -> Partial Resolved']
    infoval = ['Nuclear charge : '+string(inval.z0,format='(I3)'),$
               'Ion charges    : '+string(inval.ze1,format='(I3)')+' - '$
               +string(inval.ze2,format='(I3)'),$
               'File classes   : '+cs,$
               'File types     : '+ftype(inval.ireso),$
               'Year           : '+string(inval.year,format='(I2)')]

    ;**** Output to FORTRAN is done in d4spf1 ****

END
