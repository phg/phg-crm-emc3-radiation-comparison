; Copyright (c) 1996, Strathclyde University .
; SCCS Info : Module @(#)$Header: /home/adascvs/idl/adas4xx/adas404/adas404_proc.pro,v 1.1 2004/07/06 10:40:04 whitefor Exp $ Date $Date: 2004/07/06 10:40:04 $
;+
; PROJECT:
;       ADAS IBM MVS to UNIX conversion
;
; NAME:
;	ADAS404_PROC
;
; PURPOSE:
;	IDL ADAS user interface, processing options/input.
;
; EXPLANATION:
;	This routine creates and manages a pop-up window which allows
;	the user to select options and input data to control ADAS404
;	processing.
;
; USE:
;	This routine is ADAS404 specific, see d4ispf.pro for how it
;	is used.
;
; INPUTS:
;	PROCVAL	- A structure which determines the initial settings of
;		  the processing options widget.  The value is passed
;		  unmodified into cw_adas404_proc.pro.
;
;		  See cw_adas404_proc.pro for a full description of this
;		  structure.
;
;	DSFULL	- String; The full system file name of the input 
;		          dataset selected by the user for processing.
;
;	CLASSVAL- String; Includes the class as extracted from the name
;                 of the file the user has selected. Used as a label on
;                 the processing screen.
;
;       YEARVAL - String; Includes the year of the data file as extracted
;                 from the the name of the file the user has selected.
;                 Used as a label on the processing screen.
;
;       FILEVAL - String; Shows whether file is partial or standard.
;                 Used as a label on the processing screen.
;
;       CELEM   - String; The element symbol extracted from the file name.
;
;	NTDMAX	- Integer: Maximum number of user-entered temp/density
;		  pairs.
;
;	TEDA	- Float array: input temperatures
;
;	NDTIN	- Integer: Max. no. of input file electron temperatures.
;
;	NDDEN	- Integer: Max. no. of input file electron densities.
;
;	ITE	- Integer: Actual no. of input file electron temperatures.
;
;	IDE	- Integer: Actual no. of input file electron densities
;
;       BITFILE - String; the path to the dirctory containing bitmaps
;                 for the 'escape to series menu' button.
;
; OPTIONAL INPUTS:
;	None
;
; OUTPUTS:
;	PROCVAL	- On output the structure records the final settings of
;		  the processing selections widget if the user pressed the
;		  'Done' button, otherwise it is not changed from input.
;
;	ACT	- String; Either 'Done', 'Cancel' or 'Menu' for the button the
;		  user pressed to terminate the processing options
;		  window.
;
; OPTIONAL OUTPUTS:
;	None
;
; KEYWORD PARAMETERS:
;	FONT_LARGE - The name of a larger font.
;
;	FONT_SMALL - The name of a smaller font.
;
;	EDIT_FONTS - A structure of two fonts used in the table editor
;		     adas_edtab.pro, {font_norm:'',font_input:''}.
;
; CALLS:
;	CW_ADAS404_PROC	Declares the processing options widget.
;	DYNLABEL	Sets dynamic_resize keyword for label widgets.
;	ADAS404_PROC_EV Called indirectly during widget management,
;			routine included in this file.
;	XMANAGER
;
; SIDE EFFECTS:
;       This widget uses a common block PROC404_BLK, to pass the
;       variables VALUE and ACTION between the two routines. 
;
; CATEGORY:
;	Widgets
;
; WRITTEN:
;       William Osborn, Tessella Support Services plc, 12-Nov-1996
;
; MODIFIED:
;	1.1	William Osborn		
;		First Release
;
; VERSION:
;	1.1	12-11-96
;
;-
;-----------------------------------------------------------------------------


PRO adas404_proc_ev, event


    COMMON proc404_blk, action, value

    action = event.action
    CASE event.action OF

		;**** 'Done' button ****

	'Done'  : begin

		;**** Get the output widget value ****

	    widget_control, event.id, get_value=value 
	    widget_control, event.top, /destroy

	end

		;**** 'Cancel' button ****

	'Cancel': widget_control, event.top, /destroy

                ;**** 'Menu' button ****

        'Menu': widget_control, event.top, /destroy

	ELSE:	;**** Do nothing ****

    END

END

;-----------------------------------------------------------------------------


PRO adas404_proc, procval, infoval, ndtin, ndden, act, bitfile,         $
		  FONT_LARGE=font_large, FONT_SMALL=font_small,	 	$
		  EDIT_FONTS=edit_fonts


		;**** declare common variables ****

    COMMON proc404_blk, action, value

		;**** Copy "procval" to common ****

    value = procval

		;**** Set defaults for keywords ****

    IF NOT (KEYWORD_SET(font_large)) THEN font_large = ''
    IF NOT (KEYWORD_SET(font_small)) THEN font_small = ''
    IF NOT (KEYWORD_SET(edit_fonts)) THEN 				$
    edit_fonts = { font_norm:'', font_input:''}
		
                ;**** create base widget ****

    procid = widget_base(TITLE='ADAS404 PROCESSING OPTIONS', 		$
					XOFFSET=1,YOFFSET=1)

		;**** Declare processing widget ****

    cwid = cw_adas404_proc(procid, infoval, ndtin, ndden, bitfile,      $
			   PROCVAL=value,				$
		 	   FONT_LARGE=font_large, FONT_SMALL=font_small,$
			   EDIT_FONTS=edit_fonts)

		;**** Realize the new widget ****

    dynlabel, procid
    widget_control, procid, /realize

		;**** make widget modal ****

    xmanager,'adas404_proc', procid, event_handler='adas404_proc_ev', 	$
	     /modal, /just_reg

		;*** copy value back to procval for return to d4ispf ***

    act = action
    procval = value
 
END
