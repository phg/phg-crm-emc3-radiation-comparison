; Copyright (c) 1996, Strathclyde University.
; SCCS info: Module @(#)$Header: /home/adascvs/idl/adas4xx/adas404/adas404.pro,v 1.6 2004/07/06 10:39:56 whitefor Exp $ Date $Date: 2004/07/06 10:39:56 $
;+
; PROJECT:
;       ADAS IBM MVS to UNIX conversion
;
; NAME:
;	ADAS404
;
; PURPOSE:
;	The highest level routine for the ADAS 404 program.
;
; EXPLANATION:
;	This routine is called from the main adas system routine, adas.pro,
;	to start the ADAS 404 application.  Associated with adas404.pro
;	is a FORTRAN executable.  The IDL code provides the user
;	interface and output graphics whilst the FORTRAN code reads
;	in data files, performs numerical processing and creates the
;	output files.  The IDL code communicates with the FORTRAN
;	executable via a bi-directional UNIX pipe.  The unit number
;	used by the IDL for writing to and reading from this pipe is
;	allocated when the FORTRAN executable is 'spawned' (see code
;	below).  Pipe communications in the FORTRAN process are to
;	stdin and stdout, i.e streams 5 and 6.
;
;	The FORTRAN code is an independent process under the UNIX system.
;	The IDL process can only exert control over the FORTRAN in the
;	data which it communicates via the pipe.  The communications
;	between the IDL and FORTRAN must be exactly matched to avoid
;	input conversion errors.  The correct ammounts of data must be
;	passed so that neither process 'hangs' waiting for communications
;	which will never occur.
;
;	The FORTRAN code performs some error checking which is
;	independent of IDL.  In cases of error the FORTRAN may write
;	error messages.  To prevent these error messages from conflicting
;	with the pipe communications all FORTRAN errors are written to
;	output stream 0, which is stderr for UNIX.  These error messages
;	will appear in the window from which the ADAS session/IDL session
;	is being run.
;
;	In the case of severe errors the FORTRAN code may terminate
;	itself prematurely.  In order to detect this, and prevent the
;	IDL program from 'hanging' or crashing, the IDL checks to see
;	if the FORTRAN executable is still an active process before
;	each group of pipe communications.  The process identifier
;	for the FORTRAN process, PID, is recorded when the process is
;	first 'spawned'.  The system is then checked for the presence
;	of the FORTRAN PID.
;
; USE:
;	First the system settings must be established by calling
;	adas_sys_set.pro then adas404.pro is called to start the
;	ADAS 404 application;
;
;	adas_sys_set, adasrel, fortdir, userroot, centroot, $
;		      devlist, devcode, font_large, font_small, edit_fonts
;	adas404,   adasrel, fortdir, userroot, centroot, devlist, $
;		   devcode, font_large, font_small, edit_fonts
;
; INPUTS:
;	ADASREL - A string indicating the ADAS system version,
;		  e.g ' ADAS RELEASE: ADAS93 V1.4'.  The first
;		  character should be a space.
;
;	FORTDIR - A string holding the path to the directory where the
;		  FORTRAN executables are, e.g '/disk/adas/fortran/exec'
;
;	USERROOT - A string holding the path to the root directory of
;		   the user's adas data. e.g '/disk/bowen/adas'
;		   This root directory will be used by adas to construct
;		   other path names.  For example the users default data
;		   for adas205 should be in /disk/bowen/adas/adf04.  In
;		   particular the user's default interface settings will
;		   be stored in the directory USERROOT+'/defaults'.  An
;		   error will occur if the defaults directory does not
;		   exist.
;
;	CENTROOT - Like USERROOT, but this directory points to the
;		   central data area for the system.  User defaults are
;		   not stored on CENTROOT.
;
;	DEVLIST - A string array of hardcopy device names, used for
;		  graphical output. e.g ['Post-Script','HP-PCL','HP-GL']
;		  This array must mirror DEVCODE.  DEVCODE holds the
;		  actual device names used in a SET_PLOT statement.
;
;	DEVCODE - A string array of hardcopy device code names used in
;		  the SET_PLOT statement, e.g ['PS', 'PCL', 'HP']
;		  This array must mirror DEVLIST.
;
;	FONT_LARGE - The name of a larger font e.g 'courier_bold14'
;
;	FONT_SMALL - The name of a smaller font e.g 'courier_bold12'
;
;	EDIT_FONTS - A structure of two fonts used in the table editor
;		     adas_edtab.pro, {font_norm:'',font_input:''}.
;		     The two fonts are used to differentiate between
;		     editable and non-editable parts of the table. You
;		     may have to experiment to get two fonts which give
;		     the same default spacing in the edit widget.
;		     e.g {font_norm:'helvetica_bold14', $
;			  font_input:'helvetica_oblique14'}
;
; OPTIONAL INPUTS:
;	None
;
; OUTPUTS:
;	None - Note this routine should leave all inputs un-modified.
;
; OPTIONAL OUTPUTS:
;	None
;
; KEYWORD PARAMETERS:
;	None
;
; CALLS:
;	FIND_PROCESS	Checks to see if a given process is active.
;       XXDATE          Get date and time from operating system.
;       D4SPF0          Pipe comms with FORTRAN D4SPF0 routine.
;       D4ISPF          Pipe comms with FORTRAN D4ISPF routine.
;       D4SPF1          Pipe comms with FORTRAN D4SPF1 routine.
;
; SIDE EFFECTS:
;	This routine spawns a FORTRAN executable.  Note the pipe
;	communications routines listed above.  In addition to these
;	pipe communications there is one explicit communication of the
;	date to the FORTRAN code, search for 'printf, pipe' to find it.
;	There are also communications of the variable gomenu to the
;	FORTRAN which is used as a signal to stop the program in its
;	tracks and return immediately to the series menu. Do the same
;	search as above to find the instances of this.
;
; CATEGORY:
;	Adas system.
;
; WRITTEN:
;       William Osborn, Tessella Support Services plc, 12/11/96
;
; MODIFIED:
;	1.1	William Osborn
;		First release - created from adas402.pro
;	1.2	William Osborn
;		Added idum dummy integer variable for pipe reads
;	1.3	Richard Martin
;		Increased version number to 1.2
;	1.4	Richard Martin
;		Increased version number to 1.3
;	1.5	Richard Martin
;		Increased version number to 1.4
;	1.6	Richard Martin
;		Increased version number to 1.5
;
; VERSION:
;       1.1	12-11-96
;       1.2	21-11-96
;	  1.3	04-04-97
;	  1.4	27-02-98
;	  1.5	15-10-99
;	  1.6	21-03-00
;
;-
;-----------------------------------------------------------------------------

PRO ADAS404,	adasrel, fortdir, userroot, centroot, 			$
		devlist, devcode, font_large, font_small, edit_fonts

		;************************
		;**** Initialisation ****
		;************************

    adasprog = ' PROGRAM: ADAS404 V1.5'
    lpend = 0
    gomenu = 0
    deffile = userroot+'/defaults/adas404_defaults.dat'
    bitfile = centroot+'/bitmaps'
    device = ''
    classes = ['ACD - Recombination coefficients',			$
               'SCD - Ionisation coefficients',				$
               'CCD - Recombination coefficients: charge exchange',	$
	       'PRB - Power coefficients: recombination-bremsstrahlung',$
	       'PRC - Power coefficients: charge-exchange recombination',$
	       'QCD - Cross-coupling coefficients',			$
	       'XCD - Cross-coupling coefficients: parent',		$
               'PLT - Line power coefficients: total',                  $
               'PLS - ',$
               'MET - ',$
               'PEC - ',$
               'SXB - ']
    dataclasses = ['acd','scd','ccd','prb','prc','qcd','xcd','plt','pls','met'$
                   ,'pec','sxb']
    indices = intarr(12)
    indices(0:1) = 1

		;******************************************
		;**** Search for user default settings ****
		;**** If not found create defaults     ****
		;**** inval: settings for data files   ****
		;**** scval: settings for script files ****
		;**** procval: settings for processing ****
		;**** outval: settings for output      ****
		;******************************************

    files = findfile(deffile)
    if files(0) eq deffile then begin
        restore, deffile
        inval.centrootin = centroot+'/adf10/'
        inval.userrootin = userroot+'/adf10/'
    endif else begin
	classarr = intarr(9)
	classarr(0) = 1
	classarr(1) = 1
	fileinarr = strarr(50,10)
        inval = { 							$
		  ROOTPATHIN:userroot+'/adf10/', 			$
		  CENTROOTIN:centroot+'/adf10/', 			$
		  USERROOTIN:userroot+'/adf10/',			$
;		  DIROUT:userroot+'/pass/',				$
                  FILEIN:fileinarr,                                     $
		  IRESO:0,						$
                  Z0:1,                                                 $
                  ZE1:0,						$
                  ZE2:0,						$
		  YEAR:'',						$
                  CLASS:classarr,				        $
                  PREFIX:''                                             $
		}

      procval = {NEW:-1}

      outval = { out404_set, 			         $
                 TEXOUT:0,           TEXAPP:-1, 	 $
                 TEXREP:0,           TEXDSN:'', 	 $
                 TEXDEF:'paper.txt', TEXMES:'',		 $
                 DIRNAME:userroot+'/pass/'               $
        }
    end


		;****************************
		;**** Start fortran code ****
		;****************************

    spawn, fortdir + '/adas404.out', unit=pipe, /noshell, PID=pid

		;************************************************
		;**** Get date and write to fortran via pipe ****
		;************************************************

    date = xxdate()
    printf, pipe, date(0)

		;**** Read in the dimensions of the temp and dens input arrays
    idum = 0
    readf, pipe, idum
    ndtin = idum
    readf, pipe, idum
    ndden = idum

LABEL100:
		;**** Check FORTRAN still running ****

    if find_process(pid) eq 0 then goto, LABELEND

		;************************************************
		;**** Invoke user interface widget for       ****
		;**** Data file selection                    ****
		;************************************************

    d4spf0, inval, dataclasses, rep, infoval,	$
	    FONT_LARGE=font_large, FONT_SMALL=font_small

		;**** If cancel selected then end program ****

    if rep eq 'YES' then goto, LABELEND


LABEL200:
		;**** Check FORTRAN still running ****

    if find_process(pid) eq 0 then goto, LABELEND

		;************************************************
		;**** Communicate with d4ispf in fortran and ****
		;**** invoke user interface widget for       ****
		;**** Processing options                     ****
		;************************************************

    d4ispf, lpend, procval, infoval, ndtin, ndden,			$
            gomenu, bitfile, 				                $
	    FONT_LARGE=font_large, FONT_SMALL=font_small, 		$
            EDIT_FONTS=edit_fonts

		;**** If menu button clicked, tell FORTRAN to stop ****

    if gomenu eq 1 then begin
 	goto, LABELEND
    endif

		;**** If cancel selected then goto 100 ****

    if lpend eq 1 then goto, LABEL100

		;**** Fortran processing now in progress. Get ****
		;**** date and time for this run.	      ****

    date = xxdate()

		;**** Create header for output. ****

    header = adasrel+adasprog+' DATE: '+date(0)+' TIME: '+date(1)


LABEL300:
		;**** Check FORTRAN still running ****

    if find_process(pid) eq 0 then goto, LABELEND

		;************************************************
		;**** Communicate with d4spf1 in fortran and ****
		;**** invoke user interface widget for       ****
		;**** Output options                         ****
		;************************************************

    d4spf1, pipe, lpend, outval, infoval, header, gomenu, bitfile, $
	    inval, procval, dataclasses, DEVLIST=devlist, FONT=font_large

                ;**** Extra check to see whether FORTRAN is still there ****

    if find_process(pid) eq 0 then goto, LABELEND

		;**** If menu button clicked, tell FORTRAN to stop ****

    if gomenu eq 1 then begin
	goto, LABELEND
    endif
		;**** If cancel selected then erase output ****
		;**** messages and goto 200.		   ****

    if lpend eq 1 then begin
        outval.texmes = ''
        goto, LABEL200
    endif

    if(outval.texout eq 1) then begin
        printf, pipe, header
    endif
                ;************************************************
                ;**** Pop-up wait message and read in signal ****
                ;**** that the Fortran has finished          ****
                ;************************************************

    mes = ['The following adf11 format files will be created:']
    w = where(inval.class eq 1)
    outnames = outval.dirname + dataclasses(w)+'404.pass'
    mes = [mes,outnames]
    act = popup(message=mes,buttons=['OK'],font=font_large)

    mes = 'Creating adf11 files from adf10 data.'
    mes2 = 'Please wait'
    base = widget_base(title='ADAS404',/column)
    lab = widget_label(base,value=mes,font=font_large)
    lab = widget_label(base,value=mes2,font=font_large)
    widget_control, base, /hourglass
    widget_control, base, /realize

    num = 0
    on_ioerror, piperr
    readf, pipe, num

    widget_control, base, /destroy

    if num ne 1 then begin
PIPERR:
        act = popup(message='Fortran error - exiting',buttons=['OK'],$
                    font=font_large)
        goto, LABELEND
    endif

		;**** Back for more output options ****

    GOTO, LABEL300

LABELEND:

		;**** Tell d4ispf.for to stop ****
    lpend = 1
    printf, pipe, lpend
    outval.texmes = ''

		;**** Save user defaults ****

    save, inval, procval, outval, filename=deffile


END
