; Copyright (c) 1996 Strathclyde University .
; SCCS info: Module @(#)$Header: /home/adascvs/idl/adas4xx/adas404/adas404_in.pro,v 1.1 2004/07/06 10:39:59 whitefor Exp $ Date $Date: 2004/07/06 10:39:59 $
;+
; PROJECT:
;       ADAS IBM MVS to DEC UNIX conversion
;
; NAME:
;       ADAS404_IN
;
; PURPOSE:
;       IDL ADAS user interface, input file selection.
;
; EXPLANATION:
;       This routine creates and manages a pop-up window which allows
;       the user to select various input files from the UNIX file system.
;	Like the somewhat related adas401_in.pro upon which it is based,
;	this routine does not conform to the standard adas input
;	window.
; USE:
;       See d2spf0.pro for an example of how to
;       use this routine.
;
; INPUTS:
;       INVAL	- A structure which determines the initial settings of
;                 the input screen widgets.
;                 INVAL is passed un-modified through to cw_adas404_in.pro, 
;		  see that routine for a full description.
;
;       CLASSES - String array: the names of the isonuclear master classes
;
; OPTIONAL INPUTS:
;       None
;
; OUTPUTS:
;       INVAL   - On output the structure records the final settings of
;                 the input screen widgets if the user pressed the 
;		  'Done' button, otherwise it is not changed from input.
;
;       ACTION  - A string; indicates the user's action when the pop-up
;                 window is terminated, i.e which button was pressed to
;                 complete the input.  Possible values are 'Done' and
;                 'Cancel'.
;
; OPTIONAL OUTPUTS:
;       None
;
; KEYWORD PARAMETERS:
;       WINTITLE	- Title to be used on the banner for the pop-up window.
;                 	  Intended to indicate which application is running,
;                 	  e.g 'ADAS 404 INPUT'
;
;       FONT_LARGE      - Supplies the large font to be used for the
;                         interface widgets.
;
;       FONT_SMALL      - Supplies the small font to be used for the
;                         interface widgets.
;
;       RNUM      	- Number of the calling routine (in string form)
;
; CALLS:
;       CW_ADAS404_IN   Dataset selection widget creation.
;	DYNLABEL	Sets dynamic_resize keyword for label widgets.
;       XMANAGER        Manages the pop=up window.
;       ADAS404_IN_EV   Event manager, called indirectly during XMANAGER
;                       event management.
;
; SIDE EFFECTS:
;       XMANAGER is called in /modal mode. Any other widget become
;       inactive.
;
; CATEGORY:
;       Adas system.
;
; WRITTEN:
;       William Osborn, Tessella Support Services plc, 12th November 1996
;
; MODIFIED:
;       1.1     William Osborn
;		First release. Created from skeleton of adas402_in
;
; VERSION:
;       1.1     12-11-96
;
;-----------------------------------------------------------------------------
;-

PRO adas404_in_ev, event

    COMMON in404_blk, action, inval

                ;**** Find the event type and copy to common ****

    action = event.action

    CASE action OF

                ;**** 'Done' button ****

        'Done'  : begin

                ;**** Get the output widget value ****

            widget_control, event.id, get_value=inval
            widget_control, event.top, /destroy

        end


                ;**** 'Cancel' button ****

        'Cancel': widget_control, event.top, /destroy

    ENDCASE

END

;-----------------------------------------------------------------------------


PRO adas404_in, inval, classes, action, WINTITLE=wintitle,		$
                FONT_LARGE=font_large, FONT_SMALL=font_small, RNUM = rnum

    COMMON in404_blk, actioncom, invalcom

                ;**** Copy value to common ****

    invalcom = inval

                ;**** Set defaults for keywords ****

    IF NOT (KEYWORD_SET(wintitle)) THEN wintitle = 'ADAS INPUT FILE'
    IF NOT (KEYWORD_SET(font_large)) THEN font_large = ''
    IF NOT (KEYWORD_SET(font_small)) THEN font_small = ''
    IF NOT (KEYWORD_SET(rnum)) THEN rnum = '404'

                ;*********************************
                ;**** Pop-up the input screen ****
                ;*********************************

                ;**** create base widget ****

    inid = widget_base(TITLE=wintitle, XOFFSET=100, YOFFSET=1)

                ;**** create input compound widget ****

    cwid = cw_adas404_in(inid, classes, VALUE=inval, 			$
                         FONT_LARGE=font_large,	FONT_SMALL=font_small,	$
			 RNUM = rnum)

                ;**** Realize the new widget ****

    dynlabel, inid
    widget_control, inid, /realize

                ;**** make widget modal ****

    xmanager, 'adas404_in', inid, event_handler='adas404_in_ev', 	$
              /modal, /just_reg

                ;**** Return the output value from common ****

    action = actioncom
    inval = invalcom

END
