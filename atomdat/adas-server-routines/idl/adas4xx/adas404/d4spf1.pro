; Copyright (c) 1996, Strathclyde University.
; SCCS Info: Module @(#)$Header: /home/adascvs/idl/adas4xx/adas404/d4spf1.pro,v 1.3 2004/07/06 13:14:41 whitefor Exp $ Date $Date: 2004/07/06 13:14:41 $
;
;+
; PROJECT:
;       ADAS IBM MVS to UNIX conversion
;
; NAME:
;	D4SPF1
;
; PURPOSE:
;	IDL user interface and communications with ADAS404 FORTRAN
;	process via pipe.
;
; EXPLANATION:
;	First this routine reads some information from ADAS404 FORTRAN
;	via the UNIX pipe.  Next the 'Output Options' part of the
;	interface is invoked.  Finally the results of the user's
;	interactions are written back to the FORTRAN via the pipe.
;	Communications are with the FORTRAN subroutine D4SPF1.
;
; USE:
;	The use of this routine is specific to ADAS404, See adas404.pro.
;
; INPUTS:
;	PIPE	- The IDL unit number of the bi-directional pipe to the
;		  ADAS404 FORTRAN process.
;
;	INFOVAL	- String array; information about the input datasets
;
;	VALUE	- A structure which determines the initial settings of
;		  the dataset selection widget.  The initial value is
;		  set in adas404.pro.  VALUE is passed un-modified
;		  through to cw_adas_in.pro, see that routine for a full
;		  description.
;
;	HEADER  - Header information used for text output.
;
;       BITFILE - String; the path to the dirctory containing bitmaps
;                 for the 'escape to series menu' button.
;
; OPTIONAL INPUTS:
;	None.
;
; OUTPUTS:
;	LPEND	- Integer; indicates user action on leaving output
;		  selections.  lpend is set to 1 if the user exited
;		  with the 'Cancel' button and 0 if the user exited
;		  with 'Done'.
;
;	VALUE	- On output the structure records the final settings of
;		  the output selection widget if the user pressed the
;		  'Done' button otherwise it is not changed from input.
;
;       GOMENU  - Int; flag - set to 1 if user has selected 'escape direct
;                 to series menu , otherwise 0
;
; OPTIONAL OUTPUTS:
;	None.
;
; KEYWORD PARAMETERS:
;	DEVLIST	- A string array;  Each element of the array is the
;		  name of a hardcopy graphics output device.  This
;		  list must mirror the DEVCODE list.  DEVLIST is the
;		  list which the user will see.
;		  e.g ['Post-Script','HP-PCL','HP-GL']
;
;	FONT	- Supplies the font to be used for the interface widgets.
;
; CALLS:
;	ADAS404_OUT	User interface - output options.
;
; SIDE EFFECTS:
;	This routine communicates with the ADAS404 FORTRAN process
;	via a UNIX pipe.
;
; CATEGORY:
;	Adas system.
;
; WRITTEN:
;       William Osborn, Tessella Support Services plc, 13-11-96
;
; MODIFIED: 
;	1.1	William Osborn
;		First release
;       1.2     William Osborn
;               Removed diagnostic print statements
;       1.3     William Osborn
;               Removed check_pipe
;
; VERSION:
;	1.1	13-11-96
;       1.2     20-11-96
;       1.3     20-11-96
;-
;-----------------------------------------------------------------------------

PRO d4spf1, pipe, lpend, value, infoval, header, gomenu, bitfile, $
            inval, procval, classes, DEVLIST=devlist, FONT=font


                ;**** Set defaults for keywords ****

    IF NOT (KEYWORD_SET(font)) THEN font = ''

		;********************************
		;**** Read data from fortran ****
		;********************************

		;**************************************
		;**** Pop-up output options widget ****
		;**************************************

    adas404_out, value, infoval, action, bitfile,                   $
                 DEVLIST=devlist, FONT=font

		;********************************************
		;**** Act on the output from the widget  ****
		;**** There are three possible actions   ****
		;**** 'Done', 'Cancel' and 'Menu'.       ****
		;********************************************

    if action eq 'Done' then begin
        lpend = 0
    endif else if action eq 'Menu' then begin
        lpend = 1
        gomenu = 1
    endif else begin
        lpend = 1
    endelse

		;*******************************
		;**** Write data to fortran ****
		;*******************************

    if lpend eq 0 then begin

        printf, pipe, lpend

		;*** If text output requested tell FORTRAN ****
     
        printf, pipe, value.texout
        if (value.texout eq 1) then begin
	    printf, pipe, value.texdsn
        endif

        printf, pipe, strmid(strtrim(procval.title,2),0,32)
        printf, pipe, fix(inval.z0)
        printf, pipe, fix(inval.ze1)
        printf, pipe, fix(inval.ze2)
        printf, pipe, procval.maxd
        printf, pipe, procval.maxt
        printf, pipe, procval.ifout
        printf, pipe, inval.ireso
        for i=0,procval.maxd-1 do begin
            printf, pipe, procval.din(i)
        endfor
        for i=0,procval.maxt-1 do begin
            printf, pipe, procval.tin(i)
        endfor
        printf, pipe, inval.year
        ;**** Write out filenames ****
        nsweep = fix(inval.ze2-inval.ze1+1)
        for j=0,9 do begin
            for i=0,nsweep-1 do begin
                printf, pipe, inval.filein(i,j)
            endfor
        endfor
        printf, pipe, inval.filein(nsweep,9) ;**** Extra MET member output
        for j=0,8 do begin
;            dir = classes(j)+inval.year
;            if inval.ireso ne 0 then dir=dir+'r'
;            i4z0ie, inval.z0, symbol
;            fname = value.dirname+dir+'/'+dir+'_'+strlowcase(symbol)+'.dat'
            fname = value.dirname + classes(j)+'404.pass'
            printf, pipe, fname
            printf, pipe, inval.class(j)
        endfor
;    check_pipe, pipe

    endif

		;**************************************************
		;**** Set messages and settings ready for when ****
		;**** output window is re-displayed.           ****
		;**************************************************

    if lpend eq 0 then begin
        if value.texout eq 1 then begin

		;**** If text output is requested enable append ****
		;**** for next time and update the default to   ****
		;**** the current text file.                    ****

            value.texapp=0
            if value.texrep ge 0 then value.texrep = 0
      		value.texmes = 'Output written to file.'
        endif

    endif

END
