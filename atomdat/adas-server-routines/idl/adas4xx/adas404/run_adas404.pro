;----------------------------------------------------------------------
;+
; PROJECT    :  ADAS
;
; NAME       :  run_adas404
;
; PURPOSE    :  Runs ADAS404 adf10->adf11 file generator code as an IDL
;               subroutine.
;
; ARGUMENTS  :  All output arguments will be defined appropriately.
;
;               NAME      I/O    TYPE   DETAILS
; INPUTS     :  root       I     str    top directory of adf10 files.
;               year       I     int    year of adf10 data.
;               classes    I     str()  array of required classes from:
;                                         'acd' : recombination
;                                         'scd' : ionisation
;                                         'qcd' : metastable cross coupling
;                                         'xcd' : parent cross coupling
;                                         'ccd' : charge exchange recomb.
;                                         'prb' : recob. + bremss. power
;                                         'plt' : total line power
;                                         'prc' : charge exchange power
;               prefix     I     str    prefix of adf10 set (usually 'pj')
;               te         I     real() electron temperatures requested
;               dens       I     real() electron densities requested
;               unit_te    I     str    'eV', 'K' or 'red' (default eV)
;               iz0        I     int    atomic number
;               zlow       I     int    lowest Z required in adf11 file
;               zhigh      I     int    highest Z required in adf11 file
;               type       I     str    'resolved' or 'unresolved' (no qcd/xcd)
;               log        I     str    name of output text file.
;                                       (defaults to no output).
;
; OPTIONAL   :  get_adf10  O     str()  set of adf10 files used in adas404 - nb
;                                       routine does not generate adf11 file if
;                                       this output is selected.
;               user_adf10 I     str()  use this set of adf10 rather than
;                                       that generated in routine.
;
; KEYWORDS   :  help              -     display help entry
;
; OUTPUTS    :  none to screen. class + '404.pass'
;
; NOTES      :  run_adas404 uses the spawn command. Therefore IDL v5.3
;               cannot be used. Any other version should work.
;
;               The get_adf10 and user_adf10 are present to allow selective
;               replacement of adf10 files.
;
; AUTHOR     :  Martin O'Mullane
;
; DATE       :  28-03-2006
;
; MODIFIED
;       1.1     Martin O'Mullane
;                 - First version.
;
; DATE
;       1.1     28-03-2006
;-
;----------------------------------------------------------------------

PRO run_adas404, root       = root,       $
                 year       = year,       $
                 prefix     = prefix,     $
                 classes    = classes,    $
                 iz0        = iz0,        $
                 zlow       = zlow,       $
                 zhigh      = zhigh,      $
                 te         = te,         $
                 dens       = dens,       $
                 type       = type,       $
                 unit_te    = unit_te,    $
                 log        = log,        $
                 get_adf10  = get_adf10,  $
                 user_adf10 = user_adf10, $
                 help       = help


; If asked for help

if keyword_set(help) then begin
   doc_library, 'run_adas404'
   return
endif

; First check that we are not using IDL v5.3

if !version.release EQ '5.3' then begin
   message, 'This version of IDL (v5.3) will not work - ' + $
            'change to v5.0, v5.1, v5.2, v5.4, v6.0 or above'
   return
endif


; Check that we get all inputs and that they are correct. Otherwise print
; a message and return to command line.

on_error, 2


; Temperatures and densities

if n_elements(te) eq 0 then message, 'User requested temperatures are missing'

partype=size(te, /type)
if (partype lt 2) or (partype gt 5) then begin
   message,'Temperature must be numeric'
endif else te = DOUBLE(te)

if n_elements(dens) eq 0 then message, 'User requested densities are missing'

partype=size(dens, /type)
if (partype lt 2) or (partype gt 5) then begin
   message,'Desnities must be numeric'
endif else dens = DOUBLE(dens)

maxt = n_elements(te)
maxd = n_elements(dens)

if n_elements(unit_te) EQ 0 then ifout = 2 else begin
   case strlowcase(strtrim(unit_te,2)) of
      'k'   : ifout = 1
      'ev'  : ifout = 2
      'red' : ifout = 3
      else  : message, 'Unknown temperature units'
   endcase
endelse

; Resolved or not

str = strupcase(type)
if strpos(str, 'UN') NE -1 then itype = 1 else itype = 2


; File outputs

if n_elements(log) EQ 0 then output_file = '/dev/null' $
                        else output_file = log


; Generate adf10 filenames required by adas404

if year LT 0 OR year GT 99 then message, 'Incorrect year'
if iz0 LT 1 OR iz0 GT 50 then message, 'Incorrect atomic number'

if zhigh LT 0 OR zhigh GT iz0 then message, 'Range must be 0<=Zhigh<=Z0'
if zlow LT 0 OR zlow GT zhigh then message, 'Range must be 0<=Zlow<=Zhigh'


dataclasses = ['acd', 'scd', 'ccd', 'prb', 'prc', 'qcd', 'xcd', 'plt', 'pls']
indices     = intarr(9)

for j = 0, n_elements(classes)-1 do begin
   ind = where(dataclasses EQ classes[j], count)
   if count GT 0 then indices[ind] = 1
endfor


dirnames = strcompress(dataclasses + string(year), /remove_all)
if itype EQ 1 then dirnames = [dirnames,'met'+strcompress(string(year), /remove_all)]

nsweep  = zhigh - zlow + 1
nclass  = n_elements(dirnames)
infiles = strarr(50,10)

for i = 0, nsweep-1 do begin

   nelec = iz0 - (zlow + i)
   i4z0ie, nelec, symbol
   symbol = strlowcase(symbol)

   to_find = dirnames + '_' + symbol
   if strlen(symbol) eq 1 then symbol = ' ' + symbol
   to_find = prefix + '#' + to_find + '##.dat'

   for j = 0, nclass-1 do infiles[i,j] = root + dirnames[j] + '/' + to_find[j]

endfor

; Remove pls class

infiles[*,8] = ''


; Return adf10 set if requested and stop

if arg_present(get_adf10) then begin
   get_adf10 = infiles[0:nsweep,*]
   message, 'adf10 files returned - now stopping', /continue
   return
endif



; Use the user supplied adf10 set in preference to the one generated above

if n_elements(user_adf10) GT 0 then begin

   message, 'Using user supplied adf10 set', /continue
   infiles = user_adf10

endif


; Now run the ADAS404 code

; location of files and launch fortran.

fortdir = getenv("ADASFORT")
spawn, fortdir+'/adas404.out', unit=pipe, /noshell, PID=pid


; Input screen

date = xxdate()
printf, pipe, date[0]

idum = 0
readf, pipe, idum
ndtin = idum
readf, pipe, idum
ndden = idum

printf, pipe, 0L
printf, pipe, 1L
printf, pipe, output_file

printf, pipe, 'A title'
printf, pipe, iz0
printf, pipe, zlow
printf, pipe, zhigh
printf, pipe, maxd
printf, pipe, maxt
printf, pipe, ifout
printf, pipe, itype

for j = 0, maxd-1 do printf, pipe, dens[j]
for j = 0, maxt-1 do printf, pipe, te[j]

printf, pipe, string(year, format='(i2)')

for j = 0, 9 do begin
   for i = 0, nsweep-1 do printf, pipe, infiles[i,j]
endfor
printf, pipe, infiles[nsweep, 9]

for j = 0, 8 do begin
   fname = dataclasses[j]+'404.pass'
   printf, pipe, fname
   printf, pipe, indices[j]
endfor

printf, pipe, 'A header'

ierr = -1L
readf, pipe, ierr
printf, pipe, 1L
if ierr NE 1 then message, 'There was a problem'


; Finish-up

adas_wait, pid
close, /all

END
