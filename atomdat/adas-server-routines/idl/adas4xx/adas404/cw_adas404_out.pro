; Copyright (c) 1996, Strathclyde University.
; SCCS Info : Module @(#)$Header: /home/adascvs/idl/adas4xx/adas404/cw_adas404_out.pro,v 1.3 2004/07/06 12:44:56 whitefor Exp $ Date $Date: 2004/07/06 12:44:56 $
;+
; PROJECT:
;       ADAS IBM MVS to UNIX conversion
;
; NAME:
;	CW_ADAS404_OUT()
;
; PURPOSE:
;	Produces a widget for ADAS404 output options.
;
; EXPLANATION:
;	This function declares a compound widget consisting of an
;       output passing file directory selection widget and an
;	output file widget cw_adas_outfile.pro.  The text output
;	file specified in this widget is for tabular (paper.txt)
;	output. This widget also includes a 'Cancel' button,
;       a 'Done' button and a 'Menu' button.
;	The compound widget cw_adas_outfile.pro
;	included in this file is self managing.  This widget only
;	handles events from the 'Done', 'Cancel' and 'Menu' buttons and
;	the charge state selection widget described below.
;
;	This widget only generates events for the 'Done', 'Cancel' and 'Menu'
;	buttons.
;
; USE:
;	This routine is specific to adas404.
;
; INPUTS:
;	PARENT	- Long integer; ID of parent widget.
;
;	INFOVAL	- String array; information about the input datasets
;
;       BITFILE - String; the path to the dirctory containing bitmaps
;                 for the 'escape to series menu' button.
;
; OPTIONAL INPUTS:
;	None.
;
; OUTPUTS:
;	The return value of this function is the ID of the declared widget.
;
; OPTIONAL OUTPUTS:
;	None.
;
; KEYWORD PARAMETERS:
;	DEVLIST	- A string array;  Each element of the array is the
;		  name of a hardcopy graphics output device.  This
;		  list must mirror the DEVCODE list.  DEVLIST is the
;		  list which the user will see.
;		  e.g ['Post-Script','HP-PCL','HP-GL']
;
;	VALUE	- A structure which determines the initial settings of
;		  the output options widget.  The structure is made
;		  up of two parts.  Each part is the same as the value
;		  structure of one of the two main compound widgets
;		  included in this widget.  See cw_adas_gr_sel and
;		  cw_adas_outfile for more details.  The default value is;
;
;		      {	TEXOUT:0, TEXAPP:-1, 				$
;			TEXREP:0, TEXDSN:'', 				$
;			TEXDEF:'',TEXMES:'', 				$
;                       DIRNAME:''					$
;		      }
;
;		  For CW_ADAS_OUTFILE tabular output (paper.txt);
;			TEXOUT	Integer; Activation button 1 on, 0 off
;			TEXAPP	Integer; Append button 1 on, 0 off, -1 no button
;			TEXREP	Integer; Replace button 1 on, 0 off, -1 no button
;			TEXDSN	String; Output file name
;			TEXDEF	String; Default file name
;			TEXMES	String; file name error message
;
;			DIRNAME String; output directory for iso-nuclear files
;
;	UVALUE	- A user value for this widget.
;
;	FONT	- String; the name of a font to be used for all text in
;		  this widget.  Default to system default font.
;
; CALLS:
;	CW_ADAS_OUTFILE	Output file name entry widget.
;	See side effects for other related widget management routines.
;
; SIDE EFFECTS:
;
; 	Two other routines are included in this file
;	OUT404_GET_VAL()
;	OUT404_EVENT()
;
; CATEGORY:
;	Compound Widget
;
; WRITTEN:
;       William Osborn, Tessella Support Services plc, 13-Nov-1996
; MODIFIED:
;	1.1	William Osborn
;		First release
;       1.2     William Osborn
;               Removed diagnostic print statements
;       1.3     Lorne Horton
;               Removed obsolete widget control statement.
;
; VERSION:
;	1.1	13-11-96
;     1.2     20-11-96
;	1.3	20-10-97
;
;-
;-----------------------------------------------------------------------------

FUNCTION out404_get_val, id

                ;**** Return to caller on error ****

    ON_ERROR, 2

                ;**** Retrieve the state ****

    parent=widget_info(id, /parent)
    widget_control, parent, get_uvalue=state, /no_copy

		;**** Get directory name ****

    widget_control, state.dirnameid, get_value = dir

		;**** Get text output settings ****

    widget_control, state.paperid, get_value=papos
    os = { out404_set, 						 $
	   TEXOUT:papos.outbut,  TEXAPP:papos.appbut, 		 $
	   TEXREP:papos.repbut,  TEXDSN:papos.filename, 	 $
	   TEXDEF:papos.defname, TEXMES:papos.message, 		 $
	   DIRNAME:dir(0)					 $
        }

                ;**** Return the state ****

  widget_control, parent, set_uvalue=state, /no_copy

  RETURN, os 

END

;-----------------------------------------------------------------------------

FUNCTION out404_event, event

    COMMON outblock404, font

                ;**** Base ID of compound widget ****

    parent=event.top

                ;**** Retrieve the state ****

    widget_control, parent, get_uvalue=state, /no_copy

		;*********************************
		;**** Clear previous messages ****
		;*********************************

    widget_control, state.messid, set_value=' '

                ;************************
                ;**** Process events ****
                ;************************

    CASE event.id OF


		;***********************
		;**** Cancel button ****
		;***********************

        state.cancelid: begin
            new_event = {ID:parent, TOP:event.top, 			$
		         HANDLER:0L, ACTION:'Cancel'}
        end

		;*********************
		;**** Done button ****
		;*********************

        state.doneid: begin

		;****************************************************
                ;**** Return the state before checking can start ****
		;**** with the get_value keyword.                ****
		;****************************************************

            widget_control, parent, set_uvalue=state, /no_copy

		;***************************************
		;**** Check for errors in the input ****
		;***************************************

	    error = 0

		;**** Get a copy of the widget value ****

	    widget_control, event.handler, get_value=os
	
		;**** Check for widget error messages ****

	    if os.texout eq 1 and strtrim(os.texmes) ne '' then error=1

                ;**** Retrieve the state   ****

            widget_control, parent, get_uvalue=state, /no_copy

	    if error eq 1 then begin
	        widget_control, state.messid, set_value=		$
		'**** Error in output settings ****'
	        new_event = 0L
	    endif else begin
	        new_event = {ID:parent, TOP:event.top, HANDLER:0L, 	$
                             ACTION:'Done'}
	    endelse

        end
 
		;*********************
                ;**** Menu button ****
		;*********************

        state.outid: begin
            new_event = {ID:parent, TOP:event.top, HANDLER:0L,          $
                         ACTION:'Menu'}
        end

        ELSE: begin
            new_event = 0L
          end

    ENDCASE

                ;**** Return the state   ****

    widget_control, parent, set_uvalue=state, /no_copy

    RETURN, new_event

END

;-----------------------------------------------------------------------------

FUNCTION cw_adas404_out, parent, infoval, bitfile, DEVLIST=devlist,    $
		         VALUE=value, UVALUE=uvalue, FONT=font


    COMMON outblock404, fontcom


    IF (N_PARAMS() EQ 0) THEN MESSAGE, 'Must specify parent for cw_adas404_out'
    ON_ERROR, 2					;return to caller

		;**** Set defaults for keywords ****

    IF NOT (KEYWORD_SET(devlist)) THEN devlist = ''
    IF NOT (KEYWORD_SET(uvalue)) THEN uvalue = 0
    IF NOT (KEYWORD_SET(font)) THEN font = ''
    IF NOT (KEYWORD_SET(value)) THEN begin
	os = {out404_set, 						$
	TEXOUT:0, TEXAPP:-1, 						$
	TEXREP:0, TEXDSN:'', 						$
        TEXDEF:'paper.txt',TEXMES:'',DIRNAME:'' }
    ENDIF ELSE BEGIN
	os = {out404_set, 						$
	TEXOUT:value.texout, TEXAPP:value.texapp, 			$
	TEXREP:value.texrep, TEXDSN:value.texdsn, 			$
	TEXDEF:value.texdef, TEXMES:value.texmes, DIRNAME:value.dirname }
    ENDELSE

    fontcom = font

		;**********************************************
		;**** Create the 404 Output options widget ****
		;**********************************************

		;**** create base widget ****

    cwid = widget_base( parent, UVALUE = uvalue, 			$
			EVENT_FUNC = "out404_event", 			$
			FUNC_GET_VALUE = "out404_get_val", 		$
			/COLUMN)

		;**** Add dataset information ****

    base = widget_base(cwid,/column,/frame)
    l = widget_label(base,value='Input iso-electronic file information:',$
			font=font)
    for i=0,n_elements(infoval)-1 do begin
	l = widget_label(base,value=infoval(i),font=font)
    endfor

   		;**** Widget for directory name ****
    base = widget_base(cwid,/column,/frame)
    l = widget_label(base, value='Directory for iso-nuclear master file output:',font=font)
    dirnameid = widget_text(base,value=os.dirname,font=font,xsize=40,/editable)

		;**** Widget for text output ****

    outfval = { OUTBUT:os.texout, APPBUT:-1, REPBUT:os.texrep, 		$
	        FILENAME:os.texdsn, DEFNAME:os.texdef, MESSAGE:os.texmes }
    base = widget_base(cwid,/row,/frame)
    paperid = cw_adas_outfile(base, OUTPUT='Text Output', 		$
                              VALUE=outfval, FONT=font)

		;**** Error message ****

    messid = widget_label(cwid,value=' ', font=font)

		;**** add the exit buttons ****

    base = widget_base(cwid, /row)
    menufile = bitfile + '/menu.bmp'
    read_X11_bitmap, menufile, bitmap1
    outid = widget_button(base, value=bitmap1)          ;menu button
    cancelid = widget_button(base, value='Cancel', font=font)
    doneid = widget_button(base, value='Done', font=font)
  
		;*************************************************
		;**** create a state structure for the pop-up ****
		;**** window.                                 ****
		;*************************************************

    new_state = { PAPERID:paperid, OUTID:outid,		$
		  CANCELID:cancelid, DONEID:doneid, MESSID:messid,$
		  DIRNAMEID:dirnameid}

                ;**** Save initial state structure ****

    widget_control, parent, set_uvalue=new_state, /no_copy

    RETURN, cwid

END

