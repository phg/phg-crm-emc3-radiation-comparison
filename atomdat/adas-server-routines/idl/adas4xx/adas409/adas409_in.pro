; Copyright (c) 1997 Strathclyde University.
; SCCS info: Module @(#)$Header: /home/adascvs/idl/adas4xx/adas409/adas409_in.pro,v 1.1 2004/07/06 10:47:31 whitefor Exp $ Date $Date: 2004/07/06 10:47:31 $
;+
; PROJECT:
;       ADAS IBM MVS to DEC UNIX conversion
;
; NAME:
;       ADAS409_IN
;
; PURPOSE:
;       IDL ADAS user interface, input file selection.
;
; EXPLANATION:
;       This routine creates and manages a pop-up window which allows
;       the user to select various input files from the UNIX file system.
;	Like the somewhat related adas401_in.pro upon which it is based,
;	this routine does not conform to the standard adas input
;	window. In this case it is due to the fact that for adas409
;	various input files need to be specified rather than just the
;	single one. There are therefore two lots of information
;	required before proceeding on to the processing options.
;	The name (if any) of the 'line and analysis selection file'
;	(or script file) and the associated 'isonuclear master files'.
; USE:
;       See d9spf0.pro for an example of how to
;       use this routine.
;
; INPUTS:
;       INVAL	- A structure which determines the initial settings of
;                 the input screen widgets.
;                 INVAL is passed un-modified through to cw_adas409_in.pro, 
;		  see that routine for a full description.
;
;       CLASSES - String array: the names of the isonuclear master classes
;
; OPTIONAL INPUTS:
;       None
;
; OUTPUTS:
;       INVAL   - On output the structure records the final settings of
;                 the input screen widgets if the user pressed the 
;		  'Done' button, otherwise it is not changed from input.
;
;       ACTION  - A string; indicates the user's action when the pop-up
;                 window is terminated, i.e which button was pressed to
;                 complete the input.  Possible values are 'Done' and
;                 'Cancel'.
;
; OPTIONAL OUTPUTS:
;       None
;
; KEYWORD PARAMETERS:
;       WINTITLE	- Title to be used on the banner for the pop-up window.
;                 	  Intended to indicate which application is running,
;                 	  e.g 'ADAS 409 INPUT'
;
;       FONT_LARGE      - Supplies the large font to be used for the
;                         interface widgets.
;
;       FONT_SMALL      - Supplies the small font to be used for the
;                         interface widgets.
; CALLS:
;       CW_ADAS409_IN   Dataset selection widget creation.
;       XMANAGER        Manages the pop=up window.
;       ADAS409_IN_EV   Event manager, called indirectly during XMANAGER
;                       event management.
;
; SIDE EFFECTS:
;       XMANAGER is called in /modal mode. Any other widget become
;       inactive.
;
; CATEGORY:
;       Adas system.
;
; WRITTEN:
;       Alessandro Lanzafame, December 1995.
;	  Based on adas405_in.pro v1.4.
;
; MODIFIED:	1.1     Richard Martin
;			  Put under SCCS control.
;
; VERSION:	1.1     12-03-98
;
;-----------------------------------------------------------------------------
;-

PRO adas409_in_ev, event

    COMMON in409_blk, action, inval

                ;**** Find the event type and copy to common ****

    action = event.action

    CASE action OF

                ;**** 'Done' button ****

        'Done'  : begin

                ;**** Get the output widget value ****

            widget_control, event.id, get_value=inval
            widget_control, event.top, /destroy

        end


                ;**** 'Cancel' button ****

        'Cancel': widget_control, event.top, /destroy

    ENDCASE

END

;-----------------------------------------------------------------------------


PRO adas409_in, inval, classes, action, WINTITLE=wintitle,		$
                FONT_LARGE=font_large, FONT_SMALL=font_small

    COMMON in409_blk, actioncom, invalcom

                ;**** Copy value to common ****

    invalcom = inval

                ;**** Set defaults for keywords ****

    IF NOT (KEYWORD_SET(wintitle)) THEN wintitle = 'ADAS INPUT FILE'
    IF NOT (KEYWORD_SET(font_large)) THEN font_large = ''
    IF NOT (KEYWORD_SET(font_small)) THEN font_small = ''

                ;*********************************
                ;**** Pop-up the input screen ****
                ;*********************************

                ;**** create base widget ****

    inid = widget_base(TITLE=wintitle, XOFFSET=100, YOFFSET=1)

                ;**** create input compound widget ****

    cwid = cw_adas409_in(inid, classes, VALUE=inval, 			$
                         FONT_LARGE=font_large,	FONT_SMALL=font_small)

                ;**** Realize the new widget ****

    widget_control, inid, /realize

                ;**** make widget modal ****

    xmanager, 'adas409_in', inid, event_handler='adas409_in_ev', 	$
              /modal, /just_reg

                ;**** Return the output value from common ****

    action = actioncom
    inval = invalcom

END
