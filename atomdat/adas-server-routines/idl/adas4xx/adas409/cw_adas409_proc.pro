; Copyright (c) 1996, Strathclyde University.
; SCCS info: Module @(#)$Header: /home/adascvs/idl/adas4xx/adas409/cw_adas409_proc.pro,v 1.1 2004/07/06 12:48:15 whitefor Exp $ Date $Date: 2004/07/06 12:48:15 $
;+
; PROJECT:
;       ADAS Programs
;
; NAME:
;	CW_ADAS409_PROC()
;
; PURPOSE:
;	Produces a widget for ADAS409 processing options/input.
;
; EXPLANATION:
;	   This function creates a compound widget consisting of :-
;	   a text widget holding an editable 'Run title', 
;	   the script file name/browse widget ,
;	   information widgets,
;	   typein widgets for the electron and hydrogen isotopes,
;	   if a script file is chosen then a list of lines in that
;	   file along with the current selection,
;	   a table widget for temperature data,
;          a table widget for density data ,
;	   a button to enter default values into the table, 
;	   a button to clear the table,
;	   a message widget, 
;	   an 'Escape to series menu', a 'Cancel' and a 'Done' button.
;
;	The compound widgets included in this widget are self managing.
;	This widget only manages events from the  'Default' button,
;	the 'Cancel' button and the 'Done' button and the isotope
;	typeins and the 'escape to series menu' button.
;
; USE:
;	This widget is specific to ADAS409, see adas409_proc.pro
;	for use.
;
; INPUTS:
;	TOPPARENT- Long integer, ID of parent widget.
;
;	DSFULL	- String; The full system file name of the input 
;		  dataset selected by the user for processing.
;
;       SYMBOL  - String; isonuclear element symbol.
;
;       CLASSSTRING - String; list of the selected master classes.
;
;	NTDMAX  - Integer; max number of user entered temperatures
;
;	NDDMAX  - Integer; max number of user entered densities
;
;       NLINE   - Int; No. of lines in script file
;
;       CIION   - String array; emitting ion for a given line
;
;       CMPTS   - Int array: no. components for each line in script file
;
;       CTITLE  - String array: Any information given in script file on
;                 lines to be analysed.
;
;       BITFILE - String; the path to the directory containing bitmaps
;                 for the 'escape to series menu' button.
;
; OPTIONAL INPUTS:
;	None
;
; OUTPUTS:
;	The return value of this function is the ID of the declared widget.
;
;	ACT 	- String; result of this widget, 'done' or 'cancel'
;
; OPTIONAL OUTPUTS:
;	None
;
; KEYWORD PARAMETERS:
;	PROCVAL	- A structure which determines the initial settings of
;		  the processing options widget.
;
;		  The default PROCVAL is created thus;
;
;     		ps = {proc409_set, $
;			new   : 0,		$
;                	title : '',             $
;			lsnull: 0,		$
;			ifout : 1,		$
;			ispline: 0,		$
;			hyval : 1.0,		$
;			elval : float,		$
;                       tein  : temp_arr,       $
;                       tihn  : temp_arr,       $
;                       dein  : dens_arr,       $
;                       dhin  : dens_arr,       $
;                       maxt  : 0,              $
;                       maxd  : 0,              $
;			tvals : dblarr(2,ndtin),$
;			devals: dblarr(nddin),  $
;			dhvals: dblarr(nddin)   $
;             }
;
;
;		NEW     Flag which defines whether or not default values
;			exist or not. (< 0 if no values exist)
;
; 		TITLE	Entered general title for program run
;
;		HYVAL   Float; the hydrogen isotope mass number.
;
;		ELVAL	Float; the element isotope mass number.
;
;		IFOUT	Flag for the temperature units in use:
;			1=Kelvin, 2=eV
;
;		ISPLINE Integer - the selected line from the script file.
;
;		TEIN  	Input electron temperatures entered by user
;
;		TIHN  	Input hydrogen temperatures entered by user
;
;		DEIN	Input electron densities entered by user
;
;		DHIN	Input hydrogen densities entered by user
;
;		MAXT	Number of temperatures in table entered by user
;
;		MAXD	Number of densities in table
;
;		TVALS	The input temperatures in two different units 
;
;		DEVALS  The input electron density in cm-3
;
;		DHVALS  The input Hydrogen density in cm-3
;
;		Most of these structure elements map onto variables of
;		the same name in the ADAS409 FORTRAN program.
;
;
;	UVALUE	- A user value for the widget. Default 0.
;
;	FONT_LARGE - The name of a larger font.  Default current system
;		     font
;
;	FONT_SMALL - The name of a smaller font. Default current system
;		     font.
;
;	EDIT_FONTS - A structure of two fonts used in the table editor
;		     adas_edtab.pro, {font_norm:'',font_input:''}.  Defaults
;		     to current system font.
;
;	NUM_FORM   - String; Numeric format to use in tables.  Default
;			'(E10.3)'
;		     
;
; CALLS:
;	POPUP		Popup warning window with buttons.
;	CW_ADAS_DSBR	Dataset name and comments browsing button.
;	CW_ADAS_TABLE   Adas data table widget.
;	XXEIAM		Converts element symbol to atomic mass
;	NUM_CHK		Checks validity of a user's numbers
;	CW_SINGLE_SEL   Single selection compound widget
;	TEXT_TABLE	Constructs a correctly aligned text table
;	READ_X11_BITMAP Reads in the bitmap for the 'escape to series
;			menu' button.
;
;	See side effects for other related widget management routines.
;
; SIDE EFFECTS:
;
;	The following widget management routines are included in this file;
;	PROC409_SET_VAL()	Sets the current PROCVAL structure.
;	PROC409_GET_VAL()	Returns the current PROCVAL structure.
;	PROC409_EVENT()		Process and issue events.
;	
; CATEGORY:
;	Compound Widget
;
; WRITTEN:
;       Alessandro Lanzafame, University of Strathclyde , 17th October 1996
;		Based on cw_adas405_proc.pro
;
; MODIFIED:
;	1.1	Richard Martin.
;		Put under SCCS control.
;
; VERSION:
;	1.1	11-03-98
;-
;-----------------------------------------------------------------------------

PRO proc409_set_val, id, value

                ;**** Return to caller on error ****

    ON_ERROR, 2

                ;**** Retrieve the state ****

    first_child = widget_info(id, /child)
    widget_control, first_child, get_uvalue=state, /no_copy

		;**** Set input focus ****

    widget_control, state.eliso, input_focus=value

                ;**** Reset the value of state variable ****

    widget_control, first_child, set_uvalue=state, /no_copy

END

;-----------------------------------------------------------------------

FUNCTION proc409_get_val, id

    COMMON cw_proc409_blk, lsnull

                ;**** Return to caller on error ****

    ON_ERROR, 2

		 ;***************************************
                 ;****     Retrieve the state 	     ****
		 ;**** Get first_child widget id     ****
		 ;**** because state is stored there ****
		 ;***************************************

    first_child = widget_info(id, /child)
    widget_control, first_child, get_uvalue = state

		;***********************************
		;**** Get run title from widget ****
		;**** Then centre in in string  ****
		;**** of 40 characters 		****
		;***********************************

    widget_control, state.runid, get_value=title
    title = strcompress(title(0))
    title_len = strlen(title) 
    if (title_len gt 40 ) then begin 
	title = strmid(title,0,37)
        widget_control, state.messid, set_value="Title too long - truncated "
        widget_control, state.runid, set_value=title
	wait ,1
    endif
    pad = (40 - title_len)/2 
    spaces = '                                         '
    title(0) = strmid(spaces,0,(pad-1)) + title + strmid(spaces,0,(pad-1))
  

		;*****************************************************
		;**** Get new temperature data from table widget ****
		;*****************************************************

    widget_control, state.tempid, get_value=tempval
    tabledata = tempval.value
    ifout = tempval.units+1
 
;**** Copy out temperature values ****
    tein = dblarr(state.ntdmax)
    tihn = dblarr(state.ntdmax)
    blanks = where(strtrim(tabledata(0,*),2) eq '')

;**** next line assumes that all blanks are at the end of the columns
    if blanks(0) ge 0 then maxt=blanks(0) else maxt=state.ntdmax
    if maxt gt 0 then begin
	tein(0:maxt-1) = double(tabledata(0,0:maxt-1))
	tihn(0:maxt-1) = double(tabledata(3,0:maxt-1)) 
    endif

;**** Fill in the rest with zeroes ****
    if maxt lt state.ntdmax then begin
        tein(maxt:state.ntdmax-1) = double(0.0)
        tihn(maxt:state.ntdmax-1) = double(0.0)
    endif

    state.maxt = maxt
    tvals = tabledata(1:2,*)

		;*****************************************************
		;**** Get new density data from table widget ****
		;*****************************************************

    widget_control, state.densid, get_value=tempval
    tabledata = tempval.value

;**** Copy out density values ****

    dein = dblarr(state.nddmax)
    dhin = dblarr(state.nddmax)
    blanksd = where(strtrim(tabledata(6,*),2) eq '')


;**** next line assumes that all blanks are at the end of the columns ***

    if blanksd(0) ge 0 then maxd=blanksd(0) else maxd=state.nddmax
    if maxd gt 0 then begin
	dein(0:maxd-1) = double(tabledata(6,0:maxd-1))
	dhin(0:maxd-1) = double(tabledata(8,0:maxd-1))
    endif

;**** Fill in the rest with zeroes ****

    if maxd lt state.nddmax then begin
	dein(maxd:state.nddmax-1) = double(0.0)
	dhin(maxd:state.nddmax-1) = double(0.0)
    endif

    state.maxd = maxd
    devals = tabledata(7,*)
    dhvals = tabledata(9,*)

		;**** Get isotope mass values ****

    widget_control, state.hyiso, get_value=get_hyval
    if num_chk(get_hyval(0)) eq 0 then begin
	hyval = float(get_hyval(0))
    endif else begin
	hyval = 0.0
    endelse

    widget_control, state.eliso, get_value=get_elval
    if num_chk(get_elval(0)) eq 0 then begin
	elval = float(get_elval(0))
    endif else begin
	elval = 0.0
    endelse

		;**** get selected spectral line index ****

    if (lsnull eq 0) then begin
	widget_control, state.lineid, get_value=selected
	ispline = selected
    endif else begin
	ispline = 0
    endelse
 
 		;***********************************************
 		;**** write selected values to PS structure ****
 		;***********************************************
 
     ps = { 			 		$
	        new   : 0, 			$
                title : title,          	$
		lsnull: lsnull,			$
		hyval : hyval,			$
		elval : elval,			$
 		ifout : ifout,			$
		ispline:ispline,		$
 		maxt  : maxt,			$
 		maxd  : maxd,			$
 		tihn  : tihn,			$
 		tein  : tein,			$
 		dhin  : dhin,			$
 		dein  : dein,			$
 		tvals : tvals,			$
		devals: devals,			$
		dhvals: dhvals                  $
              }

   
    widget_control, first_child, set_uvalue=state, /no_copy

    RETURN, ps

END

;-----------------------------------------------------------------------------

FUNCTION proc409_event, event


                ;**** Base ID of compound widget ****

    parent=event.handler

		;**********************************************
                ;**** Retrieve the user value state        ****
		;**** Get id of first_child widget because ****
		;**** user value "state" is stored there   ****
		;**********************************************

    first_child = widget_info(parent, /child)
    widget_control, first_child, get_uvalue=state, /no_copy

		;*********************************
		;**** Clear previous messages ****
		;*********************************

    widget_control, state.messid, set_value=' '

                ;************************
                ;**** Process events ****
                ;************************

    CASE event.id OF


		;******************************
		;**** isotope mass numbers ****
		;******************************

	state.hyiso : widget_control, state.eliso, /input_focus
	state.eliso : widget_control, state.hyiso, /input_focus

		;**** Clear table button ****

	state.clearid: begin

                ;**** popup window to confirm ****

	    action= popup(message='Are you sure you want to clear the table?',$
                          buttons=['Confirm','Cancel'], font=state.font,$
                          title='ADAS409 Warning:-')

            if action eq 'Confirm' then begin

                ;*********************************************
                ;   Get current temperature table widget value
                ;*********************************************

                widget_control, state.tempid, get_value=tempval

                ;-----------------------------------------
		;   Zero the relevant columns
		;-----------------------------------------

		empty = strarr(state.ntdmax)
		tempval.value(0,*) = empty
		tempval.value(3,*) = empty
		state.maxt = 0

		;**** Copy new data to table widget ****

 	        widget_control, state.tempid, set_value=tempval

                ;*********************************************
                ;   Get current density table widget value
                ;*********************************************
                widget_control, state.densid, get_value=tempval
                ;-----------------------------------------
		;   Zero the relevant columns
		;-----------------------------------------
		empty = strarr(state.nddmax)
		tempval.value(6,*) = empty
		tempval.value(8,*) = empty
		state.maxd = 0

		;**** Copy new data to table widget ****

 	        widget_control, state.densid, set_value=tempval


	    endif

	end


		;*************************************
		;**** Default Energies buttons    ****
		;*************************************

        state.defttid: begin

 		;**** popup window to confirm overwriting current values ****

            action= popup(message='Confirm Overwrite values with Defaults', $
 		          buttons=['Confirm','Cancel'], font=state.font,$
			  title='ADAS409 Warning:-')

	    if action eq 'Confirm' then begin

		;**** Get current table widget value ****

 	        widget_control, state.tempid, get_value=tempval

		;***********************************************************
		;**** Copy temperaure defaults into value structure    	****
		;**** For default values use the defaults   		****
		;***********************************************************

                units = tempval.units+1

                tempval.value(0,0:state.ntdmax-1) = 			$
 	        strtrim(string(tempval.value(units,0:state.ntdmax-1),	$
 	        format=state.num_form),2)

                tempval.value(3,0:state.ntdmax-1) = 			$
 	        strtrim(string(tempval.value(units+3,0:state.ntdmax-1),	$
 	        format=state.num_form),2)

		;**** Copy new data to table widget ****

 	        widget_control, state.tempid, set_value=tempval
 	    endif

     	end

        state.deftdid: begin

 		;**** popup window to confirm overwriting current values ****

            action= popup(message='Confirm Overwrite values with Defaults', $
 		          buttons=['Confirm','Cancel'], font=state.font,$
			  title='ADAS409 Warning:-')

	    if action eq 'Confirm' then begin

		;**** Get current table widget value ****

 	        widget_control, state.densid, get_value=tempval


		;*******************************************************
		;**** Copy density defaults into value structure    ****
		;**** For default values use the defaults   	    ****
		;*******************************************************

                tempval.value(6,0:state.nddmax-1) =			$
                strtrim(string(tempval.value(7,0:state.nddmax-1),	$ 
		format=state.num_form),2)

                tempval.value(8,0:state.nddmax-1) =			$
                strtrim(string(tempval.value(9,0:state.nddmax-1),	$
		format=state.num_form),2)

		;**** Copy new data to table widget ****

 	        widget_control, state.densid, set_value=tempval

 	    endif

     	end
		;***********************
		;**** Cancel button ****
		;***********************

        state.cancelid: new_event = {ID:parent, TOP:event.top, 		$
				     HANDLER:0L, ACTION:'Cancel'}

		;*********************
		;**** Done button ****
		;*********************

        state.doneid: begin

		;***************************************
		;**** Check all user input is legal ****
		;***************************************

	    error = 0

        	;********************************************
		;*** Have to restore "state" before calls ***
		;*** to proc409_get_val so it can be used ***
		;*** there.				  ***
		;********************************************

	    widget_control, first_child, set_uvalue=state, /no_copy

	    widget_control, event.handler, get_value=ps

		;*** reset state variable ***

	    widget_control, first_child, get_uvalue=state, /no_copy

		;**** check temp values entered ****

            if error eq 0 and (state.maxt eq 0 or state.maxd eq 0) then begin
                error = 1
                message='Error: No temperatures/densities entered.'
            endif


		;**** check isotope values entered ****

	    if error eq 0 then begin
		widget_control, state.hyiso, get_value=hycheck
		if num_chk(hycheck(0), sign=1) eq 1 then begin
		    error=1
		    message='Error: The hydrogen isotope mass number is'+$
		    ' invalid.'
		endif else if float(hycheck(0)) eq 0.0 then begin
		    error=1
		    message='Error: The hydrogen isotope mass number is'+$
		    ' invalid.'
		endif
	    endif

		;**** return value or flag error ****

	    if error eq 0 then begin
                new_event = {ID:parent, TOP:event.top,                  $
                             HANDLER:0L, ACTION:'Done'}
            endif else begin
	        widget_control, state.messid, set_value=message
	        new_event = 0L
            endelse

        end

                ;**** Menu button ****

        state.outid: begin
            new_event = {ID:parent, TOP:event.top, HANDLER:0L,          $
                         ACTION:'Menu'}
        end

        ELSE: new_event = 0L

    ENDCASE

		;*********************************************
		;*** make "state" available to other files ***
		;*********************************************

    widget_control, first_child, set_uvalue=state, /no_copy

    RETURN, new_event

END

;-----------------------------------------------------------------------------

function cw_adas409_proc, topparent, dsfull, act, 			$
	                  symbol, classstring,				$
			  ntdmax, nddmax,				$
			  nline, ciion, cmpts, ctitle, bitfile,		$
		          procval=procval, uvalue=uvalue, 		$
		          font_large=font_large, font_small=font_small,	$
		          edit_fonts=edit_fonts, num_form=num_form

    COMMON cw_proc409_blk, lsnull

		;**** Set defaults for keywords ****

    if not (keyword_set(uvalue)) then uvalue = 0
    if not (keyword_set(font_large)) then font_large = 	''
    if not (keyword_set(font_small)) then font_small = ''
    if not (keyword_set(edit_fonts)) then 				$
    edit_fonts = {font_norm:'',font_input:''}
    if not (keyword_set(num_form)) then num_form = '(E10.3)'

    if not (keyword_set(procval)) then begin
        temp_arr = dblarr(ntdmax)
	dens_arr = dblarr(nddmax)
        xxeiam, symbol, elmass
        ps = { 					$ 
		new   : 0, 			$
		title : '',             	$
		lsnull: 0,			$
		ifout : 1,			$
		ispline:0,			$
		hyval : 1.0,			$
		elval : elmass,			$
                tein  : temp_arr,       	$
                tihn  : temp_arr,       	$
                dein  : dens_arr,       	$
                dhin  : dens_arr,       	$
                maxt  : 0,              	$
                maxd  : 0,              	$
                tvals : dblarr(2,ntdmax), 	$
                devals : dblarr(nddmax),        $
		dhvals : dblarr(nddmax)         $
              }
    endif else begin
        xxeiam, symbol, elmass
	ps = { 					$ 
		new    : procval.new,    	$
                title  : procval.title,  	$
		lsnull : procval.lsnull,	$
 		ifout  : procval.ifout,		$
		ispline:procval.ispline,	$
		hyval  : procval.hyval,		$
		elval  : elmass,       		$
                tein   : procval.tein,   	$
                tihn   : procval.tihn,   	$
                dein   : procval.dein,   	$
                dhin   : procval.dhin,   	$
                maxt   : procval.maxt,   	$
                maxd   : procval.maxd,   	$
                tvals  : procval.tvals,   	$
                devals : procval.devals,        $
                dhvals : procval.dhvals         $
	     }
    endelse
    lsnull = ps.lsnull

;***********************************************************************
;-----------------------------------------------------------------------
;  Assemble temp and density table data     
;-----------------------------------------------------------------------
;  The adas table widget requires data to be input as strings, so all 
;  the numeric data has to be written into a string array.
;  Declare energies  table array               
;     col 1 has user electron temp values                
;     col 2-3 have default electron temp values which can have one of 
;             two possible units
;     col 4 has user hydrogen temp values
;     col 5-6 have default hydrogen temp values which can have one of 
;             two possible units
;     col 7 has user entered electron densities 
;     col 8 has defaults electron densities
;     col 9 has user entered hydrogen densities
;     col 10 has defaults hydrogen densities	
;-----------------------------------------------------------------------
     tabledata = strarr(10, max([ntdmax,nddmax]))
;-----------------------------------------------------------------------
;   Copy out temp and density 
;-----------------------------------------------------------------------
    if (ps.maxt gt 0) then begin
        tabledata(0,0:ps.maxt-1) =					$
 	         strtrim(string(ps.tein(0:ps.maxt-1),format=num_form),2)
        tabledata(3,0:ps.maxt-1) =					$
 	         strtrim(string(ps.tihn(0:ps.maxt-1),format=num_form),2)
    endif
    tabledata(1,0:ntdmax-1) =	 					$
             strtrim(string(ps.tvals(0,0:ntdmax-1),format=num_form),2)
    tabledata(2,0:ntdmax-1) = 						$
             strtrim(string(ps.tvals(1,0:ntdmax-1),format=num_form),2)
    tabledata(4,0:ntdmax-1) =	 					$
             strtrim(string(ps.tvals(0,0:ntdmax-1),format=num_form),2)
    tabledata(5,0:ntdmax-1) = 						$
             strtrim(string(ps.tvals(1,0:ntdmax-1),format=num_form),2)
    if (ps.maxd gt 0) then begin
        tabledata(6,0:ps.maxd-1) =					$
                 strtrim(string(ps.dein(0:ps.maxd-1),format=num_form),2)
        tabledata(8,0:ps.maxd-1) =					$
                 strtrim(string(ps.dhin(0:ps.maxd-1),format=num_form),2)
    endif
    tabledata(7,0:nddmax-1) = 						$
             strtrim(string(ps.devals(0:nddmax-1),format=num_form),2)
    tabledata(9,0:nddmax-1) = 						$
             strtrim(string(ps.dhvals(0:nddmax-1),format=num_form),2)
 
;-----------------------------------------------------------------------
; fill rest of table with blanks
; allow zero values in hydrogen densities
;-----------------------------------------------------------------------

    tabledummy = tabledata(0:5,*)
    blanks = where(tabledummy eq 0.0) 
    if (blanks(0) ne -1) then tabledummy(blanks) = ' ' 
    tabledata(0:5,*) = tabledummy

    tabledummy = tabledata(6:7,*)
    blanks = where(tabledummy eq 0.0) 
    if (blanks(0) ne -1) then tabledummy(blanks) = ' ' 
    tabledata(6:7,*) = tabledummy


;***********************************************************************
;  Creeate the 409 Processing options/input window 
;***********************************************************************

;  create titled base widget

    parent = widget_base(topparent, UVALUE = uvalue, 			$
			 title = 'ADAS409 PROCESSING OPTIONS', 		$
			 EVENT_FUNC = "proc409_event", 			$
			 FUNC_GET_VALUE = "proc409_get_val", 		$
			 PRO_SET_VALUE = "proc409_set_val",		$
			 /COLUMN)

		;******************************************************
		;**** Create a dummy widget just to hold value of *****
		;**** "state" variable so as not to get confused  *****
		;**** with any other values. Adopt IDL practice   *****
		;**** of using first child widget                 *****
		;******************************************************

    first_child = widget_base(parent)

    topbase = widget_base(first_child,/column)

		;***********************
		;**** add run title ****
		;***********************

    base = widget_base(topbase,/row)
    rc = widget_label(base,value='Title for Run',font=font_large)
    runid = widget_text(base,value=ps.title,xsize=38,font=font_large,/edit)

		;**** add dataset name and browse button ****

    rc = cw_adas_dsbr(topbase, dsfull, font=font_large,			$
		      filelabel='Script file: ')

		;********************************
		;**** Base for information   ****
		;********************************

    infomain = widget_base(topbase, /row)
    infobase = widget_base(infomain, /column, /frame)
    infobase2 = widget_base(infomain, /column, /frame)
    infolab  = widget_label(infobase, value='Data file information:-',	$
                            font=font_large)
    eleval =    'Selected master file element: '
    if (strlen(symbol) eq 1) then begin
	eleval = eleval + strupcase(symbol) + '                            '
    endif else begin
	eleval = eleval + strupcase(strmid(symbol, 0,1)) +		$
	strmid(symbol,1,1) + '                           '
    endelse
    dummylab = widget_label(infobase, value='     ', font=font_small)
    elementlab = widget_label(infobase, value=eleval, font=font_small)
    classval =  'Selected master classes: '
    buffer = '                                  '
    buflen = strlen(buffer)
    classlen = strlen(classstring)
    dummy2 = '                                  '
    dummy2 = dummy2 + '                         '
    if classlen le buflen then begin
	strput, buffer, classstring
    endif else begin
	first_part = strmid(classstring, 0, strlen(buffer))
	strput, buffer, first_part
        second_part = strmid(classstring, strlen(buffer),		$
	(strlen(classstring) - strlen(buffer)))
        strput, dummy2, second_part, 24
    endelse
    classval = classval + buffer
    classlab = widget_label(infobase, value=classval, font=font_small)
    classlab2 = widget_label(infobase, value=dummy2, font=font_small)

		;**** Base for isotope mass numbers ****

    isoval = 'Enter isotope information:-'
    isolab = widget_label(infobase2, value=isoval, font=font_large)
    infobase3 = widget_base(infobase2, /row)
    elmislab = widget_label(infobase3, font=font_small,			$
    value='Enter element isotope mass number  (amu) : ')
    elvalstring = string(ps.elval, format='(f5.1)')
    eliso = widget_text(infobase3, value=elvalstring, xsize=6, 		$
                        /editable)
    infobase4 = widget_base(infobase2, /row)
    hydislab = widget_label(infobase4, font=font_small,			$
    value='Enter hydrogen isotope mass number (amu) : ')
    hyvalstring = string(ps.hyval, format='(f5.1)')
    hyiso = widget_text(infobase4, value=hyvalstring, xsize=6, 		$
                        /editable)

 		;************************************************
		;**** base for script file information and   ****
		;**** selection. This will vary depending on ****
		;**** whether a null script has been chosen  ****
		;**** (indicated by the value of ps.lsnull)  ****
 		;************************************************
 
    tablescriptbase = widget_base(topbase, /row)
    tablebase = widget_base(tablescriptbase, /column)
    scriptbase = widget_base(tablebase, /column, /frame)
    scriptheading = widget_label(scriptbase, font=font_large,		$
                    value="Select spectral line for analysis:-")
    scriptbase2 = widget_base(scriptbase, /row)
    if (ps.lsnull eq 1) then begin		;null script file
	scriptlabel=widget_label(scriptbase,				$
		    value='There is no selected script file.')
	lineid = 0
    endif else begin
	no_lines_label='Number of listed lines in script file: '+	$
        strtrim(string(nline),2)

		;**** Build up the strings for selection ****

	linestrings = strarr(4,nline)
	linestrings(0,*) = ' ' + 					$
        strcompress(string(indgen(nline)+1), /remove_all)
        linestrings(1,*) = '      ' + ciion
	linestrings(2,*) = '       ' + cmpts
	linestrings(3,*) = '    ' + ctitle
	scriptlabel=widget_label(scriptbase2, font=font_small,		$
		    value=no_lines_label)

		;**** Put in the selection list ****

	titles = [['LINE','    RADIATING','     NUMBER OF',		$
                   '     TITLE AND'],					$
		  ['INDEX','    ION CHARGE','     COMPONENTS',		$
                   '     WAVELENGTH']]
	select_data = text_table(linestrings, colhead=titles, /noindex)
	coltitles = select_data(0:1)
	select_data = select_data(2:nline+1)
	if ps.ispline gt (nline-1) then ps.ispline = 0
	lineid = cw_single_sel(scriptbase, select_data, value=ps.ispline,  $
	                       title=' ',				   $
			       coltitles=coltitles, ysize=5,		   $
			       font=font_small, big_font=font_large)
    endelse

 		;************************************************
 		;**** base for the table and defaults button ****
 		;************************************************
 
    base = widget_base(tablebase, /column, /frame)
    llabel = widget_label(base,                    			$
             value="Enter Output Temperature - Density data")

		;********************************
		;**** Energy data table      ****
		;********************************

    colhead = [['Electron','Hydrogen'],		$
               ['Output values','Output values']]


;*******************************************************************
;**** Temperature table:         				****
;**** Convert FORTRAN index to IDL index for temperature units	****
;**** Two columns in the table and two set of units		****
;**** Columns are unchnged by the unts used			****
;*******************************************************************

     units_t = ps.ifout-1
     unitname_t = ['eV','Kelvin']
     unitind_t = [[0,0],[3,3]]
     table_title = ["Temperatures"]

;*******************************************************************
;**** Density table:						****
;**** Two columns in the table and a single set of units	****
;*******************************************************************

     units_d = 0
     unitname_d = ['cm-3','cm-3']
     unitind_d = [[6,6],[8,8]]
     table_title_d = ["Densities"]

		;****************************
		;**** table of data   *******
		;****************************
 

    tableid = widget_base(base, /row,/frame)
    tempid = cw_adas_table(tableid, $
                           tabledata, units_t, unitname_t, unitind_t, $
 		           UNITSTITLE = 'Temperature Units', 		$
 		           COLEDIT = [1,1], COLHEAD = colhead, 	$
 		           TITLE = table_title, ORDER = [1,1],		$ 
 		           LIMITS = [1,1], CELLSIZE = 12, 		$
 		           /SCROLL, ytexsize = 6, NUM_FORM = num_form, 	$
 		           FONTS = edit_fonts, FONT_LARGE = font_large,	$
 		           FONT_SMALL = font_small, /ROWSKIP)

; NOTE: THE FOLLOWING GIVES TWO UNITS (CM-3, CM-3) IN THE TABLE
; HOW THIS CAN BE FIXED ??
 
    densid = cw_adas_table(tableid, $
			   tabledata, units_d, unitname_d, unitind_d,$
 		           UNITSTITLE = 'Density Units', 		$
 		           COLEDIT = [1,1], COLHEAD = colhead,		$
 		           TITLE = table_title_d, ORDER = [1,1],		$ 
 		           LIMITS = [1,1], CELLSIZE = 12, 		$
 		           /SCROLL, ytexsize = 6, NUM_FORM = num_form, 	$
 		           FONTS = edit_fonts, FONT_LARGE = font_large,	$
 		           FONT_SMALL = font_small, /ROWSKIP)

		;*************************
		;**** Default buttons ****
		;*************************

    t1base = widget_base(base, /column)
    tbase  = widget_base(t1base, /row)
    defttid = widget_button(tbase, font=font_large,                 	$
                           value='  Default Temperature Values  ')

    deftdid = widget_button(tbase, font=font_large,                 	$
                           value='  Default Density Values  ')

		;**** Clear table button ****

    clearid = widget_button(tbase, font=font_large,			$
			    value='     Clear Table     ')

		;**** Error message ****

    messid = widget_label(parent,font=font_large, 			$
    value='Edit the processing options data and press Done to proceed')

		;**** add the exit buttons ****

    base = widget_base(parent, /row)
    menufile = bitfile + '/menu.bmp'
    read_X11_bitmap, menufile, bitmap1
    outid = widget_button(base, value=bitmap1)		;menu button
    cancelid = widget_button(base, value='Cancel', font=font_large)
    doneid = widget_button(base, value='Done', font=font_large)

		;*************************************************
		;**** create a state structure for the pop-up ****
		;**** 		     window.                  ****
		;*************************************************

    new_state = {					$
		  runid		:	runid,  	$
		  messid	:	messid, 	$
 		  defttid	:	defttid,	$
 		  deftdid	:	deftdid,	$
		  clearid	:	clearid,	$
 		  tempid	:	tempid, 	$
		  densid	:	densid,		$
		  cancelid	:	cancelid,	$
		  doneid	:	doneid, 	$
		  outid		:	outid,		$
		  lineid	:	lineid,		$
		  dsfull	:	dsfull,		$
		  font		:	font_large,	$
                  tvals		:	ps.tvals,       $
		  devals	:	ps.devals,	$
		  dhvals	:	ps.devals,	$
		  hyiso		:	hyiso,		$
		  eliso		:	eliso,		$
                  maxt		:	ps.maxt,        $
                  maxd		:	ps.maxd,        $
                  ntdmax	:	ntdmax,         $
		  nddmax        :       nddmax,         $
                  tablebase	:	tablebase,      $
		  num_form	:	num_form 	$
	      }

                 ;**** Save initial state structure ****

    widget_control, first_child, set_uvalue=new_state, /no_copy

    RETURN, parent

END

