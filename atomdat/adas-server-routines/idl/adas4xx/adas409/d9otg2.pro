; Copyright (c) 1997, Strathclyde University.
; SCCS info: Module @(#)$Header: /home/adascvs/idl/adas4xx/adas409/d9otg2.pro,v 1.1 2004/07/06 13:26:22 whitefor Exp $ Date $Date: 2004/07/06 13:26:22 $
;+
; PROJECT:
;       ADAS IBM MVS to UNIX conversion
;
; NAME:
;       D9OTG2
;
; PURPOSE:
;       Communication with ADAS409 FORTRAN via UNIX pipe and
;       graphics output.
;
; EXPLANATION:
;       The routine begins by reading information from the ADAS409
;	FORTRAN routine D9OTG2.FOR via a UNIX pipe.  Then the IDL graphical
;       output routine for ADAS409 power function plots is invoked.
;
; USE:
;       This routine is specific to adas409.
;
; INPUTS:
;       DSFULL   - Script file name
;
;       PIPE     - The IDL unit number of the bi-directional pipe to the
;                  ADAS409 FORTRAN process.
;
;       UTITLE   - Optional comment by user
;
;       GRPSCAL  - Integer; 0 if default scaling required 1 if user
;                  specified scaling to be used.
;
;       XMIN     - String; User specified x-axis minimum, number as string.
;
;       XMAX     - String; User specified x-axis maximum, number as string.
;
;       ZMIN     - String; User specified z-axis minimum, number as string.
;
;       ZMAX     - String; User specified z-axis maximum, number as string.
;
;       HRDOUT   - Integer; 0 if no hardcopy required, 1 if hardcopy.
;
;       HARDNAME - String; file name for hardcopy output.
;
;       DEVICE   - String; IDL name for hardcopy output device.
;
;	YEAR	 - String; user's selected year for data.
;
;	DYEAR	 - String; user's selected default year for data (if any).
;
;       HEADER   - ADAS version header information for inclusion in the
;                  graphical output.
;
;       BITFILE - String; the path to the directory containing bitmaps
;                 for the 'escape to series menu' button.
;
;       WIDGETBASE - Long integer; widget id of the information base
;                    generated whilst pipe comms are in progress.
;
; OPTIONAL INPUTS:
;       None.
;
; OUTPUTS:
;       GOMENU  - Int; flag - set to 1 if user has selected 'escape direct
;                 to series menu', otherwise 0
;
; OPTIONAL OUTPUTS:
;       None.
;
; KEYWORD PARAMETERS:
;       FONT    - String; Name of the font to be used for the graphical
;                 output widget.
;
; CALLS:
;       ADAS409_2_PLOT	ADAS409 power function graphical output.
;	XXELEM	        Converts no. of electrons to element name
;
; SIDE EFFECTS:
;       This routine reads information from FORTRAN via a UNIX pipe.
;
; CATEGORY:
;       Adas system.
;
; WRITTEN:
;       Alessandro Lanzafame, University of Strathclyde, 12 December 1995
;
; MODIFIED:
;		1.1	Richard Martin
;			Put under SCCS control.
;
; VERSION:
;		1.1	11-03-98
;
;-
;-----------------------------------------------------------------------------

PRO d9otg2, dsfull, pipe, utitle, grpscal, xmin, xmax, zmin, zmax,	$
            hrdout, hardname, device, year, dyear, header, bitfile,	$
	    gomenu, widgetbase, FONT=font

                ;**** Set defaults for keywords ****

    IF NOT (KEYWORD_SET(font)) THEN font = ''

                ;**** Declare variables for input ****

    input = 0
    fdum = 0.0
    sdum = ' '
                                                        
                ;********************************
                ;**** Read data from fortran ****
                ;********************************

    readf, pipe, input
    itmax = input	
    readf, pipe, input
    idmax = input	
				;no. of electron temps.

    xvals = fltarr(itmax)	;.......electron temperature

    for i=0, (itmax-1) do begin
	readf, pipe, fdum
 	xvals(i) = fdum
    endfor

    yvals=fltarr(idmax)		;.......electron density
    for i=0, (idmax-1) do begin	
	readf, pipe, fdum
 	yvals(i) = fdum
    endfor

    readf, pipe, input
    nmsum = input
    pltp = fltarr(itmax,idmax,nmsum)	;metastable fractional line power
    for i=0, (itmax-1) do begin
        for idens=0, (idmax-1) do begin
           for j=0, (nmsum-1) do begin
              readf, pipe, fdum
              pltp(i,idens,j) = fdum
           endfor
        endfor
    endfor

    poptit = strarr(nmsum)	;......designation strings
    for i=0, (nmsum-1) do begin
        readf, pipe, format='(a10)', sdum
        poptit(i) = sdum
    endfor

    readf, pipe, input
    xxelem, input, output	
    species = output		;.......element name for title

    prb=fltarr(itmax,idmax)	;.......recombination-brems.-cascade power
    for it=0, (itmax-1) do begin
       for id=0, (idmax-1) do begin
          readf, pipe, fdum
          prb(it,id)=fdum
       endfor
    endfor

    prc=fltarr(itmax,idmax)	;.......charge exchange rad. recomb. power
    for it=0, (itmax-1) do begin
       for id=0, (idmax-1) do begin
          readf, pipe, fdum
          prc(it,id)=fdum
       endfor
    endfor

    plt=fltarr(itmax,idmax)	;.......line power
    for it=0, (itmax-1) do begin
       for id=0, (idmax-1) do begin
          readf, pipe, fdum
          plt(it,id)=fdum
       endfor
    endfor

    prada=fltarr(itmax,idmax)	;.......radiated power
    for it=0, (itmax-1) do begin
       for id=0, (idmax-1) do begin
          readf, pipe, fdum
          prada(it,id)=fdum
       endfor
    endfor
                                                      
                ;**** Destroy information widget ****

    wait, 1
    widget_control, widgetbase, /destroy

                ;***********************
                ;**** Plot the data ****
                ;***********************

    tot=prada
    densel=0           
    adas409_2_plot, dsfull, utitle, grpscal, nmsum, xvals, yvals,		$
                    pltp, prb, prc, plt, tot,					$
                    hrdout, hardname, device, header, poptit, 		$
		    	  species, year, dyear, xmin, xmax, zmin, zmax,		$
                    densel, bitfile, gomenu, FONT=font

    printf,pipe,densel+1                                                   

END
