; Copyright (c) 1997, Strathclyde University.
; SCCS info: Module @(#)$Header: /home/adascvs/idl/adas4xx/adas409/d9otg1.pro,v 1.1 2004/07/06 13:26:16 whitefor Exp $ Date $Date: 2004/07/06 13:26:16 $
;+
; PROJECT:
;       ADAS Programs
;
; NAME:
;       D9OTG1
;
; PURPOSE:
;       Communication with ADAS409 FORTRAN via UNIX pipe and
;       graphics output.
;
; EXPLANATION:
;       The routine begins by reading information from the ADAS409
;	FORTRAN routine D9OTG1.FOR via a UNIX pipe.  Then the IDL graphical
;       output routine for ADAS409 ion fraction plots is invoked.
;
; USE:
;       This routine is specific to adas409.
;
; INPUTS:
;       DSFULL   - Script file name
;
;       PIPE     - The IDL unit number of the bi-directional pipe to the
;                  ADAS409 FORTRAN process.
;
;       UTITLE   - Optional comment by user
;
;       GRPSCAL  - Integer; 0 if default scaling required 1 if user
;                  specified scaling to be used.
;
;       XMIN     - String; User specified x-axis minimum, number as string.
;
;       XMAX     - String; User specified x-axis maximum, number as string.
;
;       ZMIN     - String; User specified z-axis minimum, number as string.
;
;       ZMAX     - String; User specified z-axis maximum, number as string.
;
;       HRDOUT   - Integer; 0 if no hardcopy required, 1 if hardcopy.
;
;       HARDNAME - String; file name for hardcopy output.
;
;       DEVICE   - String; IDL name for hardcopy output device.
;
;	YEAR	 - String; user's selected year for data.
;
;	DYEAR	 - String; user's selected default year for data (if any).
;
;       HEADER   - ADAS version header information for inclusion in the
;                  graphical output.
;
;       BITFILE - String; the path to the directory containing bitmaps
;                 for the 'escape to series menu' button.
;
;	WIDGETBASE - Long integer; widget id of the information base
;		     generated whilst pipe comms are in progress.
;
; OPTIONAL INPUTS:
;       None.
;
; OUTPUTS:
;       GOMENU  - Int; flag - set to 1 if user has selected 'escape direct
;                 to series menu', otherwise 0
;
; OPTIONAL OUTPUTS:
;       None.
;
; KEYWORD PARAMETERS:
;       FONT    - String; Name of the font to be used for the graphical
;                 output widget.
;
; CALLS:
;       ADAS409_1_PLOT	ADAS409 ion fraction graphical output.
;
; SIDE EFFECTS:
;       This routine reads information from FORTRAN via a UNIX pipe.
;
; CATEGORY:
;       Adas system.
;
; WRITTEN:
;       Alessandro Lanzafame, University of Strathclyde, 12 December 1995
;
; MODIFIED:
;	1.1	Richard Martin
;		Put under SCCS control.
;
; VERSION:
;	1.1	11-03-98
;
;-
;-----------------------------------------------------------------------------


PRO d9otg1, dsfull, pipe, utitle, grpscal, xmin, xmax, zmin, zmax,	$
            hrdout, hardname, device, year, dyear, header, bitfile,	$
            gomenu, widgetbase, FONT=font

                ;**** Set defaults for keywords ****

    IF NOT (KEYWORD_SET(font)) THEN font = ''

                ;**** Declare variables for input ****

    input = 0
    fdum = 0.0
    sdum = ' '
                                                        
                ;********************************
                ;**** Read data from fortran ****
                ;********************************

    readf, pipe, input
    itmax = input
    xvals = fltarr(itmax)	;...............electron temperature
    readf, pipe, input
    idmax=input

    yvals =fltarr(idmax)	;...............electron density
    for i=0, (itmax-1) do begin
	readf, pipe, fdum
 	xvals(i) = fdum
    endfor

    for i=0, (idmax-1) do begin
	readf, pipe, fdum
        yvals(i)=fdum
    endfor
    readf, pipe, input
    nmsum = input

    zvals = fltarr(itmax, idmax, nmsum)	;........metastable fraction
    poptit = strarr(nmsum)		;........designation strings
    for i=0, (itmax-1) do begin
        for id=0, (idmax-1) do begin
	    for j=0, (nmsum-1) do begin
	        readf, pipe, fdum
                zvals(i,id,j) = fdum
	    endfor
	endfor
    endfor
 
    for i=0, (nmsum-1) do begin
	readf, pipe, format='(a10)', sdum
        poptit(i) = sdum
    endfor
    readf, pipe, input
    xxelem, input, output	
    species = output				;element name for title
                                                      
		;**** Destroy information widget ****

    wait, 1
    widget_control, widgetbase, /destroy


                ;***********************
                ;**** Plot the data ****
                ;***********************
    densel=0
    adas409_1_plot, dsfull, utitle, grpscal, nmsum, xvals, yvals, zvals,$
                    hrdout, hardname, device, header, poptit, 		$
		    species, year, dyear, xmin, xmax, zmin, zmax, densel,	$
                    bitfile, gomenu, FONT=font
                    
    printf,pipe,densel+1

END
