;----------------------------------------------------------------------
;+
; PROJECT    :  ADAS
;
; NAME       :  adas409_generate_gtn
;
; PURPOSE    :  Given an input adf15 file and adf11 data generate a
;               generalised contribution file. The routine determines
;               whether the PEC file is metastable unresolved or
;               resolved and calls the appropriate adf11 datasets.
;
; ARGUMENTS  :  All output arguments will be defined appropriately.
;
;               NAME      I/O    TYPE    DETAILS
; REQUIRED   :  adf15      I     char    adf15 input file
;               adf16      I     char    adf16 output file
; OPTIONAL   :  uid        I     char    user identifer for ion balance
;                                        default is 'adas'
;               year       I     int     year for ion balance
;                                        default is 89
;               num_te     I     int     number of temperatures
;                                        default is 100
;               thresh     I     float   threshold for fractional abundance
;                                        used for gtn. default is 1.0e-3
;               te         I     float   Te grid for gtn file. If not set
;                                        num_te temperatures for driving
;                                        populations above thresh are used.
;               dens       I     float   Ne grid for gtn file. If not set
;                                        the densities in the adf15 files are
;                                        used.
;
; KEYWORDS      norecomb           -     Switch off recombination contribution.
;               kelvin             -     Input temperatures are in Kelvin.
;               help               -     If specified this comment
;                                        section is written to screen.
;
; NOTES      :
;
;
; EXAMPLE    :
;
; AUTHOR     :  Martin O'Mullane
; DATE       :  12-04-2005
;
;
; MODIFIED:
;       1.1     Martin O'Mullane
;               - First version.
;       1.2     Martin O'Mullane
;               - Recombination stages of iz1=10 caused a failure
;                 because of incorrect format statement within code.
;       1.3     Martin O'Mullane
;               - Bring into line with structure returned from current
;                 version of xxdata_15 (via read_adf15).
;
; VERSION:
;       1.1    21-04-2005
;       1.2    14-10-2005
;       1.3    19-07-2008
;-
;----------------------------------------------------------------------



PRO adas409_gtn_write, lun, te, dens, gtn, wlngth, ion_str, isel

ndens = n_elements(dens)
nte   = n_elements(te)

str = string(wlngth, format='(f8.1)') + ' A' +          $
      string(ndens, format='(i4)') +                    $
      string(nte, format='(i4)') + ' /' +               $
      strupcase(ion_str) +                              $
      '/-------------------------------------/ISEL =' + $
      string(isel+1, format='(i5)')

printf, lun, str

fmt = '(8(E9.2,:))'
printf, lun, dens, format=fmt
printf, lun, te, format=fmt

for j = 0, ndens-1 do printf, lun, double(gtn[j,*]) > 1.0D-70, format=fmt

END
;----------------------------------------------------------------------



PRO adas409_gtn_limits, te, fraction, value, x1, x2,   $
                        stripped = stripped,           $
                        neutral  = neutral

res = size(fraction, /dimensions)

n_dens  = res[0]
n_te    = res[1]
n_stage = res[2]

x1_tmp = [0.0]
x2_tmp = [0.0]

for id = 0, n_dens-1 do begin

   for is = 0, n_stage-1 do begin

      y = fraction[id, *, is]

      is_st = stripped EQ 1 AND is EQ n_stage-1
      is_nt = neutral  EQ 1 AND is EQ 0

      case 1 of

        is_nt : begin
                   x1_tmp = [x1_tmp, te[0] ]
                end

        is_st : begin
                   p1_y = y
                   p2_y = fraction[id, *, is-1]

                   ind = where(p1_y GT p2_y, count)
                   if count GT 0 then x2_tmp = [x2_tmp, 5.0*te[ind[0]] ]
                end

        else  : begin
                   res  = max(y, ind)
                   p1_x = te[0:ind]
                   p2_x = reverse(te[ind:*])

                   p1_y = y[0:ind]
                   p2_y = reverse(y[ind:*])

                   t1 = interpol(p1_x, p1_y, value)
                   t2 = interpol(p2_x, p2_y, value)

                   x1_tmp = [x1_tmp, t1]
                   x2_tmp = [x2_tmp, t2]

                end

      endcase

   endfor

endfor

x1 = min(x1_tmp[1:*])
x2 = max(x2_tmp[1:*])

END
;-------------------------------------------------------------------------


PRO adas409_generate_gtn,  adf15     =  adf15,        $
                           uid       =  uid,          $
                           year      =  year,         $
                           num_te    =  num_te,       $
                           thresh    =  thresh,       $
                           te        =  te,           $
                           dens      =  dens,         $
                           adf16     =  adf16,        $
                           kelvin    =  kelvin,       $
                           norecom   =  norecom,      $
                           help      =  help


; If asked for help

if keyword_set(help) then begin
   doc_library, 'adas409_generate_gtn'
   return
endif

; Timestamp info

date = xxdate()
name = xxuser()

; Check that we get all inputs and that they are correct.

if n_elements(adf15) eq 0 then message, 'An adf15 name must be passed'
file_acc, adf15, exist, read, write, execute, filetype
if exist ne 1 then message, 'adf15 does not exist ' + adf15
if read ne 1 then message, 'adf15 cannot be read from this userid ' + adf15


if n_elements(thresh) EQ 0 then min_frac = 1.0e-3 else min_frac = double(thresh)
if n_elements(num_te) EQ 0 then nte = 100 else nte = long(num_te)


if n_elements(uid) EQ 0 then begin
   message, 'The adas uid for the ionisation balance is used', /continue
   uid = 'adas'
endif
if n_elements(year) EQ 0 then begin
    message, 'the 89 default year for the ionisation balance is assumed', /continue
    year = 89
endif





; Default output filename

if n_elements(adf16) EQ 0 then begin
   message, 'Output file sent to gtn.pass', /continue
   file_out = 'gtn.pass'
endif else file_out = adf16


; Are the temperature and density set by user

if n_elements(te) GT 0 then te_set = 1 else te_set = 0
if n_elements(dens) GT 0 then dens_set = 1 else dens_set = 0

if te_set EQ 1 AND keyword_set(kelvin) then te = te / 11605.0

; Check whether adf15 file is metastable unresolved or resolved.

read_adf15, file=adf15, fulldata=all_15
n_pec = n_elements(all_15.nte)
elem  = strlowcase(strcompress(all_15.esym, /remove_all))

num_u = -1
num_r = -1
res   = where(strpos(strupcase(all_15.cindm), 'T ') NE -1, num_u)
res   = where(stregex(all_15.cindm,'[1234] ') EQ 0, num_r)

is_partial = -1
if num_u GT 0 then is_partial = 0
if num_r GT 0 then is_partial = 1

if is_partial EQ -1 then message, 'Is this a correctly formed adf15 file'



; Extract indices of excit and recom coefficients

res_exc = where(strpos(strupcase(all_15.ctype), 'EXCIT') NE -1, n_excit)
res_rec = where(strpos(strupcase(all_15.ctype), 'RECOM') NE -1, n_recom)

if is_partial EQ 1 then begin

   par_exc = long(all_15.cindm[res_exc])
   par_rec = long(all_15.cindm[res_rec])
   res     = where(stregex(all_15.cindm,'1 ') EQ 0, n_excit)

endif

; Elements for a spectroscopic line
;   - max 4 metastables
;   - identify block numbers
;   - index by first metastable
;   - ignore CX contribution

ind_exc = intarr(n_excit, 4)
ind_rec = intarr(n_excit, 4)
met_exc = intarr(n_excit, 4)
met_rec = intarr(n_excit, 4)

if is_partial EQ 1 then begin

   for j = 1, max(par_exc) do begin
      str = string(j, format='(i1)') + ' '
      res = where(stregex(all_15.cindm[res_exc], str) EQ 0, count)
      if count GT 0 then begin
         ind_exc[*,j-1] = res + 1
         met_exc[*,j-1] = long(all_15.cindm[res])
      endif
   endfor
   nmet_exc = max(par_exc)

   ; escape + for regex
   ; prepend with res_exc to get block numbers correct
   ; exclude CHEXC

   for j = 1, max(par_rec) do begin
      str = '\+' + string(j, format='(i1)')
      res = where(stregex(all_15.cindm[[res_exc,res_rec]], str) EQ 0, count)
      if count GT 0 then begin
         ind_rec[*,j-1] = res + 1
         met_rec[*,j-1] = long(all_15.cindm[res])
      endif
   endfor
   nmet_rec = max(par_rec)

endif else begin

   ind_exc[*,0] = res_exc + 1
   met_exc[*,0] = lonarr(n_excit) + 1
   nmet_exc     = 1

   ind_rec[*,0] = res_rec + 1
   met_rec[*,0] = lonarr(n_excit) + 1
   nmet_rec     = 1

endelse



; Get the ionisation balance
;  - if Te given use that without worrying
;  - if not use Te/Ne as given in adf15 file, then refine to range of interest


if te_set EQ 1 then begin

   if dens_set EQ 0 then message, 'Set density also if Te is specified'

   run_adas405, uid=uid, year=year, elem=elem, te=te, dens=dens, $
                frac=frac, partial=is_partial, /all

   stage = frac.stage

endif else begin

   ; get ion balance for first block (excitation only).

   te_in   = reform(all_15.te[*,0])
   dens_in = reform(all_15.dens[*,0])
   n_te    = max(all_15.nte)
   n_dens  = max(all_15.ndens)

   run_adas405, uid=uid, year=year, elem=elem, te=te_in, dens=dens_in, $
                frac=frac, partial=is_partial, /all

   stage    = frac.stage
   n_stage  = n_elements(stage)
   fraction = fltarr(n_dens, n_te, n_stage, n_excit)

   fraction[*,*,*,0] = frac.ion

   block_uniq = 0

   ; now for subsequent blocks - check if te is same as block 1 before
   ; re-running 405

   for j = 1, n_excit-1 do begin

     te_ind   = reform(all_15.te[*,j])
     dens_ind = reform(all_15.dens[*,j])

     res = total(te_ind - te_in) + total(dens_ind - dens_in)

     if res NE 0.0 then begin

        run_adas405, uid=uid, year=year, elem=elem, te=te_ind, dens=dens_ind, $
                     frac=frac, partial=is_partial, /all
        block_uniq = [block_uniq, 1]
     endif else block_uniq = [block_uniq, 0]
     fraction[*,*,*,j] = frac.ion

   endfor

endelse



; Identify driving stage and the one above for recombination

str_exc = '  +  '
strput, str_exc, elem, 0
iz = all_15.is
if iz LT 10 then strput, str_exc, string(iz, format='(i1)'), 4  $
            else strput, str_exc, string(iz, format='(i2)'), 3
str_rec = '  +  '
strput, str_rec, elem, 0
iz1 = all_15.is+1
if iz1 LT 10 then strput, str_rec, string(iz1, format='(i1)'), 4  $
             else strput, str_rec, string(iz1, format='(i2)'), 3

drv_exc   = where(strpos(stage, str_exc) NE -1, num_exc)
drv_rec   = where(strpos(stage, str_rec) NE -1, num_rec)
drv_stage = [drv_exc, drv_rec]



; Refine the Te mesh when Te is not given

if n_elements(dens) GT 0 then ndens = n_elements(dens) else ndens = n_dens

te_new = fltarr(nte, n_excit)

if te_set EQ 0 then begin

   res = where(drv_stage EQ n_stage-1, val)
   if val EQ 0 then stripped = 0 else stripped = 1

   res = where(drv_stage EQ 0, val)
   if val EQ 0 then neutral = 0 else neutral = 1

   for j = 0, n_excit-1 do begin

      tmp = min(reform(all_15.dens[*,j]), i1)
      tmp = max(reform(all_15.dens[*,j]), i2)
      ind_dens = [i1, i2]
      f_test   = reform(fraction[ind_dens, *, drv_stage, j])

      adas409_gtn_limits, te_in, f_test, min_frac, x1, x2,   $
                          stripped = stripped,               $
                          neutral  = neutral

      te_new[*, j] = adas_vector(low=x1, high=x2, num=nte)

   endfor

endif else begin

   nte    = n_elements(te)
   te_new = rebin(te, nte, n_excit)

endelse



; Now calculate the GTN functions and write to output file

openw, lun, file_out, /get_lun

str = string(n_excit, format='(i5)') + $
      '    /' + strupcase(str_exc) + ' Generalised contribution functions/'
printf, lun, str


te_in = reform(te_new[*,0])
if n_elements(dens) GT 0 then dens_in = dens $
                         else dens_in = reform(all_15.dens[*,0])

te_b   = te_in
dens_b = dens_in

run_adas405, uid=uid, year=year, elem=elem, te=te_in, dens=dens_in, $
             frac=frac, partial=is_partial, /all


; Calculate and write out gtn for this transition

wave = fltarr(n_excit)

for isel = 0, n_excit-1 do begin

   te_in = reform(te_new[*,isel])
   if n_elements(dens) GT 0 then dens_in = dens $
                            else dens_in = reform(all_15.dens[*,isel])

   if isel GT 0 then begin
      res = total(te_in - te_b) + total(dens_in - dens_b)
      if res NE 0.0 then begin

         run_adas405, uid=uid, year=year, elem=elem, te=te_in, dens=dens_in, $
                      frac=frac, partial=is_partial, /all
      endif
      te_b   = te_in
      dens_b = dens_in
   endif

   pop   = frac.ion[*,*,drv_stage]
   emiss = fltarr(n_elements(dens_in), n_elements(te_in), n_elements(drv_stage))

   k = 0
   for j = 0, nmet_exc-1 do begin
      read_adf15, file=adf15, block=ind_exc[isel,j], te=te_in, $
                  dens=dens_in, data=data, wlngth=wlngth, /all
      emiss[*,*,k] = transpose(data)
      k = k + 1
   endfor
   for j = 0, nmet_rec-1 do begin
      if ind_rec[isel,j] GT 0 then begin
         read_adf15, file=adf15, block=ind_rec[isel,j], te=te_in, $
                     dens=dens_in, data=data, /all
         emiss[*,*,k] = transpose(data)
      endif else emiss[*,*,k] = 0.0
      if keyword_set(norecom) then emiss[*,*,k] = 0.0
      k = k + 1
   endfor

   wave[isel] = wlngth

   gtn = total(pop*emiss, 3, /double)

   adas409_gtn_write, lun, te_in, dens_in, gtn, wlngth, str_exc, isel

endfor


; Finish with comment section

printf, lun, format='("C",79("-"))'
printf, lun, 'C'
printf, lun, 'C  Generalised contribution function from :'
printf, lun, 'C'
printf, lun, 'C  adf15 file : ' + adf15
printf, lun, 'C'
printf, lun, 'C  Using ionisation balance data from :'
printf, lun, 'C'
printf, lun, 'C  User : ' + uid
printf, lun, 'C  Year : ' + year
printf, lun, 'C'

printf, lun, 'C                  EXCIT adf15 index       RECOM adf15 index'
printf, lun, 'C   ISEL     WAVE  met#1   #2   #3   #4    par#1   #2   #3   #4'
printf, lun, 'C   ----     ----  --------------------    --------------------'

for isel = 0, n_excit-1 do begin

  out_str = 'C                '
  strput, out_str, string(isel+1, format='(i4)'), 3
  strput, out_str, string(wave[isel], format='(f8.1)'), 8

  s_exc = '    -    -    -    -'
  k = 0
  for j = 0, nmet_exc-1 do begin
     if ind_exc[isel,j] GT 0 then begin
        strput, s_exc, string(ind_exc[isel,j], format='(i5)'), k
        k = k + 5
     endif
  endfor

  s_rec = '    -    -    -    -'
  k = 0
  for j = 0, nmet_rec-1 do  begin
     if ind_rec[isel,j] GT 0 then begin
        strput, s_rec, string(ind_rec[isel,j], format='(i5)'), k
        k = k + 5
     endif
  endfor

  printf, lun, out_str + s_exc + '    ' + s_rec

endfor

printf, lun, 'C'

printf,lun, 'C  Code     : adas409_generate_gtn'
printf,lun, 'C  Producer : ', name[0]
printf,lun, 'C  Date     : ', date[0]
printf,lun, 'C'
printf, lun, format='("C",79("-"))'


free_lun, lun

; Restore te to Kelvin

if te_set EQ 1 AND keyword_set(kelvin) then te = te * 11605.0

END
