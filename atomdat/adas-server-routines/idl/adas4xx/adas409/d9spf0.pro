; Copyright (c) 1997 Strathclyde University.
; SCCS info: Module @(#)$Header: /home/adascvs/idl/adas4xx/adas409/d9spf0.pro,v 1.2 2004/07/06 13:26:52 whitefor Exp $ Date $Date: 2004/07/06 13:26:52 $
;+
; PROJECT:
;       ADAS Programs
;
; NAME:
;       D9SPF0
;
; PURPOSE:
;       IDL user interface and communications with ADAS409 FORTRAN
;       process via pipe.
;
; EXPLANATION:
;       Firstly this routine invokes the part of the user interface
;       used to select the inputs for ADAS409. When the user's
;       interactions are complete this routine communicates with the
;       ADAS409 FORTRAN application via a UNIX pipe.  Communications
;       are to the FORTRAN subroutine D9SPF0.
;
; USE:
;       The use of this routine is specific to ADAS409, see adas409.pro.
;
; INPUTS:
;       PIPE    - The IDL unit number of the bi-directional pipe to the
;                 ADAS409 FORTRAN process.
;
;       INVAL	- A structure which determines the initial settings of
;                 the input screen widgets.
;                 INVAL is passed un-modified through to cw_adas409_in.pro,
;                 see that routine for a full description.
;
;       CLASSES - String array: the names of the isonuclear master classes
;
; OPTIONAL INPUTS:
;       None
;
; OUTPUTS:
;       INVAL	- On output the structure records the final settings of
;                 the input screen widgets if the user pressed the
;                 'Done' button, otherwise it is not changed from input.
;
;       REP     - String; Indicates whether the user pressed the 'Done'
;                 or 'Cancel' button on the interface.  The action is
;                 converted to the strings 'NO' and 'YES' respectively
;                 to match up with the existing FORTRAN code.  In the
;                 original IBM ISPF interface REP was the reply to the
;                 question 'DO YOU WANT TO END PROGRAM EXECUTION'.
;
;	LSNULL	- Integer; a flag indicating whether a null script file 
;		  has been used (=1) or not (=0)
;
; OPTIONAL OUTPUTS:
;       None
;
; KEYWORD PARAMETERS:
;       FONT_LARGE	- Supplies the large font to be used for the 
;			  interface widgets.
;
;	FONT_SMALL	- Supplies the small font to be used for the 
;			  interface widgets.
; CALLS:
;       ADAS405_IN   	- Pops-up the input selections widget.
;
; SIDE EFFECTS:
;       This routine communicates with the ADAS409 FORTRAN process
;       via a UNIX pipe.
;
; CATEGORY:
;       Adas system.
;
; WRITTEN:
;       Alessandro Lanzafame, University of Strathclyde, 07-12-95
;		First version: adapted from d5spf0
;
; MODIFIED:
;	1.1	Richard Martin
;		Put under SCCS control.
;	1.2	Richard Martin
;		Changed call to 'adas409_in' to 'adas405_in'.
;
; VERSION:
;       1.1	11-03-98
;	1.2 22-06-98
;
;-----------------------------------------------------------------------------
;-

PRO d9spf0, pipe, inval, classes, rep, lsnull,				$
            FONT_LARGE=font_large, FONT_SMALL=font_small


                ;**** Set defaults for keywords ****

    IF NOT (KEYWORD_SET(font_large)) THEN font_large = ''
    IF NOT (KEYWORD_SET(font_small)) THEN font_small = ''

                ;**********************************
                ;**** Pop-up input file widget ****
                ;**********************************

    adas405_in, inval, classes, action, WINTITLE = 'ADAS 409 INPUT',	$
		FONT_LARGE=font_large, FONT_SMALL=font_small

                ;********************************************
                ;**** Act on the event from the widget   ****
                ;********************************************
                ;**** There are only two possible events ****
                ;**** 'Done' and 'Cancel'.               ****
                ;********************************************

    if action eq 'Done' then begin
        rep = 'NO'
    endif else begin
        rep = 'YES'
    endelse
    printf, pipe, rep

    if rep eq 'NO' then begin

                ;*******************************
                ;**** Write data to fortran ****
                ;*******************************

	printf, pipe, inval.year
	printf, pipe, inval.dyear
	printf, pipe, strupcase(inval.symbol)

		;**** Write script file name ****

	scriptname = inval.scrootpath + inval.scfile
        printf, pipe, scriptname
	
		;**** Write filter name ****

	printf, pipe, strcompress(inval.filtr, /remove_all)

		;**** Write filenames ****

		;**** First construct data file names ****

	filenames = strarr(8)
	defnames = strarr(8)
	dirnames = ['acd','scd','ccd','prb','prc','qcd','xcd','plt']
	if inval.branch eq 0 then begin
	    filenames(*) = inval.userroot 
	endif else begin
	    filenames(*) = inval.centroot
	endelse
	defnames(*) = inval.centroot 
	defdirnames = dirnames + strcompress(inval.dyear, /remove_all)
	dirnames = dirnames + strcompress(inval.year, /remove_all)
	if (inval.filetype eq 1) then begin	;partial files
	    if (inval.parttype eq 0) then begin
		dirnames = dirnames + 'r'
		defdirnames = defdirnames + 'r'
	    endif else begin
		dirnames = dirnames + 'u'
		defdirnames = defdirnames + 'u'
	    endelse
	endif
	if (strcompress(inval.filtr, /remove_all) ne '') then begin
	    dirnames = dirnames + '(' + 				$
            strcompress(inval.filtr, /remove_all) + ')'
	    defdirnames = defdirnames + '(' + 				$
            strcompress(inval.filtr, /remove_all) + ')'
	endif
        filenames = filenames + dirnames  + "/"
	defnames = defnames + defdirnames + "/"
	if (strcompress(inval.member, /remove_all) ne '') then begin
	    filenames = filenames + "(" +				$
	    strcompress(inval.member, /remove_all) + ")"
	    defnames = defnames + "(" +					$
	    strcompress(inval.member, /remove_all) + ")"
	endif
        filenames = filenames + dirnames + "_" + 			$
        strcompress(inval.symbol, /remove_all) + ".dat"
	defnames = defnames + defdirnames + "_" + 			$
        strcompress(inval.symbol, /remove_all) + ".dat"

		;********************************************************
		;**** Now overwrite user filenames which are missing ****
		;**** with default filenames (whether they exist or  ****
		;**** not)                                           ****
		;********************************************************

	wherezero = where(inval.fileavailable eq 0)
	if (wherezero(0) ne -1) then begin
	    filenames(wherezero) = defnames(wherezero)
	endif

		;**** Write filenames to FORTRAN ****

	for i=0,7 do begin
	    printf, pipe, filenames(i)
	endfor

		;**** Write file availability flags ****

	for i=0,7 do begin
            printf, pipe, inval.fileavailable(i)
	endfor

		;**** Write default file availability flags ****

	for i=0,7 do begin
            printf, pipe, inval.defavailable(i)
        endfor

		;**** Write file selection flags ****

        for i=0,7 do begin
            printf, pipe, inval.indices(i)
        endfor

		;**** Write whether partial or standard ****

	printf, pipe, inval.filetype

		;**** Read in whether a null script file or not ****

        idum = 0
	readf, pipe, idum
	lsnull = idum

    endif


END
