; Copyright (c) 1997, Strathclyde University .
; SCCS info: Module @(#)$Header: /home/adascvs/idl/adas4xx/adas409/cw_adas409_graph.pro,v 1.2 2004/07/06 12:48:06 whitefor Exp $ Date $Date: 2004/07/06 12:48:06 $
;
;+
; PROJECT:
;       ADAS IBM MVS to   UNIX conversion
;
; NAME:
;	CW_ADAS409_GRAPH()
;
; PURPOSE:
;	Widget for displaying a sequence of ADAS plots.
;
; EXPLANATION:
;	This Routine differs from cw_adas_graph.pro in that a slider
;	is included which allows the user to interactively select a
;	1-D slice for viewing, from a 2-D data set. ie if the plot is
;	constructed (in adas409_1_plot.pro say) in the following manner,
;
;		plot,x(xvals),y(*,index)	y2min < index < y2max
;	
;	then if slider is moved, then the slider returns a new value
;	to be passed to index in adas409_1_plot.pro .
;
;	This widget is a convenient collection of a graphics window
;	and a number of graphical control buttons.  No actual graphics
;	are included with the widget.  This widget simply generates
;	events each time one of the control buttons is pressed.
;	The event returned is the structure;
;
;	{ID:0L, TOP:0L, HANDLER:0L, ACTION:''}
;
;	Where ACTION may have one of five values;
;
;	'previous', 'next', 'print', 'printall' or 'done'.
;
; USE:
;	An example of how to use the widget.
;
;		base = widget_base()
;		graphid = cw_adas_graph(base,/print,/multiplot)
;		widget_control,base,/realize
;		widget_control,graphid,get_value=value
;		win = value.win
;
;	Then use WSET,win to set the widget to receive graphics output.
;	Use XMANAGER or WIDGET_EVENT() to process the button events from
;	the widget.
;
; INPUTS:
;       PARENT   - The ID of the parent widget.
;	  DMAX     - Maximum index for slider.
;
; OPTIONAL INPUTS:
;       None.  See the keywords for additional controls.
;
; OUTPUTS:
;       The return value of this function is a structure containing the
;	 ID of the compound widget which is of type LONG integer, and the
;	  new slider value.
;
; OPTIONAL OUTPUTS:
;       None
;
; KEYWORD PARAMETERS:
;	VALUE	 - A structure which is the value of this widget;
;		   {WIN:0, MESSAGE:''}
;
;		   WIN	   An integer; The IDL window number of the graphical
;			   output area.
;
;		   MESSAGE A string; A message to be displayed in the widget.
;
;		   WIN is only relevant when the value of the widget
;		   is recovered using GET_VALUE.  The value of WIN is
;		   ignored when cw_adas_graph is first created or
;		   SET_VALUE is used.
;
;	MULTIPLOT- Set this keyword to get the 'Next' and 'Previous' buttons.
;		   By default MULTIPLOT is not set.
;
;	PRINT	 - Set this keyword to get the 'Print' button.  If multiplot
;		   is also set a 'Print All' button will also be included on
;		   the widget.  By default PRINT is not set.
;
;	XSIZE	 - The xsize of the graphics area in pixels.  Default
;		   850 pixels.
;
;	YSIZE	 - The ysize of the graphics area in pixels.  Default
;		   750 pixels.
;
;	TITLE	 - A title to appear at the top of the widget.
;
;	FONT	 - The font to be used for text in the widget.  Default
;		   to current screen font.
;	
;	NODONE	 - Set to 1 if 'done' button not required.
;
;	LOWERBASE- Set to 1 if lower-base not required.
;
; CALLS:
;	LOCALIZE	Routine to set the various position parameters
;			used in output plotting.
;
; SIDE EFFECTS:
;	This widget uses a COMMON BLOCK: CW_ADASGR_BLK to hold the
;       widget state.
;
;       Three other routines are included which are used to manage the
;       widget;
;
;	ADAS409_GR_SET_VAL
;	ADAS409_GR_GET_VAL()
;	ADAS409_GR_EVENT()
;
; CATEGORY:
;	Compound Widget
;
; WRITTEN:
;       Richard Martin, University of Strathclyde, 21-01-98.
;		Based on cw_adas_graph.pro v1.16
;
; MODIFIED:
;       1.1 Richard Martin
;		Put under SCCS control.
;	  1.2	Richard Martin
;		Removed obsolete cw_savestae/loadstate statements.
;
; VERSION:
;       1.1	12-03-98
;	1.2	30-01-02
;-
;-----------------------------------------------------------------------------

PRO adas409_gr_set_val, id, value


		;**** Return to caller on error ****
  ON_ERROR, 2

		;**** Retrieve the state ****

  first_child = widget_info(parent,/child)
  widget_control, first_child, get_uvalue=state, /no_copy
  
		;**** Update value in state ****
  state.grval.message = value.message

		;**** Update screen message ****
  widget_control,state.messid,set_value=value.message

		;**** Save new state structure ****

  widget_control, first_child, set_uvalue=state, /no_copy

END

;-----------------------------------------------------------------------------


FUNCTION adas409_gr_get_val, id

		;**** Return to caller on error ****
  ON_ERROR, 2

		;**** Retrieve the state ****

  first_child = widget_info(id,/child)
  widget_control, first_child, get_uvalue=state, /no_copy

		;**** Get IDL window number of the drawing area ****
  widget_control,state.drawid,get_value=value

		;**** Update value ****
  state.grval.win = value

  grval=state.grval
  widget_control, first_child, set_uvalue=state, /no_copy

		;**** Return value ****
  RETURN, grval

END

;-----------------------------------------------------------------------------

FUNCTION adas409_gr_event, event


		;**** Base ID of compound widget ****
  base=event.handler

		;**** Retrieve the state ****

  first_child = widget_info(base,/child)
  widget_control, first_child, get_uvalue=state, /no_copy
  
		;**** Clear any message ****
  widget_control,state.messid,set_value=' '
  state.grval.message = ''

		;************************
		;**** Process Events ****
		;************************
  CASE event.id OF

    state.previd:		action = 'previous'

    state.nextid:		action = 'next'

    state.printid:	action = 'print'

    state.printallid:	action = 'printall'

    state.slidid:		action = 'newdens'
    
    state.doneid:		action = 'done'

    state.keepid:		action = 'done_but_keep'

    state.bitid:		action = 'bitbutton'

  ENDCASE

  widget_control, first_child, set_uvalue=state, /no_copy

  RETURN, {ID:base, TOP:event.top, HANDLER:0L, ACTION:action}
END

;-----------------------------------------------------------------------------

FUNCTION cw_adas409_graph,	topparent, dmax, VALUE=value, MULTIPLOT = multiplot, 	$
			PRINT = print, BITBUTTON = bitbutton,		$
			XSIZE = xsize, YSIZE = ysize, TITLE = title, 	$
			FONT = font, KEEP = keep, NODONE = nodone,      $
                        NOLOWERBASE = nolowerbase

  IF (N_PARAMS() EQ 0) THEN MESSAGE, 'Must specify a parent for cw_adas_graph'
  ON_ERROR, 2					;return to caller

		;**** Set defaults for keywords ****
  IF NOT (KEYWORD_SET(value)) THEN begin
	grval = {grval, WIN:0, MESSAGE:''}
  end else begin
	grval = {grval, WIN:0, MESSAGE:value.message}
  end
  IF NOT (KEYWORD_SET(multiplot)) THEN multiplot = 0
  IF NOT (KEYWORD_SET(print)) THEN print = 0
  IF NOT (KEYWORD_SET(xsize)) THEN xsize = 850

		;**** Customize YSIZE for different TARGET_MACHINEs ****

  machine = GETENV('TARGET_MACHINE')
  IF (machine EQ 'HPUX') THEN BEGIN
    yheight = 650
  ENDIF ELSE BEGIN
    yheight = 750
  ENDELSE
  IF NOT (KEYWORD_SET(ysize)) THEN ysize = 750
  IF NOT (KEYWORD_SET(title)) THEN BEGIN
      title = ' '
      notitle=1
  ENDIF ELSE BEGIN
      notitle=0
  ENDELSE
  IF NOT (KEYWORD_SET(font)) THEN font = ''
  IF NOT (KEYWORD_SET(keep)) THEN keep = 0 else keep=1
  IF NOT (KEYWORD_SET(nodone)) THEN nodone = 0 else nodone=1
  IF NOT (KEYWORD_SET(nolowerbase)) THEN nolowerbase = 0 else nolowerbase=1
  IF NOT (KEYWORD_SET(bitbutton)) THEN BEGIN
      bitflag=0
  ENDIF ELSE BEGIN
      bitflag=1
  ENDELSE

		;**********************************************************
		;**** Assign parameters defining the size of the graph ****
		;**** in the graphics window in IDL procedure localize ****
		;**********************************************************

  localize

		;**** Create the main base for the graph widget ****

  uvalue = 0
  parent = WIDGET_BASE(topparent, UVALUE = uvalue, 				$
		EVENT_FUNC = "adas409_gr_event", 				$
		FUNC_GET_VALUE = "adas409_gr_get_val", 			$
		PRO_SET_VALUE = "adas409_gr_set_val", 			$
		/COLUMN)

		;**** Create base to hold the value of state ****

  first_child = widget_base(parent)

  cwid=widget_base(first_child,/column)
  
		;**** Create upper base ****
  
  upperbase = widget_base(cwid,/row)

		;**** Create lower base ****
  if nolowerbase eq 0 then begin
    lowerbase = widget_base(cwid,/row,space=20)
  end else if nolowerbase eq 1 then begin
    if nodone eq 0 or keep eq 1 or bitflag eq 1 or multiplot ne 0 $
      or print eq 1 then stop,$
  'CW_ADAS_GRAPH ERROR: CHOICE OF KEYWORDS INCOMPATIBLE WITH NOWLOWERBASE'
  end

		;********************************
		;**** The Upper Area         ****
		;********************************

		;**** Graphics area ****

  leftupper = widget_base(upperbase,/column)
  if notitle eq 0 then begin
    rc = widget_label(leftupper,value=title)
  end
  drawid = widget_draw(leftupper,/frame,xsize=xsize,ysize=ysize)


		;********************************
		;**** The lower Area         ****
		;********************************

		;**** Extra bitmapped button if specified ****

  if (bitflag eq 1) then begin
      read_X11_bitmap, bitbutton, bitmap1
      bitid = widget_button(lowerbase, value=bitmap1)
  endif else begin
      bitid = 0L
  endelse

		;**** Previous and Next only needed for multi-plots ****
  if multiplot ne 0 then begin
    previd = widget_button(lowerbase,value='Previous',FONT=font)
    nextid = widget_button(lowerbase,value='Next    ',FONT=font)
  end else begin
    previd = 0L
    nextid = 0L
  end

		;**** Print buttons if enabled ****
  if print eq 1 then begin
    printid = widget_button(lowerbase,value='Print   ',FONT=font)
		;**** Printall only needed for multi-plots ****
    if multiplot ne 0 then begin
      printallid = widget_button(lowerbase,value='Print All',FONT=font)
    end else begin
      printallid = 0L
    end
  end else begin
    printid = 0L
    printallid = 0L
  end

		;**** Done button ****
  if nodone eq 0 then $       
    doneid = widget_button(lowerbase,value='Done',FONT=font) $
  else doneid = 0L 

		;**** 'Done but retain' button ****
  if keep eq 1 then $
    keepid = widget_button(lowerbase,value='Done but retain',FONT=font) $
  else keepid=0L

		;**** Slider ****
 
  slidbase=widget_base(lowerbase,/frame,/row)
  slidtxt=widget_label(slidbase,value='Density Index: ')

  dval =1				; initialise slider at lowest index
  
  slidid=widget_slider(slidbase,minimum=1,maximum=dmax+1,/drag,value=dval)

		;**** Message area ****
  if strtrim(grval.message) ne '' then begin
    message = grval.message
  end else begin
    message=' '
  end
  messid = widget_label(cwid,value=message,font=font)

		;**** Create state structure ****

  new_state =  {DRAWID:drawid, PREVID:previd, BITID:bitid, $
		NEXTID:nextid, PRINTID:printid, PRINTALLID:printallid, $
		DONEID:doneid, MESSID:messid, GRVAL:grval, KEEPID:keepid, $
		SLIDID:slidid}

		;**** Save initial state structure ****
  
  widget_control, first_child, set_uvalue=new_state, /no_copy

  dynlabel, parent
  RETURN, {cwid:parent, slidid:slidid}

END
