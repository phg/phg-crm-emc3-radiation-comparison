; Copyright (c) 1997 Strathclyde University.
; SCCS info: Module @(#)$Header: /home/adascvs/idl/adas4xx/adas409/plot409_1.pro,v 1.1 2004/07/06 14:33:15 whitefor Exp $ Date $Date: 2004/07/06 14:33:15 $
;+
; PROJECT:
;       ADAS Programs
;
; NAME:
;       PLOT409_1
;
; PURPOSE:
;       Plot ion fraction graphs for ADAS409.
;
; EXPLANATION:
;       This routine plots ADAS409 output for one or more plots (each
;       can contain a maximum of seven lines).
;
; USE:
;       Use is specific to ADAS409.  See adas409_1_plot.pro for
;       example.
;
; INPUTS:
;
;	LDEF1	-	Integer; 1 if user specified axis limits to 	
;			be used, 0 if default scaling to be used.
;
;	TOP	-	Integer; last line to plot
;
;	BOTTOM	-	Integer;first line to plot
;
;	X	-	Fltarr; x-values to plot
;
;     Y       -       Fltarr; y-value  to plot
;
;	Z	-	3d Fltarr; z-values to plot (3rd dimension between
;			BOTTOM and TOP)
;
;	RIGHTSTRING  -  String; left hand column of title to side of graph
;			('INDEX')
;
;	RIGHTSTRING2 -  String; right hand column of title to side of graph
;			('DESIGNATION')
;
;	TITLE	-	String; heading to go above graph
;
;	XMIN1	-	Float; user-defined x-axis minimum
;
;	XMAX1	-	Float; user-defined x-axis maximum
;
;	ZMIN1	-	Float; user-defined z-axis minimum
;
;       ZMAX1   -       Float; user-defined z-axis maximum
;
;	DENS	-	Density index (selected by slider).
;
;	WIN	-	ID. no of X-device plot window.
;
; OPTIONAL INPUTS:
;       None.
;
; OUTPUTS:
;       xann - X value of plot labels.
;			1st dim: plot index
;			2nd dim: variable index (density)
;
;	  yann - Y value of plot labels.
;			1st dim: plot index
;			2nd dim: variable index (density) 
;
; OPTIONAL OUTPUTS:
;       None.
;
; KEYWORD PARAMETERS:
;       FONT    - String; Name of font passed to graphical output
;                 widget.
;
; CALLS:
;       None.
;
; SIDE EFFECTS:
;       None.
;
; CATEGORY:
;       Adas system.
;
; WRITTEN:
;       Alessandro Lanzafame, University of Strathclyde, 12 December 1995
;
; MODIFIED:
;	  1.1	Richard Martin
;		Updated to copy empty graph to pixmap window, so as to be
;		used in conjuction with cw_adas409_graph.pro with slider.
;		Also coordinates for labels to all available plots written
;		to an array.
;		Put under SCCS control.
;
; VERSION:
;	  1.1	11-03-98
;
;-
;----------------------------------------------------------------------------

PRO plot409_1, ldef1, top, bottom, x, y, z, rightstring, rightstring2, 	$
               title, xmin1, xmax1, zmin1, zmax1, dens, win, xann, yann

  COMMON Global_lw_data, left, right, tp, bot, grtop, grright

                ;**** Suitable character size for current device ****
                ;**** Aim for 60 characters in y direction.      ****

    charsize = (!d.y_vsize/!d.y_ch_size)/60.0

    x_annot=xann
    y_annot=yann
    
                ;**** Initialise titles ****

    xtitle = 'ELECTRON TEMPERATURE (eV)'
    ztitle = 'N!DINDEX!N / N!DTOTAL!N'
    righthead = '-- METASTABLE ASSIGNMENTS --!C!C'+			$
                '  INDEX         DESIGNATION  '

                ;**** How many points to plot ****

    npts = size(x)
    npts = npts(1)
    nypts = size(y)
    nypts = nypts(1)

		;**** Find x and z ranges for auto scaling,        ****
                ;**** check x and z in range for explicit scaling. ****

    makeplot = 1
    style = 0
    zstyle = 0
    zplot = z(*, *, bottom:top)

    if ldef1 eq 0 then begin

                ;**** identify values in the valid range ****
                ;**** plot routines only work within ****
                ;**** single precision limits.       ****

	xvals = where (x gt 1.0e-37 and x lt 1.0e37)
	zvals = where (zplot gt 1.0e-37 and zplot lt 1.0e37)
	if xvals(0) gt -1 then begin
	    maxx = max(x(xvals))
	    minx = min(x(xvals))
	endif else begin
	    makeplot = 0
	endelse
	if zvals(0) gt -1 then begin
	    maxz = max(zplot(zvals))
	    minz = min(zplot(zvals))
	endif else begin
	    makeplot = 0
	endelse
	if makeplot eq 1 then begin
	    if minz le 1.0e-36 then begin
		zstyle = 1
		minz = 1.0e-36
	    endif else begin
		zstyle = 0
	    endelse
	endif
	style = 0
    endif else begin
      minx = xmin1
	maxx = xmax1
	minz = zmin1
	maxz = zmax1
	xvals = where(x gt minx and x lt maxx)
	zvals = where(zplot gt minz and zplot lt maxz)
	if xvals(0) eq -1 or zvals(0) eq -1 then begin
	    makeplot = 0
	endif else begin
	    makeplot = 1
	endelse
	style = 0
        zstyle = 1
    endelse

    if makeplot eq 1 then begin

                ;**** Set up log-log plotting axes ****
	plot_oo, [minx, maxx], [minz, maxz], /nodata, ticklen=1.0,	$
                 position=[left, bot, grright, grtop],			$
                 xtitle=xtitle, ytitle=ztitle, xstyle=style, 		$
		 ystyle=zstyle, charsize=charsize

                ;**** Output title above graphs ****
    
    xyouts, 0.1, 0.9, title, charsize=charsize, /normal

                ;**** Output titles to right of graphs ****

    xyouts, 0.72, 0.8, righthead, charsize=charsize, /normal
    xyouts, 0.74, 0.72, rightstring, charsize=charsize, /normal
    xyouts, 0.84, 0.72, rightstring2, charsize=charsize, /normal

                ;**** Output chosen density to right of graphs ****
     xyouts, 0.76, 0.4, '-ELECTRON DENSITY-', charsize=charsize, /normal        
           
	; Copy empty graph to pixmap window

	if (!d.name eq 'X') then begin
	wset, win
		xwidth=!d.x_size
		yheight=!d.y_size	
		window, 9, /pixmap, xsize=xwidth, ysize=yheight
		device, copy=[0,0,xwidth,yheight,0,0,win]
		wset, win	
	endif
	
     densstring=strtrim(string(dens+1),2)+': '+strtrim(string(y(dens)),2)+' cm-3'
     xyouts, 0.75, 0.35, densstring, charsize=charsize, /normal

                ;*********************************
                ;**** Make and annotate plots ****
                ;*********************************

 	for i=bottom,top do begin
	    zmaxchk = max(z(*,*,i))		;check this line is okay to 
						;draw
	    if zmaxchk le 1.0e-36 then begin
		print, '******************************* D9OTG1 MESSAGE '+$
                       '*******************************'
		print, 'METASTABLE: ', i+1, ' NO GRAPH WILL BE OUTPUT ' +$
		       'BECAUSE:'
		print, 'ALL VALUES ARE BELOW THE CUTOFF OF 1.000E-36'
		print, '******************************* END OF MESSAGE '+$
                       '*******************************'
	    endif else begin

            oplot, x, z(*, dens, i), linestyle=0

            
		for idens=0, (nypts-1) do begin

                ;**** Find suitable point for annotation ****

                    if (minz eq 10^(!y.crange(0))) then begin
	               if (x(npts-1) ge minx) and $
                          (x(npts-1) le maxx) and $
                          (z(npts-1,idens,i) gt minz) and $
                          (z(npts-1,idens,i) lt $
			  (10^(!y.crange(1)))) then begin
				iplot = npts - 1
			endif else begin
		           iplot = -1
		           for id = 0, npts-2 do begin
		               if (x(id) ge minx) and $
                                  (x(id) le maxx) and $
		                  (z(id,idens,i) gt minz) and $
                                  (z(id,idens,i) le maxz) then begin
					iplot = id
		               endif
	                   endfor
	                endelse
                    endif else begin
                        if (x(npts-1) ge minx) and $
			   (x(npts-1) le maxx) and $
                           (z(npts-1,idens,i) ge minz) and $
                           (z(npts-1,idens,i) lt $
			   (10^(!y.crange(1)))) then begin
				iplot = npts - 1
                        endif else begin
				iplot = -1
				for id = 0, npts-2 do begin
				   if (x(id) ge minx) and $
                                      (x(id) le maxx) and  $
                                      (z(id,idens,i) ge minz) and $
                                      (z(id,idens,i) le maxz) then begin
				         iplot = id
				   endif
				endfor
                        endelse
                    endelse
    
                ;**** store coords for annotation ****

 	            if iplot ne -1 then begin
			  x_annot(i,idens)=x(iplot)
			  y_annot(i,idens)=z(iplot,idens,i)
	            endif

                ;**** Annotate plots with level numbers ****
                
                	if (idens eq dens) then begin
				if (iplot ne -1) then begin
		        	   xyouts, x(iplot), z(iplot,dens,i), 	$
                         	string(i+1,format='(i2)'), alignment=0.5
				endif 
  	            endif            
  	            	            
                endfor                       

	    endelse
	endfor

    endif else begin			;no plot possible
        xyouts, 0.2, 0.5, /normal, charsize=charsize*1.5,	$
                '---- No data lies within range ----'
    endelse

    xann=x_annot
    yann=y_annot


END
