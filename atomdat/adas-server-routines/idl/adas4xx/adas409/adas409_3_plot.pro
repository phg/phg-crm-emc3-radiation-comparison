; Copyright (c) 1997, Strathclyde University.
; SCCS info: Module @(#)$Header: /home/adascvs/idl/adas4xx/adas409/adas409_3_plot.pro,v 1.1 2004/07/06 10:47:28 whitefor Exp $ Date $Date: 2004/07/06 10:47:28 $
;+
; PROJECT:
;       ADAS IBM MVS to UNIX conversion
;
; NAME:
;       ADAS409_3_PLOT
;
; PURPOSE:
;       Generates ADAS409 contribution function graphical output.
;
; EXPLANATION:
;       This routine creates a window for the display of graphical
;       output. A separate routine PLOT409_3 actually plots the
;       graph.
;
; USE:
;       This routine is specific to the contribution fn plotting section
;	of ADAS409.
;
; INPUTS:
;       DSFULL  - String; Name of data file
;
;       UTITLE  - String; Optional comment by user added to graph title.
;
;       LDEF3   - Integer; 1 - use user entered graph scales
;                          0 - use default axes scaling
;
;	NMSUM	- Integer; no. of metastables (output curves)
;
;	X	- Fltarr; electron temperatures - x-values
;
;	Y	- Fltarr; electron densities - y-values
;
;	Z	- 3D Fltarr; contribution functions - z-values
;			1st dim:	temperature index
;			2nd dim:	density index
;			3rd dim:	component index
;
;	GCF	- Generalised contribution function.
;			1st dim:	temperature index
;			2nd dim:	density index
;
;       HRDOUT - Integer; 1 if hardcopy output activated, 0 if not.
;
;       HARDNAME- String; Filename for harcopy output.
;
;       DEVICE  - String; IDL name of hardcopy output device.
;
;       HEADER  - String; ADAS version number header to include in graph.
;
;	POPTIT	- String array; the metastable designations (used as label)
;
;	SPECIES - String; element name to be used in graph title
;
;	YEAR	- String; user's selected year for data
;
;	DYEAR	- String; user's selected default year for data (if any)
;
;       XMIN3   - String; Lower limit for x-axis of graph, number as string.
;
;       XMAX3   - String; Upper limit for x-axis of graph, number as string.
;
;       YMIN3   - String; Lower limit for y-axis of graph, number as string.
;
;       YMAX3   - String; Upper limit for y-axis of graph, number as string.
;
;	ISPLINE	- Integer; the selected line from the script file.
;
;	CTITLE  - String array: Any information given in script file on
;                 lines to be analysed.
;
;       CMPTS   - Int array: no. components for each line in script file
;
;	IPLINE	- String array; indices of plotted lines (used in labelling)
;
;	RIGHTSTRING5 - String; Contains all labels used in 'component
;		       parameters' label on graph, this string is
;		       interrogated by plot409_3 to extract all the
;		       required information.
;
;       BITFILE - String; the path to the directory containing bitmaps
;                 for the 'escape to series menu' button.
;
; OPTIONAL INPUTS:
;       None.
;
; OUTPUTS:
;       GOMENU  - Int; flag - set to 1 if user has selected 'escape direct
;                 to series menu', otherwise 0
;
; OPTIONAL OUTPUTS:
;       None.
;
; KEYWORD PARAMETERS:
;       FONT    - String; The name of a font to use for text in the
;                 graphical output widget.
; CALLS:
;       CW_ADAS_GRAPH   Graphical output widget.
;       PLOT409_3       Make plots to an output device for 409(cont fn)
;       XMANAGER
;
; SIDE EFFECTS:
;       This routine uses a common block to maintain its state 
;	PLOT409_3_BLK.
;
;       One other routine is included in this file;
;       ADAS409_3_PLOT_EV Called via XMANAGER during widget management.
;
; CATEGORY:
;       Adas system.
;
; WRITTEN:
;       Alessandro Lanzafame, University of Strathclyde, 13th December 1995
;
; MODIFIED:	1.1	Richard Martin
;			Put through SCCS.
;
; VERSION:	1.1	12-03-98
;-
;----------------------------------------------------------------------------

PRO adas409_3_plot_ev, event

    COMMON plot409_3_blk, action, plotdev, plotfile, fileopen, win, 	$
                          iplot, nplot, data, nsum, gomenu, cwidg

    newplot = 0
    print = 0
    done = 0
                ;****************************************
                ;**** Set graph and device requested ****
                ;****************************************

    CASE event.action OF

        'previous' : begin
            if iplot gt 0 then begin
                newplot = 1
                iplot = iplot - 1
                first = iplot
                last = iplot
            endif
	end

        'next'     : begin
            if iplot lt nplot-1 then begin
                newplot = 1
                iplot = iplot + 1
                first = iplot
                last = iplot
            endif
        end

        'print'    : begin
            newplot = 1
            print = 1
	      first = iplot
	      last = iplot
            set_plot, plotdev
            if fileopen eq 0 then begin
                fileopen = 1
                device, filename=plotfile
                device, /landscape
            endif	    
        end

        'printall' : begin
            newplot = 1
            print = 2
            first = 0
            last = nplot-1
             set_plot, plotdev
            if fileopen eq 0 then begin
                fileopen = 1
                device, filename=plotfile
                device, /landscape
            endif

        end

        'newdens' : begin
	      widget_control,cwidg.slidid,get_value=tdens
	      data.dens=tdens-1
		xwidth=!d.x_size
		yheight=!d.y_size
		device, copy=[0,0,xwidth,yheight,0,0,9]

		for i=data.bottom,data.top do begin
		
		 if (data.ldef3 eq 1) then begin

		   dummyx=where(data.x ge data.xmin3 and data.x le data.xmax3 )
		   nel=n_elements(dummyx)
		   xmn=dummyx(0)
		   xmx=dummyx(nel-1)

		   dummyz=where(data.z(*, data.dens, i) ge data.zmin3 and $
		   		  data.z(*, data.dens, i) le data.zmax3 )

		   nel=n_elements(dummyz)
		   zmn=dummyz(0)
		   zmx=dummyz(nel-1)	
		   mmin=max([xmn,zmn])
		   mmax=min([xmx,zmx])

		   if (mmax gt mmin ) then begin
                  oplot, data.x(mmin:mmax), data.z(mmin:mmax,data.dens,i), $
                  	linestyle=2
                  oplot, data.x(mmin:mmax), data.gcf(mmin:mmax,data.dens), $
                  	linestyle=0
		   endif
               

               xyouts, data.xann(i,data.dens), data.yann(i,data.dens),	$
                    	string(i+1, format='(i2)'), alignment=0.5 
               xyouts, data.xann(0,data.dens), data.yann(0,data.dens),'TOT'      
     		   densstring=strtrim(string(data.dens+1),2)+': '+strtrim(string(data.y(data.dens)),2)+' cm-3'
    		   charsize = (!d.y_vsize/!d.y_ch_size)/60.0 		; see plot409_1
     	 	   xyouts, 0.75, 0.31,densstring, charsize=charsize, /normal       
		   		   		
			;******************************************************************
     	 	               	
		 endif else begin

               oplot, data.x, data.z(*, data.dens, i),linestyle=2
               oplot, data.x, data.gcf(*, data.dens),linestyle=0           
               xyouts, data.xann(i+1,data.dens), data.yann(i+1,data.dens),	$
                    	string(i+1, format='(i2)'), alignment=0.5 
               xyouts, data.xann(0,data.dens), data.yann(0,data.dens),'TOT'                     	
     		   densstring=strtrim(string(data.dens+1),2)+': '+strtrim(string(data.y(data.dens)),2)+' cm-3'
    		   charsize = (!d.y_vsize/!d.y_ch_size)/60.0 		; see plot409_1
     	 	   xyouts, 0.75, 0.31, densstring, charsize=charsize, /normal  

		 endelse
		end
        end
        
        'done'     : begin
            if fileopen eq 1 then begin
                set_plot, plotdev
                device, /close_file
            end
            set_plot,'X'
            widget_control, event.top, /destroy
            done = 1
        end

        'bitbutton' : begin
            if fileopen eq 1 then begin
                set_plot, plotdev
                device, /close_file
            end
            set_plot,'X'
            widget_control, event.top, /destroy
            done = 1
            gomenu = 1
        end
              
    END

                ;*******************************
                ;**** Make requested plot/s ****
                ;*******************************

    if done eq 0 then begin

                ;**** Set graphics device ****

	 if print eq 0 then begin
            set_plot,'X'
            wset, win
       endif

                ;**** Draw graphics ****

        if newplot eq 1 then begin
            for i = first, last do begin
	        bottom = i * 7
	        top = bottom + 6
                if top gt (nsum-1) then top=nsum-1
	          data.top=top
	          data.bottom=bottom
		    x_annot=data.xann
		    y_annot=data.yann	                          
                plot409_3, data.ldef3, data.top, data.bottom, data.x, $
                	   data.y, data.z, data.gcf, $
                     data.rightstring, data.rightstring2, 	$
			   data.rightstring3, data.rightstring4,	$
			   data.rightstring5, data.rightstring6,	$
			   data.rightstring7, data.rightstring8, 	$
			   data.rightstring9, data.rightstring10, 	$
                           data.ipline, 				$
			   data.title, data.xmin3, data.xmax3, 		$
			   data.zmin3, data.zmax3, data.dens, win,	$
			   x_annot, y_annot
		    data.xann=x_annot
		    data.yann=y_annot			   
	        if print eq 1 then begin
                    message = 'Plot  written to print file.'
                    grval = {WIN:0, MESSAGE:message}
                    widget_control, event.id, set_value=grval
                	  set_plot, plotdev
                	  print=0
                 	  if (fileopen eq 2) then begin	;i.e. "printall" was clicked
                			first = iplot
                			last = iplot
	        			data.bottom = iplot * 7 	;Recover original plot
	        			data.top = data.bottom + 6
                			if data.top gt (nsum-1) then data.top=nsum-1	     
                			  			
            	  endif
            	  fileopen=0	             	  
                	  device, /close_file
	              set_plot,'X'
                endif else if print eq 2 then begin
                    message = 'Plots  written to print file.'
                    grval = {WIN:0, MESSAGE:message}
                    widget_control, event.id, set_value=grval
                	  print=1
                	  fileopen=2 	;just use this as a flag time-being       
              endif
	    endfor
        endif
    endif

END

;----------------------------------------------------------------------------
                                                                
PRO adas409_3_plot, dsfull, utitle, ldef3, nmsum,	   $
	 x, y, z, gcf,			   $
		hrdout, hardname, device, header, poptit, 	   $
		species, year, dyear, xmin3, xmax3, zmin3, zmax3, densel,  $
		ispline, ctitle, cmpts, ipline, rightstring5,	   $
		bitfile, gomenu, FONT=font

    COMMON plot409_3_blk, action, plotdev, plotfile, fileopen, win, 	$
                          iplot, nplot, data, nsum, gomenucom, cwidg

    ngpic = 7			;max no. of lines per plot

                ;**** Copy input values to common ****

    plotdev = device
    plotfile = hardname
    fileopen = 0
    nsum = nmsum
    gomenucom = gomenu

		;*********************************************
		;**** Generate indices to be sent to plot ****
		;**** routine if there are to be multiple ****
		;**** plots drawn			  		****
		;*********************************************

    nplot = 0
    if nmsum le ngpic then begin
	bottom = 0
	top = (nmsum-1)
	nplot = 1
    endif else begin
	bottom = 0
	top = (ngpic-1)
        if nmsum mod ngpic ne 0 then begin
            nplot = fix(nmsum/ngpic) + 1
        endif else begin
            nplot = fix(nmsum/ngpic)
        endelse
    endelse

                ;************************************
                ;**** Create general graph titles****
                ;************************************

    title = "CONTRIBUTION FUNCTION VS. ELECTRON TEMPERATURE"
    if ( strtrim(strcompress(utitle),2)  ne ' ' ) then begin
        title = title + ': ' + strupcase(strtrim(utitle,2))
    endif
    title =  title + '!C!CADAS    : ' + strupcase(header)
    if strcompress(dyear, /remove_all) eq '' then defyear = 'NONE' else $
    defyear = strcompress(dyear, /remove_all)
    year = strcompress(year, /remove_all)
    title =  title + '!CFILE     : ' + strcompress(dsfull) + 		$
    '   SPECIES: ' + strupcase(species) + '   YEAR: ' + year +		$
    '   DEFAULT YEAR: '+ defyear
    title =  title + '!CKEY     : (FULL LINE - TOTAL) (DASH LINE - PARTIAL)'
    
    rightstring = ''
    rightstring2 = ''
    rightstring3 = '!C!C'
    rightstring4 = '!C!C'
    rightstring6 = '!C!C'
    rightstring7 = '!C!C'
    rightstring8 = '!C!C'
    rightstring9 = '!C!C'
    rightstring10 = '!C!C'
    for i=0, (nmsum-1) do begin
        if (i+1) lt 10 then rightstring = rightstring + ' '
        rightstring = rightstring + strtrim(string(ipline(i)),2) + '!C'
        rightstring2 = rightstring2 + strupcase(poptit(ipline(i)-1)) + '!C'
        rightstring3 = rightstring3 + '!C'
        rightstring4 = rightstring4 + '!C'
        rightstring6 = rightstring6 + '!C'
        rightstring7 = rightstring7 + '!C'
        rightstring8 = rightstring8 + '!C'
        rightstring9 = rightstring9 + '!C'
        rightstring10 = rightstring10 + '!C'
    endfor
    rightstring3 = rightstring3 + '-- SPECTRUM LINE SPECIFICATION --'
    rightstring3 = rightstring3 + '!C!C TITLE!C SELECT NO.!C COMPONENTS'+$
                   '!C!C!C-- COMPONENT PARAMETERS --'
    rightstring4 = rightstring4 + '!C!C= ' +				$
		   strtrim(string(ctitle), 2) + 'A' + 			$
                   '!C= ' + strcompress(string(ispline+1),/remove_all) +$
                   '!C= ' + strcompress(string(cmpts), /remove_all)
    rightstring6 = rightstring6 + '!C!C!C!C!C'
    rightstring7 = rightstring7 + '!C!C!C!C!C'
    rightstring8 = rightstring8 + '!C!C!C!C!C'
    rightstring9 = rightstring9 + '!C!C!C!C!C'
    rightstring10 = rightstring10 + '!C!C!C!C!C'

                ;*************************************
                ;**** Create graph display widget ****
                ;*************************************

    graphid = widget_base(TITLE='ADAS409 GRAPHICAL OUTPUT', 		$
                          XOFFSET=1,YOFFSET=1)
    device, get_screen_size=scrsz
    xwidth=scrsz(0)*0.75
    yheight=scrsz(1)*0.75
    if nplot gt 1 then multiplot=1 else multiplot=0
    bitval = bitfile + '/menu.bmp'

    dmax=n_elements(y)
    cwidg = cw_adas409_graph(graphid, dmax-1, print=hrdout, FONT=font,		$
                         xsize=xwidth, ysize=yheight, 			$
			 multiplot=multiplot, bitbutton=bitval)

                ;**** Realize the new widget ****

    widget_control, graphid, /realize

                ;**** Get the id of the graphics area ****

    widget_control, cwidg.cwid, get_value=grval
    win = grval.win

                ;*******************************************
                ;**** Put the graphing data into common ****
                ;*******************************************

    dens=0
    
    x_annot=fltarr(nsum+1,dmax)
    y_annot=fltarr(nsum+1,dmax)
       
    data = {	x		:	x,			$
		y			:	y,		$
		Z			:	z,			$
		GCF			:	gcf,			$
		XMIN3			:	xmin3,		$
		XMAX3			:	xmax3,		$
		ZMIN3			:	zmin3,		$
		ZMAX3			:	zmax3,		$
		RIGHTSTRING		:	rightstring,	$
		RIGHTSTRING2	:	rightstring2,	$
		RIGHTSTRING3	:	rightstring3,	$
		RIGHTSTRING4	:	rightstring4,	$
		RIGHTSTRING5	:	rightstring5,	$
		RIGHTSTRING6	:	rightstring6,	$
		RIGHTSTRING7	:	rightstring7,	$
		RIGHTSTRING8	:	rightstring8,	$
		RIGHTSTRING9	:	rightstring9,	$
		RIGHTSTRING10	:	rightstring10,	$
		IPLINE		:	ipline,		$
		TITLE			:	title,		$
		LDEF3			:	ldef3,		$
		TOP			: 	top,			$
		BOTTOM		: 	bottom,		$
		DENS			:	dens,			$
		XANN			:	x_annot,		$
		YANN			:	y_annot	}

                ;**** Initialise to plot 0 ****
    iplot = 0
    wset, win
	
    plot409_3, ldef3, top, bottom,  						     $
	     x, y, z, gcf,  					     $
	     rightstring, rightstring2,					     $
	     rightstring3, rightstring4, rightstring5, rightstring6,     $
	     rightstring7, rightstring8, rightstring9, rightstring10,    $
	     ipline, title, xmin3, xmax3, zmin3, zmax3, dens, win,		$
           x_annot, y_annot	    
           
    data.xann=x_annot
    data.yann=y_annot             

                ;***************************
                ;**** make widget modal ****
                ;***************************

    xmanager, 'adas409_3_plot', graphid, /modal, /just_reg,		$
              event_handler='adas409_3_plot_ev'

    gomenu = gomenucom
    densel = data.dens
    
END
