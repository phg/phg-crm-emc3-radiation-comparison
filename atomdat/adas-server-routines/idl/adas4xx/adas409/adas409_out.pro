; Copyright (c) 1997, Strathclyde University.
; SCCS info: Module @(#)$Header: /home/adascvs/idl/adas4xx/adas409/adas409_out.pro,v 1.1 2004/07/06 10:47:33 whitefor Exp $ Date $Date: 2004/07/06 10:47:33 $
;+
; PROJECT:
;       ADAS Programs
;
; NAME:
;	ADAS409_OUT
;
; PURPOSE:
;	IDL ADAS user interface, output options.
;
; EXPLANATION:
;	This routine creates and manages a pop-up window which allows
;	the user to select options and input data to control ADAS409
;	graphical and file output.
;
; USE:
;	This routine is specific to adas409.                  
;
; INPUTS:
;	VAL	- A structure which determines the initial settings of
;		  the output options widget.  The value is passed
;		  unmodified into cw_adas409_out.pro.
;
;		  See cw_adas409_out.pro for a full description of this
;		  structure.
;
;	DSFULL	- String; The full system file name of the input 
;		  dataset selected by the user for processing.
;
;       LSNULL  - Integer; flag showing whether a null script file has
;                 been selected (=1) or not (=0)
;
;       BITFILE - String; the path to the dirctory containing bitmaps
;                 for the 'escape to series menu' button.
;
; OPTIONAL INPUTS:
;	None.
;
; OUTPUTS:
;	ACT	- String;'Done', 'Cancel' or 'Menu' for the button the
;		  user pressed to terminate the output options window.
;
; OPTIONAL OUTPUTS:
;	None.
;
; KEYWORD PARAMETERS:
;	DEVLIST	- A string array;  Each element of the array is the
;		  name of a hardcopy graphics output device.  This
;		  list must mirror the DEVCODE list.  DEVLIST is the
;		  list which the user will see.
;		  e.g ['Post-Script','HP-PCL','HP-GL']
;
;       FONT_LARGE- Supplies the large font to be used.
;
;       FONT_SMALL- Supplies the small font to be used.
;
; CALLS:
;	CW_ADAS409_OUT	Creates the output options widget.
;	XMANAGER
;	See side effects for widget management routine.
;
; SIDE EFFECTS:
;	This routine uses a common block OUT409_BLK to maintain its state.
;	ADAS409_OUT_EV	is included in this file and is called
;			indirectly from XMANAGER during widget
;			management.
;
; CATEGORY:
;	Widgets
;
; WRITTEN:
;       Alessandro Lanzafame, University of Strathclyde
;		First release: adapted from adas405_out.pro
;
; MODIFIED:
;	1.1	Richard Martin
;		Put under SCCS control.
;
; VERSION:
;	1.1	12-03-98
; 
;-
;-----------------------------------------------------------------------------


pro adas409_out_ev, event

    common out409_blk, action, value
	

		;**** Find the event type and copy to common ****

    action = event.action

    case action of

		;**** 'Done' button ****

	'Done'  : begin

			;**** Get the output widget value ****

     	     child = widget_info(event.id, /child)
	     widget_control, child, get_value=value 

                        ;*****************************************
			;**** Kill the widget to allow IDL to ****
			;**** continue and interface with     ****
			;**** FORTRAN only if there is work   ****
			;**** for the FORTRAN to do.          ****
			;*****************************************

	     if (value.grpout eq 1) or (value.texout eq 1)  or 		$
             (value.gofout eq 1) then begin
	        widget_control, event.top, /destroy
	     endif 
	end


		;**** 'Cancel' button ****

	'Cancel': widget_control, event.top, /destroy

		;**** 'Menu' button ****

	'Menu': widget_control, event.top, /destroy

    endcase

END

;-----------------------------------------------------------------------------


pro adas409_out, val, dsfull, act, lsnull, bitfile, 			$
                 devlist=devlist, font_large=font, font_small=font_small

    common out409_blk, action, value

		;**** Copy value to common ****

    value = val

		;**** Set defaults for keywords ****

    if not (keyword_set(font)) then font = ''
    if not (keyword_set(font_small)) then font_small = ''
    if not (keyword_set(devlist)) then devlist = ''

		;***************************************
		;**** Pop-up a new widget           ****
		;***************************************

                ;**** create base widget ****

    outid = widget_base(title='ADAS409 OUTPUT OPTIONS',xoffset=100,yoffset=1)

		;**** Declare output options widget ****

    cwid = cw_adas409_out(outid, dsfull, lsnull, bitfile, 		$
                          value=value, devlist=devlist, font_large=font,$
			  font_small=font_small)

		;**** Realize the new widget ****

    widget_control, outid, /realize

		;**** make widget modal ****

    xmanager, 'adas409_out', outid, event_handler='adas409_out_ev',	$
              /modal, /just_reg
 
		;**** Return the output value from common ****

    act = action
    val = value

END

