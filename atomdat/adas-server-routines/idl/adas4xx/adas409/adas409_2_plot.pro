; Copyright (c) 1997, Strathclyde University.
; SCCS info: Module @(#)$Header: /home/adascvs/idl/adas4xx/adas409/adas409_2_plot.pro,v 1.1 2004/07/06 10:47:26 whitefor Exp $ Date $Date: 2004/07/06 10:47:26 $
;+
; PROJECT:
;       ADAS IBM MVS to UNIX conversion
;
; NAME:
;       ADAS409_2_PLOT
;
; PURPOSE:
;       Generates ADAS409 power function graphical output.
;
; EXPLANATION:
;       This routine creates a window for the display of graphical
;       output. A separate routine PLOT409_2 actually plots the
;       graph.
;
; USE:
;       This routine is specific to the power function plotting section
;	of ADAS409.
;
; INPUTS:
;       DSFULL  - String; Name of data file
;
;       UTITLE  - String; Optional comment by user added to graph title.
;
;       LDEF2   - Integer; 1 - use user entered graph scales
;                          0 - use default axes scaling
;
;	NMSUM	- Integer; no. of metastables (output curves)
;
;	X	- Fltarr; electron temperatures - x-values
;
;	Y	- Fltarr; electron densities - y-values
;
;	Z	- 3D Fltarr; power functions - z-values
;			1st Dim: Temperature index
;			2nd Dim: Density index
;			3rd Dim: Metastable index.
;
;       HRDOUT - Integer; 1 if hardcopy output activated, 0 if not.
;
;       HARDNAME- String; Filename for harcopy output.
;
;       DEVICE  - String; IDL name of hardcopy output device.
;
;       HEADER  - String; ADAS version number header to include in graph.
;
;	POPTIT	- String array; the metastable designations (used as label)
;
;	SPECIES - String; element name to be used in graph title
;
;	YEAR	- String; user's selected year for data
;
;	DYEAR	- String; user's selected default year for data (if any)
;
;       XMIN2   - String; Lower limit for x-axis of graph, number as string.
;
;       XMAX2   - String; Upper limit for x-axis of graph, number as string.
;
;       YMIN2   - String; Lower limit for y-axis of graph, number as string.
;
;       YMAX2   - String; Upper limit for y-axis of graph, number as string.
;
;	PTOT	- 3D Fltarr; Total power functions (PRB,PRC,PLT,TOTAL),
;		  plotted as y-values for cases where the relevant power
;		  function data is available.
;			1st Dim: Temperature index
;			2nd Dim: Density index
;			3rd Dim: Power function index.
;
;       BITFILE - String; the path to the directory containing bitmaps
;                 for the 'escape to series menu' button.
;
; OPTIONAL INPUTS:
;       None.
;
; OUTPUTS:
;       GOMENU  - Int; flag - set to 1 if user has selected 'escape direct
;                 to series menu', otherwise 0
;
; OPTIONAL OUTPUTS:
;       None.
;
; KEYWORD PARAMETERS:
;       FONT    - String; The name of a font to use for text in the
;                 graphical output widget.
;
; CALLS:
;       CW_ADAS_GRAPH   Graphical output widget.
;       PLOT409_2       Make plots to an output device for 409(power fn).
;       XMANAGER
;
; SIDE EFFECTS:
;       This routine uses a common block to maintain its state 
;	PLOT409_2_BLK.
;
;       One other routine is included in this file;
;       ADAS409_2_PLOT_EV Called via XMANAGER during widget management.
;
; CATEGORY:
;       Adas system.
;
; WRITTEN:
;       Alessandro Lanzafame, University of Strathclyde, 12 December 1995
;
; MODIFIED:	1.1	Richard Martin
;			Put under SCCS control.
;
; VERSION:	1.1	12-03-98
;
;-
;----------------------------------------------------------------------------

PRO adas409_2_plot_ev, event

    COMMON plot409_2_blk, action, plotdev, plotfile, fileopen, win, 	$
                          iplot, nplot, data, nsum, gomenu, cwidg

    newplot = 0
    print = 0
    done = 0
                ;****************************************
                ;**** Set graph and device requested ****
                ;****************************************

    CASE event.action OF

        'previous' : begin
            if iplot gt 0 then begin
                newplot = 1
                iplot = iplot - 1
                first = iplot
                last = iplot
            endif
	end

        'next'     : begin
            if iplot lt nplot-1 then begin
                newplot = 1
                iplot = iplot + 1
                first = iplot
                last = iplot
            endif
        end

        'print'    : begin
            newplot = 1
            print = 1
	      first = iplot
	      last = iplot
            set_plot, plotdev
            if fileopen eq 0 then begin
                fileopen = 1
                device, filename=plotfile
                device, /landscape
            endif
        end

        'printall' : begin
            newplot = 1
            print = 2
            first = 0
            last = nplot-1
             set_plot, plotdev
            if fileopen eq 0 then begin
                fileopen = 1
                device, filename=plotfile
                device, /landscape
            endif            
        end

        'newdens' : begin
	      widget_control,cwidg.slidid,get_value=tdens
	      data.dens=tdens-1
		xwidth=!d.x_size
		yheight=!d.y_size
    	      tlabels = [' PRB ',' PRC ',' PLT ',' TOT ']
		device, copy=[0,0,xwidth,yheight,0,0,9]		

		for i=data.bottom,data.top do begin

		 if (data.ldef2 eq 1) then begin	;explicit scaling

		   dummyx=where(data.x ge data.xmin2 and data.x le data.xmax2 )
		   nel=n_elements(dummyx)
		   xmn=dummyx(0)
		   xmx=dummyx(nel-1)

		   dummyz=where(data.z(xmn:xmx, data.dens, i) ge data.zmin2 and $
		   		  data.z(xmn:xmx, data.dens, i) le data.zmax2 )

		   nel=n_elements(dummyz)
		   zmn=dummyz(0)
		   zmx=dummyz(nel-1)	
		   mmin=max([xmn,zmn])
		   mmax=min([xmx,zmx])

		   if (mmax gt mmin ) then begin
                    oplot, data.x(mmin:mmax), data.z(mmin:mmax,data.dens,i),$
				linestyle=2
		   endif

               xyouts, data.xann(i+4,data.dens), data.yann(i+4,data.dens),	$
                    	string(i+1, format='(i2)'), alignment=0.5 
     		   densstring=strtrim(string(data.dens+1),2)+': '+strtrim(string(data.y(data.dens)),2)+' cm-3'
    		   charsize = (!d.y_vsize/!d.y_ch_size)/60.0 		; see plot409_1
     	 	   xyouts, 0.75, 0.35, densstring, charsize=charsize, /normal        
     	 	              	
		 endif else begin

               oplot, data.x, data.z(*, data.dens, i), linestyle=2              
               xyouts, data.xann(i+4,data.dens), data.yann(i+4,data.dens),	$
                    	string(i+1, format='(i2)'), alignment=0.5 
     		   densstring=strtrim(string(data.dens+1),2)+': '+strtrim(string(data.y(data.dens)),2)+' cm-3'
    		   charsize = (!d.y_vsize/!d.y_ch_size)/60.0 		; see plot409_1
     	 	   xyouts, 0.75, 0.35, densstring, charsize=charsize, /normal        
     	 	 endelse             	
	     endfor

		; ****** Now for total plots ******

	     for i=0,3 do begin
             
             if i eq 3 then aligval=0.0 else aligval=1.0
	       
	       if (data.ldef2 eq 1) then begin

		     dummyt=where(data.ptot(xmn:xmx, data.dens, i) ge data.zmin2 and $
		   		  data.ptot(xmn:xmx, data.dens, i) le data.zmax2 )

		     nel=n_elements(dummyt)
		     tmn=dummyt(0)
		     tmx=dummyt(nel-1)	
		     mmin=max([xmn,tmn])
		     mmax=min([xmx,tmx])

		     if (mmax gt mmin ) then begin
                    oplot, data.x(mmin:mmax), data.ptot(mmin:mmax,data.dens,i),$
				linestyle=0
                    xyouts, data.xann(i,data.dens), data.yann(i,data.dens), $
              	       tlabels(i), alignment=aligval , charsize=charsize*0.9  				
		     endif
	       endif else begin

                oplot, data.x, data.ptot(*, data.dens, i), linestyle=0 
                xyouts, data.xann(i,data.dens), data.yann(i,data.dens), $
              	tlabels(i), alignment=aligval , charsize=charsize*0.9
   
	       endelse
	    endfor
        end


        'done'     : begin
            if fileopen eq 1 then begin
                set_plot, plotdev
                device, /close_file
            end
            set_plot,'X'
            widget_control, event.top, /destroy
            done = 1
        end

        'bitbutton' : begin
            if fileopen eq 1 then begin
                set_plot, plotdev
                device, /close_file
            end
            set_plot,'X'
            widget_control, event.top, /destroy
            done = 1
            gomenu = 1
        end
             
    END

                ;*******************************
                ;**** Make requested plot/s ****
                ;*******************************

    if done eq 0 then begin

                ;**** Set graphics device ****

	 if print eq 0 then begin
            set_plot,'X'
            wset, win
       endif

                ;**** Draw graphics ****

        if newplot eq 1 then begin
            for i = first, last do begin
	        bottom = i * 7
	        top = bottom + 6
                if top gt (nsum-1) then top=nsum-1
 	          data.top=top
	          data.bottom=bottom
		    x_annot=data.xann
		    y_annot=data.yann               
                plot409_2, data.ldef2, top, bottom, data.x, data.y, data.z, $
                     data.ptot,data.rightstring, data.rightstring2, 	$
			   data.title, data.xmin2, data.xmax2, 		$
			   data.zmin2, data.zmax2, data.dens, win, 	  $
			   x_annot, y_annot
		    data.xann=x_annot
		    data.yann=y_annot			   
	        if print eq 1 then begin
                    message = 'Plot  written to print file.'
                    grval = {WIN:0, MESSAGE:message}
                    widget_control, event.id, set_value=grval
                	  set_plot, plotdev
                	  print=0
                 	  if (fileopen eq 2) then begin	;i.e. "printall" was clicked
                			first = iplot
                			last = iplot
	        			data.bottom = iplot * 7 	;Recover original plot
	        			data.top = data.bottom + 6
                			if data.top gt (nsum-1) then data.top=nsum-1	   
                			    			
            	  endif
            	  fileopen=0	             	  
                	  device, /close_file
	              set_plot,'X'                    
                endif else if print eq 2 then begin
                    message = 'Plots  written to print file.'
                    grval = {WIN:0, MESSAGE:message}
                    widget_control, event.id, set_value=grval
                	  print=1
                	  fileopen=2 	;just use this as a flag time-being       
              endif
	    endfor
        endif
    endif

END

;----------------------------------------------------------------------------
                                                                
PRO adas409_2_plot, dsfull, utitle, ldef2, nmsum, x, y, z, $
		  prb, prc, plt, tot, hrdout, hardname, device, header, poptit,  $
		  species, year, dyear, xmin2, xmax2, zmin2, zmax2,  $
		  densel, bitfile, gomenu, FONT=font

    COMMON plot409_2_blk, action, plotdev, plotfile, fileopen, win, 	$
                          iplot, nplot, data, nsum, gomenucom, cwidg

    ngpic = 7			;max no. of lines per plot

                ;**** Copy input values to common ****

    plotdev = device
    plotfile = hardname
    fileopen = 0
    nsum = nmsum
    gomenucom = gomenu

		;*********************************************
		;**** Generate indices to be sent to plot ****
		;**** routine if there are to be multiple ****
		;**** plots drawn			  ****
		;*********************************************

    nplot = 0
    if nmsum le ngpic then begin
	bottom = 0
	top = (nmsum-1)
	nplot = 1
    endif else begin
	bottom = 0
	top = (ngpic-1)
        if nmsum mod ngpic ne 0 then begin
            nplot = fix(nmsum/ngpic) + 1
        endif else begin
            nplot = fix(nmsum/ngpic)
        endelse
    endelse

                ;************************************
                ;**** Create general graph titles****
                ;************************************

    title = "POWER FUNCTION VS. ELECTRON TEMPERATURE"
    if ( strtrim(strcompress(utitle),2)  ne ' ' ) then begin
        title = title + ': ' + strupcase(strtrim(utitle,2))
    endif
    title =  title + '!C!CADAS    : ' + strupcase(header)
    if strcompress(dyear, /remove_all) eq '' then defyear = 'NONE' else $
    defyear = strcompress(dyear, /remove_all)
    year = strcompress(year, /remove_all)
    title =  title + '!CFILE     : ' + strcompress(dsfull) + 		$
    '   SPECIES: ' + strupcase(species) + '   YEAR: ' + year +		$
    '   DEFAULT YEAR: '+ defyear
    title =  title + '!CKEY     : (FULL LINE - TOTAL) (DASH LINE - PARTIAL)'
    
    rightstring = ''
    rightstring2 = ''
    for i=0, (nmsum-1) do begin
        if (i+1) lt 10 then rightstring = rightstring + ' '
        rightstring = rightstring + strtrim(string(i+1),2) + '!C'
        rightstring2 = rightstring2 + strupcase(poptit(i)) + '!C'
    endfor

                ;*************************************
                ;**** Create graph display widget ****
                ;*************************************

    graphid = widget_base(TITLE='ADAS409 GRAPHICAL OUTPUT', 		$
                          XOFFSET=1,YOFFSET=1)
    device, get_screen_size = scrsz
    xwidth=scrsz(0)*0.75
    yheight=scrsz(1)*0.75
    if nplot gt 1 then multiplot=1 else multiplot=0
    bitval = bitfile + '/menu.bmp'
    
    dmax=n_elements(y)    
    cwidg = cw_adas409_graph(graphid, dmax-1, print=hrdout, FONT=font,		$
                         xsize=xwidth, ysize=yheight, 			$
		         multiplot=multiplot, bitbutton=bitval)

                ;**** Realize the new widget ****

    widget_control, graphid, /realize

                ;**** Get the id of the graphics area ****

    widget_control, cwidg.cwid, get_value=grval
    win = grval.win

                ;*******************************************
                ;**** Put the graphing data into common ****
                ;*******************************************

    dens=0
    x_annot=fltarr(nsum+4,dmax)
    y_annot=fltarr(nsum+4,dmax)
    tmax=n_elements(x)
    ptot=fltarr(tmax,dmax,4)
    ptot(*,*,0)=prb
    ptot(*,*,1)=prc
    ptot(*,*,2)=plt
    ptot(*,*,3)=tot

    data = {	X	:	x,					$
		Y		:	y,					$	
		Z  		:	z,					$
		PTOT		:	ptot,					$
		XMIN2		:	xmin2,				$
		XMAX2		:	xmax2,				$
		ZMIN2		:	zmin2,				$
		ZMAX2		:	zmax2,				$
		RIGHTSTRING	:	rightstring,			$
		RIGHTSTRING2	:	rightstring2,		$
		TITLE		:	title,				$
		LDEF2		:	ldef2,				$
		TOP		:     top,					$
		BOTTOM	:	bottom,				$
		DENS		:     dens,					$
		XANN		:	x_annot,				$
		YANN		:	y_annot			}
		
                ;**** Initialise to plot 0 ****
    iplot = 0
    wset, win

    plot409_2, ldef2, top, bottom, x, y, z, ptot, $
               rightstring, rightstring2, 	$
               title, xmin2, xmax2, zmin2, zmax2, dens, win, $
               x_annot, y_annot

    data.xann=x_annot
    data.yann=y_annot

                ;***************************
                ;**** make widget modal ****
                ;***************************

    xmanager, 'adas409_2_plot', graphid, /modal, /just_reg,		$
              event_handler='adas409_2_plot_ev'

    gomenu = gomenucom
    densel = data.dens
                 
END
