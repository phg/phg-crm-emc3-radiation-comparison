; Copyright (c) 1997 Strathclyde University.
; SCCS info: Module @(#)$Header: /home/adascvs/idl/adas4xx/adas409/plot409_3.pro,v 1.1 2004/07/06 14:33:25 whitefor Exp $ Date $Date: 2004/07/06 14:33:25 $
;+
; PROJECT:
;       ADAS IBM MVS to DEC UNIX conversion
;
; NAME:
;       PLOT409_3
;
; PURPOSE:
;       Plot contribution function graphs for ADAS409.
;
; EXPLANATION:
;       This routine plots ADAS409 output for one or more plots (each
;       can contain a maximum of seven lines).
;
; USE:
;       Use is specific to ADAS409.  See adas409_3_plot.pro for
;       example.
;
; INPUTS:
;
;	LDEF3	-	Integer; 1 if user specified axis limits to 	
;			be used, 0 if default scaling to be used.
;
;	TOP	-	Integer; last line to plot
;
;	BOTTOM	-	Integer;first line to plot
;
;	XTEV	-	Fltarr; temperature values
;
;     XDENS       Fltarr; density values
;
;	GCFP	-	3d Fltarr; z-values to plot (3rd dimension between
;			BOTTOM and TOP)
;
;	GCF   - 	Generalised contribution function.
;                       1st dim:        temperature index
;                       2nd dim:        density index
;
;       RIGHTSTRING  -  String; Metastable indices label to side of graph
;
;       RIGHTSTRING2 -  String; Metastable designations label to side of graph
;
;       RIGHTSTRING3 -  String; Spectrum line specification headings label 
;			to side of graph
;
;       RIGHTSTRING4 -  String; Spectrum line specification information    
;			label to side of graph
;
;       RIGHTSTRING5 -  String; Contains all information to be extracted   
;			to make up RIGHTSTRING6-10
;
;       RIGHTSTRING6 -  String; Contains only linefeeds - used as label
;
;       RIGHTSTRING7 -  String; Contains only linefeeds - used as label
;
;       RIGHTSTRING8 -  String; Contains only linefeeds - used as label
;
;       RIGHTSTRING9 -  String; Contains only linefeeds - used as label
;
;       RIGHTSTRING10-  String; Contains only linefeeds - used as label
;
;	IPLINE	-	String array; indices of plotted lines (used
;			in labelling)
;
;       TITLE   -       String; heading to go above graph
;
;       XMIN3   -       Float; user-defined x-axis minimum
;
;       XMAX3   -       Float; user-defined x-axis maximum
;
;       zmin3   -       Float; user-defined y-axis minimum
;
;       zmax3   -       Float; user-defined y-axis maximum
;
;	DENS	-	Density index (selected by slider).
;
;	WIN	-	ID. no of X-device plot window.
;
; OPTIONAL INPUTS:
;       None.
;
; OUTPUTS:
;       xann - X value of plot labels.
;			1st dim: plot index
;			2nd dim: variable index (density)
;
;	  yann - Y value of plot labels.
;			1st dim: plot index
;			2nd dim: variable index (density) 
;
; OPTIONAL OUTPUTS:
;       None.
;
; KEYWORD PARAMETERS:
;       FONT    - String; Name of font passed to graphical output
;                 widget.
;
; CALLS:
;       None.
;
; SIDE EFFECTS:
;       None.
;
; CATEGORY:
;       Adas system.
;
; WRITTEN:
;       Alessandro Lanzafame, University of Strathclyde, 13th December 1995
;
; MODIFIED:
;	  1.1	Richard Martin
;		Updated to copy empty graph to pixmap window, so as to be
;		used in conjuction with cw_adas409_graph.pro with slider.
;		Also coordinates for labels to all available plots written
;		to an array.
;		Put under SCCS control.
;
; VERSION:
;	  1.1	11-03-98
;
;-
;----------------------------------------------------------------------------

PRO plot409_3, ldef3, top, bottom, $
		xtev, xdens, gcfp, gcf,         $
		rightstring, rightstring2, 	$
		rightstring3, rightstring4, rightstring5, rightstring6,	$
		rightstring7, rightstring8, rightstring9, rightstring10, $
		ipline, title, xmin3, xmax3, zmin3, zmax3, dens, win,	$
		xann, yann

  COMMON Global_lw_data, left, right, tp, bot, grtop, grright

                ;**** Suitable character size for current device ****
                ;**** Aim for 60 characters in y direction.      ****

	x=xtev
	y=gcf
	z=gcfp
	
    charsize = (!d.y_vsize/!d.y_ch_size)/60.0
    
    x_annot=xann
    y_annot=yann

                ;**** Initialise titles ****

    xtitle = 'ELECTRON TEMPERATURE (eV)'
    ytitle = 'CONTRIBUTION FUNCTION (cm!E3!N s!E-1!N)'
    righthead = '-- METASTABLE ASSIGNMENTS --!C!C'+			$
                '  INDEX         DESIGNATION  '

    npts = n_elements(x)

    nypts = size(y)
    nypts = nypts(1)    
     
		;**** Find x and y ranges for auto scaling,        ****
                ;**** check x and y in range for explicit scaling. ****

    makeplot = 1
    style = 0
    ystyle = 0
    yplot = y(*, bottom:top)
    zplot = z(*,*, bottom:top)
    
    if ldef3 eq 0 then begin

                ;**** identify values in the valid range ****
                ;**** plot routines only work within ****
                ;**** single precision limits.       ****

	xvals = where (x gt 1.0e-37 and x lt 1.0e37)
	yvals = where (yplot gt 1.0e-37 and yplot lt 1.0e37)
	zvals = where (zplot gt 1.0e-37 and zplot lt 1.0e37)
	if xvals(0) gt -1 then begin
	    maxx = max(x(xvals))
	    minx = min(x(xvals))
	endif else begin
	    makeplot = 0
	endelse
	if yvals(0) gt -1 or zvals(0) gt -1 then begin
	    if yvals(0) gt -1 and zvals(0) eq -1 then begin
	        maxz = max(yplot(yvals))
	        minz = min(yplot(yvals))	        
	    endif else if yvals(0) eq -1 and zvals(0) gt -1 then begin
		maxz = max(z(zvals))
		minz = min(z(zvals))		
	    endif else begin
                maxz = max([max(yplot(yvals)), max(z(zvals))])
                minz = min([min(yplot(yvals)), min(z(zvals))])
	    endelse
	endif else begin
	    makeplot = 0
	endelse
	if makeplot eq 1 then begin
	    if minz le 1.0e-36 then begin
		ystyle = 1
		minz = 1.0e-36
	    endif else begin
		ystyle = 0
	    endelse
	endif
	style = 0
    endif else begin
      minx = xmin3
	maxx = xmax3
	minz = zmin3
	maxz = zmax3
	xvals = where(x gt minx and x lt maxx)
	yvals = where(yplot gt minz and yplot lt maxz)
	zvals = where(zplot gt minz and zplot lt maxz)
	if xvals(0) eq -1 or (yvals(0) eq -1 and zvals(0) eq -1) then begin
	    makeplot = 0
	endif else begin
	    makeplot = 1
	endelse
	style = 0
        ystyle = 1
    endelse

    if makeplot eq 1 then begin

                ;**** Set up log-log plotting axes ****

	plot_oo, [minx, maxx], [minz,maxz], /nodata, ticklen=1.0,	$
                 position=[left, bot, grright, grtop],			$
                 xtitle=xtitle, ytitle=ytitle, xstyle=style, 		$
		 ystyle=ystyle, charsize=charsize

;*************** Construct component parameters strings ********************

    number = n_elements(ipline)
    icstring = strarr(number,5)
    startpos = 0
    length = 1
    i = 0
    j = 0
    rightstring5 = rightstring5 + ' '
    while (startpos lt (strlen(rightstring5)-1)) do begin
        extract = strmid(rightstring5, startpos+length-1, 1)
	if extract eq ' ' then begin
	    if length gt 1 then begin
	        stringval = strmid(rightstring5, startpos, length-1)
                icstring(i,j) = stringval
   	        j = j + 1
	        if j eq 5 then begin
	            j = 0
	            i = i + 1
                endif
                startpos = startpos + length - 1
                length = 1
	    endif else begin
	        startpos = startpos + 1
	    endelse
	endif else begin
	    length = length + 1
	endelse
    endwhile
    rightstring6 = rightstring6 + '!C!C!C!C  IC!C'
    rightstring7 = rightstring7 + '!C!C!C!C  IZ!C'
    rightstring8 = rightstring8 + '!C!C!C!C  IM!C'
    rightstring9 = rightstring9 + '!C!C!C!C   IP!C'
    rightstring11 = rightstring10
    rightstring10 = rightstring10 + '!C!C!C!C  IF!C'
    rightstring11 = rightstring11 + '!C!C!C!C INDX!C'
    for i=0, number-1 do begin
   	if fix(icstring(i,0)) lt 10 then begin
	    rightstring6 = rightstring6 + '!C  ' + 			$
            strtrim(string(icstring(i,0)),2)
	endif else begin
            rightstring6 = rightstring6 + '!C ' +                      	$
            strtrim(string(icstring(i,0)),2)
	endelse
    endfor
    for i=0, number-1 do begin
   	if fix(icstring(i,1)) lt 10 then begin
	    rightstring7 = rightstring7 + '!C  ' + 			$
            strtrim(string(icstring(i,1)),2)
	endif else begin
            rightstring7 = rightstring7 + '!C ' +                      	$
            strtrim(string(icstring(i,1)),2)
	endelse
    endfor
    for i=0, number-1 do begin
   	if fix(icstring(i,2)) lt 10 then begin
	    rightstring8 = rightstring8 + '!C  ' + 			$
            strtrim(string(icstring(i,2)),2)
	endif else begin
            rightstring8 = rightstring8 + '!C ' +                      	$
            strtrim(string(icstring(i,2)),2)
	endelse
    endfor
    for i=0, number-1 do begin
        iplength = strlen(strtrim(icstring(i,3),2))
   	if iplength eq 1 then begin
	    rightstring9 = rightstring9 + '!C    ' + 			$
            strtrim(string(icstring(i,3)),2)
	endif else if iplength eq 2 then begin
            rightstring9 = rightstring9 + '!C   ' +                    	$
            strtrim(string(icstring(i,3)),2)
	endif else if iplength eq 3 then begin
            rightstring9 = rightstring9 + '!C  ' +                    	$
            strtrim(string(icstring(i,3)),2)
	endif else if iplength eq 4 then begin
            rightstring9 = rightstring9 + '!C ' +                    	$
            strtrim(string(icstring(i,3)),2)
	endif else begin
            rightstring9 = rightstring9 + '!C' +                    	$
            strtrim(string(icstring(i,3)),2)
	endelse
    endfor
    for i=0, number-1 do begin
   	if fix(icstring(i,4)) lt 10 then begin
	    rightstring10 = rightstring10 + '!C  ' + 			$
            strtrim(string(icstring(i,4)),2)
	endif else begin
            rightstring10 = rightstring10 + '!C ' +                    	$
            strtrim(string(icstring(i,4)),2)
	endelse
    endfor
    for i=0, number-1 do begin
   	if fix(ipline(i)) lt 10 then begin
	    rightstring11 = rightstring11 + '!C  ' + 			$
            strtrim(string(ipline(i)),2)
	endif else begin
            rightstring11 = rightstring11 + '!C ' +                    	$
            strtrim(string(ipline(i)),2)
	endelse
    endfor

                ;**** Output title above graphs ****
    
    xyouts, 0.1, 0.9, title, charsize=charsize, /normal

                ;**** Output titles to right of graphs ****

    xyouts, 0.72, 0.8,  righthead, 	   charsize=charsize, /normal
    xyouts, 0.74, 0.72, rightstring,   charsize=charsize, /normal
    xyouts, 0.84, 0.72, rightstring2,  charsize=charsize, /normal
    xyouts, 0.72, 0.72, rightstring3,  charsize=charsize, /normal
    xyouts, 0.84, 0.72, rightstring4,  charsize=charsize, /normal
    xyouts, 0.72, 0.72, rightstring6,  charsize=charsize, /normal
    xyouts, 0.76, 0.72, rightstring7,  charsize=charsize, /normal
    xyouts, 0.80, 0.72, rightstring8,  charsize=charsize, /normal
    xyouts, 0.84, 0.72, rightstring9,  charsize=charsize, /normal
    xyouts, 0.89, 0.72, rightstring10, charsize=charsize, /normal
    xyouts, 0.93, 0.72, rightstring11, charsize=charsize, /normal
    
;******************************************************************************
                ;**** Output chosen density to right of graphs ****
     xyouts, 0.76, 0.35, '-ELECTRON DENSITY-', charsize=charsize, /normal        

	; Copy empty graph to pixmap window

	if (!d.name eq 'X') then begin
		wset, win
		xwidth=!d.x_size
		yheight=!d.y_size	
		window, 9, /pixmap, xsize=xwidth, ysize=yheight
		device, copy=[0,0,xwidth,yheight,0,0,win]
		wset, win	
	endif
	
     densstring=strtrim(string(dens+1),2)+': '+strtrim(string(xdens(dens)),2)+' cm-3'
     xyouts, 0.75, 0.31,densstring, charsize=charsize, /normal

                ;*********************************
                ;**** Make and annotate plots ****
                ;*********************************

	
	if yvals(0) gt -1 then begin
 	    for i=bottom,top do begin
	        zmaxchk = max(z(*,*,i))		;check this line is okay to 
							;draw
							
	        if zmaxchk le 1.0e-36 then begin
		    print, '******************************* D9OTG1 MESSAGE '+$
                           '*******************************'
		    print, 'METASTABLE: ', i+1, ' NO GRAPH WILL BE OUTPUT ' +$
		           'BECAUSE:'
		    print, 'ALL VALUES ARE BELOW THE CUTOFF OF 1.000E-30'
		    print, '******************************* END OF MESSAGE '+$
                           '*******************************'
	            endif else begin
	       
	      oplot, x, z(*,dens,i), linestyle=2
	      	      
	      for idens= 0, (nypts-1) do begin

                ;**** Find suitable point for annotation ****

                if (minz eq 10^(!y.crange(0))) then begin
	                if (x(npts-1) ge minx) and (x(npts-1) le maxx) and $
                          (z(npts-1,idens,i) gt minz) and 			 $
                          (z(npts-1,idens,i) lt 10^(!y.crange(1))) then begin 		 
		              iplot = npts - 1
	                endif else begin
		            iplot = -1
		            for id = 0, npts-2 do begin
		                if (x(id) ge minx) and (x(id) le maxx) and   $
		                  (z(id,idens,i) gt minz) and 			 $
                                  (z(id,idens,i) le maxz) then begin
			            iplot = id
		                endif
	                    endfor
	                endelse
		    endif else begin
                        if (x(npts-1) ge minx) and (x(npts-1) le maxx) and  $
                           (z(npts-1,idens,i) ge minz) and $
                           (z(npts-1,idens,i) lt 10^(!y.crange(1))) then begin $
                          iplot = npts - 1
                        endif else begin
                            iplot = -1
                            for id = 0, npts-2 do begin
                                if (x(id) ge minx) and (x(id) le maxx) and  $
                                      (z(id,idens,i) ge minz) and $
                                      (z(id,idens,i) le maxz) then begin
                                  iplot = id
                                endif
                            endfor
                        endelse
		    endelse
    
                ;**** store coords for annotation ****

 	            if iplot ne -1 then begin
			  x_annot(i+1,idens)=x(iplot)
			  y_annot(i+1,idens)=z(iplot,idens,i)
	            endif
 	            
    
                ;**** Annotate plot with level numbers ****
			if (idens eq dens) then begin
 	             if iplot ne -1 then begin
		        xyouts, x(iplot), z(iplot,dens,i), 			$
                        string(i+1, format='(i2)'), alignment=0.5
	             endif
	            endif
		    endfor

	        endelse
	    endfor
        endif

	     ;**** Put in the total plot ****

     if yvals(0) gt -1 then begin
	    ymaxchk = max(y(*)) 		;check this line is okay to
	      					;draw
	    if ymaxchk lt 1.0e-30 then begin
	        print, '******************************* D9OTG1 MESSAGE '+$
	      	  '*******************************'
	        print, 'NO GRAPH WILL BE OUTPUT FOR THE TOTAL'   +      $
	      	  'BECAUSE:'
	        print, 'ALL VALUES ARE BELOW THE CUTOFF OF 1.000E-30'
	        print, '******************************* END OF MESSAGE '+$
	      	  '*******************************'
	    endif else begin
		
	    for idens= 0, (nypts-1) do begin		
		    oplot, x, y(*,dens), linestyle=0

		    ;**** Find suitable point for annotation ****

	     iplot = -1
	     for id = npts-2, fix(npts/2), -1 do begin
		   if (x(id) ge minx) and (x(id) le maxx) and	     $
		    (y(id) ge minz) and (y(id) le maxz) then begin
		     iplot = id
		   endif
	     endfor
	     if iplot ne -1 then begin
			  x_annot(0,idens)=x(iplot)
			  y_annot(0,idens)=y(iplot,idens)
		   xyouts, x(iplot), y(iplot), 'TOT'
	     endif
	    endfor
	 endelse	   
	endif

    endif else begin			;no plot possible
	xyouts, 0.2, 0.5, /normal, charsize=charsize*1.5,		$
                '---- No data lies within range ----'
    endelse

    xann=x_annot
    yann=y_annot

END
