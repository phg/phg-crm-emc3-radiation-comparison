; Copyright (c) 1997 Strathclyde University .
; SCCS info: Module @(#)$Header: /home/adascvs/idl/adas4xx/adas410/daispf.pro,v 1.2 2004/07/06 13:27:29 whitefor Exp $ Date $Date: 2004/07/06 13:27:29 $
; NAME:
;	DAISPF
;
; PURPOSE:
;	IDL user interface and communications with ADAS410 FORTRAN
;	process via pipe.
;
; EXPLANATION:
;	The routine begins by reading information from the ADAS410
;	FORTRAN process via a UNIX pipe.  Then part of the ADAS410
;	IDL user interface is invoked to determine how the user
;	wishes to process the input dataset.  When the user's
;	interactions are complete the information gathered with
;	the user interface is written back to the FORTRAN process
;	via the pipe.  Communications are to the FORTRAN subroutine
;	DAISPF.
;
; USE:
;	The use of this routine is specific to ADAS410, see adas410.pro.
;
; INPUTS:
;	PIPE	- The IDL unit number of the bi-directional pipe to the
;		  ADAS410 FORTRAN process.
;
;	PROCVAL	- A structure which determines the initial settings of
;		  the processing options widget.  The initial value is
;		  set in adas410.pro.  If adas410.pro passes a blank 
;		  dummy structure of the form {NMET:-1} into PROCVAL then
;		  PROCVAL is reset to a default structure.
;
;		  The PROCVAL structure is;
;       procval = {	nmet  	: 	0 ,             		$
;			title 	: 	'',				$
;                	ifout 	: 	1, 				$
;			iopt 	: 	1,				$
;			maxt  	: 	0,              		$
;			tin   	: 	rec_temp_arr,			$
;			ifsel 	: 	0,				$
;			tolval	: 	5                		}
;
;		  See cw_adas410_proc.pro for a full description of this
;		  structure.
;	
;
;	DSFULL	- String; The full system file name of the input 
;		  dataset selected by the user for processing.
;
;       BITFILE - String; the path to the dirctory containing bitmaps
;                 for the 'escape to series menu' button.
;
; OPTIONAL INPUTS:
;	None
;
; OUTPUTS:
;	PROCVAL	- On output the structure records the final settings of
;		  the processing selections widget if the user pressed the
;		  'Done' button, otherwise it is not changed from input.
;
;	LPEND	- Integer, 0 if the user pressed the 'Done' button
;		  or 1 if the user pressed 'Cancel'.  Maps directly onto
;		  the logical variable LPEND in ADAS410 FORTRAN.
;
;       GOMENU  - Int; flag - set to 1 if user has selected 'escape direct
;                 to series menu , otherwise 0
;
; OPTIONAL OUTPUTS:
;	None
;
; KEYWORD PARAMETERS:
;	FONT_LARGE - The name of a larger font.
;
;	FONT_SMALL - The name of a smaller font.
;
;	EDIT_FONTS - A structure of two fonts used in the table editor
;		     adas_edtab.pro, {font_norm:'',font_input:''}.
;
; CALLS:
;	ADAS410_PROC	Invoke the IDL interface for ADAS410 data
;		 	processing options/input.
;
; SIDE EFFECTS:
;	Two way communications with ADAS410 FORTRAN via the
;	bi-directional UNIX pipe.
;
; CATEGORY:
;	Adas system.
;
; WRITTEN:
;       Martin O'Mullane,  10-9-97, based on e5ispf.pro
; 
; MODIFIED: 
;	1.2 	Martin O'Mullane 
;		Various modifications.
;
; VERSION:
;	1.2	20-02-98	
;
;-
;-----------------------------------------------------------------------------

PRO daispf, pipe, lpend, procval, dsfull, gomenu, bitfile,		$
            FONT_LARGE=font_large, FONT_SMALL=font_small, 		$
            EDIT_FONTS=edit_fonts


                ;**** Set defaults for keywords ****

    IF NOT (KEYWORD_SET(font_large)) THEN font_large = ''
    IF NOT (KEYWORD_SET(font_small)) THEN font_small = ''
    IF NOT (KEYWORD_SET(edit_fonts)) THEN 				$
    edit_fonts = {font_norm:'', font_input:''}

		;*********************************************
		;****     Declare variables for input     ****
		;**** arrays will be declared after sizes ****
		;**** have been read.                     ****
                ;*********************************************

    lpend = 0
    ntdim = 0
    ndtin = 0
    ita   = 0
    input = 0

		;********************************
		;**** Read data from fortran ****
		;********************************

; ion charges

    readf, pipe, input
    iz  = input
    readf, pipe, input
    iz0 = input
    readf, pipe, input
    iz1 = input


; temperatures

    readf, pipe, input
    ntdim = input
    readf, pipe, input
    ndtin = input
    readf, pipe, input
    ita = input

; Auger rates

    readf, pipe, input
    ndaug = input
    readf, pipe, input
    naug = input

; resolved DR

    readf, pipe, input
    ndprnt = input
    readf, pipe, input
    ndlev = input
    readf, pipe, input
    nprnt = input
    readf, pipe, input
    il = input

; representative n-shells DR

    readf, pipe, input
    kdrep = input
    readf, pipe, input
    krep = input
    readf, pipe, input
    ndrep = input
    readf, pipe, input
    nrep = input

		;************************************
		;**** Now can define array sizes ****
		;************************************
  
    tvals  = dblarr(ntdim,3)

    if naug ne 0 then begin
       caug   = strarr(naug)
       iaug   = intarr(nrep,naug)
    endif else begin
       caug   = strarr(1)
       iaug   = intarr(nrep,1)
    endelse
    
    cprnt  = strarr(nprnt)
    clev   = strarr(il)
    idielr = intarr(il,nprnt)
    if nprnt eq 1 then idielr = reform(idielr,il,1,/overwrite)

    crep   = strarr(krep)
    cnrep  = strarr(nrep+1)              ; to account for total in position 1
    idieln = intarr(nrep+1,krep)

		;********************************
                ;**** Read data from fortran ****
                ;********************************

    next_item = 0.0
    next_int  = 0
    sdum      = ' '

    for j = 0, 2 do begin
        for i = 0, ita-1 do begin
            readf, pipe, next_item
            tvals(i,j) = next_item
        endfor
    endfor

    if naug ne 0 then begin
       for i = 0, naug-1 do begin
           readf, pipe, sdum
           caug(i) = sdum
       endfor

       for j = 0, naug-1 do begin
           for i = 0, nrep-1 do begin
               readf, pipe, next_int
               iaug(i,j) = next_int
           endfor
       endfor
    endif else begin
       readf, pipe, sdum
       caug(0) = sdum
       for i = 0, nrep-1 do begin
           readf, pipe, next_int
           iaug(i,0) = next_int
       endfor
    endelse


    for i = 0, nprnt-1 do begin
        readf, pipe, sdum
        cprnt(i) = sdum
    endfor

    for i = 0, il-1 do begin
        readf, pipe, sdum
        clev(i) = sdum
    endfor

    for j = 0, nprnt-1 do begin
        for i = 0, il-1 do begin
            readf, pipe, next_int
            idielr(i,j) = next_int
        endfor
    endfor

    
    for i = 0, krep-1 do begin
        readf, pipe, sdum
        crep(i) = sdum
    endfor
    
    for i = 0, nrep do begin         ; nrep -1 +1
        readf, pipe, sdum
        cnrep(i) = sdum
    endfor

    for j = 0, krep-1 do begin
        for i = 0, nrep do begin     ; nrep -1 +1
            readf, pipe, next_int
            idieln(i,j) = next_int
        endfor
    endfor
		;*******************************************
		;**** Set default value if non provided ****
		;*******************************************
		
    if (procval.nmet lt 0) then begin
       str0_arr = strarr(ndaug)
       str1_arr = strarr(ndprnt)
       str2_arr = strarr(ndlev)
       str3_arr = strarr(kdrep)
       str4_arr = strarr(ndrep+1)
       temp_arr = dblarr(ndtin)
       procval  = {	nmet	:	0 ,				$
        		title 	: 	'',				$
			naug 	: 	0,			      	$
			nprnt 	: 	0,			      	$
			il    	: 	0,			      	$
			krep    :	0,				$
			nrep	:	0,				$
			ip_aug	: 	0,			      	$
			ip_res	: 	0,			      	$
			il_res	: 	0,			      	$
			ips_rep	: 	0,			      	$
			in_rep	: 	0,			      	$
			uaug 	: 	str0_arr,		        $
			uprnt 	: 	str1_arr,		        $
			ulev  	: 	str2_arr,		       	$
			urep 	: 	str3_arr,		        $
			unrep  	: 	str4_arr,		       	$
                	ifout 	: 	1, 				$
			iopt 	: 	0,				$
			maxt  	: 	0,              		$
			tin   	: 	temp_arr,			$
			ifsel 	: 	0,				$
			tolval	: 	5                		}
    endif

		;****************************************
		;**** Pop-up processing input widget ****
		;****************************************

  adas410_proc, procval, dsfull, iz, iz0, iz1,			$
		ntdim, ndtin, 				        $
		ita,  tvals, 					$
		ndaug, naug, caug, iaug,   			$
		ndprnt, ndlev, nprnt, il,  cprnt, clev, idielr,	$
		kdrep,  ndrep, krep, nrep, crep, cnrep, idieln,	$
		action, bitfile,		        	$
		FONT_LARGE=font_large, FONT_SMALL=font_small,   $
		EDIT_FONTS=edit_fonts



		;*******************************************
		;****  Act on the event from the widget ****
                ;**** There are three  possible actions ****
		;**** 'Done', 'Cancel'and 'Menu'.       ****
		;*******************************************

    if action eq 'Done' then begin
        lpend = 0
    endif else if action eq 'Menu' then begin
        lpend = 0
        gomenu = 1
    endif else begin
        lpend = 1
    endelse

		;*******************************
		;**** Write data to fortran ****
		;*******************************

    printf, pipe, lpend   
    title_short = strmid(procval.title,0,39) 	
    printf, pipe, title_short, format='(a40)' 
    printf, pipe, procval.ifout
    printf, pipe, procval.iopt
    printf, pipe, procval.ip_aug
    printf, pipe, procval.ip_res
    printf, pipe, procval.il_res
    printf, pipe, procval.ips_rep
    printf, pipe, procval.in_rep


    if procval.iopt ne 0 then begin
    
       printf, pipe, procval.maxt

       for i = 0, procval.maxt-1 do begin
           printf, pipe, procval.tin(i)
       endfor

       printf, pipe, procval.ifsel
       printf, pipe, procval.tolval
       
    endif

END
