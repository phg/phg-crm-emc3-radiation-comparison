; Copyright (c) 1998 Strathclyde University .
; SCCS info: Module @(#)$Header: /home/adascvs/idl/adas4xx/adas413/plot413.pro,v 1.1 2004/07/06 14:33:36 whitefor Exp $ Date $Date: 2004/07/06 14:33:36 $
;+
; PROJECT:
;       ADAS IBM MVS to   UNIX conversion
;
; NAME:
;	PLOT413
;
; PURPOSE:
;	Plot one graph for ADAS413.
;
; EXPLANATION:
;	This routine plots ADAS413 output for a single graph.
;
; USE:
;	Use is specific to ADAS413.  See adas413_plot.pro for
;	example.
;
; INPUTS:
;	X	- Double array; list of x values.
;
;	Y	- 2D double array; y values, 2nd index ordinary level,
;		  indexed by iord1 and iord2.
;
;	ITVAL   - Integer : Number of data points and spline fit points
;
;	NPLOTS  - Integer : Number of plots  1: no minimax fit, 2 : minimax fit
;
;	NMX     - Integer : Number of points used in polynomial fit.
;
;	TITLE	- String array : General title for program run. 5 lines.
;
;	XTITLE  - String : title for x-axis annotation
;
;	YTITLE  - String : title for y-axis annotation
;
;	STRG    - String array; 7 lines of info regarding data.
;
;	HEAD1   - String : header information for receiver data source.
;
;	HEAD2   - String : header information for donor data source.
;
;	TEVA    - fltarr : receiver temperature values - for display only.
;
;	LDEF1	- Integer; 1 if user specified axis limits to be used, 
;		  0 if default scaling to be used.
;
;	XMIN	- String; Lower limit for x-axis of graph, number as string.
;
;	XMAX	- String; Upper limit for x-axis of graph, number as string.
;
;	YMIN	- String; Lower limit for y-axis of graph, number as string.
;
;	YMAX	- String; Upper limit for y-axis of graph, number as string.
;
; OPTIONAL INPUTS:
;	None.
;
; OUTPUTS:
;	None.
;
; OPTIONAL OUTPUTS:
;	None.
;
; KEYWORD PARAMETERS:
;	None.
;
; CALLS:
;	None.
;
; SIDE EFFECTS:
;	None.
;
; CATEGORY:
;	Adas system.	
;
; WRITTEN:
;       H. P. Summers, 30-01-98
;
; MODIFIED:
;	1.1	H. P. Summers
;		First Version
;
; VERSION:
;	1.1	30-01-98
;
;----------------------------------------------------------------------------

PRO plot413, x , y, iopt, itval, nplots, nmx, title, xtitle, ytitle, 	$
             strg, head1, head2,  teva, qdr,  				$
	     ldef1, xmin, xmax, ymin, ymax

    COMMON Global_lw_data, left, right, top, bottom, grtop, grright

		;****************************************************
		;**** Suitable character size for current device ****
		;**** Aim for 60 characters in y direction.      ****
		;****************************************************

    charsize = (!d.y_vsize/!d.y_ch_size)/60.0 
    small_check = GETENV('VERY_SMALL')
    if small_check eq 'YES' then charsize=charsize*0.8

		;**** set makeplot counter ****

    makeplot = 1

		;***************************************
		;**** Construct graph title         ****
		;**** "!C" is the new line control. ****
		;***************************************

    !p.font=-1
    if small_check eq 'YES' then begin
        gtitle = title(0) + "!C!C" + title(1) + "!C!C" + title(2) +"!C!C" + $
	     title(3) + "!C!C" + title(4)
    endif else begin
        gtitle = title(0) + "!C!C" + title(1) + "!C" + title(2) + "!C" + $
          title(3) + "!C" + title(4)
    endelse

		;**** Find x and y ranges for auto scaling ****

    if ldef1 eq 0 then begin

		;********************************************
		;**** identify values in the valid range ****
		;**** plot routines only work within     ****
		;**** single precision limits.	         ****
		;********************************************

        xvals = where(x gt 1.0e-37 and x lt 1.0e37)
        yvals = where(y gt 1.0e-37 and y lt 1.0e37)
        if xvals(0) gt -1 then begin
            xmax = max(x(xvals), min=xmin)
            makeplot = 1
        endif else begin
            makeplot = 0
        endelse
        if yvals(0) gt -1 then begin
            ymax = max(y(yvals), min=ymin)
            makeplot = 1
        endif else begin
            makeplot = 0
        endelse
        style = 0
    endif else begin

		;Check that at least some data in in axes range ***
        xvals = where(x ge xmin and x le xmax)
        yvals = where(y ge ymin and y le ymax)
        if xvals(0) eq -1 or yvals(0) eq -1 then begin
          makeplot = 0
        endif
        style = 1
    endelse


		;**** Set up log-log plotting axes ****

    plot_oo, [xmin,xmax], [ymin,ymax], /nodata, ticklen=1.0, 		$
	     position=[left,bottom,grright,grtop], 			$
	     xtitle=xtitle, ytitle=ytitle, xstyle=style, ystyle=style, 	$
	     charsize=charsize

    if makeplot eq 1 then begin

		;**********************
		;****  Make plots  ****
		;**********************
	
        oplot, x(0,0:itval-1), y(0,0:itval-1), psym=7
        case nplots of
          1 : begin
                  oplot, x(0,*), y(0,*)        
              end
          2 : begin
                 oplot, x(1,*), y(1,*), linestyle=0
              end
          3 : begin
                 oplot, x(1,*), y(1,*), linestyle=0
                 oplot, x(2,*), y(2,*), linestyle=5
              end
        endcase
        
    endif else begin
        print, "ADAS413 : No data found in these axes ranges"
    endelse

		;**** Annotation to plot ****

    rhs = grright + 0.02

		;**** plot title ****

    if small_check eq 'YES' then begin
	charsize = charsize * 0.9
    endif

    xyouts, (left-0.05), (top+0.05), gtitle, /normal, alignment=0.0, 	$
	    charsize=charsize

		;**** right hand side labels ****

    cpp = grtop - 0.05
    xyouts, rhs, cpp, head1, /normal, alignment=0.0,			$
            charsize=charsize*0.8
    for i = 0, 3 do begin
        cpp = cpp - 0.02
  	xyouts, rhs, cpp, strcompress(strg(i)), /normal, alignment= 0.0, 		$
	        charsize=charsize*0.8
    endfor
    cpp = cpp - 0.05

    
       xyouts, rhs, cpp, head2, /normal, alignment=0.0,			$
               charsize=charsize*0.8
       chr_units='            (eV)                  (cm!E3!N s!E-1!N)'         
       cpp = cpp - 0.02
       xyouts, rhs, cpp, chr_units, /normal, alignment=0.0,			$
               charsize=charsize*0.7

       for i = 0, itval-1 do begin

           index = strtrim(strcompress(string(i+1)))
	   line = index
           if (i le 8) then line=" "+line + "      " else line=line + "      "

	   line =  line + 							$
           strcompress(string(teva(i),format='(e10.3)')) + "    "   +  $  
           strcompress(string(qdr(i) ,format='(e10.3)'))  

           cpp = cpp - 0.02
           xyouts, rhs, cpp, line, /normal, charsize=charsize*0.8

       endfor
       

END
