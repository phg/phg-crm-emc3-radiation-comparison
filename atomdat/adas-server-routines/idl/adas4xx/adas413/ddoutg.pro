; Copyright (c) 1998 Strathclyde University .
; SCCS info: Module @(#)$Header: /home/adascvs/idl/adas4xx/adas413/ddoutg.pro,v 1.1 2004/07/06 13:29:46 whitefor Exp $ Date $Date: 2004/07/06 13:29:46 $
;+
; PROJECT:
;       ADAS IBM MVS to UNIX conversion
;
; NAME:
;	DDOUTG
;
; PURPOSE:
;	Communication with ADAS413 FORTRAN via UNIX pipe and
;	graphics output.
;
; EXPLANATION:
;	The routine begins by reading information from the ADAS413
;	FORTRAN process via a UNIX pipe.  Then the IDL graphical
;	output routine for ADAS413 is invoked.  Communications are to
;	the FORTRAN subroutine DDOUTG.
;
; USE:
;	The use of this routine is specific to ADAS413 see adas413.pro.
;
; INPUTS:
;	DSFULL   - Data file name
;
;	PIPE	 - The IDL unit number of the bi-directional pipe to the
;		   ADAS413 FORTRAN process.
;
;       UTITLE   - Optional comment by user
;
;	GRPSCAL	 - Integer; 0 if default scaling required 1 if user
;		   specified scaling to be used.
;
;	XMIN	 - String; User sepcified x-axis minimum, number as string.
;
;	XMAX	 - String; User sepcified x-axis maximum, number as string.
;
;	YMIN	 - String; User specified y-axis minimum, number as string.
;
;	YMAX	 - String; User specified y-axis maximum, number as string.
;
;	HRDOUT	 - Integer; 0 if no hardcopy required, 1 if hardcopy.
;
;	HARDNAME - String; file name for hardcopy output.
;
;	DEVICE	 - String; IDL name for hardcopy output device.
;
;	HEADER	 - ADAS version header information for inclusion in the
;		   graphical output.
;
;       BITFILE - String; the path to the directory containing bitmaps
;                 for the 'escape to series menu' button.
;
; OPTIONAL INPUTS:
;	None.
;
; OUTPUTS:
;       GOMENU  - Int; flag - set to 1 if user has selected 'escape direct
;                 to series menu , otherwise 0
;
; OPTIONAL OUTPUTS:
;	None.
;
; KEYWORD PARAMETERS:
;	FONT	- String; Name of the font to be used for the graphical
;		  output widget.
;
; CALLS:
;	ADAS413_PLOT	ADAS413 graphical output.
;
; SIDE EFFECTS:
;	This routine reads information from FORTRAN via a UNIX pipe.
;
; CATEGORY:
;	Adas system.
;
; WRITTEN:
;       H. P. Summers, 30-01-98
;
; MODIFIED:
;	1.1	H. P. Summers
;		First Version
;
; VERSION:
;	1.1	30-01-98
;
;-----------------------------------------------------------------------------

PRO ddoutg, dsfull, pipe, utitle, grpscal, xmin, xmax, ymin, ymax, 	$
	    hrdout, hardname, device, header, bitfile, gomenu, FONT=font

                ;**** Set defaults for keywords ****

  IF NOT (KEYWORD_SET(font)) THEN font = ''


		;**** Declare variables for input ****

    title = " "
    titlx = " "
    titlm = " "
    date = " "
    iz0 = 0
    iz1 = 0
    itval = 0
    irep = 0
    ldef1 = 0
    teva = 0
    din = 0
    xmin = 0.0
    xmax = 0.0
    ymin = 0.0
    ymax = 0.0
    tdfitm = 0
    lfsel = 0
    nmx = 0
    head1 = " "
    head2 = " "
    strg = make_array(4, /string, value=" ")
    dummy = 0.0
    sdum = " "
    idum = 0

		;********************************
		;**** Read data from fortran ****
		;********************************

    readf, pipe, format = '(a40)' , sdum
    title = sdum
    readf, pipe, format = '(a120)' , sdum
    titlx = sdum
    readf, pipe, format = '(a80)' , sdum
    titlm = sdum
    readf, pipe, format = '(a8)' , sdum
    date = sdum
    
    readf, pipe, idum
    iz0 = idum
    readf, pipe, idum
    iz1 = idum 
 
    readf, pipe, idum
    iopt = idum 
    
       readf, pipe, idum 
       itval = idum

   ; now declare array dimensions ****

       teva = dblarr(itval) 
       qdr  = dblarr(itval)

       for i=0, itval-1 do begin
           readf, pipe, dummy
           teva(i) = dummy 
       endfor

       for i=0, itval-1 do begin
           readf, pipe, dummy
           qdr(i) = dummy 
       endfor
       
       readf, pipe, idum
       nmx = idum

       qdrs  = fltarr(nmx)
       tfevs = fltarr(nmx)
       for i = 0, nmx-1 do begin
           readf, pipe, dummy
           qdrs(i) = dummy 
       endfor
       for i = 0, nmx-1 do begin
           readf, pipe, dummy 
           tfevs(i) = dummy
       endfor  
                
    
    readf, pipe, idum
    ldef1 = idum
    if (ldef1 eq 1) then begin
        readf, pipe, dummy
	xmin = dummy
        readf, pipe, dummy
	xmax = dummy
        readf, pipe, dummy
	ymin = dummy
        readf, pipe, dummy
	ymax = dummy
    endif 


    readf, pipe, idum
    lfsel = idum

    if (lfsel eq 1) then begin
        readf, pipe, idum
	nmx = idum

 		;**** declare arrays ****

        qdrm  = fltarr(nmx)
        tfevm = fltarr(nmx)
        for i = 0, nmx-1 do begin
	    readf, pipe, dummy
            qdrm(i) = dummy 
        endfor
        for i = 0, nmx-1 do begin
 	    readf, pipe, dummy 
            tfevm(i) = dummy
        endfor
    endif

    for i = 0, 3 do begin
        readf, pipe, format='(a49)', sdum 
        strg(i) = sdum
    endfor
     
    readf, pipe, format = '(a32)', sdum
    head1 = sdum
    readf, pipe, format = '(a45)', sdum
    head2 = sdum

		;***********************
		;**** Plot the data ****
		;***********************

    adas413_plot, dsfull, 						$
  		  title , titlx, titlm, utitle, date, iz0, iz1, iopt,	$
		  irep, nrepa, auga, 					$
		  itval, teva, qdr, qdrm, tfevm, qdrs, tfevs, 		$
                  ldef1, xmin, xmax, ymin, ymax, 			$
   		  lfsel, nmx, 						$
   		  strg, head1, head2,					$
		  hrdout, hardname, device, header, bitfile, gomenu,	$
                  FONT=font

END
