; Copyright (c) 1998 Strathclyde University .
; SCCS info: Module @(#)$Header: /home/adascvs/idl/adas4xx/adas413/cw_adas413_proc.pro,v 1.3 2004/07/06 12:49:28 whitefor Exp $ Date $Date: 2004/07/06 12:49:28 $
;+
; PROJECT:
;       ADAS IBM MVS toi   UNIX conversion
;
; NAME:
;	CW_ADAS413_PROC()
;
; PURPOSE:
;	Produces a widget for ADAS413 processing options/input.
;
; EXPLANATION:
;	This function creates a compound widget consisting of :-
;	   a text widget holding an editable 'Run title', 
;	   the dataset name/browse widget cw_adas_dsbr,
;	   a widget to select the data block for analysis,
;	   a widget to request a mimax fit and enter a tolerance for it,
;	   a table widget for receiver/donor temperature data cw_adas_table,
;	   buttons to enter default values into the table, 
;	   a message widget, 
;	   an 'Escape to series menu', a 'Cancel' and a 'Done' button.
;
;	The compound widgets included in this widget are self managing.
;	This widget only manages events from the two 'Defaults' buttons,
;	the 'Cancel' button and the 'Done' button and the
;       'escape to series menu' button.
;
; USE:
;	This widget is specific to ADAS413, see adas413_proc.pro
;	for use.
;
; INPUTS:
;	TOPPARENT- Long integer, ID of parent widget.
;
;	DSFULL	- String; The full system file name of the input 
;		  dataset selected by the user for processing.
;
;
;	NTDIM	- Integer; maximum number of temperatures allowed.
;
;	NDTIN	- Integer; number of temperatures selected
;
;  	ITA  	- Integer : INPUT FILE - NO. OF TEMPERATURES (NSTORE)
;
;	TVALS   - Double array : Receiver electron temperatures - 
;				 1st dimension - temperature index
;				 2nd dimension - units 1 - Kelvin
;						       2 - eV
;
;	The inputs map exactly onto variables of the same
;	name in the ADAS413 FORTRAN program.
;
;       BITFILE - String; the path to the directory containing bitmaps
;                 for the 'escape to series menu' button.
;
; OPTIONAL INPUTS:
;	None
;
; OUTPUTS:
;	The return value of this function is the ID of the declared widget.
;
; OPTIONAL OUTPUTS:
;	None
;
; KEYWORD PARAMETERS:
;	PROCVAL	- A structure which determines the initial settings of
;		  the processing options widget.
;
;		  The PROCVAL structure is;
;       procval = {     nmet	: 	0,
;                       title 	: 	'',				$
;                	ifout 	: 	1, 				$
;			iopt 	: 	0,				$
;			maxt  	: 	0,              		$
;			tin   	: 	temp_arr,			$
;			ifsel 	: 	0,				$
;			tolval	: 	5                		}
;
;
;		NMET    Flag which defines whether or not default values
;			exist or not. (< 0 if no values exist)
; 		TITLE	Entered general title for program run
;		IFOUT   Index indicating which units are being used
;		IOPT    Flag for plot type. 1 = direct ionis, 2 = excit
;			to d/e levels, 3 = excit to pr./sp. sys. bundle-n. 
;			0 = receiver, 1 = donor
;		MAXT    Number of receiver temperature values selected
;		TIN     User supplied receiver temperature values for fit.
;		IFSEL   Flag as to whether polynomial fit is chosen
;		TOLVAL  Tolerance required for goodness of fit if
;			polynomial fit is selected.
;
;		All of these structure elements map onto variables of
;		the same name in the ADAS413 FORTRAN program.
;
;
;	UVALUE	   - A user value for the widget. Default 0.
;
;	FONT_LARGE - The name of a larger font.  Default current system
;		     font
;
;	FONT_SMALL - The name of a smaller font. Default current system
;		     font.
;
;	EDIT_FONTS - A structure of two fonts used in the table editor
;		     adas_edtab.pro, {font_norm:'',font_input:''}.  Defaults
;		     to current system font.
;
;	NUM_FORM   - String; Numeric format to use in tables.  Default
;			'(E10.3)'
;		     
;
; CALLS:
;	POPUP		Popup warning window with buttons.
;	CW_ADAS_DSBR	Dataset name and comments browsing button.
;	CW_ADAS_TABLE	Adas data table widget.
;	CW_ADAS_SEL	Adas multiple selection widget.
;	CW_SINGLE_SEL	Adas scrolling table and selection widget.
;	CW_OPT_VALUE    Adas option selection widget with required input 
;			value. 
;
;	See side effects for other related widget management routines.
;
; SIDE EFFECTS:
;
;	The following widget management routines are included in this file;
;	PROC413_GET_VAL()	Returns the current PROCVAL structure.
;	PROC413_EVENT()		Process and issue events.
;	
; CATEGORY:
;	Compound Widget
;
; WRITTEN:
;       H. P. Summers, 30-01-98
;
; MODIFIED:
;	1.1	H. P. Summers
;		First Version
;	1.2	Richard Martin
;		Changed widget name switch to switchb for IDL 5.4 compatibility. 
;	1.3	Richard Martin
;		IDL 5.5 fixes.
;
; VERSION:
;	1.1	24-03-98
;	1.2	10-11-00
;	1.3	24-01-02
;
;;-----------------------------------------------------------------------------

FUNCTION proc413_get_val, id

                ;**** Return to caller on error ****

    ON_ERROR, 2

		 ;***************************************
                 ;****     Retrieve the state 	     ****
		 ;**** Get first_child widget id     ****
		 ;**** because state is stored there ****
		 ;***************************************

    first_child = widget_info(id, /child)
    widget_control, first_child, get_uvalue = state

		;***********************************
		;**** Get run title from widget ****
		;**** Then centre in in string  ****
		;**** of 40 characters 		****
		;***********************************

    widget_control, state.runid, get_value=title
    title(0)=strtrim(title(0))
    title_len = strlen(title(0))
    if (title_len gt 40 ) then begin 
	title(0) = strmid(title(0),0,38)
        widget_control, state.messid, set_value="Title too long - truncated "
        widget_control, state.runid, set_value=title
	wait ,1
    endif
    pad = (40 - title_len)/2 
    spaces = '                                         '
    title(0) = strmid(spaces,0,(pad-1)) + title(0) + strmid(spaces,0,(pad-1))

		;****************************************
		;**** Get new data from table widget ****
		;****************************************

    widget_control, state.tempid, get_value=tempval
    tabledata = tempval.value
    ifout = tempval.units + 1


		;***********************
		;**** Get plot type ****
		;***********************


    widget_control, state.iopt1id, get_uvalue=temp
    if (temp(0) eq 1 ) then iopt = 0 
    widget_control, state.iopt0id, get_uvalue=temp
    if (temp(0) eq 1 ) then iopt = 1 
    widget_control, state.iopt2id, get_uvalue=temp
    if (temp(0) eq 1 ) then iopt = 2 



		;**** Copy out temperature values ****

    tin = dblarr(state.ndtin)
    blanks = where(strtrim(tabledata(0,*),2) eq '')

		;***********************************************
		;**** next line assumes that all blanks are ****
		;**** at the end of the columns             ****
		;***********************************************

    if blanks(0) ge 0 then maxt=blanks(0) else maxt=state.ntdim

		;*************************************************
                ;**** Only perform following if there is 1 or ****
                ;**** more value present in the table         ****
		;*************************************************

    if (maxt ge 1) then begin
        tin(0:maxt-1) = double(tabledata(0,0:maxt-1))
    endif

		;**** Fill in the rest with zero ****

    if maxt lt state.ntdim then begin
        tin(maxt:state.ntdim-1) = double(0.0)
    endif

		;*************************************************
		;**** Get selection of polyfit from widget    ****
		;*************************************************

    widget_control, state.optid, get_uvalue=polyset, /no_copy
    ifsel = polyset.optionset.option[0]
    if (num_chk(polyset.optionset.value[0]) eq 0) then begin
       tolval = (polyset.optionset.value[0])
    endif else begin
       tolval = -999
    endelse
    widget_control, state.optid, set_uvalue=polyset, /no_copy
    

		;***********************************************
		;**** get new index for data block selected ****
		;***********************************************

    widget_control, state.o1aid, get_value=select_block
    nlev_res = select_block[0]  
    widget_control, state.o1bid, get_value=select_block
    ip_res = fix(select_block[0])  

    widget_control, state.o0aid, get_value=select_block
    ip_aug = select_block[0]  
    widget_control, state.o0bid, get_value=select_block
    im_aug = fix(select_block[0])  

    widget_control, state.o2aid, get_value=select_block
    ips_rep = select_block[0]  
    widget_control, state.o2bid, get_value=select_block
    in_rep = fix(select_block[0])


		;***********************************************
		;**** write selected values to PS structure ****
		;***********************************************
;		  The PROCVAL structure is;
       ps =	 {	nmet  	: 	0 ,             		$
			title 	: 	title,				$
			naug 	: 	state.naug,		     	$
			nprf 	: 	state.nprf,		     	$
			nprfm 	: 	state.nprfm,		     	$
			npri 	: 	state.npri,		     	$
			nlev  	: 	state.nlev,		      	$
			nlevm  	: 	state.nlevm,		      	$
			iprfm  	: 	state.iprfm,		      	$
			ipri  	: 	state.ipri,		      	$
			imeti  	: 	state.imeti,		      	$
			krep 	: 	state.krep,		     	$
			nrcnt   : 	state.nrcnt,		      	$
			ip_aug	: 	ip_aug,			      	$
			im_aug	: 	im_aug,			      	$
			ip_res	: 	ip_res,			      	$
			nlev_res : 	nlev_res,		      	$
			ips_rep	: 	ips_rep,			$
			in_rep	: 	in_rep,			      	$
			uaug 	: 	state.uaug,		      	$
			uprnt 	: 	state.uprnt,		      	$
			ulev  	: 	state.ulev,		      	$
			urep 	: 	state.urep,		      	$
			unrep  	: 	state.unrep,		      	$
                	ifout 	: 	ifout, 				$
			iopt 	: 	iopt,				$
			maxt  	: 	maxt,              		$
			tin   	: 	tin,				$
			ifsel 	: 	ifsel,				$
			tolval	: 	tolval                		}

    widget_control, first_child, set_uvalue=state, /no_copy

    RETURN, ps

END

;-----------------------------------------------------------------------------

FUNCTION proc413_event, event

                ;**** Base ID of compound widget ****

    parent=event.handler

		;**********************************************
                ;**** Retrieve the user value state        ****
		;**** Get id of first_child widget because ****
		;**** user value "state" is stored there   ****
		;**********************************************

    first_child = widget_info(parent, /child)
    widget_control, first_child, get_uvalue=state,/no_copy
  
	
		;*********************************
		;**** Clear previous messages ****
		;*********************************

    widget_control, state.messid, set_value=' '

                ;************************
                ;**** Process events ****
                ;************************

    CASE event.id OF

	state.iopt1id: begin

	   widget_control, state.aug_id, map=0
	   widget_control, state.res_id, map=1
	   widget_control, state.rep_id, map=0
	   widget_control, state.iopt0id, set_uvalue = 0
	   widget_control, state.iopt1id, set_uvalue = 1
	   widget_control, state.iopt2id, set_uvalue = 0
	   widget_control, state.tempid, sensitive=1
	   widget_control, state.deftid, sensitive=1
	   widget_control, state.optid,  sensitive=1
	   state.iopt = 0

	end	
	  	
	state.iopt0id: begin

	   widget_control, state.aug_id, map=1
	   widget_control, state.res_id, map=0
	   widget_control, state.rep_id, map=0
	   widget_control, state.iopt0id, set_uvalue = 1
	   widget_control, state.iopt1id, set_uvalue = 0
	   widget_control, state.iopt2id, set_uvalue = 0
	   widget_control, state.tempid, sensitive=1
	   widget_control, state.deftid, sensitive=1
	   widget_control, state.optid,  sensitive=1
	   state.iopt = 1

	end	
	  	
	state.iopt2id: begin

	   widget_control, state.aug_id, map=0
	   widget_control, state.res_id, map=0
	   widget_control, state.rep_id, map=1
	   widget_control, state.iopt0id, set_uvalue = 0
	   widget_control, state.iopt1id, set_uvalue = 0
	   widget_control, state.iopt2id, set_uvalue = 1
	   widget_control, state.tempid, sensitive=1
	   widget_control, state.deftid, sensitive=1
	   widget_control, state.optid,  sensitive=1
	   state.iopt = 2

	end
	

; excitation to autoionising levels
	
	state.o0aid: begin

	   widget_control, first_child, set_uvalue=state, /no_copy
	   widget_control, event.handler, get_value=ps

		;*** reset state variable ***

	   widget_control, first_child, get_uvalue=state, /no_copy
	   
	   irexc  = state.irexc
	   ipsel  = ps.ip_aug 
	   ilsel  = ps.im_aug 
	   	   
           valid_str  = ['   No  ','   Yes ']
           clev = state.ulev

           for j=0,state.nlev-1 do begin
	      str_tmp=strmid(clev(j),0,21)+"       "
              strput,str_tmp,valid_str(irexc(ipsel,j)),22
              case 1 of
                (j ge  0) and (j le   8) : str_ind = "  "+strtrim(string(j+1),1)
                (j ge  9) and (j le  98) : str_ind = " " +strtrim(string(j+1),1)
                (j ge 99) and (j le 998) : str_ind =      strtrim(string(j+1),1)
              else : print,'Too many levels',j
              endcase
              clev(j)=str_ind+"    "+str_tmp
           endfor
             	   
	   tempval=strarr(state.nlev+1)
	   tempval(0)=string(-1)
	   tempval(1:state.nlev)=clev
	   
           widget_control, state.o0bid, set_value=tempval
	   
	end
	
	state.o0bid: begin

	   widget_control, first_child, set_uvalue=state, /no_copy
	   widget_control, event.handler, get_value=ps

		;*** reset state variable ***

	   widget_control, first_child, get_uvalue=state, /no_copy
	   
	   irexc  = state.irexc
	   ipsel  = ps.ip_aug 
	   ilsel  = ps.im_aug 

; inform user if incorrect choice --- only enforce change at Done
           
	   if (irexc(ipsel,ilsel) eq 0 ) then begin
	     
	     mess_str = ['Invalid choice of meta. level and d/exc.level', $
	                 'excit. rates not available in dataset ', $
	                 ' ',					$
	                 'Choose another pair               ']
	     action = popup(message=mess_str,$
		          buttons=['Confirm'], font=state.font)
		          

	   endif            
	   	   
	end

	  	
; direct ionisation
	
	state.o1aid: begin

	   widget_control, first_child, set_uvalue=state, /no_copy
	   widget_control, event.handler, get_value=ps

		;*** reset state variable ***

	   widget_control, first_child, get_uvalue=state, /no_copy
	   
	   irion  = state.irion
	   ipsel  = ps.ip_res 
	   ilsel  = ps.nlev_res 
	   	   
           valid_str  = ['   No  ','   Yes ']
           cprf = state.uprnt

           for j=0,state.nprf-1 do begin
	      str_tmp=strmid(cprf(j),0,21)+"       "
              strput,str_tmp,valid_str(irion(ilsel,j)),22
              case 1 of
                (j ge  0) and (j le   8) : str_ind = "  "+strtrim(string(j+1),1)
                (j ge  9) and (j le  98) : str_ind = " " +strtrim(string(j+1),1)
                (j ge 99) and (j le 998) : str_ind =      strtrim(string(j+1),1)
              else : print,'Too many levels',j
              endcase
              cprf(j)=str_ind+"    "+str_tmp
           endfor
             	   
	   tempval=strarr(state.nprf+1)
	   tempval(0)=string(-1)
	   tempval(1:state.nprf)=cprf
	   
           widget_control, state.o1bid, set_value=tempval
	   
	end
	
	state.o1bid: begin

	   widget_control, first_child, set_uvalue=state, /no_copy
	   widget_control, event.handler, get_value=ps

		;*** reset state variable ***

	   widget_control, first_child, get_uvalue=state, /no_copy
	   
	   irion  = state.irion
	   ipsel  = ps.ip_res 
	   ilsel  = ps.nlev_res 

; inform user if incorrect choice --- only enforce change at Done
           
	   if (ilsel ne -1 and ipsel ne -1) then begin
	      if (irion(ilsel,ipsel) eq 0 ) then begin
	     
	         mess_str = ['Invalid choice of level and parent',      $
	                     'ionis. rates not available in dataset ',  $
	                     ' ',					$
	                     'Choose another pair               ']
	         action = popup(message=mess_str,			$
		          buttons=['Confirm'], font=state.font)
		          
	      endif            
	   endif            
	   	   
	end

; representative n-shell excitation

	state.o2aid: begin

        if (state.krep gt 0) then begin

	   widget_control, first_child, set_uvalue=state, /no_copy
	   widget_control, event.handler, get_value=ps

		;*** reset state variable ***

	   widget_control, first_child, get_uvalue=state, /no_copy
	   
	   iexcn = state.iexcn
	   ipsel  = ps.ips_rep
	   insel  = ps.in_rep

           valid_str  = ['   No  ','   Yes ']
           cnrep = state.unrep
           for j=0,state.nrcnt(ipsel)-1 do begin
	      str_tmp=strmid(cnrep(j,ipsel),0,6)+"          "
              strput,str_tmp,valid_str(iexcn(j,ipsel)),10
              case 1 of
                (j ge  0) and (j le   8) : str_ind = "  "+strtrim(string(j+1),1)
                (j ge  9) and (j le  98) : str_ind = " " +strtrim(string(j+1),1)
                (j ge 99) and (j le 998) : str_ind =      strtrim(string(j+1),1)
              else : print,'Too many levels',j
              endcase
              cnrep(j,ipsel)=str_ind+"    "+str_tmp
           endfor
             	   
	   tempval=strarr(state.nrcnt(ipsel)+1)
	   tempval(0)=string(-1)
	   tempval(1:state.nrcnt(ipsel))=cnrep(0:state.nrcnt(ipsel)-1,ipsel)
	   
           widget_control, state.o2bid, set_value=tempval

        endif
	   
	end
	
	state.o2bid: begin

        if (state.krep gt 0) then begin

	   widget_control, first_child, set_uvalue=state, /no_copy
	   widget_control, event.handler, get_value=ps

		;*** reset state variable ***

	   widget_control, first_child, get_uvalue=state, /no_copy
	   
	   iexcn = state.iexcn
	   ipsel  = ps.ips_rep
	   insel  = ps.in_rep
           
	   if (iexcn(insel,ipsel) eq 0 ) then begin
	     
	     mess_str = ['Invalid choice Parent/Spin system and n-shell', $
	                 'Excit rates not available in dataset         ', $
	                 ' ',					          $
	                 'Choose another pair                           ']
	     action = popup(message=mess_str,$
		          buttons=['Confirm'], font=state.font)
		          
	   endif
            
        endif
	   	   
	end

		;*************************************
		;**** Default temperature button ****
		;*************************************

    	state.deftid: begin

		;**** popup window to confirm overwriting current values ****

	    action= popup(message='Confirm Overwrite values with Defaults',$
		          buttons=['Confirm','Cancel'], font=state.font)

	    if action eq 'Confirm' then begin

		;**** Get current table widget value ****

 	       	widget_control, state.tempid, get_value=tempval

               	units = tempval.units+1
	    	tvalues = strarr(state.maxt)
	        for i = 0, state.maxt-1 do begin
                  tvalues(i) =  strcompress(string(tempval.value(units,i)))
                endfor

		                  
		;***********************************************
		;**** Copy defaults into value structure    ****
		;**** For default values use the existing   ****
		;**** table values and let user modify them ****
		;**** then set all donor values to same     ****
		;**** value  				    ****
		;***********************************************

                deft = tempval
	        maxt = state.maxt
 	        if maxt gt 0 then begin
   	            tempval.value(0,0:maxt-1) =                     $
		    strtrim(string(deft.value(units,0:maxt-1),      $
                    format=state.num_form),2)
 	       	endif

		;***************************************
		;**** Copy new data to table widget ****
		;***************************************

 	       widget_control,state.tempid, set_value=tempval
	   endif
      end

		;***********************
		;**** Cancel button ****
		;***********************

    	state.cancelid: new_event = {ID:parent, TOP:event.top, 		$
				     HANDLER:0L, ACTION:'Cancel'}

		;*********************
		;**** Done button ****
		;*********************

    	state.doneid: begin

		;***************************************
		;**** Check all user input is legal ****
		;***************************************

	    error = 0

        	;********************************************
		;*** Have to restore "state" before calls ***
		;*** to proc413_get_val so it can be used ***
		;*** there.				  ***
		;********************************************
		
	    widget_control, first_child, set_uvalue=state, /no_copy
	    widget_control, event.handler, get_value=ps

		;*** reset state variable ***

	    widget_control, first_child, get_uvalue=state, /no_copy

   ;**** check temp/density values entered ****
	
  	    iopt = state.iopt

	    if iopt ne 0 then begin 
	    
              if error eq 0 then begin
                  widget_control, state.tempid, get_value=tabval
                  tabvals =                                               $
                  where(strcompress(tabval.value(0,*), /remove_all) ne '')
                  if tabvals(0) eq -1 then begin
                      error = 1
                      message = '**** Error: No temperatures'+ $
                                ' entered ****'
                  endif
              endif


                  ;*** Check to see if sensible tolerance is selected.

              if (error eq 0 and ps.ifsel eq 1) then begin
                  if (float(ps.tolval) lt 0)  or                          $
                  (float(ps.tolval) gt 100) then begin
                      error = 1
                      message='Error: Tolerance for polyfit must be 0-100% '
                  endif
              endif
              
            endif

   ;*** Check to see if selections have been made.

            case iopt of
            
               0 : begin
                     if (error eq 0 and (ps.nlev_res eq -1 or ps.ip_res eq -1)) then begin
                        error = 1
                        message='Error: Choose a resolved level'
                     endif
             	   if (error eq 0 ) then begin 
                      if ( state.irion(ps.nlev_res,ps.ip_res) eq 0) then begin
                        error = 1
                        message='Error: Choose a valid resolved level'
                      endif
			   endif
                   end
               1 : begin     
                     if (error eq 0 and (ps.ip_aug eq -1 or ps.im_aug eq -1)) then begin
                        error = 1
                        message='Error: Choose a resolved level'
                     endif
             	   if (error eq 0 ) then begin
                      if ( state.irexc(ps.ip_aug,ps.im_aug) eq 0) then begin
                        error = 1
                        message='Error: Choose a valid resolved level'
                      endif
 			   endif
                   end
               2 : begin     
                     if (error eq 0 and (ps.in_rep eq -1 or ps.ips_rep eq -1)) then begin
                        error = 1
                        message='Error: Choose a representative n-shell'
                     endif
             	   if (error eq 0 ) then begin
                      if (state.iexcn(ps.in_rep,ps.ips_rep) eq 0) then begin
                        error = 1
                        message='Error: Choose a valid representative n-shell'
                      endif
 			   endif
                   end
                   
            endcase       

   ;**** return value or flag error ****

	    if error eq 0 then begin
	        new_event = {ID:parent, TOP:event.top, 			$
                             HANDLER:0L, ACTION:'Done'}
            endif else begin
	        widget_control, state.messid, set_value=message
	        new_event = 0L
            endelse
        end


                ;**** Menu button ****

        state.outid: begin
            new_event = {ID:parent, TOP:event.top, HANDLER:0L,          $
                         ACTION:'Menu'}
        end

    	ELSE: new_event = 0L

    ENDCASE

		;*********************************************
		;*** make "state" available to other files ***
		;*********************************************

    widget_control, first_child, set_uvalue=state,/no_copy

    RETURN, new_event

END

;-----------------------------------------------------------------------------

FUNCTION cw_adas413_proc, topparent, 					$
		dsfull, iz, iz0, iz1, 					$
		ntdim,  ndtin, 						$
		ita,  tvals, 						$
		ndaug, naug, caug, iaug,   				$
		ndprt, ndprti, ndmet, ndlev, 				$
		nprf , nprfm , npri , nlev , nlevm, 			$
		iprfm, ipri  , imeti, 					$
		cprf , clev, irion, irexc,				$
		kdrep,  ndrep, krep,  nrcnt, crep, cnrep, iexcn,	$
		bitfile, 						$
		procval=procval, uvalue=uvalue, 			$
		font_large=font_large, font_small=font_small, 		$
		edit_fonts=edit_fonts, num_form=num_form


		;**** Set defaults for keywords ****

    if not (keyword_set(uvalue))     then uvalue=0
    if not (keyword_set(font_large)) then font_large='' 
    if not (keyword_set(font_small)) then font_small=''
    if not (keyword_set(edit_fonts)) then 	$
    edit_fonts = {font_norm:'', font_input:''}
    if not (keyword_set(num_form)) then num_form='(E10.3)'
    if not (keyword_set(procval)) then begin
        str0_arr  = strarr(ndaug)
        str1_arr  = strarr(ndprt)
        str2_arr  = strarr(ndlev)
        str3_arr  = strarr(kdrep)
        str4_arr  = strarr(ndrep,kdrep)
	flt_arr   = fltarr(ndprt)
        temp_arr  = dblarr(ndtin)
        int0_arr  = intarr(nprfm)
        int1_arr  = intarr(npri)
        int2_arr  = intarr(nlevm)
        int3_arr  = intarr(kdrep)
        ps = {  nmet    : 0, 						$
		title   : '',             				$
		naug    : 0,						$
		nprf    : 0,						$
		nprfm   : 0,						$
		npri    : 0,						$
		nlev    : 0,						$
		nlevm   : 0,						$
		iprfm   : int0_arr,		      			$
		ipri    : int1_arr,		      			$
		imeti   : int2_arr,		      			$
		krep    : 0,						$
		nrcnt   : int3_arr,					$
		ip_aug  : 1,						$
		im_aug  : 1,						$
		ip_res  : 1,						$
		nlev_res : 1,						$
		ips_rep : 1,						$
		in_rep  : -1,						$
		uaug    : str0_arr,					$
		uprnt   : str1_arr,					$
		ulev    : str2_arr,                                     $
		urep    : str3_arr,                                     $
		unrep   : str4_arr,                                     $
		ifout   : 1,                                            $
		iopt    : 0,                                            $
		maxt    : 0,                                            $
                tin     : temp_arr,                                     $
                ifsel   : 0,                                            $
                tolval  : '5'                                           }
    endif else begin
	ps = {  nmet    : procval.nmet,                                 $
		title   : procval.title,                                $
		naug    : procval.naug,                                 $
		nprf    : procval.nprf,                                 $
		nprfm   : procval.nprfm,                                $
		npri    : procval.npri,                                 $
		nlev    : procval.nlev,                                 $
		nlevm   : procval.nlevm,                                $
		iprfm   : procval.iprfm,                                $
		ipri    : procval.ipri,                                 $
		imeti   : procval.imeti,                                $
		krep    : procval.krep,                                 $
		nrcnt   : procval.nrcnt,                                $
		ip_aug  : procval.ip_aug,                               $
		im_aug  : procval.im_aug,                               $
		ip_res  : procval.ip_res,                               $
		nlev_res : procval.nlev_res,                            $
		ips_rep : procval.ips_rep,                              $
		in_rep  : procval.in_rep,                               $
		uaug    : strarr(n_elements(caug)),                     $
		uprnt   : strarr(n_elements(cprf)),                     $
		ulev    : strarr(n_elements(clev)),                     $
		urep    : strarr(n_elements(crep)),                     $
		unrep   : strarr(n_elements(cnrep(*,0)),n_elements(cnrep(0,*))),                    $
		ifout   : procval.ifout,                                $
		iopt    : procval.iopt,                                 $
		maxt    : procval.maxt,                                 $
                tin     : procval.tin,                                  $
                ifsel   : procval.ifsel,                                $
                tolval  : procval.tolval     				}
    endelse
    
    ipres = procval.ip_res
    if ipres eq 0 then ipres = 1
    ipsrep = procval.ips_rep
    if ipsrep eq 0 then ipsrep = 1
    
    valid_str  = ['   No  ','   Yes ']

    ps.ulev  = clev
    ps.uaug  = caug
    ps.uprnt = cprf
    ps.urep  = crep
    ps.unrep = cnrep


                ;*********************************************************
                ;**** Modify certain parameters and results depending ****
                ;**** on the machine being used                       ****
                ;*********************************************************

    machine = GETENV('TARGET_MACHINE')
    if machine eq 'HPUX' then begin
        y_size = 4
        large_font = font_small
    endif else begin
        y_size = 6
        large_font = font_large
    endelse

		;****************************************************
		;*** Assemble temperature data                   ****
		;****************************************************
		;**** The adas table widget requires data to be *****
		;**** input as strings, so all the numeric data *****
		;**** has to be written into a string array.    *****
		;**** Declare temp. table arrays	        *****
		;**** col 1 has user  values             	*****
		;**** col 2 has values from files,      	*****
		;**** which can have one of three possible units*****
		;****************************************************

    tabledata = strarr(4,ndtin)

		;***********************************************
		;**** Copy out  temperatures                ****
		;**** for this data block                   ****
		;***********************************************

    maxt = ita 
    if (maxt gt 0) then begin
    
; fill table with blanks if tin(*) = 0.0
; user entered values

        full   = where(ps.tin,count_f)
        blanks = where(ps.tin eq 0.0,count_b)
        if (count_f ne 0) then begin
           tabledata(0,full) = 						$
	   strtrim(string(ps.tin(full),format=num_form),2)
	endif
        if (count_b ne 0) then begin
           tabledata(0,blanks) = ' ' 
        endif

; input file temperatures
	
        tabledata(1,0:maxt-1) = 					$
        strtrim(string(tvals(0:maxt-1,0),format=num_form),2)
        tabledata(2,0:maxt-1) = 					$
	strtrim(string(tvals(0:maxt-1,1),format=num_form),2)
        tabledata(3,0:maxt-1) = 					$
        strtrim(string(tvals(0:maxt-1,2),format=num_form),2)
        
        if (maxt lt ntdim) then begin
           tabledata(1:3,maxt:ntdim-1) = ' '
        endif
        
    endif

		;********************************************************
		;**** Create the 413 Processing options/input window ****
		;********************************************************

		;**** create titled base widget ****

    parent = widget_base(topparent, UVALUE = uvalue, 			$
			 title = 'ADAS413 PROCESSING OPTIONS', 		$
			 EVENT_FUNC = "proc413_event", 			$
			 FUNC_GET_VALUE = "proc413_get_val", 		$
			 /COLUMN)

		;******************************************************
		;**** Create a dummy widget just to hold value of *****
		;**** "state" variable so as not to get confused  *****
		;**** with any other values. Adopt IDL practice   *****
		;**** of using first child widget                 *****
		;******************************************************

    first_child = widget_base(parent)
    topbase = widget_base(first_child,/column)

		;***********************
		;**** add run title ****
		;***********************

    base = widget_base(topbase,/row)
    rc = widget_label(base,value='Title for Run',font=large_font)
    runid = widget_text(base,value=ps.title,xsize=38,font=large_font,/edit)

		;**** add dataset name and browse button ****

    rc = cw_adas_dsbr(topbase,dsfull,font=large_font,/row)

                ;**** add charge information ****

    sz0 = string(iz0,format='(i2)')           
    sz1 = string(iz1,format='(i2)')           
    sz  = string(iz,format='(i2)')           
    
    rc = widget_label(topbase,font=font_large, 			$
                      value='Nuclear Charge :'+sz0+		$
                      '  Ionised Ion Charge :'+sz1+		$
                      '  Ionising Ion Charge :'+sz   )



		;************************************
		;**** Add the plot type selector ****
		;************************************

  wrow = widget_base(parent, /row, /frame)
  wcol = widget_base(wrow, /column)

  wr_but = widget_base(wcol, /row)
  wc_but = widget_base(wr_but, /column)
  wr_but = widget_base(wr_but, /row, /exclusive) 


  iopt1id = widget_button(wr_but, value='Direct Ionis.', $
                          font=font_large, /no_release)
  iopt0id = widget_button(wr_but, value='Excit. to Autoion. Levels',  $
                          font=font_large, $
                          /no_release)
  iopt2id = widget_button(wr_but, value='Excit. to Autoion. n-shells', $
                           font=font_large, /no_release)


; **** base for iopt choice inputs - map/unmap these as appropriate and not the
;      individual widgets held within them
        
  switchb = widget_base(wcol,/frame)
  res_id = widget_base(switchb,/row)
  aug_id = widget_base(switchb,/row)
  rep_id = widget_base(switchb,/row)
  
  
  
; direct ionisation - selection of final parent and initial level


    block_info = strarr(1,nlevm)
    block_info(0,*) = strmid(clev(imeti(*)-1),0,21)
    titles = [['Ionising Level'],['Configuration      Term']]
    select_data = text_table(block_info, colhead=titles)
    coltitles = select_data(0:1)
    select_data = select_data(2:nlevm+1)
    o1aid = cw_single_sel(res_id, select_data, value=ps.nlev_res,    $
			  title='Select Ionising Level',             $
			  coltitles=coltitles,                       $
			  ysize=(y_size+2), font=font_small,         $
			  big_font=large_font)
	
    block_info = strarr(1,nprf)
    block_info(0,*) = cprf(*)
    titles = [['Ionised Parent'], 					$
             ['Configuration      Term  Valid Data']]
    select_data = text_table(block_info, colhead=titles)
    coltitles = select_data(0:1)
    
    list         = select_data(2:nprf+1)
    list_1       = strarr(nprf+1)
    list_1(0)    = string(ps.ip_res+1)
    list_1(1:nprf) = list
	
    o1bid = cw_single_list_sel(res_id, value=list_1,      		$
			       title='Select Ionised Parent',           $
			       coltitles=coltitles,                     $
			       ysize=(y_size+2), font=font_small,       $
			       big_font=large_font)

			  
; excit. to autoion. levels - selection of d/exc level and initial level

	 	     
    block_info = strarr(1,nlevm)
    block_info(0,*) = strmid(clev(imeti(*)-1),0,21)
    titles = [['Initial Level'],['Configuration      Term']]
    select_data = text_table(block_info, colhead=titles)
    coltitles = select_data(0:1)
    select_data = select_data(2:nlevm+1)
    o0aid  = cw_single_sel(aug_id, select_data, value=ps.ip_aug,     $
			  title='Select Initial Level',              $
			  coltitles=coltitles,                       $
			  ysize=(y_size+2), font=font_small,         $
			  big_font=large_font)
			  			  
    block_info = strarr(1,nlev)
    block_info(0,*) = clev(*)
    titles = [['Doubly Excited Level'], 			     $
             ['Configuration      Term  Valid Data']]
    select_data = text_table(block_info, colhead=titles)
    coltitles = select_data(0:1)
    
    list         = select_data(2:nlev+1)
    list_1       = strarr(nlev+1)
    list_1(0)    = string(ps.im_aug+1)
    list_1(1:nlev) = list
	
    o0bid = cw_single_list_sel(aug_id, value=list_1,       		$
			       title='Select Doubly Excited Level',     $
			       coltitles=coltitles,                     $
			       ysize=(y_size+2), font=font_small,       $
			       big_font=large_font)



	 	 
; representative excit. selection of parent/spin system and n-shell


    if (krep gt 0) then begin
        block_info = strarr(1,krep)
        block_info(0,*) = crep(0:krep-1)
    endif else begin
       block_info = strarr(1,1)
       block_info(0,*) = 'no n-shell data'
    endelse

    titles = [['Initial Level            Interm. Parent              Recombined'],   $
              ['Configuration      Term   Configuration      Term   System (spin)']]
    select_data = text_table(block_info, colhead=titles)
    coltitles = select_data(0:1)

    if (krep gt 0) then begin
        select_data = select_data(2:krep+1)
    endif else begin
        select_data = select_data(2:2)
    endelse

    o2aid = cw_single_sel(rep_id, select_data, 			     $
			  title='Select Parent/Spin System',         $
			  coltitles=coltitles,                       $
			  ysize=(y_size+2), font=font_small,         $
			  big_font=large_font)
		
			  
    if (krep gt 0) then begin
        block_info = strarr(1,nrcnt(ps.ips_rep))
        block_info(0,*) = cnrep(0:nrcnt(ps.ips_rep)-1,ps.ips_rep)
    endif else begin
        block_info = strarr(1,1)
        block_info(0,*) = 'no n-shells'
    endelse

    titles = [['Representative  Valid'], $
             ['n-shell          Data ']]
    select_data = text_table(block_info, colhead=titles)
    coltitles = select_data(0:1)
			
    if (krep gt 0) then begin
        list           = select_data(2:nrcnt(ps.ips_rep)+1)
        list_1         = strarr(nrcnt(ps.ips_rep)+1)
        list_1(0)      = string(ps.in_rep+1)
        list_1(1:nrcnt(ps.ips_rep)) = list
    endif else begin
        list           = select_data(2:2)
        list_1         = strarr(2)
        list_1(0)      = string(2)
        list_1(1:1)    = list
    endelse
	
    o2bid = cw_single_list_sel(rep_id, value=list_1,      		$
			       title='Select n-shell',                  $
			       coltitles=coltitles,                     $
			       ysize=(y_size+2), font=font_small,       $
			       big_font=large_font)


		;****************************************
		;**** Add a base for temp selections ****
		;****************************************

    tablebase = widget_base(parent,/row)

		;************************************************
		;**** base for the table and defaults button ****
		;************************************************

    base = widget_base(tablebase,/column,/frame)

		;********************************
		;**** temperature data table ****
		;********************************

    colhead = [['Output','Input','Output','Input'], ['','','','']]

		;********************************************
		;**** convert FORTRAN index to IDL index ****
		;********************************************

    units = ps.ifout-1 
    unitname = ['Kelvin','eV','Reduced']

		;***********************************************************
		;**** Four columns in the table and three sets of units.****
		;**** Column 1 has the same values for all three        ****
		;**** units but column 2 switches between sets 2,3 & 4  ****
		;**** in the input array tabledata as the units change. ****
		;***********************************************************

    unitind = [[0,0,0],[1,2,3]]
    table_title = [ ["Output Electron Temperatures"]]

		;****************************
		;**** table of data   *******
		;****************************

    tempid = cw_adas_table(base, tabledata, units, unitname, unitind, 	$
			   UNITSTITLE = 'Temperature Units', 		$
			   COLEDIT = [1,0], COLHEAD = colhead,   	$
			   TITLE = table_title, 			$
			   UNITS_PLUS = '',			 	$
			   ORDER = [1,0], 				$ 
			   LIMITS = [1,1], CELLSIZE = 12, 		$
			   /SCROLL, ytexsize=y_size, NUM_FORM=num_form, $
			   FONTS = edit_fonts, FONT_LARGE = large_font, $
			   FONT_SMALL = font_small)

		;*************************
		;**** Default buttons ****
		;*************************

    tbase = widget_base(base, /column)
    deftid = widget_button(tbase,value=' Default Temperature Values  ',	$
		           font=large_font)

		;**************************************
		;**** Add polynomial fit selection ****
		;**************************************

    polyset = { option:intarr(1), val:strarr(1)}  ; defined thus because 
						  ; cw_opt_value.pro
                                                  ; expects arrays
    polyset.option = [ps.ifsel]
    polyset.val = [ps.tolval]
    options = ['Fit Polynomial']
;    optbase = widget_base(topbase,/frame)
    optbase = widget_base(base)
    optid   = cw_opt_value(optbase, options, 				$
			       title='Polynomial Fitting',		$
			       limits = [0,100], 			$
			       value=polyset, font=large_font 		)

		;**** Error message ****

    messid = widget_label(parent,font=large_font, 			$
    value='Edit the processing options data and press Done to proceed')

		;**** add the exit buttons ****

    base = widget_base(parent,/row)
    menufile = bitfile + '/menu.bmp'
    read_X11_bitmap, menufile, bitmap1
    outid = widget_button(base, value=bitmap1)          ;menu button
    cancelid = widget_button(base, value='Cancel', font=large_font)
    doneid = widget_button(base, value='Done', font=large_font)




; set map/sensitive options depending on the type of plot 
 	 
	case ps.iopt of
	  0 : begin
	  	widget_control, aug_id, map=0
	  	widget_control, res_id, map=1
	  	widget_control, rep_id, map=0
	  	widget_control, iopt0id, set_uvalue = 0
	  	widget_control, iopt1id, set_uvalue = 1
	  	widget_control, iopt2id, set_uvalue = 0
	  	widget_control, iopt0id, set_button = 0
	  	widget_control, iopt1id, set_button = 1
	  	widget_control, iopt2id, set_button = 0
	  	widget_control, tempid, sensitive=1
	  	widget_control, deftid, sensitive=1
	  	widget_control, optid, sensitive=1
	      end
	  1 : begin
	  	widget_control, aug_id, map=1
	  	widget_control, res_id, map=0
	  	widget_control, rep_id, map=0
	  	widget_control, iopt0id, set_uvalue = 1
	  	widget_control, iopt1id, set_uvalue = 0
	  	widget_control, iopt2id, set_uvalue = 0
	  	widget_control, iopt0id, set_button = 1
	  	widget_control, iopt1id, set_button = 0
	  	widget_control, iopt2id, set_button = 0
	  	widget_control, tempid, sensitive=1
	  	widget_control, deftid, sensitive=1
	  	widget_control, optid, sensitive=1
	      end
	  2 : begin
	  	widget_control, aug_id, map=0
	  	widget_control, res_id, map=0
	  	widget_control, rep_id, map=1
	  	widget_control, iopt0id, set_uvalue = 0
	  	widget_control, iopt1id, set_uvalue = 0
	  	widget_control, iopt2id, set_uvalue = 1
	        widget_control, iopt0id, set_button = 0
	  	widget_control, iopt1id, set_button = 0
	  	widget_control, iopt2id, set_button = 1
	  	widget_control, tempid, sensitive=1
	  	widget_control, deftid, sensitive=1
	  	widget_control, optid, sensitive=1
	      end
	endcase
	




  
		;*************************************************
		;**** create a state structure for the pop-up ****
		;**** 		     window.                  ****
		;*************************************************

    new_state = { runid		:	runid,  			$
		  messid	:	messid, 			$
		  deftid	:	deftid,				$
		  tempid	:	tempid, 			$
		  optid		:	optid,				$
		  cancelid	:	cancelid,			$
		  doneid	:	doneid, 			$
		  outid		:	outid,				$
		  iopt0id	: 	iopt0id,			$
		  iopt1id	: 	iopt1id,			$
		  iopt2id	: 	iopt2id,			$
		  aug_id	: 	aug_id,	  			$
		  res_id	: 	res_id,	  			$
		  rep_id	: 	rep_id,	  			$
		  o0aid		:	o0aid,				$
		  o0bid		:	o0bid,				$
		  o1aid		:	o1aid,				$
		  o1bid		:	o1bid,				$
		  o2aid		:	o2aid,				$
		  o2bid		:	o2bid,				$
		  dsfull	:	dsfull,				$
		  iopt		:	ps.iopt,			$
		  ntdim	 	:	ntdim,				$
		  ndtin	 	:	ndtin,				$
		  maxt		:	maxt, 				$
		  ita		:	ita,				$
		  tvals		:	tvals,				$
		  ndaug		:	ndaug,				$
		  ndprt 	:	ndprt,				$
		  ndlev		:	ndlev,				$
		  kdrep 	:	kdrep,				$
		  ndrep		:	ndrep,				$
		  naug		:	naug,				$
		  nprf		:	nprf,				$
		  nprfm		:	nprfm,				$
		  npri		:	npri,				$
		  nlev		:	nlev,				$
		  nlevm		:	nlevm,				$
		  iprfm		:	iprfm,				$
		  ipri		:	ipri,				$
		  imeti		:	imeti,				$
		  krep		:	krep,				$
		  nrcnt		:	nrcnt,				$
		  uaug		:	caug,				$
		  uprnt		:	cprf,				$
		  ulev		:	clev,				$
		  urep		:	crep,				$
		  unrep		:	cnrep,				$
		  iaug		:	iaug,				$
		  irion		:	irion,				$
		  irexc		:	irexc,				$
		  iexcn		:	iexcn,				$
		  font		:	large_font,			$
		  num_form	:	num_form			}
  
                 ;**** Save initial state structure ****

    widget_control, first_child, set_uvalue=new_state,/no_copy

    RETURN, parent

END
