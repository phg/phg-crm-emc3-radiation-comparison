; Copyright (c) 1998 Strathclyde University .
; SCCS info: Module @(#)$Header: /home/adascvs/idl/adas4xx/adas413/adas413_plot.pro,v 1.1 2004/07/06 10:49:18 whitefor Exp $ Date @(#)$Header: /home/adascvs/idl/adas4xx/adas413/adas413_plot.pro,v 1.1 2004/07/06 10:49:18 whitefor Exp $
;+
; PROJECT:
;       ADAS IBM MVS to   UNIX conversion
;
; NAME:
;	ADAS413_PLOT
;
; PURPOSE:
;	Generates ADAS413/ graphical output.
;
; EXPLANATION:
;	This routine creates a window for the display of graphical
;	output a separate routine PLOT413 actually plots a
;	graph.
;
; USE:
;	This routine is specific to ADAS413, see ddoutg.pro for
;	example use.
;
; INPUTS:
;	(Most of these inputs map exactly onto variables of the same
;	 name in the FORTRAN code.)
;
;
;	DSFULL  - String; Name of data file 
;
;	TITLE   - String array; titles to be placed above graph
;
;	TITLX   - String; user supplied comment appended to end of title
;
;	TITLM   - String; Information about minimax fitting if selected.
;
;	UTITLE  - String; Optional comment by user
;
;	DATE	- String; Date of graph production
;
;	ESYM 	- String; Symbol of emitting ion
;
;	IZ0	- Integer; Nuclear charge of emitting ion
;
;	IZ	- Integer; Charge of emitting ion
;
;	CWAVEL 	- String; Wavelength of data block used
;
;	CFILE	- String; Specific ion file source
;
;	CPCODE 	- String; processing code from input data file
;
;	CINDM	- String; metastable index
;
;	ITVAL	- Integer; number of user entered temperature/density pairs
;
;	TEVA	- Double array; User entered electron temperatures, eV
;
;	DIN	- Double array; User entered electron densities, cm-3
;
;	SXBA	- Double array; Spline interpolated or extrapolated 
;				ionizations per photon for user entered temp.
;				and density pairs.
;
;	SXBM	- Double array; Minimax fit values of ionizations per photon  
;				at 'tdfitm()'
;
;	TDFITM	- Double array; Selected temperature values for minmiax fit.
;
;	LDEF1	- Integer; 0 - use user entered graph scales
;			   1 - use default axes scaling
;
;	XMIN    - String; Lower limit for x-axis of graph, number as string.
;
;	XMAX    - String; Upper limit for x-axis of graph, number as string.
;
;	YMIN    - String; Lower limit for y-axis of graph, number as string.
;
;	YMAX    - String; Upper limit for y-axis of graph, number as string.
;
;	LFSEL	- Integer; 0 - No minimax fitting was selected 
;			   1 - Mimimax fitting was selected
;
;	LDFIT 	- Integer; 0 - data fitted vs temperature
;			   1 - Data fitted vs Density
;
;	NMX	- Integer; Number of temp/density pairs used for minimax fit
;
;	STRG	- String array; Information regarding current data selected
;
;	HEAD1	- String; header information for data source
;
;	HEAD2A	- String; Header information for temperature/density 
;
;	HEAD2B  - String; column titles for temp/density pair information
;
;	HRDOUT - Integer; 1 if hardcopy output activated, 0 if not.
;
;	HARDNAME- String; Filename for harcopy output.
;
;	DEVICE	- String; IDL name of hardcopy output device.
;
;	HEADER	- String; ADAS version number header to include in graph.
;
;       BITFILE - String; the path to the directory containing bitmaps
;                 for the 'escape to series menu' button.
;
; OPTIONAL INPUTS:
;	None.
;
; OUTPUTS:
;       GOMENU  - Int; flag - set to 1 if user has selected 'escape direct
;                 to series menu', otherwise 0
;
; OPTIONAL OUTPUTS:
;	None.
;
; KEYWORD PARAMETERS:
;	FONT	- String; The name of a font to use for text in the
;		  graphical output widget.
;
; CALLS:
;	CW_ADAS_GRAPH	Graphical output widget.
;	PLOT413		Make one plot to an output device for 413.
;	XMANAGER
;
; SIDE EFFECTS:
;	This routine uses a common block to maintain its state PLOT413_BLK.
;
;	One other routine is included in this file;
;	ADAS413_PLOT_EV	Called via XMANAGER during widget management.
;
; CATEGORY:
;	Adas system.
;
; WRITTEN:
;       H. P. Summers, 30-01-98
;
; MODIFIED:
;	1.1	H. P. Summers
;		First Version
;
; VERSION:
;	1.1	30-01-98
;
;----------------------------------------------------------------------------

PRO adas413_plot_ev, event

    COMMON plot413_blk, data, action, win, plotdev, plotfile, 		$
                        fileopen, gomenu

    newplot = 0
    print   = 0
    done    = 0

		;****************************************
		;**** Set graph and device requested ****
		;****************************************

    CASE event.action OF

	'print': begin
	    newplot = 1
	    print = 1
	end

	'done': begin
	    if fileopen eq 1 then begin
	        set_plot, plotdev
	        device, /close_file
	    endif
	    set_plot, 'X'
	    widget_control, event.top, /destroy
	    done = 1
	end

        'bitbutton' : begin
            if fileopen eq 1 then begin
                set_plot, plotdev
                device, /close_file
            endif
            set_plot,'X'
            widget_control, event.top, /destroy
            done = 1
            gomenu = 1
        end

	ELSE:	;**** Do nothing ****

    ENDCASE

		;*******************************
		;**** Make requested plot/s ****
		;*******************************

    if done eq 0 then begin

		;**** Set graphics device ****

        if print eq 1 then begin
            set_plot, plotdev
            if fileopen eq 0 then begin
                fileopen = 1
                device, filename=plotfile
	        device, /landscape
            endif
        endif else begin
            set_plot, 'X'
            wset, win
        endelse

		;**** Draw graphics ****

        plot413, data.x , data.y, data.itval, data.iopt,   	$
                 data.nplots, data.nmx, 			$
	         data.title, data.xtitle, data.ytitle, 		$
	         data.strg, data.head1, data.head2,           	$
	         data.teva,  data.qdr , data.ldef1, 		$
	         data.xmin, data.xmax, data.ymin, data.ymax 
	if print eq 1 then begin
	    message = 'Plot  written to print file.'
	    grval = {WIN:0, MESSAGE:message}
	    widget_control, event.id, set_value=grval
	endif
    endif

END

;----------------------------------------------------------------------------

PRO adas413_plot, dsfull, title, titlx, titlm, utitle, date, iz0, iz1, iopt, $
		  irep, nrepa, auga, 					     $
		  itval, teva, qdr, qdrm, tfevm, qdrs, tfevs,		     $
		  ldef1, xmin, xmax,         	 			     $
                  ymin, ymax, lfsel,  nmx,  strg, head1, head2,              $
                  hrdout, hardname, device, header, 	                     $
		  bitfile,gomenu, FONT=font

    COMMON plot413_blk, data, action, win, plotdev, plotfile, 		$
                        fileopen, gomenucom

		;**** Copy input values to common ****

    plotdev   = device
    plotfile  = hardname
    fileopen  = 0
    gomenucom = gomenu

		;************************************
		;**** Create general graph title ****
		;************************************

    title = strarr(5)

    title(0) = "IONISATION & EXCIT./AUTO. RATE COEFFICIENTS VERSUS TEMPERATURE" 
    if ( strtrim(strcompress(utitle),2)  ne '' ) then begin
        title(0) = title(0) + ': ' + strupcase(strtrim(utitle,2))
    endif
    title(1) = 'ADAS    :' + strupcase(header)
    title(2) = 'FILE     :' + titlx
    if (lfsel eq 1) then begin
        title(3)  = "MINIMAX : " + strupcase(titlm)
    endif
    title(4) = 'KEY     : (CROSSES - INPUT DATA) (FULL LINE - SPLINE FIT)'
    if (lfsel eq 1) then  title(4) = title(4) + '   (DASH LINE - MINIMAX) '

		;********************************
		;*** Create graph annotation ****
		;********************************        

    strg = strtrim(strg, 2)

		;*********************************
		;**** Set up Y data for plots ****
		;*********************************


; nmx same for splined data and polynominal fit
    
       ydim=nmx 
       y = make_array(3, ydim, /float)
       valid_data = where((qdr gt 1e-37) and (qdr lt 1e+37))
       valid_spline = where((qdrs gt 1e-37) and (qdrs lt 1e+37))
       if (valid_data(0) ge 0) then begin
           y(0, valid_data) = qdr(valid_data)
           if (valid_spline(0) ge 0) then begin
               y(1, valid_spline) = qdrs(valid_spline)
           endif else begin 
               print, "ADAS413 : unable to plot spline fit data"
           endelse
           if (lfsel eq 1) then begin
               valid_data = where((qdrm gt 1e-37) and (qdrm lt 1e+37))
	       if (valid_data(0) ge 0) then begin
                   y(2, valid_data) = qdrm(valid_data)
               endif else begin
                   print, "ADAS413 : unable to plot polynomial fit data"
               endelse
           endif
       endif else begin
           print, "ADAS413 : unable to plot spline fit data"
       endelse
       ytitle = "Rate Coefficient (cm!E3!N s!E-1!N) "

		;**************************************
		;**** Set up x axis and xaxis title ***
		;**************************************

    
       xdim = nmx 
       x    = fltarr(3, xdim)

       if (lfsel eq 0) then begin
           x(0,0:itval-1) = teva
           x(1,*) = tfevs
           nplots = 2
       endif else begin
           x(0,0:itval-1) = teva
           x(1,*) = tfevs
           x(2,*) = tfevm
           nplots = 3
       endelse
       xtitle = "Temperature (eV)"
     


  		;******************************************
		;*** if desired set up user axis scales ***
		;******************************************

    if (ldef1 eq 0) then begin
        xmin = min(x, max = xmax)
        xmin = xmin * 0.9
        xmax = xmax * 1.1
        ymin = min( y, max = ymax)
        ymin = ymin * 0.9
        ymax = ymax *1.1
    endif

		;*************************************
		;**** Create graph display widget ****
		;*************************************

    graphid = widget_base(TITLE='ADAS413 GRAPHICAL OUTPUT', 		$
			  XOFFSET=1,YOFFSET=1)
    device, get_screen_size=scrsz
    xwidth    = scrsz(0)*0.75
    yheight   = scrsz(1)*0.75
    multiplot = 0
    bitval    = bitfile + '/menu.bmp'
    cwid      = cw_adas_graph(graphid, print=hrdout, FONT=font,         $
                         xsize=xwidth, ysize=yheight,                   $
                         multiplot=multiplot, bitbutton=bitval)

                ;**** Realize the new widget ****

    dynlabel, graphid
    widget_control, graphid, /realize

		;**** Get the id of the graphics area ****

    widget_control, cwid, get_value=grval
    win = grval.win

		;*******************************************
		;**** Put the graphing data into common ****
		;*******************************************

    data = {    Y		:	y,				$
		X		:	x,   				$
		ITVAL		:	iopt,    			$
		IOPT		:	itval,    			$
		NPLOTS		:	nplots,  			$
		NMX		:	nmx, 				$
                TITLE		:	title,   			$
		XTITLE		:	xtitle,  			$
		YTITLE		:	ytitle,           		$
		STRG		:	strg,     			$
		HEAD1		:	head1,    			$
		HEAD2		:	head2,           		$
		TEVA		:	teva,      			$
		qdr		:	qdr,      			$
		LDEF1		:	ldef1, 				$
		XMIN		:	xmin,				$
		XMAX		:	xmax,				$
		YMIN		:	ymin,				$
		YMAX		:	ymax 				}
 
    wset, win
    plot413, x, y, iopt, itval, nplots, nmx, title, xtitle, ytitle, 	$
             strg, head1, head2, teva, qdr,  				$
	     ldef1, xmin, xmax, ymin, ymax 


		;***************************
                ;**** make widget modal ****
		;***************************

    xmanager, 'adas413_plot', graphid, event_handler='adas413_plot_ev',	$
              /modal, /just_reg
    gomenu = gomenucom

END
