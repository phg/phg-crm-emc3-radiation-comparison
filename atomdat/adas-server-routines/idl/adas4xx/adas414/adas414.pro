; Copyright (c) 2000 Strathclyde University .
; SCCS info: Module @(#)$Header: /home/adascvs/idl/adas4xx/adas414/adas414.pro,v 1.3 2011/12/08 10:53:16 mog Exp $ Date $Date: 2011/12/08 10:53:16 $  
;+
; PROJECT:
;       ADAS
;
; NAME:
;       ADAS414
;
; PURPOSE:
;       The highest level routine for the ADAS 414 program.
;
; EXPLANATION:
;       This routine is called from the main adas system routine,
;       adas.pro, to start the ADAS 414 application. This is the
;       first ADAS program to be fully written in IDL, which may
;       or may not be a good thing. 
;
;       The purpose of ADAS414 is to take the partial GCR passing
;       file output of ADAS208 and to insert, or replace, these
;       data into the isoelectronic adf10 datasets. There is a strict
;       naming convention for adf10 sets, eg helike
;           /home/adas/adas/adf10/acd96/pj#acd96_he12.dat
;       This file contains the helike 1-2 partial data. The pj
;       prefix is optional but is usually enforced. The directory
;       structure in adf10 is strictly adhered to in this program.
;       ie. to choose and adf10 file the year, prefix and root
;       (user or central ADAS) are given. The dataclasses are selected
;       from a button list and the passing directory of the ADAS208
;       output is also selected. All input is checked for existence
;       and some data consistency checks are performed.
;
;       There is no processing screen. However a summary of the file
;       checks and a summary of their contents are displayed.
;
;       For output a summary paper.txt and a choice of 3 output 
;       options are provided. The user can either replace the original
;       adf10 with the supplemented ones, write all the new adf10 files
;       to a passing directory or send them to the users adf10 
;       directory (a year and prefix are the inputs for this option).
;
;
; USE:
;       First the system settings must be established by calling
;       adas_sys_set.pro then adas414.pro is called to start the
;       ADAS 414 application;
;
;       adas_sys_set, adasrel, fortdir, userroot, centroot, $
;                     devlist, devcode, font_large, font_small, edit_fonts
;       adas414,   adasrel, fortdir, userroot, centroot, devlist, $
;                  devcode, font_large, font_small, edit_fonts
;
; INPUTS:
;       ADASREL - A string indicating the ADAS system version,
;                 e.g ' ADAS RELEASE: ADAS93 V1.4'.  The first
;                 character should be a space.
;
;       FORTDIR - A string holding the path to the directory where the
;                 FORTRAN executables are, e.g '/disk/adas/fortran/exec'
;                 Not used but retainned for compatibility.
;
;       USERROOT - A string holding the path to the root directory of
;                  the user's adas data. e.g '/disk/bowen/adas' This
;                  root directory will be used by adas to construct
;                  other path names.  In particular the user's default
;                  interface settings will be stored in the directory
;                  USERROOT+'/defaults'.  An error will occur if the
;                  defaults directory does not exist.
;
;       CENTROOT - Like USERROOT, but this directory points to the
;                  central data area for the system.  User defaults are
;                  not stored on CENTROOT.
;
;       DEVLIST - A string array of hardcopy device names, used for
;                 graphical output. e.g ['Post-Script','HP-PCL','HP-GL']
;                 This array must mirror DEVCODE.  DEVCODE holds the
;                 actual device names used in a SET_PLOT statement.
;                 Not used but retainned for compatibility.
;
;       DEVCODE - A string array of hardcopy device code names used in
;                 the SET_PLOT statement, e.g ['PS', 'PCL', 'HP']
;                 This array must mirror DEVLIST.
;                 Not used but retainned for compatibility.
;
;       FONT_LARGE - The name of a larger font e.g 'courier_bold14'
;
;       FONT_SMALL - The name of a smaller font e.g 'courier_bold12'
;
;       EDIT_FONTS - A structure of two fonts used in the table editor
;                    adas_edtab.pro, {font_norm:'',font_input:''}.
;                    The two fonts are used to differentiate between
;                    editable and non-editable parts of the table. You
;                    may have to experiment to get two fonts which give
;                    the same default spacing in the edit widget.
;                    e.g {font_norm:'helvetica_bold14', $
;                         font_input:'helvetica_oblique14'}
;                  Not used but retainned for compatibility.
;
; OPTIONAL INPUTS:
;       None
;
; OUTPUTS:
;       None - Note this routine should leave all inputs un-modified.
;
; OPTIONAL OUTPUTS:
;       None
;
; KEYWORD PARAMETERS:
;       None
;
; CALLS:
;       ADAS414_IN    Prompts for and gathers input files
;       ADAS414_PROC  Tests files for data consistency
;       ADAS414_OUT   Prompts for and writes output files
;       XXDATE        Get date and time from operating system.
;       POUP          Puts up a messahe on the screen.
;
; SIDE EFFECTS:
;       Some system calls in the called functions.
;
; NOTES:
;       This program requires IDL v5. This is tested for at the beginning.
;       The mode of operation is different to other ADAS programs in that
;       the 3 called routines are functions which return the variable 'rep'.
;       rep can be 'CONTINUE', 'CANCEL' or 'MENU' and subsequent action
;       depends on the returned value.
;       As a side note there are no common blocks!
;
; CATEGORY:
;       Adas system.
;
; WRITTEN:
;       Martin O'Mullane
;
; MODIFIED:
;       1.1     Martin O'Mullane
;               First release
;       1.2     Richard Martin
;               Increased version no to 1.2
;       1.3     Martin O'Mullane
;               Allow the selection from all Henke elements.
;
; VERSION:
;       1.1     16-08-99
;       1.2     18-03-02
;
;-
;-----------------------------------------------------------------------------

PRO ADAS414,    adasrel, fortdir, userroot, centroot,                   $
                devlist, devcode, font_large, font_small, edit_fonts

                ;************************
                ;**** Initialisation ****
                ;************************

    adasprog = ' PROGRAM: ADAS414 V1.2'
    deffile  = userroot+'/defaults/adas414_defaults.dat'
    bitfile  = centroot+'/bitmaps'
    device   = ''
                
    
filter_elements = ['--','H ','He','Li',     $
                   'Be','B ','C ','N ','O ',$
                   'F ','Ne','Na','Mg','Al',$
                   'Si','P ','S ','Cl','Ar',$
                   'K ','Ca','Sc','Ti','V ',$
                   'Cr','Mn','Fe','Co','Ni',$
                   'Cu','Zn','Ga','Ge','As',$
                   'Se','Br','Kr','Rb','Sr',$
                   'Y ','Zr','Nb','Mo','Tc',$
                   'Ru','Rh','Pd','Ag','Cd']
                   
filter_elements = [filter_elements,         $                      
                   'In','Sn','Sb','Te','I ',$
                   'Xe','Cs','Ba','La','Ce',$
                   'Pr','Nd','Pm','Sm','Eu',$
                   'Gd','Tb','Dy','Ho','Er',$
                   'Tm','Yb','Lu','Hf','Ta',$
                   'W ','Re','Os','Ir','Pt',$
                   'Au','Hg','Tl','Pb','Bi',$
                   'Po','At','Rn','Fr','Ra',$
                   'Ac','Th','Pa','U '      ]

;     filter_elements = ['--','Li',     $
;                        'Be','C ',$
;                        'F ','Ne','Na','Mg','Al',$
;                        'Si','P ','Cl','Ar',$
;                        'Ca','Sc','Ti','V ',$
;                        'Cr','Mn','Fe','Co','Ni',$
;                        'Cu','Zn','Ga','Ge','As',$
;                        'Se','Br','Kr','Rb','Sr',$
;                        'Y ','Zr','Nb','Mo',$
;                        'Pd','Ag','Sn','Yb','Ta','W ',$
;                        'Pt', 'Au','Pb','Bi']
    
    
    diode_elements  = ['--','Si','Ge']

                ;******************************************
                ;**** Search for user default settings ****
                ;**** If not found create defaults     ****
                ;**** inval: settings for data files   ****
                ;**** outval: settings for output      ****
                ;******************************************

    files = findfile(deffile)
    if files(0) eq deffile then begin
    
        restore, deffile
        inval.centroot = centroot+'/adf35/henke/data/'
        inval.userroot = userroot+'/adf35/henke/'
    
    endif else begin
        
        inval =   { ROOTPATH    :   centroot+'/adf35/henke/data/',     $
                    FILE        :   '',                        $
                    CENTROOT    :   centroot+'/adf35/henke/data/',     $
                    USERROOT    :   userroot+'/adf35/henke/'   }
                  
        procval = { NEW         :  -1                     }

        outval  = { TEXOUT      :   1,                    $
                    TEXAPP      :   -1,                   $
                    TEXREP      :   1,                    $
                    TEXDSN      :   '',                   $
                    TEXDEF      :   userroot+'/pass/adas414_adf35.pass',          $
                    TEXMES      :   ' '                   }
    endelse                                               
    
                   
                ;***********************************************
                ;**** Make sure we are running v5 or above. ****
                ;***********************************************

    thisRelease = StrMid(!Version.Release, 0, 1)
    IF thisRelease LT '5' THEN BEGIN
       message = 'Sorry, ADAS414 requires IDL 5 or higher' 
       tmp = popup(message=message, buttons=['Accept'],font=font_large)
       goto, LABELEND
    ENDIF


LABEL100:

    ; These are fixed for each run
    
    outval.texapp   = -1
    
                ;********************************************
                ;**** Invoke user interface widget for   ****
                ;**** Data file selection                ****
                ;********************************************


    rep=adas414_in(inval,  $
                   FONT_LARGE=font_large, FONT_SMALL = font_small)
 
    if rep eq 'CANCEL' then goto, LABELEND



                ;************************************************
                ;**** Probe the file selection and show what ****
                ;**** was found. Pass back the info for      ****
                ;**** possible later processing.             ****
                ;************************************************


LABEL200:

    date   = xxdate()
    header = adasrel+adasprog+' DATE: '+date(0)+' TIME: '+date(1)

    rep=adas414_proc(procval, inval.rootpath,  devlist, devcode,        $
                     filter_elements, diode_elements,                   $
                     bitfile,    header,                                $
                     num_fil_elem = 6,                                  $
                     FONT_LARGE=font_large, FONT_SMALL = font_small)
    
    
    if rep eq 'MENU'   then goto, LABELEND
    if rep eq 'CANCEL' then goto, LABEL100


LABEL300:

                ;************************************************
                ;**** Communicate with d4spf1 in fortran and ****
                ;**** invoke user interface widget for       ****
                ;**** Output options                         ****
                ;************************************************

    
    
    rep=adas414_out(procval, outval,  inval.rootpath,  $
                    header, bitfile,                   $
                    FONT_LARGE=font_large, FONT_SMALL = font_small)
    
    
    if rep eq 'MENU'   then goto, LABELEND
    if rep eq 'CANCEL' then goto, LABEL200

    
                ;**** Back for more output options ****

    GOTO, LABEL300

LABELEND:


                ;**** Save user defaults ****

    save, inval, procval,  outval, filename=deffile


END
