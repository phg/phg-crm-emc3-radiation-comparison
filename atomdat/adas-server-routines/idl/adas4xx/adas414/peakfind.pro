; Copyright (c) 2000 Strathclyde University .
; SCCS info: Module @(#)$Header: /home/adascvs/idl/adas4xx/adas414/peakfind.pro,v 1.1 2004/07/06 14:27:28 whitefor Exp $ Date $Date: 2004/07/06 14:27:28 $  
 pro peakfind,y,min_peaksize,$
	      npeaks,peak_index,left_min,right_min
;
; ==================================================
; finds pekas and according minima
; 
; works with : y(peak)-.5*(y(left_min)+y(right_min)) > min_peaksize
;
; -> y : data array
; -> min_peaksize: minimum peak size
;
; <- npeaks : number of peaks
; <- peak_index: index of peak
; <- left_min: left minimum of peaks
; <- right_min: right minimum of peaks
;
y1 = shift(y,1)
y_1 = shift(y,-1)

; maxima and minima 
maxima = where( y gt y1 and y gt y_1, nmax) 
minima = where( y le y1 and y le y_1, nmin)
nmax = nmax

left_min = minima(0:nmax-2)
right_min = minima(1:nmax-1)
if maxima(0) lt minima(0) then maxima = maxima(1:nmax-1)
if maxima(0) gt minima(0) then maxima = maxima(0:nmax-2)


; signal-size
dy = y(maxima)-.5*(y(left_min)+y(right_min))


; check whether signal size is above threshold
index =  where (dy gt min_peaksize, npeaks)

; valid maxima
peak_index = maxima(index)
left_min   =left_min(index)
right_min  =right_min(index)

end
