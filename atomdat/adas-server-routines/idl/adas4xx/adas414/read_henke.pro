; Copyright (c) 2000 Strathclyde University .
;-----------------------------------------------------------------------------
;+
; PROJECT:
;       ADAS
;
; NAME:
;       READ_HENKE
;
; PURPOSE:
;       Reads in data in Henke X-ray data files.
;
;
; WRITTEN:
;       Ralph Dux, IPP Garching, 21-03-2000
;
; MODIFIED:
;       1.1   Ralph Dux
;              - First release.
;       1.2   Martin O'Mullane
;              - Add comments.
;              - Remove optional plot as many_plot.pro is not part of
;                IDL or ADAS.
;
; VERSION:
;       1.1   21-03-2000
;       1.2   30-11-2010
;-
;-----------------------------------------------------------------------------
pro read_henke, root, element, en_ev, y, index, $
                f1=f1, f2=f2, $
                cross=cross, $
                kappa=kappa, $
                ind_of_refr=ind_of_refr, $
                edges=edges, $
                error=error

 ;some physics constants
 re = 2.8179d-15 ; classical electron radius
 lambda_1ev = 1.2398d-6 ; wavelength for 1 eV

 ;variables for reading the file
 nx = 600
 ny =  2
 xx = 1.
 yy = dblarr(ny)
 en_ev = dblarr(nx)
 ff1 = dblarr(nx)
 ff2 = dblarr(nx)
 header =  ' '

 on_ioerror, not_valid
 
 file_name = root+strtrim(strlowcase(element),2)
 openr,unit,file_name,/get_lun, error=error
 if (error ne 0) then begin
    print, file_name, ' does not exist!'
    return
 endif
 readf,unit, header

 ix =  -1
 read_line:
 readf, unit, xx, yy
 ix =  ix + 1
 en_ev(ix) = xx
 ff1(ix) =  yy(0)
 ff2(ix) =  yy(1)
 goto, read_line

 not_valid:    
 close,unit
 free_lun,unit

 en_ev =  en_ev(0:ix)
 ff1 =  ff1(0:ix)
 ff2 =  ff2(0:ix)

 nx =  ix+1
;  print, element, ' number of values:', nx

 if keyword_set(f1) then begin
    y = ff1
 endif
 if keyword_set(f2) then begin
    y = ff2
 endif
 if keyword_set(cross) then begin
    const =  2.d0*re*lambda_1ev
    y =  const/en_ev * ff2
 endif
 if keyword_set(kappa) then begin
    period_system, dummy, element, /sym, $
       number_density = number_density
    const =  2.d0*re*lambda_1ev*number_density(0)
    y =  const / en_ev * ff2
 endif
 if keyword_set(ind_of_refr) then begin
    period_system, dummy, element, /sym, $
       number_density = number_density
;  only above 30 eV ff1 is tabulated    
    wh = where(en_ev gt 30.)
    en_ev =  en_ev(wh)
    const =  re*lambda_1ev^2*number_density(0)/2.d0/!DPi
    y =  1.d0 - const/en_ev^2 * complex(ff1, ff2)
 endif

 if keyword_set(edges) then begin
    peakfind,1./deriv(alog(en_ev))-60.,30.,npeak,index
    edges = en_ev(index)
 endif
 
 
end
