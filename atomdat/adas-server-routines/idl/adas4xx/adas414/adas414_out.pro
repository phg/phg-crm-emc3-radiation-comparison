; Copyright (c) 2000 Strathclyde University .
;+
; PROJECT:
;       ADAS 
;
; NAME:
;	ADAS414_OUT
;
; PURPOSE: 
;       This function does the actual work. Initially it puts up a widget 
;       asking for the output directory/directories of the 'new' adf10
;       files. There are 3 choices - send all updated adf10 files to a
;       passing subdirectory, replace the old adf10 with the updated ones
;       or write to a different set of adf10 files. An option to write
;       a summary paper.txt is also offered.
;
;
; EXPLANATION:
;       The selection of adf10 directory structure output files is
;       similar to that of ADAS414_IN. However the root is forced to
;       'userroot+/adas/adf10/' and the sequence is taken from the adf10
;       input file. As in ADAS414_IN a template of the adf10
;       isoelectronic files, with XXX replacing the data class (acd,
;       plt etc.) is dynamically constructed and displayed. 
;       The output file destination is checked for permission to 
;       write and, importantly, whether it is a directory.
;
;
; NOTES:
;       The comments in the adf10 files are handled a little clumsily and
;       this is where the program is most likely to break. The assumption
;       is that the comments are in z1 order, and that they are seperated
;       by a line of '----' strating with a '-'- in the FIRST column. All
;       the comments in the adf10 file are read into a string array with
;       an index array holding the start position of cmment z1(i).
;       See the update procedures for an explanation of the algorithm
;       for positioning the supplementary data into the new adf10 file. 
; 
;
;
; INPUTS:
;        outval       - A structure holding the 'remembered' outout options.
;                       appbut      : append to end of paper.txt
;                       repbut      : replace paper.txt
;                       filename    : name of paper.txt
;                       defname     : 'paper.txt' - the default!
;                       message     : error message for paper.txt
;                       out_type    : 0,1,2 - pass directory, replace adf10
;                                             or new adf10 files.
;                       out_pass    : adf10 passing directory
;                       out_defpass : userroot+'/pass/' - default
;                       out_root    : root of new adf10
;                       out_year    : year of new adf10
;                       out_prefix  : prefix of new adf10
;                       out_seq     : sequence of new adf10
; 
;        Note : out_root, out_seq and out_appbut are set afresh for
;               each run. 
;
;        dataclasses  - A string array of the possible dataclasses
;                       'acd','scd','ccd','prb','prc','qcd','xcd',
;                       'plt','pls' and 'met'
;
;        header       - Output header; version, date etc.
;
;        match_report - string array from ADAS414_PROC reporting on
;                       files found for writing to paper.txt.
;         
;        bitfile      - directory of bitmaps for menu button
;
;
; OPTIONAL INPUTS:
;       None
;
; OUTPUTS:
;       The function returns a string rep
;           rep = 'CONTINUE' if everything is ok
;           rep = 'CANCEL' if there is a problem or indeed if the user
;                          closes the widget with the mouse. 
;           rep = 'MENU' if user want to exit to menu at this point
;
; OPTIONAL OUTPUTS:
;       None
;
; KEYWORD PARAMETERS:
;       FONT_LARGE - The name of a larger font e.g 'courier_bold14'
;
;       FONT_SMALL - The name of a smaller font e.g 'courier_bold12'
;
;
; CALLS:
;                            
;       ADAS414_OUT_ENTRY        : Reacts to events relevant to constructing
;                                  adf10 filenames.
;       ADAS414_OUT_NULL_EVENTS  : Captures events and does nothing (function)
;       ADAS414_OUT_NULLS        : Captures events and does nothing (procedure)
;       ADAS414_OUT_SWITCH       : Switches between 3 possible output options
;                                  which are mapped/unmapped here.
;       ADAS414_OUT_MENU         : React to menu button event. The standard IDL
;                                  reaction to button events cannot deal with
;                                  pixmapped buttons. Hence the special handler.
;       ADAS414_OUT_EVENT        : Reacts to cancel and Done. Does the file
;                                  existence error checking.
;       ADAS414_UPDATE_TYPE1     : Updates adf10 for datacalsses 0-6.
;       ADAS414_UPDATE_TYPE2     : Updates adf10 for datacalsses 7-9.
;
; SIDE EFFECTS:
;
; CATEGORY:
;	Widgets
;
; WRITTEN:
;       Martin O'Mullane
;
; MODIFIED:
;	1.1	Martin O'Mullane
;		  - First release 
;	1.2	Martin O'Mullane
;		  - Syntax error in menu return block.
;	1.3	Martin O'Mullane
;		  - Put space before name of producer.
;
; VERSION:
;       1.1	16-08-1999
;       1.2	16-01-2002
;       1.3	16-03-2010
;
;-
;-----------------------------------------------------------------------------


PRO ADAS414_OUT_MENU, event

; The 'normal' get_value from a widget_button does not work with a
; bitmap button. Hence this specialised event handler.

   ; Get the info structure out of the user value of the top-level base.

Widget_Control, event.top, Get_UValue=info


; Something nefarious may cause an error also.
;  -- if so trap it and exit gracefully

CATCH, error
IF error NE 0 THEN BEGIN
   formdata = {cancel : 0, menu:1}
   *info.ptrToFormData = formdata
   Widget_Control, event.top, /Destroy
   message,!err_string
   RETURN
ENDIF


 formdata = {cancel : 0, menu:1}
 *info.ptrToFormData =formdata
 widget_Control, event.top, /destroy

END


;-----------------------------------------------------------------------------


PRO ADAS414_OUT_EVENT, event

; React to button events - Cancel and Done but not menu (this requires a
; specialised event handler ADAS414_OUT_MENU). Also deal with the passing
; directory output Default button here.

; On pressing Done check for the following


   ; Get the info structure out of the user value of the top-level base.

Widget_Control, event.top, Get_UValue=info

; Something nefarious may cause an error also.
;  -- if so trap it and exit gracefully

CATCH, error
IF error NE 0 THEN BEGIN
   formdata = {cancel:1, menu : 0}
   *info.ptrToFormData = formdata
   Widget_Control, event.top, /Destroy
   print,!err_string
   RETURN
ENDIF

Widget_Control, event.id, Get_Value=userEvent

CASE userEvent OF


  'Cancel' : begin
               formdata = {cancel:1, menu : 0}
               *info.ptrToFormData =formdata
               widget_Control, event.top, /destroy
             end 

      
  'Done'   : begin
                 
                ; gather the data for return to calling program
                
                err  = 0
                mess = ' '
                
                widget_Control, info.paperID, Get_Value=pap
                
                ; no need to set mess as it is flagged in the cw
                if pap.outbut EQ 1 AND strtrim(pap.message) NE '' then err=1
                
                
                if err EQ 0 then begin
                   formdata = { paper    : pap,    $
                                cancel   : 0,      $
                                menu     : 0       }
                   *info.ptrToFormData = formdata
                   widget_control, event.top, /destroy
                endif else begin
                   widget_Control, info.messID, Set_Value=mess
                endelse

             end
             
             
  else : print,'ADAS414_OUT_EVENT : You should not see this message! ',userEvent
                   
ENDCASE

END
  
;-----------------------------------------------------------------------------



FUNCTION ADAS414_OUT, procval , outval,  henkeroot,                     $ 
                      header,  bitfile,                                 $ 
                      FONT_LARGE = font_large, FONT_SMALL = font_small


; Set defaults for keywords and extract info for paper.txt question

  IF n_elements(font_large) eq 0 THEN font_large = ''
  IF n_elements(font_small) eq 0 THEN font_small = ''


  paperval =  { outbut   : outval.TEXOUT, $
                appbut   : outval.TEXAPP, $
                repbut   : outval.TEXREP, $
                filename : outval.TEXDSN, $
                defname  : outval.TEXDEF, $
                message  : outval.TEXMES  }
  


                ;********************************************
		;**** create modal top level base widget ****
                ;********************************************
                
  
  parent = Widget_Base(Column=1, Title='ADAS414 OUTPUT', $
                       XOFFSET=100, YOFFSET=1)

                
  rc = widget_label(parent,value='  ',font=font_large)


                ;****************************************
		;**** Ask for output filter file -   ****
                ;****************************************

  base    = widget_base(parent, /column)
  
  mrow    = widget_base(base,/frame)
  paperID = cw_adas_outfile(mrow, OUTPUT='Filter Output',   $
                                 VALUE=paperval, FONT=font_large)
  
  
                            
                ;************************************
		;**** Error/Instruction message. ****
                ;************************************
                
  messID = widget_label(parent,value='     Choose output options   ',font=font_large)
                      
                      
                            
                ;*****************
		;**** Buttons ****
                ;*****************
                
  menufile = bitfile + '/menu.bmp'
  read_X11_bitmap, menufile, bitmap1
                
  mrow     = widget_base(parent,/row)
  menuID   = widget_button(mrow,value=bitmap1,font=font_large, $
                           event_pro='ADAS414_OUT_MENU')
  cancelID = widget_button(mrow,value='Cancel',font=font_large)
  doneID   = widget_button(mrow,value='Done',font=font_large)





                ;***************************
		;**** Put up the widget ****
                ;***************************

; Realize the ADAS414_OUT input widget.

   dynlabel, parent
   widget_Control, parent, /realize

; Create a pointer to store the information collected from the form.
; The initial data stored here is set to CANCEL, so nothing needs to
; be done if the user kills the widget with the mouse.

   ptrToFormData = Ptr_New({cancel:1, menu:0})

; Create an info structure with program information.

  info = { messID          :  messID,             $
           paperID         :  paperID,            $
           ptrToFormData   :  ptrToFormData       }  
            
               
; Store the info structure in the user value of the top-level base and launch
; the widget into the world.

  widget_control, parent, Set_UValue=info

  XManager, 'ADAS414_OUT', parent, Event_Handler='ADAS414_OUT_EVENT'


; When the widget is destroyed, the block is released, and we
; return here. Get the form data that was collected by the form
; and stored in the pointer location.

formdata = *ptrToFormData


rep = 'CONTINUE'
IF N_Elements(formdata) EQ 0 THEN BEGIN
   Ptr_Free, ptrToFormData
   rep = 'CANCEL'
ENDIF
 
IF formdata.cancel EQ 1 THEN BEGIN
   Ptr_Free, ptrToFormData
   rep = 'CANCEL'
   RETURN,rep
ENDIF

IF formdata.menu EQ 1 THEN BEGIN
   Ptr_Free, ptrToFormData
   rep = 'MENU'
   RETURN,rep
ENDIF

if rep eq 'CONTINUE' then begin
   
    outval =  { TEXOUT      :   formdata.paper.outbut,    $
                TEXAPP      :   formdata.paper.appbut,    $
                TEXREP      :   formdata.paper.repbut,    $
                TEXDSN      :   formdata.paper.filename,  $
                TEXDEF      :   formdata.paper.defname,   $
                TEXMES      :   formdata.paper.message    } 
   ; Free the pointer.
   Ptr_Free, ptrToFormData

endif       
  

; Write paper.txt file if requested

  if outval.texout EQ 1 then begin
  
     filter_type = 'D'
     get_henke_data, henkeroot, procval.elements , procval.thick,       $
                     procval.diode, procval.thick_diode,                $
                     data_type = filter_type,                           $
                     xdata  = x,  ydata = y, edges = edges
     
     tmp       = size(x)
     nx        = tmp[1]
     nedges    = n_elements(edges)
     nelem     = n_elements(procval.elements)
     iopt_low  = 0
     iopt_high = 1
     
     ep   =  x[*,0]
     sens =  alog10(y[*,0])
     sens = sens > (-74.D0)
     
     date = xxdate()
     name = xxuser()
     
     openw,  lun, outval.texdsn, /get_lun
     
     printf, lun, nx, nedges, iopt_low, iopt_high, f='(4(i5))'
     printf, lun, f='(80("-"))'
     printf, lun, edges, f='(8(f10.3))'
     printf, lun, f='(80("-"))'
     
     printf, lun, ep, f='(8(f10.3))'
     printf, lun, sens, f='(8(f10.5))'
     
     printf, lun, f='("C",79("-"))'
     printf, lun, f='("C")'
     printf, lun, f='("C  Photon energy vs. Transmission of filter (T) x absorption in detector (A)")'
     printf, lun, f='("C")'
     printf, lun, f='("C  Units:  photon energy in eV")'
     printf, lun, f='("C          [T*A] in logarithm base 10")'
     printf, lun, f='("C")'
     printf, lun, f='("C  Data Source: http://xray.uu.se")'
     printf, lun, f='("C")'
     printf, lun, f='("C Energies of absorption edges (eV)")'
     printf, lun, edges, f='("C   ",8(f10.3))'
     printf, lun, f='("C")'
     
     printf, lun, f='("C  Filter :")'
     printf, lun, f='("C  Element   Thickness (micron)")'
     for j=0,nelem-1 do begin
       printf, lun, f='("C    ",a2,"     ",f10.5)', procval.elements(j),procval.thick(j)
     endfor
     printf, lun, f='("C")'
                  
     printf, lun, f='("C  Diode (absorber)")'
     printf, lun, f='("C  Element   Thickness (micron)")'
     printf, lun, f='("C    ",a2,"     ",f10.5)', procval.diode,procval.thick_diode
     printf, lun, f='("C ")'  
               
     
     printf,lun, 'C  Code     : ADAS414'
     printf,lun, 'C  Producer : ',name(0)
     printf,lun, 'C  Date     : ',date(0)
     printf,lun, 'C'  
     printf, lun, f='("C",79("-"))'
     
     close, lun
     free_lun, lun
     
  endif 



; And tell ADAS414 that we are finished

RETURN,rep

END
