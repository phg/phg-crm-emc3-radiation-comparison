; Copyright (c) 2000 Strathclyde University .
; SCCS info: Module @(#)$Header: /home/adascvs/idl/adas4xx/adas414/adas414_proc.pro,v 1.2 2011/12/08 10:53:16 mog Exp $ Date $Date: 2011/12/08 10:53:16 $  
;+
; PROJECT:
;       ADAS
;
; NAME:
;        ADAS414_PROC
;
; PURPOSE:
;        Generates ADAS414 graphical processing.
;
; EXPLANATION:
;        This routine creates a window for the display of graphical
;        output. A separate routine PLOT414 actually plots the
;        graph.
;
; USE:
;        This routine is specific to ADAS414, see b1outg.pro for
;        example use.
;
; INPUTS:
;        (Most of these inputs map exactly onto variables of the same
;         name in the FORTRAN code.)
;
;
;        DSFULL   - String; Name of data file
;
;        TITLX    - String; user supplied comment appended to end of title
;
;        UTITLE   - String; Optional comment by user added to graph title.
;
;        TEMP         - Double array; temperatures from input data file.
;
;        RATE     - Double array; Excitation rate coefficient of selected
;                                 transition from input data file
;
;        TOSA         - Double array; Selected temperatures for spline fit
;
;        ROSA         - Double array; Excitation rate coefficients for splinefit.
;
;        TOMA         - Double array; Selected temperatures for minimax fit
;
;        DATE         - String; Date of graph production
;
;        LDEF1         - Integer; 1 - use user entered graph scales
;                            0 - use default axes scaling
;
;        XMIN    .- String; Lower limit for x-axis of graph, number as string
;
;        XMAX    .- String; Upper limit for x-axis of graph, number as string
;
;        YMIN    .- String; Lower limit for y-axis of graph, number as string
;
;        YMAX    .- String; Upper limit for y-axis of graph, number as string
;
;        LFSEL         - Integer; 0 - No minimax fitting was selected
;                            1 - Mimimax fitting was selected
;
;        LOSEL    - Integer; 0 - No interpolated values for spline fit.
;                            1 - Intepolated values for spline fit.
;
;        NMX         - Integer; Number of temperatures used for minimax fit
;
;        NENER    - Integer; Number of user entered temperature values
;
;        NPSPL         - Integer; Number of interpolated values for spline.
;
;        HRDOUT   - Integer; 1 if hardcopy output activated, 0 if not.
;
;        HARDNAME - String; Filename for harcopy output.
;
;        DEVICE   - String; IDL name of hardcopy output device.
;
;        HEADER   - String; ADAS version number header to include in graph.
;
;        BITFILE  - String; the path to the dirctory containing bitmaps
;                   for the 'escape to series menu' button.
;
; OPTIONAL INPUTS:
;        None.
;
; OUTPUTS:
;       REP  -  'MENU' if user has selected 'escape direct to series menu , 
;                otherwise 'CONTINUE'
;
; OPTIONAL OUTPUTS:
;        None.
;
; KEYWORD PARAMETERS:
;        FONT        - String; The name of a font to use for text in the
;                  graphical output widget.
;
; CALLS:
;        CW_ADAS_GRAPH        Graphical output widget.
;        PLOT414                Make one plot to an output device for 414.
;        XMANAGER
;
; SIDE EFFECTS:
;
;        One other routine is included in this file;
;        ADAS414_PLOT_EV        Called via XMANAGER during widget management.
;
; CATEGORY:
;        Adas system.
;
; WRITTEN:
;       Martin O'Mullane
;
; MODIFIED:
;       1.1     Martin O'Mullane
;               First release
;       1.2     Martin O'Mullane
;               Replace droplists with combobox to avoid the list of
;               elements going off the end of the screen. This also
;               allows all of Henke's elements to be offered.
;
; VERSION:
;       1.1     09-11-99
;       1.2     08-12-2011
;
;-
;----------------------------------------------------------------------------

PRO plot414, winNum,  x, y, title,                     $
             xu_ind, xunits, xtitles, yunits, ytitle,  $
             xmin, xmax, ymin, ymax, xlog, ylog,       $
             current_plot, PRINT = print

  
  COMMON Global_lw_data, left, right, top, bottom, grtop, grright


; Set up plot decoration

    charsize    = (!d.y_vsize/!d.y_ch_size)/60.0 
    small_check = GETENV('VERY_SMALL')
    if small_check eq 'YES' then charsize=charsize*0.8
    
    xunit  = xunits(xu_ind)
    xtitle = xtitles(xu_ind)
    gtitle = title(0) + "!C" + title(1) + "!C" + title(2) 
    
  
; Choose appropriate output device
    
    if (keyword_set(print)) then begin
    help,print,/st
     
       grtype = print.devcode( where(print.dev eq print.devlist) )
       set_plot,grtype
     
       if print.paper eq 'A4' then begin
          xsize = 24.0
          ysize = 18.0
       endif else begin
          xsize = 23.0
          ysize = 17.0
       endelse
       
;       !p.font=0
       device,/color,bits=8,filename=print.dsn,           $
              /landscape,  xsize=xsize, ysize=ysize

     endif else begin
     
        wset, winNum
     
     endelse


     
; Let's do some plotting - default to log/lin

    
    xstyle = 1
    ystyle = 0
    
    dxtitle=xtitle+xunit
    
    plot, [xmin,xmax], [ymin,ymax], /nodata, ticklen=1.0, 		$
	     position=[left,bottom,grright,grtop], 			$
	     xtitle=dxtitle, ytitle=ytitle, xstyle=xstyle, ystyle=xstyle, 	$
	     charsize=charsize, xlog=xlog, ylog=ylog
             
    
    np = size(x,/dimensions)
    if n_elements(np) EQ 1 then np = 1 else np = np[1]        
    
    
    for j = 0, np-1 do begin         
    
      xvals = where(x ge xmin and x le xmax, count_x)
      yvals = where(y ge ymin and y le ymax, count_y)
      if (count_x EQ 0 OR count_y EQ 0) then makeplot = 0 else makeplot = 1

      ival = n_elements(x)
      if makeplot eq 1  then begin
         oplot, x(*,j), y(*,j), linestyle=j
      endif else begin
         xyouts, grright,grtop,'ADAS414 : No data found in these axes ranges',/normal
         print,'ADAS414 : No data found in these axes ranges'
      endelse
    
    endfor
    
    ; annotate with extra information on top
    
     xyouts, (left-0.05), (top+0.05), gtitle, /normal, alignment=0.0, 	$
	      charsize = charsize, color=17
    
    ; and RHS labels
    
    

; Reset device to X

     if (keyword_set(print)) then begin
       device,/close
       set_plot,'X'
       !p.font=-1
     endif

END

;----------------------------------------------------------------------------

PRO get_filter_data, filter_elements, diode_elements ,        $
                     filter_elemID  , filter_thickID ,        $
                     diode_elemID   , diode_thickID  ,        $
                     elements       , thick          ,        $
                     diode          , thick_diode

nelem = 0

; Read data from widgets

  num_fil = n_elements(filter_elemID)
  for j=0, num_fil-1 do begin
     res = widget_info(filter_elemID[j], /combobox_gettext)
     iel = where(strpos(filter_elements, res) NE -1)
     iel = iel[0]
     widget_control, filter_thickID[j], get_value=text_th
     if iel NE 0 AND text_th[0] NE '' then begin
        if nelem EQ 0 then begin
           elements = filter_elements[iel]
           reads,text_th[0], th
           thick = float(th)
        endif else begin
           elements = [elements,filter_elements[iel]]
           reads,text_th[0], th
           th = float(th)
           thick = [thick,th]
        endelse
        nelem=nelem+1
     endif
  endfor
 
  thick_diode = 0.0
  res = widget_info(diode_elemID, /combobox_gettext)
  idio = where(strpos(diode_elements, res) NE -1)
  idio = idio[0]
  widget_control, diode_thickID, get_value=text_th
  if idio NE 0 AND text_th[0] NE '' then begin
     diode = diode_elements[idio]
     reads,text_th[0], th
     thick_diode = float(th)
  endif 
 
END

;-----------------------------------------------------------------------------


PRO ADAS414_IN_NULLS, event

   ; The purpose of this event handler is to do nothing
   ;and ignore all events that come to it.
   
END 

;-----------------------------------------------------------------------------


PRO ADAS414_MINMAX_CONTROL, minmaxIDs, xmin, xmax, ymin, ymax, control = control

  IF n_elements(control) eq 0 THEN return
  
  cval = strupcase(control)
  
  if cval EQ 'SET' then begin
   
     xmin = strtrim(string(xmin), 1)
     xmax = strtrim(string(xmax), 1)
     ymin = strtrim(string(ymin), 1)
     ymax = strtrim(string(ymax), 1)
     widget_control, minmaxids[0], set_value=xmin
     widget_control, minmaxids[1], set_value=xmax
     widget_control, minmaxids[2], set_value=ymin
     widget_control, minmaxids[3], set_value=ymax
  
  endif

  if cval EQ 'GET' then begin
   
     widget_control, minmaxids[0], get_value=d1
     widget_control, minmaxids[1], get_value=d2
     widget_control, minmaxids[2], get_value=d3
     widget_control, minmaxids[3], get_value=d4
     
     reads, d1[0], xmin
     reads, d2[0], xmax
     reads, d3[0], ymin
     reads, d4[0], ymax
  
  endif
   
END 

;-----------------------------------------------------------------------------
PRO ADAS414_PROC_TEXT, event

; This event handler captures all text input - only accept numbers 
; ie '0-9', '.', 'd', 'e', '+' and '-'


; Only allow insertion/deletion - no text select

  
  
  Widget_Control, event.id,  Get_Value=userEvent


  accept_codes = [43, 45, 46, 48, 49, 50, 51, 52, 53, 54, 55, 56, 57, 100, 101]
  
  
  ; Handle text insertion events

     IF event.type EQ 0 THEN BEGIN

        valid_entry = where(byte(event.ch) EQ accept_codes, count)
        
        Widget_Control, event.id, Get_Value=text
        text   = text(0)
        length = StrLen(text)
        
        ; only react if there are less than 8 characters there already,
        
        if length LT 8  AND count NE 0 then begin 
           selection = Widget_Info(event.id, /Text_Select)
           Widget_Control, event.id, /Use_Text_Select, Set_Value=String(event.ch)
           Widget_Control, event.id, Set_Text_Select=event.offset + 1
        endif

     ENDIF 

  ; Must also deal with deletions

     IF event.type EQ 2 THEN BEGIN

        Widget_Control, event.id, Get_Value=text
        text = text(0)
        length = StrLen(text)

        Widget_Control, event.id, Set_Value=StrMid(text, 0, length-event.length)
        Widget_Control, event.id, Set_Text_Select=event.offset

     ENDIF


   

END 

;----------------------------------------------------------------------------

PRO ADAS414_PROC_PLOT_EVENT, event

; React to the plot/interaction button events.

; Get the info structure out of the user value of the top-level base.

Widget_Control, event.top, Get_UValue=info, /no_copy

can_update = 'YES'
update     = 'YES'

CASE event.id OF

  info.replotID : Begin
                    update = 'YES'
                  End

  info.xsecID   : Begin
                    info.current_plot = 'C'
                    info.ytitle = 'Cross Section'
                    info.yunits = 'Cm!U2!N'
                  End

  info.f2ID     : Begin
                    info.current_plot = 'F'
                    info.ytitle = 'F2-factor'
                    info.yunits = '  '
                  End

  info.transID  : Begin
                    info.current_plot  = 'T'
                    info.ytitle = 'Transmission fraction'
                    info.yunits = '  '
                  End

  info.filterID : Begin
                    info.current_plot = 'D'
                    info.ytitle = 'Detectoe efficiency'
                    info.yunits = '  '
                  End

  ELSE  :  can_update = 'NO'

ENDCASE

if can_update EQ 'NO' then begin
   print, 'ADAS414_PROC_PLOT_EVENT - there should be nothing else'
endif else begin

   get_filter_data,info.filter_elements, info.diode_elements,  $
                   info.filter_elemID  , info.filter_thickID,  $
                   info.diode_elemID   , info.diode_thickID,   $
                   elements            , thick ,               $
                   diode               , thick_diode 
                  
   get_henke_data, info.henkeroot, elements , thick,       $
                   diode,thick_diode,                      $
                   data_type = info.current_plot,          $
                   xdata  = x,  ydata = y, for_plot=1

   *info.ptrToXY = { x : x, y:y}

   info.xmin  = min(x, max = xmax)
   info.xmax  = xmax * 1.1
   ymin       = min(y, max = ymax)
   info.ymin  = ymin * 0.9
   info.ymax  = ymax *1.1
   
   if info.xu_ind EQ 1 then begin
   
       const       = 1.239842447e4
       x           = const / x
       xmin        = const / info.xmax
       xmax        = const / info.xmin
       info.xmin   = xmin
       info.xmax   = xmax
   
   endif 
   
   plot414,info.winNum, x, y,  info.title,    $
           info.xu_ind, info.xunits, info.xtitles,      $ 
           info.yunits, info.ytitle,                    $
           info.xmin, info.xmax, info.ymin,info. ymax,  $
           info.xlog, info.ylog, info.current_plot
  
  
  if XREGISTERED('adas414_adjust') AND update EQ 'YES' then             $
     adas414_minmax_control, info.minmaxids,                            $
                             info.xmin,info.xmax,info.ymin,info.ymax,   $
                             control='SET'


  
endelse


Widget_Control, event.top, Set_UValue=info, /no_copy


END

;----------------------------------------------------------------------------

PRO ADAS414_PROC_EVENT, event

; Get the info structure out of the user value of the top-level base.

Widget_Control, event.top, Get_UValue=info, /no_copy


; Now react to event.action as sent from cw_adas_graph
;    only accept 'bitbutton' - return to menu button pressed
;                'print'     
;                'done'
;                'adjust'
;                'mouse'  
;
; The 'update' event is sent from adas414_adjust.


  
CASE event.action OF
  
  'done'      : Begin
                   get_filter_data,info.filter_elements, info.diode_elements, $
                                   info.filter_elemID , info.filter_thickID,  $
                                   info.diode_elemID  , info.diode_thickID,   $
                                   elements           , thick             ,   $
                                   diode              , thick_diode

                   formdata = { elements     :  elements,     $
                                thick        :  thick,        $
                                diode        :  diode,        $
                                thick_diode  :  thick_diode,  $
                                cancel       :  0,            $
                                menu         :  0             }
                   *info.ptrToFormData = formdata
                   widget_control, event.top, /destroy
                End
  
  'print'     : Begin
                 
                  print_val = { dsn     :  '',                 $
                                defname : 'adas414.ps',        $
                                replace :  1,                  $
                                devlist :  info.devlist,       $
                                dev     :  info.devlist[0],    $
                                paper   :  'A4',               $
                                write   :  0,                  $
                                message :  'Note: cannot append to graphics files'}

                  act = 0
                  adas_file_gr, print_val, act, font=info.font_large, $
                                title='File name for graphical output'
                 
                  
                  if print_val.write EQ 1 then begin                
                  
                     print_stuff = { dsn     : print_val.dsn,   $
                                     devcode : info.devcode,    $
                                     devlist : info.devlist,    $
                                     dev     : print_val.dev,   $
                                     paper   : print_val.paper  }
                     xydata = *info.ptrToXY
                 
                     plot414,info.winNum, xydata.x, xydata.y,  info.title,   $
                             info.xu_ind, info.xunits, info.xtitles,         $ 
                             info.yunits, info.ytitle,                       $
                             info.xmin, info.xmax, info.ymin,info. ymax,     $
                             info.xlog, info.ylog, info.current_plot,        $
                             PRINT=print_stuff

	              message = 'Plot  written to print file.'
	              grval   = {WIN:0, MESSAGE:message}
	              widget_control, event.id, set_value=grval
                   
                   endif
               
                End

  'adjust'    : Begin
                   
                   if info.xlog EQ 0 AND info.ylog EQ 0 then gsel = 0 
                   if info.xlog EQ 1 AND info.ylog EQ 0 then gsel = 1 
                   if info.xlog EQ 0 AND info.ylog EQ 1 then gsel = 2 
                   if info.xlog EQ 1 AND info.ylog EQ 1 then gsel = 3 
                  
                   adas414_adjust, group_leader=info.gleader,    $
                                   notify = info.updateID,       $
                                   font_large = info.font_large, $
                                   font_small = info.font_small, $
                                   xunits = info.xunits,         $
                                   gsel = gsel,                  $
                                   minmaxIDs = minmaxIDs
                                   
                   info.minmaxIDs = minmaxIDs
                   adas414_minmax_control, minmaxids,                     $
                               info.xmin,info.xmax,info.ymin,info.ymax,   $
                               control='SET'
                                                                     
                End

  'update'    : Begin
  
                  xydata = *info.ptrToXY
                  CASE event.data.axes_type OF
                     0 : begin 
                            info.xlog = 0
                            info.ylog = 0 
                         end
                     1 : begin 
                            info.xlog = 1
                            info.ylog = 0 
                         end
                     2 : begin 
                            info.xlog = 0
                            info.ylog = 1 
                         end
                     3 : begin 
                            info.xlog = 1
                            info.ylog = 1 
                         end
                  ENDCASE
                  
                  adas414_minmax_control, info.minmaxids,          $
                                          xmin,xmax,ymin,ymax,     $
                                          control='GET'

                  info.xmin = xmin
                  info.xmax = xmax
                  info.ymin = ymin
                  info.ymax = ymax

                  const = 1.239842447e4
                   x    = xydata.x
                  
                  if event.data.xunits_type EQ 1 AND info.xu_ind EQ 0 then begin
                  
                      x           = const / xydata.x
                      xmin        = const / info.xmax
                      xmax        = const / info.xmin
                      info.xmin   = xmin
                      info.xmax   = xmax
                      info.xu_ind = event.data.xunits_type
                  
                  endif 
                  
                  if event.data.xunits_type EQ 0 AND info.xu_ind EQ 1 then begin
                  
                      x           = const / xydata.x
                      xmin        = const / info.xmax
                      xmax        = const / info.xmin
                      info.xmin   = xmin
                      info.xmax   = xmax
                      info.xu_ind = event.data.xunits_type
                  
                  endif 
                  
                  
                  CASE event.data.yunits_type OF 
                    -1 : y = xydata.y
                     0 : y = xydata.y
                     1 : y = xydata.y
                  ENDCASE
                  
                  *info.ptrToXY = { x : x, y:y}
            
                  plot414,info.winNum,  x, y,  info.title,             $
                          info.xu_ind, info.xunits, info.xtitles,      $ 
                          info.yunits, info.ytitle,                    $
                          info.xmin, info.xmax, info.ymin,info.ymax,   $
                          info.xlog, info.ylog, info.current_plot
                          
                  ;  print,info.xmin,info.xmax,info.ymin,info.ymax
                  ;  print,x(0),x(100),y(0),y(100)
    ;DEBUG        ;  print, event.data.xunits_type,info.xu_ind
                  ;
                    print,'---'
                  adas414_minmax_control, info.minmaxids,          $
                                          xmin,xmax,ymin,ymax,     $
                                          control='SET'
                    
                End


  'bitbutton' : Begin
                   formdata = {cancel:0, menu : 1}
                   *info.ptrToFormData = formdata
                   widget_Control, event.top, /destroy
                End
    
  'mouse'    : Begin
                   print,'Have got a mouse event'
               End
    
  else : print,'ADAS414_OUT_EVENT : You should not see this message! '
             
                   
ENDCASE

if NOT( event.action EQ 'done' OR event.action EQ 'bitbutton') then $
   Widget_Control, event.top, Set_UValue=info, /no_copy
    
    
END

;----------------------------------------------------------------------------
   
FUNCTION adas414_proc, procval, henkeroot, devlist, devcode,             $
                       filter_elements, diode_elements,                  $
                       bitfile  ,  header,                               $
                       num_fil_elem = num_fil_elem,                      $
                       FONT_LARGE = font_large, FONT_SMALL = font_small

  IF n_elements(font_large) eq 0 THEN font_large = ''
  IF n_elements(font_small) eq 0 THEN font_small = ''
  IF n_elements(num_fil_elem) eq 0 THEN num_fil_elem = 7

    
                ;*************************************
                ;**** Create graph display widget ****
                ;*************************************

    parent = widget_base(TITLE='ADAS414 PROCESSING',      $
                          XOFFSET=1,YOFFSET=1)
    device, get_screen_size=scrsz
    xwidth = scrsz(0)*0.60
    yheight = scrsz(1)*0.65
    
    base = widget_base(parent,/row)
    
    bitval = bitfile + '/menu.bmp'

    multiplot = 0
    adjust = 1
    winID  = cw_adas_graph(base, print=1, FONT=font_large,                $
                           xsize=xwidth, ysize=yheight,                   $
                           multiplot=multiplot, bitbutton=bitval,         $
                           adjust=adjust, mouse_active=0, backstore=1)

    base = widget_base(base,/frame)
    base = widget_base(base,/column)
  
    
; Elements of the filter and thichnesses
    
    
    filter_elemID  = lonarr(num_fil_elem)
    filter_thickID = lonarr(num_fil_elem)
    
    label = widget_label(base,value='Element/Thickness (um)',font=font_small)
    label = widget_label(base,value=' ',font=font_small)
    
    for j =0,num_fil_elem-1 do begin
       tbase = widget_base(base,/row)
       filter_elemID[j]  = widget_combobox(tbase, value=filter_elements, $
                           event_pro='ADAS414_IN_NULLS',                 $
                           font=font_small )
       filter_thickID[j] = widget_text(tbase, value='', font=font_small, $
                            xsize=8,event_pro='ADAS414_PROC_TEXT',       $
                            all_events=1, editable=0)
    endfor

    label    = widget_label(base,value=' ',font=font_small)



; Diode (absorber) material and thichness

    
    label = widget_label(base,value='Diode/Thickness (um)',font=font_small)
    label = widget_label(base,value=' ',font=font_small)
    
    tbase = widget_base(base,/row)
    diode_elemID  = widget_combobox(tbase, value=diode_elements,  $
                        event_pro='ADAS414_IN_NULLS',             $       
                        font=font_small )
    diode_thickID = widget_text(tbase, value='', font=font_small, $
                         xsize=8,event_pro='ADAS414_PROC_TEXT',   $
                         all_events=1, editable=0)



; Buttons for altering/interacting with the plot.
; Put info into a separate base for reacting to these events 
    
    interactbase = widget_base(base)
    
    label    = widget_label(base,value=' ',font=font_small)
    replotID = widget_button(base,value='Plot Now', font=font_large, $
                             event_pro = 'ADAS414_PROC_PLOT_EVENT')
    label    = widget_label(base,value=' ',font=font_small)

    xsecID   = widget_button(base,value='Cross-Section', font=font_large, $
                             event_pro = 'ADAS414_PROC_PLOT_EVENT')
    f2ID     = widget_button(base,value='F2-Factor', font=font_large, $
                             event_pro = 'ADAS414_PROC_PLOT_EVENT')
    transID  = widget_button(base,value='Transmission', font=font_large, $
                             event_pro = 'ADAS414_PROC_PLOT_EVENT')
    filterID = widget_button(base,value='Filter', font=font_large, $
                             event_pro = 'ADAS414_PROC_PLOT_EVENT')
    

                ;**************************
                ;**** Initial settings ****
                ;**************************

   if procval.new NE -1 then begin
   
      num_el = n_elements(procval.elements)
      for j =0,num_el-1 do begin
          iel = where(procval.elements[j] EQ filter_elements) 
          widget_control, filter_elemID[j], set_combobox_select = iel[0]
          str = strtrim(string(procval.thick[j]), 1)
          widget_control, filter_thickID[j], set_value=str
      endfor
      
      idio = where(procval.diode EQ diode_elements, count) 
      if count NE 0 then begin
          widget_control, diode_elemID, set_combobox_select = idio[0]
          str = strtrim(string(procval.thick_diode), 1)
          widget_control, diode_thickID, set_value=str
      endif
      
   endif


                ;********************************
                ;**** Realize the new widget ****
                ;********************************

    dynlabel, parent
    widget_control, parent, /realize

                
                
; Get the id of the graphics area and setup global plot attributes.
    
    widget_control, winID, get_value=grval
    winNum = grval.win

    wset, winNum
    
    title    = strarr(3)
    title(0) = 'Soft X-Ray Filter Production'
    title(1) = 'ADAS     :' + strupcase(header)
    title(2) = 'FILES    : ' + henkeroot

    xtitles  = strarr(2)
    xunits   = strarr(2)
    xtitles  = ['PHOTON ENERGY','WAVELENGTH']
    xunits   = [' (eV) ','  (A)  ']
    xu_ind   = 0
 
    ytitle   = 'DETECTOR EFFICIENCY'      ; default on entry to adas414_proc
    yunits   = ['        ']
    
    xlog     = 1
    ylog     = 0
    
    current_plot = 'D'
    
    
; Plot an initial graph in log/lin space if there is data. Otherwise setup
; some placeholder data.

    if procval.new NE -1 then begin
    
       get_henke_data, henkeroot, procval.elements , procval.thick,  $
                       procval.diode, procval.thick_diode,           $
                       data_type = current_plot,                     $
                       xdata  = x,  ydata = y, for_plot=1

       xmin     = min(x, max = xmax)
       xmax     = xmax * 1.1
       ymin     = min(y, max = ymax)
       ymin     = ymin * 0.9
       ymax     = ymax *1.1

       plot414, winNum,  x, y,  title,                           $
                xu_ind, xunits, xtitles, yunits, ytitle,         $
                xmin, xmax, ymin, ymax, xlog, ylog, current_plot
    
    endif else begin
    
        xmin     = -1.0
        xmax     = -1.0
        ymin     = -1.0
        ymax     = -1.0
        x        = -1
        y        = -1

    endelse
    


; Put all required data into a structure. The x and y data is put into
; a structure also but it is carried about as a reference to its location.
; This is because the size and dimensions of x and y change depending on 
; which filter elements and plot type are selected. In this way the data
; can be changed without (illegally) altering the size of members of the
; info structure. As a side benefit we don't have to carry a lot of data about.
;
; Note updateID is for interaction with events from the 'adjust' button
 
    ptrToFormData = Ptr_New({cancel:1, menu:0})
    ptrToXY       = Ptr_New({x:x, y:y})
    
    minmaxIDs = lonarr(4) 

    info = { winNum          :  winNum,            $
             updateID        :  parent,            $
             gleader         :  parent,            $
             filter_elements :  filter_elements,   $
             diode_elements  :  diode_elements,    $
             filter_elemID   :  filter_elemID,     $
             filter_thickID  :  filter_thickID,    $
             diode_elemID    :  diode_elemID,      $
             diode_thickID   :  diode_thickID,     $
             replotID        :  replotID,          $
             xsecID          :  xsecID,            $
             f2ID            :  f2ID,              $
             transID         :  transID,           $
             filterID        :  filterID,          $
             current_plot    :  current_plot,      $
             henkeroot       :  henkeroot,         $ 
             title           :  title,             $
             xu_ind          :  xu_ind,            $
             xunits          :  xunits,            $
             xtitles         :  xtitles,           $
             yunits          :  yunits,            $
             ytitle          :  ytitle,            $
             xmin            :  xmin,              $
             xmax            :  xmax,              $
             ymin            :  ymin,              $
             ymax            :  ymax,              $
             xlog            :  xlog,              $
             ylog            :  ylog,              $
             minmaxIDs       :  minmaxIDs,         $         
             devlist         :  devlist,           $
             devcode         :  devcode,           $
             font_large      :  font_large,        $
             font_small      :  font_small,        $
             ptrToXY         :  ptrToXY,           $
             ptrToFormData   :  ptrToFormData      }


; Store the info structure in the user value of the top-level base and in the
; widget which does the updating.

  widget_control, parent, Set_UValue=info
  widget_control, interactbase, Set_UValue=info

; ...and launch the widget into the world.  

  XManager, 'ADAS414_PROC', parent, Event_Handler='ADAS414_PROC_EVENT'



; When the widget is destroyed, the block is released, and we
; return here. Get the form data that was collected by the form
; and stored in the pointer location.

formdata = *ptrToFormData

rep='CONTINUE'
IF N_Elements(formdata) EQ 0 THEN BEGIN
   Ptr_Free, ptrToFormData
   rep ='CANCEL'
ENDIF
 
IF formdata.cancel EQ 1 THEN BEGIN
   Ptr_Free, ptrToFormData
   rep ='CANCEL'
   RETURN,rep
ENDIF

IF formdata.menu EQ 1 THEN BEGIN
   Ptr_Free, ptrToFormData
   rep ='MENU'
   RETURN,rep
ENDIF

if rep eq 'CONTINUE' then begin
      procval = { new          :  1,                     $
                  elements     :  formdata.elements,     $
                  thick        :  formdata.thick,        $
                  diode        :  formdata.diode,        $
                  thick_diode  :  formdata.thick_diode   }
   Ptr_Free, ptrToFormData
endif       


; And tell ADAS414 that we are finished

RETURN,rep

END
