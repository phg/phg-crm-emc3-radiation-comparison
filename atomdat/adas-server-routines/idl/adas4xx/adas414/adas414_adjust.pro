; Copyright (c) 2000 Strathclyde University .
; SCCS info: Module @(#)$Header: /home/adascvs/idl/adas4xx/adas414/adas414_adjust.pro,v 1.1 2004/07/06 10:49:29 whitefor Exp $ Date $Date: 2004/07/06 10:49:29 $  
;+
; PROJECT:
;       ADAS 
;
; NAME:
;	ADAS414_ADJUST
;
; PURPOSE: 
;       This function puts up a widget asking for two input files for
;       the adas_driver program of Ralph Dux. Pairs of ionised/ionising
;       or recombined/recombining adf04 files are reuested.
;
;
; EXPLANATION:
;       Similar to ADAS407 input screen but without the automatic 
;       selection of the second file.       
;     
;
; NOTES:
;       Based on the techniques of ADAS403. It entirely in IDL and uses
;       v5 (and above) features such as pointers to pass data.
; 
;
;
; INPUTS:
;        inval       - A structure holding the 'remembered' input data.
;                        EROOTPATH     : root file 1
;                        EFILE         : filename 1
;                        ECENTROOT     : central adas of root 1
;                        EUSERROOT     : user path of file 1
;                        CROOTPATH     :
;                        CFILE         :  file 2!
;                        CCENTROOT     :
;                        CUSERROOT     :
; OPTIONAL INPUTS:
;       None
;
; OUTPUTS:
;       The function returns a string rep
;           rep = 'CONTINUE' if everything is ok
;           rep = 'CANCEL' if there is a problem or indeed if the user
;                          closes the widget with the mouse. 
;
; OPTIONAL OUTPUTS:
;       None
;
; KEYWORD PARAMETERS:
;       FONT_LARGE - The name of a larger font e.g 'courier_bold14'
;
;       FONT_SMALL - The name of a smaller font e.g 'courier_bold12'
;
;
; CALLS:
;                            
;       ADAS414_IN_FILEA     : Reacts to first file widget and sensitises
;                              'Done' and 'Browse' if 2 valid files have
;                              been selected.
;       ADAS414_IN_FILEB     : Ditto for second file.
;       ADAS414_IN_EVENT     : Reacts to Cancel, Done and Browse.
;
; SIDE EFFECTS:
;
; CATEGORY:
;	Widgets
;
; WRITTEN:
;       Martin O'Mullane
;
; MODIFIED:
;	1.1	Martin O'Mullane
;		First release 
;
; VERSION:
;       1.1	12-10-99
;
;-
;-----------------------------------------------------------------------------
PRO ADAS414_ADJUST_TEXT, event

; This event handler captures all text input - only accept numbers 
; ie '0-9', '.', 'd', 'e', '+' and '-'


; Only allow insertion/deletion - no text select

  
  
  Widget_Control, event.id,  Get_Value=userEvent


  accept_codes = [43, 45, 46, 48, 49, 50, 51, 52, 53, 54, 55, 56, 57, 100, 101]
  
  
  ; Handle text insertion events

     IF event.type EQ 0 THEN BEGIN

        valid_entry = where(byte(event.ch) EQ accept_codes, count)
        
        Widget_Control, event.id, Get_Value=text
        text   = text(0)
        length = StrLen(text)
        
        ; only react if there are less than 8 characters there already,
        
        if length LT 8  AND count NE 0 then begin 
           selection = Widget_Info(event.id, /Text_Select)
           Widget_Control, event.id, /Use_Text_Select, Set_Value=String(event.ch)
           Widget_Control, event.id, Set_Text_Select=event.offset + 1
        endif

     ENDIF 

  ; Must also deal with deletions

     IF event.type EQ 2 THEN BEGIN
        Widget_Control, event.id, Get_Value=text
        text = text(0)
        length = StrLen(text)

help,event,/st
help,text,length
        Widget_Control, event.id, Set_Value=StrMid(text, 0, length-event.length)
        Widget_Control, event.id, Set_Text_Select=event.offset

     ENDIF


   

END 

;----------------------------------------------------------------------------

PRO ADAS414_ADJUST_EVENT, event

; Get the info structure out of the user value of the top-level base.

Widget_Control, event.top, Get_UValue=info

; All events come here. Destroy the widget if cancelled. Otherwise grab
; all data to put into an event structure which is passed straight on 
; to a named notified widget. 
;
; These values are not necessarily strings. Hence the string conversion 
; to find if the cancel button was pressed.

Widget_Control, event.id, Get_Value=userEvent

userEventStr = string(userEvent)

CASE userEventStr OF

  'Cancel' : Begin
               widget_Control, event.top, /destroy
             End 

   else    : Begin
               if widget_info(info.notifyID, /valid_id) then begin
                 
                  Widget_Control,info.axesID, get_value = axes_type
                  if info.xunitsID NE -1L then  begin
                      Widget_Control,info.xunitsID, get_value = xunits_type
                   endif else begin
                      xunits_type = -1
                   endelse
                  if info.yunitsID NE -1L then  begin
                      Widget_Control,info.xunitsID, get_value = yunits_type
                   endif else begin
                      yunits_type = -1
                   endelse
            
                  adj_par = {  axes_type    : axes_type,    $
                               xunits_type  : xunits_type,  $
                               yunits_type  : yunits_type   }
                  
                  updateEvent = {ID      : event.id,       $
                                 TOP     : 0L,             $
                                 HANDLER : 0L,             $
                                 ACTION  : 'update',       $
                                 DATA    : adj_par         }
                  widget_control, info.notifyID, send_event=updateEvent
               
               endif
             End
                   
ENDCASE

END
  
;-----------------------------------------------------------------------------


PRO adas414_adjust, GROUP_LEADER = group_leader, NOTIFYID = notifyID,      $
                    FONT_LARGE = font_large, FONT_SMALL = font_small,      $
                    xunits = xunits, yunits = yunits,                      $
                    gsel = gsel,                                           $
                    minmaxIDs = minmaxIDs,                                 $
                    xmin = xmin, xmax = xmax, ymin = ymin, ymax = ymax 


; We only want one instance of this widget.  
  
  IF XREGISTERED('adas414_adjust') THEN RETURN
		
                
; Set defaults for keywords.

  IF n_elements(font_large) eq 0 THEN font_large = ''
  IF n_elements(font_small) eq 0 THEN font_small = ''

  IF n_elements(notifyID) eq 0 THEN notifyID = -1L
  IF n_elements(gsel) eq 0 THEN gsel = 0

 
                ;********************************************
		;**** create modal top level base widget ****
                ;********************************************
                
  parent = Widget_Base(Column=1, Title='Graph Control')

  topbase = widget_base(parent,/column,/frame)
  label   = widget_label(topbase,value=' ')


                ;********************
		;**** Axis style ****
                ;********************

  style = ['X Linear/Y Linear', $
           'X Log/Y Linear   ', $
           'X Linear/Y Log   ', $
           'X Log/Y Log      ']        

  

  stylbase = widget_base(topbase,/row)                            
  label    = widget_label(stylbase,value='   Axes: ',font=font_large)
  axesID   = cw_bselector(stylbase,style,set_value=gsel,font=font_large)
                
                
                ;********************
		;**** Axis units ****
                ;********************
                
  
  IF n_elements(xunits) NE 0 then begin
  
     xunitbase = widget_base(topbase)
     base      = widget_base(xunitbase,/row)
     
     label     = widget_label(base,value='X-units: ',font=font_large)
     listval   = 0
     xunitsID  = cw_bselector(base,xunits,set_value=listval,font=font_large)
     
  endif else begin
  
    xunitsID   = -1L 
  
  endelse     
                
  IF n_elements(yunits) NE 0 then begin
  
     yunitbase = widget_base(topbase)
     base      = widget_base(yunitbase,/row)
     
     label     = widget_label(base,value='Y-units: ',font=font_large)
     listval   = 0
     yunitsID  = cw_bselector(base,yunits,set_value=listval,font=font_large)
     
  endif else begin
  
    yunitsID   = -1L 
  
  endelse     
  
                ;*****************
		;**** min/max ****
                ;*****************
                
    base   = widget_base(topbase,/col,/frame,/align_center)
    tbase  = widget_base(base,/row)
    label  = widget_label(tbase,value='X :',font=font_small)
    xminID = widget_text(tbase, value='', font=font_small, $
                         xsize=8,event_pro='ADAS414_ADJUST_TEXT',   $
                         all_events=1, editable=0)
    xmaxID = widget_text(tbase, value='', font=font_small, $
                         xsize=8,event_pro='ADAS414_ADJUST_TEXT',   $
                         all_events=1, editable=0)
    
    tbase  = widget_base(base,/row)
    label  = widget_label(tbase,value='Y :',font=font_small)
    yminID = widget_text(tbase, value='', font=font_small, $
                         xsize=8,event_pro='ADAS414_ADJUST_TEXT',   $
                         all_events=1, editable=0)
    ymaxID = widget_text(tbase, value='', font=font_small, $
                         xsize=8,event_pro='ADAS414_ADJUST_TEXT',   $
                         all_events=1, editable=0)
                
    updateID  = widget_button(base,value='Re-Plot',font=font_small)
                
                
    minmaxIDs = [xminID, xmaxID, yminID, ymaxID]
    
                ;*****************
		;**** Buttons ****
                ;*****************
                
  cancelID   = widget_button(parent,value='Cancel',font=font_large)




; Initial settings 


; Realize the ADAS414 input widget.

   widget_Control, parent, /realize


; Create an info structure with program information.

  info = { cancelID        :  cancelID,           $
           notifyID        :  notifyID,           $
           axesID          :  axesID,             $
           xunitsID        :  xunitsID,           $
           yunitsID        :  yunitsID,           $
           font_large      :  font_large          }
            
               
; Store the info structure in the user value of the top-level base and launch
; the widget into the world.

  widget_control, parent, Set_UValue=info

  XManager, 'adas414_adjust', parent, Event_Handler='ADAS414_ADJUST_EVENT', $
                                  Group_Leader = group_leader



END
