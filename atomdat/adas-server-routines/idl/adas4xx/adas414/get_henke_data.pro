; Copyright (c) 2000 Strathclyde University .
; SCCS info: Module @(#)$Header: /home/adascvs/idl/adas4xx/adas414/get_henke_data.pro,v 1.1 2004/07/06 13:58:29 whitefor Exp $ Date $Date: 2004/07/06 13:58:29 $  
PRO get_henke_data, root, elements , thick, diode, thick_diode,     $
                    data_type = data_type, $
                    xdata  = xdata,  ydata = ydata, edges = edges,  $
                    for_plot = for_plot


  IF n_elements(for_plot) EQ 0 THEN for_plot = 0
  
; Get the data  
 
  IF n_elements(data_type) EQ 0 THEN data_type = 'C'
  data_type = strupcase(data_type)
  
  nelem  = n_elements(elements)
  nx_max =  600
  
  CASE data_type OF
  
    'C' : Begin
   
             ny = nelem
             nx = intarr(nelem)
             x  = dblarr(nx_max, ny)
             y  = dblarr(nx_max, ny)
             for j=0, nelem-1 do begin
                edges = 1
                read_henke, root, elements(j), xx, yy, cross=1, edges=edges
                nx(j) = n_elements(xx)
                x(0:nx(j)-1, j) = xx
                y(0:nx(j)-1, j) = yy
             endfor
             
          End
 
    'F' : Begin
   
             ny = nelem
             nx =  intarr(nelem)
             x  = dblarr(nx_max, ny)
             y  = dblarr(nx_max, ny)
             for j=0, nelem-1 do begin
                edges = 1
                read_henke, root, elements(j), xx, yy, f2=1, edges=edges
                nx(j) = n_elements(xx)
                x(0:nx(j)-1, j) = xx
                y(0:nx(j)-1, j) = yy
             endfor
             
          End
           
    'T' : Begin
   
             ny = nelem+1
             nx = intarr(ny)
             x  = dblarr(nx_max, ny)
             y  = dblarr(nx_max, ny)
             for j=0, nelem-1 do begin
                edges = 1
                read_henke, root, elements(j), xx, yy, kappa=1, edges=edges
                nx(j) = n_elements(xx)
                x(0:nx(j)-1, j) = xx
                y(0:nx(j)-1, j) = yy*thick(j)/1.d6
                if j eq 0 then begin
                   x(0:nx(0)-1, ny-1) =  xx
                   y(0:nx(0)-1, ny-1) = yy*thick(j)/1.d6
                   nx(ny-1) =  nx(0)
                endif
                if (j gt 0) then begin
                   ff = [xx, x(0:nx(ny-1)-1, ny-1)]
                   xnew = ff(uniq(ff, sort(ff)))
                   ff1 = interpol(y(0:nx(ny-1)-1, ny-1), x(0:nx(ny-1)-1, ny-1), xnew)
                   ff2 = interpol(yy*thick(j)/1.d6, xx, xnew)
                   nx(ny-1) = n_elements(xnew)
                   x(0:nx(ny-1)-1, ny-1) =  xnew
                   y(0:nx(ny-1)-1, ny-1) =  ff1 + ff2
                endif
             endfor
          
          End
          
    'D' : Begin
   
            ny = 1
            nx = intarr(ny)
            x  = dblarr(nx_max, ny)
            y  = dblarr(nx_max, ny)
            for j=0, nelem-1 do begin
               edges = 1
               read_henke, root, elements(j), xx, yy, kappa=1, edges=edges

               if j eq 0 then all_edges = edges
               if j gt 0 then all_edges = [all_edges, edges]

               if j eq 0 then begin
                  nx(0) = n_elements(xx)
                  x(0:nx(0)-1, ny-1) =  xx
                  y(0:nx(0)-1, ny-1) = yy*thick(j)/1.d6
               endif
               if (j gt 0) then begin
                  ff = [xx, x(0:nx(0)-1, 0)]
                  xnew = ff(uniq(ff, sort(ff)))
                  ff1 = interpol(y(0:nx(0)-1, 0), x(0:nx(0)-1, 0), xnew)
                  ff2 = interpol(yy*thick(j)/1.d6, xx, xnew)
                  nx(0) = n_elements(xnew)
                  x(0:nx(0)-1, 0) =  xnew
                  y(0:nx(0)-1, 0) =  ff1 + ff2
               endif
            endfor

        ; Absorption in Diode (sensitive area) IF thick_diode is GT 0
            if  thick_diode GT 0.0 then begin
               read_henke, root, diode, xx, yy, kappa=1
               all_edges = [all_edges, edges]

               ff = [xx, x(0:nx(0)-1, 0)]
               xnew = ff(uniq(ff, sort(ff)))
               ff1 = interpol(y(0:nx(0)-1, 0), x(0:nx(0)-1, 0), xnew)
               ff2 = interpol(yy*thick_diode/1.d6, xx, xnew)
               nx(0) = n_elements(xnew)
               x(0:nx(0)-1, 0) =  xnew
               y(0:nx(0)-1, 0) =  exp(-ff1) * (1.d0-exp(-ff2))
           endif else begin
               y(0:nx(0)-1, 0) =  exp(-y(0:nx(0)-1, 0))
           endelse

          End

    else : Begin
             x = 0.0
             y = 0.0
           End

ENDCASE


; Tidy up the data for output

z0 = 0
z1 = 99
if  z0 gt 0  or  z1 lt ny-1  then begin
   z0    = (z0  > 0) < (ny-1)
   z1    = (z1  > 0) < (ny-1)
   ny_0  = min([z0,z1])
   ny_1  = max([z0,z1])
   ny    = ny_1 - ny_0 + 1
   nx    =  temporary(nx(ny_0:ny_1)) 
   x     = temporary(x(*,ny_0:ny_1))
   y     = temporary(y(*,ny_0:ny_1))
endif

IF data_type EQ 'D' then begin
   all_edges = all_edges(uniq(all_edges,sort(all_edges)))
   edges = all_edges
ENDIF


; Return 'non-zero' data for plotting purposes. However we give all
; for writing the adf filter files. 

if for_plot EQ 0 then begin

   vals = where(x NE 0.0)
   xdata = x[vals,*]
   ydata = y[vals,*]

endif else begin

   CASE data_type OF
   
    'D'  : Begin
             vals = where(y GT 1.0e-15)
             xdata = x(vals)
             ydata = y(vals)
           End
           
    'C'  : Begin
             vals = where(y[*,0] GT 1.0e-40)
             xdata = x[vals,*]
             ydata = y[vals,*]
           End
           
    'F'  : Begin
             vals = where(y[*,0] GT 1.0e-5)
             xdata = x[vals,*]
             ydata = y[vals,*]
           End
           
    'T'  : Begin
             vals = where(y[*,0] GT 1.0e-5)
             xdata = x[vals,*]
             ydata = y[vals,*]
           End
           
    else : Begin
             xdata = x
             ydata = y
           End
    
    ENDCASE

endelse

END
