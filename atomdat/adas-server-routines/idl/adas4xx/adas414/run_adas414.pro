;----------------------------------------------------------------------
;+
; PROJECT    :  ADAS
;
; NAME       :  run_adas414
;
; PURPOSE    :  Generate adf35 filter files from Henke X-ray
;               coefficient data.
;
; ARGUMENTS  :  All output arguments will be defined appropriately.
;
;               NAME        I/O   TYPE    DETAILS
; INPUTS     :  adf25        I    str     driver file.
;               henke_dir    I    str     Location of X-ray constants - defaults
;                                         to central collection if not given.
;               foil_elem    I    str()   Elements symbols (Be etc) making up
;                                         the window.
;               foil_thick   I    flt()   Foil thicknesses in microns.
;               diode_elem   I    str     Element name of diode - typically
;                                         Si or Ga.
;               diode_thick  I    flt     Diode depletion thickness in micron.
;               adf35        I    str     Name of output filter file.
;
;
; KEYWORDS   :  help       I      -     Display header as help.
;
; OUTPUTS    :  all information is returned in ADAS datasets.
;
;
; NOTES      :
;
; AUTHOR     :  Martin O'Mullane
;
; DATE       :  06-03-2008
;
;
;
; MODIFIED:
;       1.1     Martin O'Mullane
;                 - First version.
;
; VERSION:
;       1.1     06-03-2008
;-
;----------------------------------------------------------------------


PRO run_adas414, henke_dir   = henke_dir,    $
                 foil_elem   = foil_elem,    $
                 foil_thick  = foil_thick,   $
                 diode_elem  = diode_elem,   $
                 diode_thick = diode_thick,  $
                 adf35       = adf35,        $
                 help        = help


; If asked for help

if keyword_set(help) then begin
   doc_library, 'run_adas414'
   return
endif

; Check that we get all inputs and that they are correct. Otherwise print
; a message and return to command line.

on_error, 2

; Location of Henke data

if n_elements(henke_dir) EQ 0 then  begin
   adas_dir  = getenv("ADASCENT")
   henke_dir = adas_dir + '/adf35/henke/data/'
endif


; Check the output file name

if n_elements(adf35) EQ 0 then message,'An adf35 output file is required'


; Filter composition

if n_elements(foil_elem) NE n_elements(foil_thick) then $
   message, 'Mis-match between foil elements and thicknesses'

if n_elements(diode_elem) NE n_elements(diode_thick) then $
   message, 'Misssing diode element or thicknesses'

if n_elements(diode_elem) GT 1 then $
   message, 'Only one diode element is permitted - Si or Ga'

if n_elements(diode_elem) EQ 0 then begin
   diode_elem  = '--'
   diode_thick = 0.0
endif

; Process the information

filter_type = 'D'
get_henke_data, henke_dir, foil_elem , foil_thick,       $
                diode_elem, diode_thick,                 $
                data_type = 'D',                         $
                xdata  = x,  ydata = y, edges = edges


nx        = n_elements(x)
nedges    = n_elements(edges)
nelem     = n_elements(foil_elem)
iopt_low  = 0
iopt_high = 1

ep   =  x[*,0]
sens =  alog10(y[*,0]) > (-74.D0)

fulldata = { filename : 'NULL', $
             edge     : edges,  $
             energy   : x,      $
             trans    : sens    }


; Make comments

date = xxdate()
name = xxuser()

sepstr = 'C' + string(replicate(45B,79))
comments = [sepstr,                                                                        $
           'C',                                                                            $
           'C  Photon energy vs. Transmission of filter (T) x absorption in detector (A)', $
           'C',                                                                            $
           'C  Units:  photon energy in eV',                                               $
           'C          [T*A] in logarithm base 10',                                        $
           'C',                                                                            $
           'C  Data Source: http://xray.uu.se',                                            $
           'C',                                                                            $
           'C  Energies of absorption edges (eV)',                                         $
           string(edges, format='("C   ",8(f10.3))'),                                      $
           'C',                                                                            $
           'C  Filter :',                                                                  $
           'C  Element   Thickness (micron)']

for j = 0, nelem-1 do begin
  str = string(foil_elem[j], foil_thick[j], format='("C    ",a2,"     ",f10.5)')
  comments = [comments, str]
endfor

comments = [comments,                       $
           'C',                             $
           'C  Diode (absorber) : ',        $
           'C  Element   Thickness (micron)']

str = string(diode_elem, diode_thick, format='("C    ",a2,"     ",f10.5)')
comments = [comments, str]

comments = [comments,                   $
           'C',                         $
           'C  Code     : run_adas414', $
           'C  Producer : ' + name[0],  $
           'C  Date     : ' + date[0],  $
           'C',                         $
           sepstr]

; Write the data

write_adf35, fulldata=fulldata, outfile=adf35, comments=comments

END
