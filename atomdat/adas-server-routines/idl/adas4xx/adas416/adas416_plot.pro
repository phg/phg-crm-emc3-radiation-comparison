;+
; PROJECT:
;       ADAS
;
; NAME:
;       ADAS416_PLOT
;
; PURPOSE: Plots an annotated overview of the reference case.
;
;
; EXPLANATION:
;
;
; NOTES:
;
;
;
; INPUTS:
;        pipe       - to communicate with fortran.
;
; OPTIONAL INPUTS:
;       None
;
; OUTPUTS:
;       The function returns a string rep
;           rep = 'CONTINUE' if everything is ok
;           rep = 'CANCEL' if there is a problem
;           rep = 'MENU' to abandon the program or if the user
;                          closes the widget with the mouse.
;
; OPTIONAL OUTPUTS:
;       None
;
; KEYWORD PARAMETERS:
;       FONT_LARGE - The name of a larger font e.g 'courier_bold14'
;       FONT_SMALL - The name of a smaller font e.g 'courier_bold12'
;
;
; CALLS:
;
;       ADAS416_PLOT_DRAW       : Plots and annotates the reference case.
;       ADAS416_PLOT_CURSOR     : Prints in status are valur under cursor.
;       ADAS416_PLOT_MENU       : React to presses of menu button.
;       ADAS416_PLOT_EVENT      : Event handler for all other actions.
;
; SIDE EFFECTS:
;
; CATEGORY:
;       Widgets
;
; WRITTEN:
;       Martin O'Mullane
;
; MODIFIED:
;       1.1     Martin O'Mullane
;                - First release
;       1.2     Martin O'Mullane
;                - Remove /print from adas_colors call as the
;                  new version makes it redundant.
;       1.3     Martin O'Mullane
;                - Allow more flexible partition labels.
;
; VERSION:
;       1.1     21-06-2007
;       1.2     21-11-2007
;
;-
;-----------------------------------------------------------------------------

PRO adas416_plot_draw, plotval, ind,        $
                       print    = print,    $
                       info_str = info_str

; Screen or file?

if (keyword_set(print)) then begin

  grtype = print.devcode( where(print.dev eq print.devlist) )
  if print.paper eq 'A4' then begin
     xsize = 24.0
     ysize = 18.0
  endif else begin
     xsize = 23.0
     ysize = 17.0
  endelse

  set_plot, grtype

  !p.font=0
  device,/color,bits=8,filename=print.dsn,           $
         /Helvetica, font_size=12, /landscape,       $
         xsize=xsize, ysize=ysize

  adas_colors, colors=colors

endif else adas_colors, colors=colors

; Get data and plot

itmax_p  = plotval.itmax_p
idmax_p  = plotval.idmax_p
num_p    = plotval.num_p
num_c    = plotval.num_c
te       = plotval.te
dens     = plotval.dens
fabun_p  = plotval.fabun_p
fabun_c  = plotval.fabun_c
label    = plotval.label

xmin = min(te, max=xmax)
ymin = min(fabun_p, max=ymax)

ymin = 0.001
ymax = 1.2

title = 'Density : ' + string(dens[ind], format='(e8.2)') + 'cm!u-3!n'

plot_oo, [xmin, xmax], [ymin, ymax], /nodata, ystyle=1, $
         xtitle = 'Te (eV)', $
         ytitle = 'Fractional Abundance', $
         title  = title, $
         position = [0.1, 0.1, 0.8, 0.9]

; Parent set

for j = 0, num_p-1 do oplot, te, fabun_p[*, ind, j], linestyle=j mod 2

; child set

for j = 0, num_c-1 do oplot, te, fabun_c[*, ind, j], $
           linestyle=j mod 2, color=colors.red, psym=-2, symsize=0.6


; Now for the complicated labelling

ind = where(strpos(label,'//#') NE -1, count)

if count GT 0 then begin
   
   ind = ind[count-1]

   str = ''
   for j = 0, ind-1 do str = str + ' ' + label[j]
   str = strtrim(strcompress(str),2)
   parts = strsplit(str, '/p', /extract)

   parts = parts[1:*]
   ind   = where(parts NE ' ', n_parts)
   if n_parts GT 0 then parts = parts[ind]

endif

ytop  = 0.85
ystep = 0.02
xc    = 0.81
xp    = 0.84

xyouts, xc, ytop + ystep*2, 'Child', color=colors.red, /norm

for j = 0, n_parts-1, 2 do begin

   xyouts, xc, ytop - ystep*j, parts[j], color=colors.red, /norm
   xyouts, xp, ytop - ystep*j,' : ' + parts[j+1], /norm

endfor
xyouts, xp, ytop - ystep * n_parts, '   Parent', /norm



; If output is going to a file add ADAS release info and tidy up
; For screen print file identification at top.

if keyword_set(print) then begin

  xyouts, 0.01, 0.98, info_str, charsize=0.7, /normal

  user_name  = xxuser()
  print_time = xxdate()

  out_str = 'Printed by : ' + user_name + ' at ' + print_time[1] + ' on ' + $
            print_time[0]

  xyouts, 0.7, 0.98, out_str, charsize=0.7, /normal

  device,/close
  set_plot,'X'
  !p.font=-1

endif


END
;-----------------------------------------------------------------------------


FUNCTION ADAS416_PLOT_NULL_EVENTS, event

   ; The purpose of this event handler is to do nothing
   ; and ignore all events that come to it.

   RETURN, 0

END

;-----------------------------------------------------------------------------

PRO ADAS416_PLOT_MENU, event

; The 'normal' get_value from a widget_button does not work with a
; bitmap button. Hence this specialised event handler.

; Get the info structure out of the user value of the top-level base.

Widget_Control, event.top, Get_UValue=info


 formdata = {cancel : 0, menu:1}
 *info.ptrToFormData = formdata
 widget_Control, event.top, /destroy

END

;-----------------------------------------------------------------------------


PRO ADAS416_PLOT_EVENT, event

; Get the info structure out of the user value of the top-level base.

Widget_Control, event.handler, Get_UValue=info


; Clear any messages

widget_control, info.messID, set_value = ' '


; Process events

CASE event.id OF

  info.printid : begin

                    print_val = { dsn     : '',                  $
                                  defname : 'adas416_plot.ps',   $
                                  replace :  1,                  $
                                  devlist :  info.devlist,       $
                                  dev     :  info.devlist[0],    $
                                  paper   :  'A4',               $
                                  write   :  0,                  $
                                  message :  'Note: cannot append to graphics files'}

                    act = 0
                    adas_file_gr, print_val, act, font=info.font, $
                                  title='File name for graphical output'

                    print_stuff = { dsn     : print_val.dsn,   $
                                    devcode : info.devcode,    $
                                    devlist : info.devlist,    $
                                    dev     : print_val.dev,   $
                                    paper   : print_val.paper  }

                    if print_val.write EQ 1 then begin

                       widget_control, info.indID, get_value=sl_ind
                       info_str = info.adasrel + ' ' + info.adasprog
                       adas416_plot_draw, info.plotval,  sl_ind,       $
                                          print = print_stuff,  $
                                          info_str = info_str

                       widget_control, info.messID, $
                                       set_value = 'Plot written to file'

                    endif else begin

                       widget_control, info.messID,$
                                       set_value = 'No file selected'

                    endelse

                  end

  info.indID    : begin
                       widget_control, info.indID, get_value=sl_ind
                       adas416_plot_draw, info.plotval,  sl_ind
                  end

  info.cancelid : begin
                    formdata = {cancel:1}
                    *info.ptrToFormData = formdata
                    widget_Control, event.top, /destroy
                  end

  info.doneid   : begin
                    formdata = {cancel    : 0,           $
                                menu      : 0            }
                    *info.ptrToFormData = formdata
                    widget_control, event.top, /destroy
                  end

  else : print,'ADAS416_IN_EVENT : You should not see this message! '

ENDCASE

END

;-----------------------------------------------------------------------------


FUNCTION adas416_plot, pipe, bitfile, devlist, devcode, adasrel, adasprog, $
                       FONT_LARGE = font_large, $
                       FONT_SMALL = font_small

; Set defaults for keywords

IF n_elements(font_large) eq 0 THEN font_large = ''
IF n_elements(font_small) eq 0 THEN font_small = ''




; Get data from fortran for plotting

itmax_p = -1L
idmax_p = -1L
num_p   = -1L
num_c   = -1L
n_lab   = -1L
str     = ''

readf, pipe, itmax_p
readf, pipe, idmax_p
readf, pipe, num_p
readf, pipe, num_c

te      = fltarr(itmax_p)
dens    = fltarr(idmax_p)
fabun_p = fltarr(itmax_p, idmax_p, num_p)
fabun_c = fltarr(itmax_p, idmax_p, num_c)

readf, pipe, te
readf, pipe, dens
fdum = 0.0
for it =0, itmax_p-1 do begin
  for id =0, idmax_p-1 do begin
    for is =0, num_p-1 do begin
      readf, pipe, fdum
      fabun_p[it,id,is] = fdum
    endfor
    for is =0, num_c-1 do begin
      readf, pipe, fdum
      fabun_c[it,id,is] = fdum
    endfor
  endfor
endfor

readf, pipe, n_lab
label   = strarr(n_lab)
for j = 0, n_lab-1 do begin
  readf, pipe, str
  label[j] = str
endfor

plotval = { itmax_p : itmax_p,   $
            idmax_p : idmax_p,   $
            num_p   : num_p,     $
            num_c   : num_c,     $
            te      : te,        $
            dens    : dens,      $
            fabun_p : fabun_p,   $
            fabun_c : fabun_c,   $
            label   : label      }



; Create modal top level base widget

  parent = Widget_Base(Column=1, Title='ADAS416 : Overview of partitioned ionisation balance', $
                         xoffset=100, yoffset=20)
  rc     = Widget_label(parent,value='  ',font=font_large)
  base   = Widget_base(parent, /column)

; Draw area

  gap    = Widget_label(base,value=' ',font=font_small)
  mbase  = Widget_base(base, /row)
  lbase  = Widget_base(mbase, /column)
  lab    = Widget_label(parent,value=' ',font=font_large)

  device, get_screen_size=scrsz
  xsize = scrsz[0]*0.5
  ysize = scrsz[1]*0.6

  drawID = Widget_draw(lbase, ysize=ysize, xsize=xsize, uvalue=parent)

  widget_control,drawID,get_value=wid
  wset, wid


; Place for value under cursor


  sl_min  = 0
  sl_max  = plotval.idmax_p - 1
  sl_val  = sl_max / 2

  mbase   = widget_base(lbase, /row, /align_center)
  indID   = widget_slider(mbase,value=sl_val, minimum=sl_min, $
                          maximum = sl_max, scroll=1,         $
                          title = 'adf11 density index', font=font_small)

; Buttons

  messID = widget_label(base, value='  ', font=font_large)

  menufile = bitfile + '/menu.bmp'
  read_X11_bitmap, menufile, bitmap1

  mrow     = widget_base(base,/row)
  menuID   = widget_button(mrow,value=bitmap1,font=font_large, $
                           event_pro='ADAS416_PLOT_MENU')
  gap      = widget_label(mrow, value='  ', font=font_large)
  printID  = widget_button(mrow,value='Print',font=font_large)
  gap      = widget_label(mrow, value='  ', font=font_large)
  cancelID = widget_button(mrow,value='Cancel',font=font_large)
  doneID   = widget_button(mrow,value='Done',font=font_large)


; Realize the ADAS416_PLOT input widget.

   dynlabel, parent
   widget_Control, parent, /realize

; Plot the first transition - must wait until after it is realized!

   adas416_plot_draw, plotval, sl_val


; Create a pointer to store the information collected from the form.
; The initial data stored here is set to CANCEL, so nothing needs to
; be done if the user kills the widget with the mouse.

   ptrToFormData = Ptr_New({cancel:1, menu:0})

; Create an info structure with program information.

  info = { messID            :    messID,            $
           menuID            :    menuID,            $
           printID           :    printID,           $
           cancelID          :    cancelID,          $
           doneID            :    doneID,            $
           indID             :    indID,             $
           font              :    font_large,        $
           devlist           :    devlist,           $
           devcode           :    devcode,           $
           adasrel           :    adasrel,           $
           adasprog          :    adasprog,          $
           plotval           :    plotval,           $
           ptrToFormData     :    ptrToFormData      }


; Store the info structure in the user value of the top-level base and launch
; the widget into the world.

  widget_control, parent, Set_UValue=info

  XManager, 'ADAS416_PLOT', parent, Event_Handler='ADAS416_PLOT_EVENT'


; When the widget is destroyed, the block is released, and we
; return here. Get the form data that was collected by the form
; and stored in the pointer location.

  formdata = *ptrToFormData

  if n_elements(formdata) EQ 0 then begin
     rep ='CANCEL'
     ptr_free, ptrtoformdata
     return, rep
  endif

  if formdata.cancel EQ 1 then begin
     Ptr_Free, ptrToFormData
     rep ='CANCEL'
     return, rep
  endif

  if formdata.menu EQ 1 then begin
     Ptr_Free, ptrToFormData
     rep ='MENU
     return, rep
  endif

  Ptr_Free, ptrToFormData
  rep='CONTINUE'
  return, rep

END
