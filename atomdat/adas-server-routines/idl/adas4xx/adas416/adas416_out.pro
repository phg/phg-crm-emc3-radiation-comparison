;+
; PROJECT:
;       ADAS
;
; NAME:
;       ADAS416_OUT
;
; PURPOSE:
;
;
; EXPLANATION:
;       The user selects a log file (paper.txt) and confirms the choice of
;       adf11, adf15, adf40 etc. outputs as specified in the script input file.
;
;
; NOTES:
;
;
;
; INPUTS:
;        pipe    - bi-directional pipe to communicate with fortran
;        outval  - Structure of output files
;        bitfile - Directory of bitmaps for menu button
;
;
; OPTIONAL INPUTS:
;       None
;
; OUTPUTS:
;       The function returns a string rep
;           rep = 'CONTINUE' if everything is ok
;           rep = 'CANCEL' if there is a problem or indeed if the user
;                          closes the widget with the mouse.
;           rep = 'MENU' if user want to exit to menu at this point
;
; OPTIONAL OUTPUTS:
;       None
;
; KEYWORD PARAMETERS:
;       FONT_LARGE - The name of a larger font e.g 'courier_bold14'
;
;       FONT_SMALL - The name of a smaller font e.g 'courier_bold12'
;
;
; CALLS:
;
;       ADAS416_OUT_MENU   : React to menu button event. The standard IDL
;                            reaction to button events cannot deal with
;                            pixmapped buttons. Hence the special handler.
;       ADAS416_OUT_EVENT  : Reacts to cancel and Done. Does the file
;                            existence error checking.
;
; SIDE EFFECTS:
;
; CATEGORY:
;       Widgets
;
; WRITTEN:
;       Martin O'Mullane
;
; MODIFIED:
;       1.1     Martin O'Mullane
;                - First release.
;
; VERSION:
;       1.1     23-07-2007
;
;-
;-----------------------------------------------------------------------------



PRO ADAS416_OUT_MENU, event

; The 'normal' get_value from a widget_button does not work with a
; bitmap button. Hence this specialised event handler.

   ; Get the info structure out of the user value of the top-level base.

Widget_Control, event.top, Get_UValue=info


 formdata = {cancel : 0, menu:1}
 *info.ptrToFormData =formdata
 widget_Control, event.top, /destroy

END


;-----------------------------------------------------------------------------


PRO ADAS416_OUT_EVENT, event

; React to button events - Cancel and Done but not menu (this requires a
; specialised event handler ADAS416_OUT_MENU). Also deal with the passing
; directory output Default button here.

; On pressing Done check for the following


   ; Get the info structure out of the user value of the top-level base.

Widget_Control, event.top, Get_UValue=info


Widget_Control, event.id, Get_Value=userEvent

CASE userEvent OF


  'Cancel' : begin
               formdata = {cancel:1, menu : 0}
               *info.ptrToFormData =formdata
               widget_Control, event.top, /destroy
             end


  'Done'   : begin

                ; gather the data for return to calling program

                err  = 0
                mess = ''

                widget_Control, info.paperID, Get_Value=pap

                ; no need to set mess as it is flagged in the cw
                if pap.outbut  EQ 1 AND strtrim(pap.message)  NE '' then err=1

                if err EQ 0 then begin
                   formdata = { paper    : pap,      $
                                cancel   : 0,        $
                                menu     : 0         }
                   *info.ptrToFormData = formdata
                   widget_control, event.top, /destroy
                endif else begin
                   widget_Control, info.messID, Set_Value=mess
                endelse

             end

  else : print,'ADAS416_OUT_EVENT : You should not see this message! ',userEvent

ENDCASE

END

;-----------------------------------------------------------------------------



FUNCTION ADAS416_OUT, pipe, outval, bitfile,       $
                      FONT_LARGE = font_large,     $
                      FONT_SMALL = font_small


; Set defaults for keywords and extract info for paper.txt question

  IF n_elements(font_large) eq 0 THEN font_large = ''
  IF n_elements(font_small) eq 0 THEN font_small = ''

  paperval =  { outbut   : outval.TEXOUT, $
                appbut   : outval.TEXAPP, $
                repbut   : outval.TEXREP, $
                filename : outval.TEXDSN, $
                defname  : outval.TEXDEF, $
                message  : outval.TEXMES  }


; Get data from fortran for output files

ndclass = -1L
readf, pipe, ndclass

a11_files = strarr(ndclass)

str = ''
for j = 0, ndclass-1 do begin
   readf, pipe, str
   a11_files[j] = str
endfor






; Create top level base widget


  parent = Widget_Base(Column=1, Title='ADAS 416 OUTPUT', $
                       XOFFSET=100, YOFFSET=1)

  rc = widget_label(parent,value='  ',font=font_large)

  base    = widget_base(parent, /column)


; Output file just paper.txt

  mrow    = widget_base(base,/frame)
  paperID = cw_adas_outfile(mrow, OUTPUT='Text Output ',   $
                                 VALUE=paperval, FONT=font_large)


  mlab  = widget_label(parent,value='  ',font=font_large)
  mcol  = widget_base(parent, /column, /frame)



; Show name of output adf11 and emissivity files for confirmation

  mcol11 = widget_base(mcol, /column, /frame)
  mlab   = widget_label(mcol11, value='adf11 output files:',font=font_large, /align_left)
  mlab   = widget_label(mcol11, value=' ',font=font_large)

  i11 = 0
  for j =  0, ndclass-1 do begin
     test_str = strtrim(a11_files[j], 2)
     if test_str NE 'NULL' then begin
        str  = '    ' + test_str
        mlab = widget_label(mcol11, value=str,font=font_small, /align_left)
        i11 = i11 + 1
     endif
  endfor

  if i11 EQ 0 then mlab = widget_label(mcol11, value='   No adf11 files for output', $
                                       font=font_small, /align_left)


  mcol15 = widget_base(mcol, /column, /frame)
  mlab  = widget_label(mcol15, value='emissivity output files:',font=font_large, /align_left)
  mlab  = widget_label(mcol15, value=' ',font=font_large)

  iem = 0
  if iem EQ 0 then mlab = widget_label(mcol15, value='   No emissivity files for output', $
                                       font=font_small, /align_left)

; End of panel message and buttons

  mlab     = widget_label(mcol, value=' ',font=font_large)


  message  = 'Confirm writing of these output files'
  messID   = widget_label(parent, value=message, font=font_large)


  menufile = bitfile + '/menu.bmp'
  read_X11_bitmap, menufile, bitmap1

  mrow     = widget_base(parent,/row)
  menuID   = widget_button(mrow,value=bitmap1,font=font_large, $
                           event_pro='ADAS416_OUT_MENU')
  cancelID = widget_button(mrow,value='Cancel',font=font_large)
  doneID   = widget_button(mrow,value='Done',font=font_large)



; Realize the ADAS416_OUT input widget.

   dynlabel, parent
   widget_Control, parent, /realize



; Create a pointer to store the information collected from the form.
; The initial data stored here is set to CANCEL, so nothing needs to
; be done if the user kills the widget with the mouse.

   ptrToFormData = Ptr_New({cancel:1, menu:0})


; Create an info structure with program information.

  info = { messID          :  messID,             $
           paperID         :  paperID,            $
           font            :  font_large,         $
           parent          :  parent,             $
           ptrToFormData   :  ptrToFormData       }


; Store the info structure in the user value of the top-level base and launch
; the widget into the world.

  widget_control, parent, Set_UValue=info

  XManager, 'ADAS416_OUT', parent, Event_Handler='ADAS416_OUT_EVENT'


; When the widget is destroyed, the block is released, and we
; return here. Get the form data that was collected by the form
; and stored in the pointer location.

formdata = *ptrToFormData

rep='CONTINUE'
IF N_Elements(formdata) EQ 0 THEN BEGIN
   Ptr_Free, ptrToFormData
   rep ='CANCEL'
   RETURN, rep
ENDIF

IF formdata.cancel EQ 1 THEN BEGIN
   Ptr_Free, ptrToFormData
   rep ='CANCEL'
   RETURN, rep
ENDIF

IF formdata.menu EQ 1 THEN BEGIN
   Ptr_Free, ptrToFormData
   rep ='MENU'
   RETURN, rep
ENDIF

if rep eq 'CONTINUE' then begin

  outval.texout  = formdata.paper.outbut
  outval.texdsn  = formdata.paper.filename

endif

RETURN, rep

END
