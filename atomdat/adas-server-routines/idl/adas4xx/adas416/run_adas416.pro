;----------------------------------------------------------------------
;+
; PROJECT    :  ADAS
;
; NAME       :  run_adas416
;
; PURPOSE    :  Partitions a set of adf11 files and writes new adf11
;               datasets. Also outputs equilibrium balance for
;               partitioned and un-partitioned data.
;
; ARGUMENTS  :  All output arguments will be defined appropriately.
;
;               NAME      I/O    TYPE   DETAILS
; INPUTS     :  script     I     str    script file to drive routine.
;               log        I     str    name of output text file.
;
; OUTPUTS    :  frac_p     O     struc  fractional abundances of parent
;               frac_c     O     struc  fractional abundances of child
;                                       ie with bundled data.
;
;               The fractional abundance structure are defined similarly
;               to the output from run_adas405 and run_adas406,
;
;               frac        frac.te    - temperature array from adf11 file (eV)
;                           frac.dens  - density arry from adf11 file (cm-3)
;                           frac.stage - str array of ion/metastable descriptors.
;                           frac.ion   - fltarr(nte, nstage) of
;                                          1st dim: te index
;                                          2nd dim: density
;                                          3rd dim: stages/bundle index
;
; KEYWORDS   :  help              -     displays help entry
;               pbar              -     display a progress bar when running.
;
;
; NOTES      :  run_adas416 uses the spawn command. Therefore IDL v5.3
;               cannot be used. Any other version should work.
;
; AUTHOR     :  Martin O'Mullane
;
; DATE       :  11-10-2009
;
; MODIFIED
;       1.1     Martin O'Mullane
;                 - First version.
;       1.2     Martin O'Mullane
;                 - Rename output fractional abundance structures.
;                 - Improve comments.
;                 - Use xxscript_416 to construct partition labels.
;
; DATE
;       1.1     11-10-2009
;       1.2     06-04-2019
;
;-
;----------------------------------------------------------------------


PRO run_adas416, script   =  script,    $
                 frac_p   =  frac_p,    $
                 frac_c   =  frac_c,    $
                 log      =  log,       $
                 pbar     =  pbar,      $
                 help     =  help


; If asked for help

if keyword_set(help) then begin
   doc_library, 'run_adas416'
   return
endif



; First check that we are not using IDL v5.3

if !version.release EQ '5.3' then begin
   message, 'This version of IDL (v5.3) will not work - ' + $
            'change to any other v5.x or above'
   return
endif


; Check that we get all inputs and that they are correct. Otherwise print
; a message and return to command line.

on_error, 2

if n_elements(script) eq 0 then message, 'A script name must be passed'
file_acc, script, exist, read, write, execute, filetype
if exist ne 1 then message, 'ADAS416 script does not exist ' + script
if read ne 1 then message, 'ADAS416script cannot be read from this userid ' + script


; Run the calculation

fortdir = getenv('ADASFORT')
spawn, fortdir+'/adas416.out', unit=pipe, /noshell, PID=pid

date = xxdate()
printf, pipe, date[0]

printf, pipe, 1L  ; want child fractional abundances
printf, pipe, 1L


; Input screen

rep = 'NO'
printf, pipe, rep

printf, pipe, script


; Calculation proceeding

nloops = -1L
readf, pipe, nloops

if keyword_set(pbar) then begin

   adas_progressbar, pipe     = pipe,         $
                     n_call   = nloops,       $
                     adasprog = 'run_adas416',    $
                     message  = 'adf11 computation underway'

endif else begin

   message, 'partitioning underway', /continue
   idum = 1L
   for j = 0, nloops-1 do readf, pipe, idum

endelse


; Graphics screen - get the fractional abundances

itmax_p = -1L
idmax_p = -1L
num_p   = -1L
num_c   = -1L
n_lab   = -1L
str     = ''

readf, pipe, itmax_p
readf, pipe, idmax_p
readf, pipe, num_p
readf, pipe, num_c

te      = fltarr(itmax_p)
dens    = fltarr(idmax_p)
fabun_p = fltarr(itmax_p, idmax_p, num_p)
fabun_c = fltarr(itmax_p, idmax_p, num_c)

readf, pipe, te
readf, pipe, dens
fdum = 0.0
for it = 0, itmax_p-1 do begin
  for id = 0, idmax_p-1 do begin
    for is = 0, num_p-1 do begin
      readf, pipe, fdum
      fabun_p[it,id,is] = fdum
    endfor
    for is = 0, num_c-1 do begin
      readf, pipe, fdum
      fabun_c[it,id,is] = fdum
    endfor
  endfor
endfor

readf, pipe, n_lab
label   = strarr(n_lab)
for j = 0, n_lab-1 do begin
  readf, pipe, str
  label[j] = str
endfor


rep = 'CONTINUE'
printf, pipe, rep


; Output screen

ndclass = -1L
readf, pipe, ndclass
a11_files = strarr(ndclass)

str = ''
for j = 0, ndclass-1 do begin
   readf, pipe, str
   a11_files[j] = str
endfor


; Finish up

rep = 'YES'
printf, pipe, rep
rep = 'CANCEL'
printf, pipe, rep

idum = -1L
readf, pipe, idum

close, pipe
free_lun, pipe



; Construct the stage information from partition description

xxscript_416, file=script, fulldata=all

res_c = min(all.iptnla[0:all.nptnl-1], max=res_p)

ind_p = res_p-1
num_p = all.nptn[ind_p]
label_p = strarr(num_p)

for j = 0, num_p-1 do begin
   str = ''
   for k = 0, all.nptnc[ind_p,j]-1 do str = str + string(all.iptnca[ind_p, j, k], format='(i2.2)') + ' '
   label_p[j] = strtrim(str, 2)
endfor

ind_c = res_c-1
num_c = all.nptn[ind_c]
label_c = strarr(num_c)

for j = 0, num_c-1 do begin
   str = ''
   for k = 0, all.nptnc[ind_c,j]-1 do str = str + string(all.iptnca[ind_c, j, k], format='(i2.2)') + ' '
   label_c[j] = strtrim(str, 2)
endfor


; Put the abundances into structures

if arg_present(frac_p) then begin

   frac_p = { te    : te,      $
              dens  : dens,    $
              stage : label_p, $
              ion   : fabun_p  }
endif

if arg_present(frac_c) then begin

   frac_c = { te    : te,      $
              dens  : dens,    $
              stage : label_c, $
              ion   : fabun_c  }
endif

END
