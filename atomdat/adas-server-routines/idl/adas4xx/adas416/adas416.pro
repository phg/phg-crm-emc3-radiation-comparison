;+
; PROJECT:
;       ADAS
;
; NAME:
;       ADAS416
;
; PURPOSE:
;       To partition a set of isonuclear adf11 master files.
;
; USE:
;       First the system settings must be established by calling
;       adas_sys_set.pro then adas416.pro is called.
;
;       adas_sys_set, adasrel, fortdir, userroot, centroot, $
;                     devlist, devcode, font_large, font_small, edit_fonts
;       adas416,   adasrel, fortdir, userroot, centroot, devlist, $
;                  devcode, font_large, font_small, edit_fonts
;
; INPUTS:
;       ADASREL - A string indicating the ADAS system version,
;                 e.g ' ADAS RELEASE: ADAS93 V1.4'.  The first
;                 character should be a space.
;
;       FORTDIR - Not applicable here.
;
;       USERROOT - A string holding the path to the root directory of
;                  the user's adas data. e.g '/disk/bowen/adas'
;                  This root directory will be used by adas to construct
;                  other path names.  For example the users default data
;                  for adas205 should be in /disk/bowen/adas/adf04.  In
;                  particular the user's default interface settings will
;                  be stored in the directory USERROOT+'/defaults'.  An
;                  error will occur if the defaults directory does not
;                  exist.
;
;       CENTROOT - Like USERROOT, but this directory points to the
;                  central data area for the system.  User defaults are
;                  not stored on CENTROOT.
;
;       DEVLIST - A string array of hardcopy device names, used for
;                 graphical output. e.g ['Post-Script','HP-PCL','HP-GL']
;                 This array must mirror DEVCODE.  DEVCODE holds the
;                 actual device names used in a SET_PLOT statement.
;
;       DEVCODE - A string array of hardcopy device code names used in
;                 the SET_PLOT statement, e.g ['PS', 'PCL', 'HP']
;                 This array must mirror DEVLIST.
;
;       FONT_LARGE - The name of a larger font e.g 'courier_bold14'
;
;       FONT_SMALL - The name of a smaller font e.g 'courier_bold12'
;
;       EDIT_FONTS - A structure of two fonts used in the table editor
;                    adas_edtab.pro, {font_norm:'',font_input:''}.
;                    The two fonts are used to differentiate between
;                    editable and non-editable parts of the table. You
;                    may have to experiment to get two fonts which give
;                    the same default spacing in the edit widget.
;                    e.g {font_norm:'helvetica_bold14', $
;                         font_input:'helvetica_oblique14'}
;
; OPTIONAL INPUTS:
;       None
;
; OUTPUTS:
;       None - Note this routine should leave all inputs un-modified.
;
; OPTIONAL OUTPUTS:
;       None
;
; KEYWORD PARAMETERS:
;       None
;
; CALLS:
;
; SIDE EFFECTS:
;
; CATEGORY:
;       Adas system.
;
; WRITTEN:
;       Martin O'Mullane
;
; MODIFIED:
;       1.1     Martin O'Mullane
;                - First release.
;       1.2     Adam Foster
;                - Calculation and display of child partition
;                  fractional abundances has been turned off to
;                  stop code hanging.
;       1.3     Martin O'Mullane
;                - Fix fortran to prevent hanging - reinstate plot
;                  of child partition.
;
; VERSION:
;       1.1     23-07-2007
;       1.2     19-03-2009
;       1.3     03-07-2009
;
;-
;-----------------------------------------------------------------------------

PRO adas416,    adasrel, fortdir, userroot, centroot,                   $
                devlist, devcode, font_large, font_small, edit_fonts


; Initialisation and IDL v5 check

adasprog = ' PROGRAM: ADAS416 v1.1'
deffile  = userroot+'/defaults/adas416_defaults.dat'
bitfile  = centroot+'/bitmaps'
device   = ''

thisRelease = StrMid(!Version.Release, 0, 1)
IF thisRelease LT '5' THEN BEGIN
   message = 'Sorry, ADAS416 requires IDL 5 or higher'
   tmp = popup(message=message, buttons=['Accept'],font=font_large)
   goto, LABELEND
ENDIF


; Restore or set the defaults

files = findfile(deffile)
if files[0] eq deffile then begin

    restore, deffile
    inval.centroot = centroot+'/scripts416/'
    inval.userroot = userroot+'/scripts416/'

endif else begin

    inval = { ROOTPATH   :  userroot+'/scripts416/',    $
              FILE       :  '',                         $
              CENTROOT   :  centroot+'/scripts416/',    $
              USERROOT   :  userroot+'/scripts416/'     }

    texname = userroot + '/pass/paper.txt'

    outval  = { texout    :   0,                          $
                texapp    :   -1,                         $
                texrep    :   0,                          $
                texdsn    :   '',                         $
                texdef    :   texname,                    $
                texmes    :   ''                          }

endelse

;-------------------
; Start fortran code
;-------------------

spawn, fortdir+'/adas416.out', unit=pipe, /noshell, pid=pid

date = xxdate()
printf, pipe, date[0]

; Change 1 to 0 to force not wanting child fractional 
; abundances (useful if the fortran code hangs).

printf, pipe, 1L  ; want child fractional abundances
printf, pipe, 1L  ; want progress bar

;--------------------------------
; Ask for input script416 dataset
;--------------------------------

LABEL100:

    adas_in, inval, act, wintitle='ADAS416 INPUT',     $
                         title='Choose adad416 script file:', $
                         font=font_large
    rep = strupcase(act)

    printf, pipe, rep
    if rep eq 'CANCEL' then goto, LABELEND

    dsn = inval.rootpath + inval.file
    printf, pipe, dsn


;----------------------------
; Wait for adf11 partitioning
;----------------------------

    readf, pipe, nloops
    adas_progressbar, pipe     = pipe,         $
                      n_call   = nloops,       $
                      adasprog = 'ADAS416',    $
                      message  = 'adf11 computation underway', $
                      font     = font_large

;-----------------------
; Display reference case
;-----------------------

LABEL200:

    rep = adas416_plot(pipe, bitfile, devlist, devcode, adasrel, adasprog,$
                       FONT_LARGE=font_large, FONT_SMALL = font_small)

    printf, pipe, rep
    if rep eq 'CANCEL' then goto, LABEL100
    if rep eq 'MENU' then goto, LABELEND

;---------------------------------
; Wait for emissivity partitioning
;---------------------------------

LABEL300:

;     readf, pipe, nloops
;
;     adas_progressbar, pipe     = pipe,         $
;                       n_call   = nloops,       $
;                       adasprog = 'ADAS416',    $
;                       message  = 'adf11 computation underway', $
;                       font     = font_large
;

;---------------------
; Confirm output files
;---------------------

LABEL400:


    rep = adas416_out(pipe, outval, bitfile,    $
                      FONT_LARGE = font_large,  $
                      FONT_SMALL = font_small   )

    printf, pipe, rep
    if rep eq 'CANCEL' then goto, LABEL100
    if rep eq 'MENU' then goto, LABELEND


;----------------
; Return for more
;----------------

goto, LABEL100


;----------------------------
; Exit and save defaults
;----------------------------

LABELEND:

save, inval,  outval, filename=deffile

END
