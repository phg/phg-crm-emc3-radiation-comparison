; Copyright (c) 2002, Strathclyde University.
;----------------------------------------------------------------------
;+
; PROJECT    :  ADAS
;
; NAME       :  run_adas405
;
; PURPOSE    :  Runs ADAS405 ionisation balance code as an IDL
;               subroutine.
;
; ARGUMENTS  :  All output arguments will be defined appropriately.
;
;               NAME      I/O    TYPE   DETAILS
; INPUTS     :  uid        I     str    username of adf11 location.
;                                       'adas' for central ADAS data.
;               year       I     int    year of adf11 data.
;               defyear    I     int    default year of adf11 data
;                                       (defaults to 89 if not set).
;               filter     I     str    filter (if appropriate).
;               member     I     str    extra identifer (if appropriate).
;               elem       I     str    element
;               te         I     real() temperatures requested
;               dens       I     real() densities requested
;               tion       I     real() ion temperatures requested
;                                       (if /cx is used).
;               denh       I     real() ion densities requested
;                                       (if /cx is used).
;               mh         I     real   Hydrogen isotope mass
;                                       (if /cx is used).
;               mz         I     real   Element isotope mass
;                                       (if /cx is used).
;               log        I     str    name of output text file.
;                                       (defaults to no output).
;               files      I     struc  Optional structure with
;                                       replacement names for
;                                       individula files.
;                                          {acd  : '',  $
;                                           scd  : '',  $
;                                           ccd  : '',  $
;                                           prb  : '',  $
;                                           prc  : '',  $
;                                           qcd  : '',  $
;                                           xcd  : '',  $
;                                           plt  : ''   }
;                                       Not all file names are required - just
;                                       those for rplacement.
;		script     I     str    Name of script file to use.
;                                       (defaults to 'NULL').
;               goft       I     str    Output filename for Generalised
;                                       contribution file 
;                                       (defaults to no output)
;
; KEYWORDS   :  partial    I     int    =1 partial data
;                                       =0 standard (default)
;               cx         I     int    =1 use cx data
;                                       =0 no cx is included (default)
;               all        I     int    if specified data is 2D of
;                                       temperature and density.
;
; OUTPUTS    :  frac       O     struc  frac.stage - str array of ion/metastable
;                                                    descriptors.
;                                       frac.ion   - fltarr(nte, nstage) of
;                                                    nte - no. plasma parameters
;                                                    nstage - no. metastables
;                                                    fractional abundance.
;               power      O     struc  power.stage - str array of ion/metastable
;                                                     descriptors.
;                                       power.ion   - fltarr(nte, nstage) of
;                                                     nte - no. plasma parameters
;                                                     nstage - no. metastables
;                                                     of metastable power.
;                                       power.plt   - fltarr(nte) of line power
;                                       power.prb   - fltarr(nte) of continuum power
;                                       power.prc   - fltarr(nte) of charge exchange power
;                                       power.total - fltarr(nte) of total power
;
;                                If /all is specified the ion and power arrays  are
;                                        frac.ion[ndens, nte, nstage]
;                                        power.total[ndens, nte] etc.
;
;
; NOTES      :  run_adas405 uses the spawn command. Therefore IDL v5.3
;               cannot be used. Any other version should work.
;               This non-interactive version does not use the script file
;               to produce contribution functions.
;
; AUTHOR     :  Martin O'Mullane
;
; DATE       :  10-01-2001
;
; MODIFIED
;       1.2     Allan Whiteford
;                 - Tightened up the the closing of the pipe.
;       1.3     Martin O'Mullane
;                 - In final Te/density loop the loop variable (j)
;                   was redefined within the loop.
;       1.4     Martin O'Mullane
;                 - Add the /all and /help keywords.
;       1.5     Martin O'Mullane
;                 - Make /cx option work with /all.
;       1.6     Martin O'Mullane
;                 - Allow individual adf11 files to be specified
;                   via the files input structure.
;       1.7     Martin O'Mullane
;                 - Allow input elem to be specified in any case.
;       1.8     Allan Whiteford
;                 - Added support for script usage.
;       1.9     Allan Whiteford
;                 - Read final value back from Fortran
;                   before closing the pipe to allow
;                   buffers etc. to be flushed.
;       1.10    Martin O'Mullane
;                 - Correct error message for missing mass with /cx.
;       1.11    Martin O'Mullane
;                 - Do not assume adas_cent and adas_user have same path. 
;       1.12    Martin O'Mullane
;                 - Assume 'member' is the last extension before .dat 
;
; DATE
;       1.2     11-03-2002
;       1.3     02-04-2003
;       1.4     13-04-2005
;       1.5     06-05-2005
;       1.6     09-05-2005
;       1.7     21-02-2007
;       1.8     03-12-2007
;       1.9     17-02-2008
;       1.10    12-10-2009
;       1.11    13-10-2009
;       1.12    10-11-2011
;
;-
;----------------------------------------------------------------------

PRO run_adas405_filenames, year, defyear, elem, uid, member, filter,   $
                           is_partial, symbol,                         $
                           files_str,                                  $
                           filenames, fileavailable, defavailable



; Make up filenames from info passed to routine

filenames     = strarr(8)
fileavailable = intarr(8)
defnames      = strarr(8)
defavailable  = intarr(8)

dirnames = ['acd','scd','ccd','prb','prc','qcd','xcd','plt']



; To cope with odd pathnames replace the user in
; the environment ADASUSER with the input uid.
;
; ADASCENT may not be on same file system as ADASUSER (Asdex) so
; treat uid='adas' separately.

log_name  = getenv("LOGNAME")
adas_user = getenv("ADASUSER")
adas_cent = getenv("ADASCENT")

if uid NE 'adas' then begin

   part_1 = strmid(adas_user,0,strpos(adas_user,log_name))
   part_2 = strcompress(uid, /remove_all)
   part_3 = strmid(adas_user,strpos(adas_user,log_name)+strlen(log_name))

   filenames[*] = part_1 + part_2 + part_3 + '/adf11/'

endif else filenames[*] = adas_cent + '/adf11/'

defnames[*]  = adas_cent + '/adf11/'

defdirnames = dirnames + defyear
dirnames    = dirnames + year

if (is_partial) then begin
   dirnames    = dirnames + 'r'
   defdirnames = defdirnames + 'r'
endif


filenames = filenames + dirnames  + "/"
defnames  = defnames + defdirnames + "/"

if filter ne '' then begin

   filterstring = strcompress(filter, /remove_all)
   filteradd = ['','','','.'+filterstring,'.'+filterstring,    $
                '','','.'+filterstring]
   filenames = filenames + dirnames + "_" +                    $
   strcompress(symbol, /remove_all) + filteradd +".dat"
   defnames = defnames + defdirnames + "_" +                   $
   strcompress(symbol, /remove_all) + filteradd + ".dat"

endif else begin

   filenames = filenames + dirnames + "_" +               $
               strcompress(symbol, /remove_all) + ".dat"
   defnames  = defnames + defdirnames + "_" +             $
               strcompress(symbol, /remove_all) + ".dat"

endelse

if (strcompress(member, /remove_all) ne '') then begin
  
   memberstring = '.' + strcompress(member, /remove_all)
   
   for j = 0, 7 do begin
     
      fnam = filenames[j]
      ipos = strpos(fnam, '.dat')
      p1 = strmid(fnam, 0, ipos)
      p2 = memberstring
      p3 = strmid(fnam, ipos)
     
      filenames[j] = p1 + p2 + p3
     
   endfor

endif

; Overwrite user filenames which are missing with default ones

for j = 0, n_elements(filenames)-1 do begin

   res = findfile(filenames[j], count=count)
   fileavailable[j] = count
   if count EQ 0 then filenames[j] = defnames[j]

   res = findfile(defnames[j], count=count)
   defavailable[j] = count


endfor


; Overwrite choices with files_str values

if files_str.supp EQ 'YES' then begin

   for j = 1, n_tags(files_str)-1 do begin
      if files_str.(j) NE 'NULL' then begin
         filenames[j-1]     = files_str.(j)
         fileavailable[j-1] = 1
      endif
   endfor

endif

END
;----------------------------------------------------------------------



PRO run_adas405_calc, year, defyear, elem, uid, is_partial, symbol, mh, mz, $
                      member, filter, files_str,                            $
                      te, dens, tion, denh, graph_out,                      $
                      is_frac, is_power, output_file, indices, frac, power, $
                      scriptname, goftfile

; Now run the ADAS405 code - the input has been checked already.

    fortdir = getenv('ADASFORT')
    spawn, fortdir+'/adas405.out', unit=pipe, /noshell, PID=pid

    date = xxdate()
    printf, pipe, date[0]

    rep = 'NO'
    printf, pipe, rep

    printf, pipe, year
    printf, pipe, defyear
    outsymbol = strcompress(strupcase(elem), /remove_all)
    printf, pipe, outsymbol

;    scriptname = 'NULL'
    printf, pipe, scriptname

    printf, pipe, strcompress(filter, /remove_all)


    ; Generate filenames

    run_adas405_filenames, year, defyear, elem, uid, member, filter,   $
                           is_partial, symbol,                         $
                           files_str,                                  $
                           filenames, fileavailable, defavailable

    ; Write filenames to FORTRAN

    for i = 0, 7 do printf, pipe, filenames[i]
    for i = 0, 7 do printf, pipe, fileavailable[i]
    for i = 0, 7 do printf, pipe, defavailable[i]
    for i = 0, 7 do printf, pipe, indices[i]

    printf, pipe, is_partial

    if scriptname ne 'NULL' then begin
	idum = 0
	readf, pipe, idum
    	if idum eq 1 or idum eq 2 or idum eq 3 then return
    endif


    idum = 0
    readf, pipe, idum

; Now move onto processing screen options

    ntdmax = 0L
    input  = 0L
    fdum   = 0.0D0
    sdum   = ''

    readf, pipe, input
    ntdmax = input
    readf, pipe, input
    nline = input
    if nline gt 0 then begin
        ciion  = strarr(nline)
        cmpts  = strarr(nline)
        ctitle = strarr(nline)
        for i=0, (nline - 1) do begin
            readf, pipe, sdum
            ciion(i) = sdum
            readf, pipe, sdum
            cmpts(i) = sdum
            readf, pipe, sdum
            ctitle(i) = sdum
        endfor
    endif

    tvals = dblarr(2,ntdmax)

    lpend = 0L
    printf, pipe, lpend

    printf, pipe, 'Test'
    printf, pipe, 0L
    printf, pipe, 2L                 ; eV for temps
    printf, pipe, n_elements(te)
    printf, pipe, mZ
    printf, pipe, mH
    for i = 0, n_elements(te)-1 do begin
        printf, pipe, te[i]
        printf, pipe, dens[i]
        printf, pipe, tion[i]
        printf, pipe, denh[i]
    endfor

    istop = 0L
    printf, pipe, istop

; Nearly there - output options
;  - get ion balance and power


    if (is_frac) then begin

       lpend     = 0
       printf, pipe, lpend

       printf, pipe, graph_out
       if graph_out eq 1 then printf, pipe, 1

       if output_file NE '' then texout = 1 else texout = 0
       printf, pipe, texout

       if texout EQ 1 then printf, pipe, output_file

       if goftfile NE '' then begin
           gofout = 1
           printf, pipe, gofout
           printf, pipe, goftfile
       endif else begin
           gofout = 0
           printf, pipe, gofout
       endelse

       if texout EQ 1 then printf, pipe, 'Made by read405 ' + date[0]
       printf, pipe, 0

       input = 0L
       fdum  = 0.0D0
       sdum  = ' '

       readf, pipe, input
       itmax = input                            ;no. of electron temps.
       xvals = fltarr(itmax)
       for i=0, (itmax-1) do begin
           readf, pipe, fdum
           xvals[i] = fdum
       endfor

       readf, pipe, input
       nmsum = input
       yvals = fltarr(itmax, nmsum)
       poptit = strarr(nmsum)                   ;designation strings
       for i=0, (itmax-1) do begin
           for j=0, (nmsum-1) do begin
               readf, pipe, fdum
               yvals(i,j) = fdum
           endfor
       endfor

       for i=0, (nmsum-1) do begin
           readf, pipe, format='(a10)', sdum
           poptit(i) = sdum
       endfor

       readf, pipe, input

       frac = { stage : poptit,  $
                ion   : yvals    }

       if (is_power) then begin
          printf, pipe, 0L
       endif else begin
          printf, pipe, 1L
          goto, ENDOFME
       endelse

    endif



    if (is_power) then begin

       lpend     = 0L
       printf, pipe, lpend

       printf, pipe, graph_out
       if graph_out eq 1 then printf, pipe, 2L


       if output_file NE '' then texout = 1 else texout = 0L
       printf, pipe, texout

       if texout EQ 1 then printf, pipe, output_file

       gofout = 0L
       printf, pipe, gofout

       if texout EQ 1 then printf, pipe, 'Made by read405 ' + date[0]
       printf, pipe, 0L

       input = 0L
       fdum  = 0.0D0
       sdum  = ' '

       readf, pipe, input
       itmax = input                            ;no. of electron temps.
       xvals = fltarr(itmax)
       tvals = fltarr(itmax,4)                  ;total y-values
       for i=0, (itmax-1) do begin
           readf, pipe, fdum
           xvals(i) = fdum
       endfor

       readf, pipe, input
       nmsum = input
       yvals = fltarr(itmax, nmsum)
       poptit = strarr(nmsum)                   ;designation strings
       for i=0, (itmax-1) do begin
           for j=0, (nmsum-1) do begin
               readf, pipe, fdum
               yvals(i,j) = fdum
           endfor
       endfor

       for i=0, (nmsum-1) do begin
           readf, pipe, format='(a10)', sdum
           poptit(i) = sdum
       endfor
       readf, pipe, input

       for j=0,3 do begin
           for i=0, (itmax-1) do begin
               readf, pipe, fdum
               tvals(i,j) = fdum
           endfor
       endfor

       power = { stage : poptit,       $
                 ion   : yvals,        $
                 plt   : tvals[*,2],   $
                 prb   : tvals[*,0],   $
                 prc   : tvals[*,1],   $
                 total : tvals[*,3]    }

       printf, pipe, 1

    endif


; Terminate FORTRAN process

ENDOFME:

    
    readf, pipe, junk

    close, pipe
    free_lun, pipe

END



;----------------------------------------------------------------------------

PRO run_adas405, uid      =  uid,       $
                 year     =  year,      $
                 defyear  =  defyear,   $
                 elem     =  elem,      $
                 filter   =  filter,    $
                 member   =  member,    $
                 files    =  files,     $
                 partial  =  partial,   $
                 te       =  te,        $
                 dens     =  dens,      $
                 tion     =  tion,      $
                 denh     =  denh,      $
                 cx       =  cx,        $
                 mh       =  mh,        $
                 mZ       =  mZ,        $
                 frac     =  frac,      $
                 power    =  power,     $
                 log      =  log,       $
                 script   =  script,    $
                 goft     =  goft,    $
                 all      =  all,       $
                 help     =  help


; If asked for help

if keyword_set(help) then begin
   doc_library, 'run_adas405'
   return
endif



; First check that we are not using IDL v5.3

if !version.release EQ '5.3' then begin
   message, 'This version of IDL (v5.3) will not work - ' + $
            'change to v5.0, v5.1, v5.2 or v5.4'
   return
endif


; Check that we get all inputs and that they are correct. Otherwise print
; a message and return to command line.

on_error, 2

if n_elements(uid) EQ 0 then begin
    message,'A data source (uid) must be given'
endif

if n_elements(defyear) EQ 0 then begin
    defyear = 89
    print, '89 has been set as the default year'
endif

if n_elements(year) EQ 0 then begin
    print, 'The default year will be used'
    year = defyear
endif


defyear = string(defyear, format='(i2.2)')
year    = string(year, format='(i2.2)')

if n_elements(elem) EQ 0 then begin
    message, 'An element must be given'
endif else begin

    symbol = strcompress(strlowcase(elem), /remove_all)
    if (symbol eq '') then begin
        message, 'Enter a valid  isonuclear element symbol'
    endif else begin
        i4eiz0, symbol, atnumber
        if (atnumber eq 0) then  message, 'Enter a valid element'
    endelse

endelse

if n_elements(filter) EQ 0 then filter = ''

if n_elements(member) EQ 0 then member = ''

scriptname='NULL'
if n_elements(script) EQ 1 then scriptname = script

if n_elements(goft) EQ 0 then goft = ''

if keyword_set(partial) then is_partial = 1 else is_partial = 0

if n_elements(files) EQ 0 then begin
   files_str = { supp : 'NO'}
endif else begin

   files_str = { supp : '',  $
                 acd  : '',  $
                 scd  : '',  $
                 ccd  : '',  $
                 prb  : '',  $
                 prc  : '',  $
                 qcd  : '',  $
                 xcd  : '',  $
                 plt  : ''   }

   struct_assign, files, files_str

   for j = 0, n_tags(files_str)-1 do begin
      if files_str.(j) EQ '' then files_str.(j) = 'NULL'
   endfor
   files_str.supp = 'YES'

endelse

; Which type of calculation - indices : acd, scd, ccd, prb, prc, qcd, xcd, plt

indices = intarr(8)

if NOT arg_present(frac) EQ 0 then begin
   message, 'An ionisation balance is required'
endif else begin
   if (is_partial) then indices = [1,1,0,0,0,1,1,0] $
                   else indices = [1,1,0,0,0,0,0,0]
endelse

if keyword_set(cx) then begin
   indices = indices + [0,0,1,0,0,0,0,0]
   if n_elements(mh) EQ 0 then message, 'Hydrogen mass required'
   if n_elements(mZ) EQ 0 then message, 'Element mass required'
endif else begin
   mh = 1.0
   mZ = 1.0
endelse

if arg_present(power) then begin
   indices = indices + [0,0,0,1,0,0,0,1]
   if keyword_set(cx) then indices = indices + [0,0,0,0,1,0,0,0]
endif

if n_elements(log) EQ 0 then output_file = '' else output_file = log


; Temperature and density

if n_elements(te) eq 0 then message, 'User requested temperatures are missing'
if n_elements(dens) eq 0 then message, 'User requested densities are missing'

partype=size(te, /type)
if (partype lt 2) or (partype gt 5) then begin
   message,'Temperature must be numeric'
endif  else te = DOUBLE(te)

partype=size(dens, /type)
if (partype lt 2) or (partype gt 5) then begin
   message,'density must be numeric'
endif else dens = DOUBLE(dens)

len_te   = n_elements(te)
len_dens = n_elements(dens)

; are te and dens the same length and one dimensional?
; if so define data array

if keyword_set(all) then begin

   ; make a big 1D list

   t_in = dblarr(len_te*len_dens)
   d_in = dblarr(len_te*len_dens)
   if keyword_set(cx) then begin
      ti_in = dblarr(len_te*len_dens)
      dh_in = dblarr(len_te*len_dens)
   endif

   k = 0
   for i = 0, len_te-1 do begin
      for j = 0, len_dens-1 do begin
         t_in[k] = te[i]
         d_in[k] = dens[j]
         if keyword_set(cx) then begin
            ti_in[k] = tion[i]
            dh_in[k] = denh[j]
         endif
         k = k + 1
       endfor
    endfor
    itval = k


endif else begin

   ; are te and dens the same length and one dimensional?
   ; if so define data array

   if len_dens ne len_te then print, 'TE and DENS size mismatch - smaller  used'
   itval = min([len_te,len_dens])

   t_in = te
   d_in = dens
   if keyword_set(cx) then begin
      ti_in = tion
      dh_in = denh
   endif

endelse


if NOT keyword_set(cx) then begin
   ti_in = dblarr(itval)
   dh_in = dblarr(itval)
endif


; Output options - if none exit

if arg_present(frac) OR arg_present(power) then graph_out = 1 $
   else message, 'Choose at least one output!!'

if arg_present(frac) then is_frac = 1 else is_frac = 0
if arg_present(power) then is_power = 1 else is_power = 0




; Run the calculation in batchs of 30, max permitted by ADAS405


MAXVAL = 30

plt   = 0.0
prb   = 0.0
prc   = 0.0
ptot  = 0.0

n_call = numlines(itval, MAXVAL)
n_val  = 0

if n_call ne 1 and scriptname ne 'NULL' then begin
	message,'Setting script to ''NULL'' because more than '+strtrim(string(maxval),2) + ' temperature/density pairs'
	scriptname='NULL'
endif

for jl = 0, n_call - 1 do begin

  ist = jl*MAXVAL
  ifn = min([(jl+1)*MAXVAL,itval])-1

  te_ca    = t_in[ist:ifn]
  dens_ca  = d_in[ist:ifn]
  tion_ca  = ti_in[ist:ifn]
  denh_ca  = dh_in[ist:ifn]
  itval_ca = ifn-ist+1

  run_adas405_calc, year, defyear, elem, uid, is_partial, symbol, mh, mz, $
                    member, filter, files_str,                            $
                    te_ca, dens_ca, tion_ca, denh_ca,                     $
                    graph_out, is_frac, is_power, output_file, indices,   $
                    frac_ca, power_ca, scriptname, goft


  if (is_frac) then begin

     n_stage = n_elements(frac_ca.stage)

     if n_elements(fr_ca) EQ 0 then begin
        fr_ca = frac_ca.ion
     endif else begin

        n_temp = size(fr_ca)
        n_temp = n_temp[1]
        yvals  = fltarr(n_temp+itval_ca,n_stage)

        for j = 0, n_stage-1 do begin
          for k = 0, n_temp+itval_ca-1 do begin
             if k LE n_temp-1 then yvals[k,j] = fr_ca[k,j] $
                              else yvals[k,j] = frac_ca.ion[k-n_temp,j]
          endfor
        endfor

        fr_ca = yvals

     endelse

  endif

  if (is_power) then begin

     plt   = [plt, power_ca.plt]
     prb   = [prb, power_ca.prb]
     prc   = [prc, power_ca.prc]
     ptot  = [ptot, power_ca.total]

     n_stage = n_elements(power_ca.stage)

     if n_elements(pw_ca) EQ 0 then begin
        pw_ca = power_ca.ion
     endif else begin

        n_temp = size(pw_ca)
        n_temp = n_temp[1]
        yvals  = fltarr(n_temp+itval_ca,n_stage)

        for j = 0, n_stage-1 do begin
          for k = 0, n_temp+itval_ca-1 do begin
             if k LE n_temp-1 then yvals[k,j] = pw_ca[k,j] $
                              else yvals[k,j] = power_ca.ion[k-n_temp,j]
          endfor
        endfor

        pw_ca = yvals

     endelse

  endif

endfor


if keyword_set(all) then begin

   if (is_frac) then frac = { stage : frac_ca.stage,  $
                              ion   : reform(fr_ca, len_dens, len_te, n_stage)}

   if (is_power) then power = { stage : power_ca.stage,                            $
                                ion   : reform(pw_ca, len_dens, len_te, n_stage),  $
                                plt   : reform(plt[1:*],len_dens, len_te),         $
                                prb   : reform(prb[1:*],len_dens, len_te),         $
                                prc   : reform(prc[1:*],len_dens, len_te),         $
                                total : reform(ptot[1:*],len_dens, len_te)         }

endif else begin

   if (is_frac) then frac  = { stage : frac_ca.stage,  $
                               ion   : fr_ca           }


   if (is_power) then power = { stage : power_ca.stage,  $
                                ion   : pw_ca,           $
                                plt   : plt[1:*],        $
                                prb   : prb[1:*],        $
                                prc   : prc[1:*],        $
                                total : ptot[1:*]        }

endelse


END
