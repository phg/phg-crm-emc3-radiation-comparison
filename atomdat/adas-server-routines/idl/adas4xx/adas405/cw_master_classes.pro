; Copyright (c) 1995 Strathclyde University .
; SCCS info: Module @(#)$Header: /home/adascvs/idl/adas4xx/adas405/cw_master_classes.pro,v 1.16 2004/07/06 13:07:56 whitefor Exp $	Date $Date: 2004/07/06 13:07:56 $
;+
; PROJECT:
;       ADAS IBM MVS to DEC UNIX conversion
;
; NAME: CW_MASTER_CLASSES()
;
; PURPOSE:
;       Sets up details of master files to be used in adas 405.
;
; EXPLANATION:
;       This function creates a compound widget consisting of seven
;       selection widgets: typeins for the year of data, default year
;	of data, element symbol, radiated power filter and member prefix 
;       and menu type selectors for the data classes, type of master 
;	files, type of partial file and directory branch to use (User
;	or Central). There is also a button to allow the user to see
;	what files there are which actually match their choices.
;
; USE:
;       See routine cw_adas405_in.pro for an example.
;
; INPUTS:
;       PARENT  -       Long integer; the ID of the parent widget.
;
;	CLASSES -	String array; the master classes to be used.
;
;	INDICES -	Int array; the indices of selected classes.
;
;	USERDIR -	String: The rootpath for the user directories.
;
;	CENTDIR -	String: The rootpath for the central directories.
;
; OPTIONAL INPUTS:
;       None.
;
; OUTPUTS:
;       The return value of this function is the ID of the compound
;       widget which is of type LONG integer.
;
; OPTIONAL OUTPUTS:
;       None
;
; KEYWORD PARAMETERS:
;       FONT    - A font to use for all text in this widget.
;
;       TITLE   - The title to be included in this widget.
;
;	RNUM    - The number of the routine (as a string) that is calling
;		  this: used in information window titles
;
; CALLS:
;       CW_BSELECTOR - Pull-down menu type compound widget.
;	CW_CHOOSE    - Multi-choice type selection widget.
;	CW_DISPLAY_LIST - Displays a list of text items in a pop-up.
;	I4EIZ0	     - Converts element symbol into nuclear charge.
;	POPUP	     - Simple message pop-up window.
;
; SIDE EFFECTS:
;       Three other routines are included which are used to manage the
;       widget:
;
;       MASTER_SET_VAL()
;       MASTER_GET_VAL()
;       MASTER_EVENT()
;
; CATEGORY:
;       Compound Widget
;
; WRITTEN:
;       Tim Hammond, Tessella Support Services plc, 25th September 1995
;
; MODIFIED:
;       1.1     Tim Hammond 
;		First release. Routine written for adas 405, but possibly
;		can be used for 406 later.
;	1.2	Tim Hammond
;		Added searchflag to returned get_value structure. This is
;		a flag set to 1 if the last user action was a search,
;		and a 0 otherwise. This is used by subsequent programs
;		to decide whether or not to inform the user over the
;		the availability of files.
;	1.3	Tim Hammond
;		Added treatment for member prefixes and corrected logic
;		used to determine whether a file is available or not.
;	1.4	Tim Hammond
;		Added change in 'sensitivity' of partial filetype label.
;	1.5	Tim Hammond
;		Added information widget for when program is searching.
;	1.6	Tim Hammond
;		Tidied up comments and code
;	1.7	Tim Hammond
;		Added extra error checking in set_val and get_val
;		procedures to cope with Sun implementation of IDL's
;		incapacity for setting null values for text widgets.
;	1.8	Tim Hammond
;		Corrected error checking for situation when user's own
;		adf11 directory does not exist or there are no directories
;		in it.
;	1.9	Tim Hammond
;		Added different behaviours for different directory
;		branches - the program previously ignored whether the
;		user had selected User or Central data and continued
;		as though User had been set. Also ensured that the
;		variable fileavailable is not accidentally set to an
;		integer when it should always be an array.
;	1.10 	William Osborn
;		Added RNUM keyword so that 406 as well as 405 can use this
;		routine
;	1.11 	William Osborn
;		Added /noshell keyword to spawn commands so that strange
;		aliases are ignored. This also meant that the 'ls ...' 
;		commands had to be rewritten as arrays.
;	1.12	William Osborn
;		Changed symbol name to lower case automatically.
;       1.13    William Osborn / Tim Hammond
;               Ensured that all widgets are assigned a font.
;	1.14	Richard Martin
;		Ammended filterstring to accomodate name with 7 characters.
;	1.15	Martin O'Mullane & Richard Martin
;		Removed the absurd method of handling filtered data. The search
;               is now limited to the power data (prb, plt and prc) and the 
;               format is now, for example, /plt89/plt89_ni.ft25003.dat and not
;               /plt89(ft25003)/plt89_ni.dat as it was.
;		    Added check for incompatible script/master file.
;	1.16  Martin O'Mullane & Richard Martin
;	      Initialise filtr and memval with '' instead of ' '.
;
; VERSION:
;	1.1	25-09-95
;	1.2	02-10-95
;	1.3	03-10-95
;	1.4	04-10-95
;	1.5	05-10-95
;	1.6	07-11-95
;	1.7	09-11-95
;	1.8	10-11-95
;	1.9	13-11-95
;	1.10	27-06-96
;	1.11	18-07-96
;	1.12	19-09-96
;	1.13	10-10-96
;	1.14	25-03-97
;	1.15	13-05-98
;	1.16  20-11-98
;
;-
;-----------------------------------------------------------------------------
     
PRO master_set_val, id, value

                ;**** Return to caller on error ****

    ON_ERROR, 2

                ;**** Retrieve the state ****

    first_child = widget_info(id, /child)
    widget_control, first_child, get_uvalue=state, /no_copy

                ;**** Set data field values ****

		;**** set filter value ****

    if value.filtr eq '' then filval='' else filval=value.filtr
    widget_control, state.filtr, set_value=filval

		;**** Set member prefix value ****
 
    if value.member eq '' then memval='' else memval=value.member
    widget_control, state.member, set_value=memval

		;**** Set directory branch ****

    widget_control, state.branch, set_value=value.branch

		;**** Set year and default year values ****

    if value.year eq '' then yval=' ' else yval=value.year
    if value.dyear eq '' then dyval=' ' else dyval=value.dyear
    widget_control, state.year, set_value=yval
    widget_control, state.dyear, set_value=dyval


		;**** Set isonuclear element symbol ****

    if value.symbol eq '' then symval=' ' else symval=strlowcase(value.symbol)
    widget_control, state.symbol, set_value=symval

		;**** Set file type and type of partial file ****

    widget_control, state.filetype, set_value=value.filetype
    widget_control, state.parttype, set_value=value.parttype,		$
		    sensitive=value.filetype
    widget_control, state.typelabel, sensitive=value.filetype
        
                ;**** Reset the value of state variable ****

    widget_control, first_child, set_uvalue=state, /no_copy

END

;-----------------------------------------------------------------------
  
FUNCTION master_get_val, id

    COMMON mastercom, indices, topparent

                ;**** Return to caller on error ****

    ON_ERROR, 2

                ;*******************************
                ;**** Retrieve the state    ****
                ;**** Get first_child id    ****
                ;**** as state stored there ****
                ;*******************************

    first_child = widget_info(id, /child)
    widget_control, first_child, get_uvalue=state

    widget_control, state.filtr, get_value=filtr
    widget_control, state.member, get_value=member
    widget_control, state.year, get_value=year
    widget_control, state.dyear, get_value=dyear
    widget_control, state.symbol, get_value=symbol
    symbol = strlowcase(symbol)
    widget_control, state.branch, get_value=branch
    widget_control, state.filetype, get_value=filetype
    widget_control, state.parttype, get_value=parttype
    searchflag = state.searchflag

    datavalue = {	filtr		:	filtr,			$
			year		:	year,			$
			dyear		:	dyear,			$
			symbol		:	symbol,			$
			branch		:	branch,			$
			filetype	:	filetype,		$
			parttype	:	parttype,		$
			indices		:	indices,		$
			searchflag	:	searchflag,		$
			member		:	member			}

    widget_control, first_child, set_uvalue=state, /no_copy

    RETURN, datavalue

END

;------------------------------------------------------------------------
  
FUNCTION master_event, event

    COMMON mastercom, indices, topparent
    COMMON master_dirs, userdir, centdir

    parent=event.handler
    searchflag = 0

                ;**********************************************
                ;**** Retrieve the user value state        ****
                ;**** Get id of first_child widget because ****
                ;**** user value "state" is stored there   ****
                ;**********************************************

    first_child = widget_info(parent, /child)
    widget_control, first_child, get_uvalue=state, /no_copy
                                                        
                ;************************
                ;**** Process events ****
                ;************************

    CASE event.id OF

	state.classbut: begin
            choosetitle="Select isonuclear master classes for processing:-"
            choosehead="(ACD and SCD are required)"
            indices = state.indices
	    cw_choose, state.classes, indices,		 		$
                       font=state.font,	 				$
		       nochange=[1,1,-1,-1,-1,-1,-1,-1],		$
		       title=choosetitle, head=choosehead
	    state.indices = indices
        end

        state.filtr: widget_control, state.member, /input_focus

	state.member: widget_control, state.year, /input_focus

	state.year: widget_control, state.dyear, /input_focus

	state.dyear: widget_control, state.symbol, /input_focus

	state.symbol: begin
	    widget_control, state.symbol, get_value=symbol
	    widget_control, state.symbol, set_value=strlowcase(symbol(0))
	    widget_control, state.filtr, /input_focus
	end

	state.filetype: begin
	    widget_control, state.filetype, get_value=flag
            widget_control, state.parttype, sensitive=flag
            widget_control, state.typelabel, sensitive=flag
        end

	state.button: begin			;**** Display dataset ****

		;**** Set the searchflag to YES ****

	    searchflag = 1

		;**** Test validity of the entered values ****
	
	    errormess = ''
	    newfocus = 0L

                ;**** Check the member prefix ****

            widget_control, state.member, get_value=memberval
            mem = strcompress(memberval(0), /remove_all)
            member = strcompress(mem, /remove_all)
            if (strlen(member) ne 2 and (member ne '')) then begin
                errormess =                                         	$
                '**** You have entered an invalid member prefix ****'
                newfocus = state.member
            endif
	    
		;**** Check the year ****

	    if (errormess eq '') then begin
	        widget_control, state.year, get_value=yearvalue
	        year = strcompress(yearvalue(0), /remove_all)
	        if (year eq '') then begin
		    errormess = '**** You must enter a valid year '+	$
                    'for the data ****'
	        endif else if (strlen(year) ne 2 ) then begin
                    errormess = '**** You have specified the year '+	$
                    'incorrectly ****'
                endif else begin
                    yearbyt = byte(year)
                    if ((min(yearbyt) lt 48) or 			$
                    (max(yearbyt) gt 57)) then begin
                        errormess =                                 	$
                        '**** You have specified the year incorrectly ****'
                    endif
                endelse
	        if errormess ne '' then newfocus=state.year
	    endif
    
		;**** Check the default year ****

	    if (errormess eq '') then begin
		widget_control, state.dyear, get_value=dyearvalue
                dyear = strcompress(dyearvalue(0), /remove_all)
		if (dyear ne '') then begin
		    if (strlen(dyear) ne 2) then begin
			errormess = '**** You have specified the default'+$
                                    ' year incorrectly ****'
		    endif else begin
		        dyearbyt = byte(dyear)
                        if ((min(dyearbyt) lt 48) or 			$
			(max(dyearbyt) gt 57)) then begin
                            errormess = '**** You have specified the '+	$
                            'default year incorrectly ****'
                        endif
		    endelse
		endif
	        if errormess ne '' then newfocus=state.dyear
	    endif

                ;**** Check the symbol ****

            if (errormess eq '') then begin
		widget_control, state.symbol, get_value=symbolval
		symb = symbolval(0)
		symbol = strlowcase(strcompress(symb, /remove_all))
		if (symbol eq '') then begin
		    errormess = '**** You must enter a valid '+		$
                    'isonuclear element symbol ****'
		endif else begin
		    i4eiz0, symbol, atnumber
		    if (atnumber eq 0) then begin
			errormess = '**** You have specified an '+	$
			'invalid element ****'
		    endif
		endelse
		if (errormess ne '') then newfocus=state.symbol
	    endif
		
                ;**** if error then give message ****

            if errormess ne '' then begin
                action = popup(message=errormess, buttons=[' OK '],     $
                               font=state.font)
		widget_control, newfocus, /input_focus
            endif else begin

                ;**** No error so begin the search ****

		searchfail = 1		;flag for search through user space
					;remains 1 until a required file
					;is not found


		;**** Create an information widget ****

		widget_control, /hourglass
 		widgetbase = widget_base(/column, xoffset=300, yoffset=200,$
 		title = "ADAS"+state.rnum+": INFORMATION")
 		lab0 = widget_label(widgetbase, value='')
 		lab1 = widget_label(widgetbase, value='')
     		lab2 = widget_label(widgetbase, font=state.font,	$
     		value="      Checking file availability - please wait      ")
 		lab3 = widget_label(widgetbase, value='')
 		lab4 = widget_label(widgetbase, value='')
 		widget_control, widgetbase, /realize
		widget_control, topparent, sensitive=0

		;**** Firstly construct target directory names ****

		dirnames = strarr(8)		;user directories
		defnames = strarr(8)		;central (default) directories
		diravailable = intarr(8)
		defdiravailable = intarr(8)
		fileavailable = intarr(8)
		deffileavailable = intarr(8)

                dirnames = ['acd','scd','ccd','prb','prc','qcd','xcd','plt']
		items    = dirnames		;see cw_display_list below
		defnames = dirnames
		dirnames = dirnames + year
		defnames = defnames + dyear
		widget_control, state.filetype, get_value=flag
		if (flag eq 1) then begin
		    widget_control, state.parttype, get_value=partval
		    if (partval eq 0) then begin
			dirnames = dirnames + 'r'
			defnames = defnames + 'r'
		    endif else begin
			dirnames = dirnames + 'u'
			defnames = defnames + 'u'
		    endelse
		endif

; only have filtered data on power (plt, prb and prc) datatypes

		widget_control, state.filtr, get_value=filtr_val
		filterstring = strcompress(filtr_val(0), /remove_all)
		if filterstring ne '' then begin
			print,'making filteradd'
                   filteradd = ['','','','.'+filterstring,'.'+filterstring,  $
                                '','','.'+filterstring]
	        endif
		    
		;**** Now find which directories are available   ****
		;**** First those in the user space              ****
		;**** or central space if user has selected that ****

		widget_control, state.branch, get_value=branchflag
		if branchflag eq 0 then begin
		    listcom = ['ls','-l',userdir + '.']
                    listcom2 = ['ls',userdir + '.']
                    searchdir = userdir
		endif else begin
		    listcom = ['ls','-l', + centdir + '.']
		    listcom2 = ['ls',centdir + '.']
                    searchdir = centdir
		endelse
		spawn, listcom, full_list,/noshell	;get long-style list
                spawn, listcom2, short_list,/noshell    ;get short-style list

		;**** Pick out directories ****

		full_list = strcompress(full_list, /remove_all)
                dir_indices = where(strpos(full_list, 'd') eq 0) - 1
                if (dir_indices(0) eq -2) then begin ;no directories
                    searchfail = 0
		    diravailable(*) = 0
		    fileavailable(*) = 0
		endif else begin
		    dir_list = short_list(dir_indices)
                    no_dirs = n_elements(dir_list)
                endelse

		;**** Flag available directories ****

		if searchfail eq 1 then begin
		    for i=0,7 do begin
			match = where(dir_list eq dirnames(i))
			if (match(0) ne -1) then begin
			    diravailable(i) = 1
			endif else begin
			    diravailable(i) = 0
			endelse
		    endfor
		    searchtest = where(diravailable eq 1)
		    if searchtest(0) eq -1 then searchfail = 0
		endif

		;**** Now look into the available directories ****

		if searchfail eq 1 then begin
		    for i=0,(n_elements(searchtest) - 1) do begin
		      
                        listcom3 = ['ls','-l',searchdir +  		$
                        dirnames(searchtest(i))]
			listcom4 = ['ls',searchdir +			$
                        dirnames(searchtest(i))]
		        spawn, listcom3, full_list, /noshell 	;get long-style list
                        spawn, listcom4, short_list, /noshell	;get short-style list
			full_list = strcompress(full_list, /remove_all)

		;**** What file are we looking for? ****

                        if (member ne '') then begin
                            to_find = dirnames(searchtest(i)) + '_' + $
                                      member + '#'
                        endif else begin
                            to_find = dirnames(searchtest(i)) + '_'
                        endelse
                        
		        if filterstring ne '' then begin
                           to_find = to_find + symbol +  $ 
                                     filteradd(searchtest(i))  +  '.dat'
                        endif else begin
			   to_find = to_find + symbol + '.dat'
                        endelse
                        
			filematch = where(short_list eq to_find)
			if (filematch(0) eq -1) then begin
			    fileavailable(searchtest(i)) = 0
			endif else begin 
			    fileavailable(searchtest(i)) = 1
			endelse
                        
		    endfor
		endif

		;**** Now repeat for default year - if one is given ****

		if dyear ne '' then begin
       		    defsearchfail = 1
		    listcom5 = ['ls','-l',centdir + '.']
                    listcom6 = ['ls',centdir + '.']
		    spawn, listcom5, full_list,/noshell	;get long-style list
                    spawn, listcom6, short_list,/noshell ;get short-style list

		;**** Pick out directories ****

		    full_list = strcompress(full_list, /remove_all)
                    defdir_indices = where(strpos(full_list, 'd') eq 0) - 1
                    if (defdir_indices(0) eq -2) then begin ;no directories
                        defsearchfail = 0
		        defdiravailable(*) = 0
		        deffileavailable(*) = 0
		    endif else begin
		        defdir_list = short_list(defdir_indices)
                        no_defdirs = n_elements(defdir_list)
                    endelse

                ;**** Flag available directories ****

                    if defsearchfail eq 1 then begin
                        for i=0,7 do begin
                            match = where(defdir_list eq defnames(i))
                            if (match(0) ne -1) then begin
                                defdiravailable(i) = 1
                            endif else begin
                                defdiravailable(i) = 0
                            endelse
                        endfor
                        defsearchtest = where(defdiravailable eq 1)
                        if defsearchtest(0) eq -1 then defsearchfail = 0
                    endif

                ;**** Now look into the available directories ****

                    if defsearchfail eq 1 then begin
                        for i=0,(n_elements(defsearchtest) - 1) do begin
                            listcom7 = ['ls','-l',centdir +                 $
                            defnames(defsearchtest(i))]
                            listcom8 = ['ls',centdir +                    $
                            defnames(defsearchtest(i))]
                            spawn, listcom8, short_list,/noshell    ;get short-style list

                ;**** What file are we looking for? ****

                            if (member ne '') then begin
                                to_find =  defnames(defsearchtest(i)) + '_' + $
                                           member + '#'
                            endif else begin
                                to_find = defnames(defsearchtest(i)) + '_'
                            endelse
                            
		            if filterstring ne '' then begin
                               to_find = to_find + symbol +      $
                                         filteradd(defsearchtest(i)) + '.dat'
                            endif else begin
			       to_find = to_find + symbol + '.dat'
                            endelse
                            
                            filematch = where(short_list eq to_find)
                            if (filematch(0) eq -1) then begin
                                deffileavailable(defsearchtest(i)) = 0
                            endif else begin
                                deffileavailable(defsearchtest(i)) = 1
                            endelse
                                                        
                        endfor
                    endif
		endif

		;**** Search complete, display results ****

                ;**** Destroy the information widget ****
           
		widget_control, widgetbase, /destroy

                action = -1
		toptitle = 'ADAS'+state.rnum+' INPUT'
		displaytit = 'Class selection and file availability:-'
		headings = ['                      Member  Power             '+$
                            '    USER '+$
                            'DATA       DEFAULT DATA ',		$
                            'Class  Year  Element  Prefix   Filter    Type  '+$
                            '  Selected '+$
                            'Available  Used  Available ',		$
                            '-----  ----  -------  ------  -------  --------'+$
                            '  -------- '+$
                            '---------  ----  --------- ']
		if dyear ne '' then begin
		    headings(0) = headings(0) + '(' + dyear +')'
		endif else begin
		    headings(0) = headings(0) + '    '
		endelse
		items = ' ' + items + '    ' + year + '      ' + symbol
		if (strlen(symbol) eq 1) then items = items + ' '
		if member ne '' then begin
		    items = items + '      ' + member
		endif else begin
		    items = items + '        '
		endelse
		pad = '       '
                filterpad=replicate(pad,8)
		if filterstring ne '' then begin 
                    for i=0,7 do begin                             ; ugly but only
                       pad = '       '	                           ; way that's
		       strput, pad, strmid(filteradd(i), 1, 7), 0  ; acceptable to IDL
                       filterpad(i)=pad
                    endfor
		    items = items + '    ' + filterpad
		endif else begin
		    items = items + '    ' + pad
		endelse
		if flag eq 0 then begin
		    items = items + '  Standard'
		endif else begin
		    if partval eq 0 then begin
			items = items + '  RPartial'
		    endif else begin
			items = items + '  UPartial'
		    endelse
		endelse
		usedindices = where(state.indices eq 1)
		fileselected = replicate(' no', 8)
		yeses = 0
		if (usedindices(0) ne -1) then begin
		    fileselected(usedindices) = 'YES'
		    yeses = n_elements(usedindices)
		endif
		items = items + '     ' + fileselected
		fileavail = replicate(' no', 8)
		actualfiles = where(fileavailable eq 1)
		if actualfiles(0) ne -1 then begin
		    fileavail(actualfiles) = 'YES'
		endif
                items = items + '      ' + fileavail
		defused = replicate(' no', 8)
		defavail = replicate(' no', 8)
		if dyear ne '' then begin
		    deffilesavail = where(deffileavailable eq 1)
		    if deffilesavail(0) ne -1 then begin
			defavail(deffilesavail) = 'YES'
		    endif
		    teststrings = fileselected + fileavail + defavail
		    usedefault = where(teststrings eq 'YES noYES')
		    if usedefault(0) ne -1 then begin
			defused(usedefault) = 'YES'
		    endif
		endif
		items = items + '     ' + defused + '      ' + defavail
		footmessageset = 0
		messtest1 = fileselected + fileavail
		messchk1 = where(messtest1 eq 'YES no')
		if messchk1(0) eq -1 then begin
		    footmessageset = 1
		    footmessage = "All requested files available from "+$
				  "user data sets."
		endif else begin
		    messtest2 = messtest1 + defused + defavail
		    messchk2 = where(messtest2 eq 'YES no no no')
		    if messchk2(0) ne -1 then begin
		  	messchk3 = n_elements(messchk2)
			if messchk3 eq yeses then begin
			    footmessage = "None of the requested files are"+$
			                  " available."
			endif else begin
			    footmessage = "Some of the requested files are"+$
					  " not available."
			endelse
		    endif else begin
			footmessage = "All files requested are available,"+$
				      " but some must be drawn from the "+$
				      "default data sets."
		    endelse
		endelse
                cw_display_list, headings, items, action, 		$
                                 font=state.font, yoffset=50,		$
		                 title=displaytit, toptitle=toptitle,	$
				 footmessage=footmessage
		widget_control, topparent, sensitive=1
	    endelse
	end

        else : begin 				;**** do nothing ****
        end
    ENDCASE

    state.searchflag = searchflag

                ;*********************************************
                ;*** make "state" available to other files ***
                ;*********************************************

    widget_control, first_child, set_uvalue=state, /no_copy

    RETURN, event

END

;-------------------------------------------------------------------------
        
FUNCTION cw_master_classes, parent, topparent, classes, indices, 	$
                            userdir, centdir, FONT=font, TITLE=title, 	$
			    RNUM=rnum

    COMMON master_dirs, userdircom, centdircom
    COMMON mastercom, indicescom, topcom

    userdircom = userdir
    centdircom = centdir
    indicescom = indices
    topcom = topparent
    searchflag = 0			;has the user clicked search?

                ;**** Set defaults for keywords ****

    if (NOT KEYWORD_SET(font)) then font = ''
    if (NOT KEYWORD_SET(title)) then title = ' '
    if (NOT KEYWORD_SET(rnum)) then rnum = '405'

                ;**** Create main base for widget ****

    top = widget_base(parent, event_func='master_event',                $
                      func_get_val='master_get_val',                    $
                      pro_set_val='master_set_val',                     $
                      /column)

                ;******************************************************
                ;**** Create a dummy widget just to hold value of *****
                ;**** "state" variable so as not to get confused  *****
                ;**** with any other values. Adopt IDL practice   *****
                ;**** of using first child widget                 *****
                ;******************************************************

    first_child = widget_base(top)

    cwid = widget_base(first_child, /column)

                ;**** Title ****

    titleid = widget_label(cwid, value=title, font=font)


                ;**** New base for buttons ****

    infobase = widget_base(cwid, /frame)

                ;**** Buttons ****

    base = widget_base(infobase, /column)

		;**** Master classes menu ****

    classbase = widget_base(base, /row)
    classval = "Select iso-nuclear master collisional-dielectronic classes   :"
    classlabel = widget_label(classbase, font=font, value=classval)
    classbut = widget_button(classbase, value="  Select  ", font=font)

		;**** Radiated power filter typein ****

    filtrbase = widget_base(base, /row)
    filtrval = "                   Radiated power filter (blank for none)    :"
    filtrlabel = widget_label(filtrbase, font=font, value=filtrval)
    filtr = widget_text(filtrbase, value=' ', xsize=8, /editable,	$
                        font=font)

		;**** Member prefix ****

    membase = widget_base(base, /row)
    memval = "                           Member prefix (blank for none)    :"
    memlabel = widget_label(membase, font=font, value=memval)
    member = widget_text(membase, value=' ', xsize=8, /editable,       $
                        font=font)

		;**** Directory branch ****

    branbase = widget_base(base, /row)
    branval = "                                  Select directory branch    :"
    branlabel = widget_label(branbase, font=font, value=branval)
    branch = cw_bselector(branbase, ['User','Central'], font=font)

    		;**** Year typeins ****

    yearbase = widget_base(base, /row)
    yearval = " Year of data    :"
    dyearval = "   Default year (if required)    :"
    yearlabel = widget_label(yearbase, font=font, value=yearval)
    year = widget_text(yearbase, value = ' ', xsize=7, /editable, font=font)
    dyearlabel = widget_label(yearbase, font=font, value=dyearval)
    dyear = widget_text(yearbase, value = ' ', xsize=8, /editable, font=font)

		;**** Element symbol typein ****

    symbase = widget_base(base, /row)
    symval = "                                Isonuclear element symbol    :"
    symlabel = widget_label(symbase, font=font, value=symval)
    symbol = widget_text(symbase, value=' ', xsize=8, /editable, font=font)

		;**** File type ****

    filebase = widget_base(base, /row)
    fileval = "Type of master files  :"
    typeval = " Specify partial type code   :"
    filelabel = widget_label(filebase, value=fileval, font=font)
    filetype = cw_bselector(filebase, ['Standard',' Partial '], font=font)
    typelabel = widget_label(filebase, value=typeval, font=font)
    parttype = cw_bselector(filebase, [' Resolved  ','Unresolved'], font=font)

		;**** Display Dataset button ****

    butval = "Display data set availability"
    button = widget_button(base, value=butval, font=font)
    

                ;**** Create state structure ****

    state = {	font		:	font,				$
		parttype	:	parttype,			$
                typelabel	:	typelabel,			$
		filetype	:	filetype,			$
		indices		:	indices,			$
		classbut	:	classbut,			$
		year		:	year,				$
		dyear		:	dyear,				$
		branch		:	branch,				$
		member		:	member,				$
		symbol		:	symbol,				$
		button		:	button,				$
		classes		:	classes,			$
		searchflag	:	searchflag,			$
		filtr		:	filtr,				$
		rnum		:	rnum				}

                ;**** Save initial state structure ****

    widget_control, first_child, set_uvalue=state, /no_copy

    RETURN, top

END
