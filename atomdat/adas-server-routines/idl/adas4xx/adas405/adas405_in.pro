; Copyright (c) 1995 Strathclyde University .
; SCCS info: Module @(#)$Header: /home/adascvs/idl/adas4xx/adas405/adas405_in.pro,v 1.6 2004/07/06 10:42:28 whitefor Exp $ Date $Date: 2004/07/06 10:42:28 $
;+
; PROJECT:
;       ADAS IBM MVS to DEC UNIX conversion
;
; NAME:
;       ADAS405_IN
;
; PURPOSE:
;       IDL ADAS user interface, input file selection.
;
; EXPLANATION:
;       This routine creates and manages a pop-up window which allows
;       the user to select various input files from the UNIX file system.
;	Like the somewhat related adas401_in.pro upon which it is based,
;	this routine does not conform to the standard adas input
;	window. In this case it is due to the fact that for adas405
;	various input files need to be specified rather than just the
;	single one. There are therefore two lots of information
;	required before proceeding on to the processing options.
;	The name (if any) of the 'line and analysis selection file'
;	(or script file) and the associated 'isonuclear master files'.
; USE:
;       See d5spf0.pro for an example of how to
;       use this routine.
;
; INPUTS:
;       INVAL	- A structure which determines the initial settings of
;                 the input screen widgets.
;                 INVAL is passed un-modified through to cw_adas405_in.pro, 
;		  see that routine for a full description.
;
;       CLASSES - String array: the names of the isonuclear master classes
;
; OPTIONAL INPUTS:
;       None
;
; OUTPUTS:
;       INVAL   - On output the structure records the final settings of
;                 the input screen widgets if the user pressed the 
;		  'Done' button, otherwise it is not changed from input.
;
;       ACTION  - A string; indicates the user's action when the pop-up
;                 window is terminated, i.e which button was pressed to
;                 complete the input.  Possible values are 'Done' and
;                 'Cancel'.
;
; OPTIONAL OUTPUTS:
;       None
;
; KEYWORD PARAMETERS:
;       WINTITLE	- Title to be used on the banner for the pop-up window.
;                 	  Intended to indicate which application is running,
;                 	  e.g 'ADAS 405 INPUT'
;
;       FONT_LARGE      - Supplies the large font to be used for the
;                         interface widgets.
;
;       FONT_SMALL      - Supplies the small font to be used for the
;                         interface widgets.
;
;       RNUM      	- Number of the calling routine (in string form)
;
; CALLS:
;       CW_ADAS405_IN   Dataset selection widget creation.
;	DYNLABEL	Sets dynamic_resize keyword for label widgets.
;       XMANAGER        Manages the pop=up window.
;       ADAS405_IN_EV   Event manager, called indirectly during XMANAGER
;                       event management.
;
; SIDE EFFECTS:
;       XMANAGER is called in /modal mode. Any other widget become
;       inactive.
;
; CATEGORY:
;       Adas system.
;
; WRITTEN:
;       Tim Hammond, Tessella Support Services plc, 15th September 1995
;
; MODIFIED:
;       1.1     Tim Hammond
;		First release. Created from skeleton of adas401_in
;	1.2	Tim Hammond
;		Added input variable classes
;	1.3	Tim Hammond
;		Corrected name of VALUE structure passed to cw_adas405...
;	1.4	Tim Hammond
;		Tidied up code and comments
;	1.5	William Osborn
;		Added rnum keyword for different routines so that information
;		widget popped up has the correct title.
;	1.6    	William Osborn
;		Added dynlabel procedure
;
; VERSION:
;       1.1     15-09-95
;	1.2	15-09-95
;	1.3	25-09-95
;	1.4	07-11-95
;	1.5	25-06-96
;	1.6	09-07-96
;
;-----------------------------------------------------------------------------
;-

PRO adas405_in_ev, event

    COMMON in405_blk, action, inval

                ;**** Find the event type and copy to common ****

    action = event.action

    CASE action OF

                ;**** 'Done' button ****

        'Done'  : begin

                ;**** Get the output widget value ****

            widget_control, event.id, get_value=inval
            widget_control, event.top, /destroy

        end


                ;**** 'Cancel' button ****

        'Cancel': widget_control, event.top, /destroy

    ENDCASE

END

;-----------------------------------------------------------------------------


PRO adas405_in, inval, classes, action, WINTITLE=wintitle,		$
                FONT_LARGE=font_large, FONT_SMALL=font_small, RNUM = rnum

    COMMON in405_blk, actioncom, invalcom

                ;**** Copy value to common ****

    invalcom = inval

                ;**** Set defaults for keywords ****

    IF NOT (KEYWORD_SET(wintitle)) THEN wintitle = 'ADAS INPUT FILE'
    IF NOT (KEYWORD_SET(font_large)) THEN font_large = ''
    IF NOT (KEYWORD_SET(font_small)) THEN font_small = ''
    IF NOT (KEYWORD_SET(rnum)) THEN rnum = '405'

                ;*********************************
                ;**** Pop-up the input screen ****
                ;*********************************

                ;**** create base widget ****

    inid = widget_base(TITLE=wintitle, XOFFSET=100, YOFFSET=1)

                ;**** create input compound widget ****

    cwid = cw_adas405_in(inid, classes, VALUE=inval, 			$
                         FONT_LARGE=font_large,	FONT_SMALL=font_small,	$
			 RNUM = rnum)

                ;**** Realize the new widget ****

    dynlabel, inid
    widget_control, inid, /realize

                ;**** make widget modal ****

    xmanager, 'adas405_in', inid, event_handler='adas405_in_ev', 	$
              /modal, /just_reg

                ;**** Return the output value from common ****

    action = actioncom
    inval = invalcom

END
