; Copyright (c) 1995, Strathclyde University
; SCCS INFO : Module @(#)$Header: /home/adascvs/idl/adas4xx/adas405/d5ispf.pro,v 1.3 2004/07/06 13:15:24 whitefor Exp $ Date $Date: 2004/07/06 13:15:24 $
;
;+
; PROJECT:
;       ADAS IBM MVS to UNIX conversion
;
; NAME:
;       D5ISPF
;
; PURPOSE:
;       IDL user interface and communications with ADAS405 FORTRAN
;       process via pipe.
;
; EXPLANATION:
;       The routine begins by reading information from the ADAS405
;       FORTRAN process via a UNIX pipe.  Then part of the ADAS405
;       IDL user interface is invoked to determine how the user
;       wishes to process the input dataset.  When the user's
;       interactions are complete the information gathered with
;       the user interface is written back to the FORTRAN process
;       via the pipe.  Communications are to the FORTRAN subroutine
;	D5ISPF.
; USE:
;       The use of this routine is specific to ADAS405, see adas405.pro.
; INPUTS:
;       PIPE    - The IDL unit number of the bi-directional pipe to the
;                 ADAS405 FORTRAN process.
;
;       PROCVAL - A structure which determines the initial settings of
;                 the processing options widget.  The initial value is
;                 set in adas405.pro.  If adas405.pro passes a blank
;                 dummy structure of the form {NEW:-1} into PROCVAL then
;                 PROCVAL is reset to a default structure.
;
;                 The PROCVAL structure is;
;                       procval = {                             $
;                                       new   : 0 ,             $
;                                       title : '',             $
;					lsnull: lsnull,		$
;					ifout : 1,		$
;					ispline: 0,		$
;					hyval : 1.0,		$
;					elval : float,		$
;                                       tein  : fltarr(ntdmax), $
;                                       tihn  : fltarr(ntdmax), $
;                                       dein  : fltarr(ntdmax), $
;                                       dhin  : fltarr(ntdmax), $
;                                       maxt  : 0,              $
;                                       maxd  : 0,              $
;                                       tvals : tvals		}
;
;                 See cw_adas405_proc.pro for a full description of this
;                 structure.
;
;	DSFULL	- String; the full name of the script file to be used.
;
;	SYMBOL	- String; isonuclear element symbol.
;
;	CLASSSTRING - String; list of the selected master classes.
;
;	LSNULL	- Integer; flag showing whether a null script file has
;		  been selected (=1) or not (=0)
;	
;	BITFILE - String; the path to the dirctory containing bitmaps
;		  for the 'escape to series menu' button.
;
; OPTIONAL INPUTS:
;       None
;
; OUTPUTS:
;       LPEND   - Integer, 0 if the user pressed the 'Done' button
;                 or 1 if the user pressed 'Cancel'.  Maps directly onto
;                 the logical variable LPEND in ADAS405 FORTRAN.
;
;       PROCVAL - On output the structure records the final settings of
;                 the processing selections widget if the user pressed the
;                 'Done' button, otherwise it is not changed from input.
;
;	CTITLE  - String array: Any information given in script file on
;		  lines to be analysed.
;
;	CMPTS   - Int array: no. components for each line in script file
;
;	GOMENU	- Int; flag - set to 1 if user has selected 'escape direct
;		  to series menu , otherwise 0
;
; OPTIONAL OUTPUTS:
;       None
;
; KEYWORD PARAMETERS:
;       FONT_LARGE - The name of a larger font.
;
;       FONT_SMALL - The name of a smaller font.
;
;       EDIT_FONTS - A structure of two fonts used in the table editor
;                    adas_edtab.pro, {font_norm:'',font_input:''}.
;
; CALLS:
;       ADAS405_PROC    Invoke the IDL interface for ADAS405 data
;                       processing options/input.
;	XXTCON		General temperature conversion routine
;	XXEIAM		Converts element symbol to atomic mass
;
; SIDE EFFECTS:
;       Two way communications with ADAS405 FORTRAN via the
;       bi-directional UNIX pipe.
;
; CATEGORY:
;       Adas system.
;
; WRITTEN:
;       Tim Hammond, Tessella Support Services plc, 7th November 1995
;
; MODIFIED:
;       1.1       Tim Hammond  
;			First release
;	  1.2		Richard Martin
;			Increased default temperature set to 30.	
;	  1.3 	Richard Martin
;			Corrected mistake in default temperatures		
;
; VERSION:
;       1.1       07-11-95
;	  1.2		08-06-98
;	  1.3		20-11-98
;
;-
;-----------------------------------------------------------------------------

PRO d5ispf, pipe, lpend, procval, dsfull,				$
            symbol, classstring, lsnull,				$
	    ctitle, cmpts, gomenu, bitfile,				$
            FONT_LARGE=font_large, FONT_SMALL=font_small,		$
	    EDIT_FONTS=edit_fonts


                ;**** Set defaults for keywords ****

    IF NOT (KEYWORD_SET(font_large)) THEN font_large = ''
    IF NOT (KEYWORD_SET(font_small)) THEN font_small = ''
    IF NOT (KEYWORD_SET(edit_fonts)) THEN                               $
    edit_fonts = {font_norm:'',font_input:''}

                ;*********************************************
                ;****     Declare variables for input     ****
                ;**** arrays will be declared after sizes ****
                ;**** have been read.                     ****
                ;*********************************************

    ntdmax = 0
    input = 0
    fdum = 0.0
    sdum = ''

                ;********************************
                ;**** Read data from fortran ****
		;**** Set up default temps   ****
                ;********************************

    readf, pipe, input
    ntdmax = input
    readf, pipe, input
    nline = input
    if nline gt 0 then begin
        ciion = strarr(nline)
        cmpts = strarr(nline)
        ctitle = strarr(nline)
        for i=0, (nline - 1) do begin
	    readf, pipe, sdum
	    ciion(i) = sdum
	    readf, pipe, sdum
	    cmpts(i) = sdum
	    readf, pipe, sdum
	    ctitle(i) = sdum
        endfor
    endif
    tvals = dblarr(2,ntdmax)
    tvals(0,*) = [0.1,0.2,0.3,0.5,0.7,1.0,1.5,2.0,3.0,			$
    			5.0,7.0,10.0,15.0,20.0,30.0,50.0,70.0,			$
    			100.0,150.0,200.0,300.0,500.0,700.0,1000.0,1500.0,	$
    			2000.0,3000.0,5000.0,7000.0,10000.0 ]	;default temps. (eV)

    xxtcon, 2, 1, 1, ntdmax, tvals(0,*), output       ;Input temps. (Kelvin)
    tvals(1,*) = output
                                                        
                ;*******************************************
                ;**** Set default value if non provided ****
                ;*******************************************

    if (procval.new lt 0) then begin
        temp_arr = fltarr(ntdmax)
        xxeiam, symbol, elmass
        procval = {                     $
                   new   : 0 ,          $
                   title : '',          $
		   lsnull: lsnull,	$
		   ifout : 1,		$
		   ispline: 0,		$
		   hyval : 1.0,		$
		   elval : elmass,	$
                   tein  : temp_arr,    $
                   tihn  : temp_arr,    $
                   dein  : temp_arr,    $
                   dhin  : temp_arr,    $
                   maxt  : 0,           $
                   maxd  : 0,           $
                   tvals : tvals        $
                  }
    endif
    procval.lsnull = lsnull

                                                                    
                ;****************************************
                ;**** Pop-up processing input widget ****
                ;****************************************
 
    adas405_proc, procval, dsfull, action,				$
                  symbol, classstring,					$
		  ntdmax, nline, ciion, cmpts, ctitle, bitfile, 	$
	          FONT_LARGE=font_large, FONT_SMALL=font_small,         $
                  EDIT_FONTS=edit_fonts

                ;*********************************************
                ;**** Act on the output from the widget   ****
                ;**** There are three    possible actions ****
                ;**** 'Done', 'Cancel' and 'Menu'.        ****
                ;*********************************************

    if action eq 'Done' then begin
        lpend = 0
    endif else if action eq 'Menu' then begin
        lpend = 0
        gomenu = 1
    endif else begin
        lpend = 1
    endelse

                ;*******************************
                ;**** Write data to fortran ****
                ;*******************************
                                                                
    printf, pipe, lpend
    if lpend eq 0 then begin
	printf, pipe, procval.title
        printf, pipe, (procval.ispline+1)
	printf, pipe, (3 - procval.ifout)
	printf, pipe, procval.maxt
	printf, pipe, procval.elval
	printf, pipe, procval.hyval
	for i=0, (procval.maxt-1) do begin
	    printf, pipe, procval.tein(i)
	    printf, pipe, procval.dein(i)
	    printf, pipe, procval.tihn(i)
	    printf, pipe, procval.dhin(i)
	endfor
    endif

END
