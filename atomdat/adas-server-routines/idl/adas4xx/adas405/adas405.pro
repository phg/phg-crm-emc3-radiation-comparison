; Copyright (c) 1995, Strathclyde University.
; SCCS info: Module @(#)$Header: /home/adascvs/idl/adas4xx/adas405/adas405.pro,v 1.20 2004/07/06 10:41:44 whitefor Exp $ Date $Date: 2004/07/06 10:41:44 $
;+
; PROJECT:
;       ADAS IBM MVS to UNIX conversion
;
; NAME:
;	ADAS405
;
; PURPOSE:
;	The highest level routine for the ADAS 405 program.
;
; EXPLANATION:
;	This routine is called from the main adas system routine, adas.pro,
;	to start the ADAS 405 application.  Associated with adas405.pro
;	is a FORTRAN executable.  The IDL code provides the user
;	interface and output graphics whilst the FORTRAN code reads
;	in data files, performs numerical processing and creates the
;	output files.  The IDL code communicates with the FORTRAN
;	executable via a bi-directional UNIX pipe.  The unit number
;	used by the IDL for writing to and reading from this pipe is
;	allocated when the FORTRAN executable is 'spawned' (see code
;	below).  Pipe communications in the FORTRAN process are to
;	stdin and stdout, i.e streams 5 and 6.
;
;	The FORTRAN code is an independent process under the UNIX system.
;	The IDL process can only exert control over the FORTRAN in the
;	data which it communicates via the pipe.  The communications
;	between the IDL and FORTRAN must be exactly matched to avoid
;	input conversion errors.  The correct ammounts of data must be
;	passed so that neither process 'hangs' waiting for communications
;	which will never occur.
;
;	The FORTRAN code performs some error checking which is
;	independent of IDL.  In cases of error the FORTRAN may write
;	error messages.  To prevent these error messages from conflicting
;	with the pipe communications all FORTRAN errors are written to
;	output stream 0, which is stderr for UNIX.  These error messages
;	will appear in the window from which the ADAS session/IDL session
;	is being run.
;
;	In the case of severe errors the FORTRAN code may terminate
;	itself prematurely.  In order to detect this, and prevent the
;	IDL program from 'hanging' or crashing, the IDL checks to see
;	if the FORTRAN executable is still an active process before
;	each group of pipe communications.  The process identifier
;	for the FORTRAN process, PID, is recorded when the process is
;	first 'spawned'.  The system is then checked for the presence
;	of the FORTRAN PID.
;
; USE:
;	First the system settings must be established by calling
;	adas_sys_set.pro then adas405.pro is called to start the
;	ADAS 405 application;
;
;	adas_sys_set, adasrel, fortdir, userroot, centroot, $
;		      devlist, devcode, font_large, font_small, edit_fonts
;	adas405,   adasrel, fortdir, userroot, centroot, devlist, $
;		   devcode, font_large, font_small, edit_fonts
;
; INPUTS:
;	ADASREL - A string indicating the ADAS system version, 
;		  e.g ' ADAS RELEASE: ADAS93 V1.4'.  The first
;		  character should be a space.
;
;	FORTDIR - A string holding the path to the directory where the
;		  FORTRAN executables are, e.g '/disk/adas/fortran/exec'
;
;	USERROOT - A string holding the path to the root directory of
;		   the user's adas data. e.g '/disk/bowen/adas'
;		   This root directory will be used by adas to construct
;		   other path names.  For example the users default data
;		   for adas205 should be in /disk/bowen/adas/adf04.  In
;		   particular the user's default interface settings will
;		   be stored in the directory USERROOT+'/defaults'.  An
;		   error will occur if the defaults directory does not
;		   exist.
;
;	CENTROOT - Like USERROOT, but this directory points to the
;		   central data area for the system.  User defaults are
;		   not stored on CENTROOT.
;
;	DEVLIST - A string array of hardcopy device names, used for
;		  graphical output. e.g ['Post-Script','HP-PCL','HP-GL']
;		  This array must mirror DEVCODE.  DEVCODE holds the
;		  actual device names used in a SET_PLOT statement.
;
;	DEVCODE - A string array of hardcopy device code names used in
;		  the SET_PLOT statement, e.g ['PS', 'PCL', 'HP']
;		  This array must mirror DEVLIST.
;
;	FONT_LARGE - The name of a larger font e.g 'courier_bold14'
;
;	FONT_SMALL - The name of a smaller font e.g 'courier_bold12'
;
;	EDIT_FONTS - A structure of two fonts used in the table editor
;		     adas_edtab.pro, {font_norm:'',font_input:''}.
;		     The two fonts are used to differentiate between
;		     editable and non-editable parts of the table. You
;		     may have to experiment to get two fonts which give
;		     the same default spacing in the edit widget.
;		     e.g {font_norm:'helvetica_bold14', $
;			  font_input:'helvetica_oblique14'}
;
; OPTIONAL INPUTS:
;	None
;
; OUTPUTS:
;	None - Note this routine should leave all inputs un-modified.
;
; OPTIONAL OUTPUTS:
;	None
;
; KEYWORD PARAMETERS:
;	None
;
; CALLS:
;	FIND_PROCESS	Checks to see if a given process is active.
;       XXDATE          Get date and time from operating system.
;       D5SPF0          Pipe comms with FORTRAN D5SPF0 routine.
;       D5ISPF          Pipe comms with FORTRAN D5ISPF routine.
;       D5SPF1          Pipe comms with FORTRAN D5SPF1 routine.
;	D5OTG1		Pipe comms with FORTRAN D5OTG1 routine.
;	D5OTG2		Pipe comms with FORTRAN D5OTG2 routine.
;	D5OTG3		Pipe comms with FORTRAN D5OTG3 routine.
;
; SIDE EFFECTS:
;	This routine spawns a FORTRAN executable.  Note the pipe 
;	communications routines listed above.  In addition to these
;	pipe communications there is one explicit communication of the
;	date to the FORTRAN code, search for 'printf, pipe' to find it.
;	There are also communications of the variable gomenu to the
;	FORTRAN which is used as a signal to stop the program in its
;	tracks and return immediately to the series menu. Do the same
;	search as above to find the instances of this.
;
; CATEGORY:
;	Adas system.
;	
; WRITTEN:
;       Tim Hammond, Tessella Support Services plc, 14/09/1995
;
; MODIFIED:
;	1.1	Tim Hammond		
;		First release - created from adas401.pro
;	1.2	Tim Hammond		
;		First complete version
;	1.3	Tim Hammond
;		Changed default directory for script files to 
;		central adas rather than user's own.
;	1.4	Tim Hammond
;		Added an information widget for when graphical
;		output data is being passed from FORTRAN to IDL
;		and FORTRAN is opening and reading files.
;	1.5	Tim Hammond
;		Added read from FORTRAN to check that the script
;		file was read okay.
;	1.6	Tim Hammond
;		Altered way in which output information widget
;		is created to prevent it from remaining when it
;		should be destroyed.
;	1.7	Tim Hammond
;		Added reset of output screen messages when the
;		'menu' button is used.
;	1.8	Tim Hammond
;		Added extra checking to see whether FORTRAN crashes
;		during execution of d5spf1.
;	1.9	Tim Hammond
;		Ensured that text output messages are cleared when
;		escape to menu button is used.
;	1.10	Tim Hammond
;		Same as above, but for goft passing file messages.
;	1.11	Tim Hammond
;		Updated version number to 1.2
;	1.12	Tim Hammond
;		Updated version number to 1.3
;	1.13	William Osborn
;		Updated version number to 1.4
;	1.14	William Osborn
;		Updated version number to 1.5
;	1.15	William Osborn
;		Updated version number to 1.6
;	1.15	Richard Martin
;		Updated version number to 1.7
;	1.16	Richard Martin
;		Updated version number to 1.8
;	1.17	Richard Martin
;		Updated version number to 1.9
;	1.18	Richard Martin
;		Updated version number to 1.10
;	1.19	Martin O'Mullane
;		Replaced passing name file to adas405_adf16.pass to 
;               better indicate its meaning.
;		Increased version no. to 1.11.
;	1.20	Richard Martin
;		Updated version number to 1.12
;
; VERSION:
;       1.1	14-09-95
;	1.2	07-11-95
;	1.3	08-11-95
;	1.4	10-11-95
;	1.5	10-11-95
;	1.6	13-11-95
;	1.7	13-11-95
;	1.8	22-11-95
;	1.9	06-02-96
;	1.10	06-02-96
;	1.11	16-02-96
;	1.12	13-05-96
;	1.13	11-07-96
;	1.14	14-10-96
;       1.15	25-11-96
;	1.15	07-04-97
;	1.16	11-03-98
;	1.17	08-06-98
;	1.18	04-12-98
;	1.19	06-02-01
;	1.19	18-03-02
; 
;-
;-----------------------------------------------------------------------------

PRO ADAS405,	adasrel, fortdir, userroot, centroot, 			$
		devlist, devcode, font_large, font_small, edit_fonts

		;************************
		;**** Initialisation ****
		;************************

    adasprog = ' PROGRAM: ADAS405 V1.12'
    lpend = 0
    gomenu = 0
    deffile = userroot+'/defaults/adas405_defaults.dat'
    bitfile = centroot+'/bitmaps'
    device = ''
    classes = ['ACD - Recombination coefficients',			$
               'SCD - Ionisation coefficients',				$
               'CCD - Recombination coefficients: charge exchange',	$
	       'PRB - Power coefficients: recombination-bremsstrahlung',$
	       'PRC - Power coefficients: charge-exchange recombination',$
	       'QCD - Cross-coupling coefficients',			$
	       'XCD - Cross-coupling coefficients: parent',		$
	       'PLT - Line power coefficients: total']
    dataclasses = ['ACD','SCD','CCD','PRB','PRC','QCD','XCD','PLT']
    indices = intarr(8)
    indices(0:1) = 1

		;******************************************
		;**** Search for user default settings ****
		;**** If not found create defaults     ****
		;**** inval: settings for data files   ****
		;**** scval: settings for script files ****
		;**** procval: settings for processing ****
		;**** outval: settings for output      ****
		;******************************************

    files = findfile(deffile)
    if files(0) eq deffile then begin
        restore, deffile
        inval.centroot = centroot+'/adf11/'
        inval.userroot = userroot+'/adf11/'
        inval.sccentroot = centroot+'/scripts405/'
        inval.scuserroot = userroot+'/scripts405/'
    endif else begin
        inval = { 							$
		  ROOTPATH:userroot+'/adf11/', 				$
		  CENTROOT:centroot+'/adf11/', 				$
		  USERROOT:userroot+'/adf11/',				$
		  SCROOTPATH:centroot+'/scripts405/', 			$
		  SCFILE:'', 						$
		  SCCENTROOT:centroot+'/scripts405/', 			$
		  SCUSERROOT:userroot+'/scripts405/',			$
		  FILTR:'',						$
		  BRANCH:0,						$
		  FILETYPE:0,						$
		  PARTTYPE:0,						$
		  YEAR:'',						$
		  DYEAR:'',						$
		  SYMBOL:'',						$
		  MEMBER:'',						$
		  INDICES:indices 					$
		}
  
      procval = {NEW:-1}

      outval = { 							$
 			LPION_AVAIL	:	1,			$
 			LPOWER_AVAIL	:	0,			$
 			LCONT_AVAIL	:	0,			$
 			LGRAPHNO	:	0,			$
                  	XMIN1		:	'',  			$
			XMAX1		:	'',			$
                  	YMIN1		:	'',  			$
			YMAX1		:	'',			$
                  	XMIN2		:	'',  			$
			XMAX2		:	'',			$
                  	YMIN2		:	'',  			$
			YMAX2		:	'',			$
                  	XMIN3		:	'',  			$
			XMAX3		:	'',			$
                  	YMIN3		:	'',  			$
			YMAX3		:	'',			$
			LDEF1		:	0,			$
			LDEF2		:	0,			$
			LDEF3		:	0,			$
		 	GRPOUT		:	0,			$
                  	GTIT1		:	'',  			$
                  	HRDOUT		:	0, 			$
			HARDNAME	:	'',			$
                  	GRPDEF		:	'', 			$
			GRPFMESS	:	'',			$
                  	DEVSEL		:	-1, 			$
                  	TEXOUT		:	0, 			$
			TEXAPP		:	-1, 			$
                  	TEXREP		:	0, 			$
			TEXDSN		:	'',			$
                  	TEXDEF		:	'paper.txt',		$
			TEXMES		:	'',			$
		  	GOFOUT		:	0, 			$
			GOFAPP		:	-1,			$
		  	GOFREP		:	0, 			$
			GOFDSN		:	'',			$
		  	GOFDEF		:	'adas405_adf16.pass',   $
			GOFMES		:	''			}
    end


		;****************************
		;**** Start fortran code ****
		;****************************

    spawn, fortdir+'/adas405.out', unit=pipe, /noshell, PID=pid

		;************************************************
		;**** Get date and write to fortran via pipe ****
		;************************************************

    date = xxdate()
    printf, pipe, date(0)

LABEL100:
		;**** Check FORTRAN still running ****
  
    if find_process(pid) eq 0 then goto, LABELEND

		;************************************************
		;**** Communicate with d5spf0 in fortran and ****
		;**** invoke user interface widget for       ****
		;**** Data file selection                    ****
		;************************************************

    d5spf0, pipe, inval, classes, rep, lsnull,				$
	    FONT_LARGE=font_large, FONT_SMALL=font_small

		;**** If cancel selected then end program ****

    if rep eq 'YES' then goto, LABELEND

LABEL200:
		;**** Check FORTRAN still running ****

    if find_process(pid) eq 0 then goto, LABELEND

		;**** If there was a script file was it okay? ****

    input = 0
    if lsnull eq 0 then begin
	readf, pipe, input
	if input ne 0 then goto, LABELEND
    endif

		;************************************************
		;**** Communicate with d5ispf in fortran and ****
		;**** invoke user interface widget for       ****
		;**** Processing options                     ****
		;************************************************

    dsfull = inval.scrootpath + inval.scfile
    classstring = ''
    clasflg = where((inval.indices eq 1) and ((inval.fileavailable eq 1)$
              or (inval.defavailable eq 1)))
    if (clasflg(0) ne -1) then begin
	for i=0, n_elements(clasflg) - 1 do begin
	    if classstring ne '' then classstring = classstring + ', '
	    classstring = classstring + dataclasses(clasflg(i))
	endfor
    endif else begin
	classstring = 'NONE'
    endelse
    d5ispf, pipe, lpend, procval, dsfull, 				$
	    inval.symbol, classstring, lsnull,				$
	    ctitle, cmpts, gomenu, bitfile, 				$
	    FONT_LARGE=font_large, FONT_SMALL=font_small, 		$
            EDIT_FONTS=edit_fonts

		;**** If menu button clicked, tell FORTRAN to stop ****

    if gomenu eq 1 then begin
 	printf, pipe, 1
 	goto, LABELEND
    endif else begin
 	printf, pipe, 0
    endelse


		;**** If cancel selected then goto 100 ****

    if lpend eq 1 then goto, LABEL100

		;**** Fortran processing now in progress. Get ****
		;**** date and time for this run.	      ****

    date = xxdate()

		;**** Create header for output. ****

    header = adasrel+adasprog+' DATE: '+date(0)+' TIME: '+date(1)

		;**** Check FORTRAN still running ****

    if find_process(pid) eq 0 then goto, LABELEND

LABEL300:
		;**** Check FORTRAN still running ****

    if find_process(pid) eq 0 then goto, LABELEND

		;************************************************
		;**** Communicate with d5spf1 in fortran and ****
		;**** invoke user interface widget for       ****
		;**** Output options                         ****
		;************************************************

		;**** Is it possible to have a passing file? ****

    if outval.gofout eq 1 then begin
	if lsnull eq 1 then outval.gofout=0
    endif

		;**** Decide availability of output plots ****

    if lsnull eq 0 then begin
	outval.lcont_avail = 1
    endif else begin
	outval.lcont_avail = 0
	outval.ldef3 = 0
    endelse
    prbchk = strpos(classstring, 'PRB')
    prcchk = strpos(classstring, 'PRC')
    pltchk = strpos(classstring, 'PLT')
    if (prbchk(0) ne -1 or prcchk(0) ne -1 or pltchk(0) ne -1) then begin
	outval.lpower_avail = 1
    endif else begin
	outval.lpower_avail = 0
	outval.ldef2 = 0
    endelse
    outval.hrdout = 0
    d5spf1, pipe, lpend, outval, dsfull, header, lsnull, bitfile, gomenu,$
	    graph_out, pid, DEVLIST=devlist, FONT_LARGE=font_large,	 $
            FONT_SMALL=font_small

                ;**** Extra check to see whether FORTRAN is still there ****

    if find_process(pid) eq 0 then goto, LABELEND

		;**** If menu button clicked, tell FORTRAN to stop ****

    if gomenu eq 1 then begin
	printf, pipe, 1
        outval.texmes = ''
        outval.gofmes = ''
	goto, LABELEND
    endif else begin
	printf, pipe, 0
    endelse


		;**** If cancel selected then erase output ****
		;**** messages and goto 200.		   ****

    if lpend eq 1 then begin
        outval.texmes = ''
        outval.gofmes = ''
        goto, LABEL200
    endif 

                ;**** Create an information widget ****

    if outval.grpout eq 1 then begin
        widget_control, /hourglass
        widgetbase = widget_base(/column, xoffset=300, yoffset=200,	$
        title = "ADAS405: INFORMATION")
        lab0 = widget_label(widgetbase, value='')
        lab1 = widget_label(widgetbase, value='')
        lab2 = widget_label(widgetbase, font=font_large,  		$
        value=" Retrieving data from files - "+				$
        "please wait      ")
        lab3 = widget_label(widgetbase, value='')
        lab4 = widget_label(widgetbase, value='')
    endif else begin
	widgetbase = 0L
    endelse

		;************************************************
                ;**** If graphical output requested          ****
		;**** Communicate with fortran and           ****
		;**** invoke widget Graphical output         ****
		;************************************************

    if outval.grpout eq 1 then begin

		;**** Hardcopy output ****

        hrdout = outval.hrdout
        hardname = outval.hardname
        if outval.devsel ge 0 then device = devcode(outval.devsel)

                ;*** user title for graph ***

        utitle = outval.gtit1
       
        if (outval.lgraphno eq 0 and outval.lpion_avail eq 1) then begin
            widget_control, widgetbase, /realize
	    d5otg1, dsfull, pipe, utitle, outval.ldef1, outval.xmin1,	$
		    outval.xmax1, outval.ymin1, outval.ymax1, hrdout,	$
	            hardname, device, inval.year, inval.dyear,		$
                    header, bitfile, gomenu, widgetbase, FONT=font_large
	endif else if (outval.lgraphno eq 1)  and 			$
        (outval.lpower_avail eq 1) then begin
            widget_control, widgetbase, /realize
	    d5otg2, dsfull, pipe, utitle, outval.ldef2, outval.xmin2,	$
	            outval.xmax2, outval.ymin2, outval.ymax2, hrdout,	$
		    hardname, device, inval.year, inval.dyear,          $
                    header, bitfile, gomenu, widgetbase, FONT=font_large
	endif else if (outval.lgraphno eq 2)  and 			$
        (outval.lcont_avail eq 1) then begin
            widget_control, widgetbase, /realize
	    d5otg3, dsfull, pipe, utitle, outval.ldef3, outval.xmin3,	$
	            outval.xmax3, outval.ymin3, outval.ymax3, hrdout,	$
		    hardname, device, inval.year, inval.dyear,          $
                    header, procval.ispline, ctitle(procval.ispline),	$
                    cmpts(procval.ispline), bitfile, gomenu, 		$
                    widgetbase, FONT=font_large
	endif

		;**** If menu button clicked, tell FORTRAN to stop ****

    if graph_out eq 1 then begin
        if gomenu eq 1 then begin
            outval.texmes = ''
            outval.gofmes = ''
            printf, pipe, 1
            goto, LABELEND
        endif else begin
            printf, pipe, 0
            endelse
        endif
    endif
	
		;**** Now destroy info widget if it is still there ****

    chkflag = widget_info(widgetbase, /valid_id)
    if chkflag eq 1 then widget_control, widgetbase, /destroy
  

		;**** Back for more output options ****

    GOTO, LABEL300

LABELEND:


		;**** Save user defaults ****

    save, inval, procval, outval, filename=deffile


END
