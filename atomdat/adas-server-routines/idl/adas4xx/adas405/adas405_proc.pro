; Copyright (c) 1995, Strathclyde University .
; SCCS Info : Module @(#)$Header: /home/adascvs/idl/adas4xx/adas405/adas405_proc.pro,v 1.2 2004/07/06 10:42:38 whitefor Exp $ Date $Date: 2004/07/06 10:42:38 $
;+
; PROJECT:
;       ADAS IBM MVS to UNIX conversion
;
; NAME:
;	ADAS405_PROC
;
; PURPOSE:
;	IDL ADAS user interface, processing options/input.
;
; EXPLANATION:
;	This routine creates and manages a pop-up window which allows
;	the user to select options and input data to control ADAS405
;	processing.
;
; USE:
;	This routine is ADAS405 specific, see d5ispf.pro for how it
;	is used.
;
; INPUTS:
;	PROCVAL	- A structure which determines the initial settings of
;		  the processing options widget.  The value is passed
;		  unmodified into cw_adas405_proc.pro.
;
;		  See cw_adas405_proc.pro for a full description of this
;		  structure.
;
;	DSFULL	- String; The full system file name of the input 
;		          dataset selected by the user for processing.
;       SYMBOL  - String; isonuclear element symbol.
;
;       CLASSSTRING - String; list of the selected master classes.
;
;	NTDMAX	- Int; max number of user entered temp/densities
;
;	NLINE	- Int; No. of lines in script file
;
;	CIION	- String array; emitting ion for a given line
;
;       CMPTS   - Int array: no. components for each line in script file
;
;       CTITLE  - String array: Any information given in script file on
;                 lines to be analysed.
;
;       BITFILE - String; the path to the directory containing bitmaps
;                 for the 'escape to series menu' button.
;
; OPTIONAL INPUTS:
;	None
;
; OUTPUTS:
;	PROCVAL	- On output the structure records the final settings of
;		  the processing selections widget if the user pressed the
;		  'Done' button, otherwise it is not changed from input.
;
;	ACT	- String;'Done', 'Cancel' or 'Menu' for the button the
;		  user pressed to terminate the processing options
;		  window.
;
; OPTIONAL OUTPUTS:
;	None
;
; KEYWORD PARAMETERS:
;	FONT_LARGE - The name of a larger font.
;
;	FONT_SMALL - The name of a smaller font.
;
;	EDIT_FONTS - A structure of two fonts used in the table editor
;		     adas_edtab.pro, {font_norm:'',font_input:''}.
;
; CALLS:
;	DYNLABEL	Sets dynamic_resize keyword for label widgets.
;	CW_ADAS405_PROC	Declares the processing options widget.
;	ADAS405_PROC_EV Called indirectly during widget management,
;			routine included in this file.
;	XMANAGER
;
; SIDE EFFECTS:
;       This widget uses a common block PROC405_BLK, to pass the
;       variables VALUE and ACTION between the two routines. 
;
; CATEGORY:
;	Widgets
;
; WRITTEN:
;       Tim Hammond, Tessella Support Services plc, 7th November 1995
;
; MODIFIED:
;	1.1		Tim Hammond			
;			First release
;	1.2    		William Osborn
;			Added dynlabel procedure
;
; VERSION:
;	1.1		07-11-95
;	1.2		09-07-96
;-
;-----------------------------------------------------------------------------


PRO adas405_proc_ev, event


    COMMON proc405_blk, action, value

    action = event.action
    CASE event.action OF

		;**** 'Done' button ****

	'Done'  : begin

		;**** Get the output widget value ****

	    widget_control, event.id, get_value=value 
	    widget_control, event.top, /destroy

	end

		;**** 'Cancel' button ****

	'Cancel': widget_control, event.top, /destroy

		;**** 'Menu' button ****

	'Menu': widget_control, event.top, /destroy

    END

END

;-----------------------------------------------------------------------------


PRO adas405_proc, procval, dsfull, act,					$
                  symbol, classstring,					$
		  ntdmax, nline, ciion, cmpts, ctitle, bitfile, 	$
		  FONT_LARGE=font_large, FONT_SMALL=font_small,	 	$
		  EDIT_FONTS=edit_fonts


		;**** declare common variables ****

    COMMON proc405_blk, action, value

		;**** Copy "procval" to common ****

    value = procval

		;**** Set defaults for keywords ****

    IF NOT (KEYWORD_SET(font_large)) THEN font_large = ''
    IF NOT (KEYWORD_SET(font_small)) THEN font_small = ''
    IF NOT (KEYWORD_SET(edit_fonts)) THEN 				$
    edit_fonts = { font_norm:'', font_input:''}
		
                ;**** create base widget ****

    procid = widget_base(TITLE='ADAS405 PROCESSING OPTIONS', 		$
					XOFFSET=1,YOFFSET=1)

		;**** Declare processing widget ****

    cwid = cw_adas405_proc(procid, dsfull, act,				$
			   symbol, classstring,				$
			   ntdmax, nline, ciion, cmpts, ctitle, bitfile,$
			   PROCVAL=value,				$
		 	   FONT_LARGE=font_large, FONT_SMALL=font_small,$
			   EDIT_FONTS=edit_fonts)

		;**** Realize the new widget ****

    dynlabel, procid
    widget_control, procid, /realize

		;**** Set initial input_focus ****

    widget_control, cwid, set_value=1

		;**** make widget modal ****

    xmanager,'adas405_proc', procid, event_handler='adas405_proc_ev', 	$
	     /modal, /just_reg

		;*** copy value back to procval for return to d5ispf ***

    act = action
    procval = value
 
END
