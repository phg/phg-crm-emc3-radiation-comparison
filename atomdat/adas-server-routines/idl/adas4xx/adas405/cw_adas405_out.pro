; Copyright (c) 1995, Strathclyde University.
; SCCS Info : Module @(#)$Header: /home/adascvs/idl/adas4xx/adas405/cw_adas405_out.pro,v 1.9 2004/07/06 12:46:16 whitefor Exp $ Date $Date: 2004/07/06 12:46:16 $
;+
; PROJECT:
;       ADAS IBM MVS to UNIX conversion
;
; NAME:
;	CW_ADAS405_OUT()
;
; PURPOSE:
;	Produces a widget for ADAS405 output options.
;
; EXPLANATION:
;	This function declares a compound widget consisting of:
;	 - a selection button for graphical output
;	 - an exclusive selection widget to choose output graph
;	 - for each graph a 'select axis limits' button
;	 - for each graph a set of typeins for max and min axis values
;	 - the associated hard copy widget comprising a filename and
;	   list of output devices
;	 - output file widgets cw_adas_outfile for text and passing
;	   file output
;	 - a button for browsing comments in the script file
;	 - a text typein for the user to assign a title to the graph
;	 - buttons for 'Done', 'Cancel' and 'Escape to series menu' 
;	   (this latter is represented by a bitmapped button).
;	This widget only generates events for the 'Done', 'Cancel'
;	and 'Escape to series menu' buttons.
;
; USE:
;	This routine is specific to adas405.
;
; INPUTS:
;	PARENT	- Long integer; ID of parent widget.
;
;	DSFULL	- Name of input dataset for this application.
;
;       LSNULL  - Integer; flag showing whether a null script file has
;                 been selected (=1) or not (=0)
;
;       BITFILE - String; the path to the dirctory containing bitmaps
;                 for the 'escape to series menu' button.
;
; OPTIONAL INPUTS:
;	None.
;
; OUTPUTS:
;	The return value of this function is the ID of the declared widget.
;
; OPTIONAL OUTPUTS:
;	None.
;
; KEYWORD PARAMETERS:
;	DEVLIST	- A string array;  Each element of the array is the
;		  name of a hardcopy graphics output device.  This
;		  list must mirror the DEVCODE list.  DEVLIST is the
;		  list which the user will see.
;		  e.g ['Post-Script','HP-PCL','HP-GL']
;
;	VALUE	- A structure which determines the initial settings of
;		  the output options widget.  The structure has the 
;		  following form:
;
;		      { LPION_AVAIL:		1,			$
;			LPOWER_AVAIL:		0,			$
;			LCONT_AVAIL:		1-lsnull,		$
;			LGRAPHNO:		0,			$
;			XMIN1:			'',			$
;			XMAX1:			'',			$
;			YMIN1:			'',			$
;			YMAX1:			'',			$
;			XMIN2:			'',			$
;			XMAX2:			'',			$
;			YMIN2:			'',			$
;			YMAX2:			'',			$
;			XMIN3:			'',			$
;			XMAX3:			'',			$
;			YMIN3:			'',			$
;			YMAX3:			'',			$
;			LDEF1:			0,			$
;			LDEF2:			0,			$
;			LDEF3:			0,			$
;			GRPOUT:			0,			$
;			GTIT1:			'', 			$
;			HRDOUT:			0,			$
;			HARDNAME:		'',			$
;			GRPDEF:			'',			$
;			GRPFMESS:		'',			$
;			DEVSEL:			-1,			$
;			TEXOUT:			0,			$
;			TEXAPP:			-1,			$
;			TEXREP:			0,			$
;			TEXDSN:			'',			$
;			TEXDEF:			'paper.txt',		$
;			TEXMES:			'',			$
;			GOFOUT:			0,			$
;			GOFAPP:			-1,			$
;			GOFREP:			0,			$
;			GOFDSN:			'',			$
;			GOFDEF:			'goft.pass',		$
;			GOFMES:			''			}
;
;			LPION_AVAIL  Int; flag whether 1st graph available
;			
;			LPOWER_AVAIL Int; flag whether 2nd graph available
;
;			LCONT_AVAIL  Int; flag whether 3rd graph available
;
;			LGRAPHNO     Int; current graph chosen(-1 as IDL index)
;
;			XMINn	     Float; min of x-axis for graph n
;
;			XMAXn	     Float; max of x-axis for graph n
;
;			YMINn	     Float; min of y-axis for graph n
;
;			YMAXn	     Float; max of y-axis for graph n
;
;			LDEFn	     Int; flag whether user has selected
;				     explicit axes for graph n
;
;			GRPOUT	     Int; flag whether graphical output
;				     chosen
;
;			GTIT1	     String; Graph title
;
;			HRDOUT	     Integer; Hard copy activ' 1 on, 0 off
;
;			HARDNAME     String; Hard copy output file name
;
;			GRPDEF	     String; Default output file name 
;
;			GRPFMESS     String; File name error message
;
;			DEVSEL	     Integer; index of selected device in 
;				     DEVLIST
;
;			TEXOUT	     Integer; Activation button 1 on, 0 off
;
;			TEXAPP	     Integer; Append button 1 on, 0 off, 
;				     -1 no button
;
;			TEXREP	     Integer; Replace button 1 on, 0 off, 
;				     -1 no button
;
;			TEXDSN	     String; Output file name
;
;			TEXDEF	     String; Default file name
;
;			TEXMES	     String; file name error message
;
;			GOFOUT	     Integer; Activation button 1 on, 0 off
;				     for passing file
;			
;			GOFAPP       Integer; Append button 1 on, 0 off,
;                                    -1 no button for passing file
;
;			GOFREP       Integer; Replace button 1 on, 0 off,
;                                    -1 no button for passing file
;
;			GOFDSN       String; Output file name for passing
;				     file
;
;			GOFDEF       String; Default file name for passing
;                                    file
;			
;			GOFMES       String; passing file error message
;
;
;	UVALUE	- A user value for this widget.
;
;       FONT_LARGE- Supplies the large font to be used.
;
;       FONT_SMALL- Supplies the small font to be used.
;
; CALLS:
;	CW_ADAS_DSBR	Input dataset name and comments browse button.
;	CW_ADAS_OUTFILE	Output file name entry widget.
;	See side effects for other related widget management routines.
;
; SIDE EFFECTS:
;
; 	Two other routines are included in this file
;	OUT405_GET_VAL()
;	OUT405_EVENT()
;
; CATEGORY:
;	Compound Widget
;
; WRITTEN:
;       Tim Hammond, Tessella Support Services plc, 26-Oct-1995 
;
; MODIFIED:
;	1.1	Tim Hammond	
;		First version
;	1.2	Tim Hammond
;		First complete, working skeleton of the routine
;	1.3	Tim Hammond
;		Included extra functionality for first plot
;	1.4	Tim Hammond
;		Tidied up comments and code
;	1.5	Tim Hammond
;		Added extra check for hard copy device.
;	1.6	Tim Hammond
;		Added flexible font-sizing for different platforms.
;	1.7	William Osborn
;		Removed xsize=n keyword from widget_label command.
;		The dynlabel procedure does not override these
;		assignments and they were taken as being values in pixels
;		on some machines.
;	1.8	William Osborn
;               Added /row keyword to cw_adas_dsbr to shorten screen
;	1.9	Martin O'Mullane
;		Replaced description 'Goft' passing file to 
;               'GCF : G(Te)' which is a bit more informative.  
;
; VERSION:
;	1.1	26-10-95
;	1.2	26-10-95
;	1.3	31-10-95
;	1.4	07-11-95
;	1.5	06-12-95
;	1.6	27-02-96
;	1.7	01-08-96
;	1.8	10-10-96
;	1.9	06-02-01
;
;-
;-----------------------------------------------------------------------------

FUNCTION out405_get_val, id

    COMMON outblock405, font, font_small, ldef1, ldef2, ldef3, 		$
		        oldindex, lpion_avail, lcont_avail, lpower_avail

                ;**** Return to caller on error ****

    ON_ERROR, 2

                ;**** Retrieve the state ****

    parent=widget_info(id, /parent)
    widget_control, parent, get_uvalue=state, /no_copy

		;**** Get graph title ****

    widget_control, state.titleid, get_value=graphtitleval
    graphtitle = graphtitleval(0)

		;**** Get which graph is shown ****

    widget_control, state.grshown, get_value=graphnum

		;**** Get user defined limits ****
	
    widget_control, state.fxminbut, get_value=xmin
    xmin1 = xmin(0)
    widget_control, state.fxmaxbut, get_value=xmax
    xmax1 = xmax(0)
    widget_control, state.fyminbut, get_value=ymin
    ymin1 = ymin(0)
    widget_control, state.fymaxbut, get_value=ymax
    ymax1 = ymax(0)
    widget_control, state.pxminbut, get_value=xmin
    xmin2 = xmin(0)
    widget_control, state.pxmaxbut, get_value=xmax
    xmax2 = xmax(0)
    widget_control, state.pyminbut, get_value=ymin
    ymin2 = ymin(0)
    widget_control, state.pymaxbut, get_value=ymax
    ymax2 = ymax(0)
    widget_control, state.cxminbut, get_value=xmin
    xmin3 = xmin(0)
    widget_control, state.cxmaxbut, get_value=xmax
    xmax3 = xmax(0)
    widget_control, state.cyminbut, get_value=ymin
    ymin3 = ymin(0)
    widget_control, state.cymaxbut, get_value=ymax
    ymax3 = ymax(0)

		;**** Get hard copy settings ****

    widget_control, state.hardid, get_value=hardval
    if hardval.outbut eq 1 then begin
	widget_control, state.devid, get_value=devices
	devindex=devices
    endif else begin
	devindex = oldindex
    endelse

		;**** Get text output settings ****

    widget_control, state.paperid, get_value=papos

		;**** Get goft file settings ****

    widget_control, state.gofid, get_value=gofos

    os = { out405_set, 						$
		LPION_AVAIL	:	lpion_avail,		$
		LPOWER_AVAIL	:	lpower_avail,		$
		LCONT_AVAIL	:	lcont_avail,		$
		LGRAPHNO	:	graphnum,		$
	   	XMIN1		:	xmin1, 			$
		XMAX1		:	xmax1, 	 		$
	   	YMIN1		:	ymin1, 			$
		YMAX1		:	ymax1,   		$
	   	XMIN2		:	xmin2, 			$
		XMAX2		:	xmax2,    		$
	   	YMIN2		:	ymin2, 			$
		YMAX2		:	ymax2, 	 		$
	   	XMIN3		:	xmin3,  		$
		XMAX3		:	xmax3,   		$
	   	YMIN3		:	ymin3,  		$
		YMAX3		:	ymax3, 	 		$
		LDEF1		:	ldef1,			$
		LDEF2		:	ldef2,			$
		LDEF3		:	ldef3,			$
		GRPOUT		:	state.grpout,		$
	   	GTIT1		:	graphtitle,		$
	   	HRDOUT		:	hardval.outbut,		$
		HARDNAME	:	hardval.filename,	$
	   	GRPDEF		:	hardval.defname, 	$
		GRPFMESS	:	hardval.message,   	$
	   	DEVSEL		:	devindex,		$
	   	TEXOUT		:	papos.outbut,  		$
		TEXAPP		:	papos.appbut, 		$
	   	TEXREP		:	papos.repbut,  		$
		TEXDSN		:	papos.filename, 	$
	   	TEXDEF		:	papos.defname, 		$
		TEXMES		:	papos.message,		$
	   	GOFOUT		:	gofos.outbut,  		$
		GOFAPP		:	gofos.appbut, 		$
	   	GOFREP		:	gofos.repbut,  		$
		GOFDSN		:	gofos.filename, 	$
	   	GOFDEF		:	gofos.defname, 		$
		GOFMES		:	gofos.message 		 }

                ;**** Return the state ****

  widget_control, parent, set_uvalue=state, /no_copy

  RETURN, os 

END

;-----------------------------------------------------------------------------

FUNCTION out405_event, event

    COMMON outblock405, font, font_small, ldef1, ldef2, ldef3, 		$
                        oldindex, lpion_avail, lcont_avail, lpower_avail

                ;**** Base ID of compound widget ****

    parent=event.top

                ;**** Retrieve the state ****

    widget_control, parent, get_uvalue=state, /no_copy

		;*********************************
		;**** Clear previous messages ****
		;*********************************

    widget_control, state.messid, set_value=' '

                ;************************
                ;**** Process events ****
                ;************************

    CASE event.id OF

		;***********************
		;**** Cancel button ****
		;***********************

        state.cancelid: begin
            new_event = {ID:parent, TOP:event.top, 			$
		         HANDLER:0L, ACTION:'Cancel'}
        end

		;**** Graph display button ****
	
	state.grshown: begin
	    widget_control, state.grshown, get_value=shown
	    if (shown eq 0) then begin
		widget_control, state.fracbase, map=1
		widget_control, state.powbase, map=0
		widget_control, state.cfbase, map=0
		widget_control, state.hardbase, sensitive=1
	    endif else if (shown eq 1) then begin
		widget_control, state.fracbase, map=0
		widget_control, state.powbase, map=1
		widget_control, state.cfbase, map=0
		widget_control, state.hardid, get_value=hardval
		if lpower_avail eq 0 then begin
		    hardval.outbut = 0
		    widget_control, state.hardid, set_value=hardval
		    widget_control, state.hardbase, sensitive=0
		endif else begin
		    widget_control, state.hardbase, sensitive=1
		endelse
	    endif else begin
		widget_control, state.fracbase, map=0
		widget_control, state.powbase, map=0
		widget_control, state.cfbase, map=1
		widget_control, state.hardid, get_value=hardval
		if lcont_avail eq 0 then begin
		    hardval.outbut = 0
		    widget_control, state.hardid, set_value=hardval
		    widget_control, state.hardbase, sensitive=0
		endif else begin
		    widget_control, state.hardbase, sensitive=1
		endelse
	    endelse
	end

		;**** Do we want graphical output? button ****

	state.grapflag: begin
	    if state.grpout eq 1 then state.grpout=0 else state.grpout=1
	    widget_control, state.plotbase, sensitive=state.grpout
	    widget_control, state.graphbase2, sensitive=state.grpout
	    widget_control, state.hardbase, sensitive=state.grpout
	end

		;**** Explicit scaling for fractional abund. plot ****

	state.fracdef: begin
	    if ldef1 eq 1 then ldef1=0 else ldef1=1
	    widget_control, state.fracbase3b, sensitive=ldef1
	end

		;**** Explicit scaling for power function plot ****

	state.powdef: begin
	    if ldef2 eq 1 then ldef2=0 else ldef2=1
	    widget_control, state.powbase3b, sensitive=ldef2
	end

		;**** Explicit scaling for contribution fn. plot ****

	state.cfdef: begin
	    if ldef3 eq 1 then ldef3=0 else ldef3=1
	    widget_control, state.cfbase3b, sensitive=ldef3
	end

		;**** min/max buttons for fractional abundance plot ****

	state.fxminbut: widget_control, state.fxmaxbut, /input_focus
	state.fxmaxbut: widget_control, state.fyminbut, /input_focus
	state.fyminbut: widget_control, state.fymaxbut, /input_focus
	state.fymaxbut: widget_control, state.fxminbut, /input_focus

		;**** min/max buttons for power function plot ****

	state.pxminbut: widget_control, state.pxmaxbut, /input_focus
	state.pxmaxbut: widget_control, state.pyminbut, /input_focus
	state.pyminbut: widget_control, state.pymaxbut, /input_focus
	state.pymaxbut: widget_control, state.pxminbut, /input_focus

		;**** min/max buttons for contribution function plot ****

	state.cxminbut: widget_control, state.cxmaxbut, /input_focus
	state.cxmaxbut: widget_control, state.cyminbut, /input_focus
	state.cyminbut: widget_control, state.cymaxbut, /input_focus
	state.cymaxbut: widget_control, state.cxminbut, /input_focus

		;*********************
		;**** Done button ****
		;*********************

        state.doneid: begin

		;****************************************************
                ;**** Return the state before checking can start ****
		;**** with the get_value keyword.                ****
		;****************************************************

            widget_control, parent, set_uvalue=state, /no_copy

		;***************************************
		;**** Check for errors in the input ****
		;***************************************

	    error = 0

		;**** Get a copy of the widget value ****

	    widget_control, event.handler, get_value=os
	
		;**** Check for widget error messages ****

     	    if os.grpout eq 1 and strtrim(os.grpfmess) ne '' then error=1
	    if os.texout eq 1 and strtrim(os.texmes) ne '' then error=1
	    if os.gofout eq 1 and strtrim(os.gofmes) ne '' then error=1
	    if error eq 1 then warnmess = '**** Error in output settings ****'

                ;**** Check a device has been selected if ****
                ;**** hard copy requested                 ****

            if error ne 1 and os.grpout eq 1 and os.hrdout eq 1 and     $
            os.devsel eq -1 then begin
                warnmess = '**** Error: You must select an output device ****'
               error = 1
            endif

		;**** Check for errors in scalings if relevant ****

 	    if os.lgraphno eq 0 and os.ldef1 eq 1 and error ne 1 then begin
		serror = 0
		serror = serror + num_chk(os.xmin1, sign=1)
		serror = serror + num_chk(os.xmax1, sign=1)
		serror = serror + num_chk(os.ymin1, sign=1)
		serror = serror + num_chk(os.ymax1, sign=1)
		if serror eq 0 then begin
		    if (float(os.xmin1) ge float(os.xmax1)) or 		$
                    (float(os.ymin1) ge float(os.ymax1))		$
                    then serror = 1
		endif
		if serror gt 0 then begin
		    error = 1
                    warnmess = '**** Error in output settings: Ion '+	$
                               'fraction axis limits are invalid ****'
		endif
	    endif else if os.lgraphno eq 1 and os.ldef2 eq 1 		$
            and error ne 1 then begin
		serror = 0
                serror = serror + num_chk(os.xmin2, sign=1)
                serror = serror + num_chk(os.xmax2, sign=1)
                serror = serror + num_chk(os.ymin2, sign=1)
                serror = serror + num_chk(os.ymax2, sign=1)
                if serror eq 0 then begin
                    if (float(os.xmin2) ge float(os.xmax2)) or 		$
                    (float(os.ymin2) ge float(os.ymax2)) 		$
                    then serror = 1
                endif
                if serror gt 0 then begin
                    error = 1
                    warnmess = '**** Error in output settings: '+	$
                               'Power function axis limits'+		$
                               ' are invalid ****'
                endif
            endif else if os.lgraphno eq 2 and os.ldef3 eq 1            $
            and error ne 1 then begin
                serror = 0
                serror = serror + num_chk(os.xmin3, sign=1)
                serror = serror + num_chk(os.xmax3, sign=1)
                serror = serror + num_chk(os.ymin3, sign=1)
                serror = serror + num_chk(os.ymax3, sign=1)
                if serror eq 0 then begin
                    if (float(os.xmin3) ge float(os.xmax3)) or          $
                    (float(os.ymin3) ge float(os.ymax3))                $
                    then serror = 1
                endif
                if serror gt 0 then begin
                    error = 1
                    warnmess = '**** Error in output settings: '+       $
                               'Contribution function axis limits'+     $
                               ' are invalid ****'
                endif
            endif

                ;**** Retrieve the state   ****

            widget_control, parent, get_uvalue=state, /no_copy

	    if error eq 1 then begin
	        widget_control, state.messid, set_value=warnmess
	        new_event = 0L
	    endif else begin
	        new_event = {ID:parent, TOP:event.top, HANDLER:0L, 	$
                             ACTION:'Done'}
	    endelse

        end

		;**** Menu button ****

	state.outid: begin
	    new_event = {ID:parent, TOP:event.top, HANDLER:0L,      	$
                         ACTION:'Menu'}
	end

        ELSE: begin
            new_event = 0L
          end

    ENDCASE

                ;**** Return the state   ****

    widget_control, parent, set_uvalue=state, /no_copy

    RETURN, new_event

END

;-----------------------------------------------------------------------------

FUNCTION cw_adas405_out, parent, dsfull, lsnull, DEVLIST=devlist,  	$
		         VALUE=value, UVALUE=uvalue, FONT_LARGE=font, 	$
			 FONT_SMALL=font_small, bitfile

    COMMON outblock405, fontcom, fontsmallcom, ldef1com, ldef2com,	$
                        ldef3com, oldindex, lpion_availcom, 		$
		        lcont_availcom, lpower_availcom


    IF (N_PARAMS() EQ 0) THEN MESSAGE, 'Must specify parent for cw_adas405_out'
    ON_ERROR, 2					;return to caller

		;**** Set defaults for keywords ****

    IF NOT (KEYWORD_SET(devlist)) THEN devlist = ''
    IF NOT (KEYWORD_SET(uvalue)) THEN uvalue = 0
    IF NOT (KEYWORD_SET(font)) THEN font = ''
    IF NOT (KEYWORD_SET(font_small)) THEN font_small = ''
    IF NOT (KEYWORD_SET(value)) THEN begin
	os = {out405_set, 						$
                        LPION_AVAIL     :       1,                      $
                        LPOWER_AVAIL    :       0,                      $
                        LCONT_AVAIL     :       1-lsnull,               $
                        LGRAPHNO        :       0,                      $
                        XMIN1           :       '',                     $
                        XMAX1           :       '',                     $
                        YMIN1           :       '',                     $
                        YMAX1           :       '',                     $
                        XMIN2           :       '',                     $
                        XMAX2           :       '',                     $
                        YMIN2           :       '',                     $
                        YMAX2           :       '',                     $
                        XMIN3           :       '',                     $
                        XMAX3           :       '',                     $
                        YMIN3           :       '',                     $
                        YMAX3           :       '',                     $
			LDEF1		:	0,			$
			LDEF2		:	0,			$
			LDEF3		:	0,			$
			GRPOUT		:	0,			$
			GTIT1		:	'', 			$
			HRDOUT		:	0, 			$
			HARDNAME	:	'', 			$
			GRPDEF		:	'', 			$
			GRPFMESS	:	'',		 	$
			DEVSEL		:	-1, 			$
			TEXOUT		:	0, 			$
			TEXAPP		:	-1,			$
			TEXREP		:	0, 			$
			TEXDSN		:	'', 			$
        		TEXDEF		:	'paper.txt',		$
			TEXMES		:	'',			$
			GOFOUT		:	0, 			$
			GOFAPP		:	-1,			$
			GOFREP		:	0, 			$
			GOFDSN		:	'',			$
			GOFDEF		:	'goft.pass', 		$
			GOFMES		:	'' }
    ENDIF ELSE BEGIN
	os = {out405_set, 						$
                        LPION_AVAIL     :       value.lpion_avail,       $
                        LPOWER_AVAIL    :       value.lpower_avail,     $
                        LCONT_AVAIL     :       value.lcont_avail,      $
                        LGRAPHNO        :       value.lgraphno,         $
                        XMIN1           :       value.xmin1,            $
                        XMAX1           :       value.xmax1,            $
                        YMIN1           :       value.ymin1,            $
                        YMAX1           :       value.ymax1,            $
                        XMIN2           :       value.xmin2,            $
                        XMAX2           :       value.xmax2,            $
                        YMIN2           :       value.ymin2,            $
                        YMAX2           :       value.ymax2,            $
                        XMIN3           :       value.xmin3,            $
                        XMAX3           :       value.xmax3,            $
                        YMIN3           :       value.ymin3,            $
                        YMAX3           :       value.ymax3,            $
			LDEF1		:	value.ldef1,		$
			LDEF2		:	value.ldef2,		$
			LDEF3		:	value.ldef3,		$
			GRPOUT		:	value.grpout,		$
			GTIT1		:	value.gtit1, 		$
			HRDOUT		:	value.hrdout, 		$
			HARDNAME	:	value.hardname, 	$
			GRPDEF		:	value.grpdef, 		$
			GRPFMESS	:	value.grpfmess,		$
			DEVSEL		:	value.devsel, 		$
			TEXOUT		:	value.texout, 		$
			TEXAPP		:	value.texapp, 		$
			TEXREP		:	value.texrep, 		$
			TEXDSN		:	value.texdsn, 		$
			TEXDEF		:	value.texdef, 		$
			TEXMES		:	value.texmes,		$
			GOFOUT		:	value.gofout, 		$
			GOFAPP		:	value.gofapp,		$
			GOFREP		:	value.gofrep, 		$
			GOFDSN		:	value.gofdsn,		$
			GOFDEF		:	value.gofdef, 		$
			GOFMES		:	value.gofmes }
    ENDELSE

    fontsmallcom = font_small
    ldef1com = os.ldef1
    ldef2com = os.ldef2
    ldef3com = os.ldef3
    oldindex = os.devsel
    lpion_availcom = os.lpion_avail
    lcont_availcom = os.lcont_avail
    lpower_availcom = os.lpower_avail

		;**** Add flexible font-sizing ****

    machine = GETENV('TARGET_MACHINE')
    if machine eq 'HPUX' then begin
	large_font = font_small
    endif else begin
	large_font = font
    endelse
    fontcom = large_font

		;**********************************************
		;**** Create the 405 Output options widget ****
		;**********************************************

		;**** create base widget ****

    cwid = widget_base( parent, UVALUE = uvalue, 			$
			EVENT_FUNC = "out405_event", 			$
			FUNC_GET_VALUE = "out405_get_val", 		$
			/COLUMN)

		;**** Add dataset name and browse button ****

    small_check = GETENV('VERY_SMALL')		;if monitor very small omit dataset
    if small_check ne 'YES' then begin		;name and browse button
        rc = cw_adas_dsbr(cwid, dsfull, font=font_small, 			$
		          filelabel='Script file: ',/row)
    endif

		;**** Main graphics base ****

    graphbase = widget_base(cwid, /column, /frame)

		;**** Button to activate graphing ****

    graphbase1 = widget_base(graphbase, /row, /nonexclusive)
    grapflag = widget_button(graphbase1, value='Graphical output',	$
			     font=large_font)
    widget_control, grapflag, set_button=os.grpout

                ;**** Graph title ****

    titlebase = widget_base(graphbase, /row)
    rc = widget_label(titlebase, value='Graph Title', font=font_small)
    titleid = widget_text(titlebase, value=os.gtit1, font=font_small,	$
			  /editable, xsize=30)

		;**** The 3 graphics options ****

    graphbase2 = widget_base(graphbase, /column, /frame)
    groptions = ['Fractional abundance plot',				$
                 'Power function plot',					$
                 'Contribution function plot']
    grshown = cw_bgroup(graphbase2, groptions, font=font_small,		$
                        set_value=os.lgraphno, /exclusive)
    widget_control, graphbase2, sensitive=os.grpout

		;**** Base for all plots ****

    plotbase = widget_base(graphbase, /frame)

		;**** Ion Fractional abundance plot base ****

    if (os.lgraphno eq 0) then frmap=1 else frmap=0
    fracbase = widget_base(plotbase, /column, map=frmap)
    frachead = widget_label(fracbase, font=font_small,			$
			    value='Fractional abundance plot:-')
    fracbase3 = widget_base(fracbase, /row)
    fracbase3a = widget_base(fracbase3, /row, /nonexclusive)
    fracdef = widget_button(fracbase3a, value='Explicit scaling  ',	$
			    font=font_small)
    widget_control, fracdef, set_button=os.ldef1
    fracbase3b = widget_base(fracbase3, /column, /frame)
    fracbase3c = widget_base(fracbase3b, /row)
    fracxmin = widget_label(fracbase3c, font=font_small, value='X-min: ')
    fxminbut = widget_text(fracbase3c, font=font_small, value=os.xmin1,	$
		           xsize=8, /editable)
    fracxmax = widget_label(fracbase3c, font=font_small, value='X-max: ')
    fxmaxbut = widget_text(fracbase3c, font=font_small, value=os.xmax1,	$
		           xsize=8, /editable)
    fracbase3d = widget_base(fracbase3b, /row)
    fracxmin = widget_label(fracbase3d, font=font_small, value='Y-min: ')
    fyminbut = widget_text(fracbase3d, font=font_small, value=os.ymin1,	$
		           xsize=8, /editable)
    fracxmax = widget_label(fracbase3d, font=font_small, value='Y-max: ')
    fymaxbut = widget_text(fracbase3d, font=font_small, value=os.ymax1,	$
		           xsize=8, /editable)
    widget_control, fracbase, sensitive=os.lpion_avail
    widget_control, fracbase3b, sensitive=os.ldef1
    

		;**** Power function plot base ****

    if (os.lgraphno eq 1) then pomap=1 else pomap=0
    powbase = widget_base(plotbase, /column, map=pomap)
    powhead = widget_label(powbase, font=font_small,			$
		           value='Power function plot:-')
    powbase3 = widget_base(powbase, /row)
    powbase3a = widget_base(powbase3, /row, /nonexclusive)
    powdef = widget_button(powbase3a, value='Explicit scaling  ',       $
                           font=font_small)
    widget_control, powdef, set_button=os.ldef2
    powbase3b = widget_base(powbase3, /column, /frame)
    powbase3c = widget_base(powbase3b, /row)
    powxmin = widget_label(powbase3c, font=font_small, value='X-min: ')
    pxminbut = widget_text(powbase3c, font=font_small, value=os.xmin2,	$
		           xsize=8, /editable)
    powxmax = widget_label(powbase3c, font=font_small, value='X-max: ')
    pxmaxbut = widget_text(powbase3c, font=font_small, value=os.xmax2,	$
		           xsize=8, /editable)
    powbase3d = widget_base(powbase3b, /row)
    powymin = widget_label(powbase3d, font=font_small, value='Y-min: ')
    pyminbut = widget_text(powbase3d, font=font_small, value=os.ymin2,	$
		           xsize=8, /editable)
    powymax = widget_label(powbase3d, font=font_small, value='Y-max: ')
    pymaxbut = widget_text(powbase3d, font=font_small, value=os.ymax2,	$
		           xsize=8, /editable)
    widget_control, powbase, sensitive=os.lpower_avail
    widget_control, powbase3b, sensitive=os.ldef2

		;**** Contribution function plot ****

    if (os.lgraphno eq 2) then cfmap=1 else cfmap=0
    cfbase = widget_base(plotbase, /column, map=cfmap)
    cfhead = widget_label(cfbase, font=font_small,			$
			  value='Contribution function plot:-')
    cfbase3 = widget_base(cfbase, /row)
    cfbase3a = widget_base(cfbase3, /row, /nonexclusive)
    cfdef = widget_button(cfbase3a, value='Explicit scaling  ',         $
                           font=font_small)
    widget_control, cfdef, set_button=os.ldef3
    cfbase3b = widget_base(cfbase3, /column, /frame)
    cfbase3c = widget_base(cfbase3b, /row)
    cfxmin = widget_label(cfbase3c, font=font_small, value='X-min: ')
    cxminbut = widget_text(cfbase3c, font=font_small, value=os.xmin3,	$
		           xsize=8, /editable)
    cfxmax = widget_label(cfbase3c, font=font_small, value='X-max: ')
    cxmaxbut = widget_text(cfbase3c, font=font_small, value=os.xmax3,	$
		           xsize=8, /editable)
    cfbase3d = widget_base(cfbase3b, /row)
    cfymin = widget_label(cfbase3d, font=font_small, value='Y-min: ')
    cyminbut = widget_text(cfbase3d, font=font_small, value=os.ymin3,	$
		           xsize=8, /editable)
    cfymax = widget_label(cfbase3d, font=font_small, value='Y-max: ')
    cymaxbut = widget_text(cfbase3d, font=font_small, value=os.ymax3,	$
		           xsize=8, /editable)
    widget_control, cfbase, sensitive=os.lcont_avail
    widget_control, cfbase3b, sensitive=os.ldef3


		;**** Base for hard-copy widgets ****

    if os.hrdout ge 0 and strtrim(devlist(0)) ne '' then begin
        hardbase = widget_base(graphbase, /row, /frame)
        outfval = { OUTBUT:os.hrdout, APPBUT:-1, REPBUT:0,		$
                    FILENAME:os.hardname, DEFNAME:os.grpdef,		$
                    MESSAGE:os.grpfmess }
        hardid = cw_adas_outfile(hardbase, OUTPUT='Enable Hard Copy',	$
				 VALUE=outfval, FONT=font_small)
        hardbase2 = widget_base(hardbase, /column)
        devid = cw_single_sel(hardbase, devlist, title='Select Device',	$
                              value=os.devsel, font=font_small)
    endif else begin
	devid = 0L
	hardid = 0L
	hardbase = 0L
	hardbase2 = 0L
    endelse

		;**** Set the overall sensitivity of the bases ****

    widget_control, plotbase, sensitive=os.grpout
    if os.grpout eq 0 then begin
	sensflag=0
    endif else begin
	if os.lgraphno eq 0 then begin
	    sensflag = 1
	endif else if os.lgraphno eq 1 then begin
	    sensflag = os.lpower_avail
	endif else begin
	    sensflag = os.lcont_avail
	endelse
    endelse
    widget_control, hardbase, sensitive=sensflag

		;**** Widget for text output ****

    outfval = { OUTBUT:os.texout, APPBUT:-1, REPBUT:os.texrep, 		$
	        FILENAME:os.texdsn, DEFNAME:os.texdef, MESSAGE:os.texmes }
    base = widget_base(cwid,/row,/frame)
    paperid = cw_adas_outfile(base, OUTPUT='Text Output', 		$
                              VALUE=outfval, FONT=large_font)

                ;**** Widget for goft passing file ****

    outfval = { OUTBUT:os.gofout, APPBUT:os.gofapp, REPBUT:os.gofrep, 	$
                FILENAME:os.gofdsn, DEFNAME:os.gofdef, MESSAGE:os.gofmes }
    base = widget_base(cwid,/row,/frame)
    widget_control, base, sensitive=(1-lsnull)
    gofid = cw_adas_outfile(base, OUTPUT='GCF : G(Te) Passing File',		$
                              VALUE=outfval, FONT=large_font)

		;**** Error message ****

    messid = widget_label(cwid,value=' ', font=large_font)

		;**** add the exit buttons ****

    base = widget_base(cwid, /row)
    menufile = bitfile + '/menu.bmp'
    read_X11_bitmap, menufile, bitmap1
    outid = widget_button(base, value=bitmap1)		;menu button
    cancelid = widget_button(base, value='Cancel', font=large_font)
    doneid = widget_button(base, value='Done', font=large_font)

		;*************************************************
		;**** create a state structure for the pop-up ****
		;**** window.                                 ****
		;*************************************************

    new_state = { 	PAPERID		:	paperid,		$
		  	CANCELID	:	cancelid, 		$
			DONEID		:	doneid, 		$
			MESSID		:	messid,			$
			TITLEID		:	titleid,		$
			OUTID		:	outid,			$
			GRSHOWN		:	grshown,		$
			CFBASE		:	cfbase,			$
			POWBASE		:	powbase,		$
			FRACBASE	:	fracbase,		$
			GRAPFLAG	:	grapflag,		$
			PLOTBASE	:	plotbase,		$
			GRAPHBASE2	:	graphbase2,		$
			FRACDEF		:	fracdef,		$
			FRACBASE3B	:	fracbase3b,		$
			FYMINBUT	:	fyminbut,		$
			FYMAXBUT	:	fymaxbut,		$
			FXMINBUT	:	fxminbut,		$
			FXMAXBUT	:	fxmaxbut,		$
			POWDEF		:	powdef,			$
			POWBASE3B	:	powbase3b,		$
			PYMINBUT	:	pyminbut,		$
			PYMAXBUT	:	pymaxbut,		$
			PXMINBUT	:	pxminbut,		$
			PXMAXBUT	:	pxmaxbut,		$
			CFDEF		:	cfdef,			$
			CFBASE3B	:	cfbase3b,		$
			CYMINBUT	:	cyminbut,		$
			CYMAXBUT	:	cymaxbut,		$
			CXMINBUT	:	cxminbut,		$
			CXMAXBUT	:	cxmaxbut,		$
			HARDBASE	:	hardbase,		$
			HARDBASE2	:	hardbase2,		$
			HARDID		:	hardid,			$
			GRPOUT		:	os.grpout,		$
			DEVID		:	devid,			$
			GOFID		:	gofid			}

                ;**** Save initial state structure ****

    widget_control, parent, set_uvalue=new_state, /no_copy

    RETURN, cwid

END

