; Copyright (c) 1995 Strathclyde University .
; SCCS info: Module @(#)$Header: /home/adascvs/idl/adas4xx/adas405/d5spf0.pro,v 1.6 2004/07/06 13:17:39 whitefor Exp $ Date $Date: 2004/07/06 13:17:39 $
;+
; PROJECT:
;       ADAS IBM MVS to UNIX conversion
;
; NAME:
;       D5SPF0
;
; PURPOSE:
;       IDL user interface and communications with ADAS405 FORTRAN
;       process via pipe.
;
; EXPLANATION:
;       Firstly this routine invokes the part of the user interface
;       used to select the inputs for ADAS405. When the user's
;       interactions are complete this routine communicates with the
;       ADAS405 FORTRAN application via a UNIX pipe.  Communications
;       are to the FORTRAN subroutine D5SPF0.
;
; USE:
;       The use of this routine is specific to ADAS405, see adas405.pro.
;
; INPUTS:
;       PIPE    - The IDL unit number of the bi-directional pipe to the
;                 ADAS405 FORTRAN process.
;
;       INVAL	- A structure which determines the initial settings of
;                 the input screen widgets.
;                 INVAL is passed un-modified through to cw_adas405_in.pro,
;                 see that routine for a full description.
;
;       CLASSES - String array: the names of the isonuclear master classes
;
; OPTIONAL INPUTS:
;       None
;
; OUTPUTS:
;       INVAL	- On output the structure records the final settings of
;                 the input screen widgets if the user pressed the
;                 'Done' button, otherwise it is not changed from input.
;
;       REP     - String; Indicates whether the user pressed the 'Done'
;                 or 'Cancel' button on the interface.  The action is
;                 converted to the strings 'NO' and 'YES' respectively
;                 to match up with the existing FORTRAN code.  In the
;                 original IBM ISPF interface REP was the reply to the
;                 question 'DO YOU WANT TO END PROGRAM EXECUTION'.
;
;	LSNULL	- Integer; a flag indicating whether a null script file 
;		  has been used (=1) or not (=0)
;
; OPTIONAL OUTPUTS:
;       None
;
; KEYWORD PARAMETERS:
;       FONT_LARGE	- Supplies the large font to be used for the 
;			  interface widgets.
;
;	FONT_SMALL	- Supplies the small font to be used for the 
;			  interface widgets.
; CALLS:
;       ADAS405_IN   	- Pops-up the input selections widget.
;
; SIDE EFFECTS:
;       This routine communicates with the ADAS405 FORTRAN process
;       via a UNIX pipe.
;
; CATEGORY:
;       Adas system.
;
; WRITTEN:
;       Tim Hammond, Tessella Support Services plc, 7th November 1995
;
; MODIFIED:
;       1.1     Tim Hammond
;		First release
;       1.2     Tim Hammond
;		Added compression of typein strings during
;		construction of filenames - this avoids filenames
;		of form ..acd93()/()acd93()_c.dat for example.
;	1.3	Tim Hammond
;		Added further compression when contructing filenames
;               to avoid having names with spaces in them. Also
;		put in a test to see which fileavailable flags
;		should be passed to the FORTRAN (depending on the
;		directory branch chosen by the user).
;	1.4	Tim Hammond
;		Removed test to see which filenames are to be passed
;		to FORTRAN as other codes should now ensure that
;		for the user's files the correct pathnames are
;		sent (User or Central) depending on what has been
;		selected by the user.
;	1.5	Tim Hammond
;		Added compression of the element symbol string to
;	1.6     Martin O'Mullane
;	        Removed the absurd method of handling filtered data. The search
;               is now limited to the power data (prb, plt and prc) and the
;               format is now, for example, /plt89/plt89_ni.ft25003.dat and not
;               /plt89(ft25003)/plt89_ni.dat as it was.
;		avoid errors in the FORTRAN.
;
; VERSION:
;       1.1     07-11-95
;       1.2     09-11-95
;	1.3	13-11-95
;	1.4	14-11-95
;	1.5	19-01-96
;	1.6 	13-05-98
;
;-----------------------------------------------------------------------------
;-

PRO d5spf0, pipe, inval, classes, rep, lsnull,				$
            FONT_LARGE=font_large, FONT_SMALL=font_small


                ;**** Set defaults for keywords ****

    IF NOT (KEYWORD_SET(font_large)) THEN font_large = ''
    IF NOT (KEYWORD_SET(font_small)) THEN font_small = ''

                ;**********************************
                ;**** Pop-up input file widget ****
                ;**********************************

    adas405_in, inval, classes, action, WINTITLE = 'ADAS 405 INPUT',	$
		FONT_LARGE=font_large, FONT_SMALL=font_small

                ;********************************************
                ;**** Act on the event from the widget   ****
                ;********************************************
                ;**** There are only two possible events ****
                ;**** 'Done' and 'Cancel'.               ****
                ;********************************************

    if action eq 'Done' then begin
        rep = 'NO'
    endif else begin
        rep = 'YES'
    endelse
    printf, pipe, rep

    if rep eq 'NO' then begin

                ;*******************************
                ;**** Write data to fortran ****
                ;*******************************

	printf, pipe, inval.year
	printf, pipe, inval.dyear
        outsymbol = strcompress(strupcase(inval.symbol), /remove_all)
	printf, pipe, outsymbol

		;**** Write script file name ****

	scriptname = inval.scrootpath + inval.scfile
        printf, pipe, scriptname
	
		;**** Write filter name ****

	printf, pipe, strcompress(inval.filtr, /remove_all)

		;**** Write filenames ****

		;**** First construct data file names ****

	filenames = strarr(8)
	defnames = strarr(8)
	dirnames = ['acd','scd','ccd','prb','prc','qcd','xcd','plt']
	if inval.branch eq 0 then begin
	    filenames(*) = inval.userroot 
	endif else begin
	    filenames(*) = inval.centroot
	endelse
	defnames(*) = inval.centroot 
	defdirnames = dirnames + strcompress(inval.dyear, /remove_all)
	dirnames = dirnames + strcompress(inval.year, /remove_all)
	if (inval.filetype eq 1) then begin	;partial files
	    if (inval.parttype eq 0) then begin
		dirnames = dirnames + 'r'
		defdirnames = defdirnames + 'r'
	    endif else begin
		dirnames = dirnames + 'u'
		defdirnames = defdirnames + 'u'
	    endelse
	endif
        
        
        filenames = filenames + dirnames  + "/"
	defnames = defnames + defdirnames + "/"
	if (strcompress(inval.member, /remove_all) ne '') then begin
	    filenames = filenames + 				$
	    strcompress(inval.member, /remove_all) + '#'
	    defnames = defnames +  				$
	    strcompress(inval.member, /remove_all) + '#'
	endif

	if inval.filtr ne '' then begin
        
           filterstring = strcompress(inval.filtr, /remove_all)
           filteradd = ['','','','.'+filterstring,'.'+filterstring,  $
                        '','','.'+filterstring]
           filenames = filenames + dirnames + "_" + 			$
           strcompress(inval.symbol, /remove_all) + filteradd +".dat"
	   defnames = defnames + defdirnames + "_" + 			$
           strcompress(inval.symbol, /remove_all) + filteradd + ".dat"
           
	endif else begin
        
           filenames = filenames + dirnames + "_" + 			$
           strcompress(inval.symbol, /remove_all) + ".dat"
	   defnames = defnames + defdirnames + "_" + 			$
           strcompress(inval.symbol, /remove_all) + ".dat"
           
        endelse

		;********************************************************
		;**** Now overwrite user filenames which are missing ****
		;**** with default filenames (whether they exist or  ****
		;**** not)                                           ****
		;********************************************************

	wherezero = where(inval.fileavailable eq 0)
	if (wherezero(0) ne -1) then begin
	    filenames(wherezero) = defnames(wherezero)
	endif

		;**** Write filenames to FORTRAN ****

	for i=0,7 do begin
	    printf, pipe, filenames(i)
	endfor

		;**** Write file availability flags ****

	for i=0,7 do begin
            printf, pipe, inval.fileavailable(i)
	endfor

		;**** Write default file availability flags ****

	for i=0,7 do begin
            printf, pipe, inval.defavailable(i)
        endfor

		;**** Write file selection flags ****

        for i=0,7 do begin
            printf, pipe, inval.indices(i)
        endfor

		;**** Write whether partial or standard ****

	printf, pipe, inval.filetype

		;**** Read in whether a null script file or not ****

        idum = 0
	readf, pipe, idum
	lsnull = idum

    endif


END
