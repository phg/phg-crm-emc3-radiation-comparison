; Copyright (c) 1995, Strathclyde University.
; SCCS info: Module @(#)$Header: /home/adascvs/idl/adas4xx/adas405/adas405_2_plot.pro,v 1.5 2004/07/06 10:42:05 whitefor Exp $ Date $Date: 2004/07/06 10:42:05 $
;+
; PROJECT:
;       ADAS IBM MVS to UNIX conversion
;
; NAME:
;       ADAS405_2_PLOT
;
; PURPOSE:
;       Generates ADAS405 power function graphical output.
;
; EXPLANATION:
;       This routine creates a window for the display of graphical
;       output. A separate routine PLOT405_2 actually plots the
;       graph.
;
; USE:
;       This routine is specific to the power function plotting section
;	of ADAS405.
;
; INPUTS:
;       DSFULL  - String; Name of data file
;
;       UTITLE  - String; Optional comment by user added to graph title.
;
;       LDEF2   - Integer; 1 - use user entered graph scales
;                          0 - use default axes scaling
;
;	NMSUM	- Integer; no. of metastables (output curves)
;
;	X	- Fltarr; electron temperatures - x-values
;
;	Y	- 2D Fltarr; power functions - y-values
;
;       HRDOUT - Integer; 1 if hardcopy output activated, 0 if not.
;
;       HARDNAME- String; Filename for harcopy output.
;
;       DEVICE  - String; IDL name of hardcopy output device.
;
;       HEADER  - String; ADAS version number header to include in graph.
;
;	POPTIT	- String array; the metastable designations (used as label)
;
;	SPECIES - String; element name to be used in graph title
;
;	YEAR	- String; user's selected year for data
;
;	DYEAR	- String; user's selected default year for data (if any)
;
;       XMIN2   - String; Lower limit for x-axis of graph, number as string.
;
;       XMAX2   - String; Upper limit for x-axis of graph, number as string.
;
;       YMIN2   - String; Lower limit for y-axis of graph, number as string.
;
;       YMAX2   - String; Upper limit for y-axis of graph, number as string.
;
;	TOT	- 2D Fltarr; Total power functions (PRB,PRC,PLT,TOTAL),
;		  plotted as y-values for cases where the relevant power
;		  function data is available.
;
;       BITFILE - String; the path to the directory containing bitmaps
;                 for the 'escape to series menu' button.
;
; OPTIONAL INPUTS:
;       None.
;
; OUTPUTS:
;       GOMENU  - Int; flag - set to 1 if user has selected 'escape direct
;                 to series menu', otherwise 0
;
; OPTIONAL OUTPUTS:
;       None.
;
; KEYWORD PARAMETERS:
;       FONT    - String; The name of a font to use for text in the
;                 graphical output widget.
;
; CALLS:
;       CW_ADAS_GRAPH   Graphical output widget.
;       PLOT405_2       Make plots to an output device for 405(power fn).
;       XMANAGER
;
; SIDE EFFECTS:
;       This routine uses a common block to maintain its state 
;	PLOT405_2_BLK.
;
;       One other routine is included in this file;
;       ADAS405_2_PLOT_EV Called via XMANAGER during widget management.
;
; CATEGORY:
;       Adas system.
;
; WRITTEN:
;       Tim Hammond, Tessella Support Services plc,  8th November 1995
;
; MODIFIED:
;       1.1     Tim Hammond
;               First version
;	1.2	Tim Hammond
;		Corrected minor spelling error in common block
;	1.3	Tim Hammond
;		Corrected way number of plots is calculated - previously
;               was error if there was an exact multiple of 7 lines in total.
;	1.4	Tim Hammond
;		Corrected minor syntax error.
;
; VERSION:
;       1.1     08-11-95
;	1.2	08-11-95
;	1.3	22-11-95
;	1.4	22-11-95
;
;-
;----------------------------------------------------------------------------

PRO adas405_2_plot_ev, event

    COMMON plot405_2_blk, action, plotdev, plotfile, fileopen, win, 	$
                          iplot, nplot, data, nsum, gomenu

    newplot = 0
    print = 0
    done = 0
                ;****************************************
                ;**** Set graph and device requested ****
                ;****************************************

    CASE event.action OF

        'previous' : begin
            if iplot gt 0 then begin
                newplot = 1
                iplot = iplot - 1
                first = iplot
                last = iplot
            endif
	end

        'next'     : begin
            if iplot lt nplot-1 then begin
                newplot = 1
                iplot = iplot + 1
                first = iplot
                last = iplot
            endif
        end

        'print'    : begin
            newplot = 1
            print = 1
	    first = iplot
	    last = iplot
        end

        'printall' : begin
            newplot = 1
            print = 1
            first = 0
            last = nplot-1
        end

        'done'     : begin
            if fileopen eq 1 then begin
                set_plot, plotdev
                device, /close_file
            end
            set_plot,'X'
            widget_control, event.top, /destroy
            done = 1
        end

        'bitbutton' : begin
            if fileopen eq 1 then begin
                set_plot, plotdev
                device, /close_file
            end
            set_plot,'X'
            widget_control, event.top, /destroy
            done = 1
            gomenu = 1
        end
             
    END

                ;*******************************
                ;**** Make requested plot/s ****
                ;*******************************

    if done eq 0 then begin

                ;**** Set graphics device ****

        if print eq 1 then begin
            set_plot, plotdev
            if fileopen eq 0 then begin
                fileopen = 1
                device, filename=plotfile
                device, /landscape
            endif
        endif else begin
            set_plot,'X'
            wset, win
        endelse

                ;**** Draw graphics ****

        if newplot eq 1 then begin
            for i = first, last do begin
	        bottom = i * 7
	        top = bottom + 6
                if top gt (nsum-1) then top=nsum-1
                plot405_2, data.ldef2, top, bottom, data.x, data.y,	$
                           data.rightstring, data.rightstring2, 	$
			   data.title, data.xmin2, data.xmax2, 		$
			   data.ymin2, data.ymax2, data.tot
	        if print eq 1 then begin
                    message = 'Plot  written to print file.'
                    grval = {WIN:0, MESSAGE:message}
                    widget_control, event.id, set_value=grval
                endif
	    endfor
        endif
    endif

END

;----------------------------------------------------------------------------
                                                                
PRO adas405_2_plot, dsfull, utitle, ldef2, nmsum, x, y,			$
                    hrdout, hardname, device, header, poptit, 		$
                    species, year, dyear, xmin2, xmax2, ymin2, ymax2,	$
                    tot, bitfile, gomenu, FONT=font

    COMMON plot405_2_blk, action, plotdev, plotfile, fileopen, win, 	$
                          iplot, nplot, data, nsum, gomenucom

    ngpic = 7			;max no. of lines per plot

                ;**** Copy input values to common ****

    plotdev = device
    plotfile = hardname
    fileopen = 0
    nsum = nmsum
    gomenucom = gomenu

		;*********************************************
		;**** Generate indices to be sent to plot ****
		;**** routine if there are to be multiple ****
		;**** plots drawn			  ****
		;*********************************************

    nplot = 0
    if nmsum le ngpic then begin
	bottom = 0
	top = (nmsum-1)
	nplot = 1
    endif else begin
	bottom = 0
	top = (ngpic-1)
        if nmsum mod ngpic ne 0 then begin
            nplot = fix(nmsum/ngpic) + 1
        endif else begin
            nplot = fix(nmsum/ngpic)
        endelse
    endelse

                ;************************************
                ;**** Create general graph titles****
                ;************************************

    title = "POWER FUNCTION VS. ELECTRON TEMPERATURE"
    if ( strtrim(strcompress(utitle),2)  ne ' ' ) then begin
        title = title + ': ' + strupcase(strtrim(utitle,2))
    endif
    small_check = GETENV('VERY_SMALL')
    if small_check eq 'YES' then begin
      title =  title + '!C!CADAS    : ' + strupcase(header)
      if strcompress(dyear, /remove_all) eq '' then defyear = 'NONE' else $
      defyear = strcompress(dyear, /remove_all)
      year = strcompress(year, /remove_all)
      title =  title + '!C!CFILE     : ' + strcompress(dsfull) + 		$
      '   SPECIES: ' + strupcase(species) + '   YEAR: ' + year +		$
      '   DEFAULT YEAR: '+ defyear
      title =  title + '!C!CKEY     : (FULL LINE - TOTAL) (DASH LINE - PARTIAL)'
    endif else begin
      title =  title + '!C!CADAS    : ' + strupcase(header)
      if strcompress(dyear, /remove_all) eq '' then defyear = 'NONE' else $
      defyear = strcompress(dyear, /remove_all)
      year = strcompress(year, /remove_all)
      title =  title + '!CFILE     : ' + strcompress(dsfull) + 		$
      '   SPECIES: ' + strupcase(species) + '   YEAR: ' + year +		$
      '   DEFAULT YEAR: '+ defyear
      title =  title + '!CKEY     : (FULL LINE - TOTAL) (DASH LINE - PARTIAL)'
    endelse

    rightstring = ''
    rightstring2 = ''
    for i=0, (nmsum-1) do begin
        if (i+1) lt 10 then rightstring = rightstring + ' '
        rightstring = rightstring + strtrim(string(i+1),2) + '!C'
        rightstring2 = rightstring2 + strupcase(poptit(i)) + '!C'
  	if small_check eq 'YES' then begin
	    rightstring = rightstring + '!C'
	    rightstring2 = rightstring2 + '!C'
	endif
    endfor

                ;*************************************
                ;**** Create graph display widget ****
                ;*************************************

    graphid = widget_base(TITLE='ADAS405 GRAPHICAL OUTPUT', 		$
                          XOFFSET=1,YOFFSET=1)
    device, get_screen_size = scrsz
    xwidth=scrsz(0)*0.75
    yheight=scrsz(1)*0.75
    if nplot gt 1 then multiplot=1 else multiplot=0
    bitval = bitfile + '/menu.bmp'
    cwid = cw_adas_graph(graphid, print=hrdout, FONT=font,		$
                         xsize=xwidth, ysize=yheight, 			$
		         multiplot=multiplot, bitbutton=bitval)

                ;**** Realize the new widget ****

    widget_control, graphid, /realize

                ;**** Get the id of the graphics area ****

    widget_control, cwid, get_value=grval
    win = grval.win

                ;*******************************************
                ;**** Put the graphing data into common ****
                ;*******************************************

    data = {	X		:	x,				$
		Y		:	y,				$
		TOT  		:	tot,				$
		XMIN2		:	xmin2,				$
		XMAX2		:	xmax2,				$
		YMIN2		:	ymin2,				$
		YMAX2		:	ymax2,				$
		RIGHTSTRING	:	rightstring,			$
		RIGHTSTRING2	:	rightstring2,			$
		TITLE		:	title,				$
		LDEF2		:	ldef2				}

                ;**** Initialise to plot 0 ****
    iplot = 0
    wset, win

    plot405_2, ldef2, top, bottom, x, y, rightstring, rightstring2, 	$
               title, xmin2, xmax2, ymin2, ymax2, tot

                ;***************************
                ;**** make widget modal ****
                ;***************************

    xmanager, 'adas405_2_plot', graphid, /modal, /just_reg,		$
              event_handler='adas405_2_plot_ev'

    gomenu = gomenucom
                 
END
