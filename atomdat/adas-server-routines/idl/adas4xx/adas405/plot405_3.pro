; Copyright (c) 1995 Strathclyde University .
; SCCS info: Module @(#)$Header: /home/adascvs/idl/adas4xx/adas405/plot405_3.pro,v 1.3 2004/07/06 14:32:45 whitefor Exp $ Date $Date: 2004/07/06 14:32:45 $
;+
; PROJECT:
;       ADAS IBM MVS to DEC UNIX conversion
;
; NAME:
;       PLOT405_3
;
; PURPOSE:
;       Plot contribution function graphs for ADAS405.
;
; EXPLANATION:
;       This routine plots ADAS405 output for one or more plots (each
;       can contain a maximum of seven lines).
;
; USE:
;       Use is specific to ADAS405.  See adas405_3_plot.pro for
;       example.
;
; INPUTS:
;
;	LDEF3	-	Integer; 1 if user specified axis limits to 	
;			be used, 0 if default scaling to be used.
;
;	TOP	-	Integer; last line to plot
;
;	BOTTOM	-	Integer;first line to plot
;
;	X	-	Fltarr; x-values to plot
;
;	Y	-	2d Fltarr; y-values to plot (2nd dimension between
;			BOTTOM and TOP)
;
;	Z	-	Fltarr; total y-values to plot
;
;       RIGHTSTRING  -  String; Metastable indices label to side of graph
;
;       RIGHTSTRING2 -  String; Metastable designations label to side of graph
;
;       RIGHTSTRING3 -  String; Spectrum line specification headings label 
;			to side of graph
;
;       RIGHTSTRING4 -  String; Spectrum line specification information    
;			label to side of graph
;
;       RIGHTSTRING5 -  String; Contains all information to be extracted   
;			to make up RIGHTSTRING6-10
;
;       RIGHTSTRING6 -  String; Contains only linefeeds - used as label
;
;       RIGHTSTRING7 -  String; Contains only linefeeds - used as label
;
;       RIGHTSTRING8 -  String; Contains only linefeeds - used as label
;
;       RIGHTSTRING9 -  String; Contains only linefeeds - used as label
;
;       RIGHTSTRING10-  String; Contains only linefeeds - used as label
;
;	IPLINE	-	String array; indices of plotted lines (used
;			in labelling)
;
;       TITLE   -       String; heading to go above graph
;
;       XMIN3   -       Float; user-defined x-axis minimum
;
;       XMAX3   -       Float; user-defined x-axis maximum
;
;       YMIN3   -       Float; user-defined y-axis minimum
;
;       YMAX3   -       Float; user-defined y-axis maximum
;
; OPTIONAL INPUTS:
;       None.
;
; OUTPUTS:
;       None.
;
; OPTIONAL OUTPUTS:
;       None.
;
; KEYWORD PARAMETERS:
;       FONT    - String; Name of font passed to graphical output
;                 widget.
;
; CALLS:
;       None.
;
; SIDE EFFECTS:
;       None.
;
; CATEGORY:
;       Adas system.
;
; WRITTEN:
;       Tim Hammond, Tessella Support Services plc,  8th November 1995
;
; MODIFIED:
;       1.1     Tim Hammond      
;		First version
;	1.2	Tim Hammond
;		Added new COMMON block Global_lw_data which contains the
;               values of left, right, tp, bot, grtop, grright
;
; VERSION:
;	1.1	08-11-95
;	1.2	27-02-96
;
;-
;----------------------------------------------------------------------------

PRO plot405_3, ldef3, top, bottom, x, y, z, rightstring, rightstring2, 	$
               rightstring3, rightstring4, rightstring5, rightstring6,	$
               rightstring7, rightstring8, rightstring9, rightstring10, $
               ipline, title, xmin3, xmax3, ymin3, ymax3

    COMMON Global_lw_data, left, right, tp, bot, grtop, grright

                ;****************************************************
                ;**** Suitable character size for current device ****
                ;**** Aim for 60 characters in y direction.      ****
                ;****************************************************

    charsize = (!d.y_vsize/!d.y_ch_size)/60.0
    small_check = GETENV('VERY_SMALL')
    if small_check eq 'YES' then charsize=charsize*0.8

                ;**** Initialise titles ****

    xtitle = 'ELECTRON TEMPERATURE (eV)'
    ytitle = 'CONTRIBUTION FUNCTION (cm!E3!N s!E-1!N)'
    righthead = '-- METASTABLE ASSIGNMENTS --!C!C'+			$
                '  INDEX         DESIGNATION  '
    erase

                ;**** How many points to plot ****

    npts = size(x)
    npts = npts(1)

		;**** Find x and y ranges for auto scaling,        ****
                ;**** check x and y in range for explicit scaling. ****

    makeplot = 1
    style = 0
    ystyle = 0
    yplot = y(*, bottom:top)

    if ldef3 eq 0 then begin

                ;**** identify values in the valid range ****
                ;**** plot routines only work within ****
                ;**** single precision limits.       ****

	xvals = where (x gt 1.0e-37 and x lt 1.0e37)
	yvals = where (yplot gt 1.0e-37 and yplot lt 1.0e37)
	zvals = where (z gt 1.0e-37 and z lt 1.0e37)
	if xvals(0) gt -1 then begin
	    maxx = max(x(xvals))
	    minx = min(x(xvals))
	endif else begin
	    makeplot = 0
	endelse
	if yvals(0) gt -1 or zvals(0) gt -1 then begin
	    if yvals(0) gt -1 and zvals(0) eq -1 then begin
	        maxy = max(yplot(yvals))
	        miny = min(yplot(yvals))
	    endif else if yvals(0) eq -1 and zvals(0) gt -1 then begin
		maxy = max(z(zvals))
		miny = min(z(zvals))
	    endif else begin
                maxy = max([max(yplot(yvals)), max(z(zvals))])
                miny = min([min(yplot(yvals)), min(z(zvals))])
	    endelse
	endif else begin
	    makeplot = 0
	endelse
	if makeplot eq 1 then begin
	    if miny le 1.0e-30 then begin
		ystyle = 1
		miny = 1.0e-30
	    endif else begin
		ystyle = 0
	    endelse
	endif
	style = 0
    endif else begin
        minx = xmin3
	maxx = xmax3
	miny = ymin3
	maxy = ymax3
	xvals = where(x gt minx and x lt maxx)
	yvals = where(yplot gt miny and yplot lt maxy)
	zvals = where(z gt miny and z lt maxy)
	if xvals(0) eq -1 or (yvals(0) eq -1 and zvals(0) eq -1) then begin
	    makeplot = 0
	endif else begin
	    makeplot = 1
	endelse
	style = 1
        ystyle = 1
    endelse

    if makeplot eq 1 then begin

                ;**** Set up log-log plotting axes ****

	plot_oo, [minx, maxx], [miny, maxy], /nodata, ticklen=1.0,	$
                 position=[left, bot, grright, grtop],			$
                 xtitle=xtitle, ytitle=ytitle, xstyle=style, 		$
		 ystyle=ystyle, charsize=charsize

                ;*********************************
                ;**** Make and annotate plots ****
                ;*********************************

	if yvals(0) gt -1 then begin
 	    for i=bottom,top do begin
	        ymaxchk = max(y(*,i))		;check this line is okay to 
						;draw
	        if ymaxchk le 1.0e-30 then begin
		    print, '******************************* D5OTG1 MESSAGE '+$
                           '*******************************'
		    print, 'METASTABLE: ', i+1, ' NO GRAPH WILL BE OUTPUT ' +$
		           'BECAUSE:'
		    print, 'ALL VALUES ARE BELOW THE CUTOFF OF 1.000E-30'
		    print, '******************************* END OF MESSAGE '+$
                           '*******************************'
	            endif else begin
	                oplot, x, y(*,i), linestyle=2

                ;**** Find suitable point for annotation ****

                    if (miny eq 10^(!y.crange(0))) then begin
	                if (x(npts-1) ge minx) and (x(npts-1) le maxx) and $
                        (y(npts-1,i) gt miny) and 			   $
                        (y(npts-1,i) le maxy) then begin
		            iplot = npts - 1
	                endif else begin
		            iplot = -1
		            for id = 0, npts-2 do begin
		                if (x(id) ge minx) and (x(id) le maxx) and $
		                (y(id,i) gt miny) and 			   $
                                (y(id,i) le maxy) then begin
			            iplot = id
		                endif
	                    endfor
	                endelse
		    endif else begin
                        if (x(npts-1) ge minx) and (x(npts-1) le maxx) and  $
                        (y(npts-1,i) ge miny) and 			    $
                        (y(npts-1,i) le maxy) then begin
                           iplot = npts - 1
                        endif else begin
                            iplot = -1
                            for id = 0, npts-2 do begin
                                if (x(id) ge minx) and (x(id) le maxx) and  $
                                (y(id,i) ge miny) and 			    $
                                (y(id,i) le maxy) then begin
                                   iplot = id
                                endif
                            endfor
                        endelse
		    endelse
    
                ;**** Annotate plots with level numbers ****

 	            if iplot ne -1 then begin
		        xyouts, x(iplot), y(iplot,i), 			$
                        string(ipline(i), format='(i2)'), alignment=0.5,$
                          charsize=charsize*0.8
	            endif
	        endelse
	    endfor
        endif

		;**** Put in the total plot ****

	if zvals(0) gt -1 then begin
            zmaxchk = max(z(*))             ;check this line is okay to
                                            ;draw
            if zmaxchk lt 1.0e-30 then begin
                print, '******************************* D5OTG1 MESSAGE '+$
                      '*******************************'
                print, 'NO GRAPH WILL BE OUTPUT FOR THE TOTAL'   +	 $
                      'BECAUSE:'
                print, 'ALL VALUES ARE BELOW THE CUTOFF OF 1.000E-30'
                print, '******************************* END OF MESSAGE '+$
                      '*******************************'
            endif else begin
                oplot, x, z(*), linestyle=0

                ;**** Find suitable point for annotation ****

		iplot = -1
		for id = npts-2, fix(npts/2), -1 do begin
		    if (x(id) ge minx) and (x(id) le maxx) and 		$
		    (z(id) ge miny) and (z(id) le maxy) then begin
			iplot = id
		    endif
		endfor
		if iplot ne -1 then begin
		    xyouts, x(iplot), z(iplot), 'TOT', charsize=charsize*0.8
		endif
	    endelse
        endif
    endif else begin			;no plot possible
	xyouts, 0.2, 0.5, /normal, charsize=charsize*1.5,		$
                '---- No data lies within range ----'
    endelse

		;**** Construct component parameters strings ****

    number = n_elements(ipline)
    icstring = strarr(number,5)
    startpos = 0
    length = 1
    i = 0
    j = 0
    rightstring5 = rightstring5 + ' '
    while (startpos lt (strlen(rightstring5)-1)) do begin
        extract = strmid(rightstring5, startpos+length-1, 1)
	if extract eq ' ' then begin
	    if length gt 1 then begin
	        stringval = strmid(rightstring5, startpos, length-1)
                icstring(i,j) = stringval
   	        j = j + 1
	        if j eq 5 then begin
	            j = 0
	            i = i + 1
                endif
                startpos = startpos + length - 1
                length = 1
	    endif else begin
	        startpos = startpos + 1
	    endelse
	endif else begin
	    length = length + 1
	endelse
    endwhile
    rightstring6 = rightstring6 + '!C!C!C!C  IC!C'
    rightstring7 = rightstring7 + '!C!C!C!C  IZ!C'
    rightstring8 = rightstring8 + '!C!C!C!C  IM!C'
    rightstring9 = rightstring9 + '!C!C!C!C   IP!C'
    rightstring11 = rightstring10
    rightstring10 = rightstring10 + '!C!C!C!C  IF!C'
    rightstring11 = rightstring11 + '!C!C!C!C INDX!C'
    for i=0, number-1 do begin
   	if fix(icstring(i,0)) lt 10 then begin
	    rightstring6 = rightstring6 + '!C  ' + 			$
            strtrim(string(icstring(i,0)),2)
	endif else begin
            rightstring6 = rightstring6 + '!C ' +                      	$
            strtrim(string(icstring(i,0)),2)
	endelse
    endfor
    for i=0, number-1 do begin
   	if fix(icstring(i,1)) lt 10 then begin
	    rightstring7 = rightstring7 + '!C  ' + 			$
            strtrim(string(icstring(i,1)),2)
	endif else begin
            rightstring7 = rightstring7 + '!C ' +                      	$
            strtrim(string(icstring(i,1)),2)
	endelse
    endfor
    for i=0, number-1 do begin
   	if fix(icstring(i,2)) lt 10 then begin
	    rightstring8 = rightstring8 + '!C  ' + 			$
            strtrim(string(icstring(i,2)),2)
	endif else begin
            rightstring8 = rightstring8 + '!C ' +                      	$
            strtrim(string(icstring(i,2)),2)
	endelse
    endfor
    for i=0, number-1 do begin
        iplength = strlen(strtrim(icstring(i,3),2))
   	if iplength eq 1 then begin
	    rightstring9 = rightstring9 + '!C    ' + 			$
            strtrim(string(icstring(i,3)),2)
	endif else if iplength eq 2 then begin
            rightstring9 = rightstring9 + '!C   ' +                    	$
            strtrim(string(icstring(i,3)),2)
	endif else if iplength eq 3 then begin
            rightstring9 = rightstring9 + '!C  ' +                    	$
            strtrim(string(icstring(i,3)),2)
	endif else if iplength eq 4 then begin
            rightstring9 = rightstring9 + '!C ' +                    	$
            strtrim(string(icstring(i,3)),2)
	endif else begin
            rightstring9 = rightstring9 + '!C' +                    	$
            strtrim(string(icstring(i,3)),2)
	endelse
    endfor
    for i=0, number-1 do begin
   	if fix(icstring(i,4)) lt 10 then begin
	    rightstring10 = rightstring10 + '!C  ' + 			$
            strtrim(string(icstring(i,4)),2)
	endif else begin
            rightstring10 = rightstring10 + '!C ' +                    	$
            strtrim(string(icstring(i,4)),2)
	endelse
    endfor
    for i=0, number-1 do begin
   	if fix(ipline(i)) lt 10 then begin
	    rightstring11 = rightstring11 + '!C  ' + 			$
            strtrim(string(ipline(i)),2)
	endif else begin
            rightstring11 = rightstring11 + '!C ' +                    	$
            strtrim(string(ipline(i)),2)
	endelse
    endfor

                ;**** Output title above graphs ****
    
    xyouts, 0.1, 0.9, title, charsize=charsize, /normal

                ;**** Output titles to right of graphs ****

    xyouts, 0.72, 0.8, righthead, charsize=charsize, /normal
    xyouts, 0.74, 0.72, rightstring, charsize=charsize, /normal
    xyouts, 0.84, 0.72, rightstring2, charsize=charsize, /normal
    xyouts, 0.72, 0.72, rightstring3, charsize=charsize, /normal
    xyouts, 0.84, 0.72, rightstring4, charsize=charsize, /normal
    xyouts, 0.72, 0.72, rightstring6, charsize=charsize, /normal
    xyouts, 0.76, 0.72, rightstring7, charsize=charsize, /normal
    xyouts, 0.80, 0.72, rightstring8, charsize=charsize, /normal
    xyouts, 0.84, 0.72, rightstring9, charsize=charsize, /normal
    xyouts, 0.89, 0.72, rightstring10, charsize=charsize, /normal
    xyouts, 0.93, 0.72, rightstring11, charsize=charsize, /normal

END
