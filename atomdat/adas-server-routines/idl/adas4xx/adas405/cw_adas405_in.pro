; Copyright (c) 1995 Strathclyde University.
; SCCS info: Module @(#)$Header: /home/adascvs/idl/adas4xx/adas405/cw_adas405_in.pro,v 1.12 2004/07/06 12:45:47 whitefor Exp $ Date $Date: 2004/07/06 12:45:47 $
;+
; PROJECT:
;       ADAS IBM MVS to UNIX conversion
;
; NAME:
;       CW_ADAS405_IN()
;
; PURPOSE:
;       Data file and script file selection for adas 405.
;
; EXPLANATION:
;       This function creates a compound widget consisting of the compound
;       widget cw_adas4xx_infile.pro, the compound widget cw_master_classes,
;	a 'Cancel' button, a 'Done' button and a button to allow the browsing
;	of any comments in the selected script file. The browsing and 'Done'
;	buttons are automatically de-sensitised until appropriate times.
;
;	The value of this widget is contained in the VALUE structure.
;
; USE:
;       See routine adas405_in.pro for an example.
;
; INPUTS:
;       PARENT  - Long integer; the ID of the parent widget.
;
;	CLASSES - String array: the names of the isonuclear master classes
;
; OPTIONAL INPUTS:
;       None
;
; OUTPUTS:
;       This function returns the ID of the compound widget created.
;
; OPTIONAL OUTPUTS:
;       None
;
; KEYWORD PARAMETERS:
;       VALUE   - A structure which determines the initial settings of
;                 the entire compound widget. The structure must be:
;		 	{ ROOTPATH:'',					$
;			  CENTROOT:'',					$
;			  USERROOT:'',					$
;			  SCROOTPATH:'',				$
;			  SCFILE:'',					$
;			  SCCENTROOT:'',				$
;			  SCUSERROOT:'',				$
;			  FILTR:'',					$
;			  BRANCH:0,					$
;			  FILETYPE:0,					$
;			  PARTTYPE:0,					$
;			  YEAR:'',					$
;			  DYEAR:'',					$
;			  SYMBOL:'',					$
;			  MEMBER:'',					$
;			  INDICES:intarr(8) }
;
;		  Where the elements of the structure are as follows:
;
;                 ROOTPATH   - Current data directory e.g '/usr/fred/adas/'
;
;                 CENTROOT   - Default central data store e.g '/usr/adas/'
;
;                 USERROOT   - Default user data store e.g '/usr/fred/adas/'
;
;		  SCROOTPATH - Current script directory e.g. 
;			       '/usr/fred/adas/scripts'
;
;		  SCFILE     - Current script file in SCROOTPATH e.g. 
;			     'script.o.93.dat'
;
;		  SCCENTROOT - Default central script store e.g.
;			       '/usr/adas/adas/scripts'
;
;		  SCUSERROOT - Default user data store e.g.
;			       '/usr/fred/adas/scripts'
;
;		  FILTR	     - String: the radiated power filter
;			       e.g. 'EV1000' (case insensitive)
;
;		  BRANCH     - Int: flag showing which directory branch
;				is selected. 0=user, 1=central
;
;		  FILETYPE   - Int: flag showing what filetype is required.
;				0=standard, 1=partial
;
;		  PARTTYPE   - Int: flag showing what sort of partial
;				filetype is required, 0=Resolved,
;				1=Unresolved.
;
;		  YEAR       - String: the year for the data to be taken from.
;
;		  DYEAR	     - String: a default year if the requested
;				year is not available for some classes.
;
;		  SYMBOL     - String: isonuclear element symbol.
;
;		  MEMBER     - String: the required member prefix for
;			       all files (case insensitive, 2 chars only).
;
;		  INDICES    - Intarr:8 elements showing 1 if the 
;			       corresponding item of CLASSES is selected
;			       and 0 if it is not selected.
;
;		  The script file selected by the user is obtained by
;                 appending SCROOTPATH and SCFILE.  In the above example
;                 the full name of the data file is;
;                 /usr/fred/adas/scripts/script.o.93.dat
;
;                 Path names may be supplied with or without the trailing
;                 '/'.  The underlying routines add this character where
;                 required so that USERROOT will always end in '/' on
;                 output.
;
;       FONT_LARGE  - Supplies the large font to be used for the
;                     interface widgets.
;
;       FONT_SMALL  - Supplies the small font to be used for the
;                     interface widgets.
;
;       RNUM  	    - Supplies the number (as a string) to be used in the
;		      information widget title
;
; CALLS:
;       CW_MASTER_CLASSES  	Compound widget handling the datafile 
;				selection for adas 405
;	CW_ADAS4XX_INFILE	Dataset selection widget.
;	I4EIZ0			Converts element symbol into atomic number
;	POPUP			Pops up simple message window
;
; SIDE EFFECTS:
;       IN405_GET_VAL() Widget management routine in this file.
;       IN405_EVENT()   Widget management routine in this file.
;
; CATEGORY:
;       Compound Widget
;
; WRITTEN:
;       Tim Hammond, Tessella Support Services plc, 15th September 1995
;
; MODIFIED:
;       1.1     Tim Hammond 
;		First release. Created from skeleton of cw_adas401_in
;		and cw_adas309_proc (for hints on not using outdated
;		CW_LOADSTATE and CW_SAVESTATE routines).
;	1.2	Tim Hammond
;		Partially completed. 
;	1.3	Tim Hammond
;		First complete version
;	1.4	Tim Hammond
;		Added extra compression of strings from cw_master_classes
;		widget when 'Done' button is selected. This is to make up
;		for them no longer being set to null due to problems with
;		Sun version of IDL - see cw_master_classes for more details.
;	1.5	Tim Hammond
;               Corrected error checking for situation when user's own
;               adf11 directory does not exist or there are no directories
;               in it.
;	1.6	Tim Hammond
;		Added different behaviours for different directory
;               branches - the program previously ignored whether the
;               user had selected User or Central data and continued
;               as though User had been set. Also ensured that the
;               variable fileavailable is not accidentally set to an
;               integer when it should always be an array.
;	1.7	Tim Hammond
;		Added flexible font-sizing for different target platforms.
;	1.8	William Osborn
;		Added rnum for the routine number to put in the info widget
;		title
;	1.9	William Osborn
;		Added rnum keyword to cw_master_classes
;	1.10	William Osborn
;		Added /noshell keyword to spawn commands and changed input
;		string appropriately.
;	1.11	Martin O'Mullane & R. Martin
;		Removed the absurd method of handling filtered data. The search
;               is now limited to the power data (prb, plt and prc) and the 
;               format is now, for example, /plt89/plt89_ni.ft25003.dat and not
;               /plt89(ft25003)/plt89_ni.dat as it was.
;		    Added check for incompatible scripts/master files.
;	1.12	Richard Martin
;		IDL 5.5 fixes.
;
; VERSION:
;       1.1     15-09-95
;	1.2	02-10-95
;	1.3	07-11-95
;	1.4	09-11-95
;	1.5	10-11-95
;	1.6	13-11-95
;	1.7	27-02-96
;	1.8	25-06-96
;	1.9	27-06-96
;	1.10	19-07-96
;	1.11	13-05-98
;	1.12	17-01-02
;
;-----------------------------------------------------------------------------
;-

FUNCTION in405_get_val, id

    COMMON adas405_availblock, fileavailable, deffileavailable

                ;**** Return to caller on error ****

    ON_ERROR, 2

                 ;***************************************
                 ;****     Retrieve the state        ****
                 ;**** Get first_child widget id     ****
                 ;**** because state is stored there ****
                 ;***************************************

    first_child = widget_info(id, /child)
    widget_control, first_child, get_uvalue = state

		;**** Get the settings ****

    widget_control, state.dataid[0], get_value=classvalues
    widget_control, state.fileid[0], get_value=fileval


    ps = {	ROOTPATH	:	state.inval.rootpath[0],		$
		CENTROOT	:	state.inval.centroot[0],		$
		USERROOT	:	state.inval.userroot[0],		$
		SCROOTPATH	:	fileval.rootpath[0],		$
		SCFILE		:	fileval.file[0],			$
		SCCENTROOT	:	fileval.centroot[0],		$
		SCUSERROOT	:	fileval.userroot[0],		$
		FILTR		:	classvalues.filtr[0],		$
		BRANCH		:	classvalues.branch[0],		$
		FILETYPE	:	classvalues.filetype[0],		$
		PARTTYPE	:	classvalues.parttype[0],		$
		YEAR		:	classvalues.year[0],		$
		DYEAR		:	classvalues.dyear[0],		$
		SYMBOL		:	classvalues.symbol[0],		$
		MEMBER		:	classvalues.member[0],		$
		FILEAVAILABLE	:	fileavailable,			$
		DEFAVAILABLE	:	deffileavailable,		$
		INDICES		:	classvalues.indices		}

    widget_control, first_child, set_uvalue=state, /no_copy

    RETURN, ps

END

;-----------------------------------------------------------------------------

FUNCTION in405_event, event

    COMMON adas405_inblock, userdir, centdir
    COMMON adas405_availblock, fileavailablecom, defavailablecom

    datafilename = strarr(8)

                ;**** Base ID of compound widget ****

    parent = event.handler

                ;**** Default output no event ****

    new_event = 0L

                ;**********************************************
                ;**** Retrieve the user value state        ****
                ;**** Get id of first_child widget because ****
                ;**** user value "state" is stored there   ****
                ;**********************************************

    first_child = widget_info(parent, /child)
    widget_control, first_child, get_uvalue=state, /no_copy
    topparent = widget_info(parent, /parent)

                ;************************
                ;**** Process events ****
                ;************************

    CASE event.id OF

                ;***********************************
                ;**** Event from file selection ****
                ;***********************************

        state.fileid: begin
            if event.action eq 'newfile' then begin
                widget_control, state.doneid, /sensitive
                widget_control, state.browseid, /sensitive
            endif else begin
                widget_control, state.doneid, sensitive=0
                widget_control, state.browseid, sensitive=0
            endelse
        end

                ;***********************
                ;**** Browse button ****
                ;***********************

        state.browseid: begin

                ;**** Get latest filename ****

            widget_control, state.fileid, get_value=inset
            filename = inset.rootpath + inset.file

                ;**** Invoke comments browsing ****

            xxtext, filename, font=state.font_large
        end

                ;***********************
                ;**** Cancel button ****
                ;***********************

        state.cancelid: begin
            new_event = {ID:parent, TOP:event.top,          		$
                         HANDLER:0L, ACTION:'Cancel'}
        end

	state.doneid: begin

		;************************************************
		;**** Get the current settings of the master ****
		;**** classes widget.                        ****
		;************************************************

	    widget_control, state.dataid, get_value=data

		;**** Test the validity of the user's values ****

            errormess = ''

                ;**** Check the member prefix ****
                                                               
            member = strcompress(data.member[0], /remove_all)
            if (strlen(member) ne 2 and (member ne '')) then begin
                errormess =                                             $
                '**** You have entered an invalid member prefix ****'
            endif

                ;**** Check the year ****

            if (errormess eq '') then begin
                year = strcompress(data.year[0], /remove_all)
                if (year eq '') then begin
                    errormess = '**** You must enter a valid year '+    $
                    'for the data ****'
                endif else if (strlen(year) ne 2 ) then begin
                    errormess = '**** You have specified the year '+    $
                    'incorrectly ****'
                endif else begin
                    yearbyt = byte(year)
                    if ((min(yearbyt) lt 48) or                         $
                    (max(yearbyt) gt 57)) then begin
                        errormess =                                     $
                        '**** You have specified the year incorrectly ****'
                    endif
                endelse
            endif

                ;**** Check the default year ****

            if (errormess eq '') then begin
                dyear = strcompress(data.dyear[0], /remove_all)
                if (dyear ne '') then begin
                    if (strlen(dyear) ne 2) then begin
                        errormess = '**** You have specified the default'+$
                                    ' year incorrectly ****'
                    endif else begin
                        dyearbyt = byte(dyear)
                        if ((min(dyearbyt) lt 48) or                    $
                        (max(dyearbyt) gt 57)) then begin
                            errormess = '**** You have specified the '+ $
                            'default year incorrectly ****'
                        endif
                    endelse
                endif
            endif

                ;**** Check the symbol ****

            if (errormess eq '') then begin
                symb = data.symbol[0]
                symbol = strcompress(symb, /remove_all)
		    dummy=symbol
		    symbol=dummy
                if (symbol eq '') then begin
                    errormess = '**** You must enter a valid '+         $
                    'isonuclear element symbol ****'
                endif else begin
                    i4eiz0, symbol, atnumber
                    if (atnumber eq 0) then begin
                        errormess = '**** You have specified an '+      $
                        'invalid element ****'
                    endif
                endelse
            endif

                ;**** if error then give message ****

            if errormess ne '' then begin
                action = popup(message=errormess, buttons=[' OK '],     $
                               font=state.font_large)
		new_event = 0L
            endif  else begin

		;**** No error so start to check on the availability ****

		endsearch = 0		;value of 0=Continue searching
					;         1=stop searching(success)
					;         2=stop searching(failure)

                ;**** Create an information widget ****

                widget_control, /hourglass
                widgetbase = widget_base(/column, xoffset=300, yoffset=200,$
                title = "ADAS"+state.rnum+": INFORMATION")
                lab0 = widget_label(widgetbase, value='')
                lab1 = widget_label(widgetbase, value='')
                lab2 = widget_label(widgetbase, font=state.font_large,  $
                value=" Checking input values and file availability - "+$
                "please wait      ")
                lab3 = widget_label(widgetbase, value='')
                lab4 = widget_label(widgetbase, value='')
                widget_control, widgetbase, /realize
		widget_control, topparent, sensitive=0

                ;**** Firstly construct target directory names ****

                dirnames = strarr(8)            ;user directories
                defnames = strarr(8)            ;central (default) directories
                diravailable = intarr(8)
                defdiravailable = intarr(8)
                fileavailable = intarr(8)
                deffileavailable = intarr(8)

                dirnames = ['acd','scd','ccd','prb','prc','qcd','xcd','plt']
                items = dirnames                ;see cw_display_list below
                defnames = dirnames
                dirnames = dirnames + year
                defnames = defnames + dyear
	    	flag = data.filetype[0]
                if (flag eq 1) then begin
                    partval = data.parttype
                    if (partval eq 0) then begin
                        dirnames = dirnames + 'r'
                        defnames = defnames + 'r'
                    endif else begin
                        dirnames = dirnames + 'u'
                        defnames = defnames + 'u'
                    endelse
                endif
                filtr_val = data.filtr[0]
                filterstring = strcompress(filtr_val(0), /remove_all)
			if filterstring ne '' then begin
                   filteradd = ['','','','.'+filterstring,'.'+filterstring,  $
                                '','','.'+filterstring]
	          endif

                ;**** Now find which directories are available   ****
                ;**** First those in the user space              ****
                ;**** or central space if user has selected that ****

                branchflag = data.branch[0]
                if branchflag eq 0 then begin
                    searchdir = userdir
                endif else begin
                    searchdir = centdir
                endelse
                listcom = ['ls','-l',searchdir + '.']
                listcom2 = ['ls',searchdir + '.']
                spawn, listcom, full_list, /noshell       ;get long-style list
                spawn, listcom2, short_list, /noshell     ;get short-style list

                ;**** Pick out directories ****

                full_list = strcompress(full_list, /remove_all)
                dir_indices = where(strpos(full_list, 'd') eq 0) - 1
                if (dir_indices(0) eq -2) then begin ;no directories
                    endsearch = 2
                    diravailable(*) = 0
                    fileavailable(*) = 0
                endif else begin
                    dir_list = short_list(dir_indices)
                    no_dirs = n_elements(dir_list)
                endelse

                ;**** Flag available directories ****

                if endsearch eq 0 then begin
                    for i=0,7 do begin
                        match = where(dir_list eq dirnames(i))
                        if (match(0) ne -1) then begin
                            diravailable(i) = 1
                        endif else begin
                            diravailable(i) = 0
                        endelse
                    endfor
                    searchtest = where(diravailable eq 1)
                    if searchtest(0) eq -1 then endsearch = 2
                endif

                ;**** Now look into the available directories ****

                if endsearch eq 0 then begin
                    for i=0,(n_elements(searchtest) - 1) do begin
                        listcom3 = ['ls', '-l', searchdir +                 $
                        dirnames(searchtest(i))]
                        listcom4 = ['ls', searchdir +                    $
                        dirnames(searchtest(i))]
                        spawn, listcom3, full_list,/noshell;get long-style list
                        spawn, listcom4, short_list,/noshell    ;get short-style list
                        full_list = strcompress(full_list, /remove_all)

                ;**** What file are we looking for? ****

			if (member ne '') then begin
			    to_find = dirnames(searchtest(i)) + '_' + $
                                      member + '#'
			endif else begin
			    to_find = dirnames(searchtest(i)) + '_'
			endelse
                        
		        if filterstring ne '' then begin
                           to_find = to_find + symbol + $
                                     filteradd(searchtest(i)) + '.dat'
                        endif else begin
			   to_find = to_find + symbol + '.dat'
                        endelse
                        
                        datafilename(i) = to_find
                        filematch = where(short_list eq to_find)
                        if (filematch(0) eq -1) then begin
                            fileavailable(searchtest(i)) = 0
                        endif else begin
                            fileavailable(searchtest(i)) = 1
                        endelse
                    endfor
                endif

                ;**** Now repeat for default year - if one is given ****

                if dyear ne '' then begin
                    defsearchfail = 1
                    listcom5 = ['ls','-l',centdir + '.']
                    listcom6 = ['ls',centdir + '.']
                    spawn, listcom5, full_list, /noshell  ;get long-style list
                    spawn, listcom6, short_list, /noshell ;get short-style list

                ;**** Pick out directories ****

                    full_list = strcompress(full_list, /remove_all)
                    defdir_indices = where(strpos(full_list, 'd') eq 0) - 1
                    if (defdir_indices(0) eq -2) then begin ;no directories
                        defsearchfail = 0
                        defdiravailable(*) = 0
                        deffileavailable(*) = 0
                    endif else begin
                        defdir_list = short_list(defdir_indices)
                        no_defdirs = n_elements(defdir_list)
                    endelse

                ;**** Flag available directories ****

                    if defsearchfail eq 1 then begin
                        for i=0,7 do begin
                            match = where(defdir_list eq defnames(i))
                            if (match(0) ne -1) then begin
                                defdiravailable(i) = 1
                            endif else begin
                                defdiravailable(i) = 0
                            endelse
                        endfor
                        defsearchtest = where(defdiravailable eq 1)
                        if defsearchtest(0) eq -1 then defsearchfail = 0
                    endif

                ;**** Now look into the available directories ****

                    if defsearchfail eq 1 then begin
                        for i=0,(n_elements(defsearchtest) - 1) do begin
                            listcom7 = ['ls','-l',centdir +                 $
                            defnames(defsearchtest(i))]
                            listcom8 = ['ls',centdir +                    $
                            defnames(defsearchtest(i))]
                            spawn, listcom8, short_list,/noshell    ;get short-style list

                ;**** What file are we looking for? ****

                            if (member ne '') then begin
                                to_find = defnames(defsearchtest(i)) + '_' +$
                                          member + '#'
                            endif else begin
                                to_find = defnames(defsearchtest(i)) + '_'
                            endelse
                            
		                if filterstring ne '' then begin
                               to_find = to_find + symbol + $
                                         filteradd(defsearchtest(i)) + '.dat'
                            endif else begin
			       		to_find = to_find + symbol + '.dat'
                            endelse

                            filematch = where(short_list eq to_find)
                            if (filematch(0) eq -1) then begin
                                deffileavailable(defsearchtest(i)) = 0
                            endif else begin
                                deffileavailable(defsearchtest(i)) = 1
                            endelse
                        endfor
                    endif
                endif

                ;**** Search complete, decide on action ****

		infomess = ''
		critical = 0
		fileavailablecom = fileavailable
		defavailablecom = deffileavailable
		messchk1 = where((data.indices eq 1) and 		$
                (fileavailable ne 1))
		if messchk1(0) ne -1 then begin   ;-1 => All files available
					          ;from user space
		    messchk2 = where((data.indices eq 1) and 	$
		    ((fileavailable ne 1) and (deffileavailable ne 1)))
		    if (messchk2(0) ne -1) then begin
		        messchk2a = where((messchk2 eq 0) or 	$
		        (messchk2 eq 1))
		        if messchk2a(0) ne -1 then begin
			    critical = 1
			    infomess = 'Sorry, one or more of '+$
			    'the required files are not available.'
			    infbutts = [' Return to input screen ']
		        endif else begin
			    infomess = 'One or more of the requested'+$
			    ' files is not available.'
			    infbutts = [' Cancel ',' Continue ']
		        endelse
		    endif else begin
		        infomess = 'One or more of the requested '+$
		        'files must be drawn from default data sets.'
		        infbutts = [' Cancel ',' Continue ']
		    endelse
		endif

		widget_control, state.fileid, get_value=scdata

		filename= scdata.rootpath[0] + scdata.file[0]
		if (scdata.file[0] eq 'NULL' ) then begin
		endif else begin		
		 openr, 1, filename
		 txt1=''
		 txt2=''
		 nlines=0
		 ncomp=0
		 izion=0
		 icomp=0
		 imet1=0
		 imet2=0
		
		 for i=1,4 do begin
			readf,1,txt1
		 end

		 epos=strpos(txt1,'=')

		 txt2=strmid(txt1,epos+1,3)

		 nfiles=fix(strtrim(txt2,2))

		 for i=1,nfiles+3 do begin
			readf,1,txt1
		 end

		 readf,1,nlines,ncomp,izion,icomp,imet1	
		 readf,1,txt1
		 if (strmid(txt1,0,1) eq '-') then begin
			imet=1
		 endif else begin
			dummy=strcompress(txt1,/remove_all)
			imet=fix(strmid(dummy,2,1))
		 endelse
		
		 close,1

		 if ( data.filetype[0] eq 0 ) then begin
		  if ( imet gt 1 ) then begin
			critical = 1
			infomess = ['Data type and script incompatible.', $
					'Script requires resolved data.']
		  endif
		 endif else begin
		  if ( data.parttype[0] eq 1 ) and ( imet gt 1 ) then begin
			critical = 1
			infomess = ['Data type and script incompatible.', $
					'Script requires resolved data']
		  endif else if ( data.parttype[0] eq 0 ) and ( imet eq 1 ) then begin
			critical = 1
			infomess = ['Data type and script incompatible.', $
					'Script requires unresolved data']
		  endif
		 endelse	
		endelse

                ;**** Destroy the information widget ****

                widget_control, widgetbase, /destroy
                         
		;**** Now either pop up a window or continue ****

		if critical eq 1 then begin
		    action = popup(message=infomess, buttons=infbutts,	$
		                   font=state.font_large)
		    new_event = 0L
		endif else begin
		    if infomess ne '' and data.searchflag[0] eq 0 then begin
			action = popup(message=infomess, buttons=infbutts,$
				       font=state.font_large)
			if action eq ' Continue ' then begin
                        new_event = {ID:parent, TOP:event.top,              $
                                     HANDLER:0L, ACTION:'Done'}
			endif else begin
			    new_event = 0L
			endelse
		    endif else begin
                        new_event = {ID:parent, TOP:event.top,              $
                                     HANDLER:0L, ACTION:'Done'}
		    endelse
		endelse
		widget_control, topparent, sensitive=1
	    endelse
	end

        ELSE: new_event = 0L

    ENDCASE


                ;*********************************************
                ;*** make "state" available to other files ***
                ;*********************************************

    widget_control, first_child, set_uvalue=state, /no_copy

    RETURN, new_event

END

;-----------------------------------------------------------------------------

FUNCTION cw_adas405_in, parent, classes, VALUE=value,			$
			FONT_LARGE=font_large, FONT_SMALL=font_small,	$
			RNUM = rnum

    COMMON adas405_inblock, userdir, centdir


    IF (N_PARAMS() LT 2) THEN MESSAGE,					$
    'Must specify parent and classes for cw_adas405_in'

    ON_ERROR, 2                                 ;return to caller on error

                ;**** Set defaults for keywords ****

    IF NOT (KEYWORD_SET(value)) THEN begin
        inset = {	ROOTPATH	:	'./',			$
			CENTROOT	:	'',			$
			USERROOT	:	'',			$
			SCROOTPATH	:	'./',			$
			SCFILE		:	'',			$
			SCCENTROOT	:	'',			$
			SCUSERROOT	:	'',			$
			FILTR		:	'',			$
			BRANCH		:	0,			$
			FILETYPE	:	0,			$
			PARTTYPE	:	0,			$
			YEAR		:	'',			$
			DYEAR		:	'',			$
			SYMBOL		:	'',			$
			MEMBER		:	'',			$
			INDICES		:	intarr(8)		}
	inset.indices(0:1) = 1
    ENDIF ELSE BEGIN
	inset = {	ROOTPATH        :       value.rootpath,		$
			CENTROOT        :       value.centroot,		$
			USERROOT        :       value.userroot,		$
                        SCROOTPATH      :       value.scrootpath,	$
                        SCFILE          :       value.scfile,		$
                        SCCENTROOT      :       value.sccentroot,	$
                        SCUSERROOT      :       value.scuserroot,	$
			FILTR		:	value.filtr,		$
			BRANCH		:	value.branch,		$
			FILETYPE	:	value.filetype,		$
			PARTTYPE	:	value.parttype,		$
			YEAR		:	value.year,		$
			DYEAR		:	value.dyear,		$
			SYMBOL		:	value.symbol,		$
			MEMBER		:	value.member,		$
                        INDICES         :       value.indices		}
        if strtrim(inset.rootpath) eq '' then begin
            inset.rootpath = './'
        endif else if                                                   $
        strmid(inset.rootpath, strlen(inset.rootpath)-1,1) ne '/' then begin
            inset.rootpath = inset.rootpath+'/'
        endif
        if strtrim(inset.scrootpath) eq '' then begin
            inset.scrootpath = './'
        endif else if                                                   $
        strmid(inset.scrootpath, strlen(inset.scrootpath)-1,1) ne '/' then begin
            inset.scrootpath = inset.scrootpath+'/'
        endif
	if strmid(inset.scfile, 0, 1) eq '/' then begin
            inset.scfile = strmid(inset.scfile, 1, strlen(inset.scfile)-1)
        endif
    ENDELSE
    userdir = inset.userroot
    centdir = inset.centroot
    IF NOT (KEYWORD_SET(font_large)) THEN font_large = ''
    IF NOT (KEYWORD_SET(font_small)) THEN font_small = ''
    IF NOT (KEYWORD_SET(rnum)) THEN rnum = '405'

		;**** Modify font sizes depending on platform ****

    machine = GETENV('TARGET_MACHINE')
    if machine eq 'HPUX' then begin
	large_font = font_small
    endif else begin
	large_font = font_large
    endelse

                ;*********************************
                ;**** Create the Input widget ****
                ;*********************************

                ;**** create base widget ****

    topbase = widget_base(parent, EVENT_FUNC = "in405_event",		$
                          FUNC_GET_VALUE = "in405_get_val",		$
		          /column)

                ;******************************************************
                ;**** Create a dummy widget just to hold value of *****
                ;**** "state" variable so as not to get confused  *****
                ;**** with any other values. Adopt IDL practice   *****
                ;**** of using first child widget                 *****
                ;******************************************************

    first_child = widget_base(topbase)

    cwid = widget_base(first_child, /column)

		;****************************
		;**** Data files widget  ****
		;****************************

    database = widget_base(cwid, /frame)
    classtit = "Enter details of the iso-nuclear master files to be analysed:-"
    dataid = cw_master_classes(database, cwid, classes, inset.indices,	$
			       inset.userroot, inset.centroot,		$
			       font=large_font,	title=classtit, 	$
			       rnum = rnum)
    datavalue = {	member		:	inset.member,		$
			year		:	inset.year,		$
			dyear		:	inset.dyear,		$
			symbol		:	inset.symbol,		$
			branch		:	inset.branch,		$
			filetype	:	inset.filetype,		$
			parttype	:	inset.parttype,		$
			filtr		:	inset.filtr		}
    widget_control, dataid, set_value = datavalue

                ;**************************************
                ;**** Script file selection widget ****
                ;**************************************

    scriptbase = widget_base(cwid, /column, /frame)

    scrval = {	ROOTPATH	:	inset.scrootpath,		$
		FILE		:	inset.scfile,			$
		CENTROOT	:	inset.sccentroot,		$
		USERROOT	:	inset.scuserroot		}
    scrtitle = 'Input Line and Analysis Selection File:-'
    fileid = cw_adas4xx_infile(scriptbase, value=scrval,       		$
                               title=scrtitle, font=large_font)


                ;*****************
                ;**** Buttons ****
                ;*****************

    base = widget_base(cwid, /row)

                ;**** Browse Dataset button ****

    browseid = widget_button(base, value='Browse Selection File Comments',$
                             font=large_font)

                ;**** Cancel Button ****

    cancelid = widget_button(base, value='Cancel', font=large_font)

                ;**** Done Button ****

    doneid = widget_button(base, value='Done', font=large_font)

		;************************************************
		;**** Check filename and desensitise buttons ****
		;**** if it is a directory or it is a file   ****
		;**** without read access.		     ****
		;************************************************

    filename = inset.scrootpath + inset.scfile
    file_acc, filename, fileexist, read, write, execute, filetype
    if filetype ne '-' then begin
        widget_control, browseid, sensitive=0
        widget_control, doneid, sensitive=0
    endif else begin
        if read eq 0 then begin
            widget_control, browseid, sensitive=0
            widget_control, doneid, sensitive=0
        endif
    endelse



                ;*************************************************
                ;**** create a state structure for the pop-up ****
                ;****                window.                  ****
                ;*************************************************

    new_state = {	font_large	:	large_font,		$
			font_small	:	font_small,		$
			inval		:	inset,			$
			dataid		:	dataid,			$
			doneid		:	doneid,			$
			browseid	:	browseid,		$
			fileid		:	fileid,			$
			cancelid	:	cancelid,		$
			rnum		: 	rnum			}

                 ;**** Save initial state structure ****

    widget_control, first_child, set_uvalue=new_state, /no_copy
    
    RETURN, topbase

END
