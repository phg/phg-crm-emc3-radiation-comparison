; Copyright (c) 1996, Strathclyde University.
; SCCS info: Module @(#)$Header: /home/adascvs/idl/adas4xx/adas406/adas406_3_plot.pro,v 1.2 2004/07/06 10:43:43 whitefor Exp $ Date $Date: 2004/07/06 10:43:43 $
;+
; PROJECT:
;       ADAS IBM MVS to UNIX conversion
;
; NAME:
;       ADAS406_3_PLOT
;
; PURPOSE:
;       Generates ADAS406 contribution function graphical output.
;
; EXPLANATION:
;       This routine creates a window for the display of graphical
;       output. A separate routine PLOT406_3 actually plots the
;       graph.
;
; USE:
;       This routine is specific to the contribution fn plotting section
;	of ADAS406.
;
; INPUTS:
;       DSFULL  - String; Name of data file
;
;       UTITLE  - String; Optional comment by user added to graph title.
;
;       LDEF3   - Integer; 1 - use user entered graph scales
;                          0 - use default axes scaling
;
;	NMSUM	- Integer; no. of metastables (output curves)
;
;	X	- Fltarr; electron temperatures - x-values
;
;	Y	- 2D Fltarr; contribution functions - y-values
;
;	Z	- Fltarr; total y-values
;
;       HRDOUT - Integer; 1 if hardcopy output activated, 0 if not.
;
;       HARDNAME- String; Filename for harcopy output.
;
;       DEVICE  - String; IDL name of hardcopy output device.
;
;       HEADER  - String; ADAS version number header to include in graph.
;
;	POPTIT	- String array; the metastable designations (used as label)
;
;	SPECIES - String; element name to be used in graph title
;
;	YEAR	- String; user's selected year for data
;
;	DYEAR	- String; user's selected default year for data (if any)
;
;       XMIN3   - String; Lower limit for x-axis of graph, number as string.
;
;       XMAX3   - String; Upper limit for x-axis of graph, number as string.
;
;       YMIN3   - String; Lower limit for y-axis of graph, number as string.
;
;       YMAX3   - String; Upper limit for y-axis of graph, number as string.
;
;	ISPLINE	- Integer; the selected line from the script file.
;
;	CTITLE  - String array: Any information given in script file on
;                 lines to be analysed.
;
;       CMPTS   - Int array: no. components for each line in script file
;
;	IPLINE	- String array; indices of plotted lines (used in labelling)
;
;	RIGHTSTRING5 - String; Contains all labels used in 'component
;		       parameters' label on graph, this string is
;		       interrogated by plot406_3 to extract all the
;		       required information.
;
;	TIME4	- Double; integration time
;
;	SY0	- Double vector; initial fractional abundances
;
;       BITFILE - String; the path to the directory containing bitmaps
;                 for the 'escape to series menu' button.
;
; OPTIONAL INPUTS:
;       None.
;
; OUTPUTS:
;       GOMENU  - Int; flag - set to 1 if user has selected 'escape direct
;                 to series menu', otherwise 0
;
; OPTIONAL OUTPUTS:
;       None.
;
; KEYWORD PARAMETERS:
;       FONT    - String; The name of a font to use for text in the
;                 graphical output widget.
; CALLS:
;       CW_ADAS_GRAPH   Graphical output widget.
;       PLOT406_3       Make plots to an output device for 406(cont fn)
;       XMANAGER
;
; SIDE EFFECTS:
;       This routine uses a common block to maintain its state 
;	PLOT406_3_BLK.
;
;       One other routine is included in this file;
;       ADAS406_3_PLOT_EV Called via XMANAGER during widget management.
;
; CATEGORY:
;       Adas system.
;
; WRITTEN:
;       William Osborn, Tessella Support Services plc, 21st June 1996
;
; MODIFIED:
;       1.1     William Osborn
;               First version
;	1.2     William Osborn
;               Added check for new environment variable VERY_SMALL
;               If this is set to the string "YES" (preferably in the
;               .adas_setup file) then the font size used on the graphs
;               is reduced and the positioning of labels adjusted
;               slightly to make things fit on a very small screen.
;
; VERSION:
;       1.1     21-06-96
;	1.2	11-10-96
;
;-
;----------------------------------------------------------------------------

PRO adas406_3_plot_ev, event

    COMMON plot406_3_blk, action, plotdev, plotfile, fileopen, win, 	$
                          iplot, nplot, data, nsum, gomenu

    newplot = 0
    print = 0
    done = 0
                ;****************************************
                ;**** Set graph and device requested ****
                ;****************************************

    CASE event.action OF

        'previous' : begin
            if iplot gt 0 then begin
                newplot = 1
                iplot = iplot - 1
                first = iplot
                last = iplot
            endif
	end

        'next'     : begin
            if iplot lt nplot-1 then begin
                newplot = 1
                iplot = iplot + 1
                first = iplot
                last = iplot
            endif
        end

        'print'    : begin
            newplot = 1
            print = 1
	    first = iplot
	    last = iplot
        end

        'printall' : begin
            newplot = 1
            print = 1
            first = 0
            last = nplot-1
        end

        'done'     : begin
            if fileopen eq 1 then begin
                set_plot, plotdev
                device, /close_file
            end
            set_plot,'X'
            widget_control, event.top, /destroy
            done = 1
        end

        'bitbutton' : begin
            if fileopen eq 1 then begin
                set_plot, plotdev
                device, /close_file
            end
            set_plot,'X'
            widget_control, event.top, /destroy
            done = 1
            gomenu = 1
        end
              
    END

                ;*******************************
                ;**** Make requested plot/s ****
                ;*******************************

    if done eq 0 then begin

                ;**** Set graphics device ****

        if print eq 1 then begin
            set_plot, plotdev
            if fileopen eq 0 then begin
                fileopen = 1
                device, filename=plotfile
                device, /landscape
            endif
        endif else begin
            set_plot,'X'
            wset, win
        endelse

                ;**** Draw graphics ****

        if newplot eq 1 then begin
            for i = first, last do begin
	        bottom = i * 7
	        top = bottom + 6
                if top gt (nsum-1) then top=nsum-1
                plot406_3, data.ldef3, top, bottom, data.x, data.y, data.z,$
                           data.rightstring, data.rightstring2, 	$
			   data.rightstring3, data.rightstring4,	$
			   data.rightstring5, data.rightstring6,	$
			   data.rightstring7, data.rightstring8, 	$
			   data.rightstring9, data.rightstring10, 	$
                           data.ipline, 				$
			   data.title, data.xmin3, data.xmax3, 		$
			   data.ymin3, data.ymax3
	        if print eq 1 then begin
                    message = 'Plot  written to print file.'
                    grval = {WIN:0, MESSAGE:message}
                    widget_control, event.id, set_value=grval
                endif
	    endfor
        endif
    endif

END

;----------------------------------------------------------------------------
                                                                
PRO adas406_3_plot, dsfull, utitle, ldef3, nmsum, x, y, z,		$
                    hrdout, hardname, device, header, poptit, 		$
                    species, year, dyear, xmin3, xmax3, ymin3, ymax3,	$
                    ispline, ctitle, cmpts, ipline, rightstring5, 	$
                    time4, sy0, bitfile, gomenu, FONT=font

    COMMON plot406_3_blk, action, plotdev, plotfile, fileopen, win, 	$
                          iplot, nplot, data, nsum, gomenucom

    ngpic = 7			;max no. of lines per plot

                ;**** Copy input values to common ****

    plotdev = device
    plotfile = hardname
    fileopen = 0
    nsum = nmsum
    gomenucom = gomenu

		;*********************************************
		;**** Generate indices to be sent to plot ****
		;**** routine if there are to be multiple ****
		;**** plots drawn			  ****
		;*********************************************

    nplot = 0
    if nmsum le ngpic then begin
	bottom = 0
	top = (nmsum-1)
	nplot = 1
    endif else begin
	bottom = 0
	top = (ngpic-1)
        if nmsum mod ngpic ne 0 then begin
            nplot = fix(nmsum/ngpic) + 1
        endif else begin
            nplot = fix(nmsum/ngpic)
        endelse
    endelse

                ;************************************
                ;**** Create general graph titles****
                ;************************************

    title = "CONTRIBUTION FUNCTION VS. ELECTRON TEMPERATURE"
    if ( strtrim(strcompress(utitle),2)  ne ' ' ) then begin
        title = title + ': ' + strupcase(strtrim(utitle,2))
    endif
    small_check = GETENV('VERY_SMALL')
    if small_check eq 'YES' then begin
        title =  title + '!C!CADAS    : ' + strupcase(header)
        if strcompress(dyear, /remove_all) eq '' then defyear = 'NONE' else $
          defyear = strcompress(dyear, /remove_all)
        year = strcompress(year, /remove_all)
        title =  title + '!C!CFILE     : ' + strcompress(dsfull) + 	$
          '   SPECIES: ' + strupcase(species) + '   YEAR: ' + year +	$
          '   DEFAULT YEAR: '+ defyear
        title =  title + '!C!CKEY     : (FULL LINE - TOTAL) (DASH LINE - PARTIAL)'
    endif else begin
        title =  title + '!C!CADAS    : ' + strupcase(header)
        if strcompress(dyear, /remove_all) eq '' then defyear = 'NONE' else $
          defyear = strcompress(dyear, /remove_all)
        year = strcompress(year, /remove_all)
        title =  title + '!CFILE     : ' + strcompress(dsfull) + 	$
          '   SPECIES: ' + strupcase(species) + '   YEAR: ' + year +	$
          '   DEFAULT YEAR: '+ defyear
        title =  title + '!CKEY     : (FULL LINE - TOTAL) (DASH LINE - PARTIAL)'
    endelse

    rightstring = ''
    rightstring2 = ''
    rightstring12 = ''
    rightstring3 = '!C!C'
    rightstring4 = '!C!C'
    rightstring6 = '!C!C'
    rightstring7 = '!C!C'
    rightstring8 = '!C!C'
    rightstring9 = '!C!C'
    rightstring10 = '!C!C'
    for i=0, (nmsum-1) do begin
        if (i+1) lt 10 then rightstring = rightstring + ' '
        rightstring = rightstring + strtrim(string(ipline(i)),2) + '!C'
        rightstring2 = rightstring2 + strupcase(poptit(ipline(i)-1)) + '!C'
        rightstring12 = rightstring12 + strupcase(sy0(ipline(i)-1)) + '!C'
        rightstring3 = rightstring3 + '!C'
        rightstring4 = rightstring4 + '!C'
        rightstring6 = rightstring6 + '!C'
        rightstring7 = rightstring7 + '!C'
        rightstring8 = rightstring8 + '!C'
        rightstring9 = rightstring9 + '!C'
        rightstring10 = rightstring10 + '!C'
    endfor
    rightstring3 = rightstring3 + '-- SPECTRUM LINE SPECIFICATION --'
    rightstring3 = rightstring3 + '!C!C TITLE!C SELECT NO.!C COMPONENTS'+$
                   '!C!C!C-- COMPONENT PARAMETERS --'
    rightstring4 = rightstring4 + '!C!C= ' +				$
		   strtrim(string(ctitle), 2) + 'A' + 			$
                   '!C= ' + strcompress(string(ispline+1),/remove_all) +$
                   '!C= ' + strcompress(string(cmpts), /remove_all)
    rightstring6 = rightstring6 + '!C!C!C!C!C'
    rightstring7 = rightstring7 + '!C!C!C!C!C'
    rightstring8 = rightstring8 + '!C!C!C!C!C'
    rightstring9 = rightstring9 + '!C!C!C!C!C'
    rightstring10 = rightstring10 + '!C!C!C!C!C'

    inttime = 'INTEGRATION TIME (sec) = ' + string(time4,format='(e10.2)')

                ;*************************************
                ;**** Create graph display widget ****
                ;*************************************

    graphid = widget_base(TITLE='ADAS406 GRAPHICAL OUTPUT', 		$
                          XOFFSET=1,YOFFSET=1)
    device, get_screen_size=scrsz
    xwidth=scrsz(0)*0.75
    yheight=scrsz(1)*0.75
    if nplot gt 1 then multiplot=1 else multiplot=0
    bitval = bitfile + '/menu.bmp'
    cwid = cw_adas_graph(graphid, print=hrdout, FONT=font,		$
                         xsize=xwidth, ysize=yheight, 			$
			 multiplot=multiplot, bitbutton=bitval)

                ;**** Realize the new widget ****

    widget_control, graphid, /realize

                ;**** Get the id of the graphics area ****

    widget_control, cwid, get_value=grval
    win = grval.win

                ;*******************************************
                ;**** Put the graphing data into common ****
                ;*******************************************

    data = {	X		:	x,				$
		Y		:	y,				$
		Z		:	z,				$
		XMIN3		:	xmin3,				$
		XMAX3		:	xmax3,				$
		YMIN3		:	ymin3,				$
		YMAX3		:	ymax3,				$
		RIGHTSTRING	:	rightstring,			$
		RIGHTSTRING2	:	rightstring2,			$
		RIGHTSTRING3	:	rightstring3,			$
		RIGHTSTRING4	:	rightstring4,			$
		RIGHTSTRING5	:	rightstring5,			$
		RIGHTSTRING6	:	rightstring6,			$
		RIGHTSTRING7	:	rightstring7,			$
		RIGHTSTRING8	:	rightstring8,			$
		RIGHTSTRING9	:	rightstring9,			$
		RIGHTSTRING10	:	rightstring10,			$
		RIGHTSTRING12	:	rightstring12,			$
		INTTIME		:	inttime,			$
		IPLINE		:	ipline,				$
		TITLE		:	title,				$
		LDEF3		:	ldef3				}

                ;**** Initialise to plot 0 ****
    iplot = 0
    wset, win

    plot406_3, ldef3, top, bottom, x, y, z, rightstring, rightstring2, 	$
               rightstring3, rightstring4, rightstring5, rightstring6,	$
               rightstring7, rightstring8, rightstring9, rightstring10,	$
               rightstring12, inttime, ipline, title, xmin3, xmax3, 	$
	       ymin3, ymax3

                ;***************************
                ;**** make widget modal ****
                ;***************************

    xmanager, 'adas406_3_plot', graphid, /modal, /just_reg,		$
              event_handler='adas406_3_plot_ev'

    gomenu = gomenucom
    
END
