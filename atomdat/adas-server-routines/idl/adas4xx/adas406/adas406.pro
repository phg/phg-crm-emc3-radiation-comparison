; Copyright (c) 1996, Strathclyde University.
; SCCS info: Module @(#)$Header: /home/adascvs/idl/adas4xx/adas406/adas406.pro,v 1.9 2004/07/06 10:43:28 whitefor Exp $ Date $Date: 2004/07/06 10:43:28 $
;+
; PROJECT:
;       ADAS IBM MVS to UNIX conversion
;
; NAME:
;	ADAS406
;
; PURPOSE:
;	The highest level routine for the ADAS 406 program.
;
; EXPLANATION:
;	This routine is called from the main adas system routine, adas.pro,
;	to start the ADAS 406 application.  Associated with adas406.pro
;	is a FORTRAN executable.  The IDL code provides the user
;	interface and output graphics whilst the FORTRAN code reads
;	in data files, performs numerical processing and creates the
;	output files.  The IDL code communicates with the FORTRAN
;	executable via a bi-directional UNIX pipe.  The unit number
;	used by the IDL for writing to and reading from this pipe is
;	allocated when the FORTRAN executable is 'spawned' (see code
;	below).  Pipe communications in the FORTRAN process are to
;	stdin and stdout, i.e streams 5 and 6.
;
;	The FORTRAN code is an independent process under the UNIX system.
;	The IDL process can only exert control over the FORTRAN in the
;	data which it communicates via the pipe.  The communications
;	between the IDL and FORTRAN must be exactly matched to avoid
;	input conversion errors.  The correct ammounts of data must be
;	passed so that neither process 'hangs' waiting for communications
;	which will never occur.
;
;	The FORTRAN code performs some error checking which is
;	independent of IDL.  In cases of error the FORTRAN may write
;	error messages.  To prevent these error messages from conflicting
;	with the pipe communications all FORTRAN errors are written to
;	output stream 0, which is stderr for UNIX.  These error messages
;	will appear in the window from which the ADAS session/IDL session
;	is being run.
;
;	In the case of severe errors the FORTRAN code may terminate
;	itself prematurely.  In order to detect this, and prevent the
;	IDL program from 'hanging' or crashing, the IDL checks to see
;	if the FORTRAN executable is still an active process before
;	each group of pipe communications.  The process identifier
;	for the FORTRAN process, PID, is recorded when the process is
;	first 'spawned'.  The system is then checked for the presence
;	of the FORTRAN PID.
;
; USE:
;	First the system settings must be established by calling
;	adas_sys_set.pro then adas406.pro is called to start the
;	ADAS 406 application;
;
;	adas_sys_set, adasrel, fortdir, userroot, centroot, $
;		      devlist, devcode, font_large, font_small, edit_fonts
;	adas406,   adasrel, fortdir, userroot, centroot, devlist, $
;		   devcode, font_large, font_small, edit_fonts
;
; INPUTS:
;	ADASREL - A string indicating the ADAS system version, 
;		  e.g ' ADAS RELEASE: ADAS93 V1.4'.  The first
;		  character should be a space.
;
;	FORTDIR - A string holding the path to the directory where the
;		  FORTRAN executables are, e.g '/disk/adas/fortran/exec'
;
;	USERROOT - A string holding the path to the root directory of
;		   the user's adas data. e.g '/disk/bowen/adas'
;		   This root directory will be used by adas to construct
;		   other path names.  For example the users default data
;		   for adas205 should be in /disk/bowen/adas/adf04.  In
;		   particular the user's default interface settings will
;		   be stored in the directory USERROOT+'/defaults'.  An
;		   error will occur if the defaults directory does not
;		   exist.
;
;	CENTROOT - Like USERROOT, but this directory points to the
;		   central data area for the system.  User defaults are
;		   not stored on CENTROOT.
;
;	DEVLIST - A string array of hardcopy device names, used for
;		  graphical output. e.g ['Post-Script','HP-PCL','HP-GL']
;		  This array must mirror DEVCODE.  DEVCODE holds the
;		  actual device names used in a SET_PLOT statement.
;
;	DEVCODE - A string array of hardcopy device code names used in
;		  the SET_PLOT statement, e.g ['PS', 'PCL', 'HP']
;		  This array must mirror DEVLIST.
;
;	FONT_LARGE - The name of a larger font e.g 'courier_bold14'
;
;	FONT_SMALL - The name of a smaller font e.g 'courier_bold12'
;
;	EDIT_FONTS - A structure of two fonts used in the table editor
;		     adas_edtab.pro, {font_norm:'',font_input:''}.
;		     The two fonts are used to differentiate between
;		     editable and non-editable parts of the table. You
;		     may have to experiment to get two fonts which give
;		     the same default spacing in the edit widget.
;		     e.g {font_norm:'helvetica_bold14', $
;			  font_input:'helvetica_oblique14'}
;
; OPTIONAL INPUTS:
;	None
;
; OUTPUTS:
;	None - Note this routine should leave all inputs un-modified.
;
; OPTIONAL OUTPUTS:
;	None
;
; KEYWORD PARAMETERS:
;	None
;
; CALLS:
;	FIND_PROCESS	Checks to see if a given process is active.
;       XXDATE          Get date and time from operating system.
;       D6SPF0          Pipe comms with FORTRAN D6SPF0 routine.
;       D6ISPF          Pipe comms with FORTRAN D6ISPF routine.
;       D6SPF1          Pipe comms with FORTRAN D6SPF1 routine.
;	D6OTG1		Pipe comms with FORTRAN D6OTG1 routine.
;	D6OTG2		Pipe comms with FORTRAN D6OTG2 routine.
;	D6OTG3		Pipe comms with FORTRAN D6OTG3 routine.
;
; SIDE EFFECTS:
;	This routine spawns a FORTRAN executable.  Note the pipe 
;	communications routines listed above.  In addition to these
;	pipe communications there is one explicit communication of the
;	date to the FORTRAN code, search for 'printf, pipe' to find it.
;	There are also communications of the variable gomenu to the
;	FORTRAN which is used as a signal to stop the program in its
;	tracks and return immediately to the series menu. Do the same
;	search as above to find the instances of this.
;
; CATEGORY:
;	Adas system.
;	
; WRITTEN:
;       William Osborn, Tessella Support Services plc, 11/06/1996
;
; MODIFIED:
;	1.1	William Osborn
;		First release - created from adas405.pro v1.12
;	1.2	William Osborn
;		Changed GOF to GCF in outval structure
;       1.3     William Osborn
;               Increased version number to 1.2
;	1.4	Richard Martin
;		Increased version number to 1.3
;	1.5	Richard Martin
;		Increased version number to 1.4
;	1.6	Richard Martin
;		Increased version number to 1.5
;	1.7	Richard Martin
;		Increased version number to 1.6
;       1.8     Martin O'Mullane
;               Replaced passing name file to adas406_adf16.pass to 
;               better indicate its meaning.
;		Increased version no. to 1.7
;	1.9	Richard Martin
;		Increased version number to 1.8
;
; VERSION:
;       1.1	11-06-96
;       1.2	04-07-96
;       1.3	14-10-96
;	  1.4	07-04-97
;	  1.5	11-03-98
;	  1.6 09-06-98
;	  1.7 04-12-98
;       1.8     06-02-01
;	  1.9 18-03-02
; 
;-
;-----------------------------------------------------------------------------

PRO ADAS406,	adasrel, fortdir, userroot, centroot, 			$
		devlist, devcode, font_large, font_small, edit_fonts

		;************************
		;**** Initialisation ****
		;************************

    adasprog = ' PROGRAM: ADAS406 V1.8'
    lpend = 0
    gomenu = 0
    deffile = userroot+'/defaults/adas406_defaults.dat'
    bitfile = centroot+'/bitmaps'
    device = ''
    classes = ['ACD - Recombination coefficients',			$
               'SCD - Ionisation coefficients',				$
               'CCD - Recombination coefficients: charge exchange',	$
	       'PRB - Power coefficients: recombination-bremsstrahlung',$
	       'PRC - Power coefficients: charge-exchange recombination',$
	       'QCD - Cross-coupling coefficients',			$
	       'XCD - Cross-coupling coefficients: parent',		$
	       'PLT - Line power coefficients: total']
    dataclasses = ['ACD','SCD','CCD','PRB','PRC','QCD','XCD','PLT']
    indices = intarr(8)
    indices(0:1) = 1

		;******************************************
		;**** Search for user default settings ****
		;**** If not found create defaults     ****
		;**** inval: settings for data files   ****
		;**** scval: settings for script files ****
		;**** procval: settings for processing ****
		;**** outval: settings for output      ****
		;******************************************

    files = findfile(deffile)
    if files(0) eq deffile then begin
        restore, deffile
        inval.centroot = centroot+'/adf11/'
        inval.userroot = userroot+'/adf11/'
        inval.sccentroot = centroot+'/scripts406/'
        inval.scuserroot = userroot+'/scripts406/'
    endif else begin
        inval = { 							$
		  ROOTPATH:userroot+'/adf11/', 				$
		  CENTROOT:centroot+'/adf11/', 				$
		  USERROOT:userroot+'/adf11/',				$
		  SCROOTPATH:centroot+'/scripts406/', 			$
		  SCFILE:'', 						$
		  SCCENTROOT:centroot+'/scripts406/', 			$
		  SCUSERROOT:userroot+'/scripts406/',			$
		  FILTR:'',						$
		  BRANCH:0,						$
		  FILETYPE:0,						$
		  PARTTYPE:0,						$
		  YEAR:'',						$
		  DYEAR:'',						$
		  SYMBOL:'',						$
		  MEMBER:'',						$
		  INDICES:indices 					$
		}
  
      procval = {NEW:-1}

      outval = { 							$
 			LPION_AVAIL	:	1,			$
 			LPOWER_AVAIL	:	0,			$
 			LCONT_AVAIL	:	0,			$
 			LEXCPM_AVAIL	:	0,			$
 			LGRAPHNO	:	0,			$
                  	XMIN1		:	'',  			$
			XMAX1		:	'',			$
                  	YMIN1		:	'',  			$
			YMAX1		:	'',			$
                  	XMIN2		:	'',  			$
			XMAX2		:	'',			$
                  	YMIN2		:	'',  			$
			YMAX2		:	'',			$
                  	XMIN3		:	'',  			$
			XMAX3		:	'',			$
                  	YMIN3		:	'',  			$
			YMAX3		:	'',			$
                  	XMIN4		:	'',  			$
			XMAX4		:	'',			$
                  	YMIN4		:	'',  			$
			YMAX4		:	'',			$
			LDEF1		:	0,			$
			LDEF2		:	0,			$
			LDEF3		:	0,			$
			LDEF4		:	0,			$
		 	GRPOUT		:	0,			$
                  	GTIT1		:	'',  			$
                  	HRDOUT		:	0, 			$
			HARDNAME	:	'',			$
                  	GRPDEF		:	'', 			$
			GRPFMESS	:	'',			$
                  	DEVSEL		:	-1, 			$
                  	TEXOUT		:	0, 			$
			TEXAPP		:	-1, 			$
                  	TEXREP		:	0, 			$
			TEXDSN		:	'',			$
                  	TEXDEF		:	'paper.txt',		$
			TEXMES		:	'',			$
		  	GCFOUT		:	0, 			$
			GCFAPP		:	-1,			$
		  	GCFREP		:	0, 			$
			GCFDSN		:	'',			$
		  	GCFDEF		:	'adas406_adf16.pass', 	$
			GCFMES		:	''			}
    end


		;****************************
		;**** Start fortran code ****
		;****************************

    spawn, fortdir+'/adas406.out', unit=pipe, /noshell, PID=pid

		;************************************************
		;**** Get date and write to fortran via pipe ****
		;************************************************

    date = xxdate()
    printf, pipe, date(0)

LABEL100:
		;**** Check FORTRAN still running ****
  
    if find_process(pid) eq 0 then goto, LABELEND

		;************************************************
		;**** Communicate with d6spf0 in fortran and ****
		;**** invoke user interface widget for       ****
		;**** Data file selection                    ****
		;************************************************

    d6spf0, pipe, inval, classes, rep, lsnull,				$
	    FONT_LARGE=font_large, FONT_SMALL=font_small

		;**** If cancel selected then end program ****

    if rep eq 'YES' then goto, LABELEND

LABEL5:
		;**** Check FORTRAN still running ****

    if find_process(pid) eq 0 then goto, LABELEND

		;**** If there was a script file was it okay? ****

    input = 0
    if lsnull eq 0 then begin
	readf, pipe, input
	if input ne 0 then goto, LABELEND
    endif

		;************************************************
		;**** Communicate with d6ispf in fortran and ****
		;**** invoke user interface widget for       ****
		;**** Processing options                     ****
		;************************************************

    dsfull = inval.scrootpath + inval.scfile
    classstring = ''
    clasflg = where((inval.indices eq 1) and ((inval.fileavailable eq 1)$
              or (inval.defavailable eq 1)))
    if (clasflg(0) ne -1) then begin
	for i=0, n_elements(clasflg) - 1 do begin
	    if classstring ne '' then classstring = classstring + ', '
	    classstring = classstring + dataclasses(clasflg(i))
	endfor
    endif else begin
	classstring = 'NONE'
    endelse

		;**** Check FORTRAN still running ****

    if find_process(pid) eq 0 then goto, LABELEND

    d6ispf, pipe, lpend, procval, dsfull, 				$
	    inval.symbol, classstring, lsnull,				$
	    ctitle, cmpts, gomenu, bitfile, 				$
	    FONT_LARGE=font_large, FONT_SMALL=font_small, 		$
            EDIT_FONTS=edit_fonts

		;**** If menu button clicked, tell FORTRAN to stop ****

    if gomenu eq 1 then begin
 	printf, pipe, 1
 	goto, LABELEND
    endif else begin
 	printf, pipe, 0
    endelse


		;**** If cancel selected then goto 100 ****

    if lpend eq 1 then goto, LABEL100

		;**** Fortran processing now in progress. Get ****
		;**** date and time for this run.	      ****

    date = xxdate()

		;**** Create header for output. ****

    header = adasrel+adasprog+' DATE: '+date(0)+' TIME: '+date(1)

		;**** Check FORTRAN still running ****

    if find_process(pid) eq 0 then goto, LABELEND

LABEL300:
		;**** Check FORTRAN still running ****

    if find_process(pid) eq 0 then goto, LABELEND

		;************************************************
		;**** Communicate with d6spf1 in fortran and ****
		;**** invoke user interface widget for       ****
		;**** Output options                         ****
		;************************************************

		;**** Is it possible to have a passing file? ****

    if outval.gcfout eq 1 then begin
	if lsnull eq 1 then outval.gcfout=0
    endif

		;**** Decide availability of output plots ****

    if lsnull eq 0 then begin
	outval.lcont_avail = 1
    endif else begin
	outval.lcont_avail = 0
	outval.ldef3 = 0
    endelse
    prbchk = strpos(classstring, 'PRB')
    prcchk = strpos(classstring, 'PRC')
    pltchk = strpos(classstring, 'PLT')
    if (prbchk(0) ne -1 or prcchk(0) ne -1 or pltchk(0) ne -1) then begin
	outval.lpower_avail = 1
	outval.lexcpm_avail = 1
    endif else begin
	outval.lpower_avail = 0
	outval.lexcpm_avail = 0
	outval.ldef2 = 0
	outval.ldef4 = 0
    endelse
    outval.hrdout = 0
    d6spf1, pipe, lpend, outval, dsfull, lsnull, bitfile, gomenu,$
	    graph_out, pid, DEVLIST=devlist, FONT_LARGE=font_large,	 $
            FONT_SMALL=font_small

                ;**** Extra check to see whether FORTRAN is still there ****

    if find_process(pid) eq 0 then goto, LABELEND

		;**** If menu button clicked, tell FORTRAN to stop ****

    if gomenu eq 1 then begin
	printf, pipe, 1
        outval.texmes = ''
        outval.gcfmes = ''
	goto, LABELEND
    endif else begin
	printf, pipe, 0
    endelse

		;**** If cancel selected then erase output ****
		;**** messages and goto 5.		   ****

    if lpend eq 1 then begin
        outval.texmes = ''
        outval.gcfmes = ''
        goto, LABEL5
    endif 

                ;**** Create an information widget ****

    if outval.grpout eq 1 then begin
        widget_control, /hourglass
        widgetbase = widget_base(/column, xoffset=300, yoffset=200,	$
        title = "ADAS406: INFORMATION")
        lab0 = widget_label(widgetbase, value='')
        lab1 = widget_label(widgetbase, value='')
        lab2 = widget_label(widgetbase, font=font_large,  		$
        value=" Retrieving data from files - "+				$
        "please wait      ")
        lab3 = widget_label(widgetbase, value='')
        lab4 = widget_label(widgetbase, value='')
    endif else begin
	widgetbase = 0L
    endelse

		;*************************************************
		;**** If text output has been requested write ****
		;**** header to pipe for XXADAS to read.      ****
		;*************************************************

    if (outval.texout eq 1) then begin
        printf, pipe, header, format ='(A)'
    endif

		;************************************************
                ;**** If graphical output requested          ****
		;**** Communicate with fortran and           ****
		;**** invoke widget Graphical output         ****
		;************************************************

    if outval.grpout eq 1 then begin

		;**** Hardcopy output ****

        hrdout = outval.hrdout
        hardname = outval.hardname
        if outval.devsel ge 0 then device = devcode(outval.devsel)

                ;*** user title for graph ***

        utitle = outval.gtit1
       
        if (outval.lgraphno eq 0 and outval.lpion_avail eq 1) then begin
            widget_control, widgetbase, /realize
	    d6otg1, dsfull, pipe, utitle, outval.ldef1, outval.xmin1,	$
		    outval.xmax1, outval.ymin1, outval.ymax1, hrdout,	$
	            hardname, device, inval.year, inval.dyear,		$
                    header, bitfile, gomenu, widgetbase, FONT=font_large
	endif else if (outval.lgraphno eq 1)  and 			$
        (outval.lpower_avail eq 1) then begin
            widget_control, widgetbase, /realize
	    d6otg2, dsfull, pipe, utitle, outval.ldef2, outval.xmin2,	$
	            outval.xmax2, outval.ymin2, outval.ymax2, hrdout,	$
		    hardname, device, inval.year, inval.dyear,          $
                    header, bitfile, gomenu, widgetbase, FONT=font_large
	endif else if (outval.lgraphno eq 2)  and 			$
        (outval.lcont_avail eq 1) then begin
            widget_control, widgetbase, /realize
	    d6otg3, dsfull, pipe, utitle, outval.ldef3, outval.xmin3,	$
	            outval.xmax3, outval.ymin3, outval.ymax3, hrdout,	$
		    hardname, device, inval.year, inval.dyear,          $
                    header, procval.ispline, ctitle(procval.ispline),	$
                    cmpts(procval.ispline), bitfile, gomenu, 		$
                    widgetbase, FONT=font_large
	endif else if (outval.lgraphno eq 3)  and 			$
        (outval.lexcpm_avail eq 1) then begin
            widget_control, widgetbase, /realize
	    d6otg4, dsfull, pipe, utitle, outval.ldef4, outval.xmin3,	$
	            outval.xmax3, outval.ymin3, outval.ymax3, hrdout,	$
		    hardname, device, inval.year, inval.dyear,          $
                    header, bitfile, gomenu, widgetbase, FONT=font_large
	endif

		;**** If menu button clicked, tell FORTRAN to stop ****

        if gomenu eq 1 then begin
            outval.texmes = ''
            outval.gcfmes = ''
            printf, pipe, 1
            goto, LABELEND
        endif else begin
            printf, pipe, 0
        endelse
    endif
	
		;**** Now destroy info widget if it is still there ****

    chkflag = widget_info(widgetbase, /valid_id)
    if chkflag eq 1 then widget_control, widgetbase, /destroy
  

		;**** Back for more output options ****

    GOTO, LABEL300

LABELEND:


		;**** Save user defaults ****

    save, inval, procval, outval, filename=deffile


END
