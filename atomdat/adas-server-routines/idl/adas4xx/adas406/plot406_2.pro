; Copyright (c) 1996 Strathclyde University .
; SCCS info: Module @(#)$Header: /home/adascvs/idl/adas4xx/adas406/plot406_2.pro,v 1.2 2004/07/06 14:32:54 whitefor Exp $ Date $Date: 2004/07/06 14:32:54 $
;+
; PROJECT:
;       ADAS IBM MVS to DEC UNIX conversion
;
; NAME:
;       PLOT406_2
;
; PURPOSE:
;       Plot power function graphs for ADAS406.
;
; EXPLANATION:
;       This routine plots ADAS406 output for one or more plots (each
;       can contain a maximum of seven lines + totals).
;
; USE:
;       Use is specific to ADAS406.  See adas406_2_plot.pro for
;       example.
;
; INPUTS:
;
;	LDEF2	-	Integer; 1 if user specified axis limits to 	
;			be used, 0 if default scaling to be used.
;
;	TOP	-	Integer; last line to plot
;
;	BOTTOM	-	Integer;first line to plot
;
;	X	-	Fltarr; x-values to plot
;
;	Y	-	2d Fltarr; y-values to plot (2nd dimension between
;			BOTTOM and TOP)
;       RIGHTSTRING  -  String; left hand column of title to side of graph
;                       ('INDEX')
;
;       RIGHTSTRING2 -  String; middle column of title to side of graph
;                       ('DESIGNATION')
;
;       RIGHTSTRING2 -  String; right hand column of title to side of graph
;                       ('FRACTION')
;
;       INTTIME -  	String; integration time heading
;
;       TITLE   -       String; heading to go above graph
;
;       XMIN2   -       Float; user-defined x-axis minimum
;
;       XMAX2   -       Float; user-defined x-axis maximum
;
;       YMIN2   -       Float; user-defined y-axis minimum
;
;       YMAX2   -       Float; user-defined y-axis maximum
;
;
; OPTIONAL INPUTS:
;       None.
;
; OUTPUTS:
;       None.
;
; OPTIONAL OUTPUTS:
;       None.
;
; KEYWORD PARAMETERS:
;       FONT    - String; Name of font passed to graphical output
;                 widget.
;
; CALLS:
;       None.
;
; SIDE EFFECTS:
;       None.
;
; CATEGORY:
;       Adas system.
;
; WRITTEN:
;       William Osborn, Tessella Support Services plc,  12th June 1996
;
; MODIFIED:
;       1.1     William Osborn    
;		First release
;	1.2     William Osborn
;               Added check for new environment variable VERY_SMALL
;               If this is set to the string "YES" (preferably in the
;               .adas_setup file) then the font size used on the graphs
;               is reduced and the positioning of labels adjusted
;               slightly to make things fit on a very small screen.
;
; VERSION:
;	1.1	12-06-96
;	1.2	11-10-96
;
;-
;----------------------------------------------------------------------------

;*** ANNO: Builds up a linked list of annotations made, storing the positions,
;***       checks that the requested annotation does not overwrite another
;***	   and writes it. If it does overwrite another then the procedure
;***       recursively calls itself with an offset until a place can be found

PRO plot406_2, ldef2, top, bottom, x, y, rightstring, rightstring2, 	$
               rightstring3, inttime, title, xmin2, xmax2, ymin2, ymax2

    COMMON Global_lw_data, left, right, tp, bot, grtop, grright

                ;****************************************************
                ;**** Suitable character size for current device ****
                ;**** Aim for 60 characters in y direction.      ****
                ;****************************************************

    charsize = (!d.y_vsize/!d.y_ch_size)/60.0
    small_check = GETENV('VERY_SMALL')
    if small_check eq 'YES' then charsize=charsize*0.8

                ;**** Initialise titles ****

    xtitle = 'ELECTRON TEMPERATURE (eV)'
    ytitle = 'ENERGY FUNCTION (J cm!E3!N)'
    righthead = '----- METASTABLE ASSIGNMENTS -----!C!C'+		$
                '  INDEX     DESIGNATION   INIT. FRAC.'
    erase

                ;**** How many points to plot ****

    npts = size(x)
    npts = npts(1)

		;**** Find x and y ranges for auto scaling,        ****
                ;**** check x and y in range for explicit scaling. ****

    makeplot = 1
    style = 0
    ystyle = 0
    yplot = y(*, bottom:top)

    if ldef2 eq 0 then begin

                ;**** identify values in the valid range ****
                ;**** plot routines only work within ****
                ;**** single precision limits.       ****

	xvals = where (x gt 1.0e-37 and x lt 1.0e37)
	ypvals = where (yplot gt 1.0e-37 and yplot lt 1.0e37)
	ymvals = where (yplot lt -1.0e-37 and yplot gt -1.0e37)
	if xvals(0) gt -1 then begin
	    maxx = max(x(xvals))
	    minx = min(x(xvals))
	endif else begin
	    makeplot = 0
	endelse
	if ypvals(0) gt -1 or ymvals(0) gt -1 then begin
            if ypvals(0) gt -1 and ymvals(0) eq -1 then begin
	        maxy = max(yplot(ypvals))
	        miny = min(yplot(ypvals))
	    endif else if ypvals(0) eq -1 and ymvals(0) gt -1 then begin
		maxy = max(-y(ymvals))
                miny = min(-y(ymvals))
            endif else begin
                maxy = max([max(yplot(ypvals)), max(-y(ymvals))])
                miny = min([min(yplot(ypvals)), min(-y(ymvals))])
            endelse
	endif else begin
	    makeplot = 0
	endelse
	if makeplot eq 1 then begin
	    if miny le 1.0e-36 then begin
		ystyle = 1
		miny = 1.0e-36
		maxy = 1.2*maxy
	    endif else begin
		ystyle = 0
	    endelse
	endif
	style = 0
    endif else begin
        minx = xmin2
	maxx = xmax2
	miny = ymin2
	maxy = ymax2
	xvals = where(x gt minx and x lt maxx)
	ypvals = where(yplot gt miny and yplot lt maxy)
	ymvals = where(-yplot gt miny and -yplot lt maxy)
	if xvals(0) eq -1 or (ypvals(0) eq -1 and ymvals(0) eq -1) then begin
	    makeplot = 0
	endif else begin
	    makeplot = 1
	endelse
	style = 1
        ystyle = 1
    endelse

    top_handle = handle_create()

    if makeplot eq 1 then begin

        p_out_of_range = bytarr(top-bottom+1)

                ;**** Set up log-log plotting axes ****

	plot_oo, [minx, maxx], [miny, maxy], /nodata, ticklen=1.0,	$
                 position=[left, bot, grright, grtop],			$
                 xtitle=xtitle, ytitle=ytitle, xstyle=style, 		$
		 ystyle=ystyle, charsize=charsize

                ;*********************************
                ;**** Make and annotate plots ****
                ;*********************************

		;**** Plot positive values ****

	if ypvals(0) gt -1 then begin
 	    for i=bottom, top do begin
	        ymaxchk = max(y(*,i))		;check this line is okay to 
						;draw
	        if ymaxchk le 1.0e-36 then begin
		    p_out_of_range(i-bottom) = 1B
	        endif else begin
 	            oplot, x, y(*,i), linestyle=2
     
                ;**** Find suitable point for annotation ****

	            if (x(npts-1) ge minx) and (x(npts-1) le maxx) and 	$
                    (y(npts-1,i) gt miny) and (y(npts-1,i) le maxy) then begin
		        iplot = npts - 1
	            endif else begin
		        iplot = -1
		        for id = 0, npts-2 do begin
		            if (x(id) ge minx) and (x(id) lt maxx) and	$
		            (y(id,i) gt miny) and (y(id,i) le maxy) then begin
			        iplot = id
		            endif
	                endfor
	            endelse
        
                ;**** Annotate plots with level numbers ****

 	            if iplot ne -1 then begin
			anno, x(iplot), y(iplot,i), 	$
			    string(i+1, format='(i2)')+'+', top_handle
	            endif
	        endelse
	    endfor
	endif

		;**** Plot negative values ****

	if ymvals(0) gt -1 then begin
 	    for i=bottom, top do begin
	        ymaxchk = max(-y(*,i))		;check this line is okay to 
						;draw
	        if ymaxchk le 1.0e-36 and p_out_of_range(i-bottom) eq 1B then begin
		    print, '******************************* D6OTG2 MESSAGE '+$
                           '*******************************'
		    print, 'METASTABLE: ', i+1, ' NO GRAPH WILL BE OUTPUT ' +$
		           'BECAUSE:'
		    print, 'ALL VALUES ARE BELOW THE CUTOFF OF 1.000E-36'
		    print, '******************************* END OF MESSAGE '+$
                           '*******************************'
	        endif else begin
 	            oplot, x, -y(*,i), linestyle=5
     
                ;**** Find suitable point for annotation ****

	            if (x(npts-1) ge minx) and (x(npts-1) le maxx) and 	$
                    (-y(npts-1,i) gt miny) and (-y(npts-1,i) le maxy) then begin
		        iplot = npts - 1
	            endif else begin
		        iplot = -1
		        for id = 0, npts-2 do begin
		            if (x(id) ge minx) and (x(id) lt maxx) and	$
		            (-y(id,i) gt miny) and (-y(id,i) le maxy) then begin
			        iplot = id
		            endif
	                endfor
	            endelse
        
                ;**** Annotate plots with level numbers ****

 	            if iplot ne -1 then begin
		        anno, x(iplot), -y(iplot,i), 			$
                            string(i+1, format='(i2)')+'-', top_handle
	            endif
	        endelse
	    endfor
	endif

    endif else begin			;no plot possible
        xyouts, 0.2, 0.5, /normal, charsize=charsize*1.5,               $
                '---- No data lies within range ----'
    endelse

                ;**** Output title above graphs ****
    
    xyouts, 0.1, 0.9, title, charsize=charsize, /normal

                ;**** Output titles to right of graphs ****


    xyouts, 0.72, 0.78, inttime, charsize=charsize, /normal
    xyouts, 0.72, 0.72, righthead, charsize=charsize, /normal
    xyouts, 0.74, 0.64, rightstring, charsize=charsize, /normal
    xyouts, 0.80, 0.64, rightstring2, charsize=charsize, /normal
    xyouts, 0.90, 0.64, rightstring3, charsize=charsize, /normal

    handle_free, top_handle

END
