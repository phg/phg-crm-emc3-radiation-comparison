; Copyright (c) 1996, Strathclyde University.
; SCCS info: Module @(#)$Header: /home/adascvs/idl/adas4xx/adas406/adas406_4_plot.pro,v 1.3 2004/07/06 10:43:50 whitefor Exp $ Date $Date: 2004/07/06 10:43:50 $
;+
; PROJECT:
;       ADAS IBM MVS to UNIX conversion
;
; NAME:
;       ADAS406_4_PLOT
;
; PURPOSE:
;       Generates ADAS406 total power function graphical output.
;
; EXPLANATION:
;       This routine creates a window for the display of graphical
;       output. A separate routine PLOT406_4 actually plots the
;       graph.
;
; USE:
;       This routine is specific to the total power function plotting
;	section	of ADAS406.
;
; INPUTS:
;       DSFULL  - String; Name of data file
;
;       UTITLE  - String; Optional comment by user added to graph title.
;
;       LDEF4   - Integer; 1 - use user entered graph scales
;                          0 - use default axes scaling
;
;	NMSUM	- Integer; no. of metastables (output curves)
;
;	X	- Fltarr; electron temperatures - x-values
;
;	Y	- 2D Fltarr; power functions - y-values
;
;       HRDOUT - Integer; 1 if hardcopy output activated, 0 if not.
;
;       HARDNAME- String; Filename for harcopy output.
;
;       DEVICE  - String; IDL name of hardcopy output device.
;
;       HEADER  - String; ADAS version number header to include in graph.
;
;	SPECIES - String; element name to be used in graph title
;
;	YEAR	- String; user's selected year for data
;
;	DYEAR	- String; user's selected default year for data (if any)
;
;       XMIN2   - String; Lower limit for x-axis of graph, number as string.
;
;       XMAX2   - String; Upper limit for x-axis of graph, number as string.
;
;       YMIN2   - String; Lower limit for y-axis of graph, number as string.
;
;       YMAX2   - String; Upper limit for y-axis of graph, number as string.
;
;	TIME4	- Double; integration time
;
;       BITFILE - String; the path to the directory containing bitmaps
;                 for the 'escape to series menu' button.
;
; OPTIONAL INPUTS:
;       None.
;
; OUTPUTS:
;       GOMENU  - Int; flag - set to 1 if user has selected 'escape direct
;                 to series menu', otherwise 0
;
; OPTIONAL OUTPUTS:
;       None.
;
; KEYWORD PARAMETERS:
;       FONT    - String; The name of a font to use for text in the
;                 graphical output widget.
;
; CALLS:
;       CW_ADAS_GRAPH   Graphical output widget.
;       PLOT406_4       Make plots to an output device for 406(power fn).
;       XMANAGER
;
; SIDE EFFECTS:
;       This routine uses a common block to maintain its state 
;	PLOT406_4_BLK.
;
;       One other routine is included in this file;
;       ADAS406_4_PLOT_EV Called via XMANAGER during widget management.
;
; CATEGORY:
;       Adas system.
;
; WRITTEN:
;       William Osborn, Tessella Support Services plc,  24th June 1996
;
; MODIFIED:
;       1.1     William Osborn
;               First version
;	1.2     William Osborn
;               Added check for new environment variable VERY_SMALL
;               If this is set to the string "YES" (preferably in the
;               .adas_setup file) then the font size used on the graphs
;               is reduced and the positioning of labels adjusted
;               slightly to make things fit on a very small screen.
;       1.3     Martin O'Mullane
;               The variables 'first' and 'last' were not set.
;
; VERSION:
;       1.1     24-06-96
;	1.2	11-10-96
;	1.3	29-07-2003
;
;-
;----------------------------------------------------------------------------

PRO adas406_4_plot_ev, event

    COMMON plot406_4_blk, action, plotdev, plotfile, fileopen, win, 	$
                          iplot, nplot, data, nsum, gomenu

    newplot = 0
    print = 0
    done = 0
    first = iplot
    last = iplot
    
                ;****************************************
                ;**** Set graph and device requested ****
                ;****************************************

    CASE event.action OF

        'print'    : begin
            newplot = 1
            print = 1
        end

        'printall' : begin
            newplot = 1
            print = 1
        end

        'done'     : begin
            if fileopen eq 1 then begin
                set_plot, plotdev
                device, /close_file
            end
            set_plot,'X'
            widget_control, event.top, /destroy
            done = 1
        end

        'bitbutton' : begin
            if fileopen eq 1 then begin
                set_plot, plotdev
                device, /close_file
            end
            set_plot,'X'
            widget_control, event.top, /destroy
            done = 1
            gomenu = 1
        end
             
    END

                ;*******************************
                ;**** Make requested plot/s ****
                ;*******************************

    if done eq 0 then begin

                ;**** Set graphics device ****

        if print eq 1 then begin
            set_plot, plotdev
            if fileopen eq 0 then begin
                fileopen = 1
                device, filename=plotfile
                device, /landscape
            endif
        endif else begin
            set_plot,'X'
            wset, win
        endelse

                ;**** Draw graphics ****

        if newplot eq 1 then begin
            for i = first, last do begin
                plot406_4, data.ldef4, data.x, data.y,			$
			   data.inttime,				$
			   data.title, data.xmin2, data.xmax2, 		$
			   data.ymin2, data.ymax2
	        if print eq 1 then begin
                    message = 'Plot  written to print file.'
                    grval = {WIN:0, MESSAGE:message}
                    widget_control, event.id, set_value=grval
                endif
	    endfor
        endif
    endif

END

;----------------------------------------------------------------------------
                                                                
PRO adas406_4_plot, dsfull, utitle, ldef4, nmsum, x, y,			$
                    hrdout, hardname, device, header, 			$
                    species, year, dyear, xmin2, xmax2, ymin2, ymax2,	$
                    time4, bitfile, gomenu, FONT=font

    COMMON plot406_4_blk, action, plotdev, plotfile, fileopen, win, 	$
                          iplot, nplot, data, nsum, gomenucom

    ngpic = 7			;max no. of lines per plot

                ;**** Copy input values to common ****

    plotdev = device
    plotfile = hardname
    fileopen = 0
    nsum = nmsum
    gomenucom = gomenu

		;***********************
		;**** Only one plot ****
		;***********************

    nplot = 1

                ;************************************
                ;**** Create general graph titles****
                ;************************************

    title = "ENERGY EXCESS VS. ELECTRON TEMPERATURE"
    if ( strtrim(strcompress(utitle),2)  ne ' ' ) then begin
        title = title + ': ' + strupcase(strtrim(utitle,2))
    endif
    small_check = GETENV('VERY_SMALL')
    if small_check eq 'YES' then begin
        title =  title + '!C!CADAS    : ' + strupcase(header)
        if strcompress(dyear, /remove_all) eq '' then defyear = 'NONE' else $
          defyear = strcompress(dyear, /remove_all)
        year = strcompress(year, /remove_all)
        title =  title + '!C!CFILE     : ' + strcompress(dsfull) + 	$
          '   SPECIES: ' + strupcase(species) + '   YEAR: ' + year +	$
          '   DEFAULT YEAR: '+ defyear
        title =  title + '!C!CKEY     : (DOTTED - NEGATIVE) (SOLID - POSITIVE)'
    endif else begin
        title =  title + '!C!CADAS    : ' + strupcase(header)
        if strcompress(dyear, /remove_all) eq '' then defyear = 'NONE' else $
          defyear = strcompress(dyear, /remove_all)
        year = strcompress(year, /remove_all)
        title =  title + '!CFILE     : ' + strcompress(dsfull) + 		$
          '   SPECIES: ' + strupcase(species) + '   YEAR: ' + year +		$
          '   DEFAULT YEAR: '+ defyear
        title =  title + '!CKEY     : (DOTTED - NEGATIVE) (SOLID - POSITIVE)'
    endelse

    inttime = 'INTEGRATION TIME (sec) = ' + string(time4,format='(e10.2)')

                ;*************************************
                ;**** Create graph display widget ****
                ;*************************************

    graphid = widget_base(TITLE='ADAS406 GRAPHICAL OUTPUT', 		$
                          XOFFSET=1,YOFFSET=1)
    device, get_screen_size = scrsz
    xwidth=scrsz(0)*0.75
    yheight=scrsz(1)*0.75
    if nplot gt 1 then multiplot=1 else multiplot=0
    bitval = bitfile + '/menu.bmp'
    cwid = cw_adas_graph(graphid, print=hrdout, FONT=font,		$
                         xsize=xwidth, ysize=yheight, 			$
		         multiplot=multiplot, bitbutton=bitval)

                ;**** Realize the new widget ****

    widget_control, graphid, /realize

                ;**** Get the id of the graphics area ****

    widget_control, cwid, get_value=grval
    win = grval.win

                ;*******************************************
                ;**** Put the graphing data into common ****
                ;*******************************************

    data = {	X		:	x,				$
		Y		:	y,				$
		XMIN2		:	xmin2,				$
		XMAX2		:	xmax2,				$
		YMIN2		:	ymin2,				$
		YMAX2		:	ymax2,				$
		TITLE		:	title,				$
		INTTIME		:	inttime,			$
		LDEF4		:	ldef4				}

                ;**** Initialise to plot 0 ****
    iplot = 0
    wset, win

    plot406_4, ldef4, x, y, inttime, title, xmin2, xmax2, ymin2, ymax2

                ;***************************
                ;**** make widget modal ****
                ;***************************

    xmanager, 'adas406_4_plot', graphid, /modal, /just_reg,		$
              event_handler='adas406_4_plot_ev'

    gomenu = gomenucom
                 
END
