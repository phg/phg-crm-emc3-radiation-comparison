; Copyright (c) 1996, Strathclyde University.
; SCCS info: Module @(#)$Header: /home/adascvs/idl/adas4xx/adas406/d6otg3.pro,v 1.1 2004/07/06 13:19:17 whitefor Exp $ Date $Date: 2004/07/06 13:19:17 $
;+
; PROJECT:
;       ADAS IBM MVS to UNIX conversion
;
; NAME:
;       D6OTG3
;
; PURPOSE:
;       Communication with ADAS406 FORTRAN via UNIX pipe and
;       graphics output.
;
; EXPLANATION:
;       The routine begins by reading information from the ADAS406
;	FORTRAN routine D6OTG3.FOR via a UNIX pipe.  Then the IDL graphical
;       output routine for ADAS406 contribution fn. plots is invoked.
;
; USE:
;       This routine is specific to adas406.
;
; INPUTS:
;       DSFULL   - Script file name
;
;       PIPE     - The IDL unit number of the bi-directional pipe to the
;                  ADAS401 FORTRAN process.
;
;       UTITLE   - Optional comment by user
;
;       GRPSCAL  - Integer; 0 if default scaling required 1 if user
;                  specified scaling to be used.
;
;       XMIN     - String; User specified x-axis minimum, number as string.
;
;       XMAX     - String; User specified x-axis maximum, number as string.
;
;       YMIN     - String; User specified y-axis minimum, number as string.
;
;       YMAX     - String; User specified y-axis maximum, number as string.
;
;       HRDOUT   - Integer; 0 if no hardcopy required, 1 if hardcopy.
;
;       HARDNAME - String; file name for hardcopy output.
;
;       DEVICE   - String; IDL name for hardcopy output device.
;
;	YEAR	 - String; user's selected year for data.
;
;	DYEAR	 - String; user's selected default year for data (if any).
;
;       HEADER   - ADAS version header information for inclusion in the
;                  graphical output.
;
;       ISPLINE - Integer; the selected line from the script file.
;
;       CTITLE  - String array: Any information given in script file on
;                 lines to be analysed.
;
;       CMPTS   - Int array: no. components for each line in script file
;
;       BITFILE - String; the path to the directory containing bitmaps
;                 for the 'escape to series menu' button.
;
;       WIDGETBASE - Long integer; widget id of the information base
;                    generated whilst pipe comms are in progress.
;
; OPTIONAL INPUTS:
;       None.
;
; OUTPUTS:
;       GOMENU  - Int; flag - set to 1 if user has selected 'escape direct
;                 to series menu', otherwise 0
;
; OPTIONAL OUTPUTS:
;       None.
;
; KEYWORD PARAMETERS:
;       FONT    - String; Name of the font to be used for the graphical
;                 output widget.
;
; CALLS:
;       ADAS406_3_PLOT	ADAS406 contribution function graphical output.
;	XXELEM          Converts electron number to element name
;
; SIDE EFFECTS:
;       This routine reads information from FORTRAN via a UNIX pipe.
;
; CATEGORY:
;       Adas system.
;
; WRITTEN:
;       William Osborn, Tessella Support Services plc, 21st June 1996
;
; MODIFIED:
;       1.1     William Osborn
;               First version
;
; VERSION:
;       1.1     21-06-96
;
;-
;-----------------------------------------------------------------------------



PRO d6otg3, dsfull, pipe, utitle, grpscal, xmin, xmax, ymin, ymax,	$
            hrdout, hardname, device, year, dyear, header, 		$
            ispline, ctitle, cmpts, bitfile, gomenu, widgetbase,	$
            FONT=font

                ;**** Set defaults for keywords ****

    IF NOT (KEYWORD_SET(font)) THEN font = ''

                ;**** Declare variables for input ****

    input = 0
    fdum = 0.0
    sdum = ' '
                                                        
                ;********************************
                ;**** Read data from fortran ****
                ;********************************

    readf, pipe, input
    itmax = input				;no. of electron temps.
    xvals = fltarr(itmax)
    for i=0, (itmax-1) do begin
	readf, pipe, fdum
 	xvals(i) = fdum
    endfor
    readf, pipe, input
    nmsum = input
    yvals = fltarr(itmax, nmsum)
    zvals = fltarr(itmax)
    ipline = strarr(nmsum)			;line indices
    for j=0, (nmsum-1) do begin
        for i=0, (itmax-1) do begin
	    readf, pipe, fdum
            yvals(i,j) = fdum
	endfor
    endfor
    readf, pipe, input
    nmsum2 = input
    poptit = strarr(nmsum2)			;designation strings
    for i=0, (nmsum2-1) do begin
	readf, pipe, format='(a)', sdum
        poptit(i) = sdum
    endfor
    readf, pipe, input
    xxelem, input, output	
    species = output				;element name for title
    rightstring5 = ''
    for i=0, (nmsum-1) do begin			;information for plot
        readf, pipe, format='(a)', sdum
        rightstring5 = rightstring5 + ' ' + sdum
        readf, pipe, format='(a)', sdum
        ipline(i) = sdum
    endfor
    for i=0,(itmax-1) do begin
	readf, pipe, fdum
        zvals(i) = fdum
    endfor
    readf,pipe,fdum
    time4 = fdum				;time information
    sy0 = dblarr(nmsum2)
    for i=0,nmsum2-1 do begin
 	readf,pipe,fdum
        sy0(i)=fdum
    endfor

                ;**** Destroy information widget ****

    wait, 1
    widget_control, widgetbase, /destroy
 
                ;***********************
                ;**** Plot the data ****
                ;***********************

    adas406_3_plot, dsfull, utitle, grpscal, nmsum, xvals, yvals, zvals,$
                    hrdout, hardname, device, header, poptit, 		$
 	    	    species, year, dyear, xmin, xmax, ymin, ymax,	$
                    ispline, ctitle, cmpts, ipline, rightstring5, 	$
                    time4, sy0, bitfile, gomenu, FONT=font
                                                   
END
