; Copyright (c) 1996 Strathclyde University .
; SCCS info: Module @(#)$Header: /home/adascvs/idl/adas4xx/adas406/plot406_1.pro,v 1.2 2004/07/06 14:32:50 whitefor Exp $ Date $Date: 2004/07/06 14:32:50 $
;+
; PROJECT:
;       ADAS IBM MVS to DEC UNIX conversion
;
; NAME:
;       PLOT406_1
;
; PURPOSE:
;       Plot ion fraction graphs for ADAS406.
;
; EXPLANATION:
;       This routine plots ADAS406 output for one or more plots (each
;       can contain a maximum of seven lines).
;
; USE:
;       Use is specific to ADAS406.  See adas406_1_plot.pro for
;       example.
;
; INPUTS:
;
;	LDEF1	-	Integer; 1 if user specified axis limits to 	
;			be used, 0 if default scaling to be used.
;
;	TOP	-	Integer; last line to plot
;
;	BOTTOM	-	Integer;first line to plot
;
;	X	-	Fltarr; x-values to plot
;
;	Y	-	2d Fltarr; y-values to plot (2nd dimension between
;			BOTTOM and TOP)
;
;	RIGHTSTRING  -  String; left hand column of title to side of graph
;			('INDEX')
;
;	RIGHTSTRING2 -  String; middle column of title to side of graph
;			('DESIGNATION')
;
;	RIGHTSTRING3 -  String; right hand column of title to side of graph
;			('INIT. FRAC.')
;
;	INTTIME -       String; label giving the integration time
;
;	TITLE	-	String; heading to go above graph
;
;	XMIN1	-	Float; user-defined x-axis minimum
;
;	XMAX1	-	Float; user-defined x-axis maximum
;
;	YMIN1	-	Float; user-defined y-axis minimum
;
;       YMAX1   -       Float; user-defined y-axis maximum
;
; OPTIONAL INPUTS:
;       None.
;
; OUTPUTS:
;       None.
;
; OPTIONAL OUTPUTS:
;       None.
;
; KEYWORD PARAMETERS:
;       FONT    - String; Name of font passed to graphical output
;                 widget.
;
; CALLS:
;       None.
;
; SIDE EFFECTS:
;       None.
;
; CATEGORY:
;       Adas system.
;
; WRITTEN:
;       William Osborn, Tessella Support Services plc,  11th June 1996
;
; MODIFIED:
;       1.1     William Osborn
;		First version
;	1.2     William Osborn
;               Added check for new environment variable VERY_SMALL
;               If this is set to the string "YES" (preferably in the
;               .adas_setup file) then the font size used on the graphs
;               is reduced and the positioning of labels adjusted
;               slightly to make things fit on a very small screen.
;
; VERSION:
;	1.1	11-06-96
;	1.2	11-10-96
;
;-
;----------------------------------------------------------------------------

PRO plot406_1, ldef1, top, bottom, x, y, rightstring, rightstring2, 	$
               rightstring3, inttime, title, xmin1, xmax1, ymin1, ymax1

    COMMON Global_lw_data, left, right, tp, bot, grtop, grright

                ;****************************************************
                ;**** Suitable character size for current device ****
                ;**** Aim for 60 characters in y direction.      ****
                ;****************************************************

    charsize = (!d.y_vsize/!d.y_ch_size)/60.0
    small_check = GETENV('VERY_SMALL')
    if small_check eq 'YES' then charsize=charsize*0.8

                ;**** Initialise titles ****

    xtitle = 'ELECTRON TEMPERATURE (eV)'
    ytitle = 'N!DINDEX!N / N!DTOTAL!N'
    righthead = '----- METASTABLE ASSIGNMENTS -----!C!C'+		$
                '  INDEX     DESIGNATION   INIT. FRAC.'
    erase

                ;**** How many points to plot ****

    npts = size(x)
    npts = npts(1)

		;**** Find x and y ranges for auto scaling,        ****
                ;**** check x and y in range for explicit scaling. ****

    makeplot = 1
    style = 0
    ystyle = 0
    yplot = y(*, bottom:top)

    if ldef1 eq 0 then begin

                ;**** identify values in the valid range ****
                ;**** plot routines only work within ****
                ;**** single precision limits.       ****

	xvals = where (x gt 1.0e-37 and x lt 1.0e37)
	yvals = where (yplot gt 1.0e-37 and yplot lt 1.0e37)
	if xvals(0) gt -1 then begin
	    maxx = max(x(xvals))
	    minx = min(x(xvals))
	endif else begin
	    makeplot = 0
	endelse
	if yvals(0) gt -1 then begin
	    maxy = max(yplot(yvals))
	    miny = min(yplot(yvals))
	endif else begin
	    makeplot = 0
	endelse
	if makeplot eq 1 then begin
	    if miny le 1.0e-36 then begin
		ystyle = 1
		miny = 1.0e-36
	    endif else begin
		ystyle = 0
	    endelse
	endif
	style = 0
    endif else begin
        minx = xmin1
	maxx = xmax1
	miny = ymin1
	maxy = ymax1
	xvals = where(x gt minx and x lt maxx)
	yvals = where(yplot gt miny and yplot lt maxy)
	if xvals(0) eq -1 or yvals(0) eq -1 then begin
	    makeplot = 0
	endif else begin
	    makeplot = 1
	endelse
	style = 1
        ystyle = 1
    endelse

    if makeplot eq 1 then begin

                ;**** Set up log-log plotting axes ****

	plot_oo, [minx, maxx], [miny, maxy], /nodata, ticklen=1.0,	$
                 position=[left, bot, grright, grtop],			$
                 xtitle=xtitle, ytitle=ytitle, xstyle=style, 		$
		 ystyle=ystyle, charsize=charsize

                ;*********************************
                ;**** Make and annotate plots ****
                ;*********************************

 	for i=bottom,top do begin
	    ymaxchk = max(y(*,i))		;check this line is okay to 
						;draw
	    if ymaxchk le 1.0e-36 then begin
		print, '******************************* D6OTG1 MESSAGE '+$
                       '*******************************'
		print, 'METASTABLE: ', i+1, ' NO GRAPH WILL BE OUTPUT ' +$
		       'BECAUSE:'
		print, 'ALL VALUES ARE BELOW THE CUTOFF OF 1.000E-36'
		print, '******************************* END OF MESSAGE '+$
                       '*******************************'
	    endif else begin
                oplot, x, y(*,i), linestyle=2

                ;**** Find suitable point for annotation ****

                if (miny eq 10^(!y.crange(0))) then begin
	            if (x(npts-1) ge minx) and (x(npts-1) le maxx) and 	$
                    (y(npts-1,i) gt miny) and 				$
                    (y(npts-1,i) lt (10^(!y.crange(1)))) then begin
		        iplot = npts - 1
	            endif else begin
		        iplot = -1
		        for id = 0, npts-2 do begin
		            if (x(id) ge minx) and (x(id) le maxx) and	$
		            (y(id,i) gt miny) and (y(id,i) le maxy) then begin
			        iplot = id
		            endif
	                endfor
	            endelse
                endif else begin
                    if (x(npts-1) ge minx) and (x(npts-1) le maxx) and  $
                    (y(npts-1,i) ge miny) and                           $
                    (y(npts-1,i) lt (10^(!y.crange(1)))) then begin
                       iplot = npts - 1
                    endif else begin
                        iplot = -1
                        for id = 0, npts-2 do begin
                            if (x(id) ge minx) and (x(id) le maxx) and  $
                            (y(id,i) ge miny) and                       $
                            (y(id,i) le maxy) then begin
                               iplot = id
                            endif
                        endfor
                    endelse
                endelse
    
                ;**** Annotate plots with level numbers ****

 	        if iplot ne -1 then begin
		    xyouts, x(iplot), y(iplot,i), 			$
                    string(i+1, format='(i2)'), alignment=0.5
	        endif
	    endelse
	endfor
    endif else begin			;no plot possible
        xyouts, 0.2, 0.5, /normal, charsize=charsize*1.5,               $
                '---- No data lies within range ----'
    endelse

                ;**** Output title above graphs ****
    
    xyouts, 0.1, 0.9, title, charsize=charsize, /normal

                ;**** Output titles to right of graphs ****

    xyouts, 0.72, 0.78, inttime, charsize=charsize, /normal
    xyouts, 0.72, 0.72, righthead, charsize=charsize, /normal
    xyouts, 0.74, 0.64, rightstring, charsize=charsize, /normal
    xyouts, 0.80, 0.64, rightstring2, charsize=charsize, /normal
    xyouts, 0.90, 0.64, rightstring3, charsize=charsize, /normal

END
