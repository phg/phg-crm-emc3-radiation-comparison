; Copyright (c) 1996, Strathclyde University.
; SCCS Info: Module @(#)$Header: /home/adascvs/idl/adas4xx/adas406/d6spf1.pro,v 1.2 2004/07/06 13:20:21 whitefor Exp $ Date $Date: 2004/07/06 13:20:21 $
;
;+
; PROJECT:
;       ADAS IBM MVS to UNIX conversion
;
; NAME:
;	D6SPF1
;
; PURPOSE:
;	IDL user interface and communications with ADAS406 FORTRAN
;	process via pipe.
;
; EXPLANATION:
;	First this routine reads some information from ADAS406 FORTRAN
;	via the UNIX pipe.  Next the 'Output Options' part of the
;	interface is invoked.  Finally the results of the user's
;	interactions are written back to the FORTRAN via the pipe.
;	Communications are with the FORTRAN subroutine D6SPF1.
;
; USE:
;	The use of this routine is specific to ADAS406, See adas406.pro.
;
; INPUTS:
;	PIPE	- The IDL unit number of the bi-directional pipe to the
;		  ADAS406 FORTRAN process.
;
;	DSFULL  - The name of the data set being analyzed.
;
;	VALUE	- A structure which determines the initial settings of
;		  the dataset selection widget.  The initial value is
;		  set in adas406.pro.  VALUE is passed un-modified
;		  through to cw_adas_in.pro, see that routine for a full
;		  description.
;
;       LSNULL  - Integer; flag showing whether a null script file has
;                 been selected (=1) or not (=0)
;
;       BITFILE - String; the path to the dirctory containing bitmaps
;                 for the 'escape to series menu' button.
;
;	PID	- Integer; the process id of the FORTRAN part of the code.
;
; OPTIONAL INPUTS:
;	None.
;
; OUTPUTS:
;	LPEND	- Integer; indicates user action on leaving output
;		  selections.  lpend is set to 1 if the user exited
;		  with the 'Cancel' button and 0 if the user exited
;		  with 'Done'.
;
;	VALUE	- On output the structure records the final settings of
;		  the output selection widget if the user pressed the
;		  'Done' button otherwise it is not changed from input.
;
;	GRAPH_OUT - Integer; flag showing whether graphical output
;		    is possible (1) or not (0).
;
;       GOMENU  - Int; flag - set to 1 if user has selected 'escape direct
;                 to series menu' , otherwise 0
;
; OPTIONAL OUTPUTS:
;	None.
;
; KEYWORD PARAMETERS:
;	DEVLIST	- A string array;  Each element of the array is the
;		  name of a hardcopy graphics output device.  This
;		  list must mirror the DEVCODE list.  DEVLIST is the
;		  list which the user will see.
;		  e.g ['Post-Script','HP-PCL','HP-GL']
;
;	FONT_LARGE- Supplies the large font to be used.
;
;	FONT_SMALL- Supplies the small font to be used.
;
; CALLS:
;	ADAS406_OUT	User interface - output options.
;
; SIDE EFFECTS:
;	This routine communicates with the ADAS406 FORTRAN process
;	via a UNIX pipe.
;
; CATEGORY:
;	Adas system.
;
; WRITTEN:
;       William Osborn, Tessella Support Services plc, 11-06-96
;
; MODIFIED: 
;	1.1	William Osborn
;		First version - written using d5spf1.pro v1.2
;	1.2	William Osborn
;		Replaced gof with gcf in outval structure
;
; VERSION:
;	1.1	11-06-96
;	1.2	04-07-96
;
;-
;-----------------------------------------------------------------------------

PRO d6spf1, pipe, lpend, value, dsfull, lsnull, bitfile, 	$
            gomenu, graph_out, pid, DEVLIST=devlist, FONT_LARGE=font,	$
            FONT_SMALL=font_small


                ;**** Set defaults for keywords ****

    IF NOT (KEYWORD_SET(font)) THEN font = ''
    IF NOT (KEYWORD_SET(font_small)) THEN font_small = ''

		;********************************
		;**** Read data from fortran ****
		;********************************

		;**************************************
		;**** Pop-up output options widget ****
		;**************************************

    adas406_out, value, dsfull, action, lsnull, bitfile,  		$
                 DEVLIST=devlist, FONT_LARGE=font, FONT_SMALL=font_small

		;*********************************************
		;**** Act on the output from the widget   ****
		;**** There are three    possible actions ****
		;**** 'Done', 'Cancel' and 'Menu'.        ****
		;*********************************************

    if action eq 'Done' then begin
        lpend = 0
    endif else if action eq 'Menu' then begin
	lpend = 0
	gomenu = 1
    endif else begin
        lpend = 1
    endelse

		;*******************************
		;**** Write data to fortran ****
		;*******************************

    printf, pipe, lpend

    if lpend eq 0 then begin

	printf, pipe, value.grpout
        if (value.grpout eq 1) then begin
	    if (value.lgraphno eq 0) then begin
	        if (value.lpion_avail eq 1) then begin
		    graph_out = 1
		    graphval = 1
		endif else begin
		    graph_out = 0
		endelse
	    endif else if (value.lgraphno eq 1) then begin
                if (value.lpower_avail eq 1) then begin
		    graph_out = 1
                    graphval = 2
                endif else begin
                    graph_out = 0
                endelse
	    endif else if (value.lgraphno eq 2) then begin
		if (value.lcont_avail eq 1) then begin
                    graph_out = 1
                    graphval = 3
                endif else begin
                    graph_out = 0
                endelse
	    endif else begin
		if (value.lexcpm_avail eq 1) then begin
                    graph_out = 1
                    graphval = 4
                endif else begin
                    graph_out = 0
                endelse
	    endelse
        endif else begin
	    graph_out = 0
	endelse

		;**** Extra check to see whether FORTRAN is still there ****

	if find_process(pid) eq 0 then return

	if graph_out eq 1 then printf, pipe, graphval

		;*** If text output requested tell FORTRAN ****
     
        printf, pipe, value.texout
        if (value.texout eq 1) then begin
            printf, pipe, value.texdsn
        endif

		;*** If passing file requested tell FORTRAN ***

        printf, pipe, value.gcfout
        if (value.gcfout eq 1) then begin
            printf, pipe, value.gcfdsn
        endif

    endif

		;**************************************************
		;**** Set messages and settings ready for when ****
		;**** output window is re-displayed.           ****
		;**************************************************

    if lpend eq 0 then begin
        if value.texout eq 1 then begin

		;**** If text output is requested enable append ****
		;**** for next time and update the default to   ****
		;**** the current text file.                    ****

            value.texapp=0
	    value.texdef = value.texdsn
            if value.texrep ge 0 then value.texrep = 0
      	    value.texmes = 'Output written to file.'
        endif

		;**** DO same for passing file ****

	if value.gcfout eq 1 then begin
	    value.gcfapp=0
	    if value.gcfrep ge 0 then value.gcfrep = 0
	    value.gcfmes = 'Output written to file.'
        endif

    endif

END
