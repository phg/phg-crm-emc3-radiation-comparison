; Copyright (c) 1996 Strathclyde University .
; SCCS info: Module @(#)$Header: /home/adascvs/idl/adas4xx/adas406/plot406_4.pro,v 1.3 2004/07/06 14:33:07 whitefor Exp $ Date $Date: 2004/07/06 14:33:07 $
;+
; PROJECT:
;       ADAS IBM MVS to DEC UNIX conversion
;
; NAME:
;       PLOT406_4
;
; PURPOSE:
;       Plot total energy excess graphs for ADAS406.
;
; EXPLANATION:
;       This routine plots ADAS406 output for one or more plots (each
;       can contain a maximum of seven lines + totals).
;
; USE:
;       Use is specific to ADAS406.  See adas406_4_plot.pro for
;       example.
;
; INPUTS:
;
;	LDEF4	-	Integer; 1 if user specified axis limits to 	
;			be used, 0 if default scaling to be used.
;
;	X	-	Fltarr; x-values to plot
;
;	Y	-	2d Fltarr; y-values to plot (2nd dimension 4)
;
;       INTTIME -  	String; integration time heading
;
;       TITLE   -       String; heading to go above graph
;
;       XMIN2   -       Float; user-defined x-axis minimum
;
;       XMAX2   -       Float; user-defined x-axis maximum
;
;       YMIN2   -       Float; user-defined y-axis minimum
;
;       YMAX2   -       Float; user-defined y-axis maximum
;
;
; OPTIONAL INPUTS:
;       None.
;
; OUTPUTS:
;       None.
;
; OPTIONAL OUTPUTS:
;       None.
;
; KEYWORD PARAMETERS:
;       FONT    - String; Name of font passed to graphical output
;                 widget.
;
; CALLS:
;       None.
;
; SIDE EFFECTS:
;       None.
;
; CATEGORY:
;       Adas system.
;
; WRITTEN:
;       William Osborn, Tessella Support Services plc,  24th June 1996
;
; MODIFIED:
;       1.1     William Osborn    
;		First release
;       1.2     William Osborn    
;		Corrected error message
;	1.3     William Osborn
;               Added check for new environment variable VERY_SMALL
;               If this is set to the string "YES" (preferably in the
;               .adas_setup file) then the font size used on the graphs
;               is reduced and the positioning of labels adjusted
;               slightly to make things fit on a very small screen.
;
; VERSION:
;	1.1	24-06-96
;	1.2	26-06-96
;	1.3	11-10-96
;
;-
;----------------------------------------------------------------------------

PRO plot406_4, ldef4, x, y, inttime, title, xmin2, xmax2, ymin2, ymax2

    COMMON Global_lw_data, left, right, tp, bot, grtop, grright

                ;****************************************************
                ;**** Suitable character size for current device ****
                ;**** Aim for 60 characters in y direction.      ****
                ;****************************************************

    charsize = (!d.y_vsize/!d.y_ch_size)/60.0
    small_check = GETENV('VERY_SMALL')
    if small_check eq 'YES' then charsize=charsize*0.8

                ;**** Initialise titles ****

    xtitle = 'ELECTRON TEMPERATURE (eV)'
    ytitle = 'ENERGY FUNCTION (J cm!E3!N)'

    erase

    ann = ['ERB','ERC','ELT','TOT']

                ;**** How many points to plot ****

    npts = size(x)
    npts = npts(1)

		;**** Find x and y ranges for auto scaling,        ****
                ;**** check x and y in range for explicit scaling. ****

    makeplot = 1
    style = 0
    ystyle = 0

    if ldef4 eq 0 then begin

                ;**** identify values in the valid range ****
                ;**** plot routines only work within ****
                ;**** single precision limits.       ****

	xvals = where (x gt 1.0e-37 and x lt 1.0e37)
	ypvals = where (y gt 1.0e-37 and y lt 1.0e37)
	ymvals = where (y lt -1.0e-37 and y gt -1.0e37)
	if xvals(0) gt -1 then begin
	    maxx = max(x(xvals))
	    minx = min(x(xvals))
	endif else begin
	    makeplot = 0
	endelse
	if ypvals(0) gt -1 or ymvals(0) gt -1 then begin
            if ypvals(0) gt -1 and ymvals(0) eq -1 then begin
	        maxy = max(y(ypvals))
	        miny = min(y(ypvals))
	    endif else if ypvals(0) eq -1 and ymvals(0) gt -1 then begin
		maxy = max(-y(ymvals))
                miny = min(-y(ymvals))
            endif else begin
                maxy = max([max(y(ypvals)), max(-y(ymvals))])
                miny = min([min(y(ypvals)), min(-y(ymvals))])
            endelse
	endif else begin
	    makeplot = 0
	endelse
	if makeplot eq 1 then begin
	    if miny le 1.0e-36 then begin
		ystyle = 1
		miny = 1.0e-36
		maxy = 1.2*maxy
	    endif else begin
		ystyle = 0
	    endelse
	endif
	style = 0
    endif else begin
        minx = xmin2
	maxx = xmax2
	miny = ymin2
	maxy = ymax2
	xvals = where(x gt minx and x lt maxx)
	ypvals = where(y gt miny and y lt maxy)
	ymvals = where(-y gt miny and -y lt maxy)
	if xvals(0) eq -1 or (ypvals(0) eq -1 and ymvals(0) eq -1) then begin
	    makeplot = 0
	endif else begin
	    makeplot = 1
	endelse
	style = 1
        ystyle = 1
    endelse

	;**** Information needed for the anno procedure ****

    top_handle = handle_create()

    if makeplot eq 1 then begin

        p_out_of_range = bytarr(4)

                ;**** Set up log-log plotting axes ****

	plot_oo, [minx, maxx], [miny, maxy], /nodata, ticklen=1.0,	$
                 position=[left, bot, grright, grtop],			$
                 xtitle=xtitle, ytitle=ytitle, xstyle=style, 		$
		 ystyle=ystyle, charsize=charsize

                ;*********************************
                ;**** Make and annotate plots ****
                ;*********************************

		;**** Plot positive values ****

	if ypvals(0) gt -1 then begin
 	    for i=0, 3 do begin
	        ymaxchk = max(y(*,i))		;check this line is okay to 
						;draw
	        if ymaxchk le 1.0e-36 then begin
		    p_out_of_range(i) = 1B
	        endif else begin
 	            oplot, x, y(*,i), linestyle=0
     
                ;**** Find suitable points for annotation ****
		;**** If the graph has been split into separate segments ****
		;**** by the y-limits then annotate the segments too ****
		    intemp = where(y(*,i) lt miny)
		    numin = n_elements(intemp)
		    if intemp(numin-1) ne npts-1 then begin
			;**** Extra two elements: first is the last point,
			;**** second is a buffer so the while statement later
			;**** doesn't cause trouble
		      	in = lonarr(numin+2)
		    	in(numin)=npts-1
		    	in(0:numin-1)=intemp
			numin = numin+1
		    endif else begin
			in = lonarr(numin+1)
		    	in(0:numin-1)=intemp
		    endelse
		    for j=0, numin-1 do begin
			if in(j) ne 0 then begin
			    anno, x(in(j)-1), y(in(j)-1,i), ann(i)+'+', $
				top_handle
			endif
			;**** Increase j until next drawn segment found ****
			while j lt numin-1 and (in(j+1) eq in(j)+1) $
			    		do j = j + 1
		    endfor
	        endelse
	    endfor
	endif

		;**** Plot negative values ****

	if ymvals(0) gt -1 then begin
 	    for i=0, 3 do begin
	        ymaxchk = max(-y(*,i))		;check this line is okay to 
						;draw
	        if ymaxchk le 1.0e-36 and p_out_of_range(i) eq 1B then begin
		    print, '******************************* D6OTG4 MESSAGE '+$
                           '*******************************'
		    print, 'NO '+ann(i)+' GRAPH WILL BE OUTPUT BECAUSE:'
		    print, 'ALL VALUES ARE BELOW THE CUTOFF OF 1.000E-36'
		    print, '******************************* END OF MESSAGE '+$
                           '*******************************'
	        endif else begin
 	            oplot, x, -y(*,i), linestyle=1
     
                ;**** Find suitable point for annotation ****

		    intemp = where(-y(*,i) lt miny)
		    numin = n_elements(intemp)
		    if intemp(numin-1) ne npts-1 then begin
			;**** Extra two elements: first is the last point,
			;**** second is a buffer so the while statement later
			;**** doesn't cause trouble
		      	in = lonarr(numin+2)
		    	in(numin)=npts-1
		    	in(0:numin-1)=intemp
			numin = numin+1
		    endif else begin
			in = lonarr(numin+1)
		    	in(0:numin-1)=intemp
		    endelse
		    for j=0, numin-1 do begin
			if in(j) ne 0 then begin
			    anno, x(in(j)-1), -y(in(j)-1,i), ann(i)+'-', $
				top_handle
			endif
			;**** Increase j until next drawn segment found ****
			while j lt numin-1 and (in(j+1) eq in(j)+1) $
			    		do j = j + 1
		    endfor

	        endelse
	    endfor
	endif

    endif else begin			;no plot possible
        xyouts, 0.2, 0.5, /normal, charsize=charsize*1.5,               $
                '---- No data lies within range ----'
    endelse

                ;**** Output title above graph ****
    
    xyouts, 0.1, 0.9, title, charsize=charsize, /normal

                ;**** Output titles to right of graph ****

    xyouts, 0.72, 0.78, inttime, charsize=charsize, /normal

    handle_free, top_handle

END
