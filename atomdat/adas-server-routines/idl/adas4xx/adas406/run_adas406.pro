; Copyright (c) 2003, Strathclyde University.
;----------------------------------------------------------------------
;+
; PROJECT    :  ADAS
;
; NAME       :  run_adas406
;
; PURPOSE    :  Runs ADAS406 ionisation balance code as an IDL
;               subroutine.
;
; ARGUMENTS  :  All output arguments will be defined appropriately.
;
;               NAME      I/O    TYPE   DETAILS
; INPUTS     :  uid        I     str    username of adf11 location.
;                                       'adas' for central ADAS data.
;               year       I     int    year of adf11 data.
;               defyear    I     int    default year of adf11 data
;                                       (defaults to 89 if not set).
;               filter     I     str    filter (if appropriate).
;               member     I     str    extra identifer (if appropriate).
;               elem       I     str    element
;               te         I     real() temperatures requested
;               dens       I     real() densities requested
;               tion       I     real() ion temperatures requested
;                                       (if /cx is used).
;               denh       I     real() ion densities requested
;                                       (if /cx is used).
;               mh         I     real   Hydrogen isotope mass
;                                       (if /cx is used).
;               mz         I     real   Element isotope mass
;                                       (if /cx is used).
;               meta       I     real() initial fractional abundance
;                                       at t=0sec. Defaults to all in
;                                       ground stage/metastable.
;               tint       I     real   integration time (sec)
;               log        I     str    name of output text file.
;                                       (defaults to no output).
;               files      I     struc  Optional structure with
;                                       replacement names for
;                                       individula files.
;                                          {acd  : '',  $
;                                           scd  : '',  $
;                                           ccd  : '',  $
;                                           prb  : '',  $
;                                           prc  : '',  $
;                                           qcd  : '',  $
;                                           xcd  : '',  $
;                                           plt  : ''   }
;                                       Not all file names are required - just
;                                       those for rplacement.
;
;
; KEYWORDS   :  partial    I     int    =1 partial data
;                                       =0 standard (default)
;            :  cx         I     int    =1 use cx data
;                                       =0 no cx is included (default)
;
; OUTPUTS    :  frac       O     struc  frac.stage - str array of ion/metastable
;                                                    descriptors.
;                                       frac.ion   - fltarr(nte, nstage) of
;                                                    nte - no. plasma parameters
;                                                    nstage - no. metastables
;                                                    fractional abundance.
;               energy      O     struc  energy.stage - str array of ion/metastable
;                                                     descriptors.
;                                       energy.ion   - fltarr(nte, nstage) of
;                                                     nte - no. plasma parameters
;                                                     nstage - no. metastables
;                                                     of metastable energy.
;                                       energy.plt   - fltarr(nte) of line energy
;                                       energy.prb   - fltarr(nte) of continuum energy
;                                       energy.prc   - fltarr(nte) of charge exchange energy
;                                       energy.total - fltarr(nte) of total energy
;
; NOTES      :  run_adas406 uses the spawn command. Therefore IDL v5.3
;               cannot be used. Any other version should work.
;
; AUTHOR     :  Martin O'Mullane
;
; DATE       :  10-01-2001
;
; MODIFIED
;       1.1     Martin O'Mullane
;                 - First version. Based on run_adas405.pro.
;       1.2     Martin O'Mullane
;                 - Add files, /all and /help to keep in synch
;                   with run_adas405.
;       1.3     Martin O'Mullane
;                 - Extraction of partition failed if implicit names (uid, year)
;                   rather than the files structure was input.
;       1.4     Martin O'Mullane
;                 - Routine crashed when relying on the default when no 
;                   initial fractional abundance was set.
;                 - Document meta and tint inputs.
;       1.5     Martin O'Mullane
;                 - Do not assume adas_cent and adas_user have same path. 
;       1.6     Martin O'Mullane
;                 - Correct error message for missing mass with /cx.
;
; DATE
;       1.1     28-07-2003
;       1.2     12-05-2005
;       1.3     12-02-2008
;       1.4     23-07-2009
;       1.5     08-10-2009
;       1.6     12-10-2009
;
;-
;----------------------------------------------------------------------

PRO run_adas406_filenames, year, defyear, elem, uid, member, filter,   $
                           is_partial, symbol,                         $
                           files_str,                                  $
                           filenames, fileavailable, defavailable



; Make up filenames from info passed to routine

filenames     = strarr(8)
fileavailable = intarr(8)
defnames      = strarr(8)
defavailable  = intarr(8)

dirnames = ['acd','scd','ccd','prb','prc','qcd','xcd','plt']



; To cope with odd pathnames replace the user in
; the environment ADASUSER with the input uid.
;
; ADASCENT may not be on same file system as ADASUSER (Asdex) so
; treat uid='adas' separately.

log_name  = getenv("LOGNAME")
adas_user = getenv("ADASUSER")
adas_cent = getenv("ADASCENT")

if uid NE 'adas' then begin

   part_1 = strmid(adas_user,0,strpos(adas_user,log_name))
   part_2 = strcompress(uid, /remove_all)
   part_3 = strmid(adas_user,strpos(adas_user,log_name)+strlen(log_name))

   filenames[*] = part_1 + part_2 + part_3 + '/adf11/'

endif else filenames[*] = adas_cent + '/adf11/'

defnames[*]  = adas_cent + '/adf11/'

defdirnames = dirnames + defyear
dirnames    = dirnames + year

if (is_partial) then begin
   dirnames    = dirnames + 'r'
   defdirnames = defdirnames + 'r'
endif


filenames = filenames + dirnames  + "/"
defnames  = defnames + defdirnames + "/"
if (strcompress(member, /remove_all) ne '') then begin
    filenames = filenames + strcompress(member, /remove_all) + '#'
    defnames  = defnames  + strcompress(member, /remove_all) + '#'
endif

if filter ne '' then begin

   filterstring = strcompress(filter, /remove_all)
   filteradd = ['','','','.'+filterstring,'.'+filterstring,    $
                '','','.'+filterstring]
   filenames = filenames + dirnames + "_" +                    $
   strcompress(symbol, /remove_all) + filteradd +".dat"
   defnames = defnames + defdirnames + "_" +                   $
   strcompress(symbol, /remove_all) + filteradd + ".dat"

endif else begin

   filenames = filenames + dirnames + "_" +               $
               strcompress(symbol, /remove_all) + ".dat"
   defnames  = defnames + defdirnames + "_" +             $
               strcompress(symbol, /remove_all) + ".dat"

endelse


; Overwrite user filenames which are missing with default ones

for j = 0, n_elements(filenames)-1 do begin

   res = findfile(filenames[j], count=count)
   fileavailable[j] = count
   if count EQ 0 then filenames[j] = defnames[j]

   res = findfile(defnames[j], count=count)
   defavailable[j] = count


endfor



; Overwrite choices with files_str values

if files_str.supp EQ 'YES' then begin

   for j = 1, n_tags(files_str)-1 do begin
      if files_str.(j) NE 'NULL' then begin
         filenames[j-1]     = files_str.(j)
         fileavailable[j-1] = 1
      endif
   endfor

endif

END
;----------------------------------------------------------------------



PRO run_adas406_calc, year, defyear, elem, uid, is_partial, symbol, mh, mz, $
                      member, filter, files_str, init_meta, timef,          $
                      te, dens, tion, denh, graph_out,                      $
                      is_frac, is_energy, output_file, indices, frac, energy

; Now run the ADAS406 code - the input has been checked already.

    fortdir = getenv('ADASFORT')
    spawn, fortdir+'/adas406.out', unit=pipe, /noshell, PID=pid

    date = xxdate()
    printf, pipe, date[0]

    rep = 'NO'
    printf, pipe, rep

    printf, pipe, year
    printf, pipe, defyear
    outsymbol = strcompress(strupcase(elem), /remove_all)
    printf, pipe, outsymbol

    scriptname = 'NULL'
    printf, pipe, scriptname

    printf, pipe, strcompress(filter, /remove_all)

    ; Generate filenames

    run_adas406_filenames, year, defyear, elem, uid, member, filter,   $
                           is_partial, symbol,                         $
                           files_str,                                  $
                           filenames, fileavailable, defavailable

    ; Write filenames to FORTRAN

    for i = 0, 7 do printf, pipe, filenames[i]
    for i = 0, 7 do printf, pipe, fileavailable[i]
    for i = 0, 7 do printf, pipe, defavailable[i]
    for i = 0, 7 do printf, pipe, indices[i]

    printf, pipe, is_partial

    idum = 0
    readf, pipe, idum


; Now move onto processing screen options

    ntdmax = 0
    input  = 0
    fdum   = 0.0
    sdum   = ''

    readf, pipe, input
    ntdmax = input

    readf, pipe, input
    imdimd = input
    readf, pipe, input
    nmsum = input
    chpop = strarr(nmsum)
    for i=0,nmsum-1 do begin
        readf, pipe, sdum, format='(A14)'
        chpop[i]=sdum
    endfor

    readf, pipe, input
    nline = input
    if nline gt 0 then begin
        ciion  = strarr(nline)
        cmpts  = strarr(nline)
        ctitle = strarr(nline)
        for i=0, (nline - 1) do begin
            readf, pipe, sdum
            ciion(i) = sdum
            readf, pipe, sdum
            cmpts(i) = sdum
            readf, pipe, sdum
            ctitle(i) = sdum
        endfor
    endif

    tvals = dblarr(2,ntdmax)

    lpend = 0
    printf, pipe, lpend

    printf, pipe, 'Test'
    printf, pipe, 0

    printf, pipe, timef
    for i=0, nmsum-1 do begin
        printf, pipe, init_meta[i]
    endfor
    printf, pipe, 2                 ; eV for temps
    printf, pipe, n_elements(te)
    printf, pipe, mZ
    printf, pipe, mH
    for i = 0, n_elements(te)-1 do begin
        printf, pipe, te[i]
        printf, pipe, dens[i]
        printf, pipe, tion[i]
        printf, pipe, denh[i]
    endfor

    istop = 0
    printf, pipe, istop



; Nearly there - output options
;  - get ion balance and energy


    if (is_frac) then begin

       lpend     = 0
       printf, pipe, lpend

       printf, pipe, graph_out
       if graph_out eq 1 then printf, pipe, 1

       if output_file NE '' then texout = 1 else texout = 0
       printf, pipe, texout

       if texout EQ 1 then printf, pipe, output_file

       gofout = 0
       printf, pipe, gofout

       if texout EQ 1 then printf, pipe, 'Made by read406 ' + date[0]
       printf, pipe, 0

       input = 0
       fdum  = 0.0
       sdum  = ' '

       readf, pipe, input
       itmax = input                            ;no. of electron temps.
       xvals = fltarr(itmax)
       for i=0, (itmax-1) do begin
           readf, pipe, fdum
           xvals[i] = fdum
       endfor

       readf, pipe, input
       nmsum = input
       yvals = fltarr(itmax, nmsum)
       poptit = strarr(nmsum)                   ; designation strings
       for i=0, (itmax-1) do begin
           for j=0, (nmsum-1) do begin
               readf, pipe, fdum
               yvals(i,j) = fdum
           endfor
       endfor

       for i=0, (nmsum-1) do begin
           readf, pipe, format='(a10)', sdum
           poptit(i) = sdum
       endfor

       readf, pipe, input

       readf, pipe, fdum                        ; integration time
       timef = fdum

       sy0 = fltarr(nmsum)
       for i = 0, nmsum-1 do begin
           readf, pipe, fdum
           sy0[i] = fdum
       endfor

       frac = { stage : poptit,  $
                ion   : yvals    }

       if (is_energy) then begin
          printf, pipe, 0
       endif else begin
          printf, pipe, 1
          goto, ENDOFME
       endelse

    endif



    if (is_energy) then begin

       lpend     = 0
       printf, pipe, lpend

       printf, pipe, graph_out
       if graph_out eq 1 then printf, pipe, 2


       if output_file NE '' then texout = 1 else texout = 0
       printf, pipe, texout

       if texout EQ 1 then printf, pipe, output_file

       gofout = 0
       printf, pipe, gofout

       if texout EQ 1 then printf, pipe, 'Made by read406 ' + date[0]
       printf, pipe, 0

       input = 0
       fdum  = 0.0
       sdum  = ' '

       readf, pipe, input
       itmax = input                            ;no. of electron temps.
       xvals = fltarr(itmax)
       for i=0, (itmax-1) do begin
           readf, pipe, fdum
           xvals(i) = fdum
       endfor

       readf, pipe, input
       nmsum = input
       yvals = fltarr(itmax, nmsum)
       poptit = strarr(nmsum)                   ;designation strings
       for i=0, (itmax-1) do begin
           for j=0, (nmsum-1) do begin
               readf, pipe, fdum
               yvals(i,j) = fdum
           endfor
       endfor

       for i=0, (nmsum-1) do begin
           readf, pipe, format='(a10)', sdum
           poptit(i) = sdum
       endfor
       readf, pipe, input

       readf, pipe, fdum                        ; integration time
       timef = fdum

       sy0 = fltarr(nmsum)
       for i = 0, nmsum-1 do begin
           readf, pipe, fdum
           sy0[i] = fdum
       endfor


       ; Go back for totals

       printf, pipe, 0

       lpend     = 0
       printf, pipe, lpend

       printf, pipe, graph_out
       if graph_out eq 1 then printf, pipe, 4


       ; No output file

       printf, pipe, 0


       gofout = 0
       printf, pipe, gofout

       if texout EQ 1 then printf, pipe, 'Made by read406 ' + date[0]
       printf, pipe, 0


       readf, pipe, input
       itmax = input                            ; no. of electron temps.
       xvals = fltarr(itmax)
       for i=0, (itmax-1) do begin
           readf, pipe, fdum
           xvals[i] = fdum
       endfor

       readf, pipe, input
       nmsum = input
       tvals = fltarr(itmax,4)                  ; total y-values
       for i=0, (itmax-1) do begin
           for j=0, 3 do begin
               readf, pipe, fdum
               tvals[i,j] = fdum
           endfor
       endfor

       readf, pipe, input

       readf, pipe, fdum                        ; integration time
       timef = fdum



       energy = { stage : poptit,       $
                  ion   : yvals,        $
                  elt   : tvals[*,2],   $
                  erb   : tvals[*,0],   $
                  erc   : tvals[*,1],   $
                  total : tvals[*,3]    }

       printf, pipe, 1

    endif


; Terminate FORTRAN process

ENDOFME:

    close, pipe
    free_lun, pipe

END
;----------------------------------------------------------------------------




PRO run_adas406_get_partition, uid, year, defyear, member, symbol, files, $
                               is_partial, num_meta

if files.supp EQ 'YES' then begin

   file_use = files.acd
   if file_use EQ 'NULL' then message, 'No valid files'

endif else begin

   elem = strlowcase(symbol)

   ; Construct the acd filename to read metastable partition

   log_name  = getenv("LOGNAME")
   adas_user = getenv("ADASUSER")
   adas_cent = getenv("ADASCENT")

   if uid NE 'adas' then begin

      part_1 = strmid(adas_user,0,strpos(adas_user,log_name))
      part_2 = strcompress(uid, /remove_all)
      part_3 = strmid(adas_user,strpos(adas_user,log_name)+strlen(log_name))

      filename = part_1 + part_2 + part_3 + '/adf11/'
   endif else filename = adas_cent + '/adf11/'
   
   defname  = adas_cent + '/adf11/'

   dirname    = 'acd' + year
   defdirname = 'acd' + defyear

   if (is_partial) then begin
      dirname    = dirname + 'r'
      defdirname = defdirname + 'r'
   endif


   filename = filename + dirname  + "/"
   defname  = defname + defdirname + "/"
   if (strcompress(member, /remove_all) ne '') then begin
       filename = filename + strcompress(member, /remove_all) + '#'
       defname  = defname  + strcompress(member, /remove_all) + '#'
   endif


   filename = filename + dirname + "_" +               $
               strcompress(elem, /remove_all) + ".dat"
   defname  = defname + defdirname + "_" +             $
               strcompress(elem, /remove_all) + ".dat"


   ; Use acd file or the default one

   file_use = 'NULL'
   file_acc, filename, exist, read, write, execute, filetype
   if exist EQ 1 AND read EQ 1 then begin
      file_use = filename
   endif else begin
      file_acc, defname, exist, read, write, execute, filetype
      if exist EQ 1 AND read EQ 1 then file_use = defname
   endelse

   if file_use EQ 'NULL' then message, 'No valid files'

endelse



; Read in the partition for partial or generate for standard

str  = ''
sdum = ''
idum = 0
zl   = 0
zh   = 0

openr, lun, file_use, /get_lun

readf, lun, zl, zh, format='(15x,2i5)'
readf, lun, str
readf, lun, str
if strpos(str, '.') NE -1 then begin
   nprt = intarr(zh-zl+2) + 1
endif else begin
   if (zh+zl-2 GT 16) then readf, lun, sdum
   str = str + sdum
   nprt = long(strsplit(str, ' ', /extract))
endelse

free_lun, lun

num_meta = long(total(nprt))


END
;----------------------------------------------------------------------------



PRO run_adas406, uid      =  uid,       $
                 year     =  year,      $
                 defyear  =  defyear,   $
                 elem     =  elem,      $
                 filter   =  filter,    $
                 member   =  member,    $
                 files    =  files,     $
                 partial  =  partial,   $
                 te       =  te,        $
                 dens     =  dens,      $
                 tion     =  tion,      $
                 denh     =  denh,      $
                 cx       =  cx,        $
                 mh       =  mh,        $
                 mZ       =  mZ,        $
                 meta     =  meta,      $
                 tint     =  tint,      $
                 frac     =  frac,      $
                 energy   =  energy,    $
                 log      =  log,       $
                 all      =  all,       $
                 help     =  help


; If asked for help

if keyword_set(help) then begin
   doc_library, 'run_adas406'
   return
endif


; First check that we are not using IDL v5.3

if !version.release EQ '5.3' then begin
   message, 'This version of IDL (v5.3) will not work - ' + $
            'change to other v5 (v5.0, v5.1, v5.2 or v5.4) or higher'
   return
endif


; Check that we get all inputs and that they are correct. Otherwise print
; a message and return to command line.

on_error, 2

if n_elements(uid) EQ 0 then begin
    message,'A data source (uid) must be given'
endif

if n_elements(defyear) EQ 0 then begin
    defyear = 89
    print, '89 has been set as the default year'
endif

if n_elements(year) EQ 0 then begin
    print, 'The default year will be used'
    year = defyear
endif


defyear = string(defyear, format='(i2.2)')
year    = string(year   , format='(i2.2)')

if n_elements(elem) EQ 0 then begin
    message, 'An element must be given'
endif else begin

    symbol = strcompress(elem, /remove_all)
    if (symbol eq '') then begin
        message, 'Enter a valid  isonuclear element symbol'
    endif else begin
        i4eiz0, symbol, atnumber
        if (atnumber eq 0) then  message, 'Enter a valid element'
    endelse

endelse


if n_elements(filter) EQ 0 then filter = ''

if n_elements(member) EQ 0 then member = ''

if keyword_set(partial) then is_partial = 1 else is_partial = 0


if n_elements(files) EQ 0 then begin
   files_str = { supp : 'NO'}
endif else begin

   files_str = { supp : '',  $
                 acd  : '',  $
                 scd  : '',  $
                 ccd  : '',  $
                 prb  : '',  $
                 prc  : '',  $
                 qcd  : '',  $
                 xcd  : '',  $
                 plt  : ''   }

   struct_assign, files, files_str

   for j = 0, n_tags(files_str)-1 do begin
      if files_str.(j) EQ '' then files_str.(j) = 'NULL'
   endfor
   files_str.supp = 'YES'

endelse






; Which type of calculation - indices : acd, scd, ccd, prb, prc, qcd, xcd, plt

indices = intarr(8)

if NOT arg_present(frac) EQ 0 then begin
   message, 'An ionisation balance output is required'
endif else begin
   if (is_partial) then indices = [1,1,0,0,0,1,1,0] $
                   else indices = [1,1,0,0,0,0,0,0]
endelse

if keyword_set(cx) then begin
   indices = indices + [0,0,1,0,0,0,0,0]
   if n_elements(mh) EQ 0 then message, 'Hydrogen mass required'
   if n_elements(mZ) EQ 0 then message, 'Element mass required'
endif else begin
   mh = 1.0
   mZ = 1.0
endelse

if arg_present(energy) then begin
   indices = indices + [0,0,0,1,0,0,0,1]
   if keyword_set(cx) then indices = indices + [0,0,0,0,1,0,0,0]
endif

; if n_elements(log) EQ 0 then output_file = '' else output_file = log
if n_elements(log) EQ 0 then output_file = '' $
                        else begin
                                message, 'No output file yet',/continue
                                output_file = ''
                        endelse


; Temperature and density

if n_elements(te) eq 0 then message, 'User requested temperatures are missing'
if n_elements(dens) eq 0 then message, 'User requested densities are missing'

partype=size(te, /type)
if (partype lt 2) or (partype gt 5) then begin
   message,'Temperature must be numeric'
endif  else te = DOUBLE(te)

partype=size(dens, /type)
if (partype lt 2) or (partype gt 5) then begin
   message,'density must be numeric'
endif else dens = DOUBLE(dens)

len_te   = n_elements(te)
len_dens = n_elements(dens)

; are te and dens the same length and one dimensional?
; if so define data array

if keyword_set(all) then begin

   ; make a big 1D list

   t_in = dblarr(len_te*len_dens)
   d_in = dblarr(len_te*len_dens)
   if keyword_set(cx) then begin
      ti_in = dblarr(len_te*len_dens)
      dh_in = dblarr(len_te*len_dens)
   endif

   k = 0
   for i = 0, len_te-1 do begin
      for j = 0, len_dens-1 do begin
         t_in[k] = te[i]
         d_in[k] = dens[j]
         if keyword_set(cx) then begin
            ti_in[k] = tion[i]
            dh_in[k] = denh[j]
         endif
         k = k + 1
       endfor
    endfor
    itval = k


endif else begin

   ; are te and dens the same length and one dimensional?
   ; if so define data array

   if len_dens ne len_te then print, 'TE and DENS size mismatch - smaller  used'
   itval = min([len_te,len_dens])

   t_in = te
   d_in = dens
   if keyword_set(cx) then begin
      ti_in = tion
      dh_in = denh
   endif

endelse


if NOT keyword_set(cx) then begin
   ti_in = dblarr(itval)
   dh_in = dblarr(itval)
endif



; Get initial metastable fractions


run_adas406_get_partition, uid, year, defyear, member, symbol, files_str, $
                           is_partial, num_meta

if n_elements(meta) EQ 0 then begin

  message, 'Assume initial population is all in ground stage', /continue
  init_meta    = dblarr(num_meta)
  init_meta[0] = 1.0

endif else begin

  if n_elements(meta) NE num_meta then $
     message, 'Incorrect number of initial metastables'

  mtot = total(meta)
  init_meta = double(meta/mtot)

endelse

; Integration time

if n_elements(tint) EQ 0 then message, 'An integration is required' $
                         else timef = double(tint)



; Output options - if none exit

if arg_present(frac) OR arg_present(energy) then graph_out = 1 $
   else message, 'Choose at least one output!!'

if arg_present(frac) then is_frac = 1 else is_frac = 0
if arg_present(energy) then is_energy = 1 else is_energy = 0




; Run the calculation in batchs of 30, max permitted by ADAS406


MAXVAL = 30

elt   = 0.0
erb   = 0.0
erc   = 0.0
etot  = 0.0

n_call = numlines(itval, MAXVAL)
n_val  = 0

for jl = 0, n_call - 1 do begin

  ist = jl*MAXVAL
  ifn = min([(jl+1)*MAXVAL,itval])-1

  te_ca    = t_in[ist:ifn]
  dens_ca  = d_in[ist:ifn]
  tion_ca  = ti_in[ist:ifn]
  denh_ca  = dh_in[ist:ifn]
  itval_ca = ifn-ist+1

  run_adas406_calc, year, defyear, elem, uid, is_partial, symbol, mh, mz,  $
                    member, filter, files_str, init_meta, timef,           $
                    te_ca, dens_ca, tion_ca, denh_ca,                      $
                    graph_out, is_frac, is_energy, output_file, indices,   $
                    frac_ca, energy_ca


  if (is_frac) then begin

     n_stage = n_elements(frac_ca.stage)

     if n_elements(fr_ca) EQ 0 then begin
        fr_ca = frac_ca.ion
     endif else begin

        n_temp = size(fr_ca)
        n_temp = n_temp[1]
        yvals  = fltarr(n_temp+itval_ca,n_stage)

        for j = 0, n_stage-1 do begin
          for k = 0, n_temp+itval_ca-1 do begin
             if k LE n_temp-1 then yvals[k,j] = fr_ca[k,j] $
                              else yvals[k,j] = frac_ca.ion[k-n_temp,j]
          endfor
        endfor

        fr_ca = yvals

     endelse

  endif

  if (is_energy) then begin

     elt   = [elt, energy_ca.elt]
     erb   = [erb, energy_ca.erb]
     erc   = [erc, energy_ca.erc]
     etot  = [etot,energy_ca.total]

     n_stage = n_elements(energy_ca.stage)

     if n_elements(pw_ca) EQ 0 then begin
        pw_ca = energy_ca.ion
     endif else begin

        n_temp = size(pw_ca)
        n_temp = n_temp[1]
        yvals  = fltarr(n_temp+itval_ca,n_stage)

        for j = 0, n_stage-1 do begin
          for k = 0, n_temp+itval_ca-1 do begin
             if k LE n_temp-1 then yvals[k,j] = pw_ca[k,j] $
                              else yvals[k,j] = energy_ca.ion[k-n_temp,j]
          endfor
        endfor

        pw_ca = yvals

     endelse

  endif

endfor

if keyword_set(all) then begin

   if (is_frac) then frac = { stage : frac_ca.stage,  $
                              ion   : reform(fr_ca, len_dens, len_te, n_stage)}

   if (is_energy) then energy = { stage : energy_ca.stage,                           $
                                  ion   : reform(pw_ca, len_dens, len_te, n_stage),  $
                                  elt   : reform(elt[1:*],len_dens, len_te),         $
                                  erb   : reform(erb[1:*],len_dens, len_te),         $
                                  erc   : reform(erc[1:*],len_dens, len_te),         $
                                  total : reform(etot[1:*],len_dens, len_te)         }

endif else begin

   if (is_frac) then frac  = { stage : frac_ca.stage,  $
                               ion   : fr_ca           }


   if (is_energy) then energy = { stage : energy_ca.stage, $
                                  ion   : pw_ca,           $
                                  elt   : elt[1:*],        $
                                  erb   : erb[1:*],        $
                                  erc   : erc[1:*],        $
                                  total : etot[1:*]        }

endelse


; Free up all units

close, /all

END
