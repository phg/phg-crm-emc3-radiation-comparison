; Copyright (c) 1996, Strathclyde University.
; SCCS info: Module @(#)$Header: /home/adascvs/idl/adas4xx/adas406/cw_adas406_proc.pro,v 1.3 2004/07/06 12:46:59 whitefor Exp $ Date $Date: 2004/07/06 12:46:59 $
;+
; PROJECT:
;       ADAS IBM MVS to UNIX conversion
;
; NAME:
;	CW_ADAS406_PROC()
;
; PURPOSE:
;	Produces a widget for ADAS406 processing options/input.
;
; EXPLANATION:
;	This function creates a compound widget consisting of :-
;	   a text widget holding an editable 'Run title', 
;	   the script file name/browse widget ,
;	   information widgets,
;	   typein widgets for the electron and hydrogen isotopes,
;	   if a script file is chosen then a list of lines in that
;	   file along with the current selection,
;	   a table widget for temp/density data ,
;	   a button to enter default values into the table, 
;	   a button to clear the table,
;	   a message widget, 
;	   an 'Escape to series menu', a 'Cancel' and a 'Done' button.
;
;	The compound widgets included in this widget are self managing.
;	This widget only manages events from the  'Default' button,
;	the 'Cancel' button and the 'Done' button and the isotope
;	typeins and the 'escape to series menu' button.
;
; USE:
;	This widget is specific to ADAS406, see adas406_proc.pro
;	for use.
;
; INPUTS:
;	TOPPARENT- Long integer, ID of parent widget.
;
;	DSFULL	- String; The full system file name of the input 
;		  dataset selected by the user for processing.
;
;       SYMBOL  - String; isonuclear element symbol.
;
;       CLASSSTRING - String; list of the selected master classes.
;
;	NTDMAX  - Integer; max number of user entered temp/densities
;
;       NLINE   - Int; No. of lines in script file
;
;	NMSUM  - Int; Number of metastables
;
;	CHPOP	- String Array; metastable identifier strings
;
;	IMDIMD  - Int; Max. number of metastables
;
;       CIION   - String array; emitting ion for a given line
;
;       CMPTS   - Int array: no. components for each line in script file
;
;       CTITLE  - String array: Any information given in script file on
;                 lines to be analysed.
;
;       BITFILE - String; the path to the directory containing bitmaps
;                 for the 'escape to series menu' button.
;
; OPTIONAL INPUTS:
;	None
;
; OUTPUTS:
;	The return value of this function is the ID of the declared widget.
;
;	ACT 	- String; result of this widget, 'done' or 'cancel'
;
; OPTIONAL OUTPUTS:
;	None
;
; KEYWORD PARAMETERS:
;	PROCVAL	- A structure which determines the initial settings of
;		  the processing options widget.
;
;		  The default PROCVAL is created thus;
;
;     		ps = {proc406_set, $
;			new   : 0,		$
;                	title : '',             $
;			lsnull: 0,		$
;			ifout : 1,		$
;			ispline: 0,		$
;			hyval : 1.0,		$
;			elval : float,		$
;                       tein  : temp_arr,       $
;                       tihn  : temp_arr,       $
;                       dein  : temp_arr,       $
;                       dhin  : temp_arr,       $
;                       maxt  : 0,              $
;                       maxd  : 0,              $
;			tvals : dblarr(2,ndtin),$
;			timef : 0.0,		$
;			fabun0: dblarr(imdimd)  $
;             }
;
;
;		NEW     Flag which defines whether or not default values
;			exist or not. (< 0 if no values exist)
;
; 		TITLE	Entered general title for program run
;
;		HYVAL   Float; the hydrogen isotope mass number.
;
;		ELVAL	Float; the element isotope mass number.
;
;		IFOUT	Flag for the temperature units in use:
;			1=Kelvin, 2=eV
;
;		ISPLINE Integer - the selected line from the script file.
;
;		TEIN  	Input electron temperatures entered by user
;
;		TIHN  	Input hydrogen temperatures entered by user
;
;		DEIN	Input electron densities entered by user
;
;		DHIN	Input hydrogen densities entered by user
;
;		MAXT	Number of temperatures in table entered by user
;
;		MAXD	Number of densities in table (always=MAXT)
;
;		TVALS	The input temperatures in two different units 
;
;		TIMEF	Integration time in seconds
;
;		FABUN0	Fractional abundance of metastables at t=0
;
;		Most of these structure elements map onto variables of
;		the same name in the ADAS406 FORTRAN program.
;
;
;	UVALUE	- A user value for the widget. Default 0.
;
;	FONT_LARGE - The name of a larger font.  Default current system
;		     font
;
;	FONT_SMALL - The name of a smaller font. Default current system
;		     font.
;
;	EDIT_FONTS - A structure of two fonts used in the table editor
;		     adas_edtab.pro, {font_norm:'',font_input:''}.  Defaults
;		     to current system font.
;
;	NUM_FORM   - String; Numeric format to use in tables.  Default
;			'(E10.3)'
;		     
;
; CALLS:
;	POPUP		Popup warning window with buttons.
;	CW_ADAS_DSBR	Dataset name and comments browsing button.
;	CW_ADAS_TABLE   Adas data table widget.
;	XXEIAM		Converts element symbol to atomic mass
;	NUM_CHK		Checks validity of a user's numbers
;	CW_SINGLE_SEL   Single selection compound widget
;	TEXT_TABLE	Constructs a correctly aligned text table
;	READ_X11_BITMAP Reads in the bitmap for the 'escape to series
;			menu' button.
;
;	See side effects for other related widget management routines.
;
; SIDE EFFECTS:
;
;	The following widget management routines are included in this file;
;	PROC406_SET_VAL()	Sets the current PROCVAL structure.
;	PROC406_GET_VAL()	Returns the current PROCVAL structure.
;	PROC406_EVENT()		Process and issue events.
;	
; CATEGORY:
;	Compound Widget
;
; WRITTEN:
;       William Osborn, Tessella Support Services plc, 11th June 1996
;
; MODIFIED:
;	1.1	William Osborn
;		First version
;	1.2	Richard Martin
;		Corrected font for 'Enter Output Temperature/Density data'
;       1.3     Martin O'Mullane
;               Added font to element and H isotope mass request
;               text edit widgets.
;
; VERSION:
;	1.1	11-06-96
;	1.2	20-11-98
;       1.3     06-02-01
;
;-
;-----------------------------------------------------------------------------

PRO proc406_set_val, id, value

                ;**** Return to caller on error ****

    ON_ERROR, 2

                ;**** Retrieve the state ****

    first_child = widget_info(id, /child)
    widget_control, first_child, get_uvalue=state, /no_copy

		;**** Set input focus ****

    widget_control, state.eliso, input_focus=value

                ;**** Reset the value of state variable ****

    widget_control, first_child, set_uvalue=state, /no_copy

END

;-----------------------------------------------------------------------

FUNCTION proc406_get_val, id

    COMMON cw_proc406_blk, lsnull

                ;**** Return to caller on error ****

    ON_ERROR, 2

		 ;***************************************
                 ;****     Retrieve the state 	     ****
		 ;**** Get first_child widget id     ****
		 ;**** because state is stored there ****
		 ;***************************************

    first_child = widget_info(id, /child)
    widget_control, first_child, get_uvalue = state

		;***********************************
		;**** Get run title from widget ****
		;**** Then centre in in string  ****
		;**** of 40 characters 		****
		;***********************************

    widget_control, state.runid, get_value=title
    title = strcompress(title(0))
    title_len = strlen(title) 
    if (title_len gt 40 ) then begin 
	title = strmid(title,0,37)
        widget_control, state.messid, set_value="Title too long - truncated "
        widget_control, state.runid, set_value=title
	wait ,1
    endif
    pad = (40 - title_len)/2 
    spaces = '                                         '
    title(0) = strmid(spaces,0,(pad-1)) + title + strmid(spaces,0,(pad-1))
  

		;****************************************************
		;**** Get new temp/densitydata from table widget ****
		;****************************************************

    widget_control, state.tempid, get_value=tempval
    tabledata = tempval.value
    ifout = tempval.units+1
 
 	        ;**** Copy out temperature values ****

    tein = dblarr(state.ntdmax)
    tihn = dblarr(state.ntdmax)
    dein = dblarr(state.ntdmax)
    dhin = dblarr(state.ntdmax)
    blanks = where(strtrim(tabledata(0,*),2) eq '')
    blanksd = where(strtrim(tabledata(6,*),2) eq '')

                ;***********************************************
		;**** next line assumes that all blanks are ****
		;**** at the end of the columns             ****
		;***********************************************

    if blanks(0) ge 0 then maxt=blanks(0) else maxt=state.ntdmax
    if blanksd(0) ge 0 then maxd=blanksd(0) else maxd=state.ntdmax
    if maxt gt 0 then begin
	tein(0:maxt-1) = double(tabledata(0,0:maxt-1))
	tihn(0:maxt-1) = double(tabledata(3,0:maxt-1)) 
    endif
    if maxd gt 0 then begin
	dein(0:maxd-1) = double(tabledata(6,0:maxd-1))
	dhin(0:maxd-1) = double(tabledata(7,0:maxd-1))
    endif

		;**** Fill in the rest with zeroes ****

    if maxt lt state.ntdmax then begin
        tein(maxt:state.ntdmax-1) = double(0.0)
        tihn(maxt:state.ntdmax-1) = double(0.0)
	dein(maxd:state.ntdmax-1) = double(0.0)
	dhin(maxd:state.ntdmax-1) = double(0.0)
    endif

    state.maxt = maxt
    state.maxd = maxd
    tvals = tabledata(1:2,*)

		;**** Get isotope mass values ****

    widget_control, state.hyiso, get_value=get_hyval
    if num_chk(get_hyval(0)) eq 0 then begin
	hyval = float(get_hyval(0))
    endif else begin
	hyval = 0.0
    endelse

    widget_control, state.eliso, get_value=get_elval
    if num_chk(get_elval(0)) eq 0 then begin
	elval = float(get_elval(0))
    endif else begin
	elval = 0.0
    endelse

		;**** get selected spectral line index ****

    if (lsnull eq 0) then begin
	widget_control, state.lineid, get_value=selected
	ispline = selected
    endif else begin
	ispline = 0
    endelse

		;**** get integration time ****
    widget_control, state.inttimeid, get_value=get_time
    if num_chk(get_time(0)) eq 0 then begin
	timef = float(get_time(0))
    endif else begin
	timef = 0.0
    endelse

		;**** get fractional abundances ****
    widget_control, state.fabunid, get_value=fval
    fabun0 = dblarr(state.imdimd)
    fabun0(0:state.nmsum-1) = double(fval.value(1,*))

 		;***********************************************
 		;**** write selected values to PS structure ****
 		;***********************************************
 
     ps = { 			 		$
	        new   : 0, 			$
                title : title,          	$
		lsnull: lsnull,			$
		hyval : hyval,			$
		elval : elval,			$
 		ifout : ifout,			$
		ispline:ispline,		$
 		maxt  : maxt,			$
 		maxd  : maxd,			$
 		tihn  : tihn,			$
 		tein  : tein,			$
 		dhin  : dhin,			$
 		dein  : dein,			$
 		tvals : tvals,			$
		timef : timef,			$
		fabun0: fabun0 			$
              }

   
    widget_control, first_child, set_uvalue=state, /no_copy

    RETURN, ps

END

;-----------------------------------------------------------------------------

FUNCTION proc406_event, event


                ;**** Base ID of compound widget ****

    parent=event.handler

		;**********************************************
                ;**** Retrieve the user value state        ****
		;**** Get id of first_child widget because ****
		;**** user value "state" is stored there   ****
		;**********************************************

    first_child = widget_info(parent, /child)
    widget_control, first_child, get_uvalue=state, /no_copy

		;*********************************
		;**** Clear previous messages ****
		;*********************************

    widget_control, state.messid, set_value=' '

                ;************************
                ;**** Process events ****
                ;************************

    CASE event.id OF


		;******************************
		;**** isotope mass numbers ****
		;******************************

	state.hyiso : widget_control, state.inttimeid, /input_focus
	state.eliso : widget_control, state.hyiso, /input_focus

		;**** Integration time entry ****
  	state.inttimeid : widget_control, state.eliso, /input_focus

		;**** Clear table button ****

	state.clearid: begin

                ;**** popup window to confirm ****

	    action= popup(message='Are you sure you want to clear the table?',$
                          buttons=['Confirm','Cancel'], font=state.font,$
                          title='ADAS406 Warning:-')

            if action eq 'Confirm' then begin

                ;**** Get current table widget value ****

                widget_control, state.tempid, get_value=tempval

		;**** Zero the relevant columns ****

		empty = strarr(state.ntdmax)
		tempval.value(0,*) = empty
		tempval.value(3,*) = empty
		tempval.value(6,*) = empty
		tempval.value(7,*) = empty
		state.maxt = 0
		state.maxd = 0

		;**** Copy new data to table widget ****

 	        widget_control, state.tempid, set_value=tempval

	    endif

	end


		;*************************************
		;**** Default Energies button     ****
		;*************************************

        state.deftid: begin

 		;**** popup window to confirm overwriting current values ****

            action= popup(message='Confirm Overwrite values with Defaults', $
 		          buttons=['Confirm','Cancel'], font=state.font,$
			  title='ADAS406 Warning:-')

	    if action eq 'Confirm' then begin

		;**** Get current table widget value ****

 	        widget_control, state.tempid, get_value=tempval

		;**** Force user to select default density values ****

		messvalue = " Enter default electron density:- "
                titlvalue = "DEFAULT DENSITIES"
		action = popin(message=messvalue, font=state.font, 	$
                               title = titlvalue)
		defedensity = action
		messvalue = " Enter default hydrogen density:- "
                titlvalue = "DEFAULT DENSITIES"
		action = popin(message=messvalue, font=state.font, 	$
                               title = titlvalue)
		defhdensity = action

		;***********************************************
		;**** Copy defaults into value structure    ****
		;**** For default values use the defaults   ****
		;***********************************************

                units = tempval.units+1
                tempval.value(0,0:state.ntdmax-1) = 			$
 	        strtrim(string(tempval.value(units,0:state.ntdmax-1),	$
 	        format=state.num_form),2)
                tempval.value(3,0:state.ntdmax-1) = 			$
 	        strtrim(string(tempval.value(units+3,0:state.ntdmax-1),	$
 	        format=state.num_form),2)
                tempval.value(6,0:state.ntdmax-1) =			$
                string(defedensity, format=state.num_form)
                tempval.value(7,0:state.ntdmax-1) =			$
                string(defhdensity, format=state.num_form)

		;**** Copy new data to table widget ****

 	        widget_control, state.tempid, set_value=tempval

 	    endif

     	end

		;***********************
		;**** Cancel button ****
		;***********************

        state.cancelid: new_event = {ID:parent, TOP:event.top, 		$
				     HANDLER:0L, ACTION:'Cancel'}

		;*********************
		;**** Done button ****
		;*********************

        state.doneid: begin

		;***************************************
		;**** Check all user input is legal ****
		;***************************************

	    error = 0

        	;********************************************
		;*** Have to restore "state" before calls ***
		;*** to proc406_get_val so it can be used ***
		;*** there.				  ***
		;********************************************

	    widget_control, first_child, set_uvalue=state, /no_copy

	    widget_control, event.handler, get_value=ps

		;*** reset state variable ***

	    widget_control, first_child, get_uvalue=state, /no_copy

		;**** check temp values entered ****

            if error eq 0 and (state.maxt eq 0 or state.maxd eq 0) then begin
                error = 1
                message='Error: No temperatures/densities entered.'
            endif

		;**** check isotope values entered ****

	    if error eq 0 then begin
		widget_control, state.hyiso, get_value=hycheck
		if num_chk(hycheck(0), sign=1) eq 1 then begin
		    error=1
		    message='Error: The hydrogen isotope mass number is'+$
		    ' invalid.'
		endif else if float(hycheck(0)) eq 0.0 then begin
		    error=1
		    message='Error: The hydrogen isotope mass number is'+$
		    ' invalid.'
		endif
	    endif

		;**** Check integration time ****

	    if error eq 0 then begin
		widget_control, state.inttimeid, get_value=icheck
		if num_chk(icheck(0),sign=1) eq 1 then begin
		    error=1
		    message='Error: the integration time is invalid'
		    widget_control, state.inttimeid, /input_focus
		endif
	    endif

		;**** Check fractional abundances ****

	    if error eq 0 then begin
		fsum0 = 0.0d0
		for i=0,state.nmsum-1 do begin
		    fsum0 = fsum0 + ps.fabun0(i)
		endfor
		if fsum0 ne 1.0d0 then begin
		    error=1
		    message='Error: the fractional abundances do not add up'+$
			    ' to one'
		    widget_control, state.fabunid, /input_focus
		endif
	    endif

		;**** return value or flag error ****

	    if error eq 0 then begin
                new_event = {ID:parent, TOP:event.top,                  $
                             HANDLER:0L, ACTION:'Done'}
            endif else begin
	        widget_control, state.messid, set_value=message
	        new_event = 0L
            endelse

        end

                ;**** Menu button ****

        state.outid: begin
            new_event = {ID:parent, TOP:event.top, HANDLER:0L,          $
                         ACTION:'Menu'}
        end

        ELSE: new_event = 0L

    ENDCASE

		;*********************************************
		;*** make "state" available to other files ***
		;*********************************************

    widget_control, first_child, set_uvalue=state, /no_copy

    RETURN, new_event

END

;-----------------------------------------------------------------------------

function cw_adas406_proc, topparent, dsfull, act, 			$
	                  symbol, classstring, nmsum, chpop, imdimd,	$
			  ntdmax, nline, ciion, cmpts, ctitle, bitfile,	$
		          procval=procval, uvalue=uvalue, 		$
		          font_large=font_large, font_small=font_small,	$
		          edit_fonts=edit_fonts, num_form=num_form

    COMMON cw_proc406_blk, lsnull

		;**** Set defaults for keywords ****

    if not (keyword_set(uvalue)) then uvalue = 0
    if not (keyword_set(font_large)) then font_large = 	''
    if not (keyword_set(font_small)) then font_small = ''
    if not (keyword_set(edit_fonts)) then 				$
    edit_fonts = {font_norm:'',font_input:''}
    if not (keyword_set(num_form)) then num_form = '(E10.3)'

    if not (keyword_set(procval)) then begin
        temp_arr = dblarr(ntdmax)
	fabun_arr= dblarr(imdimd)
	fabun_arr(0) = 1.0
        xxeiam, symbol, elmass
        ps = { 					$ 
		new   : 0, 			$
		title : '',             	$
		lsnull: 0,			$
		ifout : 1,			$
		ispline:0,			$
		hyval : 1.0,			$
		elval : elmass,			$
                tein  : temp_arr,       	$
                tihn  : temp_arr,       	$
                dein  : temp_arr,       	$
                dhin  : temp_arr,       	$
                maxt  : 0,              	$
                maxd  : 0,              	$
                tvals : dblarr(2,ntdmax), 	$
		timef : 0.0,			$
		fabun0: fabun_arr		$
              }
    endif else begin
        xxeiam, symbol, elmass
	ps = { 					$ 
		new   : procval.new,    	$
                title : procval.title,  	$
		lsnull: procval.lsnull,		$
 		ifout : procval.ifout,		$
		ispline:procval.ispline,	$
		hyval : procval.hyval,		$
		elval : elmass,      		$
                tein  : procval.tein,   	$
                tihn  : procval.tihn,   	$
                dein  : procval.dein,   	$
                dhin  : procval.dhin,   	$
                maxt  : procval.maxt,   	$
                maxd  : procval.maxd,   	$
                tvals : procval.tvals,   	$
		timef : procval.timef,		$
		fabun0: procval.fabun0		$
	     }
    endelse
    lsnull = ps.lsnull

                ;*********************************************************
                ;**** Modify certain parameters and results depending ****
                ;**** on the machine being used                       ****
                ;*********************************************************

    machine = GETENV('TARGET_MACHINE')
    if machine eq 'HPUX' then begin
        y_size = 4
        large_font = font_small
    endif else begin
        y_size = 6
        large_font = font_large
    endelse

		;********************************************************
		;**** Assemble temp and density  table data          ****
		;********************************************************
		;**** The adas table widget requires data to be     *****
		;**** input as strings, so all the numeric data     *****
		;**** has to be written into a string array.        *****
		;**** Declare energies  table array                 *****
		;**** col 1 has user electron temp values           *****
		;**** col 2-3 have defaultelectron temp values      *****
		;**** which can have one of two possible units      *****
		;**** col4 has user hydrogen temp values	    *****
		;**** col 5-6 have default hydrogen temp values     *****
		;**** which can have one of two possible units      *****
		;**** col 7 has user entered electron densities     *****
		;**** col 8 has user entered hydrogen densities	    *****
		;********************************************************

     tabledata = strarr(8, ntdmax)

                ;*************************************
		;**** Copy out temp and density   ****
		;**** values                      ****
		;*************************************

    if (ps.maxt gt 0) then begin
        tabledata(0,0:ps.maxt-1) =					$
 	strtrim(string(ps.tein(0:ps.maxt-1),format=num_form),2)
        tabledata(3,0:ps.maxt-1) =					$
 	strtrim(string(ps.tihn(0:ps.maxt-1),format=num_form),2)
    endif
    tabledata(1,0:ntdmax-1) =	 					$
    strtrim(string(ps.tvals(0,0:ntdmax-1),format=num_form),2)
    tabledata(2,0:ntdmax-1) = 						$
    strtrim(string(ps.tvals(1,0:ntdmax-1),format=num_form),2)
    tabledata(4,0:ntdmax-1) =	 					$
    strtrim(string(ps.tvals(0,0:ntdmax-1),format=num_form),2)
    tabledata(5,0:ntdmax-1) = 						$
    strtrim(string(ps.tvals(1,0:ntdmax-1),format=num_form),2)
    if (ps.maxd gt 0) then begin
        tabledata(6,0:ps.maxd-1) =					$
        strtrim(string(ps.dein(0:ps.maxd-1),format=num_form),2)
        tabledata(7,0:ps.maxd-1) =					$
        strtrim(string(ps.dhin(0:ps.maxd-1),format=num_form),2)
    endif

		;**** fill rest of table with blanks          ****
		;**** allow zero values in hydrogen densities ****

    tabledummy = tabledata(0:6,*)
    blanks = where(tabledummy eq 0.0) 
    if (blanks(0) ne -1) then tabledummy(blanks) = ' ' 
    tabledata(0:6,*) = tabledummy

		;********************************************************
		;**** Create the 406 Processing options/input window ****
		;********************************************************

		;**** create titled base widget ****

    parent = widget_base(topparent, UVALUE = uvalue, 			$
			 title = 'ADAS406 PROCESSING OPTIONS', 		$
			 EVENT_FUNC = "proc406_event", 			$
			 FUNC_GET_VALUE = "proc406_get_val", 		$
			 PRO_SET_VALUE = "proc406_set_val",		$
			 /COLUMN)

		;******************************************************
		;**** Create a dummy widget just to hold value of *****
		;**** "state" variable so as not to get confused  *****
		;**** with any other values. Adopt IDL practice   *****
		;**** of using first child widget                 *****
		;******************************************************

    first_child = widget_base(parent)

    topbase = widget_base(first_child,/column)

		;***********************
		;**** add run title ****
		;***********************

    base = widget_base(topbase,/row)
    rc = widget_label(base,value='Title for Run',font=large_font)
    runid = widget_text(base,value=ps.title,xsize=38,font=large_font,/edit)

		;**** add dataset name and browse button ****

    rc = cw_adas_dsbr(topbase, dsfull, font=large_font,			$
		      filelabel='Script file: ')

		;********************************
		;**** Base for information   ****
		;********************************

    infomain = widget_base(topbase, /row)
    infobase = widget_base(infomain, /column, /frame)
    infobase2 = widget_base(infomain, /column, /frame)
    infolab  = widget_label(infobase, value='Data file information:-',	$
                            font=large_font)
    eleval =    'Selected master file element: '
    if (strlen(symbol) eq 1) then begin
	eleval = eleval + strupcase(symbol) + '                            '
    endif else begin
	eleval = eleval + strupcase(strmid(symbol, 0,1)) +		$
	strmid(symbol,1,1) + '                           '
    endelse
    dummylab = widget_label(infobase, value='     ', font=font_small)
    elementlab = widget_label(infobase, value=eleval, font=font_small)
    classval =  'Selected master classes: '
    buffer = '                                  '
    buflen = strlen(buffer)
    classlen = strlen(classstring)
    dummy2 = '                                  '
    dummy2 = dummy2 + '                         '
    if classlen le buflen then begin
	strput, buffer, classstring
    endif else begin
	first_part = strmid(classstring, 0, strlen(buffer))
	strput, buffer, first_part
        second_part = strmid(classstring, strlen(buffer),		$
	(strlen(classstring) - strlen(buffer)))
        strput, dummy2, second_part, 24
    endelse
    classval = classval + buffer
    classlab = widget_label(infobase, value=classval, font=font_small)
    classlab2 = widget_label(infobase, value=dummy2, font=font_small)

		;**** Base for isotope mass numbers ****

    isoval = 'Enter isotope information:-'
    isolab = widget_label(infobase2, value=isoval, font=large_font)
    infobase3 = widget_base(infobase2, /row)
    elmislab = widget_label(infobase3, font=font_small,			$
    value='Enter element isotope mass number  (amu) : ')
    elvalstring = string(ps.elval, format='(f5.1)')
    eliso = widget_text(infobase3, value=elvalstring, xsize=6, 		$
                        /editable, font=font_small)
    infobase4 = widget_base(infobase2, /row)
    hydislab = widget_label(infobase4, font=font_small,			$
    value='Enter hydrogen isotope mass number (amu) : ')
    hyvalstring = string(ps.hyval, format='(f5.1)')
    hyiso = widget_text(infobase4, value=hyvalstring, xsize=6, 		$
                        /editable, font=font_small)

 		;************************************************
		;**** base for script file information and   ****
		;**** selection. This will vary depending on ****
		;**** whether a null script has been chosen  ****
		;**** (indicated by the value of ps.lsnull)  ****
 		;************************************************
 
    tablescriptbase = widget_base(topbase, /row)
    tablebase = widget_base(tablescriptbase, /column)
    scriptbase = widget_base(tablebase, /column, /frame)
    scriptheading = widget_label(scriptbase, font=large_font,		$
                    value="Select spectral line for analysis:-")
    scriptbase2 = widget_base(scriptbase, /row)
    if (ps.lsnull eq 1) then begin		;null script file
	scriptlabel=widget_label(scriptbase,				$
		    value='There is no selected script file.')
	lineid = 0
    endif else begin
	no_lines_label='Number of listed lines in script file: '+	$
        strtrim(string(nline),2)

		;**** Build up the strings for selection ****

	linestrings = strarr(4,nline)
	linestrings(0,*) = ' ' + 					$
        strcompress(string(indgen(nline)+1), /remove_all)
        linestrings(1,*) = '      ' + ciion
	linestrings(2,*) = '       ' + cmpts
	linestrings(3,*) = '    ' + ctitle
	scriptlabel=widget_label(scriptbase2, font=font_small,		$
		    value=no_lines_label)

		;**** Put in the selection list ****

	titles = [['LINE','    RADIATING','     NUMBER OF',		$
                   '     TITLE AND'],					$
		  ['INDEX','    ION CHARGE','     COMPONENTS',		$
                   '     WAVELENGTH']]
	select_data = text_table(linestrings, colhead=titles, /noindex)
	coltitles = select_data(0:1)
	select_data = select_data(2:nline+1)
	if ps.ispline gt (nline-1) then ps.ispline = 0
	lineid = cw_single_sel(scriptbase, select_data, value=ps.ispline,  $
	                       title=' ',				   $
			       coltitles=coltitles, ysize=5,		   $
			       font=font_small, big_font=large_font)
    endelse

 		;************************************************
 		;**** base for the table and defaults button ****
 		;************************************************
 
    base = widget_base(tablebase, /column, /frame)
    llabel = widget_label(base, font=large_font,  			$
             value="Enter Output Temperature/Density data")

		;********************************
		;**** Energy data table      ****
		;********************************

    colhead = [['Electron','Hydrogen','Electron','Hydrogen'],		$
               ['Output values','Output values','Output values',	$
                'Output values']]

		;********************************************
		;**** convert FORTRAN index to IDL index ****
		;********************************************

    units = ps.ifout-1 
    unitname = ['eV','Kelvin']

		;************************************************************
		;**** Four columns in the table and two sets of units.   ****
		;**** All columns are unchanged by the units used        ****
		;************************************************************

    unitind = [[0,0],[3,3],[6,6],[7,7]]
    table_title = ["Temperatures                  Densities"]

		;****************************
		;**** table of data   *******
		;****************************
 
    tempid = cw_adas_table(base, tabledata, units, unitname, unitind, 	$
 		           UNITSTITLE = 'Temperature Units', 		$
 		           COLEDIT = [1,1,1,1], COLHEAD = colhead, 	$
 		           TITLE = table_title, ORDER = [1,1,0,0],	$ 
			   UNITS_PLUS = '       Density Units : cm-3',	$
 		           LIMITS = [1,1,1,1], CELLSIZE = 12, 		$
 		           /SCROLL, ytexsize=y_size, NUM_FORM=num_form,	$
 		           FONTS = edit_fonts, FONT_LARGE = large_font,	$
 		           FONT_SMALL = font_small, /ROWSKIP)

		;*************************
		;**** Default buttons ****
		;*************************

    t1base = widget_base(base, /column)
    tbase = widget_base(t1base, /row)
    deftid = widget_button(tbase, font=large_font,                 	$
                           value='   Default Temperature/Density Values   ')

		;**** Clear table button ****

    clearid = widget_button(tbase, font=large_font,			$
			    value='     Clear Table     ')

		;**** Error message ****

    messid = widget_label(parent,font=large_font, 			$
    value='Edit the processing options data and press Done to proceed')

		;*********************************************
		;**** Base for time-dependant information ****
		;*********************************************

    timebase=widget_base(tablescriptbase,/column)

		;**** Integration time entry ****

    intbase = widget_base(timebase,/row,/frame)
    labid = widget_label(intbase, value='Integration time (sec) : ',	$
		font=large_font)
    inttimeid = widget_text(intbase, /editable, font=large_font, 	$
		value = strtrim(string(ps.timef,format=num_form),2),	$
		xsize = 9)

		;**** Initial fractional abundance entry ****

    fbase = widget_base(timebase,/column,/frame)
    tabledata = strarr(2,nmsum)
    tabledata(0,*) = chpop
    tabledata(1,*) = strtrim(string(ps.fabun0(0:nmsum-1),format=num_form),2)
    fabunid = cw_adas_table(fbase,tabledata,coledit=[0,1],		$
	colhead=['Metastable','Fraction'],title=['Fractional Abundance',$
	'At Start Time'], ytexsize=y_size+5, fltint=['(a)',num_form],	$
	/SCROLL, FONTS = edit_fonts, FONT_LARGE = large_font,		$
        FONT_SMALL = font_small)

		;**** add the exit buttons ****

    base = widget_base(parent, /row)
    menufile = bitfile + '/menu.bmp'
    read_X11_bitmap, menufile, bitmap1
    outid = widget_button(base, value=bitmap1)		;menu button
    cancelid = widget_button(base, value='Cancel', font=large_font)
    doneid = widget_button(base, value='Done', font=large_font)

		;*************************************************
		;**** create a state structure for the pop-up ****
		;**** 		     window.                  ****
		;*************************************************

    new_state = {					$
		  runid		:	runid,  	$
		  messid	:	messid, 	$
 		  deftid	:	deftid,		$
		  clearid	:	clearid,	$
 		  tempid	:	tempid, 	$
		  cancelid	:	cancelid,	$
		  doneid	:	doneid, 	$
		  outid		:	outid,		$
		  lineid	:	lineid,		$
		  dsfull	:	dsfull,		$
		  font		:	large_font,	$
                  tvals		:	ps.tvals,       $
		  hyiso		:	hyiso,		$
		  eliso		:	eliso,		$
                  maxt		:	ps.maxt,        $
                  maxd		:	ps.maxd,        $
                  imdimd	:	imdimd,         $
                  ntdmax	:	ntdmax,         $
                  tablebase	:	tablebase,      $
		  num_form	:	num_form,	$
		  inttimeid	:	inttimeid,	$
		  fabunid	:	fabunid,	$
		  nmsum		:	nmsum	 	$
	      }

                 ;**** Save initial state structure ****

    widget_control, first_child, set_uvalue=new_state, /no_copy

    RETURN, parent

END

