; Copyright (c) 1997 Strathclyde University .
; SCCS info: Module @(#)$Header: /home/adascvs/idl/adas4xx/adas412/cw_adas412_proc.pro,v 1.4 2008/08/31 17:58:33 mog Exp $ Date $Date: 2008/08/31 17:58:33 $
;+
; PROJECT:
;       ADAS IBM MVS to DEC UNIX conversion
;
; NAME:
;       CW_ADAS412_PROC()
;
; PURPOSE:
;       Produces a widget for ADAS412 processing options/input.
;
; EXPLANATION:
;       This function creates a compound widget consisting of a text
;       widget holding an editable 'Run title', the dataset name/browse
;       widget cw_adas_dsbr, label widgets with year and element information for
;       the input data, a table widget for the plasma model data cw_adas_table,
;       buttons to enter default values into and clear the model tables.
;       The widget also has toggle buttons to activate either tranistion selection
;       by indices or by wavelength range. Correspondingly it has a table widget
;       cw_adas412_table for entering indices, along with an extra text widget 
;       displaying the levels present, and input boxes for wavelength selection.
;       Finally, the widget has a 'Menu' button, 'Cancel' button and  a 'Done' 
;       button.
;
;       The compound widgets included in this widget are self managing.
;       This widget only manages events from the 'Defaults' and 'Clear data'
;       buttons, the 'Menu' button,'Cancel' button and the 'Done' button.
;
;       This widget only generates events for the 'Done' and 'Cancel'
;       buttons.
;
; USE:
;       This widget is specific to ADAS412, see adas412_proc.pro
;       for use.
;
; INPUTS:
;       TOPPARENT- Long integer, ID of parent widget.
;
;       YEAR     - String, year of required adf11 data.
;
;       ELEM   - String, selected element.
;
;       LSTRGA - String array, level designations.
;
;       DSNINC - String; The full system file name of the input COPASE
;                  dataset selected by the user for processing.
;
;       NDTEM    - Integer; Maximum number of temperatures allowed.
;
;       NDTRANS- Integer; Maximum number of transitions allowed.
;
;       IL       - Integer; Number of energy levels.
;
;     BITFILE- String; the path to the dirctory containing bitmaps
;                 for the 'escape to series menu' button.
;
; OPTIONAL INPUTS:
;       None
;
; OUTPUTS:
;       The return value of this function is the ID of the declared widget.
;
; OPTIONAL OUTPUTS:
;       None
;
; KEYWORD PARAMETERS:
;       VALUE   - A structure which determines the initial settings of
;                 the processing options widget.
;
;                 The default VALUE is created thus;
;
;       ps = {proc412_set, $
;                 TITLE:''  ,   IFOUT:0         ,       $
;                 MAXDT:0   ,   MAXT:0          ,       $
;                 TINE:temp ,   DINE:temp       ,       $
;                 PINE:temp ,   NHNE:temp       ,       $
;                 DEFT:deft ,   TOGGLE:0        ,       $
;                 TOGGLED:0 ,   ITOGGLE:1       ,       $
;                 ISEL:0    ,                           $
;                 NDTRANS:ndtrans,NTRANS:0      ,       $
;                 LMIN:ltemp,   LMAX:ltemp      ,       $
;                 SWL:swl       ,       WAVN:''         ,       $
;                 WAVX:''   ,   IMAXJ:''          }     
;
;       TITLE   - Title of the program run.
;       IFOUT   - 0 no defaults 1- defaults exist.
;       MAXDT   - Maximum number of default temperatures.
;       MAXT    - Maximum number of user entered temperatures.
;       TINE    - Output Electron Temperature array.
;       DINE    - Output Electron Density array.
;       PINE    - Output Electron Pressure array.
;       NHNE    - Array of nh/ne coefficients.
;       DEFT    - Array of default temperature, density and pressures.
;       TOGGLE  - 0- transitions selected by index 1- by wavelength range.
;       TOGGLED - 0- Uniform density selected 1- uniform pressure.
;     ITOGGLE - 0- Density independent ionisation balance 1-Density dependent 
;       ISEL      - Index for fortran: 1- transitions by index 3- by wavelength range.
;       NDTRANS - Maximum number of transition selections allowed.
;       NTRANS  - Number of transitions selected.
;       LMIN      - Lower indices of selected transitions.
;       LMAX      - Maximum indices of selected transitions.
;       SWL       - Array of spectroscopic wavelengths.
;       WAVN      - Minumum of wavelength range selected.
;       WAVX      - Maximum of wavelength range selected.
;       IMAXJ     - Maximum level allowed when transitions selected by wavelength
;                       range.
;
;
;               All of these structure elements map onto variables of
;               the same name in the ADAS412 FORTRAN program.
;
;
;       UVALUE  - A user value for the widget. Default 0.
;
;       FONT_LARGE - The name of a larger font.  Default current system
;                    font
;
;       FONT_SMALL - The name of a smaller font. Default current system
;                    font.
;
;       EDIT_FONTS - A structure of two fonts used in the table editor
;                    adas_edtab.pro, {font_norm:'',font_input:''}.  Defaults
;                    to current system font.
;
;       NUM_FORM   - String; Numeric format to use in tables.  Default
;                       '(E10.3)'
;                    
;
; CALLS:
;       POPUP                   Popup warning window with buttons.
;       POPIN412                Popup window with density/pressure entry field.
;       CW_ADAS_DSBR    Dataset name and comments browsing button.
;       CW_ADAS_TABLE   Adas data table widget.
;       CW_ADAS412_TABLE        Adas data table widget with list of atomic levels.
;       CW_SAVESTATE    Widget management routine.
;       CW_LOADSTATE    Widget management routine.
;
;       See side effects for other related widget management routines.
;
; SIDE EFFECTS:
;       A COMMON BLOCK: CW_PROC412_BLK is used which is private to
;       this widget.
;
;       The following widget management routines are included in this
;       file:
;
;       PROC412_GET_VAL()               Returns the current VALUE structure.
;       PROC412_EVENT()         Process and issue events.
;
; CATEGORY:
;       Compound Widget
;
; WRITTEN:
;       Richard Martin, University of Strathclyde, October 1997.
;
; MODIFIED:
;
;       1.1     Richard Martin
;               First release
;       1.2     Richard Martin
;               Added Ionisation balance selection toggle buttons.
;       1.3     Richard Martin
;               Removed obsolete cw_loadstate/savestate statements.
;               IDL 5.5 fixes.
;       1.4     Martin O'Mullane
;               Correct spelling mistake (Maximun!) in error message.
;
; VERSION:
;       1.1   24-10-97
;       1.2   23-06-98
;       1.3   30-01-02
;       1.4   31-08-2008
;-
;-----------------------------------------------------------------------------

FUNCTION proc412_get_val, id


                ;**** Return to caller on error ****
  ON_ERROR, 2

                ;**** Retrieve the state ****

  first_child = widget_info(id,/child)
  widget_control, first_child, get_uvalue=state, /no_copy
  
                ;***********************************
                ;**** Get run title from widget ****
                ;***********************************

  widget_control,state.runid,get_value=title

        ;********************************************************
      ;*** Get current value of transition selection button ***
        ;********************************************************
        
  widget_control, state.transid, get_value=dummy
  toggle = dummy[0]

        ;**************************************************************
      ;*** Get selected transitions and spectroscopic wavelengths ***
        ;**************************************************************

  lmin=intarr(state.ndtrans)
  lmax=intarr(state.ndtrans)
  swl=fltarr(state.ndtrans)
        
  widget_control, state.lselid, get_value=templev
  templv = templev.value
  index = where(strtrim(templv(0,*)))
  nltrans = n_elements(index)
  lmax(0:nltrans-1)=fix(templv(0,0:nltrans-1))
  
  index = where(strtrim(templv(1,*)))
  lmin(0:nltrans-1)=fix(templv(1,0:nltrans-1))
  if index(0) lt 0 then ntrans=0
  
  swl(0:nltrans-1)=float(templv(2,0:nltrans-1))

        ;*******************************************************************
      ;*** Get entered max. & min. wavelengths and maximum level index ***
      ;*******************************************************************
      
  widget_control, state.wmax, get_value=wmax
  wlmax =strtrim(wmax[0],2)

  widget_control, state.wmin, get_value=wmin
  wlmin = strtrim(wmin[0],2)

  widget_control, state.maxj, get_value=maxj
  imaxj = strtrim(maxj[0],2)
 
  wavn = wlmin
  wavx = wlmax

        ;************************************************************
      ;*** Update isel - corresponds to isel in fortran adas412 ***
      ;************************************************************

  if toggle eq 1 then begin
     isel=3 
     ntrans=0
  endif else begin
     isel=1
     ntrans=nltrans
  endelse

                ;************************************************
                ;**** Get new Model data from table widget   ****
                ;************************************************

  widget_control,state.densid,get_value=model
  modeldata = model.value

                ;**** Copy out Model data values ****

  blanks = where(strtrim(modeldata(0,*),2) eq '',count)
  maxt=state.ndtem-count(0)
        
  tine = dblarr(state.ndtem)
  dine = dblarr(state.ndtem)
  pine = dblarr(state.ndtem)  
  nhne = dblarr(state.ndtem)

  toggled=state.toggled

  itoggle=state.itoggle

  if maxt gt 0 then begin
    tine(0:maxt-1) = double(modeldata(0,0:maxt-1))
    if toggled eq 0 then begin
        dine(0:maxt-1) = double(modeldata(1,0:maxt-1))
        pine(0:maxt-1) = double(modeldata(1,0:maxt-1))*(10^tine(0:maxt-1))
    endif else begin
        dine(0:maxt-1) = double(modeldata(1,0:maxt-1))/(10^tine(0:maxt-1))
        pine(0:maxt-1) = double(modeldata(1,0:maxt-1))
    endelse
    nhne(0:maxt-1) = double(modeldata(2,0:maxt-1))
  end
 
                ;**** Fill out the rest with blanks ****
  if maxt lt state.ndtem then begin
    tine(maxt:state.ndtem-1) = double(0.0)
    dine(maxt:state.ndtem-1) = double(0.0)
    pine(maxt:state.ndtem-1) = double(0.0)    
    nhne(maxt:state.ndtem-1) = double(0.0)
  end

    ps = {        TITLE:title[0]        ,       IFOUT:state.ifout ,     $
                  MAXDT:state.maxdt     ,       MAXT:maxt       ,       $
                  TINE:tine             ,       DINE:dine               ,       $
                  PINE:pine             ,       NHNE:nhne               ,       $
                  DEFT:state.deft       ,       TOGGLE:toggle   ,       $
                  TOGGLED:toggled       ,       ITOGGLE:itoggle         ,       $
                  ISEL:isel     ,                                               $
                  NDTRANS:state.ndtrans ,       NTRANS:ntrans   ,       $
                  LMIN:lmin                     ,       LMAX:lmax               ,       $
                  SWL:swl                       ,       WAVN:wavn       ,       $
                  WAVX:wavx             ,       IMAXJ:imaxj               }  

  widget_control, first_child, set_uvalue=state, /no_copy       

  RETURN, ps

END

;-----------------------------------------------------------------------------

FUNCTION proc412_event, event


                ;**** Base ID of compound widget ****
  parent=event.handler

                ;**** Retrieve the state ****

  first_child = widget_info(parent,/child)
  widget_control, first_child, get_uvalue=state
  
                ;*********************************
                ;**** Clear previous messages ****
                ;*********************************

  widget_control,state.messid,set_value=' '

                ;************************
                ;**** Process events ****
                ;************************

  CASE event.id OF

                ;*****************************
                ;**** Default data button ****
                ;*****************************

    state.defdid: begin

                ;**** popup window to confirm overwriting current values ****

        action = popup(message='Confirm: Overwrite Data with Defaults', $
                        buttons=['Confirm','Cancel'],font=state.font)

        if action eq 'Confirm' then begin

                ;**** Get current table widget value ****
          widget_control,state.densid,get_value=modelval

          ;**** Force user to select default density values ****

          messvalue = " Enter default electron density/pressure:- "
                titlvalue = "DEFAULT DENSITY/PRESSURE"

                data={ dpvalue: '', toggled: state.toggled }
          action = popin412(message=messvalue, font=state.font,   $
                                   title = titlvalue, VALUE=data)
          deftval=action.dpvalue
          toggled=action.toggled
          state.toggled=action.toggled
        
          widget_control,state.colhead1,get_value=colhead
          if state.toggled eq 0 then begin
                  colhead(0) = '        Electron    Electron      NH/NE'
                  colhead(1) = '      Temperature    Density      Ratio'
                  colhead(2) = '        (log(K))     (cm-3) '
          endif else begin
                  colhead(0) = '        Electron    Electron      NH/NE'
                  colhead(1) = '      Temperature   Pressure      Ratio'
                  colhead(2) = '        (log(K))    (Kcm-3) ' 
          endelse         
                                          
          ;**** Get the default densities and set new units ****

          maxd=state.maxdt
          
                ;**** Copy defaults into value structure ****
          if maxd gt 0 then begin
            modelval.value(0,0:maxd-1) = $
                strtrim(string(state.deft(0,0:maxd-1),format=state.num_form),2)

            modelval.value(1,0:maxd-1) = $
                strtrim(string(deftval,format=state.num_form),2)
                
            modelval.value(2,0:maxd-1) = $
                strtrim(string(state.deft(2,0:maxd-1),format=state.num_form),2)             
          end

                ;**** Fill out the rest with blanks ****
          if maxd lt state.ndtem then begin
            modelval.value(*,maxd:state.ndtem-1) = ''
          end
                
                ;**** Copy new temperature data to table widget ****

          widget_control,state.colhead1,set_value=colhead
          widget_control,state.densid,set_value=modelval

        end

      end

        state.cleardid: begin

                ;**** popup window to confirm overwriting current values ****
           action = popup(message='Confirm: Clear current data.', $
                        buttons=['Confirm','Cancel'],font=state.font)           

                if action eq 'Confirm' then begin                       

                        ;**** Get current table widget value ****
                        widget_control,state.densid,get_value=modelval  
                        
                maxd=state.maxt
                        modelval.value(0,0:maxd-1) =''
                        modelval.value(1,0:maxd-1) =''                  
                        modelval.value(2,0:maxd-1) =''                  

                 ;**** Copy new temperature data to table widget ****
                 widget_control,state.densid,set_value=modelval
                end
        end
        
      state.transid: begin

                ;*** Get current value of transition selection buttons ***

            widget_control, state.transid, get_value=dummy

     ;****** Select transitions by indices or by wavelength ?

            if dummy[0] eq 1 then state.toggle=0 else state.toggle=1    
            widget_control, state.selecbase, sensitive=state.toggle
            widget_control, state.wbase, sensitive=dummy[0]
            if state.toggle eq 0 then state.isel=3 else state.isel=1
      end                                                               

      state.idepid: begin

      ;*** Get current value of ionisation balance selection buttons ***

            widget_control, state.idepid, get_value=dummy

     ;****** Density independent/depenent ionisation balance ? ***

            if dummy[0] eq 0 then state.itoggle=0 else state.itoggle=1  

      end                                                               


                ;***********************
                ;**** Cancel button ****
                ;***********************

    state.cancelid: new_event = {ID:parent, TOP:event.top, $
                                HANDLER:0L, ACTION:'Cancel'}

                ;*********************
                ;**** Done button ****
                ;*********************

    state.doneid: begin
                        ;***************************************
                        ;**** Check all user input is legal ****
                        ;***************************************
        error = 0
        widget_control,event.handler,get_value=ps


                ;**** check model data entered ****
        if error eq 0 and ps.maxt eq 0 then begin
          error = 1
          message='Error: No densities entered.'
      end

        if ps.toggle eq 1 then begin
                if error eq 0 then begin
                  error=num_chk(ps.wavx)
                  if error gt 0 then message='**Error: Maximum wavelength should be a number.**'
        endif

                if error eq 0 then begin
                 error=num_chk(ps.wavn)
                 if error gt 0 then message='**Error: Minimum wavelength should be a number.**'
        endif
      
                if error eq 0 then begin
                  error=num_chk(ps.imaxj)
                  if error gt 0 then message='**Error: Maximum level should be a number.**'
        endif     

                if error eq 0 then begin
                        if (float(ps.wavx) lt float(ps.wavn)) then begin
                                error=1
                        message = '**Error: Maximum wavelength too small.**'
                        endif
                endif
        endif
                ;**** return value or flag error ****
        if error eq 0 then begin
          new_event = {ID:parent, TOP:event.top, HANDLER:0L, ACTION:'Done'}
      endif else begin
          widget_control,state.messid,set_value=message
          new_event = 0L
      end

     end

                ;**** Menu button ****

      state.outid: begin
          new_event = {ID:parent, TOP:event.top, HANDLER:0L,          $
                       ACTION:'Menu'}
      end

    ELSE: new_event = 0L

  ENDCASE

  RETURN, new_event
END

;-----------------------------------------------------------------------------

FUNCTION cw_adas412_proc, topparent, $
                year, elem, lstrga, dsninc, ndtem,  ndtrans, il, $
                bitfile, $
                VALUE=value, UVALUE=uvalue, $
                FONT_LARGE=font_large, FONT_SMALL=font_small, $
                EDIT_FONTS=edit_fonts, NUM_FORM=num_form


                ;**** Set defaults for keywords ****
  IF NOT (KEYWORD_SET(uvalue)) THEN uvalue = 0
  IF NOT (KEYWORD_SET(font_large)) THEN font_large = ''
  IF NOT (KEYWORD_SET(font_small)) THEN font_small = ''
  IF NOT (KEYWORD_SET(edit_fonts)) THEN $
                                edit_fonts = {font_norm:'',font_input:''}
  IF NOT (KEYWORD_SET(num_form)) THEN num_form = '(E10.3)'

  IF NOT (KEYWORD_SET(value)) THEN begin
        temp     = dblarr(ndtem)
        deft     = dblarr(3,ndtem)
        ltemp    = intarr(ndtrans)
        swtemp = fltarr(ndtrans)
        
        ps = {proc412_set, $
                  TITLE:''  ,   IFOUT:0         ,       $
                  MAXDT:0   ,   MAXT:0          ,       $
                  TINE:temp ,   DINE:temp       ,       $
                  PINE:temp ,   NHNE:temp       ,       $
                  DEFT:deft ,   TOGGLE:0        ,       $
                  TOGGLED:0 ,   ITOGGLE:1   ,   $
                  ISEL:0    ,                   $
                  NDTRANS:ndtrans,NTRANS:0      ,       $
                  LMIN:ltemp,   LMAX:ltemp      ,       $
                  SWL:swl       ,       WAVN:''         ,       $
                  WAVX:''   ,   IMAXJ:''          }
                        
  END ELSE BEGIN
        ps = {proc412_set, $
                  TITLE: value.title    ,       IFOUT: value.ifout      ,       $
                  MAXDT: value.maxdt    ,       MAXT: value.maxt        ,       $
                  TINE: value.tine      ,       DINE: value.dine        ,       $
                  PINE: value.pine      ,       NHNE: value.nhne        ,       $
                  DEFT: value.deft      ,       TOGGLE: value.toggle    ,       $
                  TOGGLED: value.toggled,       ITOGGLE: value.itoggle  ,       $
                  ISEL: value.isel      ,                                               $
                  NDTRANS: value.ndtrans,       NTRANS: value.ntrans    ,       $
                  LMIN:value.lmin               ,       LMAX: value.lmax                ,       $
                  SWL: value.swl                ,       WAVN:  value.wavn               ,       $
                  WAVX: value.wavx      ,       IMAXJ: value.imaxj        }     

  END

                ;***************************************************
                ;*****    Assemble Model data table          *****
                ;***************************************************
                ;**** The adas table widget requires data to be ****
                ;**** input as strings, so all the numeric data ****
                ;**** has to be written into a string array.    ****
                ;***************************************************

  modeldata = strarr(3,ndtem)

                ;**** Copy out temperature values ****
  if ps.ifout gt 0 then begin
      modeldata(0,0:ps.maxt-1) = $
                strtrim(string(ps.tine(0:ps.maxt-1),format=num_form),2)

        if ps.toggled eq 0 then begin
          modeldata(1,0:ps.maxt-1) = $
                  strtrim(string(ps.dine(0:ps.maxt-1),format=num_form),2)
        endif else begin
          modeldata(1,0:ps.maxt-1) = $
                  strtrim(string(ps.pine(0:ps.maxt-1),format=num_form),2)         
        endelse

      modeldata(2,0:ps.maxt-1) = $
                strtrim(string(ps.nhne(0:ps.maxt-1),format=num_form),2)
      if ps.maxt lt ndtem then begin
          modeldata(*,ps.maxt:ndtem-1) = ''
      endif
  endif else begin
        modeldata(*,0:ndtem-1) = ''
        ps.ifout=1
  endelse

                ;********************************************************
                ;**** Create the 412 Processing options/input window ****
                ;********************************************************

                ;**** create titled base widget ****
                
  parent = widget_base(topparent, UVALUE = uvalue, $
                        EVENT_FUNC = "proc412_event", $
                        FUNC_GET_VALUE = "proc412_get_val", $
                        /COLUMN)
                ;**** Create base to hold the value of state ****

  first_child = widget_base(parent)

  topbase = widget_base(first_child,/column)

                ;**** add run title ****
  base = widget_base(topbase,/row)
  rc = widget_label(base,value='Title for Run',font=font_large)
  runid = widget_text(base,value=ps.title,xsize=40,font=font_large,/edit)

                ;**** add dataset name and browse button ****
  rc = cw_adas_dsbr(topbase,dsninc,font=font_large)

                ;**** add data information ****
  rc = widget_label(topbase,font=font_large, $
                        value='adf11 year:'+year+'  Element:'+elem)

                ;**** Another base ****
  tablebase = widget_base(parent,/row,/frame)


                ;**** base for the density table and defaults button ****
  base1 = widget_base(tablebase,/column,/frame)

  
                ;**** Model data table ****

                ;*************************************************************
                ;**** Three columns in the table. Middle column is either ****
                ;**** density or pressure (denity default). Column        ****
                ;**** headings & units changed accordingly.                 ****
                ;*************************************************************


  colhead=strarr(3)
  if ps.toggled eq 0 then begin
        colhead(0) ='        Electron    Electron      NH/NE'
        colhead(1) ='      Temperature    Density      Ratio'
        colhead(2) ='        (log(K))     (cm-3) '
  endif else begin
        colhead(0) ='        Electron    Electron      NH/NE'
        colhead(1) ='      Temperature   Pressure      Ratio'
        colhead(2) ='        (log(K))     (Kcm-3) '
  endelse               

  rc = widget_label(base1,value='Model Data',font=font_large)

  colhead1=widget_text(base1,value=colhead,     $
                                font=font_small,ysize=3)

  densid = cw_adas_table(base1, modeldata, /scroll,                     $
                     ORDER = [1,0,0], LIMITS = [2,1,1], CELLSIZE = 12,$
                     FONTS = edit_fonts, FONT_LARGE = font_large,       $
                     FONT_SMALL = font_small)

                ;**** Default densities button ****

  baseb = widget_base(base1,/row)
  defdid = widget_button(baseb,value='  Default Data   ',font=font_large)
  cleardid = widget_button(baseb,value='    Clear Data   ',font=font_large)

  ionbbase = widget_base(tablebase,/column,/frame)
  ionlabel = widget_label(ionbbase,value='Ionisation balance')
  idepsel=['Density independent','Density dependent']
  idepid=cw_bgroup(ionbbase,idepsel,set_value=ps.itoggle,font=font_small,$
                        /exclusive,/row)


                ;************************************
                ;**** Transition Selection Table ****
                ;************************************

  botbase = widget_base(parent,/column,/frame)
  sbase = widget_base(botbase,/row)                              
  rc = widget_label(sbase,value='Select Transitions By:',font=font_large) 
  selections=['Indices','Wavelengths']
  transid=cw_bgroup(sbase,selections,set_value=ps.toggle,font=font_small,$
                        /exclusive,/row)
  selecbase=widget_base(botbase,/column)
  tselbase = widget_base(selecbase,/row,/frame)

  selid = widget_text(tselbase, value=lstrga,/scroll,ysize=10, $
                        xsize=26, FONT=font_small)

  if ps.ntrans eq 0 then begin
     leveldata = make_array(3,ps.ndtrans,/STRING)
  endif else begin
     leveldata = make_array(3,ps.ndtrans,/STRING)
     leveldata(0,*) = strtrim(string(ps.lmax),2)
     leveldata(1,*) = strtrim(string(ps.lmin),2)
     leveldata(2,*) = strtrim(string(ps.swl),2)
     leveldata(0:1,ps.ntrans:ps.ndtrans-1)=''
     index=where(leveldata(2,*) eq 0.0)
     values=fix(index)
     leveldata(2,values)=''
  endelse
  
  colhead=[['Upper','Lower','Assigned'],['Level','Level','Wavelength']]
  fltint=['(I2)','(I2)','(F7.2)']
  lselid = cw_adas412_table(tselbase, leveldata, lstrga, $
             UNITSTITLE = 'Levels', COLEDIT=[1,1,1],$
             COLHEAD = colhead, /scroll, $
             TITLE = 'Transitions', /gaps, /difflen,$
             LIMITS = [2,2,2], CELLSIZE = 12, $
             FONTS = edit_fonts, FONT_LARGE = font_large, $
             FONT_SMALL = font_small, FLTINT=fltint )                
  
  wlmn=ps.wavn
  wlmx=ps.wavx
  mxj=ps.imaxj

  wbase = widget_base(botbase,/row,/frame)
  rc = widget_label(wbase,value='Wavelength Max. (A):',font=font_large)
  wmax = widget_text(wbase,value=wlmx,/edit,xsize=6,font=font_large)
  rc = widget_label(wbase,value='Wavelength Min. (A):',font=font_large)
  wmin = widget_text(wbase,value=wlmn,/edit,xsize=6,font=font_large)
  rc = widget_label(wbase,value='Highest Level Included',font=font_large)
  maxj = widget_text(wbase,value=mxj,/edit,xsize=3,font=font_large)    

 
  if ps.toggle eq 0 then begin
        widget_control, selecbase, sensitive=1                  
        widget_control, wbase, sensitive=ps.toggle              
  endif else begin
        widget_control, selecbase, sensitive=0                  
        widget_control, wbase, sensitive=ps.toggle              
  endelse
   

                ;**** Error message ****
  messid = widget_label(parent,font=font_large, $
        value='Edit the processing options data and press Done to proceed')

                ;**** add the exit buttons ****
  base = widget_base(parent,/row)
  menufile = bitfile + '/menu.bmp'
  read_X11_bitmap, menufile, bitmap1
  outid = widget_button(base, value=bitmap1)          ;menu button
  cancelid = widget_button(base,value='Cancel',font=font_large)
  doneid = widget_button(base,value='Done',font=font_large)
  
                ;**** create a state structure for the pop-up ****
                ;**** window.                                 ****
  new_state = { RUNID:runid     ,       OUTID:outid,                            $
                    LSELID:lselid       ,       DENSID:densid,                      $
                    DEFDID:defdid       ,       CLEARDID:cleardid,                  $     
                    COLHEAD1:colhead1,  DEFT:ps.deft,           $
                    DINE:ps.dine,               CANCELID:cancelid, $
                    DONEID:doneid,                  $
                    MESSID:messid, NUM_FORM:num_form,               $
                    NDTEM:ndtem, NDTRANS:ndtrans, $
                    MAXT:ps.maxt, MAXDT:ps.maxdt,IFOUT:ps.ifout,    $
                    IL:il, FONT:font_large, $
                    TOGGLE:ps.toggle, TRANSID:transid,              $
                    TOGGLED:ps.toggled,                                     $
                    IDEPID:idepid,     ITOGGLE:ps.itoggle,                  $
                    SELECBASE:selecbase, WBASE:wbase,               $
                    ISEL:ps.isel,                                           $
                    MAXJ:maxj,   WMIN:wmin,                         $
                    WMAX:wmax  }


                ;**** Save initial state structure ****

  widget_control, first_child, set_uvalue=new_state, /no_copy 

  RETURN, parent

END

