; Copyright (c) 1997 Strathclyde University .
; SCCS info: Module @(#)$Header: /home/adascvs/idl/adas4xx/adas412/adas412.pro,v 1.7 2013/03/25 15:06:58 mog Exp $ Date $Date: 2013/03/25 15:06:58 $
;+
; PROJECT:
;       ADAS
;
; NAME:
;       ADAS412
;
; PURPOSE:
;       The highest level routine for the ADAS 412 program.
;
; EXPLANATION:
;       This routine is called from the main adas system routine, adas.pro,
;       to start the ADAS 412 application.  Associated with adas412.pro
;       is a FORTRAN executable.  The IDL code provides the user
;       interface and output graphics whilst the FORTRAN code reads
;       in data files, performs numerical processing and creates the
;       output files.  The IDL code communicates with the FORTRAN
;       executable via a bi-directional UNIX pipe.  The unit number
;       used by the IDL for writing to and reading from this pipe is
;       allocated when the FORTRAN executable is 'spawned' (see code
;       below).  Pipe communications in the FORTRAN process are to
;       stdin and stdout, i.e streams 5 and 6.
;
;       The FORTRAN code is an independent process under the UNIX system.
;       The IDL process can only exert control over the FORTRAN in the
;       data which it communicates via the pipe.  The communications
;       between the IDL and FORTRAN must be exactly matched to avoid
;       input conversion errors.  The correct ammounts of data must be
;       passed so that neither process 'hangs' waiting for communications
;       which will never occur.
;
;       The FORTRAN code performs some error checking which is
;       independent of IDL.  In cases of error the FORTRAN may write
;       error messages.  To prevent these error messages from conflicting
;       with the pipe communications all FORTRAN errors are written to
;       output stream 0, which is stderr for UNIX.  These error messages
;       will appear in the window from which the ADAS session/IDL session
;       is being run.
;
;       In the case of severe errors the FORTRAN code may terminate
;       itself prematurely.  In order to detect this, and prevent the
;       IDL program from 'hanging' or crashing, the IDL checks to see
;       if the FORTRAN executable is still an active process before
;       each group of pipe communications.  The process identifier
;       for the FORTRAN process, PID, is recorded when the process is
;       first 'spawned'.  The system is then checked for the presence
;       of the FORTRAN PID.
;
; USE:
;       First the system settings must be established by calling
;       adas_sys_set.pro then adas205.pro is called to start the
;       ADAS 412 application;
;
;       adas_sys_set, adasrel, fortdir, userroot, centroot, $
;                     devlist, devcode, font_large, font_small, edit_fonts
;       adas412,   adasrel, fortdir, userroot, centroot, devlist, $
;                  devcode, font_large, font_small, edit_fonts
;
; INPUTS:
;       ADASREL - A string indicating the ADAS system version, 
;                 e.g ' ADAS RELEASE: ADAS93 V1.11'.  The first
;                 character should be a space.
;
;       FORTDIR - A string holding the path to the directory where the
;                 FORTRAN executables are, e.g '/disk/adas/fortran/exec'
;
;       USERROOT - A string holding the path to the root directory of
;                  the user's adas data. e.g '/disk/bowen/adas'
;                  This root directory will be used by adas to construct
;                  other path names.  For example the users default data
;                  for adas205 should be in /disk/bowen/adas/adf04.  In
;                  particular the user's default interface settings will
;                  be stored in the directory USERROOT+'/defaults'.  An
;                  error will occur if the defaults directory does not
;                  exist.
;
;       CENTROOT - Like USERROOT, but this directory points to the
;                  central data area for the system.  User defaults are
;                  not stored on CENTROOT.
;
;       DEVLIST - A string array of hardcopy device names, used for
;                 graphical output. e.g ['Post-Script','HP-PCL','HP-GL']
;                 This array must mirror DEVCODE.  DEVCODE holds the
;                 actual device names used in a SET_PLOT statement.
;
;       DEVCODE - A string array of hardcopy device code names used in
;                 the SET_PLOT statement, e.g ['PS', 'PCL', 'HP']
;                 This array must mirror DEVLIST.
;
;       FONT_LARGE - The name of a larger font e.g 'courier_bold14'
;
;       FONT_SMALL - The name of a smaller font e.g 'courier_bold12'
;
;       EDIT_FONTS - A structure of two fonts used in the table editor
;                    adas_edtab.pro, {font_norm:'',font_input:''}.
;                    The two fonts are used to differentiate between
;                    editable and non-editable parts of the table. You
;                    may have to experiment to get two fonts which give
;                    the same default spacing in the edit widget.
;                    e.g {font_norm:'helvetica_bold14', $
;                         font_input:'helvetica_oblique14'}
;
; OPTIONAL INPUTS:
;       None
;
; OUTPUTS:
;       None - Note this routine should leave all inputs un-modified.
;
; OPTIONAL OUTPUTS:
;       None
;
; KEYWORD PARAMETERS:
;       None
;
; CALLS:
;       FIND_PROCESS    Checks to see if a given process is active.
;       DCSPF0          Pipe comms with FORTRAN DCSPF0 routine.
;       DCDATA          Obtains Model Plasma Data
;       BXSETP          Pipe comms with FORTRAN BXSETP routine.
;       DCISPF          Pipe comms with FORTRAN DCISPF routine.
;       XXDATE          Get date and time from operating system.
;       DCSPF1          Pipe comms with FORTRAN DCSPF1 routine.
;       DCOUTG          Pipe comms with FORTRAN DCOUTG routine.
;
; SIDE EFFECTS:
;       This routine spawns a FORTRAN executable.  Note the pipe 
;       communications routines listed above.  In addition to these
;       pipe communications there is one explicit communication of the
;       date to the FORTRAN code, search for 'printf,pipe' to find it.
;
; CATEGORY:
;       Adas system.
;       
; WRITTEN:
;       Richard Martin, University of Strathclyde, October 1997
;         Based on ADAS205 v1.30
;
; MODIFIED:
;       1.1     Richard Martin    
;               First release.
;       1.2     Richard Martin    
;               Increased version no. to 1.2.
;       1.3     Richard Martin
;               Increased version no. to 1.3.
;       1.4     Richard Martin
;               Increased version no. to 1.4.
;       1.5     Richard Martin
;               Increased version no. to 1.5.
;       1.6     Richard Martin
;               Increased version no. to 1.6.
;       1.3     Martin O'Mullane
;                 - Change argument list to dcispf and remove call to
;                   dcdata since way of calculating the nh/ne ratio
;                   has chabged.
;                 - Increased version no. to 1.7.
;
; VERSION:
;       1.1     23-10-97
;       1.2     09-03-98
;       1.3     23-06-98
;       1.4     04-12-98
;       1.5     18-03-02
;       1.6     08-09-03
;       1.7     25-03-2013
;-
;-----------------------------------------------------------------------------

PRO ADAS412,    adasrel, fortdir, userroot, centroot,                   $
                devlist, devcode, font_large, font_small, edit_fonts

                ;************************
                ;**** Initialisation ****
                ;************************

  adasprog = ' PROGRAM: ADAS412 V1.7'
  lpend = 0
  gomenu = 0
  deffile = userroot+'/defaults/adas412_defaults.dat'
  device = ''
  bitfile = centroot + '/bitmaps'


                ;******************************************
                ;**** Search for user default settings ****
                ;**** If not found create defaults     ****
                ;******************************************
        
  files = findfile(deffile)
  if files[0] eq deffile then begin
    restore,deffile
    inval.centroot1 = centroot+'/adf04/'
    inval.userroot1 = userroot+'/adf04/'
    inval.centroot2 = centroot+'/adf11/'
    inval.userroot2 = userroot+'/adf11/'
       
  endif else begin

    inval = { ROOTPATH1:userroot+'/adf04/',         $
              FILE1:'',                             $
              CENTROOT1:centroot+'/adf04/',         $
              USERROOT1:userroot+'/adf04/',         $
              ROOTPATH2:userroot+'/adf11/',         $
              CENTROOT2:centroot+'/adf11/',         $
              USERROOT2:userroot+'/adf11/',         $
              YEAR:'', ELEM:''                      }

    procval = {NDTRANS:-1}

    outval = {                                      $
                GRPOUT:0, GTIT1:'', GRPSCAL:0,      $
                XMIN:'',  XMAX:'',                  $
                YMIN:'',  YMAX:'',                  $
                HRDOUT:0, HARDNAME:'',              $
                GRPDEF:'', GRPFMESS:'',             $
                GRPSEL:-1, GRPRMESS:'',             $
                DEVSEL:-1, GRSELMESS:'',            $
                TEXOUT:0,                           $
                TEXREP:0, TEXDSN:'GOFT.PASS',       $
                TEXDEF:'',TEXMES:'',                $
                FLIPVAL:0                           }

  end


                ;****************************
                ;**** Start fortran code ****
                ;****************************

  spawn, fortdir + '/adas412.out',unit=pipe,/noshell,PID=pid

                ;************************************************
                ;**** Get date and write to fortran via pipe ****
                ;************************************************

  date = xxdate()
  printf,pipe,date[0]

LABEL100:
                ;**** Check FORTRAN still running ****
 
  if find_process(pid) eq 0 then goto, LABELEND

                ;************************************************
                ;**** Communicate with b5spf0 in fortran and ****
                ;**** invoke user interface widget for       ****
                ;**** Data file selection                    ****
                ;************************************************

  dcspf0, pipe, inval, dsninc, dsnini,  rep, FONT=font_large
  
  
                ;**** If cancel selected then end program ****

  if rep eq 'YES' then goto, LABELEND

                ;**** Check FORTRAN still running ****

  if find_process(pid) eq 0 then goto, LABELEND


                ;*************************************
                ;**** Read pipe comms from bxsetp ****
                ;**** and create list of levels   ****
                ;**** lstrga for output on graph. ****
                ;*************************************

  bxsetp, pipe, sz0, sz, scnte, sil, lfpool, strga

  levend = where(strtrim(strga,2) eq '*** END OF LEVELS ***')
  listsize = size(strga)
  if levend[0] ge 0 then listend=levend[0]-1 else listend=listsize[1]-1
  levtemp = indgen(listend+1)+1
  lstrga = strmid(string(levtemp[0:listend]),6,5) + ' ' + strga[0:listend]
  il=fix(listend)
  
  
LABEL200:
                ;**** Check FORTRAN still running ****

  if find_process(pid) eq 0 then goto, LABELEND


                ;************************************************
                ;**** Communicate with bcispf in fortran and ****
                ;**** invoke user interface widget for       ****
                ;**** Processing options                     ****
                ;************************************************

  dcispf, pipe, lpend, procval,$
          inval.year,inval.elem, lstrga, dsninc, dsnini, bitfile, gomenu,$
          FONT_LARGE=font_large, FONT_SMALL=font_small, $
          EDIT_FONTS=edit_fonts
            
                ;**** If menu button clicked, tell FORTRAN to stop ****

  if gomenu eq 1 then goto, LABELEND

                ;**** If cancel selected then goto 100 ****

  if lpend eq 1 then goto, LABEL100
  

                ;**** Create an information widget ****

  widget_control, /hourglass
  widgetbase = widget_base(/column,xoffset=300,yoffset=200,     $
                           title = "ADAS410 INFORMATION")
  lab0 = widget_label(widgetbase, value='')
  lab1 = widget_label(widgetbase, value='')
  lab2 = widget_label(widgetbase, font=font_large,              $
                      value="       Processing - please wait       ")
  lab3 = widget_label(widgetbase, value='')
  lab4 = widget_label(widgetbase, value='')
  widget_control, widgetbase, /realize

            ;**** wait for signal from FORTRAN ****
                ;**** before continuing ****

  idum = 0
  readf, pipe, idum
  widget_control, widgetbase, /destroy


                ;**** Create header for output. ****

  header = adasrel+adasprog+' DATE: '+date(0)+' TIME: '+date(1)

LABEL300:
                ;**** Check FORTRAN still running ****
  if find_process(pid) eq 0 then goto, LABELEND

                ;************************************************
                ;**** Communicate with b5spf1 in fortran and ****
                ;**** invoke user interface widget for       ****
                ;**** Output options                         ****
                ;************************************************

  dcspf1, pipe, lpend, outval, dsninc, bitfile, gomenu, $
                DEVLIST=devlist, FONT=font_large

                ;**** If menu button clicked, tell FORTRAN to stop ****

  if gomenu eq 1 then begin
      printf, pipe, 1
      outval.texmes = ''
      goto, LABELEND
  endif else begin
      printf, pipe, 0
  endelse

                ;**** If cancel selected then erase output ****
                ;**** messages and goto 200.               ****

  if lpend eq 1 then begin
    outval.texmes = ''
    goto, LABEL200
  end

                ;**** If graphical output requested ****

                ;************************************************
                ;**** Communicate with bxoutg in fortran and ****
                ;**** invoke widget Graphical output         ****
                ;************************************************

  if outval.grpout eq 1 then begin
                ;**** User explicit scaling ****
    grpscal = outval.grpscal
    xmin = outval.xmin
    xmax = outval.xmax
    ymin = outval.ymin
    ymax = outval.ymax

                ;**** Hardcopy output ****
 
    hrdout = outval.hrdout
    hardname = outval.hardname
    if outval.devsel ge 0 then device = devcode(outval.devsel)
    

    dcoutg, pipe, inval.year,inval.elem, procval.tine, procval.isel,$
                  procval.lmax, procval.lmin,   $
                  procval.title, outval.gtit1, il,grpscal, dsninc,$
                  lstrga, xmin, xmax, ymin, ymax, $
                  hrdout, hardname, device, header, bitfile, gomenu,    $
                  FONT=font_large

                ;**** If menu button clicked, tell FORTRAN to stop ****

    if gomenu eq 1 then begin
        printf, pipe, 1
        outval.texmes = ''
        goto, LABELEND
    endif else begin
        printf, pipe, 0
    endelse

  end

                ;**** Back for more output options ****
GOTO, LABEL300

LABELEND:

                ;**** Save user defaults ****

save,inval,procval,outval,filename=deffile


END
