;----------------------------------------------------------------------
;+
; PROJECT    :  ADAS
;
; NAME       :  dcnhne
;
; PURPOSE    :  Returns solar NH/NE ratios.
;
;               NAME      TYPE   DETAILS
; INPUTS     :  te        real() Array of temperatures (K)
;     
; OUTPUTS    :  Array of NH/NE at input temperatures.
;
; CALLS      :
;               DCDATA : Obtains tabulated values of known
;                        ratios and derivatives of these
;                        ratios.
;
; AUTHOR     :  Allan Whiteford
;
; DATE       :  03-02-2009
;
; MODIFIED
;       1.1     Allan Whiteford
;                 - First release
;
; DATE
;       1.1     03-02-2009
;
;-
;----------------------------------------------------------------------
function dcnhne,te
	dcdata,tempdat,nhnedat,nhne_deriv
	nhne= nr_splint(tempdat,nhnedat,nhne_deriv,alog10(te))
	return,nhne
end
