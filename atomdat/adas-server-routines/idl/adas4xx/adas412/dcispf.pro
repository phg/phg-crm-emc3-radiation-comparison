; Copyright (c) 1997 Strathclyde University .
; SCCS info: Module @(#)$Header: /home/adascvs/idl/adas4xx/adas412/dcispf.pro,v 1.4 2013/03/25 15:06:58 mog Exp $ Date $Date: 2013/03/25 15:06:58 $
;+
; PROJECT:
;       ADAS IBM MVS to DEC UNIX conversion
;
; NAME:
;       B5ISPF
;
; PURPOSE:
;       IDL user interface and communications with ADAS412 FORTRAN
;       routine DCISPF via pipe.
;
; EXPLANATION:
;       The routine begins by reading information from the ADAS412
;       FORTRAN process via a UNIX pipe.  Then part of the ADAS412
;       IDL user interface is invoked to determine how the user
;       wishes to process the input dataset.  When the user's
;       interactions are complete the information gathered with
;       the user interface is written back to the FORTRAN process
;       via the pipe.  Communications are to the FORTRAN subroutine
;       DCISPF.
;
; USE:
;       The use of this routine is specific to ADAS412, see adas412.pro.
;
; INPUTS:
;       PIPE    - The IDL unit number of the bi-directional pipe to the
;                 ADAS412 FORTRAN process.
;
;       LPEND   - Integer, 0 if the user pressed the 'Done' button
;                 or 1 if the user pressed 'Cancel'.  Maps directly onto
;                 the logical variable LPEND in ADAS412 FORTRAN.
;
;       VALUE   - A structure which determines the initial settings of
;                 the processing options widget.  The initial value is
;                 set in adas412.pro.  If adas412.pro passes a blank 
;                 dummy structure of the form {NMET:-1} into VALUE then
;                 VALUE is reset to a default structure.
;
;                 The VALUE structure is;
;                       value = { TITLE:''  , IFOUT:0     ,     $
;                                 MAXDT:0   , MAXT:0      ,     $
;                                 TINE:temp , DINE:temp   ,     $
;                                 PINE:temp , NHNE:temp   ,     $
;                                 DEFT:deft , TOGGLE:0    ,     $
;                                 TOGGLED:0 , ITOGGLE:1   ,     $
;                                 ISEL:0    ,                   $
;                                 NDTRANS:ndtrans,  NTRANS:0,   $
;                                 LMIN:ltemp, LMAX:ltemp  ,     $
;                                 SWL:swl       , WAVN:''     , $
;                                 WAVX:''   , IMAXJ:''      }
;
;                 See cw_adas412_proc.pro for a full description of this
;                 structure and related variables ndmet, ndtem and ndden.
;
;       YEAR    - String, recombined ion charge read.
;
;       ELEM    - String, nuclear charge read.
;
;       LSTRGA- String array, level designations.
;
;       DSNINC  - String; The full system file name of the input COPASE
;                 dataset selected by the user for processing.
;
;       DSNINI  - String; The full system file name of the required 
;                       'adf11/acd<year>/acd<year>_<elem>.dat' dataset 
;                       year & elem selected by the user. The default set of
;                       temperatures are obtained from this file.
;
;     BITFILE - String; the path to the dirctory containing bitmaps
;                 for the 'escape to series menu' button.
;
; OPTIONAL INPUTS:
;       None
;
; OUTPUTS:
;       VALUE   - On output the structure records the final settings of
;                 the processing selections widget if the user pressed the
;                 'Done' button, otherwise it is not changed from input.
;
;     GOMENU  - Int; flag - set to 1 if user has selected 'escape direct
;                 to series menu , otherwise 0
;
; OPTIONAL OUTPUTS:
;       None
;
; KEYWORD PARAMETERS:
;       FONT_LARGE - The name of a larger font.
;
;       FONT_SMALL - The name of a smaller font.
;
;       EDIT_FONTS - A structure of two fonts used in the table editor
;                    adas_edtab.pro, {font_norm:'',font_input:''}.
;
; CALLS:
;       ADAS412_PROC    Invoke the IDL interface for ADAS205 data
;                       processing options/input.
;
; SIDE EFFECTS:
;       Two way communications with ADAS412 FORTRAN via the
;       bi-directional UNIX pipe.
;
; CATEGORY:
;       Adas system.
;
; WRITTEN:
;       Richard Martin, Strathclyde University, October 1997.
;       Based on b5ispf.pro v 1.8
;
; MODIFIED:
;       1.1     Richard Martin
;               First release.
;       1.2     Richard Martin
;               Added if statement which uses appropriate spline routine name,
;               according to IDL version.
;       1.3     Richard Martin
;               Corrected error in the value passed to IDDEP in Fortran -
;               Added new toggle value itoggle.
;       1.4     Martin O'Mullane
;                - Acquire temperatures from adf11 acd file with read_adf11
;                  rather than directly opening the file.
;                - Calculate nh/ne ratio with adas412_nhne.pro function. The
;                  argument list has changed.
;
; VERSION:
;       1.1     23-10-97
;       1.2     01-12-97
;       1.3     23-06-98
;       1.4     23-03-2013
;
;-
;-----------------------------------------------------------------------------

PRO dcispf, pipe, lpend, value, $
                year, elem, lstrga, dsninc, dsnini, bitfile, gomenu,    $
                FONT_LARGE=font_large, FONT_SMALL=font_small, $
                EDIT_FONTS=edit_fonts

                ;**** Set defaults for keywords ****

  IF NOT (KEYWORD_SET(font_large)) THEN font_large = ''
  IF NOT (KEYWORD_SET(font_small)) THEN font_small = ''
  IF NOT (KEYWORD_SET(edit_fonts)) THEN $
                                edit_fonts = {font_norm:'',font_input:''}


                ;**** Declare variables for input ****
  lpend = 0
  gomenu = 0
  ndtem = 0
  il = 0

                ;********************************
                ;**** Read data from fortran ****
                ;********************************

  idum=0
  readf,pipe,idum
  ndtem=idum
  readf,pipe,idum
  ndtrans=idum
  
                ;*******************************************
                ;**** Set default value if non provided ****
                ;*******************************************
                
  if value.ndtrans lt 0 then begin
    temp = dblarr(ndtem)
    deft = dblarr(3,ndtem)
    ltemp= intarr(ndtrans)
    swl  = fltarr(ndtrans)

    value = { TITLE:''  ,       IFOUT:0         ,       $
                  MAXDT:0   ,   MAXT:0          ,       $
                  TINE:temp ,   DINE:temp       ,       $
                  PINE:temp ,   NHNE:temp       ,       $
                  DEFT:deft ,   TOGGLE:0        ,       $
                  TOGGLED:0 ,   ITOGGLE:1       ,       $
                  ISEL:0    ,                           $
                  NDTRANS:ndtrans,NTRANS:0      ,       $
                  LMIN:ltemp,   LMAX:ltemp      ,       $
                  SWL:swl       ,       WAVN:'' ,       $
                  WAVX:''   ,   IMAXJ:''          }
  end    

      
                ;***************************************************
                ;**** Obtain default temps. from adf11 acd file ****
                ;**** and obtain interpolated values at inter-  ****
                ;**** mediate points. Store in array DEFT       ****
                ;***************************************************

  
  read_adf11, file=dsnini, fulldata=all
  dummy_dens = all.ddens
  dummy_temp = all.dtev
  
  itemp = n_elements(dummy_temp)
  new_temp=congrid(dummy_temp,2*itemp,/INTERP)
  value.DEFT[0,0:2*itemp-2]=alog10(10^(new_temp(0:2*itemp-2))*11605.4)
  
  a=itemp mod 2
  if a eq 0 then value.maxdt=2*itemp-2 else value.maxdt=2*itemp-1

                ;************************************************
                ;**** Obtain interpolated values for nh/ne   ****
                ;**** ratios. Store in DEFT.                 ****
                ;************************************************

  dummy_nhne=fltarr(ndtem)
  
  value.DEFT[2,0:2*itemp-2] = adas412_nhne((10.0^new_temp[0:2*itemp-2])*11605.4)
  
                ;****************************************
                ;**** Pop-up processing input widget ****
                ;****************************************

  adas412_proc, value, year, elem, lstrga, dsninc, $
                ndtem, ndtrans, il, action, bitfile, $
                FONT_LARGE=font_large, FONT_SMALL=font_small, $
                EDIT_FONTS=edit_fonts


                ;******************************************
                ;**** Act on the event from the widget ****
                ;******************************************
                ;**** There are only two possible events ****
                ;**** 'Done' and 'Cancel'.               ****
                ;********************************************
  gomenu = 0
  if action eq 'Done' then begin
    lpend = 0
  endif else if action eq 'Cancel' then begin
    lpend = 1 
  endif else begin
    lpend = 0
    gomenu = 1
  end

                ;*******************************
                ;**** Write data to fortran ****
                ;*******************************

  printf,pipe,lpend
  printf,pipe,gomenu
           
  printf,pipe,value.isel
  printf,pipe,value.ntrans

  if value.isel eq 1 then begin
     printf,pipe,value.lmin
     printf,pipe,value.lmax
     printf,pipe,value.swl
  endif else begin 
     printf,pipe,fix(value.imaxj)
     printf,pipe,float(value.wavn)
     printf,pipe,float(value.wavx)
  endelse 
  
  iddep=value.itoggle           ;corresponds to variable in fortran.
  printf,pipe,iddep
             
  printf,pipe,value.maxt

  for i=0, value.maxt-1 do begin  
    printf,pipe,value.tine[i]
    printf,pipe,value.dine[i]
    printf,pipe,value.pine[i]     
    printf,pipe,value.nhne[i]               
  endfor

END
