; Copyright (c) 1997 Strathclyde University .
; SCCS info: Module @(#)$Header: /home/adascvs/idl/adas4xx/adas412/cw_adas412_out.pro,v 1.2 2004/07/06 12:49:01 whitefor Exp $ Date $Date: 2004/07/06 12:49:01 $
;+
; PROJECT:
;       ADAS IBM MVS to DEC UNIX conversion
;
; NAME:
;	CW_ADAS412_OUT()
;
; PURPOSE:
;	Produces a widget for ADAS412 output options.
;
; EXPLANATION:
;	This function declares a compound widget consisting of the
;	graphical output selection widget cw_adas_gr_sel.pro, and an
;	output file widget cw_adas_outfile.pro.  The text output
;	file is specified in this widget and are for tabular (paper.txt)
;	output.  This widget also includes a button for browsing the 
;	comments from the input dataset, a 'Cancel' button and a 'Done' button.
;
;	The compound widgets cw_adas_gr_sel.pro and cw_adas_outfile.pro
;	included in this file are self managing.  This widget only
;	handles events from the 'Done' and 'Cancel' buttons.
;
;	This widget only generates events for the 'Done' and 'Cancel'
;	buttons.
;
; USE:
;	This widget is specific to ADAS412, see adas412_out.pro for use.
;
; INPUTS:
;	PARENT	- Long integer; ID of parent widget.
;
;	DSNINC	- Name of input dataset for this application.
;
;     BITFILE - String; the path to the dirctory containing bitmaps
;                 for the 'escape to series menu' button.
;
; OPTIONAL INPUTS:
;	None.
;
; OUTPUTS:
;	The return value of this function is the ID of the declared widget.
;
; OPTIONAL OUTPUTS:
;	None.
;
; KEYWORD PARAMETERS:
;
;	DEVLIST	- A string array;  Each element of the array is the
;		  name of a hardcopy graphics output device.  This
;		  list must mirror the DEVCODE list.  DEVLIST is the
;		  list which the user will see.
;		  e.g ['Post-Script','HP-PCL','HP-GL']
;
;	VALUE	- A structure which determines the initial settings of
;		  the output options widget.  The structure is made
;		  up of four parts.  Each part is the same as the value
;		  structure of one of the four main compound widgets
;		  included in this widget.  See cw_adas_gr_sel and
;		  cw_adas_outfile for more details.  The default value is;
;
;		      { GRPOUT:0, GTIT1:'', 				$
;			GRPSCAL:0, 						$
;			XMIN:'',  XMAX:'',   				$
;			YMIN:'',  YMAX:'',   				$
;			HRDOUT:0, HARDNAME:'', 				$
;			GRPDEF:'', GRPFMESS:'', 			$
;			GRPRMESS:'', 					$
;			DEVSEL:-1, GRSELMESS:'', 			$
;			TEXOUT:0, 		 				$
;			TEXREP:0, TEXDSN:'', 				$
;			TEXDEF:'',TEXMES:'', 			}
;
;		  For CW_ADAS_GR_SEL;
;			GRPOUT	   Integer; Activation button 1 on, 0 off
;			GTIT1	   String; Graph title
;			GRPSCAL	   Integer; Scaling activation 1 on, 0 off
;			XMIN	   String; x-axis minimum, string of number
;			XMAX	   String; x-axis maximum, string of number
;			YMIN	   String; y-axis minimum, string of number
;			YMAX	   String; y-axis maximum, string of number
;			HRDOUT	   Integer; Hard copy activ' 1 on, 0 off
;			HARDNAME   String; Hard copy output file name
;			GRPDEF	   String; Default output file name 
;			GRPFMESS   String; File name error message
;			GRPRMESS   String; Scaling ranges error message
;			DEVSEL	   Integer; index of selected device in DEVLIST
;			GRSELMESS  String; General error message
;
;		  For CW_ADAS_OUTFILE tabular output (paper.txt);
;			TEXOUT	Integer; Activation button 1 on, 0 off
;			TEXREP	Integer; Replace but' 1 on, 0 off, -1 no button
;			TEXDSN	String; Output file name
;			TEXDEF	String; Default file name
;			TEXMES	String; file name error message
;
;
;	UVALUE	- A user value for this widget.
;
;	FONT	- String; the name of a font to be used for all text in
;		  this widget.  Default to system default font.
;
; CALLS:
;	CW_ADAS_DSBR	Input dataset name and comments browse button.
;	CW_ADAS_GR_SEL	Graphical output selection widget.
;	CW_ADAS_OUTFILE	Output file name entry widget.
;	See side effects for other related widget management routines.
;
; SIDE EFFECTS:
;       A COMMON BLOCK: CW_OUT205_BLK is used which is private to
;       this widget.
;
;	OUT412_GET_VAL()
;	OUT412_EVENT()
;
; CATEGORY:
;	Compound Widget
;
; WRITTEN:
;       Richard Martin, University of Strathclyde, October 1997.
;	  Based on cw_adas205_out.pro, v1.12.
;
; MODIFIED:
;       1.1	Richard Martin    
;           First release.
;	  1.2	Richard Martin
;		Removed obsolete cw_loadstate/savestate statements.
;
; VERSION:
;       1.1     23-10-97
;	  1.2	08-03-02
;-
;-----------------------------------------------------------------------------

FUNCTION out412_get_val, id


                ;**** Return to caller on error ****
  ON_ERROR, 2

                ;**** Retrieve the state ****

  first_child = widget_info(id,/child)
  widget_control, first_child, get_uvalue=state, /no_copy

		;**** Get graphical output settings ****
  widget_control,state.grpid,get_value=grselval

		;**** Get text output settings ****
  widget_control,state.paperid,get_value=papos


  os = { 	GRPOUT:grselval.outbut, GTIT1:grselval.gtit1, 		$
		GRPSCAL:grselval.scalbut, 					$
		XMIN:grselval.xmin,     XMAX:grselval.xmax,   		$
		YMIN:grselval.ymin,     YMAX:grselval.ymax,   		$
		HRDOUT:grselval.hrdout, HARDNAME:grselval.hardname, 	$
		GRPDEF:grselval.grpdef, GRPFMESS:grselval.grpfmess, 	$
		GRPRMESS:grselval.grprmess, 					$
		DEVSEL:grselval.devsel, GRSELMESS:grselval.grselmess, $
		TEXOUT:papos.outbut,  				 		$
		TEXREP:papos.repbut,  TEXDSN:papos.filename, 		$
		TEXDEF:papos.defname, TEXMES:papos.message 	}

  widget_control, first_child, set_uvalue=state, /no_copy  

  RETURN, os 

END

;-----------------------------------------------------------------------------

FUNCTION out412_event, event


                ;**** Base ID of compound widget ****
  parent=event.handler

                ;**** Retrieve the state ****

  first_child = widget_info(parent,/child)
  widget_control, first_child, get_uvalue=state

		;*********************************
		;**** Clear previous messages ****
		;*********************************
  widget_control,state.messid,set_value=' '

                ;************************
                ;**** Process events ****
                ;************************
  CASE event.id OF


		;***********************
		;**** Cancel button ****
		;***********************

    state.cancelid: new_event = {ID:parent, TOP:event.top, 		$
				HANDLER:0L, ACTION:'Cancel'}

		;*********************
		;**** Done button ****
		;*********************

    state.doneid: begin

		;***************************************
		;**** Check for errors in the input ****
		;***************************************
	  error = 0

		;**** Get a copy of the widget value ****
	  widget_control,event.handler,get_value=os

		;**** Check for widget error messages ****
	  if os.grpout eq 1 and strtrim(os.grpfmess) ne '' then error=1
	  if os.grpout eq 1 and os.grpscal eq 1 and $
				strtrim(os.grprmess) ne '' then error=1
	  if os.grpout eq 1 and strtrim(os.grselmess) ne '' then error=1
	  if os.texout eq 1 and strtrim(os.texmes) ne '' then error=1

	  if error eq 1 then begin
	    widget_control,state.messid,set_value= $
				'**** Error in output settings ****'
	    new_event = 0L
	  end else begin
	    new_event = {ID:parent, TOP:event.top, HANDLER:0L, ACTION:'Done'}
	  end

        end

                ;**** Menu button ****

    state.outid: begin
            new_event = {ID:parent, TOP:event.top, HANDLER:0L,          $
                         ACTION:'Menu'}
        end

    ELSE: new_event = 0L

  ENDCASE

  RETURN, new_event
END

;-----------------------------------------------------------------------------

FUNCTION cw_adas412_out, topparent, dsninc, bitfile,		$
		DEVLIST=devlist, $
		VALUE=value, UVALUE=uvalue, $
		FONT=font


  IF (N_PARAMS() EQ 0) THEN MESSAGE, 'Must specify parent for cw_adas412_out'
  ON_ERROR, 2					;return to caller

		;**** Set defaults for keywords ****

  IF NOT (KEYWORD_SET(devlist)) THEN devlist = ''
  IF NOT (KEYWORD_SET(uvalue)) THEN uvalue = 0
  IF NOT (KEYWORD_SET(font)) THEN font = ''
  IF NOT (KEYWORD_SET(value)) THEN begin
	os = {out412_set, 						$
			GRPOUT:0, GTIT1:'', 				$
			GRPSCAL:0, 						$
			XMIN:'',  XMAX:'',   				$
			YMIN:'',  YMAX:'',   				$
			HRDOUT:0, HARDNAME:'', 				$
			GRPDEF:'', GRPFMESS:'', 			$
			GRPRMESS:'', 					$
			DEVSEL:-1, GRSELMESS:'', 			$
			TEXOUT:0, 						$
			TEXREP:0, TEXDSN:'', 				$
                  TEXDEF:'',TEXMES:'' 		}			

  END ELSE BEGIN
	os = {out412_set, 						$	
			GRPOUT:value.grpout, GTIT1:value.gtit1, 		$
			GRPSCAL:value.grpscal, 					$
			XMIN:value.xmin,     XMAX:value.xmax,   		$
			YMIN:value.ymin,     YMAX:value.ymax,   		$
			HRDOUT:value.hrdout, HARDNAME:value.hardname, 	$
			GRPDEF:value.grpdef, GRPFMESS:value.grpfmess, 	$
			GRPRMESS:value.grprmess, 				$
			DEVSEL:value.devsel, GRSELMESS:value.grselmess, $
			TEXOUT:value.texout, 				 	$
			TEXREP:value.texrep, TEXDSN:value.texdsn, 	$
			TEXDEF:value.texdef, TEXMES:value.texmes	} 	
  END

		;**********************************************
		;**** Create the 412 Output options widget ****
		;**********************************************

		;**** create base widget ****
  parent = widget_base( topparent, UVALUE = uvalue, $
			EVENT_FUNC = "out412_event", $
			FUNC_GET_VALUE = "out412_get_val", $
			/COLUMN)

		;**** Create base to hold the value of state ****

  first_child = widget_base(parent)

  cwid = widget_base(first_child,/column)

		;**** Add dataset name and browse button ****

  rc = cw_adas_dsbr(cwid,dsninc,font=font)

		;****************************************************
		;**** Add another base for the switching between ****
		;**** Graphics and text output options           ****
		;****************************************************

  flipbase = widget_base(cwid)

		;**** Widget for graphics selection ****
		;**** Note change in names for GRPOUT and GRPSCAL ****

  grselval = {  OUTBUT:os.grpout, GTIT1:os.gtit1, SCALBUT:os.grpscal, $
		XMIN:os.xmin, XMAX:os.xmax, 			$
		YMIN:os.ymin, YMAX:os.ymax, 			$
		HRDOUT:os.hrdout, HARDNAME:os.hardname, 	$
		GRPDEF:os.grpdef, GRPFMESS:os.grpfmess, 	$
		GRPSEL:'',	GRPRMESS:os.grprmess,		$
		DEVSEL:os.devsel, GRSELMESS:os.grselmess }

  base = widget_base(flipbase,/column,/frame)
  graphbase = base

  grpid = cw_adas_gr_sel(base, OUTPUT='Graphical Output',$
                        LISTTITLE='Graph Temperature', $
				DEVLIST=devlist, $
                        VALUE=grselval, FONT=font)
		;**** Widget for text output ****


  outfval = { OUTBUT:os.texout, REPBUT:os.texrep, APPBUT:'-1',$
	      FILENAME:os.texdsn, DEFNAME:os.texdef, MESSAGE:os.texmes }
  base = widget_base(base,/row,/frame)
  paperid = cw_adas_outfile(base, OUTPUT='Text Output', $
                        VALUE=outfval, FONT=font)

  widget_control, base


		;**** Error message ****

  messid = widget_label(cwid,value=' ',font=font)

		;**** add the exit buttons ****

  base = widget_base(cwid,/row)
  menufile = bitfile + '/menu.bmp'
  read_X11_bitmap, menufile, bitmap1
  outid = widget_button(base, value=bitmap1)          ;menu button
  cancelid = widget_button(base,value='Cancel',font=font)
  doneid = widget_button(base,value='Done',font=font)
  
		;*************************************************
		;**** create a state structure for the pop-up ****
		;**** window.                                 ****
		;*************************************************

  new_state = { GRPID		:	grpid, 			$
 		OUTID			:	outid, 			$
		PAPERID		:	paperid, 			$
		CANCELID		:	cancelid, 			$
		DONEID		:	doneid, 			$
		MESSID		:	messid,			$
		GRAPHBASE		:	graphbase		}

                ;**** Save initial state structure ****

  widget_control, first_child, set_uvalue=new_state, /no_copy

  RETURN, parent

END

