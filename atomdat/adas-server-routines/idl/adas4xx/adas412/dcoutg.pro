; Copyright (c) 1995 Strathclyde University .
; SCCS info: Module @(#)$Header: /home/adascvs/idl/adas4xx/adas412/dcoutg.pro,v 1.2 2004/07/06 13:29:07 whitefor Exp $ Date $Date: 2004/07/06 13:29:07 $
;+
; PROJECT:
;       ADAS IBM MVS to DEC UNIX conversion
;
; NAME:
;	BXOUTG
;
; PURPOSE:
;	Communication with ADAS412 FORTRAN via UNIX pipe and
;	graphics output.
;
; EXPLANATION:
;	The routine begins by reading information from the ADAS412
;	FORTRAN process via a UNIX pipe.  Then the IDL graphical
;	output routine for ADAS412 is invoked.  Communications are to
;	the FORTRAN subroutine DCOUTG.
;
; USE:
;	The use of this routine is specific to ADAS412 see adas412.pro.
;
; INPUTS:
;	PIPE	 - The IDL unit number of the bi-directional pipe to the
;		   ADAS412 FORTRAN process.
;
;	YEAR   - String, year of required adf11 data.
;
;	ELEM   - String, selected element.
;
;	TINE   - Output Electron Temperature array.
;
;	ISEL	 - Index for fortran: 1- transitions by index 3- by wavelength range.
;
;	LMIN	 - Lower indices of selected transitions.
;
;	LMAX	 - Maximum indices of selected transitions.
;
;	TITLE  - Title of the program run.
;
;	GTIT1	 - String; Title for graph.
;
;	IL	 - Integer; Number of energy levels.
;
;	GRPSCAL- Integer; 0 if default scaling required 1 if user
;		   specified scaling to be used.
;
;	DSNINC - String; Input COPASE data set name.
;
;	LSTRGA - String array; Level designations.
;
;	XMIN	 - String; User sepcified x-axis minimum, number as string.
;
;	XMAX	 - String; User sepcified x-axis maximum, number as string.
;
;	YMIN	 - String; User sepcified y-axis minimum, number as string.
;
;	YMAX	 - String; User sepcified y-axis maximum, number as string.
;
;	HRDOUT - Integer; 0 if no hardcopy required, 1 if hardcopy.
;
;	HARDNAME - String; file name for hardcopy output.
;
;	DEVICE - String; IDL name for hardcopy output device.
;
;	HEADER - ADAS version header information for inclusion in the
;		   graphical output.
;
;     BITFILE- String; the path to the dirctory containing bitmaps
;                 for the 'escape to series menu' button.
;
; OPTIONAL INPUTS:
;	None.
;
; OUTPUTS:
;       GOMENU  - Int; flag - set to 1 if user has selected 'escape direct
;                 to series menu , otherwise 0
;
; OPTIONAL OUTPUTS:
;	None.
;
; KEYWORD PARAMETERS:
;	FONT	- String; Name of the font to be used for the graphical
;		  output widget.
;
; CALLS:
;	ADAS412_PLOT	ADAS412 graphical output.
;
; SIDE EFFECTS:
;	This routine reads information from FORTRAN via a UNIX pipe.
;
; CATEGORY:
;	Adas system.
;
; WRITTEN:
;       Richard Martin, University of Strathclyde, October 1997.
;	  Based on b5outg.for v1.11 .
;
; MODIFIED:
;     1.1       	Richard Martin.
;                 First release.
;	1.2		Richard Martin.
;			Defined parameter 'ntrans' for adas412_plot when transitions
;			selected by wavelength. 
;		
; VERSION:
;     1.1       	24-10-97
;	1.2		01-10-97		
;-
;-----------------------------------------------------------------------------



PRO dcoutg, pipe, year, elem, tine, isel,lmax, lmin, title, gtit1, $
		il, grpscal, dsninc, lstrga, $
	    xmin, xmax, ymin, ymax, hrdout, hardname,	$
	    device, header, bitfile, gomenu, FONT=font


                ;**** Set defaults for keywords ****
                
  IF NOT (KEYWORD_SET(font)) THEN font = ''

		;********************************
		;**** Read data from fortran ****
		;********************************
  idum=0
  ddum=0.0d0

  readf,pipe,idum
  ntemp = idum  
  readf,pipe,idum
  ntmax = idum  		;This many G(T)'s will be read from FORTRAN.
  
  if isel eq 3 then begin
  	jmax=make_array(ntmax,/int)
  	jmin=make_array(ntmax,/int)
  	for i=0,ntmax-1 do begin
  		readf,pipe,idum
  		jmax(i)=idum
  		readf,pipe,idum
  		jmin(i)=idum
  	endfor
  	dummy=where(jmax,ntrans)
  	strlev=make_array(ntrans,/STRING)
  	for i=0,ntrans-1 do begin
  		strlev(i)='('+strtrim(string(i+1,format='(I2)'),2)+')' + $
  			'         ' + strtrim(string(jmax(i),format='(I2)'),2) + $
  			' - ' + strtrim(string(jmin(i),format='(I2)'),2)
  	end  	
  endif else begin	
  	dummy=where(lmax,ntmax)
  	ntrans=ntmax
  	strlev=make_array(ntmax,/STRING)
  	for i=0,ntmax-1 do begin
  		strlev(i)='('+strtrim(string(i+1,format='(I2)'),2)+')' + $
  			'         ' + strtrim(string(lmax(i),format='(I2)'),2) + $
  			' - ' + strtrim(string(lmin(i),format='(I2)'),2)
  	end  	
  endelse

  goft = dblarr(ntmax,ntemp)
  
  for i=0,ntmax-1 do begin
  	for j=0,ntemp-1 do begin
  		readf,pipe,ddum
  		goft(i,j)=ddum 
  	endfor
  endfor
		;****************************************
		;****************************************

 adas412_plot, ntrans, ntemp,				$
	     title,  gtit1,  dsninc,     		$
	     year,elem, grpscal,			$
	     xmin, xmax, ymin, ymax,  	     	$
	     il, lstrga, strlev, tine,  goft,	$
	     hrdout,hardname,device,header,     	$
	     bitfile, gomenu,			    	$
	     FONT=font


END
