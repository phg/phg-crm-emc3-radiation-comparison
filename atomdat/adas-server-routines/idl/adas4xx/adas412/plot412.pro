; Copyright (c) 1997 Strathclyde University .
; SCCS info: Module @(#)$Header: /home/adascvs/idl/adas4xx/adas412/plot412.pro,v 1.2 2013/03/14 13:25:15 mog Exp $ Date $Date: 2013/03/14 13:25:15 $.
;+
; PROJECT:
;       ADAS IBM MVS to DEC UNIX conversion
;
; NAME:
;	PLOT412
;
; PURPOSE:
;	Plot one graph for ADAS412.
;
; EXPLANATION:
;	This routine plots ADAS412 output for a single graph.
;	That includes the data for up to 7 ordinary levels for
;	a given metastable level.
;
; USE:
;	Use is specific to ADAS412.  See adas412_plot.pro for
;	example.
;
; INPUTS:
;	X	- Double array; list of x values.
;
;	Y	- 2D double array; y values, 2nd index ordinary level,
;		  indexed by gplot1 and gplot2.
;
;	GPLOT1- Integer; First G(T) function to plot from array.
;
;	GPLOT2- Integer; Last ordinary level to plot from array.
;
;	ISPEC	- String; Constructed title for plot including adf11
;		  year and element information.
;
;	HEADER- String; ADAS version number header to include in graph.
;
;	TITLE	- String; General title for program run.
;
;	GTIT1	- String; Title for graph.
;
;	DSNINC- String; Input COPASE data set name.
;
;	LSTRGA- String array; Level designations.
;
;	STRLEV- String array; Identifier of selected transition.
;
;	IL	- Integer; Number of energy levels.
;
;	GRPSCAL	- Integer; 1 if user specified axis limits to be used, 
;		  0 if default scaling to be used.
;
;	XMIN	- String; Lower limit for x-axis of graph, number as string.
;
;	XMAX	- String; Upper limit for x-axis of graph, number as string.
;
;	YMIN	- String; Lower limit for y-axis of graph, number as string.
;
;	YMAX	- String; Upper limit for y-axis of graph, number as string.
;
; OPTIONAL INPUTS:
;	None.
;
; OUTPUTS:
;	None.
;
; OPTIONAL OUTPUTS:
;	None.
;
; KEYWORD PARAMETERS:
;	FONT	- String; Name of font passed to graphical output
;		  widget.
;
; CALLS:
;	None.
;
; SIDE EFFECTS:
;	None.
;
; CATEGORY:
;	Adas system.	
;
; WRITTEN:
;       Richard Martin, University of Strathclyde, October 1997.
;	  Modified from plot205.pro.
;
; MODIFIED:
;     1.1 Richard Martin.
;          - First release.
;     1.2 Martin O'Mullane
;          - Correct y-axis label.
;
; VERSION:
;	1.1 24-10-1997
;	1.2 14-03-2013
;
;-
;----------------------------------------------------------------------------

PRO plot412, x, y, gplot1, gplot2, ispec, header, title, gtit1, $
		dsninc, lstrga, strlev, il, grpscal, xmin, xmax, ymin, ymax

  COMMON Global_lw_data, left, right, top, bottom, grtop, grright
   
		;**************************************************** 
		;**** Suitable character size for current device ****
		;**** Aim for 60 characters in y direction.      ****
		;**************************************************** 

  charsize = (!d.y_vsize/!d.y_ch_size)/60.0
  small_check = GETENV('VERY_SMALL')
  if small_check eq 'YES' then charsize=charsize*0.8


		;**** Initialise titles ****
		;**** '!E' shifts for exponent, !N returns to normal ****
  xtitle = 'log(ELECTRON TEMPERATURE(K))'
  ytitle = 'G(T) (cm!E3!N s!E-1!N)'
  strg1 = '---- Level Details -----'
  strg2 = 'INDEX       DESIGNATION       '

		;**** Construct graph title ****
		;**** '!C' is the new line control. ****
  gtitle =' '+    ispec+'!C!C'
  if small_check eq 'YES' then begin
    gtitle = gtitle+header+'!C!C'
    gtitle = gtitle+' GRAPH TITLE:'+gtit1+'!C!C'
    gtitle = gtitle+' INPUT FILE :'+dsninc+'!C!C'
  endif else begin
    gtitle = gtitle+header+'!C'
    gtitle = gtitle+' GRAPH TITLE:'+gtit1+'!C'
    gtitle = gtitle+' INPUT FILE :'+dsninc+'!C'
  endelse

		;**** Construct run title and complete level list ****
		
  list = '!C!C'+title+'!C!C'
  if small_check eq 'YES' then begin
    list = list+strg1+'!C!C'+strg2+'!C!C'
  endif else begin
    list = list+strg1+'!C'+strg2+'!C'
  endelse
  for ilev = 1, il do begin
    strg3 = string(lstrga(ilev-1))
    if small_check eq 'YES' then list=list+'!C'
    if (ilev le 22) then list = list+'!C'+strg3
  end
  list=list+'!C!C!C'
  strg4 = '---- Transition Details ----'
  strg5 = 'PLOT       TRANSITION       ' 
  list = list+strg4+'!C'+strg5+'!C!C'
     
		;**** How many points to plot ****
  npts = size(x)
  npts = npts(1)

		;**** Find x and y ranges for auto scaling,	   ****
		;**** check x and y in range for explicit scaling. ****
  makeplot = 1
  yext = y(gplot1:gplot2,*)

  if grpscal eq 0 then begin
		;**** identify values in the valid range ****
		;**** plot routines only work within     ****
		;**** single precision limits.	     ****

    xvals = where(x gt 1.6d-99 and x lt 1.0d99)
    yvals = where(yext gt 1.6d-99 and yext lt 1.0d99)

    if xvals(0) gt -1 then begin
      xmax = max(x(xvals))
      xmin = min(x(xvals))
    end else begin
      makeplot = 0
    end

    if yvals(0) gt -1 then begin
      ymax = max(yext(yvals))
      ymin = min(yext(yvals))
    end else begin
      makeplot = 0
    end

    style = 0
  end else begin
    xvals = where(x ge xmin and x le xmax)
    yvals = where(yext ge ymin and yext le ymax)
    if xvals(0) eq -1 or yvals(0) eq -1 then begin
      makeplot = 0
    end
    style = 1
  end
		
  if makeplot eq 1 then begin

		;**** Set up log-log plotting axes ****

    plot_io,[xmin,xmax],[ymin,ymax],/nodata,ticklen=1.0, $
		position=[left,bottom,grright,grtop], $
		xtitle=xtitle, ytitle=ytitle, xstyle=style, ystyle=style, $
		charsize=charsize

		;*********************************
		;**** Make and annotate plots ****
		;*********************************

    for i = gplot1, gplot2 do begin
		;**** Make plot ****
      oplot,x,y(i,*)

	list =list + strlev(i) +'!C'
	
		;**** Annotate plots with level numbers ****
	ymx=max(y(i,*),yp)

      xyouts,x(yp),y(i,yp),'('+strtrim(string(i+1),2)+')'

    end


		;**** Output title above graphs ****

    if small_check eq 'YES' then begin
        xyouts,left,top,gtitle,/normal,charsize=charsize*0.9
    endif else begin
        xyouts,left,top,gtitle,/normal,charsize=charsize
    endelse

		;**** Output level list at the side of the graph ****

    if small_check eq 'YES' then begin
        xyouts,grright+0.03,top-0.08,list,/normal,charsize=charsize*0.8
    endif else begin
        xyouts,grright+0.03,top-0.08,list,/normal,charsize=charsize
    endelse


  end else begin

		;**** Print a message to the screen ****
    erase
    format = "('Data for ordinary levels ',i2,' to ',i2,' out of range.')"
    mess = string(gplot1+1,gplot2+1,format=format)
    xyouts,left,grtop*0.7,ytitle+'   V   '+xtitle+'!C'+mess, $
		/normal,charsize=charsize

		;**** Output title above graphs ****

    xyouts,(left-0.18),top,gtitle,/normal,charsize=charsize*0.91

		;**** Output level list at the side of the graph ****

    if small_check eq 'YES' then begin
        xyouts,grright+0.03,top-0.02,list,/normal,charsize=charsize*0.8
    endif else begin
        xyouts,grright+0.03,top,list,/normal,charsize=charsize*0.95
    endelse


  end

END
