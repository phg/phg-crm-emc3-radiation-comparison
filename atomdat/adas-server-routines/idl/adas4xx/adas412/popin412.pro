; Copyright (c) 1997 Strathclyde University .
; SCCS info: Module @(#)$Header: /home/adascvs/idl/adas4xx/adas412/popin412.pro,v 1.1 2004/07/06 14:36:00 whitefor Exp $ Date $Date: 2004/07/06 14:36:00 $
;
;+
; PROJECT:
;       ADAS IBM MVS to DEC UNIX conversion
;
; NAME:
;       POPIN412()
;
; PURPOSE:
;       Produces a popup window with a message and a typein box and 
;	  toggle buttons.
;
; EXPLANATION:
;       This is simply a way of prompting the user to enter a numerical
;	value. A box pops up, with an optional message and title and
;	a typein text widget and a toggle switch. The user can choose to
;	enter either a density or pressure value. When the user enters a 
;	value it is checked to ensure that it is a good number and if it 
;	is the window is closed and the value passed back to the calling 
;	routine, otherwise the user must try again.
;
; INPUTS:
;       None
;
; OPTIONAL INPUTS:
;       VALUE   - a structure of the form
;			ps = {	DPVALUE:  	0.0,		$
;					TOGGLED:	0		}
;	
;			DPVALUE - Previous density/pressure value.
;			TOGGLED - 0 corresponds to density value
;				    1 corresponds to pressure value.       
;
; OUTPUTS:
;       The return value of this function is the string of the number
;	entered by the user.
;
; OPTIONAL OUTPUTS:
;	  VALUE -   As above.
;
; KEYWORD PARAMETERS:
;       MESSAGE - A string; A message which appears in the widget.
;                 Default 'Please enter a value: '
;
;       TITLE   - A string; The title of the popup window.  Default 'Warning!'
;
;       XOFFSET - Integer; X offset of pop-up window in pixels. Default 500.
;
;       YOFFSET - Integer; Y offset of pop-up window in pixels. Default 500.
;
;       FONT    - A font to use for all text in this widget.
;
; CALLS:
;       NUM_CHK - to check that the input is a valid number.
;	XMANAGER
;
; SIDE EFFECTS:
;       One other routine is included which ise used to manage the
;       widget;
;
;       POPIN412_EVENT
;
; CATEGORY:
;       Compound Widget
;
; WRITTEN:
;       Richard Martin, University Of Strathclyde, October 1997
;
; MODIFIED:
;       1.1	Richard Martin
;		First release.
;
; VERSION:
;       1.1	23-10-97
;
;-
;-----------------------------------------------------------------------------

PRO popin412_event, event  

    COMMON popin412_com, ps


                ;**** Base ID of compound widget ****

    parent=event.handler

                ;**********************************************
                ;**** Retrieve the user value state        ****
                ;**** Get id of first_child widget because ****
                ;**** user value "state" is stored there   ****
                ;**********************************************

    first_child = widget_info(parent, /child)
    widget_control, first_child, get_uvalue=state, /no_copy

                ;*********************************
                ;**** Clear previous messages ****
                ;*********************************

    widget_control, state.messid, set_value=' '

                ;************************
                ;**** Process events ****
                ;************************

    destroy = 0
    CASE event.id OF
    
      state.dselid: begin
      	widget_control, state.dselid, get_value=toggled
    		ps.toggled=toggled
      end
      
	state.userval: begin
	    widget_control, state.userval, get_value=userstring
	    if num_chk(userstring(0)) eq 0 then begin
		ps.dpvalue = double(userstring(0))
		widget_control, event.top, /destroy
		destroy = 1
	    endif else begin
		messvalue = 'Invalid entry, please try again'
		widget_control, state.messid, set_value=messvalue
		destroy = 0
	    endelse
	end

	ELSE:			;do nothing

    ENDCASE

                ;*********************************************
                ;*** make "state" available to other files ***
                ;*********************************************

    if destroy eq 0 then widget_control, first_child, set_uvalue=state, /no_copy

END

;-----------------------------------------------------------------------------

FUNCTION popin412, MESSAGE=message, TITLE=title, XOFFSET=xoffset, 		$
                YOFFSET=yoffset, FONT=font, VALUE = value

    COMMON popin412_com, ps

    ON_ERROR, 2

                ;**** Set defaults for keywords ****
		;**** Default value is set later ***
    IF NOT (KEYWORD_SET(message)) THEN message = 'Please enter a value: '
    IF NOT (KEYWORD_SET(title)) THEN title = 'Warning!'
    IF NOT (KEYWORD_SET(xoffset)) THEN xoffset = 500
    IF NOT (KEYWORD_SET(yoffset)) THEN yoffset = 500
    IF NOT (KEYWORD_SET(font)) THEN font = ''

                ;**** create titled base widget ****

    topparent = widget_base(title=title, xoffset=xoffset,		$
                            yoffset=yoffset, /column)

     IF NOT (KEYWORD_SET(value)) THEN begin
	ps = {	DPVALUE:  	0.0,		$
			TOGGLED:	0		}
     ENDIF ELSE BEGIN
      ps = {	DPVALUE:	value.dpvalue,	$
      		TOGGLED:	value.toggled 	}
     ENDELSE
 
                ;******************************************************
                ;**** Create a dummy widget just to hold value of *****
                ;**** "state" variable so as not to get confused  *****
                ;**** with any other values. Adopt IDL practice   *****
                ;**** of using first child widget                 *****
                ;******************************************************

    first_child = widget_base(topparent)

    parent = widget_base(first_child, /column)

                ;**** Message ****

    rc = widget_label(parent, value=message, font=font)
    rc = widget_label(parent, value=' ')

    dsbase = widget_base(parent,/row)				 
    rc = widget_label(dsbase,value='Default:',font=font) 
    selections=['Density (cm-3)','Pressure(Kcm-3)']
    toggled=ps.toggled
    dselid=cw_bgroup(dsbase,selections,set_value=toggled,font=font,$
    		/exclusive,/row)

		;**** Typein ****

    if ps.dpvalue eq 0.0 then begin
        userval = widget_text(parent, xsize=8, font=font, /editable)
    endif else begin
	value = ps.dpvalue
        userval = widget_text(parent, value=string(value,format='(e12.4)'), $
				 xsize=8, font=font, /editable)
    endelse

		;**** warning message ****

    messid = widget_label(parent, value=' ', font=font)

                ;*************************************************
                ;**** create a state structure for the pop-up ****
                ;****                window.                  ****
                ;*************************************************

    new_state = {userval	:	userval,			$
	         messid		:	messid,			$
	         TOGGLED		:    toggled,			$
	         DSELID:dselid				}

                 ;**** Save initial state structure ****

    widget_control, first_child, set_uvalue=new_state, /no_copy

                ;**** realize the widget ****

    widget_control, topparent, /realize
    widget_control, userval, /input_focus

                ;**** make widget modal ****

    xmanager, 'popin412', topparent, /modal, /just_reg

                ;**** Return the output value from common ****

    RETURN, ps

END
