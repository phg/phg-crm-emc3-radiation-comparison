; Copyright (c) 1997 Strathclyde University .
; SCCS info: Module @(#)$Header: /home/adascvs/idl/adas4xx/adas412/adas412_plot.pro,v 1.1 2004/07/06 10:48:57 whitefor Exp $ Date $Date: 2004/07/06 10:48:57 $
;+
; PROJECT:
;       ADAS IBM MVS to DEC UNIX conversion
;
; NAME:
;	ADAS412_PLOT
;
; PURPOSE:
;	Generates ADAS412 graphical output.
;
; EXPLANATION:
;	This routine creates a window for the display of graphical
;	output and displays output as the user directs by interaction
;	with the widget.  The widget allows the user to step backwards
;	and forwards through the graphical output at will.  To make
;	this possible a separate routine PLOT412 actually plots a
;	graph.  This routine compiles a table of all possible graphs
;	and calls PLOT412 as each new graph is requested via an event
;	from the graphical output widget.
;
; USE:
;	This routine is specific to ADAS412, see dcoutg.pro for
;	example use.
;
; INPUTS:
;	(Most of these inputs map exactly onto varaibles of the same
;	 name in the FORTRAN code.)
;
;	NTRANS  - Integer; Number of transitions.
;
;	NDTEM   - Integer; Maximum number of temperatures allowed.
;
;	TITLE   - String; General title for program run.
;
;	GTIT1   - String; Title for graph.
;
;	DSNINC  - String; Input COPASE data set name.
;
;	YEAR    - String; Year of adf11 data.
;
;	ELEM    - String; Selected element.
;
;	GRPSCAL - Integer; 1 indicates use user supplied axis limits
;		  	for scaling, 0 use defaults.
;
;	XMIN    - String; Lower limit for x-axis of graph, number as string.
;
;	XMAX    - String; Upper limit for x-axis of graph, number as string.
;
;	YMIN    - String; Lower limit for y-axis of graph, number as string.
;
;	YMAX    - String; Upper limit for y-axis of graph, number as string.
;
;	IL      - Integer; Number of energy levels = 'NMET' + 'NORD'.
;
;	LSTRGA  - String array; Level designations.
;
;	STLEV	  - String array; Selected transition identifiers.
;
;	TINE    - Float; Array of temperatures.
;
;	GOFT    - Float; Array of calculated G(T) functions for selected transitions.
;
;	HRDOUT  - Integer; 1 if hardcopy output activated, 0 if not.
;
;	HARDNAME- String; Filename for harcopy output.
;
;	DEVICE  - String; IDL name of hardcopy output device.
;
;	HEADER  - String; ADAS version number header to include in graph.
;
;     BITFILE - String; the path to the dirctory containing bitmaps
;                 for the 'escape to series menu' button.
;
; OPTIONAL INPUTS:
;	None.
;
; OUTPUTS:
;       GOMENU  - Int; flag - set to 1 if user has selected 'escape direct
;                 to series menu , otherwise 0
;
; OPTIONAL OUTPUTS:
;	None.
;
; KEYWORD PARAMETERS:
;	FONT	- String; The name of a font to use for text in the
;		  graphical output widget.
;
;
; CALLS:
;	CW_ADAS_GRAPH	Graphical output widget.
;	PLOT412		Make one plot to an output device for adas412.
;	XMANAGER
;
; SIDE EFFECTS:
;	This routine uses a common block to maintain its state PLOT412_BLK.
;
;	One other routine is included in this file;
;	ADAS412_PLOT_EV	Called via XMANAGER during widget management.
;
; CATEGORY:
;	Adas system.
;
; WRITTEN:
;       Richard Martin, University of Strathclyde, October 1997.
;	  Modified from adas208_plot.pro . 
;
; MODIFIED:
;	1.1	Richard Martin.
;		First Release.
;
; VERSION:
;     1.1	24-10-97.
;
;----------------------------------------------------------------------------

PRO adas412_plot_ev, event

  COMMON plot412_blk, data, action, nplot, iplot, win, plotdev, $
		      plotfile, fileopen, gomenu


  newplot = 0
  print = 0
  done = 0
		;****************************************
		;**** Set graph and device requested ****
		;****************************************
  CASE event.action OF

	'previous' : begin
			if iplot gt 0 then begin
			  newplot = 1
			  iplot = iplot - 1
			  first = iplot
			  last = iplot
			end
		end

	'next'	   : begin
			if iplot lt nplot-1 then begin
			  newplot = 1
			  iplot = iplot + 1
			  first = iplot
			  last = iplot
			end
		end

	'print'	   : begin
			newplot = 1
			print = 1
			first = iplot
			last = iplot
		end

	'printall' : begin
			newplot = 1
			print = 1
			first = 0
			last = nplot-1
		end

	'done'	   : begin
			if fileopen eq 1 then begin
			  set_plot,plotdev
			  device,/close_file
			end
			set_plot,'X'
			widget_control,event.top,/destroy
			done = 1
		end
        'bitbutton' : begin
            		if fileopen eq 1 then begin
                	  set_plot, plotdev
                	  device, /close_file
            		endif
            		set_plot,'X'
            		widget_control, event.top, /destroy
            		done = 1
            		gomenu = 1
        end
  END

		;*******************************
		;**** Make requested plot/s ****
		;*******************************

  if done eq 0 then begin
		;**** Set graphics device ****
    if print eq 1 then begin

      set_plot,plotdev
      if fileopen eq 0 then begin
        fileopen = 1
        device,filename=plotfile
	device,/landscape
      end

    end else begin

      set_plot,'X'
      wset,win
  
    end

		;**** Draw graphics ****
    if newplot eq 1 then begin
      for i = first, last do begin
        iord1 = data.iord1(i)
        iord2 = data.iord2(i)
        plot412,data.tine, data.y, iord1, iord2, $
		data.ispec, data.header, data.title, data.gtit1, data.dsninc, $
		data.lstrga, data.strlev, data.il, data.grpscal, $
		data.xmin, data.xmax, data.ymin, data.ymax

	if print eq 1 then begin
	  message = 'Plot '+strtrim(string(i+1),2)+' written to print file.'
	  grval = {WIN:0, MESSAGE:message}
	  widget_control,event.id,set_value=grval
	end

      end
    end

  end

END

;----------------------------------------------------------------------------

PRO adas412_plot, ntrans, ndtem,			$
	     title, gtit1,  dsninc,     		$
	     year, elem, grpscal,		      $
	     xmin, xmax, ymin, ymax, 			$
	     il ,lstrga, strlev, tine, goft,		$
	     hrdout, hardname, device, header,    $
	     bitfile, gomenu,	     			$
	     FONT=font

  ngpic = 7

  COMMON plot412_blk, data, action, nplot, iplot, win, plotdev, $
		      plotfile, fileopen, gomenucom

		;**** Copy input values to common ****
  plotdev = device
  plotfile = hardname
  fileopen = 0
  gomenucom = gomenu

		;************************************
		;**** Create general graph title ****
		;************************************

	ispec='G(T) FUNCTIONS:  ADF11 DATA YEAR: '+year+'  ELEMENT: '+elem
	
		;*********************************
		;**** Set up Y data for plots ****
		;*********************************

  y = dblarr(ntrans,ndtem)

  y = goft(*,*)

		;********************************************
		;**** Create table of all possible plots ****
		;********************************************
  nplot = 0

		;**** Create table entries for each plot ****
		;**** for this metastable level.         ****
		;**** Three arrays make up the table.  There is one   ****
		;**** entry in each array for each plot.	      ****
		;**** imtab(i)		metastable for plot i 		****
		;**** iord1tab(i)	first ordinary level for plot i ****
		;**** iord2tab(i)	last ordinary level for plot i  ****
		;**** note iord1 and iord2 are indices through the list ****
		;**** of ordinary levels which is not the same as the   ****
		;**** level numbers themselves, imet offsets them.      ****
		
  for iord1 = 0, ntrans-1, ngpic do begin
        iord2 = iord1 + ngpic-1
        iord2 = min([iord2,ntrans-1])

		;**** check x and y in range for explicit or auto scaling. ****
		;**** identify values in the valid range ****
		;**** plot routines only work within     ****
		;**** single precision limits.	     ****
	makeplot = 1
	yext = y(iord1:iord2,*)
	if grpscal eq 0 then begin
	  xvals = where(tine gt 1.0d-99 and tine lt 1.0d99)
	  yvals = where(yext gt 1.0d-99 and yext lt 1.0d99)
	end else begin
	  xvals = where(tine ge float(xmin) and tine le float(xmax))
	  yvals = where(yext ge float(ymin) and yext le float(ymax))
	end
	if xvals(0) eq -1 or yvals(0) eq -1 then begin
	  makeplot = 0
	end

	if makeplot eq 1 then begin
          if nplot eq 0 then begin
	    nplot = 1
            iord1tab = [iord1]
            iord2tab = [iord2]
          endif else begin
	    nplot = nplot + 1
            iord1tab = [iord1tab,iord1]
            iord2tab = [iord2tab,iord2]
          endelse
	endif
  end

  

		;**** Give an error if there's nothing to plot ****

  if nplot eq 0 then begin

    print,'********** ADAS412_PLOT MESSAGE **********'
    if grpscal eq 0 then begin
      print,'* No points in x or y ranges : 1.0e-99 to 1.0e+99 *'
    endif else begin
      print,'************ No points in plot range **************'
    endelse
    print,'***************** END OF MESSAGE ******************'

  endif else begin
     
		;*************************************
		;**** Create graph display widget ****
		;*************************************
  graphid = widget_base(TITLE='ADAS412 GRAPHICAL OUTPUT', $
					XOFFSET=1,YOFFSET=1)
  device, get_screen_size=scrsz
  xwidth = scrsz(0)*0.75
  yheight = scrsz(1)*0.75
  if nplot gt 1 then multiplot = 1 else multiplot = 0
  bitval = bitfile + '/menu.bmp'
  cwid = cw_adas_graph(graphid,print=hrdout,multiplot=multiplot,FONT=font,$
		bitbutton=bitval, xsize=xwidth, ysize=yheight)

                ;**** Realize the new widget ****
  dynlabel, graphid
  widget_control,graphid,/realize

		;**** Get the id of the graphics area ****
  widget_control,cwid,get_value=grval
  win = grval.win

		;*******************************************
		;**** Put the graphing data into common ****
		;*******************************************
		
  data = {	IORD1:iord1tab,	IORD2:iord2tab, 			$
		TINE:tine,		Y:y,		ISPEC:ispec,	$
		HEADER:header,	TITLE:title,	GTIT1:gtit1,$
		DSNINC:dsninc,	LSTRGA:lstrga,			$
		STRLEV:strlev,	GRPSCAL:grpscal, 			$
		XMIN:xmin,	XMAX:xmax,	YMIN:ymin,	YMAX:ymax, 	$
		IL:il}

		;**** Initialise to plot 0 ****
  iplot = 0
  wset,win
  
  iord1 = data.iord1(0)
  iord2 = data.iord2(0)
  plot412,	data.tine, data.y, iord1, iord2, $
		data.ispec, data.header, data.title, data.gtit1, data.dsninc, $
		data.lstrga, data.strlev, data.il,  data.grpscal, $
		data.xmin, data.xmax, data.ymin, data.ymax


		;***************************
                ;**** make widget modal ****
		;***************************
  xmanager,'adas412_plot',graphid,event_handler='adas412_plot_ev', $
                                        /modal,/just_reg

                ;**** Return the output value from common ****

  endelse

  gomenu = gomenucom

END
