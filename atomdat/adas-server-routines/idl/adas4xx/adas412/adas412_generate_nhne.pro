;+
; PROJECT:
;       ADAS
;
; NAME:
;       adas412_generate_nhne
;
; PURPOSE:
;       Generates arrays of temperature and nh/ne coefficients
;       for use in the generation of G(T) functions in adas412.
;
; EXPLANATION:
;       A support routine not called by adas412. Included as a
;       way to reference the source of the nh/ne ratio used. Run
;       this to modify the data in adas412_nhne.pro function.
;
;       The solar abundances are taken from K Philips 2008 and
;       the fractional abundance of the elements from central 
;       ADAS 85 (Arnaud and Rothenflug) data.
;
;
; MODIFIED:
;       1.1     Martin O'Mullane
;               First release.
;
; VERSION:
;       1.1     25-03-2013
;
;-
;-----------------------------------------------------------------------------


PRO adas412_generate_nhne

; Read in Philip's coronal abundance

file = '/home/adas/adas/arch601/abundance/abund.coronal_phillips08'

adas_readfile, file=file, all=all, nlines=nlines
i1 = where(strpos(strupcase(all), 'C-') EQ 0, c1)
if c1 EQ 0 then i1 = nlines-1 else i1 = i1[0]-1
res  = float(strmid(all[0:i1], 3))
i2   = where(res GT 0.0)

elem   = strlowcase(strmid(all[i2], 0, 2))
ab     = 10.0^float(strmid(all[i2], 3))
n_elem = n_elements(elem)
iz0    = xxeiz0(elem)

; Normalize abundance to hydrogen

ab = ab / 1e12


; Evaluate electron density with ADAS 85 data at 1.0e13/cm3 and 5000->1e9K

itval = 100
tek   = adas_vector(low=1e3, high=1e9, num=itval)
tev   = tek / 11605.0
dens  = fltarr(itval) + 1.0e13

res = fltarr(itval)

for j = 0, n_elem-1 do begin

   print, 'Element   : ' + elem[j]
   print, 'Abundance : ' + string(ab[j])

   run_adas405, uid='adas', year=96, defyear=85, elem=elem[j], te=tev, dens=dens, frac=frac

   zbar = (indgen(iz0[j] + 1)) ## frac.ion

   res = res + ab[j] * zbar

   print, 'Max <Z>   : ' + string(zbar[itval-1])

endfor

nhne = 1.0 / res


; Print nhne to arrays for use in adas412 interpolation code

print, 'te_s = [ $'
print, string(tek[0:itval-2], format='(6(e12.5, ",",:), " $")')
print, string(tek[itval-1], format='(e12.4)') + ']'

print, 'nhne_s = [ $'
print, string(nhne[0:itval-2], format='(6(e12.5, ",",:), " $")')
print, string(nhne[itval-1], format='(e12.4)') + ']'


END
