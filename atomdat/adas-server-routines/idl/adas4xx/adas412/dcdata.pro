; Copyright (c) 1997 Strathclyde University .
; SCCS info: Module @(#)$Header: /home/adascvs/idl/adas4xx/adas412/dcdata.pro,v 1.2 2004/07/06 13:28:47 whitefor Exp $ Date $Date: 2004/07/06 13:28:47 $
;
;+
; PROJECT:
;       ADAS IBM MVS to DEC UNIX conversion
;
; NAME:
;       DCDATA
;
; PURPOSE:
;       Initialises default arrays of temperature and nh/ne coefficients
;	  for the generation of G(T) functions in adas412, along with the
;	  2nd derivative of the interpolating function. 
;
; EXPLANATION:
;       This is simply a way of prompting the user to enter a numerical
;	value. A box pops up, with an optional message and title and
;	a typein text widget and a toggle switch. The user can choose to
;	enter either a density or pressure value. When the user enters a 
;	value it is checked to ensure that it is a good number and if it 
;	is the window is closed and the value passed back to the calling 
;	routine, otherwise the user must try again.
;
; INPUTS:
;       None     
;
; OUTPUTS:
;       TEMPDAT - Tabulated set of temperatures for model plasma.
;	  NHNEDAT - Tabulated set of nh/ne coefficients for model plasma.
;	  NHNE_DERIV - 2nd derivatives of the interpolating function obtained
;			 for corresponding values of tempdat.
;
; CALLS:
;	  Calls the IDL routine SPL_INIT to obtain the 2nd derivatives of
;	  the interpolating function at the default temperatures.
;
; WRITTEN:
;       Richard Martin, University Of Strathclyde, October 1997
;
; MODIFIED:
;       1.1	Richard Martin
;		First release.
;	  1.2 Richard Martin
;	   	Added if statement which uses appropriate spline routine name,
;		according to IDL version.
;
; VERSION:
;       1.1	23-10-97
;	  1.2 01-12-97
;
;-
;-----------------------------------------------------------------------------


pro dcdata, tempdat, nhnedat, nhne_deriv

tempdat=FLTARR(101)
nhnedat=FLTARR(101)
nhne_deriv=FLTARR(101)
dummy=0.0
count=0

for i=4.00,9.05,0.05 do begin
	tempdat(count)=float(i)
	count=count+1
end

nhnedat=[1.61,1.43,1.33,1.24,1.17,1.10,1.06,1.03,0.995,0.957,0.935,0.920,0.913, $
	0.910,0.909,0.905,0.901,0.885,0.865,0.855,0.849,0.842,0.835,0.835,0.834,  $
	0.834,0.833,0.833,0.833,0.833,0.832,0.832,0.832,0.832,0.831,0.831,0.831,  $
	0.831,0.830,0.830,0.830,0.830,0.830,0.830,0.830,0.830,0.830,0.829,0.829,  $
	0.829,0.829,0.829,0.829,0.829,0.829,0.829,0.829,0.829,0.829,0.829,0.828 ]

nhnedat=[nhnedat,	0.828,0.828,0.828,0.828,0.828,0.828,0.828,0.828,0.828,0.828,0.828,0.828,  $
	0.828,0.828,0.828,0.827,0.827,0.827,0.827,0.827,0.827,0.827,0.827,0.827,  $					
	0.827,0.827,0.827,0.827,0.827,0.827,0.827,0.827,0.827,0.827,0.827,0.827,  $
	0.827,0.827,0.827,0.827]

  if (!version.release eq '3.6.1a') OR (!version.release eq '3.6.1c') then begin 
	nhne_deriv=NR_SPLINE(tempdat,nhnedat)
  endif else begin
  	nhne_deriv=SPL_INIT(tempdat,nhnedat)
  endelse	

end
