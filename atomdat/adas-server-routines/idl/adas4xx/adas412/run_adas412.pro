;----------------------------------------------------------------------
;+
; PROJECT    :  ADAS
;
; NAME       :  run_adas412
;
; PURPOSE    :  Runs ADAS412 GTN code as an IDL subroutine.
;
;               NAME      TYPE   DETAILS
; INPUTS     :  
;     Mandatory:
;               adf04     str    ADF04 input filename
;               adf20     str    ADF20 output filename
;
;     Optional:
;		year      int    Year of ADF11 input data
;               elem      str    Element symbol
;               uid       str    UID of ADF11 input data
;      
;     Require two of:          
;               te        real() Temperature (K)
;               dens      real() Density (cm-3)
;               pressure  real() Pressure (Kcm-3)
;      
;     Then either:
;               upper     int()  Index of upper states
;               lower     int()  Index of lower states
;               assigned  real() Spectroscopic wavelength
;     Or:
;               wmin      real   Minimum wavelength (A)
;               wmax      real   Maximum wavelength (A)
;               highest   int    Highest included level
;
; KEYWORDS   :  independent_ionbal      Set to produce a density
;                                       independent ionisation
;                                       balance
;
;		help      		Prints help and exits
;
; OUTPUTS    :  None, produces an ADF20 file.
;
; NOTES      :  run_adas412 uses the spawn command. Therefore IDL v5.3
;               cannot be used. Any other version should work.
;
; AUTHOR     :  Allan Whiteford
;
; DATE       :  03-02-2009
;
; MODIFIED
;       1.1     Allan Whiteford
;                 - First release
;       1.2     Martin O'Mullane
;                 - Send uid back to fortran
;                 - Allow an even number of upper/lower levels to be used.
;       1.3     Martin O'Mullane
;                 - Evaluate nh/ne ratio with the new adas412_nhne function.
;
; DATE
;       1.1     03-02-2009
;       1.2     14-03-2013
;       1.3     25-03-2013
;
;-
;----------------------------------------------------------------------

pro run_adas412, adf04              = adf04,              $
                 year               = year,               $
                 elem               = elem,               $
                 uid                = uid,                $
                 te                 = te,                 $
                 dens               = dens,               $
                 pressure           = pressure,           $
                 independent_ionbal = independent_ionbal, $
                 upper              = upper,              $
                 lower              = lower,              $
                 assigned           = assigned,           $
                 wmin               = wmin,               $
                 wmax               = wmax,               $
                 highest            = highest,            $
                 adf20              = adf20,              $
                 help               = help
	
if keyword_set(help) then begin
	doc_library,'run_adas412'
        return
endif

on_error, 2

if n_elements(adf04) eq 0 then message, 'An ADF04 file must be specified'
read_adf04,file=adf04,fulldata=all
if n_elements(elem) eq 0 then elem=xxesym(all.iz0,/lower)
if strlowcase(elem) ne xxesym(all.iz0,/lower) then message,'Element and ADF04 file inconsistent'

if n_elements(independent_ionbal) eq 0 then independent_ionbal=0
if n_elements(year) eq 0 then year=96

log_name  = getenv("LOGNAME")
adas_user = getenv("ADASUSER")
adas_cent = getenv("ADASCENT")

if n_elements(uid) eq 0 then begin

   root2 = adas_cent + '/adf11/'
   
endif else begin

   p1 = strmid(adas_user,0,strpos(adas_user,log_name))
   p2 = strcompress(uid, /remove_all)
   p3 = strmid(adas_user,strpos(adas_user,log_name)+strlen(log_name))
   
   root2 = p1 + p2 + p3 + '/adf11/'
   
endelse

if (n_elements(te) ne 0)+(n_elements(dens) ne 0)+(n_elements(pressure) ne 0) ne 2 then message,'Two of temperature, density and pressure must be specified'

isel=0

if n_elements(upper) gt 0 or n_elements(lower) gt 0 then begin
	if n_elements(upper) ne n_elements(lower) then message,'Upper and lower states must be of same length'
	if n_elements(assigned) eq 0 then assigned=fltarr(n_elements(upper))
	if n_elements(upper) ne n_elements(assigned) then message,'State specification and assigned wavelength vector must be of same length'
	ntrans=n_elements(upper)
	isel=isel+1
endif

if n_elements(wmin) or n_elements(wmax) then begin
	if n_elements(wmin) + n_elements(wmax) ne 2 then message,'Must supply both an upper and lower wavelength'
	if n_elements(highest) eq 0 then highest=0
	if wmin ge wmax then message,'Maximum wavelength must be higher than minimum'
        isel=isel+2
end

if isel eq 0 then message,'Must supply either upper and lower states or a wavelength range'
if isel eq 3 then message,'Cannot supply both upper and lower states along with a wavelength range'

junk=''

fortdir = getenv('ADASFORT')
spawn, fortdir+'/adas412.out', unit=pipe, /noshell, PID=pid
date = xxdate()
printf, pipe, date[0]

printf, pipe, 'NO'
printf, pipe, adf04
printf, pipe, strtrim(string(year),2)
printf, pipe, strupcase(elem)
printf, pipe, root2

readf,pipe,iz0
readf,pipe,iz
readf,pipe,icnte
readf,pipe,il
readf,pipe,lfpool
for i=0,lfpool-1 do readf,pipe,junk

nte=0
ntselmax=0

readf,pipe,nte
readf,pipe,ntselmax
printf,pipe,0
printf,pipe,0
        
printf,pipe,isel

if isel eq 1 then begin
	send_upper=intarr(ntselmax)
	send_lower=intarr(ntselmax)
	send_assigned=dblarr(ntselmax)

	len=(ntrans < ntselmax)-1
	printf,pipe,len+1

	send_upper[0:len]=upper[0:len]
	send_lower[0:len]=lower[0:len]
	send_assigned[0:len]=assigned[0:len]
	
        printf,pipe,send_lower
	printf,pipe,send_upper
	printf,pipe,send_assigned
endif else begin
	printf,pipe,1
	printf,pipe,highest
        printf,pipe,wmin
        printf,pipe,wmax
endelse

printf,pipe,independent_ionbal+1
printf,pipe,n_elements(te)

if n_elements(pressure) eq 0 then pressure=dens*te
if n_elements(dens) eq 0 then dens=pressure/te
if n_elements(te) eq 0 then te=pressure/dens

if n_elements(te) gt nte then begin
	te=te[0:nte-1]
	dens=dens[0:nte-1]
	pressure=pressure[0:nte-1]
        print,'RUN_ADAS412 Warning: Truncating Te/Ne/Pressure grid'
endif

nhne=adas412_nhne(te)
        
for i=0,n_elements(te)-1 do begin
	printf,pipe,alog10(te[i])
	printf,pipe,dens[i]
	printf,pipe,pressure[i]
	printf,pipe,nhne[i]
end

readf,pipe,one

graph=0

printf,pipe,0
printf,pipe,graph

if n_elements(adf20) then begin
	printf,pipe,1
        printf,pipe,adf20
endif else begin
	printf,pipe,0
endelse

printf,pipe,0

if graph eq 1 then begin

endif

printf,pipe,1
printf,pipe,1	

readf,pipe,one
free_lun,pipe
end
