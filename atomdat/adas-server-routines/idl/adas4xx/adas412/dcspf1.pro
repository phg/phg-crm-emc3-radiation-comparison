; Copyright (c) 1997 Strathclyde University .
; SCCS info: Module @(#)$Header: /home/adascvs/idl/adas4xx/adas412/dcspf1.pro,v 1.1 2004/07/06 13:29:21 whitefor Exp $ Date $Date: 2004/07/06 13:29:21 $
;
;+
; PROJECT:
;       ADAS IBM MVS to DEC UNIX conversion
;
; NAME:
;	DCSPF1
;
; PURPOSE:
;	IDL user interface and communications with ADAS412 FORTRAN
;	process via pipe.
;
; EXPLANATION:
;	First this routine invokes the 'Output Options' part of the
;	interface. The results of the user's interactions are written 
;	to the FORTRAN via the pipe. Communications are with the FORTRAN 
;	routine ADAS412.
;
; USE:
;	The user of this routine is specific to ADAS412, See adas412.pro.
;
; INPUTS:
;	PIPE	- The IDL unit number of the bi-directional pipe to the
;		  ADAS412 FORTRAN process.
;
;	VALUE	- A structure which determines the initial settings of
;		  the dataset selection widget.  The initial value is
;		  set in adas412.pro.  VALUE is passed un-modified
;		  through to cw_adas_in.pro, see that routine for a full
;		  description.
;
;	DSNINC	- String; The full system file name of the input COPASE
;		  dataset selected by the user for processing.
;
;       BITFILE - String; the path to the dirctory containing bitmaps
;                 for the 'escape to series menu' button.
;
; OPTIONAL INPUTS:
;	None.
;
; OUTPUTS:
;	LPEND	- Integer; indicates user action on leaving output
;		  selections.  lpend is set to 1 if the user exited
;		  with the 'Cancel' button and 0 if the user exited
;		  with 'Done'.
;
;	VALUE	- On output the structure records the final settings of
;		  the output selection widget if the user pressed the
;		  'Done' button otherwise it is not changed from input.
;
;       GOMENU  - Int; flag - set to 1 if user has selected 'escape direct
;                 to series menu , otherwise 0
;
; OPTIONAL OUTPUTS:
;	None.
;
; KEYWORD PARAMETERS:
;	DEVLIST	- A string array;  Each element of the array is the
;		  name of a hardcopy graphics output device.  This
;		  list must mirror the DEVCODE list.  DEVLIST is the
;		  list which the user will see.
;		  e.g ['Post-Script','HP-PCL','HP-GL']
;
;	FONT	- Supplies the font to be used for the interface widgets.
;
; CALLS:
;	ADAS412_OUT	User interface - output options.
;
; SIDE EFFECTS:
;	This routine communicates with the ADAS412 FORTRAN process
;	via a UNIX pipe.
;
; CATEGORY:
;	Adas system.
;
; WRITTEN:
;       Richard Martin, University of Strathclyde, October 1997
;	  Based on b5spf1.pro v1.11
;
; MODIFIED:
;       1.1     Richard Martin
;               First release.
;
; VERSION:
;       1.1     23-10-97
;
;-
;-----------------------------------------------------------------------------


PRO dcspf1, pipe, lpend, value, dsninc, bitfile, gomenu,	$
		DEVLIST=devlist, FONT=font


                ;**** Set defaults for keywords ****
  IF NOT (KEYWORD_SET(font)) THEN font = ''


		;**************************************
		;**** Pop-up output options widget ****
		;**************************************
 
  adas412_out, value, dsninc, action, bitfile, $
		 DEVLIST=devlist, FONT=font

		;***********************************************
		;**** Act on the output from the widget     ****
		;***********************************************
		;**** There are only three possible actions ****
		;**** 'Done', 'Cancel' and 'Menu'.          ****
		;***********************************************

  gomenu = 0
  if action eq 'Done' then begin
    lpend = 0
  endif else if action eq 'Menu' then begin
    lpend = 1
    gomenu = 1
  endif else begin
    lpend = 1
  end

		;*******************************
		;**** Write data to fortran ****
		;*******************************

  printf,pipe,lpend

  if lpend eq 0 then begin

    printf,pipe,value.grpout

    printf,pipe,value.texout
    if value.texout eq 1 then begin
	printf,pipe,value.texdsn
    end
  end

		;**************************************************
		;**** Set messages and settings ready for when ****
		;**** output window is re-displayed.           ****
		;**************************************************

  if lpend eq 0 then begin
    if value.texout eq 1 then begin
		;**** If text output is requested  update the    ****
		;**** default to the current text file.          ****
		
      value.texdef = value.texdsn
      if value.texrep ge 0 then value.texrep = 0
      value.texmes = 'Output written to file.'
    end
  end
  

END
