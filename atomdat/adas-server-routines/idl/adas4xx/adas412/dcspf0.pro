; Copyright (c) 1997 Strathclyde University .
; SCCS info: Module @(#)$Header: /home/adascvs/idl/adas4xx/adas412/dcspf0.pro,v 1.1 2004/07/06 13:29:19 whitefor Exp $ Date $Date: 2004/07/06 13:29:19 $
;+
; PROJECT:
;       ADAS IBM MVS to DEC UNIX conversion
;
; NAME:
;	DCSPF0
;
; PURPOSE:
;	IDL user interface and communications with ADAS412 FORTRAN
;	process via pipe.
;
; EXPLANATION:
;	Firstly this routine invokes the part of the user interface
;	used to select the input dataset for ADAS412.  When the user's
;	interactions are complete this routine communicates with the
;	ADAS412 FORTRAN application via a UNIX pipe.  Communications
;	are to the FORTRAN subroutine DCSPF0.
;
; USE:
;	The use of this routine is specific to ADAS412, see adas412.pro.
;
; INPUTS:
;	PIPE	- The IDL unit number of the bi-directional pipe to the
;		  ADAS412 FORTRAN process.
;
;	VALUE	- A structure which determines the initial settings of
;		  the dataset selection widget.  The initial value is
;		  set in adas412.pro.  VALUE is passed un-modified
;		  through to cw_adas_in.pro, see that routine for a full
;		  description.
;
; OPTIONAL INPUTS:
;	None
;
; OUTPUTS:
;	VALUE	- On output the structure records the final settings of
;		  the dataset selection widget if the user pressed the
;		  'Done' button otherwise it is not changed from input.
;
;	DSNINC	- String; The full system file name of the input COPASE
;		  dataset selected by the user for processing.
;
;	DSNINI	- String; The full system file name of the input adf11
;		  'acd' dataset for the chosen year and element eg
;			/packages/adas/adas/adf11/acd89/acd89_c.dat .
;
;	REP	- String; Indicates whether the user pressed the 'Done'
;		  or 'Cancel' button on the interface.  The action is
;		  converted to the strings 'NO' and 'YES' respectively
;		  to match up with the existing FORTRAN code.  In the
;		  original IBM ISPF interface REP was the reply to the
;		  question 'DO YOU WANT TO END PROGRAM EXECUTION'.
;
; OPTIONAL OUTPUTS:
;	None
;
; KEYWORD PARAMETERS:
;	FONT	- Supplies the font to be used for the interface widgets.
;
; CALLS:
;	ADAS412_IN		Pops-up the dataset selection widget.
;
; SIDE EFFECTS:
;	This routine communicates with the ADAS412 FORTRAN process
;	via a UNIX pipe.
;
; CATEGORY:
;	Adas system.
;
; WRITTEN:
;       Richard Martin, University of Strathclyde, October 1997.
;
; MODIFIED:
;       1.1       Richard Martin    
;
; VERSION:
;       1.1       23-10-97
;-
;-----------------------------------------------------------------------------


PRO dcspf0, pipe, value, dsninc, dsnini, rep, FONT=font


                ;**** Set defaults for keywords ****

  IF NOT (KEYWORD_SET(font)) THEN font = ''


		;**********************************
		;**** Pop-up input file widget ****
		;**********************************

  adas412_in, value, action, WINTITLE = 'ADAS 412 INPUT', $
	TITLE = 'Input Specific Ion File', FONT = font

		;******************************************
		;**** Act on the event from the widget ****
		;******************************************
		;**** There are only two possible events ****
		;**** 'Done' and 'Cancel'.               ****
		;********************************************

  if action eq 'Done' then begin
    rep = 'NO'
  end else begin
    rep = 'YES'
  end

		;**** Construct adf04 datatset name ****
  
  dsninc = value.rootpath1 + value.file1


		;**** Construct adf11 datatset name ****
  
  dsnini = value.rootpath2+'acd'+value.year+'/acd'+value.year+'_'+value.elem+'.dat'

		;*******************************
		;**** Write data to fortran ****
		;*******************************

  printf,pipe,rep
  printf,pipe,dsninc
  printf,pipe,value.year
  printf,pipe,strupcase(value.elem)
  printf,pipe,value.rootpath2
  

END
