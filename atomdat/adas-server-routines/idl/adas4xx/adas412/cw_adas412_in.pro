; Copyright (c) 1997, Strathclyde University
; SCCS info: Module @(#)$Header: /home/adascvs/idl/adas4xx/adas412/cw_adas412_in.pro,v 1.4 2010/11/30 08:39:35 mog Exp $   Date  $Date: 2010/11/30 08:39:35 $
;+
; PROJECT:
;       ADAS IBM MVS to UNIX conversion
;
; NAME:
;       CW_ADAS412_IN()
;
; PURPOSE:
;       Data file selection for a single adf04 input dataset, and entry
;       of year and element for required adf11 data sets.
;
; EXPLANATION:
;       This function creates a compound widget consisting of the compound
;       widget cw_adas_infile.pro, for selecting an adf04 file, a 'Cancel' 
;       button, a 'Done' button and a button to allow the browsing of the 
;       selected data file comments. The browsing and done buttons are 
;       automatically de-sensitised until a valid input dataset has been 
;       selected.
;
;       The widget also allows the entry of a year and element for required
;       adf11 data sets (eg, acd89_c.dat, scd89_c.dat, ccd89_c.dat). The
;       routine checks the existence of the acd89 dataset and checks that
;       the chose element corresponds to the chosen adf04 file.
;
;       The value of this widget is the settings structure of the
;       cw_adas_infile widget.  This widget only generates events
;       when either the 'Done' or 'Cancel' buttons are pressed.
;       The event structure returned is;
;       {ID:0L, TOP:0L, HANDLER:0L, ACTION:''}
;       ACTION has one of two values 'Done' or 'Cancel'.
;
; USE:
;       See routine adas_in.pro for an example.
;
; INPUTS:
;       PARENT  - Long integer; the ID of the parent widget.
;
; OPTIONAL INPUTS:
;       None
;
; OUTPUTS:
;       This function returns the ID of the compound widget created.
;
; OPTIONAL OUTPUTS:
;       None
;
; KEYWORD PARAMETERS:
;       VALUE   - A structure which determines the initial settings of
;                 the dataset selection widget cw_adas_infile.  The
;                 structure must be;
;                 {ROOTPATH1:'', FILE1:'', CENTROOT1:'', USERROOT1:'' ,
;                  ROOTPATH2:'', CENTROOT2:'', USERROOT2:'' ,YEAR:'',ELEM:''}
;                 The elements of the structure are as follows;
;
;                 ROOTPATH1 - Current adf04 directory e.g '/usr/fred/adas/adf04'
;                 FILE1     - Current data file in ROOTPATH e.g 'input.dat'
;                 CENTROOT1 - Default central data store e.g '/usr/adas/adf04'
;                 USERROOT1 - Default user data store e.g '/usr/fred/adas/adf04'
;                 ROOTPATH2 - Current adf11 directory e.g '/usr/fred/adas/adf11'
;                 CENTROOT2 - Default central data store e.g '/usr/adas/adf11'
;                 USERROOT2 - Default user data store e.g '/usr/fred/adas/adf11'
;                 YEAR  - Current year e.g. '89'
;                 ELEM  - Current element e.g. 'c'
;
;                 The adf04 data file selected by the user is obtained by
;                 appending ROOTPATH1 and FILE1.  In the above example
;                 the full name of the data file is;
;                 /usr/fred/adas/adf04/input.dat
;
;                 Path names may be supplied with or without the trailing
;                 '/'.  The underlying routines add this character where
;                 required so that USERROOT1 will always end in '/' on
;                 output.
;
;                 The default value is;
;                 {ROOTPATH1:'./', FILE1:'', CENTROOT1:'', USERROOT1:''
;                 {ROOTPATH2:'./', CENTROOT2:'', USERROOT2:', YEAR:'', ELEM:''}
;                 i.e ROOTPATH is set to the user's current directory.
;
;       TITLE   - The title to be included in the input file widget, used
;                 to indicate exactly what the required input dataset is,
;                 e.g 'Input COPASE Dataset'
;
;       UVALUE  - A user value for the widget.  Defaults to 0.
;
;       FONT    - Supplies the font to be used for the interface widgets.
;                 Defaults to the current system font.
;
; CALLS:
;       CW_LOADSTATE    Recover compound widget state.
;       CW_SAVESTATE    Save compound widget state.
;       XXTEXT          Pop-up window to browse dataset comments.
;       CW_ADAS_INFILE  Dataset selection widget.
;       FILE_ACC        Determine filetype and access permissions.
;
;       See side effects for other related widget management routines.
;
; SIDE EFFECTS:
;       A COMMON BLOCK: CW_IN_BLK is used which is private to
;       this widget.
;
;       IN412_GET_VAL() Widget management routine in this file.
;       IN412_EVENT()   Widget management routine in this file.
;
; CATEGORY:
;       Compound Widget
;
; WRITTEN:
;       Richard Martin, University of Strathclyde, October 1997.
;
; MODIFIED:
;       1.1     Richard Martin    
;               First release.
;       1.2     Richard Martin.
;               Changed inset.file1(0) to inset.file1 .
;       1.3     Richard Martin.
;               Removed obsolete cw_loadstate/savestate statements. 
;               IDL 5.5 updates.
;       1.4     Martin O'Mullane
;               Give the internal routines (event and get_val) unique
;               names (in412_get_val and in412_event) to avoid conflict
;               with other ADAS routines. 
;
; VERSION:
;       1.1     23-10-97
;       1.2     01-12-97
;       1.3     31-01-02
;       1.4     30-11-2010
;
;-
;-----------------------------------------------------------------------------

FUNCTION in412_get_val, id

                ;**** Return to caller on error ****
  ON_ERROR, 2

                ;**** Retrieve the state ****

  first_child = widget_info(id,/child)
  widget_control, first_child, get_uvalue=state, /no_copy
        
                ;**** Get settings ****
  widget_control,state.fileid,get_value=fileset1

                ;**** Get settings ****
  widget_control,state.rootid,get_value=fileset2
    
  widget_control, state.yr, get_value=dummy
  year=strtrim(dummy[0],2)
  
  widget_control, state.el, get_value=dummy
  elem=strlowcase(strtrim(dummy[0],2))    


  ps={ ROOTPATH1:fileset1.rootpath,     $
       FILE1:fileset1.file,             $
       CENTROOT1:fileset1.centroot, $
       USERROOT1:fileset1.userroot,     $
         ROOTPATH2:fileset2.rootpath,   $
       CENTROOT2:fileset2.centroot, $
       USERROOT2:fileset2.userroot, $
         YEAR:year,                             $
         ELEM:elem                      }

  widget_control, first_child, set_uvalue=state, /no_copy

  RETURN, ps

END

;-----------------------------------------------------------------------------

FUNCTION in412_event, event


                ;**** Base ID of compound widget ****
  parent=event.handler

                ;**** Retrieve the state ****

  first_child = widget_info(parent,/child)
  widget_control, first_child, get_uvalue=state

                ;*********************************
                ;**** Clear previous messages ****
                ;*********************************
  widget_control,state.messid,set_value=' '

                ;**** Default output no event ****
  new_event = 0L
                ;************************
                ;**** Process events ****
                ;************************
  CASE event.id OF

                ;***********************************
                ;**** Event from file selection ****
                ;***********************************
    state.fileid: begin
          if event.action eq 'newfile' then begin
            widget_control,state.doneid,/sensitive
            widget_control,state.browseid,/sensitive
          end else begin
            widget_control,state.doneid,sensitive=0
            widget_control,state.browseid,sensitive=0
          end
        end

                ;***********************
                ;**** Browse button ****
                ;***********************
    state.browseid: begin
                ;**** Get latest filename ****
          widget_control,state.fileid,get_value=inset
          filename = inset.rootpath+inset.file

                ;**** Invoke comments browsing ****
          xxtext, filename, font=state.font
    end

                ;***********************
                ;**** Cancel button ****
                ;***********************
    state.cancelid: new_event = {ID:parent, TOP:event.top, $
                                HANDLER:0L, ACTION:'Cancel'}

                ;*********************
                ;**** Done button ****
                ;*********************
    state.doneid: begin

          error=0

          widget_control, event.handler, get_value=inset          
          
          widget_control,state.yr,get_value=dummy
          year=strtrim(dummy[0],2)
          widget_control,state.el,get_value=dummy
          elem=strlowcase(strtrim(dummy[0],2))    
          adffile=inset.rootpath2[0]+'acd'+year+'/acd'+year+'_'+elem+'.dat'

          error=num_chk(inset.year[0])
          if error gt 0 then message='       **Error: Year should be a number.**'
          
          file_acc,adffile,exist,read,write,execute,filetype
          if exist eq 0 then begin
            error = 1
            message='       **Ionisation and recombination datasets do not exist.**'
            goto,err
          end               
        
          adffile=inset.rootpath1[0]+inset.file1[0]
          adfelem=''
          openr,unit,adffile,/GET_LUN
          readf,unit,format='(A2)',adfelem
          close,unit
          free_lun,unit
          
          adfelem=strlowcase(strtrim(adfelem,2))
          
          if adfelem ne elem then begin
                error = 1
                message = '        **Selected element does not match with adf04 file**'
                goto, err
          end

                ;**** return value or flag error ****

ERR:    if error eq 0 then begin
          new_event = {ID:parent, TOP:event.top, HANDLER:0L, ACTION:'Done'}
        end else begin
          widget_control,state.messid,set_value=message
          new_event = 0L
      end                 

     end

    ELSE:

  ENDCASE

  RETURN, new_event
END

;-----------------------------------------------------------------------------

FUNCTION cw_adas412_in, topparent, $
                VALUE=value, TITLE=title, UVALUE=uvalue, FONT=font


  IF (N_PARAMS() EQ 0) THEN MESSAGE, 'Must specify parent for cw_adas412_in'
  ON_ERROR, 2                                   ;return to caller

                ;**** Set defaults for keywords ****
  IF NOT (KEYWORD_SET(value)) THEN begin
    inset = {   ROOTPATH1:'./', FILE1:'',         $
                    CENTROOT1:'', USERROOT1:'',           $
                    ROOTPATH2:'./',       $
                    CENTROOT2:'', USERROOT2:'',           $
                    YEAR:'', ELEM:''   }
  END ELSE BEGIN
    inset = {   ROOTPATH1:value.rootpath1, FILE1:value.file1,           $
                    CENTROOT1:value.centroot1, USERROOT1:value.userroot1,   $
                    ROOTPATH2:value.rootpath2, $
                    CENTROOT2:value.centroot2, USERROOT2:value.userroot2,   $ 
                    YEAR:value.year, ELEM:value.elem  }
                    
    if strtrim(inset.rootpath1) eq '' then begin
      inset.rootpath1 = './'      
    end else if $
            strmid(inset.rootpath1,strlen(inset.rootpath1)-1,1) ne '/' then begin
      inset.rootpath1 = inset.rootpath1+'/'
    end
    if strmid(inset.file1,0,1) eq '/' then begin
      inset.file1 = strmid(inset.file1,1,strlen(inset.file1)-1)
    end

    if strtrim(inset.rootpath2) eq '' then begin
      inset.rootpath2 = './'      
    end else if $
            strmid(inset.rootpath2,strlen(inset.rootpath2)-1,1) ne '/' then begin
      inset.rootpath2 = inset.rootpath2+'/'
    end

  END
  
  IF NOT (KEYWORD_SET(title)) THEN title = ''
  IF NOT (KEYWORD_SET(uvalue)) THEN uvalue = 0
  IF NOT (KEYWORD_SET(font)) THEN font = ''

                ;*********************************
                ;**** Create the Input widget ****
                ;*********************************

                ;**** create base widget ****
  parent = widget_base( topparent, $
                        EVENT_FUNC = "in412_event", $
                        FUNC_GET_VALUE = "in412_get_val", $
                        /COLUMN)

                ;**** Create base to hold the value of state ****

  first_child = widget_base(parent)

  cwid = widget_base(first_child,/column)
  
                ;*******************************************
                ;**** Input adf04 file selection widget ****
                ;*******************************************

  fbase = widget_base(cwid,/frame)
  fileval = {   ROOTPATH:inset.rootpath1,       $
                        FILE:inset.file1,                       $
                        CENTROOT:inset.centroot1,       $
                        USERROOT:inset.userroot1                }
  fileid = cw_adas_infile(fbase,value=fileval,title=title,font=font)
  
                ;***********************************
                ;**** Input adf11 filename data ****
                ;***********************************
  
  cwid2=widget_base(cwid,/column,/frame)
  rc = widget_label(cwid2,      $
                value='             Select Ionisation and Recombination Datasets',font=font)
                                                
  rootval2= { ROOTPATH:inset.rootpath2, $
                  CENTROOT:inset.centroot2, $
                  USERROOT:inset.userroot2 }
  rootid = cw_adas_root(cwid2,value=rootval2,font=font)
  
  ibase = widget_base(cwid2,/row)
  rc = widget_label(ibase,value='Data Year.:',font=font)
  yr = widget_text(ibase,value=inset.year,/edit,xsize=6,font=font)
  rc = widget_label(ibase,value='Element:',font=font)
  el = widget_text(ibase,value=inset.elem,/edit,xsize=6,font=font)  
                
                
                ;**** Error message ****
  errbase=widget_base(cwid,/row,/frame)
  messid = widget_label(errbase,font=font, $
  value='          Enter input data and press Done to proceed')

                ;*****************
                ;**** Buttons ****
                ;*****************
  base = widget_base(cwid,/row)

                ;**** Browse Dataset button ****

  browseid = widget_button(base,value='Browse Comments',font=font)

                ;**** Cancel Button ****
  cancelid = widget_button(base,value='Cancel',font=font)

                ;**** Done Button ****
  doneid = widget_button(base,value='Done',font=font)


                ;**** Check filename and desnsitise buttons if it ****
                ;**** is a directory or it is a file without read ****
                ;**** access.                                     ****
  filename = inset.rootpath1+inset.file1
  file_acc,filename,fileexist,read,write,execute,filetype
  if filetype ne '-' then begin
    widget_control,browseid,sensitive=0
    widget_control,doneid,sensitive=0
  end else begin
    if read eq 0 then begin
      widget_control,browseid,sensitive=0
      widget_control,doneid,sensitive=0
    end
  end

                ;**** create a state structure for the widget ****
  new_state = { FILEID:fileid, BROWSEID:browseid,                $
                ROOTID:rootid,  YR:yr,  EL:el,           $
                CANCELID:cancelid, DONEID:doneid, MESSID:messid, $
                FONT:font }

                ;**** Save initial state structure ****

  widget_control, first_child, set_uvalue=new_state, /no_copy

  RETURN, parent

END

