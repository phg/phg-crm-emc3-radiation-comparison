; Copyright (c) 1997 Strathclyde University .
; SCCS info: Module @(#)$Header: /home/adascvs/idl/adas4xx/adas411/adas411_proc.pro,v 1.1 2004/07/06 10:48:26 whitefor Exp $    Date $Date: 2004/07/06 10:48:26 $
;+
; PROJECT:
;       ADAS IBM MVS to   UNIX conversion
;
; NAME:
;	ADAS411_PROC
;
; PURPOSE:
;	IDL ADAS user interface, processing options/input.
;
; EXPLANATION:
;	This routine creates and manages a pop-up window which allows
;	the user to select options and input data to control ADAS411
;	processing.
;
; USE:
;	This routine is ADAS411 specific, see daispf.pro for how it
;	is used.
;
; INPUTS:
;	PROCVAL	- A structure which determines the initial settings of
;		  the processing options widget.  The value is passed
;		  unmodified into cw_adas411_proc.pro.
;
;		  See cw_adas411_proc.pro for a full description of this
;		  structure.
;
;	DSFULL	- String; The full system file name of the input 
;		          dataset selected by the user for processing.
;
;	NTDIM	- Integer : Maximum number of temperatures allowed.
;
;	NDTIN	- Integer : Maximum number of temperature/density pairs
;		           allowed.
;  	ITA  	- Integer : INPUT FILE - NO. OF RECEIVER TEMPERATURES (NSTORE)
;
;	TVALS   - Double array : Receiver electron temperatures - 
;				 1st dimension - temperature index
;				 2nd dimension - units 1 - Kelvin
;						       2 - eV
;						       3 - Reduced
;       BITFILE - String; the path to the directory containing bitmaps
;                 for the 'escape to series menu' button.
;
; OPTIONAL INPUTS:
;	None
;
; OUTPUTS:
;	PROCVAL	- On output the structure records the final settings of
;		  the processing selections widget if the user pressed the
;		  'Done' button, otherwise it is not changed from input.
;
;	ACT	- String; Either 'Done' or 'Cancel' for the button the
;		  user pressed to terminate the processing options
;		  window.
;
; OPTIONAL OUTPUTS:
;	None
;
; KEYWORD PARAMETERS:
;	FONT_LARGE - The name of a larger font.
;
;	FONT_SMALL - The name of a smaller font.
;
;	EDIT_FONTS - A structure of two fonts used in the table editor
;		     adas_edtab.pro, {font_norm:'',font_input:''}.
;
; CALLS:
;	CW_ADAS411_PROC	Declares the processing options widget.
;	DYNLABEL	Recursively adds dynamic_resize keyword to label
;			widgets for version 4.0.1 and higher of IDL
;	ADAS411_PROC_EV Called indirectly during widget management,
;			routine included in this file.
;	XMANAGER
;
; SIDE EFFECTS:
;       This widget uses a common block PROC411_BLK, to pass the
;       variables VALUE and ACTION between the two routines. 
;
; CATEGORY:
;	Widgets
;
; WRITTEN:
;       H. P. Summers, 10-11-97
;
; MODIFIED:	1.1	Richard Martin
;			Put under SCCS control.
;
; VERSION:	1.1	11-03-98
;-
;-----------------------------------------------------------------------------

PRO adas411_proc_ev, event

    COMMON proc411_blk, action, value

    action = event.action

    CASE event.action OF

		;**** 'Done' button ****

	'Done'  : begin

		;**** Get the output widget value ****

	    widget_control, event.id, get_value=value 
	    widget_control, event.top, /destroy
	end

		;**** 'Cancel' button ****

	'Cancel': widget_control, event.top, /destroy

                ;**** 'Menu' button ****

        'Menu': widget_control, event.top, /destroy

	ELSE:			;**** Do nothing ****

    ENDCASE

END

;-----------------------------------------------------------------------------

PRO adas411_proc, procval, dsfull,  iz, iz0, iz1, 		  $
		  ntdim,  ndtin,				  $
		  ita, tvals, 					  $
		  ndprnt, ndlev, nprnt, il,  cprnt, clev, idielr, $
		  act, bitfile, 				  $
		  FONT_LARGE=font_large, 			  $
                  FONT_SMALL=font_small, EDIT_FONTS=edit_fonts


		;**** declare common variables ****

    COMMON proc411_blk, action, value

		;**** Copy "procval" to common ****

    value = procval

		;**** Set defaults for keywords ****

    IF NOT (KEYWORD_SET(font_large)) THEN font_large = ''
    IF NOT (KEYWORD_SET(font_small)) THEN font_small = ''
    IF NOT (KEYWORD_SET(edit_fonts)) THEN 				$
    edit_fonts = { font_norm:'', font_input:''}
		
                ;**** create base widget ****

    procid = widget_base(TITLE='ADAS411 PROCESSING OPTIONS', 		$
                         XOFFSET=50, YOFFSET=1)

		;**** Declare processing widget ****

    cwid = cw_adas411_proc(procid,   					$
			   dsfull,  iz, iz0, iz1, 			$
			   ntdim,  ndtin, 			        $
			   ita,  tvals, 				$
			   ndprnt, ndlev,nprnt, il, cprnt, clev, idielr,$
			   bitfile, 					$
			   PROCVAL=value, 				$
		 	   FONT_LARGE=font_large, FONT_SMALL=font_small,$
			   EDIT_FONTS=edit_fonts)

		;**** Realize the new widget ****

    dynlabel, procid
    widget_control, procid, /realize

		;**** make widget modal ****

    xmanager, 'adas411_proc', procid, event_handler='adas411_proc_ev', 	$
              /modal, /just_reg

		;*** copy value back to procval for return to dbispf ***

    act     = action
    procval = value
 
END

