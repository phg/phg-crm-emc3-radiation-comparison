; Copyright (c) 1997 Strathclyde University .
; SCCS info: Module @(#)$Header: /home/adascvs/idl/adas4xx/adas411/adas411_out.pro,v 1.1 2004/07/06 10:48:18 whitefor Exp $    Date $Date: 2004/07/06 10:48:18 $
;+
; PROJECT:
;       ADAS IBM MVS to   UNIX conversion
;
; NAME:
;	ADAS411_OUT
;
; PURPOSE:
;	IDL ADAS user interface, output options.
;
; EXPLANATION:
;	This routine creates and manages a pop-up window which allows
;	the user to select options and input data to control ADAS411
;	graphical and file output.
;
; USE:
;	This routine is ADAS411 specific, see dbspf1.pro for how it
;	is used.
;
; INPUTS:
;	VAL	- A structure which determines the initial settings of
;		  the output options widget.  The value is passed
;		  unmodified into cw_adas411_out.pro.
;
;		  See cw_adas411_out.pro for a full description of this
;		  structure.
;
;	DSFULL	- String; The full system file name of the input 
;		  dataset selected by the user for processing.
;
;       BITFILE - String; the path to the dirctory containing bitmaps
;                 for the 'escape to series menu' button.
;
; OPTIONAL INPUTS:
;	None.
;
; OUTPUTS:
;	ACT	- String; 'Done', 'Cancel' or 'Menu' for the button the
;		  user pressed to terminate the output options window.
;
; OPTIONAL OUTPUTS:
;	None.
;
; KEYWORD PARAMETERS:
;	DEVLIST	- A string array;  Each element of the array is the
;		  name of a hardcopy graphics output device.  This
;		  list must mirror the DEVCODE list.  DEVLIST is the
;		  list which the user will see.
;		  e.g ['Post-Script','HP-PCL','HP-GL']
;
;	FONT	- Supplies the font to be used for the interface widgets.
;
; CALLS:
;	CW_ADAS411_OUT	Creates the output options widget.
;	DYNLABEL	Recursively adds dynamic_resize keyword to label
;			widgets for version 4.0.1 and higher of IDL
;	XMANAGER
;	See side effects for widget management routine.
;
; SIDE EFFECTS:
;	This routine uses a common block OUT411_BLK to maintain its state.
;	ADAS411_OUT_EV	is included in this file and is called
;			indirectly from XMANAGER during widget
;			management.
;
; CATEGORY:
;	Widgets
;
; WRITTEN:
;       Martin O'Mullane, 23-9-97
;	
; MODIFIED: 1.1	Richard Martin
;			Put under SCCS control.
;
; VERSION: 1.1	11-03-98
;	
;-
;-----------------------------------------------------------------------------

PRO adas411_out_ev, event

    COMMON out411_blk,action,value


		;**** Find the event type and copy to common ****

    action = event.action

    case action of

		;**** 'Done' button ****

	'Done'  : begin

		;**** Get the output widget value ****

	    child = widget_info(event.id,/child)
	    widget_control,child,get_value=value 

		;*****************************************
		;**** Kill the widget to allow IDL to ****
		;**** continue and interface with     ****
		;**** FORTRAN only if there is work   ****
		;**** for the FORTRAN to do.          ****
		;*****************************************

	    if (value.grpout eq 1) or (value.texout eq 1) then begin
	        widget_control,event.top,/destroy
	    endif 
        end

		;**** 'Cancel' button ****

	'Cancel': widget_control, event.top, /destroy

                ;**** 'Menu' button ****

        'Menu': widget_control, event.top, /destroy
	
	ELSE:	;**** Do nothing ****

    ENDCASE

END

;-----------------------------------------------------------------------------

PRO adas411_out, val, dsfull, act, bitfile, devlist=devlist, font=font

    COMMON out411_blk,action,value

		;**** Copy value to common ****

    value = val

		;**** Set defaults for keywords ****

    if not (keyword_set(font)) then font = ''
    if not (keyword_set(devlist)) then devlist = ''

		;***************************************
		;**** Pop-up a new widget           ****
		;***************************************

                ;**** create base widget ****

    outid = widget_base(title='ADAS411 OUTPUT OPTIONS', xoffset=100,	$
                        yoffset=1)
;    widget_control, outid, /realize, update=0

		;**** Declare output options widget ****

    cwid = cw_adas411_out(outid, dsfull, bitfile, value=value, 		$
			  devlist=devlist, font=font )

		;**** Realize the new widget ****

    dynlabel, outid
    widget_control, outid, /realize
;    widget_control, outid, /update

		;**** make widget modal ****

    xmanager, 'adas411_out', outid, event_handler='adas411_out_ev',	$
              /modal, /just_reg
 
		;**** Return the output value from common ****

    act = action
    val = value

END

