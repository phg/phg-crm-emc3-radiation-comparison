; Copyright (c) 1997 Strathclyde University .
; SCCS info: Module @(#)$Header: /home/adascvs/idl/adas4xx/adas411/cw_adas411_proc.pro,v 1.3 2004/07/06 12:48:45 whitefor Exp $    Date $Date: 2004/07/06 12:48:45 $
;+
; PROJECT:
;       ADAS IBM MVS toi   UNIX conversion
;
; NAME:
;	CW_ADAS411_PROC()
;
; PURPOSE:
;	Produces a widget for ADAS411 processing options/input.
;
; EXPLANATION:
;	This function creates a compound widget consisting of :-
;	   a text widget holding an editable 'Run title', 
;	   the dataset name/browse widget cw_adas_dsbr,
;	   a widget to select the data block for analysis,
;	   a widget to request a mimax fit and enter a tolerance for it,
;	   a table widget for receiver/donor temperature data cw_adas_table,
;	   buttons to enter default values into the table, 
;	   a message widget, 
;	   an 'Escape to series menu', a 'Cancel' and a 'Done' button.
;
;	The compound widgets included in this widget are self managing.
;	This widget only manages events from the two 'Defaults' buttons,
;	the 'Cancel' button and the 'Done' button and the
;       'escape to series menu' button.
;
; USE:
;	This widget is specific to ADAS411, see adas411_proc.pro
;	for use.
;
; INPUTS:
;	TOPPARENT- Long integer, ID of parent widget.
;
;	DSFULL	- String; The full system file name of the input 
;		  dataset selected by the user for processing.
;
;
;	NTDIM	- Integer; maximum number of temperatures allowed.
;
;	NDTIN	- Integer; number of temperatures selected
;
;  	ITA  	- Integer : INPUT FILE - NO. OF TEMPERATURES (NSTORE)
;
;	TVALS   - Double array : Receiver electron temperatures - 
;				 1st dimension - temperature index
;				 2nd dimension - units 1 - Kelvin
;						       2 - eV
;
;	The inputs map exactly onto variables of the same
;	name in the ADAS411 FORTRAN program.
;
;       BITFILE - String; the path to the directory containing bitmaps
;                 for the 'escape to series menu' button.
;
; OPTIONAL INPUTS:
;	None
;
; OUTPUTS:
;	The return value of this function is the ID of the declared widget.
;
; OPTIONAL OUTPUTS:
;	None
;
; KEYWORD PARAMETERS:
;	PROCVAL	- A structure which determines the initial settings of
;		  the processing options widget.
;
;		  The PROCVAL structure is;
;       procval = {     nmet	: 	0,
;                       title 	: 	'',				$
;                	ifout 	: 	1, 				$
;			iopt 	: 	1,				$
;			maxt  	: 	0,              		$
;			tin   	: 	temp_arr,			$
;			ifsel 	: 	0,				$
;			tolval	: 	5                		}
;
;
;		NMET    Flag which defines whether or not default values
;			exist or not. (< 0 if no values exist)
; 		TITLE	Entered general title for program run
;		IFOUT   Index indicating which units are being used
;		IOPT    Flag for plot type. 1 = Auger rates, 2 = resolved
;			state selective RR, 3 = representative bundle-n DR. 
;			0 = receiver, 1 = donor
;		MAXT    Number of receiver temperature values selected
;		TIN     User supplied receiver temperature values for fit.
;		IFSEL   Flag as to whether polynomial fit is chosen
;		TOLVAL  Tolerance required for goodness of fit if
;			polynomial fit is selected.
;
;		All of these structure elements map onto variables of
;		the same name in the ADAS411 FORTRAN program.
;
;
;	UVALUE	   - A user value for the widget. Default 0.
;
;	FONT_LARGE - The name of a larger font.  Default current system
;		     font
;
;	FONT_SMALL - The name of a smaller font. Default current system
;		     font.
;
;	EDIT_FONTS - A structure of two fonts used in the table editor
;		     adas_edtab.pro, {font_norm:'',font_input:''}.  Defaults
;		     to current system font.
;
;	NUM_FORM   - String; Numeric format to use in tables.  Default
;			'(E10.3)'
;		     
;
; CALLS:
;	POPUP		Popup warning window with buttons.
;	CW_ADAS_DSBR	Dataset name and comments browsing button.
;	CW_ADAS_TABLE	Adas data table widget.
;	CW_ADAS_SEL	Adas multiple selection widget.
;	CW_SINGLE_SEL	Adas scrolling table and selection widget.
;	CW_OPT_VALUE    Adas option selection widget with required input 
;			value. 
;
;	See side effects for other related widget management routines.
;
; SIDE EFFECTS:
;
;	The following widget management routines are included in this file;
;	PROC411_GET_VAL()	Returns the current PROCVAL structure.
;	PROC411_EVENT()		Process and issue events.
;	
; CATEGORY:
;	Compound Widget
;
; WRITTEN:
;	H. P. Summers, 10-11-97
;
; MODIFIED:	1.1	Richard Martin
;			Put under SCCS control.
;		1.2	Richard Martin
;			Changed widget name switch to switchb for IDL 5.4 compatibility.
;		1.3	Richard Martin
;			IDL 5.5 updtes.
;			
;
; VERSION:	1.1	11-03-98
;		1.2	10-11-00
;		1.3	08-03-02
;-
;-----------------------------------------------------------------------------

FUNCTION proc411_get_val, id

                ;**** Return to caller on error ****

    ON_ERROR, 2

		 ;***************************************
                 ;****     Retrieve the state 	     ****
		 ;**** Get first_child widget id     ****
		 ;**** because state is stored there ****
		 ;***************************************

    first_child = widget_info(id, /child)
    widget_control, first_child, get_uvalue = state

		;***********************************
		;**** Get run title from widget ****
		;**** Then centre in in string  ****
		;**** of 40 characters 		****
		;***********************************

    widget_control, state.runid, get_value=title
    title(0)=strtrim(title(0))
    title_len = strlen(title(0))
    if (title_len gt 40 ) then begin 
	title(0) = strmid(title(0),0,38)
        widget_control, state.messid, set_value="Title too long - truncated "
        widget_control, state.runid, set_value=title
	wait ,1
    endif
    pad = (40 - title_len)/2 
    spaces = '                                         '
    title(0) = strmid(spaces,0,(pad-1)) + title(0) + strmid(spaces,0,(pad-1))

		;****************************************
		;**** Get new data from table widget ****
		;****************************************

    widget_control, state.tempid, get_value=tempval
    tabledata = tempval.value
    ifout = tempval.units + 1


		;***********************
		;**** Get plot type ****
		;***********************


    widget_control, state.iopt1id, get_uvalue=temp

     if (temp(0) eq 1 ) then iopt = 1 

		;**** Copy out temperature values ****

    tin = dblarr(state.ndtin)
    blanks = where(strtrim(tabledata(0,*),2) eq '')

		;***********************************************
		;**** next line assumes that all blanks are ****
		;**** at the end of the columns             ****
		;***********************************************

    if blanks(0) ge 0 then maxt=blanks(0) else maxt=state.ntdim

		;*************************************************
                ;**** Only perform following if there is 1 or ****
                ;**** more value present in the table         ****
		;*************************************************

    if (maxt ge 1) then begin
        tin(0:maxt-1) = double(tabledata(0,0:maxt-1))
    endif

		;**** Fill in the rest with zero ****

    if maxt lt state.ntdim then begin
        tin(maxt:state.ntdim-1) = double(0.0)
    endif

		;*************************************************
		;**** Get selection of polyfit from widget    ****
		;*************************************************

    widget_control, state.optid, get_uvalue=polyset, /no_copy
    ifsel = polyset.optionset.option[0]
    if (num_chk(polyset.optionset.value[0]) eq 0) then begin
       tolval = (polyset.optionset.value[0])
    endif else begin
       tolval = -999
    endelse
    widget_control, state.optid, set_uvalue=polyset, /no_copy
    

		;***********************************************
		;**** get new index for data block selected ****
		;***********************************************

    widget_control, state.o1aid, get_value=select_block
    ip_res = select_block[0]
    widget_control, state.o1bid, get_value=select_block
    il_res = fix(select_block[0])  
   
		;***********************************************
		;**** write selected values to PS structure ****
		;***********************************************
;		  The PROCVAL structure is;
       ps =	 {	nmet  	: 	0 ,             		$
			title 	: 	title,			$
			nprnt 	: 	state.nprnt,		$
			il    	: 	state.il,		      $
			ip_res	: 	ip_res,			$
			il_res	: 	il_res,			$
			uprnt 	: 	state.uprnt,		$
			ulev  	: 	state.ulev,		      $
                	ifout 	: 	ifout, 			$
			iopt 		: 	iopt,				$
			maxt  	: 	maxt,              	$
			tin   	: 	tin,				$
			ifsel 	: 	ifsel,			$
			tolval	: 	tolval                		}

    widget_control, first_child, set_uvalue=state, /no_copy

    RETURN, ps

END

;-----------------------------------------------------------------------------

FUNCTION proc411_event, event

                ;**** Base ID of compound widget ****

    parent=event.handler

		;**********************************************
                ;**** Retrieve the user value state        ****
		;**** Get id of first_child widget because ****
		;**** user value "state" is stored there   ****
		;**********************************************

    first_child = widget_info(parent, /child)
    widget_control, first_child, get_uvalue=state,/no_copy
  
	
		;*********************************
		;**** Clear previous messages ****
		;*********************************

    widget_control, state.messid, set_value=' '

                ;************************
                ;**** Process events ****
                ;************************

    CASE event.id OF

	state.iopt1id: begin

	   widget_control, state.res_id, map=1
	   widget_control, state.iopt1id, set_uvalue = 1
	   widget_control, state.tempid, sensitive=1
	   widget_control, state.deftid, sensitive=1
	   widget_control, state.optid,  sensitive=1

	end	
	  	
	  	
; resolved RR
	
	state.o1aid: begin

	   widget_control, first_child, set_uvalue=state, /no_copy
	   widget_control, event.handler, get_value=ps

		;*** reset state variable ***

	   widget_control, first_child, get_uvalue=state, /no_copy
	   
	   iradr = state.iradr
	   ipsel  = ps.ip_res 
	   ilsel  = ps.il_res 
	   	   
           valid_str  = ['   No  ','   Yes ']
           clev = state.ulev
           for j=0,state.il-1 do begin
	      str_tmp=clev(j)
              strput,str_tmp,valid_str(iradr(j,ipsel)),21
              case 1 of
                (j ge  0) and (j le   8) : str_ind = "  "+strtrim(string(j+1),1)
                (j ge  9) and (j le  98) : str_ind = " " +strtrim(string(j+1),1)
                (j ge 99) and (j le 998) : str_ind =      strtrim(string(j+1),1)
              else : print,'Too many levels',j
              endcase
              clev(j)=str_ind+"    "+str_tmp
           endfor
             	   
	   tempval=strarr(state.il+1)
	   tempval(0)=string(-1)
	   tempval(1:state.il)=clev
	   
           widget_control, state.o1bid, set_value=tempval
	   
	end
	
	state.o1bid: begin

	   widget_control, first_child, set_uvalue=state, /no_copy
	   widget_control, event.handler, get_value=ps

		;*** reset state variable ***

	   widget_control, first_child, get_uvalue=state, /no_copy
	   
	   iradr = state.iradr
	   ipsel  = ps.ip_res 
	   ilsel  = ps.il_res 

; inform user if incorrect choice --- only enforce change at Done
; wait until a choice for both parent and level are selected!
           
        if (ilsel ne -1 and ipsel ne -1) then begin 
	   if (iradr(ilsel,ipsel) eq 0 ) then begin
	     
	     mess_str = ['Invalid choice of level and parent', $
	                 'DR rates not available in dataset ', $
	                 ' ',					$
	                 'Choose another pair               ']
	     action = popup(message=mess_str,$
		          buttons=['Confirm'], font=state.font)
		          

	   endif    
	  endif	           
	   	   
	end


		;*************************************
		;**** Default temperature button ****
		;*************************************

    	state.deftid: begin

		;**** popup window to confirm overwriting current values ****

	    action= popup(message='Confirm Overwrite values with Defaults',$
		          buttons=['Confirm','Cancel'], font=state.font)

	    if action eq 'Confirm' then begin

		;**** Get current table widget value ****

 	       	widget_control, state.tempid, get_value=tempval

               	units = tempval.units+1
	    	tvalues = strarr(state.maxt)
	        for i = 0, state.maxt-1 do begin
                  tvalues(i) =  strcompress(string(tempval.value(units,i)))
                endfor

		                  
		;***********************************************
		;**** Copy defaults into value structure    ****
		;**** For default values use the existing   ****
		;**** table values and let user modify them ****
		;**** then set all donor values to same     ****
		;**** value  				    ****
		;***********************************************

                deft = tempval
	        maxt = state.maxt
 	        if maxt gt 0 then begin
   	            tempval.value(0,0:maxt-1) =                     $
		    strtrim(string(deft.value(units,0:maxt-1),      $
                    format=state.num_form),2)
 	        endif
 	        if maxt lt state.ndtin then begin				
 	        	tempval.value(*,maxt:state.ndtin-1)= ' '
		  endif
		  
		;***************************************
		;**** Copy new data to table widget ****
		;***************************************

 	       widget_control,state.tempid, set_value=tempval
	   endif
      end

		;***********************
		;**** Cancel button ****
		;***********************

    	state.cancelid: new_event = {ID:parent, TOP:event.top, 		$
				     HANDLER:0L, ACTION:'Cancel'}

		;*********************
		;**** Done button ****
		;*********************

    	state.doneid: begin

		;***************************************
		;**** Check all user input is legal ****
		;***************************************

	    error = 0

        	;********************************************
		;*** Have to restore "state" before calls ***
		;*** to proc411_get_val so it can be used ***
		;*** there.				  ***
		;********************************************
		
	    widget_control, first_child, set_uvalue=state, /no_copy
	    widget_control, event.handler, get_value=ps

		;*** reset state variable ***

	    widget_control, first_child, get_uvalue=state, /no_copy

   ;**** check temp/density values entered ****
	
	    
              if error eq 0 then begin
                  widget_control, state.tempid, get_value=tabval
                  tabvals =                                               $
                  where(strcompress(tabval.value(0,*), /remove_all) ne '')
                  if tabvals(0) eq -1 then begin
                      error = 1
                      message = '**** Error: No temperatures'+ $
                                ' entered ****'
                  endif
              endif


                  ;*** Check to see if sensible tolerance is selected.

              if (error eq 0 and ps.ifsel eq 1) then begin
                  if (float(ps.tolval) lt 0)  or                          $
                  (float(ps.tolval) gt 100) then begin
                      error = 1
                      message='Error: Tolerance for polyfit must be 0-100% '
                  endif
              endif
              

            if (error eq 0 and (ps.il_res eq -1 or ps.ip_res eq -1)) then begin
                 error = 1
                 message='Error: Choose a resolved level'
            endif

            if (error eq 0 ) then begin
            	if state.iradr(ps.il_res,ps.ip_res) eq 0 then begin
                 	  error = 1
                 	  message='Error: Choose a valid resolved level'
                 	endif
            endif

   ;**** return value or flag error ****

	    if error eq 0 then begin
	        new_event = {ID:parent, TOP:event.top, 			$
                             HANDLER:0L, ACTION:'Done'}
            endif else begin
	        widget_control, state.messid, set_value=message
	        new_event = 0L
            endelse
        end


                ;**** Menu button ****

        state.outid: begin
            new_event = {ID:parent, TOP:event.top, HANDLER:0L,          $
                         ACTION:'Menu'}
        end

    	ELSE: new_event = 0L

    ENDCASE

		;*********************************************
		;*** make "state" available to other files ***
		;*********************************************

    widget_control, first_child, set_uvalue=state,/no_copy

    RETURN, new_event

END

;-----------------------------------------------------------------------------

FUNCTION cw_adas411_proc, topparent, 					$
		dsfull, iz, iz0, iz1, 					$
		ntdim,  ndtin, 						$
		ita,  tvals, 						$
		ndprnt, ndlev, nprnt, il,   cprnt, clev, iradr,	$
		bitfile, 						$
		procval=procval, uvalue=uvalue, 			$
		font_large=font_large, font_small=font_small, 		$
		edit_fonts=edit_fonts, num_form=num_form


		;**** Set defaults for keywords ****

    if not (keyword_set(uvalue))     then uvalue=0
    if not (keyword_set(font_large)) then font_large='' 
    if not (keyword_set(font_small)) then font_small=''
    if not (keyword_set(edit_fonts)) then 	$
    edit_fonts = {font_norm:'', font_input:''}
    if not (keyword_set(num_form)) then num_form='(E10.3)'
    if not (keyword_set(procval)) then begin
        str1_arr  = strarr(ndprnt)
        str2_arr  = strarr(ndlev)
	flt_arr   = fltarr(ndprnt)
        temp_arr  = dblarr(ndtin)
        ps = {  nmet    : 0, 						$
		title   : '',             				$
		nprnt   : 0,						$
		il      : 0,						$
		ip_res  : 1,						$
		il_res  : -1,						$
		uprnt   : str1_arr,					$
		ulev    : str2_arr,                             $
		ifout   : 1,                                    $
;		iopt    : 0,                                    $
		iopt    : 1,                                    $
		maxt    : 0,                                    $
                tin     : temp_arr,                         $
                ifsel   : 0,                                $
                tolval  : '5'                                           }
    endif else begin
	ps = {  nmet    : procval.nmet,                       $
		title   : procval.title,                        $
		nprnt   : procval.nprnt,                        $
		il      : procval.il,                           $
		ip_res  : procval.ip_res,                       $
		il_res  : procval.il_res,                       $
		uprnt   : strarr(n_elements(cprnt)),            $
		ulev    : strarr(n_elements(clev)),             $
		ifout   : procval.ifout,                        $
		iopt    : procval.iopt,                         $
		maxt    : procval.maxt,                         $
                tin     : procval.tin,                      $
                ifsel   : procval.ifsel,                    $
                tolval  : procval.tolval     				}
    endelse
    
    ipres = procval.ip_res

    if ipres gt nprnt-1 then ipres = 0  ; this checks if the file has changed

      
    valid_str  = ['   No  ','   Yes ']

    ps.ulev  = clev  + valid_str(iradr(*,ipres )) 

    ps.uprnt = cprnt


    clev  = clev  + valid_str(iradr(*,procval.ip_res))


                ;*********************************************************
                ;**** Modify certain parameters and results depending ****
                ;**** on the machine being used                       ****
                ;*********************************************************

    machine = GETENV('TARGET_MACHINE')
    if machine eq 'HPUX' then begin
        y_size = 4
        large_font = font_small
    endif else begin
        y_size = 6
        large_font = font_large
    endelse

		;****************************************************
		;*** Assemble temperature data                   ***
		;****************************************************
		;**** The adas table widget requires data to be *****
		;**** input as strings, so all the numeric data *****
		;**** has to be written into a string array.    *****
		;**** Declare temp. table arrays	        *****
		;**** col 1 has user  values             	*****
		;**** col 2 has values from files,      	*****
		;**** which can have one of three possible units*****
		;****************************************************

    tabledata = strarr(4,ndtin)

		;***********************************************
		;**** Copy out  temperatures                ****
		;**** for this data block                   ****
		;***********************************************

    maxt = ita 
    if (maxt gt 0) then begin
    
; fill table with blanks if tin(*) = 0.0
; user entered values

        full   = where(ps.tin,count_f)
        blanks = where(ps.tin eq 0.0,count_b)
        if (count_f ne 0) then begin
           tabledata(0,full) = 						$
	   strtrim(string(ps.tin(full),format=num_form),2)
	endif
        if (count_b ne 0) then begin
           tabledata(0,blanks) = ' ' 
        endif

; input file temperatures
	
        tabledata(1,0:maxt-1) = 					$
        strtrim(string(tvals(0:maxt-1,0),format=num_form),2)
        tabledata(2,0:maxt-1) = 					$
	strtrim(string(tvals(0:maxt-1,1),format=num_form),2)
        tabledata(3,0:maxt-1) = 					$
        strtrim(string(tvals(0:maxt-1,2),format=num_form),2)
        
        if (maxt lt ntdim) then begin
           tabledata(1:3,maxt:ntdim-1) = ' '
        endif
        
    endif

		;********************************************************
		;**** Create the 411 Processing options/input window ****
		;********************************************************

		;**** create titled base widget ****

    parent = widget_base(topparent, UVALUE = uvalue, 			$
			 title = 'ADAS411 PROCESSING OPTIONS', 		$
			 EVENT_FUNC = "proc411_event", 			$
			 FUNC_GET_VALUE = "proc411_get_val", 		$
			 /COLUMN)

		;******************************************************
		;**** Create a dummy widget just to hold value of *****
		;**** "state" variable so as not to get confused  *****
		;**** with any other values. Adopt IDL practice   *****
		;**** of using first child widget                 *****
		;******************************************************

    first_child = widget_base(parent)
    topbase = widget_base(first_child,/column)

		;***********************
		;**** add run title ****
		;***********************

    base = widget_base(topbase,/row)
    rc = widget_label(base,value='Title for Run',font=large_font)
    runid = widget_text(base,value=ps.title,xsize=38,font=large_font,/edit)

		;**** add dataset name and browse button ****

    rc = cw_adas_dsbr(topbase,dsfull,font=large_font,/row)

                ;**** add charge information ****

    sz0 = string(iz0,format='(i2)')           
    sz1 = string(iz1,format='(i2)')           
    sz  = string(iz,format='(i2)')           
    
    rc = widget_label(topbase,font=font_large, 			$
                      value='Nuclear Charge :'+sz0+		$
                      '  Recombining Ion Charge :'+sz1+		$
                      '  Recombined Ion Charge :'+sz   )



		;************************************
		;**** Add the plot type selector ****
		;************************************

  wrow = widget_base(parent, /row, /frame)
  wcol = widget_base(wrow, /column)

  wr_but = widget_base(wcol, /row)
  wc_but = widget_base(wr_but, /column)
  wr_but = widget_base(wr_but, /row, /exclusive) 


  iopt1id = widget_button(wr_but, value='Resolved state selective RR', $
                          font=font_large, /no_release)


; **** base for iopt choice inputs - map/unmap these as appropriate and not the
;      individual widgets held within them
        
  switchb = widget_base(wcol,/frame)
  res_id = widget_base(switchb,/row)
  
  

; resolved RR selection of initial parent and level


    block_info = strarr(1,nprnt)
    block_info(0,*) = cprnt
    titles = [['Initial Parent'],['Configuration      Term']]
    select_data = text_table(block_info, colhead=titles)
    coltitles = select_data(0:1)
    select_data = select_data(2:nprnt+1)
    o1aid = cw_single_sel(res_id, select_data, value=ps.ip_res,      $
			  title='Select Initial Parent',             $
			  coltitles=coltitles,                       $
			  ysize=(y_size+2), font=font_small,         $
			  big_font=large_font)
	
			  
    block_info = strarr(1,il)
    block_info(0,*) = clev
    titles = [['LS resolved Level'], $
             ['Configuration      Term  Valid Data']]
    select_data = text_table(block_info, colhead=titles)
    coltitles = select_data(0:1)
    
    list         = select_data(2:il+1)
    list_1       = strarr(il+1)
    if (ps.il_res eq -1) then begin	
	    list_1(0)    = string(-1)
    endif else begin
	    list_1(0)    = string(ps.il_res+1)
    endelse
    list_1(1:il) = list
	
    o1bid = cw_single_list_sel(res_id, value=list_1,      		$
			       title='Select Level',                    $
			       coltitles=coltitles,                     $
			       ysize=(y_size+2), font=font_small,       $
			       big_font=large_font)



		;****************************************
		;**** Add a base for temp selections ****
		;****************************************

    tablebase = widget_base(parent,/row)

		;************************************************
		;**** base for the table and defaults button ****
		;************************************************

    base = widget_base(tablebase,/column,/frame)

		;********************************
		;**** temperature data table ****
		;********************************

    colhead = [['Output','Input','Output','Input'], ['','','','']]

		;********************************************
		;**** convert FORTRAN index to IDL index ****
		;********************************************

    units = ps.ifout-1 
    unitname = ['Kelvin','eV','Reduced']

		;***********************************************************
		;**** Four columns in the table and three sets of units.****
		;**** Column 1 has the same values for all three        ****
		;**** units but column 2 switches between sets 2,3 & 4  ****
		;**** in the input array tabledata as the units change. ****
		;***********************************************************

    unitind = [[0,0,0],[1,2,3]]
    table_title = [ ["Output Electron Temperatures"]]

		;****************************
		;**** table of data   *******
		;****************************

    tempid = cw_adas_table(base, tabledata, units, unitname, unitind, 	$
			   UNITSTITLE = 'Temperature Units', 		$
			   COLEDIT = [1,0], COLHEAD = colhead,   	$
			   TITLE = table_title, 			$
			   UNITS_PLUS = '',			 	$
			   ORDER = [1,0], 				$ 
			   LIMITS = [1,1], CELLSIZE = 12, 		$
			   /SCROLL, ytexsize=y_size, NUM_FORM=num_form, $
			   FONTS = edit_fonts, FONT_LARGE = large_font, $
			   FONT_SMALL = font_small)

		;*************************
		;**** Default buttons ****
		;*************************

    tbase = widget_base(base, /column)
    deftid = widget_button(tbase,value=' Default Temperature Values  ',	$
		           font=large_font)

		;**************************************
		;**** Add polynomial fit selection ****
		;**************************************

    polyset = { option:intarr(1), val:strarr(1)}  ; defined thus because 
						  ; cw_opt_value.pro
                                                  ; expects arrays
    polyset.option = [ps.ifsel]
    polyset.val = [ps.tolval]
    options = ['Fit Polynomial']
;    optbase = widget_base(topbase,/frame)
    optbase = widget_base(base)
    optid   = cw_opt_value(optbase, options, 				$
			       title='Polynomial Fitting',		$
			       limits = [0,100], 			$
			       value=polyset, font=large_font 		)

		;**** Error message ****

    messid = widget_label(parent,font=large_font, 			$
    value='Edit the processing options data and press Done to proceed')

		;**** add the exit buttons ****

    base = widget_base(parent,/row)
    menufile = bitfile + '/menu.bmp'
    read_X11_bitmap, menufile, bitmap1
    outid = widget_button(base, value=bitmap1)          ;menu button
    cancelid = widget_button(base, value='Cancel', font=large_font)
    doneid = widget_button(base, value='Done', font=large_font)




; set map/sensitive options depending on the type of plot 
 	 
	  	widget_control, res_id, map=1
	  	widget_control, iopt1id, set_uvalue = 1
	  	widget_control, iopt1id, set_button = 1
	  	widget_control, tempid, sensitive=1
	  	widget_control, deftid, sensitive=1
	  	widget_control, optid, sensitive=1
	




  
		;*************************************************
		;**** create a state structure for the pop-up ****
		;**** 		     window.                  ****
		;*************************************************

    new_state = { runid		:	runid,  			$
		  messid	:	messid, 			$
		  deftid	:	deftid,				$
		  tempid	:	tempid, 			$
		  optid		:	optid,				$
		  cancelid	:	cancelid,			$
		  doneid	:	doneid, 			$
		  outid		:	outid,				$
		  iopt1id	: 	iopt1id,			$
		  res_id	: 	res_id,	  			$
		  o1aid		:	o1aid,				$
		  o1bid		:	o1bid,				$
		  dsfull	:	dsfull,				$
		  iopt		:	ps.iopt,			$
		  ntdim	 	:	ntdim,				$
		  ndtin	 	:	ndtin,				$
		  maxt		:	maxt, 				$
		  ita		:	ita,				$
		  tvals		:	tvals,				$
		  ndprnt	:	ndprnt,				$
		  ndlev		:	ndlev,				$
		  nprnt		:	nprnt,				$
		  il		:	il,				$
		  uprnt		:	cprnt,				$
		  ulev		:	clev,				$
		  iradr 	:	iradr,				$
		  font		:	large_font,			$
		  num_form	:	num_form			}
  
                 ;**** Save initial state structure ****

    widget_control, first_child, set_uvalue=new_state,/no_copy

    RETURN, parent

END
