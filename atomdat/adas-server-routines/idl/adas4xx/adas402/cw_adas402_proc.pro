; Copyright (c) 1995 Strathclyde University 
; SCCS info: Module @(#)$Header: /home/adascvs/idl/adas4xx/adas402/cw_adas402_proc.pro,v 1.3 2004/07/06 12:44:28 whitefor Exp $ Date $Date: 2004/07/06 12:44:28 $
;+
; PROJECT:
;       ADAS IBM MVS to UNIX conversion
;
; NAME:
;	CW_ADAS402_PROC()
;
; PURPOSE:
;	Produces a widget for ADAS402 processing options/input.
;
; EXPLANATION:
;	This function creates a compound widget consisting of :-
;	   a text widget holding an editable 'Run title', 
;	   the dataset name/browse widget ,
;	   information widgets for the data file and iso-electronic sequence,
;	   typein widgets for the ion selection parameters,
;	   a widget to request a mimax fit and enter a tolerance for it,
;	   a table widget for temp/density data ,
;	   a button to enter default values into the table, 
;	   a message widget, and a 'Cancel' and a 'Done' button.
;
;	The compound widgets included in this widget are self managing.
;	This widget only manages events from the  'Default' button,
;	the 'Cancel' button and the 'Done' button and the ion parameter
;	typeins.
;
;	This widget only generates events for the 'Done' and 'Cancel'
;	buttons.
;
; USE:
;	This widget is specific to ADAS402, see adas402_proc.pro
;	for use.
;
; INPUTS:
;	TOPPARENT- Long integer, ID of parent widget.
;
;	DSFULL	- String; The full system file name of the input 
;		  dataset selected by the user for processing.
;
;       CLASSVAL- String; Includes the class as extracted from the name
;                 of the file the user has selected. Used as a label on
;                 the processing screen.
;
;       YEARVAL - String; Includes the year of the data file as extracted
;                 from the the name of the file the user has selected.
;                 Used as a label on the processing screen.
;
;       FILEVAL - String; Shows whether file is partial or standard.
;                 Used as a label on the processing screen.
;
;       CELEM   - String; The element symbol extracted from the file name.
;
;	NTDMAX  - Integer; max number of user entered temp/densities
;
;	TEDA	- Float array - the actual temps read from input file
;
;	NDTIN	- Integer - max possible number of input temperatures
;
;	NDDEN	- Integer - max possible number of input densities
;
;	ITE	- Integer - actual number of input temperatures
;
;	IDE	- Integer - actual number of input densities
;
;       Z0BUT   - Long - z0 button widget identifier
;
;       BITFILE - String; the path to the dirctory containing bitmaps
;                 for the 'escape to series menu' button.
;
;	Some inputs map exactly onto variables of the same
;	name in the ADAS402 FORTRAN program.
;
; OPTIONAL INPUTS:
;	None
;
; OUTPUTS:
;	The return value of this function is the ID of the declared widget.
;
;	ACT 	- String; result of this widget, 'done' or 'cancel'
;
; OPTIONAL OUTPUTS:
;	None
;
; KEYWORD PARAMETERS:
;	PROCVAL	- A structure which determines the initial settings of
;		  the processing options widget.
;
;		  The default PROCVAL is created thus;
;
;     		ps = {proc402_set, $
;			new   : 0,		$
;                	title : '',             $
;                	lfsel : 0,              $
;                	tolval: '5', 		$
;			ifout : 1,		$
;                       tin   : temp_arr,       $
;                       din   : dens_arr,       $
;                       maxt  : 0,              $
;                       maxd  : 0,              $
;			tvals : dblarr(3,ndtin),$
;			losel : 0 ,		$
;			z0val : '',		$
;			zval  : '',		$
;			z1val : '', 		$
;                       apden : 0.0             $
;             }
;
;
;		NEW     Flag which defines whether or not default values
;			exist or not. (< 0 if no values exist)
; 		TITLE	Entered general title for program run
;		LFSEL   Flag as to whether polynomial fit is chosen
;		TOLVAL  Tolerance required for goodness of fit if
;			polynomial fit is selected.
;		IFOUT	Flag for the temperature units in use:
;			1=Kelvin, 2=eV, 3=Reduced
;		TIN  	Input temperatures entered by user
;		DIN	Input densities entered by user
;		MAXT	Number of temperatures in table entered by user
;		MAXD	Number of densities in table (always=MAXT)
;		TVALS	The input temperatures from the file in the
;			different units (note that conversion factors
;			are altered by the values of the ion parameters
;			and so these values are constantly updated)
;		LOSEL   Flag whether or not interpolated values for spline
;			fit have been used.
;		Z0VAL	Element nuclear charge
;		ZVAL	Recombined ion charge
;		Z1VAL	Recombining ion charge
;               APDEN   Approximate density to be extracted
;;               IEVCUT  Cutoff energy in eV
;
;		Most of these structure elements map onto variables of
;		the same name in the ADAS402 FORTRAN program.
;
;
;	UVALUE	- A user value for the widget. Default 0.
;
;	FONT_LARGE - The name of a larger font.  Default current system
;		     font
;
;	FONT_SMALL - The name of a smaller font. Default current system
;		     font.
;
;	EDIT_FONTS - A structure of two fonts used in the table editor
;		     adas_edtab.pro, {font_norm:'',font_input:''}.  Defaults
;		     to current system font.
;
;	NUM_FORM   - String; Numeric format to use in tables.  Default
;			'(E10.3)'
;		     
;
; CALLS:
;	POPUP		Popup warning window with buttons.
;	CW_ADAS_DSBR	Dataset name and comments browsing button.
;	CW_ADAS_TABLE   Adas data table widget.
;	CW_OPT_VALUE    Adas option selection widget with required input 
;			value. 
;	I4EIZ0		Converts element symbol into atomic number
;
;	See side effects for other related widget management routines.
;
; SIDE EFFECTS:
;
;	The following widget management routines are included in this file;
;	PROC402_GET_VAL()	Returns the current PROCVAL structure.
;	PROC402_EVENT()		Process and issue events.
;	
; CATEGORY:
;	Compound Widget
;
; WRITTEN:
;       William Osborn, Tessella Support Services plc, 29-Oct-1996
;
; MODIFIED:
;	1.1	William Osborn 			
;		First version - written using cw_adas401_proc.pro
;	1.2	Martin O'Mullane 			
;		Lingering problem with IDL 5.5 arrays of size 1.
;	1.3	Martin O'Mullane
;		Force the user to choose output conditions and
;               remove the nearest density from all other ions.
;               Changes are prefaced by Apr04 simplification tag.
;
; VERSION:
;	1.1	29-10-96
;	1.2	19-11-2002
;	1.3	14-04-2004
;
;-
;-----------------------------------------------------------------------------

FUNCTION proc402_get_val, id

    COMMON cw_proc402_blk, z0min, z0max, zmin, zmax, z1min, z1max


                ;**** Return to caller on error ****

    ON_ERROR, 2

		 ;***************************************
                 ;****     Retrieve the state 	     ****
		 ;**** Get first_child widget id     ****
		 ;**** because state is stored there ****
		 ;***************************************

    first_child = widget_info(id, /child)
    widget_control, first_child, get_uvalue = state

		;***********************************
		;**** Get run title from widget ****
		;**** Then centre in in string  ****
		;**** of 40 characters 		****
		;***********************************

    widget_control, state.runid, get_value=title
    title = strcompress(title(0))
    title_len = strlen(title) 
    if (title_len gt 40 ) then begin 
	title = strmid(title,0,37)
        widget_control, state.messid, set_value="Title too long - truncated "
        widget_control, state.runid, set_value=title
	wait ,1
    endif
    pad = (40 - title_len)/2 
    spaces = '                                         '
    title(0) = strmid(spaces,0,(pad-1)) + title + strmid(spaces,0,(pad-1))
  

		;****************************************************
		;**** Get new temp/densitydata from table widget ****
		;**** whether or not losel selected              ****
		;****************************************************

     widget_control, state.tempid, get_value=tempval
     tabledata = tempval.value
     ifout = tempval.units+1

 	        ;**** Copy out temperature values ****

     tin = dblarr(state.ntdmax)
     din = dblarr(state.ntdmax)
     blanks = where(strtrim(tabledata(0,*),2) eq '')
     blanksd = where(strtrim(tabledata(4,*),2) eq '')

                ;***********************************************
		;**** next line assumes that all blanks are ****
		;**** at the end of the columns             ****
		;***********************************************

    if blanks(0) ge 0 then maxt=blanks(0) else maxt=state.ntdmax
    if blanksd(0) ge 0 then maxd=blanksd(0) else maxd=state.ntdmax

                ;*************************************************
                ;**** Only perform following if there is 1 or ****
                ;**** more value present in the table         ****
                ;*************************************************

    if (maxt ge 1) then begin
    	tin(0:maxt-1) = double(tabledata(0,0:maxt-1))
    endif
    if (maxd ge 1) then begin
    	din(0:maxd-1) = double(tabledata(4,0:maxd-1))
    endif

		;**** Fill in the rest with zeroes ****

    if maxt lt state.ntdmax then begin
        tin(maxt:state.ntdmax-1) = double(0.0)
	din(maxd:state.ntdmax-1) = double(0.0)
    endif

    state.maxt = maxt
    state.maxd = maxd
    tvals = tabledata(1:3,*)

		;*************************************************
		;**** Get selection of polyfit from widget    ****
		;*************************************************

    widget_control, state.optid, get_uvalue=polyset, /no_copy
    lfsel = polyset.optionset.option[0]
    if (num_chk(polyset.optionset.value[0]) eq 0) then begin
        tolval = (polyset.optionset.value)
    endif else begin
        tolval = -999
    endelse
    widget_control, state.optid, set_uvalue=polyset, /no_copy

                ;*************************************************
                ;**** Get ion selection parameters            ****
                ;*************************************************

    widget_control, state.z0but, get_value=get_z0val
    widget_control, state.zbut, get_value=get_zval
    widget_control, state.z1but, get_value=get_z1val

    widget_control, state.apdenid, get_value=apdenval
    if num_chk(apdenval(0)) eq 0 then apden = float(apdenval(0)) else $
      apden = din(0)

;    widget_control, state.ievcutid, get_value=ievcutval
;    if num_chk(ievcutval(0)) eq 0 then ievcut = float(ievcutval(0)) else $
;      ievcut = 0.0

		;***********************************************
		;**** write selected values to PS structure ****
		;***********************************************

     ps = { 			 		$
	        new   : 0, 			$
                title : title,          	$
                lfsel : lfsel,              	$
		ifout : ifout,			$
		maxt  : maxt,			$
		maxd  : maxd,			$
		tin   : tin,			$
		din   : din,			$
		tvals : tvals,			$
                tolval: tolval,			$
		losel : state.losel,		$
		z0val : get_z0val(0),		$
		zval  : get_zval(0),		$
		z1val : get_z1val(0),		$
                apden : apden                  $
;                ievcut: ievcut                  $
              }

   
    widget_control, first_child, set_uvalue=state, /no_copy

    RETURN, ps

END

;-----------------------------------------------------------------------------

FUNCTION proc402_event, event

    COMMON cw_proc402_blk, z0min, z0max, zmin, zmax, z1min, z1max

                ;**** Base ID of compound widget ****

    parent=event.handler

		;**********************************************
                ;**** Retrieve the user value state        ****
		;**** Get id of first_child widget because ****
		;**** user value "state" is stored there   ****
		;**********************************************

    first_child = widget_info(parent, /child)
    widget_control, first_child, get_uvalue=state, /no_copy

		;*********************************
		;**** Clear previous messages ****
		;**** Check for dummy event   ****
		;*********************************

    if event.id eq event.handler then begin
	event.id = state.z0but
    endif else begin
        widget_control, state.messid, set_value=' '
    endelse

                ;************************
                ;**** Process events ****
                ;************************

    CASE event.id OF


		;*********************************
		;*** ener. selected for output ***
		;*********************************

        state.lbut : begin
 	    if (state.losel eq 0) then state.losel = 1 else 		$
 	    state.losel = 0 
 	    widget_control, state.deftid, sensitive= state.losel
 	    widget_control, state.tempid, sensitive = state.losel
;	    widget_control, state.optbase, sensitive = state.losel
 	end

		;*************************************
		;**** Default Energies button     ****
		;*************************************

        state.deftid: begin

 		;**** popup window to confirm overwriting current values ****

            action= popup(message='Confirm Overwrite values with Defaults', $
 		          buttons=['Confirm','Cancel'], font=state.font,$
			  title='ADAS402 Warning:-')

	    if action eq 'Confirm' then begin

		;**** Get current table widget value ****

 	        widget_control, state.tempid, get_value=tempval

		;**** Force user to select default density value ****

		dvalues = strarr(state.ide)
		for i = 0, (state.ide - 1) do begin
		    dvalues(i) =  strcompress(string(tempval.value(5,i)))
		endfor
		action= popup(message='Select Default density value', 	$
		              buttons=dvalues, /column, font=state.font,$
			      title = ' ')
		defdensity = action

		;***********************************************
		;**** Copy defaults into value structure    ****
		;**** For default values use the existing   ****
		;**** table values and let user modify them ****
		;***********************************************

                units = tempval.units+1
   	        nv = state.ite 
 	        if nv gt 0 then begin
   	            tempval.value(0,0:nv-1) = 				$
 	            strtrim(string(tempval.value(units,0:nv-1), 	$
 	            format=state.num_form),2)
                    tempval.value(4,0:nv-1) =  				$
                    string(defdensity , format=state.num_form)
 		endif

		;**** Fill in the rest with blanks ****

 	        if nv lt state.ntdmax then begin
   	            tempval.value(0,nv:state.ntdmax-1) = ''
                    tempval.value(4,nv:state.ntdmax-1) = ''
 	        endif

		;**** Copy new data to table widget ****

 	        widget_control, state.tempid, set_value=tempval

 	    endif

     	end

		;***********************
		;**** Cancel button ****
		;***********************

        state.cancelid: new_event = {ID:parent, TOP:event.top, 		$
				     HANDLER:0L, ACTION:'Cancel'}

                ;***********************
                ;**** Z text widget ****
                ;***********************

        state.zbut: widget_control, state.z1but, /input_focus

                ;************************
                ;**** Z1 text widget ****
                ;************************

        state.z1but: begin
            widget_control, state.z1but, get_value=val
            if num_chk(val(0),sign=+1) ne 0 then z1=1.0 else z1 = float(val(0))
            ;**** Alter the temperature set for
            ;**** reduced temperature units according to z1

            widget_control, state.tempid, get_value=tabval
            tv = where(strtrim(tabval.value(1,*),2) ne '')
            tvc = where(strtrim(tabval.value(1,*),2) eq '')
            for i=0, n_elements(tv)-1 do begin
                if num_chk(tabval.value(1,tv(i))) eq 0 then begin
                    tabval.value(3,tv(i))=$
                      string(float(tabval.value(1,tv(i)))/z1,$
                             format=state.num_form)
                endif
            endfor
            tabval.value(3,tvc)=''
            widget_control, state.tempid, set_value=tabval
            if event.type eq 0 then if event.ch eq 10 then $
              widget_control, state.zbut, /input_focus
        end
		;*********************
		;**** Done button ****
		;*********************

        state.doneid: begin

		;***************************************
		;**** Check all user input is legal ****
		;***************************************

	    error = 0

        	;********************************************
		;*** Have to restore "state" before calls ***
		;*** to proc402_get_val so it can be used ***
		;*** there.				  ***
		;********************************************

	    widget_control, first_child, set_uvalue=state, /no_copy

	    widget_control, event.handler, get_value=ps

		;*** reset state variable ***

	    widget_control, first_child, get_uvalue=state, /no_copy

		;**** check temp values entered ****

            if (ps.losel eq 1) and error eq 0 then begin
                widget_control, state.tempid, get_value=tabval
                tabvals =                                               $
                where(strcompress(tabval.value(0,*), /remove_all) ne '')
                if tabvals(0) eq -1 then begin
                    error = 1
                    message='**** Error: No temperatures/densities'+	$
                            ' entered ****'
                endif
            endif

		;*** Check to see if sensible tolerance is selected.

            if (error eq 0 and ps.lfsel eq 1) then begin                $
                if (float(ps.tolval) lt 0)  or                          $
                (float(ps.tolval) gt 100) then begin
                   error = 1
                   message='**** Error: Tolerance for polyfit must '+	$
                           'be 0-100% ****'
                endif
            endif

                ;**** Check whether value of "z0" is reasonable ****

            if (error eq 0) then begin 
                z0byte = byte(ps.z0val)
                if (min(z0byte) lt 48) or (max(z0byte) gt 57) then begin
                    i4eiz0, string(ps.z0val), z0test
                endif else begin
                    z0test = fix(ps.z0val)
                endelse
                if (z0test gt z0max) or                                 $
                (z0test lt z0min) then begin
                    error = 1
                    message="**** Error: Element nuclear charge Z0 is"+ $
                            " out of range ****"
		    widget_control, state.z0but, /input_focus
                endif
            endif

                ;**** Check whether value of "z" is reasonable ****

            if (error eq 0) then begin
                ztest = fix(ps.zval)
                if (fix(ps.zval) gt zmax) or                          	$
                (fix(ps.zval) lt zmin) then begin
                    error = 1
                    message="**** Error: Recombined ion charge Z is"+ 	$
                            " out of range ****"
                    widget_control, state.zbut, /input_focus
                endif
            endif

                ;**** Check whether value of "z1" is reasonable ****

            if (error eq 0) then begin
                z1test = fix(ps.z1val)
                if (fix(ps.z1val) gt z1max) or                        	$
                (fix(ps.z1val) lt z1min) then begin
                    error = 1
                    message="**** Error: Recombining ion charge Z1 is"+	$
                            " out of range ****"
                    widget_control, state.z1but, /input_focus
                endif
            endif

                ;**** Check whether approximate density is OK ****
            if (error eq 0) then begin
                widget_control, state.apdenid, get_value=val
                ok=1
                if num_chk(val(0),sign=+1) eq 0 then begin
                    if float(val(0)) eq 0.0 then ok = 0
                endif else ok=0
                if ok eq 0 then begin
                    error=1
                    message="**** Error: inconsistent approximate density ****"
                    widget_control, state.apdenid, /input_focus
                endif
            endif

;                ;**** Check whether energy cutoff is OK ****
;            if (error eq 0) then begin
;                widget_control, state.ievcutid, get_value=val
;                if num_chk(val(0),sign=+1) ne 0 then begin
;                    error=1
;                    message="**** Error: inconsistent energy cutoff ****"
;                    widget_control, state.ievcutid, /input_focus
;                endif
;            endif

		;**** return value or flag error ****

	    if error eq 0 then begin
                new_event = {ID:parent, TOP:event.top,                  $
                             HANDLER:0L, ACTION:'Done'}
            endif else begin
	        widget_control,state.messid,set_value=message
	        new_event = 0L
            endelse

        end

		;*********************
                ;**** Menu button ****
		;*********************

        state.outid: begin
            new_event = {ID:parent, TOP:event.top, HANDLER:0L,          $
                         ACTION:'Menu'}
        end

        ELSE: new_event = 0L

    ENDCASE

		;*********************************************
		;*** make "state" available to other files ***
		;*********************************************

    widget_control, first_child, set_uvalue=state, /no_copy

    RETURN, new_event

END

;-----------------------------------------------------------------------------

function cw_adas402_proc, topparent, dsfull, classval, yearval, 	$
                          fileval, act,		                        $
		          celem, 			                $
		          ntdmax, teda, ndtin, ndden, 			$
		          ite, ide, z0but, bitfile,			$
		          procval=procval, uvalue=uvalue, 		$
		          font_large=font_large, font_small=font_small,	$
		          edit_fonts=edit_fonts, num_form=num_form

    COMMON cw_proc402_blk, z0mincom, z0maxcom, zmincom, zmaxcom,	$
			   z1mincom, z1maxcom

		;**** Set defaults for keywords ****

    if not (keyword_set(uvalue)) then uvalue = 0
    if not (keyword_set(font_large)) then font_large = ''
    if not (keyword_set(font_small)) then font_small = ''
    if not (keyword_set(edit_fonts)) then 				$
    edit_fonts = {font_norm:'',font_input:''}
    if not (keyword_set(num_form)) then num_form = '(E10.3)'
    if not (keyword_set(procval)) then begin
        temp_arr = fltarr(ndtin)
        dens_arr = fltarr(ndden)
        ps = { 				$ 
		new   : 0, 		$
		title : '',             $
                lfsel : 0,              $
                tolval: '5',		$
		ifout : 1,		$
                tin   : temp_arr,       $
                din   : dens_arr,       $
                maxt  : 0,              $
                maxd  : 0,              $
                tvals : dblarr(3,ndtin),$
 		losel : 0 ,		$
		z0val : '',		$
		zval  : '',		$
		z1val : '',		$
                apden : 1.0            $
;                ievcut: 0.0             $
              }
    endif else begin
	ps = { 				$ 
		new   : procval.new,    $
                title : procval.title,  $
                lfsel : procval.lfsel,  $
                tolval: procval.tolval, $
		ifout : procval.ifout,	$
                tin   : procval.tin,    $
                din   : procval.din,    $
                maxt  : procval.maxt,   $
                maxd  : procval.maxd,   $
                tvals : procval.tvals,  $
		losel : procval.losel,  $
		z0val : procval.z0val,	$
		zval  : procval.zval,	$
		z1val : procval.z1val,	$
                apden : procval.apden  $
;                ievcut: procval.ievcut  $
	     }
    endelse
    
; Apr04 Simplification - Force these to be true
    
    ps.losel = 1
    ps.apden = 1.0

    
    
    z0max = 50				;max. value of element nuclear chg.
    zsmax = 'Sn'			;corresponding element symbol
    z0min = 0
    zsmin = celem
    z1min = z0min + 1
    z1max = z0max + 1
    zmin  = z1min - 1
    zmax  = z1max - 1
    z0mincom = z0min
    z0maxcom = z0max
    z1mincom = z1min
    z1maxcom = z1max
    zmaxcom  = zmax
    zmincom  = zmin

                ;*********************************************************
                ;**** Modify certain parameters and results depending ****
                ;**** on the machine being used                       ****
                ;*********************************************************

    machine = GETENV('TARGET_MACHINE')
    if machine eq 'HPUX' then begin
        y_size = 3
        large_font = font_small
    endif else begin
        y_size = 6
        large_font = font_large
    endelse

		;****************************************************
		;**** Assemble temp and density  table data      ****
		;****************************************************
		;**** The adas table widget requires data to be *****
		;**** input as strings, so all the numeric data *****
		;**** has to be written into a string array.    *****
		;**** Declare energies  table array             *****
		;**** col 1 has user temp   values              *****
		;**** col 2-4 have temp   values from files,    *****
		;**** which can have one of three possible units*****
		;**** col5 has user density values		*****
		;**** col6 has density values from file         *****
		;****************************************************

    tabledata = strarr(6,ntdmax)

                ;*************************************
		;**** Copy out temp and density   ****
		;**** values                      ****
		;*************************************

    if (ps.maxt gt 0) then begin
        tabledata(0,0:ps.maxt-1) =					$
 	strtrim(string(ps.tin(0:ps.maxt-1),format=num_form),2)
    endif
    tabledata(1,0:ite-1) =	 					$
    strtrim(string(ps.tvals(0,0:ite-1),format=num_form),2)
    tabledata(2,0:ite-1) = 						$
    strtrim(string(ps.tvals(1,0:ite-1),format=num_form),2)
    tabledata(3,0:ite-1) = 						$
    strtrim(string(ps.tvals(2,0:ite-1),format=num_form),2)
    if (ps.maxd gt 0) then begin
        tabledata(4,0:ps.maxd-1) =					$
        strtrim(string(ps.din(0:ps.maxd-1),format=num_form),2)
    endif
    if ide gt 0 then begin
        tabledata(5,0:ide-1) =                                          $
          strtrim(string(teda(0:ide-1), format=num_form),2)
    endif

		;**** fill rest of table with blanks ****

    blanks = where(tabledata eq 0.0) 
    tabledata(blanks) = ' ' 

		;********************************************************
		;**** Create the 402 Processing options/input window ****
		;********************************************************

		;**** create titled base widget ****

    parent = widget_base(topparent, UVALUE = uvalue, 			$
			 title = 'ADAS402 PROCESSING OPTIONS', 		$
 			 EVENT_FUNC = "proc402_event", 			$
			 FUNC_GET_VALUE = "proc402_get_val", 		$
			 /COLUMN)

		;******************************************************
		;**** Create a dummy widget just to hold value of *****
		;**** "state" variable so as not to get confused  *****
		;**** with any other values. Adopt IDL practice   *****
		;**** of using first child widget                 *****
		;******************************************************

    first_child = widget_base(parent)

    topbase = widget_base(first_child,/column)

		;***********************
		;**** add run title ****
		;***********************

    base = widget_base(topbase,/row)
    rc = widget_label(base,value='Title for Run',font=large_font)
    runid = widget_text(base,value=ps.title,xsize=38,font=large_font,/edit)

		;**** add dataset name and browse button ****

    rc = cw_adas_dsbr(topbase, dsfull, font=large_font)

		;********************************
		;**** Base for information   ****
		;********************************

    infomain = widget_base(topbase, /row)
    infobase = widget_base(infomain, /column, /frame)
    infolab  = widget_label(infobase, value='Data file information:-',	$
                            font=large_font)
    classlab = widget_label(infobase, value=classval, font=font_small)
    yearlab  = widget_label(infobase, value=yearval,  font=font_small)
    filelab  = widget_label(infobase, value=fileval,  font=font_small)

                ;********************************
                ;**** Base for ion parameters ***
                ;********************************

    marapbase = widget_base(topbase, /row, /frame)
    parambase = widget_base(marapbase, /column, /frame)
    paramval  = 'Please input the following charge '+		$
                'information:-'
    paramlab  = widget_label(parambase, value=paramval, font=large_font)
    parambas2 = widget_base(parambase, /row)
    paramva2  = 'Element nuclear charge        Z0 : '
    paramlab2 = widget_label(parambas2, value=paramva2, font=font_small)
    if (procval.z0val ne '') then begin
        z0chk = byte(procval.z0val)
        if max(z0chk) lt 58 then begin
            z0_value = fix(procval.z0val)
        endif else begin
            i4eiz0, string(z0chk), z0_value
        endelse
        if (z0_value ge z0min) and (z0_value le z0max) then begin
            z0string = strtrim(string(z0_value),2)
        endif else begin
            z0string = ''
        endelse
    endif else begin
        z0string =''
    endelse
    z0but     = widget_text (parambas2, value=z0string, xsize=4)

    parambas4 = widget_base(parambase, /row)
    paramva4  = 'Recombining ion charge        Z1 : '
    paramlab4 = widget_label(parambas4, value=paramva4, font=font_small)
    if (procval.z1val ne '') and (fix(procval.z1val) ge z1min) and      $
    (fix(procval.z1val) le z1max) then begin
        z1string = procval.z1val
    endif else begin
        z1string = ''
    endelse
    z1but     = widget_text (parambas4, value=z1string, xsize=4,        $
                             /editable, /all_events)
    parambas3 = widget_base(parambase, /row)
    paramva3  = 'Recombined ion charge         Z  : '
    paramlab3 = widget_label(parambas3, value=paramva3, font=font_small)
    if (procval.zval ne '') and (fix(procval.zval) ge zmin) and      $
    (fix(procval.zval) le zmax) then begin
        zstring = procval.zval
    endif else begin
        zstring = ''
    endelse
    zbut      = widget_text (parambas3, value=zstring, xsize=4,        $
                             /editable)

                ;**************************************
                ;**** Add polynomial fit selection ****
                ;**************************************

    polyset = { option:intarr(1), val:strarr(1)}  ; defined thus because
                                                  ; cw_opt_value.pro
                                                  ; expects arrays
    polyset.option = [ps.lfsel]
    polyset.val = [ps.tolval]
    options = ['Fit Polynomial']
    optbase = widget_base(marapbase,/frame)
    optid   = cw_opt_value(optbase, options, 				$
                           title='Polynomial Fitting',      		$
                           limits = [0,100],                		$
                           value=polyset, font=large_font)
;    widget_control, optbase, sensitive=ps.losel

		;************************************************
		;**** base for the table and defaults button ****
		;**** and whether table used for      output ****
		;************************************************

    tablebase = widget_base(topbase, /row)
    base = widget_base(tablebase, /column, /frame)

		;***************************************
		;*** "LOSEL" parameter switch **********
		;***************************************

; Apr04 Simplification - insist that Te/ne are selected

;     lbase = widget_base(base, /row)
;     llabel = widget_label(lbase,                    			$
;              value="Select Output Temperature/Density pairs", font=large_font)
;     lbbase = widget_base(lbase, /row, /nonexclusive)
;     lbut = widget_button(lbbase, value=' ')
;     widget_control, lbut, set_button=ps.losel

		;********************************
		;**** Energy data table      ****
		;********************************

    colhead = [['Output','Input','Output','Input'], 			$
              ['','','','']]

		;********************************************
		;**** convert FORTRAN index to IDL index ****
		;********************************************

    units = ps.ifout-1 
    unitname = ['Kelvin','eV','Reduced']

		;************************************************************
		;**** Four columns in the table and three sets of units. ****
		;**** Column 1 has the same values for all three         ****
		;**** units but column 2 switches between sets 2,3 & 4   ****
		;**** in the input array tabledata as the units change.  ****
		;************************************************************

    unitind = [[0,0,0],[1,2,3],[4,4,4],[5,5,5]]
    table_title =  [ ["Temperature & Density Values"],			$
                   ["Temperature             Density"]] 

		;****************************
		;**** table of data   *******
		;****************************
 
    tempid = cw_adas_table(base, tabledata, units, unitname, unitind, 	$
 		           UNITSTITLE = 'Temperature Units', 		$
 		           COLEDIT = [1,0,1,0], COLHEAD = colhead, 	$
 		           TITLE = table_title, ORDER = [1,0,0,0],	$ 
			   UNITS_PLUS = '       Density Units : cm-3',	$
 		           LIMITS = [1,1,1,1], CELLSIZE = 12, 		$
 		           /SCROLL, ytexsize=y_size, NUM_FORM=num_form,	$
 		           FONTS = edit_fonts, FONT_LARGE = large_font,	$
 		           FONT_SMALL = font_small)

		;*************************
		;**** Default buttons ****
		;*************************

    tbase = widget_base(base, /column)
    deftid = widget_button(tbase, font=large_font,                 	$
                           value='Default Temperature/Density Values')

		;*** Make table non/sensitive as ps.losel ***

    widget_control, tempid, sensitive = ps.losel
    widget_control, deftid, sensitive = ps.losel

                ;******************************************************
                ;**** Approximate density and energy cutoff widget ****
                ;******************************************************

    adbase = widget_base(tablebase,/frame,/column)
;    lab = widget_label(adbase,value='Energy cutoff:',$
;                       font=large_font)
;    adbase1 = widget_base(adbase,/row)
;    apdenid = widget_text(adbase1,/editable,value=string(ps.ievcut),$
;                          font=large_font)
;    lab = widget_label(adbase1,value='eV',font=large_font)
;    lab = widget_label(adbase,value=' ', font=large_font)


; Apr04 Simplification - Do not ask for approximate density

;     lab = widget_label(adbase,value='Approximate density to be extracted:',$
;                        font=large_font)
;     adbase2 = widget_base(adbase,/row)
;     apdenid = widget_text(adbase2,/editable,value=string(ps.apden),$
;                           font=large_font)
;     lab = widget_label(adbase2,value='cm^-3',font=large_font)
;     lab = widget_label(adbase,value='(Dataset closest to the above value',$
;                        font=large_font)
;     lab = widget_label(adbase,value='        will be selected)',font=large_font)


; Apr04 Simplification - Set these to be unseen

 lbut    = widget_label(adbase, value='')
 apdenid = widget_text(adbase, value=string(ps.apden))
 widget_control, adbase, map=0

 
		;**** Error message ****

    messid = widget_label(parent,font=large_font, 			$
    value='Edit the processing options data and press Done to proceed')

		;**** add the exit buttons ****

    base = widget_base(parent, /row)
    menufile = bitfile + '/menu.bmp'
    read_X11_bitmap, menufile, bitmap1
    outid = widget_button(base, value=bitmap1)          ;menu button
    cancelid = widget_button(base, value='Cancel', font=large_font)
    doneid = widget_button(base, value='Done', font=large_font)

  
		;*************************************************
		;**** create a state structure for the pop-up ****
		;**** 		     window.                  ****
		;*************************************************

    new_state = { runid:runid,  		$
		  outid:outid,			$
		  messid:messid, 		$
		  deftid:deftid,		$
		  tempid:tempid, 		$
		  losel : ps.losel, 		$
                  lfsel : ps.lfsel,             $
                  z0but : z0but,          	$
		  z1but : z1but,		$
		  zbut  : zbut,			$
                  lbut  : lbut,                 $
 		  optid:optid,			$
		  cancelid:cancelid,		$
		  doneid:doneid, 		$
		  dsfull:dsfull,		$
		  font:font_large,		$
                  tvals:ps.tvals,               $
                  teda:teda,                    $
                  ide:ide,                      $
                  ite:ite,                      $
                  optbase:optbase,              $
                  maxt:ps.maxt,                 $
                  maxd:ps.maxd,                 $
                  ntdmax:ntdmax,                $
                  tablebase:tablebase,          $
		  num_form:num_form, 		$
                  apdenid:apdenid              $
;                  ievcutid:ievcutid             $
	      }

                 ;**** Save initial state structure ****

    widget_control, first_child, set_uvalue=new_state,/no_copy

    RETURN, parent

END

