; Copyright (c) 1996, Strathclyde University.
; SCCS info: Module @(#)$Header: /home/adascvs/idl/adas4xx/adas402/adas402_plot.pro,v 1.1 2004/07/06 10:39:07 whitefor Exp $ Date $Date: 2004/07/06 10:39:07 $
;+
; PROJECT:
;       ADAS IBM MVS to UNIX conversion
;
; NAME:
;	ADAS402_PLOT
;
; PURPOSE:
;	Generates ADAS402 graphical output.
;
; EXPLANATION:
;	This routine creates a window for the display of graphical
;	output. A separate routine PLOT402 actually plots the
;	graph.
;
; USE:
;	This routine is very similar to that used for ADAS201.
; 
; INPUTS:
;
;	DSFULL  - String; Name of data file 
;
;       GTIT    - String; Graph title entered on output screen.
;
;       APDEN   - Float; Approximate density entered on processing screen.
;
;       MFDATA  - Structure array {plot_struct}(ndplot); input dataset data:
;                 {plot_struct, NUMPOI:0 ; number of points
;                               X:fltarr ; x positions
;                               Y:fltarr ; y positions
;                               LABX:0.0 ; x position of label
;                               LABY:0.0 ; y position of label
;                               LAB:""   ; label string
;                 }
;       NDPLOT  - Int; number of structures in mfdata array - number
;                 of input dataset plots
;
;       MINMAX  - Structure {plot_struct}; minimax data to plot
;
;       SPLINE  - Structure {plot_struct}; spline data to plot
;
;	UTITLE  - String; Optional comment by user added to graph title.
;
;       TITLM   - String; minimax curve info
;
;	XTITLE  - String; Title for x-axis
;
;	YTITLE  - String; Title for y-axis
;
;       LDEF1   - Int; whether user-specified ranges are to be used.
;
;	XMIN    - String; Lower limit for x-axis of graph, number as string.
;
;	XMAX    - String; Upper limit for x-axis of graph, number as string.
;
;	YMIN    - String; Lower limit for y-axis of graph, number as string.
;
;	YMAX    - String; Upper limit for y-axis of graph, number as string.
;
;	LFSEL	- Integer; 0 - No minimax fitting was selected 
;			   1 - Mimimax fitting was selected
;
;	LOSEL   - Integer; 0 - No interpolated values for spline fit. 
;			   1 - Intepolated values for spline fit.
;
;	HRDOUT - Integer; 1 if hardcopy output activated, 0 if not.
;
;	HARDNAME- String; Filename for harcopy output.
;
;	DEVICE	- String; IDL name of hardcopy output device.
;
;	HEADER	- String; ADAS version number header to include in graph.
;
;       BITFILE - String; the path to the dirctory containing bitmaps
;                 for the 'escape to series menu' button.
;
;       RESTIT  - String; extra info string if resolved data is being used.
;
; OPTIONAL INPUTS:
;	None.
;
; OUTPUTS:
;       GOMENU  - Int; flag - set to 1 if user has selected 'escape direct
;                 to series menu , otherwise 0
;
; OPTIONAL OUTPUTS:
;	None.
;
; KEYWORD PARAMETERS:
;	FONT	- String; The name of a font to use for text in the
;		  graphical output widget.
;
; CALLS:
;	CW_ADAS_GRAPH	Graphical output widget.
;	PLOT402		Make one plot to an output device for 402.
;	XMANAGER
;
; SIDE EFFECTS:
;	This routine uses a common block to maintain its state PLOT402_BLK.
;
;	One other routine is included in this file;
;	ADAS402_PLOT_EV	Called via XMANAGER during widget management.
;
; CATEGORY:
;	Adas system.
;
; WRITTEN:
;       Tim Hammond, Tessella Support Services plc,   8-Sep-1995
;
; MODIFIED:
;	1.1	William Osborn
;		First release
;
; VERSION:
;	1.1	30-10-96
;
;-
;----------------------------------------------------------------------------

PRO adas402_plot_ev, event

    COMMON plot402_blk, data, action, win, plotdev, 			$
		        plotfile, fileopen, lfsel, 			$
                        losel, gomenu


    newplot = 0
    print = 0
    done = 0
    gomenu = 0
		;****************************************
		;**** Set graph and device requested ****
		;****************************************

    CASE event.action OF

	'print'	   : begin
	    newplot = 1
	    print = 1
	end

	'done'	   : begin
	    if fileopen eq 1 then begin
	  	set_plot, plotdev
	  	device, /close_file
	    end
	    set_plot,'X'
	    widget_control, event.top, /destroy
	    done = 1
        end

        'bitbutton' : begin
            if fileopen eq 1 then begin
                set_plot, plotdev
                device, /close_file
            endif
            set_plot,'X'
            widget_control, event.top, /destroy
            done = 1
            gomenu = 1
        end

    END

		;*******************************
		;**** Make requested plot/s ****
		;*******************************

    if done eq 0 then begin

		;**** Set graphics device ****

        if print eq 1 then begin
            set_plot, plotdev
            if fileopen eq 0 then begin
                fileopen = 1
                device, filename=plotfile
	        device, /landscape
            endif
        endif else begin
            set_plot,'X'
            wset, win
        endelse

		;**** Draw graphics ****

        plot402, data.mfdata, data.minmax, data.spline, data.title, $
          data.xtitle, data.ytitle, lfsel, losel , data.ldef1,$
          data.xmin, data.xmax, data.ymin, data.ymax, data.ndplot

        if print eq 1 then begin
	    message = 'Plot  written to print file.'
	    grval = {WIN:0, MESSAGE:message}
	    widget_control, event.id, set_value=grval
	endif

    endif

END

;----------------------------------------------------------------------------

PRO adas402_plot, dsfull, hrdout, hardname, device, header, gtit, apden, $
      mfdata, ndplot ,minmax, spline, utitle, titlm, xtitle, ytitle, $
      lfsel, losel, ldef1, xmin, xmax, ymin, ymax, gomenu, bitfile, restit, $
      FONT=font

    COMMON plot402_blk, data, action, win, plotdev, 			$
		        plotfile, fileopen, lfselcom, 			$
                        loselcom, gomenucom

		;**** Copy input values to common ****

    plotdev = device
    plotfile = hardname
    fileopen = 0
    lfselcom = lfsel
    loselcom = losel

		;************************************
		;**** Create general graph title ****
		;************************************

    title = strarr(6)

    title(0) = "COLLISIONAL-DIELECTRONIC COEFF. VS TEMPERATURE"
    if ( strtrim(strcompress(utitle),2)  ne ' ' ) then begin
        title(0) = title(0) + ': ' + strupcase(strtrim(utitle,2))
    endif
    title(1) =  'ADAS    :' + strupcase(header)
    title(2) =  'TITLE   : '+strcompress(strupcase(gtit))+'         DENSITY   : '+$
      string(apden,format='(1e9.2)')+restit
    title(3) =  'FILE     : ' + strcompress(dsfull) 
    if (lfsel eq 1) then begin
        title(4)  = "MINIMAX : " + strupcase(titlm)
    endif 
  
		;*************************************
		;**** Create graph display widget ****
		;*************************************

    graphid = widget_base(TITLE='ADAS402 GRAPHICAL OUTPUT', $
					XOFFSET=50,YOFFSET=0)
    bitval = bitfile + '/menu.bmp'
    cwid = cw_adas_graph(graphid,print=hrdout,FONT=font, bitbutton=bitval)

                ;**** Realize the new widget ****

    widget_control,graphid,/realize

		;**** Get the id of the graphics area ****

    widget_control,cwid,get_value=grval
    win = grval.win

		;*******************************************
		;**** Put the graphing data into common ****
		;*******************************************

    data = {title:title,  xtitle:xtitle, ytitle:ytitle, 	$
            mfdata:mfdata  ,  minmax:minmax    , spline:spline, $
            ldef1:ldef1, xmin:xmin, xmax:xmax, ymin:ymin, ymax:ymax,$
            ndplot:ndplot}
 
    wset, win

    plot402, mfdata, minmax, spline, title, xtitle, ytitle, 		$
      lfsel, losel, ldef1, xmin, xmax, ymin, ymax, ndplot

		;***************************
                ;**** make widget modal ****
		;***************************

    xmanager,'adas402_plot', graphid, event_handler='adas402_plot_ev', $
             /modal, /just_reg

    gomenu = gomenucom

END
