; Copyright (c) 1996 Strathclyde University.
; SCCS info: Module @(#)$Header: /home/adascvs/idl/adas4xx/adas402/cw_adas402_in.pro,v 1.2 2004/07/06 12:44:11 whitefor Exp $ Date $Date: 2004/07/06 12:44:11 $
;+
; PROJECT:
;       ADAS IBM MVS to UNIX conversion
;
; NAME:
;       CW_ADAS402_IN()
;
; PURPOSE:
;       Data file selection for adas 402.
;
; EXPLANATION:
;       This function creates a compound widget consisting of the compound
;       widget cw_adas4xx_infile.pro, the compound widget cw_402_classes,
;	a 'Cancel' button, a 'Done' button, a button to allow the browsing
;	of any comments in the selected file and a 'Search' button to
;	search for files with the given details. The browsing and 'Done'
;	buttons are automatically de-sensitised until appropriate times.
;
;	The value of this widget is contained in the VALUE structure.
;
; USE:
;       See routine adas402_in.pro for an example.
;
; INPUTS:
;       PARENT  - Long integer; the ID of the parent widget.
;
;	CLASSES - String array: the names of the isonuclear master classes
;
; OPTIONAL INPUTS:
;       None
;
; OUTPUTS:
;       This function returns the ID of the compound widget created.
;
; OPTIONAL OUTPUTS:
;       None
;
; KEYWORD PARAMETERS:
;       VALUE   - A structure which determines the initial settings of
;                 the entire compound widget. The structure must be:
;		 	{ ROOTPATH:'',					$
;			  CENTROOT:'',					$
;			  USERROOT:'',					$
;                         FILE:'',       $
;			  FILETYPE:0,					$
;			  PARTTYPE:0,					$
;			  YEAR:'',					$
;			  DYEAR:'',					$
;			  SYMBOL:'',					$
;			  CLASS:0,       $
;			  PARINDX:0,					$
;			  GRNDINDX:0}
;
;		  Where the elements of the structure are as follows:
;
;                 ROOTPATH   - Current data directory e.g '/usr/fred/adas/'
;
;                 CENTROOT   - Default central data store e.g '/usr/adas/'
;
;                 USERROOT   - Default user data store e.g '/usr/fred/adas/'
;
;                 FILE       - Current file name
;
;		  BRANCH     - Int: flag showing which directory branch
;				is selected. 0=user, 1=central
;
;		  FILETYPE   - Int: flag showing what filetype is required.
;				0=standard, 1=partial
;
;		  PARTTYPE   - Int: flag showing what sort of partial
;				filetype is required, 0=Resolved,
;				1=Unresolved.
;
;		  YEAR       - String: the year for the data to be taken from.
;
;		  DYEAR	     - String: a default year if the requested
;				year is not available for some classes.
;
;		  SYMBOL     - String: isonuclear element symbol.
;
;		  MEMBER     - String: the required member prefix for
;			       all files (case insensitive, 2 chars only).
;
;		  CLASS      - Int:index to the class chosen in CLASSES
;
;                 PARINDX    - Int: selected parent index from the
;                              file if partial resolved
;
;                 GRNDINDX   - Int: selected ground index from the
;                              file if partial resolved
;
;                 Path names may be supplied with or without the trailing
;                 '/'.  The underlying routines add this character where
;                 required so that USERROOT will always end in '/' on
;                 output.
;
;       FONT_LARGE  - Supplies the large font to be used for the
;                     interface widgets.
;
;       FONT_SMALL  - Supplies the small font to be used for the
;                     interface widgets.
;
;       RNUM  	    - Supplies the number (as a string) to be used in the
;		      information widget title
;
; CALLS:
;       CW_402_CLASSES  	Compound widget handling the datafile 
;				selection for adas 402
;	CW_ADAS4XX_INFILE	Dataset selection widget.
;	I4EIZ0			Converts element symbol into atomic number
;	POPUP			Pops up simple message window
;
; SIDE EFFECTS:
;       IN402_GET_VAL() Widget management routine in this file.
;       IN402_EVENT()   Widget management routine in this file.
;
; CATEGORY:
;       Compound Widget
;
; WRITTEN:
;       William Osborn, Tessella Support Services plc, 29th October 1996
;
; MODIFIED:
;       1.1     William Osborn
;               First written using cw_adas405_in.pro and cw_adas401_in.pro
;       1.2     Martin O'Mullane
;               Logic flaw when using search button - central year was
;               not searched if a default year was present.
;
; VERSION:
;       1.1     29-10-96
;       1.2     19-11-2002
;
;-----------------------------------------------------------------------------
;-

FUNCTION check_indices, file, iprt, igrnd
;**** Checks whether a dataset given by parent and ground indices
;**** iprt and igrnd exists in file. Returns 1 if so, 0 if not.


   if file EQ '' then return, 0
   
   on_ioerror, search_end
   openr,unit,file,/get_lun
   instr = ''
AGAIN:
   readf,unit,instr,format='(A80)'
   a = strpos(instr,'IPRT=')
   if a ne -1 then begin
       ip = fix(strmid(instr,a+5,4))
       ig = fix(strmid(instr,a+16,4))
       if ip eq iprt and ig eq igrnd then return, 1
   endif
   goto, AGAIN
SEARCH_END:
   free_lun, unit

   return, 0
END

FUNCTION check_partial, file
;**** Checks whether file is a partial unresolved file
;**** Returns 1 if so, 0 if not and -1 if error or unknown.

   if file EQ '' then return, 0

    openr, unit, file, /get_lun
    on_ioerror, endfil
    a=''
again:
    readf,unit,a,format='(a80)'
    if strpos(a,'Z1=') ne -1 then begin
        if strpos(a,'IPRT') ne -1 then begin
            return, 0
        endif else begin
            return, 1
        endelse
    endif
    goto, again
endfil:
    free_lun, unit
    return, -1
END
;-----------------------------------------------------------------------------

FUNCTION in402_get_val, id

                ;**** Return to caller on error ****

    ON_ERROR, 2

                 ;***************************************
                 ;****     Retrieve the state        ****
                 ;**** Get first_child widget id     ****
                 ;**** because state is stored there ****
                 ;***************************************

    first_child = widget_info(id, /child)
    widget_control, first_child, get_uvalue = state

		;**** Get the settings ****

    widget_control, state.dataid, get_value=classvalues
    widget_control, state.fileid, get_value=fileval


    ps = {	ROOTPATH	:	fileval.rootpath,		$
		CENTROOT	:	fileval.centroot,		$
		USERROOT	:	fileval.userroot,		$
		FILE		:	fileval.file,			$
		FILTR		:	classvalues.filtr,		$
;		BRANCH		:	classvalues.branch,		$
		FILETYPE	:	classvalues.filetype,		$
		PARTTYPE	:	classvalues.parttype,		$
		YEAR		:	classvalues.year,		$
		DYEAR		:	classvalues.dyear,		$
		SYMBOL		:	classvalues.symbol,		$
		MEMBER		:	classvalues.member,             $
                CLASS           :       classvalues.class,              $
                PARINDX         :       classvalues.parindx,            $
                GRNDINDX        :       classvalues.grndindx            $
          }

    widget_control, first_child, set_uvalue=state, /no_copy

    RETURN, ps

END

;-----------------------------------------------------------------------------

FUNCTION in402_event, event

                ;**** Base ID of compound widget ****

    parent = event.handler

                ;**** Default output no event ****

    new_event = 0L

                ;**********************************************
                ;**** Retrieve the user value state        ****
                ;**** Get id of first_child widget because ****
                ;**** user value "state" is stored there   ****
                ;**********************************************

    first_child = widget_info(parent, /child)
    widget_control, first_child, get_uvalue=state, /no_copy
    topparent = widget_info(parent, /parent)

                ;************************
                ;**** Process events ****
                ;************************

    CASE event.id OF

		;***********************************
		;**** Event from file selection ****
		;***********************************

        state.fileid: begin
	    if event.action eq 'newfile' then begin
	        widget_control, state.doneid, /sensitive
	        widget_control, state.browseid, /sensitive
	    endif else begin
	        widget_control, state.doneid, sensitive=0
	        widget_control, state.browseid, sensitive=0
	    endelse
	end

                ;***********************
                ;**** Browse button ****
                ;***********************

        state.browseid: begin

                ;**** Get latest filename ****

            widget_control, state.fileid, get_value=inset
            filename = inset.rootpath + inset.file

                ;**** Invoke comments browsing ****

            xxtext, filename, font=state.font_large
        end

                ;***********************
                ;**** Cancel button ****
                ;***********************

        state.cancelid: begin
            new_event = {ID:parent, TOP:event.top,          		$
                         HANDLER:0L, ACTION:'Cancel'}
        end

	state.searchid: begin

		;************************************************
		;**** Get the current settings of the master ****
		;**** classes widget.                        ****
		;************************************************

	    widget_control, state.dataid, get_value=data

		;**** Test the validity of the user's values ****

            errormess = ''

                ;**** Check the member prefix ****
                                                               
            member = strcompress(data.member[0], /remove_all)
            if (strlen(member) ne 2 and (member ne '')) then begin
                errormess =                                             $
                '**** You have entered an invalid member prefix ****'
            endif

                ;**** Check the year ****

            if (errormess eq '') then begin
                year = strcompress(data.year[0], /remove_all)
                if (year eq '') then begin
                    errormess = '**** You must enter a valid year '+    $
                    'for the data ****'
                endif else if (strlen(year) ne 2 ) then begin
                    errormess = '**** You have specified the year '+    $
                    'incorrectly ****'
                endif else begin
                    yearbyt = byte(year)
                    if ((min(yearbyt) lt 48) or                         $
                    (max(yearbyt) gt 57)) then begin
                        errormess =                                     $
                        '**** You have specified the year incorrectly ****'
                    endif
                endelse
            endif

                ;**** Check the default year ****

            if (errormess eq '') then begin
                dyear = strcompress(data.dyear[0], /remove_all)
                if (dyear ne '') then begin
                    if (strlen(dyear) ne 2) then begin
                        errormess = '**** You have specified the default'+$
                                    ' year incorrectly ****'
                    endif else begin
                        dyearbyt = byte(dyear)
                        if ((min(dyearbyt) lt 48) or                    $
                        (max(dyearbyt) gt 57)) then begin
                            errormess = '**** You have specified the '+ $
                            'default year incorrectly ****'
                        endif
                    endelse
                endif
            endif

                ;**** Check the symbol ****

            if (errormess eq '') then begin
                symb = data.symbol[0]
                symbol = strcompress(symb, /remove_all)
                if (symbol eq '') then begin
                    errormess = '**** You must enter a valid '+         $
                    'isonuclear element symbol ****'
                endif else begin
                    i4eiz0, symbol, atnumber
                    if (atnumber eq 0) then begin
                        errormess = '**** You have specified an '+      $
                        'invalid element ****'
                    endif
                endelse
            endif

                ;**** if error then give message ****

            if errormess ne '' then begin
                action = popup(message=errormess, buttons=[' OK '],     $
                               font=state.font_large)
		new_event = 0L
            endif else begin

		;**** No error so begin the search ****

	        fileflag = 1		;Set to 1 if there are files
					;that fit, otherwise 0

                ;**** Create an information widget ****

                widget_control, /hourglass
                widgetbase = widget_base(/column, xoffset=300, yoffset=200,$
                title = "ADAS"+state.rnum+": INFORMATION")
                lab0 = widget_label(widgetbase, value='')
                lab1 = widget_label(widgetbase, value='')
                lab2 = widget_label(widgetbase, font=state.font_large,  $
                value=" Searching for files - "+$
                "please wait      ")
                lab3 = widget_label(widgetbase, value='')
                lab4 = widget_label(widgetbase, value='')
                widget_control, widgetbase, /realize
		widget_control, topparent, sensitive=0

                ;**************************
		;**** Search for files ****
                ;**************************

                widget_control, state.fileid, get_value=inset

                ;**** Construct target directory name ****

                dirnames = ['acd','scd','ccd','prb','prc','qcd','xcd','plt']
                dirname = dirnames(data.class)
                defname = dirname
                dirname = dirname + year
                defname = defname + dyear
	    	flag = data.filetype
                if (flag eq 1) then begin
                    partval = data.parttype
                    if (partval eq 0) then begin
                        dirname = dirname + 'r'
                        defname = defname + 'r'
                    endif
                endif
                filtr_val = data.filtr[0]
                filterstring = strcompress(filtr_val(0), /remove_all)
                if filterstring ne '' then begin
                    dirname = dirname + '(' + filterstring + ')'
                    defname = defname + '(' + filterstring + ')'
                endif

                ;**** Look for files in user space ****

                if (member ne '') then begin
                    pref = '(' + member + ')'
                    to_find = pref + dirname + '_'
                endif else begin
                    to_find = dirname + '_'
                endelse
                to_find_u = to_find + symbol + '.dat'
                com = ['find',inset.rootpath,'-name',to_find_u,'-print']
                spawn, com, userfiles, /noshell
                s=size(userfiles)
                if s(0) eq 0 then no_user_files=0 else $
                  no_user_files=n_elements(userfiles)

                ;**** Look for files in central space ****

                 if (member ne '') then begin
                     pref = '(' + member + ')'
                     to_find = pref + dirname + '_'
                 endif else begin
                     to_find = dirname + '_'
                 endelse
                 to_find_c = to_find + symbol + '.dat'
                 com = ['find',inset.centroot,'-name',to_find_c,'-print']
                 spawn, com, centfiles, /noshell

                 if (member ne '') then begin
                     pref = '(' + member + ')'
                     to_find = pref + defname + '_'
                 endif else begin
                     to_find = defname + '_'
                 endelse
                 to_find_c = to_find + symbol + '.dat'
                 com = ['find',inset.centroot,'-name',to_find_c,'-print']
                 spawn, com, defcentfiles, /noshell
                 
                 centfiles = [centfiles,defcentfiles]
                 s=size(centfiles)
                 if s(0) eq 0 then no_cent_files=0 else $
                   no_cent_files=n_elements(centfiles)


        if (no_user_files eq 0 and no_cent_files eq 0) then fileflag = 0

                old_nuf=no_user_files
                old_ncf=no_cent_files

                ;**** Check the user files for parent and ground indices ****
                ;**** or for signiature of unresolved datasets as appropriate ****

                if no_user_files gt 0 then begin
                    if data.filetype eq 1 then begin
                        if data.parttype eq 0 then begin
                            ok = intarr(no_user_files)
                            for i=0,no_user_files-1 do begin
                                ok(i) = check_indices(userfiles(i),$
                                         data.parindx[0], data.grndindx[0])
                            endfor
                        endif else begin
                            ok = intarr(no_user_files)
                            for i=0,no_user_files-1 do begin
                                ok(i) = check_partial(userfiles(i))
                            endfor
                        endelse
                        w = where(ok eq 1)
                        if w(0) ne -1 then begin
                            userfiles=userfiles(w)
                            no_user_files=n_elements(userfiles)
                        endif else no_user_files=0
                    endif
                endif
                
                ;**** Check the central files for parent and ground indices ****
                ;**** or for signiature of unresolved datasets as appropriate ****

                if no_cent_files gt 0 then begin
                    if data.filetype eq 1 then begin
                        if data.parttype eq 0 then begin
                            ok = intarr(no_cent_files)
                            for i=0,no_cent_files-1 do begin
                                ok(i) = check_indices(centfiles(i),$
                                         data.parindx[0], data.grndindx[0])
                            endfor
                        endif else begin
                            ok = intarr(no_cent_files)
                            for i=0,no_cent_files-1 do begin
                                ok(i) = check_partial(centfiles(i))
                            endfor
                        endelse
                        w = where(ok eq 1)
                        if w(0) ne -1 then begin
                            centfiles=centfiles(w)
                            no_cent_files=n_elements(centfiles)
                        endif else no_cent_files=0
                    endif
                endif

		;**** Search complete - popup information ****

	        if (no_user_files eq 0 and no_cent_files eq 0) then begin
                    mess = '**** Sorry - No data files match'+$
			           ' the given parameters ****'
                    if (data.filetype eq 1 and data.parttype eq 0) and $
                      (old_nuf ne 0 or old_ncf ne 0) then begin
                        mess = [mess,'**** but there were files with different indices ****']
                    endif else if (data.filetype eq 1 and data.parttype eq 1)$
                      and (old_nuf ne 0 or old_ncf ne 0) then begin
                        mess = [mess,"**** but there were 'Standard' files with the same specification ****"]
                    endif
                    action = popup(message=mess,   $
			           buttons=['  OK  '], font=state.font_large, $
			           title='Information:-')
	        endif else begin

		;**** Construct information for the popup window ****
		
		    headings=['Filename']
                    if no_user_files gt 0 then begin
                        choices = userfiles
                        if no_cent_files gt 0 then begin
                            choices = [choices,centfiles]
                        endif
                    endif else if no_cent_files gt 0 then begin
                        choices = [centfiles]
                    endif

		;**** Pop up the selection window ****

		    index = 0
		    action = -1
                    if n_elements(choices) gt 1 then begin
                        tit = 'The following files match the'+$
				    ' given parameters:- '
                    endif else begin
                        tit = 'The following file matches the'+$
				    ' given parameters:- '
                    endelse
		    cw_list_select, headings, choices, index, action,	$
				    font=state.font_large,		$
				    title=tit,		$
				    toptitle='Search results:-',	$
				    buttons=['Cancel','Select'], ysize=15
		    if action eq 1 then begin
		        widget_control, state.fileid, get_value=inset
                        if index lt no_user_files then begin
                            inset.file = dirname+'/'+to_find_u
                        endif else begin
                            inset.file = defname+'/'+to_find_c
                            inset.rootpath = inset.centroot
                        endelse
		        widget_control, state.fileid, set_value=inset
	 	        widget_control, state.doneid, /sensitive
		        widget_control, state.browseid, /sensitive
		    endif
	        endelse
                ;**** Destroy the information widget ****

                widget_control, widgetbase, /destroy
                         
		widget_control, topparent, sensitive=1
	    endelse
	end

                ;*********************
                ;**** Done button ****
                ;*********************

        state.doneid: begin
                ;**** Check that the indices exist if
                ;**** resolved data is chosen 
	    widget_control, state.dataid, get_value=data
	    widget_control, state.fileid, get_value=inset
            str = strmid(inset.file,5,1)
            ok = 1
            if str eq 'r' then begin
                ok=check_indices(inset.rootpath+inset.file,data.parindx[0],$
                                 data.grndindx[0])
            endif
            if ok eq 1 then begin
                new_event = {ID:parent, TOP:event.top,          		$
                             HANDLER:0L, ACTION:'Done'}
            endif else begin
                new_event = 0L
                widget_control, state.messid, set_value='** Error: The given indices '+$
                  'do not exist in the chosen dataset ***'
            endelse
        end

        ELSE: new_event = 0L

    ENDCASE


                ;*********************************************
                ;*** make "state" available to other files ***
                ;*********************************************

    widget_control, first_child, set_uvalue=state, /no_copy

    RETURN, new_event

END

;-----------------------------------------------------------------------------

FUNCTION cw_adas402_in, parent, classes, VALUE=value,			$
			FONT_LARGE=font_large, FONT_SMALL=font_small,	$
			RNUM = rnum


    IF (N_PARAMS() LT 2) THEN MESSAGE,					$
    'Must specify parent and classes for cw_adas402_in'

    ON_ERROR, 2                                 ;return to caller on error

                ;**** Set defaults for keywords ****

    IF NOT (KEYWORD_SET(value)) THEN begin
        inset = {	ROOTPATH	:	'./',			$
			CENTROOT	:	'',			$
			USERROOT	:	'',			$
                        FILE            :       '',                     $
                        FILTR           :       '',                     $
			FILETYPE	:	0,			$
			PARTTYPE	:	0,			$
			YEAR		:	'',			$
			DYEAR		:	'',			$
			SYMBOL		:	'',			$
			MEMBER		:	'',			$
                        CLASS		:	0,		        $
                        PARINDX         :       0,                      $
                        GRNDINDX        :       0          }
    ENDIF ELSE BEGIN
	inset = {	ROOTPATH        :       value.rootpath,		$
			CENTROOT        :       value.centroot,		$
			USERROOT        :       value.userroot,		$
                        FILE            :       value.file,             $
                        FILTR           :       value.filtr,            $
			FILETYPE	:	value.filetype,		$
			PARTTYPE	:	value.parttype,		$
			YEAR		:	value.year,		$
			DYEAR		:	value.dyear,		$
			SYMBOL		:	value.symbol,		$
			MEMBER		:	value.member,		$
                        CLASS           :       value.class,            $
                        PARINDX         :       value.parindx,          $
                        GRNDINDX        :       value.grndindx          }
        if strtrim(inset.rootpath) eq '' then begin
            inset.rootpath = './'
        endif else if                                                   $
        strmid(inset.rootpath, strlen(inset.rootpath)-1,1) ne '/' then begin
            inset.rootpath = inset.rootpath+'/'
        endif
    ENDELSE
    IF NOT (KEYWORD_SET(font_large)) THEN font_large = ''
    IF NOT (KEYWORD_SET(font_small)) THEN font_small = ''
    IF NOT (KEYWORD_SET(rnum)) THEN rnum = '402'

		;**** Modify font sizes depending on platform ****

    machine = GETENV('TARGET_MACHINE')
    if machine eq 'HPUX' then begin
	large_font = font_small
    endif else begin
	large_font = font_large
    endelse

                ;*********************************
                ;**** Create the Input widget ****
                ;*********************************

                ;**** create base widget ****

    topbase = widget_base(parent, EVENT_FUNC = "in402_event",		$
                          FUNC_GET_VALUE = "in402_get_val",		$
		          /column)

                ;******************************************************
                ;**** Create a dummy widget just to hold value of *****
                ;**** "state" variable so as not to get confused  *****
                ;**** with any other values. Adopt IDL practice   *****
                ;**** of using first child widget                 *****
                ;******************************************************

    first_child = widget_base(topbase)

    cwid = widget_base(first_child, /column)

		;*************************************
		;**** Input file selection widget ****
		;*************************************

    filst = {   ROOTPATH:inset.rootpath, FILE:inset.file,		$
                CENTROOT:inset.centroot, USERROOT:inset.userroot }
    fileid = cw_adas4xx_infile(cwid,value=filst,                    	$
                               title=title,font=large_font)

		;****************************
		;**** Data files widget  ****
		;****************************

    database = widget_base(cwid, /frame)
    classtit = "Search for iso-nuclear master file from details:-"
    dataid = cw_402_classes(database, cwid, classes, inset.class,	$
			       inset.userroot, inset.centroot,		$
			       font=large_font,	title=classtit, 	$
			       rnum = rnum)
    datavalue = {	filtr           :       inset.filtr,            $
                        member		:	inset.member,		$
			year		:	inset.year,		$
			dyear		:	inset.dyear,		$
			symbol		:	inset.symbol,		$
			filetype	:	inset.filetype,		$
                        parttype	:	inset.parttype,         $
                        class           :       inset.class,            $
                        parindx         :       inset.parindx,          $
                        grndindx        :       inset.grndindx          }

    widget_control, dataid, set_value = datavalue

    messid = widget_label(cwid,value=' ',font=large_font)

                ;*****************
                ;**** Buttons ****
                ;*****************

    base = widget_base(cwid, /row)

                ;**** Browse Dataset button ****

    browseid = widget_button(base, value='Browse File Comments',$
                             font=large_font)

                ;**** Search button ****

    searchid = widget_button(base, value='Search',$
                             font=large_font)

                ;**** Cancel Button ****

    cancelid = widget_button(base, value='Cancel', font=large_font)

                ;**** Done Button ****

    doneid = widget_button(base, value='Done', font=large_font)

		;******************************************************
		;**** Check filename and desensitise buttons if it ****
		;**** is a directory or it is a file without read  ****
		;**** access.					   ****
		;******************************************************

    filename = inset.rootpath+inset.file
    file_acc, filename, fileexist, read, write, execute, filetype
    if filetype ne '-' then begin
        widget_control, browseid, sensitive=0
        widget_control, doneid, sensitive=0
    endif else begin
        if read eq 0 then begin
            widget_control, browseid, sensitive=0
            widget_control, doneid, sensitive=0
        endif
    endelse

                ;*************************************************
                ;**** create a state structure for the pop-up ****
                ;****                window.                  ****
                ;*************************************************

    new_state = {	font_large	:	large_font,		$
			font_small	:	font_small,		$
			inval		:	inset,			$
			dataid		:	dataid,			$
			doneid		:	doneid,			$
			browseid	:	browseid,		$
			cancelid	:	cancelid,		$
                        fileid          :       fileid,                 $
                        rnum		: 	rnum,			$
                        searchid        :       searchid,               $
                        messid          :       messid}

                 ;**** Save initial state structure ****

    widget_control, first_child, set_uvalue=new_state, /no_copy
    
    RETURN, topbase

END
