; Copyright (c) 1996 Strathclyde University .
; SCCS info: Module @(#)$Header: /home/adascvs/idl/adas4xx/adas402/cw_402_classes.pro,v 1.2 2004/07/06 12:25:26 whitefor Exp $ Date $Date: 2004/07/06 12:25:26 $
;+
; PROJECT:
;       ADAS IBM MVS to DEC UNIX conversion
;
; NAME: CW_402_CLASSES()
;
; PURPOSE:
;       Sets up details of master files to be used in adas 402.
;
; EXPLANATION:
;       This function creates a compound widget consisting of seven
;       selection widgets: typeins for the year of data, default year
;	of data, element symbol, radiated power filter and member prefix 
;       and menu type selectors for the data classes, type of master 
;	files, type of partial file and directory branch to use (User
;	or Central). There is also a button to allow the user to see
;	what files there are which actually match their choices.
;
; USE:
;       See routine cw_adas402_in.pro for an example.
;
; INPUTS:
;       PARENT  -       Long integer; the ID of the parent widget.
;
;	CLASSES -	String array; the master classes to be used.
;
;	CLASS   -	Int; the index of the selected class.
;
;	USERDIR -	String: The rootpath for the user directories.
;
;	CENTDIR -	String: The rootpath for the central directories.
;
; OPTIONAL INPUTS:
;       None.
;
; OUTPUTS:
;       The return value of this function is the ID of the compound
;       widget which is of type LONG integer.
;
; OPTIONAL OUTPUTS:
;       None
;
; KEYWORD PARAMETERS:
;       FONT    - A font to use for all text in this widget.
;
;       TITLE   - The title to be included in this widget.
;
;	RNUM    - The number of the routine (as a string) that is calling
;		  this: used in information window titles
;
; CALLS:
;       CW_BSELECTOR - Pull-down menu type compound widget.
;	CW_DISPLAY_LIST - Displays a list of text items in a pop-up.
;	I4EIZ0	     - Converts element symbol into nuclear charge.
;	POPUP	     - Simple message pop-up window.
;
; SIDE EFFECTS:
;       Three other routines are included which are used to manage the
;       widget:
;
;       MASTER_402_SET_VAL()
;       MASTER_402_GET_VAL()
;       MASTER_402_EVENT()
;
; CATEGORY:
;       Compound Widget
;
; WRITTEN:
;       William Osborn, Tessella Support Services plc, 28th October 1996
;
; MODIFIED:
;     1.1   William Osborn
;		First release. Routine written using cw_master_classes.pro
;	1.2	Richard Martin
;		Renamed common block to mastercom402 to avoid conflictwith 405
;
; VERSION:
;	1.1	28-10-96
;	1.2	20-09-99
;
;-
;-----------------------------------------------------------------------------
     
PRO master_402_set_val, id, value

                ;**** Return to caller on error ****

    ON_ERROR, 2

                ;**** Retrieve the state ****

    first_child = widget_info(id, /child)
    widget_control, first_child, get_uvalue=state, /no_copy

                ;**** Set data field values ****

		;**** set filter value ****

    if value.filtr eq '' then filval=' ' else filval=value.filtr
    widget_control, state.filtr, set_value=filval

		;**** Set member prefix value ****
 
    if value.member eq '' then memval=' ' else memval=value.member
    widget_control, state.member, set_value=memval

		;**** Set directory branch ****

;    widget_control, state.branch, set_value=value.branch

		;**** Set year and default year values ****

    if value.year eq '' then yval=' ' else yval=value.year
    if value.dyear eq '' then dyval=' ' else dyval=value.dyear
    widget_control, state.year, set_value=yval
    widget_control, state.dyear, set_value=dyval


		;**** Set isonuclear element symbol ****

    if value.symbol eq '' then symval=' ' else symval=strlowcase(value.symbol)
    widget_control, state.symbol, set_value=symval

		;**** Set file type and type of partial file ****

    widget_control, state.filetype, set_value=value.filetype
    widget_control, state.parttype, set_value=value.parttype,		$
		    sensitive=value.filetype
    widget_control, state.typelabel, sensitive=value.filetype
    widget_control, state.indxbase, sensitive=value.filetype
        
                ;**** Set indices ****
    widget_control, state.parindxid, $
      set_value=string(value.parindx,format='(I3)')
    widget_control, state.grndindxid, $
      set_value=string(value.grndindx,format='(I3)')

                ;**** Reset the value of state variable ****

    widget_control, first_child, set_uvalue=state, /no_copy

END

;-----------------------------------------------------------------------
  
FUNCTION master_402_get_val, id

    COMMON mastercom402, topparent

                ;**** Return to caller on error ****

    ON_ERROR, 2

                ;*******************************
                ;**** Retrieve the state    ****
                ;**** Get first_child id    ****
                ;**** as state stored there ****
                ;*******************************

    first_child = widget_info(id, /child)
    widget_control, first_child, get_uvalue=state

    widget_control, state.filtr, get_value=filtr
    widget_control, state.classbut, get_value=class
    widget_control, state.member, get_value=member
    widget_control, state.year, get_value=year
    widget_control, state.dyear, get_value=dyear
    widget_control, state.symbol, get_value=symbol
    symbol = strlowcase(symbol)
;    widget_control, state.branch, get_value=branch
    widget_control, state.filetype, get_value=filetype
    widget_control, state.parttype, get_value=parttype
    widget_control, state.parindxid, get_value=parindx
    widget_control, state.grndindxid, get_value=grndindx
    searchflag = state.searchflag

    datavalue = {	filtr		:	filtr,			$
			year		:	year,			$
			dyear		:	dyear,			$
			symbol		:	symbol,			$
;			branch		:	branch,			$
			filetype	:	filetype,		$
			parttype	:	parttype,		$
                        class		:	class,		        $
			searchflag	:	searchflag,		$
			member		:	member,			$
                        parindx         :       fix(parindx),           $
                        grndindx        :       fix(grndindx) }

    widget_control, first_child, set_uvalue=state, /no_copy

    RETURN, datavalue

END

;------------------------------------------------------------------------
  
FUNCTION master_402_event, event

    COMMON mastercom402, topparent
    COMMON master_dirs, userdir, centdir

    parent=event.handler
    searchflag = 0

                ;**********************************************
                ;**** Retrieve the user value state        ****
                ;**** Get id of first_child widget because ****
                ;**** user value "state" is stored there   ****
                ;**********************************************

    first_child = widget_info(parent, /child)
    widget_control, first_child, get_uvalue=state, /no_copy
                                                        
                ;************************
                ;**** Process events ****
                ;************************

    CASE event.id OF

        state.filtr: widget_control, state.member, /input_focus

	state.member: widget_control, state.year, /input_focus

	state.year: widget_control, state.dyear, /input_focus

	state.dyear: widget_control, state.symbol, /input_focus

	state.symbol: begin
	    widget_control, state.symbol, get_value=symbol
	    widget_control, state.symbol, set_value=strlowcase(symbol(0))
	    widget_control, state.filtr, /input_focus
	end

	state.filetype: begin
	    flag=event.index
            widget_control, state.parttype, sensitive=flag
            widget_control, state.typelabel, sensitive=flag
            widget_control, state.parttype, get_value=val
            if flag eq 1 and val(0) eq 0 then begin
                widget_control, state.indxbase, sensitive=1
            endif else begin
                widget_control, state.indxbase, sensitive=0
            endelse
        end
        state.parttype: begin
            if event.index eq 0 then begin
                widget_control, state.indxbase, sensitive=1
            endif else begin
                widget_control, state.indxbase, sensitive=0
            endelse
        end

        state.parindxid: widget_control, state.grndindxid, /input_focus
        state.grndindxid: widget_control, state.parindxid, /input_focus

        else : begin 				;**** do nothing ****
        end
    ENDCASE

    state.searchflag = searchflag

                ;*********************************************
                ;*** make "state" available to other files ***
                ;*********************************************

    widget_control, first_child, set_uvalue=state, /no_copy

    RETURN, event

END

;-------------------------------------------------------------------------
        
FUNCTION cw_402_classes, parent, topparent, classes, class, 	$
                            userdir, centdir, FONT=font, TITLE=title, 	$
			    RNUM=rnum

    COMMON master_dirs, userdircom, centdircom
    COMMON mastercom402, topcom

    userdircom = userdir
    centdircom = centdir
    topcom = topparent
    searchflag = 0			;has the user clicked search?

                ;**** Set defaults for keywords ****

    if (NOT KEYWORD_SET(font)) then font = ''
    if (NOT KEYWORD_SET(title)) then title = ' '
    if (NOT KEYWORD_SET(rnum)) then rnum = '402'

                ;**** Create main base for widget ****

    top = widget_base(parent, event_func='master_402_event',                $
                      func_get_val='master_402_get_val',                    $
                      pro_set_val='master_402_set_val',                     $
                      /column)

                ;******************************************************
                ;**** Create a dummy widget just to hold value of *****
                ;**** "state" variable so as not to get confused  *****
                ;**** with any other values. Adopt IDL practice   *****
                ;**** of using first child widget                 *****
                ;******************************************************

    first_child = widget_base(top)

    cwid = widget_base(first_child, /column)

                ;**** Title ****

    titleid = widget_label(cwid, value=title, font=font)


                ;**** New base for buttons ****

    infobase = widget_base(cwid, /frame)

                ;**** Buttons ****

    base = widget_base(infobase, /column)

		;**** Master classes menu ****

    classbase = widget_base(base, /row)
    classval = "Select iso-nuclear master collisional-dielectronic class   :"
    classlabel = widget_label(classbase, font=font, value=classval)
    classbut = cw_bselector(classbase, classes, set_value=class, font=font)

		;**** Radiated power filter typein ****

    filtrbase = widget_base(base, /row)
    filtrval = "                   Radiated power filter (blank for none)    :"
    filtrlabel = widget_label(filtrbase, font=font, value=filtrval)
    filtr = widget_text(filtrbase, value=' ', xsize=8, /editable,	$
                        font=font)

		;**** Member prefix ****

    membase = widget_base(base, /row)
    memval = "                           Member prefix (blank for none)    :"
    memlabel = widget_label(membase, font=font, value=memval)
    member = widget_text(membase, value=' ', xsize=8, /editable,       $
                        font=font)

		;**** Directory branch ****
                ;**** This has been removed since the
                ;**** user's selection in cw_adas4xx_infile
                ;**** is now used
;    branbase = widget_base(base, /row)
;    branval = "                                  Select directory branch    :"
;    branlabel = widget_label(branbase, font=font, value=branval)
;    branch = cw_bselector(branbase, ['User','Central'], font=font)

    		;**** Year typeins ****

    yearbase = widget_base(base, /row)
    yearval = " Year of data    :"
    dyearval = "   Default year (if required)    :"
    yearlabel = widget_label(yearbase, font=font, value=yearval)
    year = widget_text(yearbase, value = ' ', xsize=7, /editable, font=font)
    dyearlabel = widget_label(yearbase, font=font, value=dyearval)
    dyear = widget_text(yearbase, value = ' ', xsize=8, /editable, font=font)

		;**** Element symbol typein ****

    symbase = widget_base(base, /row)
    symval = "                                Isonuclear element symbol    :"
    symlabel = widget_label(symbase, font=font, value=symval)
    symbol = widget_text(symbase, value=' ', xsize=8, /editable, font=font)

		;**** File type ****

    filebase = widget_base(base, /row)
    fileval = "Type of master files  :"
    typeval = " Specify partial type code   :"
    filelabel = widget_label(filebase, value=fileval, font=font)
    filetype = cw_bselector(filebase, ['Standard',' Partial '], font=font)
    typelabel = widget_label(filebase, value=typeval, font=font)
    parttype = cw_bselector(filebase, [' Resolved  ','Unresolved'], font=font)

                                ;**** Internal file index choices
    ibase = widget_base(base, /row)
    plab = widget_label(ibase, value='                       Parent index', font=font)
    parindxid = widget_text(ibase, xsize=4, font=font, /editable)
    glab = widget_label(ibase, value='Recombined ion index', font=font)
    grndindxid = widget_text(ibase, xsize=4, font=font, /editable)

                ;**** Create state structure ****

    state = {	font		:	font,				$
		parttype	:	parttype,			$
                typelabel	:	typelabel,			$
		filetype	:	filetype,			$
		classbut	:	classbut,			$
		year		:	year,				$
		dyear		:	dyear,				$
;		branch		:	branch,				$
		member		:	member,				$
		symbol		:	symbol,				$
		classes		:	classes,			$
		searchflag	:	searchflag,			$
		filtr		:	filtr,				$
		rnum		:	rnum,				$
                parindxid       :       parindxid,                      $
                grndindxid      :       grndindxid,                     $
                indxbase        :       ibase}

                ;**** Save initial state structure ****

    widget_control, first_child, set_uvalue=state, /no_copy

    RETURN, top

END
