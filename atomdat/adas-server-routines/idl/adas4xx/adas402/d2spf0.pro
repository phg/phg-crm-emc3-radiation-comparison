; Copyright (c) 1996 Strathclyde University .
; SCCS info: Module @(#)$Header: /home/adascvs/idl/adas4xx/adas402/d2spf0.pro,v 1.2 2004/07/06 13:13:32 whitefor Exp $ Date $Date: 2004/07/06 13:13:32 $
;+
; PROJECT:
;       ADAS IBM MVS to UNIX conversion
;
; NAME:
;       D2SPF0
;
; PURPOSE:
;       IDL user interface and communications with ADAS402 FORTRAN
;       process via pipe.
;
; EXPLANATION:
;       Firstly this routine invokes the part of the user interface
;       used to select the inputs for ADAS402. When the user's
;       interactions are complete this routine communicates with the
;       ADAS402 FORTRAN application via a UNIX pipe.  Communications
;       are to the FORTRAN subroutine D2SPF0.
;
; USE:
;       The use of this routine is specific to ADAS402, see adas402.pro.
;
; INPUTS:
;       PIPE    - The IDL unit number of the bi-directional pipe to the
;                 ADAS402 FORTRAN process.
;
;       INVAL	- A structure which determines the initial settings of
;                 the input screen widgets.
;                 INVAL is passed un-modified through to cw_adas402_in.pro,
;                 see that routine for a full description.
;
;       CLASSES - String array: the names of the isonuclear master classes
;
; OPTIONAL INPUTS:
;       None
;
; OUTPUTS:
;       INVAL	- On output the structure records the final settings of
;                 the input screen widgets if the user pressed the
;                 'Done' button, otherwise it is not changed from input.
;
;       REP     - String; Indicates whether the user pressed the 'Done'
;                 or 'Cancel' button on the interface.  The action is
;                 converted to the strings 'NO' and 'YES' respectively
;                 to match up with the existing FORTRAN code.  In the
;                 original IBM ISPF interface REP was the reply to the
;                 question 'DO YOU WANT TO END PROGRAM EXECUTION'.
;       DSFULL  - Full input filename
;
;       CLASSVAL, YEARVAL, FILEVAL, CELEM - Information strings for
;                                           use in cw_adas402_proc
;
;
; OPTIONAL OUTPUTS:
;       None
;
; KEYWORD PARAMETERS:
;       FONT_LARGE	- Supplies the large font to be used for the 
;			  interface widgets.
;
;	FONT_SMALL	- Supplies the small font to be used for the 
;			  interface widgets.
; CALLS:
;       ADAS402_IN   	- Pops-up the input selections widget.
;
; SIDE EFFECTS:
;       This routine communicates with the ADAS402 FORTRAN process
;       via a UNIX pipe.
;
; CATEGORY:
;       Adas system.
;
; WRITTEN:
;       William Osborn, Tessella Support Services plc, 28th October 1996
;
; MODIFIED:
;       1.1             William Osborn
;			First release - written using d5ispf.pro
;
;       1.2             William Osborn
;			Added default definition of lpartl
;
; VERSION:
;       1.1             28-10-96
;       1.2             26-11-96
;
;-----------------------------------------------------------------------------
;-

PRO d2spf0, pipe, inval, classes, rep,	dsfull, classval, yearval,	$
            fileval, celem,			                $
            FONT_LARGE=font_large, FONT_SMALL=font_small


                ;**** Set defaults for keywords ****

    IF NOT (KEYWORD_SET(font_large)) THEN font_large = ''
    IF NOT (KEYWORD_SET(font_small)) THEN font_small = ''

                ;**********************************
                ;**** Pop-up input file widget ****
                ;**********************************

    adas402_in, inval, classes, action, WINTITLE = 'ADAS 402 INPUT',	$
		FONT_LARGE=font_large, FONT_SMALL=font_small

                ;********************************************
                ;**** Act on the event from the widget   ****
                ;********************************************
                ;**** There are only two possible events ****
                ;**** 'Done' and 'Cancel'.               ****
                ;********************************************

    if action eq 'Done' then begin
        rep = 'NO'
    endif else begin
        rep = 'YES'
    endelse
    printf, pipe, rep

    if rep eq 'NO' then begin

                ;*******************************
                ;**** Write data to fortran ****
                ;*******************************

		;**** Write filename to FORTRAN ****
        dsfull = inval.rootpath+inval.file
        printf, pipe, dsfull

		;***************************************************
		;**** Construct titles for processing screen    ****
		;***************************************************

        classval = 'Class     :                                         '+$
                       '                     '
        yearval  = 'Year      :                                         '+$
                       '                     '
        fileval  = 'File type :                                         '+$
                       '                     '
    		;**** What class is the data? ****

        iswit = 0
        dstart = strpos(dsfull, 'adf') + 6
        dataclass = strmid(dsfull, dstart, 3)
;        printf, pipe, dataclass
        CASE dataclass OF
            'acd' : begin
                classstring = 'ACD - Recombination coefficients'
                chead = 'Recombination coefficients'
                strput, classval, classstring, 12
                iswit = 1
            end
            'scd' : begin
                classstring = 'SCD - Ionisation coefficients'
                chead = 'Ionisation coefficients'
                strput, classval, classstring, 12
                iswit = 2
            end
            'ccd' : begin
                classstring = 'CCD - Recombination coefficients: charge'+$
                  ' exchange'
                chead = 'Recombination coefficients: Charge exchange'
                strput, classval, classstring, 12
                iswit = 3
            end
            'prb' : begin
                classstring = 'PRB - Power coefficients: recombination-'+$
                  'bremsstrahlung'
                chead = 'Power coefficients: Recombination-Bremsstrahlung'
                strput, classval, classstring, 12
                iswit = 4
            end
            'prc' : begin
                classstring = 'PRC - Power coefficients: charge-exchange'+$
                  ' recombination'
                chead = 'Power coefficients: Charge-exchange recombination'
                strput, classval, classstring, 12
                iswit = 5
            end
            'qcd' : begin
                classstring = 'QCD - Cross-coupling coefficients'
                chead = 'Cross-coupling coefficients'
                strput, classval, classstring, 12
                iswit = 6
            end
            'xcd' : begin
                classstring = 'XCD - Cross-coupling coefficients: parent'
                chead = 'Cross-coupling coefficients: parent'
                strput, classval, classstring, 12
                iswit = 7
            end
            'plt' : begin
                classstring = 'PLT - Line power coefficients: total'
                chead = 'Line power coefficients: total'
                strput, classval, classstring, 12
                iswit = 8
            end
            'pls' : begin
                classstring = 'PLS - Line power coefficients: specific'
                chead = 'Line power coefficients: specific'
                strput, classval, classstring, 12
                iswit = 9
            end
            ELSE  : begin
                strput, classval, 'UNKNOWN', 12
                chead = 'UNKNOWN'
            end
        END
        ;**** Write class to FORTRAN ****
        printf, pipe, iswit

		;**** What year is the data from? ****

        dstart = dstart + 3
        year = strmid(dsfull, dstart, 2)
        yearchk = byte(year)
        if (max(yearchk) gt 57) or (min(yearchk) lt 48) then begin
            strput, yearval, 'UNKNOWN', 12
        endif else begin
            strput, yearval, string(year), 12
        endelse
;        printf, pipe, strtrim(string(year),2)
        printf, pipe, fix(year)

		;**** What sort of file is it? ****
                ;**** Differentiate between resolved and standard   ****
		;**** or unresolved using the filename.             ****
		;**** Differentiate between standard and unresolved ****
		;**** by searching for 'IPRT=' in the file itself.  ****

        preftest = strmid(dsfull, dstart, 20)
        preflag = strpos(preftest, '#')
        if (preflag(0) ne 2) then begin
            dstart = dstart + 10
        endif else begin
            dstart = dstart + 13
        endelse
        filechk = strmid(dsfull, dstart, 2)
        filebyt = byte(filechk)
        if (filebyt(0) eq 46) or (filebyt(1) eq 46) then begin
            openr, unit, dsfull, /get_lun
            on_ioerror, endfil
            a=''
again:
            readf,unit,a,format='(a80)'
            if strpos(a,'Z1=') ne -1 then begin
                if strpos(a,'IPRT') ne -1 then begin
                    strput, fileval, 'Standard', 12
                    lpartl = 0
                    lreso = 0
                endif else begin
                    strput, fileval, 'Partial - Unresolved', 12
                    lpartl = 1
                    lreso = 0
                endelse
                goto, endfil
            endif
            goto, again
endfil:
            free_lun, unit
        endif else begin
            filechk=strmid(preftest,2,1)
            if filechk eq 'r' then begin
                strput, fileval, 'Partial - Resolved', 12
		lpartl = 1
                lreso = 1
            endif else begin
                strput, fileval, 'Unknown', 12
		lpartl = 0
                lreso = 0
            endelse
        endelse
        if not keyword_set(lpartl) then begin
            strput, fileval, 'Unknown', 12
            lpartl = 0
            lreso = 0
        endif
        printf, pipe, lpartl
        printf, pipe, lreso

		;**** What's the symbol? ****

        dstart = strpos(dsfull, '_') + 1
        symchk = strmid(dsfull, dstart, 2)
        symbt  = byte(symchk)
        if ((symbt(1) le 57) and (symbt(1) ge 48)) 			$
          or (symbt(1) eq 46) or (symbt(1) eq 35) then begin
            symstring = strupcase(strmid(symchk, 0, 1)) ;Single letter symbol
        endif else begin
            symstring = strupcase(strmid(symchk, 0, 1)) + 		$
              strmid(symchk, 1, 1)
        endelse

        celem = strcompress(symstring, /remove_all)

        ;**** Output indices ****
        printf, pipe, inval.parindx
        printf, pipe, inval.grndindx

    endif


END
