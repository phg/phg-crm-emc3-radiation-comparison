; Copyright (c) 1996, Strathclyde University.
; SCCS info: Module @(#)$Header: /home/adascvs/idl/adas4xx/adas402/d2outg.pro,v 1.1 2004/07/06 13:13:23 whitefor Exp $ Date $Date: 2004/07/06 13:13:23 $
;+
; PROJECT:
;       ADAS IBM MVS to UNIX conversion
;
; NAME:
;	D2OUTG
;
; PURPOSE:
;	Communication with ADAS402 FORTRAN via UNIX pipe and
;	graphics output.
;
; EXPLANATION:
;	The routine begins by reading information from the ADAS402
;	FORTRAN process via a UNIX pipe.  Then the IDL graphical
;	output routine for ADAS402 is invoked.  Communications are to
;	the FORTRAN subroutine D2OUTG.
;
; USE:
;	This routine is specific to adas402.                            
;
; INPUTS:
;	DSFULL   - Data file name
;
;	PIPE	 - The IDL unit number of the bi-directional pipe to the
;		   ADAS402 FORTRAN process.
;
;       UTITLE   - Optional comment by user
;
;	GRPSCAL	 - Integer; 0 if default scaling required 1 if user
;		   specified scaling to be used.
;
;	XMIN	 - String; User sepcified x-axis minimum, number as string.
;
;	XMAX	 - String; User sepcified x-axis maximum, number as string.
;
;	YMIN	 - String; User specified y-axis minimum, number as string.
;
;	YMAX	 - String; User specified y-axis maximum, number as string.
;
;	HRDOUT	 - Integer; 0 if no hardcopy required, 1 if hardcopy.
;
;	HARDNAME - String; file name for hardcopy output.
;
;	DEVICE	 - String; IDL name for hardcopy output device.
;
;	HEADER	 - ADAS version header information for inclusion in the
;		   graphical output.
;
;       LFSEL    - Minimax plotting option
;
;       LOSEL    - Spline plotting option
;
;       BITFILE - String; the path to the dirctory containing bitmaps
;                 for the 'escape to series menu' button.
;
; OPTIONAL INPUTS:
;	None.
;
; OUTPUTS:
;       GOMENU  - Int; flag - set to 1 if user has selected 'escape direct
;                 to series menu , otherwise 0
;
; OPTIONAL OUTPUTS:
;	None.
;
; KEYWORD PARAMETERS:
;	FONT	- String; Name of the font to be used for the graphical
;		  output widget.
;
; CALLS:
;	ADAS402_PLOT	ADAS402 graphical output.
;
; SIDE EFFECTS:
;	This routine reads information from FORTRAN via a UNIX pipe.
;
; CATEGORY:
;	Adas system.
;
; WRITTEN:
;       William Osborn, Tessella Support Services plc, 30-Oct-1996
;
; MODIFIED:
;	1.1	William Osborn
;		First release
;
; VERSION:
;	1.1	30-10-96
;
;-
;-----------------------------------------------------------------------------

PRO d2outg, dsfull, pipe, utitle, grpscal, xmin, xmax, ymin, ymax, 	$
	    hrdout, hardname, device, header, lfsel, losel,	$
            gomenu, bitfile, apden, restit, FONT=font

                ;**** Set defaults for keywords ****

    IF NOT (KEYWORD_SET(font)) THEN font = ''


		;**** Declare variables for input ****

    idum=0
    sdum=""
    fdum=0.0

		;********************************
		;**** Read data from fortran ****
		;********************************
    readf, pipe, idum
    maxnumpoi = idum
    readf, pipe, idum
    iz1min = idum
    readf, pipe, idum
    iz1max = idum
    ndplot=iz1max-iz1min+1
                                ;**** Create array of structures to
                                ;**** hold the master file data
    arr = fltarr(maxnumpoi)
    mfdata = replicate({plot_struct, numpoi:0, x:arr, y:arr, labx:0.0, $
                        laby:0.0, lab:''}, ndplot)

    for j=0,ndplot-1 do begin
        readf, pipe, idum
        mfdata(j).numpoi = idum
        if idum gt 0 then begin
            for i=0,idum-1 do begin
                readf,pipe,fdum
                mfdata(j).x(i)=fdum
                readf,pipe,fdum
                mfdata(j).y(i)=fdum
            endfor
            readf,pipe,fdum
            mfdata(j).labx=fdum
            readf,pipe,fdum
            mfdata(j).laby=fdum
            readf,pipe,sdum
            mfdata(j).lab=sdum
        endif
    endfor

    ;**** Read in minimax plot data ****
    minmax = {plot_struct}
    readf,pipe,idum
    minmax.numpoi=idum
    if idum gt 0 then begin
        for i=0,idum-1 do begin
            readf,pipe,fdum
            minmax.x(i)=fdum
            readf,pipe,fdum
            minmax.y(i)=fdum
        endfor
    endif

    ;**** Read in spline plot data ****
    spline = {plot_struct}
    readf,pipe,idum
    spline.numpoi=idum
    for i=0,idum-1 do begin
        readf,pipe,fdum
        spline.x(i)=fdum
        readf,pipe,fdum
        spline.y(i)=fdum
    endfor
    readf, pipe, idum
    if idum gt 0 then begin
        readf, pipe, fdum
        spline.labx=fdum
        readf, pipe, fdum
        spline.laby=fdum
        readf, pipe, sdum
        spline.lab=sdum
    endif

    readf,pipe,sdum
    ytit=sdum
    readf,pipe,sdum
    title=sdum
    readf,pipe,sdum
    titlf=sdum
    readf,pipe,sdum
    titlm=sdum
    readf,pipe,sdum
    xtit=sdum
		;***********************
		;**** Plot the data ****
		;***********************

    adas402_plot, dsfull, hrdout, hardname, device, header, utitle, apden,$
      mfdata, ndplot, minmax, spline, title, titlm, xtit, ytit,     $
      lfsel, losel, grpscal, xmin, xmax, ymin, ymax, gomenu, bitfile, restit, FONT=font

END
