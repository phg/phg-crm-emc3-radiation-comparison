; Copyright (c) 1996, Strathclyde University 
; SCCS INFO : Module @(#)$Header: /home/adascvs/idl/adas4xx/adas402/d2ispf.pro,v 1.1 2004/07/06 13:13:14 whitefor Exp $ Date $Date: 2004/07/06 13:13:14 $
;
;+
; PROJECT:
;       ADAS IBM MVS to UNIX conversion
;
; NAME:
;	D2ISPF
;
; PURPOSE:
;	IDL user interface and communications with ADAS402 FORTRAN
;	process via pipe.
;
; EXPLANATION:
;	The routine begins by reading information from the ADAS402
;	FORTRAN process via a UNIX pipe.  Then part of the ADAS402
;	IDL user interface is invoked to determine how the user
;	wishes to process the input dataset.  When the user's
;	interactions are complete the information gathered with
;	the user interface is written back to the FORTRAN process
;	via the pipe.  Communications are to the FORTRAN subroutine
;	D1ISPF.
;
; USE:
;	The use of this routine is specific to ADAS402, see adas402.pro.
;
; INPUTS:
;	PIPE	- The IDL unit number of the bi-directional pipe to the
;		  ADAS402 FORTRAN process.
;
;	LPEND	- Integer, 0 if the user pressed the 'Done' button
;		  or 1 if the user pressed 'Cancel'.  Maps directly onto
;		  the logical variable LPEND in ADAS402 FORTRAN.
;
;	PROCVAL	- A structure which determines the initial settings of
;		  the processing options widget.  The initial value is
;		  set in adas402.pro.  If adas402.pro passes a blank 
;		  dummy structure of the form {NEW:-1} into PROCVAL then
;		  PROCVAL is reset to a default structure.
;
;		  The PROCVAL structure is;
;			procval = {                        	$
;                			new   : 0 ,             $
;                			title : '',             $
;                			lfsel : 0,              $
;                			tolval: 5,              $
;					ifout : 1,		$
;                                       tin   : temp_arr,       $
;                                       din   : dens_arr,       $
;                                       maxt  : 0,              $
;                                       maxd  : 0,              $
;					tvals : dblarr(3,ndtin),$
;					losel : 0,		$
;					z0val : '',		$
;					zval  : '',		$
;					z1val : '',		$
;                                       apden : 0.0            $
;;                                       ievcut: 0.0             $
;              			}
;
;
;		  See cw_adas402_proc.pro for a full description of this
;		  structure.
;	
;
;	DSFULL	- String; The full system file name of the input 
;		  dataset selected by the user for processing.
;
;       CLASSVAL- String; Includes the class as extracted from the name
;                 of the file the user has selected. Used as a label on
;                 the processing screen.
;
;       YEARVAL - String; Includes the year of the data file as extracted
;                 from the the name of the file the user has selected.
;                 Used as a label on the processing screen.
;
;       FILEVAL - String; Shows whether file is partial or standard.
;                 Used as a label on the processing screen.
;
;       CELEM   - String; The element symbol extracted from the file name.
;
;       BITFILE - String; the path to the dirctory containing bitmaps
;                 for the 'escape to series menu' button.
;
; OPTIONAL INPUTS:
;	None
;
; OUTPUTS:
;	PROCVAL	- On output the structure records the final settings of
;		  the processing selections widget if the user pressed the
;		  'Done' button, otherwise it is not changed from input.
;
;       GOMENU  - Int; flag - set to 1 if user has selected 'escape direct
;                 to series menu , otherwise 0
;
; OPTIONAL OUTPUTS:
;	None
;
; KEYWORD PARAMETERS:
;	FONT_LARGE - The name of a larger font.
;
;	FONT_SMALL - The name of a smaller font.
;
;	EDIT_FONTS - A structure of two fonts used in the table editor
;		     adas_edtab.pro, {font_norm:'',font_input:''}.
;
; CALLS:
;	ADAS402_PROC	Invoke the IDL interface for ADAS402 data
;		 	processing options/input.
;
; SIDE EFFECTS:
;	Two way communications with ADAS402 FORTRAN via the
;	bi-directional UNIX pipe.
;
; CATEGORY:
;	Adas system.
;
; WRITTEN:
;       William Osborn, Tessella Support Services plc, 29-Oct-1996
;
; MODIFIED:
;	1.1     William Osborn
;		First release
;
; VERSION:
;	1.1	29-10-96
;
;-
;-----------------------------------------------------------------------------


PRO d2ispf, 	pipe, lpend, procval, dsfull,				$
		classval, yearval, fileval, 		                $
		celem, 					                $
		gomenu, bitfile,				        $
		FONT_LARGE=font_large, FONT_SMALL=font_small, 		$
		EDIT_FONTS=edit_fonts


                ;**** Set defaults for keywords ****

    IF NOT (KEYWORD_SET(font_large)) THEN font_large = ''
    IF NOT (KEYWORD_SET(font_small)) THEN font_small = ''
    IF NOT (KEYWORD_SET(edit_fonts)) THEN 				$
    edit_fonts = {font_norm:'',font_input:''}

		;********************************************
		;****     Declare variables for input    ****
		;**** arrays wil be declared after sizes ****
		;**** have been read.                    ****
                ;********************************************
    lpend  = 0
    ntdmax = 0
    nblock = 0
    nenrgy = 0
    nener = 0
    lsetl = 0
    lsetm = 0
    nmin = 1
    nmax = 2
    input = 0

		;********************************
		;**** Read data from fortran ****
		;********************************

    readf, pipe, input
    ntdmax = input
    readf, pipe, input
    ndtin = input
    readf, pipe, input
    ndden = input
    ite = ndtin  ;????
    ide = ndden  ;????
    tvals = dblarr(3, ndtin)
    teda  = dblarr(ndden)
    for j=0,(ndtin-1) do begin
        readf, pipe, fdum
        tvals(1,j) = fdum                       ;Input temperatures(eV)
        tvals(0,j) = fdum*1.160D+04
    endfor					
    for j=0,(ndden-1) do begin
        readf, pipe, fdum
        teda(j) = fdum                          ;Input densities
    endfor


		;*******************************************
		;**** Set default value if non provided ****
		;*******************************************

    if (procval.new lt 0) then begin
        temp_arr = fltarr(ndtin)
        dens_arr = fltarr(ndden)
        procval = {			$
		   new   : 0 ,          $
 		   title : '',		$
 		   lfsel : 0,		$
 		   tolval: 5,           $
		   ifout : 1,		$
 		   losel : 0,		$
                   tin   : temp_arr,    $
                   din   : dens_arr,    $
                   maxt  : 0,           $
                   maxd  : 0,           $
	    	   tvals : tvals,	$
		   z0val : '',	        $
		   zval  : '',		$
		   z1val : '',		$
                   apden : 1.0         $
;                   ievcut: 0.0          $
	          }
    endif
    procval.tvals = tvals
    ;**** Set z0 to the nuclear charge of the dataset ****
    i4eiz0, celem, z0
    procval.z0val = string(z0,format='(I3)')
    ;**** Calculate the reduced temperatures given z1val ****
    if num_chk(procval.z1val) ne 0 then z1 = 1.0 else z1 = float(procval.z1val)
    for j=0,ndtin-1 do begin
        procval.tvals(2,j) = tvals(0,j)/(z1^2)
    endfor

		;****************************************
		;**** Pop-up processing input widget ****
		;****************************************

    adas402_proc, procval, dsfull,  					$
		  classval, yearval, fileval, action,	                $
		  celem, 				                $
		  ntdmax, teda, ndtin, ndden, 				$
		  ite, ide, bitfile,					$
		  FONT_LARGE=font_large, FONT_SMALL=font_small,		$
		  EDIT_FONTS=edit_fonts

		;********************************************
		;****  Act on the event from the widget  ****
		;**** There are three possible events    ****
		;**** 'Done', 'Cancel' and 'Menu'.       ****
		;********************************************

    gomenu = 0
    if action eq 'Done' then begin
        lpend = 0
    endif else if action eq 'Menu' then begin
        lpend = 0
        gomenu = 1
    endif else begin
        lpend = 1
    endelse

		;*******************************
		;**** Write data to fortran ****
		;*******************************

    printf, pipe, lpend
    if lpend eq 0 then begin
        printf, pipe, procval.title
;        printf, pipe, procval.ievcut

        ;**** ifout already a FORTRAN rather than IDL index ****
        printf, pipe, procval.ifout

        ;**** Density and Rate coefficient units are always choice 1 ****
        one = 1
        printf, pipe, one
        printf, pipe, one

        printf, pipe, procval.maxt
        printf, pipe, procval.lfsel
        printf, pipe, procval.losel
        printf, pipe, procval.tolval
        ;**** Convert z0 from symbol if necessary ****
        z0byte = byte(procval.z0val)
        if (min(z0byte) lt 48) or (max(z0byte) gt 57) then begin
            i4eiz0, procval.z0val, z0test
        endif else begin
            z0test = fix(procval.z0val)
        endelse
        printf, pipe, z0test
        printf, pipe, fix(procval.z1val)
        printf, pipe, fix(procval.zval)
        printf, pipe, procval.apden
        if procval.losel ne 0 then begin
            for i = 0, (procval.maxt - 1) do begin
	        printf, pipe, procval.tin(i)
	        printf, pipe, procval.din(i)
            endfor
        endif
    endif
    


END
