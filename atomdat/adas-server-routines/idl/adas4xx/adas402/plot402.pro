; Copyright (c) 1996, Strathclyde University .
; SCCS INFO : Module @(#)$Header: /home/adascvs/idl/adas4xx/adas402/plot402.pro,v 1.2 2004/07/06 14:32:09 whitefor Exp $ Date $Date: 2004/07/06 14:32:09 $
;
;+
; PROJECT:
;       ADAS IBM MVS to UNIX conversion
;
; NAME:
;	PLOT402
;
; PURPOSE:
;	Plot one graph for ADAS402.
;
; EXPLANATION:
;	This routine plots ADAS402 output for a single graph.
;
; USE:
;	This routine is very similar to that for adas201.    
;
; INPUTS:
;       MFDATA  - Structure array {plot_struct}(ndplot); input dataset data:
;                 {plot_struct, NUMPOI:0 ; number of points
;                               X:fltarr ; x positions
;                               Y:fltarr ; y positions
;                               LABX:0.0 ; x position of label
;                               LABY:0.0 ; y position of label
;                               LAB:""   ; label string
;                 }
;       NDPLOT  - Int; number of structures in mfdata array - number
;                 of input dataset plots
;
;       MINMAX  - Structure {plot_struct}; minimax data to plot
;
;       SPLINE  - Structure {plot_struct}; spline data to plot
;
;	TITLE  -  String array; Graph title
;
;	XTITLE  - String; Title for x-axis
;
;	YTITLE  - String; Title for y-axis
;
;       LDEF1   - Int; whether user-specified ranges are to be used.
;
;	XMIN    - String; Lower limit for x-axis of graph, number as string.
;
;	XMAX    - String; Upper limit for x-axis of graph, number as string.
;
;	YMIN    - String; Lower limit for y-axis of graph, number as string.
;
;	YMAX    - String; Upper limit for y-axis of graph, number as string.
;
;	LFSEL	- Integer; 0 - No minimax fitting was selected 
;			   1 - Mimimax fitting was selected
;
;	LOSEL   - Integer; 0 - No interpolated values for spline fit. 
;			   1 - Intepolated values for spline fit.
;
; OPTIONAL INPUTS:
;	None.
;
; OUTPUTS:
;	None.
;
; OPTIONAL OUTPUTS:
;	None.
;
; KEYWORD PARAMETERS:
;	None.
;
; CALLS:
;	None.
;
; SIDE EFFECTS:
;	None.
;
; CATEGORY:
;	Adas system.	
;
; WRITTEN:
;       Tim Hammond, Tessella Support Services plc,  8-Sep-1995 
;
; MODIFIED:
;	1.1	William Osborn	
;		First release
;	1.2	William Osborn	
;		Removed help,xmin command
;
; VERSION:
;	1.1	30-10-96
;	1.2	08-11-96
;
;-
;----------------------------------------------------------------------------

PRO plot402, mfdata, minmax, spline, title, xtitle, ytitle, lfsel, losel, $
             ldef1, xmin, xmax, ymin, ymax, ndplot


    COMMON Global_lw_data, left, right, top, bottom, grtop, grright

                ;****************************************************
		;**** Suitable character size for current device ****
		;**** Aim for 60 characters in y direction.      ****
		;**** Also reduce slightly to fit on page        ****
		;****************************************************

    charsize = (!d.y_vsize/!d.y_ch_size)/60.0
    small_check = GETENV('VERY_SMALL')
    if small_check eq 'YES' then charsize=charsize*0.8

    xlow=1e35
    xhigh=1e-34
    ylow=1e35
    yhigh=1e-34

    if ldef1 eq 0 then begin
        for i=0,ndplot-1 do begin
            np=mfdata(i).numpoi
            if np gt 0 then begin
                xlow = xlow < min(mfdata(i).x(0:np-1))
                ylow = ylow < min(mfdata(i).y(0:np-1))
                xhigh = xhigh > max(mfdata(i).x(0:np-1))
                yhigh = yhigh > max(mfdata(i).y(0:np-1))
            endif
        endfor
        np=minmax.numpoi
        if np gt 0 then begin
            xlow = xlow < min(minmax.x(0:np-1))
            ylow = ylow < min(minmax.y(0:np-1))
            xhigh = xhigh > max(minmax.x(0:np-1))
            yhigh = yhigh > max(minmax.y(0:np-1))
        endif
        np=spline.numpoi
        if np gt 0 then begin
            xlow = xlow < min(spline.x(0:np-1))
            ylow = ylow < min(spline.y(0:np-1))
            xhigh = xhigh > max(spline.x(0:np-1))
            yhigh = yhigh > max(spline.y(0:np-1))
        endif
    endif else begin
        xlow = float(xmin)
        xhigh= float(xmax)
        ylow = float(ymin)
        yhigh= float(ymax)
    endelse

    style = 0
                                ;**** Set up log-log plotting axes ****

    plot_oo, [xlow,xhigh], [ylow,yhigh], /nodata, ticklen=1.0,	$
      position=[left, bottom, grright, grtop],		$
      xtitle=xtitle, ytitle=ytitle, xstyle=style, 		$
      ystyle=style, charsize=charsize
    

		;**********************
		;****  Make plots  ****
		;**********************

		;**********************************
		;**** Input master file plots  ****
		;**********************************

    inputplots = 0
    for i=0,ndplot-1 do begin
        np=mfdata(i).numpoi
        if np gt 0 then begin
            oplot, mfdata(i).x(0:np-1), mfdata(i).y(0:np-1), linestyle = 2
            xyouts, mfdata(i).labx, mfdata(i).laby, mfdata(i).lab,   $
              charsize=charsize
            inputplots=inputplots+1
        endif
    endfor
                ;**********************
                ;**** Minimax plot ****
                ;**********************
    
    if lfsel gt 0 then begin
        np=minmax.numpoi
        if np gt 0 then begin
            oplot, minmax.x(0:np-1), minmax.y(0:np-1), linestyle = 1
        endif
    endif
                ;*************************
                ;**** Spline fit plot ****
                ;*************************
    if losel gt 0 then begin
        np=spline.numpoi
        if np gt 0 then begin
            oplot, spline.x(0:np-1), spline.y(0:np-1), psym = -7
            xyouts, spline.labx, spline.laby, spline.lab,   $
              charsize=charsize
        endif
    endif

		;***************************************
		;**** Construct graph title         ****
		;**** "!C" is the new line control. ****
		;***************************************

    !p.font=-1
    if inputplots gt 0 then begin
        title(5) = "KEY     : (LONG DASH - INPUT DATA) "
    endif
    if losel eq 1 then begin
	if title(5) eq '' then begin
	    title(5) = "KEY     : (CROSSES/FULL LINE - SPLINE) "
	endif else begin
	    title(5) = title(5) + "(CROSSES/FULL LINE - SPLINE) "
	endelse
    endif
    if lfsel eq 1 then begin
	if title(5) eq '' then begin
	    title(5) = "KEY     : (DOTTED - MINIMAX)"
	endif else begin
	    title(5) = title(5) + "(DOTTED - MINIMAX)"
	endelse
    endif else begin
        title(4) = title(5)
  	title(5) = ' '
    endelse

    if small_check eq 'YES' then begin
        gtitle =  title(0) + "!C!C" + title(1) + "!C!C" + title(2)+"!C!C" + $
          title(3) + "!C!C" + title(4) + "!C!C" + title(5)
    endif else begin
        gtitle =  title(0) + "!C!C" + title(1) + "!C" + title(2) + "!C" + $
          title(3) + "!C" + title(4) + "!C" + title(5)
    endelse

		;**** plot title **** 

    if small_check eq 'YES' then begin
	charsize = charsize * 0.9
    endif

    if small_check eq 'YES' then begin
        xyouts, left, top+0.02, gtitle, /normal, alignment=0.0, 	$
          charsize=charsize*0.95
    endif else begin
        xyouts, left, top, gtitle, /normal, alignment=0.0, 	$
          charsize=charsize*0.95
    endelse

END
