; Copyright (c) 1995 Strathclyde University 
; SCCS info: Module @(#)$Header: /home/adascvs/idl/adas4xx/adas402/adas402.pro,v 1.8 2004/07/06 10:38:59 whitefor Exp $ Date $Date: 2004/07/06 10:38:59 $
;+
; PROJECT:
;       ADAS IBM MVS to UNIX conversion
;
; NAME:
;	ADAS402
;
; PURPOSE:
;	The highest level routine for the ADAS 402 program.
;
; EXPLANATION:
;	This routine is called from the main adas system routine, adas.pro,
;	to start the ADAS 402 application.  Associated with adas402.pro
;	is a FORTRAN executable.  The IDL code provides the user
;	interface and output graphics whilst the FORTRAN code reads
;	in data files, performs numerical processing and creates the
;	output files.  The IDL code communicates with the FORTRAN
;	executable via a bi-directional UNIX pipe.  The unit number
;	used by the IDL for writing to and reading from this pipe is
;	allocated when the FORTRAN executable is 'spawned' (see code
;	below).  Pipe communications in the FORTRAN process are to
;	stdin and stdout, i.e streams 5 and 6.
;
;	The FORTRAN code is an independent process under the UNIX system.
;	The IDL process can only exert control over the FORTRAN in the
;	data which it communicates via the pipe.  The communications
;	between the IDL and FORTRAN must be exactly matched to avoid
;	input conversion errors.  The correct ammounts of data must be
;	passed so that neither process 'hangs' waiting for communications
;	which will never occur.
;
;	The FORTRAN code performs some error checking which is
;	independent of IDL.  In cases of error the FORTRAN may write
;	error messages.  To prevent these error messages from conflicting
;	with the pipe communications all FORTRAN errors are written to
;	output stream 0, which is stderr for UNIX.  These error messages
;	will appear in the window from which the ADAS session/IDL session
;	is being run.
;
;	In the case of severe errors the FORTRAN code may terminate
;	itself prematurely.  In order to detect this, and prevent the
;	IDL program from 'hanging' or crashing, the IDL checks to see
;	if the FORTRAN executable is still an active process before
;	each group of pipe communications.  The process identifier
;	for the FORTRAN process, PID, is recorded when the process is
;	first 'spawned'.  The system is then checked for the presence
;	of the FORTRAN PID.
;
; USE:
;	First the system settings must be established by calling
;	adas_sys_set.pro then adas402.pro is called to start the
;	ADAS 402 application;
;
;	adas_sys_set, adasrel, fortdir, userroot, centroot, $
;		      devlist, devcode, font_large, font_small, edit_fonts
;	adas402,   adasrel, fortdir, userroot, centroot, devlist, $
;		   devcode, font_large, font_small, edit_fonts
;
; INPUTS:
;	ADASREL - A string indicating the ADAS system version, 
;		  e.g ' ADAS RELEASE: ADAS93 V1.4'.  The first
;		  character should be a space.
;
;	FORTDIR - A string holding the path to the directory where the
;		  FORTRAN executables are, e.g '/disk/adas/fortran/exec'
;
;	USERROOT - A string holding the path to the root directory of
;		   the user's adas data. e.g '/disk/bowen/adas'
;		   This root directory will be used by adas to construct
;		   other path names.  For example the users default data
;		   for adas205 should be in /disk/bowen/adas/adf04.  In
;		   particular the user's default interface settings will
;		   be stored in the directory USERROOT+'/defaults'.  An
;		   error will occur if the defaults directory does not
;		   exist.
;
;	CENTROOT - Like USERROOT, but this directory points to the
;		   central data area for the system.  User defaults are
;		   not stored on CENTROOT.
;
;	DEVLIST - A string array of hardcopy device names, used for
;		  graphical output. e.g ['Post-Script','HP-PCL','HP-GL']
;		  This array must mirror DEVCODE.  DEVCODE holds the
;		  actual device names used in a SET_PLOT statement.
;
;	DEVCODE - A string array of hardcopy device code names used in
;		  the SET_PLOT statement, e.g ['PS', 'PCL', 'HP']
;		  This array must mirror DEVLIST.
;
;	FONT_LARGE - The name of a larger font e.g 'courier_bold14'
;
;	FONT_SMALL - The name of a smaller font e.g 'courier_bold12'
;
;	EDIT_FONTS - A structure of two fonts used in the table editor
;		     adas_edtab.pro, {font_norm:'',font_input:''}.
;		     The two fonts are used to differentiate between
;		     editable and non-editable parts of the table. You
;		     may have to experiment to get two fonts which give
;		     the same default spacing in the edit widget.
;		     e.g {font_norm:'helvetica_bold14', $
;			  font_input:'helvetica_oblique14'}
;
; OPTIONAL INPUTS:
;	None
;
; OUTPUTS:
;	None - Note this routine should leave all inputs un-modified.
;
; OPTIONAL OUTPUTS:
;	None
;
; KEYWORD PARAMETERS:
;	None
;
; CALLS:
;	FIND_PROCESS	Checks to see if a given process is active.
;       XXDATE          Get date and time from operating system.
;       D2SPF0          Pipe comms with FORTRAN D2ISPF routine.
;       D2ISPF          Pipe comms with FORTRAN D2ISPF routine.
;       D2SPF1          Pipe comms with FORTRAN D2ISPF routine.
;	D2OUTG		Pipe comms with FORTRAN D2OUTG routine.
;
; SIDE EFFECTS:
;	This routine spawns a FORTRAN executable.  Note the pipe 
;	communications routines listed above.  In addition to these
;	pipe communications there is one explicit communication of the
;	date to the FORTRAN code, search for 'printf, pipe' to find it.
;	There are also communications of the variable gomenu to the
;	FORTRAN which is used as a signal to stop the program in its
;	tracks and return immediately to the series menu. Do the same
;	search as above to find the instances of this.
;
; CATEGORY:
;	Adas system.
;	
; WRITTEN:
;       William Osborn, Tessella Support Services plc, 28/10/96
;
; MODIFIED:
;	1.1	William Osborn
;		First release - created from adas405.pro
;	1.2	Richard Martin
;		Increased release number to 1.2
;	1.3	Richard Martin
;		Increased release number to 1.3
;	1.4	Richard Martin
;		Corrected error in version no.
;	1.5	Richard Martin
;		Increased release number to 1.4
;	1.6	Richard Martin
;		Increased release number to 1.5
;	1.7	Martin O'Mullane
;		Increased release number to 1.6.
;               From this version (Apr04 simplification) only data
;               from the selected ion stage is plotted. The user is
;               forced to select a temperature and density set and
;               the rates from the other ionisation stages are not
;               plotted.
;
; VERSION:
;         1.1   28-10-96
;	  1.2   04-04-97
;	  1.3   27-02-98
;	  1.4   11-03-98
;	  1.5   14-10-99
;	  1.6   17-03-03
;	  1.7   14-04-2004
; 
;-
;-----------------------------------------------------------------------------

PRO ADAS402,	adasrel, fortdir, userroot, centroot, 			$
		devlist, devcode, font_large, font_small, edit_fonts

		;************************
		;**** Initialisation ****
		;************************

    adasprog = ' PROGRAM: ADAS402 V1.6'
    lpend = 0
    gomenu = 0
    deffile = userroot+'/defaults/adas402_defaults.dat'
    bitfile = centroot+'/bitmaps'
    device = ''
    classes = ['ACD - Recombination coefficients',			$
               'SCD - Ionisation coefficients',				$
               'CCD - Recombination coefficients: charge exchange',	$
	       'PRB - Power coefficients: recombination-bremsstrahlung',$
	       'PRC - Power coefficients: charge-exchange recombination',$
	       'QCD - Cross-coupling coefficients',			$
	       'XCD - Cross-coupling coefficients: parent',		$
	       'PLT - Line power coefficients: total']
    dataclasses = ['ACD','SCD','CCD','PRB','PRC','QCD','XCD','PLT']
    indices = intarr(8)
    indices(0:1) = 1

		;******************************************
		;**** Search for user default settings ****
		;**** If not found create defaults     ****
		;**** inval: settings for data files   ****
		;**** scval: settings for script files ****
		;**** procval: settings for processing ****
		;**** outval: settings for output      ****
		;******************************************

    files = findfile(deffile)
    if files(0) eq deffile then begin
        restore, deffile
        inval.centroot = centroot+'/adf11/'
        inval.userroot = userroot+'/adf11/'
    endif else begin
        inval = { 							$
		  ROOTPATH:userroot+'/adf11/', 				$
		  CENTROOT:centroot+'/adf11/', 				$
		  USERROOT:userroot+'/adf11/',				$
                  FILE:'',                                              $
                  FILTR:'',                                             $
;		  BRANCH:0,						$
		  FILETYPE:0,						$
		  PARTTYPE:0,						$
		  YEAR:'',						$
		  DYEAR:'',						$
		  SYMBOL:'',						$
		  MEMBER:'',						$
                  CLASS:0, 					        $
                  PARINDX:1,                                            $
                  GRNDINDX:1                                            $
		}
  
      procval = {NEW:-1}

      outval = { out402_set, 					 $
                 GRPOUT:0,           GTIT1:'', 	         $
                 GRPSCAL:0, 			 	 $
                 XMIN:'',            XMAX:'',    	 $
                 YMIN:'',            YMAX:'',    	 $
                 HRDOUT:0,           HARDNAME:'',        $
                 GRPDEF:'',          GRPFMESS:'',        $
                 GRPSEL:-1,          GRPRMESS:'',        $
                 DEVSEL:-1,          GRSELMESS:'',       $
                 TEXOUT:0,           TEXAPP:-1, 	 $
                 TEXREP:0,           TEXDSN:'', 	 $
                 TEXDEF:'paper.txt', TEXMES:'' 		 $
        }
    end


		;****************************
		;**** Start fortran code ****
		;****************************

    spawn, fortdir+'/adas402.out', unit=pipe, /noshell, PID=pid

		;************************************************
		;**** Get date and write to fortran via pipe ****
		;************************************************

    date = xxdate()
    printf, pipe, date(0)

LABEL100:
		;**** Check FORTRAN still running ****
  
    if find_process(pid) eq 0 then goto, LABELEND

		;************************************************
		;**** Communicate with d2spf0 in fortran and ****
		;**** invoke user interface widget for       ****
		;**** Data file selection                    ****
		;************************************************

    d2spf0, pipe, inval, dataclasses, rep, dsfull, classval, yearval,	$
            fileval, celem,                                    $
	    FONT_LARGE=font_large, FONT_SMALL=font_small

		;**** If cancel selected then end program ****

    if rep eq 'YES' then goto, LABELEND


LABEL200:
		;**** Check FORTRAN still running ****

    if find_process(pid) eq 0 then goto, LABELEND

		;************************************************
		;**** Communicate with d2ispf in fortran and ****
		;**** invoke user interface widget for       ****
		;**** Processing options                     ****
		;************************************************

    d2ispf, pipe, lpend, procval, dsfull, 				$
	    classval, yearval, fileval,			                $
	    celem,                                                      $
            gomenu, bitfile, 				                $
	    FONT_LARGE=font_large, FONT_SMALL=font_small, 		$
            EDIT_FONTS=edit_fonts

		;**** If menu button clicked, tell FORTRAN to stop ****

    if gomenu eq 1 then begin
 	printf, pipe, 1
 	goto, LABELEND
    endif else begin
 	printf, pipe, 0
    endelse


		;**** If cancel selected then goto 100 ****

    if lpend eq 1 then goto, LABEL100

		;**** Fortran processing now in progress. Get ****
		;**** date and time for this run.	      ****

    date = xxdate()

		;**** Create header for output. ****

    header = adasrel+adasprog+' DATE: '+date(0)+' TIME: '+date(1)


LABEL300:
		;**** Check FORTRAN still running ****

    if find_process(pid) eq 0 then goto, LABELEND

		;************************************************
		;**** Communicate with d2spf1 in fortran and ****
		;**** invoke user interface widget for       ****
		;**** Output options                         ****
		;************************************************

    outval.hrdout = 0
    d2spf1, pipe, lpend, outval, dsfull, header, gomenu, bitfile, $
	    DEVLIST=devlist, FONT=font_large

                ;**** Extra check to see whether FORTRAN is still there ****

    if find_process(pid) eq 0 then goto, LABELEND

		;**** If menu button clicked, tell FORTRAN to stop ****

    if gomenu eq 1 then begin
	printf, pipe, 1
        outval.texmes = ''
	goto, LABELEND
    endif else begin
	printf, pipe, 0
    endelse


		;**** If cancel selected then erase output ****
		;**** messages and goto 200.		   ****

    if lpend eq 1 then begin
        outval.texmes = ''
        goto, LABEL200
    endif 

		;************************************************
                ;**** If graphical output requested          ****
		;**** Communicate with fortran and           ****
		;**** invoke widget Graphical output         ****
		;************************************************

    if outval.grpout eq 1 then begin

		;**** Hardcopy output ****

        hrdout = outval.hrdout
        hardname = outval.hardname
        if outval.devsel ge 0 then device = devcode(outval.devsel)

                ;*** user title for graph ***

        utitle = outval.gtit1
       
        if inval.filetype eq 1 and inval.parttype eq 0 then begin
            restit = '        RESOLVED DATA: IPRT= '+$
              string(inval.parindx,format='(I2)')+$
              ' IGRD= '+string(inval.grndindx,format='(I2)')
        endif else restit = ''

        d2outg, dsfull, pipe, utitle, outval.grpscal, outval.xmin,	$
          outval.xmax, outval.ymin, outval.ymax, hrdout,	$
          hardname, device, header, procval.lfsel, procval.losel, $
          gomenu, bitfile, procval.apden, restit, FONT=font_large

		;**** If menu button clicked, tell FORTRAN to stop ****

        if gomenu eq 1 then begin
            outval.texmes = ''
            outval.grpfmess = ''
            printf, pipe, 1
            goto, LABELEND
        endif else begin
            printf, pipe, 0
        endelse
    endif
	
		;**** Back for more output options ****

    GOTO, LABEL300

LABELEND:


		;**** Save user defaults ****

    save, inval, procval, outval, filename=deffile


END
