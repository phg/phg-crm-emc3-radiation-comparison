pro h2wrin, data, path=path

   if not keyword_set(path) then begin
        path='./'
   endif   
   
   if strmid(path,strlen(path)-1) ne '/' then path=path+'/'


   for i=data.ion_min,data.ion_max do begin
       get_lun,lun
       name=''
       name=path+strtrim(string(data.el_sym),2)+'_'+strtrim(string(i),2)+'_inst.dat'
       openw,lun,name
        
        ntemp=data.nte_04
        temps=data.te_04        
        

        if data.te_unit_04 eq 1 then begin
                ; eV to K
                temps=temps*11607.
        endif

        if data.te_unit_04 ne 2 then begin
                ; K to red.
                temps=temps/((data.iz0-i+1)^2.)
        endif


        if ntemp gt 0  and ntemp lt 25 then begin
                bah=dblarr(25-ntemp)
                bah[*]=temps[ntemp-1]*1.1
                temps=[temps,bah]
                ntemp=25
        endif

        ;stop

        printf,lun,'z0 '+strtrim(string(data.iz0),2)
        printf,lun,'zi '+strtrim(string(i),2)
        data.calculate_config[i,0]=1
        npars1=where(data.parity[i,where(data.calculate_config[i,*] eq 1)] eq data.parity[i,0])
        npars2=where(data.parity[i,where(data.calculate_config[i,*] eq 1)] ne data.parity[i,0])
        if npars1[0] eq -1 then npars1=0 else npars1=(size(npars1))[1]
        if npars2[0] eq -1 then npars2=0 else npars2=(size(npars2))[1]
        printf,lun,'parity-1 '+strtrim(string(npars1),2)
        printf,lun,'parity-2 '+strtrim(string(npars2),2)
        printf,lun,'E2 3'
        printf,lun,'M1 3'
        printf,lun,'scale 93 99 93 93 93'
        printf,lun,'temperature '+strtrim(string(ntemp),2)
        if ntemp gt 0 then printf,lun,temps,format='(8(1X,E11.5,2X))'



       free_lun, lun
   endfor 




end
