PRO h2wr34, data, path=path

   lvals=strarr(6)
   lvals=['s','p','d','f','g','h']

   if not keyword_set(path) then begin
        path='./'
   endif   
   
   if strmid(path,strlen(path)-1) ne '/' then path=path+'/'
   
   for i=data.ion_min,data.ion_max do begin
       get_lun,lun
       name=''
       name=path+strtrim(string(data.el_sym),2)+'_'+strtrim(string(i),2)+'_adf34.dat'
       openw,lun,name
       printf,lun,'2  -5    2   10  1.0    5.d-09    5.d-11-2  0130    1.0 0.65  0.0  0.5'
       dumstr=strtrim(data.configuration(i,0),2)
       lendumstr=string(strlen(dumstr))
       if strlen(dumstr) gt 12 then begin
          dumstr2=strtrim(strmid(data.configuration(i,0),0,9)+'  '+'0',2)
       endif else begin
          dumstr2=data.configuration(i,0)
       endelse
       printf,lun, data.iz0, i+1, data.el_sym, dumstr2,dumstr,$
		format='(3x, i2, 3x, i2, 3x, a2, 1x, a12, 5x,a'+lendumstr+')'

       for j=1, data.nconf(i)-1 do begin
          if ((data.parity(i,j) eq data.parity(i,0)) and (data.lastl(i,j) eq data.lastl(i,0))  AND DATA.CALCULATE_CONFIG[I,J] EQ 1) then begin
       		dumstr=strtrim(data.configuration(i,j),2)
       		lendumstr=string(strlen(dumstr))
       		if strlen(dumstr) gt 12 then begin
		   if j lt 10 then begin
		      dumstr2=strtrim(strmid(data.configuration(i,j),0,9)+'  '+strtrim(string(j),2),2)		
		   endif else begin
		      dumstr2=strtrim(strmid(data.configuration(i,j),0,9)+' '+strtrim(string(j),2),2)	
		   endelse
		endif else begin
		   dumstr2=data.configuration(i,j)
		endelse
		printf,lun, data.iz0, i+1, data.el_sym,dumstr2, dumstr,$
		format='(3x, i2, 3x, i2, 3x, a2, 1x, a12, 5x,a'+lendumstr+')'
		data.lastl(i,j)=' '
          endif
       endfor
       for l=0,5 do begin
          for j=1, data.nconf(i)-1 do begin
             if ((data.parity(i,j) eq data.parity(i,0)) and (data.lastl(i,j) eq lvals[l]) AND DATA.CALCULATE_CONFIG[I,J] EQ 1) then begin
       		   dumstr=strtrim(data.configuration(i,j),2)
       		   lendumstr=string(strlen(dumstr))
       		   if strlen(dumstr) gt 12 then begin
		      if j lt 10 then begin
		         dumstr2=strtrim(strmid(data.configuration(i,j),0,9)+'  '+strtrim(string(j),2),2)		
		      endif else begin
		         dumstr2=strtrim(strmid(data.configuration(i,j),0,9)+' '+strtrim(string(j),2),2)	
		      endelse
		   endif else begin
		      dumstr2=data.configuration(i,j)
		   endelse
                   printf,lun, data.iz0, i+1, data.el_sym,dumstr2 ,dumstr  ,$
		   format='(3x, i2, 3x, i2, 3x, a2, 1x, a12, 5x,a'+lendumstr+')'
		   data.lastl(i,j)=' '
             endif
          endfor
       endfor
       for l=0,5 do begin
          for j=1, data.nconf(i)-1 do begin
             if ((data.parity(i,j) ne data.parity(i,0)) and (data.lastl(i,j) eq lvals[l]) AND DATA.CALCULATE_CONFIG[I,J] EQ 1) then begin
       		   dumstr=strtrim(data.configuration(i,j),2)
       		   lendumstr=string(strlen(dumstr))
       		   if strlen(dumstr) gt 14 then begin
		      if j lt 10 then begin
		         dumstr2=strtrim(strmid(data.configuration(i,j),0,11)+'  '+strtrim(string(j),2),2)		
		      endif else begin
		         dumstr2=strtrim(strmid(data.configuration(i,j),0,11)+' '+strtrim(string(j),2),2)	
		      endelse
		   endif else begin
		      dumstr2=data.configuration(i,j)
		   endelse
                   printf,lun, data.iz0, i+1, data.el_sym,dumstr2 ,dumstr ,$
		   format='(3x, i2, 3x, i2, 3x, a2, 1x, a12, 5x,a'+lendumstr+')'
		   data.lastl(i,j)=' '
             endif
          endfor
       endfor

       printf,lun,'   -1'
       free_lun, lun
   endfor 

END
