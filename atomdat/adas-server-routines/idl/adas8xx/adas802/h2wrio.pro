PRO h2wrio,data,path=path
;-------------------------------------------------------------------------------
;routine to write driver files for CA ionisation codes
;-------------------------------------------------------------------------------
num_shls=36
shl_lab=['1s','2s','2p','3s','3p','3d','4s','4p','4d','4f','5s','5p',$
'5d','5f','5g','6s','6p','6d','6f','6g','6h','7s','7p','7d','7f','7g',$
'7h','7i','8s','8p','8d','8f','8g','8h','8i','8k']
shmax=[2,2,6,2,6,10,2,6,10,14,2,6,10,14,18,2,6,10,14,18,22,$
2,6,10,14,18,22,26,2,6,10,14,18,22,26,30]
first_array=[0,1,1,3,3,5,5,5,8,8,8,8,8,8,8,8,8,17,17,17,17,17,17,17,17,$
17,17,17,17,17,17,17,17,17,17,17]
lvalue=[0,0,1,0,1,2,0,1,2,3,0,1,2,3,4,0,1,2,3,4,5,0,1,2,3,$
4,5,6,0,1,2,3,4,5,6,7]
;-------------------------------------------------------------------------------
;Write level energy driver files
;-------------------------------------------------------------------------------
for i=data.ion_min,data.ion_max do begin
	rcg_str=path+strtrim(string(data.el_sym),2)+'_'+strtrim(string(i),2)+'_di_'
	get_lun,lun_rcg1
	get_lun,lun_rcg2
	name_rcg1=''
	name_rcg1=rcg_str+'rcg_1.dat'
	name_rcg2=''
	name_rcg2=rcg_str+'rcg_2.dat'
	
        openw,lun_rcg1,name_rcg1
	openw,lun_rcg2,name_rcg2
;	for j=0,data.grnd_indx[i]-1 do begin
                min_shl=36 											
		printf,lun_rcg1,'2  -5    2   10  1.0    5.d-09    5.d-11-2  0130    1.0 0.65  0.0  0.5'
		printf,lun_rcg2,'2  -5    2   10  1.0    5.d-09    5.d-11-2  0130    1.0 0.65  0.0  0.5'
                for l=0,data.icount_dirct[i]-1 do begin
			for k=0,num_shls-1 do begin
				if (data.ioccup_ionsn_dirct[i,l,k] ne 0) and (data.ioccup_ionsn_dirct[i,l,k] ne shmax[k]) and (first_array[k] lt min_shl) then begin
					min_shl=first_array[k]
				endif
			endfor
                endfor
                config=''
                grnd_parity=0
                for k=min_shl,num_shls-1 do begin
			if data.ioccup_ionsn_grnds[i,0,k] ne 0 then begin
				if data.ioccup_ionsn_grnds[i,0,k] lt 10 then begin
					str_occup=strtrim(string(data.ioccup_ionsn_grnds[i,0,k]),2)+' '
                               endif else begin
                                      str_occup=strtrim(string(data.ioccup_ionsn_grnds[i,0,k]),2)
                               endelse
                               config=config+' '+shl_lab[k]+str_occup
                               grnd_parity=grnd_parity+lvalue[k]*data.ioccup_ionsn_grnds[i,0,k]
                        endif
               endfor
               grnd_parity=grnd_parity mod 2
                dumstr=strtrim(config,2)
               lendumstr=string(strlen(dumstr))
               lenleft=string(42-strlen(dumstr))
               printf,lun_rcg1, data.iz0, i+1, strtrim(data.el_sym+'+'+strtrim(string(i),2),2), 'ground      ',dumstr,$
                       format='(3x, i2, 3x, i2, 1x, a5, 1x, a12,a'+string(lendumstr)+',a'+string(lenleft)+')'
               config=''
		parity_rcg2=0
                for k=min_shl,num_shls-1 do begin
                       if data.ioccup_ionsn_dirct[i,0,k] ne 0 then begin
                               if data.ioccup_ionsn_dirct[i,0,k] lt 10 then begin
                                       str_occup=strtrim(string(data.ioccup_ionsn_dirct[i,0,k]),2)+' '
                               endif else begin
                                       str_occup=strtrim(string(data.ioccup_ionsn_dirct[i,0,k]),2)
                               endelse
                               config=config+' '+shl_lab[k]+str_occup
                               parity_rcg2=parity_rcg2+lvalue[k]*data.ioccup_ionsn_dirct[i,0,k]
                       endif
               endfor
               parity_rcg2=parity_rcg2 mod 2
               dumstr=strtrim(config,2)
               lendumstr=string(strlen(dumstr))
               lenleft=string(42-strlen(dumstr))
               printf,lun_rcg2, data.iz0, i+2, strtrim(data.el_sym+'+'+strtrim(string(i+1),2),2), 'ground      ',dumstr,$
                       format='(3x, i2, 3x, i2, 1x, a5, 1x, a12,a'+string(lendumstr)+',a'+string(lenleft)+')'
		
;-------------------------------------------------------------------------------
;If parity equals ground parity, and configuration is new write to file to file rcg1
;-------------------------------------------------------------------------------
                cfg_store1=strarr(100)
                icount_cfg1=0
                cfg=''
                for k=0,num_shls-1 do begin
			if data.ioccup_ionsn_grnds[i,0,k] ne 0 then cfg=strtrim(cfg+shl_lab[k]+strtrim(string(data.ioccup_ionsn_grnds[i,0,k]),2),2)
                endfor
                cfg_store1[icount_cfg1]=cfg
                icount_cfg1=icount_cfg1+1
                for l=1,data.icount_dirct[i]-1 do begin
                	parity=0
                        cfg=''
                        for k=0,num_shls-1 do begin
                        	parity=parity+lvalue[k]*data.ioccup_ionsn_grnds[i,l,k]
				if data.ioccup_ionsn_grnds[i,l,k] ne 0 then cfg=strtrim(cfg+shl_lab[k]+strtrim(string(data.ioccup_ionsn_grnds[i,l,k]),2),2)
                        endfor
                        skip=where(cfg_store1 eq cfg)
                        skip_val=fix(skip[0])
                        parity=parity mod 2
                        if (parity eq grnd_parity) and (skip_val eq -1) then begin
        			config=''
                        	for k=min_shl,num_shls-1 do begin
					if data.ioccup_ionsn_grnds[i,l,k] ne 0 then begin
						if data.ioccup_ionsn_grnds[i,l,k] lt 10 then begin
							str_occup=strtrim(string(data.ioccup_ionsn_grnds[i,l,k]),2)+' '
						endif else begin
							str_occup=strtrim(string(data.ioccup_ionsn_grnds[i,l,k]),2)
						endelse
						config=config+' '+shl_lab[k]+str_occup
					endif
				endfor
                        	dumstr=strtrim(config,2)
				lendumstr=string(strlen(dumstr))
				lenleft=string(42-strlen(dumstr))		
				printf,lun_rcg1, data.iz0, i+1, strtrim(data.el_sym+'+'+strtrim(string(i),2),2), 'I from '+shl_lab[1]+'   ',dumstr,$
				format='(3x, i2, 3x, i2, 1x, a5, 1x, a12,a'+string(lendumstr)+',a'+string(lenleft)+')'
                                cfg_store1[icount_cfg1]=cfg
                                icount_cfg1=icount_cfg1+1
			endif
                endfor
;-------------------------------------------------------------------------------
;If parity doesn't equal ground parity, and configuration is new then write to file rcg1
;-------------------------------------------------------------------------------
                for l=1,data.icount_dirct[i]-1 do begin
                	parity=0
                        cfg=''
                        for k=0,num_shls-1 do begin
                        	parity=parity+lvalue[k]*data.ioccup_ionsn_grnds[i,l,k]
				if data.ioccup_ionsn_grnds[i,l,k] ne 0 then cfg=strtrim(cfg+shl_lab[k]+strtrim(string(data.ioccup_ionsn_grnds[i,l,k]),2),2)
                        endfor
                        skip=where(cfg_store1 eq cfg)
                        skip_val=fix(skip[0])
                        parity=parity mod 2
                        if (parity ne grnd_parity) and (skip_val eq -1) then begin
        			config=''
                        	for k=min_shl,num_shls-1 do begin
					if data.ioccup_ionsn_grnds[i,l,k] ne 0 then begin
						if data.ioccup_ionsn_grnds[i,l,k] lt 10 then begin
							str_occup=strtrim(string(data.ioccup_ionsn_grnds[i,l,k]),2)+' '
						endif else begin
							str_occup=strtrim(string(data.ioccup_ionsn_grnds[i,l,k]),2)
						endelse
						config=config+' '+shl_lab[k]+str_occup
					endif
				endfor
                        	dumstr=strtrim(config,2)
				lendumstr=string(strlen(dumstr))
				lenleft=string(42-strlen(dumstr))		
				printf,lun_rcg1, data.iz0, i+1, strtrim(data.el_sym+'+'+strtrim(string(i),2),2), 'I from '+shl_lab[1]+'   ',dumstr,$
				format='(3x, i2, 3x, i2, 1x, a5, 1x, a12,a'+string(lendumstr)+',a'+string(lenleft)+')'
                                cfg_store1[icount_cfg1]=cfg
                                icount_cfg1=icount_cfg1+1
			endif
                endfor
;-------------------------------------------------------------------------------
;If parity equals ground parity, and configuration is new write to file rcg2
;-------------------------------------------------------------------------------
               cfg_store2=strarr(100)
               icount_cfg2=0
               cfg=''
               for k=0,num_shls-1 do begin
	               if data.ioccup_ionsn_dirct[i,0,k] ne 0 then cfg=strtrim(cfg+shl_lab[k]+strtrim(string(data.ioccup_ionsn_dirct[i,0,k]),2),2)
               endfor
               cfg_store2[icount_cfg2]=cfg
               icount_cfg2=icount_cfg2+1
               for l=1,data.icount_dirct[i]-1 do begin
                	parity=0
                        cfg=''
                        for k=0,num_shls-1 do begin
                        	parity=parity+lvalue[k]*data.ioccup_ionsn_dirct[i,l,k]
				if data.ioccup_ionsn_dirct[i,l,k] ne 0 then cfg=strtrim(cfg+shl_lab[k]+strtrim(string(data.ioccup_ionsn_dirct[i,l,k]),2),2)
                        endfor
                        skip=where(cfg_store2 eq cfg)
                        skip_val=fix(skip[0])
                        parity=parity mod 2
                        if (parity eq parity_rcg2) and (skip_val eq -1) then begin
        			config=''
                        	for k=min_shl,num_shls-1 do begin
					if data.ioccup_ionsn_dirct[i,l,k] ne 0 then begin
						if data.ioccup_ionsn_dirct[i,l,k] lt 10 then begin
							str_occup=strtrim(string(data.ioccup_ionsn_dirct[i,l,k]),2)+' '
						endif else begin
							str_occup=strtrim(string(data.ioccup_ionsn_dirct[i,l,k]),2)
						endelse
						config=config+' '+shl_lab[k]+str_occup
					endif
				endfor
                        	dumstr=strtrim(config,2)
				lendumstr=string(strlen(dumstr))
				lenleft=string(42-strlen(dumstr))		
				printf,lun_rcg2, data.iz0, i+2, strtrim(data.el_sym+'+'+strtrim(string(i+1),2),2), 'I from '+shl_lab[1]+'   ',dumstr,$
				format='(3x, i2, 3x, i2, 1x, a5, 1x, a12,a'+string(lendumstr)+',a'+string(lenleft)+')'
                                cfg_store2[icount_cfg2]=cfg
                                icount_cfg2=icount_cfg2+1
			endif
                endfor
;-------------------------------------------------------------------------------
;If parity doesn't equal ground parity, and configuration is new write to file rcg2
;-------------------------------------------------------------------------------
                for l=1,data.icount_dirct[i]-1 do begin
                	parity=0
                        cfg=''
                        for k=0,num_shls-1 do begin
                        	parity=parity+lvalue[k]*data.ioccup_ionsn_dirct[i,l,k]
				if data.ioccup_ionsn_dirct[i,l,k] ne 0 then cfg=strtrim(cfg+shl_lab[k]+strtrim(string(data.ioccup_ionsn_dirct[i,l,k]),2),2)
                        endfor
                        skip=where(cfg_store2 eq cfg)
                        skip_val=fix(skip[0])
                        parity=parity mod 2
			if (parity ne parity_rcg2) and (skip_val eq -1) then begin
        			config=''
                        	for k=min_shl,num_shls-1 do begin
					if data.ioccup_ionsn_dirct[i,l,k] ne 0 then begin
						if data.ioccup_ionsn_dirct[i,l,k] lt 10 then begin
							str_occup=strtrim(string(data.ioccup_ionsn_dirct[i,l,k]),2)+' '
						endif else begin
							str_occup=strtrim(string(data.ioccup_ionsn_dirct[i,l,k]),2)
						endelse
						config=config+' '+shl_lab[k]+str_occup
					endif
				endfor
                        	dumstr=strtrim(config,2)
				lendumstr=string(strlen(dumstr))
				lenleft=string(42-strlen(dumstr))		
				printf,lun_rcg2, data.iz0, i+2, strtrim(data.el_sym+'+'+strtrim(string(i+1),2),2), 'I from '+shl_lab[1]+'   ',dumstr,$
				format='(3x, i2, 3x, i2, 1x, a5, 1x, a12,a'+string(lendumstr)+',a'+string(lenleft)+')'
                                cfg_store2[icount_cfg2]=cfg
                                icount_cfg2=icount_cfg2+1
			endif
                endfor
;        endfor
	printf,lun_rcg1,'   -1'
	printf,lun_rcg2,'   -1'
	close,lun_rcg1
	free_lun, lun_rcg1
	close,lun_rcg2
	free_lun, lun_rcg2
;endfor
;-------------------------------------------------------------------------------
;Write direct from ground ionisation driver files
;-------------------------------------------------------------------------------
;for i=data.ion_min,data.ion_max do begin
	seqid=xxseqid(data.iz0,i)
        if i lt 10 then begin
        	seqfile=path+strtrim(string(data.el_sym),2)+'_'+strtrim(string(i),2)+'_.seq'
        endif else begin
        	seqfile=path+strtrim(string(data.el_sym),2)+'_'+strtrim(string(i),2)+'.seq'
        endelse
        get_lun,lun
        openw,lun,seqfile
	printf,lun,seqid
        close,lun
        free_lun,lun
        if i lt 10 then begin
        	filenm=path+strtrim(string(data.el_sym),2)+'_'+strtrim(string(i),2)+'_.indx'
        endif else begin
        	filenm=path+strtrim(string(data.el_sym),2)+'_'+strtrim(string(i),2)+'.indx'
        endelse
        get_lun,lun_indx
	openw,lun_indx,filenm
        icount=0
	name=''
;	for j=0,data.grnd_indx[i]-1 do begin
		for l=0,data.icount_dirct[i]-1 do begin
			name=path+strtrim(string(data.el_sym),2)+'_'+strtrim(string(i),2)+'_ionsn_dirct_'+strtrim(string(icount),2)+'_'+strtrim(string(data.grnd_indx_dirct[i,l]),2)+'.dat'
;			name=dirct_grnd_str+strtrim(string(icount),2)+'.dat'
			get_lun,lun
			openw,lun,name
			if i eq (data.iz0-1) then begin
				printf,lun,'200-51 1 2  01.   1.   5.0E-08   1.0E-11-2  0130 0 1.00 0.65 1.00 0.5       0.70'
			endif else begin
				printf,lun,'200-51 1 2  01.   1.   5.0E-08   1.0E-11-2  0130 0 1.00 0.6510.00 0.5       0.70'		
			endelse
			config=''
			min_shl=0
; find shell to start writing from for ionised config			
                        flag_min_shl=0
			for k=0,num_shls-1 do begin
				if (data.ioccup_ionsn_dirct[i,l,k] ne 0) and (data.ioccup_ionsn_dirct[i,l,k] ne shmax[k]) and (flag_min_shl eq 0) then begin
					min_shl_dirct=first_array[k]
					flag_min_shl=1
				endif
			endfor
			if flag_min_shl eq 0 then begin
				for k=0,num_shls-1 do begin
					if data.ioccup_ionsn_dirct[i,l,k] eq shmax[k] then min_shl_dirct=first_array[k]
				endfor
			endif			
; find shell to start writing from for ionising config			
			flag_min_shl=0
			for k=0,num_shls-1 do begin
				if (data.ioccup_ionsn_grnds[i,l,k] ne 0) and (data.ioccup_ionsn_grnds[i,l,k] ne shmax[k]) and (flag_min_shl eq 0) then begin
					min_shl_grnds=first_array[k]
					flag_min_shl=1
				endif
			endfor
			if flag_min_shl eq 0 then begin
				for k=0,num_shls-1 do begin
					if data.ioccup_ionsn_grnds[i,l,k] eq shmax[k] then min_shl_grnds=first_array[k]
				endfor
			endif			
                        testarr=[min_shl_dirct,min_shl_grnds]
                        min_shl=min(testarr)
			config=''
                        for k=min_shl,num_shls-1 do begin
				if data.ioccup_ionsn_grnds[i,l,k] ne 0 then begin
					if data.ioccup_ionsn_grnds[i,l,k] lt 10 then begin
						str_occup=strtrim(string(data.ioccup_ionsn_grnds[i,l,k]),2)+' '
					endif else begin
						str_occup=strtrim(string(data.ioccup_ionsn_grnds[i,l,k]),2)
					endelse
					config=config+' '+shl_lab[k]+str_occup
				endif
			endfor
                        dumstr_grnd=strtrim(config,2)
			lendumstr_grnd=string(strlen(dumstr_grnd))
			lenleft_grnd=string(42-strlen(dumstr_grnd))		
			printf,lun, data.iz0, i+1, strtrim(data.el_sym+'+'+strtrim(string(i),2),2), 'ground      ',dumstr_grnd,data.dirct_lbl[i,l],$
			format='(3x, i2, 3x, i2, 1x, a5, 1x, a12,a'+string(lendumstr_grnd)+',a'+string(lenleft_grnd)+',a2)'
			cfg1=''
                        for k=0,num_shls-1 do begin
                        	if data.ioccup_ionsn_grnds[i,l,k] ne 0 then cfg1=cfg1+shl_lab[k]+strtrim(string(data.ioccup_ionsn_grnds[i,l,k]),2)
			endfor
                        for k=0,icount_cfg1-1 do begin
                        	if cfg1 eq cfg_store1[k] then indx1=k+1
                        endfor
			if i ne (data.iz0-1) then begin
                        	config=''
				for k=min_shl,num_shls-1 do begin
					if data.ioccup_ionsn_dirct[i,l,k] ne 0 then begin
						if data.ioccup_ionsn_dirct[i,l,k] lt 10 then begin
							str_occup=strtrim(string(data.ioccup_ionsn_dirct[i,l,k]),2)+' '
						endif else begin
							str_occup=strtrim(string(data.ioccup_ionsn_dirct[i,l,k]),2)
						endelse
						config=config+' '+shl_lab[k]+str_occup
					endif
				endfor
				dumstr=strtrim(config,2)
				lendumstr=string(strlen(dumstr))
				lenleft=string(42-strlen(dumstr))		
				printf,lun, data.iz0, i+2, strtrim(data.el_sym+'+'+strtrim(string(i+1),2),2), 'I from '+data.dirct_lbl[i,l]+'   ',dumstr,$
				format='(3x, i2, 3x, i2, 1x, a5, 1x, a12,a'+string(lendumstr)+',a'+string(lenleft)+')'
				cfg2=''
                        	for k=0,num_shls-1 do begin
                        		if data.ioccup_ionsn_dirct[i,l,k] ne 0 then cfg2=cfg2+shl_lab[k]+strtrim(string(data.ioccup_ionsn_dirct[i,l,k]),2)
                        	endfor
                        	for k=0,icount_cfg2-1 do begin
                        		if cfg2 eq cfg_store2[k] then indx2=k+1
                        	endfor
                        	printf,lun_indx,indx1,indx2,format='(i5,i5)'
				icount=icount+1	
                        endif else begin
                        	printf,lun_indx,indx1,format='(i5)'
				icount=icount+1	
                        endelse
			printf,lun,'   -1'
			close,lun
			free_lun, lun
		endfor
;	endfor
        close,lun_indx
        free_lun,lun_indx
;endfor
;-------------------------------------------------------------------------------
;Write excitation autoionisation driver files
;-------------------------------------------------------------------------------
;for i=data.ion_min,data.ion_max do begin
	icount=0
	name=''
;	for j=0,data.grnd_indx[i]-1 do begin
		for l=0,data.ea_indx_set[i]-1 do begin
			name=path+strtrim(string(data.el_sym),2)+'_'+strtrim(string(i),2)+'_ionsn_indirct_'+strtrim(string(icount),2)+'_'+strtrim(string(data.grnd_indx_ea[i,l]),2)+'.dat'
;			name=indirct_str+strtrim(string(icount),2)+'.dat'
			get_lun,lun
			openw,lun,name
			if i eq (data.iz0-1) then begin
				printf,lun,'20 -51 0 2   10  1.0    5.e-08    1.e-11-2   130    1.0 0.65   1.  0.5       0.7'
			endif else begin
				printf,lun,'20 -51 0 2   10  1.0    5.e-08    1.e-11-2   130    1.0 0.65  10.  0.5       0.7'
			endelse
			config=''
			min_shl=36
			for m=0,data.icount_ea[i,l]-1 do begin
				for k=0,num_shls-1 do begin
					if (data.ioccup_ionsn_ea[i,l,m,k] ne 0) and (data.ioccup_ionsn_ea[i,l,m,k] ne shmax[k]) and (first_array[k] lt min_shl) then begin
						min_shl=first_array[k]
					endif
				endfor
                        endfor
			config=''
                        for k=min_shl,num_shls-1 do begin
				if data.ioccup_ionsn_ea_grnds[i,l,k] ne 0 then begin
					if data.ioccup_ionsn_ea_grnds[i,l,k] lt 10 then begin
					str_occup=strtrim(string(data.ioccup_ionsn_ea_grnds[i,l,k]),2)+' '
					endif else begin
					str_occup=strtrim(string(data.ioccup_ionsn_ea_grnds[i,l,k]),2)
					endelse
					config=config+' '+shl_lab[k]+str_occup
				endif
			endfor
                        dumstr_grnd=strtrim(config,2)
			lendumstr_grnd=string(strlen(dumstr_grnd))
			lenleft_grnd=string(42-strlen(dumstr_grnd))		
			printf,lun, data.iz0, i+1, strtrim(data.el_sym+'+'+strtrim(string(i),2),2), 'ground      ',dumstr_grnd,data.ea_grnd_lbl[i,l],$
			format='(3x, i2, 3x, i2, 1x, a5, 1x, a12,a'+string(lendumstr_grnd)+',a'+string(lenleft_grnd)+',a2)'
			for m=0,data.icount_ea[i,l]-1 do begin
				config=''
				for k=min_shl,num_shls-1 do begin
					if data.ioccup_ionsn_ea[i,l,m,k] ne 0 then begin
						if data.ioccup_ionsn_ea[i,l,m,k] lt 10 then begin
							str_occup=strtrim(string(data.ioccup_ionsn_ea[i,l,m,k]),2)+' '
						endif else begin
							str_occup=strtrim(string(data.ioccup_ionsn_ea[i,l,m,k]),2)
						endelse
						config=config+' '+shl_lab[k]+str_occup
					endif
				endfor
				dumstr=strtrim(config,2)
				lendumstr=string(strlen(dumstr))
				lenleft=string(42-strlen(dumstr))		
				printf,lun, data.iz0, i+1, strtrim(data.el_sym+'+'+strtrim(string(i),2),2), 'I via  '+data.ea_ex_lbl[i,l,m]+'   ',dumstr,data.ea_ex_lbl[i,l,m],$
					format='(3x, i2, 3x, i2, 1x, a5, 1x, a12,a'+string(lendumstr)+',a'+string(lenleft)+',a2)'
			endfor
			printf,lun,'   -1'
			close,lun
			free_lun, lun
			icount=icount+1	
		endfor
;	endfor
endfor
END
