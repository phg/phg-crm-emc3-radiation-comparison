; Copyright (c) 1999, Strathclyde University.
; SCCS info: Module @(#)adas216_proc.pro	1.1 Date 04/21/99
;+
; PROJECT:
;       ADAS 
;
; NAME:
;	ADAS216_PROC
;
; PURPOSE:
;	IDL ADAS user interface, processing options/input.
;
; EXPLANATION:
;	This routine creates and manages a pop-up window which allows
;	the user to select options and input data to control ADAS216
;	processing.
;
; USE:
;	This routine is ADAS216 specific, see bgispf.pro for how it
;	is used.
;
; INPUTS:
;	VAL	- A structure which determines the initial settings of
;		  the processing options widget.  The value is passed
;		  unmodified into cw_adas216_proc.pro.
;
;		  See cw_adas216_proc.pro for a full description of this
;		  structure.
;
;       BITFILE - String; the path to the directory containing bitmaps
;                 for the 'escape to series menu' button.
;
; OPTIONAL INPUTS:
;	None
;
; OUTPUTS:
;	VAL	- On output the structure records the final settings of
;		  the processing selections widget if the user pressed the
;		  'Done' button, otherwise it is not changed from input.
;
;	ACT	- String; Either 'Done' or 'Cancel' for the button the
;		  user pressed to terminate the processing options
;		  window.
;
; OPTIONAL OUTPUTS:
;	None
;
; KEYWORD PARAMETERS:
;	FONT_LARGE - The name of a larger font.
;
;	FONT_SMALL - The name of a smaller font.
;
;	EDIT_FONTS - A structure of two fonts used in the table editor
;		     adas_edtab.pro, {font_norm:'',font_input:''}.
;
; CALLS:
;	CW_ADAS216_PROC	Declares the processing options widget.
;	DYNLABEL	Sets dynamic_resize keyword for label widgets.
;	ADAS216_PROC_EV Called indirectly during widget management,
;			routine included in this file.
;	XMAMAGER
;
; SIDE EFFECTS:
;	This routine uses a common block PROC216_BLK in the management
;	of the pop-up window.
;
; CATEGORY:
;	Widgets
;
; WRITTEN:
;       Martin O'Mullane
;
; MODIFIED:
;	1.1	Martin O'Mullane
;		First release 
;
; VERSION:
;       1.1	17-03-99
;
;-
;-----------------------------------------------------------------------------


PRO adas802_proc_ev, event

  COMMON proc802_blk,action,value

		;**** Find the event type and copy to common ****
                
    action = event.action

    CASE action OF

		;**** 'Done' button   ****
	'Done'  : begin

			;**** Get the output widget value ****
		widget_control,event.id,get_value=value 
		widget_control,event.top,/destroy        
        
        
           end


		;**** 'Cancel' button ****
	'Cancel': widget_control,event.top,/destroy

                ;**** 'Menu' button   ****

        'Menu'  : widget_control, event.top, /destroy

        ELSE:   ;**** do nothing ****

    END

END

;-----------------------------------------------------------------------------


PRO adas802_proc, val, data, bitfile, passdir, fortdir, $
		act, OPTIONS, selem, 	$
                fileerr, reporterr,					$
		FONT_LARGE=font_large, FONT_SMALL=font_small, 		$
		EDIT_FONTS=edit_fonts,cadwion_on=cadwion_on

  COMMON proc802_blk,action,value

		;**** Copy value to common ****
  value = val

		;**** Set defaults for keywords ****
                
  IF NOT (KEYWORD_SET(font_large)) THEN font_large = ''
  IF NOT (KEYWORD_SET(font_small)) THEN font_small = ''
  IF NOT (KEYWORD_SET(edit_fonts)) THEN $
				edit_fonts = { font_norm:'', font_input:''}

                ;**** create base widget ****
                
  procid = widget_base(TITLE='ADAS802 PROCESSING OPTIONS', $
					XOFFSET=50,YOFFSET=0)

		;**** Declare processing widget ****
                
  cwid = cw_adas802_proc(procid, 	$
			bitfile, passdir, fortdir, $
			VALUE=value, 					$
			OPTIONS=OPTIONS, $
                        FONT_LARGE=font_large, FONT_SMALL=font_small, 	$
			EDIT_FONTS=edit_fonts,cadwion_on=cadwion_on)

		;**** Realize the new widget ****
                
  dynlabel, procid
  widget_control,procid,/realize

		;**** make widget modal ****
                
  xmanager,'adas802_proc',procid,event_handler='adas802_proc_ev', $
					/modal,/just_reg
 
		;**** Return the output value from common ****
  act = action
  val = value

END

