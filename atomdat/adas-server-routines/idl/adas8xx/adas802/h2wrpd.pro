pro h2wrpd, data, path=path

   if not keyword_set(path) then begin
        path='./'
   endif   
   
   if strmid(path,strlen(path)-1) ne '/' then path=path+'/'


   for i=data.ion_min,data.ion_max do begin
       get_lun,lun
       name=''
       name=path+strtrim(string(data.el_sym),2)+'_'+strtrim(string(i),2)+'_pec_drv.dat'
       openw,lun,name
       
       ionpotcm=data.ionpot[i]*8061.
       
       printf,lun,data.el_sym,'+',strtrim(string(i),2),data.iz0,i+1,ionpotcm
       printf,lun
       printf,lun,strtrim(string('./'+strtrim(string(data.el_sym),2)+'_'+strtrim(string(i),2)+'_adf04.dat'),2)
       printf,lun
       printf,lun,strtrim(string('./'+strtrim(string(data.el_sym),2)+'_'+strtrim(string(i),2)+'_adf15.dat'),2)
       printf,lun
       printf,lun,strtrim(string('./'+strtrim(string(data.el_sym),2)+'_'+strtrim(string(i),2)+'_adf40.dat'),2)
       printf,lun

       printf,lun,'lmetr = false'               ;true to resolve metastables
       printf,lun,'ltscl = true'                ;true for z-scaled temperatures
       printf,lun,'lmetr = false'               ;true for z-scaled densities
       printf,lun,'lmetr = true'                ;true for ion temp. broadening

       printf,lun

       ntemp=data.nte_15
       temps=data.te_15        

       if data.te_unit_15 eq 1 then begin
               ; eV to K
               temps=temps*11607.
       endif

       if data.te_unit_15 ne 2 then begin
               ; K to red.
               temps=temps/((i+1)^2.)
       endif
              
       ;printf,lun,'temperature '+strtrim(string(ntemp),2)
       printf,lun,ntemp,format='(I5)'
       if ntemp gt 0 then printf,lun,temps,format='(4(1X,E9.3))'
       
        printf,lun

       
       ndense=5
       dense=[1.000e+11, 1.000e+12, 1.000e+13, 1.000e+14, 1.000e+15]
       
       
       printf,lun,ndense,format='(I5)'
       if ndense gt 0 then printf,lun,dense,format='(4(1X,E9.3))'

        printf,lun
         
       
       ; do wavelength grids now
       ;printf,lun,'grids '+strtrim(string(data.nints),2)
       printf,lun,data.nints,format='(I5)'
       for j=0,data.nints-1 do begin
                printf,lun,128,data.lmin[j],data.lmax[j],format='(i5,2e10.3)'
       end       
       
       free_lun, lun
   endfor 




end
