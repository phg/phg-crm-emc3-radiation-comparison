PRO H2IONE, ionsn_rules, state, str_confg, ion_ch
;-------------------------------------------------------------------------------
;routine to generate configurations for CADW ionisation codes
;-------------------------------------------------------------------------------

num_shls=36
shl_lab=['1s','2s','2p','3s','3p','3d','4s','4p','4d','4f','5s','5p',$
'5d','5f','5g','6s','6p','6d','6f','6g','6h','7s','7p','7d','7f','7g',$
'7h','7i','8s','8p','8d','8f','8g','8h','8i','8k']
shmax=[2,2,6,2,6,10,2,6,10,14,2,6,10,14,18,2,6,10,14,18,22,$
2,6,10,14,18,22,26,2,6,10,14,18,22,26,30]
noble_core=[0,0,0,3,3,5,5,5,8,8,8,11,11,11,11,11,11,11,11,11,$
11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11]
lvalue=[0,0,1,0,1,2,0,1,2,3,0,1,2,3,4,0,1,2,3,4,5,0,1,2,3,$
4,5,6,0,1,2,3,4,5,6,7]
lstr=['s','p','d','f','g','h','i','k']

ionsn_occup=intarr(num_shls)

;-------------------------------------------------------------------------------
; evaluate indices for ionisation rules taken from processing widget
;-------------------------------------------------------------------------------
for i=0,35 do begin
	if strtrim(ionsn_rules.ion_stage_drct_mnshl,2) eq shl_lab[i] then drct_mnshl = i
	if strtrim(ionsn_rules.ion_stage_ind_mnshl,2) eq shl_lab[i] then ind_mnshl = i
	if strtrim(ionsn_rules.ion_stage_ind_mxshl,2) eq shl_lab[i] then ind_mxshl = i
	if strtrim(ionsn_rules.ion_stage_pe_vac,2) eq shl_lab[i] then pe_vac = i
	if strtrim(ionsn_rules.ion_stage_pe_mnshl,2) eq shl_lab[i] then pe_mnshl = i
	if strtrim(ionsn_rules.ion_stage_pe_mxshl,2) eq shl_lab[i] then pe_mxshl = i
endfor
for i=0,7 do begin
	if strtrim(ionsn_rules.ion_stage_ind_mxl,2) eq lstr[i] then ind_lmax=i
	if strtrim(ionsn_rules.ion_stage_pe_mxl,2) eq lstr[i] then pe_lmax=i
endfor

;   for i=ionsn_rules.ion_stage_min,ionsn_rules.ion_stage_max do begin
;-------------------------------------------------------------------------------
; convert ground configuration to occupation array
;-------------------------------------------------------------------------------
	if state.nconf[ion_ch] eq 0 then begin
        	icount_configs=0
        endif else begin 
        	icount_configs=state.nconf[ion_ch]-1
        endelse
        if state.nconf[ion_ch+1] eq 0 then begin
        	icount_configs_p1=0
        endif else begin
        	icount_configs_p1=state.nconf[ion_ch+1]-1
        endelse
	for l=0,num_shls-1 do ionsn_occup[l]=0
	iflag=0
	confg=' '+strtrim(str_confg,2)
	adjust=0
	if fix(strlen(confg)) mod 5 ne 0 then adjust=1
	len_confg=fix(fix(strlen(confg))/5)+adjust
	first_shl=strtrim(strmid(confg,0,3),2)
	for j=0,num_shls-1 do begin
		if first_shl eq shl_lab[j] then indx=j
	endfor
	for j=0,indx-1 do begin
		ionsn_occup[j]=shmax[j]
	endfor
	for j=0,len_confg-1 do begin
		shl=strmid(confg,5*j,3)
		for k=0,num_shls-1 do begin
			if strtrim(string(shl),2) eq shl_lab[k] then indx=k
		endfor
		ionsn_occup[indx]=fix(strtrim(strmid(confg,5*j+3,2),2))
	endfor
	for j=0,num_shls-1  do begin
		if ionsn_occup[j] ne 0 then max_shl=j
	endfor
;	config_grnd=''
;	for k=drct_mnshl,max_shl do begin
;		if ionsn_occup[k] ne 0 then begin
;			if ionsn_occup[k] lt 10 then begin
;				str_occup=strtrim(string(ionsn_occup[k]),2)+' '
;			endif else begin
;				str_occup=strtrim(string(ionsn_occup[k]),2)
;			endelse
;			config_grnd=config_grnd+' '+shl_lab[k]+str_occup
;		endif
;	endfor

;-------------------------------------------------------------------------------
; evaluate possible direct ionisation routes
;-------------------------------------------------------------------------------
	for j=max_shl, drct_mnshl,-1  do begin
		if ionsn_occup[j]-1 ge 0 then begin
			for k=0,num_shls-1 do begin
				state.ioccup_ionsn_grnds[ion_ch,state.grnd_indx[ion_ch], state.icount_dirct[ion_ch,state.grnd_indx[ion_ch]],k]=ionsn_occup[k]
			endfor
                        ionsn_occup[j]=ionsn_occup[j]-1
			parity=0
			for k=0,num_shls-1 do begin
				state.ioccup_ionsn_dirct[ion_ch, state.grnd_indx[ion_ch], state.icount_dirct[ion_ch,state.grnd_indx[ion_ch]],k]=ionsn_occup[k]
				parity=parity+lvalue[k]*ionsn_occup[k]
			endfor
			parity=parity mod 2
			state.dirct_parity[ion_ch,state.grnd_indx[ion_ch],state.icount_dirct[ion_ch,state.grnd_indx[ion_ch]]]=parity
			state.dirct_lbl[ion_ch,state.grnd_indx[ion_ch], state.icount_dirct[ion_ch,state.grnd_indx[ion_ch]]]=shl_lab[j]
			state.icount_dirct[ion_ch,state.grnd_indx[ion_ch]]=state.icount_dirct[ion_ch,state.grnd_indx[ion_ch]]+1
;-------------------------------------------------------------------------------
; if new ionised configuration add to config list in state structure
;-------------------------------------------------------------------------------
                        imatch=99
			for ii=0,icount_configs_p1 do begin
				new_conf=0
				for iii=21,num_shls-1 do begin
					if ionsn_occup[iii] gt 0 then begin
						imatch=ii
					endif
				endfor
				for iii=0,20 do begin
					if ionsn_occup[iii] eq state.ioccup_store[ion_ch+1,ii,iii] then begin
						new_conf=new_conf+1
					endif
				endfor
				if new_conf eq 21 then imatch=ii
			endfor
			if (imatch eq 99) or (icount_configs_p1 eq 0) then begin
				iflag=0
				for jj=0,num_shls-1 do begin
					if (ionsn_occup[jj] ne shmax[jj]) and (iflag eq 0) then begin
						ifirst_shl=jj
						iflag=1
					endif
				endfor
				config=''
				for k=ifirst_shl,max_shl do begin
					if ionsn_occup[k] ne 0 then begin
						if ionsn_occup[k] lt 10 then begin
							str_occup=strtrim(string(ionsn_occup[k]),2)+' '
						endif else begin
							str_occup=strtrim(string(ionsn_occup[k]),2)
						endelse
						config=config+' '+shl_lab[k]+str_occup
					endif
				endfor
				icount_configs_p1=icount_configs_p1+1
				state.prom_config[ion_ch+1,icount_configs_p1]=config
				len=strlen(strtrim(state.prom_config[ion_ch+1,icount_configs_p1],2))
				state.prom_lastl[ion_ch+1,icount_configs_p1]=strmid(strtrim(state.prom_config[ion_ch+1,icount_configs_p1],2),len-2,1)
				parity=0
				for iii=0,20 do begin
					state.ioccup_store[ion_ch+1,icount_configs_p1,iii]=ionsn_occup[iii]
					parity=parity+lvalue[iii]*ionsn_occup[iii]
				endfor
				parity=parity mod 2
				state.prom_pars[ion_ch+1,icount_configs_p1]=parity
				state.prom_nlev[ion_ch+1,icount_configs_p1]=h2nlev(ionsn_occup)
				state.nconf[ion_ch+1]=state.nconf[ion_ch+1]+1
			endif
			ionsn_occup[j]=ionsn_occup[j]+1
		endif
	endfor	
;-------------------------------------------------------------------------------
; evaluate parent+excited electron ionisation routes
;-------------------------------------------------------------------------------
;	for j=max_shl, pe_mnshl,-1  do begin
	j=pe_vac
		if ionsn_occup[j]-1 ge 0 then begin
;			for k=0,num_shls-1 do begin
;				state.ioccup_ionsn_grnds[ion_ch,state.grnd_indx[ion_ch], state.icount_dirct[ion_ch,state.grnd_indx[ion_ch]],k]=ionsn_occup[k]
;			endfor
                        ionsn_occup[j]=ionsn_occup[j]-1
			for k=j+1,pe_mxshl do begin
				if lvalue[k] le pe_lmax then begin
					ionsn_occup[k]=ionsn_occup[k]+1
                                        for kk=pe_mnshl,max_shl do begin
						for l=0,num_shls-1 do begin
							state.ioccup_ionsn_grnds[ion_ch,state.grnd_indx[ion_ch],state.icount_dirct[ion_ch,state.grnd_indx[ion_ch]],l]=ionsn_occup[l]
						endfor
						ionsn_occup[kk]=ionsn_occup[kk]-1
						for l=0,num_shls-1 do begin
							state.ioccup_ionsn_dirct[ion_ch,state.grnd_indx[ion_ch],state.icount_dirct[ion_ch,state.grnd_indx[ion_ch]],l]=ionsn_occup[l]
							parity=parity+lvalue[l]*ionsn_occup[l]
						endfor
;						state.ioccup_ionsn_dirct[ion_ch,state.grnd_indx[ion_ch],state.icount_dirct[ion_ch,state.grnd_indx[ion_ch]],kk]=ionsn_occup[kk]-1
						parity=parity mod 2
						state.dirct_parity[ion_ch,state.grnd_indx[ion_ch],state.icount_dirct[ion_ch,state.grnd_indx[ion_ch]]]=parity
						state.dirct_lbl[ion_ch,state.grnd_indx[ion_ch], state.icount_dirct[ion_ch,state.grnd_indx[ion_ch]]]=shl_lab[k]
						state.icount_dirct[ion_ch,state.grnd_indx[ion_ch]]=state.icount_dirct[ion_ch,state.grnd_indx[ion_ch]]+1
						parity=0
;-------------------------------------------------------------------------------
; if new ionised configuration add to config list in state structure
;-------------------------------------------------------------------------------
						imatch=99
						for ii=0,icount_configs_p1 do begin
							new_conf=0
							for iii=21,num_shls-1 do begin
								if ionsn_occup[iii] gt 0 then begin
									imatch=ii
								endif
							endfor
							for iii=0,20 do begin
								if ionsn_occup[iii] eq state.ioccup_store[ion_ch+1,ii,iii] then begin
									new_conf=new_conf+1
								endif
							endfor
							if new_conf eq 21 then imatch=ii
						endfor
						if (imatch eq 99) or (icount_configs_p1 eq 0) then begin
							iflag=0
							for jj=0,num_shls-1 do begin
								if (ionsn_occup[jj] ne shmax[jj]) and (iflag eq 0) then begin
									ifirst_shl=jj
									iflag=1
								endif
							endfor
							config=''
							for l=ifirst_shl,k do begin
								if ionsn_occup[l] ne 0 then begin
									if ionsn_occup[l] lt 10 then begin
										str_occup=strtrim(string(ionsn_occup[l]),2)+' '
									endif else begin
										str_occup=strtrim(string(ionsn_occup[l]),2)
									endelse
									config=config+' '+shl_lab[l]+str_occup
								endif
							endfor
							icount_configs_p1=icount_configs_p1+1
							state.prom_config[ion_ch+1,icount_configs_p1]=config
							len=strlen(strtrim(state.prom_config[ion_ch+1,icount_configs_p1],2))
							state.prom_lastl[ion_ch+1,icount_configs_p1]=strmid(strtrim(state.prom_config[ion_ch+1,icount_configs_p1],2),len-2,1)
							parity=0
							for iii=0,20 do begin
								parity=parity+lvalue[iii]*ionsn_occup[iii]
								state.ioccup_store[ion_ch+1,icount_configs_p1,iii]=ionsn_occup[iii]
							endfor
							parity=parity mod 2
							state.prom_pars[ion_ch+1,icount_configs_p1]=parity
							state.prom_nlev[ion_ch+1,icount_configs_p1]=h2nlev(ionsn_occup)
							state.nconf[ion_ch+1]=state.nconf[ion_ch+1]+1
						endif
						ionsn_occup[kk]=ionsn_occup[kk]+1
					endfor
;-------------------------------------------------------------------------------
; if new ground configuration add to config list in state structure
;-------------------------------------------------------------------------------
					imatch=99
					for ii=0,icount_configs do begin
						new_conf=0
						for iii=21,num_shls-1 do begin
							if ionsn_occup[iii] gt 0 then begin
								imatch=ii
							endif
						endfor
						for iii=0,20 do begin
							if ionsn_occup[iii] eq state.ioccup_store[ion_ch,ii,iii] then begin
								new_conf=new_conf+1
							endif
						endfor
						if new_conf eq 21 then imatch=ii
					endfor
					if (imatch eq 99) or (icount_configs eq 0) then begin
						iflag=0
						for jj=0,num_shls-1 do begin
							if (ionsn_occup[jj] ne shmax[jj]) and (iflag eq 0) then begin
								ifirst_shl=jj
								iflag=1
							endif
						endfor
						config=''
						for l=ifirst_shl,k do begin
							if ionsn_occup[l] ne 0 then begin
								if ionsn_occup[l] lt 10 then begin
									str_occup=strtrim(string(ionsn_occup[l]),2)+' '
								endif else begin
									str_occup=strtrim(string(ionsn_occup[l]),2)
								endelse
								config=config+' '+shl_lab[l]+str_occup
							endif
						endfor
						icount_configs=icount_configs+1
						state.prom_config[ion_ch,icount_configs]=config
						len=strlen(strtrim(state.prom_config[ion_ch,icount_configs],2))
						state.prom_lastl[ion_ch,icount_configs]=strmid(strtrim(state.prom_config[ion_ch,icount_configs],2),len-2,1)
						parity=0
						for iii=0,20 do begin
							parity=parity+lvalue[iii]*ionsn_occup[iii]
							state.ioccup_store[ion_ch,icount_configs,iii]=ionsn_occup[iii]
						endfor
						parity=parity mod 2
						state.prom_pars[ion_ch,icount_configs]=parity
						state.prom_nlev[ion_ch,icount_configs]=h2nlev(ionsn_occup)
						state.nconf[ion_ch]=state.nconf[ion_ch]+1
					endif
;-------------------------------------------------------------------------------
;-------------------------------------------------------------------------------
					ionsn_occup[k]=ionsn_occup[k]-1
				endif
			endfor
			ionsn_occup[j]=ionsn_occup[j]+1
		endif
;	endfor	

;-------------------------------------------------------------------------------
; evaluate indirect ionisation routes
;-------------------------------------------------------------------------------
	state.ea_indx_set[ion_ch,state.grnd_indx[ion_ch]]=0
	for j=ind_mnshl,max_shl-1 do begin
                state.icount_ea[ion_ch,state.grnd_indx[ion_ch], state.ea_indx_set[ion_ch,state.grnd_indx[ion_ch]]]=0
		if (ionsn_occup[j] -1) ge 0 then  begin
		for k=0,num_shls-1 do begin
			state.ioccup_ionsn_ea_grnds[ion_ch,state.grnd_indx[ion_ch],k]=ionsn_occup[k]
		endfor
		ionsn_occup[j]=ionsn_occup[j]-1
		state.ea_grnd_lbl[ion_ch,state.grnd_indx[ion_ch],state.ea_indx_set[ion_ch,state.grnd_indx[ion_ch]]]=shl_lab[j]
                for k=j+1,ind_mxshl do begin
			if lvalue[k] le ind_lmax then begin
				if ionsn_occup[k] le shmax[k]-1 then begin
					ionsn_occup[k]=ionsn_occup[k]+1
;					config=''
;					for l=ind_mnshl,num_shls-1 do begin
;						if ionsn_occup[l] ne 0 then begin
;							if ionsn_occup[l] lt 10 then begin
;								str_occup=strtrim(string(ionsn_occup[l]),2)+' '
;							endif else begin
;								str_occup=strtrim(string(ionsn_occup[l]),2)
;							endelse
;							config=config+' '+shl_lab[l]+str_occup
;						endif
;					endfor
                                        for l=0,num_shls-1 do begin
						state.ioccup_ionsn_ea[ion_ch,state.grnd_indx[ion_ch],state.ea_indx_set[ion_ch,state.grnd_indx[ion_ch]],state.icount_ea[ion_ch,state.grnd_indx[ion_ch],state.ea_indx_set[ion_ch,state.grnd_indx[ion_ch]]],l]=ionsn_occup[l]
					endfor
                                        state.ea_ex_lbl[ion_ch,state.grnd_indx[ion_ch],state.ea_indx_set[ion_ch,state.grnd_indx[ion_ch]],state.icount_ea[ion_ch,state.grnd_indx[ion_ch],state.ea_indx_set[ion_ch,state.grnd_indx[ion_ch]]]]=shl_lab[k]
					state.icount_ea[ion_ch,state.grnd_indx[ion_ch],state.ea_indx_set[ion_ch,state.grnd_indx[ion_ch]]]=state.icount_ea[ion_ch,state.grnd_indx[ion_ch],state.ea_indx_set[ion_ch,state.grnd_indx[ion_ch]]]+1
					ionsn_occup[k]=ionsn_occup[k]-1
;					icount_ea=icount_ea+1
				endif
			endif
		endfor
		state.ea_indx_set[ion_ch,state.grnd_indx[ion_ch]]=state.ea_indx_set[ion_ch,state.grnd_indx[ion_ch]]+1
		ionsn_occup[j]=ionsn_occup[j]+1
		endif 		
	endfor
	state.grnd_indx[ion_ch]=state.grnd_indx[ion_ch]+1
	
;   endfor 
END
