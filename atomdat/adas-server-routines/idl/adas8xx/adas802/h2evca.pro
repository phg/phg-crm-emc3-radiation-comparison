PRO h2evca, STATE, ION_CH, ATOMDATA,bessel=bessel

	; do all electric mupole and bessel function thingy
	if not keyword_set(bessel) then bessel=0

       lvals=strarr(6)
       lvals=['s','p','d','f','g','h']
	shmax=[2,2,6,2,6,10,2,6,10,14,2,6,10,14,18,2,6,10,14,18,22,$
	2,6,10,14,18,22,26,2,6,10,14,18,22,26,30]
	shl_lval=[0,0,1,0,1,2,0,1,2,3,0,1,2,3,4,0,1,2,3,4,5,$
	0,1,2,3,4,5,6,0,1,2,3,4,5,6,7]
	shl_lab=['1s','2s','2p','3s','3p','3d','4s','4p','4d','4f','5s','5p',$
	'5d','5f','5g','6s','6p','6d','6f','6g','6h','7s','7p','7d','7f','7g',$
	'7h','7i','8s','8p','8d','8f','8g','8h','8i','8k']
	noble_core=[0,1,1,3,3,5,5,5,8,8,8,11,11,11,11,11,11,11,11,11,$
	11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11]
       ind_val=0
       num_shls=36
       str_conf_label=strarr(state.nconf[ion_ch])
       name='in36'
       openw,lun,name, /get_lun
       printf,lun,'2  -5    2   10  1.0    5.d-09    5.d-11-2  0130    1.0 0.65  0.0  0.5'
       config_list=strarr(state.nconf[ion_ch])
       
       min_shl=35
       for i=0,state.nconf[ion_ch]-1 do begin
	for k=0,num_shls-1 do begin
       		if (state.ioccup_store[ion_ch,i,k] gt 0) and (state.ioccup_store[ion_ch,i,k] lt shmax[k]) then begin
			if k lt min_shl then min_shl=k
		endif
       	endfor
       endfor
       min_shl=noble_core[min_shl]
       for i=0,state.nconf[ion_ch]-1 do begin
	config=''
	for k=min_shl,num_shls-1 do begin
		if state.ioccup_store[ion_ch,i,k] ne 0 then begin
			if state.ioccup_store[ion_ch,i,k] lt 10 then begin
				str_occup=strtrim(string(state.ioccup_store[ion_ch,i,k]),2)+' '
			endif else begin
				str_occup=strtrim(string(state.ioccup_store[ion_ch,i,k]),2)
			endelse
			config=config+' '+shl_lab[k]+str_occup
		endif
	endfor
	config_list[i]=config
       endfor
       
       dumstr=strtrim(config_list[0],2)
       lendumstr=string(strlen(dumstr))
       prom_lastl=state.prom_lastl
       if strlen(dumstr) gt 14 then begin
          dumstr2=' xx'+'0'+'          '
       endif else begin
          dumstr2=strtrim(config_list[0],2)
       endelse
       
       dumstr2='grnd'
       
       lendumstr2=string(strlen(dumstr2))
       lendumstr2_left=string(14-strlen(dumstr2))
       if lendumstr2_left ne 0 then begin
       		printf,lun, state.iz0, ion_ch+1, state.el_sym, dumstr2,dumstr,$
		format='(3x, i2, 3x, i2, 1x, a2, 1x, a'+lendumstr2+','+lendumstr2_left+'x, 4x,a'+lendumstr+')'
	endif else begin
		printf,lun, state.iz0, ion_ch+1, state.el_sym, dumstr2,dumstr,$
		format='(3x, i2, 3x, i2, 1x, a2, 1x, a'+lendumstr2+', 4x,a'+lendumstr+')'
	endelse
	str_conf_label(ind_val)=dumstr2
	ind_val=ind_val+1
       
       cross=intarr(state.nconf[ion_ch])
       cross2=intarr(state.nconf[ion_ch])
       k=1
       for parity=0,1 do begin
       
       
       
       
       for j=1, state.nconf[ion_ch]-1 do begin
       
       		if (parity eq 0 and state.prom_pars[ion_ch,j] eq state.igrndpar[ion_ch]) $
       		or (parity eq 1 and state.prom_pars[ion_ch,j] ne state.igrndpar[ion_ch]) then begin
       
                print,j,k
		CROSS[k]=j
		CROSS2[j]=k
		k=k+1

       		dumstr=strtrim(config_list[j],2)
       		lendumstr=string(strlen(dumstr))
       		if strlen(dumstr) gt 14 then begin
		   if j lt 10 then begin
		      dumstr2=' xx'+strtrim(string(j),2)+'          '	
		   endif else begin
		      dumstr2=' x'+strtrim(string(j),2)+'          '
		   endelse
		endif else begin
		   dumstr2=strtrim(config_list[j],2)
		endelse
		
		dumstr2='cfg'+strtrim(string(j),2)
		
       		lendumstr2=string(strlen(dumstr2))
       		lendumstr2_left=string(14-strlen(dumstr2))
		if lendumstr2_left ne 0 then begin
			printf,lun, state.iz0, ion_ch+1, state.el_sym,dumstr2, dumstr,$
			format='(3x, i2, 3x, i2, 1x, a2, 1x, a'+lendumstr2+','+lendumstr2_left+'x, 4x,a'+lendumstr+')'
		endif else begin
			printf,lun, state.iz0, ion_ch+1, state.el_sym,dumstr2, dumstr,$
			format='(3x, i2, 3x, i2, 1x, a2, 1x, a'+lendumstr2+', 4x,a'+lendumstr+')'
		endelse
	str_conf_label(ind_val)=dumstr2
	ind_val=ind_val+1
	endif
       endfor
	endfor

;  evaluate configuration averaged energies
       printf,lun,'   -1'
       free_lun, lun
       spawn,state.fortdir+'/rcn.x'
;       spawn,'rm in36'
;       spawn,'rm out36'
       inx=intarr(state.nconf[ion_ch])
       eav=dblarr(state.nconf[ion_ch])
       str_conf=strarr(state.nconf[ion_ch])
       dums=' '
       openr,lunfort,'fort.91',/get_lun
       for i=0,state.nconf[ion_ch]-1 do begin
           readf,lunfort,dumi,dumr, dums
	   inx[i]=fix(dumi)
	   eav[i]=double(dumr)
	   str_conf[i]=string(dums)
       endfor
       free_lun,lunfort

	ei=dblarr(100,36)

	openr,lunfort,'fort.94',/get_lun
	for i=0,state.nconf[ion_ch]-1 do begin
		readf,lunfort,dumi, format='(i3)'
		max=fix(dumi)
		for m=0,max-1 do begin
			readf,lunfort,dumr, format='(f12.5)'
			ei(i,m)=double(dumr)
		endfor
	endfor
	close,lunfort
	free_lun,lunfort
	
	
        stat_wt=lonarr(state.nconf(ion_ch))

	for j=0,state.nconf(ion_ch)-1 do begin
		stat_wt[cross2[j]]=1
		for k=0,num_shls-1 do begin
			stat_wt[cross2[j]]=stat_wt[cross2[j]]*factorial(4*shl_lval[k]+2)/$
				factorial(state.ioccup_store[ion_ch,j,k])/$
				factorial(4*shl_lval[k]+2-state.ioccup_store[ion_ch,j,k])
		endfor
	end

	
	
;	stop
;  evaluate wavelengths of all possible parity changing transitions

   
        procval = { options_set :   'NO',                 $
                    configs_set :   'NO',                 $
                    te          :   fltarr(14),           $
	            nte         :   0,			  $
                    te_unit     :   0,                    $
                    irnq        :   0,                    $
                    irxq        :   2,                    $
                    irnd        :   1,                    $
                    irxd        :   1,                    $
                    iquad       :   0,                    $
                    imag        :   0,                    $
                    fii         :   95,                   $
                    zeta        :   99,                   $
                    fij         :   75,                   $
                    gij         :   75,                   $
                    rij         :   60,                   $
                    cfg1        :   strarr(10),           $
                    cfg2        :   strarr(10),           $
                    z0          :   0,                    $
                    z1          :   0                     }
	ncfs=state.nconf(ion_ch)
	prom_pars=fltarr(ncfs)
	prom_pars=state.prom_pars(ion_ch,0:ncfs-1)
	ind_1 = where(prom_pars EQ state.igrndpar(ion_ch),num_cfg1)
  	if ncfs EQ 1 then num_cfg2 = 0 else $
      	ind_2 = where(prom_pars NE state.igrndpar(ion_ch),num_cfg2)

	adas801_rcn2_make,'ONE',1,procval,num_cfg1,num_cfg2,state.iz0,state.iz1(ion_ch),$
		text,num_rcn2_p1,error
	rcn2_p1_input = 'in2'
  
 	if bessel eq 1 then begin 
	  	temp=text[0]
		strput,temp,'3',49
		text[0]=temp
	endif
  	
	
	openw, lun, rcn2_p1_input, /get_lun
  	for j =0, n_elements(text)-1 do begin
    	    printf, lun, text[j]
  	endfor
  	free_lun, lun
	spawn,state.fortdir+'/rcn2.x'




	spawn,'grep //r1// out2 > dipole'
        spawn,'rm nlines.dat'
        spawn,'wc dipole > nlines.dat'
        openr,lun_lines,'nlines.dat', /get_lun
        readf,lun_lines,val,dumi,dumi,dums
	close,lun_lines
	free_lun,lun_lines
        val=fix(val)

        type=strarr(val)
        reddip=dblarr(val)
        indx_1=intarr(val)
        indx_2=intarr(val)
        wavel=dblarr(val)
        
        aval=dblarr(val)
        oscstr=dblarr(val)
        ediff=dblarr(val)
        CONF1=INTARR(VAL)
        CONF2=INTARR(VAL)

        dums1=' '
        dums2=' '
		
	openr,lunfort,'dipole',/get_lun
	
 	for i=0,val-1 do begin
 	    readf,lunfort,dums1,dums2, dumr, format='(20x,a20,a20,29x,f8.5)'
 	    str_conf1=strmid(strtrim(string(dums1),2),2)
 	    str_conf2=strmid(strtrim(string(dums2),2),2)
 	    reddip(i)=abs(double(dumr))
 	    for j=0,state.nconf(ion_ch)-1 do begin
 		if strtrim(str_conf1,2) eq strtrim(str_conf_label[j],2) then begin
 			indx_1[i]=j
 		endif
 		if strtrim(str_conf2,2) eq strtrim(str_conf_label[j],2) then begin
 			indx_2[i]=j
 		endif
	    endfor
	    type[i]='d'
	end

        close,lunfort
	free_lun,lunfort
	
	;stop	
	
;        openr,lunfort,'fort.92',/get_lun
; 	for i=0,val-1 do begin
; 	    readf,lunfort,dums1,dums2, dumr, dumi,format='(a22,a22,e12.5,2x,i3)'
; 	    str_conf1[i]=strmid(strtrim(string(dums1),2),2)
; 	    str_conf2[i]=strmid(strtrim(string(dums2),2),2)
; 	    reddip(i)=abs(double(dumr))
; 	    for j=0,state.nconf(ion_ch)-1 do begin
; 		if strtrim(str_conf1[i],2) eq strtrim(str_conf_label[j],2) then begin
; 			indx_1[i]=j
; 		endif
;	        if strtrim(str_conf2[i],2) eq strtrim(str_conf_label[j],2) then begin
;			indx_2[i]=j
;		endif
;	    endfor
;	end

;        close,lunfort
;	free_lun,lunfort
		
	
	; re-order eav/wavel so that it is in energy order
	
	


	for i=0,val-1 do begin
		
		if eav(indx_1[i]) gt eav(indx_2[i]) then begin
			stat_up=stat_wt[indx_1[i]]
			stat_low=stat_wt[indx_2[i]]
		endif else begin
			stat_up=stat_wt[indx_2[i]]
			stat_low=stat_wt[indx_1[i]]		
		endelse
	
	    
	    	ediff[i]=abs(eav(indx_1[i])-eav(indx_2[i]))
		wavel[i]=1.d8/(ediff[i]*109737.26d0)
		if type[i] eq 'd' then begin
	    		aval[i]=reddip[i]^2*(ediff[i]/13.606d0)^3/6.d0/(137.04d0^4)*2.997d10/5.2918d-9/stat_wt[indx_1[i]]
			
  ; Greetings and Salutations Stuart.
  ; Your above expression is a good attempt, no doubt of that
  ; But, it is still wrong
  ; It seems dividing by statistical weight is incorrect.
  ; Plus, you were off by a factor of ~ 840
  ; so...
	
		
  ; further note: the main mistake was the /13.606 since ediff is in rydbergs
  ; this gives a factor of 2519 underestimate., if we pull from nowhere a
  ; factor of 3 then we get our factor of 840
			
			aval[i]=aval[i]*stat_wt[indx_1[i]]*840d0		
			
			oscstr[i]=wavel[i]^2 * aval[i] * (stat_up / (1.0 * stat_low)) / 6.672e15
		endif

		print,str_conf_label[indx_1[i]],str_conf_label[indx_2[i]],reddip[i],ediff[i],stat_wt[indx_1[i]],aval[i]

	endfor

	spawn,'grep //r2// out2 > qupole'
        spawn,'rm nlines.dat'
        spawn,'wc qupole > nlines.dat'
        openr,lun_lines,'nlines.dat', /get_lun
        readf,lun_lines,val2,dumi,dumi,dums
	close,lun_lines
	free_lun,lun_lines
        val2=fix(val2)

	
	openr,lunfort,'qupole',/get_lun

 	for i=0,val2-1 do begin
 	    readf,lunfort,dums1,dums2, dumr, format='(20x,a20,a20,29x,f8.5)'
 	    str_conf1=strmid(strtrim(string(dums1),2),2)
 	    str_conf2=strmid(strtrim(string(dums2),2),2)
 	    for j=0,state.nconf(ion_ch)-1 do begin
		if strtrim(str_conf1,2) eq strtrim(str_conf_label[j],2) then begin
			t_indx_1=j
		endif
		if strtrim(str_conf2,2) eq strtrim(str_conf_label[j],2) then begin
			t_indx_2=j
		endif
	    endfor
	    
	    if t_indx_1 ne t_indx_2 and abs(double(dumr)) gt 0d0 then begin
	    		
			;if i eq 3 then stop
			
			alreadygot=0
			for j=0,val-1 do begin
				if (indx_1[j] eq t_indx_1 and indx_2[j] eq t_indx_2) $
				or (indx_1[j] eq t_indx_2 and indx_2[j] eq t_indx_1) $
				then alreadygot=1	
			end
	    	
	    		if alreadygot eq 0 then begin
			   	indx_1=[indx_1,t_indx_1]
		   		indx_2=[indx_2,t_indx_2]
			   	reddipole=abs(double(dumr))
				reddip=[reddip,reddipole]
				ediffer=abs(eav(t_indx_1)-eav(t_indx_2))
				ediff=[ediff,ediffer]
			   	
				avalue=839d-4*reddipole^2*(ediffer/13.606d0)^5/6.d0/(137.04d0^4)*2.997d10/5.2918d-9

				aval=[aval,avalue]
				
				oscstr=[oscstr,0.0]
				wavel=[wavel,1.d8/(ediffer*109737.26d0)]
				type=[type,'q']
		   		val=val+1
			endif
			
			if alreadygot eq 0 then print,t_indx_1,t_indx_2,'yes'
			if alreadygot eq 1 then print,t_indx_1,t_indx_2,'no'
			
	   endif
	end
	
	free_lun,lunfort






;	ok, now we try to get tabulation over spherical bessel functions:

	if bessel eq 1 then begin

		temp=text[0]
		strput,temp,'50211',32  	; I had this as 50311, but I reckon it's not nesessary now
		text[0]=temp

  		openw, lun, rcn2_p1_input, /get_lun
  		for j =0, n_elements(text)-1 do begin
    	    	printf, lun, text[j]
  		endfor
  		free_lun, lun
		spawn,state.fortdir+'/rcn2.x'

		; now we make sense of the mess
		
		; first find k-grid, according to my research this will be constant for all
		; transitions in a given run of the cowan code. I don't know why cowan
		; prints it out more than once
		
		openr,unit,'out2',/get_lun
		junk=''
readagain:
		readf,unit,junk,format='(A50)'		
		if strpos(junk,'nbigks') ne -1 then nbigks=fix(strmid(junk,strpos(junk,'nbigks')+7))
		if strtrim(junk,2) ne 'k-values' then goto,readagain
		readf,unit,junk
		kvalues=dblarr(nbigks)
		
		readf,unit,kvalues,format='(8F14.7)'
		
		
		spawn,'grep //j0// out2 | grep -v "(   //j0//   )" > j0val'
		spawn,'grep //j1// out2 | grep -v "(   //j1//   )" > j1val'
		spawn,'grep //j2// out2 | grep -v "(   //j2//   )" > j2val'
		
		spawn,'rm nlines.dat'
		spawn,'wc j0val > nlines.dat'
		openr,lun_lines,'nlines.dat', /get_lun
		readf,lun_lines,val2,dumi,dumi,dums
		close,lun_lines
		free_lun,lun_lines
        	nj0=fix(val2) / nbigks
		spawn,'rm nlines.dat'
		spawn,'wc j1val > nlines.dat'
		openr,lun_lines,'nlines.dat', /get_lun
		readf,lun_lines,val2,dumi,dumi,dums
		close,lun_lines
		free_lun,lun_lines
        	nj1=fix(val2) / nbigks
		spawn,'rm nlines.dat'
		spawn,'wc j2val > nlines.dat'
		openr,lun_lines,'nlines.dat', /get_lun
		readf,lun_lines,val2,dumi,dumi,dums
		close,lun_lines
		free_lun,lun_lines
        	nj2=fix(val2) / nbigks

		tempdata=dblarr(nbigks)
		
		
		collindx_1=lonarr(nj0+nj1+nj2)
		collindx_2=lonarr(nj0+nj1+nj2)
		colltype=strarr(nj0+nj1+nj2)
		colldata=dblarr(nj0+nj1+nj2,nbigks)
		rawdata=dblarr(nj0+nj1+nj2,nbigks)
		collediff=dblarr(nj0+nj1+nj2)
		
		currentplace=0
		
		for typoftrans=0,2 do begin
			if typoftrans eq 0 then begin
				njvals=nj0
				stringtype='j0'
				openr,unit,'j0val',/get_lun
			endif
			
			if typoftrans eq 1 then begin
				njvals=nj1
				stringtype='j1'
				openr,unit,'j1val',/get_lun
			endif
			
			if typoftrans eq 2 then begin
				njvals=nj2
				stringtype='j2'			
				openr,unit,'j2val',/get_lun
			endif
			
			
		
		
		
			for i=0,njvals-1 do begin
				for j=0,nbigks-1 do begin
 	    				readf,unit,dums1,dums2, dumr, format='(20x,a20,a20,29x,f8.5)'
 	    				str_conf1=strmid(strtrim(string(dums1),2),2)
 	    				str_conf2=strmid(strtrim(string(dums2),2),2)
	 	    			if j eq 0 then begin
						for k=0,state.nconf(ion_ch)-1 do begin
							if strtrim(str_conf1,2) eq strtrim(str_conf_label[k],2) then begin
								t_indx_1=k
							endif
							if strtrim(str_conf2,2) eq strtrim(str_conf_label[k],2) then begin
								t_indx_2=k
							endif
		    				endfor
					endif
					tempdata[j]=dumr
				end
			
				;stop

				collediffer=abs(eav(t_indx_1)-eav(t_indx_2))
			
				process=1
			
				if t_indx_1 eq t_indx_2 then process=0
				
				
				for j=0,currentplace-1 do begin
					if  ( (collindx_1[j] eq t_indx_1 and collindx_2[j] eq t_indx_2) $
					or    (collindx_1[j] eq t_indx_2 and collindx_2[j] eq t_indx_1) $
					    ) $
					and (strpos(colltype[j],stringtype) ne -1) $
					then process=0	
				end
				
							
				;print,t_indx_1,t_indx_2,process

				insertindex=currentplace

				for j=0,currentplace-1 do begin
					if  ( (collindx_1[j] eq t_indx_1 and collindx_2[j] eq t_indx_2) $
					or    (collindx_1[j] eq t_indx_2 and collindx_2[j] eq t_indx_1) $
					    ) $
					and (strpos(colltype[j],stringtype) eq -1) $
					then insertindex=j	
				end

				if process eq 1 then begin
					collindx_1[insertindex]=t_indx_1
					collindx_2[insertindex]=t_indx_2
					collediff[insertindex]=collediffer
					rawdata[insertindex,*]=tempdata[*]
					;colldata[insertindex,*]=colldata[insertindex,*] + collediffer*(2*typoftrans+1)*(tempdata[*])^2.
					;colldata[insertindex,*]=colldata[insertindex,*] + (2*typoftrans+1)*(tempdata[*])^2.
					colldata[insertindex,*]=colldata[insertindex,*] + (tempdata[*])^2.
					
					colltype[insertindex]=colltype[insertindex]+stringtype
					if insertindex eq currentplace then begin
						currentplace=currentplace+1
					endif
					print,insertindex
				endif
			end
		
		end
		
		colltype=colltype[0:currentplace-1]
		collindx_1=collindx_1[0:currentplace-1]
		collindx_2=collindx_2[0:currentplace-1]
		collediff=collediff[0:currentplace-1]
		colldata=colldata[0:currentplace-1,*]
		rawdata=rawdata[0:currentplace-1,*]
		
		;stop
		
		free_lun,unit
		


	endif

	;stop

	; let's try an integration for the first transition, 2s - 3s
	
if bessel eq 1 then begin
	
	egrid=[ 2.80, $
		2.90, $
		3.00, $
		3.50, $
		4.00, $
		5.00, $
		6.0, $
		10.0, $
		15.0, $
		20.0, $
		30.0, $
		50.0, $
		100.0, $
		500.0 ]
	
	;egrid=[100.]
	
	kmin=dblarr((size(egrid))[1])
	kmax=dblarr((size(egrid))[1])
	collstr=dblarr((size(egrid))[1])
	
        allan=[0,1,2,4,6]
        
        plot,[0],xrange=[1,100],yrange=[0.1,100],/xlog,/ylog,/nodata
        
        for bah=0,4 do begin
        
        transition=allan[bah]
	
        ;transition=2
        
	for i=0,(size(egrid))[1]-1 do begin
	        
                if (eav[collindx_1[transition]] gt eav[collindx_2[transition]] ) then begin
                        stat_up=stat_wt[collindx_1[transition]]
                        stat_low=stat_wt[collindx_2[transition]]
                endif else begin
                        stat_up=stat_wt[collindx_2[transition]]
                        stat_low=stat_wt[collindx_1[transition]]
                endelse
                
        
        
		kmin[i]=egrid[i]^(1./2.)-(egrid[i]-collediff[transition])^(1./2.)
		kmax[i]=egrid[i]^(1./2.)+(egrid[i]-collediff[transition])^(1./2.)
	
		first=1
		lastused=0
	        
                collstr[i]=0
        
		for j=0,nbigks-1 do begin
			if kvalues[j] ge kmin[i] and kvalues[j] le kmax[i] then begin
				if first eq 1 then begin
					collstr[i]=collstr[i]+colldata[transition,j]
					first=0
				endif else begin			
					collstr[i]=collstr[i]+colldata[transition,j]*2
					lastused=j
				endelse
			endif
		end
		
		if lastused ne 0 then collstr[i]=collstr[i]-colldata[transition,lastused]
		
		;stop
		
		;collstr[i]=collstr[i]*4*(alog(kvalues[1])-alog(kvalues[0]))*2.;*stat_low/(1.0 * stat_up);*collediff[transition]
		;collstr[i]=collstr[i]*4*(alog(kvalues[1])-alog(kvalues[0]))*stat_low
                print,stat_up,stat_low
		collstr[i]=collstr[i]*4*(alog(kvalues[1])-alog(kvalues[0]))*2.  ; <--- I don't know where this factor 2 comes from,
                                                                                ; it could be pi/sqrt(3), note that the 4 takes into
                                                                                ; account he division by two following integration
                                                                                ; so this shouldn't be 8.

	end
	
        oplot,egrid / collediff[transition],collstr
        ;plot,egrid,collstr,/ylog;,xrange=[0,250],yrange=[0.001,0.1]
   
        end
        
	stop	

endif



;	sort into energy order (for CR calculations)

	svec=sort(eav)
	
	eav=eav[svec]
	stat_wt=stat_wt[svec]
	
	indx_1old=indx_1
	indx_2old=indx_2
	for i=0,n_elements(indx_1)-1 do begin
		indx_1[i]=(where(svec eq indx_1old[i]))[0]
		indx_2[i]=(where(svec eq indx_2old[i]))[0]
	end
	
	;stop

	cross=cross[svec]
	
	; at a casual glance, you may think cross will just end up as 0,1,2,3...
	; this is not the case generally and don't assume it will be
	
        FOR I=0,(SIZE(CONF1))[1]-1 DO BEGIN
                CONF1[I]=CROSS[INDX_1[I]]
                CONF2[I]=CROSS[INDX_2[I]]
        END



	atomdata={ntran:val,eav:eav,conf1:conf1,conf2:conf2,stat_wt:stat_wt,aval:aval,oscstr:oscstr,ediff:ediff,indx_1:indx_1,indx_2:indx_2,wavel:wavel,cross:cross,config_list:config_list,ei:ei,bessel:bessel}

 
END
