PRO h2prom, strct,state
shl_lab=['1s','2s','2p','3s','3p','3d','4s','4p','4d','4f','5s','5p',$
'5d','5f','5g','6s','6p','6d','6f','6g','6h']
shmax=[2,2,6,2,6,10,2,6,10,14,2,6,10,14,18,2,6,10,14,18,22]
lvals=['s','p','d','f','g','h']
shl_min=[1,2,4,7,11,16]
iflag=0
icut=0
for i=strct.ion_min,strct.ion_max do begin
	if state.nval(i) eq strct.val_val then begin
	if strtrim(state.prom_config[i,0],2) ne '' then begin
		state.grndl[i]=''
		state.nconf[i]=0
		for j=0,99 do begin
			state.prom_config[i,j]=''
			state.prom_lastl[i,j]=''
			state.prom_pars[i,j]=0
			for k=0,20 do begin
				state.ioccup_store_excit[i,j,k]=0
			endfor
		endfor     
	endif
;	stop

	test_ioccup=intarr(21)
	confg=' '+strtrim(state.config[i],2)
	adjust=0
	if fix(strlen(confg)) mod 5 ne 0 then adjust=1
	len_confg=fix(fix(strlen(confg))/5)+adjust
	first_shl=strtrim(strmid(confg,0,3),2)
	for j=0,20 do begin
		if first_shl eq shl_lab[j] then indx=j
	endfor
	for j=0,indx-1 do begin
		test_ioccup[j]=shmax[j]
	endfor
	for j=0,len_confg-1 do begin
		shl=strmid(confg,5*j,3)
		for k=0,20 do begin
			if strtrim(string(shl),2) eq shl_lab[k] then indx=k
		endfor
		test_ioccup[indx]=fix(strtrim(strmid(confg,5*j+3,2),2))
		if test_ioccup[indx] ne shmax[indx] and iflag eq 0 then begin
			icut=indx
			iflag=1
		endif
	endfor
	maxshl=where(test_ioccup ne 0)
	maxshl=size(maxshl)
	if icut eq 0 and maxshl[1] ne 0 then icut=maxshl[1]-1
;	if icut eq 0 then icut=1
	if (strct.iprom eq 0) then begin
		icut=shl_min[strct.iprom_low-1]
	endif
	groundl=strtrim(strmid(shl,2,1),2)
	for j=0,5 do begin
		if groundl eq lvals[j] then groundl_value=j
	endfor
	state.grndl[i]=lvals[groundl_value]
;	stop

	print,'idl :',state.config(i)
	spawn, state.fortdir+'/promotion.x', unit=pipe, /noshell, PID=pid
	for j=0,20 do begin
		printf,pipe,test_ioccup[j], format='(i2)'
	endfor
	printf,pipe,icut,format='(i2)'
	printf,pipe,state.nval(i), format='(i6)'
	printf,pipe,state.iz0, state.iz1(i), format='(i3,i3)'
	printf,pipe,strct.open_val, format='(i3)'
	if strct.open_val eq 0 then begin
		printf,pipe,strct.nmaxdn,strct.nmindn, format='(i3,i3)'
		printf,pipe,strct.nmaxl, strct.nminl, format='(i3,i3)'
	endif
	if strct.open_val eq 1 then begin
		printf,pipe,strct.nmaxdn_1,strct.nmindn_1, format='(i3,i3)'
		printf,pipe,strct.nmaxl_1, strct.nminl_1, format='(i3,i3)'
		printf,pipe,strct.nmaxdn_2,strct.nmindn_2, format='(i3,i3)'
		printf,pipe,strct.nmaxl_2, strct.nminl_2, format='(i3,i3)'
	endif
	printf,pipe,strct.iprom, format='(i3)'
	if (strct.iprom eq 0) then begin
		printf,pipe,strct.iprom_low, strct.iprom_high, format='(i3,i3)'
	endif
   
	temp=' '
	readf,pipe,allan   
	nconf=fix(allan)
	state.nconf(i)=nconf
	readf,pipe,temp, format='(a100)'
	state.prom_config(i,0)=string(temp)
	readf,pipe,temp,format='(I2)'
	ndshell=fix(temp)
	for k=0,ndshell-1 do begin
		readf,pipe,temp,format='(I2)'
		state.ioccup_store_excit(i,0,k)=fix(temp)
	endfor
	STATE.PROM_LASTL(I,0)=STATE.GRNDL(I)
	STATE.PROM_PARS(I,0)=STATE.IGRNDPAR(I)
	for j=1,nconf-1 do begin
		a=' '
		b=0
		c=' '
		readf,pipe,a,format='(a100)'
		readf,pipe,b,format='(i1)' 
		state.prom_config(i,j)=string(a)
		state.prom_pars(i,j)=fix(b)
		len=strlen(strtrim(state.prom_config[i,j],2))
		state.prom_lastl(i,j)=strmid(strtrim(state.prom_config[i,j],2),len-2,1)
		for k=0,ndshell-1 do begin
			readf,pipe,temp,format='(i2)'
			state.ioccup_store_excit(i,j,k)=fix(temp)
		endfor
	endfor
	print,'finished stage ',i, nconf, strtrim(state.config(i))
	free_lun,pipe
	endif
   
   ;print,state.prom_nlev(i,0:nconf-1)
   
   ;stop
;-------------------------------------------------------------------------
;now do level count
	res=where(strtrim(state.prom_config[i,*],2) ne '')
	n_prom_config=n_elements(res)
	n_levs=intarr(n_elements(res))
	for l=0,n_prom_config-1 do begin
		ioccup=reform(state.ioccup_store_excit[i,l,*],n_elements(state.ioccup_store_excit[i,l,*]))
		n_levs[l]=h2nlev(ioccup)
		if total(ioccup) eq 0 then n_levs[l]=0
	endfor
	state.prom_nlev[i,0:n_elements(n_levs)-1]=n_levs[*]
;-------------------------------------------------------------------------
   
endfor
END
