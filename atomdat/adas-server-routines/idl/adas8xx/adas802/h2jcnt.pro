FUNCTION h2jcnt,TERM

	tmp_l=fix(term/100)*100
	if tmp_l eq 0 then tmp_l=100
	l_val=term mod tmp_l
	s_val=((double(term-l_val))/100.d0/2.d0)
	
	count=0
	jmax = l_val > s_val
	jmin = l_val < s_val
	jindx = 2*jmax+1
	
	for i=0,jindx-1 do begin
		j=jmax+jmin-i
		if j lt (jmax-jmin) then goto,jump
		count=count+1
	endfor
jump:
	return,count

END
