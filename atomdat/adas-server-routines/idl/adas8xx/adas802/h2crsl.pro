pro h2crsl,state,ion_ch,data,tev,edens
		
	
	
	ntran=(*state.atomdata[ion_ch]).ntran
	eav=(*state.atomdata[ion_ch]).eav
	conf1=(*state.atomdata[ion_ch]).conf1
	conf2=(*state.atomdata[ion_ch]).conf2
	stat_wt=(*state.atomdata[ion_ch]).stat_wt
	aval=(*state.atomdata[ion_ch]).aval
	oscstr=(*state.atomdata[ion_ch]).oscstr
	ediff=(*state.atomdata[ion_ch]).ediff
	indx_1=(*state.atomdata[ion_ch]).indx_1
	indx_2=(*state.atomdata[ion_ch]).indx_2
	wavel=(*state.atomdata[ion_ch]).wavel

	

	effcollstr=dblarr(ntran)
        qup=dblarr(ntran)
        qdown=dblarr(ntran)
        pec=dblarr(ntran)


	arstup=lonarr(ntran)
	arstlow=lonarr(ntran)
	argiiiav=dblarr(ntran)


        h2effc,tev,1,effcollstr,(*state.atomdata[ion_ch]),ion_ch,state.fortdir
	for i=0,ntran-1 do begin
		if eav(indx_1[i]) gt eav(indx_2[i]) then begin
			stat_up=stat_wt[indx_1[i]]
			stat_low=stat_wt[indx_2[i]]
		endif else begin
			stat_up=stat_wt[indx_2[i]]
			stat_low=stat_wt[indx_1[i]]		
		endelse
                
                ; adas manual, equation 2.1.8
		qdown[i]=(2.1716e-8 / stat_up) * (13.606 / tev)^(1./2.) * effcollstr[i]
		qup[i]= exp (ediff[i] * 13.606 / tev) * (double(stat_low) / stat_up) * qdown[i]

		if stat_up eq 0 or stat_low eq 0 then begin
			print,'Error calculating Statistical Weight, setting A-value and rates to 1e-30'
			aval[i]=1e-30
			qup[i]=1e-30
			qdown[i]=1e-30
		endif		
        endfor
        		
	rates=dblarr(state.nconf[ion_ch],state.nconf[ion_ch])
	rates[*]=1e-30
	for i=0,state.nconf[ion_ch]-1 do begin
		for j=0,state.nconf[ion_ch]-1 do begin
			for k=0,ntran-1 do begin
				if eav(indx_1[k]) gt eav(indx_2[k]) then begin
					upper_index=indx_1[k]
					lower_index=indx_2[k]
				endif else begin
					upper_index=indx_2[k]
					lower_index=indx_1[k]
				endelse
				
				if eav[i] gt eav[j] then begin
					if upper_index eq i and lower_index eq j then begin
						rates[i,j]=aval[k]+edens*qdown[k]
					endif			
				endif
				
				if eav[j] gt eav[i] then begin
					if lower_index eq i and upper_index eq j then begin
						rates[i,j]=edens*qup[k]
					endif			
				endif				
			end		
		end
	end
		
		
	crmat=dblarr(state.nconf[ion_ch],state.nconf[ion_ch])
	
	crmat[*]=1e-30
	
	for i=0,state.nconf[ion_ch]-1 do begin
		for j=0,state.nconf[ion_ch]-1 do begin
			if i eq j then begin
				crmat[i,i]=-total(rates[i,*])
			endif else begin		
				crmat[j,i]=rates[j,i]
			endelse
		end
	end
	
	;stop
	
	a=where(crmat eq 0)
	if a[0] ne -1 then crmat[a]=1e-30
		
	crinv=invert(crmat[1:*,1:*],/double)
	
	popn=dblarr(state.nconf[ion_ch])
	popn[0]=1
	for i=0,state.nconf[ion_ch]-2 do begin
		popn(i+1)=-total(crinv[*,i]*crmat[0,1:*])
	end

;	print,popn

	for i=0,ntran-1 do begin
		if eav[indx_1[i]] gt eav[indx_2[i]] then begin
			upper_index=indx_1[i]
		endif else begin
			upper_index=indx_2[i]
		endelse
		pec[i]=aval[i]*popn[upper_index]
	end

        DATA={AVAL:pec,WAVEL:WAVEL,CONF1:CONF1,CONF2:CONF2}
       
       
       	print,crmat
	
	print,'------------------'
	
	print,crinv
       
	
		print,'------------------',oscstr
		print,'------------------',effcollstr
		print,'------------------',aval
		print,'------------------',double(arstup)
		print,'------------------',double(arstlow)
		print,'------------------',argiiiav
		print,'------------------',ediff
	
		print,popn
		print,eav



end
