PRO H2IONE, ionsn_rules, state, str_confg, ion_ch
;-------------------------------------------------------------------------------
;routine to generate configurations for CADW ionisation codes
;-------------------------------------------------------------------------------

num_shls=36
shl_lab=['1s','2s','2p','3s','3p','3d','4s','4p','4d','4f','5s','5p',$
'5d','5f','5g','6s','6p','6d','6f','6g','6h','7s','7p','7d','7f','7g',$
'7h','7i','8s','8p','8d','8f','8g','8h','8i','8k']
shmax=[2,2,6,2,6,10,2,6,10,14,2,6,10,14,18,2,6,10,14,18,22,$
2,6,10,14,18,22,26,2,6,10,14,18,22,26,30]
noble_core=[0,0,0,3,3,5,5,5,8,8,8,11,11,11,11,11,11,11,11,11,$
11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11]
lvalue=[0,0,1,0,1,2,0,1,2,3,0,1,2,3,4,0,1,2,3,4,5,0,1,2,3,$
4,5,6,0,1,2,3,4,5,6,7]
lstr=['s','p','d','f','g','h','i','k']

ionsn_occup=intarr(num_shls)
state.icount_dirct[ion_ch]=0
;-------------------------------------------------------------------------------
; zero arrays for ionisation stages being processed
;-------------------------------------------------------------------------------
for i=0,49 do begin
	for j=0,num_shls-1 do begin
		state.ioccup_ionsn_grnds[ion_ch,i,j]=0
		state.ioccup_ionsn_dirct[ion_ch,i,j]=0
		state.dirct_parity[ion_ch,i]=0
		state.dirct_lbl[ion_ch,i]=''
                state.grnd_indx_dirct[ion_ch,i]=0
                state.grnd_indx_ea[ion_ch,i]=0                
	endfor
endfor
state.ea_indx_set[ion_ch]=0
igindx=1
;-------------------------------------------------------------------------------
; evaluate indices for ionisation rules taken from processing widget
;-------------------------------------------------------------------------------
drct_mnshl=fix(ionsn_rules.ion_stage_drct_mnshl)
ind_mnshl=fix(ionsn_rules.ion_stage_ind_mnshl)
ind_mxshl=fix(ionsn_rules.ion_stage_ind_mxshl)
ind_lmax=fix(ionsn_rules.ion_stage_ind_mxl)

pe_vac=fix(ionsn_rules.ion_stage_pe_vac)
pe_mnshl=fix(ionsn_rules.ion_stage_pe_mnshl)
pe_mxshl=fix(ionsn_rules.ion_stage_pe_mxshl)
pe_lmax=fix(ionsn_rules.ion_stage_pe_mxl)

pe_ea_mnshl=fix(ionsn_rules.ion_stage_pe_indrct_dnmin)
pe_ea_mxshl=fix(ionsn_rules.ion_stage_pe_indrct_dnmax)
pe_ea_lmax=fix(ionsn_rules.ion_stage_pe_indrct_mxl)
;-------------------------------------------------------------------------------
; convert ground configuration to occupation array
;-------------------------------------------------------------------------------
	if state.nconf[ion_ch] eq 0 then begin
        	icount_configs=0
        endif else begin 
        	icount_configs=state.nconf[ion_ch]-1
        endelse
        if state.nconf[ion_ch+1] eq 0 then begin
        	icount_configs_p1=0
        endif else begin
        	icount_configs_p1=state.nconf[ion_ch+1]-1
        endelse
	for l=0,num_shls-1 do ionsn_occup[l]=0
	iflag=0
	confg=' '+strtrim(str_confg,2)
	adjust=0
	if fix(strlen(confg)) mod 5 ne 0 then adjust=1
	len_confg=fix(fix(strlen(confg))/5)+adjust
	first_shl=strtrim(strmid(confg,0,3),2)
	for j=0,num_shls-1 do begin
		if first_shl eq shl_lab[j] then indx=j
	endfor
	for j=0,indx-1 do begin
		ionsn_occup[j]=shmax[j]
	endfor
	for j=0,len_confg-1 do begin
		shl=strmid(confg,5*j,3)
		for k=0,num_shls-1 do begin
			if strtrim(string(shl),2) eq shl_lab[k] then indx=k
		endfor
		ionsn_occup[indx]=fix(strtrim(strmid(confg,5*j+3,2),2))
	endfor
	for j=0,num_shls-1  do begin
		if ionsn_occup[j] ne 0 then max_shl=j
	endfor

;-------------------------------------------------------------------------------
; evaluate possible direct ionisation routes for ground
;-------------------------------------------------------------------------------
	for j=max_shl, drct_mnshl,-1  do begin
		if ionsn_occup[j]-1 ge 0 then begin
			for k=0,num_shls-1 do begin
				state.ioccup_ionsn_grnds[ion_ch, state.icount_dirct[ion_ch],k]=ionsn_occup[k]
			endfor
                        state.grnd_indx_dirct[ion_ch,state.icount_dirct[ion_ch]]=igindx
                        ionsn_occup[j]=ionsn_occup[j]-1
			parity=0
			for k=0,num_shls-1 do begin
				state.ioccup_ionsn_dirct[ion_ch, state.icount_dirct[ion_ch],k]=ionsn_occup[k]
				parity=parity+lvalue[k]*ionsn_occup[k]
			endfor
			parity=parity mod 2
			state.dirct_parity[ion_ch,state.icount_dirct[ion_ch]]=parity
			state.dirct_lbl[ion_ch, state.icount_dirct[ion_ch]]=shl_lab[j]
			state.icount_dirct[ion_ch]=state.icount_dirct[ion_ch]+1
			ionsn_occup[j]=ionsn_occup[j]+1
		endif
	endfor	
;-------------------------------------------------------------------------------
; evaluate indirect ionisation routes for ground
;-------------------------------------------------------------------------------
if ionsn_rules.onoff_indirct eq 1 then begin
        for j=ind_mnshl,max_shl-1 do begin
                state.icount_ea[ion_ch, state.ea_indx_set[ion_ch]]=0
		if (ionsn_occup[j] -1) ge 0 then  begin
		for k=0,num_shls-1 do begin
			state.ioccup_ionsn_ea_grnds[ion_ch,state.ea_indx_set[ion_ch],k]=ionsn_occup[k]
		endfor
                state.grnd_indx_ea[ion_ch,state.ea_indx_set[ion_ch]]=igindx
		ionsn_occup[j]=ionsn_occup[j]-1
		state.ea_grnd_lbl[ion_ch,state.ea_indx_set[ion_ch]]=shl_lab[j]
                for k=j+1,ind_mxshl do begin
			if lvalue[k] le ind_lmax then begin
				if ionsn_occup[k] le shmax[k]-1 then begin
					ionsn_occup[k]=ionsn_occup[k]+1
                                        for l=0,num_shls-1 do begin
						state.ioccup_ionsn_ea[ion_ch,state.ea_indx_set[ion_ch],state.icount_ea[ion_ch,state.ea_indx_set[ion_ch]],l]=ionsn_occup[l]
					endfor
                                        state.ea_ex_lbl[ion_ch,state.ea_indx_set[ion_ch],state.icount_ea[ion_ch,state.ea_indx_set[ion_ch]]]=shl_lab[k]
					state.icount_ea[ion_ch,state.ea_indx_set[ion_ch]]=state.icount_ea[ion_ch,state.ea_indx_set[ion_ch]]+1
					ionsn_occup[k]=ionsn_occup[k]-1
				endif
			endif
		endfor
		state.ea_indx_set[ion_ch]=state.ea_indx_set[ion_ch]+1
		ionsn_occup[j]=ionsn_occup[j]+1
		endif 		
	endfor
endif
;-------------------------------------------------------------------------------
; evaluate parent+excited electron ionisation routes
;-------------------------------------------------------------------------------
if ionsn_rules.onoff_ps_dirct eq 1 then begin
	j=pe_vac
	if ionsn_occup[j]-1 ge 0 then begin
		ionsn_occup[j]=ionsn_occup[j]-1
		for k=j+1,pe_mxshl do begin
			if lvalue[k] le pe_lmax then begin
				ionsn_occup[k]=ionsn_occup[k]+1
                		igindx=igindx+1
				mx_array=[max_shl,k]
				for kk=pe_mnshl,max(mx_array) do begin
					if ionsn_occup[kk] gt 0 then begin
						for l=0,num_shls-1 do begin
							state.ioccup_ionsn_grnds[ion_ch,state.icount_dirct[ion_ch],l]=ionsn_occup[l]
						endfor
						state.grnd_indx_dirct[ion_ch,state.icount_dirct[ion_ch]]=igindx
						ionsn_occup[kk]=ionsn_occup[kk]-1
						for l=0,num_shls-1 do begin
							state.ioccup_ionsn_dirct[ion_ch,state.icount_dirct[ion_ch],l]=ionsn_occup[l]
							parity=parity+lvalue[l]*ionsn_occup[l]
						endfor
						parity=parity mod 2
						state.dirct_parity[ion_ch,state.icount_dirct[ion_ch]]=parity
						state.dirct_lbl[ion_ch, state.icount_dirct[ion_ch]]=shl_lab[kk]
						state.icount_dirct[ion_ch]=state.icount_dirct[ion_ch]+1
						parity=0
						ionsn_occup[kk]=ionsn_occup[kk]+1
					endif
				endfor
;-------------------------------------------------------------------------------
; evaluate parent+excited electron EA ionisation routes
;-------------------------------------------------------------------------------
				if ionsn_rules.onoff_ps_indirct	eq 1 then begin
					for jj=pe_ea_mnshl,pe_ea_mxshl-1 do begin
				                state.icount_ea[ion_ch, state.ea_indx_set[ion_ch]]=0
						if (ionsn_occup[jj] -1) ge 0 then  begin
							for kk=0,num_shls-1 do begin
								state.ioccup_ionsn_ea_grnds[ion_ch,state.ea_indx_set[ion_ch],kk]=ionsn_occup[kk]
							endfor
                                                        state.grnd_indx_ea[ion_ch,state.ea_indx_set[ion_ch]]=igindx
							ionsn_occup[jj]=ionsn_occup[jj]-1
							state.ea_grnd_lbl[ion_ch,state.ea_indx_set[ion_ch]]=shl_lab[jj]
				        	        for kk=jj+1,pe_ea_mxshl do begin
								if lvalue[kk] le pe_ea_lmax and kk ne jj then begin
									if ionsn_occup[kk] le shmax[kk]-1 then begin
										ionsn_occup[kk]=ionsn_occup[kk]+1

				                                        	for l=0,num_shls-1 do begin
											state.ioccup_ionsn_ea[ion_ch,state.ea_indx_set[ion_ch],state.icount_ea[ion_ch,state.ea_indx_set[ion_ch]],l]=ionsn_occup[l]
										endfor
                	                				       	state.ea_ex_lbl[ion_ch,state.ea_indx_set[ion_ch],state.icount_ea[ion_ch,state.ea_indx_set[ion_ch]]]=shl_lab[kk]
										state.icount_ea[ion_ch,state.ea_indx_set[ion_ch]]=state.icount_ea[ion_ch,state.ea_indx_set[ion_ch]]+1
										ionsn_occup[kk]=ionsn_occup[kk]-1
									endif
								endif
							endfor
							state.ea_indx_set[ion_ch]=state.ea_indx_set[ion_ch]+1
							ionsn_occup[jj]=ionsn_occup[jj]+1
						endif 		
					endfor
				endif
;-------------------------------------------------------------------------------
				ionsn_occup[k]=ionsn_occup[k]-1
			endif
		endfor
		ionsn_occup[j]=ionsn_occup[j]+1
	endif
endif	
END
