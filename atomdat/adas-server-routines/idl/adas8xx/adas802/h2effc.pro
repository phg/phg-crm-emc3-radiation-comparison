pro h2effc,tev,ntemp,effcollstr,atomdata,ion_ch,fortdir

        if atomdata.bessel eq 0 then begin

                effcollstr=dblarr(atomdata.ntran,ntemp)

                for i=0,atomdata.ntran-1 do begin
			indx_1=atomdata.indx_1[i]
			indx_2=atomdata.indx_2[i]
			aval=atomdata.aval[i]
			oscstr=atomdata.oscstr[i]
			ediff=atomdata.ediff[i]
			
			if atomdata.eav(indx_1) gt atomdata.eav(indx_2) then begin
				stat_low=atomdata.stat_wt[indx_2]
			endif else begin
				stat_low=atomdata.stat_wt[indx_1]		
			endelse
			
			for j=0,ntemp-1 do begin
				u=ediff*13.606 / tev[j]
				if ion_ch eq 0 then begin
					gam2=double(1) / ediff
				endif else begin
					gam2=double(ion_ch^2) / ediff
				endelse
				h2giii,u,gam2, giiiav,fortdir
				effcollstr[i,j]=oscstr* (8d0 * acos(-1) / sqrt(3)) * (1d0 / ediff) * stat_low*giiiav
			end

			
                end
                
                return
        endif
        
        
        
        



end
