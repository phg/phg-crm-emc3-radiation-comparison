PRO H2MRG,state

;Routine to merge excitation configurations and ionisation configurations
;The routine first checks for multiple occurances of the same configuration.

num_shls=36
numcfgs=50
lvalue=[0,0,1,0,1,2,0,1,2,3,0,1,2,3,4,0,1,2,3,4,5,0,1,2,3,$
4,5,6,0,1,2,3,4,5,6,7]
shmax=[2,2,6,2,6,10,2,6,10,14,2,6,10,14,18,2,6,10,14,18,22,$
2,6,10,14,18,22,26,2,6,10,14,18,22,26,30]
first_array=[0,1,1,3,3,5,5,5,8,8,8,8,8,8,8,8,8,17,17,17,17,17,17,17,17,$
17,17,17,17,17,17,17,17,17,17,17]
shl_lab=['1s','2s','2p','3s','3p','3d','4s','4p','4d','4f','5s','5p',$
'5d','5f','5g','6s','6p','6d','6f','6g','6h','7s','7p','7d','7f','7g',$
'7h','7i','8s','8p','8d','8f','8g','8h','8i','8k']
str_he='1s2' 
str_ne='1s22s22p6'
str_ar='1s22s22p63s23p6' 
str_kr='1s22s22p63s23p63d104s24p6'
str_xe='1s22s22p63s23p63d104s24p64d105s25p6'
str_rn='1s22s22p63s23p63d104s24p64d104f105s25p65d106s26p6'                           

for i=state.ion_min,state.ion_max do begin
	for j=0,numcfgs-1 do begin
		state.prom_config[i,j]=''
		state.prom_config_tmp[i,j]=''
		state.prom_nlev[i,j]=0
		state.prom_pars[i,j]=0
		state.prom_lastl[i,j]=''
		for k=0,num_shls-1 do state.ioccup_store[i,j,k]=0
	endfor
endfor

str_conf_store=strarr(100,1000)
parity=intarr(100,1000)
tmp_occup_store=intarr(100,1000,num_shls)
ioccup_tmp=intarr(num_shls)
indx=intarr(1000)

for i = state.ion_min,state.ion_max do begin
	indx[i] = 0
	for j=0,numcfgs-1 do begin
		etot=0
		for k=0,num_shls-1 do etot=etot+state.ioccup_store_excit[i,j,k]
		if etot gt 0 then begin
			for k=0,num_shls-1 do begin
				if state.ioccup_store_excit[i,j,k] ne 0 then begin
					str_conf_store[i,indx[i]] = str_conf_store[i,indx[i]]+shl_lab[k]+strtrim(string(state.ioccup_store_excit[i,j,k]),2)
					tmp_occup_store[i,indx[i],k]=state.ioccup_store_excit[i,j,k]
				endif
			endfor
			indx[i]=indx[i]+1
		endif
	endfor
        
	if state.cadwion_on eq 1 then begin
		for j=0,numcfgs-1 do begin
			etot=0
			for k=0,num_shls-1 do etot=etot+state.ioccup_ionsn_grnds[i,j,k]
			if etot gt 0 then begin
				for k=0,num_shls-1 do begin
					if state.ioccup_ionsn_grnds[i,j,k] ne 0 then begin
						str_conf_store[i,indx[i]] = str_conf_store[i,indx[i]]+shl_lab[k]+strtrim(string(state.ioccup_ionsn_grnds[i,j,k]),2)
						tmp_occup_store[i,indx[i],k]=state.ioccup_ionsn_grnds[i,j,k]
					endif
				endfor
				indx[i]=indx[i]+1
			endif
		endfor
	endif
endfor
if state.cadwion_on eq 1 then begin
	for i = state.ion_min+1,state.ion_max+1 do begin
		for j=0,numcfgs-1 do begin
			etot=0
			for k=0,num_shls-1 do etot=etot+state.ioccup_ionsn_dirct[i-1,j,k]
			if etot gt 0 then begin
				for k=0,num_shls-1 do begin
					if state.ioccup_ionsn_dirct[i-1,j,k] ne 0 then begin
						str_conf_store[i,indx[i]] = str_conf_store[i,indx[i]]+shl_lab[k]+strtrim(string(state.ioccup_ionsn_dirct[i-1,j,k]),2) 					     
						tmp_occup_store[i,indx[i],k]=state.ioccup_ionsn_dirct[i-1,j,k]
					endif
				endfor
				indx[i]=indx[i]+1
			endif
		endfor
	endfor
endif

for i = state.ion_min,state.ion_max+1 do begin
	indx_prom_config=0
	for ii=0,indx[i]-1 do begin
		test = where (str_conf_store[i,ii] eq state.prom_config_tmp[i,*])
		if fix(test[0]) eq -1 then begin
;			min_shl=35
;			for k=0,num_shls-1 do begin
;				if (tmp_occup_store[i,ii,k] ne 0) and (tmp_occup_store[i,ii,k] ne shmax[k]) and (first_array[k] lt min_shl) then begin
;					min_shl=first_array[k]
;				endif
;			endfor
;                        
;                        if min_shl eq 35 then begin
;                        	str_tmp=''
;                                for k=0,num_shls-1 do begin
;                                	if tmp_occup_store[i,ii,k] ne 0 then str_tmp=str_tmp+shl_lab[k]+strtrim(string(tmp_occup_store[i,ii,k]),2)
;				endfor
;                                if str_tmp eq str_he then min_shl=0
;                                if str_tmp eq str_ne then min_shl=2
;                                if str_tmp eq str_ar then min_shl=4
;                                if str_tmp eq str_kr then min_shl=7
;                                if str_tmp eq str_xe then min_shl=11
;                                if str_tmp eq str_rn then min_shl=16 
;                                min_shl=first_array[min_shl]                               
;                        endif
			str_tmp=''
			for k=0,num_shls-1 do begin
				if tmp_occup_store[i,ii,k] ne 0 then str_tmp=str_tmp+shl_lab[k]+strtrim(string(tmp_occup_store[i,ii,k]),2)
			endfor
                        icount=0
                        if strpos(str_tmp,str_he) ne -1 then icount=0
                        if strpos(str_tmp,str_ne) ne -1 then icount=2
                        if strpos(str_tmp,str_ar) ne -1 then icount=4
                        if strpos(str_tmp,str_kr) ne -1 then icount=7
                        if strpos(str_tmp,str_xe) ne -1 then icount=11
                        if strpos(str_tmp,str_rn) ne -1 then icount=16
;			if str_tmp eq str_he then icount=0
;			if str_tmp eq str_ne then icount=2
;			if str_tmp eq str_ar then icount=3
;			if str_tmp eq str_kr then icount=7
;			if str_tmp eq str_xe then icount=11
;			if str_tmp eq str_rn then icount=16
                        min_shl=icount
                        if min_shl lt num_shls-1 then begin
                        	for k=min_shl,num_shls-1 do begin
					if tmp_occup_store[i,ii,k] ne 0 then begin
						if tmp_occup_store[i,ii,k] lt 10 then begin
							str_occup=strtrim(string(tmp_occup_store[i,ii,k]),2)+' '
		                               endif else begin
        		                              str_occup=strtrim(string(tmp_occup_store[i,ii,k]),2)
                		               endelse
                        
                        		       state.prom_config[i,indx_prom_config]=state.prom_config[i,indx_prom_config]+' '+shl_lab[k]+str_occup
	                        	endif
				endfor
                        endif
			parity[i,indx_prom_config]=0
			for iii=0,num_shls-1 do parity[i,indx_prom_config]=parity[i,indx_prom_config]+lvalue[iii]*tmp_occup_store[i,ii,iii]				
			parity[i,indx_prom_config]=parity[i,indx_prom_config] mod 2
                        
			state.prom_config_tmp[i,indx_prom_config]=str_conf_store[i,ii]
			len=strlen(strtrim(state.prom_config[i,indx_prom_config],2))
			state.prom_lastl[i,indx_prom_config]=strmid(strtrim(state.prom_config[i,indx_prom_config],2),len-2,1)
			state.ioccup_store[i,indx_prom_config,*]=tmp_occup_store[i,ii,*]
			state.prom_pars[i,indx_prom_config]=parity[i,ii]
			ioccup_tmp[*]=state.ioccup_store[i,indx_prom_config,*]
			state.prom_nlev[i,indx_prom_config]=h2nlev(ioccup_tmp)
			indx_prom_config=indx_prom_config+1
		endif
	endfor
	state.nconf[i]=indx_prom_config-1
endfor
	



END
