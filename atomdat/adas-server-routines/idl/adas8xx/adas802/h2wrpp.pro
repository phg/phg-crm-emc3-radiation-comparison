pro h2wrpp, data, path=path

   if not keyword_set(path) then begin
        path='./'
   endif   
   
   if strmid(path,strlen(path)-1) ne '/' then path=path+'/'

   for i=data.ion_min,data.ion_max do begin
       get_lun,lun
       name=''
       name=path+strtrim(string(data.el_sym),2)+'_'+strtrim(string(i),2)+'_pp.dat'
       openw,lun,name

       ionpotcm=data.ionpot[i]*8061.


        printf,lun,'1'
        printf,lun,'Allan Whiteford'
        printf,lun,'15/12/79'
        printf,lun,'           1'
        printf,lun,'C    Comments are for losers!'
        printf,lun," &FILES ifgfile = '"+strtrim(string('./'+'ifg#'+string(data.el_sym)+'_'+strtrim(string(i),2)+'_adf34.dat'),2)+"' , outfile = '"+strtrim(string('./'+strtrim(string(data.el_sym),2)+'_'+strtrim(string(i),2)+'_adf04.dat'),2)+"' &END" 
        printf,lun," &OPTIONS  ip =   "+strtrim(string(ionpotcm),2)+", coupling = 'IC' ,  aval = 'YES' ,  isonuclear = 'NO', lweight = 'NO' ,  comments = 2,  numtemp = 14, &END" 
        printf,lun,' 1  2  3  4  5  6  7  8  9 10 11 12 13 14'

       free_lun, lun
   endfor 




end
