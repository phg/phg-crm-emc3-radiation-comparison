; Copyright (c) 1999, Strathclyde University.
; SCCS info: Module @(#)adas802.pro	1.3 Date 11/10/00
;+
; PROJECT:
;       ADAS 
;
; NAME:
;	ADAS802
;
; PURPOSE:
;	The highest level routine for the ADAS 802 program.
;
; EXPLANATION:
;	This routine is called from the main adas system routine, adas.pro,
;	to start the ADAS 802 application.  Associated with adas802.pro
;	is a FORTRAN executable.  The IDL code provides the user
;	interface and output graphics whilst the FORTRAN code reads
;	in data files, performs numerical processing and creates the
;	output files.  The IDL code communicates with the FORTRAN
;	executable via a bi-directional UNIX pipe.  The unit number
;	used by the IDL for writing to and reading from this pipe is
;	allocated when the FORTRAN executable is 'spawned' (see code
;	below).  Pipe communications in the FORTRAN process are to
;	stdin and stdout, i.e streams 5 and 6.
;
;	The FORTRAN code is an independent process under the UNIX system.
;	The IDL process can only exert control over the FORTRAN in the
;	data which it communicates via the pipe.  The communications
;	between the IDL and FORTRAN must be exactly matched to avoid
;	input conversion errors.  The correct ammounts of data must be
;	passed so that neither process 'hangs' waiting for communications
;	which will never occur.
;
;	The FORTRAN code performs some error checking which is
;	independent of IDL.  In cases of error the FORTRAN may write
;	error messages.  To prevent these error messages from conflicting
;	with the pipe communications all FORTRAN errors are written to
;	output stream 0, which is stderr for UNIX.  These error messages
;	will appear in the window from which the ADAS session/IDL session
;	is being run.
;
;	In the case of severe errors the FORTRAN code may terminate
;	itself prematurely.  In order to detect this, and prevent the
;	IDL program from 'hanging' or crashing, the IDL checks to see
;	if the FORTRAN executable is still an active process before
;	each group of pipe communications.  The process identifier
;	for the FORTRAN process, PID, is recorded when the process is
;	first 'spawned'.  The system is then checked for the presence
;	of the FORTRAN PID.
;
; USE:
;	First the system settings must be established by calling
;	adas_sys_set.pro then adas802.pro is called to start the
;	ADAS 802 application;
;
;	adas_sys_set, adasrel, fortdir, userroot, centroot, $
;		      devlist, devcode, font_large, font_small, edit_fonts
;	adas802,   adasrel, fortdir, userroot, centroot, devlist, $
;		   devcode, font_large, font_small, edit_fonts
;
; INPUTS:
;	ADASREL - A string indicating the ADAS system version, 
;		  e.g ' ADAS RELEASE: ADAS93 V1.4'.  The first
;		  character should be a space.
;
;	FORTDIR - A string holding the path to the directory where the
;		  FORTRAN executables are, e.g '/disk/adas/fortran/exec'
;
;	USERROOT - A string holding the path to the root directory of
;		   the user's adas data. e.g '/disk/bowen/adas'
;		   This root directory will be used by adas to construct
;		   other path names.  For example the users default data
;		   for adas205 should be in /disk/bowen/adas/adf04.  In
;		   particular the user's default interface settings will
;		   be stored in the directory USERROOT+'/defaults'.  An
;		   error will occur if the defaults directory does not
;		   exist.
;
;	CENTROOT - Like USERROOT, but this directory points to the
;		   central data area for the system.  User defaults are
;		   not stored on CENTROOT.
;
;	FONT_LARGE - The name of a larger font e.g 'courier_bold14'
;
;	FONT_SMALL - The name of a smaller font e.g 'courier_bold12'
;
;	EDIT_FONTS - A structure of two fonts used in the table editor
;		     adas_edtab.pro, {font_norm:'',font_input:''}.
;		     The two fonts are used to differentiate between
;		     editable and non-editable parts of the table. You
;		     may have to experiment to get two fonts which give
;		     the same default spacing in the edit widget.
;		     e.g {font_norm:'helvetica_bold14', $
;			  font_input:'helvetica_oblique14'}
;
; OPTIONAL INPUTS:
;	None
;
; OUTPUTS:
;	None - Note this routine should leave all inputs un-modified.
;
; OPTIONAL OUTPUTS:
;	None
;
; KEYWORD PARAMETERS:
;	None
;
; CALLS:
;	FIND_PROCESS	Checks to see if a given process is active.
;       XXDATE          Get date and time from operating system.
;
; SIDE EFFECTS:
;	This routine spawns a FORTRAN executable.  Note the pipe 
;	communications routines listed above.  In addition to these
;	pipe communications there is one explicit communication of the
;	date to the FORTRAN code, search for 'printf, pipe' to find it.
;	There are also communications of the variable gomenu to the
;	FORTRAN which is used as a signal to stop the program in its
;	tracks and return immediately to the series menu. Do the same
;	search as above to find the instances of this.
;
; CATEGORY:
;	Adas system.
;	
; WRITTEN:
;       Martin O'Mullane
;
; MODIFIED:
;	1.1	Martin O'Mullane
;		First release 
;	1.2	Richard Martin
;		Increased version no. to 1.2
;	1.3	Richard Martin
;		Increased version no. to 1.3
;
; VERSION:
;       1.1	17-03-99
;	1.2	21-03-00
;	1.3	10-11-00
; 
;-
;-----------------------------------------------------------------------------

PRO ADAS802,	adasrel, fortdir, userroot, centroot, 			$
		devlist, devcode, font_large, font_small, edit_fonts

		;************************
		;**** Initialisation ****
		;************************

    adasprog = ' PROGRAM: ADAS802 V1.0'
    lpend    = 0
    gomenu   = 0
    deffile  = userroot+'/defaults/adas802_defaults.dat'
    bitfile  = centroot+'/bitmaps'
    passdir  = userroot+'/pass/'
    device   = ''
  
    dumpfile = userroot+'/pass/adas802_dump.out'


    sorry = ['This option is currently unavailable.',  $
             'Perhaps in the next release!         '  ]
		
                
                ;***************************************
		;**** We need the IDL version also. ****
		;***************************************
                
    idl_ver = strmid(!version.release,0,1)
    defsysv,'!except',1


;	if not keyword_set(ionisation) then begin
;		ionisation=0
;	endif
		ionisation=1
	
	cadwion_on=ionisation


		;******************************************
		;**** Search for user default settings ****
		;**** If not found create defaults     ****
		;**** inval: settings for data files   ****
		;**** procval: settings for processing ****
		;**** outval: settings for output      ****
		;******************************************
                
    tfont_struct    = { ef,                                         $
                        font_norm   :   edit_fonts.font_norm,       $
                        font_input  :   edit_fonts.font_input }

    files = findfile(deffile)
    
    if files(0) eq deffile then begin
    
        restore, deffile
         
    endif else begin
    
        inval =	{ adf00_root: '', $
                  te_04:fltarr(14), $
                  te_11:fltarr(14), $
                  te_15:fltarr(14), $
                  nte_04:0, $
                  nte_11:0, $
                  nte_15:0, $
                  te_unit_04:2, $
                  te_unit_11:2, $
                  te_unit_15:2, $
                  nuc_ch:74, $
                 el_sym:'w', $
                 ion_min:0, $
                 ion_max:20 $
                  }
                    
        procval = {new        	:	0,              $
                TITLE	   	:	'',             $
		nuc_ch	   	:	74,   		$
		el_sym	  	:	'w', 		$
  		ion_min	  	:	0, 		$
 		ion_max	  	:       20,		$
		nmindn		:	-1,	    	$
		nmaxdn		:	1,	     	$
		nmaxl		:	1,	     	$
		nminl		:	1,	     	$
		nmindn_1	:	-1,	     	$
		nmaxdn_1	:	1,	     	$
		nmaxl_1		:	1,	     	$
		nminl_1		:	1,	     	$
		nmindn_2	:	1,	     	$
		nmaxdn_2	:	-1,	     	$
		nmaxl_2		:	1,	     	$
		nminl_2		:	1,		$
		iprom		:	1,		$
		sel_but		:	0,		$
                ionsn_mn	:	'0',		$
                ionsn_mx	:	'0',		$
                ionsn_d_mnshl	:	0,		$
                ionsn_ind_mnshl	:	0,		$
                ionsn_ind_mxshl:	1,		$
                ionsn_ind_mxl	:	1,		$
                ionsn_pe_vac	:	0,		$
                ionsn_pe_d_mnshl:	0,		$
                ionsn_pe_d_mxshl:	1,		$
                ionsn_pe_d_mxl	:	1,		$
                ionsn_pe_ind_mnshl:	0,		$
                ionsn_pe_ind_mxshl:	1,		$
                ionsn_pe_ind_mxl:	1}
        
        outval = {      texout          :       0,                      $
                        texapp          :       -1,                     $
                        texrep          :       0,                      $
                        texdsn          :       '',                     $
                        texdef          :       'paper.txt',            $
                        texmes          :       '',			$
                        adfout          :       0,                      $
                        adfapp          :       -1,                     $
                        adfrep          :       0,                      $
                        adfdsn          :       '',                     $
                        adfdef          :       userroot+'/pass802',      $
                        adfmes          :       ''			}
    end


                
		;************************************************
		;**** Get date and write to fortran via pipe ****
		;************************************************

    date = xxdate()



LABEL100:

		;**** Check FORTRAN still running ****
  
		;************************************************
		;**** Communicate with bgspf0 in fortran and ****
		;**** invoke user interface widget for       ****
		;**** Data file selection                    ****
		;************************************************

     H2SPF0, INVAL, REP, OPTIONS,FORTDIR,FONT_LARGE=FONT_LARGE,FONT_SMALL=FONT_SMALL
;rep='who cares'

		;**** If cancel selected then end program ****

    if rep eq 'YES' then goto, LABELEND


LABEL200:

		;************************************************
		;**** Communicate with bgispf in fortran and ****
		;**** invoke user interface widget for       ****
		;**** processing options                     ****
		;************************************************

    selem = ''
    h2ispf, lpend, data, procval, 					  $
            gomenu, bitfile, passdir, fortdir, $
	    userroot, OPTIONS,						  $
            FONT_LARGE=font_large, FONT_SMALL=font_small, 		  $
            EDIT_FONTS=edit_fonts,cadwion_on=cadwion_on
 
		;**** If menu button clicked, tell FORTRAN to stop ****

        ;stop

        ;write_adf34,data

    if gomenu eq 1 then begin
 	goto, LABELEND
    endif else begin
; 	printf, pipe, 0
    endelse


		;**** If cancel selected then goto previous panel ****

    if lpend eq 1 then goto, LABEL100

                ;*************************************************
		;**** Fortran processing now in progress. Get ****
		;**** date and time for this run.	      ****
                ;*************************************************

    date = xxdate()

		;**** Create header for output. ****

    header = adasrel+adasprog+' DATE: '+date(0)+' TIME: '+date(1)



LABEL400:

		;************************************************
		;**** Communicate with bgspf1 in fortran and ****
		;**** invoke user interface widget for       ****
		;**** Output options                         ****
		;************************************************
    
    
    h2spf1, lpend, outval, dsninc, 		$ 
            header, bitfile, gomenu,			$
	    FONT=font_large

		;**** If menu button clicked, tell FORTRAN to stop ****

    if gomenu eq 1 then begin
;	printf, pipe, 1
        outval.texmes = ''
	goto, LABELEND
    endif else begin
;	printf, pipe, 0
    endelse

                    
                ;***********************************************
                ;**** If cancel selected then erase output  ****
                ;**** messages and goto process window 300. ****
                ;***********************************************

    if lpend eq 1 then begin
        outval.texmes = ''
        goto, LABEL200
    endif
    
    
                ;****************************************************
                ;**** Here we write the results of the error     ****
                ;**** analysis - depends on the analysis option. ****
                ;****************************************************

        
    if outval.texout eq 1 then begin        
        
        
	;FILL IN LATER WITH OUTPUT PROCESSING    
        
    
    endif
   
    if outval.adfout eq 1 then begin
    
    	file_acc, outval.adfdsn, exist, read, write, execute, filetype
	
	
	if exist eq 1 then begin
		if filetype eq '-' then begin
			a=dialog_message('Specified output is a file, you must pick a directory.')
		    	GOTO, LABEL400
		endif
	
		if write eq 0 or execute eq 0 then begin
			a=dialog_message('Cannot access directory.')
		    	GOTO, LABEL400
		end
	endif else begin
		spawn,'mkdir '+	outval.adfdsn
	endelse
	
	file_acc, outval.adfdsn, exist, read, write, execute, filetype
	
	
	if exist eq 1 then begin
		if write eq 0 or execute eq 0 then begin
			a=dialog_message('Cannot access directory.')
		    	GOTO, LABEL400
		end
	endif else begin
		a=dialog_message('Cannot create directory.')
		GOTO, LABEL400
	endelse
	
    
;        h2wr34,data,path=outval.adfdsn
;        h2wrin,data,path=outval.adfdsn
;        h2wrpp,data,path=outval.adfdsn
;        h2wrpd,data,path=outval.adfdsn
;        h2wr04,data,path=outval.adfdsn
	if cadwion_on eq 1 then begin
        	h2wrio,data,path=outval.adfdsn
        endif
    endif
   
   
                ;**************************************
                ;**** Back for more output options ****
                ;**************************************

    GOTO, LABEL400

LABELEND:

                ;*********************************************
                ;**** Ensure appending is not enabled for ****
                ;**** text output at start of next run.   ****
                ;*********************************************

    outval.texapp = -1


		;**** Save user defaults ****

    save, inval, procval, outval, filename=deffile

END
