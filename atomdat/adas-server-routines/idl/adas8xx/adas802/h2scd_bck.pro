pro h2scd,z,tev,edens,ionpot,ioccup,scd,ei

shmax=[2,2,6,2,6,10,2,6,10,14,2,6,10,14,18,2,6,10,14,18,22,$
	2,6,10,14,18,22,26,2,6,10,14,18,22,26,30]
lvalue=[0,0,1,0,1,2,0,1,2,3,0,1,2,3,4,0,1,2,3,4,5,$
	0,1,2,3,4,5,6,0,1,2,3,4,5,6,7]

nshellno=[1,2,2,3,3,3,4,4,4,4,5,5,5,5,5,6,6,6,6,6,6,$
	7,7,7,7,7,7,7,8,8,8,8,8,8,8,8]

;convert ionpot to ev

ionev=ionpot[z]

z1=z+1

grnd=ioccup[z,0,*]

for i=n_elements(grnd)-1,0,-1 do begin
	if grnd[i] gt 0 then begin
		orbital=i
		goto,foundit
	endif
end

foundit:

a=2.0
b=0.
c=0.

equiv=grnd[orbital]


; adas manual, equation 5.8.11, assume only ionise from highest shell

temper=[2.00D+04,5.00D+04,1.00D+05,2.00D+05,5.00D+05,1.00D+06,2.00D+06,5.00D+06 ,$       
1.00D+07,2.00D+07,5.00D+07,1.00D+08]

for ii=0,11 do begin
	tev=temper[ii]/11604.d0
	scd=1.42e-6 *(13.606 / tev)^(3./2.)*a*equiv*(expint(1,ionev/tev) * (tev/ionev) -b * exp(c) * expint(1,(ionev / tev + c) ) * (( c +tev / ionev)) )
; gives crap result because who knows what a, b and c are.

; Lets try BCHID expression one day
; BCHID call to assemble ionisation rate for each n-shell

;stop
	val_shl_indx=1
	sbchid=0.e0
	valnce_eav=0
	tmp_val1=0.d0
	for i=7,0,-1 do begin
		zeta=0
		indx=where(nshellno eq i+1)
		ioc_tmp=grnd[indx]
		d=where(ioc_tmp,count)
		if val_shl_indx eq 1 then begin 
			if count ne 0 then begin
				icount=0
				xi=0
				zeta=0
				tmp_val=0.e0
				for j=0,n_elements(ioc_tmp)-1 do begin
					if grnd[indx[j]] ne 0 then begin
						zeta=zeta+grnd[indx[j]]
						valnce_eav=valnce_eav+(2*lvalue[indx[j]]+1)*ei[0,indx[j]]
						icount=icount+(2*lvalue[indx[j]]+1)
					endif
				endfor
				valnce_eav=valnce_eav/icount
				xi=ionpot[z]
				val_shl_indx=0
				h2rbchid,z,xi,zeta,tev, tmp_val
				;stop
				sbchid=sbchid+tmp_val
				tmp_val1=tmp_val
			endif
		endif else begin
			if count ne 0 then begin
				totl=0
				xi=0.e0
				zeta=0
				tmp_val=0.e0
				for j=0,n_elements(ioc_tmp)-1 do begin
					if grnd[indx[j]] ne 0 then begin
						zeta=zeta+grnd[indx[j]]
						xi=xi+(2*lvalue[indx[j]]+1)*ei[0,indx[j]]
						totl=totl+(2*lvalue[indx[j]]+1)
					endif
				endfor
				xi=(valnce_eav-xi/totl)/13.606
				h2rbchid,z,xi,zeta,tev, tmp_val
				sbchid=sbchid+tmp_val
			endif
		endelse
	endfor
	print,tev*11604.d0,scd,sbchid, tmp_val1
endfor
stop
scd=sbchid
;print,'ECIP ionisation rate=',scd
end
