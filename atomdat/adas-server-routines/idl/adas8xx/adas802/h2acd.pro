pro h2acd,z,tev,edens,ionpot,ioccup,acd

shmax=[2,2,6,2,6,10,2,6,10,14,2,6,10,14,18,2,6,10,14,18,22,$
	2,6,10,14,18,22,26,2,6,10,14,18,22,26,30]

nshellno=[1,2,2,3,3,3,4,4,4,4,5,5,5,5,5,6,6,6,6,6,6,$
	7,7,7,7,7,7,7,8,8,8,8,8,8,8,8]


; convert ionpot to ev

ionev=ionpot[z]

z1=z+1


; Evaluate radiative recombination, adas manual equation 5.8.2

grnd=ioccup[z,0,*]

; assume recombination to highest shell
phfrac=0.
for i=n_elements(grnd)-1,0,-1 do begin
	if grnd[i] gt 0 then begin
		if grnd[i] eq shmax[i] then begin
			orbital=i+1
		endif else begin
			orbital=i
		endelse
		goto,foundit
	endif
end

foundit:

nshell=nshellno[orbital]

if phfrac eq 0 then begin

	totelec=total(shmax[(where(nshellno eq nshell))])
	numelec=total(ioccup[(where(nshellno eq nshell))])
	phfrac=1-numelec/totelec

endif

nmax=fix(126. * z1^(24./17.) * ((tev / 13.606) * (1/edens^2.))^(1./17.))

acdr=0

for n=nshell,nmax do begin
	arg=double(z1^2*13.606/(tev*n^2.))
	acdr=acdr+(2.*phfrac/n^3.)*(z1^2.*13.606/tev)*h2eei(arg)
	phfrac=1.
	;print,acdr,arg
end

acdr=acdr*2.6E-14*z1^2.*(13.606/tev)^(1./2.)

; hmm, add in DR one day

acdd=0.
;for i=0,1 do begin
;	deltae=?
;	occstr=?
;	deltan=?
;	y=(deltae / 13.606) / (z1+1)
;	nt=(((5.57^17) / edens) * z1^6. * (tev / 13.606)^(1./2.))^(1./7.)
;	a=1.0 + 0.015 *(z1^3 / (z1 +1)^2.)
;	if deltan eq 0 then begin
;		ay=y^(1./2.) / ( 2. + 0.105 * y + 0.015 * y^2)
;		d=nt / (200. + nt)
;	endif else begin
;		ay=y^(1./2.) / ( 2. + 0.41 * y + 0.006 * y^2)
;		d=0.0015 * ((z1+1)*nt)^2 / (1 + 0.0015 * ((z1+1) * nt)^(1./2.))
;	endelse
;	
;	acdd=acdd+occstr*ay*exp(deltae / (a*tev))
;end

;acdd=acdd*(4.78e-11)*((z1^(1./2.))*((z1+1)^(5./2.)) / (z1^2.+13.4)^(1./2.))*(13.606 / tev)^(3./2.)*d

acdd=0.

acd=acdr+acdd

;stop

end
