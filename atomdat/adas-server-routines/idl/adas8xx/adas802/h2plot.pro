pro h2plot,dataptrs,xlog=xlog,ylog=ylog,xrange=xrange,yrange=yrange,state=state,fill=fill,boxes=boxes,dashedbox=dashedbox,line=line,ion_ch=ion_ch,ion_min=ion_min,ion_max=ion_max,currentbox=currentbox

	if !D.name ne 'PS' then device,get_visual_name=colourtype else colourtype=''

        if colourtype eq 'TrueColor' then begin
                print,'TrueColour Display Detected'
                red=255L
                blue=(256L^2)*255L
                green=256L*255L
                white=255L+256L*(255L + 256L*255L)
                black=0L
                yellow=256L*(255L+256L*255L)
        endif else begin
                print,'Loadable Colour Table Display Detected'
                tvlct,255,255,255,1
		tvlct,0,0,255,2
		tvlct,255,0,0,3
		tvlct,0,255,0,4
		tvlct,0,255,255,5
		tvlct,0,0,0,6
                white=1
                red=3
                blue=2
                green=4
                black=6
                yellow=5        
        endelse

        if not keyword_set(currentbox) then begin
                if keyword_set(state) then begin
                        widget_control,state.select_but[0],get_uvalue=currentbox
                endif else begin
                        currentbox=-1
                endelse        
        endif


        if not keyword_set(ion_ch) then begin
                if keyword_set(state) then begin
                        widget_control, state.txt_conf1,get_value=temp
	                if temp[0] eq '' or temp[0] eq ' ' or temp[0] eq '  ' then begin
				ion_ch=-1		
			endif else begin
				ion_ch=fix(temp(0))
			endelse
                endif else begin
                        ion_ch=-1
                endelse
        endif
        
        if not keyword_set(ion_min) then begin
                if keyword_set(state) then begin
	                ion_min=state.ion_min
                endif else begin
                        ion_min=ion_ch
                endelse  
        endif

        if not keyword_set(ion_max) then begin
                if keyword_set(state) then begin
	                ion_max=state.ion_max
                endif else begin
                        ion_max=ion_ch
                endelse  
        endif
        
        if keyword_set(state) then begin
                widget_control,state.plot_wid,get_value=wid
                wset,wid
        endif
        
        print,ion_ch,ion_min,ion_max
        
        if ion_min eq -1 or ion_max eq -1 then return

        ;if keyword_set(state) then begin
        ;        if state.gotpreviewdata[ion_ch] eq 0 then return
        ;end
        
        if not keyword_set(dashedbox) then dashedbox=[-1,-1,-1,-1]
       
        if not keyword_set(xrange) then begin
                if keyword_set(state) then begin
                        widget_control,state.lmin,get_value=a
                        min=float(a)
                        widget_control,state.lmax,get_value=a
                        max=float(a)
                        xrange=[min,max]
                endif else begin
                        xrange=[0,0]
                endelse
        endif
        
        if not keyword_set(yrange) then begin
                if keyword_set(state) then begin
                        widget_control,state.cmin,get_value=a
                        min=float(a)
                        widget_control,state.cmax,get_value=a
                        max=float(a)                
                        yrange=[min,max]
                endif else begin
                        yrange=[0,0]
                endelse
        endif
        
        
        if not keyword_set(xlog) then begin
                if keyword_set(state) then begin
                        widget_control,state.xlogon,get_uvalue=xlog
                endif else begin
                        xlog=0
                endelse                
        endif
        
        if not keyword_set(ylog) then begin
                if keyword_set(state) then begin
                        widget_control,state.ylogon,get_uvalue=ylog
                endif else begin
                        ylog=0
                endelse
        endif

        xdata=dblarr(1)
        ydata=dblarr(1)
        highlight=intarr(1)
        for plotion=ion_min,ion_max do begin
                if not keyword_set(state) or state.gotpreviewdata[plotion] eq 1 then begin
                
                        data=*(dataptrs[plotion])
        
                        if (where(data.aval gt 0))[0] ne -1 then begin
                                xdata=[xdata,data.wavel[where(data.aval gt 0)]]
                                ydata=[ydata,data.aval[where(data.aval gt 0)]]
                                if (size(where(data.aval gt 0)))[0] ne -1 then begin
                                        a=lonarr((size(where(data.aval gt 0)))[1])
                                        a[*]=plotion eq ion_ch ? 1:0
                                endif
                                highlight=[highlight,a]
                        
                        endif
                endif
        end

        help,xdata,ydata
        
;        stop

        if (size(xdata))[1] eq 1 then return

        xdata=xdata[1:(size(xdata))[1]-1]
        ydata=ydata[1:(size(ydata))[1]-1]
        highlight=highlight[1:(size(highlight))[1]-1]

        plot,xdata,ydata,xlog=xlog,ylog=ylog,xrange=xrange,yrange=yrange,/nodata,color=black,background=white

;        data=*(dataptrs[ion_ch])

        ;stop


        if keyword_set(boxes) then begin
                cmin=boxes.cmin
                lmin=boxes.lmin
                lmax=boxes.lmax
        
        endif else if keyword_set(state) then begin
                cmin=dblarr(1)
                lmin=dblarr(1)
                lmax=dblarr(1)
                highlightbox=intarr(1)
                for i=0,(size(state.select_lmin))[1]-1 do begin
                        widget_control,state.select_onoff[i],get_uvalue=onoff
                        if onoff eq 1 then begin
                                widget_control,state.select_cmin[i],get_value=a
                                cmin=[cmin,double(a[0])]
                                widget_control,state.select_lmin[i],get_value=a
                                lmin=[lmin,double(a[0])]
                                widget_control,state.select_lmax[i],get_value=a
                                lmax=[lmax,double(a[0])]
                                highlightbox=[highlightbox,i eq currentbox ? 1:0]
                        endif                
                end
                if (size(cmin))[1] eq 1 then begin
                        cmin=-1
                endif else begin
                        cmin=cmin[1:(size(cmin))[1]-1]
                        lmin=lmin[1:(size(lmin))[1]-1]
                        lmax=lmax[1:(size(lmax))[1]-1]
                        highlightbox=highlightbox[1:(size(highlightbox))[1]-1]
                endelse
        endif else cmin=-1


        if keyword_set(fill) then begin
                if (size(cmin))[0] eq 1 then for i=0,(size(cmin))[1]-1 do polyfill,[lmin[i],lmin[i],lmax[i],lmax[i]],[!y.type eq 1 ? 10.^!y.crange[1]:!y.crange[1],cmin[i],cmin[i],!y.type eq 1 ? 10.^!y.crange[1]:!y.crange[1]],pattern=bindgen(10,10)
        endif
        if (size(cmin))[0] eq 1 then for i=0,(size(cmin))[1]-1 do plots,[lmin[i],lmin[i]],[!y.type eq 1 ? 10.^!y.crange[1]:!y.crange[1],cmin[i]],linestyle=((dashedbox[0] eq i and dashedbox[1]) eq 1 ? 1:0),color=highlightbox[i] eq 1 ? green:yellow
        if (size(cmin))[0] eq 1 then for i=0,(size(cmin))[1]-1 do plots,[lmin[i],lmax[i]],[cmin[i],cmin[i]],linestyle=((dashedbox[0] eq i and dashedbox[3] eq 1) ? 1:0),color=highlightbox[i] eq 1 ? green:yellow
        if (size(cmin))[0] eq 1 then for i=0,(size(cmin))[1]-1 do plots,[lmax[i],lmax[i]],[cmin[i],!y.type eq 1 ? 10.^!y.crange[1]:!y.crange[1]],linestyle=((dashedbox[0] eq i and dashedbox[2]) eq 1 ? 1:0),color=highlightbox[i] eq 1 ? green:yellow


        if (size(cmin))[0] eq 1 then for i=0,(size(cmin))[1]-2 do print,lmin[i],lmax[i],cmin[i],'linestyle=',((dashedbox[0] eq i and dashedbox[3] eq 1) ? 1:0)

;        stop

        if (where(highlight eq 0))[0] ne -1 then for i=0,(size(where(highlight eq 0)))[1]-1 do plots,[xdata[(where(highlight eq 0))[i]],xdata[(where(highlight eq 0))[i]]],[!y.type eq 1 ? 10.^!y.crange[0]:!y.crange[0],ydata[(where(highlight eq 0))[i]] gt (!y.type eq 1 ? 10.^!y.crange[0]:!y.crange[0]) ? ydata[(where(highlight eq 0))[i]] : (!y.type eq 1 ? 10.^!y.crange[0]:!y.crange[0])],color=blue
        if (where(highlight eq 1))[0] ne -1 then for i=0,(size(where(highlight eq 1)))[1]-1 do plots,[xdata[(where(highlight eq 1))[i]],xdata[(where(highlight eq 1))[i]]],[!y.type eq 1 ? 10.^!y.crange[0]:!y.crange[0],ydata[(where(highlight eq 1))[i]] gt (!y.type eq 1 ? 10.^!y.crange[0]:!y.crange[0]) ? ydata[(where(highlight eq 1))[i]] : (!y.type eq 1 ? 10.^!y.crange[0]:!y.crange[0])],color=red
        

;        for i=0,(size(xdata))[1]-1 do plots,[xdata[i],xdata[i]],[!y.type eq 1 ? 10.^!y.crange[0]:!y.crange[0],ydata[i] gt (!y.type eq 1 ? 10.^!y.crange[0]:!y.crange[0]) ? ydata[i] : (!y.type eq 1 ? 10.^!y.crange[0]:!y.crange[0])];,color=256

        if keyword_set(line) then begin
                plots,[line[0],line[2]],[line[1],line[3]]
        endif

end

