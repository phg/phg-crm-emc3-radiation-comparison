pro h2ionbal,state,fracabun,tev,edens

	acd=dblarr(state.iz0)
	scd=dblarr(state.iz0)

	for ion_ch=state.ion_min,state.ion_max do begin
	if total(state.ioccup_store[ion_ch,0,*]) gt 0 and state.gotatomdata[ion_ch] gt 0 then begin
       		h2acd,ion_ch,tev,edens,state.ionpot,state.ioccup_store,temp
	       	acd[ion_ch]=temp
		ei=(*state.atomdata[ion_ch]).ei
		h2scd,ion_ch,tev,edens,state.ionpot,state.ioccup_store,temp,ei
		scd[ion_ch]=temp
	endif else begin
		acd[ion_ch]=0
		scd[ion_ch]=0
	endelse
	end


	;print,acd
	;print,scd

	; so, we have some acd and scd coefficients
	; I'd bet a lot of money they are exceptionally crap
	
	; lets find the state with acd ~ scd
	
	junk=min(abs(acd-scd),central)
	
	;print,'central stage is ', central
	
	fracabun=dblarr(state.iz0)
	
	fracabun[central]=1.
	
	for i=central+1,state.ion_max do begin
		fracabun[i]=fracabun[i-1]*scd[i-1]/acd[i]
	end

	for i=central-1,state.ion_min,-1 do begin
		fracabun[i]=fracabun[i+1]*acd[i+1]/scd[i]	
	end

	;plot,fracabun,/ylog

	;stop

end
