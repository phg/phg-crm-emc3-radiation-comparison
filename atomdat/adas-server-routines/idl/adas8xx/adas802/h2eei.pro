function h2eei,x

;copied from central adas eei.for routine

     IF (X LE 1.0D0) THEN begin

         A = -aLOG(X) - 0.57721566D0 + X * (0.99999193D0 - X *   $
            (0.24991055D0 - X * (0.05519968D0 - X *   		$
            (0.00976004D0 - X * 0.00107857D0))))		
         Y = 0.5D0 * X						
         Z = 1.0D0 - Y * (0.9998684D0 - Y * (0.4982926D0 - Y *	$
            (0.1595332D0 - Y * 0.0293641D0)))			
         EEI = A / (Z * Z)					
	endif else begin
	
        EEI = (0.2677737343D0 + X * (8.6347608925D0 + X *		$
              (18.059016973D0 + X * (8.5733287401D0 + X)))) /		$
              (X * (3.9584969228D0 + X * (21.0996530827D0 + X *		$
              (25.6329561486D0 + X * (9.5733223454D0 + X)))))		

      ENDelse

	return,eei

end
