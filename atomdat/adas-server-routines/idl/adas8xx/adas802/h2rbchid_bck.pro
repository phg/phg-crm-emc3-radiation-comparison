pro h2rbchid,z,xi,zeta,tev, sbchid

;  evaluates a shell contribution to the ionisation rate coefficient
;  in the burgess-chidichimo approximation  mnras(1983)203,1269.
;      z=target ion charge number
;      xi_ryd=effective ionisation potential for shell (ryd)
;      zeta=effective number of equivalent electrons in shell
;      tev=electron temperature (ev)
; IDL version of the adaslib routine rbchid,for
;
; date:    6th July 2002
;
; version: 1.1				date: 06-06-02
;
; author: S.D. Loch

;sbchid=0.d0
;c = 2.3d0
;beta = 0.25d0*(sqrt((100.d0*z+91.d0)/(4.d0*z+3.d0))-5.d0)
;ate=1.5789d5/tev/11604.d0
;y=ate*xi_ryd
;if y le 150.d0 then begin
;	t1=zeta*sqrt(y)*exp(-y)*expint(1,y)/(xi_ryd^(1.5d0))
;	p=1.d0+1.d0/y
;	w=(alog(p))^(beta/p)
;	sbchid=2.1715d-8*c*t1*w
;endif
xi_ryd=xi/13.606d0
sbchid=0.0d0
c=2.3d0
beta=0.25d0*(sqrt((100.0d0*z+91.0)/(4.0d0*z+3.0d0))-5.0d0)
ate=1.5789d5/tev/11604.d0
y=ate*xi_ryd
if y lt 150.0d0 then begin
	t1=zeta*sqrt(y)*expint(1,y,/double)/(xi_ryd^1.5d0)
	p=1.0d0+1.0d0/y
	w=(alog(p))^(beta/p)
	sbchid=2.1715d-8*c*t1*w
endif

end
