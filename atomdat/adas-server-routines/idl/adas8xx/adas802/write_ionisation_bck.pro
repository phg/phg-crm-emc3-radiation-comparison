PRO WRITE_ionisation, data, path=path
;-------------------------------------------------------------------------------
;routine to write driver files for CA ionisation codes
;-------------------------------------------------------------------------------
stop
num_shls=36
shl_lab=['1s','2s','2p','3s','3p','3d','4s','4p','4d','4f','5s','5p',$
'5d','5f','5g','6s','6p','6d','6f','6g','6h','7s','7p','7d','7f','7g',$
'7h','7i','8s','8p','8d','8f','8g','8h','8i','8k']
shmax=[2,2,6,2,6,10,2,6,10,14,2,6,10,14,18,2,6,10,14,18,22,$
2,6,10,14,18,22,26,2,6,10,14,18,22,26,30]
first_array=[0,1,1,3,3,5,5,5,8,8,8,8,8,8,8,8,8,17,17,17,17,17,17,17,17,$
17,17,17,17,17,17,17,17,17,17,17]
lvalue=[0,0,1,0,1,2,0,1,2,3,0,1,2,3,4,0,1,2,3,4,5,0,1,2,3,$
4,5,6,0,1,2,3,4,5,6,7]

   parity=intarr(100)
   order=intarr(100)
   config_list=strarr(100)
   if not keyword_set(path) then begin
        path='./'
   endif   
   
   if strmid(path,strlen(path)-1) ne '/' then path=path+'/'
   
   for i=data.ion_min,data.ion_max do begin
   	dirct_str=path+strtrim(string(data.el_sym),2)+'_'+strtrim(string(i),2)+'_ionsn_dirct_'
	indirct_str=path+strtrim(string(data.el_sym),2)+'_'+strtrim(string(i),2)+'_ionsn_indirct_'
	rcg_str=path+strtrim(string(data.el_sym),2)+'_'+strtrim(string(i),2)+'_di_'
;-------------------------------------------------------------------------------
; convert ground configuration to occupation array
;-------------------------------------------------------------------------------
   	iflag=0
	ionsn_occup=intarr(num_shls)
	confg=' '+strtrim(data.configuration[i,0],2)
	adjust=0
	if fix(strlen(confg)) mod 5 ne 0 then adjust=1
	len_confg=fix(fix(strlen(confg))/5)+adjust
	first_shl=strtrim(strmid(confg,0,3),2)
	for j=0,num_shls-1 do begin
		if first_shl eq shl_lab[j] then indx=j
	endfor
	for j=0,indx-1 do begin
		ionsn_occup[j]=shmax[j]
	endfor
	for j=0,len_confg-1 do begin
		shl=strmid(confg,5*j,3)
		for k=0,num_shls-1 do begin
			if strtrim(string(shl),2) eq shl_lab[k] then indx=k
		endfor
		ionsn_occup[indx]=fix(strtrim(strmid(confg,5*j+3,2),2))
	endfor
	for j=0,num_shls-1  do begin
		if ionsn_occup[j] ne 0 then max_shl=j
		if ionsn_occup[j] ne shmax[j] and iflag eq 0 then begin
			non_full=j
			iflag=1
		endif
	endfor
	first_write=first_array[non_full]
	min_shl=max_shl-3
	if min_shl lt 0 then min_shl=0
	if min_shl le first_write then first_write=first_array[min_shl]
	
;-------------------------------------------------------------------------------
; evaluate possible direct ionisation routes
;-------------------------------------------------------------------------------
	icount=1
	config_grnd=''
	for k=first_write,max_shl do begin
		if ionsn_occup[k] ne 0 then begin
			if ionsn_occup[k] lt 10 then begin
				str_occup=strtrim(string(ionsn_occup[k]),2)+' '
			endif else begin
				str_occup=strtrim(string(ionsn_occup[k]),2)
			endelse
			config_grnd=config_grnd+' '+shl_lab[k]+str_occup
		endif
	endfor
	dumstr_grnd=strtrim(config_grnd,2)
	lendumstr_grnd=string(strlen(dumstr_grnd))
	lenleft_grnd=string(42-strlen(dumstr_grnd))

	for j=max_shl, min_shl,-1  do begin
		if ionsn_occup[j]-1 ge 0 then begin
			parity[j]=0
			ionsn_occup[j]=ionsn_occup[j]-1
			config=''
			for k=first_write,max_shl do begin
				if ionsn_occup[k] ne 0 then begin
					if ionsn_occup[k] lt 10 then begin
						str_occup=strtrim(string(ionsn_occup[k]),2)+' '
					endif else begin
						str_occup=strtrim(string(ionsn_occup[k]),2)
					endelse
					config=config+' '+shl_lab[k]+str_occup
					parity[j]=parity[j]+lvalue[k]*ionsn_occup[k]
				endif
			endfor
			parity[j]=parity[j] mod 2
			config_list[j]=config
			ionsn_occup[j]=ionsn_occup[j]+1
		endif
	endfor	
;-------------------------------------------------------------------------------
; write direct ionisation files and rcg input files for level energy calcn
;-------------------------------------------------------------------------------
	get_lun,lun_rcg1
	get_lun,lun_rcg2
	name_rcg1=''
	name_rcg1=rcg_str+'rcg_1.dat'
	name_rcg2=''
	name_rcg2=rcg_str+'rcg_2.dat'
	openw,lun_rcg1,name_rcg1
	openw,lun_rcg2,name_rcg2
	printf,lun_rcg1,'2  -5    2   10  1.0    5.d-09    5.d-11-2  0130    1.0 0.65  0.0  0.5'
	printf,lun_rcg2,'2  -5    2   10  1.0    5.d-09    5.d-11-2  0130    1.0 0.65  0.0  0.5'
	printf,lun_rcg1, data.iz0, i+1, strtrim(data.el_sym+'+'+strtrim(string(i),2),2), 'ground      ',dumstr_grnd,$
		format='(3x, i2, 3x, i2, 1x, a5, 1x, a12,a'+string(lendumstr_grnd)+',a'+string(lenleft_grnd)+')'
	icount_order=0
	for j=max_shl,min_shl,-1 do begin
		if parity[j] eq parity[max_shl] then begin
			order[icount_order]=j
			icount_order=icount_order+1
			get_lun,lun
			name=''
			name=dirct_str+strtrim(string(icount),2)+'.dat'
			openw,lun,name
			if i+1 eq 1 then begin
				printf,lun,'200-51 1 2  01.   1.   5.0E-08   1.0E-11-2  0130 0 1.00 0.6510.00 0.5       0.70'
			endif else begin
				printf,lun,'200-51 1 2  01.   1.   5.0E-08   1.0E-11-2  0130 0 1.00 0.6560.00 0.5       0.70'		
			endelse
			printf,lun, data.iz0, i+1, strtrim(data.el_sym+'+'+strtrim(string(i),2),2), 'ground      ',dumstr_grnd,shl_lab[j],$
			format='(3x, i2, 3x, i2, 1x, a5, 1x, a12,a'+string(lendumstr_grnd)+',a'+string(lenleft_grnd)+',a2)'
			dumstr=strtrim(config_list[j],2)
			lendumstr=string(strlen(dumstr))
			lenleft=string(42-strlen(dumstr))		
			printf,lun, data.iz0, i+2, strtrim(data.el_sym+'+'+strtrim(string(i+1),2),2), 'I from '+shl_lab[j]+'   ',dumstr,$
			format='(3x, i2, 3x, i2, 1x, a5, 1x, a12,a'+string(lendumstr)+',a'+string(lenleft)+')'
			icount=icount+1
			printf,lun,'   -1'
			close,lun
			free_lun, lun
			
			printf,lun_rcg2, data.iz0, i+2, strtrim(data.el_sym+'+'+strtrim(string(i+1),2),2), 'I from '+shl_lab[j]+'   ',dumstr,$
			format='(3x, i2, 3x, i2, 1x, a5, 1x, a12,a'+string(lendumstr)+',a'+string(lenleft)+')'
		endif
	endfor
	for j=max_shl,min_shl,-1 do begin
		if parity[j] ne parity[max_shl] then begin
			order[icount_order]=j
			icount_order=icount_order+1
			get_lun,lun
			name=''
			name=dirct_str+strtrim(string(icount),2)+'.dat'
			openw,lun,name
			if i+1 eq 1 then begin
				printf,lun,'200-51 1 2  01.   1.   5.0E-08   1.0E-11-2  0130 0 1.00 0.6510.00 0.5       0.70'
			endif else begin
				printf,lun,'200-51 1 2  01.   1.   5.0E-08   1.0E-11-2  0130 0 1.00 0.6560.00 0.5       0.70'		
			endelse
			printf,lun, data.iz0, i+1, strtrim(data.el_sym+'+'+strtrim(string(i),2),2), 'ground      ',dumstr_grnd,shl_lab[j],$
			format='(3x, i2, 3x, i2, 1x, a5, 1x, a12,a'+string(lendumstr_grnd)+',a'+string(lenleft_grnd)+',a2)'
			dumstr=strtrim(config_list[j],2)
			lendumstr=string(strlen(dumstr))
			lenleft=string(42-strlen(dumstr))		
			printf,lun, data.iz0, i+2, strtrim(data.el_sym+'+'+strtrim(string(i+1),2),2), 'I from '+shl_lab[j]+'   ',dumstr,$
			format='(3x, i2, 3x, i2, 1x, a5, 1x, a12,a'+string(lendumstr)+',a'+string(lenleft)+')'
			icount=icount+1
			printf,lun,'   -1'
			close,lun
			free_lun, lun

			printf,lun_rcg2, data.iz0, i+2, strtrim(data.el_sym+'+'+strtrim(string(i+1),2),2), 'I from '+shl_lab[j]+'   ',dumstr,$
			format='(3x, i2, 3x, i2, 1x, a5, 1x, a12,a'+string(lendumstr)+',a'+string(lenleft)+')'
		endif
	endfor
	printf,lun_rcg1,'   -1'
	printf,lun_rcg2,'   -1'
	close,lun_rcg1
	free_lun, lun_rcg1
	close,lun_rcg2
	free_lun, lun_rcg2
;-------------------------------------------------------------------------------
; evaluate indirect ionisation routes
;-------------------------------------------------------------------------------

	icount=1
	for val=0,icount_order-1 do begin
		j=order[val]
		if (ionsn_occup[j] -1) ge 0 then  begin
;-------------------------------------------------------------------------------
; write indirect ionisation files, files are written out in same parity order as 
; direct files.
;-------------------------------------------------------------------------------
		get_lun,lun
		name=''
		name=indirct_str+strtrim(string(icount),2)+'.dat'
		openw,lun,name
		if i+1 eq 1 then begin
			printf,lun,'20 -51 0 2   10  1.0    5.e-08    1.e-11-2   130    1.0 0.65  10.  0.5       0.7'
		endif else begin
			printf,lun,'20 -51 0 2   10  1.0    5.e-08    1.e-11-2   130    1.0 0.65  60.  0.5       0.7'
		endelse
		printf,lun, data.iz0, i+1, strtrim(data.el_sym+'+'+strtrim(string(i),2),2), 'ground      ',dumstr_grnd,shl_lab[j],$
			format='(3x, i2, 3x, i2, 1x, a5, 1x, a12,a'+string(lendumstr_grnd)+',a'+string(lenleft_grnd)+',a2)'
		ionsn_occup[j]=ionsn_occup[j]-1
		for k=j+1,num_shls-1 do begin
			if strmid(shl_lab[k],1,1) ne 'g' and strmid(shl_lab[k],1,1) ne 'h' $
				and strmid(shl_lab[k],1,1) ne 'i' and strmid(shl_lab[k],1,1) ne 'k'then begin
			ionsn_occup[k]=ionsn_occup[k]+1
			if ionsn_occup[k] le shmax[k] then begin
				config=''
				for l=first_write,num_shls-1 do begin
					if ionsn_occup[l] ne 0 then begin
						if ionsn_occup[l] lt 10 then begin
							str_occup=strtrim(string(ionsn_occup[l]),2)+' '
						endif else begin
							str_occup=strtrim(string(ionsn_occup[l]),2)
						endelse
						config=config+' '+shl_lab[l]+str_occup
					endif
				endfor
				dumstr=strtrim(config,2)
				lendumstr=string(strlen(dumstr))
				lenleft=string(42-strlen(dumstr))
				printf,lun, data.iz0, i+1, strtrim(data.el_sym+'+'+strtrim(string(i),2),2), 'I via  '+shl_lab[k]+'   ',dumstr,shl_lab[k],$
					format='(3x, i2, 3x, i2, 1x, a5, 1x, a12,a'+string(lendumstr)+',a'+string(lenleft)+',a2)'
			endif
			ionsn_occup[k]=ionsn_occup[k]-1
			endif
		endfor
		printf,lun,'   -1'
		close,lun
		free_lun, lun
		icount=icount+1
		ionsn_occup[j]=ionsn_occup[j]+1
		endif 		
	endfor
   endfor 

END
