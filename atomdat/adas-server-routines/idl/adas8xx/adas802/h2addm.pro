PRO h2addm, M1,M2, MULT_ADD, TERMS_ADD

; 	Adds momentum

	tmp_l1=fix(m1/100)*100
	tmp_l2=fix(m2/100)*100
	if tmp_l1 eq 0 then tmp_l1=100
	if tmp_l2 eq 0 then tmp_l2=100
	l1_val=m1 mod tmp_l1
	l2_val=m2 mod tmp_l2
	s1_val=fix((m1-l1_val)/100)
	s2_val=fix((m2-l2_val)/100)

	lmax=l1_val > l2_val
	lmin=l1_val < l2_val
	smax=s1_val > s2_val
	smin=s1_val < s2_val
	
	lindx=2*lmax+1
	larr=intarr(lindx)
	sindx=2*smax+1
	sarr=intarr(sindx)
	
	for i=0,lindx-1 do begin
		larr[i]=lmin+lmax-i
		if larr[i] lt (lmax-lmin) then begin
			lindx=i
			goto, jump1
		endif
	endfor
jump1:	
	for i=0,sindx-1 do begin
		sarr[i]=smin+smax-2*i
		if sarr[i] lt (smax-smin) then begin
			sindx=i
			goto, jump1
		endif
	endfor
jump2:	
	indx=0
	terms_tmp=intarr(lindx*sindx)
	for i=0,sindx-1 do begin
		for j=0,lindx-1 do begin
			terms_tmp(indx)=sarr[i]*100+larr[j]
			indx=indx+1
		endfor
	endfor
	
	terms_order=terms_tmp[sort(terms_tmp)]
	terms_add=terms_order[uniq(terms_order)]
	mult_add=intarr(n_elements(terms_add))
	for i = 0, n_elements(terms_add)-1 do begin
		mult_add[i]=n_elements(where(terms_order eq terms_add[i]))
	endfor
END
