; Copyright (c) 2000 Strathclyde University .
; SCCS info: Module @(#)$Header: /home/adascvs/idl/adas8xx/adas801/get_parity.pro,v 1.1 2004/07/06 13:58:31 whitefor Exp $ Date $Date: 2004/07/06 13:58:31 $
;+
; PROJECT:
;       ADAS 
;
; NAME:
;	GET_PARITY
;
; PURPOSE: 
;       Given a standard configureation return whether it is even or odd. 
;
;
; EXPLANATION:
;       Only need to check for odd l-values.
;
; NOTES:
;
;
; INPUTS:
;       cfg  -  A string containing the configuration eg '2s2  3p4'. Note
;               that they are defined as in the Cowan RCN input. 
;               ie orbital specification and occupation (a3,a2).
;
; OPTIONAL INPUTS:
;       None
;
; OUTPUTS:
;
; OPTIONAL OUTPUTS:
;       None
;
; KEYWORD PARAMETERS:
;
; CALLS:
;
; SIDE EFFECTS:
;
; CATEGORY:
;	
;
; WRITTEN:
;       Martin O'Mullane
;
; MODIFIED:
;	1.1	Martin O'Mullane
;		First release 
;
; VERSION:
;       1.1	12-10-99
;
;-
;-----------------------------------------------------------------------------


FUNCTION GET_PARITY, cfg_in

CATCH, error
IF error NE 0 THEN BEGIN
   print,'Not a valid configuration'
   RETURN, 'ERROR'
ENDIF

odd  = ['p','f','h','k','m','o','r','u','w','y']

; Convert to lowercase for the test

cfg = strlowcase(cfg_in)


; Find position of the odd orbitals. Note that strpos only returns the
; first instance of the find expression. The work around is as suggested
; in the IDL help. Also we cannot pass an array as the search expression.

parity='bbb'

for k = 0, n_elements(cfg)-1 do begin

   o_pos = -1
   for j = 0, n_elements(odd)-1 do begin
     i = 0
     while (i NE -1) do begin
        i = strpos(cfg[k],odd[j],i)
        o_pos = [o_pos, i]
        if i NE -1 then i = i+1
     endwhile
   endfor

   ind = where(o_pos NE -1, count)

   num = -1
   if count GT 0 then begin
      p = 0
      o_pos = o_pos(ind)
      for j=0, count-1 do begin
         reads, strmid(cfg[k], o_pos[j]+1,2), num, format='(i2)'
         p = p + (num mod 2)
      endfor
      if (p mod 2) GT 0 then parity=[parity,'ODD'] else parity=[parity,'EVEN']
   endif else begin
      parity=[parity,'EVEN']
   endelse

endfor

parity = parity[1:*]

if n_elements(parity) EQ 1 then parity = parity[0]

RETURN, parity

end
