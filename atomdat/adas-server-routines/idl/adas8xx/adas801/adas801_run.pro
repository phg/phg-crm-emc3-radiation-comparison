; Copyright (c) 2000 Strathclyde University .
; SCCS info: Module @(#)$Header: /home/adascvs/idl/adas8xx/adas801/adas801_run.pro,v 1.3 2004/07/06 11:05:44 whitefor Exp $ Date $Date: 2004/07/06 11:05:44 $
;+
; PROJECT:
;       ADAS 
;
; NAME:
;       ADAS801_RUN
;
; PURPOSE: 
;
;
; EXPLANATION:
;
;
; NOTES:
; 
;
;
; INPUTS:
;        outval       - A structure holding the 'remembered' outout options.
;                       appbut      : append to end of paper.txt
;                       repbut      : replace paper.txt
;                       filename    : name of paper.txt
;                       defname     : 'paper.txt' - the default!
;                       message     : error message for paper.txt
;
;         
;        bitfile      - directory of bitmaps for menu button
;
;
; OPTIONAL INPUTS:
;       None
;
; OUTPUTS:
;       The function returns a string rep
;           rep = 'CONTINUE' if everything is ok
;           rep = 'CANCEL' if there is a problem or indeed if the user
;                          closes the widget with the mouse. 
;           rep = 'MENU' if user want to exit to menu at this point
;
; OPTIONAL OUTPUTS:
;       None
;
; KEYWORD PARAMETERS:
;       FONT_LARGE - The name of a larger font e.g 'courier_bold14'
;
;       FONT_SMALL - The name of a smaller font e.g 'courier_bold12'
;
;
; CALLS:
;                            
;       ADAS801_RUN_ENTRY        : Reacts to events relevant to constructing
;                                  adf10 filenames.
;       ADAS801_RUN_NULL_EVENTS  : Captures events and does nothing (function)
;       ADAS801_RUN_NULLS        : Captures events and does nothing (procedure)
;       ADAS801_RUN_SWITCH       : Switches between 3 possible output options
;                                  which are mapped/unmapped here.
;       ADAS801_RUN_MENU         : React to menu button event. The standard IDL
;                                  reaction to button events cannot deal with
;                                  pixmapped buttons. Hence the special handler.
;       ADAS801_RUN_EVENT        : Reacts to cancel and Done. Does the file
;                                  existence error checking.
;       ADAS801_UPDATE_TYPE1     : Updates adf10 for datacalsses 0-6.
;       ADAS801_UPDATE_TYPE2     : Updates adf10 for datacalsses 7-9.
;
; SIDE EFFECTS:
;
; CATEGORY:
;       Widgets
;
; WRITTEN:
;       Martin O'Mullane
;
; MODIFIED:
;       1.1     Martin O'Mullane
;                - First release 
;       1.2     Martin O'Mullane
;                - Added buttons/event handler for iosnuclear style output. 
;       1.3     Martin O'Mullane
;                - Allow batch operation.  
;
; VERSION:
;       1.1     16-08-99
;       1.2     05-06-2000
;       1.3     19-06-2003
;
;-
;-----------------------------------------------------------------------------



PRO ADAS801_RUN_MENU, event

; The 'normal' get_value from a widget_button does not work with a
; bitmap button. Hence this specialised event handler.

   ; Get the info structure out of the user value of the top-level base.

Widget_Control, event.top, Get_UValue=info


; Something nefarious may cause an error also.
;  -- if so trap it and exit gracefully

CATCH, error
IF error NE 0 THEN BEGIN
   formdata = {cancel : 0, menu:1}
   *info.ptrToFormData = formdata
   Widget_Control, event.top, /Destroy
   message,!err_string
   RETURN
ENDIF


 formdata = {cancel : 0, menu:1}
 *info.ptrToFormData =formdata
 widget_Control, event.top, /destroy

END


;-----------------------------------------------------------------------------


PRO ADAS801_RUN_EVENT, event

; React to button events - Cancel and Done but not menu (this requires a
; specialised event handler ADAS801_RUN_MENU). Also deal with the passing
; directory output Default button here.

; On pressing Done check for the following


   ; Get the info structure out of the user value of the top-level base.

Widget_Control, event.top, Get_UValue=info

; Something nefarious may cause an error also.
;  -- if so trap it and exit gracefully

CATCH, error
IF error NE 0 THEN BEGIN
   formdata = {cancel:1, menu : 0}
   *info.ptrToFormData = formdata
   Widget_Control, event.top, /Destroy
   print,!err_string
   RETURN
ENDIF

Widget_Control, event.id, Get_Value=userEvent

CASE userEvent OF


  'Cancel' : begin
               formdata = {cancel:1, menu : 0}
               *info.ptrToFormData =formdata
               widget_Control, event.top, /destroy
             end 

      
  'Run Now'   : begin
                 
                ; gather the data for return to calling program
                
                err  = 0
                mess = ''
                
                widget_Control, info.paperID, Get_Value=pap
                widget_Control, info.passID, Get_Value=pass
                widget_Control, info.icID, Get_Value=icfile
                widget_Control, info.lsID, Get_Value=lsfile
                                
                Widget_Control, info.opbutID, Get_Value=cfgstyle
               
                ; no need to set mess as it is flagged in the cw
                if pap.outbut EQ 1 AND strtrim(pap.message) NE '' then err=1
                if icfile.outbut EQ 1 AND strtrim(icfile.message) NE '' then err=1
                if lsfile.outbut EQ 1 AND strtrim(lsfile.message) NE '' then err=1
                
                
                if err EQ 0 then begin
                   formdata = { paper    : pap,      $
                                pass     : pass,     $
                                icfile   : icfile,   $
                                lsfile   : lsfile,   $
                                cfgstyle : cfgstyle, $
                                batch    : 0,        $
                                cancel   : 0,        $
                                menu     : 0         }
                   *info.ptrToFormData = formdata
                   widget_control, event.top, /destroy
                endif else begin
                   widget_Control, info.messID, Set_Value=mess
                endelse

             end
             
             
  'Run in Batch'   : begin
                 
                ; gather the data for return to calling program
                
                err  = 0
                mess = ''
                
                widget_Control, info.paperID, Get_Value=pap
                widget_Control, info.passID, Get_Value=pass
                widget_Control, info.icID, Get_Value=icfile
                widget_Control, info.lsID, Get_Value=lsfile
                
                Widget_Control, info.opbutID, Get_Value=cfgstyle
                 
               ; no need to set mess as it is flagged in the cw
                if pap.outbut EQ 1 AND strtrim(pap.message) NE '' then err=1
                if icfile.outbut EQ 1 AND strtrim(icfile.message) NE '' then err=1
                if lsfile.outbut EQ 1 AND strtrim(lsfile.message) NE '' then err=1
                
                
                if err EQ 0 then begin
                   formdata = { paper    : pap,      $
                                pass     : pass,     $
                                icfile   : icfile,   $
                                lsfile   : lsfile,   $
                                cfgstyle : cfgstyle, $
                                batch    : 1,        $
                                cancel   : 0,        $
                                menu     : 0         }
                   *info.ptrToFormData = formdata
                   widget_control, event.top, /destroy
                endif else begin
                   widget_Control, info.messID, Set_Value=mess
                endelse

             end
             
  'Extra Options...'  : Begin
  
                print, 'Surprise!!!'
              
             end                
             
  else : print,'ADAS801_RUN_EVENT : You should not see this message! ',userEvent
                   
ENDCASE

END
  
;-----------------------------------------------------------------------------



FUNCTION ADAS801_RUN, outval,bitfile,                    $
                      FONT_LARGE = font_large,           $
                      FONT_SMALL = font_small


; Set defaults for keywords and extract info for paper.txt question

  IF n_elements(font_large) eq 0 THEN font_large = ''
  IF n_elements(font_small) eq 0 THEN font_small = ''


  paperval =  { outbut   : outval.texout, $
                appbut   : outval.texapp, $
                repbut   : outval.texrep, $
                filename : outval.texdsn, $
                defname  : outval.texdef, $
                message  : outval.texmes  }



  lsval   =  { outbut    : outval.lsout,  $
                appbut   : outval.lsapp,  $
                repbut   : outval.lsrep,  $
                filename : outval.lsdsn,  $
                defname  : outval.lsdef,  $
                message  : outval.lsmes   }
 
  icval   =  { outbut    : outval.icout,  $
                appbut   : outval.icapp,  $
                repbut   : outval.icrep,  $
                filename : outval.icdsn,  $
                defname  : outval.icdef,  $
                message  : outval.icmes   }



                ;********************************************
                ;**** create modal top level base widget ****
                ;********************************************
                
  parent = Widget_Base(Column=1, Title='ADAS 801 OUTPUT', $
                       XOFFSET=100, YOFFSET=1)

                
  rc = widget_label(parent,value='  ',font=font_large)

                 
  
                ;************************************
                ;**** Ask for output files -     ****
                ;**** IC and LS adf04 output and ****
                ;**** paper.txt, pass directory  ****
                ;************************************

  mlab    = widget_label(parent,value='Names of adf04 files:-',font=font_large)
  mcol    = widget_base(parent, /column, /frame)
 

  icID    = cw_adas_outfile(mcol, OUTPUT='Intermediate Coupling ',   $
                                   VALUE=icval, FONT=font_large)

 
  lsID    = cw_adas_outfile(mcol, OUTPUT='LS coupling           ',   $
                                   VALUE=lsval, FONT=font_large)
                       
  butbase = widget_base(mcol, /row, /align_center)
  buttons = ['Standard configurations',                        $
             'Eissner' ]
  opbutID = cw_bgroup(butbase,buttons,                      $
                       exclusive=1,row=1,                   $
                       event_func='ADAS_PROC_NULL_EVENTS',  $
                       font=font_small    )
                       

  mlab    = widget_label(parent,value='  ',font=font_large)
  mlab    = widget_label(parent,font=font_large,    $
                         value='Names of log file and temporary directory:-')
  base    = widget_base(parent, /column,/frame)
  
  mrow    = widget_base(base)
  paperID = cw_adas_outfile(mrow, OUTPUT='Text Output',   $
                                 VALUE=paperval, FONT=font_large)
  
  
  passval   = { rootpath  :  outval.passroot,   $
                centroot  :  '',                $
                userroot  :  outval.userroot    }
  passtitle = widget_label(base, font=font_large,              $
                         value='Destination of pass files:-')
  
  passID    = cw_adas_seldir(base, value=passval, font=font_large, $
                             event_func='ADAS_PROC_NULL_EVENTS',/default)
  
                       
  
                       
                            
                ;************************************
                ;**** Error/Instruction message. ****
                ;************************************
                
  messID = widget_label(parent,value='     Choose output options   ',font=font_large)
                      
                      
                            
                ;*****************
                ;**** Buttons ****
                ;*****************
                
  menufile = bitfile + '/menu.bmp'
  read_X11_bitmap, menufile, bitmap1
                
  mrow     = widget_base(parent,/row)
  menuID   = widget_button(mrow,value=bitmap1,font=font_large, $
                           event_pro='ADAS801_RUN_MENU')
  cancelID = widget_button(mrow,value='Cancel',font=font_large)
  runID    = widget_button(mrow,value='Run Now',font=font_large)
  batchID  = widget_button(mrow,value='Run in Batch',font=font_large)
  extraID  = widget_button(mrow,value='Extra Options...',font=font_large)



                ;************************
                ;**** Initial values ****
                ;************************
                
; De-sensitize Extra Options until they are added

  Widget_Control, extraID, sensitive=0
  
  Widget_Control, opbutID, set_value = outval.cfgstyle
  

                ;***************************
                ;**** Put up the widget ****
                ;***************************

; Realize the ADAS801_RUN input widget.

   dynlabel, parent
   widget_Control, parent, /realize

; Create a pointer to store the information collected from the form.
; The initial data stored here is set to CANCEL, so nothing needs to
; be done if the user kills the widget with the mouse.

   ptrToFormData = Ptr_New({cancel:1, menu:0})

; Create an info structure with program information.

  info = { messID          :  messID,             $
           paperID         :  paperID,            $
           passID          :  passID,             $
           icID            :  icID,               $
           lsID            :  lsID,               $
           opbutID         :  opbutID,            $
           extraID         :  extraID,            $
           ptrToFormData   :  ptrToFormData       }  
            
               
; Store the info structure in the user value of the top-level base and launch
; the widget into the world.

  widget_control, parent, Set_UValue=info

  XManager, 'ADAS801_RUN', parent, Event_Handler='ADAS801_RUN_EVENT'


; When the widget is destroyed, the block is released, and we
; return here. Get the form data that was collected by the form
; and stored in the pointer location.

formdata = *ptrToFormData


rep='CONTINUE'
IF N_Elements(formdata) EQ 0 THEN BEGIN
   Ptr_Free, ptrToFormData
   rep ='CANCEL'
   RETURN,rep
ENDIF
 
IF formdata.cancel EQ 1 THEN BEGIN
   Ptr_Free, ptrToFormData
   rep ='CANCEL'
   RETURN,rep
ENDIF

IF formdata.menu EQ 1 THEN BEGIN
   Ptr_Free, ptrToFormData
   rep ='MENU'
   RETURN,rep
ENDIF

if rep eq 'CONTINUE' then begin
   
    outval =  { texout      :  formdata.paper.outbut,     $
                texapp      :  formdata.paper.appbut,     $
                texrep      :  formdata.paper.repbut,     $
                texdsn      :  formdata.paper.filename,   $
                texdef      :  formdata.paper.defname,    $
                texmes      :  formdata.paper.message,    $
                icout       :  formdata.icfile.outbut,    $
                icapp       :  formdata.icfile.appbut,    $
                icrep       :  formdata.icfile.repbut,    $
                icdsn       :  formdata.icfile.filename,  $
                icdef       :  formdata.icfile.defname,   $
                icmes       :  formdata.icfile.message,   $
                lsout       :  formdata.lsfile.outbut,    $
                lsapp       :  formdata.lsfile.appbut,    $
                lsrep       :  formdata.lsfile.repbut,    $
                lsdsn       :  formdata.lsfile.filename,  $
                lsdef       :  formdata.lsfile.defname,   $
                lsmes       :  formdata.lsfile.message,   $
                cfgstyle    :  formdata.cfgstyle,         $
                userroot    :  outval.userroot,           $
                passroot    :  formdata.pass.rootpath     }
   
   if formdata.batch EQ 0 then rep = 'NOW' else rep = 'BATCH'
   
   ; Free the pointer.
   Ptr_Free, ptrToFormData

endif       


RETURN,rep

END
