; Copyright (c) 2000 Strathclyde University .
; SCCS info: Module @(#)$Header: /home/adascvs/idl/adas8xx/adas801/adas801_rcn_analyse.pro,v 1.1 2004/07/06 11:05:36 whitefor Exp $ Date $Date: 2004/07/06 11:05:36 $
;+
; PROJECT:
;       ADAS 
;
; NAME:
;	ADAS801_RCN_ANALYSE
;
; PURPOSE: 
;       Reads the RCN input file, finds the configurations and
;       determines whether there are any errors in it. 
;
;
; EXPLANATION:
;     
;
; NOTES:
;
;
; INPUTS:
;
; OPTIONAL INPUTS:
;       None
;
; OUTPUTS:
;
; OPTIONAL OUTPUTS:
;       None
;
; KEYWORD PARAMETERS:
;
; CALLS:
;
; SIDE EFFECTS:
;
; CATEGORY:
;	
;
; WRITTEN:
;       Martin O'Mullane
;
; MODIFIED:
;	1.1	Martin O'Mullane
;		First release 
;
; VERSION:
;       1.1	12-10-99
;
;-
;-----------------------------------------------------------------------------



PRO ADAS801_RCN_ANALYSE, file, num_cfg1, num_cfg2, cfg1, cfg2,   $
                         z0, z1, text, error

  error = ''


; Check if the file exists

  file_acc, file, exist, read, write, execute, filetype

  if exist NE 1 OR read NE 1 then begin
     error = 'File does not exist'
     return
  endif


; Read z0, z1 and configurations - ignore the rest

  str_opt = ''
  str     = ''
  cfa     = ''
  cf      = ''
  z0      = 0
  z1      = 0
  iend    = 0
  
  openr, lun, file, /get_lun
  readf, lun, str_opt
  
  while not eof(lun) AND iend GE 0 do begin
     readf, lun, str
     reads, str, iend, format='(1x,i4)'
     if iend NE -1 then begin
        reads, str, z0, z1, cfa, format='(1x,i4,3x,i2,22x,a40)'
        cf = [cf, cfa]
     endif
  endwhile

  free_lun, lun
  
  if n_elements(cf) LT 2 then begin
     error = 'No configurations in the file'
     return
  endif else begin  
     cf = cf[1:*]
  endelse
  
  
; Seperate the configurations


  parity = get_parity(cf)
  
  test = uniq(parity)
  
  ind_1 = where(parity EQ parity[0],num_cfg1)
  
  if n_elements(parity) EQ 1 then num_cfg2 = 0 else $
      ind_2 = where(parity EQ parity[n_elements(parity)-1],num_cfg2)
 
  if num_cfg1 GT 0 then cfg1 = cf[ind_1]
  if num_cfg2 GT 0 then cfg2 = cf[ind_2]

  
; Validate the RCN file and write a new file to string array.

  text = str_opt
  for j =0, n_elements(parity)-1 do begin
     elem = xxesym(z0) 
     str  = string(z0,z1,elem,cf[j],cf[j],  $
            format='(i5,3x,i2,3x,a2,1x,a12,4x,a)')
     text = [text,str]
  endfor
  text = [text,'   -1']


END
