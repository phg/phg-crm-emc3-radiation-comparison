; Copyright (c) 2000 Strathclyde University .
; SCCS info: Module @(#)$Header: /home/adascvs/idl/adas8xx/adas801/adas801_in.pro,v 1.6 2013/01/17 01:08:44 mog Exp $ Date $Date: 2013/01/17 01:08:44 $
;+
; PROJECT:
;       ADAS 
;
; NAME:
;	ADAS801_IN
;
; PURPOSE: 
;       This function puts up a widget asking for adf34 input file.
;       Five options are offered - structure run, type 3 and type 1 
;       and type 3 adf04 file production limited to the standard
;       14 outputs and eother dipole or all transitions allowed.
;       The ionisation potential is required for adf04 files and
;       can be entered or automatically retrieved from central
;       ADAS adf00 files. 
;
;
; EXPLANATION:
;             
;     
;
; NOTES:
;       Based on the techniques of ADAS403. It entirely in IDL and uses
;       v5 (and above) features such as pointers to pass data.
; 
;
;
; INPUTS:
;        inval       - A structure holding the 'remembered' input data.
;                        ROOTPATH     : root file 1
;                        FILE         : filename 1
;                        CENTROOT     : central adas of root 1
;                        USERROOT     : user path of file 1
;
; OPTIONAL INPUTS:
;       None
;
; OUTPUTS:
;       The function returns a string rep
;           rep = 'CONTINUE' if everything is ok
;           rep = 'CANCEL' if there is a problem or indeed if the user
;                          closes the widget with the mouse. 
;
; OPTIONAL OUTPUTS:
;       None
;
; KEYWORD PARAMETERS:
;       FONT_LARGE - The name of a larger font e.g 'courier_bold14'
;
;       FONT_SMALL - The name of a smaller font e.g 'courier_bold12'
;
;
; CALLS:
;                            
;       ADAS801_IN_FILEA     : Reacts to first file widget and sensitises
;                              'Done' and 'Browse' if 2 valid files have
;                              been selected.
;       ADAS801_IN_FILEB     : Ditto for second file.
;       ADAS801_IN_EVENT     : Reacts to Cancel, Done and Browse.
;       READ_ADF00           : Reads ionisation potential values.
;
; SIDE EFFECTS:
;
; CATEGORY:
;	Widgets
;
; WRITTEN:
;       Martin O'Mullane
;
; MODIFIED:
;	1.1	Martin O'Mullane
;		First release 
;       1.2     Martin O'Mullane 
;                - Add option to get ionisation potential from central ADAS
;                  adf00 files.
;                - Add buttons for production of type 1 adf04 files.
;       1.3     Martin O'Mullane 
;                - If no 'Extra options...' are selected set procval.nte
;                  or the default file value will be used.
;       1.4     Martin O'Mullane
;                - Changes in procval and adas801_read_adf34 to enable the
;                  user to select which configurations to include in the
;                  adf04 output file.
;       1.5     Martin O'Mullane
;                - Remove comments before processing adf34 file.
;                - Increase number of even and odd configurations in procval to 20.
;                - When the 'Extra option...' route is not chosen, check for NULL
;                  when setting the included configurations.
;
; VERSION:
;       1.1	12-10-1999
;       1.2     06-12-2002
;       1.3     04-08-2008
;       1.4     11-01-2012
;       1.5     16-01-2013
;
;-
;-----------------------------------------------------------------------------

PRO adas801_read_adf34, file=file, iz0=iz0, iz1=iz1, cfg_p1=cfg_p1, cfg_p2=cfg_p2

adas_readfile, file=file, all=all, nlines=nlines

; remove any comments

ind = where(strpos(all, 'C') EQ 0, count, complement=i1)
if count GT 0 then begin
   all    = all[i1]
   nlines = n_elements(all)
endif

iz0_in = 0
iz1_in = 0
reads, all[1], iz0_in, iz1_in

cfg = strmid(all[1:nlines-2], 33)
par = get_parity(cfg)

ip1 = where(par EQ par[0], n_p1, complement=ip2, ncomplement=n_p2)

iz0 = iz0_in
iz1 = iz1_in

cfg_p1 = strarr(20) + 'NULL'
cfg_p2 = strarr(20) + 'NULL'

if n_p1 GT 0 then cfg_p1[0:n_p1-1] = cfg[ip1]
if n_p2 GT 0 then cfg_p2[0:n_p2-1] = cfg[ip2]

END
;-----------------------------------------------------------------------------



FUNCTION ADAS801_IN_FILE, event

   ; Sensitise 'Done' if we are ready to continue - this also allows
   ; the Browse and Extra options buttons to become active. 

   Widget_Control, event.top, Get_UValue=info
     
   Widget_Control, info.flID, Get_Value=file

   filename = file.rootpath
   file_acc, filename, exist, read, write, execute, filetype
        
   if event.action EQ 'newfile' then begin
      Widget_Control, info.doneID, sensitive=1
      Widget_Control, info.browseID, sensitive=1
      Widget_Control, info.extraID, sensitive=1
   endif else begin
      Widget_Control, info.doneID, sensitive=0
      Widget_Control, info.browseID, sensitive=0
      Widget_Control, info.extraID, sensitive=0
   endelse
   
   RETURN,0

END



;-----------------------------------------------------------------------------

FUNCTION ADAS801_CALCTYPE_EVENTS, event

   ; If a structure calculation is selected desensitise the IP and 
   ; coupling options.
   

   Widget_Control, event.top, Get_UValue=info
   
   if event.value EQ 0 then begin
   
      Widget_Control, info.opipID, sensitive=0
      Widget_Control, info.opgetipID, sensitive=0
   
   endif else begin
   
      Widget_Control, info.opipID, sensitive=1
      Widget_Control, info.opgetipID, sensitive=1
   
   
   endelse
   
   
   RETURN,0

END 

;-----------------------------------------------------------------------------



PRO ADAS801_IN_EVENT, event

   ; Get the info structure out of the user value of the top-level base.

Widget_Control, event.top, Get_UValue=info



Widget_Control, event.id, Get_Value=userEvent

widget_control, info.messID, set_value=' '



CASE userEvent OF

  'Cancel' : begin
               formdata = {cancel:1}
               *info.ptrToFormData =formdata
               widget_Control, event.top, /destroy
             end 

      
  'Done'   : begin
                 
                ; gather the data for return to calling program
                ; 'Done' is not sensitised until valid filenames
                ; have been chosen. 
                ; No need to check the others as the compound
                ; widgets ensure that only correct values
                ; are returned.
               
                Widget_Control, info.flID, Get_Value=val
                Widget_Control, info.opbutID, Get_Value=calctype
                Widget_Control, info.opipID, Get_Value=ipvalue
               
                formdata = {val      : val,          $
                            procval  : info.procval, $
                            calctype : calctype,     $
                            ionpot   : ipvalue,      $
                            cancel   : 0             }
                 *info.ptrToFormData = formdata
                 widget_control, event.top, /destroy

             end


  'Browse Comments' : Begin
  
                Widget_Control, info.flID, get_value=val
                filename = val.rootpath + val.file
                xxtext, filename, font=info.font_large
  
             end 
             
             
  'Get IP' : Begin
  
                ; read adf34 file for element and ion charge
                ; check if it exists and is reabable
                
                Widget_Control, info.flID, Get_Value=val
                a34_file = val.rootpath + val.file
                file_acc, a34_file, exist, read, write, execute, filetype
                
                err  = 0
                mess = ''
                
                if filetype EQ 'd' then begin
                   err  = 1
                   mess = 'No adf34 file selected yet'
                endif
                
                if err EQ 0 AND read EQ 0 then begin
                   err  = 1
                   mess = 'File cannot be read from your id'
                endif
                
                ; If it's valid get ionisation potential from central ADAS
                
                if err EQ 1 then begin
                   widget_control, info.messID, set_value=mess
                endif else begin
                
                   adas801_read_adf34, file=a34_file, iz0=iz0, iz1=iz1
                   
                   read_adf00, z0 = iz0, z1 = iz1-1, ionpot = ip
                
                   ionpot = ip[0] * 8065.0
                   
                   info.ionpot = ionpot
                   widget_control, info.opipID, set_value=ionpot
                
                endelse

             end 
             
             
  'Extra Options...'  : Begin
  
                ; We need the ion charge of the input file for
                ; reduced temperatures in extra options. The RCN
                ; input file is already chosen at this stage so
                ; we can examine it!
                ;
                ; We also need the type of adf04 output file.
                
                Widget_Control, info.flID, Get_Value=val
                
                adas801_read_adf34, file=val.rootpath+val.file, iz0=iz0, iz1=iz1, cfg_p1=cfg_p1, cfg_p2=cfg_p2
                
                info.procval.z0 = iz0
                info.procval.z1 = iz1
                info.procval.cfg1 = cfg_p1
                info.procval.cfg2 = cfg_p2
                
                widget_control, info.opbutID, Get_Value=calctype
                case calctype[0] of
                   0 : a04_type = 0
                   1 : a04_type = 3
                   2 : a04_type = 3
                   3 : a04_type = 1
                   4 : a04_type = 1
                endcase
                                          
                st_in = info.procval      
                rep=adas801_in_opt(st_in, a04_type,               $
                                   parent     = info.parent,      $
                                   FONT_SMALL = info.font_small,  $
                                   FONT_LARGE = info.font_large,  $
                                   EDIT_FONTS = info.edit_fonts   )

                ; Remember to store info.procval in the Uvalue!
                                 
                if rep EQ 'CONTINUE' then begin
                    info.procval = st_in
                    Widget_Control, event.top, Set_UValue=info
                endif

             end                
  
  
  else : print,'ADAS801_IN_EVENT : You should not see this message! '
                   
ENDCASE

END
  
;-----------------------------------------------------------------------------


FUNCTION adas801_in, inval, procval,  $
                     FONT_LARGE = font_large, FONT_SMALL = font_small, $
                     EDIT_FONTS = edit_fonts

		;**** Set defaults for keywords ****

  IF n_elements(font_large) eq 0 THEN font_large = ''
  IF n_elements(font_small) eq 0 THEN font_small = ''


; Path names may be supplied with or without the trailing '/'.  Add
; this character where required so that USERROOT will always end in
; '/' on output.
  
  if strtrim(inval.rootpath) eq '' then begin
      inval.rootpath = './'
  endif else if                                                   $
  strmid(inval.rootpath, strlen(inval.rootpath)-1,1) ne '/' then begin
      inval.rootpath = inval.rootpath+'/'
  endif

 
                ;********************************************
		;**** create modal top level base widget ****
                ;********************************************
                
  parent = Widget_Base(Column=1, Title='ADAS 801 INPUT', $
                       XOFFSET=100, YOFFSET=1)

                
  rc = widget_label(parent,value='  ',font=font_large)


  base   = widget_base(parent, /column)
  
  
                ;*********************************
                ;**** adf32 input file widget ****
                ;*********************************

  flbase  = widget_base(base, /column)
  flval   = {  ROOTPATH        :       inval.rootpath,             $
               FILE            :       inval.file,                 $
               CENTROOT        :       inval.centroot,             $
               USERROOT        :       inval.userroot              }
 
  fltitle = widget_label(flbase, font=font_large,                  $
                          value='Input File Details:-')
  flID    = cw_adas_infile(flbase, value=flval, font=font_small,   $
                           ysize = 10,                             $
                           event_funct='ADAS801_IN_FILE')


                ;*********************************
                ;**** options input on widget ****
                ;*********************************

  oplab   = widget_label(base,font=font_large, value='Options:-')   
  opbase  = widget_base(base, /column,/frame)
  
  buttons = ['Structure run only',                        $
             'Standard 14 temperatures, E1 only',         $
             'Standard 14 temperatures, E1 and forbidden',$
             'Type 1 adf04 file, E1 only',                $
             'Type 1 adf04 file, E1 and forbidden'        ]
  opbutID = cw_bgroup(opbase,buttons,                         $
                       exclusive=1,column=1,                  $
                       event_func='ADAS801_CALCTYPE_EVENTS',  $
                       font=font_small    )


  oplab     = widget_label(opbase,font=font_small, value=' ')
  
  opbase    = widget_base(opbase, /row)
  
  opipID    = cw_field(opbase, title='Ionisation Potential (cm-1)', $
                      floating=1, fieldfont=font_small, font=font_small) 
  opgetipID = widget_button(opbase,value='Get IP',font=font_large)
  oplab     = widget_label(opbase,font=font_small, value=' from central ADAS')


                ;*****************
		;**** Buttons ****
                ;*****************
                
  mrow      = widget_base(parent,/row,/align_center)
  messID    = widget_label(mrow,font=font_large, $
                          value='                         ')
                
  mrow      = widget_base(parent,/row)
  
  browseID  = widget_button(mrow,value='Browse Comments', font=font_large)
  cancelID  = widget_button(mrow,value='Cancel',font=font_large)
  doneID    = widget_button(mrow,value='Done',font=font_large)
  configsID = widget_button(mrow,value='Make Input File...',font=font_large)
  extraID   = widget_button(mrow,value='Extra Options...',font=font_large)



                ;**************************
		;**** Initial settings ****
                ;**************************
                

; Main button sensitivities 


    filename = inval.rootpath+inval.file
    file_acc, filename, exist, read, write, execute, filetype
    if filetype ne '-' then begin
      widget_control,browseID,sensitive=0
      widget_control,doneID,sensitive=0
      widget_control,extraID,sensitive=0
    end else begin
      if read eq 0 then begin
        widget_control,browseID,sensitive=0
        widget_control,doneID,sensitive=0
       widget_control,extraID,sensitive=0
      end
    end


; Type of calculation

   widget_control,opbutID, set_value=inval.calctype

; Ionisation potential
   
   widget_control,opipID, set_value=inval.ionpot



; For structure calculations turn off the rest

  if inval.calctype EQ 0 then widget_control,opipID,sensitive=0



; Turn off making input file for the moment


  widget_control,configsID,sensitive=0




; Realize the ADAS801 input widget.

   dynlabel, parent
   widget_Control, parent, /realize

; Create a pointer to store the information collected from the form.
; The initial data stored here is set to CANCEL, so nothing needs to
; be done if the user kills the widget with the mouse.

   ptrToFormData = Ptr_New({cancel:1})

; Create an info structure with program information.

  info = { parent          :  parent, $
           flID            :  flID,               $
           opbutID         :  opbutID,            $
           opipID          :  opipID,             $
           opgetipID       :  opgetipID,          $
           browseID        :  browseID,           $
           doneID          :  doneID,             $
           configsID       :  configsID,          $
           extraID         :  extraID,            $
           messID          :  messID,             $
           ionpot          :  inval.ionpot,       $
           font_large      :  font_large,         $
           font_small      :  font_small,         $
           edit_fonts      :  edit_fonts,         $
           procval         :  procval,            $
           ptrToFormData   :  ptrToFormData       }  
            
               
; Store the info structure in the user value of the top-level base and launch
; the widget into the world.

  widget_control, parent, Set_UValue=info

  XManager, 'adas801_in', parent, Event_Handler='ADAS801_IN_EVENT'


; When the widget is destroyed, the block is released, and we
; return here. Get the data that was collected by the widget
; and stored in the pointer location. Finally free the pointer.

formdata = *ptrToFormData

rep='CONTINUE'
IF N_Elements(formdata) EQ 0 THEN BEGIN
   Ptr_Free, ptrToFormData
   rep ='CANCEL'
   RETURN,rep
ENDIF
 
IF formdata.cancel EQ 1 THEN BEGIN
   Ptr_Free, ptrToFormData
   rep ='CANCEL'
ENDIF

if rep eq 'CONTINUE' then begin
   
   inval.rootpath   = formdata.val.rootpath
   inval.file       = formdata.val.file
   inval.calctype   = formdata.calctype
   inval.ionpot     = formdata.ionpot

   procval          = formdata.procval
   
   ; Set the standard Te set if not set by 'Extra options...'. Note we
   ; also need z1 for conversion of reduced to K.
   
   ; For type 1 adf04 files we still need to send a set of temperatures 
   ; to rcn2.
   
   if procval.options_set EQ 'NO' OR procval.a04_type EQ 1 then begin
      
      te = [5.00e+02, 1.00e+03, 2.00e+03, 3.00e+03, 5.00e+03, 1.00e+04, 1.50e+04, $
            2.00e+04, 3.00e+04, 5.00e+04, 1.00e+05, 1.50e+05, 2.00e+05, 5.00e+05]
      nte = n_elements(te)
      procval.te[0:nte-1] = te
      procval.te_unit = 2
      procval.nte = nte
      
      adas801_read_adf34, file=inval.rootpath+inval.file, iz0=iz0, iz1=iz1, cfg_p1=cfg_p1, cfg_p2=cfg_p2
      procval.z0   = iz0
      procval.z1   = iz1
      
      procval.cfg1   = cfg_p1
      i1             = where(strpos(cfg_p1, 'NULL') EQ -1, c1)
      ilist          = intarr(20)
      ilist[i1]      = intarr(c1) + 1
      procval.p1list = ilist            

      procval.cfg2   = cfg_p2
      i2             = where(strpos(cfg_p2, 'NULL') EQ -1, c2)
      ilist          = intarr(20)
      ilist[i2]      = intarr(c2) + 1
      procval.p2list = ilist            

   endif
   
   ; Set ind_xpar to be a fixed set always
   
   procval.ind_xpar = [0,1,2,3,4,5,6,7,8,10,12,16,18,20]
   case formdata.calctype[0] of
      0 : procval.a04_type = 0
      1 : procval.a04_type = 3
      2 : procval.a04_type = 3
      3 : procval.a04_type = 1
      4 : procval.a04_type = 1
   endcase
   
   Ptr_Free, ptrToFormData

endif       



RETURN,rep

END
