; Copyright (c) 2000 Strathclyde University .
;+
; PROJECT:
;       ADAS 
;
; NAME:
;       ADAS801_IN_OPT
;
; PURPOSE: 
;       This function puts up a widget asking for optional parameters
;       such as scaling factors, temperature sets and E1/M1/E2 choices. 
;
;
; EXPLANATION:
;             
;     
;
; NOTES:
;       Based on the techniques of ADAS403. It entirely in IDL and uses
;       v5 (and above) features such as pointers to pass data.
; 
;
;
; INPUTS:
;        inval       - A structure holding the 'remembered' input data.
;
; OPTIONAL INPUTS:
;       None
;
; OUTPUTS:
;       The function returns a string rep
;           rep = 'CONTINUE' if everything is ok
;           rep = 'CANCEL' if there is a problem or indeed if the user
;                          closes the widget with the mouse. 
;
; OPTIONAL OUTPUTS:
;       None
;
; KEYWORD PARAMETERS:
;       FONT_LARGE - The name of a larger font e.g 'courier_bold14'
;
;       FONT_SMALL - The name of a smaller font e.g 'courier_bold12'
;
;
; CALLS:
;       ADAS801_IN_OPT_DEF   : Event handler for Temperature selection.                     
;       ADAS801_IN_OPT_UNIT  : Converts between red. eV and K temperatures.
;       ADAS801_IN_OPT_EVENT : General event handler.
;
; SIDE EFFECTS:
;
; CATEGORY:
;       Widgets
;
; WRITTEN:
;       Martin O'Mullane
;
; MODIFIED:
;       1.1     Martin O'Mullane
;                - First release 
;       1.2     Martin O'Mullane
;                - Blank out Temperature entry panel when doing a
;                  structure only run or for a type 1 adf04 output.  
;       1.3     Martin O'Mullane
;                - Add section to limit configurations used in collison
;                  calculation. 
;       1.4     Martin O'Mullane
;                - Do not display configurations labelled as 'NULL'
;
; VERSION:
;       1.1     12-10-1999
;       1.2     06-12-2002
;       1.3     10-01-2012
;       1.4     16-01-2013
;
;-
;-----------------------------------------------------------------------------

PRO ADAS801_IN_OPT_DEF, event

; Get the info structure out of the user value of the top-level base.

Widget_Control, event.top, Get_UValue=info


; Is it the 'clear' button or a new default temperature set?

ind = where(tag_names(event) EQ 'SELECT', isclear)

if isclear EQ 1 then begin

   ; Don't bother asking for confirmation!
   
   te      = fltarr(14)
   empty   = strarr(1,14)
   tempval = {VALUE:empty, UNITS:-1}
   widget_control, info.tempID, set_value = tempval

endif else begin
 
   CASE event.index OF
      0 : te = [5.00e+02, 1.00e+03, 2.00e+03, 3.00e+03, 5.00e+03, 1.00e+04, 1.50e+04, $
                2.00e+04, 3.00e+04, 5.00e+04, 1.00e+05, 1.50e+05, 2.00e+05, 5.00e+05]
      1 : te = [1.16e+03, 1.74e+03, 2.32e+03, 3.48e+03, 5.80e+03, 8.12e+03, 1.16e+04, $
                1.74e+04, 2.32e+04, 3.48e+04, 5.80e+04, 8.12e+04, 1.16e+05]
      2 : te = [1.16e+05, 2.32e+05, 5.80e+05, 1.16e+06, 2.32e+06, 5.80e+06, 1.16e+07, $
                2.32e+07, 5.80e+07, 1.16e+08, 2.32e+08, 5.80e+08, 8.12e+08] 
   ENDCASE

   nte                  = n_elements(te)
   tabledata            = strarr(1,14)
   tabledata(0,0:nte-1) = strtrim(string(te,format=info.num_form),2)
   tempval              = {VALUE:tabledata, UNITS:-1}

   Widget_Control, info.tempID, Set_Value = tempval

   info.te_unit = 2
   info.procval.te[*] = 0.0
   info.procval.te[0:nte-1] = te
   info.procval.nte = nte
   
   Widget_Control, info.unitID, Set_Droplist_select=2

endelse


; Make sure to set stuff in procval reflecting these changes


Widget_Control, event.top, Set_UValue=info


END

;-----------------------------------------------------------------------------



PRO ADAS801_IN_OPT_UNIT, event

   ; Get the info structure out of the user value of the top-level base.

Widget_Control, event.top, Get_UValue=info



; Get current temperatures from the table and get into float array - tin.
 
widget_control, info.tempID, get_value = tempval

ind  = where(tempval.value NE '', nte)

if nte GT 0 then begin 

   tin  = fltarr(nte)
   tout = fltarr(nte)

   for j=0, nte-1 do begin
      reads, tempval.value[0,j], tmp
      tin[j] = tmp
   endfor

   ; Convert to selected units. The following is a bit obscure. xxtcon
   ; converts between K, eV and Red. depending on whether itype and jtype
   ; are set to 1, 2 or 3. itype is the current setting as is carried 
   ; as info.te_unit. The event.index from the widget_droplist are 0,1,2,..
   ; depending on the button. The buttons are mapped to K, eV and Red. 
   ; Hence event.index is jtype!!  

   itype = info.te_unit+1
   jtype = event.index+1

   xxtcon, itype, jtype, info.procval.z1, nte, tin, tout 

   ; Convert new temperature to strings and update the table.

   tabledata            = strarr(1,14)
   tabledata(0,0:nte-1) = strtrim(string(tout,format=info.num_form),2)
   tempval              = {VALUE:tabledata, UNITS:-1}

   Widget_Control, info.tempID, Set_Value = tempval

   info.procval.te[0:nte-1] = tout 

endif

; Make sure to set the current unit type

info.te_unit = event.index

Widget_Control, event.top, Set_UValue=info


END

;-----------------------------------------------------------------------------

PRO ADAS801_IN_OPT_EVENT, event

   ; Get the info structure out of the user value of the top-level base.

Widget_Control, event.top, Get_UValue=info


; Something nefarious may cause an error also.
;  -- if so trap it and exit gracefully

;CATCH, error
;IF error NE 0 THEN BEGIN
;   formdata = {cancel:1}
;   *info.ptrToFormData = formdata
;   message,!err_string
;   Widget_Control, event.top, /Destroy
;   RETURN
;ENDIF

Widget_Control, event.id, Get_Value=userEvent

CASE userEvent OF

  'Cancel' : begin
               formdata = {cancel:1}
               *info.ptrToFormData =formdata
               widget_Control, event.top, /destroy
             end 

      
  'Done'   : begin
  
                err  = 0
                mess = ' '
                 
                ; gather the data for return to calling program
                ; 'Done' is not sensitised until valid filenames
                ; have been chosen. 
                
                
                ; Confirm the extra options has been set
                
                info.procval.options_set = 'YES'
               
                ; IQUAD and IMAG are set
               
                widget_control, info.e2ID, get_value=e2b
                widget_control, info.m1ID, get_value=m1b
                
                iquad = 0
                if e2b[0]+e2b[1] EQ 2 then iquad = 3 else begin
                    if e2b[0] EQ 1 then iquad = 1
                    if e2b[1] EQ 1 then iquad = 2
                endelse
                    
                imag = 0
                if m1b[0]+m1b[1] EQ 2 then imag = 3 else begin
                    if m1b[0] EQ 1 then imag = 1
                    if m1b[1] EQ 1 then imag = 2
                endelse
                    
                info.procval.iquad = iquad
                info.procval.imag  = imag
                
                ; Scale factors
                
                scale = intarr(5)
                for j =0, 4 do begin
                  Widget_Control, info.sID[j], Get_Value=sc
                  scale[j] = sc
                  if sc LT 1 OR sc GT 100 then begin
                     mess = 'Scale Factors must lie in range 1-100'
                     err = 1 
                  endif
                endfor
                
                info.procval.fii   =  scale[0]
                info.procval.zeta  =  scale[1]
                info.procval.fij   =  scale[2]
                info.procval.gij   =  scale[3]
                info.procval.rij   =  scale[4]
                
                ; Retained configurations
                
                if info.p1ID NE -1 then begin
                
                   widget_control, info.p1ID, get_value=p1but
                   num = n_elements(p1but)
                   info.procval.p1list[0:num-1] = p1but
                   
                endif
                
                if info.p2ID NE -1 then begin
                
                   widget_control, info.p2ID, get_value=p2but
                   num = n_elements(p2but)
                   info.procval.p2list[0:num-1] = p2but
                   
                endif
                
                ; Temperatures
                
                Widget_Control, info.tempID, Get_Value=tempval
                
                info.procval.te_unit = info.te_unit
                
                ind  = where(tempval.value NE '', nte)

                if nte GT 0 then begin 

                   te   = fltarr(nte)
                   info.procval.nte = nte
                   
                   for j=0, nte-1 do begin
                      reads, tempval.value[0,j], tmp
                      info.procval.te[j] = tmp
                   endfor
                   
                 endif else begin
                 
                    err = 1
                    mess = 'At least one Temperature must be set'
                    
                 endelse
                
                               
                if err EQ 0 then begin
                   formdata = { procval  : info.procval,    $
                                cancel   : 0                }
                    *info.ptrToFormData = formdata
                    widget_control, event.top, /destroy
                endif else begin
                   widget_Control, info.messID, Set_Value=mess
                endelse

             end

  
  
  else : print,'ADAS801_IN_OPT_EVENT : You should not see this message! '
                   
ENDCASE

END
  
;-----------------------------------------------------------------------------


FUNCTION adas801_in_opt, procval, a04_type,         $
                         PARENT     = parent,       $
                         FONT_LARGE = font_large,   $
                         FONT_SMALL = font_small,   $
                         EDIT_FONTS = edit_fonts

; Set defaults for keywords 

  IF n_elements(font_large) eq 0 THEN font_large = ''
  IF n_elements(font_small) eq 0 THEN font_small = ''


; We want a modal top level base widget
  
  if n_elements(parent) eq 0 then begin
     widparent = Widget_Base(Column=1, Title='ADAS 801 INPUT - EXTRA OPTIONS', $
                             XOFFSET=200, YOFFSET=100   )
  endif else begin              
     widparent = Widget_Base(Column=1, Title='ADAS 801 INPUT - EXTRA OPTIONS', $
                             XOFFSET=200, YOFFSET=100,                 $
                             group_leader=parent, modal=1)
  endelse


; Display whether Type 1 or 3
                
  str = 'Type ' + string(a04_type, format='(i1)') + ' adf04 file'
  if a04_type EQ 0 then str = 'Structure run only'            
  
  rc      = widget_label(widparent, value='  ',font=font_large)
  tylab   = widget_label(widparent, value=str, font=font_large)   


  topbase = widget_base(widparent, /row)
  base    = widget_base(topbase, /column)
  
  tylab   = widget_label(base,font=font_large, value='Options:-')   
  tybase  = widget_base(base, /column,/frame)



; On the left select options and scaling parameters
  
  ltop = '                      Parity 1/Parity 2'
  le2  = 'Electric quadrupole :'
  lm1  = 'Magnetic Dipole     :'
  buttons = ['     ',' ']     
  e2ID = cw_bgroup(tybase,buttons,                            $
                       label_left=le2,  label_top=ltop,       $
                       nonexclusive=1,row=1,                  $
                       event_func='ADAS_PROC_NULL_EVENTS',    $
                       font=font_small    )
  m1ID = cw_bgroup(tybase,buttons,                            $
                       label_left=lm1,                        $
                       nonexclusive=1,row=1,                  $
                       event_func='ADAS_PROC_NULL_EVENTS',    $
                       font=font_small    )



; scale factors

  tylab   = widget_label(base,font=font_large, value='Scale Factors:-')   
  tybase  = widget_base(base, /column,/frame)
  
  sID   = lonarr(5)
  
  stle  = ['F(ii) :','zeta  :','F(ij) :','G(ij) :','R     :']
  
  for j =0, 4 do begin
  
     sID[j]  = cw_field(tybase, title=stle[j], $
                        integer=1, fieldfont=font_small, font=font_small) 
  endfor


; In middle set of configurations to include. 

  incbase  = widget_base(topbase, /column)

  inclab   = widget_label(incbase,font=font_large, value='Retain configs')   
  incbase  = widget_base(incbase, /column,/frame)
  
  ind = where(strlen(procval.cfg1) GT 0 AND strpos(procval.cfg1, 'NULL') EQ -1, count)
  if count GT 0 then begin
     p1ID   = cw_bgroup(incbase,procval.cfg1[ind],             $
                        label_top='1st parity:',               $
                        nonexclusive=1,column=1,               $
                        set_value=intarr(count)+1,             $
                        event_func='ADAS_PROC_NULL_EVENTS',    $
                        font=font_small)
  endif else p1base = -1
  
  ind = where(strlen(procval.cfg2) GT 0 AND strpos(procval.cfg2, 'NULL') EQ -1, count)
  if count GT 0 then begin
     p2ID   = cw_bgroup(incbase,procval.cfg2[ind],             $
                        label_top='2nd parity:',               $
                        nonexclusive=1,column=1,               $
                        set_value=intarr(count)+1,             $
                        event_func='ADAS_PROC_NULL_EVENTS',    $
                        font=font_small)
  endif else p2base = -1
 

; On the right temperatures (only for Type 3). 

  tebase  = widget_base(topbase, /column)

  tylab   = widget_label(tebase,font=font_large, value='Temperatures:-')   
  tybase  = widget_base(tebase, /column,/frame)
  
  
  num_form   = '(E10.3)'
  temps      = procval.te  
  ind        = where(temps NE 0.0, ntemps)
  tabledata  = strarr(1,14)
  ntemps = procval.nte
  if ntemps GT 0 then $
      tabledata(0,0:ntemps-1) = strtrim(string(temps(0:ntemps-1),format=num_form),2)
 
 
  table_title = 'adf04 Temperatures'
  table_head  = ['    Te    ']

  tempID  = cw_adas_table(tybase, tabledata, coledit=[1],               $
               limits=[2], title=table_title, font_large=font_small,    $
               font_small=font_small, colhead=table_head,               $
               ytexsize=15, num_form=num_form, order=[1],               $
               fonts = edit_fonts,                                      $
               event_func='ADAS_PROC_NULL_EVENTS')


; Modifers of temperature

  unit_str = ['K   ', 'eV  ', 'Red.']
  unitID  = widget_droplist(tybase,value = unit_str,font=font_large, $
                            title='Units  :', event_pro = 'ADAS801_IN_OPT_UNIT')
 
  def_str = ['Standard  ','Low Temp. ','High Temp.']
  defID   = widget_droplist(tybase,value = def_str,font=font_large, $
                            title='Default:', event_pro = 'ADAS801_IN_OPT_DEF')
 
  clearID = widget_button(tybase, font=font_small, value=' Clear Table ', $
                          event_pro = 'ADAS801_IN_OPT_DEF')
  



; The usual buttons at the bottom
                
  mrow      = widget_base(widparent,/row,/align_center)
  messID    = widget_label(mrow,font=font_large, $
                          value='                         ')
                
  mrow      = widget_base(widparent,/row)
  
  cancelID  = widget_button(mrow,value='Cancel',font=font_large)
  doneID    = widget_button(mrow,value='Done',font=font_large)



; Initial settings
   
   CASE procval.iquad Of
      0 : 
      1 : widget_control, e2ID[0], set_value = [1,0]              
      2 : widget_control, e2ID[0], set_value = [0,1]              
      3 : widget_control, e2ID[0], set_value = [1,1] 
   ENDCASE              
   CASE procval.imag Of
      0 : 
      1 : widget_control, m1ID[0], set_value = [1,0]              
      2 : widget_control, m1ID[0], set_value = [0,1]              
      3 : widget_control, m1ID[0], set_value = [1,1] 
   ENDCASE              
     
   widget_control, sID[0], set_value = procval.fii
   widget_control, sID[1], set_value = procval.zeta
   widget_control, sID[2], set_value = procval.fij
   widget_control, sID[3], set_value = procval.gij
   widget_control, sID[4], set_value = procval.rij

   widget_control, unitID, set_droplist_select = procval.te_unit


; If adf04 is Type 1 or a structure run disable tebase

  if a04_type EQ 1 then widget_control, tebase, map=0
  if a04_type EQ 0 then widget_control, tebase, map=0


;------------------------------------------------
; Realize the ADAS801 extra options input widget.

   dynlabel, widparent
   widget_Control, widparent, /realize

; Create a pointer to store the information collected from the form.
; The initial data stored here is set to CANCEL, so nothing needs to
; be done if the user kills the widget with the mouse.

   ptrToFormData = Ptr_New({cancel:1})

; Create an info structure with program information.

  info = { doneID          :  doneID,             $
           messID          :  messID,             $
           e2ID            :  e2ID,               $
           m1ID            :  m1ID,               $
           sID             :  sID,                $
           p1ID            :  p1ID,               $
           p2ID            :  p2ID,               $
           tempID          :  tempID,             $
           te_unit         :  procval.te_unit,    $
           num_form        :  num_form,           $
           unitID          :  unitID,             $
           font_large      :  font_large,         $
           procval         :  procval,            $
           ptrToFormData   :  ptrToFormData       }  
               
; Store the info structure in the user value of the top-level base and launch
; the widget into the world.

  widget_control, widparent, Set_UValue=info

  XManager, 'adas801_in_opt', widparent, Event_Handler='ADAS801_IN_OPT_EVENT'


; When the widget is destroyed, the block is released, and we
; return here. Get the data that was collected by the widget
; and stored in the pointer location. Finally free the pointer.

formdata = *ptrToFormData

rep='CONTINUE'
IF N_Elements(formdata) EQ 0 THEN BEGIN
   Ptr_Free, ptrToFormData
   rep ='CANCEL'
   RETURN,rep
ENDIF
 
IF formdata.cancel EQ 1 THEN BEGIN
   Ptr_Free, ptrToFormData
   rep ='CANCEL'
ENDIF

if rep eq 'CONTINUE' then begin
   procval = formdata.procval
   Ptr_Free, ptrToFormData
endif       



RETURN,rep

END
