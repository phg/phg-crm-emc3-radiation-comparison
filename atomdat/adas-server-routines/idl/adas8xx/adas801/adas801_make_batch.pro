; Copyright (c) 2003 Strathclyde University .
; SCCS info: Module @(#)$Header: /home/adascvs/idl/adas8xx/adas801/adas801_make_batch.pro,v 1.1 2004/07/06 11:05:15 whitefor Exp $ Date $Date: 2004/07/06 11:05:15 $
;
;
; MODIFIED:
;       1.1     Martin O'Mullane
;                - First version
;
; VERSION:
;       1.1     09-09-03
;
;-----------------------------------------------------------------------
PRO adas801_make_batch, fortdir      , rand,          $
                        calctype     , num_cfg1,      $
                        rcn_file     , rcn_input,     $
                        rcn2_p1_file , rcn2_p1_input, $
                        rcn2_d_file  , cfp_file,      $
                        rcg_p1_file  , rcg_p1_input,  $
                        rcn2_p2_file , rcn2_p2_input, $
                        rcg_p2_file  , rcg_p2_input,  $
                        ifg_file     ,                $
                        icout        , ppic_file,     $
                        lsout        , ppls_file,     $
                        list

; Continue until there is a problem

mess = 'OK'


; Determine whether perl is available

loc_perl = file_which(getenv('PATH'), 'perl')

if loc_perl EQ '' then begin
   
   mess = 'NO PERL'
   return
   
endif



; Make list of commands

list = ''

; 1. RCN

  binary = fortdir+'/rcn.x'
  list = [list, binary + ' < ' + rcn_file, $
                'rm -f ' + rcn_file + ' ' + rcn_input]

; 2. RCN2 part 1

  binary = fortdir+'/rcn2.x'
  list = [list, binary + ' < ' + rcn2_p1_file,  $
                'rm -f ' + rcn2_P1_file + ' ' + rcn2_p1_input]


; 3. RCG part 1                
;
;  - modify RCN2 output by writing a perl program


   file = 'rcn2_p1_mod' + rand

   openw, lun, file, /get_lun
   
   printf, lun, '#!' + strcompress(loc_perl)
   printf, lun, '$file = "' + cfp_file + '";'
   printf, lun, 'open(FILE, "$file")  or die "No cfp file $file: $!";'
   printf, lun, '@cfp_file = <FILE>;'
   printf, lun, 'close FILE;'

   printf, lun, '$file = "' + rcn2_d_file + '";'
   printf, lun, 'open(FILE, "$file")  or die "cannot open $file: $!";'
   printf, lun, '@d_file = <FILE>;'
   printf, lun, 'close FILE;'


   printf, lun, '$line = $d_file[0];'

   printf, lun, 'chop($v1 = substr($line, 0,6));'
   printf, lun, '$v1 =~ s/ //g;'
   printf, lun, 'chop($v3 = substr($line, 7,2));'
   printf, lun, '$v3 =~ s/ //g;'
   printf, lun, 'chop($v4 = substr($line, 9,2));'
   printf, lun, '$v4 =~ s/ //g;'
   printf, lun, 'chop($p2 = substr($line, 10));'

   printf, lun, 'if (!$v3) {'
   printf, lun, '   $v3 = '' 0'';'
   printf, lun, '}'

   printf, lun, '@out_str = ("N", "$v1", "' +                  $
                  strcompress(string(num_cfg1), /remove_all) + $
                 '", "$v3", "$v4", "$p2");'


   printf, lun, '$file = "' + rcg_p1_input + '";'

   printf, lun, 'open(FILE, "> $file")  or die "cannot open $file: $!";'
   printf, lun, 'print  FILE @cfp_file;'
   printf, lun, 'printf FILE "%1s%2s%2s%2s %2s%s\n", @out_str;'
   printf, lun, 'print  FILE @d_file[1..$#d_file-1];'
   printf, lun, 'print  FILE " -1\n";
   printf, lun, 'close FILE;'
   printf, lun, '0;'
   
   free_lun, lun
   
   list = [list, 'chmod +x ' + file, file]
   
   
   binary = fortdir+'/rcg.x'
   list   = [list, binary + ' < ' + rcg_p1_file,  $
             'rm -f ' + rcg_p1_file + ' ' + rcg_p1_input + ' ' + file]
                  
                  
; Continue to collision calculation ?


if calctype EQ 0 then return


; 4. RCN2 part 2

  binary = fortdir+'/rcn2.x'
  list = [list, binary + ' < ' + rcn2_p2_file,  $
               'rm -f ' + rcn2_p2_file + ' ' + rcn2_p2_input]


; 5. RCG part 2                
;
;  - modify RCN2 output by writing a perl program


   file = 'rcn2_p2_mod' + rand

   openw, lun, file, /get_lun
   
   printf, lun, '#!' + strcompress(loc_perl)
   printf, lun, '$file = "' + cfp_file + '";'
   printf, lun, 'open(FILE, "$file")  or die "No cfp file $file: $!";'
   printf, lun, '@cfp_file = <FILE>;'
   printf, lun, 'close FILE;'

   printf, lun, '$file = "' + rcn2_d_file + '";'
   printf, lun, 'open(FILE, "$file")  or die "cannot open $file: $!";'
   printf, lun, '@d_file = <FILE>;'
   printf, lun, 'close FILE;'


   printf, lun, '$line = $d_file[0];'

   printf, lun, 'chop($v1 = substr($line, 0,6));'
   printf, lun, '$v1 =~ s/ //g;'
   printf, lun, 'chop($v3 = substr($line, 7,2));'
   printf, lun, '$v3 =~ s/ //g;'
   printf, lun, 'chop($v4 = substr($line, 9,2));'
   printf, lun, '$v4 =~ s/ //g;'
   printf, lun, 'chop($p2 = substr($line, 10));'

   printf, lun, 'if (!$v3) {'
   printf, lun, '   $v3 = '' 0'';'
   printf, lun, '}'

;   printf, lun, '@out_str = ("N", "$v1", "$npar1", "$v3", "$v4", "$p2");'
   printf, lun, '@out_str = ("N", "$v1", "' +                  $
                  strcompress(string(num_cfg1), /remove_all) + $
                 '", "$v3", "$v4", "$p2");'


   printf, lun, '$file = "' + rcg_p2_input + '";'

   printf, lun, 'open(FILE, "> $file")  or die "cannot open $file: $!";'
   printf, lun, 'print  FILE @cfp_file;'
   printf, lun, 'printf FILE "%1s%2s%2s%2s %2s%s\n", @out_str;'
   printf, lun, 'print  FILE @d_file[1..$#d_file-1];'
   printf, lun, 'print  FILE " -1\n";
   printf, lun, 'close FILE;'
   printf, lun, '0;'
   
   free_lun, lun
   
   list = [list, 'chmod +x ' + file, file]
   
   
   binary = fortdir+'/rcg.x'
   list   = [list, binary + ' < ' + rcg_p2_file,  $
             'rm -f ' + rcg_p2_file + ' ' + rcg_p2_input + ' ' + file]
                  
                  

; 6. Intermediate file - IFG

  binary = fortdir+'/ifg.x'
  list = [list, binary + ' < ' + ifg_file,  'rm -f ' + ifg_file]
  
  
; 7.  Post processing

  if icout EQ 1 then begin
  
     binary = fortdir+'/ifgpp.x'
     list = [list, binary + ' < ' + ppic_file,  'rm -f ' + ppic_file]
  
  
  endif

  if lsout EQ 1 then begin
  
     binary = fortdir+'/ifgpp.x'
     list = [list, binary + ' < ' + ppls_file,  'rm -f ' + ppls_file]
  
  
  endif

   
END
