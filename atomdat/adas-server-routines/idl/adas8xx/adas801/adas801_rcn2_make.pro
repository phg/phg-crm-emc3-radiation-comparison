; Copyright (c) 2000 Strathclyde University .
; SCCS info: Module @(#)$Header: /home/adascvs/idl/adas8xx/adas801/adas801_rcn2_make.pro,v 1.3 2004/07/06 11:05:33 whitefor Exp $ Date $Date: 2004/07/06 11:05:33 $
;+
; PROJECT:
;       ADAS 
;
; NAME:
;	ADAS801_RCN2_MAKE
;
; PURPOSE: 
;       Makes the RCN2 input file. 
;
;
; EXPLANATION:
;     
;
; NOTES:
;
;
; INPUTS:
;
; OPTIONAL INPUTS:
;       None
;
; OUTPUTS:
;
; OPTIONAL OUTPUTS:
;       None
;
; KEYWORD PARAMETERS:
;
; CALLS:
;
; SIDE EFFECTS:
;
; CATEGORY:
;	
;
; WRITTEN:
;       Martin O'Mullane
;
; MODIFIED:
;	1.1	Martin O'Mullane
;		 - First release 
;	1.2	Martin O'Mullane
;		- typo ncgg1 -> num_cfg1 
;	1.3	Martin O'Mullane
;		- Add defaults for type 1 adf04 files. 
;
; VERSION:
;       1.1	12-10-1999
;       1.2	03-01-2001
;       1.3	06-12-2002
;-
;-----------------------------------------------------------------------------



PRO ADAS801_RCN2_MAKE, part, type, procval,                             $
                       num_cfg1, num_cfg2, z0, z1, text, num, error

  error = ''

  text = ''

; If options have been set by the user use these - otherwise set the
; defaults for the ion and from type of calculation.

  if procval.options_set EQ 'YES' then begin
     
     irnq   = procval.irnq
     irxq   = procval.irxq
     irnd   = procval.irnd
     irxd   = procval.irxd
     iquad  = procval.iquad
     imag   = procval.imag
     fii    = procval.fii
     zeta   = procval.zeta
     fij    = procval.fij
     gij    = procval.gij
     rij    = procval.rij
  
  endif else begin
  
     CASE type OF
     
        0: begin
             irnq   = 0
             irxq   = 0
             irnd   = 0 
             irxd   = 0
             iquad  = 0
             imag   = 0
           end
        1:  begin
             irnq   = 0
             irxq   = 2
             irnd   = 1 
             irxd   = 1
             iquad  = 0
             imag   = 0
           end
        2:  begin
             irnq   = 0
             irxq   = 2
             irnd   = 1 
             irxd   = 1
             iquad  = 3
             imag   = 3
           end
        3:  begin
             irnq   = 0
             irxq   = 2
             irnd   = 1 
             irxd   = 1
             iquad  = 0
             imag   = 0
           end
        4:  begin
             irnq   = 0
             irxq   = 2
             irnd   = 1 
             irxd   = 1
             iquad  = 3
             imag   = 3
           end
           
     ENDCASE


     fii   = 80
     zeta  = 90
     fij   = 80
     gij   = 80
     rij   = 80

     if z0 EQ 1 then begin
    
        fii   = 75
        zeta  = 95
        fij   = 75
        gij   = 75
        rij   = 60
        
     endif
     
     test = z0-z1
     
     if test GE 5 AND test LE 10 then begin
     
        fii   = 80
        zeta  = 99
        fij   = 85
        gij   = 85
        rij   = 85
     
     endif   
  
     if test GT 10 then begin
     
        fii   = 93
        zeta  = 99
        fij   = 93
        gij   = 93
        rij   = 93
     
     endif   
  
     procval.irnq  = irnq
     procval.irxq  = irxq
     procval.irnd  = irnd
     procval.irxd  = irxd
     procval.iquad = iquad
     procval.imag  = imag
     procval.fii   = fii
     procval.zeta  = zeta
     procval.fij   = fij
     procval.gij   = gij
     procval.rij   = rij
  
  endelse


; Now write the rcn2 input file to 'text' array

  text = 'g5inp     000 0.00000         00'
  
  if imag  EQ 0 then tmp1 = ' ' else tmp1 = string(imag, format='(i1)')
  if iquad EQ 0 then tmp2 = ' ' else tmp2 = string(iquad, format='(i1)')
  tmp3 = string(fii,zeta,fij,gij,rij, format='(5i2)')

  if part EQ 'ONE' then tmp0 = '     ' else  $
                        tmp0 = '9' + string(irnq, irxq, irnd, irxd, format='(4i1)')
  
  text = text + tmp0 + '           ' + tmp1 + tmp2 + tmp3 + '      0111 1020'
  
  text = [text,'       -1']
  

      
; Finally determine the number of iteration steps - implicit assumption here
; is that imag and iquad are both set.

  
  neven = 1 + (irxq - irnq)/2
  nodd  = 1 + (irxd - irnd)/2
  if part EQ 'ONE' then begin
  
     case imag of
        0 : num = num_cfg1*num_cfg2
        1 : num = num_cfg1*num_cfg1 + num_cfg1*num_cfg2
        2 : num = num_cfg2*num_cfg2 + num_cfg1*num_cfg2
        3 : num = num_cfg1*num_cfg1 + num_cfg2*num_cfg2 + num_cfg1*num_cfg2
     endcase
     
  endif else begin
  
     case imag of
        0 : num = nodd*(num_cfg1*num_cfg2)
        1 : num = neven*(num_cfg1*num_cfg1) + nodd*(num_cfg1*num_cfg2)
        2 : num = neven*(num_cfg2*num_cfg2) + nodd*(num_cfg1*num_cfg2)
        3 : num = neven*(num_cfg1*num_cfg1 + num_cfg2*num_cfg2) + $
                  nodd*(num_cfg1*num_cfg2)
     endcase 
  
  endelse
       
  num = num + 1      

END
