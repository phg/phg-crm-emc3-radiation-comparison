; Copyright (c) 2000 Strathclyde University .
; SCCS info: Module @(#)$Header: /home/adascvs/idl/adas8xx/adas801/adas801_rcg_make.pro,v 1.4 2004/09/09 14:18:53 mog Exp $ Date $Date: 2004/09/09 14:18:53 $
;+
; PROJECT:
;       ADAS 
;
; NAME:
;	ADAS801_RCG_MAKE
;
; PURPOSE: 
;       Makes the RCG input file. 
;
;
; EXPLANATION:
;     
;
; NOTES:
;
;
; INPUTS:
;
; OPTIONAL INPUTS:
;       None
;
; OUTPUTS:
;
; OPTIONAL OUTPUTS:
;       None
;
; KEYWORD PARAMETERS:
;
; CALLS:
;
; SIDE EFFECTS:
;
; CATEGORY:
;	
;
; WRITTEN:
;       Martin O'Mullane
;
; MODIFIED:
;	1.1	Martin O'Mullane
;		 - First release 
;	1.2	Richard Martin
;		 - Added /sh keyword to spawn commnds.
;	1.3	Martin O'Mullane
;		 - Make loop variable which reads xdata.dat CFP
;                  of type long.
;                - Add output to paper.txt. We do not write the
;                  CFP data as it would make the files too large.
;                  Put a note to this effect in paper.txt.
;       1.4     Martin O'Mullane
;                - New location for the coefficients of fractional
;                  parentage.
;
; VERSION:
;       1.1	12-10-1999
;	1.2	15-11-2000
;	1.3	06-12-2002
;       1.4     09-09-2004
;
;-
;-----------------------------------------------------------------------------



PRO ADAS801_RCG_MAKE, part, procval, centroot, fortdir,        $
                      infile, script,                          $
                      num_cfg1, num_cfg2, num_spectr, error,   $
                      is_out, lun_out

  error = ''
  
  num_spectr = 0
  CASE procval.imag OF
     0 : num_spectr = 0
     1 : num_spectr = 1
     2 : if num_cfg2 GT 0 then num_spectr = 1
     3 : if num_cfg2 GT 0 then num_spectr = 2
  ENDCASE
  CASE procval.iquad OF
     0 : 
     1 : num_spectr = 1 + num_spectr
     2 : if num_cfg2 GT 0 then num_spectr = 1 + num_spectr
     3 : if num_cfg2 GT 0 then num_spectr = 2 + num_spectr
  ENDCASE
  
  if num_cfg1 GT 0 AND num_cfg2 GT 0 then num_spectr = 1 + num_spectr 


  i1 = 0 & i2 = 0 & i3 = 0 & i4 = 0

  cfp_file  = centroot + '/adf00/cfp.dat'
  command_a = 'cat ' + cfp_file
  command_b = 'cat ' + infile
               
  spawn, command_a, result_a, /sh
  spawn, command_b, result_b, /sh
  result_a = strlowcase(result_a)
  result_b = strlowcase(result_b)
  
  
  openw, lun, script, /get_lun
  for j =0L, n_elements(result_a)-1 do begin
    printf, lun, result_a[j]
  endfor
  
; Modify RCG input from that passed by RCN2 for the ADAS version of RCG

  str = result_b[0]
  reads, str, i1,i2,i3,i4, format = '(i5,i1,i2,i2)'
  str2 = 'N' + string(i1,num_cfg1,i3,i4, format = '(i2,i2,i2,1x,i2)') + $
               strmid(str, 10)
  printf, lun, str2
  
  for j = 1L, n_elements(result_b)-2 do begin
    printf, lun, result_b[j]
  endfor
  printf, lun, ' -1'
  
  free_lun, lun


; If paper.txt output is active

  if is_out then begin
   
     printf, lun_out,'File : '+script
     printf, lun_out,'Add ' + cfp_file 
     printf, lun_out,'to top of this file to give full input to rcg.'
     printf, lun_out,' '
     
     printf, lun_out, str2
     for j = 1L, n_elements(result_b)-2 do printf, lun_out, result_b[j]
     printf, lun_out, ' -1'

     printf, lun_out,' '
     printf, lun_out,'---------'
     printf, lun_out,' '

  endif

END
