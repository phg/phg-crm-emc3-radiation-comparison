; Copyright (c) 2000 Strathclyde University.
;+
; PROJECT:
;       ADAS
;
; NAME:
;       ADAS801_CALC
;
; PURPOSE:
;       This routine does the work - prepares the input ans script
;       files and pops up a progress bar indicating how things are
;       progressing.
;
;
; EXPLANATION:
;
;
; NOTES:
;       Uses IDL v5 (and above) features such as pointers to pass data.
;
;
;
; INPUTS:
;        inval       - A structure holding the 'remembered' input data.
;                        ROOTPATH     : root file 1
;                        FILE         : filename 1
;                        CENTROOT     : central adas of root 1
;                        USERROOT     : user path of file 1
;
; OPTIONAL INPUTS:
;       None
;
; OUTPUTS:
;       The function returns a string rep
;           rep = 'CONTINUE' if everything is ok
;           rep = 'CANCEL' if there is a problem or indeed if the user
;                          closes the widget with the mouse.
;
; OPTIONAL OUTPUTS:
;       None
;
; KEYWORD PARAMETERS:
;       FONT_LARGE - The name of a larger font e.g 'courier_bold14'
;
;       FONT_SMALL - The name of a smaller font e.g 'courier_bold12'
;
;
; CALLS:
;       ADAS801_TEST_NLINES  : Determines number of spectral lines
;                              from spectru#<EL>.dat file and
;                              compare these to ktran (of RCG).
;       ADAS801_WRITE_PAPER  : Writes copy of inout file to paper.txt.
;       ADAS801_CALC_PLOT    : Updates progress bar by calculating how
;                              many steps are left. Different methods
;                              for the various steps.
;
; SIDE EFFECTS:
;       A lot of spawning of fortran executables and system commands.
;
;
; CATEGORY:
;       Widgets
;
; WRITTEN:
;       Martin O'Mullane
;
; MODIFIED:
;       1.1     Martin O'Mullane
;                - First release
;       1.2     Martin O'Mullane
;                - Add option for isonuclear style output.
;       1.3     Martin O'Mullane
;                - Forgot case where only one parity is selected so
;                  we only have M1 and E2. Previous version assumed
;                  that E1 was always present.
;       1.4     Martin O'Mullane
;                - Add modifications to output type 1 adf04 files.
;                - Send copies of temporary files to paper.txt.
;       1.5     Martin O'Mullane
;                - Change names of binaries to align them with
;                  the offline adas8#1 version. (now .x was .out).
;       1.6     Martin O'Mullane
;                - Check that the number of expected collisions are
;                  less than RCG's ktran dimension. This addition
;                  required a change in RCG to output ktran at the
;                  start of the run. ADAS801_CALC_PLOT is altered
;                  to read this number when the /ktran switch is
;                  active.
;       1.7     Martin O'Mullane
;                - New location for the coefficients of fractional
;                  parentage.
;       1.8     Allan Whiteford
;                 - Changed rstrpos to strpos with /reverse_search.
;       1.9     Martin O'Mullane
;                - Take number of temperatures from procval as set by user.
;       1.10    Martin O'Mullane
;                - Changes to enable the user to select which configurations 
;                  to include in the adf04 output file.
;                - Use fortran code in offline location.
;                - Remove trailing blanks in the configuration strings sent
;                  to ifg to avoid IDL hanging. 
;       1.11    Martin O'Mullane
;                - Control the output format of the number of configurations
;                  in the driver file for rcg part 1 and 2.
;       1.12    Martin O'Mullane
;                - Check for NULL when calculating the number of configurations
;                  in the ifgpp driver.
;       1.13    Martin O'Mullane
;                - Allow up to 20 parities in ifgpp driver file. Matched
;                  to number in RCN2 second run.
;
; VERSION:
;       1.1     12-10-1999
;       1.2     05-10-2000
;       1.3     04-01-2001
;       1.4     06-12-2002
;       1.5     16-04-2003
;       1.6     17-06-2003
;       1.7     09-09-2004
;       1.8     13-04-2005
;       1.9     04-08-2008
;       1.10    12-01-2012
;       1.11    08-09-2012
;       1.12    16-01-2013
;       1.13    30-03-2015
;
;-
;-----------------------------------------------------------------------------

PRO ADAS801_WRITE_PAPER, lun_out, file

; Write contents of file to fileout (paper.txt). This must be opened
; in the calling subroutine.

printf, lun_out,'File : '+file
printf, lun_out,' '

openr, lun, file, /get_lun

str = ''
while NOT EOF(lun) do begin

   readf, lun, str
   printf, lun_out, str

endwhile
free_lun, lun

printf, lun_out,' '
printf, lun_out,'---------'
printf, lun_out,' '


END

;-----------------------------------------------------------------------------


FUNCTION ADAS801_TEST_NLINES, specfile, ktran

; Check the number of spectrum lines against maximun dimension
; for generalised oscillator strengths.

str = ''
ind = -1
k   = 0
openr, lun, specfile, /get_lun
while not eof(lun) do begin
   readf, lun, str, format='(a10)'
   if str EQ '   mag dip' then ind = [ind, k]
   if str EQ '  elec qud' then ind = [ind, k]
   if str EQ '  elec dip' then ind = [ind, k]
   k = k + 1
endwhile
ind = [ind, k-1]
ind = ind[1:*]

max_lines = abs(ind-shift(ind,1))
max_lines = max_lines[1:*]        ; ignore wrap around value


if max(max_lines) LT ktran then begin

   rep = 'CONTINUE'

endif else begin

   rep = 'PROBLEM'

   print, 'Too many transitions'
   print, 'Breakdown - M1, E2, E1     : ', max_lines
   print, 'Maximum allowed (per type) : ', ktran

endelse

return, rep

END
;-----------------------------------------------------------------------------



PRO ADAS801_CALC_PLOT, binary, infile, pipe, pid, num, $
                       tran_info,                      $
                       get_lines=get_lines, rcg2=rcg2, $
                       ktran=ktran


; get_lines is for RCG part 1 which records the number of spectrum
; lines in the calculation. It returns in num and is counted by
; summing the non-zero update values from RCG. If is_batch is set to -1
; then spectr returns the number of spectrum lines rather than a 0.
;
; The other option is rcg2 which invokes....
;
; If set the ktran keyword reads ktran from RCG.

; Assume that the time scales linearly in this calculation. Update the
; information widget in stages with num being the number of stages.


; Erase the previous progress bar

    erase

; Start the fortran sub-program and write the instruction file to it.

    adas_readfile, file=infile, all=all, nlines=num_lines

    spawn, binary, unit=pipe, /noshell, PID=pid

    for j = 0, num_lines-1 do begin
       res = find_process(pid)
       printf, pipe, all[j]
    endfor

; Find ktran dimension from RCG

  if keyword_set(ktran) GT 0 then begin

     ndtran = 0L
     readf, pipe, ndtran

  endif

; Get events from sub-program and update progress bar.

    next = 0
    step = 1000.0/num

    nlines = 0
    numtot = 0
    for i=0,num-1 do begin

      ON_IOERROR, jump_out
      readf, pipe, next
      p = float(i)/num*1000
      q = p+step

      for j=fix(p),fix(q) do begin
          x = (float(j)+1)/1000.0
          plots, [x, x],[0.0,1.0], /normal
      endfor

      if next GT -1 then begin
         nlines = [nlines, next]     ; for part 1
         numtot = numtot + next      ; for part 2
      endif

    endfor
jump_out:

; RCG part 2 may produce more collision strengths than A values so
; read the excess ouptut from RCG here.

    if keyword_set(rcg2) then begin

       nextra = numtot/10 - num
       for j = 0, nextra-1 do begin
          if find_process(pid) EQ 1 then readf, pipe, next
       endfor
    endif

; In RCG part 1 spectr returns the number of lines for M1 and E2 separately.
; The max of these is, usually E2, somewhat less than the number of collision
; strengths for the forbidden transitions. It is close but M1+E2 is too much.
; We must also test for IMSAG/IQUAD settings. code below assumes both are set.

    if keyword_set(get_lines) then begin
       nlines = nlines[1:*]
       nx     = n_elements(nlines)
       CASE nx OF
             1 : num = nlines[0]
             2 : num = max(nlines[0:1])
             3 : num = nlines[2] + max(nlines[0:1])
             5 : num = nlines[4] + max(nlines[0:1]) + max(nlines[2:3])
          ELSE : print,'Got that wrong!'
       ENDCASE
    endif


; Wait for the fortran to end before freeing the pipe and
; deleting the driver file.

    while find_process(pid) EQ 1 do temp = 0
    free_lun,pipe

    command = 'rm -f '+infile
    spawn,command

; Pass back dimensions and number of lines per type

  if keyword_set(ktran) GT 0 then tran_info = ndtran $
                             else tran_info = -1

END

;-----------------------------------------------------------------------------
PRO ADAS801_CALC, inval, procval, outval,                 $
                  centroot, fortdir, rand, batch, header, $
                  FONT_LARGE = font_large,                $
                  FONT_SMALL = font_small

; More nuanced location of executable code

  fortdir_int = fortdir + '/../../offline_adas/adas8#1/bin'

; Set defaults for keywords

  IF n_elements(font_large) eq 0 THEN font_large = ''
  IF n_elements(font_small) eq 0 THEN font_small = ''


; Print all temporary files to paper.txt

  if outval.texout EQ 1 then begin
     is_out = 1
     openw, lun_out, outval.texdsn, /get_lun

     printf, lun_out, header
     printf, lun_out, ' '
     printf, lun_out, '---------'
     printf, lun_out, ' '

     start_time = xxdate()

  endif else is_out = 0



; The stages of the calculation

    status_vals  = [' Prep ',' RCN ', ' RCN2-1 ', ' RCG-1 ', $
                    ' RCN-2 ',' RCG-2 ',                     $
                    ' IFG ', ' PP-IC ', ' PP-LS ']
    current_vals = ['Create Files',              $
                    'Radial wavefunctions',      $
                    'Dipole integrals',          $
                    'Transition Probabilities',  $
                    'Bessel Functions',          $
                    'Collision Strengths',       $
                    'Gather Data',               $
                    'Make IC adf04 file',        $
                    'Make LS adf04 file'         ]



; Create an information widget which is updated for interactive operation.
; It is put up in batch operation when making the files.

    widget_control, /hourglass
    base  = widget_base(/column, xoffset=300, yoffset=200,           $
                        title = "ADAS801: INFORMATION")

    lab   = widget_label(base, value=' ')
    lab   = widget_label(base,  font=font_large,        $
              value='   ADAS801 Computation Underway - Please Wait   ')
    lab   = widget_label(base, value=' ')

    mrow  = widget_base(base,/row,/align_center)



; Only highlight part currently working. grey out the rest.

    statusID = lonarr(n_elements(status_vals))
    for j=0, n_elements(status_vals)-1 do begin
       statusID[j] = widget_label(mrow, value=status_vals[j],  $
                                  font=font_small)
       widget_control, statusID[j], sensitive=0
    endfor


; Size of the progress bar is hardwired

    barbase   = widget_base(base,/align_center)

    grap      = widget_draw(barbase, ysize=20, xsize=480)

    mlab      = widget_label(base, value = ' ')
    currentID = widget_label(base, value = current_vals[0], font=font_small)


; Make labels resize dynamically and set it going.

    dynlabel, base
    widget_control, base, /realize



;------- Now do the work -----
;
; All temporary work to a random number (passed in to this subroutine)
;
;-----------------------------


  widget_control, statusID[0], sensitive=1


;---------------------------------------
; Step 1 - Make the input files
;

; The passing files are identified by appending the input filename
; to rcn_out# etc.

  ind_s  = strpos(inval.file,'/',/reverse_search)
  if ind_s NE -1 then  passtag = strmid(inval.file,ind_s+1) $
                 else  passtag = inval.file

; RCN - for all. First make the rcn input file and then the driver
;                file for the RCN program.

  tmp = inval.rootpath + inval.file
  adas801_rcn_analyse, tmp, num_cfg1, num_cfg2, cfg1, cfg2, z0, z1, text, error

  rcn_input = 'rcn_input'+rand
  openw, lun, rcn_input, /get_lun
  for j =0, n_elements(text)-1 do begin
    printf, lun, text[j]
  endfor
  free_lun, lun
  if is_out then adas801_write_paper, lun_out, rcn_input

  num_rcn = 2 * ( num_cfg1 + num_cfg2 )


  rcn_file = 'rcn'+rand+'.in'
  openw, lun, rcn_file, /get_lun

  if batch EQ 'NO' then printf, lun, '0' else  printf, lun, '1'
  printf, lun, rcn_input
  printf, lun, outval.passroot + 'rcn_out#'+passtag
  printf, lun, outval.passroot + 'd2_#'+passtag

  free_lun,lun
  if is_out then adas801_write_paper, lun_out, rcn_file


; RCN2 - part 1 - structure and rest

  part = 'ONE'
  type = inval.calctype
  adas801_rcn2_make, part, type, procval, num_cfg1, num_cfg2, z0, z1, text, $
                     num_rcn2_p1, error


  rcn2_p1_input = 'rcn2_p1_input'+rand
  openw, lun, rcn2_p1_input, /get_lun
  for j =0, n_elements(text)-1 do begin
    printf, lun, text[j]
  endfor
  free_lun, lun
  if is_out then adas801_write_paper, lun_out, rcn2_p1_input

  rcn2_p1_file = 'rcn2_p1'+rand+'.in'
  openw, lun, rcn2_p1_file, /get_lun

  if batch EQ 'NO' then printf, lun, '0' else  printf, lun, '1'
  printf, lun, rcn2_p1_input
  printf, lun, outval.passroot + 'd2_#'+passtag
  printf, lun, outval.passroot + 'rcn2_out#'+passtag
  printf, lun, outval.passroot + 'd_#'+passtag

  free_lun,lun
  if is_out then adas801_write_paper, lun_out, rcn2_p1_file


; RCG - part 1 - structure and A-values

  ; note: rcg_p1_input is a forward reference. It is made following
  ;       the running of rcn2.

  rcg_p1_file  = 'rcg_p1'+rand+'.in'
  rcg_p1_input = 'rcg_p1_input'+rand
  openw, lun, rcg_p1_file, /get_lun

  if batch EQ 'NO' then printf, lun, '-1  1' else  printf, lun, '1  1'

  i1 = where(procval.p1list NE 0, c1)
  printf, lun, c1, i1+1, format='(20(i3,:))'   ; fortran index
  i2 = where(procval.p2list NE 0, c2)
  printf, lun, c2, i2+1, format='(20(i3,:))'

  printf, lun, rcg_p1_input
  printf, lun, outval.passroot + 'rcg_out#'+passtag
  printf, lun, outval.passroot + 'spectrum#'+passtag

  free_lun,lun
  if is_out then adas801_write_paper, lun_out, rcg_p1_file

; Part 2 is applicable only for collision strength calculation

  type = inval.calctype
  if type GT 0 then begin


   ; RCN2 - part 2 - calculate Bessel functions for
   ;                 generalised oscillator strengths.


     part = 'TWO'
     adas801_rcn2_make, part, type, procval, num_cfg1, num_cfg2, z0, z1, text, $
                        num_rcn2_p2, error


     rcn2_p2_input = 'rcn2_p2_input'+rand
     openw, lun, rcn2_p2_input, /get_lun
     for j =0, n_elements(text)-1 do begin
       printf, lun, text[j]
     endfor
     free_lun, lun
     if is_out then adas801_write_paper, lun_out, rcn2_p2_input

     rcn2_p2_file = 'rcn2_p2'+rand+'.in'
     openw, lun, rcn2_p2_file, /get_lun

     if batch EQ 'NO' then printf, lun, '0' else  printf, lun, '1'
     printf, lun, rcn2_p2_input
     printf, lun, outval.passroot + 'd2_#'+passtag
     printf, lun, outval.passroot + 'rcn2_out#'+passtag
     printf, lun, outval.passroot + 'd_#'+passtag

     free_lun,lun
     if is_out then adas801_write_paper, lun_out, rcn2_p2_file


   ; RCG - part 2 - collision strength calculation


     rcg_p2_file  = 'rcg_p2'+rand+'.in'
     rcg_p2_input = 'rcg_p2_input'+rand
     openw, lun, rcg_p2_file, /get_lun

     if batch EQ 'NO' then printf, lun, '-2  2' else  printf, lun, '1  2'

     i1 = where(procval.p1list NE 0, c1)
     printf, lun, c1, i1+1, format='(20(i3,:))'   ; fortran index
     i2 = where(procval.p2list NE 0, c2)
     printf, lun, c2, i2+1, format='(20(i3,:))'

     printf, lun, rcg_p2_input
     printf, lun, outval.passroot + 'rcg_out#'+passtag
     printf, lun, outval.passroot + 'collision#'+passtag

     ; add the temperatures (must be in reduced units)


     tin   = procval.te
     tout  = fltarr(14)
     itype = procval.te_unit + 1
     jtype = 3
     xxtcon, itype, jtype, procval.z1, 14, tin, te

     ind   = where (te NE 0.0, nte)
     temps = fltarr(25)
     index = indgen(nte)+1

     temps[0:nte-1] = te[ind]
     if nte EQ 1 then inc = te[0]*0.2 else inc = te[nte-1] - te[nte-2]
     for j = nte, 24 do temps[j] = temps[j-1] + inc

     printf, lun, procval.z0, procval.z1-1
     printf, lun, temps, format='(8(e12.5,2x,:))'


     free_lun,lun
     if is_out then adas801_write_paper, lun_out, rcg_p2_file

   ; IFG - intermediate file generation

     num_ifg = 3 + (num_cfg1 ge 1) + (num_cfg2 ge 1)  +  $
                   (num_cfg1 gt 0 AND num_cfg2 gt 0 )
     if procval.imag EQ 1 OR procval.iquad EQ 1 then num_ifg = num_ifg + 1
     if procval.imag EQ 2 OR procval.iquad EQ 2 then num_ifg = num_ifg + 1
     if procval.imag EQ 3 OR procval.iquad EQ 3 then num_ifg = num_ifg + 2

     ifg_file  = 'ifg'+rand+'.in'
     openw, lun, ifg_file, /get_lun

     if batch EQ 'NO' then printf, lun, '0' else  printf, lun, '1'
     printf, lun, z0, z1, format='(2i5)'
     printf, lun, num_cfg1, num_cfg2, format='(2i5)'
     for j =0, num_cfg1-1 do printf, lun, strtrim(cfg1[j])
     if num_cfg2 GT 0 then for j =0, num_cfg2-1 do printf, lun, strtrim(cfg2[j])
     printf, lun, outval.passroot + 'rcn_out#'+passtag
     printf, lun, outval.passroot + 'd_#'+passtag
     printf, lun, outval.passroot + 'rcg_out#'+passtag
     printf, lun, outval.passroot + 'spectrum#'+passtag
     printf, lun, outval.passroot + 'collision#'+passtag
     printf, lun, outval.passroot + 'ifg#'+passtag

     free_lun,lun
     if is_out then adas801_write_paper, lun_out, ifg_file


   ; IFGPP - post process for adf04 file generation


     num_ifgpp = 5 + (num_cfg1 gt 0 AND num_cfg2 gt 0 )
     if procval.imag EQ 1 OR procval.iquad EQ 1 then num_ifgpp = num_ifgpp + 1
     if procval.imag EQ 2 OR procval.iquad EQ 2 then num_ifgpp = num_ifgpp + 1
     if procval.imag EQ 3 OR procval.iquad EQ 3 then num_ifgpp = num_ifgpp + 2

     comments = 'C'
     comments = [comments,'C     SCF method used                      :  HR', $
                 'C']

     str = string(procval.fii,procval.zeta, procval.fij, procval.gij,   $
                  procval.rij, format='(5i3)' )
     comments = [comments,'C     Scale factors for Slater Parameters  : ' + $
                 str, 'C']

     if num_cfg2 EQ 0 then str = ' No' else str = ' Yes'
     comments = [comments,'C     Optically allowed transitions        : ' +  $
                 str,'C']
     CASE procval.imag OF
        0 : str = ' None'
        1 : str = ' First Parity'
        2 : str = ' Second Parity'
        3 : str = ' Both Parities'
     ENDCASE
     comments = [comments,'C     Optically forbidden transitions (M1) : ' +  $
                 str,'C']
     CASE procval.iquad OF
        0 : str = ' None'
        1 : str = ' First Parity'
        2 : str = ' Second Parity'
        3 : str = ' Both Parities'
     ENDCASE
     comments = [comments,'C     Optically forbidden transitions (E2) : ' +  $
                 str,'C']
     str = string(procval.irnq, format='(i2)') + ' -> ' + $
           string(procval.irxq, format='(i2)')
     comments = [comments,'C     Born Collision-Strength  - forbidden : ' + $
                 str,'C']
     str = string(procval.irnd, format='(i2)') + ' -> ' + $
           string(procval.irxd, format='(i2)')
     comments = [comments,'C     Born Collision-Strength  - allowed   : ' + $
                 str,'C','C']

     numcom   = n_elements(comments)
     producer = xxuser()
     date     = xxdate()

     if outval.cfgstyle EQ 0 then isoval = "'NO'" else isoval = "'YES'"


     ; Special case of type 1 adf04 files

     typestr = "'UNKNOWN'"
     if procval.a04_type EQ 1 then begin
        typestr   = "'GAMMA'"
        num_ind   = n_elements(procval.ind_xpar)
        index_val = procval.ind_xpar + 1   ; it's fortran
     endif
     if procval.a04_type EQ 3 then begin
        typestr = "'RATES'"
        num_ind   = procval.nte
        index_val = index
     endif


     if outval.icout EQ 1 then begin

        ppic_file = 'pp_ic'+rand+'.in'
        openw, lun, ppic_file, /get_lun

        if batch EQ 'NO' then printf, lun, '0' else  printf, lun, '1'
        printf, lun, producer
        printf, lun, date[0]
        printf, lun, numcom
        for j = 0, numcom-1 do printf, lun, comments[j]

        line1 = ' &FILES ifgfile = ''' + outval.passroot + 'ifg#' + passtag + $
                ''' , outfile = ''' + outval.icdsn +''' &END '

        line2 = ' &OPTIONS  ip = ' + string(inval.ionpot) + ',' +              $
                            ' coupling = ''IC'' , ' +                          $
                            ' aval = ''YES'' , '    +                          $
                            ' isonuclear = ' + isoval + ',' +                  $
                            ' quantity = ' + typestr + ',' +                   $
                            ' lweight = ''NO'' , ' +                           $
                            ' comments = 2, ' +                                $
                            ' numtemp = ' + string(num_ind,format='(i2)') + ',' +  $
                            ' &END'
        printf, lun, line1
        printf, lun, line2

        printf, lun,  index_val, format = '(14(i2,1x,:))'

        i1  = where(procval.p1list NE 0, c1)
        res = where(procval.cfg1 NE 'NULL', n1)
        i2  = where(procval.p2list NE 0, c2)
        res = where(procval.cfg2 NE 'NULL', n2)

        printf, lun, 'parity-1 ' + string(n1, format='(i3)') + ' : ' + $
                     string(c1, format='(i3)') + $
                     string(i1+1, format='(20(i3,:))')

        printf, lun, 'parity-2 ' + string(n2, format='(i3)') + ' : ' + $
                     string(c2, format='(i3)') + $
                     string(i2+1, format='(20(i3,:))')

        free_lun,lun
        if is_out then adas801_write_paper, lun_out, ppic_file

     endif

     if outval.lsout EQ 1 then begin

        ppls_file = 'pp_ls'+rand+'.in'
        openw, lun, ppls_file, /get_lun

        if batch EQ 'NO' then printf, lun, '0' else  printf, lun, '1'
        printf, lun, producer
        printf, lun, date[0]
        printf, lun, numcom
        for j = 0, numcom-1 do printf, lun, comments[j]

        line1 = ' &FILES ifgfile = ''' + outval.passroot + 'ifg#' + passtag + $
                ''' , outfile = ''' + outval.lsdsn +''' &END '

        line2 = ' &OPTIONS  ip = ' + string(inval.ionpot) + ',' +              $
                            ' coupling = ''LS'' , ' +                          $
                            ' aval = ''YES'' , '    +                          $
                            ' isonuclear = ' + isoval + ',' +                  $
                            ' quantity = ' + typestr + ',' +                   $
                            ' lweight = ''NO'' , ' +                           $
                            ' comments = 2, ' +                                $
                            ' numtemp = ' + string(num_ind,format='(i2)') + ',' +  $
                            ' &END'
        printf, lun, line1
        printf, lun, line2

        printf, lun,  index_val, format = '(14(i2,1x,:))'

        i1  = where(procval.p1list NE 0, c1)
        res = where(procval.cfg1 NE 'NULL', n1)
        i2  = where(procval.p2list NE 0, c2)
        res = where(procval.cfg2 NE 'NULL', n2)

        printf, lun, 'parity-1 ' + string(n1, format='(i3)') + ' : ' + $
                     string(c1, format='(i3)') + $
                     string(i1+1, format='(20(i3,:))')

        printf, lun, 'parity-2 ' + string(n2, format='(i3)') + ' : ' + $
                     string(c2, format='(i3)') + $
                     string(i2+1, format='(20(i3,:))')

        free_lun,lun
        if is_out then adas801_write_paper, lun_out, ppls_file

     endif



  endif


;---------------------------
; Step 2 - Run the code
;---------------------------

  if batch EQ 'NO' then begin

     ; 1. RCN

       widget_control, statusID[0], sensitive=0
       widget_control, statusID[1], sensitive=1
       widget_control, currentID, set_value=current_vals[1]

       binary = fortdir_int+'/rcn.x'
       adas801_calc_plot, binary, rcn_file, pipe, pid, num_rcn

       command = 'rm -f '+ rcn_input
       spawn, command, /sh


     ; 2. RCN2 part 1

       widget_control, statusID[1], sensitive=0
       widget_control, statusID[2], sensitive=1
       widget_control, currentID, set_value=current_vals[2]

       binary = fortdir_int+'/rcn2.x'
       adas801_calc_plot, binary, rcn2_p1_file, pipe, pid, num_rcn2_p1

       command = 'rm -f '+rcn2_p1_input
       spawn, command, /sh


     ; 3. Add configuration data and run RCG part 1

       part   = 'ONE'
       infile = outval.passroot + 'd_#'+passtag
       adas801_rcg_make, part, procval, centroot, fortdir_int,     $
                         infile, rcg_p1_input,                     $
                         num_cfg1, num_cfg2, num_spectr, error,    $
                         is_out, lun_out

       num_rcg_p1 = 4 + num_spectr + (num_cfg1 ge 1) + (num_cfg2 ge 1)

       widget_control, statusID[2], sensitive=0
       widget_control, statusID[3], sensitive=1
       widget_control, currentID, set_value=current_vals[3]

       binary = fortdir_int+'/rcg.x'
       adas801_calc_plot, binary, rcg_p1_file, pipe, pid, num_rcg_p1, $
                          tran_info, /get_lines, /ktran

       command = 'rm -f '+rcg_p1_input
       spawn, command, /sh



     ; Stop now if structure calculation


       type = inval.calctype

       if type GT 0 then begin

       ; 3.5. Check that number of expected transitions is
       ;      compatiable with RCG dimensions.

          specfile = outval.passroot + 'spectrum#'+passtag
          rep      = adas801_test_nlines(specfile, tran_info)

          if rep EQ 'PROBLEM' then begin

             mess = [' There are too many spectrum lines   ', $
                     ' for the collision calculation to    ', $
                     ' proceed.                            ', $
                     '',                                      $
                     ' Reduce the number of configurations ', $
                     ' in the calculation.                 ', $
                     ''                                       ]

             tell = popup(message=mess, buttons=['Accept'],font=font_large)

             ; remove unused files for subsequent  parts

             command = 'rm -f '+ rcn2_p2_input + ' ' + rcn2_p2_file + ' ' + $
                                 rcg_p2_file   + ' ' + ifg_file
             if outval.icout EQ 1 then command = command + ' ' + ppic_file
             if outval.lsout EQ 1 then command = command + ' ' + ppls_file

             spawn, command, /sh

             ; jump out of this if block!

             goto, TooManyLines

          endif

       ; 4. RCN2 part 2

          widget_control, statusID[3], sensitive=0
          widget_control, statusID[4], sensitive=1
          widget_control, currentID, set_value=current_vals[4]

          binary = fortdir_int+'/rcn2.x'
          adas801_calc_plot, binary, rcn2_p2_file, pipe, pid, num_rcn2_p2

          command = 'rm -f '+rcn2_p2_input
          spawn, command, /sh


       ; 5. Add configuration data and run RCG part 2


          part   = 'TWO'
          infile = outval.passroot + 'd_#'+passtag
          adas801_rcg_make, part, procval, centroot, fortdir_int,  $
                            infile, rcg_p2_input,                  $
                            num_cfg1, num_cfg2, num_spectr, error, $
                            is_out, lun_out

          num_rcg_p2 = 4 + num_rcg_p1/10 + (num_cfg1 ge 1) + (num_cfg2 ge 1)

          widget_control, statusID[4], sensitive=0
          widget_control, statusID[5], sensitive=1
          widget_control, currentID, set_value=current_vals[5]


          binary = fortdir_int+'/rcg.x'
          adas801_calc_plot, binary, rcg_p2_file, pipe, pid, num_rcg_p2, $
                             tran_info, /rcg2, /ktran

          command = 'rm -f '+rcg_p2_input
          spawn, command, /sh


       ; 6. Intermediate file - IFG

          widget_control, statusID[5], sensitive=0
          widget_control, statusID[6], sensitive=1
          widget_control, currentID, set_value=current_vals[6]


          binary = fortdir_int+'/ifg.x'
          adas801_calc_plot, binary, ifg_file, pipe, pid, num_ifg


       ; 7. Post process the Intermediate file - IFGPP

         if outval.icout EQ 1 then begin

            widget_control, statusID[6], sensitive=0
            widget_control, statusID[7], sensitive=1
            widget_control, currentID, set_value=current_vals[7]


            binary = fortdir_int + '/ifgpp.x'
            adas801_calc_plot, binary, ppic_file, pipe, pid, num_ifgpp

         endif

         if outval.lsout EQ 1 then begin

            widget_control, statusID[6], sensitive=0
            widget_control, statusID[7], sensitive=0
            widget_control, statusID[8], sensitive=1
            widget_control, currentID, set_value=current_vals[8]


            binary = fortdir_int + '/ifgpp.x'
            adas801_calc_plot, binary, ppls_file, pipe, pid, num_ifgpp

         endif

       endif

TooManyLines:

     ; All Done - destroy progress bar.

       widget_control, base, /destroy


  endif else begin


     ; Get list of commands to run


       rcn2_d_file = outval.passroot + 'd_#'+passtag
       cfp_file    = centroot + '/adf00/cfp.dat'

       adas801_make_batch, fortdir_int      , rand,          $
                           type         , num_cfg1,      $
                           rcn_file     , rcn_input,     $
                           rcn2_p1_file , rcn2_p1_input, $
                           rcn2_d_file  , cfp_file,      $
                           rcg_p1_file  , rcg_p1_input,  $
                           rcn2_p2_file , rcn2_p2_input, $
                           rcg_p2_file  , rcg_p2_input,  $
                           ifg_file     ,                $
                           outval.icout , ppic_file,     $
                           outval.lsout , ppls_file,     $
                           list
       loc_sh = file_which(getenv('PATH'), 'sh')

       dscontrol = 'control'+rand+'.tmp'
       list = ['#!' + strcompress(loc_sh), list, 'rm -f ' + dscontrol]

       openw, lun, dscontrol, /get_lun
       for j = 0, n_elements(list)-1 do printf, lun, list[j]
       free_lun, lun

       spawn, 'chmod u+x '+ dscontrol

       ; Remove progress bar (file writing) and put up batch widget.

       widget_control, base, /destroy
       batch, dscontrol, title='ADAS801: INFORMATION', $
              jobname='NO. '+rand, font_large=font_large

  endelse


; Close output paper.txt

  if is_out then begin

     printf, lun_out, ' '
     printf, lun_out, '=================================================='
     printf, lun_out, ' '

     end_time = xxdate()

     printf, lun_out, 'Code       : ADAS801'
     printf, lun_out, 'Producer   : ' + xxuser()
     printf, lun_out, 'Date       : ' + start_time[0]
     printf, lun_out, ' '
     printf, lun_out, 'Start time : ' + start_time[1]
     printf, lun_out, 'End time   : ' + end_time[1]

     free_lun, lun_out

  endif


END
