; Copyright (c) 2000 Strathclyde University .
; SCCS info: Module @(#)$Header: /home/adascvs/idl/adas8xx/adas801/adas801.pro,v 1.9 2013/01/16 17:27:19 mog Exp $ Date $Date: 2013/01/16 17:27:19 $
;+
; PROJECT:
;       ADAS
;
; NAME:
;       ADAS801
;
; PURPOSE:
;       The highest level routine for the ADAS 801 program.
;
; EXPLANATION:
;       This routine is called from the main adas system routine,
;       adas.pro, to start the ADAS 801 application. This is the
;       first ADAS program to be fully written in IDL, which may
;       or may not be a good thing. 
;
;
;
; USE:
;       First the system settings must be established by calling
;       adas_sys_set.pro then adas801.pro is called to start the
;       ADAS 801 application;
;
;       adas_sys_set, adasrel, fortdir, userroot, centroot, $
;                     devlist, devcode, font_large, font_small, edit_fonts
;       adas801,   adasrel, fortdir, userroot, centroot, devlist, $
;                  devcode, font_large, font_small, edit_fonts
;
; INPUTS:
;       ADASREL - A string indicating the ADAS system version,
;                 e.g ' ADAS RELEASE: ADAS93 V1.4'.  The first
;                 character should be a space.
;
;       FORTDIR - A string holding the path to the directory where the
;                 FORTRAN executables are, e.g '/disk/adas/fortran/exec'
;                 Not used but retainned for compatibility.
;
;       USERROOT - A string holding the path to the root directory of
;                  the user's adas data. e.g '/disk/bowen/adas' This
;                  root directory will be used by adas to construct
;                  other path names.  In particular the user's default
;                  interface settings will be stored in the directory
;                  USERROOT+'/defaults'.  An error will occur if the
;                  defaults directory does not exist.
;
;       CENTROOT - Like USERROOT, but this directory points to the
;                  central data area for the system.  User defaults are
;                  not stored on CENTROOT.
;
;       DEVLIST - A string array of hardcopy device names, used for
;                 graphical output. e.g ['Post-Script','HP-PCL','HP-GL']
;                 This array must mirror DEVCODE.  DEVCODE holds the
;                 actual device names used in a SET_PLOT statement.
;                 Not used but retainned for compatibility.
;
;       DEVCODE - A string array of hardcopy device code names used in
;                 the SET_PLOT statement, e.g ['PS', 'PCL', 'HP']
;                 This array must mirror DEVLIST.
;                 Not used but retainned for compatibility.
;
;       FONT_LARGE - The name of a larger font e.g 'courier_bold14'
;
;       FONT_SMALL - The name of a smaller font e.g 'courier_bold12'
;
;       EDIT_FONTS - A structure of two fonts used in the table editor
;                    adas_edtab.pro, {font_norm:'',font_input:''}.
;                    The two fonts are used to differentiate between
;                    editable and non-editable parts of the table. You
;                    may have to experiment to get two fonts which give
;                    the same default spacing in the edit widget.
;                    e.g {font_norm:'helvetica_bold14', $
;                         font_input:'helvetica_oblique14'}
;                  Not used but retainned for compatibility.
;
; OPTIONAL INPUTS:
;       None
;
; OUTPUTS:
;       None - Note this routine should leave all inputs un-modified.
;
; OPTIONAL OUTPUTS:
;       None
;
; KEYWORD PARAMETERS:
;       None
;
; CALLS:
;       ADAS801_IN    Prompts for and gathers input files
;       ADAS801_PROC  Tests files for data consistency
;       ADAS801_OUT   Prompts for and writes output files
;       XXDATE        Get date and time from operating system.
;       POUP          Puts up a message on the screen.
;
; SIDE EFFECTS:
;       Some system calls in the called functions. Spawns some unix
;       specific commands.
;
; NOTES:
;       This program requires IDL v5. This is tested for at the beginning.
;       The mode of operation is different to other ADAS programs in that
;       the 3 called routines are functions which return the variable 'rep'.
;       rep can be 'CONTINUE', 'CANCEL' or 'MENU' and subsequent action
;       depends on the returned value.
;       As a side note there are no common blocks!
;
; CATEGORY:
;       Adas system.
;
; WRITTEN:
;       Martin O'Mullane
;
; MODIFIED:
;       1.1     Martin O'Mullane
;                - First release
;       1.2     Martin O'Mullane & Richard Martin
;                - Add extra options to outval.
;		 - Increased version no. to 1.2
;       1.3     Martin O'Mullane & Richard Martin
;		    Increased version no. to 1.3
;       1.4     Martin O'Mullane
;                - Add option to get ionisation potential from central ADAS
;                  adf00 files.
;                - Allow production of type 1 adf04 files.
;		 - Increased version no to 1.4
;       1.5     Richard Martin	
;                - Increased version no to 1.5 .
;       1.6     Martin O'Mullane
;                - New location for the coefficients of fractional
;                  parentage.
;       1.7     Martin O'Mullane
;                - Allow user to select which configurations to include
;                  in the output adf04 file (in Extra Options....).
;                - Use fortran in offline_adas/adas8#1.
;                - Increase version number.
;       1.8     Martin O'Mullane
;                - Increase number of even ad odd configuration in 
;                  procval to 20.
;
; VERSION:
;       1.1     20-01-2000
;       1.2     05-06-2000
;       1.3     14-03-2001
;       1.4     06-12-2002
;       1.5     09-09-2003
;       1.6     09-09-2004
;       1.7     12-01-2012
;       1.8     16-01-2013
;
;-
;-----------------------------------------------------------------------------

PRO ADAS801,    adasrel, fortdir, userroot, centroot,                   $
                devlist, devcode, font_large, font_small, edit_fonts


                ;************************
                ;**** Initialisation ****
                ;************************

    adasprog = ' PROGRAM: ADAS801 V1.6'
    deffile  = userroot+'/defaults/adas801_defaults.dat'
    bitfile  = centroot+'/bitmaps'
    device   = ''
                
   

                ;******************************************
                ;**** Search for user default settings ****
                ;**** If not found create defaults     ****
                ;**** inval: settings for data files   ****
                ;**** outval: settings for output      ****
                ;******************************************

    files = findfile(deffile)
    if files(0) eq deffile then begin
    
        restore, deffile
        inval.centroot = centroot + '/adf34/'
        inval.userroot = userroot + '/adf34/'
    
    endif else begin
        
        inval   = { rootpath    :   userroot+'/adf34/',   $
                    file        :   '',                   $
                    centroot    :   centroot+'/adf34/',   $
                    userroot    :   userroot+'/adf34/',   $
                    calctype    :   1,                    $
                    ionpot      :   0.0                   }
                  
        procval = { options_set :   'NO',                 $
                    configs_set :   'NO',                 $
                    te          :   fltarr(14),           $
	            nte         :   0,			  $
                    te_unit     :   0,                    $
                    ind_xpar    :   intarr(14),           $
                    a04_type    :   3,                    $
                    irnq        :   0,                    $
                    irxq        :   2,                    $
                    irnd        :   1,                    $
                    irxd        :   1,                    $
                    iquad       :   0,                    $
                    imag        :   0,                    $
                    fii         :   95,                   $
                    zeta        :   99,                   $
                    fij         :   75,                   $
                    gij         :   75,                   $
                    rij         :   60,                   $
                    cfg1        :   strarr(20),           $
                    cfg2        :   strarr(20),           $
                    p1list      :   intarr(20),           $
                    p2list      :   intarr(20),           $
                    z0          :   0,                    $
                    z1          :   0                     }
        

        lsname  = userroot + '/pass/adas801_adf04_ls.pass'
        icname  = userroot + '/pass/adas801_adf04_ic.pass'
        
        outval  = { texout      :   1,                    $
                    texapp      :   -1,                   $
                    texrep      :   1,                    $
                    texdsn      :   '',                   $
                    texdef      :   'paper.txt',          $
                    texmes      :   '',                   $
                    icout       :   0,                    $
                    icapp       :   -1,                   $
                    icrep       :   0,                    $
                    icdsn       :   '',                   $
                    icdef       :   icname,               $
                    icmes       :   '',                   $
                    lsout       :   0,                    $
                    lsapp       :   -1,                   $
                    lsrep       :   0,                    $
                    lsdsn       :   '',                   $
                    lsdef       :   lsname,               $
                    lsmes       :   '',                   $
                    cfgstyle    :   0,                    $
                    userroot    :   userroot+'/pass/',    $
                    passroot    :   userroot+'/pass/'     }
    endelse                                            
    procval.options_set = 'NO'
                    
                ;***********************************************
                ;**** Make sure we are running v5 or above. ****
                ;***********************************************

    thisRelease = StrMid(!Version.Release, 0, 1)
    IF thisRelease LT '5' THEN BEGIN
       message = 'Sorry, ADAS801 requires IDL 5 or higher' 
       tmp = popup(message=message, buttons=['Accept'],font=font_large)
       goto, LABELEND
    ENDIF


; Key all temporary work files to a random number

  rand = strmid(strtrim(string(randomu(seed)), 2), 2, 4)


LABEL100:

    ; These are fixed for each run
    
    outval.texapp   = -1
    
                ;********************************************
                ;**** Invoke user interface widget for   ****
                ;**** Data file selection                ****
                ;********************************************


    rep=adas801_in(inval,  procval, $
                   FONT_LARGE=font_large, FONT_SMALL = font_small, $
                   EDIT_FONTS = edit_fonts)
 
    if rep eq 'CANCEL' then goto, LABELEND





LABEL200:

                ;************************************************
                ;**** Communicate with d4spf1 in fortran and ****
                ;**** invoke user interface widget for       ****
                ;**** Output options                         ****
                ;************************************************

    
    date   = xxdate()
    header = adasrel+adasprog+' DATE: '+date(0)+' TIME: '+date(1)

    rep=adas801_run(outval, bitfile,                   $
                    FONT_LARGE=font_large, FONT_SMALL = font_small)
    
    
    if rep eq 'MENU'   then goto, LABELEND
    if rep eq 'CANCEL' then goto, LABEL100

    
    if rep EQ 'NOW' then isbatch = 'NO' else isbatch = 'YES'
    adas801_calc, inval, procval, outval,                   $ 
                  centroot, fortdir, rand, isbatch, header, $
                  FONT_LARGE=font_large,                    $
                  FONT_SMALL = font_small
    
                ;**** Back for more output options ****

    GOTO, LABEL200

LABELEND:


                ;**** Save user defaults ****

    save, inval, procval,  outval, filename=deffile


END
