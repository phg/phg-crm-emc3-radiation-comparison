; Copyright (c) 1995 Strathclyde University 
; SCCS info: Module @(#)$Header: /home/adascvs/idl/adas8xx/adas810/adas810_out.pro,v 1.2 2004/07/06 11:07:42 whitefor Exp $ Date $Date: 2004/07/06 11:07:42 $
;+
; PROJECT:
;       ADAS 
;
; NAME:
;	ADAS810_OUT
;
; PURPOSE: 
;
;
; EXPLANATION:
;       The user selects output files with suitable defaults available.
;       Defaults are of the form adas810_adfXX.pass variety.
;
;
; NOTES:
; 
;
;
; INPUTS:
;        outval  - Structure of output files,
;        bitfile - Directory of bitmaps for menu button
;
;
; OPTIONAL INPUTS:
;       None
;
; OUTPUTS:
;       The function returns a string rep
;           rep = 'CONTINUE' if everything is ok
;           rep = 'CANCEL' if there is a problem or indeed if the user
;                          closes the widget with the mouse. 
;           rep = 'MENU' if user want to exit to menu at this point
;
; OPTIONAL OUTPUTS:
;       None
;
; KEYWORD PARAMETERS:
;       FONT_LARGE - The name of a larger font e.g 'courier_bold14'
;
;       FONT_SMALL - The name of a smaller font e.g 'courier_bold12'
;
;
; CALLS:
;                            
;       ADAS810_OUT_MENU   : React to menu button event. The standard IDL
;                            reaction to button events cannot deal with
;                            pixmapped buttons. Hence the special handler.
;       ADAS810_OUT_EVENT  : Reacts to cancel and Done. Does the file
;                            existence error checking.
;
; SIDE EFFECTS:
;
; CATEGORY:
;	Widgets
;
; WRITTEN:
;       Martin O'Mullane
;
; MODIFIED:
;	1.1	Martin O'Mullane
;		 - First release. 
;	1.2	Martin O'Mullane
;		 - Allow the filtered plt output option. 
;
; VERSION:
;       1.1	14-02-2003
;       1.2	28-02-2004
;
;-
;-----------------------------------------------------------------------------



PRO ADAS810_OUT_MENU, event

; The 'normal' get_value from a widget_button does not work with a
; bitmap button. Hence this specialised event handler.

   ; Get the info structure out of the user value of the top-level base.

Widget_Control, event.top, Get_UValue=info


 formdata = {cancel : 0, menu:1}
 *info.ptrToFormData =formdata
 widget_Control, event.top, /destroy

END


;-----------------------------------------------------------------------------


PRO ADAS810_OUT_EVENT, event

; React to button events - Cancel and Done but not menu (this requires a
; specialised event handler ADAS810_OUT_MENU). Also deal with the passing
; directory output Default button here.

; On pressing Done check for the following


   ; Get the info structure out of the user value of the top-level base.

Widget_Control, event.top, Get_UValue=info


Widget_Control, event.id, Get_Value=userEvent

CASE userEvent OF


  'Cancel' : begin
               formdata = {cancel:1, menu : 0}
               *info.ptrToFormData =formdata
               widget_Control, event.top, /destroy
             end 

      
  'Done'   : begin
                 
                ; gather the data for return to calling program
                
                err  = 0
                mess = ''
                
                widget_Control, info.paperID, Get_Value=pap
                widget_Control, info.a15ID, Get_Value=a15
                widget_Control, info.a40ID, Get_Value=a40
                widget_Control, info.a11ID, Get_Value=a11
                widget_Control, info.a11fID, Get_Value=a11f

                ; no need to set mess as it is flagged in the cw
                if pap.outbut  EQ 1 AND strtrim(pap.message)  NE '' then err=1
                if a15.outbut  EQ 1 AND strtrim(a15.message)  NE '' then err=1
                if a40.outbut  EQ 1 AND strtrim(a40.message)  NE '' then err=1
                if a11.outbut  EQ 1 AND strtrim(a11.message)  NE '' then err=1
                if a11f.outbut EQ 1 AND strtrim(a11f.message) NE '' then err=1
                
                                               
                if err EQ 0 then begin
                   formdata = { paper    : pap,      $
                                a15      : a15,      $
                                a40      : a40,      $
                                a11      : a11,      $
                                a11f     : a11f,     $
                                cancel   : 0,        $
                                menu     : 0         }
                   *info.ptrToFormData = formdata
                   widget_control, event.top, /destroy
                endif else begin
                   widget_Control, info.messID, Set_Value=mess
                endelse

             end
             
             
  else : print,'ADAS810_OUT_EVENT : You should not see this message! ',userEvent
                   
ENDCASE

END
  
;-----------------------------------------------------------------------------



FUNCTION ADAS810_OUT, outval,                      $  
                      bitfile,                     $
                      FONT_LARGE = font_large,     $
                      FONT_SMALL = font_small


; Set defaults for keywords and extract info for paper.txt question

  IF n_elements(font_large) eq 0 THEN font_large = ''
  IF n_elements(font_small) eq 0 THEN font_small = ''


  paperval =  { outbut   : outval.TEXOUT, $
                appbut   : outval.TEXAPP, $
                repbut   : outval.TEXREP, $
                filename : outval.TEXDSN, $
                defname  : outval.TEXDEF, $
                message  : outval.TEXMES  }

  a15val   =  { outbut   : outval.A15OUT, $
                appbut   : outval.A15APP, $
                repbut   : outval.A15REP, $
                filename : outval.A15DSN, $
                defname  : outval.A15DEF, $
                message  : outval.A15MES  }

  a40val   =  { outbut   : outval.A40OUT, $
                appbut   : outval.A40APP, $
                repbut   : outval.A40REP, $
                filename : outval.A40DSN, $
                defname  : outval.A40DEF, $
                message  : outval.A40MES  }
  
  a11val   =  { outbut   : outval.A11OUT, $
                appbut   : outval.A11APP, $
                repbut   : outval.A11REP, $
                filename : outval.A11DSN, $
                defname  : outval.A11DEF, $
                message  : outval.A11MES  }

  a11fval   = { outbut   : outval.A11fOUT, $
                appbut   : outval.A11fAPP, $
                repbut   : outval.A11fREP, $
                filename : outval.A11fDSN, $
                defname  : outval.A11fDEF, $
                message  : outval.A11fMES  }
                
                


; Create top level base widget

                
  parent = Widget_Base(Column=1, Title='ADAS 810 OUTPUT', $
                       XOFFSET=100, YOFFSET=1)
                
  rc = widget_label(parent,value='  ',font=font_large)

  base    = widget_base(parent, /column)
  
; Output files - paper.txt and the others

  mrow    = widget_base(base,/frame)
  paperID = cw_adas_outfile(mrow, OUTPUT='Text Output ',   $
                                 VALUE=paperval, FONT=font_large)
  
  
  mlab  = widget_label(parent,value='  ',font=font_large)
  mcol  = widget_base(parent, /column, /frame)
 

  a15ID = cw_adas_outfile(mcol, OUTPUT='PEC (adf15) file ',   $
                                VALUE=a15val, FONT=font_large, xsize=50)

  a40ID = cw_adas_outfile(mcol, OUTPUT='Feature PEC (adf40) file ', $
                                VALUE=a40val, FONT=font_large, xsize=50)

                            
  a11ID = cw_adas_outfile(mcol, OUTPUT='Total power (adf11/plt) unfiltered',   $
                                VALUE=a11val, FONT=font_large, xsize=50)
                       
                            
  a11fID = cw_adas_outfile(mcol, OUTPUT='Total power (adf11/plt) filtered ',          $
                                 VALUE=a11fval, FONT=font_large, xsize=50)
                       
                       
                            
; End of panel message and buttons                

  message  = 'Choose output options'

  messID   = widget_label(parent, value=message, font=font_large)
                      
                
  menufile = bitfile + '/menu.bmp'
  read_X11_bitmap, menufile, bitmap1
                
  mrow     = widget_base(parent,/row)
  menuID   = widget_button(mrow,value=bitmap1,font=font_large, $
                           event_pro='ADAS810_OUT_MENU')
  cancelID = widget_button(mrow,value='Cancel',font=font_large)
  doneID   = widget_button(mrow,value='Done',font=font_large)



; Realize the ADAS810_OUT input widget.

   dynlabel, parent
   widget_Control, parent, /realize



; Create a pointer to store the information collected from the form.
; The initial data stored here is set to CANCEL, so nothing needs to
; be done if the user kills the widget with the mouse.

   ptrToFormData = Ptr_New({cancel:1, menu:0})


; Create an info structure with program information.

  info = { messID          :  messID,             $
           paperID         :  paperID,            $
           a15ID           :  a15ID,              $
           a40ID           :  a40ID,              $
           a11ID           :  a11ID,              $
           a11fID          :  a11fID,             $
           font            :  font_large,         $
           parent          :  parent,             $  
           ptrToFormData   :  ptrToFormData       }  
            
               
; Store the info structure in the user value of the top-level base and launch
; the widget into the world.

  widget_control, parent, Set_UValue=info

  XManager, 'ADAS810_OUT', parent, Event_Handler='ADAS810_OUT_EVENT'


; When the widget is destroyed, the block is released, and we
; return here. Get the form data that was collected by the form
; and stored in the pointer location.

formdata = *ptrToFormData

rep='CONTINUE'
IF N_Elements(formdata) EQ 0 THEN BEGIN
   Ptr_Free, ptrToFormData
   rep ='CANCEL'
   RETURN, rep
ENDIF
 
IF formdata.cancel EQ 1 THEN BEGIN
   Ptr_Free, ptrToFormData
   rep ='CANCEL'
   RETURN, rep
ENDIF

IF formdata.menu EQ 1 THEN BEGIN
   Ptr_Free, ptrToFormData
   rep ='MENU'
   RETURN, rep
ENDIF

if rep eq 'CONTINUE' then begin

  outval.texout  = formdata.paper.outbut
  outval.a15out  = formdata.a15.outbut
  outval.a40out  = formdata.a40.outbut
  outval.a11out  = formdata.a11.outbut
  outval.a11fout = formdata.a11f.outbut
  
  outval.texdsn  = formdata.paper.filename
  outval.a15dsn  = formdata.a15.filename
  outval.a40dsn  = formdata.a40.filename
  outval.a11dsn  = formdata.a11.filename
  outval.a11fdsn = formdata.a11f.filename
  
endif
RETURN, rep

END
