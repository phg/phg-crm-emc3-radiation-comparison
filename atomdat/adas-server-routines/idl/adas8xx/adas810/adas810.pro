; Copyright (c) 2003 Strathclyde University .
; SCCS info: Module @(#)$Header: /home/adascvs/idl/adas8xx/adas810/adas810.pro,v 1.4 2019/04/25 10:59:15 mog Exp $ Date $Date: 2019/04/25 10:59:15 $
;+
; PROJECT:
;       ADAS
;
; NAME:
;	ADAS810
;
; PURPOSE:
;	The highest level routine for the ADAS 810 program.
;
; EXPLANATION:
;	This routine is called from the main adas system routine, adas.pro,
;	to start the ADAS 810 application.  Associated with adas810.pro
;	is a FORTRAN executable.  The IDL code provides the user
;	interface and output graphics whilst the FORTRAN code reads
;	in data files, performs numerical processing and creates the
;	output files.  The IDL code communicates with the FORTRAN
;	executable via a bi-directional UNIX pipe.  The unit number
;	used by the IDL for writing to and reading from this pipe is
;	allocated when the FORTRAN executable is 'spawned' (see code
;	below).  Pipe communications in the FORTRAN process are to
;	stdin and stdout, i.e streams 5 and 6.
;
;	The FORTRAN code is an independent process under the UNIX system.
;	The IDL process can only exert control over the FORTRAN in the
;	data which it communicates via the pipe.  The communications
;	between the IDL and FORTRAN must be exactly matched to avoid
;	input conversion errors.  The correct ammounts of data must be
;	passed so that neither process 'hangs' waiting for communications
;	which will never occur.
;
;	The FORTRAN code performs some error checking which is
;	independent of IDL.  In cases of error the FORTRAN may write
;	error messages.  To prevent these error messages from conflicting
;	with the pipe communications all FORTRAN errors are written to
;	output stream 0, which is stderr for UNIX.  These error messages
;	will appear in the window from which the ADAS session/IDL session
;	is being run.
;
;	In the case of severe errors the FORTRAN code may terminate
;	itself prematurely.  In order to detect this, and prevent the
;	IDL program from 'hanging' or crashing, the IDL checks to see
;	if the FORTRAN executable is still an active process before
;	each group of pipe communications.  The process identifier
;	for the FORTRAN process, PID, is recorded when the process is
;	first 'spawned'.  The system is then checked for the presence
;	of the FORTRAN PID.
;
; USE:
;	First the system settings must be established by calling
;	adas_sys_set.pro then adas810.pro is called to start the
;	ADAS 810 application;
;
;	adas_sys_set, adasrel, fortdir, userroot, centroot, $
;		      devlist, devcode, font_large, font_small, edit_fonts
;	adas810,   adasrel, fortdir, userroot, centroot, devlist, $
;		   devcode, font_large, font_small, edit_fonts
;
; INPUTS:
;	ADASREL - A string indicating the ADAS system version, 
;		  e.g ' ADAS RELEASE: ADAS93 V1.1.0'.  The first
;		  character should be a space.
;
;	FORTDIR - A string holding the path to the directory where the
;		  FORTRAN executables are, e.g '/disk/adas/fortran/exec'
;
;	USERROOT - A string holding the path to the root directory of
;		   the user's adas data. e.g '/disk/bowen/adas'
;		   This root directory will be used by adas to construct
;		   other path names.  For example the users default data
;		   for adas810 should be in /disk/bowen/adas/adf04.  In
;		   particular the user's default interface settings will
;		   be stored in the directory USERROOT+'/defaults'.  An
;		   error will occur if the defaults directory does not
;		   exist.
;
;	CENTROOT - Like USERROOT, but this directory points to the
;		   central data area for the system.  User defaults are
;		   not stored on CENTROOT.
;
;	DEVLIST - A string array of hardcopy device names, used for
;		  graphical output. e.g ['Post-Script','HP-PCL','HP-GL']
;		  This array must mirror DEVCODE.  DEVCODE holds the
;		  actual device names used in a SET_PLOT statement.
;
;	DEVCODE - A string array of hardcopy device code names used in
;		  the SET_PLOT statement, e.g ['PS', 'PCL', 'HP']
;		  This array must mirror DEVLIST.
;
;	FONT_LARGE - The name of a larger font e.g 'courier_bold14'
;
;	FONT_SMALL - The name of a smaller font e.g 'courier_bold12'
;
;	EDIT_FONTS - A structure of two fonts used in the table editor
;		     adas_edtab.pro, {font_norm:'',font_input:''}.
;		     The two fonts are used to differentiate between
;		     editable and non-editable parts of the table. You
;		     may have to experiment to get two fonts which give
;		     the same default spacing in the edit widget.
;		     e.g {font_norm:'helvetica_bold14', $
;			  font_input:'helvetica_oblique14'}
;
; OPTIONAL INPUTS:
;	None
;
; OUTPUTS:
;	None - Note this routine should leave all inputs un-modified.
;
; OPTIONAL OUTPUTS:
;	None
;
; KEYWORD PARAMETERS:
;	None
;
; CALLS:
;	FIND_PROCESS  Checks to see if a given process is active.
;       ADAS810_IN    Prompts for and gathers input files
;       ADAS810_PROC  Prompts for and writes output files
;       ADAS810_OUT   Prompts for and writes output files
;       XXDATE        Get date and time from operating system.
;       POPUP         Puts up a message on the screen.
;
; SIDE EFFECTS:
;	This routine spawns a FORTRAN executable.  Note the pipe 
;	communications routines listed above.  In addition to these
;	pipe communications there is one explicit communication of the
;	date to the FORTRAN code, search for 'printf,pipe' to find it.
;
; CATEGORY:
;	Adas system.
;	
; WRITTEN:
;       Martin O'Mullane
;
; MODIFIED:
;       1.1     Martin O'Mullane
;		 - First version
;       1.2     Martin O'Mullane
;		 - Incorrect handling of expansion files in FILE mode.
;                - Use adas_progressbar routine to tidy code.
;		 - Increased version no to 1.2.
;       1.3     Martin O'Mullane
;		 - Arguments to read_adf04 have changed.
;       1.4     Martin O'Mullane
;		 - Send a signal indicating termination to fortran.
;
;
; VERSION:
;	1.1     30-11-2002
;       1.2     29-05-2003
;       1.3     16-11-2004
;       1.4     25-04-2019
;
;-
;-----------------------------------------------------------------------------

PRO adas810_prepare, rep, pipe, dsn04, procval, outval, adf04_levels, adf04_te


; Get info for processing screen from adf04 file     

   read_adf04, file=dsn04, fulldata=adf04_data
   
   adf04_levels = adf04_data.cstrga
   adf04_te     = adf04_data.te
   iz0          = adf04_data.iz0
   iz1          = adf04_data.iz

; Fill the procval structure - Communicate with fortran if adf42 driver 
; is selected. Note we define procval variables here and fill in procval 
; if it is empty for the adf04/adf18 option.

   title   = ''
   
   numt    = 0
   te      = dblarr(24)
   tion    = dblarr(24)
   th      = dblarr(24)
   ifout   = 2
   
   numd    = 0
   dens    = dblarr(24)
   dion    = dblarr(24)
   idout   = 1
   
   nmet    = 0
   imetr   = intarr(4)
   
   liosel  = 0 
   lhsel   = 0 
   lrsel   = 0 
   lisel   = 0 
   lnsel   = 0 
   lpsel   = 0 
   zeff    = 0.0D0

   lnorm   = 0

   nwvl    = 0
   npix    = intarr(5)
   wvmin   = dblarr(5)
   wvmax   = dblarr(5)
   amin    = 0.0D0
   

; Setup a new procval for DRIVER or 
; if it's the first time for FILE branch
   
   if rep EQ 'DRIVER' OR procval.nmet EQ -1 then begin
   
      procval = { title   :   title,   $
                  iz0     :   iz0,     $
                  iz1     :   iz1,     $
                  ifout   :   ifout,   $
                  numt    :   numt,    $   
                  te      :   te,      $  
                  tion    :   tion,    $ 
                  th      :   th,      $ 
                  idout   :   idout,   $
                  numd    :   numd,    $
                  dens    :   dens,    $
                  dion    :   dion,    $
                  nmet    :   nmet,    $
                  imetr   :   imetr,   $
                  liosel  :   liosel,  $
                  lhsel   :   lhsel,   $
                  lrsel   :   lrsel,   $
                  lisel   :   lisel,   $
                  lnsel   :   lnsel,   $
                  lpsel   :   lpsel,   $
                  zeff    :   zeff,    $
                  lnorm   :   lnorm,   $
                  nwvl    :   nwvl,    $ 
                  npix    :   npix,    $ 
                  wvmin   :   wvmin,   $
                  wvmax   :   wvmax,   $
                  amin    :   amin     }
   
   endif   

; Overwrite procval and outval values if comming from an adf42 driver file.
   
   idum    = 0
   fdum    = 0.0D0
   fdum1   = 0.0D0
   
   if rep EQ 'DRIVER' then begin
    
      procval.te[*]   = 0.0D0
      procval.tion[*] = 0.0D0
      procval.th[*]   = 0.0D0
      procval.dens[*] = 0.0D0
      procval.dion[*] = 0.0D0
            
      readf, pipe, numt
      for j = 0, numt-1 do begin
         readf, pipe, fdum
         te[j] = fdum
      endfor
      for j = 0, numt-1 do begin
         readf, pipe, fdum
         tion[j] = fdum
      endfor
      for j = 0, numt-1 do begin
         readf, pipe, fdum
         th[j] = fdum
      endfor
      
      procval.numt           = numt
      procval.te[0:numt-1]   = te[0:numt-1]
      procval.tion[0:numt-1] = tion[0:numt-1]
      procval.th[0:numt-1]   = th[0:numt-1]
      
      readf, pipe, numd
      for j = 0, numd-1 do begin
         readf, pipe, fdum
         dens[j] = fdum
      endfor
      for j = 0, numd-1 do begin
         readf, pipe, fdum
         dion[j] = fdum
      endfor
      
      procval.numd           = numd
      procval.dens[0:numd-1] = dens[0:numd-1]
      procval.dion[0:numd-1] = dion[0:numd-1]
     
      readf, pipe, idum
      if idum EQ 1 then procval.lnorm = 1 else procval.lnorm = 0
      readf, pipe, idum
      procval.nmet = idum
      for j = 0, 3 do begin
         readf, pipe, idum
         procval.imetr[j] = idum
      endfor
     
      readf, pipe, idum
      if idum EQ 1 then procval.liosel = 1 else procval.liosel = 0
      readf, pipe, idum
      if idum EQ 1 then procval.lhsel = 1 else procval.lhsel = 0
      readf, pipe, idum
      if idum EQ 1 then procval.lrsel = 1 else procval.lrsel = 0
      readf, pipe, idum
      if idum EQ 1 then procval.lisel = 1 else procval.lisel = 0
      readf, pipe, idum
      if idum EQ 1 then procval.lnsel = 1 else procval.lnsel = 0
      readf, pipe, idum
      if idum EQ 1 then procval.lpsel = 1 else procval.lpsel = 0
     
      readf, pipe, fdum
      procval.zeff = fdum
     
     
      readf, pipe, nwvl
      for j = 0, nwvl-1 do begin
         readf, pipe, idum
         readf, pipe, fdum
         readf, pipe, fdum1
         npix[j]  = idum
         wvmin[j] = fdum
         wvmax[j] = fdum1
      endfor

      procval.nwvl            = nwvl
      procval.npix[0:nwvl-1]  = npix[0:nwvl-1]
      procval.wvmin[0:nwvl-1] = wvmin[0:nwvl-1]
      procval.wvmax[0:nwvl-1] = wvmax[0:nwvl-1]
 
      readf, pipe, fdum
      procval.amin = fdum
      
      
   endif
   



END

;-----------------------------------------------------------------------------


PRO ADAS810,	adasrel, fortdir, userroot, centroot, 			$
		devlist, devcode, font_large, font_small, edit_fonts


; Initialisation

    adasprog = ' PROGRAM: ADAS810 V1.2'
    deffile  = userroot+'/defaults/adas810_defaults.dat'
    bitfile  = centroot+'/bitmaps'
    device   = ''
                
    
; We rely on IDL v5 features - are there any v4 places left?

    thisRelease = StrMid(!Version.Release, 0, 1)
    IF thisRelease LT '5' THEN BEGIN
       message = 'Sorry, ADAS810 requires IDL 5 or higher' 
       tmp = popup(message=message, buttons=['Accept'],font=font_large)
       goto, LABELEND
    ENDIF

 
; Restore user default settings - keep adf18 in defaults although we
; have yet to decide on whether to include it.


    files = findfile(deffile)
  
    if files[0] EQ deffile then begin

        restore, deffile
        inval.acentroot = centroot+'/adf04/'
        inval.auserroot = userroot+'/adf04/'
        inval.bcentroot = centroot+'/adf18/a17_p208/'
        inval.buserroot = userroot+'/adf18/a17_p208/'
        inval.ccentroot = centroot+'/adf35/'
        inval.cuserroot = userroot+'/adf35/'
        inval.dcentroot = centroot+'/adf42/'
        inval.duserroot = userroot+'/adf42/'
    
    endif else begin

       inval = { branch      :   'FILE',                         $
                 arootpath   :   userroot+'/adf04/',             $
                 afile       :   '',                             $
                 acentroot   :   centroot+'/adf04/',             $
                 auserroot   :   userroot+'/adf04/',             $
                 brootpath   :   userroot+'/adf18/a17_p208/',    $
                 bfile       :   '',                             $
                 bcentroot   :   centroot+'/adf18/a17_p208/',    $
                 buserroot   :   userroot+'/adf18/a17_p208/',    $
                 crootpath   :   userroot+'/adf35/',             $
                 cfile       :   '',                             $
                 ccentroot   :   centroot+'/adf35/',             $
                 cuserroot   :   userroot+'/adf35/',             $
                 drootpath   :   userroot+'/adf42/',             $
                 dfile       :   '',                             $
                 dcentroot   :   centroot+'/adf42/',             $
                 duserroot   :   userroot+'/adf42/'              }
                   

       procval = {NMET:-1}

       passdir = userroot+'/pass/'
       outval  = { texout    :    0,                         $
                   texapp    :   -1,                         $
                   texrep    :    0,                         $
                   texdsn    :   '',                         $
                   texdef    :   'paper.txt',                $
                   texmes    :   '',                         $
                   a15out    :    0,                         $
                   a15app    :   -1,                         $
                   a15rep    :    0,                         $
                   a15dsn    :   '',                         $
                   a15def    :   'adas810_adf15.pass',       $
                   a15mes    :   '',                         $
                   a40out    :    0,                         $
                   a40app    :   -1,                         $
                   a40rep    :    0,                         $
                   a40dsn    :   '',                         $
                   a40def    :   'adas810_adf40.pass',       $
                   a40mes    :   '',                         $
                   a11out    :    0,                         $
                   a11app    :   -1,                         $
                   a11rep    :    0,                         $
                   a11dsn    :   '',                         $
                   a11def    :   'adas810_adf11_plt.pass',   $
                   a11mes    :   '',                         $
                   a11fout   :    0,                         $
                   a11fapp   :   -1,                         $
                   a11frep   :    0,                         $
                   a11fdsn   :   '',                         $
                   a11fdef   :   'adas810_adf11_plt_f.pass', $
                   a11fmes   :   '',                         $
                   passdir   :    passdir                    }
  endelse


; Start the fortran
  
  spawn, fortdir + '/adas810.out', unit=pipe,/noshell,PID=pid


; First task is to get date and time and pass to fortran

  date = bxdate()
  printf, pipe, date[0]

  header = adasrel + adasprog + ' DATE: ' + date[0] + ' TIME: ' + date[1]
  printf, pipe, header
	
; Start the main event loop - 3 screens; input, processing and output
; Check that the fortran code is running at each stage       


;-------------
; Input screen
;-------------

LABEL100:
  
    
    if find_process(pid) EQ 0 then goto, LABELEND

    rep = adas810_in(inval,                     $
                     font_large=font_large,     $ 
                     font_small = font_small    )

    if rep eq 'CANCEL' then goto, LABELEND
    
    method = rep

    if find_process(pid) EQ 0 then goto, LABELEND
    printf, pipe, rep


; Choice of normal (adf04/adf18) or driver (adf42) file 

    if rep EQ 'DRIVER' then begin
    
       dsn42 = strcompress(inval.drootpath + inval.dfile)
       printf, pipe, dsn42
       
       dsn04 = ''
       dsn18 = ''
       dsn35 = ''
       readf, pipe, dsn04
       dsn04 = strtrim(dsn04)
       readf, pipe, dsn18
       dsn18 = strtrim(dsn18)
       readf, pipe, dsn35
       dsn35 = strtrim(dsn35)
       
       
       ; output files
       
       dsn15  = ''
       dsn40  = ''
       dsn11  = ''
       dsn11f = ''
       readf, pipe, dsn15
       outval.a15dsn = strtrim(dsn15)
       readf, pipe, dsn40
       outval.a40dsn = strtrim(dsn40)
       readf, pipe, dsn11
       outval.a11dsn = strtrim(dsn11)
       readf, pipe, dsn11f
       outval.a11fdsn = strtrim(dsn11f)
       
   
    endif else begin
    
       dsn04 = strcompress(inval.arootpath + inval.afile)
       dsn18 = strcompress(inval.brootpath + inval.bfile)
       dsn35 = strcompress(inval.crootpath + inval.cfile)
       
       file_acc, dsn18, ex, re, wr, ex, ty
       if ty EQ '-' then lpdata = 1 else lpdata = 0
    
       file_acc, dsn35, ex, re, wr, ex, ty
       if ty EQ '-' then lflt = 1 else lflt = 0

       printf, pipe, dsn04
       
       if lpdata EQ 0 then dsn18 = 'NULL' 
       printf, pipe, dsn18
       printf, pipe, lpdata
       
       if lflt EQ 1 then printf, pipe, dsn35 else printf, pipe, 'NULL'
       printf, pipe, lflt
  
    endelse
    
    adas810_prepare, rep, pipe, dsn04, procval, outval, adf04_levels, adf04_te



;------------------
; Processing screen
;------------------

LABEL200:

    if find_process(pid) eq 0 then goto, LABELEND

    rep = adas810_proc(procval,                   $
                       adf04_levels,              $
                       adf04_te,                  $
                       method,                    $                  
                       dsn04,                     $
                       dsn18,                     $
                       dsn42,                     $
                       bitfile,                   $
                       FONT_LARGE = font_large,   $    
                       FONT_SMALL = font_small,   $    
                       EDIT_FONTS = edit_fonts    )

    if find_process(pid) EQ 0 then goto, LABELEND
    printf, pipe, rep

    case rep of

      'MENU'     : goto, LABELEND

      'CANCEL'   : goto, LABEL100
     
      'CONTINUE' : begin
        
                      printf, pipe, procval.title
                      printf, pipe, procval.ifout
                      printf, pipe, procval.numt
                      printf, pipe, procval.te
                      printf, pipe, procval.tion
                      printf, pipe, procval.th
                      printf, pipe, procval.idout
                      printf, pipe, procval.numd
                      printf, pipe, procval.dens
                      printf, pipe, procval.dion
                      printf, pipe, procval.nmet
                      printf, pipe, procval.imetr
                      printf, pipe, procval.liosel
                      printf, pipe, procval.lhsel
                      printf, pipe, procval.lrsel
                      printf, pipe, procval.lisel
                      printf, pipe, procval.lnsel
                      printf, pipe, procval.lpsel
                      printf, pipe, procval.zeff
                      printf, pipe, procval.lnorm
                      printf, pipe, procval.nwvl
                      printf, pipe, procval.npix
                      printf, pipe, procval.wvmin
                      printf, pipe, procval.wvmax
                      printf, pipe, procval.amin
                      
                   end
    
    endcase
  



;-----------------------
; Population calculation
;-----------------------

 
    if find_process(pid) EQ 0 then goto, LABELEND

    n_call  = procval.numt*procval.numd

    adas_progressbar, pipe     = pipe,         $
                      n_call   = n_call,       $
                      adasprog = adasprog,     $
                      font     = font_large

    

;--------------
; Output screen
;--------------

LABEL300:

    if find_process(pid) eq 0 then goto, LABELEND

    rep = adas810_out(outval,                    $
                      bitfile,                   $
                      FONT_LARGE = font_large,   $    
                      FONT_SMALL = font_small    )


    if find_process(pid) EQ 0 then goto, LABELEND
    printf, pipe, rep

    case rep of

      'MENU'     : goto, LABELEND

      'CANCEL'   : goto, LABEL200
     
      'CONTINUE' : begin
        
                      printf, pipe, outval.texout 
                      printf, pipe, outval.a15out 
                      printf, pipe, outval.a40out 
                      printf, pipe, outval.a11out 
                      printf, pipe, outval.a11fout

                      printf, pipe, outval.texdsn 
                      printf, pipe, outval.a15dsn 
                      printf, pipe, outval.a40dsn 
                      printf, pipe, outval.a11dsn 
                      printf, pipe, outval.a11fdsn
                      
                   end
                   
    endcase
    
    GOTO, LABEL300


LABELEND:



; Save user defaults and free all units

save,inval,procval,outval,filename=deffile

printf, pipe, 1L

close, /all

END
