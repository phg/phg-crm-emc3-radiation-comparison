;----------------------------------------------------------------------
;+
; PROJECT    :  ADAS
;
; NAME       :  run_adas810
;
; PURPOSE    :  Runs the ADAS810 collisional-radiative population code
;               as an IDL subroutine.
;
; ARGUMENTS  :  All output arguments will be defined appropriately.
;
;               NAME      I/O    TYPE   DETAILS
; INPUTS     :  adf42      I     str    driver file.
;               log        I     str    name of output text file.
;                                       (defaults to no output).
;
; KEYWORDS   :  pbar                    display progress bar widget
;               help       I     int    Returns help and exits if set
;              
;
; OUTPUTS    :
;
; NOTES      :  Originally called the offline perl script to provide the
;               convenience of an all-IDL workflow. Now call the
;               central/interactive adas810 executable.
;
;
; MODIFIED :
;               1.1   Martin O'Mullane
;                       - First version
;               1.2   Martin O'Mullane
;                       - Call interactive 810 since it has diverged
;                         too far from offline version.
;                       - Add progress bar switch.
;
; VERSION :
;
;               1.1   19-02-2017
;               1.2   24-04-2019
;-
;----------------------------------------------------------------------


PRO run_adas810, adf42 = adf42,  $
                 log   = log,    $
                 pbar  = pbar,   $
                 help  = help


; If asked for help

if keyword_set(help) then begin
   doc_library, 'run_adas810'
   return
endif

; Check that we get all inputs and that they are correct. Otherwise print
; a message and return to command line.

on_error, 2

if n_elements(adf42) EQ 0 then begin
    message,'A driver file of type adf42 is required'
endif

; Suitable defaults

if n_elements(log) EQ 0 then begin
   outfile = 0
   output_file = '/dev/null'
endif else begin
   outfile = 1
   output_file = log
endelse

date    = (xxdate())[0]
rand    = strmid(strtrim(string(randomu(seed)), 2), 2, 4)
fortdir = getenv('ADASFORT')

spawn, fortdir+'/adas810.out', unit=pipe, /noshell, PID=pid

printf, pipe, date[0]
printf, pipe, 'run_adas810'


; Input screen

printf, pipe, 'DRIVER'
printf, pipe, adf42

dsn04 = ''
dsn18 = ''
dsn35 = ''
readf, pipe, dsn04
dsn04 = strtrim(dsn04)
readf, pipe, dsn18
dsn18 = strtrim(dsn18)
readf, pipe, dsn35
dsn35 = strtrim(dsn35)

dsn15  = ''
dsn40  = ''
dsn11  = ''
dsn11f = ''
readf, pipe, dsn15
a15dsn = strtrim(dsn15)
readf, pipe, dsn40
a40dsn = strtrim(dsn40)
readf, pipe, dsn11
a11dsn = strtrim(dsn11)
readf, pipe, dsn11f
a11fdsn = strtrim(dsn11f)


; Pre-processing -- reading back adf42 data from fortran

numt    = 0
te      = dblarr(24)
tion    = dblarr(24)
th      = dblarr(24)
ifout   = 2

numd    = 0
dens    = dblarr(24)
dion    = dblarr(24)
idout   = 1

nmet    = 0
imetr   = intarr(4)

liosel  = 0 
lhsel   = 0 
lrsel   = 0 
lisel   = 0 
lnsel   = 0 
lpsel   = 0 
zeff    = 0.0D0

lnorm   = 0

nwvl    = 0
npix    = intarr(5)
wvmin   = dblarr(5)
wvmax   = dblarr(5)
amin    = 0.0D0


readf, pipe, numt
for j = 0, numt-1 do begin
   readf, pipe, fdum
   te[j] = fdum
endfor
for j = 0, numt-1 do begin
   readf, pipe, fdum
   tion[j] = fdum
endfor
for j = 0, numt-1 do begin
   readf, pipe, fdum
   th[j] = fdum
endfor

readf, pipe, numd
for j = 0, numd-1 do begin
   readf, pipe, fdum
   dens[j] = fdum
endfor
for j = 0, numd-1 do begin
   readf, pipe, fdum
   dion[j] = fdum
endfor

idum = 0
readf, pipe, idum
if idum EQ 1 then lnorm = 1 else lnorm = 0
readf, pipe, idum
nmet = idum
for j = 0, 3 do begin
   readf, pipe, idum
   imetr[j] = idum
endfor

readf, pipe, idum
if idum EQ 1 then liosel = 1 else liosel = 0
readf, pipe, idum
if idum EQ 1 then lhsel = 1 else lhsel = 0
readf, pipe, idum
if idum EQ 1 then lrsel = 1 else lrsel = 0
readf, pipe, idum
if idum EQ 1 then lisel = 1 else lisel = 0
readf, pipe, idum
if idum EQ 1 then lnsel = 1 else lnsel = 0
readf, pipe, idum
if idum EQ 1 then lpsel = 1 else lpsel = 0

readf, pipe, fdum
zeff = fdum

readf, pipe, nwvl
for j = 0, nwvl-1 do begin
   readf, pipe, idum
   readf, pipe, fdum
   readf, pipe, fdum1
   npix[j]  = idum
   wvmin[j] = fdum
   wvmax[j] = fdum1
endfor

readf, pipe, fdum
amin = fdum


; Processing screen

printf, pipe, 'CONTINUE'

printf, pipe, 'from run_adas810'
printf, pipe, ifout
printf, pipe, numt
printf, pipe, te
printf, pipe, tion
printf, pipe, th
printf, pipe, idout
printf, pipe, numd
printf, pipe, dens
printf, pipe, dion
printf, pipe, nmet
printf, pipe, imetr
printf, pipe, liosel
printf, pipe, lhsel
printf, pipe, lrsel
printf, pipe, lisel
printf, pipe, lnsel
printf, pipe, lpsel
printf, pipe, zeff
printf, pipe, lnorm
printf, pipe, nwvl
printf, pipe, npix
printf, pipe, wvmin
printf, pipe, wvmax
printf, pipe, amin


; Population calculation

n_call = numt * numd

if keyword_set(pbar) then begin

   adas_progressbar, pipe     = pipe,     $
                     n_call   = n_call,   $
                     adasprog = ' PROGRAM: ADAS810 '

endif else begin
   
   idum = 0
   for j = 0, n_call-1 do readf, pipe, idum

endelse

; Output screen

if strmid(strupcase(dsn15), 0, 4) EQ 'NULL' then a15out = 0 else a15out = 1  
if strmid(strupcase(dsn40), 0, 4) EQ 'NULL' then a40out = 0 else a40out = 1  
if strmid(strupcase(dsn11), 0, 4) EQ 'NULL' then a11out = 0 else a11out = 1  
if strmid(strupcase(dsn11f), 0, 4) EQ 'NULL' then a11fout = 0 else a11fout = 1  


printf, pipe, 'CONTINUE'
printf, pipe, outfile
printf, pipe, a15out 
printf, pipe, a40out 
printf, pipe, a11out 
printf, pipe, a11fout

printf, pipe, output_file
printf, pipe, a15dsn 
printf, pipe, a40dsn 
printf, pipe, a11dsn 
printf, pipe, a11fdsn

; Finish

printf, pipe, 'MENU'
printf, pipe, 1L

END
