; Copyright (c) 2003 Strathclyde University .
; SCCS info: Module @(#)$Header: /home/adascvs/idl/adas8xx/adas810/adas810_proc.pro,v 1.3 2015/12/20 18:36:48 mog Exp $ Date $Date: 2015/12/20 18:36:48 $
;+
; PROJECT:
;       ADAS 
;
; NAME:
;	ADAS810_PROC
;
; PURPOSE:
;	IDL ADAS user interface, processing options/input.
;
; EXPLANATION:
;	This routine creates and manages a pop-up window which allows
;	the user to select options and input data to control ADAS810
;	processing.
;
; USE:
;	This routine is ADAS810 specific.
;
; INPUTS:
;	VALUE	- A structure which determines the initial settings of
;		  the processing options widget.
;
;
;       BITFILE - String; the path to the directory containing bitmaps
;                 for the 'escape to series menu' button.
;
;
; OPTIONAL INPUTS:
;	None
;
; OUTPUTS:
;	VALUE	- On output the structure records the final settings of
;		  the processing selections widget if the user pressed the
;		  'Done' button, otherwise it is not changed from input.
;
;	REP	- String; Either 'CANCEL' or 'MENU' or 'CONTINUE' depending
;		  on how the user terminated the processing options
;		  window.
;
; OPTIONAL OUTPUTS:
;	None
;
; KEYWORD PARAMETERS:
;	FONT_LARGE - The name of a larger font.
;
;	FONT_SMALL - The name of a smaller font.
;
;	EDIT_FONTS - A structure of two fonts used in the table editor
;		     adas_edtab.pro, {font_norm:'',font_input:''}.
;
; CALLS:
;	DYNLABEL	Sets dynamic_resize keyword for label widgets.
;	ADAS810_PROC_EVENT Called indirectly during widget management,
;			   routine included in this file.
;	XMAMAGER
;
; SIDE EFFECTS:
;
;
; CATEGORY:
;	Widgets
;
; WRITTEN:
;       Martin O'Mullane
;
; MODIFIED:
;	1.1	Martin O'Mullane
;		 - First release 
;	1.2	Martin O'Mullane
;		 - Default density did not work as expected.
;                - Do not react to 'return' from title field.
;                - Number of metastable selected is checked
;                  and is not allowed to exceed 5.
;	1.3	Martin O'Mullane
;		 - Only initialze metastable selection if
;                  there are more than 3 levels in adf04 file.
;
; VERSION:
;       1.1	17-01-2003
;       1.2	29-05-2003
;       1.3	01-11-2010
;
;-
;-----------------------------------------------------------------------------

PRO ADAS810_PROC_MENU, event

; The 'normal' get_value from a widget_button does not work with a
; bitmap button. Hence this specialised event handler.

; Get the info structure out of the user value of the top-level base.

Widget_Control, event.top, Get_UValue=info


 formdata = {cancel : 0, menu:1}
 *info.ptrToFormData = formdata
 widget_Control, event.top, /destroy

END

;-----------------------------------------------------------------------------


PRO adas810_proc_event, event

; Get the info structure out of the user value of the top-level base.

Widget_Control, event.top, Get_UValue=info


; Clear any messages 

widget_control, info.messID, set_value = ' '


; Process events


CASE event.id OF

  info.cancelID : begin
                    formdata = {cancel:1}
                    *info.ptrToFormData = formdata
                    widget_Control, event.top, /destroy
                  end 

  
   info.deftID  : begin

                    message = 'Confirm Overwrite Temperatures with Defaults'
                    buttons = ['Confirm','Cancel']
                    action  = popup(message = message, buttons = buttons, $
                                    font = info.font)

                    if action eq 'Confirm' then begin
       
                       widget_control, info.tempID, get_value=tempval
               
                       index         = event.index
                       tempval.units = info.default_te.units[index]

                       numt = info.default_te.maxval[index]
                       deft = info.default_te.data[index,*]
                       
                       if numt GT 0 then begin
                         tempval.value[0,0:numt-1] = $
                             strtrim(string(deft[0:numt-1],format=info.num_form),2)
                         tempval.value[1,0:numt-1] = $
                             strtrim(string(deft[0:numt-1],format=info.num_form),2)
                         tempval.value[2,0:numt-1] = $
                             strtrim(string(deft[0:numt-1],format=info.num_form),2)
                       end

                       if numt LT info.ndtem then tempval.value[0:2,numt:info.ndtem-1] = ''
                       
                       widget_control, info.tempID, set_value=tempval
                     
                    endif

                  end
  

   info.defdID  : begin

                    message = 'Confirm Overwrite Densities with Defaults'
                    buttons = ['Confirm','Cancel']
                    action  = popup(message = message, buttons = buttons, $
                                    font = info.font)

                    if action eq 'Confirm' then begin
       
  	               widget_control,info.densID,get_value=densval
              
                       index          = event.index
	               densval.units = info.default_dens.units[index]

                       numd = info.default_dens.maxval[index]
                       defd = info.default_dens.data[index,*]
                       
 	               if numd GT 0 then begin
   	                 densval.value[0,0:numd-1] = $
		             strtrim(string(defd[0:numd-1],format=info.num_form),2)
   	                 densval.value[1,0:numd-1] = '0.0000'
 	               end

 	               widget_control,info.densID,set_value=densval

	            endif

                  end


   
   

   info.doneID  : begin

                   ; Gather the data for return to calling program
                   ; but do some consistency checks first
                
                    err  = 0
                    mess = ''
                    
                    ; Title of run
                    
                    widget_control, info.runID, get_value=title
                    
                    
                    ; Plasma parameters
                    
                    if err EQ 0 then begin
                       
                       widget_control, info.tempID, get_value=tempval
                       
                       blanks = where(strtrim(tempval.value[0,*],2) ne '', maxt)
                       ifout  = tempval.units+1
                       temp   = double(tempval.value)
                       tine   = reform(temp[0,*])
                       tinp   = reform(temp[1,*])
                       tinh   = reform(temp[2,*])
                       
                       widget_control, info.densID, get_value=tempval
                       
                       blanks = where(strtrim(tempval.value[0,*],2) ne '', maxd)
                       idout  = tempval.units+1
                       temp   = double(tempval.value)
                       dine   = reform(temp[0,*])
                       dinp   = reform(temp[1,*])
                      
                    endif
                    
                    
                    ; Metastables selected
                    
                    widget_control, info.selID, get_value=selected

                    ind = where(selected EQ 1, nmet)
                    if nmet GT 4 then begin
                       err  = 1
                       mess = 'A maximum of 4 metastables can be selected'
                    endif 
                    if err EQ 0 AND nmet EQ 0 then begin
                       err  = 1
                       mess = 'Choose a metastable'
                    endif 
                    
                    if err EQ 0 then begin
                       imetr = intarr(4)
                       imetr[0:nmet-1] = ind + 1
                    endif
                    
                    widget_control, info.normID, get_value=norm
                    
                    
                    ; Reactions
                    
                    widget_control, info.reactID, get_value=reacset
                    widget_control, info.zeffID, get_value=zeff
                    
                    if err EQ 0 AND reacset[5] EQ 1 then begin
                       err = num_chk(zeff[0], sign=1)
                       if err EQ 1 then mess = 'Incorrect Zeff'
                    endif
                    
                    ; fPEC intervals
                    
                    if err EQ 0 then begin
                       
                       widget_control, info.wvlID, get_value=tempval
                    
                       blanks = where(strtrim(tempval.value[0,*],2) ne '', maxw)
                       temp   = double(tempval.value)
                       npix   = reform(temp[0,*])
                       wvmin  = reform(temp[1,*])
                       wvmax  = reform(temp[2,*])
                    
                    endif
                    
                    if err EQ 0 then widget_control, info.avalID, get_value=aval
                    
                    if err EQ 0 then begin
                       err = num_chk(aval[0], sign=0)
                       if err EQ 1 then mess = 'Incorrect minimum A-value'
                    endif
                   
                    ; Return information
                                           
                    if err EQ 0 then begin
                       formdata = {title    : title,         $
                                   ifout    : ifout,         $
                                   maxt     : maxt,          $
                                   tine     : tine,          $
                                   tinp     : tinp,          $
                                   tinh     : tinh,          $
                                   idout    : idout,         $
                                   maxd     : maxd,          $
                                   dine     : dine,          $
                                   dinp     : dinp,          $
                                   nmet     : nmet,          $
                                   imetr    : imetr,         $
                                   norm     : norm[0],       $
                                   react    : reacset,       $
                                   zeff     : zeff[0],       $
                                   maxw     : maxw,          $
                                   npix     : npix,          $
                                   wvmax    : wvmax,         $
                                   wvmin    : wvmin,         $
                                   aval     : aval[0],       $
                                   menu     : 0,             $
                                   cancel   : 0              }
                        *info.ptrToFormData = formdata
                        widget_control, event.top, /destroy
                    endif else begin
                       widget_Control, info.messID, Set_Value=mess
                    endelse

                  end
                   
   info.selID   : begin
                  
	             widget_control, info.selID, get_value=selected
	             sels = where(selected eq 1, count)

                     ; only permit ndmet

                     if count GT info.ndmet then begin

                        str = 'A maximum of ' + strtrim(string(info.ndmet),2) $
                               + ' metastables are permitted'
                        widget_control, info.messID, set_value=str
                        widget_control, info.selID, get_value=sels
                        sels[event.value] = 0
                        widget_control, info.selID, set_value=sels 
                     
                     endif
                    
                  
                  end
                  
   info.reactID : ; so nothing
  
   ELSE :  print,'ADAS810_PROC_EVENT : You should not see this message! '
  
ENDCASE


; update the info (except when we cancel!)

if event.id EQ info.cancelid OR event.id EQ info.doneID then print,'' $ ; do nothing 
            else Widget_Control, event.top, Set_UValue=info



END

;-----------------------------------------------------------------------------


FUNCTION adas810_proc, value,                     $
                       adf04_levels,              $
                       adf04_te,                  $
                       method,                    $                  
                       dsn04,                     $
                       dsn18,                     $
                       dsn42,                     $
                       bitfile,                   $
                       FONT_LARGE = font_large,   $    
                       FONT_SMALL = font_small,   $    
                       EDIT_FONTS = edit_fonts


; Constants of this subroutine

  num_form = '(E10.3)'
  ndtem    = 24
  ndden    = 24
  ndwvl    = 5
  ndmet    = 5
  

; Set defaults for keywords

  if not keyword_set(font_large) then font_large = ''
  if not keyword_set(font_small) then font_small = ''
  if not keyword_set(edit_fonts) then edit_fonts = { font_norm  : '', $
                                                     font_input : ''}

; Worry about special HP case

  machine = GETENV('TARGET_MACHINE')  
  if machine eq 'HPUX' then begin     
      y_size = 4                      
      large_font = font_small         
  endif else begin                    
      y_size = 6                      
      large_font = font_large         
  endelse                             



; Copy temperature, density and spectral interval data 
; into string arrays for use in table editor


  ; Temperature
  
  nv = n_elements(adf04_te)
  
  tempdata = strarr(6,ndtem)

  if value.numt gt 0 then begin
    tempdata[0, 0:value.numt-1] = $
		strtrim(string(value.te[0:value.numt-1],format=num_form),2)
    tempdata[1, 0:value.numt-1] = $
		strtrim(string(value.tion[0:value.numt-1],format=num_form),2)
    tempdata[2, 0:value.numt-1] = $
		strtrim(string(value.th[0:value.numt-1],format=num_form),2)
  endif

  tempdata[3,0:nv-1] = strtrim(string(adf04_te[0:nv-1],format=num_form),2)
  tempdata[4,0:nv-1] = strtrim(string(adf04_te[0:nv-1]/11605.0,format=num_form),2)
  tempdata[5,0:nv-1] = strtrim(string(adf04_te[0:nv-1]/(value.iz1*value.iz1),format=num_form),2)

  if value.numt LT ndtem then tempdata[0:2,value.numt:ndtem-1] = ''
  tempdata[3:5,nv:ndtem-1] = ''


  ; Density
  
  densdata = strarr(2,ndden)
  
  if value.numd gt 0 then begin
  
    densdata[0,0:value.numd-1] = $
		strtrim(string(value.dens[0:value.numd-1],format=num_form),2)
    densdata[1,0:value.numd-1] = $
		strtrim(string(value.dion[0:value.numd-1],format=num_form),2)
                
  endif
  
  if value.numd LT ndden then densdata[*,value.numd:ndden-1] = ''


  ; Spectral Intervals
  
  wvldata = strarr(3,ndwvl)
  
  if value.nwvl gt 0 then begin
  
    wvldata[0,0:value.nwvl-1] = $
		strtrim(string(value.npix[0:value.nwvl-1],format='(i4)'),2)
    wvldata[1,0:value.nwvl-1] = $
		strtrim(string(value.wvmin[0:value.nwvl-1],format='(f10.2)'),2)
    wvldata[2,0:value.nwvl-1] = $
		strtrim(string(value.wvmax[0:value.nwvl-1],format='(f10.2)'),2)
                
  endif
  
  if value.nwvl LT ndwvl then wvldata[*,value.nwvl:ndwvl-1] = ''



; Create top level base widget

  parent = widget_base(Column=1, Title='ADAS810 PROCESSING OPTIONS', $
                       xoffset=50, yoffset=1)

  rc     = widget_label(parent,value='  ',font=font_small)

  base   = widget_base(parent, /column)


; Information

  mbase = widget_base(base, /row)
  rc    = widget_label(mbase, value='Title for Run', font=font_large)
  runID = widget_text(mbase,value=value.title, xsize=40, font=font_large,  $
                            /edit, EVENT_FUNC = 'ADAS_PROC_NULL_EVENTS')

  sz0   = strcompress(string(value.iz0))
  sz1   = strcompress(string(value.iz1))
		
  lab   = widget_label(mbase,font=font_large, $
                       value='Nuclear Charge:'+sz0+'  Ion Charge:'+sz1)


; Filenames

  mbase = Widget_base(base, /column)
  
  if method EQ 'DRIVER' then begin
     
     lab = cw_adas_dsbr(mbase, dsn42, font=font_small, $
                               filelabel='Input from adf42 file : ',/row)
  
  endif
  
 
  mbase  = widget_base(mbase,/column,/frame)
    
  lab = cw_adas_dsbr(mbase, dsn04, font=font_small, $
                            filelabel='adf04 file : ',/row)
  if dsn18 EQ 'NULL' then begin
     lab = widget_label(mbase, value='No expansion data', font=font_small)
  endif else begin
     lab = cw_adas_dsbr(mbase, dsn18, font=font_small, $
                               filelabel='adf18 file : ',/row)
  endelse
  

                
                
; Temperature and density tables                
                
                
  tablebase = widget_base(base,/row)
  
  
  ; Temperature
  
  mbase    = widget_base(tablebase,/column,/frame)
  
  colhead  = [['Electron','Ion','Neutral','Input'],['','','Hydrogen','Value']]
  
  units    = value.ifout-1
  unitname = ['Kelvin','eV','Reduced']

  unitind = [[0,0,0],[1,1,1],[2,2,2],[3,4,5]]
  tempID  = cw_adas_table(mbase, tempdata, units, unitname, unitind,    $
			  UNITSTITLE = 'Temperature Units', /scroll,   $
			  COLEDIT = [1,1,1,0], COLHEAD = colhead,      $
			  TITLE = 'Temperatures', ORDER = [1,0,0,0],   $
			  LIMITS = [2,1,1,2], CELLSIZE = 12,           $
			  FONTS = edit_fonts, FONT_LARGE = font_large, $
			  FONT_SMALL = font_small,                     $
                          EVENT_FUNCT = 'ADAS_PROC_NULL_EVENTS')

  ; Drop down Te defaults - use 208 set

  adas208_get_defaults, 'TE', ndtem, ndden, default_te
  deftID  = widget_droplist(mbase,value = default_te.names,font=font_large, $
                            title='Default:')


  ; Densities

  mbase    = widget_base(tablebase,/column,/frame) 

  col      = widget_base(mbase,/column)

  colhead  = [ ['Electron', 'Ion'], ['Densities','Densities'] ]

  units    = value.idout-1
  unitname = ['cm-3','Reduced']

  unitind  = [[0,0],[1,1]]
  densID   = cw_adas_table(col, densdata, units, unitname, unitind,     $
			   UNITSTITLE = 'Density Units',                $
			   COLHEAD = colhead, /scroll,                  $
			   TITLE = 'Densities', ORDER = [1,0],          $
			   LIMITS = [2,1], CELLSIZE = 12,               $
			   FONTS = edit_fonts, FONT_LARGE = font_large, $
			   FONT_SMALL = font_small,                     $
                           EVENT_FUNCT = 'ADAS_PROC_NULL_EVENTS')


  ; Drop down density defaults - use 208 set

  adas208_get_defaults, 'DENS', ndtem, ndden, default_dens
  defdID  = widget_droplist(mbase,value = default_dens.names,font=font_large, $
                            title='Default:')




; Next row of inputs

  botbase = widget_base(parent,/row)
  mbase   = widget_base(botbase,/row)



; Spectral intervals, metastables and reaction               
                
                
  tablebase = widget_base(mbase,/row)


  ; Metastable selection list

  mbase =  widget_base(tablebase,/column,/frame)
  
  slen  = max(strlen(adf04_levels))
  longS = ''
  for j=0, slen-1+4 do longS = longS + 'H'
 
  bd = widget_base(map=0)
  lb = widget_label(bd, /dynamic_resize, value='',font=font_large)
  widget_control, lb, set_value=longS
  wd = (widget_info(lb, /geometry)).scr_xsize  
  xs = fix(wd)

  buttons = adf04_levels
  
  tmp     =  widget_label(mbase,value='Metastable State', font=font_large)
  selID   =  cw_bgroup(mbase,buttons,                 $
                       nonexclusive=1,column=1,       $
                       set_value = set_button,        $
                       scroll=1, y_scroll_size=120,   $
                       xsize=xs,x_scroll_size=xs,     $
                       /frame, font=font_large    )
                       
  ; Ask whether to normalise
  
  
  text0 = "For a single metastable"
  text1 = "normalise PLT and PEC ?"
  base  = widget_base(mbase, /row)
  base1 = widget_base(base, /column)
  l = widget_label(base1, value=text0, font=font_large)
  l = widget_label(base1, value=text1, font=font_large)
  normID = cw_bgroup(base1, ["NO","YES"], set_value=0,        $
                       /no_release, /exclusive,               $
                       event_funct = 'ADAS_PROC_NULL_EVENTS', $
                       font = font_large)
 

  ; Reactions to include
  
  mbase   = widget_base(tablebase,/column,/frame)


  react_names = ['Ionisation Rates ',         $
                 'Charge Exchange  ',         $
                 'Recombination    ',         $
                 'Inner Shell Ionisation  ',  $
                 'Include Projection Data ',  $
                 'Proton Impact Collisions']
                 
  tmp     =  widget_label(mbase,value='Include Reactions:', font=font_large)
  reactID = cw_bgroup(mbase,react_names,nonexclusive=1,column=1, $
                     font=font_large, event_funct = 'ADAS_PROC_NULL_EVENTS')

  ; Ask for zeff for proton impact scaling

  zeffID  = cw_field(mbase, title='Zeff :', $
                     floating=1, fieldfont=font_small, font=font_small) 


  ; Spectral intervals
  
  mbase = widget_base(tablebase,/column,/frame)
  
  wvlID = cw_adas_table(mbase, wvldata,                                 $
              coledit=[1,1,1],		                                $
              colhead=['# pixels','min wave','max wave'],/scroll,       $
	      title='Spectral Intervals',limits=[1,2,2],CELLSIZE = 12, 	$
	      fltint=['(i4)','(f10.2)','(f10.2)'],                      $
              /rowskip,  /difflen, /gaps,                               $
              FONTS = edit_fonts, font_small=font_small,                $
              font_large=large_font,                                    $
              EVENT_FUNCT = 'ADAS_PROC_NULL_EVENTS')


  ; Ask for minimum A-value

  avalID  = cw_field(mbase, title='Lower limit of A-value:', $
                     floating=1, fieldfont=font_small, font=font_small) 




; End of panel message and buttons                

  message  = 'Edit the processing options data and press Done to proceed'

  messID   = widget_label(parent, value=message, font=font_large)
                            
  menufile = bitfile + '/menu.bmp'
  read_X11_bitmap, menufile, bitmap1
                
  mrow     = widget_base(parent,/row)
  menuID   = widget_button(mrow,value=bitmap1,font=font_large, $
                           event_pro='ADAS810_PROC_MENU')
  cancelID = widget_button(mrow,value='Cancel',font=font_large)
  doneID   = widget_button(mrow,value='Done',font=font_large)




; Initialise the widget

  reac_set = [value.liosel, value.lhsel, value.lrsel, value.lisel, $
              value.lnsel, value.lpsel ]
              
  widget_control, runID, set_value=value.title
  if n_elements(adf04_levels) GE 4 then widget_control, selID, set_value=value.imetr
  widget_control, normID, set_value=value.lnorm
  widget_control, reactID, set_value=reac_set
  widget_control, zeffID, set_value=value.zeff
  widget_control, avalID, set_value=value.amin



; Realize the ADAS810_PROC input widget.


   dynlabel, parent
   widget_Control, parent, /realize



; Create a pointer to store the information collected from the form.
; The initial data stored here is set to CANCEL, so nothing needs to
; be done if the user kills the widget with the mouse.

   ptrToFormData = Ptr_New({cancel:1, menu:0})

; Create an info structure with program information.

  info = { messID          :   messID,         $
           runID           :   runID,          $
           deftID          :   deftID,         $
           tempID          :   tempID,         $
           defdID          :   defdID,         $
           densID          :   densID,         $
           selID           :   selID,          $
           normID          :   normID,         $
           reactID         :   reactID,        $
           zeffID          :   zeffID,         $
           wvlId           :   wvlID,          $
           avalID          :   avalID,         $
           cancelID        :   cancelID,       $
           doneID          :   doneID,         $
           font            :   font_large,     $
           default_dens    :   default_dens,   $
           default_te      :   default_te,     $
           ndtem           :   ndtem,          $
           ndden           :   ndden,          $
           ndmet           :   ndmet,          $
           num_form        :   num_form,       $
           group_leader    :   parent,         $
           ptrToFormData   :   ptrToFormData   } 
  
           
; Store the info structure in the user value of the top-level base and launch
; the widget into the world.

  widget_control, parent, Set_UValue=info

  XManager, 'ADAS810_PROC', parent, Event_Handler='ADAS810_PROC_EVENT'


; When the widget is destroyed, the block is released, and we
; return here. Get the form data that was collected by the form
; and stored in the pointer location.

formdata = *ptrToFormData

rep='CONTINUE'
IF N_Elements(formdata) EQ 0 THEN BEGIN
   Ptr_Free, ptrToFormData
   rep ='CANCEL'
   RETURN,rep
ENDIF
 
IF formdata.cancel EQ 1 THEN BEGIN
   Ptr_Free, ptrToFormData
   rep ='CANCEL'
   RETURN,rep
ENDIF

IF formdata.menu EQ 1 THEN BEGIN
   Ptr_Free, ptrToFormData
   rep ='MENU
   RETURN,rep
ENDIF


if rep eq 'CONTINUE' then begin
   
   value.title   =  formdata.title
   value.ifout   =  formdata.ifout
   value.numt    =  formdata.maxt 
   value.te      =  formdata.tine  
   value.tion    =  formdata.tinp 
   value.th      =  formdata.tinh 
   value.idout   =  formdata.idout
   value.numd    =  formdata.maxd 
   value.dens    =  formdata.dine 
   value.dion    =  formdata.dinp 
   value.nmet    =  formdata.nmet    
   temp_imetr    =  formdata.imetr   
   value.imetr   =  temp_imetr   
   value.liosel  =  formdata.react[0]
   value.lhsel   =  formdata.react[1]
   value.lrsel   =  formdata.react[2]
   value.lisel   =  formdata.react[3]
   value.lnsel   =  formdata.react[4]
   value.lpsel   =  formdata.react[5]
   value.zeff    =  formdata.zeff 
   value.lnorm   =  formdata.norm
   value.nwvl    =  formdata.maxw
   value.npix    =  formdata.npix 
   value.wvmin   =  formdata.wvmin
   value.wvmax   =  formdata.wvmax
   value.amin    =  formdata.aval

   Ptr_Free, ptrToFormData

endif       


RETURN, rep


END
