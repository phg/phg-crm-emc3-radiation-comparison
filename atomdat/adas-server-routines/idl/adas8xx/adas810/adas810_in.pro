; Copyright (c) 2003 Strathclyde University .
; SCCS info: Module @(#)$Header: /home/adascvs/idl/adas8xx/adas810/adas810_in.pro,v 1.1 2004/07/06 11:07:30 whitefor Exp $ Date $Date: 2004/07/06 11:07:30 $
;+
; PROJECT:
;       ADAS 
;
; NAME:
;	ADAS810_IN
;
; PURPOSE: 
;       This function puts up a widget asking for an adf04 input file 
;       and an 9optional) adf18/a17_p208/ expansion file.
;
;
; EXPLANATION:
;       Similar to ADAS208 input screen but with a different (better?)
;       way of dealing with events.
;     
;
; NOTES:
;       
; 
;
;
; INPUTS:
;        inval       - A structure holding the 'remembered' input data.
;
; OPTIONAL INPUTS:
;       None
;
; OUTPUTS:
;       The function returns a string rep
;           rep = 'CONTINUE' if everything is ok
;           rep = 'CANCEL' if there is a problem or indeed if the user
;                          closes the widget with the mouse. 
;
; OPTIONAL OUTPUTS:
;       None
;
; KEYWORD PARAMETERS:
;       FONT_LARGE - The name of a larger font e.g 'courier_bold14'
;
;       FONT_SMALL - The name of a smaller font e.g 'courier_bold12'
;
;
; CALLS:
;       ADAS807_IN_FILE     : Reacts to adf04 file widget and sensitises
;                             'Done' and 'Browse' if a valid file has
;                             been selected.
;       ADAS810_IN_EVENT    : Reacts to Cancel, Done and Browse.
;
;
; SIDE EFFECTS:
;
; CATEGORY:
;	Widgets
;
; WRITTEN:
;       Martin O'Mullane
;
; MODIFIED:
;	1.1	Martin O'Mullane
;		First release 
;
; VERSION:
;       1.1	30-11-2002
;
;-
;-----------------------------------------------------------------------------

FUNCTION ADAS810_IN_FILE, event

   ; Sensitise 'Done' and 'Browse' if we are ready to continue 

   Widget_Control, event.top, Get_UValue=info
   
   Widget_Control, info.branchID, get_value = branch
   
   if branch[0] EQ 0 then Widget_Control, info.flaID, Get_Value=file $
                          else Widget_Control, info.fldID, Get_Value=file

   
   filename = file.rootpath + file.file
   file_acc, filename, exist, read, write, execute, filetype
     
   
   if event.action EQ 'newfile' AND filetype EQ '-' then begin
      Widget_Control, info.doneID, sensitive=1
      Widget_Control, info.browseID, sensitive=1
   endif else begin
      Widget_Control, info.doneID, sensitive=0
      Widget_Control, info.browseID, sensitive=0
   endelse
      
   
   RETURN,0

END 

;-----------------------------------------------------------------------------


FUNCTION ADAS801_IN_BRANCH_EVENTS, event

   ; Map and unmap adf42 and the adf04 bases as appropriate
   

   Widget_Control, event.top, Get_UValue=info
   
   if event.value EQ 0 then begin
   
      Widget_Control, info.filebase, map=1
      Widget_Control, info.driverbase, map=0
      Widget_Control, info.messID, set_value = info.branch_mess[0]
      Widget_Control, info.flaID, Get_Value=file

      Widget_Control, info.extraID, map=1
   
   endif else begin
   
      Widget_Control, info.filebase, map=0
      Widget_Control, info.driverbase, map=1
      Widget_Control, info.messID, set_value = info.branch_mess[1]
      Widget_Control, info.fldID, Get_Value=file
  
      Widget_Control, info.extraID, map=0
      
   endelse
   
   
   ; Sensitise Done and Browse if necessary
   
   filename = file.rootpath + file.file
   file_acc, filename, exist, read, write, execute, filetype
     
   
   if exist EQ 1 AND read EQ 1 AND filetype EQ '-' then begin
      Widget_Control, info.doneID, sensitive=1
      Widget_Control, info.browseID, sensitive=1
   endif else begin
      Widget_Control, info.doneID, sensitive=0
      Widget_Control, info.browseID, sensitive=0
   endelse
   
   RETURN,0

END 
;-----------------------------------------------------------------------------



PRO ADAS810_IN_EVENT, event

   ; Get the info structure out of the user value of the top-level base.

Widget_Control, event.top, Get_UValue=info
Widget_Control, event.id, Get_Value=userEvent


CASE userEvent OF

  'Cancel' : begin
               formdata = {cancel:1}
               *info.ptrToFormData =formdata
               widget_Control, event.top, /destroy
             end 

  'Expansion and filter files...' : begin
                
                flbval = info.flbval
                flcval = info.flcval
               
                rep=adas810_in_opt(flbval, flcval,                $
                                   parent     = info.parent,      $
                                   FONT_SMALL = info.font_small,  $
                                   FONT_LARGE = info.font_large   )
  
       
                ; Remember to store file structures in the Uvalue
                                 
                if rep EQ 'CONTINUE' then begin
                    info.flbval = flbval
                    info.flcval = flcval
                   Widget_Control, event.top, Set_UValue=info
                endif

             end
                   
  'Done'   : begin
                 
                ; gather the data for return to calling program
                ; 'Done' is not sensitised until valid filenames
                ; have been chosen.
               
                widget_Control, info.flaID, Get_Value=vala
                widget_Control, info.fldID, Get_Value=vald

                widget_Control, info.branchID, Get_Value=branch
                
                err  = 0
                mess = ''
                               
                if err EQ 0 then begin
                   formdata = {vala    : vala,         $
                               valb    : info.flbval,  $
                               valc    : info.flcval,  $
                               vald    : vald,         $
                               branch  : branch,       $
                               cancel  : 0             }
                    *info.ptrToFormData = formdata
                    widget_control, event.top, /destroy
                endif else begin
                   widget_Control, info.messID, Set_Value=mess
                endelse

             end
             
    'Browse Comments' : Begin
    
                Widget_Control, info.branchID, get_value = branch
                
                if branch[0] EQ 0 then begin
                
                   Widget_Control, info.flaID, get_value=val
                   filename = val.rootpath + val.file
                   file_acc, filename, exist, read, write, execute, filetype
                   if exist EQ 1 AND read EQ 1 AND filetype EQ '-' then $
                      xxtext, filename, font=info.font_large
                                         
                 endif else begin
                 
                   Widget_Control, info.fldID, get_value=val
                   filename = val.rootpath + val.file
                   file_acc, filename, exist, read, write, execute, filetype
                   if exist EQ 1 AND read EQ 1 AND filetype EQ '-' then $
                      xxtext, filename, font=info.font_large
                
                 endelse

             end     
                
  else : print,'ADAS810_IN_EVENT : You should not see this message! ',userEvent
                   
ENDCASE

END
  
;-----------------------------------------------------------------------------


FUNCTION adas810_in, inval,                   $
                     FONT_LARGE = font_large, $
                     FONT_SMALL = font_small


; Set defaults for keywords

  IF n_elements(font_large) eq 0 THEN font_large = ''
  IF n_elements(font_small) eq 0 THEN font_small = ''


; Path names may be supplied with or without the trailing '/'.  Add
; this character where required so that USERROOT will always end in
; '/' on output.
  
  if strtrim(inval.arootpath) eq '' then begin
      inval.arootpath = './'
  endif else if                                                   $
  strmid(inval.arootpath, strlen(inval.arootpath)-1,1) ne '/' then begin
      inval.arootpath = inval.arootpath+'/'
  endif
  
  if strtrim(inval.drootpath) eq '' then begin
      inval.drootpath = './'
  endif else if                                                   $
  strmid(inval.drootpath, strlen(inval.drootpath)-1,1) ne '/' then begin
      inval.drootpath = inval.drootpath+'/'
  endif



; Top level (modal) base widget                
  
  
  parent = Widget_Base(Column=1, Title='ADAS 810 INPUT', $
                       XOFFSET=100, YOFFSET=1)

                
  rc       = widget_label(parent,value='  ',font=font_large)
  base     = widget_base(parent,  /align_center,/column)
  
  buttons  = ['Standard file (adf04)','Driver File (adf42)']
  branchID = cw_bgroup(base,buttons,                             $
                       exclusive=1,row=1,                        $
                       event_func = 'ADAS801_IN_BRANCH_EVENTS',  $
                       font=font_large    )

  switchb     = widget_base(base)
  
  filebase    = widget_base(switchb, /column)
  driverbase  = widget_base(switchb, /column)

  

; 'FILE' branch : Ask for adf04 and adf18/a17_p208 input selections


  flabase = widget_base(filebase, /column, /frame)
  flaval = {  ROOTPATH        :       inval.arootpath,                $
              FILE            :       inval.afile,                    $
              CENTROOT        :       inval.acentroot,                $
              USERROOT        :       inval.auserroot                 }
  flatitle = widget_label(flabase, font=font_large,                   $
                          value='Specific Ion File Details:-')
  flaID = cw_adas_infile(flabase, value=flaval, font=font_small, $
                         ysize = 15, event_func='ADAS810_IN_FILE')

; Expansion and filter file details

  flbval = {  ROOTPATH        :       inval.brootpath,                $
              FILE            :       inval.bfile,                    $
              CENTROOT        :       inval.bcentroot,                $
              USERROOT        :       inval.buserroot                 }

  flcval = {  ROOTPATH        :       inval.crootpath,                $
              FILE            :       inval.cfile,                    $
              CENTROOT        :       inval.ccentroot,                $
              USERROOT        :       inval.cuserroot                 }



; 'DRIVER' branch : Ask for adf42 input selection


  fldbase = widget_base(driverbase, /column, /frame)
  fldval = {  ROOTPATH        :       inval.drootpath,                $
              FILE            :       inval.dfile,                    $
              CENTROOT        :       inval.dcentroot,                $
              USERROOT        :       inval.duserroot                 }
  fldtitle = widget_label(fldbase, font=font_large,                   $
                          value='Driver File Details:-')
  fldID = cw_adas_infile(fldbase, value=fldval, font=font_small, $
                         ysize = 15, event_func='ADAS810_IN_FILE')



; Control buttons
                
  mrow     = widget_base(parent,/row,/align_center)
  messID   = widget_label(mrow,font=font_large, value=' ')
                
  mrow     = widget_base(parent,/row)
  
  browseID = widget_button(mrow,value='Browse Comments', font=font_large)
  cancelID = widget_button(mrow,value='Cancel',font=font_large)
  doneID   = widget_button(mrow,value='Done',font=font_large)
  labID    = widget_label(mrow,font=font_large, value='  ')

  ebase    = widget_base(mrow,/row)
  extraID  = widget_button(ebase,value='Expansion and filter files...',font=font_large)


; Initial settings - depends on branch

  branch_mess = [ '       Enter specific ion and ' +        $
                  '(optional) expansion file names       ', $
                  '       Enter driver file name ']


  if inval.branch EQ 'FILE' then begin
     
     filename = inval.arootpath + inval.afile
     widget_control, driverbase, map=0
     widget_control, messID, set_value = branch_mess[0]
     widget_control, branchID, set_value = 0
     filename = inval.arootpath + inval.afile
     widget_control, extraID, map=1
     
  endif else begin
     
     filename = inval.drootpath + inval.dfile
     widget_control, filebase, map=0
     widget_control, messID, set_value = branch_mess[1]
     widget_control, branchID, set_value = 1
     filename = inval.drootpath + inval.dfile
     widget_control, extraID, map=0
  
  endelse


; Check filenames and desensitise 'Done' if adf04 or adf42 selection is a 
; directory or if it is a file without read access.                   

  browseallow = 0
  file_acc, filename, fileexist, read, write, execute, filetype
  if filetype ne '-' then begin
      Widget_Control, browseID, sensitive=0
      Widget_Control, doneID, sensitive=0
  endif else begin
      if read eq 0 then begin
          Widget_Control, browseID, sensitive=0
          Widget_Control, doneID, sensitive=0
      endif else begin
          browseallow = 1
      endelse
  endelse


; Realize the ADAS810 input widget.
   
   dynlabel, parent
   widget_Control, parent, /realize

; Create a pointer to store the information collected from the form.
; The initial data stored here is set to CANCEL, so nothing needs to
; be done if the user kills the widget with the mouse.

   ptrToFormData = Ptr_New({cancel:1})

; Create an info structure with program information.

  info = { parent          :  parent,             $
           flaID           :  flaID,              $
           fldID           :  fldID,              $
           branchID        :  branchID,           $
           flbval          :  flbval,             $
           flcval          :  flcval,             $
           filebase        :  filebase,           $
           driverbase      :  driverbase,         $
           doneID          :  doneID,             $
           browseID        :  browseID,           $
           messID          :  messID,             $
           extraID         :  extraID,            $
           branch_mess     :  branch_mess,        $
           font_large      :  font_large,         $
           font_small      :  font_small,         $
           ptrToFormData   :  ptrToFormData       }  
            
               
; Store the info structure in the user value of the top-level base and launch
; the widget into the world.

  widget_control, parent, Set_UValue=info

  XManager, 'adas810_in', parent, Event_Handler='ADAS810_IN_EVENT'


; When the widget is destroyed, the block is released, and we
; return here. Get the data that was collected by the widget
; and stored in the pointer location. Finally free the pointer.

formdata = *ptrToFormData

rep='CONTINUE'
IF N_Elements(formdata) EQ 0 THEN BEGIN
   Ptr_Free, ptrToFormData
   rep ='CANCEL'
ENDIF
 
IF formdata.cancel EQ 1 THEN BEGIN
   Ptr_Free, ptrToFormData
   rep ='CANCEL'
ENDIF

if rep eq 'CONTINUE' then begin
   
   inval.arootpath   = formdata.vala.rootpath
   inval.afile       = formdata.vala.file
   inval.acentroot   = formdata.vala.centroot
   inval.auserroot   = formdata.vala.userroot
   inval.brootpath   = formdata.valb.rootpath
   inval.bfile       = formdata.valb.file
   inval.bcentroot   = formdata.valb.centroot
   inval.buserroot   = formdata.valb.userroot
   inval.crootpath   = formdata.valc.rootpath
   inval.cfile       = formdata.valc.file
   inval.ccentroot   = formdata.valc.centroot
   inval.cuserroot   = formdata.valc.userroot
   inval.drootpath   = formdata.vald.rootpath
   inval.dfile       = formdata.vald.file
   inval.dcentroot   = formdata.vald.centroot
   inval.duserroot   = formdata.vald.userroot
   
   if formdata.branch EQ 0 then begin
      inval.branch = 'FILE' 
      rep          = 'FILE'
   endif else begin
      inval.branch = 'DRIVER' 
      rep          = 'DRIVER'
   endelse
   
   Ptr_Free, ptrToFormData
   
endif       



RETURN,rep

END
