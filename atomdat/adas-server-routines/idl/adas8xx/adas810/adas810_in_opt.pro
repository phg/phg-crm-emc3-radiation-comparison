; Copyright (c) 1995 Strathclyde University 
; SCCS info: Module @(#)$Header: /home/adascvs/idl/adas8xx/adas810/adas810_in_opt.pro,v 1.2 2004/07/06 11:07:36 whitefor Exp $ Date $Date: 2004/07/06 11:07:36 $
;+
; PROJECT:
;       ADAS 
;
; NAME:
;	ADAS810_IN_OPT
;
; PURPOSE: 
;       This function puts up a widget asking for optional parameters
;       such as scaling factors, temperature sets and E1/M1/E2 choices. 
;
;
; EXPLANATION:
;             
;     
;
; NOTES:
;       Based on the techniques of ADAS403. It entirely in IDL and uses
;       v5 (and above) features such as pointers to pass data.
; 
;
;
; INPUTS:
;        inval       - A structure holding the 'remembered' input data.
;
; OPTIONAL INPUTS:
;       None
;
; OUTPUTS:
;       The function returns a string rep
;           rep = 'CONTINUE' if everything is ok
;           rep = 'CANCEL' if there is a problem or indeed if the user
;                          closes the widget with the mouse. 
;
; OPTIONAL OUTPUTS:
;       None
;
; KEYWORD PARAMETERS:
;       FONT_LARGE - The name of a larger font e.g 'courier_bold14'
;
;       FONT_SMALL - The name of a smaller font e.g 'courier_bold12'
;
;
; CALLS:
;       ADAS810_IN_OPT_DEF   : Event handler for Temperature selection.                     
;       ADAS810_IN_OPT_UNIT  : Converts between red. eV and K temperatures.
;       ADAS810_IN_OPT_EVENT : General event handler.
;
; SIDE EFFECTS:
;
; CATEGORY:
;	Widgets
;
; WRITTEN:
;       Martin O'Mullane
;
; MODIFIED:
;	1.1	Martin O'Mullane
;		 - First release. 
;	1.2	Martin O'Mullane
;		 - Switch on the filter file selection.
;
; VERSION:
;       1.1	10-02-2003
;       1.2	28-02-2004
;
;-
;-----------------------------------------------------------------------------


PRO ADAS810_IN_OPT_EVENT, event

   ; Get the info structure out of the user value of the top-level base.

Widget_Control, event.top, Get_UValue=info

Widget_Control, event.id, Get_Value=userEvent


CASE userEvent OF

  'Cancel' : begin
               formdata = {cancel:1}
               *info.ptrToFormData =formdata
               widget_Control, event.top, /destroy
             end 

      
  'Done'   : begin
                  
                widget_control, info.flbID, get_value=flb
                widget_control, info.flcID, get_value=flc
                
                formdata = { flbval  :  flb,    $
                             flcval  :  flc,    $
                             cancel  :  0       }
                *info.ptrToFormData = formdata
                widget_control, event.top, /destroy

             end

  
  
  else : print,'ADAS810_IN_OPT_EVENT : You should not see this message! '
                   
ENDCASE

END
  
;-----------------------------------------------------------------------------


FUNCTION adas810_in_opt, flbval, flcval,            $
                         PARENT     = parent,       $
                         FONT_LARGE = font_large,   $
                         FONT_SMALL = font_small

; Set defaults for keywords 

  IF n_elements(font_large) eq 0 THEN font_large = ''
  IF n_elements(font_small) eq 0 THEN font_small = ''


; Path names may be supplied with or without the trailing '/'.  Add
; this character where required so that USERROOT will always end in
; '/' on output.
  
  if strtrim(flbval.rootpath) eq '' then begin
      flbval.rootpath = './'
  endif else if                                                   $
  strmid(flbval.rootpath, strlen(flbval.rootpath)-1,1) ne '/' then begin
      flbval.rootpath = flbval.rootpath+'/'
  endif

  if strtrim(flcval.rootpath) eq '' then begin
      flcval.rootpath = './'
  endif else if                                                   $
  strmid(flcval.rootpath, strlen(flcval.rootpath)-1,1) ne '/' then begin
      flcval.rootpath = flcval.rootpath+'/'
  endif


; We want a modal top level base widget
  
  if n_elements(parent) eq 0 then begin
     widparent = Widget_Base(Column=1, Title='ADAS 810 INPUT - EXTRA FILES', $
                             XOFFSET=200, YOFFSET=100   )
  endif else begin              
     widparent = Widget_Base(Column=1, Title='ADAS 810 INPUT - EXTRA FILES', $
                             XOFFSET=200, YOFFSET=100,                 $
                             group_leader=parent, modal=1)
  endelse


; Ask for expansion and filter files

  rc       = widget_label(widparent,value='  ',font=font_small)
  base     = widget_base(widparent,  /align_center,/column,/frame)
                

  flab  = widget_label(base, font=font_large,                  $
                       value='Expansion File Details:-')
  flbID = cw_adas_infile(base, value=flbval, font=font_small,  $
                         ysize = 5, event_funct='ADAS_PROC_NULL_EVENTS')

  flab  = widget_label(base, font=font_large,                  $
                       value='Filter File Details:-')
  flcID = cw_adas_infile(base, value=flcval, font=font_small,  $
                         ysize = 5, event_funct='ADAS_PROC_NULL_EVENTS')


; The usual buttons at the bottom
                
  mrow      = widget_base(widparent,/row,/align_center)
  messID    = widget_label(mrow,font=font_large, $
                          value='                         ')
                
  mrow      = widget_base(widparent,/row)
  
  cancelID  = widget_button(mrow,value='Cancel',font=font_small)
  doneID    = widget_button(mrow,value='Done',font=font_small)



; Initial settings
      
  widget_control, flcID, sensitive=0

;------------------------------------------------
; Realize the ADAS810 extra options input widget.

   dynlabel, widparent
   widget_Control, widparent, /realize

; Create a pointer to store the information collected from the form.
; The initial data stored here is set to CANCEL, so nothing needs to
; be done if the user kills the widget with the mouse.

   ptrToFormData = Ptr_New({cancel:1})

; Create an info structure with program information.

  info = { flbID           :  flbID,              $
           flcID           :  flcID,              $
           doneID          :  doneID,             $
           messID          :  messID,             $
           font_large      :  font_large,         $
           ptrToFormData   :  ptrToFormData       }  
               
; Store the info structure in the user value of the top-level base and launch
; the widget into the world.

  widget_control, widparent, Set_UValue=info

  XManager, 'adas810_in_opt', widparent, Event_Handler='ADAS810_IN_OPT_EVENT'


; When the widget is destroyed, the block is released, and we
; return here. Get the data that was collected by the widget
; and stored in the pointer location. Finally free the pointer.

formdata = *ptrToFormData

rep='CONTINUE'
IF N_Elements(formdata) EQ 0 THEN BEGIN
   Ptr_Free, ptrToFormData
   rep ='CANCEL'
   RETURN,rep
ENDIF
 
IF formdata.cancel EQ 1 THEN BEGIN
   Ptr_Free, ptrToFormData
   rep ='CANCEL'
ENDIF

if rep eq 'CONTINUE' then begin
   flbval = formdata.flbval
   flcval = formdata.flcval
   Ptr_Free, ptrToFormData
endif       



RETURN,rep

END
