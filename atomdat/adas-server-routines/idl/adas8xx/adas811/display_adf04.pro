; Copyright (c) 2004, Strathclyde University.
;----------------------------------------------------------------------
;+
; PROJECT    :  ADAS
;
; NAME       :  display_adf04
;
; PURPOSE    :  Displays one or more adf04 files
;
; EXPLANATION:
;       This routine launches the final ADAS811 screen with
;       the supplied files.
;
; USE:
;       An example;
;               display_adf04,'/home/adas/adas/adf04/adas#6/mom97_ls#c2.dat'
;       or
;               display_adf04,['file1.dat','file2.dat']
;
; INPUTS:
;       FILES: A single string or a vector of string containing
;              filenames (or paths and filenames) of adf04 files.
;
; OPTIONAL INPUTS:
;       None.
;
; OUTPUTS:
;       None.
;
; OPTIONAL OUTPUTS:
;       None.
;
; KEYWORD PARAMETERS:
;       None.
;
; CALLS:
;       adas_sys_set
;       adas811_proc
;
; SIDE EFFECTS:
;       A widget is created
;
; AUTHOR     :  Allan Whiteford
; 
; DATE       :  30-11-04
; 
;
; MODIFIED:
;       1.1     Allan Whiteford
;              	- First version.
;
; VERSION:
;       1.1    30-11-04
;-
;----------------------------------------------------------------------

pro display_adf04,files

	adas_sys_set, adasrel, fortdir, userroot, centroot, $
		      devlist, devcode, font_large, font_small, edit_fonts

	adasprog="Display ADF04 file";
        
	deffile  = userroot+'/defaults/adas811_defaults.dat'
	bitfile  = centroot+'/bitmaps'

	if (size(files))[0] eq 0 then begin
        	file1=files
                file2=''
        endif else begin
		file2=strarr((size(files))[1]-1)
        	file1=files[0]
                for i=1,(size(files))[1]-1 do begin
                	file2[i-1]=files[i]
                end
        endelse
                
        rep=adas811_proc(file1, file2, bitfile, devlist,             $
                     devcode, adasrel, adasprog,                     $
                     FONT_LARGE=font_large, FONT_SMALL = font_small, $
                     /standalone)

end
