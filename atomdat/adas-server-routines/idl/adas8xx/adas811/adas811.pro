; Copyright (c) 2002 Strathclyde University .
; SCCS info: Module @(#)$Header: /home/adascvs/idl/adas8xx/adas811/adas811.pro,v 1.2 2004/07/06 11:07:52 whitefor Exp $ Date $Date: 2004/07/06 11:07:52 $
;+
; PROJECT:
;       ADAS
;
; NAME:
;	ADAS811
;
; PURPOSE:
;	Compare up to 3 (at the moment) adf04 files.
;
; EXPLANATION: Steps through the various types of data in type 3
;              adf04 files showing a comparison between the data.
;              There is no user supplied temperatures and the only
;              hardcopy output is graphical.
;
; USE:
;	First the system settings must be established by calling
;	adas_sys_set.pro then adas811.pro is called.
;
;	adas_sys_set, adasrel, fortdir, userroot, centroot, $
;		      devlist, devcode, font_large, font_small, edit_fonts
;	adas811,   adasrel, fortdir, userroot, centroot, devlist, $
;		   devcode, font_large, font_small, edit_fonts
;
; INPUTS:
;	ADASREL - A string indicating the ADAS system version, 
;		  e.g ' ADAS RELEASE: ADAS93 V1.4'.  The first
;		  character should be a space.
;
;	FORTDIR - Not applicable here.
;
;	USERROOT - A string holding the path to the root directory of
;		   the user's adas data. e.g '/disk/bowen/adas'
;		   This root directory will be used by adas to construct
;		   other path names.  For example the users default data
;		   for adas205 should be in /disk/bowen/adas/adf04.  In
;		   particular the user's default interface settings will
;		   be stored in the directory USERROOT+'/defaults'.  An
;		   error will occur if the defaults directory does not
;		   exist.
;
;	CENTROOT - Like USERROOT, but this directory points to the
;		   central data area for the system.  User defaults are
;		   not stored on CENTROOT.
;
;	DEVLIST - A string array of hardcopy device names, used for
;		  graphical output. e.g ['Post-Script','HP-PCL','HP-GL']
;		  This array must mirror DEVCODE.  DEVCODE holds the
;		  actual device names used in a SET_PLOT statement.
;
;	DEVCODE - A string array of hardcopy device code names used in
;		  the SET_PLOT statement, e.g ['PS', 'PCL', 'HP']
;		  This array must mirror DEVLIST.
;
;	FONT_LARGE - The name of a larger font e.g 'courier_bold14'
;
;	FONT_SMALL - The name of a smaller font e.g 'courier_bold12'
;
;	EDIT_FONTS - A structure of two fonts used in the table editor
;		     adas_edtab.pro, {font_norm:'',font_input:''}.
;		     The two fonts are used to differentiate between
;		     editable and non-editable parts of the table. You
;		     may have to experiment to get two fonts which give
;		     the same default spacing in the edit widget.
;		     e.g {font_norm:'helvetica_bold14', $
;			  font_input:'helvetica_oblique14'}
;
; OPTIONAL INPUTS:
;	None
;
; OUTPUTS:
;	None - Note this routine should leave all inputs un-modified.
;
; OPTIONAL OUTPUTS:
;	None
;
; KEYWORD PARAMETERS:
;	None
;
; CALLS:
;
; SIDE EFFECTS:
;
; CATEGORY:
;	Adas system.
;	
; WRITTEN:
;       Martin O'Mullane
;
; MODIFIED:
;	1.1	Martin O'Mullane
;		First release
;	1.2	Richard Martin
;		Increased version no. to 1.2 .
;
; VERSION:
;	1.1	31-01-2002
;	1.2	17-03-2003
; 
;-
;-----------------------------------------------------------------------------

PRO adas811,	adasrel, fortdir, userroot, centroot, 			$
		devlist, devcode, font_large, font_small, edit_fonts


; Initialisation and IDL v5 check

    adasprog = ' PROGRAM: ADAS811 v1.2'
    deffile  = userroot+'/defaults/adas811_defaults.dat'
    bitfile  = centroot+'/bitmaps'
    device   = ''

    thisRelease = StrMid(!Version.Release, 0, 1)
    IF thisRelease LT '5' THEN BEGIN
       message = 'Sorry, ADAS811 requires IDL 5 or higher' 
       tmp = popup(message=message, buttons=['Accept'],font=font_large)
       goto, LABELEND
    ENDIF


; Restore or set the defaults

    files = findfile(deffile)
    if files(0) eq deffile then begin
        
        restore, deffile
        inval.centroot = centroot+'/adf04/'
        inval.userroot = userroot+'/adf04/'
    
    endif else begin
	inval =	{ ROOTPATH   :   userroot+'/adf04/',    $
		  FILE       :   '',                    $
		  CENTROOT   :   centroot+'/adf04/',    $
		  USERROOT   :   userroot+'/adf04/',    $
                  NFILES     :   2,                     $
                  CFILES     :   strarr(2)              }
  
        procval = {NMET:-1}
    
    end


; Ask for (up to 3) input adf04 files

LABEL100:

    rep = adas811_in(inval, FONT_LARGE=font_large, FONT_SMALL = font_small)
 
    if rep eq 'CANCEL' then goto, LABELEND


; Display comparisons

LABEL200:

    adas_file  = inval.rootpath + inval.file
    comp_files = inval.cfiles 
    
    rep=adas811_proc(adas_file, comp_files, bitfile, devlist,       $
                     devcode, adasrel, adasprog,                    $
                     FONT_LARGE=font_large, FONT_SMALL = font_small)
   
    
    if rep eq 'MENU'   then goto, LABELEND
    if rep eq 'CANCEL' then goto, LABEL100


LABELEND:


                ;**** Save user defaults ****

    save, inval,  procval, filename=deffile


END
