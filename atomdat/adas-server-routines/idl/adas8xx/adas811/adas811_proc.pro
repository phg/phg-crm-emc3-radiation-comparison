; Copyright (c) 2002 Strathclyde University .
; SCCS info: Module @(#)$Header: /home/adascvs/idl/adas8xx/adas811/adas811_proc.pro,v 1.5 2010/12/06 19:07:47 mog Exp $ Date $Date: 2010/12/06 19:07:47 $
;+
; PROJECT: ADAS 
;
; NAME: ADAS811_PROC
;
; PURPOSE: This procedure plot the comparison one transition at a time.
;          The user can interact with it to advance through these or to
;          select a particular transition. Various options for plotting
;          are available; excitation, ionisation and recombination rates.
;          For excitation effective collison strengths, rates or Burgess-
;          Tully plots are possible. Printed output of the graphical
;          screen is via a popup widget. 
;
;
;
; NOTES: IDL only and uses v5 (and above) features such as pointers 
;        to pass data around the program.
;
;        The user can only step through the transitions of one, primary,
;        adf04 file. Levels in the other files are mapped to the primary.
;        However the peimary can be switched between any of the files
;        via an exclusive button.
;
;        The level matching is not as robust as it could be. This should
;        be replaced by Hugh's new configuration identification routines.
;        The adf04 file must have either Eissner or (strict) standard
;        configuration strings.
;
;        Proton excitation and Cx are not implemented yet.
;
;        We assume the parents are the same - these are not checked.
;
;        If no explicit ionisation ECIP approximation is used.
; 
;
;
; INPUTS:
;         adas811_proc, adas_file, comp_files,                        $  
;                       bitfile, devlist, devcode, adasrel, adasprog, $
;                       FONT_LARGE = font_large,                      $
;                       FONT_SMALL = font_small
;
;          adas_file  - initial primary file from standard ADAS 
;                       selection widget
;          comp_files - other adf04 files
;          bitfile    - location of bitmaps
;          devlist    - printer devices
;          devcode    - 
;          adasrel    - ADAS release
;          adasprog   - ADAS program
;          standalone - Code is being run in standalone mode so should
;                       not display a menu button.
;          cwid       - The code should create a self managing
;                       compound-widget with the parent as given in
;                       the value of cwid.
;
;
; OPTIONAL INPUTS:
;       None
;
; OUTPUTS:
;       The function returns a string rep
;           rep = 'MENU'   to leave ADAS811
;           rep = 'CANCEL' user want to go back a screen or the user
;                          closes the widget with the mouse. 
;
; OPTIONAL OUTPUTS:
;       None
;
; KEYWORD PARAMETERS:
;       FONT_LARGE - The name of a larger font e.g 'courier_bold14'
;       FONT_SMALL - The name of a smaller font e.g 'courier_bold12'
;
;
; CALLS:
;                            
;       ADAS811_CONVERT           : For e-excitation converts between gamma,
;                                   rate and Burgess-Tully.
;       ADAS811_CFGSTR            : Generates the transition label.
;       ADAS811_PLOT              : Plots and annotates the comparison.
;       ADAS811_MAKE_LIST         : Makes a list of configuration + quantum
;                                   numbers + energy for level matching.
;       ADAS811_NEWMAP            : Generates a map between primary file
;                                   and the others.
;       ADAS811_TRANSITION        : Generates 'tran of ntran' for each type.
;       ADAS811_ADVANCE           : Called by event handler to advance/retract
;                                   along the possible transitions.
;       ADAS811_PROC_NULL_EVENTS  : Black hole for events which we don't use.
;       ADAS811_PROC_MENU         : Event handler for menu (bitmap) actions.
;       ADAS811_PROC_EVENT        : Event handler for most actions.
;       ADAS811_FIRST_TRANSITION  : Generate first and number of transitions
;                                   for each type.
;
;
; SIDE EFFECTS:
;
;       Calls routines from idl/read_adf/ and fortran routines from
;       the wrapper series. 
;
; CATEGORY:
;	Widgets
;
; WRITTEN:
;       Martin O'Mullane
;
; MODIFIED:
;	1.1	Martin O'Mullane
;		 - First release 
;	1.2	Martin O'Mullane
;		 - Add a 'Print All' button to print all (of a particular tye)
;                  transitions to a file.
;                - Add a field showing the data value under the cursor.
;       1.3     Martin O'Mullane
;                - Arguments to read_adf04 have changed.
;       1.4     Allan Whiteford
;                - Added /standalone argument
;                - Added /cwid argument
;                - Increased format of code of transition to I5
;                - Fixed where/zero indexing bug which showed up in < IDL 5.5
;       1.5     Martin O'Mullane
;                - Work around xxcftr adding a 5 to the first shell when 
;                  converting to Eissner style.
;
; VERSION:
;       1.1	08-04-2002
;       1.2	04-09-2002
;       1.3     16-11-2004
;       1.4     16-11-2004
;       1.5     06-12-2010
;
;-
;-----------------------------------------------------------------------------


PRO adas811_convert, exc_type, burgess, file, index, lower, upper, $
                     list, x, y, tcode
                     
                     
; Set tcode to -1 if not a Burgess-Tully plot                     

   tcode = -1

; Fill in this part of the multi-dimensional x array

   list = indgen(n_elements(file.te))

; Default is to return effective collision strengths 'gamma'

   x = file.te       
   y = reform(file.gamma[index,*])


; Rates - but limit to values above 1.0e-30

   if exc_type EQ 'rate' then begin

      w_l    = 2.0*file.xja[lower-1]+1.0 ; IDL indexing
      e_l    = file.wa[lower-1]
      e_u    = file.wa[upper-1]
      deltaE = (e_u - e_l)/8065.541

      y = 2.1716e-8 * sqrt(157894.0/ x) *           $
                  y * exp(-11605.0*deltaE/x) / w_l

      y = y > 1.0e-30

   endif


; Burgess-Tully C-plot
   
   if exc_type EQ 'C-plot' then begin

        fzero = 1.0E-4
        fbig  = 0.01   

        wvnou = file.wa[upper-1]
        wvnol = file.wa[lower-1]
        elu   = abs(wvnou-wvnol)/109737.26
        wtu   = 2.0*file.xja[upper-1]+1.0
        wtl   = 2.0*file.xja[lower-1]+1.0
        ain   = file.aval[index]
        s     = 3.73491e-10*wtu*ain/(elu^3.0)                        
        fin   = 3.333333e-1*elu*s/wtl                                     

        if file.isa[upper-1] EQ file.isa[lower-1] then begin

           if (abs(file.ila[upper-1]-file.ila[lower-1]) LE 1) AND $
              (fin GT fbig) then tcode = 1 $
                            else tcode = 2

        endif else begin

            if (fin GT fzero) AND (fin LT fbig) then tcode = 4 $
                                                else tcode = 3  
        endelse

        et = 1.43884*(wvnou-wvnol)/x   ; dimensionless energy cm-1, Te in K
        c  = burgess
        
        CASE tcode OF

          1 : Begin
                x_burgess = 1.0 - alog(c) / alog(1.0/et + c)
                y_burgess = y / alog(1.0/et + 2.718281828)
                x_burgess = [x_burgess, 1.0]
                y_burgess = [y_burgess, 4.0*wtl*fin/elu]
                list = [list, n_elements(list)]
              End
          2 : Begin
                x_burgess = (1.0/et) / (1.0/et + c)
                y_burgess = y
              End
          3 : Begin
                x_burgess = (1.0/et) / (1.0/et + c)
                y_burgess = y * (1.0/et + 1.0)
              End
          4 : Begin
                x_burgess = 1.0 - alog(c) / alog(1.0/et + c)
                y_burgess = y / alog(1.0/et + c)
                x_burgess = [x_burgess, 1.0]
                y_burgess = [y_burgess, 4.0*wtl*fin/elu]
                list = [list, n_elements(list)]
              End

        ENDCASE
        
        x = x_burgess
        y = y_burgess


   endif



END
;------------------------------------------------------------------------

PRO adas811_cfgstr, type, file, is_main, lower, upper, index, cfgstr

case type of
  
  'e-exc'  : begin
               str1 = '                  '
               strput, str1,file.cstrga[lower-1],0 
               str1 = str1 + '(' +                                      $
                      string(file.isa[lower-1], format='(i1)') + ')' +  $
                      string(file.ila[lower-1], format='(i1)') + '(' +  $
                      string(file.xja[lower-1], format='(f4.1)') + ')'

               str2 = '                  '
               strput, str2,file.cstrga[upper-1],0 
               str2 = str2 + '(' +                                      $
                      string(file.isa[upper-1], format='(i1)') + ')' +  $
                      string(file.ila[upper-1], format='(i1)') + '(' +  $
                      string(file.xja[upper-1], format='(f4.1)') + ')'

               str3 = string(file.aval[index], format='(e8.2)')
               str4 = '(' + string(file.wa[upper-1], format='(f12.1)') + ' - ' + $
                            string(file.wa[lower-1], format='(f12.1)') + ')'

               cfgstr = str1 + '  -  ' + str2 + ' : ' + str3 + 's!u-1!n, ' + str4
             end

  'rec.'  : begin
               str1 = '                  '
               strput, str1,file.cstrga[upper-1],0 
               str1 = str1 + '(' +                                      $
                      string(file.isa[upper-1], format='(i1)') + ')' +  $
                      string(file.ila[upper-1], format='(i1)') + '(' +  $
                      string(file.xja[upper-1], format='(f4.1)') + ')'
               str2 = ' - parent (' + string(lower, format='(i1)') + ')'
               str3 = '(' + string(file.wa[upper-1], format='(f12.1)') + ')'
               
               cfgstr = str1 + str2 + ' : ' + str3
            
            end
             
  'ionis.': begin
               str1 = '                  '
               strput, str1,file.cstrga[upper-1],0 
               str1 = str1 + '(' +                                      $
                      string(file.isa[upper-1], format='(i1)') + ')' +  $
                      string(file.ila[upper-1], format='(i1)') + '(' +  $
                      string(file.xja[upper-1], format='(f4.1)') + ')'
               str2 = ' - parent (' + string(lower, format='(i1)') + ')'
               str3 = '(' + string(file.wa[upper-1], format='(f12.1)') + ')'
               
               cfgstr = str1 + str2 + ' : ' + str3
            
            end
             
  else    : cfgstr = 'There are problems in getting the configurations'
              
endcase


; Check for principal file

if is_main then str0 = '* ' else str0 = '  '
cfgstr = str0 + strcompress(cfgstr)

END
;------------------------------------------------------------------------




PRO ADAS811_plot, ifile, lower, upper, type, exc_type, burgess, $
                  data, map,                                    $
                  print = print, info_str=info_str, te=te,      $
                  p_first = p_first, p_last = p_last

; note that data and map are pointers

; default to K

if n_elements(te) EQ 0 then te = 'K'

; Screen or file?

if (keyword_set(print)) then begin
  
  grtype = print.devcode( where(print.dev eq print.devlist) )
  if print.paper eq 'A4' then begin
     xsize = 24.0
     ysize = 18.0
  endif else begin
     xsize = 23.0
     ysize = 17.0
  endelse
  
  open_file = 1
  if n_elements(p_first) GT 0 then if p_first then open_file = 1 else open_file = 0
  
  if open_file then begin
     set_plot, grtype

     !p.font=0
     device,/color,bits=8,filename=print.dsn,           $
            /Helvetica, font_size=12, /landscape,       $
            xsize=xsize, ysize=ysize
  endif
  
endif 


; Make plot data containers

n_files    = n_elements(data)
x          = dblarr(n_files, 15)
y          = dblarr(n_files, 15)
cfg_str    = strarr(n_files)
file_names = strarr(n_files)



; make master list - corresponds to ifile selection

adf04       = *data[ifile]
master_map  = *map[ifile]

case type of

  'e-exc' : begin

               ind_master = where(adf04.upper EQ upper AND $
                                  adf04.lower EQ lower, count)
               if count EQ 0 then begin
                  erase
                  xyouts, 0.2,0.5, 'Transition does not exist', /normal
                  return
               endif else begin
                  adas811_convert, exc_type, burgess, adf04,       $
                                   ind_master[0], lower, upper, list, $
                                   x_conv, y_conv, tcode
                  x[0, list] = x_conv
                  y[0, list] = y_conv
                  
                  is_main = 1
                  adas811_cfgstr, type, adf04, is_main, lower, upper, $
                                  ind_master[0], cfgstr
               
                  cfg_str[0]    = cfgstr
                  file_names[0] = adf04.filename
               endelse
              
            end

  'rec.' : begin

               ind_master = where(adf04.level_rec EQ upper AND $
                                  adf04.parent_rec EQ lower, count)
               if count EQ 0 then begin
                  erase
                  xyouts, 0.2,0.5, 'Transition does not exist', /normal
                  return
               endif else begin

                  tcode = -1
                  list  = indgen(n_elements(adf04.te))

                  x[0, list] = adf04.te       
                  y[0, list] = reform(adf04.rec[ind_master,*])
                  y[0, list] = y[0, list] > 1.0e-30

                  is_main = 1
                  adas811_cfgstr, type, adf04, is_main, lower, upper, $
                                  ind_master[0], cfgstr
               
                  cfg_str[0] = cfgstr
                  file_names[0] = adf04.filename
                  
               endelse
              
            end
            
  'ionis.' : begin

               ind_master = where(adf04.level_ion_rate EQ upper AND $
                                  adf04.parent_ion_rate EQ lower, count)
               if count EQ 0 then begin
                  erase
                  xyouts, 0.2,0.5, 'Transition does not exist', /normal
                  return
               endif else begin

                  tcode = -1
                  list  = indgen(n_elements(adf04.te))

                  x[0, list] = adf04.te       
                  y[0, list] = reform(adf04.ion_rate[ind_master,*])
                  y[0, list] = y[0, list] > 1.0e-30

                  is_main = 1
                  adas811_cfgstr, type, adf04, is_main, lower, upper, $
                                  ind_master[0], cfgstr
               
                  cfg_str[0] = cfgstr + ' ' + adf04.ion_source[ind_master]
                  file_names[0] = adf04.filename
                  
               endelse
              
            end
            
   else : print,'Do nothing at master level'

endcase



; now cycle through the remaining files matching the energy levels

index = where(indgen(n_files) NE ifile, c_files)

for i = 0, c_files-1 do begin

  k = index[i]

  file = *data[k]
  mmap = *map[k]
  
  is_main = 0

  case type of

    'e-exc' : begin
                 
                 res_u = where(mmap EQ upper, c_u)
                 res_l = where(mmap EQ lower, c_l)
  
                 if c_u GT 0 AND c_l GT 0 then begin
                 
                    search_u = res_u[0]+1
                    search_l = res_l[0]+1
                    if search_l GT search_u then begin
                       tmp      = search_u
                       search_u = search_l
                       search_l = tmp
                    endif
                    ind_minor = where(file.upper EQ search_u AND $
                                      file.lower EQ search_l, count)
                    if count GT 0 then begin
                       
                       adas811_convert, exc_type, burgess, file,       $
                                        ind_minor[0], search_l, search_u, list, $
                                        x_conv, y_conv, tcode
                       x[i+1, list] = x_conv
                       y[i+1, list] = y_conv
                       
                       adas811_cfgstr, type, file, is_main, search_l, search_u, $
                                       ind_minor[0], cfgstr

                       cfg_str[i+1]    = cfgstr
                       file_names[i+1] = file.filename
                       
                    endif
                 
                 
                 endif

              end

  'rec.' : begin
                
                ; assume parents are same
               
                res_u = where(mmap EQ upper, c_u)

                if c_u GT 0 then begin
                 
                  search_u = res_u[0]+1
                  search_l = lower

                  ind_minor = where(file.level_rec EQ search_u AND $
                                    file.parent_rec EQ search_l, count)
                  
                  if count GT 0 then begin

                     tcode = -1
                     list  = indgen(n_elements(file.te))

                     x[i+1, list] = file.te       
                     y[i+1, list] = reform(file.rec[ind_minor,*])
                     y[i+1, list] = y[i+1, list] > 1.0e-30

                     adas811_cfgstr, type, file, is_main, lower, search_u, $
                                     ind_minor[0], cfgstr

                     cfg_str[i+1]    = cfgstr
                     file_names[i+1] = file.filename
                  
                  endif
              
               endif
              
            end
            
  'ionis.' : begin
                
                ; assume parents are same
               
                res_u = where(mmap EQ upper, c_u)

                if c_u GT 0 then begin
                 
                  search_u = res_u[0]+1
                  search_l = lower

                  ind_minor = where(file.level_ion_rate EQ search_u AND $
                                    file.parent_ion_rate EQ search_l, count)
                  
                  if count GT 0 then begin

                     tcode = -1
                     list  = indgen(n_elements(file.te))

                     x[i+1, list] = file.te       
                     y[i+1, list] = reform(file.ion_rate[ind_minor,*])
                     y[i+1, list] = y[i+1, list] > 1.0e-30

                     adas811_cfgstr, type, file, is_main, lower, search_u, $
                                     ind_minor[0], cfgstr

                     cfg_str[i+1]    = cfgstr + ' ' + $
                                       file.ion_source[ind_minor]
                     file_names[i+1] = file.filename
                  
                  endif
              
               endif
              
            end
            
    else   : print,'Do nothing at minor level'

  endcase


endfor


; Now plot the data
;   - label the axes
;   - determine limits of axes

ind  = where(x NE 0.0)
xmin = min(x[ind], max=xmax)
ymin = min(y[ind], max=ymax)

pos  = [0.15,0.2,0.95,0.90]


; Scale x depending on units chosen

xtitle = 'Electron Temperature (K)'
factor = 1.0
if te EQ 'eV' AND exc_type NE 'C-plot' then begin
   xtitle = 'Electron Temperature (eV)'
   factor = 11605.0
   xmin   = xmin/factor
   xmax   = xmax/factor
endif


case type of

  'e-exc'  : begin

               case exc_type of

                 'gamma' : begin
                             title  = 'Effective Collision Strengths'
                             ytitle = 'gamma'
                             plot_oo, [xmin, xmax], [ymin, ymax], /nodata, $
                                      position = pos,                      $
                                      xtitle = xtitle, ytitle=ytitle,      $
                                      title=title
                           end

                 'rate'  : begin
                             title  = 'Excitation rate'
                             ytitle = 'q!dij!n (cm!u3!n s!u-1!n)'
                             plot_oo, [xmin, xmax], [ymin, ymax], /nodata,  $
                                      position = pos,                       $
                                      xtitle = xtitle, ytitle=ytitle,       $
                                      title=title
                           end

                 'C-plot': begin
                             xtitle = 'x'
                             ytitle = 'y'
                             title  = 'Type of transition : ' +  $
                                       string(tcode, format='(i2)')
                             plot_io, [0.9*xmin, 1.1*xmax], [ymin, ymax], /nodata, $ 
                                      xstyle=1,                                    $
                                      position = pos,                              $
                                      xtitle = xtitle, ytitle=ytitle, title=title
                           end

               endcase

             end
  
  'rec.'   : begin
                title  = 'Recombination Rate'
                ytitle = 'q!dij!n!ur!n (cm!u3!n s!u-1!n)'
                plot_oo, [xmin, xmax], [ymin, ymax], /nodata, $
                         position = pos,                      $
                         xtitle = xtitle, ytitle=ytitle,      $
                         title=title
             end
             
  'ionis.'   : begin
                title  = 'Ionisation Rate from levels'
                ytitle = 'q!dij!n!ui!n (cm!u3!n s!u-1!n)'
                plot_oo, [xmin, xmax], [ymin, ymax], /nodata, $
                         position = pos,                      $
                         xtitle = xtitle, ytitle=ytitle,      $
                         title=title
             end
             
  else     : print, 'Cannot draw axes'
  
endcase


; Do the plotting and put up a caption underneath

step = 0.15/3   ; 3 is number of files!

for j = 0, n_files-1 do begin
  ind = where(x[j,*] NE 0.0, count)
  if count GT 1 then begin
  
     if exc_type EQ 'C-plot' AND x[j,count-1] EQ 1.0 then begin
        ind = ind[0:count-2]
        oplot, [x[j,count-2],1.0], [y[j,count-2], y[j,count-1]], $
               psym=-(4+j), linestyle = j
     endif
     oplot, x[j,ind]/factor, y[j, ind], linestyle = j
     
     ypos = 0.5*(0.2-(j+1)*step)
     res = convert_coord([0.01, 0.06], [ypos, ypos], /normal)
     
     x_res = reform(res[0,*])
     y_res = reform(res[1,*])
     oplot, x_res/factor, y_res, linestyle = j, /noclip

     if keyword_set(print) then out_str = cfg_str[j] + '  [' + file_names[j] + ']'$
                           else out_str = cfg_str[j]
     
     xyouts, res[0,1], res[1,1], out_str, charsize=0.7
 
  endif
endfor 

; Diagnostic printout of x and y

; for j=0, n_files-1 do begin
;   print, 'File...', j
;   for i=0, 14 do print, x[j,i], y[j,i]
;   print, '---'
; endfor



; If output is going to a file add ADAS release info and tidy up
; For screen print file identification at top.
 
if keyword_set(print) then begin

  close_file = 1
  if n_elements(p_last) GT 0 then if p_last then close_file = 1 else close_file = 0
  
  xyouts, 0.01, 0.98, info_str, charsize=0.7, /normal
  
  user_name  = xxuser()
  print_time = xxdate()
  
  out_str = 'Printed by : ' + user_name + ' at ' + print_time[1] + ' on ' + $
            print_time[0]
  
  xyouts, 0.7, 0.98, out_str, charsize=0.7, /normal
  
  if close_file then begin
     device,/close
     set_plot,'X'
     !p.font=-1
  endif

endif else begin

   csize   = 0.55
   out_str = ''
   out_pos = fltarr(n_files)
   for j = 0, n_files-1 do begin
      out_str = out_str + file_names[j] + '  '
      xyouts, 0.01,1, out_str, width=w,charsize=-1, /normal
      out_pos[j] = w
   endfor 
   
   xyouts, 0.01, 0.985, out_str, /normal, charsize=csize
   
   ypos    = 0.97
   out_pos = 0.01+[0.0, out_pos]*csize
   for j = 0, n_files-1 do begin
      xpos = out_pos[j]
      res = convert_coord([xpos,xpos+0.06], [ypos, ypos], /normal)
      x_res = reform(res[0,*])
      y_res = reform(res[1,*])
      oplot, x_res, y_res, linestyle = j, /noclip
   endfor    

endelse


END
;------------------------------------------------------------------------





PRO adas811_make_list, cfg, isa, ila, xja, energy, list

; Here we assume that configurations are either in standard (eg 1S1 2P2)
; or in Eissner form. Convert the standard to Eissner in order to
; make the comparisons.
;
; Note that the H-like GCR files have n=1 etc as their configuration.
; We treat this as a special case.


n_levels = n_elements(cfg)
list     = strarr(n_levels)

if strmid(cfg[0],0,2) EQ 'n=' then begin

   for j = 0, n_levels-1 do begin
      tmp = cfg[j] + string(isa[j]) + string(ila[j]) + string(xja[j])
      list[j] = strcompress(tmp)
   endfor
   
endif else begin

   xxdtes, in_cfg = cfg[0], is_eissner=is_eissner

   for j = 0, n_levels-1 do begin

     if NOT is_eissner then begin
        xxcftr, in_cfg = cfg[j], out_cfg = tmpcfg, type=2
        tmpcfg = strmid(tmpcfg, 1)
     endif else tmpcfg = cfg[j]
     
     tmp = tmpcfg + string(isa[j]) + string(ila[j]) + string(xja[j])
     list[j] = strcompress(tmp)

   endfor

endelse

END
;------------------------------------------------------------------------


PRO adas811_newmap, ifile, data, map

; note that data and map are pointers


n_files = n_elements(data)


; make master list - corresponds to ifile selection

adf04 = *data[ifile]

adas811_make_list, adf04.cstrga, adf04.isa, adf04.ila, adf04.xja, adf04.wa, $
                   master_list
master_eng  = adf04.wa
master_map  = adf04.ia
*map[ifile] = adf04.ia


; now cycle through the remaining files matching the energy levels

index = where(indgen(n_files) NE ifile, c_files)

for i = 0, c_files-1 do begin

  k = index[i]

  file = *data[k]

  adas811_make_list, file.cstrga, file.isa, file.ila, file.xja, file.wa, $
                     list
  eng  = file.wa

  new_map = intarr(n_elements(file.ia))
  
  for j = 0, n_elements(file.ia)-1 do begin
  
    loc = where(master_list EQ list[j], count)
    
    case count of 
      
         0 : new_map[j] = -1
         1 : new_map[j] = master_map[loc[0]]
      else : begin
                e_master = master_eng[loc]
                e_search = eng[j]
                
                if e_search LT 0.85*e_master[0] OR     $
                   e_search GT 1.15*e_master[count-1]  then begin
                      new_map[j] = -1
                endif else begin
                   ik = i4indf(e_master, e_search)
                       new_map[j] = master_map[loc[ik]]
                endelse
                   
             end
    
    endcase
   
  endfor
  
  *map[k] = new_map

endfor

END
;------------------------------------------------------------------------



PRO adas811_transition, type, file, lower, upper, updateID

case type of

  'e-exc'  : begin
                 itran = where(file.upper EQ upper AND $
                               file.lower EQ lower, count)
                 itran = itran[0] + 1              
                 
                 ntran = string(n_elements(file.upper), format='(i5)') 

                 if count GT 0 then mess = string(itran, format='(i5)') + $
                                           ' of ' + ntran                 $
                               else mess = 'n/a of ' + ntran
             end

  'rec.'   : begin
                 itran = where(file.level_rec EQ upper AND $
                               file.parent_rec EQ lower, count)
                 itran = itran[0] + 1              
                 
                 if file.level_rec[0] EQ -1 then begin
                    ntran_str = 'n/a' 
                 endif else begin             
                    ntran_str = string(n_elements(file.level_rec), format='(i5)') 
                 endelse

                 if count GT 0 then mess = string(itran, format='(i5)') + $
                                           ' of ' + ntran_str             $
                               else mess = 'n/a of ' + ntran_str
             end

  'ionis.' : begin
                 itran = where(file.level_ion_rate EQ upper AND $
                               file.parent_ion_rate EQ lower, count)
                 itran = itran[0] + 1              
                 
                 if file.level_ion_rate[0] EQ -1 then begin
                    ntran_str = 'n/a' 
                 endif else begin             
                    ntran_str = string(n_elements(file.level_ion_rate), format='(i5)') 
                 endelse

                 if count GT 0 then mess = string(itran, format='(i5)') + $
                                           ' of ' + ntran_str             $
                               else mess = 'n/a of ' + ntran_str
             end

   else : print, 'Not currently available'

endcase


widget_control, updateID, set_value = mess


END
;-----------------------------------------------------------------------------



PRO adas811_advance, step, info
  
  
; Assume positive step move up and negative steps down (contrary to shift!)
  
  file  = *info.ptrToData[info.ifile]

  case info.type of
  
    'e-exc' : begin 
                ntran = n_elements(file.upper)

                ; current setting

                itran = where(file.upper EQ info.upper AND $
                              file.lower EQ info.lower, count)
                itran = itran[0]

                ; advance or decrease itran and wrap if we get 
                ; to the end or start

                index     = indgen(ntran)
                itran_new = shift(index, -1*step)
                itran_new = itran_new[itran]
                upper     = file.upper[itran_new]
                lower     = file.lower[itran_new]
              end
              
    'rec.' : begin 
                ntran = n_elements(file.level_rec)

                ; current setting

                itran = where(file.level_rec EQ info.upper AND      $
                              file.parent_rec EQ info.lower, count)
                itran = itran[0]

                ; advance or decrease itran and wrap if we get 
                ; to the end or start

                index     = indgen(ntran)
                itran_new = shift(index, -1*step)
                itran_new = itran_new[itran]
                upper     = file.level_rec[itran_new]
                lower     = file.parent_rec[itran_new]
              end
              
    'ionis.' : begin 
                ntran = n_elements(file.level_ion_rate)

                ; current setting

                itran = where(file.level_ion_rate EQ info.upper AND      $
                              file.parent_ion_rate EQ info.lower, count)
                itran = itran[0]

                ; advance or decrease itran and wrap if we get 
                ; to the end or start

                index     = indgen(ntran)
                itran_new = shift(index, -1*step)
                itran_new = itran_new[itran]
                upper     = file.level_ion_rate[itran_new]
                lower     = file.parent_ion_rate[itran_new]
              end
              
     else : print, 'Problem in advancing/retreating'
            
  endcase
  
  
  mess = string(itran_new+1, format='(i5)') + ' of ' + $
         string(ntran, format='(i5)') 
  
  ; update all dependant info widgets and plot new transitions
  
  widget_control, info.panelID, set_value = mess
  widget_control, info.t1ID, set_value = upper
  widget_control, info.t2ID, set_value = lower
  
  adas811_plot, info.ifile, lower, upper, info.type, info.exc_type, $
                info.burgess, info.ptrToData, info.ptrToMap, te=info.units
  
  ; Store new transition indices
  
  info.upper = upper
  info.lower = lower
              
END
;-----------------------------------------------------------------------------


FUNCTION ADAS811_PROC_NULL_EVENTS, event

   ; The purpose of this event handler is to do nothing
   ; and ignore all events that come to it.
      
   RETURN, 0

END 

;-----------------------------------------------------------------------------

PRO ADAS811_PROC_CURSOR, event

; Put all mouse movement events in here

; Get the info structure out of the user value of the top-level base.

  Widget_Control, event.id, Get_UValue=handlerid
  Widget_Control, handlerid, Get_UValue=info

  xpos = event.x
  ypos = event.y
  
  res = convert_coord(xpos, ypos, /device)

  out_str = '(' +  string(res[0], format='(g10.4)') + ', ' + $
                   string(res[1], format='(g10.4)') +')'
  out_str = strtrim(out_str)
  out_str = strcompress(out_str)
                   
  widget_control, info.valueID, set_value=out_str

END
;-----------------------------------------------------------------------------

PRO ADAS811_PROC_MENU, event

; The 'normal' get_value from a widget_button does not work with a
; bitmap button. Hence this specialised event handler.

; Get the info structure out of the user value of the top-level base.

Widget_Control, event.top, Get_UValue=info


 formdata = {cancel : 0, menu:1}
 *info.ptrToFormData = formdata
 widget_Control, event.top, /destroy

END

;-----------------------------------------------------------------------------


PRO ADAS811_PROC_EVENT, event

; Get the info structure out of the user value of the top-level base.

Widget_Control, event.handler, Get_UValue=info


; Clear any messages 

widget_control, info.messID, set_value = ' '


; Process events

CASE event.id OF

  info.cancelid : begin
                    formdata = {cancel:1}
                    *info.ptrToFormData = formdata
                    widget_Control, event.top, /destroy
                  end 

      
  info.printid : begin
                 
                    print_val = { dsn     : '',                  $
                                  defname : 'adas811_plot.ps',   $
                                  replace :  1,                  $
                                  devlist :  info.devlist,       $
                                  dev     :  info.devlist[0],    $
                                  paper   :  'A4',               $
                                  write   :  0,                  $
                                  message :  'Note: cannot append to graphics files'}
                    
                    act = 0
                    adas_file_gr, print_val, act, font=info.font, $
                                  title='File name for graphical output'

                    print_stuff = { dsn     : print_val.dsn,   $
                                    devcode : info.devcode,    $
                                    devlist : info.devlist,    $
                                    dev     : print_val.dev,   $
                                    paper   : print_val.paper  }

                    if print_val.write EQ 1 then begin               

                       info_str = info.adasrel + ' ' + info.adasprog
                       adas811_plot, info.ifile, info.lower, info.upper,      $
                                     info.type, info.exc_type, info.burgess,  $
                                     info.ptrToData, info.ptrToMap,           $
                                     print = print_stuff, info_str = info_str,$
                                     te=info.units 

                       widget_control, info.messID, $
                                       set_value = 'Plot written to file'

                    endif else begin

                       widget_control, info.messID,$
                                       set_value = 'No file selected'

                    endelse

                 end
             

      
  info.printallID : begin
                 
                 
                     file  = *info.ptrToData[info.ifile]
                     type  = info.type

                     case type of

                        'e-exc'  : begin
                                      lower    = file.lower
                                      upper    = file.upper
                                      num_tran = n_elements(file.aval)
                                   end

                        'p-exc'  : begin
                                      lower    = -1
                                      upper    = -1
                                      num_tran = 0
                                   end

                        'rec.'   : begin
                                      lower    = file.parent_rec
                                      upper    = file.level_rec
                                      if file.rec[0] EQ -1 then num_tran = 0 $
                                                           else num_tran = n_elements(file.level_rec)
                                   end

                        'ionis.' : begin
                                      lower    = file.parent_ion_rate
                                      upper    = file.level_ion_rate
                                      if file.ion_rate[0] EQ -1 then num_tran = 0 $
                                                               else num_tran = n_elements(file.level_ion_rate)
                                   end

                        'CX'     : begin
                                      lower    = file.parent_cx
                                      upper    = file.level_cx
                                      if file.cx[0] EQ -1 then num_tran = 0 $
                                                          else num_tran = n_elements(file.level_cx)
                                   end

                         else    : begin
                                        lower    = -1
                                        upper    = -1
                                        num_tran = 0
                                     end

                     endcase


                     
                     print_val = { dsn     : '',                   $
                                  defname : 'adas811_plot_all.ps', $
                                  replace :  1,                    $
                                  devlist :  info.devlist,         $
                                  dev     :  info.devlist[0],      $
                                  paper   :  'A4',                 $
                                  write   :  0,                    $
                                  message :  'Note: cannot append to graphics files'}
                    
                     act = 0
                     adas_file_gr, print_val, act, font=info.font, $
                                   title='File name for graphical output'

                     print_stuff = { dsn     : print_val.dsn,   $
                                     devcode : info.devcode,    $
                                     devlist : info.devlist,    $
                                     dev     : print_val.dev,   $
                                     paper   : print_val.paper  }

                     if print_val.write EQ 1 then begin               

                        info_str = info.adasrel + ' ' + info.adasprog

                        for j = 0, num_tran-1 do begin
                           if j EQ 0 then p_first = 1 else p_first = 0
                           if j EQ num_tran-1 then p_last = 1 else p_last = 0

                           adas811_plot, info.ifile, lower[j], upper[j],          $
                                         info.type, info.exc_type, info.burgess,  $
                                         info.ptrToData, info.ptrToMap,           $
                                         print = print_stuff, info_str = info_str,$
                                         te=info.units,                           $
                                         p_first = p_first, p_last = p_last
                           
                           out_str = 'Printing transition : ' + string(j+1, format='(i5)')
                           widget_control, info.messID, set_value = out_str
                        
                        endfor


                        widget_control, info.messID, $
                                        set_value = 'Plots written to file'

                     endif else begin

                        widget_control, info.messID,$
                                        set_value = 'No file selected'

                     endelse
                  
                  
                 end 
                 
                
  info.filescanid : begin
                                    
                   widget_control, info. filescanID, get_value=ifile
                   
                   if ifile NE info.ifile then begin
                      
                      info.ifile = ifile
                      
                      adas811_newmap, ifile, info.ptrtodata, info.ptrtomap
                  
                      file = *info.ptrToData[ifile]
                      
                      adas811_transition, info.type, file, $
                                          info.lower, info.upper, info.panelID
                      
                      adas811_plot, info.ifile, info.lower, info.upper,     $
                                    info.type, info.exc_type, info.burgess, $
                                    info.ptrToData, info.ptrToMap, te=info.units
                    
                   endif

                 end
             
  info.showid : begin
                 
                  widget_Control, info.t1ID, get_value=t1
                  widget_Control, info.t2ID, get_value=t2
                  
                  tmin = min([t1,t2], max=tmax)
                  info.lower = tmin
                  info.upper = tmax
                  
                  file = *info.ptrToData[info.ifile]
                  
                  adas811_transition, info.type, file, $
                                      info.lower, info.upper, info.panelID
                      
  
                  adas811_plot, info.ifile, tmin, tmax, info.type, $
                                info.exc_type, info.burgess,       $
                                info.ptrToData, info.ptrToMap, te=info.units
                  
                end
                
  info.go_r_id  : adas811_advance, 1, info
  info.go_l_id  : adas811_advance, -1, info
  info.go_rr_id : adas811_advance, info.jump, info
  info.go_ll_id : adas811_advance, -1*info.jump, info
            
             
  info.typeid : begin
                 
                 
                  widget_Control, info.typeID, get_value=ind
                 
                  if info.type_buttons[ind] NE info.type then begin
                     
                     ; if not electron excitation de-sensitise options 
                     
                     if ind NE 0 then begin
                        widget_Control, info.excID, map=0
                        widget_Control, info.cplotID, map=0
                     endif else begin
                        widget_Control, info.excID, map=1
                        widget_Control, info.cplotID, map=1
                     endelse
                     
                     ; if CX or p-exc put up message and reset to what it was
                     
                     if info.type_buttons[ind] EQ 'CX' OR $
                        info.type_buttons[ind] EQ 'p-exc' then begin
                        
                        mess = 'Not currently available'
                        widget_control, info.messID, set_value = mess
                       
                        last = where(info.type_buttons EQ info.type)
                        widget_control, info.typeID, set_value = last
                        if last[0] EQ 0 then begin
                           widget_Control, info.excID, map=1
                           widget_Control, info.cplotID, map=1
                        endif
                     
                     endif else begin
                        
                        ; set first transition and update info structure

                        file = *info.ptrToData[info.ifile]
                        adas811_first_transition, info.type_buttons[ind],  $
                                                  file, lower, upper, ntran

                        adas811_transition, info.type_buttons[ind], file, $
                                            lower, upper, info.panelID

                        widget_Control, info.t1ID, set_value=upper
                        widget_Control, info.t2ID, set_value=lower

                        info.type  = info.type_buttons[ind]
                        info.lower = lower
                        info.upper = upper

                        ; Plot the new transition

                        adas811_plot, info.ifile, lower, upper, info.type, $
                                      info.exc_type, info.burgess,         $
                                      info.ptrToData, info.ptrToMap,       $
                                      te=info.units
                     
                     endelse
                     
                  endif
                  
                 
                 
                 end
             
  info.excid : begin

                  ; get current transition
                  
                  widget_Control, info.t1ID, get_value=t1
                  widget_Control, info.t2ID, get_value=t2
                  tmin = min([t1,t2], max=tmax)
                  
                  widget_Control, info.excID, get_value=ind
                  widget_Control, info.cplotID, get_value=burgess
                 
                  exc_type = info.exc_type_buttons[ind[0]]
                  
                  adas811_plot, info.ifile, tmin, tmax, info.type, exc_type, $
                                burgess, info.ptrToData, info.ptrToMap,      $
                                te=info.units
                                
                  info.burgess  = burgess
                  info.exc_type = exc_type
               
               end
           
  info.teid : begin
                    
                 widget_Control, info.teID, get_value=ind
                 info.units = info.units_buttons[ind[0]]
                   
                 adas811_plot, info.ifile, info.lower, info.upper, info.type, $
                               info.exc_type, info.burgess,                   $
                               info.ptrToData, info.ptrToMap, te=info.units
                               
              end 


  else : print,'ADAS811_IN_EVENT : You should not see this message! '
                   
ENDCASE


; update the info (except when we cancel!)

if event.id NE info.cancelid then Widget_Control, event.handler, Set_UValue=info


END
  
;-----------------------------------------------------------------------------


PRO adas811_first_transition, type, file, lower, upper, num_tran


case type of

   'e-exc'  : begin
                 lower    = file.lower[0]
                 upper    = file.upper[0]
                 num_tran = n_elements(file.aval)
              end

   'p-exc'  : begin
                 lower    = -1
                 upper    = -1
                 num_tran = 0
              end

   'rec.'   : begin
                 lower    = file.parent_rec[0]
                 upper    = file.level_rec[0]
                 if file.rec[0] EQ -1 then num_tran = 0 $
                                      else num_tran = n_elements(file.level_rec)
              end

   'ionis.' : begin
                 lower    = file.parent_ion_rate[0]
                 upper    = file.level_ion_rate[0]
                 if file.ion_rate[0] EQ -1 then num_tran = 0 $
                                          else num_tran = n_elements(file.level_ion_rate)
              end

   'CX'     : begin
                 lower    = file.parent_cx[0]
                 upper    = file.level_cx[0]
                 if file.cx[0] EQ -1 then num_tran = 0 $
                                     else num_tran = n_elements(file.level_cx)
              end

    else    : begin
                   lower    = -1
                   upper    = -1
                   num_tran = 0
                end
                
endcase


END
;-----------------------------------------------------------------------------




FUNCTION adas811_proc, adas_file, comp_files,                        $  
                       bitfile, devlist, devcode, adasrel, adasprog, $
                       FONT_LARGE = font_large,                      $
                       FONT_SMALL = font_small,                      $
                       standalone = standalone,                        $
                       cwid       = cwid


; Set defaults for keywords

  IF n_elements(font_large) eq 0 THEN font_large = ''
  IF n_elements(font_small) eq 0 THEN font_small = ''


; Some constants and initial conditions

  type_buttons     = ['e-exc', 'p-exc', 'rec.', 'ionis.', 'CX']
  exc_type_buttons = ['gamma', 'rate', 'C-plot']
  units_buttons    = ['K', 'eV']
  
  jump = 5

  type     = type_buttons[0]
  exc_type = exc_type_buttons[0]
  burgess  = 1.5
  units    = 'K'
  
  linestyles = ['(solid)', '(dot)', '(dash)', '(dash-dot)', '(dash-dot-dot)']
                    	
                       
; No more distinction on file source - only deal with actual files
; Remove non existing files and print a message to the screen

  files = [adas_file, comp_files]
  res   = where(files NE '', n_files)
  if n_files GT 0 then files = files[res]
  
  res   = file_test(files)
  ind_f = where(res EQ 1, n_files)
  ind_n = where(res EQ 0, c_nofile)
  if c_nofile GT 0 then print, 'File(s) do not exist : ' + files[ind_n] 
  
  if n_files EQ 0 then message, 'No files - unable to continue'
  files = files[ind_f]



  

; Read in the files into an array of pointers

  
  read_adf04, file=files[0], fulldata=adf04, /ecipcalc
  ptrToData = [ptr_new(adf04)]
  ptrToMap  = [ptr_new(indgen(n_elements(adf04.ia)))]

  for j = 1, n_files-1 do begin
  
     read_adf04, file=files[j], fulldata=adf04, /ecipcalc
     ptrToData = [ptrToData, ptr_new(adf04)]
     ptrToMap  = [ptrToMap, ptr_new(indgen(n_elements(adf04.ia)))]
     
  endfor


; Check if all files are for the same ion

  iz_test  = intarr(n_files)
  iz0_test = intarr(n_files)
  for j = 0, n_files-1 do begin
     iz_test[j]  = (*ptrToDAta[j]).iz + 1
     iz0_test[j] = (*ptrToDAta[j]).iz0
  endfor
  
  if total(iz_test)  NE n_files*iz_test[0] OR    $
     total(iz0_test) NE n_files*iz0_test[0] then begin
     warn_message =  'WARNING: may not be the same element or ionisation stage'
     print, warn_message
  endif else warn_message = ' '
  
  
  
  
; assume first file is initially selected for display purposes   
  
  ifile = 0
  file  = *ptrToData[ifile]
  
  adas811_first_transition, type, file, lower, upper, ntran  

  adas811_newmap, ifile, ptrtodata, ptrtomap


  

; Create modal top level base widget
 
  if not keyword_set(cwid) then begin
    parent = Widget_Base(Column=1, Title='ADAS811 : Compare adf04 files', $
                         xoffset=100, yoffset=1)
  endif else begin
    parent = Widget_Base(cwid,/column,event_pro='ADAS811_PROC_EVENT')
  endelse
  
  rc     = Widget_label(parent,value='  ',font=font_large)
  

  base   = Widget_base(parent, /column)


; Ask for primary file for scan through

  ind        = indgen(n_files)
  file_label = files + ' ' + linestyles[ind]
  file_label = files
  
  filescanID = cw_bgroup(base, file_label, exclusive=1,column=1, $
                         label_top = 'Choose file to scan through:', $
                         font=font_small, /frame, /no_release)


; Draw area

  gap    = Widget_label(base,value=' ',font=font_small)

  mbase  = Widget_base(base, /row)
  lbase  = Widget_base(mbase, /column)

  
  lab    = Widget_label(parent,value=' ',font=font_large)

  device, get_screen_size=scrsz
  xsize = scrsz[0]*0.5
  ysize = scrsz[1]*0.6

  drawID = Widget_draw(lbase, ysize=ysize, xsize=xsize, $
                       /motion_events, event_pro='ADAS811_PROC_CURSOR', $
                       uvalue=parent) 

  widget_control,drawID,get_value=wid
  wset,wid

; Set advance / retract frame buttons
  
  go_l_file  = bitfile + '/go_l.bmp'
  go_ll_file = bitfile + '/go_ll.bmp'
  go_r_file  = bitfile + '/go_r.bmp'
  go_rr_file = bitfile + '/go_rr.bmp'
  
  read_X11_bitmap, go_l_file,  go_l_bmp
  read_X11_bitmap, go_ll_file, go_ll_bmp
  read_X11_bitmap, go_r_file,  go_r_bmp
  read_X11_bitmap, go_rr_file, go_rr_bmp

  fbase     = widget_base(lbase,/row)
  go_ll_ID  = widget_button(fbase,value=go_ll_bmp)
  go_l_ID   = widget_button(fbase,value=go_l_bmp)
  message   = '    1 of '+string(ntran,format='(i5)')
  panelID   = widget_label(fbase,value=message,font=font_large)
  go_r_ID   = widget_button(fbase,value=go_r_bmp)
  go_rr_ID  = widget_button(fbase,value=go_rr_bmp)


; Choose a specific transition

  lab       = Widget_label(fbase,value=' ',font=font_large)
  showID    = Widget_button(fbase, value='Show',font=font_large)
  t1ID      = cw_field(fbase,  title='', $
                         fieldfont=font_large, xsize=3,       $
                         value=upper, /integer)  
  lab       = Widget_label(fbase,value=' : ',font=font_large)
  t2ID      = cw_field(fbase,  title='', $
                         fieldfont=font_large, xsize=3,       $
                         value=lower, /integer)  

; Put in a gap

  gap = Widget_label(mbase,value='   ',font=font_large)



; Options on the right
  
  rbase   = Widget_base(mbase, /column)

  lab     = Widget_label(rbase,value='Type of plot:',font=font_large)
  lab     = Widget_label(rbase,value='-------------',font=font_large)
  
  typeID  = cw_bgroup(rbase, type_buttons, exclusive=1, column=1, $
                      font=font_large, /no_release)
 
  lab     = Widget_label(rbase,value='  ',font=font_large)
  
  
; Extra options for excitation and ionisation from excited states
  
  switchbase = widget_base(rbase, /column, /frame)
  excbaseID  = Widget_base(switchbase, /column)
  ionbaseID  = Widget_base(switchbase, /column)

;    --- excitation

  lab     = Widget_label(excbaseID,value='Type of e-exc:',font=font_large)
  lab     = Widget_label(excbaseID,value='-------------',font=font_large)

  excID   = cw_bgroup(excbaseID,exc_type_buttons, exclusive=1,column=1, $
                      font=font_large, /no_release)
  cplotID = cw_field(excbaseID, font=font_large, title='', $
                                fieldfont=font_large, xsize=6,       $
                                value=1.5, /float)

;    --- ionisation from excited states


; Temperature units

  rbase   = Widget_base(rbase, /column)

  lab     = Widget_label(rbase,value='  ',font=font_large)
  lab     = Widget_label(rbase,value='Units of Te: ',font=font_large)
  lab     = Widget_label(rbase,value='-------------',font=font_large)
  
  teID    = cw_bgroup(rbase, units_buttons, exclusive=1, column=1, $
                      font=font_large, /no_release)
 



; End of panel message and buttons                


  messID = widget_label(parent, value=warn_message, font=font_large)
                            
  menufile = bitfile + '/menu.bmp'
  read_X11_bitmap, menufile, bitmap1
  
  if not keyword_set(cwid) then begin              
    mrow        = widget_base(parent,/row)
    if not keyword_set(standalone) then begin
      menuID      = widget_button(mrow,value=bitmap1,font=font_large, $
                                  event_pro='ADAS811_PROC_MENU')
      canceltext='Cancel'
    endif else begin
      menuID = -1
      canceltext='Close'
    endelse 
  
  
    cancelID    = widget_button(mrow,value=canceltext,font=font_large)
    printID     = widget_button(mrow,value='Print',font=font_large)
    printallID  = widget_button(mrow,value='Print All',font=font_large)

    mskip     = Widget_label(mrow,value='             ',font=font_small)
  endif else begin
    mrow       = fbase
    cancelID   = -1
    printID    = -1
    printallID = -1
  endelse

; Place for value under cursor

    valueID   = Widget_label(mrow,value='  ',font=font_small)

; Initial settings


widget_control, filescanID, set_value=0
widget_control, excID, set_value=0
widget_control, typeID, set_value=0
widget_control, teID, set_value=0



; Realize the ADAS811_PROC input widget.


   dynlabel, parent
   
   if not keyword_set(cwid) then widget_Control, parent, /realize

; Plot the first transition - must wait until after it is realized!

  
  if not keyword_set(cwid) then begin
    adas811_plot, ifile, lower, upper, type, exc_type, burgess, $
                  ptrToData, ptrToMap, te=units
  endif



; Create a pointer to store the information collected from the form.
; The initial data stored here is set to CANCEL, so nothing needs to
; be done if the user kills the widget with the mouse.

   ptrToFormData = Ptr_New({cancel:1, menu:0})

; Create an info structure with program information.

  info = { messID            :    messID,            $
           filescanID        :    filescanID,        $
           go_ll_ID          :    go_ll_ID,          $
           go_l_ID           :    go_l_ID,           $
           panelID           :    panelID,           $
           go_r_ID           :    go_r_ID,           $
           go_rr_ID          :    go_rr_ID,          $
           showID            :    showID,            $
           t1ID              :    t1ID,              $
           t2ID              :    t2ID,              $
           valueID           :    valueID,           $
           typeID            :    typeID,            $
           excID             :    excID,             $
           cplotID           :    cplotID,           $
           excbaseID         :    excbaseID,         $
           ionbaseID         :    ionbaseID,         $
           teID              :    teID,              $
           cancelID          :    cancelID,          $
           printID           :    printID,           $
           printallID        :    printallID,        $
           devlist           :    devlist,           $
           devcode           :    devcode,           $
           adasrel           :    adasrel,           $
           adasprog          :    adasprog,          $
           font              :    font_large,        $
           ptrToData         :    ptrToData,         $ 
           ptrToMap          :    ptrToMap,          $
           jump              :    jump,              $
           ifile             :    ifile,             $
           lower             :    lower,             $
           upper             :    upper,             $
           ntran             :    ntran,             $
           type              :    type,              $
           exc_type          :    exc_type,          $
           type_buttons      :    type_buttons,      $
           exc_type_buttons  :    exc_type_buttons,  $
           units_buttons     :    units_buttons,     $
           burgess           :    burgess,           $
           units             :    units,             $
           ptrToFormData     :    ptrToFormData      } 
  
            
; Store the info structure in the user value of the top-level base and launch
; the widget into the world.

  widget_control, parent, Set_UValue=info

  if not keyword_set(cwid) then begin

    XManager, 'ADAS811_PROC', parent, Event_Handler='ADAS811_PROC_EVENT'


; When the widget is destroyed, the block is released, and we
; return here. Get the form data that was collected by the form
; and stored in the pointer location.

    formdata = *ptrToFormData

    rep='CONTINUE'
    IF N_Elements(formdata) EQ 0 THEN BEGIN
       Ptr_Free, ptrToFormData
       Ptr_Free, ptrToData
       Ptr_Free, ptrToMap
       rep ='CANCEL'
    ENDIF
 
    IF formdata.cancel EQ 1 THEN BEGIN
       Ptr_Free, ptrToFormData
       Ptr_Free, ptrToData
       Ptr_Free, ptrToMap
       rep ='CANCEL'
       RETURN,rep
    ENDIF

    IF formdata.menu EQ 1 THEN BEGIN
       Ptr_Free, ptrToFormData
       Ptr_Free, ptrToData
       Ptr_Free, ptrToMap
       rep ='MENU
       RETURN,rep
    ENDIF

    RETURN, rep

endif else begin

  RETURN, parent

endelse

END
