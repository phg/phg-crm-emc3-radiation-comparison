pro adas812_match_levels,data1,data2,idx1,idx2

	n1=(size(data1.ia))[1]
	n2=(size(data2.ia))[1]

	str1=data1.cstrga
	str2=data2.cstrga

	idx1=[-1]
	idx2=[-1]

	if data1.cstrga[0] ne data2.cstrga[0] then begin
        	print,'Ground configurations do not match'
                stop
                ; N.B. Catch here for formats being different
        
        endif

	for i=0,n1-1 do begin
        	; find where configuration, and outer quantum numbers are the same

		pos=where(data1.cstrga[i] eq data2.cstrga $
                     and  data1.ila[i] eq data2.ila $
                     and  data1.isa[i] eq data2.isa $
                     and  data1.xja[i] eq data2.xja )
                     
                if pos[0] ne -1 then begin

			if (size(pos))[1] eq 1 then begin
				ediff=abs((data1.wa[i]-data2.wa[pos[0]]) / data1.wa[i])
                		if ediff lt 0.01 or i eq 0 then begin
        	        	        idx1=[idx1,i]
		                	idx2=[idx2,pos[0]]
        	                endif else begin
					print,'Can not find energy level within 1% for level ',strtrim(string(i+1),2),' of file 1'                 
                        	endelse
	                endif else begin
        	        	;print,'need to use energy ordering'
				ediff=abs((data1.wa[i]-data2.wa[pos]) / data1.wa[i])
	                        junk=min(ediff,posi)
				if (ediff[posi] lt 0.01) then begin
	        	                idx1=[idx1,i]
		                        idx2=[idx2,pos[posi]]
				endif else begin
	                        	print,'Can not find energy level within 1% for level ',strtrim(string(i+1),2),' of file 1'
        	                endelse
                	endelse
		endif
                		        
        end

	idx1=idx1[1:n_elements(idx1)-1]
	idx2=idx2[1:n_elements(idx2)-1]
                lvals=['S','P','D','F','G']
        
;        for i=0,(size(idx1))[1]-1 do begin
;		index1=idx1[i]
;		index2=idx2[i]
;                strshow=strtrim(data1.cstrga[index1],2) + ' (' + strtrim(string(data1.isa[index1]),2) + ')' + lvals[data1.ila[index1]] + '(' + strtrim(string(data1.xja[index1],format='(F8.1)'),2) + ') - ' + strtrim(data2.cstrga[index2],2) + ' (' + strtrim(string(data2.isa[index2]),2) + ')' + lvals[data2.ila[index2]] + '(' + strtrim(string(data2.xja[index2],format='(F8.1)'),2) + ')'
;		print,strshow
;        
;        end

end
