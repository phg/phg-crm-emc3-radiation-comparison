; Copyright (c) 2002 Strathclyde University .
;+
; PROJECT:
;       ADAS
;
; NAME:
;	ADAS812
;
; PURPOSE:
;	Compare 2 adf04 files via ratios of upsilons.
;
; EXPLANATION: ???
;
; USE:
;	First the system settings must be established by calling
;	adas_sys_set.pro then adas812.pro is called.
;
;	adas_sys_set, adasrel, fortdir, userroot, centroot, $
;		      devlist, devcode, font_large, font_small, edit_fonts
;	adas812,   adasrel, fortdir, userroot, centroot, devlist, $
;		   devcode, font_large, font_small, edit_fonts
;
; INPUTS:
;	ADASREL - A string indicating the ADAS system version, 
;		  e.g ' ADAS RELEASE: ADAS93 V1.4'.  The first
;		  character should be a space.
;
;	FORTDIR - Not applicable here.
;
;	USERROOT - A string holding the path to the root directory of
;		   the user's adas data. e.g '/disk/bowen/adas'
;		   This root directory will be used by adas to construct
;		   other path names.  For example the users default data
;		   for adas205 should be in /disk/bowen/adas/adf04.  In
;		   particular the user's default interface settings will
;		   be stored in the directory USERROOT+'/defaults'.  An
;		   error will occur if the defaults directory does not
;		   exist.
;
;	CENTROOT - Like USERROOT, but this directory points to the
;		   central data area for the system.  User defaults are
;		   not stored on CENTROOT.
;
;	DEVLIST - A string array of hardcopy device names, used for
;		  graphical output. e.g ['Post-Script','HP-PCL','HP-GL']
;		  This array must mirror DEVCODE.  DEVCODE holds the
;		  actual device names used in a SET_PLOT statement.
;
;	DEVCODE - A string array of hardcopy device code names used in
;		  the SET_PLOT statement, e.g ['PS', 'PCL', 'HP']
;		  This array must mirror DEVLIST.
;
;	FONT_LARGE - The name of a larger font e.g 'courier_bold14'
;
;	FONT_SMALL - The name of a smaller font e.g 'courier_bold12'
;
;	EDIT_FONTS - A structure of two fonts used in the table editor
;		     adas_edtab.pro, {font_norm:'',font_input:''}.
;		     The two fonts are used to differentiate between
;		     editable and non-editable parts of the table. You
;		     may have to experiment to get two fonts which give
;		     the same default spacing in the edit widget.
;		     e.g {font_norm:'helvetica_bold14', $
;			  font_input:'helvetica_oblique14'}
;
; OPTIONAL INPUTS:
;	None
;
; OUTPUTS:
;	None - Note this routine should leave all inputs un-modified.
;
; OPTIONAL OUTPUTS:
;	None
;
; KEYWORD PARAMETERS:
;	None
;
; CALLS:
;
; SIDE EFFECTS:
;
; CATEGORY:
;	Adas system.
;	
; WRITTEN:
;       Allan Whiteford
;
; MODIFIED:
;	1.1	Allan Whiteford
;		First release, based on adas812.pro v 1.2
;
; VERSION:
;	1.1	22-06-2004
; 
;-
;-----------------------------------------------------------------------------

PRO adas812,	adasrel, fortdir, userroot, centroot, 			$
		devlist, devcode, font_large, font_small, edit_fonts


; Initialisation and IDL v5 check

    adasprog = ' PROGRAM: ADAS812 v1.2'
    deffile  = userroot+'/defaults/adas812_defaults.dat'
    bitfile  = centroot+'/bitmaps'
    device   = ''

    thisRelease = StrMid(!Version.Release, 0, 1)
    IF thisRelease LT '5' THEN BEGIN
       message = 'Sorry, ADAS812 requires IDL 5 or higher' 
       tmp = popup(message=message, buttons=['Accept'],font=font_large)
       goto, LABELEND
    ENDIF


; Restore or set the defaults

    files = findfile(deffile)
    if files(0) eq deffile then begin
        
        restore, deffile
        inval.centroot1 = centroot+'/adf04/'
        inval.userroot1 = userroot+'/adf04/'
        inval.centroot2 = centroot+'/adf04/'
        inval.userroot2 = userroot+'/adf04/'
    
    endif else begin
	inval =	{ ROOTPATH1   :   userroot+'/adf04/',    $
		  FILE1       :   '',                    $
		  CENTROOT1   :   centroot+'/adf04/',    $
		  USERROOT1   :   userroot+'/adf04/',    $
		  ROOTPATH2   :   userroot+'/adf04/',    $
		  FILE2       :   '',                    $
		  CENTROOT2   :   centroot+'/adf04/',    $
		  USERROOT2   :   userroot+'/adf04/'     }
  
        procval = {NMET:-1}
    
    end


; Ask for two input adf04 files

LABEL100:

    rep = adas812_in(inval, FONT_LARGE=font_large, FONT_SMALL = font_small)
 
    if rep eq 'CANCEL' then goto, LABELEND


; Display comparisons

LABEL200:

    first_file  = inval.rootpath1 + inval.file1
    second_file = inval.rootpath2 + inval.file2
    
    ;stop
    
    rep=adas812_proc(first_file, second_file, bitfile, devlist,       $
                     devcode, adasrel, adasprog,                    $
                     FONT_LARGE=font_large, FONT_SMALL = font_small)
   
    
    if rep eq 'MENU'   then goto, LABELEND
    if rep eq 'CANCEL' then goto, LABEL100


LABELEND:


                ;**** Save user defaults ****

    save, inval,  procval, filename=deffile


END
