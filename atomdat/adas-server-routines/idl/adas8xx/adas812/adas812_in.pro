; Copyright (c) 2002 Strathclyde University .
;+
; PROJECT:
;       ADAS 
;
; NAME:
;	ADAS812_IN
;
; PURPOSE: Acquires names of adf04 files which are to be compared. 
;
;
; EXPLANATION:
;
;     
;
; NOTES: It entirely in IDL and uses v5 (and above) features such as 
;        pointers to pass data.
; 
;
;
; INPUTS:
;        inval       - A structure holding the 'remembered' input data.
;
; OPTIONAL INPUTS:
;       None
;
; OUTPUTS:
;       The function returns a string rep
;           rep = 'CONTINUE' if everything is ok
;           rep = 'CANCEL' if there is a problem or indeed if the user
;                          closes the widget with the mouse. 
;
; OPTIONAL OUTPUTS:
;       None
;
; KEYWORD PARAMETERS:
;       FONT_LARGE - The name of a larger font e.g 'courier_bold14'
;
;       FONT_SMALL - The name of a smaller font e.g 'courier_bold12'
;
;
; CALLS:
;                            
;       ADAS812_FILE_TEST       : Tests whether a file exists and if so 
;                                 increments the allow to browse variable.
;       ADAS812_IN_NULL_EVENTS  : Black hole for events which we don't use.
;       ADAS812_IN_EVENT        : Event handler for all possible actions.
;
; SIDE EFFECTS:
;
; CATEGORY:
;	Widgets
;
; WRITTEN:
;       Allan Whiteford
;
; MODIFIED:
;	1.1	Allan Whiteford
;		First release, based on adas811_in.pro v 1.2
;
; VERSION:
;       1.1	22-06-2004
;
;-
;-----------------------------------------------------------------------------

PRO ADAS812_FILE_TEST, filename, browse


file_acc, filename, fileexist, read, write, execute, filetype

if filetype EQ '-' AND read EQ 1 then browse = browse + 1

END
;-----------------------------------------------------------------------------

FUNCTION ADAS812_IN_NULL_EVENTS, event

   ; The purpose of this event handler is to do nothing
   ;and ignore all events that come to it.
      
   RETURN, 0

END 

;-----------------------------------------------------------------------------



PRO ADAS812_IN_EVENT, event

; Get the info structure out of the user value of the top-level base.

Widget_Control, event.top, Get_UValue=info



Widget_Control, event.id, Get_Value=userEvent

; React to the Select... buttons for unix-style file selection

; More conventional ADAS events

CASE userEvent OF

  'Cancel' : begin
               formdata = {cancel:1}
               *info.ptrToFormData =formdata
               widget_Control, event.top, /destroy
             end 

      
  'Done'   : begin
                 
                ; gather the data for return to calling program
                ; 'Done' is not sensitised until valid filenames
                ; have been chosen.
               
                widget_Control, info.flaID1, Get_Value=vala1
                widget_Control, info.flaID2, Get_Value=vala2
                
                browse=0
                
                file1 = vala1.rootpath + vala1.file
                adas812_file_test, file1, browse
                
                file2 = vala2.rootpath + vala2.file
                adas812_file_test, file2, browse
                
                err=0
                if browse ne 2 then begin
                	err=1
                        mess="Both files do not exist"
                
                endif                
                                              
                if err EQ 0 then begin
                   formdata = {file1   : vala1,       $
                               file2   : vala2,     $
                               cancel  : 0           }
                    *info.ptrToFormData = formdata
                    widget_control, event.top, /destroy
                endif else begin
                   widget_Control, info.messID, Set_Value=mess
                endelse

             end
             
    'Browse Comments' : Begin
    
                browse = 0
                
                Widget_Control, info.flaID1, get_value=vala1
                file1 = vala1.rootpath + vala1.file
                adas812_file_test, file, browse
                Widget_Control, info.flaID2, get_value=vala2
                file2 = vala1.rootpath + vala2.file
                adas812_file_test, file2, browse
                
                if browse eq 2 then begin
       	        	xxtext, file1, font=info.font_large
	                xxtext, file2, font=info.font_large
     		endif
     
             end
    
    'Select...' :  ; Do nothing!!
          
  else : print,'ADAS812_IN_EVENT : You should not see this message! ',userEvent
                   
ENDCASE

END
  
;-----------------------------------------------------------------------------


FUNCTION adas812_in, inval,                  $
                    FONT_LARGE = font_large, $
                    FONT_SMALL = font_small


; Set defaults for keywords

  IF n_elements(font_large) eq 0 THEN font_large = ''
  IF n_elements(font_small) eq 0 THEN font_small = ''


; Path names may be supplied with or without the trailing '/'.  Add
; this character where required so that USERROOT will always end in
; '/' on output.
  
  if strtrim(inval.rootpath1) eq '' then begin
      inval.rootpath1 = './'
  endif else if                                                   $
  strmid(inval.rootpath1, strlen(inval.rootpath1)-1,1) ne '/' then begin
      inval.rootpath1 = inval.rootpath1+'/'
  endif
  if strtrim(inval.rootpath2) eq '' then begin
      inval.rootpath2 = './'
  endif else if                                                   $
  strmid(inval.rootpath2, strlen(inval.rootpath2)-1,1) ne '/' then begin
      inval.rootpath2 = inval.rootpath2+'/'
  endif



 

; Modal top level base widget

                
  parent = Widget_Base(Column=1, Title='ADAS812 INPUT', XOFFSET=100, YOFFSET=1)

  rc     = widget_label(parent,value='  ',font=font_large)

  base   = widget_base(parent, /column)
  
 
  
; ADAS style adf04 file selection


  flabase  = widget_base(base, /column, /frame)
  flaval   = {  ROOTPATH   :  inval.rootpath1,                $
                FILE       :  inval.file1,                    $
                CENTROOT   :  inval.centroot1,                $
                USERROOT   :  inval.userroot1                 }
  flatitle = widget_label(flabase, font=font_large, value='adf04 file 1:')
  flaID1   = cw_adas_infile(flabase, value=flaval, font=font_large, $
                            ysize = 5, event_func='ADAS812_IN_NULL_EVENTS')

  flabase  = widget_base(base, /column, /frame)
  flaval   = {  ROOTPATH   :  inval.rootpath2,                $
                FILE       :  inval.file2,                    $
                CENTROOT   :  inval.centroot2,                $
                USERROOT   :  inval.userroot2                 }
  flatitle = widget_label(flabase, font=font_large, value='adf04 file 2:')
  flaID2   = cw_adas_infile(flabase, value=flaval, font=font_large, $
                            ysize = 5, event_func='ADAS812_IN_NULL_EVENTS')

                            
; Buttons 
                
  mrow     = widget_base(parent,/row,/align_center)
  messID   = widget_label(mrow,font=font_large, $
                          value='          Enter File information             ')
                
  mrow     = widget_base(parent,/row)
  
  browseID = widget_button(mrow, value='Browse Comments', font=font_large)
  cancelID = widget_button(mrow,value='Cancel',font=font_large)
  doneID   = widget_button(mrow,value='Done',font=font_large)





; Initial settings - none! If files don't exist say so when
; attempting to continuie or browse.



; Realize the adas812 input widget.

   widget_Control, parent, /realize

; Create a pointer to store the information collected from the form.
; The initial data stored here is set to CANCEL, so nothing needs to
; be done if the user kills the widget with the mouse.

   ptrToFormData = Ptr_New({cancel:1})

; Create an info structure with program information.

  info = { flaID1          :  flaID1,             $
           flaID2          :  flaID2,             $
           doneID          :  doneID,             $
           browseID        :  browseID,           $
           messID          :  messID,             $
           parent          :  parent,             $
           font_large      :  font_large,         $
           ptrToFormData   :  ptrToFormData       }  
            
               
; Store the info structure in the user value of the top-level base and launch
; the widget into the world.

  widget_control, parent, Set_UValue=info

  XManager, 'adas812_in', parent, Event_Handler='ADAS812_IN_EVENT'


; When the widget is destroyed, the block is released, and we
; return here. Get the data that was collected by the widget
; and stored in the pointer location. Finally free the pointer.

formdata = *ptrToFormData

rep='CONTINUE'
IF N_Elements(formdata) EQ 0 THEN BEGIN
   Ptr_Free, ptrToFormData
   rep ='CANCEL'
ENDIF
 
IF formdata.cancel EQ 1 THEN BEGIN
   Ptr_Free, ptrToFormData
   rep ='CANCEL'
ENDIF

if rep eq 'CONTINUE' then begin
   inval.rootpath1   = formdata.file1.rootpath
   inval.file1       = formdata.file1.file
   inval.centroot1   = formdata.file1.centroot
   inval.userroot1   = formdata.file1.userroot
   inval.rootpath2   = formdata.file2.rootpath
   inval.file2       = formdata.file2.file
   inval.centroot2   = formdata.file2.centroot
   inval.userroot2   = formdata.file2.userroot
   Ptr_Free, ptrToFormData
endif       


RETURN, rep

END
