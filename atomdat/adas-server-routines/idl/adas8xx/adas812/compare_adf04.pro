; Copyright (c) 2004, Strathclyde University.
;----------------------------------------------------------------------
;+
; PROJECT    :  ADAS
;
; NAME       :  compare_adf04
;
; PURPOSE    :  Displays comparison of two adf04 files
;
; EXPLANATION:
;       This routine launches the final ADAS812 screen with
;       the supplied files.
;
; USE:
;       An example;
;               compare_adf04,['file1.dat','file2.dat']
;
; INPUTS:
;       FILES: A two element vector containing
;              filenames (or paths and filenames) of adf04 files.
;
; OPTIONAL INPUTS:
;       None.
;
; OUTPUTS:
;       None.
;
; OPTIONAL OUTPUTS:
;       None.
;
; KEYWORD PARAMETERS:
;       None.
;
; CALLS:
;       adas_sys_set
;       adas812_proc
;
; SIDE EFFECTS:
;       A widget is created
;
; AUTHOR     :  Allan Whiteford
; 
; DATE       :  22-06-05
; 
;
; MODIFIED:
;       1.1     Allan Whiteford
;              	- First version.
;
; VERSION:
;       1.1    22-06-05
;-
;----------------------------------------------------------------------

pro compare_adf04,files

	adas_sys_set, adasrel, fortdir, userroot, centroot, $
		      devlist, devcode, font_large, font_small, edit_fonts

	adasprog="Compare ADF04 files";
        
	deffile  = userroot+'/defaults/adas811_defaults.dat'
	bitfile  = centroot+'/bitmaps'

	if (size(files))[0] eq 0 then begin
		print,'You must supply two files'
 		return
        endif else begin
        	file1=files[0]
                file2=files[1]
        endelse
                
        rep=adas812_proc(file1, file2, bitfile, devlist,             $
                     devcode, adasrel, adasprog,                     $
                     FONT_LARGE=font_large, FONT_SMALL = font_small, $
                     /standalone)

end
