; Copyright (c) 2002 Strathclyde University .
;+
; PROJECT: ADAS 
;
; NAME: ADAS812_PROC
;
; PURPOSE: Compare two ADF04 files
;
; NOTES: Graphical interface to compare two adf04 files, primarily through
;        a scatter plot. Can be called as a standalone program via
;        the compare_adf04 IDL command line procedure, as a compound
;        widget inside another code using the /cwid keyword or as
;        part of the central ADAS ADAS812 program which has an input
;        screen to select files
;
; INPUTS:
;         adas812_proc, file1, file2,                        $  
;                       bitfile, devlist, devcode, adasrel, adasprog, $
;                       FONT_LARGE = font_large,                      $
;                       FONT_SMALL = font_small
;
;          file1      - first adf04 file
;          file2      - second adf04 file
;          bitfile    - location of bitmaps
;          devlist    - printer devices
;          devcode    - 
;          adasrel    - ADAS release
;          adasprog   - ADAS program
;          standalone - Code is being run in standalone mode so should
;                       not display a menu button.
;          showtext   - Show extra buttons to print out a text file
;                       and also stop the program.
;          cwid       - The code should create a self managing
;                       compound-widget with the parent as given in
;                       the value of cwid.
;
;
; OPTIONAL INPUTS:
;       None
;
; OUTPUTS:
;       The function returns a string rep
;           rep = 'MENU'   to leave ADAS811
;           rep = 'CANCEL' user want to go back a screen or the user
;                          closes the widget with the mouse. 
;
; OPTIONAL OUTPUTS:
;       None
;
; KEYWORD PARAMETERS:
;       FONT_LARGE - The name of a larger font e.g 'courier_bold14'
;       FONT_SMALL - The name of a smaller font e.g 'courier_bold12'
;
; CALLS:
;
;
; SIDE EFFECTS:
;       Calls routines from idl/read_adf/ and fortran routines from
;       the wrapper series. 
;
; CATEGORY:
;	Widgets
;
; WRITTEN:
;       Allan Whiteford
;
; MODIFIED:
;	1.1	Allan Whiteford
;		 - First release 
;	1.2	Allan Whiteford
;		 - Protection against type not being calculated
;		   can happen if no A-values are present in file
;	1.3	Allan Whiteford
;		 - Allowed for more than 32,768 transitions.
; VERSION:
;       1.1	22-06-2005
;       1.2	09-01-2006
;       1.3	17-02-2009
;
;-
;-----------------------------------------------------------------------------

pro adas812_proc_event,event

	widget_control,event.top,get_uvalue=info
        
        device,get_visual_name=colourtype
        
        if colourtype eq 'TrueColor' then begin
	        black = 0l
        	white = 255l+256l*255l+(256l*256l*255l)
	        red   = 255l+256l*000l+(256l*256l*000l)
        	green = 000l+256l*255l+(256l*256l*000l)
	        blue  = 000l+256l*000l+(256l*256l*255l)
        endif else begin
		tvlct,0,0,0,1
                black=1
		tvlct,0,0,255,2
		blue=2
		tvlct,255,0,0,3	
		red=3
		tvlct,0,255,0,4
                green=4
        	tvlct,255,255,255,5
                white=5
        endelse
                
        redraw=0
        draw_ups=0
	new_didx=0

	if (event.id eq -999) then begin
        	redraw=1
                new_didx=1
        endif

	if event.id eq info.textid then begin
        
		text=dialog_pickfile(/write)
        
        	openw,unit,text,/get_lun
                	
                        stop
                        
		ups1=*info.ups1
		ups2=*info.ups2
                        
		for i=0,(size(ups1))[1]-1 do printf,unit,ups1[i],ups2[i]
                
                free_lun,unit
        
        endif

	if event.id eq info.draw2 then begin
		if event.type ne 0 then return
                if event.press gt 1 then begin
			upslog=info.upslog
                        cplot=info.cplot
			if upslog eq 1 then begin
				info.upslog=0
				info.cplot=1
                        endif
			if upslog eq 0 and cplot eq 1 then begin
				info.upslog=0
				info.cplot=0
                        endif
			if upslog eq 0 and cplot eq 0 then begin
				info.upslog=1
				info.cplot=0
                        endif

        	        widget_control,event.top,set_uvalue=info
			draw_ups=1
		endif
                if event.press eq 1 then begin
			if info.selected eq -1 then return
                	!x=*info.xaxis2
			!y=*info.yaxis2
			!p=*info.pdata2
			point=convert_coord(event.x,event.y,/device,/to_data)
	                widget_control,info.draw2,get_value=wid
			wset,wid
			temp=point[0]
			widget_control,info.but_k,get_uvalue=tunit

			if tunit eq 'r' then begin
				temp=temp*((info.data1.iz+1.0)^2d0)        
	                endif
                
	                if tunit eq 'e' then begin
				temp=temp*11607d0      
	                endif
                
        	        if tunit eq 'l' then begin
	                	temp=10d0^temp
	                endif

			junk=min(abs(temp-info.common_temp),idx)
                        
                        widget_control,info.temp_sel,set_droplist_select=idx
			redraw=1
                        draw_ups=1
		endif
        endif

	if event.id eq info.but_k then begin
        	widget_control,info.but_k,set_uvalue='k'
                sel=widget_info(info.temp_sel,/droplist_select)
                widget_control,info.temp_sel,set_value=strtrim(string(info.common_temp),2)
        	widget_control,info.temp_sel,set_droplist_select=sel
		draw_ups=1
        endif
	if event.id eq info.but_e then begin
        	widget_control,info.but_k,set_uvalue='e'
                sel=widget_info(info.temp_sel,/droplist_select)
                widget_control,info.temp_sel,set_value=strtrim(string(info.common_temp / 11607d0),2)
        	widget_control,info.temp_sel,set_droplist_select=sel
		draw_ups=1
        endif
	if event.id eq info.but_l then begin
        	widget_control,info.but_k,set_uvalue='l'
                sel=widget_info(info.temp_sel,/droplist_select)
                widget_control,info.temp_sel,set_value=strtrim(string(alog10(info.common_temp)),2)
        	widget_control,info.temp_sel,set_droplist_select=sel
		draw_ups=1
        endif
	if event.id eq info.but_r then begin
        	widget_control,info.but_k,set_uvalue='r'
                sel=widget_info(info.temp_sel,/droplist_select)
                widget_control,info.temp_sel,set_value=strtrim(string(info.common_temp/((info.data1.iz+1.0)^2d0)),2)
        	widget_control,info.temp_sel,set_droplist_select=sel
		draw_ups=1
        endif

	if event.id eq info.xmin or event.id eq info.xmax or event.id eq info.ymin or event.id eq info.ymax then begin
        	redraw=1
        endif

	if event.id eq info.draw then begin
		if event.type ne 0 then return
		;widget_control,info.temp_sel_but,get_uvalue=temp_sel
                ;if temp_sel ne 1 then return
                

                !x=*info.xaxis
		!y=*info.yaxis
		!p=*info.pdata
		point=convert_coord(event.x,event.y,/device,/to_data)

	        widget_control,info.draw,get_value=wid
        	wset,wid

		
        	ups1=*info.ups1
	        ups2=*info.ups2


		;stop
                dist=dblarr((size(ups1))[1])
		for i=0,(size(dist))[1]-1 do begin
        		pos=convert_coord(ups1[i],ups2[i],/data,/to_device)
                        dist[i]=(pos[0]-event.x)^2d0 + (pos[1]-event.y)^2d0
	        end
		
                junk=min(dist,idx)
                
;                stop
                
                if event.press gt 1 then begin
			idx2=(where(info.gidx1 eq (*info.didx1)[idx]))[0]
			;stop
                        
                        ;idx2=idx
                        for i=0,(size(info.cfglo))[1]-1 do begin
                        	if (where(*info.cfglo[i] eq idx2))[0] ne -1 then begin
                                	widget_control,info.lower_sel[i],set_uvalue=1
                                	widget_control,info.lower_sel[i],set_button=1
                                endif
                        end
			for i=0,(size(info.cfghi))[1]-1 do begin
                        	if (where(*info.cfghi[i] eq idx2))[0] ne -1 then begin
                                	widget_control,info.upper_sel[i],set_uvalue=1
                                	widget_control,info.upper_sel[i],set_button=1
                                endif
                        
                        end
			new_didx=1
                
                endif else begin
                
			oplot,[ups1[idx]],[ups2[idx]],psym=6,color=blue
                	info.selected=idx
			widget_control,event.top,set_uvalue=info
			draw_ups=1
       		
                endelse
                
		redraw=1
      
        endif

	if event.id eq info.up_button then begin
        	a=widget_info(info.temp_sel,/droplist_select)
                b=widget_info(info.temp_sel,/droplist_number)
                if a lt b-1 then widget_control,info.temp_sel,set_droplist_select=a+1
                redraw=1
		draw_ups=1
        endif

	if event.id eq info.down_button then begin
        	a=widget_info(info.temp_sel,/droplist_select)
                if a gt 0 then widget_control,info.temp_sel,set_droplist_select=a-1
		redraw=1
		draw_ups=1
        endif

	if event.id eq info.temp_sel then begin
        	redraw=1
		draw_ups=1
        endif
        
        if event.id eq info.temp_sel_but then begin        
        	widget_control,info.temp_text_but,set_button=abs(event.select-1)
		widget_control,info.temp_sel_but,set_uvalue=event.select
        	redraw=1
        endif
        
        if event.id eq info.temp_text_but then begin
        	widget_control,info.temp_sel_but,set_button=abs(event.select-1)
		widget_control,info.temp_sel_but,set_uvalue=abs(event.select-1)
        	redraw=1
        endif
 
 	if event.id eq info.dbutton or event.id eq info.pbutton then begin
         	widget_control,event.id,set_uvalue=event.select
        	redraw=1
        endif
        
        if event.id eq info.type1 or event.id eq info.type2 or event.id eq info.type3 or event.id eq info.type4 or event.id eq info.ground_but or event.id eq info.ground_sel or event.id eq info.auto_refresh or event.id eq info.dynamic_scale then begin
        	widget_control,event.id,set_uvalue=event.select
        	new_didx=1
        	redraw=1
        endif
       
	for i=0,(size(info.lower_but))[1]-1 do begin
        	if event.id eq info.lower_but[i] or event.id eq info.upper_but[i] or event.id eq info.lower_sel[i] or event.id eq info.upper_sel[i] then begin
	         	widget_control,event.id,set_uvalue=event.select
	        	redraw=1
                        new_didx=1
                endif        
        end

	if event.id eq info.all_trans then begin
        	widget_control,info.ground_but,set_uvalue=1
        	widget_control,info.ground_but,set_button=1
		for i=0,(size(info.lower_but))[1]-1 do begin
        		widget_control,info.lower_but[i],set_uvalue=1
        		widget_control,info.lower_but[i],set_button=1
        		widget_control,info.upper_but[i],set_uvalue=1
        		widget_control,info.upper_but[i],set_button=1
        	end
		new_didx=1
        endif
        
	if event.id eq info.ground_trans then begin
        	widget_control,info.ground_but,set_uvalue=1
        	widget_control,info.ground_but,set_button=1
		for i=0,(size(info.lower_but))[1]-1 do begin
        		widget_control,info.lower_but[i],set_uvalue=0
        		widget_control,info.lower_but[i],set_button=0
        		widget_control,info.upper_but[i],set_uvalue=1
        		widget_control,info.upper_but[i],set_button=1
        	end
		new_didx=1
	endif
        
	if event.id eq info.complex_trans then begin
        	widget_control,info.ground_but,set_uvalue=0
        	widget_control,info.ground_but,set_button=0
		for i=0,(size(info.lower_but))[1]-1 do begin
        		widget_control,info.lower_but[i],set_uvalue=0
        		widget_control,info.lower_but[i],set_button=0
        		widget_control,info.upper_but[i],set_uvalue=1
        		widget_control,info.upper_but[i],set_button=1
        	end
        	widget_control,info.lower_but[0],set_uvalue=1
        	widget_control,info.lower_but[0],set_button=1
		new_didx=1
                
	endif
        
        widget_control,info.auto_refresh,get_uvalue=autoref
	if (new_didx eq 1 and autoref eq 1) or event.id eq info.manual_refresh or (*info.didx1)[0] eq -1 then begin
                        
                lower_vec=[-1]
                
                widget_control,info.ground_but,get_uvalue=sel
		if sel eq 1 then begin
			vec=*info.grndlo
			if vec[0] ne -1 then lower_vec=[lower_vec,vec]
		endif
                
                for i=0,(size(info.lower_but))[1]-1 do begin
                	widget_control,info.lower_but[i],get_uvalue=sel
                	if sel eq 1 then begin
                        	vec=*info.cfglo[i]
                        	if vec[0] ne -1 then lower_vec=[lower_vec,info.gidx1[vec]]
                        endif
                end
               	
                upper_vec=[-1]
                for i=0,(size(info.upper_but))[1]-1 do begin
                	widget_control,info.upper_but[i],get_uvalue=sel
                	if sel eq 1 then begin
                        	vec=*info.cfghi[i]
                        	if vec[0] ne -1 then upper_vec=[upper_vec,info.gidx1[vec]]
                        endif
                end

                lower_sel=[-1]


                widget_control,info.ground_sel,get_uvalue=sel
		if sel eq 1 then begin
			vec=*info.grndlo
			if vec[0] ne -1 then lower_sel=[lower_sel,vec]
		endif

                for i=0,(size(info.lower_sel))[1]-1 do begin
                	widget_control,info.lower_sel[i],get_uvalue=sel
                	if sel eq 1 then begin
                        	vec=*info.cfglo[i]
                        	if vec[0] ne -1 then lower_sel=[lower_sel,info.gidx1[vec]]
                        endif
                end
               	
                upper_sel=[-1]
                for i=0,(size(info.upper_sel))[1]-1 do begin
                	widget_control,info.upper_sel[i],get_uvalue=sel
                	if sel eq 1 then begin
                        	vec=*info.cfghi[i]
                        	if vec[0] ne -1 then upper_sel=[upper_sel,info.gidx1[vec]]
                        endif
                end


		type_vec=[-1]
                widget_control,info.type1,get_uvalue=sel
                if sel eq 1 then begin
                	vec=where(info.type eq 1)
                        if vec[0] ne -1 then type_vec=[type_vec,info.gidx1[vec]]
                endif
		widget_control,info.type2,get_uvalue=sel
                if sel eq 1 then begin
                	vec=where(info.type eq 2)
                        if vec[0] ne -1 then type_vec=[type_vec,info.gidx1[vec]]
                endif
		widget_control,info.type3,get_uvalue=sel
                if sel eq 1 then begin
                	vec=where(info.type eq 3)
                        if vec[0] ne -1 then type_vec=[type_vec,info.gidx1[vec]]
                endif
		widget_control,info.type4,get_uvalue=sel
                if sel eq 1 then begin
                	vec=where(info.type eq 4)
                        if vec[0] ne -1 then type_vec=[type_vec,info.gidx1[vec]]
                endif
                
                
                if n_elements(lower_vec) eq 1 or n_elements(upper_vec) eq 1 or n_elements(type_vec) eq 1 then begin

			widget_control,info.draw,get_value=wid
                        wset,wid
                        plot,[0],/nodata,background=white,color=black
			return                
                	stop
                
                endif
                
		lower_vec=lower_vec[1:n_elements(lower_vec)-1]
                upper_vec=upper_vec[1:n_elements(upper_vec)-1]
                type_vec=type_vec[1:n_elements(type_vec)-1]

		didx1=[-1]
		didx2=[-1]
		
                ;stop
                
                for i=0,(size(info.gidx1))[1]-1 do begin
                	if (where(info.gidx1[i] eq lower_vec))[0] ne -1 and (where(info.gidx1[i] eq upper_vec))[0] ne -1 and (where(info.gidx1[i] eq type_vec))[0] ne -1  then begin
                        	didx1=[didx1,info.gidx1[i]]
                        	didx2=[didx2,info.gidx2[i]]
                        endif
                
                end

		if n_elements(didx1) gt 1 then begin
        	        didx1=didx1[1:n_elements(didx1)-1]
			didx2=didx2[1:n_elements(didx2)-1]
		endif

		;stop
              
		ptr_free,info.didx1
		ptr_free,info.didx2
		info.didx1=ptr_new(didx1)
		info.didx2=ptr_new(didx2)
                
;                print,didx1
                
;                stop
                
		sidx1=[-1]
		sidx2=[-1]
                
                if n_elements(lower_sel) gt 1 and n_elements(upper_sel) gt 1 then begin
			lower_sel=lower_sel[1:n_elements(lower_sel)-1]
                	upper_sel=upper_sel[1:n_elements(upper_sel)-1]
                
			for i=0,(size(info.gidx1))[1]-1 do begin
                		if (where(info.gidx1[i] eq lower_sel))[0] ne -1 and (where(info.gidx1[i] eq upper_sel))[0] ne -1 and (where(info.gidx1[i] eq type_vec))[0] ne -1  then begin
                        		sidx1=[sidx1,info.gidx1[i]]
                        		sidx2=[sidx2,info.gidx2[i]]
                        	endif
	                end

			sidx1=sidx1[1:n_elements(sidx1)-1]
			sidx2=sidx2[1:n_elements(sidx2)-1]
                endif 
                
		ptr_free,info.sidx1
		ptr_free,info.sidx2
		info.sidx1=ptr_new(sidx1)
		info.sidx2=ptr_new(sidx2)
                
                info.selected=-1
        	
                redraw=1
        
        endif


	if redraw eq 1 then begin
        	; get data
                
                
                tidx=widget_info(info.temp_sel,/droplist_select)
                temp=info.common_temp[tidx]
        
        	idx1=where(info.data1.te eq temp)
        	idx2=where(info.data2.te eq temp)
        
 		didx1=*info.didx1
 		didx2=*info.didx2
                
        	ups1=info.data1.gamma[didx1,idx1[0]]
        	ups2=info.data2.gamma[didx2,idx2[0]]

 		
                
                
        
        	widget_control,info.draw,get_value=wid
                wset,wid
                
                
                xrange=[info.mind,info.maxd]
                yrange=[info.mind,info.maxd]

		widget_control,info.dynamic_scale,get_uvalue=dyn
                if dyn eq 1 then begin
                	;stop
                        xrange=[min(ups1[where(ups1 gt 1d-30)]) < min(ups2[where(ups2 gt 1d-30)]),max(ups1) < max(ups2)]
                        yrange=xrange
                endif
                
                widget_control,info.xmin,get_value=xmin
                xmin=double(xmin[0])
                if xmin ne 0 then xrange[0]=xmin
                widget_control,info.xmax,get_value=xmax
                xmax=double(xmax[0])
                if xmax ne 0 then xrange[1]=xmax
                widget_control,info.ymin,get_value=ymin
                ymin=double(ymin[0])
                if ymin ne 0 then yrange[0]=ymin
                widget_control,info.ymax,get_value=ymax
                ymax=double(ymax[0])
                if ymax ne 0 then yrange[1]=ymax
                
                ;print,xrange
                
                isotropic=0
                if xrange[0] eq yrange[0] and xrange[1] eq yrange[1] then isotropic=1

		if (size(ups1))[0] eq 0 then begin
                	ups1=[ups1]
                	ups2=[ups2]
                endif
                
                plot,[0],/nodata,xrange=xrange,yrange=yrange,isotropic=isotropic,/xlog,/ylog,xtitle=info.xtitle,ytitle=info.ytitle,background=white,color=black
                oplot,ups1,ups2,psym=1,color=black

                sidx1=*info.sidx1
 		sidx2=*info.sidx2
                
                if sidx1[0] ne -1 then begin                
	        	sups1=info.data1.gamma[sidx1,idx1[0]]
        		sups2=info.data2.gamma[sidx2,idx2[0]]		
			if (size(sups1))[0] eq 0 then begin
				sups1=[sups1]
				sups2=[sups2]
                        endif
                        oplot,sups1,sups2,psym=1,color=red
                endif

;                stop
                
                if info.selected ne -1 then begin
			oplot,[ups1[info.selected]],[ups2[info.selected]],psym=6,color=blue
                endif
                
                ptr_free,info.ups1
                ptr_free,info.ups2
                info.ups1=ptr_new(ups1)
                info.ups2=ptr_new(ups2)
                
                widget_control,info.pbutton,get_uvalue=pbutton_press
                widget_control,info.dbutton,get_uvalue=dbutton_press
        	
                line=[1e-40,1e10]
                
                if dbutton_press eq 1 then oplot,line,line,color=blue
        	                
                if pbutton_press eq 1 then begin
        	        widget_control,info.percent_bars,get_value=percent
	                percent=float(percent[0])
	        	if (percent gt 0 and pbutton_press) then begin
                		oplot,line,line*(1d0+percent/100d0),color=blue
        	        	oplot,line,line/(1d0+percent/100d0),color=blue
	                endif
        	endif
        ;        stop
        
        	ptr_free,info.xaxis
        	ptr_free,info.yaxis
        	ptr_free,info.pdata
   		info.xaxis=ptr_new(!x)
		info.yaxis=ptr_new(!y)
		info.pdata=ptr_new(!p)
                ;stop
		widget_control,event.top,set_uvalue=info             
        
        endif
        
        if draw_ups and info.selected ne -1 then begin
        
                widget_control,info.draw2,get_value=wid
                wset,wid

		plot1=info.data1.gamma[(*info.didx1)[info.selected],*] 
		plot2=info.data2.gamma[(*info.didx2)[info.selected],*] 

		widget_control,info.but_k,get_uvalue=tunit

		x1=info.data1.te
		x2=info.data2.te
		xlog=1

                index1=info.data1.lower[(*info.didx1)[info.selected]]-1
                index2=info.data1.upper[(*info.didx1)[info.selected]]-1
                index3=info.data2.lower[(*info.didx2)[info.selected]]-1
                index4=info.data2.upper[(*info.didx2)[info.selected]]-1

		;print,index1,index2,index3,index4

		if tunit eq 'r' then begin
			x1=x1/((info.data1.iz+1.0)^2d0)        
			x2=x2/((info.data2.iz+1.0)^2d0)              
                endif
                
                if tunit eq 'e' then begin
			x1=x1/11607d0      
			x2=x2/11607d0                
                endif
                
                if tunit eq 'l' then begin
                	x1=alog10(x1)
                        x2=alog10(x2)
                        xlog=0
                endif
		
		if info.cplot then begin
                	type=info.type[(*info.didx1)[info.selected]]
                        
                        ;print,type
                        
                        burgspace,x1,reform(plot1),xb1,yb1,info.data1.wa[index2]-info.data1.wa[index1],type=type,avalue=info.data1.aval[(*info.didx1)[info.selected]],stat_wt=1d0+2*info.data1.xja[index2],born=info.data1.beth[(*info.didx1)[info.selected]]
                	burgspace,x2,reform(plot2),xb2,yb2,info.data2.wa[index4]-info.data2.wa[index3],type=type,avalue=info.data2.aval[(*info.didx2)[info.selected]],stat_wt=1d0+2*info.data2.xja[index4],born=info.data1.beth[(*info.didx2)[info.selected]]

			xrange=[0,1]
                        yrange=[min(yb1) < min(yb2),max(yb1) > max(yb2)]
			plot,[0],/nodata,xrange=xrange,yrange=yrange,xtitle='X (type '+ strtrim(string(type),2) +' transition)',ytitle='Y',title='Solid line: '+info.xtitle,background=white,color=black
        	        oplot,xb1,yb1,color=black
			oplot,xb2,yb2,linestyle=2,color=black

                        ;stop
			                
                endif else begin

	                xrange=[min([x1,x2]),max([x1,x2])]
        	        yrange=[ min(plot1) < min(plot2) , max(plot1) > max(plot2) ]
	                plot,[0],/nodata,xrange=xrange,yrange=yrange,xlog=xlog,ylog=info.upslog,xtitle='Temperature',ytitle='Upsilon',title='Solid line: '+info.xtitle,background=white,color=black
        	        oplot,x1,plot1,color=black
			oplot,x2,plot2,linestyle=2,color=black
                                
	                tidx=widget_info(info.temp_sel,/droplist_select)
        	        temp=info.common_temp[tidx]
                
			if tunit eq 'r' then begin
				temp=temp/((info.data1.iz+1.0)^2d0)        
	                endif
                
	                if tunit eq 'e' then begin
				temp=temp/11607d0      
	                endif
                
	                if tunit eq 'l' then begin
        	        	temp=alog10(temp)
	                endif

	                if info.upslog eq 1 then begin
        	        	oplot,[temp,temp],[yrange[0]/1d3,yrange[1]*1d3],linestyle=3,color=black
	                endif else begin
		                oplot,[temp,temp],[0,yrange[1]*2],linestyle=3,color=black
	                endelse
                endelse
                
                
                lvals=['S','P','D','F','G']
                
                strshow1=strtrim(info.data1.cstrga[index1],2) + ' (' + strtrim(string(info.data1.isa[index1]),2) + ')' + lvals[info.data1.ila[index1]] + '(' + strtrim(string(info.data1.xja[index1],format='(F8.1)'),2) + ') ['+ strtrim(string(info.data1.lower[(*info.didx1)[info.selected]]),2) +'] - [' + strtrim(string(info.data1.upper[(*info.didx1)[info.selected]]),2) + '] '+ strtrim(info.data1.cstrga[index2],2) + ' (' + strtrim(string(info.data1.isa[index2]),2) + ')' + lvals[info.data1.ila[index2]] + '(' + strtrim(string(info.data1.xja[index2],format='(F8.1)'),2) + ')'
                strshow2=strtrim(info.data2.cstrga[index3],2) + ' (' + strtrim(string(info.data2.isa[index3]),2) + ')' + lvals[info.data2.ila[index3]] + '(' + strtrim(string(info.data2.xja[index3],format='(F8.1)'),2) + ') ['+ strtrim(string(info.data2.lower[(*info.didx2)[info.selected]]),2) +'] - [' + strtrim(string(info.data2.upper[(*info.didx2)[info.selected]]),2) + '] '+ strtrim(info.data2.cstrga[index4],2) + ' (' + strtrim(string(info.data2.isa[index4]),2) + ')' + lvals[info.data2.ila[index4]] + '(' + strtrim(string(info.data2.xja[index4],format='(F8.1)'),2) + ')'
                
                widget_control,info.label1,set_value=strshow1
                widget_control,info.label2,set_value=strshow2
        	ptr_free,info.xaxis2
        	ptr_free,info.yaxis2
        	ptr_free,info.pdata2
   		info.xaxis2=ptr_new(!x)
		info.yaxis2=ptr_new(!y)
		info.pdata2=ptr_new(!p)
         
 		widget_control,event.top,set_uvalue=info       
        endif
        

	if event.id eq info.cancelID then begin
		widget_Control, event.top, /destroy
        endif

	if event.id eq info.menuID then begin
        	(*info.ptrtodata).cancel=0
        	(*info.ptrtodata).menu=1
 		widget_Control, event.top, /destroy
        endif

end

FUNCTION adas812_proc, file1, file2,                        $  
                       bitfile, devlist, devcode, adasrel, adasprog, $
                       FONT_LARGE = font_large,                      $
                       FONT_SMALL = font_small,                      $
                       standalone = standalone,                      $
                       showtext   = showtext  ,                      $
                       cwid       = cwid


; Set defaults for keywords

  IF n_elements(font_large) eq 0 THEN font_large = ''
  IF n_elements(font_small) eq 0 THEN font_small = ''

                         
; Read in the files into an array of pointers

  
  read_adf04, file=file1, fulldata=data1
  read_adf04, file=file2, fulldata=data2

;stop

  adas812_match_levels,data1,data2,idx1,idx2 
  for i=0,(size(data1.te))[1]-1 do if (where(data1.te[i] eq data2.te))[0] ne -1 then common_temp = (n_elements(common_temp) gt 0 ? [common_temp,data1.te[i]] : [data1.te[i]])


  gidx1=[-1]
  gidx2=[-1]
  
  for i=0l,(size(data1.gamma))[1]-1 do begin
          a=where(data1.lower[i]-1 eq idx1)
          b=where(data1.upper[i]-1 eq idx1)
  
          if a[0] ne -1 and b[0] ne -1 then begin
                  c=where(idx2[a[0]]+1 eq data2.lower and idx2[b[0]]+1 eq data2.upper)
                  if c[0] ne -1 then begin
	                  gidx1=[gidx1,i]
        	          gidx2=[gidx2,c[0]]
                  endif
                  ;print,i,c[0]
          endif
  
  end

  gidx1=gidx1[1:n_elements(gidx1)-1]
  gidx2=gidx2[1:n_elements(gidx2)-1]


  ranged=[reform(data1.gamma[gidx1,*],n_elements(data1.gamma[gidx1,*])),reform(data2.gamma[gidx2,*],n_elements(data2.gamma[gidx1,*]))]
  idx=where(ranged gt 1d-30)
  mind=min(ranged[idx])
  maxd=max(ranged[idx])

  tlb=widget_base(title='Comparison of adf04 files',/column)

  controlbase=widget_base(tlb,/row)

  base2=widget_base(controlbase,/column)
  draw=widget_draw(base2,xsize=600,ysize=600,/button_events)

  selbase=widget_base(tlb,/row)
  tbase=widget_base(selbase,/column)
  tbase2=widget_base(tbase,/row)
  bbase=widget_base(tbase2,/exclusive)
  temp_sel_but=widget_button(bbase,value='Common temperature: ',uvalue=1)
  widget_control,temp_sel_but,/set_button
  temp_sel=widget_droplist(tbase2,value=strtrim(string(common_temp),2))
  down_button=widget_button(tbase2,value='Down')
  up_button=widget_button(tbase2,value='Up')
  tbase2=widget_base(tbase,/row)
;  bbase=widget_base(tbase2,/exclusive)
;  temp_text_but=widget_button(bbase,value='Splined temperature:')
;  temp_text=widget_text(tbase2,value='',uvalue='',/editable,/all_events)
  
  temp_text_but=-1;
  temp_text=-1;
  
  gbase=widget_base(selbase,/column)
  lbase=widget_base(gbase,column=2,/exclusive)
  but_k=widget_button(lbase,value='Kelvin',uvalue='k')
  but_l=widget_button(lbase,value='Log Kelvin')
  but_e=widget_button(lbase,value='eV')
  but_r=widget_button(lbase,value='Reduced')
  
  widget_control,but_k,/set_button

  bbase=widget_base(gbase,/nonexclusive)
  dynamic_scale=widget_button(bbase,value='Dynamic Scale',uvalue=1)
  widget_control,dynamic_scale,/set_button
  
  lbase=widget_base(selbase,/column)
  
  bbase=widget_base(lbase,/nonexclusive)
  dbutton=widget_button(bbase,value='Display reference line',uvalue=1)
  widget_control,dbutton,/set_button
  pbase=widget_base(lbase,/row)
  bbase=widget_base(pbase,/nonexclusive)
  pbutton=widget_button(bbase,value='Display error lines',uvalue=0)
  percent_bars=widget_text(pbase,value='30',uvalue='30',/editable,/all_events,xsize=5)
  label=widget_label(pbase,value='%')

  rangebase=widget_base(selbase,/column)
  xbase=widget_base(rangebase,/row)
  label=widget_label(xbase,value='X-range:')
  xmin=widget_text(xbase,/editable,/all_events,xsize=6)
  xmax=widget_text(xbase,/editable,/all_events,xsize=6)
  ybase=widget_base(rangebase,/row)
  label=widget_label(ybase,value='Y-range:')
  ymin=widget_text(ybase,/editable,/all_events,xsize=6)
  ymax=widget_text(ybase,/editable,/all_events,xsize=6)
  
  rbase=widget_base(controlbase,/column)
  draw2=widget_draw(rbase,xsize=400,ysize=200,/button_events)
  label1=widget_label(rbase,value=' ',/dynamic_resize)
  label2=widget_label(rbase,value=' ',/dynamic_resize)
  
  filterbase=widget_base(rbase,/column,/frame)
  gbase=widget_base(filterbase,/row)
  hbase=widget_base(gbase,/column)
  bbase=widget_base(hbase,/nonexclusive)
  auto_refresh=widget_button(bbase,value='Auto Refresh',uvalue=1)
  widget_control,auto_refresh,/set_button
  manual_refresh=widget_button(hbase,value='Manual Refresh')
  all_trans=widget_button(hbase,value='All Transitions')
  ground_trans=widget_button(hbase,value='Transitions from ground level')
  complex_trans=widget_button(hbase,value='Transitions from ground complex')

  ibase=widget_base(gbase,/column,/frame)
  label=widget_label(ibase,value='Transition type')
  typebase=widget_base(ibase,/nonexclusive)
  type1=widget_button(typebase,value='Type 1 transitions',uvalue=1)
  type2=widget_button(typebase,value='Type 2 transitions',uvalue=1)
  type3=widget_button(typebase,value='Type 3 transitions',uvalue=1)
  type4=widget_button(typebase,value='Type 4 transitions',uvalue=1)
  widget_control,type1,/set_button
  widget_control,type2,/set_button
  widget_control,type3,/set_button
  widget_control,type4,/set_button
  
;  label=widget_label(filterbase,value='Filter by configurations')
  cfgbase=widget_base(filterbase,/row)
  
  ; get unique configurations
  
  cfgs=data1.cstrga[idx1]
  
  list=strtrim(cfgs[0],2)
  for i=1,(size(cfgs))[1]-1 do if (where(list eq strtrim(cfgs[i],2)))[0] eq -1 then list=[list,strtrim(cfgs[i],2)]

  ; build vectors of transitions with same lower configuration
  
  cfglo=ptrarr((size(list))[1])
  cfghi=ptrarr((size(list))[1])

  for i=0,(size(list))[1]-1 do begin
  	vec=where(strtrim(data1.cstrga[data1.lower[gidx1]-1],2) eq list[i])
	cfglo[i]=ptr_new(vec)
  	vec=where(strtrim(data1.cstrga[data1.upper[gidx1]-1],2) eq list[i])
	cfghi[i]=ptr_new(vec)
  end

  ;stop

  vec=where(data1.lower[gidx1] eq 1)
  grndlo=ptr_new(vec)

;  stop
  
  temp=widget_base(/column)
  a=widget_label(temp,value='Ground')
  a=widget_info(a,/geom)
  lsize=a.scr_xsize
  for i=0,(size(list))[1]-1 do begin
    a=widget_label(temp,value=list[i])
    a=widget_info(a,/geom)
    if a.scr_xsize gt lsize then lsize=a.scr_xsize
  end

  cfgbase2=widget_base(cfgbase,/column)
  label=widget_label(cfgbase2,value='Lower state')

  scrollbase=widget_base(cfgbase2,/scroll)
  listbase=widget_base(scrollbase,/column)
  
  lower_but=lonarr((size(list))[1])
  upper_but=lonarr((size(list))[1])
  lower_sel=lonarr((size(list))[1])
  upper_sel=lonarr((size(list))[1])

  ibase=widget_base(listbase,/row)
  label=widget_label(ibase,value='Ground',scr_xsize=lsize)
  bbase=widget_base(ibase,/row,/nonexclusive)
  ground_but=widget_button(bbase,value=' ',uvalue=1)
  widget_control,ground_but,/set_button
  ground_sel=widget_button(bbase,value=' ',uvalue=0)
  for i=0,(size(list))[1]-1 do begin
  	ibase=widget_base(listbase,/row)
        label=widget_label(ibase,value=list[i],scr_xsize=lsize)
	bbase=widget_base(ibase,/row,/nonexclusive)
        lower_but[i]=widget_button(bbase,value=' ',uvalue=0)
        ;widget_control,lower_but[i],/set_button
	lower_sel[i]=widget_button(bbase,value=' ',uvalue=0)
  end

  a=widget_info(ibase,/geom)
  widget_control,scrollbase,scr_xsize=a.scr_xsize*1.3,scr_ysize=a.scr_ysize*4.3

  cfgbase2=widget_base(cfgbase,/column)
  label=widget_label(cfgbase2,value='Upper state')

  scrollbase=widget_base(cfgbase2,/scroll)
  listbase=widget_base(scrollbase,/column)

  for i=0,(size(list))[1]-1 do begin
  	ibase=widget_base(listbase,/row)
	label=widget_label(ibase,value=list[i],scr_xsize=lsize)
	bbase=widget_base(ibase,/row,/nonexclusive)
	upper_but[i]=widget_button(bbase,value=' ',uvalue=1)
        widget_control,upper_but[i],/set_button
	upper_sel[i]=widget_button(bbase,value=' ',uvalue=0)
  end

  a=widget_info(ibase,/geom)
  widget_control,scrollbase,scr_xsize=a.scr_xsize*1.3,scr_ysize=a.scr_ysize*4.3


  ;stop
  

  ; figure out type of each transition
  
  type=intarr((size(gidx1))[1])
  
  fzero = 1.0E-4
  fbig  = 0.01   
  
  for i=0,(size(gidx1))[1]-1 do begin
	isal=data1.isa[data1.lower[gidx1[i]]-1]
        isau=data1.isa[data1.upper[gidx1[i]]-1]
        ilal=data1.ila[data1.lower[gidx1[i]]-1]
        ilau=data1.ila[data1.upper[gidx1[i]]-1]
        wvl=data1.wa[data1.lower[gidx1[i]]-1]
        wvu=data1.wa[data1.upper[gidx1[i]]-1]
  	wtl=1.0 + 2.0*data1.xja[data1.lower[gidx1[i]]-1]
        
        fin=(1d0/3d0) * ( abs (wvu-wvl) / 109737.26d0 ) / wtl 
        
        if isal eq isau then begin
        	if abs(ilal-ilau) le 1 and fin gt fbig then begin
                	type[i]=1
                endif else begin
			type[i]=2
                endelse        
        endif else begin
        	if fin gt fzero and fin lt fbig then begin
                	type[i]=4
                endif else begin
                	type[i]=3
                endelse     
        endelse
  
  
  end
  
  menufile = bitfile + '/menu.bmp'
  read_X11_bitmap, menufile, bitmap1

  
  if not keyword_set(cwid) then begin              
    mrow        = widget_base(tlb,/row)
    if not keyword_set(standalone) then begin
      menuID      = widget_button(mrow,value=bitmap1,font=font_large)
      canceltext='Cancel'
    endif else begin
      menuID = -1
      canceltext='Close'
    endelse 
  
  
    cancelID    = widget_button(mrow,value=canceltext,font=font_large)
    ;printID     = widget_button(mrow,value='Print',font=font_large)
    printID = -1
    if keyword_set(showtext) then begin
       textID      = widget_button(mrow,value='Text File',font=font_large)
       stopID      = widget_button(mrow,value='Stop',font=font_large,event_pro='adas_stop')
    endif else begin
       textID = -1
       stopID = -1
    endelse

    mskip     = Widget_label(mrow,value='             ',font=font_small)
  endif else begin
    mrow       = fbase
    cancelID   = -1
    printID    = -1
    textID = -1
  endelse

  xtitle=strmid(file1,strpos(file1,'/',/reverse_search)+1)
  ytitle=strmid(file2,strpos(file2,'/',/reverse_search)+1)
  
  ptrtodata=Ptr_New({cancel:1, menu:0})

  info={temp_sel:temp_sel,draw:draw,data1:data1,data2:data2,idx1:idx1,idx2:idx2,common_temp:common_temp,temp_text:temp_text,temp_sel_but:temp_sel_but,temp_text_but:temp_text_but,mind:mind,maxd:maxd,percent_bars:percent_bars,dbutton:dbutton,pbutton:pbutton,xaxis:ptr_new(0),yaxis:ptr_new(0),xaxis2:ptr_new(0),yaxis2:ptr_new(0),selected:-1,draw2:draw2,pdata:ptr_new(0),pdata2:ptr_new(0),label1:label1,label2:label2,gidx1:gidx1,gidx2:gidx2,cfglo:cfglo,cfghi:cfghi,ups1:ptr_new(0),ups2:ptr_new(0),lower_but:lower_but,upper_but:upper_but,lower_sel:lower_sel,upper_sel:upper_sel,type1:type1,type2:type2,type3:type3,type4:type4,didx1:ptr_new([-1]),didx2:ptr_new(-1),sidx1:ptr_new([-1]),sidx2:ptr_new(-1),down_button:down_button,up_button:up_button,upslog:0,xtitle:xtitle,ytitle:ytitle,type:type,xmin:xmin,xmax:xmax,ymin:ymin,ymax:ymax,but_k:but_k,but_e:but_e,but_r:but_r,but_l:but_l,menuid:menuid,cancelID:cancelID,printID:printID,textID:textID,ptrtodata:ptrtodata,ground_but:ground_but,ground_sel:ground_sel,grndlo:grndlo,auto_refresh:auto_refresh,manual_refresh:manual_refresh,all_trans:all_trans,ground_trans:ground_trans,complex_trans:complex_trans,cplot:0,dynamic_scale:dynamic_scale}

  widget_control, tlb, set_uvalue=info
  widget_control,tlb,/realize
  adas812_proc_event,{top:tlb,id:-999}

  xmanager, 'adas812_proc', tlb, event_handler='adas812_proc_event'

  data=*ptrtodata
  
  if data.cancel eq 1 then begin
  	return,'CANCEL'
  endif

  if data.menu eq 1 then begin
  	return,'MENU'
  endif




END
