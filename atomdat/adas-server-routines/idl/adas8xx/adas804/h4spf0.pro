; SCCS info: Module @(#)$Header: /home/adascvs/idl/adas8xx/adas804/h4spf0.pro,v 1.1 2004/07/06 14:01:00 whitefor Exp $ Date $Date: 2004/07/06 14:01:00 $
;+
; project:
;       adas ibm mvs to unix conversion
;
; name:
;	h4spf0
;
; purpose:
;	idl user interface and communications with adas804 fortran
;	process via pipe.
;
; explanation:
;	Firstly this routine invokes the part of the user interface
;	used to select the input archive for adas804.  When the user's
;	interactions are complete this routine communicates with the
;	adas804 fortran application via a unix pipe.  Communications
;	are to the fortran subroutine h4spf0.
;
; use:
;	the use of this routine is specific to adas804, see adas804.pro.
;
; inputs:
;	pipe	- the idl unit number of the bi-directional pipe to the
;		  adas804 fortran process.
;
;	value	- a structure which determines the initial settings of
;		  the archive selection widget.  the initial value is
;		  set in adas804.pro.  value is passed un-modified
;		  through to cw_adas_in.pro, see that routine for a full
;		  description.
;
; optional inputs:
;	none
;
; outputs:
;	value	- on output the structure records the final settings of
;		  the dataset selection widget if the user pressed the
;		  'done' button otherwise it is not changed from input.
;
;	dsfull	- string; the full system file name of the input 
;		  archive selected by the user for processing.
;
;	rep	- string; indicates whether the user pressed the 'done'
;		  or 'cancel' button on the interface.  the action is
;		  converted to the strings 'no' and 'yes' respectively
;		  to match up with the existing fortran code.  in the
;		  original ibm ispf interface rep was the reply to the
;		  question 'do you want to end program execution'.
;
;       lchoice - integer; records the archiving option selected by the 
;                 user for future processing.
;                 0 - no archive
;                 1 - examine old archive
;                 2 - refresh index from old archive
;                 3 - create new archive
;
; optional outputs:
;	none
;
; keyword parameters:
;	font	- the name of the font to be used for the 
;                 interface widgets.
;
; calls:
;	adas_in		pops-up the archive selection widget.
;
; side effects:
;       none
;
; category:
;	adas system.
;
; written:
;       hugh summers, univ.of strathclyde, 08-june-2002
;
; version:
;       1.1     hugh p summers
;		first release
;	
; modified:
;	1.1	08-06-02
;-
;-----------------------------------------------------------------------------


pro h4spf0, pipe, value, dsfull, lchoice, rep, font=font

		;***********************************
                ;**** set defaults for keywords ****
		;***********************************

    if not (keyword_set(font)) then font = ''

                ;***********************************
                ;**** reset value.refresh to '' ****
                ;**** so as to simplify choices ****
                ;***********************************

    value.refresh = ''

		;**********************************
		;**** pop-up input file widget ****
		;**********************************

    adas804_in, value, action, wintitle = 'ADAS804 INPUT', 		$
	        title = 'Input archive file', font = font

		;********************************************
		;**** act on the event from the widget   ****
		;**** there are only two possible events ****
		;**** 'done' and 'cancel'.               ****
		;********************************************

    if action eq 'done' then begin
        rep = 'no'
    endif else begin
        rep = 'yes'
    endelse

		;********************************
		;**** construct dataset name ****
		;********************************

    dsfull = strcompress(value.rootpath+value.file)

		;*****************************************
                ;**** set up flag for archive choice  ****
                ;**** 0 (no archive), 1 (old archive) ****
                ;**** 2 (refresh)   , 3 (new archive) ****
		;*****************************************
		
    if lchoice eq -1 then begin
        if value.file eq '' then begin
            lchoice = 0
        endif else begin
            openr, /get_lun, tempunit, dsfull

                ;**** check on the size of the file  ****
                ;****if just created it will be empty****

            status = fstat(tempunit)
            if status.size ne 0 then begin
                if value.refresh eq '' then begin
                    lchoice = 1
                endif else begin
                    lchoice = 2
                endelse
            endif else begin
                lchoice = 3
            endelse
            free_lun, tempunit
        endelse
    endif
    

		;*******************************
                ;*** write dsname and choice ***
                ;***      to fortran         ***
		;*******************************
    
    printf, pipe, format = '(1a3)', rep
    printf, pipe, format = '(i5)', lchoice
    if lchoice ne 0 then begin

        printf, pipe, dsfull
        if lchoice eq 2 then begin
            printf, pipe, format = '(i5)', long(value.refresh) 
        endif
    endif

end
