; SCCS info: Module @(#)$Header: /home/adascvs/idl/adas8xx/adas804/adas804_in.pro,v 1.2 2010/11/30 07:58:00 mog Exp $ Date $Date: 2010/11/30 07:58:00 $
;+
; project:
;       adas ibm mvs to dec unix conversion
;
; name:
;	adas804_in
;
; purpose:
;	idl adas user interface, input file selection.
;
; explanation:
;	this routine creates and manages a pop-up window which allows
;	the user to select a file from the unix file system.  it is
;	intended as a general routine and hence the widget titles may
;	be passed into this routine using keywords.
;
; use:
;	see the adas205 routine b5spf0.pro for an example of how to
;	use this routine.
;
; inputs:
;	val	- a structure which determines the initial settings of
;		  the dataset selection widget.  value is passed un-modified
;		  through to cw_adas804_in.pro, see that routine for a full
;		  description.
;
; optional inputs:
;	none
;
; outputs:
;	val	- on output the structure records the final settings of
;		  the dataset selection widget if the user pressed the
;		  'done' button otherwise it is not changed from input.
;
;	act	- a string; indicates the user's action when the pop-up
;		  window is terminated, i.e which button was pressed to
;		  complete the input.  possible values are 'done' and
;		  'cancel'.
;
; optional outputs:
;	none
;
; keyword parameters:
;	wintitle- a title to be used on the banner for the pop-up window.
;		  intended to indicate which application is running,
;		  e.g 'adas 205 input'
;
;	title	- the title to be included in the input file widget, used
;		  to indicate exactly what the required input dataset is,
;		  e.g 'input copase dataset'
;
;	font	- supplies the font to be used for the interface widgets.
;
; calls:
;	cw_adas804_in	dataset selection widget creation.
;	xmanager	manages the pop=up window.
;	adas804_in_ev	event manager, called indirectly during xmanager
;			event management.
;
; side effects:
;	xmanager is called in /modal mode. any other widget become
;	inactive.
;
; category:
;	adas system.
;
; written:
;       hugh summers, univ.of strathclyde, 08-june-2002
;
; version:
;       1.1     hugh p summers
;		first release
;	1.2    	Martin O'Mullane
;		Rename common block in_blk to in804_blk.
;	
; modified:
;	1.1	08-06-02
;-
;-----------------------------------------------------------------------------


pro adas804_in_ev, event

    common in804_blk,action,value

		;**** find the event type and copy to common ****

    action = event.action

    case action of

		;**** 'done' button ****

	'done'  : begin

			;**** get the output widget value ****

  		  widget_control,event.id,get_value=value 
		  widget_control,event.top,/destroy

         end


		;**** 'cancel' button ****

	'cancel': widget_control, event.top, /destroy

    end

end

;-----------------------------------------------------------------------------


pro adas804_in, val, act, wintitle = wintitle, title = title, font = font

    common in804_blk,action,value

		;**** copy value to common ****

    value = val

		;**** set defaults for keywords ****

    if not (keyword_set(wintitle)) then wintitle = 'adas input file'
    if not (keyword_set(title)) then title = ''
    if not (keyword_set(font)) then font = ''

		;***************************************
		;**** pop-up a new widget if needed ****
		;***************************************

                ;**** create base widget ****

    inid = widget_base(title=wintitle,xoffset=200,yoffset=0)

		;**** declare output options widget ****

    cwid = cw_adas804_in(inid, value=value, title=title, font=font)

		;**** realize the new widget ****

    dynlabel, inid
    widget_control,inid,/realize

		;**** make widget modal ****

    xmanager,'adas804_in',inid,event_handler='adas804_in_ev',/modal,/just_reg
 
		;**** return the output value from common ****

    act = action
    val = value

end

