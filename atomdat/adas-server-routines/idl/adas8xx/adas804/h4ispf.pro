;+
; Project:
;       ADAS
;
; Name:
;       h4ispf
;
; Purpose:
;       idl user interface and communications with adas804 fortran
;       process via pipe.
;
; Explanation:
;       the routine begins by reading information from the adas804
;       fortran process via a unix pipe.  then part of the adas804
;       idl user interface is invoked to determine how the user
;       wishes to process the input dataset.  when the user's
;       interactions are complete the information gathered with
;       the user interface is written back to the fortran process
;       via the pipe.  communications are to the fortran subroutine
;       h4ispf.
;
; Use:
;       the use of this routine is specific to adas804, see adas804.pro.
;
; Inputs:
;       pipe    - the idl unit number of the bi-directional pipe to the
;                 adas804 fortran process.
;
;       lpend   - integer, 0 if the user pressed the 'done' button
;                 or 1 if the user pressed 'cancel'.  maps directly onto
;                 the logical variable lpend in adas804 fortran.
;
;       procval - a structure which determines the initial settings of
;                 the processing options widget.  the initial value is
;                 set in adas804.pro.  if adas804.pro passes a blank 
;                 dummy structure of the form {new:-1} into procval then
;                 procval is reset to a default structure.
;
;                 the procval structure is
;
;     procval = {                                 $
;                new    : 0,                      $
;                title  : '  ',                   $
;                ifout  : ifout_arr,              $
;                z      : 0.,                     $
;                z0     : 0.,                     $
;                zeff   : 0.,                     $
;                n1     : 0,                      $
;                l1     : 0,                      $
;                eb1    : 0.,                     $
;                n2     : 0,                      $
;                l2     : 0,                      $
;                eb2    : 0.,                     $
;                isp    : 0,                      $
;                lp     : 0,                      $
;                xjp    : 0.,                     $
;                ist1   : 0,                      $
;                lt1    : 0,                      $
;                xj1    : 0.,                     $
;                xjt1   : 0.,                     $
;                ist2   : 0,                      $
;                lt2    : 0,                      $
;                xj2    : 0.,                     $
;                xjt2   : 0.,                     $
;                neqv1  : 0,                      $
;                fpc1   : 0.,                     $
;                neqv2  : 0,                      $
;                fpc2   : 0.,                     $
;                aval   : 0.,                     $
;                xmax   : 0.,                     $
;                iextwf : 0,                      $
;                ijucys : 0,                      $
;                isrch  : 0,                      $
;                nshell : 0,                      $
;                ns     : shel_arr,               $
;                ls     : shel_arr,               $
;                iqs    : shel_arr,               $
;                alfaa  : alfa_arr,               $
;                nx     : 0,                      $
;                xa     : ener_arr,               $
;                nt     : 0,                      $
;                tea    : temp_arr                $
;                }
;
;                 see cw_adas804_proc.pro for a full description of this
;                 structure.
;
;
;       dsfull  - string; the full system file name of the input 
;                 dataset selected by the user for processing.
;
;       lchoice - integer; variable to flag user choice of archive
;                 option. 
;                 0 = no archive
;                 1 = old archive
;                 2 = refresh from old archive
;                 3 = new archive
;
;       bitfile - string; the path to the directory containing bitmaps
;                 for the 'escape to series menu' button.
;
; Optional inputs:
;       none
;
; Input/output: 
;
; Outputs:
;       procval - on output the structure records the final settings of
;                 the processing selections widget if the user pressed the
;                 'done' button, otherwise it is not changed from input.
;
;       gomenu  - int; flag - set to 1 if user has selected 'escape direct
;                 to series menu , otherwise 0
;
; Optional outputs:
;       none
;
; Keyword parameters:
;       font_large - the name of a larger font.
;
;       font_small - the name of a smaller font.
;
;       edit_fonts - a structure of two fonts used in the table editor
;                    adas_edtab.pro, {font_norm:'',font_input:''}.
;
; Calls:
;       adas804_proc    invoke the idl interface for adas804 data
;                       processing options/input.
;
; Side effects:
;       two way communications with adas804 fortran via the
;       bi-directional unix pipe.
;
; Category:
;       ADAS system.
;
; Written:
;       Hugh Summers, Univ. of Strathclyde, 28-June-2002
;
; Version:
;       1.1     Hugh P Summers
;                - First release
;       1.2     Hugh P Summers
;                - Send temperatures in Kelvin to fortran irrespective
;                  of units chosen by user.
;                - Make search on alpha the default.
;
; Modified:
;       1.1     28-06-2002
;       1.2     28-05-2004
;-
;-----------------------------------------------------------------------------

pro h4ispf, pipe, lpend, procval, dsfull, lchoice, 			$
            iwhc, bitfile, gomenu,	                                $
	    font_large=font_large, font_small=font_small, 		$
	    edit_fonts=edit_fonts


                ;**** set defaults for keywords ****

  if not (keyword_set(font_large)) then font_large = ''
  if not (keyword_set(font_small)) then font_small = ''
  if not (keyword_set(edit_fonts)) then 				$
                                edit_fonts = {font_norm:'',font_input:''}

		;*******************************************
		;**** set default value if non provided ****
                ;****  set energy units off always to   ****
                ;****  force a selection                ****
		;*******************************************

  idum = 0
  readf, pipe, idum  
  ixdim = idum
  idum = 0
  readf, pipe, idum  
  itdim = idum
  idum = 0
  readf, pipe, idum  
  isdim = idum
  readf, pipe, idum                               
  iwhc = idum
  if (procval.new lt 0) then begin
     shel_arr = intarr(isdim)
     alfa_arr = fltarr(2,isdim)
     ener_arr = fltarr(ixdim)
     temp_arr = fltarr(itdim)
     ifout_arr = intarr(2)
     
     procval = {                                 $
                new    : 0,                      $
		title  : '  ',             	 $
		ifout  : ifout_arr,	         $
                z      : 0.,                     $
                z0     : 0.,                     $
                zeff   : 0.,                     $
                n1     : 0,                      $
                l1     : 0,                      $
                eb1    : 0.,                     $
                n2     : 0,                      $
                l2     : 0,                      $
                eb2    : 0.,                     $
                isp    : 0,                      $
                lp     : 0,                      $
                xjp    : 0.,                     $
                ist1   : 0,                      $
                lt1    : 0,                      $
                xj1    : 0.,                     $
                xjt1   : 0.,                     $
                ist2   : 0,                      $
                lt2    : 0,                      $
                xj2    : 0.,                     $
                xjt2   : 0.,                     $
                neqv1  : 0,                      $
                fpc1   : 0.,                     $
                neqv2  : 0,                      $
                fpc2   : 0.,                     $
                aval   : 0.,                     $
                xmax   : 0.,                     $
                iextwf : 0,                      $
                ijucys : 0,                      $
                isrch  : 1,                      $
                nshell : 0,                      $
                ns     : shel_arr,           	 $
                ls     : shel_arr,           	 $
                iqs    : shel_arr,           	 $
                alfaa  : alfa_arr,       	 $
                nx     : 0,                      $
                xin    : ener_arr,               $
                nt     : 0,                      $
                tin    : temp_arr           	 $
               }

		;***************************************************
                ;**** force units to those of archive if chosen ****
		;***************************************************

      if lchoice eq 1 or lchoice eq 2 then begin
          procval.ifout(0) = 3
          procval.ifout(1) = 0
      endif else begin
          procval.ifout(0) = 3
          procval.ifout(1) = 0
;          for i = 0,1 do begin
;              procval.ifout(i) = 5*i
;          endfor  
      endelse

  endif else begin


     procval = { 			         $
                new    : procval.new,            $
		title  : procval.title,     	 $
		ifout  : procval.ifout,          $
                z      : procval.z,              $
                z0     : procval.z0,             $
                zeff   : procval.zeff,           $
                n1     : procval.n1,             $
                l1     : procval.l1,             $
                eb1    : procval.eb1,            $
                n2     : procval.n2,             $
                l2     : procval.l2,             $
                eb2    : procval.eb2,            $
                isp    : procval.isp,            $
                lp     : procval.lp,             $
                xjp    : procval.xjp,            $
                ist1   : procval.ist1,           $
                lt1    : procval.lt1,            $
                xj1    : procval.xj1,            $
                xjt1   : procval.xjt1,           $
                ist2   : procval.ist2,           $
                lt2    : procval.lt2,            $
                xj2    : procval.xj2,            $
                xjt2   : procval.xjt2,           $
                neqv1  : procval.neqv1,          $
                fpc1   : procval.fpc1,           $
                neqv2  : procval.neqv2,          $
                fpc2   : procval.fpc2,           $
                aval   : procval.aval,           $
                xmax   : procval.xmax,           $
                iextwf : procval.iextwf,         $
                ijucys : procval.ijucys,         $
                isrch  : procval.isrch,          $
                nshell : procval.nshell,         $
                ns     : procval.ns,             $
                ls     : procval.ls,             $
                iqs    : procval.iqs,            $
                alfaa  : procval.alfaa,       	 $
                nx     : procval.nx,             $
                xin    : procval.xin,            $
                nt     : procval.nt,             $
                tin    : procval.tin             $
              }
	      
		;***************************************************
                ;**** force units to those of archive if chosen ****
		;***************************************************

      if lchoice eq 1 or lchoice eq 2 then begin
          procval.ifout(0) = 3
          procval.ifout(1) = 0
      endif else begin
          procval.ifout(0) = 3
          procval.ifout(1) = 0
      endelse


  endelse

  lpend = 0

		;******************************************
		;**** now define other variable types  ****
		;******************************************
  
  ndum   = 0
  ldum   = 0
  iqdum  = 0
  sdum   = ' '
  title  = procval.title

		;******************************************
		;*** now define other array types sizes ***
		;******************************************

  ns   = intarr(isdim)
  ls   = intarr(isdim)
  iqs  = intarr(isdim)
  alfaa  = fltarr(2,isdim)
  xa     = fltarr(ixdim,5)
  tea    = fltarr(itdim,5)

		;**********************************************
		;**** read data from fortran if refreshing ****
		;**** otherwise use default dataset        ****
		;**********************************************
		
  if (lchoice ne 2) and (procval.new ge 0) then begin
  
    ;*** move xin, tin data from procval to xa and tea arrays ***
    ;*** note procval.ifout is also used for widget table position  ***
    
    nx = procval.nx
    nt = procval.nt
    ifout0=procval.ifout(0)
    ifout1=procval.ifout(1)
    if ifout1 ge 5 then ifout1=ifout1-5
    eij = procval.eb1-procval.eb2
    zeff=procval.zeff
    z1 = procval.z+1.0d0 
    z0 = procval.z0 
    
    if nx gt 0 then begin
       for ict = 0,nx-1 do begin
         if ifout0 eq 0 then xa(ict,3)=procval.xin(ict)/eij+1.0d0 
         if ifout0 eq 1 then xa(ict,3)=procval.xin(ict)/eij 
         if ifout0 eq 1 then xa(ict,3)=z0*z0*procval.xin(ict)/eij+1.0d0 
         if ifout0 eq 3 then xa(ict,3)=procval.xin(ict) 
         if ifout0 eq 4 then xa(ict,3)=zeff*zeff*procval.xin(ict)/eij+1.0d0 
       endfor
    endif   

    if nt gt 0 then begin
       for itout = 0,nt-1 do begin
         if ifout1 eq 0 then tea(itout,0)=procval.tin(itout) 
         if ifout1 eq 1 then tea(itout,0)=1.16054d4*procval.tin(itout) 
         if ifout1 eq 2 then tea(itout,0)=z1*z1*procval.tin(itout) 
         if ifout1 eq 3 then tea(itout,0)=1.5789d5*eij*procval.tin(itout) 
       endfor
    
    endif   
  
  endif else if lchoice eq 2 then begin
    if iwhc eq 1 then begin
      readf, pipe, sdum
        title = strtrim(sdum,2)
      readf, pipe, z
      readf, pipe, z0   
      readf, pipe, zeff 
      readf, pipe, n1
      readf, pipe, l1 
      readf, pipe, eb1
      readf, pipe, n2
      readf, pipe, l2
      readf, pipe, eb2
      readf, pipe, isp
      readf, pipe, lp
      readf, pipe, xjp
      readf, pipe, ist1                      
      readf, pipe, lt1
      readf, pipe, xj1   
      readf, pipe, xjt1   
      readf, pipe, ist2 
      readf, pipe, lt2
      readf, pipe, xj2 
      readf, pipe, xjt2 
      readf, pipe, neqv1
      readf, pipe, fpc1
      readf, pipe, neqv2
      readf, pipe, fpc2
      readf, pipe, aval
      readf, pipe, xmax
      readf, pipe, iextwf
      readf, pipe, ijucys                      
      readf, pipe, isrch

		;***************************************
                ;**** read vector data from fortran ****
                ;***************************************
		
      readf, pipe, nshell
      
      for i = 0,nshell-1 do begin
        readf, pipe, format='(1x,i4,1x,i4,1x,i4,1x,f8.4,1x,f8.4)',	$
	                      ndum,ldum,iqdum,alf1,alf2
	
                     ns(i)  = ndum
                     ls(i)  = ldum
                     iqs(i) = iqdum
                     alfaa(0,i) = alf1
                     alfaa(1,i) = alf2
      endfor
      
      readf, pipe, nx

      for i = 0,nx-1 do begin
        readf, pipe, format = '(1x,e12.4)',xa1
                     xa(i,3) = xa1
      endfor
      
      readf, pipe, nt

      for i = 0,nt-1 do begin
        readf, pipe, format = '(1x,e12.4)',tea1
                     tea(i,0) = tea1
      endfor
    
 
    endif else if iwhc eq 2 then begin
      readf, pipe, sdum
        title = strtrim(sdum,2)
      readf, pipe, z
      readf, pipe, z0   
      readf, pipe, zeff 
      readf, pipe, n1
      readf, pipe, l1 
      readf, pipe, eb1
      readf, pipe, n2
      readf, pipe, l2
      readf, pipe, eb2
      readf, pipe, isp
      readf, pipe, lp
      readf, pipe, xjp
      readf, pipe, ist1                      
      readf, pipe, lt1
      readf, pipe, xj1   
      readf, pipe, xjt1   
      readf, pipe, ist2 
      readf, pipe, lt2
      readf, pipe, xj2 
      readf, pipe, xjt2 
      readf, pipe, neqv1
      readf, pipe, fpc1
      readf, pipe, neqv2
      readf, pipe, fpc2
      readf, pipe, aval
      readf, pipe, xmax
      readf, pipe, iextwf
      readf, pipe, ijucys                      
      readf, pipe, isrch
      
		;***************************************
                ;**** read vector data from fortran ****
                ;***************************************
      
      readf, pipe, nshell

      for i = 0,nshell-1 do begin
        readf, pipe, format='(1x,i4,1x,i4,1x,i4,1x,f8.4,1x,f8.4)',	$
	                      n1,l1,iq1,alf1,alf2
                     ns(i)  = n1
                     ls(i)  = l1
                     iqs(i) = iq1
                     alfaa(0,i) = alf1
                     alfaa(1,i) = alf2
      endfor
      
      readf, pipe, nx
      
      for i = 0,nx-1 do begin
        readf, pipe, format = '(1x,e12.4)',xa1
                     xa(i,3) = xa1
      endfor
      
      readf, pipe, nt
      
      for i = 0,nt-1 do begin
        readf, pipe, format = '(1x,e12.4)',tea1
                     tea(i,0) = tea1
      endfor
    
    endif
    
    eij = eb1-eb2
    z1 = z+1.0d0 
    

 

		;*******************************************
		;****    copy values to structure       ****
		;*******************************************
		
     procval = { 			         $
                new    : procval.new,            $
                title  : title,                  $
		ifout  : procval.ifout,          $
                z      : z,                      $
                z0     : z0,                     $
                zeff   : zeff,                   $
                n1     : n1,                     $
                l1     : l1,                     $
                eb1    : eb1,                    $
                n2     : n2,                     $
                l2     : l2,                     $
                eb2    : eb2,                    $
                isp    : isp,                    $
                lp     : lp,                     $
                xjp    : xjp,                    $
                ist1   : ist1,                   $
                lt1    : lt1,                    $
                xj1    : xj1,                    $
                xjt1   : xjt1,                   $
                ist2   : ist2,                   $
                lt2    : lt2,                    $
                xj2    : xj2,                    $
                xjt2   : xjt2,                   $
                neqv1  : neqv1,                  $
                fpc1   : fpc1,                   $
                neqv2  : neqv2,                  $
                fpc2   : fpc2,                   $
                aval   : aval,                   $
                xmax   : xmax,                   $
                iextwf : iextwf,                 $
                ijucys : ijucys,                 $
                isrch  : isrch,                  $
                nshell : nshell,                 $
                ns     : ns,                     $
                ls     : ls,                     $
                iqs    : iqs,                    $
                alfaa  : alfaa,       	         $
                nx     : nx,                     $
                xin    : procval.xin,            $
                nt     : nt,                     $
                tin    : procval.tin             $
              }

endif 

    
 for ict = 0,nx-1 do begin
   x = xa(ict,3)
   xa(ict,0) = eij*(x-1.0d0)                     ; energy units
   xa(ict,1) = x*eij
   xa(ict,2) = (eij*(x-1.0d0))/(z0*z0)
   xa(ict,3) = x
   xa(ict,4) = (eij*(x-1.0d0))/(zeff*zeff)
 endfor
    
                                                               
 for itout = 0,nt-1 do begin
   te = tea(itout,0)                             ; temp units
   tea(itout,1) = te/1.16054d4                 
   tea(itout,2) = te/(z1*z1)
   tea(itout,3) = te/(1.5789d5*eij)
 endfor

 
		;****************************************
		;**** pop-up processing input widget ****
		;****************************************

  adas804_proc, procval, dsfull, lchoice,                               $
  		ixdim,  itdim  , isdim , xa , tea, 			$
    		action, bitfile,  	       				$
		font_large=font_large, font_small=font_small, 		$
		edit_fonts=edit_fonts

;		;********************************************
		;****  act on the event from the widget  ****
		;**** there are only two possible events ****
		;**** 'done' and 'cancel'.               ****
		;********************************************
		
		
;  print,'h4ispf.pro:procval=',procval
  
  
    ;*** again move xin, tin data from procval to xa and tea arrays ***
    ;*** note procval.ifout is also used for widget table position  ***
    
    nx = procval.nx
    nt = procval.nt
    ifout0=procval.ifout(0)
    ifout1=procval.ifout(1)
    if ifout1 ge 5 then ifout1=ifout1-5
    eij = procval.eb1-procval.eb2
    zeff=procval.zeff
    z1 = procval.z+1.0d0 
    z0 = procval.z0 
    
;    print,'h4ispf.pro:ifout0,ifout1=',ifout0,ifout1
    
    if nx gt 0 then begin
       for ict = 0,nx-1 do begin
         if ifout0 eq 0 then xa(ict,3)=procval.xin(ict)/eij+1.0d0 
         if ifout0 eq 1 then xa(ict,3)=procval.xin(ict)/eij 
         if ifout0 eq 1 then xa(ict,3)=z0*z0*procval.xin(ict)/eij+1.0d0 
         if ifout0 eq 3 then xa(ict,3)=procval.xin(ict) 
         if ifout0 eq 4 then xa(ict,3)=zeff*zeff*procval.xin(ict)/eij+1.0d0 
       endfor
    endif   

    if nt gt 0 then begin
       for itout = 0,nt-1 do begin
         if ifout1 eq 0 then tea(itout,0)=procval.tin(itout) 
         if ifout1 eq 1 then tea(itout,0)=1.16054d4*procval.tin(itout) 
         if ifout1 eq 2 then tea(itout,0)=z1*z1*procval.tin(itout) 
         if ifout1 eq 3 then tea(itout,0)=1.5789d5*eij*procval.tin(itout) 
       endfor
    
    endif   
  
  		
		
  gomenu = 0
  if action eq 'done' then begin
    lpend = 0
  endif else if action eq 'menu' then begin
    lpend = 0
    gomenu = 1
  endif else begin
    lpend = 1
  end
		;**************************************
		;**** write edited data to fortran ****
		;**************************************

  printf, pipe, lpend
  printf, pipe, format = '(1x,a40)', procval.title
  printf, pipe, format = '(1x,f6.2)', procval.z
  printf, pipe, format = '(1x,f6.2)', procval.z0
  printf, pipe, format = '(1x,f6.2)', procval.zeff
  printf, pipe, format = '(1x,i3)', procval.n1
  printf, pipe, format = '(1x,i3)', procval.l1
  printf, pipe, format = '(1x,f15.8)', procval.eb1
  printf, pipe, format = '(1x,i3)', procval.n2
  printf, pipe, format = '(1x,i3)', procval.l2
  printf, pipe, format = '(1x,f15.8)', procval.eb2
  printf, pipe, format = '(1x,i3)', procval.isp
  printf, pipe, format = '(1x,i3)', procval.lp
  printf, pipe, format = '(1x,f6.2)', procval.xjp
  printf, pipe, format = '(1x,i3)', procval.ist1
  printf, pipe, format = '(1x,i3)', procval.lt1
  printf, pipe, format = '(1x,f6.2)', procval.xj1
  printf, pipe, format = '(1x,f6.2)', procval.xjt1
  printf, pipe, format = '(1x,i3)', procval.ist2
  printf, pipe, format = '(1x,i3)', procval.lt2
  printf, pipe, format = '(1x,f6.2)', procval.xj2
  printf, pipe, format = '(1x,f6.2)', procval.xjt2
  printf, pipe, format = '(1x,i3)', procval.neqv1
  printf, pipe, format = '(1x,f8.4)', procval.fpc1
  printf, pipe, format = '(1x,i3)', procval.neqv2
  printf, pipe, format = '(1x,f8.4)', procval.fpc2
  printf, pipe, format = '(1x,1e12.4)', procval.aval
  printf, pipe, format = '(1x,f10.5)', procval.xmax
  printf, pipe, format = '(1x,i1)', procval.iextwf
  printf, pipe, format = '(1x,i1)', procval.ijucys
  printf, pipe, format = '(1x,i1)', procval.isrch

  printf, pipe, format = '(1x,i3)', procval.nshell
 
  for i = 0,procval.nshell-1 do begin
    printf, pipe, format='(1x,i4,1x,i4,1x,i4,1x,f8.4,1x,f8.4)',	$
                  procval.ns(i),procval.ls(i),procval.iqs(i),   $
		  procval.alfaa(0,i),procval.alfaa(1,i)
  endfor
  
  printf, pipe, format = '(1x,i3)', procval.nx
 
  for i = 0,procval.nx-1 do begin
    printf, pipe, format='(1x,1e12.4)',	$
                  xa(i,3)
  endfor
 
  printf, pipe, format = '(1x,i3)', procval.nt
 
  for i = 0,procval.nt-1 do begin
    printf, pipe, format='(1x,1e12.4)',	$
                  tea(i,0)
  endfor

  for i = 0,n_elements(procval.ifout)-1 do begin
    unit = procval.ifout(i)-(5*i)
    printf, pipe, format = '(1x,i3)', unit
  endfor
  


end
