; SCCS info: Module @(#)$Header: /home/adascvs/idl/adas8xx/adas804/cw_804_gr_sel.pro,v 1.1 2004/07/06 12:25:28 whitefor Exp $ Date $Date: 2004/07/06 12:25:28 $
;
;+
; PROJECT:
;       ADAS IBM MVS to   UNIX conversion
;
; NAME:
;	 cw_804_gr_SEL()
;
; PURPOSE:
;	Generates a widget used for graphical output selection.
;
; EXPLANATION:
;	This widget consists of a titled activation button, a text
;	widget for entering a graph title, two scaling ranges input
;	widgets cw_adas_ranges.pro, an output file widget
;	cw_adas_outfile.pro, a single selection widget cw_single_sel.pro
;	with a list of available hardcopy devices.  A general error
;	message is also included in the widget.
;
;	cw_adas_ranges.pro and cw_adas_outfile.pro handle their own
;	events and perform their own error checking.  The general
;	activation button desensitises all other component widgets when
;	it is set off.  The only other events processed by this widget
;	are from the two single selection lists for graphs and devices.
;
;	Many of the elements of this widget are optional and controlled
;	by the caller.
;
;	This widget generates an event when the activation button is
;	pressed.  The event structure is;
;		{ID:0L, TOP:0L, HANDLER:0L, OUTBUT:0}
;	where OUTBUT is 0 if the activation button is off and 1 if
;	the activaion button is on.
;
; USE:
;	An example of use is given below.  Also see cw_adas205_out.pro
;	for another example.
;
;	grplist = ['Graph 1','Graph2','Graph3']
;	devlist = ['Device 1','Device 2','Device 3']
;	value = {							$
;		OUTBUT:0 , GTIT1:'', 					$
;		SCALBUT:0, 						$
;		XMIN:'', XMAX:'', 					$
;		YMIN:'', YMAX:'', 					$
;		HRDOUT:0, HARDNAME:'', 					$
;		GRPDEF:'', GRPFMESS:'', 				$
;		GRPSEL:-1, GRPRMESS:'', 				$
;		DEVSEL:-1, GRSELMESS:'' }
;
;	base=widget_base(/column)
;	grpid= cw_804_gr_sel(base,value=value,grplist=grplist,devlist=devlist)
;	rc=widget_button(base,value='Done')
;	widget_control,base,/realize
;	rc=widget_event()
;	widget_control,grpid,get_value=value
;
;	if strtrim(value.grselmess) eq '' and 				$
;	   strtrim(value.grpfmess) eq '' and 				$
;	   strtrim(value.grprmess) eq '' then begin
;
;	  if value.outbut eq 1 then begin
;	    print,'Graph selected: ',grplist(value.grpsel)
;
;	    if value.hrdout eq 1 then begin
;	      print,'Device selected: ',devlist(value.devsel)
;	      print,'Hard copy file: ',value.hardname
;	    end
;
;	    if value.scalbut eq 1 then begin
;	      print,'Scaling ranges:'
;	      print,'XMIN: ',value.xmin,'  XMAX: ',value.xmax
;	      print,'YMIN: ',value.ymin,'  YMAX: ',value.ymax
;	    end
;	  end else begin
;	    print,'No graphical output requested.'
;	  end
;
;	end else begin
;	  print,'Graphical selection error;'
;	  print,value.grselmess,value.grpfmess,value.grprmess
;	end
;
; INPUTS:
;       PARENT   - The ID of the parent widget.
;
; OPTIONAL INPUTS:
;	None.
;
; OUTPUTS:
;       The return value of this function is the ID of the compound
;       widget which is of type LONG integer.
;
; OPTIONAL OUTPUTS:
;       None
;
; KEYWORD PARAMETERS:
;	SIGN	  - Integer: < 0  => Graph ranges must be negative
;			     0	  => Graph ranges can be positive or negative
;			     > 0  => Graph ranges must be positive
;	OUTPUT	  - A string; Label to use for activate button.  Default
;		    'Graphical Output'.
;
;	GRPLIST	  - A string array; A list of graphs one array element per
;		    graph.  If the list is empty i.e grplist='' then the
;		    selection list widget will not appear.  Default value=''.
;
;	LISTTITLE - A string; title for the graph selection list.
;		    Default 'Select Graph'.
;
;	DEVLIST	  - A string array; A list of devices one array element per
;		    device.  If the list is empty i.e devlist='' then the
;		    selection list widget will not appear.  Default value=''.
;
;	VALUE	  - A structure which defines the value of this widget.
;		    The structure is made up of a number of parts each
;		    pertaining to one of the widgets in this compound
;		    widget.  Default value;
;
;		      { OUTBUT:0 , GTIT1:'', 				$
;			SCALBUT:0, 					$
;			XMIN:'', XMAX:'', 				$
;			YMIN:'', YMAX:'', 				$
;			HRDOUT:0, HARDNAME:'', 				$
;			GRPDEF:'', GRPFMESS:'', 			$
;			GRPSEL:-1, GRPRMESS:'', 			$
;			DEVSEL:-1, GRSELMESS:'' }
;
;		    Note these do not appear in the same order as in the
;		    value structure.
;		    General;
;			OUTBUT	   Integer; Activation button 1 on, 0 off
;			GTIT1	   String; Graph title
;			GRSELMESS  String; General error message
;
;		    For CW_ADAS_RANGES;
;			SCALBUT	   Integer; Scaling activation 1 on, 0 off,
;				   -1 no widget.
;			XMIN	   String; x-axis minimum, string of number
;			XMAX	   String; x-axis maximum, string of number
;			YMIN	   String; y-axis minimum, string of number
;			YMAX	   String; y-axis maximum, string of number
;			GRPRMESS   String; Scaling ranges error message
;
;		    For CW_ADAS_OUTFILE hard copy output file;
;			HRDOUT	   Integer; Hard copy activ' 1 on, 0 off,
;				   -1 no widget.
;			HARDNAME   String; Hard copy output file name
;			GRPDEF	   String; Default output file name 
;			GRPFMESS   String; File name error message
;			(Replace and append values are fixed)
;
;		    For CW_SINGLE_SEL graph selection;
;			DEVSEL	   Integer; index of selected device in DEVLIST
;				   -1 indicates no selection made.
;
;       FONT      - String; Name of a font to use for all text in this widget.
;
;	UVALUE	  - A user value for this widget.
;
;	LABEL#	  - Labels to put over the scaling option widgets
;
; CALLS:
;	CW_ADAS_RANGES	Specify explict x and y axis ranges.
;	CW_ADAS_OUTFILE	Specify output file name.
;	CW_SINGLE_SEL	A single selection list widget.
;       See side effects for other related widget management routines.
;
; SIDE EFFECTS:
;       Three other routines are included which are used to manage the
;       widget;
;
;	gr_804_SET_VAL
;	gr_804_GET_VAL()
;	gr_804_EVENT()
;
; CATEGORY:
;	Compound Widget
;
; WRITTEN:
;	Hugh P. Summers, University of Strathclyde, 10-Jul-2002
;
; MODIFIED:
;       1.1	Hugh P. Summers  
;		Renamed from cw_adas_gr_sel to cw_804_gr_sel as this
;		is easier than generalising the routine for use by all
;		other adas programs.
; VERSION:
;	1.1	10-Jul-02
;-
;-----------------------------------------------------------------------------

PRO gr_804_set_val, id, value


		;**** Return to caller on error ****
  ON_ERROR, 2

               ;***************************************
               ;****     Retrieve the state        ****
               ;**** Get first_child widget id     ****
               ;**** because state is stored there ****
               ;***************************************

  first_child = widget_info(id, /child)
  widget_control, first_child, get_uvalue = state

		;**** Value for the file widget ****

  outfval = { OUTBUT:value.hrdout, APPBUT:-1, REPBUT:0, 		$
              FILENAME:value.hardname, DEFNAME:value.grpdef, 		$
	      MESSAGE:value.grpfmess 			}

		;**** Value for the ranges widget ****

  if state.scbasid1 gt 0 then begin
    rngval1 = {  SCALBUT:value.scalbut1, 				$
		XMIN:value.xmin1, XMAX:value.xmax1, 			$
		YMIN:value.ymin1, YMAX:value.ymax1, 			$
		GRPRMESS:value.grprmess, SIGN:value.sign, 		$
                TITLE:'1st Graph Default Scaling' }
  end
  if state.scbasid2 gt 0 then begin
    rngval2 = {  SCALBUT:value.scalbut2, 				$
		XMIN:value.xmin2, XMAX:value.xmax2, 			$
		YMIN:value.ymin2, YMAX:value.ymax2, 			$
		GRPRMESS:value.grprmess, SIGN:value.sign,		$
                TITLE:'2nd Graph Default Scaling'  }
  end

		;**** Sensitise or desensitise with output button setting ****

  if value.outbut eq 0 then begin

    widget_control,state.actid,set_button=0

		;**** desensitise graph title ****

    widget_control,state.tibasid,sensitive=0

		;**** desensitise graph selection list ****

    if state.listid gt 0 then widget_control,state.lsbasid,sensitive=0

		;**** desensitise device selection list ****

    if state.devbasid gt 0 then widget_control,state.devbasid,sensitive=0

		;**** desensitise filename and hide settings ****

    widget_control,state.fbaseid,sensitive=0
    outfval.outbut = 0
    widget_control,state.fileid,set_value=outfval

		;**** desensitise ranges and hide settings ****

    if state.scbasid1 gt 0 then begin
      widget_control,state.scbasid1,sensitive=0
      rngval1.scalbut = 0
      widget_control,state.rangeid1,set_value=rngval1
    end
    if state.scbasid2 gt 0 then begin
      widget_control,state.scbasid2,sensitive=0
      rngval2.scalbut = 0
      widget_control,state.rangeid2,set_value=rngval2
    end

  end else begin

    widget_control,state.actid,set_button=1

    widget_control,state.tibasid,/sensitive

    if state.listid gt 0 then widget_control,state.lsbasid,/sensitive

    if state.devbasid gt 0 then widget_control,state.devbasid,/sensitive

    widget_control,state.fbaseid,/sensitive
    widget_control,state.fileid,set_value=outfval

    if state.scbasid1 gt 0 then begin
      widget_control,state.scbasid1,/sensitive
      widget_control,state.rangeid1,set_value=rngval1
    end
    if state.scbasid2 gt 0 then begin
      widget_control,state.scbasid2,/sensitive
      widget_control,state.rangeid2,set_value=rngval2
    end

  end

		;**** Update value of graph selection ****

  if state.listid gt 0 then widget_control,state.listid,set_value=value.grpsel

		;**** Update value of device selection ****

  if state.devid gt 0 then widget_control,state.devid,set_value=value.devsel

		;**** Update message ****

  if strtrim(value.grselmess) ne '' then begin
    message = value.grselmess
  end else begin
    message = ' '
  end
  widget_control,state.messid,set_value=message

		;**** Copy the new value to state structure ****

  state.gr804val.outbut = value.outbut
  state.gr804val.gtit1 = value.gtit1
  state.gr804val.scalbut1 = value.scalbut1
  state.gr804val.xmin1 = value.xmin1
  state.gr804val.xmax1 = value.xmax1
  state.gr804val.ymin1 = value.ymin1
  state.gr804val.ymax1 = value.ymax1
  state.gr804val.scalbut2 = value.scalbut2
  state.gr804val.xmin2 = value.xmin2
  state.gr804val.xmax2 = value.xmax2
  state.gr804val.ymin2 = value.ymin2
  state.gr804val.ymax2 = value.ymax2
  state.gr804val.hrdout = value.hrdout
  state.gr804val.hardname = value.hardname
  state.gr804val.grpdef = value.grpdef
  state.gr804val.grpfmess = value.grpfmess
  state.gr804val.grpsel = value.grpsel
  state.gr804val.grprmess = value.grprmess
  state.gr804val.devsel = value.devsel
  state.gr804val.grselmess = value.grselmess


		;**** Save the new state ****

  widget_control, first_child, set_uvalue=state, /no_copy

END

;-----------------------------------------------------------------------------


FUNCTION gr_804_get_val, id


		;**** Return to caller on error ****
  ON_ERROR, 2

               ;***************************************
               ;****     Retrieve the state        ****
               ;**** Get first_child widget id     ****
               ;**** because state is stored there ****
               ;***************************************

  first_child = widget_info(id, /child)
  widget_control, first_child, get_uvalue = state

		;**** Get the graph title ****
		;*** truncate to 32 characters ***

  widget_control,state.titleid,get_value=gtit1
  if (strlen(gtit1(0)) gt 32 ) then begin
	gtit1(0) = strmid(gtit1(0),0,32)
        message = 'Warning : Title truncated to 32 characters'
        widget_control,state.messid,set_value=message
        wait, 1.0
	message = ' '
  endif
  state.gr804val.gtit1 = gtit1(0)

		;**** Get the latest filename ****

  widget_control,state.fileid,get_value=outfval
  state.gr804val.hardname = outfval.filename
  state.gr804val.grpfmess = outfval.message
  if state.gr804val.outbut eq 1 then state.gr804val.hrdout = outfval.outbut

		;**** Get scaling ranges and copy to value ****

  if state.scbasid1 gt 0 then begin
    widget_control,state.rangeid1,get_value=rngval1
    state.gr804val.xmin1 = rngval1.xmin
    state.gr804val.xmax1 = rngval1.xmax
    state.gr804val.ymin1 = rngval1.ymin
    state.gr804val.ymax1 = rngval1.ymax
    state.gr804val.grprmess = rngval1.grprmess
    if state.gr804val.outbut eq 1 then state.gr804val.scalbut1=rngval1.scalbut
  end
  if state.scbasid2 gt 0 then begin
    widget_control,state.rangeid2,get_value=rngval2
    state.gr804val.xmin2 = rngval2.xmin
    state.gr804val.xmax2 = rngval2.xmax
    state.gr804val.ymin2 = rngval2.ymin
    state.gr804val.ymax2 = rngval2.ymax
    state.gr804val.grprmess = rngval2.grprmess
    if state.gr804val.outbut eq 1 then state.gr804val.scalbut2=rngval2.scalbut
  end

		;**** Clear any error message ****

  if state.gr804val.hrdout ge 0 then begin
    state.gr804val.grselmess = ''
    widget_control,state.messid,set_value=' '
  end

		;**** Check that a graph has been selected if appropriate ****

  if state.listid gt 0L then begin
    if state.gr804val.grpsel lt 0 then begin
      state.gr804val.grselmess = '**** You must select a graph ****'
      widget_control,state.messid,set_value=state.gr804val.grselmess
    end
  end

		;**** Check that a device has been selected if appropriate ****

  if state.gr804val.hrdout eq 1 and state.devid gt 0L then begin
    if state.gr804val.devsel lt 0 then begin
      state.gr804val.grselmess = '**** You must select a device ****'
      widget_control,state.messid,set_value=state.gr804val.grselmess
    end
  end


  RETURN, state.gr804val

END

;-----------------------------------------------------------------------------

FUNCTION gr_804_event, event


		;**** Base ID of compound widget ****
  base=event.handler

               ;***************************************
               ;****     Retrieve the state        ****
               ;**** Get first_child widget id     ****
               ;**** because state is stored there ****
               ;***************************************

  first_child = widget_info(base, /child)
  widget_control, first_child, get_uvalue = state

  new_event = 0L
		;************************
		;**** Process Events ****
		;************************
  CASE event.id OF

    state.actid: begin
			;**** Sensitise and desensitise with toggle ****
			;**** of output button.			    ****
	if state.gr804val.outbut eq 1 then begin

	  state.gr804val.outbut = 0
	  new_event = {ID:base, TOP:event.top, HANDLER:0L, OUTBUT:0}

			;**** Desensitise graph title ****

	  widget_control,state.tibasid,sensitive=0

			;**** Desensitise graph selection list ****

	  if state.listid gt 0 then $
				widget_control,state.lsbasid,sensitive=0

			;**** Desensitise device selection list ****

	  if state.devbasid gt 0 then widget_control,state.devbasid,sensitive=0

			;**** Desensitise filename and hide settings ****

	  outfval = {	OUTBUT:0, APPBUT:-1, REPBUT:0, 			$
                	FILENAME:state.gr804val.hardname, 		$
			DEFNAME:state.gr804val.grpdef, 			$
	        	MESSAGE:'' }
	  widget_control,state.fbaseid,sensitive=0
	  widget_control,state.fileid,set_value=outfval

			;**** Desensitise scaling and hide settings ****

	  if state.scbasid1 gt 0 then begin
	  rngval1 = {	SCALBUT:0, 					$
			XMIN:state.gr804val.xmin1, XMAX:state.gr804val.xmax1, $
			YMIN:state.gr804val.ymin1, YMAX:state.gr804val.ymax1, $
			GRPRMESS:'' , SIGN:state.gr804val.sign,		$
                        TITLE:'1st Graph Default Scaling' }
	    widget_control,state.scbasid1,sensitive=0
	    widget_control,state.rangeid1,set_value=rngval1
	  end
	  if state.scbasid2 gt 0 then begin
	  rngval2 = {	SCALBUT:0, 					$
			XMIN:state.gr804val.xmin2, XMAX:state.gr804val.xmax2, $
			YMIN:state.gr804val.ymin2, YMAX:state.gr804val.ymax2, $
			GRPRMESS:'' , SIGN:state.gr804val.sign,		$
                        TITLE:'2nd Graph Default Scaling' }
	    widget_control,state.scbasid2,sensitive=0
	    widget_control,state.rangeid2,set_value=rngval2
	  end

	end else begin

	  state.gr804val.outbut = 1
	  new_event = {ID:base, TOP:event.top, HANDLER:0L, OUTBUT:1}

			;**** Sensitise graph title ****

	  widget_control,state.tibasid,/sensitive

			;**** Sensitise selection list ****

	  if state.listid gt 0 then widget_control,state.lsbasid,/sensitive

			;**** Sensitise device selection list ****

	  if state.devbasid gt 0 then widget_control,state.devbasid,/sensitive

			;**** Sensitise filename and restore settings ****

	  widget_control,state.fbaseid,/sensitive
	  widget_control,state.fileid,get_value=outfval
	  outfval.outbut = state.gr804val.hrdout
	  widget_control,state.fileid,set_value=outfval

			;**** Sensitise scaling and restore settings ****

	  if state.scbasid1 gt 0 then begin
	    widget_control,state.scbasid1,/sensitive
	    widget_control,state.rangeid1,get_value=rngval1
	    rngval1.scalbut = state.gr804val.scalbut1
	    widget_control,state.rangeid1,set_value=rngval1
	  end
	  if state.scbasid2 gt 0 then begin
	    widget_control,state.scbasid2,/sensitive
	    widget_control,state.rangeid2,get_value=rngval2
	    rngval2.scalbut = state.gr804val.scalbut2
	    widget_control,state.rangeid2,set_value=rngval2
	  end

	end
    end

    state.listid: state.gr804val.grpsel = event.index

    state.devid: state.gr804val.devsel = event.index

    ELSE:

  ENDCASE

		;**** Save the new state structure ****

    widget_control, first_child, set_uvalue=state, /no_copy

  RETURN, new_event
END

;-----------------------------------------------------------------------------

FUNCTION  cw_804_gr_sel, parent, SIGN=sign, OUTPUT=output, GRPLIST=grplist, $
			LISTTITLE=listtitle, DEVLIST=devlist, 		$
			VALUE=value, FONT=font, UVALUE=uvalue, LABEL1=label1,$
			LABEL2=label2



  IF (N_PARAMS() LT 1) THEN MESSAGE, 'Must specify PARENT for  cw_804_gr_sel'

		;**** Set defaults for keywords ****

  IF NOT (KEYWORD_SET(sign)) THEN sign = 0
  IF NOT (KEYWORD_SET(output)) THEN output = 'Graphical Output'
  IF NOT (KEYWORD_SET(grplist)) THEN grplist = ''
  IF NOT (KEYWORD_SET(listtitle)) THEN listtitle = 'Select Graph'
  IF NOT (KEYWORD_SET(devlist)) THEN devlist = ''
  IF NOT (KEYWORD_SET(value)) THEN begin
	  gr804val = {gr804val, 		$
			OUTBUT:0 , GTIT1:'', 	$
			SCALBUT1:0, 		$
			XMIN1:'', XMAX1:'', 	$
			YMIN1:'', YMAX1:'', 	$
			SCALBUT2:0, 		$
			XMIN2:'', XMAX2:'', 	$
			YMIN2:'', YMAX2:'', 	$
			SIGN:sign, 	  	$	
			HRDOUT:0, HARDNAME:'', 	$
			GRPDEF:'', GRPFMESS:'', $
			GRPSEL:-1, GRPRMESS:'', $
			DEVSEL:-1, GRSELMESS:'' $
		     }
	end else begin
	  gr804val = {gr804val, 					$
			OUTBUT:value.outbut, GTIT1:value.gtit1, 	$
			SCALBUT1:value.scalbut1, 			$
			XMIN1:value.xmin1, XMAX1:value.xmax1, 		$
			YMIN1:value.ymin1, YMAX1:value.ymax1, 		$
			SCALBUT2:value.scalbut2, 			$
			XMIN2:value.xmin2, XMAX2:value.xmax2, 		$
			YMIN2:value.ymin2, YMAX2:value.ymax2, 		$
			SIGN:sign,		  			$
			HRDOUT:value.hrdout, HARDNAME:value.hardname, 	$
			GRPDEF:value.grpdef, GRPFMESS:value.grpfmess, 	$
			GRPSEL:value.grpsel, GRPRMESS:value.grprmess, 	$
			DEVSEL:value.devsel, GRSELMESS:value.grselmess }
	end
  IF NOT (KEYWORD_SET(font)) THEN font = ''
  IF NOT (KEYWORD_SET(uvalue))  THEN uvalue = 0
  IF NOT (KEYWORD_SET(label1)) THEN label1 = 'Omega graph scaling'
  IF NOT (KEYWORD_SET(label2)) THEN label2 = 'Upsilon graph scaling'

		;**** Create the main base for the widget ****

  main = WIDGET_BASE(parent, UVALUE = uvalue, 				$
		EVENT_FUNC = "gr_804_event", 				$
		FUNC_GET_VALUE = "gr_804_get_val", 			$
		PRO_SET_VALUE = "gr_804_set_val", 			$
		/ROW)

                ;******************************************************
                ;**** Create a dummy widget just to hold value of *****
                ;**** "state" variable so as not to get confused  *****
                ;**** with any other values. Adopt IDL practice   *****
                ;**** of using first child widget                 *****
                ;******************************************************

  first_child = widget_base(main)

  cwid = widget_base(first_child,/row)

		;**** Left hand base ****

  lfid = widget_base(cwid,/column)

		;**** Right hand base ****

  rgid = widget_base(cwid,/column)

		;**** Create button to activate graphing ****

  base = widget_base(lfid,/column,/nonexclusive)
  actid = widget_button(base,value=output,font=font)

		;**** Graph title ****

  tibasid = widget_base(lfid,/row)
  rc = widget_label(tibasid,value='Graph Title',font=font)
  titleid = widget_text(tibasid,value=gr804val.gtit1, 			$
			/editable,xsize=32,font=font)

		;**** Scaling and ranges widgets ****
                ;**** With a new rows to put them side by side ****

  newrow = widget_base(lfid,/row)
  if gr804val.scalbut1 ge 0 then begin
    scbasid1 = widget_base(newrow,/column,/frame)
    rngval1 = {  SCALBUT:gr804val.scalbut1, 				$
		XMIN:gr804val.xmin1, XMAX:gr804val.xmax1, 		$
		YMIN:gr804val.ymin1, YMAX:gr804val.ymax1, 		$
                GRPRMESS:gr804val.grprmess}
    rangeid1 = cw_adas_ranges(scbasid1, SIGN=sign, VALUE=rngval1, 	$
                              BELOW=1, FONT=font, TITLE=label1) 
  end else begin
    scbasid1 = 0L
  end
  if gr804val.scalbut2 ge 0 then begin
    scbasid2 = widget_base(newrow,/column,/frame)
    rngval2 = {  SCALBUT:gr804val.scalbut2, 				$
		XMIN:gr804val.xmin2, XMAX:gr804val.xmax2, 		$
		YMIN:gr804val.ymin2, YMAX:gr804val.ymax2, 		$
                GRPRMESS:gr804val.grprmess}
    rangeid2 = cw_adas_ranges(scbasid2, SIGN=sign, VALUE=rngval2, 	$
                              BELOW=1, FONT=font, TITLE=label2) 
  end else begin
    scbasid2 = 0L
  end


		;**** Output file name widget ****

  if gr804val.hrdout ge 0 then begin
    outfval = { OUTBUT:gr804val.hrdout, APPBUT:-1, REPBUT:0, 		$
                FILENAME:gr804val.hardname, DEFNAME:gr804val.grpdef, 	$
	        MESSAGE:gr804val.grpfmess, ROOT:'' }
    fbaseid = widget_base(lfid,/row,/frame)
    fileid = cw_adas_outfile(fbaseid, OUTPUT='Enable Hard Copy', 	$
                              VALUE=outfval, FONT=font)
  end else begin
    fbaseid = 0L
    fileid = 0L
  end


		;**** Add selection list if required ****

  if strtrim(grplist(0)) ne '' then begin
    lsbasid = widget_base(rgid,/column)
    listid = cw_single_sel(lsbasid,grplist,title=listtitle, 		$
				value=gr804val.grpsel, font=font)
  end else begin
    gr804val.grpsel = -1
    lsbasid = 0L
    listid = 0L
  end

		;**** Add hardcopy device selection list ****

  if gr804val.hrdout ge 0 and strtrim(devlist(0)) ne '' then begin
    devbasid = widget_base(rgid,/column)
    devid = cw_single_sel(devbasid,devlist,title='Select Device', 	$
				value=gr804val.devsel, font=font)
  end else begin
    gr804val.devsel = -1
    devbasid = 0L
    devid = 0L
  end

		;**** Add message widget ****

  if strtrim(gr804val.grselmess) ne '' then begin
    message = gr804val.grselmess
  end else begin
    message = ' '
  end
  messid = widget_label(lfid,value=message,font=font)

		;**** Set initial state according to value ****

  if gr804val.outbut eq 1 then begin

    widget_control,actid,set_button=1

  end else begin

    widget_control,tibasid,sensitive=0

    if scbasid1 gt 0 then begin
      widget_control,scbasid1,sensitive=0
      rngval1.scalbut = 0
      widget_control,rangeid1,set_value=rngval1
    end
    if scbasid2 gt 0 then begin
      widget_control,scbasid2,sensitive=0
      rngval2.scalbut = 0
      widget_control,rangeid2,set_value=rngval2
    end

    widget_control,fbaseid,sensitive=0
    outfval.outbut = 0
    widget_control,fileid,set_value=outfval

    if listid gt 0 then widget_control,lsbasid,sensitive=0

    if devbasid gt 0 then widget_control,devbasid,sensitive=0

  end


		;**** Create state structure ****

  new_state = { ACTID:actid, 						$
		TIBASID:tibasid, TITLEID:titleid, 			$
		SCBASID1:scbasid1, RANGEID1:rangeid1, 			$
		SCBASID2:scbasid2, RANGEID2:rangeid2, 			$
		FBASEID:fbaseid, FILEID:fileid, 			$
		LSBASID:lsbasid, LISTID:listid, 			$
		DEVBASID:devbasid, DEVID:devid, MESSID:messid, 		$
		gr804val:gr804val, FONT:font }

		;**** Save initial state structure ****

  widget_control, first_child, set_uvalue=new_state,/no_copy

  RETURN, main

END
