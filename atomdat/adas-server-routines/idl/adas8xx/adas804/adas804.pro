; SCCS info: Module @(#)$Header: /home/adascvs/idl/adas8xx/adas804/adas804.pro,v 1.1 2004/07/06 11:05:49 whitefor Exp $ Date $Date: 2004/07/06 11:05:49 $
;+
; project:
;       adas ibm mvs to unix conversion and development
;
; name:
;	adas804
;
; purpose:
;	the highest level routine for the adas804 program.
;
; explanation:
;	this routine is called from the main adas system routine, adas.pro,
;	to start the adas804 application.  associated with adas804.pro
;	is a fortran executable.  the idl code provides the user
;	interface and output graphics whilst the fortran code reads
;	in data files, performs numerical processing and creates the
;	output files.  the idl code communicates with the fortran
;	executable via a bi-directional unix pipe.  the unit number
;	used by the idl for writing to and reading from this pipe is
;	allocated when the fortran executable is 'spawned' (see code
;	below).  pipe communications in the fortran process are to
;	stdin and stdout, i.e streams 5 and 6.
;
;	the fortran code is an independent process under the unix system.
;	the idl process can only exert control over the fortran in the
;	data which it communicates via the pipe.  the communications
;	between the idl and fortran must be exactly matched to avoid
;	input conversion errors.  the correct ammounts of data must be
;	passed so that neither process 'hangs' waiting for communications
;	which will never occur.
;
;	the fortran code performs some error checking which is
;	independent of idl.  in cases of error the fortran may write
;	error messages.  to prevent these error messages from conflicting
;	with the pipe communications all fortran errors are written to
;	output stream 0, which is stderr for unix.  these error messages
;	will appear in the window from which the adas session/idl session
;	is being run.
;
;	in the case of severe errors the fortran code may terminate
;	itself prematurely.  in order to detect this, and prevent the
;	idl program from 'hanging' or crashing, the idl checks to see
;	if the fortran executable is still an active process before
;	each group of pipe communications.  the process identifier
;	for the fortran process, pid, is recorded when the process is
;	first 'spawned'.  the system is then checked for the presence
;	of the fortran pid.
;
; use:
;	first the system settings must be established by calling
;	adas_sys_set.pro then adas804.pro is called to start the
;	adas804 application;
;
;	adas_sys_set, adasrel, fortdir, userroot, centroot, $
;		      devlist, devcode, font_large, font_small, edit_fonts
;	adas804,   adasrel, fortdir, userroot, centroot, devlist, $
;		   devcode, font_large, font_small, edit_fonts
;
; inputs:
;	adasrel - a string indicating the adas system version, 
;		  e.g ' adas release: adas93 v1.1.0'.  the first
;		  character should be a space.
;
;	fortdir - a string holding the path to the directory where the
;		  fortran executables are, e.g '/disk/adas/fortran/exec'
;
;	userroot - a string holding the path to the root directory of
;		   the user's adas data. e.g '/disk/bowen/adas'
;		   this root directory will be used by adas to construct
;		   other path names.  for example the users default data
;		   for adas804 should be in /disk/bowen/adas/default.
;		   in particular the user's default interface settings will
;		   be stored in the directory userroot+'/defaults'.  an
;		   error will occur if the defaults directory does not
;		   exist.
;
;	centroot - like userroot, but this directory points to the
;		   central data area for the system.  user defaults are
;		   not stored on centroot.
;
;	devlist  - a string array of hardcopy device names, used for
;		   graphical output. e.g ['post-script','hp-pcl','hp-gl']
;		   this array must mirror devcode.  devcode holds the
;		   actual device names used in a set_plot statement.
;
;	devcode  - a string array of hardcopy device code names used in
;		   the set_plot statement, e.g ['ps', 'pcl', 'hp']
;		   this array must mirror devlist.
;
;	font_large - the name of a larger font e.g 'courier_bold14'
;
;	font_small - the name of a smaller font e.g 'courier_bold12'
;
;	edit_fonts - a structure of two fonts used in the table editor
;		     adas_edtab.pro, {font_norm:'',font_input:''}.
;		     the two fonts are used to differentiate between
;		     editable and non-editable parts of the table. you
;		     may have to experiment to get two fonts which give
;		     the same default spacing in the edit widget.
;		     e.g {font_norm:'helvetica_bold14', $
;			  font_input:'helvetica_oblique14'}
;
; optional inputs:
;	none
;
; outputs:
;	none - note this routine should leave all inputs un-modified.
;
; optional outputs:
;	none
;
; keyword parameters:
;	none
;
; calls:
;	find_process	checks to see if a given process is active.
;	h4spf0		pipe comms with fortran h4spf0 routine.
;	h4ispf		pipe comms with fortran h4ispf routine.
;	xxdate		get date and time from operating system.
;	h4spf1		pipe comms with fortran h4spf1 routine.
;       h4outga         pipe comms with fortran h4outga routine
;       h4outgb         pipe comms with fortran h4outgb routine
;
; side effects:
;	this routine spawns a fortran executable.  note the pipe 
;	communications routines listed above.  in addition to these
;	pipe communications there is one explicit communication of the
;	date to the fortran code, search for 'printf,pipe' to find it.
;
; category:
;	adas system.
;	
; written:
;       hugh summers, univ.of strathclyde, 08-june-2002
;
; version:
;       1.1     hugh p summers
;		first release
;	
; modified:
;	1.1	08-06-02
;
;-
;-----------------------------------------------------------------------------


pro adas804,	adasrel, fortdir, userroot, centroot, 			$
		devlist, devcode, font_large, font_small, edit_fonts

		;************************
		;**** initialisation ****
		;************************

    adasprog = ' program: adas804 v1.1'
    lpend = 0
    gomenu = 0
    deffile = userroot+'/defaults/adas804_defaults.dat'
    device = ''
    bitfile = centroot + '/bitmaps'

		;******************************************
		;**** search for user default settings ****
		;**** if not found create defaults     ****
		;******************************************

    files = findfile(deffile)
    if files(0) eq deffile then begin
        restore, deffile
        inval.centroot = centroot+'/arch804/'
        inval.userroot = userroot+'/arch804/'
    end else begin
        inval = { 							$
    		  rootpath:userroot+'/arch804/', 			$
		  file:'', 						$
		  centroot:centroot+'/arch804/', 			$
		  userroot:userroot+'/arch804/',			$
                  refresh:''}

        procval = {new:-1}
        outval = { 							$
		    grpout:0, gtit1:'', 				$
                    grpscal1:0, 					$
		    xmin1:'',  xmax1:'',   				$
		    ymin1:'',  ymax1:'',   				$
                    grpscal2:0, 					$
		    xmin2:'',  xmax2:'',   				$
		    ymin2:'',  ymax2:'',   				$
                    anopt:-1, 			  			$
                    bcval:1.5, 			  			$
     		    hrdout:0, hardname:'', 				$
		    grpdef:'', grpfmess:'', 				$
		    grpsel:-1, grprmess:'', 				$
		    devsel:-1, grselmess:'', 				$
		    texout:0, texapp:-1, 				$
		    texrep:0, texdsn:inval.rootpath+'paper.txt',	$
		    texdef:'',texmes:'' 				$
                 }
    end

                ;****************************
                ;**** start fortran code ****
                ;****************************

    spawn, fortdir+'/adas804.out', unit=pipe, /noshell, pid=pid

		;************************************************
		;**** get date and write to fortran via pipe ****
		;************************************************

    date = xxdate()
    printf, pipe, date(0)

label100:
		;**** check fortran still running ****
  
    if find_process(pid) eq 0 then goto, labelend

		;************************************************
		;**** communicate with h4spf0 in fortran and ****
		;**** invoke user interface widget for       ****
		;**** data file selection                    ****
		;************************************************

    lchoice = -1

    h4spf0, pipe, inval, dsfull, lchoice, rep, font=font_large

		;**** if cancel selected then end program ****

    if rep eq 'yes' then goto, labelend

label200:


    if find_process(pid) eq 0 then goto, labelend
    
    h4ispf, pipe, lpend, procval, dsfull, lchoice, 			$
            iwhc, bitfile, gomenu,                                      $
            font_large=font_large, font_small=font_small, 		$
	    edit_fonts=edit_fonts 

                ;**** if menu button clicked, tell fortran to stop ****

    if gomenu eq 1 then begin
        printf, pipe, 1
        goto, labelend
    endif else begin
        printf, pipe, 0
    endelse

		;**** if cancel selected then go back to ****
                ;**** the previous interface window ****
  
    if lpend eq 1 then goto, label100

		;************************************************
                ;**** if refresh selected then replace adas *****
                ;**** analysis options defaults with those  *****
                ;**** from the archive                      *****
		;************************************************

;    if lchoice eq 2 then begin
;        if iwhc eq 1 then begin
;            outval.axop = ixops
;            outval.adif = idiff
;            outval.adopt = ifpts
;            outval.bdptopt = ibpts
;        endif else begin
;            outval.bcval = bcval
;        endelse
;    endif                

                ;**** create header for output ****

    header=adasrel+adasprog+' date: '+date(0)+' time: '+date(1)

label300:

    if find_process(pid) eq 0 then goto, labelend

    h4spf1, pipe, lpend, inval, procval, outval, dsfull, header, 	$
            bitfile, gomenu, devlist=devlist, font=font_small  

                ;**** if menu button clicked, tell fortran to stop ****

    if gomenu eq 1 then begin
        printf, pipe, 1
        goto, labelend
    endif else begin
        printf, pipe, 0
    endelse
     
		;**** if cancel selected then go back to ****
                ;**** the previous interface window ****
  
    if lpend eq 1 then goto, label200

                ;************************************** 
                ;**** set up graphic displays for  ****
                ;**** chosen option                ****
                ;************************************** 

    if outval.devsel ge 0 then device = devcode(outval.devsel)
    titlx = strtrim(dsfull,2)
    if outval.grpout eq 1 then begin
        if outval.anopt eq 1 then begin
    	    if find_process(pid) eq 0 then goto, labelend
            h4outg, pipe, lpend, lchoice, inval.rootpath, outval, 	$
                     procval, device, date, header, titlx, bitfile, 	$
		     gomenu, font = font_large
            wait, 1.0 
        endif else begin
    	    if find_process(pid) eq 0 then goto, labelend
            h4outg, pipe, lpend, lchoice, inval.rootpath, outval, 	$
                     procval, device, date, header, titlx, bitfile, 	$
		     gomenu, font = font_large
            wait, 1.0
        endelse
		;**** if menu button clicked, tell fortran to stop ****

     	if gomenu eq 1 then begin
            printf, pipe, 1
            goto, labelend
    	endif else begin
            printf, pipe, 0
    	endelse

    endif

    if lpend eq 1 or lpend eq 2 then goto, label300

labelend:

		;**** ensure appending is not enabled for ****
		;**** text output at start of next run.   ****

   outval.texapp = -1

		;**** save user defaults ****

    save, inval, procval, outval, filename=deffile

end
