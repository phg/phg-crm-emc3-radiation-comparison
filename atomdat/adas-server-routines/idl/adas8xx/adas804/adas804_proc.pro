; SCCS info: Module @(#)$Header: /home/adascvs/idl/adas8xx/adas804/adas804_proc.pro,v 1.1 2004/07/06 11:06:01 whitefor Exp $ Date $Date: 2004/07/06 11:06:01 $
;+
; project:
;       adas ibm mvs to   unix conversion
;
; name:
;	adas804_proc
;
; purpose:
;	idl adas user interface, processing options/input.
;
; explanation:
;	this routine creates and manages a pop-up window which allows
;	the user to select options and input data to control adas804
;	processing.
;
; use:
;	this routine is adas804 specific, see a1ispf.pro for how it
;	is used.
;
; inputs:
;	procval	- a structure which determines the initial settings of
;		  the processing options widget.  the value is passed
;		  unmodified into cw_adas804_proc.pro.
;
;		  see cw_adas804_proc.pro for a full description of this
;		  structure.
;
;	dsfull	- string; the full system file name of the input 
;		          dataset selected by the user for processing.
;
;	lchoice - integer: the archiving option, see h4spf0.pro 
;
;       bitfile - string; the path to the directory containing bitmaps
;                 for the 'escape to series menu' button.
;
; optional inputs:
;	none
;
; outputs:
;	procval	- on output the structure records the final settings of
;		  the processing selections widget if the user pressed the
;		  'done' button, otherwise it is not changed from input.
;
;	act	- string; either 'done', 'cancel' or 'menu' for the button the
;		  user pressed to terminate the processing options
;		  window.
;
; optional outputs:
;	none
;
; keyword parameters:
;	font_large - the name of a larger font.
;
;	font_small - the name of a smaller font.
;
;	edit_fonts - a structure of two fonts used in the table editor
;		     adas_edtab.pro, {font_norm:'',font_input:''}.
;
; calls:
;	cw_adas804_proc	declares the processing options widget.
;	adas804_proc_ev called indirectly during widget management,
;			routine included in this file.
;	xmanager
;
; side effects:
;       this widget uses a common block proc101_blk, to pass the
;       variables value and action between the two routines. 
;
; category:
;	widgets
;
; written:
;       hugh summers, univ.of strathclyde, 28-june-2002
;
; version:
;       1.1     hugh p summers
;		first release
;	
; modified:
;	1.1	28-06-02
;
;-----------------------------------------------------------------------------


pro adas804_proc_ev, event


  common proc804_blk,action,value

    action = event.action
    case event.action of

		;**** 'done' button ****

	'done'  : begin

		;**** get the output widget value ****

		      widget_control,event.id,get_value=value

		      widget_control,event.top,/destroy

	           end


		;**** 'cancel' button ****

	'cancel': widget_control,event.top,/destroy

		;**** 'menu' button ****

	'menu': widget_control,event.top,/destroy

    end

end

;-----------------------------------------------------------------------------


pro adas804_proc, procval, dsfull, lchoice, 				$
  		  ixdim,  itdim  , isdim , xa , tea, 			$
		  act, bitfile,	        				$
		  font_large=font_large, font_small=font_small, 	$
		  edit_fonts=edit_fonts


		;**** declare common variables ****

  common proc804_blk,action,value

		;**** copy "procval" to common ****

  value = procval

		;**** set defaults for keywords ****

  if not (keyword_set(font_large)) then font_large = ''
  if not (keyword_set(font_small)) then font_small = ''
  if not (keyword_set(edit_fonts)) then 				$
				edit_fonts = { font_norm:'', font_input:''}
		
                ;**** create base widget ****

  procid = widget_base(title='adas804: processing options', 		$
					xoffset=200,yoffset=0)

		;**** declare processing widget ****

  cwid = cw_adas804_proc(procid, dsfull,lchoice,			$
  		         ixdim,  itdim  , isdim , xa , tea,		$ 			$
  			 bitfile,					$
			 procval=value, 				$
		 	 font_large=font_large, font_small=font_small, 	$
			 edit_fonts=edit_fonts)

		;**** realize the new widget ****

  dynlabel, procid
  widget_control,procid,/realize

		;**** make widget modal ****

  xmanager,'adas804_proc',procid,event_handler='adas804_proc_ev', 	$
					/modal,/just_reg

		;*** copy value back to procval for return to h4ispf ***

  act = action
  procval = value
 
end

