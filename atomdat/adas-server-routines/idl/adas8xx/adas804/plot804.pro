; SCCS info: Module @(#)$Header: /home/adascvs/idl/adas8xx/adas804/plot804.pro,v 1.1 2004/07/06 14:35:18 whitefor Exp $ Date $Date: 2004/07/06 14:35:18 $
;+
; PROJECT:
;       ADAS IBM MVS to UNIX conversion
;
; NAME:
;	PLOT804
;
; PURPOSE:
;	Plot one graph for ADAS804.
;
; EXPLANATION:
;	This routine plots ADAS804 output for a single graph.
;
; USE:   
;
; INPUTS:
;	X	- Float Array; List of x-values
;
;	Y	- Float Array; list of y-values
;
;       NSP     - Integer; Number of spline points for ADAS branch.
;
;	TITLE	- String array : General title for program run. 5 lines.
;
;	XTITLE  - String : title for x-axis annotation
;
;	YTITLE  - String : title for y-axis annotation
;
;	LDEF1	- Integer; 1 - User specified axis limits to be used, 
;		  	   0 - Default scaling to be used.
;
;	XMIN	- String; Lower limit for x-axis of graph, number as string.
;
;	XMAX	- String; Upper limit for x-axis of graph, number as string.
;
;	YMIN	- String; Lower limit for y-axis of graph, number as string.
;
;	YMAX	- String; Upper limit for y-axis of graph, number as string.
;
;       B       - Float Array; Contains the y-values of the Burgess knot
;                              points.
;
; OPTIONAL INPUTS:
;	None.
;
; OUTPUTS:
;	None.
;
; OPTIONAL OUTPUTS:
;	None.
;
; KEYWORD PARAMETERS:
;	None.
;
; CALLS:
;       LOCALIZE        Routine to set the various position parameters
;                       used in output plotting.
;
; SIDE EFFECTS:
;	None.
;
; CATEGORY:
;	Adas system.	
;
; WRITTEN: 
;       Hugh P Summers, Univ.of Strathclyde,  27-07-02
;
; MODIFIED:
;	1.1	Hugh P Summers
;		First release
; VERSION:
;	1.1	27-07-02
;
;-
;----------------------------------------------------------------------------

PRO plot804, x , y,  title, xtitle, ytitle, 				$
             xmin, xmax, ymin, ymax, nsp, ldef1, b
                
  COMMON Global_lw_data, left, right, top, bottom, grtop, grright

                ;****************************************************
		;**** Suitable character size for current device ****
		;**** Aim for 60 characters in y direction.      ****
		;****************************************************

  charsize = (!d.y_vsize/!d.y_ch_size)/60.0
  small_check = GETENV('VERY_SMALL')
  if small_check eq 'YES' then charsize=charsize*0.8

		;**** set makeplot counter ****

  makeplot = 1

		;**** set values of window area positions ****

;  localize

		;**** Window area positions ****

 left = 0.1
 right = 0.9
 top = 0.91
 bottom = 0.1
 grtop = 0.8
 grright = 0.70

		;***************************************
		;**** Construct graph title         ****
		;**** "!C" is the new line control. ****
		;***************************************

  !p.font=-1
  if small_check eq 'YES' then begin
      gtitle =  title(0) + "!C!C" + title(1) + "!C!C" + title(2) + "!C!C" + $
        title(3) + "!C!C" + title(4)
  endif else begin
      gtitle =  title(0) + "!C" + title(1) + "!C" + title(2) + "!C" + $
        title(3) + "!C" + title(4)
  endelse
		;**** Find x and y ranges for auto scaling ****

  if ldef1 eq 0 then begin
		;**** identify values in the valid range ****
		;**** plot routines only work within ****
		;**** single precision limits.	     ****
    xvals = where(x gt 1.0e-37 and x lt 1.0e37)
    yvals = where(y gt 1.0e-37 and y lt 1.0e37)

    if xvals(0) gt -1 then begin
      makeplot = 1
      xmax = max(x(xvals), min=xmin)
      end else begin
      makeplot = 0
    end

    if yvals(0) gt -1 then begin
      makeplot = 1
      ymax = max(y(yvals), min=ymin)
    end else begin
      makeplot = 0
    end

    style = 0
  end else begin
		;Check that at least some data in in axes range ***
    xvals = where(x ge xmin and x le xmax)
    yvals = where(y ge ymin and y le ymax)
    if xvals(0) eq -1 or yvals(0) eq -1 then begin
      makeplot = 0
    end
    style = 1
  end


		;**** Set up log-log plotting axes ****

    plot,   [xmin,xmax],[ymin,ymax],/nodata,ticklen=1.0, 		$
		position=[left,bottom,grright,grtop], 			$
		xtitle=xtitle, ytitle=ytitle, xstyle=style, ystyle=style, $
		charsize=charsize

                ;**** spline option ****

  if nsp ne -1 then begin
    fact = nsp/(max(x)-min(x))
    t = findgen(nsp+1)/fact+min(x)
    z = spline( x, y, t)
  end

  if makeplot eq 1 then begin

		;**********************
		;****  Make plots  ****
		;**********************

      oplot, x, y, psym = 4
     if b(0) ne -1 then begin
       oplot, x, y, linestyle = 2
     end else begin
       oplot, x, y
     end
      if nsp ne -1 then oplot, t, z
      if b(0) ne -1 then begin                      ; Burgess Option
        z = fltarr(70)
        k = [0.0,0.25,0.5,0.75,1.0]
        oplot, k, b, psym = 7
        fact = 70/(max(x)-min(x))
        t = findgen(71)/fact+min(k)
        for i = 0,69 do begin
          z(i) = sp5(t(i),b)
        endfor
        oplot, t, z
      end

  endif else begin
     print, "ADAS804 : No data found in these axes ranges"
  endelse

		;**** plot title ****

      xyouts,(left-0.05), top, gtitle, /normal, alignment=0.0, 		$
			charsize=charsize*0.95

END
