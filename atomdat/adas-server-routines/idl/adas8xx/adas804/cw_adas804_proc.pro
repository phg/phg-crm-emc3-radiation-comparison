;+
; Project:
;       adas
;
; Name:
;       cw_adas804_proc()
;
; Purpose:
;       produces a widget for adas804 processing options/input.
;
; Explanation:
;       this function creates a compound widget consisting of :-
;          a text widget holding an editable 'run title', 
;          the dataset name/browse widget cw_adas_dsbr,
;          a template compound widget for adding information about 
;          the transiton, 
;          a table widget for temperature/density data cw_adas_table,
;          buttons to enter default values into the table, 
;          a message widget, a 'cancel' and a 'done' button, and
;          energy units options buttons.
;
;       the compound widgets included in this widget are self managing.
;       this widget only manages events from the two 'defaults' buttons,
;       the 'cancel' button and the 'done' button, and from the energy units
;       buttons.
;
;       this widget only generates events for the 'done' and 'cancel'
;       buttons.
;
; Use:
;       this widget is specific to adas804, see adas804_proc.pro
;       for use.
;
; Inputs:
;       topparent- long integer, id of parent widget.
;
;       dsfull  - string; the full system file name of the input 
;                 dataset selected by the user for processing.
;
;       lchoice - integer; the archiving choice of the user, see 
;                 h4spf0.pro
;
;       bitfile - string; the path to the directory containing bitmaps
;                 for the 'escape to series menu' button.
;
; Optional inputs:
;       none
;
; Outputs:
;       the return value of this function is the id of the declared widget.
;
; Optional outputs:
;       none
;
; Keyword parameters:
;       procval - a structure which determines the initial settings of
;                 the processing options widget.
;
;                 the default procval is created thus;
;
;     shel_arr = intarr(isdim)
;     alfa_arr = fltarr(2,isdim)
;     ener_arr = fltarr(ixdim)
;     temp_arr = fltarr(itdim)
;     ifout_arr = intarr(2)
;
;     ps = {                                      $
;                new    : 0,                      $
;                title  : ' ',                    $
;                ifout  : ifout_arr ,             $
;                z      : 0.,                     $
;                z0     : 0.,                     $
;                zeff   : 0.,                     $
;                n1     : 0,                      $
;                l1     : 0,                      $
;                eb1    : 0.,                     $
;                n2     : 0,                      $
;                l2     : 0,                      $
;                eb2    : 0.,                     $
;                isp    : 0,                      $
;                lp     : 0,                      $
;                xjp    : 0.,                     $
;                ist1   : 0,                      $
;                lt1    : 0,                      $
;                xj1    : 0.,                     $
;                xjt1   : 0.,                     $
;                ist2   : 0,                      $
;                lt2    : 0,                      $
;                xj2    : 0.,                     $
;                xjt2   : 0.,                     $
;                neqv1  : 0,                      $
;                fpc1   : 0.,                     $
;                neqv2  : 0,                      $
;                fpc2   : 0.,                     $
;                aval   : 0.,                     $
;                xmax   : 0.,                     $
;                iextwf : 0,                      $
;                ijucys : 0,                      $
;                isrch  : 0,                      $
;                nshell : 0,                      $
;                ns     : shel_arr,               $
;                ls     : shel_arr,               $
;                iqs    : shel_arr,               $
;                alfaa  : alfa_arr,               $
;                nx     : 0,                      $
;                xin    : ener_arr,               $
;                nt     : 0,                      $
;                tin    : temp_arr                $
;               }

;               new      = flag which defines whether or not default 
;                          values exist or not. (< 0 if no values exist)
;               ifout    = array containing two values indicating which 
;                          units are being used 
;               z0       = nuclear charge
;               z        = ion charge
;               zeff     = ion charge +1
;               n1       = lower n-shell of transition
;               l1       = lower l-shell of transition
;               eb1      = binding energy (Ryd) in lower level
;               n2       = upper n-shell of transition
;               l2       = upper l-shell of transition
;               eb2      = binding energy (Ryd) in upper level
;               isp      = 2*Sp+1 for parent
;               lp       = Lp for parent
;               xjp      = Jp for parent (if 'ic' coupling)
;               ist1     = 2*S+1 for lower state
;               lt1      = L for lower state
;               xj1      = j for lower state
;               xjt1     = J for lower state
;               ist2     = 2*S'+1 for upper state
;               lt2      = L' for upper state
;               xj2      = j' for upper state
;               xjt2     = J' for upper state
;               neqv1    = no. of equiv. electrons for lower shell. 
;               fpc1     = fract. parentage for lower state
;               neqv2    = no. of equiv. electrons for upper shell. 
;               fpc2     = fract. parentage for upper state
;               aval     = A-value (sec-1) if dipole; else -ve
;               xmax     = range of numerical wave functions
;               iextwf   = 0 => calculate radial wave functions 
;                      = 1 => read in radial wave functions
;               ijucys   = -1 => Jucys potential form adopted
;                        = 0  => Slater potential form adopted
;               ijucys   = -1 => Jucys potential form adopted
;                        = 0  => Slater potential form adopted
;               isrch    = 0 => fcf6 search for energy eigenvalue 
;                        = 1 => fcf6 search for scaling parameters
;               nshell   = number of screening shells
;               ns       = n for each screening shell
;                            1st dim: screening shell index
;               ls       = l for each screening shell
;                            1st dim: screening shell index
;               iqs      = iq for each screening shell
;                            1st dim: screening shell index
;               alfaa    = scaling factor for each screening shell
;                            1st dim: index for lower & upper state                            
;                            2nd dim: index over screening shells
;               nx       = number of incident electron energies
;               xin()    = user supplied energy parameter values 
;               nt       = number of electron temperatures
;               tin()    = user supplied electron temperatures
;
;       uvalue  - a user value for the widget. default 0.
;
;       font_large - the name of a larger font.  default current system
;                    font
;
;       font_small - the name of a smaller font. default current system
;                    font.
;
;       edit_fonts - a structure of two fonts used in the table editor
;                    adas_edtab.pro, {font_norm:'',font_input:''}.  defaults
;                    to current system font.
;
;       num_form   - string; numeric format to use in tables.  default
;                       '(e10.3)'
;                    
;
; Calls:
;       popup           popup warning window with buttons.
;       cw_adas_dsbr    dataset name and comments browsing button.
;       cw_adas_table   adas data table widget.
;       cw_adas_tmpl         transition information
;       see side effects for other related widget management routines.
;
; Side effects:
;
;       the following widget management routines are included in this file;
;       proc804_get_val()       returns the current procval structure.
;       proc804_event()         process and issue events.
;
; Category:
;       compound widget
;
; Written:
;       Hugh Summers, Univ. of Strathclyde, 28-June-2002
;
; Version:
;       1.1     Hugh P Summers
;                - First release
;       1.2     Hugh P Summers
;                - Remove errors in getting data from widget fields.
;                - Some restructuring to fit widget on smaller screens. 
;
; Modified:
;	1.1	08-06-2002
;	1.2	25-05-2004
;
;
;-----------------------------------------------------------------------------

function proc804_get_val, id

                ;**** return to caller on error ****
  on_error, 2

		 ;***************************************
                 ;****     retrieve the state 	     ****
		 ;**** get first_child widget id     ****
		 ;**** because state is stored there ****
		 ;***************************************

  first_child = widget_info(id, /child)
  widget_control, first_child, get_uvalue = state

		;***********************************
		;**** Get run title from widget ****
		;**** Then centre in on string  ****
		;**** of 40 characters 		****
		;***********************************

    widget_control, state.runid, get_value=title
    title = strcompress(title(0))
    title_len = strlen(title) 
    if (title_len gt 40 ) then begin 
        title = strmid(title, 0, 37)
        widget_control, state.messid, set_value="Title too long - truncated "
        widget_control, state.runid, set_value=title
	wait, 1
    endif
    pad = (40 - title_len)/2 
    spaces = '                                         '
    title(0) = strmid(spaces,0,(pad-1)) + title + strmid(spaces,0,(pad-1))

               ;***********************************************
               ;**** Get value of Z0                        ***
               ;***********************************************

    widget_control, state.z0text, get_value=z0val 
    if (num_chk(z0val(0)) eq 0) then begin
        z0 = float(z0val(0))
    endif else begin
        z0 = -99.0
    endelse
    widget_control, state.z0text, set_value=z0val 

               ;***********************************************
               ;**** Get value of Z                         ***
               ;***********************************************

    widget_control, state.ztext, get_value=zval 
    if (num_chk(zval(0)) eq 0) then begin
        z = float(zval(0))
    endif else begin
        z = -99.0
    endelse
    widget_control, state.ztext, set_value=zval 

               ;***********************************************
               ;**** Get value of Zeff                      ***
               ;***********************************************

    widget_control, state.zefftext, get_value=zeffval 
    if (num_chk(zeffval(0)) eq 0) then begin
        zeff = float(zeffval(0))
    endif else begin
        zeff = -99.0
    endelse
    widget_control, state.zefftext, set_value=zeffval 

               ;***********************************************
               ;**** Get value of isp                       ***
               ;***********************************************

    widget_control, state.isptext, get_value=ispval 
    if (num_chk(ispval(0)) eq 0) then begin
        isp = ispval(0)
    endif else begin
        isp = -99
    endelse
    widget_control, state.isptext, set_value=ispval 

               ;***********************************************
               ;**** Get value of lp                       ***
               ;***********************************************

    widget_control, state.lptext, get_value=lpval 
    if (num_chk(lpval(0)) eq 0) then begin
        lp = lpval(0)
    endif else begin
        lp = -99
    endelse
    widget_control, state.lptext, set_value=lpval 

               ;***********************************************
               ;**** Get value of xjp                       ***
               ;***********************************************

    widget_control, state.xjptext, get_value=xjpval 
    if (num_chk(xjpval(0)) eq 0) then begin
        xjp = float(xjpval(0))
    endif else begin
        xjp = -99.0
    endelse
    widget_control, state.xjptext, set_value=xjpval 

               ;***********************************************
               ;**** Get value of n1                       ***
               ;***********************************************
    if(xjp lt 0) then begin
        widget_control, state.n1lstext, get_value=n1val
    endif else begin 
        widget_control, state.n1ictext, get_value=n1val
    endelse
    if (num_chk(n1val(0)) eq 0) then begin
        n1 = n1val(0)
    endif else begin
        n1 = -99
    endelse
    if(xjp lt 0) then begin
        widget_control, state.n1lstext, set_value=n1val 
    endif else begin 
        widget_control, state.n1ictext, set_value=n1val 
    endelse
    
               ;***********************************************
               ;**** Get value of l1                       ***
               ;***********************************************

    if(xjp lt 0) then begin
        widget_control, state.l1lstext, get_value=l1val 
    endif else begin 
        widget_control, state.l1ictext, get_value=l1val 
    endelse
    if (num_chk(l1val(0)) eq 0) then begin
        l1 = l1val(0)
    endif else begin
        l1 = -99
    endelse
    if(xjp lt 0) then begin
        widget_control, state.l1lstext, set_value=l1val 
    endif else begin 
        widget_control, state.l1ictext, set_value=l1val 
    endelse
    
               ;***********************************************
               ;**** Get value of ist1                       ***
               ;***********************************************

    widget_control, state.ist1text, get_value=ist1val 
    if (num_chk(ist1val(0)) eq 0) then begin
        ist1 = ist1val(0)
    endif else begin
        ist1 = -99
    endelse
    widget_control, state.ist1text, set_value=ist1val 
    
               ;***********************************************
               ;**** Get value of lt1                       ***
               ;***********************************************

    widget_control, state.lt1text, get_value=lt1val 
    if (num_chk(lt1val(0)) eq 0) then begin
        lt1 = lt1val(0)
    endif else begin
        lt1 = -99
    endelse
    widget_control, state.lt1text, set_value=lt1val 

               ;***********************************************
               ;**** Get value of xj1                       ***
               ;***********************************************

    widget_control, state.xj1text, get_value=xj1val 
    if (num_chk(xj1val(0)) eq 0) then begin
        xj1 = float(xj1val(0))
    endif else begin
        xj1 = -99.0
    endelse
    widget_control, state.xj1text, set_value=xj1val 

               ;***********************************************
               ;**** Get value of xjt1                      ***
               ;***********************************************

    if(xjp lt 0) then begin
        widget_control, state.xjt1lstext, get_value=xjt1val 
    endif else begin
        widget_control, state.xjt1ictext, get_value=xjt1val 
    endelse
    if (num_chk(xjt1val(0)) eq 0) then begin
        xjt1 = float(xjt1val(0))
    endif else begin
        xjt1 = -99.0
    endelse
    if(xjp lt 0) then begin
        widget_control, state.xjt1lstext, set_value=xjt1val 
    endif else begin
        widget_control, state.xjt1ictext, set_value=xjt1val 
    endelse
    
               ;***********************************************
               ;**** Get value of neqv1                       ***
               ;***********************************************

    if(xjp lt 0) then begin
        widget_control, state.neqv1lstext, get_value=neqv1val 
    endif else begin 
        widget_control, state.neqv1ictext, get_value=neqv1val 
    endelse
    if (num_chk(neqv1val(0)) eq 0) then begin
        neqv1 = neqv1val(0)
    endif else begin
        neqv1 = -99
    endelse
    if(xjp lt 0) then begin
        widget_control, state.neqv1lstext, set_value=neqv1val 
    endif else begin 
        widget_control, state.neqv1lstext, set_value=neqv1val 
    endelse

               ;***********************************************
               ;**** Get value of fpc1                       ***
               ;***********************************************

    if(xjp lt 0) then begin
         widget_control, state.fpc1lstext, get_value=fpc1val 
    endif else begin 
         widget_control, state.fpc1ictext, get_value=fpc1val 
    endelse
    if (num_chk(fpc1val(0)) eq 0) then begin
        fpc1 = float(fpc1val(0))
    endif else begin
        fpc1 = -99.0
    endelse
    if(xjp lt 0) then begin
         widget_control, state.fpc1lstext, set_value=fpc1val 
    endif else begin 
         widget_control, state.fpc1ictext, set_value=fpc1val 
    endelse
    
               ;***********************************************
               ;**** Get value of eb1                       ***
               ;***********************************************

    if(xjp lt 0) then begin
        widget_control, state.eb1lstext, get_value=eb1val 
    endif else begin 
        widget_control, state.eb1ictext, get_value=eb1val 
    endelse
    if (num_chk(eb1val(0)) eq 0) then begin
        eb1 = float(eb1val(0))
    endif else begin
        eb1 = -99.0
    endelse
    if(xjp lt 0) then begin
        widget_control, state.eb1lstext, set_value=eb1val 
    endif else begin 
        widget_control, state.eb1ictext, set_value=eb1val 
    endelse

               ;***********************************************
               ;**** Get value of n2                       ***
               ;***********************************************

    if(xjp lt 0) then begin
        widget_control, state.n2lstext, get_value=n2val 
    endif else begin 
        widget_control, state.n2ictext, get_value=n2val 
    endelse
    if (num_chk(n2val(0)) eq 0) then begin
        n2 = n2val(0)
    endif else begin
        n2 = -99
    endelse
    if(xjp lt 0) then begin
        widget_control, state.n2lstext, set_value=n2val 
    endif else begin 
        widget_control, state.n2ictext, set_value=n2val 
    endelse
    
               ;***********************************************
               ;**** Get value of l2                       ***
               ;***********************************************

    if(xjp lt 0) then begin
        widget_control, state.l2lstext, get_value=l2val 
    endif else begin 
        widget_control, state.l2ictext, get_value=l2val 
    endelse
    if (num_chk(l2val(0)) eq 0) then begin
        l2 = l2val(0)
    endif else begin
        l2 = -99
    endelse
    if(xjp lt 0) then begin
        widget_control, state.l2lstext, set_value=l2val 
    endif else begin 
        widget_control, state.l2ictext, set_value=l2val 
    endelse
    
               ;***********************************************
               ;**** Get value of ist2                       ***
               ;***********************************************

    widget_control, state.ist2text, get_value=ist2val 
    if (num_chk(ist2val(0)) eq 0) then begin
        ist2 = ist2val(0)
    endif else begin
        ist2 = -99
    endelse
    widget_control, state.ist2text, set_value=ist2val 
    
               ;***********************************************
               ;**** Get value of lt2                       ***
               ;***********************************************

    widget_control, state.lt2text, get_value=lt2val 
    if (num_chk(lt2val(0)) eq 0) then begin
        lt2 = lt2val(0)
    endif else begin
        lt2 = -99
    endelse
    widget_control, state.lt2text, set_value=lt2val 

               ;***********************************************
               ;**** Get value of xj2                       ***
               ;***********************************************

    widget_control, state.xj2text, get_value=xj2val 
    if (num_chk(xj2val(0)) eq 0) then begin
        xj2 = float(xj2val(0))
    endif else begin
        xj2 = -99.0
    endelse
    widget_control, state.xj2text, set_value=xj2val 

               ;***********************************************
               ;**** Get value of xjt2                       ***
               ;***********************************************

    if(xjp lt 0) then begin
        widget_control, state.xjt2lstext, get_value=xjt2val 
    endif else begin
        widget_control, state.xjt2ictext, get_value=xjt2val 
    endelse
    if (num_chk(xjt2val(0)) eq 0) then begin
        xjt2 = float(xjt2val(0))
    endif else begin
        xjt2 = -99.0
    endelse
    if(xjp lt 0) then begin
        widget_control, state.xjt2lstext, set_value=xjt2val 
    endif else begin
        widget_control, state.xjt2ictext, set_value=xjt2val 
    endelse
    
               ;***********************************************
               ;**** Get value of neqv2                       ***
               ;***********************************************

    if(xjp lt 0) then begin
        widget_control, state.neqv2lstext, get_value=neqv2val 
    endif else begin
        widget_control, state.neqv2ictext, get_value=neqv2val 
    endelse
    if (num_chk(neqv2val(0)) eq 0) then begin
        neqv2 = neqv2val(0)
    endif else begin
        neqv2 = -99
    endelse
    if(xjp lt 0) then begin
        widget_control, state.neqv2lstext, set_value=neqv2val 
    endif else begin
        widget_control, state.neqv2ictext, set_value=neqv2val 
    endelse

               ;***********************************************
               ;**** Get value of fpc2                       ***
               ;***********************************************

    if(xjp lt 0) then begin
        widget_control, state.fpc2lstext, get_value=fpc2val 
    endif else begin
        widget_control, state.fpc2ictext, get_value=fpc2val 
    endelse
    if (num_chk(fpc2val(0)) eq 0) then begin
        fpc2 = float(fpc2val(0))
    endif else begin
        fpc2 = -99.0
    endelse
    if(xjp lt 0) then begin
        widget_control, state.fpc2lstext, set_value=fpc2val 
    endif else begin
        widget_control, state.fpc2ictext, set_value=fpc2val 
    endelse
    
               ;***********************************************
               ;**** Get value of eb2                       ***
               ;***********************************************

    if(xjp lt 0) then begin
        widget_control, state.eb2lstext, get_value=eb2val 
    endif else begin
        widget_control, state.eb2ictext, get_value=eb2val 
    endelse
    if (num_chk(eb2val(0)) eq 0) then begin
        eb2 = float(eb2val(0))
    endif else begin
        eb2 = -99.0
    endelse
    if(xjp lt 0) then begin
        widget_control, state.eb2lstext, set_value=eb2val 
    endif else begin
        widget_control, state.eb2ictext, set_value=eb2val 
    endelse


		;****************************************************
		;**** get new temperature data from table widget ****
		;****************************************************

  widget_control,state.tempid,get_value=tempval
  tabledata = tempval.value
  ifout = intarr(2)
    for i = 0,1 do begin
      ifout(i) = tempval.units(i)+(5*i)
    endfor

		;**** copy out temperature values ****

  xin = dblarr(state.ixdim)
  tin = dblarr(state.itdim)
  blanks = where(strtrim(tabledata(ifout(0),*),2) eq '')
		;**** next line assumes that all blanks are ****
		;**** at the end of the columns             ****
  if blanks(0) ge 0 then nx=blanks(0) else nx=state.ixdim
  xin(0:nx-1) = double(tabledata(ifout(0),0:nx-1))
  blanks = where(strtrim(tabledata(ifout(1),*),2) eq '')
  if blanks(0) ge 0 then nt=blanks(0) else nt=state.itdim
  tin(0:nt-1) = double(tabledata(ifout(1),0:nt-1))

		;**** fill out the rest with zero ****

  if nx lt state.ixdim then begin
    xin(nx:state.ixdim-1) = double(0.0)
  endif 
  if nt lt state.itdim then begin
    tin(nt:state.itdim-1) = double(0.0)
  endif

                ;**** get edits from template widget ****

;  widget_control,state.infoid,get_value=values
    
               ;***********************************************
               ;**** Get value of iextwf                    ***
               ;***********************************************

    widget_control, state.fextwf, get_value=iextwfval 
    if (num_chk(iextwfval(0)) eq 0) then begin
        iextwf = fix(iextwfval(0))
    endif else begin
        iextwf = -99
    endelse
    widget_control, state.fextwf, set_value=iextwfval 
    
               ;***********************************************
               ;**** Get value of ijucys                    ***
               ;***********************************************

    widget_control, state.fjucys, get_value=ijucysval 
    if (num_chk(ijucysval(0)) eq 0) then begin
        ijucys = fix(ijucysval(0))
    endif else begin
        ijucys = -99
    endelse
    widget_control, state.fjucys, set_value=ijucysval 
    
               ;***********************************************
               ;**** Get value of isrch                     ***
               ;***********************************************

    widget_control, state.fsrch, get_value=isrchval 
    if (num_chk(isrchval(0)) eq 0) then begin
        isrch = fix(isrchval(0))
    endif else begin
        isrch = -99
    endelse
    widget_control, state.fsrch, set_value=isrchval 
    
               ;***********************************************
               ;**** Get value of xmax                       ***
               ;***********************************************

    widget_control, state.xmaxtext, get_value=xmaxval 
    if (num_chk(xmaxval(0)) eq 0) then begin
        xmax = float(xmaxval(0))
    endif else begin
        xmax = -99.0
    endelse
    widget_control, state.xmaxtext, set_value=xmaxval 
    
               ;***********************************************
               ;**** Get value of aval                       ***
               ;***********************************************

    widget_control, state.avaltext, get_value=avalval 
    if (num_chk(avalval(0)) eq 0) then begin
        aval = float(avalval(0))
    endif else begin
        aval = -99.0
    endelse
    widget_control, state.avaltext, set_value=avalval 

		;****************************************************
		;**** get new screening data from table widget ****
		;****************************************************

  widget_control,state.screentabid,get_value=tempval
  tabledata = tempval.value

		;**** copy out screening values ****

  ns = intarr(state.isdim)
  ls = intarr(state.itdim)
  iqs= intarr(state.itdim)
  alfaa= dblarr(2,state.isdim)
  blanks = where(strtrim(tabledata(0,*),2) eq '')
		;**** next line assumes that all blanks are ****
		;**** at the end of the columns             ****
  if blanks(0) ge 0 then nshell=blanks(0) else nshell=state.isdim
  ns(0:nshell-1)  = fix(tabledata(0,0:nshell-1))
  ls(0:nshell-1)  = fix(tabledata(1,0:nshell-1))
  iqs(0:nshell-1) = fix(tabledata(2,0:nshell-1))
  alfaa(0,0:nshell-1) = float(tabledata(3,0:nshell-1))
  alfaa(1,0:nshell-1) = float(tabledata(4,0:nshell-1))

		;**** fill out the rest with zero ****

  if nshell lt state.isdim then begin
    ns(nshell:state.isdim-1)  = 0
    ls(nshell:state.isdim-1)  = 0
    iqs(nshell:state.isdim-1) = 0
    alfaa(0,nshell:state.isdim-1) = 0.0
    alfaa(1,nshell:state.isdim-1) = 0.0
  endif 

		;***********************************************
		;**** write selected values to ps structure ****
		;***********************************************

     ps = { 			 		$
                new    : 0,                     $
		title  : title,             	$
		ifout  : ifout,         	$
                z      : z,                     $
                z0     : z0,                    $
                zeff   : zeff,                  $
                n1     : n1,                    $
                l1     : l1,                    $
                eb1    : eb1,                   $
                n2     : n2,                    $
                l2     : l2,                    $
                eb2    : eb2,                   $
                isp    : isp,                   $
                lp     : lp,                    $
                xjp    : xjp,                   $
                ist1   : ist1,                  $
                lt1    : lt1,                   $
                xj1    : xj1,                   $
                xjt1   : xjt1,                  $
                ist2   : ist2,                  $
                lt2    : lt2,                   $
                xj2    : xj2,                   $
                xjt2   : xjt2,                  $
                neqv1  : neqv1,                 $
                fpc1   : fpc1,                  $
                neqv2  : neqv2,                 $
                fpc2   : fpc2,                  $
                aval   : aval,                  $
                xmax   : xmax,                  $
                iextwf : iextwf,                $
                ijucys : ijucys,                $
                isrch  : isrch,                 $
                nshell : nshell,                $
                ns     : ns,           		$
                ls     : ls,           		$
                iqs    : iqs,           	$
                alfaa  : alfaa,       	        $
                nx     : nx,                    $
                xin    : xin,                   $
                nt     : nt,                    $
                tin    : tin           		$
               }
   
  widget_control, first_child, set_uvalue=state, /no_copy

  return, ps

end

;-----------------------------------------------------------------------------

function proc804_event, event

                ;**** base id of compound widget ****

  parent=event.handler

		;**********************************************
                ;**** retrieve the user value state        ****
		;**** get id of first_child widget because ****
		;**** user value "state" is stored there   ****
		;**********************************************

  first_child = widget_info(parent, /child)
  widget_control, first_child, get_uvalue=state,/no_copy

		;*********************************
		;**** clear previous messages ****
		;*********************************

  widget_control,state.messid,set_value=' '

                ;************************
                ;**** process events ****
                ;************************

  case event.id of
   
    state.z0text: widget_control, state.ztext,/input_focus
   
    state.ztext: widget_control, state.zefftext,/input_focus
   
    state.zefftext: widget_control, state.isptext,/input_focus
   
    state.isptext: widget_control, state.lptext,/input_focus
   
    state.lptext: widget_control, state.xjptext,/input_focus
    

		;*************************************
		;**** LS or IC switch based on Jp ****
		;*************************************
    
    state.xjptext: begin
           widget_control, state.xjptext, get_value=xjpval 
           if (num_chk(xjpval(0)) eq 0) then begin
              xjp = float(xjpval(0))
           endif else begin
              xjp = -99.0
           endelse
    	  if (xjp lt 0) then begin
    		widget_control,state.lsbase,map=1
    		widget_control,state.icbase,map=0
                widget_control, state.n1lstext,/input_focus
    	  endif else begin
    		widget_control,state.lsbase,map=0
    		widget_control,state.icbase,map=2    		
                widget_control, state.n1ictext,/input_focus
	  endelse
   
    end
   
    state.n1lstext: widget_control, state.l1lstext,/input_focus
    
    state.l1lstext: widget_control, state.ist1text,/input_focus
    
    state.ist1text: widget_control, state.lt1text,/input_focus
    
    state.lt1text: widget_control, state.xjt1lstext,/input_focus
    
    state.xjt1lstext: widget_control, state.neqv1lstext,/input_focus
   
    state.neqv1lstext: widget_control, state.eb1lstext,/input_focus
   
    state.eb1lstext: widget_control, state.fpc1lstext,/input_focus
   
    state.fpc1lstext: widget_control, state.n2lstext,/input_focus
   
    state.n2lstext: widget_control, state.l2lstext,/input_focus
    
    state.l2lstext: widget_control, state.ist2text,/input_focus
    
    state.ist2text: widget_control, state.lt2text,/input_focus
    
    state.lt2text: widget_control, state.xjt2lstext,/input_focus
    
    state.xjt2lstext: widget_control, state.neqv2lstext,/input_focus
   
    state.neqv2lstext: widget_control, state.eb2lstext,/input_focus
   
    state.eb2lstext: widget_control, state.fpc2lstext,/input_focus
    
   
    state.n1ictext: widget_control, state.l1ictext,/input_focus
    
    state.l1ictext: widget_control, state.xj1text,/input_focus
    
    state.xj1text: widget_control, state.xjt1ictext,/input_focus
    
    state.xjt1ictext: widget_control, state.neqv1ictext,/input_focus
   
    state.neqv1ictext: widget_control, state.eb1ictext,/input_focus
   
    state.eb1ictext: widget_control, state.fpc1ictext,/input_focus
   
    state.fpc1ictext: widget_control, state.n2ictext,/input_focus
   
    state.n2ictext: widget_control, state.l2ictext,/input_focus
    
    state.l2ictext: widget_control, state.xj2text,/input_focus
    
    state.xj2text: widget_control, state.xjt2ictext,/input_focus
    
    state.xjt2ictext: widget_control, state.neqv2ictext,/input_focus
   
    state.neqv2ictext: widget_control, state.eb2ictext,/input_focus
   
    state.eb2ictext: widget_control, state.fpc2ictext,/input_focus
    
    
		;*************************************
		;**** default energy button       ****
		;*************************************

    state.defxid: begin

		;**** popup window to confirm overwriting current values ****

	action= popup(message='confirm overwrite values with defaults', $
			buttons=['confirm','cancel'],font=state.font)

	if action eq 'confirm' then begin

		;**** get current table widget value ****

 	   widget_control,state.tempid,get_value=tempval

           units = tempval.units(0)
  	   nx = state.nx
 	   if nx ge 0 then begin

                ;**** default energy values          ****
                ;**** can add up to nx-1 as required ****

           xvalues = [1.00100,1.00200,1.00300,1.00500,1.00700,	$
	              1.01000,1.02000,1.03000,1.05000,1.07000,	$
		      1.10000,1.20000,1.30000,1.50000,1.70000,	$
		      2.00000,2.50000,3.00000,5.00000,7.00000,	$
		      1.0e+01,1.5e+01,2.0e+01,5.0e+01,7.0e+01,	$
		      1.0e+02,1.5e+02,2.0e+02,5.0e+02,7.0e+02,	$
		      1.0e+03]
            ndef = n_elements(xvalues)

		;**** Note: alter length of nx vector if necessary ****	    
	    if nx lt ndef then begin
	        nx=ndef
		state.nx=nx
	    end
	    
            defx = strarr(ndef)
            for i = 0,ndef-1 do begin
               defx(i) = strtrim(string(xvalues(i),format=state.num_form))
            end
            for i = 0,nx-1 do begin
               if i le ndef-1 then begin
   	         tempval.value(units,i) =  $
			   string(defx(i) , format=state.num_form)
               end else begin
                 tempval.value(units,i) = ''
               end
            endfor
 	  end

		;**** fill out the rest with blanks ****

 	  if nx lt state.ixdim then begin
            tempval.value(units,nx:state.ixdim-1) = ''
 	  end


		;**** copy new data to table widget ****

 	  widget_control,state.tempid, set_value=tempval
	end

      end

		;*************************************
		;**** default temperature button ****
		;*************************************

    state.deftid: begin

		;**** popup window to confirm overwriting current values ****

	action= popup(message='confirm overwrite values with defaults', $
			buttons=['confirm','cancel'],font=state.font)

	if action eq 'confirm' then begin

		;**** get current table widget value ****

 	   widget_control,state.tempid,get_value=tempval

           units = tempval.units(1)+5   
  	   nt = state.nt
 	   if nt ge 0 then begin

                ;**** default temperature values ****
                ;**** can add up to nt-1 as required ****

           tvalues = [5.00e+02,1.00e+03,2.00e+03,3.00e+03,	$
             	      5.00e+03,1.00e+04,1.50e+04,2.00e+04,	$
             	      3.00e+04,5.00e+04,1.00e+05,2.00e+05,	$
             	      3.00e+05,5.00e+05]
            ndef = n_elements(tvalues)

		;**** Note: alter length of nt vector if necessary ****	    
	    if nt lt ndef then begin
	        nt=ndef
		state.nt=nt
	    end
	    
            deftemp = strarr(ndef)
	    zeff=state.ps.zeff
;----------------------------------	    
	    
               ;***********************************************
               ;**** Get value of Zeff                      ***
               ;***********************************************

    widget_control, state.zefftext, get_value=zeffval 
    if (num_chk(zeffval(0)) eq 0) then begin
        zeff = float(zeffval(0))
    endif else begin
	action= popup(message='zeff must be set ', $
			buttons=['cancel'],font=state.font)
        zeff = -99.0
    endelse
;----------------------------------	    
	    
            for i = 0,ndef-1 do begin
               deftemp(i) = strtrim(string(zeff*zeff*tvalues(i),	$
	                    format=state.num_form))
            end
            for i = 0,nt-1 do begin
               if i le ndef-1 then begin
   	         tempval.value(units,i) =  $
			   string(deftemp(i) , format=state.num_form)
               end else begin
                 tempval.value(units,i) = ''
               end
            endfor
 	  end

		;**** fill out the rest with blanks ****

 	  if nt lt state.itdim then begin
            tempval.value(units,nt:state.itdim-1) = ''
 	  end

		;**** copy new data to table widget ****

 	  widget_control,state.tempid, set_value=tempval
	end

      end
      
		;*************************************
		;**** Born or Impact Param switch ****
		;*************************************
    
    state.fbut: begin
    	widget_control, state.fbut, get_value=temp
    	  if (temp eq 0) then begin
    		widget_control,state.right1,map=1
    		widget_control,state.right2,map=0
    	endif else begin
    		widget_control,state.right1,map=0
    		widget_control,state.right2,map=2    		
	endelse
    end
    
		;***********************
		;**** cancel button ****
		;***********************

    state.cancelid: new_event = {id:parent, top:event.top, 		$
				handler:0l, action:'cancel'}

		;*********************
		;**** done button ****
		;*********************

    state.doneid: begin

		;***************************************
		;**** check all user input is legal ****
		;***************************************

	error = 0

        	;********************************************
		;*** have to restore "state" before calls ***
		;*** to proc804_get_val so it can be used ***
		;*** there.				  ***
		;********************************************
		
	widget_control, first_child, set_uvalue=state, /no_copy

	widget_control,event.handler,get_value=ps

		;*** reset state variable ***

	widget_control, first_child,get_uvalue=state, /no_copy

		;**** check energy/temp values entered ****

	if error eq 0 and ps.nt eq 0 then begin
	  error = 1
	  message='error: no temperatures entered.'
        end
	if error eq 0 and ps.nx eq 0 then begin
          error = 1
          message='error: no energies entered.'
        end


		;**** return value or flag error ****

	if error eq 0 then begin
	  new_event = {id:parent, top:event.top, handler:0l, action:'done'}
        end else begin
	  widget_control,state.messid,set_value=message
	  new_event = 0l
        end

      end

		;*********************
                ;**** menu button ****
		;*********************

        state.outid: begin
            new_event = {id:parent, top:event.top, handler:0l,          $
                         action:'menu'}
        end

    else: new_event = 0l

  endcase

		;*********************************************
		;*** make "state" available to other files ***
		;*********************************************

  widget_control, first_child, set_uvalue=state,/no_copy

  return, new_event
end

;-----------------------------------------------------------------------------

function cw_adas804_proc, topparent, dsfull, lchoice, 			$
  		          ixdim,  itdim  , isdim , xa , tea,		$ 			$
			  bitfile, 					$
			  procval=procval, 				$
                          uvalue=uvalue, font_large=font_large, 	$
                          font_small=font_small, edit_fonts=edit_fonts, $
                          num_form=num_form

		;**** set defaults for keywords ****

  if not (keyword_set(uvalue)) then uvalue = 0
  if not (keyword_set(font_large)) then font_large = ''
  if not (keyword_set(font_small)) then font_small = ''
  if not (keyword_set(edit_fonts)) then 				$
				edit_fonts = {font_norm:'',font_input:''}
  if not (keyword_set(num_form)) then num_form = '(e10.4)'

  if not (keyword_set(procval)) then begin     
     ps = { 			 		$
                new    : new,                   $
		title : '',             	$
		ifout  : ifout,         	$
                z      : z,                     $
                z0     : z0,                    $
                zeff   : zeff,                  $
                n1     : n1,                    $
                l1     : l1,                    $
                eb1    : eb1,                   $
                n2     : n2,                    $
                l2     : l2,                    $
                eb2    : eb2,                   $
                isp    : isp,                   $
                lp     : lp,                    $
                xjp    : xjp,                   $
                ist1   : ist1,                  $
                lt1    : lt1,                   $
                xj1    : xj1,                   $
                xjt1   : xjt1,                  $
                ist2   : ist2,                  $
                lt2    : lt2,                   $
                xj2    : xj2,                   $
                xjt2   : xjt2,                  $
                neqv1  : neqv1,                 $
                fpc1   : fpc1,                  $
                neqv2  : neqv2,                 $
                fpc2   : fpc2,                  $
                aval   : aval,                  $
                xmax   : xmax,                  $
                iextwf : iextwf,                $
                ijucys : ijucys,                $
                isrch  : isrch,                 $
                nshell : nshell,                $
                ns     : ns,           		$
                ls     : ls,           		$
                iqs    : iqs,           	$
                alfaa  : alfaa,       	        $
                nx     : nx,                    $
                xin    : xin,                   $
                nt     : nt,                    $
                tin    : tin            	$
               }
  endif else begin
     ps = { 			 			$
                new    : procval.new,                   $
                title  : procval.title,  		$
		ifout  : procval.ifout,          	$
                z      : procval.z,                     $
                z0     : procval.z0,                    $
                zeff   : procval.zeff,                  $
                n1     : procval.n1,                    $
                l1     : procval.l1,                    $
                eb1    : procval.eb1,                   $
                n2     : procval.n2,                    $
                l2     : procval.l2,                    $
                eb2    : procval.eb2,                   $
                isp    : procval.isp,                   $
                lp     : procval.lp,                    $
                xjp    : procval.xjp,                   $
                ist1   : procval.ist1,                  $
                lt1    : procval.lt1,                   $
                xj1    : procval.xj1,                   $
                xjt1   : procval.xjt1,                  $
                ist2   : procval.ist2,                  $
                lt2    : procval.lt2,                   $
                xj2    : procval.xj2,                   $
                xjt2   : procval.xjt2,                  $
                neqv1  : procval.neqv1,                 $
                fpc1   : procval.fpc1,                  $
                neqv2  : procval.neqv2,                 $
                fpc2   : procval.fpc2,                  $
                aval   : procval.aval,                  $
                xmax   : procval.xmax,                  $
                iextwf : procval.iextwf,                $
                ijucys : procval.ijucys,                $
                isrch  : procval.isrch,                 $
                nshell : procval.nshell,                $
                ns     : procval.ns,           		$
                ls     : procval.ls,           		$
                iqs    : procval.iqs,           	$
                alfaa  : procval.alfaa,       	        $
                nx     : procval.nx,                    $
                xin    : procval.xin,                   $
                nt     : procval.nt,                    $
                tin    : procval.tin         	  	$
               }

  endelse

                ;*********************************************************
                ;**** modify certain parameters and results depending ****
                ;**** on the machine being used                       ****
                ;*********************************************************

  machine = getenv('target_machine')
  if machine eq 'hpux' then begin
    y_size = 4
    large_font = font_small
  endif else begin
    y_size = 6
    large_font = font_large
  endelse

		;****************************************************
		;**** the adas table widget requires data to be *****
		;**** input as strings, so all the numeric data *****
		;**** has to be written into a string array.    *****
		;**** cols.1,2,3,4,5 - energy units             *****
		;**** cols.6,7,8,9,10 - temperature units       *****
		;****************************************************

  nt = ps.nt  
  nx = ps.nx
  neg_form='(e11.4)'
  tabledata = strarr(10,ixdim) 

  if (nx gt 0) then begin
    

    tabledata(0,0:nx-1) = 						$
	        strtrim(string(xa(0:nx-1,0),format=num_form),2)
    tabledata(1,0:nx-1) = 						$
		strtrim(string(xa(0:nx-1,1),format=num_form),2)
    tabledata(2,0:nx-1) = 						$
                strtrim(string(xa(0:nx-1,2),format=num_form),2)
    tabledata(3,0:nx-1) = 						$
                strtrim(string(xa(0:nx-1,3),format=num_form),2)
    tabledata(4,0:nx-1) = 						$
                strtrim(string(xa(0:nx-1,4),format=num_form),2)
    tabledata(5,0:nt-1) = 						$
	        strtrim(string(tea(0:nt-1,0),format=num_form),2)
    tabledata(6,0:nt-1) = 						$
		strtrim(string(tea(0:nt-1,1),format=num_form),2)
    tabledata(7,0:nt-1) = 						$
                strtrim(string(tea(0:nt-1,2),format=num_form),2)
    tabledata(8,0:nt-1) = 						$
                strtrim(string(tea(0:nt-1,3),format=num_form),2)
    tabledata(9,0:nt-1) = 						$
                strtrim(string(tea(0:nt-1,3),format=num_form),2)

	;**** fill rest of table with blanks ****

    if nx lt ixdim then tabledata(0:4,nx:ixdim-1) = ' '
    if nt lt itdim then tabledata(5:9,nt:itdim-1) = ' '
  end

        ;**** default blanks everywhere ****

  if lchoice eq 0 or lchoice eq 3 then begin
   if nx lt ixdim then begin
    blanks = where(xa eq 0.0) 
    tabledata(0:4,blanks) = ' ' 
    tabledata(0:4,nx:ixdim-1) = ' '
   end
   if nt lt itdim then begin
    blanks = where(tea eq 0.0) 
    tabledata(5:9,blanks) = ' ' 
    tabledata(5:9,nt:itdim-1) = ' '
   end
  
  end

		;********************************************************
		;**** create the 804 processing options/input window ****
		;********************************************************

		;**** create titled base widget ****

  parent = widget_base(topparent, uvalue = uvalue, 			$
			title = 'ADAS804 Processing Options', 		$
			event_func = "proc804_event", 			$
			func_get_value = "proc804_get_val", 		$
			/column)

		;******************************************************
		;**** create a dummy widget just to hold value of *****
		;**** "state" variable so as not to get confused  *****
		;**** with any other values. adopt idl practice   *****
		;**** of using first child widget                 *****
		;******************************************************

  first_child = widget_base(parent)

  topbase = widget_base(first_child,/column)

		;*******************************************************
		;**** add run title, dataset name and browse button ****
		;*******************************************************
		
  base = widget_base(topbase, /row)
  
    
  rc = widget_label(base, value='Title for Run', font=font_small)
  runid = widget_text(base, value=ps.title, xsize=38,			$
  			 font=font_small, /edit)


  rc = cw_adas_dsbr(topbase, dsfull, font=font_small,			$
                    button='Browse index',/row)


 		;************************************************
 		;**** base for the 'Information' widgets     ****
	 	;************************************************

  infobase = widget_base(topbase, /row, /frame)
    
                ;************************************************
                ;**** base for z0, z, zeff    widgets     ****
                ;************************************************

  leftbase = widget_base(infobase, /column, /frame)
    

    z0base = widget_base(leftbase, /column, /frame)
    z0dum = widget_label(z0base, font=font_small, value=" ")
    z0label1 = widget_label(z0base, font=font_small,			$
    value = "Element and ion data:-")
    z0base2 = widget_base(z0base, /row)
    z0label2 = widget_label(z0base2, font=font_small,                   $
    value = " Nuclear charge Z0: ")
    if (procval.z0 ne 0.0) then begin
        z0val = string (procval.z0, format='(f5.1)')
    endif else begin
        z0val = "  "
    endelse
    z0text = widget_text(z0base2, xsize=5,				$
    value = z0val, /editable)

    zbase2 = widget_base(z0base, /row)
    zlabel2 = widget_label(zbase2, font=font_small,                     $
    value = " Ion charge      Z: ")
    if (procval.z ne 0.0) then begin
        zval = string (procval.z, format='(f5.1)')
    endif else begin
        zval = "  "
    endelse
    ztext = widget_text(zbase2, xsize=5,				$
    value = zval, /editable)

    zeffbase2 = widget_base(z0base, /row)
    zefflabel2 = widget_label(zeffbase2, font=font_small,               $
    value = " Eff. charge  Zeff: ")
    if (procval.zeff ne 0.0) then begin
        zeffval = string (procval.zeff, format='(f5.1)')
    endif else begin
        zeffval = "  "
    endelse
    zefftext = widget_text(zeffbase2, xsize=5,				$
    value = zeffval, /editable)

                ;************************************************
                ;**** base for parent    widgets     ****
                ;************************************************

    parentbase = widget_base(leftbase, /column, /frame)
    parentdum = widget_label(parentbase, font=font_small, value=" ")
    parentlabel = widget_label(parentbase, font=font_small,			$
    value=" Parent data:-")

    ispbase = widget_base(parentbase, /row)
    isplabel = widget_label(ispbase, font=font_small,               $
    value = "    2Sp+1: ")
    if (procval.isp ne 0) then begin
        ispval = string (procval.isp, format='(i3)')
    endif else begin
        ispval = "  "
    endelse
    isptext = widget_text(ispbase, xsize=5,				$
    value = ispval, /editable)

    lpbase = widget_base(parentbase, /row)
    lplabel = widget_label(lpbase, font=font_small,               $
    value = "       Lp: ")
    if (procval.lp ge 0) then begin
        lpval = string (procval.lp, format='(i3)')
    endif else begin
        lpval = "  "
    endelse
    lptext = widget_text(lpbase, xsize=5,				$
    value = lpval, /editable)

    xjpbase = widget_base(parentbase, /row)
    xjplabel = widget_label(xjpbase, font=font_small,               $
    value = "       Jp: ")
    if (procval.xjp ge 0) then begin
        xjpval = string (procval.xjp, format='(f5.1)')
    endif else begin
        xjpval = "  "
    endelse
    xjptext = widget_text(xjpbase, xsize=5,				$
    value = xjpval, /editable)


  actbase = widget_base(infobase)
    widget_control, actbase, /sensitive
    lsbase = widget_base(actbase,/row)
    icbase = widget_base(actbase,/row)
;      if os.anopt eq -1 then begin
       if procval.xjp lt 0.0d0 then begin
        widget_control, lsbase, map=1
        widget_control, icbase, map=0
       endif else begin
        widget_control, lsbase, map=0
        widget_control, icbase, map=1
       endelse
;      end

    
                ;************************************************
                ;**** base for n1,l1,L1,S1,J1    widgets     ****
                ;************************************************

;    lowerbase = widget_base(infobase, /column, /frame)
    lowerbase = widget_base(lsbase, /column, /frame)
    lowerdum = widget_label(lowerbase, font=font_small, value=" ")
    lowerlabel = widget_label(lowerbase, font=font_small,	      $
    value=" Lower level data:-")
    
    n1lsbase = widget_base(lowerbase, /row)
    n1lslabel = widget_label(n1lsbase, font=font_small,                   $
    value = "      n1: ")
    if (procval.n1 ne 0.0) then begin
        n1val = string (procval.n1, format='(i3)')
    endif else begin
        n1val = "          "
    endelse
    n1lstext = widget_text(n1lsbase, xsize=5,			      $
    value = n1val, /editable)
    
    l1lsbase = widget_base(lowerbase, /row)
    l1lslabel = widget_label(l1lsbase, font=font_small,                   $
    value = "      l1: ")
    if (procval.l1 ge 0) then begin
        l1val = string (procval.l1, format='(i3)')
    endif else begin
        l1val = "          "
    endelse
    l1lstext = widget_text(l1lsbase, xsize=5,			      $
    value = l1val, /editable)
    
    ist1base = widget_base(lowerbase, /row)
    ist1label = widget_label(ist1base, font=font_small,                   $
    value = " 2*ST1+1: ")
    if (procval.ist1 ne 0.0) then begin
        ist1val = string (procval.ist1, format='(i3)')
    endif else begin
        ist1val = "          "
    endelse
    ist1text = widget_text(ist1base, xsize=5,			      $
    value = ist1val, /editable)
    
    lt1base = widget_base(lowerbase, /row)
    lt1label = widget_label(lt1base, font=font_small,                   $
    value = "     LT1: ")
    if (procval.lt1 ge 0) then begin
        lt1val = string (procval.lt1, format='(i3)')
    endif else begin
        lt1val = "          "
    endelse
    lt1text = widget_text(lt1base, xsize=5,			      $
    value = lt1val, /editable)
    
    xjt1lsbase = widget_base(lowerbase, /row)
    xjt1lslabel = widget_label(xjt1lsbase, font=font_small,                   $
    value = "     JT1: ")
    if (procval.xjt1 ge 0.0) then begin
        xjt1val = string (procval.xjt1, format='(f5.1)')
    endif else begin
        xjt1val = "          "
    endelse
    xjt1lstext = widget_text(xjt1lsbase, xsize=5,			      $
    value = xjt1val, /editable)
    
    neqv1lsbase = widget_base(lowerbase, /row)
    neqv1lslabel = widget_label(neqv1lsbase, font=font_small,                   $
    value = "   Neqv1: ")
    if (procval.neqv1 ge 0) then begin
        neqv1val = string (procval.neqv1, format='(i3)')
    endif else begin
        neqv1val = "          "
    endelse
    neqv1lstext = widget_text(neqv1lsbase, xsize=5,			      $
    value = neqv1val, /editable)

    eb1lsbase = widget_base(lowerbase, /row)
    eb1lslabel = widget_label(eb1lsbase, font=font_small,                 $
    value = " b.e. (Ryd): ")
    if (procval.eb1 ge 0.0) then begin
        eb1val = string (procval.eb1, format='(f12.7)')
    endif else begin
        eb1val = "  "
    endelse
    eb1lstext = widget_text(eb1lsbase, xsize=12,			      $
    value = eb1val, /editable)

    fpc1lsbase = widget_base(lowerbase, /row)
    fpc1lslabel = widget_label(fpc1lsbase, font=font_small,                 $
    value = " Fr. prtge.: ")
    if (procval.fpc1 ge 0.0) then begin
        fpc1val = string (procval.fpc1, format='(f12.7)')
    endif else begin
        fpc1val = "  "
    endelse
    fpc1lstext = widget_text(fpc1lsbase, xsize=12,			      $
    value = fpc1val, /editable)
    
                ;************************************************
                ;**** base for n2,l2,L2,S2, J2    widgets     ****
                ;************************************************

;    upperbase = widget_base(infobase, /column, /frame)
    upperbase = widget_base(lsbase, /column, /frame)
    upperdum = widget_label(upperbase, font=font_small, value=" ")
    upperlabel = widget_label(upperbase, font=font_small,	      $
    value=" Upper level data:-")
    n2lsbase = widget_base(upperbase, /row)
    n2lslabel = widget_label(n2lsbase, font=font_small,                   $
    value = "      n2: ")
    if (procval.n2 ne 0.0) then begin
        n2val = string (procval.n2, format='(i3)')
    endif else begin
        n2val = "  "
    endelse
    n2lstext = widget_text(n2lsbase, xsize=5,			      $
    value = n2val, /editable)
    
    l2lsbase = widget_base(upperbase, /row)
    l2lslabel = widget_label(l2lsbase, font=font_small,                   $
    value = "      l2: ")
    if (procval.l2 ge 0) then begin
        l2val = string (procval.l2, format='(i3)')
    endif else begin
        l2val = "  "
    endelse
    l2lstext = widget_text(l2lsbase, xsize=5,			      $
    value = l2val, /editable)
    
    ist2base = widget_base(upperbase, /row)
    ist2label = widget_label(ist2base, font=font_small,                   $
    value = " 2*ST2+1: ")
    if (procval.ist2 ne 0.0) then begin
        ist2val = string (procval.ist2, format='(i3)')
    endif else begin
        ist2val = "  "
    endelse
    ist2text = widget_text(ist2base, xsize=5,			      $
    value = ist2val, /editable)
    
    lt2base = widget_base(upperbase, /row)
    lt2label = widget_label(lt2base, font=font_small,                   $
    value = "     LT2: ")
    if (procval.lt2 ge 0) then begin
        lt2val = string (procval.lt2, format='(i3)')
    endif else begin
        lt2val = "  "
    endelse
    lt2text = widget_text(lt2base, xsize=5,			      $
    value = lt2val, /editable)
    
    xjt2lsbase = widget_base(upperbase, /row)
    xjt2lslabel = widget_label(xjt2lsbase, font=font_small,                   $
    value = "     JT2: ")
    if (procval.xjt2 ge 0.0) then begin
        xjt2val = string (procval.xjt2, format='(f5.1)')
    endif else begin
        xjt2val = "  "
    endelse
    xjt2lstext = widget_text(xjt2lsbase, xsize=5,			      $
    value = xjt2val, /editable)
    
    neqv2lsbase = widget_base(upperbase, /row)
    neqv2lslabel = widget_label(neqv2lsbase, font=font_small,                   $
    value = "   Neqv2: ")
    if (procval.neqv2 ge 0) then begin
        neqv2val = string (procval.neqv2, format='(i3)')
    endif else begin
        neqv2val = "  "
    endelse
    neqv2lstext = widget_text(neqv2lsbase, xsize=5,			      $
    value = neqv2val, /editable)

    eb2lsbase = widget_base(upperbase, /row)
    eb2lslabel = widget_label(eb2lsbase, font=font_small,                 $
    value = " b.e. (Ryd): ")
    if (procval.eb2 ge 0.0) then begin
        eb2val = string (procval.eb2, format='(f12.7)')
    endif else begin
        eb2val = "  "
    endelse
    eb2lstext = widget_text(eb2lsbase, xsize=12,			      $
    value = eb2val, /editable)

    fpc2lsbase = widget_base(upperbase, /row)
    fpc2lslabel = widget_label(fpc2lsbase, font=font_small,                 $
    value = " Fr. prtge.: ")
    if (procval.fpc2 ge 0.0) then begin
        fpc2val = string (procval.fpc2, format='(f12.7)')
    endif else begin
        fpc2val = "  "
    endelse
    fpc2lstext = widget_text(fpc2lsbase, xsize=12,			      $
    value = fpc2val, /editable)
    
    
                ;************************************************
                ;**** base for n1,l1,j1, J1      widgets     ****
                ;************************************************

;    lowerbase = widget_base(infobase, /column, /frame)
    lowerbase = widget_base(icbase, /column, /frame)
    lowerdum = widget_label(lowerbase, font=font_small, value=" ")
    lowerlabel = widget_label(lowerbase, font=font_small,	      $
    value=" Lower level data:-")
    
    n1icbase = widget_base(lowerbase, /row)
    n1iclabel = widget_label(n1icbase, font=font_small,                   $
    value = "      n1: ")
    if (procval.n1 ne 0.0) then begin
        n1val = string (procval.n1, format='(i3)')
    endif else begin
        n1val = "  "
    endelse
    n1ictext = widget_text(n1icbase, xsize=5,			      $
    value = n1val, /editable)
    
    l1icbase = widget_base(lowerbase, /row)
    l1iclabel = widget_label(l1icbase, font=font_small,                   $
    value = "      l1: ")
    if (procval.l1 ge 0) then begin
        l1val = string (procval.l1, format='(i3)')
    endif else begin
        l1val = "  "
    endelse
    l1ictext = widget_text(l1icbase, xsize=5,			      $
    value = l1val, /editable)
    
    xj1base = widget_base(lowerbase, /row)
    xj1label = widget_label(xj1base, font=font_small,                   $
    value = "      j1: ")
    if (procval.xj1 ge 0.0) then begin
        xj1val = string (procval.xj1, format='(f5.1)')
    endif else begin
        xj1val = "  "
    endelse
    xj1text = widget_text(xj1base, xsize=5,			      $
    value = xj1val, /editable)
    
    xjt1icbase = widget_base(lowerbase, /row)
    xjt1iclabel = widget_label(xjt1icbase, font=font_small,                   $
    value = "     JT1: ")
    if (procval.xjt1 ge 0.0) then begin
        xjt1val = string (procval.xjt1, format='(f5.1)')
    endif else begin
        xjt1val = "  "
    endelse
    xjt1ictext = widget_text(xjt1icbase, xsize=5,			      $
    value = xjt1val, /editable)
    
    neqv1icbase = widget_base(lowerbase, /row)
    neqv1iclabel = widget_label(neqv1icbase, font=font_small,                   $
    value = "   Neqv1: ")
    if (procval.neqv1 ge 0) then begin
        neqv1val = string (procval.neqv1, format='(i3)')
    endif else begin
        neqv1val = "  "
    endelse
    neqv1ictext = widget_text(neqv1icbase, xsize=5,			      $
    value = neqv1val, /editable)

    eb1icbase = widget_base(lowerbase, /row)
    eb1iclabel = widget_label(eb1icbase, font=font_small,                 $
    value = " b.e. (Ryd): ")
    if (procval.eb1 ge 0.0) then begin
        eb1val = string (procval.eb1, format='(f12.7)')
    endif else begin
        eb1val = "  "
    endelse
    eb1ictext = widget_text(eb1icbase, xsize=12,			      $
    value = eb1val, /editable)

    fpc1icbase = widget_base(lowerbase, /row)
    fpc1iclabel = widget_label(fpc1icbase, font=font_small,                 $
    value = " Fr. prtge.: ")
    if (procval.fpc1 ge 0.0) then begin
        fpc1val = string (procval.fpc1, format='(f12.7)')
    endif else begin
        fpc1val = "  "
    endelse
    fpc1ictext = widget_text(fpc1icbase, xsize=12,			      $
    value = fpc1val, /editable)
    
                ;************************************************
                ;**** base for n2,l2,j2,J2    widgets        ****
                ;************************************************

;    upperbase = widget_base(infobase, /column, /frame)
    upperbase = widget_base(icbase, /column, /frame)
    upperdum = widget_label(upperbase, font=font_small, value=" ")
    upperlabel = widget_label(upperbase, font=font_small,	      $
    value=" Upper level data:-")
    n2icbase = widget_base(upperbase, /row)
    n2iclabel = widget_label(n2icbase, font=font_small,                   $
    value = "      n2: ")
    if (procval.n2 ne 0.0) then begin
        n2val = string (procval.n2, format='(i3)')
    endif else begin
        n2val = "  "
    endelse
    n2ictext = widget_text(n2icbase, xsize=5,			      $
    value = n2val, /editable)
    
    l2icbase = widget_base(upperbase, /row)
    l2iclabel = widget_label(l2icbase, font=font_small,                   $
    value = "      l2: ")
    if (procval.l2 ge 0) then begin
        l2val = string (procval.l2, format='(i3)')
    endif else begin
        l2val = "  "
    endelse
    l2ictext = widget_text(l2icbase, xsize=5,			      $
    value = l2val, /editable)
    
    xj2base = widget_base(upperbase, /row)
    xj2label = widget_label(xj2base, font=font_small,                   $
    value = "      j2: ")
    if (procval.xj2 ge 0.0) then begin
        xj2val = string (procval.xj2, format='(f5.1)')
    endif else begin
        xj2val = "  "
    endelse
    xj2text = widget_text(xj2base, xsize=5,			      $
    value = xj2val, /editable)
    
    xjt2icbase = widget_base(upperbase, /row)
    xjt2iclabel = widget_label(xjt2icbase, font=font_small,                   $
    value = "     JT2: ")
    if (procval.xjt2 ge 0.0) then begin
        xjt2val = string (procval.xjt2, format='(f5.1)')
    endif else begin
        xjt2val = "  "
    endelse
    xjt2ictext = widget_text(xjt2icbase, xsize=5,			      $
    value = xjt2val, /editable)
    
    neqv2icbase = widget_base(upperbase, /row)
    neqv2iclabel = widget_label(neqv2icbase, font=font_small,                   $
    value = "   Neqv2: ")
    if (procval.neqv2 ge 0) then begin
        neqv2val = string (procval.neqv2, format='(i3)')
    endif else begin
        neqv2val = "  "
    endelse
    neqv2ictext = widget_text(neqv2icbase, xsize=5,			      $
    value = neqv2val, /editable)

    eb2icbase = widget_base(upperbase, /row)
    eb2iclabel = widget_label(eb2icbase, font=font_small,                 $
    value = " b.e. (Ryd): ")
    if (procval.eb2 ge 0.0) then begin
        eb2val = string (procval.eb2, format='(f12.7)')
    endif else begin
        eb2val = "  "
    endelse
    eb2ictext = widget_text(eb2icbase, xsize=12,			      $
    value = eb2val, /editable)

    fpc2icbase = widget_base(upperbase, /row)
    fpc2iclabel = widget_label(fpc2icbase, font=font_small,                 $
    value = " Fr. prtge.: ")
    if (procval.fpc2 ge 0.0) then begin
        fpc2val = string (procval.fpc2, format='(f12.7)')
    endif else begin
        fpc2val = "  "
    endelse
    fpc2ictext = widget_text(fpc2icbase, xsize=12,			      $
    value = fpc2val, /editable)
    

		;**********************
		;**** another base ****
		;**********************

  tablebase = widget_base(infobase,/column)

		;***************************************************
		;**** base for the energy and temperature table ****
		;***************************************************

  base = widget_base(tablebase,/column,/frame)

  colhead = ['output energy','output temp.']


		;********************************************
		;**** convert fortran index to idl index ****
		;********************************************

  units = intarr(2)
  units(0) = ps.ifout(0)
  units(1) = ps.ifout(1)
  
  unitname = [['ej*k^2     ','ei*k^2     ','ej*(k/z0)^2','X-param      ',$
               'ej*(k/zeff)^2'],['K          ',				$
               'eV         ','scaled     ','reduced     ','dummy       .']]

		;**********************************************************
		;**** two columns in the table and 10 sets of units.   ****
		;**** column 1 switches between sets 0,1,2,3 & 4       ****
		;**** in the input array tabledata as the units change.****
		;**** similarly for column 2                           ****
		;**********************************************************

  unitind = [[0,1,2,3,4],[5,6,7,8,9]]
  unitstitle = ['Energy units','Temp. units']


		;****************************
		;**** table of data   *******
		;****************************

  tempid = cw_adas_table(base, tabledata, units, unitname, unitind, 	$
			unitstitle = unitstitle, 			$
			title = 'Energy and Temperature Table',         $
                        colhead=colhead, 				$
			order = [1,1], /difflen, 			$ 
			limits = [2,2], cellsize = 12, 		        $
			/scroll, ytexsize=y_size, num_form = num_form, 	$
			fonts = edit_fonts,font_large=font_large, 	$
			font_small = font_small)

		;*************************
		;**** default buttons ****
		;*************************

  xbase = widget_base(base, /row)
  defxid = widget_button(xbase,value=' Default E values  ',	$
		font=font_small)

  tbase = widget_base(base, /row)
  deftid = widget_button(tbase,value=' Default Te values  ',	$
		font=font_small)


 		;************************************************
 		;** base for the 'calculation method' widgets  **
	 	;************************************************

  methodbase = widget_base(topbase, /row)
    
        ;******************************************
        ; **** method buttons                 ****
        ;******************************************


  fbutlab = ['Effective Potential Born','Impact Parameter']
  switchb = widget_base(topbase, /frame)

  fbut = cw_bgroup(methodbase,fbutlab,/exclusive,set_value=0,/row,	$
  		   font=font_small)

  right1  = widget_base(switchb,/row)
  right2  = widget_base(switchb,/row)
    
    right1_1 = widget_base(right1,/frame,/column)
    

    fjucyslab = ['Slater  ','Jucys   ']
        ijucysval = string (procval.ijucys, format='(i3)')
    fjucys = cw_bgroup(right1_1,fjucyslab,/exclusive,set_value=0,/row,	$
                       label_left='Potential :', font=font_small)

    fextwflab = ['Internal','External']
        iextwfval = string (procval.iextwf, format='(i3)')
    fextwf = cw_bgroup(right1_1,fextwflab,/exclusive,			$
    			label_left='Wave fns. :',                       $
                        set_value=iextwfval,/row,font=font_small)

    fsrchlab =  ['Energy  ','Alpha   ']
        isrchval = string (procval.isrch, format='(i3)')
    fsrch = cw_bgroup(right1_1,fsrchlab,/exclusive,			$
    			label_left='Search on :',                       $
    			set_value=isrchval,/row,font=font_small)
    
    xmaxbase = widget_base(right1_1, /row)
    tmplabel = widget_label(xmaxbase, font=font_small,value = " ")
    xmaxlabel = widget_label(xmaxbase, font=font_small,value = "xmax: ")
    if (procval.xmax ne 0.0) then begin
        xmaxval = string (procval.xmax, format='(f12.5)')
    endif else begin
        xmaxval = "  "
    endelse
    xmaxtext = widget_text(xmaxbase, xsize=12,			      $
    value = xmaxval, /editable)
    
    avalbase = widget_base(right2, /row)
;    avalbase = widget_base(right1_1, /row)
    avallabel = widget_label(avalbase, font=font_small,                   $
    value = "aval: ")
    if (procval.aval ne 0.0) then begin
        avalval = string (procval.aval, format='(f12.5)')
    endif else begin
        avalval = "  "
    endelse
    avaltext = widget_text(avalbase, xsize=12,			      $
    value = avalval, /editable)


                ;************************************************
                ;**** Base for the screening shell table     ****
                ;************************************************

    screenbase = widget_base(right1, /column, map=1)
    tabscreen = strarr(5,isdim)
    if (procval.nshell ne 0) then begin
    tabscreen(0,0:procval.nshell-1)="   "+strtrim(string(procval.ns(0:procval.nshell-1),$
        format='(i3)'),2)
    tabscreen(1,0:procval.nshell-1)="   "+strtrim(string(procval.ls(0:procval.nshell-1),$
        format='(i3)'),2)
    tabscreen(2,0:procval.nshell-1)="   "+strtrim(string(procval.iqs(0:procval.nshell-1),$
        format='(i3)'),2)
    tabscreen(3,0:procval.nshell-1)=strtrim(string(procval.alfaa(0,0:procval.nshell-1),$
        format=num_form),2)
    tabscreen(4,0:procval.nshell-1)=strtrim(string(procval.alfaa(1,0:procval.nshell-1),$
        format=num_form),2)
    endif

    screentabid = cw_adas_table( screenbase, tabscreen, coledit=[1,1,1,1,1],	$
              colhead=['   N   ','   L   ','  IQ    ','   alf1  ','   alf2  '], $
              /scroll, title='Screening Configuration',limits=[2,1,2,2,2],	$
	      fltint=['(i4)','(i4)','(i4)','(e12.4)','(e12.4)'],		$
	      /rowskip, ytexsize=y_size, cellsize=12,fonts = edit_fonts,	$
              font_large=font_large,font_small=font_small)


		;**** error message ****
		
  if (fbut eq 0) then begin
  	widget_control, right1, map=1
  	widget_control, right2, map=0
  endif

		;**** error message ****

  messid = widget_label(parent,font=font_small, 			$
  value='Edit the processing options data and press done to proceed')

		;**** add the exit buttons ****

  base = widget_base(parent,/row)
  menufile = bitfile + '/menu.bmp'
  read_x11_bitmap, menufile, bitmap1
  outid = widget_button(base, value=bitmap1)          ;menu button
  cancelid = widget_button(base,value='Cancel',font=large_font)
  doneid = widget_button(base,value='Done',font=large_font)
  
		;**** create a state structure for the pop-up ****
		;**** 		     window.                  ****


   new_state = { runid:runid,  		$
		 messid:messid,         $
		 z0text:z0text,         $
		 ztext:ztext,           $
		 zefftext:zefftext,     $
		 isptext:isptext,       $
		 lptext:lptext,         $
		 xjptext:xjptext,       $
		 lsbase:lsbase, 	$
		 icbase:icbase, 	$
		 n1lstext:n1lstext,     $
		 l1lstext:l1lstext,     $
		 n1ictext:n1ictext,     $
		 l1ictext:l1ictext,     $
		 ist1text:ist1text,     $
		 lt1text:lt1text,       $
		 xj1text:xj1text,       $
		 xjt1lstext:xjt1lstext,     $
		 xjt1ictext:xjt1ictext,     $
		 neqv1lstext:neqv1lstext,   $
		 fpc1lstext:fpc1lstext,     $
		 eb1lstext:eb1lstext,       $
		 neqv1ictext:neqv1ictext,   $
		 fpc1ictext:fpc1ictext,     $
		 eb1ictext:eb1ictext,       $
		 n2lstext:n2lstext,         $
		 l2lstext:l2lstext,         $
		 n2ictext:n2ictext,         $
		 l2ictext:l2ictext,         $
		 ist2text:ist2text,     $
		 lt2text:lt2text,       $
		 xj2text:xj2text,       $
		 xjt2lstext:xjt2lstext,     $
		 xjt2ictext:xjt2ictext,     $
		 neqv2lstext:neqv2lstext,   $
		 fpc2lstext:fpc2lstext,     $
		 eb2lstext:eb2lstext,       $
		 neqv2ictext:neqv2ictext,   $
		 fpc2ictext:fpc2ictext,     $
		 eb2ictext:eb2ictext,       $
		 defxid:defxid,		$
		 deftid:deftid,		$
		 tempid:tempid, 	$
		 right1:right1, 	$
		 right2:right2, 	$
		 fbut:fbut, 		$
		 fjucys:fjucys, 	$
		 fextwf:fextwf, 	$
		 fsrch:fsrch, 		$
		 xmaxtext:xmaxtext,     $
		 avaltext:avaltext,     $
		 screentabid:screentabid, $
		 cancelid:cancelid,	$
		 doneid:doneid, 	$
		 outid:outid,		$
		 dsfull:dsfull,		$
		 itdim:itdim,		$
		 ixdim:ixdim,		$
		 isdim:isdim,		$
		 nx:nx, 		$
                 nt:nt,              	$
		 font:font_large,	$
;                 infoid:infoid,        $
		 num_form:num_form,     $
		 neg_form:neg_form,     $
                 ps:ps                  $
	      }


                 ;**** save initial state structure ****

   widget_control, first_child, set_uvalue=new_state,/no_copy

  return, parent

end

