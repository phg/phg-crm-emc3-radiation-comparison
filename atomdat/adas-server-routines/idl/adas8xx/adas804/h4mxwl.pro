;----------------------------------------------------------------------
;+
; PROJECT    :  ADAS
;
; NAME       :  h4mxwl
;
; PURPOSE    :  Converts electron impact collision strengths 
;               to Maxwell averaged collision strengths.
;
; ARGUMENTS  :  All output arguments will be defined appropriately.
;               Inputs will be converted to correct type, if possible,
;               internally without changing calling type.
;
;                h4mxwl, z0      = z0,      $
;                        iz      = iz,      $
;                        effz    = effz,    $
;                        type    = type,    $
;                        aval    = aval,    $
;                        xju     = xju,     $
;                        xjl     = xjl,     $
;                        enu     = enu,     $
;                        enl     = enl,     $ 
;                        xparam  = xparam,  $
;                        omega   = omega,   $
;                        te      = te,      $
;                        upsilon = upsilon, $
;                        sval    = sval,    $
;                        help    = help
; 
;              
;
;               NAME      I/O    TYPE    DETAILS
; REQUIRED   :  z0         I     double  nuclear charge
;               iz         I     double  ion charge
;               effz       I     double  ion charge + 1
;               type       I     integer type of transition
;                                          = 1  dipole transition   
;                                          = 2  non-dipole transition   
;                                          = 3  spin change transition  
;                                          = 4  other         
;               aval       I     double   transition probability (s-1)
;               xju        I     double   upper level statistical weight     
;               xjl        I     double   lower level statistical weight    
;               enu        I     double   upper level energy (cm-1)
;               enl        I     double   lower level energy (cm-1)
;               xparam     I     double() array of X=e/(Delta E) 
;               omega      I     double() array of collision strength
;               te         I     double() array of electron temperature (K)
; OPTIONAL      upsilon    O     double() array of effective collision strength
;               sval       O     double   line strength
;
;
; KEYWORDS      help : Get these comments as help.         
; 
;
; NOTES      :  Calls the fortran h4mxwl routine.
;               Energies are in cm-1 not Rydbergs!
;
;
; AUTHOR     :  Martin O'Mullane
;
; DATE       :  08/09/04
;
;
; MODIFIED:
;       1.1     Martin O'Mullane
;              	- First version.
;
;
; VERSION:
;       1.1    08/09/2004
;
;-
;----------------------------------------------------------------------

PRO h4mxwl, z0      = z0,      $
            iz      = iz,      $
            effz    = effz,    $
            type    = type,    $
            aval    = aval,    $
            xju     = xju,     $
            xjl     = xjl,     $
            enu     = enu,     $
            enl     = enl,     $ 
            xparam  = xparam,  $
            omega   = omega,   $
            te      = te,      $
            upsilon = upsilon, $
            sval    = sval,    $
            help    = help

; If we ask for help

if keyword_set(help) then begin 
   doc_library, 'h4mxwl' 
   return
endif



fortdir = getenv('ADASFORT')

; Check inputs

if n_elements(z0)     EQ 0 OR   $           
   n_elements(iz)     EQ 0 OR   $           
   n_elements(effz)   EQ 0 OR   $           
   n_elements(type)   EQ 0 OR   $
   n_elements(aval)   EQ 0 OR   $
   n_elements(xju)    EQ 0 OR   $
   n_elements(xjl)    EQ 0 OR   $
   n_elements(enu)    EQ 0 OR   $
   n_elements(enl)    EQ 0 OR   $
   n_elements(xparam) EQ 0 OR   $
   n_elements(omega)  EQ 0 OR   $
   n_elements(te)     EQ 0      $
   then message, 'essential parameter(s) missing'       

if type LT 1 OR type GT 4 then message, 'invalid type : select 1-4'

if n_elements(xparam) NE n_elements(omega) then message, 'X and Omega must have the same size'



; Convert input vlaues to correct form for external routine.
; Energy units changed here.

ixtyp  = long(type)

z0     = double(z0)
z      = double(iz)
zeff   = double(effz)
acoeff = double(aval)  
wi     = double(xjl)  
wj     = double(xju)   
ei     = double(enl)/109737.315   
ej     = double(enu)/109737.315   
xa     = double(xparam)
omga   = double(omega)
tea    = double(te) 

nx     = long(n_elements(xa))
nt     = long(n_elements(te))

upsa   = dblarr(nt)
s      = 0.0D0

dummy = CALL_EXTERNAL(fortdir+'/h4mxwl_if.so','h4mxwl_if',  $
                      z0, z, zeff, ixtyp, acoeff,           $
                      s, wi, wj, ei, ej,                    $
                      nx, xa, omga, nt, tea, upsa)

if arg_present(sval) then sval = s
if arg_present(upsilon) then upsilon = upsa


END
