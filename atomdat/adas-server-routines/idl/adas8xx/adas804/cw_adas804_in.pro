; SCCS info: Module @(#)cw_adas804_in.pro	1.1 Date 03/17/03
;+
; Project:
;       ADAS
;
; Name:
;       cw_adas804_in()
;
; Purpose:
;       data file selection for a single input dataset.
;
; Explanation:
;       this function creates a compound widget consisting of the compound
;       widget cw_adas804_infile.pro, a 'cancel' button, a 'done' button and
;       a button to allow the browsing of the selected data file comments.
;       the browsing and done buttons are automatically de-sensitised
;       until a valid input dataset has been selected.
;
;       the value of this widget is the settings structure of the
;       cw_adas804_infile widget.  this widget only generates events
;       when either the 'done' or 'cancel' buttons are pressed.
;       the event structure returned is;
;       {id:0l, top:0l, handler:0l, action:''}
;       action has one of two values 'done' or 'cancel'.
;
; Use:
;       see routine adas804_in.pro for an example.
;
; Inputs:
;       parent  - long integer; the id of the parent widget.
;
; Optional inputs:
;       none
;
; Outputs:
;       this function returns the id of the compound widget created.
;
; Optional outputs:
;       none
;
; Keyword parameters:
;       value   - a structure which determines the initial settings of
;                 the dataset selection widget cw_adas804_infile.  the
;                 structure must be;
;                 {rootpath:'', file:'', centroot:'', userroot:'' }
;                 the elements of the structure are as follows;
;
;                 rootpath - current data directory e.g '/usr/fred/adas/'
;                 file     - current data file in rootpath e.g 'input.dat'
;                 centroot - default central data store e.g '/usr/adas/'
;                 userroot - default user data store e.g '/usr/fred/adas/'
;
;                 the data file selected by the user is obtained by
;                 appending rootpath and file.  in the above example
;                 the full name of the data file is;
;                 /usr/fred/adas/input.dat
;
;                 path names may be supplied with or without the trailing
;                 '/'.  the underlying routines add this character where
;                 required so that userroot will always end in '/' on
;                 output.
;
;                 the default value is;
;                 {rootpath:'./', file:'', centroot:'', userroot:''}
;                 i.e rootpath is set to the user's current directory.
;
;       title   - the title to be included in the input file widget, used
;                 to indicate exactly what the required input dataset is,
;                 e.g 'input copase dataset'
;
;       uvalue  - a user value for the widget.  defaults to 0.
;
;       font    - supplies the font to be used for the interface widgets.
;                 defaults to the current system font.
;
; Calls:
;       xxtext          pop-up window to browse dataset comments.
;       cw_adas804_infile       dataset selection widget.
;       file_acc        determine filetype and access permissions.
;
;       see side effects for other related widget management routines.
;
; Side effects:
;
;       in804_get_val()    widget management routine in this file.
;       in804_event()      widget management routine in this file.
;
; Category:
;       compound widget - specific to adas804
;
; Written:
;       Hugh Summers, Univ. of Strathclyde, 08-june-2002
;
; Version:
;       1.1     Hugh P Summers
;                - First release
;       1.2     Martin O'Mullane
;                - Changed done to Done etc. for button labels.
;       1.3     Martin O'Mullane
;               Give the internal routines (event and get_val) unique
;               names (in804_get_val and in804_event) to avoid conflict
;               with other ADAS routines. 
;
; Modified:
;	1.1	08-06-2002
;	1.2	25-05-2004
;       1.3     30-11-2010
;-
;-----------------------------------------------------------------------------

function in804_get_val, id


                ;**** return to caller on error ****
  on_error, 2

                ;**** retrieve the state ****

  first_child = widget_info(id,/child)
  widget_control, first_child, get_uvalue=state, /no_copy

		;**** get settings ****

  widget_control,state.fileid,get_value=inset

  widget_control, first_child, set_uvalue=state, /no_copy

  return, inset

end

;-----------------------------------------------------------------------------

function in804_event, event


                ;**** base id of compound widget ****

  parent=event.handler

                ;**** retrieve the state ****

  first_child = widget_info(parent,/child)
  widget_control, first_child, get_uvalue=state, /no_copy



		;*********************************
		;**** clear previous messages ****
		;*********************************

  widget_control,state.messid,set_value=''

		;**** default output no event ****

  new_event = 0l

                ;************************
                ;**** process events ****
                ;************************

  case event.id of

		;***********************************
		;**** event from file selection ****
		;***********************************

    state.fileid: begin
	  if event.action eq 'newfile' then begin
	      widget_control, state.doneid, /sensitive
	      widget_control, state.browseid, /sensitive
	  endif else begin
	      widget_control, state.doneid, sensitive=0
	      widget_control, state.browseid, sensitive=0
	  endelse
	end

		;***********************
		;**** browse button ****
		;***********************

    state.browseid: begin

		;**** get latest filename ****

	  widget_control, state.fileid, get_value=inset
	  filename = inset.rootpath+inset.file
          filelen = strlen(filename) - 1
	  if (strmid(filename, filelen, 1) eq '/') then begin
	      buttons = ['continue']
	      title = 'ADAS804 error'
	      message = 'There is no index to browse...'
              action = popup(message=message, title=title, buttons=buttons, $
	                     font=font)
	  endif else begin

		;**** invoke comments browsing ****

	      xxtext, filename, font=state.font
          endelse
	end

		;***********************
		;**** cancel button ****
		;***********************

    state.cancelid: new_event = {id:parent, top:event.top, 		$
				 handler:0l, action:'cancel'}

		;*********************
		;**** done button ****
		;*********************

    state.doneid: begin

	  new_event = {id:parent, top:event.top, handler:0l, action:'done'}

        end

    else:

  endcase

  widget_control, first_child, set_uvalue=state, /no_copy  

  return, new_event

end

;-----------------------------------------------------------------------------

function cw_adas804_in, parent, value=value, title=title, 		$
                        uvalue=uvalue, font=font


  if (n_params() eq 0) then message, 'Must specify parent for cw_adas804_in'
  on_error, 2					;return to caller

		;**** set defaults for keywords ****

  if not (keyword_set(value)) then begin
      inset = {   rootpath:'./', file:'', 				$
		  centroot:'', userroot:'',refresh:''}
  endif else begin
      inset = {   rootpath:value.rootpath, file:value.file, 		$
		  centroot:value.centroot, userroot:value.userroot,	$
                  refresh:value.refresh }
      if strtrim(inset.rootpath) eq '' then begin
          inset.rootpath = './'
      endif else if 							$
      strmid(inset.rootpath,strlen(inset.rootpath)-1,1) ne '/' then begin
          inset.rootpath = inset.rootpath+'/'
      endif
      if strmid(inset.file,0,1) eq '/' then begin
          inset.file = strmid(inset.file,1,strlen(inset.file)-1)
      endif
  endelse
  if not (keyword_set(title)) then title = ''
  if not (keyword_set(uvalue)) then uvalue = 0
  if not (keyword_set(font)) then font = ''

		;*********************************
		;**** create the input widget ****
		;*********************************

		;**** create base widget ****

  cwid = widget_base( parent, uvalue = uvalue, 				$
			event_func = "in804_event", 			$
			func_get_value = "in804_get_val", 			$
			/column)

		;**** create base to hold the value of state ****

  first_child = widget_base(cwid)

		;*************************************
		;**** input file selection widget ****
		;*************************************

  fileid = cw_adas804_infile(first_child,value=inset,title=title,font=font)

		;*****************
		;**** buttons ****
		;*****************

  base = widget_base(cwid, /row)

		;**** browse dataset button ****

  browseid = widget_button(base,value='Browse index',font=font)

		;**** cancel button ****

  cancelid = widget_button(base,value='Cancel',font=font)

		;**** done button ****

  doneid = widget_button(base,value='Done',font=font)

		;**** error message ****

  messid = widget_label(parent,font=font,value='*')

		;**** check filename and desensitise buttons if it ****
		;**** is a directory or it is a file without read  ****
		;**** access.					   ****

  if inset.file ne '' then begin			  
      filename = inset.rootpath+inset.file
      file_acc, filename, fileexist, read, write, execute, filetype
      if filetype ne '-' then begin
          widget_control, browseid, sensitive=0
          widget_control, doneid, sensitive=0
      endif else begin
          if read eq 0 then begin
              widget_control, browseid, sensitive=0
              widget_control, doneid, sensitive=0
          endif
      endelse
  endif else begin
      filename = inset.rootpath+''
  endelse

		;**** create a state structure for the widget ****

  new_state = { fileid:fileid, browseid:browseid, 			$
		cancelid:cancelid, doneid:doneid, messid:messid, 	$
		font:font }

                ;**** save initial state structure ****

  widget_control, first_child, set_uvalue=new_state, /no_copy

  return, cwid

end
