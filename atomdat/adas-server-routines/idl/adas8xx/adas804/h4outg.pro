;+
; PROJECT:
;       ADAS IBM MVS to DEC UNIX conversion & development
;
; NAME:
;       H4OUTG
;
; PURPOSE:
;       To read omegas and upsilons after fortran processing.
;
; USE:
;       ADAS804 Specific
;
; INPUTS:
;       PIPE    - The IDL unit number of the bi-directional pipe to the
;                 ADAS804 FORTRAN process.
;
;       LPEND   - Integer, 0 if the user pressed the 'Done' button
;                 or 1 if the user pressed 'Cancel'.  Maps directly onto
;                 the logical variable LPEND in ADAS804 FORTRAN.
;
;       LCHOICE - Integer; Variable to flag user choice of archive
;                 option. 
;                 0 = No Archive
;                 1 = Old Archive
;                 2 = Refresh form Old Archive
;                 3 = New Archive
;
;       OUTVAL -  A Structure which holds the output widget settings see 
;                 h4spf1.pro
;
;       PROCVAL - A Structure which holds the processing widget settings
;                 see h4ispf.pro.
;
;       ROOT -    The path to the user's default archive directory, 
;                 '/home/user/adas/arch804'
;
;       DEVICE -  The name of the current output device.
;
;       DATE -    The date
;
;       HEADER -  The ADAS release header for the final plot.  
;
;       TITLX  -  The datset name, for display on the plot.
;
;       BITFILE - String; the path to the directory containing bitmaps
;                 for the 'escape to series menu' button.
;
; OPTIONAL INPUTS:
;                 None
;
; OUTPUTS:   
;       GOMENU  - Int; flag - set to 1 if user has selected 'escape direct
;                 to series menu , otherwise 0
;
; OPTIONAL OUTPUTS:
;                 None
;
; KEYWORD PARAMETERS:
;       FONT - The name of a font.         
;
; CALLS:
;       ADAS_EDIT_GRAPH - The ADAS version of the interactive Graph Editor
;       ADAS804_PLOT - The plotting procedure for the Upsilon graph
;
; SIDE EFFECTS:
;       Two way communications with the Fortran process via the Unix pipe.
;
; CATEGORY:
;       ADAS System
;
; WRITTEN: 
;       Hugh P Summers, Univ.of Strathclyde,  27-07-02
;
; MODIFIED:
;	1.1	Hugh P Summers
;		First release
;	1.2	Hugh P Summers
;		Initialise nx and nt before reading from fortran.
; VERSION:
;	1.1	27-07-02
;	1.1	28-05-2004
;-
;-----------------------------------------------------------------------------
pro h4outg, pipe, lpend, lchoice, root, outval, procval, device, date, $
            header , titlx, bitfile, gomenu, FONT = font

                   ;**** re-set lpend ****
  lpend = 0
  
  nx=0
  nt=0
  
  readf, pipe, format = '(1x,i3)', nx
  indim1 = nx
  xa   = fltarr(indim1)
  omga = xa 
  
  for i = 0,nx-1 do begin
    readf, pipe, format = '(1x,e12.4,1x,e12.4)', xat, omgat
                 xa(i) = xat
                 omga(i) = omgat
  endfor
   
  readf, pipe, format = '(1x,i3)', nt
  indim2 = nt
  tea  = fltarr(indim2)
  upsa = tea
  
  for i = 0,nt-1 do begin
    readf, pipe, format = '(1x,e12.4,1x,e12.4)', teat, upsat
                 tea(i) = teat
                 upsa(i) = upsat
  endfor
  
  ianopt = -1
  ixval = 0
  s12 = 0.0
  e12 = 0.0
  bcval = 1.5
  
  readf, pipe, format = '(1x,i3)', ixval
  readf, pipe, format = '(1x,e12.4)', s12
  readf, pipe, format = '(1x,e12.4)', e12
  readf, pipe, format = '(1x,i3)', ianopt
  readf, pipe, format = '(1x,e12.4)', bcval
  
                   ;***********************************
                   ;**** convert to plot variables ****
                   ;***********************************
  

  if (ianopt eq 1) then begin
  
      dgex = 1-1/xa
      dgey = omga
      plotx = alog10(tea)
      ploty = upsa
  
  endif else begin
  
      if (ixval eq 1) then begin
      
          dgex = 1.0d0-alog(bcval)/alog(xa-1.0d0+bcval)
          dgey = omga/alog(xa-1.0+2.7182818)
          plotx = 1.0e0-alog(bcval)/alog(tea/(157890.0*e12) +bcval)
          ploty = upsa/alog(tea/(157890.0*e12) +2.7182818)
      
      endif else if (ixval eq 2) then begin
      
          dgex = (xa-1.0d0)/(xa-1.0d0+bcval)
          dgey = omga
          plotx = (tea/(157890.0*e12))/(tea/(157890.0*e12) +bcval)
          ploty = upsa
      
      endif else if (ixval eq 3) then begin
      
          dgex = (xa-1.0d0)/(xa-1.0d0+bcval)
          dgey = xa^2*omga
          plotx = (tea/(157890.0*e12))/(tea/(157890.0*e12) +bcval)
          ploty = (tea/(157890.0*e12)+1)^2*upsa
      
      endif else begin

          dgex = 1-1/xa
          dgey = omga
          plotx = alog10(tea)
          ploty = upsa
      
      endelse 
  
  endelse
  
    hrdout = 0
    arch = 0       ;**** default archive option is off
    
    b = intarr(5)
    for i = 0,4 do b(i) = -1  ;set display of Burgess fixed knots off

    adas804_plot, plotx, ploty, dgex, dgey, b, ianopt, ixval,		$
    		  lpend, hrdout, arch, 					$
                  lchoice, outval, action, device, date, header, titlx, $
                  bitfile, gomenu, wintitle = 'UPSILON GRAPH', nsp = 70,$
                  font = font  
    printf, pipe, lpend

                   ;**** Print archiving decision if continuing ****

    if lpend eq 0 then printf, pipe, arch
    
    if lpend eq 1 then goto, LABELEND


                   ;**** special case if archiving selected after ****
                   ;**** picking no archiving initially ****

    if arch eq 1 and lchoice eq 0 then begin
      dsarch = root+'archive.dat'
      printf, pipe,format = '(1a80)', dsarch
    end

LABELEND:

end
