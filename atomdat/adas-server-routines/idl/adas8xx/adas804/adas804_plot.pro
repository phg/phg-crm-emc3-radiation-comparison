; SCCS info: Module @(#)$Header: /home/adascvs/idl/adas8xx/adas804/adas804_plot.pro,v 1.1 2004/07/06 11:05:58 whitefor Exp $ Date $Date: 2004/07/06 11:05:58 $
;+
; PROJECT:
;       ADAS IBM MVS to DEC UNIX conversion & development
;
; NAME:
;	ADAS804_PLOT
;
; PURPOSE:
;       To plot the omega and Upsilon graphs  in Standard of Burgess
;       form & to allow printing of either or both graphs.
;
; EXPLANATION:
;       creates a window for the display of the graph and displays
;       output as the user directs through interaction with the
;       widget.
;
; USE:  ADAS804 Specific
;
; INPUTS:
;       XC    - Array; The x-coordinates for the Upsilon graph 
;                      (log10(Te)))
;
;       YC    - Array; The y-coordinates for the Upsilon graph
;                      (Upsilon)
;
;       DGEX  - Array; The x-coordinates for the Reduced Omega
;                      graph.
;
;       DGEY  - Array; The y-coordinates for the Reduced Omega
;                      graph.
;
;       B     - Array - empty
;
;       LPENDC- Integer; Determines whether 'cancel' or 'done' 
;                        selected.
;
;       HRDOUT- Integer; Determines whether graph printout
;                        is requested.
;
;       ARCHC - Integer; Determines whether archiving is requested.
;
;       LCHOICEC-Integer; Holds archiving option
;
;       DEVICE - String; The current graphics device.
;
;       DATE   - String; The date
;
;       HEADER - String; The Printout header.
;
;       TITLX  - String; Part of the graph title (The dataset name)
;
;       BITFILE - String; the path to the directory containing bitmaps
;                 for the 'escape to series menu' button.
;
; OPTIONAL INPUTS:
;
; OUTPUTS:
;       VAL   - Structure; Contains the values of the output widget
;                          selected by the user, see h4spf1.pro.
;
;       ACT   - String; The evnt action; 'Cancel' or 'Done'
;
;       GOMENU  - Int; flag - set to 1 if user has selected 'escape direct
;                 to series menu , otherwise 0
;
; OPTIONAL OUTPUTS:
;
; KEYWORD PARAMETERS:
;       WINTITLE - A title for the window
;       FONT 
;       NSP - The number of requested spline points
;
; CALLS:
;       ADAS804_PLOT_EV
;       XMANAGER
;
; SIDE EFFECTS:
;       None
;
; CATEGORY: ADAS System
;
; WRITTEN: 
;       Hugh P Summers, Univ.of Strathclyde,  27-07-02
;
; MODIFIED:
;	1.1	Hugh P Summers
;		First release
; VERSION:
;	1.1	27-07-02
;-
;-----------------------------------------------------------------------------


PRO adas804_plot_ev, event

  COMMON plot804_blk, action, value, lpend, arch, lchoice, data, iplot, $
                      plotdev, plotfile, fileopen, win, hrdout, gomenu
  newplot = 0
  print = 0
  done = 0

		;**** Find the event type and copy to common ****
    action = event.action

    CASE action OF
            
                ;**** 'Previous' button ****
        'Previous' : begin
	       if iplot eq 1 then begin
                   newplot = 1
		   iplot = 0
	       end
           end
            
                ;**** 'Next' button ****
        'Next' : begin
	       if iplot eq 0 then begin
                   newplot = 1
		   iplot = 1
	       end
           end
            
                ;**** 'Print' button ****
        'Print' : begin
               newplot = 1
               print = 1
	       first = iplot
	       last = iplot
           end
            
                ;**** 'Printall' button ****
        'Printall' : begin
               newplot = 1
               print = 1
	       first = 0
	       last = 1
           end
	   
                ;**** 'Archive' button ****

        'Archive' : begin
                arch = 1
                if lchoice eq 0 then begin
                  buttons = ['Continue']
                  title = 'ADAS804:DEFAULT ACTION CONTINUING'
                  message = $
"Archive selection clashes with 'no archive' flag arch804/archive.dat created"
                  dec = popup(message=message,title=title,$
                              buttons=buttons,font=font)
                end
           end

		;**** 'Done' button ****
	'Done'  : begin

                if fileopen eq 1 then begin
                   set_plot,plotdev
                   device,/close_file
                end
                set_plot,'X'
			;**** Get the output widget value ****
;		widget_control,event.id,get_value=value 
		widget_control,event.top,/destroy
                done = 1
                lpend = 0

	   end


		;**** 'Cancel' button ****
	'Cancel': begin
                  widget_control,event.top,/destroy
                  done = 1
                  lpend = 1
                  set_plot,'X'
           end

		;**** 'Menu' button ****

	'Menu': begin
                if fileopen eq 1 then begin
                   set_plot,plotdev
                   device,/close_file
                end
		widget_control,event.top,/destroy
		done = 1
		lpend = 1
		gomenu = 1
		set_plot,'X'
	   end
    END

  if done eq 0 then begin
  
;                ;**** Set graphics device ****
     if print eq 1 then begin
       set_plot,plotdev
       if fileopen eq 0 then begin
         fileopen = 1
         device,filename = plotfile
         device, /landscape
       end
     end else begin
       set_plot,'X'
       wset,win
     end
     
                ;**** Draw Graphics ****
     if iplot eq 0 then begin
     
         plot804, data.dgex, data.dgey, data.title1, 			$
     		  data.xtitle1, data.ytitle1, 				$ 
              	  data.xmin1, data.xmax1, data.ymin1, data.ymax1,  	$
             	  data.nsp, data.ldef1, data.b
	      
     endif else begin
     
         plot804, data.x, data.y, data.title2, 				$
	 	  data.xtitle2, data.ytitle2, 				$ 
              	  data.xmin2, data.xmax2, data.ymin2, data.ymax2, 	$
              	  data.nsp, data.ldef2, data.b
	      
     endelse

  end

END

;-----------------------------------------------------------------------------


PRO adas804_plot, xc, yc, dgex, dgey, b, ianopt, ixval, 		$
		  lpendc, hrdoutc, archc, lchoicec, 			$
                  val, act, device, date, header, titlx, bitfile, gomenu, $
                  WINTITLE = wintitle, NSP = nspc, FONT = font

  COMMON plot804_blk,action,value, lpend, arch, lchoice, data, iplot,	$
                   plotdev, plotfile, fileopen, win, hrdout, gomenucom

		;**** Copy values to common ****
  value = val
  lpend = lpendc
  arch = archc
  lchoice = lchoicec
  iplot = 0
  plotdev = device
  plotfile = value.hardname
  fileopen = 0
;  hrdout = hrdoutc
  hrdout = value.hrdout
  gomenucom = gomenu

		;**** Set defaults for keywords ****
  IF NOT (KEYWORD_SET(wintitle)) THEN wintitle = ''
  IF NOT (KEYWORD_SET(nsp)) THEN NSP = -1
  IF NOT (KEYWORD_SET(font)) THEN font = 'courier-bold14'

		;***************************************
		;**** Pop-up a new widget if needed ****
		;***************************************
  plotval = {WIN:0}

                ;**** create base widget ****
  inid = widget_base(TITLE=wintitle,XOFFSET=200,YOFFSET=0)
		;**** Declare output options widget ****
  cwid = cw_h4plot(inid, bitfile, hrdout, plotval = plotval, FONT=font)

		;**** Realize the new widget ****
  dynlabel, inid
  widget_control,inid,/realize
                ;**** get the id of the graphics area ****
  widget_control,cwid,get_value = window
  win = window.win
  wset,win
                ;**** Set up titles ****
  title1 = strarr(5)
  title2 = title1
    title1(0) = "COLLISION STRENGTH GRAPH "
      if (strtrim(strcompress(value.gtit1),2) ne ' ' ) then begin
        title1(0) = title1(0)+': '+strupcase(strtrim(value.gtit1,2))
      end
    title1(1) = 'ADAS    :'+strupcase(header)
    title1(2) = 'FILE     : '+strcompress(titlx)
    title1(3) = 'KEY     : (DIAMONDS - INPUT DATA) '+$
               '(FULL LINE - IDL SPLINE FIT)'
    title1(4) = ' '
    title2(0) = "UPSILON GRAPH "
      if (strtrim(strcompress(value.gtit1),2) ne ' ' ) then begin
         title2(0) = title2(0)+': '+strupcase(strtrim(value.gtit1,2))
      end
    title2(1) = 'ADAS    :'+strupcase(header)
    title2(2) = 'FILE     :'+strcompress(titlx)
    title2(3) = 'KEY     : (DIAMONDS - OUTPUT DATA) '+$
               '(FULL LINE - IDL SPLINE FIT)'
    title2(4) = ' '
  if (ianopt eq 1) then begin
  
      xtitle1 = ' 1-1/X (X=Threshold Parameter) '
      ytitle1 = ' Omega '
      xtitle2 = ' Log10 Te(K) '
      ytitle2 = ' Upsilon '
  
  endif else begin
  
      if (ixval eq 1) then begin
      
          xtitle1 = ' 1-lnC/ln(X-1+C (X=Threshold Parameter) '
          ytitle1 = ' Omega/ln(X-1+e) '
          xtitle2 = ' 1-lnC/ln(kTe/E12+C) '
          ytitle2 = ' Upsilon/ln(kTe/E12+e) '
      
      endif else if (ixval eq 2) then begin
      
          xtitle1 = ' (X-1)/(X-1+C) (X=Threshold Parameter) '
          ytitle1 = ' Omega '
          xtitle2 = ' (kTe/E12)/(kTe/E12+C) '
          ytitle2 = ' Upsilon '
      
      endif else if (ixval eq 3) then begin
      
          xtitle1 = ' (X-1)/(X-1+C) (X=Threshold Parameter) '
          ytitle1 = ' X^2*Omega '
          xtitle2 = ' (kTe/E12)/(kTe/E12+C) '
          ytitle2 = ' (kTe/E12)^2*Upsilon '
      
      endif else begin

          xtitle1 = ' 1-1/X (X=Threshold Parameter) '
          ytitle1 = ' Omega '
          xtitle2 = ' Log10 Te(K) '
          ytitle2 = ' Upsilon '
      
      endelse 
  
  endelse

                ;**** Put the graphing data into common ****
  data = { dgex:dgex, dgey:dgey, title1:title1, xtitle1:xtitle1, $
           ytitle1:ytitle1,$
           x:xc , y:yc, title2:title2, xtitle2:xtitle2, ytitle2:ytitle2, $
           xmin1:value.xmin1, xmax1:value.xmax1, ymin1:value.ymin1, $
           ymax1:value.ymax1, ldef1:value.grpscal1, $
           xmin2:value.xmin2, xmax2:value.xmax2, ymin2:value.ymin2, $
           ymax2:value.ymax2, NSP:nspc, ldef2:value.grpscal2, b:b }
  
  plot804, xc, yc, title2, xtitle2, ytitle2, value.xmin2, value.xmax2, $
           value.ymin2, value.ymax2,  nsp, value.grpscal2, b

		;**** make widget modal ****
  xmanager,'adas804_plot',inid,event_handler='adas804_plot_ev',/modal,/just_reg
 
		;**** Return the output value from common ****
  act = action
  val = value
  lpendc = lpend
  archc = arch
  gomenu = gomenucom

END

