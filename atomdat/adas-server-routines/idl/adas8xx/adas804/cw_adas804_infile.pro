; SCCS info: Module @(#)$Header: /home/adascvs/idl/adas8xx/adas804/cw_adas804_infile.pro,v 1.1 2004/07/06 12:55:40 whitefor Exp $ Date $Date: 2004/07/06 12:55:40 $
;+
; project:
;       adas ibm mvs to dec unix conversion
;
; name: cw_adas804_infile()
;
; purpose:
;	archive file selection for an input dataset.
;
; explanation:
;	this function creates a compound widget consisting of the
;	compound widget cw_adas_root, compound widget cw_archive_select,
;	a title and a message.  this widget provides its own error
;	messages.
;
;	this widget generates events as the user interacts with it.
;	the event structure returned is;
;	{id:0l, top:0l, handler:0l, action:''}
;	action has one of two values 'nofile' or 'newfile' indicating
;	the current state of the file selection process.
;
; use:
;	see routine cw_adas804_in.pro for an example.
;
; inputs:
;       parent   - long integer; the id of the parent widget.
;
; optional inputs:
;	none.
;
; outputs:
;       the return value of this function is the id of the compound
;       widget which is of type long integer.
;
; optional outputs:
;       none
;
; keyword parameters:
;	value	- a structure which determines the initial settings of
;		  widgets cw_adas_root and cw_file_select.
;		  the structure must be;
;		  {rootpath:'', file:'', centroot:'', userroot:'' }
;		  the elements of the structure are as follows;
;
;		  rootpath - current data directory e.g '/usr/fred/adas/'
;		  file     - current data file in rootpath e.g 'input.dat'
;		  centroot - default central data store e.g '/usr/adas/'
;		  userroot - default user data store e.g '/usr/fred/adas/'
;
;		  the archive file selected by the user is obtained by
;		  appending rootpath and file.  in the above example
;		  the full name of the data file is;
;		  /usr/fred/adas/input.dat
;
;		  path names may be supplied with or without the trailing
;		  '/'.  the widget cw_adas_root will add this where
;		  required so that userroot will always end in '/' on
;		  output.
;
;		  the default value is;
;		  {rootpath:'./', file:'', centroot:'', userroot:''}
;		  i.e rootpath is set to the user's current directory.
;
;	title	- the title to be included in this widget, used
;                 to indicate exactly what the required input dataset is,
;                 e.g 'input copase dataset'
;
;       font    - a font to use for all text in this widget.
;
;	uvalue	- a user value for this widget.
;
; calls:
;       cw_loadstate    recover compound widget state.
;       cw_savestate    save compound widget state.
;	cw_adas_root	used to select root adas data directory.
;	cw_archive_select used to select an archive file.
;       see side effects for other related widget management routines.
;
; side effects:
;	this widget uses a common block: cw_infile_blk to hold the
;       widget state.
;
;       three other routines are included which are used to manage the
;       widget;
;
;	infile_get_val()
;	infile_event()
;
; category:
;	compound widget
;
; written:
;       hugh summers, univ.of strathclyde, 08-june-2002
;
; version:
;       1.1     hugh p summers
;		first release
;	
; modified:
;	1.1	08-06-02
;-
;-----------------------------------------------------------------------------


function infile804_get_val, id


		;**** return to caller on error ****
  on_error, 2

		;**** retrieve the state ****

  first_child = widget_info(id,/child)
  widget_control, first_child, get_uvalue=state, /no_copy

		;**** get latest root and file name ****

  widget_control, state.fileid, get_value=flesval

		;**** update state ****

  state.inarchval.file = flesval.file
  state.inarchval.rootpath = flesval.root

  inarchval=state.inarchval
  widget_control, first_child, set_uvalue=state, /no_copy

  return, inarchval

end

;-----------------------------------------------------------------------------

function infile804_event, event


		;**** base id of compound widget ****

  base=event.handler

		;**** retrieve the state ****

  first_child = widget_info(base,/child)
  widget_control, first_child, get_uvalue=state, /no_copy


		;**** clear any existing error message ****

  widget_control, state.messid, set_value=' '

		;************************
		;**** process events ****
		;************************

  case event.id of

    state.rootid: begin

	  if event.action eq 'newroot' then begin

		;**** reset the value of the file selection widget ****

	      widget_control, state.rootid, get_value=rootval
	      flesval = { root:rootval.rootpath, file:'',		$
                          arch1:'old archive', arch2:'new archive',	$
                          arch3:'no archive' ,refresh:''}
	      widget_control, state.fileid, set_value=flesval

		;**** update the state ****

	      state.inarchval.file = ''
	      state.inarchval.rootpath = rootval.rootpath

		;**** resensitise file selection ****

	      widget_control,state.fileid,/sensitive

	  endif else if event.action eq 'rootedit' then begin

		;**** when path is being edited desensitise file selection ****

	      widget_control,state.fileid,sensitive=0

	  endif
	  action = 'nofile'
	end

    state.fileid: begin
	  if event.action eq 'nofile' then begin
	      action = event.action
	  endif else begin

		;**** get latest root and file name ****

	      widget_control, state.fileid, get_value=flesval
              state.inarchval.refresh=flesval.refresh

                ;**** keep buttons on for 'no archive' ****

            if flesval.file ne '' then begin      
  	        filename=flesval.root+flesval.file

		;**** test for file read access ****

	        file_acc, filename, exist, read, write, execute, filetype
	        if read eq 0 then begin
	            action = 'nofile'
	            widget_control, state.messid, 			$
		    set_value='no read access for file'
	        endif else begin
	            action = 'newfile'
	        endelse
            endif else begin
                filename = flesval.root+''   ; no archive
                action = 'newfile'
            endelse
	  endelse
	end

    else:

  endcase

		;**** save the new state structure ****

  widget_control, first_child, set_uvalue=state, /no_copy

  return, {id:base, top:event.top, handler:0l, action:action}

end

;-----------------------------------------------------------------------------

function cw_adas804_infile, parent, value=value, title=title, 		$
                            font=font, uvalue=uvalue



  if (n_params() lt 1) then message, 'must specify parent for cw_adas_root'

		;**** set defaults for keywords ****

  if not (keyword_set(value)) then begin
      inarchval = {inarchval, rootpath:'./', 				$
			      file:'', 					$
			      centroot:'', 				$
			      userroot:'',				$
                              refresh:'' }
  end else begin
      inarchval = {inarchval, rootpath:value.rootpath, 			$
			      file:value.file, 				$
			      centroot:value.centroot, 			$
			      userroot:value.userroot,			$
                              refresh:value.refresh }
      if strtrim(inarchval.rootpath) eq '' then begin
          inarchval.rootpath = './'
      endif else if strmid(inarchval.rootpath,strlen(inarchval.rootpath)-1,1) $
							     ne '/' then begin
          inarchval.rootpath = inarchval.rootpath+'/'
      endif
      if strmid(inarchval.file,0,1) eq '/' then begin
          inarchval.file = strmid(inarchval.file,1,strlen(inarchval.file)-1)
      endif
  end

  if not (keyword_set(title)) then title = ''
  if not (keyword_set(font)) then font = ''
  if not (keyword_set(uvalue))  then uvalue = 0

		;**** create the main base for the widget ****

  cwid = widget_base(parent, uvalue = uvalue, 				$
		     event_func = "infile804_event", 			$
		     func_get_value = "infile804_get_val", 		$
		     /column)

		;**** create base to hold the value of state ****

  first_child = widget_base(cwid)

		;**** title for input file ****

  rc = widget_label(first_child, value=title, font=font)

		;**** root path name widget ****

  rootval = {	rootpath:inarchval.rootpath, 				$
		centroot:inarchval.centroot, 				$
		userroot:inarchval.userroot }
  rootid = cw_adas_root(cwid, value=rootval, font=font, nocentral=1)

	 	;**** file selection widget ****

  flesval = {	root:inarchval.rootpath, file:inarchval.file,		$
                arch1:'old archive', arch2:'new archive',		$
                arch3:'no archive', refresh:inarchval.refresh }
  fileid = cw_archive_select(cwid, value=flesval, font=font)

		;**** message ****

  messid = widget_label(cwid, value=' ', font=font)

		;**** test for file read access ****

  filename = inarchval.rootpath+inarchval.file
  if inarchval.file ne '' then begin
      file_acc, filename, exist, read, write, execute, filetype
      if filetype eq '-' and read eq 0 then begin
          widget_control,messid,set_value='no read access for file'
      endif
  endif
  
		;**** create state structure ****

  new_state = { rootid:rootid, fileid:fileid, messid:messid, 		$
		inarchval:inarchval, font:font }

		;**** save initial state structure ****

    widget_control, first_child, set_uvalue=new_state, /no_copy
    
  return, cwid

end
