;+
; PROJECT:
;       ADAS IBM MVS to UNIX conversion
;
; NAME:
;	ADAS809
;
; PURPOSE:
;	The highest level routine for the ADAS 809 program.
;
; EXPLANATION:
;	This routine is called from the main adas system routine, adas.pro,
;	to start the ADAS 809 application.  Associated with adas809.pro
;	is a FORTRAN executable.  The IDL code provides the user
;	interface and output graphics whilst the FORTRAN code reads
;	in data files, performs numerical processing and creates the
;	output files.  The IDL code communicates with the FORTRAN
;	executable via a bi-directional UNIX pipe.  The unit number
;	used by the IDL for writing to and reading from this pipe is
;	allocated when the FORTRAN executable is 'spawned' (see code
;	below).  Pipe communications in the FORTRAN process are to
;	stdin and stdout, i.e streams 5 and 6.
;
;	The FORTRAN code is an independent process under the UNIX system.
;	The IDL process can only exert control over the FORTRAN in the
;	data which it communicates via the pipe.  The communications
;	between the IDL and FORTRAN must be exactly matched to avoid
;	input conversion errors.  The correct amounts of data must be
;	passed so that neither process 'hangs' waiting for communications
;	which will never occur.
;
;	The FORTRAN code performs some error checking which is
;	independent of IDL.  In cases of error the FORTRAN may write
;	error messages.  To prevent these error messages from conflicting
;	with the pipe communications all FORTRAN errors are written to
;	output stream 0, which is stderr for UNIX.  These error messages
;	will appear in the window from which the ADAS session/IDL session
;	is being run.
;
;	In the case of severe errors the FORTRAN code may terminate
;	itself prematurely.  In order to detect this, and prevent the
;	IDL program from 'hanging' or crashing, the IDL checks to see
;	if the FORTRAN executable is still an active process before
;	each group of pipe communications.  The process identifier
;	for the FORTRAN process, PID, is recorded when the process is
;	first 'spawned'.  The system is then checked for the presence
;	of the FORTRAN PID.
;
; USE:
;	First the system settings must be established by calling
;	adas_sys_set.pro then adas809.pro is called to start the
;	ADAS 809 application;
;
;	adas_sys_set, adasrel, fortdir, userroot, centroot, $
;		      devlist, devcode, font_large, font_small, edit_fonts
;	adas809,   adasrel, fortdir, userroot, centroot, devlist, $
;		   devcode, font_large, font_small, edit_fonts
;
; INPUTS:
;	ADASREL - A string indicating the ADAS system version, 
;		  e.g ' ADAS RELEASE: ADAS93 V1.1.0'.  The first
;		  character should be a space.
;
;	FORTDIR - A string holding the path to the directory where the
;		  FORTRAN executables are, e.g '/disk/adas/fortran/exec'
;
;	USERROOT - A string holding the path to the root directory of
;		   the user's adas data. e.g '/disk/bowen/adas'
;		   This root directory will be used by adas to construct
;		   other path names.  For example the users default data
;		   for adas809 should be in /disk/bowen/adas/adf04.  In
;		   particular the user's default interface settings will
;		   be stored in the directory USERROOT+'/defaults'.  An
;		   error will occur if the defaults directory does not
;		   exist.
;
;	CENTROOT - Like USERROOT, but this directory points to the
;		   central data area for the system.  User defaults are
;		   not stored on CENTROOT.
;
;	DEVLIST - A string array of hardcopy device names, used for
;		  graphical output. e.g ['Post-Script','HP-PCL','HP-GL']
;		  This array must mirror DEVCODE.  DEVCODE holds the
;		  actual device names used in a SET_PLOT statement.
;
;	DEVCODE - A string array of hardcopy device code names used in
;		  the SET_PLOT statement, e.g ['PS', 'PCL', 'HP']
;		  This array must mirror DEVLIST.
;
;	FONT_LARGE - The name of a larger font e.g 'courier_bold14'
;
;	FONT_SMALL - The name of a smaller font e.g 'courier_bold12'
;
;	EDIT_FONTS - A structure of two fonts used in the table editor
;		     adas_edtab.pro, {font_norm:'',font_input:''}.
;		     The two fonts are used to differentiate between
;		     editable and non-editable parts of the table. You
;		     may have to experiment to get two fonts which give
;		     the same default spacing in the edit widget.
;		     e.g {font_norm:'helvetica_bold14', $
;			  font_input:'helvetica_oblique14'}
;
; OPTIONAL INPUTS:
;	None
;
; OUTPUTS:
;	None - Note this routine should leave all inputs un-modified.
;
; OPTIONAL OUTPUTS:
;	None
;
; KEYWORD PARAMETERS:
;	None
;
; CALLS:
;	FIND_PROCESS	Checks to see if a given process is active.
;	H9SPF0		Pipe comms with FORTRAN H9SPF0 routine.
;	BXSETP		Pipe comms with FORTRAN BXSETP routine.
;	H9ISPF		Pipe comms with FORTRAN H9ISPF routine.
;	XXDATE		Get date and time from operating system.
;	H9SPF1		Pipe comms with FORTRAN H9SPF1 routine.
;	H9OUTG		Pipe comms with FORTRAN H9OUTG routine.
;	XXADAS          Pipe comms with FORTRAN XXADAS routine.
;
; SIDE EFFECTS:
;	This routine spawns a FORTRAN executable.  Note the pipe 
;	communications routines listed above.  In addition to these
;	pipe communications there is one explicit communication of the
;	date to the FORTRAN code, search for 'printf,pipe' to find it.
;
; CATEGORY:
;	Adas system.
;	
; WRITTEN:
;       Hugh Summers, University of Strathclyde,  30 November 2001
;
; MODIFIED:
;	1.1	Hugh Summers
;		First version
;       1.2     Martin Torney
;               Changed I/O structures to accomodate updated widgets
;       1.3     Martin O'Mullane
;               Change default pass file to standard name. 
;       1.4     Allan Whiteford
;               IDL now waits for FORTRAN to finish before closing pipe
;
; VERSION:
;	1.1	30-11-2001
;       1.2     29-05-2003
;       1.3     07-12-2004
;       1.3     04-04-2005
;-
;-----------------------------------------------------------------------------

 PRO ADAS809, adasrel, fortdir, userroot, centroot, $
              devlist, devcode, font_large, font_small, edit_fonts

		;************************
		;**** Initialisation ****
		;************************

 adasprog = ' PROGRAM: ADAS809 V1.3'
 lpend = 0
 gomenu = 0
 deffile = userroot+'/defaults/adas809_defaults.dat'
 device = ''
 bitfile = centroot + '/bitmaps'


		;******************************************
		;**** Search for user default settings ****
		;**** If not found create defaults     ****
		;******************************************

 files = findfile(deffile)
 if (files(0) eq deffile) then begin
   restore,deffile
   inval.centroot = centroot+'/adf04/'
   inval.userroot = userroot+'/adf04/'
   inval.centroot2 = centroot+'/adf37/'
   inval.userroot2 = userroot+'/adf37/'
 endif else begin
   inval = { $
        	  ROOTPATH1	:	userroot + '/adf04/', 		$
	          FILE1		:	'', 				$
                  F1TYPE        :       0,                              $
		  F2TYPE        :       0,                              $
		  ROOTPATH2	:	userroot + '/adf37/', 		$
                  FILE2 	:	'',                        	$
		  CENTROOT      :       centroot+'/adf04/',             $
		  USERROOT      :       userroot+'/adf04/',             $
		  CENTROOT2     :       centroot+'/adf37/',             $
		  USERROOT2     :       userroot+'/adf37/'              $
	     }
	     
   procval = {NEW:-1}
   outval = {                                     $
		GRPOUT    : 0,                    $
		GTIT1     : '',                   $
		GRPSCAL   : 0,                    $
		XMIN      : '',                   $
		XMAX      : '',                   $
		YMIN      : '',                   $
		YMAX      : '',                   $
		HRDOUT    : 0,                    $
		HARDNAME  : '',                   $
		GRPDEF    : '',                   $
		GRPFMESS  : '',                   $
		GRPSEL    : -1,                   $
		GRPRMESS  : '',                   $
		DEVSEL    : -1,                   $
		GRSELMESS : '',                   $
		ADF04OUT  : 0,                    $
		ADF04APP  : -1,                   $
		ADF04REP  : 0,                    $
		ADF04DSN  : '',                   $
		ADF04DEF  : 'adas809_adf04.pass', $
		ADF04MES  : '',                   $
		TEXOUT    : 0,                    $
		TEXAPP    : -1,                   $
		TEXREP    : 0,                    $
		TEXDSN    : '',                   $
		TEXDEF    : 'paper.txt',          $
		TEXMES    : ''                    $
	     }
	     
 endelse
 
		;****************************
		;**** Start fortran code ****
		;****************************

 get_lun, pipe
 spawn, fortdir + '/adas809.out',unit=pipe,/noshell,PID=pid

		;************************************************
		;**** Get date and write to fortran via pipe ****
		;************************************************

 date = xxdate()
 printf,pipe,date(0)

 LABEL100:
		;**** Check FORTRAN still running ****
  
 if (find_process(pid) eq 0) then goto, LABELEND

		;************************************************
		;**** Communicate with h9spf0 in fortran and ****
		;**** invoke user interface widget for       ****
		;**** Data file selection                    ****
		;************************************************
 
 inval.centroot2 = centroot+'/adf37/'  ; fixes bug when 'Cancel' is 
 inval.userroot2 = userroot+'/adf37/'  ; selected on proc screen
 
 h9spf0, pipe, inval, dsfull1, dsfull2, rep, FONT=font_large

		;**** If cancel selected then end program ****

 if (rep eq 'YES') then goto, LABELEND

		;**** Check FORTRAN still running ****

 if (find_process(pid) eq 0) then goto, LABELEND

		;*************************************
		;**** Read pipe comms from bxsetp ****
		;*************************************

 bxsetp, pipe, sz0, sz, scnte, sil, lfpool, strga

 LABEL200:
		;**** Check FORTRAN still running ****
		
 if (find_process(pid) eq 0) then goto, LABELEND

		;************************************************
		;**** Communicate with h9ispf in fortran and ****
		;**** invoke user interface widget for       ****
		;**** Processing options                     ****
		;************************************************
  
 h9ispf, pipe, lpend, procval,  strga, dsfull1, dsfull2, bitfile,  $
	 gomenu, FONT_LARGE=font_large, FONT_SMALL=font_small, $
	 EDIT_FONTS=edit_fonts
               ;**** If menu button clicked, tell FORTRAN to stop ****
 
 if (gomenu eq 1) then begin
   printf, pipe, 1
   goto, LABELEND
 endif else begin
   printf, pipe, 0
 endelse
 
 		;**** If cancel selected then goto 100 ****
 if (lpend eq 1) then goto, LABEL100

		;**** Fortran processing now in progress. Get ****
		;**** date and time for this run.	      ****

 date = xxdate()

		;**** Create header for output. ****

 header = adasrel+adasprog+' DATE: '+date(0)+' TIME: '+date(1)

 LABEL300:

		;**** Check FORTRAN still running ****
 if (find_process(pid) eq 0) then goto, LABELEND

		;************************************************
		;**** Communicate with h9spf1 in fortran and ****
		;**** invoke user interface widget for       ****
		;**** Output options                         ****
		;************************************************

 h9spf1, pipe, lpend, outval, dsfull1, bitfile, gomenu,	$
         DEVLIST=devlist, FONT=font_large

                ;**** If menu button clicked, tell FORTRAN to stop ****

 if (gomenu eq 1) then begin
   printf, pipe, 1
   outval.texmes = ''
   goto, LABELEND
 endif else printf, pipe, 0

		;**** If cancel selected then erase output ****
		;**** messages and goto 200.		   ****
 
 if (lpend eq 1) then begin
   outval.texmes = ''
   goto, LABEL200
 endif

		;*************************************************
		;**** If text output has been requested write ****
		;**** header to pipe for XXADAS to read.      ****
		;*************************************************

 if (outval.texout eq 1) then printf,pipe,header

		;************************************************
		;**** If graphical output requested          ****
		;**** Communicate with b1outg in fortran and ****
		;**** invoke widget Graphical output         ****
		;************************************************

 if (outval.grpout eq 1) then begin

		;**** User explicit scaling ****

   grpscal = outval.grpscal
   xmin = outval.xmin
   xmax = outval.xmax
   ymin = outval.ymin
   ymax = outval.ymax
	
		 ;**** Hardcopy output ****
   
   hrdout = outval.hrdout
   hardname = outval.hardname
   if (outval.devsel ge 0) then device = devcode(outval.devsel)
 
		 ;*** user title for graph ***

   utitle = outval.gtit1
  
   h9outg, dsfull, pipe, utitle, grpscal, xmin, xmax, ymin, ymax, $
		   hrdout, hardname, device, header, bitfile, gomenu,	$
	           FONT=font_large

                ;**** If menu button clicked, tell FORTRAN to stop ****

   if (gomenu eq 1) then begin
     printf, pipe, 1
     outval.texmes = ''
     goto, LABELEND
   endif else printf, pipe, 0

 endif
     
		;**** Back for more output options ****
 
 GOTO, LABEL300

 LABELEND:

		;**** Ensure appending is not enabled for ****
		;**** text output at start of next run.   ****
 
 outval.texapp = -1

		;**** Save user defaults ****
		
 save, inval, procval, outval, filename=deffile
 junk=''
 readf,pipe,junk,format='(A3)'
 free_lun, pipe

 END
