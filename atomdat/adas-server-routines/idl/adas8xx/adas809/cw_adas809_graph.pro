; Copyright (c) 1995, Strathclyde University .
; SCCS info: Module @(#)cw_adas_graph.pro	1.17 Date 10/21/99
;
;+
; PROJECT:
;       ADAS IBM MVS to   UNIX conversion
;
; NAME:
;	CW_ADAS809_GRAPH()
;
; PURPOSE:
;	Widget for displaying a sequence of ADAS plots.
;
; EXPLANATION:
;	This widget is adapted from version 1.18 of CW_ADAS_GRAPH.PRO.
;       Unconventional is a convenient collection of a graphics window
;	and a number of graphical control buttons.  No actual graphics
;	are included with the widget.  This widget simply generates
;	events each time one of the control buttons is pressed.
;	The event returned is the structure;
;
;	{ID:0L, TOP:0L, HANDLER:0L, ACTION:''}
;
;	Where ACTION may have one of several values;
;
;	'previous', 'next', 'print', 'printall' or 'done'.
;
;       ACTION values unique to the ADAS809 version are:
;
;       'ups', 'downs', 'exc', 'deexc', and 'updown'.
;
;       These indicate the user's choice of function to plot.
;
; USE:
;	An example of how to use the widget.
;
;		base = widget_base()
;		graphid = cw_adas_graph(base,/print,/multiplot)
;		widget_control,base,/realize
;		widget_control,graphid,get_value=value
;		win = value.win
;
;	Then use WSET,win to set the widget to receive graphics output.
;	Use XMANAGER or WIDGET_EVENT() to process the button events from
;	the widget.
;
; INPUTS:
;       PARENT   - The ID of the parent widget.
;
; OPTIONAL INPUTS:
;       None.  See the keywords for additional controls.
;
; OUTPUTS:
;       The return value of this function is the ID of the compound
;       widget which is of type LONG integer.
;
; OPTIONAL OUTPUTS:
;       None
;
; KEYWORD PARAMETERS:
;	VALUE	 - A structure which is the value of this widget;
;		   {WIN:0, MESSAGE:''}
;
;		   WIN	   An integer; The IDL window number of the graphical
;			   output area.
;
;		   MESSAGE A string; A message to be displayed in the widget.
;
;		   WIN is only relevant when the value of the widget
;		   is recovered using GET_VALUE.  The value of WIN is
;		   ignored when cw_adas_graph is first created or
;		   SET_VALUE is used.
;
;	MULTIPLOT- Set this keyword to get the 'Next' and 'Previous' buttons.
;		   By default MULTIPLOT is not set.
;
;	PRINT	 - Set this keyword to get the 'Print' button.  If multiplot
;		   is also set a 'Print All' button will also be included on
;		   the widget.  By default PRINT is not set.
;
;	XSIZE	 - The xsize of the graphics area in pixels.  Default
;		   850 pixels.
;
;	YSIZE	 - The ysize of the graphics area in pixels.  Default
;		   750 pixels.
;
;	TITLE	 - A title to appear at the top of the widget.
;
;	FONT	 - The font to be used for text in the widget.  Default
;		   to current screen font.
;	
;	NODONE	 - Set to 1 if 'done' button not required.
;
;	LOWERBASE- Set to 1 if lower-base not required.
;
; CALLS:
;	LOCALIZE	Routine to set the various position parameters
;			used in output plotting.
;
; SIDE EFFECTS:
;	This widget uses a COMMON BLOCK: CW_ADASGR_BLK to hold the
;       widget state.
;
;       Three other routines are included which are used to manage the
;       widget;
;
;	ADAS_GR_SET_VAL
;	ADAS_GR_GET_VAL()
;	ADAS_GR_EVENT()
;
; CATEGORY:
;	Compound Widget
;
; WRITTEN:
;       Andrew Bowen, Tessella Support Services plc, 2-Apr-1993
;
; MODIFIED: (CW_ADAS_GRAPH)
;       1.1 	Andrew Bowen    
;               First release.
;       1.2 	Andrew Bowen    
;		Re-modelling of widget.
;       1.3 	Andrew Bowen    
;		Message widget added.
;       1.4 	Andrew Bowen    
;		Header comments added.
;       1.5 	Andrew Bowen    
;		Header changed.  Bug in example in USE section.
;       1.6 	Andrew Bowen    
;		Changed default aspect ratio of plot window to allow for
;		fatter characters on OSF/IDL.
;	1.7	Unknown
;	1.8	Lalit Jalota
;	1.9	Lalit Jalota
;	1.10	Tim Hammond	
;		Added keyword BITBUTTON which allows the addition of a button
;		with a bitmapped label if required
;	1.11	Tim Hammond/Ed Mansky (ORNL)
;		Tidied up comments and added new sections to customise output
;		display for different machines - currently only different
;		for HP-UX machines. The procedure localize is called to set
;		various parameters used in the positioning of the output plot.
;	1.12	William Osborn
;		Removed xsize=n keyword from widget_label command.
;		The dynlabel procedure did not override these
;		assignments and they were taken as being values in pixels.
;	1.13	William Osborn
;		Added 'Done but retain' button
;	1.14	William Osborn
;		Added keepid=0L if keep is not selected
;       1.15    William Osborn
;               Added dynlabel
;       1.16    David H.Brooks
;               Added nodone and nolowerbase options as keywords and flag
;               notitle to cut out space if no title is given.
;	1.17	Richard Martin
;		Added adjust & retain buttons/keywords.
;	1.18	Hugh Summers
;		Added contraction of graph area when VERY_SMALL environment.
;
; VERSION: (CW_ADAS_GRAPH)
;       1.1     05-05-93
;	1.2	11-05-93
;	1.3	20-05-93
;	1.4	02-06-93
;	1.5	15-06-93
;	1.6	15-06-93
;	1.7	21-02-95
;	1.8	13-03-95
;	1.9	13-03-95
;	1.10	07-11-95
;	1.12	01-08-96
;	1.13	07-08-96
;	1.14	07-08-96
;	1.15	04-10-96
;	1.16	09-04-97
;	1.17	13-10-99
;	1.18	18-06-01
;
; MODIFIED: (CW_ADAD809_GRAPH)
;       1.1     Martin Torney
;               Added function selection buttons to CW_ADAS_GRAPH
;               First version of CW_ADAS809_GRAPH
;       1.2     Allan Whiteford
;               Changed name of event handlers, get & set value routines
;               and common blocks to have '809' in them.
;       1.3     Martin Torney and Allan Whiteford
;               Fixed bug in logic for buttons turning themselves on
;               and off. Only affected IDL <= 5.4.
;       1.4     Allan Whiteford
;               Put the bottons ('Done' etc) in a base of their own so
;               they aren't scaled along with the upsilon/downsilon buttons.
;
; VERSION: (CW_ADAS809_GRAPH)
;       1.1     28-01-04
;       1.2     25-11-04
;       1.3     25-11-04
;       1.4     25-11-04
;-
;-----------------------------------------------------------------------------

PRO adas809_gr_set_val, id, value

  COMMON cw_adas809gr_blk, state_base, state_stash, state

		;**** Return to caller on error ****
  ON_ERROR, 2

		;**** Retrieve the state ****
  CW_LOADSTATE, id, state_base, state_stash, state

		;**** Update value in state ****
  state.grval.message = value.message

		;**** Update screen message ****
  widget_control,state.messid,set_value=value.message

		;**** Save new state structure ****
  CW_SAVESTATE, id, state_base, state

END

;-----------------------------------------------------------------------------


FUNCTION adas809_gr_get_val, id

  COMMON cw_adas809gr_blk, state_base, state_stash, state

		;**** Return to caller on error ****
  ON_ERROR, 2

		;**** Retrieve the state ****
  CW_LOADSTATE, id, state_base, state_stash, state

		;**** Get IDL window number of the drawing area ****
  widget_control,state.drawid,get_value=value

		;**** Update value ****
  state.grval.win = value

		;**** Return value ****
  RETURN, state.grval

END

;-----------------------------------------------------------------------------

FUNCTION adas809_gr_event, event

  COMMON cw_adas809gr_blk, state_base, state_stash, state

		;**** Base ID of compound widget ****
  base=event.handler

		;**** Retrieve the state ****
  CW_LOADSTATE, base, state_base, state_stash, state

		;**** Clear any message ****
  widget_control,state.messid,set_value=' '
  state.grval.message = ''

		;************************
		;**** Process Events ****
		;************************
  CASE event.id OF

    state.previd:	action = 'previous'

    state.nextid:	action = 'next'

    state.printid:	action = 'print'

    state.printallid:	action = 'printall'

    state.doneid:	action = 'done'

    state.keepid:	action = 'done_but_keep'

    state.adjustid:	action = 'adjust'    
    
    state.retainid:	action = 'retain'

    state.bitid:	action = 'bitbutton'
    
    state.upsilonid: begin
;      upsilonid_index = (where(event.id eq state.upsilonid))[0]
;      if (upsilonid_index ne -1) then begin
        state.upsilon_state = 1 - state.upsilon_state
        state.gr_state(2) = 0
        state.gr_state(3) = 0
        state.gr_state(0) = state.upsilon_state
        if (total(state.gr_state) eq 0) then begin
          state.gr_state(0) = 1
          state.upsilon_state = 1
          widget_control, state.upsilonid, set_button = 1
        endif
;      endif
      if (state.gr_state(0) eq 1) then action = 'ups'
      if (state.gr_state(1) eq 1) then action = 'downs'
      if (total(state.gr_state) eq 2) then action = 'updown'
    end
    
    state.downsilonid: begin
;      downsilonid_index = where(event.id eq state.downsilonid)
;      if (downsilonid_index ne -1) then begin
        state.downsilon_state = 1 - state.downsilon_state
        state.gr_state(2) = 0
        state.gr_state(3) = 0
        state.gr_state(1) = state.downsilon_state
        if (total(state.gr_state) eq 0) then begin
          state.gr_state(1) = 1
          state.downsilon_state = 1
          widget_control, state.downsilonid, set_button = 1
        endif
;      endif
      if (state.gr_state(0) eq 1) then action = 'ups'
      if (state.gr_state(1) eq 1) then action = 'downs'
      if (total(state.gr_state) eq 2) then action = 'updown'
    end    
    
    state.excid: begin
;      excid_index = where(event.id eq state.excid)
;      if (excid_index ne -1) then begin
        state.gr_state(0) = 0
        state.gr_state(1) = 0
        state.gr_state(2) = 1
        state.gr_state(3) = 0
;      endif
      action = 'exc'
    end          
    
    state.deexcid: begin
;      deexcid_index = where(event.id eq state.deexcid)
;      if (deexcid_index ne -1) then begin
          state.gr_state(0) = 0
          state.gr_state(1) = 0
          state.gr_state(2) = 0
          state.gr_state(3) = 1
;      endif
      action = 'deexc'
    end        
  ENDCASE
  
  widget_control, state.upsilonid, set_button = state.gr_state(0)
  widget_control, state.downsilonid, set_button = state.gr_state(1)
  widget_control, state.excid, set_button = state.gr_state(2)
  widget_control, state.deexcid, set_button = state.gr_state(3)
          
  RETURN, {ID:base, TOP:event.top, HANDLER:0L, ACTION:action}
END

;-----------------------------------------------------------------------------

FUNCTION cw_adas809_graph, parent, VALUE=value, MULTIPLOT = multiplot, 	$
			   PRINT = print, BITBUTTON = bitbutton,	$
			   XSIZE = xsize, YSIZE = ysize, TITLE = title,	$
			   FONT = font, KEEP = keep, ADJUST = adjust,   $
                           RETAIN=retain, DONE = nodone,                $
                           NOLOWERBASE = nolowerbase, GR_STATE=gr_state
  
  
  
  COMMON cw_adas809gr_blk, state_base, state_stash, state

  IF (N_PARAMS() EQ 0) THEN MESSAGE, 'Must specify a parent for cw_adas809_graph'
  ON_ERROR, 2					;return to caller

		;**** Set defaults for keywords ****
  IF NOT (KEYWORD_SET(value)) THEN begin
	grval = {grval, WIN:0, MESSAGE:''}
  end else begin
	grval = {grval, WIN:0, MESSAGE:value.message}
  end
  IF NOT (KEYWORD_SET(multiplot)) THEN multiplot = 0
  IF NOT (KEYWORD_SET(print)) THEN print = 0
  IF NOT (KEYWORD_SET(xsize)) THEN xsize = 850

		;**** Customize YSIZE for different TARGET_MACHINEs ****

  machine = GETENV('TARGET_MACHINE')
  IF (machine EQ 'HPUX') THEN BEGIN
    yheight = 650
  ENDIF ELSE BEGIN
    yheight = 750
  ENDELSE
  IF NOT (KEYWORD_SET(ysize)) THEN ysize = 750
  IF NOT (KEYWORD_SET(title)) THEN BEGIN
      title = ' '
      notitle=1
  ENDIF ELSE BEGIN
      notitle=0
  ENDELSE
  IF NOT (KEYWORD_SET(font)) THEN font = ''
  IF NOT (KEYWORD_SET(keep)) THEN keep = 0 else keep=1
  IF NOT (KEYWORD_SET(nodone)) THEN nodone = 0 else nodone=1
  IF NOT (KEYWORD_SET(nolowerbase)) THEN nolowerbase = 0 else nolowerbase=1
  IF NOT (KEYWORD_SET(bitbutton)) THEN BEGIN
      bitflag=0
  ENDIF ELSE BEGIN
      bitflag=1
  ENDELSE
  IF NOT (KEYWORD_SET(adjust)) THEN adjust= 0 else adjust = 1
  IF NOT (KEYWORD_SET(retain)) THEN retain= 0 else retain = 1
  IF NOT (KEYWORD_SET(gr_state)) THEN gr_state = [0,0,1,0]

		;**********************************************************
		;**** Assign parameters defining the size of the graph ****
		;**** in the graphics window in IDL procedure localize ****
		;**********************************************************

  localize

		;**** Create the main base for the graph widget ****

  uvalue = 0
  cwid = WIDGET_BASE(parent, UVALUE = uvalue, 				$
		EVENT_FUNC = "adas809_gr_event", 				$
		FUNC_GET_VALUE = "adas809_gr_get_val", 			$
		PRO_SET_VALUE = "adas809_gr_set_val", 			$
		/COLUMN)

		;**** Create upper base ****

  upperbase = widget_base(cwid,/row)

		;**** Create lower base ****
  if nolowerbase eq 0 then begin
    lowerbase = widget_base(cwid,/row,space=20)
  end else if nolowerbase eq 1 then begin
    if nodone eq 0 or keep eq 1 or bitflag eq 1 or multiplot ne 0 $
      or print eq 1 then stop,$
  'CW_ADAS809_GRAPH ERROR: CHOICE OF KEYWORDS INCOMPATIBLE WITH NOWLOWERBASE'
  end

		;********************************
		;**** The Upper Area         ****
		;********************************

		;**** Graphics area ****

  leftupper = widget_base(upperbase,/column)
  if notitle eq 0 then begin
    rc = widget_label(leftupper,value=title)
  end
  
  vsmall=GETENV('VERY_SMALL')
  if (vsmall EQ 'YES' ) then begin 
      xsize=0.8*xsize
      ysize=0.8*ysize
  end
  
  drawid = widget_draw(leftupper,/frame,xsize=xsize,ysize=ysize)


		;********************************
		;**** The lower Area         ****
		;********************************

  buttonbase=widget_base(lowerbase,/row)

		;**** Extra bitmapped button if specified ****

  if (bitflag eq 1) then begin
      read_X11_bitmap, bitbutton, bitmap1
      bitid = widget_button(buttonbase, value=bitmap1)
  endif else begin
      bitid = 0L
  endelse

		;**** Previous and Next only needed for multi-plots ****
  if multiplot ne 0 then begin
    previd = widget_button(buttonbase,value='Previous',FONT=font)
    nextid = widget_button(buttonbase,value='Next    ',FONT=font)
  end else begin
    previd = 0L
    nextid = 0L
  end

		;**** Print buttons if enabled ****
  if print eq 1 then begin
    printid = widget_button(buttonbase,value='Print   ',FONT=font)
;		;**** Printall only needed for multi-plots ****
;    if multiplot ne 0 then begin
      printallid = widget_button(buttonbase,value='Print All',FONT=font)
;    end else begin
;      printallid = 0L
;    end
  end else begin
    printid = 0L
    printallid = 0L
  end

		;**** Done button ****
  if nodone eq 0 then $       
    doneid = widget_button(buttonbase,value='Done',FONT=font) $
  else doneid = 0L 

		;**** 'Done but retain' button ****
  if keep eq 1 then $
    keepid = widget_button(buttonbase,value='Done but retain',FONT=font, frame=10) $
  else keepid = 0L
		;**** 'Adjust' button ****
  if adjust eq 1 then $
    adjustid = widget_button(buttonbase,value='Adjust',FONT=font) $
  else adjustid=0L  
  
 		;**** 'Retain' button ****
  if retain eq 1 then $
    retainid = widget_button(buttonbase,value='Retain',FONT=font) $
  else retainid=0L 
  
                ;**** Graph Options ****
 
  upsilonbase = widget_base(lowerbase, /column, /nonexclusive)
  upsilonid = widget_button(upsilonbase, value='Upsilon')
  widget_control, upsilonid, set_button=gr_state(0)
  upsilon_state = gr_state(0) 
  
  downsilonid = widget_button(upsilonbase, value='Downsilon')
  widget_control, downsilonid, set_button=gr_state(1)
  downsilon_state = gr_state(1)
  
  qratebase = widget_base(lowerbase, /column, /nonexclusive)
  excid = widget_button(qratebase, value='Excitation')
  widget_control, excid, set_button=gr_state(2)
  exc_state = gr_state(2)
  
  deexcid = widget_button(qratebase, value='De-excitation')
  widget_control, deexcid, set_button=gr_state(3)
  deexc_state = gr_state(3)
  
		;**** Message area ****
  if strtrim(grval.message) ne '' then begin
    message = grval.message
  end else begin
    message=' '
  end
  messid = widget_label(cwid,value=message,font=font)

		;**** Create state structure ****

  new_state =  {DRAWID:drawid, PREVID:previd, BITID:bitid, $
		NEXTID:nextid, PRINTID:printid, PRINTALLID:printallid, $
		DONEID:doneid, MESSID:messid, GRVAL:grval, KEEPID:keepid, $
		RETAINID:retainid, ADJUSTID:adjustid, UPSILONID:upsilonid, $
                UPSILON_STATE:upsilon_state, DOWNSILONID:downsilonid, $
                DOWNSILON_STATE:downsilon_state, EXCID:excid, $
                EXC_STATE:exc_state, DEEXCID:deexcid, DEEXC_STATE:deexc_state, $
                GR_STATE:gr_state}

		;**** Save initial state structure ****

  CW_SAVESTATE, cwid, state_base, new_state

  dynlabel, parent
  RETURN, cwid

END
