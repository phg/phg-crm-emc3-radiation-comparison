;
; PROJECT:
;       ADAS IBM MVS to UNIX conversion
;
; NAME:
;	ADAS809_PLOT
;
; PURPOSE:
;	Generates ADAS809 graphical output.
;
; EXPLANATION:
;	This routine creates a window for the display of graphical
;	output. A separate routine PLOT809 actually plots the
;	graph.
;
; USE:
;	This routine is specific to ADAS809, see b1outg.pro for
;	example use.
;
; INPUTS:
;	(Most of these inputs map exactly onto variables of the same
;	 name in the FORTRAN code.)
;
;
;	DSFULL  - String; Name of data file 
;
;	TITLX   - String; user supplied comment appended to end of title
;
;	TITLM   - String; Information about minimax fitting if selected.
;
;	UTITLE  - String; Optional comment by user added to graph title.
;
;	TEMP	- Double array; temperatures from input data file.
;	
;	RATE    - Double array; Excitation rate coefficient of selected 
;				transition from input data file
;
;	TOSA	- Double array; Selected temperatures for spline fit
;
;	ROSA	- Double array; Excitation rate coefficients for spline fit.
;
;	TOMA	- Double array; Selected temperatures for minimax fit
;
;	DATE	- String; Date of graph production
;
;	LDEF1	- Integer; 1 - use user entered graph scales
;			   0 - use default axes scaling
;
;	XMIN    - String; Lower limit for x-axis of graph, number as string.
;
;	XMAX    - String; Upper limit for x-axis of graph, number as string.
;
;	YMIN    - String; Lower limit for y-axis of graph, number as string.
;
;	YMAX    - String; Upper limit for y-axis of graph, number as string.
;
;	LFSEL	- Integer; 0 - No minimax fitting was selected 
;			   1 - Mimimax fitting was selected
;
;	LOSEL   - Integer; 0 - No interpolated values for spline fit. 
;			   1 - Intepolated values for spline fit.
;
;	NMX	- Integer; Number of temperatures used for minimax fit
;
;	NENER   - Integer; Number of user entered temperature values
;
;	NPSPL	- Integer; Number of interpolated values for spline.
;
;	HRDOUT - Integer; 1 if hardcopy output activated, 0 if not.
;
;	HARDNAME- String; Filename for harcopy output.
;
;	DEVICE	- String; IDL name of hardcopy output device.
;
;	HEADER	- String; ADAS version number header to include in graph.
;
;       BITFILE - String; the path to the dirctory containing bitmaps
;                 for the 'escape to series menu' button.
;
; OPTIONAL INPUTS:
;	None.
;
; OUTPUTS:
;       GOMENU  - Int; flag - set to 1 if user has selected 'escape direct
;                 to series menu , otherwise 0
;
; OPTIONAL OUTPUTS:
;	None.
;
; KEYWORD PARAMETERS:
;	FONT	- String; The name of a font to use for text in the
;		  graphical output widget.
;
; CALLS:
;	CW_ADAS_GRAPH	Graphical output widget.
;	PLOT809		Make one plot to an output device for 809.
;	XMANAGER
;
; SIDE EFFECTS:
;	This routine uses a common block to maintain its state PLOT809_BLK.
;
;	One other routine is included in this file;
;	ADAS809_PLOT_EV	Called via XMANAGER during widget management.
;
; CATEGORY:
;	Adas system.
;
; WRITTEN:
;       Hugh Summers, University of Strathclyde  30 November 2001
;
; MODIFIED:
;	1.1	Hugh Summers
;		First Release
;       1.2     Martin Torney
;               Added a dimension to 'y' to allow various functions to 
;               be plotted.
;
; VERSION:
;	1.1	30-11-01
;       1.2     28-01-04
;
;----------------------------------------------------------------------------

 PRO adas809_plot_ev, event

  COMMON plot809_blk, data, action, win, plotdev, $
	 	      plotfile, fileopen, gomenu

  newplot = 0
  print = 0
  done = 0
		;****************************************
		;**** Set graph and device requested ****
		;****************************************
                
  CASE event.action OF

	'print'	   : begin
			newplot = 1
			print = 1
		     end

	'done'	   : begin
			if (fileopen eq 1) then begin
			  set_plot, plotdev
			  device, /close_file
			endif
			set_plot, 'X'
			widget_control, event.top, /destroy
			done = 1
		     end

        'bitbutton' : begin
            		if (fileopen eq 1) then begin
                	  set_plot, plotdev
                	  device, /close_file
            		endif
            		set_plot, 'X'
            		widget_control, event.top, /destroy
            		done = 1
            		gomenu = 1
        	      end
                      
        'ups':    begin
                    data.gr_value=0
                    data.ytitle = "COLLISION STRENGTH"
                    data.title(0) = "UPSILON VERSUS TEMPERATURE "
                  end
        'downs':  begin
                    data.gr_value=1
                    data.ytitle = "COLLISION STRENGTH"
                    data.title(0) = "DOWNSILON VERSUS TEMPERATURE "
                  end
        'exc':    begin
                    data.gr_value=2
                    data.ytitle = "RATE COEFF. (CM**3/SEC)"
                    data.title(0) = "EXCITATION RATE COEFFICIENT "+ $
                      "VERSUS TEMPERATURE "
                  end
        'deexc':  begin
                    data.gr_value=3
                    data.ytitle = "RATE COEFF. (CM**3/SEC)"
                    data.title(0) = "DE-EXCITATION RATE COEFFICIENT "+ $
                      "VERSUS TEMPERATURE "
                  end
        'updown': begin
                    data.gr_value=4
                    data.ytitle = "COLLISION STRENGTH"
                    data.title(0) = "UPSILON AND DOWNSILON "+ $
                      "VERSUS TEMPERATURE "
                  end        
        

  END

		;*******************************
		;**** Make requested plot/s ****
		;*******************************

  if (done eq 0) then begin
		;**** Set graphics device ****
    if (print eq 1) then begin
      set_plot, plotdev
      if (fileopen eq 0) then begin
        fileopen = 1
        device, filename=plotfile
	device, /landscape
      endif
    endif else begin
      set_plot, 'X'
      wset, win
    endelse

		;**** Draw graphics ****
                
    plot809, data.gr_value, data.x , data.y, data.nener, data.nmx, $
             data.npspl, data.nplots, data.title, data.xtitle, $
             data.ytitle, data.ldef1, data.xmin, data.xmax, $
             data.ymin, data.ymax 

    if (print eq 1) then begin
      message = 'Plot  written to print file.'
      grval = {WIN:0, MESSAGE:message}
      widget_control, event.id, set_value=grval
    endif

  endif

 END

;----------------------------------------------------------------------------

 PRO adas809_plot,  dsfull, $
		    titlx, titlm, utitle, date, $
		    temp, rate, drate, tosa, rosa, drosa, $
                    toma, roma, droma, $
                    ldef1, xmin, xmax, ymin, ymax, $
                    lfsel, losel, nmx, nener, npspl, $
                    hrdout, hardname, device, header, $
		    bitfile, gomenu, FONT=font

  COMMON plot809_blk, data, action, win, plotdev, $
		      plotfile, fileopen, gomenucom

		;**** Copy input values to common ****
  plotdev = device
  plotfile = hardname
  fileopen = 0
  gomenucom = gomenu
 
		;************************************
		;**** Create general graph title ****
		;************************************
                
  title = strarr(5)

  title(0) = "EXCITATION RATE COEFFICIENT VERSUS TEMPERATURE " 
  if (strtrim(strcompress(utitle),2)  ne ' ') then begin
    title(0) = title(0)+': '+strupcase(strtrim(utitle,2))
  endif
  title(1) = 'ADAS    :'+strupcase(header)
  title(2) = 'FILE     :'+strcompress((titlx))
  if (lfsel eq 1) then begin
    title(3)  = "MINIMAX : " + strupcase(titlm)
    title(4)  = 'KEY     : (CROSSES - INPUT DATA) '+ $ 
		'(FULL LINE - SPLINE FIT)'+'   (DASH LINE - MINIMAX) '
  endif else begin
    title(3) = 'KEY     : (CROSSES - INPUT DATA) (FULL LINE - SPLINE FIT)'
    title(4) = ' ' 
  endelse
  
		;*************************************
		;***   Set up Y data for plots     ***
		;*** dimension of y axis depends   ***
		;*** upon selected fitting options ***
		;*************************************
  
  if ((lfsel eq 1) and (losel eq 1)) then begin
    ydim = nmx > npspl
  endif else begin
    if (lfsel eq 1) then ydim= nmx  else $
      if (losel eq 1) then ydim= npspl else $ 
            ydim = nener
  endelse
  
  y = make_array(4, 3, ydim, /float)
  
                ;**********************************
                ;*** Put upsilons into y(0,*,*) ***
                ;**********************************
  
  valid_data = where((rate(0,*) gt 1e-37) and (rate(0,*) lt 1e37))
  if (valid_data(0) ge 0) then begin
    y(0,0,valid_data) = rate(0,valid_data)
     if (losel eq 1) then begin
        valid_data = where((rosa(0,*) gt 1e-37) and (rosa(0,*) lt 1e+37))
	if (valid_data(0) ge 0) then begin
           y(0, 1, valid_data) = rosa(0,valid_data)
        endif else begin
           print, "ADAS809 : unable to plot upsilon spline fit data"
        endelse
     endif    
     if (lfsel eq 1) then begin
        valid_data = where((roma(0,*) gt 1e-37) and (roma(0,*) lt 1e+37))
	if (valid_data(0) ge 0) then begin
           y(0, 2, valid_data) = roma(0,valid_data)
        endif else begin
           print, "ADAS809 : unable to plot upsilon polynomial fit data"
        endelse
     endif
  endif else begin
           print, "ADAS809 : unable to plot upsilon"
  endelse
  
                ;************************************
                ;*** Put downsilons into y(1,*,*) ***
                ;************************************  
  
  valid_data = where((rate(1,*) gt 1e-37) and (rate(1,*) lt 1e37))
  if (valid_data(0) ge 0) then begin
    y(1,0,valid_data) = rate(1,valid_data)
     if (losel eq 1) then begin
        valid_data = where((rosa(1,*) gt 1e-37) and (rosa(1,*) lt 1e+37))
	if (valid_data(0) ge 0) then begin
           y(1, 1, valid_data) = rosa(1,valid_data)
        endif else begin
           print, "ADAS809 : unable to plot upsilon spline fit data"
        endelse
     endif    
     if (lfsel eq 1) then begin
        valid_data = where((roma(1,*) gt 1e-37) and (roma(1,*) lt 1e+37))
	if (valid_data(0) ge 0) then begin
           y(1, 2, valid_data) = roma(1,valid_data)
        endif else begin
           print, "ADAS809 : unable to plot upsilon polynomial fit data"
        endelse
     endif
  endif else begin
           print, "ADAS809 : unable to plot upsilon"
  endelse 
                
                ;*************************************************
                ;*** Put excitation coefficients into y(2,*,*) ***
                ;*************************************************
     
  valid_data = where((rate(2,*) gt 1e-37) and (rate(2,*) lt 1e+37))
  if (valid_data(0) ge 0) then begin
     y(2, 0, valid_data) = rate(2,valid_data)
     if (losel eq 1) then begin
        valid_data = where((rosa(2,*) gt 1e-37) and (rosa(2,*) lt 1e+37))
	if (valid_data(0) ge 0) then begin
           y(2, 1, valid_data) = rosa(2,valid_data)
        endif else begin
           print, "ADAS809 : unable to plot excitation spline fit data"
        endelse
     endif
     if (lfsel eq 1) then begin
        valid_data = where((roma(2,*) gt 1e-37) and (roma(2,*) lt 1e+37))
	if (valid_data(0) ge 0) then begin
           y(2, 2, valid_data) = roma(2,valid_data)
        endif else begin
           print, "ADAS809 : unable to plot excitation polynomial fit data"
        endelse
     endif
  endif else begin
           print, "ADAS809 : unable to plot excitation data"
  endelse
  
                ;****************************************************
                ;*** Put de-excitation coefficients into y(2,*,*) ***
                ;****************************************************     
  
  valid_data = where((rate(3,*) gt 1e-37) and (rate(3,*) lt 1e+37))
  if (valid_data(0) ge 0) then begin
     y(3, 0, valid_data) = rate(3,valid_data)
     if (losel eq 1) then begin
        valid_data = where((rosa(3,*) gt 1e-37) and rosa(3,*) lt 1e37)
        if (valid_data(0) ge 0) then $
           y(3, 1, valid_data) = rosa(3,valid_data) $
        else $
           print, "ADAS809 : unable to plot de-excitation spline filt data"
     endif
     if (lfsel eq 1) then begin
        valid_data = where((roma(3,*) gt 1e-37) and (roma(3,*) lt 1e+37))
        if (valid_data(0) ge 0) then $
           y(3, 2, valid_data) = roma(3,valid_data) $
        else $
           print, "ADAS809 : unable to plot de-excitation polynomial data"
     endif
  endif else print, "ADAS809 : unable to plot de-excitation data"
  
  ytitle = "RATE COEFF. (CM**3/SEC)"
 
		;**************************************
		;**** Set up x axis and xaxis title ***
		;**************************************
  if ((lfsel eq 1) and (losel eq 1)) then begin
     xdim = nmx > npspl
  endif else begin
      if (lfsel eq 1) then xdim= nmx  else $
          if (losel eq 1) then xdim= npspl else $ 
             xdim = nener
  endelse
  x = fltarr(3, xdim)

		;*** write x axis array ***
  x(0,0:nener-1) = temp
  nplots = 1

  if (losel eq 1) then begin
     x(1,0:npspl-1) = tosa
     nplots = nplots +2
  endif 
  if (lfsel eq 1) then begin
     x(2,0:nmx-1)  = toma
     nplots = nplots + 4
  endif
  
  
  xtitle = " TEMPERATURE (KELVIN)"

  		;******************************************
		;*** if desired set up axis scales      ***
		;******************************************
  if (ldef1 eq 0) then begin
      xmin = min(x(*,1)) 	; data has to be monotonically increasing.
      xmin = xmin * 0.9
      xmax = max(x) * 1.1

      ymin = min(y(*,1))	; data has to be monotonically increasing.
      ymin = ymin * 0.9
      ymax = max(y) * 1.1
  endif
  
		;*************************************
		;**** Create graph display widget ****
		;*************************************
  graphid = widget_base(TITLE='ADAS809 GRAPHICAL OUTPUT', $
					XOFFSET=50,YOFFSET=0)
  bitval = bitfile + '/menu.bmp'
  cwid = cw_adas809_graph(graphid,print=hrdout,FONT=font, bitbutton = bitval)

                ;**** Realize the new widget ****
  widget_control,graphid,/realize

		;**** Get the id of the graphics area ****
  widget_control,cwid,get_value=grval
  win = grval.win

		;*******************************************
		;**** Put the graphing data into common ****
		;*******************************************
  
  gr_value = 2 ; i.e. plot excitation rate coefficient by default
                
  data = {    	gr_value:gr_value, y:y,	x:x, nener:nener, nmx:nmx, $
                npspl:npspl, nplots:nplots,title:title, xtitle:xtitle, $
                ytitle:ytitle, ldef1:ldef1, xmin:xmin,xmax:xmax, $
                ymin:ymin, ymax:ymax $
          }
 
  wset,win

  
  plot809, gr_value, x , y, nener, nmx, npspl, nplots, title, xtitle, $
           ytitle, ldef1, xmin, xmax, ymin, ymax 


		;***************************
                ;**** make widget modal ****
		;***************************
  xmanager,'adas809_plot',graphid,event_handler='adas809_plot_ev', $
                                        /modal,/just_reg

  gomenu = gomenucom

END
