;+
; PROJECT : ADAS 
;
; NAME    : H9INT
;
; PURPOSE : Integrates adf04 type-I data to give effective collision
;           strengths.
;
; EXPLANATION:
;       This routine calls a subroutine version of ADAS809 (called h9int)
;       via a shared object which performs integration. The adf04 data
;       should be read externally by read_adf04.       
;
; USE:
;       An example;
;
;		read_adf04,file='/.../helike_idp04he0_t1.dat',fulldata=adf04data
;        	te=[5.00e+02, 1.00e+03, 2.00e+00, 3.00e+03]
;      		h9int,adf04data=adf04data,te=te,/kelvin,upsilon=upsilon,dist=0
;		plot,te,upsilon[*,5],/xlog
;
;       this integrates all of the date in the type-I file and then plots
;       the effective collision strength of the 5th transition.
;
; INPUTS:
;       adf04data - Structure containing ADF04 type-I data
;       te        - Temperatures for output.
;
; OPTIONAL INPUTS:
;       dist      - Distribution type:
;				  0 - Maxwellian [default]
;				  1 - Kappa
;				  2 - Numerical
;				  3 - Druvesteyn
;       dparam    - Distribution parameter:
;				  Maxwellian: N/A
;				  Kappa:      kappa
;				  Numerical:  N/A
;				  Druvesteyn: x
;	dfile    -  ADF37 file for numerical distribution
;       ev       -  Output temperatures are in eV [default]
;       kelvin   -  Output temperatures are in Kelvin
;       reduced  -  Output temperatures are in reduced units
;       fortdir  -  Directory to find shared object file
;
; OUTPUTS:
;       upsilon  -  Effective collision strength at selected
;                   temperatures.
;
; OPTIONAL OUTPUTS:
;       dnsilon  -  Effective collision strength (downwards) at
;                   selected temperatures. Note this only applies
;                   in the non-Maxwellian case.
;
; KEYWORD PARAMETERS:
;       None.
;
; CALLS:
;       None.
;
; SIDE EFFECTS:
;       This function calls a shared object via call_external.
;
;
; WRITTEN:
;       Allan Whiteford
;
; MODIFIED:
;       Version 1.1   Allan Whiteford   17-03-2005
;                     First release.
;       Version 1.2   Allan Whiteford   23-03-2005
;                     Pass IPLA correctly
;       Version 1.3   Allan Whiteford   10-06-2005
;                     dtype and dparam cast explicitly to long
;                     and double respectively.
; VERSION:
;       1.1     17-03-2005
;       1.2     23-03-2005
;       1.3     10-06-2005
;-
;-----------------------------------------------------------------------------

pro h9int,	adf04data=adf04data,te=te,		$
		upsilon=upsilon,dnsilon=dnsilon,	$
		dist=dist,dparam=dparam,dfile=dfile,	$
                ev=ev,kelvin=kelvin,reduced=reduced,	$
                fortdir=fortdir,help=help

	; **************************************************
	; Show help if requested or if arguments are missing
	; **************************************************

	if keyword_set(help) then begin 
   		doc_library, 'h9int' 
   		return
	endif

	if not arg_present(adf04data) then begin
     		doc_library, 'h9int' 
   		return
        endif

	if not arg_present(te) then begin
     		doc_library, 'h9int' 
   		return
        endif

	if not arg_present(upsilon) then begin
     		doc_library, 'h9int' 
   		return
        endif

	; **************************************************
	; These parameters much match the fortran dimensions
	; **************************************************

	ndmet  = 4
	ndlev  = 110
	ndtrn  = 2100
	nedim  = 50
	ndtin  = 50
        
	; **************************************************
	; Extract scalers (array sizes etc.) from data
	; **************************************************
        
	iz=long(adf04data.iz)
        iz1=iz+1
	iadftyp=adf04data.iadftyp

        itran=(size(adf04data.aval))[1]
        nv=(size(adf04data.gamma))[2]
        il=(size(adf04data.ia))[1]

	; **************************************************
	; Prepare adf04 input variables for shared object
	; **************************************************
	
        bwnoa=dblarr(ndmet)
        wa=dblarr(NDLEV)
   	ia=lonarr(ndlev)
        xja=dblarr(ndlev)
   	i1a=lonarr(ndtrn)
   	i2a=lonarr(ndtrn)
	zpla=dblarr(ndmet,ndlev)
   	scx=dblarr(nedim)
        aval=dblarr(ndtrn)
        omga=dblarr(nedim,ndtrn)
        tcode = strarr(ndtrn)
        beth=dblarr(ndtrn)
	ipla=lonarr(ndmet,ndlev)

	; **************************************************
	; Fill variables from adf04 data structure
	; **************************************************

        if (size(adf04data.bwnoa))[0] eq 1 then begin
        	bwnoa[0:(size(adf04data.bwnoa))[1]-1]=adf04data.bwnoa    
	endif else begin
        	bwnoa[0]=adf04data.bwnoa    
        endelse
        for i=0,(size(adf04data.zpla))[1]-1 do begin
	   	zpla[i,0:(size(adf04data.zpla))[2]-1]=adf04data.zpla[i,*]
        end
        for i=0,(size(adf04data.ipla))[1]-1 do begin
	   	ipla[i,0:(size(adf04data.ipla))[2]-1]=adf04data.ipla[i,*]
        end
        
        wa[0:il-1]=adf04data.wa        
        ia[0:il-1]=adf04data.ia      
   	xja[0:il-1]=adf04data.xja        
	
        i1a[0:itran-1]=adf04data.lower       
   	i2a[0:itran-1]=adf04data.upper       
        tcode[0:itran-1] = adf04data.tcode 
	beth[0:itran-1]=adf04data.beth 
   	aval[0:itran-1]=adf04data.aval   

        scx[0:nv-1]=adf04data.te     
        
        for i=0,itran-1 do begin
   		omga[0:nv-1,i]=adf04data.gamma[i,*]   
        end

	; **************************************************
	; Encode variables which IDL doesn't have a type for
	; **************************************************

	stcode=byte(tcode)
	stcode=long(stcode)
   
        ilbeth=adf04data.lbeth 
        
	; **************************************************
	; Process and prepare inputs for shared object call
	; **************************************************
	

        if keyword_set(dist) and dist ne 0 then begin
        	dtype=long(dist)
        	dparam=double(dparam)
        	adf37=dfile
  	endif else begin
        	dtype=0l
                dparam=0d0
                adf37=''
        endelse

        ifout=2
        if keyword_set(ev) then ifout=2l
        if keyword_set(kelvin) then ifout=1l
        if keyword_set(reduced) then ifout=3l
  
        maxt=(size(te))[1]
        tine=dblarr(ndtin)
        tine[0:maxt-1]=te

	; **************************************************
	; Prepare outputs for shared object call
	; **************************************************

        upsilon=dblarr(ndtin,ndtrn)
        dnsilon=dblarr(ndtin,ndtrn)

	; **************************************************
	; Find path to binary if not specified
	; **************************************************

	if (not arg_present(fortdir)) then fortdir=GETENV('ADASFORT')

	; **************************************************
	; Call fortran routine to perform calculation
	; **************************************************

	dummy = CALL_EXTERNAL(  fortdir+'/h9int_if.so'   , 'h9int_if'    , $
				itran  , stcode , i1a    , i2a   , aval  , $
                  		iadftyp, DTYPE  , ADF37  , iz1   , MAXT  , $
                  		beth   , il     , nv     , ia    , wa    , $
                  		xja    , omga   , scx    , ilbeth, DPARAM, $
                  		zpla   , bwnoa  , ipla   , TINE  , IFOUT , $
                                upsilon, dnsilon                           )

	; **************************************************
	; Cut down outputs to sensible size
	; **************************************************

	upsilon=upsilon[0:maxt-1,0:itran-1]
	dnsilon=dnsilon[0:maxt-1,0:itran-1]
end
