;
; PROJECT:
;       ADAS IBM MVS to UNIX conversion
;
; NAME:
;	H9OUTG
;
; PURPOSE:
;	Communication with ADAS809 FORTRAN via UNIX pipe and
;	graphics output.
;
; EXPLANATION:
;	The routine begins by reading information from the ADAS809
;	FORTRAN process via a UNIX pipe.  Then the IDL graphical
;	output routine for ADAS809 is invoked.  Communications are to
;	the FORTRAN subroutine H9OUTG.
;
; USE:
;	The use of this routine is specific to ADAS809 see adas809.pro.
;
; INPUTS:
;	DSFULL   - Data file name
;
;	PIPE	 - The IDL unit number of the bi-directional pipe to the
;		   ADAS809 FORTRAN process.
;
;       UTITLE   - Optional comment by user
;
;	GRPSCAL	 - Integer; 0 if default scaling required 1 if user
;		   specified scaling to be used.
;
;	XMIN	 - String; User sepcified x-axis minimum, number as string.
;
;	XMAX	 - String; User sepcified x-axis maximum, number as string.
;
;	YMIN	 - String; User specified y-axis minimum, number as string.
;
;	YMAX	 - String; User specified y-axis maximum, number as string.
;
;	HRDOUT	 - Integer; 0 if no hardcopy required, 1 if hardcopy.
;
;	HARDNAME - String; file name for hardcopy output.
;
;	DEVICE	 - String; IDL name for hardcopy output device.
;
;	HEADER	 - ADAS version header information for inclusion in the
;		   graphical output.
;
;       BITFILE - String; the path to the dirctory containing bitmaps
;                 for the 'escape to series menu' button.
;
; OPTIONAL INPUTS:
;	None.
;
; OUTPUTS:
;       GOMENU  - Int; flag - set to 1 if user has selected 'escape direct
;                 to series menu , otherwise 0
;
; OPTIONAL OUTPUTS:
;	None.
;
; KEYWORD PARAMETERS:
;	FONT	- String; Name of the font to be used for the graphical
;		  output widget.
;
; CALLS:
;	ADAS809_PLOT	ADAS809 graphical output.
;
; SIDE EFFECTS:
;	This routine reads information from FORTRAN via a UNIX pipe.
;
; CATEGORY:
;	Adas system.
;
; WRITTEN:
;       Hugh Summers, University of Strathclyde, 30 November 2001
;
; MODIFIED:
;	1.1	Hugh Summers
;		First Release
;       1.2     Martin Torney
;               Added extra dimension to all the rate arrays.
;               These now contain upsilons, downsilons, and excitation 
;               and de-excitation rate coefficients
; VERSION:
;	1.1	30-11-01
;       1.2     28-01-04
;
;-----------------------------------------------------------------------------



PRO H9OUTG, dsfull, pipe, utitle, grpscal, xmin, xmax, ymin, ymax, $
		  hrdout, hardname, device, header, bitfile, gomenu,	$
	  	  FONT=font


                ;**** Set defaults for keywords ****
  IF NOT (KEYWORD_SET(font)) THEN font = ''


		;**** Declare variables for input ****

  titlx = " "
  titlm = " "
  date = " "
  nener = 0
  npspl = 0
  nmx = 0
  ldef1 = 0
  xmin = 0.0
  xmax = 0.0
  ymin = 0.0
  ymax = 0.0
  lfsel = 0
  dummy = 0.0D
  input = 0

		;********************************
		;**** Read data from fortran ****
		;********************************
		
		;*** Read titles and date information ***
  readf, pipe, format = '(a120)' , titlx 
  readf, pipe, format = '(a80)' , titlm 
  readf, pipe, format = '(a8)' , date 
  readf, pipe, input
  nener = input
  readf, pipe, input
  npspl = input
  

		;**** now declare array dimensions ****
  temp = dblarr(nener)
  rate = dblarr(4,nener)
  drate = dblarr(4,nener)

		;*** Read data from file ***
  for i=0, nener-1 do begin
     readf, pipe, dummy
     temp(i) = dummy 
  endfor
  for j=0, 3 do begin
    for i=0, nener-1 do begin
      readf, pipe, dummy
      rate(j,i) = dummy
    endfor 
  endfor

		;*** read spline interpolated values ***
  readf, pipe, losel
  if (losel eq 1) then begin
     tosa = dblarr(npspl)
     rosa = dblarr(4,npspl)
     drosa = dblarr(4,npspl)
     for i=0, npspl-1 do begin
        readf, pipe, dummy
        tosa(i) = dummy 
     endfor
     for j=0, 3 do begin
       for i=0, npspl-1 do begin
         readf, pipe, dummy
         rosa(j,i) = dummy
       endfor 
     endfor
  endif
  
		;*** Read user selected axes ranges if any ***
  int = 0
  readf, pipe, int
  ldef1 = int
  if (ldef1 eq 1) then begin
     readf, pipe, dummy
     xmin = dummy
     readf, pipe, dummy
     xmax = dummy
     readf, pipe, dummy
     ymin = dummy
     readf, pipe, dummy
     ymax = dummy
  endif 

		;** read minimax fit data if chosen ***

  readf, pipe, input 
  lfsel = input
  if (lfsel eq 1) then begin
     readf, pipe, input
     nmx = input
     toma = fltarr(nmx)
     roma = fltarr(4,nmx)
     droma = dblarr(4,nmx)
     for i=0, nmx-1 do begin
        readf, pipe, dummy
        toma(i) = dummy
     endfor
     for j=0, 3 do begin
       for i=0, nmx - 1 do begin
         readf, pipe, dummy
         roma(j,i) = dummy
       endfor
     endfor
  endif
  
		;***********************
		;**** Plot the data ****
		;***********************
 
  adas809_plot, dsfull, $
  		titlx, titlm, utitle, date, $
 		temp, rate, drate, tosa, rosa, drosa, $
                toma, roma, droma, $
                ldef1, xmin, xmax, ymin, ymax, $
   		lfsel, losel, nmx, nener, npspl, $
		hrdout, hardname, device, header, $
		bitfile, gomenu, FONT=font


END
