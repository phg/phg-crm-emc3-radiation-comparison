;+
; PROJECT:
;       ADAS IBM MVS to UNIX conversion
;
; NAME:
;	H9SPF0
;
; PURPOSE:
;	IDL user interface and communications with ADAS809 FORTRAN
;	process via pipe.
;
; EXPLANATION:
;	Firstly this routine invokes the part of the user interface
;	used to select the input dataset for ADAS809.  When the user's
;	interactions are complete this routine communicates with the
;	ADAS809 FORTRAN application via a UNIX pipe.  Communications
;	are to the FORTRAN subroutine B1SPF0.
;
; USE:
;	The use of this routine is specific to ADAS809, see adas809.pro.
;
; INPUTS:
;	PIPE	- The IDL unit number of the bi-directional pipe to the
;		  ADAS809 FORTRAN process.
;
;	VALUE	- A structure which determines the initial settings of
;		  the dataset selection widget.  The initial value is
;		  set in adas809.pro.  VALUE is passed un-modified
;		  through to cw_adas_in.pro, see that routine for a full
;		  description.
;
; OPTIONAL INPUTS:
;	None
;
; OUTPUTS:
;	VALUE	- On output the structure records the final settings of
;		  the dataset selection widget if the user pressed the
;		  'Done' button otherwise it is not changed from input.
;
;	DSFULL1 - String; The full system file name of the input 
;		  dataset selected by the user for processing.
;
;	DSFULL2	- String; The full system file name of the comparative 
;		  dataset selected by the user for processing.
;
;	REP	- String; Indicates whether the user pressed the 'Done'
;		  or 'Cancel' button on the interface.  The action is
;		  converted to the strings 'NO' and 'YES' respectively
;		  to match up with the existing FORTRAN code.  In the
;		  original IBM ISPF interface REP was the reply to the
;		  question 'DO YOU WANT TO END PROGRAM EXECUTION'.
;
; OPTIONAL OUTPUTS:
;	None
;
; KEYWORD PARAMETERS:
;	FONT	- Supplies the font to be used for the interface widgets.
;
; CALLS:
;	ADAS_IN		Pops-up the dataset selection widget.
;       UTC_IS_FLOAT    Check if a string can safely be converted 
;                       to a float
;
; SIDE EFFECTS:
;	This routine communicates with the ADAS809 FORTRAN process
;	via a UNIX pipe.
;
; CATEGORY:
;	Adas system.
;
; WRITTEN:
;       Hugh Summers, University of Strathclyde,  30 November 2001
;
; MODIFIED:
;	1.1	Hugh Summers
;		First version
;       1.2     Martin Torney
;               Updated to accomodate non-Maxwellians
;
; VERSION:
;	1.1	30-11-01
;       1.2     19-12-02
;-
;-----------------------------------------------------------------------------

 PRO H9SPF0, pipe, value, dsfull1, dsfull2, rep, FONT=font

		;***********************************
                ;**** Set defaults for keywords ****
		;***********************************
		
 IF NOT (KEYWORD_SET(font)) THEN font = ''

		;**********************************
		;**** Pop-up input file widget ****
		;**********************************
		
 adas809_in, value, action, WINTITLE = 'ADAS 809 INPUT', $
	     TITLE = 'Input ADF04 - type 1 or ADF37 - type 1 file', $
             FONT = font

		;********************************************
		;**** Act on the event from the widget   ****
		;**** There are only two possible events ****
		;**** 'Done' and 'Cancel'.               ****
		;********************************************
		
 if (action eq 'Done') then begin
   rep = 'NO'
 end else rep = 'YES'

		;**********************************
		;**** Construct datatset names ****
		;**********************************
		
 dsfull1 = strcompress(value.rootpath1+value.file1)
 CASE value.f1type OF
   0: file_type1 = 'adf04'
   1: file_type1 = 'adf37'
   else:
 endCASE
 
 value.file2 = strtrim(value.file2, 2)
 if (value.file2 ne '') then begin
   dsfull2 = strcompress(value.rootpath2+value.file2)
   string = ''
   openr, chan, dsfull2, /get_lun
   readf, chan, string, format='(a6)'
   free_lun, chan
      
   CASE value.f2type OF
   
     0: file_type2 = 'adf043'
     1: file_type2 = 'adf044'
     2: file_type2 = 'adf37'
     else:
   
   endCASE
      
 endif else begin
   dsfull2 = ''
   file_type2 = ''
 endelse
    
		;*******************************
		;**** Write data to fortran ****
		;*******************************
 
 printf, pipe, rep
 printf, pipe, dsfull1
 printf, pipe, dsfull2
 printf, pipe, file_type1
 printf, pipe, file_type2
  
 end
