;
; PROJECT:
;       ADAS IBM MVS to UNIX conversion
;
; NAME:
;       CW_ADAS809_PROC()
;
; PURPOSE:
;       Produces a widget for ADAS809 processing options/input.
;
; EXPLANATION:
;       This function creates a compound widget consisting of :-
;          a text widget holding an editable 'Run title', 
;          the dataset name/browse widget ,
;          a widget to select the transition for analysis,
;          a widget to request a mimax fit and enter a tolerance for it,
;          a table widget for temperature/density data ,
;          a button to enter default values into the table, 
;          a message widget, and a 'Cancel' and a 'Done' button.
;
;       The compound widgets included in this widget are self managing.
;       This widget only manages events from the two 'Defaults' buttons,
;       the 'Cancel' button and the 'Done' button.
;
;       This widget only generates events for the 'Done' and 'Cancel'
;       buttons.
;
; USE:
;       This widget is specific to ADAS809, see adas809_proc.pro
;       for use.
;
; INPUTS:
;       TOPPARENT- Long integer, ID of parent widget.
;
;       DSFULL  - String; The full system file name of the input 
;                 dataset selected by the user for processing.
;
;       STRGA   - String array; designations for the levels
;
;       NDTIN   - Integer; number of temp/density pairs selected
;
;       IL      - Integer; Number of energy index levels
;
;       NDTEM   - Integer; maximum number of input data file temps.
;
;       NV      - Integer; number of gamma/temp pairs for the selected
;                          transition
;       
;       ITRAN   - Integer; Number of electron impact transitions
;
;       TSCEF   - Double array ; Temperature values from input data file.
;       
;       I1A     - Integer array; Lower energy level index
;
;       I2A     - Integer array; Upper energy level index
;       
;       The inputs map exactly onto variables of the same
;       name in the ADAS809 FORTRAN program.
;
;       BITFILE - String; the path to the dirctory containing bitmaps
;                 for the 'escape to series menu' button.
;
; OPTIONAL INPUTS:
;       None
;
; OUTPUTS:
;       The return value of this function is the ID of the declared widget.
;
;       ACT     - String; result of this widget, 'Done', 'Cancel' or 'Menu'
;
; OPTIONAL OUTPUTS:
;       None
;
; KEYWORD PARAMETERS:
;       PROCVAL - A structure which determines the initial settings of
;                 the processing options widget.
;
;                 The default PROCVAL is created thus;
;
;               ps = {proc809_set,            $
;                       new     : 0,          $
;                       title   : '',         $
;                       istrn   : 0,          $
;                       maxt    : 0,          $
;                       ifout   : 1 ,         $
;                       tine    : tscef(1,*), $
;                       lfsel   : 0,          $
;                       tolval  : '5',        $
;                       losel   : 0,          $
;                       dtype   : '',         $
;                       kappa   : 0,          $
;                       num_val : 0,          $
;                       ilinr   : 0,          $
;                       ifint   : 0,          $
;                       iescl   : 0           $
;             }
;
;
;               NEW     Flag which defines whether or not default values
;                       exist or not. (< 0 if no values exist)
;               TITLE   Entered general title for program run
;               ISTRN   Selected transition index
;               MAXT    Number of temperature values selected
;               IFOUT   Index indicating which units are being used
;               TINE    User supplied temperature values for fit.
;               LFSEL   Flag as to whether polynomial fit is chosen
;               TOLVAL  Tolerance required for goodness of fit if
;                       polynomial fit is selected.
;               LOSEL   Flag whether or not interpolated values for spline
;                       fit have been used.
;               DTYPE   Type of distribution function selected, 0, 1, 2 
;                       for Maxwellian, Kappa or Numerical respectively
;               KAPPA   Value of kappa is a kappa distribution is selected
;               NUM_VAL Name of the adf37 file if numerical distribution 
;                       is selected
;               ILINR   Option for linear(0) or quadratic(1) interpolation
;               IFINT   Independant variable for interpolation, 0, 1 for 
;                       E and 1/E
;               IESCL   Option to allow E**2*omega+linear interpolation
;                       0, 1 for normal use, E**2*omega+linear.
;
;               All of these structure elements map onto variables of
;               the same name in the ADAS809 FORTRAN program.
;
;
;       UVALUE  - A user value for the widget. Default 0.
;
;       FONT_LARGE - The name of a larger font.  Default current system
;                    font
;
;       FONT_SMALL - The name of a smaller font. Default current system
;                    font.
;
;       EDIT_FONTS - A structure of two fonts used in the table editor
;                    adas_edtab.pro, {font_norm:'',font_input:''}.  Defaults
;                    to current system font.
;
;       NUM_FORM   - String; Numeric format to use in tables.  Default
;                       '(E10.3)'
;                    
;
; CALLS:
;       POPUP           Popup warning window with buttons.
;       CW_ADAS_DSBR    Dataset name and comments browsing button.
;       CW_ADAS_TABLE   Adas data table widget.
;       CW_ADAS_SEL     Adas multiple selection widget.
;       CW_SINGLE_SEL   Adas scrolling table and selection widget.
;       CW_OPT_VALUE    Adas option selection widget with required input 
;                       value.
;       NUM_CHK         Checks if a string represents a number.
;
;       See side effects for other related widget management routines.
;
; SIDE EFFECTS:
;
;       The following widget management routines are included in this file;
;       PROC809_GET_VAL()       Returns the current PROCVAL structure.
;       PROC809_EVENT()         Process and issue events.
;       
; CATEGORY:
;       Compound Widget
;
; WRITTEN:
;       Hugh Summers, University of Strathclyde, 30 November 2001
;
; MODIFIED:
;       1.1     Hugh Summers
;               First Release
;       1.2     Martin Torney
;               Modified code to allow averaging over non-Maxwellian
;               distributions
;       1.3     Paul Bryans
;               Removed quadrature options
;       1.4     Allan Whiteford
;               Fixed bugs in code relating to indexing single element
;               arrays.
;       1.5     Allan Whiteford
;               Changed call from UTC_IS_FLOAT to NUM_CHK
;       1.6     Martin O'Mullane
;               Remove the space in first colume before FUNCTION. 
;
; VERSION:
;       1.1     30-11-01
;       1.2     19-12-02
;       1.3     22-11-04
;       1.4     06-12-04
;       1.5     29-05-09
;       1.6     16-10-18
;
;-----------------------------------------------------------------------------

FUNCTION proc809_get_val, id

               ;**** Return to caller on error ****
               
 ON_ERROR, 2

               ;***************************************
               ;****      Retrieve the state        ****
               ;**** Get first_child widget id     ****
               ;**** because state is stored there ****
               ;***************************************
                
 first_child = widget_info(id, /child)
 widget_control, first_child, get_uvalue = state

               ;***********************************
               ;**** Get run title from widget ****
               ;**** Then centre in in string  ****
               ;**** of 40 characters          ****
               ;***********************************
               
 widget_control, state.runid, get_value=title
 title = strcompress(title(0))
 title_len = strlen(title) 
 if (title_len gt 40) then begin 
   title = strmid(title,0,37)
   widget_control, state.messid, set_value="Title too long - truncated "
   widget_control, state.runid, set_value=title
 endif
 pad = (40 - title_len)/2 
 spaces = '                                         '
 title(0) = strmid(spaces,0,(pad-1)) + title + strmid(spaces,0,(pad-1))
 
               ;*******************************************************
               ;*** Check if up->down trans. selected or vice versa ***
               ;*******************************************************
 widget_control, state.dtypeid, get_uvalue=dtype_uval
 dtype = widget_info(state.dtypeid, /droplist_select)
 dtype = dtype_uval(dtype)
 
               ;****************************************************
               ;**** Get new temperature data from table widget ****
               ;**** If losel is selected                       ****
               ;****************************************************
               
 if (state.losel eq 1) then begin
   if (dtype eq 0) then $
     index = long(state.tempid0(0)) $
       else index = long(state.tempid0(1))
   widget_control, index, get_value=tempval
 
   tabledata = tempval.value
   ifout = tempval.units+1

               ;**** Copy out temperature values ****
               
   tine = dblarr(state.ndtin)
   
   blanks = where(strtrim(tabledata(0, *), 2) eq '')

               ;**** next line assumes that all blanks are ****
               ;**** at the end of the columns             ****
               
   if (blanks(0) ge 0) then maxt=blanks(0) else maxt=state.ndtin
   tine(0:maxt-1) = double(tabledata(0, 0:maxt-1))
   
               ;**** Fill out the rest with zero ****
               
   if (maxt lt state.ndtin) then $
     tine(maxt:state.ndtin-1) = double(0.0)
 endif else begin      ; *** use existing values
   ifout = 1
   tine = state.tscef(0, *)
   maxt = state.maxt
 endelse
 
               ;***********************************
               ;*** Get distribution parameters ***
               ;***********************************
  
 CASE dtype OF
   
   0: begin
      kap_val = 0.
      dru_val = 0.
      num_val = ' '
   end
   
   1: begin
      widget_control, state.kap_val, get_value=temp
      check_float = num_chk(temp[0])
      if (check_float eq 0) then kap_val = float(temp[0])
      if (check_float eq 1) then begin
        message, 'Value must be a float.  Value = 1.6 taken', /con
        kap_val = 1.6
      endif
      if (kap_val le 1.5) then begin
        message, 'Kappa must be greater than 1.5. Value = 1.6 taken', $
                 /con
        kap_val = 1.6
      endif
      dru_val = 0.
      num_val = ' '
   end
   
   2: begin
      widget_control, state.num_val, get_value=temp
      num_val = temp[0]
      kap_val = 0.
      dru_val = 0.
   end
   
   3: begin
      widget_control, state.dru_val, get_value=temp
      check_float = num_chk(temp[0])
      if (check_float eq 0) then dru_val = float(temp[0])
      if (check_float eq 1) then begin
        message, 'Value must be a float.  Value = 2.0 taken', /con
        dru_val = 2.
      endif
;      if (kap_val le 1.5) then begin
;        message, 'Kappa must be greater than 1.5. Value = 1.6 taken', $
;                /con
;       dru_val = 1.6
;      endif
      kap_val = 0.
      num_val = ' '
   end   
   else:
      
 endCASE
 
               ;******************************
               ;*** Get quadrature options ***
               ;******************************
               
; ilinr = widget_info(state.qlist1, /droplist_select)
; ifint = widget_info(state.qlist2, /droplist_select)
; iescl = widget_info(state.qlist3, /droplist_select)
; 
;               ;*** Match indexing to fortran
;              
; ilinr = ilinr + 1
; ifint = ifint + 1
; iescl = iescl + 1
    
               ;*************************************************
               ;**** Get selection of polyfit from widget    ****
               ;*************************************************
               
 widget_control, state.optid, get_uvalue=polyset, /no_copy
 lfsel = polyset.optionset.option[0]
 
 if (num_chk(polyset.optionset.value[0]) eq 0) then begin
   tolval = (polyset.optionset.value[0])
 endif else tolval = -999
 widget_control, state.optid, set_uvalue=polyset, /no_copy

               ;***********************************************
               ;**** get new index for data block selected ****
               ;***********************************************
               
 widget_control, state.indexid, get_value=select_block
 istrn = select_block  
  
               ;***********************************************
               ;**** write selected values to PS structure ****
               ;***********************************************

    ps = {                                     $
               new      : 0,                   $
               title    : title,               $
               istrn    : istrn,               $
               ifout    : ifout,               $
               maxt     : maxt,                $
               tine     : tine,                $
               lfsel    : lfsel,               $
               tolval   : tolval,              $
               losel    : state.losel,         $
               dtype    : dtype,               $
               kap_val  : kap_val,             $
               dru_val  : dru_val,             $
               num_val  : num_val              $
;              ilinr    : ilinr,               $
;              ifint    : ifint,               $
;              iescl    : iescl                $
             }

 widget_control, first_child, set_uvalue=state, /no_copy

 RETURN, ps

END

;-----------------------------------------------------------------------------

FUNCTION proc809_event, event

               ;**** Base ID of compound widget ****
               
 parent = event.handler

               ;**********************************************
               ;**** Retrieve the user value state        ****
               ;**** Get id of first_child widget because ****
               ;**** user value "state" is stored there   ****
               ;**********************************************
               
 first_child = widget_info(parent, /child)
 widget_control, first_child, get_uvalue=state, /no_copy

               ;*********************************
               ;**** Clear previous messages ****
               ;*********************************
 
 widget_control, state.messid, set_value=' '
 
 dtype = widget_info(state.dtypeid, /droplist_select)
 widget_control, state.dtypeid, get_uvalue=dtype2
 

               ;************************
               ;**** Process events ****
               ;************************
               
 CASE event.id OF

               ;***************************
               ;**** distribution type ****
               ;***************************
               
   state.dtypeid: begin
       
       if (dtype2(event.index) eq 0) then begin

         widget_control, state.dtypeid, set_droplist_select=0
         widget_control, state.maxbase, map=1
         widget_control, state.kappa_in_base, map=0
         widget_control, state.kapbase, map=0
         widget_control, state.dru_in_base, map=0
         widget_control, state.drubase, map=0
         widget_control, state.numbase, map=0
         widget_control, state.tempid0(0),map=1
         widget_control, state.tempid0(1), map=0
;        widget_control, state.qlist2, sensitive=1
;        widget_control, state.qlist3, sensitive=1
                 
       endif
       if (dtype2(event.index) eq 1) then begin

         widget_control, state.dtypeid, set_droplist_select=1
         widget_control, state.maxbase, map=0
         widget_control, state.kappa_in_base, map=1
         widget_control, state.kapbase, map=1
         widget_control, state.dru_in_base, map=0
         widget_control, state.drubase, map=0
         widget_control, state.numbase, map=0
         widget_control, state.tempid0(0), map=0
         widget_control, state.tempid0(1), map=1
;        widget_control, state.qlist2, set_droplist_select=0, $
;                         sensitive=0
;        widget_control, state.qlist3, set_droplist_select=0, $
;                         sensitive=0
                 
       endif   
       if (dtype2(event.index) eq 2) then begin

         widget_control, state.dtypeid, set_droplist_select=3
         widget_control, state.maxbase, map=0
         widget_control, state.kappa_in_base, map=0
         widget_control, state.kapbase, map=0
         widget_control, state.dru_in_base, map=0
         widget_control, state.drubase, map=0
         widget_control, state.numbase, map=1
         widget_control, state.tempid0(0), map=0
         widget_control, state.tempid0(1), map=1
;        widget_control, state.qlist2, set_droplist_select=0, $
;                         sensitive=0
;        widget_control, state.qlist3, set_droplist_select=0, $
;                         sensitive=0
         
       endif
        
        if (dtype2(event.index) eq 3) then begin

         widget_control, state.dtypeid, set_droplist_select=2
         widget_control, state.maxbase, map=0
         widget_control, state.kappa_in_base, map=0
         widget_control, state.kapbase, map=0
         widget_control, state.dru_in_base, map=1
         widget_control, state.drubase, map=1
         widget_control, state.numbase, map=0
         widget_control, state.tempid0(0), map=0
         widget_control, state.tempid0(1), map=1
;        widget_control, state.qlist2, set_droplist_select=0, $
;                         sensitive=0
;        widget_control, state.qlist3, set_droplist_select=0, $
;                         sensitive=0
         
       endif              
   end 
   
               ;***********************
               ;*** pick adf37 file ***
               ;***********************
   
   state.num_but: begin
       
       title = 'Select adf37 file'
       user = getenv('USER')
       path = '/home/'+user+'/adas/adf37/'
       new_parent = widget_base(title=title, /column)
       name37 = dialog_pickfile(dialog_parent=new_parent, /must_exist, $
                                path=path, title=title)
       widget_control, state.num_val, set_value=name37
                                
   end

               ;*********************************
               ;*** temp. selected for output ***
               ;*********************************
               
;   state.lbut : begin
;       if (state.losel eq 0) then state.losel = 1 else $
;           state.losel = 0 
;       widget_control, state.deftid, sensitive= state.losel
;       widget_control, state.tempid0(0), sensitive = state.losel
;       widget_control, state.tempid0(1), sensitive = state.losel
;   end

               ;*************************************
               ;**** Default temperature button ****
               ;*************************************
               
   state.deftid: begin

           ;**** popup window to confirm overwriting current values ****
               
       action= popup(message='Confirm Overwrite values with Defaults', $
                       buttons=['Confirm','Cancel'], font=state.font)

       if (action eq 'Confirm') then begin
       
               ;**** Get current table widget value ****
               
         if (dtype eq 0) then $
           index = long(state.tempid0(0)) $
             else index = long(state.tempid0(1))
         widget_control, index, get_value=tempval

               ;**** Copy defaults into value structure    ****
               ;**** For default values use the existing   ****
               ;**** table values and let user modify them ****
               ;**** then set all density values to same   ****
               ;**** value                                 ****

         units = tempval.units+1
         ndtem = state.ndtem
         nv = state.nv 
         if (nv gt 0) then begin
           tempval.value(0,0:nv-1) = $
             strtrim(string(tempval.value(units,0:nv-1), $
             format=state.num_form),2)
         endif

               ;**** Fill out the rest with blanks ****
               
         if (nv lt state.ndtin) then $
           tempval.value(0,nv:state.ndtin-1) = ''

               ;**** Copy new data to table widget ****
               
         widget_control, index, set_value=tempval
       endif

     end
     
               ;**************************
               ;*** Quadrature Options ***
               ;**************************

;   state.qlist1: begin
;   
;       if (event.index eq 0) then begin
;         widget_control, state.qlist1, set_droplist_select=0
;        if dtype eq 0 then widget_control, state.qlist3, sensitive=1
;       endif
;       if (event.index eq 1) then begin
;         widget_control, state.qlist1, set_droplist_select=1
;         widget_control, state.qlist3, set_droplist_select=0, $
;                         sensitive=0
;       endif
;
;   end
;   
;   state.qlist2: begin
;   
;       if (event.index eq 0) then $
;         widget_control, state.qlist2, set_droplist_select=0
;       if (event.index eq 1) then $   
;         widget_control, state.qlist2, set_droplist_select=1
;      
;   end
;   
;   state.qlist3: begin
;   
;       if (event.index eq 0) then $
;         widget_control, state.qlist3, set_droplist_select=0
;       if (event.index eq 1) then $
;         widget_control, state.qlist3, set_droplist_select=1
;
;   end

               ;***********************
               ;**** Cancel button ****
               ;***********************
               
   state.cancelid: new_event = {ID:parent, TOP:event.top, $
                               HANDLER:0L, ACTION:'Cancel'}

               ;*********************
               ;**** Done button ****
               ;*********************
               
   state.doneid: begin
   
               ;***************************************
               ;**** Check all user input is legal ****
               ;***************************************
               
       error = 0
       
               ;********************************************
               ;*** Have to restore "state" before calls ***
               ;*** to proc809_get_val so it can be used ***
               ;*** there.                               ***
               ;********************************************
               
       widget_control, first_child, set_uvalue=state, /no_copy

       widget_control, event.handler, get_value=ps

               ;*** reset state variable ***
               
       widget_control, first_child, get_uvalue=state, /no_copy

               ;**** check temp values entered ****
               
       if (ps.losel eq 1) then begin
         if (error eq 0) and (ps.maxt eq 0) then begin
           error = 1
           message='Error: No temperatures entered.'
         endif
       endif

               ;*** Check to see if index has been selected ***
               
       if (error eq 0) and (ps.istrn lt 0) then begin
         error = 1
         message='Error: Invalid block selected'
       endif

               ;*** Check to see if sensible tolerance is selected ***
               
       if (error eq 0) and (ps.lfsel eq 1) then $
         if (float(ps.tolval) lt 0) or $
           (float(ps.tolval) gt 100) then begin
           error = 1
           message='Error: Tolerance for polyfit must be 0-100% '
         endif

               ;**** return value or flag error ****
               
       if (error eq 0) then $
         new_event = {ID:parent, TOP:event.top, HANDLER:0L, $
                     ACTION:'Done'} $
           else begin
             widget_control, state.messid, set_value=message
             new_event = 0L
           endelse

     end

               ;**** Menu button ****

     state.outid: begin
         new_event = {ID:parent, TOP:event.top, HANDLER:0L, $
                      ACTION:'Menu'}
     end

   ELSE: new_event = 0L

 ENDCASE  
    
               ;*********************************************
               ;*** make "state" available to other files ***
               ;*********************************************
               
 widget_control, first_child, set_uvalue=state, /no_copy

 RETURN, new_event
 
END

;-----------------------------------------------------------------------------

FUNCTION cw_adas809_proc, topparent, strga, dsfull, $
               ndtin, il, ndtem, nv, itran, ftype, $;icnte, $
               tscef, tscef2, i1a, i2a, act, bitfile, $
               procval=procval, uvalue=uvalue, $
               font_large=font_large, font_small=font_small, $
               edit_fonts=edit_fonts, num_form=num_form

               ;**** Set defaults for keywords ****
               
 if not (keyword_set(uvalue)) then uvalue = 0
 if not (keyword_set(font_large)) then font_large = ''
 if not (keyword_set(font_small)) then font_small = ''
 if not (keyword_set(edit_fonts)) then edit_fonts = {font_norm:'',font_input:''}
 if not (keyword_set(num_form)) then num_form = '(E10.3)'

 if not (keyword_set(procval)) then begin
    ps = {    $ 
               new     : 0,            $
               title   : '',           $
               istrn   : 0,            $
               maxt    : ndtin,        $
               ifout   : 1,            $
               tine    : tscef(1,*),   $
               lfsel   : 0,            $
               tolval  : '5',          $
               losel   : 1,            $
               dtype   : '',           $
               kap_val : 0,            $
               dru_val : 0,            $
               num_val : ' '           $
;              ilinr   : 0,            $
;              ifint   : 0,            $
;              iescl   : 0             $
             }
 endif else begin
       ps = { $ 
               new     : procval.new,     $
               title   : procval.title,   $
               istrn   : procval.istrn,   $
               maxt    : procval.maxt,    $
               ifout   : procval.ifout,   $
               tine    : procval.tine,    $
               lfsel   : procval.lfsel,   $
               tolval  : procval.tolval,  $
               losel   : procval.losel,   $
               dtype   : procval.dtype,   $
               kap_val : procval.kap_val, $
               dru_val : procval.dru_val, $
               num_val : procval.num_val  $
;              ilinr   : procval.ilinr,   $
;              ifint   : procval.ifint,   $
;              iescl   : procval.iescl    $
            }
           
            
               ;*** change fortran indexing to idl:
               
;       ps.ilinr = ps.ilinr-1
;       ps.ifint = ps.ifint-1
;       ps.iescl = ps.iescl-1
 
 endelse

       ;*********************************************************
       ;**** Modify certain parameters and results depending ****
       ;**** on the machine being used                       ****
       ;*********************************************************

 machine = GETENV('TARGET_MACHINE')
 if (machine eq 'HPUX') then begin
   y_size = 4
   large_font = font_small
 endif else begin
   y_size = 6
   large_font = font_large
 endelse

               ;****************************************************
               ;**** Assemble temperature  & density table data ****
               ;****************************************************
               ;**** The adas table widget requires data to be *****
               ;**** input as strings, so all the numeric data *****
               ;**** has to be written into a string array.    *****
               ;**** Declare temp. & dens.table array          *****
               ;**** col 1 has user temperature values         *****
               ;**** col 2-4 havetemperature values from files,*****
               ;**** which can have one of three possible units*****
               ;****************************************************
               
 maxt = ps.maxt
 
               ;**********************************
               ;*** Create 3 types of dataset: ***
               ;*** adf04 comparison file,     ***
               ;*** adf37 comparison file,     ***
               ;*** no comparison file         ***
               ;**********************************
 
 tabledata0 = strarr(4,ndtin) ; for adf04 type 1 files
 tabledata1 = strarr(7,ndtin) ; for adf37 files
 tabledata2 = strarr(4,ndtin) ; for no comparison file
 tabledata3 = strarr(4,ndtin) ; for adf04 type 4 files
 colhead0 = strarr(2,2,2)
 colhead1 = strarr(2,3,2)       
 colhead2 = strarr(2,2,2)    ;
 colhead3 = strarr(2,2,2)    ; These arrays allow a selection of 
 coledit0 = intarr(2)        ; table widgets to be created.  Depending
 coledit1 = intarr(3)        ; on which comparison file and distribution
 coledit2 = intarr(2)        ; function are selected, one of the tables
 coledit3 = intarr(2)        ; is displayed on screen
 order0 = intarr(2)          ;
 order1 = intarr(3)             
 order2 = intarr(2)             
 order3 = intarr(2)             
 limits0 = intarr(2)+1          
 limimts1 = intarr(3)+1         
 limits2 = intarr(2)+1          
 limits3 = intarr(2)+1          
 unitind0 = intarr(3,2)         
 unitind1 = intarr(3,3)         
 unitind2 = intarr(3,2)         
 unitind3 = intarr(3,2)         
 
 defaultk = [1.16e4, 2.32e4, 5.80e4, 1.16e5, 2.32e5, 5.80e5, 1.16e6, $
             2.32e6, 5.80e6, 1.16e7, 2.32e7, 5.80e7, 1.16e8, 2.32e8]
 defaultev = defaultk/1.16e4
 defaultred = defaultk
 
               ;**** Copy out temperature values ****
               ;**** number of temperature       ****
               ;**** values for this data block  ****

 ftype = strtrim(ftype,2)
 
 if (ftype eq 'adf043') then ftype = 'adf04'
 
 if (maxt gt 0) then begin
   case ftype of
     'adf04': begin
          tabledata0(0,0:maxt-1) = $
            strtrim(string(ps.tine(0:maxt-1),format=num_form),2)
          tabledata0(1,0:nv-1 ) = $
            strtrim(string(tscef(0,0:nv-1),format=num_form),2)
          tabledata0(2,0:nv-1) = $
            strtrim(string(tscef(1,0:nv-1),format=num_form),2)
          tabledata0(3,0:nv-1) = $
            strtrim(string(tscef(2,0:nv-1),format=num_form),2)
          colhead0(0,*,*) = [['Output Temp', 'Input Temp'], ['','']]
          colhead0(1,*,*) = [['Output Scan Param', 'Input Temp'],['','']]
          coledit0 = [1,0]
          order0 = [0,0]
          limits0 = [1,1]
          unitind0 = [[0,0,0],[1,2,3]]
      end
      'adf37': begin
          tabledata1(0,0:maxt-1) = $
            strtrim(string(ps.tine(0:maxt-1),format=num_form),2)
          tabledata1(1,0:maxt-1) = $
            strtrim(string(tscef(0,0:nv-1),format=num_form),2)
          tabledata1(2,0:maxt-1) = $
            strtrim(string(tscef(1,0:nv-1),format=num_form),2)
          tabledata1(3,0:maxt-1) = $
            strtrim(string(tscef(2,0:nv-1),format=num_form),2)
          tabledata1(4,0:maxt-1) = $
            strtrim(string(tscef2(0,0:nv-1),format=num_form),2)
          tabledata1(5,0:maxt-1) = $
            strtrim(string(tscef2(1,0:nv-1),format=num_form),2)
          tabledata1(6,0:maxt-1) = $
            strtrim(string(tscef2(2,0:nv-1),format=num_form),2)
          colhead1(0,*,*) = [['Output Temp','2/3 Input Av. E', $
                       '2 Input Mode E'], ['','','']]
          colhead1(1,*,*) = [['Output Scan Param','2/3 Input Av. E', $
                       '2 Input Mode E'], ['','','']]
          coledit1 = [1,0,0]
          order1 = [0,0,0]
          limits1 = [1,1,1]
          unitind1 = [[0,0,0],[1,2,3],[4,5,6]]
      end
      '': begin
        tabledata2(0,0:maxt-1) = $
           strtrim(string(ps.tine(0:maxt-1),format=num_form),2)
        tabledata2(1,0:maxt-1) = $
           strtrim(string(defaultk(0:maxt-1),format=num_form),2)   
        tabledata2(2,0:maxt-1) = $
           strtrim(string(defaultev(0:maxt-1),format=num_form),2)   
        tabledata2(3,0:maxt-1) = $
           strtrim(string(defaultred(0:maxt-1),format=num_form),2)   
        colhead2(0,*,*) = [['Output Temp','Default Temp'],['','']]
        colhead2(1,*,*) = [['Output Scan Param','Default Temp'],['','']]
        coledit2 = [1,0]
        order2 = [0,0]
        limits2 = [1,1]
        unitind2 = [[0,0,0],[1,2,3]];,[0,0,0]]
      end
      'adf044': begin
          tabledata3(0,0:maxt-1) = $
            strtrim(string(ps.tine(0:maxt-1),format=num_form),2)
          tabledata3(1,0:nv-1 ) = $
            strtrim(string(tscef(0,0:nv-1),format=num_form),2)
          tabledata3(2,0:nv-1) = $
            strtrim(string(tscef(1,0:nv-1),format=num_form),2)
          tabledata3(3,0:nv-1) = $
            strtrim(string(tscef(2,0:nv-1),format=num_form),2)
          colhead3(0,*,*) = [['Output Temp', 'Input Scan Param'], $
                             ['','']]
          colhead3(1,*,*) = [['Output Scan Param', $
                              'Input Scan Param'],['','']]
          coledit3 = [1,0]
          order3 = [0,0]
          limits3 = [1,1]
          unitind3 = [[0,0,0],[1,2,3]]
      end
      else:
   endcase
   
               ;**** fill rest of table with blanks ****
               
   blanks = where(tabledata0 eq 0.0) 
   tabledata0(blanks) = ' '
   blanks = where(tabledata1 eq 0.0)
   tabledata1(blanks) = ' '
   blanks = where(tabledata2 eq 0.0)
   tabledata2(blanks) = ' ' 
   blanks = where(tabledata3 eq 0.0)
   tabledata3(blanks) = ' ' 
 end

               ;********************************************************
               ;**** Create the 809 Processing options/input window ****
               ;********************************************************

               ;**** create titled base widget ****
               
 parent = widget_base(topparent, UVALUE = uvalue, $
                      title = 'ADAS809 PROCESSING OPTIONS', $
                      EVENT_FUNC = "proc809_event", $
                      FUNC_GET_VALUE = "proc809_get_val", $
                      /COLUMN)
 
               ;******************************************************
               ;**** Create a dummy widget just to hold value of *****
               ;**** "state" variable so as not to get confused  *****
               ;**** with any other values. Adopt IDL practice   *****
               ;**** of using first child widget                 *****
               ;******************************************************
               
 first_child = widget_base(parent);, /align_center)

 topbase = widget_base(first_child, /column);, /align_center)

               ;***********************
               ;**** add run title ****
               ;***********************
               
 base = widget_base(topbase, /row, /align_center)
 rc = widget_label(base, value='Title for Run', font=large_font)
 runid = widget_text(base, value=ps.title, xsize=38 ,font=large_font, $
                    /edit)

               ;**** add dataset name and browse button ****
               
 rc = cw_adas_dsbr(topbase, dsfull, font=large_font)

               ;**** add information from data file about data set ****
               
 file_info = 'Number of Electron transitions : ' + $
                       strtrim(strcompress(string(itran))) + $
                       '   Number of energy levels : ' + $
                       strtrim(strcompress(string(il)))
 rc = widget_label(topbase, value=$
                   'Number of Electron Impact Transitions : ' + $
                   strtrim(strcompress(string(itran)))  , $
                   font=large_font)

 rc = widget_label(topbase, value=$
                   'Number of Index Energy levels : ' + $
                   strtrim(strcompress(string(il))) , $
                   font=large_font)

               ;***************************************************
               ;**** add a window to display and select index   ***
               ;**** first create data array for table          ***
               ;**** Then convert to 1D string array(text_table)***
               ;**** call cw_single_sel.pro for options choice  ***
               ;***************************************************

 block_info = strarr(3, itran)
 for i = 0, itran-1 do begin
    block_info(0,i) = strcompress(string(i+1))
 endfor

  for i = 0, itran-1 do begin
      block_info(1,i) = strcompress(i1a(i)) 
      if (i1a(i) lt 10) then  begin 
         block_info(1,i) = block_info(1,i) + '  ' +strga(i1a(i)-1)
      endif else $
         block_info(1,i) = block_info(1,i) + ' '  +strga(i1a(i)-1)

      block_info(2,i) = strcompress(i2a(i)) 
      if (i2a(i) lt 10) then begin
         block_info(2,i) = block_info(2,i) + '  ' +strga(i2a(i)-1)
      endif else  $
         block_info(2,i) = block_info(2,i) + ' ' +strga(i2a(i)-1)
  endfor
     
 titles = [['TRANSITION','  ----- LOWER LEVEL ----- ', $
                         '  ----- UPPER LEVEL ----- '], $
          [ '  INDEX   ','    INDEX  DESIGNATION    ', $
                         '    INDEX  DESIGNATION    ']]

 select_data = text_table(block_info, colhead=titles, /noindex)
 coltitles = select_data(0:1)
 select_data = select_data(2:itran+1)
 if (ps.istrn gt itran) then select_index = 0 $
   else select_index = ps.istrn
 
 transbase = widget_base(topbase, /row)
 indexid = cw_single_sel( transbase, select_data, value=select_index,  $
                          title='Select Specific Electron Impact'+     $
                                       ' Transition',                  $
                          coltitles = coltitles,                       $
                          ysize = y_size-1, font=font_small,           $
                          big_font=large_font )
                          
               ;**********************
               ;**** Another base ****
               ;**********************
               
 tablebase = widget_base(parent, /row)

               ;************************************************
               ;**** base for the table and defaults button ****
               ;**** and whether table used for text output ****
               ;************************************************
               
 base = widget_base(tablebase, /column, /frame)

               ;*************************************
               ;**** distribution selection list ****
               ;*************************************
               
 dtypebase = widget_base(base, /row, /align_center)
 dtypeid = widget_droplist(dtypebase, /dynamic_resize, /sensitive, $
                           value=['Maxwellian', 'Kappa', 'Druyvestyn', $
                                  'Numeric'], $
                           uvalue=[0,1,3,2], font=large_font)
 
 widget_control, dtypeid, get_uval=dtype_uval
 widget_control, dtypeid, set_droplist_select=dtype_uval(ps.dtype) 
 dtypeselect = widget_info(dtypeid,/droplist_select)
 dtypeselect = dtype_uval(dtypeselect)

               ;*******************************************************
               ;*** create a new base for each type of distribution ***
               ;*******************************************************
               
 dummybase = widget_base(base)

 parambase = widget_base(dtypebase)

 maxbase = widget_base(parambase)
 kapbase = widget_base(parambase)
 drubase = widget_base(parambase)
 numbase = widget_base(parambase)

 maxbase2 = widget_base(dummybase, /column)
 kapbase2 = widget_base(dummybase, /column)
 drubase2 = widget_base(dummybase, /column)
 numbase2 = widget_base(dummybase, /column)
 commonbase = widget_base(dummybase, /column)
 commonbase1 = widget_base(commonbase, /column, /align_right)

 widget_control, maxbase, map=0
 widget_control, kapbase, map=0
 widget_control, drubase, map=0
 widget_control, numbase, map=0

 dummybase2 = widget_base(dtypebase)

           ;***************************************
           ;*** Create bases that are exclusive ***
           ;*** to each distribution type       ***
           ;***************************************

               ;***********************
           ;******** maxwellian base ******
               ;***********************
         
      ; no bases exclusive to maxwellians

               ;******************
           ;******* kappa base *******
               ;******************
               
 kappa_in_base = widget_base(kapbase, /row)
 kappa_lab = widget_label(kappa_in_base, value='Kappa', font=large_font)
 kap_val = widget_text(kappa_in_base, value=string(ps.kap_val), $
                      /editable)
 widget_control, kappa_in_base, map=0
 
               ;************************
           ;*******  druyvestyn base *******
               ;************************
 
 dru_in_base = widget_base(drubase, /row)
 dru_lab = widget_label(dru_in_base, value='x  ', font=large_font)
 dru_val = widget_text(dru_in_base, value=string(ps.dru_val), $
                      /editable)
 widget_control, dru_in_base, map=0
 
               ;**********************
           ;******* numerical base ******
               ;**********************
               
 num_in_base = widget_base(numbase, /column)
 num_lab = widget_label(num_in_base, value='Distribution Function:', $
                       font=large_font, /align_left)
 num_in_base1 = widget_base(num_in_base, /row)
 num_val = widget_text(num_in_base1, value=ps.num_val, xsize=40, $
                       /editable, /align_left)
 num_but = widget_button(num_in_base1, value='Browse', font=large_font)
 
               ;********************************
               ;*** create appropriate bases ***
               ;********************************
               
 case dtypeselect of
 
   0: begin
        widget_control, maxbase, map=1
        widget_control, kappa_in_base, map=0
        widget_control, kapbase, map=0
        widget_control, kapbase2, map=0
        widget_control, dru_in_base, map=0
        widget_control, drubase, map=0
        widget_control, numbase, map=0
      end
   1: begin
        widget_control, maxbase, map=0
        widget_control, kappa_in_base, map=1
        widget_control, kapbase, map=1
        widget_control, kapbase2, map=1
        widget_control, dru_in_base, map=0
        widget_control, drubase, map=0
        widget_control, numbase, map=0
      end
   2: begin
        widget_control, maxbase, map=0
        widget_control, kappa_in_base, map=0
        widget_control, kapbase, map=0
        widget_control, kapbase2, map=0
        widget_control, drubase, map=0
        widget_control, dru_in_base, map=0
        widget_control, numbase, map=1
        widget_control, numbase2, map=1
     end
   3: begin
        widget_control, maxbase, map=0
        widget_control, kappa_in_base, map=0
        widget_control, kapbase, map=0
        widget_control, kapbase2, map=0
        widget_control, dru_in_base, map=1
        widget_control, drubase, map=1
        widget_control, numbase, map=0
      end      

   else: begin
   end
 endcase
 
               ;**********************************************
               ;*** Items in a common (to all dtypes) base ***
               ;**********************************************
               
               ;***************************************
               ;*** "LOSEL" parameter switch **********
               ;***************************************
               
; lbase = widget_base(commonbase1, /row, /align_center)
; llabel = widget_label(lbase, $
;             value="Select Temperatures for output file", $
;                    font=large_font)
; lbbase = widget_base(lbase, /row, /nonexclusive)
; lbut = widget_button(lbbase, value=' ')
; widget_control, lbut, set_button=ps.losel

               ;********************************
               ;**** temperature data table ****
               ;********************************
               
               ;********************************************
               ;**** convert FORTRAN index to IDL index ****
               ;********************************************
               
 units = ps.ifout-1 
 unitname = ['Kelvin','eV','Reduced']
 
        ;**** Two  columns in the table and three sets of units. ****
        ;**** Column 1 has the same values for all three         ****
        ;**** units but column 2 switches between sets 2,3 & 4   ****
        ;**** in the input array tabledata as the units change.  ****
               
 unitind = [[0,0,0],[1,2,3],[0,0,0]]
 table_title =  ["Output Electron Properties "] 

               ;****************************
               ;**** table of data   *******
               ;****************************
  
 edtablebase = widget_base(commonbase1)
 
 tempid0 = intarr(2)
 tempid0a = intarr(2)
 tempid0b = intarr(2)
 tempid0c = intarr(2)
 tempid0d = intarr(2)
 
 for i = 0, 1 do begin
   
   colhead = reform(colhead0(i,*,*))
   
   tempid0a(i) = cw_adas_table(edtablebase, tabledata0, units,         $
                              unitname, unitind0,                      $
                              UNITSTITLE = 'Temperature Units',        $
                              COLEDIT=coledit0, COLHEAD=colhead,       $
                              TITLE=table_title, ORDER=order0,         $
                              LIMITS=limits0, CELLSIZE=12,             $
                              /SCROLL, YTABSIZE=7, NUM_FORM=num_form,  $
                              FONTS=edit_fonts, FONT_LARGE=large_font, $
                              FONT_SMALL=font_small, YTEXSIZE=y_size)
                         
   widget_control, tempid0a(i), map=0
   
   colhead = reform(colhead1(i,*,*))    
   
   tempid0b(i) = cw_adas_table(edtablebase, tabledata1, units,         $
                              unitname, unitind1,                      $
                              UNITSTITLE = 'Temperature Units',        $
                              COLEDIT=coledit1, COLHEAD=colhead,       $
                              TITLE=table_title, ORDER=order1,         $
                              LIMITS=lilits1, CELLSIZE=12,             $
                              /SCROLL, YTABSIZE=7, NUM_FORM=num_form,  $
                              FONTS=edit_fonts, FONT_LARGE=large_font, $
                              FONT_SMALL=font_small, YTEXSIZE=y_size)
                         
   widget_control, tempid0b(i), map=0           
   
   colhead = reform(colhead2(i,*,*))    
   
   tempid0c(i)= cw_adas_table(edtablebase, tabledata2, units,          $
                             unitname, unitind2,                       $
                             UNITSTITLE = 'Temperature Units',         $
                             COLEDIT=coledit2, COLHEAD=colhead,        $
                             TITLE=table_title, ORDER=order2,          $
                             LIMITS=limits2, CELLSIZE=12,              $
                             /SCROLL, YTABSIZE=7, NUM_FORM=num_form,   $
                             FONTS=edit_fonts, FONT_LARGE=large_font,  $
                             FONT_SMALL=font_small, YTEXSIZE=y_size)            
 
   widget_control, tempid0c(i), map=0 
   
   colhead = reform(colhead3(i,*,*))

   tempid0d(i)= cw_adas_table(edtablebase, tabledata3, units,          $
                             unitname, unitind3,                       $
                             UNITSTITLE = 'Temperature Units',         $
                             COLEDIT=coledit3, COLHEAD=colhead,        $
                             TITLE=table_title, ORDER=order3,          $
                             LIMITS=limits3, CELLSIZE=12,              $
                             /SCROLL, YTABSIZE=7, NUM_FORM=num_form,   $
                             FONTS=edit_fonts, FONT_LARGE=large_font,  $
                             FONT_SMALL=font_small, YTEXSIZE=y_size)            
 
   widget_control, tempid0d(i), map=0 
 
 endfor
 
 case ftype of
   'adf04': tempid0 = tempid0a
   'adf37': tempid0 = tempid0b
   '':      tempid0 = tempid0c
   'adf044': tempid0 = tempid0d
   else:
 endcase
 
 if (dtypeselect eq 0) then begin
   widget_control, tempid0(0), map=1
 endif else widget_control, tempid0(1), map=1
                         
               ;**************************************
               ;**** Default temperatures button  ****
               ;**************************************
               
 deftid = widget_button(commonbase, $
          value=' Default Temperature Values  ', font=large_font)

               ;*** Make table non/sensitive as ps.losel ***
               
 for i = 0, 1 do begin
   widget_control, tempid0(i), sensitive = ps.losel
   widget_control, tempid0(i), sensitive = ps.losel
   widget_control, tempid0(i), sensitive = ps.losel
   widget_control, deftid, sensitive = ps.losel
 endfor  
 
               ;**************************
               ;*** Quadrature Options ***
               ;**************************
 
; quadbase = widget_base(tablebase, /column, /frame)
;                         
; qtitle = widget_label(quadbase, value='Quadrature Options:', $
;                       ysize=50, font=large_font)
; quadbase2 = widget_base(quadbase, /column, /frame)
;
; qlab1 = widget_label(quadbase2, value='Coll.Strth.Intp:', ysize=50, $
;                     font=large_font)
; qlist1 = widget_droplist(quadbase2, /dynamic_resize, /sensitive, $
;                         value=['Linear','Quadratic'], uvalue=[0,1], $
;                         font=large_font)
; widget_control, qlist1, set_droplist_select=ps.ilinr
; q1_select = widget_info(qlist1, /droplist_select)
; 
; qlab2 = widget_label(quadbase2, value='Coll.Strth.Var:', ysize=50, $
;                     font=large_font)
; qlist2 = widget_droplist(quadbase2, /dynamic_resize, /sensitive, $
;                         value=['E','1/E'], uvalue=[0,1], $
;                         font=large_font)
; widget_control, qlist2, set_droplist_select=ps.ifint
; q2_select = widget_info(qlist2, /droplist_select)
;  
; qlab3 = widget_label(quadbase2, value='Coll.Strth.Scaling:', $
;                      ysize=50, font=large_font)
; qlist3 = widget_droplist(quadbase2, /dynamic_resize, $
;                          sensitive=1-q1_select, $
;                          value=['Normal','E^2*Om + Lin'], $
;                          uvalue=[0,1], font=large_font)
; widget_control, qlist3, set_droplist_select=ps.iescl
; q3_select = widget_info(qlist3, /droplist_select)
; 
; case dtypeselect of
;   0: begin
;      widget_control, qlist2, set_droplist_select=ps.ifint, sensitive=1
;      widget_control, qlist3, set_droplist_select=ps.iescl, sensitive=1
;   end
;   1: begin
;      widget_control, qlist2, set_droplist_select=0, sensitive=0
;      widget_control, qlist3, set_droplist_select=0, sensitive=0
;   end
;   2: begin
;      widget_control, qlist2, set_droplist_select=0, sensitive=0
;      widget_control, qlist3, set_droplist_select=0, sensitive=0
;   end
;   3: begin
;      widget_control, qlist2, set_droplist_select=0, sensitive=0
;      widget_control, qlist3, set_droplist_select=0, sensitive=0
;   end 
; endcase
 
               ;**************************************
               ;**** Add polynomial fit selection ****
               ;**************************************
               
 polyset = { option:intarr(1), val:strarr(1)}  ; defined thus because 
                                               ; cw_opt_value.pro
                                               ; expects arrays
 polyset.option = [ps.lfsel]
 polyset.val = [ps.tolval]
 options = ['Fit Polynomial']
 optbase = widget_base(topbase,/frame)
 optid   = cw_opt_value(optbase, options,             $
                        title='Polynomial Fitting',   $
                        limits = [0,100],             $
                        value=polyset, font=large_font)

               ;**** Error message ****
               
 messid = widget_label(parent, font=large_font, value= $
           'Edit the processing options data and press Done to proceed')

               ;**** add the exit buttons ****
               
 base = widget_base(parent, /row)
 menufile = bitfile + '/menu.bmp'
 read_X11_bitmap, menufile, bitmap1
 outid = widget_button(base, value=bitmap1)          ;menu button
 cancelid = widget_button(base, value='Cancel', font=large_font)
 doneid = widget_button(base, value='Done', font=large_font)
 
               ;**** create a state structure for the pop-up ****
               ;****                window.                  ****

 new_state = { runid         : runid,          $
               messid        : messid,         $
               outid         : outid,          $
               deftid        : deftid,         $
               tempid0       : tempid0,        $
               losel         : ps.losel,       $
               ftype         : ftype,          $
               dtypeid       : dtypeid,        $
               kap_val       : kap_val,        $
               dru_val       : dru_val,        $
               maxbase       : maxbase,        $
               kappa_in_base : kappa_in_base,  $
               kapbase       : kapbase,        $
               dru_in_base   : dru_in_base,    $
               drubase       : drubase,        $
               numbase       : numbase,        $
               num_val       : num_val,        $
               num_but       : num_but,        $
;               qlist1       : qlist1,         $
;               qlist2       : qlist2,         $
;               qlist3       : qlist3,         $
;              lbut          : lbut,           $
               optid         : optid,          $
               indexid       : indexid,        $
               cancelid      : cancelid,       $
               doneid        : doneid,         $
               dsfull        : dsfull,         $
               ndtin         : ndtin,          $
               ndtem         : ndtem,          $
               nv            : nv,             $
               maxt          : maxt,           $
               tscef         : tscef,          $
               font          : font_large,     $
               num_form      : num_form,       $
               ps            : ps              $
             }

                ;**** Save initial state structure ****
                
 widget_control, first_child, set_uvalue=new_state,/no_copy

 RETURN, parent

END
