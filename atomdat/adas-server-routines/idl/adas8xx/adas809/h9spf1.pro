;
; PROJECT:
;       ADAS IBM MVS to UNIX conversion
;
; NAME:
;	H9SPF1
;
; PURPOSE:
;	IDL user interface and communications with ADAS809 FORTRAN
;	process via pipe.
;
; EXPLANATION:
;	First this routine reads some information from ADAS809 FORTRAN
;	via the UNIX pipe.  Next the 'Output Options' part of the
;	interface is invoked.  Finally the results of the user's
;	interactions are written back to the FORTRAN via the pipe.
;	Communications are with the FORTRAN subroutine B1SPF1.
;
; USE:
;	The use of this routine is specific to ADAS809, See adas809.pro.
;
; INPUTS:
;	PIPE	- The IDL unit number of the bi-directional pipe to the
;		  ADAS809 FORTRAN process.
;
;	DSFULL  - The name of the data set being analyzed.
;
;	VALUE	- A structure which determines the initial settings of
;		  the dataset selection widget.  The initial value is
;		  set in adas809.pro.  VALUE is passed un-modified
;		  through to cw_adas_in.pro, see that routine for a full
;		  description.
;
;       BITFILE - String; the path to the dirctory containing bitmaps
;                 for the 'escape to series menu' button.
;
; OPTIONAL INPUTS:
;	None.
;
; OUTPUTS:
;	LPEND	- Integer; indicates user action on leaving output
;		  selections.  lpend is set to 1 if the user exited
;		  with the 'Cancel' button and 0 if the user exited
;		  with 'Done'.
;
;	VALUE	- On output the structure records the final settings of
;		  the output selection widget if the user pressed the
;		  'Done' button otherwise it is not changed from input.
;
;       GOMENU  - Int; flag - set to 1 if user has selected 'escape direct
;                 to series menu , otherwise 0
;
; OPTIONAL OUTPUTS:
;	None.
;
; KEYWORD PARAMETERS:
;	DEVLIST	- A string array;  Each element of the array is the
;		  name of a hardcopy graphics output device.  This
;		  list must mirror the DEVCODE list.  DEVLIST is the
;		  list which the user will see.
;		  e.g ['Post-Script','HP-PCL','HP-GL']
;
;	FONT	- Supplies the font to be used for the interface widgets.
;
; CALLS:
;	ADAS809_OUT	User interface - output options.
;
; SIDE EFFECTS:
;	This routine communicates with the ADAS809 FORTRAN process
;	via a UNIX pipe.
;
; CATEGORY:
;	Adas system.
;
; WRITTEN:
;       Hugh Summers, University of Strathclyde, 30 September 2001 
;
; MODIFIED:
;	1.1	Hugh Summers
;		First Release
;
; VERSION:
;	1.1	30-11-01
;
;
;-----------------------------------------------------------------------------

PRO H9SPF1, pipe, lpend, value, dsfull, bitfile, gomenu,	$
		DEVLIST=devlist, FONT=font


                ;**** Set defaults for keywords ****
  IF NOT (KEYWORD_SET(font)) THEN font = ''

		;********************************
		;**** Read data from fortran ****
		;********************************

		;**************************************
		;**** Pop-up output options widget ****
		;**************************************
		
  adas809_out, value, dsfull, action,  bitfile, $
                DEVLIST=devlist, FONT=font

		;***********************************************
		;**** Act on the output from the widget     ****
		;**** There are only three possible actions ****
		;**** 'Done', 'Cancel' and 'Menu'.          ****
		;***********************************************
		
  gomenu = 0
  if action eq 'Done' then begin
    lpend = 0
  endif else if action eq 'Menu' then begin
    lpend = 1
    gomenu = 1
  endif else begin
    lpend = 1
  end

		;*******************************
		;**** Write data to fortran ****
		;*******************************
		
  printf,pipe,lpend

  if lpend eq 0 then begin

     printf, pipe, value.grpout
     if (value.grpout eq 1) then begin
	printf, pipe, value.grpscal
	if (value.grpscal eq 1) then begin
	    printf,pipe, value.xmin
	    printf,pipe, value.xmax
	    printf,pipe, value.ymin
	    printf,pipe, value.ymax
        endif
     endif

		;*** If text output requested tell FORTRAN ****
     
     printf, pipe, value.texout
     if (value.texout eq 1) then begin
         printf, pipe, value.texrep
         printf, pipe, value.texdsn
     endif
     printf, pipe, value.adf04out
     if (value.adf04out eq 1) then begin
         printf, pipe, value.adf04rep
	 printf, pipe, value.adf04dsn
     endif
  endif

		;**************************************************
		;**** Set messages and settings ready for when ****
		;**** output window is re-displayed.           ****
		;**************************************************

  if lpend eq 0 then begin
    if value.texout eq 1 then begin
    
                ;***************************************************
		;**** If text output is requested enable append ****
		;**** for next time and update the default to   ****
		;**** the current text file.                    ****
                ;***************************************************
                
      value.texapp = 0
      value.texdef = value.texdsn
      if value.texrep ge 0 then value.texrep = 0
      value.texmes = 'Output written to file.'
    end

    if value.hrdout eq 1 then begin
    end
  end

END
