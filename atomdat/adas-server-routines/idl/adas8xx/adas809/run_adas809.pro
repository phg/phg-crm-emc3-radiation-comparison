;-----------------------------------------------------------------------
;+
; PROJECT:
;      ADAS
;
; NAME:
;      RUN_ADAS809
;
; PURPOSE:
;      Runs ADAS809 adf04 conversion as an IDL subroutine
;
; INPUTS:
;      FILEIN  : String containing the name of the input file.  
;                The file must exist and be readable, and 
;                be an adf04 type 1 or an adf37
;
; OPTIONAL INPUTS:
;      DTYPE   : Integer, indicates the distribution type.
;                  0 => Maxwellian (Default) 
;                       (DPARAM and NUMFILE not required)
;                  1 => Kappa 
;                       (DPARAM required, NUMFILE not required)
;                  2 => Numerical
;                       (NUMFILE required, DPARAM not required)
;                  3 => Druyvestian 
;                       (DPARAM required, NUMFILE not required)
;                  
;      DPARAM  : The appropriate characteristic value for the selected
;                distribution
;      NUMFILE : String containing the adf37 file if a numerical
;                distribution is selected
;      TE      : An array of temeratures.  The default is the
;                Z-scaled standard ADAS grid
;      LOG     : String containing the name of the output log file.
;                If this is not specified, not log file will be created.
;      FILEOUT : String containing the name of the output adf04 type 3 file.
;                The default will be FILEINprefix + "_type3" + FILEINsuffix
;      EV      : Specifies the temperatures given in TE are in ev.
;      REDUCED : Specifies the temperatures given in TE are in reduced units.
;      HELP    : Displays help entry
;      
; OUTPUTS:
;      None
;
; OPTIONAL OUTPUTS:
;      None
;
; KEYWORD PARAMETERS:
;      None
;
; CALLS:
;      RUN_ADAS809_CALC
;     
; SIDE EFFECTS:
;      
;
; CATEGORY:
;      
;
; WRITTEN:
;      
;
; MODIFIED:
;      1.0      Martin Torney
;                 First Version
;      1.1      Paul Bryans
;                 Reads string from fortran to wait until fortran has
;                 completed its calculations
;      1.2      Allan Whiteford
;                 Removed teunit keyword and replaced the functionality
;                 with /ev or /reduced. Added /help keyword.
;
; VERSION:
;      1.1      
;      1.2      07/04/04
;-
;----------------------------------------------------------------------

 pro run_adas809_calc,	dsfull1, file_type1, $
 			dtype, dparam, $
			numfile, te, paper, adf044, ifout
                        
     ;*** adas809.pro
 
 fortdir = getenv('ADASFORT')
 spawn, fortdir+'/adas809.out', unit=pipe, /noshell, pid=pid
 date = xxdate() 
 printf, pipe, date(0) ; pipe date to xxdate.for
 
     ;*** h9spf0.pro
 
 rep = 'NO'
 dsfull2 = ''
 file_type2 = ''
 printf, pipe, rep    ; tells fortran to continue ('YES' to terminate)
 printf, pipe, dsfull1    ; the type 1 filename
 printf, pipe, dsfull2    ; the comparison filename
 printf, pipe, file_type1    ; the type 1 file type
 printf, pipe, file_type2    ; the comparison file type
  
     ;*** bxsetp.pro
 
 sz0 = ''
 sz = ''
 scnte = ''
 sil = ''
 lfpool = 0
 input = ''
 
 readf, pipe, input    ; nuclear charge
 sz0 = input
 readf, pipe, input    ; recombined ion charge
 sz = input
 readf, pipe, input    ; number of electron impact transitions
 scnte = input
 readf, pipe, input    ; number of energy levels
 sil = input

 readf, pipe, input    ; number of levels for string array
 lfpool = input
 strga = strarr(lfpool)

 for l = 0, lfpool-1 do begin
   readf, pipe, input   ; character string associated with level
   strga(l) = input     ; eg ' 1s2   (1)S( 0.0)'
 endfor
 
     ;*** h9ispf.pro
 
 lpend = 0 ; 'Done'
 ndtin = 0
 ndtem = 0
 il = 0
 nv = 0
 itran = 0
 input = 0
 ftype = ' '
 
 readf, pipe, input    ; maximum number of distributions allowed in 
 ndtin = input         ; adf37
 readf, pipe, input    ; number of energy levels
 il = input
 readf, pipe, input    ; maximum number of temperatures allowed in
 ndtem = input         ; output file
 readf, pipe, input    ; number of temperatures to be read
 nv = input
 readf, pipe, input    ; number of electron transitions
 itran = input      
 readf, pipe, ftype    ; type of file selected as comparison
 
 tscef = dblarr(3,ndtem)
 tscef2 = dblarr(3,ndtem)
 i1a = intarr(itran)
 i2a = intarr(itran) 
     
 temp = 0.0D
 itemp = 0  
 
 for l = 0, 2 do for m = 0, ndtem-1 do begin
   readf, pipe, temp    ; (effective) temperature arrays, this one is for
   tscef(l,m) = temp    ; 2/3 the average electron energy
 endfor
 
 for l = 0, 2 do for m = 0, ndtem-1 do begin
   readf, pipe, temp     ; (effective) temperature array, this one is for
   tscef2(l,m) = temp    ; the most common electron energy
 endfor    

 for l = 0, itran-1 do begin
   readf, pipe, itemp    ; lower index for electron impact transition
   i1a(l)  = itemp
 endfor
 
 for l = 0, itran-1 do begin
   readf, pipe, itemp    ; upper index for electron impact transition
   i2a(l) = itemp
 endfor  
 
 printf, pipe, 0     ; continue
 
 printf, pipe, ''    ; title for run
 printf, pipe, 0     ; selected transition
 printf, pipe, ifout ; selects the units of the temperatures for adf04
     
     ; If no temperature array given then create a temperature grid. 
 
 if n_elements(te) eq 0 then begin
   te = [ 1.00d2, 2.00d2, 5.00d2, 1.00d3, 2.00d3, 5.00d3, $
          1.00d4, 2.00d4, 5.00d4, 1.00d5, 2.00d5, 5.00d5, $
          1.00d6, 2.00d6 ]
   te = te*(sz+1d)
 endif 
 
 printf, pipe, n_elements(te)    ; the number of temperatures
  
 for l = 0, n_elements(te)-1 do $
   printf, pipe, te(l)    ; pipe the temperatures
 
 printf, pipe, 0    ; do not carry out polynomial fitting
 printf, pipe, 1    ; calculate interpolated values
 printf, pipe, dtype    ; pipe the distribution type
 
 case dtype of
   1: printf, pipe, dparam
   2: printf, pipe, numfile, format='(A)'
   3: printf, pipe, dparam
   else:
 endcase
 
 flush, pipe
 
     ;*** adas809.pro
 
 printf, pipe, 0, format='(I1)'    ; tells fortran to continue
 
 adasrel = ' '
 adasprog = ' PROGRAM: ADAS809 V1.0'
 
 header = adasrel+adasprog+' DATE: '+date(0)+' TIME: '+date(1)
 
     ;*** h9spf1.pro
 
 if paper eq '' then text = 0 else text = 1
 printf, pipe, 0    ; tells fortran that output options are activated
 printf, pipe, 0    ; graphical output
 printf, pipe, text    ; text file output
 if text then begin
   printf, pipe, 1    ; replace file
   printf, pipe, paper    ; name of output file
 endif

 printf, pipe, 1    ; create adf04
 printf, pipe, 1    ; replace file
 printf, pipe, adf044    ; name of adf04 file
 
     ;*** adas809
 
 printf, pipe, 0    ; tells fortran to continue
 if text then printf, pipe, header

 printf, pipe, 1 ; set lpend to true
 
 printf, pipe, 1 ; tells fortran to stop
 
     ; wait until fortran has completed calculation
 
 junk=''
 readf,pipe,junk,format='(A3)'
 
 flush, pipe
 close, pipe
 free_lun, pipe
  
 end

;-----------------------------------------------------------------------

 pro run_adas809, filein=filein, dtype=dtype              , $
                  dparam=dparam, numfile=numfile, te=te   , $
                  log=log, fileout=fileout, ev=ev, reduced=reduced,$
                  help=help

 if keyword_set(help) then begin 
   doc_library, 'run_adas809' 
   return
 endif
 
     ; Check that IDL 5.3 is not being used.

 if !version.release eq '5.3' then $
   message, 'This version of IDL (v5.3) will not work with the ' + $
           'SPAWN command.'

     ; Check that the inputs are valid.

     ; Check that a valid and readable type 1 filename is given.
     ; If not then the code stops.

 if (n_elements(filein) eq 0) then $
   message, 'A type 1 file must be selected'
   
 file_acc, filein, fileexist, r, w, ex, filetype
 if (fileexist eq 0) then message, 'type 1 file does not exist'
 
 if (fileexist and r eq 0) then $
   message, 'User does not have permission to read selected file'
 
     ; Get the input file type
 
 openr, unit, filein, /get_lun
 str = ''
 readf, unit, str
 if (strpos(str,'37') eq -1) then ftype = 'adf04' $
 else ftype = 'adf37'
 close, unit
 free_lun, unit    
   
     ; Get the distribution type.  Select 0 (Maxwellian) as default.
 
 dtype = n_elements(dtype) eq 0 ? 0 : dtype
 
     ; Check the distribution parameter if necessary
 
 if dtype eq 1 and n_elements(dparam) eq 0 then $
   message, 'Kappa distribution selected, but no specific value ' + $
            'of kappa chosen.'
 
 if dtype eq 3 and n_elements(dparam) eq 0 then $
   message, 'Druyvesteyn distribution selected, but no specific value ' + $
            'of x parameter chosen.'
 
 if dtype eq 2 then begin
   if n_elements(numfile) eq 0 then $
     message, 'Numerical distribution selected, but no specific ' + $
              'adf37 file chosen.'
   file_acc, numfile, fileexist, r, w, ex, filetype
   if fileexist eq 0 then message, 'adf37 file does not exist'
   if read eq 0 then $
     message, 'User does not have permission to read adf37 file'
 endif
 
 if n_elements(log) eq 0 then log = ''

     ; Get the name of the output file
 
 if n_elements(fileout) eq 0 then begin
   fileout = 'adas809_adf04.pass'
 endif 

     ; Determine temperature unit

 if keyword_set(ev) and keyword_set(reduced) then begin
      message, 'Specifying /ev and /reduced simultaneously is non-sensical'
 endif
  
 teunit=1
 if keyword_set(ev) then teunit=2
 if keyword_set(reduced) then teunit=3
 
 run_adas809_calc, filein, ftype, dtype, dparam, numfile, $
                   te, log, fileout, teunit
 
 end
