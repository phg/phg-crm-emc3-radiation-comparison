;+
; PROJECT:
;       UNIX IDL ADAS development
;
; NAME:
;	CW_ADAS809_IN()
;
; PURPOSE:
;	Data file selection for two input datasets.
;
; EXPLANATION:
;	This function creates a compound widget consisting of two
;       occurences of the compound widget cw_adas4xx_infile.pro
;	a 'Cancel' button, a 'Done' button and two buttons 
;	to allow the browsing of the selected data file comments.
;	The browsing and done buttons are automatically de-sensitised
;	until a valid input dataset has been selected.
;
;	The value of this widget is the settings structure of the
;	cw_adas4xx_infile widget.  This widget only generates events
;	when either the 'Done' or 'Cancel' buttons are pressed.
;	The event structure returned is;
;	{ID:0L, TOP:0L, HANDLER:0L, ACTION:''}
;	ACTION has one of two values 'Done' or 'Cancel'.
;
; USE:
;	See routine adas_in.pro for an example.
;
; INPUTS:
;	PARENT	- Long integer; the ID of the parent widget.
;
; OPTIONAL INPUTS:
;	None
;
; OUTPUTS:
;	This function returns the ID of the compound widget created.
;
; OPTIONAL OUTPUTS:
;	None
;
; KEYWORD PARAMETERS:
;	VALUE	- A structure which determines the initial settings of
;                 the dataset selection widget cw_adas4xx_infile.  The
;		  structure must be;
;		  {ROOTPATH1:'', FILE1:'', F2TYPE:0, ROOTPATH2:'', 
;                  FILE2:'', CENTROOT:'', USERROOT:'', CENTROOT2:'',
;                  USERROOT2:''}
;		  The elements of the structure are as follows;
;
;		  ROOTPATH1 - first data directory 
;                             e.g '/usr/fred/adas/adf04/'
;		  FILE1     - first data file in ROOTPATH1
;                             e.g 'type1/input.dat'
;		  ROOTPATH2 - second data directory
;                             e.g '/usr/fred/adas/adf04/'
;                 F1TYPE    - indicates the type of first file
;                             i.e. 0, 1 correspoinding to adf04 I
;                             or adf37 I 
;                 F2TYPE    - indicates the type of comparison file 
;                             chosen  i.e. 0, 1, 2 corresponding to
;                             adf04 III or IV, or adf37
;		  FILE2     - second data file in ROOTPATH2
;                             e.g 'input.dat'
;		  CENTROOT  - Default central data store 
;                             e.g '/usr/adas/adf04/'
;		  USERROOT  - Default user data store
;                             e.g '/usr/fred/adas/adf04'
;                 CENTROOT2 - Default central data store 
;                             e.g '/usr/adas/adf37/'
;		  USERROOT2 - Default user data store
;                             e.g '/usr/fred/adas/adf37'
;
;		  The first data file selected by the user is obtained
;                 by appending ROOTPATH1 and FILE1, and the second by
;                 appending ROOTPATH2 and FILE2.  In the above example
;		  the full names of the data files are
;		  /usr/fred/adas/adf04/type1/input.dat
;                 /usr/fred/adas/adf04/input.dat
;
;		  Path names may be supplied with or without the
;                 trailing '/'.  The underlying routines add this
;                 character where required so that USERROOT will always
;                 end in '/' on output.
;
;		  The default value is;
;		  {ROOTPATH1:'./', FILE1:'', ROOTPATH2:'./', FILE2:'',
;                  CENTROOT:'', USERROOT:''}
;		  i.e ROOTPATH1 & ROOTPATH2 are set to the user's
;                 current directory.
;
;	TITLE	- The title to be included in the input file widget,
;                 used to indicate exactly what the required input
;                 dataset is, e.g 'Input COPASE Dataset'
;
;	UVALUE	- A user value for the widget.  Defaults to 0.
;
;	FONT	- Supplies the font to be used for the interface
;                 widgets.
;		  Defaults to the current system font.
;
; CALLS:
;	XXTEXT	          Pop-up window to browse dataset comments.
;	CW_ADAS4xx_INFILE Dataset selection widget.
;	FILE_ACC	  Determine filetype and access permissions.
;
;	See side effects for other related widget management routines.
;
; SIDE EFFECTS:
;	IN809_GET_VAL()	Widget management routine in this file.
;	IN809_EVENT()	Widget management routine in this file.
;
; CATEGORY:
;	Compound Widget
;
; WRITTEN:
;       Hugh Summers, University of Strathclyde, 30 November 2001
;
; MODIFIED:
;	1.0	Hugh Summers
;		First version
;       1.1     Martin Torney
;               Updated comparison file selection
;               Fixed 'Done' button logic problems
;       1.2     Martin Torney
;               Added option of selecting an ADF37 type 1 for 1st file
;
; VERSION:
;       1.0	30-11-01
;       1.1     19-12-02
;       1.2     30-01-04
;-
;-----------------------------------------------------------------------

FUNCTION in809_get_val, id

		;***********************************
                ;**** Return to caller on error ****
		;***********************************
		
    ON_ERROR, 2
    

		;***********************************************
                ;**** Retrieve the structure from the child ****
		;***********************************************
    
    first_child = widget_info(id, /child)
    widget_control, first_child, get_uvalue=state, /no_copy
    inset = state.inset
    
         ;********************************************************
         ;**** Get settings and copy values to main structure ****
	 ;********************************************************
    
    ftype1 = widget_info(state.type1id, /droplist_select)
    
    if (ftype1 eq 0) then begin
      
      widget_control, state.file1id_0, get_value=inset1_0
      inset.rootpath1 = inset1_0.rootpath
      inset.file1 = inset1_0.file
      inset.f1type = 0
      inset.centroot = inset1_0.centroot
      inset.userroot = inset1_0.userroot      
      
    endif else if (ftype1 = 1) then begin
      
      widget_control, state.file1id_1, get_value=inset1_1
      inset.rootpath1 = inset1_1.rootpath
      inset.file1 = inset1_1.file
      inset.f1type = 1
      inset.centroot = inset1_1.centroot
      inset.userroot = inset1_1.userroot 
    
    endif    
    

    file_type = widget_info(state.type2id, /droplist_select)
    if (file_type eq 0) then begin
    
      widget_control, state.file2id_0, get_value=inset2_0
      inset.f2type = 0
      inset.rootpath2 = inset2_0.rootpath
      inset.file2 = inset2_0.file
      inset.centroot2 = inset2_0.centroot
      inset.userroot2 = inset2_0.userroot
      
    endif else if (file_type eq 1) then begin
    
      widget_control, state.file2id_1, get_value=inset2_1
      inset.f2type = 1
      inset.rootpath2 = inset2_1.rootpath
      inset.file2 = inset2_1.file
      inset.centroot2 = inset2_1.centroot
      inset.userroot2 = inset2_1.userroot
 
      
    endif else if (file_type eq 2) then begin
    
      widget_control, state.file2id_2, get_value=inset2_2
      inset.f2type = 2
      inset.rootpath2 = inset2_2.rootpath
      inset.file2 = inset2_2.file
      inset.centroot2 = inset2_2.centroot
      inset.userroot2 = inset2_2.userroot
 
    endif
    
    state.inset = inset

		;***************************
                ;**** restore the state ****
		;***************************

    widget_control, first_child, set_uvalue=state, /no_copy

		;*******************************
                ;**** return the value here ****
		;*******************************
   
   RETURN, inset

END

;-----------------------------------------------------------------------

FUNCTION in809_event, event

		;************************************
                ;**** Base ID of compound widget ****
		;************************************

    parent=event.handler

		;***********************************************
                ;**** Retrieve the structure from the child ****
		;***********************************************

    first_child = widget_info(parent, /child)
    widget_control, first_child, get_uvalue=state, /no_copy

		;*********************************
		;**** Clear previous messages ****
		;*********************************

    widget_control, state.messid, set_value=''    

		;*********************************
		;**** Default output no event ****
		;*********************************
    
    new_event = 0L

                ;************************
                ;**** Process events ****
                ;************************

    CASE event.id OF

		;******************************
		;**** First file selection ****
		;******************************
        
      state.type1id: begin
            
        if (event.index eq 0) then begin
            
          widget_control, state.type1id, set_droplist_select=0
          widget_control, state.base1_0, map=1
          widget_control, state.base1_1, map=0
          comp_sens = 1
          widget_control, state.comp_title, sensitive=comp_sens
          widget_control, state.type2id, sensitive=comp_sens
          widget_control, state.file2id_0, sensitive=comp_sens
          widget_control, state.file2id_1, sensitive=comp_sens
          widget_control, state.file2id_2, sensitive=comp_sens
          widget_control, state.browse2id, sensitive=comp_sens
          ;if (n_elements(filename2) ne 0) then begin
            filename2 = state.inset.rootpath2 + state.inset.file2
            file_acc, filename2, fileexist, read, write, execute,$
               filetype
            if (filetype ne '-') then $
              widget_control, state.browse2id, sensitive=0 $
            else if (read eq 0) then widget_control, state.browse2id, $
              sensitive=0
              
          ;endif
        endif
        
        if (event.index eq 1) then begin
              
          widget_control, state.type1id, set_droplist_select=1
          widget_control, state.base1_0, map=0
          widget_control, state.base1_1, map=1
          comp_sens = 0
          widget_control, state.comp_title, sensitive=comp_sens
          widget_control, state.type2id, sensitive=comp_sens
          widget_control, state.file2id_0, sensitive=comp_sens
          widget_control, state.file2id_1, sensitive=comp_sens
          widget_control, state.file2id_2, sensitive=comp_sens
          widget_control, state.browse2id, sensitive=comp_sens             
        endif
        
      end
      
              
      state.file1id_0: goto, jump1
      state.file1id_1: begin
        jump1:
	if (event.action eq 'newfile') then begin
	  widget_control, state.browse1id, /sensitive
	  widget_control, state.doneid, /sensitive
	endif else begin
	  widget_control, state.browse1id, sensitive=0
	  widget_control, state.doneid, sensitive=0
	endelse
      end

		;***********************
		;**** Browse button ****
		;***********************

      state.browse1id: begin

		;*****************************
		;**** Get latest filename ****
		;*****************************
                
        f1type = widget_info(state.type1id, /droplist_select)
        if (f1type eq 0) then begin
	      
          widget_control, state.file1id_0, get_value=inset1_0
          file1_0 = inset1_0.rootpath + inset1_0.file
	  file1_1 = ' '
	  filename1 = file1_0
            
        endif else if (f1type eq 1) then begin
              
          widget_control, state.file1id_1, get_value=inset1_1
	  file1_0 = ' '
          file1_1 = inset1_1.rootpath + inset1_1.file
	  filename1 = file1_1
  
        endif                 


		;**********************************
		;**** Invoke comments browsing ****
		;**********************************
          
	xxtext, filename1, font=state.font
          
      end
        
                ;****************************************
                ;**** Comparison file type Selection ****
                ;****************************************
                
      state.type2id: begin
        
        if (event.index eq 0) then begin 
             
          widget_control, state.type2id, set_droplist_select = 0
          widget_control, state.base2_0,map=1
          widget_control, state.base2_1,map=0
          widget_control, state.base2_2,map=0
              
        endif
        if (event.index eq 1) then begin
              
          widget_control, state.type2id, set_droplist_select = 1
          widget_control, state.base2_0,map=0
          widget_control, state.base2_1,map=1
          widget_control, state.base2_2,map=0

        endif
        if (event.index eq 2) then begin
              
          widget_control, state.type2id, set_droplist_select = 2
          widget_control, state.base2_0,map=0
          widget_control, state.base2_1,map=0
          widget_control, state.base2_2,map=1

        endif

      end
        
		;*******************************
		;**** Second file selection ****
		;*******************************

      state.file2id_0: goto, jump2
      state.file2id_1: goto, jump2
      state.file2id_2: begin
        jump2:
	if (event.action eq 'newfile') then $
	  widget_control, state.browse2id, /sensitive $
	    else widget_control, state.browse2id, sensitive=0
      end

		;***********************
		;**** Browse button ****
		;***********************

      state.browse2id: begin

		;*****************************
		;**** Get latest filename ****
		;*****************************
               
        f2type = widget_info(state.type2id, /droplist_select)
             
        if (f2type eq 0) then begin
	     
          widget_control, state.file2id_0, get_value=inset_0
          file2_0 = inset_0.rootpath + inset_0.file
	  file2_1 = ' '
	  file2_2 = ' '
	  filename2 = file2_0
          state.inset.rootpath2=inset_0.rootpath
          state.inset.file2 = inset_0.file
              
        endif else if (f2type eq 1) then begin
              
          widget_control, state.file2id_1, get_value=inset_1
	  file2_0 = ' '
          file2_1 = inset_1.rootpath + inset_1.file
          file2_2 = ' '
	  filename2 = file2_1
          state.inset.rootpath2=inset_1.rootpath
          state.inset.file2 = inset_1.file
	      
        endif else if (f2type eq 2) then begin
              
          widget_control, state.file2id_2, get_value=inset_2
	  file2_0 = ' '
	  file2_1 = ' '
          file2_2 = inset_2.rootpath + inset_2.file
	  filename2 = file2_2
          state.inset.rootpath2=inset_2.rootpath
          state.inset.file2 = inset_2.file
              
        endif 
              
                ;**********************************
		;**** Invoke comments browsing ****
		;**********************************

	xxtext, filename2, font=state.font
            
      end
		;***********************
		;**** Cancel button ****
		;***********************

      state.cancelid: new_event = {ID:parent, TOP:event.top, 	$
	 		           HANDLER:0L, ACTION:'Cancel'  }

		;*********************
		;**** Done button ****
		;*********************

      state.doneid: new_event = {ID:parent, TOP:event.top, 	$
                                 HANDLER:0L, ACTION:'Done'      }                  

      ELSE:			;**** Do nothing ****

    ENDCASE
    
                ;*********************************************
                ;*** Check if comparison file is selected, ***
                ;*** and if soupdate STATE.INSET           ***
                ;*********************************************
                
    f2type = widget_info(state.type2id, /droplist_select)
             
    if (f2type eq 0) then begin
	     
      widget_control, state.file2id_0, get_value=inset_0
      file2_0 = inset_0.rootpath + inset_0.file
      file2_1 = ' '
      file2_2 = ' '
      filename2 = file2_0
      state.inset.rootpath2=inset_0.rootpath
      state.inset.file2 = inset_0.file
              
    endif else if (f2type eq 1) then begin
              
      widget_control, state.file2id_1, get_value=inset_1
      file2_0 = ' '
      file2_1 = inset_1.rootpath + inset_1.file
      file2_2 = ' '
      filename2 = file2_1
      state.inset.rootpath2=inset_1.rootpath
      state.inset.file2 = inset_1.file
	      
    endif else if (f2type eq 2) then begin
              
      widget_control, state.file2id_2, get_value=inset_2
      file2_0 = ' '
      file2_1 = ' '
      file2_2 = inset_2.rootpath + inset_2.file
      filename2 = file2_2
      state.inset.rootpath2=inset_2.rootpath
      state.inset.file2 = inset_2.file
              
    endif
    
                ;**************************************************
                ;*** De/sensitise browse comparison file button ***
                ;**************************************************
    
    if (n_elements(filename2) ne 0) then begin
      filename2 = state.inset.rootpath2 + state.inset.file2
      file_acc, filename2, fileexist, read, write, execute, filetype
      if (filetype ne '-') then $
        widget_control, state.browse2id, sensitive=0 $
          else if (read eq 0) then widget_control, state.browse2id, $
            sensitive=0
    endif
		;***************************
                ;**** restore the state ****
		;***************************
                        
    widget_control, first_child, set_uvalue=state, /no_copy
    
    RETURN, new_event

END

;-----------------------------------------------------------------------

FUNCTION cw_adas809_in, parent, VALUE=value, TITLE=title, 	$
                        UVALUE=uvalue, FONT=font

    IF (N_PARAMS() EQ 0) THEN $
      MESSAGE, 'Must specify parent for cw_adas_in'
    ON_ERROR, 2					    ;return to caller

		;***********************************
		;**** Set defaults for keywords ****
		;***********************************

    IF NOT (KEYWORD_SET(value)) THEN begin
      inset = {	PMDFLG	  : 0,    $
               	ROOTPATH1 : './', $
		FILE1	  : '',   $
                F1TYPE    : 0,    $
		F2TYPE    : 0,    $
		ROOTPATH2 : './', $
		FILE2	  : '',   $
		CENTROOT  : '',   $
		USERROOT  : '',	  $
                CENTROOT2 : '',   $
                USERROOT2 : ''    }
    ENDIF ELSE BEGIN
      inset = {	PMDFLG	  : 0,               $
               	ROOTPATH1 : value.rootpath1, $
		FILE1	  : value.file1,     $
                F1TYPE    : value.f1type,    $
		F2TYPE    : value.f2type,    $
                ROOTPATH2 : value.rootpath2, $
		FILE2	  : value.file2,     $
		CENTROOT  : value.centroot,  $
		USERROOT  : value.userroot,  $
                CENTROOT2 : value.centroot2, $
                USERROOT2 : value.userroot2  }
			
      if (strtrim(inset.rootpath1) eq '') then begin
        inset.rootpath1 = './'
      endif else if $
	(strmid(inset.rootpath1,strlen(inset.rootpath1)-1, 1) ne '/') $
          then inset.rootpath1 = inset.rootpath1 + '/'
      if (strmid(inset.file1, 0, 1) eq '/') then $
        inset.file1 = strmid(inset.file1, 1, strlen(inset.file1)-1)
      if (strtrim(inset.rootpath2) eq '') then begin
        inset.rootpath2 = './'
      endif else if $
       (strmid(inset.rootpath2, $
         strlen(inset.rootpath2)-1, 1) ne '/') $
	   then inset.rootpath2 = inset.rootpath2 + '/'
      if (strmid(inset.file2, 0, 1) eq '/') then $
        inset.file2 = strmid(inset.file2, 1, strlen(inset.file2)-1)
	
    ENDELSE
    
    IF NOT (KEYWORD_SET(title)) THEN title = ''
    IF NOT (KEYWORD_SET(uvalue)) THEN uvalue = 0
    IF NOT (KEYWORD_SET(font)) THEN font = ''
    
		;*********************************
		;**** Create the Input widget ****
		;*********************************

		;****************************
		;**** create base widget ****
		;****************************

    topbase = widget_base( parent, UVALUE = uvalue,    		$
			   EVENT_FUNC = "in809_event", 		$
		       	   FUNC_GET_VALUE = "in809_get_val",	$
			   /COLUMN)

                ;******************************************************
                ;**** Create a dummy widget just to hold value of *****
                ;**** "state" variable so as not to get confused  *****
                ;**** with any other values. Adopt IDL practice   *****
                ;**** of using first child widget                 *****
                ;******************************************************

    first_child = widget_base(topbase)
    cwid = widget_base(first_child, /column)

		;*******************************************
		;**** Set up first file structure       ****
		;*******************************************

    inset1 = {   ROOTPATH:inset.rootpath1, FILE:inset.file1,      $
                 CENTROOT:inset.centroot, USERROOT:inset.userroot }

		;*******************************************
		;****  First file selection widget      ****
		;*******************************************
    
    filebase1 = widget_base(cwid, /frame, /column)
    type_text = widget_label(filebase1, $
                  value='Input ADF04 type 1 or ADF37 type 1 file', $
                  font=font, /align_center)
    type1id = widget_droplist(filebase1, /dynamic_resize, /sensitive, $
                value=['ADF04 type 1','ADF37 type 1'], $
                uvalue=[0,1], font=font)
                
    widget_control, type1id, set_droplist_select=inset.f1type
                
    fname1 = strarr(2)
    
    for i=0, 1 do $
      if inset.f1type eq i then $
        fname1(i) = inset.file1 $
          else fname1(i)=''
    
    multibase1 = widget_base(filebase1)
    
    base1_0 = widget_base(multibase1)
    inset1_0 = {  ROOTPATH:inset.userroot, FILE:'',        $
                  CENTROOT:inset.centroot, USERROOT:inset.userroot }
    if (inset.f1type eq 0) then begin
      inset1_0.rootpath = inset.rootpath1
      inset1_0.file = inset.file1
    endif                    
    file1id_0 = cw_adas4xx_infile(base1_0, value=inset1_0, font=font)
    
    base1_1 = widget_base(multibase1)
    inset1_1 = {  ROOTPATH:inset.userroot2, FILE:fname1(1),        $
                  CENTROOT:inset.centroot2, USERROOT:inset.userroot2 }
    if (inset.f1type eq 1) then begin
      inset1_1.rootpath = inset.rootpath1
      inset1_1.file = inset.file1
    endif                     
    file1id_1 = cw_adas4xx_infile(base1_1, value=inset1_1, font=font)
    
    widget_control, base1_0, map=0
    widget_control, base1_1, map=0
    
    CASE inset.f1type OF
    
      0: begin
        widget_control, base1_0, map=1
        widget_control, base1_1, map=0
      end
      1: begin
        widget_control, base1_0, map=0
        widget_control, base1_1, map=1
      end
           
    endCASE
        
		;*****************
		;**** Buttons ****
		;*****************

    base1 = widget_base(filebase1, /row)

		;*******************************
		;**** Browse Dataset button ****
		;*******************************

    browse1id = widget_button(base1, value='Browse Comments', font=font)
    
		;**************************************
		;**** Set up second file structure ****
		;**************************************
    
            ; If file 1 is an adf37 type 1 then all the comparison 
            ; file widgets are desensitised.
            
    if (inset.f1type eq 1) then comp_sens = 0 else comp_sens = 1
    
    		;**************************************
		;**** Second file selection widget ****
		;**************************************

    filebase2 = widget_base(cwid, /frame, /column)
    
                ;**************************
		;*** File type droplist ***
		;**************************
		
    comp_title = widget_label(filebase2, $
                 value='  Select file for temperature comparison:', $
                 font=font)
    widget_control, comp_title, sensitive=comp_sens
    type2id = widget_droplist(filebase2, /dynamic_resize, $
              sensitive=comp_sens, $
              value=['ADF04 type 3','ADF04 type 4','ADF37 type 2'], $
              uvalue=[0,1,2], font=font)
    
    widget_control, type2id, set_droplist_select=inset.f2type
              
                ;*****************************************************
		;*** Create a file input widget for each file type ***
		;*****************************************************
		
    fname2 = strarr(3)
    for i=0,2 do $
      if (inset.f2type eq i) then $
        fname2(i) = inset.file2 $
         else fname2(i) = ' '
    
    multibase2=widget_base(filebase2)
    
    base2_0=widget_base(multibase2)
    inset2_0 = {  ROOTPATH:inset.userroot, FILE:'',       $
                  CENTROOT:inset.centroot, USERROOT:inset.userroot }
    if (inset.f2type eq 0) then begin
      inset2_0.rootpath = inset.rootpath2
      inset2_0.file = inset.file2
    endif
    file2id_0 = cw_adas4xx_infile(base2_0, value=inset2_0, 	  $
                                  font=font)
    widget_control, file2id_0, sensitive=comp_sens

    base2_1=widget_base(multibase2)
    inset2_1 = {  ROOTPATH:inset.userroot, FILE:'', $
                  CENTROOT:inset.centroot, USERROOT:inset.userroot   }
    if (inset.f2type eq 1) then begin
      inset2_1.rootpath = inset.rootpath2
      inset2_1.file = inset.file2
    endif              
    file2id_1 = cw_adas4xx_infile(base2_1, value=inset2_1, font=font)
    widget_control, file2id_1, sensitive=comp_sens
      
    base2_2=widget_base(multibase2)
    inset2_2 = {  ROOTPATH:inset.userroot2, FILE:'',         $
                  CENTROOT:inset.centroot2, USERROOT:inset.userroot2 }
    if (inset.f2type eq 2) then begin
      inset2_2.rootpath = inset.rootpath2
      inset2_2.file = inset.file2
    endif                       
    file2id_2 = cw_adas4xx_infile(base2_2, value=inset2_2, font=font)
    widget_control, file2id_2, sensitive=comp_sens
     
                 ;*****************************************
                 ;***Select which base to map initially ***
		 ;*****************************************
    
    widget_control,base2_0,map=0
    widget_control,base2_1,map=0
    widget_control,base2_2,map=0
     
    CASE inset.f2type OF

      0: widget_control,base2_0,map=1
      1: widget_control,base2_1,map=1
      2: widget_control,base2_2,map=1
	   
    endCASE 
         
		;*****************
		;**** Buttons ****
		;*****************

    base2 = widget_base(filebase2,/row)

		;*******************************
		;**** Browse Dataset button ****
		;*******************************

    browse2id = widget_button(base2, value='Browse Comments', font=font)
    widget_control, browse2id, sensitive=comp_sens
		;***********************
		;**** Cancel Button ****
		;***********************

    base = widget_base(cwid, /row)

    cancelid = widget_button(base, value='Cancel', font=font)
  
		;*********************
		;**** Done Button ****
		;*********************

    doneid = widget_button(base, value='Done', font=font)

		;***********************
		;**** Error message ****
		;***********************

    messid = widget_label(parent, font=font, value='*')

	       ;********************************************************
	       ;**** Check default filenames and desensitise buttons****
	       ;**** if they are directories or have no read access ****
	       ;********************************************************

    filename1 = inset.rootpath1 + inset.file1
    file_acc, filename1, fileexist, read, write, execute, filetype
    if (filetype ne '-') then begin
      widget_control, browse1id, sensitive=0
      widget_control, doneid, sensitive=0
    endif else if (read eq 0) then begin
      widget_control, browse1id, sensitive=0
      widget_control, doneid, sensitive=0
    endif
    
    filename2 = inset.rootpath2 + inset.file2
    file_acc, filename2, fileexist, read, write, execute, filetype
    if (filetype ne '-') then $
      widget_control, browse2id, sensitive=0 $
        else if (read eq 0) then widget_control, browse2id, $
          sensitive=0
  
		;*************************************************
		;**** create a state structure for the widget ****
		;*************************************************

    new_state = {       FILE1ID_0 : file1id_0 ,	$
                 	FILE1ID_1 : file1id_1 ,	$
                 	BROWSE1ID : browse1id ,	$
                        TYPE1ID   : type1id   , $
                        TYPE2ID   : type2id   , $
                 	FILE2ID_0 : file2id_0 ,	$
                 	FILE2ID_1 : file2id_1 ,	$
                 	FILE2ID_2 : file2id_2 ,	$
                        BASE1_0   : base1_0   ,	$
                 	BASE1_1   : base1_1   ,	$
                  	BASE2_0   : base2_0   ,	$
                 	BASE2_1   : base2_1   ,	$
                 	BASE2_2   : base2_2   ,	$
                 	BROWSE2ID : browse2id ,	$
	 	 	CANCELID  : cancelid  ,	$
                 	DONEID    : doneid    ,	$
                 	MESSID    : messid    ,	$
		 	FONT      : font      ,	$
                        COMP_TITLE: comp_title, $
                 	INSET     : inset     	}

		;**************************************
                ;**** Save initial state structure ****
		;**************************************
    
    widget_control, first_child,  set_uvalue=new_state, /no_copy
    
    RETURN, cwid

END

