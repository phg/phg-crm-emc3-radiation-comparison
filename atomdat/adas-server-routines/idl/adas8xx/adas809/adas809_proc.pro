;+
; PROJECT:
;       ADAS IBM MVS to UNIX conversion
;
; NAME:
;	ADAS809_PROC
;
; PURPOSE:
;	IDL ADAS user interface, processing options/input.
;
; EXPLANATION:
;	This routine creates and manages a pop-up window which allows
;	the user to select options and input data to control ADAS809
;	processing.
;
; USE:
;	This routine is ADAS809 specific, see b1ispf.pro for how it
;	is used.
;
; INPUTS:
;	PROCVAL	- A structure which determines the initial settings of
;		  the processing options widget.  The value is passed
;		  unmodified into cw_adas809_proc.pro.
;
;		  See cw_adas809_proc.pro for a full description of this
;		  structure.
;
;	STRGA() - String array  : Level designations
;
;	DSFULL	- String; The full system file name of the input 
;		          dataset selected by the user for processing.
;
;	NDTIN	- Integer : Maximum number of temperatures allowed.
;
;	IL	- Integer : Number of energy levels
;
;	NDTEM   - Integer : Maximum number of temperatures from data file.
;
;	NV	- Integer : Number of temperatures frominput data file
;
;	ITRAN	- Integer : Number of electron impact transitions.
;
;	TSCEF(,)- Double array : Electron temperatures from input data file
;				first dimension - ndtem
;				 Second dimension - 3
;				                  - 1 => Kelvin
;						    2 => eV
;						    3 => Reduced
;
;	I1A()	- Integer array : Electron impact transition, lower energy
;				  level index.
;				 
;	I2A()	- Integer array : Electron impact transition, upper energy
;				  level index.
;
;       BITFILE - String; the path to the dirctory containing bitmaps
;                 for the 'escape to series menu' button.
;
; OPTIONAL INPUTS:
;	None
;
; OUTPUTS:
;	PROCVAL	- On output the structure records the final settings of
;		  the processing selections widget if the user pressed the
;		  'Done' button, otherwise it is not changed from input.
;
;	ACT	- String; Either 'Done' or 'Cancel' for the button the
;		  user pressed to terminate the processing options
;		  window.
;
; OPTIONAL OUTPUTS:
;	None
;
; KEYWORD PARAMETERS:
;	FONT_LARGE - The name of a larger font.
;
;	FONT_SMALL - The name of a smaller font.
;
;	EDIT_FONTS - A structure of two fonts used in the table editor
;		     adas_edtab.pro, {font_norm:'',font_input:''}.
;
; CALLS:
;	CW_ADAS809_PROC	Declares the processing options widget.
;	ADAS809_PROC_EV Called indirectly during widget management,
;			routine included in this file.
;	XMANAGER
;
; SIDE EFFECTS:
;       This widget uses a common block PROC809_BLK, to pass the
;       variables VALUE and ACTION between the two routines. 
;
; CATEGORY:
;	Widgets
;
; WRITTEN:
;       Hugh Summers, University of Strathclyde,  30 November 2001
;
; MODIFIED:
;	1.1	Hugh Summers
;		First Release
;       1.2     Martin Torney
;               Code now handles non-Maxwellian distributions
;       1.3     Paul Bryans
;               Removed quadrature options
;
; VERSION:
;	1.1	30-11-01
;       1.2     19-12-02
;       1.3     22-11-04
;-
;-----------------------------------------------------------------------------


PRO adas809_proc_ev, event


  COMMON proc809_blk,action,value

    action = event.action
    CASE event.action OF

		;**** 'Done' button ****
	'Done'  : begin

		;**** Get the output widget value ****
		      widget_control,event.id,get_value=value 
		      widget_control,event.top,/destroy

	           end


		;**** 'Cancel' button ****
	'Cancel': widget_control,event.top,/destroy

		;**** 'Menu' button ****
	'Menu': widget_control,event.top,/destroy

    END

END

;-----------------------------------------------------------------------------


PRO adas809_proc, procval, strga, dsfull, $
		  ndtin, il, ndtem, nv, itran, ftype, $
		  tscef, tscef2, i1a, i2a, act, bitfile, $
		  FONT_LARGE=font_large, FONT_SMALL=font_small, $
		  EDIT_FONTS=edit_fonts


		;**** declare common variables ****
  COMMON proc809_blk,action,value

		;**** Copy "procval" to common ****
  value = procval

		;**** Set defaults for keywords ****
  IF NOT (KEYWORD_SET(font_large)) THEN font_large = ''
  IF NOT (KEYWORD_SET(font_small)) THEN font_small = ''
  IF NOT (KEYWORD_SET(edit_fonts)) THEN $
				edit_fonts = { font_norm:'', font_input:''}
		
                ;**** create base widget ****
  procid = widget_base(TITLE='ADAS809 PROCESSING OPTIONS', $
					XOFFSET=100,YOFFSET=0)

		;**** Declare processing widget ****
  cwid = cw_adas809_proc(procid, strga, dsfull,  $
                         ndtin, il, ndtem, nv, itran, ftype, $
                  	 tscef, tscef2, i1a, i2a, act, bitfile, $
			 PROCVAL=value, $
		 	 FONT_LARGE=font_large, FONT_SMALL=font_small, $
			 EDIT_FONTS=edit_fonts)

		;**** Realize the new widget ****
  dynlabel, procid
  widget_control,procid,/realize

		;**** make widget modal ****
  xmanager,'adas809_proc',procid,event_handler='adas809_proc_ev', $
					/modal,/just_reg

		;*** copy value back to procval for return to b1ispf ***
  act = action
  procval = value
 
END

