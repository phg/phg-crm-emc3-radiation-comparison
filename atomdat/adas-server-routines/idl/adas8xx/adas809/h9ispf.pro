;+
; PROJECT:
;       ADAS IBM MVS to UNIX conversion
;
; NAME:
;	H9ISPF
;
; PURPOSE:
;	IDL user interface and communications with ADAS809 FORTRAN
;	process via pipe.
;
; EXPLANATION:
;	The routine begins by reading information from the ADAS809
;	FORTRAN process via a UNIX pipe.  Then part of the ADAS809
;	IDL user interface is invoked to determine how the user
;	wishes to process the input dataset.  When the user's
;	interactions are complete the information gathered with
;	the user interface is written back to the FORTRAN process
;	via the pipe.  Communications are to the FORTRAN subroutine
;	B1ISPF.
;
; USE:
;	The use of this routine is specific to ADAS809, see adas809.pro.
;
; INPUTS:
;	PIPE	- The IDL unit number of the bi-directional pipe to the
;		  ADAS809 FORTRAN process.
;
;	LPEND	- Integer, 0 if the user pressed the 'Done' button
;		  or 1 if the user pressed 'Cancel'.  Maps directly onto
;		  the logical variable LPEND in ADAS809 FORTRAN.
;
;	PROCVAL	- A structure which determines the initial settings of
;		  the processing options widget.  The initial value is
;		  set in adas809.pro.  If adas809.pro passes a blank 
;		  dummy structure of the form {NEW:-1} into PROCVAL then
;		  PROCVAL is reset to a default structure.
;
;		  The PROCVAL structure is;
;			procval = {                           $
;                			new     : 0 ,         $
;                			title   : '',         $
;                                       istrn   : 0 ,         $
;                                       maxt    : nv,         $
;                                       ifout   : 1,          $
;                                       tine    : tscef(0,*), $
;                                       lfsel   : 0,          $
;                                       tolval  : 5,          $
;                                       losel   : 0,          $
;                                       updown  : 0,          $
;                                       dtype   : '',         $
;                                       kap_val : 0,          $
;                                       num_val : ' ',        $     
;                                 }
;
;
;		  See cw_adas809_proc.pro for a full description of this
;		  structure.
;	
;
;	STRGA 	- String array; Designations for levels
;
;	DSFULL1	- String; The full system file name of the input 
;		  dataset selected by the user for processing.
;	DSFULL2	- String; The full system file name of the input 
;		  dataset selected by the user for comparison.
;
;       BITFILE - String; the path to the dirctory containing bitmaps
;                 for the 'escape to series menu' button.
;
; OPTIONAL INPUTS:
;	None
;
; OUTPUTS:
;	PROCVAL	- On output the structure records the final settings of
;		  the processing selections widget if the user pressed the
;		  'Done' button, otherwise it is not changed from input.
;
;       GOMENU  - Int; flag - set to 1 if user has selected 'escape direct
;                 to series menu , otherwise 0
;
; OPTIONAL OUTPUTS:
;	None
;
; KEYWORD PARAMETERS:
;	FONT_LARGE - The name of a larger font.
;
;	FONT_SMALL - The name of a smaller font.
;
;	EDIT_FONTS - A structure of two fonts used in the table editor
;		     adas_edtab.pro, {font_norm:'',font_input:''}.
;
; CALLS:
;	ADAS809_PROC	Invoke the IDL interface for ADAS809 data
;		 	processing options/input.
;
; SIDE EFFECTS:
;	Two way communications with ADAS809 FORTRAN via the
;	bi-directional UNIX pipe.
;
; CATEGORY:
;	Adas system.
;
; WRITTEN:
;       Hugh Summers, University of Strathclyde, 30 November 2001
;
; MODIFIED:
;	1.1	Hugh Summers
;		First Release
;       1.2     Martin Torney
;               Included various new elements to accomodate 
;               non-Maxwellians
;       1.3     Paul Bryans
;               Removed quadrature options
; VERSION:
;	1.1	30-11-01
;       1.2     19-12-02
;       1.3     22-11-04
;-
;-----------------------------------------------------------------------------

 PRO H9ISPF, pipe, lpend, procval, strga, dsfull1, dsfull2, bitfile, $
	     gomenu, FONT_LARGE=font_large, FONT_SMALL=font_small,   $
	     EDIT_FONTS=edit_fonts

                ;**** Set defaults for keywords ****

 IF NOT (KEYWORD_SET(font_large)) THEN font_large = ''
 IF NOT (KEYWORD_SET(font_small)) THEN font_small = ''
 IF NOT (KEYWORD_SET(edit_fonts)) THEN $
			       edit_fonts = {font_norm:'',font_input:''}

               ;********************************************
               ;**** Declare variables for input	****
               ;**** arrays wil be declared after sizes ****
               ;**** have been read.			****
	       ;********************************************
	       
 lpend = 0
 ndtin = 0
 ndtem = 0
 il = 0
 nv = 0
 itran = 0
 input = 0
 ftype = ' '

               ;********************************
               ;**** Read data from fortran ****
               ;********************************
	       
 readf, pipe, input
 ndtin = input
 readf, pipe, input
 il = input
 readf, pipe, input
 ndtem = input
 readf, pipe, input
 nv = input
 readf, pipe, input
 itran = input
 readf, pipe, ftype  

               ;******************************************
               ;**** Now can define other array sizes ****
               ;******************************************
	       
 tscef = dblarr(3,ndtem)
 tscef2 = dblarr(3,ndtem)
 i1a = intarr(itran)
 i2a = intarr(itran)

               ;****************************************************
               ;*** Define some variables used to read arrays in ***
               ;****************************************************
 temp = 0.0D
 itemp = 0
 
               ;********************************
	       ;**** Read data from fortran ****
	       ;********************************

 for i = 0, 2 do for j = 0, ndtem-1 do begin
   readf, pipe, temp
   tscef(i,j) = temp
 endfor
 
 for i = 0, 2 do for j = 0, ndtem-1 do begin
   readf, pipe, temp
   tscef2(i,j) = temp
 endfor
 
 for i = 0, itran-1 do begin
   readf, pipe, itemp
   i1a(i)  = itemp
 endfor

 for i = 0, itran-1 do begin
   readf, pipe, itemp
   i2a(i) = itemp
 endfor
 
               ;*******************************************
               ;**** Set default value if non provided ****
               ;*******************************************
	       
 if (procval.new lt 0) then            $
    procval = { 		       $
               new	: 0 ,	       $
               title	: '',	       $
               istrn	: 0 ,	       $
               maxt	: nv,	       $
	       ifout	: 1,	       $
               tine	: tscef(0,*),  $
               lfsel	: 0,	       $
               tolval	: 5,	       $
               losel	: 1,	       $
               updown	: 0,	       $
               dtype	: '',	       $
               kap_val  : 0,	       $
               dru_val  : 0,           $
	       num_val  : ' '          $
;               ilinr	: 0,	       $
;               ifint	: 0,	       $
;               iescl	: 0	       $	       
             }
             
 procval.maxt = nv

	       ;****************************************
               ;**** Pop-up processing input widget ****
               ;****************************************
 
 adas809_proc, procval, strga, dsfull1,  $
               ndtin, il, ndtem, nv, itran, ftype, $ 
               tscef, tscef2, i1a, i2a, action, bitfile, $
               FONT_LARGE=font_large, FONT_SMALL=font_small, $
               EDIT_FONTS=edit_fonts
 
               ;**********************************************
               ;****  Act on the event from the widget    ****
               ;**** There are only three possible events ****
               ;**** 'Done', 'Cancel' and 'Menu'.	  ****
               ;**********************************************
	       
 gomenu = 0
 if (action eq 'Done') then begin
   lpend = 0
 endif else if (action eq 'Menu') then begin
   lpend = 1
   gomenu = 1
 endif else lpend = 1
   
; if (procval.dtype eq 2) then read_adf37, procval.num_val, en, f

               ;*******************************
               ;**** Write data to fortran ****
               ;*******************************
                	       
 printf, pipe, lpend   
 printf, pipe, procval.title,format='(a40)' 
 printf, pipe, procval.istrn
 printf, pipe, procval.ifout
 printf, pipe, procval.maxt 
 
 for i = 0, procval.maxt-1 do $
   printf, pipe, procval.tine(i)
 
 printf, pipe, procval.lfsel
 if (procval.lfsel eq 1) then printf,pipe,procval.tolval
 printf, pipe, procval.losel
 
 dtype = fix(procval.dtype)
 printf, pipe, dtype
 
 CASE dtype OF
 
   1: begin
      printf, pipe, procval.kap_val
;      k1 = lngamma(procval.kap_val+1.)
;      k2 = lngamma(procval.kap_val-.5)
;      printf, pipe, k1
;      printf, pipe, k2
   end
   2: begin
      printf, pipe, procval.num_val, format='(A)'
;      printf, pipe, n_elements(en(*,0))
;      printf, pipe, n_elements(en(0,*))
;      for i = 0, n_elements(en(*,0))-1 do $
;	for j = 0, n_elements(en(0,*))-1 do $
;	  printf, pipe, en(i,j), f(i,j)
   end
   3: begin
      printf, pipe, procval.dru_val
   end        
   else:
   
 endCASE
 
; printf, pipe, procval.ilinr
; printf, pipe, procval.ifint
; printf, pipe, procval.iescl
  
 END
