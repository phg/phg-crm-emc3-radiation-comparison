;
; PROJECT:
;       ADAS IBM MVS to UNIX conversion
;
; NAME:
;	PLOT809
;
; PURPOSE:
;	Plot one graph for ADAS809.
;
; EXPLANATION:
;	This routine plots ADAS809 output for a single graph.
;
; USE:
;	Use is specific to ADAS809.  See adas809_plot.pro for
;	example.
;
; INPUTS:
;       GR_VALUE- Integer; Indicates which function to plot:
;                          0: Upsilon
;                          1: Downsilon
;                          2: Excitation rate coefficient
;                          3: De-excitation rate coefficient
;                          4: Upsilon and Downsilon
;	X	- Double array; list of x values.
;
;	Y	- 3D double array; function type, y values, 
;                 2nd index ordinary level, indexed by iord1 and iord2.
;
;	NENER   - Integer : Number of data points and spline fit points
;
;	NMX     - Integer : Number of points used in polynomial fit.
;
;	NPSPL	- Integer : Number of intepolated points for spline fit.
;
;	NPLOTS  - Integer : type of plots  1: Data from file only
;					   3: Data and spline fit
;					   5: Data and minmax fit
;					   7: Data, spline, & minimax fits.
;
;	TITLE	- String array : General title for program run. 5 lines.
;
;	XTITLE  - String : title for x-axis annotation
;
;	YTITLE  - String : title for y-axis annotation
;
;	LDEF1	- Integer; 1 - User specified axis limits to be used, 
;		  	   0 - Default scaling to be used.
;
;	XMIN	- String; Lower limit for x-axis of graph, number as string.
;
;	XMAX	- String; Upper limit for x-axis of graph, number as string.
;
;	YMIN	- String; Lower limit for y-axis of graph, number as string.
;
;	YMAX	- String; Upper limit for y-axis of graph, number as string.
;
; OPTIONAL INPUTS:
;	None.
;
; OUTPUTS:
;	None.
;
; OPTIONAL OUTPUTS:
;	None.
;
; KEYWORD PARAMETERS:
;	None.
;
; CALLS:
;	None.
;
; SIDE EFFECTS:
;	None.
;
; CATEGORY:
;	Adas system.	
;
; WRITTEN:
;       Hugh Summers, University of Strathclyde, 30 November 2001 
;
; MODIFIED:
;	1.1	Hugh Summers
;		First Release
;       1.2     Martin Torney
;               Added a dimension to 'y'
;               Code now plots user selected function
;
; VERSION:
;	1.1	30-11-01
;       1.2     28-01-04
;
;----------------------------------------------------------------------------

PRO plot809, gr_value, x , y, nener, nmx, npspl, nplots, title, xtitle, $
             ytitle, ldef1, xmin, xmax, ymin, ymax

COMMON Global_lw_data, left, right, top, bottom, grtop, grright

		;****************************************************
		;**** Suitable character size for current device ****
		;**** Aim for 60 characters in y direction.      ****
		;****************************************************

  charsize = (!d.y_vsize/!d.y_ch_size)/60.0 
  small_check = GETENV('VERY_SMALL')
  if (small_check eq 'YES') then charsize=charsize*0.8
  if (gr_value lt 4) then pl_value = gr_value else pl_value = 0

		;**** set makeplot counter ****

  makeplot = 1

		;**** Construct graph title ****
		;**** "!C" is the new line control. ****
  !p.font=-1
  if (small_check eq 'YES') then $
    gtitle =  title(0) + "!C!C" + title(1) + "!C!C" + title(2) + "!C!C" + $
              title(3) + "!C!C" + title(4) $
  else $
    gtitle =  title(0) + "!C!C" + title(1) + "!C" + title(2) + "!C" + $
              title(3) + "!C" + title(4)
  

		;**** Find x and y ranges for auto scaling ****
 
  if (ldef1 eq 0) then begin
		
                ;**** identify values in the valid range ****
		;**** plot routines only work within ****
		;**** single precision limits.	     ****
                
    xvals = where(x gt 1.0e-37 and x lt 1.0e37)
    if (gr_value eq 4) then begin
      yvals = where(y(0,*,*) gt 1.0e-37 and y(0,*,*) lt 1.0e37)
      if (yvals(0) gt -1) then begin
        ytemp = reform(y(0,*,*))
        ymax1 = max(ytemp(yvals), min=ymin1)
      endif
      
      yvals = where(y(1,*,*) gt 1.0e-37 and y(1,*,*) lt 1.0e37)
      if (yvals(0) gt -1) then begin
        ytemp = reform(y(1,*,*))
        ymax2 = max(ytemp(yvals), min=ymin2)
      endif
      ymax = ymax2 > ymax1
      ymin = ymin2 < ymin1
      makeplot = 0
    endif else begin
      yvals = where(y(pl_value,*,*) gt 1.0e-37 and y(pl_value,*,*) lt 1.0e37)
      if (yvals(0) gt -1) then begin
        ytemp = reform(y(pl_value,*,*))
        ymax = max(ytemp(yvals), min=ymin)
        makeplot = 1
      end else makeplot = 0
    endelse  
       
    if (xvals(0) gt -1) then begin
      xmax = max(x(xvals), min=xmin)
      makeplot = 1
    end else makeplot = 0
    style = 0
  end else begin
  
		;Check that at least some data in in axes range ***
    xvals = where(x ge xmin and x le xmax)
    yvals = where(y(pl_value,*,*) ge ymin and y(pl_value,*,*) le ymax)
    if (xvals(0) eq -1 or yvals(0) eq -1) then begin
      makeplot = 0
    endif 
    style = 1
  endelse
  
		;**** Set up log-log plotting axes ****
  plot_oo, [xmin,xmax], [ymin,ymax], /nodata, ticklen=1.0, $
	   position=[left,bottom,grright,grtop], $
	   xtitle=xtitle, ytitle=ytitle, xstyle=style, ystyle=style, $
	   charsize=charsize
 
  loop:
  
    if makeplot eq 1 then begin

		;**********************
		;****  Make plots  ****
		;**********************
      case nplots of 

	1 : oplot, x(0,0:nener-1), y(pl_value,0,0:nener-1), psym=1
	3 : begin
	      oplot, x(0,0:nener-1), y(pl_value,0,0:nener-1), psym=1
	      oplot, x(1,0:npspl-1), y(pl_value,1,0:npspl-1)
            end
        5 : begin
              oplot, x(0,0:nener-1), y(pl_value,0,0:nener-1), psym=1
              oplot, x(2,0:nmx-1), y(pl_value,2,0:nmx-1),linestyle=5
	    end
        7 : begin
              oplot, x(0,0:nener-1), y(pl_value,0,0:nener-1), psym=1
	      oplot, x(1,0:npspl-1), y(pl_value,1,0:npspl-1)
              oplot, x(2,0:nmx-1), y(pl_value,2,0:nmx-1), linestyle=5
	    end
        else : print, "ADAS809 : NO PLOTS"

      endcase        

    endif else print, "ADAS809 : No data found in these axes ranges"
    if (gr_value eq 4) then begin
      gr_value = 0
      pl_value = 1
      goto, loop
    endif
 
		;**** plot title ****
  xyouts, (left-0.05), (top+0.05), gtitle, /normal, alignment=0.0, $
	  charsize=charsize*0.95

END
