; SCCS INFO Module: @(#)$Header: /home/adascvs/idl/adas8xx/adas808/h8grev.pro,v 1.1 2004/07/06 14:01:57 whitefor Exp $  Data $Date: 2004/07/06 14:01:57 $
pro h8grev,event

        widget_control,event.id,get_value=a
        wset,a

        widget_control,event.id,get_uvalue=localstate
        widget_control,localstate.first_child,get_uvalue=state

        a=convert_coord([event.x,event.y],/device,/to_data)
        xdata=a[0]
        ydata=a[1]
        xscr=event.x
        yscr=event.y

        if event.type eq 2 then begin
                ; mouse movement
                ;print,xdata,ydata,xscr,yscr
                if localstate.moving[0] gt -1 then begin
                        ;print,xdata,ydata,xscr,yscr
                
                        if localstate.moving[1] eq 1 then widget_control,state.select_lmin[localstate.moving[0]],set_value=strtrim(string(xdata),2)
                        if localstate.moving[2] eq 1 then widget_control,state.select_lmax[localstate.moving[0]],set_value=strtrim(string(xdata),2)
                        if localstate.moving[3] eq 1 then widget_control,state.select_cmin[localstate.moving[0]],set_value=strtrim(string(ydata),2)

                ;stop


                        h8plot,state.previewdata,state=state,dashedbox=localstate.moving
                        h8splt,state.previewdata,state

                
                endif
                
                if localstate.line[0] gt -1 then begin
                        print,xdata,ydata
                        h8plot,state.previewdata,state=state,line=[localstate.line[0],localstate.line[1],xdata,ydata]
                
                endif
                
                
                
                return
        endif

        widget_control,state.select_but[0],get_uvalue=moving

        print,moving

        if event.type eq 0 and localstate.moving[0] eq -1 and event.press eq 1 then begin

                widget_control,state.select_lmin[moving],get_value=a
                lmin=float(a)
                widget_control,state.select_lmax[moving],get_value=a
                lmax=float(a)
                widget_control,state.select_cmin[moving],get_value=a
                cmin=float(a)
                a=convert_coord([lmin,cmin],/data,/to_device)
                lmin_scr=a[0]
                cmin_scr=a[1]
                lmax_scr=(convert_coord([lmax,cmin],/data,/to_device))[0]
                print,lmin_scr,lmax_scr,cmin_scr
                
                lhd=abs(lmin_scr-event.x)
                rhd=abs(lmax_scr-event.x)
                cbd=abs(cmin_scr-event.y)
                
                if cbd lt 20 then movec=1 else movec=0
                if lhd lt 20 then movel=1 else movel=0
                if rhd lt 20 then mover=1 else mover=0
                
                if movel eq 1 and mover eq 1 then begin
                        if lhd lt rhd then begin
                                mover=0
                        endif else begin
                                movel=0
                        endelse
                endif
                
                h8plot,STATE.PREVIEWDATA,STATE=STATE,dashedbox=[moving,movel,mover,movec]
                                
                widget_control,event.id,set_uvalue={first_child:localstate.first_child,moving:[moving,movel,mover,movec],line:[-1.0,-1.0]}
                
                print,lhd,rhd,cbd
                
;                stop 
        
        
        endif

        if event.type eq 0 and localstate.line[0] eq -1.0 and (event.press eq 2 or event.press eq 4) then begin
                localstate.line[0]=xdata
                localstate.line[1]=ydata
                widget_control,event.id,set_uvalue=localstate

        end

        if event.type eq 1 and localstate.line[0] gt -1.0 and (event.release eq 2 or event.release eq 4) then begin
                
                print,localstate.line[0],xdata
                
                minx=min([localstate.line[0],xdata])
                maxx=max([localstate.line[0],xdata])
                
                print,minx,maxx
                
                widget_control,state.select_lmin[moving],set_value=strtrim(string(minx),2)
                widget_control,state.select_lmax[moving],set_value=strtrim(string(maxx),2)
                widget_control,state.select_cmin[moving],set_value=strtrim(string((ydata+localstate.line[1])/2.),2)
        
                widget_control,event.id,set_uvalue={first_child:localstate.first_child,moving:[-1,-1,-1,-1],line:[-1.0,-1.0]}
                h8plot,state.previewdata,state=state
                h8splt,state.previewdata,state
        end



        if event.type eq 1 and localstate.moving[0] gt -1 and event.release eq 1 then begin
                print,xdata,ydata
                
                
                widget_control,event.id,set_uvalue={first_child:localstate.first_child,moving:[-1,-1,-1,-1],line:[-1.0,-1.0]}
                
                if localstate.moving[1] eq 1 then widget_control,state.select_lmin[localstate.moving[0]],set_value=strtrim(string(xdata),2)
                if localstate.moving[2] eq 1 then widget_control,state.select_lmax[localstate.moving[0]],set_value=strtrim(string(xdata),2)
                if localstate.moving[3] eq 1 then widget_control,state.select_cmin[localstate.moving[0]],set_value=strtrim(string(ydata),2)

                ;stop


                h8plot,state.previewdata,state=state
                h8splt,state.previewdata,state

        endif


        if event.type eq 1 then begin
        
        
        endif


        ;stop


end
