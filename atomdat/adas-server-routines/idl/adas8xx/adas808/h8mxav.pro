; SCCS INFO Module: @(#)$Header: /home/adascvs/idl/adas8xx/adas808/h8mxav.pro,v 1.1 2004/07/06 14:02:26 whitefor Exp $  Data $Date: 2004/07/06 14:02:26 $
pro h8mxav,inegrid,incollstr,z0,z,zeff,type,avalue,wi,wj,ei,ej,temperature,effcoll,fortdir

        openw,unit,'input.txt',/get_lun

        a=where(incollstr gt 0)

        if a[0] eq -1 then begin
                effcoll=dblarr((size(temperature))[1])
                return
        end

        egrid=inegrid[a]
        collstr=incollstr[a]
        

        printf,unit,z0,z,zeff,type
        printf,unit,avalue,wi,wj,ei,ej
        printf,unit,(size(egrid))[1],(size(temperature))[1]
        for i=0,(size(egrid))[1]-1 do begin
                printf,unit,egrid[i]/abs(ei-ej),collstr[i]
        end
        for i=0,(size(temperature))[1]-1 do begin
                printf,unit,temperature[i]
        end

        free_lun,unit

        spawn,fortdir+'/808_mxav.out'

        openr,unit,'output.txt',/get_lun
        
        effcoll=dblarr((size(temperature))[1])
        
        a=''
        for i=0,(size(temperature))[1]-1 do begin
                readf,unit,a
                effcoll[i]=double(a)
        end
        
        free_lun,unit
        
        spawn,'rm -f input.txt output.txt'
end

