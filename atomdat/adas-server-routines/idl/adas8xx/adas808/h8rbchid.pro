; SCCS INFO Module: @(#)$Header: /home/adascvs/idl/adas8xx/adas808/h8rbchid.pro,v 1.1 2004/07/06 14:02:41 whitefor Exp $  Data $Date: 2004/07/06 14:02:41 $
pro h8rbchid,z,xi,zeta,tev, sbchid

;  evaluates a shell contribution to the ionisation rate coefficient
;  in the burgess-chidichimo approximation  mnras(1983)203,1269.
;      z=target ion charge number
;      xi=effective ionisation potential for shell (ryd)
;      zeta=effective number of equivalent electrons in shell
;      te=electron temperature (k)
; IDL version of the adaslib routine rbchid,for
;
; date:    6th July 2002
;
; version: 1.1				date: 06-06-02
;
; author: S.D. Loch

ih=13.606e0
c = 2.3e0
beta = 0.25e0*(((100.e0*z+91)/(4.e0*z+3.e0))^(0.5e0)-5.e0)
w=(alog(1.e0+tev/xi))^(beta/(1+tev/xi))
sbchid=(2.1715e-8)*c*zeta*(ih/xi)^(1.5e0)*(xi/tev)^(0.5e0)*expint(1,xi/tev)*w

end
