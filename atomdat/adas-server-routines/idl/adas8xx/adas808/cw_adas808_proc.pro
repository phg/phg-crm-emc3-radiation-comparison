; SCCS INFO Module: @(#)$Header: /home/adascvs/idl/adas8xx/adas808/cw_adas808_proc.pro,v 1.1 2004/07/06 12:55:59 whitefor Exp $  Data $Date: 2004/07/06 12:55:59 $
;+
; PROJECT:
;       ADAS 
;
; NAME:
;	CW_ADAS216_PROC()
;
; PURPOSE:
;	Produces a widget for ADAS216 processing options/input.
;
; EXPLANATION:
;	This function creates a compound widget consisting of a text
;	widget holding an editable 'Run title', the dataset name/browse
;	widget cw_adas_dsbr, label widgets with charge information for
;	the input data, a table widget for temperature data cw_adas_table,
;	a second cw_adas_table widget for density data, buttons to
;	enter default values into the temperature and density tables,
;	a multiple selection widget cw_adas_sel, a reaction settings
;	widget cw_adas_reac2, a message widget, a 'Cancel' button and
;	finally a 'Done' button.
;
;	The compound widgets included in this widget are self managing.
;	This widget only manages events from the two 'Defaults' buttons,
;	the 'Cancel' button and the 'Done' button.
;
;	This widget only generates events for the 'Done' and 'Cancel'
;	buttons.
;
; USE:
;	This widget is specific to ADAS216, see adas216_proc.pro
;	for use.
;
; INPUTS:
;	TOPPARENT- Long integer, ID of parent widget.
;
;       BITFILE - String; the path to the directory containing bitmaps
;                 for the 'escape to series menu' button.
;
;	INVAL	- Structure; the input screen structure - see cw_adas216_in.pro
;
;
; OPTIONAL INPUTS:
;	None
;
; OUTPUTS:
;	The return value of this function is the ID of the declared widget.
;
; OPTIONAL OUTPUTS:
;	None
;
; KEYWORD PARAMETERS:
;	VALUE	- A structure which determines the initial settings of
;		  the processing options widget.
;
;		  The default VALUE is created thus;
;
;			imetr = make_array(ndmet,/int,value=0)
;			temp = dblarr(ndtem)
;			dens = dblarr(ndden)
;			ps = { $
;		 	TITLE:'',	NMET:0, $
;			IMETR:imetr,	IFOUT:1, $
;			MAXT:0,		TINE:temp, $
;			TINP:temp,	TINH:temp, $
;			IDOUT:1,	MAXD:0, $
;			DINE:dens,	DINP:dens, $
;			RATHA:dens,	RATPIA:dens, $
;			ZEFF:'',	LPSEL:0, $
;			LZSEL:0,	LISEL:0, $
;			LHSEL:0,	LRSEL:0, $
;                       LIOSEL:0,       LNSEL:0, $
;			LNORM:0,		 $
;			VAL502:struct502,	 $
;			WVLS:0.0,	WVLL:0.0,$
;			AVLT:0.0  }
;
; 		TITLE	entered general title for program run
; 		NMET	number of metastables
; 		(IMETR(I),I=1,NMET)	index of metastables
; 		IFOUT	input temperature units (1,2,3)
; 		MAXT	number of input temperatures (1-20)
; 		(TINE(I),I=1,MAXT)	electron temperatures
; 		(TINP(I),I=1,MAXT)	proton temperatures
; 		(TINH(I),I=1,MAXT)	neutral hydrogen temperatures
; 		IDOUT	input density units (1,2)
; 		MAXD	number of input densities
; 		(DINE(I),I=1,MAXD)	electron densities
; 		(DINP(I),I=1,MAXD)	proton densities
; 		(RATHA(I),I=1,MAXD)	ratio (neut h dens/elec dens)
; 		(RATPIA(I),I=1,MAXD)	ratio (n(z+1)/n(z) stage abund)
; 		ZEFF	plasma z effective, a string holding a valid number.
; 		LPSEL	include proton collisions?
; 		LZSEL	scale proton collisions with plasma z effective?
; 		LISEL	include ionisation rates?
; 		LHSEL	include charge transfer from neutral hydrogen?
; 		LRSEL	include free electron recombination?
;               LIOSEL to be determined
;               LNSEL   include projected bundle-n data?
;
;		All of the above structure elements map onto variables of
;		the same name in the ADAS216 FORTRAN program.
;
;
;	UVALUE	- A user value for the widget. Default 0.
;
;	FONT_LARGE - The name of a larger font.  Default current system
;		     font
;
;	FONT_SMALL - The name of a smaller font. Default current system
;		     font.
;
;	EDIT_FONTS - A structure of two fonts used in the table editor
;		     adas_edtab.pro, {font_norm:'',font_input:''}.  Defaults
;		     to current system font.
;
;	NUM_FORM   - String; Numeric format to use in tables.  Default
;			'(E10.3)'
;		     
;
; CALLS:
;	POPUP		 Popup warning window with buttons.
;	XXDEFT		 Returns default temperature table.
;	XXDEFD		 Returns default density table.
;	CW_ADAS_DSBR	 Dataset name and comments browsing button.
;	CW_ADAS_TABLE	 Adas data table widget.
;	CW_ADAS_SEL	 Adas mulitple selection widget.
;	CW_ADAS208_REAC2 Reaction settings for ADAS216.
;
;	See side effects for other related widget management routines.
;
; SIDE EFFECTS:
;
;	The following widget management routines are included in this
;	file;
;	PROC216_GET_VAL()	Returns the current VALUE structure.
;	PROC216_EVENT()		Process and issue events.
;
; CATEGORY:
;	Compound Widget
;
; WRITTEN:
;       Martin O'Mullane
;
; MODIFIED:
;	1.1	Martin O'Mullane
;		First release, based on cw_adas208_proc.pro
;
; VERSION:
;       1.1	17-03-99
;
;-
;-----------------------------------------------------------------------------

FUNCTION proc808_get_val, id


                ;**** Return to caller on error ****
  ;ON_ERROR, 2

		 ;***************************************
                 ;****     Retrieve the state 	     ****
		 ;**** Get first_child widget id     ****
		 ;**** because state is stored there ****
		 ;***************************************

  first_child = widget_info(id, /child)
  widget_control, first_child, get_uvalue = state

  ;widget_control,state.txt1, get_value=temp
  ;temp=strtrim(temp,2)
  ;nuc_ch=fix(temp(0))
  ;widget_control,state.txt2, get_value=temp
  ;temp=strtrim(temp,2)
  ;el_sym=string(temp(0))
  ;widget_control,state.txt3, get_value=temp
  ;temp=strtrim(temp,2)
  ;ion_min=fix(temp(0))
  ;widget_control,state.txt4, get_value=temp
  ;temp=strtrim(temp,2)
  ;ion_max=fix(temp(0))
  
  ;widget_control,state.txt1_rul_2, get_value=temp
  ;temp=strtrim(temp,2)
  ;nmaxdn=fix(temp(0))
  ;widget_control, state.txt2_rul_2, get_value=temp
  ;temp=strtrim(temp,2)
  ;nmindn=fix(temp(0))
  ;widget_control, state.txt1_rul_3, get_value=temp
  ;temp=strtrim(temp,2)
  ;nmaxl=fix(temp(0))
  ;widget_control, state.txt2_rul_3, get_value=temp
  ;temp=strtrim(temp,2)
  ;nminl=fix(temp(0))
  
  ;widget_control, state.val_buts, get_value=sel_but
  ;widget_control,state.ntxt1_rul_2, get_value=temp
  ;temp=strtrim(temp,2)
  ;nmaxdn_1=fix(temp(0))
  ;widget_control,state.ntxt2_rul_2, get_value=temp
  ;temp=strtrim(temp,2)
  ;nmindn_1=fix(temp(0))
  ;widget_control,state.ntxt1_rul_3, get_value=temp
  ;temp=strtrim(temp,2)
  ;nmaxl_1=fix(temp(0))
  ;widget_control,state.ntxt2_rul_3, get_value=temp
  ;temp=strtrim(temp,2)
  ;nminl_1=fix(temp(0))
  ;widget_control,state.ntxt1_rul_4, get_value=temp
  ;temp=strtrim(temp,2)
  ;nmaxdn_2=fix(temp(0))
  ;widget_control,state.ntxt2_rul_4, get_value=temp
  ;temp=strtrim(temp,2)
  ;nmindn_2=fix(temp(0))
  ;widget_control,state.ntxt1_rul_5, get_value=temp
  ;temp=strtrim(temp,2)
  ;nmaxl_2=fix(temp(0))
  ;widget_control,state.ntxt2_rul_5, get_value=temp
  ;temp=strtrim(temp,2)
  ;nminl_2=fix(temp(0))

  ;widget_control, state.but_prom, get_value=iprom
  ;widget_control,state.txt_prom_2, get_value=temp
  ;temp=strtrim(temp,2)
  ;iprom_low=fix(temp(0))
  ;widget_control,state.txt_prom_3, get_value=temp
  ;temp=strtrim(temp,2)
  ;iprom_high=fix(temp(0))
  

  ps = {	proc808_set,					$
		new       	:       0,		$
		title     	:       '',		$
		nuc_ch	  	:	0,		$
		el_sym	  	:       '',		$
		ion_min	  	:       0,		$
		ion_max	  	:       0,		$
		nmindn		:	0,           $
		nmaxdn		:	0,           $
		nmaxl		:	 0,          $
		nminl		:	 0,          $
		nmindn_1	:	0,         $
		nmaxdn_1	:	0,         $
		nmaxl_1		:	0,          $
		nminl_1		:	0,          $
		nmindn_2	:	0,         $
		nmaxdn_2	:	0,         $
		nmaxl_2		:	0,          $
		nminl_2		:	 0,         $
		iprom		:	 0,         $
		sel_but		:	0}

    widget_control, first_child, set_uvalue=state;, /no_copy


        ;stop



        calculate_config=state.manual_split
        configuration=state.prom_config
        nconf=intarr(100)
        
        for ion_ch=state.ion_min,state.ion_max do begin
                if state.gotpreviewdata[ion_ch] gt 0 then begin 
                        data=*(state.previewdata[ion_ch])
                        for i=0,(size(state.select_onoff))[1]-1 do begin
                                widget_control,state.select_cmin[i],get_value=a
                                cmin=double(a[0])
                                widget_control,state.select_lmin[i],get_value=a
                                lmin=double(a[0])
                                widget_control,state.select_lmax[i],get_value=a
                                lmax=double(a[0])
                
                                for j=0,(size(data.aval))[0] eq 0 ? 0:(size(data.aval))[1]-1 do begin
                                        if data.aval[j] gt cmin and data.wavel[j] gt lmin and data.wavel[j] lt lmax then begin
                                                calculate_config[ion_ch,data.conf1[j]]=1        
                                                calculate_config[ion_ch,data.conf2[j]]=1        
                                        endif
                                end
        
        
                        end
                endif
        
                a=where(configuration[ion_ch,*] ne '')
                if a[0] eq -1 then begin
                        nconf[ion_ch]=0
                endif else begin
                        nconf[ion_ch]=(size(a))[1]
                endelse
        end
        

        nints=0
        lmin=double(0)
	lmax=double(0)
	 
        for i=0,(size(state.select_onoff))[1]-1 do begin
                widget_control,state.select_onoff[i],get_uvalue=onoff
                if onoff eq 1 then begin
                        widget_control,state.select_lmin[i],get_value=a
                        widget_control,state.select_lmax[i],get_value=b
                        if nints eq 0 then begin
                                lmin=double(a[0])
                                lmax=double(b[0])
                        endif else begin
                                lmin=[lmin,double(a[0])]
                                lmax=[lmax,double(b[0])]
                        endelse
                        nints=nints+1
                endif
        
        end
        
        
        
        ;end
        

       ; stop

        data={configuration:configuration,calculate_config:calculate_config, $
                ion_min:state.ion_min,ion_max:state.ion_max, $
                el_sym:state.el_sym,iz0:state.iz0,nconf:nconf, $
                parity:state.prom_pars,lastl:state.prom_lastl, $
                  te_04:state.te_04, $
                  nte_04:state.nte_04, $
                  te_unit_04:state.te_unit_04, $
                  te_11:state.te_11, $
                  nte_11:state.nte_11, $
                  te_unit_11:state.te_unit_11, $
                  te_15:state.te_15, $
                  nte_15:state.nte_15, $
                  te_unit_15:state.te_unit_15, $
                  lmin:lmin,lmax:lmax,nints:nints, $
                  ionpot:state.ionpot, $
		  atomdata:state.atomdata, $
		  gotatomdata:state.gotatomdata, $
		  fortdir:state.fortdir, $
                ioccup_ionsn_grnds:state.ioccup_ionsn_grnds,$
                ioccup_ionsn_ea_grnds:state.ioccup_ionsn_ea_grnds,$
                ioccup_ionsn_dirct:state.ioccup_ionsn_dirct ,$
                ioccup_ionsn_ea: state.ioccup_ionsn_ea,$
                dirct_parity:state.dirct_parity,$
                grnd_indx:state.grnd_indx,$
                icount_dirct:state.icount_dirct,$
                ea_indx_set:state.ea_indx_set,$
                icount_ea:state.icount_ea,$
                dirct_lbl:state.dirct_lbl,$
                ea_grnd_lbl:state.ea_grnd_lbl,$
                ea_ex_lbl:state.ea_ex_lbl}

  RETURN, {ps:ps,data:data}

END

;-----------------------------------------------------------------------------

FUNCTION proc808_event, event

                ;**** Base ID of compound widget ****
                
   parent=event.handler

		;**********************************************
                ;**** Retrieve the user value state        ****
		;**** Get id of first_child widget because ****
		;**** user value "state" is stored there   ****
		;**********************************************

   first_child = widget_info(parent, /child)
   widget_control, first_child, get_uvalue=state;,/no_copy

        ;stop


		;*********************************
		;**** Clear previous messages ****
		;*********************************
                
;  widget_control,state.messid,set_value=' '
;  whoever wrote the above line deserves to be strung up!


                ;************************
                ;**** Process events ****
                ;************************
      
  FOR I=0,(SIZE(STATE.SELECT_ONOFF))[1]-1 DO BEGIN    
        IF EVENT.ID EQ STATE.SELECT_ONOFF[I] THEN BEGIN
                WIDGET_CONTROL,EVENT.ID,SET_UVALUE=EVENT.SELECT
                WIDGET_CONTROL,STATE.SELECT_BASE[I],SENSITIVE=EVENT.SELECT
                H8PLOT,STATE.PREVIEWDATA,STATE=STATE
                H8SPLT,STATE.PREVIEWDATA,STATE
        ENDIF
        IF EVENT.ID EQ STATE.SELECT_LMIN[I] OR EVENT.ID EQ STATE.SELECT_LMAX[I] OR EVENT.ID EQ STATE.SELECT_CMIN[I] THEN BEGIN
                H8PLOT,STATE.PREVIEWDATA,STATE=STATE
                H8SPLT,STATE.PREVIEWDATA,STATE                        
        ENDIF
        IF EVENT.ID EQ STATE.SELECT_BUT[I] THEN BEGIN
                FOR J=0,(SIZE(STATE.SELECT_ONOFF))[1]-1 DO BEGIN
                        IF I NE J THEN WIDGET_CONTROL,STATE.SELECT_BUT[J],SET_BUTTON=0
                END
                WIDGET_CONTROL,STATE.SELECT_BUT[0],SET_UVALUE=I
                H8PLOT,STATE.PREVIEWDATA,STATE=STATE
                H8SPLT,STATE.PREVIEWDATA,STATE
        ENDIF
        
          
  END    

  FOR I=0,(SIZE(STATE.CONFIG_SPLIT_BUT))[1]-1 DO BEGIN
        IF EVENT.ID EQ STATE.CONFIG_SPLIT_BUT[I] OR EVENT.ID EQ STATE.CONFIG_SPLIT_MAN[I]THEN BEGIN
                WIDGET_CONTROL,EVENT.ID,SET_UVALUE=EVENT.SELECT
	        WIDGET_CONTROL, STATE.TXT_CONF1,GET_VALUE=TEMP
	        ION_CH=FIX(TEMP(0))
                TOTAL=0
                FOR J=0,(SIZE(STATE.PROM_NLEV))[2]-1 DO BEGIN
                        WIDGET_CONTROL,STATE.CONFIG_SPLIT_BUT[J],GET_UVALUE=ONOFF
                        IF ONOFF EQ 1 THEN TOTAL=TOTAL+STATE.PROM_NLEV[ION_CH,J]
                        IF ONOFF EQ 0 THEN BEGIN
                                WIDGET_CONTROL,STATE.CONFIG_SPLIT_MAN[J],GET_UVALUE=ONOFF
                                IF ONOFF EQ 1 THEN TOTAL=TOTAL+STATE.PROM_NLEV[ION_CH,J]
                        ENDIF
                END
                
                WIDGET_CONTROL,STATE.TXT_BASE_NIONSEL,SET_VALUE=STRTRIM(STRING(TOTAL),2)                
        
        
                IF EVENT.ID EQ STATE.CONFIG_SPLIT_MAN[I] THEN BEGIN
                        STATE.MANUAL_SPLIT[ION_CH,I]=EVENT.SELECT
                ENDIF
                
                h8cplt,state
        
        ENDIF
  END    


shl_lab=['1s','2s','2p','3s','3p','3d','4s','4p','4d','4f','5s','5p',$
 	 '5d','5f','5g','6s','6p','6d','6f','6g','6h','7s','7p','7d','7f','7g',$
	 '7h','7i','8s','8p','8d','8f','8g','8h','8i','8k']


	shl_lval=[0,0,1,0,1,2,0,1,2,3,0,1,2,3,4,0,1,2,3,4,5,$
	0,1,2,3,4,5,6,0,1,2,3,4,5,6,7]


        for i=0,(size(state.config_label))[1]-1 do begin
                IF EVENT.ID EQ STATE.CONFIG_label[I] then begin
                        widget_control, state.txt_conf1,get_value=temp
	                if temp[0] eq '' or temp[0] eq ' ' or temp[0] eq '  ' then begin
				ion_ch=-1		
			endif else begin
				ion_ch=fix(temp(0))
			endelse
                        
                        if ion_ch ne -1 then begin
                
                        widget_control,event.id,get_value=toparse
                        toparse=toparse[0]
                        
                        if toparse eq '' then begin
                                state.ioccup_store[ion_ch,i,*]=dblarr((size(state.ioccup_store))[3])
                        endif

                        if toparse ne '' then begin
                                toparse=strlowcase(toparse)
                                
                                state.prom_config[ion_ch,i]=toparse
                                state.ioccup_store[ion_ch,i,*]=state.ioccup_store[ion_ch,0,*]
                                
                                print,toparse
                                for j=0,35 do begin
                                        position=strpos(toparse,shl_lab[j])
                                        if position ne -1 then begin
                                                a=strmid(toparse,position+2,2)
                                                state.ioccup_store[ion_ch,i,j]=fix(a)
                                                state.prom_lastl[ion_ch]=shl_lval[j]
                                        endif
                                end
                                state.prom_nlev[ion_ch,i]=h8nlev(state.ioccup_store[ion_ch,i,*])
                                state.prom_pars[ion_ch,i]=(-1)^(total(reform(state.ioccup_store[ion_ch,i,*])*shl_lval))

                        endif

         
                                ; rebuild configurations based on ioccup
                                
                        for j=0,99 do begin
                                if total(state.ioccup_store[ion_ch,j,*]) eq 0d0 and j le 98 then begin
                                        state.ioccup_store[ion_ch,j,*]=state.ioccup_store[ion_ch,j+1,*]
                                        STATE.PROM_NLEV[ion_ch,j]=STATE.PROM_NLEV[ion_ch,j+1]
                                        STATE.MANUAL_SPLIT[ion_ch,j]=STATE.MANUAL_SPLIT[ion_ch,j+1]
                                        STATE.prom_pars[ion_ch,j]=STATE.prom_pars[ion_ch,j+1]
                                        STATE.PROM_LASTL[ion_ch,j]=STATE.PROM_LASTL[ion_ch,j+1]
                                        
                                        state.ioccup_store[ion_ch,j+1,*]=dblarr((size(state.ioccup_store))[3])
                                        STATE.PROM_NLEV[ion_ch,j+1]=0
                                        STATE.MANUAL_SPLIT[ION_CH,j+1]=0
                                        STATE.prom_pars[ION_CH,j+1]=0
                                        STATE.PROM_LASTL[ION_CH,j+1]=0
                                endif
                        
                        end
                        
                        for j=99,0,-1 do begin
                                state.prom_config[ion_ch,j]=""
                                if total(state.ioccup_store[ion_ch,j,*]) eq 0d0 then begin
                                        numbercfgs=j
                                endif
                        end
                        
                                                                
                        
                        
                        lowestactive=-1
                        for k=0,(size(state.ioccup_store))[3]-1 do begin
                                extract=state.ioccup_store[ion_ch,0:numbercfgs-1,k]
                                ;print,extract
                                for l=0,numbercfgs-2 do begin
                                        if extract[l] ne extract[l+1] and lowestactive eq -1 then begin
                                                lowestactive=k
                                                ;print,k
                                                if total(state.ioccup_store[ion_ch,0:numbercfgs,*]) eq 0 then begin
                                                        lowestactive=-1
                                                endif
                                        endif     
                                
                                end
                        end
                        
                        for j=0,numbercfgs-1 do begin
                                        for k=lowestactive,(size(state.ioccup_store))[3]-1 do begin
                                                if state.ioccup_store[ion_ch,j,k] ne 0 then begin
                                                        occup=state.ioccup_store[ion_ch,j,k]
                                                        soccup=strtrim(string(occup),2)
                                                        if occup le 9 then soccup=soccup+" "
                                                        state.prom_config[ion_ch,j]=state.prom_config[ion_ch,j]+" "+shl_lab[k]+soccup
                                                endif
                                        end
                                        state.prom_config[ion_ch,j]=strtrim(state.prom_config[ion_ch,j],1)
                        end
 
                        state.nconf[ion_ch]=numbercfgs
                        
                        FOR j=0,(SIZE(state.prom_config))[2]-1 DO BEGIN
                                WIDGET_CONTROL,STATE.CONFIG_LABEL[j],SET_VALUE=STRTRIM(state.prom_config[ION_CH,j],2)
                                WIDGET_CONTROL,STATE.CONFIG_NLEVS[j],SET_VALUE=STRTRIM(STRING(STATE.PROM_NLEV[ION_CH,j]),2)
                                WIDGET_CONTROL,STATE.CONFIG_SPLIT_MAN[j],SET_BUTTON=STATE.MANUAL_SPLIT[ION_CH,j]
                                WIDGET_CONTROL,STATE.CONFIG_SPLIT_MAN[j],SET_UVALUE=STATE.MANUAL_SPLIT[ION_CH,j]
                        END

                        total=0
                        for j=0,(size(state.prom_nlev))[2]-1 do begin
                                total=total+state.prom_nlev[ion_ch,j]
                        end

                        widget_control,state.txt_base_nion,set_value=strtrim(string(total),2)

                        
                        numelec=state.iz0-ion_ch
                        
                        for j=0,numbercfgs-1 do begin
                                if total(state.ioccup_store[ion_ch,j,*]) ne numelec then begin
                                        a=dialog_message('Wrong number of electrons in configuration')
                                endif
                        end

                        ;stop

                        ;stop
                        
                        ;stop

                        
                        
                        
                        
                        endif
                
                
                endif
        end

                
  CASE event.id OF

        ;STATE.STOPID: BEGIN
        ;        stop
       ; 
       ; END

		;*************************************
		;**** Element symbol text input   ****
		;*************************************
   STATE.RUN_ENERGY:BEGIN

   	widget_control,hourglass=1

  	
	for ion_ch=state.ion_min,state.ion_max do begin
                if state.nconf(ion_ch) gt 0 then begin
			checksum=long(1)
			for i=0,35 do begin
				checksum=checksum+total(state.ioccup_store[ion_ch,*,i])*(i+1)
			end
			
			;save,state,file='allan123.dat'
                        ;stop
                        if STATE.GOTATOMDATA[ION_CH] ne checksum then begin
				h8evca,state,ion_ch,ATOMDATA,/bessel
				STATE.ATOMDATA[ION_CH]=PTR_NEW(ATOMDATA)
				STATE.GOTATOMDATA[ION_CH]=checksum
			endif
                endif
;                print,strtrim(string(100.*(ion_ch-state.ion_min)/(state.ion_max-state.ion_min)),2)+'%'
        end

	tev=4411.
	edens=1e14

	h8ionbal,state,baldata,tev,edens

        ;save,state,file='allan123.dat'
        ;stop

	for ion_ch=state.ion_min,state.ion_max do begin
                if state.nconf(ion_ch) gt 0 then begin
;				h8wr04,state
;				print,'adw!!!'
				h8crsl,state,ion_ch,data,tev,edens
;				print,'adw!!!'
                                STATE.PREVIEWDATA[ION_CH]=PTR_NEW(DATA)
				STATE.GOTPREVIEWDATA[ION_CH]=1

		endif
	end


	
   	widget_control,hourglass=0
   	widget_control,event.top,set_uvalue=state
   	H8PLOT,STATE.PREVIEWDATA,STATE=STATE
   
   END
   
;   STATE.RUN_AVALUE:BEGIN
;        PRINT,'AVALUE'
;   END
   
;   STATE.REPLOT: BEGIN
   STATE.CMIN:BEGIN
        H8PLOT,STATE.PREVIEWDATA,STATE=STATE
        H8SPLT,STATE.PREVIEWDATA,STATE
   END
   STATE.CMAX:BEGIN
        H8PLOT,STATE.PREVIEWDATA,STATE=STATE
        H8SPLT,STATE.PREVIEWDATA,STATE
   END
   STATE.LMIN:BEGIN
        H8PLOT,STATE.PREVIEWDATA,STATE=STATE
        H8SPLT,STATE.PREVIEWDATA,STATE
   END
   STATE.LMAX:BEGIN
        H8PLOT,STATE.PREVIEWDATA,STATE=STATE
        H8SPLT,STATE.PREVIEWDATA,STATE
   END
   STATE.CNT_XMIN:BEGIN
        H8CPLT,STATE
   END
   STATE.CNT_XMAX:BEGIN
        H8CPLT,STATE
   END
   STATE.CNT_YMIN:BEGIN
        H8CPLT,STATE
   END
   STATE.CNT_YMAX:BEGIN
        H8CPLT,STATE
   END
 
   STATE.SEL_LEV_BUT:BEGIN
        WIDGET_CONTROL,STATE.SEL_LEV_BUT,SET_UVALUE=0
        H8CPLT,STATE
   END
   STATE.TOT_LEV_BUT:BEGIN
        WIDGET_CONTROL,STATE.SEL_LEV_BUT,SET_UVALUE=1
        H8CPLT,STATE
   END
   STATE.SEL_CFG_BUT:BEGIN
        WIDGET_CONTROL,STATE.SEL_LEV_BUT,SET_UVALUE=2
        H8CPLT,STATE
   END
   STATE.TOT_CFG_BUT:BEGIN
        WIDGET_CONTROL,STATE.SEL_LEV_BUT,SET_UVALUE=3
        H8CPLT,STATE
   END

   STATE.YLOGON: BEGIN
        WIDGET_CONTROL,STATE.YLOGON,SET_UVALUE=1
        H8PLOT,STATE.PREVIEWDATA,STATE=STATE
        H8SPLT,STATE.PREVIEWDATA,STATE
   END
   STATE.YLOGOFF: BEGIN
        WIDGET_CONTROL,STATE.YLOGON,SET_UVALUE=0
        H8PLOT,STATE.PREVIEWDATA,STATE=STATE
        H8SPLT,STATE.PREVIEWDATA,STATE
   END
   STATE.XLOGON: BEGIN
        WIDGET_CONTROL,STATE.XLOGON,SET_UVALUE=1
        H8PLOT,STATE.PREVIEWDATA,STATE=STATE
        H8SPLT,STATE.PREVIEWDATA,STATE
   END
   STATE.XLOGOFF: BEGIN
        WIDGET_CONTROL,STATE.XLOGON,SET_UVALUE=0
        H8PLOT,STATE.PREVIEWDATA,STATE=STATE
        H8SPLT,STATE.PREVIEWDATA,STATE
   END
   STATE.CNT_YLOGON: BEGIN
        WIDGET_CONTROL,STATE.CNT_YLOGON,SET_UVALUE=1
        H8CPLT,STATE
   END
   STATE.CNT_YLOGOFF: BEGIN
        WIDGET_CONTROL,STATE.CNT_YLOGON,SET_UVALUE=0
        H8CPLT,STATE
   END

   STATE.SWITCH_CFG: BEGIN
        WIDGET_CONTROL,STATE.BASE_G,MAP=0
        WIDGET_CONTROL,STATE.BASE_C,MAP=0
        WIDGET_CONTROL,STATE.BASE_R,MAP=1
        WIDGET_CONTROL,STATE.BASE_ionsn,MAP=0
   END

   STATE.SWITCH_GRF: BEGIN
        WIDGET_CONTROL,STATE.BASE_R,MAP=0
        WIDGET_CONTROL,STATE.BASE_C,MAP=0
        WIDGET_CONTROL,STATE.BASE_G,MAP=1
        widget_control,state.base_ionsn,map=0   
        H8PLOT,(STATE.PREVIEWDATA),STATE=STATE
   END

   STATE.SWITCH_CNT: BEGIN
        WIDGET_CONTROL,STATE.BASE_R,MAP=0
        WIDGET_CONTROL,STATE.BASE_G,MAP=0
        WIDGET_CONTROL,STATE.BASE_C,MAP=1   
        widget_control,state.base_ionsn,map=0   
        	
  

	
	



	
	
	
	
	H8CPLT,STATE
   END
   
   
    state.switch_ionsn: begin
 
   	if state.cadwion_on eq 1 then begin
         widget_control,state.base_r,map=0
         widget_control,state.base_g,map=0
         widget_control,state.base_c,map=0   
         widget_control,state.base_ionsn,map=1   
         h8cplt,state
	endif
    end

		
   state.tab_val_sel: begin
	widget_control, state.tab_val_sel, get_value=val_ch
   end

   
   state.val_buts: begin
     open_val=0
     widget_control, state.val_buts, get_value=open_val
     if (open_val eq 0) then begin
        widget_control, state.tab_val_sel, set_value=state.table_data1
	widget_control,state.base_sw_1, map=1
	widget_control,state.base_sw_2, map=0
     endif
     if (open_val eq 1) then begin
        widget_control, state.tab_val_sel, set_value=state.table_data2
	widget_control,state.base_sw_1, map=0
	widget_control,state.base_sw_2, map=1
     endif
   end
                   
   state.but_set_defs: begin
        widget_control, state.val_buts, get_value=open_val
;   	widget_control, state.txt1, get_value=temp
;   	widget_control, state.txt2, get_value=temp0
	nmaxdn=0
	nmindn=0
	nmaxl=0
	nminl=0
	nmaxdn_1=0
	nmindn_1=0
	nmaxl_1=0
	nminl_1=0
	nmaxdn_2=0
	nmindn_2=0
	nmaxl_2=0
	nminl_2=0
		
	if open_val eq 0 then begin
   	   widget_control, state.txt1_rul_2, get_value=temp1
   	   widget_control, state.txt2_rul_2, get_value=temp2
   	   widget_control, state.txt1_rul_3, get_value=temp3
   	   widget_control, state.txt2_rul_3, get_value=temp4
	   temp1=strtrim(temp1,2)
	   nmaxdn=fix(temp1(0))
	   temp2=strtrim(temp2,2)
	   nmindn=fix(temp2(0))
	   temp3=strtrim(temp3,2)
	   nmaxl=fix(temp3(0))
	   temp4=strtrim(temp4,2)
	   nminl=fix(temp4(0))
	endif
	if open_val eq 1 then begin
   	   widget_control, state.ntxt1_rul_2, get_value=temp10
   	   widget_control, state.ntxt2_rul_2, get_value=temp11
   	   widget_control, state.ntxt1_rul_3, get_value=temp12
   	   widget_control, state.ntxt2_rul_3, get_value=temp13
   	   widget_control, state.ntxt1_rul_4, get_value=temp14
   	   widget_control, state.ntxt2_rul_4, get_value=temp15
   	   widget_control, state.ntxt1_rul_5, get_value=temp16
   	   widget_control, state.ntxt2_rul_5, get_value=temp17
	   temp10=strtrim(temp10,2)
	   nmaxdn_1=fix(temp10(0))
	   temp11=strtrim(temp11,2)
	   nmindn_1=fix(temp11(0))
	   temp12=strtrim(temp12,2)
	   nmaxl_1=fix(temp12(0))
	   temp13=strtrim(temp13,2)
	   nminl_1=fix(temp13(0))
	   nmaxdn_2=fix(temp14(0))
	   temp15=strtrim(temp15,2)
	   nmindn_2=fix(temp15(0))
	   temp16=strtrim(temp16,2)
	   nmaxl_2=fix(temp16(0))
	   temp17=strtrim(temp17,2)
	   nminl_2=fix(temp17(0))
	endif
   	widget_control, state.but_prom, get_value=iprom
   	widget_control, state.txt_prom_2, get_value=temp5
   	widget_control, state.txt_prom_3, get_value=temp6
;   	widget_control, state.txt3, get_value=temp7
;   	widget_control, state.txt4, get_value=temp8
;	temp7=strtrim(temp7,2)
;	ion_min=fix(temp7(0))
	ION_MIN=STATE.OPTIONS.ION_MIN
;	temp8=strtrim(temp8,2)
;	ion_max=fix(temp8(0))
	ION_MAX=STATE.OPTIONS.ION_MAX
;	temp=strtrim(temp,2)
;	iz0=fix(temp(0))
        IZ0=STATE.OPTIONS.IZ0
;	temp0=strtrim(temp0,2)
;	el_symb=temp0(0)
	EL_SYMB=STATE.OPTIONS.EL_SYM
        temp5=strtrim(temp5,2)
	iprom_low=fix(temp5(0))
	temp6=strtrim(temp6,2)
	iprom_high=fix(temp6(0))
	config=strarr(100)
	ipar=intarr(100)
        widget_control, state.tab_val_sel, get_value=itemp
        widget_control, state.val_buts, get_value=open_val
	val_val=fix(itemp)
        if open_val eq 0 then begin
	   val_val=state.table_data1_indx(val_val)
	endif
	if open_val eq 1 then begin
	   val_val=state.table_data2_indx(val_val)
        endif	   
	
	state.iz0=iz0
	
	strct={ion_min:ion_min, ion_max :ion_max,iz0:iz0,el_symb:el_symb,$
	nmaxdn:nmaxdn,nmindn:nmindn,nmaxl:nmaxl,nminl:nminl,iprom_low:iprom_low,$
	iprom_high:iprom_high, iprom:iprom, config:config, ipar:ipar,$
	nmaxdn_1:nmaxdn_1,nmindn_1:nmindn_1,nmaxl_1:nmaxl_1,nminl_1:nminl_1,$
	nmaxdn_2:nmaxdn_2,nmindn_2:nmindn_2,nmaxl_2:nmaxl_2,nminl_2:nminl_2,$
	open_val:open_val, val_val:val_val}
	
	h8prom,strct, state
;	stop
        if open_val eq 0 then begin
	   text_value=state.table_data1[fix(itemp)]
	   if strpos(text_value, 'Done') eq -1 then begin
	      state.table_data1[fix(itemp)]=state.table_data1[fix(itemp)]+' Done'
           endif
	   widget_control,state.tab_val_sel,set_value=state.table_data1
	endif
	if open_val eq 1 then begin
	   text_value=state.table_data2[fix(itemp)]
	   if strpos(text_value, 'Done') eq -1 then begin
	      state.table_data2[fix(itemp)]=state.table_data2[fix(itemp)]+' Done'
           endif
           widget_control,state.tab_val_sel,set_value=state.table_data2
        endif	
;	stop 
   end

   state.txt_conf1: begin
	widget_control, state.txt_conf1,get_value=temp
	ion_ch=fix(temp(0))
	tabval=state.prom_config(ion_ch,*)
;	widget_control, state.conf_ed_id, set_value=tabval
;	widget_control, state.conf_ed_id,get_value=tabval2
        FOR I=0,(SIZE(TABVAL))[2]-1 DO BEGIN
                WIDGET_CONTROL,STATE.CONFIG_LABEL[I],SET_VALUE=STRTRIM(TABVAL[0,I],2)
                WIDGET_CONTROL,STATE.CONFIG_NLEVS[I],SET_VALUE=STRTRIM(STRING(STATE.PROM_NLEV[ION_CH,I]),2)
                WIDGET_CONTROL,STATE.CONFIG_SPLIT_MAN[I],SET_BUTTON=STATE.MANUAL_SPLIT[ION_CH,I]
                WIDGET_CONTROL,STATE.CONFIG_SPLIT_MAN[I],SET_UVALUE=STATE.MANUAL_SPLIT[ION_CH,I]
        END
        
        total=0
        for j=0,(size(state.prom_nlev))[2]-1 do begin
                total=total+state.prom_nlev[ion_ch,j]
        end

        widget_control,state.txt_base_nion,set_value=strtrim(string(total),2)

        
;        stop
	;eval_conf_av,state,ion_ch,DATA
        
        ;STATE.PREVIEWDATA[ION_CH]=PTR_NEW(DATA)
        H8PLOT,(STATE.PREVIEWDATA),STATE=STATE
        H8SPLT,(STATE.PREVIEWDATA),STATE
        H8CPLT,STATE
        	
        ;stop
        
        
   end
   
   state.but_sel_all_all:begin
   	for i=0,99 do begin
		widget_control,state.config_split_man[i],/set_button
	end
	for i=0,99 do begin
		for j=state.ion_min,state.ion_max do begin
			state.manual_split[j,i]=1
		end
	end
	H8SPLT,(STATE.PREVIEWDATA),STATE
        H8CPLT,STATE
   end
   
   
   state.but_desel_all_all:begin
   	for i=0,99 do begin
		widget_control,state.config_split_man[i],set_button=0
	end
	for i=0,99 do begin
		for j=state.ion_min,state.ion_max do begin
			state.manual_split[j,i]=0
		end	
	end
	H8SPLT,(STATE.PREVIEWDATA),STATE
        H8CPLT,STATE
   end

   state.but_sel_all_one:begin
	WIDGET_CONTROL, STATE.TXT_CONF1,GET_VALUE=TEMP
	ION_CH=FIX(TEMP(0))
 	if temp[0] ne '' and temp[0] ne ' ' and temp[0] ne '  ' then begin
	   	for i=0,99 do begin
			widget_control,state.config_split_man[i],/set_button
			state.manual_split[ion_ch,i]=1
		end
		H8SPLT,(STATE.PREVIEWDATA),STATE
	        H8CPLT,STATE
	endif
   end
   
   
   state.but_desel_all_one:begin
	WIDGET_CONTROL, STATE.TXT_CONF1,GET_VALUE=TEMP
	ION_CH=FIX(TEMP(0))
 	if temp[0] ne '' and temp[0] ne ' ' and temp[0] ne '  ' then begin
	    	for i=0,99 do begin
			widget_control,state.config_split_man[i],set_button=0
			state.manual_split[ion_ch,i]=0
		end
		H8SPLT,(STATE.PREVIEWDATA),STATE
	        H8CPLT,STATE
	endif
   end

   
   
   state.but_ground_proc: begin
     if state.cadwion_on eq 1 then begin

       widget_control, state.txt_ion_stage_1, get_value=dummy
       ion_stage_min=fix(dummy[0])
       widget_control, state.txt_ion_stage_2, get_value=dummy
       ion_stage_max=fix(dummy[0])
       widget_control, state.txt_rul_dirct_shl, get_value=dummy
       ion_stage_drct_mnshl=string(dummy[0])
       widget_control, state.txt_rul_ind_dnmin, get_value=dummy
       ion_stage_ind_mnshl=string(dummy[0])
       widget_control, state.txt_rul_ind_dnmax, get_value=dummy
       ion_stage_ind_mxshl=string(dummy[0])
       widget_control, state.txt_rul_ind_mxl, get_value=dummy
       ion_stage_ind_mxl=string(dummy[0])
;       widget_control, state.txt_rul_dirct_prnt_shl, get_value=dummy
;       ion_stage_dirct_prnt_shl=string(dummy[0])
;       widget_control, state.txt_rul_dirct_prnt_mxl, get_value=dummy
;       ion_stage_dirct_prnt_mxl=string(dummy[0])
       widget_control, state.ps_txt_rul_dirct_shl, get_value=dummy
       ion_stage_pe_mnshl=string(dummy[0])
       widget_control, state.ps_txt_rul_dirct_shl_vac, get_value=dummy
       ion_stage_pe_vac=string(dummy[0])
       widget_control, state.ps_txt_rul_dirct_prnt_shl, get_value=dummy
       ion_stage_pe_mxshl=string(dummy[0])
       widget_control, state.ps_txt_rul_dirct_prnt_mxl, get_value=dummy
       ion_stage_pe_mxl=string(dummy[0])
       ionsn_rules={   ion_stage_drct_mnshl:ion_stage_drct_mnshl,$
		       ion_stage_ind_mnshl:ion_stage_ind_mnshl,  ion_stage_ind_mxshl:ion_stage_ind_mxshl,$
		       ion_stage_ind_mxl:ion_stage_ind_mxl, $
                       ion_stage_pe_mnshl:ion_stage_pe_mnshl, ion_stage_pe_mxshl:ion_stage_pe_mxshl,$
                       ion_stage_pe_mxl:ion_stage_pe_mxl, ion_stage_pe_vac:ion_stage_pe_vac}
       for i=ion_stage_min,ion_stage_max do begin
	       grnd_confg=strtrim(state.config[i],2)
	       h8ione, ionsn_rules, state, grnd_confg,i
       endfor
     endif
   end

;   state.but_met_proc: begin
;   	if state.cadwion_on eq 1 then begin
;
;	widget_control, state.txt_met_enter, get_value=dummy
; 	ion_ch=fix(dummy[0])
; 	widget_control, state.txt_rul_dirct_shl_met, get_value=dummy
; 	ion_stage_drct_shl=string(dummy[0])
;; 	widget_control, state.txt_rul_dirct_prnt_met_shl, get_value=dummy
;; 	ion_stage_dirct_mnshl=string(dummy[0])
;; 	widget_control, state.txt_rul_dirct_prnt_met_mxl, get_value=dummy
;; 	ion_stage_dirct_prnt_mxl=string(dummy[0])
; 	widget_control, state.txt_rul_ind_dnmin_met, get_value=dummy
; 	ion_stage_ind_mnshl=string(dummy[0])
; 	widget_control, state.txt_rul_ind_dnmax_met, get_value=dummy
;	ion_stage_ind_mxshl=string(dummy[0])
; 	widget_control, state.txt_rul_ind_mxl_met, get_value=dummy
; 	ion_stage_ind_mxl=string(dummy[0])
; 	widget_control, state.txt_ion_selected_met, get_value=dummy
; 	met_confg=strtrim(string(dummy[0]),2)
;	widget_control, state.ps_txt_rul_dirct_shl_met, get_value=dummy
;	ion_stage_pe_mnshl=string(dummy[0])
;	widget_control, state.ps_txt_rul_dirct_prnt_shl_met, get_value=dummy
;	ion_stage_pe_mxshl_met=string(dummy[0])
;	widget_control, state.ps_txt_rul_dirct_prnt_mxl_met, get_value=dummy
;	ion_stage_pe_mxl_met=string(dummy[0])
; 	ionsn_rules={	ion_stage_drct_mnshl:ion_stage_drct_mnshl,$
; 			ion_stage_ind_mnshl:ion_stage_ind_mnshl,  ion_stage_ind_mxshl:ion_stage_ind_mxshl,$
; 			ion_stage_ind_mxl:ion_stage_ind_mxl,$
;			ion_stage_pe_mnshl:ion_stage_pe_mnshl,ion_stage_pe_mxshl:ion_stage_pe_mxshl,$
;			ion_stage_pe_mxl:ion_stage_pe_mxl}
;        h8ione,  ionsn_rules, state, met_confg, ion_ch
;    endif
;   end
;   state.txt_met_enter: begin
;     if state.cadwion_on eq 1 then begin
;   	widget_control, state.txt_met_enter, get_value=dummy
;	ion_ch=fix(dummy)
;	widget_control, state.txt_ion_selected_met,set_value=strtrim(state.config[ion_ch],2)
;     endif
;   end
                ;***********************
		;**** Cancel button ****
		;***********************
                
    state.cancelid: new_event = {ID:parent, TOP:event.top, $
				HANDLER:0L, ACTION:'Cancel'}


		;*********************
		;**** Done button ****
		;*********************
                
    state.doneid: begin
    
		;***************************************
		;**** Check all user input is legal ****
		;***************************************
                
	error = 0
        
                ;*************************************************
                ;*** Have to restore state before call to     ****
                ;*** proc216_get_val so it can be used there. ****
                ;*************************************************
        
	widget_control,hourglass=1

  	
	for ion_ch=state.ion_min,state.ion_max do begin
                if state.nconf(ion_ch) gt 0 then begin
			checksum=long(1)
			for i=0,35 do begin
				checksum=checksum+total(state.ioccup_store[ion_ch,*,i])*(i+1)
			end
			
			if STATE.GOTATOMDATA[ION_CH] ne checksum then begin
				h8evca,state,ion_ch,ATOMDATA,/bessel
				STATE.ATOMDATA[ION_CH]=PTR_NEW(ATOMDATA)
				STATE.GOTATOMDATA[ION_CH]=checksum
                        endif
                endif
;                print,strtrim(string(100.*(ion_ch-state.ion_min)/(state.ion_max-state.ion_min)),2)+'%'
        end
	
	widget_control,hourglass=0
	
	
	
	        
        widget_control, first_child, set_uvalue=state;, /no_copy
	
        widget_control,event.handler,get_value=val

                ;*** reset state variable ***

        widget_control, first_child, get_uvalue=state;, /no_copy
            
            
		;******************************************
		;**** now we can begin to check input. ****
		;******************************************
                
		;**** check metastable states selected ****
                
;	if error eq 0 and ps.nmet eq 0 then begin
;	  error = 1
;	  message='Error: No Metastable States selected.'
;        end


		;**** return value or flag error ****
                
	if error eq 0 then begin
	  new_event = {ID:parent, TOP:event.top, HANDLER:0L, ACTION:'Done'}
        end else begin
	  widget_control,state.messid,set_value=message
	  new_event = 0L
        end

        ; AND DATA.CALCULATE_CONFIG[I,J] EQ 1stop

      ;write_adf34,val.data

      end
                ;**** Menu button ****
                
    state.outid: begin
      new_event = {ID:parent, TOP:event.top, HANDLER:0L, ACTION:'Menu'}
    end
    

    ELSE: new_event = 0L
    
    

  ENDCASE
  
		;*********************************************
		;*** make "state" available to other files ***
		;*********************************************

    widget_control, first_child, set_uvalue=state;, /no_copy

  RETURN, new_event
  
END

;-----------------------------------------------------------------------------

FUNCTION cw_adas808_proc, topparent, 					$
                bitfile,  passdir, fortdir, $
                OPTIONS=OPTIONS, $
		VALUE=value, UVALUE=uvalue, 				$
		FONT_LARGE=font_large, FONT_SMALL=font_small, 		$
		EDIT_FONTS=edit_fonts, NUM_FORM=num_form,		$
		cadwion_on=cadwion_on


		;**** Set defaults for keywords ****
                
  IF NOT (KEYWORD_SET(uvalue)) THEN uvalue = 0
  IF NOT (KEYWORD_SET(font_large)) THEN font_large = ''
  IF NOT (KEYWORD_SET(font_small)) THEN font_small = ''
  IF NOT (KEYWORD_SET(edit_fonts)) THEN $
				edit_fonts = {font_norm:'',font_input:''}
  IF NOT (KEYWORD_SET(num_form)) THEN num_form = '(E10.3)'

  IF NOT (KEYWORD_SET(value)) THEN begin
	message,'A structure must be passed'
  END ELSE BEGIN
	ps = {	proc808_set,					$
		new       	:       value.new,		$
		title     	:       value.title,		$
		nuc_ch	  	:	value.nuc_ch,		$
		el_sym	  	:       value.el_sym,		$
		ion_min	  	:       value.ion_min,		$
		ion_max	  	:       value.ion_max,		$
		nmindn		:	value.nmindn,		$
		nmaxdn		:	value.nmaxdn,		$
		nmaxl		:	value.nmaxl,		$
		nminl		:	value.nminl,		$
		nmindn_1	:	value.nmindn_1,		$
		nmaxdn_1	:	value.nmaxdn_1,		$
		nmaxl_1		:	value.nmaxl_1,		$
		nminl_1		:	value.nminl_1,		$
		nmindn_2	:	value.nmindn_2,		$
		nmaxdn_2	:	value.nmaxdn_2,		$
		nmaxl_2		:	value.nmaxl_2,		$
		nminl_2		:	value.nminl_2,		$
		iprom		:	value.iprom,		$
		sel_but		:	value.sel_but}
  END

		;***************************************************
		;****      Assemble temperature table data      ****
		;***************************************************
		;**** The adas table widget requires data to be ****
		;**** input as strings, so all the numeric data ****
		;**** has to be written into a string array.    ****
		;**** Declare temp table array                  ****
		;**** Three editable columns and input values   ****
		;**** in three different units.                 ****
		;***************************************************
                
  tempdata = strarr(6,14)



		;********************************************************
		;**** Create the 216 Processing options/input window ****
		;********************************************************

		;**** create titled base widget ****
                
  parent = widget_base(topparent, UVALUE = uvalue, 		$
		       EVENT_FUNC = "proc808_event", 		$
		       FUNC_GET_VALUE = "proc808_get_val", 	$
		       /COLUMN)

		;******************************************************
		;**** Create a dummy widget just to hold value of *****
		;**** "state" variable so as not to get confused  *****
		;**** with any other values. Adopt IDL practice   *****
		;**** of using first child widget                 *****
		;******************************************************

  first_child = widget_base(parent)
  topbase = widget_base(first_child,/column)


		;*****************************
		;**** Information options ****
		;*****************************
                
  base  = widget_base(topbase,/row)




 ; botbase = widget_base(parent,/row)
 ; base = widget_base(botbase,/row,/frame)

  
  
 
 		;*****************************************
                ;**** Base config selection criteria  ****
		;*****************************************

  base_conf1   = widget_base(parent,/column, /frame)
  base_conf2   = widget_base(base_conf1,/column)
  base_conf3   = widget_base(base_conf1,/row)

  BBASE=WIDGET_BASE(BASE_CONF2,/ROW,/EXCLUSIVE)
  SWITCH_CFG=WIDGET_BUTTON(BBASE,VALUE='Configuration Generator')
  SWITCH_CNT=WIDGET_BUTTON(BBASE,VALUE='Level Count')
  SWITCH_GRF=WIDGET_BUTTON(BBASE,VALUE='Configuration Selection')
  if cadwion_on eq 1 then begin
  	switch_ionsn=widget_button(bbase,value='Ionisation selection')
  endif else begin
        switch_ionsn=-1
  endelse	

  WIDGET_CONTROL,SWITCH_CFG,/SET_BUTTON
  
;  labl1_conf2 = widget_label(base_conf2, value='Configuration generator')
  
  
  
  
;  labl2_conf2 = widget_label(base_conf2, $
;  	value='Set selection criteria, then click Set Default Configurations')
  base   = widget_base(base_conf1,/row)
        
  

		;************************************
                ;**** promotion selection rules  ****
		;************************************

  SWITCH_BASE=WIDGET_BASE(BASE_CONF3)
  base_r = widget_base(SWITCH_BASE,/column, /frame, /align_left)
  selections=['One open shell','Two open shells']
  val_buts = cw_bgroup(base_r, selections, $
                     font=font, /exclusive, /row, /no_release)
  widget_control, val_buts, set_value=ps.sel_but

  base_rules = widget_base(base_r, /row, /align_left)
  base_rul_1 = widget_base(base_rules )
  base_sel = widget_base(base_rules , /row)
  base_sw_1 = widget_base(base_rul_1 , /column)
  base_sw_2 = widget_base(base_rul_1 , /column)


; nmaxdn=strtrim(string(ps.nmaxdn),2)
; nmindn=strtrim(string(ps.nmindn),2)
; nmaxl=strtrim(string(ps.nmaxl),2)
; nminl=strtrim(string(ps.nminl),2)
; nmaxdn_1=strtrim(string(ps.nmaxdn_1),2)
; nmindn_1=strtrim(string(ps.nmindn_1),2)
; nmaxl_1=strtrim(string(ps.nmaxl_1),2)
; nminl_1=strtrim(string(ps.nminl_1),2)
; nmaxdn_2=strtrim(string(ps.nmaxdn_2),2)
; nmindn_2=strtrim(string(ps.nmindn_2),2)
; nmaxl_2=strtrim(string(ps.nmaxl_2),2)
; nminl_2=strtrim(string(ps.nminl_2),2)
 
  nmaxdn=strtrim(string(1),2)
  nmindn=strtrim(string(-1),2)
  nmaxl=strtrim(string(1),2)
  nminl=strtrim(string(1),2)
  nmaxdn_1=strtrim(string(1),2)
  nmindn_1=strtrim(string(-1),2)
  nmaxl_1=strtrim(string(1),2)
  nminl_1=strtrim(string(1),2)
  nmaxdn_2=strtrim(string(1),2)  ; Stuart had this as -1
  nmindn_2=strtrim(string(-1),2) ; Stuart had this as 1
  nmaxl_2=strtrim(string(1),2)
  nminl_2=strtrim(string(1),2)
 
  
  base_rul_10 = widget_base(base_sw_1 , /column, /align_left)
  lab1_rules = widget_label(base_rul_10, $
   	value='                   +ve   -ve')
  base_rul_2 = widget_base(base_rul_10 , /row, /align_left)
  lab_rul_2 = widget_label(base_rul_2, value='Max. Dn          ')
  txt1_rul_2 = widget_text(base_rul_2,value=nmaxdn,xsize=3,font=font_large,/edit)
  txt2_rul_2 = widget_text(base_rul_2,value=nmindn,xsize=3,font=font_large,/edit)
  base_rul_3 = widget_base(base_rul_10 , /row, /align_left)
  lab_rul_3 = widget_label(base_rul_3, value='Max. l           ')
  txt1_rul_3 = widget_text(base_rul_3,value=nmaxl,xsize=3,font=font_large,/edit)
  junk=widget_base(group_leader=base_rul_10)
  txt2_rul_3 = widget_text(junk,value=nminl,xsize=3,font=font_large,/edit)

  nbase_rul_11 = widget_base(base_sw_2 , /column, /align_left)
  nlab1_rules = widget_label(nbase_rul_11, $
   	value='                                           +ve   -ve')
  nbase_rul_2 = widget_base(nbase_rul_11 , /row, /align_left)
  nlab_rul_2 = widget_label(nbase_rul_2, value='Max. Dn for 1st val shl')
  ntxt1_rul_2 = widget_text(nbase_rul_2,value=nmaxdn_1,xsize=3,font=font_large,/edit)
  ntxt2_rul_2 = widget_text(nbase_rul_2,value=nmindn_1,xsize=3,font=font_large,/edit)
  nbase_rul_3 = widget_base(nbase_rul_11 , /row, /align_left)
  nlab_rul_3 = widget_label(nbase_rul_3, value='Max. l for 1st val shl')
  ntxt1_rul_3 = widget_text(nbase_rul_3,value=nmaxl_1,xsize=3,font=font_large,/edit)
  ntxt2_rul_3 = widget_text(nbase_rul_3,value=nminl_1,xsize=3,font=font_large,/edit)
  nbase_rul_4 = widget_base(nbase_rul_11 , /row, /align_left)
  nlab_rul_4 = widget_label(nbase_rul_4, value='Max. Dn  for 2nd val shl')
  ntxt1_rul_4 = widget_text(nbase_rul_4,value=nmaxdn_2,xsize=3,font=font_large,/edit)
  ntxt2_rul_4 = widget_text(nbase_rul_4,value=nmindn_2,xsize=3,font=font_large,/edit)
  nbase_rul_5 = widget_base(nbase_rul_11 , /row, /align_left)
  nlab_rul_5 = widget_label(nbase_rul_5, value='Max. l for 2nd val shl')
  ntxt1_rul_5 = widget_text(nbase_rul_5,value=nmaxl_2,xsize=3,font=font_large,/edit)
  ntxt2_rul_5 = widget_text(nbase_rul_5,value=nminl_2,xsize=3,font=font_large,/edit)

widget_control, base_sw_1, map=1
widget_control, base_sw_2, map=0

                                  
		;************************************
                ;**** valence selection buttons  ****
		;************************************
  lab_rul_1 = widget_label(base_sel, value='Select valence shell')
;  table_data1=strarr(15)
;  table_data2=strarr(15)
  tab_val_sel = h8ssel( base_sel, strarr(15), ysize=4, title='Select Orbital')
  widget_control,tab_val_sel,set_value=options.table_data1
	

		;************************************
                ;**** promotion rules buttons    ****
		;************************************

        a=widget_base(group_leader=base_r)

;  base_prom = widget_base(base_r, /column,/frame)
  base_prom = widget_base(a, /column,/frame)
  base_prom_1 = widget_base(base_prom , /row)
  base_prom_2 = widget_base(base_prom, /row)
  base_prom_3 = widget_base(base_prom, /row)
  
  
  
  lab_prom1 = widget_label(base_prom_1, value='Promote from inner shells?')
  selections = ['Yes', 'no']
  but_prom = cw_bgroup(base_prom_1, selections, $
                     font=font, /exclusive, /row, /no_release)
  widget_control,but_prom,set_value=1

  lab_prom_2 = widget_label(base_prom_2, value='Lowest starting n-shell       ')
  txt_prom_2 = widget_text(base_prom_2,value='',xsize=3,font=font_large,/edit)  
  lab_prom_3 = widget_label(base_prom_3, value='Highest finishing n-shell     ')
  txt_prom_3 = widget_text(base_prom_3,value='',xsize=3,font=font_large,/edit)  
   
		;************************************
                ;**** Include complexes buttons  ****
		;************************************

;  base_comp = widget_base(base_r, /column,/frame)
;  base_comp_1 = widget_base(base_comp , /row)
;  base_comp_2 = widget_base(base_comp, /row)
;  base_comp_3 = widget_base(base_comp_2, /row, /frame)
  
;  lab_comp1 = widget_label(base_comp_1, value='Include complexes?')
;  selections = ['Yes', 'no']
;  but_comp = cw_bgroup(base_comp_1, selections, $
;                     font=font, /exclusive, /row, /no_release)

;  lab_comp_2 = widget_label(base_comp_3, value='Promotions:')
;  selections = ['1 electron', '2 electrons']
;  but_comp = cw_bgroup(base_comp_3, selections, $
;                     font=font, /exclusive, /row, /no_release)
   
  bbase=widget_base(base_r,/column)
  but_set_defs = widget_button(bbase, value='Set configurations for selected orbital')
;  but_set_defs_all = widget_button(bbase, value='Set configurations for all orbitals')
  
  
                ;graph thing
  
  BASE_C=WIDGET_BASE(SWITCH_BASE,/COLUMN)
  
  WIDGET_CONTROL,BASE_C,MAP=0  

  PLOT_CNT_WID=WIDGET_DRAW(BASE_C,XSIZE=600,YSIZE=400);,EVENT_PRO='h8grev',/BUTTON_EVENTS,/MOTION_EVENTS,UVALUE={FIRST_CHILD:FIRST_CHILD,MOVING:[-1,-1,-1,-1],LINE:[-1.0,-1.0]})
  
  BOT_BASE=WIDGET_BASE(BASE_C,/ROW)
  
  LEFT_BASE=WIDGET_BASE(BOT_BASE,/COLUMN)
  
  CONTROL_BASE=WIDGET_BASE(LEFT_BASE,/COLUMN)
  TBASE=WIDGET_BASE(CONTROL_BASE,/ROW)
  LABEL=WIDGET_LABEL(TBASE,VALUE='Ion. Stage ')
  CNT_XMIN=WIDGET_TEXT(TBASE,/EDITABLE,XSIZE=5)
  CNT_XMAX=WIDGET_TEXT(TBASE,/EDITABLE,XSIZE=5)
  TBASE=WIDGET_BASE(CONTROL_BASE,/ROW)
  LABEL=WIDGET_LABEL(TBASE,VALUE='Number     ')
  CNT_YMIN=WIDGET_TEXT(TBASE,/EDITABLE,XSIZE=5)
  CNT_YMAX=WIDGET_TEXT(TBASE,/EDITABLE,XSIZE=5)
  TBASE=WIDGET_BASE(CONTROL_BASE,/ROW)
  LABEL=WIDGET_LABEL(TBASE,VALUE='Y Log Scale')
  BBASE=WIDGET_BASE(TBASE,/EXCLUSIVE,/ROW)
  CNT_YLOGON=WIDGET_BUTTON(BBASE,VALUE='On') 
  CNT_YLOGOFF=WIDGET_BUTTON(BBASE,VALUE='Off') 
  
  WIDGET_CONTROL,CNT_YLOGON,/SET_BUTTON,SET_UVALUE=1
      
  RIGHT_BASE=WIDGET_BASE(BOT_BASE,/COLUMN)

  OPT_BASE=WIDGET_BASE(RIGHT_BASE,/COLUMN,/EXCLUSIVE)
  SEL_LEV_BUT=WIDGET_BUTTON(OPT_BASE,VALUE='Selected Levels',UVALUE=0)
  TOT_LEV_BUT=WIDGET_BUTTON(OPT_BASE,VALUE='Total Levels')
  SEL_CFG_BUT=WIDGET_BUTTON(OPT_BASE,VALUE='Selected Configurations')
  TOT_CFG_BUT=WIDGET_BUTTON(OPT_BASE,VALUE='Total Configurations')
  
  WIDGET_CONTROL,SEL_LEV_BUT,/SET_BUTTON
         
  BASE_COUNT=WIDGET_BASE(BASE_C,/ROW)
  BBASE=WIDGET_BASE(BASE_COUNT,/COLUMN)  
  TBASE=WIDGET_BASE(BBASE,/ROW)
  LABEL=WIDGET_LABEL(TBASE,VALUE='Selected Levels')	 
  SEL_LEV_TXT=WIDGET_TEXT(TBASE,XSIZE=7,VALUE='-')
  TBASE=WIDGET_BASE(BBASE,/ROW)
  LABEL=WIDGET_LABEL(TBASE,VALUE='Total Levels   ')	 
  TOT_LEV_TXT=WIDGET_TEXT(TBASE,XSIZE=7,VALUE='-')
  BBASE=WIDGET_BASE(BASE_COUNT,/COLUMN)  
  TBASE=WIDGET_BASE(BBASE,/ROW)
  LABEL=WIDGET_LABEL(TBASE,VALUE='Selected Configurations')	 
  SEL_CFG_TXT=WIDGET_TEXT(TBASE,XSIZE=7,VALUE='-')
  TBASE=WIDGET_BASE(BBASE,/ROW)
  LABEL=WIDGET_LABEL(TBASE,VALUE='Total Configurations   ')	 
  TOT_CFG_TXT=WIDGET_TEXT(TBASE,XSIZE=7,VALUE='-')
	        
  BASE_G=WIDGET_BASE(SWITCH_BASE,/COLUMN)
  
  PLOT_WID=WIDGET_DRAW(BASE_G,XSIZE=600,YSIZE=400,EVENT_PRO='h8grev',/BUTTON_EVENTS,/MOTION_EVENTS,UVALUE={FIRST_CHILD:FIRST_CHILD,MOVING:[-1,-1,-1,-1],LINE:[-1.0,-1.0]})
  
  BOT_BASE=WIDGET_BASE(BASE_G,/ROW)
  
  LEFT_BASE=WIDGET_BASE(BOT_BASE,/COLUMN)

  RUN_COWAN_BASE=WIDGET_BASE(LEFT_BASE,/ROW)
  RUN_ENERGY=WIDGET_BUTTON(RUN_COWAN_BASE,VALUE='Calculate Survey Spectrum')
  ;RUN_AVALUE=WIDGET_BUTTON(RUN_COWAN_BASE,VALUE='Calculate A-values')

  
  CONTROL_BASE=WIDGET_BASE(LEFT_BASE,/COLUMN)
  TBASE=WIDGET_BASE(CONTROL_BASE,/ROW)
  LABEL=WIDGET_LABEL(TBASE,VALUE='Wavelength ')
  LMIN=WIDGET_TEXT(TBASE,/EDITABLE,XSIZE=5)
  LMAX=WIDGET_TEXT(TBASE,/EDITABLE,XSIZE=5)
  TBASE=WIDGET_BASE(CONTROL_BASE,/ROW)
  LABEL=WIDGET_LABEL(TBASE,VALUE='Criterion  ')
  CMIN=WIDGET_TEXT(TBASE,/EDITABLE,XSIZE=5)
  CMAX=WIDGET_TEXT(TBASE,/EDITABLE,XSIZE=5)
  TBASE=WIDGET_BASE(CONTROL_BASE,/ROW)
  LABEL=WIDGET_LABEL(TBASE,VALUE='X Log Scale')
  BBASE=WIDGET_BASE(TBASE,/EXCLUSIVE,/ROW)
  XLOGON=WIDGET_BUTTON(BBASE,VALUE='On') 
  XLOGOFF=WIDGET_BUTTON(BBASE,VALUE='Off') 
  TBASE=WIDGET_BASE(CONTROL_BASE,/ROW)
  LABEL=WIDGET_LABEL(TBASE,VALUE='Y Log Scale')
  BBASE=WIDGET_BASE(TBASE,/EXCLUSIVE,/ROW)
  YLOGON=WIDGET_BUTTON(BBASE,VALUE='On') 
  YLOGOFF=WIDGET_BUTTON(BBASE,VALUE='Off') 
  
  WIDGET_CONTROL,YLOGON,/SET_BUTTON,SET_UVALUE=1
  WIDGET_CONTROL,XLOGON,/SET_BUTTON,SET_UVALUE=1
  
  ;REPLOT=WIDGET_BUTTON(CONTROL_BASE,VALUE='Replot')
    
  RIGHT_BASE=WIDGET_BASE(BOT_BASE,/COLUMN)
  SCROLLY_BASE=WIDGET_BASE(RIGHT_BASE,/SCROLL)
  INSIDE_BASE=WIDGET_BASE(SCROLLY_BASE,/COLUMN)
  SELECT_BASE=LONARR(10)
  SELECT_ONOFF=LONARR(10)
  SELECT_LMIN=LONARR(10)
  SELECT_LMAX=LONARR(10)
  SELECT_CMIN=LONARR(10)
  SELECT_BUT=LONARR(10)
  FOR I=0,9 DO BEGIN
        TBASE=WIDGET_BASE(INSIDE_BASE,/ROW)
        BBASE=WIDGET_BASE(TBASE,/NONEXCLUSIVE)
        SELECT_ONOFF[I]=WIDGET_BUTTON(BBASE,VALUE=' ',UVALUE=0)
        SELECT_BASE[I]=WIDGET_BASE(TBASE,/ROW)
        SELECT_LMIN[I]=WIDGET_TEXT(SELECT_BASE[I],/EDITABLE,XSIZE=5)
        SELECT_LMAX[I]=WIDGET_TEXT(SELECT_BASE[I],/EDITABLE,XSIZE=5)
        SELECT_CMIN[I]=WIDGET_TEXT(SELECT_BASE[I],/EDITABLE,XSIZE=5)
        BBASE=WIDGET_BASE(SELECT_BASE[I],/EXCLUSIVE)
        SELECT_BUT[I]=WIDGET_BUTTON(BBASE,VALUE=' ')
        WIDGET_CONTROL,SELECT_BASE[I],SENSITIVE=0
  END
  ;stop 
  A=WIDGET_INFO(TBASE,/GEOM)
  WIDGET_CONTROL,SCROLLY_BASE,SCR_XSIZE=A.SCR_XSIZE*1.4,SCR_YSIZE=A.SCR_YSIZE*4.3
  HELP,A,/STR
  WIDGET_CONTROL,SELECT_BUT[0],/SET_BUTTON,SET_UVALUE=0
  
  
  WIDGET_CONTROL,BASE_G,MAP=0  
  
  
  
      
		;*************************
                ;**** Config editor   ****
		;*************************
                  
  base_conf = widget_base(base_conf3,/column,/frame,/align_left)
  selections=['Standard notation','Eissner notation']
;  conf_buts = cw_bgroup(base_conf, selections, font=font, /exclusive, /row, /no_release)
  base_conf1 = widget_base(base_conf, /row)
  but_sel_all_all=widget_button(base_conf1,value='Select All')
  but_desel_all_all=widget_button(base_conf1,value='Deselect All')
  lab_conf1 = widget_label(base_conf1, value='        Ion charge to view')
  txt_conf1 = widget_text(base_conf1, value='',xsize=3,font=font_large,/edit)
   
                       
  base_tab_conf = widget_base(base_conf,/column, /frame)
  conf_ed_val = strarr(1,100)
;  conf_ed_id = cw_adas_table(base_tab_conf, conf_ed_val,/scroll, $
;		       COLEDIT = [1], COLHEAD = ['Configuration'], $
;		       CELLSIZE = 40, $
;		       FONTS = edit_fonts, FONT_LARGE = font_large, $
;		       FONT_SMALL = font_small, XTEXSIZE=50, /DIFFLEN)
;   conf_ed_id=cw_edit_table(base_tab_conf, value=conf_ed_val, NCOLS=1, $
;   NROWS=100, ysize=5, COLHEAD=['Configuration'], cellsize=30)
  
  
  SCROLLY_BASE=widget_base(base_tab_conf,/scroll)
  CONF_ED_BASE=widget_base(SCROLLY_BASE,/COLUMN)
  
  
  CONFIG_LABEL=LONARR(100)
  CONFIG_NLEVS=LONARR(100)
  CONFIG_SPLIT_BUT=LONARR(100)
  CONFIG_SPLIT_MAN=LONARR(100)
  CONFIG_SPLIT_BUT_ION=LONARR(100)
  lab_filler=lonarr(100)
  MANUAL_SPLIT=INTARR(100,100)
  header=widget_base(conf_ed_base,/row)
  but_sel_all_one=widget_button(header,value='Select All')
  but_desel_all_one=widget_button(header,value='Deselect All')
  
  ; header = widget_label(conf_ed_base, value='                                                    wavel sel.    Ionsn.')
  ; stuart... no, no no! you can't assume everyone and their granny has the same font size!
  ; the following gives a good _approximation_ to what we want
  header=widget_base(conf_ed_base,/row)
  junk=widget_base(title='bah',/row)
  l1=widget_label(junk,value='1')
  t1=widget_text(junk)
  t2=widget_text(junk,xsize=4)
  totsize=(widget_info(l1,/geom)).scr_xsize+(widget_info(t1,/geom)).scr_xsize+(widget_info(t2,/geom)).scr_xsize+((widget_info(l1,/geom)).xpad+(widget_info(t1,/geom)).xpad+(widget_info(t2,/geom)).xpad)*2
  gap=widget_label(header,scr_xsize=totsize,value='')
  bb=widget_base(junk,/NONEXCLUSIVE,/ROW)
  b1=WIDGET_BUTTON(junk,VALUE=' ')
  b2=WIDGET_BUTTON(junk,VALUE=' ')
  l2=widget_label(junk,value='')
  totsize=2*((widget_info(l2,/geom)).xoffset-(widget_info(bb,/geom)).xoffset)+(widget_info(b1,/geom)).xpad
  label=widget_label(header,scr_xsize=totsize,value='Resolve')
  if cadwion_on eq 1 then begin
  	label=widget_label(header,scr_xsize=totsize,value='Ionise')
  endif
  
  widget_control,junk,/destroy  
  
  FOR I=0,99 DO BEGIN
        TBASE=WIDGET_BASE(CONF_ED_BASE,/ROW)
        LABEL=WIDGET_LABEL(TBASE,VALUE=STRTRIM(STRING(I+1),2))
        CONFIG_LABEL[I]=WIDGET_TEXT(TBASE,/EDITABLE)
;        CONFIG_LABEL[I]=WIDGET_LABEL(TBASE,VALUE=' ',/dynamic_resize)
        CONFIG_NLEVS[I]=WIDGET_TEXT(TBASE,XSIZE=4)
        BBASE=WIDGET_BASE(TBASE,/NONEXCLUSIVE,/ROW)
        CONFIG_SPLIT_BUT[I]=WIDGET_BUTTON(BBASE,VALUE=' ',UVALUE=0,SENSITIVE=0)
        CONFIG_SPLIT_MAN[I]=WIDGET_BUTTON(BBASE,VALUE=' ',UVALUE=0)
	if cadwion_on eq 1 then begin
 		CONFIG_SPLIT_BUT_ION[I]=WIDGET_BUTTON(BBASE,VALUE=' ',UVALUE=0,SENSITIVE=0)
        endif
  END
  A=WIDGET_INFO(TBASE,/GEOM)
  WIDGET_CONTROL,SCROLLY_BASE,SCR_XSIZE=A.SCR_XSIZE*1.15,SCR_YSIZE=A.SCR_YSIZE*10.5
   
  but_rest_conf = widget_button(base_tab_conf, $
  value='Restore Default Configurations for ion')
  
  
  
  base_nion = widget_base(base_conf , /row)
  lab_base_nion = widget_label(base_nion, $
  	value='Number of levels for ion                        ')
  txt_base_nion = widget_text(base_nion, value='',xsize=6,font=font_large,/edit)
  base_niontot = widget_base(base_conf , /row)
  lab_base_niontot = widget_label(base_niontot, $
  	value='Number of levels for selected configurations    ')
  TXT_BASE_NIONSEL = widget_text(base_niontot, value='',xsize=6,font=font_large,/edit)

		;**********************************
		;**** Ionisation selection rules***
		;**********************************
	
  base_ionsn=widget_base(switch_base,/row, /frame)
  widget_control,base_ionsn,map=0  
  base_ionsn_1=widget_base(base_ionsn, /column)
  base_ionsn_2=widget_base(base_ionsn, /column)
  base_blocks_1 = widget_base(base_ionsn_1, /column)
;  base_ground = widget_base(base_blocks_1, /column)
  base_ionsn_1_1=widget_base(base_blocks_1, /column, /frame)
  base_ionsn_1_2=widget_base(base_blocks_1, /column, /frame)
  base_ionsn_1_3=widget_base(base_blocks_1, /column, /frame)
  base_ionsn_1_4=widget_base(base_blocks_1, /column, /frame)
  base_blocks_2 = widget_base(base_ionsn_1, /row, /frame)
;  base_met = widget_base(base_blocks_2, /column)
;  base_ionsn_1_3=widget_base(base_met, /column, /frame)
;  base_ionsn_1_4=widget_base(base_met, /column, /frame)
;  base_ionsn_1_5=widget_base(base_ionsn_1, /column, /frame)

  base_ion_selected = widget_base(base_ionsn_1_1, /column)
  base_ion_stage = widget_base(base_ion_selected, /row)
  lab_ion_stage_1 = widget_label(base_ion_stage, value = 'Ionisation range')
  txt_ion_stage_1 = widget_text(base_ion_stage, value = ' ' ,xsize=3,font=font_large,/edit)
  lab_ion_stage_2 = widget_label(base_ion_stage, value = ' - ')
  txt_ion_stage_2 = widget_text(base_ion_stage, value = ' ' ,xsize=3,font=font_large,/edit)

  lab_rul_dirct = widget_label(base_ionsn_1_2, value = 'Direct ionisation rules from the ground')
  base_rules_dirct = widget_base(base_ionsn_1_2, /row, /align_left)
  lab_rul_dirct_shl = widget_label(base_rules_dirct, value = 'Lowest shell to ionise from')
  txt_rul_dirct_shl = widget_text(base_rules_dirct, value=' ',xsize=3,font=font_large,/edit)
;  lab_rul_dirct = widget_label(base_ionsn_1_2, value = 'Parent + excited electron')
;  base_rul_dirct_prnt = widget_base(base_ionsn_1_2, /row)
;  base_rules_dirct_prnt_1 = widget_base(base_rul_dirct_prnt, /row, /align_left)
;  lab_rul_dirct_prnt = widget_label(base_rules_dirct_prnt_1 , value = 'Max shell')
;  txt_rul_dirct_prnt_shl = widget_text(base_rules_dirct_prnt_1 , value=' ',xsize=3,font=font_large,/edit)
;  base_rules_dirct_prnt_2 = widget_base(base_rul_dirct_prnt, /row, /align_left)
;  lab_rul_dirct_prnt = widget_label(base_rules_dirct_prnt_2, value = 'Max l value allowed')
;  txt_rul_dirct_prnt_mxl = widget_text(base_rules_dirct_prnt_2, value=' ',xsize=3,font=font_large,/edit)

  lab_rul_indrct = widget_label(base_ionsn_1_3, value = 'Excitation Autoionisation rules')
  base_rules_ind_1 = widget_base(base_ionsn_1_3, /row, /align_left)
  lab_rul_ind_dn_1 = widget_label(base_rules_ind_1, value = 'Lowest shell to excite from')
  txt_rul_ind_dnmin = widget_text(base_rules_ind_1, value=' ',xsize=3,font=font_large,/edit)
  base_rul_ind_ea = widget_base(base_ionsn_1_3, /row)
  base_rules_ind_2 = widget_base(base_rul_ind_ea, /row, /align_left)
  lab_rul_ind_dnmax = widget_label(base_rul_ind_ea, value = 'Max excited shell')
  txt_rul_ind_dnmax = widget_text(base_rul_ind_ea, value=' ',xsize=3,font=font_large,/edit)
  base_rules_ind_3 = widget_base(base_rul_ind_ea, /row, /align_left)
  lab_rul_ind_mxl = widget_label(base_rules_ind_3, value = 'Max excited l allowed')
  txt_rul_ind_mxl = widget_text(base_rules_ind_3, value=' ',xsize=3,font=font_large,/edit)
  

  ps_lab_rul_dirct = widget_label(base_ionsn_1_4, value = 'Define parent + excited shell')
  ps_base_rules_dirct_1 = widget_base(base_ionsn_1_4, /row, /align_left)
  ps_base_rules_dirct_2 = widget_base(base_ionsn_1_4, /row, /align_left)
  ps_lab_rul_dirct_shl_vac = widget_label(ps_base_rules_dirct_1, value = 'Vacancy orbital')
  ps_txt_rul_dirct_shl_vac = widget_text(ps_base_rules_dirct_1, value=' ',xsize=3,font=font_large,/edit)
  ps_lab_rul_dirct_shl = widget_label(ps_base_rules_dirct_2, value = 'Lowest shell to ionise from')
  ps_txt_rul_dirct_shl = widget_text(ps_base_rules_dirct_2, value=' ',xsize=3,font=font_large,/edit)
  ps_base_1 = widget_base(base_ionsn_1_4, /row)
  ps_base_rul_dirct_prnt = widget_base(ps_base_1, /row)
  ps_base_rules_dirct_prnt_1 = widget_base(ps_base_rul_dirct_prnt, /row, /align_left)
  ps_lab_rul_dirct_prnt = widget_label(ps_base_rules_dirct_prnt_1 , value = 'Max shell')
  ps_txt_rul_dirct_prnt_shl = widget_text(ps_base_rules_dirct_prnt_1 , value=' ',xsize=3,font=font_large,/edit)
  ps_base_rules_dirct_prnt_2 = widget_base(ps_base_1, /row, /align_left)
  ps_lab_rul_dirct_prnt = widget_label(ps_base_rules_dirct_prnt_2, value = 'Max l')
  ps_txt_rul_dirct_prnt_mxl = widget_text(ps_base_rules_dirct_prnt_2, value=' ',xsize=3,font=font_large,/edit)

  but_ground_proc = widget_button(base_blocks_1, value = ' Evaluate ionisation configurations')

;  base_met_rules = widget_base(base_ionsn_1_3, /column, /frame)
;  lab_met_rules = widget_label(base_met_rules, value ='METASTABLE RULES')
;  base_met_enter = widget_base(base_met_rules, /row)
;  lab_met_enter = widget_label(base_met_enter, value='Enter ion charge')
;  txt_met_enter = widget_text(base_met_enter, value=' ', xsize=3, /edit)
;  lab_met_rules_1 = widget_label(base_ionsn_1_3, value ='Direct ionisation rules')
;  base_rules_dirct_met = widget_base(base_ionsn_1_3, /row, /align_left)
;  lab_rul_dirct_shl_met = widget_label(base_rules_dirct_met, value = 'lowest shell to ionise from')
;  txt_rul_dirct_shl_met = widget_text(base_rules_dirct_met, value=' ',xsize=3,font=font_large,/edit)
;;  lab_rul_dirct_met = widget_label(base_ionsn_1_3, value = 'parent + excited electron')
;;  base_rul_dirct_prnt_met = widget_base(base_ionsn_1_3, /row)
;;  base_rules_dirct_prnt_met_1 = widget_base(base_rul_dirct_prnt_met, /row, /align_left)
;;  lab_rul_dirct_prnt_met = widget_label(base_rules_dirct_prnt_met_1 , value = 'max shell')
;;  txt_rul_dirct_prnt_met_shl = widget_text(base_rules_dirct_prnt_met_1 , value=' ',xsize=3,font=font_large,/edit)
;;  base_rules_dirct_prnt_met_2 = widget_base(base_rul_dirct_prnt_met, /row, /align_left)
;;  lab_rul_dirct_prnt_met = widget_label(base_rules_dirct_prnt_met_2, value = 'max l value allowed')
;;  txt_rul_dirct_prnt_met_mxl = widget_text(base_rules_dirct_prnt_met_2, value=' ',xsize=3,font=font_large,/edit)
;
;  lab_rul_indrct_met = widget_label(base_ionsn_1_4, value = 'indirect ionisation rules')
;  base_rules_ind_met_1 = widget_base(base_ionsn_1_4, /row, /align_left)
;  lab_rul_ind_dn_met_1 = widget_label(base_rules_ind_met_1, value = 'lowest shell to ionise from')
;  txt_rul_ind_dnmin_met = widget_text(base_rules_ind_met_1, value=' ',xsize=3,font=font_large,/edit)
;  base_rul_ind_ea_met = widget_base(base_ionsn_1_4, /row)
;  base_rules_ind_met_2 = widget_base(base_rul_ind_ea_met, /row, /align_left)
;  lab_rul_ind_dnmax_met = widget_label(base_rul_ind_ea_met, value = 'max ea shell')
;  txt_rul_ind_dnmax_met = widget_text(base_rul_ind_ea_met, value=' ',xsize=3,font=font_large,/edit)
;  base_rules_ind_met_3 = widget_base(base_rul_ind_ea_met, /row, /align_left)
;  lab_rul_ind_mxl_met = widget_label(base_rules_ind_met_3, value = 'max ea l allowed')
;  txt_rul_ind_mxl_met = widget_text(base_rules_ind_met_3, value=' ',xsize=3,font=font_large,/edit)
;
;  base_met_selected = widget_base(base_blocks_2, /column)
;  base_ion_selected_ch_met = widget_base(base_met_selected, /row)
;  lab_ion_selected_met = widget_label(base_ion_selected_ch_met, value = 'Metastable')
;  txt_ion_selected_met = widget_text(base_ion_selected_ch_met, value = ' ',/edit)
;  but_met_proc = widget_button(base_met_selected, value = ' Evaluate ionisation configurations')
;  
;  ps_base_met=widget_base(base_met_selected, /column, /frame)
;  ps_lab_rul_dirct_met = widget_label(ps_base_met, value = 'Parent + excited')
;  ps_base_rules_dirct_met = widget_base(ps_base_met, /row, /align_left)
;  ps_lab_rul_dirct_shl_met = widget_label(ps_base_rules_dirct_met, value = 'Lowest shell to ionise from')
;  ps_txt_rul_dirct_shl_met = widget_text(ps_base_rules_dirct_met, value=' ',xsize=3,font=font_large,/edit)
;  ps_base_met_1 = widget_base(ps_base_met, /row)
;  ps_base_rul_dirct_prnt_met = widget_base(ps_base_met_1, /row)
;  ps_base_rules_dirct_prnt_met_1 = widget_base(ps_base_rul_dirct_prnt_met, /row, /align_left)
;  ps_lab_rul_dirct_prnt_met = widget_label(ps_base_rules_dirct_prnt_met_1 , value = 'Max shell')
;  ps_txt_rul_dirct_prnt_shl_met = widget_text(ps_base_rules_dirct_prnt_met_1 , value=' ',xsize=3,font=font_large,/edit)
;  ps_base_rules_dirct_prnt_met_2 = widget_base(ps_base_met_1, /row, /align_left)
;  ps_lab_rul_dirct_prnt_met = widget_label(ps_base_rules_dirct_prnt_met_2, value = 'Max l')
;  ps_txt_rul_dirct_prnt_mxl_met = widget_text(ps_base_rules_dirct_prnt_met_2, value=' ',xsize=3,font=font_large,/edit)


		;***********************
		;**** Error message ****
		;***********************
                
  messid = widget_label(parent,font=font_large, $
	value='Edit the processing options data and press Done to proceed')



		;**********************************
		;**** Finally the exit buttons ****
		;**********************************
                
  base     = widget_base(parent,/row)
  menufile = bitfile + '/menu.bmp'
  read_X11_bitmap, menufile, bitmap1
  outid    = widget_button(base, value = bitmap1)
  cancelid = widget_button(base,value='Cancel',font=font_large)
  doneid   = widget_button(base,value='Done',font=font_large)
;  stopid   = widget_button(base,value='Stop',font=font_large,event_pro='h8stop')
  
  
		;*************************************************
		;**** create a state structure for the pop-up ****
		;**** window.                                 ****
		;*************************************************
                
;  nval=intarr(100)
  nval_str=strarr(100)
;  config=strarr(100)
;  prom_config=strarr(100,100)
prom_nlev=intarr(100,100)
prom_pars=intarr(100,100)
prom_lastl=strarr(100,100)
ioccup_store=intarr(100,100,36)
;  table_data1_indx=intarr(100)
;  table_data2_indx=intarr(100)
;  iz0=0
;  iz1=intarr(100)
grndl=strarr(100)
;  ion_min=0
;  ion_max=0
  nconf=intarr(100)
;  igrndpar=intarr(100)

nval=options.nval
;nval_str=options.nval_str
config=options.config
prom_config=options.prom_config
;prom_pars=options.prom_pars
;prom_lastl=options.prom_lastl
table_data1_indx=options.table_data1_indx
table_data2_indx=options.table_data2_indx
table_data1=options.table_data1
table_data2=options.table_data2
iz0=options.iz0
iz1=options.iz1
ionpot=options.ionpot
;grndl=options.grndl
ion_min=options.ion_min
ion_max=options.ion_max
;ion_size=ion_max-ion_min+1
ion_size=80
;nconf=options.nconf
igrndpar=options.igrndpar
if cadwion_on eq 1 then begin
	nummet=3
	numcfgs=50
	numshls=36
	ion_configs_ea=strarr(ion_size,numcfgs)
	ion_configs_dirct=strarr(ion_size,numcfgs)
	ion_configs_prnt=strarr(ion_size,numcfgs)
;	ion_configs_ea_met=strarr(ion_size,numcfgs)
;	ion_configs_dirct_met=strarr(ion_size,numcfgs)
;	ion_configs_prnt_met=strarr(ion_size,numcfgs)
	ioccup_ionsn_grnds=intarr(ion_size,numcfgs,numcfgs,numshls)
	ioccup_ionsn_ea_grnds=intarr(ion_size,numcfgs,numshls)
	ioccup_ionsn_dirct=intarr(ion_size,nummet,numcfgs,numshls)
	ioccup_ionsn_prnt=intarr(ion_size,nummet,numcfgs,numshls)
	ioccup_ionsn_ea=intarr(ion_size,nummet,10,numcfgs,numshls)
	grnd_indx=intarr(ion_size)
        icount_dirct=intarr(ion_size,nummet)
        icount_prnt=intarr(ion_size,nummet)
        icount_ea=intarr(ion_size,nummet,10)
        ea_indx_set=intarr(ion_size,nummet)
	dirct_parity=intarr(ion_size,nummet,numcfgs)
        dirct_lbl=strarr(ion_size,nummet,numcfgs)
        ea_grnd_lbl=strarr(ion_size,nummet,numcfgs)
        ea_ex_lbl=strarr(ion_size,nummet,20,numcfgs)
endif else begin
	ion_configs_ea=-1
	ion_configs_dirct=-1
	ion_configs_prnt=-1
;	ion_configs_ea_met=-1
;	ion_configs_dirct_met=-1
;	ion_configs_prnt_met=-1
	ioccup_ionsn_grnds=-1
	ioccup_ionsn_ea_grnds=-1
	ioccup_ionsn_dirct=-1
	ioccup_ionsn_prnt=-1
	ioccup_ionsn_ea=-1
	grnd_indx=-1
        icount_dirct=-1
        icount_prnt=-1
        icount_ea=-1
        ea_indx_set=-1
	dirct_parity=-1
        dirct_lbl=-1
        ea_grnd_lbl=-1
        ea_ex_lbl=-1
endelse

;stop 

  new_state = { ps              :       ps,             $
;                tempid          :       tempid,         $
;	        deftid          :       deftid,         $
                outid           :       outid,          $
                cancelid        :       cancelid,       $
                doneid          :       doneid,         $
	        messid          :       messid,         $
                num_form        :       num_form,       $
                font            :       font_large,     $
;		txt1		:	txt1,		$
;		txt2		:	txt2,		$
;		txt3		:	txt3,		$
;		txt4		:	txt4,		$
;	        but1		:	but1,		$
		tab_val_sel	:	tab_val_sel,	$
		base_rul_1	:	base_rul_1,	$
		val_buts	:	val_buts,	$
		table_data1	:	table_data1,	$
		table_data2	:	table_data2,	$
		but_set_defs	:	but_set_defs,	$
;		but_set_defs_all:	but_set_defs_all,	$
		txt1_rul_2	:	txt1_rul_2,	$
		txt2_rul_2	:	txt2_rul_2,	$
		txt1_rul_3	:	txt1_rul_3,	$
		txt2_rul_3	:	txt2_rul_3,	$
		but_prom	:	but_prom,	$
		txt_prom_2	:	txt_prom_2,	$
		txt_prom_3	:	txt_prom_3,	$
		nval		:	nval,		$
		config		:	config,		$
		iz0		:	iz0,		$
		iz1		:	iz1,		$
		prom_config	:	prom_config,	$
		prom_pars	:	prom_pars,	$
		PROM_NLEV	:	PROM_NLEV,	$
		base_sw_1	:	base_sw_1,	$
		base_sw_2	:	base_sw_2,	$
		base_sel	:	base_sel,	$
		ntxt1_rul_2	:	ntxt1_rul_2,	$
		ntxt2_rul_2	:	ntxt2_rul_2,	$
		ntxt1_rul_3	:	ntxt1_rul_3,	$
		ntxt2_rul_3	:	ntxt2_rul_3,	$
		ntxt1_rul_4	:	ntxt1_rul_4,	$
		ntxt2_rul_4	:	ntxt2_rul_4,	$
		ntxt1_rul_5	:	ntxt1_rul_5,	$
		ntxt2_rul_5	:	ntxt2_rul_5,	$
		table_data1_indx:	table_data1_indx,$
		table_data2_indx:	table_data2_indx,$
		nval_str	:	nval_str,	$
;		conf_ed_id	:	conf_ed_id,	$
		txt_conf1	:	txt_conf1,	$
		but_sel_all_all	:	but_sel_all_all, $
		but_desel_all_all:	but_desel_all_all, $
		but_sel_all_one	:	but_sel_all_one, $
		but_desel_all_one:	but_desel_all_one, $
		igrndpar	:	igrndpar,	$
		prom_lastl	:	prom_lastl, 	$
		grndl		:	grndl,		$
		el_sym		:	options.el_sym, $
		ion_min		:	ion_min,	$
		ion_max		:	ion_max,	$
		nconf		:	nconf, $
                PREVIEWDATA     :       ptrarr(100), $
                GOTPREVIEWDATA  :       intarr(100), $
                ATOMDATA        :       ptrarr(100), $
                GOTATOMDATA     :       lonarr(100), $
                SELECT_BASE     :       SELECT_BASE, $
                SELECT_LMIN     :       SELECT_LMIN, $
                SELECT_LMAX     :       SELECT_LMAX, $
                SELECT_CMIN     :       SELECT_CMIN, $
                SELECT_ONOFF    :       SELECT_ONOFF, $
                SELECT_BUT      :       SELECT_BUT, $
                XLOGON          :       XLOGON, $
                XLOGOFF         :       XLOGOFF, $
                YLOGON          :       YLOGON, $
                YLOGOFF         :       YLOGOFF, $
                CMIN            :       CMIN, $
                CMAX            :       CMAX, $
                LMIN            :       LMIN, $
                LMAX            :       LMAX, $
;                REPLOT          :       REPLOT, $
                OPTIONS         :       OPTIONS, $
                CONFIG_LABEL    :       CONFIG_LABEL, $
                CONFIG_NLEVS    :       CONFIG_NLEVS, $ 
                CONFIG_SPLIT_BUT:       CONFIG_SPLIT_BUT, $
                CONFIG_SPLIT_MAN:       CONFIG_SPLIT_MAN, $
                SWITCH_CFG      :       SWITCH_CFG, $
                SWITCH_GRF      :       SWITCH_GRF, $
                SWITCH_CNT      :       SWITCH_CNT, $
                switch_ionsn      :       switch_ionsn, $
                TXT_BASE_NIONSEL:       TXT_BASE_NIONSEL, $
                TXT_BASE_NION   :       TXT_BASE_NION, $
                RUN_ENERGY      :       RUN_ENERGY, $
;                RUN_AVALUE      :       RUN_AVALUE, $
                MANUAL_SPLIT    :       MANUAL_SPLIT, $
                BASE_R:BASE_R, $
                BASE_G:BASE_G, $
                BASE_C:BASE_C, $
                base_ionsn:base_ionsn, $
                  TE_04:OPTIONS.TE_04, $
                  NTE_04:OPTIONS.NTE_04, $
                  TE_UNIT_04:OPTIONS.TE_UNIT_04, $
                  TE_11:OPTIONS.TE_11, $
                  NTE_11:OPTIONS.NTE_11, $
                  TE_UNIT_11:OPTIONS.TE_UNIT_11, $
                  TE_15:OPTIONS.TE_15, $
                  NTE_15:OPTIONS.NTE_15, $
                  TE_UNIT_15:OPTIONS.TE_UNIT_15, $
                CNT_YMIN        :       CNT_YMIN, $
                CNT_YMAX        :       CNT_YMAX, $
                CNT_YLOGON      :       CNT_YLOGON, $
                CNT_YLOGOFF     :       CNT_YLOGOFF, $
                CNT_XMIN        :       CNT_XMIN, $
                CNT_XMAX        :       CNT_XMAX, $
                SEL_LEV_BUT     :       SEL_LEV_BUT, $
                TOT_LEV_BUT     :       TOT_LEV_BUT, $
                SEL_CFG_BUT     :       SEL_CFG_BUT, $
                TOT_CFG_BUT     :       TOT_CFG_BUT, $
                SEL_LEV_TXT     :       SEL_LEV_TXT, $
                TOT_LEV_TXT     :       TOT_LEV_TXT, $
                SEL_CFG_TXT     :       SEL_CFG_TXT, $
                TOT_CFG_TXT     :       TOT_CFG_TXT, $
                PLOT_WID        :       PLOT_WID, $
                PLOT_CNT_WID    :       PLOT_CNT_WID, $	
		IONPOT          :       IONPOT, $
                IOCCUP_STORE:IOCCUP_STORE,$
	        but_ground_proc: but_ground_proc,$
;	        but_met_proc: but_met_proc,$
	        txt_ion_stage_1:txt_ion_stage_1,$
	        txt_ion_stage_2:txt_ion_stage_2,$
	        txt_rul_ind_dnmin:txt_rul_ind_dnmin,$
	        txt_rul_ind_dnmax:txt_rul_ind_dnmax,$
	        txt_rul_ind_mxl:txt_rul_ind_mxl,$
	        ion_configs_ea:ion_configs_ea,$
	        ion_configs_dirct:ion_configs_dirct,$
	        ion_configs_prnt:ion_configs_prnt,$
	        txt_rul_dirct_shl:txt_rul_dirct_shl,$
;	        txt_rul_dirct_prnt_shl:txt_rul_dirct_prnt_shl,$
;	        txt_rul_dirct_prnt_mxl:txt_rul_dirct_prnt_mxl,$
	        ps_txt_rul_dirct_shl:ps_txt_rul_dirct_shl,$
	        ps_txt_rul_dirct_prnt_shl:ps_txt_rul_dirct_prnt_shl,$
	        ps_txt_rul_dirct_prnt_mxl:ps_txt_rul_dirct_prnt_mxl,$
		ps_txt_rul_dirct_shl_vac:ps_txt_rul_dirct_shl_vac,$
;	        ps_txt_rul_dirct_shl_met:ps_txt_rul_dirct_shl_met,$
;	        ps_txt_rul_dirct_prnt_shl_met:ps_txt_rul_dirct_prnt_shl_met,$
;	        ps_txt_rul_dirct_prnt_mxl_met:ps_txt_rul_dirct_prnt_mxl_met,$
;	        txt_rul_dirct_shl_met:txt_rul_dirct_shl_met,$
;	        txt_rul_ind_dnmin_met:txt_rul_ind_dnmin_met,$
;	        txt_rul_ind_dnmax_met:txt_rul_ind_dnmax_met,$
;	        txt_rul_ind_mxl_met:txt_rul_ind_mxl_met,$
;;	        txt_rul_dirct_prnt_met_shl:txt_rul_dirct_prnt_met_shl,$
;;	        txt_rul_dirct_prnt_met_mxl:txt_rul_dirct_prnt_met_mxl,$
;	        ion_configs_ea_met:ion_configs_ea_met,$
;	        ion_configs_dirct_met:ion_configs_dirct_met,$
;	        ion_configs_prnt_met:ion_configs_prnt_met,$
		dirct_parity:dirct_parity,$
;	        txt_met_enter  : txt_met_enter,$
;	        txt_ion_selected_met : txt_ion_selected_met,$
;	        lab_met_enter : lab_met_enter,$
	 	config_split_but_ion : config_split_but_ion,$
	        ioccup_ionsn_grnds : ioccup_ionsn_grnds,$
	        ioccup_ionsn_ea_grnds : ioccup_ionsn_ea_grnds,$
	        ioccup_ionsn_dirct : ioccup_ionsn_dirct,$
	        ioccup_ionsn_prnt : ioccup_ionsn_prnt,$
	        ioccup_ionsn_ea : ioccup_ionsn_ea,$
		grnd_indx:grnd_indx,$
        	icount_dirct:icount_dirct,$
        	icount_prnt:icount_prnt,$
        	icount_ea:icount_ea,$
        	ea_indx_set:ea_indx_set,$
        	dirct_lbl:dirct_lbl,$
        	ea_grnd_lbl:ea_grnd_lbl,$
        	ea_ex_lbl:ea_ex_lbl,$
		cadwion_on:cadwion_on, $			
		passdir:passdir, $
		fortdir:fortdir}

                ;**** Save initial state structure ****

 	;widget_control, state.val_buts, get_value=open_val
	;if (open_val eq 0) then begin
	;   widget_control,tab_val_sel, set_value=table_data1
	;endif
	;if (open_val eq 1) then begin
	;   widget_control,state.tab_val_sel, set_value=state.table_data2
	;endif


    widget_control, first_child, set_uvalue=new_state;, /no_copy

  RETURN, parent

END

