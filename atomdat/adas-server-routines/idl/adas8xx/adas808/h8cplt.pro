; SCCS INFO Module: @(#)$Header: /home/adascvs/idl/adas8xx/adas808/h8cplt.pro,v 1.1 2004/07/06 14:01:21 whitefor Exp $  Data $Date: 2004/07/06 14:01:21 $
pro h8cplt,state

	if !D.name ne 'PS' then device,get_visual_name=colourtype else colourtype=''

        if colourtype eq 'TrueColor' then begin
                print,'TrueColour Display Detected'
                red=255L
                blue=(256L^2)*255L
                green=256L*255L
                white=255L+256L*(255L + 256L*255L)
                black=0L
                yellow=256L*(255L+256L*255L)
        endif else begin
                print,'Loadable Colour Table Display Detected'
                tvlct,255,255,255,1
		tvlct,0,0,255,2
		tvlct,255,0,0,3
		tvlct,0,255,0,4
		tvlct,0,255,255,5
		tvlct,0,0,0,6
                white=1
                red=3
                blue=2
                green=4
                black=6
                yellow=5        
        endelse


        widget_control,state.sel_lev_but,get_uvalue=type
        widget_control,state.plot_cnt_wid,get_value=wid
        wset,wid


        calculate_config=state.manual_split
        configuration=state.prom_config
        nconf=intarr(100)
        
        for ion_ch=state.ion_min,state.ion_max do begin
                if state.gotpreviewdata[ion_ch] gt 0 then begin 
                        data=*(state.previewdata[ion_ch])
                        for i=0,(size(state.select_onoff))[1]-1 do begin
                                widget_control,state.select_cmin[i],get_value=a
                                cmin=double(a[0])
                                widget_control,state.select_lmin[i],get_value=a
                                lmin=double(a[0])
                                widget_control,state.select_lmax[i],get_value=a
                                lmax=double(a[0])
                
                                for j=0,(size(data.aval))[0] eq 0 ? 0:(size(data.aval))[1]-1 do begin
                                        if data.aval[j] gt cmin and data.wavel[j] gt lmin and data.wavel[j] lt lmax then begin
                                                calculate_config[ion_ch,data.conf1[j]]=1        
                                                calculate_config[ion_ch,data.conf2[j]]=1        
                                        endif
                                end
        
        
                        end
                endif
        end



	nlevs=state.prom_nlev
	nconf=state.nconf
	total=long(total(nlevs))
	widget_control,state.tot_lev_txt,set_value=strtrim(string(total),2)
	total=long(total(nconf))
	widget_control,state.tot_cfg_txt,set_value=strtrim(string(total),2)
	for ion_ch=state.ion_min,state.ion_max do begin
		for i=0,state.nconf[ion_ch]-1 do begin
			if calculate_config[ion_ch,i] eq 0 then begin
				nconf[ion_ch]=nconf[ion_ch]-1
				nlevs[ion_ch,i]=0
                        endif
		end
	end
	total=long(total(nlevs))
	widget_control,state.sel_lev_txt,set_value=strtrim(string(total),2)
	total=long(total(nconf))
	widget_control,state.sel_cfg_txt,set_value=strtrim(string(total),2)


        
        if type eq 0 then begin
                nlevs=state.prom_nlev
                ; now see which are selected
                for ion_ch=state.ion_min,state.ion_max do begin
                        for i=0,state.nconf[ion_ch] do begin
                                ;print,'grrg'
                                if calculate_config[ion_ch,i] eq 0 then begin
                                        ;print,nlevs[ion_ch,i],ion_ch,i
                                        nlevs[ion_ch,i]=0
                                endif
                        end
                end

        endif
        if type eq 1 then begin
                nlevs=state.prom_nlev
        endif
        if type eq 2 then begin
                nlevs=fltarr(100,1)
                for ion_ch=state.ion_min,state.ion_max do begin
                        nlevs[ion_ch,0]=total(calculate_config[ion_ch,*])
                end
        endif
        if type eq 3 then begin
                nlevs=fltarr(100,1)
                nlevs[*,0]=state.nconf
                ; now see which are selected
        endif
        
        

        ;stop
        
        xdata=dblarr(state.ion_max-state.ion_min+1)
        ydata=dblarr(state.ion_max-state.ion_min+1)
        j=0
        for i=state.ion_min,state.ion_max do begin
                xdata[j]=i
                ydata[j]=total(nlevs[i,*])
                j=j+1
        end


        widget_control,state.cnt_ymin,get_value=a
        min=float(a)
        widget_control,state.cnt_ymax,get_value=a
        max=float(a)                
        yrange=[min,max]
        widget_control,state.cnt_xmin,get_value=a
        min=float(a)
        widget_control,state.cnt_xmax,get_value=a
        max=float(a)                
        xrange=[min,max]

        if xrange[0] eq 0. and xrange[1] eq 0. then begin
                xrange[0]=state.ion_min-1
                xrange[1]=state.ion_max+1
        endif
        
        widget_control,state.cnt_ylogon,get_uvalue=ylog

	if ylog eq 1 and yrange[0] eq 0 then begin
		plot,xdata,ydata,xrange=xrange,/nodata,/ylog
		yrange[0]=.9
		yrange[1]=10^(!y.crange[1])
		ystyle=1
	endif

        if (where(ydata ne 0))[0] eq -1 then return

        plot,xdata,ydata,ylog=ylog,xrange=xrange,yrange=yrange,/nodata,color=black,background=white,xstyle=1,ystyle=ystyle

;        for i=0,j-1 do plots,[xdata[i],xdata[i]],[!y.type eq 1 ? 10.^!y.crange[0]:!y.crange[0],ydata[i] gt (!y.type eq 1 ? 10.^!y.crange[0]:!y.crange[0]) ? ydata[i] : (!y.type eq 1 ? 10.^!y.crange[0]:!y.crange[0])],color=blue
         for i=0,j-1 do begin
                xmin=!x.crange[0]
                xmax=!x.crange[1]
                if !x.type eq 1 then xmin=10.^xmin
                if !x.type eq 1 then xmax=10.^xmax
                
                if xdata[i] gt xmin and xdata[i] lt xmax then begin
                
                
        
                        plots,[xdata[i],xdata[i]],[!y.type eq 1 ? 10.^!y.crange[0]:!y.crange[0],ydata[i] gt (!y.type eq 1 ? 10.^!y.crange[0]:!y.crange[0]) ? ydata[i] : (!y.type eq 1 ? 10.^!y.crange[0]:!y.crange[0])],color=blue
                endif

         end


end
