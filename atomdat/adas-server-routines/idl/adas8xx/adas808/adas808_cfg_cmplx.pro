; Copyright (c) 2006, Strathclyde University.
;----------------------------------------------------------------------
;+
; PROJECT    :  ADAS
;
; NAME       :  adas808_cfg_cmplx
;
; PURPOSE    :  Gives all possible configurations of nel electrons
;		in a given n shell
;
; EXPLANATION:
;       This routine calculates all the possible ways to put nel
;	electrons into the n l-shells with a principal quantum
;	number of n. It returns a list of occupation numbers
;	indexed by l 
;
; USE:
;       An example;
;               n	=	3
;		nel	=	3
;		cfgs	=	complexes(n,nel)
;		for i=0,(size(cfgs))[1]-1 do print,reform(cfgs[i,*])
;	gives:
;		2       1       0
;		2       0       1
;		1       2       0
;		1       1       1
;		1       0       2
;		0       3       0
;		0       2       1
;		0       1       2
;		0       0       3
;
; OPTIONAL INPUTS:
;       l = l value to start at (zero by default, used for recursion)
;
; OUTPUTS:
;       This function an array nxn_conf
;
; OPTIONAL OUTPUTS:
;       None.
;
; KEYWORD PARAMETERS:
;       None.
;
; CALLS:
;      	Recursive
;
; SIDE EFFECTS:
;
; AUTHOR     :  Allan Whiteford
; 
; DATE       :  24-08-06
; 
;
; MODIFIED:
;       1.1     Allan Whiteford
;              	- First version.
;
; VERSION:
;       1.1    24-08-06
;-
;----------------------------------------------------------------------


function adas808_cfg_cmplx,n,nel,l=l

	if not keyword_set(l) then l=0

	if nel lt 0 then return,-1
        
        val=intarr(1,n)
        
        if n eq 1 then begin
		if nel gt 4*l+2 then return,-1
                return,reform(nel,1,1)
        endif
        
        for i=nel < 4*l+2,0,-1 do begin
		a=adas808_cfg_cmplx(n-1,nel-i,l=l+1)
		if a[0] ne -1 then begin
			b=intarr((size(a))[1],(size(a))[2]+1)
			b[*,0]=i
			for j=0,(size(a))[1]-1 do begin
				b[j,1:*]=a[j,*]
			end
			val=[val,b]
		endif
        end

        if n_elements(val) eq 1 then begin
        	return,-1
        endif else begin
        	return,val[1:*,*]
        endelse
end        
	
