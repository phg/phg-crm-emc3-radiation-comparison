;-----------------------------------------------------------------------------
;+
;
; NAME     : run_adas808
;
; PURPOSE  : generate configuration sets for the ions of elements and 
;            write all ls-, ic- and ca-coupling adf34 and adf42 driver and 
;            data files.  Execute configuration average Cowan structure 
;            calculation and write ca- type 1 and type 3 adf04 files.
;            Create ls- and ic- adf04 files.  Create  ls- ic- and ca- 
;            adf15 and adf40 and adf11 (partial plt) files. 
;
; CATEGORY : ADAS
;
; NOTES    :
;  
; USE      : This is a set up procedure for both offline and online use of
;            adas801, adas808, adas810.  It is designed for economised
;            handling of heavy species and subsequent use in a partitioned
;            superstage framework.
;
;            Use of consistent temperature, density, wavelength ranges 
;            throughout large scale production is achieved by inclusion
;            of defaults sets in the procedure with working subsets 
;            specified by indexing vectors
;
;                 indx_theta: pointing to the temperature vector theta
;                 indx_rho  : pointing to the density vector rho
;                 indx_wvl  : pointing to the npix, wvlmin, wvlmax vectors
;
;            The default ADAS subsets are adopted by omitting the keywords
;            in calling the procedure. 
;
;            By default, the temperatures theta[] are treated as reduced 
;            to be scaled with the ion charge, z, as te[]=(z+1)^2*theta[],
;            and are treated as absolute by using the keyword /theta_noscale
;            
;            By default the densities rho[] are treated as absolute are 
;            scaled only if the keyword /rho_scale is used.  Then
;            dens[]=rho[]/(z+1)^7. 
;
;            The internal settings are:
 
;            theta = [ 2.00e+02, 3.00e+02, 5.00e+02, 7.00e+02, 1.00e+03,
;                      1.50e+03, 2.00e+03, 3.00e+03, 5.00e+03, 7.00e+03, 
;                      1.00e+04, 1.50e+04, 2.00e+04, 3.00e+04, 5.00e+04,
;                      7.00e+04, 1.00e+05, 1.50e+05, 2.00e+05, 3.00e+05, 
;                      5.00e+05, 1.00e+06, 2.00e+06, 5.00e+06, 1.00e+07]
;           
;            indx_theta = [ 0, 2, 4, 6, 8,10,12,14,16,18,20,21,22,23]
;
;            rho   = [ 1.00e-03, 1.00e+00, 1.00e+01, 1.00e+02, 1.00e+03,
;                      1.00e+04, 3.00e+04, 1.00e+05, 3.00e+05, 1.00e+06,
;                      3.00e+06, 1.00e+07, 3.00e+07, 1.00e+08, 3.00e+08,
;                      1.00e+09, 3.00e+09, 1.00e+10, 3.00e+10, 1.00e+11,
;                      3.00e+11, 1.00e+12, 3.00e+12, 1.00e+13, 3.00e+13,
;                      1.00e+14, 3.00e+14, 1.00e+15, 3.00e+15, 1.00e+16]
;           
;            indx_rho   = [17,19,21,23,25,27,29]
;    
;    
;            npix  = [      128,      128,      128,      128,      512,
;                           256,      256,      256,      256,      256]
;
;            wvlmin= [ 1.00e+00, 1.00e+01, 1.00e+02, 1.00e+03, 1.00e+00,
;                      3.00e+00, 7.00e+01, 5.00e+02, 1.00e+03, 3.00e+03]
;
;            wvlmax= [ 1.00e+01, 1.00e+02, 1.00e+03, 1.00e+04, 1.00e+04,
;                      5.00e+00, 1.20e+02, 1.50e+03, 2.00e+03, 7.00e+03]
;
; INPUT    :
;
;   (I*4)   z0_list        = set of nuclear charge of required elements
;   (I*4)   nel_min        = lowest number of bound electrons for ion range
;   (I*4)   nel_max        = largest number of bound electrons for ion range
;   (I*4)   indx_theta[]   = keyword = pointer vector to temperature 
;                                      values  in theta[] to be used.
;   (I*4)   indx_rho[]     = keyword = pointer vector to density 
;                                      values  in rho[] to be used.
;   (I*4)   indx_wvl[]     = keyword = pointer vector to wavelength 
;                                      ranges in npix[],wvlmin[] and
;                                      wvlmax[] to be used.
;   (C  )   a54file        = keyword => 'small', 'medium' or 'large'
;   (I*4)   /theta_noscale = keyword => treat theta[] as absolute 
;   (I*4)   /rho_scale     = keyword => treat rho[] as reduced 
;   (I*4)   /ca_only       = keyword => compute only config. average results  
;   (I*4)   year           = year id for data produced (default=40)
;   (I*4)   donotrun       = keyword => generate all files for 801/810
;                            but stop befeore executing them
;   (C* )   verbose        = file name for verbose output (if desired)
;                                       
;
; ROUTINES: 
;          NAME                            TYPE   COMMENT
;          xxelem                          adas   obtain an element name 
;          xxesym                          adas   returns an element chemical symbol
;          adas8xx_promotion_rules         adas   set promotion rules for any element
;          adas8xx_promotions              adas   create configuration set for an ion
;          adas8xx_create_drivers          adas   write adf34/adf42 drivers for an ion
;          adas8xx_create_ca_adf04         adas   create 'ca' adf04 type 1 and 3 files
;          adas8xx_create_ls_ic_adf04      adas   create 'ls' and 'ic' adf04 type 1 
;                                                 and 3 files
;          adas8xx_create_adf15_adf40      adas   create 'ca', 'ls' and 'ic' adf15, 
;                                                 adf40 and adf11(partial plt) files
;
;
; CATEGORY:
;       Adas system.
;
; WRITTEN:
;       H. P. Summers, University of Strathclyde,22-08-06.
;
; MODIFIED:
;       1.1     H. P. Summers
;               First version put into CVS.
;
;       1.2     Adam Foster
;               - Substantial rewrite to accommodate heavy species:
;                 - inclusion of ca_only keyword
;                 - changed to use adf54 file for promotion rules
;                 - use long integers for term and level counts
;                 - added Cowan scale factors keyword
;                 - added year, verbose, donotrun keywords
;                 - use theta structure to supply custom plasma
;                 - conditions
;
; VERSION:
;       1.1   25-08-06
;       1.2   16-12-08
; 
;-
;-----------------------------------------------------------------------------

PRO run_adas808,                z0_list       = z0_list,               $
                                nel_min       = nel_min,               $
                                nel_max       = nel_max,               $
                                a54file       = a54file,               $
                                plasma        = plasma,                $
                                ca_only       = ca_only,               $
                                only_801      = only_801,              $
                              cowan_scale_factors=cowan_scale_factors, $
                                archive_dir   = archive_dir,           $
                                archive_files = archive_files,         $
                                year          = year,                  $
                                verbose       = verbose,               $
                                donotrun      = donotrun,              $
                                help          = help
common shells, ndshell     ,                                           $
               shmax       , nval        , lval       , shl_lab  ,     $
               iprom_1     , iprom_2     ,                             $
               nrg_cnt_idx , nrg_exl_idx , nrg_el_cnt ,                $
               nval_max    , nval_min    , lval_max   , lval_min ,     $
               njvals 

if keyword_set(help) then begin
   doc_library, 'run_adas808'
   return
endif

lun_verb=0

IF keyword_set(verbose) then begin
  openw, lun_verb, verbose, /get_lun
ENDIF

IF not keyword_set(year) then begin
  print, 'RUS_ADAS808 WARNING: year not set, using '+$
         '40 as default value)'
  year=40
endif

yearstr=strcompress(string(year),/remove_all)
;----------------------------------------------------------------------
; Invariant and default data
;----------------------------------------------------------------------

if NOT keyword_set(plasma) then begin
  print,'RUN_ADAS808 WARNING: plasma conditions undefined: '+$
        'using defaults'
        
  plasma=adas8xx_plasma_defaults(/all)
endif
if (where(strupcase(tag_names(plasma)) EQ $
                   'THETA'))[0] NE -1 THEN BEGIN
  theta = plasma.theta
endif

if (where(strupcase(tag_names(plasma)) EQ $
                   'INDX_THETA'))[0] NE -1 THEN BEGIN
  indx_theta = plasma.indx_theta
endif

if (where(strupcase(tag_names(plasma)) EQ $
                   'THETA_NOSCALE'))[0] NE -1 THEN BEGIN
  theta_noscale = plasma.theta_noscale
endif

if (where(strupcase(tag_names(plasma)) EQ $
                   'RHO'))[0] NE -1 THEN BEGIN
  rho = plasma.rho
endif

if (where(strupcase(tag_names(plasma)) EQ $
                   'INDX_RHO'))[0] NE -1 THEN BEGIN
  indx_rho = plasma.indx_rho
endif

if (where(strupcase(tag_names(plasma)) EQ $
                   'RHO_SCALE'))[0] NE -1 THEN BEGIN
  rho_scale = plasma.rho_scale
endif

if (where(strupcase(tag_names(plasma)) EQ $
                   'NPIX'))[0] NE -1 THEN BEGIN
  npix = plasma.npix
endif

if (where(strupcase(tag_names(plasma)) EQ $
                   'WVLMIN'))[0] NE -1 THEN BEGIN
  wvlmin = plasma.wvlmin
endif

if (where(strupcase(tag_names(plasma)) EQ $
                   'WVLMAX'))[0] NE -1 THEN BEGIN
  wvlmax = plasma.wvlmax
endif

if (where(strupcase(tag_names(plasma)) EQ $
                   'INDX_WVL'))[0] NE -1 THEN BEGIN
  indx_wvl = plasma.indx_wvl
endif


if n_elements(theta) EQ 0 THEN BEGIN
  print, 'RUN_ADAS808 WARNING: theta undefined: using default values'  
  theta=adas8xx_plasma_defaults('theta')
endif

if n_elements(indx_theta) EQ 0 THEN BEGIN
  print, 'RUN_ADAS808 WARNING: indx_theta undefined: using default values'  
  indx_theta=adas8xx_plasma_defaults('indx_theta')
endif

if n_elements(theta_noscale) EQ 0 THEN BEGIN
  print, 'RUN_ADAS808 WARNING: theta_noscale undefined: using default values'  
  theta_noscale=adas8xx_plasma_defaults('theta_noscale')
endif

if n_elements(rho) EQ 0 THEN BEGIN
  print, 'RUN_ADAS808 WARNING: rho undefined: using default values'  
  rho=adas8xx_plasma_defaults('rho')
endif

if n_elements(indx_rho) EQ 0 THEN BEGIN
  print, 'RUN_ADAS808 WARNING: indx_rho undefined: using default values'  
  indx_rho=adas8xx_plasma_defaults('indx_rho')
endif

if n_elements(rho_scale) EQ 0 THEN BEGIN
  print, 'RUN_ADAS808 WARNING: rho_scale undefined: using default values'  
  rho_scale=adas8xx_plasma_defaults('rho_scale')
endif

if n_elements(npix) EQ 0 THEN BEGIN
  print, 'RUN_ADAS808 WARNING: npix undefined: using default values'  
  npix=adas8xx_plasma_defaults('npix')
endif

if n_elements(wvlmin) EQ 0 THEN BEGIN
  print, 'RUN_ADAS808 WARNING: wvlmin undefined: using default values'  
  wvlmin=adas8xx_plasma_defaults('wvlmin')
endif

if n_elements(wvlmax) EQ 0 THEN BEGIN
  print, 'RUN_ADAS808 WARNING: wvlmax undefined: using default values'  
  wvlmax=adas8xx_plasma_defaults('wvlamx')
endif

if n_elements(indx_wvl) EQ 0 THEN BEGIN
  print, 'RUN_ADAS808 WARNING: indx_wvl undefined: using default values'  
  indx_wvl=adas8xx_plasma_defaults('indx_wvl')
endif

if not keyword_set(ca_only) then begin
    ca_only = 0
end else begin
    ca_only = 1
end

;----------------------------------------------------------------------
; check existence of ADF54 file
;----------------------------------------------------------------------
if not keyword_set(a54file) then begin
  print, 'ERROR: run_adas808.pro: ADF54 file must be specified'
  stop
end else begin
    if file_test(a54file) EQ 0 then begin
      print, 'ERROR: run_adas808.pro: ADF54 file '+a54file+$
             ' does not exist'
      stop
    endif
endelse

;----------------------------------------------------------------------
; set up the element and ion ranges
;----------------------------------------------------------------------

if not (keyword_set(nel_min) and keyword_set(nel_max))  then begin
    print," run_adas808: error - nel_min and nel_min keyword values required."
    stop
end else begin
    if nel_min gt nel_max then begin
       print," run_adas808: error - nel_min must be <= nel_max."
       stop
    endif
end


if not keyword_set(z0_list) then begin
  print, 'RUN_ADAS808 error: z0_list not defined'
  stop
endif

z0_list=z0_list[where(z0_list ge nel_min)]

nel=indgen(nel_max-nel_min+1) + nel_min

;print,'z0_list=',z0_list
  
for z0_indx = 0,n_elements(z0_list)-1 do begin

  z0=z0_list[z0_indx]
  z1=indgen(z0)
  zc=indgen(z0)

  list= z0-nel
  list=list[where(list ge 0)]
    
;  print, 'nuclear charge  z0 =', z0

  xxelem,z0,element
   
  elname     = strlowcase(element)
  elsymb     = strlowcase(xxesym(z0))
  plevel     = '01'

;----------------------------------------------------------------------
; set up the rules
;----------------------------------------------------------------------

  for k = 0, n_elements(list)-1 do begin
 
    j = list[k]
   
;-----------------------------------
; note that j and z1[j] are the same   
;-----------------------------------
   
    if keyword_set(verbose) then $
       printf, lun_verb, 'ionisation stage z =', z1[j]
   
    adas8xx_promotion_rules        , z0_nuc      = z0,               $
                                     z_ion      = j,                $
                                     config     = config,              $
                                     ionpot     = ionpot,              $
                                     prom_rules = prom_rules,          $
                                     a54file    = a54file

;stop
;----------------------------------------------------------------------
; generate the configurations for each ion in turn
;----------------------------------------------------------------------

    adas8xx_promotions, z0_nuc      = z0,                                $
                       z_ion      = z1[j],                             $
                       ionpot     = ionpot,                            $
                       prom_rules = prom_rules,                        $
                       promotion_results=promotion_results

    if keyword_set(verbose) then begin
      printf,lun_verb,promotion_results.no_configs,format='("no_configs          =",7i8)'
      printf,lun_verb,promotion_results.no_terms,  format='("no_terms            =",7i8)'
      printf,lun_verb,promotion_results.no_levels, format='("no_levels           =",7i8)'
      printf,lun_verb,'total configs       =',long(total(promotion_results.no_configs))
      printf,lun_verb,'total terms         =',long(total(promotion_results.no_terms))
      printf,lun_verb,'total levels        =',long(total(promotion_results.no_levels))
    endif
   
;----------------------------------------------------------------------
; prepare the output adf34 and adf42 driver and data file names
;----------------------------------------------------------------------
   
    if not keyword_set(ca_only) then begin
; adf34 unchanged   
        adf34_file      = strcompress('adf34_' + elname + '_' + elsymb        $
                          + string(z1[j],format='(i2)') + '.dat',             $
                          /remove_all)
   
        adf34_inst_file = strcompress('adf34_' + elname + '_' + elsymb        $
                          + string(z1[j],format='(i2)') + '_inst.dat',        $
                          /remove_all)
   
        adf34_ls_pp_file= strcompress('adf34_' + elname + '_' + elsymb        $
                          + string(z1[j],format='(i2)') + '_ls_pp.dat',       $
                          /remove_all)
   
        adf34_ic_pp_file= strcompress('adf34_' + elname + '_' + elsymb        $
                          + string(z1[j],format='(i2)') + '_ic_pp.dat',       $
                          /remove_all)
   
        adf42_ls_file   = strcompress('adf42_' + elname + '_' + 'ls#'+yearstr+'_'      $
                          +elsymb + string(z1[j],format='(i2)') + '.dat',     $
                          /remove_all)
   
        adf42_ic_file   = strcompress('adf42_' + elname + '_' + 'ic#'+yearstr+'_'      $
                          +elsymb + string(z1[j],format='(i2)') + '.dat',     $
                          /remove_all)
   
        adf42_ls_pp_file= strcompress('adf42_' + elname + '_' + elsymb        $
                          + string(z1[j],format='(i2)') + '_ls_'+yearstr+'_pp.dat',       $
                          /remove_all)
                          
        adf04_ls_file   = strcompress('adf04_copmm#' + string(z0,             $
                          format='(i2)') + '_' + 'ls_'+yearstr+'#' + elsymb +          $
                          string(z1[j], format='(i2)') + '.dat',/remove_all)

        adf04_ic_file   = strcompress('adf04_copmm#' + string(z0,             $
                          format='(i2)') + '_' + 'ic_'+yearstr+'#' + elsymb +          $
                          string(z1[j], format='(i2)') + '.dat',/remove_all)
   
        adf42_ic_pp_file= strcompress('adf42_' + elname + '_' + elsymb        $
                          + string(z1[j],format='(i2)') + '_ic_'+yearstr+'_pp.dat',    $
                          /remove_all)
                          
    endif else begin
   
        adf34_file        = ' '  
        adf34_inst_file   = ' '                           
        adf34_ls_pp_file  = ' '
        adf34_ic_pp_file  = ' ' 
        adf42_ls_file     = ' ' 
        adf42_ic_file     = ' ' 
        adf42_ls_pp_file  = ' ' 
        adf42_ic_pp_file  = ' '
        adf04_ls_file     = ' '
        adf04_ic_file     = ' ' 
        
                                          
    endelse                  
                     
;   adf42_ca_file   = strcompress('adf42_' + elname + '_' + 'ca#'         $
;                     +elsymb + string(z1[j],format='(i2)') + '.dat',     $
;                     /remove_all)
    adf42_ca_file   = strcompress('adf42_' + elname + '_' + 'ca#'+yearstr+'_'      $
                     +elsymb + string(z1[j],format='(i2)') + '.dat',     $
                     /remove_all)
   
;   adf42_ca_pp_file= strcompress('adf42_' + elname + '_' + elsymb        $
;                     + string(z1[j],format='(i2)') + '_ca_pp.dat',       $
;                     /remove_all)
    adf42_ca_pp_file= strcompress('adf42_' + elname + '_' + elsymb        $
                     + string(z1[j],format='(i2)') + '_ca_'+yearstr+'_pp.dat',    $
                     /remove_all)

;----------------------------------------------------------------------
; prepare adf04 type 1 and type 3 configuration average data file names
;----------------------------------------------------------------------

;   adf04_ca_t1_file  =  strcompress('adf04_copmm#'                       $
;                        + string(z0,format='(i2)') + '_ca#'              $
;                        + elsymb + string(z1[j],format='(i2)')           $
;                        + '_t1.dat', /remove_all)
    adf04_ca_t1_file     = strcompress('adf04_copmm#' + string(z0,        $
                       format='(i2)') + '_' + 'ca_'+yearstr+'#' + elsymb +        $
                       string(z1[j], format='(i2)') + '_t1.dat',/remove_all)

;   adf04_ca_t3_file  =  strcompress('adf04_copmm#'                       $
;                        + string(z0,format='(i2)') + '_ca#'              $
;                        + elsymb + string(z1[j],format='(i2)')           $
;                        + '.dat', /remove_all)
    adf04_ca_t3_file     = strcompress('adf04_copmm#' + string(z0,        $
                       format='(i2)') + '_' + 'ca_'+yearstr+'#' + elsymb +        $
                       string(z1[j], format='(i2)') + '_t3.dat',/remove_all)

    adf04_ca_file     = adf04_ca_t3_file

;----------------------------------------------------------------------
; prepare adf15, adf40 and adf11 data file names of ls, ic and ca type
;----------------------------------------------------------------------

    if not keyword_set(ca_only) then begin

;        adf15_ls_file     =  strcompress('adf15_pec06#'                       $
;                             + string(z0,format='(i2)') + '_'                 $
;                             + elsymb + '_01' + '_ls#'                        $
;                             + elsymb + string(z1[j],format='(i2)')           $
;                             + '.dat', /remove_all)
        adf15_ls_file     =  strcompress('adf15_pec'+yearstr+'#'                       $
                             + elsymb + '_pec'+yearstr+'#'                             $
                             + elsymb + '_ls#'+ elsymb +                      $
                             string(z1[j],format='(i2)')                      $
                             + '.dat', /remove_all)
;        adf15_ic_file     =  strcompress('adf15_pec06#'                       $
;                             + string(z0,format='(i2)') + '_'                 $
;                             + elsymb + '_01' + '_ic#'                        $
;                             + elsymb + string(z1[j],format='(i2)')           $
;                             + '.dat', /remove_all)
        adf15_ic_file     =  strcompress('adf15_pec'+yearstr+'#'                       $
                             + elsymb + '_pec'+yearstr+'#'                             $
                             + elsymb + '_ic#'+ elsymb +                      $
                             string(z1[j],format='(i2)')                      $
                             + '.dat', /remove_all)
;        adf40_ls_file     =  strcompress('adf40_fpec06#'                      $
;                             + string(z0,format='(i2)') + '_'                 $
;                             + elsymb + '_01' + '_ls#'                        $
;                             + elsymb + string(z1[j],format='(i2)')           $
;                             + '.dat', /remove_all)
        adf40_ls_file     =  strcompress('adf40_fpec'+yearstr+'#'                      $
                             + elsymb + '_fpec'+yearstr+'#'                            $
                             + elsymb + '_ls#'+ elsymb +                      $
                             string(z1[j],format='(i2)')                      $
                             + '.dat', /remove_all)
;        adf40_ic_file     =  strcompress('adf40_fpec06#'                      $
;                             + string(z0,format='(i2)') + '_'                 $
;                             + elsymb + '_01' + '_ic#'                        $
;                             + elsymb + string(z1[j],format='(i2)')           $
;                             + '.dat', /remove_all)
        adf40_ic_file     =  strcompress('adf40_fpec'+yearstr+'#'                      $
                             + elsymb + '_fpec'+yearstr+'#'                            $
                             + elsymb + '_ic#'+ elsymb +                      $
                             string(z1[j],format='(i2)')                      $
                             + '.dat', /remove_all)
;        adf11_ls_file     =  strcompress('adf11_plt06#'                       $
;                             + string(z0,format='(i2)') + '_'                 $
;                             + elsymb + '_01' + '_ls#'                        $
;                             + elsymb + string(z1[j],format='(i2)')           $
;                             + '.dat', /remove_all)
        adf11_ls_file     =  strcompress('adf11_plt'+yearstr+'_partial_plt'+yearstr+'_partial_' $
                             + elsymb +'_plt'+yearstr+'_ls#' + elsymb                  $
                             + string(z1[j],format='(i2)') +'.dat',           $
                             /remove_all)
;        adf11_ic_file     =  strcompress('adf11_plt06#'                       $
;                             + string(z0,format='(i2)') + '_'                 $
;                             + elsymb + '_01' + '_ic#'                        $
;                             + elsymb + string(z1[j],format='(i2)')           $
;                             + '.dat', /remove_all)
        adf11_ic_file     =  strcompress('adf11_plt'+yearstr+'_partial_plt'+yearstr+'_partial_' $
                             + elsymb +'_plt'+yearstr+'_ic#' + elsymb                  $
                             + string(z1[j],format='(i2)') +'.dat',           $
                             /remove_all)
                             
    end else begin
   
        adf15_ls_file     = ' '     
        adf15_ic_file     = ' '     
        adf40_ls_file     = ' '     
        adf40_ic_file     = ' '                              
        adf11_ls_file     = ' '  
        adf11_ic_file     = ' '         
                                     
    end                      
                             
;   adf15_ca_file     =  strcompress('adf15_pec06#'                       $
;                        + string(z0,format='(i2)') + '_'                 $
;                        + elsymb + '_01' + '_ca#'                        $
;                        + elsymb + string(z1[j],format='(i2)')           $
;                        + '.dat', /remove_all)
    adf15_ca_file     =  strcompress('adf15_pec'+yearstr+'#'                       $
                        + elsymb + '_pec'+yearstr+'#'                             $
                        + elsymb + '_ca#'+ elsymb +                      $
                        string(z1[j],format='(i2)')                      $
                        + '.dat', /remove_all)
;   adf40_ca_file     =  strcompress('adf40_fpec06#'                      $
;                        + string(z0,format='(i2)') + '_'                 $
;                        + elsymb + '_01' + '_ca#'                        $
;                        + elsymb + string(z1[j],format='(i2)')           $
;                        + '.dat', /remove_all)
    adf40_ca_file     =  strcompress('adf40_fpec'+yearstr+'#'                      $
                        + elsymb + '_fpec'+yearstr+'#'                            $
                        + elsymb + '_ca#'+ elsymb +                      $
                        string(z1[j],format='(i2)')                      $
                        + '.dat', /remove_all)
;   adf11_ca_file     =  strcompress('adf11_plt06#'                       $
;                        + string(z0,format='(i2)') + '_'                 $
;                        + elsymb + '_01' + '_ca#'                        $
;                        + elsymb + string(z1[j],format='(i2)')           $
;                        + '.dat', /remove_all)
                       
    adf11_ca_file     =  strcompress('adf11_plt'+yearstr+'_partial_plt'+yearstr+'_partial_' $
                        + elsymb +'_plt'+yearstr+'_ca#' + elsymb                  $
                        + string(z1[j],format='(i2)') +'.dat',           $
                        /remove_all)
                        
    files={adf34_file      :adf34_file      ,$
           adf34_inst_file :adf34_inst_file ,$
           adf34_ls_pp_file:adf34_ls_pp_file,$
           adf34_ic_pp_file:adf34_ic_pp_file,$
           adf42_ls_file   :adf42_ls_file   ,$
           adf42_ic_file   :adf42_ic_file   ,$
           adf42_ls_pp_file:adf42_ls_pp_file,$
           adf04_ls_file   :adf04_ls_file   ,$
           adf04_ic_file   :adf04_ic_file   ,$
           adf42_ic_pp_file:adf42_ic_pp_file,$
           adf42_ca_file   :adf42_ca_file   ,$
           adf42_ca_pp_file:adf42_ca_pp_file,$
           adf04_ca_t1_file:adf04_ca_t1_file,$
           adf04_ca_t3_file:adf04_ca_t3_file,$
           adf04_ca_file   :adf04_ca_file   ,$
           adf15_ls_file   :adf15_ls_file   ,$
           adf15_ic_file   :adf15_ic_file   ,$
           adf40_ls_file   :adf40_ls_file   ,$
           adf40_ic_file   :adf40_ic_file   ,$
           adf11_ls_file   :adf11_ls_file   ,$
           adf11_ic_file   :adf11_ic_file   ,$
           adf15_ca_file   :adf15_ca_file   ,$
           adf40_ca_file   :adf40_ca_file   ,$
           adf11_ca_file   :adf11_ca_file   }
                           
;------------------------------------------------------------------------
; scale theta and rho arrays as required for default entry to procedures
;------------------------------------------------------------------------

    if theta_noscale eq 1 then begin
       theta_sc = theta/(j+1.0)^2
    end else begin
       theta_sc = theta
    end
   
    if rho_scale eq 1 then begin
       rho_sc = rho*(j+1.0)^7
    end else begin
       rho_sc = rho
    end 
    plasma ={theta         : theta, $
             theta_sc      : theta_sc, $
             theta_noscale : theta_noscale, $
             indx_theta    : indx_theta, $
             rho           : rho, $
             rho_sc        : rho_sc, $
             rho_scale     : rho_scale, $
             indx_rho      : indx_rho, $
             npix          : npix, $
             wvlmin        : wvlmin, $
             wvlmax        : wvlmax, $
             indx_wvl      : indx_wvl }
             
;----------------------------------------------------------------------
; write the ouput files for an ion 
;----------------------------------------------------------------------
    adas8xx_create_drivers, z0_nuc            = z0,                    $
                            z1               = z1[j],                 $
                            ionpot           = ionpot,             $
                            files            = files,                 $
                            plasma           = plasma,                $
                            for_tr_sel       = prom_rules.for_tr_sel,$
                            promotion_results= promotion_results,     $ 
                            ca_only          = ca_only,               $
                            cowan_scale_factors=cowan_scale_factors
          

;----------------------------------------------------------------------
; create the configuration average adf04 data files
;----------------------------------------------------------------------
;stop
    adas8xx_create_ca_adf04, z1[j],                                   $
                             z0,                                      $
                             promotion_results.oc_store,              $
                             ionpot     = ionpot,                  $
                             plasma     = plasma,                     $
                             adf04_t1_file  = adf04_ca_t1_file,       $
                             adf04_t3_file  = adf04_ca_t3_file,       $
                             archive_files = archive_files,           $
                             archive_dir   = archive_dir,             $
                             cowan_scale_factors=cowan_scale_factors, $
                             lun_verb  = lun_verb

;stop
;----------------------------------------------------------------------
; create the ls and ic adf04 data files
;----------------------------------------------------------------------

    if not keyword_set(ca_only) then begin

       adas8xx_create_ls_ic_adf04, z0,                                $
                                   z1[j],                             $
                                   files = files,                     $
                                   archive_dir       = archive_dir,   $
                                   archive_files     = archive_files, $
                                   donotrun          = donotrun

    endif
;----------------------------------------------------------------------
; create the ls, ic and ca adf15, adf40 and adf11 data files
;----------------------------------------------------------------------
    if not keyword_set(only_801) then begin
      adas8xx_create_adf15_adf40, z0,                                  $
                                  z1[j],                               $
                                  files             = files,           $
                                  ca_only           = ca_only,         $
                                  archive_files     = archive_files,   $
                                  archive_dir       = archive_dir,     $
                                  donotrun          = donotrun
    endif
   
    print,'---------------------------------'


; by this point, everything should have been archived if it is going to be
; so delete any leftover files.

  if keyword_set(archive_files) and not keyword_set(donotrun) then begin
    if file_test(files.adf34_file) then begin
      cmd='rm -v '+files.adf34_file
      spawn, cmd
    endif  
    if file_test(files.adf34_inst_file) then begin
      cmd='rm -v '+files.adf34_inst_file
      spawn, cmd
    endif  
    if file_test(files.adf34_ls_pp_file) then begin
      cmd='rm -v '+files.adf34_ls_pp_file
      spawn, cmd
    endif  
    if file_test(files.adf34_ic_pp_file) then begin
      cmd='rm -v '+files.adf34_ic_pp_file
      spawn, cmd
    endif  
    if file_test(files.adf42_ls_file) then begin
      cmd='rm -v '+files.adf42_ls_file
      spawn, cmd
    endif  
    if file_test(files.adf42_ic_file) then begin
      cmd='rm -v '+files.adf42_ic_file
      spawn, cmd
    endif  
    if file_test(files.adf42_ls_pp_file) then begin
      cmd='rm -v '+files.adf42_ls_pp_file
      spawn, cmd
    endif  
    if file_test(files.adf04_ls_file) then begin
      cmd='rm -v '+files.adf04_ls_file
      spawn, cmd
    endif  
    if file_test(files.adf04_ic_file) then begin
      cmd='rm -v '+files.adf04_ic_file
      spawn, cmd
    endif  
    if file_test(files.adf42_ic_pp_file) then begin
      cmd='rm -v '+files.adf42_ic_pp_file
      spawn, cmd
    endif  
    if file_test(files.adf42_ca_file) then begin
      cmd='rm -v '+files.adf42_ca_file
      spawn, cmd
    endif  
    if file_test(files.adf42_ca_pp_file) then begin
      cmd='rm -v '+files.adf42_ca_pp_file
      spawn, cmd
    endif  
    if file_test(files.adf04_ca_t1_file) then begin
      cmd='rm -v '+files.adf04_ca_t1_file
      spawn, cmd
    endif  
    if file_test(files.adf04_ca_t3_file) then begin
      cmd='rm -v '+files.adf04_ca_t3_file
      spawn, cmd
    endif  
    if file_test(files.adf04_ca_file) then begin
      cmd='rm -v '+files.adf04_ca_file
      spawn, cmd
    endif  
    if file_test(files.adf15_ls_file) then begin
      cmd='rm -v '+files.adf15_ls_file
      spawn, cmd
    endif  
    if file_test(files.adf15_ic_file) then begin
      cmd='rm -v '+files.adf15_ic_file
      spawn, cmd
    endif  
    if file_test(files.adf40_ls_file) then begin
      cmd='rm -v '+files.adf40_ls_file
      spawn, cmd
    endif  
    if file_test(files.adf40_ic_file) then begin
      cmd='rm -v '+files.adf40_ic_file
      spawn, cmd
    endif  
    if file_test(files.adf11_ls_file) then begin
      cmd='rm -v '+files.adf11_ls_file
      spawn, cmd
    endif  
    if file_test(files.adf11_ic_file) then begin
      cmd='rm -v '+files.adf11_ic_file
      spawn, cmd
    endif  
    if file_test(files.adf15_ca_file) then begin
      cmd='rm -v '+files.adf15_ca_file
      spawn, cmd
    endif  
    if file_test(files.adf40_ca_file) then begin
      cmd='rm -v '+files.adf40_ca_file
      spawn, cmd
    endif  
    if file_test(files.adf11_ca_file) then begin
      cmd='rm -v '+files.adf11_ca_file
      spawn, cmd
    endif  
  endif

  endfor
  
  

endfor

IF keyword_set(verbose) then begin
  close, lun_verb, verbose, /get_lun
  free_lun, lun_verb
ENDIF

END
