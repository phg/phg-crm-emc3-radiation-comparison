; SCCS INFO Module: @(#)$Header: /home/adascvs/idl/adas8xx/adas808/cw_adas808_in.pro,v 1.1 2004/07/06 12:55:50 whitefor Exp $  Data $Date: 2004/07/06 12:55:50 $
;+
; PROJECT:
;       ADAS IBM MVS to UNIX conversion
;
; NAME:
;       CW_ADAS407_IN()
;
; PURPOSE:
;       Data file selection for adas 407.
;
; EXPLANATION:
;       This function creates a compound widget consisting of text typeins
;	for the beam species element symbol (default h) and the beam 
;	species ion charge (default 0).
;	The compound widget is completed with 'Cancel' and 'Done' buttons.
;
;	The value of this widget is contained in the VALUE structure.
;
; USE:
;       See routine adas407_in.pro for an example.
;
; INPUTS:
;       PARENT  - Long integer; the ID of the parent widget.
;
; OPTIONAL INPUTS:
;       None
;
; OUTPUTS:
;       This function returns the ID of the compound widget created.
;
; OPTIONAL OUTPUTS:
;       None
;
; KEYWORD PARAMETERS:
;       VALUE   - A structure which determines the initial settings of
;                 the entire compound widget. The structure must be:
;		 	{ 	EROOTPATH	:	'',		$
;				EFILE		:	'',		$
;				ECENTROOT	:	'',		$
;				EUSERROOT	:	'',		$
;				CROOTPATH	:	'',		$
;				CFILE		:	'',		$
;			  	CCENTROOT	:	'',		$
;			  	CUSERROOT	:	'',		$
;				FILE_CHANGED	:	0		}
;
;		  Where the elements of the structure are as follows:
;
;                 EROOTPATH   - Current data directory for ionising files
;
;		  EFILE	      - Current ionising file
;
;                 ECENTROOT   - Default central data store for ionising files
;
;                 EUSERROOT   - Default user data store for ionising files
;
;                 CROOTPATH   - Current data directory for ionised files
;
;		  CFILE	      - Current charge exchange data file
;
;                 CCENTROOT   - Default central data store for ionised files
;
;                 CUSERROOT   - Default user data store for ionised files
;
;		  FILE_CHANGED- 1 = Files have been changed in input widget
;				0 = Files the same
;
;                 Path names may be supplied with or without the trailing
;                 '/'.  The underlying routines add this character where
;                 required so that USERROOT will always end in '/' on
;                 output. This does not apply to DSNEX and DSNCX.
;
;       FONT_LARGE  - Supplies the large font to be used for the
;                     interface widgets.
;
;       FONT_SMALL  - Supplies the small font to be used for the
;                     interface widgets.
; CALLS:
;	CW_ADAS_INFILE	Data file selection widget.
;	I4Z0IE		Converts atomic number into element symbol.
;	I4EIZ0		Converts element symbol into atomic number.
;	FILE_ACC	Checks availability of files.
;	NUM_CHK		Checks validity of numerical data.
;
; SIDE EFFECTS:
;       IN407_GET_VAL() Widget management routine in this file.
;       IN407_EVENT()   Widget management routine in this file.
;
; CATEGORY:
;       Compound Widget
;
; WRITTEN:
;       William Osborn, Tessella Support Services plc, 28th March 1996
;
; MODIFIED:
;       1.1     William Osborn 
;		First release. Created from skeleton of cw_adas310_in.
;
; VERSION:
;       1.1     28-03-96
;
;-
;-----------------------------------------------------------------------------

FUNCTION in808_get_val, id


                ;**** Return to caller on error ****

    ON_ERROR, 2

                 ;***************************************
                 ;****     Retrieve the state        ****
                 ;**** Get first_child widget id     ****
                 ;**** because state is stored there ****
                 ;***************************************

    first_child = widget_info(id, /child)
    widget_control, first_child, get_uvalue = state

		;**** Get the settings ****

;    widget_control, state.expid, get_value=fileval
;    widget_control, state.chxid, get_value=fileval2

;    ps = {	EROOTPATH	:	fileval.rootpath,		$
;		EFILE		:	fileval.file,			$
;		ECENTROOT	:	fileval.centroot,		$
;		EUSERROOT	:	fileval.userroot,		$
;		CROOTPATH	:	fileval2.rootpath,		$
;		CFILE		:	fileval2.file,			$
;		CCENTROOT	:	fileval2.centroot,		$
;		CUSERROOT	:	fileval2.userroot,		$
;		FILE_CHANGED	:	state.file_changed		}


   	widget_control, state.txt1, get_value=temp
  	temp=strtrim(temp,2)
  	nuc_ch=fix(temp(0))
   	widget_control, state.txt2, get_value=temp2
	temp2=strtrim(temp2,2)
	el_symb=temp2(0)
	widget_control, state.txt3, get_value=temp3
	temp3=strtrim(temp3,2)
	ion_min=fix(temp3(0))
   	widget_control, state.txt4, get_value=temp4
	temp4=strtrim(temp4,2)
	ion_max=fix(temp4(0))
	strout_one=strarr(21)
	strout_two=strarr(21)
	config=strarr(100)
	nval=intarr(100)
	ncn1=0
	ncn2=0
	open_val=0
	
	state.el_sym=el_symb
	state.ion_min=ion_min
	state.ion_max=ion_max
	state.nuc_ch=nuc_ch
	
	strct={ncn1:ncn1,ncn2:ncn2}
	
        
        widget_control,state.adf00_text,get_value=adf00_root
        
        adf00_root=adf00_root[0]
        
        h8val,el_symb,ion_min,ion_max,strct,state,adf00_root,state.fortdir


        ; find nte, te and te_unit
 
        widget_control,state.tempid_04,get_value=tempval

        te_unit_04=tempval.units
        te_04=tempval.value[0,*]
        if (where(te_04 gt 0))[0] eq -1 then begin
                nte_04=0
        endif else begin
                te_04=te_04[where(te_04 gt 0)]
                if (size(te_04))[0] eq 0 then begin
                        nte_04=1                
                endif else begin
                        nte_04=(size(te_04))[1]
                endelse
        endelse 

        widget_control,state.tempid_11,get_value=tempval

        te_unit_11=tempval.units
        te_11=tempval.value[0,*]
        if (where(te_11 gt 0))[0] eq -1 then begin
                nte_11=0
        endif else begin
                te_11=te_11[where(te_11 gt 0)]
                if (size(te_11))[0] eq 0 then begin
                        nte_11=1                
                endif else begin
                        nte_11=(size(te_11))[1]
                endelse
        endelse 

        widget_control,state.tempid_15,get_value=tempval

        te_unit_15=tempval.units
        te_15=tempval.value[0,*]
        if (where(te_15 gt 0))[0] eq -1 then begin
                nte_15=0
        endif else begin
                te_15=te_15[where(te_15 gt 0)]
                if (size(te_15))[0] eq 0 then begin
                        nte_15=1                
                endif else begin
                        nte_15=(size(te_15))[1]
                endelse
        endelse 
                        
        ;stop
       
        inval= {  adf00_root:adf00_root, $
                  te_04:te_04, $
                  nte_04:nte_04, $
                  te_unit_04:te_unit_04, $
                  te_11:te_11, $
                  nte_11:nte_11, $
                  te_unit_11:te_unit_11, $
                  te_15:te_15, $
                  nte_15:nte_15, $
                  te_unit_15:te_unit_15, $
                  nuc_ch:state.nuc_ch, $
                 el_sym:state.el_sym, $
                 ion_min:state.ion_min, $
                 ion_max:state.ion_max $
                }
        options={       nval		:	state.nval,             $
		        config		:	state.config,           $
		        iz0		:	state.iz0,              $
		        iz1		:	state.iz1,              $
		        prom_config	:	state.prom_config,      $
                        igrndpar        :       state.igrndpar,         $
                        table_data1     :       state.table_data1,      $
                        table_data2     :       state.table_data2,      $
                        table_data1_indx:       state.table_data1_indx, $
                        table_data2_indx:       state.table_data2_indx, $
                  te_04:te_04, $
                  nte_04:nte_04, $
                  te_unit_04:te_unit_04, $
                  te_11:te_11, $
                  nte_11:nte_11, $
                  te_unit_11:te_unit_11, $
                  te_15:te_15, $
                  nte_15:nte_15, $
                  te_unit_15:te_unit_15, $
                        ion_min         :       state.ion_min,          $
                        ion_max         :       state.ion_max,          $
                        el_sym          :       state.el_sym,           $
                        ionpot          :       state.ionpot,           $
                        nuc_ch          :       state.nuc_ch            }

;        stop

    widget_control, first_child, set_uvalue=state, /no_copy

    RETURN, {inval:inval,options:options}

END

;-----------------------------------------------------------------------------

FUNCTION in808_event, event

    COMMON adas808_inblock, userdir, centdir

                ;**** Base ID of compound widget ****

    parent = event.handler

                ;**** Default output no event ****

    new_event = 0L

                ;**********************************************
                ;**** Retrieve the user value state        ****
                ;**** Get id of first_child widget because ****
                ;**** user value "state" is stored there   ****
                ;**********************************************

    first_child = widget_info(parent, /child)
    widget_control, first_child, get_uvalue=state, /no_copy
    topparent = widget_info(parent, /parent)

		;**** Clear any messages away ****

    widget_control, state.messid, set_value= '   '

                ;************************
                ;**** Process events ****
                ;************************

    CASE event.id OF

		;**** Event from ionising file selection ****


 state.txt1: begin
   	nuc_char=0
   	widget_control, state.txt1, get_value=temp
  	temp=strtrim(temp,2)
  	nuc_char=fix(temp(0))
	elem_symb=xxesym(nuc_char)
	widget_control, state.txt2, set_value=elem_symb
	widget_control, /input_focus, state.txt2       
   end
   state.txt2: begin
	iz0=0
   	widget_control, state.txt2, get_value=temp
	temp=strtrim(temp,2)
	elem_symb=string(temp(0))
	iz0=xxeiz0(elem_symb)
	if iz0 eq 0 then begin
		widget_control,state.txt2,set_value='Ar'
		widget_control,state.txt1,set_value='18'
	endif else begin
		str_iz0=string(iz0)
		str_iz0=strtrim(str_iz0,1)
		widget_control, state.txt1, set_value=str_iz0
		widget_control, /input_focus, state.txt3       
	endelse
   end


   state.deftid_04: begin
	action= popup(message='Confirm Overwrite Temperatures with Defaults', $
			buttons=['Confirm','Cancel'],font=state.font_large)

	if action eq 'Confirm' then begin
        
 	  widget_control,state.tempid_04,get_value=tempval
	  tempval.units = 2
          dflt=[2.0e+02,5.0e+2,1.0e+03,2.0e+03,5.0e+03 $
                  ,1.0e+04,2.0e+04,5.0e+04,1.0e+05,2.0e+05,5.0e+05 $
                  ,1.0e+06,2.0e+06]
          maxt=13
          tempval.value[*]=''
          for i=0,maxt-1 do tempval.value[0,i]=strtrim(string(dflt[i],format=state.num_form),2)
 	  widget_control,state.tempid_04,set_value=tempval
          
	endif
   end

   state.deftid_11: begin
	action= popup(message='Confirm Overwrite Temperatures with Defaults', $
			buttons=['Confirm','Cancel'],font=state.font_large)

	if action eq 'Confirm' then begin
                
 	  widget_control,state.tempid_11,get_value=tempval
	  tempval.units = 2
          dflt=[2.0e+02,5.0e+2,1.0e+03,2.0e+03,5.0e+03 $
                  ,1.0e+04,2.0e+04,5.0e+04,1.0e+05,2.0e+05,5.0e+05 $
                  ,1.0e+06,2.0e+06]
          maxt=13
          tempval.value[*]=''
          for i=0,maxt-1 do tempval.value[0,i]=strtrim(string(dflt[i],format=state.num_form),2)
 	  widget_control,state.tempid_11,set_value=tempval
                    
	endif
   end


   state.deftid_15: begin
	action= popup(message='Confirm Overwrite Temperatures with Defaults', $
			buttons=['Confirm','Cancel'],font=state.font_large)

	if action eq 'Confirm' then begin
        
 	  widget_control,state.tempid_15,get_value=tempval
	  tempval.units = 2
          dflt=[2.0e+02,5.0e+2,1.0e+03,2.0e+03,5.0e+03 $
                  ,1.0e+04,2.0e+04,5.0e+04,1.0e+05,2.0e+05,5.0e+05 $
                  ,1.0e+06,2.0e+06]
          maxt=13
          tempval.value[*]=''
          for i=0,maxt-1 do tempval.value[0,i]=strtrim(string(dflt[i],format=state.num_form),2)
 	  widget_control,state.tempid_15,set_value=tempval
          
	endif
   end

 

;   state.but1: begin
;   	widget_control, state.txt2, get_value=temp2
;	temp2=strtrim(temp2,2)
;	el_symb=temp2(0)
;	widget_control, state.txt3, get_value=temp3
;	temp3=strtrim(temp3,2)
;	ion_min=fix(temp3(0))
;   	widget_control, state.txt4, get_value=temp4
;	temp4=strtrim(temp4,2)
;	ion_max=fix(temp4(0))
;	strout_one=strarr(21)
;	strout_two=strarr(21)
;	config=strarr(100)
;	nval=intarr(100)
;	ncn1=0
;	ncn2=0
;	open_val=0
;	
;       state.el_sym=el_symb
;	state.ion_min=ion_min
;	state.ion_max=ion_max
;	
;	strct={ncn1:ncn1,ncn2:ncn2}
;	valence,el_symb,ion_min,ion_max, strct,state
;
;  	widget_control, state.val_buts, get_value=open_val
;	if (open_val eq 0) then begin
;	   widget_control,state.tab_val_sel, set_value=state.table_data1
;	endif
;	if (open_val eq 1) then begin
;	   widget_control,state.tab_val_sel, set_value=state.table_data2
;	endif
;   end


        state.central00_but:begin
                path=getenv('ADASCENT')
                path=path+'/adf00/'
                widget_control,state.adf00_text,set_value=path                        
        end

        state.user00_but:begin
                path=getenv('ADASUSER')
                path=path+'/adf00/'
                widget_control,state.adf00_text,set_value=path                        
        end


                ;***********************
                ;**** Browse button ****
                ;***********************

 
                ;***********************
                ;**** Cancel button ****
                ;***********************

        state.cancelid: begin
            new_event = {ID:parent, TOP:event.top,          		$
                         HANDLER:0L, ACTION:'Cancel'}
        end

		;*********************
		;**** Done Button ****
		;*********************

	state.doneid: begin
	    error = 0

		;************************************
                ;**** return value or flag error ****
		;************************************

            if error eq 0 then begin
                new_event = {ID:parent, TOP:event.top,                  $
                             HANDLER:0L, ACTION:'Done'}
            endif else begin
		widget_control,state.messid,set_value=message
                new_event = 0L
            endelse
	end

        ELSE: new_event = 0L

    ENDCASE


	widget_control,state.txt1,get_value=charge
	widget_control,state.txt3,get_value=ionmin
	widget_control,state.txt4,get_value=ionmax

	charge=fix(charge[0])
	ionmin=fix(ionmin[0])
	ionmax=fix(ionmax[0])

	if ionmin ge charge then begin
		widget_control,state.txt3,set_value='0'
	endif
	if ionmax ge charge then begin
		widget_control,state.txt4,set_value=strtrim(string(charge-1),2)
	endif
	
	
                ;*********************************************
                ;*** make "state" available to other files ***
                ;*********************************************

    widget_control, first_child, set_uvalue=state, /no_copy

    RETURN, new_event

END

;-----------------------------------------------------------------------------

FUNCTION cw_adas808_in, parent, VALUE=value, fortdir, FONT_LARGE=font_large, 	$
                        FONT_SMALL=font_small, NUM_FORM=num_form

    COMMON adas808_inblock, userdir, centdir

    ON_ERROR, 2                                 ;return to caller on error


  nval=intarr(100)
;  nval_str=strarr(100)
  config=strarr(100)
  prom_config=strarr(100,100)
  prom_pars=intarr(100,100)
  prom_lastl=strarr(100,100)
  table_data1_indx=intarr(100)
  table_data2_indx=intarr(100)
  iz0=0
  iz1=intarr(100)
  grndl=strarr(100)
  ion_min=0
  ion_max=0
  ionpot=dblarr(100)
  nconf=intarr(100)
  igrndpar=intarr(100)
  table_data1=strarr(15)
  table_data2=strarr(15)
  table_data1_indx=intarr(100)
  table_data2_indx=intarr(100)


                ;**** Set defaults for keywords ****

;    IF NOT (KEYWORD_SET(value)) THEN begin
;        inset = {allan:'bah'}
    
    IF NOT (KEYWORD_SET(font_large)) THEN font_large = ''
    IF NOT (KEYWORD_SET(font_small)) THEN font_small = ''
    IF NOT (KEYWORD_SET(num_form)) THEN num_form = '(E10.3)'

                ;*********************************
                ;**** Create the Input widget ****
                ;*********************************

                ;**** create base widget ****

    topbase = widget_base(parent, EVENT_FUNC = "in808_event",		$
                          FUNC_GET_VALUE = "in808_get_val",		$
		          /column)

                ;******************************************************
                ;**** Create a dummy widget just to hold value of *****
                ;**** "state" variable so as not to get confused  *****
                ;**** with any other values. Adopt IDL practice   *****
                ;**** of using first child widget                 *****
                ;******************************************************

    first_child = widget_base(topbase)

    cwid = widget_base(first_child, /column)

		;*************************************************
		;**** Base for temperature                    ****
		;*************************************************
                

		;********************************************************
		;**** base for the elemnent ionisation stages        ****
		;********************************************************
                
  generalbase = widget_base(cwid,/row)
  base = widget_base(generalbase,/column,/frame)

  nuc_ch=strtrim(string(value.nuc_ch),2)
  el_sym=strtrim(string(value.el_sym),2)
  ion_min=strtrim(string(value.ion_min),2)
  ion_max=strtrim(string(value.ion_max),2)
  row1    = widget_base(base,/row)
  lab1_1 =widget_label(base, value='')
  row2    = widget_base(base,/row)
  lab2_1 =widget_label(base, value='')
  row3    = widget_base(base,/row)
  lab3_1 =widget_label(base, value='')
  row4    = widget_base(base,/row)
  lab4_1 =widget_label(base, value='')
  lab1 =widget_label(row1, value='Nuclear charge:          ')
  lab2 =widget_label(row2, value='Element symbol:          ')
  lab3 =widget_label(row3, value='Minimum ion charge:      ')
  lab4 =widget_label(row4, value='Maximum ion charge:      ')
  txt1 =widget_text(row1, value=nuc_ch,xsize=4,font=font_large,/edit)
  txt2 =widget_text(row2, value=el_sym,xsize=4,font=font_large,/edit)
  txt3 =widget_text(row3, value=ion_min,xsize=4,font=font_large,/edit)
  txt4 =widget_text(row4, value=ion_max,xsize=4,font=font_large,/edit)
;  but1 =widget_button(base, value='Select to read configurations')


        base=widget_base(generalbase,/column,/frame)
        label=widget_label(base,value='Ionisation Potentials',font=font_large);
        adf00_text=widget_text(base,value=value.adf00_root,xsize=40,/editable,font=font_large)
        bbase=widget_base(base,/row)
        central00_but=widget_button(bbase,value='Central',font=font_large)
        user00_but=widget_button(bbase,value='User',font=font_large)
        

  tablebase = widget_base(cwid,/row)

;  base3 = widget_base(tablebase,/column)
;  lab_base3 = widget_label(base3, value='        ')
		;**** Temperature data table ****



                
  colhead = ['Electron', 'Input']
  unitname = ['Kelvin','eV','Reduced']                
 		
            
		;**** convert FORTRAN index to IDL index ****
  ;stop
                
  base2 = widget_base(tablebase,/column,/frame)
  units_04 = value.te_unit_04
  unitind = [[0,0,0],[1,2,3]]
  tempdata_04 = strarr(6,14)
  for i=0,value.nte_04-1 do tempdata_04[0,i]=strtrim(string(value.te_04[i],format=num_form),2)

  tempid_04 = cw_adas_table(base2, tempdata_04, units_04, unitname, unitind, $
			UNITSTITLE = 'Temperature Units', /scroll, $
			COLEDIT = [1,0], COLHEAD = colhead, $
			TITLE = 'ADF04 Temperatures', ORDER = [1,0], $
			LIMITS = [2,2], CELLSIZE = 12, $
			FONTS = edit_fonts, FONT_LARGE = font_large, $
			FONT_SMALL = font_small)

		;**** Default temperatures button ****
                
  base3 = widget_base(base2,/column)
  deftid_04 = widget_button(base3,value='Default Temperatures',xsize=237,$
  	ysize=26, font=font_large)



  base2 = widget_base(tablebase,/column,/frame)
  units_11 = value.te_unit_11
  unitind = [[0,0,0],[1,2,3]]
  tempdata_11 = strarr(6,14)
  for i=0,value.nte_11-1 do tempdata_11[0,i]=strtrim(string(value.te_11[i],format=num_form),2)

  tempid_11 = cw_adas_table(base2, tempdata_11, units_11, unitname, unitind, $
			UNITSTITLE = 'Temperature Units', /scroll, $
			COLEDIT = [1,0], COLHEAD = colhead, $
			TITLE = 'ADF11 Temperatures', ORDER = [1,0], $
			LIMITS = [2,2], CELLSIZE = 12, $
			FONTS = edit_fonts, FONT_LARGE = font_large, $
			FONT_SMALL = font_small)

		;**** Default temperatures button ****
                
  base3 = widget_base(base2,/column)
  deftid_11 = widget_button(base3,value='Default Temperatures',xsize=237,$
  	ysize=26, font=font_large)

  base2 = widget_base(tablebase,/column,/frame)
  units_15 = value.te_unit_15
  unitind = [[0,0,0],[1,2,3]]
  tempdata_15 = strarr(6,14)
  for i=0,value.nte_15-1 do tempdata_15[0,i]=strtrim(string(value.te_15[i],format=num_form),2)

  tempid_15 = cw_adas_table(base2, tempdata_15, units_15, unitname, unitind, $
			UNITSTITLE = 'Temperature Units', /scroll, $
			COLEDIT = [1,0], COLHEAD = colhead, $
			TITLE = 'ADF15 Temperatures', ORDER = [1,0], $
			LIMITS = [2,2], CELLSIZE = 12, $
			FONTS = edit_fonts, FONT_LARGE = font_large, $
			FONT_SMALL = font_small)

		;**** Default temperatures button ****
                
  base3 = widget_base(base2,/column)
  deftid_15 = widget_button(base3,value='Default Temperatures',xsize=237,$
  	ysize=26, font=font_large)


                ;*****************************************
                ;**** Ionising file selection widget ****
                ;*****************************************

 ;   expbase = widget_base(cwid, /column, /frame)
 ;   expval = {	ROOTPATH	:	inset.erootpath,		$
;		FILE		:	inset.efile,			$
;		CENTROOT	:	inset.ecentroot,		$
;		USERROOT	:	inset.euserroot			}
;    exptitle = widget_label(expbase, font=font_large,			$
;	                    value='Ionising Ion File Details:-')
;    expid = cw_adas4xx_infile(expbase, value=expval, font=font_small)

                ;***********************************************
                ;**** Ionised file selection widget ****
                ;***********************************************

 ;   chxbase = widget_base(cwid, /column, /frame)
 ;   chxval = {	ROOTPATH	:	inset.crootpath,		$
;		FILE		:	inset.cfile,			$
;		CENTROOT	:	inset.ccentroot,		$
;		USERROOT	:	inset.cuserroot			}
;    chxtitle = widget_label(chxbase, font=font_large,			$
;	                    value='Ionised Ion File Details:-')
;    chxid = cw_adas4xx_infile(chxbase, value=chxval, font=font_small)

		;**** Error message ****

    messid = widget_label(cwid, font=font_large,			$
    value='Edit the processing options data and press Done to proceed')

                ;*****************
                ;**** Buttons ****
                ;*****************

    base = widget_base(cwid, /row)

                ;**** Browse Dataset button ****

    browseid = widget_button(base, value='Browse Comments',		$
                             font=font_large)

                ;**** Cancel Button ****

    cancelid = widget_button(base, value='Cancel', font=font_large)

                ;**** Done Button ****

    doneid = widget_button(base, value='Done', font=font_large)

                ;*************************************************
                ;**** Check filenames and desensitise buttons ****
                ;**** if it is a directory or it is a file    ****
                ;**** without read access.                    ****
                ;*************************************************


                ;*************************************************
                ;**** create a state structure for the pop-up ****
                ;****                window.                  ****
                ;*************************************************

    new_state = {	font_large	:	font_large,		$
			font_small	:	font_small,		$
			inval		:	value,			$
			doneid		:	doneid,			$
                        txt1            :       txt1,                   $
                        txt2            :       txt2,                   $
                        txt3            :       txt3,                   $
                        txt4            :       txt4,                   $
			deftid_04       :       deftid_04,                 $
			deftid_11       :       deftid_11,                 $
			deftid_15       :       deftid_15,                 $
                        tempid_04       :       tempid_04,                 $
                        tempid_11       :       tempid_11,                 $
                        tempid_15       :       tempid_15,                 $
                        num_form        :       num_form,               $
                        el_sym          :       value.el_sym,           $
                        ion_min         :       value.ion_min,          $
                        ion_max         :       value.ion_max,          $
                        nuc_ch          :       value.nuc_ch,           $
		        nval		:	nval,		        $
		        config		:	config,		        $
		        iz0		:	iz0,		        $
		        iz1		:	iz1,		        $
		        IONPOT          :       IONPOT,                 $
                        prom_config	:	prom_config,	        $
                        igrndpar        :       igrndpar,               $
                        fortdir         :       fortdir,                $
                        table_data1     :       table_data1,            $
                        table_data2     :       table_data2,            $
                        table_data1_indx:       table_data1_indx,       $
                        table_data2_indx:       table_data2_indx,       $
                        adf00_text	:	adf00_text,		$
                        central00_but	:	central00_but,		$
                        user00_but      :	user00_but,	        $
                        browseid	:	browseid,		$
			messid		:	messid,			$
			cancelid	:	cancelid                }
               ;**** Save initial state structure ****

    widget_control, first_child, set_uvalue=new_state, /no_copy
    
    RETURN, topbase

END
