; SCCS INFO Module: @(#)$Header: /home/adascvs/idl/adas8xx/adas808/adas808_in.pro,v 1.2 2010/11/30 08:42:37 mog Exp $  Data $Date: 2010/11/30 08:42:37 $
;+
; PROJECT:
;       ADAS IBM MVS to DEC UNIX conversion
;
; NAME:
;	ADAS407_IN
;
; PURPOSE:
;	IDL ADAS user interface, input file selection.
;
; EXPLANATION:
;	This routine creates and manages a pop-up window which allows
;	the user to select two files from the UNIX file system, as needed
;     	for adas407: the ionising and the ionised ion file. The second
;   	file should have a charge state one greater than the first - 
;	this is checked for.
;
; USE:
;	See the d7spf0.pro for an example of how to
;	use this routine.
;
; INPUTS:
;	VAL	- A structure which determines the initial settings of
;		  the dataset selection widget.  VALUE is passed un-modified
;		  through to cw_adas407_in.pro, see that routine for a full
;		  description.
;
; OPTIONAL INPUTS:
;	None
;
; OUTPUTS:
;	VAL	- On output the structure records the final settings of
;		  the dataset selection widget if the user pressed the
;		  'Done' button otherwise it is not changed from input.
;
;	ACT	- A string; indicates the user's action when the pop-up
;		  window is terminated, i.e which button was pressed to
;		  complete the input.  Possible values are 'Done' and
;		  'Cancel'.
;
; OPTIONAL OUTPUTS:
;	None
;
; KEYWORD PARAMETERS:
;	WINTITLE- A title to be used on the banner for the pop-up window.
;		  Intended to indicate which application is running,
;		  e.g 'ADAS 407 INPUT'
;
;    FONT_LARGE - Supplies the large font to be used for the interface widgets.
;    FONT_SMALL - Supplies the small font to be used for the interface widgets.
;
; CALLS:
;	CW_ADAS407_IN	Dataset selection widget creation.
;	XMANAGER	Manages the pop=up window.
;	DYNLABEL	Sets dynamic_resize keyword for label widgets.
;	ADAS407_IN_EV	Event manager, called indirectly during XMANAGER
;			event management.
;
; SIDE EFFECTS:
;	XMANAGER is called in /modal mode. Any other widgets become
;	inactive.
;
; CATEGORY:
;	Adas system.
;
; WRITTEN:
;       William Osborn, Tessella Support Services plc, 27th March 1996
;		Created from adas_in.pro
; MODIFIED:
;	1.1	William Osborn
;		First Version
;	1.2    	William Osborn
;		Added dynlabel procedure
;	1.3    	Martin O'Mullane
;		Rename common block in_blk to in808_blk.
; VERSION:
;	1.1	16-05-96
;	1.2	09-07-96
;	1.3	30-11-2010
;
;-
;-----------------------------------------------------------------------------


PRO adas808_in_ev, event

  COMMON in808_blk,action,value,options

		;**** Find the event type and copy to common ****
    action = event.action

    CASE action OF

		;**** 'Done' button ****
	'Done'  : begin

			;**** Get the output widget value ****
		widget_control,event.id,get_value=val
		widget_control,event.top,/destroy
                
                ;stop
                
                value=val.inval
                options=val.options

	   end


		;**** 'Cancel' button ****
	'Cancel': widget_control,event.top,/destroy

    ENDCASE

END

;-----------------------------------------------------------------------------


PRO adas808_in, val, act, opt, fortdir, WINTITLE = wintitle,				$
		FONT_LARGE=font_large, FONT_SMALL=font_small

  COMMON in808_blk,action,value,options

		;**** Copy value to common ****
  value = val
  action = 0L
  options = 0L

		;**** Set defaults for keywords ****
  IF NOT (KEYWORD_SET(wintitle)) THEN wintitle = 'ADAS808 INPUT FILE'
  IF NOT (KEYWORD_SET(font_large)) THEN font_large = ''
  IF NOT (KEYWORD_SET(font_small)) THEN font_small = ''

		;***************************************
		;**** Pop-up a new widget if needed ****
		;***************************************

                ;**** create base widget ****
  inid = widget_base(TITLE=wintitle,XOFFSET=100,YOFFSET=100)

		;**** Create input compund widget ****
  cwid = cw_adas808_in(inid, VALUE=value, fortdir, FONT_LARGE=font_large, 	$
			FONT_SMALL=font_small)

		;**** Realize the new widget ****
  dynlabel, inid
  widget_control,inid,/realize

		;**** make widget modal ****
  xmanager,'adas808_in',inid,event_handler='adas808_in_ev',/modal,/just_reg
 
		;**** Return the output value from common ****
  act = action
  val = value
  opt = options

END

