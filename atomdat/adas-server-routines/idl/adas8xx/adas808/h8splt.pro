; SCCS INFO Module: @(#)$Header: /home/adascvs/idl/adas8xx/adas808/h8splt.pro,v 1.1 2004/07/06 14:02:59 whitefor Exp $  Data $Date: 2004/07/06 14:02:59 $
pro h8splt,dataptrs,state,ion_ch=ion_ch

        if not keyword_set(ion_ch) then begin
                widget_control, state.txt_conf1,get_value=temp
                if temp[0] ne '' then begin
                        ion_ch=fix(temp(0))
                endif else begin
                        ion_ch=-1
                endelse
        endif

        if ion_ch eq -1 then return

        if state.gotpreviewdata[ion_ch] eq 1 then begin
                data=*(dataptrs[ion_ch])
        endif else begin
                goto,skipcrap
        endelse

        for i=0,(size(state.config_split_but))[1]-1 do begin
                widget_control,state.config_split_but[i],set_button=0,set_uvalue=0
        end
        
	if (where(data.aval gt 0))[0] eq -1 then return

        xdata=data.wavel[where(data.aval gt 0)]
        ydata=data.aval[where(data.aval gt 0)]
        conf1=data.conf1[where(data.aval gt 0)]
        conf2=data.conf2[where(data.aval gt 0)]

        for i=0,(size(state.select_onoff))[1]-1 do begin
                widget_control,state.select_onoff[i],get_uvalue=onoff
                if onoff eq 1 then begin
                        widget_control,state.select_cmin[i],get_value=a
                        cmin=double(a[0])
                        widget_control,state.select_lmin[i],get_value=a
                        lmin=double(a[0])
                        widget_control,state.select_lmax[i],get_value=a
                        lmax=double(a[0])
                
                        if lmin gt lmax then begin
                                widget_control,state.select_lmin[i],set_value=strtrim(string(lmax),2)
                                widget_control,state.select_lmax[i],set_value=strtrim(string(lmin),2)
                        
                                temp=lmin
                                lmin=lmax
                                lmax=temp
                        endif
                
                
                        if (size(xdata))[0] eq 0 then begin 
                                xdata=[xdata]
                                ydata=[ydata]
                        endif
                        for j=0,(size(xdata))[1]-1 do begin
                                if xdata[j] gt lmin and xdata[j] lt lmax and ydata[j] gt cmin then begin
                                        print,conf1[j],conf2[j]
                                        widget_control,state.config_split_but[conf1[j]],/set_button,/set_uvalue
                                        widget_control,state.config_split_but[conf2[j]],/set_button,/set_uvalue
                                endif
                        end
                
                endif     
        end

skipcrap:
        
        widget_control, state.txt_conf1,get_value=temp
        ion_ch=fix(temp(0))
        total=0
        for j=0,(size(state.prom_nlev))[2]-1 do begin
                widget_control,state.config_split_but[j],get_uvalue=onoff
                if onoff eq 1 then total=total+state.prom_nlev[ion_ch,j]
                if onoff eq 0 then begin
                        widget_control,state.config_split_man[j],get_uvalue=onoff
                        if onoff eq 1 then total=total+state.prom_nlev[ion_ch,j]
                endif
        end
 
        widget_control,state.txt_base_nionsel,set_value=strtrim(string(total),2)


end
