; SCCS INFO Module: @(#)$Header: /home/adascvs/idl/adas8xx/adas808/h8effc.pro,v 1.1 2004/07/06 14:01:30 whitefor Exp $  Data $Date: 2004/07/06 14:01:30 $
pro h8effc,tev,ntemp,effcollstr,atomdata,ion_ch,fortdir

        if atomdata.bessel eq 0 then begin

                effcollstr=dblarr(atomdata.ntran,ntemp)

                for i=0,atomdata.ntran-1 do begin
			indx_1=atomdata.indx_1[i]
			indx_2=atomdata.indx_2[i]
			aval=atomdata.aval[i]
			oscstr=atomdata.oscstr[i]
			ediff=atomdata.ediff[i]
			
			if atomdata.eav(indx_1) gt atomdata.eav(indx_2) then begin
				stat_low=atomdata.stat_wt[indx_2]
			endif else begin
				stat_low=atomdata.stat_wt[indx_1]		
			endelse
			
			for j=0,ntemp-1 do begin
				u=ediff*13.606 / tev[j]
				if ion_ch eq 0 then begin
					gam2=double(1) / ediff
				endif else begin
					gam2=double(ion_ch^2) / ediff
				endelse
				h8giii,u,gam2, giiiav,fortdir
				effcollstr[i,j]=oscstr* (8d0 * acos(-1) / sqrt(3)) * (1d0 / ediff) * stat_low*giiiav
			end

			
                end
                
                return
        endif
        
        if atomdata.bessel eq 1 then begin

                effcollstr=dblarr((size(atomdata.collindx_1))[1],ntemp)


                for i=0,(size(atomdata.collindx_1))[1]-1 do begin
                        transition=i
                        
                        
                        
                        avalue=1e-30

                        for j=0,atomdata.ntran-1 do begin
                                if (atomdata.indx_1[j] eq atomdata.collindx_1[transition] $
                                and atomdata.indx_2[j] eq atomdata.collindx_2[transition]) $
                                or (atomdata.indx_2[j] eq atomdata.collindx_1[transition] $
                                and atomdata.indx_1[j] eq atomdata.collindx_2[transition]) $
                                then avalue=atomdata.aval[j]
                        end

                        if (atomdata.eav[atomdata.collindx_1[transition]] gt atomdata.eav[atomdata.collindx_2[transition]] ) then begin
                                stat_up=atomdata.stat_wt[atomdata.collindx_1[transition]]
                                stat_low=atomdata.stat_wt[atomdata.collindx_2[transition]]
                                e_up=atomdata.eav[atomdata.collindx_1[transition]]
                                e_low=atomdata.eav[atomdata.collindx_2[transition]]
                        endif else begin
                                stat_up=atomdata.stat_wt[atomdata.collindx_2[transition]]
                                stat_low=atomdata.stat_wt[atomdata.collindx_1[transition]]
                                e_up=atomdata.eav[atomdata.collindx_2[transition]]
                                e_low=atomdata.eav[atomdata.collindx_1[transition]]
                        endelse

                        typestr=atomdata.colltype[transition]
                        type=2
                        if strpos(typestr,'j1') ne -1 then type=1
        
        
        
;                        stop
                        
                        egrid=reform(atomdata.egrids[transition,*])
                        collstr=reform(atomdata.collstrs[transition,*])
                
                        allan=where(collstr gt 0)
                        egrid=egrid[allan]
                        collstr=collstr[allan]
                
                        if allan[0] ne -1 then begin
;                        stop
                        ;h8mxav,reform(atomdata.egrids[transition,*]),reform(atomdata.collstrs[transition,*]),atomdata.iz0,ion_ch,ion_ch+1,type,avalue,stat_low,stat_up,e_low,e_up,tev * 11607d0,effcoll,fortdir
                        h8mxav,egrid,collstr,atomdata.iz0,ion_ch,ion_ch+1,type,avalue,stat_low,stat_up,e_low,e_up,tev * 11607d0,effcoll,fortdir

                        effcollstr[transition,*]=effcoll
                        
                        endif else begin
                                effcollstr[transition,*]=1e-30
                        
                        endelse
                        ;stop

                end

        endif
end
