; SCCS INFO Module: @(#)$Header: /home/adascvs/idl/adas8xx/adas808/h8wr04.pro,v 1.2 2004/08/13 09:24:42 allan Exp $  Data $Date: 2004/08/13 09:24:42 $
PRO h8wr04, data, path=path

   	if not keyword_set(path) then begin
        	path='./'
   	endif   

   	for ion_ch=data.ion_min,data.ion_max do begin
		if data.gotatomdata[ion_ch] gt 0 then begin
			name=''
	         	name=path+'/'+strtrim(string(data.el_sym),2)+'_'+strtrim(string(ion_ch),2)+'_ca_adf04.dat'
        	 	openw,unit,name,/get_lun

			ionpotcm=data.ionpot[ion_ch]*8065.54445
			energies=109737.26d0*(*data.atomdata[ion_ch]).eav-109737.26d0*(*data.atomdata[ion_ch]).eav[0]
			config_list=(*data.atomdata[ion_ch]).config_list
			jvalues=((*data.atomdata[ion_ch]).stat_wt-1)/2

			;stop
		
			symbol=data.el_sym
			if strlen(symbol) eq 2 then begin
				symbol=strupcase(strmid(symbol,0,1))+strlowcase(strmid(symbol,1,1))
			endif

			if strlen(symbol) eq 1 then begin
				symbol=strupcase(symbol)+' '
			endif
			
		
			printf,unit,symbol,'+',strtrim(string(ion_ch),2),data.iz0,ion_ch+1,ionpotcm,format='(A2,A1,I2,8X,I2,8X,I2,1X,F15.1)'

			if (size(config_list))[0] eq -1 then begin
				printf,unit,'-1'
				free_lun,unit	
				return
			endif
                        
                        width=strtrim(string(fix(alog10(max(jvalues))+3)),2)
                        listformat='(I5,1X,A17,1X,A1,I1,A1,I1,A1,F'+width+'.1,A1,1X,F15.1)'

			for i=0,(size(config_list))[1]-1 do begin
				lval=0
				sval=0
				jval=jvalues[i]
				configuration=strtrim(config_list[i],2)
				k=17-strlen(configuration)
				for j=0,k-1 do begin
					configuration=configuration+' '
				end
				printf,unit,i+1,configuration,'(',sval,')',lval,'(',jval,')',energies[i],format=listformat
			end
	
			printf,unit,'-1'
	
			ntemp=data.nte_04
       			temps=data.te_04        

			if ntemp gt 0 then begin
       			
				temps=temps[0:ntemp-1]
			
				if data.te_unit_04 eq 1 then begin
        	       			; eV to K
               				temps=temps*11607.
       				endif

	       			if data.te_unit_04 eq 2 then begin
        	       			; red to K
               				temps=temps*((ion_ch+1)^2.)
	       			endif
 
 				string=''
				exponent=fix(alog10(temps))
				if (where(exponent lt 0))[0] ne -1 then exponent[(where(exponent lt 0))]=exponent[(where(exponent lt 0))]-1
				mantissa=temps/(10.^exponent)
				pmin=strarr(ntemp)
				
				for k=0,ntemp-1 do begin
					if mantissa[k] lt 1. then begin
						mantissa[k]=mantissa[k]*10.
						exponent[k]=exponent[k]-1.
					endif
					if exponent[k] ge 0 then pmin[k]='+' else pmin[k]='-'
					exponent[k]=abs(exponent[k])
					if exponent[k] lt 10. then pmin[k]=pmin[k]+'0'	
				end
				for k=0,ntemp-1 do begin
					string=string+' '+strmid(strtrim(string(mantissa[k]),2),0,4)+pmin[k]+strtrim(string(exponent[k]),2)
				end
				 
				printf,unit,ion_ch+1,3,string,format='(1X,F4.1,4X,I1,6X,A'+strtrim(string(strlen(string)),2)+')'
	
  				tev=temps / 11607d0
                                h8effc,tev,ntemp,effcollstr,(*data.atomdata[ion_ch]),ion_ch,data.fortdir
				                                
                                for i=0,(size((*data.atomdata[ion_ch]).collindx_1))[1]-1 do begin
				
                                        indx_1=(*data.atomdata[ion_ch]).collindx_1[i]
			                indx_2=(*data.atomdata[ion_ch]).collindx_2[i]
				
					datatowrite=dblarr(ntemp+1)
                                        
                                        avalue=1e-30
                                        

                                        for j=0,(*data.atomdata[ion_ch]).ntran-1 do begin
                                                if ((*data.atomdata[ion_ch]).indx_1[j] eq indx_1 $
                                                and (*data.atomdata[ion_ch]).indx_2[j] eq indx_2) $
                                                or ((*data.atomdata[ion_ch]).indx_2[j] eq indx_1 $
                                                and (*data.atomdata[ion_ch]).indx_1[j] eq indx_2) $
                                                then avalue=(*data.atomdata[ion_ch]).aval[j]
                                        end

					datatowrite[0]=avalue
                                        datatowrite[1:ntemp]=effcollstr[i,*]
					
				


 					string=''
					exponent=fix(alog10(datatowrite))
					if (where(exponent lt 0))[0] ne -1 then exponent[(where(exponent lt 0))]=exponent[(where(exponent lt 0))]-1
					mantissa=datatowrite/(10.^exponent)
					pmin=strarr(ntemp+1)
				
				
				
					for k=0,ntemp do begin
						if mantissa[k] lt 1. then begin
							mantissa[k]=mantissa[k]*10.
							exponent[k]=exponent[k]-1.
						endif
						if exponent[k] ge 0 then pmin[k]='+' else pmin[k]='-'
						exponent[k]=abs(exponent[k])
						if exponent[k] lt 10. then pmin[k]=pmin[k]+'0'	
					end
					for k=0,ntemp do begin
						string=string+' '+strmid(strtrim(string(mantissa[k]),2),0,4)+pmin[k]+strtrim(string(exponent[k]),2)
					end

					if indx_1 lt indx_2 then begin
						temp=indx_2
						indx_2=indx_1
						indx_1=temp
					endif

					printf,unit,indx_1+1,indx_2+1,string,format='(2I4,A'+strtrim(string(strlen(string)),2)+')'
				end

			endif
	

		printf,unit,'  -1'
		printf,unit,'  -1  -1'
		printf,unit,'C---------------------------------------------------------------------------------'
		printf,unit,'C Configuration Averaged Specific Ion File'
		printf,unit,'C Generated using code ADAS808'
		printf,unit,'C---------------------------------------------------------------------------------'



		free_lun,unit
		endif
   	end


end


