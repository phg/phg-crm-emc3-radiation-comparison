; SCCS INFO Module: @(#)$Header: /home/adascvs/idl/adas8xx/adas808/h8val.pro,v 1.1 2004/07/06 14:03:12 whitefor Exp $  Data $Date: 2004/07/06 14:03:12 $
PRO h8val, el_symb, ion_min, ion_max,strct,state,adf00_dir,fortdir

iz0=0
iz1=intarr(100)
ionpot=dblarr(100)
config=strarr(100)
symbol=''
sdum=''
idum=0
rdum=0.d0

el_symb=strlowcase(el_symb)+'.dat'
;filename='/disk3/loch/ground_ionpots/'
;filename='/home/whitefor/adas/adf00/'
filename=adf00_dir
filename=filename+el_symb

openr, lun, filename, /get_lun
readf,lun,symbol, iz0, format='(a17,i4)'
for i=0,abs(iz0)-1 do begin
    if iz0 lt 0 then begin
        readf,lun,idum, rdum, sdum, format='(i2,2x,f14.8,2x,a100)'
    endif else begin
        readf,lun,idum, sdum, format='(i2,3x,a100)'
    endelse
    
    iz1(i)=idum+1
    ionpot(i)=rdum
    config(i)=sdum
endfor
free_lun,lun

iz0=abs(iz0)

spawn, fortdir+'/808_val.out', unit=pipe, /noshell, PID=pid

iflag=0
tempstr=''
tempnc=0
ncn1=0
ncn2=0
idum2=0
idum3=0
state.iz0=iz0
printf,pipe,ion_min, ion_max
for i=ion_min,ion_max do begin
    state.config(i)=config(i)
    state.iz1(i)=iz1(i)
    STATE.IONPOT[I]=IONPOT[I]
    printf,pipe,config(i), format='(a100)'
    readf,pipe,idum2
    state.nval(i)=fix(idum2)
    readf,pipe,idum3, format='(i1)'
    state.igrndpar(i)=fix(idum3)
    readf,pipe,iflag
    if (iflag eq 0) then begin
       readf,pipe, tempstr,tempnc,format='(a5,i3)'
       if (tempnc eq 1) then begin
          state.table_data1(ncn1)=tempstr
          state.table_data1_indx(ncn1)=fix(idum2)
          ncn1=ncn1+1
       endif
       if (tempnc eq 2) then begin
          state.table_data2(ncn2)=tempstr
          state.table_data2_indx(ncn2)=fix(idum2)
          ncn2=ncn2+1
       endif
    endif
endfor
strct.ncn1=ncn1-1
strct.ncn2=ncn2-1

END
