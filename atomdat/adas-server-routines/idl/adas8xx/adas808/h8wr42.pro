; SCCS INFO Module: @(#)$Header: /home/adascvs/idl/adas8xx/adas808/h8wr42.pro,v 1.2 2004/08/13 09:24:42 allan Exp $  Data $Date: 2004/08/13 09:24:42 $
pro h8wr42, data, path=path

   if not keyword_set(path) then begin
        path='./'
   endif   
   
   if strmid(path,strlen(path)-1) ne '/' then path=path+'/'

   for i=data.ion_min,data.ion_max do begin
       get_lun,lun
       name=''
       name=path+strtrim(string(data.el_sym),2)+'_'+strtrim(string(i),2)+'_adf42.dat'
       openw,lun,name
       
       ionpotcm=data.ionpot[i]*8065.54445

        ;stop
       
       printf,lun,' &FILES'
       printf,lun,"     dsn04 = '" + strtrim(string('./'+strtrim(string(data.el_sym),2)+'_'+strtrim(string(i),2)+'_adf04.dat'),2) +"'"
       printf,lun,"     dsn18 = 'NULL'" 
       printf,lun,"     dsn35 = 'NULL'" 
       printf,lun,"     dsn15 = '" + strtrim(string('./'+strtrim(string(data.el_sym),2)+'_'+strtrim(string(i),2)+'_adf15.dat'),2) +"'"
       printf,lun,"     dsn40 = '" + strtrim(string('./'+strtrim(string(data.el_sym),2)+'_'+strtrim(string(i),2)+'_adf40.dat'),2) +"'"
       printf,lun,"     dsn11 = '" + strtrim(string('./'+strtrim(string(data.el_sym),2)+'_'+strtrim(string(i),2)+'_plt_adf11.dat'),2) +"'"
       printf,lun,"     dsn11f = '" + strtrim(string('./'+strtrim(string(data.el_sym),2)+'_'+strtrim(string(i),2)+'_pltf_adf11.dat'),2) +"'"
       printf,lun,' &END'
       printf,lun
       printf,lun,' &ION'
       printf,lun,"     element = '"+ strtrim(string(data.el_sym),2) +"'"
       printf,lun,"     z0 = "+ strtrim(string(data.iz0),2)
       printf,lun,"     z1 = "+ strtrim(string(i+1),2)
       printf,lun,"     ip = "+ strtrim(string(ionpotcm),2)
       printf,lun,' &END'
       printf,lun       
       printf,lun,' &META'
       printf,lun,'     nmet     = 1,'
       printf,lun,'     imetr(1) = 1'
       printf,lun,'     lnorm  = .TRUE.'
       printf,lun,' &END'
       printf,lun       
       printf,lun,' &PROCESS'
       printf,lun,'     liosel = .FALSE.'
       printf,lun,'     lhsel  = .FALSE.'
       printf,lun,'     lrsel  = .FALSE.'
       printf,lun,'     lisel  = .FALSE.'
       printf,lun,'     lnsel  = .FALSE.'
       printf,lun,'     lpsel  = .FALSE.'
       printf,lun,'     zeff   = 0.0'
       printf,lun,' &END
       printf,lun       
       printf,lun,' &OUTPUT'
       printf,lun,'     lmetr   = .FALSE.,'
       printf,lun,'     ltscl   = .FALSE.,'
       printf,lun,'     ldscl   = .FALSE.,'
       printf,lun,'     lbrdi   = .FALSE.,'
       printf,lun,'     numte   = ' + strtrim(string(data.nte_15),2)
       printf,lun,'     numtion = ' + strtrim(string(0),2)
       printf,lun,'     numth   = ' + strtrim(string(0),2)
       printf,lun,'     numdens = ' + strtrim(string(5),2)
       printf,lun,'     numdion = ' + strtrim(string(0),2)
       printf,lun,'     numwvl  = ' + strtrim(string(data.nints),2)
       printf,lun,' &END
       printf,lun       
       
       ntemp=data.nte_15
       temps=data.te_15        

       if data.te_unit_15 eq 1 then begin
               ; eV to K
               temps=temps*11607.
       endif

       if data.te_unit_15 ne 2 then begin
               ; K to red.
               temps=temps/((i+1)^2.)
       endif
              
       if ntemp gt 0 then printf,lun,temps,format='(4(1X,E9.3))'
       
       printf,lun       


       ndense=5
       dense=[1.000e+11, 1.000e+12, 1.000e+13, 1.000e+14, 1.000e+15]
       if ndense gt 0 then printf,lun,dense,format='(4(1X,E9.3))'

       printf,lun       
       for j=0,data.nints-1 do begin
                printf,lun,1024,data.lmin[j],data.lmax[j],format='(i5,2e10.3)'
       end
       
       free_lun, lun
   endfor 

end
