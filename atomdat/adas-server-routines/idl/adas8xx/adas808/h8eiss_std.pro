; SCCS INFO Module: @(#)$Header: /home/adascvs/idl/adas8xx/adas808/h8eiss_std.pro,v 1.1 2004/07/06 14:01:34 whitefor Exp $  Data $Date: 2004/07/06 14:01:34 $
function h8eiss_std, cfg, cfg_type, occup=occup
;-----------------------------------------------------------------------
; code to convert standard form to Eissner notation
; cfg_type = 0		=> 	standard -> Eissner
; cfg_type = 1		=> 	Eissner  -> standard
; cfg_type = 2		=>	occupation array -> Standard 
; cfg_type = 3		=>	occupation array -> Eissner 
;-----------------------------------------------------------------------
	std_confgs=strarr(21)
	eiss_no=strarr(21)
	nmax_shl=intarr(21)
	std_confgs=['1s','2s','2p','3s','3p','3d','4s','4p','4d','4f',$
		'5s','5p','5d','5f', '5g','6s','6p','6d','6f','6g','6h']
	eiss_no=['1','2','3','4','5','6','7','8','9','A','B',$
		'C','D','E','F','G','H','I','J','K','L']
	nmax_shl=[2,2,6,2,6,10,2,6,10,14,2,6,10,14,18,2,6,18,22]
	cfg=strtrim(cfg,2)
	if cfg_type ne 0 and cfg_type ne 1 then begin
   	   print, 'conversion type incorrect'
   	   stop
	endif
	if keyword_set(occup) then goto,lab_occ

;-----------------------------------------------------------------------
; standard -> Eissner conversion
;-----------------------------------------------------------------------
	if cfg_type eq 0 then begin
	   res=strsplit(cfg,' ')
	   n_shells=n_elements(res)
	   cfg2=strarr(n_shells*2)
	   shell=strarr(n_shells)
	   eiss_shell=strarr(n_shells)
	   occup=strarr(n_shells)
	   reads,cfg,cfg2,format='('+strtrim(string(n_shells),2)+'(1a2,a2,1x))'
	   for i=0,n_shells-1 do begin
	       shell[i]=cfg2[i*2]
	       if i eq 0 then begin
	          occup[i]=strtrim(cfg2[i*2+1],2)
	       endif else begin 
	          occup[i]=strtrim(string(fix(cfg2[i*2+1])+50),2)
	       endelse
	       for j=0,20 do begin
	           if shell[i] eq std_confgs[j] then begin
		      eiss_shell[i]=strtrim(string(eiss_no[j]),2)
		   endif
	       endfor
	   endfor
	   cfg_out=''
	   for i=0,n_shells-1 do begin
	       cfg_out=cfg_out+occup[i]+eiss_shell[i]
	   endfor
	endif
	goto,lab_end
;-----------------------------------------------------------------------
; Eissner -> standard conversion
;-----------------------------------------------------------------------
	if cfg_type eq 1 then begin
	   res=strlen(strtrim(cfg,2))
	   n_shells=fix((res+1)/3)
	   cfg2=strarr(n_shells*2)
	   shell=strarr(n_shells)
	   std_shell=strarr(n_shells)
	   occup=strarr(n_shells)
	   cfg=' '+strtrim(cfg,2)
	   reads,cfg,cfg2, format='('+strtrim(string(n_shells),2)+'(I2,1A1))'
	   for i=0,n_shells-1 do begin
	       shell[i]=cfg2[i*2+1]
	       if i eq 0 then begin
	          occup[i]=strtrim(cfg2[i*2],2)
	       endif else begin 
	          occup[i]=strtrim(string(fix(cfg2[i*2])-50),2)
	       endelse
	       for j=0,20 do begin
	           if shell[i] eq strtrim(eiss_no[j],2) then $
		      std_shell[i]=strtrim(string(std_confgs[j]),2)
	       endfor
	   endfor
	   cfg_out=''
	   for i=0,n_shells-1 do begin
	       if occup[i] lt 9 then occup[i]=occup[i]+' '
	       cfg_out=cfg_out+std_shell[i]+occup[i]+' '
	   endfor
	endif
	goto,lab_end


lab_occ:
	cfg_out=''
	eiss_val=''
;-----------------------------------------------------------------------
; occupation array -> standard conversion
;-----------------------------------------------------------------------
	if cfg_type eq 1 then begin
		str_occup=strtrim(string(occup),2)
		n_shells=n_elements(occup)
		for i=0,n_shells-1 do begin
			if occup[i] eq 0 then goto,jump1
			if occup[i] lt 9 then str_occup[i]=str_occup[i]+' '
			cfg_out=cfg_out+std_confgs[i]+str_occup[i]+' '
jump1:
		endfor
	endif
	
;-----------------------------------------------------------------------
; occupation array -> Eissner conversion
;-----------------------------------------------------------------------
	if cfg_type eq 0 then begin
		a=where(occup ne 0)
		b=fix(n_elements(a))
		n_shells=a[b-1]
		first_shl=fix(a[0])
		for i=0,n_shells do begin
			if occup[i] eq 0 then goto,jump2
			if i eq first_shl then begin
				eiss_val=strtrim(string(occup[i]),2)
			endif else begin
	          		eiss_val=strtrim(string(occup[i]+50),2)
			endelse
		cfg_out=cfg_out+eiss_val+eiss_no[i]
jump2:
		endfor
	endif
lab_end:	
	cfg_out=strtrim(cfg_out,2)
	return,cfg_out
end
