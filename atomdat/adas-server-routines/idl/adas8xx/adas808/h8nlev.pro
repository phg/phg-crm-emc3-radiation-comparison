;-----------------------------------------------------------------------------
;+
;
; NAME     : h8nlev
;
; PURPOSE  : evaluate the number of levels in a given configuration - specified
;            via an occupation array 
;
; CATEGORY : ADAS
;
; NOTES    : configuration occupation array, as illustrated below, is 2-d 
;            indexed by 
;                       column -  orbital shell index 
;                       row    -  configuration number 
;
;            For configurations 1s2 3d1
;       
;                occup=[2,0,0,0,0,1]
;
;            with standard ordering and indexing of orbitals 
;  
; USE      :  no_of_levels = h8nlev(occup, term_count=term_count) 
;
; INPUT    :
;
;   (I*4)   occup          = occupation numbers of configurations
;                            1st dim: orbital shell index.
;
; OPTIONAL INPUTS:

;   (I*4)   term_count     = keyword = return number of terms
;
; OUTPUTS:
;
;   (I*4)   h8nlev         = number of levels for configuration 
;
; OPTIONAL OUTPUTS:
;
;   (I*4)   term_count     = keyword = number of terms for configuration
;                                       
;
; ROUTINES: 
;          NAME 	 TYPE	 COMMENT
;          h8nlev_jcnt	 local   number of levels for an LS-term 
;          h8nlev_addm	 local   finds new terms from adding two terms 
;
;
; CATEGORY:
;       Adas system.
;
; WRITTEN:
;       S. D. Loch, University of Strathclyde,20/11/01.
;
; MODIFIED:
;       1.1     S. D. Loch
;               First version put into CVS.
;
;       1.2     H. P. Summers
;               Reorganisated and extended to shell 8k with revised local functions.
;
;       1.3     A. D. Whiteford
;               Modification to allow level counts > 32,768 (limit of
;		default IDL integers)
;
; VERSION:
;       1.1   20/11/01
;       1.2   22/08/06
;       1.3   14/11/06
; 
;-
;-----------------------------------------------------------------------------
FUNCTION h8nlev_jcnt,term

;  returns number of J-values for an input term. The LS-term
;  is held as an integer 100*2S+L

	l  = term mod 100
	ss = (term-l)/100
	
	jcnt = (2*l < ss) +1
	
	return,jcnt

END
;-----------------------------------------------------------------------------
PRO h8nlev_addm, term1, term2, new_term_repetitions, new_terms

;  obtains a vector of distinct resultant terms from adding two 
;  input terms and a vector of the number of repetitions of each   
;  new term.  LS-terms are held as integers 100*2S+L.
    

 	l1 = term1 mod 100
	l2 = term2 mod 100
	
	ss1 = (term1-l1)/100
	ss2 = (term2-l2)/100
	
	lindx  = 2*(l1 < l2)+1
	ssindx = (ss1 <ss2)+1
	
	larr=indgen(lindx) + abs(l1-l2)
	ssarr=2*indgen(ssindx) + abs(ss1-ss2)
	 

	indx=0
	terms_tmp=intarr(lindx*ssindx)
	for i=0,ssindx-1 do begin
		for j=0,lindx-1 do begin
			terms_tmp(indx)=ssarr[i]*100+larr[j]
			indx=indx+1
		endfor
	endfor
	
	terms_order=terms_tmp[sort(terms_tmp)]
	new_terms=terms_order[uniq(terms_order)]
	new_term_repetitions=intarr(n_elements(new_terms))
	for i = 0, n_elements(new_terms)-1 do begin
		new_term_repetitions[i]=n_elements(where(terms_order eq new_terms[i]))
	endfor
END
;-----------------------------------------------------------------------------
FUNCTION h8nlev, ioccup      ,                                         $
                 term_count = term_count

	compile_opt defint32

;-----------------------------------------------------------------------------
; definitions and occupations to ndshell=36 (8k) : specify common 'shells'
;-----------------------------------------------------------------------------


   ndshell = 36
   shmax   = [   2,   2,   6,   2,   6,  10,   2,   6,  10,  14,   2,   6,  10,  14,  18,   2,   6,  10,      $
                14,  18,  22,   2,   6,  10,  14,  18,  22,  26,   2,   6,  10,  14,  18,  22,  26,  30]
   nval    = [   1,   2,   2,   3,   3,   3,   4,   4,   4,   4,   5,   5,   5,   5,   5,   6,   6,   6,      $
                 6,   6,   6,   7,   7,   7,   7,   7,   7,   7,   8,   8,   8,   8,   8,   8,   8,   8]
   lval    = [   0,   0,   1,   0,   1,   2,   0,   1,   2,   3,   0,   1,   2,   3,   4,   0,   1,   2,      $
                 3,   4,   5,   0,   1,   2,   3,   4,   5,   6,   0,   1,   2,   3,   4,   5,   6,   7]
   shl_lab = ['1s','2s','2p','3s','3p','3d','4s','4p','4d','4f','5s','5p','5d','5f','5g','6s','6p','6d',      $
              '6f','6g','6h','7s','7p','7d','7f','7g','7h','7i','8s','8p','8d','8f','8g','8h','8i','8k']
	      
;--------------------------------------------------------------------
;  internally an LS-term is  represented by single integer = 100*2S+L 	       
;--------------------------------------------------------------------
	s1=[100]
	s2=[0]
	s1_mult=[1]
	s2_mult=[1]
	p1=[101]
	p2=[0,2,201]
	p3=[101,102,300]
	p1_mult=[1]
	p2_mult=[1,1,1]
	p3_mult=[1,1,1]
	d1=[102]
	d2=[0,2,4,201,203]
	d3=[101,102,103,104,105,301,303]
	d4=[0,2,3,4,6,201,202,203,204,205,402]
	d5=[100,101,102,103,104,105,106,301,302,303,304,500]
	d1_mult=[1]
	d2_mult=[1,1,1,1,1]
	d3_mult=[1,2,1,1,1,1,1]
	d4_mult=[2,2,1,2,1,2,1,2,1,1,1]
	d5_mult=[1,1,3,2,2,1,1,1,1,1,1,1]
	f1=[103]
	f2=[0,2,4,6,201,203,205]
	f3=[101,102,103,104,105,106,107,108,$
		300,302,303,304,306]
	f4=[0,2,3,4,5,6,7,8,10,201,202,203,204,$
		205,206,207,208,209,400,402,403,404,406]
	f5=[101,102,103,104,105,$
		106,107,108,109,110,111,300,301,302,303,$
		304,305,306,307,308,309,501,503,505]
	f6=[0,1,2,3,4,5,6,7,8,9,10,12,201,202,203,204,205,206,207,$
		208,209,210,211,400,401,402,403,404,405,406,407,408,603]
	f7=[100,101,102,103,104,105,106,107,108,109,110,$
		111,112,300,301,302,303,$
		304,305,306,307,308,309,310,$
		501,502,503,504,505,506,700]
	f1_mult=[1]
	f2_mult=[1,1,1,1,1,1,1]
	f3_mult=[1,2,2,2,2,1,1,1,1,1,1,1,1]
	f4_mult=[2,4,1,4,2,3,1,2,1,3,2,4,3,4,2,2,1,1,1,1,1,1,1]
	f5_mult=[4,5,7,6,7,5,5,3,2,1,1,1,2,3,4,4,3,3,2,1,1,1,1,1]
	f6_mult=[4,1,6,4,8,4,7,3,4,2,2,1,6,5,9,7,9,6,6,3,3,1,1,1,1,3,2,3,2,2,1,1,1]
	f7_mult=[2,5,7,10,10,9,9,7,5,4,2,1,1,2,2,6,5,7,5,5,3,3,1,1,1,1,1,1,1,1,1]
	
	g1=[104]
	g2=[0,2,4,6,8,201,203,205,207]
	g1_mult=[1]
	g2_mult=[1,1,1,1,1,1,1,1,1]
	
	h1=[105]
	h2=[0,2,4,6,8,10,201,203,205,207,209]
	h1_mult=[1]
	h2_mult=[1,1,1,1,1,1,1,1,1,1,1]
	
	i1=[106]
	i2=[0,2,4,6,8,10,12,201,203,205,207,209,211]
	i1_mult=[1]
	i2_mult=[1,1,1,1,1,1,1,1,1,1,1,1,1]
	
	k1=[107]
	k2=[0,2,4,6,8,10,12,14,201,203,205,207,209,211,213]
	k1_mult=[1]
	k2_mult=[1,1,1,1,1,1,1,1,1,1,1,1,1,1,1]

	
	
	level_count=0
	iflag=0
	
	n_shells=n_elements(ioccup)
	mult_hold=intarr(1)
	for i=0,n_shells-1 do begin
		if ioccup[i] ne 0 and ioccup[i] ne shmax[i] then begin
			if i eq 0 or i eq 1 or i eq 3 or i eq 6 or i eq 10 or i eq 15 or i eq 21 or i eq 28 then begin
				term_tmp=s1
				mult_tmp=s1_mult
				iflag=iflag+1
			endif
			if i eq 2 or i eq 4 or i eq 7 or i eq 11 or i eq 16 or i eq 22 or i eq 29 then begin
				if ioccup[i] eq 1 or ioccup[i] eq 5 then begin
					term_tmp=p1
					mult_tmp=p1_mult
				endif
				if ioccup[i] eq 2 or ioccup[i] eq 4 then begin
					term_tmp=p2
					mult_tmp=p2_mult
				endif
				if ioccup[i] eq 3 then begin
					term_tmp=p3
					mult_tmp=p3_mult
				endif
				iflag=iflag+1
			endif
			if i eq 5 or i eq 8 or i eq 12 or i eq 17 or i eq 23 or i eq 30 then begin
				if ioccup[i] eq 1 or ioccup[i] eq 9 then begin
					term_tmp=d1
					mult_tmp=d1_mult
				endif
				if ioccup[i] eq 2 or ioccup[i] eq 8 then begin
					term_tmp=d2
					mult_tmp=d2_mult
				endif
				if ioccup[i] eq 3 or ioccup[i] eq 7 then begin
					term_tmp=d3
					mult_tmp=d3_mult
				endif
				if ioccup[i] eq 4 or ioccup[i] eq 6 then begin
					term_tmp=d4
					mult_tmp=d4_mult
				endif
				if ioccup[i] eq 5 then begin
					term_tmp=d5
					mult_tmp=d5_mult
				endif
				iflag=iflag+1
			endif
			if i eq 9 or i eq 13 or i eq 18 or i eq 24 or i eq 31 then begin
				if ioccup[i] eq 1 or ioccup[i] eq 13 then begin
					term_tmp=f1
					mult_tmp=f1_mult
				endif
				if ioccup[i] eq 2 or ioccup[i] eq 12 then begin
					term_tmp=f2
					mult_tmp=f2_mult
				endif
				if ioccup[i] eq 3 or ioccup[i] eq 11 then begin
					term_tmp=f3
					mult_tmp=f3_mult
				endif
				if ioccup[i] eq 4 or ioccup[i] eq 10 then begin
					term_tmp=f4
					mult_tmp=f4_mult
				endif
				if ioccup[i] eq 5 or ioccup[i] eq 9 then begin
					term_tmp=f5
					mult_tmp=f5_mult
				endif
				if ioccup[i] eq 6 or ioccup[i] eq 8 then begin
					term_tmp=f6
					mult_tmp=f6_mult
				endif
				if ioccup[i] eq 7 then begin
					term_tmp=f7
					mult_tmp=f7_mult
				endif
				iflag=iflag+1
			endif
			if i eq 14 or i eq 19 or i eq 25 or i eq 32 then begin
				if ioccup[i] eq 1 then begin
					term_tmp=g1
					mult_tmp=g1_mult
				endif
				if ioccup[i] eq 2 then begin
					term_tmp=g2
					mult_tmp=g2_mult
				endif
				iflag=iflag+1
			endif
			if i eq 20 or i eq 26 or i eq 33 then begin
				if ioccup[i] eq 1 then begin
					term_tmp=h1
					mult_tmp=h1_mult
				endif
				if ioccup[i] eq 2 then begin
					term_tmp=h2
					mult_tmp=h2_mult
				endif
				iflag=iflag+1
			endif
			if i eq 27 or i eq 34 then begin
				if ioccup[i] eq 1 then begin
					term_tmp=i1
					mult_tmp=i1_mult
				endif
				if ioccup[i] eq 2 then begin
					term_tmp=i2
					mult_tmp=i2_mult
				endif
				iflag=iflag+1
			endif
			if i eq 35 then begin
				if ioccup[i] eq 1 then begin
					term_tmp=k1
					mult_tmp=k1_mult
				endif
				if ioccup[i] eq 2 then begin
					term_tmp=k2
					mult_tmp=k2_mult
				endif
				iflag=iflag+1
			endif
			if iflag eq 1 then begin
				term1 = term_tmp
				mult1 = mult_tmp
			endif else begin
				term2 = term_tmp
				mult2 = mult_tmp
			endelse
			if iflag gt 1 then begin
				n_term1=n_elements(term1)
				n_term2=n_elements(term2)
				indx=0
				for k=0,n_term1-1 do begin
					for j=0,n_term2-1 do begin
						h8nlev_addm,term1[k],term2[j], mult_add,terms
						if indx eq 0 then begin
							term_hold=terms
							mult_hold=mult1[k]*mult2[j]*mult_add
						endif else begin
							term_hold=[term_hold,terms]
							mult_hold=[mult_hold,mult1[k]*mult2[j]*mult_add]
						endelse
						indx=indx+1
					endfor
				endfor
				term1 = term_hold
				mult1 = mult_hold
			endif	
		endif
	endfor

	if iflag eq 1 then begin
		term_hold=term1
		mult_hold=mult1
	endif
	if iflag eq 0 then begin
		term_hold=[0]
		mult_hold=[1]
	endif
	term_list=term_hold
	mult_list=mult_hold
	n_term_list=n_elements(term_list)
	level_count=0
	i=long(0)
	
	for i=0l,n_term_list-1 do begin
		level_count=level_count+h8nlev_jcnt(term_list[i])*mult_list[i]
	endfor
	
	term_count = n_term_list
	
return,level_count

END
