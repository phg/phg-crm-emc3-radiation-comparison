; SCCS INFO Module: @(#)$Header: /home/adascvs/idl/adas8xx/adas808/adas808_out.pro,v 1.1 2004/07/06 11:06:45 whitefor Exp $  Data $Date: 2004/07/06 11:06:45 $
;+
; PROJECT:
;       ADAS 
;
; NAME:
;	ADAS216_OUT
;
; PURPOSE:
;	IDL ADAS user interface, output options.
;
; EXPLANATION:
;	This routine creates and manages a pop-up window which allows
;	the user to select options and input data to control ADAS216
;	graphical and file output.
;
; USE:
;	This routine is ADAS216 specific, see bgspf1.pro for how it
;	is used.
;
; INPUTS:
;	VAL	- A structure which determines the initial settings of
;		  the output options widget.  The value is passed
;		  unmodified into cw_adas216_out.pro.
;
;		  See cw_adas216_out.pro for a full description of this
;		  structure.
;
;	DSFULL	- String; The full system file name of the input 
;		  dataset selected by the user for processing.
;
;       BITFILE - String; the path to the dirctory containing bitmaps
;                 for the 'escape to series menu' button.
;
; OPTIONAL INPUTS:
;	None.
;
; OUTPUTS:
;       ACT     - String; 'Done', 'Cancel' or 'Menu' for the button the
;		  user pressed to terminate the output options window.
;
; OPTIONAL OUTPUTS:
;	None.
;
; KEYWORD PARAMETERS:
;	DEVLIST	- A string array;  Each element of the array is the
;		  name of a hardcopy graphics output device.  This
;		  list must mirror the DEVCODE list.  DEVLIST is the
;		  list which the user will see.
;		  e.g ['Post-Script','HP-PCL','HP-GL']
;
;	FONT	- Supplies the font to be used for the interface widgets.
;
; CALLS:
;	CW_ADAS216_OUT	Creates the output options widget.
;	DYNLABEL	Recursively adds dynamic_resize keyword to label
;			widgets for version 4.0.1 and higher of IDL
;	XMANAGER
;	See side effects for widget management routine.
;
; SIDE EFFECTS:
;	This routine uses a common block OUT216_BLK to maintain its state.
;	ADAS216_OUT_EV	is included in this file and is called
;			indirectly from XMANAGER during widget
;			management.
;
; CATEGORY:
;	Widgets
;
; WRITTEN:
;       Martin O'Mullane
;
; MODIFIED:
;	1.1	Martin O'Mullane
;		First release 
;
; VERSION:
;       1.1	17-03-99
;
;-
;-----------------------------------------------------------------------------

PRO adas808_out_ev, event

    COMMON out808_blk, action, value
	

		;**** Find the event type and copy to common ****

    action = event.action

    CASE action of

		;**** 'Done' button ****

	'Done'  : begin

		;**** Get the output widget value ****

     	    child = widget_info(event.id,/child)
	    widget_control, child, get_value=value 

                ;*****************************************
		;**** Kill the widget to allow IDL to ****
		;**** continue and interface with     ****
		;**** FORTRAN only if there is work   ****
		;**** for the FORTRAN to do.          ****
                ;*****************************************

     	    ;if (value.texout eq 1) then begin
	        widget_control, event.top, /destroy
	    ;endif 
	end

		;**** 'Cancel' button ****

	'Cancel': widget_control, event.top, /destroy

                ;**** 'Menu' button ****

        'Menu': widget_control, event.top, /destroy

        ELSE:   ;**** Do nothing ****

    ENDCASE

END

;-----------------------------------------------------------------------------

PRO adas808_out, val, dsfull, act, bitfile,  font=font

    COMMON out808_blk, action, value

		;**** Copy value to common ****

    value = val

		;**** Set defaults for keywords ****

    if not (keyword_set(font)) then font = ''
    if not (keyword_set(devlist)) then devlist = ''

		;***************************************
		;**** Pop-up a new widget           ****
		;***************************************

                ;**** create base widget ****

    outid = widget_base(title='ADAS808 OUTPUT OPTIONS', xoffset=100,	$
                        yoffset=1)

		;**** Declare output options widget ****

    cwid = cw_adas808_out(outid, dsfull, bitfile, value=value, font=font )

		;**** Realize the new widget ****

    dynlabel, outid
    widget_control, outid, /realize

		;**** make widget modal ****

    xmanager, 'adas808_out', outid, event_handler='adas808_out_ev',	$
              /modal, /just_reg
 
		;**** Return the output value from common ****

    act = action
    val = value

END

