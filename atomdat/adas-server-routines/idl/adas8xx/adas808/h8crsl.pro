; SCCS INFO Module: @(#)$Header: /home/adascvs/idl/adas8xx/adas808/h8crsl.pro,v 1.1 2004/07/06 14:01:24 whitefor Exp $  Data $Date: 2004/07/06 14:01:24 $
pro h8crsl,state,ion_ch,data,tev,edens
		
	;stop
	
	ntran=(*state.atomdata[ion_ch]).ntran
	eav=(*state.atomdata[ion_ch]).eav
	;conf1=(*state.atomdata[ion_ch]).conf1
	;conf2=(*state.atomdata[ion_ch]).conf2
	stat_wt=(*state.atomdata[ion_ch]).stat_wt
	aval=(*state.atomdata[ion_ch]).aval
	oscstr=(*state.atomdata[ion_ch]).oscstr
	indx_1=(*state.atomdata[ion_ch]).indx_1
	indx_2=(*state.atomdata[ion_ch]).indx_2
	wavel=(*state.atomdata[ion_ch]).wavel
	cross=(*state.atomdata[ion_ch]).cross

	

        if (*state.atomdata[ion_ch]).bessel eq 0 then begin
	        effcollstr=dblarr(ntran)        
                qup=dblarr(ntran)
                qdown=dblarr(ntran)
                pec=dblarr(ntran)
                conf1=intarr(ntran)
                conf2=intarr(ntran)
        	arstup=lonarr(ntran)
	        arstlow=lonarr(ntran)
        	argiiiav=dblarr(ntran)
        endif else begin
	        effcollstr=dblarr((size((*state.atomdata[ion_ch]).collindx_1))[1])        
                qup=dblarr((size((*state.atomdata[ion_ch]).collindx_1))[1])
                qdown=dblarr((size((*state.atomdata[ion_ch]).collindx_1))[1])
                pec=dblarr((size((*state.atomdata[ion_ch]).collindx_1))[1])
                conf1=intarr((size((*state.atomdata[ion_ch]).collindx_1))[1])
                conf2=intarr((size((*state.atomdata[ion_ch]).collindx_1))[1])
        	arstup=lonarr((size((*state.atomdata[ion_ch]).collindx_1))[1])
	        arstlow=lonarr((size((*state.atomdata[ion_ch]).collindx_1))[1])
        	argiiiav=dblarr((size((*state.atomdata[ion_ch]).collindx_1))[1])
        endelse

        h8effc,[tev],1,effcollstr,(*state.atomdata[ion_ch]),ion_ch,state.fortdir
        
        
        if (*state.atomdata[ion_ch]).bessel eq 0 then begin
                for i=0,ntran-1 do begin
        		if eav(indx_1[i]) gt eav(indx_2[i]) then begin
		        	stat_up=stat_wt[indx_1[i]]
	        		stat_low=stat_wt[indx_2[i]]
        		endif else begin
		        	stat_up=stat_wt[indx_2[i]]
	        		stat_low=stat_wt[indx_1[i]]		
        		endelse
                
                        ; adas manual, equation 2.1.8
        		
                        ediff=abs(eav(indx_1[i]) - eav(indx_2[i]))
                        
                        qdown[i]=(2.1716e-8 / stat_up) * (13.606 / tev)^(1./2.) * effcollstr[i]
		        qup[i]= exp (ediff * 13.606 / tev) * (double(stat_low) / stat_up) * qdown[i]

	        	if stat_up eq 0 or stat_low eq 0 then begin
        			print,'Error calculating Statistical Weight, setting A-value and rates to 1e-30'
		        	aval[i]=1e-30
	        		qup[i]=1e-30
        			qdown[i]=1e-30
		        endif		
                endfor
        
        endif else begin
                aval=dblarr((size((*state.atomdata[ion_ch]).collindx_1))[1])
                wavel=dblarr((size((*state.atomdata[ion_ch]).collindx_1))[1])
                for i=0,(size((*state.atomdata[ion_ch]).collindx_1))[1]-1 do begin

        		if eav((*state.atomdata[ion_ch]).collindx_1[i]) gt eav((*state.atomdata[ion_ch]).collindx_2[i]) then begin
		        	stat_up=stat_wt[(*state.atomdata[ion_ch]).collindx_1[i]]
	        		stat_low=stat_wt[(*state.atomdata[ion_ch]).collindx_2[i]]
        		endif else begin
		        	stat_up=stat_wt[(*state.atomdata[ion_ch]).collindx_2[i]]
	        		stat_low=stat_wt[(*state.atomdata[ion_ch]).collindx_1[i]]		
        		endelse
        
                        for j=0,(*state.atomdata[ion_ch]).ntran-1 do begin
                                if ((*state.atomdata[ion_ch]).indx_1[j] eq (*state.atomdata[ion_ch]).collindx_1[i] $
                                and (*state.atomdata[ion_ch]).indx_2[j] eq (*state.atomdata[ion_ch]).collindx_2[i]) $
                                or ((*state.atomdata[ion_ch]).indx_2[j] eq (*state.atomdata[ion_ch]).collindx_1[i] $
                                and (*state.atomdata[ion_ch]).indx_1[j] eq (*state.atomdata[ion_ch]).collindx_2[i]) $
                                then begin
                                        aval[i]=(*state.atomdata[ion_ch]).aval[j]
                                        wavel[i]=(*state.atomdata[ion_ch]).wavel[j]
                                endif
                        end
                        

                        ediff=abs(eav((*state.atomdata[ion_ch]).collindx_1[i]) - eav((*state.atomdata[ion_ch]).collindx_2[i]))

        		qdown[i]=(2.1716e-8 / stat_up) * (13.606 / tev)^(1./2.) * effcollstr[i]
		        qup[i]= exp (ediff * 13.606 / tev) * (double(stat_low) / stat_up) * qdown[i]
                end
        endelse
        
        		
	rates=dblarr(state.nconf[ion_ch],state.nconf[ion_ch])
	rates[*]=1e-30
	
        if (*state.atomdata[ion_ch]).bessel eq 0 then begin
                for i=0,state.nconf[ion_ch]-1 do begin
		        for j=0,state.nconf[ion_ch]-1 do begin
			        for k=0,ntran-1 do begin
				        if eav(indx_1[k]) gt eav(indx_2[k]) then begin
					        upper_index=indx_1[k]
        					lower_index=indx_2[k]
	        			endif else begin
		        			upper_index=indx_2[k]
			        		lower_index=indx_1[k]
				        endelse
				
        				if eav[i] gt eav[j] then begin
	        				if upper_index eq i and lower_index eq j then begin
		        				rates[i,j]=aval[k]+edens*qdown[k]
			        		endif			
				        endif
				
        				if eav[j] gt eav[i] then begin
	        				if lower_index eq i and upper_index eq j then begin
		        				rates[i,j]=edens*qup[k]
			        		endif			
				        endif				
        			end		
	        	end
        	end
        endif else begin
                for i=0,state.nconf[ion_ch]-1 do begin
		        for j=0,state.nconf[ion_ch]-1 do begin
			        for k=0,(size(qup))[1]-1 do begin
				        if eav((*state.atomdata[ion_ch]).collindx_1[k]) gt eav((*state.atomdata[ion_ch]).collindx_2[k]) then begin
					        upper_index=(*state.atomdata[ion_ch]).collindx_1[k]
        					lower_index=(*state.atomdata[ion_ch]).collindx_2[k]
	        			endif else begin
		        			upper_index=(*state.atomdata[ion_ch]).collindx_2[k]
			        		lower_index=(*state.atomdata[ion_ch]).collindx_1[k]
				        endelse
        
				
        				if eav[i] gt eav[j] then begin
	        				if upper_index eq i and lower_index eq j then begin
		        				rates[i,j]=aval[k]+edens*qdown[k]
			        		endif			
				        endif
				
        				if eav[j] gt eav[i] then begin
	        				if lower_index eq i and upper_index eq j then begin
		        				rates[i,j]=edens*qup[k]
			        		endif			
				        endif				
                        
                        
                                end
                        end
                end
        
        
        endelse
        	
		
	crmat=dblarr(state.nconf[ion_ch],state.nconf[ion_ch])
	
	crmat[*]=1e-30
	
	for i=0,state.nconf[ion_ch]-1 do begin
		for j=0,state.nconf[ion_ch]-1 do begin
			if i eq j then begin
				crmat[i,i]=-total(rates[i,*])
			endif else begin		
				crmat[j,i]=rates[j,i]
			endelse
		end
	end
	
	;stop
	
	a=where(crmat eq 0)
	if a[0] ne -1 then crmat[a]=1e-30
		
	crinv=invert(crmat[1:*,1:*],/double)
	
	popn=dblarr(state.nconf[ion_ch])
	popn[0]=1
	for i=0,state.nconf[ion_ch]-2 do begin
		popn(i+1)=-total(crinv[*,i]*crmat[0,1:*])
	end

;	print,popn

        if (*state.atomdata[ion_ch]).bessel eq 0 then begin

                for i=0,ntran-1 do begin
		        if eav[indx_1[i]] gt eav[indx_2[i]] then begin
			        upper_index=indx_1[i]
		        endif else begin       
			        upper_index=indx_2[i]
		        endelse
		        pec[i]=aval[i]*popn[upper_index]
                        conf1[i]=cross[indx_1[i]]
                        conf2[i]=cross[indx_2[i]]
	        end

        endif else begin

                for i=0,(size(pec))[1]-1 do begin
		        if eav[(*state.atomdata[ion_ch]).collindx_1[i]] gt eav[(*state.atomdata[ion_ch]).collindx_2[i]] then begin
			        upper_index=(*state.atomdata[ion_ch]).collindx_1[i]
		        endif else begin       
			        upper_index=(*state.atomdata[ion_ch]).collindx_2[i]
		        endelse
		        pec[i]=aval[i]*popn[upper_index]
                        conf1[i]=cross[(*state.atomdata[ion_ch]).collindx_1[i]]
                        conf2[i]=cross[(*state.atomdata[ion_ch]).collindx_2[i]]
                end
                
        
        
        endelse

        DATA={AVAL:pec,WAVEL:WAVEL,CONF1:CONF1,CONF2:CONF2}
       
       
;       	print,crmat
	
;	print,'------------------'
	
;	print,crinv
       
	
;		print,'------------------',oscstr
;		print,'------------------',effcollstr
;		print,'------------------',aval
;       	print,'------------------',double(arstup)
;		print,'------------------',double(arstlow)
;		print,'------------------',argiiiav
	
;		print,popn
;		print,eav

        ;stop

        

end
