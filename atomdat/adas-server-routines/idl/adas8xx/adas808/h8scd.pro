; SCCS INFO Module: @(#)$Header: /home/adascvs/idl/adas8xx/adas808/h8scd.pro,v 1.1 2004/07/06 14:02:44 whitefor Exp $  Data $Date: 2004/07/06 14:02:44 $
pro h8scd,z,tev,edens,ionpot,ioccup,scd,ei

shmax=[2,2,6,2,6,10,2,6,10,14,2,6,10,14,18,2,6,10,14,18,22,$
	2,6,10,14,18,22,26,2,6,10,14,18,22,26,30]
lvalue=[0,0,1,0,1,2,0,1,2,3,0,1,2,3,4,0,1,2,3,4,5,$
	0,1,2,3,4,5,6,0,1,2,3,4,5,6,7]

nshellno=[1,2,2,3,3,3,4,4,4,4,5,5,5,5,5,6,6,6,6,6,6,$
	7,7,7,7,7,7,7,8,8,8,8,8,8,8,8]

;convert ionpot to ev

ionev=ionpot[z]

z1=z+1

grnd=ioccup[z,0,*]

for i=n_elements(grnd)-1,0,-1 do begin
	if grnd[i] gt 0 then begin
		orbital=i
		goto,foundit
	endif
end

foundit:

a=4.5
b=0.
c=0.

equiv=grnd[orbital]


; adas manual, equation 5.8.11, assume only ionise from highest shell

;scd=1.42e-6 *(13.606 / tev)^(3./2.)*a*equiv*(expint(1,ionev/tev) * (tev/ionev) -b * exp(c) * expint(1,ionev / (tev + c) ) * ((tev + c )/ ionev) )

; gives crap result because who knows what a, b and c are.

; Lets try BCHID expression one day
; BCHID call to assemble ionisation rate for each n-shell

;stop
val_shl_indx=1
sbchid=0.e0
valnce_eav=0
for i=7,0,-1 do begin
	indx=where(nshellno eq i+1)
	ioc_tmp=grnd[indx]
	d=where(ioc_tmp,count)
	if val_shl_indx eq 1 then begin 
		if count ne 0 then begin
			icount=0
			xi=0
			zeta=0
			tmp_val=0.e0
			for j=0,n_elements(ioc_tmp)-1 do begin
				if grnd[indx[j]] ne 0 then begin
					zeta=zeta+grnd[indx[j]]
					valnce_eav=valnce_eav+ei[0,indx[j]]
					icount=icount+1
				endif
			endfor
			valnce_eav=valnce_eav/icount/13.606
			xi=ionpot[z]
			val_shl_indx=0
			h8rbchid,z,xi,zeta,tev, tmp_val
			sbchid=sbchid+tmp_val
		endif
	endif else begin
		if count ne 0 then begin
			totl=0
			xi=0.e0
			zeta=0
			tmp_val=0.e0
			for j=0,n_elements(ioc_tmp)-1 do begin
				if grnd[indx[j]] ne 0 then begin
					zeta=zeta+grnd[indx[j]]
					xi=(2*lvalue[indx[j]]+1)*ei[0,indx[j]]
					totl=totl+(2*lvalue[indx[j]]+1)
				endif
			endfor
			xi=(valnce_eav-xi/totl)/13.606
			h8rbchid,z,xi,zeta,tev, tmp_val
			sbchid=sbchid+tmp_val
		endif
	endelse
	zeta=0
endfor
scd=sbchid

;print,'ECIP ionisation rate=',scd
end
