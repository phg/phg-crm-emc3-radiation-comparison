;-----------------------------------------------------------------------------
;+
;
; NAME     : adas8xx_opt_initialise_rules
;
; PURPOSE  : analyse the ground configuration string and extract
;          : shell populations and initial promotion rules
;
; CATEGORY : ADAS
;
; NOTES    :
;  
; USE      : 
;
; INPUT    :
;
;   (C* )   config          = ground configuration string
;
; OUTPUTS   :
;           data structure containing:
;   (C* )  configs    = config split into array of seperate strings
;   (I*4)  n          = n quantum number of each shell
;   (I*4)  l          = l quantum number of each shell
;   (I*4)  nel        = number of electrons in each shell
;   (I*4)  occupied   = 2: shell is full
;                       1: shell is partially occupied
;                       0: shell is empty
;   (I*4)  iv1        = index of 1st valence shell
;   (I*4)  v1_n       = n quantum number of 1st valence shell
;   (I*4)  v1_l       = l quantum number of 1st valence shell
;   (I*4)  no_v_shl   = number of valence shells
;   (I*4)  iv2        = index of 2nd valence shell, -1 if not present
;   (I*4)  v2_n       = n quantum number of 2nd valence shell
;   (I*4)  v2_l       = l quantum number of 2nd valence shell
;   (I*4)  icl        = index of 1st closed shell, -1 if not present
;   (I*4)  cl_n       = n quantum number of closed shell
;   (I*4)  cl_l       = l quantum number of closed shell
;   (I*4)  prom_cl    = 1 if promotion from closed shell is allowed
;   (I*4)  max_occup  = max population of each nl shell (hardcoded)
;   (I*4)  allow_4f   = 1 if 4f shell is present but not full
;
; CATEGORY:
;       Adas system.
;
; WRITTEN:
;       Adam Foster, University of Strathclyde,08-01-08
;
; MODIFIED:
;       1.1     Adam Foster

; VERSION:
;       1.1   08-01-08
; 
;-
;-----------------------------------------------------------------------------


FUNCTION adas8xx_opt_initialise_rules, config

tmp=STRSPLIT(config,' ',/EXTRACT)
configs=tmp

; create 3 arrays, containing n,l,nel for each shell

n=intarr(n_elements(tmp))
l=intarr(n_elements(tmp))
nel=intarr(n_elements(tmp))

; create an array indicating whether each element is full (2), 
; partiall full (1) or empty (0)

occupied=intarr(n_elements(n))
nshells=n_elements(n)

; now read in each string and parse it

FOR i=0, n_elements(tmp)-1 DO BEGIN

  n[i]=FIX(strmid(tmp[i],0,1))
  ltmp=(strmid(tmp[i],1,1))
  CASE ltmp OF
   's':l[i]=0
   'p':l[i]=1
   'd':l[i]=2  
   'f':l[i]=3
   'g':l[i]=4
   'h':l[i]=5
   'i':l[i]=6
   'k':l[i]=7       
  ENDCASE

  nel[i]=FIX(strmid(tmp[i],2))

  CASE ltmp OF
   's':full=2
   'p':full=6
   'd':full=10 
   'f':full=14
   'g':full=32
   'h':full=32
   'i':full=32
   'k':full=32      
  ENDCASE
 
  IF nel[i] EQ 0 THEN occupied[i]=0 ELSE $
  IF nel[i] EQ full THEN occupied[i]=2 ELSE $
  occupied[i]=1
 
ENDFOR

; declare valence shell

iv1=nshells -1 
v1_l=l[iv1]
v1_n=n[iv1]

; declare if a valence shell is likely
; initialise to "no"

iv2=-1
no_v_shl=1
v2_l = 0
v2_n = 0

IF n_elements(occupied) NE 1 THEN BEGIN  ; there is more that 1 shell
  partial=where(occupied[0:nshells-2] EQ 1) 
  IF (partial[0] NE -1) THEN BEGIN ;no other shells are empty
    iv2 = max(partial)
    no_v_shl=2
    v2_l = l[iv2]
    v2_n = n[iv2]

 ; prepare exception for if we have 3d10 4s2 4pX
  ENDIF ELSE IF configs[n_elements(configs)-2] EQ '4s2' THEN BEGIN
    iv2 = n_elements(configs)-2
    no_v_shl=2
    v2_l = l[iv2]
    v2_n = n[iv2]
  ENDIF ELSE IF configs[n_elements(configs)-2] EQ '5s2' THEN BEGIN
 ; prepare exception for if we have 5s2 5pX
    iv2 = n_elements(configs)-2
    no_v_shl=2
    v2_l = l[iv2]
    v2_n = n[iv2]
  ENDIF
 
ENDIF

; now prepare prom_cl rules

; set up defaults
cl_n=0
icl=-1  
cl_l=0

prom_cl =0

IF n_elements(occupied) NE 1 THEN BEGIN

; find the highest non-valence closed shell

  full=where(occupied[0:nshells-2] EQ 2)

  IF full[0] NE -1 THEN BEGIN
  
    itmp=where(full NE iv2)
    IF itmp[0] NE -1 THEN BEGIN
      prom_cl =1
      icl=max(itmp)
      cl_n=n[icl]
      cl_l=l[icl]

    ENDIF
  ENDIF  
ENDIF

; prepare 4f rule
allow_4f=0
; see if there is an electron in a shell above 4f
IF n_elements(occupied) GT 9 THEN BEGIN
  IF nel(9) GT 0 AND nel(9) LT 14 THEN BEGIN
    allow_4f = 1
  ENDIF
ENDIF


ret={configs      : configs      , $
     n            : n            , $
     l            : l            , $
     nel          : nel          , $
     occupied     : occupied     , $
     iv1          : iv1          , $
     v1_l         : v1_l         , $
     v1_n         : v1_n         , $
     iv2          : iv2          , $
     no_v_shl     : no_v_shl     , $
     v2_l         : v2_l         , $
     v2_n         : v2_n         , $
     icl          : icl          , $
     cl_n         : cl_n         , $
     cl_l         : cl_l         , $
     prom_cl      : prom_cl      , $
     allow_4f     : allow_4f     , $
     max_occup    : [2,2,6,2,6,10,2,6,10,14,2,6,10,14,18,2,6,10,14,18, $
                  22,2,6,10,14,18,22,26]}

return, ret
END
