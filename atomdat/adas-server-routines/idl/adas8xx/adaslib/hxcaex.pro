;+
; PROJECT : ADAS 
;
; NAME    : HXCAEX()
;
; PURPOSE : Generates configuration average adf04 files
;
; EXPLANATION:
;       This routines takes a given set of configurations, nuclear and ionic
;       charge and generates a complete adf04 file (type I and/or type III)
;       working in a PWB CA approximation.
;
; USE:
;       An example;
;
;       iz0=6
;       iz=3
;        
;       occup=[ [2,1,0,0,0,0], $
;               [2,0,0,0,0,1], $
;               [2,0,0,1,0,0], $
;               [2,0,0,0,1,0], $
;               [2,0,1,0,0,0] $
;               ] 
;        
;       hxcaex,iz,iz0,occup,type1file='adf04_1',type3file='adf04_3'
;
; INPUTS:
;       iz :   Ion charge
;       iz0:   Nuclear charge
;       occup: Occupation numbers of configurations
;                   First index:  configuration number
;                   Second index: orbital number.
;
; OPTIONAL INPUTS:
;       None.
;
; OUTPUTS:
;       None.
;
; OPTIONAL OUTPUTS:
;       None.
;
; KEYWORD PARAMETERS:
;       ionpot:    Ionisation potential, if not specified it will be read from central ADAS
;       type1file: Filename of type I file to produce
;       type3file: Filename of type III file to produce
;       keeppass:  Do not remove temporary passing files.
;       occ2cow:   Convert occupation numbers to Cowan (i.e. adas801) input standard
;
; CALLS:
;       read_adf00: read adf00 files.
;       h4mxwl: Maxwell average collision strengths.
;       xxspln: Used for splining (momentum transfer)
;
; SIDE EFFECTS:
;       This function spawns UNIX commands.
;
; CATEGORY:
;       Series 8 utility.
;
; WRITTEN:
;       Allan Whiteford
;
; MODIFIED:
;       Version 1.1   Allan Whiteford    01-11-2004
;                     First version
;       Version 1.2   Martin O'Mullane
;                     Change name defined in pro statement to hxcaex
;                     to match the name of the file.
; VERSION:
;       1.1     01-11-2004
;       1.2     16-10-2018
;-
;-----------------------------------------------------------------------------
pro hxcaex,iz,iz0,occup,ionpot=ionpot,type1file=type1file,type3file=type3file,keeppass=keeppass

	adasbin=getenv('ADASFORT')+'/'

	if not keyword_set(ionpot) then begin
		read_adf00,z0=iz0,z1=iz,ionpot=ionpot
	endif
	
	if keyword_set(type1file) then begin
		openw,type1,type1file,/get_lun
	endif else begin
		openw,type1,'/dev/null',/get_lun
	endelse
	
	if keyword_set(type3file) then begin
		openw,type3,type3file,/get_lun
	endif else begin
		openw,type3,'/dev/null',/get_lun
	endelse
	
	energy=dblarr((size(occup))[2])
	stat_wt=lonarr((size(occup))[2])
	conf=strarr((size(occup))[2])
	
	shmax=[2,2,6,2,6,10,2,6,10,14,2,6,10,14,18,2,6,10,14,18,22,$
	2,6,10,14,18,22,26,2,6,10,14,18,22,26,30]
	shl_lval=[0,0,1,0,1,2,0,1,2,3,0,1,2,3,4,0,1,2,3,4,5,$
	0,1,2,3,4,5,6,0,1,2,3,4,5,6,7]
	shl_lab=['1s','2s','2p','3s','3p','3d','4s','4p','4d','4f','5s','5p',$
	'5d','5f','5g','6s','6p','6d','6f','6g','6h','7s','7p','7d','7f','7g',$
	'7h','7i','8s','8p','8d','8f','8g','8h','8i','8k']
	eiss_lab=['1','2','3','4','5','6','7','8','9','A','B','C','D','E','F','G','H','I','J','K','L','M','N']

	tegrid=(iz+1)^2d0 * [   5.00e+02, 1.00e+03, 2.00e+03, 3.00e+03, 5.00e+03, 1.00e+04, 1.50e+04, $
            			2.00e+04, 3.00e+04, 5.00e+04, 1.00e+05, 1.50e+05, 2.00e+05, 5.00e+05]

	;tegrid=[1.60e+04, 3.20e+04, 4.80e+04, 8.00e+04, 1.60e+05, 3.20e+05, 4.80e+05, 8.00e+05, 1.60e+06, 3.20e+06, 4.80e+06, 8.00e+06, 1.60e+07]
	;tegrid=[4.50e+03, 9.00e+03, 1.80e+04, 2.70e+04, 4.50e+04, 9.00e+04, 1.35e+05, 1.80e+05, 2.70e+05, 4.50e+05, 9.00e+05, 1.35e+06, 1.80e+06, 4.50e+06]
	

	xegrid_out=[	1.001d0, $
                        1.002d0, $
                        1.003d0, $
                        1.005d0, $
                        1.007d0, $
                        1.01d0, $ 
                        1.02d0, $ 
                        1.03d0, $ 
                        1.05d0, $ 
                        1.07d0, $ 
                        1.1d0, $ 
                        1.2d0, $ 
                        1.3d0, $ 
                        1.5d0, $ 
                        1.7d0, $ 
                        2.0d0, $ 
                        2.5d0, $ 
                        3.0d0, $ 
                        5.0d0, $ 
                        7.0d0, $ 
                       10.0d0, $ 
                       15.0d0, $ 
                       20.0d0, $ 
                       50.0d0, $ 
                       70.0d0, $ 
                      100.0d0, $ 
                      150.0d0, $ 
                      200.0d0, $ 
                      500.0d0, $ 
                      700.0d0, $ 
                     1000.0d0 ]

	xegrid_print=[0,where(xegrid_out ge 1.01)]

	if iz gt 1 then begin
		xegrid_calc=xegrid_out + (3d0 / (1d0 + xegrid_out))
		; Cowan equation 18.160
	endif else begin
		xegrid_calc=xegrid_print
	endelse


     	fii   = 80
     	zeta  = 90
     	fij   = 80
     	gij   = 80
     	rij   = 80

     	if iz0 EQ 1 then begin
        	fii   = 75
		zeta  = 95
        	fij   = 75
        	gij   = 75
        	rij   = 60
     	endif
     
     	test = iz0-iz+1
     
     	if test GE 5 AND test LE 10 then begin
        	fii   = 80
        	zeta  = 99
        	fij   = 85
        	gij   = 85
        	rij   = 85
     	endif   
  
     	if test GT 10 then begin
        	fii   = 93
        	zeta  = 99
        	fij   = 93
        	gij   = 93
        	rij   = 93
     	endif   

	scale=string(fii,zeta,fij,gij,rij, format='(5i2)')
	
	for i=0,(size(occup))[2]-1 do begin
		stat_wt[i]=1
		for j=0,(size(occup))[1]-1 do begin
			stat_wt[i]=stat_wt[i]*factorial(4*shl_lval[j]+2)/$
				factorial(occup[j,i])/$
				factorial(4*shl_lval[j]+2-occup[j,i])
		endfor
	end

       	name='rcn1_input_unsorted'
       	openw,lun,name, /get_lun
       	printf,lun,'2  -5    2   10  1.0    5.d-09    5.d-11-2  0130    1.0 0.65  0.0  0.5'
       
	for i=0,(size(occup))[2]-1 do begin
		line = "   "+string(iz0,format='(I2)') +"   "+ string(iz+1,format='(I2)') + " xx cf"+string(i,format='(I2)')+"              "
                line=line+occ2cow(reform(occup[*,i]))
        	printf,lun,line
        end
        printf,lun,"   -1"
              
  	free_lun,lun

	hx34sr,'rcn1_input_unsorted','rcn1_input'
	
	openw,lun,'rcn1_driver',/get_lun
        printf,lun,1
        printf,lun,"rcn1_input"
	printf,lun,"rcn1_out"
	printf,lun,"d2"
        free_lun,lun

	spawn, adasbin + 'rcn.x < rcn1_driver'


	dummy="string"
        openr,lun,'rcn1_out',/get_lun
	while (not eof(lun)) do begin
	        readf,lun,dummy
                pos=strpos(dummy,"xx cf")
                if pos ne -1 then begin
                	pos=strpos(dummy,"nconf")
                        if pos eq -1 then begin
                                energy_temp=0.00d0
                                idx=-1
                                reads,dummy,idx,energy_temp,format='(7X,I2,16X,F12.4)'
                                energy[idx]=energy_temp
                        endif
                endif
	end

	free_lun,lun

	esort=sort(energy)

	for i=0,(size(conf))[1]-1 do begin
                for j=0,(size(occup))[1]-1 do begin
	                if occup[j,i] gt 0 then begin
				conf[i]=conf[i] + strtrim(string( (conf[i] eq "" ? 0 : 50)  +occup[j,i]),2) + eiss_lab[j]  
			endif
		end       
	end

	esym=xxesym(iz0)
	if strlen(esym) eq 1 then esym=esym+' '

	printf,type1,esym,'+',iz,iz0,iz+1,ionpot * 8065.54445,format='(A2,A1,I2,8X,I2,8X,I2,5X,F15.1)'
	printf,type3,esym,'+',iz,iz0,iz+1,ionpot * 8065.54445,format='(A2,A1,I2,8X,I2,8X,I2,4X,F12.1)'

	conflen=max(strlen(conf)) > 19

	for i=0,(size(conf))[1]-1 do begin
		if strlen(conf[i]) lt conflen then begin
			conf[i]=string(conf[i],' ',format='(A'+strtrim(string(strlen(conf[i])),2)+','+strtrim(string(conflen-strlen(conf[i])-1),2)+'X,A1)')
		endif
	end
	
	for i=0,(size(esort))[1]-1 do begin
		printf,type1,i+1,conf[esort[i]],"(0)0(",(stat_wt[esort[i]] -1d0 ) / 2d0,")",(energy[esort[i]]-energy[esort[0]]) * 109737.26d0,format='(I5,1X,A'+strtrim(string(conflen),2)+',3X,A5,F8.1,A1,F15.1)'
		printf,type3,i+1,conf[esort[i]],"(0)0(",(stat_wt[esort[i]] -1d0 ) / 2d0,")",(energy[esort[i]]-energy[esort[0]]) * 109737.26d0,format='(I5,1X,A'+strtrim(string(conflen),2)+',3X,A5,F8.1,A1,F15.1)'
	end
		
	orbenergies=dblarr(1)
	;orbenergies=get_orbital('rcn1_out') ; ADW!
	
	cmd = adasbin + 'get_orbital.x < '+ 'rcn1_out'
	spawn, cmd, res, /sh
	orbenergies = res[0]

	
	
	if (where(orbenergies ne 0.0))[0] ne -1 then begin
		printf,type1,orbenergies
		printf,type3,orbenergies
	endif else begin
		printf,type1,-1,format='(I5)'
		printf,type3,-1,format='(I5)'
	endelse
	
	fmt = '(F5.1,I5,8X,50e9.2)'
	
	str = string(iz+1, 1, xegrid_out[xegrid_print],format=fmt)
	b   = byte(str)
	i1  = where(b EQ 101b, complement=i2)              
	str = string(b[i2])
	printf, type1, str

	str = string(iz+1, 3, tegrid[*],format=fmt)
	b   = byte(str)
	i1  = where(b EQ 101b, complement=i2)              
	str = string(b[i2])
	printf, type3, str

	openw,lun,'rcn2_input_d',/get_lun
        	printf,lun,'g5inp     000 0.00000         00                 3'+scale+'      0111 1020'
        	printf,lun,'       -1'
	free_lun,lun
        
        openw,lun,'rcn2_driver_d',/get_lun
        	printf,lun,1
	        printf,lun,'rcn2_input_d'
        	printf,lun,'d2'
	        printf,lun,'rcn2_output_d'
        	printf,lun,'d_d'
	free_lun,lun

	spawn, adasbin + 'rcn2.x < rcn2_driver_d'
        
	openr,lun,'rcn2_output_d',/get_lun
	dummy="string"
        
	rad_idx1=intarr(1)
	rad_idx2=intarr(1)
	rad_avalue=dblarr(1)
        
        
        while (not eof(lun)) do begin
	        readf,lun,dummy
                pos=strpos(dummy,"xx cf")
                if pos ne -1 then begin
                	pos=strpos(dummy,"a0")
                        if pos ne -1 then begin
                                mupole_temp=0.0
                                idx1=0
                                idx2=0
                                type='??'
				reads,dummy,idx1,idx2,mupole_temp,type,format='(25X,I2,18X,I2,11X,F11.6,9X,A2)'
                                if (idx1 ne idx2 and mupole_temp ne 0.0) then begin
	                                ediff=energy[idx1] - energy[idx2]
                                	if ediff lt 0 then begin
                                        	stat_upper=stat_wt[idx2]
                                        	stat_lower=stat_wt[idx1]
						diff=occup[*,idx1] - occup[*,idx2]						
						q1=occup[(where(diff gt 0)),idx1] 
						q2=occup[(where(diff lt 0)),idx2] 
						l1=shl_lval[(where(diff gt 0))]
						l2=shl_lval[(where(diff lt 0))]
                                        	ediff=-ediff
                                        endif else begin
                                         	stat_upper=stat_wt[idx1]
                                        	stat_lower=stat_wt[idx2]
						diff=occup[*,idx2] - occup[*,idx1]						
						q1=occup[(where(diff lt 0)),idx1] 
						q2=occup[(where(diff gt 0)),idx2] 
						l1=shl_lval[(where(diff lt 0))]
						l2=shl_lval[(where(diff gt 0))]
                                        endelse
                                        
                                        
                                        
                                        if type eq 'r1' then begin
						
                                                avalue = 2.0261d-6 * (ediff * 109737.26d0)^3d0 * mupole_temp^2d0; / stat_upper
                                                ; cowan equation 14.33
                                        
                                        	factor=2d0 * q2 * (4d0 * l1 + 3 - q1) / ((4d0 * l1 + 2d0) * (4d0 * l2 + 2d0)) * max([l1,l2])
						; Pindzola equation 14 corrected by Nigel
						
						avalue = avalue * factor
					
						print,mupole_temp,ediff*109737.26d0,stat_upper,stat_lower,avalue," ",conf[idx1]," ",conf[idx2]
					endif
                                        
                                        if type eq 'r2' then begin
                                        
                                                avalue = 1.1199e-22 * (ediff * 109737.26d0)^5d0 * mupole_temp^2d0; / stat_upper
						; cowan equation 15.13
						
						factor=2d0 * q2 * (4d0 * l1 + 3 - q1) / ((4d0 * l1 + 2d0) * (4d0 * l2 + 2d0)) * max([l1,l2]) * (max([l1,l2]) + 1d0 ) / (4d0 * max([l1,l2]) + 1)
						; Expression from Nigel
						
						avalue = avalue * factor
						
                                        
                                        endif
					
                                        ;avalue = avalue * 2d0	; ADW!
					;avalue=avalue * factor
					
					
					;if (idx1 eq 0 and idx2 eq 2) then begin
					;	print,mupole_temp,factor,ediff
					;endif
					
					;print,idx1,idx2,q1,q2,l1,l2,factor,format='(2I3,5F10.3)'
                                        rad_idx1=[rad_idx1,idx1]
                                        rad_idx2=[rad_idx2,idx2]
                                        rad_avalue=[rad_avalue,avalue]
                                        
                                endif
                        endif
                endif
	end

    	rad_idx1=rad_idx1[1:(size(rad_idx1))[1]-1]
	rad_idx2=rad_idx2[1:(size(rad_idx2))[1]-1]
        rad_avalue=rad_avalue[1:(size(rad_avalue))[1]-1]

	free_lun,lun
        
	openw,lun,'rcn2_input_j',/get_lun
        	printf,lun,'g5inp     000 0.00000         0050211            3'+scale+'      0111 1020'
        	printf,lun,'       -1'
	free_lun,lun
        
        openw,lun,'rcn2_driver_j',/get_lun
        	printf,lun,1
	        printf,lun,'rcn2_input_j'
        	printf,lun,'d2'
	        printf,lun,'rcn2_output_j'
        	printf,lun,'d_j'
	free_lun,lun

	spawn, adasbin + 'rcn2.x < rcn2_driver_j'

	openr,lun,'rcn2_output_j',/get_lun
	dummy="string"


readagain:
	readf,lun,dummy,format='(A50)'		
	if strpos(dummy,'nbigks') ne -1 then nbigks=fix(strmid(dummy,strpos(dummy,'nbigks')+7))
	if strtrim(dummy,2) ne 'k-values' then goto,readagain
	readf,lun,dummy
	kvalues=dblarr(nbigks)
	readf,lun,kvalues,format='(8F14.7)'

	bessel_idx1=intarr(1)
	bessel_idx2=intarr(1)
        bessel_type=strarr(1)
        bessel_mpol=dblarr(1)
        bessel_kidx=intarr(1)
	kidx=0

        while (not eof(lun)) do begin
	        readf,lun,dummy
                pos=strpos(dummy,"//j")
                if pos ne -1 then begin
			mupole_temp=0.0
			idx1=0
			idx2=0
			type='??'
                        ok='?'
			reads,dummy,idx1,idx2,mupole_temp,ok,type,format='(25X,I2,18X,I2,11X,F11.6,6X,A1,2X,A2)'
                        if (idx1 ne idx2 and ok ne ' ') then begin
                                if (kidx eq nbigks) then begin
                                	kidx=0
				endif
                                mupole_temp=abs(mupole_temp)
				bessel_idx1=[bessel_idx1,idx1]
				bessel_idx2=[bessel_idx2,idx2]
				bessel_type=[bessel_type,type]
				bessel_mpol=[bessel_mpol,mupole_temp]
				bessel_kidx=[bessel_kidx,kidx]                                       
				kidx=kidx+1
			endif
                endif
	end
	
        free_lun,lun
        
    	bessel_idx1=bessel_idx1[1:(size(bessel_idx1))[1]-1]
	bessel_idx2=bessel_idx2[1:(size(bessel_idx2))[1]-1]
        bessel_type=bessel_type[1:(size(bessel_type))[1]-1]
        bessel_mpol=bessel_mpol[1:(size(bessel_mpol))[1]-1]
        bessel_kidx=bessel_kidx[1:(size(bessel_kidx))[1]-1]
        
        for i=0,(size(occup))[2]-1 do begin
        	for j=0,(size(occup))[2]-1 do begin
           		if i ne j then begin
                         endif
                end
	end

	printed=intarr((size(occup))[2],(size(occup))[2])

        for i=0,(size(occup))[2]-1 do begin
        	for j=0,(size(occup))[2]-1 do begin
			if i ne j and printed[i,j] eq 0 and printed[j,i] eq 0  then begin
				avalue=1e-30
				
				; do we have an a-value?
                        	dataidx=where(rad_idx1 eq i and rad_idx2 eq j)
	                        if (dataidx[0] ne -1) then begin
                        		;print,'a-value for ',strtrim(string(i),2),'-',strtrim(string(j),2),' = ',rad_avalue[dataidx[0]]
                      			avalue=rad_avalue[dataidx[0]]
				endif

				collstr=dblarr((size(xegrid_calc))[1])
				effcollstr=dblarr((size(tegrid))[1])
				collstr[*]=1e-40
				effcollstr[*]=1e-40

				; do we have bessel whatsits
                        	dataidx=where(bessel_idx1 eq i and bessel_idx2 eq j)
	                        if (dataidx[0] ne -1) then begin
					bessel_total=dblarr(nbigks)
					temp_mpol=bessel_mpol[dataidx]
					temp_kidx=bessel_kidx[dataidx]
                       			for k=0,nbigks-1 do begin
                                         	bessel_total[k]=total((temp_mpol[where(temp_kidx eq k)])^2d0)
					end
                                        
					; Cowan equation 18.145ish
					 
                                	;print,'bessel data found for ',strtrim(string(i),2),'-',strtrim(string(j),2),' = ',bessel_total
                      		
					ediff=energy[i] - energy[j]
                                	if ediff lt 0 then begin
                                        	stat_upper=stat_wt[j]
                                        	stat_lower=stat_wt[i]
						energy_upper=(energy[j]-energy[esort[0]]) * 109737.26d0
						energy_lower=(energy[i]-energy[esort[0]]) * 109737.26d0
						q1=occup[(where(diff gt 0)),idx1] 
						q2=occup[(where(diff lt 0)),idx2] 
						l1=shl_lval[(where(diff gt 0))]
						l2=shl_lval[(where(diff lt 0))]
                                        	ediff=-ediff
                                        endif else begin
                                         	stat_upper=stat_wt[i]
                                        	stat_lower=stat_wt[j]
						energy_upper=(energy[i]-energy[esort[0]]) * 109737.26d0
						energy_lower=(energy[j]-energy[esort[0]]) * 109737.26d0
						q1=occup[(where(diff lt 0)),idx1] 
						q2=occup[(where(diff gt 0)),idx2] 
						l1=shl_lval[(where(diff lt 0))]
						l2=shl_lval[(where(diff gt 0))]
                                        endelse
						
					egrid=xegrid_calc*ediff

                                        for k=0,(size(egrid))[1]-1 do begin
						kmin=egrid[k]^(1./2.)-(egrid[k]-ediff)^(1./2.)
	        				kmax=egrid[k]^(1./2.)+(egrid[k]-ediff)^(1./2.)
						; Cowan equation 18.158
						
						npoints=51
						
						x_int = alog(kmin) + dindgen(npoints)/float(npoints-1) * (alog(kmax)-alog(kmin))
						x_int = exp(x_int)
						
						xxsple,xin=kvalues,yin=bessel_total,xout=x_int,yout=y_int
						
						total=y_int[0]+y_int[npoints-1]
						for l=1,npoints-2 do begin
							total=total+(3+(-1d0)^(l-1))*y_int[l]
						end
						
						total = total * (alog(x_int[1])-alog(x_int[0])) / 3d0
						; Cowan equation 18.157 (Corrected)
						
						collstr[k]=8d0 * total
						
                                        end
					
					
					
					
					collstr = collstr * 2; ADW!
					
					type=2
					if (where(bessel_type[dataidx] eq 'j1'))[0] ne -1 then type=1
					
					
					; not sure if the below is correct, need to check!
					
					if type eq 1 then begin
						factor=2d0 * q2 * (4d0 * l1 + 3 - q1) / ((4d0 * l1 + 2d0) * (4d0 * l2 + 2d0)) * max([l1,l2])
						; assume factor same as for A-value
					endif else begin
						factor=2d0 * q2 * (4d0 * l1 + 3 - q1) / ((4d0 * l1 + 2d0) * (4d0 * l2 + 2d0)) * max([l1,l2]) * (max([l1,l2]) + 1d0 ) / (4d0 * max([l1,l2]) + 1)
						; assume factor same as for A-value
					endelse
					
					;print,stat_upper,stat_lower
										
					h4mxwl,z0=iz0,iz=iz,effz=float(iz+1),type=type,aval=avalue,xju=stat_upper,xjl=stat_lower, $
					enu=energy_upper,enl=energy_lower,xparam=xegrid_out,omega=collstr,te=tegrid,upsilon=effcollstr
        			endif
				
				collstr = collstr > 1e-30
				effcollstr = effcollstr > 1e-30
								
				indx_1=(where(esort eq i))[0]+1
				indx_2=(where(esort eq j))[0]+1

				
				if indx_1 lt indx_2 then begin
					temp=indx_2
					indx_2=indx_1
					indx_1=temp
				endif
				fmt = '(2i5,50e9.2)'
				if avalue ne 1e-30 or (where(collstr ne 1e-30))[0] ne -1 then begin
					str = string(indx_1, indx_2, avalue, collstr[xegrid_print],format=fmt)
   					b   = byte(str)
					i1  = where(b EQ 101b, complement=i2)              
   					str = string(b[i2])
   					printf, type1, str
					printed[i,j]=1
				endif
				
				if avalue ne 1e-30 or (where(effcollstr ne 1e-30))[0] ne -1 then begin
					str = string(indx_1, indx_2, avalue, effcollstr[*],format=fmt)
   					b   = byte(str)
					i1  = where(b EQ 101b, complement=i2)              
   					str = string(b[i2])
   					printf, type3, str
					printed[i,j]=1
				endif
                        endif
                end
	end

	printf,type1,-1,format='(I5)'
	printf,type3,-1,format='(I5)'
	printf,type1,-1,-1,format='(2I5)'
	printf,type3,-1,-1,format='(2I5)'

	free_lun,type1
	free_lun,type3
	
	if not keyword_set(keeppass) then begin
		file_delete,'rcn1_input_unsorted','rcn1_input','rcn1_driver', $
			    'rcn2_input_d','rcn2_input_j','rcn2_driver_d', $
			    'rcn2_driver_j','rcn2_output_d','rcn2_output_j', $
			    'd_j','d_d','d2','rcn1_out',/quiet
	end
end
