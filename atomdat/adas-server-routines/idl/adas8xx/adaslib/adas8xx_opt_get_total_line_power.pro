;-----------------------------------------------------------------------------
;+
;
; NAME     : adas8xx_opt_get_total_line_power
;
; PURPOSE  : to calculate the line emitted power from an 
;            adas8xx_opt_promotions_run_ca run. Will return adf11 plt 
;            and a sum of the adf40 power for comparison.
;            
;
; CATEGORY : ADAS
;
; NOTES    : Code always calculated the power from the first
;            wavelength block in the plasma structure, and the 
;            first Te and Ne points. 
;  
; USE      : 
;
; INPUT    :
; (I*4) z0_nuc            = Ion nuclear charge
; (I*4) nel           = number of electrons
; (I*4) z_ion            = z0_nuc-nel
; (I*4) ionpot        = ionisation potential (cm-1)
; (I*4) for_tr_sel    = Cowan option for radiative transitions,
;                       default=3
; (STRUCT) promotion_results
;   (C* ) grd_cfg       = ground configuration string
;   (C* ) ex_cfg        = excited state configuration strings (array)
;   (I*4) grd_par       = parity of ground state
;   (I*4) ex_par        = parity of excited states
;   (I*4) grd_zc        = ground effective charage for adf34 driver
;   (I*4) ex_zc         = excited effective charage for adf34 driver
;   (I*4) oc_store      = configuration occupancies
; (STRUCT) plasma
;   (R*4) wvl_range     = wavelength range for adf40 generation
;   (I*4) te_ref        = electron temperature for ADAS810 run
;   (I*4) ne_ref        = electron density for ADAS810 run
;   (I*4) year          = year id for data produced (default=40)
;
; OUTPUTS   :
; struct fpecdata       = returned data structure, containing:
;   (R*8) a11_total_power = total line power from plt file
;   (R*8) a40_total_power = total line power from adf40 power
;   (R*8)() fpec          = Feature photon emissivity coefficient
;   (R*8)() wave          = wavelengths for each pixel of fpec
;   (I*4) exit_status    = negative on error
;
; CALLS:
;       adas8xx_opt_promotions_run_ca
;       adas_vector
;       xxdata_11
;       xxesym
;       xxelem
; CATEGORY:
;       Adas system.
;
; WRITTEN:
;       Adam Foster, University of Strathclyde,08-01-08
;
; MODIFIED:
;       1.1     Adam Foster
;               - Original Version
;
;       1.2     Adam Foster
;               - change z1 to z_ion and z0 to z0_nuc
;
; VERSION:
;       1.1   08-01-08
; 
;-
;-----------------------------------------------------------------------------


PRO adas8xx_opt_get_total_line_power,  z0_nuc        = z0_nuc,         $
                                       nel           = nel,            $
                                       z_ion         = z_ion,          $
                                       ionpot        = ionpot,         $
                                       for_tr_sel    = for_tr_sel,     $
                                   promotion_results=promotion_results,$
                                       plasma        = plasma,         $
                                       fpecdata      = fpecdata,       $
                                       year          = year,           $
                                cowan_scale_factors=cowan_scale_factors,$
                                       lun_verb      = lun_verb


; take the given inputs and run adas801/810 (via adas8xx_opt_promotions_run_ca)

adas8xx_opt_promotions_run_ca,         z0_nuc        = z0_nuc,         $
                                       nel           = nel,            $
                                       z_ion         = z_ion,          $
                                       ionpot        = ionpot,         $
                                       for_tr_sel    = for_tr_sel,     $
                                   promotion_results=promotion_results,$
                                       exit_status   = exit_status,    $
                                       plasma        = plasma,         $
                               cowan_scale_factors=cowan_scale_factors,$
                                       files         = files,          $
                                       year          = year,           $
                                       lun_verb      = lun_verb

                                       


if exit_status LT 0 THEN BEGIN

  if keyword_set(lun_verb) then begin
    printf, lun_verb, 'Error in running ca Cowan, invalid configuration'
  endif
  
  fd={fpec:dblarr(plasma.npix[plasma.indx_wvl[0]]), $
      wave_max:plasma.wvlmin[plasma.indx_wvl[0]], wave_min:plasma.wvlmin[plasma.indx_wvl[0]], $
      npix:plasma.npix[plasma.indx_wvl[0]]}
  a11_total_power= 0.0
  a40_total_power= 0.0
  wave=adas_vector(high=fd.wave_max[0], low=fd.wave_min[0], $
                 num=fd.npix[0], /linear)

  total_power=TOTAL(fd.fpec*(1.9806e-15)/wave)
  fpecdata={a11_total_power: a11_total_power, $
            a40_total_power: a40_total_power, $
            fpec:fd.fpec, wave: wave, $
            exit_status:exit_status}

  return
ENDIF


; now need to open adf40 file and read in the fpec:

; define some useful strings
SPAWN, 'pwd',wd
esymb=strlowcase(xxesym(z0_nuc))
xxelem, z0_nuc, ename
coupling='ca'
nelstring=STRCOMPRESS(string(z0_nuc-nel),/REMOVE_ALL)
z0string=STRCOMPRESS(string(z0_nuc),/REMOVE_ALL)

; get adf40 and adf11 file names and directory structure

p0=0
p1=strpos(files.adf40_ca_file,'_',p0)
archive_adf40_dir=wd + '/' +                            $
                  strmid(files.adf40_ca_file,p0,p1-p0)           

p0=p1+1
p1=strpos(files.adf40_ca_file,'_',p0)
archive_adf40_element_dir=archive_adf40_dir + '/' +              $
                  strmid(files.adf40_ca_file,p0,p1-p0)           

p0=p1+1
                                        

a40file=archive_adf40_element_dir +       $
                      '/' + strmid(files.adf40_ca_file,p0)
;-----------------------------------------------------------------------

p0=0
p1=strpos(files.adf11_ca_file,'_',p0)
archive_adf11_dir=wd + '/' +                            $
                  strmid(files.adf11_ca_file,p0,p1-p0)           

p0=p1+1
p1=strpos(files.adf11_ca_file,'_',p0)
p2=p1+1
p2=strpos(files.adf11_ca_file,'_',p2)

archive_adf11_partial_dir=archive_adf11_dir + '/' +              $
                  strmid(files.adf11_ca_file,p0,p2-p0)

p0=p2+1
p1=strpos(files.adf11_ca_file,'_',p0)
p2=p1+1
p2=strpos(files.adf11_ca_file,'_',p2)
p2=p2+1
p2=strpos(files.adf11_ca_file,'_',p2)

archive_adf11_element_dir=archive_adf11_partial_dir + '/' +              $
                  strmid(files.adf11_ca_file,p0,p2-p0)           

p0=p2+1

                                         
a11file=archive_adf11_element_dir +       $
                       '/' + strmid(files.adf11_ca_file,p0)




;a40file=files.adf40_ca_file
;a40file=STRJOIN([wd,'adf40','fpec06#'+z0string,esymb+'_01',$
;                 'ca#'+esymb+nelstring+'.dat'],'/')

if keyword_set(lun_verb) then begin
  printf, lun_verb, a40file
endif
tmp=file_test(a40file)
if tmp EQ 1 THEN BEGIN
  
  if keyword_set(lun_verb) then begin
    printf, lun_verb, 'adf40 file exists, this is good!' 
  endif

  read_adf40, file      = a40file,     $
              fulldata  = fd
endif else begin
  if keyword_set(lun_verb) then begin
    printf, lun_verb, 'adf40 file does not exist, this is bad!'
  endif
  fd={fpec:dblarr(plasma.npix[plasma.indx_wvl[0]]), $
      wave_max:plasma.wvlmin[plasma.indx_wvl[0]], wave_min:plasma.wvlmin[plasma.indx_wvl[0]], $
      npix:plasma.npix[plasma.indx_wvl[0]]}
endelse

; now read in adf11 total power

tmp=file_test(a11file)
if tmp EQ 1 THEN BEGIN
  if keyword_set(lun_verb) then begin
    printf, lun_verb, 'adf11 file exists, this is good!' 
  endif

  xxdata_11, file      = a11file,     $
              fulldata  = a11fd
              
  a11_total_power= 10.0^(a11fd.drcof)
endif else begin
  if keyword_set(lun_verb) then begin
    printf, lun_verb, 'adf11 file does not exist, this is bad!'
  endif
  a11_total_power= 0.0
endelse

; convert FPEC to W cm3 and sum            
wave=adas_vector(high=fd.wave_max[0], low=fd.wave_min[0], $
               num=fd.npix[0], /linear)
;stop
a40_total_power=TOTAL(fd.fpec*(1.9806e-15)/wave)


; return both adf11 and adf40 data

; now remove the adf40 and adf11 files
;stop
cmd='rm -f '+a11file
spawn, cmd
cmd='rm -f '+a40file
spawn, cmd
fpecdata={a11_total_power: a11_total_power[0], $
          a40_total_power: a40_total_power, $
          fpec:fd.fpec, wave: wave, $
          exit_status:exit_status}

;stop

END
