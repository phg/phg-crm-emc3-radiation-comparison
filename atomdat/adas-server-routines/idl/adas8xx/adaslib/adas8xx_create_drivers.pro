;-----------------------------------------------------------------------------
;+
;
; NAME     : adas8xx_create_drivers
;
; PURPOSE  : create driver files adf34 and adf42 for running ADAS801
;            and ADAS810.
;
; INPUTS
;
; integer    z0_nuc     = nuclear charge of ion
; integer    z1         = ion charge (deprecated)
; integer    z_ion      = ion charge (preferred)
; float      ionpot     = ionisation potential (eV)
; integer    for_tr_sel = Cowan option for radiative transitions default=3
; integer    lun_verb   = logical unit number for outputting diagnostic
;                         information (if set)
; integer()  cowan_scale_factors = Slater parameter multipliers for Cowan
;                         code (optional, defaults are usually sufficient)
; integer()  par1_list  = restrict collision strength calculations to these
;                         configuration in the first parity. If not set, the 
;                         default behaviour is to include all configurations. 
;                         Note there is no check on whether the list is sensible.
; integer()  par2_list  = same for second parity.
;
; keyword    make_ic_adf34 = generate the IC adf34, but no other IC files
;                            (used for checking for convergence in RCN)
;
; keyword    ca_only       = Only make configuration averaged case
;
; structure  files  = list of filenames:
;                  adf04_ca_t1_file
;                  adf04_ca_t3_file
;                  adf04_ca_file
;                  adf34_file
;                  adf34_inst_file
;                  adf34_ls_pp_file
;                  adf34_ic_pp_file
;                  adf42_ls_file
;                  adf42_ic_file
;                  adf42_ls_pp_file
;                  adf42_ic_pp_file
;                  adf04_ls_file
;                  adf04_ic_file
;                  adf42_ca_pp_file
;                  adf42_ca_file
;                  adf15_ls_file
;                  adf15_ic_file
;                  adf40_ls_file
;                  adf40_ic_file
;                  adf11_ls_file
;                  adf11_ic_file
;                  adf15_ca_file
;                  adf40_ca_file
;                  adf11_ca_file
;
;              note the ls & ic files can be omitted if ca_only is set.
;
; structure plasma = plasma conditions:
;                  THETA        : array of electron temperatures (eV)
;                  INDX_THETA   : array of indices of theta to select Te
;                  RHO          : array of electron density (cm-3)
;                  INDX_RHO     : array of indices of rho to select Ne
;                  NPIX         : array of pixels per wavelength region
;                  WVLMIN       : lower bounds of wavelength region(s)
;                  WVLMAX       : upper bounds of wavelength region(s)
;                  INDX_WVL     : indicies of wavelength regions to use
;                  THETA_NOSCALE: if 0, theta is assumed to be reduced Te
;                  RHO_SCALE    : if 1, rho is assumed to be reduced Ne
;
; structure promotion_results = details of configurations to include:
;                  grd_cfg    = ground configuration
;                  grd_occ    = ground occupancy array
;                  ex_cfg     = excited state configurations
;                  grd_par    = ground state parity
;                  ex_par     = excited state parity
;                  grd_zc_cow = ground state charge for cowan code
;                  ex_zc_cow  = excited state charge for cowan code
;                  oc_store   = excited state occupancy arrays
;                  no_configs = number of configurations in configuration set
;                  no_terms   = number of terms in configuration set
;                  no_levels  = number of levels in configuration set
;
;
; WRITTEN:
;       Adam Foster
;
; MODIFIED:
;       1.1     Adam Foster
;                - First commented version
;                - moved file names into files structure
;                - moved plasma conditions into plasma structure
;                - Added support for z0_nuc and z_ion inputs instead
;                  of z0, z1.
;
;       1.2     Martin O'Mullane
;                - Option to restrict the number of configurations in the
;                  output IC and LS file.
;
;       1.3     Hugh Summers
;                - Corrected error in parity_2 index list in adf34  _inst
;                  and _pp datasets.
;
;       1.4     Stuart Henderson
;                - Redefine COWAN scaling factors to align with ADAS801.
;                - Set Ti=Th=Te for adf42 driver file to avoid producing 
;                  NaNs in the adf40 file.
;
;       1.5     Martin O'Mullane
;                - Merge in 1.3 and 1.4 from Hugh and Stuart svn development.
;                - Change configuration labels to conform to rcn input
;                  specification.
;
; VERSION:
;       1.1   20-03-2009
;       1.2   09-01-2012
;       1.3   29-06-2015
;       1.4   21-07-2015
;       1.5   17-12-2018
;
;-
;-----------------------------------------------------------------------------
PRO adas8xx_create_drivers, z0_nuc              =  z0_nuc,               $
                            z1                  =  z1,                   $
                            z_ion               =  z_ion,                $
                            ionpot              =  ionpot,               $
                            files               =  files,                $
                            plasma              =  plasma,               $
                            for_tr_sel          =  for_tr_sel,           $
                            promotion_results   =  promotion_results,    $
                            ca_only             =  ca_only,              $
                            cowan_scale_factors =  cowan_scale_factors,  $
                            par1_list           =  par1_list,            $
                            par2_list           =  par2_list,            $
                            make_ic_adf34       =  make_ic_adf34,        $
                            lun_verb            =  lun_verb



;-------------
; Set defaults
;-------------

today = xxdate()

if n_elements(for_tr_sel) EQ 0 then f_tran = 3 else f_tran = for_tr_sel

If not keyword_set(ca_only) then ca_only=0

 elsymb     = strlowcase(xxesym(z0_nuc))
 plevel     = '01'

;----------------------------
; check for z1, z_ion keyword
;----------------------------

IF n_elements(z1) EQ 0 THEN BEGIN
  IF n_elements(z_ion) EQ 0 THEN BEGIN
    print, 'ADAS8xx_CREATE_DRIVERS error: must specify ion charge using z1 '+$
           'or z_ion (z_ion preferred)'
    STOP
  ENDIF ELSE BEGIN
   ; assign z1 = z_ion, as z1 is used in this code
    z1=z_ion
  ENDELSE
ENDIF ELSE BEGIN
  IF n_elements(z_ion) NE 0 THEN BEGIN
    IF z_ion NE z1 THEN BEGIN
      print,STRCOMPRESS('ADAS8XX_CREATE_DRIVERS error: both z1 and z_ion '+$
                        'have been specified and their values do not match '+$
                        '(z1='+string(z1)+', z_ion='+string(z_ion)+')')
    ENDIF
  ENDIF ELSE BEGIN
    z_ion = z1
  ENDELSE
ENDELSE


;-----------------------------------------------------------------------
; Extract relevant data from plasma structure.
;-----------------------------------------------------------------------

if not keyword_set(plasma) then begin
  print, 'ADAS8XX_CREATE_DRIVERS warning: plasma conditions undefined: using defaults'
  plasma = adas8xx_plasma_defaults(/all)
endif

theta         = plasma.theta
indx_theta    = plasma.indx_theta
rho           = plasma.rho
indx_rho      = plasma.indx_rho
npix          = plasma.npix
wvlmin        = plasma.wvlmin
wvlmax        = plasma.wvlmax
indx_wvl      = plasma.indx_wvl
theta_noscale = plasma.theta_noscale
rho_scale     = plasma.rho_scale

;-----------------------------------------------------------------------
; Code expects scaled Te [theta], unscaled Ne [rho]
; Convert if required
;-----------------------------------------------------------------------

if theta_noscale EQ 1 then theta=theta/((z_ion+1d0)^2)
if rho_scale EQ 1 then rho=rho*((z_ion+1d0)^7)


if ca_only eq 0 or keyword_set(make_ic_adf34) then begin

      ;---------------------------
      ; Write the adf34 input file
      ;---------------------------

      ind_p1 = where(promotion_results.ex_par EQ promotion_results.grd_par, $
                     n_par1, complement=ind_p2, ncomplement=n_par2)

      top_line = '2  -5    2   10  1.0    5.d-09    5.d-11-2  0130    1.0 0.65  0.0  0.5'



      z0_str  = string(z0_nuc, format='(i5)')
      tmp    = '  '
      strput, tmp, xxesym(z0_nuc)
      z_str  = z0_str  + string(promotion_results.grd_zc_cow, format='(i5)') + '   ' + tmp

      g_line = z_str + ' gnd z1=' + string(z1, format='(i2)') +            $
                       '  ' + string(promotion_results.grd_par, format='(i1)') +$
                       '     ' + promotion_results.grd_cfg
      a34    = [top_line, g_line]

      k = 1
      for j = 0, n_par1-1 do begin
          if promotion_results.ex_cfg[ind_p1[j]] NE '' then begin
             z_str  = z0_str  + string(promotion_results.ex_zc_cow[ind_p1[j]], format='(i5)') + '   ' + tmp
             a34 = [a34, z_str + ' cfg ' + string(k, format='(i2.2)') + '     ' + $
                                 string(promotion_results.ex_par[ind_p1[j]], format='(i1)')         + $
                                 '     ' + promotion_results.ex_cfg[ind_p1[j]]]
             k = k + 1
          endif
      endfor
      for j = 0, n_par2-1 do begin
          if promotion_results.ex_cfg[ind_p2[j]] NE '' then begin
             z_str  = z0_str  + string(promotion_results.ex_zc_cow[ind_p2[j]], format='(i5)') + '   ' + tmp
             a34 = [a34, z_str + ' cfg ' + string(k, format='(i2.2)') + '     ' + $
                                 string(promotion_results.ex_par[ind_p2[j]], format='(i1)')         + $
                                 '     ' + promotion_results.ex_cfg[ind_p2[j]]]
             k = k + 1
          endif
      endfor

      a34 = [a34, '   -1']

      openw, lun, files.adf34_file, /get_lun
      for j = 0, n_elements(a34)-1 do printf, lun, a34[j]
      free_lun, lun


endif

IF ca_only EQ 0 AND (where(strupcase(tag_names(files)) EQ $
                           'ADF34_INST_FILE'))[0] NE -1 THEN BEGIN

      ;---------------------------
      ; Write the instruction file
      ;---------------------------

      ; use same parameters as recommended by Cowan and set in adas801.

      scale = '80 90 80 80 80'
      if z0_nuc eq 1 then scale = '75 95 75 75 60'
      if (z0_nuc-(z1+1)) ge 5 and (z0_nuc-(z1+1)) le 10 then scale = '80 99 85 85 85'
      if (z0_nuc-(z1+1)) gt 10 then scale = '93 99 93 93 93'      
      
      if keyword_set(cowan_scale_factors) then begin
        scale= strjoin(string(cowan_scale_factors, FORMAT='(I2,X)'),' ')
        if keyword_set(lun_verb) then begin
          printf,lun_verb, 'ADAS8xx_CREATE_DRIVERS: using assigned scale factors!:',$
                 scale
        endif
      endif

      if n_elements(par1_list) EQ 0 then p1_list = indgen(n_par1+1) + 1 $
                                    else p1_list = par1_list
      if n_elements(par2_list) EQ 0 then p2_list = indgen(n_par2) + 1 $
                                    else p2_list = par2_list

      n1_list = n_elements(p1_list)
      n2_list = n_elements(p2_list)

      str_par_1  = 'parity-1 ' + string(n_par1+1, format='(i2)') + ' : ' + $
                                 string(n1_list, format='(i2)') +  ' ' + $
                                 string(p1_list, format='(20(i3,:))')

      str_par_2  = 'parity-2 ' + string(n_par2, format='(i2)') + ' : ' + $
                                 string(n2_list, format='(i2)') + ' ' + $
                                 string(p2_list, format='(20(i3,:))')

      openw, lun, files.adf34_inst_file, /get_lun

      printf, lun, 'z0 ' + string(z0_nuc, format='(i2)')
      printf, lun, 'zi ' + string(z1, format='(i2)')
      printf, lun, str_par_1
      printf, lun, str_par_2
      printf, lun, 'E2 ' + string(f_tran, format='(i1)')
      printf, lun, 'M1 ' + string(f_tran, format='(i1)')

      printf, lun, 'scale ' + scale

      printf, lun, 'temperature ' + string(n_elements(theta),format='(i2)')
      printf, lun, theta,format='(8e9.2)'

      free_lun, lun


      ;------------------------------------------
      ; Write the ifgpp post processing file - ls
      ;------------------------------------------

      IF (where(strupcase(tag_names(files)) EQ $
                           'ADF34_LS_PP_FILE'))[0] NE -1 THEN BEGIN
        openw, lun, files.adf34_ls_pp_file, /get_lun

        printf, lun, '1'
        printf, lun, xxuser()
        printf, lun, today[0]
        printf, lun, '5'
        printf, lun, 'C'
        printf, lun, 'C  Cowan plane wave Born method'
        printf, lun, 'C'
        printf, lun, 'C  Scale factors ' + scale
        printf, lun, 'C'

        line1 = ' &FILES ifgfile = ''./ifg#' + files.adf34_file + $
                ''' , outfile = ''' + files.adf04_ls_file + ''' &END '

        line2 = ' &OPTIONS  ip = ' +                          $
                            string(ionpot*8065.54445) + ',' + $
                            ' coupling = ''LS'' , ' +         $
                            ' aval = ''YES'' , '    +         $
                            ' isonuclear = ''YES'',' +        $
                            ' quantity = ''RATES'',' +        $
                            ' lweight = ''NO'' , ' +          $
                            ' comments = 2, ' +               $
                            ' numtemp = '+ string(n_elements(indx_theta),format='(i2)') + ' ,'+ $
                            ' &END '
        printf, lun, line1
        printf, lun, line2

        printf, lun, indx_theta + 1, format = '(14i3)'
        printf, lun, str_par_1
        printf, lun, str_par_2

        free_lun, lun

      ENDIF

      ;------------------------------------------
      ; Write the ifgpp post processing file - ic
      ;------------------------------------------

      IF (where(strupcase(tag_names(files)) EQ $
                           'ADF34_IC_PP_FILE'))[0] NE -1 THEN BEGIN
        openw, lun, files.adf34_ic_pp_file, /get_lun

        printf, lun, '1'
        printf, lun, xxuser()
        printf, lun, today[0]
        printf, lun, '5'
        printf, lun, 'C'
        printf, lun, 'C  Cowan plane wave Born method'
        printf, lun, 'C'
        printf, lun, 'C  Scale factors ' + scale
        printf, lun, 'C'

        line1 = ' &FILES ifgfile = ''./ifg#' + files.adf34_file + $
                ''' , outfile = ''' + files.adf04_ic_file + ''' &END '

        line2 = ' &OPTIONS  ip = ' +                          $
                            string(ionpot*8065.54445) + ',' + $
                            ' coupling = ''IC'' , ' +         $
                            ' aval = ''YES'' , '    +         $
                            ' isonuclear = ''YES'',' +        $
                            ' quantity = ''RATES'',' +        $
                            ' lweight = ''NO'' , ' +          $
                            ' comments = 2, ' +               $
                            ' numtemp = '+ string(n_elements(indx_theta),format='(i2)') + ' ,'+ $
                            ' &END '
        printf, lun, line1
        printf, lun, line2

        printf, lun, indx_theta + 1, format = '(14i3)'
        printf, lun, str_par_1
        printf, lun, str_par_2

        free_lun, lun

      ENDIF


      ;---------------------------------------------------
      ; Write the adf42 file - ls
      ;---------------------------------------------------

      IF (where(strupcase(tag_names(files)) EQ $
                           'ADF42_LS_FILE'))[0] NE -1 THEN BEGIN

        openw, lun, files.adf42_ls_file, /get_lun


        printf, lun, ' &FILES'
        printf, lun, '   dsn04  = ''' + files.adf04_ls_file + ''','
        printf, lun, '   dsn18  = ''NULL'','
        printf, lun, '   dsn35  = ''NULL'','
        printf, lun, '   dsn15  = ''' + files.adf15_ls_file + ''','
        printf, lun, '   dsn40  = ''' + files.adf40_ls_file + ''','
        printf, lun, '   dsn11  = ''' + files.adf11_ls_file + ''','
        printf, lun, '   dsn11f = ''NULL''
        printf, lun, ' &END
        printf, lun, ' '

        printf, lun, ' &ION
        printf, lun, '    element = ''' + xxesym(z0_nuc) + ''','
        printf, lun, '    z0      = ' + string(z0_nuc, format='(i3)') + ','
        printf, lun, '    z1      = ' + string(z1, format='(i3)') + ','
        printf, lun, '    ip      = ' + string(ionpot*8065.54445)
        printf, lun, ' &END
        printf, lun, ' '

        printf, lun, ' &META'
        printf, lun, '   lnorm    = .TRUE.,'
        printf, lun, '   nmet     = 1,'
        printf, lun, '   imetr(1) = 1'
        printf, lun, ' &END'
        printf, lun, ' '

        printf, lun, ' &PROCESS'
        printf, lun, '   liosel = .FALSE.,'
        printf, lun, '   lhsel  = .FALSE.,'
        printf, lun, '   lrsel  = .FALSE.,'
        printf, lun, '   lisel  = .FALSE.,'
        printf, lun, '   lnsel  = .FALSE.,'
        printf, lun, '   lpsel  = .FALSE.,'
        printf, lun, '   zeff   = 3.0'
        printf, lun, ' &END'
        printf, lun, ' '
        ;
        printf, lun, ' &OUTPUT'
        printf, lun, '   lmetr   = .FALSE.,'
        printf, lun, '   ltscl   = .FALSE.,'
        printf, lun, '   ldscl   = .FALSE.,'
        printf, lun, '   lbrdi   = .FALSE.,'
        printf, lun, '   amin    = 0.0,'
        printf, lun, '   numte   = ' + string(n_elements(indx_theta)) + ','
        printf, lun, '   numtion = ' + string(n_elements(indx_theta)) + ','
        printf, lun, '   numth   = ' + string(n_elements(indx_theta)) + ','
        printf, lun, '   numdens = ' + string(n_elements(indx_rho)) + ','
        printf, lun, '   numdion = 0,'
        printf, lun, '   numwvl  = ' + string(n_elements(indx_wvl))
        printf, lun, ' &END'
        printf, lun, '

        printf, lun, (z1+1.0)^2.0d0*theta[indx_theta]/11605.4, format='(5(e12.4,:))'
        printf, lun, ' '
        printf, lun, (z1+1.0)^2.0d0*theta[indx_theta]/11605.4, format='(5(e12.4,:))'
        printf, lun, ' '
        printf, lun, (z1+1.0)^2.0d0*theta[indx_theta]/11605.4, format='(5(e12.4,:))'
        printf, lun, ' '
        printf, lun, rho[indx_rho], format='(5(e12.4,:))'
        printf, lun, ' '
        for j = 0, n_elements(indx_wvl)-1 do printf, lun, npix[indx_wvl[j]], wvlmin[indx_wvl[j]], wvlmax[indx_wvl[j]]

        free_lun, lun
      ENDIF

      ;---------------------------------------------------
      ; Write the adf42 file - ic
      ;---------------------------------------------------

      IF (where(strupcase(tag_names(files)) EQ $
                           'ADF42_IC_FILE'))[0] NE -1 THEN BEGIN
        openw, lun, files.adf42_ic_file, /get_lun

        printf, lun, ' &FILES'
        printf, lun, '   dsn04  = ''' + files.adf04_ic_file + ''','
        printf, lun, '   dsn18  = ''NULL'','
        printf, lun, '   dsn35  = ''NULL'','
        printf, lun, '   dsn15  = ''' + files.adf15_ic_file + ''','
        printf, lun, '   dsn40  = ''' + files.adf40_ic_file + ''','
        printf, lun, '   dsn11  = ''' + files.adf11_ic_file + ''','
        printf, lun, '   dsn11f = ''NULL''
        printf, lun, ' &END
        printf, lun, ' '

        printf, lun, ' &ION
        printf, lun, '    element = ''' + xxesym(z0_nuc) + ''','
        printf, lun, '    z0      = ' + string(z0_nuc, format='(i3)') + ','
        printf, lun, '    z1      = ' + string(z1, format='(i3)') + ','
        printf, lun, '    ip      = ' + string(ionpot*8065.54445)
        printf, lun, ' &END
        printf, lun, ' '

        printf, lun, ' &META'
        printf, lun, '   lnorm    = .TRUE.,'
        printf, lun, '   nmet     = 1,'
        printf, lun, '   imetr(1) = 1'
        printf, lun, ' &END'
        printf, lun, ' '

        printf, lun, ' &PROCESS'
        printf, lun, '   liosel = .FALSE.,'
        printf, lun, '   lhsel  = .FALSE.,'
        printf, lun, '   lrsel  = .FALSE.,'
        printf, lun, '   lisel  = .FALSE.,'
        printf, lun, '   lnsel  = .FALSE.,'
        printf, lun, '   lpsel  = .FALSE.,'
        printf, lun, '   zeff   = 3.0'
        printf, lun, ' &END'
        printf, lun, ' '

        printf, lun, ' &OUTPUT'
        printf, lun, '   lmetr   = .FALSE.,'
        printf, lun, '   ltscl   = .FALSE.,'
        printf, lun, '   ldscl   = .FALSE.,'
        printf, lun, '   lbrdi   = .FALSE.,'
        printf, lun, '   amin    = 0.0,'
        printf, lun, '   numte   = ' + string(n_elements(indx_theta)) + ','
        printf, lun, '   numtion = ' + string(n_elements(indx_theta)) + ','
        printf, lun, '   numth   = ' + string(n_elements(indx_theta)) + ','
        printf, lun, '   numdens = ' + string(n_elements(indx_rho)) + ','
        printf, lun, '   numdion = 0,'
        printf, lun, '   numwvl  = ' + string(n_elements(indx_wvl))
        printf, lun, ' &END'
        printf, lun, '

        printf, lun, (z1+1.0)^2.0d0*theta[indx_theta]/11605.4, format='(5(e12.4,:))'
        printf, lun, ' '
        printf, lun, (z1+1.0)^2.0d0*theta[indx_theta]/11605.4, format='(5(e12.4,:))'
        printf, lun, ' '
        printf, lun, (z1+1.0)^2.0d0*theta[indx_theta]/11605.4, format='(5(e12.4,:))'
        printf, lun, ' '
        printf, lun, rho[indx_rho], format='(5(e12.4,:))'
        printf, lun, ' '
        for j = 0, n_elements(indx_wvl)-1 do printf, lun, npix[indx_wvl[j]], wvlmin[indx_wvl[j]], wvlmax[indx_wvl[j]]

        free_lun, lun
      ENDIF
endif


;---------------------------------------------------
; Write the adf42 file - ca
;---------------------------------------------------

if (where(strupcase(tag_names(files)) EQ $
                           'ADF42_CA_FILE'))[0] NE -1 THEN BEGIN
  openw, lun, files.adf42_ca_file, /get_lun

  printf, lun, ' &FILES'
  printf, lun, '   dsn04  = ''' + files.adf04_ca_file + ''','
  printf, lun, '   dsn18  = ''NULL'','
  printf, lun, '   dsn35  = ''NULL'','
  printf, lun, '   dsn15  = ''' + files.adf15_ca_file + ''','
  printf, lun, '   dsn40  = ''' + files.adf40_ca_file + ''','
  printf, lun, '   dsn11  = ''' + files.adf11_ca_file + ''','
  printf, lun, '   dsn11f = ''NULL''
  printf, lun, ' &END
  printf, lun, ' '

  printf, lun, ' &ION
  printf, lun, '    element = ''' + xxesym(z0_nuc) + ''','
  printf, lun, '    z0      = ' + string(z0_nuc, format='(i3)') + ','
  printf, lun, '    z1      = ' + string(z1, format='(i3)') + ','
  printf, lun, '    ip      = ' + string(ionpot*8065.54445)
  printf, lun, ' &END
  printf, lun, ' '

  printf, lun, ' &META'
  printf, lun, '   lnorm    = .TRUE.,'
  printf, lun, '   nmet     = 1,'
  printf, lun, '   imetr(1) = 1'
  printf, lun, ' &END'
  printf, lun, ' '

  printf, lun, ' &PROCESS'
  printf, lun, '   liosel = .FALSE.,'
  printf, lun, '   lhsel  = .FALSE.,'
  printf, lun, '   lrsel  = .FALSE.,'
  printf, lun, '   lisel  = .FALSE.,'
  printf, lun, '   lnsel  = .FALSE.,'
  printf, lun, '   lpsel  = .FALSE.,'
  printf, lun, '   zeff   = 3.0'
  printf, lun, ' &END'
  printf, lun, ' '

  printf, lun, ' &OUTPUT'
  printf, lun, '   lmetr   = .FALSE.,'
  printf, lun, '   ltscl   = .FALSE.,'
  printf, lun, '   ldscl   = .FALSE.,'
  printf, lun, '   lbrdi   = .FALSE.,'
  printf, lun, '   amin    = 0.0,'
  printf, lun, '   numte   = ' + string(n_elements(indx_theta)) + ','
  printf, lun, '   numtion = ' + string(n_elements(indx_theta)) + ','
  printf, lun, '   numth   = ' + string(n_elements(indx_theta)) + ','
  printf, lun, '   numdens = ' + string(n_elements(indx_rho)) + ','
  printf, lun, '   numdion = 0,'
  printf, lun, '   numwvl  = ' + string(n_elements(indx_wvl))
  printf, lun, ' &END'
  printf, lun, '

  printf, lun, (z1+1.0)^2.0d0*theta[indx_theta]/11605.4, format='(5(e12.4,:))'
  printf, lun, ' '
  printf, lun, (z1+1.0)^2.0d0*theta[indx_theta]/11605.4, format='(5(e12.4,:))'
  printf, lun, ' '
  printf, lun, (z1+1.0)^2.0d0*theta[indx_theta]/11605.4, format='(5(e12.4,:))'
  printf, lun, ' '
  printf, lun, rho[indx_rho], format='(5(e12.4,:))'
  printf, lun, ' '
  for j = 0, n_elements(indx_wvl)-1 do printf, lun, npix[indx_wvl[j]], wvlmin[indx_wvl[j]], wvlmax[indx_wvl[j]]

  free_lun, lun

  if ca_only eq 0 then begin

      ;------------------------------------------
      ; Write the adf42 post processing file - ls
      ;------------------------------------------

      IF (where(strupcase(tag_names(files)) EQ $
                           'ADF42_LS_PP_FILE'))[0] NE -1 THEN BEGIN
        adf42_ls_paper = files.adf42_ls_file
        strput,adf42_ls_paper,'txt',strpos(adf42_ls_paper,'dat')

        openw, lun, files.adf42_ls_pp_file, /get_lun

        printf, lun, files.adf42_ls_file
        printf, lun, adf42_ls_paper
        printf, lun, today[0]

        free_lun, lun
      ENDIF

      ;------------------------------------------
      ; Write the adf42 post processing file - ic
      ;------------------------------------------

      IF (where(strupcase(tag_names(files)) EQ $
                           'ADF42_IC_PP_FILE'))[0] NE -1 THEN BEGIN
        adf42_ic_paper = files.adf42_ic_file
        strput,adf42_ic_paper,'txt',strpos(adf42_ic_paper,'dat')

        openw, lun, files.adf42_ic_pp_file, /get_lun

        printf, lun, files.adf42_ic_file
        printf, lun, adf42_ic_paper
        printf, lun, today[0]

        free_lun, lun

      ENDIF

  endif


;------------------------------------------
; Write the adf42 post processing file - ca
;------------------------------------------

  IF (where(strupcase(tag_names(files)) EQ $
                      'ADF42_CA_PP_FILE'))[0] NE -1 THEN BEGIN

    adf42_ca_paper = files.adf42_ca_file
    strput,adf42_ca_paper,'txt',strpos(adf42_ca_paper,'dat')

    openw, lun, files.adf42_ca_pp_file, /get_lun

    printf, lun, files.adf42_ca_file
    printf, lun, adf42_ca_paper
    printf, lun, today[0]

    free_lun, lun

  ENDIF

ENDIF ELSE BEGIN

  print, 'ADAS8XX_CREATE_DRIVERS WARNING: no adf42 filenames in files structure:'
  print, '       generation of ADF42 file omitted. '

ENDELSE


END


