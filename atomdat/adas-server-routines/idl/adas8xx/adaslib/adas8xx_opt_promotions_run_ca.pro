;-----------------------------------------------------------------------------
;+
;
; NAME     : adas8xx_opt_promotions_run_ca
;
; PURPOSE  : performs an adas801/810 ca_only run. For use with
;            adas8xx_get_total_line_power
;
; CATEGORY : ADAS
;
; NOTES    :
;  
; USE      : 
;
; INPUT    :
; (I*4) z0_nuc        = Ion nuclear charge
; (I*4) nel           = number of electrons
; (I*4) z_ion         = z0_nuc-nel
; (I*4) ionpot        = ionisation potential (cm-1)
; (I*4) for_tr_sel    = Cowan option for radiative transitions,
;                       default=3
; (I*4) year          = year identifier for data (defualt = 40)
; (STRUCT) promotion_results
;   (C* ) grd_cfg       = ground configuration string
;   (C* ) ex_cfg        = excited state configuration strings (array)
;   (I*4) grd_par       = parity of ground state
;   (I*4) ex_par        = parity of excited states
;   (I*4) grd_zc        = ground effective charage for adf34 driver
;   (I*4) ex_zc         = excited effective charage for adf34 driver
;   (I*4) oc_store      = configuration occupancies
; 
; (1/0) only801       = if 1 then stop after running adas801, otherwise
;                       continue to adas810
;(STRUCT) plasma = plasma conditions.
;   (R*8) THETA        : array of electron temperatures (eV)
;   (I*4) INDX_THETA   : array of indices of theta to select Te
;   (R*8) RHO          : array of electron density (cm-3)
;   (I*4) INDX_RHO     : array of indices of rho to select Ne
;   (I*4) NPIX         : array of pixels per wavelength region
;   (R*8) WVLMIN       : lower bounds of wavelength region(s)
;   (R*8) WVLMAX       : upper bounds of wavelength region(s)
;   (I*4) INDX_WVL     : indicies of wavelength regions to use
;   (I*4) THETA_NOSCALE: if 0, theta is assumed to be reduced Te
;   (I*4) RHO_SCALE    : if 1, rho is assumed to be reduced Ne
; (I*4) cowan_scale_factors: Slater parameter multipliers for Cowan 
;                     code (optional, defaults are usually sufficient)
; (I*4) lun_verb       : logical unit number for outputting diagnostic 
;                        information (if set)
 
;
; OUTPUTS   :
; (I*4) exit_status   = returned from adas8xx_create_ca_adf04.
; (STRUCT) files = names of all the files used:
;   (C* ) adf04_ca_t1_file  
;   (C* ) adf04_ca_t3_file  
;   (C* ) adf04_ca_file     
;   (C* ) adf34_file        
;   (C* ) adf34_inst_file   
;   (C* ) adf34_ls_pp_file  
;   (C* ) adf34_ic_pp_file  
;   (C* ) adf42_ls_file     
;   (C* ) adf42_ic_file     
;   (C* ) adf42_ls_pp_file  
;   (C* ) adf42_ic_pp_file  
;   (C* ) adf04_ls_file     
;   (C* ) adf04_ic_file     
;   (C* ) adf42_ca_pp_file  
;   (C* ) adf42_ca_file     
;   (C* ) adf15_ls_file     
;   (C* ) adf15_ic_file     
;   (C* ) adf40_ls_file     
;   (C* ) adf40_ic_file     
;   (C* ) adf11_ls_file     
;   (C* ) adf11_ic_file     
;   (C* ) adf15_ca_file     
;   (C* ) adf40_ca_file     
;   (C* ) adf11_ca_file     
;
; CATEGORY:
;       Adas system.
;
; WRITTEN:
;       Adam Foster, University of Strathclyde,08-01-08
;
; MODIFIED:
;       1.1   Adam Foster
;              - First version
;       1.2   Martin O'Mullane
;              - Call binaries from central ADAS rather than
;                offline_adas/adas8#1/bin.
;
; VERSION:
;       1.1   08-01-2008
;       1.2   06-11-2009
; 
;-
;-----------------------------------------------------------------------------

PRO adas8xx_opt_promotions_run_ca, z0_nuc     = z0_nuc,               $
                                nel           = nel,                  $
                                z_ion         = z_ion,                $
                                ionpot        = ionpot,               $
                                for_tr_sel    = for_tr_sel,           $
                                promotion_results=promotion_results,  $
                                only801       = only801,              $
                                exit_status   = exit_status,          $
                                plasma        = plasma,               $
                             cowan_scale_factors=cowan_scale_factors, $
                                files         = files,                $
                                year          = year,                 $
                                lun_verb      = lun_verb
				

common shells, ndshell     ,                                          $
               shmax       , nval        , lval       , shl_lab  ,    $
               iprom_1     , iprom_2     ,                            $
               nrg_cnt_idx , nrg_exl_idx , nrg_el_cnt ,               $
               nval_max    , nval_min    , lval_max   , lval_min ,    $
               njvals 

; checks for plasma structure

theta = plasma.theta
rho = plasma.rho
indx_theta = plasma.indx_theta

indx_rho = plasma.indx_rho

npix = plasma.npix
wvlmin = plasma.wvlmin
wvlmax = plasma.wvlmax
indx_wvl = plasma.indx_wvl
theta_noscale = plasma.theta_noscale
rho_scale = plasma.rho_scale

;-----------------------------------------------------------------------
; theta and rho must be of length 2 or more.
; Create second value and then ignore result from it
;-----------------------------------------------------------------------

if n_elements(theta) EQ 1 then begin
  theta=[theta, 2* theta]
  indx_theta=[0,1]
endif
if n_elements(rho) EQ 1 then begin
  rho=[rho, 2* rho]
  indx_rho=[0,1]
endif
  
if not keyword_set(indx_theta) and keyword_set(theta) then $
       indx_theta=indgen(n_elements(theta))
if not keyword_set(indx_rho) and keyword_set(rho) then $
       indx_theta=indgen(n_elements(rho))

; convert theta from eV to K
theta=theta*11600.0

IF not keyword_set(year) then begin
  print, 'adas8xx_opt_promotions_run_ca warning: year not set, using '+$
         '40 as default value)'
  year=40
endif

yearstr=strcompress(string(year),/remove_all)


;indx_rho=indgen(n_elements(rho))
;rho_scale =0
ca_only = 1

    
;----------------------------------------------------------------------
; set up the element and ion ranges
;----------------------------------------------------------------------

if not (keyword_set(nel)) then begin
    print," adas8xx_opt_promotions_run_ca: error - nel keyword value required."
    stop
end 

if z_ion NE z0_nuc-nel then begin
  print, 'ADAS8XX_OPT_PROMOTIONS_RUN_CA ERROR: z_ion=',z_ion, ' z0_nuc-nel='$
        ,z0_nuc-nel
  print, 'Forcing z1=z0_nuc-nel=',z0_nuc-nel
  z1=z0_nuc-nel
endif else  z1=z_ion
;zc=indgen(z0_nuc)

if keyword_set(lun_verb) then begin
  printf, lun_verb, 'nuclear charge  z0_nuc =', z0_nuc
endif

    xxelem,z0_nuc,element

elname     = strlowcase(element)
elsymb     = strlowcase(xxesym(z0_nuc))
plevel     = '01'

;----------------------------------------------------------------------
; set up the rules
;----------------------------------------------------------------------

j = z1
   
;-----------------------------------
; note that j and z1[j] are the same   
;-----------------------------------
if keyword_set(lun_verb) then begin
  printf, lun_verb, 'ionisation stage z =', z1
endif   
  
;----------------------------------------------------------------------
; prepare the output adf34 and adf42 driver and data file names
;----------------------------------------------------------------------
   
if not ca_only then begin

     adf34_file      = strcompress('adf34_' + elname + '_' + elsymb        $
                       + string(z1,format='(i2)') + '.dat',             $
                       /remove_all)

     adf34_inst_file = strcompress('adf34_' + elname + '_' + elsymb        $
                       + string(z1,format='(i2)') + '_inst.dat',        $
                       /remove_all)

     adf34_ls_pp_file= strcompress('adf34_' + elname + '_' + elsymb        $
                       + string(z1,format='(i2)') + '_ls_pp.dat',       $
                       /remove_all)

     adf34_ic_pp_file= strcompress('adf34_' + elname + '_' + elsymb        $
                       + string(z1,format='(i2)') + '_ic_pp.dat',       $
                       /remove_all)

     adf42_ls_file   = strcompress('adf42_' + elname + '_' + 'ls#_'+yearstr+''      $
                       +elsymb + string(z1,format='(i2)') + '.dat',     $
                       /remove_all)

     adf42_ic_file   = strcompress('adf42_' + elname + '_' + 'ic#_'+yearstr+''      $
                       +elsymb + string(z1,format='(i2)') + '.dat',     $
                       /remove_all)

     adf42_ls_pp_file= strcompress('adf42_' + elname + '_' + elsymb        $
                       + string(z1,format='(i2)') + '_ls_'+yearstr+'_pp.dat',    $
                       /remove_all)
                       
     adf04_ls_file   = strcompress('adf04_copmm#' + string(z0,             $
                       format='(i2)') + '_' + 'ls_'+yearstr+'#' + elsymb +          $
                       string(z1, format='(i2)') + '.dat')

     adf04_ic_file   = strcompress('adf04_copmm#' + string(z0,             $
                       format='(i2)') + '_' + 'ic_'+yearstr+'#' + elsymb +          $
                       string(z1, format='(i2)') + '.dat',/remove_all)

     adf42_ic_pp_file= strcompress('adf42_' + elname + '_' + elsymb        $
                       + string(z1,format='(i2)') + '_ic_'+yearstr+'_pp.dat',    $
                       /remove_all)
                       
end else begin

     adf34_file        = ' '  
     adf34_inst_file   = ' '                           
     adf34_ls_pp_file  = ' '
     adf34_ic_pp_file  = ' ' 
     adf42_ls_file     = ' ' 
     adf42_ic_file     = ' ' 
     adf42_ls_pp_file  = ' ' 
     adf42_ic_pp_file  = ' '
     adf04_ls_file     = ' '
     adf04_ic_file     = ' ' 
     
                                       
end                    
                  
adf42_ca_file   = strcompress('adf42_' + elname + '_' + 'ca#'+yearstr+'_'      $
                  +elsymb + string(z1,format='(i2)') + '.dat',     $
                  /remove_all)

adf42_ca_pp_file= strcompress('adf42_' + elname + '_' + elsymb        $
                  + string(z1,format='(i2)') + '_ca_'+yearstr+'_pp.dat',    $
                  /remove_all)


;------------------------------------------------------------------------
; scale theta and rho arrays as required for default entry to procedures
;------------------------------------------------------------------------

if theta_noscale eq 1 then begin
    theta_sc = theta/(j+1.0)^2
end else begin
    theta_sc = theta
end

if rho_scale eq 1 then begin
    rho_sc = rho*(j+1.0)^7
end else begin
    rho_sc = rho
end 

;----------------------------------------------------------------------
; write the ouput files for an ion 
;----------------------------------------------------------------------
; a check for near neutrals
if z1 LE 1 then begin
  make_ic_adf34=1 
  adf34_file = strcompress('adf34_' + elname + '_' + elsymb        $
               + string(z1,format='(i2)') + 'iccheck.dat',$
                       /remove_all)
endif else begin
  make_ic_adf34=0
endelse

;----------------------------------------------------------------------
; prepare adf04 type 1 and type 3 configuration average data file names
;----------------------------------------------------------------------

adf04_ca_t1_file     = strcompress('adf04_copmm#' + string(z0_nuc,        $
                    format='(i2)') + '_' + 'ca_'+yearstr+'#' + elsymb +        $
                    string(z_ion, format='(i2)') + '_t1.dat',/remove_all)
adf04_ca_t3_file     = strcompress('adf04_copmm#' + string(z0_nuc,        $
                    format='(i2)') + '_' + 'ca_'+yearstr+'#' + elsymb +        $
                    string(z_ion, format='(i2)') + '_t3.dat',/remove_all)
adf04_ca_file     = adf04_ca_t3_file


;----------------------------------------------------------------------
; prepare adf15, adf40 and adf11 data file names of ls, ic and ca type
;----------------------------------------------------------------------

if not ca_only then begin

     adf15_ls_file     =  strcompress('adf15_pec'+yearstr+'#'                       $
                          + elsymb + '_pec'+yearstr+'#'                             $
                          + elsymb + '_ls#'+ elsymb +                      $
                          string(z_ion,format='(i2)')                      $
                          + '.dat', /remove_all)
     adf15_ic_file     =  strcompress('adf15_pec'+yearstr+'#'                       $
                          + elsymb + '_pec'+yearstr+'#'                             $
                          + elsymb + '_ic#'+ elsymb +                      $
                          string(z_ion,format='(i2)')                      $
                          + '.dat', /remove_all)
     adf40_ls_file     =  strcompress('adf40_fpec'+yearstr+'#'                      $
                          + elsymb + '_fpec'+yearstr+'#'                            $
                          + elsymb + '_ls#'+ elsymb +                      $
                          string(z_ion,format='(i2)')                      $
                          + '.dat', /remove_all)
     adf40_ic_file     =  strcompress('adf40_fpec'+yearstr+'#'                      $
                          + elsymb + '_fpec'+yearstr+'#'                            $
                          + elsymb + '_ic#'+ elsymb +                      $
                          string(z_ion,format='(i2)')                      $
                          + '.dat', /remove_all)
     adf11_ls_file     =  strcompress('adf11_plt'+yearstr+'_partial_plt'+yearstr+'_partial_' $
                          + elsymb +'_plt'+yearstr+'_ls#' + elsymb                  $
                          + string(z_ion,format='(i2)') +'.dat',           $
                          /remove_all)
     adf11_ic_file     =  strcompress('adf11_plt'+yearstr+'_partial_plt'+yearstr+'_partial_' $
                          + elsymb +'_plt'+yearstr+'_ic#' + elsymb                  $
                          + string(z_ion,format='(i2)') +'.dat',           $
                          /remove_all)
                             
end else begin

     adf15_ls_file     = ' '     
     adf15_ic_file     = ' '     
     adf40_ls_file     = ' '     
     adf40_ic_file     = ' '                              
     adf11_ls_file     = ' '  
     adf11_ic_file     = ' '         
                                  
end                      
                          
adf15_ca_file     =  strcompress('adf15_pec'+yearstr+'#'                       $
                     + elsymb + '_pec'+yearstr+'#'                             $
                     + elsymb + '_ca#'+ elsymb +                      $
                     string(z_ion,format='(i2)')                      $
                     + '.dat', /remove_all)
adf40_ca_file     =  strcompress('adf40_fpec'+yearstr+'#'                      $
                     + elsymb + '_fpec'+yearstr+'#'                            $
                     + elsymb + '_ca#'+ elsymb +                      $
                     string(z_ion,format='(i2)')                      $
                     + '.dat', /remove_all)
                      
adf11_ca_file     =  strcompress('adf11_plt'+yearstr+'_partial_plt'+yearstr+'_partial_' $
                     + elsymb +'_plt'+yearstr+'_ca#' + elsymb                  $
                     + string(z_ion,format='(i2)') +'.dat',           $
                     /remove_all)

                     
                     

;------------------------------------------------------------------------
; create an array of all the file names
;------------------------------------------------------------------------
files={adf04_ca_t1_file  :adf04_ca_t1_file  ,$
       adf04_ca_t3_file  :adf04_ca_t3_file  ,$
       adf04_ca_file     :adf04_ca_file     ,$
       adf34_file        :adf34_file        ,$
       adf34_inst_file   :adf34_inst_file   ,$
       adf34_ls_pp_file  :adf34_ls_pp_file  ,$
       adf34_ic_pp_file  :adf34_ic_pp_file  ,$
       adf42_ls_file     :adf42_ls_file     ,$
       adf42_ic_file     :adf42_ic_file     ,$
       adf42_ls_pp_file  :adf42_ls_pp_file  ,$
       adf42_ic_pp_file  :adf42_ic_pp_file  ,$
       adf04_ls_file     :adf04_ls_file     ,$
       adf04_ic_file     :adf04_ic_file     ,$
       adf42_ca_pp_file  :adf42_ca_pp_file  ,$
       adf42_ca_file     :adf42_ca_file     ,$
       adf15_ls_file     :adf15_ls_file     ,$
       adf15_ic_file     :adf15_ic_file     ,$
       adf40_ls_file     :adf40_ls_file     ,$
       adf40_ic_file     :adf40_ic_file     ,$
       adf11_ls_file     :adf11_ls_file     ,$
       adf11_ic_file     :adf11_ic_file     ,$
       adf15_ca_file     :adf15_ca_file     ,$
       adf40_ca_file     :adf40_ca_file     ,$
       adf11_ca_file     :adf11_ca_file}

; create temporary plasma structure

tmpplasma={THETA           :  THETA        ,$
           INDX_THETA      :  INDX_THETA   ,$
           RHO             :  RHO          ,$
           INDX_RHO        :  INDX_RHO     ,$
           NPIX            :  NPIX         ,$
           WVLMIN          :  WVLMIN       ,$
           WVLMAX          :  WVLMAX       ,$
           INDX_WVL        :  INDX_WVL     ,$
           THETA_NOSCALE   :  THETA_NOSCALE,$
           RHO_SCALE       :  RHO_SCALE    }

         


adas8xx_create_drivers, z0_nuc           = z0_nuc,                  $
                        z1               = z1,                      $
                        ionpot           = ionpot,                  $
                        plasma           = tmpplasma,               $
                        files            = files,                   $
                        for_tr_sel       = for_tr_sel,            $
                        promotion_results= promotion_results,     $
                        ca_only          = ca_only,               $
                        make_ic_adf34    = make_ic_adf34,         $
                        cowan_scale_factors=cowan_scale_factors


;----------------------------------------------------------------------
; create the configuration average adf04 data files
;----------------------------------------------------------------------


exit_status=0

adas8xx_create_ca_adf04, z1,                                      $
                         z0_nuc,                                  $
                         promotion_results.oc_store,              $
                         ionpot     = ionpot,                     $
                         plasma     = tmpplasma,                     $
                         adf04_t1_file  = files.adf04_ca_t1_file, $
                         adf04_t3_file  = files.adf04_ca_t3_file, $
                         /archive_files , $
                         exit_status = exit_status, $
                         cowan_scale_factors=cowan_scale_factors

if exit_status LT 0 THEN return

;----------------------------------------------------------------------
; create the ls and ic adf04 data files
;----------------------------------------------------------------------

if not ca_only then begin

    adas8xx_create_ls_ic_adf04, z0_nuc,                                   $
                                z1,                                       $
                                files             = files,                $
                                archive_dir       = archive_dir,          $
                                /archive_files

endif


IF KEYWORD_SET(only801) THEN BEGIN
  adf42_ca_archive= files.adf42_ca_file
  ipos=STRPOS(adf42_ca_archive,'_')
  STRPUT,adf42_ca_archive,'/',ipos
  ipos=STRPOS(adf42_ca_archive,'_')
  STRPUT,adf42_ca_archive,'/',ipos
  
  cmd= 'mv '+files.adf42_ca_file+' '+adf42_ca_archive
  spawn,cmd
   
  adf42_ca_pp_archive= files.adf42_ca_pp_file
  ipos=STRPOS(adf42_ca_pp_archive,'_')
  STRPUT,adf42_ca_pp_archive,'/',ipos
  ipos=STRPOS(adf42_ca_pp_archive,'_')
  STRPUT,adf42_ca_pp_archive,'/',ipos

  cmd= 'mv '+files.adf42_ca_pp_file+' '+adf42_ca_pp_archive
  spawn,cmd  
ENDIF  ELSE BEGIN

;----------------------------------------------------------------------
; create the ls, ic and ca adf15, adf40 and adf11 data files
;----------------------------------------------------------------------

  adas8xx_create_adf15_adf40,  z0_nuc,                                   $
                               z1,                                   $  
                               files             = files,            $
                               ca_only           = ca_only,          $  
                               /archive_files                          



  if keyword_set(lun_verb) then begin
    printf,lun_verb,'---------------------------------'
  endif
ENDELSE
adf42file=files.adf42_ca_file

; z0_nuc for some reason has changed to non integer value - put it back!
z0_nuc=fix(z0_nuc)


;-----------------------------------------------------------------------
; If keyword make_ic_adf34 is set, run rcn to check for convergence on
; energy levels.
;-----------------------------------------------------------------------

IF keyword_set(make_ic_adf34) THEN BEGIN
  if keyword_set(lun_verb) then begin
    printf, lun_verb, 'Testing for convergence:'
  endif
  ; here we will run rcn and see if it works.

; sort configs by parity
  hx34sr,files.adf34_file,adf34_file+'sorted'
  ionname=strcompress(elsymb+string(z1), /remove_all)
; write the rcn driver file
  openw,lun,ionname+'_rcn1_driver',/get_lun
  printf,lun,1
  printf,lun,adf34_file+'sorted'
  printf,lun,ionname+'_rcn1_out'
  printf,lun,ionname+'_d2'
  close, lun
  free_lun, lun
  
  adasbin=getenv('ADASFORT')+'/'
;  adasbin=adasbin+'../offline_adas/adas8#1/bin/'
  
  cmd=adasbin+'rcn.x < '+ionname+'_rcn1_driver'
  spawn, cmd
  
  ; check output for phrase "no convergence"
  no_cvg=0
  dummy=''
  openr, lun, ionname+'_rcn1_out',/get_lun
  while NOT eof(lun) DO BEGIN
    readf,lun, dummy
    if (strpos(dummy, 'No convergence'))[0] NE -1 then begin
      no_cvg=1
;      stop
    endif  
  endwhile
  close, lun
  free_lun, lun
  
  
  if no_cvg EQ 1 then begin
    if keyword_set(lun_verb) then begin
      printf, lun_verb, 'Configuration set does not converge!'
    endif
    exit_status=-1
  endif
  
  ; delete files
  
  spawn, 'rm '+ionname+'_rcn1_out'
  spawn, 'rm '+files.adf34_file+'sorted'
  spawn, 'rm '+files.adf34_file
  spawn, 'rm '+ionname+'_d2'
  spawn, 'rm '+ionname+'_rcn1_driver'
ENDIF
;stop
END
