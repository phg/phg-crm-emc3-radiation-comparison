;+
; PROJECT : ADAS 
;
; NAME    : OCC2COW()
;
; PURPOSE : Convert occupation numbers to a Cowan input string.
;
; EXPLANATION:
;       This routines takes a given list of occupation numbers and
;       returns a string suitable for rcn1.
;
; USE:
;       An example:
;
;         print,occ2cow([1,2,3,2,4])
;       
;       Gives:
; 
;         1s1  2s2  2p3  3s2  3p4
;
; INPUTS:
;       occ: Occupation numbers
;
; OPTIONAL INPUTS:
;       None.
;
; OUTPUTS:
;       A string suitable for the input to rcn.
;
; OPTIONAL OUTPUTS:
;       None.
;
; KEYWORD PARAMETERS:
;       None.
;
; CALLS:
;       None.
;
; SIDE EFFECTS:
;       None.
;
; CATEGORY:
;       Series 8 utility.
;
; WRITTEN:
;       Allan Whiteford
;
; MODIFIED:
;       Version 1.1   Allan Whiteford    01-11-2004
;                     First release.
;
;       Version 1.2   Hugh Summers       28-09-2006
;                     Modified to deal more generally with Cowan
;
;       Version 1.3   Hugh Summers       29-11-2006
;                     Correction to logic for rare gas omitted-shell detection
;
;       Version 1.4   H. P. Summers      18-06-2007
;                     Further correction to logic for Cowan effective z for 
;                     driver.  Now use zc=z1 for z0<19.
;
;       Version 1.5   Martin O'Mullane
;                      - Return izc as charge state + 1.
;                      - Add configurations with zero occupancy to the
;                        returned string if they involve an s, p or d
;                        orbital before the final orbital.
;                      - Both changes are a simpler way to specify the
;                        configuration and revert previous, mostly
;                        unsuccessful attempts, to minimize having to
;                        specify the occupied core.
;                        
;
; VERSION:
;       1.1     01-11-2004
;       1.2     28-09-2006
;       1.3     29-11-2006
;       1.4     18-06-2007
;       1.5     21-06-2019
;-
;-----------------------------------------------------------------------------

FUNCTION occ2cow, occ, iz0, iz, izc, start_shell

;-----------------------------------------------------------------------------
; Shell definitions and occupations - to ndshell=36 (8k)
;-----------------------------------------------------------------------------

   shmax   = [   2,   2,   6,   2,   6,  10,   2,   6,  10,  14,   2,   6,  10,  14,  18,   2,   6,  10,      $
                14,  18,  22,   2,   6,  10,  14,  18,  22,  26,   2,   6,  10,  14,  18,  22,  26,  30]
   shl_lab = ['1s','2s','2p','3s','3p','3d','4s','4p','4d','4f','5s','5p','5d','5f','5g','6s','6p','6d',      $
              '6f','6g','6h','7s','7p','7d','7f','7g','7h','7i','8s','8p','8d','8f','8g','8h','8i','8k']

   nrg_cnt_idx = [   1,   3,   5,   8,  12,  17]
   nrg_exl_idx = [ [ -1, -1],  [ -1, -1],  [ -1, -1],  [ -1, -1],  [  9, -1],  [ 13, 14]]
   nrg_el_cnt  = [   2,  10,  18,  36,  54,  86]

   special_case = ['4d', '4f', '5s', '5p']

   output = ''

   start_shell = 0

   ; Last occupied shell
   
   icut = where(occ NE 0)
   icut = icut[n_elements(icut)-1]

   for i = 0,5 do begin
   
      if i ne 0 then begin 
         idx_cnt=indgen(nrg_cnt_idx[i])
         idx1_cnt=where((idx_cnt ne nrg_exl_idx[0,i]) and (idx_cnt ne nrg_exl_idx[1,i]))
      endif else begin
         idx1_cnt = 0
         idx_cnt  = 0
      endelse
   
     if (total(occ[idx1_cnt]) eq nrg_el_cnt[i]) and (total(occ[idx_cnt]) eq nrg_el_cnt[i]) then start_shell = nrg_cnt_idx[i]
   
     if (start_shell gt icut and i gt 0) then start_shell = nrg_cnt_idx[i-1]
     
   endfor  

   if start_shell gt icut then start_shell = icut
   
   izc=iz+1 

   for i = start_shell, icut do begin
   
      ; Cowan's cfg strings
      if occ[i] gt 0 then begin
         if occ[i] gt 9 then str=strtrim(string(occ[i]),2) + " "
         if occ[i] le 9 then str=strtrim(string(occ[i]),2) + "  "
         output = output + shl_lab[i] + str
      endif
      
      ; special cases - fill empty shells but don't allow the string to get too long
      
      if occ[i] EQ 0 then begin
         res = where(strpos(special_case, shl_lab[i]) NE -1, count)
         if count NE 0 then begin
            if strlen(output) LE 34 then output = output + shl_lab[i] + '0  '
         endif
      endif
      
   endfor

   return, output

end
