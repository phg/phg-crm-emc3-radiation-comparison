;-----------------------------------------------------------------------------
;+
;
; NAME     : adas8xx_plasma_defaults
;
; PURPOSE  : This routine returns default values for the "plasma"
;            structure in adas8xx_opt_promotions_control. In theory
;            this shouldn't be called if full inputs are provided, 
;            but it provides an acceptable set of standard values
;            in the absence of user supplied ones. 
;            
; CATEGORY : ADAS
;
; NOTES    :
;  
; USE      : 
;
; INPUT    : 
; (C* )    item           : label of item you wish to return
;           'theta'       : Electron Temperature
;           'indx_theta'  : Indicies of theta vector to use
;           'rho'         : Electron Density
;           'indx_rho'    : Indicies of rho vector to use
;           'npix'        : number of pixels in each wavelength range
;           'wvlmin'      : min wavelength in each wavelength range
;           'wvlmax'      : max wavelength in each wavelength range
;           'indx_wvl'    : indices of wavelength ranges to use
;           'theta_noscale' : Theta is scaled Te if not set
;           'rho_scale'   : Rho is scaled Ne is set
; (KEYWORD) all           : if set, return all the items in a structure
;                           not just one (overrides request for 
;                           individual item)
;
; OUTPUTS   :
; returns requested item as an array, or everything in a structure
; if keyword "all" is set.
; CATEGORY:
;       Adas system.
;
; WRITTEN:
;       Adam Foster, University of Strathclyde,23-03-2009
;
; MODIFIED:
;       1.1     Adam Foster

; VERSION:
;       1.1   23-03-2009
; 
;-
;-----------------------------------------------------------------------------
FUNCTION adas8xx_plasma_defaults, item, all=all

if not keyword_set(all) then begin
  CASE strlowcase(item) of
     'theta'        : ret = [ 2.00e+02, 3.00e+02, 5.00e+02, 7.00e+02, 1.00e+03,  $
                            1.50e+03, 2.00e+03, 3.00e+03, 5.00e+03, 7.00e+03,  $
                            1.00e+04, 1.50e+04, 2.00e+04, 3.00e+04, 5.00e+04,  $
                            7.00e+04, 1.00e+05, 1.50e+05, 2.00e+05, 3.00e+05,  $
                            5.00e+05, 1.00e+06, 2.00e+06, 5.00e+06, 1.00e+07]
     'indx_theta'   : ret = [ 0, 1, 2, 4, 6, 8,10,11,12,13,14,16,18,19]
     'rho'          : ret = [ 1.00e-03, 1.00e+00, 1.00e+01, 1.00e+02, 1.00e+03,  $
                            1.00e+04, 3.00e+04, 1.00e+05, 3.00e+05, 1.00e+06,  $
                            3.00e+06, 1.00e+07, 3.00e+07, 1.00e+08, 3.00e+08,  $
                            1.00e+09, 3.00e+09, 1.00e+10, 3.00e+10, 1.00e+11,  $
                            3.00e+11, 1.00e+12, 3.00e+12, 1.00e+13, 3.00e+13,  $
                            1.00e+14, 3.00e+14, 1.00e+15, 3.00e+15, 1.00e+16]
     'indx_rho'     : ret = [17,19,21,23,25,27,29]
     'npix'         : ret = [      128,      128,      128,      128,      512,  $
                                   256,      256,      256,      256,      256]
     'wvlmin'       : ret = [ 1.00e+00, 1.00e+01, 1.00e+02, 1.00e+03, 1.00e+00,  $
                              3.00e+00, 7.00e+01, 5.00e+02, 1.00e+03, 3.00e+03]
     'wvlmax'       : ret = [ 1.00e+01, 1.00e+02, 1.00e+03, 1.00e+04, 1.00e+04,  $
                              5.00e+00, 1.20e+02, 1.50e+03, 2.00e+03, 7.00e+03]
     'indx_wvl'     : ret = [ 0, 1, 4]
     'theta_noscale': ret = [0]
     'rho_scale'    : ret = [0]
     ELSE           : begin
                        print, 'ADAS8XX_PLASMA_DEFAULTS error: unrecognised item'
                        STOP
                      endcase  
  ENDCASE

endif else begin
  theta=adas8xx_plasma_defaults('theta')
  indx_theta=adas8xx_plasma_defaults('indx_theta')
  rho=adas8xx_plasma_defaults('rho')
  indx_rho=adas8xx_plasma_defaults('indx_rho')
  npix=adas8xx_plasma_defaults('npix')
  wvlmin=adas8xx_plasma_defaults('wvlmin')
  wvlmax=adas8xx_plasma_defaults('wvlmax')
  indx_wvl=adas8xx_plasma_defaults('indx_wvl')
  theta_noscale=adas8xx_plasma_defaults('theta_noscale')
  rho_scale=adas8xx_plasma_defaults('rho_scale')

  ret={theta         : theta, $
       indx_theta    : indx_theta, $
       rho           : rho, $
       indx_rho      : indx_rho, $
       npix          : npix, $
       wvlmin        : wvlmin, $
       wvlmax        : wvlmax, $
       indx_wvl      : indx_wvl, $
       theta_noscale : theta_noscale, $
       rho_scale     : rho_scale}
  
endelse
RETURN, ret

END
