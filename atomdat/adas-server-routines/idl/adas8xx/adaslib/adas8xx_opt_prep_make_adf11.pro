;----------------------------------------------------------------------
;+
; PROJECT    :  ADAS
;
; NAME       :  adas8xx_opt_prep_make_adf11
;
; PURPOSE    :  Alters ADF42 driver files to run ic, ca and ca_large
;               sized ADAS810 calculations to obtain PLTs
;               Precursor to run_optimise_plt.pl
;
; ARGUMENTS  :  All output arguments will be defined appropriately.
;
;               NAME      I/O    TYPE   DETAILS
; REQUIRED   :  normdir    I     str    full path to directory from 
;                                       which ADAS808 data has beem
;                                       archived (ic and ca)
;               largedir   I     str    full path to directory from 
;                                       which ADAS808 data has beem
;                                       archived (large ca)
;               z_nuc      I     int    Atomic number of element
;               year       I     int    Data prodcution method identifier
;               plasma     I     struct Plasma condition
;            contains:
;                theta           dbl()  Array of T_e(eV)
;                indx_theta      int()  Indices of theta to use
;                rho             dbl()  Array of N_e(cm-3)
;                indx_rho        int()  Indices of rho to use
;                wvlmin          dbl()  Minimum wavelength to use
;                wvlmax          dbl()  Maximum wavelength to use
;                npix            int()  Number of pixels in region
;                theta_noscale   dbl()  If 0, theta is reduced temp
;                                       If 1, theta is Te in eV
;                                       (should be 1 here)
;                rho_scale       dbl()  If 0, rho is density (cm-3)
;                                       If 1, rho is reduced density
;                                       (should be 0 here)
;
; OPTIONAL   :  a11file    I     str    ADF11 file name for output
;                                       (default is in:
;                              <normdir>/adf11/plt40/plt40_<elsymb>.dat
;               archived   I     int    If set, data is assumed to be 
;                                       archived in standard 
;                                       subdirectories
;
;
; KEYWORDS      help              -     display help entry
;
; NOTES    
;
; AUTHOR     :  Adam Foster
;
; DATE       :  09/02/2009
;
; MODIFIED:
;
;
; VERSION:
;
;       1.1     09/02/2009
;-
;----------------------------------------------------------------------

PRO run_adas8xx_opt_prep_make_adf11, wkdir    = wkdir, $
                             archived = archived, $
                             z_nuc    = z_nuc, $
                             ca_only  = ca_only, $
                             year     = year, $
                             plasma   = plasma, $
                             large    = large
                             

; this code will generate the adf42 files to allow generation of
; the PLT adf11 for an entire element on an identical temperature
; and density grid
;
; It does not launch the cowan calculations themselves. 

if not keyword_set(year) then begin
  print, 'adas8xx_opt_prep_make_adf11.pro warning: keyowrd year not '+$
         'set, using default value of 40'
  year = 40
endif
yearstr=strcompress(string(year),/remove_all)

if not keyword_set(plasma) then begin 
  print, 'Using default plasma values'
  te     = adas_vector(low=10,high=10000, num=20)
  dens   = adas_vector(low=1e9,high=1e18, num=10)
  wvlmin = [1.0]
  wvlmax = [10000.0]
  npix   = [128]



  plasma={theta         : te,                       $
          indx_theta    : indgen(n_elements(te)),   $
          rho           : dens,                     $
          indx_rho      : indgen(n_elements(dens)), $
          wvlmin        : wvlmin,                   $
          wvlmax        : wvlmax,                   $
          npix          : npix,                     $
          theta_noscale : 1,                        $
          rho_scale     : 0 }
endif else begin
  if plasma.theta_noscale NE 1 then begin
    print, 'WARNING: theta_noscale NE 1 -> variable Te range: not '+$
            ' compatible with whole element ADF11 generation!'
    return
  endif
  if plasma.rho_scale NE 0 THEN BEGIN
    print, 'WARNING: rho_scale NE 1 -> variable Ne range: not '+$
            ' compatible with whole element ADF11 generation!'
  endif
endelse

z_nucstring=strcompress(string(z_nuc, format='(i2)'),/remove_all)
elsymb=xxesym(z_nuc,/lower)
print, elsymb
if keyword_set(large) then lstring='l' else lstring=''

xxelem, z_nuc,elname

for z_ion=0, z_nuc-1 do begin

  
  spacer = '_'
  
  if not keyword_set(ca_only) then begin
    adf04_ic_end  = strcompress('adf04'+spacer+'copmm#' + z_nucstring +$
                      spacer + 'ic_'+yearstr+'#' + elsymb +        $
                      string(z_ion, format='(i2)') + '.dat',/remove_all)
  
    adf04_ls_end  = strcompress('adf04'+spacer+'copmm#' + z_nucstring +$
                      spacer + 'ls_'+yearstr+'#' + elsymb +        $
                      string(z_ion, format='(i2)') + '.dat',/remove_all)
  endif
  
  adf04_ca_end  = strcompress('adf04'+spacer+'copmm#' + z_nucstring +$
                    spacer + lstring+'ca_'+yearstr+'#' + elsymb +        $
                    string(z_ion, format='(i2)') + '_t3.dat',/remove_all)
  
  if not keyword_set(ca_only) then begin
    adf11_ic_start  = strcompress('adf11'+spacer+'plt'+yearstr+'_partial'+$
                    spacer+'plt'+yearstr+'_partial_' +                        $
                    elsymb +'_mkadf11'+spacer+'plt'+yearstr+'_ic#' + elsymb +            $
                    string(z_ion,format='(i2)') +'.dat',     $
                    /remove_all)
  
    adf11_ls_start  = strcompress('adf11'+spacer+'plt'+yearstr+'_partial'+$
                      spacer+'plt'+yearstr+'_partial_' +                        $
                      elsymb +'_mkadf11'+spacer+'plt'+yearstr+'_ls#' + elsymb +            $
                      string(z_ion,format='(i2)') +'.dat',     $
                      /remove_all)
  endif
  
  adf11_ca_start  = strcompress('adf11'+spacer+'plt'+yearstr+'_partial'+$
                    spacer+'plt'+yearstr+'_partial_' +elsymb +'_mkadf11'+$
                    spacer+'plt'+yearstr+'_'+lstring+'ca#' + elsymb +      $
                    string(z_ion,format='(i2)') +'.dat',     $
                    /remove_all)

  if not keyword_set(ca_only) then begin
    adf42_ic_end   = strcompress('adf42' +spacer + elname + '_mkadf11'+ spacer +  $
                      'ic#'+yearstr+'_'+elsymb + string(z_ion,format='(i2)') + $
                      '.dat', /remove_all)
    
    adf42_ls_end   = strcompress('adf42' +spacer + elname + '_mkadf11'+ spacer +  $
                      'ls#'+yearstr+'_'+elsymb + string(z_ion,format='(i2)') + $
                      '.dat', /remove_all)
  endif 
  adf42_ca_end   = strcompress('adf42' +spacer + elname + '_mkadf11'+ spacer +  $
                    lstring+'ca#'+yearstr+'_'+elsymb + string(z_ion,format='(i2)') + $
                    '.dat', /remove_all)
  

  if not keyword_set(ca_only) then begin
    adf42_ic_pp_end= strcompress('adf42' + spacer+ elname + spacer + $
                      elsymb + string(z_ion,format='(i2)') + $
                      '_ic_'+yearstr+'_pp_mkadf11.dat', /remove_all)
  
    adf42_ls_pp_end= strcompress('adf42' + spacer+ elname + spacer + $
                      elsymb + string(z_ion,format='(i2)') + $
                      '_ls_'+yearstr+'_pp_mkadf11.dat', /remove_all)
  endif
  adf42_ca_pp_end= strcompress('adf42' + spacer+ elname + spacer + $
                    elsymb + string(z_ion,format='(i2)') + $
                    '_'+lstring+'ca_'+yearstr+'_pp_mkadf11.dat', /remove_all)

  if keyword_set(archived) then spacer='/'
  ; generate ADF04 & ADF42 file names:
  
  
  if not keyword_set(ca_only) then begin
    adf04_ic_start  = strcompress('adf04'+spacer+'copmm#' + z_nucstring +$
                      spacer + 'ic_'+yearstr+'#' + elsymb +        $
                      string(z_ion, format='(i2)') + '.dat',/remove_all)
  
    adf04_ls_start  = strcompress('adf04'+spacer+'copmm#' + z_nucstring +$
                      spacer + 'ls_'+yearstr+'#' + elsymb +        $
                      string(z_ion, format='(i2)') + '.dat',/remove_all)
  endif
  adf04_ca_start  = strcompress('adf04'+spacer+'copmm#' + z_nucstring +$
                    spacer + 'ca_'+yearstr+'#' + elsymb +        $
                    string(z_ion, format='(i2)') + '_t3.dat',/remove_all)
  
  if not keyword_set(ca_only) then begin
    adf11_ic_end  = strcompress('adf11'+spacer+'plt'+yearstr+'_partial'+$
                    spacer+'plt'+yearstr+'_partial_' +                        $
                    elsymb +spacer+'mkadf11_plt'+yearstr+'_ic#' + elsymb +            $
                    string(z_ion,format='(i2)') +'.dat',     $
                    /remove_all)
  
    adf11_ls_end  = strcompress('adf11'+spacer+'plt'+yearstr+'_partial'+$
                      spacer+'plt'+yearstr+'_partial_' +                        $
                      elsymb +spacer+'mkadf11_plt'+yearstr+'_ls#' + elsymb +            $
                      string(z_ion,format='(i2)') +'.dat',     $
                      /remove_all)
  endif
  adf11_ca_end  = strcompress('adf11'+spacer+'plt'+yearstr+'_partial'+$
                    spacer+'plt'+yearstr+'_partial_' +                        $
                    elsymb +spacer+'mkadf11_plt'+yearstr+'_ca#' + elsymb +            $
                    string(z_ion,format='(i2)') +'.dat',     $
                    /remove_all)
  
  if not keyword_set(ca_only) then begin
    adf42_ic_start   = strcompress('adf42' +spacer + elname + spacer +  $
                      'ic#'+yearstr+'_'+elsymb + string(z_ion,format='(i2)') +     $
                      '.dat', /remove_all)
    
    adf42_ls_start   = strcompress('adf42' +spacer + elname + spacer +  $
                      'ls#'+yearstr+'_'+elsymb + string(z_ion,format='(i2)') +     $
                      '.dat', /remove_all)
  endif
  
  adf42_ca_start   = strcompress('adf42' +spacer + elname + spacer +  $
                    'ca#'+yearstr+'_'+elsymb + string(z_ion,format='(i2)') +     $
                    '.dat', /remove_all)
  
  if not keyword_set(ca_only) then begin
    adf42_ic_pp_start= strcompress('adf42' + spacer+ elname + spacer + $
                      elsymb + string(z_ion,format='(i2)') + $
                      '_ic_'+yearstr+'_pp_mkadf11.dat', /remove_all)
  
    adf42_ls_pp_start= strcompress('adf42' + spacer+ elname + spacer + $
                      elsymb + string(z_ion,format='(i2)') + $
                      '_ls_'+yearstr+'_pp_mkadf11.dat', /remove_all)
  endif
  adf42_ca_pp_start= strcompress('adf42' + spacer+ elname + spacer + $
                    elsymb + string(z_ion,format='(i2)') + $
                    '_ca_'+yearstr+'_pp_mkadf11.dat', /remove_all)
  ;-----------------------------------------------------------------------
  ; now, I want to generate the new adf42 files
  ;-----------------------------------------------------------------------

  if not keyword_set(ca_only) then begin
   
    read_adf42, file=wkdir+'/'+adf42_ic_start, fulldata=fd
    
    ; change the values in the structure
    
    fd2={dsn04    : fd.dsn04    , $
         dsn18    : fd.dsn18    , $
         dsn35    : fd.dsn35    , $
         dsn15    : 'NULL'    , $
         dsn40    : 'NULL'    , $
         dsn11    : adf11_ic_start    , $
         dsn11f   : fd.dsn11f   , $
         element  : fd.element  , $
         z0       : fd.z0       , $
         z1       : fd.z1       , $
         ip       : fd.ip       , $
         lnorm    : fd.lnorm    , $
         nmet     : fd.nmet     , $
         imetr    : fd.imetr    , $
         liosel   : fd.liosel   , $
         lhsel    : fd.lhsel    , $
         lrsel    : fd.lrsel    , $
         lisel    : fd.lisel    , $
         lnsel    : fd.lnsel    , $
         lpsel    : fd.lpsel    , $
         zeff     : fd.zeff     , $
         lmetr    : fd.lmetr    , $
         ltscl    : fd.ltscl    , $
         ldscl    : fd.ldscl    , $
         lbrdi    : fd.lbrdi    , $
         amin     : fd.amin     , $
         numte    : n_elements(plasma.theta)    , $
         numtion  : fd.numtion  , $
         numdens  : n_elements(plasma.rho)  , $
         numdion  : fd.numdion  , $
         numwvl   : fd.numwvl   , $
         te       : plasma.theta      , $
         tion     : fd.tion     , $
         th       : fd.th       , $
         dens     : plasma.rho     , $
         dion     : fd.dion     , $
         npix     : plasma.npix     , $
         wvmin    : plasma.wvlmin    , $
         wvmax    : plasma.wvlmax    }


    ; write the adf42
    
    write_adf42, outfile=wkdir+'/'+adf42_ic_end, fulldata=fd2
    
    
    read_adf42, file=wkdir+'/'+adf42_ls_start, fulldata=fd
    
    ; change the values in the structure
    
    fd2={dsn04    : fd.dsn04    , $
         dsn18    : fd.dsn18    , $
         dsn35    : fd.dsn35    , $
         dsn15    : 'NULL'    , $
         dsn40    : 'NULL'    , $
         dsn11    : adf11_ls_start    , $
         dsn11f   : fd.dsn11f   , $
         element  : fd.element  , $
         z0       : fd.z0       , $
         z1       : fd.z1       , $
         ip       : fd.ip       , $
         lnorm    : fd.lnorm    , $
         nmet     : fd.nmet     , $
         imetr    : fd.imetr    , $
         liosel   : fd.liosel   , $
         lhsel    : fd.lhsel    , $
         lrsel    : fd.lrsel    , $
         lisel    : fd.lisel    , $
         lnsel    : fd.lnsel    , $
         lpsel    : fd.lpsel    , $
         zeff     : fd.zeff     , $
         lmetr    : fd.lmetr    , $
         ltscl    : fd.ltscl    , $
         ldscl    : fd.ldscl    , $
         lbrdi    : fd.lbrdi    , $
         amin     : fd.amin     , $
         numte    : n_elements(plasma.theta)    , $
         numtion  : fd.numtion  , $
         numdens  : n_elements(plasma.rho)  , $
         numdion  : fd.numdion  , $
         numwvl   : fd.numwvl   , $
         te       : plasma.theta      , $
         tion     : fd.tion     , $
         th       : fd.th       , $
         dens     : plasma.rho     , $
         dion     : fd.dion     , $
         npix     : plasma.npix     , $
         wvmin    : plasma.wvlmin    , $
         wvmax    : plasma.wvlmax    }
   
    ; write the adf42
   
    write_adf42, outfile=wkdir+'/'+adf42_ls_end, fulldata=fd2
  endif
  
  read_adf42, file=wkdir+'/'+adf42_ca_start, fulldata=fd
  
  ; change the values in the structure
  fd2={dsn04    : adf04_ca_end, $
       dsn18    : fd.dsn18    , $
       dsn35    : fd.dsn35    , $
       dsn15    : 'NULL'    , $
       dsn40    : 'NULL'    , $
         dsn11    : adf11_ca_start    , $
         dsn11f   : fd.dsn11f   , $
         element  : fd.element  , $
         z0       : fd.z0       , $
         z1       : fd.z1       , $
         ip       : fd.ip       , $
         lnorm    : fd.lnorm    , $
         nmet     : fd.nmet     , $
         imetr    : fd.imetr    , $
         liosel   : fd.liosel   , $
         lhsel    : fd.lhsel    , $
         lrsel    : fd.lrsel    , $
         lisel    : fd.lisel    , $
         lnsel    : fd.lnsel    , $
         lpsel    : fd.lpsel    , $
         zeff     : fd.zeff     , $
         lmetr    : fd.lmetr    , $
         ltscl    : fd.ltscl    , $
         ldscl    : fd.ldscl    , $
         lbrdi    : fd.lbrdi    , $
         amin     : fd.amin     , $
         numte    : n_elements(plasma.theta)    , $
         numtion  : fd.numtion  , $
         numdens  : n_elements(plasma.rho)  , $
         numdion  : fd.numdion  , $
         numwvl   : fd.numwvl   , $
         te       : plasma.theta      , $
         tion     : fd.tion     , $
         th       : fd.th       , $
         dens     : plasma.rho     , $
         dion     : fd.dion     , $
         npix     : plasma.npix     , $
         wvmin    : plasma.wvlmin    , $
         wvmax    : plasma.wvlmax    }
  ; write the adf42

  write_adf42, outfile=wkdir+'/'+adf42_ca_end, fulldata=fd2
  ;-----------------------------------------------------------------------
  ; create some scripts...
  ;-----------------------------------------------------------------------
  
  today=(xxdate())[0]
  
  
  ; generate pp files
  if not keyword_set(ca_only) then begin
  
    openw, lun, wkdir+'/'+adf42_ic_pp_start,/get_lun
    printf, lun, adf42_ic_end
    adf42_ic_end_paper=adf42_ic_end
    strput,adf42_ic_end_paper,'.txt', strpos(adf42_ic_end_paper,'.dat')
    printf,lun, adf42_ic_end_paper
    printf,lun,today
    close, lun
    free_lun, lun
    
    openw, lun, wkdir+'/'+adf42_ls_pp_start,/get_lun
    printf, lun, adf42_ls_end
    adf42_ls_end_paper=adf42_ls_end
    strput,adf42_ls_end_paper,'.txt', strpos(adf42_ls_end_paper,'.dat')
    printf,lun, adf42_ls_end_paper
    printf,lun,today
    close, lun
    free_lun, lun
  endif
  openw, lun, wkdir+'/'+adf42_ca_pp_start,/get_lun
  printf, lun, adf42_ca_end
  adf42_ca_end_paper=adf42_ca_end
  strput,adf42_ca_end_paper,'.txt', strpos(adf42_ca_end_paper,'.dat')
  printf,lun, adf42_ca_end_paper
  printf,lun,today
  close, lun
  free_lun, lun
  
  if not keyword_set(ca_only) then begin

    script_ic_file = strcompress(elsymb + string(z_ion,format='(i2)') $
                     + '_mkadf11_ic_810_script', /remove_all)
                                
    script_ls_file = strcompress(elsymb + string(z_ion,format='(i2)') $
                     + '_mkadf11_ls_810_script', /remove_all)
  endif
  script_ca_file = strcompress(elsymb + string(z_ion,format='(i2)') $
                   + '_mkadf11_'+lstring+'ca_810_script', /remove_all)
;-----------------------------------------------------------------------
; IC coupling script
;-----------------------------------------------------------------------
  loc_ksh =file_which(getenv('PATH'),'ksh')
  adaspath=getenv('ADASCENT')+'/../'
  scriptdir=wkdir+'/scripts'
  spawn, 'mkdir -p '+scriptdir
  scriptdir=scriptdir+'/'+elname
  spawn, 'mkdir -p '+scriptdir
  
  if not keyword_set(ca_only) then begin

    openw, lun, scriptdir+'/'+script_ic_file, /get_lun
    
    printf, lun, '#!' + loc_ksh
    printf, lun, ' '
    printf, lun, 'date'
    printf, lun, ' '
    printf, lun, 'uname -a'
    printf, lun, ' '
    printf, lun, 'cp ' + wkdir + '/' + adf42_ic_end + ' /tmp/.'
    printf, lun, 'cp ' + wkdir + '/' + adf42_ic_pp_start + ' /tmp/'+ $
                 adf42_ic_pp_end
    
    printf, lun, 'if [ -e '+wkdir+'/'+adf04_ic_start +' ]'
    printf, lun, ' then'
    printf, lun, '  cp ' + wkdir + '/' + adf04_ic_start + ' /tmp/'+$
                       adf04_ic_end
    printf, lun, 'else'
    printf, lun, ' if [ -e '+wkdir+'/'+adf04_ic_start+'.bz2 ]'
    printf, lun, '  then'
    printf, lun, '  cp ' + wkdir + '/' + adf04_ic_start+'.bz2' + $
                 ' /tmp/'+adf04_ic_end+'.bz2'
    printf, lun, '  bunzip2 -f /tmp/'+ adf04_ic_end+'.bz2'
    printf, lun, ' fi'
    printf, lun, 'fi'
    
    printf, lun, 'cd /tmp/'
    printf, lun, adaspath+ 'offline_adas/adas8#1/scripts/run_adas810 ' + $
                 '/tmp/' + adf42_ic_pp_end
    ;printf, lun, 'mv /tmp/' + adf15_ic_file + ' ' + wkdir + '/.'
    ;printf, lun, 'mv /tmp/' + adf40_ic_file + ' ' + wkdir + '/.'
    printf, lun, 'mv /tmp/' + adf11_ic_start + ' ' + wkdir + '/.'
    printf, lun, 'date'
    
    if keyword_set(archived) then begin 
    
      printf, lun, 'mv ' + wkdir + '/' +  adf11_ic_start + ' ' +  $
              wkdir + '/' + adf11_ic_end
    
    endif
; cleanup
    printf, lun, 'rm /tmp/'+adf04_ic_end    
    printf, lun, 'rm /tmp/' + adf42_ic_end
    printf, lun, 'rm /tmp/' + adf42_ic_pp_end
    
    free_lun, lun
    
    cmd = 'chmod +x ' + scriptdir+'/'+script_ic_file
    spawn, cmd
    
;-----------------------------------------------------------------------
; LS coupling script
;-----------------------------------------------------------------------

    openw, lun, scriptdir+'/'+script_ls_file, /get_lun
    
    printf, lun, '#!' + loc_ksh
    printf, lun, ' '
    printf, lun, 'date'
    printf, lun, ' '
    printf, lun, 'uname -a'
    printf, lun, ' '
    printf, lun, 'cp ' + wkdir + '/' + adf42_ls_end + ' /tmp/.'
    printf, lun, 'cp ' + wkdir + '/' + adf42_ls_pp_start + ' /tmp/'+ $
                 adf42_ls_pp_end
    
    printf, lun, 'if [ -e '+wkdir+'/'+adf04_ls_start +' ]'
    printf, lun, ' then'
    printf, lun, '  cp ' + wkdir + '/' + adf04_ls_start + ' /tmp/'+$
                       adf04_ls_end
    printf, lun, 'else'
    printf, lun, ' if [ -e '+wkdir+'/'+adf04_ls_start+'.bz2 ]'
    printf, lun, '  then'
    printf, lun, '  cp ' + wkdir + '/' + adf04_ls_start+'.bz2' + $
                 ' /tmp/'+adf04_ls_end+'.bz2'
    printf, lun, '  bunzip2 -f /tmp/'+ adf04_ls_end+'.bz2'
    printf, lun, ' fi'
    printf, lun, 'fi'
    
    printf, lun, 'cd /tmp/'
    printf, lun, adaspath+ 'offline_adas/adas8#1/scripts/run_adas810 ' + $
                 '/tmp/' + adf42_ls_pp_end
    ;printf, lun, 'mv /tmp/' + adf15_ls_file + ' ' + wkdir + '/.'
    ;printf, lun, 'mv /tmp/' + adf40_ls_file + ' ' + wkdir + '/.'
    printf, lun, 'mv /tmp/' + adf11_ls_start + ' ' + wkdir + '/.'
    printf, lun, 'date'
    
    if keyword_set(archived) then begin 
    
      printf, lun, 'mv ' + wkdir + '/' +  adf11_ls_start + ' ' +  $
              wkdir + '/' + adf11_ls_end
    
    endif
; cleanup
    printf, lun, 'rm /tmp/'+adf04_ls_end    
    printf, lun, 'rm /tmp/' + adf42_ls_end
    printf, lun, 'rm /tmp/' + adf42_ls_pp_end
    
    free_lun, lun
    
    cmd = 'chmod +x ' + scriptdir+'/'+script_ls_file
    spawn, cmd
    
  endif
;-----------------------------------------------------------------------
;  CA coupling script
;-----------------------------------------------------------------------
  
  openw, lun, scriptdir+'/'+script_ca_file, /get_lun
  
  printf, lun, '#!' + loc_ksh
  printf, lun, ' '
  printf, lun, 'date'
  printf, lun, ' '
  printf, lun, 'uname -a'
  printf, lun, ' '
  printf, lun, 'cp ' + wkdir + '/' + adf42_ca_end + ' /tmp/.'
  printf, lun, 'cp ' + wkdir + '/' + adf42_ca_pp_start + ' /tmp/'+ $
               adf42_ca_pp_end
  
  printf, lun, 'if [ -e '+wkdir+'/'+adf04_ca_start +' ]'
  printf, lun, ' then'
  printf, lun, '  cp ' + wkdir + '/' + adf04_ca_start + ' /tmp/'+$
                     adf04_ca_end
  printf, lun, 'else'
  printf, lun, ' if [ -e '+wkdir+'/'+adf04_ca_start+'.bz2 ]'
  printf, lun, '  then'
  printf, lun, '  cp ' + wkdir + '/' + adf04_ca_start+'.bz2' + $
               ' /tmp/'+adf04_ca_end+'.bz2'
  printf, lun, '  bunzip2 -f /tmp/'+ adf04_ca_end+'.bz2'
  printf, lun, ' fi'
  printf, lun, 'fi'
  
  printf, lun, 'cd /tmp/'
  printf, lun, adaspath+ 'offline_adas/adas8#1/scripts/run_adas810 ' + $
               '/tmp/' + adf42_ca_pp_end
  ;printf, lun, 'mv /tmp/' + adf15_ca_file + ' ' + wkdir + '/.'
  ;printf, lun, 'mv /tmp/' + adf40_ca_file + ' ' + wkdir + '/.'
  printf, lun, 'mv /tmp/' + adf11_ca_start + ' ' + wkdir + '/.'
  printf, lun, 'date'
  
  if keyword_set(archived) then begin 
  
    printf, lun, 'mv ' + wkdir + '/' +  adf11_ca_start + ' ' +  $
            wkdir + '/' + adf11_ca_end
  
  endif
; cleanup
  printf, lun, 'rm /tmp/'+adf04_ca_end    
  printf, lun, 'rm /tmp/' + adf42_ca_end
  printf, lun, 'rm /tmp/' + adf42_ca_pp_end
  
  free_lun, lun
  
  cmd = 'chmod +x ' + scriptdir+'/'+script_ca_file
  spawn, cmd
  
endfor  


END



PRO adas8xx_opt_prep_make_adf11, normdir    = normdir, $
                                 largedir   = largedir, $
                                 z_nuc    = z_nuc, $
                                 year     = year, $
                                 plasma   = plasma, $
                                 archived = archived
                                 
if not keyword_set(year) then begin
  print, 'adas8xx_opt_prep_make_adf11.pro warning: keyowrd year not '+$
         'set, using default value of 40'
  year = 40
endif
yearstr=strcompress(string(year),/remove_all)

if arg_present(archive) and not keyword_set(archived) then archived= 1

run_adas8xx_opt_prep_make_adf11, wkdir    = normdir, $
                             archived = archived, $
                             z_nuc    = z_nuc, $
                             ca_only  = 0, $
                             year     = year, $
                             plasma   = plasma
                                     
run_adas8xx_opt_prep_make_adf11, wkdir    = largedir, $
                             archived = archived, $
                             z_nuc    = z_nuc, $
                             ca_only  = 1, $
                             year     = year, $
                             plasma   = plasma, $
                             /large

end
