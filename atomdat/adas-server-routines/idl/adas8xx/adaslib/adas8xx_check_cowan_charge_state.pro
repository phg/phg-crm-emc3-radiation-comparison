;-----------------------------------------------------------------------------
;+
;
; NAME     : adas8xx_check_cowan_charge_state
;
; PURPOSE  : checks the rcn output from Cowan structure run to see if 
;            the determined number of electrons is correct.  Fault 
;            generating configurations are identified. 
;
; CATEGORY : ADAS
;
; NOTES    :
;  
; USE      : 
;
; INPUT    :
;(I*4) z0 = nuclear charge of ion
;(I*4) z1 = ion charge
;(C* ) rcn_file = Cowan rcn file for examination.
;(I*4) lun_verb = logical unit number for printing diagnostic info
;
; OUTPUTS   :
;
; CATEGORY:
;       Adas system.
;
; WRITTEN:
;       Hugh Summers
;
; MODIFIED:
;       1.1     Hugh Summers
;
;       1.2     Adam Foster
;               - Updated comment structure
;               - changed tabs to spaces
;
; VERSION:
;       1.1   07-07-06
;       1.2   19-03-09
; 
;-
;-----------------------------------------------------------------------------
PRO adas8xx_check_cowan_charge_state,z0=z0, $
                                     z1=z1, $
                                     rcn_file=rcn_file, $
                                     lun_verb=lun_verb
                                     

      if keyword_set(rcn_file) and keyword_set(z0) and keyword_set(z1) then begin
      
          res = file_info(rcn_file)
          
          if res.exists eq 1 then begin
             test_str='.                                    '
             adas_readfile, file=rcn_file, all=all
             ind=where(strpos(all,test_str) ne -1, count)
             if count gt 0 and count mod 2 eq 0 then begin 
                 ind=ind(indgen(count/2)*2)
                 nel=float(strmid(all(ind),6,3))
                  ind=where(nel ne z0-z1,count)
                 if count gt 0 then begin
             
                   if keyword_set(lun_verb) then begin  
                       printf, lun_verb,'*** Cowan rcn error: incorrect configuration charges for z0 =',z0,' and z =',z1
                       for i =0, count-1 do begin
                         printf, lun_verb,'      configuration index', ind[i]
                       end
                   endif  
                                
                  endif
          
              endif else begin
                   
                if keyword_set(lun_verb) then begin  
                  printf, lun_verb,'*** Cowan rcn error: rcn file is empty'
                endif
             endelse          
          
          endif else begin
            if keyword_set(lun_verb) then begin  
              printf, lun_verb,'*** Cowan rcn error: no rcn file present'
            endif
          endelse
      

      endif else begin
            if keyword_set(lun_verb) then begin
              printf, lun_verb,'*** adas8xx_check_cowan_charge_state: keywords not set'
            endif
      endelse
      
END      
 

 
