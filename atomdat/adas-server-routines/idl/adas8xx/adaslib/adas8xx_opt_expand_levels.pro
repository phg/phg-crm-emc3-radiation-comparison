;-----------------------------------------------------------------------------
;+
;
; NAME     : adas8xx_opt_expand_levels
;
; PURPOSE  : To expand the promotion rules
;
; CATEGORY : ADAS
;
; NOTES    :
;  
; USE      : 
;
; INPUT    :
; (I*4) z0_nuc           = nuclear charge
; (I*4) z_ion            = ion charge
; (C* ) config           = ground configuration string
; (R*8) ionpot           = ionisation potential
; (STRUCT) prom_rules  
;   (I*4) no_v_shl         = number of valence shells
;   (I*4) max_dn_v1        = max dn for v1
;   (I*4) min_dn_v1        = min dn for v1
;   (I*4) max_dl_v1        = max dl for v1
;   (I*4) min_dl_v1        = min dl for v1
;   (I*4) max_dn_v2        = max dn for v2
;   (I*4) min_dn_v2        = min dn for v2
;   (I*4) max_dl_v2        = max dl for v2
;   (I*4) min_dl_v2        = min dl for v2
;   (I*4) prom_cl          = 1/0 if prom_cl is allowed
;   (I*4) max_n_cl         = max n for cl
;   (I*4) min_n_cl         = min n for cl
;   (I*4) max_l_cl         = max l for cl
;   (I*4) min_l_cl         = min l for cl
;   (I*4) max_dn_cl        = max dn for cl
;   (I*4) min_dn_cl        = min dn for cl
;   (I*4) max_dl_cl        = max dl for cl
;   (I*4) min_dl_cl        = min dl for cl
;   (I*4) fill_n_v1        = fill all nl of v1
;   (I*4) fill_par         = fill only with opposite parity to v1
;   (I*4) for_tr_sel       = Cowan option for radiative transitions
;   (I*4) last_4f          = shift an electron valence shell to unfilled 4f as
;                            extra ground
;   (I*4) grd_cmplx        = include ground complex
; (I*4) forcechange      = integer defining which rules change to try
; (STRUCT) base_rules    = output from adas8xx_opt_initialise_rules
; (I*4) n_cl_promotions  = switch, if set then force an expansion of closed
;                          shells
; (I*4) two_configs      = switch, denoting if a dipole has to be forced
;
; OUTPUT   :
; (I*4) done             = 1 if a change has been made, 0 if change was not
;                          possible
; struct final_rules     = new values for all of the promotion rules
; (C* ) message          = record of what was changed
; (I*4) last_case        = 1 if rule change was last one in the list.


; CATEGORY:
;       Adas system.
;
; WRITTEN:
;       Adam Foster, University of Strathclyde,08-01-08
;
; MODIFIED:
;       1.1     Adam Foster
;                - Initial version
;
;       1.2     Adam Foster
;                - Moved last case flag to correct position.
;
; VERSION:
;       1.1   08-01-08
; 
;-
;-----------------------------------------------------------------------------

PRO adas8xx_opt_expand_levels, z0_nuc   = z0_nuc,                      $
                           z_ion        = z_ion ,                      $
                           config       = config ,                     $
                           ionpot       = ionpot ,                     $
                           prom_rules   = prom_rules,                  $
                           done         = done,                        $ 
                           final_rules  = final_rules,                 $
                           forcechange  = forcechange ,                $
                           base_rules   = base_rules, $
                           n_cl_promotions = n_cl_promotions, $
                           message      = message, $
                           two_configs  = two_configs, $
                           last_case    = last_case
                           
                           
; All inputs are inherited from adas8xx_promotion

no_v_shl  =[prom_rules.no_v_shl  ]
max_dn_v1 =[prom_rules.max_dn_v1 ]
min_dn_v1 =[prom_rules.min_dn_v1 ]
max_dl_v1 =[prom_rules.max_dl_v1 ]
min_dl_v1 =[prom_rules.min_dl_v1 ]
max_dn_v2 =[prom_rules.max_dn_v2 ]
min_dn_v2 =[prom_rules.min_dn_v2 ]
max_dl_v2 =[prom_rules.max_dl_v2 ]
min_dl_v2 =[prom_rules.min_dl_v2 ]
prom_cl   =[prom_rules.prom_cl   ]
max_n_cl  =[prom_rules.max_n_cl  ]
min_n_cl  =[prom_rules.min_n_cl  ]
max_l_cl  =[prom_rules.max_l_cl  ]
min_l_cl  =[prom_rules.min_l_cl  ]
max_dn_cl =[prom_rules.max_dn_cl ]
min_dn_cl =[prom_rules.min_dn_cl ]
max_dl_cl =[prom_rules.max_dl_cl ]
min_dl_cl =[prom_rules.min_dl_cl ]
fill_n_v1 =[prom_rules.fill_n_v1 ]
fill_par  =[prom_rules.fill_par  ]
for_tr_sel=[prom_rules.for_tr_sel]
last_4f   =[prom_rules.last_4f   ]
grd_cmplx =[prom_rules.grd_cmplx ]


done=0
last_case=0

; copy all the promotion rules.

no_v_shl  =[no_v_shl  ,no_v_shl  ]
max_dn_v1 =[max_dn_v1 ,max_dn_v1 ]
min_dn_v1 =[min_dn_v1 ,min_dn_v1 ]
max_dl_v1 =[max_dl_v1 ,max_dl_v1 ]
min_dl_v1 =[min_dl_v1 ,min_dl_v1 ]
max_dn_v2 =[max_dn_v2 ,max_dn_v2 ]
min_dn_v2 =[min_dn_v2 ,min_dn_v2 ]
max_dl_v2 =[max_dl_v2 ,max_dl_v2 ]
min_dl_v2 =[min_dl_v2 ,min_dl_v2 ]
prom_cl   =[prom_cl   ,prom_cl   ]
max_n_cl  =[max_n_cl  ,max_n_cl  ]
min_n_cl  =[min_n_cl  ,min_n_cl  ]
max_l_cl  =[max_l_cl  ,max_l_cl  ]
min_l_cl  =[min_l_cl  ,min_l_cl  ]
max_dn_cl =[max_dn_cl ,max_dn_cl ]
min_dn_cl =[min_dn_cl ,min_dn_cl ]
max_dl_cl =[max_dl_cl ,max_dl_cl ]
min_dl_cl =[min_dl_cl ,min_dl_cl ]
fill_n_v1 =[fill_n_v1 ,fill_n_v1 ]
fill_par  =[fill_par  ,fill_par  ]
for_tr_sel=[for_tr_sel,for_tr_sel]
last_4f   =[last_4f   ,last_4f   ]
grd_cmplx =[grd_cmplx ,grd_cmplx ]
;

;-----------------------------------------------------------------------
; Start changing the promotion rules, as dictated by forcechange
; First, expand dl to fill existing n shells. If this has been done, add
; an n shell.
;-----------------------------------------------------------------------
IF not keyword_set(forcechange) then forcechange=0
IF not keyword_set(n_cl_promotions) then n_cl_promotions=0

message=STRCOMPRESS('DEFAULT MESSAGE: forcechange='+STRING(forcechange)+$
        ', n_cl_promotions='+STRING(n_cl_promotions))

IF keyword_set(forcechange) THEN BEGIN

  IF keyword_set(two_configs) THEN BEGIN

    CASE (forcechange) OF
      1: BEGIN
         ; dl_v1=-1, dn_v1 =0
         
         ; check that there are empty l shells below this one
           
           IF (base_rules.iv1 EQ 0) THEN BEGIN
             message=STRCOMPRESS('Skipping dl_v1=-1, dn_v1 =0')
             done =0
           ENDIF ELSE IF ((base_rules.v1_l EQ 0) OR $
               (base_rules.occupied[base_rules.iv1-1] EQ 2)) THEN BEGIN
             message=STRCOMPRESS('Skipping dl_v1=-1, dn_v1 =0')
             done =0
;             stop
           ENDIF ELSE BEGIN
             message=STRCOMPRESS('Running dl_v1=-1, dn_v1 =0')
             max_dl_v1[1]=-1  
             min_dl_v1[1]=-1  
             max_dn_v1[1]=0  
             min_dn_v1[1]=0  
             done =1
           ENDELSE
         END

      2: BEGIN
         ; dl_v1=-1, dn_v1 =1
         
         ; check that there are empty l shells below this one
         
           IF ((base_rules.v1_l EQ 0)) THEN BEGIN
             message=STRCOMPRESS('Skipping dl_v1=-1, dn_v1 =1')
             done =0
;             stop
           ENDIF ELSE BEGIN
             message=STRCOMPRESS('Running dl_v1=-1, dn_v1 =1')
             max_dl_v1[1]=-1  
             min_dl_v1[1]=-1  
             max_dn_v1[1]=1  
             min_dn_v1[1]=1 
             done =1
           ENDELSE
         END
      3: BEGIN
         ; dl_v1=1, dn_v1 =0
         
         ; check that there are empty l shells below this one
         
           IF ((base_rules.v1_l EQ base_rules.v1_n-1)) THEN BEGIN
             message=STRCOMPRESS('Skipping dl_v1=1, dn_v1 =0')
             done =0
;             stop
           ENDIF ELSE BEGIN
             message=STRCOMPRESS('Running dl_v1=1, dn_v1 =0')
             max_dl_v1[1]=1  
             min_dl_v1[1]=1  
             max_dn_v1[1]=0  
             min_dn_v1[1]=0 
             done =1 
           ENDELSE
         END
      4: BEGIN
         ; dl_v1=1, dn_v1 =1
         
         ; check that there are empty l shells below this one
;           stop
           message=STRCOMPRESS('Running dl_v1=1, dn_v1 =1')
           max_dl_v1[1]=1
           min_dl_v1[1]=1
           max_dn_v1[1]=1
           min_dn_v1[1]=1
           done =1
         END
      5: BEGIN
         ; dl_v2=-1, dn_v2 =0
         
         ; check that there are empty l shells below this one
           IF ((no_v_shl[1] EQ 1) OR $
               (base_rules.v2_l EQ 0)) THEN BEGIN
             message=STRCOMPRESS('Skipping dl_v2=-1, dn_v1 =0')
             done =0
;             stop
           ENDIF ELSE IF $
                 (base_rules.occupied[base_rules.iv2-1] EQ 2) THEN BEGIN
             message=STRCOMPRESS('Skipping dl_v2=-1, dn_v1 =0')
             done =0
;             stop
               
           ENDIF ELSE BEGIN
             message=STRCOMPRESS('Running dl_v2=-1, dn_v2 =0')
             max_dl_v2[1]=-1
             min_dl_v2[1]=-1
             max_dn_v2[1]=0
             min_dn_v2[1]=0
             done =1
           ENDELSE
         END
      6: BEGIN
         ; dl_v2=-1, dn_v2 =1
         
         ; check that there are empty l shells below this one
           IF ((no_v_shl[1] EQ 1) OR $
               (base_rules.v2_l EQ 0)) THEN BEGIN
             message=STRCOMPRESS('Skipping dl_v2=-1, dn_v1 =1')
             done =0
;             stop
           ENDIF ELSE BEGIN
             message=STRCOMPRESS('Running dl_v2=-1, dn_v2 =1')
             max_dl_v2[1]=-1
             min_dl_v2[1]=-1
             max_dn_v2[1]=1
             min_dn_v2[1]=1
             done =1
           ENDELSE
         END
      7: BEGIN
         ; dl_v2=1, dn_v2 =0
         
         ; check that there are empty l shells below this one
           IF ((no_v_shl[1] EQ 1) OR $
               (base_rules.v2_l EQ base_rules.v2_n-1) OR $
               (base_rules.occupied[base_rules.iv2+1] EQ 2)) THEN BEGIN
             message=STRCOMPRESS('Skipping dl_v2=1, dn_v1 =0')
             done =0
;             stop
           ENDIF ELSE BEGIN
             message=STRCOMPRESS('Running dl_v2=1, dn_v2 =0')
             max_dl_v2[1]=1
             min_dl_v2[1]=1
             max_dn_v2[1]=0
             min_dn_v2[1]=0
             done =1
           ENDELSE
         END
      8: BEGIN
         ; dl_v2=-1, dn_v2 =1
         
         ; check that there are empty l shells below this one
           IF ((no_v_shl[1] EQ 1)) THEN BEGIN
             message=STRCOMPRESS('Skipping dl_v2=1, dn_v1 =1')
             done =0
;             stop
           ENDIF ELSE BEGIN
             message=STRCOMPRESS('Running dl_v2=1, dn_v2 =1')
             max_dl_v2[1]=1
             min_dl_v2[1]=1
             max_dn_v2[1]=1
             min_dn_v2[1]=1
             done =1
           ENDELSE
         END
      9: BEGIN
         ; dl_cl=-1, dn_cl =0
         
         ; check that there are empty l shells below this one
           
           IF (base_rules.icl LE 0) THEN BEGIN
             message=STRCOMPRESS('Skipping dl_cl=-1, dn_v1 =0')
             done =0
           ENDIF ELSE IF ((prom_cl[1] EQ 0) OR $
               (base_rules.cl_l EQ 0) OR $
               (base_rules.occupied[base_rules.icl-1] EQ 2)) THEN BEGIN
             message=STRCOMPRESS('Skipping dl_cl=-1, dn_v1 =0')
             done =0
;             stop
           ENDIF ELSE BEGIN
             message=STRCOMPRESS('Running dl_cl=-1, dn_cl =0')
             max_dl_cl[1]=-1
             min_dl_cl[1]=-1
             max_dn_cl[1]=0
             min_dn_cl[1]=0
             done =1
           ENDELSE
         END
      10:BEGIN
         ; dl_cl=-1, dn_cl =1
         
         ; check that there are empty l shells below this one
           IF ((prom_cl[1] EQ 0) OR $
               (base_rules.cl_l EQ 0)) THEN BEGIN
             message=STRCOMPRESS('Skipping dl_cl=-1, dn_v1 =1')
             done =0
;             stop
           ENDIF ELSE BEGIN
             message=STRCOMPRESS('Running dl_cl=-1, dn_cl =1')
             max_dl_cl[1]=-1
             min_dl_cl[1]=-1
             max_dn_cl[1]=1
             min_dn_cl[1]=1
             done =1
           ENDELSE
         END
      11:BEGIN
         ; dl_cl=1, dn_cl =0
         
         ; check that there are empty l shells below this one
           IF ((prom_cl[1] EQ 0) OR $
               (base_rules.cl_l EQ base_rules.cl_n-1)) THEN BEGIN
             message=STRCOMPRESS('Skipping dl_cl=1, dn_v1 =0')
             done =0
;             stop
           ENDIF ELSE BEGIN
             message=STRCOMPRESS('Running dl_cl=1, dn_cl =0')
             max_dl_cl[1]=1
             min_dl_cl[1]=1
             max_dn_cl[1]=0
             min_dn_cl[1]=0
             done =1
           ENDELSE
         END
      12:BEGIN
         ; dl_cl=-1, dn_cl =1
         
         ; check that there are empty l shells below this one
           IF ((prom_cl[1] EQ 0)) THEN BEGIN
             message=STRCOMPRESS('Skipping dl_cl=1, dn_v1 =1')
             done =0
;             stop
           ENDIF ELSE BEGIN
             message=STRCOMPRESS('Running dl_cl=1, dn_cl =1')
             max_dl_cl[1]=1
             min_dl_cl[1]=1
             max_dn_cl[1]=1
             min_dn_cl[1]=1
             done =1
           ENDELSE
           
         END


     13: BEGIN
; this rule will introduce fill_n_v1 and fill_par
           IF fill_n_v1[0] NE 1 THEN BEGIN
             fill_n_v1[1] = 1
             fill_par[1] = 1
             message= STRCOMPRESS('Change in fill_n_v1:'+$
                      STRING(fill_n_v1[0])+' -> '+STRING(fill_n_v1[1])+ $
                      '; change in fill_par:'+ STRING(fill_par[0])+' -> '+$
                      STRING(fill_par[1]))
             done = 1         
           ENDIF ELSE BEGIN
             message= STRCOMPRESS('No change to fill_n_v1: already set')
             done=0  
           ENDELSE
         END

     14: BEGIN
           IF fill_n_v1[0] NE 1 THEN BEGIN
             fill_n_v1[1] = 1
             fill_par[1] = 0
             message= STRCOMPRESS('Change in fill_n_v1:'+$
                      STRING(fill_n_v1[0])+' -> '+STRING(fill_n_v1[1])+ $
                      '; change in fill_par:'+ STRING(fill_par[0])+' -> '+$
                      STRING(fill_par[1]))
             done = 1
           ENDIF ELSE BEGIN
             IF fill_par[0] EQ 1 THEN BEGIN
               fill_par[1] = 0
               message= STRCOMPRESS('Change in fill_n_v1:'+$
                        STRING(fill_n_v1[0])+' -> '+STRING(fill_n_v1[1])+ $
                        '; change in fill_par:'+ STRING(fill_par[0])+' -> '+$
                        STRING(fill_par[1]))
               done = 1
             ENDIF ELSE BEGIN
               message=STRCOMPRESS('No change to fill_n_v1: already set')
             ENDELSE
           ENDELSE

           last_case=1
         END


    ENDCASE

  ENDIF ELSE BEGIN

;-----------------------------------------------------------------------
; For 1s and 2s configurations, there is a need to kick start the
; promotions, otherwise they crash later on (code assumes there are some
; dipole transitions!)
; So increase dl for these configs if it is 0
;-----------------------------------------------------------------------
  
    IF ((base_rules.v1_n LE 2) AND (base_rules.v1_l+max_dl_v1[1] EQ 0)) $
        THEN BEGIN
      max_dl_v1[1]=max_dl_v1[1]+1
    ENDIF
    
    CASE (forcechange) OF
      1: BEGIN
           IF max_dn_v1[1] GE 5 THEN BEGIN
             message=STRCOMPRESS('Not changing max_dn_v1: too large'+$
                   ' max_dn_v1= '+ STRING(max_dn_v1[1])+$
                   ', upper limit is 5)')
             done =0
           ENDIF ELSE IF max_dn_v1[1]+base_rules.v1_n LE 7 THEN BEGIN
             max_dn_v1[1]=max_dn_v1[1]+1
             message=STRCOMPRESS('Change in max_dn_v1: max_dn_v1= '+$
                    STRING(max_dn_v1[0])+' -> '+STRING(max_dn_v1[1]))
             
             done =1       
           ENDIF ELSE BEGIN
             message=STRCOMPRESS('Not changing max_dn_v1: too large'+$
                   ' max_dn_v1+v1_n='+ STRING(max_dn_v1[1]+$
                     base_rules.v1_n))
             done =0
           ENDELSE
         END
      2: BEGIN
           tmp=where(base_rules.occupied NE 2)
           IF tmp[0] NE -1 THEN BEGIN
             ntmp=min(base_rules.n[tmp])
             IF min_dn_v1[1]+base_rules.v1_n GT ntmp THEN BEGIN
               min_dn_v1[1]=min_dn_v1[1]-1
               message=STRCOMPRESS('Change in min_dn_v1: min_dn_v1= '+$
                      STRING(min_dn_v1[0])+' -> '+ STRING(min_dn_v1[1]))
               done = 1       
             ENDIF ELSE BEGIN
               message=STRCOMPRESS('Not changing min_dn_v1: too large'+$
                     ' min_dn_v1+v1_n='+ STRING(min_dn_v1[1]+$
                       base_rules.v1_n))
               done = 0
             ENDELSE
           ENDIF ELSE BEGIN
             message=STRCOMPRESS('Not changing min_dn_v1: no open ' +$
                                  'shells')
             done = 0
           ENDELSE  
         END
      3: BEGIN
           IF (max_dl_v1[1]+base_rules.v1_l LE $
              max_dn_v1[1]+base_rules.v1_n -1) THEN BEGIN

             max_dl_v1[1]=max_dl_v1[1]+1
             message=STRCOMPRESS('Change in max_dl_v1: max_dl_v1='+$
                    STRING(max_dl_v1[0])+' -> '+STRING(max_dl_v1[1]))
             
             done =1       
           ENDIF ELSE BEGIN
             message=STRCOMPRESS('Not changing max_dl_v1: too large'+$
                   ' max_dl_v1+v1_l='+ STRING(max_dl_v1[1]+$
                     base_rules.v1_l))
             done =0
           ENDELSE
         END
      4: BEGIN
           IF (min_dl_v1[1]+base_rules.v1_l GE 1) THEN BEGIN

             min_dl_v1[1]=min_dl_v1[1]-1
             message=STRCOMPRESS('Change in min_dl_v1: min_dl_v1='+$
                    STRING(min_dl_v1[0])+' -> '+STRING(min_dl_v1[1]))
             
             done =1       
           ENDIF ELSE BEGIN
             message=STRCOMPRESS('Not changing min_dl_v1: too large'+$
                   ' min_dl_v1+v1_l='+ STRING(min_dl_v1[1]+$
                     base_rules.v1_l))
             done =0
           ENDELSE
         END
      5: BEGIN
           IF no_v_shl[1] EQ 2 THEN BEGIN
             IF max_dn_v2[1]+base_rules.v2_n LE 6 THEN BEGIN
               max_dn_v2[1]=max_dn_v2[1]+1
               message=STRCOMPRESS('Change in max_dn_v2: max_dn_v2='+$
                      STRING(max_dn_v2[0])+' -> '+STRING(max_dn_v2[1]))
             
               done =1       
             ENDIF ELSE BEGIN
               message=STRCOMPRESS('Not changing max_dn_v2: too large'+$
                     ' max_dn_v2+v2_n='+ STRING(max_dn_v2[1]+$
                       base_rules.v2_n))
               done =0
             ENDELSE
           ENDIF ELSE BEGIN  
               message=STRCOMPRESS('Not changing max_dn_v2: no_v_shl=1')
               done =0
           ENDELSE         
         END
      6: BEGIN
           IF no_v_shl[1] EQ 2 THEN BEGIN
             tmp=where(base_rules.occupied NE 2)
             IF tmp[0] NE -1 THEN BEGIN
               ntmp=min(base_rules.n[tmp])
               IF min_dn_v2[1]+base_rules.v2_n GT ntmp THEN BEGIN
                 min_dn_v2[1]=min_dn_v2[1]-1
                 message=STRCOMPRESS('Change in min_dn_v2: min_dn_v2='+$
                        STRING(min_dn_v2[0])+' -> '+STRING(min_dn_v2[1]))
                 done = 1       
               ENDIF ELSE BEGIN
                 message=STRCOMPRESS('Not changing min_dn_v2: too large'+$
                       ' min_dn_v2+v2_n='+STRING(min_dn_v2[1]-$
                         base_rules.v2_n))
                 done = 0
               ENDELSE
               
             ENDIF ELSE BEGIN
               message=STRCOMPRESS('Not changing min_dn_v2: no open'+$
               ' shells')
               done = 0
             ENDELSE  
           ENDIF ELSE BEGIN  
               message=STRCOMPRESS('Not changing min_dn_v2: no_v_shl=1')
               done =0
           ENDELSE         
         END
      7: BEGIN
           IF no_v_shl[1] EQ 2 THEN BEGIN
             IF (max_dl_v2[1]+base_rules.v2_l LE $
                max_dn_v2[1]+base_rules.v2_n -1) THEN BEGIN

               max_dl_v2[1]=max_dl_v2[1]+1
               message=STRCOMPRESS('Change in max_dl_v2: max_dl_v2='+$
                      STRING(max_dl_v2[0])+' -> '+STRING(max_dl_v2[1]))
               
               done =1       
             ENDIF ELSE BEGIN
               message=STRCOMPRESS('Not changing max_dl_v2: too large'+$
                     ' max_dl_v2+v2_l='+ STRING(max_dl_v2[1]+ $
                       base_rules.v2_l))
               done =0
             ENDELSE
           ENDIF ELSE BEGIN  
               message=STRCOMPRESS('Not changing max_dn_v2: no_v_shl=1')
               done =0
           ENDELSE         
         END
      8: BEGIN
           IF no_v_shl[1] EQ 2 THEN BEGIN
             IF (min_dl_v2[1]+base_rules.v2_l GT 1) THEN BEGIN

               min_dl_v2[1]=min_dl_v2[1]+1
               message=STRCOMPRESS('Change in min_dl_v2: min_dl_v2='+$
                      STRING(min_dl_v2[0])+' -> '+STRING(min_dl_v2[1]))
               
               done =1       
             ENDIF ELSE BEGIN
               message=STRCOMPRESS('Not changing min_dl_v2: too large'+$
                     ' min_dl_v2+v2_l='+STRING(min_dl_v2[1]+$
                       base_rules.v2_l))
               done =0
             ENDELSE
           ENDIF ELSE BEGIN  
               message=STRCOMPRESS('Not changing min_dl_v2: no_v_shl=1')
               done =0
           ENDELSE         
         END
      9: BEGIN
           IF prom_cl[1] EQ 1 THEN BEGIN
                 
             IF max_dn_cl[1]+base_rules.cl_n LE 6 THEN BEGIN
               max_dn_cl[1]=max_dn_cl[1]+1
               message=STRCOMPRESS('Change in max_dn_cl: max_dn_cl='+$
                      STRING(max_dn_cl[0])+' -> '+STRING(max_dn_cl[1]))
               
               done =1       
             ENDIF ELSE BEGIN
               message=STRCOMPRESS('Not changing max_dn_cl: too large'+$
                     ' max_dn_cl+cl_n='+ STRING(max_dn_cl[1]+$
                       base_rules.cl_n))
               done =0
             ENDELSE
           ENDIF ELSE BEGIN  
             message=STRCOMPRESS('Not changing max_dn_cl: prom_cl=0')
             done =0
           ENDELSE         

         END
     10: BEGIN
           IF prom_cl[1] EQ 1 THEN BEGIN
             tmp=where(base_rules.occupied NE 2)
             IF tmp[0] NE -1 THEN BEGIN
               ntmp=min(base_rules.n[tmp])
               IF min_dn_cl[1]+base_rules.cl_n GT ntmp THEN BEGIN
                 min_dn_cl[1]=min_dn_cl[1]-1
                 message=STRCOMPRESS('Change in min_dn_cl: min_dn_cl='+$
                        STRING(min_dn_cl[0])+' -> '+STRING(min_dn_cl[1]))
                 done = 1       
               ENDIF ELSE BEGIN
                 message=STRCOMPRESS('Not changing min_dn_cl: too large'+$
                       ' min_dn_cl+cl_n='+ STRING(min_dn_cl[1]-$
                         base_rules.cl_n))
                 done = 0
               ENDELSE
             ENDIF ELSE BEGIN
               message=STRCOMPRESS('Not changing min_dn_cl: no open'+$
                                   ' shells')
               done = 0
             ENDELSE  
           ENDIF ELSE BEGIN  
             message=STRCOMPRESS('Not changing max_dn_cl: prom_cl=0')
             done =0
           ENDELSE         
         END
     11: BEGIN
           IF prom_cl[1] EQ 1 THEN BEGIN
             IF (max_dl_cl[1]+base_rules.cl_l LE $
                max_dn_cl[1]+base_rules.cl_n -1) THEN BEGIN

               max_dl_cl[1]=max_dl_cl[1]+1
               message=STRCOMPRESS('Change in max_dl_cl: max_dl_cl='+$
                      STRING(max_dl_cl[0])+' -> '+STRING(max_dl_cl[1]))
               
               done =1       
             ENDIF ELSE BEGIN
               message=STRCOMPRESS('Not changing max_dl_cl: too large'+$
                     ' max_dl_cl+cl_l='+ STRING(max_dl_cl[1]+$
                       base_rules.cl_l))
               done =0
             ENDELSE
           ENDIF ELSE BEGIN  
             message=STRCOMPRESS('Not changing max_dn_cl: prom_cl=0')
             done =0
           ENDELSE         
         END
     12: BEGIN
           IF prom_cl[1] EQ 1 THEN BEGIN
             IF (min_dl_cl[1]+base_rules.cl_l GT 1) THEN BEGIN

               min_dl_cl[1]=min_dl_cl[1]-1
               message=STRCOMPRESS('Change in min_dl_cl: min_dl_cl='+$
                      STRING(min_dl_cl[0])+' -> '+STRING(min_dl_cl[1]))
               
               done =1       
             ENDIF ELSE BEGIN
               message=STRCOMPRESS('Not changing min_dl_cl: too large'+$
                     ' min_dl_cl+cl_l='+STRING(min_dl_cl[1]+$
                       base_rules.cl_l))
               done =0
             ENDELSE
           ENDIF ELSE BEGIN  
             message=STRCOMPRESS('Not changing max_dn_cl: prom_cl=0')
             done =0
           ENDELSE         
         END

     13: BEGIN
           IF grd_cmplx[1] EQ 0 THEN BEGIN
             done =1
             IF (base_rules.v1_l+1 EQ base_rules.v1_n) AND $
                (base_rules.v1_n LE 4) THEN BEGIN
               IF (base_rules.nel[base_rules.iv1] GE $
                   base_rules.max_occup[base_rules.iv1]-1) THEN BEGIN
                 message=STRCOMPRESS('Not initiating grd_cmplx: n shell'+$
                 'is full(2)')
                 grd_cmplx[1]=grd_cmplx[1]
                 done=0
               ENDIF
             ENDIF   

             IF (base_rules.v1_l EQ 3) AND (base_rules.v1_n EQ 4) AND $
                (base_rules.nel[base_rules.iv1] GE 7) AND $
                (done NE 0)THEN BEGIN
               message=STRCOMPRESS('No change in grd_cmplx: 4f7 shell '+$
                                   'is trouble')
               grd_cmplx[1]=grd_cmplx[1]
               done=0
             ENDIF        

        
             IF base_rules.v1_n LT 5 AND done NE 0 THEN BEGIN
               grd_cmplx[1]=grd_cmplx[1]+1
               message=STRCOMPRESS('Change in grd_cmplx: grd_cmplx='+$
                      STRING(grd_cmplx[1-1])+' -> '+STRING(grd_cmplx[1]))
               done =1
             ENDIF ELSE BEGIN
               grd_cmplx[1]=grd_cmplx[1]
                      
               message=STRCOMPRESS( 'n=5 shell or greater - no ground complex can be '+$
                ' introduced')
             
               done =0
             ENDELSE
           ENDIF ELSE BEGIN
             message=STRCOMPRESS( 'Not introducing grd_cmplx: already'+$
                                  ' here!') 
             done =0
           ENDELSE         

         END
     
     14: BEGIN
; this rule will introduce fill_n_v1 and fill_par
           IF fill_n_v1[0] NE 1 THEN BEGIN
             fill_n_v1[1] = 1
             fill_par[1] = 1
             message= STRCOMPRESS('Change in fill_n_v1:'+$
                      STRING(fill_n_v1[0])+' -> '+STRING(fill_n_v1[1])+ $
                      '; change in fill_par:'+ STRING(fill_par[0])+' -> '+$
                      STRING(fill_par[1]))
             done = 1         
           ENDIF ELSE BEGIN
             message= STRCOMPRESS('No change to fill_n_v1: already set')
             done=0  
           ENDELSE
         END

     15: BEGIN
           IF fill_n_v1[0] NE 1 THEN BEGIN
             fill_n_v1[1] = 1
             fill_par[1] = 0
             message= STRCOMPRESS('Change in fill_n_v1:'+$
                      STRING(fill_n_v1[0])+' -> '+STRING(fill_n_v1[1])+ $
                      '; change in fill_par:'+ STRING(fill_par[0])+' -> '+$
                      STRING(fill_par[1]))
             done = 1
           ENDIF ELSE BEGIN
             IF fill_par[0] EQ 1 THEN BEGIN
               fill_par[1] = 0
               message= STRCOMPRESS('Change in fill_n_v1:'+$
                        STRING(fill_n_v1[0])+' -> '+STRING(fill_n_v1[1])+ $
                        '; change in fill_par:'+ STRING(fill_par[0])+' -> '+$
                        STRING(fill_par[1]))
               done = 1
             ENDIF ELSE BEGIN
               message=STRCOMPRESS('No change to fill_n_v1: already set')
             ENDELSE
           ENDELSE

         END


     16: BEGIN
           IF base_rules.allow_4f EQ  1 THEN BEGIN
             IF last_4f[0] EQ 0 THEN BEGIN
               last_4f[1] = -1
               message= STRCOMPRESS('Change in last_4f:'+$
                        STRING(last_4f[0])+' -> '+STRING(last_4f[1]))
               done = 1
             ENDIF ELSE BEGIN
               message = 'Not changing last_4f, already set'
               done = 0
             ENDELSE  
           ENDIF ELSE BEGIN
             message=STRCOMPRESS('No change to last_4f: not viable promotion')
           ENDELSE
 
         END
     17: BEGIN ;max_dn_v1+1, max_dl_v1 +1
           IF max_dn_v1[1] GE 5 THEN BEGIN
             message=STRCOMPRESS('Not changing max_dn_v1: too large'+$
                   ' max_dn_v1= '+ STRING(max_dn_v1[1])+$
                   ', upper limit is 5)')
             done =0
           ENDIF ELSE IF max_dn_v1[1]+base_rules.v1_n LE 7 THEN BEGIN
             IF max_dl_v1[1]+base_rules.v1_l LE $
                max_dn_v1[1]+base_rules.v1_n THEN BEGIN
               max_dn_v1[1]=max_dn_v1[1]+1
               max_dl_v1[1]=max_dl_v1[1]+1
               message=STRCOMPRESS('Change in max_dn_v1: max_dn_v1= '+$
                      STRING(max_dn_v1[0])+' -> '+STRING(max_dn_v1[1])+$
                      'Change in max_dl_v1: max_dl_v1= '+$
                      STRING(max_dl_v1[0])+' -> '+STRING(max_dl_v1[1]))
               done =1       
             ENDIF
           ENDIF ELSE BEGIN
             message=STRCOMPRESS('Not changing max_dn_v1: too large'+$
                   ' max_dn_v1+v1_n='+ STRING(max_dn_v1[1]+$
                     base_rules.v1_n))
             done =0
           ENDELSE

         END

      18: BEGIN
           IF no_v_shl[1] EQ 2 THEN BEGIN
             IF max_dn_v2[1] GE 5 THEN BEGIN
               message=STRCOMPRESS('Not changing max_dn_v2: too large'+$
                     ' max_dn_v2= '+ STRING(max_dn_v2[1])+$
                     ', upper limit is 5)')
               done =0
             ENDIF ELSE IF max_dn_v2[1]+base_rules.v2_n LE 7 THEN BEGIN
               IF max_dl_v2[1]+base_rules.v2_l LE $
                 max_dn_v2[1]+base_rules.v2_n THEN BEGIN
                 max_dn_v2[1]=max_dn_v2[1]+1
                 max_dl_v2[1]=max_dl_v2[1]+1
                 message=STRCOMPRESS('Change in max_dn_v2: max_dn_v2= '+$
                        STRING(max_dn_v2[0])+' -> '+STRING(max_dn_v2[1]) +$
                        'Change in max_dl_v2: max_dl_v2= '+$
                        STRING(max_dl_v2[0])+' -> '+STRING(max_dl_v2[1]))
                 done =1       
               ENDIF
             ENDIF ELSE BEGIN
               message=STRCOMPRESS('Not changing max_dn_v2: too large'+$
                     ' max_dn_v2+v2_n='+ STRING(max_dn_v2[1]+$
                       base_rules.v2_n))
               done =0
             ENDELSE
           ENDIF
         END
         
     19: BEGIN
           IF prom_cl[1] EQ 1 THEN BEGIN
             IF max_dn_cl[1] GE 5 THEN BEGIN
               message=STRCOMPRESS('Not changing max_dn_cl: too large'+$
                     ' max_dn_cl= '+ STRING(max_dn_cl[1])+$
                     ', upper limit is 5)')
               done =0
             ENDIF ELSE IF max_dn_cl[1]+base_rules.cl_n LE 7 THEN BEGIN
               IF max_dl_cl[1]+base_rules.cl_l LE $
                 max_dn_cl[1]+base_rules.cl_n THEN BEGIN
                 max_dn_cl[1]=max_dn_cl[1]+1
                 max_dl_cl[1]=max_dl_cl[1]+1
                 message=STRCOMPRESS('Change in max_dn_cl: max_dn_cl= '+$
                        STRING(max_dn_cl[0])+' -> '+STRING(max_dn_cl[1])+$
                        'Change in max_dl_cl: max_dl_cl= '+$
                        STRING(max_dl_cl[0])+' -> '+STRING(max_dl_cl[1]))
                 done =1       
               ENDIF

             ENDIF ELSE BEGIN
               message=STRCOMPRESS('Not changing min_dn_cl and max_dl_cl: no open'+$
                                   ' shells')
               done = 0
             ENDELSE  
           ENDIF ELSE BEGIN  
             message=STRCOMPRESS('Not changing max_dn_cl and max_dl_cl: prom_cl=0')
             done =0
           ENDELSE         
; **** IMPORTANT: If you add more rules, move "last_case=1" to the end of the
; last rule!!! ****
           last_case=1
         END
    
    ENDCASE  
  ENDELSE
ENDIF

IF KEYWORD_SET(n_cl_promotions) THEN BEGIN
  FOR i=1, n_cl_promotions DO BEGIN
    IF prom_cl[1] NE 1 THEN BEGIN
      IF base_rules.prom_cl NE 0 THEN prom_cl[1]=1
    ENDIF
      
    IF (prom_cl[1] EQ 1 AND base_rules.prom_cl NE 0) THEN BEGIN

      
      IF min_l_cl[1] GT 0 THEN BEGIN
        min_l_cl[1] = min_l_cl[1] -1
        message=STRCOMPRESS('Change in min_l_cl: min_l_cl='+$
               STRING(min_l_cl[1-1])+' -> '+STRING(min_l_cl[1]))
;        print, message       
        done =1
        CONTINUE
      ENDIF

      IF (max_l_cl[1] LT base_rules.cl_l) AND $
         (max_l_cl[1] LT max_n_cl[1]-2) THEN BEGIN
        max_l_cl = max_l_cl +1
        message=STRCOMPRESS('Change in max_l_cl: max_l_cl='+$
               STRING(max_l_cl[1-1])+' -> '+STRING(max_l_cl[1]))
 ;       print, message
        done =1
        CONTINUE
      ENDIF

      IF (max_n_cl[1] LT base_rules.cl_n) THEN BEGIN
        max_n_cl[1] = max_n_cl[1] +1
        message=STRCOMPRESS('Change in max_n_cl: max_n_cl='+$
               STRING(max_n_cl[1-1])+' -> '+STRING(max_n_cl[1]))
 ;       print, message               
        done =1
        CONTINUE
      ENDIF


      IF (min_n_cl[1] GT base_rules.cl_n) THEN BEGIN
        min_n_cl[1] = min_n_cl[1] -1
        message=STRCOMPRESS('Change in min_n_cl: min_n_cl='+$
               STRING(min_n_cl[1-1])+' -> '+STRING(min_n_cl[1]))
;        print, message
        done =1
        CONTINUE
      ENDIF
 
      IF (min_l_cl[1] EQ 0 AND min_n_cl[1] EQ 2) THEN BEGIN
        min_n_cl[1]=min_n_cl[1]-1
        message=STRCOMPRESS('Change in min_n_cl: min_n_cl='+$
               STRING(min_n_cl[1-1])+' -> '+STRING(min_n_cl[1]))
;        print, message
        done =1
        CONTINUE
      ENDIF
    ENDIF
  ENDFOR

ENDIF

;-----------------------------------------------------------------------
; prepare data to return
;-----------------------------------------------------------------------

IF keyword_set(final_rules) THEN BEGIN
  ret={ z0_nuc           : z0_nuc,          $
        z_ion            : z_ion,           $
        config           : config,          $
        ionpot           : ionpot,          $
        no_v_shl         : no_v_shl[1],     $
        max_dn_v1        : max_dn_v1[1],    $
        min_dn_v1        : min_dn_v1[1],    $
        max_dl_v1        : max_dl_v1[1],    $
        min_dl_v1        : min_dl_v1[1],    $
        max_dn_v2        : max_dn_v2[1],    $
        min_dn_v2        : min_dn_v2[1],    $
        max_dl_v2        : max_dl_v2[1],    $
        min_dl_v2        : min_dl_v2[1],    $
        prom_cl          : prom_cl[1],      $
        max_n_cl         : max_n_cl[1],     $
        min_n_cl         : min_n_cl[1],     $
        max_l_cl         : max_l_cl[1],     $
        min_l_cl         : min_l_cl[1],     $
        max_dn_cl        : max_dn_cl[1],    $
        min_dn_cl        : min_dn_cl[1],    $
        max_dl_cl        : max_dl_cl[1],    $
        min_dl_cl        : min_dl_cl[1],    $
        fill_n_v1        : fill_n_v1[1],    $
        fill_par         : fill_par[1],     $
        for_tr_sel       : for_tr_sel[1],   $
        last_4f          : last_4f[1],      $
        grd_cmplx        : grd_cmplx[1],    $
;        grd_cfg          : grd_cfg,         $
;        ex_cfg           : ex_cfg,          $
        message          : message,         $
        done             : done}
        
        
        
  final_rules=ret
ENDIF

END
