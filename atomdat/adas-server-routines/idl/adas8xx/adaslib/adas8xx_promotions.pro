;-----------------------------------------------------------------------------
;+
;
; NAME     : adas8xx_promotions
;
; PURPOSE  : generate a set of excited configurations for an ion of an
;            element from an initial ground configuration and a set of rules.
;
; CATEGORY : ADAS
;
; USE      : Usually called after 'adas8xx_promotion_rules'
;
; INPUT    :
;
;   (I*4)   z0_nuc          = keyword = nuclear charge
;   (I*4)   z_ion           = keyword = selected ion charge
;   (R*8)   ionpot          = keyword = ionis. potential (ev) for an ion    
;   (struc) prom_rules      = keyword = structure of rules for ion charges 0 --> z0_nuc-1
;
;
; OUTPUTS   :
;
;   (struc) promotion_results=keyword = structure containing results, contains:
;
;
;
;     (C* )   grd_cfg      = keyword = ground configuration
;     (I*4)   grd_occ[]    = keyword = ground occupation nos.
;     (C* )   ex_cfg[]     = keyword = excited configurations  
;     (I*4)   grd_par      = keyword = ground parity
;     (I*4)   ex_par[]     = keyword = excited parities
;     (I*4)   grd_zc_cow[] = keyword = grd. eff. charge for Cowan adf34 driver
;     (I*4)   ex_zc_cow[]  = keyword = exc. eff. charge for Cowan adf34 driver
;     (I*4)   oc_store[]   = keyword = configuration occupancies
;     (I*4)   no_configs[] = keyword = no. of configs in 7 categories (0: grd, 1: 1st val., 2: 2nd val.,
;                                       3: inner shell, 4: all val. n, 5: alter. grd., 6: grd complex.
;     (I*4)   no_terms []  = keyword = no. of levels in 7 categories (as above)
;     (I*4)   no_levels[]  = keyword = no. of levels in 7 categories (as above)
;
; NOTES    : The prom_rules structure is defined as:  
;                             
;              index        : index of ground configuration of each ion of element in adf54 file 
;              config       : ground configuration for each ion of element
;              no_v_shl     : number of open (valence) shells. Include outer-most shell even if closed.
;              max_dn_v1    : maximum delta n promotion for first (outer-most) valence shell
;              min_dn_v1    : minimum delta n promotion for first (outer-most) valence shell. 
;                                       Negative value allows access to inner unoccupied or open shells
;              max_dl_v1    : maximum delta l promotion for first (outer-most) valence shell
;              min_dl_v1    : minimum delta l promotion for first (outer-most) valence shell.
;              max_dn_v2    : maximum delta n promotion for second (inner-most) valence shell
;              min_dn_v2    : maximum delta n promotion for second (inner-most) valence shell
;              max_dl_v2    : maximum delta l promotion for second (inner-most) valence shell
;              min_dl_v2    : minimum delta l promotion for second (inner-most) valence shell
;              prom_cl      : promote from inner shell closed shells (1=yes,0=no) 
;              max_n_cl     : maximum inner shell n for promotion from
;              min_n_cl     : minimum inner shell n for promotion from
;              max_l_cl     : maximum inner shell l for promotion from
;              min_l_cl     : minimum inner shell l for promotion from
;              max_dn_cl    : maximum delta n promotion for inner shell to
;              min_dn_cl    : minimum delta n promotion for inner shell to
;              max_dl_cl    : maximum delta l promotion for inner shell to
;              min_dl_cl    : minimum delta n promotion for inner shell to
;                                       Negative value allows access to inner unoccupied or open shells
;              fill_n_v1    : add all nl configurations of outer valence shell n (1=yes,0=no)
;              fill_par     : if n_fill only add opposite parity to valence shell else add both parities (1=yes, 0=n0)
;              for_tr_sel   : Cowan option for radiative transitions 1 - first parity, 2 or 3(default)
;              last_4f      : shift an electron valence shell to unfilled 4f as extra ground
;              grd_cmplx    : include configurations of same complex as ground configuation for valence n-shell
;
; ROUTINES: 
;            NAME              TYPE    COMMENT
;            get_parity        local   returns the parity given occupation numbers
;            new_occup         local   determine whether the occupation vector
;                                      matchs a group.
;            write_config      local   writes an adf34 compatible configuration.
;            adas808_cfg_cmplx ADAS    return configurations of an n-shell complex 
;            h8nlev            ADAS    returns the level count for a configuration
;
;
; CATEGORY:
;       Adas system.
;
; WRITTEN:
;       H. P. Summers, University of Strathclyde,29-06-06.
;
; MODIFIED:
;       1.1     H. P. Summers
;                 - First version put into CVS.
;       1.2     Allan Whiteford
;                 - Changed call from complexes to adas808_cfg_complx
;                   Was missed during renaming exercise.
;       1.3     H. P. Summers
;                 - Added lonarr for config,term and level count vector; improved 
;                   input/output descriptors.  Correction to logic for rare gas
;                   omitted closed-shell detection
;       1.4     H. P. Summers
;                 - Further correction to logic for Cowan effective z for adf34
;                   driver.  Now use zc=z1 for z0<19.
;       1.5     H. P. Summers
;                 - Correction to preamble text for fill_par
;       1.6     H. P. Summers
;                 - Replaced variable name z0 by z0_nuc, z1 by z_ion and zc by zc_cow to 
;                   avoid confusion.  Introduced rules structure as a keyword parameter
;       1.7     A. R. Foster
;                 - Changed rules structure to cope with both single element 
;                   fields, and vectors
;                 - data now returned in promotion_results structure.
;       1.8    S. S. Henderson
;                 - Ensure the number of valence shells is less than or
;   	    	    equal to the allowed value in the adf54 file
;       1.9    M O'Mullane
;                 - Do not return a negative ion charge.
;                 - Cowan fills 5s and 5p before 4f. Return 5s0 and 5s0 in the
;                   configuration string depending on the number of electrons.
;
; VERSION:
;       1.1   25-08-2006
;       1.2   09-10-2006
;       1.3   29-11-2006
;       1.4   18-06-2007
;       1.5   18-07-2008
;       1.6   22-08-2008
;       1.7   18-12-2008
;       1.8   15-11-2016
;       1.9   17-12-2018
;-
;-----------------------------------------------------------------------------

FUNCTION get_parity, occup

; Returns the parity given the occupation numbers.

common shells, ndshell     ,                                          $
               shmax       , nval        , lval       , shl_lab  ,    $
               iprom_1     , iprom_2     ,                            $
               nrg_cnt_idx , nrg_exl_idx , nrg_el_cnt ,               $
               nval_max    , nval_min    , lval_max   , lval_min ,    $
               njvals 

parity = 0

for j = 0, n_elements(occup)-1 do begin
   
   if occup[j] NE 0 AND occup[j] NE shmax[j] then $
           parity = parity + lval[j] * occup[j]

endfor
parity = parity mod 2

return, parity

END

;---------------------------------------------------------------------------

FUNCTION new_occup, occup_store, occup, ndshell

; The inner shell promotions can arrive at the same configurations
; as standard promotion - check to see if the candidate is already
; in the list.

oc_store  = reform(occup_store, ndshell, n_elements(occup_store)/ndshell)

val   = 1
n_std = (size(oc_store))[2]
n_sh  = (size(oc_store))[1]

for j = 0, n_std-1 do begin
 
 tmp = oc_store[*,j]-occup
 res = where(tmp EQ 0, num)
 if num EQ n_sh then val = 0

endfor

return, val

END

;---------------------------------------------------------------------------

PRO write_config, z0_nuc, z_ion, zc_cow, occup, config, parity

; Write the configuration, in a form suitable for Cowan adf34 input,
; from the ifirst to ilast shell. Calculate the parity also.
;

common shells, ndshell     ,                                          $
               shmax       , nval        , lval       , shl_lab  ,    $
               iprom_1     , iprom_2     ,                            $
               nrg_cnt_idx , nrg_exl_idx , nrg_el_cnt ,               $
               nval_max    , nval_min    , lval_max   , lval_min ,    $
               njvals


; Cowan fills 5s and 5p before 4f so if there are 4f electrons check
; whether filled 5s2 and 5p6 is consistent with the actual number of
; electrons for the stage.

n_elec = total(occup, /integer)
is_cowan_5 = 0
if occup[9] GT 0 then begin
   ind = [indgen(10), indgen(n_elements(occup)-9-2-1)+12]
   n_elec_cowan = total(occup[ind], /integer)
   if occup[10] EQ 0 then n_elec_cowan = n_elec_cowan + 2 else n_elec_cowan = n_elec_cowan + occup[10]
   if occup[11] EQ 0 then n_elec_cowan = n_elec_cowan + 6 else n_elec_cowan = n_elec_cowan + occup[11]
   if n_elec NE n_elec_cowan then is_cowan_5 = 1
endif


ind = where(occup LT 0, count)

config = ''

if count EQ 0 then begin

    ind_first = 0

   ; Last occupied shell

   icut = where(occup NE 0)
   icut = icut[n_elements(icut)-1]

   ; Check for missing 3d and 4f configuration

   for i = 0, 5 do begin
   
     if i ne 0 then begin 
        idx_cnt=indgen(nrg_cnt_idx[i])
        idx1_cnt=where((idx_cnt ne nrg_exl_idx[0,i]) and (idx_cnt ne nrg_exl_idx[1,i]))
     endif else begin
        idx_cnt  = 0
        idx1_cnt = 0
     endelse
                     
     if (total(occup[idx1_cnt]) eq nrg_el_cnt[i]) and (total(occup[idx_cnt]) eq nrg_el_cnt[i]) then ind_first = nrg_cnt_idx[i]

      if (ind_first gt icut and i gt 0) then ind_first = nrg_cnt_idx[i-1]
      
   endfor  

   
   if ind_first gt icut then ind_first = icut
   if is_cowan_5 EQ 1 then icut = icut + 2

   for j = ind_first, icut do begin

      if occup[j] GT 0 then begin

         tmp = '     '
         strput, tmp, strcompress(shl_lab[j] + string(occup[j]), /remove_all)
         config = config + tmp

      endif

      if is_cowan_5 EQ 1 AND (j EQ 10 OR j EQ 11) AND occup[j] EQ 0 then begin

         tmp = '     '
         strput, tmp, strcompress(shl_lab[j] + string(occup[j]), /remove_all)
         config = config + tmp

      endif

   endfor

   parity = get_parity(occup)
   
   zc_cow = z_ion+1 

endif


END

;---------------------------------------------------------------------------

PRO adas8xx_promotions,  z0_nuc     = z0_nuc,                         $
                         z_ion      = z_ion,                          $
                         ionpot     = ionpot,                         $
                         prom_rules = prom_rules,                     $
                         promotion_results=promotion_results

common shells, ndshell     ,                                          $
               shmax       , nval        , lval       , shl_lab  ,    $
               iprom_1     , iprom_2     ,                            $
               nrg_cnt_idx , nrg_exl_idx , nrg_el_cnt ,               $
               nval_max    , nval_min    , lval_max   , lval_min ,    $
               njvals 

;-----------------------------------------------------------------------------
; definitions and occupations to ndshell=36 (8k) : specify common 'shells'
;-----------------------------------------------------------------------------


   ndshell = 36
   shmax   = [   2,   2,   6,   2,   6,  10,   2,   6,  10,  14,   2,   6,  10,  14,  18,   2,   6,  10,      $
                14,  18,  22,   2,   6,  10,  14,  18,  22,  26,   2,   6,  10,  14,  18,  22,  26,  30]
   nval    = [   1,   2,   2,   3,   3,   3,   4,   4,   4,   4,   5,   5,   5,   5,   5,   6,   6,   6,      $
                 6,   6,   6,   7,   7,   7,   7,   7,   7,   7,   8,   8,   8,   8,   8,   8,   8,   8]
   lval    = [   0,   0,   1,   0,   1,   2,   0,   1,   2,   3,   0,   1,   2,   3,   4,   0,   1,   2,      $
                 3,   4,   5,   0,   1,   2,   3,   4,   5,   6,   0,   1,   2,   3,   4,   5,   6,   7]
   shl_lab = ['1s','2s','2p','3s','3p','3d','4s','4p','4d','4f','5s','5p','5d','5f','5g','6s','6p','6d',      $
              '6f','6g','6h','7s','7p','7d','7f','7g','7h','7i','8s','8p','8d','8f','8g','8h','8i','8k']
              
   iprom_1 = [0,1,3,6,10,15,21,28]
   iprom_2 = [0,2,5,9,14,20,27,35]
   
   nrg_cnt_idx = [   1,   3,   5,   8,  12,  17]
   nrg_exl_idx = [ [ -1, -1],  [ -1, -1],  [ -1, -1],  [ -1, -1],  [  9, -1],  [ 13, 14]]
   nrg_el_cnt  = [   2,  10,  18,  36,  54,  86]
   

   nval_max=max(nval)
   nval_min=min(nval)
   lval_max=max(lval)
   lval_min=min(lval)

   njvals         = intarr(2*(2*lval_max+1),lval_max+1)
   njvals[*,*]    = -1
   njvals[0:1,0]  = [1, 1] 
   njvals[0:5,1]  = [2, 5,  5,  5,  2,  1] 
   njvals[0:9,2]  = [2, 9, 19, 29, 36, 29, 19,  9,  2,  1] 
   njvals[0:13,3] = [2,13, 41,106,158,295,326,295,158,106, 41, 13,  2,  1]
 
   ; *** do not bother with l^q with q>1 for l=g,h,i,k ***

   njvals[0:1,4]  = [2,-1] 
   njvals[0:1,5]  = [2,-1] 
   njvals[0:1,6]  = [2,-1] 
   njvals[0:1,7]  = [2,-1] 

;----------------------------------------------------
; Establish if input is 1 set of rules of whole array
;----------------------------------------------------

if n_elements(prom_rules.config) GT 1 then begin
  if n_elements(z_ion) GE 1 then begin
    ruleindex=z_ion

  endif else begin
    print, 'adas8xx_promotion.pro error: if more than 1 promotion ruleset'+$
           ' provided, z_ion must be specified'
    stop
  endelse
endif else begin
  ruleindex=0
endelse
;------------------------------------------
; Find the last fully occupied shell - icut
;------------------------------------------

res   = strsplit(prom_rules.config[ruleindex], ' ', /extract)
occup = long(strmid(res, 2))

ilast = n_elements(occup)-1
icut  = ilast-1 > 0

for j = ilast, 0, -1 do begin
   if occup[j] NE shmax[j] then icut = j-1
endfor

icut  = icut > 0


;--------------------------------------------------------------------
; Fill out the occup vector to ndshell, check the number of electrons 
; and return the ground occupation 
;--------------------------------------------------------------------

tmp            = occup
occup          = intarr(ndshell)
occup[0:ilast] = tmp

if total(occup) NE (z0_nuc-z_ion) then begin

    print,'*** default_promotions: incorrect number of electrons ***'
    stop
    
endif

grd_occ = occup


;-------------------------------------------------------------------
; Find the valence shells and check against no_v_shl input parameter 
;-------------------------------------------------------------------

; Find the number of valence shells
 
ncount = 0
nopen  = intarr(2)

for j = ilast, 0, -1 do begin

   if  ((occup[j] gt 0) and (occup[j] lt shmax[j])) or   $
       ((j eq ilast) and (occup[j] eq shmax[j])) then  begin
           if ncount LT prom_rules.no_v_shl[ruleindex]then begin
              nopen[ncount] = j
              ncount = ncount+1
           endif
   endif

endfor
   
if ncount ge 3 then begin
 
  print,'*** adas8xx_promotions warning: more than 2 valence shells               ***'
  print,'***                     restrict to default ',prom_rules.no_v_shl[ruleindex], ' shells      ***'
  
endif else begin

  if (ncount eq 2) and (ncount ne prom_rules.no_v_shl[ruleindex]) then begin 

    print,'*** adas8xx_promotions warning: default valence shell count mismatch   ***'
    print,'***                     restrict to  default ',prom_rules.no_v_shl[ruleindex], ' shell    ***'

   endif

  if (ncount eq 1) and (prom_rules.no_v_shl[ruleindex] eq 2) then begin 

;    print,'*** adas8xx_promotions: default valence shell count mismatch   ***'
;    print,'***                     take last-1 shell as valence shell 2   ***'
    
    nopen[ncount] = ilast-1
    ncount = ncount+1

   endif
   
endelse   
   

;print,'prom_rules.no_v_shl=',prom_rules.no_v_shl,' ncount=',ncount
;print,'nopen=',nopen

;------------------------------------------------------------------
; generate the possible configurations given the input criteria
;------------------------------------------------------------------

no_configs = lonarr(7)
no_terms   = lonarr(7)
no_levels  = lonarr(7)

ex_cfg = 'NULL'
ex_par = -99
ex_zc_cow  = 100

;---------------------------
; stack ground configuration
;---------------------------


write_config, z0_nuc, z_ion, zc_cow, occup, grd_cfg, grd_par





grd_occup  = occup
grd_zc_cow = zc_cow

oc_store = occup
no_configs[0] = 1
no_levels[0]  = h8nlev(occup,term_count=term_count)
no_terms[0]   = term_count

;--------------------------------------------------------------
; execute 1st valence shell promotions and stack configurations 
;--------------------------------------------------------------

if prom_rules.no_v_shl[ruleindex] ge 1 then begin

    no_configs[1] = 0
    no_terms[1]   = 0
    no_levels[1]  = 0
    ind=nopen(0)
    occup[ind] = occup[ind]-1
    inval      = nval[ind]
    ilval      = lval(ind)
    nlow  = inval + prom_rules.min_dn_v1[ruleindex] > nval_min
    nhigh = inval + prom_rules.max_dn_v1[ruleindex] < nval_max
    llow  = ilval + prom_rules.min_dl_v1[ruleindex] > lval_min
    lhigh = ilval + prom_rules.max_dl_v1[ruleindex] < lval_max
    

    ilow  = iprom_1[nlow-1]
    ihigh = iprom_2[nhigh-1]
    

    for j = ilow, ihigh do begin

       cfg = ''
      
       case 1 of
      
        lval[j] lt llow or lval[j] gt lhigh : 
       j EQ ind :
       occup[j] GE shmax[j] :
;       abs(ilast-ind) GT 1 : 
      
       else : begin
                 occup[j] = occup[j]+1
                 oc_store = [oc_store, occup]
                 no_configs[1] = no_configs[1]+1
                 no_levels[1]  = no_levels[1]+h8nlev(occup,term_count=term_count)
                 no_terms[1]   = no_terms[1]+term_count
                 write_config, z0_nuc, z_ion, zc_cow, occup, cfg, parity   
                 occup[j] = occup[j]-1
                 ex_cfg = [ex_cfg, cfg]
                 ex_par = [ex_par, parity]
                 ex_zc_cow  = [ex_zc_cow, zc_cow]
              end
             
        endcase

   endfor

    occup[ind] = occup[ind]+1

endif

;--------------------------------------------------------------
; execute 2nd valence shell promotions and stack configurations 
;--------------------------------------------------------------

if prom_rules.no_v_shl[ruleindex] eq 2 then begin

    no_configs[2] = 0
    no_terms[2]   = 0
    no_levels[2]  = 0
    ind=nopen(1)
    occup[ind] = occup[ind]-1
    inval      = nval[ind]
    ilval      = lval(ind)
    nlow  = inval + prom_rules.min_dn_v2[ruleindex] > nval_min
    nhigh = inval + prom_rules.max_dn_v2[ruleindex] < nval_max
    llow  = ilval + prom_rules.min_dl_v2[ruleindex] > lval_min
    lhigh = ilval + prom_rules.max_dl_v2[ruleindex] < lval_max
    

    ilow  = iprom_1[nlow-1]
    ihigh = iprom_2[nhigh-1]
    

    for j = ilow, ihigh do begin

       cfg = ''
      
       case 1 of
      
        lval[j] lt llow or lval[j] gt lhigh : 
        j EQ ind :
        occup[j] GE shmax[j] :
;       abs(ilast-ind) GT 1 : 
      
       else : begin
                 occup[j] = occup[j]+1
                 if( new_occup(oc_store, occup, ndshell)) then begin
                     oc_store = [oc_store, occup]
                     no_configs[2] = no_configs[2]+1
                     no_levels[2]  = no_levels[2]+h8nlev(occup,term_count=term_count)
                     no_terms[2]   = no_terms[2]+term_count
                     write_config, z0_nuc, z_ion, zc_cow, occup, cfg, parity   
                     ex_cfg = [ex_cfg, cfg]
                     ex_par = [ex_par, parity]
                     ex_zc_cow  = [ex_zc_cow, zc_cow]
                 endif    
                 occup[j] = occup[j]-1
              end
             
        endcase

   endfor
   
    occup[ind] = occup[ind]+1

endif

;---------------------------------------------------------------
; execute closed inner shell promotions and stack configurations 
;---------------------------------------------------------------


if keyword_set(prom_rules.prom_cl[ruleindex]) then begin

;    oc_store  = reform(oc_store, ndshell, n_elements(oc_store)/ndshell)

     no_configs[3] = 0
     no_terms[3]   = 0
     no_levels[3]  = 0
     
     nlow_cl  = prom_rules.min_n_cl[ruleindex] > nval_min
     nhigh_cl = prom_rules.max_n_cl[ruleindex] < nval_max
     llow_cl  = prom_rules.min_l_cl[ruleindex] > lval_min
     lhigh_cl = prom_rules.max_l_cl[ruleindex] < lval_max


     ilow_cl  = iprom_1[nlow_cl-1]
     ihigh_cl = iprom_2[nhigh_cl-1]
     

     for j = ilow_cl, ihigh_cl do begin
     
        inval=nval(j)
        ilval=lval(j)

        case 1 of

           occup[j] LE 0 :
           ilval lt llow_cl or ilval gt lhigh_cl :

        else : begin

     
                   nlow  = inval + prom_rules.min_dn_cl[ruleindex] > nval_min
                   nhigh = inval + prom_rules.max_dn_cl[ruleindex] < nval_max
                   llow  = ilval + prom_rules.min_dl_cl[ruleindex] > lval_min
                   lhigh = ilval + prom_rules.max_dl_cl[ruleindex] < lval_max
    

                   ilow  = iprom_1[nlow-1]
                   ihigh = iprom_2[nhigh-1]
    

                   occup[j] = occup[j]-1
                   for i = ilow, ihigh do begin

                      occup[i] = occup[i]+1
                      if j NE i                     AND $
                         occup[i] LE shmax[i]       AND $
                         lval(i) ge llow            AND $
                         lval(i) le lhigh           AND $
                         new_occup(oc_store, occup, ndshell) then begin
                         oc_store = [oc_store, occup]
                         no_configs[3] = no_configs[3]+1 
                         no_levels[3]  = no_levels[3]+h8nlev(occup,term_count=term_count)
                         no_terms[3]   = no_terms[3]+term_count
                         write_config, z0_nuc, z_ion, zc_cow, occup, cfg, parity
                         ex_cfg = [ex_cfg, cfg]
                         ex_par = [ex_par, parity]
                         ex_zc_cow  = [ex_zc_cow, zc_cow]

                      endif
                      occup[i] = occup[i]-1

                   endfor
                   occup[j] = occup[j] + 1

                end

          endcase

      endfor

endif

;---------------------------------------------------------------
; execute outer valence electron promotion to l-shells of same n 
;---------------------------------------------------------------

no_configs[4] = 0
no_terms[4]   = 0
no_levels[4]  = 0

if keyword_set(prom_rules.fill_n_v1[ruleindex]) then begin

   iupper = value_locate(iprom_2+0.1, ilast) + 1    ; 0.1 to avoid boundary

   if iupper LE n_elements(iprom_2)-1 then begin
   
      occup[ilast]  = occup[ilast] - 1

      for j = ilast+1, iprom_2[iupper] do begin

         occup[j] = occup[j] + 1
         
         if keyword_set(prom_rules.fill_par[ruleindex]) then begin
         
;--------------------------------------------
; promote to l-shells of opposite parity only 
;--------------------------------------------
     
            if new_occup(oc_store, occup, ndshell) AND $
               get_parity(occup) NE grd_par then begin
                  oc_store     = [oc_store, occup]
                  no_configs[4] = no_configs[4]+1 
                  no_levels[4]  = no_levels[4]+h8nlev(occup,term_count=term_count)
                  no_terms[4]   = no_terms[4]+term_count
                  write_config, z0_nuc, z_ion, zc_cow, occup, cfg, parity
                  ex_cfg       = [ex_cfg, cfg]
                  ex_par       = [ex_par, parity]
                   ex_zc_cow   = [ex_zc_cow, zc_cow]
            endif
         
         endif else begin
         
;-------------------------------------
; promote to l-shells of either parity 
;-------------------------------------
   
            if new_occup(oc_store, occup, ndshell) then begin
               oc_store     = [oc_store, occup]
               no_configs[4] = no_configs[4]+1 
               no_levels[4]  = no_levels[4]+h8nlev(occup,term_count=term_count)
               no_terms[4]   = no_terms[4]+term_count
               write_config, z0_nuc, z_ion, zc_cow, occup, cfg, parity
               ex_cfg       = [ex_cfg, cfg]
               ex_par       = [ex_par, parity]
               ex_zc_cow    = [ex_zc_cow, zc_cow]
            endif
         
         endelse
         
         occup[j] = occup[j] - 1

      endfor
      
      occup[ilast]  = occup[ilast] + 1

   endif

endif

;--------------------------------------------------------------
; ambiguous ground configuration for loosly bound 4f electrons,
; shift a valence electron to 4f and promote valence electron
; within n-shell subject to same rules as above 
;--------------------------------------------------------------

no_configs[5] = 0
no_terms[5]   = 0
no_levels[5]  = 0

if keyword_set(prom_rules.last_4f[ruleindex]) and (occup[9] lt 14) then begin

;----------------------------------
; move electron from valence shell to 4f
;----------------------------------
   
   occup[ilast] = occup[ilast] - 1
   occup[9]     = occup[9] + 1
   
   if new_occup(oc_store, occup, ndshell) then begin
      oc_store          = [oc_store, occup]
      no_configs[5] = no_configs[5]+1 
      no_levels[5]  = no_levels[5]+h8nlev(occup,term_count=term_count)
      no_terms[5]   = no_terms[5]+term_count
      write_config, z0_nuc, z_ion, zc_cow, occup, cfg, parity
      ex_cfg        = [ex_cfg, cfg]
      ex_par        = [ex_par, parity]
      ex_zc_cow     = [ex_zc_cow, zc_cow]
   endif   

   ; Now promote test_occup according to input rules
   
   if (occup[ilast] ge 1) and prom_rules.fill_n_v1[ruleindex] then begin 
    
   
      iupper = value_locate(iprom_2+0.1, ilast) + 1    ; 0.1 to avoid boundary

      if iupper LE n_elements(iprom_2)-1 then begin

         occup[ilast]  = occup[ilast] - 1

         for j = ilast+1, iprom_2[iupper] do begin

            occup[j] = occup[j] + 1
         
            if keyword_set(prom_rules.fill_par[ruleindex]) then begin
         
;--------------------------------------------
; promote to l-shells of opposite parity only 
;--------------------------------------------
       
                if new_occup(oc_store, occup, ndshell) AND $
                   get_parity(occup) NE grd_par then begin
                      oc_store     = [oc_store, occup]
                      no_configs[5] = no_configs[5]+1 
                      no_levels[5]  = no_levels[5]+h8nlev(occup,term_count=term_count)
                      no_terms[5]   = no_terms[5]+term_count
                      write_config, z0_nuc, z_ion, zc_cow, occup, cfg, parity
                      ex_cfg        = [ex_cfg, cfg]
                      ex_par        = [ex_par, parity]
                      ex_zc_cow     = [ex_zc_cow, zc_cow]
                endif
         
             endif else begin
         
;-------------------------------------
; promote to l-shells of either parity 
;-------------------------------------
   
                if new_occup(oc_store, occup, ndshell) then begin
                   oc_store     = [oc_store, occup]
                   no_configs[5] = no_configs[5]+1 
                   no_levels[5]  = no_levels[5]+h8nlev(occup)
                   no_levels[5]  = no_levels[5]+h8nlev(occup,term_count=term_count)
                   no_terms[5]   = no_terms[5]+term_count
                   write_config, z0_nuc, z_ion, zc_cow, occup, cfg, parity
                   ex_cfg        = [ex_cfg, cfg]
                   ex_par        = [ex_par, parity]
                   ex_zc_cow     = [ex_zc_cow, zc_cow]
                endif
         
             endelse
         
             occup[j] = occup[j] - 1

          endfor

       endif
       
       occup[ilast]  = occup[ilast] + 1

   endif
      
   occup[ilast] = occup[ilast] + 1
   occup[9]     = occup[9] - 1

endif

;---------------------------------------------------------------
; include configurations of same complex as the ground - restrict
; to the occupancies of the n-shell of the 1st valence electron 
;---------------------------------------------------------------

no_configs[6] = 0
no_terms[6]   = 0
no_levels[6]  = 0

if keyword_set(prom_rules.grd_cmplx[ruleindex]) then begin

    valence_n    = nval[nopen[0]]
    cmplx_list   = where(nval eq valence_n)
    valence_nel  = fix(total(occup[cmplx_list]))
    cmplx_parity = get_parity(occup)
    occup_save   = occup[cmplx_list]
    
    cmplx_occup=adas808_cfg_cmplx(valence_n,valence_nel)
    
    for i=0,(size(cmplx_occup))[1]-1 do begin
      
       occup[cmplx_list]=cmplx_occup[i,*]

;--------------------------------------------
;  
;--------------------------------------------
     
            if new_occup(oc_store, occup, ndshell) AND $
               get_parity(occup) EQ cmplx_parity then begin
                  oc_store     = [oc_store, occup]
                  no_configs[6] = no_configs[6]+1 
                  no_levels[6]  = no_levels[6]+h8nlev(occup)
                  no_levels[6]  = no_levels[6]+h8nlev(occup,term_count=term_count)
                  no_terms[6]   = no_terms[6]+term_count
                  write_config, z0_nuc, z_ion, zc_cow, occup, cfg, parity
                  ex_cfg        = [ex_cfg, cfg]
                  ex_par        = [ex_par, parity]
                   ex_zc_cow    = [ex_zc_cow, zc_cow]
            endif
         
         
       occup[cmplx_list]=occup_save

    endfor

endif


;------------------------------------
; Empty d shells with higher n ground
;------------------------------------

;izero = -1
;for j = ilast-1, 0, -1 do if grd_occup[j] EQ 0 then izero = j 

;if izero NE -1 AND abs(ilast-izero) EQ 1 then begin
   
;   test_occup        = grd_occup
;   test_occup[ilast] = test_occup[ilast] - 1
;   test_occup[izero] = test_occup[izero] + 1
;   oc_store          = [oc_store, test_occup]
;   write_config, z0_nuc, z_ion, zc_cow, test_occup, cfg, parity
;   ex_cfg            = [ex_cfg, cfg]
;   ex_par            = [ex_par, parity]

;endif

;---------------------------------------------------------
; Remove 'NULL' in the ex_cfg array and 100 in ex_zc_cow array 
;---------------------------------------------------------

if n_elements(ex_cfg) GT 1 then begin
   ex_cfg = ex_cfg[1:*]
   ex_par = ex_par[1:*]
   ex_zc_cow  = ex_zc_cow[1:*]
endif

oc_store  = reform(oc_store,ndshell,n_elements(oc_store)/ndshell,/overwrite)

promotion_results={      grd_cfg    : grd_cfg,                        $
                         grd_occ    : grd_occ,                        $
                         ex_cfg     : ex_cfg,                         $
                         grd_par    : grd_par,                        $
                         ex_par     : ex_par,                         $
                         grd_zc_cow : grd_zc_cow,                     $
                         ex_zc_cow  : ex_zc_cow,                      $
                         oc_store   : oc_store,                       $
                         no_configs : no_configs,                     $
                         no_terms   : no_terms,                       $
                         no_levels  : no_levels                       }

END
