; Copyright (c) 2003 Strathclyde University .
; SCCS info: Module @(#)$Header: /home/adascvs/idl/adas8xx/adaslib/hx34sr.pro,v 1.1 2004/07/06 14:06:01 whitefor Exp $ Date $Date: 2004/07/06 14:06:01 $
;+
; PROJECT:
;       ADAS
;
; NAME:
;	HX34SR()
;
; PURPOSE:
;	Sorts an adf34 file.
;
; EXPLANATION:
;	This procedure takes an input filename and an output filename.
;	It will attempt to read the input file and check that the order
;	of the levels is valid for input to rcn, the first stage of the
;	cowan code. If not then it will sort the configurations into a
;	valid order. A valid adf34 file is written to the output filename.
;
; USE:
;	An example;
;		hx34sr,'invalid_adf34_file','valid_adf34_file'
;
;	Or, perhaps more useful:
;		hx34sr,'adf34_file','tempfile'
;		spawn,'rm -f adf34_file'
;		spawn,'mv tempfile adf34_file'
;
;
; INPUTS:
;	infile - Input filename
;	outfile - Output filename
;
; OPTIONAL INPUTS:
;	None.
;
; OUTPUTS:
;	None.
;
; OPTIONAL OUTPUTS:
;	None.
;
; KEYWORD PARAMETERS:
;	None.
;
; CALLS:
;	None.
;
; SIDE EFFECTS:
;	Writes a file.
;
; CATEGORY:
;	ADAS System.
;
; WRITTEN:
;       Allan Whiteford, University of Strathclyde, 09-April-2003
;
; MODIFIED:
;	1.1	Allan Whiteford
;		First version
; VERSION:
;	1.1	09-04-03
;-
;-----------------------------------------------------------------------------

pro hx34sr,infile,outfile

	orbitals=['1s','2s','2p','3s','3p','3d','4s','4p','4d','4f', $
		  '5s','5p','5d','5f','5g','6s','6p','6d','6f','6g','6h', $
		  '7s','7p','7d','7f','7g','7h','7i', $
		   '8s','8p','8d','8f','8g','8h','8i','8k']
	
	shl_lval=[0,0,1,0,1,2,0,1,2,3,0,1,2,3,4,0,1,2,3,4,5,$
	0,1,2,3,4,5,6,0,1,2,3,4,5,6,7]

	openr,runit,infile,/get_lun
	openw,wunit,outfile,/get_lun
	
	line=''
	
	readf,runit,line,format='(A80)'
	line=strtrim(line)
	printf,wunit,line
	data=strarr(1)
	keepreading=1

readagain:
		
		readf,runit,line,format='(A80)'
		data=[data,strtrim(line)]
	if strmid(line,3,2) ne -1 then goto,readagain
	
	if (size(data))[1] lt 3 then begin
		free_lun,runit
		free_lun,wunit
		spawn,'rm -f '+outfile
		spawn,'cp '+infile+' '+outfile
		return
	endif
	
	lastline=data[(size(data))[1]-1]
	data=data[1:(size(data))[1]-2]
		
	ncfgs=(size(data))[1]
		
	norb=(size(orbitals))[1]
	
	occup=intarr(ncfgs,norb)
	
	for i=0,ncfgs-1 do begin
		for j=0,norb-1 do begin
			pos=strpos(strmid(data[i],32),orbitals[j])
			if pos ne -1 then begin
				occup[i,j]=fix(strmid(data[i],32+pos+2,2))
			endif
		end
	end
	
	
	
	parity=intarr(ncfgs)
	
	for i=0,ncfgs-1 do begin
		parity[i]=(-1)^(total(reform(occup[i,*])*shl_lval))
	end
	
	firstpar=where(parity eq parity[0])
	secondpar=where(parity ne parity[0])

	; could do sorting by which n/l is promoted at this point

	temp=[data[0]]
	
	if (size(firstpar))[1] gt 1 then begin
		for i=1,(size(firstpar))[1]-1 do begin
			temp=[temp,data[firstpar[i]]]
		end
	end
	if secondpar[0] ne -1 then begin
		for i=0,(size(secondpar))[1]-1 do begin
			temp=[temp,data[secondpar[i]]]
		end
	end

	if secondpar[0] ne -1 then begin
		if (size(firstpar))[1] eq 1 and (size(secondpar))[1] gt 1 then begin
			temp=[temp[1:ncfgs-1],temp[0]]
		endif
	endif
	
	for i=0,ncfgs-1 do begin
		printf,wunit,temp[i]
	end
			
	printf,wunit,lastline
	
	free_lun,runit
	free_lun,wunit
end
