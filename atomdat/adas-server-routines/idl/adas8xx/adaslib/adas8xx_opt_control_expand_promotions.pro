;-----------------------------------------------------------------------------
;+
;
; NAME     : adas8xx_opt_control_expand_promotions
;
; PURPOSE  : To control the expansion of the promotion, cycling through 
;            all possibilities
;
; CATEGORY : ADAS
;
; NOTES    :
;  
; USE      : 
;
; INPUT    :
; (I*4) z0_nuc           = nuclear charge
; (I*4) z_ion            = ion charge
; (C* ) config           = ground configuration string
; (R*8) ionpot           = ionisation potential
; (STRUCT) prom_rules:
;   (I*4) no_v_shl         = number of valence shells
;   (I*4) max_dn_v1        = max dn for v1
;   (I*4) min_dn_v1        = min dn for v1
;   (I*4) max_dl_v1        = max dl for v1
;   (I*4) min_dl_v1        = min dl for v1
;   (I*4) max_dn_v2        = max dn for v2
;   (I*4) min_dn_v2        = min dn for v2
;   (I*4) max_dl_v2        = max dl for v2
;   (I*4) min_dl_v2        = min dl for v2
;   (I*4) prom_cl          = 1/0 if prom_cl is allowed
;   (I*4) max_n_cl         = max n for cl
;   (I*4) min_n_cl         = min n for cl
;   (I*4) max_l_cl         = max l for cl
;   (I*4) min_l_cl         = min l for cl
;   (I*4) max_dn_cl        = max dn for cl
;   (I*4) min_dn_cl        = min dn for cl
;   (I*4) max_dl_cl        = max dl for cl
;   (I*4) min_dl_cl        = min dl for cl
;   (I*4) fill_n_v1        = fill all nl of v1
;   (I*4) fill_par         = fill only with opposite parity to v1
;   (I*4) for_tr_sel       = Cowan option for radiative transitions
;   (I*4) last_4f          = shift an electron valence shell to unfilled 4f as
;                            extra ground
;   (I*4) grd_cmplx        = include ground complex
; (STRUCT) promotion_results
;   (C* ) grd_cfg          = ground configuration string
;   (C* ) ex_cfg           = excited state configurations
; (C* ) configs          = excited state configuration before increasing
;                          promotion rules
; (STRUCT) base_rules      = output from adas8xx_opt_initialise_rules
; (I*4) next_rule        = next rule to change
; (I*4) major_rule       = if wanting to try combinations of rules,
;                          this determines the first one to change
;                          before cycling through the other options.
; OUTPUT   :
; (STRUCT) final_rules   = records results of each promotion (see prom_rules)
;                           with two additional elements:
;   (I*4) done             = 1 if the rule change was possible, 0 if not
;   (C* ) message          = text describing change made
;
;
;
; CALLS    :
;       adas8xx_opt_expand_levels
;
; CATEGORY:
;       Adas system.
;
; WRITTEN:
;       Adam Foster, University of Strathclyde,08-01-08
;
; MODIFIED:
;       1.1     Adam Foster

; VERSION:
;       1.1   08-01-08
; 
;-
;-----------------------------------------------------------------------------


PRO adas8xx_opt_control_expand_promotions, z0_nuc    = z0_nuc,         $
                                      z_ion          = z_ion,          $
                                      configurations = config,         $
                                      ionpot         = ionpot,         $
                                      prom_rules     = prom_rules,     $
                                promotion_results = promotion_results, $
                                      final_rules    = final_rules ,   $
                                      configs        = configs ,       $
                                      base_rules     = base_rules, $
                                      next_rule      = next_rule, $
                                      major_rule     = major_rule, $
                                      lun_verb       = lun_verb


; set up the various rules that need changed.


; n_iter=0

final_rules=1

; definition of the forcechanges

;  1: increase max_dn_v1 by 1
;  2: decrease min_dn_v1 by 1
;  3: increase max_dl_v1 by 1
;  4: decrease min_dl_v1 by 1
;  5: increase max_dn_v2 by 1
;  6: decrease min_dn_v2 by 1
;  7: increase max_dl_v2 by 1
;  8: decrease min_dl_v2 by 1
;  9: increase max_dn_cl by 1
; 10: decrease min_dn_cl by 1
; 11: increase max_dl_cl by 1
; 12: decrease min_dl_cl by 1
; 13: turn on ground complex

;IF not keyword_set(forcechange) then forcechange=[1]

; copy all the rules to avoid overwriting the originals....

tmp_z0_nuc      = z0_nuc
tmp_z_ion       = z_ion
tmp_config      = config
tmp_ionpot      = ionpot
tmp_no_v_shl    = prom_rules.no_v_shl
tmp_max_dn_v1   = prom_rules.max_dn_v1
tmp_min_dn_v1   = prom_rules.min_dn_v1
tmp_max_dl_v1   = prom_rules.max_dl_v1
tmp_min_dl_v1   = prom_rules.min_dl_v1
tmp_max_dn_v2   = prom_rules.max_dn_v2
tmp_min_dn_v2   = prom_rules.min_dn_v2
tmp_max_dl_v2   = prom_rules.max_dl_v2
tmp_min_dl_v2   = prom_rules.min_dl_v2
tmp_prom_cl     = prom_rules.prom_cl
tmp_max_n_cl    = prom_rules.max_n_cl
tmp_min_n_cl    = prom_rules.min_n_cl
tmp_max_l_cl    = prom_rules.max_l_cl
tmp_min_l_cl    = prom_rules.min_l_cl
tmp_max_dn_cl   = prom_rules.max_dn_cl
tmp_min_dn_cl   = prom_rules.min_dn_cl
tmp_max_dl_cl   = prom_rules.max_dl_cl
tmp_min_dl_cl   = prom_rules.min_dl_cl
tmp_fill_n_v1   = prom_rules.fill_n_v1
tmp_fill_par    = prom_rules.fill_par
tmp_for_tr_sel  = prom_rules.for_tr_sel
tmp_last_4f     = prom_rules.last_4f
tmp_grd_cmplx   = prom_rules.grd_cmplx
tmp_config_set  = [promotion_results.grd_cfg,configs]

; Identify if we need to force an extra promotion

two_configs   = (adas8xx_opt_check_parity(tmp_config_set,lun_verb=lun_verb)).dual
; need to invert this
IF two_configs EQ 1 then two_configs = 0 ELSE BEGIN
  if keyword_set(lun_verb) then begin
    printf, lun_verb, '------------------------------------------------------------------------'
    printf, lun_verb, '              Only 2 levels found: starting dipole changes              '
    printf, lun_verb, '------------------------------------------------------------------------'
  endif
  two_configs =1
ENDELSE
      
      
IF KEYWORD_SET(major_rule) THEN BEGIN
  imajor_rule=max(where(major_rule[*,0] NE -1))
  IF imajor_rule NE -1 THEN BEGIN
  forcechange=major_rule[imajor_rule,*]
    FOR iforcechange = 0, n_elements(forcechange)-1 DO BEGIN
    



      adas8xx_opt_expand_levels, z0_nuc      = tmp_z0_nuc,                 $
                                 z_ion       = tmp_z_ion,                  $
                                 config      = tmp_config ,                $
                                 ionpot      = tmp_ionpot ,                $
                                 prom_rules  = prom_rules,                 $
                                 done        = done,                      $
                                 final_rules = final_rules,               $
                                 forcechange = forcechange[iforcechange] ,  $
                                 base_rules  = base_rules,            $
                                 message     = message, $
                                 last_case   = last_case, $
                                 two_configs = two_configs

if keyword_set(lun_verb) then begin
  printf, lun_verb,'Here are the changes we made:'                               
  IF tmp_no_v_shl[0]  NE final_rules.no_v_shl THEN $
    printf, lun_verb, 'no_v_shl  :'+STRING(tmp_no_v_shl[0])  +' -> ' + STRING(final_rules.no_v_shl)
  IF tmp_max_dn_v1[0] NE final_rules.max_dn_v1 THEN $
    printf, lun_verb, 'max_dn_v1 :'+STRING(tmp_max_dn_v1[0]) +' -> ' + STRING(final_rules.max_dn_v1)
  IF tmp_min_dn_v1[0] NE final_rules.min_dn_v1 THEN $
    printf, lun_verb, 'min_dn_v1 :'+STRING(tmp_min_dn_v1[0]) +' -> ' + STRING(final_rules.min_dn_v1)
  IF tmp_max_dl_v1[0] NE final_rules.max_dl_v1 THEN $
    printf, lun_verb, 'max_dl_v1 :'+STRING(tmp_max_dl_v1[0]) +' -> ' + STRING(final_rules.max_dl_v1)
  IF tmp_min_dl_v1[0] NE final_rules.min_dl_v1 THEN $
    printf, lun_verb, 'min_dl_v1 :'+STRING(tmp_min_dl_v1[0]) +' -> ' + STRING(final_rules.min_dl_v1)
  IF tmp_max_dn_v2[0] NE final_rules.max_dn_v2 THEN $
    printf, lun_verb, 'max_dn_v2 :'+STRING(tmp_max_dn_v2[0]) +' -> ' + STRING(final_rules.max_dn_v2)
  IF tmp_min_dn_v2[0] NE final_rules.min_dn_v2 THEN $
    printf, lun_verb, 'min_dn_v2 :'+STRING(tmp_min_dn_v2[0]) +' -> ' + STRING(final_rules.min_dn_v2)
  IF tmp_max_dl_v2[0] NE final_rules.max_dl_v2 THEN $
    printf, lun_verb, 'max_dl_v2 :'+STRING(tmp_max_dl_v2[0]) +' -> ' + STRING(final_rules.max_dl_v2)
  IF tmp_min_dl_v2[0] NE final_rules.min_dl_v2 THEN $
    printf, lun_verb, 'min_dl_v2 :'+STRING(tmp_min_dl_v2[0]) +' -> ' + STRING(final_rules.min_dl_v2)
  IF tmp_prom_cl[0]   NE final_rules.prom_cl THEN $
    printf, lun_verb, 'prom_cl   :'+STRING(tmp_prom_cl[0])   +' -> ' + STRING(final_rules.prom_cl)
  IF tmp_max_n_cl[0]  NE final_rules.max_n_cl THEN $
    printf, lun_verb, 'max_n_cl  :'+STRING(tmp_max_n_cl[0])  +' -> ' + STRING(final_rules.max_n_cl)
  IF tmp_min_n_cl[0]  NE final_rules.min_n_cl THEN $
    printf, lun_verb, 'min_n_cl  :'+STRING(tmp_min_n_cl[0])  +' -> ' + STRING(final_rules.min_n_cl)
  IF tmp_max_l_cl[0]  NE final_rules.max_l_cl THEN $
    printf, lun_verb, 'max_l_cl  :'+STRING(tmp_max_l_cl[0])  +' -> ' + STRING(final_rules.max_l_cl)
  IF tmp_min_l_cl[0]  NE final_rules.min_l_cl THEN $
    printf, lun_verb, 'min_l_cl  :'+STRING(tmp_min_l_cl[0])  +' -> ' + STRING(final_rules.min_l_cl)
  IF tmp_max_dn_cl[0] NE final_rules.max_dn_cl THEN $
    printf, lun_verb, 'max_dn_cl :'+STRING(tmp_max_dn_cl[0]) +' -> ' + STRING(final_rules.max_dn_cl)
  IF tmp_min_dn_cl[0] NE final_rules.min_dn_cl THEN $  
    printf, lun_verb, 'min_dn_cl :'+STRING(tmp_min_dn_cl[0]) +' -> ' + STRING(final_rules.min_dn_cl)
  IF tmp_max_dl_cl[0] NE final_rules.max_dl_cl THEN $
    printf, lun_verb, 'max_dl_cl :'+STRING(tmp_max_dl_cl[0]) +' -> ' + STRING(final_rules.max_dl_cl)
  IF tmp_min_dl_cl[0] NE final_rules.min_dl_cl THEN $
    printf, lun_verb, 'min_dl_cl :'+STRING(tmp_min_dl_cl[0]) +' -> ' + STRING(final_rules.min_dl_cl)
  IF tmp_fill_n_v1[0] NE final_rules.fill_n_v1 THEN $
    printf, lun_verb, 'fill_n_v1 :'+STRING(tmp_fill_n_v1[0]) +' -> ' + STRING(final_rules.fill_n_v1)
  IF tmp_fill_par[0]  NE final_rules.fill_par THEN $
    printf, lun_verb, 'fill_par  :'+STRING(tmp_fill_par[0])  +' -> ' + STRING(final_rules.fill_par)
  IF tmp_for_tr_sel[0] NE final_rules.for_tr_sel THEN $
    printf, lun_verb, 'for_tr_sel:'+STRING(tmp_for_tr_sel[0])+' -> ' + STRING(final_rules.for_tr_sel)
  IF tmp_last_4f[0]   NE final_rules.last_4f THEN $
    printf, lun_verb, 'last_4f   :'+STRING(tmp_last_4f[0])   +' -> ' + STRING(final_rules.last_4f)
  IF tmp_grd_cmplx[0] NE final_rules.grd_cmplx THEN $
    printf, lun_verb, 'grd_cmplx :'+STRING(tmp_grd_cmplx[0] )+' -> ' + STRING(final_rules.grd_cmplx)
  
  printf, lun_verb, 'end of changes'
endif

    ; update 'tmp' variables
    
      tmp_z0_nuc    = final_rules.z0_nuc[0]        
      tmp_z_ion     = final_rules.z_ion[0]     
      tmp_config    = final_rules.config[0]    
      tmp_ionpot    = final_rules.ionpot[0]    
      tmp_no_v_shl  = final_rules.no_v_shl[0]  
      tmp_max_dn_v1 = final_rules.max_dn_v1[0] 
      tmp_min_dn_v1 = final_rules.min_dn_v1[0] 
      tmp_max_dl_v1 = final_rules.max_dl_v1[0] 
      tmp_min_dl_v1 = final_rules.min_dl_v1[0] 
      tmp_max_dn_v2 = final_rules.max_dn_v2[0] 
      tmp_min_dn_v2 = final_rules.min_dn_v2[0] 
      tmp_max_dl_v2 = final_rules.max_dl_v2[0] 
      tmp_min_dl_v2 = final_rules.min_dl_v2[0] 
      tmp_prom_cl   = final_rules.prom_cl[0]   
      tmp_max_n_cl  = final_rules.max_n_cl[0]  
      tmp_min_n_cl  = final_rules.min_n_cl[0]  
      tmp_max_l_cl  = final_rules.max_l_cl[0]  
      tmp_min_l_cl  = final_rules.min_l_cl[0]  
      tmp_max_dn_cl = final_rules.max_dn_cl[0] 
      tmp_min_dn_cl = final_rules.min_dn_cl[0] 
      tmp_max_dl_cl = final_rules.max_dl_cl[0] 
      tmp_min_dl_cl = final_rules.min_dl_cl[0] 
      tmp_fill_n_v1 = final_rules.fill_n_v1[0] 
      tmp_fill_par  = final_rules.fill_par[0]  
      tmp_for_tr_sel= final_rules.for_tr_sel[0]
      tmp_last_4f   = final_rules.last_4f[0]   
      tmp_grd_cmplx = final_rules.grd_cmplx[0]
      two_configs=0
    ENDFOR
  ENDIF
ENDIF

; now perform the obligatory promotion test :)


tmp_rules={no_v_shl    : tmp_no_v_shl ,              $
           max_dn_v1   : tmp_max_dn_v1 ,             $
           min_dn_v1   : tmp_min_dn_v1 ,             $
           max_dl_v1   : tmp_max_dl_v1 ,             $
           min_dl_v1   : tmp_min_dl_v1 ,             $
           max_dn_v2   : tmp_max_dn_v2 ,             $
           min_dn_v2   : tmp_min_dn_v2 ,             $
           max_dl_v2   : tmp_max_dl_v2 ,             $
           min_dl_v2   : tmp_min_dl_v2 ,             $
           prom_cl     : tmp_prom_cl ,               $
           max_n_cl    : tmp_max_n_cl ,              $
           min_n_cl    : tmp_min_n_cl ,              $
           max_l_cl    : tmp_max_l_cl ,              $
           min_l_cl    : tmp_min_l_cl ,              $
           max_dn_cl   : tmp_max_dn_cl ,             $
           min_dn_cl   : tmp_min_dn_cl ,             $
           max_dl_cl   : tmp_max_dl_cl ,             $
           min_dl_cl   : tmp_min_dl_cl ,             $
           fill_n_v1   : tmp_fill_n_v1 ,             $
           fill_par    : tmp_fill_par ,              $
           for_tr_sel  : tmp_for_tr_sel ,            $
           last_4f     : tmp_last_4f ,               $
           grd_cmplx   : tmp_grd_cmplx }

adas8xx_opt_expand_levels, z0_nuc      = tmp_z0_nuc,                 $
                           z_ion       = tmp_z_ion,                  $
                           config      = tmp_config ,                $
                           ionpot      = tmp_ionpot ,                $
                           prom_rules  = tmp_rules,                  $
                           done        = done,                      $
                           final_rules = final_rules,               $
                           forcechange = next_rule ,  $
                           base_rules  = base_rules,            $
                           message     = message, $
                           last_case   = last_case, $
                           two_configs = two_configs

         
; increment various counters
if keyword_set(major_rule) then begin

; if all the major rules are done:
  if n_elements(where(major_rule[*,0] NE -1)) EQ 1 then begin
    if last_case EQ 1 then next_rule = -1 else next_rule = next_rule +1
    
  endif else begin
    if last_case EQ 1 then begin
      major_rule[imajor_rule,*]=-1 
      next_rule=1
    endif ELSE begin
      next_rule = next_rule +1
    endelse                       
  endelse

endif else begin
  if last_case ne 1 then next_rule = next_rule +1 ELSE next_rule=-1
endelse



; prepare "final_rules" to return data.

final_rules= {z0_nuc      : final_rules.z0_nuc     , $
              z_ion       : final_rules.z_ion      , $
              config      : final_rules.config     , $
              ionpot      : final_rules.ionpot     , $
              no_v_shl    : final_rules.no_v_shl   , $
              max_dn_v1   : final_rules.max_dn_v1  , $
              min_dn_v1   : final_rules.min_dn_v1  , $
              max_dl_v1   : final_rules.max_dl_v1  , $
              min_dl_v1   : final_rules.min_dl_v1  , $
              max_dn_v2   : final_rules.max_dn_v2  , $
              min_dn_v2   : final_rules.min_dn_v2  , $
              max_dl_v2   : final_rules.max_dl_v2  , $
              min_dl_v2   : final_rules.min_dl_v2  , $
              prom_cl     : final_rules.prom_cl    , $
              max_n_cl    : final_rules.max_n_cl   , $
              min_n_cl    : final_rules.min_n_cl   , $
              max_l_cl    : final_rules.max_l_cl   , $
              min_l_cl    : final_rules.min_l_cl   , $
              max_dn_cl   : final_rules.max_dn_cl  , $
              min_dn_cl   : final_rules.min_dn_cl  , $
              max_dl_cl   : final_rules.max_dl_cl  , $
              min_dl_cl   : final_rules.min_dl_cl  , $
              fill_n_v1   : final_rules.fill_n_v1  , $
              fill_par    : final_rules.fill_par   , $
              for_tr_sel  : final_rules.for_tr_sel , $
              last_4f     : final_rules.last_4f    , $
              grd_cmplx   : final_rules.grd_cmplx  , $
              done        : final_rules.done       , $
              message     : final_rules.message  }



END
