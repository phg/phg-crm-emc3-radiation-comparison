;-----------------------------------------------------------------------------
;+
;
; NAME     : adas8xx_opt_promotions_control
;
; PURPOSE  : to optimise the promotion rules which will be used in 
;            adas808. This is the main control routine which calls
;            all the others. 
;            
;            The routine deals with one stage of one element. Multiple
;            runs will be required for an entire element (except H)
;
; CATEGORY : ADAS
;
; NOTES    :
;  
; USE      : 
;
; INPUT    :
;(I*4) n_levels_target = target number of levels (used to control size
;                        of ruleset created)
;(I*4) n_terms_target= target number of terms (used to control size
;                        of ruleset created)
;(I*4) n_configs_target= target number of configs (used to control size
;                        of ruleset created)
;(I*4) monitor_power   = switch to use either the line radiated power
;                        or simply the number of levels as the 
;                        promotion technique
;(STRUCT) plasma = plasma conditions.
;                THETA        : array of electron temperatures (eV)
;                INDX_THETA   : array of indices of theta to select Te
;                RHO          : array of electron density (cm-3)
;                INDX_RHO     : array of indices of rho to select Ne
;                NPIX         : array of pixels per wavelength region
;                WVLMIN       : lower bounds of wavelength region(s)
;                WVLMAX       : upper bounds of wavelength region(s)
;                INDX_WVL     : indicies of wavelength regions to use
;                THETA_NOSCALE: if 0, theta is assumed to be reduced Te
;                RHO_SCALE    : if 1, rho is assumed to be reduced Ne
;(I*4) z0_nuc              = atomic number
;(I*4) z_ion              = ion charge
;(C* ) a54file_in     = input adf54 file to start from
;(C* ) a54file_out    = file to add updated rules to.

; the following inputs are optional, and are an "old" set of rules for
; comparison. The intention is that if the old set of rules from
; the ADF54 were put in here, you could compare them with the result
; of this calculation and look for differences.

; (C* ) config         = ground configuration string - taken verbatim
;                        from ADF00 file.
; (R*8) ionpot         = ionisation potential


; within structure oldrules:
;   (I*4) no_v_shl       = number of valence shells
;   (I*4) max_dn_v1      = max dn for v1
;   (I*4) min_dn_v1      = min dn for v1
;   (I*4) max_dl_v1      = max dl for v1
;   (I*4) min_dl_v1      = min dl for v1
;   (I*4) max_dn_v2      = max dn for v2
;   (I*4) min_dn_v2      = min dn for v2
;   (I*4) max_dl_v2      = max dl for v2
;   (I*4) min_dl_v2      = min dl for v2
;   (I*4) prom_cl        = 1/0 if prom_cl is allowed
;   (I*4) max_n_cl       = max n for cl
;   (I*4) min_n_cl       = min n for cl
;   (I*4) max_l_cl       = max l for cl
;   (I*4) min_l_cl       = min l for cl
;   (I*4) max_dn_cl      = max dn for cl
;   (I*4) min_dn_cl      = min dn for cl
;   (I*4) max_dl_cl      = max dl for cl
;   (I*4) min_dl_cl      = min dl for cl
;   (I*4) fill_n_v1      = fill all nl of v1
;   (I*4) fill_par       = fill only with opposite parity to v1
;   (I*4) for_tr_sel     = Cowan option for radiative transitions 
;                          (default=3)
;   (I*4) last_4f        = shift an electron valence shell to unfilled 
;                          4f as extra ground
;   (I*4) grd_cmplx      = include ground complex
;
;
; (C* ) logfile        = file for storing record of which configuration
;                        choices at each step
; (I*4) useadf40power  = switch, if = 1 then use power from adf40 files
;                        instead of that from adf11 plt (default)
; (I*2) cowan_scale_factors = Array of 5 user specified slater parameters
;                        for Cowan code (integer percent, eg "95" = 0.95)
; (I*4)    year           : year id for data produced (default=40)
; (C* ) verbose        = file name for verbose output (if set)
;
;
; OUTPUTS   :
; struct outdata = returned data structure, containing all the promotion
;               rules above (eg. data.min_dn_cl) etc for the final
;               ruleset.
;
; CATEGORY:
;       Adas system.
;
; WRITTEN:
;       Adam Foster, University of Strathclyde,08-01-08
;
; MODIFIED:
;       1.1     Adam Foster
;
;       1.2     Adam Foster
;               - Moved "old" rules to structure oldrules.
;               - changed z0,z1 keywords to z0_nuc, z_ion
;
; VERSION:
;       1.1   08-01-08
;       1.2   18-12-08
; 
;-
;-----------------------------------------------------------------------------
PRO adas8xx_opt_promotions_control, n_levels_target = n_levels_target,$
                            n_configs_target= n_configs_target,       $
                            n_terms_target  = n_terms_target,       $
                            monitor_power   = monitor_power,          $ 
                            z0_nuc          = z0_nuc,                 $
                            z_ion           = z_ion,                  $
                            a54file_in      = a54file_in,             $
                            a54file_out     = a54file_out,            $
                            oldrules        = oldrules,               $
                            for_tr_sel      = for_tr_sel,             $ 
                            outdata         = outdata,                $
                            logfile         = logfile,                $
                            useadf40power   = useadf40power,          $
                            cowan_scale_factors=cowan_scale_factors,  $
                            plasma          = plasma,                 $
                            year            = year,                   $
                            verbose         = verbose

;-----------------------------------------------------------------------
; DECLARE HARD LIMIT ON CONFIGURATION NUMBER
;-----------------------------------------------------------------------
max_configurations=30

if keyword_set(verbose) then openw, lun_verb, verbose,/get_lun

IF NOT keyword_set(n_configs_target) AND $
   NOT keyword_set(n_terms_target) AND $
   NOT keyword_set(n_levels_target) THEN BEGIN
  
  print, 'WARNING: Neither n_configs_target, n_levels_target set nor '+$
         'n_terms_target set.'
  print, 'Optimising for max_configurations (='$
         +STRCOMPRESS(STRING(max_configurations))+')'
         
  n_levels_target=100000
  n_terms_target=100000
ENDIF
   
IF keyword_set(n_configs_target) THEN BEGIN
  IF n_configs_target LE max_configurations THEN BEGIN
    print, 'Using user supplied n_configs_target=', n_configs_target
    max_configurations = n_configs_target
  ENDIF ELSE BEGIN
    print, 'Ignoring user selected n_configs_target=',n_configs_target,$
           ', maximum value is hardwired to ',max_configurations
  ENDELSE
  
  IF NOT keyword_set(n_levels_target) THEN n_levels_target=100000

  IF NOT keyword_set(n_terms_target) THEN n_terms_target=100000
  
ENDIF

IF keyword_set(n_levels_target) AND $
   NOT keyword_set(n_terms_target) THEN BEGIN
;   print, 'setting nterms target to 100000'
   n_terms_target=100000
endif

IF keyword_set(n_terms_target) AND $
   NOT keyword_set(n_levels_target) THEN BEGIN
;   print, 'setting nlevels target'
   n_levels_target=100000
endif

if keyword_set(verbose) then begin
  printf, lun_verb, 'n_levels_target=',n_levels_target
  printf, lun_verb, 'n_terms_target=',n_terms_target
  printf, lun_verb, 'n_configs_target=',n_terms_target
  printf, lun_verb, 'monitor_power=',monitor_power
endif
;-----------------------------------------------------------------------
; declare useful variables
;-----------------------------------------------------------------------

nel=z0_nuc-z_ion  ; number of electrons

;-----------------------------------------------------------------------
; open logging file if present
;-----------------------------------------------------------------------
IF keyword_set(logfile) THEN BEGIN
  openw, loglun, logfile, /get_lun
  print, logfile
  IF keyword_set(monitor_power) THEN BEGIN
    printf, loglun, 'monitor power: '+strcompress(string(monitor_power),/remove_all)
    IF keyword_set(useadf40power) THEN $
      printf, loglun, 'Monitoring power using adf40 file' ELSE $
      printf, loglun, 'Monitoring power using adf11 file'
      
    printf, loglun,'ID        No     Change                               '+$
                   '      n_lev n_ter n_con'+$
    '   Power(adf11)    Power(adf40)    Dp11/Dnlev      Dp40/Dnlev   '+$
    'Selected?'
    logformat='(I1,".",I2.2,".",I2.2,X,I4,X,A45, I8,I5,I5,4E16.8,3X,A1)'
  ENDIF ELSE BEGIN
    printf, loglun, 'monitor power: 0'
    printf, loglun,'ID        No     Change                                '+$ 
                   '      n_lev n_ter n_con Selected?'
    
    logformat='(I1,".",I2.2,".",I2.2,X,I4,X,A45, I8,I5,I5,E16.8,3X,A1)'
  ENDELSE

ENDIF

;-----------------------------------------------------------------------
; Using the ground configuration, set up some rules for the coming 
; promotions
;-----------------------------------------------------------------------
IF NOT keyword_set(config) THEN BEGIN
; get ground configuration
  read_adf00,z_nuc=z0_nuc,z_ion=z_ion,config=config,ionpot=tmpionpot
  IF NOT keyword_set(ionpot) THEN ionpot=tmpionpot
ENDIF


base_rules=adas8xx_opt_initialise_rules(config)

;-----------------------------------------------------------------------
; currently for_tr_sel is not set by this code and must be inherited
; from calling program
;-----------------------------------------------------------------------

IF NOT keyword_set(for_tr_sel) THEN BEGIN
  if keyword_set(verbose) then begin
    printf,lun_verb, 'for_tr_sel not specified: setting to 3 (default)'
  endif
  for_tr_sel=3
ENDIF


;-----------------------------------------------------------------------
; If a full array of initial values of the variables has been passed in
; then calculate number of levels with these for comparsion at the end
;-----------------------------------------------------------------------

old_promotion_ruleset=0

if keyword_set(verbose) then begin
  printf,lun_verb, 'setting old_promotion_ruleset'
endif

IF keyword_set(oldrules) THEN old_promotion_ruleset=1
  IF keyword_set(monitor_power) THEN BEGIN
;-----------------------------------------------------------------------
; Set up default values of Ne, Te etc in plasma structure
;-----------------------------------------------------------------------
    IF NOT keyword_set(plasma) THEN BEGIN
      print, 'ADAS8XX_OPT_PROMOTIONS_CONTROL WARNING: plasma '+$
             'parameters undefined, using default values'
      theta=[ionpot*3d0]
      indx_theta=[0]
      rho=[4d13]
      indx_rho=[0]
      npix=[128]
      wvlmin=[1.0]
      wvlamx=[10000.0]
      indx_wvl=[0]
      theta_noscale=[1]
      rho_scale=[0]
    ENDIF ELSE BEGIN


      if (where(strupcase(tag_names(plasma)) EQ $
                         'THETA'))[0] NE -1 THEN BEGIN
        theta=plasma.theta
      endif
      if (where(strupcase(tag_names(plasma)) EQ $
                         'INDX_THETA'))[0] NE -1 THEN BEGIN
        indx_theta=plasma.indx_theta
      endif
      if (where(strupcase(tag_names(plasma)) EQ $
                         'RHO'))[0] NE -1 THEN BEGIN
        RHO=plasma.RHO
      endif
      if (where(strupcase(tag_names(plasma)) EQ $
                         'INDX_RHO'))[0] NE -1 THEN BEGIN
        INDX_RHO=plasma.INDX_RHO
      endif
      if (where(strupcase(tag_names(plasma)) EQ $
                         'NPIX'))[0] NE -1 THEN BEGIN
        NPIX=plasma.NPIX
      endif
      if (where(strupcase(tag_names(plasma)) EQ $
                         'WVLMIN'))[0] NE -1 THEN BEGIN
        WVLMIN=plasma.WVLMIN
      endif
      if (where(strupcase(tag_names(plasma)) EQ $
                         'WVLMAX'))[0] NE -1 THEN BEGIN
        WVLMAX=plasma.WVLMAX
      endif
      if (where(strupcase(tag_names(plasma)) EQ $
                         'INDX_WVL'))[0] NE -1 THEN BEGIN
        INDX_WVL=plasma.INDX_WVL
      endif
      if (where(strupcase(tag_names(plasma)) EQ $
                         'THETA_NOSCALE'))[0] NE -1 THEN BEGIN
        THETA_NOSCALE=plasma.THETA_NOSCALE
      endif
      if (where(strupcase(tag_names(plasma)) EQ $
                         'RHO_SCALE'))[0] NE -1 THEN BEGIN
        RHO_SCALE=plasma.RHO_SCALE
      endif

      ; now check for presence of each setting and appropriate value:
      
      if n_elements(theta) EQ 0 then begin
        print, 'ADAS8XX_OPT_PROMOTIONS_CONTROL WARNING: theta '+$
             'parameters undefined, using default value'
        theta=[ionpot*3d0]
        theta_noscale=[1]
        indx_theta=[0]
      endif
      
      if n_elements(indx_theta) EQ 0 then begin
        if n_elements(theta) EQ 1 then begin
          indx_theta = [0] ; if only 1 theta, use it
        endif else begin
          print, 'ADAS8XX_OPT_PROMOTIONS_CONTROL ERROR: indx_theta '+$
             'is undefined'
          stop
        endelse
      endif   

      if n_elements(theta_noscale) EQ 0 then begin
        print, 'ADAS8XX_OPT_PROMOTIONS_CONTROL ERROR: theta_noscale '+$
           'is undefined'
        stop
      endif

      if n_elements(rho) EQ 0 then begin
        print, 'ADAS8XX_OPT_PROMOTIONS_CONTROL WARNING: rho '+$
             'parameters undefined, using default value'
        rho=[ionpot*3d0]
        rho_scale=[1]
        indx_rho=[0]
      endif
      
      if n_elements(indx_rho) EQ 0 then begin
        if n_elements(rho) EQ 1 then begin
          indx_rho = [0] ; if only 1 rho, use it
        endif else begin
          print, 'ADAS8XX_OPT_PROMOTIONS_CONTROL ERROR: indx_rho '+$
             'is undefined'
          stop
        endelse
      endif

      if n_elements(rho_scale) EQ 0 then begin
        print, 'ADAS8XX_OPT_PROMOTIONS_CONTROL ERROR: rho_scale '+$
           'is undefined'
        stop
      endif
      
      if n_elements(wvlmax) EQ 0 then begin
        wvlmax=[9000d0]
      endif
      if n_elements(wvlmin) EQ 0 then begin
        wvlmin=[10d0]
      endif
      if n_elements(npix) EQ 0 then begin
        npix=[128]
      endif
      if n_elements(indx_wvl) EQ 0 then begin
        indx_wvl=[0]
      endif

    ENDELSE
    
    plasma= {theta         :theta,         $
             indx_theta    :indx_theta,    $
             rho           :rho,           $
             indx_rho      :indx_rho,      $
             npix          :npix,          $ 
             wvlmin        :wvlmin,        $ 
             wvlmax        :wvlmax,        $
             indx_wvl      :indx_wvl,      $
             theta_noscale :theta_noscale, $
             rho_scale     :rho_scale     }    

endif
;-----------------------------------------------------------------------
; calculate number of levels in old promotion ruleset
;-----------------------------------------------------------------------
if keyword_set(oldrules) THEN BEGIN
  adas8xx_promotions,z0_nuc     = z0_nuc,                             $
                     z_ion      = z_ion,                              $
                     ionpot     = ionpot,                             $
                     prom_rules   = oldrules,                           $
                     promotion_results = promotion_results

  message=['starting']

  two_configs=0



; Inititalise 3 arrays to store important information: 
; the number of levels for each ruleset (sum_levels)
; the number of configurations for each ruleset (sum_configs)
; and the excited configurations of each set (ex_cfg)
  sum_levels=[TOTAL(promotion_results.no_levels)]
  sum_terms=[TOTAL(promotion_results.no_terms)]
  sum_configs=[TOTAL(promotion_results.no_configs)]
  configs=PTRARR(1)
  configs[0]=PTR_NEW(promotion_results.ex_cfg)

; if monitor_power is set, there are more arrays to initialise:

  no_levels=promotion_results.no_levels
  no_configs=promotion_results.no_configs
  no_terms=promotion_results.no_terms

  IF keyword_set(monitor_power) THEN BEGIN
;-----------------------------------------------------------------------
; Set up default values of Ne, Te etc in plasma structure
;-----------------------------------------------------------------------
    ;        struct_theta=[-1]
;        struct_indx_theta=[-1]
;        if keyword_set(plasma) then begin
;          if (where(strupcase(tag_names(plasma)) EQ $
;                         'THETA_SC'))[0] NE -1 THEN BEGIN
;            struct_theta=plasma.theta_sc
;          endif else if (where(strupcase(tag_names(plasma)) EQ $
;                         'THETA'))[0] NE -1 THEN BEGIN
;            if (where(strupcase(tag_names(plasma)) EQ $
;                         'THETA_NOSCALE'))[0] NE -1 THEN BEGIN
;              struct_theta=plasma.theta
;              if plasma.theta_noscale EQ 1 then begin
;                theta_sc = theta/(j+1.0)^2
;              ENDIF
;            ENDIF
;          ENDIF
;          if (where(strupcase(tag_names(plasma)) EQ $
;                         'INDX_THETA'))[0] NE -1 THEN BEGIN
;            struct_indx_theta=plasma.indx_theta
;          endif
;
;        ENDIF

;      ENDELSE  
    
    IF sum_configs LT 2 THEN BEGIN
      total_power = dblarr(1)
      total_adf40_power = dblarr(1)
      total_adf11_power = dblarr(1)
      if keyword_set(verbose) then begin
        PRINTF,lun_verb, 'Only 1 configuration - setting power to 0'
      endif
    ENDIF ELSE IF (adas8xx_opt_check_parity([promotion_results.grd_cfg,(*configs[0])],lun_verb=lun_verb)).dual EQ 0 THEN BEGIN
      total_power = dblarr(1)
      total_adf40_power = dblarr(1)
      total_adf11_power = dblarr(1)
      if keyword_set(verbose) then begin
        PRINTF,lun_verb, 'Only 1 parity present - setting power to 0'
      endif
    ENDIF ELSE BEGIN
      if keyword_set(verbose) then begin
        PRINTF,lun_verb, '*** Starting power run with initial (i.e. old) values ***'
      endif
      total_power = dblarr(1)
      total_adf40_power = dblarr(1)
      total_adf11_power = dblarr(1)
      adas8xx_opt_get_total_line_power, z0_nuc     = z0_nuc,            $
                                     nel           = nel,               $
                                     z_ion         = z_ion,             $
                                     ionpot        = ionpot,            $
                                     for_tr_sel    = for_tr_sel,        $
                                     promotion_results=promotion_results,$
                                     plasma        = plasma,            $
                                     fpecdata      = fpecdata,          $
                                cowan_scale_factors=cowan_scale_factors,$
                                     year          = year,              $
                                     lun_verb      = lun_verb
 
      IF keyword_set(useadf40power) then $
        total_power[0]=fpecdata.a40_total_power ELSE $
        total_power[0]=fpecdata.a11_total_power
  
      total_adf11_power[0]=fpecdata.a11_total_power
      total_adf40_power[0]=fpecdata.a40_total_power
  
    ENDELSE                                   
                                  
  ENDIF
  ionpot     = [ionpot]
  no_v_shl   = [oldrules.no_v_shl]
  max_dn_v1  = [oldrules.max_dn_v1]
  min_dn_v1  = [oldrules.min_dn_v1]
  max_dl_v1  = [oldrules.max_dl_v1]
  min_dl_v1  = [oldrules.min_dl_v1]
  max_dn_v2  = [oldrules.max_dn_v2]
  min_dn_v2  = [oldrules.min_dn_v2]
  max_dl_v2  = [oldrules.max_dl_v2]
  min_dl_v2  = [oldrules.min_dl_v2]
  prom_cl    = [oldrules.prom_cl]
  max_n_cl   = [oldrules.max_n_cl]
  min_n_cl   = [oldrules.min_n_cl]
  max_l_cl   = [oldrules.max_l_cl]
  min_l_cl   = [oldrules.min_l_cl]
  max_dn_cl  = [oldrules.max_dn_cl]
  min_dn_cl  = [oldrules.min_dn_cl]
  max_dl_cl  = [oldrules.max_dl_cl]
  min_dl_cl  = [oldrules.min_dl_cl]
  fill_n_v1  = [oldrules.fill_n_v1]
  fill_par   = [oldrules.fill_par]
  for_tr_sel = [oldrules.for_tr_sel]
  last_4f    = [oldrules.last_4f]
  grd_cmplx  = [oldrules.grd_cmplx]
  message    = ['starting']
  sum_levels=[TOTAL(no_levels)]
  sum_terms=[TOTAL(no_terms)]
  sum_configs=[TOTAL(no_configs)]

ENDIF ELSE BEGIN
  z0_nuc         = [z0_nuc]
  z_ion         = [z_ion]
  configs    = PTRARR(1)
  configs[0] = PTR_NEW('')
  ionpot     = [ionpot]
  no_v_shl   = [0]
  max_dn_v1  = [0]
  min_dn_v1  = [0]
  max_dl_v1  = [0]
  min_dl_v1  = [0]
  max_dn_v2  = [0]
  min_dn_v2  = [0]
  max_dl_v2  = [0]
  min_dl_v2  = [0]
  prom_cl    = [0]
  max_n_cl   = [0]
  min_n_cl   = [0]
  max_l_cl   = [0]
  min_l_cl   = [0]
  max_dn_cl  = [0]
  min_dn_cl  = [0]
  max_dl_cl  = [0]
  min_dl_cl  = [0]
  fill_n_v1  = [0]
  fill_par   = [0]
  for_tr_sel = [0]
  last_4f    = [0]
  grd_cmplx  = [0]
  no_configs = [0]
  no_terms   = [0]
  no_levels  = [0]
  message    = ['starting']
  sum_levels=[TOTAL(no_levels)]
  sum_terms=[TOTAL(no_terms)]
  sum_configs=[TOTAL(no_configs)]

  
  IF keyword_set(monitor_power) THEN BEGIN
    total_adf40_power=[0]
    total_adf11_power=[0]
    total_power=[0]
  ENDIF  
ENDELSE










;-----------------------------------------------------------------------
; set up minimum values as a reference case.
;-----------------------------------------------------------------------

init_z0_nuc     = [z0_nuc]
init_z_ion         = [z_ion]
init_nel        = [nel]
init_config     = [config]
init_ionpot     = [ionpot]
init_no_v_shl   = [base_rules.no_v_shl]
init_max_dn_v1  = [0]
init_min_dn_v1  = [0]
init_max_dl_v1  = [0]
init_min_dl_v1  = [0]
init_max_dn_v2  = [0]
init_min_dn_v2  = [0]
init_max_dl_v2  = [0]
init_min_dl_v2  = [0]
init_prom_cl    = [base_rules.prom_cl]
init_max_n_cl   = [base_rules.cl_n]
init_min_n_cl   = [base_rules.cl_n]
init_max_l_cl   = [base_rules.cl_l]
init_min_l_cl   = [base_rules.cl_l]
init_max_dn_cl  = [0]
init_min_dn_cl  = [0]
init_max_dl_cl  = [0]
init_min_dl_cl  = [0]
init_fill_n_v1  = [0]
init_fill_par   = [0] 
init_for_tr_sel = [for_tr_sel]
init_last_4f    = [0]   
init_grd_cmplx  = [0]
init_sum_levels = [total(no_levels)]
init_sum_terms = [total(no_terms)]
init_sum_configs= [total(no_configs)]
init_message    = ['initial setup']
best_cases=[0]
;-----------------------------------------------------------------------
; exitstatus will keep track of whether each result is valid or not.
; 0= valid, -1 is invalid., -2 is it crashed during an 810 run
;
; set to ignore initial settings
;-----------------------------------------------------------------------

exitstatus=[-1]
;-----------------------------------------------------------------------
; i_initial_index will keep track
i_initial_index=[-1]

;-----------------------------------------------------------------------
; There will be 3 different starting points for closed shells, if they
; exist. If there is only 1, then there can be only 1...
;-----------------------------------------------------------------------

IF base_rules.prom_cl EQ 0 THEN number_of_cl_iterations=1 ELSE $
                                number_of_cl_iterations=3
  if keyword_set(verbose) then begin
    PRINTF, lun_verb,'BASE_Rules.prom_cl=',base_rules.prom_cl
  endif




;-----------------------------------------------------------------------
; Now that we have the reference case, commence a loop where everything
; is gradually increased. There will be X changes tried per loop, with 
; combinations of grd_cmplx on/off, prom_cl on/off, active_v2 on/off, 
; and all in different orders for activation
;
; Also, if there is no dipole transition present, the code will force 
; one in it's first iteration
;-----------------------------------------------------------------------
  


FOR n_cl_promotions=1,number_of_cl_iterations DO BEGIN

  if keyword_set(verbose) then begin
    printf,lun_verb,'-----------------------------------------------------------------------'
    printf,lun_verb,' Beginning case:'+STRCOMPRESS(STRING(n_cl_promotions),/REMOVE_ALL)
    printf,lun_verb,'-----------------------------------------------------------------------'
  endif
  firstrun=1
  iref=n_elements(z0_nuc)

;-----------------------------------------------------------------------
; copy the initial rules to a new column in all the arrays
;-----------------------------------------------------------------------


  z0_nuc     = [z0_nuc,     init_z0_nuc]
  z_ion         = [z_ion,         init_z_ion]
  nel        = [nel,        init_nel]
  config     = [config,     init_config]
  ionpot     = [ionpot,     init_ionpot]
  no_v_shl   = [no_v_shl,   init_no_v_shl]
  max_dn_v1  = [max_dn_v1,  init_max_dn_v1]
  min_dn_v1  = [min_dn_v1,  init_min_dn_v1]
  max_dl_v1  = [max_dl_v1,  init_max_dl_v1]
  min_dl_v1  = [min_dl_v1,  init_min_dl_v1]
  max_dn_v2  = [max_dn_v2,  init_max_dn_v2]
  min_dn_v2  = [min_dn_v2,  init_min_dn_v2]
  max_dl_v2  = [max_dl_v2,  init_max_dl_v2]
  min_dl_v2  = [min_dl_v2,  init_min_dl_v2]
  prom_cl    = [prom_cl,    init_prom_cl]
  max_n_cl   = [max_n_cl,   init_max_n_cl]
  min_n_cl   = [min_n_cl,   init_min_n_cl]
  max_l_cl   = [max_l_cl,   init_max_l_cl]
  min_l_cl   = [min_l_cl,   init_min_l_cl]
  max_dn_cl  = [max_dn_cl,  init_max_dn_cl]
  min_dn_cl  = [min_dn_cl,  init_min_dn_cl]
  max_dl_cl  = [max_dl_cl,  init_max_dl_cl]
  min_dl_cl  = [min_dl_cl,  init_min_dl_cl]
  fill_n_v1  = [fill_n_v1,  init_fill_n_v1]
  fill_par   = [fill_par,   init_fill_par]
  for_tr_sel = [for_tr_sel, init_for_tr_sel]
  last_4f    = [last_4f,    init_last_4f]
  grd_cmplx  = [grd_cmplx,  init_grd_cmplx]
  message    = [message,    init_message]

;-----------------------------------------------------------------------
; now submit this to adas_8xx_promotion_rules to increase closed shells
; from which promotions can occur (if required)
;-----------------------------------------------------------------------
  IF n_cl_promotions GT 1 THEN BEGIN
    prom_rules={no_v_shl     : no_v_shl[iref],$
                max_dn_v1    : max_dn_v1[iref],$
                min_dn_v1    : min_dn_v1[iref],$
                max_dl_v1    : max_dl_v1[iref],$
                min_dl_v1    : min_dl_v1[iref],$
                max_dn_v2    : max_dn_v2[iref],$
                min_dn_v2    : min_dn_v2[iref],$
                max_dl_v2    : max_dl_v2[iref],$
                min_dl_v2    : min_dl_v2[iref],$
                prom_cl      : prom_cl[iref],$
                max_n_cl     : max_n_cl[iref],$
                min_n_cl     : min_n_cl[iref],$
                max_l_cl     : max_l_cl[iref],$
                min_l_cl     : min_l_cl[iref],$
                max_dn_cl    : max_dn_cl[iref],$
                min_dn_cl    : min_dn_cl[iref],$
                max_dl_cl    : max_dl_cl[iref],$
                min_dl_cl    : min_dl_cl[iref],$
                fill_n_v1    : fill_n_v1[iref],$
                fill_par     : fill_par[iref],$
                for_tr_sel   : for_tr_sel[iref],$
                last_4f      : last_4f[iref],$
                grd_cmplx    : grd_cmplx[iref]}

    adas8xx_opt_expand_levels, z0_nuc   = z0_nuc[iref],         $
                           z_ion        = z_ion[iref],          $
                           config       = config [iref],        $
                           ionpot       = ionpot [iref],        $
                           prom_rules   = prom_rules,           $
                           done         = done,                 $
                           final_rules  = final_rules,          $
                           base_rules    = base_rules,          $
                           n_cl_promotions = n_cl_promotions-1

;-----------------------------------------------------------------------
; Update this reference case to deal with changes to closed shells
;-----------------------------------------------------------------------

    if keyword_set(verbose) then begin

      printf, lun_verb, '-----------------------------------------------------------------------'
      printf, lun_verb, ' after having extended closed shells, rules are now:'
      printf, lun_verb, '-----------------------------------------------------------------------'


      printf, lun_verb, 'no_v_shl  :'+STRING(no_v_shl[iref])  +' -> ' + STRING(final_rules.no_v_shl)
      printf, lun_verb, 'max_dn_v1 :'+STRING(max_dn_v1[iref]) +' -> ' + STRING(final_rules.max_dn_v1)
      printf, lun_verb, 'min_dn_v1 :'+STRING(min_dn_v1[iref]) +' -> ' + STRING(final_rules.min_dn_v1)
      printf, lun_verb, 'max_dl_v1 :'+STRING(max_dl_v1[iref]) +' -> ' + STRING(final_rules.max_dl_v1)
      printf, lun_verb, 'min_dl_v1 :'+STRING(min_dl_v1[iref]) +' -> ' + STRING(final_rules.min_dl_v1)
      printf, lun_verb, 'max_dn_v2 :'+STRING(max_dn_v2[iref]) +' -> ' + STRING(final_rules.max_dn_v2)
      printf, lun_verb, 'min_dn_v2 :'+STRING(min_dn_v2[iref]) +' -> ' + STRING(final_rules.min_dn_v2)
      printf, lun_verb, 'max_dl_v2 :'+STRING(max_dl_v2[iref]) +' -> ' + STRING(final_rules.max_dl_v2)
      printf, lun_verb, 'min_dl_v2 :'+STRING(min_dl_v2[iref]) +' -> ' + STRING(final_rules.min_dl_v2)
      printf, lun_verb, 'prom_cl   :'+STRING(prom_cl[iref])   +' -> ' + STRING(final_rules.prom_cl)
      printf, lun_verb, 'max_n_cl  :'+STRING(max_n_cl[iref])  +' -> ' + STRING(final_rules.max_n_cl)
      printf, lun_verb, 'min_n_cl  :'+STRING(min_n_cl[iref])  +' -> ' + STRING(final_rules.min_n_cl)
      printf, lun_verb, 'max_l_cl  :'+STRING(max_l_cl[iref])  +' -> ' + STRING(final_rules.max_l_cl)
      printf, lun_verb, 'min_l_cl  :'+STRING(min_l_cl[iref])  +' -> ' + STRING(final_rules.min_l_cl)
      printf, lun_verb, 'max_dn_cl :'+STRING(max_dn_cl[iref]) +' -> ' + STRING(final_rules.max_dn_cl)
      printf, lun_verb, 'min_dn_cl :'+STRING(min_dn_cl[iref]) +' -> ' + STRING(final_rules.min_dn_cl)
      printf, lun_verb, 'max_dl_cl :'+STRING(max_dl_cl[iref]) +' -> ' + STRING(final_rules.max_dl_cl)
      printf, lun_verb, 'min_dl_cl :'+STRING(min_dl_cl[iref]) +' -> ' + STRING(final_rules.min_dl_cl)
      printf, lun_verb, 'fill_n_v1 :'+STRING(fill_n_v1[iref]) +' -> ' + STRING(final_rules.fill_n_v1)
      printf, lun_verb, 'fill_par  :'+STRING(fill_par[iref])  +' -> ' + STRING(final_rules.fill_par)
      printf, lun_verb, 'for_tr_sel:'+STRING(for_tr_sel[iref])+' -> ' + STRING(final_rules.for_tr_sel)
      printf, lun_verb, 'last_4f   :'+STRING(last_4f[iref])   +' -> ' + STRING(final_rules.last_4f)
      printf, lun_verb, 'grd_cmplx :'+STRING(grd_cmplx [iref])+' -> ' + STRING(final_rules.grd_cmplx)
    endif


    z0_nuc[iref] =        final_rules.z0_nuc
    z_ion[iref] =        final_rules.z_ion
    nel[iref] =       nel[0]
    config[iref] =    final_rules.config
    ionpot[iref] =    final_rules.ionpot
    no_v_shl[iref] =  final_rules.no_v_shl
    max_dn_v1[iref] = final_rules.max_dn_v1
    min_dn_v1[iref] = final_rules.min_dn_v1
    max_dl_v1[iref] = final_rules.max_dl_v1
    min_dl_v1[iref] = final_rules.min_dl_v1
    max_dn_v2[iref] = final_rules.max_dn_v2
    min_dn_v2[iref] = final_rules.min_dn_v2
    max_dl_v2[iref] = final_rules.max_dl_v2
    min_dl_v2[iref] = final_rules.min_dl_v2
    prom_cl[iref] =   final_rules.prom_cl
    max_n_cl[iref] =  final_rules.max_n_cl
    min_n_cl[iref] =  final_rules.min_n_cl
    max_l_cl[iref] =  final_rules.max_l_cl
    min_l_cl[iref] =  final_rules.min_l_cl
    max_dn_cl[iref] = final_rules.max_dn_cl
    min_dn_cl[iref] = final_rules.min_dn_cl
    max_dl_cl[iref] = final_rules.max_dl_cl
    min_dl_cl[iref] = final_rules.min_dl_cl
    fill_n_v1[iref] = final_rules.fill_n_v1
    fill_par[iref] =  final_rules.fill_par
    for_tr_sel[iref] =final_rules.for_tr_sel
    last_4f[iref] =   final_rules.last_4f
    grd_cmplx[iref] = final_rules.grd_cmplx
    message[iref]   = final_rules.message
  
  
  ENDIF

    prom_rules={config       : config[iref],$
                no_v_shl     : no_v_shl[iref],$
                max_dn_v1    : max_dn_v1[iref],$
                min_dn_v1    : min_dn_v1[iref],$
                max_dl_v1    : max_dl_v1[iref],$
                min_dl_v1    : min_dl_v1[iref],$
                max_dn_v2    : max_dn_v2[iref],$
                min_dn_v2    : min_dn_v2[iref],$
                max_dl_v2    : max_dl_v2[iref],$
                min_dl_v2    : min_dl_v2[iref],$
                prom_cl      : prom_cl[iref],$
                max_n_cl     : max_n_cl[iref],$
                min_n_cl     : min_n_cl[iref],$
                max_l_cl     : max_l_cl[iref],$
                min_l_cl     : min_l_cl[iref],$
                max_dn_cl    : max_dn_cl[iref],$
                min_dn_cl    : min_dn_cl[iref],$
                max_dl_cl    : max_dl_cl[iref],$
                min_dl_cl    : min_dl_cl[iref],$
                fill_n_v1    : fill_n_v1[iref],$
                fill_par     : fill_par[iref],$
                for_tr_sel   : for_tr_sel[iref],$
                last_4f      : last_4f[iref],$
                grd_cmplx    : grd_cmplx[iref]}

  adas8xx_promotions,  z0_nuc    = z0_nuc[iref],    $
                       z_ion     = z_ion[iref],     $
                       ionpot     = ionpot[iref],    $
                       prom_rules = prom_rules,      $
                       promotion_results=promotion_results


  tmp=configs

  configs=PTRARR(n_elements(tmp) +1)

  FOR iconf=0, n_elements(tmp)-1 DO BEGIN
    tmp2=*tmp[iconf]
    configs[iconf]=PTR_NEW(tmp2, /no_copy)
  ENDFOR
  tmp=0
  tmp2=0
  configs[n_elements(configs)-1]=PTR_NEW(promotion_results.ex_cfg)
  
  i_initial_index=[i_initial_index,n_elements(configs)]

;-----------------------------------------------------------------------
; commence variety of checks that this configuration set is ok.
;-----------------------------------------------------------------------

  adas8xx_opt_check_valid_promotion_set, $
                        max_configurations = max_configurations, $
                        monitor_power      = monitor_power, $
                        valid_rules        = valid_rules, $
                        z0_nuc             = z0_nuc[iref], $
                        nel                = nel[iref],    $
                        z_ion              = z_ion[iref],    $
                        ionpot             = ionpot[iref],    $
                        for_tr_sel         = for_tr_sel[iref],  $
                        promotion_results  = promotion_results, $
                        plasma             = plasma,      $
                        fpecdata           = fpecdata,   $
                        configs            = configs,   $
                        exitstatus         = exitstatus, $
                        total_power        = total_power, $
                        total_adf40_power  = total_adf40_power, $
                        total_adf11_power  = total_adf11_power, $
                        sum_configs        = sum_configs, $
                        sum_terms        = sum_terms, $
                        sum_levels         = sum_levels, $
                        old_promotion_ruleset=old_promotion_ruleset, $
                        cowan_scale_factors=cowan_scale_factors, $
                        year               = year,               $
                        lun_verb           = lun_verb
;-----------------------------------------------------------------------
; define arrays to record all the different chosen changes
;-----------------------------------------------------------------------

; record this level in the logfile if it exists
    IF keyword_set(logfile) THEN BEGIN  
    
      IF keyword_set(monitor_power) THEN BEGIN

        printf, loglun, FORMAT=logformat, n_cl_promotions, 0,0,$
                iref, $
                '                              ',$
                sum_levels[iref]  ,$
                sum_terms[iref]  ,$
                sum_configs[iref]  ,$
                total_adf11_power[iref]  ,$
                total_adf40_power[iref]  ,$
                0d0,0d0,$
                'Y'

                        
      ENDIF ELSE BEGIN
        printf, loglun, FORMAT=logformat, n_cl_promotions, 0,0,$
                iref,$
                '                              ',$
                sum_levels[iref]  ,$
                sum_terms[iref]  ,$
                sum_configs[iref]  ,$
                0d0,$
                'Y'
      ENDELSE  
    ENDIF


  
  loopcount=0
  
  next_rule=[-1]
  major_rule = [-1]
  old_irange=[-1,-1]
  
; check we haven't already overstepped the bounds of the system
  done = 1
  IF sum_configs[iref] GE MAX_CONFIGURATIONS OR  $
     sum_terms[iref] GT n_terms_target OR $
     sum_levels[iref] GT n_levels_target THEN done =0


;-----------------------------------------------------------------------
; BEGIN PROMOTION LOOP
;-----------------------------------------------------------------------

  WHILE done NE 0 DO BEGIN
    if keyword_set(verbose) then begin
      printf, lun_verb, '------------------------------------------------------------------------'
      printf, lun_verb, STRCOMPRESS('Beginning loop '+STRING(n_cl_promotions)+'.'+$
                         STRING(loopcount))
      printf, lun_verb, '------------------------------------------------------------------------'
    endif
    done =1
    loopcount=loopcount+1
    i=n_elements(z0_nuc)-1

;-----------------------------------------------------------------------
; make a  promotion
;-----------------------------------------------------------------------    

; some numerical accounting - calculate the number of promotions
; we are about to try and where they fit into the grand scheme of things


; set lower bound of this promotion rule set
    irange=intarr(2)
    irange[0]=[n_elements(z0_nuc)]

; next_rule is a counter to say what rule we should try next. It is set to -1
; when all the rule sets are complete. Therefore if it is -1, reset it. 
; Do not always reset it, as setting next_rule to something else is used
; for two (or more) step increases.

    if next_rule[0] EQ -1 THEN next_rule=1
;    if major_rule[0] EQ -1 THEN major_rule=[-1]


    WHILE next_rule[0] NE -1 DO BEGIN
    
      if keyword_set(verbose) then begin
        printf, lun_verb, '*--------------------------------*'
        printf, lun_verb, '   starting next promotion rule  '
        printf, lun_verb, '*--------------------------------*'
      endif
      
      prom_rules={no_v_shl       : no_v_shl[iref],             $
                  max_dn_v1      : max_dn_v1[iref],            $
                  min_dn_v1      : min_dn_v1[iref],            $
                  max_dl_v1      : max_dl_v1[iref],            $
                  min_dl_v1      : min_dl_v1[iref],            $
                  max_dn_v2      : max_dn_v2[iref],            $
                  min_dn_v2      : min_dn_v2[iref],            $
                  max_dl_v2      : max_dl_v2[iref],            $
                  min_dl_v2      : min_dl_v2[iref],            $
                  prom_cl        : prom_cl[iref],              $
                  max_n_cl       : max_n_cl[iref],             $
                  min_n_cl       : min_n_cl[iref],             $
                  max_l_cl       : max_l_cl[iref],             $
                  min_l_cl       : min_l_cl[iref],             $
                  max_dn_cl      : max_dn_cl[iref],            $
                  min_dn_cl      : min_dn_cl[iref],            $
                  max_dl_cl      : max_dl_cl[iref],            $
                  min_dl_cl      : min_dl_cl[iref],            $
                  fill_n_v1      : fill_n_v1[iref],            $
                  fill_par       : fill_par[iref],             $
                  for_tr_sel     : for_tr_sel[iref],           $
                  last_4f        : last_4f[iref],              $
                  grd_cmplx      : grd_cmplx[iref]            }


      adas8xx_opt_control_expand_promotions, $
                             z0_nuc         = z0_nuc[iref],               $
                             z_ion          = z_ion[iref],                $
                             configurations = config[iref],               $
                             ionpot         = ionpot[iref],               $
                             prom_rules     = prom_rules,                 $
                             promotion_results=promotion_results,         $
                             final_rules    = final_rules,                $
                             configs        = (*configs[iref]),           $
                             base_rules     = base_rules,                 $
                             next_rule      = next_rule,                  $
                             major_rule     = major_rule,                 $
                             lun_verb       = lun_verb



;-----------------------------------------------------------------------
; add to arrays
;-----------------------------------------------------------------------

      z0_nuc      =  [z0_nuc      ,final_rules.z0_nuc]     
      z_ion       =  [z_ion       ,final_rules.z_ion]      
      nel         =  [nel         ,nel[0]]
      config      =  [config      ,final_rules.config]
      ionpot      =  [ionpot      ,final_rules.ionpot]
      no_v_shl    =  [no_v_shl    ,final_rules.no_v_shl]
      max_dn_v1   =  [max_dn_v1   ,final_rules.max_dn_v1]
      min_dn_v1   =  [min_dn_v1   ,final_rules.min_dn_v1]
      max_dl_v1   =  [max_dl_v1   ,final_rules.max_dl_v1]
      min_dl_v1   =  [min_dl_v1   ,final_rules.min_dl_v1]
      max_dn_v2   =  [max_dn_v2   ,final_rules.max_dn_v2]
      min_dn_v2   =  [min_dn_v2   ,final_rules.min_dn_v2]
      max_dl_v2   =  [max_dl_v2   ,final_rules.max_dl_v2]
      min_dl_v2   =  [min_dl_v2   ,final_rules.min_dl_v2]
      prom_cl     =  [prom_cl     ,final_rules.prom_cl]
      max_n_cl    =  [max_n_cl    ,final_rules.max_n_cl]
      min_n_cl    =  [min_n_cl    ,final_rules.min_n_cl]
      max_l_cl    =  [max_l_cl    ,final_rules.max_l_cl]
      min_l_cl    =  [min_l_cl    ,final_rules.min_l_cl]
      max_dn_cl   =  [max_dn_cl   ,final_rules.max_dn_cl]
      min_dn_cl   =  [min_dn_cl   ,final_rules.min_dn_cl]
      max_dl_cl   =  [max_dl_cl   ,final_rules.max_dl_cl]
      min_dl_cl   =  [min_dl_cl   ,final_rules.min_dl_cl]
      fill_n_v1   =  [fill_n_v1   ,final_rules.fill_n_v1]
      fill_par    =  [fill_par    ,final_rules.fill_par]
      for_tr_sel  =  [for_tr_sel  ,final_rules.for_tr_sel]
      last_4f     =  [last_4f     ,final_rules.last_4f]
      grd_cmplx   =  [grd_cmplx   ,final_rules.grd_cmplx]
      message     =  [message     ,final_rules.message]

      if keyword_set(verbose) then begin
        printf, lun_verb, '*---- rule set: '+$
               STRCOMPRESS(STRING(n_elements(sum_configs)),/REMOVE_ALL)+$
               ',ref: '+STRCOMPRESS(STRING(iref),/REMOVE_ALL)+'----*'
        printf, lun_verb, final_rules.message

        IF no_v_shl[iref] NE no_v_shl[n_elements(no_v_shl)-1] THEN $
          printf, lun_verb, 'no_v_shl  :'+STRING(no_v_shl[iref])  +' -> ' + STRING(no_v_shl[n_elements(no_v_shl)-1])
        IF max_dn_v1[iref] NE max_dn_v1[n_elements(max_dn_v1)-1] THEN $
          printf, lun_verb, 'max_dn_v1 :'+STRING(max_dn_v1[iref]) +' -> ' + STRING(max_dn_v1[n_elements(max_dn_v1)-1])
        IF min_dn_v1[iref] NE min_dn_v1[n_elements(min_dn_v1)-1] THEN $
          printf, lun_verb, 'min_dn_v1 :'+STRING(min_dn_v1[iref]) +' -> ' + STRING(min_dn_v1[n_elements(min_dn_v1)-1])
        IF max_dl_v1[iref] NE max_dl_v1[n_elements(max_dl_v1)-1] THEN $
          printf, lun_verb, 'max_dl_v1 :'+STRING(max_dl_v1[iref]) +' -> ' + STRING(max_dl_v1[n_elements(max_dl_v1)-1])
        IF min_dl_v1[iref] NE min_dl_v1[n_elements(min_dl_v1)-1] THEN $
          printf, lun_verb, 'min_dl_v1 :'+STRING(min_dl_v1[iref]) +' -> ' + STRING(min_dl_v1[n_elements(min_dl_v1)-1])
        IF max_dn_v2[iref] NE max_dn_v2[n_elements(max_dn_v2)-1] THEN $
          printf, lun_verb, 'max_dn_v2 :'+STRING(max_dn_v2[iref]) +' -> ' + STRING(max_dn_v2[n_elements(max_dn_v2)-1])
        IF min_dn_v2[iref] NE min_dn_v2[n_elements(min_dn_v2)-1] THEN $
          printf, lun_verb, 'min_dn_v2 :'+STRING(min_dn_v2[iref]) +' -> ' + STRING(min_dn_v2[n_elements(min_dn_v2)-1])
        IF max_dl_v2[iref] NE max_dl_v2[n_elements(max_dl_v2)-1] THEN $
          printf, lun_verb, 'max_dl_v2 :'+STRING(max_dl_v2[iref]) +' -> ' + STRING(max_dl_v2[n_elements(max_dl_v2)-1])
        IF min_dl_v2[iref] NE min_dl_v2[n_elements(min_dl_v2)-1] THEN $
          printf, lun_verb, 'min_dl_v2 :'+STRING(min_dl_v2[iref]) +' -> ' + STRING(min_dl_v2[n_elements(min_dl_v2)-1])
        IF prom_cl[iref] NE prom_cl[n_elements(prom_cl)-1] THEN $
          printf, lun_verb, 'prom_cl   :'+STRING(prom_cl[iref])   +' -> ' + STRING(prom_cl[n_elements(prom_cl)-1])
        IF max_n_cl[iref] NE max_n_cl[n_elements(max_n_cl)-1] THEN $
          printf, lun_verb, 'max_n_cl  :'+STRING(max_n_cl[iref])  +' -> ' + STRING(max_n_cl[n_elements(max_n_cl)-1])
        IF min_n_cl[iref] NE min_n_cl[n_elements(min_n_cl)-1] THEN $
          printf, lun_verb, 'min_n_cl  :'+STRING(min_n_cl[iref])  +' -> ' + STRING(min_n_cl[n_elements(min_n_cl)-1])
        IF max_l_cl[iref] NE max_l_cl[n_elements(max_l_cl)-1] THEN $
          printf, lun_verb, 'max_l_cl  :'+STRING(max_l_cl[iref])  +' -> ' + STRING(max_l_cl[n_elements(max_l_cl)-1])
        IF min_l_cl[iref] NE min_l_cl[n_elements(min_l_cl)-1] THEN $
          printf, lun_verb, 'min_l_cl  :'+STRING(min_l_cl[iref])  +' -> ' + STRING(min_l_cl[n_elements(min_l_cl)-1])
        IF max_dn_cl[iref] NE max_dn_cl[n_elements(max_dn_cl)-1] THEN $
          printf, lun_verb, 'max_dn_cl :'+STRING(max_dn_cl[iref]) +' -> ' + STRING(max_dn_cl[n_elements(max_dn_cl)-1])
        IF min_dn_cl[iref] NE min_dn_cl[n_elements(min_dn_cl)-1] THEN $  
          printf, lun_verb, 'min_dn_cl :'+STRING(min_dn_cl[iref]) +' -> ' + STRING(min_dn_cl[n_elements(min_dn_cl)-1])
        IF max_dl_cl[iref] NE max_dl_cl[n_elements(max_dl_cl)-1] THEN $
          printf, lun_verb, 'max_dl_cl :'+STRING(max_dl_cl[iref]) +' -> ' + STRING(max_dl_cl[n_elements(max_dl_cl)-1])
        IF min_dl_cl[iref] NE min_dl_cl[n_elements(min_dl_cl)-1] THEN $
          printf, lun_verb, 'min_dl_cl :'+STRING(min_dl_cl[iref]) +' -> ' + STRING(min_dl_cl[n_elements(min_dl_cl)-1])
        IF fill_n_v1[iref] NE fill_n_v1[n_elements(fill_n_v1)-1] THEN $
          printf, lun_verb, 'fill_n_v1 :'+STRING(fill_n_v1[iref]) +' -> ' + STRING(fill_n_v1[n_elements(fill_n_v1)-1])
        IF fill_par[iref] NE fill_par[n_elements(fill_par)-1] THEN $
          printf, lun_verb, 'fill_par  :'+STRING(fill_par[iref])  +' -> ' + STRING(fill_par[n_elements(fill_par)-1])
        IF for_tr_sel[iref] NE for_tr_sel[n_elements(for_tr_sel)-1] THEN $
          printf, lun_verb, 'for_tr_sel:'+STRING(for_tr_sel[iref])+' -> ' + STRING(for_tr_sel[n_elements(for_tr_sel)-1])
        IF last_4f[iref] NE last_4f[n_elements(last_4f)-1] THEN $
          printf, lun_verb, 'last_4f   :'+STRING(last_4f[iref])   +' -> ' + STRING(last_4f[n_elements(last_4f)-1])
        IF grd_cmplx[iref] NE grd_cmplx[n_elements(grd_cmplx)-1] THEN $
          printf, lun_verb, 'grd_cmplx :'+STRING(grd_cmplx [iref])+' -> ' + STRING(grd_cmplx[n_elements(grd_cmplx)-1])
      endif

      IF final_rules.done NE 0 THEN BEGIN
        adas8xx_promotions,  z0_nuc     = final_rules.z0_nuc,    $
                             z_ion      = final_rules.z_ion,     $
                             ionpot     = final_rules.ionpot,    $
                             prom_rules = final_rules,           $                             
                             promotion_results = promotion_results
        

        tmp=configs
    
        configs=PTRARR(n_elements(tmp) +1)
    
        FOR iconf=0, n_elements(tmp)-1 DO BEGIN
          tmp2=*tmp[iconf]
          configs[iconf]=PTR_NEW(tmp2, /no_copy)
        ENDFOR
        tmp=0
        tmp2=0
        configs[n_elements(configs)-1]=PTR_NEW(promotion_results.ex_cfg)
    
        adas8xx_opt_check_valid_promotion_set, $
                        max_configurations = max_configurations, $
                        monitor_power      = monitor_power, $
                        valid_rules        = valid_rules, $
                        z0_nuc             = final_rules.z0_nuc, $
                        nel                = nel[0],    $
                        z_ion              = final_rules.z_ion,    $
                        ionpot             = final_rules.ionpot,    $
                        for_tr_sel         = final_rules.for_tr_sel,  $
                        promotion_results  = promotion_results, $
                        plasma             = plasma, $
                        fpecdata           = fpecdata,   $
                        configs            = configs, $
                        exitstatus         = exitstatus, $
                        total_power        = total_power, $
                        total_adf11_power  = total_adf11_power, $
                        total_adf40_power  = total_adf40_power, $
                        sum_configs        = sum_configs, $
                        sum_levels         = sum_levels, $
                        sum_terms          = sum_terms, $
                        old_promotion_ruleset=old_promotion_ruleset, $
                        cowan_scale_factors=cowan_scale_factors, $
                        year               = year, $
                        lun_verb           = lun_verb

        if keyword_set(verbose) then begin
          printf, lun_verb,'total_power=', total_power
        endif
      ENDIF ELSE BEGIN  ; IF final_rules.done eq 0
      
        tmp=configs
    
        configs=PTRARR(n_elements(tmp) +1)
    
        FOR iconf=0, n_elements(tmp)-1 DO BEGIN
          tmp2=*tmp[iconf]
          configs[iconf]=PTR_NEW(tmp2, /no_copy)
        ENDFOR
        tmp=0
        tmp2=0
        configs[n_elements(configs)-1]=PTR_NEW('empty')
;
;
        IF KEYWORD_SET(monitor_power) THEN BEGIN
          total_power=[total_power,0]
          total_adf11_power=[total_adf11_power,0]
          total_adf40_power=[total_adf40_power,0]
        ENDIF      
;
;
        exitstatus=[exitstatus, -1]
        sum_configs=[sum_configs,0]
        sum_terms=[sum_terms,0]
        sum_levels=[sum_levels,0]

      ENDELSE
      if keyword_set(verbose) then begin
        IF sum_configs[iref] NE sum_configs[n_elements(sum_configs)-1] THEN $
          printf, lun_verb, 'Nconfigs  :'+STRING(sum_configs[iref]) +' -> ' +STRING(sum_configs[n_elements(sum_configs)-1])
        IF sum_terms[iref] NE sum_terms[n_elements(sum_terms)-1] THEN $
          printf, lun_verb, 'Nterms   :'+STRING(sum_terms[iref]) +' -> ' +STRING(sum_terms[n_elements(sum_terms)-1])
        IF sum_levels[iref] NE sum_levels[n_elements(sum_levels)-1] THEN $
          printf, lun_verb, 'Nlevels   :'+STRING(sum_levels[iref]) +' -> ' +STRING(sum_levels[n_elements(sum_levels)-1])
      endif
;       stop
    ENDWHILE ; loop of number of final_rules (i..e different promotions tried)
    
    

    ; set upper bound of irange to bound this "promotion round"
    irange[1]=n_elements(z0_nuc)-1
    
    if keyword_set(verbose) then printf, lun_verb, 'We are at the end of a promotion rule set, irange=', irange

    ; decide on the best case to use as a reference scenario.

    ; Deal with total_power being present first:

    irefold=iref
    IF KEYWORD_SET(total_power) THEN BEGIN

      ; find total_power from those rulesets which ran

      iran=where(exitstatus[irange[0]:irange[1]] EQ 0)

      IF iran[0] EQ -1 THEN BEGIN

        done = 0
      ENDIF ELSE BEGIN
        iran=iran+irange[0]
        
        ; filter out cases with too many levels, defined as 10% higher
        ; than the closest match (arbitrary, but acceptable I think)

        IF exitstatus[iref] GE 0 THEN BEGIN
          deltan=SQRT(([sum_levels[iran],sum_levels[iref]]-n_levels_target)^2)
        ENDIF ELSE BEGIN
          deltan=SQRT((sum_levels[iran]-n_levels_target)^2)        
        ENDELSE  
        maxnl=1.1*(n_levels_target+min(deltan))

        IF exitstatus[iref] GE 0 THEN BEGIN
          deltan=SQRT(([sum_terms[iran],sum_terms[iref]]-n_terms_target)^2)
        ENDIF ELSE BEGIN
          deltan=SQRT((sum_terms[iran]-n_terms_target)^2)        
        ENDELSE  
        maxnt=1.1*(n_terms_target+min(deltan))

        itmp=where(sum_levels[iran] LE maxnl AND sum_terms[iran] LE maxnt)
;        print,'itmp1=',itmp
        IF itmp[0] EQ -1 THEN BEGIN 

          iref=iref

          done=0
        ENDIF ELSE BEGIN

          iran=iran[itmp]
;          print,'iran2',iran
          tmpes=exitstatus[iran]
          tmptp=total_power[iran]
          tmpnl=sum_levels[iran]
          tmpnt=sum_terms[iran]
          
          ; if there is a reference case...
          IF (iref NE 1) THEN BEGIN

            tmpnl=tmpnl-sum_levels[iref]
            tmpnt=tmpnt-sum_terms[iref]
            tmptp=tmptp-total_power[iref]
            ; if monitor power =1, ratio is relative to nlevels, if =2 then use nterms
            if monitor_power EQ 1 then $
              tmp_ratio=(tmptp/tmpnl) else if monitor_power eq 2 then $
              tmp_ratio=(tmptp/tmpnt) else print, 'ERROR: invalid monitor power!!!!'
            
            ; remove those with an infinite ratio

            IF (where(finite(tmp_ratio) AND $
                (tmp_ratio GT 0) AND (tmpnl GT 0) $
                ))[0] NE -1 THEN BEGIN            
                iran=iran[where(finite(tmp_ratio))]
                tmpnl=tmpnl[where(finite(tmp_ratio))]
                tmptp=tmptp[where(finite(tmp_ratio))]
                tmp_ratio=tmp_ratio[where(finite(tmp_ratio))]
            
                srttmp_ratio=reverse(sort(tmp_ratio)) ; highest first
            
            
            
; find the set or rules that maximises this ratio while staying below
; the target level count

              itmp=where(tmp_ratio[srttmp_ratio] GT 0 AND $
                         sum_levels[iran[srttmp_ratio]] LT maxnl AND $
                         sum_terms[iran[srttmp_ratio]] LT maxnt)
            
              IF itmp[0] EQ -1 THEN BEGIN
; there are no levels which are an improvement!
                iref=irefold
              ENDIF ELSE BEGIN
  
                iref=iran[srttmp_ratio[itmp[0]]]
              ENDELSE
            ENDIF ELSE BEGIN
              iref=irefold
            ENDELSE

          ENDIF ELSE BEGIN

            if monitor_power EQ 1 then $
              tmp_ratio=(tmptp/tmpnl) else if monitor_power eq 2 then $
              tmp_ratio=(tmptp/tmpnt) else print, 'ADAS*xx_OPR_PROMOTIONS_CONTROL error: invalid monitor power!!!!'

            srttmp_ratio=reverse(sort(tmp_ratio)) ; highest first

            itmp=where(tmp_ratio[srttmp_ratio] GT 0 AND $
                       sum_levels[iran[srttmp_ratio]] LT maxnl AND $
                       sum_terms[iran[srttmp_ratio]] LT maxnt)
            IF itmp[0] EQ -1 THEN BEGIN
; there are no cases which are an improvement!
              iref=irefold
            ENDIF ELSE BEGIN
              iref=iran[srttmp_ratio[itmp[0]]]
            ENDELSE

          ENDELSE

          IF sum_levels[iref] GT n_levels_target OR $
             sum_terms[iref] GT n_terms_target THEN done =0

          IF sum_levels[iref] EQ sum_levels[irefold] AND $
             sum_terms[iref] EQ sum_terms[irefold] THEN done=0
          
;          IF srttmp_ratio[itmp[0]]=0

        ENDELSE
      ENDELSE


    ENDIF ;ELSE BEGIN ; total power not set (not sure why you would use this?)
;
;      iran=where(exitstatus[irange[0]:irange[1]] EQ 0)
;
;      IF iran[0] EQ -1 THEN BEGIN
;        done = 0
;      ENDIF ELSE BEGIN
;        iran=iran+irange[0]
;        
;        ; filter out cases with too many levels
;        
;        deltan=SQRT(([sum_levels[iran],sum_levels[iref]]-n_levels_target)^2)
;        maxnl=1.1*(n_levels_target+min(deltan))
;
;        deltan=SQRT(([sum_terms[iran],sum_terms[iref]]-n_terms_target)^2)
;        maxnt=1.1*(n_terms_target+min(deltan))
;
;        itmp=where(sum_levels[iran] LE maxnl AND $
;                   sum_terms[iran] LE maxnt)
;        
;        IF itmp[0] EQ -1 THEN BEGIN 
;          irefold=iref
;          iref=iref
;          done=0
;        ENDIF ELSE BEGIN
;        
;          iran=iran[itmp]
;        
;          tmpes=exitstatus[iran]
;          tmpnc=sum_configs[iran]
;          tmpnt=sum_terms[iran]
;          tmpnl=sum_levels[iran]
;          
;          ; if there is a reference case...
;          IF (iref NE 0) THEN BEGIN
;
;            tmpnl=tmpnl-sum_levels[iref]
;            tmpnt=tmpnt-sum_terms[iref]
;            tmpnc=tmpnc-sum_configs[iref]
;
;            tmp_ratio=(tmpnl/tmpnc)
;            srttmp_ratio=reverse(sort(tmp_ratio)) ; highest first
;
;
;            irefold=iref
;            iref=iran[srttmp_ratio[0]]
;
;          ENDIF ELSE BEGIN
;            tmp_ratio=(tmpnl/tmpnc)
;            srttmp_ratio=reverse(sort(tmp_ratio)) ; highest first
;
;            irefold=iref
;            iref=iran[srttmp_ratio[0]]
;          ENDELSE
;
;          IF sum_levels[iref] GT n_levels_target THEN done =0
;
;          IF sum_levels[iref] GE sum_levels[irefold] THEN done=0
;        ENDELSE
;      ENDELSE
;
;    
;    ENDELSE
;    END
;    END
;    stop
    
    IF (iref EQ irefold) then begin
      redo=0

      IF (base_rules.v1_n+max_dn_v1[iref] LT 7) AND $
         (irange[1]-iref LT 500) THEN BEGIN
        ; check the number of levels for repeats
        if keyword_set(verbose) then begin
          printf, lun_verb, 'INVESTIGATING iref=irefold'
        ; EDIT HERE
          printf, lun_verb, 'old_irange=',old_irange
        endif
        IF old_irange[0] NE -1 THEN BEGIN
          igood=intarr(1+irange[1]-irange[0])
          FOR itmp=irange[0], irange[1] DO BEGIN
            itmp2=where(sum_levels[old_irange[0]:old_irange[1]] EQ $
                        sum_levels[itmp] AND $
                        sum_terms[old_irange[0]:old_irange[1]] EQ $
                        sum_terms[itmp])

            IF itmp2[0] EQ -1 THEN BEGIN
              igood[itmp-irange[0]]=1
            ENDIF
            IF sum_levels[itmp] EQ 0 OR $
               sum_terms[itmp] EQ 0 THEN igood[itmp-irange[0]]=0
          ENDFOR
        ENDIF ELSE BEGIN
          igood=intarr(1+irange[1]-irange[0])
          itmp2=where(sum_levels[iref] NE $
                      sum_levels[irange[0]:irange[1]] AND $
                      sum_levels[irange[0]:irange[1]] GT 0 AND $
                      sum_terms[iref] NE $
                      sum_terms[irange[0]:irange[1]] AND $
                      sum_terms[irange[0]:irange[1]] GT 0)
          
          IF itmp2[0] NE -1 THEN BEGIN
            igood[itmp2]=1
          ENDIF ELSE BEGIN
;            igood=[-1] 
          ENDELSE
;          print, 'l.1363: igood=',igood
        ENDELSE
          ; check for level count
          
        if keyword_set(verbose) then begin
          printf, lun_verb,'igood+irange[0]=', irange[0]+where(igood EQ 1)
          printf, lun_verb,'n_levels[irange[0]+igood]=',sum_levels[irange[0]+where(igood EQ 1)]  
          printf, lun_verb,'n_levels[irange[0]:irange[1]]='$
                                        ,sum_levels[irange[0]:irange[1]]
          printf, lun_verb,'n_terms[irange[0]+igood]=',sum_terms[irange[0]+where(igood EQ 1)]  
          printf, lun_verb,'n_terms[irange[0]:irange[1]]='$
                                        ,sum_terms[irange[0]:irange[1]]

          IF old_irange[0] GT -1 THEN BEGIN
            printf, lun_verb,'n_levels[old_irange[0]:old_irange[1]]='$
                                  ,sum_levels[old_irange[0]:old_irange[1]]
            printf, lun_verb,'n_terms[old_irange[0]:old_irange[1]]='$
                                  ,sum_terms[old_irange[0]:old_irange[1]]
          ENDIF                                

          printf, lun_verb,'n_configs[irange[0]+igood]=',sum_configs[irange[0]+where(igood EQ 1)]
          printf, lun_verb,'n_configs[irange[0]:irange[1]]='$
                                          ,sum_configs[irange[0]:irange[1]]
          IF old_irange[0] GT -1 THEN $
          printf, lun_verb,'n_configs[old_irange[0]:old_irange[1]]='$
                                  ,sum_configs[old_irange[0]:old_irange[1]]
          
          printf, lun_verb,'total_power[irange[0]+igood]=',total_power[irange[0]+where(igood EQ 1)]  
          printf, lun_verb,'total_power[irange[0]:irange[1]]='$
                                          ,total_power[irange[0]:irange[1]]

          IF old_irange[0] GT -1 THEN $
          printf, lun_verb,'total_power[old_irange[0]:old_irange[1]]='$
                                  ,total_power[old_irange[0]:old_irange[1]]
        ENDIF
        IF ((WHERE(igood EQ 1))[0] NE -1) THEN BEGIN
            ; 
          IF (min((sum_levels[irange[0]:irange[1]])[WHERE(igood EQ 1)]) LT $
                                          n_levels_target*(0.8)) AND $
             (min((sum_terms[irange[0]:irange[1]])[WHERE(igood EQ 1)]) LT $
                                          n_terms_target*(0.8)) AND $
             (min((sum_configs[irange[0]:irange[1]])[WHERE(igood EQ 1)]) LT $
                                 MAX_CONFIGURATIONS*(0.8)) THEN redo=1

          if keyword_set(verbose) then begin
            IF redo EQ 1 then printf, lun_verb,'GOING TO BE A REDO'
            IF redo EQ 0 then printf, lun_verb,'NOT GOING TO BE A REDO'
          endif
        ENDIF ELSE BEGIN
          redo=0
          if keyword_set(verbose) then printf, lun_verb, 'we are NOT GOING TO REDO!'
        ENDELSE

          
         
         
      ENDIF
;     IF (base_rules.v1_n+max_dn_v1[iref] LT 7) AND $
;         irange[1]-iref LT 300 THEN BEGIN
        
;       redo=0
;       IF iran[0] NE -1 THEN BEGIN
;         IF (median(sum_levels[iran]) LT n_levels_target*(0.8)) AND $
;            (median(sum_configs[iran]) LT MAX_CONFIGURATIONS*(0.8)) THEN $
;            redo=1

;       ENDIF 

;       IF iran[0] EQ -1 THEN redo=1
;
;       IF (min(sum_levels[iran]) LT n_levels_target*(0.8)) AND $
;          (min(sum_configs[iran]) LT MAX_CONFIGURATIONS*(0.8)) AND $          
      IF redo EQ 1 THEN BEGIN

        ; we have few configurations. Try some double promotions
        if keyword_set(verbose) then begin
          printf, lun_verb, '---------------------------------------------------'
          printf, lun_verb, 'ROOM FOR IMPROVEMENT: try some double promotions'
          printf, lun_verb, '---------------------------------------------------'
        endif
;         stop
        ; define major rule
        
        if n_elements(major_rule) EQ 1 AND major_rule[0] EQ -1 THEN BEGIN
          major_rule=1+[where(exitstatus[irange[0]:irange[1]] NE -1 $
            AND sum_configs[irange[0]:irange[1]] LT MAX_CONFIGURATIONS $
            AND sum_levels[irange[0]:irange[1]] LT n_levels_target $
            AND sum_terms[irange[0]:irange[1]] LT n_terms_target $
            AND (igood EQ 1))]

          store_major_rule=major_rule
;          stop
        endif else begin
;        store_major_rule=major_rule
        
        ; calculate major_rules
        ; this is complicated because it can require a backtrack through
        ; several different stages.
        
          n_rules_per_major_rule=(1+irange[1]-irange[0])/(size(major_rule))[1]
        
        ; now expand major rule
;          IF (size(store_major_rule))[0] GT 1 THEN BEGIN
          
          IF (size(store_major_rule))[0] EQ 1 THEN BEGIN
            new_major_rule=intarr(n_elements($
            where(exitstatus[irange[0]:irange[1]] NE -1 AND $
            sum_configs[irange[0]:irange[1]] LT MAX_CONFIGURATIONS AND $
            sum_terms[irange[0]:irange[1]] LT n_terms_target AND $
            sum_levels[irange[0]:irange[1]] LT n_levels_target AND $
            (igood EQ 1))), $
            (size(store_major_rule))[0]+1)
          ENDIF ELSE BEGIN        
            new_major_rule=intarr(n_elements( $
            where(exitstatus[irange[0]:irange[1]] NE -1 AND $
            sum_configs[irange[0]:irange[1]] LT MAX_CONFIGURATIONS AND $
            sum_terms[irange[0]:irange[1]] LT n_terms_target AND $
            sum_levels[irange[0]:irange[1]] LT n_levels_target AND $
            (igood EQ 1))), $
            (size(store_major_rule))[2]+1)
          ENDELSE
;           
;           new_major_rule=dblarr([(size(store_major_rule))[1:$ 
;                                     (size(store_major_rule))[0]],$
;                     n_elements(where(exitstatus[irange[0]:irange[1]] NE -1))])

          icounting=irange[0]
          iworking=0
;          stop
          FOR i=0,(size(store_major_rule))[1]-1 DO BEGIN
            FOR j=0, n_rules_per_major_rule -1 DO BEGIN
              if (exitstatus[icounting] NE -1 AND $
                sum_configs[icounting] LT MAX_CONFIGURATIONS AND $
                sum_terms[icounting] LT n_terms_target AND $
                sum_levels[icounting] LT n_levels_target AND $
                (igood[icounting-irange[0]] EQ 1)) THEN BEGIN
              
;              if exitstatus[icounting] NE -1 THEN BEGIN
                new_major_rule[iworking, *]=[reform(store_major_rule[i,*]),j+1]
                iworking=iworking+1
              endif 
              icounting=icounting+1
            ENDFOR 
          ENDFOR
          major_rule=new_major_rule
          store_major_rule=major_rule  
;          stop      
        ENDELSE  
;        major_rule=intarr((size(major_rule))[0]+1)
;        print, "major_rule:", major_rule
         done = 1    
;        stop
;      ENDIF ELSE BEGIN
;      
;          print, '---------------------------------------------------'
;          print, 'CHOSE NO RULE CHANGE: old reference case is better'
;          print, '---------------------------------------------------'
;      ENDELSE
      ENDIF ELSE BEGIN
        if keyword_set(verbose) then begin
          printf, lun_verb, '---------------------------------------------------'
          printf, lun_verb, 'CHOSE NO RULE CHANGE: old reference case is better'
          printf, lun_verb, '---------------------------------------------------'
        endif
      ENDELSE    
      
    ENDIF ELSE BEGIN
      if keyword_set(verbose) then begin  
        printf, lun_verb, '---------------------------------------------------'
        printf, lun_verb, 'CHOSE RULE SET:'+ string(iref)
        printf, lun_verb, '---------------------------------------------------'
        IF no_v_shl[iref] NE no_v_shl[irefold] THEN $
          printf, lun_verb, 'no_v_shl  :'+STRING(no_v_shl[irefold])  +' -> ' + STRING(no_v_shl[iref])
        IF max_dn_v1[iref] NE max_dn_v1[irefold] THEN $
          printf, lun_verb, 'max_dn_v1 :'+STRING(max_dn_v1[irefold]) +' -> ' + STRING(max_dn_v1[iref])
        IF min_dn_v1[iref] NE min_dn_v1[irefold] THEN $
          printf, lun_verb, 'min_dn_v1 :'+STRING(min_dn_v1[irefold]) +' -> ' + STRING(min_dn_v1[iref])
        IF max_dl_v1[iref] NE max_dl_v1[irefold] THEN $
          printf, lun_verb, 'max_dl_v1 :'+STRING(max_dl_v1[irefold]) +' -> ' + STRING(max_dl_v1[iref])
        IF min_dl_v1[iref] NE min_dl_v1[irefold] THEN $
          printf, lun_verb, 'min_dl_v1 :'+STRING(min_dl_v1[irefold]) +' -> ' + STRING(min_dl_v1[iref])
        IF max_dn_v2[iref] NE max_dn_v2[irefold] THEN $
          printf, lun_verb, 'max_dn_v2 :'+STRING(max_dn_v2[irefold]) +' -> ' + STRING(max_dn_v2[iref])
        IF min_dn_v2[iref] NE min_dn_v2[irefold] THEN $
          printf, lun_verb, 'min_dn_v2 :'+STRING(min_dn_v2[irefold]) +' -> ' + STRING(min_dn_v2[iref])
        IF max_dl_v2[iref] NE max_dl_v2[irefold] THEN $
          printf, lun_verb, 'max_dl_v2 :'+STRING(max_dl_v2[irefold]) +' -> ' + STRING(max_dl_v2[iref])
        IF min_dl_v2[iref] NE min_dl_v2[irefold] THEN $
          printf, lun_verb, 'min_dl_v2 :'+STRING(min_dl_v2[irefold]) +' -> ' + STRING(min_dl_v2[iref])
        IF prom_cl[iref] NE prom_cl[irefold] THEN $
          printf, lun_verb, 'prom_cl   :'+STRING(prom_cl[irefold])   +' -> ' + STRING(prom_cl[iref])
        IF max_n_cl[iref] NE max_n_cl[irefold] THEN $
          printf, lun_verb, 'max_n_cl  :'+STRING(max_n_cl[irefold])  +' -> ' + STRING(max_n_cl[iref])
        IF min_n_cl[iref] NE min_n_cl[irefold] THEN $
          printf, lun_verb, 'min_n_cl  :'+STRING(min_n_cl[irefold])  +' -> ' + STRING(min_n_cl[iref])
        IF max_l_cl[iref] NE max_l_cl[irefold] THEN $
          printf, lun_verb, 'max_l_cl  :'+STRING(max_l_cl[irefold])  +' -> ' + STRING(max_l_cl[iref])
        IF min_l_cl[iref] NE min_l_cl[irefold] THEN $
          printf, lun_verb, 'min_l_cl  :'+STRING(min_l_cl[irefold])  +' -> ' + STRING(min_l_cl[iref])
        IF max_dn_cl[iref] NE max_dn_cl[irefold] THEN $
          printf, lun_verb, 'max_dn_cl :'+STRING(max_dn_cl[irefold]) +' -> ' + STRING(max_dn_cl[iref])
        IF min_dn_cl[iref] NE min_dn_cl[irefold] THEN $  
          printf, lun_verb, 'min_dn_cl :'+STRING(min_dn_cl[irefold]) +' -> ' + STRING(min_dn_cl[iref])
        IF max_dl_cl[iref] NE max_dl_cl[irefold] THEN $
          printf, lun_verb, 'max_dl_cl :'+STRING(max_dl_cl[irefold]) +' -> ' + STRING(max_dl_cl[iref])
        IF min_dl_cl[iref] NE min_dl_cl[irefold] THEN $
          printf, lun_verb, 'min_dl_cl :'+STRING(min_dl_cl[irefold]) +' -> ' + STRING(min_dl_cl[iref])
        IF fill_n_v1[iref] NE fill_n_v1[irefold] THEN $
          printf, lun_verb, 'fill_n_v1 :'+STRING(fill_n_v1[irefold]) +' -> ' + STRING(fill_n_v1[iref])
        IF fill_par[iref] NE fill_par[irefold] THEN $
          printf, lun_verb, 'fill_par  :'+STRING(fill_par[irefold])  +' -> ' + STRING(fill_par[iref])
        IF for_tr_sel[iref] NE for_tr_sel[irefold] THEN $
          printf, lun_verb, 'for_tr_sel:'+STRING(for_tr_sel[irefold])+' -> ' + STRING(for_tr_sel[iref])
        IF last_4f[iref] NE last_4f[irefold] THEN $
          printf, lun_verb, 'last_4f   :'+STRING(last_4f[irefold])   +' -> ' + STRING(last_4f[iref])
        IF grd_cmplx[iref] NE grd_cmplx[irefold] THEN $
          printf, lun_verb, 'grd_cmplx :'+STRING(grd_cmplx [irefold])+' -> ' + STRING(grd_cmplx[iref])
        printf, lun_verb, '---------------------------------------------------'

      endif
      major_rule=[-1]
      store_major_rule=major_rule
      
      IF keyword_set(logfile) THEN BEGIN  
        FOR iout=irange[0],irange[1] DO BEGIN
          IF (exitstatus[iout] EQ 0) THEN BEGIN
            if iout EQ iref then chosen='Y' ELSE chosen='N'
            IF keyword_set(monitor_power) THEN BEGIN
              if monitor_power EQ 1 then denom=sum_levels[iout]-sum_levels[irefold] $
                else if monitor_power EQ 2 then denom=sum_terms[iout]-sum_terms[irefold]
                  printf, loglun, FORMAT=logformat, n_cl_promotions, $
                  loopcount, iout-irange[0],$
                  iout,$
                  ' ',$
                  sum_levels[iout]  ,$
                  sum_terms[iout]  ,$
                  sum_configs[iout]  ,$
                  total_adf11_power[iout]  ,$
                  total_adf40_power[iout],$
                  (total_adf11_power[iout]-total_adf11_power[irefold])/$
                    denom, $
                  (total_adf40_power[iout]-total_adf40_power[irefold])/$
                    denom, $
                  chosen
            ENDIF ELSE BEGIN
               printf, loglun, FORMAT=logformat, n_cl_promotions, $
                  loopcount, iout-irange[0],$
                  iout,$
                  message[iout],$
                  sum_levels[iout]  ,$
                  (sum_levels[iout]-sum_levels[irefold])/$
                    (sum_configs[iout]-sum_configs[irefold]), $
                  chosen
            ENDELSE
                
            ; print the changes to the log file
            
            IF no_v_shl[irefold] NE no_v_shl[iout] THEN $
              printf, loglun,  '  X           no_v_shl  :'+STRING(no_v_shl[irefold])  +' -> ' + STRING(no_v_shl[iout])
            IF max_dn_v1[irefold] NE max_dn_v1[iout] THEN $
              printf, loglun,  '  X           max_dn_v1 :'+STRING(max_dn_v1[irefold]) +' -> ' + STRING(max_dn_v1[iout])
            IF min_dn_v1[irefold] NE min_dn_v1[iout] THEN $
              printf, loglun,  '  X           min_dn_v1 :'+STRING(min_dn_v1[irefold]) +' -> ' + STRING(min_dn_v1[iout])
            IF max_dl_v1[irefold] NE max_dl_v1[iout] THEN $
              printf, loglun,  '  X           max_dl_v1 :'+STRING(max_dl_v1[irefold]) +' -> ' + STRING(max_dl_v1[iout])
            IF min_dl_v1[irefold] NE min_dl_v1[iout] THEN $
              printf, loglun,  '  X           min_dl_v1 :'+STRING(min_dl_v1[irefold]) +' -> ' + STRING(min_dl_v1[iout])
            IF max_dn_v2[irefold] NE max_dn_v2[iout] THEN $
              printf, loglun,  '  X           max_dn_v2 :'+STRING(max_dn_v2[irefold]) +' -> ' + STRING(max_dn_v2[iout])
            IF min_dn_v2[irefold] NE min_dn_v2[iout] THEN $
              printf, loglun,  '  X           min_dn_v2 :'+STRING(min_dn_v2[irefold]) +' -> ' + STRING(min_dn_v2[iout])
            IF max_dl_v2[irefold] NE max_dl_v2[iout] THEN $
              printf, loglun,  '  X           max_dl_v2 :'+STRING(max_dl_v2[irefold]) +' -> ' + STRING(max_dl_v2[iout])
            IF min_dl_v2[irefold] NE min_dl_v2[iout] THEN $
              printf, loglun,  '  X           min_dl_v2 :'+STRING(min_dl_v2[irefold]) +' -> ' + STRING(min_dl_v2[iout])
            IF prom_cl[irefold] NE prom_cl[iout] THEN $
              printf, loglun,  '  X           prom_cl   :'+STRING(prom_cl[irefold])   +' -> ' + STRING(prom_cl[iout])
            IF max_n_cl[irefold] NE max_n_cl[iout] THEN $
              printf, loglun,  '  X           max_n_cl  :'+STRING(max_n_cl[irefold])  +' -> ' + STRING(max_n_cl[iout])
            IF min_n_cl[irefold] NE min_n_cl[iout] THEN $
              printf, loglun,  '  X           min_n_cl  :'+STRING(min_n_cl[irefold])  +' -> ' + STRING(min_n_cl[iout])
            IF max_l_cl[irefold] NE max_l_cl[iout] THEN $
              printf, loglun,  '  X           max_l_cl  :'+STRING(max_l_cl[irefold])  +' -> ' + STRING(max_l_cl[iout])
            IF min_l_cl[irefold] NE min_l_cl[iout] THEN $
              printf, loglun,  '  X           min_l_cl  :'+STRING(min_l_cl[irefold])  +' -> ' + STRING(min_l_cl[iout])
            IF max_dn_cl[irefold] NE max_dn_cl[iout] THEN $
              printf, loglun,  '  X           max_dn_cl :'+STRING(max_dn_cl[irefold]) +' -> ' + STRING(max_dn_cl[iout])
            IF min_dn_cl[irefold] NE min_dn_cl[iout] THEN $  
              printf, loglun,  '  X           min_dn_cl :'+STRING(min_dn_cl[irefold]) +' -> ' + STRING(min_dn_cl[iout])
            IF max_dl_cl[irefold] NE max_dl_cl[iout] THEN $
              printf, loglun,  'X             max_dl_cl :'+STRING(max_dl_cl[irefold]) +' -> ' + STRING(max_dl_cl[iout])
            IF min_dl_cl[irefold] NE min_dl_cl[iout] THEN $
              printf, loglun,  'X             min_dl_cl :'+STRING(min_dl_cl[irefold]) +' -> ' + STRING(min_dl_cl[iout])
            IF fill_n_v1[irefold] NE fill_n_v1[iout] THEN $
              printf, loglun,  'X             fill_n_v1 :'+STRING(fill_n_v1[irefold]) +' -> ' + STRING(fill_n_v1[iout])
            IF fill_par[irefold] NE fill_par[iout] THEN $
              printf, loglun,  'X             fill_par  :'+STRING(fill_par[irefold])  +' -> ' + STRING(fill_par[iout])
            IF for_tr_sel[irefold] NE for_tr_sel[iout] THEN $
              printf, loglun,  'X             for_tr_sel:'+STRING(for_tr_sel[irefold])+' -> ' + STRING(for_tr_sel[iout])
            IF last_4f[irefold] NE last_4f[iout] THEN $
              printf, loglun,  'X             last_4f   :'+STRING(last_4f[irefold])   +' -> ' + STRING(last_4f[iout])
            IF grd_cmplx[irefold] NE grd_cmplx[iout] THEN $
              printf, loglun,  'X             grd_cmplx :'+STRING(grd_cmplx [irefold])+' -> ' + STRING(grd_cmplx[iout])
          ENDIF
        ENDFOR
      ENDIF

    ENDELSE
    
    ; store the irange in old_irange
    old_irange=irange
    
    IF (done EQ 0) THEN BEGIN
      IF (WHERE( [min_dl_cl[iref],max_dl_cl[iref],$
            min_dn_cl[iref],max_dn_cl[iref]] NE 0))[0] EQ -1 THEN BEGIN
        prom_cl[iref]=0
      ENDIF
    ENDIF

;    stop 
  ENDWHILE ; end "while done EQ 1" loop    
  
  ; record the reference cases
  
  if keyword_set(verbose) then begin 
    printf, lun_verb, 'recording case'+ STRCOMPRESS(STRING(iref))+$
           ' as best case'
    printf, lun_verb, 'We have completed promotions from this initial rule set.'
  endif
  best_cases=[best_cases,iref]


;  stop
ENDFOR

ivalid_rules=best_cases[1:*]


; now find the best set of rules.
;
;;-----------------------------------------------------------------------
;; if monitor_power is set rule out configurations with zero power
;;-----------------------------------------------------------------------
IF keyword_set(monitor_power) THEN BEGIN
  itmp=where(total_power[ivalid_rules] GT 0)
  IF itmp[0] EQ -1 THEN BEGIN
    if keyword_set(verbose) then begin
      printf, lun_verb, 'ERROR: THERE ARE NO RULESETS WITH NON ZERO POWER!'
      printf, lun_verb, ' TOTAL POWERS:'
      printf, lun_verb, total_power
    endif
;    stop
  ENDIF ELSE BEGIN
    ivalid_rules=ivalid_rules[itmp]
  ENDELSE
ENDIF
;    
;
;;-----------------------------------------------------------------------
;; now find minimum difference between n_levels and target
;;-----------------------------------------------------------------------
;
min_deltanl=min((sum_levels[ivalid_rules]-n_levels_target)^2)
min_deltant=min((sum_terms[ivalid_rules]-n_terms_target)^2)
;
;;-----------------------------------------------------------------------
;; give a leeway of 10%
;;-----------------------------------------------------------------------
;
ibest_rules=where((sum_levels[ivalid_rules]-n_levels_target)^2 LT 1.21*min_deltanl AND $
                  (sum_terms[ivalid_rules]-n_terms_target)^2 LT 1.21*min_deltant)

ibest_rules=ivalid_rules[ibest_rules]

IF KEYWORD_SET(monitor_power) THEN BEGIN
;-----------------------------------------------------------------------
; Find where the maximum power is
;-----------------------------------------------------------------------  
  ibestpower=where(total_power[ibest_rules] EQ max(total_power[ibest_rules]))
  
  ibest_rules=ibest_rules[ibestpower]

;-----------------------------------------------------------------------
; now find the location within this that has the minimum n_levels
;-----------------------------------------------------------------------
  if monitor_power EQ 1 then  begin
    tmp=min(sum_levels[ibest_rules], ibest)
    itmp=where(sum_levels[ibest_rules] EQ tmp)
  endif else if monitor_power EQ 2 then begin
    tmp=min(sum_terms[ibest_rules], ibest)
    itmp=where(sum_terms[ibest_rules] EQ tmp)
  endif else begin
    if keyword_set(verbose) then $
      printf, lun_verb, 'Why am I here? Monitor power should be 1 or 2!'
    stop
  endelse  

  ibest_rules=ibest_rules[itmp]

ENDIF; ELSE BEGIN
;;-----------------------------------------------------------------------
;; if not looking at power, look for the most levels...
;;-----------------------------------------------------------------------  
;  tmp=max(sum_levels[ibest_rules],ibest)
;  itmp=where(sum_levels[ibest_rules] EQ tmp)
;;  stop
;  ibest_rules=ibest_rules[itmp]
;ENDELSE

IF (n_elements(ibest_rules) GT 1) THEN BEGIN
;-----------------------------------------------------------------------   
; pick the one with minimum set of rules, e.g. prom_cl, no_v2_shl, 
; grd_cmplx
;-----------------------------------------------------------------------  
  sumrules=intarr(n_elements(itmp))
  FOR i=0, n_elements(itmp)-1 DO BEGIN
    sumrules[i]=grd_cmplx[ibest_rules[i]] +$
                no_v_shl[ibest_rules[i]] +$
                prom_cl[ibest_rules[i]]
  ENDFOR
    
  itmp=where(sumrules EQ min(sumrules))
  
  ibest_rules=ibest_rules[itmp]
  
ENDIF    

IF n_elements(ibest_rules) GT 1 THEN BEGIN
  sumrules=intarr(n_elements(itmp))
  FOR i=0, n_elements(itmp)-1 DO BEGIN
    sumrules[i]= (max_dn_v1[ibest_rules[i]]^2)^0.5 +$
                 (min_dn_v1[ibest_rules[i]]^2)^0.5 +$
                 (max_dl_v1[ibest_rules[i]]^2)^0.5 +$
                 (min_dl_v1[ibest_rules[i]]^2)^0.5 +$
                 (max_dn_v2[ibest_rules[i]]^2)^0.5 +$
                 (min_dn_v2[ibest_rules[i]]^2)^0.5 +$
                 (max_dl_v2[ibest_rules[i]]^2)^0.5 +$
                 (min_dl_v2[ibest_rules[i]]^2)^0.5 +$
                 (max_n_cl[ibest_rules[i]]^2)^0.5 +$
                 (min_n_cl[ibest_rules[i]]^2)^0.5 +$
                 (max_l_cl[ibest_rules[i]]^2)^0.5 +$
                 (min_l_cl[ibest_rules[i]]^2)^0.5 +$
                 (max_dn_cl[ibest_rules[i]]^2)^0.5 +$
                 (min_dn_cl[ibest_rules[i]]^2)^0.5 +$
                 (max_dl_cl[ibest_rules[i]]^2)^0.5 +$
                 (min_dl_cl[ibest_rules[i]]^2)^0.5
  ENDFOR
  
  itmp=where(sumrules EQ min(sumrules))
;-----------------------------------------------------------------------  
; at this point, give up. They are probably all the same....
;-----------------------------------------------------------------------  

  ibest_rules=ibest_rules[itmp[0]]

       
ENDIF
;    
ibest=([ibest_rules])[0]

printf, loglun, where(ivalid_rules EQ ibest)

; find which 'i_initial_index' is the one we started from
original_index=i_initial_index[where(ivalid_rules EQ ibest)+1]


;print, old_promotion_ruleset
;stop


IF keyword_set(old_promotion_ruleset) THEN BEGIN
  if keyword_set(verbose) then begin
    printf, lun_verb, '------------------------------------------------------------------------'
    printf, lun_verb, '************************************************************************'
    printf, lun_verb, '------------------------------------------------------------------------'
    printf, lun_verb, '                      FINAL SETTINGS FOR THIS ION                       '
    printf, lun_verb, '------------------------------------------------------------------------'
    printf, lun_verb, '************************************************************************'
    printf, lun_verb, '------------------------------------------------------------------------'
  
  
    printf, lun_verb, 'Nconfigs  :'+STRING(sum_configs[0])+' -> '+STRING(sum_configs[original_index])+' -> ' + STRING(sum_configs[ibest])
    printf, lun_verb, 'Nterms    :'+STRING(sum_terms[0])  +' -> '+STRING(sum_terms[original_index])  +' -> ' + STRING(sum_terms[ibest])
    printf, lun_verb, 'Nlevels   :'+STRING(sum_levels[0]) +' -> '+STRING(sum_levels[original_index]) +' -> ' + STRING(sum_levels[ibest])
    printf, lun_verb, 'z0_nuc    :'+STRING(z0_nuc[0])     +' -> '+STRING(z0_nuc[original_index])     +' -> ' + STRING(z0_nuc[ibest])
    printf, lun_verb, 'z_ion     :'+STRING(z_ion[0])      +' -> '+STRING(z_ion[original_index])      +' -> ' + STRING(z_ion[ibest])
    printf, lun_verb, 'nel       :'+STRING(nel[0])        +' -> '+STRING(nel[original_index])        +' -> ' + STRING(nel[ibest])
;    printf, lun_verb, 'config    :'+STRING(config[0])     +' -> '+STRING(config[original_index])     +' -> ' + STRING(config[ibest])
    printf, lun_verb, 'ionpot    :'+STRING(ionpot[0])     +' -> '+STRING(ionpot[original_index])     +' -> ' + STRING(ionpot[ibest])
    printf, lun_verb, 'no_v_shl  :'+STRING(no_v_shl[0])   +' -> '+STRING(no_v_shl[original_index])   +' -> ' + STRING(no_v_shl[ibest])
    printf, lun_verb, 'max_dn_v1 :'+STRING(max_dn_v1[0])  +' -> '+STRING(max_dn_v1[original_index])  +' -> ' + STRING(max_dn_v1[ibest])
    printf, lun_verb, 'min_dn_v1 :'+STRING(min_dn_v1[0])  +' -> '+STRING(min_dn_v1[original_index])  +' -> ' + STRING(min_dn_v1[ibest])
    printf, lun_verb, 'max_dl_v1 :'+STRING(max_dl_v1[0])  +' -> '+STRING(max_dl_v1[original_index])  +' -> ' + STRING(max_dl_v1[ibest])
    printf, lun_verb, 'min_dl_v1 :'+STRING(min_dl_v1[0])  +' -> '+STRING(min_dl_v1[original_index])  +' -> ' + STRING(min_dl_v1[ibest])
    printf, lun_verb, 'max_dn_v2 :'+STRING(max_dn_v2[0])  +' -> '+STRING(max_dn_v2[original_index])  +' -> ' + STRING(max_dn_v2[ibest])
    printf, lun_verb, 'min_dn_v2 :'+STRING(min_dn_v2[0])  +' -> '+STRING(min_dn_v2[original_index])  +' -> ' + STRING(min_dn_v2[ibest])
    printf, lun_verb, 'max_dl_v2 :'+STRING(max_dl_v2[0])  +' -> '+STRING(max_dl_v2[original_index])  +' -> ' + STRING(max_dl_v2[ibest])
    printf, lun_verb, 'min_dl_v2 :'+STRING(min_dl_v2[0])  +' -> '+STRING(min_dl_v2[original_index])  +' -> ' + STRING(min_dl_v2[ibest])
    printf, lun_verb, 'prom_cl   :'+STRING(prom_cl[0])    +' -> '+STRING(prom_cl[original_index])    +' -> ' + STRING(prom_cl[ibest])
    printf, lun_verb, 'max_n_cl  :'+STRING(max_n_cl[0])   +' -> '+STRING(max_n_cl[original_index])   +' -> ' + STRING(max_n_cl[ibest])
    printf, lun_verb, 'min_n_cl  :'+STRING(min_n_cl[0])   +' -> '+STRING(min_n_cl[original_index])   +' -> ' + STRING(min_n_cl[ibest])
    printf, lun_verb, 'max_l_cl  :'+STRING(max_l_cl[0])   +' -> '+STRING(max_l_cl[original_index])   +' -> ' + STRING(max_l_cl[ibest])
    printf, lun_verb, 'min_l_cl  :'+STRING(min_l_cl[0])   +' -> '+STRING(min_l_cl[original_index])   +' -> ' + STRING(min_l_cl[ibest])
    printf, lun_verb, 'max_dn_cl :'+STRING(max_dn_cl[0])  +' -> '+STRING(max_dn_cl[original_index])  +' -> ' + STRING(max_dn_cl[ibest])
    printf, lun_verb, 'min_dn_cl :'+STRING(min_dn_cl[0])  +' -> '+STRING(min_dn_cl[original_index])  +' -> ' + STRING(min_dn_cl[ibest])
    printf, lun_verb, 'max_dl_cl :'+STRING(max_dl_cl[0])  +' -> '+STRING(max_dl_cl[original_index])  +' -> ' + STRING(max_dl_cl[ibest])
    printf, lun_verb, 'min_dl_cl :'+STRING(min_dl_cl[0])  +' -> '+STRING(min_dl_cl[original_index])  +' -> ' + STRING(min_dl_cl[ibest])
    printf, lun_verb, 'fill_n_v1 :'+STRING(fill_n_v1[0])  +' -> '+STRING(fill_n_v1[original_index])  +' -> ' + STRING(fill_n_v1[ibest])
    printf, lun_verb, 'fill_par  :'+STRING(fill_par[0])   +' -> '+STRING(fill_par[original_index])   +' -> ' + STRING(fill_par[ibest])
    printf, lun_verb, 'for_tr_sel:'+STRING(for_tr_sel[0]) +' -> '+STRING(for_tr_sel[original_index]) +' -> ' + STRING(for_tr_sel[ibest])
    printf, lun_verb, 'last_4f   :'+STRING(last_4f[0])    +' -> '+STRING(last_4f[original_index])    +' -> ' + STRING(last_4f[ibest])
    printf, lun_verb, 'grd_cmplx :'+STRING(grd_cmplx [0]) +' -> '+STRING(grd_cmplx [original_index]) +' -> ' + STRING(grd_cmplx[ibest])
  ENDIF
  
  IF KEYWORD_SET(logfile) THEN BEGIN  
    printf, loglun, '------------------------------------------------------------------------'
    printf, loglun, '************************************************************************'
    printf, loglun, '------------------------------------------------------------------------'
    printf, loglun, '                      FINAL SETTINGS FOR THIS ION                       '
    printf, loglun, '------------------------------------------------------------------------'
    printf, loglun, '************************************************************************'
    printf, loglun, '------------------------------------------------------------------------'
    
    printf, loglun, '            OLD SETTING / STARTING POINT / FINAL SETTING'
    printf, loglun, 'Nconfigs  :'+STRING(sum_configs[0])+' -> '+STRING(sum_configs[original_index]) +' -> ' +STRING(sum_configs[ibest])
    printf, loglun, 'Nterms    :'+STRING(sum_terms[0])  +' -> '+STRING(sum_terms[original_index])   +' -> ' +STRING(sum_terms[ibest])
    printf, loglun, 'Nlevels   :'+STRING(sum_levels[0]) +' -> '+STRING(sum_levels[original_index])  +' -> ' +STRING(sum_levels[ibest])
    printf, loglun, 'z0_nuc    :'+STRING(z0_nuc[0])     +' -> '+STRING(z0_nuc[original_index])      +' -> ' + STRING(z0_nuc[ibest])
    printf, loglun, 'z_ion     :'+STRING(z_ion[0])      +' -> '+STRING(z_ion[original_index])       +' -> ' + STRING(z_ion[ibest])
    printf, loglun, 'nel       :'+STRING(nel[0])        +' -> '+STRING(nel[original_index])         +' -> ' + STRING(nel[ibest])
;    printf, loglun, 'config    :'+STRING(config[0])     +' -> '+STRING(config[original_index])      +' -> ' + STRING(config[ibest])
    printf, loglun, 'ionpot    :'+STRING(ionpot[0])     +' -> '+STRING(ionpot[original_index])      +' -> ' + STRING(ionpot[ibest])
    printf, loglun, 'no_v_shl  :'+STRING(no_v_shl[0])   +' -> '+STRING(no_v_shl[original_index])    +' -> ' + STRING(no_v_shl[ibest])
    printf, loglun, 'max_dn_v1 :'+STRING(max_dn_v1[0])  +' -> '+STRING(max_dn_v1[original_index])   +' -> ' + STRING(max_dn_v1[ibest])
    printf, loglun, 'min_dn_v1 :'+STRING(min_dn_v1[0])  +' -> '+STRING(min_dn_v1[original_index])   +' -> ' + STRING(min_dn_v1[ibest])
    printf, loglun, 'max_dl_v1 :'+STRING(max_dl_v1[0])  +' -> '+STRING(max_dl_v1[original_index])   +' -> ' + STRING(max_dl_v1[ibest])
    printf, loglun, 'min_dl_v1 :'+STRING(min_dl_v1[0])  +' -> '+STRING(min_dl_v1[original_index])   +' -> ' + STRING(min_dl_v1[ibest])
    printf, loglun, 'max_dn_v2 :'+STRING(max_dn_v2[0])  +' -> '+STRING(max_dn_v2[original_index])   +' -> ' + STRING(max_dn_v2[ibest])
    printf, loglun, 'min_dn_v2 :'+STRING(min_dn_v2[0])  +' -> '+STRING(min_dn_v2[original_index])   +' -> ' + STRING(min_dn_v2[ibest])
    printf, loglun, 'max_dl_v2 :'+STRING(max_dl_v2[0])  +' -> '+STRING(max_dl_v2[original_index])   +' -> ' + STRING(max_dl_v2[ibest])
    printf, loglun, 'min_dl_v2 :'+STRING(min_dl_v2[0])  +' -> '+STRING(min_dl_v2[original_index])   +' -> ' + STRING(min_dl_v2[ibest])
    printf, loglun, 'prom_cl   :'+STRING(prom_cl[0])    +' -> '+STRING(prom_cl[original_index])     +' -> ' + STRING(prom_cl[ibest])
    printf, loglun, 'max_n_cl  :'+STRING(max_n_cl[0])   +' -> '+STRING(max_n_cl[original_index])    +' -> ' + STRING(max_n_cl[ibest])
    printf, loglun, 'min_n_cl  :'+STRING(min_n_cl[0])   +' -> '+STRING(min_n_cl[original_index])    +' -> ' + STRING(min_n_cl[ibest])
    printf, loglun, 'max_l_cl  :'+STRING(max_l_cl[0])   +' -> '+STRING(max_l_cl[original_index])    +' -> ' + STRING(max_l_cl[ibest])
    printf, loglun, 'min_l_cl  :'+STRING(min_l_cl[0])   +' -> '+STRING(min_l_cl[original_index])    +' -> ' + STRING(min_l_cl[ibest])
    printf, loglun, 'max_dn_cl :'+STRING(max_dn_cl[0])  +' -> '+STRING(max_dn_cl[original_index])   +' -> ' + STRING(max_dn_cl[ibest])
    printf, loglun, 'min_dn_cl :'+STRING(min_dn_cl[0])  +' -> '+STRING(min_dn_cl[original_index])   +' -> ' + STRING(min_dn_cl[ibest])
    printf, loglun, 'max_dl_cl :'+STRING(max_dl_cl[0])  +' -> '+STRING(max_dl_cl[original_index])   +' -> ' + STRING(max_dl_cl[ibest])
    printf, loglun, 'min_dl_cl :'+STRING(min_dl_cl[0])  +' -> '+STRING(min_dl_cl[original_index])   +' -> ' + STRING(min_dl_cl[ibest])
    printf, loglun, 'fill_n_v1 :'+STRING(fill_n_v1[0])  +' -> '+STRING(fill_n_v1[original_index])   +' -> ' + STRING(fill_n_v1[ibest])
    printf, loglun, 'fill_par  :'+STRING(fill_par[0])   +' -> '+STRING(fill_par[original_index])    +' -> ' + STRING(fill_par[ibest])
    printf, loglun, 'for_tr_sel:'+STRING(for_tr_sel[0]) +' -> '+STRING(for_tr_sel[original_index])  +' -> ' + STRING(for_tr_sel[ibest])
    printf, loglun, 'last_4f   :'+STRING(last_4f[0])    +' -> '+STRING(last_4f[original_index])     +' -> ' + STRING(last_4f[ibest])
    printf, loglun, 'grd_cmplx :'+STRING(grd_cmplx [0]) +' -> '+STRING(grd_cmplx [original_index])  +' -> ' + STRING(grd_cmplx[ibest])
  ENDIF


ENDIF ELSE BEGIN
  if keyword_set(verbose) then begin 
    printf, lun_verb, '------------------------------------------------------------------------'
    printf, lun_verb, '************************************************************************'
    printf, lun_verb, '------------------------------------------------------------------------'
    printf, lun_verb, '                      FINAL SETTINGS FOR THIS ION                       '
    printf, lun_verb, '------------------------------------------------------------------------'
    printf, lun_verb, '************************************************************************'
    printf, lun_verb, '------------------------------------------------------------------------'
  
  
    printf, lun_verb, 'Nconfigs  :'+STRING(sum_configs[original_index])+' -> ' +STRING(sum_configs[ibest])
    printf, lun_verb, 'Nterms    :'+STRING(sum_terms[original_index])  +' -> ' + STRING(sum_terms[ibest])
    printf, lun_verb, 'Nlevels   :'+STRING(sum_levels[original_index]) +' -> ' + STRING(sum_levels[ibest])
    printf, lun_verb, 'z0_nuc    :'+STRING(z0_nuc[original_index])     +' -> ' + STRING(z0_nuc[ibest])
    printf, lun_verb, 'z_ion     :'+STRING(z_ion[original_index])      +' -> ' + STRING(z_ion[ibest])
    printf, lun_verb, 'nel       :'+STRING(nel[original_index])        +' -> ' + STRING(nel[ibest])
;    printf, lun_verb, 'config    :'+STRING(config[original_index])    +' -> ' + STRING(config[ibest])
    printf, lun_verb, 'ionpot    :'+STRING(ionpot[original_index])     +' -> ' + STRING(ionpot[ibest])
    printf, lun_verb, 'no_v_shl  :'+STRING(no_v_shl[original_index])   +' -> ' + STRING(no_v_shl[ibest])
    printf, lun_verb, 'max_dn_v1 :'+STRING(max_dn_v1[original_index])  +' -> ' + STRING(max_dn_v1[ibest])
    printf, lun_verb, 'min_dn_v1 :'+STRING(min_dn_v1[original_index])  +' -> ' + STRING(min_dn_v1[ibest])
    printf, lun_verb, 'max_dl_v1 :'+STRING(max_dl_v1[original_index])  +' -> ' + STRING(max_dl_v1[ibest])
    printf, lun_verb, 'min_dl_v1 :'+STRING(min_dl_v1[original_index])  +' -> ' + STRING(min_dl_v1[ibest])
    printf, lun_verb, 'max_dn_v2 :'+STRING(max_dn_v2[original_index])  +' -> ' + STRING(max_dn_v2[ibest])
    printf, lun_verb, 'min_dn_v2 :'+STRING(min_dn_v2[original_index])  +' -> ' + STRING(min_dn_v2[ibest])
    printf, lun_verb, 'max_dl_v2 :'+STRING(max_dl_v2[original_index])  +' -> ' + STRING(max_dl_v2[ibest])
    printf, lun_verb, 'min_dl_v2 :'+STRING(min_dl_v2[original_index])  +' -> ' + STRING(min_dl_v2[ibest])
    printf, lun_verb, 'prom_cl   :'+STRING(prom_cl[original_index])    +' -> ' + STRING(prom_cl[ibest])
    printf, lun_verb, 'max_n_cl  :'+STRING(max_n_cl[original_index])   +' -> ' + STRING(max_n_cl[ibest])
    printf, lun_verb, 'min_n_cl  :'+STRING(min_n_cl[original_index])   +' -> ' + STRING(min_n_cl[ibest])
    printf, lun_verb, 'max_l_cl  :'+STRING(max_l_cl[original_index])   +' -> ' + STRING(max_l_cl[ibest])
    printf, lun_verb, 'min_l_cl  :'+STRING(min_l_cl[original_index])   +' -> ' + STRING(min_l_cl[ibest])
    printf, lun_verb, 'max_dn_cl :'+STRING(max_dn_cl[original_index])  +' -> ' + STRING(max_dn_cl[ibest])
    printf, lun_verb, 'min_dn_cl :'+STRING(min_dn_cl[original_index])  +' -> ' + STRING(min_dn_cl[ibest])
    printf, lun_verb, 'max_dl_cl :'+STRING(max_dl_cl[original_index])  +' -> ' + STRING(max_dl_cl[ibest])
    printf, lun_verb, 'min_dl_cl :'+STRING(min_dl_cl[original_index])  +' -> ' + STRING(min_dl_cl[ibest])
    printf, lun_verb, 'fill_n_v1 :'+STRING(fill_n_v1[original_index])  +' -> ' + STRING(fill_n_v1[ibest])
    printf, lun_verb, 'fill_par  :'+STRING(fill_par[original_index])   +' -> ' + STRING(fill_par[ibest])
    printf, lun_verb, 'for_tr_sel:'+STRING(for_tr_sel[original_index]) +' -> ' + STRING(for_tr_sel[ibest])
    printf, lun_verb, 'last_4f   :'+STRING(last_4f[original_index])    +' -> ' + STRING(last_4f[ibest])
    printf, lun_verb, 'grd_cmplx :'+STRING(grd_cmplx [original_index]) +' -> ' + STRING(grd_cmplx[ibest])
  ENDIF

  IF keyword_set(loglun) THEN BEGIN
  
    printf, loglun, '------------------------------------------------------------------------'
    printf, loglun, '************************************************************************'
    printf, loglun, '------------------------------------------------------------------------'
    printf, loglun, '                      FINAL SETTINGS FOR THIS ION                       '
    printf, loglun, '------------------------------------------------------------------------'
    printf, loglun, '************************************************************************'
    printf, loglun, '------------------------------------------------------------------------'
    
      
    printf, loglun, 'Nconfigs  :'+STRING(sum_configs[original_index]) +' -> ' +STRING(sum_configs[ibest])
    printf, loglun, 'Nterms    :'+STRING(sum_terms[original_index]) +' -> ' +STRING(sum_terms[ibest])
    printf, loglun, 'Nlevels   :'+STRING(sum_levels[original_index]) +' -> ' +STRING(sum_levels[ibest])
    printf, loglun, 'z0_nuc    :'+STRING(z0_nuc[original_index])        +' -> ' + STRING(z0_nuc[ibest])
    printf, loglun, 'z_ion     :'+STRING(z_ion[original_index])        +' -> ' + STRING(z_ion[ibest])
    printf, loglun, 'nel       :'+STRING(nel[original_index])       +' -> ' + STRING(nel[ibest])
;    printf, loglun, 'config    :'+STRING(config[original_index])    +' -> ' + STRING(config[ibest])
    printf, loglun, 'ionpot    :'+STRING(ionpot[original_index])    +' -> ' + STRING(ionpot[ibest])
    printf, loglun, 'no_v_shl  :'+STRING(no_v_shl[original_index])  +' -> ' + STRING(no_v_shl[ibest])
    printf, loglun, 'max_dn_v1 :'+STRING(max_dn_v1[original_index]) +' -> ' + STRING(max_dn_v1[ibest])
    printf, loglun, 'min_dn_v1 :'+STRING(min_dn_v1[original_index]) +' -> ' + STRING(min_dn_v1[ibest])
    printf, loglun, 'max_dl_v1 :'+STRING(max_dl_v1[original_index]) +' -> ' + STRING(max_dl_v1[ibest])
    printf, loglun, 'min_dl_v1 :'+STRING(min_dl_v1[original_index]) +' -> ' + STRING(min_dl_v1[ibest])
    printf, loglun, 'max_dn_v2 :'+STRING(max_dn_v2[original_index]) +' -> ' + STRING(max_dn_v2[ibest])
    printf, loglun, 'min_dn_v2 :'+STRING(min_dn_v2[original_index]) +' -> ' + STRING(min_dn_v2[ibest])
    printf, loglun, 'max_dl_v2 :'+STRING(max_dl_v2[original_index]) +' -> ' + STRING(max_dl_v2[ibest])
    printf, loglun, 'min_dl_v2 :'+STRING(min_dl_v2[original_index]) +' -> ' + STRING(min_dl_v2[ibest])
    printf, loglun, 'prom_cl   :'+STRING(prom_cl[original_index])   +' -> ' + STRING(prom_cl[ibest])
    printf, loglun, 'max_n_cl  :'+STRING(max_n_cl[original_index])  +' -> ' + STRING(max_n_cl[ibest])
    printf, loglun, 'min_n_cl  :'+STRING(min_n_cl[original_index])  +' -> ' + STRING(min_n_cl[ibest])
    printf, loglun, 'max_l_cl  :'+STRING(max_l_cl[original_index])  +' -> ' + STRING(max_l_cl[ibest])
    printf, loglun, 'min_l_cl  :'+STRING(min_l_cl[original_index])  +' -> ' + STRING(min_l_cl[ibest])
    printf, loglun, 'max_dn_cl :'+STRING(max_dn_cl[original_index]) +' -> ' + STRING(max_dn_cl[ibest])
    printf, loglun, 'min_dn_cl :'+STRING(min_dn_cl[original_index]) +' -> ' + STRING(min_dn_cl[ibest])
    printf, loglun, 'max_dl_cl :'+STRING(max_dl_cl[original_index]) +' -> ' + STRING(max_dl_cl[ibest])
    printf, loglun, 'min_dl_cl :'+STRING(min_dl_cl[original_index]) +' -> ' + STRING(min_dl_cl[ibest])
    printf, loglun, 'fill_n_v1 :'+STRING(fill_n_v1[original_index]) +' -> ' + STRING(fill_n_v1[ibest])
    printf, loglun, 'fill_par  :'+STRING(fill_par[original_index])  +' -> ' + STRING(fill_par[ibest])
    printf, loglun, 'for_tr_sel:'+STRING(for_tr_sel[original_index])+' -> ' + STRING(for_tr_sel[ibest])
    printf, loglun, 'last_4f   :'+STRING(last_4f[original_index])   +' -> ' + STRING(last_4f[ibest])
    printf, loglun, 'grd_cmplx :'+STRING(grd_cmplx [original_index])+' -> ' + STRING(grd_cmplx[ibest])
  ENDIF
ENDELSE

prom_rules= {config     : config[ibest],                    $
             no_v_shl   : no_v_shl[ibest],                  $
             max_dn_v1  : max_dn_v1[ibest],                 $
             min_dn_v1  : min_dn_v1[ibest],                 $
             max_dl_v1  : max_dl_v1[ibest],                 $
             min_dl_v1  : min_dl_v1[ibest],                 $
             max_dn_v2  : max_dn_v2[ibest],                 $
             min_dn_v2  : min_dn_v2[ibest],                 $
             max_dl_v2  : max_dl_v2[ibest],                 $
             min_dl_v2  : min_dl_v2[ibest],                 $
             prom_cl    : prom_cl[ibest],                   $
             max_n_cl   : max_n_cl[ibest],                  $
             min_n_cl   : min_n_cl[ibest],                  $
             max_l_cl   : max_l_cl[ibest],                  $
             min_l_cl   : min_l_cl[ibest],                  $
             max_dn_cl  : max_dn_cl[ibest],                 $
             min_dn_cl  : min_dn_cl[ibest],                 $
             max_dl_cl  : max_dl_cl[ibest],                 $
             min_dl_cl  : min_dl_cl[ibest],                 $
             fill_n_v1  : fill_n_v1[ibest],                 $
             fill_par   : fill_par[ibest],                  $
             for_tr_sel : for_tr_sel[ibest],                $
             last_4f    : last_4f[ibest],                   $
             grd_cmplx  : grd_cmplx[ibest]}


  
adas8xx_promotions,  z0_nuc     = z0_nuc[ibest],                    $
                     z_ion      = z_ion[ibest],                     $
                     ionpot     = ionpot[ibest],                    $
                     prom_rules = prom_rules,                       $
                     promotion_results = promotion_results;,        $


;-----------------------------------------------------------------------
; print the configuration sets to both screen & log file
;-----------------------------------------------------------------------

if keyword_set(verbose) then begin
  printf, lun_verb, '*-------------------- FINAL CONFIGURATION SET --------------------*'

  printf, lun_verb, promotion_results.grd_cfg

  FOR icfg=0, n_elements(promotion_results.ex_cfg)-1 DO BEGIN
    printf, lun_verb, promotion_results.ex_cfg[icfg]
  ENDFOR
endif

IF keyword_set(logfile) THEN BEGIN
  printf, loglun, '*-------------------- FINAL CONFIGURATION SET --------------------*'
  printf, loglun, promotion_results.grd_cfg
  FOR icfg=0, n_elements(promotion_results.ex_cfg)-1 DO BEGIN
    printf, loglun, promotion_results.ex_cfg[icfg]
  ENDFOR
  close, loglun
  free_lun, loglun
ENDIF


;-----------------------------------------------------------------------  
; return best rules:
;-----------------------------------------------------------------------  

z0_nuc     = z0_nuc[ibest]
z_ion      = z_ion[ibest]
nel        = nel[ibest]
config     = config[ibest]
ionpot     = ionpot[ibest]
no_v_shl   = no_v_shl[ibest]
max_dn_v1  = max_dn_v1[ibest]
min_dn_v1  = min_dn_v1[ibest]
max_dl_v1  = max_dl_v1[ibest]
min_dl_v1  = min_dl_v1[ibest]
max_dn_v2  = max_dn_v2[ibest]
min_dn_v2  = min_dn_v2[ibest]
max_dl_v2  = max_dl_v2[ibest]
min_dl_v2  = min_dl_v2[ibest]
prom_cl    = prom_cl[ibest]
max_n_cl   = max_n_cl[ibest]
min_n_cl   = min_n_cl[ibest]
max_l_cl   = max_l_cl[ibest]
min_l_cl   = min_l_cl[ibest]
max_dn_cl  = max_dn_cl[ibest]
min_dn_cl  = min_dn_cl[ibest]
max_dl_cl  = max_dl_cl[ibest]
min_dl_cl  = min_dl_cl[ibest]
fill_n_v1  = fill_n_v1[ibest]
fill_par   = fill_par[ibest]
for_tr_sel = for_tr_sel[ibest]
last_4f    = last_4f[ibest]
grd_cmplx  = grd_cmplx[ibest]


outdata={ z0_nuc  :  z0_nuc     ,$
       z_ion      :  z_ion      ,$
       nel        :  nel        ,$
       config     :  config     ,$
       ionpot     :  ionpot     ,$
       no_v_shl   :  no_v_shl   ,$
       max_dn_v1  :  max_dn_v1  ,$
       min_dn_v1  :  min_dn_v1  ,$
       max_dl_v1  :  max_dl_v1  ,$
       min_dl_v1  :  min_dl_v1  ,$
       max_dn_v2  :  max_dn_v2  ,$
       min_dn_v2  :  min_dn_v2  ,$
       max_dl_v2  :  max_dl_v2  ,$
       min_dl_v2  :  min_dl_v2  ,$
       prom_cl    :  prom_cl    ,$
       max_n_cl   :  max_n_cl   ,$
       min_n_cl   :  min_n_cl   ,$
       max_l_cl   :  max_l_cl   ,$
       min_l_cl   :  min_l_cl   ,$
       max_dn_cl  :  max_dn_cl  ,$
       min_dn_cl  :  min_dn_cl  ,$
       max_dl_cl  :  max_dl_cl  ,$
       min_dl_cl  :  min_dl_cl  ,$
       fill_n_v1  :  fill_n_v1  ,$
       fill_par   :  fill_par   ,$
       for_tr_sel :  for_tr_sel ,$
       last_4f    :  last_4f    ,$
       grd_cmplx  :  grd_cmplx  }

END
