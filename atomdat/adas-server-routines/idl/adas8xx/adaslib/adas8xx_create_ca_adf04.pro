;+
; PROJECT : ADAS 
;
; NAME    : adas8xx_create_ca_adf04
;
; PURPOSE : Generates configuration average adf04 files
;
; EXPLANATION:
;       This routines takes a given set of configurations, nuclear and ionic
;       charge and generates a complete adf04 file (type I and/or type III)
;       working in a PWB CA approximation.
;
; USE:
;       An example;
;
;       z0_nuc=6
;       z_ion=3
;        
;       occup=[ [2,1,0,0,0,0], $
;               [2,0,0,0,0,1], $
;               [2,0,0,1,0,0], $
;               [2,0,0,0,1,0], $
;               [2,0,1,0,0,0] $
;               ] 
;        
;       caex,z_ion,z0_nuc,occup,adf04_t1_file='adf04_1',adf04_t3_file='adf04_3'
;
; INPUTS:
;       z_ion :   Ion charge
;       z0_nuc:   Nuclear charge
;       occup: Occupation numbers of configurations
;                   First index:  configuration number
;                   Second index: orbital number.
;       plasma: structure containing temperature information:
;            theta: array of electron temperatures
;            noscale: is theta an array of scaled temperatures?
;            indx_theta: subset of theta to use for calculations.
;
; OPTIONAL INPUTS:
;       None.
;
; OUTPUTS:
;       None.
;
; OPTIONAL OUTPUTS:
;       exit_status: returns -1 if ADF04 generation has failed for any reason
;
; KEYWORD PARAMETERS:
;       ionpot:    Ionisation potential, if not specified it will be read from central ADAS
;       adf04_t1_file: Filename of type I file to produce
;       adf04_t3_file: Filename of type III file to produce
;       keeppass:  Do not remove temporary passing files.
;       occ2cow:   Convert occupation numbers to Cowan (i.e. adas801) input standard
;       archive_dir: directory to store output files in (default = current directory)
;       archive_files: put outputs into subdirectories of archive_dir
;       cowan_scale_factors: Slater parameter multipliers for Cowan 
;                     code (optional, defaults are usually sufficient)
;       lun_verb: logical unit number for file for diagnostic info
;
;
; CALLS:
;       read_adf00: read adf00 files.
;       h4mxwl: Maxwell average collision strengths.
;       xxspln: Used for splining (momentum transfer)
;
; SIDE EFFECTS:
;       This function spawns UNIX commands.
;
; CATEGORY:
;       Series 8 utility.
;
; WRITTEN:
;       Allan Whiteford
;
; MODIFIED:
;       Version 1.1   Allan Whiteford    01-11-2004
;                     First release (originally h8caex).
;       Version 1.2   Hugh Summers       28-09-2006
;                     Major revision and name change to
;                     adas8xx_create_ca_adf04
;       Version 1.3   Hugh Summers       04-12-2006
;                     Added call to adas8xx_cowan_string_check 
;       Version 1.4   Hugh Summers       24-07-2007
;                     Modified temporary filenames to make unique 
;       Version 1.5   Adam Foster        08-01-2008
;                     Modified to return 'exit_status'. Also quits
;                     rather than crashes if Cowan run has failed
;       Version 1.6   Hugh Summers       25-07-2008
;                     Put ,/sh on spawning final copying to archive 
;       Version 1.7   Adam Foster        25-03-2009
;                     Changed z0, z1 inputs to z0_nuc, z_ion
;                     Moved plasma conditions into plasma structure
;                     Added exitstatus output
;                     Added lun_verb for diagnostic output to file
;                     Added cowan_scale_factors to allow custom
;                     adjustments to Slater parameters
;       Version 1.8   Martin O'Mullane
;                     Call binaries from central ADAS rather than
;                     offline_adas/adas8#1/bin.
;       Version 1.9   Martin O'Mullane
;                     The get_orbital.x program returns the orbital energies
;                     line with an extra leading space. Do not write this
;                     space to the output adf04 file or xxdata_04 will fail.
;
; VERSION:
;       1.1     01-11-2004
;       1.2     28-09-2006
;       1.3     04-12-2006
;       1.4     24-07-2007
;       1.5     08-01-2008
;       1.6     25-07-2008
;       1.7     25-03-2009
;       1.8     06-11-2009
;       1.9     30-11-2011
;-
;------------------------------------------------------------------
pro adas8xx_create_ca_adf04, z_ion,                                    $
                             z0_nuc,                                   $
                             occup,                                    $
                             ionpot              = ionpot,             $
                             plasma              = plasma,             $
                             adf04_t1_file       = adf04_t1_file,      $
                             adf04_t3_file       = adf04_t3_file,      $
                             archive_dir         = archive_dir,        $
                             archive_files       = archive_files,      $
                             keeppass            = keeppass,           $
                             exit_status         = exit_status,        $
                             cowan_scale_factors = cowan_scale_factors,$
                             lun_verb            = lun_verb
                             
        IF not keyword_set(exit_status) then exit_status =0 
        
        adasbin=getenv('ADASFORT')+'/'
;        adasbin=adasbin+'../offline_adas/adas8#1/bin/'

        if not keyword_set(ionpot) then begin
                read_adf00,z0_nuc=z0_nuc,z_ion=z_ion,ionpot=ionpot
        endif
        
        if keyword_set(adf04_t1_file) then begin
                openw,type1,adf04_t1_file,/get_lun
        endif else begin
                openw,type1,'/dev/null',/get_lun
        endelse
        
        if keyword_set(adf04_t3_file) then begin
                openw,type3,adf04_t3_file,/get_lun
        endif else begin
                openw,type3,'/dev/null',/get_lun
        endelse
        
        energy=dblarr((size(occup))[2])
        stat_wt=lonarr((size(occup))[2])
        conf=strarr((size(occup))[2])
        
        
;------------------------------------------------
; extract relevant data from plasma structure
;------------------------------------------------
        if plasma.theta_noscale EQ 1 then $
           theta=plasma.theta/((z_ion+1)^2)  else $ ; convert to scaled Te
           theta=plasma.theta                       ; already scaled Te

        indx_theta=plasma.indx_theta

;------------------------------------------------
; prepare for archiving if required
;------------------------------------------------

        if keyword_set(archive_files) then begin

            if keyword_set(archiv_dir) then begin
    
                res = file_info(archive_dir)
                if res.exists EQ 1 AND res.directory EQ 0 then message,      $
                     'Cannot create ' + archive_dir
                if res.exists EQ 0 then file_mkdir, archive_dir
        
            endif else begin
    
                cmd = 'pwd'
                spawn, cmd, result
                here = result[0]
                archive_dir = here
        
            endelse

            p0=0
            p1=strpos(adf04_t1_file,'_',p0)
            archive_adf04_dir=archive_dir + '/' +                            $
                              strmid(adf04_t1_file,p0,p1-p0)            
            res = file_info(archive_adf04_dir)
            if res.exists EQ 1 AND res.directory EQ 0 then message,          $
                 'Cannot create ' + archive_adf04_dir
            if res.exists EQ 0 then file_mkdir, archive_adf04_dir
    
            p0=p1+1
            p1=strpos(adf04_t1_file,'_',p0)
            archive_adf04_element_dir=archive_adf04_dir + '/' +              $
                              strmid(adf04_t1_file,p0,p1-p0)            
            res = file_info(archive_adf04_element_dir)
            if res.exists EQ 1 AND res.directory EQ 0 then message,          $
                 'Cannot create ' + archive_adf04_element_dir
            if res.exists EQ 0 then file_mkdir, archive_adf04_element_dir
    
            p0=p1+1
            archive_adf04_t1_file=archive_adf04_element_dir + '/' +          $
                               strmid(adf04_t1_file,p0)                 
            archive_adf04_t3_file=archive_adf04_element_dir + '/' +          $
                               strmid(adf04_t3_file,p0)                 


        endif

        
;-----------------------------------------------------------------------------
; Shell definitions and occupations - to ndshell=36 (8k)
;-----------------------------------------------------------------------------
        
        shmax        = [   2,   2,        6,   2,   6,  10,   2,   6,  10,  14,        2,   6,  10,  14,  18,   2,   6,  10,           $
                     14,  18,  22,   2,   6,  10,  14,  18,  22,  26,        2,   6,  10,  14,  18,  22,  26,  30]
        shl_lab = ['1s','2s','2p','3s','3p','3d','4s','4p','4d','4f','5s','5p','5d','5f','5g','6s','6p','6d',           $
                   '6f','6g','6h','7s','7p','7d','7f','7g','7h','7i','8s','8p','8d','8f','8g','8h','8i','8k']
        lval        = [   0,   0,        1,   0,   1,   2,   0,   1,   2,   3,        0,   1,   2,   3,   4,   0,   1,   2,           $
                      3,   4,        5,   0,   1,   2,   3,   4,   5,   6,        0,   1,   2,   3,   4,   5,   6,   7]
        eiss_lab= [ '1', '2', '3', '4', '5', '6', '7', '8', '9', 'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I',           $
                    'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z', 'a']

        tegrid=(z_ion+1)^2d0 * theta[indx_theta]


        xegrid_out=[        1.001d0, 1.002d0, 1.003d0, 1.005d0, 1.007d0, 1.010d0, 1.020d0, 1.030d0, 1.050d0,           $ 
                        1.070d0, 1.100d0, 1.200d0, 1.300d0, 1.500d0, 1.700d0, 2.000d0, 2.500d0, 3.000d0,           $ 
                        5.000d0, 7.000d0, 1.000d1, 1.500d1, 2.000d1, 5.000d1, 7.000d1, 1.000d2, 1.500d2,           $ 
                        2.000d2, 5.000d2, 7.000d2, 1.000d3 ]

        xegrid_print=[0,where(xegrid_out ge 1.01)]

;-------------------------------------------------------------------------------------
; set finite threshold by Cowan energy shift for collision strengths for positive ions
;-------------------------------------------------------------------------------------
        if z_ion ge 1 then begin
                xegrid_calc=xegrid_out + (3d0 / (1d0 + xegrid_out))
                ; Cowan equation 18.160
        endif else begin
                xegrid_calc=xegrid_out
        endelse

;---------------------------------------------------
; set Cowan structure calculation control parameters
;---------------------------------------------------
        if keyword_set(cowan_scale_factors) then begin

          fii   = cowan_scale_factors[0]
          zeta  = cowan_scale_factors[1]
          fij   = cowan_scale_factors[2]
          gij   = cowan_scale_factors[3]
          rij   = cowan_scale_factors[4]
        
        
        endif else begin
        
          fii   = 80
          zeta  = 90
          fij   = 80
          gij   = 80
          rij   = 80
          if z0_nuc EQ 1 then begin
                  fii   = 75
                  zeta  = 95
                  fij   = 75
                  gij   = 75
                  rij   = 60
          endif
     
          test = z0_nuc-z_ion+1
     
          if test GE 5 AND test LE 10 then begin
                  fii   = 80
                  zeta  = 99
                  fij   = 85
                  gij   = 85
                  rij   = 85
          endif   
  
          if test GT 10 then begin
                  fii   = 93
                  zeta  = 99
                  fij   = 93
                  gij   = 93
                  rij   = 93
          endif   
        endelse
        scale=string(fii,zeta,fij,gij,rij, format='(5i2)')
;----------------------------------------------
; set configuration average statistical weights
;----------------------------------------------

        for i=0,(size(occup))[2]-1 do begin
                stat_wt[i]=1
                for j=0,(size(occup))[1]-1 do begin
                        stat_wt[i]=stat_wt[i]*factorial(4*lval[j]+2)/$
                                factorial(occup[j,i])/$
                                factorial(4*lval[j]+2-occup[j,i])
                endfor
        end

;----------------------------------------------------
; create data set input drivers for rcn.x with unique
; ion file name and execute 
;----------------------------------------------------

        ionname=strlowcase(strcompress(xxesym(z0_nuc)+string(z_ion,format='(i5)'),/remove_all))

        name=ionname+'_rcn1_input_unsorted'
        
        openw,lun,name, /get_lun
        printf,lun,'2  -5    2   10  1.0    5.d-09    5.d-11-2  0130    1.0 0.65  0.0  0.5'
        
        start_shells=intarr((size(occup))[2])
       
        for i=0,(size(occup))[2]-1 do begin
                line=occ2cow(reform(occup[*,i]),z0_nuc,z_ion,izc,start_shell)
                
                new_line=adas8xx_cowan_string_check(line,z0_nuc,izc,ifail,$
                                                    lun_verb=lun_verb)
                
                if ifail eq 0 then begin
                    line=new_line
                endif else begin
                    stop
                endelse        
                
                start_shells[i]=start_shell
                line = "   "+string(z0_nuc,format='(I2)') +"  "+ string(izc,format='(I3)') + " xx cf"+string(i,format='(I2)')+"              "+line
                printf,lun,line
        end
        printf,lun,"   -1"
              
        free_lun,lun

;----------------------------------------------------
; sort the configurations by parity
;----------------------------------------------------

        hx34sr,ionname+'_rcn1_input_unsorted',ionname+'_rcn1_input'
        
        openw,lun,ionname+'_rcn1_driver',/get_lun
        printf,lun,1
        printf,lun,ionname+'_rcn1_input'
        printf,lun,ionname+'_rcn1_out'
        printf,lun,ionname+'_d2'
        free_lun,lun
        time=STRCOMPRESS(systime(),/remove_all)



        cmd= adasbin + 'rcn.x < '+ionname+'_rcn1_driver'
        spawn, cmd

        dummy="string"
        openr,lun,ionname+'_rcn1_out',/get_lun
        while (not eof(lun)) do begin
                readf,lun,dummy
                pos=strpos(dummy,"xx cf")
                if pos ne -1 then begin
                        pos=strpos(dummy,"nconf")
                        if pos eq -1 then begin
                                energy_temp=0.00d0
                                idx=-1
                                reads,dummy,idx,energy_temp,format='(7X,I2,16X,F12.4)'
                                energy[idx]=energy_temp
                        endif
                endif
        end

        free_lun,lun

        esort=sort(energy)

        for i=0,(size(conf))[1]-1 do begin
                for j=start_shells[i],(size(occup))[1]-1 do begin
                        if occup[j,i] gt 0 then begin
                                conf[i]=conf[i] + strtrim(string( (((conf[i] eq "") and (occup(j,i) lt 10)) ? 0 : 50)  +occup[j,i]),2) + eiss_lab[j]  
                        endif
                end       
        end

        esym=xxesym(z0_nuc)
        if strlen(esym) eq 1 then esym=esym+' '

        printf,type1,esym,'+',z_ion,z0_nuc,z_ion+1,ionpot * 8065.54445,format='(A2,A1,I2,8X,I2,8X,I2,5X,F15.1)'
        printf,type3,esym,'+',z_ion,z0_nuc,z_ion+1,ionpot * 8065.54445,format='(A2,A1,I2,8X,I2,8X,I2,4X,F12.1)'

        conflen=max(strlen(conf)) > 19

        for i=0,(size(conf))[1]-1 do begin
                if strlen(conf[i]) lt conflen-1 then begin
                        conf[i]=string(conf[i],' ',format='(A'+strtrim(string(strlen(conf[i])),2)+','+strtrim(string(conflen-strlen(conf[i])-1),2)+'X,A1)')
                endif else if strlen(conf[i]) eq conflen-1 then begin
                        conf[i]=string(conf[i],' ',format='(A'+strtrim(string(strlen(conf[i])),2)+','+'A1)')
                endif        
        end
        
        for i=0,(size(esort))[1]-1 do begin
                printf,type1,i+1,conf[esort[i]],"(0)0(",(stat_wt[esort[i]] -1d0 ) / 2d0,")",(energy[esort[i]]-energy[esort[0]]) * 109737.26d0,format='(I5,1X,A'+strtrim(string(conflen),2)+',1X,A5,F8.1,A1,F15.1)'
                printf,type3,i+1,conf[esort[i]],"(0)0(",(stat_wt[esort[i]] -1d0 ) / 2d0,")",(energy[esort[i]]-energy[esort[0]]) * 109737.26d0,format='(I5,1X,A'+strtrim(string(conflen),2)+',1X,A5,F8.1,A1,F15.1)'
        end

        ; orbital energies from rcn - note line is meant to be read with (1x, a)        
        
        cmd = adasbin + 'get_orbital.x < '+ ionname+'_rcn1_out'

        spawn, cmd, res, /sh
        
        orbenergies = res[0]
        
        if (where(orbenergies ne 0.0))[0] ne -1 then begin
                printf,type1, strmid(orbenergies, 1)
                printf,type3, strmid(orbenergies, 1)
        endif else begin
                printf,type1,-1,format='(I5)'
                printf,type3,-1,format='(I5)'
        endelse
        
        fmt = '(F5.1,I5,8X,50e9.2)'
        
        str = string(z_ion+1, 1, xegrid_out[xegrid_print],format=fmt)
        b   = byte(str)
        i1  = where(b EQ 101b, complement=i2)              
        str = string(b[i2])
        printf, type1, str

        str = string(z_ion+1, 3, tegrid[*],format=fmt)
        b   = byte(str)
        i1  = where(b EQ 101b, complement=i2)              
        str = string(b[i2])
        printf, type3, str

        openw,lun,ionname+'_rcn2_input_d',/get_lun
                printf,lun,'g5inp     000 0.00000         00                 3'+scale+'      0111 1020'
                printf,lun,'       -1'
        free_lun,lun
        

        openw,lun,ionname+'_rcn2_driver_d',/get_lun
                printf,lun,1
                printf,lun,ionname+'_rcn2_input_d'
                printf,lun,ionname+'_d2'
                printf,lun,ionname+'_rcn2_output_d'
                printf,lun,ionname+'_d_d'
        close, lun
        free_lun,lun


        time=STRCOMPRESS(systime(),/REMOVE_ALL)
        cmd=adasbin + 'rcn2.x < ' + ionname+'_rcn2_driver_d'
        spawn, cmd
        
        openr,lun,ionname+'_rcn2_output_d',/get_lun

        dummy="string"
        
        rad_idx1=intarr(1)
        rad_idx2=intarr(1)
        rad_avalue=dblarr(1)
        

        while (not eof(lun)) do begin
                readf,lun,dummy
                
; insert check for '******' which appears sometimes & will crash everything
                IF STRPOS(dummy,"******") NE -1 THEN BEGIN
                  IF keyword_set(lun_verb) THEN BEGIN
                    printf,lun_verb, 'This file is broken: ****** present'
                  ENDIF
                  close, lun
                  free_lun, lun
                  free_lun, type1
                  free_lun, type3
                  exit_status=-1
                  return
                ENDIF ELSE BEGIN  
                
                  pos=strpos(dummy,"xx cf")
                  if pos ne -1 then begin
                          pos=strpos(dummy,"a0")
                          if pos ne -1 then begin
                                avalue = 1.0e-30
                                  mupole_temp=0.0
                                  idx1=0
                                  idx2=0
                                  type='??'
                                  reads,dummy,idx1,idx2,mupole_temp,type,format='(25X,I2,18X,I2,11X,F11.6,9X,A2)'
                                  if (idx1 ne idx2 and mupole_temp ne 0.0) then begin
                                          ediff=energy[idx1] - energy[idx2]
                                          if ediff gt 0 then begin
                                                temp=idx1
                                                idx1=idx2
                                                idx2=temp
                                        endif
                                                        
                                          stat_upper=stat_wt[idx2]
                                          stat_lower=stat_wt[idx1]
                                          diff=occup[*,idx1] - occup[*,idx2]
                                          q1=occup[(where(diff gt 0)),idx1] 
                                          q2=occup[(where(diff lt 0)),idx2] 
                                          l1=lval[(where(diff gt 0))]
                                          l2=lval[(where(diff lt 0))]
                                          ediff=abs(ediff)
                                          
                                          
                                          
                                          if type eq 'r1' then begin
                                                  
                                                  avalue = 2.0261d-6 * (ediff * 109737.26d0)^3d0 * mupole_temp^2d0; / stat_upper
                                                  ; cowan equation 14.33
                                          
                                                   factor=2d0 * q2 * (4d0 * l1 + 3 - q1) / ((4d0 * l1 + 2d0) * (4d0 * l2 + 2d0)) * max([l1,l2])
                                                  ; Pindzola equation 14 corrected 
                                                  
                                                  avalue = avalue * factor
                                          
                                          endif
                                          
                                          if type eq 'r2' then begin
                                          
                                                  avalue = 1.1199e-22 * (ediff * 109737.26d0)^5d0 * mupole_temp^2d0; / stat_upper
                                                  ; cowan equation 15.13
                                                  
                                                  factor= 2d0 * q2 * (4d0 * l1 + 3 - q1) / ((4d0 * l1 + 2d0) * (4d0 * l2 + 2d0)) *           $
                                                        (2d0*l1+1)*(2d0*l2+1)*wig3j(l1,2.0,l2,0.0,0.0,0.0)^2
                                                  ; expression evaluated from Cowan
                                                  
                                                  avalue = avalue * factor
                                                  
                                          
                                          endif
                                        
                                        avalue = avalue > 1.0e-30
                                        
                                          rad_idx1=[rad_idx1,idx1]
                                          rad_idx2=[rad_idx2,idx2]
                                          rad_avalue=[rad_avalue,avalue]
                                          
                                  endif
                          endif
                  endif
                endelse
        end

        if ((size(rad_idx1))[1]-1 LT 1) THEN begin
          IF keyword_set(lun_verb) THEN BEGIN
            printf,lun_verb, 'This file is broken: No convergence found'
          ENDIF
          close, lun
          free_lun, lun
          free_lun, type1
          free_lun, type3
          exit_status=-1
          return
        endif
        rad_idx1=rad_idx1[1:(size(rad_idx1))[1]-1]
        rad_idx2=rad_idx2[1:(size(rad_idx2))[1]-1]
        rad_avalue=rad_avalue[1:(size(rad_avalue))[1]-1]

        free_lun,lun
        
        openw,lun,ionname+'_rcn2_input_j',/get_lun
                printf,lun,'g5inp     000 0.00000         0050211            3'+scale+'      0111 1020'
                printf,lun,'       -1'
        free_lun,lun
        
        openw,lun,ionname+'_rcn2_driver_j',/get_lun
                printf,lun,1
                printf,lun,ionname+'_rcn2_input_j'
                printf,lun,ionname+'_d2'
                printf,lun,ionname+'_rcn2_output_j'
                printf,lun,ionname+'_d_j'
        free_lun,lun
        time=strcompress(systime(),/remove_all)

        spawn, adasbin + 'rcn2.x < ' + ionname+'_rcn2_driver_j'            
            
            
;-----------------------------------------------------------------------
; check now that rcn2.x has not failed giving a ENMAX<0.000 error.
; If this error is present the rcn2_output_j file will not get as far
; as lines containing 'sqrtdel', so use this as the test condition
;-----------------------------------------------------------------------

        openr,lun,ionname+'_rcn2_output_j',/get_lun
        
        dummy='string'
        IF exit_status LT 0 THEN BEGIN
          print, 'error with exit_status, =', exit_status
          stop
        ENDIF
        exit_status=-2
        WHILE NOT EOF(lun) DO BEGIN
          readf, lun, dummy
          IF (STRPOS(dummy, '0finished g5inp')) NE -1 THEN BEGIN
            exit_status=0
            break
          ENDIF
        ENDWHILE
        free_lun, lun
        print, exit_status
;        stop
        IF exit_status LT 0 THEN BEGIN
;          stop

          IF keyword_set(lun_verb) THEN BEGIN
            printf,lun_verb, '  RCN has failed to converge: this configuration set'+$
                ' cannot be used'
          ENDIF
          return
        ENDIF
;        stop
;        free_lun, lun

        openr,lun,ionname+'_rcn2_output_j',/get_lun
        dummy="string"


readagain:

        readf,lun,dummy,format='(A50)'          
        if strpos(dummy,'nbigks') ne -1 then nbigks=fix(strmid(dummy,strpos(dummy,'nbigks')+7))
        if strtrim(dummy,2) ne 'k-values' then goto,readagain
        readf,lun,dummy
        kvalues=dblarr(nbigks)
        readf,lun,kvalues,format='(8F14.7)'
        
; -------------------------------------------------------------------------------------------------------------------
; Use bessel_types for detecting j0, j1 and j2 types if present. Distinguish from transition stack vector bessel_type         
; -------------------------------------------------------------------------------------------------------------------
        bessel_types=strarr(1)
        bessel_types[0]='j1'

        bessel_idx1=intarr(1)
        bessel_idx2=intarr(1)
        bessel_type=strarr(1)
        bessel_mpol=dblarr(1)
        bessel_kidx=intarr(1)
        kidx=0

        while (not eof(lun)) do begin
                readf,lun,dummy
                pos=strpos(dummy,"//j")
                if pos ne -1 then begin
                        mupole_temp=0.0
                        idx1=0
                        idx2=0
                        type='??'
                        ok='?'
                        reads,dummy,idx1,idx2,mupole_temp,ok,type,format='(25X,I2,18X,I2,11X,F11.6,6X,A1,2X,A2)'
                        if (idx1 ne idx2 and ok ne ' ') then begin
                        
                                if where(bessel_types eq type) eq -1 then bessel_types=[bessel_types,type]
                                
                                if (kidx eq nbigks) then begin
                                        kidx=0
                                endif
                                mupole_temp=abs(mupole_temp)
; ------------------------------------------------------------------------------------------------------------------------
; Swap read idx1 and idx2 to maintain energy(idx1) <= energy(idx2) in transition stack vectors bessel_idx1 and bessel_idx2         
; ------------------------------------------------------------------------------------------------------------------------
                                ediff=energy(idx1)-energy(idx2)
                                if ediff gt 0 then begin
                                    itemp=idx1
                                    idx1=idx2
                                    idx2=itemp
                                endif
                                ediff=abs(ediff) 
                                   
                                bessel_idx1=[bessel_idx1,idx1]
                                bessel_idx2=[bessel_idx2,idx2]
                                bessel_type=[bessel_type,type]
                                bessel_mpol=[bessel_mpol,mupole_temp]
                                bessel_kidx=[bessel_kidx,kidx]                                       
                                kidx=kidx+1
                        endif
                endif
        end
        
        free_lun,lun
        
        bessel_idx1=bessel_idx1[1:(size(bessel_idx1))[1]-1]
        bessel_idx2=bessel_idx2[1:(size(bessel_idx2))[1]-1]
        bessel_type=bessel_type[1:(size(bessel_type))[1]-1]
        bessel_mpol=bessel_mpol[1:(size(bessel_mpol))[1]-1]
        bessel_kidx=bessel_kidx[1:(size(bessel_kidx))[1]-1]
        

        
        printed=intarr((size(occup))[2],(size(occup))[2])
        

        for i=0,(size(occup))[2]-1 do begin
                for j=0,(size(occup))[2]-1 do begin
                        if i ne j and printed[i,j] eq 0 and printed[j,i] eq 0  then begin
                                avalue=1e-30
                                
                                ; do we have an a-value?
                                dataidx=where(rad_idx1 eq i and rad_idx2 eq j)
                                if (dataidx[0] ne -1) then begin
                                        ;print,'a-value for ',strtrim(string(i),2),'-',strtrim(string(j),2),' = ',rad_avalue[dataidx[0]]
                                        avalue=rad_avalue[dataidx[0]]
                                endif

                                collstr=dblarr((size(xegrid_calc))[1])
                                effcollstr=dblarr((size(tegrid))[1])
                                collstr[*]=1e-40
                                effcollstr[*]=1e-40
                                
                                collstrj=collstr
                                effcollstrj=effcollstr

; ------------------------------------------------------------------------------------------------------------------
;  check for repetitions of the same transition data for i -> j and j -> i.  Set up warning and correcting divisor        
; ------------------------------------------------------------------------------------------------------------------
                                         for kj=0,(size(bessel_types))[1]-1 do begin
                                
                                    collstrj[*]=0.0d0
                                    effcollstrj[*]=0.0d0
                                    
                                    divisor=1.0d0
                                    
                                             vector=where(bessel_idx1 eq i and bessel_idx2 eq j and bessel_type eq bessel_types[kj] and bessel_kidx eq 0)
                                             if vector[0] ne -1 then begin
                                                 sum=total(bessel_mpol[vector[0]:vector[0]+nbigks-1])
                                                 divisor=1.0d0
                                                 if(size(vector))[1] gt 1 then begin
                                                     for kk=1,(size(vector))[1]-1 do begin
                                             
                                                        if total(bessel_mpol[vector[kk]:vector[kk]+nbigks-1]) ne sum then begin
                                                             IF keyword_set(lun_verb) THEN BEGIN
                                                               printf,lun_verb, ' create_ca_adf04.pro: error -  bessel repetitions check sum fault'
                                                             ENDIF
                                                        endif else begin
                                                            divisor = divisor + 1.0d0
                                                        endelse
                                                     end   
                                                 endif
                                             endif
; ------------------------------------------------------------------------------------------------------------------
                                                               
                                    dataidx=where(bessel_idx1 eq i and bessel_idx2 eq j and bessel_type eq bessel_types[kj])
                                    if (dataidx[0] ne -1) then begin
                                            bessel_total=dblarr(nbigks)
                                            temp_mpol=bessel_mpol[dataidx]
                                            temp_kidx=bessel_kidx[dataidx]
                                            
                                            for k=0,nbigks-1 do begin
                                                    bessel_total[k]=total((temp_mpol[where(temp_kidx eq k)])^2d0)
                                            end
                                        
                                           ; Cowan equation 18.145ish
                                         
                                           ;print,'bessel data found for ',strtrim(string(i),2),'-',strtrim(string(j),2),' = ',bessel_total
                                
                                        ediff=energy[i] - energy[j]
                                        stat_upper=stat_wt[j]
                                        stat_lower=stat_wt[i]
                                        energy_upper=(energy[j]-energy[esort[0]]) * 109737.26d0
                                        energy_lower=(energy[i]-energy[esort[0]]) * 109737.26d0
                                        diff=occup[*,i] - occup[*,j]
                                        q1=occup[(where(diff gt 0)),i] 
                                        q2=occup[(where(diff lt 0)),j] 
                                        l1=lval[(where(diff gt 0))]
                                        l2=lval[(where(diff lt 0))]
                                        ediff=abs(ediff)
                                        
                                        egrid=xegrid_calc*ediff


                                        for k=0,(size(egrid))[1]-1 do begin
                                        
                                           if ediff gt 1.0d-7*(z_ion+1.0d0)^2 then begin
                                           
                                                kmin=egrid[k]^(1./2.)-(egrid[k]-ediff)^(1./2.)
                                                kmax=egrid[k]^(1./2.)+(egrid[k]-ediff)^(1./2.)
                                                ; Cowan equation 18.158
                                                
                                                npoints=51
                                                
                                                x_int = alog(kmin) + dindgen(npoints)/float(npoints-1) * (alog(kmax)-alog(kmin))
                                                x_int = exp(x_int)
                                                

                                                xxsple,xin=kvalues,yin=bessel_total,xout=x_int,yout=y_int
                                                
                                                total=y_int[0]+y_int[npoints-1]
                                                for l=1,npoints-2 do begin
                                                        total=total+(3+(-1d0)^(l-1))*y_int[l]
                                                end
                                                
                                                total = total * (alog(x_int[1])-alog(x_int[0])) / 3d0
                                                ; Cowan equation 18.157 (Corrected)
                                                
                                                collstrj[k]=8d0 * total
                                                
                                            endif else begin
                                            
                                                collstrj(k)=0.0d0
                                                
                                            endelse             
                                                
                                        end
 
                                        factor = 0.0
                                        type=-1
                                        
                                        if (where(bessel_type[dataidx] eq 'j0'))[0] ne -1 then begin
                                            type=2
                                            factor=2d0 * q2 * (4d0 * l1 + 3 - q1) / ((4d0 * l1 + 2d0) * (4d0 * l2 + 2d0)) * (2d0*l1+1d0)
                                        endif else begin
                                            if (where(bessel_type[dataidx] eq 'j1'))[0] ne -1 then begin    
                                               type=1
                                               factor=2d0 * q2 * (4d0 * l1 + 3 - q1) / ((4d0 * l1 + 2d0) * (4d0 * l2 + 2d0)) * max([l1,l2])
                                            endif else begin
                                               if (where(bessel_type[dataidx] eq 'j2'))[0] ne -1 then begin
                                                  type=2
;                                                  factor = 2d0 * q2 * (4d0 * l1 + 3 - q1) / ((4d0 * l1 + 2d0) * (4d0 * l2 + 2d0)) *           $
;                                                            max([l1,l2]) * (max([l1,l2]) + 1d0 ) / (4d0 * max([l1,l2]) + 1)
                                                  factor = 2d0 * q2 * (4d0 * l1 + 3 - q1) / ((4d0 * l1 + 2d0) * (4d0 * l2 + 2d0)) *            $
                                                           (2d0*l1+1)*(2d0*l2+1)*wig3j(l1,2.0,l2,0.0,0.0,0.0)^2
                                               endif
                                            endelse
                                        endelse    
                                        
                                        h4mxwl,z0=z0_nuc,iz=z_ion,effz=float(z_ion+1),type=type,aval=avalue,xju=stat_upper,xjl=stat_lower, $
                                        enu=energy_upper,enl=energy_lower,xparam=xegrid_out,omega=collstrj,te=tegrid,upsilon=effcollstrj
                                        
                                        collstrj = collstrj*factor[0]*stat_upper[0]/divisor[0]
                                        effcollstrj = effcollstrj*factor[0]*stat_upper[0]/divisor[0]
                                        
;                                        fmt1 = '(9i3,3f10.5,e10.2)'
;                                        print,(where(esort eq i))[0]+1,i,(where(esort eq j))[0]+1,j,l1,l2,q1,q2,kj,factor,divisor,stat_upper,effcollstrj[0],format=fmt1
                                        
                                        collstr = collstr+ collstrj
                                        effcollstr = effcollstr + effcollstrj

                                    endif
                                
                                end
                                
                                collstr[*]=collstr[*] > 1.0e-30                    
                                effcollstr[*]=effcollstr[*] > 1.0e-30
                                                    
                                indx_1=(where(esort eq i))[0]+1
                                indx_2=(where(esort eq j))[0]+1

                                
                                if indx_1 lt indx_2 then begin
                                        temp=indx_2
                                        indx_2=indx_1
                                        indx_1=temp
                                endif
                                fmt = '(2i5,50e9.2)'
                                if avalue ne 1e-30 or (where(collstr ne 1e-30))[0] ne -1 then begin
                                
                                        str = string(indx_1, indx_2, avalue, collstr[xegrid_print],format=fmt)
                                        b   = byte(str)
                                        i1  = where(b EQ 101b, complement=i2)              
                                        str = string(b[i2])
                                        printf, type1, str
                                        printed[i,j]=1
                                endif
                                
                                if avalue ne 1e-30 or (where(effcollstr ne 1e-30))[0] ne -1 then begin
                                        str = string(indx_1, indx_2, avalue, effcollstr[*],format=fmt)
                                        b   = byte(str)
                                        i1  = where(b EQ 101b, complement=i2)              
                                        str = string(b[i2])
                                        printf, type3, str
                                        printed[i,j]=1
                                endif
                        endif
                end
        end

        printf,type1,-1,format='(I5)'
        printf,type3,-1,format='(I5)'
        printf,type1,-1,-1,format='(2I5)'
        printf,type3,-1,-1,format='(2I5)'
        
        producer=xxuser()
        date = xxdate()
        
        comments=strarr(10)
        comments(0)  = 'C--------------------------------------------------------------------------------'
        comments(1)  = 'C'
        comments(2)  = 'C   Atomic Structure from Cowan Atomic Structure Program'
        comments(3)  = 'C'
        comments(4)  = 'C--------------------------------------------------------------------------------'
        comments(5)  = 'C'
        comments(6)  = 'C   Code     : ADAS8XX_CREATE_CA_ADF04'
        comments(7)  = 'C   Producer : ' + producer
        comments(8)  = 'C   Date     : ' + date
        comments(9)  = 'C--------------------------------------------------------------------------------'
        
        for i= 0,n_elements(comments)-1 do begin
          printf,type1,comments[i]  ;,format='(I5)'
          printf,type3,comments[i]  ;,format='(I5)'
        endfor  
        
        free_lun,type1
        free_lun,type3
        
        adas8xx_check_cowan_charge_state,z0=z0_nuc,z1=z_ion,$
                                        rcn_file=ionname+'_rcn1_out', $
                                        lun_verb=lun_verb
        
        if not keyword_set(keeppass) then begin
                file_delete,ionname+'_rcn1_input_unsorted',ionname+'_rcn1_input',ionname+'_rcn1_driver',$
                            ionname+'_rcn2_input_d',ionname+'_rcn2_input_j',ionname+'_rcn2_driver_d',   $
                            ionname+'_rcn2_driver_j',ionname+'_rcn2_output_d',ionname+'_rcn2_output_j', $
                            ionname+'_d_j',ionname+'_d_d',ionname+'_d2',ionname+'_rcn1_out',/quiet
        end
        
        
        if keyword_set(archive_files) then begin


            cmd= 'mv ' + here + '/' + adf04_t1_file + ' ' + archive_adf04_t1_file
            spawn,cmd,/sh
            cmd= 'cp ' + here + '/' + adf04_t3_file + ' ' + archive_adf04_t3_file
            spawn,cmd,/sh

        endif

end
