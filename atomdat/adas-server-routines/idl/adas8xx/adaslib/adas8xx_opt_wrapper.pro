;-----------------------------------------------------------------------------
;+
;
; NAME     : adas8xx_opt_wrapper
;
; PURPOSE  : This is a wrapper routine for
;            adas8xx_opt_promotions_control. It sets common options.
;            Not part of any "official" version, but I have been using
;            it to simplify inputs
;
; CATEGORY : ADAS
;
; NOTES    :
;  
; USE      : 
;
; INPUT    : 
; (I*4)    z_nuc          : Atomic number of element
; (I*4)    nel            : Number of electrons
; (R*8)    te_ref         : Reference electron temperature
; (R*8)    ne_ref         : Reference electron density
; set only one of these 3, make the others large (e.g. 999999)
; (I*4)    n_levels_target: target calculation size
; (I*4)    n_configs_target: target calculation size
; (I*4)    n_terms_target : target calculation size
; (I*4)    monitor_power  : If set, use line power to decide on best
;                           promotion rule set
; (C* )    file           : adf54 file 
; (C* )    logfile        : file for recording progress in.
; (I*4)    year           : year id for data produced (default=40)
;
;
; OUTPUTS   :
; struct outdata = returned data structure, containing all the promotion
;               rules above (eg. outdata.min_dn_cl) etc for the final
;               ruleset.
;
; CATEGORY:
;       Adas system.
;
; WRITTEN:
;       Adam Foster, University of Strathclyde,08-01-08
;
; MODIFIED:
;       1.1     Adam Foster

; VERSION:
;       1.1   08-01-08
; 
;-
;-----------------------------------------------------------------------------

PRO adas8xx_opt_wrapper, z_nuc            = z_nuc,            $
                         nel              = nel,              $
                         te_ref           = te_ref,           $
                         ne_ref           = ne_ref,           $
                         n_levels_target  = n_levels_target,  $
                         n_terms_target   = n_terms_target,   $
                         n_configs_target = n_configs_target, $
                         monitor_power    = monitor_power,    $
                         file             = file,             $
                         logfile          = logfile, $
                         cowan_scale_factors=cowan_scale_factors,$
                         year             = year, $
                         verbose          = verbose


                         

useadf40power=0

IF not keyword_set(z_nuc) THEN BEGIN
  print, 'z0 must be specified'
  return
ENDIF
IF not keyword_set(nel) THEN BEGIN
  print, 'nel must be specified'
  return
ENDIF

ca_only=1

IF not keyword_set(year) then begin
  print, 'adas8xx_opt_wrapper warning: year not set, using '+$
         '40 as default value)'
  year=40
endif


IF not keyword_set(monitor_power) THEN monitor_power=0

IF keyword_set(monitor_power) THEN BEGIN
  IF not keyword_set(te_ref) THEN BEGIN
    print, 'keyword monitor_power has been set; te_ref must also be set'
    return
  ENDIF
  IF not keyword_set(ne_ref) THEN BEGIN
    print, 'keyword monitor_power has been set; ne_ref must also be set'    
    return
  ENDIF
ENDIF
z_ion=z_nuc-nel
IF not keyword_set(file) THEN  BEGIN
  print, 'You must specify an ADF54 file (keyword: file). Returning'
  return
ENDIF

rulesdata=1
read_adf54,            file       = file,    $
                       fulldata   = rulesdata

read_adf00,z_nuc=z_nuc,z_ion=z_ion,config=config,ionpot=ionpot
index=where(strtrim(config[0],2) eq strtrim(rulesdata.config,2))

no_v_shl   = rulesdata.no_v_shl[index]
max_dn_v1  = rulesdata.max_dn_v1[index]
min_dn_v1  = rulesdata.min_dn_v1[index]
max_dl_v1  = rulesdata.max_dl_v1[index]
min_dl_v1  = rulesdata.min_dl_v1[index]
max_dn_v2  = rulesdata.max_dn_v2[index]
min_dn_v2  = rulesdata.min_dn_v2[index]
max_dl_v2  = rulesdata.max_dl_v2[index]
min_dl_v2  = rulesdata.min_dl_v2[index]
prom_cl    = rulesdata.prom_cl[index]  
max_n_cl   = rulesdata.max_n_cl[index]
min_n_cl   = rulesdata.min_n_cl[index]
max_l_cl   = rulesdata.max_l_cl[index]
min_l_cl   = rulesdata.min_l_cl[index]
max_dn_cl  = rulesdata.max_dn_cl[index]
min_dn_cl  = rulesdata.min_dn_cl[index]
max_dl_cl  = rulesdata.max_dl_cl[index]
min_dl_cl  = rulesdata.min_dl_cl[index]
fill_n_v1  = rulesdata.fill_n_v1[index]
fill_par   = rulesdata.fill_par[index] 
for_tr_sel = rulesdata.for_tr_sel[index]    
last_4f    = rulesdata.last_4f[index]    
grd_cmplx  = rulesdata.grd_cmplx[index]    

rules   = {    no_v_shl   :  no_v_shl   ,$
               max_dn_v1  :  max_dn_v1  ,$
               min_dn_v1  :  min_dn_v1  ,$
               max_dl_v1  :  max_dl_v1  ,$
               min_dl_v1  :  min_dl_v1  ,$
               max_dn_v2  :  max_dn_v2  ,$
               min_dn_v2  :  min_dn_v2  ,$
               max_dl_v2  :  max_dl_v2  ,$
               min_dl_v2  :  min_dl_v2  ,$
               prom_cl    :  prom_cl    ,$
               max_n_cl   :  max_n_cl   ,$
               min_n_cl   :  min_n_cl   ,$
               max_l_cl   :  max_l_cl   ,$
               min_l_cl   :  min_l_cl   ,$
               max_dn_cl  :  max_dn_cl  ,$
               min_dn_cl  :  min_dn_cl  ,$
               max_dl_cl  :  max_dl_cl  ,$
               min_dl_cl  :  min_dl_cl  ,$
               fill_n_v1  :  fill_n_v1  ,$
               fill_par   :  fill_par   ,$
               for_tr_sel :  for_tr_sel ,$
               last_4f    :  last_4f    ,$
               grd_cmplx  :  grd_cmplx  ,$
               ionpot     :  ionpot     ,$
               config     :  config     ,$
               z0         :  z_nuc         }


  plasma={theta:te_ref, $
          theta_noscale:[1], $
          rho:ne_ref, $
          rho_scale:[0], $
          indx_theta:[1], $
          indx_rho:[1],$
          wvlmin:[1.0],$
          wvlmax:[1e4],$
          npix:[128],$
          indx_wvl:[0] }
          

  adas8xx_opt_promotions_control, n_levels_target  = n_levels_target,        $
                          monitor_power    = monitor_power,          $
                          plasma           = plasma,                 $
                          z0_nuc            = z_nuc,                  $
                          z_ion            = z_ion,                  $
                          oldrules         = rules,                  $
                          for_tr_sel       = rules.for_tr_sel,       $
                          outdata          = outdata,                $
                          logfile          = logfile,                $
                          useadf40power    = useadf40power,          $
                          n_configs_target = n_configs_target,       $
                          n_terms_target = n_terms_target,       $
                          cowan_scale_factors=cowan_scale_factors, $
                          year             = year, $
                          verbose          = verbose


; Now quickly re read and re write the data from the adf54 file

read_adf54,            file       = file,    $
                       fulldata   = rulesdata

rulesdata.no_v_shl[index]   =  outdata.no_v_shl   
rulesdata.max_dn_v1[index]  =  outdata.max_dn_v1  
rulesdata.min_dn_v1[index]  =  outdata.min_dn_v1  
rulesdata.max_dl_v1[index]  =  outdata.max_dl_v1  
rulesdata.min_dl_v1[index]  =  outdata.min_dl_v1  
rulesdata.max_dn_v2[index]  =  outdata.max_dn_v2  
rulesdata.min_dn_v2[index]  =  outdata.min_dn_v2  
rulesdata.max_dl_v2[index]  =  outdata.max_dl_v2  
rulesdata.min_dl_v2[index]  =  outdata.min_dl_v2  
rulesdata.prom_cl[index]    =  outdata.prom_cl    
rulesdata.max_n_cl[index]   =  outdata.max_n_cl   
rulesdata.min_n_cl[index]   =  outdata.min_n_cl   
rulesdata.max_l_cl[index]   =  outdata.max_l_cl   
rulesdata.min_l_cl[index]   =  outdata.min_l_cl   
rulesdata.max_dn_cl[index]  =  outdata.max_dn_cl  
rulesdata.min_dn_cl[index]  =  outdata.min_dn_cl  
rulesdata.max_dl_cl[index]  =  outdata.max_dl_cl  
rulesdata.min_dl_cl[index]  =  outdata.min_dl_cl  
rulesdata.fill_n_v1[index]  =  outdata.fill_n_v1  
rulesdata.fill_par[index]   =  outdata.fill_par   
rulesdata.for_tr_sel[index] =  outdata.for_tr_sel 
rulesdata.last_4f[index]    =  outdata.last_4f    
rulesdata.grd_cmplx[index]  =  outdata.grd_cmplx  
write_adf54, fulldata=rulesdata, outfile=file

print, 'NEW PROMOTION RULES FOR STAGE WRITTEN SUCCESSFULLY TO:'
print, file
END
