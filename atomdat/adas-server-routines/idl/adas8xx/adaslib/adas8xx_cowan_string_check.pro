;+
; PROJECT : ADAS 
;
; NAME    : adas8xx_cowan_string_check()
;
; PURPOSE : Assess if cowan configuration string has more than eight explicit
;           shells.  Attempt to correct by moving to higher rare gas core 
;           reference for izc and omitting associated shells which are closed.
;
; EXPLANATION:
;       The routine takes the Cowan configuration string and returns a new 
;       permitted string with adjusted izc.
;
; INPUTS:
;       cowan_string :  configuration string in Cowan format
;       iz0          :  nuclear charge
;       izc          :  ion charge charge for rare gas core (-ve sign)
;
; OPTIONAL INPUTS:
;       lun_verb     : logical unit number for diagnostic information
;
; OUTPUTS:
;       function      : revised configuration string
;       ifail         : 0 if string OK or correctable
;       izc           : revised izc
;
; OPTIONAL OUTPUTS:
;       None.
;
; KEYWORD PARAMETERS:
;       None.
;
; CALLS:
;       None.
;
; SIDE EFFECTS:
;       None.
;
; CATEGORY:
;       Series 8 utility.
;
; WRITTEN:
;       Hugh Summers
;
; MODIFIED:
;       Version 1.1   Hugh Summers    04-12-2006
;                     First release.
;
;       Version 1.2   Adam Foster     25-03-2009
;                     Added lun_verb keyword to put diagnostic
;                     output in a file.
;
; VERSION:
;       1.1     04-12-2006
;-
;-----------------------------------------------------------------------------
function adas8xx_cowan_string_check,cowan_string,iz0,izc,ifail,$
                                    lun_verb=lun_verb

   nrg_el_cnt  = [   2,  10,  18,  36,  54,  86]

   nshl_check  = [1,2,2,3,3,4]
   shl_check   = [['1s2  ','     ','     ','     '],     $
                  ['2s2  ','2p6  ','     ','     '],     $
                  ['3s2  ','3p6  ','     ','     '],     $
                  ['3d10 ','4s2  ','4p6  ','     '],     $
                  ['4d10 ','5s2  ','5p5  ','     '],     $
                  ['4f14 ','5d10 ','6s2  ','6p6  ']]

   cowan_string_max = 8*4+8
   
   ifail=0
   new_cowan_string=cowan_string
   
      if strlen(new_cowan_string) gt cowan_string_max then begin
      
          if izc lt 0 then begin
          
             i0=where(nrg_el_cnt eq iz0+izc)
             if i0[0] gt -1 then begin
                 i1=i0[0]+1
               nshel=nshl_check[i1]
               for k=0,nshel-1 do begin
                  expression=shl_check[k,i1]
                  new_cowan_string=strjoin(strsplit(new_cowan_string,expression,/regex,/extract,/preserve_null),'')
               endfor
               if strlen(new_cowan_string) gt cowan_string_max then begin
                   ifail = 1
               endif else begin
                   ifail=0
                   izc=-iz0+ nrg_el_cnt[i1]
                       if keyword_set(lun_verb) then begin
                         printf, lun_verb,'**** cowan maximum active shell overflow - allow configuration correction'
                       endif else begin
                         print,'**** cowan maximum active shell overflow - allow configuration correction'
                       endelse
               endelse
               
            endif            
              
          endif else begin
          
             ifail = 1
             if keyword_set(lun_verb) then begin
               printf, lun_verb,'**** cowan maximum active shell overflow - delete configuration'
             endif else begin
               print,'**** cowan maximum active shell overflow - delete configuration'
             endelse
          
          endelse
          
      endif   

      return,new_cowan_string


end
