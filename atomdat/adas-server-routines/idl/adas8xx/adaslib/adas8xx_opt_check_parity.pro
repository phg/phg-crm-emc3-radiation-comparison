;-----------------------------------------------------------------------------
;+
;
; NAME     : adas8xx_opt_check_parity
;
; PURPOSE  : To check the parities present in a given configuration
;            set. Used to check if a diploe transition is present.
;
; CATEGORY : ADAS
;
; NOTES    :
;  
; USE      : 
;
; INPUT    :
; (C* ) configs[]        = array of configuration strings in 
;                          standard form (eg 1s2  2s2  2p1)
; OUTPUT   :
; (I*4) parity           = array of the parities of each configuration
; (I*4) dual             = 1 if configurations of both parities are
;                          present, otherwise 0.
;
; CATEGORY:
;       Adas system.
;
; WRITTEN:
;       Adam Foster, University of Strathclyde,08-01-08
;
; MODIFIED:
;       1.1     Adam Foster

; VERSION:
;       1.1   08-01-08
; 
;-
;-----------------------------------------------------------------------------

FUNCTION adas8xx_opt_check_parity, configs, lun_verb=lun_verb

; Code to check the parity of each configuration and then return the
; values in a structure. Also returns "dual", which is 1 if 
; configurations of both parities are present, otherwise 0.


nconfigs=n_elements(configs)

bad=WHERE(configs NE 'NULL')
if bad[0] EQ -1 THEN BEGIN
  nconfigs=1
endif

parity=intarr(n_elements(configs))

FOR iconfig=0, nconfigs-1 DO BEGIN
  tmp=STRSPLIT(configs[iconfig],' ',/EXTRACT)
  
  IF (WHERE(tmp EQ 'NULL'))[0] NE -1 THEN BEGIN
    if keyword_set(lun_verb) then begin
      printf, lun_verb, 'Null string detected, number',iconfig
    endif else begin
      print, 'Null string detected, number',iconfig
    endelse
    parity[iconfig]=-1  
  ENDIF ELSE BEGIN
  
    nel= intarr(n_elements(tmp))
    l  = intarr(n_elements(tmp))
    nshells=n_elements(tmp)
    sumparity =0
    
    
    FOR ish=0, nshells-1 DO BEGIN
       
      ltmp=(strmid(tmp[ish],1,1))
      CASE ltmp OF
        's':tmpparity=0
        'p':tmpparity=1
        'd':tmpparity=0
        'f':tmpparity=1
        'g':tmpparity=0
        'h':tmpparity=1
        'i':tmpparity=0
        'k':tmpparity=1
      ENDCASE

      nel=FIX(strmid(tmp[ish],2)) 
      sumparity= sumparity + (nel*tmpparity)


    ENDFOR
    IF ((sumparity MOD 2) EQ 1) THEN parity[iconfig]=1 ELSE $
                                     parity[iconfig]=0
  ENDELSE

ENDFOR

IF (((WHERE(parity EQ 0))[0] NE -1) AND ((WHERE(parity EQ 1))[0] NE -1)) $
  THEN dual = 1 ELSE dual =0
  
IF ((WHERE(parity EQ -1))[0] NE -1) THEN dual = 0

ret={dual: dual, $
     parity: parity}

return, ret


END
