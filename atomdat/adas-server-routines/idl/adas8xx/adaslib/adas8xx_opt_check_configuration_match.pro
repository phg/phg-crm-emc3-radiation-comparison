;-----------------------------------------------------------------------
;+
;
; NAME     : adas8xx_opt_check_configuration_match
;
; PURPOSE  : analyse the input configuration string array and compare 
;            with other arrays of configuration strings from pointer to 
;            find a complete matching set if one exists.
;
; CATEGORY : ADAS
;
; NOTES    :
;  
; USE      : 
;
; INPUT    :
;
;    (C* )  config          = array of configuration strings to test
; PTR(C* )  config_ptr      = pointer to stored arrays of configuration 
;                             strings to check for match
;
; RETURNS :
;   (I*4 )  : index of matching configuration, -1 if no match
;
; CATEGORY:
;       Adas system.
;
; WRITTEN:
;       Adam Foster, University of Strathclyde, 08-01-08
;
; MODIFIED:
;       1.1     Adam Foster

; VERSION:
;       1.1   08-01-08
; 
;-
;-----------------------------------------------------------------------


FUNCTION adas8xx_opt_check_configuration_match, config, config_ptr

ret=-1

FOR i=0, n_elements(config_ptr)-1 DO BEGIN

  IF n_elements(config) EQ n_elements( (*config_ptr[i]) ) THEN BEGIN
    
    trackerold=intarr(n_elements(config))
    trackernew=intarr(n_elements(config))
    
    FOR j=0, n_elements(config)-1 DO BEGIN
      FOR k=0, n_elements(config)-1 DO BEGIN
      
        IF config[j] EQ (*config_ptr[i])[k] THEN BEGIN
          trackerold[k]=1
          trackernew[j]=1
          BREAK
        ENDIF  
      ENDFOR
    
    ENDFOR
    
    IF TOTAL(trackernew) EQ n_elements(trackerold) AND $
       TOTAL(trackernew) EQ TOTAL(trackerold) THEN BEGIN
      ret=i
      BREAK
    ENDIF
  
  ENDIF
  
  

ENDFOR

RETURN, ret
END
