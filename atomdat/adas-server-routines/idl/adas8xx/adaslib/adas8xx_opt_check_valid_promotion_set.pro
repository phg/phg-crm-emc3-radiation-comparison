;-----------------------------------------------------------------------------
;+
;
; NAME     : adas8xx_opt_check_valid_promotion_set
;
; PURPOSE  : Checks that input configuration set is unique and
;            valid for a Cowan calculation.
;
; CATEGORY : ADAS
;
; NOTES    :
;  
; USE      : 
;
; INPUT    :
; max_configurations : maximum number of configurations allowed
; monitor_power      : is PLT being generated (should be 'on')
; z0_nuc             : nuclear charge
; nel                : number of electrons
; z_ion              : ion charge
; ionpot             : ionisation potential
; for_tr_sel         : Cowan option for radiative transitions 
;                      (default=3)
;(STRUCT) promotion_results = details of configurations to include:
;         grd_cfg    = ground configuration
;         grd_occ    = ground occupancy array
;         ex_cfg     = excited state configurations
;         grd_par    = ground state parity
;         ex_par     = excited state parity
;         grd_zc_cow = ground state charge for cowan code
;         ex_zc_cow  = excited state charge for cowan code
;         oc_store   = excited state occupancy arrays
;         no_configs = number of configurations in configuration set
;         no_terms   = number of terms in configuration set
;         no_levels  = number of levels in configuration set
;(STRUCT) plasma     = plasma conditions.
;         THETA        : array of electron temperatures (eV)
;         INDX_THETA   : array of indices of theta to select Te
;         RHO          : array of electron density (cm-3)
;         INDX_RHO     : array of indices of rho to select Ne
;         NPIX         : array of pixels per wavelength region
;         WVLMIN       : lower bounds of wavelength region(s)
;         WVLMAX       : upper bounds of wavelength region(s)
;         INDX_WVL     : indicies of wavelength regions to use
;         THETA_NOSCALE: if 0, theta is assumed to be reduced Te
;         RHO_SCALE    : if 1, rho is assumed to be reduced Ne
; configs            : list of old configuration sets to check for 
;                      matches (saves time redoing calculations)
; old_promotion_ruleset: if 1, ignore first configuration in configs
;                        when searching for a match
; cowan_scale_factors: Slater parameter multipliers for Cowan 
;                     code (optional, defaults are usually sufficient)
; 
; OUTPUTS   :
; valid_rules        : 0 if configuration set is ok, -1 otherwise.
;(STRUCT)fpecdata    : returned from adas8xx_opt_get_total_line_power
;       a11_total_power : total line power from plt file
;       a40_total_power : total line power from adf40 power
;       fpec            : Feature photon emissivity coefficient
;       wave            : wavelengths for each pixel of fpec
; exitstatus         :  < 0 if an error is found.
; total_power        : array holding the total power calculated in
;                      adas8xx_opt_get_total_line_power. The power
;                      calculated from this configuration set is
;                      appended to the end. (0 is added if run fails)
; total_adf40_power  : See total power, but calculated by adding up
;                      contents of FPEC file
; total_adf11_power  : See total power, but obtained from PLT file
;                      (by default this is also whatis in total_power)
; sum_configs        : total number of configurations will be appended
;                      to this array
; sum_terms          : total number of terms will be appended
;                      to this array
; sum_levels         : total number of levels will be appended
;                      to this array


; CALLS     :
; adas8xx_opt_get_total_line_power
; adas8xx_opt_check_configuration_match
;
; CATEGORY:
;       Adas system.
;
; WRITTEN:
;       Adam Foster
;
; MODIFIED:
;       1.1     Adam Foster
;
; VERSION:
;       1.1   20-03-09
; 
;-
;-----------------------------------------------------------------------------


PRO adas8xx_opt_check_valid_promotion_set, $
                        max_configurations = max_configurations, $
                        monitor_power      = monitor_power, $
                        valid_rules        = valid_rules, $
                        z0_nuc             = z0_nuc, $
                        nel                = nel,    $
                        z_ion              = z_ion,    $
                        ionpot             = ionpot,    $
                        for_tr_sel         = for_tr_sel,  $
                        promotion_results  = promotion_results, $
                        plasma             = plasma,  $
                        fpecdata           = fpecdata,   $
                        configs            = configs, $
                        exitstatus         = exitstatus,   $
                        total_power        = total_power, $
                        total_adf40_power  = total_adf40_power, $
                        total_adf11_power  = total_adf11_power, $
                        sum_configs        = sum_configs, $
                        sum_terms        = sum_terms, $
                        sum_levels         = sum_levels, $
                        old_promotion_ruleset=old_promotion_ruleset, $
                        cowan_scale_factors=cowan_scale_factors, $
                        year               = year, $
                        lun_verb           = lun_verb
   

    IF total(promotion_results.no_configs) GE max_configurations THEN BEGIN
    done=0
    if keyword_set(lun_verb) then begin
      printf, lun_verb, 'invalid rule set: too many configurations'
    endif
    valid_rules=-1
    done=1

  ENDIF ELSE IF total(promotion_results.no_configs) LT 2 THEN BEGIN
    if keyword_set(lun_verb) then begin
      printf, lun_verb, 'Only 1 configuration - setting power to 0'
    endif
    IF KEYWORD_SET(monitor_power) THEN BEGIN

    ENDIF      
    valid_rules=-1


    done=1

  ENDIF ELSE IF total(promotion_results.no_levels) LE 2 THEN BEGIN
    if keyword_set(lun_verb) then begin
      printf, lun_verb, '< 2 levels - setting power to 0'
    endif
    valid_rules=-1


    done=1

  ENDIF ELSE IF (adas8xx_opt_check_parity([promotion_Results.grd_cfg, $
                              promotion_results.ex_cfg])).dual EQ 0 $
    THEN BEGIN
    
    if keyword_set(lun_verb) then begin
      printf, lun_verb, 'Only 1 parity present - not counting level'
    endif
    valid_rules =-1
    done =1
    
  ENDIF ELSE BEGIN
;-----------------------------------------------------------------------
;    If all the other checks have been passed, this level is ok.
;-----------------------------------------------------------------------  
    valid_rules=0
    if keyword_set(lun_verb) then begin
      printf, lun_verb, 'All checks passed - this ruleset proceeds to power monitoring (if'+$
             ' selected)'
    endif
    done=1
  ENDELSE

 

  IF KEYWORD_SET(monitor_power) THEN BEGIN
;-----------------------------------------------------------------------
;    Check to see if this matches any other configuration set. If so we 
; can just copy the results instead of running the calculation again.
; Also, exclude the first set if this is just the reference case.
;-----------------------------------------------------------------------  
    IF keyword_set(old_promotion_ruleset) THEN BEGIN
      IF n_elements(configs) GT 2 THEN BEGIN
        tmp=adas8xx_opt_check_configuration_match(promotion_results.ex_cfg,configs[1:n_elements(configs)-2])
      ENDIF ELSE BEGIN
        tmp=-1
      ENDELSE
    ENDIF ELSE BEGIN      
      tmp=adas8xx_opt_check_configuration_match(promotion_results.ex_cfg,configs[0:n_elements(configs)-2])    
    ENDELSE
    IF tmp EQ -1 THEN BEGIN


      IF (valid_rules GE 0) THEN BEGIN
      adas8xx_opt_get_total_line_power, z0_nuc     = z0_nuc,      $
                                     nel           = nel,         $
                                     z_ion         = z_ion,       $
                                     ionpot        = ionpot,      $
                                     for_tr_sel    = for_tr_sel,  $
                                   promotion_results=promotion_results,$
                                     plasma        = plasma,            $
                                     fpecdata      = fpecdata, $
                               cowan_scale_factors=cowan_scale_factors,$
                                     year          = year, $
                                     lun_verb      = lun_verb

      ENDIF
    ENDIF ELSE BEGIN
      IF keyword_set(old_promotion_ruleset) THEN tmp=tmp+1
      if keyword_set(lun_verb) then begin
        printf, lun_verb, 'THIS IS A REPEAT: copying results for config number '+$
                strcompress(string(tmp),/remove_all)
      endif

      fpecdata={a40_total_power: total_adf40_power[tmp], $
                a11_total_power: total_adf11_power[tmp], $
                exit_status:exitstatus[tmp]}
    ENDELSE
  
  ENDIF

  IF valid_rules NE -1 THEN BEGIN
    IF keyword_set(monitor_power) THEN BEGIN
      IF keyword_set(useadf40power) THEN $
          total_power=[total_power,fpecdata.a40_total_power] ELSE $
          total_power=[total_power,fpecdata.a11_total_power]

      total_adf11_power=[total_adf11_power,fpecdata.a11_total_power]
      total_adf40_power=[total_adf40_power,fpecdata.a40_total_power]
    ENDIF

    exitstatus=[exitstatus, fpecdata.exit_status]
    sum_configs=[sum_configs,total(promotion_results.no_configs)]
    sum_terms=[sum_terms,total(promotion_results.no_terms)]
    sum_levels=[sum_levels, TOTAL(promotion_results.no_levels)]
  ENDIF ELSE BEGIN
  
    IF keyword_set(monitor_power) THEN BEGIN
      total_power=[total_power,0.0]

      total_adf11_power=[total_adf11_power,0.0]
      total_adf40_power=[total_adf40_power,0.0]
    ENDIF

    exitstatus=[exitstatus, -1]
    sum_configs=[sum_configs,total(promotion_results.no_configs)]
    sum_terms=[sum_terms,total(promotion_results.no_terms)]
    sum_levels=[sum_levels,total(promotion_results.no_levels)]
  ENDELSE    

END
