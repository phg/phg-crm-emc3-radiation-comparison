;-----------------------------------------------------------------------------
;+
;
; NAME     : adas8xx_promotion_rules
;
; PURPOSE  : To set the default promotional rules for configuration generation  
;            of the complete set of ions of an element.
;
; CATEGORY : ADAS
;
; NOTES    :
;  
; USE      : Usually called before the promotional routine 'default_promotions'
;
; INPUT    :
;
;   (I*4)   z0_nuc           = keyword = nuclear charge
;   (I*4)   z_ion            = keyword = ion charge(s)
;   (C* )   a54file          = keyword = Adf54 files to read to obtain promotion rules.
;
; OUTPUTS   :
;   (C* )   config[]        = keyword = ground configuration for each ion of element
;                                         1st dim: ion charge 0 --> z0_nuc-1    
;   (R*8)   ionpot[]        = keyword = ionis. potential (ev) for each ion of element
;                                         1st dim: ion charge 0 --> z0_nuc-1    

;   (STRUCT) prom_rules:
;
;     (I*4)   no_v_shl[]       = number of open (valence) shells. Include 
;                                outer-most shell even if closed.
;                                1st dim: ion charge 0 --> z0_nuc-1    
;     (I*4)   max_dn_v1[]      = maximum delta n promotion for first (outer-
;                                most valence shell
;                                1st dim: ion charge 0 --> z0_nuc-1    
;     (I*4)   min_dn_v1[]      = minimum delta n promotion for first (outer-
;                                most valence shell. Negative value allows access  
;                                to inner unoccupied or open shells
;                                1st dim: ion charge 0 --> z0_nuc-1    
;     (I*4)   max_dl_v1[]      = maximum delta l promotion for first (outer-
;                                most valence shell
;                                1st dim: ion charge 0 --> z0_nuc-1    
;     (I*4)   min_dl_v1[]      = minimum delta l promotion for first (outer-
;                                most valence shell. 
;                                1st dim: ion charge 0 --> z0_nuc-1    
;     (I*4)   max_dn_v2[]      = maximum delta n promotion for second (inner-
;                                most valence shell
;                                1st dim: ion charge 0 --> z0_nuc-1    
;     (I*4)   min_dn_v2[]      = minimum delta n promotion for second (inner-
;                                most valence shell. Negative value allows access  
;                                to inner unoccupied or open shells
;                                1st dim: ion charge 0 --> z0_nuc-1    
;     (I*4)   max_dl_v2[]      = maximum delta l promotion for second (inner-
;                                most valence shell
;                                1st dim: ion charge 0 --> z0_nuc-1    
;     (I*4)   min_dl_v2[]      = minimum delta l promotion for second (inner-
;                                most valence shell. 
;                                1st dim: ion charge 0 --> z0_nuc-1    
;     (I*4)   prom_cl[]        = promote from inner shell closed shells (1=yes,0=no)
;                                 1st dim: ion charge 0 --> z0_nuc-1    
;     (I*4)   max_n_cl[]       = maximum inner shell n for promotion from 
;                                1st dim: ion charge 0 --> z0_nuc-1    
;     (I*4)   min_n_cl[]       = minimum inner shell n for promotion from
;                                1st dim: ion charge 0 --> z0_nuc-1    
;     (I*4)   max_l_cl[]       = maximum inner shell l for promotion from
;                                1st dim: ion charge 0 --> z0_nuc-1    
;     (I*4)   min_l_cl[]       = minimum inner shell l for promotion from 
;                                1st dim: ion charge 0 --> z0_nuc-1    
;     (I*4)   max_dn_cl[]      = maximum delta n promotion for inner shell to
;                                1st dim: ion charge 0 --> z0_nuc-1    
;     (I*4)   min_dn_cl[]      = minimum delta n promotion for inner shell to
;                                Negative value allows access  
;                                to inner unoccupied or open shells
;                                1st dim: ion charge 0 --> z0_nuc-1    
;     (I*4)   max_dl_cl[]      = maximum delta l promotion for inner shell to
;                                1st dim: ion charge 0 --> z0_nuc-1    
;     (I*4)   min_dl_cl[]      = minimum delta l promotion for inner shell to 
;                                1st dim: ion charge 0 --> z0_nuc-1    
;     (I*4)   fill_n_v1[]      = add all nl configurations of outer valence
;                                shell n (1=yes,0=no)
;                                1st dim: ion charge 0 --> z0_nuc-1    
;     (I*4)   fill_par[]       = if n_fill only add opposite parity to valence
;                                shell else add both parities (1=yes, 0=n0)
;                                1st dim: ion charge 0 --> z0_nuc-1    
;     (I*4)   for_tr_sel[]     = Cowan option for radiative transitions 1 - first parity, 2 or 3(default)
;                                1st dim: ion charge 0 --> z0_nuc-1    
;     (I*4)   last_4f[]        = shift an electron valence shell to unfilled 4f as extra ground                                
;                                1st dim: ion charge 0 --> z0_nuc-1    
;     (I*4)   grd_cmplx[]      = include configurations of same complex as ground configuation for valence n-shell                                
;                                1st dim: ion charge 0 --> z0_nuc-1    
;
; ROUTINES:
;            NAME            TYPE      COMMENT
;            read_adf00      ADAS      reads an adf00 file
;
; CATEGORY:
;       Adas system.
;
; WRITTEN:
;       H. P. Summers, University of Strathclyde,29-06-06.
;
; MODIFIED:
;       1.1     H. P. Summers
;               First version put into CVS.
;
;       1.2     A. R. Foster
;               Major rewrite: now calls read_adf54 to obtain promotion rules instead of using
;               hardcoded versions. Also return data in prom_rules structure.
;               Now uses z0_nuc and z_ion instead of z0, z1 inputs.
;
; VERSION:
;       1.1   25-08-06
;       1.2   23-03-09
;
;-
;-----------------------------------------------------------------------------

PRO adas8xx_promotion_rules, z0_nuc      = z0_nuc,                           $
                             z_ion      = z_ion,                             $
                             config     = config,                            $
                             ionpot     = ionpot,                            $
                             prom_rules = prom_rules,                        $
                             a54file    = a54file
;-----------------------------------------------------------------------------
; initial checks
;-----------------------------------------------------------------------------

z0_nuc_max = 86

if z0_nuc gt z0_nuc_max or z0_nuc lt 1 then begin

    print,'*** adas8xx_promotion_rules: error:- z0_nuc = ',z0_nuc,' out of range ***'
    stop
    
endif    

;-----------------------------------------------------------------------------------------
; Read in reference data from adf54
;-----------------------------------------------------------------------------------------

read_adf54, file=a54file, fulldata=fd


;-----------------------------------------------------------------------------------------
; get configurations and ionisation potentials (cm-1) of required element from adf00 file
;-----------------------------------------------------------------------------------------

read_adf00,z_nuc=z0_nuc,z1=z,config=config,ionpot=ionpot,/all

;ionpot = ionpot*8066.0

;---------------------------------------------------------------
; assemble set of rules for element
;---------------------------------------------------------------

index=intarr(z0_nuc)
for i=0,z0_nuc-1 do index(i)=where(strtrim(fd.config,2) eq strtrim(config[i],2))

;print,'index=',index

    no_v_shl   = fd.no_v_shl[index]
    max_dn_v1  = fd.max_dn_v1[index]
    min_dn_v1  = fd.min_dn_v1[index]
    max_dl_v1  = fd.max_dl_v1[index]
    min_dl_v1  = fd.min_dl_v1[index]
    max_dn_v2  = fd.max_dn_v2[index]
    min_dn_v2  = fd.min_dn_v2[index]
    max_dl_v2  = fd.max_dl_v2[index]
    min_dl_v2  = fd.min_dl_v2[index]
    prom_cl    = fd.prom_cl[index]  
    max_n_cl   = fd.max_n_cl[index]
    min_n_cl   = fd.min_n_cl[index]
    max_l_cl   = fd.max_l_cl[index]
    min_l_cl   = fd.min_l_cl[index]
    max_dn_cl  = fd.max_dn_cl[index]
    min_dn_cl  = fd.min_dn_cl[index]
    max_dl_cl  = fd.max_dl_cl[index]
    min_dl_cl  = fd.min_dl_cl[index]
    fill_n_v1  = fd.fill_n_v1[index]
    fill_par   = fd.fill_par[index] 
    for_tr_sel = fd.for_tr_sel[index]    
    last_4f    = fd.last_4f[index]    
    grd_cmplx  = fd.grd_cmplx[index]    


; pick out individual stages if requested
if n_elements(z_ion) GE 1 then begin
  tmpindex = z_ion
  index      = index[tmpindex]
  no_v_shl   = no_v_shl[tmpindex]
  max_dn_v1  = max_dn_v1[tmpindex]
  min_dn_v1  = min_dn_v1[tmpindex]
  max_dl_v1  = max_dl_v1[tmpindex]
  min_dl_v1  = min_dl_v1[tmpindex]
  max_dn_v2  = max_dn_v2[tmpindex]
  min_dn_v2  = min_dn_v2[tmpindex]
  max_dl_v2  = max_dl_v2[tmpindex]
  min_dl_v2  = min_dl_v2[tmpindex]
  prom_cl    = prom_cl[tmpindex]
  max_n_cl   = max_n_cl[tmpindex]
  min_n_cl   = min_n_cl[tmpindex]
  max_l_cl   = max_l_cl[tmpindex]
  min_l_cl   = min_l_cl[tmpindex]
  max_dn_cl  = max_dn_cl[tmpindex]
  min_dn_cl  = min_dn_cl[tmpindex]
  max_dl_cl  = max_dl_cl[tmpindex]
  min_dl_cl  = min_dl_cl[tmpindex]
  fill_n_v1  = fill_n_v1[tmpindex]
  fill_par   = fill_par[tmpindex]
  for_tr_sel = for_tr_sel[tmpindex]
  last_4f    = last_4f[tmpindex]
  grd_cmplx  = grd_cmplx[tmpindex]
  
  ionpot     = ionpot[tmpindex]
  config     = config[tmpindex]
endif
    prom_rules={config     : config     ,$
                index      : index      ,$
                no_v_shl   : no_v_shl   ,$ 
                max_dn_v1  : max_dn_v1  ,$ 
                min_dn_v1  : min_dn_v1  ,$ 
                max_dl_v1  : max_dl_v1  ,$ 
                min_dl_v1  : min_dl_v1  ,$ 
                max_dn_v2  : max_dn_v2  ,$ 
                min_dn_v2  : min_dn_v2  ,$ 
                max_dl_v2  : max_dl_v2  ,$ 
                min_dl_v2  : min_dl_v2  ,$ 
                prom_cl    : prom_cl    ,$ 
                max_n_cl   : max_n_cl   ,$ 
                min_n_cl   : min_n_cl   ,$ 
                max_l_cl   : max_l_cl   ,$ 
                min_l_cl   : min_l_cl   ,$ 
                max_dn_cl  : max_dn_cl  ,$ 
                min_dn_cl  : min_dn_cl  ,$ 
                max_dl_cl  : max_dl_cl  ,$ 
                min_dl_cl  : min_dl_cl  ,$ 
                fill_n_v1  : fill_n_v1  ,$ 
                fill_par   : fill_par   ,$ 
                for_tr_sel : for_tr_sel ,$
                last_4f    : last_4f    ,$
                grd_cmplx  : grd_cmplx}

END
