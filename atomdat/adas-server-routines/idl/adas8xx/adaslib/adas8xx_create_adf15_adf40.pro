;-----------------------------------------------------------------------------
;+
;
; NAME     : adas8xx_create_adf15_adf40
;
; PURPOSE  : runs adas810 for an ion.
;
; CATEGORY : ADAS
;
; NOTES    :
;  
; USE      : 
;
; INPUT    :
;(I*4) z0_nuc        = nuclear charge of ion
;(I*4) z_ion         = ion charge
;(keyword) ca_only   = run only configuration averaged
;(keyword) donotrun  = generate script file but do not run 810.
;(C* ) archive_dir   = directory to archive results in 
;                   (use current directory if not specified)
;(C* ) archive_files = store files in various subdirectories of
;                      archive_dir if set.
;(STRUCT) files = structure storing file names. Required names are:
;                   adf42_ic_file
;                   adf42_ic_pp_file
;                   adf04_ic_file
;                   adf15_ic_file
;                   adf40_ic_file   
;                   adf11_ic_file
;                   adf42_ca_file
;                   adf42_ca_pp_file
;                   adf04_ca_file
;                   adf15_ca_file
;                   adf40_ca_file
;                   adf11_ca_file
;                   adf42_ls_file
;                   adf42_ls_pp_file
;                   adf04_ls_file
;                   adf15_ls_file
;                   adf40_ls_file
;                   adf11_ls_file
;                 note that if ca_only is set then the ic & ls files
;                 need not be supplied. ADF04 & ADF42 files must 
;                 already exist, the others are files which will be 
;                 generated.
; 
; OUTPUTS   :
;
; CATEGORY:
;       Adas system.
;
; WRITTEN:
;       Adam Foster
;
; MODIFIED:
;       1.1     Adam Foster 
;              - first commented version
;              - added donotrun keyword
;              - changed inputs to z0_nuc and z_ion from z0,z1
;              - moved list of files into files structure
;              - added ca_only keyword
;
; VERSION:
;       1.1   20-03-09
; 
;-
;-----------------------------------------------------------------------------

PRO adas8xx_create_adf15_adf40, z0_nuc,                                $
                                z_ion,                                 $
                                files             = files,             $
                                archive_dir       = archive_dir,       $
                                archive_files     = archive_files,     $
                                ca_only           = ca_only,           $
                                donotrun          = donotrun


;-----------------------------------------------------
; Get location of ADAS from environment variable
;-----------------------------------------------------
adaspath=getenv('ADASCENT')+'/../'
;-----------------------------------------------------
; prepare the script and submit file names if required
;-----------------------------------------------------

    elsymb      = strlowcase(xxesym(z0_nuc))
    len_elsymb  = strlen(elsymb)
    
    if ca_only eq 0 then begin
    
          if keyword_set(files.adf42_ls_file) and                                  $
             keyword_set(files.adf42_ls_pp_file) and                               $
             keyword_set(files.adf04_ls_file) then begin                           $
              script_ls_file = strcompress(elsymb + string(z_ion,format='(i2)') $
                               + '_ls_810_script', /remove_all)
                              
          endif         

          if keyword_set(files.adf42_ic_file) and                                  $
             keyword_set(files.adf42_ic_pp_file) and                               $
             keyword_set(files.adf04_ic_file) then begin                           $
              script_ic_file = strcompress(elsymb +                          $
                               string(z_ion,format='(i2)') +                    $
                               '_ic_810_script', /remove_all)
                        
          endif                            
          
    endif         
                  
    if keyword_set(files.adf42_ca_file) and                                  $
       keyword_set(files.adf42_ca_pp_file) and                               $
       keyword_set(files.adf04_ca_file) then begin                           $
        script_ca_file = strcompress(elsymb +                          $
                         string(z_ion,format='(i2)') +                    $
                         '_ca_810_script', /remove_all)
                  
     endif      

;------------------------------------------------
; prepare for archiving if required
;------------------------------------------------

if keyword_set(archive_files) then begin

    if keyword_set(archiv_dir) then begin
    
        res = file_info(archive_dir)
        if res.exists EQ 1 AND res.directory EQ 0 then message,       $
             'Cannot create ' + archive_dir
        if res.exists EQ 0 then file_mkdir, archive_dir
        
    endif else begin
    
        cmd = 'pwd'
        spawn, cmd, result
        here = result[0]
        archive_dir = here
        
    endelse

;------------------------------------------------
; adf42: only archive if standard file naming 
;------------------------------------------------
    
    adf42_template = 'adf42_.*_.*'
    
    if keyword_set(files.adf42_ls_file) and stregex(files.adf42_ls_file,adf42_template,/fold_case) eq 0 and ca_only eq 0 then begin
        adf42_template = files.adf42_ls_file
    end else if keyword_set(files.adf42_ic_file) and stregex(files.adf42_ic_file,adf42_template,/fold_case) eq 0 and ca_only eq 0 then begin
        adf42_template = files.adf42_ic_file
    end else if keyword_set(files.adf42_ca_file) and stregex(files.adf42_ca_file,adf42_template,/fold_case) eq 0 then begin
        adf42_template = files.adf42_ca_file
    end else begin
        print,'*** create_adf15_40_files:  non-standard adf42 file names - no archiving. ***'
        archive_files = 0
    end        
        
endif
                
if keyword_set(archive_files) then begin
    
    p0=0
    p1=strpos(adf42_template,'_',p0)
    archive_adf42_dir=archive_dir + '/' +                            $
                      strmid(adf42_template,p0,p1-p0)
    res = file_info(archive_adf42_dir)
    if res.exists EQ 1 AND res.directory EQ 0 then message,          $
         'Cannot create ' + archive_adf42_dir
    if res.exists EQ 0 then file_mkdir, archive_adf42_dir
    
    p0=p1+1
    p1=strpos(adf42_template,'_',p0)
    archive_adf42_element_dir=archive_adf42_dir + '/' +              $
                      strmid(adf42_template,p0,p1-p0)
    res = file_info(archive_adf42_element_dir)
    if res.exists EQ 1 AND res.directory EQ 0 then message,          $
         'Cannot create ' + archive_adf42_element_dir
    if res.exists EQ 0 then file_mkdir, archive_adf42_element_dir

    p0=p1+1
    
    if ca_only eq 0 then begin
    
         if keyword_set(files.adf42_ls_file) then begin
             archive_adf42_ls_file=archive_adf42_element_dir + '/' +      $
                                   strmid(files.adf42_ls_file,p0)
         endif                               
         if keyword_set(files.adf42_ic_file) then begin
             archive_adf42_ic_file=archive_adf42_element_dir + '/' +      $
                                   strmid(files.adf42_ic_file,p0)
         endif                                       
         if keyword_set(files.adf42_ca_file) then begin
             archive_adf42_ca_file=archive_adf42_element_dir + '/' +      $
                                   strmid(files.adf42_ca_file,p0)
         endif                                       
         if keyword_set(files.adf42_ls_pp_file) then begin
             archive_adf42_ls_pp_file=archive_adf42_element_dir + '/' +   $
                            strmid(files.adf42_ls_pp_file,p0)
         endif                                       
         if keyword_set(files.adf42_ic_pp_file) then begin
             archive_adf42_ic_pp_file=archive_adf42_element_dir + '/' +   $
                                      strmid(files.adf42_ic_pp_file,p0)
         endif
         
    endif        
                                                     
    if keyword_set(files.adf42_ca_file) then begin
        archive_adf42_ca_file=archive_adf42_element_dir + '/' +      $
                              strmid(files.adf42_ca_file,p0)
    endif                                       
    if keyword_set(files.adf42_ca_pp_file) then begin
        archive_adf42_ca_pp_file=archive_adf42_element_dir + '/' +   $
                       strmid(files.adf42_ca_pp_file,p0)
    endif
                                        
;------------------------------------------------
; adf15: only archive if standard file naming 
;------------------------------------------------
    
    if len_elsymb eq 1 then adf15_template = 'adf15_.*_......._.*'
    if len_elsymb eq 2 then adf15_template = 'adf15_.*_........_.*'
    
    archive_adf15 = 1
    
    if keyword_set(files.adf15_ls_file) and stregex(files.adf15_ls_file,adf15_template,/fold_case) eq 0 and ca_only eq 0 then begin              
        adf15_template = files.adf15_ls_file                                                                                               
    end else if keyword_set(files.adf15_ic_file) and stregex(files.adf15_ic_file,adf15_template,/fold_case) eq 0 and ca_only eq 0 then begin     
        adf15_template = files.adf15_ic_file                                                                                               
    end else if keyword_set(files.adf15_ca_file) and stregex(files.adf15_ca_file,adf15_template,/fold_case) eq 0 then begin
        adf15_template = files.adf15_ca_file
    end else begin
        print,'*** adas8xx_create_adf15_adf40:  no adf15 output file names - no archiving. ***'
        archive_adf15 = 0
    end 

    if archive_adf15  then begin
    
        p0=0
        p1=strpos(adf15_template,'_',p0)
        archive_adf15_dir=archive_dir + '/' +                            $
                          strmid(adf15_template,p0,p1-p0)           
        res = file_info(archive_adf15_dir)
        if res.exists EQ 1 AND res.directory EQ 0 then message,          $
             'Cannot create ' + archive_adf15_dir
        if res.exists EQ 0 then file_mkdir, archive_adf15_dir
        
        p0=p1+1
        p1=strpos(adf15_template,'_',p0)
        archive_adf15_element_dir=archive_adf15_dir + '/' +              $
                          strmid(adf15_template,p0,p1-p0)           
        res = file_info(archive_adf15_element_dir)
        if res.exists EQ 1 AND res.directory EQ 0 then message,          $
             'Cannot create ' + archive_adf15_element_dir
        if res.exists EQ 0 then file_mkdir, archive_adf15_element_dir
        
        p0=p1+1
        
        if ca_only eq 0 then begin
        
             if keyword_set(files.adf15_ls_file) then begin
                 archive_adf15_ls_file=archive_adf15_element_dir +       $
                                       '/' +  strmid(files.adf15_ls_file,p0)
             endif                                       
             if keyword_set(files.adf15_ic_file) then begin
                 archive_adf15_ic_file=archive_adf15_element_dir +       $
                                       '/' +  strmid(files.adf15_ic_file,p0)
             endif
             
        endif     
                                                 
        if keyword_set(files.adf15_ca_file) then begin
            archive_adf15_ca_file=archive_adf15_element_dir +       $
                                  '/' +  strmid(files.adf15_ca_file,p0)
        endif
    endif                                                   
                                        
;------------------------------------------------
; adf40: only archive if standard file naming 
;------------------------------------------------
    
    if len_elsymb eq 1 then adf40_template = 'adf40_.*_........_.*'
    if len_elsymb eq 2 then adf40_template = 'adf40_.*_........._.*'
    
    archive_adf40 = 1
    
    if keyword_set(files.adf40_ls_file) and stregex(files.adf40_ls_file,adf40_template,/fold_case) eq 0 and ca_only eq 0 then begin              
        adf40_template = files.adf40_ls_file                                                                                               
    end else if keyword_set(files.adf40_ic_file) and stregex(files.adf40_ic_file,adf40_template,/fold_case) eq 0 and ca_only eq 0 then begin     
        adf40_template = files.adf40_ic_file                                                                                               
    end else if keyword_set(files.adf40_ca_file) and stregex(files.adf40_ca_file,adf40_template,/fold_case) eq 0 then begin
        adf40_template = files.adf40_ca_file
    end else begin
        print,'*** adas8xx_create_adf15_adf40:  no adf40 output file names - no archiving. ***'
        archive_adf40 = 0
    end 

    if archive_adf40  then begin
    
        p0=0
        p1=strpos(adf40_template,'_',p0)
        archive_adf40_dir=archive_dir + '/' +                            $
                          strmid(adf40_template,p0,p1-p0)           
        res = file_info(archive_adf40_dir)
        if res.exists EQ 1 AND res.directory EQ 0 then message,          $
             'Cannot create ' + archive_adf40_dir
        if res.exists EQ 0 then file_mkdir, archive_adf40_dir
        
        p0=p1+1
        p1=strpos(adf40_template,'_',p0)
        archive_adf40_element_dir=archive_adf40_dir + '/' +              $
                          strmid(adf40_template,p0,p1-p0)           
        res = file_info(archive_adf40_element_dir)
        if res.exists EQ 1 AND res.directory EQ 0 then message,          $
             'Cannot create ' + archive_adf40_element_dir
        if res.exists EQ 0 then file_mkdir, archive_adf40_element_dir
        
        p0=p1+1
        
        if ca_only eq 0 then begin
        
             if keyword_set(files.adf40_ls_file) then begin
                 archive_adf40_ls_file=archive_adf40_element_dir +       $
                                       '/' +  strmid(files.adf40_ls_file,p0)
             endif                                       
             if keyword_set(files.adf40_ic_file) then begin
                 archive_adf40_ic_file=archive_adf40_element_dir +       $
                                       '/' + strmid(files.adf40_ic_file,p0)
             endif
             
        endif     
                                                 
        if keyword_set(files.adf40_ca_file) then begin
            archive_adf40_ca_file=archive_adf40_element_dir +       $
                                  '/' + strmid(files.adf40_ca_file,p0)
        endif                                       
    endif
                                        
;------------------------------------------------
; adf11: only archive if standard file naming 
;------------------------------------------------
    
    if len_elsymb eq 1 then adf11_template = 'adf11_....._.*'
    if len_elsymb eq 2 then adf11_template = 'adf11_....._.*'
    
    archive_adf11 = 1
    
    if keyword_set(files.adf11_ls_file) and stregex(files.adf11_ls_file,adf11_template,/fold_case) eq 0 and ca_only eq 0 then begin             
        adf11_template = files.adf11_ls_file                                                                              
    end else if keyword_set(files.adf11_ic_file) and stregex(files.adf11_ic_file,adf11_template,/fold_case) eq 0 and ca_only eq 0 then begin     
        adf11_template = files.adf11_ic_file                                                                              
    end else if keyword_set(files.adf11_ca_file) and stregex(files.adf11_ca_file,adf11_template,/fold_case) eq 0 then begin
        adf11_template = files.adf11_ca_file
    end else begin
        print,'*** adas8xx_create_adf15_adf40:  no adf11 output file names - no archiving. ***'
        archive_adf11 = 0
    end 

    if archive_adf11  then begin
    
        p0=0
        p1=strpos(adf11_template,'_',p0)
        archive_adf11_dir=archive_dir + '/' +                            $
                          strmid(adf11_template,p0,p1-p0)           
        res = file_info(archive_adf11_dir)
        if res.exists EQ 1 AND res.directory EQ 0 then message,          $
             'Cannot create ' + archive_adf11_dir
        if res.exists EQ 0 then file_mkdir, archive_adf11_dir
        
        p0=p1+1
        p1=strpos(adf11_template,'_',p0)
        p2=p1+1
        p2=strpos(adf11_template,'_',p2)
        
        archive_adf11_partial_dir=archive_adf11_dir + '/' +              $
                          strmid(adf11_template,p0,p2-p0)
        res = file_info(archive_adf11_partial_dir)
        if res.exists EQ 1 AND res.directory EQ 0 then message,          $
             'Cannot create ' + archive_adf11_partial_dir
        if res.exists EQ 0 then file_mkdir, archive_adf11_partial_dir

        p0=p2+1
        p1=strpos(adf11_template,'_',p0)
        p2=p1+1
        p2=strpos(adf11_template,'_',p2)
        p2=p2+1
        p2=strpos(adf11_template,'_',p2)
        
        archive_adf11_element_dir=archive_adf11_partial_dir + '/' +              $
                          strmid(adf11_template,p0,p2-p0)           
        res = file_info(archive_adf11_element_dir)
        if res.exists EQ 1 AND res.directory EQ 0 then message,          $
             'Cannot create ' + archive_adf11_element_dir
        if res.exists EQ 0 then file_mkdir, archive_adf11_element_dir
        
        p0=p2+1
        
        if ca_only eq 0 then begin
        
             if keyword_set(files.adf11_ls_file) then begin
                 archive_adf11_ls_file=archive_adf11_element_dir +       $
                                       '/' + strmid(files.adf11_ls_file,p0)
             endif                                       
             if keyword_set(files.adf11_ic_file) then begin
                 archive_adf11_ic_file=archive_adf11_element_dir +       $
                                       '/' + strmid(files.adf11_ic_file,p0)
             endif
             
        endif     
                                                 
        if keyword_set(files.adf11_ca_file) then begin
            archive_adf11_ca_file=archive_adf11_element_dir +       $
                                  '/' + strmid(files.adf11_ca_file,p0)
        endif                     
                       
    endif                                       

endif

loc_ksh = file_which(getenv('PATH'), 'ksh')         
cmd = 'pwd'                                         
spawn, cmd, res                                     
here = res[0]                                       
         
if ca_only eq 0 then begin

     ;-----------------------------------------
     ; write the ls script file
     ;-----------------------------------------

     if keyword_set(files.adf42_ls_file) and                                  $
        keyword_set(files.adf42_ls_pp_file) and                               $
        keyword_set(files.adf04_ls_file) then begin                           $

         openw, lun, script_ls_file, /GET_LUN

         printf, lun, '#!' + loc_ksh
         printf, lun, ' '
         printf, lun, 'date'
         printf, lun, ' '
         printf, lun, 'uname -a'
         printf, lun, ' '
         printf, lun, 'cp ' + here + '/' + files.adf42_ls_file + ' /tmp/.'
         printf, lun, 'cp ' + here + '/' + files.adf42_ls_pp_file + ' /tmp/.'
         printf, lun, 'cp ' + here + '/' + files.adf04_ls_file + ' /tmp/.'

         printf, lun, 'cd /tmp/'
         printf, lun, adaspath+ 'offline_adas/adas8#1/scripts/run_adas810 ' + $
                      '/tmp/' + files.adf42_ls_pp_file
         printf, lun, 'mv /tmp/' + files.adf15_ls_file + ' ' + here + '/.'
         printf, lun, 'mv /tmp/' + files.adf40_ls_file + ' ' + here + '/.'
         printf, lun, 'mv /tmp/' + files.adf11_ls_file + ' ' + here + '/.'
         printf, lun, 'date'

         if keyword_set(archive_files) then begin 

             printf, lun, 'mv ' + here + '/' +  files.adf42_ls_file + ' ' +          $
                     archive_adf42_ls_file
             printf, lun, 'mv ' + here + '/' +  files.adf42_ls_pp_file + ' ' +       $
                     archive_adf42_ls_pp_file
             printf, lun, 'mv ' + here + '/' +  files.adf15_ls_file + ' ' +          $
                     archive_adf15_ls_file
             printf, lun, 'mv ' + here + '/' +  files.adf40_ls_file + ' ' +          $
                     archive_adf40_ls_file
             printf, lun, 'mv ' + here + '/' +  files.adf11_ls_file + ' ' +          $
                     archive_adf11_ls_file

         endif

         free_lun, lun

         cmd = 'chmod +x ' + script_ls_file
         spawn, cmd

           
           if not keyword_set(donotrun) then begin
             cmd = './' + script_ls_file
             spawn, cmd
           endif

          
     endif

     ;-----------------------------------------
     ; write the ic script file
     ;-----------------------------------------

     if keyword_set(files.adf42_ic_file) and                                  $
        keyword_set(files.adf42_ic_pp_file) and                               $
        keyword_set(files.adf04_ic_file) then begin                           $

         loc_ksh = file_which(getenv('PATH'), 'ksh')
         cmd = 'pwd'
         spawn, cmd, res
         here = res[0]

         openw, lun, script_ic_file, /GET_LUN

         printf, lun, '#!' + loc_ksh
         printf, lun, ' ' 
         printf, lun, 'date'
         printf, lun, ' '
         printf, lun, 'uname -a'
         printf, lun, ' '
         printf, lun, 'cp ' + here + '/' + files.adf42_ic_file + ' /tmp/.'
         printf, lun, 'cp ' + here + '/' + files.adf42_ic_pp_file + ' /tmp/.'
         printf, lun, 'cp ' + here + '/' + files.adf04_ic_file + ' /tmp/.'

         printf, lun, 'cd /tmp/'
         printf, lun, adaspath+ 'offline_adas/adas8#1/scripts/run_adas810 ' + $
                      '/tmp/' + files.adf42_ic_pp_file
         printf, lun, 'mv /tmp/' + files.adf15_ic_file + ' ' + here + '/.'
         printf, lun, 'mv /tmp/' + files.adf40_ic_file + ' ' + here + '/.'
         printf, lun, 'mv /tmp/' + files.adf11_ic_file + ' ' + here + '/.'
         printf, lun, 'date'

         if keyword_set(archive_files) then begin

             printf, lun, 'mv ' + here + '/' +  files.adf42_ic_file + ' ' +   $
                     archive_adf42_ic_file
             printf, lun, 'mv ' + here + '/' +  files.adf42_ic_pp_file + ' ' +$
                     archive_adf42_ic_pp_file
             printf, lun, 'mv ' + here + '/' +  files.adf15_ic_file + ' ' +   $
                     archive_adf15_ic_file
             printf, lun, 'mv ' + here + '/' +  files.adf40_ic_file + ' ' +   $
                     archive_adf40_ic_file
             printf, lun, 'mv ' + here + '/' +  files.adf11_ic_file + ' ' +   $
                     archive_adf11_ic_file

          endif

          free_lun, lun

          cmd = 'chmod +x ' + script_ic_file
          spawn, cmd
           if not keyword_set(donotrun) then begin
             cmd = './' + script_ic_file
             spawn, cmd
           endif

          
     endif
     
endif     
     
;-----------------------------------------
; write the ca script file
;-----------------------------------------

if keyword_set(files.adf42_ca_file) and                                  $
   keyword_set(files.adf42_ca_pp_file) and                               $
   keyword_set(files.adf04_ca_file) then begin                           $

     openw, lun, script_ca_file, /GET_LUN

     printf, lun, '#!' + loc_ksh
     printf, lun, ' '
     printf, lun, 'date'
     printf, lun, ' '
     printf, lun, 'uname -a'
     printf, lun, ' '
     printf, lun, 'cp ' + here + '/' + files.adf42_ca_file + ' /tmp/.'
     printf, lun, 'cp ' + here + '/' + files.adf42_ca_pp_file + ' /tmp/.'
     printf, lun, 'cp ' + here + '/' + files.adf04_ca_file + ' /tmp/.'

     printf, lun, 'cd /tmp/'
     printf, lun, adaspath+ 'offline_adas/adas8#1/scripts/run_adas810 ' + $
                  '/tmp/' + files.adf42_ca_pp_file
     printf, lun, 'mv /tmp/' + files.adf15_ca_file + ' ' + here + '/.'
     printf, lun, 'mv /tmp/' + files.adf40_ca_file + ' ' + here + '/.'
     printf, lun, 'mv /tmp/' + files.adf11_ca_file + ' ' + here + '/.'
     printf, lun, 'date'


     if keyword_set(archive_files) then begin

         printf, lun, 'mv ' + here + '/' +  files.adf42_ca_file + ' ' +          $
                 archive_adf42_ca_file
         printf, lun, 'mv ' + here + '/' +  files.adf42_ca_pp_file + ' ' +       $
                 archive_adf42_ca_pp_file
         printf, lun, 'mv ' + here + '/' +  files.adf15_ca_file + ' ' +          $
                 archive_adf15_ca_file
         printf, lun, 'mv ' + here + '/' +  files.adf40_ca_file + ' ' +          $
                 archive_adf40_ca_file
         printf, lun, 'mv ' + here + '/' +  files.adf11_ca_file + ' ' +          $
                 archive_adf11_ca_file
;         printf, lun, 'rm ' + here + '/' + adf04_ca_file

     endif

     free_lun, lun

     cmd = 'chmod +x ' + script_ca_file
     spawn, cmd

      if not keyword_set(donotrun) then begin
        cmd = './' + script_ca_file
        spawn, cmd
      endif



endif
;-----------------------------------------------------------------------
; Tidy up script files
;-----------------------------------------------------------------------

if keyword_set(archive_files) then begin
  xxelem, z0_nuc, elname
  scriptdir=strcompress(archive_dir+'/scripts/', /remove_all)
  cmd = 'mkdir -p '+scriptdir
  spawn, cmd
  scriptdir=strcompress(scriptdir+'/'+elname+'/', /remove_all)
  cmd = 'mkdir -p '+scriptdir
  spawn, cmd

  if not keyword_set(ca_only) then begin
    if file_test(script_ic_file) then begin
     cmd = 'mv '+script_ic_file+' '+scriptdir
      spawn, cmd
      if keyword_set(donotrun) then begin
        print, 'adas810 IC script file: '+scriptdir+'/'+script_ic_file
      endif
    endif
    if file_test(script_ls_file) then begin
       cmd = 'mv '+script_ls_file+' '+scriptdir
      spawn, cmd
      if keyword_set(donotrun) then begin
        print, 'adas810 LS script file: '+scriptdir+'/'+script_ls_file
      endif
    endif
  endif
  if file_test(script_ca_file) then begin
    cmd = 'mv '+script_ca_file+' '+scriptdir
    spawn, cmd
    if keyword_set(donotrun) then begin
      print, 'adas810 CA script file: '+scriptdir+'/'+script_ca_file
    endif

  endif
endif else begin
  if keyword_set(donotrun) then begin
    print, 'adas810 IC script file: '+script_ic_file
    print, 'adas810 LS script file: '+script_ls_file
    print, 'adas810 CA script file: '+script_ca_file
  endif
endelse
END
