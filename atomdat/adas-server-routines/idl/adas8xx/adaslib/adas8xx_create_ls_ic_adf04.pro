;+
; PROJECT : ADAS 
;
; NAME    : adas8xx_create_ls_ic_adf04
;
; PURPOSE : Runs adas801 for LS and IC coupling
;
; EXPLANATION:
;
; USE:
;
; INPUTS:
;       z0_nuc:   Nuclear charge
;       z_ion :   Ion charge
;       occup: Occupation numbers of configurations
;                   First index:  configuration number
;                   Second index: orbital number.
;       plasma: structure containing temperature information:
;            theta: array of electron temperatures
;            noscale: is theta an array of scaled temperatures?
;            indx_theta: subset of theta to use for calculations.
;
; OPTIONAL INPUTS:
;       None.
;
; OUTPUTS:
;       None.
;
; OPTIONAL OUTPUTS:
;
; KEYWORD PARAMETERS:
;       files = structure storing file names. Required names are:
;                   adf42_ic_file
;                   adf42_ic_pp_file
;                   adf04_ic_file
;                   adf15_ic_file
;                   adf40_ic_file   
;                   adf11_ic_file
;                   adf42_ca_file
;                   adf42_ca_pp_file
;                   adf04_ca_file
;                   adf15_ca_file
;                   adf40_ca_file
;                   adf11_ca_file
;                   adf42_ls_file
;                   adf42_ls_pp_file
;                   adf04_ls_file
;                   adf15_ls_file
;                   adf40_ls_file
;                   adf11_ls_file
;                 note that if ca_only is set then the ic & ls files
;                 need not be supplied. ADF04 & ADF42 files must 
;                 already exist, the others are files which will be 
;                 generated.
;       donotrun  : generate script file but do not run 801.
;       archive_dir: directory to store output files in (default = current directory)
;       archive_files: put outputs into subdirectories of archive_dir
;       cowan_scale_factors: Slater parameters for Cowan code (optional, defaults are usually
;                            sufficient)
;
;
; CALLS:
;
; SIDE EFFECTS:
;       This function spawns UNIX commands.
;
; CATEGORY:
;       Series 8 utility.
;
; WRITTEN:
;       Adam Foster
;
; MODIFIED:
;       Version 1.1   Adam Foster
;                      - First commented version
;                      - added donotrun keyword
;                      - files are now provided in files structure

; VERSION:
;       1.1     20-03-2009
;-
;------------------------------------------------------------------

PRO adas8xx_create_ls_ic_adf04, z0_nuc,                                $
                                z_ion,                                 $
                                files             = files,             $
                                archive_dir       = archive_dir,       $
                                archive_files     = archive_files,     $
                                donotrun          = donotrun

;-----------------------------------------------------
; Get location of ADAS from environment variable
;-----------------------------------------------------
adaspath=getenv('ADASCENT')+'/../'

;------------------------------------------------
; prepare the script, submit and paper file names
;------------------------------------------------

    elsymb      = strlowcase(xxesym(z0_nuc))
    
    script_ls_file = strcompress(elsymb + string(z_ion,format='(i2)')     $
                  + '_ls_801_script', /remove_all)
                  
                  
    paper_ls_file = strcompress(elsymb + string(z_ion,format='(i2)')     $
                  + '_ls_801_paper', /remove_all)

    script_ic_file = strcompress(elsymb + string(z_ion,format='(i2)')     $
                  + '_ic_801_script', /remove_all)
                  
    paper_ic_file = strcompress(elsymb + string(z_ion,format='(i2)')     $
                  + '_ic_801_paper', /remove_all)


;------------------------------------------------
; prepare for archiving if required
;------------------------------------------------

if keyword_set(archive_files) then begin

    if keyword_set(archiv_dir) then begin
    
        res = file_info(archive_dir)
        if res.exists EQ 1 AND res.directory EQ 0 then message,       $
             'Cannot create ' + archive_dir
        if res.exists EQ 0 then file_mkdir, archive_dir
        
    endif else begin
    
        cmd = 'pwd'
        spawn, cmd, result
        here = result[0]
        archive_dir = here
        
    endelse
    
    p0=0
    p1=strpos(files.adf34_file,'_',p0)
    archive_adf34_dir=archive_dir + '/' +                            $
                      strmid(files.adf34_file,p0,p1-p0)                  
    res = file_info(archive_adf34_dir)
    if res.exists EQ 1 AND res.directory EQ 0 then message,             $
             'Cannot create ' + archive_adf34_dir
    if res.exists EQ 0 then file_mkdir, archive_adf34_dir
    
    p0=p1+1
    p1=strpos(files.adf34_file,'_',p0)
    archive_adf34_element_dir=archive_adf34_dir + '/' +              $
                      strmid(files.adf34_file,p0,p1-p0)                  
    res = file_info(archive_adf34_element_dir)
    if res.exists EQ 1 AND res.directory EQ 0 then message,             $
             'Cannot create ' + archive_adf34_element_dir
    if res.exists EQ 0 then file_mkdir, archive_adf34_element_dir
    
    p0=p1+1
    archive_adf34_file=archive_adf34_element_dir + '/' +             $
                       strmid(files.adf34_file,p0)                  
    archive_adf34_inst_file=archive_adf34_element_dir + '/' +        $
                       strmid(files.adf34_inst_file,p0)                  
    archive_adf34_ls_pp_file=archive_adf34_element_dir + '/' +       $
                       strmid(files.adf34_ls_pp_file,p0)                  
    archive_adf34_ic_pp_file=archive_adf34_element_dir + '/' +       $
                       strmid(files.adf34_ic_pp_file,p0)                  

    p0=0
    p1=strpos(files.adf04_ls_file,'_',p0)
    archive_adf04_dir=archive_dir + '/' +                            $
                      strmid(files.adf04_ls_file,p0,p1-p0)                  
    res = file_info(archive_adf04_dir)
    if res.exists EQ 1 AND res.directory EQ 0 then message,             $
             'Cannot create ' + archive_adf04_dir
    if res.exists EQ 0 then file_mkdir, archive_adf04_dir
    
    p0=p1+1
    p1=strpos(files.adf04_ls_file,'_',p0)
    archive_adf04_element_dir=archive_adf04_dir + '/' +              $
                      strmid(files.adf04_ls_file,p0,p1-p0)                  
    res = file_info(archive_adf04_element_dir)
    if res.exists EQ 1 AND res.directory EQ 0 then message,             $
             'Cannot create ' + archive_adf04_element_dir
    if res.exists EQ 0 then file_mkdir, archive_adf04_element_dir
    
    p0=p1+1
    archive_adf04_ls_file=archive_adf04_element_dir + '/' +          $
                       strmid(files.adf04_ls_file,p0)                  
    archive_adf04_ic_file=archive_adf04_element_dir + '/' +        $
                       strmid(files.adf04_ic_file,p0)                  

endif

;-----------------------------------------
; write the ls script file
;-----------------------------------------

loc_ksh = file_which(getenv('PATH'), 'ksh')
cmd = 'pwd'
spawn, cmd, res
here = res[0]

openw, lun, script_ls_file, /GET_LUN

printf, lun, '#!' + loc_ksh
printf, lun, ' '
printf, lun, 'date'
printf, lun, ' '
printf, lun, 'uname -a'
printf, lun, ' '
printf, lun, 'cp ' + here + '/' + files.adf34_file + ' /tmp/.'
printf, lun, 'cp ' + here + '/' + files.adf34_inst_file + ' /tmp/.'
printf, lun, 'cp ' + here + '/' + files.adf34_ls_pp_file + ' /tmp/.'

printf, lun, 'cd /tmp/'
printf, lun, adaspath+'offline_adas/adas8#1/scripts/run_adas8#1 ' + $
             '/tmp/' + files.adf34_file + ' '                             + $
             '/tmp/' + files.adf34_inst_file + ' '                        + $
             '/tmp/' + files.adf34_ls_pp_file
printf, lun, 'mv /tmp/' + files.adf04_ls_file + ' ' + here + '/.'
printf, lun, 'date'

free_lun, lun

cmd = 'chmod +x ' + script_ls_file
spawn, cmd

;-----------------------------------------------------------------
; decide if submit to loadleveler and if so prepare ls submit file
;-----------------------------------------------------------------

if not keyword_set(donotrun) then begin
  cmd = './' + script_ls_file
  spawn, cmd
endif 

;-----------------------------------------
; write the ic script file
;-----------------------------------------

openw, lun, script_ic_file, /GET_LUN

printf, lun, '#!' + loc_ksh
printf, lun, ' '
printf, lun, 'date'
printf, lun, ' '
printf, lun, 'uname -a'
printf, lun, ' '
printf, lun, 'cp ' + here + '/' + files.adf34_file + ' /tmp/.'
printf, lun, 'cp ' + here + '/' + files.adf34_inst_file + ' /tmp/.'
printf, lun, 'cp ' + here + '/' + files.adf34_ic_pp_file + ' /tmp/.'

printf, lun, 'cd /tmp/'
printf, lun, adaspath+'/offline_adas/adas8#1/scripts/run_adas8#1 ' + $
             '/tmp/' + files.adf34_file + ' '                             + $
             '/tmp/' + files.adf34_inst_file + ' '                        + $
             '/tmp/' + files.adf34_ic_pp_file
printf, lun, 'mv /tmp/' + files.adf04_ic_file + ' ' + here + '/.'
printf, lun, 'date'

if keyword_set(archive_files) then begin

    printf, lun, 'mv ' + here + '/' +  files.adf34_file + ' ' +             $
            archive_adf34_file
    printf, lun, 'mv ' + here + '/' +  files.adf34_inst_file + ' ' +        $
            archive_adf34_inst_file
    printf, lun, 'mv ' + here + '/' +  files.adf34_ls_pp_file + ' ' +       $
            archive_adf34_ls_pp_file
    printf, lun, 'mv ' + here + '/' +  files.adf34_ic_pp_file + ' ' +       $
            archive_adf34_ic_pp_file
    
    printf, lun, 'cp ' + here + '/' +  files.adf04_ls_file + ' ' +          $
            archive_adf04_ls_file
    printf, lun, 'cp ' + here + '/' +  files.adf04_ic_file + ' ' +          $
            archive_adf04_ic_file

endif

free_lun, lun

cmd = 'chmod +x ' + script_ic_file
spawn, cmd

if not keyword_set(donotrun) then begin
  cmd = './' + script_ic_file
  spawn, cmd
endif

;endelse

if keyword_set(archive_files) then begin
  xxelem, z0_nuc, elname
  scriptdir=strcompress(archive_dir+'/scripts/', /remove_all)
  cmd = 'mkdir -p '+scriptdir
  spawn, cmd
  scriptdir=strcompress(scriptdir+'/'+elname+'/', /remove_all)
  cmd = 'mkdir -p '+scriptdir
  spawn, cmd

  
  
  cmd = 'mv '+script_ic_file+' '+scriptdir
  spawn, cmd
  cmd = 'mv '+script_ls_file+' '+scriptdir
  spawn, cmd
  
  if keyword_set(donotrun) then begin
    print, 'adas801 IC script file: '+scriptdir+'/'+script_ic_file
    print, 'adas801 LS script file: '+scriptdir+'/'+script_ls_file
  endif
  
endif else begin
  if keyword_set(donotrun) then begin
    print, 'adas801 IC script file: '+script_ic_file
    print, 'adas801 LS script file: '+script_ls_file
  endif
endelse
END
