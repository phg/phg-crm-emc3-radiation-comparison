;----------------------------------------------------------------------
;+
; PROJECT    :  ADAS
;
; NAME       :  adas8xx_opt_make_adf11
;
; PURPOSE    :  Takes ic, ca and large ca run ADAS810 PLTs for individual
;               stages and cretes a master ADF11 PLT for the element
;               formula is PLT(ic)+PLT(ca_large)-PLT(ca)
;
; ARGUMENTS  :  All output arguments will be defined appropriately.
;
;               NAME      I/O    TYPE   DETAILS
; REQUIRED   :  normdir    I     str    full path to directory from 
;                                       which ADAS808 data has beem
;                                       archived (ic and ca)
;               largedir   I     str    full path to directory from 
;                                       which ADAS808 data has beem
;                                       archived (large ca)
;               z_nuc      I     int    Atomic number of element
; OPTIONAL   :  a11file    I     str    ADF11 file name for output
;                                       (default is in:
;                              <normdir>/adf11/plt<year>/plt<year>_<elsymb>.dat
;               year       I     int    Year number for data (default
;                                       is 40)
;
;
; KEYWORDS      comments          -     array of additional comments to add to file
;               help              -     display help entry
;
; NOTES    
;
;       This code is loosely based on read_adf11. Does not yet take into account superstaging.
;       Designed to be compatible with fulldata structure from read_adf11, although only
;       uses a fraction of the information within that structure.
;
; AUTHOR     :  Adam Foster
;
; DATE       :  09/02/2009
;
; MODIFIED:
;       1.1     Adam Foster
;               - First version.
;       1.2     Martin O'Mullane
;               - Modify for changes in write_adf11 routine.
;
; VERSION:
;
;       1.1     09/02/2009
;       1.2     21-04-2010
;-
;----------------------------------------------------------------------


PRO adas8xx_opt_make_adf11, normdir  = normdir,  $
                            largedir = largedir, $
                            a11file  = a11file,  $
                            z_nuc    = z_nuc,    $
                            year     = year,     $
                            help     = help

if keyword_set(help) then begin
   doc_library, 'adas8xx_opt_make_adf11'
   return
endif

elsymb=xxesym(z_nuc,/lower)

if not keyword_set(year) then begin
  print, 'ADAS8xx_opt_make_adf11 warning: year not '+$
         'set, using 40 as default'
  year=40
endif
yearstr=strcompress(string(year),/remove_all)
;default ADF11 output file:
if not keyword_set(a11file) then begin
  a11file=normdir+'/adf11/plt'+yearstr+'/plt'+yearstr+'_'+elsymb+'.dat'
endif

;ca files:
cafile=strcompress(normdir+'/'+$
              '/adf11/plt'+yearstr+'_partial/plt'+yearstr+'_partial_'+elsymb+$
              '/mkadf11_plt'+yearstr+'_ca#'+elsymb+string(indgen(z_nuc))+$
              '.dat',/remove_all)

icfile=strcompress(normdir+'/'+$
              '/adf11/plt'+yearstr+'_partial/plt'+yearstr+'_partial_'+elsymb+$
              '/mkadf11_plt'+yearstr+'_ic#'+elsymb+string(indgen(z_nuc))+$
              '.dat',/remove_all)

largecafile=strcompress(largedir+'/'+$
              '/adf11/plt'+yearstr+'_partial/plt'+yearstr+'_partial_'+elsymb+$
              '/mkadf11_plt'+yearstr+'_ca#'+elsymb+string(indgen(z_nuc))+$
              '.dat',/remove_all)

read_adf11, file=cafile[0], fulldata=fd_ca
read_adf11, file=icfile[0], fulldata=fd_ic
read_adf11, file=largecafile[0], fulldata=fd_lca

; consistency checks
if (n_elements(fd_ca.ddens) NE n_elements(fd_ic.ddens)) OR $
    (n_elements(fd_ca.ddens) NE n_elements(fd_lca.ddens)) THEN BEGIN
  print,' Mismatch in number of densities! Halting'
  stop
endif  

if (n_elements(fd_ca.dtev) NE n_elements(fd_ic.dtev)) OR $
    (n_elements(fd_ca.dtev) NE n_elements(fd_lca.dtev)) THEN BEGIN
  print,' Mismatch in number of temperatures! Halting'
  stop
endif 

if ((where(fd_ca.ddens NE fd_ic.ddens))[0] NE -1) OR $
   ((where(fd_ca.ddens NE fd_lca.ddens))[0] NE -1) THEN BEGIN
  print,' Mismatch in densities! Halting'
  stop
endif  
 
if ((where(fd_ca.dtev NE fd_ic.dtev))[0] NE -1) OR $
   ((where(fd_ca.dtev NE fd_lca.dtev))[0] NE -1) THEN BEGIN
  print,' Mismatch in temperatures! Halting'
  stop
endif  

ddens=fd_ca.ddens
dtev=fd_ca.dtev
drcof_ca=dblarr(z_nuc,n_elements(dtev),n_elements(ddens))
drcof_ic=dblarr(z_nuc,n_elements(dtev),n_elements(ddens))
drcof_lca=dblarr(z_nuc,n_elements(dtev),n_elements(ddens))
isppr=intarr(z_nuc)
ispbr=intarr(z_nuc)

drcof_ca[0,*,*]=reform(fd_ca.drcof)
drcof_ic[0,*,*]=reform(fd_ic.drcof)
drcof_lca[0,*,*]=reform(fd_lca.drcof)
isppr[0]=fd_ca.isppr
ispbr[0]=fd_ca.ispbr
if z_nuc GT 1 then begin
  for iz1=1, z_nuc-1 do begin

    read_adf11, file=cafile[iz1], fulldata=fd_ca
    read_adf11, file=icfile[iz1], fulldata=fd_ic
    read_adf11, file=largecafile[iz1], fulldata=fd_lca
  
  
    if (n_elements(fd_ca.ddens) NE n_elements(ddens)) OR $
       (n_elements(fd_ic.ddens) NE n_elements(ddens)) OR $
       (n_elements(fd_lca.ddens) NE n_elements(ddens)) THEN BEGIN
      print,' Mismatch in number of densities! Halting'
      stop
    endif  

    if (n_elements(fd_ca.dtev) NE n_elements(dtev)) OR $
       (n_elements(fd_ic.dtev) NE n_elements(dtev)) OR $
       (n_elements(fd_lca.dtev) NE n_elements(dtev)) THEN BEGIN
      print,' Mismatch in number of temperatures! Halting'
      stop
    endif 

    if ((where(fd_ca.ddens NE ddens))[0] NE -1) OR $
       ((where(fd_ic.ddens NE ddens))[0] NE -1) OR $
       ((where(fd_lca.ddens NE ddens))[0] NE -1) THEN BEGIN
      print,' Mismatch in densities! Halting'
      stop
    endif  
 
    if ((where(fd_ca.dtev NE dtev))[0] NE -1) OR $
       ((where(fd_ic.dtev NE dtev))[0] NE -1) OR $
       ((where(fd_lca.dtev NE dtev))[0] NE -1) THEN BEGIN
      print,' Mismatch in temperatures! Halting'
      stop
    endif  
  
    
    drcof_ca[iz1,*,*]=reform(fd_ca.drcof)
    drcof_ic[iz1,*,*]=reform(fd_ic.drcof)
    drcof_lca[iz1,*,*]=reform(fd_lca.drcof)
    
    ispbr[iz1]=fd_ca.ispbr
    isppr[iz1]=fd_ca.isppr
  endfor
endif

; do the maths on the PLTs

drcof_out=alog10(10^drcof_ic+10^drcof_lca-10^drcof_ca)


;prepare to write the ADF11

data={iz0    :z_nuc,            $
      is1min :1,                $
      is1max :z_nuc,            $
      isppr  :isppr,            $
      ispbr  :ispbr,            $
      isstgr :indgen(z_nuc)+1,  $
      itmax  :n_elements(dtev), $
      idmax  :n_elements(ddens),$
      ismax  :z_nuc,            $
      ddens  :ddens,            $
      dtev   :dtev,             $
      drcof  :drcof_out,        $
      class  :'plt'}

comments=['C Generated from ADAS810 data in '+normdir,$
          'C and expanded configuration count PLT data in '+largedir,$
          'C by '+xxuser(),$
          'C on '+(xxdate())[0]]

cmd='mkdir -p '+normdir+'/adf11/plt'+yearstr+''
spawn, cmd
          
write_adf11, outfile  = a11file,         $
             fulldata = data,            $
             project  = 'ADAS810-total', $
             comments = comments

end
