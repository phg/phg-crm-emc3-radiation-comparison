;----------------------------------------------------------------------
;+
; PROJECT    :  ADAS
;
; NAME       :  run_adas807
;
; PURPOSE    :  A non-interactive version of adas807 which generates
;               supporting datasets for GCR processing.
;
;               NAME       I/O   TYPE    DETAILS
; INPUTS     :  adf04()     I    str     2 element array. First entry ionising
;                                        adf04 and second is the ionised adf04
;                                        (except if ionising is H-like).
;               adf09()     I    str     Set of DR datasets.
;               adf07       I    str     Ionisation rates dataset.
;
; KEYWORDS   :  help        I     -      Display header as help.
;
; OUTPUTS    :  All information is returned in ADAS datasets.
;               op_ion      O    str  Output file for ion pathways
;               op_slines   O    str  Output file for adf04 S-lines
;               op_adf08    O    str  Output file for adf08 driver
;               op_a09_a04  O    str  Output file for adf18 DR map to adf04
;               op_a17_p208 O    str  Output file for adf18 for projection to
;                                     adf04.
;
;
; NOTES      :  No fortran is spawned.
;
;
; AUTHOR     :  Martin O'Mullane
;
;
; MODIFIED:
;       1.1     Martin O'Mullane
;                 - First version.
;       1.2     Martin O'Mullane
;                 - Allow Eissner adf04 files by testing configurations
;                   with xxdtes and converting on the fly with xxcftr.
;
; VERSION:
;       1.1     28-08-2009
;       1.2     07-06-2013
;-
;----------------------------------------------------------------------



PRO run_adas807, adf04       =  adf04,        $
                 adf09       =  adf09,        $
                 adf07       =  adf07,        $
                 op_ion      =  op_ion,       $
                 op_slines   =  op_slines,    $
                 op_adf08    =  op_adf08,     $
                 op_a09_a04  =  op_a09_a04,   $
                 op_a17_p208 =  op_a17_p208,  $
                 help        =  help


; If asked for help

if keyword_set(help) then begin
   doc_library, 'run_adas807'
   return
endif


; Check the file names

if n_elements(adf04) EQ 0 then begin

    message,'Specific ion data adf04 file(s) required'

endif else begin

   adf04_filea = adf04[0]
   file_acc, adf04_filea, exist, read, write, execute, filetype
   if exist ne 1 then message, 'Ionising adf04 file does not exist '+ adf04_filea
   if read ne 1 then message, 'Ionising adf04 file cannot be read from this userid '+ adf04_filea

   if n_elements(adf04) EQ 2 then begin

      adf04_filep = adf04[1]
      file_acc, adf04_filep, exist, read, write, execute, filetype
      if exist ne 1 then message, 'Ionised adf04 file does not exist '+ adf04_filep
      if read ne 1 then message, 'Ionised adf04 file cannot be read from this userid '+ adf04_filep

   endif else adf04_filep = 'NULL'

endelse




; Run the calculation - follow logic of interactive program


;--------------------------------------
; Input and metastable selection screen
;--------------------------------------

; Read Recombined/Ionizing file

read_adf04, file = adf04_filea, fulldata=all

scefa  = all.te
iza    = all.iz
nza    = all.iz0
ia     = all.ia
isa    = all.isa
ila    = all.ila
xja    = all.xja
wva    = all.wa
bwna   = all.bwno

nlev   = n_elements(isa)
cstrga = strarr(nlev)
for j = 0, nlev-1 do begin
   xxdtes, in_cfg=all.cstrga[j], is_eissner=is_eissner
   if is_eissner EQ 1 then begin
      xxcftr, in_cfg=all.cstrga[j], out_cfg=tcfg, type=3
   endif else tcfg = all.cstrga[j]
   xxprs3, config = tcfg, z_nuc=nza, z_ion=iza, occup=occ
   cstrga[j] = occ2cfg(occ, /display, /upper)
endfor

; number of levels of recombined ion
na = n_elements(ia)

; sequence of recombined ion
ff  =  nza-iza
seq = xxesym(ff)


; Read recombining/ionised ion-file if sequence is not 'H'

if strupcase(seq) ne 'H' then begin

   read_adf04, file = adf04_filep, fulldata=all

   izp    = all.iz
   nzp    = all.iz0
   ip     = all.ia
   isp    = all.isa
   ilp    = all.ila
   xjp    = all.xja
   wvp    = all.wa
   bwnp   = all.bwno

   nlev   = n_elements(isp)
   cstrgp = strarr(nlev)
   for j = 0, nlev-1 do begin
      xxdtes, in_cfg=all.cstrga[j], is_eissner=is_eissner
      if is_eissner EQ 1 then begin
         xxcftr, in_cfg=all.cstrga[j], out_cfg=tcfg, type=3
      endif else tcfg = all.cstrga[j]
      xxprs3, config = tcfg, z_nuc=nzp, z_ion=izp, occup=occ
      cstrgp[j] = occ2cfg(occ, /display, /upper)
   endfor

   ;number of levels of recombinig ion
   np = n_elements(ip)

   ;level description as a string (to choose the metastables)
   slen = max(strlen(cstrgp))
   levelp = strarr(np) + string(replicate(32B,slen))
   level_guess = strarr(np)+ string(replicate(32B,slen))
   for i=0,np-1 do begin
      str = string(replicate(32B,slen))

      strput, str, cstrgp[i]
      levelp[i] = str+'    '+string(isp[i],form='(i1)')+ $
                  l_value(ilp[i])+j_value(xjp[i])
      level_guess[i] = str+'    '+string(isp[i],form='(i1)')+ $
                  l_value(ilp[i])
   endfor

endif else begin

   message,'H- sequence: continuing...', /continue
   nzp = nza
   izp = iza+1
   ip = 1
   cstrgp = ' '
   isp = 1
   ilp = 0
   xjp = 0.0
   wvp = 0.0
   bwnp = 0.0

endelse


; Now guess the metastables

  ; sequence of recombining ion - special handling for fully stripped
  ff =  nzp-izp
  seq = xxesym(ff)

  if ff GT 0 then begin

     metastable, seq=seq, config=config_g, term=term_g, /upper
     n_meta = n_elements(config_g)
     guess_str = strarr(n_meta) + string(replicate(32B,slen))
     for i = 0, n_meta-1 do begin
        str = guess_str[i]
        strput, str, config_g[i]
        guess_str[i] = str + '    ' + term_g[i]
     endfor

     parents = intarr(np)

     for j = 0, n_elements(guess_str)-1 do begin
        ind = where(level_guess EQ guess_str[j], count)
        if count EQ 1 then begin
           parents[ind[0]] = 1
           print, guess_str[j] + ' will be used as a metastable'
        endif
     endfor

   parents = where(parents eq 1)
   ip = ip[parents]
   cstrgp = cstrgp[parents]
   isp = isp[parents]
   ilp = ilp[parents]
   xjp = xjp[parents]
   wvp = wvp[parents]

   nva   = n_elements(scefa)

   data_a = { iz    : iza,     $
              nz    : nza,     $
              i     : ia,      $
              cstrg : cstrga,  $
              is    : isa,     $
              il    : ila,     $
              xj    : xja,     $
              wv    : wva,     $
              bwn   : bwna,    $
              nv    : nva,     $
              scef  : scefa    }

   scefp = fltarr(1)

   data_p = { iz    : izp,     $
              nz    : nzp,     $
              i     : ip,      $
              cstrg : cstrgp,  $
              is    : isp,     $
              il    : ilp,     $
              xj    : xjp,     $
              wv    : wvp,     $
              bwn   : bwnp,    $
              nv    : 0,       $
              scef  : scefp    }

endif else begin

   nva   = n_elements(scefa)

   data_a = { iz    : iza,     $
              nz    : nza,     $
              i     : ia,      $
              cstrg : cstrga,  $
              is    : isa,     $
              il    : ila,     $
              xj    : xja,     $
              wv    : wva,     $
              bwn   : bwna,    $
              nv    : nva,     $
              scef  : scefa    }

   scefp = fltarr(1)

   data_p = { iz    : iza+1,   $
              nz    : nza,     $
              i     : 1,       $
              cstrg : ' ',     $
              is    : 1,       $
              il    : 0,       $
              xj    : 1.0,     $
              wv    : 0.0,     $
              bwn   : 0.0,     $
              nv    : 0,       $
              scef  : scefp    }
endelse


;--------------
; Output screen
;--------------

;number of levels of recombining ion
  np = n_elements(data_p.i)

;number of levels of recombined ion
  na = n_elements(data_a.i)

;sequence of recombined ion
  ff =  data_a.nz - data_a.iz
  seq = xxesym(ff)

;configurations are constructed from configuration string
;
; configuration array is ordered along n,l-shells
;
;   conf[0,k,j] = n-number of shell k of level j
;   conf[1,k,j] = l-number of shell k of level j
;   conf[2,k,j] = number of electrons in that shell of level j
;

;configuration for recombined ion
  confa =  config_str2arr(data_a.cstrg)

;configuration for recombining ion
  confp =  config_str2arr(data_p.cstrg)


;level description as a string (for output)
  levela = strarr(na)
  levelp = strarr(np)
  for i=0,na-1 do begin
     levela[i] = data_a.cstrg[i]+' '+ $
                 string(data_a.is[i],format='(i1)') + $
                 l_value(data_a.il[i])
  endfor
  for i=0,np-1 do begin
     levelp[i] = data_p.cstrg[i]+' ' + $
                 string(data_p.is[i],format='(i1)') + $
                 l_value(data_p.il[i])
  endfor


;Possible pathways to parent for each level of recombined ion
;rec_path   <-- possible transition
;               (logical variable which will become false or true)
;               rec_path = intarr(number of parents, number of levels)
adas807_recom_path, $
   data_a.is, data_a.il, data_a.xj, confa, data_a.wv, data_a.bwn, $
   data_p.is, data_p.il, confp, data_p.wv, $
   rec_path, ion_path



;-------------
; Output files
;-------------

which_path = 'X'

; Write the adf18/adf17_p208 file - if requested

  if n_elements(op_a17_p208) EQ 1 then begin

     outfile = op_a17_p208

     adas807_prep_a17_p208,                           $
        outfile,                                      $
        which_path, adf04_filea, adf04_filep, seq,    $
        na, data_a.iz, data_a.nz, data_a.i,           $
        data_a.is, data_a.il, data_a.xj, confa,       $
        np, data_p.iz, data_p.nz,                     $
        data_p.i, data_p.is, data_p.il,               $
        ion_path

  endif



; Write the adf08 driver file - if requested

  if n_elements(op_adf08) EQ 1 then begin

     outfile = op_adf08

     adas807_prep_adf08, $
        outfile,                                                $
        which_path, adf04_filea, adf04_filep, seq,              $
        data_a.scef, na, data_a.nz, data_a.i, confa, levela,    $
        data_a.is, data_a.il, data_a.xj, data_a.wv, data_a.bwn, $
        np, data_p.i, confp, levelp, data_p.is, data_p.il,      $
        data_p.xj, data_p.wv, data_p.bwn,                                        $
        rec_path

  endif



; Write the ionisation part of the adf04 file - if requested

  if n_elements(op_ion) EQ 1 then begin

     outfile = op_ion

     adas807_add_ion_adf04, $
        outfile,                                                $
        which_path, adf04_filea, adf04_filep,                   $
        na, data_a.nz, data_a.iz, data_a.i, confa, levela,      $
        data_a.is, data_a.il, data_a.xj, data_a.wv, data_a.bwn, $
        np, data_p.i, levelp, data_p.is, data_p.il, data_p.wv,  $
        ion_path

  endif



; Write the S-lines of the adf04 file - if requested

  if n_elements(op_slines) EQ 1 then begin

     outfile = op_slines

     if n_elements(adf07) EQ 0 then message, 'Supply an adf07 rate file' $
                               else adf07file = adf07

     if seq NE 'H' then begin
        adas807_prep_slines, outfile,                     $
                     adf04_filea, adf07file, seq, data_a.scef,  $
                     data_a.nz, data_a.iz, data_p.iz,     $
                     data_a.wv, data_p.wv, data_a.bwn
     endif else begin
       adas807_prep_slines, outfile,                      $
                     adf04_filea, adf07file, seq, data_a.scef,  $
                     data_a.nz, data_a.iz, data_a.iz+1,   $
                     data_a.wv, [0.0], data_a.bwn
     endelse
  endif


; Write the adf18/a09_a04 matching file - if requested

  if n_elements(op_a09_a04) EQ 1 then begin

     outfile = op_a09_a04

     if seq NE 'H' then begin
        if n_elements(adf09) EQ 0 then message, 'Supply adf09 DR file(s)' $
                                  else adf09files = adf09
        adas807_prep_a09_a04, outfile, adf09files, adf04_filea, $
                      data_a.iz, data_a.nz,       $
                      na, data_a.i, data_a.il, data_a.is, data_a.xj, confa
     endif

  endif

END
