; Copyright (c) 2000 Strathclyde University .
;+
; PROJECT:
;       ADAS 
;
; NAME:
;	ADAS807_OUT
;
; PURPOSE: 
;       Work out possible recombination and ionisation pathways. 
;
;INPUT:
; LEVELS OF RECOMBINED ATOM:
;      isa      --> MULTIPLICITY 
;                   (ISA-1)/2 = QUANTUM NUMBER (S)
;      ila      --> QUANTUM NUMBER (L) 
;      configa  --> configuration arrays
;      wva      --> Energy  of levels above ground state
;      bwna     --> Ionisation Energy to ground state parent
; LEVELS OF RECOMBINING ATOM:
;      isp      --> MULTIPLICITY 
;      ilp      --> QUANTUM NUMBER (L)
;      configp  --> configuration arrays
;      wvp      --> Energy of levels above it's ground state              
;
;OUTPUT:
;      trans    <-- possible transition 
;                   (logical variable which will become false or true)
;                   trans = intarr(number of parents, number of levels)

;
; WRITTEN:
;       Ralph Dux
;
; MODIFIED:
;	1.1	Ralph Dux
;		First release 
;	1.2	Martin O'Mullane
;		 - Rename to adas807_recom_path.
;                - Use square brackets for arrays.
;	1.3	Martin O'Mullane
;		 - Typo: call message and not the non-existant meassage.
;	1.4	Martin O'Mullane
;		 - After removing 1 electron set to zero n and l (in confrem)
;                  if the shell then has zero electrons before moving it 
;                  to the end.
;
; VERSION:
;       1.1	08-09-1999
;       1.2	27-08-2009
;       1.3	29-11-2010
;       1.4	30-01-2013
;
;-
;-----------------------------------------------------------------------------


pro adas807_recom_path, isa, ila, xja,   confa, wva, bwna, $
                isp, ilp, confp, wvp, $
                trans, trans_ion


na = n_elements(isa)
np = n_elements(isp)
nlmax = n_elements(confa[0,*,0])
nlmaxp = n_elements(confp[0,*,0])
if nlmax ne nlmaxp then begin
 message, ' Config. arrays of Ion and Parent have different nl-shell dimension'
endif
ntot=3*nlmax


;define transition array
trans = intarr(np, na)
trans_ion = intarr(np, na)

for ja=0,na-1 do begin

;set possible transitions to false
 trans[0:np-1,ja] = 0
 trans_ion[0:np-1,ja] = 0

;for each possible nl-shell of the configuration of the recombined ion .....
 for i=0,nlmax-1 do begin
  confrem = confa[*,*,ja]
  
  ;we look whether there are electrons.... 
  if confrem[2,i] ge 1 then begin
  
   ;and remove one electron if we found any
   confrem[2,i] = confrem[2,i]-1

   ;save l-quantum nuber of removed electron
   lnum_rem = confrem[1,i]
   
   ;clean up the nl-shells if one shell becomes empty
   if confrem[2,i] eq 0 then begin
    confrem[*,i] = [0,0,0]
    for k=i+1,nlmax-2 do confrem[*,k-1] = confrem[*,k]
    confrem[*,nlmax-1] = [0,0,0]
   endif
  
   ;perform different tests for each parent
   for jp=0,np-1 do begin
   
    ; 1st test: (2*S+1) changes only by one
    test_1 = isa[ja] eq isp[jp]-1 or isa[ja] eq isp[jp]+1
    
    ; 2nd test: |L_p -l| le  L_a  le |L_p + l|
    test_2 = ila[ja] ge abs(ilp[jp]-lnum_rem) and $
             ila[ja] le abs(ilp[jp]+lnum_rem)
    
    ; configurations are equal         
    test_3 = total (confp[*,*,jp] eq confrem) eq ntot
    
    ; the energy of the level is not above the IP to
    ; that parent
    test_4 = wva[ja] lt  (bwna + wvp[jp])	
    
    ;transition passed all test ?
    test = test_1 eq 1 and test_2 eq 1 and test_3 eq 1 and test_4 eq 1
               
    ;if one configuration change was succesful the pathway
    ; is accepted           
    trans[jp,ja] = trans[jp,ja] or test
    
    test_ion = test_1 eq 1 and test_2 eq 1 and test_3 eq 1
    trans_ion[jp,ja] = trans_ion[jp,ja] or test_ion
    
   endfor
  endif 
 endfor
endfor 


; The ionisation paths are now the same as the recombination ones!
; If more than one is accessible then the lowest is to be preferred.
; However for duplicate configurations+terms it is more likely that
; these are built on different parents. Rank these in energy order.
;
; The implicit assumption in the following is that the adf04 file is 
; energy ordered. We should really check that this is the case.



nmaxa=max(confa[0,*,*])
nmaxp=max(confp[0,*,*])


cfa = config_arr2str(confa)
loca = intarr(na)

for ja=0, na-1 do begin

  cfa[ja]=cfa[ja]+string(isa[ja], ila[ja], xja[ja], $
                         format='("(",i1,")",i1,"(",f4.1,")")') 
  
  max_n = max(confa[0,*,ja])
  if max_n LE nmaxp then cfa[ja] = 'PARENT' 
 
endfor

for ja=0, na-1 do begin
   cf_test = cfa[ja]
   ind = where(cf_test EQ cfa, count)
   for j = 0, count-1 do loca[ind[j]] = j
endfor


for ja=0, na-1 do begin

 if cfa[ja] NE 'PARENT' then begin
     
    ind = where(trans_ion[*,ja] NE 0, count)
    if count GT 1 then begin
       newind = intarr(np)
       ioff = ind[0]
       if ioff GT 0 then newind[loca[ja]+ioff] = 1 $
                    else newind[loca[ja]] = 1
       trans_ion[*,ja] = newind
    endif 
    
 endif

endfor
     


end
