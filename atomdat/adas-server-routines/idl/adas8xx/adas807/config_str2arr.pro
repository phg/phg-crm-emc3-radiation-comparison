; Copyright (c) 2000 Strathclyde University .
;+
; PROJECT:
;       ADAS
;
; NAME:
;       CONFIG_STR2ARR
;
; PURPOSE: construct configuration of electrons from configuration strings
;          configuration array is ordered along n,l-shells
;
;
;   conf[0,k,j] = n-number of shell k of level j
;   conf[1,k,j] = l-number of shell k of level j
;   conf[2,k,j] = number of electrons in that shell of level j
;
; input:   csrtg    string array with one string for each level
;                   (example: '1S2 2S1 2P1')
;          nlmax    max. number of nl-shells
;
; WRITTEN:
;       Ralph Dux
;
; MODIFIED:
;       1.1     Ralph Dux
;               First release
;       1.2     Martin O'Mullane
;                - Use square brackets for arrays.
;                - Replace (obsolete) str_sep with strsplit.
;       1.3     Martin O'Mullane
;                - Allow l as far as 'H'.
;
; VERSION:
;       1.1     08-09-1999
;       1.2     27-08-2009
;       1.3     22-10-2009
;
;-
;-----------------------------------------------------------------------------

function config_str2arr, cstrg


;constants
name_shell = ['S','P','D','F','G', 'H']

;number of configuration strings
nn = n_elements(cstrg)

;maximum number of shells
nlmax = 6

;prepare arrays
conf = intarr(3, nlmax, nn)
dum1 = intarr(3, nlmax)
dum2 = intarr(3, nlmax)

for i=0, nn-1 do begin

;reset arrays
   dum1[*, *] = 0
   dum2[*, *] = 0

;the configuration string we are working on
   string = cstrg[i]

;cut string into sub strings: seperator is ' '
   str =  strsplit(string, ' ', /extract)

;substrings need to have three characters
   wh = where( strlen(str) eq 3, nc)

;if we found such strings ...
   if nc ge 1 then begin
      str = str[wh]

; we fill up the array
      for ic=0,nc-1 do begin

;n-number
         dum1[0,ic] = fix(strmid(str[ic],0,1))

;l-number
         dum1[1,ic] = ( where(name_shell eq strmid(str[ic],1,1)))(0)

;occupaton
         dum1[2,ic] = fix(strmid(str[ic],2,1))
      endfor
   endif

;number of occupied shells
   wh = where(dum1[0,*] ne 0, k_tot)

; now we sort the string in ascending order of n and l
   k_used = 0
   for n=1,7 do begin
      for l=0,n-1 do begin
         wh = where( dum1[0,*] eq n and dum1[1,*] eq l, n_wh)
         if (n_wh eq 1) then begin
            dum2[*,k_used] = dum1[*,wh[0]]
            k_used = k_used + 1
            if k_used eq k_tot then goto, sort_finished
         endif
      endfor
   endfor

sort_finished:
   conf[*, *, i] = dum2
endfor

return,conf

end
