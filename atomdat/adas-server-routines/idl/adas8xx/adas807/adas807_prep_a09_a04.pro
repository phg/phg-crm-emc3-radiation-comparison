; Copyright (c) 2000 Strathclyde University .
;+
; PROJECT:
;       ADAS
;
; NAME:
;       prep_a09_a04
;
; PURPOSE: Compare energy levels of adf04 and adf09 files and prepare
;          adf18/a09_a04 file
;
; WRITTEN:
;       Ralph Dux
;
; MODIFIED:
;       1.1     Ralph Dux
;                - First release
;       1.2     Martin O'Mullane
;                - added outfile
;       1.3     Martin O'Mullane
;                - Renamed from prep_a04_a09 to prep_a09_a04.
;       1.4     Martin O'Mullane
;                - Another rename to adas807_prep_a09_a04.
;                - Use square brackets for arrays.
;                - Write out filenames if ADASCENT/ADASUSER is not
;                  part of the name.
;       1.5     Martin O'Mullane
;                 - Fix logic when writing out filenames which are
;                   in ADASCENT or ADASUSER.
;
; VERSION:
;       1.1     18-11-1998
;       1.2     15-08-2000
;       1.3     06-02-2003
;       1.4     27-08-2009
;       1.5     03-09-2009
;
;-----------------------------------------------------------------------
pro adas807_prep_a09_a04, outfile, adf09_files, adf04_file,           $
                  iz_4, nz_4, n_4, ia_4, ila_4, isa_4, xja_4, conf_4


;short configuration strings from arrays
cstrg_4 =  config_arr2str(conf_4)

;level description for output
level_4 = strarr(n_4)
for i=0,n_4-1 do begin
   level_4[i] = cstrg_4[i]+' '+string(isa_4[i],form='(i1)')+l_value(ila_4[i])+j_value(xja_4[i])
endfor

;number of elements in configuration array
nconf_tot =  n_elements(conf_4[*, *, 0])


nfiles = n_elements(adf09_files)

;prepare output arrays
ia_out = intarr(n_4, nfiles)
str_out = strarr(n_4, nfiles)

for k= 0,  nfiles-1 do begin

   xxdata_09, file=adf09_files[k], fulldata=all

   iz_9  = all.iz
   nz_9  = all.iz0
   ia_9  = all.ia
   isa_9 = all.isa
   ila_9 = all.ila
   xja_9 = all.xja
   wa_9  = all.wa
   
   n_9     = n_elements(ia_9)
   cstrg_9 = strarr(n_9)

   for  j = 0, n_9-1 do begin
      xxprs3, config=all.cstrga[j], z_nuc=nz_9, z_ion=iz_9, occup=occ
      cstrg_9[j] = occ2cfg(occ, /upper)
   endfor

;consistency check
   if (nz_4 ne nz_9) then begin
      print, 'adf04:', adf04_file
      print, 'adf09:', adf09_files[k]
      message, 'nuclear charge of adf04-file and adf09-file are not equal'
   endif
   if (iz_4 ne iz_9) then begin
      print, 'adf04:', adf04_file
      print, 'adf09:', adf09_files[k]
      message, 'ion charge of adf04-file and adf09-file are not equal'
   endif

;configuration for levels in adf09-file
   conf_9 =  config_str2arr(cstrg_9)

;short configuration strings from arrays
   cstrg_9 =  config_arr2str(conf_9)

;level description for output
   level_9 = strarr(n_9)
   for i=0,n_9-1 do begin
      level_9[i] = cstrg_9[i]+' '+string(isa_9[i],form='(i1)')+l_value(ila_9[i])+j_value(xja_9[i])
   endfor


;usage of levels
   not_used = intarr(n_9)+1

;go through all levels of adf04 file
for i4=0,n_4-1 do begin
      match = intarr(n_9)
      for i9=0,n_9-1 do begin
         match[i9] = ( total( conf_4[*, *, i4] eq conf_9[*, *, i9] ) eq nconf_tot ) $
            and (isa_4[i4] eq isa_9[i9] ) $
            and (ila_4[i4] eq ila_9[i9] ) $
            and (xja_4[i4] eq xja_9[i9] )
      endfor

;How many levels match?
      dummy = where(match, nmatch)

;no match
      if  nmatch lt 1 then begin
         ia_out[i4, k] = 0
         str_out[i4, k] =  '*** no match **** | '
      endif

;the same level was found
      if nmatch ge 1 then begin

;Was this level already matched to a previous adf04 level?
         whtake = where(match and not_used, ntake)

;all levels that match are used
         if  ntake lt 1 then begin
            ia_out[i4, k] =  0
            str_out[i4, k] =  '*** used **** | '
         endif

;not all level that match are used
         if  ntake ge 1 then begin

;find first level that was not used so far
            i9_min = min(ia_9[whtake])-1
            not_used[i9_min] = 0
            ia_out[i4, k] =  ia_9[i9_min]
            str_out[i4, k] =  level_9[i9_min]+ ' | '
            if nmatch gt 1 then str_out[i4, k] =  level_9[i9_min] + '* | '
         endif
      endif
   endfor
endfor



; Output - need to know source of the file

cpath = getenv('ADASCENT')
upath = getenv('ADASUSER')

;open file for output

openw, lun, outfile, /get_lun


printf, lun, f='(a)', 'Specific ion input file'
printf, lun, f='(a)', '-----------------------'
pos = strpos(adf04_file,'/adf04')
str = strmid(adf04_file,pos)

p_str = strmid(adf04_file,0,pos)

if p_str EQ upath then $
   printf, lun, '"ADASUSER"',str, form='(a,a,"   : specific ion file")'
if p_str EQ cpath then $
   printf, lun, '"ADASCENT"',str, form='(a,a,"   : specific ion file")'
if p_str NE upath and p_str NE cpath then $
   printf, lun, adf04_file, form='(a,"   : specific ion file")' 

printf, lun, f='(a)', ' '
printf, lun, f='(a)', 'Badnell dielectronic files'
printf, lun, f='(a)', '--------------------------'
str2 =  ['1st', '2nd', '3rd', '4th', '5th']
for k=0, nfiles-1 do begin
   pos   = strpos(adf09_files[k],'/adf09')
   str   = strmid(adf09_files[k],pos)
   p_str = strmid(adf09_files[k],0,pos)
   if p_str EQ upath then $
      printf, lun, '"ADASUSER"',str, str2[k], form='(a,a," : ",a,". Badnell file")'
   if p_str EQ cpath then $
      printf, lun, '"ADASCENT"',str, str2[k], form='(a,a," : ",a,". Badnell file")'
   if p_str NE upath and p_str NE cpath then $
      printf, lun, adf09_files[k], str2[k], form='(a," : ",a,". Badnell file")'
endfor
printf, lun, f='(a)', ' '
printf, lun, f='(a)', 'Output files'
printf, lun, f='(a)', '------------'
printf, lun, f='(a)','"ADASUSER"/pass/postllev.pass       : supplemented spec. ion file'
printf, lun, f='(a)','"ADASUSER"/pass/postllev.pass1      : dielectronic data for MAINCL codes'
printf, lun, f='(a)', ' '
printf, lun, f='(a)', 'Level cross-reference lists for specific ion and badnell files'
printf, lun, f='(a)', '---------------------------------------------------------------'
printf, lun, f='(a)', ' '
printf, lun, f='(a)', '    sp.        bd1.        bd2.        bd3.        bd4.        bd5.        bd6.'
printf, lun, f='(a)', '    ---        ----        ----        ----        ----        ----        ----'
for i4=0, n_4-1  do begin
   printf, lun, ia_4[i4], ia_out[i4, *], f='(3x,i3,6(8x,i4))'
endfor
printf, lun, f='(a)', ' '



; Tag on some comments

; report on the matchs

printf, lun, 'C--------------------------------------------------------------------'

str = 'C  #  specific ion            '
for k = 0, nfiles-1 do str = str + '         Badnell ' +   $
                                 string(k+1, format='(i1)') + '           '
printf, lun, str
for i4=0, n_4-1 do begin
   printf, lun, 'C ', ia_4[i4], level_4[i4], str_out[i4, 0:nfiles-1], f='(a2,i3,a30,"-->",6(a30))'
endfor
printf, lun, f='(a)', 'C--------------------------------------------------------------------'

; and who is responsible

date = xxdate()
name = xxuser()

printf,lun, ' '
printf,lun, 'C-----------------------------------------------------------------'
printf,lun, 'C'
printf,lun, 'C  ADF04 FILE  : '
printf,lun, 'C     '+ adf04_file
printf,lun, 'C'
printf,lun, 'C  ADF09 FILE(S) FOR RECOMBINED ION: '
for j=0, nfiles-1 do printf, lun, 'C     ' + adf09_files[j]
printf,lun, 'C'
printf,lun, 'C'
printf,lun, 'C  Code     : ADAS807'
printf,lun, 'C  Producer : ',name[0]
printf,lun, 'C  Date     : ',date[0]
printf,lun, 'C'
printf,lun, 'C-----------------------------------------------------------------'


close,lun
free_lun,lun


end
