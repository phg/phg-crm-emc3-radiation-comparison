; Copyright (c) 2000 Strathclyde University .
;+
; PROJECT:
;       ADAS 
;
; NAME:
;       ADAS807_IN
;
; PURPOSE: 
;       This function puts up a widget asking for two input files for
;       the adas_driver program of Ralph Dux. Pairs of ionised/ionising
;       or recombined/recombining adf04 files are reuested.
;
;
; EXPLANATION:
;       Similar to ADAS407 input screen but without the automatic 
;       selection of the second file.       
;     
;
; NOTES:
;       Based on the techniques of ADAS403. It entirely in IDL and uses
;       v5 (and above) features such as pointers to pass data.
; 
;
;
; INPUTS:
;        inval       - A structure holding the 'remembered' input data.
;                        EROOTPATH     : root file 1
;                        EFILE         : filename 1
;                        ECENTROOT     : central adas of root 1
;                        EUSERROOT     : user path of file 1
;                        CROOTPATH     :
;                        CFILE         :  file 2!
;                        CCENTROOT     :
;                        CUSERROOT     :
; OPTIONAL INPUTS:
;       None
;
; OUTPUTS:
;       The function returns a string rep
;           rep = 'CONTINUE' if everything is ok
;           rep = 'CANCEL' if there is a problem or indeed if the user
;                          closes the widget with the mouse. 
;
; OPTIONAL OUTPUTS:
;       None
;
; KEYWORD PARAMETERS:
;       FONT_LARGE - The name of a larger font e.g 'courier_bold14'
;
;       FONT_SMALL - The name of a smaller font e.g 'courier_bold12'
;
;
; CALLS:
;                            
;       ADAS807_IN_FILEA     : Reacts to first file widget and sensitises
;                              'Done' and 'Browse' if 2 valid files have
;                              been selected.
;       ADAS807_IN_FILEB     : Ditto for second file.
;       ADAS807_IN_EVENT     : Reacts to Cancel, Done and Browse.
;
; SIDE EFFECTS:
;
; CATEGORY:
;       Widgets
;
; WRITTEN:
;       Martin O'Mullane
;
; MODIFIED:
;       1.1     Martin O'Mullane
;                - First release 
;       1.1     Martin O'Mullane
;                - Replace h7_getz by call to read_adf04
;
; VERSION:
;       1.1     12-10-99
;       1.2     24-08-2009
;
;-
;-----------------------------------------------------------------------------


FUNCTION ADAS807_IN_FILEA, event

   ; Sensitise 'Done' and 'Browse' if we are ready to continue 

   Widget_Control, event.top, Get_UValue=info
     
   Widget_Control, info.flbID, Get_Value=fileb

   filename = fileb.rootpath + fileb.file
   file_acc, filename, exist, read, write, execute, filetype
     
   
   if event.action EQ 'newfile' AND filetype EQ '-' then begin
      Widget_Control, info.doneID, sensitive=1
      Widget_Control, info.browseID, sensitive=1
   endif else begin
      Widget_Control, info.doneID, sensitive=0
      Widget_Control, info.browseID, sensitive=0
   endelse
   
   RETURN,0

END 

;-----------------------------------------------------------------------------

FUNCTION ADAS807_IN_FILEB, event

   ; Sensitise 'Done' and 'Browse' if we are ready to continue 

   Widget_Control, event.top, Get_UValue=info
     
   Widget_Control, info.flaID, Get_Value=filea
   
   filename = filea.rootpath + filea.file
   file_acc, filename, exist, read, write, execute, filetype
   
   if event.action EQ 'newfile' AND filetype EQ '-' then begin
      Widget_Control, info.doneID, sensitive=1
      Widget_Control, info.browseID, sensitive=1
   endif else begin
      Widget_Control, info.doneID, sensitive=0
      Widget_Control, info.browseID, sensitive=0
   endelse
   
   RETURN,0

END 

;-----------------------------------------------------------------------------


PRO ADAS807_IN_EVENT, event

   ; Get the info structure out of the user value of the top-level base.

Widget_Control, event.top, Get_UValue=info


; Something nefarious may cause an error also.
;  -- if so trap it and exit gracefully

CATCH, error
IF error NE 0 THEN BEGIN
   formdata = {cancel:1}
   *info.ptrToFormData = formdata
   Widget_Control, event.top, /Destroy
   message,!err_string
   RETURN
ENDIF

Widget_Control, event.id, Get_Value=userEvent


CASE userEvent OF

  'Cancel' : begin
               formdata = {cancel:1}
               *info.ptrToFormData =formdata
               widget_Control, event.top, /destroy
             end 

      
  'Done'   : begin
                 
                ; gather the data for return to calling program
                ; 'Done' is not sensitised until valid filenames
                ; have been chosen.
               
                widget_Control, info.flaID, Get_Value=vala
                widget_Control, info.flbID, Get_Value=valb
                
                ; We must do some consistency checks on the adf04 
                ; files to check for same Z
                
                err  = 0
                mess = ''
                
                file_a = vala.rootpath + vala.file
                read_adf04, file=file_a, fulldata=all
                iza = all.iz
                nza = all.iz0
                
                file_p = valb.rootpath + valb.file
                read_adf04, file=file_p, fulldata=all
                izp = all.iz
                nzp = all.iz0
                
                if (nza NE nzp) then begin
                   mess = 'These are not the same element'
                   err = 1
                endif
                if err EQ 0 AND iza NE izp-1 then begin
                   mess = 'Charge of recombined is not 1 less than charge of recombinig ion'
                   err = 1
                endif
               
                if err EQ 0 then begin
                   formdata = {vala    : vala,       $
                               valb    : valb,       $
                               cancel   : 0          }
                    *info.ptrToFormData = formdata
                    widget_control, event.top, /destroy
                endif else begin
                   widget_Control, info.messID, Set_Value=mess
                endelse

             end
             
    'Browse Comments' : Begin
    
                Widget_Control, info.flaID, get_value=val
                filename = val.rootpath + val.file
                xxtext, filename, font=info.font_large
                Widget_Control, info.flbID, get_value=val
                filename = val.rootpath + val.file
                xxtext, filename, font=info.font_large
    
             end        
  else : print,'ADAS807_IN_EVENT : You should not see this message! ',userEvent
                   
ENDCASE

END
  
;-----------------------------------------------------------------------------


FUNCTION adas807_in, inval, dataclasses, $
                     FONT_LARGE = font_large, FONT_SMALL = font_small


                ;**** Set defaults for keywords ****

  IF n_elements(font_large) eq 0 THEN font_large = ''
  IF n_elements(font_small) eq 0 THEN font_small = ''


; Path names may be supplied with or without the trailing '/'.  Add
; this character where required so that USERROOT will always end in
; '/' on output.
  
  if strtrim(inval.erootpath) eq '' then begin
      inval.erootpath = './'
  endif else if                                                   $
  strmid(inval.erootpath, strlen(inval.erootpath)-1,1) ne '/' then begin
      inval.erootpath = inval.erootpath+'/'
  endif
  if strtrim(inval.crootpath) eq '' then begin
      inval.crootpath = './'
  endif else if                                                   $
  strmid(inval.crootpath, strlen(inval.crootpath)-1,1) ne '/' then begin
      inval.crootpath = inval.crootpath+'/'
  endif


 
                ;********************************************
                ;**** create modal top level base widget ****
                ;********************************************
                
  parent = Widget_Base(Column=1, Title='ADAS 807 INPUT', $
                       XOFFSET=100, YOFFSET=1)

                
  rc = widget_label(parent,value='  ',font=font_large)


  base  = widget_base(parent, /column)
  
  
                ;*****************************************
                ;**** Ionising file selection widget ****
                ;*****************************************

  flabase = widget_base(base, /column, /frame)
  flaval = {  ROOTPATH        :       inval.erootpath,                $
              FILE            :       inval.efile,                    $
              CENTROOT        :       inval.ecentroot,                $
              USERROOT        :       inval.euserroot                 }
  flatitle = widget_label(flabase, font=font_large,                   $
                          value='Ionising Ion File Details:-')
  flaID = cw_adas_infile(flabase, value=flaval, font=font_small, $
                         ysize = 5 ,                             $
                         event_funct='ADAS807_IN_FILEA')

              ;***********************************************
              ;**** Ionised file selection widget ****
              ;***********************************************

  flbbase = widget_base(base, /column, /frame)
  flbval = {  ROOTPATH        :       inval.crootpath,                $
              FILE            :       inval.cfile,                    $
              CENTROOT        :       inval.ccentroot,                $
              USERROOT        :       inval.cuserroot                 }
  flbtitle = widget_label(flbbase, font=font_large,                   $
                          value='Ionised Ion File Details:-')
  flbID = cw_adas_infile(flbbase, value=flbval, font=font_small, $
                         ysize = 5 ,                             $
                         event_funct='ADAS807_IN_FILEB')



                            
                ;*****************
                ;**** Buttons ****
                ;*****************
                
  mrow     = widget_base(parent,/row,/align_center)
  messID   = widget_label(mrow,font=font_large, $
                          value='          Enter File information             ')
                
  mrow     = widget_base(parent,/row)
  
  browseID = widget_button(mrow, value='Browse Comments', font=font_large)
  cancelID = widget_button(mrow,value='Cancel',font=font_large)
  doneID   = widget_button(mrow,value='Done',font=font_large)




; Initial settings 

                ;*************************************************
                ;**** Check filenames and desensitise buttons ****
                ;**** if it is a directory or it is a file    ****
                ;**** without read access.                    ****
                ;*************************************************

    browseallow = 0
    filename = inval.erootpath + inval.efile
    file_acc, filename, fileexist, read, write, execute, filetype
    if filetype ne '-' then begin
        Widget_Control, browseID, sensitive=0
        Widget_Control, doneID, sensitive=0
    endif else begin
        if read eq 0 then begin
            Widget_Control, browseID, sensitive=0
            Widget_Control, doneID, sensitive=0
        endif else begin
            browseallow = 1
        endelse
    endelse
    filename = inval.crootpath + inval.cfile
    file_acc, filename, fileexist, read, write, execute, filetype
    if filetype ne '-' then begin
        if browseallow eq 0 then Widget_Control, browseID, sensitive=0
        Widget_Control, doneID, sensitive=0
    endif else begin
        if read eq 0 then begin
            if browseallow eq 0 then Widget_Control, browseID, sensitive=0
            Widget_Control, doneID, sensitive=0
        endif
    endelse




; Realize the ADAS807 input widget.

   widget_Control, parent, /realize

; Create a pointer to store the information collected from the form.
; The initial data stored here is set to CANCEL, so nothing needs to
; be done if the user kills the widget with the mouse.

   ptrToFormData = Ptr_New({cancel:1})

; Create an info structure with program information.

  info = { flaID           :  flaID,              $
           flbID           :  flbID,              $
           doneID          :  doneID,             $
           browseID        :  browseID,           $
           messID          :  messID,             $
           font_large      :  font_large,         $
           ptrToFormData   :  ptrToFormData       }  
            
               
; Store the info structure in the user value of the top-level base and launch
; the widget into the world.

  widget_control, parent, Set_UValue=info

  XManager, 'adas807_in', parent, Event_Handler='ADAS807_IN_EVENT'


; When the widget is destroyed, the block is released, and we
; return here. Get the data that was collected by the widget
; and stored in the pointer location. Finally free the pointer.

formdata = *ptrToFormData

rep='CONTINUE'
IF N_Elements(formdata) EQ 0 THEN BEGIN
   Ptr_Free, ptrToFormData
   rep ='CANCEL'
ENDIF
 
IF formdata.cancel EQ 1 THEN BEGIN
   Ptr_Free, ptrToFormData
   rep ='CANCEL'
ENDIF

if rep eq 'CONTINUE' then begin
   inval.erootpath   = formdata.vala.rootpath
   inval.efile       = formdata.vala.file
   inval.ecentroot   = formdata.vala.centroot
   inval.euserroot   = formdata.vala.userroot
   inval.crootpath   = formdata.valb.rootpath
   inval.cfile       = formdata.valb.file
   inval.ccentroot   = formdata.valb.centroot
   inval.cuserroot   = formdata.valb.userroot
   Ptr_Free, ptrToFormData
endif       



RETURN,rep

END
