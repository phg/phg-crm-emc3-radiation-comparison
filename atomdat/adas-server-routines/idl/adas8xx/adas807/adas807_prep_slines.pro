; Copyright (c) 2000 Strathclyde University .
;+
; PROJECT:
;       ADAS
;
; NAME:
;       ADAS807_PREP_SLINES
;
; PURPOSE: Calculate the ionisation rates (S-lines) for adf04 files.
;
;
; WRITTEN:
;       Martin O'Mullane
;
; MODIFIED:
;       1.1     Martin O'Mullane
;                - First release
;       1.2     Martin O'Mullane
;                - Rename to adas807_prep_slines.
;       1.3     Martin O'Mullane
;                - Changed rstrpos to strpos with /reverse_search.
;
; VERSION:
;       1.1     22-08-2000
;       1.2     27-08-2009
;       1.3     17-10-2018
;
;-
;-----------------------------------------------------------------------------
PRO adas807_prep_slines, outfile,                     $
                 adf04_filea, adf07_file, seq, scef,  $
                 nz_a, iz_a, iz_p, wv_a, wv_p, bwno


; To find which blocks are in the adf07 file constuct a search string
; based on ICODE and FCODE

metastable, seq=seq, term=term

n_icode = n_elements(term)
n_fcode = n_elements(wv_p)

elem = xxesym(nz_a)

search_str = 'LINE 1'
for j = 0, n_icode-1 do begin
   for k = 0, n_fcode-1 do begin

      str = elem + '+' + string(iz_a, format='(i3)') + '/' + $
            elem + '+' + string(iz_p, format='(i3)') + '/' + $
            'icode=' + string(j+1, format='(i3)') + '/' + $
            'fcode=' + string(k+1, format='(i3)') + '/'
      str =  strupcase(strcompress(str,/remove_all))

      search_str = [search_str, str]

   endfor
endfor
search_str   = search_str[1:*]
n_search_str = n_elements(search_str)


; now read in the lines in the adf07 file with 'ISEL='.

headers = 'LINE1'
str     = ''
openr, lun, adf07_file, /get_lun

while not EOF(lun) do begin

  readf, lun, str
  str1 = strupcase(strcompress(str,/remove_all))
  if strpos(str1, 'ISEL=') NE -1 then headers = [headers, str]

endwhile
headers   = headers[1:*]
n_headers = n_elements(headers)


; remove the quantity and IP fields and compress

for j=0,n_headers-1 do begin

   ip_pos = strpos(headers[j], '/I.P.')
   sl_pos = strpos(headers[j], '/', ip_pos, /reverse_search)
   sr_pos = strpos(headers[j], '/',ip_pos+1)
   r_pos  = strpos(headers[j], 'ISEL')
   len    = r_pos - sr_pos - 1

   headers[j] = strmid(headers[j],0,sl_pos+1) + strmid(headers[j],sr_pos+1,len)
   headers[j] = strupcase(strcompress(headers[j],/remove_all))

endfor

; Now find the indices

isel_ind = intarr(n_search_str)
for j=0,n_search_str-1 do isel_ind[j] = where(headers EQ search_str[j])

; convert to starting with 1.

isel_ind = isel_ind + 1

free_lun, lun




; Construct the S-lines

e_next = wv_p + bwno
e_ion  = dblarr(n_icode)
for j = 0, n_icode-1 do e_ion[j] = wv_a[j]

openw, lun, outfile, /get_lun


tev = scef/11605.0

comment = 'LINE1'
ind = -1
for j = 0, n_icode-1 do begin
   for k = 0, n_fcode-1 do begin

      ind = ind + 1
      if isel_ind[ind] NE 0 then begin   ; -1+1 = 0!!

         read_adf07,file=adf07_file,te=tev,data=data,block=isel_ind[ind]

         if total(data) GT 1.0e-40 then begin

            energy = e_next(k) - e_ion(j)
            data   = data * exp(energy/(8065.541*tev))
            data   = data > 1e-40

            str = string(data,format='(14e9.2,:)')
            parts = strsplit(str,'e',/extract)

            str_pts = ''
            for i=0, n_elements(parts)-1 do str_pts = str_pts + parts[i]
            str_ind = 'S' + string(j+1, format='(i3)') + '  +' +$
                           string(k+1, format='(i1)') + '        '
            printf,lun,str_ind+str_pts

            str     = 'C    ' + strtrim(str_ind) + '   ISEL = ' +     $
                      string(isel_ind[ind], format='(i4)') + $
                      '   Energy = ' + string(energy, format='(f12.2)')
            comment = [comment, str]

         endif

      endif

   endfor
endfor
if n_elements(comment) GT 1 then comment = comment[1:*] else comment = 'C'



; Comments

date = xxdate()
name = xxuser()

printf,lun, ' '
printf,lun, 'C-----------------------------------------------------------------'
printf,lun, 'C'
printf,lun, 'C  Added ionisation rates (S-lines) from :
printf,lun, 'C      '+ adf07_file
printf,lun, 'C'
for j = 0, n_elements(comment)-1 do printf,lun, comment[j]
printf,lun, 'C'
printf,lun, 'C  Code     : ADAS807'
printf,lun, 'C  Producer : ',name(0)
printf,lun, 'C  Date     : ',date(0)
printf,lun, 'C'
printf,lun, 'C-----------------------------------------------------------------'

close, lun
free_lun, lun




END
