; Copyright (c) 2000 Strathclyde University .
;+
; PROJECT:
;       ADAS 
;
; NAME:
;	ADAS807_OUT
;
; PURPOSE: 
;       This function does the actual work. Initially it puts up a widget 
;       asking for the various output files
;             - ionisation pathways
;             - ionisation rates (S-lines)
;             - adf08 driver for radiative recombination
;             - adf09 to adf04 mapping for dielectronic recombination
;             - adf17 to adf04 projection mapping
;
;
; EXPLANATION:
;       The user selects output files with suitable defaults available.
;       Defaults are of the form adas807_adfXX.pass variety.
;
;
; NOTES:
;       If the DR option is chosen the user is asked to select the 
;       appropriate adf09 files. A adas_in file selection box is popped up;
;       note that multiple selections are allowed here. These are not stored.
; 
;
;
; INPUTS:
;        inval   - Structure holding adf04 file names
;        outval  - Structure of output files,
;        procval - Processing structure, holds metastable inmformation,
;        data_a  - Ionising ion data,
;        data_p  - Recombining ion data,          $
;        header  - Output header; version, date etc.
;        bitfile - Directory of bitmaps for menu button
;
;
; OPTIONAL INPUTS:
;       None
;
; OUTPUTS:
;       The function returns a string rep
;           rep = 'CONTINUE' if everything is ok
;           rep = 'CANCEL' if there is a problem or indeed if the user
;                          closes the widget with the mouse. 
;           rep = 'MENU' if user want to exit to menu at this point
;
; OPTIONAL OUTPUTS:
;       None
;
; KEYWORD PARAMETERS:
;       FONT_LARGE - The name of a larger font e.g 'courier_bold14'
;
;       FONT_SMALL - The name of a smaller font e.g 'courier_bold12'
;
;
; CALLS:
;                            
;       ADAS807_OUT_MENU   : React to menu button event. The standard IDL
;                            reaction to button events cannot deal with
;                            pixmapped buttons. Hence the special handler.
;       ADAS807_OUT_EVENT  : Reacts to cancel and Done. Does the file
;                            existence error checking.
;       ADAS807_POP_EVENT  : Reacts to S-line and adf09 mapping buttons
;                            by immediately asking for the adf07/adf09 files.
;       RECOM_PATH         : Finds recombination pathways                   
;       PREP_A17_P208      : adf17 ro ADAS208 mapping.                   
;       PREP_ADF08         : Prepares adf08 files for ADAS211.            
;       ADD_ION_ADF04      : Produces adf04 configurations with ionisation
;                            pathways.
;       PREP_SLINES        : Calculates ionisation rates (S-lines).      
;       PREP_A04_A09       : Maps adf09 data onto adf04 levels for ADAS212.
;
; SIDE EFFECTS:
;
; CATEGORY:
;	Widgets
;
; WRITTEN:
;       Martin O'Mullane
;
; MODIFIED:
;	1.1	Martin O'Mullane
;		First release 
;	1.2	Martin O'Mullane
;		Finally added adf09 to adf04 mapping. Introduced an output
;               option to generate the S-lines in the adf04 file. Also
;               write the ionisation potentials along with the ionisation.
;               pathways. Improved documentation.
;       1.3     Martin O'Mullane
;               Correct misnamed adf18/a09_a04 file (from a04_a09).
;               Calls a renamed prep_a09_a04 routine.
;       1.4     Martin O'Mullane
;                - Use square brackets for arrays.
;                - Prefix supplementary routines with adas807_.
;
; VERSION:
;       1.1	16-08-1999
;       1.2	25-08-2000
;       1.3     06-02-2003
;       1.4     24-08-2009
;
;-
;-----------------------------------------------------------------------------



PRO ADAS807_OUT_MENU, event

; The 'normal' get_value from a widget_button does not work with a
; bitmap button. Hence this specialised event handler.

   ; Get the info structure out of the user value of the top-level base.

Widget_Control, event.top, Get_UValue=info


; Something nefarious may cause an error also.
;  -- if so trap it and exit gracefully

CATCH, error
IF error NE 0 THEN BEGIN
   formdata = {cancel : 0, menu:1}
   *info.ptrToFormData = formdata
   Widget_Control, event.top, /Destroy
   message,!err_string
   RETURN
ENDIF


 formdata = {cancel : 0, menu:1}
 *info.ptrToFormData =formdata
 widget_Control, event.top, /destroy

END


;-----------------------------------------------------------------------------


FUNCTION ADAS807_POP_EVENT, event

; React to sline and adf09 selections - pop up a file entry dialog
; when these output options are activated.


; Get the info structure out of the user value of the top-level base.

if event.activate EQ 1 AND event.action EQ 1 then begin
  
  widget_control, event.top, get_uvalue = state

  if event.handler EQ state.slnID then begin
  
     tmp_fd  = *state.ptrToFiles
    
     val_a07 = { rootpath : state.procval.a07path,  $
                 file     : state.procval.a07file,  $
                 centroot : state.procval.a07cent,  $
                 userroot : state.procval.a07user   }

     adas_in_v2, val_a07, action, font_large = state.font,      $
                 wintitle='Ionisation rate file selection',     $
                 title='Select ionisation rate (adf07) file:-', $
                 parent=state.parent

     filedata = { a07path : val_a07.rootpath,  $
                  a07file : val_a07.file,      $
                  a07cent : val_a07.centroot,  $
                  a07user : val_a07.userroot,  $
                  a09path : tmp_fd.a09path,    $
                  a09file : tmp_fd.a09file,    $
                  a09cent : tmp_fd.a09cent,    $
                  a09user : tmp_fd.a09user     }

     *state.ptrToFiles = filedata

     ; if we cancel the popup then de-activate the selection - force
     ; the user to re-select the option.

     if action EQ 'Cancel' then begin

        Widget_Control, state.slnID, get_value = value
        value.outbut = 0
        Widget_Control, state.slnID, set_value = value

     endif 
     
  endif 
  
  
  if event.handler EQ state.a09ID then begin
  
  
    tmp_fd  = *state.ptrToFiles
    
    val_a09 = { rootpath : state.procval.a09path,  $
                 file     : '',  $
                 centroot : state.procval.a09cent,  $
                 userroot : state.procval.a09user   }

     adas_in_v2, val_a09, action, font_large = state.font, $
                 wintitle='DR file selection',             $
                 title='Select DR (adf09) files:-', /many, $
                 parent=state.parent

     filedata = { a07path : tmp_fd.a07path,    $
                  a07file : tmp_fd.a07file,    $
                  a07cent : tmp_fd.a07cent,    $
                  a07user : tmp_fd.a07user,    $
                  a09path : val_a09.rootpath,  $
                  a09file : val_a09.file,      $
                  a09cent : val_a09.centroot,  $
                  a09user : val_a09.userroot   }

     *state.ptrToFiles = filedata

     ; if we cancel the popup then de-activate the selection - force
     ; the user to re-select the option.

     if action EQ 'Cancel' then begin

        Widget_Control, state.a09ID, get_value = value
        value.outbut = 0
        Widget_Control, state.a09ID, set_value = value

     endif 
     
  
  endif
  
endif

return, 0L

END



;-----------------------------------------------------------------------------


PRO ADAS807_OUT_EVENT, event

; React to button events - Cancel and Done but not menu (this requires a
; specialised event handler ADAS807_OUT_MENU). Also deal with the passing
; directory output Default button here.

; On pressing Done check for the following


   ; Get the info structure out of the user value of the top-level base.

Widget_Control, event.top, Get_UValue=info


Widget_Control, event.id, Get_Value=userEvent

CASE userEvent OF


  'Cancel' : begin
               formdata = {cancel:1, menu : 0}
               *info.ptrToFormData =formdata
               widget_Control, event.top, /destroy
             end 

      
  'Done'   : begin
                 
                ; gather the data for return to calling program
                
                err  = 0
                mess = ''
                
                widget_Control, info.paperID, Get_Value=pap
                widget_Control, info.ionID, Get_Value=ion
                widget_Control, info.slnID, Get_Value=sln
                widget_Control, info.a08ID, Get_Value=a08
                widget_Control, info.a09ID, Get_Value=a09
                widget_Control, info.a17ID, Get_Value=a17
                
                ; no need to set mess as it is flagged in the cw
                if pap.outbut EQ 1 AND strtrim(pap.message) NE '' then err=1
                if ion.outbut EQ 1 AND strtrim(ion.message) NE '' then err=1
                if sln.outbut EQ 1 AND strtrim(sln.message) NE '' then err=1
                if a08.outbut EQ 1 AND strtrim(a08.message) NE '' then err=1
                if a09.outbut EQ 1 AND strtrim(a09.message) NE '' then err=1
                if a17.outbut EQ 1 AND strtrim(a17.message) NE '' then err=1
                
                
                ; With the adf07 and adf09 selections we need to put
                ; data into formdata even if they are not chosen.
                ; But check for completely empty case of a no 
                ; defaults initial case!
                
                tmp_st  = *info.ptrToFiles
                
                if n_tags(tmp_st) GT 1 then begin
                
                   val_a07 = { rootpath : tmp_st.a07path,  $
                               file     : tmp_st.a07file,  $
                               centroot : tmp_st.a07cent,  $
                               userroot : tmp_st.a07user   }
                   if err NE 1 AND                  $
                      sln.outbut EQ 1 AND           $
                      strtrim(sln.message) EQ '' then begin
                         a07file = tmp_st.a07path + tmp_st.a07file 
                   endif else begin
                      a07file = ''
                   endelse 


                   val_a09 = { rootpath : tmp_st.a09path,  $
                               file     : tmp_st.a09file,  $
                               centroot : tmp_st.a09cent,  $
                               userroot : tmp_st.a09user   }
                   if err NE 1 AND                  $
                      a09.outbut EQ 1 AND           $
                      strtrim(a09.message) EQ '' then begin

                         n_file = n_elements(tmp_st.a09file)
                         a09files = strarr(n_file)
                         for j = 0, n_file-1 do a09files[j] = tmp_st.a09path + tmp_st.a09file[j]

                   endif else begin
                      a09files = ''
                   endelse 
                   
                endif else begin
                   val_a07 = { rootpath : info.procval.a07path,  $
                               file     : info.procval.a07file,  $
                               centroot : info.procval.a07cent,  $
                               userroot : info.procval.a07user   }
                   val_a09 = { rootpath : info.procval.a09path,  $
                               file     : info.procval.a09file,  $
                               centroot : info.procval.a09cent,  $
                               userroot : info.procval.a09user   }
                   a07file = ''
                   a09files = ''
                endelse

                               
                if err EQ 0 then begin
                   formdata = { paper    : pap,      $
                                ion      : ion,      $
                                sln      : sln,      $
                                a08      : a08,      $
                                a09      : a09,      $
                                a17      : a17,      $
                                a09files : a09files, $
                                a07file  : a07file,  $
                                val_a07  : val_a07,  $
                                val_a09  : val_a09,  $
                                cancel   : 0,        $
                                menu     : 0         }
                   *info.ptrToFormData = formdata
                   widget_control, event.top, /destroy
                endif else begin
                   widget_Control, info.messID, Set_Value=mess
                endelse

             end
             
             
  else : print,'ADAS807_OUT_EVENT : You should not see this message! ',userEvent
                   
ENDCASE

END
  
;-----------------------------------------------------------------------------



FUNCTION ADAS807_OUT, inval , outval, procval, data_a, data_p,          $  
                      header,  bitfile,                                 $
                      FONT_LARGE = font_large, FONT_SMALL = font_small


; Set defaults for keywords and extract info for paper.txt question

  IF n_elements(font_large) eq 0 THEN font_large = ''
  IF n_elements(font_small) eq 0 THEN font_small = ''


  paperval =  { outbut   : outval.TEXOUT, $
                appbut   : outval.TEXAPP, $
                repbut   : outval.TEXREP, $
                filename : outval.TEXDSN, $
                defname  : outval.TEXDEF, $
                message  : outval.TEXMES  }

  ionval   =  { outbut   : outval.IONOUT, $
                appbut   : outval.IONAPP, $
                repbut   : outval.IONREP, $
                filename : outval.IONDSN, $
                defname  : outval.IONDEF, $
                message  : outval.IONMES  }

  slnval   =  { outbut   : outval.SLNOUT, $
                appbut   : outval.SLNAPP, $
                repbut   : outval.SLNREP, $
                filename : outval.SLNDSN, $
                defname  : outval.SLNDEF, $
                message  : outval.SLNMES  }

  a08val   =  { outbut   : outval.A08OUT, $
                appbut   : outval.A08APP, $
                repbut   : outval.A08REP, $
                filename : outval.A08DSN, $
                defname  : outval.A08DEF, $
                message  : outval.A08MES  }

  a09val   =  { outbut   : outval.A09OUT, $
                appbut   : outval.A09APP, $
                repbut   : outval.A09REP, $
                filename : outval.A09DSN, $
                defname  : outval.A09DEF, $
                message  : outval.A09MES  }

  a17val =    { outbut   : outval.A17OUT, $
                appbut   : outval.A17APP, $
                repbut   : outval.A17REP, $
                filename : outval.A17DSN, $
                defname  : outval.A17DEF, $
                message  : outval.A17MES  }

  
; Switch off S-lines and DR every time because these need interactive
; file selection popups.  
  
  slnval.outbut = 0
  a09val.outbut = 0

                ;********************************************
		;**** create modal top level base widget ****
                ;********************************************
                
  parent = Widget_Base(Column=1, Title='ADAS 807 OUTPUT', $
                       XOFFSET=100, YOFFSET=1)

                
  rc = widget_label(parent,value='  ',font=font_large)

                 
  
                ;**********************************
		;**** Ask for output files -   ****
                ;**** paper.txt and new adf10  ****
                ;**********************************

  base    = widget_base(parent, /column)
  
  mrow    = widget_base(base,/frame)
  paperID = cw_adas_outfile(mrow, OUTPUT='Text Output',   $
                                 VALUE=paperval, FONT=font_large)
  
  
  mlab  = widget_label(parent,value='  ',font=font_large)
  mcol  = widget_base(parent, /column)
 

  ionID = cw_adas_outfile(mcol, OUTPUT='Ionisation Pathways',   $
                                VALUE=ionval, FONT=font_large, xsize=50)

  slnID = cw_adas_outfile(mcol, OUTPUT='Ionisation Rates (S-lines)', $
                                EVENT_FUNCT = 'ADAS807_POP_EVENT',   $
                                VALUE=slnval, FONT=font_large, xsize=50)

                            
  a08ID = cw_adas_outfile(mcol, OUTPUT='adf08 file         ',   $
                                VALUE=a08val, FONT=font_large, xsize=50)
                       
                            
  a09ID = cw_adas_outfile(mcol, OUTPUT='adf18/a09_a04 file ',          $
                                EVENT_FUNCT = 'ADAS807_POP_EVENT',     $
                                VALUE=a09val, FONT=font_large, xsize=50)
                       
                            
  a17ID = cw_adas_outfile(mcol, OUTPUT='adf18/a17_p208 file',   $
                                VALUE=a17val, FONT=font_large, xsize=50)
                       
                       
                       
                            
                ;************************************
		;**** Error/Instruction message. ****
                ;************************************
                
  messID = widget_label(parent,value='     Choose output options   ',font=font_large)
                      
                      
                            
                ;*****************
		;**** Buttons ****
                ;*****************
                
  menufile = bitfile + '/menu.bmp'
  read_X11_bitmap, menufile, bitmap1
                
  mrow     = widget_base(parent,/row)
  menuID   = widget_button(mrow,value=bitmap1,font=font_large, $
                           event_pro='ADAS807_OUT_MENU')
  cancelID = widget_button(mrow,value='Cancel',font=font_large)
  doneID   = widget_button(mrow,value='Done',font=font_large)




                ;***************************
		;**** Put up the widget ****
                ;***************************

; Realize the ADAS807_OUT input widget.

   dynlabel, parent
   widget_Control, parent, /realize

; Create a pointer to store the information collected from the form.
; The initial data stored here is set to CANCEL, so nothing needs to
; be done if the user kills the widget with the mouse.

   ptrToFormData = Ptr_New({cancel:1, menu:0})
   ptrToFiles    = Ptr_New({a07path :  procval.a07path,    $
                            a07file :  procval.a07file,    $
                            a07cent :  procval.a07cent,    $
                            a07user :  procval.a07user,    $
                            a09path :  procval.a09path,    $
                            a09file :  procval.a09file,    $
                            a09cent :  procval.a09cent,    $
                            a09user :  procval.a09user    })

; Create an info structure with program information.

  info = { messID          :  messID,             $
           paperID         :  paperID,            $
           ionID           :  ionID,              $
           slnID           :  slnID,              $
           a08ID           :  a08ID,              $
           a09ID           :  a09ID,              $
           a17ID           :  a17ID,              $
           procval         :  procval,            $
           font            :  font_large,         $
           parent          :  parent,             $  
           ptrToFiles      :  ptrToFiles,         $
           ptrToFormData   :  ptrToFormData       }  
            
               
; Store the info structure in the user value of the top-level base and launch
; the widget into the world.

  widget_control, parent, Set_UValue=info

  XManager, 'ADAS807_OUT', parent, Event_Handler='ADAS807_OUT_EVENT'


; When the widget is destroyed, the block is released, and we
; return here. Get the form data that was collected by the form
; and stored in the pointer location.

formdata = *ptrToFormData

rep='CONTINUE'
IF N_Elements(formdata) EQ 0 THEN BEGIN
   Ptr_Free, ptrToFormData
   rep ='CANCEL'
ENDIF
 
IF formdata.cancel EQ 1 THEN BEGIN
   Ptr_Free, ptrToFormData
   rep ='CANCEL'
   RETURN,rep
ENDIF

IF formdata.menu EQ 1 THEN BEGIN
   Ptr_Free, ptrToFormData
   rep ='MENU
   RETURN,rep
ENDIF

if rep eq 'CONTINUE' then begin
   
    outval =  { TEXOUT      :   formdata.paper.outbut,      $
                TEXAPP      :   formdata.paper.appbut,      $
                TEXREP      :   formdata.paper.repbut,      $
                TEXDSN      :   formdata.paper.filename,    $
                TEXDEF      :   formdata.paper.defname,     $
                TEXMES      :   formdata.paper.message,     $
                IONOUT      :   formdata.ion.outbut,        $
                IONAPP      :   formdata.ion.appbut,        $
                IONREP      :   formdata.ion.repbut,        $
                IONDSN      :   formdata.ion.filename,      $
                IONDEF      :   formdata.ion.defname,       $
                IONMES      :   formdata.ion.message,       $
                SLNOUT      :   formdata.sln.outbut,        $
                SLNAPP      :   formdata.sln.appbut,        $
                SLNREP      :   formdata.sln.repbut,        $
                SLNDSN      :   formdata.sln.filename,      $
                SLNDEF      :   formdata.sln.defname,       $
                SLNMES      :   formdata.sln.message,       $
                A08OUT      :   formdata.a08.outbut,        $
                A08APP      :   formdata.a08.appbut,        $
                A08REP      :   formdata.a08.repbut,        $
                A08DSN      :   formdata.a08.filename,      $
                A08DEF      :   formdata.a08.defname,       $
                A08MES      :   formdata.a08.message,       $
                A09OUT      :   formdata.a09.outbut,        $
                A09APP      :   formdata.a09.appbut,        $
                A09REP      :   formdata.a09.repbut,        $
                A09DSN      :   formdata.a09.filename,      $
                A09DEF      :   formdata.a09.defname,       $
                A09MES      :   formdata.a09.message,       $
                A17OUT      :   formdata.a17.outbut,        $
                A17APP      :   formdata.a17.appbut,        $
                A17REP      :   formdata.a17.repbut,        $
                A17DSN      :   formdata.a17.filename,      $
                A17DEF      :   formdata.a17.defname,       $
                A17MES      :   formdata.a17.message        }
 
    procval = { a07path     :   formdata.val_a07.rootpath,  $
                a07file     :   formdata.val_a07.file,      $
                a07cent     :   formdata.val_a07.centroot,  $
                a07user     :   formdata.val_a07.userroot,  $
                a09path     :   formdata.val_a09.rootpath,  $
                a09file     :   formdata.val_a09.file,      $
                a09cent     :   formdata.val_a09.centroot,  $
                a09user     :   formdata.val_a09.userroot   }


   adf09files = formdata.a09files
   adf07file  = formdata.a07file

   ; Free the pointer.
   Ptr_Free, ptrToFormData

endif       



                ;******************************************
		;**** Output new files if all is well. ****
                ;******************************************


; During writing of files put up a busy widget

  widget_control, /hourglass
  busybase = widget_base(/column,xoffset=300,yoffset=200,     $
                           title = "ADAS807 INFORMATION")
  lab0 = widget_label(busybase, value='')
  lab1 = widget_label(busybase, value='')
  lab2 = widget_label(busybase, font=font_large,              $
                      value='       Writing files - please wait       ')
  lab3 = widget_label(busybase, value='')
  lab4 = widget_label(busybase, value='')
  widget_control, busybase, /realize



; Now do the writing - note some processing is needed first.

 filea = inval.erootpath + inval.efile
 filep = inval.crootpath + inval.cfile
 
 which_path = 'X'
 if inval.erootpath EQ inval.euserroot then which_path = 'U'
 if inval.erootpath EQ inval.ecentroot then which_path = 'C'
  
         
;number of levels of recombining ion 
  np = n_elements(data_p.i)

;number of levels of recombined ion
  na = n_elements(data_a.i)

;sequence of recombined ion
  ff =  data_a.nz - data_a.iz
  seq = xxesym(ff)

;configurations are constructed from configuration string
;
; configuration array is ordered along n,l-shells
;   
;   conf[0,k,j] = n-number of shell k of level j
;   conf[1,k,j] = l-number of shell k of level j
;   conf[2,k,j] = number of electrons in that shell of level j
;

;configuration for recombined ion
  confa =  config_str2arr(data_a.cstrg)

;configuration for recombining ion
  confp =  config_str2arr(data_p.cstrg)


;level description as a string (for output)
  levela = strarr(na)
  levelp = strarr(np)
  for i=0,na-1 do begin
     levela[i] = data_a.cstrg[i]+' '+ $
                 string(data_a.is[i],format='(i1)') + $
                 l_value(data_a.il[i])
  endfor
  for i=0,np-1 do begin
     levelp[i] = data_p.cstrg[i]+' ' + $
                 string(data_p.is[i],format='(i1)') + $
                 l_value(data_p.il[i])
  endfor


;Possible pathways to parent for each level of recombined ion
;rec_path   <-- possible transition 
;               (logical variable which will become false or true)
;               rec_path = intarr(number of parents, number of levels)
adas807_recom_path, $
   data_a.is, data_a.il, data_a.xj, confa, data_a.wv, data_a.bwn, $
   data_p.is, data_p.il, confp, data_p.wv, $
   rec_path, ion_path


; Write the adf18/adf17_p208 file - if requested

  if outval.a17out EQ 1 then begin
    
     outfile = outval.a17dsn
  
     adas807_prep_a17_p208,                           $
        outfile,                                      $
        which_path, filea, filep, seq,                $
        na, data_a.iz, data_a.nz, data_a.i,           $
        data_a.is, data_a.il, data_a.xj, confa,       $
        np, data_p.iz, data_p.nz,                     $
        data_p.i, data_p.is, data_p.il,               $
        ion_path
  
  
  endif



; Write the adf08 driver file - if requested

  if outval.a08out EQ 1 then begin
    
     outfile = outval.a08dsn

     adas807_prep_adf08, $
        outfile,                                                $
        which_path, filea, filep, seq,                          $
        data_a.scef, na, data_a.nz, data_a.i, confa, levela,    $
        data_a.is, data_a.il, data_a.xj, data_a.wv, data_a.bwn, $
        np, data_p.i, confp, levelp, data_p.is, data_p.il,      $
        data_p.xj, data_p.wv, data_p.bwn,                                        $
        rec_path
  
  endif



; Write the ionisation part of the adf04 file - if requested

  if outval.ionout EQ 1 then begin
    
     outfile = outval.iondsn

     adas807_add_ion_adf04, $
        outfile,                                                $ 
        which_path, filea, filep,                               $ 
        na, data_a.nz, data_a.iz, data_a.i, confa, levela,      $
        data_a.is, data_a.il, data_a.xj, data_a.wv, data_a.bwn, $
        np, data_p.i, levelp, data_p.is, data_p.il, data_p.wv,  $
        ion_path
  
  endif



; Write the S-lines of the adf04 file - if requested

  if outval.slnout EQ 1 then begin
    
     outfile = outval.slndsn

     if seq NE 'H' then begin     
        adas807_prep_slines, outfile,                     $
                     filea, adf07file, seq, data_a.scef,  $
                     data_a.nz, data_a.iz, data_p.iz,     $
                     data_a.wv, data_p.wv, data_a.bwn
     endif else begin
       adas807_prep_slines, outfile,                      $
                     filea, adf07file, seq, data_a.scef,  $
                     data_a.nz, data_a.iz, data_a.iz+1,   $
                     data_a.wv, [0.0], data_a.bwn
     endelse
  endif


; Write the adf18/a09_a04 matching file - if requested

  if outval.a09out EQ 1 then begin
    
     outfile = outval.a09dsn
               
     if seq NE 'H' then begin
        adas807_prep_a09_a04, outfile, adf09files, filea, $
                      data_a.iz, data_a.nz,       $
                      na, data_a.i, data_a.il, data_a.is, data_a.xj, confa
     endif
  
  endif







; Write the paper.txt file - if selected
  
    
; We have finished writing the data so destroy the busy widget

  widget_control, busybase, /destroy

; And tell ADAS807 that we are finished

RETURN,rep

END
