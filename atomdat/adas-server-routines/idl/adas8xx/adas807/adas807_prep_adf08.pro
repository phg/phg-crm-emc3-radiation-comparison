; Copyright (c) 2000 Strathclyde University .
;+
; PROJECT:
;       ADAS 
;
; NAME:
;	ADAS807_PREP_ADF08
;
; PURPOSE: prepare radiative recombination files (adf08)
;          for an ADAS211 run from adf04-files
;
;
; WRITTEN:
;       Ralph Dux
;
; MODIFIED:
;	1.1	Ralph Dux
;		 - First release
;       1.2     Martin O'Mullane
;                - add special case handling for Li-like. It is essential
;                  to have 1s2 in the adf08 file for correct calculation.
;                - Write SEQ= field for 1 character sequences with blank
;                  following rather than preceeding the identifer.
;	1.3	Martin O'Mullane
;		 - Write sequence as uppercase.
;	1.4	Martin O'Mullane
;		 - Rename to adas807_prep_adf08.
;                - Use square brackets for arrays.
;
; VERSION:
;       1.1	24-11-1998
;       1.2	30-08-2002
;       1.3     05-02-2003
;       1.4	27-08-2009
;
;-
;-----------------------------------------------------------------------------


pro adas807_prep_adf08, outfile,                                      $
                which_path, adf04_filea, adf04_filep, seq, scefa,     $
                na, nza, ia, confa, levela, isa, ila, xja, wva, bwna, $
                np, ip, confp, levelp, isp, ilp, xjp, wvp, bwnp,      $
                rec_path



;prepare string for multiplying factors
fac_string = strarr(na)

for ja=0,na-1 do begin

;are there possible recombination paths
   wh = where(rec_path[*, ja], n_wh)
   
;No!
   if n_wh le 0 then fac_string[ja] = '  {X}' else fac_string[ja] ='  '

;Yes! 
   if n_wh gt 0 then begin

;For each possible path    
      for i_wh=0,n_wh-1 do begin
         jp = wh[i_wh]

;usually factor is 1
         factor = 1.0

;check whether it is one of the following transitions
;and change factor accordingly       
       
;  Be-like  -> B-like
         if levelp[jp] eq '1S2 2S1 2P1 3P' then begin
            if levela[ja] eq '1S2 2S2 2P1 2P' then factor=3./4.
            if levela[ja] eq '1S2 2S1 2P2 4P' then factor=4./3.
            if levela[ja] eq '1S2 2S1 2P2 2D' then factor=3./2.
            if levela[ja] eq '1S2 2S1 2P2 2S' then factor=3./2.
            if levela[ja] eq '1S2 2S1 2P2 2P' then factor=1./6.
         endif
         
;  B-like  -> C-like
         if levelp[jp] eq '1S2 2S1 2P2 4P' then begin
            if levela[ja] eq '1S2 2S2 2P2 3P' then factor=2./3.
            if levela[ja] eq '1S2 2S1 2P3 5S' then factor=5./4.
            if levela[ja] eq '1S2 2S1 2P3 3D' then factor=2./3.
            if levela[ja] eq '1S2 2S1 2P3 3P' then factor=2./3.
            if levela[ja] eq '1S2 2S1 2P3 3S' then factor=1./12.
         endif   
         
;  C-like  -> N-like
         if levelp[jp] eq '1S2 2S2 2P2 3P' then begin
            if levela[ja] eq '1S2 2S2 2P3 2D' then factor=1./2.
            if levela[ja] eq '1S2 2S2 2P3 2P' then factor=1./2.
         endif 
         if levelp[jp] eq '1S2 2S2 2P2 1D' then begin
            if levela[ja] eq '1S2 2S2 2P3 2D' then factor=1./2.
            if levela[ja] eq '1S2 2S2 2P3 2P' then factor=5./18.
         endif  
         if levelp[jp] eq '1S2 2S2 2P2 1S' then begin
            if levela[ja] eq '1S2 2S2 2P3 2P' then factor=2./3.
         endif 
         if levelp[jp] eq '1S2 2S1 2P3 5S' then begin
            if levela[ja] eq '1S2 2S2 2P3 4S' then factor=5./8.
            if levela[ja] eq '1S2 2S1 2P4 4P' then factor=5./12.
         endif
         
;  N-like  -> O-like
         if levelp[jp] eq '1S2 2S2 2P3 4S' then begin
            if levela[ja] eq '1S2 2S2 2P4 3P' then factor=1./3.
         endif 
         if levelp[jp] eq '1S2 2S2 2P3 2D' then begin
            if levela[ja] eq '1S2 2S2 2P4 3P' then factor=5./12.
            if levela[ja] eq '1S2 2S2 2P4 1D' then factor=3./4.
         endif
         if levelp[jp] eq '1S2 2S2 2P3 2P' then begin
            if levela[ja] eq '1S2 2S2 2P4 3P' then factor=1./4.
            if levela[ja] eq '1S2 2S2 2P4 1D' then factor=1./4.
         endif 
         
;  O-like  -> F-like
         if levelp[jp] eq '1S2 2S2 2P4 3P' then begin
            if levela[ja] eq '1S2 2S2 2P5 2P' then factor=3./5.
         endif 
         if levelp[jp] eq '1S2 2S2 2P4 1D' then begin
            if levela[ja] eq '1S2 2S2 2P5 2P' then factor=1./3.
         endif 
         if levelp[jp] eq '1S2 2S2 2P4 1S' then begin
            if levela[ja] eq '1S2 2S2 2P5 2P' then factor=1./15.
         endif 
         
         fac_string[ja] = fac_string[ja] + string(ip[jp],factor,form='("{",i1,"}",f5.3," ")')
      endfor
   endif
endfor

;short configuration strings from arrays - Li-like is a special case

special_seq = strupcase(strtrim(seq))

case special_seq of
 'LI' : begin
          cstrga =  config_arr2str(confa, nlow=1)
          cstrgp =  config_arr2str(confp)
        end
 else : begin
          cstrga =  config_arr2str(confa)
          cstrgp =  config_arr2str(confp)
        end
endcase

;open file for output
openw, lun, outfile, /get_lun


;TITLE

seq_out = '  '
strput, seq_out, strupcase(seq)
printf, lun, seq_out, nza, form='("SEQ=''",a2,"''     NUCCHG=",i2)'
printf, lun, ' '

;PARENTS
printf, lun, bwnp, np, form='("   PARENT TERM INDEXING            BWNP=  ",'+$
   'f10.1,"   NPRNTI=",i2)'
printf, lun, '   --------------------',form='(a)'
printf, lun, '   INDP    CODE                 S L   WI        WNP', form='(a)'
printf, lun, '   ----    ----                 - -   --  ----------'

for jp=0,np-1 do begin
   printf, lun, ip[jp], cstrgp[jp], isp[jp], ilp[jp], xjp[jp], wvp[jp], $
      form='(I6,5X,1A17,3X,"(",i1,")",i1,"(",f4.1,")",X,F10.1)'
endfor
printf, lun, ' '

;TERMS
printf, lun, bwna, na, form='("   LS RESOLVED TERM INDEXING       BWNR=  ",'+$
   'f10.1,"    NTRM=",i3)'
printf, lun, '   -------------------------',form='(a)'
printf, lun, '   INDX    CODE                 S L   WJ        WNR', form='(a)'
printf, lun, '   ----    ----                 - -   --  ----------'

for ja=0,na-1 do begin
   printf, lun, ia[ja], cstrga[ja], isa[ja], ila[ja], xja[ja], wva[ja], fac_string[ja], $
      form='(I6,5X,1A17,3X,"(",i1,")",i1,"(",f4.1,")",X,F10.1,A)'
endfor

printf,lun,' '
printf,lun,' '
printf,lun,scefa,form='("        TE=  ",14(E8.2,2x))'


;comments

pos_a = strpos(adf04_filea,'/adf04')
str_a = strmid(adf04_filea,pos_a)
pos_p = strpos(adf04_filep,'/adf04')
str_p = strmid(adf04_filep,pos_p)
CASE which_path OF
 'U' : Begin
         str_p = string('"ADASUSER"',str_p, /print, format='(a,a)')
         str_a = string('"ADASUSER"',str_a, /print, format='(a,a)')
       End
 'C' : Begin
         str_p = string('"ADASCENT"',str_p, /print, format='(a,a)')
         str_a = string('"ADASCENT"',str_a, /print, format='(a,a)')
       End

 'X' : Begin
         str_p = string(str_p, /print, format='(a)')  
         str_a = string(str_a, /print, format='(a)')  
       End
ENDCASE

date = xxdate()
name = xxuser()
     
printf,lun, ' '
printf,lun, 'C-----------------------------------------------------------------'
printf,lun, 'C'
printf,lun, 'C  ADF04 FILE FOR PARENT ION: ', form='(a)'
printf,lun, 'C  '+str_p 
printf,lun, 'C'
printf,lun, 'C  ADF04 FILE FOR RECOMBINED ION: ', form='(a)'
printf,lun, 'C  '+str_a 
printf,lun, 'C'
printf,lun, 'C  Code     : ADAS807'
printf,lun, 'C  Producer : ',name(0)
printf,lun, 'C  Date     : ',date(0)
printf,lun, 'C'
printf,lun, 'C-----------------------------------------------------------------'

close, lun
free_lun, lun

end
