; Copyright (c) 2000 Strathclyde University .
;----------------------------------------------------------------------------
;+
; PROJECT:
;       ADAS
;
; NAME:
;       ADAS807_PROC
;
; PURPOSE:
;       Acts on the two adf04 files to produce the necessary information.
;
;
; VARIABLES:
;
;  adf04_filea  --> file name for recombined ion
;  adf04_filep  --> file_name for recombining ion
;
; LEVELS OF RECOMBINED ATOM:
;      ia       --> ENERGY LEVEL INDEX NUMBER
;      cstrga   --> NOMENCLATURE/CONFIGURATION FOR LEVEL 'IA()'
;      isa      --> MULTIPLICITY FOR LEVEL 'IA()'
;                   (ISA-1)/2 = QUANTUM NUMBER (S)
;      ila      --> QUANTUM NUMBER (L) FOR LEVEL 'IA()'
;      xja      --> QUANTUM NUMBER (J-VALUE) FOR LEVEL 'IA()'
;                   (2*XJA)+1 = STATISTICAL WEIGHT
;      wva      --> ENERGY RELATIVE TO LEVEL 1 (CM-1) FOR LEVEL 'IA()'
;      bwna     --> ionisation energy to first matastable of parent
; LEVELS OF RECOMBINING ATOM:
;      ip       --> ENERGY LEVEL INDEX NUMBER
;      cstrgp   --> NOMENCLATURE/CONFIGURATION FOR LEVEL 'IP()'
;      isp      --> MULTIPLICITY FOR LEVEL 'IP()'
;                   (ISP-1)/2 = QUANTUM NUMBER (S)
;      ilp      --> QUANTUM NUMBER (L) FOR LEVEL 'IP()'
;      xjp      --> QUANTUM NUMBER (J-VALUE) FOR LEVEL 'IP()'
;                   (2*XJP)+1 = STATISTICAL WEIGHT
;      wvp      --> ENERGY RELATIVE TO LEVEL 1 (CM-1) FOR LEVEL 'IP()'
;      bwnp     --> ionisation energy to first matastable of parent
;
;      nva      --> NUMBER OF GAMMA/TEMPERATURE PAIRS IN ADF04 FILE
;                   FOR RECOMBINED ION
;      scefa    --> ELECTRON TEMPERATURES (K) IN ADF04 FILE
;                   FOR RECOMBINED ION
;
;
; OPTIONAL INPUTS:
;       None
;
; OUTPUTS:
;       None - Note this routine should leave all inputs un-modified.
;
; OPTIONAL OUTPUTS:
;       None
;
; KEYWORD PARAMETERS:
;       None
;
; CALLS:
;
; SIDE EFFECTS:
;       Some system calls in the called functions.
;
; NOTES:
;
;
; CATEGORY:
;       Adas system.
;
; WRITTEN:
;       Ralph Dux/Martin O'Mullane
;
; MODIFIED:
;       1.1     Martin O'Mullane
;                - First release
;       1.2     Martin O'Mullane
;                - Guess the metastables to allow quicker operation.
;       1.3     Martin O'Mullane
;                - Missed setting all parameters for the H-like parent case.
;       1.4     Martin O'Mullane
;                - Replace reading of adf04 files and determining recombining
;                  isoelectronic sequence with calls to standard ADAS routines.
;                - Remove calls to period_system, read_levels_04 and
;                  read_tegrid_04.
;                - Use square brackets for arrays.
;       1.5     Martin O'Mullane
;                 - Allow Eissner adf04 files by testing configurations
;                   with xxdtes and converting on the fly with xxcftr.
;
; VERSION:
;       1.1     12-10-1999
;       1.2     07-11-2000
;       1.3     18-12-2000
;       1.4     24-08-2009
;       1.5     07-06-2013
;
;-
;----------------------------------------------------------------------------

FUNCTION ADAS807_PROC_NULL_EVENTS, event

   ; The purpose of this event handler is to do nothing
   ;and ignore all events that come to it.

   ; need a function for cw_bgroup.

   RETURN,0

END

;-----------------------------------------------------------------------------

PRO ADAS807_PROC_MENU, event

; The 'normal' get_value from a widget_button does not work with a
; bitmap button. Hence this specialised event handler.

   ; Get the info structure out of the user value of the top-level base.

Widget_Control, event.top, Get_UValue=info


; Something nefarious may cause an error also.
;  -- if so trap it and exit gracefully

CATCH, error
IF error NE 0 THEN BEGIN
   formdata = {cancel : 0, menu:1}
   *info.ptrToFormData = formdata
   Widget_Control, event.top, /Destroy
   message,!err_string
   RETURN
ENDIF


 formdata = {cancel : 0, menu:1}
 *info.ptrToFormData = formdata
 widget_Control, event.top, /destroy

END

;-----------------------------------------------------------------------------


PRO ADAS807_PROC_EVENT, event

   ; Get the info structure out of the user value of the top-level base.

Widget_Control, event.top, Get_UValue=info

Widget_Control, event.id, Get_Value=userEvent


CASE userEvent OF

  'Cancel' : begin
               formdata = {cancel:1}
               *info.ptrToFormData =formdata
               widget_Control, event.top, /destroy
             end


  'Done'   : begin

                ; gather the data for return to calling program

                widget_Control, info.metID, Get_Value=buttons

                err = 0

                ;have any metastables been selected
                if total(buttons) EQ 0 then begin
                   err = 1
                   mess = 'Choose at least ONE metastable'
                endif

                if err EQ 0 then begin
                   parents = where(buttons EQ 1)
                   formdata = {parents   : parents,     $
                               cancel    : 0,           $
                               menu      : 0            }
                   *info.ptrToFormData = formdata
                   widget_control, event.top, /destroy
                endif else begin
                   widget_Control, info.messID, Set_Value=mess
                endelse

             end


  else : print,'ADAS807_IN_EVENT : You should not see this message! ',userEvent

ENDCASE

END

;-----------------------------------------------------------------------------



FUNCTION adas807_proc, adf04_filea, adf04_filep, data_a, data_p, $
                       bitfile, FONT_LARGE=font_large, FONT_SMALL = font_small

                ;**** Set defaults for keywords ****

  IF n_elements(font_large) eq 0 THEN font_large = ''
  IF n_elements(font_small) eq 0 THEN font_small = ''


; Preliminary work to read files and get configuration information.

; Read Recombined/Ionizing file

read_adf04, file = adf04_filea, fulldata=all

scefa  = all.te
iza    = all.iz
nza    = all.iz0
ia     = all.ia
isa    = all.isa
ila    = all.ila
xja    = all.xja
wva    = all.wa
bwna   = all.bwno

nlev   = n_elements(isa)
cstrga = strarr(nlev)
for j = 0, nlev-1 do begin
   xxdtes, in_cfg=all.cstrga[j], is_eissner=is_eissner
   if is_eissner EQ 1 then begin
      xxcftr, in_cfg=all.cstrga[j], out_cfg=tcfg, type=3
   endif else tcfg = all.cstrga[j]
   xxprs3, config = tcfg, z_nuc=nza, z_ion=iza, occup=occ
   cstrga[j] = occ2cfg(occ, /display, /upper)
endfor

; number of levels of recombined ion
na = n_elements(ia)

; sequence of recombined ion
ff  =  nza-iza
seq = xxesym(ff)


; Read recombining/ionised ion-file if sequence is not 'H'
if seq ne 'H' then begin

   read_adf04, file = adf04_filep, fulldata=all

   izp    = all.iz
   nzp    = all.iz0
   ip     = all.ia
   isp    = all.isa
   ilp    = all.ila
   xjp    = all.xja
   wvp    = all.wa
   bwnp   = all.bwno
   
   nlev   = n_elements(isp)
   cstrgp = strarr(nlev)
   for j = 0, nlev-1 do begin
      xxdtes, in_cfg=all.cstrga[j], is_eissner=is_eissner
      if is_eissner EQ 1 then begin
         xxcftr, in_cfg=all.cstrga[j], out_cfg=tcfg, type=3
      endif else tcfg = all.cstrga[j]
      xxprs3, config = tcfg, z_nuc=nzp, z_ion=izp, occup=occ
      cstrgp[j] = occ2cfg(occ, /display, /upper)
   endfor

;number of levels of recombinig ion
   np = n_elements(ip)

;level description as a string (to choose the metastables)
   slen = max(strlen(cstrgp))
   levelp = strarr(np) + string(replicate(32B,slen))
   level_guess = strarr(np)+ string(replicate(32B,slen))
   for i=0,np-1 do begin
      str = string(replicate(32B,slen))
      
      strput, str, cstrgp[i]
      levelp[i] = str+'    '+string(isp[i],form='(i1)')+ $
                  l_value(ilp[i])+j_value(xjp[i])
      level_guess[i] = str+'    '+string(isp[i],form='(i1)')+ $
                  l_value(ilp[i])
   endfor

endif else begin

   print,'H- sequence: continue...'
   nzp = nza
   izp = iza+1
   ip = 1
   cstrgp = ' '
   isp = 1
   ilp = 0
   xjp = 0.0
   wvp = 0.0
   bwnp = 0.0

   rep='CONTINUE'
   goto, LABELEND

endelse


; Now ask the user to select the metastables but try to guess what
; they are!

  ; sequence of recombining ion
  ff =  nzp-izp
  seq = xxesym(ff)
  metastable, seq=seq, config=config_g, term=term_g, /upper
  n_meta = n_elements(config_g)
  guess_str = strarr(n_meta) + string(replicate(32B,slen))
  for i = 0, n_meta-1 do begin
     str = guess_str[i]
     strput, str, config_g[i]
     guess_str[i] = str + '    ' + term_g[i]
  endfor

  onbut = -1
  for j = 0, n_elements(guess_str)-1 do begin
     onbut = [onbut, where(level_guess EQ guess_str[j])]
  endfor
  if n_elements(onbut) GT 1 then onbut = onbut[1:*]

  set_button = intarr(np)
  set_button[onbut] = 1

                ;********************************************
                ;**** create modal top level base widget ****
                ;********************************************

  parent = Widget_Base(Column=1, Title='ADAS 807 : METASTABLE SELECTION', $
                       XOFFSET=100, YOFFSET=1)


  rc = widget_label(parent,value='  ',font=font_large)


  base  = widget_base(parent, /column, /base_align_center)


  rc = cw_adas_dsbr(base,adf04_filep,font=font_large)



; The cw_bgroup is sized in pixels. We must make the xsize large enough
; so that there will be no x scrollbar. This method uses a dynamically
; resized label widget to calculate the xsize. The idea came from
; Phil Aldis on the comp.lang.idl-pvwave newsgroup (19/10/99).
;
; Add an extra 4 characters for the button.

  slen = max(strlen(levelp))
  longS = ''
  for j=0, slen-1+4 do longS = longS + 'H'

  bd = widget_base(map=0)
  lb = widget_label(bd, /dynamic_resize, value='',font=font_small)
  widget_control, lb, set_value=longS
  wd = (widget_info(lb, /geometry)).scr_xsize
  xs = fix(wd)


  buttons = levelp
  metID   =  cw_bgroup(base,buttons,                           $
                       nonexclusive=1,column=1,                $
                       set_value = set_button,                 $
                       event_func='ADAS807_PROC_NULL_EVENTS',  $
                       scroll=1, y_scroll_size=200,            $
                       xsize=xs,x_scroll_size=xs,              $
                       /no_release, /frame, font=font_small    )


                ;************************************
                ;**** Error/Instruction message. ****
                ;************************************

  messID = widget_label(parent,value='     Select metastable levels     ',$
                        font=font_large)

                ;*****************
                ;**** Buttons ****
                ;*****************

  menufile = bitfile + '/menu.bmp'
  read_X11_bitmap, menufile, bitmap1

  mrow     = widget_base(parent,/row)
  menuID   = widget_button(mrow,value=bitmap1,font=font_large, $
                           event_pro='ADAS807_PROC_MENU')
  cancelID = widget_button(mrow,value='Cancel',font=font_large)
  doneID   = widget_button(mrow,value='Done',font=font_large)





                ;***************************
                ;**** Put up the widget ****
                ;***************************

; Realize the ADAS807_PROC input widget.

   dynlabel, parent
   widget_Control, parent, /realize

; Create a pointer to store the information collected from the form.
; The initial data stored here is set to CANCEL, so nothing needs to
; be done if the user kills the widget with the mouse.

   ptrToFormData = Ptr_New({cancel:1, menu:0})

; Create an info structure with program information.

  info = { messID          :  messID,             $
           metID           :  metID,              $
           ptrToFormData   :  ptrToFormData       }


; Store the info structure in the user value of the top-level base and launch
; the widget into the world.

  widget_control, parent, Set_UValue=info

  XManager, 'ADAS807_PROC', parent, Event_Handler='ADAS807_PROC_EVENT'


; When the widget is destroyed, the block is released, and we
; return here. Get the form data that was collected by the form
; and stored in the pointer location.

formdata = *ptrToFormData


rep='CONTINUE'
IF N_Elements(formdata) EQ 0 THEN BEGIN
   Ptr_Free, ptrToFormData
   rep ='CANCEL'
ENDIF

IF formdata.cancel EQ 1 THEN BEGIN
   Ptr_Free, ptrToFormData
   rep ='CANCEL'
   RETURN,rep
ENDIF

IF formdata.menu EQ 1 THEN BEGIN
   Ptr_Free, ptrToFormData
   rep ='MENU
   RETURN,rep
ENDIF


if rep eq 'CONTINUE' then begin

   parents = formdata.parents
   ; Free the pointer.
   Ptr_Free, ptrToFormData

endif

nparents = n_elements(parents)
ip = ip[parents]
cstrgp = cstrgp[parents]
isp = isp[parents]
ilp = ilp[parents]
xjp = xjp[parents]
wvp = wvp[parents]


LABELEND:

nva   = n_elements(scefa)

data_a = { iz    : iza,     $
           nz    : nza,     $
           i     : ia,      $
           cstrg : cstrga,  $
           is    : isa,     $
           il    : ila,     $
           xj    : xja,     $
           wv    : wva,     $
           bwn   : bwna,    $
           nv    : nva,     $
           scef  : scefa    }

scefp = fltarr(1)

data_p = { iz    : izp,     $
           nz    : nzp,     $
           i     : ip,      $
           cstrg : cstrgp,  $
           is    : isp,     $
           il    : ilp,     $
           xj    : xjp,     $
           wv    : wvp,     $
           bwn   : bwnp,    $
           nv    : 0,       $
           scef  : scefp    }


RETURN, rep

END
