; Copyright (c) 2000 Strathclyde University .
;+
; PROJECT:
;       ADAS
;
; NAME:
;       L_VALUE
;
; PURPOSE:
;       Returns the J-value as a string - using fractions for 1/2.
;
;INPUT:
;      l        --> quantum number
;OUTPUT:
;      trans    <-- string representation.

;
; WRITTEN:
;       Ralph Dux
;
; MODIFIED:
;       1.1     Ralph Dux
;               First release
;       1.2     Martin O'Mullane
;                - Add comments.
;       1.3     Martin O'Mullane
;                - Allow l as far as 'H'.
;
; VERSION:
;       1.1     08-09-1999
;       1.2     27-08-2009
;       1.3     22-10-2009
;
;-
;-----------------------------------------------------------------------------
function l_value,i

if (i eq 0) then l='S'
if (i eq 1) then l='P'
if (i eq 2) then l='D'
if (i eq 3) then l='F'
if (i eq 4) then l='G'
if (i eq 5) then l='H'
if (i gt 5) then l='X'

return,l
end
