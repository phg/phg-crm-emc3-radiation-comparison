; Copyright (c) 2000 Strathclyde University .
;+
; PURPOSE: add ionisation multipliers and complete ionisation energy list
;          of adf04-files
;
; WRITTEN:
;       Ralph Dux
;
; MODIFIED:
;       1.1     Ralph Dux
;                - First release
;       1.2     Martin O'Mullane
;                - Rename to adas807_add_ion_adf04.
;                - Use square brackets for arrays.
;
; VERSION:
;       1.1     24-11-1998
;       1.2     27-08-2009
;
;-
;-----------------------------------------------------------------------------

pro adas807_add_ion_adf04, outfile,                                           $
                   which_path, adf04_filea, adf04_filep,                      $
                   na, nza, iza, ia, confa, levela, isa, ila, xja, wva, bwna, $
                   np, ip, levelp, isp, ilp, wvp,                             $
                   rec_path


;ionisation multipliers
ionmultstr = strarr(na)


; We may have exceptions - indicate their nature in a comment
exc_comment = 'C'


; Begin with exception handling for special cases

for ja=0,na-1 do begin

   ionmultstr[ja] = 'NORMAL'      ; for when it's not special!

   CASE (nza-iza) OF


     2 :  Begin                   ;  He-like  -> H-like

            if levela[ja] eq '1S2 2S1 2S' then ionmultstr[ja] = ' {1}2.000'

          End

     3 :  Begin                   ;  Li-like  -> He-like

            if levela[ja] eq '1S2 2S1 2S' then ionmultstr[ja] = ' {1}1.000 {2}1.500'

          End

     4 :  Begin                   ;  Be-like  -> Li-like

            if levela[ja] eq '1S2 2S2 1S'     then ionmultstr[ja] = ' {1}2.000'
            if levela[ja] eq '1S2 2S1 2P1 3P' then ionmultstr[ja] = ' {1}2.000'
            if levela[ja] eq '1S2 2S1 2P1 1P' then ionmultstr[ja] = ' {1}2.000'
            if strmid(levela[ja],0,9) eq '1S2 2P1 3' then begin
               exc_comment = ['C',  $
                              'C  The ionisation pathways for the 1s2 2p parent with n=3 terms',$
                              'C  (1s2 2p 3n) have a parent which is not a metastable but it is',$
                              'C  a resonance transition to ground so parent 1 was used for',$
                              'C  these terms.',$
                              'C']
               ionmultstr[ja] = ' {1}1.000'
            endif

          End


     5 :  Begin                   ;  B-like  -> Be-like

            if levela[ja] eq '1S2 2S2 2P1 2P' then ionmultstr[ja] = ' {1}1.500 {2}1.500'
            if levela[ja] eq '1S2 2S1 2P2 4P' then ionmultstr[ja] = ' {2}2.000'
            if levela[ja] eq '1S2 2S1 2P2 2D' then ionmultstr[ja] = ' {1}0.500 {2}1.500'
            if levela[ja] eq '1S2 2S1 2P2 2S' then ionmultstr[ja] = ' {1}0.500 {2}1.500'
            if levela[ja] eq '1S2 2S1 2P2 2P' then ionmultstr[ja] = ' {1}0.500 {2}1.500'
            exc_comment = ['C',  $
                           'C  The 2s 2p2 doublet terms have equivalent electrons and the',$
                           'C  ionisation pathways are distributed between the 2 parents.',$
                           'C']

          End

     6 :  Begin                   ;  C-like  -> B-like

            if levela[ja] eq '1S2 2S2 2P2 3P' then ionmultstr[ja] = ' {1}2.667 {2}1.330'
            if levela[ja] eq '1S2 2S2 2P2 1D' then ionmultstr[ja] = ' {1}4.000'
            if levela[ja] eq '1S2 2S2 2P2 1S' then ionmultstr[ja] = ' {1}4.000'
            if levela[ja] eq '1S2 2S1 2P3 5S' then ionmultstr[ja] = ' {2}4.000'
            if levela[ja] eq '1S2 2S1 2P3 3D' then ionmultstr[ja] = ' {1}1.333 {2}2.667'
            if levela[ja] eq '1S2 2S1 2P3 3P' then ionmultstr[ja] = ' {1}1.333 {2}2.667'
            if levela[ja] eq '1S2 2S1 2P3 1D' then ionmultstr[ja] = ' {X}'
            if levela[ja] eq '1S2 2S1 2P3 3S' then ionmultstr[ja] = ' {1}1.333 {2}2.667'
            exc_comment = ['C',  $
                           'C  The 2s2 2p2 and 2s 2p3 terms have equivalent electrons and the',$
                           'C  ionisation pathways are distributed between the 2 parents.',$
                           'C']

          End

     7 :  Begin                   ;  N-like  -> C-like

            if levela[ja] eq '1S2 2S2 2P3 4S' then ionmultstr[ja] = ' {1}3.000 {4}1.250'
            if levela[ja] eq '1S2 2S2 2P3 2D' then ionmultstr[ja] = ' {1}2.250 {2}0.624 {3}0.126'
            if levela[ja] eq '1S2 2S2 2P3 2P' then ionmultstr[ja] = ' {1}2.250 {2}0.624 {3}0.126'
            if levela[ja] eq '1S2 2S1 2P4 4P' then ionmultstr[ja] = ' {1}1.500 {4}2.500'
            if levela[ja] eq '1S2 2S1 2P4 2D' then ionmultstr[ja] = ' {1}2.250 {2}0.624 {3}0.126'
            if levela[ja] eq '1S2 2S1 2P4 2S' then ionmultstr[ja] = ' {1}2.250 {2}0.624 {3}0.126'
            if levela[ja] eq '1S2 2S1 2P4 2P' then ionmultstr[ja] = ' {1}2.250 {2}0.624 {3}0.126'
             exc_comment = ['C',  $
                           'C  The 2s2 2p3 and 2s 2p4 terms have equivalent electrons and the',$
                           'C  ionisation pathways are distributed between the 2 parents.',$
                           'C']

          End

     8 :  Begin                   ;  O-like  -> N-like

            if levela[ja] eq '1S2 2S2 2P4 3P' then ionmultstr[ja] = ' {1}2.668 {2}0.832 {3}0.500'
            if levela[ja] eq '1S2 2S2 2P4 1D' then ionmultstr[ja] = ' {2}2.500 {3}1.500'
            if levela[ja] eq '1S2 2S2 2P4 1S' then ionmultstr[ja] = ' {3}4.000'
             exc_comment = ['C',  $
                           'C  The 2s2 2p4 terms have equivalent electrons and the',$
                           'C  ionisation pathways are distributed between the 3 parents.',$
                           'C']

          End

     9 :  Begin                   ;  F-like  -> O-like

            if levela[ja] eq '1S2 2S2 2P5 2P' then ionmultstr[ja] = ' {1}3.750 {2}1.040 {3}0.210'
            if levela[ja] eq '1S2 2S1 2P4 3S1 4P' then ionmultstr[ja] = ' {1}1.000 {4}4.000'
             exc_comment = ['C',  $
                           'C  The ionisation pathways are distributed between the parents.',$
                           'C']

          End

    10 :  Begin                   ;  Ne-like  -> F-like

            if levela[ja] eq '1S2 2S2 2P6 1S' then ionmultstr[ja] = ' {1}6.000'
            if levela[ja] eq '1S2 2S1 2P5 3S1 3P' then ionmultstr[ja] = ' {1}1.000 {2}5.000'
             exc_comment = ['C',  $
                           'C  The ionisation pathways are distributed between the parents.',$
                           'C']

          End

    ELSE :

    ENDCASE


endfor


; And now the rest - set the ionmultstr only if there are no exceptions

for ja=0,na-1 do begin

   wh = where(rec_path(*, ja), n_wh)
   if ionmultstr[ja] eq 'NORMAL' then begin

      if n_wh le 0 then ionmultstr[ja] = ' {X}' else ionmultstr[ja] = ' '


      if n_wh gt 0 then begin
         for i_wh=0,n_wh-1 do begin

            jp = wh(i_wh)
            factor = 1.0

            ionmultstr[ja] = ionmultstr[ja] + string(ip(jp),factor,form='("{",i1,"}",f5.3," ")')

         endfor

     endif

  endif

endfor



;short configuration strings from arrays
cstrga =  config_arr2str(confa)

;prepare string with ionisation energies
ion_out = ' '
for i=0,np-1 do begin
   ion_out =  ion_out + $
      string( bwna+wvp(i), isp(i), l_value(ilp(i)), $
              form='(F15.1,"(",i1,a1,") ")' )
endfor

;what element
ff =  nza
element = xxesym(ff)
if strlen(element) eq 1 then element = element+' '

;open file for output
openw, lun, outfile, /get_lun

printf, lun, element, iza, nza, iza+1, ion_out, $
   f='(a2,"+",i2,2(i10),a)'

for ja=0, na-1 do begin
   printf, lun, ia[ja], cstrga[ja], isa[ja], ila[ja], xja[ja], wva[ja], ionmultstr[ja], $
      form='(I5,X,A17,X,"(",i1,")",i1,"(",f4.1,")",X,F10.1,A)'
endfor


; comments

date = xxdate()
name = xxuser()

printf,lun, ' '
printf,lun, 'C-----------------------------------------------------------------'
printf,lun, 'C'
printf,lun, 'C  Ionisation pathways added.'
printf,lun,exc_comment, format='(a)'
printf,lun, 'C  Code     : ADAS807'
printf,lun, 'C  Producer : ',name(0)
printf,lun, 'C  Date     : ',date(0)
printf,lun, 'C'
printf,lun, 'C-----------------------------------------------------------------'

close,lun
free_lun,lun


end
