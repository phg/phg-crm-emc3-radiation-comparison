; Copyright (c) 2000 Strathclyde University .
;+
; PROJECT:
;       ADAS
;
; NAME:
;       J_VALUE
;
; PURPOSE:
;       Returns the J-value as a string - using fractions for 1/2.
;
;INPUT:
;      j        --> quantum number
;OUTPUT:
;      trans    <-- string representation.

;
; WRITTEN:
;       Ralph Dux
;
; MODIFIED:
;       1.1     Ralph Dux
;               First release
;       1.2     Martin O'Mullane
;                - Add comments.
;
; VERSION:
;       1.1     08-09-1999
;       1.2     27-08-2009
;
;-
;-----------------------------------------------------------------------------


function j_value,i

half = i*2 mod 2

if (half)     then j=string(i*2,format='(i2)')+'/2'
if (not half and i lt 10) then j=string(i,format='(i1)')
if (not half and i ge 10) then j=string(i,format='(i2)')

return,j
end
