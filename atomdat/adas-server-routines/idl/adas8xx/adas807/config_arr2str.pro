; Copyright (c) 2000 Strathclyde University .
;+
; PROJECT:
;       ADAS
;
; NAME:
;       CONFIG_STR2ARR
;
; PURPOSE:  configuration strings from configuration array
;
; configuration array is ordered along n,l-shells
;
;
; INPUT   :  conf[0,k,j] = n-number of shell k of level j
;            conf[1,k,j] = l-number of shell k of level j
;            conf[2,k,j] = number of electrons in that shell of level j
;
;
; KEYWORDS:  short - do not print occupation for single electrons
;            nlow  - force configuration to start at this n shell
;
; OUTPUT  :  csrtg    string array with one string for each level
;                     (example: '1S2 2S1 2P1')
;
; WRITTEN:
;       Ralph Dux
;
; MODIFIED:
;       1.1     Ralph Dux
;               First release
;       1.2     Martin O'Mullane
;                - Add nlow keyword
;       1.3     Martin O'Mullane
;                - Use square brackets for arrays.
;
; VERSION:
;       1.1     08-09-1999
;       1.2     20-08-2002
;       1.3     27-08-2009
;
;-
;-----------------------------------------------------------------------------

function config_arr2str, conf, short=short, nlow=nlow

;constants
name_shell = ['S', 'P', 'D', 'F', 'G', 'H']

;number of configuration strings
nn = n_elements(conf[0, 0, *])

;maximum number of shells
nlmax =  n_elements(conf[0, *, 0])

;prepare arrays
cstrg = strarr(nn)


for i=0, nn-1 do begin
   str =  ' '

;number of electrons
   nel =  total(conf[2, *, i])

;first n-shell which is considerd

   if n_elements(nlow) GT 0 then begin
      n_low = nlow
   endif else begin
      n_low = 1
      if (nel gt 2)  then n_low = 2
      if (nel gt 10) then n_low = 3
   endelse
   wh = where(conf[0, *, i] ge n_low, n_wh)

;the usual string
   if not keyword_set(short) then begin
      for i_wh=0, n_wh-1 do begin
         nl = wh[i_wh]

         str =  str + $
            string(conf[0, nl, i], f='(i1)')+$
            l_value(conf[1, nl, i])+$
            string(conf[2, nl, i], f='(i1)')+' '
      endfor

      str = strtrim(str, 2)
      nlen =  strlen(str)

;fill up with spaces up to 17 character
      for k=0, 16-nlen do begin
         str =  str + ' '
      endfor
      cstrg[i] =  str
   endif

;the short string
   if keyword_set(short) then begin
      for i_wh=0, n_wh-1 do begin
         nl = wh[i_wh]

         occ = conf[2, nl, i]
         filled_inner_s =  (conf[2, nl, i] eq 2)  and  (conf[1, nl, i] eq 0) $
                           and (n_wh gt 1)

         if occ eq 1 then $
         str =  str + $
            string(conf[0, nl, i], f='(i1)')+$
            l_value(conf[1, nl, i])+' '

         if occ gt 1 and not filled_inner_s then $
         str =  str + $
            string(conf[0, nl, i], f='(i1)')+$
            l_value(conf[1, nl, i])+$
            string(conf[2, nl, i], f='(i1)')+' '
      endfor

      str = strtrim(str, 2)
      nlen =  strlen(str)

;fill up with spaces up to 10 character
      for k=0, 9-nlen do begin
         str =  str + ' '
      endfor
      cstrg[i] =  str
   endif

endfor

return,cstrg

end
