; Copyright (c) 2000 Strathclyde University .
;+
; PROJECT:
;       ADAS
;
; NAME:
;       ADAS807_PREP_A17_P208
;
; PURPOSE: Prepare adf17 - ADAS 208 cross referencing data
;          (adf18/a17_p208/*) from adf04-files
;
;
; WRITTEN:
;       Ralph Dux
;
; MODIFIED:
;       1.1     Ralph Dux
;                - First release
;       1.2     Martin O'Mullane
;                - Add outfile.
;       1.3     Martin O'Mullane
;                - Rename to adas807_prep_a17_p208.
;                - Use square brackets for arrays.
;
; VERSION:
;       1.1     08-09-1999
;       1.2     15-10-1999
;       1.3     27-08-2009
;
;-
;-----------------------------------------------------------------------------
pro adas807_prep_a17_p208, outfile,                           $
                   which_path, adf04_filea, adf04_filep, seq, $
                   na, iza, nza, ia, isa, ila, xja, confa,    $
                   np, izp, nzp, ip, isp, ilp,                $
                   rec_path



;highest occupied n shell of configuration -> n_shell
n_shell = intarr(na)
for ja=0,na-1 do begin
   n_shell[ja] = max(confa[0,*,ja])
endfor

;What spinsystems are there in the recombined ion ?
spin_sysa = isa(uniq(isa, sort(isa)))
nspin_sysa = n_elements(spin_sysa)

;Lowest N-Shell of each spinsystem

lowestn = intarr(nspin_sysa)
for ja=0,nspin_sysa-1 do begin
   lowestn[ja] = min( n_shell(where(isa eq spin_sysa[ja])))
endfor

;Fractional Parentage
;fractional parentage depends on spinstem and parent
frpr = fltarr(nspin_sysa,np)
frpr[*,*] = 0.0

;go through the spin systems
for ja=0,nspin_sysa-1 do begin

;reset weight
   total_weight = 0.0

;go through the parents
   for jp=0,np-1 do begin

;Does (2S+1) change by 1? (Is a transition possible?)
      test = spin_sysa[ja] eq isp[jp]-1 or spin_sysa[ja] eq isp[jp]+1

;Yes!  Add (2S+1) to total weight
      if test then total_weight = total_weight+ isp[jp]
   endfor

;go through parents once again
   for jp=0,np-1 do begin

;Does (2S+1) change by 1?  (Is a transition possible?)
      test = spin_sysa[ja] eq isp[jp]-1 or spin_sysa[ja] eq isp[jp]+1

;Yes! Calculate fractional parentage
      if test then frpr[ja,jp] = isp[jp] / total_weight
   endfor
endfor

;open file for output
openw, lun, outfile, /get_lun

printf, lun, seq, form='(" &SEQINF SEQ=''",a,"'',")'

pos = strpos(adf04_filea,'/adf04')
str = strmid(adf04_filea,pos)
CASE which_path OF
 'U' : printf, lun, '"ADASUSER"',str, form='("         DSNREF=''",a,a,"'',")'
 'C' : printf, lun, '"ADASCENT"',str, form='("         DSNREF=''",a,a,"'',")'
 'X' : printf, lun, adf04_filea,form='("         DSNREF=''",a,"'',")'
ENDCASE



printf, lun,'         DSNCPM= fill in adf17 file name'
printf, lun,'         NPARNT=',np,',',f='(a,i1,a)'
printf, lun,'         NSHEL=',max(n_shell),',',f='(a,i1,a)'
printf, lun,'         NLEV=',na,',',f='(a,i3,a)'
printf, lun,' &END',f='(a)'
printf, lun,' '

;save parents and fractional parentages
str=':'
for jp=0,np-1 do $
   str = str+'('+string(isp[jp],form='(i1)')+l_value(ilp[jp])+')'+'       :'
printf,lun, '       PARENT      ',str,f='(a,a)'

;Multiplicity of Parents
str=':'
for jp=0,np-1 do $
   str = str+string(isp[jp],form='(i1)')+'          :'
printf,lun, '       SPINPRT     ',str,f='(a,a)'

;Number of Spinsystems the parent is connected with
str=':'
for jp=0,np-1 do begin
   whfr = where(frpr[*,jp] gt 1.d-10, n_wh)
   str = str+string(n_wh,form='(i1)')+'          :'
endfor
printf,lun, '       NSPNSYS     ',str,f='(a,a)'
printf,lun, ' '

;for each parent
for jp=0,np-1 do begin
   printf, lun, '    PARENT = ', ip[jp], f='(a,i2)'
   printf, lun, '    -----------'

   whfr = where(frpr[*,jp] gt 1.d-10, n_wh)

; spin systems the parent is connected with
   str=':'
   for k=0,n_wh-1 do begin
      ja = whfr[k]
      str = str + string(spin_sysa[ja],form='(i1)')+'          :'
   endfor
   printf,lun, '       SPINSYS     ', str, f='(a,a)'

; lowest n-shell in that spin system
   str=':'
   for k=0,n_wh-1 do begin
      ja = whfr[k]
      str = str+string(lowestn[ja],form='(i1)')+'          :'
   endfor
   printf,lun, '       LOWESTN     ', str, f='(a,a)'

; not described in user manual, may be it is NSPNSYS
   str=':'
   for k=0,n_wh-1 do begin
      str = str+string(n_wh,form='(i1)')+'          :'
   endfor
   printf,lun, '       IMAX        ', str, f='(a,a)'

;no idea what to put here
   str=':'
   for k=0,n_wh-1 do begin
      ja = whfr[k]
      str = str+string(1.0,form='(f6.4)')+'     :'
   endfor
   printf,lun, '       LOWESTP     ', str, f='(a,a)'

;fractional parentage
   str=':'
   for k=0,n_wh-1 do begin
      ja = whfr[k]
      str = str+string(frpr[ja,jp],form='(f6.4)')+'     :'
   endfor
   printf,lun, '       FRPARNT     ', str, f='(a,a)'
   printf, lun, ' '
endfor

printf,lun, f='(a)','                                                                             '
printf,lun, f='(a)',' RESOLVED NL  <-- BUNDLE-N                N:1     N:2     N:3     N:4     N:5'
printf,lun, f='(a)','                                                                             '
printf,lun, f='(a)','INDX CODE                 SZD SP SH PT   N-SHELL NORMALISED WEIGHTS          '
printf,lun, f='(a)','---- ----                 --- -- -- --   --------------------------          '



;now go through spinsystems of n-shells and add weights for paths
nsh=  5
nsp = 5
tot_weight = fltarr(nsp,nsh,np)
tt = intarr(na)

for jp=0,np-1 do begin ;each parent
   for jsh=0,nsh-1 do begin ;each shell
      msh = jsh+1
      for jsp=0,nsp-1 do begin ;each spinsystem
         msp = jsp+1

;possible paths to parent jp
         tt[*] = rec_path[jp,*]

;check for levels in that n-shell and spinsystem
;with a path to that specific parent
         whja = where(n_shell eq msh and isa eq msp and tt, n_whja)

;add up (2L+1) for these levels
         if n_whja ge 1 then begin
            for ii=0, n_whja-1 do begin
               ja = whja[ii]
               tot_weight[jsp,jsh,jp] = tot_weight[jsp,jsh,jp] + 2*ila[ja]+1.
            endfor
         endif

      endfor
   endfor
endfor

;short configuration
confa_short = config_arr2str(confa, /short)

;now go through levels and write out normalized weights
norm_weight=fltarr(nsh)

for ja=0,na-1 do begin

;reset norm_weight
   norm_weight[0:nsh-1] = 0.

;Are there paths ?
   whjp = where(rec_path[*,ja], n_whjp)

;Yes
   if n_whjp ge 1 then begin

;one line for each path
      for ii=0,n_whjp-1 do begin

;that is the parent
         jp = whjp[ii]

;total weight for Spin-System , n-shell to that parent
         ff = tot_weight(isa[ja]-1, n_shell[ja]-1, jp)

;normalized weight
         norm_weight[n_shell[ja]-1] = (2.*ila[ja] +1.)/ff

;make the output string
         str_out=' '
         for k=0,nsh-1 do begin
            if norm_weight[k] lt 1.e-6 then str_out = str_out+':       '
            if norm_weight[k] gt 1.e-6 then str_out = str_out+':'+$
               string(norm_weight[k],f='(f7.4)')
         endfor

;and dump everything
         printf, lun, ia[ja], confa_short[ja], isa[ja], ila[ja], xja[ja], $
            0, isa[ja], n_shell[ja], ip[jp], str_out, $
            form='(i3,x,a10,x,"(",i1,")",i1,"(",f4.1,")",x,4(i3),x,a41)'
      endfor
   endif

;No
   if n_whjp lt 1 then begin

;make a zero string
      str_out=' '
      for k=0,nsh-1 do begin
         if norm_weight[k] lt 1.d-6 then str_out = str_out+':       '
         if norm_weight[k] gt 1.d-6 then str_out = str_out+':'+$
            string(norm_weight[k],f='(f7.4)')
      endfor

;and dump it
      printf, lun, ia[ja], confa_short[ja], isa[ja], ila[ja], xja[ja], $
         0, isa[ja], n_shell[ja], 0, str_out, $
         form='(i3,x,a10,x,"(",i1,")",i1,"(",f4.1,")",x,4(i3),x,a41)'
   endif
endfor



;comments

pos_a = strpos(adf04_filea,'/adf04')
str_a = strmid(adf04_filea,pos_a)
pos_p = strpos(adf04_filep,'/adf04')
str_p = strmid(adf04_filep,pos_p)
CASE which_path OF
 'U' : Begin
         str_p = string('"ADASUSER"',str_p, /print, format='(a,a)')
         str_a = string('"ADASUSER"',str_a, /print, format='(a,a)')
       End
 'C' : Begin
         str_p = string('"ADASCENT"',str_p, /print, format='(a,a)')
         str_a = string('"ADASCENT"',str_a, /print, format='(a,a)')
       End

 'X' : Begin
         str_p = string(str_p, /print, format='(a)')
         str_a = string(str_a, /print, format='(a)')
       End
ENDCASE

date = xxdate()
name = xxuser()

printf,lun, ' '
printf,lun, 'C-----------------------------------------------------------------'
printf,lun, 'C'
printf,lun, 'C  ADF04 FILE FOR PARENT ION: ', form='(a)'
printf,lun, 'C  '+str_p
printf,lun, 'C'
printf,lun, 'C  ADF04 FILE FOR RECOMBINED ION: ', form='(a)'
printf,lun, 'C  '+str_a
printf,lun, 'C'
printf,lun, 'C  Code     : ADAS807'
printf,lun, 'C  Producer : ',name(0)
printf,lun, 'C  Date     : ',date(0)
printf,lun, 'C'
printf,lun, 'C-----------------------------------------------------------------'


close,lun
free_lun,lun
end
