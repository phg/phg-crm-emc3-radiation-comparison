; Copyright (c) 2000 Strathclyde University .
;+
; PROJECT:
;       ADAS
;
; NAME:
;       ADAS807
;
; PURPOSE:
;       The highest level routine for the ADAS 807 program.
;
; EXPLANATION:
;       This routine is called from the main adas system routine,
;       adas.pro, to start the ADAS 807 application.
;
;       The purpose of ADAS807 is to
;
;
; USE:
;       First the system settings must be established by calling
;       adas_sys_set.pro then adas807.pro is called to start the
;       ADAS 807 application;
;
;       adas_sys_set, adasrel, fortdir, userroot, centroot, $
;                     devlist, devcode, font_large, font_small, edit_fonts
;       adas807,   adasrel, fortdir, userroot, centroot, devlist, $
;                  devcode, font_large, font_small, edit_fonts
;
; INPUTS:
;       ADASREL - A string indicating the ADAS system version,
;                 e.g ' ADAS RELEASE: ADAS93 V1.4'.  The first
;                 character should be a space.
;
;       FORTDIR - A string holding the path to the directory where the
;                 FORTRAN executables are, e.g '/disk/adas/fortran/exec'
;                 Not used but retainned for compatibility.
;
;       USERROOT - A string holding the path to the root directory of
;                  the user's adas data. e.g '/disk/bowen/adas' This
;                  root directory will be used by adas to construct
;                  other path names.  In particular the user's default
;                  interface settings will be stored in the directory
;                  USERROOT+'/defaults'.  An error will occur if the
;                  defaults directory does not exist.
;
;       CENTROOT - Like USERROOT, but this directory points to the
;                  central data area for the system.  User defaults are
;                  not stored on CENTROOT.
;
;       DEVLIST - A string array of hardcopy device names, used for
;                 graphical output. e.g ['Post-Script','HP-PCL','HP-GL']
;                 This array must mirror DEVCODE.  DEVCODE holds the
;                 actual device names used in a SET_PLOT statement.
;                 Not used but retainned for compatibility.
;
;       DEVCODE - A string array of hardcopy device code names used in
;                 the SET_PLOT statement, e.g ['PS', 'PCL', 'HP']
;                 This array must mirror DEVLIST.
;                 Not used but retainned for compatibility.
;
;       FONT_LARGE - The name of a larger font e.g 'courier_bold14'
;
;       FONT_SMALL - The name of a smaller font e.g 'courier_bold12'
;
;       EDIT_FONTS - A structure of two fonts used in the table editor
;                    adas_edtab.pro, {font_norm:'',font_input:''}.
;                    The two fonts are used to differentiate between
;                    editable and non-editable parts of the table. You
;                    may have to experiment to get two fonts which give
;                    the same default spacing in the edit widget.
;                    e.g {font_norm:'helvetica_bold14', $
;                         font_input:'helvetica_oblique14'}
;                    Not used but retainned for compatibility.
;
; OPTIONAL INPUTS:
;       None
;
; OUTPUTS:
;       None - Note this routine should leave all inputs un-modified.
;
; OPTIONAL OUTPUTS:
;       None
;
; KEYWORD PARAMETERS:
;       None
;
; CALLS:
;       ADAS807_IN    Prompts for and gathers input files
;       ADAS807_OUT   Prompts for and writes output files
;       XXDATE        Get date and time from operating system.
;       POPUP         Puts up a message on the screen.
;
; SIDE EFFECTS:
;       Some system calls in the called functions.
;
; NOTES:
;       This program requires IDL v5. This is tested for at the beginning.
;       The mode of operation is different to other ADAS programs in that
;       the 3 called routines are functions which return the variable 'rep'.
;       rep can be 'CONTINUE', 'CANCEL' or 'MENU' and subsequent action
;       depends on the returned value.
;
; CATEGORY:
;       Adas system.
;
; WRITTEN:
;       Ralph Dux/Martin O'Mullane
;
; MODIFIED:
;       1.1  Martin O'Mullane
;             - First release
;       1.2  Martin O'Mullane
;             - Added support for s-lines. Added procval structure.
;             - Increased version no. to 1.2 .
;	1.3  Richard Martin
;	      - Increased version no. to 1.3 .
;       1.4  Martin O'Mullane
;             - Correct misnamed adf18/a09_a04 file (from a04_a09).
;             - Increased version no to 1.4
;       1.4  Martin O'Mullane
;             - Version 1.5 extended to Ar-like metastables.
;
; VERSION:
;       1.1  12-10-1999
;       1.2  10-22-2000
;	1.3  14-03-2001
;       1.4  06-02-2003
;       1.5  24-08-2009
;
;-
;----------------------------------------------------------------------------

PRO ADAS807,    adasrel, fortdir, userroot, centroot,                   $
                devlist, devcode, font_large, font_small, edit_fonts

                ;************************
                ;**** Initialisation ****
                ;************************

    adasprog = ' PROGRAM: ADAS807 V1.5'
    deffile  = userroot+'/defaults/adas807_defaults.dat'
    bitfile  = centroot+'/bitmaps'
    device   = ''
                
    
                ;***********************************************
                ;**** Make sure we are running v5 or above. ****
                ;***********************************************

    thisRelease = StrMid(!Version.Release, 0, 1)
    IF thisRelease LT '5' THEN BEGIN
       message = 'Sorry, ADAS807 requires IDL 5 or higher' 
       tmp = popup(message=message, buttons=['Accept'],font=font_large)
       goto, LABELEND
    ENDIF

                ;******************************************
                ;**** Search for user default settings ****
                ;**** If not found create defaults     ****
                ;**** inval: settings for data files   ****
                ;**** outval: settings for output      ****
                ;******************************************

    files = findfile(deffile)
    if files(0) eq deffile then begin
    
        restore, deffile
        inval.ecentroot = centroot+'/adf04/'
        inval.ccentroot = centroot+'/adf04/'
        inval.euserroot = userroot+'/adf04/'
        inval.cuserroot = userroot+'/adf04/'
    
    endif else begin
        
        inval = { EROOTPATH     :       userroot+'/adf04/',             $
                  EFILE         :       '',                             $
                  ECENTROOT     :       centroot+'/adf04/',             $
                  EUSERROOT     :       userroot+'/adf04/',             $
                  CROOTPATH     :       userroot+'/adf04/',             $
                  CFILE         :       '',                             $
                  CCENTROOT     :       centroot+'/adf04/',             $
                  CUSERROOT     :       userroot+'/adf04/'              }

        ionname = userroot + '/pass/adas807_adf04_ion.pass'
        a08name = userroot + '/pass/adas807_adf08.pass'
        a09name = userroot + '/pass/adas807_a09_a04.pass'
        a17name = userroot + '/pass/adas807_a17_p208.pass'
        slnname = userroot + '/pass/adas807_adf04_sline.pass'
        
        outval  = { TEXOUT      :       0,                      $
                    TEXAPP      :       -1,                     $
                    TEXREP      :       0,                      $
                    TEXDSN      :       '',                     $
                    TEXDEF      :       'paper.txt',            $
                    TEXMES      :       '',                     $
                    IONOUT      :       0,                      $
                    IONAPP      :       -1,                     $
                    IONREP      :       0,                      $
                    IONDSN      :       '',                     $
                    IONDEF      :       IONname,                $
                    IONMES      :       '',                     $
                    SLNOUT      :       0,                      $
                    SLNAPP      :       -1,                     $
                    SLNREP      :       0,                      $
                    SLNDSN      :       '',                     $
                    SLNDEF      :       SLNname,                $
                    SLNMES      :       '',                     $
                    A08OUT      :       0,                      $
                    A08APP      :       -1,                     $
                    A08REP      :       0,                      $
                    A08DSN      :       '',                     $
                    A08DEF      :       A08name,                $
                    A08MES      :       '',                     $
                    A09OUT      :       0,                      $
                    A09APP      :       -1,                     $
                    A09REP      :       0,                      $
                    A09DSN      :       '',                     $
                    A09DEF      :       A09name,                $
                    A09MES      :       '',                     $
                    A17OUT      :       0,                      $
                    A17APP      :       -1,                     $
                    A17REP      :       0,                      $
                    A17DSN      :       '',                     $
                    A17DEF      :       A17name,                $
                    A17MES      :       ''                      }

        procval = { a07path     :      centroot+'/adf07/' ,     $
                    a07file     :      '',                      $
                    a07cent     :      centroot+'/adf07/' ,     $
                    a07user     :      userroot+'/adf07/' ,     $
                    a09path     :      centroot+'/adf09/' ,     $
                    a09file     :      '',                      $
                    a09cent     :      centroot+'/adf09/' ,     $
                    a09user     :      userroot+'/adf09/'       }
                    
   endelse
        
    
                   


LABEL100:

                ;********************************************
                ;**** Invoke user interface widget for   ****
                ;**** Data file selection                ****
                ;********************************************

    rep=adas807_in(inval, FONT_LARGE=font_large, FONT_SMALL = font_small)
 
    if rep eq 'CANCEL' then goto, LABELEND


LABEL200:
                ;********************************************
                ;**** Invoke user interface widget for   ****
                ;********************************************

    filea = inval.erootpath + inval.efile
    filep = inval.crootpath + inval.cfile
    rep=adas807_proc(filea,filep,data_a, data_p, $
                     bitfile, FONT_LARGE=font_large, FONT_SMALL = font_small)
 
    if rep eq 'MENU'   then goto, LABELEND
    if rep eq 'CANCEL' then goto, LABEL100

    
LABEL300:

                ;************************************************
                ;**** Communicate with d4spf1 in fortran and ****
                ;**** invoke user interface widget for       ****
                ;**** Output options                         ****
                ;************************************************

                ;**** Create header for output. ****

    date   = xxdate()
    header = adasrel+adasprog+' DATE: '+date(0)+' TIME: '+date(1)
                    
    rep=adas807_out(inval , outval, procval,  data_a  , data_p ,      $
                    header, bitfile ,                                 $
                    FONT_LARGE=font_large, FONT_SMALL = font_small)
    

    if rep eq 'MENU'   then goto, LABELEND
    if rep eq 'CANCEL' then goto, LABEL100

                ;**** Back for more output options ****

    GOTO, LABEL300

LABELEND:


                ;**** Save user defaults ****

    save, inval, outval, procval, filename=deffile


END
