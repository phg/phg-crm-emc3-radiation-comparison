; Copyright (c) 1996 Strathclyde University .
; SCCS info: Module @(#)$Header: /home/adascvs/idl/adas1xx/adas105/a5outg.pro,v 1.4 2004/07/06 10:02:35 whitefor Exp $ Date $Date: 2004/07/06 10:02:35 $
;+
; PROJECT:
;       ADAS IBM MVS to DEC UNIX conversion & development
;
; NAME:
;       A5OUTGA
;
; PURPOSE:
;       To read omegas and rate coeffs. after fortran processing, popup the 
;       ADAS_GRAPH_EDITOR write any alterations to the fortran and plot the
;       final rate coeffs.
;
; USE:
;       ADAS105 Specific
;
; INPUTS:
;       PIPE    - The IDL unit number of the bi-directional pipe to the
;                 ADAS105 FORTRAN process.
;
;       LPEND   - Integer, 0 if the user pressed the 'Done' button
;                 or 1 if the user pressed 'Cancel'.  Maps directly onto
;                 the logical variable LPEND in ADAS105 FORTRAN.
;
;       LCHOICE - Integer; Variable to flag user choice of archive
;                 option. 
;                 0 = No Archive
;                 1 = Old Archive
;                 2 = Refresh form Old Archive
;                 3 = New Archive
;
;       OUTVAL -  A Structure which holds the output widget settings see 
;                 a5spf1.pro
;
;       PROCVAL - A Structure which holds the processing widget settings
;                 see a5ispf.pro.
;
;       ROOT -    The path to the user's default archive directory, 
;                 '/home/user/adas/arch105'
;
;       DEVICE -  The name of the current output device.
;
;       DATE -    The date
;
;       HEADER -  The ADAS release header for the final plot.  
;
;       TITLX  -  The datset name, for display on the plot.
;
;       BITFILE - String; the path to the directory containing bitmaps
;                 for the 'escape to series menu' button.
;
; OPTIONAL INPUTS:
;                 None
;
; OUTPUTS:   
;       GOMENU  - Int; flag - set to 1 if user has selected 'escape direct
;                 to series menu , otherwise 0
;
; OPTIONAL OUTPUTS:
;                 None
;
; KEYWORD PARAMETERS:
;       FONT - The name of a font.         
;
; CALLS:
;       ADAS_EDIT_GRAPH - The ADAS version of the interactive Graph Editor
;       ADAS105_PLOT - The plotting procedure for the Upsilon graph
;
; SIDE EFFECTS:
;       Two way communications with the Fortran process via the Unix pipe.
;
; CATEGORY:
;       ADAS System
;
; WRITTEN: 
;       William Osborn, Tessella Support Services plc, 24-09-96
;
; MODIFIED:
;	1.1	William Osborn
;		First written using a1outga.pro
;       1.2     William Osborn
;               Added clearing of text message for lpend=1 or 2
;       1.3     William Osborn
;               Changed window title
;       1.4     William Osborn
;               Set x-limits to [0,1] on editable graph
;
; VERSION:
;	1.1	24-09-96
;	1.2	04-10-96
;	1.3	08-10-96
;	1.4	11-10-96
;-
;-----------------------------------------------------------------------------
pro a5outg, pipe, lpend, lchoice, root, outval, procval, device, date, $
             header , titlx, bitfile, gomenu, FONT = font

                   ;**** re-set lpend ****
  lpend = 0
                   ;********************************
                   ;**** read data from fortran ****                   
                   ;********************************

  fdum=0.0
  idum=0
  sdum = ''
  readf, pipe, fdum
  ascl = fdum
  readf, pipe, idum
  ict=idum
  readf, pipe, idum
  itout=idum
  readf, pipe, idum
  nithr=idum
  readf, pipe, idum
  nrthr=idum
  indim1 = ict
  xa = fltarr(indim1)
  ya = xa & apa = xa 
  xp = fltarr(12)
  indim2 = itout
  toa = fltarr(indim2)
  yoa = toa
  yoap = toa
  for i = 0,ict-1 do begin
    readf, pipe, fdum
    xa(i) = fdum
    readf, pipe, fdum
    ya(i) = fdum
    readf, pipe, fdum
    apa(i) = fdum
  endfor
  for i = 0,itout-1 do begin
    readf, pipe, fdum
    toa(i) = fdum
    readf, pipe, fdum
    yoa(i) = fdum
    readf, pipe, fdum
    yoap(i) = fdum
  endfor
  for i=0, nithr+nrthr-1 do begin
    readf, pipe, fdum
    xp(i) = fdum
  endfor
            ;*********************************************************
            ;**** Read in the annotation giving CIA, CRA, A, etc. ****
            ;*********************************************************
  readf, pipe, idum
  nigrp = idum
  readf, pipe, idum
  nrgrp = idum
  readf, pipe, fdum
  emin = fdum
  stitle1='CIA = '
  stitle2='DIRECT IONISATION     NIGRP='+$
    strtrim(string(nigrp,format='(I2)'),2)+'  EMIN='+$
    strtrim(string(emin,format='(1E12.5)'),2)+'!C!C!C'
  if nigrp gt 0 then begin
    for i=0,nigrp-1 do begin
      readf, pipe, fdum
      stitle1=stitle1+ strtrim(string(fdum, format='(F9.5)'),2)+'  '
      stitle2=stitle2+'GROUP='+strtrim(string(i+1,format='(I2)'),2)+'  '+$
        'CIA='+strtrim(string(fdum, format='(F6.3)'),2)+ $
        '        EION(RYD)   ZETA!C'
      readf, pipe, idum
      if idum gt 0 then begin
          for j=0,idum-1 do begin
              readf,pipe, fdum
              stitle2=stitle2+'                              '+$
                strtrim(string(fdum,format='(F9.5)'),2)+'   '
              readf, pipe, idum
              stitle2=stitle2+strtrim(string(idum,format='(I3)'),2)+'!C'
          endfor
      endif
    endfor
  endif
  stitle1=stitle1+', CRA = '
  stitle2=stitle2+'!CEXCIT/AUTOIONISATION  NRGRP='+$
    strtrim(string(nrgrp,format='(I2)'),2)+'!C!C!C'
  if nrgrp gt 0 then begin
    for i=0,nrgrp-1 do begin
      readf, pipe, fdum
      stitle1=stitle1+ strtrim(string(fdum, format='(F9.5)'),2)+'  '
      stitle2=stitle2+'GROUP='+strtrim(string(i+1,format='(I2)'),2)+'  '+$
        'CRA='+strtrim(string(fdum, format='(F6.3)'),2)+ $
        '        EIJ(RYD)   WGHT!C'
      readf, pipe, idum
      if idum gt 0 then begin
          for j=0,idum-1 do begin
              readf,pipe, fdum
              stitle2=stitle2+'                              '+$
                strtrim(string(fdum,format='(F9.5)'),2)+'   '
              readf, pipe, fdum
              stitle2=stitle2+strtrim(string(fdum,format='(F9.5)'),2)+'!C'
          endfor
      endif
    endfor
  endif
  stitle1=stitle1+', A = '+strtrim(string(ascl, format='(F9.5)'),2)
  readf,pipe,fdum
  z1 = fdum
  stitle2=stitle2+'!CZ1='+strtrim(string(z1,format='(F5.2)'),2)

                   ;********************************
                   ;****    Alter to AGE        **** 
                   ;****    required forms  &   ****
                   ;**** set up arrays for AGE  ****  
                   ;********************************

  dgey = ya/apa
  dgex = 1.0d0-(1.0d0+ascl)/(xa+ascl)
  x = toa/z1^2
  y = yoa*z1^3
  yp = yoap*z1^3
  for i=0,nithr+nrthr-1 do begin
    xp(i)=1.0d0-(1.0d0+ascl)/(xp(i)+ascl)
  endfor
  res = {res105, nithr:nithr, nrthr:nrthr, xv:xp}

                   ;********************************
                   ;**** Popup diagnostic graph ****
                   ;****         editor         ****                   
                   ;********************************
  print1 = 0
  xtit='(1-(A+1)/(A+X))'
  ytit='OM(OBS)/OM(APPROX)'
  tit='Ratio Plot'
  if outval.grpscal1 eq 1 then begin
    adas_edit_graph, dgex,    dgey,    lpend,   print1,  nsp = 70, $
      xmax = float(outval.xmax1), xmin = float(outval.xmin1), $
      ymax = float(outval.ymax1), ymin = float(outval.ymin1), $
      font = font , resonances = res, xtitle=xtit, ytitle = ytit, $
      no_print = 1, title=tit
  end else begin
    adas_edit_graph, dgex,    dgey,    lpend,   print1,  nsp = 70,	$
      font = font, resonances = res, xtitle=xtit, ytitle = ytit, $
      no_print = 1, title=tit, xmin=0.0d0, xmax=1.0d0
  end
                   ;**** Re-scale arrays if alterations made ****
  xa = fltarr(n_elements(dgex))

	;*******************************************************************
	;**** Unlike in ADAS101, the calculation of the approximate 	****
	;**** values, apa, is not simple and so dgey is passed back to 	****
	;**** the fortran for reanalysing			    	****
	;*******************************************************************

	;**** Re-calculate xa ****

  for i = 0,n_elements(dgex)-1 do begin
    xa(i) = (1+ascl)/(1-dgex(i))-ascl
  endfor

  ictn = n_elements(dgey)

                   ;****print new values to pipe****
                   ;****   for re-analysing     ****    

  printf, pipe, lpend
  if lpend eq 2 then begin
    printf, pipe, ictn
    for i = 0,ictn-1 do begin
      printf, pipe, xa(i)
      printf, pipe, dgey(i)
    endfor
  endif

    if lpend eq 1 or lpend eq 2 then begin
        outval.texmes = ''
        goto, LABELEND
    endif
                   ;**** re-set lpend ****
    lpend = 0
    arch = 0       ;**** default archive option is off
    b = intarr(5)
    for i = 0,4 do b(i) = -1

    adas105_plot, dgex, dgey, x, y, yp, lpend, arch, lchoice, stitle1,$
      stitle2, outval, action, device, date, header, $
      titlx, bitfile, gomenu, wintitle = 'ADAS105 GRAPHICAL OUTPUT', nsp = 70,$
      font = font, res=res
    printf, pipe, lpend

                   ;**** Print archiving decision if continuing ****

    if lpend eq 0 then printf, pipe, arch
    
    if lpend eq 1 then goto, LABELEND


                   ;**** special case if archiving selected after ****
                   ;**** picking no archiving initially ****

    if arch eq 1 and lchoice eq 0 then begin
      dsarch = root+'archive.dat'
      printf, pipe, dsarch
    endif

LABELEND:

end


 
