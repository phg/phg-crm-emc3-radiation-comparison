; Copyright (c) 1996, Strathclyde University.
; SCCS Info : Module @(#)$Header: /home/adascvs/idl/adas1xx/adas105/cw_adas105_out.pro,v 1.2 2004/07/06 12:28:07 whitefor Exp $ Date $Date: 2004/07/06 12:28:07 $
;+
; PROJECT:
;       ADAS IBM MVS to   UNIX conversion
;
; NAME:
;	CW_ADAS105_OUT()
;
; PURPOSE:
;	Produces a widget for ADAS105 output options.
;
; EXPLANATION:
;	This function declares a compound widget consisting of two
;	graphical output selection widget cw_105_gr_sel.pro, and an 
;	output file widget cw_adas_outfile.pro.  The text output
;	file specified in this widget is for tabular (paper.txt)
;	output.
;       This widget also includes a button for browsing the comments
;       from the input dataset, a 'Cancel' button and a 'Done' button.
;	The compound widgets cw_105_gr_sel.pro and cw_adas_outfile.pro
;	included in this file are self managing.  This widget only
;	handles events from the 'Done' and 'Cancel' buttons.
;
; USE:
;	This widget is specific to ADAS105, see adas105_out.pro	for use.
;
; INPUTS:
;	PARENT	- Long integer; ID of parent widget.
;
;	DSFULL	- Name of input dataset for this application.
;
;       ROOT    - String; The path to the users archive directory.
;
;       BITFILE - String; the path to the directory containing bitmaps
;                 for the 'escape to series menu' button.
;
; OPTIONAL INPUTS:
;	None.
;
; OUTPUTS:
;	The return value of this function is the ID of the declared widget.
;
; OPTIONAL OUTPUTS:
;	None.
;
; KEYWORD PARAMETERS:
;	DEVLIST	- A string array;  Each element of the array is the
;		  name of a hardcopy graphics output device.  This
;		  list must mirror the DEVCODE list.  DEVLIST is the
;		  list which the user will see.
;		  e.g ['Post-Script','HP-PCL','HP-GL']
;
;	VALUE	- A structure which determines the initial settings of
;		  the output options widget.  The structure is made
;		  up of three parts.  Two of the parts are the same as
;                 the value structure of one of the two main compound widgets
;		  included in this widget.  See cw_105_gr_sel and
;		  cw_adas_outfile for more details. The other part is
;                 associated with the two analysis options.
;                 The default value is;
;
;             {out105_set, 						$
;			GRPOUT:0, GTIT1:'', 				$
;			GRPSCAL1:0, 					$
;			XMIN1:'',  XMAX1:'',   				$
;			YMIN1:'',  YMAX1:'',   				$
;			GRPSCAL2:0, 					$
;			XMIN2:'',  XMAX2:'',   				$
;			YMIN2:'',  YMAX2:'',   				$
; 		        HRDOUT:0, HARDNAME:'', 				$
;	                GRPDEF:'', GRPFMESS:'', 			$
;			GRPSEL:-1, GRPRMESS:'', 			$
;			DEVSEL:-1, GRSELMESS:'', 			$
;			TEXOUT:0, TEXAPP:-1, 				$
;			TEXREP:0, TEXDSN:'', 				$
;                       TEXDEF:'',TEXMES:'',ASCL:0.0			$
;              }
;  For CW_1XX_GR_SEL;
;			GRPOUT	   Integer; Activation button 1 on, 0 off
;			GTIT1	   String; Graph title
;			GRPSCAL1   Integer; Scaling activation 1 on, 0 off
;			XMIN1	   String; x-axis minimum for 1st plot
;			XMAX1	   String; x-axis maximum for 1st plot
;			YMIN1	   String; y-axis minimum for 1st plot
;			YMAX1	   String; y-axis maximum for 1st plot
;			GRPSCAL2   Integer; Scaling activation 1 on, 0 off
;			XMIN2	   String; x-axis minimum for 2nd plot
;			XMAX2	   String; x-axis maximum for 2nd plot
;			YMIN2	   String; y-axis minimum for 2nd plot
;			YMAX2	   String; y-axis maximum for 2nd plot
;			HRDOUT	   Integer; Hard copy activ' 1 on, 0 off
;			HARDNAME   String; Hard copy output file name
;			GRPDEF	   String; Default output file name 
;			GRPFMESS   String; File name error message
;			GRPSEL	   Integer; index of selected graph in GRPLIST
;			GRPRMESS   String; Scaling ranges error message
;			DEVSEL	   Integer; index of selected device in DEVLIST
;			GRSELMESS  String; General error message
;                       ASCL       Float; graphical scaling parameter
;
;		  For CW_ADAS_OUTFILE tabular output (paper.txt);
;			TEXOUT	Integer; Activation button 1 on, 0 off
;			TEXAPP	Integer; Append button 1 on, 0 off, 
;					-1 no button
;			TEXREP	Integer; Replace button 1 on, 0 off, 
;					-1 no button
;			TEXDSN	String; Output file name
;			TEXDEF	String; Default file name
;			TEXMES	String; file name error message
;
;
;	UVALUE	- A user value for this widget.
;
;	FONT	- String; the name of a font to be used for all text in
;		  this widget.  Default to system default font.
;
; CALLS:
;	CW_ADAS_DSBR	Input dataset name and comments browse button.
;	CW_105_GR_SEL	Graphical output selection widget.
;	CW_ADAS_OUTFILE	Output file name entry widget.
;	See side effects for other related widget management routines.
;
; SIDE EFFECTS:
;
; 	Two other routines are included in this file
;	OUT105_GET_VAL()
;	OUT105_EVENT()
;
; CATEGORY:
;	Compound Widget
;
; WRITTEN:
;       William Osborn, Tessella Support Services plc., 13th September 1996
;
; MODIFIED:
;	1.1	William Osborn
;		First release. Written using cw_adas101_out.pro
;       1.2     William Osborn
;               Replaced call of cw_101_gr_sel with cw_1xx_gr_sel.
;               ASCL parameter added to out105_set.
; VERSION:
;	1.1	13-09-96
;	1.2	08-10-96
;
;-
;-----------------------------------------------------------------------------

FUNCTION out105_get_val, id

                ;**** Return to caller on error ****
  ON_ERROR, 2

                ;**** Retrieve the state ****

  parent=widget_info(id,/parent)
  widget_control, parent, get_uvalue=state, /no_copy

		;**** Get graphical output settings ****

  widget_control,state.grpid,get_value=gr105val

		;**** Get text output settings ****

  widget_control,state.paperid,get_value=papos

                ;**** save out structure ****  

  os = { out105_set, 						 $
	   GRPOUT:gr105val.outbut, GTIT1:gr105val.gtit1, 	 $
	   GRPSCAL1:gr105val.scalbut1, 			 	 $
	   XMIN1:gr105val.xmin1,   XMAX1:gr105val.xmax1,    	 $
	   YMIN1:gr105val.ymin1,   YMAX1:gr105val.ymax1,    	 $
	   GRPSCAL2:gr105val.scalbut2, 			 	 $
	   XMIN2:gr105val.xmin2,   XMAX2:gr105val.xmax2,    	 $
	   YMIN2:gr105val.ymin2,   YMAX2:gr105val.ymax2,    	 $
	   HRDOUT:gr105val.hrdout, HARDNAME:gr105val.hardname,   $
	   GRPDEF:gr105val.grpdef, GRPFMESS:gr105val.grpfmess,   $
	   GRPSEL:gr105val.grpsel, GRPRMESS:gr105val.grprmess,   $
	   DEVSEL:gr105val.devsel, GRSELMESS:gr105val.grselmess, $
	   TEXOUT:papos.outbut,    TEXAPP:papos.appbut, 	 $
	   TEXREP:papos.repbut,    TEXDSN:papos.filename, 	 $
	   TEXDEF:papos.defname,   TEXMES:papos.message, 	 $
           ASCL:gr105val.ascl                                    $
        }

                ;**** Return the state ****

  widget_control, parent, set_uvalue=state, /no_copy

  RETURN, os 

END

;-----------------------------------------------------------------------------

FUNCTION out105_event, event


                ;**** Base ID of compound widget ****

  parent=event.top

                ;**** Retrieve the state ****

  widget_control, parent, get_uvalue=state, /no_copy

		;*********************************
		;**** Clear previous messages ****
		;*********************************

  widget_control,state.messid,set_value=' '

                ;************************
                ;**** Process events ****
                ;************************

  CASE event.id OF

		;***********************
		;**** Cancel button ****
		;***********************

    state.cancelid: new_event = {ID:parent, TOP:event.top, 		$
				HANDLER:0L, ACTION:'Cancel'}

		;*********************
		;**** Done button ****
		;*********************

    state.doneid: begin

                ;**** Return the state before checking can start ****
		;**** with the get_value keyword.                ****

        widget_control, parent, set_uvalue=state, /no_copy

		;***************************************
		;**** Check for errors in the input ****
		;***************************************

        error = 0

		;**** Get a copy of the widget value ****

	widget_control,event.handler,get_value=os
	
		;**** Check for widget error messages ****

	if os.grpout eq 1 and strtrim(os.grpfmess) ne '' then error=1
	if os.grpout eq 1 and os.grpscal1 eq 1 and 			$
	  			strtrim(os.grprmess) ne '' then error=1
	if os.grpout eq 1 and os.grpscal2 eq 1 and 			$
	  			strtrim(os.grprmess) ne '' then error=1
	if os.grpout eq 1 and strtrim(os.grselmess) ne '' then error=1
	if os.texout eq 1 and strtrim(os.texmes) ne '' then error=1

                ;**** Retrieve the state   ****

        widget_control, parent, get_uvalue=state, /no_copy

	if error eq 1 then begin
	      widget_control,state.messid,set_value= 			$

		'**** Error in output settings ****'

	  new_event = 0L
	end else begin
	  new_event = {ID:parent, TOP:event.top, HANDLER:0L, ACTION:'Done'}
	end

    end
		;*********************
                ;**** Menu button ****
		;*********************

    state.outid: begin
        new_event = {ID:parent, TOP:event.top, HANDLER:0L,          $
                         ACTION:'Menu'}
    end


    ELSE: new_event = 0L

  ENDCASE

                ;**** Return the state   ****

          widget_control, parent, set_uvalue=state, /no_copy

  RETURN, new_event
END

;-----------------------------------------------------------------------------

FUNCTION cw_adas105_out, parent, dsfull, root, bitfile, 	$
		DEVLIST=devlist, VALUE=value, UVALUE=uvalue, FONT=font

  IF (N_PARAMS() EQ 0) THEN MESSAGE, 'Must specify parent for cw_adas105_out'
  ON_ERROR, 2					;return to caller

		;**** Set defaults for keywords ****
  IF NOT (KEYWORD_SET(devlist)) THEN devlist = ''
  IF NOT (KEYWORD_SET(uvalue)) THEN uvalue = 0
  IF NOT (KEYWORD_SET(font)) THEN font = ''
  IF NOT (KEYWORD_SET(value)) THEN begin
	os = {out105_set, 						$
			GRPOUT:0, GTIT1:'', 				$
			GRPSCAL1:0, 					$
			XMIN1:'',  XMAX1:'',   				$
			YMIN1:'',  YMAX1:'',   				$
			GRPSCAL2:0, 					$
			XMIN2:'',  XMAX2:'',   				$
			YMIN2:'',  YMAX2:'',   				$
 		        HRDOUT:0, HARDNAME:'', 				$
	                GRPDEF:'', GRPFMESS:'',				$
			GRPSEL:-1, GRPRMESS:'', 			$
			DEVSEL:-1, GRSELMESS:'', 			$
			TEXOUT:0, TEXAPP:-1, 				$
			TEXREP:0, TEXDSN:'', 				$
                        TEXDEF:'',TEXMES:'', ASCL:0.0d0			$
              }
  END ELSE BEGIN
	os = {out105_set, 						$
			GRPOUT:value.grpout, GTIT1:value.gtit1, 	$
			GRPSCAL1:value.grpscal1, 			$
			XMIN1:value.xmin1,     XMAX1:value.xmax1,   	$
			YMIN1:value.ymin1,     YMAX1:value.ymax1,   	$
			GRPSCAL2:value.grpscal2, 			$
			XMIN2:value.xmin2,     XMAX2:value.xmax2,   	$
			YMIN2:value.ymin2,     YMAX2:value.ymax2,   	$
			HRDOUT:value.hrdout, HARDNAME:value.hardname, 	$
			GRPDEF:value.grpdef, GRPFMESS:value.grpfmess, 	$
			GRPSEL:value.grpsel, GRPRMESS:value.grprmess, 	$
			DEVSEL:value.devsel, GRSELMESS:value.grselmess, $
			TEXOUT:value.texout, TEXAPP:value.texapp, 	$
			TEXREP:value.texrep, TEXDSN:value.texdsn, 	$
			TEXDEF:value.texdef, TEXMES:value.texmes, 	$
                        ASCL:value.ascl                                 $
	      }
  END
		;**********************************************
		;**** Create the 105 Output options widget ****
		;**********************************************

		;**** create base widget ****

  cwid = widget_base( parent, UVALUE = uvalue, 				$
			EVENT_FUNC = "out105_event", 			$
			FUNC_GET_VALUE = "out105_get_val", 		$
			/COLUMN)

		;**** Add dataset name and browse button ****

  rc = cw_adas_dsbr(cwid, dsfull, font=font, button='Browse Index')

		;**** Widget for graphics selection ****
		;**** Note change in names for GRPOUT and GRPSCAL ****

  gr105val = {  OUTBUT:os.grpout, GTIT1:os.gtit1, 			$
                SCALBUT1:os.grpscal1, 					$
		XMIN1:os.xmin1, XMAX1:os.xmax1, 			$
		YMIN1:os.ymin1, YMAX1:os.ymax1, 			$
                SCALBUT2:os.grpscal2, 					$
		XMIN2:os.xmin2, XMAX2:os.xmax2, 			$
		YMIN2:os.ymin2, YMAX2:os.ymax2, 			$
		HRDOUT:os.hrdout, HARDNAME:os.hardname, 		$
		GRPDEF:os.grpdef, GRPFMESS:os.grpfmess, 		$
		GRPSEL:os.grpsel, GRPRMESS:os.grprmess, 		$
		DEVSEL:os.devsel, GRSELMESS:os.grselmess, ASCL:os.ascl  }


                ;**** widget for graphics selection ****

  base = widget_base(cwid,/row,/frame)
  grpid = cw_1xx_gr_sel(base, /SIGN, OUTPUT='Graphical Output', 	$
                        DEVLIST=devlist, VALUE=gr105val, FONT=font, 	$
                        LABEL1='Ratio graph scaling',                   $
                        LABEL2='Ionis. coeff. graph scaling',           $
                        ASCL_YES = 1 )

		;**** Widget for text output ****

  outfval = { OUTBUT:os.texout, APPBUT:-1, REPBUT:os.texrep, 		$
	      FILENAME:os.texdsn, DEFNAME:os.texdef, MESSAGE:os.texmes, $
              ROOT:root }
  base = widget_base(cwid,/row,/frame)
  paperid = cw_adas_outfile(base, OUTPUT='Text Output', 		$
                        VALUE=outfval, FONT=font)

		;**** Error message ****

  messid = widget_label(cwid,value=' ',font=font)

		;**** add the exit buttons ****

  base = widget_base(cwid,/row)
  menufile = bitfile + '/menu.bmp'
  read_X11_bitmap, menufile, bitmap1
  outid = widget_button(base, value=bitmap1)          ;menu button
  cancelid = widget_button(base,value='Cancel',font=font)
  doneid = widget_button(base,value='Done',font=font)
  
		;**** create a state structure for the pop-up ****
		;**** window.                                 ****

  new_state = { GRPID:grpid, PAPERID:paperid, OUTID:outid,		$
		CANCELID:cancelid, DONEID:doneid, MESSID:messid 	$
                }

                ;**** Save initial state structure ****

  widget_control, parent, set_uvalue=new_state, /no_copy

  RETURN, cwid

END

