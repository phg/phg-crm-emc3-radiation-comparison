; Copyright (c) 1996, Strathclyde University.
; SCCS info: Module @(#)$Header: /home/adascvs/idl/adas1xx/adas106/cw_adas106_proc.pro,v 1.1 2004/07/06 12:28:16 whitefor Exp $ Date $Date: 2004/07/06 12:28:16 $
;+
; PROJECT:
;       ADAS IBM MVS to   UNIX conversion
;
; NAME:
;	CW_ADAS106_PROC()
;
; PURPOSE:
;	Produces a widget for ADAS106 processing options/input.
;
; EXPLANATION:
;	This function creates a compound widget consisting of :-
;	   a text widget holding an editable 'Run title', 
;	   the dataset name/browse widget cw_adas_dsbr,
;          various input option widgets,
;	   a table widget for temperature/density data cw_adas_table,
;	   buttons to enter default values into the table, 
;	   a message widget, a 'Cancel' and a 'Done' button, and
;          energy units options buttons.
;
;	The compound widgets included in this widget are self managing.
;	This widget only manages events from the two 'Defaults' buttons,
;	the 'Cancel' button and the 'Done' button, and from the energy units
;       buttons.
;
;	This widget only generates events for the 'Done' and 'Cancel'
;	buttons.
;
; USE:
;	This widget is specific to ADAS106, see adas106_proc.pro
;	for use.
;
; INPUTS:
;	TOPPARENT- Long integer, ID of parent widget.
;
;	DSFULL	- String; The full system file name of the input 
;		  dataset selected by the user for processing.
;
;	LCHOICE - Integer; The archiving choice of the user, see 
;                 a1spf0.pro
;
;	NTDIM	- Integer; maximum number of temperatures allowed.
;
;	NEDIM	- Integer; maximum number of energies allowed.
;
;	NXDIM	- Integer; maximum number of x-sections allowed.
;
;	TEMP	- float array; The temperature values (K).
;
;	ENG	- float array; The energy values (Ryd).
;
;	RATE	- float array; The rate values (X).
;
;       BITFILE - String; the path to the directory containing bitmaps
;                 for the 'escape to series menu' button.
;
; OPTIONAL INPUTS:
;	None
;
; OUTPUTS:
;	The return value of this function is the ID of the declared widget.
;
; OPTIONAL OUTPUTS:
;	None
;
; KEYWORD PARAMETERS:
;	PROCVAL	- A structure which determines the initial settings of
;		  the processing options widget.
;
;		  The default PROCVAL is created thus;
;      temp_arr = fltarr(ntdim)
;      rate_arr = fltarr(nxdim)
;      eng_arr = fltarr(nedim)
;	tlarr  = lonarr(2)
;	tiarr  = intarr(2)
;	tsiarr = intarr(2,6)
;	tsfarr = fltarr(2,6)
;	ifarr  = intarr(3)
;     ps = { 			 		  $
;                new    : 0,                      $
;		 maxe   : 0,
;		 maxx	: 0,
;		 maxt	: 0,
;		 tin	: temp_arr,
;		 ein 	: eng_arr,
;		 xin	: rate_arr,
;		 z	: 0,
;		 z0	: 0,
;		 z1 	: 0,
;		 iop	: 0,
;		 nigrp  : 0,
;		 cia	: tlarr,
;		 nshela	: tiarr,
;		 ins	: tsiarr,
;		 ils	: tsiarr,
;		 es 	: tsfarr,
;		 izs	: tsiarr,
;		 nrgrp  : 0,
;		 cra	: tlarr,
;		 nresoa	: tiarr,
;		 er	: tsfarr,
;		 wr	: tsfarr,
;		 ifout  : ifarr
;              }
;
;		NEW     Flag which defines whether or not default values
;			exist or not. (< 0 if no values exist)
;		MAXE    Number of energy values entered/refreshed
;		MAXX    Number of x-sections values entered/refreshed
;		MAXT    Number of temperature values entered/refreshed
;		TIN     User supplied temperature values.
;		EIN 	User supplied energy values.
;		XIN 	User supplied x-section values
;               Z       Initial Ion Charge
;               Z0      Nuclear Charge
;               Z1      Final Ion Charge
;		IOP	Default scaling option. 0=No, 1=Yes.
;		NRGRP	Number of shell groups
;		CIA	Scaling factors for shell groups
;		NSHELA	Number of entries for each shell group
;		INS	Shell group data: N
;		ILS	Shell group data: L
;		ES	Shell group data: EION
;		IZS	Shell group data: IZETA
;		NRGRP	Number of resonance groups
;		CRA	Scaling factors for resonance groups
;		NRESOA	Number of entries for each resonance group
;		ER	Resonance group data: ENERGY
;		WR	Resonance group data: WEIGHT
;		IFOUT	Units for the input data
;
;	UVALUE	- A user value for the widget. Default 0.
;
;	FONT_LARGE - The name of a larger font.  Default current system
;		     font
;
;	FONT_SMALL - The name of a smaller font. Default current system
;		     font.
;
;	EDIT_FONTS - A structure of two fonts used in the table editor
;		     adas_edtab.pro, {font_norm:'',font_input:''}.  Defaults
;		     to current system font.
;
;	NUM_FORM   - String; Numeric format to use in tables.  Default
;			'(E10.3)'
;		     
;
; CALLS:
;	POPUP		Popup warning window with buttons.
;	CW_ADAS_DSBR	Dataset name and comments browsing button.
;	CW_ADAS_TABLE	Adas data table widget.
;       CW_TMPL         Transition Information
;	See side effects for other related widget management routines.
;
; SIDE EFFECTS:
;
;	The following widget management routines are included in this file;
;	PROC106_GET_VAL()	Returns the current PROCVAL structure.
;	PROC106_EVENT()		Process and issue events.
;	
; CATEGORY:
;	Compound Widget
;
; WRITTEN:
;       William Osborn, Tessella Support Services plc., 07-10-96
;
; MODIFIED:
;	1.1	William Osborn
;		First release - copied cw_adas105_proc.pro
;
; VERSION:
;	1.1	07-10-96
;
;-
;-----------------------------------------------------------------------------

FUNCTION proc106_get_val, id

                ;**** Return to caller on error ****
;  ON_ERROR, 2

		 ;***************************************
                 ;****     Retrieve the state 	     ****
		 ;**** Get first_child widget id     ****
		 ;**** because state is stored there ****
		 ;***************************************

  first_child = widget_info(id, /child)
  widget_control, first_child, get_uvalue = state

		;***************************
		;**** Get input options ****
		;***************************

  widget_control, state.z0id, get_value=val
  if num_chk(val(0)) eq 0 then z0 = float(val(0)) else z0 = 0.0
  widget_control, state.zid, get_value=val
  if num_chk(val(0)) eq 0 then z = float(val(0)) else z = 0.0
  widget_control, state.z1id, get_value=val
  if num_chk(val(0)) eq 0 then z1 = float(val(0)) else z1 = 0.0
  widget_control, state.defid, get_value = val
  iop = val(0)

		;******************************
		;**** Get shell group info ****
		;******************************

  cia = fltarr(2)
  ins = intarr(2,6)
  ils = intarr(2,6)
  es = fltarr(2,6)
  izs = intarr(2,6)
  nshela = intarr(2)
  widget_control, state.nigrpid, get_value = val
  nigrp = val(0)
  for i=0,nigrp-1 do begin
    widget_control, state.ciaid(i), get_value = val
    if num_chk(val(0)) eq 0 then cia(i)=val(0)
    widget_control, state.shtid(i), get_value = val
    data = val.value
    blanks = where(strtrim(data(0,*),2) eq '')
    if blanks(0) ge 0 then nshela(i) = blanks(0) else nshela(i)=6
    if nshela(i) gt 0 then begin
      ins(i,0:nshela(i)-1)=fix(data(0,0:nshela(i)-1))
      ils(i,0:nshela(i)-1)=fix(data(1,0:nshela(i)-1))
      es(i,0:nshela(i)-1)=float(data(2,0:nshela(i)-1))
      izs(i,0:nshela(i)-1)=fix(data(3,0:nshela(i)-1))
    endif
  endfor
		;**********************************
		;**** Get resonance group info ****
		;**********************************

  cra = fltarr(2)
  er = fltarr(2,6)
  wr = fltarr(2,6)
  nresoa = intarr(2)
  widget_control, state.nrgrpid, get_value = val
  nrgrp = val(0)
  for i=0,nrgrp-1 do begin
    widget_control, state.craid(i), get_value = val
    if num_chk(val(0)) eq 0 then cra(i)=val(0)
    widget_control, state.retid(i), get_value = val
    data = val.value
    blanks = where(strtrim(data(0,*),2) eq '')
    if blanks(0) ge 0 then nresoa(i) = blanks(0) else nresoa(i)=6
    if nresoa(i) gt 0 then begin
      er(i,0:nresoa(i)-1)=float(data(0,0:nresoa(i)-1))
      wr(i,0:nresoa(i)-1)=float(data(1,0:nresoa(i)-1))
    endif
  endfor
		;****************************************************
		;**** Get new temperature data from table widget ****
		;****************************************************

  widget_control,state.tempid,get_value=tempval
  tabledata = tempval.value
  ifout = intarr(3)
  ifout(0) = tempval.units(0)
  ifout(1) = tempval.units(1)+3
  ifout(2) = tempval.units(2)+7

		;**** Copy out temperature values ****

  ein = dblarr(state.nedim)
  xin = dblarr(state.nxdim)
  tin = dblarr(state.ntdim)
  blanks = where(strtrim(tabledata(ifout(0),*),2) eq '')
		;**** next line assumes that all blanks are ****
		;**** at the end of the columns             ****
  if blanks(0) ge 0 then maxe=blanks(0) else maxe=state.nedim
  if maxe gt 0 then ein(0:maxe-1) = double(tabledata(ifout(0),0:maxe-1))
  blanks = where(strtrim(tabledata(ifout(1),*),2) eq '')
  if blanks(0) ge 0 then maxx=blanks(0) else maxx=state.nxdim
  if maxx gt 0 then xin(0:maxx-1) = double(tabledata(ifout(1),0:maxx-1))
  blanks = where(strtrim(tabledata(ifout(2),*),2) eq '')
  if blanks(0) ge 0 then maxt=blanks(0) else maxt=state.ntdim
  if maxt gt 0 then tin(0:maxt-1) = double(tabledata(ifout(2),0:maxt-1))

		;**** Fill out the rest with zero ****

  if maxt lt state.ntdim then begin
    tin(maxt:state.ntdim-1) = double(0.0)
  endif
  if maxx lt state.nxdim then begin
    xin(maxx:state.nxdim-1) = double(0.0)
  endif 
  if maxe lt state.nedim then begin
    xin(maxe:state.nedim-1) = double(0.0)
  endif 

		;***********************************************
		;**** write selected values to PS structure ****
		;***********************************************

     ps = { 			 		 $
                new    : 0,                      $
		maxe 	: maxe,				$
		maxx	: maxx,				$
		maxt	: maxt,				$
		tin	: tin,				$
		ein 	: ein,				$
		xin	: xin,				$
		z	: z,				$
		z0	: z0,				$
		z1 	: z1,				$
		iop	: iop,				$
		nigrp	: nigrp,			$
		cia	: cia,				$
		nshela	: nshela,			$
		ins	: ins,				$
		ils	: ils,				$
		es 	: es,				$
		izs	: izs,				$
		nrgrp	: nrgrp,			$
		cra	: cra,				$
		nresoa	: nresoa,			$
		er	: er,				$
		wr	: wr,				$
		ifout	: ifout				$
              }
   
  widget_control, first_child, set_uvalue=state, /no_copy

  RETURN, ps

END

;-----------------------------------------------------------------------------

FUNCTION proc106_event, event

                ;**** Base ID of compound widget ****

  parent=event.handler

		;**********************************************
                ;**** Retrieve the user value state        ****
		;**** Get id of first_child widget because ****
		;**** user value "state" is stored there   ****
		;**********************************************

  first_child = widget_info(parent, /child)
  widget_control, first_child, get_uvalue=state,/no_copy

		;*********************************
		;**** Clear previous messages ****
		;*********************************

  widget_control,state.messid,set_value=' '

                ;************************
                ;**** Process events ****
                ;************************

  cASE event.id OF

		;*************************************
		;**** Default temperature button ****
		;*************************************

    state.deftid: begin

		;**** popup window to confirm overwriting current values ****

	action= popup(message='Confirm Overwrite values with Defaults', $
			buttons=['Confirm','Cancel'],font=state.font)

	if action eq 'Confirm' then begin

		;**** Get current table widget value ****

 	   widget_control,state.tempid,get_value=tempval

;           units = tempval.units(2)+7
	   units = 8
	   tempval.units(2) = 1
  	   maxt = state.maxt
 	   if maxt gt 0 then begin

                ;**** default temperature values ****
                ;**** can add up to maxt-1 as required ****

;            tvalues = [2.50e+04,5.00e+04,1.25e+05,2.50e+05,5.00e+05,	$
;                       1.25e+06,2.50e+06,5.00e+06]
	    tvalues = [1.000e+00, 2.000e+00, 3.000e+00, 4.000e+00,	$
		 5.000e+00, 7.000e+00, 1.000e+01, 1.500e+01, 2.000e+01,	$
		 3.000e+01, 4.000e+01, 5.000e+01, 7.000e+01, 1.000e+02,	$
		 1.500e+02, 2.000e+02, 3.000e+02, 4.000e+02, 5.000e+02,	$
		 7.000e+02, 1.000e+03, 2.000e+03, 5.000e+03, 1.000e+04]
            ndef = n_elements(tvalues)
            deftemp = strarr(ndef)
            for i = 0,ndef-1 do begin
               deftemp(i) = strtrim(string(tvalues(i),format=state.num_form))
            end
            for i = 0,maxt-1 do begin
               if i le ndef-1 then begin
   	         tempval.value(units,i) =  $
			   string(deftemp(i) , format=state.num_form)
               end else begin
                 tempval.value(units,i) = ''
               end
            endfor
 	  end

		;**** Fill out the rest with blanks ****

 	  if maxt lt state.ntdim then begin
            tempval.value(units,maxt:state.ntdim-1) = ''
 	  end

		;**** Copy new data to table widget ****

 	  widget_control,state.tempid, set_value=tempval
	end

      end

		;****************************
		;**** User input options ****
		;****************************

    state.z0id: widget_control, state.zid, /input_focus
    state.zid: widget_control, state.z1id, /input_focus
    state.z1id: widget_control, state.z0id, /input_focus

		;******************************
		;**** Group display button ****
		;******************************
    state.showid: begin
	if event.value eq 0 then begin
	    widget_control, state.shellbase, /map
	    widget_control, state.resbase, map=0
	    widget_control, state.showid, set_value=0
	endif else begin
	    widget_control, state.shellbase, map=0
	    widget_control, state.resbase, /map
	    widget_control, state.showid, set_value=1
	endelse
    end
		;***************************************
		;**** Number of shell groups button ****
		;***************************************
    state.nigrpid: begin
	if event.index eq 0 then begin
	    widget_control, state.shtbase(0), sensitive=0
	    widget_control, state.shtbase(1), sensitive=0
	endif else if event.index eq 1 then begin
	    widget_control, state.shtbase(0), /sensitive, /map
	    widget_control, state.shtbase(1), sensitive=0, map=0
	    widget_control, state.shelbutid, set_value=0
	endif else begin
	    widget_control, state.shtbase(0), /sensitive
	    widget_control, state.shtbase(1), /sensitive
	endelse
    end
		;***********************************
		;**** Shell group choice button ****
		;***********************************
    state.shelbutid: begin
	if event.value eq 0 then begin
	    widget_control, state.shtbase(0), /map
	    widget_control, state.shtbase(1), map=0
	    widget_control, state.shelbutid, set_value=0
	endif else begin
	    widget_control, state.shtbase(0), map=0
	    widget_control, state.shtbase(1), /map
	    widget_control, state.shelbutid, set_value=1
	endelse
    end
		;*******************************************
		;**** Number of resonance groups button ****
		;*******************************************
    state.nrgrpid: begin
	if event.index eq 0 then begin
	    widget_control, state.retbase(0), sensitive=0
	    widget_control, state.retbase(1), sensitive=0
	endif else if event.index eq 1 then begin
	    widget_control, state.retbase(0), /sensitive, /map
	    widget_control, state.retbase(1), sensitive=0, map=0
	    widget_control, state.resbutid, set_value=0
	endif else begin
	    widget_control, state.retbase(0), /sensitive
	    widget_control, state.retbase(1), /sensitive
	endelse
    end
		;***************************************
		;**** Resonance group choice button ****
		;***************************************
    state.resbutid: begin
	if event.value eq 0 then begin
	    widget_control, state.retbase(0), /map
	    widget_control, state.retbase(1), map=0
	    widget_control, state.resbutid, set_value=0
	endif else begin
	    widget_control, state.retbase(0), map=0
	    widget_control, state.retbase(1), /map
	    widget_control, state.resbutid, set_value=1
	endelse
    end

		;***********************
		;**** Cancel button ****
		;***********************

    state.cancelid: new_event = {ID:parent, TOP:event.top, 		$
				HANDLER:0L, ACTION:'Cancel'}

		;*********************
		;**** Done button ****
		;*********************

    state.doneid: begin

		;***************************************
		;**** Check all user input is legal ****
		;***************************************

	error = 0

        	;********************************************
		;*** Have to restore "state" before calls ***
		;*** to proc106_get_val so it can be used ***
		;*** there.				  ***
		;********************************************
		
	widget_control, first_child, set_uvalue=state, /no_copy

	widget_control,event.handler,get_value=ps

		;*** reset state variable ***

	widget_control, first_child,get_uvalue=state, /no_copy

		;**** Check input parameter values ****

        widget_control, state.z0id, get_value=val
	if num_chk(val(0)) ne 0 then begin
	  error = 1
	  message = 'Error: Z0 not a number'
	  widget_control, state.z0id, /input_focus
    	endif
        widget_control, state.zid, get_value=val
	if num_chk(val(0)) ne 0 then begin
	  error = 1
	  message = 'Error: Z not a number'
	  widget_control, state.zid, /input_focus
    	endif
        widget_control, state.z1id, get_value=val
	if num_chk(val(0)) ne 0 then begin
	  error = 1
	  message = 'Error: Z1 not a number'
	  widget_control, state.z1id, /input_focus
    	endif
	for i=0,ps.nigrp-1 do begin
	  widget_control, state.ciaid(i), get_value=val
	  if num_chk(val(0)) ne 0 then begin
	    error = 1
	    message = 'Error: scaling parameter not a number'
	    widget_control, state.shellbase, map=1
	    widget_control, state.resbase, map=0
            widget_control, state.shtbase(i), map=1
            widget_control, state.shtbase((i+1) MOD 2), map=0
            widget_control, state.ciaid(i), /input_focus
	    widget_control, state.shelbutid, set_value = i
	    widget_control, state.showid, set_value = 0
    	  endif
	endfor
	for i=0,ps.nrgrp-1 do begin
	  widget_control, state.craid(i), get_value=val
	  if num_chk(val(0)) ne 0 then begin
	    error = 1
	    message = 'Error: scaling parameter not a number'
	    widget_control, state.shellbase, map=0
	    widget_control, state.resbase, map=1
            widget_control, state.retbase(i), map=1
            widget_control, state.retbase((i+1) MOD 2), map=0
            widget_control, state.craid(i), /input_focus
	    widget_control, state.retbutid, set_value = i
	    widget_control, state.showid, set_value = 1
    	  endif
	endfor
		;**** check temp/density values entered ****

	if error eq 0 and ps.maxt eq 0 then begin
	  error = 1
	  message='Error: No temperatures entered.'
        end
	if error eq 0 and ps.maxx eq 0 then begin
          error = 1
          message='Error: No x-sects entered.'
        end
	if error eq 0 and ps.maxe eq 0 then begin
          error = 1
          message='Error: No energies entered.'
        end


		;**** return value or flag error ****

	if error eq 0 then begin
	  new_event = {ID:parent, TOP:event.top, HANDLER:0L, ACTION:'Done'}
        end else begin
	  widget_control,state.messid,set_value=message
	  new_event = 0L
        end

      end

		;*********************
                ;**** Menu button ****
		;*********************

        state.outid: begin
            new_event = {ID:parent, TOP:event.top, HANDLER:0L,          $
                         ACTION:'Menu'}
        end

    ELSE: new_event = 0L

  ENDCASE

		;*********************************************
		;*** make "state" available to other files ***
		;*********************************************

  widget_control, first_child, set_uvalue=state,/no_copy

  RETURN, new_event
END

;-----------------------------------------------------------------------------

function cw_adas106_proc, topparent, dsfull, lchoice, nedim, nxdim, 	$
                          ntdim, eng, rate, temp, bitfile, 		$
			  procval=procval, 				$
                          uvalue=uvalue, font_large=font_large, 	$
                          font_small=font_small, edit_fonts=edit_fonts, $
                          num_form=num_form

		;**** Set defaults for keywords ****

  if not (keyword_set(uvalue)) then uvalue = 0
  if not (keyword_set(font_large)) then font_large = ''
  if not (keyword_set(font_small)) then font_small = ''
  if not (keyword_set(edit_fonts)) then 				$
				edit_fonts = {font_norm:'',font_input:''}
  if not (keyword_set(num_form)) then num_form = '(E10.4)'

  if not (keyword_set(procval)) then begin     
    temp_arr = fltarr(ntdim)
    rate_arr = fltarr(nxdim)
    eng_arr = fltarr(nedim)
    tfarr  = fltarr(2)
    tiarr  = intarr(2)
    tsiarr = intarr(2,6)
    tsfarr = fltarr(2,6)
    ifarr  = intarr(3)

    ps = { 			 		$
                new    	: 0,                    $
		maxe 	: nedim,		$
		maxx	: nxdim,		$
		maxt	: ntdim,		$
		tin	: temp_arr,		$
		ein 	: eng_arr,		$
		xin	: rate_arr,		$
		z	: 0,			$
		z0	: 0,			$
		z1 	: 0,			$
		iop	: 0,			$
		nigrp	: 0,			$
		cia	: tfarr,		$
		nshela	: tiarr,		$
		ins	: tsiarr,		$
		ils	: tsiarr,		$
		es 	: tsfarr,		$
		izs	: tsiarr,		$
		nrgrp	: 0,			$
		cra	: tfarr,		$
		nresoa	: tiarr,		$
		er	: tsfarr,		$
		wr	: tsfarr,		$
		ifout	: ifarr			$
              }
	ps.ifout(1)=3
	ps.ifout(2)=7
  endif else begin
    ps = { 			 			$
                new    	: procval.new,			$
		maxe 	: procval.maxe,			$
		maxx	: procval.maxx,			$
		maxt	: procval.maxt,			$
		tin	: procval.tin,			$
		ein 	: procval.ein,			$
		xin	: procval.xin,			$
		z	: procval.z,			$
		z0	: procval.z0,			$
		z1 	: procval.z1,			$
		iop	: procval.iop,			$
		nigrp	: procval.nigrp,		$
		cia	: procval.cia,			$
		nshela	: procval.nshela,		$
		ins	: procval.ins,			$
		ils	: procval.ils,			$
		es 	: procval.es,			$
		izs	: procval.izs,			$
		nrgrp	: procval.nrgrp,		$
		cra	: procval.cra,			$
		nresoa	: procval.nresoa,		$
		er	: procval.er,			$
		wr	: procval.wr,			$
		ifout	: procval.ifout			$
              }
  endelse

                ;*********************************************************
                ;**** Modify certain parameters and results depending ****
                ;**** on the machine being used                       ****
                ;*********************************************************

  machine = GETENV('TARGET_MACHINE')
  if machine eq 'HPUX' then begin
    y_size = 4
    large_font = font_small
  endif else begin
    y_size = 6
    large_font = font_large
  endelse

		;****************************************************
		;**** The adas table widget requires data to be *****
		;**** input as strings, so all the numeric data *****
		;**** has to be written into a string array.    *****
		;**** cols.1,2,3 - energy units               *****
		;**** cols.4,5,6,7 - x-section units          *****
		;**** cols.8,9,10 - Temperature units      *****
		;****************************************************

  maxt = ps.maxt  
  maxx = ps.maxx
  maxe = ps.maxe
  neg_form='(E11.4)'
  tabledata = strarr(10,nxdim)

  if lchoice eq 1 or lchoice eq 2 then begin

    If (maxt gt 0) then begin

      tabledata(0,0:maxe-1) = 						$
	        strtrim(string(eng(0:maxe-1,0),format=num_form),2)
      tabledata(1,0:maxe-1) = 						$
		strtrim(string(eng(0:maxe-1,1),format=num_form),2)
      tabledata(2,0:maxe-1) = 						$
                strtrim(string(eng(0:maxe-1,2),format=num_form),2)
      tabledata(3,0:maxx-1) = 						$
	        strtrim(string(rate(0:maxx-1,0),format=num_form),2)
      tabledata(4,0:maxx-1) = 						$
		strtrim(string(rate(0:maxx-1,1),format=neg_form),2)
      tabledata(5,0:maxx-1) = 						$
                strtrim(string(rate(0:maxx-1,2),format=num_form),2)     
      tabledata(6,0:maxx-1) = 						$
	        strtrim(string(rate(0:maxx-1,3),format=num_form),2)
      tabledata(7,0:maxt-1) = 						$
	        strtrim(string(temp(0:maxt-1,0),format=num_form),2)
      tabledata(8,0:maxt-1) = 						$
		strtrim(string(temp(0:maxt-1,1),format=num_form),2)
      tabledata(9,0:maxt-1) = 						$
                strtrim(string(temp(0:maxt-1,2),format=num_form),2)

    endif
	;**** fill rest of table with blanks ****

    if maxe lt nedim then tabledata(0:2,maxe:nedim-1) = ' '
    if maxx lt nxdim then tabledata(3:6,maxx:nxdim-1) = ' '
    if maxt lt ntdim then tabledata(7:9,maxt:ntdim-1) = ' '

  endif else begin
	;**** If no archive chosen then use the values from the last ****
	;**** time we were here. Different to 101 where set to blank.****
    tabledata(*,*)=' '
    if maxt gt 0 then begin
      tabledata(ps.ifout(0),0:maxe-1) = 				$
	          strtrim(string(ps.ein(0:maxe-1),format=num_form),2)
      tabledata(ps.ifout(1),0:maxx-1) = 				$
		strtrim(string(ps.xin(0:maxx-1),format=num_form),2)
      tabledata(ps.ifout(2),0:maxt-1) = 				$
                strtrim(string(ps.tin(0:maxt-1),format=num_form),2)
    endif
  endelse

		;********************************************************
		;**** Create the 106 Processing options/input window ****
		;********************************************************

		;**** create titled base widget ****

  parent = widget_base(topparent, UVALUE = uvalue, 			$
			title = 'ADAS106 PROCESSING OPTIONS', 		$
			EVENT_FUNC = "proc106_event", 			$
			FUNC_GET_VALUE = "proc106_get_val", 		$
			/COLUMN)

		;******************************************************
		;**** Create a dummy widget just to hold value of *****
		;**** "state" variable so as not to get confused  *****
		;**** with any other values. Adopt IDL practice   *****
		;**** of using first child widget                 *****
		;******************************************************

  first_child = widget_base(parent)

  topbase = widget_base(first_child,/column)

		;*******************************************************
		;**** add run title, dataset name and browse button ****
		;*******************************************************

  rc = cw_adas_dsbr(topbase, dsfull, font=large_font,			$
                    button='Browse Index')


		;*******************************
		;**** Add the input options ****
		;*******************************

  inpbase = widget_base(topbase, /row)
  base1 = widget_base(inpbase, /column, /frame)
  base1a = widget_base(base1, /row)
  base1b = widget_base(base1, /row)
  base1c = widget_base(base1, /row)
  base2 = widget_base(base1, /row)
  base2a = widget_base(base2, /column)
  l = widget_label(base1a, value='Nuclear charge Z0   ', font=large_font)
  z0id = widget_text(base1a, value=string(ps.z0,format=num_form), $
		font=large_font, /editable, xsize=10)
  l = widget_label(base1b, value='Initial ion charge Z', font=large_font)
  zid = widget_text(base1b, value=string(ps.z, format=num_form),  $
		font=large_font, /editable, xsize=10)
  l = widget_label(base1c, value='Final ion charge Z1 ', font=large_font)
  z1id = widget_text(base1c, value=string(ps.z1, format=num_form),$
		font=large_font, /editable, xsize=10)
  l = widget_label(base2a, value='Use default scaling', font=large_font)
  l = widget_label(base2a, value='parameters?', font=large_font)
  defid = cw_bselector(base2, ['No ','Yes'], set_value=ps.iop, 	$
		font=large_font)
  l = widget_label(base1, value="If 'No' then optimised scaling", 	$
		font=large_font)
  l = widget_label(base1, value="will be used", font=large_font)
  showid = cw_bgroup(base1, ['Shell','Resonance'], 		$
		font=large_font, /exclusive,			$
		set_value=0, /row, label_left='Display groups: ')
		
		;**********************************************
		;**** Bases for shell and resonance groups ****
		;**********************************************
  srbase = widget_base(inpbase)
  shellbase = widget_base(srbase,/column,/frame)
  resbase = widget_base(srbase,/column,/frame, map=0)

		;**********************
		;**** Shell groups ****
		;**********************

  base1 = widget_base(shellbase, /row)
  base2 = widget_base(shellbase,/column)
  base3 = widget_base(shellbase)
  l = widget_label(base1, value = 'No. of shell groups:',font=large_font)
  nigrpid = cw_bselector(base1, ['0','1','2'], set_value=ps.nigrp,$
	font=large_font)
  shelbutid = cw_bgroup(base1, ['1','2'], font=large_font, /exclusive,$
		set_value=0, /row, label_left='Group: ')
		;*** bases for the two tables ***
  shtbase = lonarr(2)
  shtbase(0) = widget_base(base3, /column, /frame)
  shtbase(1) = widget_base(base3, /column, /frame, map=0)
  shtdata = strarr(4,6)
  shtid = lonarr(2)
  ciaid = lonarr(2)
  colhead =['N','L','EION(RYD)','IZETA']
  for i=0,1 do begin
    base = widget_base(shtbase(i),/row)
    l = widget_label(base, value='Default scaling parameter', 	$
		font=large_font)
    ciaid(i) = widget_text(base, value=string(ps.cia(i), format=num_form),$
		font=large_font, /editable, xsize=10)
		;************************************************************
		;**** Create table of information for each shell group   ****
		;************************************************************
    i1 = ps.nshela(i)
    if i1 gt 0 then begin
      shtdata(0,0:i1-1)=strtrim(string(ps.ins(i,0:i1-1,0)),2)
      shtdata(1,0:i1-1)=strtrim(string(ps.ils(i,0:i1-1,0)),2)
      shtdata(2,0:i1-1)=strtrim(string(ps.es(i,0:i1-1,0),	$
		format=num_form),2)
      shtdata(3,0:i1-1)=strtrim(string(ps.izs(i,0:i1-1,0)),2)
    endif
    if i1 lt 6 then begin
      shtdata(0:3,i1:5)=' '
    endif
		;****************************
		;**** table of data   *******
		;****************************

    shtid(i) = cw_adas_table(shtbase(i), shtdata,			$
                        COLHEAD=colhead, 				$
			LIMITS = [1,1,0,1], CELLSIZE = 12, 		$
			FLTINT = ['(I2)','(I2)',num_form,'(I2)'],	$
			FONTS = edit_fonts, FONT_LARGE = large_font, 	$
			FONT_SMALL = font_small)

		;**************************************************************
		;**** Sensitize the base according to the number of groups ****
		;**************************************************************
    if ps.nigrp le i then widget_control, shtbase(i), sensitive=0

  endfor


		;**************************
		;**** Resonance groups ****
		;**************************

  base1 = widget_base(resbase, /row)
  base2 = widget_base(resbase,/column)
  base3 = widget_base(resbase)
  l = widget_label(base1, value = 'No. of resonance groups:',font=large_font)
  nrgrpid = cw_bselector(base1, ['0','1','2'], set_value=ps.nrgrp,$
	font=large_font)
  resbutid = cw_bgroup(base1, ['1','2'], font=large_font, /exclusive,$
		set_value=0, /row, label_left='Group: ')
		;*** bases for the two tables ***
  retbase = lonarr(2)
  retbase(0) = widget_base(base3, /column,/frame)
  retbase(1) = widget_base(base3, /column,/frame, map=0)
  retdata = strarr(2,6)
  retid = lonarr(2)
  craid = lonarr(2)
  colhead =['ENERGY(RYD)','WEIGHT']
  for i=0,1 do begin
    base = widget_base(retbase(i),/row)
    l = widget_label(base, value='Default scaling parameter', 	$
		font=large_font)
    craid(i) = widget_text(base, value=string(ps.cra(i),format=num_form),$
		font=large_font, /editable, xsize=10)
		;************************************************************
		;**** Create table of information for each shell group   ****
		;************************************************************
    i1 = ps.nresoa(i)
    if i1 gt 0 then begin
      retdata(0,0:i1-1)=strtrim(string(ps.er(i,0:i1-1,0),	$
		format=num_form),2)
      retdata(1,0:i1-1)=strtrim(string(ps.wr(i,0:i1-1,0),	$
		format=num_form),2)
    endif
    if i1 lt 6 then begin
      retdata(0:1,i1:5)=' '
    endif
		;****************************
		;**** table of data   *******
		;****************************

    retid(i) = cw_adas_table(retbase(i), retdata,			$
                        COLHEAD=colhead, 				$
			LIMITS=[1,1,0,1], CELLSIZE = 12, 		$
			NUM_FORM = num_form,				$
			FONTS = edit_fonts, FONT_LARGE = large_font, 	$
			FONT_SMALL = font_small)

		;**************************************************************
		;**** Sensitize the base according to the number of groups ****
		;**************************************************************
    if ps.nrgrp le i then widget_control, retbase(i), sensitive=0

  endfor


		;**********************
		;**** Another base ****
		;**********************

  tablebase = widget_base(topbase,/row)

		;************************************************
		;**** base for the table and defaults button ****
		;************************************************

  base = widget_base(tablebase,/column,/frame)

  colhead = ['Input Temp.','Input Rate Coefft.','Output Temp.']


		;********************************************
		;**** convert FORTRAN index to IDL index ****
		;********************************************

  units = intarr(3)
  units(0) = ps.ifout(0)
  units(1) = ps.ifout(1)-3
  units(2) = ps.ifout(2)-7

  unitname = [['Kelvin','Ev','Scaled(Te(K)/Z1^2)','Dummy    '],$
              ['Q (cm^3 sec-1)','Gamma',$
               'Scaled (Z1^3*Q)','Z1^2*Gamma'],$
              ['Kelvin     ',$
               'eV         ','Scaled(Te(K)/(Z1^2))','Dummy       ']]

		;**********************************************************
		;**** three columns in the table and 12 sets of units. ****
		;**** column 1 switches between sets 0,1,& 2           ****
		;**** in the input array tabledata as the units change.****
		;**** Similarly for columns 2 and 3                    ****
		;**********************************************************

  unitind = [[0,1,2,2],[3,4,5,6],[7,8,9,9]]
  unitstitle = ['Input temp.units','Rate coefft.units','Output temp.units']


		;****************************
		;**** table of data   *******
		;****************************

  tempid = cw_adas_table(base, tabledata, units, unitname, unitind, 	$
			UNITSTITLE = unitstitle, 			$
                        COLHEAD=colhead, 				$
			ORDER = [1,0,1], /DIFFLEN, 			$ 
			LIMITS = [1,0,1], CELLSIZE = 12, 		$
			/SCROLL, ytexsize=y_size, NUM_FORM = num_form, 	$
			FONTS = edit_fonts, FONT_LARGE = large_font, 	$
			FONT_SMALL = font_small)

		;*************************
		;**** Default buttons ****
		;*************************

  tbase = widget_base(base, /column)
  deftid = widget_button(tbase,value=' Default Output Temperature Values  ',$
		font=large_font)

		;**** Error message ****

  messid = widget_label(parent,font=large_font, 			$
  value='Edit the processing options data and press Done to proceed')

		;**** add the exit buttons ****

  base = widget_base(parent,/row)
  menufile = bitfile + '/menu.bmp'
  read_X11_bitmap, menufile, bitmap1
  outid = widget_button(base, value=bitmap1)          ;menu button
  cancelid = widget_button(base,value='Cancel',font=large_font)
  doneid = widget_button(base,value='Done',font=large_font)
  
		;**** create a state structure for the pop-up ****
		;**** 		     window.                  ****


   new_state = {messid:messid,          $
		deftid:deftid,		$
		tempid:tempid, 		$
		cancelid:cancelid,	$
		doneid:doneid, 		$
		outid:outid,		$
		z0id:z0id,		$
		zid:zid,		$
		z1id:z1id,		$
		defid:defid,		$
		nigrpid:nigrpid,	$
		shelbutid:shelbutid,	$
		shtbase:shtbase,	$
		shellbase:shellbase,	$
		resbase:resbase,	$
		showid:showid,		$
		shtid:shtid,		$
		ciaid:ciaid,		$
		nrgrpid:nrgrpid,	$
		resbutid:resbutid,	$
		retbase:retbase,	$
		retid:retid,		$
		craid:craid,		$
		ntdim:ntdim,		$
		nxdim:nxdim,		$
                nedim:nedim,            $
		maxt:maxt, 		$
		maxx:maxx, 		$
                maxe:maxe,              $
		font:font_large,	$
		num_form:num_form       $
	      }

                 ;**** Save initial state structure ****

   widget_control, first_child, set_uvalue=new_state,/no_copy

  RETURN, parent

END

