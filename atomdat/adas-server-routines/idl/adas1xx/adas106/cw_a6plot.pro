; Copyright (c) 1996 Strathclyde University .
; SCCS info: Module @(#)$Header: /home/adascvs/idl/adas1xx/adas106/cw_a6plot.pro,v 1.1 2004/07/06 12:25:53 whitefor Exp $ Date $Date: 2004/07/06 12:25:53 $
;+
; NAME: CW_A6PLOT
;
; PURPOSE:
;       To create a widget which displays the two plots for adas106
;       and allows a choice of archiving and printing.
;
; INPUTS:
;       PARENT - The ID of the parent widget.
;
;       BITFILE - String; the path to the directory containing bitmaps
;                 for the 'escape to series menu' button.
;
; KEYWORD PARAMETERS:
;       FONT   - The name of a font
;	UVALUE - Supplies the user value for the widget.
;       PLOTVAL- A struicture containing the value of the window display.
;
; OUTPUTS:
;       The ID of the created widget is returned.
;
; SIDE EFFECTS:
;      None
;
; CALLS:
;       A6PLOT_EVENT - The event handling routine
;
; INCLUDED:
;       A6PLOT_GET_VALUE- The procedure to get the value of the widget
;
; WRITTEN:
;       William Osborn, Tessella Support Services plc., 07-10-96
;
; MODIFICATION HISTORY:
;	1.1	William Osborn
;		First written using cw_a5plot as template
;
; VERSION
;	1.1	07-10-96
;
;-----------------------------------------------------------------------

PRO a6plot_set_value, id, value

		;**** Return to caller on error ****
  ON_ERROR, 2

		;**** Retrieve the state ****
  stash = widget_info(id, /child)
  widget_control, stash, get_uvalue=state, /no_copy

		;**** Update screen message ****
  widget_control,state.messid,set_value=value.message

		;**** Save state structure ****
  widget_control, stash, set_uvalue=state, /no_copy

END


;-----------------------------------------------------------------------

FUNCTION a6plot_get_value, id

	        ;**** Return to caller ****
  ON_ERROR, 2

	        ;**** Retrieve the structure ****
  stash = WIDGET_INFO(id, /CHILD)
  WIDGET_CONTROL, stash, GET_UVALUE=state, /NO_COPY

	        ;**** Get the graphics area value ****
  widget_control,state.drawid,get_value=window


  ps = { WIN:window }

  WIDGET_CONTROL, stash, SET_UVALUE=state, /NO_COPY

                ;**** Return the value ****
return,ps

END

;-----------------------------------------------------------------------------

FUNCTION a6plot_event, ev

  parent=ev.handler


	        ;**** Retrieve the structure ****
  stash = WIDGET_INFO(parent, /CHILD)
  WIDGET_CONTROL, stash, GET_UVALUE=state, /NO_COPY
		;*********************************
		;**** Clear previous messages ****
		;*********************************
  widget_control,state.messid,set_value=''


                ;************************
                ;**** Process events ****
                ;************************
  CASE ev.id OF

		;***********************
		;**** Archive button****
		;***********************
    state.archiveid: actn = 'archive'
   		;***********************
		;**** Print button  ****
		;***********************
    state.printid:   actn = 'print'
		;***********************
		;**** Plot 1 button ****
		;***********************
    state.plot1id:  actn = 'plot1'
		;***********************
		;**** Plot 2 button ****
		;***********************
    state.plot2id:  actn = 'plot2'
		;***********************
		;**** Cancel button ****
		;***********************
    state.cancelid:  actn = 'cancel'
		;*********************
		;**** Done button ****
		;*********************
    state.doneid:    actn = 'done'
		;*********************
		;**** Menu button ****
		;*********************
    state.outid:    actn = 'menu'

    ELSE:

  ENDCASE

                ;**** Restore the state structure ****
  WIDGET_CONTROL, stash, SET_UVALUE=state, /NO_COPY


  RETURN, { ID:parent, TOP:ev.top, HANDLER:0L, ACTION:actn  }
END

;-----------------------------------------------------------------------------

FUNCTION cw_a6plot, parent, bitfile, plotval = plotval, UVALUE = uval, $
			FONT = font

  IF (N_PARAMS() EQ 0) THEN MESSAGE, 'Must specify a parent for cw_a6plot'

  ON_ERROR, 2		

	        ;**** Set the defaults for keywords ****
  IF NOT (KEYWORD_SET(uval))  THEN uval = 0
  IF NOT (KEYWORD_SET(font)) THEN font = ''
  IF NOT (KEYWORD_SET(plotval)) THEN begin
    ps = { WIN:0 }
  END ELSE BEGIN
    ps = { WIN:plotval.win }
  END

  mainbase = WIDGET_BASE(parent, UVALUE = uval, $
		EVENT_FUNC = "a6plot_event", $
		FUNC_GET_VALUE = "a6plot_get_value",$
                PRO_SET_VALUE = "a6plot_set_value",/column)

                ;*********************************
                ;**** Create window for plot  ****
                ;*********************************
  plotbase = widget_base(mainbase,/row)
  drawid = widget_draw(plotbase,xsize=950,ysize=750)


		;*****************
		;**** Buttons ****
		;*****************
  spacerow = widget_base(mainbase,/row)
  space = widget_label(spacerow,value='')
  base = widget_base(mainbase,/row,/frame)
		;**** Menu Button ****
  menufile = bitfile + '/menu.bmp'
  read_X11_bitmap, menufile, bitmap1
  outid = widget_button(base, value=bitmap1)

		;**** Print Button ****
  printid = widget_button(base,value='Print',font=font)

		;**** Archive Button ****
  archiveid = widget_button(base,value='Archive',font=font)

                ;**** Button for graph 1 ****
  plot1id = widget_button(base,value='Show Plot 1',font=font)

                ;**** Button for graph 2 ****
  plot2id = widget_button(base,value='Show Plot 2',font=font)

		;**** Cancel Button ****
  cancelid = widget_button(base,value='Cancel',font=font)

		;**** Done Button ****
  doneid = widget_button(base,value='Done',font=font)

		;**** Error message ****
  messid = widget_label(mainbase,font=font,value='')

                ;**** Save the initial state ****
  state = { ARCHIVEID:archiveid, PRINTID:printid, $
            CANCELID:cancelid, DONEID:doneid, MESSID:messid, $
            DRAWID:drawid, FONT:font, OUTID:outid, PLOT1ID:plot1id,$
            PLOT2ID:plot2id}

  WIDGET_CONTROL, WIDGET_INFO(mainbase, /CHILD), SET_UVALUE=state, /NO_COPY

  RETURN, mainbase

END





