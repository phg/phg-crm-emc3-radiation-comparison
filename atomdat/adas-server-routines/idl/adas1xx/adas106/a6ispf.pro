; Copyright (c) 1996 Strathclyde University .
; SCCS info: Module @(#)$Header: /home/adascvs/idl/adas1xx/adas106/a6ispf.pro,v 1.1 2004/07/06 10:02:57 whitefor Exp $ Date $Date: 2004/07/06 10:02:57 $
;+
; PROJECT:
;       ADAS IBM MVS to   UNIX conversion
;
; NAME:
;	A6ISPF
;
; PURPOSE:
;	IDL user interface and communications with ADAS106 FORTRAN
;	process via pipe.
;
; EXPLANATION:
;	The routine begins by reading information from the ADAS106
;	FORTRAN process via a UNIX pipe.  Then part of the ADAS106
;	IDL user interface is invoked to determine how the user
;	wishes to process the input dataset.  When the user's
;	interactions are complete the information gathered with
;	the user interface is written back to the FORTRAN process
;	via the pipe.  Communications are to the FORTRAN subroutine
;	A6ISPF.
;
; USE:
;	The use of this routine is specific to ADAS106, see adas106.pro.
;
; INPUTS:
;	PIPE	- The IDL unit number of the bi-directional pipe to the
;		  ADAS106 FORTRAN process.
;
;	LPEND	- Integer, 0 if the user pressed the 'Done' button
;		  or 1 if the user pressed 'Cancel'.  Maps directly onto
;		  the logical variable LPEND in ADAS106 FORTRAN.
;
;	PROCVAL	- A structure which determines the initial settings of
;		  the processing options widget.  The initial value is
;		  set in adas106.pro.  If adas106.pro passes a blank 
;		  dummy structure of the form {NEW:-1} into PROCVAL then
;		  PROCVAL is reset to a default structure.
;
;		  The PROCVAL structure is
;
;     procval = {                                 $
;                new    : 0,                      $
;                }
;
;		  See cw_adas106_proc.pro for a full description of this
;		  structure.
;	
;
;	DSFULL	- String; The full system file name of the input 
;		  dataset selected by the user for processing.
;
;	LCHOICE	- Integer; Variable to flag user choice of archive
;                 option. 
;                 0 = No Archive
;                 1 = Old Archive
;                 2 = Refresh form Old Archive
;                 3 = New Archive
;
;       BITFILE - String; the path to the directory containing bitmaps
;                 for the 'escape to series menu' button.
;
; OPTIONAL INPUTS:
;	None
;
; INPUT/OUTPUT: 
;                 ADAS Analysis options if refreshing from archive
;       IXOPS   - Optimisation 0-off 1-on
;
;       IBPTS   - Bad Point Option 0-off 1-on
;
;       IFPTS   - Optimising 1-One point, 2-Two point
;
;       IDIFF   - Dipole X-section fitting, 0-Ratio 1-Difference
;
; OUTPUTS:
;	PROCVAL	- On output the structure records the final settings of
;		  the processing selections widget if the user pressed the
;		  'Done' button, otherwise it is not changed from input.
;
;       GOMENU  - Int; flag - set to 1 if user has selected 'escape direct
;                 to series menu , otherwise 0
;
; OPTIONAL OUTPUTS:
;	None
;
; KEYWORD PARAMETERS:
;	FONT_LARGE - The name of a larger font.
;
;	FONT_SMALL - The name of a smaller font.
;
;	EDIT_FONTS - A structure of two fonts used in the table editor
;		     adas_edtab.pro, {font_norm:'',font_input:''}.
;
; CALLS:
;	ADAS106_PROC	Invoke the IDL interface for ADAS106 data
;		 	processing options/input.
;
; SIDE EFFECTS:
;	Two way communications with ADAS106 FORTRAN via the
;	bi-directional UNIX pipe.
;
; CATEGORY:
;	Adas system.
;
; WRITTEN:
;       William Osborn, Tessella Support Services plc., 07-10-96
;       
; VERSION:
;	1.1	William Osborn
;		First written using a5ispf.pro as a template
;
; MODIFIED:
;	1.1	07-10-96
;
;-----------------------------------------------------------------------------

PRO a6ispf, pipe, lpend, procval, dsfull, lchoice, 			$
            ixops, ibpts, ifpts, idiff, bcval, iwhc, bitfile, gomenu,	$
	    FONT_LARGE=font_large, FONT_SMALL=font_small, 		$
	    EDIT_FONTS=edit_fonts


                ;**** Set defaults for keywords ****

  IF NOT (KEYWORD_SET(font_large)) THEN font_large = ''
  IF NOT (KEYWORD_SET(font_small)) THEN font_small = ''
  IF NOT (KEYWORD_SET(edit_fonts)) THEN 				$
                                edit_fonts = {font_norm:'',font_input:''}

		;*******************************************
		;**** Set default value if non provided ****
                ;****  Set energy units off always to   ****
                ;****  force a selection                ****
		;*******************************************

  idum = 0
  readf, pipe, idum  
  nedim = idum
  readf, pipe, idum                               
  iwhc = idum
  nxdim = nedim & ntdim = nedim 
  if (procval.new lt 0) then begin
    temp_arr = fltarr(ntdim)
    rate_arr = fltarr(nxdim)
    eng_arr = fltarr(nedim)
    tfarr  = fltarr(2)
    tiarr  = intarr(2)
    tsiarr = intarr(2,6)
    tsfarr = fltarr(2,6)
    ifarr  = intarr(3)

    procval = { 			 	$
                new    	: 0,                    $
		maxe 	: nedim,		$
		maxx	: nxdim,		$
		maxt	: ntdim,		$
		tin	: temp_arr,		$
		ein 	: eng_arr,		$
		xin	: rate_arr,		$
		z	: 0,			$
		z0	: 0,			$
		z1 	: 0,			$
		iop	: 0,			$
		nigrp	: 0,			$
		cia	: tfarr,		$
		nshela	: tiarr,		$
		ins	: tsiarr,		$
		ils	: tsiarr,		$
		es 	: tsfarr,		$
		izs	: tsiarr,		$
		nrgrp	: 0,			$
		cra	: tfarr,		$
		nresoa	: tiarr,		$
		er	: tsfarr,		$
		wr	: tsfarr,		$
		ifout	: ifarr			$
              }

                ;**** force units to those of the ****
                ;**** archive if chosen ****

;      if lchoice eq 1 or lchoice eq 2 then begin
;          procval.ifout(0) = 3
;          procval.ifout(1) = 7
;          procval.ifout(2) = 10
;      endif else begin
	   procval.ifout(0) = 0
	   procval.ifout(1) = 4
	   procval.ifout(2) = 7
;      endelse

  endif else begin

    procval = { 			 		$
                new    	: procval.new,			$
		maxe 	: procval.maxe,			$
		maxx	: procval.maxx,			$
		maxt	: procval.maxt,			$
		tin	: procval.tin,			$
		ein 	: procval.ein,			$
		xin	: procval.xin,			$
		z	: procval.z,			$
		z0	: procval.z0,			$
		z1 	: procval.z1,			$
		iop	: procval.iop,			$
		nigrp	: procval.nigrp,		$
		cia	: procval.cia,			$
		nshela	: procval.nshela,		$
		ins	: procval.ins,			$
		ils	: procval.ils,			$
		es 	: procval.es,			$
		izs	: procval.izs,			$
		nrgrp	: procval.nrgrp,		$
		cra	: procval.cra,			$
		nresoa	: procval.nresoa,		$
		er	: procval.er,			$
		wr	: procval.wr,			$
		ifout	: procval.ifout			$
              }
                ;**** force units to those of the ****
                ;**** archive if chosen ****

;      if lchoice eq 1 or lchoice eq 2 then begin
;          procval.ifout(0) = 3
;          procval.ifout(1) = 7
;          procval.ifout(2) = 10
;      endif

  endelse

  lpend = 0

		;******************************************
		;**** Now can define other array sizes ****
		;******************************************

  eng   = fltarr(nedim,3)
  rate = fltarr(nxdim,4)
  temp  = fltarr(ntdim,3)
  cia = fltarr(2)
  cra = fltarr(2)
  nshela = intarr(2)
  nresoa = intarr(2)
  ins = intarr(2,6)
  ils = intarr(2,6)
  izs = intarr(2,6)
  es = fltarr(2,6)
  er = fltarr(2,6)
  wr = fltarr(2,6)
  ifout  = intarr(3)

		;**********************************************
		;**** Read data from fortran if refreshing ****
		;**********************************************
  fdum = 0.0
  if lchoice eq 2 then begin
    if iwhc eq 1 then begin
	readf, pipe, fdum
	z = fdum
	readf, pipe, fdum
	z0 = fdum
	readf, pipe, fdum
	z1 = fdum
	readf, pipe, idum
	nigrp = idum
	readf, pipe, idum
	nrgrp = idum
	readf, pipe, idum
        maxe = idum
 	maxx = idum
	for i=0,maxx-1 do begin
	    readf, pipe, fdum
	    eng(i,0)=fdum
	    readf, pipe, fdum
	    rate(i,0)=fdum
	endfor
	readf, pipe, idum
        maxt = idum
	for i=0,maxt-1 do begin
	    readf, pipe, fdum
	    temp(i,0)=fdum
	endfor
        emin = 10.0d10
	for j=0,nigrp-1 do begin
	    readf, pipe, fdum
	    cia(j) = fdum
	    readf, pipe, idum
	    nshela(j) = idum
	    for i=0,nshela(j)-1 do begin
                readf,pipe,idum
		ins(j,i)=idum
                readf,pipe,idum
		ils(j,i)=idum
		readf, pipe, fdum
		es(j,i)=fdum
                emin = emin < fdum
		readf, pipe, idum
		izs(j,i)=idum
	    endfor
        endfor
        for j=0,nrgrp-1 do begin
	    readf, pipe, fdum
	    cra(j) = fdum
	    readf, pipe, idum
	    nresoa(j) = idum
	    for i=0,nresoa(j)-1 do begin
		readf, pipe, fdum
		er(j,i)=fdum
		readf, pipe, fdum
		wr(j,i)=fdum
	    endfor
	endfor
    endif else if iwhc eq 2 then begin
	;**** No Burgess option present ****
    endif

                ;*********************************************
                ;**** Convert standard forms to all units.****
                ;**** Inverse of the operation at the     ****
                ;**** beginning of spfman12.              ****
                ;*********************************************

    for ict = 0,maxe-1 do begin
      x = eng(ict,0)
      y = rate(ict,0)
      eng(ict,0) = x                          ; energy units
      eng(ict,1) = x/1.16054d4
      eng(ict,2) = x/(z1*z1)
                                ; rate units
      rate(ict,0) = y/(4.60488d7*sqrt(x/157890.0)*exp(157890.0*emin/x))
      rate(ict,1) = y
      rate(ict,2) = rate(ict,0)*(z1^3)
      rate(ict,3) = y*z1*z1
    endfor
    for itout = 0,maxt-1 do begin
      te = temp(itout,0)                           ; temp units
      temp(itout,1) = te/1.16054d4       
      temp(itout,2) = te/(z1*z1)
    endfor

		;*******************************************
		;****    Copy values to structure       ****
		;*******************************************
     procval = { 			      	$
                new    : procval.new,          	$
		maxe 	: maxe,			$
		maxx	: maxx,			$
		maxt	: maxt,			$
		tin	: procval.tin,		$
		ein 	: procval.ein,		$
		xin	: procval.xin,		$
		z	: z,			$
		z0	: z0,			$
		z1 	: z1,			$
		iop	: 1,			$
		nigrp	: nigrp,		$
		cia	: cia,			$
		nshela	: nshela,		$
		ins	: ins,			$
		ils	: ils,			$
		es 	: es,			$
		izs	: izs,			$
		nrgrp	: nrgrp,		$
		cra	: cra,			$
		nresoa	: nresoa,		$
		er	: er,			$
		wr	: wr,			$
		ifout	: procval.ifout		$
              }
  endif
 
		;****************************************
		;**** Pop-up processing input widget ****
		;****************************************

  adas106_proc, procval, dsfull, lchoice,				$
                nedim, nxdim, ntdim,					$ 
                eng, rate, temp, action, bitfile,			$
		FONT_LARGE=font_large, FONT_SMALL=font_small, 		$
		EDIT_FONTS=edit_fonts

		;********************************************
		;****  Act on the event from the widget  ****
		;**** There are three possible events    ****
		;**** 'Done', 'Cancel' and 'Menu'.       ****
		;********************************************
  gomenu = 0
  if action eq 'Done' then begin
    lpend = 0
  endif else if action eq 'Menu' then begin
    lpend = 0
    gomenu = 1
  endif else begin
    lpend = 1
  end
		;**************************************
		;**** Write edited data to fortran ****
		;**************************************

  printf, pipe, lpend
  printf, pipe, procval.z
  printf, pipe, procval.z0
  printf, pipe, procval.z1
  printf, pipe, procval.cia(0)
  printf, pipe, procval.cia(1)
  printf, pipe, procval.cra(0)
  printf, pipe, procval.cra(1)
  printf, pipe, procval.nigrp
  printf, pipe, procval.nrgrp
  for i=0,procval.nigrp-1 do begin
    printf, pipe, procval.nshela(i)
  endfor
  for i=0, procval.nrgrp-1 do begin
    printf, pipe, procval.nresoa(i)
  endfor
  for j=0,procval.nigrp-1 do begin
    for i=0,procval.nshela(j)-1 do begin
      printf, pipe, procval.ins(j,i)
      printf, pipe, procval.ils(j,i)
      printf, pipe, procval.es(j,i)
      printf, pipe, procval.izs(j,i)
    endfor
  endfor
  for j=0,procval.nrgrp-1 do begin
    for i=0,procval.nresoa(j)-1 do begin
      printf, pipe, procval.er(j,i)
      printf, pipe, procval.wr(j,i)
    endfor
  endfor
  printf, pipe, procval.ifout(0)
  printf, pipe, procval.ifout(1)-3
  printf, pipe, procval.ifout(2)-7
  printf, pipe, procval.maxe
  printf, pipe, procval.maxt
  for i=0,procval.maxe-1 do begin
    printf, pipe, procval.ein(i)
    printf, pipe, procval.xin(i)
  endfor
  for i=0,procval.maxt-1 do begin
    printf, pipe, procval.tin(i)
  endfor
  printf, pipe, procval.iop

END
