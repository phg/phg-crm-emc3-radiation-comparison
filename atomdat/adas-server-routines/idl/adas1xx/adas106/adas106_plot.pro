; Copyright (c) 1996 Strathclyde University .
; SCCS info: Module @(#)$Header: /home/adascvs/idl/adas1xx/adas106/adas106_plot.pro,v 1.1 2004/07/06 10:09:58 whitefor Exp $ Date $Date: 2004/07/06 10:09:58 $
;+
;+
; PROJECT:
;       ADAS IBM MVS to DEC UNIX conversion & development
;
; NAME:
;	ADAS106_PLOT
;
; PURPOSE:
;       To plot the ratio graph for the ADAS option & to allow
;       printing of it.
;
; EXPLANATION:
;       creates a window for the display of the graph and displays
;       output as the user directs through interaction with the
;       widget.
;
; USE:  ADAS106 Specific
;
; INPUTS:
;       DGEX  - Array; The x-coordinates for the ratio graph.
;
;       DGEY  - Array; The y-coordinates for the ratio graph.
;
;       X     - Array; the x-coords. for the ionisation coeff. graph.
;
;       Y     - Array; the y-coords. for the ionisation coeff. graph.
;
;       LPENDC- Integer; Determines whether 'cancel' or 'done' 
;                        selected.
;
;       ARCHC - Integer; Determines whether archiving is requested.
;
;       LCHOICEC-Integer; Holds archiving option
;
;	STITLE - Information string for display
;
;       DEVICE - String; The current graphics device.
;
;       DATE   - String; The date
;
;       HEADER - String; The Printout header.
;
;       TITLX  - String; Part of the graph title (The dataset name)
;
;       BITFILE - String; the path to the directory containing bitmaps
;                 for the 'escape to series menu' button.
;
; OPTIONAL INPUTS:
;
; OUTPUTS:
;       VAL   - Structure; Contains the values of the output widget
;                          selected by the user, see a1spf1.pro.
;
;       ACT   - String; The event action; 'Cancel' or 'Done'
;
;       GOMENU  - Int; flag - set to 1 if user has selected 'escape direct
;                 to series menu , otherwise 0
;
; OPTIONAL OUTPUTS:
;
; KEYWORD PARAMETERS:
;       WINTITLE - A title for the window
;       FONT 
;       NSP - The number of requested spline points
;
; CALLS:
;       ADAS106_PLOT_EV
;       XMANAGER
;
; SIDE EFFECTS:
;       None
;
; CATEGORY: ADAS System
;
;       William Osborn, Tessella Support Services plc., 07-10-96
;
; MODIFIED: 
;	1.1	William Osborn
;           	Altered adas105_plot.pro to the needs of ADAS106
;
; VERSION:
;	1.1	07-10-96
;
;-
;-----------------------------------------------------------------------------


PRO adas106_plot_ev, event

  COMMON plot106_blk, action, value, lpend, arch, lchoice, data, plotdev, $
                   plotfile, fileopen, win, gomenu, plotch
  newplot = 0
  print = 0
  done = 0

		;**** Find the event type and copy to common ****
    action = event.action

    CASE action OF
            
                ;**** 'Print' button ****
        'print' : begin
               newplot = 1
               print = 1
           end
                ;**** 'Archive' button ****

        'archive' : begin
                arch = 1
                if lchoice eq 0 then begin
                  buttons = ['Continue']
                  title = 'ADAS106:DEFAULT ACTION CONTINUING'
                  message = $
"Archive selection clashes with 'no archive' flag arch106/archive.dat created"
                  dec = popup(message=message,title=title,$
                              buttons=buttons,font=font)
                end
           end

                ;**** 'Plot1' button ****
         'plot1' : begin
             plotch = 0
         end

                ;**** 'Plot2' button ****
         'plot2' : begin
             plotch = 1
         end
		;**** 'Done' button ****
	'done'  : begin

                if fileopen eq 1 then begin
                   set_plot,plotdev
                   device,/close_file
                end
                set_plot,'X'
			;**** Get the output widget value ****
;		widget_control,event.id,get_value=value 
		widget_control,event.top,/destroy
                done = 1
                lpend = 0

	   end


		;**** 'Cancel' button ****
	'cancel': begin
                  widget_control,event.top,/destroy
                  done = 1
                  lpend = 1
                  set_plot,'X'
           end

		;**** 'Menu' button ****

	'menu': begin
                if fileopen eq 1 then begin
                   set_plot,plotdev
                   device,/close_file
                end
		widget_control,event.top,/destroy
		done = 1
		lpend = 1
		gomenu = 1
		set_plot,'X'
	   end
    END

  if done eq 0 then begin
                ;**** Set graphics device ****
     if print eq 1 and value.hrdout eq 1 then begin
        set_plot,plotdev
        if fileopen eq 0 then begin
           fileopen = 1
           device,filename = plotfile
           device, /landscape
        end
        message = 'Plot written to print file.'
	grval = {WIN:0, MESSAGE:message}
	widget_control,event.id,set_value=grval
     end else begin
        set_plot,'X'
        wset,win
     end
                ;**** Draw Graphics ****
     if plotch eq 0 then begin
         plot106a, data.dgex, data.dgey, data.title1, data.xtitle1, $
	      data.ytitle1, 	$
              data.xmin1, data.xmax1, data.ymin1, data.ymax1, data.nsp, $
              data.ldef1, data.res
     endif else begin
         plot106b, data.x, data.y, data.yp, data.title2, data.stitle,$
           data.xtitle2, data.ytitle2, 	$
           data.xmin2, data.xmax2, data.ymin2, data.ymax2, data.ldef2
     endelse
  end

END

;-----------------------------------------------------------------------------


PRO adas106_plot, dgex, dgey, x, y, yp, lpendc, archc, lchoicec, stitle1, $
                  stitle2,$
                  val, act, device, date, header, titlx, bitfile, gomenu, $
                  WINTITLE = wintitle, NSP = nsp, FONT = font, RES = res

  COMMON plot106_blk,action,value, lpend, arch, lchoice, data, plotdev, $
                   plotfile, fileopen, win, gomenucom, plotch

		;**** Copy values to common ****
  value = val
  lpend = lpendc
  arch = archc
  lchoice = lchoicec
  plotdev = device
  plotfile = value.hardname
  fileopen = 0
  gomenucom = gomenu
  plotch=0

		;**** Set defaults for keywords ****
  IF NOT (KEYWORD_SET(wintitle)) THEN wintitle = ''
  IF NOT (KEYWORD_SET(nsp)) THEN NSP = -1
  IF NOT (KEYWORD_SET(font)) THEN font = 'courier-bold14'
  IF NOT (KEYWORD_SET(res)) THEN begin
	xarr = fltarr(12)
	res = {res106, nithr:0, nrthr:0, xv:xarr}
  endif

		;***************************************
		;**** Pop-up a new widget if needed ****
		;***************************************

  plotval = {WIN:0}

                ;**** create base widget ****
  inid = widget_base(TITLE=wintitle,XOFFSET=200,YOFFSET=0)

		;**** Declare output options widget ****
  cwid = cw_a6plot(inid, bitfile, plotval = plotval, FONT=font)

		;**** Realize the new widget ****
  dynlabel, inid
  widget_control,inid,/realize
                ;**** get the id of the graphics area ****
  widget_control,cwid,get_value = window
  win = window.win
  wset,win
                ;**** Set up titles ****
  title1 = strarr(5)
  title2 = title1
  title1(0) = "COMPARITIVE RATIO PLOT "
    if (strtrim(strcompress(value.gtit1),2) ne ' ' ) then begin
      title1(0) = title1(0)+': '+strupcase(strtrim(value.gtit1,2))
    end
  title1(1) = 'ADAS    :'+strupcase(header)
  title1(2) = 'FILE     : '+strcompress(titlx)
  title1(3) = 'KEY     : (DIAMONDS - INPUT DATA) '+$
               '(FULL LINE - IDL SPLINE FIT)'
  title1(4) = stitle1
  xtitle1 = ' 1/(1+A*EMIN/KTE) '
  ytitle1 = ' Gamma/Approx.Gamma '
  title2(0) = "IONISATION COEFFICIENT PLOT"
    if (strtrim(strcompress(value.gtit1),2) ne ' ' ) then begin
      title2(0) = title2(0)+': '+strupcase(strtrim(value.gtit1,2))
    end
  title2(1) = 'ADAS    :'+strupcase(header)
  title2(2) = 'FILE     : '+strcompress(titlx)
  title2(3) = 'KEY     : (FULL LINE - INPUT DATA) '+$
               '(DASHED LINE - APPROXIMATE FORM)'
  title2(4) = ''
  xtitle2 = ' TE(K)/Z1^2 '
  ytitle2 = ' Z1^3*S (CM^3 SEC^-1) '

                ;**** Put the graphing data into common ****
  data = { dgex:dgex, dgey:dgey, x:x, y:y, yp:yp, stitle:stitle2, $
           title1:title1, xtitle1:xtitle1, ytitle1:ytitle1,$
           xmin1:value.xmin1, xmax1:value.xmax1, ymin1:value.ymin1, $
           ymax1:value.ymax1, ldef1:value.grpscal1, NSP:nsp, RES:res, $
           title2:title2, xtitle2:xtitle2, ytitle2:ytitle2,$
           xmin2:value.xmin2, xmax2:value.xmax2, ymin2:value.ymin2, $
           ymax2:value.ymax2, ldef2:value.grpscal2 $
         }
  
  plot106a, dgex, dgey, title1, xtitle1, ytitle1, value.xmin1,	$
	   value.xmax1, value.ymin1, value.ymax1,  nsp, value.grpscal1, res

		;**** make widget modal ****
  xmanager,'adas106_plot',inid,event_handler='adas106_plot_ev',/modal,/just_reg
 
		;**** Return the output value from common ****
  act = action
  val = value
  lpendc = lpend
  archc = arch
  gomenu = gomenucom

END

