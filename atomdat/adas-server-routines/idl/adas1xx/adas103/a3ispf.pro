; Copyright (c) 1996 Strathclyde University .
; SCCS info: Module @(#)$Header: /home/adascvs/idl/adas1xx/adas103/a3ispf.pro,v 1.1 2004/07/06 10:01:31 whitefor Exp $ Date $Date: 2004/07/06 10:01:31 $
;+
; PROJECT:
;       ADAS IBM MVS to   UNIX conversion
;
; NAME:
;	A3ISPF
;
; PURPOSE:
;	IDL user interface and communications with ADAS103 FORTRAN
;	process via pipe.
;
; EXPLANATION:
;	The routine begins by reading information from the ADAS103
;	FORTRAN process via a UNIX pipe.  Then part of the ADAS103
;	IDL user interface is invoked to determine how the user
;	wishes to process the input dataset.  When the user's
;	interactions are complete the information gathered with
;	the user interface is written back to the FORTRAN process
;	via the pipe.  Communications are to the FORTRAN subroutine
;	A3ISPF.
;
; USE:
;	The use of this routine is specific to ADAS103, see adas103.pro.
;
; INPUTS:
;	PIPE	- The IDL unit number of the bi-directional pipe to the
;		  ADAS103 FORTRAN process.
;
;	LPEND	- Integer, 0 if the user pressed the 'Done' button
;		  or 1 if the user pressed 'Cancel'.  Maps directly onto
;		  the logical variable LPEND in ADAS103 FORTRAN.
;
;	PROCVAL	- A structure which determines the initial settings of
;		  the processing options widget.  The initial value is
;		  set in adas103.pro.  If adas103.pro passes a blank 
;		  dummy structure of the form {NEW:-1} into PROCVAL then
;		  PROCVAL is reset to a default structure.
;
;		  The PROCVAL structure is defined at the start of
;		  this routine.
;
;		  See cw_adas103_proc.pro for a full description of this
;		  structure.
;	
;
;	DSFULL	- String; The full system file name of the input 
;		  dataset selected by the user for processing.
;
;	LCHOICE	- Integer; Variable to flag user choice of archive
;                 option. 
;                 0 = No Archive
;                 1 = Old Archive
;                 2 = Refresh form Old Archive
;                 3 = New Archive
;
;       BITFILE - String; the path to the directory containing bitmaps
;                 for the 'escape to series menu' button.
;
; OPTIONAL INPUTS:
;	None
;
; INPUT/OUTPUT: 
;
;
; OUTPUTS:
;	PROCVAL	- On output the structure records the final settings of
;		  the processing selections widget if the user pressed the
;		  'Done' button, otherwise it is not changed from input.
;
;       GOMENU  - Int; flag - set to 1 if user has selected 'escape direct
;                 to series menu , otherwise 0
;
; OPTIONAL OUTPUTS:
;	None
;
; KEYWORD PARAMETERS:
;	FONT_LARGE - The name of a larger font.
;
;	FONT_SMALL - The name of a smaller font.
;
;	EDIT_FONTS - A structure of two fonts used in the table editor
;		     adas_edtab.pro, {font_norm:'',font_input:''}.
;
; CALLS:
;	ADAS103_PROC	Invoke the IDL interface for ADAS103 data
;		 	processing options/input.
;
; SIDE EFFECTS:
;	Two way communications with ADAS103 FORTRAN via the
;	bi-directional UNIX pipe.
;
; CATEGORY:
;	Adas system.
;
; WRITTEN:
;       William Osborn, Tessella Support Services plc., 04-11-96
;       
; VERSION:
;	1.1	William Osborn
;		First written using a5ispf.pro as a template
;
; MODIFIED:
;	1.1	04-11-96
;
;-----------------------------------------------------------------------------

PRO a3ispf, pipe, lpend, procval, dsfull, lchoice, 			$
            bitfile, gomenu,	$
	    FONT_LARGE=font_large, FONT_SMALL=font_small, 		$
	    EDIT_FONTS=edit_fonts


                ;**** Set defaults for keywords ****

  IF NOT (KEYWORD_SET(font_large)) THEN font_large = ''
  IF NOT (KEYWORD_SET(font_small)) THEN font_small = ''
  IF NOT (KEYWORD_SET(edit_fonts)) THEN 				$
                                edit_fonts = {font_norm:'',font_input:''}

		;*******************************************
		;**** Set default value if non provided ****
                ;****  Set energy units off always to   ****
                ;****  force a selection                ****
		;*******************************************

  idum = 0
  readf, pipe, idum  
  nedim = idum
  readf, pipe, idum                               
  iwhc = idum
  nxdim = nedim & ntdim = nedim 
  if (procval.new lt 0) then begin
    temp_arr = fltarr(ntdim)
    rate_arr = fltarr(nxdim)
    tempi_arr = fltarr(nedim)
    edarr  = fltarr(2)
    igarr  = intarr(2)
    niarr  = intarr(2,6)
    wiarr  = fltarr(2,6)
    ifarr = intarr(3)

    procval = { 			 	$
                new    	: 0,                    $
		maxe 	: nedim,		$
		maxx	: nxdim,		$
		maxt	: ntdim,		$
		tin	: temp_arr,		$
		ein 	: tempi_arr,		$
		xin	: rate_arr,		$
		z	: 0,			$
		z0	: 0,			$
		z1 	: 0,			$
                n0      : 0,                    $
                v0      : 0.0,                  $
                phfrac  : 0.0,                  $
                ingrup  : 1,                    $
                edgrp   : edarr,                $
                scgrp   : edarr,                $
                igrp    : igarr,                $
                ni      : niarr,                $
                li      : niarr,                $
                wi      : wiarr,                $
                nj      : niarr,                $
                lj      : niarr,                $
                wj      : wiarr,                $
                eij     : wiarr,                $
                aprob   : wiarr,                $
                corfac  : wiarr,                $
                ncut    : niarr,                $
		ifout	: ifarr			$
              }

                ;**** force units to those of the ****
                ;**** archive if chosen ****

;      if lchoice eq 1 or lchoice eq 2 then begin
;          procval.ifout(0) = 3
;          procval.ifout(1) = 7
;          procval.ifout(2) = 10
;      endif else begin
	   procval.ifout(0) = 0
	   procval.ifout(1) = 3
	   procval.ifout(2) = 5
;      endelse

  endif else begin

    procval = { 			 		$
                new    	: procval.new,			$
		maxe 	: procval.maxe,			$
		maxx	: procval.maxx,			$
		maxt	: procval.maxt,			$
		tin	: procval.tin,			$
		ein 	: procval.ein,			$
		xin	: procval.xin,			$
		z	: procval.z,			$
		z0	: procval.z0,			$
		z1 	: procval.z1,			$
                n0      : procval.n0,                   $
                v0      : procval.v0,                   $
                phfrac  : procval.phfrac,               $
                ingrup  : procval.ingrup,               $
                edgrp   : procval.edgrp,                $
                scgrp   : procval.scgrp,                $
                igrp    : procval.igrp,                 $
                ni      : procval.ni,                   $
                li      : procval.li,                   $
                wi      : procval.wi,                   $
                nj      : procval.nj,                   $
                lj      : procval.lj,                   $
                wj      : procval.wj,                   $
                eij     : procval.eij,                  $
                aprob   : procval.aprob,                $
                corfac  : procval.corfac,               $
                ncut    : procval.ncut,                 $
		ifout	: procval.ifout			$
              }
                ;**** force units to those of the ****
                ;**** archive if chosen ****

;      if lchoice eq 1 or lchoice eq 2 then begin
;          procval.ifout(0) = 3
;          procval.ifout(1) = 7
;          procval.ifout(2) = 10
;      endif

  endelse

  lpend = 0

		;******************************************
		;**** Now can define other array sizes ****
		;******************************************

  tempi   = fltarr(nedim,3)
  rate = fltarr(nxdim,2)
  temp  = fltarr(ntdim,3)
  edgrp = fltarr(2)
  scgrp = fltarr(2)
  ni = intarr(2,6)
  li = intarr(2,6)
  wi = fltarr(2,6)
  nj = intarr(2,6)
  lj = intarr(2,6)
  wj = fltarr(2,6)
  ni = fltarr(2,6)
  ni = fltarr(2,6)
  eij = fltarr(2,6)
  aprob = fltarr(2,6)
  corfac = fltarr(2,6)
  ncut = intarr(2,6)
  igrp = intarr(2)
  ifout  = intarr(3)

		;**********************************************
		;**** Read data from fortran if refreshing ****
		;**********************************************
  fdum = 0.0
  if lchoice eq 2 then begin
    if iwhc eq 2 then begin
	readf, pipe, fdum
	z = fdum
	readf, pipe, fdum
	z0 = fdum
	readf, pipe, fdum
	z1 = fdum
	readf, pipe, fdum
	n0 = fdum
	readf, pipe, fdum
	v0 = fdum
	readf, pipe, fdum
	phfrac = fdum
	readf, pipe, idum
	ingrup = idum
	readf, pipe, idum
        maxe = idum
 	maxx = idum
	for i=0,maxx-1 do begin
	    readf, pipe, fdum
	    tempi(i,0)=fdum
	    readf, pipe, fdum
	    rate(i,0)=fdum
	endfor
	readf, pipe, idum
        maxt = idum
	for i=0,maxt-1 do begin
	    readf, pipe, fdum
	    temp(i,0)=fdum
	endfor
	for j=0,ingrup-1 do begin
	    readf, pipe, fdum
	    edgrp(j) = fdum
	    readf, pipe, fdum
	    scgrp(j) = fdum
	    readf, pipe, idum
	    igrp(j) = idum
	    for i=0,igrp(j)-1 do begin
                readf, pipe, idum
		ni(j,i)=idum
                readf, pipe, idum
		li(j,i)=idum
		readf, pipe, fdum
		wi(j,i)=fdum
		readf, pipe, idum
		nj(j,i)=idum
		readf, pipe, idum
		lj(j,i)=idum
		readf, pipe, fdum
		wj(j,i)=fdum
		readf, pipe, fdum
		eij(j,i)=fdum
		readf, pipe, fdum
		aprob(j,i)=fdum
		readf, pipe, fdum
		corfac(j,i)=fdum
		readf, pipe, idum
		ncut(j,i)=idum
	    endfor
        endfor
    endif else if iwhc eq 2 then begin
	;**** No ADAS option present ****
    endif

                ;*********************************************
                ;**** Convert standard forms to all units.****
                ;**** Inverse of the operation at the     ****
                ;**** beginning of spfman8h.              ****
                ;*********************************************

    for ict = 0,maxe-1 do begin
      x = tempi(ict,0)
      y = rate(ict,0)
      tempi(ict,0) = x                     ; energy units
      tempi(ict,1) = x/1.16054d4
      tempi(ict,2) = x/(z1*z1)
      rate(ict,0) = y        ; rate form
      rate(ict,1) = y
    endfor
    for itout = 0,maxt-1 do begin
      te = temp(itout,0)                           ; temp units
      temp(itout,0) = te
      temp(itout,1) = te/1.16054d4
      temp(itout,2) = te/(z1*z1)
    endfor

		;*******************************************
		;****    Copy values to structure       ****
		;*******************************************
     procval = { 			      	$
                new    : procval.new,          	$
		maxe 	: maxe,			$
		maxx	: maxx,			$
		maxt	: maxt,			$
		tin	: procval.tin,		$
		ein 	: procval.ein,		$
		xin	: procval.xin,		$
		z	: z,			$
		z0	: z0,			$
		z1 	: z1,			$
                n0      : n0,                   $
                v0      : v0,                   $
                phfrac  : phfrac,               $
                ingrup  : ingrup,               $
                edgrp   : edgrp,                $
                scgrp   : scgrp,                $
                igrp    : igrp,                 $
                ni      : ni,                   $
                li      : li,                   $
                wi      : wi,                   $
                nj      : nj,                   $
                lj      : lj,                   $
                wj      : wj,                   $
                eij     : eij,                  $
                aprob   : aprob,                $
                corfac  : corfac,               $
                ncut    : ncut,                 $
		ifout	: procval.ifout		$
              }
  endif
 
		;****************************************
		;**** Pop-up processing input widget ****
		;****************************************

  adas103_proc, procval, dsfull, lchoice,				$
                nedim, nxdim, ntdim,					$ 
                tempi, rate, temp, action, bitfile,			$
		FONT_LARGE=font_large, FONT_SMALL=font_small, 		$
		EDIT_FONTS=edit_fonts

		;********************************************
		;****  Act on the event from the widget  ****
		;**** There are three possible events    ****
		;**** 'Done', 'Cancel' and 'Menu'.       ****
		;********************************************
  gomenu = 0
  if action eq 'Done' then begin
    lpend = 0
  endif else if action eq 'Menu' then begin
    lpend = 0
    gomenu = 1
  endif else begin
    lpend = 1
  end
		;**************************************
		;**** Write edited data to fortran ****
		;**************************************

  printf, pipe, lpend
  printf, pipe, procval.z
  printf, pipe, procval.z0
  printf, pipe, procval.z1
  printf, pipe, procval.n0
  printf, pipe, procval.v0
  printf, pipe, procval.phfrac
  printf, pipe, procval.ingrup
  for i=0,procval.ingrup-1 do begin
    printf, pipe, procval.edgrp(i)
    printf, pipe, procval.scgrp(i)
    printf, pipe, procval.igrp(i)
  endfor
  for j=0,procval.ingrup-1 do begin
    for i=0,procval.igrp(j)-1 do begin
      printf, pipe, procval.ni(j,i)
      printf, pipe, procval.li(j,i)
      printf, pipe, procval.wi(j,i)
      printf, pipe, procval.nj(j,i)
      printf, pipe, procval.lj(j,i)
      printf, pipe, procval.wj(j,i)
      printf, pipe, procval.eij(j,i)
      printf, pipe, procval.aprob(j,i)
      printf, pipe, procval.corfac(j,i)
      printf, pipe, procval.ncut(j,i)
    endfor
  endfor
  ;**** Add one to convert indices to FORTRAN ****
  printf, pipe, procval.ifout(0)+1
  printf, pipe, procval.ifout(1)-2
  printf, pipe, procval.ifout(2)-4
  printf, pipe, procval.maxe
  printf, pipe, procval.maxt
  for i=0,procval.maxe-1 do begin
    printf, pipe, procval.ein(i)
    printf, pipe, procval.xin(i)
  endfor
  for i=0,procval.maxt-1 do begin
    printf, pipe, procval.tin(i)
  endfor

END
