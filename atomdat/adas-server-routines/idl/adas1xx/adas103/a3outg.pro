; Copyright (c) 1996 Strathclyde University .
; SCCS info: Module @(#)$Header: /home/adascvs/idl/adas1xx/adas103/a3outg.pro,v 1.1 2004/07/06 10:01:41 whitefor Exp $ Date $Date: 2004/07/06 10:01:41 $
;+
; PROJECT:
;       ADAS IBM MVS to DEC UNIX conversion & development
;
; NAME:
;       A3OUTGA
;
; PURPOSE:
;       To read omegas and rate coeffs. after fortran processing, popup the 
;       ADAS_GRAPH_EDITOR write any alterations to the fortran and plot the
;       final rate coeffs.
;
; USE:
;       ADAS103 Specific
;
; INPUTS:
;       PIPE    - The IDL unit number of the bi-directional pipe to the
;                 ADAS103 FORTRAN process.
;
;       LPEND   - Integer, 0 if the user pressed the 'Done' button
;                 or 1 if the user pressed 'Cancel'.  Maps directly onto
;                 the logical variable LPEND in ADAS103 FORTRAN.
;
;       LCHOICE - Integer; Variable to flag user choice of archive
;                 option. 
;                 0 = No Archive
;                 1 = Old Archive
;                 2 = Refresh form Old Archive
;                 3 = New Archive
;
;       OUTVAL -  A Structure which holds the output widget settings see 
;                 a3spf1.pro
;
;       PROCVAL - A Structure which holds the processing widget settings
;                 see a3ispf.pro.
;
;       ROOT -    The path to the user's default archive directory, 
;                 '/home/user/adas/arch103'
;
;       DEVICE -  The name of the current output device.
;
;       DATE -    The date
;
;       HEADER -  The ADAS release header for the final plot.  
;
;       TITLX  -  The datset name, for display on the plot.
;
;       BITFILE - String; the path to the directory containing bitmaps
;                 for the 'escape to series menu' button.
;
; OPTIONAL INPUTS:
;                 None
;
; OUTPUTS:   
;       GOMENU  - Int; flag - set to 1 if user has selected 'escape direct
;                 to series menu , otherwise 0
;
; OPTIONAL OUTPUTS:
;                 None
;
; KEYWORD PARAMETERS:
;       FONT - The name of a font.         
;
; CALLS:
;       ADAS_EDIT_GRAPH - The ADAS version of the interactive Graph Editor
;       ADAS103_PLOT - The plotting procedure for the Upsilon graph
;
; SIDE EFFECTS:
;       Two way communications with the Fortran process via the Unix pipe.
;
; CATEGORY:
;       ADAS System
;
; WRITTEN: 
;       William Osborn, Tessella Support Services plc, 04-11-96
;
; MODIFIED:
;	1.1	William Osborn
;		First written using a5outg.pro
;
; VERSION:
;	1.1	04-11-96
;
;-
;-----------------------------------------------------------------------------
pro a3outg, pipe, lpend, lchoice, root, outval, procval, device, date, $
             header , titlx, bitfile, gomenu, FONT = font

                   ;**** re-set lpend ****
  lpend = 0
                   ;********************************
                   ;**** read data from fortran ****                   
                   ;********************************

  fdum=0.0
  idum=0
  sdum = ''
  readf, pipe, fdum
  ascl = fdum
  readf, pipe, idum
  ict=idum
  readf, pipe, idum
  icout=idum
  indim1 = ict > icout
  xpa = fltarr(5,indim1)
  ypa = xpa
  tea4 = fltarr(icout)
  excra = fltarr(3,icout)
  for i = 0,ict-1 do begin
      for j=0,3 do begin
          readf, pipe, fdum
          xpa(j,i) = fdum
          readf, pipe, fdum
          ypa(j,i) = fdum
      endfor
  endfor
  for i = 0,icout-1 do begin
    readf, pipe, fdum
    xpa(4,i) = fdum
    readf, pipe, fdum
    ypa(4,i) = fdum
  endfor
  for i = 0,icout-1 do begin
    readf, pipe, fdum
    tea4(i) = fdum
    for j=0,2 do begin
        readf, pipe, fdum
        excra(j,i) = fdum
    endfor
  endfor

            ;********************************
            ;**** Read in the annotation ****
            ;********************************
  readf,pipe, fdum
  emean=fdum
  readf,pipe, fdum
  scexp=fdum

  stitle = 'EMEAN='+string(emean,format='(F12.5)')+'  SCEXP='+$
    string(scexp,format='(F6.2)')+'  A='+string(ascl,format='(F6.2)')

  orb=['s','p','d','f','#','#','#','#','#','#','#','#','#','#','#']
  stitle2='Core Transition Data!C!C!C'
  readf,pipe,idum
  ngroup=idum
  for i=0,ngroup-1 do begin
      readf,pipe,idum
      igroup=idum
      readf,pipe,fdum
      edisp=fdum
      readf,pipe,fdum
      scale=fdum
      stitle2=stitle2+'GROUP='+string(igroup,format='(I2)')+' DISP.='+$
        string(edisp,format='(f10.5)')+' SCALE='+$
        string(scale,format='(f10.5)')+$
        '!C!C  I  TRANS      EIJ            FIJ          CORFAC!C'
      for i=0,igroup-1 do begin
          readf,pipe,idum
          ni=idum
          readf,pipe,idum
          li=idum
          readf,pipe,idum
          nj=idum
          readf,pipe,idum
          lj=idum
          readf,pipe,fdum
          eij=fdum
          readf,pipe,fdum
          fij=fdum
          readf,pipe,fdum
          corfac=fdum
          stitle2=stitle2+string(i,format='(I2)')+'  '+$
            string(ni,format='(I1)')+$
            orb(li)+'- '+string(nj,format='(I1)')+orb(lj)+'  '+$
            string(eij,format='(F10.5)')+'  '+string(fij,format='(F10.5)')+$
            '  '+string(corfac,format='(F10.5)')
      endfor
  endfor
                   ;********************************
                   ;****    Alter to AGE        **** 
                   ;****    required forms  &   ****
                   ;**** set up arrays for AGE  ****  
                   ;********************************

  z1 = float(procval.z1)
  ;**** First graph ****
  dgey = ypa
  dgex = 1.0d0/(1.0d0+ascl*xpa)
  ;**** Second graph ****
  x = tea4/(z1*z1)
  y = excra/z1

                   ;********************************
                   ;**** Popup diagnostic graph ****
                   ;****         editor         ****                   
                   ;********************************
  print1 = 0
  xtit = '1/(1+A*SCEXP*EMEAN/KTE)'
  ytit = 'LOG10(RATIO)'
  sel  = outval.gredsel
  if sel eq 2 then tit = 'FINAL RATIO PLOT USING BURGESS FORMULA' $
  else tit = 'FINAL RATIO PLOT USING BURGESS PROGRAM'
  x1=dgex(sel,*)
  y1=dgey(sel,*)
  if outval.grpscal1 eq 1 then begin
    adas_edit_graph, x1, y1,  lpend,   print1,  nsp = 70, $
      xmax = float(outval.xmax1), xmin = float(outval.xmin1), $
      ymax = float(outval.ymax1), ymin = float(outval.ymin1), $
      font = font , xtitle=xtit, ytitle = ytit, $
      no_print = 1, title=tit
  end else begin
    adas_edit_graph, x1, y1,  lpend,   print1,  nsp = 70, $
      font = font, resonances = res, xtitle=xtit, ytitle = ytit, $
      no_print = 1, title=tit, xmin=0.0d0, xmax=1.0d0,ymin=-1.0,ymax=1.0
  end

	;*******************************************************************
	;**** Unlike in ADAS101, the calculation of the approximate 	****
	;**** values, apga, is not simple and so dgey is passed back to ****
	;**** the fortran for reanalysing			    	****
	;*******************************************************************


                   ;****print new values to pipe****
                   ;****   for re-analysing     ****    

  ictn = n_elements(x1)
  if ictn ne ict then begin
      a=popup(message=['Warning: changing the number of'+$
       ' points can have','unpredictable effects on this routine, including',+$
       'crashing it!'],font = font,buttons=['Cancel','Continue'])
      if a eq 'Cancel' then lpend = 1
  endif
  printf, pipe, lpend
  if lpend eq 2 then begin
      if ictn gt 10 then x1=x1(0:9)
      ;recalculate xpa
      x1 = (1.0/x1-1.0)/ascl
      printf, pipe, sel
      printf, pipe, ictn
      for i = 0,ictn-1 do begin
          printf, pipe, x1(i)
          printf, pipe, y1(i)
      endfor
  endif

    if lpend eq 1 or lpend eq 2 then begin
        outval.texmes = ''
        goto, LABELEND
    endif
                   ;**** re-set lpend ****
    lpend = 0
    arch = 0       ;**** default archive option is off
    b = intarr(5)
    for i = 0,4 do b(i) = -1

    adas103_plot, dgex, dgey, x, y, lpend, arch, lchoice, stitle, $
      stitle2, outval, action, device, date, header, $
      titlx, bitfile, gomenu, wintitle = 'ADAS103 GRAPHICAL OUTPUT', nsp = 70,$
      font = font
    printf, pipe, lpend

                   ;**** Print archiving decision if continuing ****

    if lpend eq 0 then printf, pipe, arch
    
    if lpend eq 1 then goto, LABELEND


                   ;**** special case if archiving selected after ****
                   ;**** picking no archiving initially ****

    if arch eq 1 and lchoice eq 0 then begin
      dsarch = root+'archive.dat'
      printf, pipe, dsarch
    endif

LABELEND:

end


 
