; Copyright (c) 1996 Strathclyde University .
; SCCS info: Module @(#)$Header: /home/adascvs/idl/adas1xx/adas103/adas103_proc.pro,v 1.1 2004/07/06 10:09:20 whitefor Exp $ Date $Date: 2004/07/06 10:09:20 $
;+
; PROJECT:
;       ADAS IBM MVS to   UNIX conversion
;
; NAME:
;	ADAS103_PROC
;
; PURPOSE:
;	IDL ADAS user interface, processing options/input.
;
; EXPLANATION:
;	This routine creates and manages a pop-up window which allows
;	the user to select options and input data to control ADAS103
;	processing.
;
; USE:
;	This routine is ADAS103 specific, see a3ispf.pro for how it
;	is used.
;
; INPUTS:
;	PROCVAL	- A structure which determines the initial settings of
;		  the processing options widget.  The value is passed
;		  unmodified into cw_adas103_proc.pro.
;
;		  See cw_adas103_proc.pro for a full description of this
;		  structure.
;
;	DSFULL	- String; The full system file name of the input 
;		          dataset selected by the user for processing.
;
;	LCHOICE - Integer: The archiving option, see a3spf0.pro 
;
;	NTDIM	- Integer : Maximum number of output temperatures allowed.
;
;	NEDIM	- Integer : Maximum number of input allowed.
;
;	NXDIM	- Integer : Maximum number of rates allowed.
;
;       TEMPO   - Float Array; The output temperature set.
;
;       TEMPI   - Float Array;  The input temperature set.
;
;       RATE    - Float Array;  The input rate set.
;
;       BITFILE - String; the path to the directory containing bitmaps
;                 for the 'escape to series menu' button.
;
; OPTIONAL INPUTS:
;	None
;
; OUTPUTS:
;	PROCVAL	- On output the structure records the final settings of
;		  the processing selections widget if the user pressed the
;		  'Done' button, otherwise it is not changed from input.
;
;	ACT	- String; Either 'Done', 'Cancel' or 'Menu' for the button the
;		  user pressed to terminate the processing options
;		  window.
;
; OPTIONAL OUTPUTS:
;	None
;
; KEYWORD PARAMETERS:
;	FONT_LARGE - The name of a larger font.
;
;	FONT_SMALL - The name of a smaller font.
;
;	EDIT_FONTS - A structure of two fonts used in the table editor
;		     adas_edtab.pro, {font_norm:'',font_input:''}.
;
; CALLS:
;	CW_ADAS103_PROC	Declares the processing options widget.
;	ADAS103_PROC_EV Called indirectly during widget management,
;			routine included in this file.
;	XMANAGER
;
; SIDE EFFECTS:
;       This widget uses a common block PROC103_BLK, to pass the
;       variables VALUE and ACTION between the two routines. 
;
; CATEGORY:
;	Widgets
;
; WRITTEN:
;       William Osborn, Tessella Support Services plc, 4-Nov-1996
;
; MODIFIED:
;       1.1	William Osborn
;		First Release. Written using adas105_proc.pro v1.1
; VERSION:
;	1.1	04-11-96
;-
;-----------------------------------------------------------------------------


PRO adas103_proc_ev, event


  COMMON proc103_blk,action,value

    action = event.action
    CASE event.action OF

		;**** 'Done' button ****

	'Done'  : begin

		;**** Get the output widget value ****

		      widget_control,event.id,get_value=value

		      widget_control,event.top,/destroy

	           end


		;**** 'Cancel' button ****

	'Cancel': widget_control,event.top,/destroy

		;**** 'Menu' button ****

	'Menu': widget_control,event.top,/destroy

    END

END

;-----------------------------------------------------------------------------


PRO adas103_proc, procval, dsfull, lchoice,				$
                  nedim, nxdim, ntdim,					$
                  tempi, rate, tempo, act, bitfile,			$
		  FONT_LARGE=font_large, FONT_SMALL=font_small, 	$
		  EDIT_FONTS=edit_fonts


		;**** declare common variables ****

  COMMON proc103_blk,action,value

		;**** Copy "procval" to common ****

  value = procval

		;**** Set defaults for keywords ****

  IF NOT (KEYWORD_SET(font_large)) THEN font_large = ''
  IF NOT (KEYWORD_SET(font_small)) THEN font_small = ''
  IF NOT (KEYWORD_SET(edit_fonts)) THEN 				$
				edit_fonts = { font_norm:'', font_input:''}
		
                ;**** create base widget ****

  procid = widget_base(TITLE='ADAS103: PROCESSING OPTIONS', 		$
					XOFFSET=200,YOFFSET=0)

		;**** Declare processing widget ****

  cwid = cw_adas103_proc(procid, dsfull,lchoice,			$
                         nedim, nxdim, ntdim,				$
                         tempi, rate, tempo, bitfile,			$
			 PROCVAL=value, 				$
		 	 FONT_LARGE=font_large, FONT_SMALL=font_small, 	$
			 EDIT_FONTS=edit_fonts)

		;**** Realize the new widget ****

  dynlabel, procid
  widget_control,procid,/realize

		;**** make widget modal ****

  xmanager,'adas103_proc',procid,event_handler='adas103_proc_ev', 	$
					/modal,/just_reg

		;*** copy value back to procval for return to e1ispf ***

  act = action
  procval = value
 
END

