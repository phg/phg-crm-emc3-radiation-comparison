; Copyright (c) 1996, Strathclyde University .
; SCCS info: Module @(#)$Header: /home/adascvs/idl/adas1xx/adas103/plot103a.pro,v 1.1 2004/07/06 14:28:39 whitefor Exp $ Date $Date: 2004/07/06 14:28:39 $
;+
; PROJECT:
;       ADAS IBM MVS to UNIX conversion
;
; NAME:
;	PLOT103A
;
; PURPOSE:
;	Plot one graph for ADAS103.
;
; EXPLANATION:
;	This routine plots ADAS103 output for a single graph.
;
; USE:   
;
; INPUTS:
;	X	- Float Array; List of x-values
;
;	Y	- Float Array; list of y-values
;
;	TITLE	- String array : General title for program run. 5 lines.
;
;	XTITLE  - String : title for x-axis annotation
;
;	YTITLE  - String : title for y-axis annotation
;
;	LDEF1	- Integer; 1 - User specified axis limits to be used, 
;		  	   0 - Default scaling to be used.
;
;	XMIN	- String; Lower limit for x-axis of graph, number as string.
;
;	XMAX	- String; Upper limit for x-axis of graph, number as string.
;
;	YMIN	- String; Lower limit for y-axis of graph, number as string.
;
;	YMAX	- String; Upper limit for y-axis of graph, number as string.
;
; OPTIONAL INPUTS:
;	None.
;
; OUTPUTS:
;	None.
;
; OPTIONAL OUTPUTS:
;	None.
;
; KEYWORD PARAMETERS:
;	None.
;
; CALLS:
;       LOCALIZE        Routine to set the various position parameters
;                       used in output plotting.
;
; SIDE EFFECTS:
;	None.
;
; CATEGORY:
;	Adas system.	
;
; WRITTEN:
;       William Osborn, Tessella Support Services plc, 05-Nov-96
;
; MODIFIED:
;       1.1     William Osborn
;		Wrote using plot105a as a template
;
; VERSION:
;	1.1	05-11-96
;
;-
;----------------------------------------------------------------------------

PRO plot103a, x , y,  title, xtitle, ytitle, 			$
             xmin, xmax, ymin, ymax, ldef1
                
  COMMON Global_lw_data, left, right, top, bottom, grtop, grright

                ;****************************************************
		;**** Suitable character size for current device ****
		;**** Aim for 60 characters in y direction.      ****
		;****************************************************

  charsize = (!d.y_vsize/!d.y_ch_size)/60.0
  small_check = GETENV('VERY_SMALL')
  if small_check eq 'YES' then charsize=charsize*0.8

		;**** set makeplot counter ****

  makeplot = 1

		;**** set values of window area positions ****

  localize

		;**** Window area positions ****

; left = 0.1
; right = 0.9
; top = 0.91
; bottom = 0.1
; grtop = 0.8
; grright = 0.70

		;***************************************
		;**** Construct graph title         ****
		;**** "!C" is the new line control. ****
		;***************************************

  !p.font=-1
  if small_check eq 'YES' then begin
      gtitle =  title(0) + "!C!C" + title(1) + "!C!C" + title(2) + "!C!C" + $
        title(3) + "!C!C" + title(4)
  endif else begin
      gtitle =  title(0) + "!C" + title(1) + "!C" + title(2) + "!C" + $
        title(3) + "!C" + title(4)
  endelse
		;**** Find x and y ranges for auto scaling ****

  if ldef1 eq 0 then begin
		;**** identify values in the valid range ****
		;**** plot routines only work within ****
		;**** single precision limits.	     ****
    xvals = where(x gt 1.0e-37 and x lt 1.0e37)
    yvals = [where(y lt -1.0e-37 and y gt -1.0e37),where(y gt 1.0e-37 and y lt 1.0e37)]

    if xvals(0) gt -1 then begin
      makeplot = 1
      xmax = max(x(xvals), min=xmin)
      end else begin
      makeplot = 0
    end

    if yvals(0) gt -1 then begin
      makeplot = 1
      ymax = max(y(yvals), min=ymin)
    end else begin
      makeplot = 0
    end

    style = 0
  end else begin
		;Check that at least some data in in axes range ***
    xvals = where(x ge xmin and x le xmax)
    yvals = where(y ge ymin and y le ymax)
    if xvals(0) eq -1 or yvals(0) eq -1 then begin
      makeplot = 0
    end
    style = 1
  end


		;**** Set up plotting axes ****

    plot,   [xmin,xmax],[ymin,ymax],/nodata, 		$
		position=[left,bottom,grright,grtop], 			$
		xtitle=xtitle, ytitle=ytitle, xstyle=style, ystyle=style, $
		charsize=charsize

                ;**** spline done in IDL rather than FORTRAN ****

    spline_p, x(2,*), y(2,*), xs, ys, interval=x(2,1)-x(2,0),tan0=[1,0]

    if makeplot eq 1 then begin

		;**********************
		;****  Make plots  ****
		;**********************
        np = n_elements(x(0,*))-1
        for i=0,3 do begin
            oplot, x(i,*), y(i,*), linestyle=i
        endfor
        oplot, xs, ys, psym=-1, linestyle=2

      ;*** Append labels ****
      xyouts, x(0,np)+0.05, y(0,np),'(ALF/ALFG) I',charsize=charsize*0.95
      xyouts, x(1,np)+0.05, y(1,np),'(ALF/ALFP) I',charsize=charsize*0.95
      xyouts, x(2,np)+0.05, y(2,np),'(ALF/ALFG) F',charsize=charsize*0.95
      xyouts, x(3,np)+0.05, y(3,np),'(ALF/ALFP) F',charsize=charsize*0.95

  endif else begin
     print, "ADAS103 : No data found in these axes ranges"
  endelse

		;**** plot title ****

      xyouts,(left-0.05), top, gtitle, /normal, alignment=0.0, 		$
			charsize=charsize*0.95

END
