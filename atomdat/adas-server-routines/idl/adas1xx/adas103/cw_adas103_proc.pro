; Copyright (c) 1996, Strathclyde University.
; SCCS info: Module @(#)$Header: /home/adascvs/idl/adas1xx/adas103/cw_adas103_proc.pro,v 1.2 2004/07/06 12:27:59 whitefor Exp $ Date $Date: 2004/07/06 12:27:59 $
;+
; PROJECT:
;       ADAS IBM MVS to   UNIX conversion
;
; NAME:
;	CW_ADAS103_PROC()
;
; PURPOSE:
;	Produces a widget for ADAS103 processing options/input.
;
; EXPLANATION:
;	This function creates a compound widget consisting of :-
;	   a text widget holding an editable 'Run title', 
;	   the dataset name/browse widget cw_adas_dsbr,
;          various input option widgets,
;	   a table widget for temperature/density data cw_adas_table,
;	   buttons to enter default values into the table, 
;	   a message widget, a 'Cancel' and a 'Done' button, and
;          energy units options buttons.
;
;	The compound widgets included in this widget are self managing.
;	This widget only manages events from the two 'Defaults' buttons,
;	the 'Cancel' button and the 'Done' button, and from the energy units
;       buttons.
;
;	This widget only generates events for the 'Done' and 'Cancel'
;	buttons.
;
; USE:
;	This widget is specific to ADAS103, see adas103_proc.pro
;	for use.
;
; INPUTS:
;	TOPPARENT- Long integer, ID of parent widget.
;
;	DSFULL	- String; The full system file name of the input 
;		  dataset selected by the user for processing.
;
;	LCHOICE - Integer; The archiving choice of the user, see 
;                 a1spf0.pro
;
;	NTDIM	- Integer; maximum number of temperatures allowed.
;
;	NEDIM	- Integer; maximum number of energies allowed.
;
;	NXDIM	- Integer; maximum number of x-sections allowed.
;
;	TEMPO	- float array; The output temperature values (K).
;
;	TEMPI	- float array; The input temperature values (K).
;
;	RATE	- float array; The rate values.
;
;       BITFILE - String; the path to the directory containing bitmaps
;                 for the 'escape to series menu' button.
;
; OPTIONAL INPUTS:
;	None
;
; OUTPUTS:
;	The return value of this function is the ID of the declared widget.
;
; OPTIONAL OUTPUTS:
;	None
;
; KEYWORD PARAMETERS:
;	PROCVAL	- A structure which determines the initial settings of
;		  the processing options widget.
;
;		  The default PROCVAL is created thus;
;      tempo_arr = fltarr(ntdim)
;      rate_arr = fltarr(nxdim)
;      tempi_arr = fltarr(nedim)
;      edarr  = fltarr(2)
;      igarr  = intarr(2)
;      niarr  = intarr(2,6)
;      wiarr  = fltarr(2,6)
;      ifarr = intarr(3)
;     ps = { 			 		  $
;                new    : 0,                      $
;		 maxe   : 0,$
;		 maxx	: 0,$
;		 maxt	: 0,$
;		 tin	: tempo_arr,$
;		 ein 	: tempi_arr,$
;		 xin	: rate_arr,$
;		 z	: 0,$
;		 z0	: 0,$
;		 z1 	: 0,$
;                n0      : 0,                    $
;                v0      : 0.0,                  $
;                phfrac  : 0.0,                  $
;                ingrup  : 1,                    $
;                edgrp   : edarr,                $
;                scgrp   : edarr,                $
;                igrp    : igarr,                $
;                ni      : niarr,                $
;                li      : niarr,                $
;                wi      : wiarr,                $
;                nj      : niarr,                $
;                lj      : liarr,                $
;                wj      : wiarr,                $
;                eij     : wiarr,                $
;                aprob   : wiarr,                $
;                corfac  : wiarr,                $
;                ncut    : niarr,                $
; 		 ifout	 : ifarr		 $
;              }
;
;		NEW     Flag which defines whether or not default values
;			exist or not. (< 0 if no values exist)
;		MAXE    Number of energy values entered/refreshed
;		MAXX    Number of x-sections values entered/refreshed
;		MAXT    Number of temperature values entered/refreshed
;		TIN     User supplied output temperature values.
;		EIN 	User supplied input temp. values.
;		XIN 	User supplied rate values
;               Z       Initial Ion Charge
;               Z0      Nuclear Charge
;               Z1      Final Ion Charge
;               N0      Lowest accessible principle quantum no.
;               V0      Lowest accessible eff. principle quantum no.
;               PHFRAC  Lowest accessible phase occupation factor
;               INGRUP  No of core transition groups
;               IIATYP  Transition prob. form, 1= a-coefficient
;                                              2= oscillator strength
;                                              3= line strength
;               IGRP()  No of entries for each group
;               EDGRP() Initial mean energy disp. for each group
;               SCGRP() Mean scale factor for each group
;               NI      Transition group info
;               LI      "
;               WI      "
;               NJ      "
;               LJ      "
;               WJ      "
;               EIJ     "
;               WJ      "
;               APROB   "
;               CORFAC  "
;               NCUT    "
;               IFOUT   Temperature and rate units.
;
;	UVALUE	- A user value for the widget. Default 0.
;
;	FONT_LARGE - The name of a larger font.  Default current system
;		     font
;
;	FONT_SMALL - The name of a smaller font. Default current system
;		     font.
;
;	EDIT_FONTS - A structure of two fonts used in the table editor
;		     adas_edtab.pro, {font_norm:'',font_input:''}.  Defaults
;		     to current system font.
;
;	NUM_FORM   - String; Numeric format to use in tables.  Default
;			'(E10.3)'
;		     
;
; CALLS:
;	POPUP		Popup warning window with buttons.
;	CW_ADAS_DSBR	Dataset name and comments browsing button.
;	CW_ADAS_TABLE	Adas data table widget.
;       CW_TMPL         Transition Information
;	See side effects for other related widget management routines.
;
; SIDE EFFECTS:
;
;	The following widget management routines are included in this file;
;	PROC103_GET_VAL()	Returns the current PROCVAL structure.
;	PROC103_EVENT()		Process and issue events.
;	
; CATEGORY:
;	Compound Widget
;
; WRITTEN:
;       William Osborn, Tessella Support Services plc. 4th November 1996
;       Used cw_adas105_proc.pro as framework and extensively modified it.
; MODIFIED:
;	1.1	William Osborn
;		First release
;	1.2	William Osborn
;		Corrected 'Default Temperature' button action
;
; VERSION:
;	1.1	04-11-96
;	1.1	08-11-96
;
;-
;-----------------------------------------------------------------------------

FUNCTION proc103_get_val, id

                ;**** Return to caller on error ****
;  ON_ERROR, 2

		 ;***************************************
                 ;****     Retrieve the state 	     ****
		 ;**** Get first_child widget id     ****
		 ;**** because state is stored there ****
		 ;***************************************

  first_child = widget_info(id, /child)
  widget_control, first_child, get_uvalue = state

		;***************************
		;**** Get input options ****
		;***************************

  widget_control, state.z0id, get_value=val
  if num_chk(val(0)) eq 0 then z0 = float(val(0)) else z0 = 0.0
  widget_control, state.zid, get_value=val
  if num_chk(val(0)) eq 0 then z = float(val(0)) else z = 0.0
  widget_control, state.z1id, get_value=val
  if num_chk(val(0)) eq 0 then z1 = float(val(0)) else z1 = 0.0
  widget_control, state.n0id, get_value = val
  if num_chk(val(0),/integer) eq 0 then n0 = fix(val(0)) else n0=1
  widget_control, state.v0id, get_value = val
  if num_chk(val(0)) eq 0 then v0 = float(val(0)) else v0=1.0
  widget_control, state.phfracid, get_value = val
  if num_chk(val(0)) eq 0 then phfrac = float(val(0)) else phfrac=1.0

		;******************************
		;**** Get trans group info ****
		;******************************

  edgrp = fltarr(2)
  scgrp = edgrp
  igrp = intarr(2)
  ni = intarr(2,6)
  li=ni
  nj=ni
  lj=ni
  wi=fltarr(2,6)
  wj=wi
  eij=wi
  aprob=wi
  corfac=wi
  ncut=ni
  widget_control, state.ingrupid, get_value = val
  ingrup = val(0)+1
  for i=0,ingrup-1 do begin
    widget_control, state.edgrpid(i), get_value = val
    if num_chk(val(0)) eq 0 then edgrp(i)=val(0)
    widget_control, state.scgrpid(i), get_value = val
    if num_chk(val(0)) eq 0 then scgrp(i)=val(0)    
    widget_control, state.cttid(i), get_value = val
    data = val.value
    blanks = where(strtrim(data(0,*),2) eq '')
    if blanks(0) ge 0 then igrp(i) = blanks(0) else igrp(i)=6
    if igrp(i) gt 0 then begin
      ni(i,0:igrp(i)-1)=fix(data(0,0:igrp(i)-1))
      li(i,0:igrp(i)-1)=fix(data(1,0:igrp(i)-1))
      wi(i,0:igrp(i)-1)=float(data(2,0:igrp(i)-1))
      nj(i,0:igrp(i)-1)=fix(data(3,0:igrp(i)-1))
      lj(i,0:igrp(i)-1)=fix(data(4,0:igrp(i)-1))
      wj(i,0:igrp(i)-1)=float(data(5,0:igrp(i)-1))
      eij(i,0:igrp(i)-1)=float(data(6,0:igrp(i)-1))
      aprob(i,0:igrp(i)-1)=float(data(7,0:igrp(i)-1))
      corfac(i,0:igrp(i)-1)=float(data(8,0:igrp(i)-1))
      ncut(i,0:igrp(i)-1)=fix(data(9,0:igrp(i)-1))
    endif
  endfor

		;****************************************************
		;**** Get new temperature data from table widget ****
		;****************************************************

  widget_control,state.tempid,get_value=tempval
  tabledata = tempval.value
  ifout = intarr(3)
  ifout(0) = tempval.units(0)
  ifout(1) = tempval.units(1)+3
  ifout(2) = tempval.units(2)+5

		;**** Copy out temperature values ****

  ein = dblarr(state.nedim)
  xin = dblarr(state.nxdim)
  tin = dblarr(state.ntdim)
  blanks = where(strtrim(tabledata(ifout(0),*),2) eq '')
		;**** next line assumes that all blanks are ****
		;**** at the end of the columns             ****
  if blanks(0) ge 0 then maxe=blanks(0) else maxe=state.nedim
  if maxe gt 0 then ein(0:maxe-1) = double(tabledata(ifout(0),0:maxe-1))
  blanks = where(strtrim(tabledata(ifout(1),*),2) eq '')
  if blanks(0) ge 0 then maxx=blanks(0) else maxx=state.nxdim
  if maxx gt 0 then xin(0:maxx-1) = double(tabledata(ifout(1),0:maxx-1))
  blanks = where(strtrim(tabledata(ifout(2),*),2) eq '')
  if blanks(0) ge 0 then maxt=blanks(0) else maxt=state.ntdim
  if maxt gt 0 then tin(0:maxt-1) = double(tabledata(ifout(2),0:maxt-1))

		;**** Fill out the rest with zero ****

  if maxt lt state.ntdim then begin
    tin(maxt:state.ntdim-1) = double(0.0)
  endif
  if maxx lt state.nxdim then begin
    xin(maxx:state.nxdim-1) = double(0.0)
  endif 
  if maxe lt state.nedim then begin
    xin(maxe:state.nedim-1) = double(0.0)
  endif 

		;***********************************************
		;**** write selected values to PS structure ****
		;***********************************************

     ps = { 			 		 $
                new    : 0,                      $
		maxe 	: maxe,				$
		maxx	: maxx,				$
		maxt	: maxt,				$
		tin	: tin,				$
		ein 	: ein,				$
		xin	: xin,				$
		z	: z,				$
		z0	: z0,				$
		z1 	: z1,				$
                n0      : n0,                   $
                v0      : v0,                   $
                phfrac  : phfrac,               $
                ingrup  : ingrup,               $
                edgrp   : edgrp,                $
                scgrp   : scgrp,                $
                igrp    : igrp,                 $
                ni      : ni,                   $
                li      : li,                   $
                wi      : wi,                   $
                nj      : nj,                   $
                lj      : lj,                   $
                wj      : wj,                   $
                eij     : eij,                  $
                aprob   : aprob,                $
                corfac  : corfac,               $
                ncut    : ncut,                 $
		ifout	: ifout				$
              }
   
  widget_control, first_child, set_uvalue=state, /no_copy

  RETURN, ps

END

;-----------------------------------------------------------------------------

FUNCTION proc103_event, event

                ;**** Base ID of compound widget ****

  parent=event.handler

		;**********************************************
                ;**** Retrieve the user value state        ****
		;**** Get id of first_child widget because ****
		;**** user value "state" is stored there   ****
		;**********************************************

  first_child = widget_info(parent, /child)
  widget_control, first_child, get_uvalue=state,/no_copy

		;*********************************
		;**** Clear previous messages ****
		;*********************************

  widget_control,state.messid,set_value=' '

                ;************************
                ;**** Process events ****
                ;************************

  cASE event.id OF

		;*************************************
		;**** Default temperature button ****
		;*************************************

    state.deftid: begin

		;**** popup window to confirm overwriting current values ****

	action= popup(message='Confirm Overwrite values with Defaults', $
			buttons=['Confirm','Cancel'],font=state.font)

	if action eq 'Confirm' then begin

		;**** Get current table widget value ****

 	   widget_control,state.tempid,get_value=tempval

;           units = tempval.units(2)+7
	   units = 5
	   tempval.units(2) = 0
  	   maxt = state.maxt
 	   if maxt gt 0 then begin

                ;**** default temperature values ****
                ;**** can add up to maxt-1 as required ****

;            tvalues = [2.50e+04,5.00e+04,1.25e+05,2.50e+05,5.00e+05,	$
;                       1.25e+06,2.50e+06,5.00e+06]
	    tvalues = [1e6,2e6,4e6,7e6,1e7,2e7,4e7,7e7,1e8,2e8]
            ndef = n_elements(tvalues)
            deftemp = strarr(ndef)
            for i = 0,ndef-1 do begin
               deftemp(i) = strtrim(string(tvalues(i),format=state.num_form))
            end
            for i = 0,maxt-1 do begin
               if i le ndef-1 then begin
   	         tempval.value(units,i) =  $
			   string(deftemp(i) , format=state.num_form)
               end else begin
                 tempval.value(units,i) = ''
               end
            endfor
 	  end

		;**** Fill out the rest with blanks ****

 	  if maxt lt state.ntdim then begin
            tempval.value(units,maxt:state.ntdim-1) = ''
 	  end

		;**** Copy new data to table widget ****

 	  widget_control,state.tempid, set_value=tempval
	end

      end

		;****************************
		;**** User input options ****
		;****************************

    state.z0id: widget_control, state.zid, /input_focus
    state.zid: widget_control, state.z1id, /input_focus
    state.z1id: widget_control, state.z0id, /input_focus
;???? z1id -> change units ????
    state.n0id: widget_control, state.v0id, /input_focus
    state.v0id: widget_control, state.phfracid, /input_focus
    state.phfracid: widget_control, state.n0id, /input_focus

		;********************************************
		;**** Number of transition groups button ****
		;********************************************
    state.ingrupid: begin
	if event.index eq 0 then begin
	    widget_control, state.cttbase(0), /sensitive, /map
	    widget_control, state.cttbase(1), sensitive=0, map=0
	    widget_control, state.ctbutid, set_value=0
	endif else begin
	    widget_control, state.cttbase(0), /sensitive
	    widget_control, state.cttbase(1), /sensitive
	endelse
    end
		;****************************************
		;**** Transition group choice button ****
		;****************************************
    state.ctbutid: begin
	if event.value eq 0 then begin
	    widget_control, state.cttbase(0), /map
	    widget_control, state.cttbase(1), map=0
	    widget_control, state.ctbutid, set_value=0
	endif else begin
	    widget_control, state.cttbase(0), map=0
	    widget_control, state.cttbase(1), /map
	    widget_control, state.ctbutid, set_value=1
	endelse
    end
		;***********************
		;**** Cancel button ****
		;***********************

    state.cancelid: new_event = {ID:parent, TOP:event.top, 		$
				HANDLER:0L, ACTION:'Cancel'}

		;*********************
		;**** Done button ****
		;*********************

    state.doneid: begin

		;***************************************
		;**** Check all user input is legal ****
		;***************************************

	error = 0

        	;********************************************
		;*** Have to restore "state" before calls ***
		;*** to proc103_get_val so it can be used ***
		;*** there.				  ***
		;********************************************
		
	widget_control, first_child, set_uvalue=state, /no_copy

	widget_control,event.handler,get_value=ps

		;*** reset state variable ***

	widget_control, first_child,get_uvalue=state, /no_copy

		;**** Check input parameter values ****

        widget_control, state.z0id, get_value=val
	if num_chk(val(0)) ne 0 then begin
	  error = 1
	  message = 'Error: Z0 not a number'
	  widget_control, state.z0id, /input_focus
    	endif
        widget_control, state.zid, get_value=val
	if num_chk(val(0)) ne 0 then begin
	  error = 1
	  message = 'Error: Z not a number'
	  widget_control, state.zid, /input_focus
    	endif
        widget_control, state.z1id, get_value=val
	if num_chk(val(0)) ne 0 then begin
	  error = 1
	  message = 'Error: Z1 not a number'
	  widget_control, state.z1id, /input_focus
    	endif
	for i=0,ps.ingrup-1 do begin
	  widget_control, state.edgrpid(i), get_value=val
	  if num_chk(val(0)) ne 0 then begin
	    error = 1
	    message = 'Error: energy displacement not a number'
            widget_control, state.cttbase(i), map=1
            widget_control, state.cttbase((i+1) MOD 2), map=0
            widget_control, state.edgrpid(i), /input_focus
	    widget_control, state.ctbutid, set_value = i
    	  endif
	  widget_control, state.scgrpid(i), get_value=val
	  if num_chk(val(0)) ne 0 then begin
	    error = 1
	    message = 'Error: scale factor not a number'
            widget_control, state.cttbase(i), map=1
            widget_control, state.cttbase((i+1) MOD 2), map=0
            widget_control, state.scgrpid(i), /input_focus
	    widget_control, state.ctbutid, set_value = i
    	  endif
	endfor
		;**** check temp/density values entered ****

	if error eq 0 and ps.maxt eq 0 then begin
	  error = 1
	  message='Error: No input temperatures entered.'
        end
	if error eq 0 and ps.maxx eq 0 then begin
          error = 1
          message='Error: No rates entered.'
        end
	if error eq 0 and ps.maxe eq 0 then begin
          error = 1
          message='Error: No output temperatures entered.'
        end


		;**** return value or flag error ****

	if error eq 0 then begin
	  new_event = {ID:parent, TOP:event.top, HANDLER:0L, ACTION:'Done'}
        end else begin
	  widget_control,state.messid,set_value=message
	  new_event = 0L
        end

      end

		;*********************
                ;**** Menu button ****
		;*********************

        state.outid: begin
            new_event = {ID:parent, TOP:event.top, HANDLER:0L,          $
                         ACTION:'Menu'}
        end

    ELSE: new_event = 0L

  ENDCASE

		;*********************************************
		;*** make "state" available to other files ***
		;*********************************************

  widget_control, first_child, set_uvalue=state,/no_copy

  RETURN, new_event
END

;-----------------------------------------------------------------------------

function cw_adas103_proc, topparent, dsfull, lchoice, nedim, nxdim, 	$
                          ntdim, tempi, rate, tempo, bitfile, 		$
			  procval=procval, 				$
                          uvalue=uvalue, font_large=font_large, 	$
                          font_small=font_small, edit_fonts=edit_fonts, $
                          num_form=num_form

		;**** Set defaults for keywords ****

  if not (keyword_set(uvalue)) then uvalue = 0
  if not (keyword_set(font_large)) then font_large = ''
  if not (keyword_set(font_small)) then font_small = ''
  if not (keyword_set(edit_fonts)) then 				$
				edit_fonts = {font_norm:'',font_input:''}
  if not (keyword_set(num_form)) then num_form = '(E10.3)'

  if not (keyword_set(procval)) then begin     
    tempo_arr = fltarr(ntdim)
    rate_arr = fltarr(nxdim)
    tempi_arr = fltarr(nedim)
    edarr  = fltarr(2)
    igarr  = intarr(2)
    niarr  = intarr(2,6)
    wiarr  = fltarr(2,6)
    ifarr = intarr(3)

    ps = { 			 		$
                new    	: 0,                    $
		maxe 	: nedim,		$
		maxx	: nxdim,		$
		maxt	: ntdim,		$
		tin	: tempo_arr,		$
		ein 	: tempi_arr,		$
		xin	: rate_arr,		$
		z	: 0,			$
		z0	: 0,			$
		z1 	: 0,			$
                n0      : 0,                    $
                v0      : 0.0,                  $
                phfrac  : 0.0,                  $
                ingrup  : 1,                    $
                edgrp   : edarr,                $
                scgrp   : edarr,                $
                igrp    : igarr,                $
                ni      : niarr,                $
                li      : niarr,                $
                wi      : wiarr,                $
                nj      : niarr,                $
                lj      : liarr,                $
                wj      : wiarr,                $
                eij     : wiarr,                $
                aprob   : wiarr,                $
                corfac  : wiarr,                $
                ncut    : niarr,                $
		ifout	: ifarr			$
              }
	ps.ifout(1)=3
	ps.ifout(2)=5
  endif else begin
    ps = { 			 			$
                new    	: procval.new,			$
		maxe 	: procval.maxe,			$
		maxx	: procval.maxx,			$
		maxt	: procval.maxt,			$
		tin	: procval.tin,			$
		ein 	: procval.ein,			$
		xin	: procval.xin,			$
		z	: procval.z,			$
		z0	: procval.z0,			$
		z1 	: procval.z1,			$
                n0      : procval.n0,                   $
                v0      : procval.v0,                   $
                phfrac  : procval.phfrac,               $
                ingrup  : procval.ingrup,               $
                edgrp   : procval.edgrp,                $
                scgrp   : procval.scgrp,                $
                igrp    : procval.igrp,                 $
                ni      : procval.ni,                   $
                li      : procval.li,                   $
                wi      : procval.wi,                   $
                nj      : procval.nj,                   $
                lj      : procval.lj,                   $
                wj      : procval.wj,                   $
                eij     : procval.eij,                  $
                aprob   : procval.aprob,                $
                corfac  : procval.corfac,               $
                ncut    : procval.ncut,                 $
		ifout	: procval.ifout			$
              }
  endelse

                ;*********************************************************
                ;**** Modify certain parameters and results depending ****
                ;**** on the machine being used                       ****
                ;*********************************************************

  machine = GETENV('TARGET_MACHINE')
  if machine eq 'HPUX' then begin
    y_size = 4
    large_font = font_small
  endif else begin
    y_size = 6
    large_font = font_large
  endelse

		;****************************************************
		;**** The adas table widget requires data to be *****
		;**** input as strings, so all the numeric data *****
		;**** has to be written into a string array.    *****
		;**** cols.1,2,3 - Input temp. units            *****
		;**** cols.4,5   - rate      units              *****
		;**** cols.6,7,8 - Output temp. units           *****
		;****************************************************

  maxt = ps.maxt  
  maxx = ps.maxx
  maxe = ps.maxe
  neg_form='(E11.4)'
  tabledata = strarr(10,nxdim)

  if lchoice eq 1 or lchoice eq 2 then begin

    If (maxt gt 0) then begin

      tabledata(0,0:maxe-1) = 						$
	        strtrim(string(tempi(0:maxe-1,0),format=num_form),2)
      tabledata(1,0:maxe-1) = 						$
		strtrim(string(tempi(0:maxe-1,1),format=num_form),2)
      tabledata(2,0:maxe-1) = 						$
                strtrim(string(tempi(0:maxe-1,2),format=num_form),2)
      tabledata(3,0:maxx-1) = 						$
	        strtrim(string(rate(0:maxx-1,0),format=num_form),2)
      tabledata(4,0:maxx-1) = 						$
		strtrim(string(rate(0:maxx-1,1),format=neg_form),2)
      tabledata(5,0:maxt-1) = 						$
	        strtrim(string(tempo(0:maxt-1,0),format=num_form),2)
      tabledata(6,0:maxt-1) = 						$
		strtrim(string(tempo(0:maxt-1,1),format=num_form),2)
      tabledata(7,0:maxt-1) = 						$
                strtrim(string(tempo(0:maxt-1,2),format=num_form),2)

    endif
	;**** fill rest of table with blanks ****

    if maxe lt nedim then tabledata(0:2,maxe:nedim-1) = ' '
    if maxx lt nxdim then tabledata(3:6,maxx:nxdim-1) = ' '
    if maxt lt ntdim then tabledata(7:9,maxt:ntdim-1) = ' '

  endif else begin
	;**** If no archive chosen then use the values from the last ****
	;**** time we were here. Different to 101 where set to blank.****
    tabledata(*,*)=' '
    if maxt gt 0 then begin
      tabledata(ps.ifout(0),0:maxe-1) = 				$
	          strtrim(string(ps.ein(0:maxe-1),format=num_form),2)
      tabledata(ps.ifout(1),0:maxx-1) = 				$
		strtrim(string(ps.xin(0:maxx-1),format=num_form),2)
      tabledata(ps.ifout(2),0:maxt-1) = 				$
                strtrim(string(ps.tin(0:maxt-1),format=num_form),2)
    endif
  endelse

		;********************************************************
		;**** Create the 103 Processing options/input window ****
		;********************************************************

		;**** create titled base widget ****

  parent = widget_base(topparent, UVALUE = uvalue, 			$
			title = 'ADAS103 PROCESSING OPTIONS', 		$
			EVENT_FUNC = "proc103_event", 			$
			FUNC_GET_VALUE = "proc103_get_val", 		$
			/COLUMN)

		;******************************************************
		;**** Create a dummy widget just to hold value of *****
		;**** "state" variable so as not to get confused  *****
		;**** with any other values. Adopt IDL practice   *****
		;**** of using first child widget                 *****
		;******************************************************

  first_child = widget_base(parent)

  topbase = widget_base(first_child,/column)

		;*******************************************************
		;**** add run title, dataset name and browse button ****
		;*******************************************************

  rc = cw_adas_dsbr(topbase, dsfull, font=large_font,			$
                    button='Browse Index')


		;*******************************
		;**** Add the input options ****
		;*******************************

  inpbase = widget_base(topbase, /row, /frame)
  base1 = widget_base(inpbase, /column)
  base1a = widget_base(base1, /row)
  base1b = widget_base(base1, /row)
  base1c = widget_base(base1, /row)
  base2 = widget_base(inpbase, /column)
  base2a = widget_base(base2, /row)
  base2b = widget_base(base2, /row)
  base2c = widget_base(base2, /row)
  base2d = widget_base(base2, /row)
  l = widget_label(base1a, value='Nuclear charge Z0   ', font=font_small)
  z0id = widget_text(base1a, value=string(ps.z0,format=num_form), $
		font=font_small, /editable, xsize=10)
  l = widget_label(base1b, value='Initial ion charge Z', font=font_small)
  zid = widget_text(base1b, value=string(ps.z, format=num_form),  $
		font=font_small, /editable, xsize=10)
  l = widget_label(base1c, value='Final ion charge Z1 ', font=font_small)
  z1id = widget_text(base1c, value=string(ps.z1, format=num_form),$
		font=font_small, /editable, xsize=10)
  l = widget_label(base2a, value='Lowest accessible params. for diel. recomb.:', font=font_small)
  l= widget_label(base2b, value='Principal quantum number          ', font=font_small)
  n0id = widget_text(base2b, value=string(ps.n0, format='(I3)'),$
		font=font_small, /editable, xsize=10)
  l= widget_label(base2c, value='Effective principal quantum number', font=font_small)
  v0id = widget_text(base2c, value=string(ps.v0, format=num_form),$
		font=font_small, /editable, xsize=10)
  l= widget_label(base2d, value='Phase occupation factor           ', font=font_small)
  phfracid = widget_text(base2d, value=string(ps.phfrac, format=num_form),$
		font=font_small, /editable, xsize=10)
		
		;*****************************************
		;**** Base for core transition groups ****
		;*****************************************
  ctbase = widget_base(topbase,/column,/frame)

  base1 = widget_base(ctbase, /row)
  base2 = widget_base(ctbase,/column)
  base3 = widget_base(ctbase)
  l = widget_label(base1, value = 'No. of core transition groups:',font=font_small)
  ingrupid = cw_bselector(base1, ['1','2'], set_value=ps.ingrup-1,$
	font=font_small)
  ctbutid = cw_bgroup(base1, ['1','2'], font=font_small, /exclusive,$
		set_value=0, /row, label_left='Group: ')
		;*** bases for the two tables ***
  cttbase = lonarr(2)
  cttbase(0) = widget_base(base3, /column, /frame)
  cttbase(1) = widget_base(base3, /column, /frame, map=0)
  cttdata = strarr(4,6)
  cttid = lonarr(2)
  edgrpid = lonarr(2)
  scgrpid = lonarr(2)
  cttdata = strarr(10,6)
  colhead =['NI','LI','WI','NJ','LJ','WJ','EIJ(RYD)','TR.PROB.','CORFAC','NCUT']
  for i=0,1 do begin
    base = widget_base(cttbase(i),/row)
    l = widget_label(base, value='Initial mean energy disp.', 	$
		font=font_small)
    edgrpid(i) = widget_text(base, value=string(ps.edgrp(i), format=num_form),$
		font=font_small, /editable, xsize=10)
    l = widget_label(base, value='Mean scale factor', 	$
		font=font_small)
    scgrpid(i) = widget_text(base, value=string(ps.scgrp(i), format=num_form),$
		font=font_small, /editable, xsize=10)
		;******************************************************
		;**** Create table of information for each group   ****
		;******************************************************
    i1 = ps.igrp(i)
    if i1 gt 0 then begin
      cttdata(0,0:i1-1)=strtrim(string(ps.ni(i,0:i1-1,0)),2)
      cttdata(1,0:i1-1)=strtrim(string(ps.li(i,0:i1-1,0)),2)
      cttdata(2,0:i1-1)=strtrim(string(ps.wi(i,0:i1-1,0),	$
                                       format=num_form),2)
      cttdata(3,0:i1-1)=strtrim(string(ps.nj(i,0:i1-1,0)),2)
      cttdata(4,0:i1-1)=strtrim(string(ps.lj(i,0:i1-1,0)),2)
      cttdata(5,0:i1-1)=strtrim(string(ps.wj(i,0:i1-1,0)),2)
      cttdata(6,0:i1-1)=strtrim(string(ps.eij(i,0:i1-1,0),	$
                                       format=num_form),2)
      cttdata(7,0:i1-1)=strtrim(string(ps.aprob(i,0:i1-1,0)),2)
      cttdata(8,0:i1-1)=strtrim(string(ps.corfac(i,0:i1-1,0)),2)
      cttdata(9,0:i1-1)=strtrim(string(ps.ncut(i,0:i1-1,0)),2)
    endif
    if i1 lt 6 then begin
      cttdata(0:9,i1:5)=' '
    endif
		;****************************
		;**** table of data   *******
		;****************************

    forma = ['(I2)','(I2)',num_form,'(I2)','(I2)',num_form,num_form,num_form,$
             num_form,'(I4)']
    edit_fonts.font_norm=font_small
    edit_fonts.font_input=font_small
    cttid(i) = cw_adas_table(cttbase(i), cttdata,			$
                        COLHEAD=colhead, 				$
			LIMITS = [1,1,0,1,1,0,0,1,1,2], CELLSIZE = 8, 	$
			FLTINT = forma,	                                $
			FONTS = edit_fonts, FONT_LARGE = font_small, 	$
			FONT_SMALL = font_small, /scroll, ytexsize=3,   $
                        /ROWSKIP)

		;**************************************************************
		;**** Sensitize the base according to the number of groups ****
		;**************************************************************
    if ps.ingrup le i then widget_control, cttbase(i), sensitive=0

  endfor

		;**********************
		;**** Another base ****
		;**********************

  tablebase = widget_base(topbase,/row)

		;************************************************
		;**** base for the table and defaults button ****
		;************************************************

  base = widget_base(tablebase,/column,/frame)

  colhead = ['Input Temp.','Rate','Output Temp.']


		;********************************************
		;**** convert FORTRAN index to IDL index ****
		;********************************************

  units = intarr(3)
  units(0) = ps.ifout(0)
  units(1) = ps.ifout(1)-3
  units(2) = ps.ifout(2)-5

  unitname = [['Kelvin     ','eV         ','Scaled (TE(K)/(Z1^2))'],$
              ['Diel.(zero e- dens. rate)','Diel.+Rad.(zero e- dens. rate)','Dummy'],$
              ['Kelvin     ','eV         ','Scaled (TE(K)/(Z1^2))']]

		;**********************************************************
		;**** three columns in the table and 12 sets of units. ****
		;**** column 1 switches between sets 0,1,& 2           ****
		;**** in the input array tabledata as the units change.****
		;**** Similarly for columns 2 and 3                    ****
		;**********************************************************

  unitind = [[0,1,2],[3,4,4],[5,6,7]]
  unitstitle = ['Input temp. units','Input rate form','Output temp. units']


		;****************************
		;**** table of data   *******
		;****************************

  tempid = cw_adas_table(base, tabledata, units, unitname, unitind, 	$
			UNITSTITLE = unitstitle, 			$
                        COLHEAD=colhead, 				$
			ORDER = [1,0,1], /DIFFLEN, 			$ 
			LIMITS = [1,0,1], CELLSIZE = 12, 		$
			/SCROLL, ytexsize=y_size, NUM_FORM = num_form, 	$
			FONTS = edit_fonts, FONT_LARGE = font_small, 	$
			FONT_SMALL = font_small)

		;*************************
		;**** Default buttons ****
		;*************************

  tbase = widget_base(base, /column)
  deftid = widget_button(tbase,value=' Default Temperature Values  ',	$
		font=large_font)

		;**** Error message ****

  messid = widget_label(parent,font=large_font, 			$
  value='Edit the processing options data and press Done to proceed')

		;**** add the exit buttons ****

  base = widget_base(parent,/row)
  menufile = bitfile + '/menu.bmp'
  read_X11_bitmap, menufile, bitmap1
  outid = widget_button(base, value=bitmap1)          ;menu button
  cancelid = widget_button(base,value='Cancel',font=large_font)
  doneid = widget_button(base,value='Done',font=large_font)
  
		;**** create a state structure for the pop-up ****
		;**** 		     window.                  ****


   new_state = {messid:messid,          $
		deftid:deftid,		$
		tempid:tempid, 		$
		cancelid:cancelid,	$
		doneid:doneid, 		$
		outid:outid,		$
		z0id:z0id,		$
		zid:zid,		$
		z1id:z1id,		$
		ingrupid:ingrupid,	$
		ctbutid:ctbutid,	$
		cttbase:cttbase,	$
                ctbase:ctbase,	        $
                edgrpid:edgrpid,        $
                scgrpid:scgrpid,        $
		cttid:cttid,		$
		ntdim:ntdim,		$
		nxdim:nxdim,		$
                nedim:nedim,            $
		maxt:maxt, 		$
		maxx:maxx, 		$
                maxe:maxe,              $
		font:font_large,	$
		num_form:num_form,      $
                n0id:n0id,              $
                v0id:v0id,              $
                phfracid:phfracid       $
	      }

                 ;**** Save initial state structure ****

   widget_control, first_child, set_uvalue=new_state,/no_copy

  RETURN, parent

END

