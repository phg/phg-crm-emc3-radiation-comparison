; Copyright (c) 1995 Strathclyde University .
; SCCS info: Module @(#)$Header: /home/adascvs/idl/adas1xx/adaslib/add_pt.pro,v 1.1 2004/07/06 11:15:22 whitefor Exp $ Date $Date: 2004/07/06 11:15:22 $
;+
; PROJECT:
;       ADAS IBM MVS to DEC UNIX conversion & dveelopment
;
; NAME:
;       ADD_PT
;
; PURPOSE:
;       To add a point to an editable graph
;
; USE:
;       ADAS Graph Editor
;
; INPUTS:
;       X      - Float Array; Contains the x-coordinates of the plot.
;
;       Y      - Float Array; Contains the y-coordinates of the plot..
;
;       NSIZE  - Integer; The size of the array x
;
;       NSP    - Integer; the number of ADAS branch spline points.
;
;       XMIN   - Float; The minimum value of the x-coordinates, chosen
;                       by the user.
;
;       XMAX   - Float; The maximum value of the x-coordinates, chosen
;                       by the user.
;
;       YMIN   - Float; The minimum value of the y-coordinates, chosen
;                       by the user.
;
;       YMAX   - Float; The maximum value of the y-coordinates, chosen
;                       by the user.
;
; OPTIONAL INPUTS:
;       None
;
; OUTPUTS:
;       X      - Float Array; The edited values of the x-coordinates.
;
;       Y      - Float array; The edited values of the y-coordinates.
;
;       LPEND  - Integer; Determines whether any editing has been done,
;                         and whether to re-process.
;
; OPTIONAL OUTPUTS:
;       None
;
;
; KEYWORD PARAMETERS:
;       None
; CALLS:
;
; SIDE EFFECTS:
;       None
;
; CATEGORY: ADAS System
;
; WRITTEN:  DAVID H.BROOKS, Univ.of Strathclyde, EXT.4213/4205
;
; MODIFIED:
;       1.1     David H. Brooks/Tim Hammond
;               First release
; VERSION:
;       1.1     13-07-95
;-
;---------------------------------------------------------------------

pro add_pt, x, y, nsize, nsp, xmax, xmin, ymax, ymin, lpend

       ;**** set initial plot ****

    plot, x, y, xrange=[xmin,xmax], yrange=[ymin,ymax], /xstyle,	$
          title='left button selects position of point to add', psym=4
    oplot, x, y, linestyle=2

       ;****** SPLINE OPTION ******;

    if nsp ne -1 then begin
        fact = nsp/(max(x) - min(x))
        t = findgen(nsp)/fact + min(x)
        z = spline(x, y, t)
        oplot, t, z
    endif

    tvcrs,1                                 ; enable cursor
again: cursor, a, b, /down                  ; read first cursor location
        case !err of
            1:begin
                xt = fltarr(nsize+1)
                yt = xt
                ptx = 0
                if a ne x(nsize-1) then begin
                    for i = 0, nsize-2 do begin
                        if a ne x(i) then begin
                            if a gt x(nsize-1) then begin
                                ptx = nsize
                            endif else if a lt x(0) then begin
                                ptx = 0
                            endif else if(a gt x(i) and a lt x(i+1)) then begin
                                ptx = i+1
                            endif
                        endif else begin
                            print,					$
          'ADAS101 ERROR: xval matches an x element-new x not increasing'
                            goto, done  
                        endelse
                    endfor
                endif else begin
                    print,						$
          'ADAS101 ERROR: xval matches an x element-new x not increasing'
                   goto, done
                endelse 
                for i = 0, nsize-1 do begin
                    xt(i) = x(i)
                    yt(i) = y(i)
                endfor
                for i = 0, nsize-1 do begin
                    if i ge ptx then begin
                        xt(i+1) = x(i)
                        yt(i+1) = y(i)
                    endif
                endfor
                xt(ptx) = a
                yt(ptx) = b
                x = xt & y = yt
                plot, x, y, xrange=[xmin,xmax], yrange=[ymin,ymax],	$
                      /xstyle, psym=4
                oplot, x, y, linestyle=2
                if nsp ne -1 then begin
                    fact = nsp/(max(x) - min(x))
                    t = findgen(nsp+1)/fact + min(x)
                    z = spline(x,y,t)
                    oplot, t, z
                endif
                nsize = nsize+1
                lpend = 2
                goto, again
            end

            else: goto, done

        endcase

done:

END
