; Copyright (c) 1995 Strathclyde University .
; SCCS info: Module @(#)$Header: /home/adascvs/idl/adas1xx/adaslib/del_pt.pro,v 1.2 2004/07/06 13:30:09 whitefor Exp $ Date $Date: 2004/07/06 13:30:09 $
;+
; PROJECT:
;       ADAS IBM MVS to DEC UNIX conversion & dveelopment
;
; NAME:
;       DEL_PT
;
; PURPOSE:
;       To delete a point from an editable graph.
;
; EXPLANATION:
;
; USE:
;       ADAS Graph Editor
;
; INPUTS:
;       X      - Float Array; Contains the x-coordinates of the plot.
;
;       Y      - Float Array; Contains the y-coordinates of the plot..
;
;       NSIZE  - Integer; The size of the array x
;
;       NSP    - Integer; the number of ADAS branch spline points.
;
;       XMIN   - Float; The minimum value of the x-coordinates, chosen
;                       by the user.
;
;       XMAX   - Float; The maximum value of the x-coordinates, chosen
;                       by the user.
;
;       YMIN   - Float; The minimum value of the y-coordinates, chosen
;                       by the user.
;
;       YMAX   - Float; The maximum value of the y-coordinates, chosen
;                       by the user.
;
; OPTIONAL INPUTS:
;       None
;
; OUTPUTS:
;       X      - Float Array; The edited values of the x-coordinates.
;
;       Y      - Float array; The edited values of the y-coordinates.
;
;       LPEND  - Integer; Determines whether any editing has been done,
;                         and whether to re-process.
;
; OPTIONAL OUTPUTS:
;       None
;
;
; OPTIONAL OUTPUTS:
;       None
;
; KEYWORD PARAMETERS:
;       None
; CALLS:
;
; SIDE EFFECTS:
;       None
;
; CATEGORY: ADAS System
;
; WRITTEN:  DAVID H.BROOKS, Univ.of Strathclyde, EXT.4213/4205
;
; MODIFIED:
;	1.1	David H. Brooks
;		First release
;       1.2     William Osborn
;               Changed to normal coordinates for cursor positioning -
;               horizontal and vertical lines of points were not being
;               picked correctly.
; VERSION: 
;	1.1	12-07-95
;       1.2     03-10-96
;-
;---------------------------------------------------------------------
pro del_pt, x, y, nsize, nsp, xmax, xmin, ymax, ymin, lpend

             ;**** Define mouse selection accuracy   ****
             ;**** and convert to normal coordinates ****

    targetsqu = 0.0001
    points = convert_coord(x,y,/to_normal)
    xn = points(0,*)
    yn = points(1,*)

       ;**** set initial plot ****

    plot, x, y, xrange=[xmin,xmax], yrange=[ymin,ymax], /xstyle,	$
          title='left button selects point to delete', psym=4
    oplot, x, y, linestyle = 2

       ;****** SPLINE OPTION ******;

    if  nsp ne -1 then begin
        fact =  nsp/(max( x) - min( x))
        t = findgen( nsp)/fact + min( x)
        z = spline( x, y, t)
        oplot, t, z
    endif

       ;**** enable cursor and read first location ****

    tvcrs, 1                             
again: cursor, a, b, /down, /normal
    case !err of
        1:begin
            dist = (xn-a)^2+(yn-b)^2

            minim = min(dist,w1)

            if minim le targetsqu then begin
                ptx = w1
                xt = fltarr( nsize-1)
                yt = xt
                for i = 0, nsize-2 do begin
                    xt(i) =  x(i)
                    yt(i) =  y(i)
                endfor
                for i = 0, nsize-2 do begin
                    if i ge ptx then begin
                        xt(i) =  x(i+1)
                        yt(i) =  y(i+1)
                    endif
                endfor
                x = xt &  y = yt
                points=convert_coord(x,y,/to_normal)
                xn = points(0,*) &  yn = points(1,*)
                plot, x, y,						$
                  xrange=[xmin,xmax], yrange=[ymin,ymax],	$
                  /xstyle, psym=4
                oplot, x, y, linestyle = 2
                if  nsp ne -1 then begin
                    fact = nsp/(max(x) - min(x))
                    t = findgen( nsp+1)/fact + min(x)
                    z = spline( x, y, t)
                    oplot, t, z
                endif
                nsize =  nsize-1
                lpend = 2
            endif
            goto, again
        end

        else: goto, done

      endcase

done:

END    
    

