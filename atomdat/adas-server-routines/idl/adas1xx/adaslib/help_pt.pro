; Copyright (c) 1995 Strathclyde University .
; SCCS info: Module @(#)$Header: /home/adascvs/idl/adas1xx/adaslib/help_pt.pro,v 1.2 2004/07/06 14:05:32 whitefor Exp $ Date $Date: 2004/07/06 14:05:32 $
;+
; PROJECT:
;       ADAS IBM MVS to DEC UNIX conversion & dveelopment
;
; NAME:
;       HELP_PT
;
; PURPOSE:
;       To popup help on the mouse operations of ADAS_EDIT_GRAPH
;
; EXPLANATION:
;       This routine produces a popup widget with a help
;       factsheet on the mouse controls of thethe editable graph 
;       widget.
; USE:
;       ADAS Graph Editor
;
; INPUTS:
;       None
; OPTIONAL INPUTS:
;       None
;
; OUTPUTS:
;       None
; OPTIONAL OUTPUTS:
;       None
;
; KEYWORD PARAMETERS:
;       FONT - A font name
; CALLS:
;
; SIDE EFFECTS:
;       None
;
; CATEGORY: ADAS System
;
; WRITTEN: David H.Brooks, Univ.of Strathclyde, Ext.4213/4205
;
; VERSION:
;       1.1	David H.Brooks
;			- First Release    
;       1.2	Hugh Summers
;			- Made the widget modal    
; MODIFIED:
;	1.1	02-July-1995
;	1.2	09-June-1999
;
;--------------------------------------------------------------------------
PRO help_pt_event, event

    name = strmid(tag_names(event,/structure_name),7,1000)
    case (name) of 
        "BUTTON":begin
             widget_control, event.id, get_value = button
             if button eq ' done ' then begin
                 widget_control, /destroy, event.top
             endif
        end
    endcase

END
;---------------------------------------------------------------------------
PRO help_pt, FONT=font

    IF NOT (KEYWORD_SET(font)) THEN font = ''
    base = widget_base(title = 'ADAS1XX: MOUSE INSTRUCTIONS',XOFFSET = 550,$
                       YOFFSET = 5,/row)
    col = widget_base(base,/column)
    row1 = widget_base(col,/row)
    row2 = widget_base(col,/row)
 
    text=								  $
     ["About ADAS1XX_pmd                                                ",$
      "                                                                 ",$
      "     The ADAS1XX graph editing display program allows the  user  ",$
      "to interactively alter an x-y plot to the one they require.      ",$
      "It has facilities to add points between x-coordinates (either by ",$
      "value or with the mouse),to add points anywhere after a specified",$
      "point ( e.g. between y-coordinates), to delete points and to     ",$
      "interactively stretch the plot to the users' required fit. The   ",$
      "program can be called either with or without the standard IDL    ",$
      "library splining routine.                                        ",$
      "                                                                 ",$
      "Instructions                                                     ",$
      "                                                                 ",$
      "Generally: left button selects, right button exits               ",$
      "                                                                 ",$
      "Plot stretching mode: Select the 'stretch plot' button on the    ",$
      "widget display. Select the point you wish to move with the left  ",$
      "mouse button. Keeping the button down drag it to your desired    ",$
      "location. Once you are happy with its' position release the      ",$
      "button. Repeat as many times as desired. To exit this mode press ",$
      "the right mouse button.                                          ",$
      "                                                                 ",$
      "Delete mode: Select the 'delete a point' button on the widget    ",$
      "display. Select the point you wish to delete with the left mouse ",$
      "button. To exit this mode press the right mouse button.          ",$
      "                                                                 ",$
      "Add x-point mode: Select the 'add x-point' button in the widget  ",$
      "display. Decide where to place the new point. Add the point by   ",$
      "pressing the left mouse button. To exit this mode press the right",$
      "mouse button.                                                    ",$
      "Or: select the 'X-val' button in the widget display and enter a  ",$
      "value, then select the 'Y-val' button and enter a value [ N.B.   ",$
      "remember to press enter after typing to register your selection  ",$
      "with the event handling routines].Then select 'click to insert'  ",$
      "to add the point                                                 ",$
      "                                                                 ",$
      "N.B.the above two methods for adding points will place your      ",$
      "selection between the nearest two x-values to maintain the       ",$
      "x-vector as monotonically increasing.However the following method",$
      "allows points to be added anywhere.                              ",$
      "                                                                 ",$
      "Add anywhere mode: Select the 'add anywhere' button on the widget",$
      "display. Select the point you wish to insert your point after    ",$
      "with the left mouse button. Insert as many points after it as you",$
      "wish then press the right mouse button to stop adding after the  ",$
      "selected point. If you wish to add more points somewhere else,   ",$
      "select the point you wish to insert after again with the left    ",$
      "mouse button. After you have finished use the right mouse button ",$
      "again to stop. Once you have finished adding points press the    ",$
      "right mouse button once more to exit this mode[N.B.remember you  ",$
      "need to use the right mouse button to stop adding and also to    ",$
      "exit, thus if you add 1 point the right mouse button would be    ",$
      "used twice.                                                      ",$
      "                                                                 ",$
      "Additional notes: The 'refresh' button returns the original plot.",$
      "The 'help' button displays these notes. The 'print' button       ",$
      "produces a postscript file in your current working directory.    ",$
      "                                                                 ",$
      "Version 1.1                                                      ",$
      "                                                                 ",$
      "Author: David H.Brooks                                           ",$
      "        Univ.of Strathclyde    Ext.4213/4205                     ",$
      "        email: dbrooks@phys.strath.ac.uk                         ",$
      "                                                                 ",$
      "Date: 25.04.95                                                   ",$
      "                                                                 ",$
      "Modified: David H.Brooks      23-06-95                           ",$
      "          Added routine 'pull_knot' for ADAS108 Burgess Option   ",$
      "          which only allows vertical movement of the points. This",$
      "          is otherwise the same as the 'Plot stretching' Mode.   ",$
      "Modified: Hugh Summers        09-06-99                           ",$
      "          Made the 'help' widget modal."]
    w = widget_text(row1,value = text,/frame,/scroll,			  $
                    xsize = 65,ysize = 30,font=font)
    done = widget_button(row2,value = ' done ',font=font)
 
    widget_control, /realize, base
    xmanager,'help_pt', base, event_handler ='help_pt_event',/modal 

END



