; Copyright (c) 1995 Strathclyde University .
; SCCS info: Module @(#)$Header: /home/adascvs/idl/adas1xx/adaslib/add_any_pt.pro,v 1.2 2004/07/06 11:15:15 whitefor Exp $ Date $Date: 2004/07/06 11:15:15 $
;+
; PROJECT:
;       ADAS IBM MVS to DEC UNIX conversion & dveelopment
;
; NAME:
;       ADD_ANY_PT
;
; PURPOSE:
;       To add a point anywhere on an editable graph.
;
; EXPLANATION:
;       The user selects a point to add after and then selects the
;       position that they want the new point to be placed. It is an
;       extension of the procedure add_pt.pro whcih allowsthe x values
;       to NOT be monotonically increasing. Although physically unrealistic
;       it has been added for completeness.
;
; USE:
;       ADAS Graph Editor
;
; INPUTS:
;       X      - Float Array; Contains the x-coordinates of the plot.
;
;       Y      - Float Array; Contains the y-coordinates of the plot..
;
;       NSIZE  - Integer; The size of the array x
;
;       NSP    - Integer; the number of ADAS branch spline points.
;
;       XMIN   - Float; The minimum value of the x-coordinates, chosen 
;                       by the user.
;
;       XMAX   - Float; The maximum value of the x-coordinates, chosen 
;                       by the user.
;
;       YMIN   - Float; The minimum value of the y-coordinates, chosen 
;                       by the user.
;
;       YMAX   - Float; The maximum value of the y-coordinates, chosen 
;                       by the user.
;
; OPTIONAL INPUTS:
;       None
;
; OUTPUTS:
;       X      - Float Array; The edited values of the x-coordinates.
;
;       Y      - Float array; The edited values of the y-coordinates.
;
;       LPEND  - Integer; Determines whether any editing has been done,
;                         and whether to re-process.
;
; OPTIONAL OUTPUTS:
;       None
;
; KEYWORD PARAMETERS:
;       None
; CALLS:
;
; SIDE EFFECTS:
;       None
;
; CATEGORY: ADAS System
;
; WRITTEN:  DAVID H.BROOKS, Univ.of Strathclyde, EXT.4213/4205
;
; MODIFIED:
;	1.1	DAVID H.BROOKS
;		First Release	
;       1.2     William Osborn
;               Changed to normal coordinates for cursor positioning -
;               horizontal and vertical lines of points were not being
;               picked correctly.
; VERSION: 
;	1.1	02-07-95
;       1.2     03-10-96
;-
;-------------------------------------------------------------------------

pro add_any_pt, x, y, nsize, nsp, xmax, xmin, ymax, ymin, lpend

             ;**** Define mouse selection accuracy   ****
             ;**** and convert to normal coordinates ****

    targetsqu = 0.0001
    points = convert_coord(x,y,/to_normal)
    xn = points(0,*)
    yn = points(1,*)

    plot,x,y,xrange=[xmin,xmax],yrange=[ymin,ymax],/xstyle,$
    title='left button selects point to add after',psym=4
    oplot,x,y,linestyle = 2

                ;**** SPLINE OPTION ****;

    if nsp ne -1 then begin
        fact = nsp/(max(x)+min(x))
        t = findgen(nsp)/fact+min(x)
        z = spline(x,y,t)
        oplot,t,z
    endif
                ;**** Enable Cursor and read ****
                ;**** first location         ****

    tvcrs,1                           
again: cursor, a, b, /down, /normal

    case !err of
        1:begin
            dist = (xn-a)^2+(yn-b)^2

            minim = min(dist,w1)

            if minim le targetsqu then begin
                ptx = w1
                repeat begin

                ;**** add point at new coordinate ****

                    cursor, c, d, /down          
                    case !err of
                        1:begin
                            lpend = 2
                            xt = fltarr(nsize+1)
                            yt = xt
                            for i = 0,nsize-1 do begin
                                xt(i) = x(i)
                                yt(i) = y(i)
                            endfor
                            for i = 0,nsize-1 do begin
                                if i gt ptx then begin     
                                    xt(i+1) = x(i)
                                    yt(i+1) = y(i)
                                endif
                            endfor
                            xt(ptx+1) = c              
                            yt(ptx+1) = d
                            x = xt & y = yt
                            points = convert_coord(x,y,/to_normal)
                            xn=points(0,*)
                            yn=points(1,*)
                            plot, x, y, xrange=[xmin,xmax],		$
                              yrange=[ymin,ymax],		$
                              /xstyle, psym=4
                            oplot,x,y,linestyle = 2
                            if nsp ne -1 then begin
                                fact = nsp/(max(x)-min(x))
                                t = findgen(nsp+1)/fact+min(x)
                                z = spline(x,y,t)
                                oplot,t,z
                            endif
                            nsize = nsize+1
                            ptx = ptx+1   
                        end
                        4:begin
                        end
                    endcase
                endrep until !mouse.button eq 4
            endif    
            goto, again
        end
        else:goto,done
        endcase

done:

END    
    

