; Copyright (c) 1995 Strathclyde University .
; SCCS info: Module @(#)$Header: /home/adascvs/idl/adas1xx/adaslib/ref_pt.pro,v 1.3 2004/07/06 14:50:12 whitefor Exp $ Date $Date: 2004/07/06 14:50:12 $
;+
; PROJECT:
;       ADAS IBM MVS to DEC UNIX conversion & development
;
; NAME:
;       REF_PT
;
; PURPOSE:
;       To refresh the original plot before editing.
;
; EXPLANATION:
;
; USE:  
;       ADAS Graph Editor 
;
; INPUTS:
;       X      - Float Array; Contains the original x-coordinates.
;
;       Y      - Float Array; Contains the original y-coordinates.
;
;       XCOPY  - Float Array; Contains the x-coordinates of the current
;                plot.
;
;       YCOPY  - Float Array; Contains the y-coordinates of the current
;                plot.
;
;       NSIZE  - Integer; The size of the array y
;
;       NSP    - Integer; the number of ADAS branch spline points
;
;       XMIN   - Float; The minimum value of the x-coordinates
;
;       XMAX   - Float; The maximum value of the x-coordinates
;
;       YMIN   - Float; The minimum value of the y-coordinates
;
;       YMAX   - Float; The maximum value of the y-coordinates
;
;	RES    - Structure res105; resonance structure, see adas_edit_graph.pro
;
;	XTITLE - String; title for x-axis
;
;	YTITLE - String; title for y-axis
;
;	TITLE  - String; title for graph
;
; OPTIONAL INPUTS:
;
; OUTPUTS:
;       X      - Float Array; The refreshed values of the x-coordinates
;       Y      - Float Array; The refreshed values of the y-coordinates
;       B      - Float Array; The refreshed values of the knot points
; OPTIONAL OUTPUTS:
;
; KEYWORD PARAMETERS:
;
; CALLS:
;
; SIDE EFFECTS:
;       None
;
; CATEGORY:
;       ADAS System
;
; WRITTEN: 
;       DAVID H.BROOKS, Univ.of Strathclyde, EXT.4213/4205
; MODIFIED:
;	1.1	DAVID H.BROOKS
;		First Release
;	1.2	William Osborn
;		Added RES, XTITLE, YTITLE and TITLE parameters
;	1.3	William Osborn
;               S.C.C.S. mistake
; VERSION: 
;	1.1	23-06-95
;	1.2	01-10-96
;	1.3	03-10-96
;-
;------------------------------------------------------------
pro ref_pt, x, y, xcopy, ycopy, nsize, nsp, xmax, xmin, ymax, ymin, res, $
		xtitle, ytitle, title

    nx = n_elements(x)
    ny = n_elements(y)
    if nx eq ny then begin
        plot, x, y, xrange = [xmin, xmax], yrange=[ymin,ymax],		$
              /xstyle, psym = 4, title = title+' : Original points', xtitle=xtitle,$
		ytitle=ytitle
        oplot, x, y
        if nsp ne -1 then begin
            fact = nsp/(max(x)-min(x))
            t = findgen(nsp+1)/fact + min(x)
            z = spline(x,y,t)
            oplot, t, z
        endif
		     ;**** Plot resonances ****
	for i=0,res.nithr-1 do begin
	  plots,[res.xv(i),res.xv(i)],[0,0.4], linestyle=0
	endfor
	for i=res.nithr,res.nrthr+res.nithr-1 do begin
	  plots,[res.xv(i),res.xv(i)],[0,0.4], linestyle=2
	endfor
    endif else begin
        print,'STOPPED : !**< dimension mismatch >**!'
    endelse
    xcopy = x
    ycopy = y
    nsize = nx

END
 

