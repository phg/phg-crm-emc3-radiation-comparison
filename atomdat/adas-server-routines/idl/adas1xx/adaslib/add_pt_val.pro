; Copyright (c) 1995 Strathclyde University .
; SCCS info: Module @(#)$Header: /home/adascvs/idl/adas1xx/adaslib/add_pt_val.pro,v 1.2 2004/07/06 11:15:27 whitefor Exp $ Date $Date: 2004/07/06 11:15:27 $
;+
; PROJECT:
;       ADAS IBM MVS to DEC UNIX conversion & dveelopment
;
; NAME:
;       ADD_PT_VAL
;
; PURPOSE:
;       To add an exact point between x-values on an editable graph.
;
; EXPLANATION:
;       The user first specifies the exact x and y values in the editable
;       text boxes. Then they select the position at which they wish to 
;       add the new point.
;       The x-values are assumed to be monotonically increasing so the
;       point will always be added between two x-coordinates.
;
; USE:
;       ADAS Graph Editor
;
; INPUTS:
;       X      - Float Array; Contains the x-coordinates of the plot.
;
;       Y      - Float Array; Contains the y-coordinates of the plot..
;
;       NSIZE  - Integer; The size of the array x
;
;       NSP    - Integer; the number of ADAS branch spline points
;
;       XMIN   - Float; The minimum value of the x-coordinates, chosen
;                       by the user.
;
;       XMAX   - Float; The maximum value of the x-coordinates, chosen
;                       by the user.
;
;       YMIN   - Float; The minimum value of the y-coordinates, chosen
;                       by the user.
;
;       YMAX   - Float; The maximum value of the y-coordinates, chosen
;                       by the user.
;
;       XVAL   - Float; The x-value to be added at the chosen point.
;
;       YVAL   - Float; The y-value to be added at the chosen point.
;
; OPTIONAL INPUTS:
;       None
;
; OUTPUTS:
;       X      - Float Array; The edited values of the x-coordinates.
;
;       Y      - Float array; The edited values of the y-coordinates.
;
; OPTIONAL OUTPUTS:
;       None
;
; KEYWORD PARAMETERS:
;       None
; CALLS:
;
; SIDE EFFECTS:
;       None
;
; CATEGORY: ADAS System
;
; WRITTEN:  DAVID H.BROOKS, Univ.of Strathclyde, EXT.4213/4205
;
; MODIFIED:
;	1.1	DAVID H.BROOKS
;		First Release
;	1.2	Tim Hammond (Tessella Support Services plc)
;		Corrected minor syntax error
; VERSION: 
;	1.1	02-07-95
;	1.2	17-07-95
;-
;-------------------------------------------------------------------------

pro add_pt_val, x, y, nsize, xvalt, yvalt, nsp, xmax, xmin, ymax, ymin

    if nsp ne -1 then begin
        fact = nsp/(max(x) - min(x))
        t = findgen(nsp)/fact + min(x)
        z = spline(x, y, t)
    endif
    xt = fltarr(nsize+1)
    yt = xt
    xval = 0.                     
    yval = 0.
    ptx = 0
               ;**** Convert string text to integer ****

    reads, xvalt, xval             
    reads, yvalt, yval
 
    if xval ne x(nsize-1) then begin
        for i = 0, nsize-2 do begin
            if xval ne x(i) then begin
                if xval gt x(nsize-1) then begin
                    ptx = nsize
                endif else if xval lt x(0) then begin
                    ptx = 0
                endif else if (xval gt x(i) and xval lt x(i+1)) then begin
                    ptx = i+1
                endif
            endif else begin
                print,'ADAS101 ERROR: xval matches an x element-new x not increasing'
                goto, done  
            endelse
        endfor
    endif else begin
        print,'ADAS101 ERROR: xval matches an x element-new x not increasing'
        goto, done
    endelse 
    for i = 0, nsize-1 do begin
        xt(i) = x(i)
        yt(i) = y(i)
    endfor
    for i = 0, nsize-1 do begin
        if i ge ptx then begin
            xt(i+1) = x(i)
            yt(i+1) = y(i)
        endif
    endfor
    xt(ptx) = xval
    yt(ptx) = yval
    x = xt & y = yt
    plot, x, y, xrange = [xmin,xmax], yrange=[ymin,ymax], /xstyle, psym=4
    oplot, x, y, linestyle = 2
    if nsp ne -1 then begin
        fact = nsp/(max(x)-min(x))
        t = findgen(nsp+1)/fact + min(x)
        z = spline(x,y,t)
        oplot, t, z
    endif
    nsize = nsize+1

done:

END
