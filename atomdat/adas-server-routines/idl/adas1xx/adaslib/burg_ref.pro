; Copyright (c) 1995 Strathclyde University .
; SCCS info: Module @(#)$Header: /home/adascvs/idl/adas1xx/adaslib/burg_ref.pro,v 1.1 2004/07/06 11:43:19 whitefor Exp $ Date $Date: 2004/07/06 11:43:19 $
;+
; PROJECT:
;       ADAS IBM MVS to DEC UNIX conversion & development
;
; NAME:
;       BURG_REF
;
; PURPOSE:
;       To refresh the original plot with the original five knots
;       overlayed.
;
; EXPLANATION:
;
; USE:  ADAS101 Burgess branch specific
;
; INPUTS:
;       X      - Float Array; Contains the original x-coordinates.
;
;       Y      - Float Array; Contains the original y-coordinates.
;
;       B      - Float array; contains the original five knots.
;
;       XCOPY  - Float Array; Contains the x-coordinates of the current
;                plot.
;
;       YCOPY  - Float Array; Contains the y-coordinates of the current
;                plot.
;
;       BCOPY  - Float array; contains the five knots of the current
;                plot.
;
;       K      - Float array; The x-coordiantes of the knot points
;                             [0.0,0.25,0.5,0.75,1.0]
;       NSIZE  - Integer; The size of the array y
;
;       NSP    - Integer; the number of ADAS branch spline points
;
;       XMIN   - Float; The minimum value of the x-coordinates
;
;       XMAX   - Float; The maximum value of the x-coordinates
;
;       YMIN   - Float; The minimum value of the y-coordinates
;
;       YMAX   - Float; The maximum value of the y-coordinates
;
; OPTIONAL INPUTS:
;
; OUTPUTS:
;       X      - Float Array; The refreshed values of the x-coordinates
;       Y      - Float Array; The refreshed values of the y-coordinates
;       B      - Float Array; The refreshed values of the knot points
; OPTIONAL OUTPUTS:
;
; KEYWORD PARAMETERS:
;
; CALLS:
;
; SIDE EFFECTS:
;       None
;
; CATEGORY: ADAS System
;
; WRITTEN:  DAVID H.BROOKS, Univ.of Strathclyde, EXT.4213/4205
;
; MODIFIED:
;	1.1	David H. Brooks
;		First release
; VERSION: 
;	1.1	23-06-95
;-
;--------------------------------------------------------------------
pro burg_ref, x, y, b, xcopy, ycopy, bcopy, 				$
              k, nsize, nsp, xmax, xmin, ymax, ymin

    nx = n_elements(x)
    ny = n_elements(y)
    ns = 70
    z = fltarr(ns+1)
    if nx eq ny then begin
        plot, k, bcopy, xrange = [xmin, xmax], yrange=[ymin, ymax],	$
              /xstyle, psym = 7, title = 'Original x-y plot'
        for i = 0,4 do begin
            plots, [0.25*i,0.25*i], [!y.crange]
        endfor
        fact =  ns/(max(k) - min(k))
        t = findgen(ns+1)/fact + min(k)
        for i = 0, ns do begin
            z(i) = sp5(t(i),bcopy)
        endfor
        oplot, t, z 
        oplot, x, y, psym=4   
        oplot, x, y, linestyle=2
    endif else begin
        print,'STOPPED : !**< dimension mismatch >**!'
    endelse
    xcopy = x
    ycopy = y
    nsize = nx
    bcopy = b

END
 

