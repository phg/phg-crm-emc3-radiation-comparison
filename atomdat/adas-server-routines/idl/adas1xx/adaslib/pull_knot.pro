; Copyright (c) 1995 Strathclyde University .
; SCCS info: Module @(#)$Header: /home/adascvs/idl/adas1xx/adaslib/pull_knot.pro,v 1.2 2004/07/06 14:37:59 whitefor Exp $ Date $Date: 2004/07/06 14:37:59 $
;+
; PROJECT:
;       ADAS IBM MVS to DEC UNIX conversion & development
;
; NAME:
;       PULL_KNOT
;
; PURPOSE:
;       To move Burgess knots in Vertical direction.
;
; EXPLANATION:
;       Allows the user to select a point and move it in the y
;       direction whilst continuosly updating the Burgess spline.
;
; USE:  ADAS101 Burgess branch specific
;
; INPUTS:
;       X      - Float Array; Contains the x-coordinates of the plot.
;
;       Y      - Float Array; Contains the y-coordinates of the plot.
;                i.e the five knots.
;
;       BGX    - Float array; The original data of the x-coordinates
;
;       BGGY   - Float arrat; The original data of the y-coordinates
;
;       NSIZE  - Integer; The size of the array y
;
;       NSP    - Integer; the number of ADAS branch spline points
;
;       LPEND  - Integer; 0 - Done, 1 - Cancel
;
;       XMIN   - Float; The minimum value of the x-coordinates
;
;       XMAX   - Float; The maximum value of the x-coordinates
;
;       YMIN   - Float; The minimum value of the y-coordinates
;
;       YMAX   - Float; The maximum value of the y-coordinates
;
;       FONT   - String; the name of a font
;
; OPTIONAL INPUTS:
;
; OUTPUTS:
;       Y      - Float Array; The new values of the y-coordinates
;
; OPTIONAL OUTPUTS:
;
; KEYWORD PARAMETERS:
;
; CALLS:
;
; SIDE EFFECTS:
;       None
;
; CATEGORY: ADAS System
;
; WRITTEN:  DAVID H.BROOKS, Univ.of Strathclyde, EXT.4213/4205 
;
; MODIFIED: 
;	1.1	DAVID H.BROOKS
;		First Release
;       1.2     William Osborn
;               Changed to normal coordinates for cursor positioning -
;               horizontal and vertical lines of points were not being
;               picked correctly.
; VERSION: 
;	1.1	23-04-95
;       1.2     03-10-96
;-
;-----------------------------------------------------------------------------
pro pull_knot, x, y, bgx, bgy, nsize, nsp, 				$
               xmax, xmin, ymax, ymin, lpend, font

             ;**** Define mouse selection accuracy   ****
             ;**** and convert to normal coordinates ****

    targetsqu = 0.0001
    points = convert_coord(x,y,/to_normal)
    xn = points(0,*)
    yn = points(1,*)

    ns = 70                    ; optional spline setting
    z = fltarr(ns+1)
    lines = [ymin,ymax]
    plot, x, y, xrange=[xmin,xmax], yrange=[ymin,ymax], /xstyle,	$
          title='left button selects point to move', psym=7
    for i = 0,4 do begin
        plots,[0.25*i,0.25*i],[!y.crange]
    endfor

               ;**** spline option ****;

    fact =  ns/(max( x)-min( x))
    t = findgen(ns+1)/fact+min(x)
    for i = 0, ns do begin
        z(i) = sp5(t(i),y)
    endfor
    oplot, t, z
    oplot, bgx, bgy, psym=4
    oplot, bgx, bgy, linestyle = 2

               ;**** enable cursor and read ****
               ;**** first location         ****

    tvcrs,1                                
again: cursor, a, b, /down, /normal

    case !err of
        1:begin

            dist = (xn-a)^2+(yn-b)^2

            minim = min(dist,w1)

            if minim le targetsqu then begin
                while !mouse.button eq 1 do begin
                    cursor, c, d, /nowait        
                    
               ;**** move point to new coordinate ****

                    y(w1) = d
                    points=convert_coord(0.0,y(w1),/to_normal)
                    yn(w1)=points(1)

               ;**** constantly display cursor position ****

                    gapx = abs(!x.crange(1)-!x.crange(0))/5.
                    gapy = abs(!y.crange(1)-!y.crange(0))/30.
                    plot, x, y ,xrange=[xmin,xmax],yrange=[ymin,ymax],$
                      /xstyle,psym=7
                    xyouts,[!x.crange(0)+gapx,!x.crange(0)+3*gapx],	$
                      [!y.crange(0)+gapy,!y.crange(0)+gapy],	$
                      [c,d]
                    for i = 0,4 do begin
                        plots,[0.25*i,0.25*i],[!y.crange]
                    endfor
                    if  nsp ne -1 then begin
                        fact =  nsp/(max( x)-min( x))
                        t = findgen( nsp+1)/fact+min( x)
                        z = spline( x, y,t)

               ;****  update spline continuously ****

                        oplot,t,z
                    endif
                    t = findgen(ns+1)/fact+min(x)
                    for i = 0, ns do begin
                        z(i) = sp5(t(i),y)
                    endfor
                    oplot, t, z
                    oplot, bgx, bgy, psym = 4
                    oplot, bgx, bgy, linestyle = 2
                endwhile
            endif    
            goto, again
        end

    else: goto, done

    endcase

done:

END    
    

