; Copyright (c) 1995 Strathclyde University .
; SCCS info: Module @(#)$Header: /home/adascvs/idl/adas1xx/adaslib/pull_pt.pro,v 1.2 2004/07/06 14:38:07 whitefor Exp $ Date $Date: 2004/07/06 14:38:07 $
;+
; PROJECT:
;       ADAS IBM MVS to DEC UNIX conversion & development
;
; NAME:
;       PULL_PT
;
; PURPOSE:
;       To move points around an editable graph.
;
; EXPLANATION:
;       Allows the user to select a point and move it in any
;       direction whilst continuously updating the spline.
;
; USE:  ADAS Graph Editor
;
; INPUTS:
;       X      - Float Array; Contains the x-coordinates of the plot.
;
;       Y      - Float Array; Contains the y-coordinates of the plot.
;
;       NSIZE  - Integer; The size of the array x
;
;       NSP    - Integer; the number of ADAS branch spline points
;
;       XMIN   - Float; The minimum value of the x-coordinates
;
;       XMAX   - Float; The maximum value of the x-coordinates
;
;       YMIN   - Float; The minimum value of the y-coordinates
;
;       YMAX   - Float; The maximum value of the y-coordinates
;
;
; OPTIONAL INPUTS:
;
; OUTPUTS:
;       X      - Float Array; The new values of the x-coordinates
;
;       Y      - Float Array; The new values of the y-coordinates
;
;       LPEND  - Integer; Flag for 'Done' & 'Cancel', also determines
;                whether any alterations have been made.
; OPTIONAL OUTPUTS:
;
; KEYWORD PARAMETERS:
;
; CALLS:
;
; SIDE EFFECTS:
;       None
;
; CATEGORY: ADAS System
;
; WRITTEN:  DAVID H.BROOKS, Univ.of Strathclyde, EXT.4213/4205 
;
; MODIFIED: 
;	1.1	David H. Brooks
;		First release
;       1.2     William Osborn
;               Changed to normal coordinates for cursor positioning -
;               horizontal and vertical lines of points were not being
;               picked correctly.
; VERSION: 
;	1.1	23-04-95
;       1.2     03-10-96
;-
;-----------------------------------------------------------------------------
pro pull_pt, x, y, nsize, nsp, xmax, xmin, ymax, ymin, lpend

             ;**** Define mouse selection accuracy   ****
             ;**** and convert to normal coordinates ****

    targetsqu = 0.0001
    points = convert_coord(x,y,/to_normal)
    xn = points(0,*)
    yn = points(1,*)

    plot, x, y, xrange=[xmin,xmax], yrange=[ymin,ymax], /xstyle,	$
          title='left button selects point to move', psym=4
    oplot, x, y, linestyle = 2

             ;**** SPLINE OPTION ****;

    fact =  nsp/(max(x) - min(x))
    if  nsp ne -1 then begin
        t = findgen(nsp+1)/fact + min(x)
        z = spline(x, y, t)
        oplot, t, z
    end

             ;**** enable cursor & read first ****
             ;**** cursor location            ****

    tvcrs, 1
again: cursor,a, b, /down, /normal
    case !err of
        1:begin

            dist = (xn-a)^2+(yn-b)^2

            minim = min(dist,w1)

            if minim le targetsqu then begin
                while !mouse.button eq 1 do begin
                    cursor, c, d, /nowait       
 
              ;**** move point to new coordinate ****

                    x(w1) = c
                    y(w1) = d
                    points = convert_coord(x(w1),y(w1),/to_normal)
                    xn(w1)=points(0)
                    yn(w1)=points(1)

              ;**** constantly display cursor position ****

                    gapx = abs(!x.crange(1)-!x.crange(0))/5.
                    gapy = abs(!y.crange(1)-!y.crange(0))/30.
                    plot, x, y, xrange=[xmin,xmax], yrange=[ymin,ymax],$
                      /xstyle, psym=4
                    xyouts, [!x.crange(0)+gapx,!x.crange(0)+3*gapx],$
                      [!y.crange(0)+gapy,!y.crange(0)+gapy],	$
                      [c,d]
                    oplot, x, y, linestyle = 2
                    if  nsp ne -1 then begin
                        fact =  nsp/(max( x)-min( x))
                        t = findgen( nsp+1)/fact+min( x)
                        z = spline( x, y, t)
                        
              ;**** update spline continuously ****

                        oplot,t,z
                    endif
                endwhile
                lpend = 2
            endif    
            goto, again
        end

    else: goto, done

    endcase

done:

END    
    

