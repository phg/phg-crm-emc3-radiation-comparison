; SCCS info: Module @(#)$Header: /home/adascvs/idl/adas1xx/adas108/a8spf1.pro,v 1.1 2004/07/06 10:04:14 whitefor Exp $ Date $Date: 2004/07/06 10:04:14 $
;+
; PROJECT:
;       ADAS IBM MVS to UNIX conversion & Development
;
; NAME:
;	A8SPF1
;
; PURPOSE:
;	IDL user interface and communications with ADAS108 FORTRAN
;	process via pipe.
;
; EXPLANATION:
;       The output options interface is invoked and the results of the
;	interactions are written back to the FORTRAN via the pipe.
;	Communications are with the FORTRAN subroutine A8SPF1.
;
; USE:
;	The use of this routine is specific to ADAS108, See adas108.pro.
;
; INPUTS:
;	PIPE	- The IDL unit number of the bi-directional pipe to the
;		  ADAS108 FORTRAN process.
;
;	DSFULL  - The name of the data set being analyzed.
;
;	VALUE	- A structure which determines the initial settings of
;		  the dataset selection widget.  The initial value is
;		  set in adas108.pro.  VALUE is passed un-modified
;		  through to cw_adas108_out.pro, see that routine for a
;                 full description.
;
;	HEADER  - Header information used for text output.
;
;       INVAL   - A structure containing the selections on the Archive
;                 interface widget, see a8spf0.pro.
; 
;       PROCVAL - A structure containing the selections on the 
;                 processing interface widget, see a8ispf.pro.
;
;       BITFILE - String; the path to the directory containing bitmaps
;                 for the 'escape to series menu' button.
;
;
; OPTIONAL INPUTS:
;	None.
;
; OUTPUTS:
;	LPEND	- Integer; indicates user action on leaving output
;		  selections.  lpend is set to 1 if the user exited
;		  with the 'Cancel' button and 0 if the user exited
;		  with 'Done'.
;
;	VALUE	- On output the structure records the final settings of
;		  the output selection widget if the user pressed the
;		  'Done' button otherwise it is not changed from input.
;
;       GOMENU  - Int; flag - set to 1 if user has selected 'escape direct
;                 to series menu , otherwise 0
;
; OPTIONAL OUTPUTS:
;	None.
;
; KEYWORD PARAMETERS:
;	DEVLIST	- A string array;  Each element of the array is the
;		  name of a hardcopy graphics output device.  This
;		  list must mirror the DEVCODE list.  DEVLIST is the
;		  list which the user will see.
;		  e.g ['Post-Script','HP-PCL','HP-GL']
;
;	FONT	- Supplies the font to be used for the interface widgets.
;
; CALLS:
;	ADAS108_OUT	User interface - output options.
;
; SIDE EFFECTS:
;       None
;
; CATEGORY:
;	Adas system.
;
; WRITTEN:
;       Hugh Summers, Univ.of Strathclyde, 23-May-1999
;
; MODIFIED:
;	Version 1.1	Hugh Summers
;			First release
;
; VERSION:
;       Version 1.1     23-May-1999   
;-
;-----------------------------------------------------------------------------

PRO a8spf1, pipe, lpend, inval, procval, value, dsfull, header, bitfile,$
	    gomenu, DEVLIST=devlist, FONT=font


                ;**** Set defaults for keywords ****

  IF NOT (KEYWORD_SET(font)) THEN font = ''

                ;**** re-set lpend ****
   lpend = 0

                ;**** Set analysis option off always, to avoid ****
                ;**** confusion between option bases & and to  ****
                ;****          allow error trapping            ****

  value.anopt = -1

		;**************************************
		;**** Pop-up output options widget ****
		;**************************************

  adas108_out, value, dsfull, action, inval.rootpath, procval.type, 	$
                bitfile, DEVLIST=devlist, FONT=font

		;********************************************
		;**** Act on the output from the widget   ****
		;**** There are only two possible actions ****
		;**** 'Done' and 'Cancel'.                ****
		;*********************************************
  gomenu=0
  if action eq 'Done' then begin
    lpend = 0
  end else if action eq 'Menu' then begin
    lpend = 1	;lpend set to 1 since checking on variables has not been done.
		;Otherwise, format errors occur in pipe comms below
    gomenu = 1
  endif else begin
    lpend = 1
  end

		;*******************************
		;**** Write data to fortran ****
		;*******************************

  printf, pipe, format = '(1x,i3)', lpend
  if lpend eq 0 then begin

     printf, pipe, format = '(1x,i3)', value.anopt
     if (value.anopt eq 1) then begin
         printf, pipe, format = '(1x,i3)',  value.adopt
         printf, pipe, format = '(1x,i3)',  value.bdptopt
         printf, pipe, format = '(1x,i3)',  value.axop
         printf, pipe, format = '(1x,i3)',  value.adif
     end else begin
         printf, pipe, format = '(1x,e8.2)',  value.bbval
         printf, pipe, format = '(1x,e8.2)',  value.bcval
         printf, pipe, format = '(1x,i3)',    value.bxval
         printf, pipe, format = '(1x,e8.2)',  value.byval
     end
     printf, pipe, format = '(1x,i3)', value.grpout
     if (value.grpout eq 1) then begin
         printf, pipe, format = '(1x,1a40)', strtrim(value.gtit1,2)
         printf, pipe, format = '(1x,i3)',  value.grpscal1
	 if (value.grpscal1 eq 1) then begin
	     printf, pipe, format = '(1x,e8.2)',  value.xmin1
	     printf, pipe, format = '(1x,e8.2)',  value.xmax1
	     printf, pipe, format = '(1x,e8.2)',  value.ymin1
	     printf, pipe, format = '(1x,e8.2)',  value.ymax1
        endif
	printf, pipe, format = '(1x,i3)',  value.grpscal2
	if (value.grpscal2 eq 1) then begin
	     printf, pipe, format = '(1x,e8.2)',  value.xmin2
	     printf, pipe, format = '(1x,e8.2)',  value.xmax2
	     printf, pipe, format = '(1x,e8.2)',  value.ymin2
	     printf, pipe, format = '(1x,e8.2)',  value.ymax2
        endif
     endif


		;*** If text output requested tell FORTRAN ****
     
     printf, pipe, format = '(1x,i3)',  value.texout
     if (value.texout eq 1) then begin
         deftext = '  '+value.texdsn
	 printf, pipe, deftext
     endif
  endif


		;**************************************************
		;**** Set messages and settings ready for when ****
		;**** output window is re-displayed.           ****
		;**************************************************

  if lpend eq 0 then begin
     if value.texout eq 1 then begin
		;**** If text output is requested enable append ****
		;**** for next time and update the default to   ****
		;**** the current text file.                    ****
         value.texapp=0
         value.texdef = deftext     
         if value.texrep ge 0 then value.texrep = 0
;        value.texmes = 'Output written to file.'
	 value.texmes = ' '
     end
  end

		;*************************************************
		;**** If text output has been requested write ****
		;**** header to pipe for XXADAS to read.      ****
		;*************************************************

  if lpend eq 0 and value.texout eq 1 then begin
    printf, pipe, format = '(1x,1a80)', header
  end
END
