; SCCS info: Module @(#)$Header: /home/adascvs/idl/adas1xx/adas108/adas108_plot.pro,v 1.1 2004/07/06 10:10:17 whitefor Exp $ Date $Date: 2004/07/06 10:10:17 $
;+
; PROJECT:
;       ADAS IBM MVS to DEC UNIX conversion & development
;
; NAME:
;	ADAS108_PLOT
;
; PURPOSE:
;       To plot the Upsilon graph for the ADAS option & to allow
;       printing of both this and the reduced omega graph.
;
; EXPLANATION:
;       creates a window for the display of the graph and displays
;       output as the user directs through interaction with the
;       widget.
;
; USE:  ADAS108 Specific
;
; INPUTS:
;       XC    - Array; The x-coordinates for the Upsilon graph 
;                      (log10(Te)))
;
;       YC    - Array; The y-coordinates for the Upsilon graph
;                      (Upsilon)
;
;       YAPC  - Array; The y-coordinates for the approximate form
;                      Upsilon graph if required
;
;       DGEX  - Array; The x-coordinates for the Reduced Omega
;                      graph.
;
;       DGEY  - Array; The y-coordinates for the Reduced Omega
;                      graph.
;
;       B     - Array - empty
;
;       LPENDC- Integer; Determines whether 'cancel' or 'done' 
;                        selected.
;
;       PRINT1- Integer; Determines whether Reduced Omega printout
;                        is requested.
;
;       ARCHC - Integer; Determines whether archiving is requested.
;
;       LCHOICEC-Integer; Holds archiving option
;
;       DEVICE - String; The current graphics device.
;
;       DATE   - String; The date
;
;       HEADER - String; The Printout header.
;
;       TITLX  - String; Part of the graph title (The dataset name)
;
;       BITFILE - String; the path to the directory containing bitmaps
;                 for the 'escape to series menu' button.
;
; OPTIONAL INPUTS:
;
; OUTPUTS:
;       VAL   - Structure; Contains the values of the output widget
;                          selected by the user, see a1spf1.pro.
;
;       ACT   - String; The evnt action; 'Cancel' or 'Done'
;
;       GOMENU  - Int; flag - set to 1 if user has selected 'escape direct
;                 to series menu , otherwise 0
;
; OPTIONAL OUTPUTS:
;
; KEYWORD PARAMETERS:
;       WINTITLE - A title for the window
;       FONT 
;       NSP - The number of requested spline points
;
; CALLS:
;       ADAS108_PLOT_EV
;       XMANAGER
;
; SIDE EFFECTS:
;       None
;
; CATEGORY: ADAS System
;
; WRITTEN: Hugh Summers, University of Strathclyde, 12-June-1999
;
; MODIFIED: 
;	1.1	Hugh Summers,  
;           	First release
;
; VERSION:
;	1.1	26-06-99
;-
;-----------------------------------------------------------------------------


PRO adas108_plot_ev, event

  COMMON plot108_blk, action, value, lpend, arch, lchoice, data, plotdev, $
                   plotfile, fileopen, win, print1, print, gomenu
  newplot = 0
;  print = 0
  done = 0
  plottype = ''

		;**** Find the event type and copy to common ****
    action = event.action

    CASE action OF
            
                ;**** 'Print' button ****
        'Print' : begin
               newplot = 1
               print = 1
           end
                ;**** 'Archive' button ****

        'Archive' : begin
                arch = 1
                if lchoice eq 0 then begin
                  buttons = ['Continue']
                  title = 'ADAS108:DEFAULT ACTION CONTINUING'
                  message = $
"Archive selection clashes with 'no archive' flag arch108/archive.dat created"
                  dec = popup(message=message,title=title,$
                              buttons=buttons,font=font)
                end
           end

		;**** 'Done' button ****
	'Done'  : begin

                if fileopen eq 1 then begin
                   set_plot,plotdev
                   device,/close_file
                end
                set_plot,'X'
			;**** Get the output widget value ****
;		widget_control,event.id,get_value=value 
		widget_control,event.top,/destroy
                done = 1
                lpend = 0

	   end


		;**** 'Cancel' button ****
	'Cancel': begin
                  widget_control,event.top,/destroy
                  done = 1
                  lpend = 1
                  set_plot,'X'
           end

		;**** 'Menu' button ****

	'Menu': begin
                if fileopen eq 1 then begin
                   set_plot,plotdev
                   device,/close_file
                end
		widget_control,event.top,/destroy
		done = 1
		lpend = 1
		gomenu = 1
		set_plot,'X'
	   end
    END

  if done eq 0 then begin
;                ;**** Set graphics device ****
;     if print1 eq 1 then begin
;       set_plot,plotdev
;       if fileopen eq 0 then begin
;         fileopen = 1
;         device,filename = plotfile
;         device, /landscape
;       end 
;                ;**** plot first graph if requested ****
;       plot108, data.dgex, data.dgey, data.dgey, data.title1, data.xtitle1, $
;                data.ytitle1, data.xmin1, data.xmax1, data.ymin1, $
;                data.ymax1, data.nsp, data.ldef1, data.b
;     end      
		;**** send upsilon graph if selected to graphic device ****    
		;****                    : connected points only       ****    
     if print eq 1 then begin
       set_plot,plotdev
       if fileopen eq 0 then begin
         fileopen = 1
         device,filename = plotfile
         device, /landscape
       end
       bgraph = replicate(-1.0,9)
       nspgraph = data.nsp
       plottype = 'ufile'
       plot108, data.x, data.y, data.yap, data.title2, data.xtitle2, data.ytitle2, $ 
               data.xmin2, data.xmax2, data.ymin2, data.ymax2, nspgraph, $
               data.ldef2, bgraph , plottype
       print = 0
       set_plot,'X'
       wset,win
     end

  end

END

;-----------------------------------------------------------------------------


PRO adas108_plot, xc, yc, yapc, dgex, dgey, b, lpendc, print1c, archc, lchoicec, $
                  val, act, device, date, header, titlx, infofit,	   $
                  bitfile, gomenu, WINTITLE = wintitle, NSP = nspc, 	   $
                  FONT = font

  COMMON plot108_blk,action,value, lpend, arch, lchoice, data, plotdev, $
                   plotfile, fileopen, win, print1, print, gomenucom

		;**** Copy values to common ****
  value = val
  lpend = lpendc
  arch = archc
  lchoice = lchoicec
  plotdev = device
  plotfile = value.hardname
  fileopen = 0
  print1 = print1c
  print = 0
  gomenucom = gomenu
  plottype=''

		;**** Set defaults for keywords ****
  IF NOT (KEYWORD_SET(wintitle)) THEN wintitle = ''
  IF NOT (KEYWORD_SET(nsp)) THEN NSP = -1
  IF NOT (KEYWORD_SET(font)) THEN font = 'courier-bold14'

		;***************************************
		;**** Pop-up a new widget if needed ****
		;***************************************
  plotval = {WIN:0}

                ;**** create base widget ****
  inid = widget_base(TITLE=wintitle,XOFFSET=200,YOFFSET=0)
		;**** Declare output options widget ****
  cwid = cw_a8plot(inid, bitfile, plotval = plotval, FONT=font)

		;**** Realize the new widget ****
  dynlabel, inid
  widget_control,inid,/realize
                ;**** get the id of the graphics area ****
  widget_control,cwid,get_value = window
  win = window.win
  wset,win
                ;**** Set up titles ****
  title1 = strarr(5)
  title2 = title1
    title1(0) = "COLLISION STRENGTH RATIO GRAPH "
      if (strtrim(strcompress(value.gtit1),2) ne ' ' ) then begin
        title1(0) = title1(0)+': '+strupcase(strtrim(value.gtit1,2))
      end
    title1(1) = 'ADAS    :'+strupcase(header)
    title1(2) = 'FILE     : '+strcompress(titlx)
    title1(3) = 'KEY     : (DIAMONDS - INPUT DATA) '+$
               '(FULL LINE - IDL SPLINE FIT)'
    title1(4) = 'FIT      : '+strcompress(infofit)
    title2(0) = "UPSILON GRAPH "
      if (strtrim(strcompress(value.gtit1),2) ne ' ' ) then begin
         title2(0) = title2(0)+': '+strupcase(strtrim(value.gtit1,2))
      end
    title2(1) = 'ADAS    :'+strupcase(header)
    title2(2) = 'FILE     :'+strcompress(titlx)
    title2(3) = 'KEY     : (DIAMONDS + FULL LINE - OUTPUT DATA) '+$
               '(DASHED LINE - APPROXIMATE FORM)'
    title2(4) = ' '
  xtitle1 = ' 1-2/X (X=Threshold Parameter) '
  ytitle1 = ' Omega/Approx.Omega '
  xtitle2 = ' LOG10 TE(K) '
  ytitle2 = ' UPSILON '
                ;**** Put the graphing data into common ****
  b = replicate(-1.0,9)
  data = { dgex:dgex, dgey:dgey, title1:title1, xtitle1:xtitle1, $
           ytitle1:ytitle1,$
           x:xc , y:yc, yap:yapc, title2:title2, xtitle2:xtitle2, ytitle2:ytitle2, $
           xmin1:value.xmin1, xmax1:value.xmax1, ymin1:value.ymin1, $
           ymax1:value.ymax1, ldef1:value.grpscal1, $
           xmin2:value.xmin2, xmax2:value.xmax2, ymin2:value.ymin2, $
           ymax2:value.ymax2, NSP:nspc, ldef2:value.grpscal2, b:b }
           
		;**** send analysis graph if selected to graphic device ****   
		;****                    : points and analysis spline   ****   
				
     if print1 eq 1 then begin
       set_plot,plotdev
       if fileopen eq 0 then begin
         fileopen = 1
         device,filename = plotfile
         device, /landscape
       end 
       bgraph = data.b
       nspgraph = 70
       plottype = 'afile'
       plot108, data.dgex, data.dgey, data.dgey, data.title1, data.xtitle1, $
                data.ytitle1, data.xmin1, data.xmax1, data.ymin1, $
                data.ymax1, nspgraph, data.ldef1, bgraph, plottype
       print1 = 0
       set_plot,'X'
       wset,win
     end      
  
		;  show upsilon graph on screen : connected points only
		 
  bgraph = replicate(-1.0,9)
  nspgraph = -1
  plottype = 'uscreen'
  plot108, xc, yc, yapc, title2, xtitle2, ytitle2, value.xmin2, value.xmax2, $
           value.ymin2, value.ymax2,  nspgraph, value.grpscal2, bgraph,$
           plottype

		;**** make widget modal ****
  xmanager,'adas108_plot',inid,$
  	    event_handler='adas108_plot_ev',/modal,/just_reg
 
		;**** Return the output value from common ****
  act = action
  val = value
  lpendc = lpend
  archc = arch
  gomenu = gomenucom

END

