; SCCS info: Module @(#)$Header: /home/adascvs/idl/adas1xx/adas108/a8ispf.pro,v 1.1 2004/07/06 10:03:42 whitefor Exp $ Date $Date: 2004/07/06 10:03:42 $
;+
; PROJECT:
;       ADAS IBM MVS to   UNIX conversion
;
; NAME:
;	A8ISPF
;
; PURPOSE:
;	IDL user interface and communications with ADAS108 FORTRAN
;	process via pipe.
;
; EXPLANATION:
;	The routine begins by reading information from the ADAS108
;	FORTRAN process via a UNIX pipe.  Then part of the ADAS108
;	IDL user interface is invoked to determine how the user
;	wishes to process the input dataset.  When the user's
;	interactions are complete the information gathered with
;	the user interface is written back to the FORTRAN process
;	via the pipe.  Communications are to the FORTRAN subroutine
;	A1ISPF.
;
; USE:
;	The use of this routine is specific to ADAS108, see adas108.pro.
;
; INPUTS:
;	PIPE	- The IDL unit number of the bi-directional pipe to the
;		  ADAS108 FORTRAN process.
;
;	LPEND	- Integer, 0 if the user pressed the 'Done' button
;		  or 1 if the user pressed 'Cancel'.  Maps directly onto
;		  the logical variable LPEND in ADAS108 FORTRAN.
;
;	PROCVAL	- A structure which determines the initial settings of
;		  the processing options widget.  The initial value is
;		  set in adas108.pro.  If adas108.pro passes a blank 
;		  dummy structure of the form {NEW:-1} into PROCVAL then
;		  PROCVAL is reset to a default structure.
;
;		  The PROCVAL structure is
;
;     procval = {                                 $
;                new    : 0,                      $
;		 ifout  : ifout_arr,	          $
;		 ldfit  : 0, 			  $
;		 maxt   : 0,			  $
;		 maxx   : 0,			  $
;                maxe   : 0,                      $
;                tin    : temp_arr,        	  $
;                xin    : xsect_arr,       	  $
;                ein    : eng_arr,                $
;                z      : 0,                      $
;                zo     : 0,                      $
;                zeff   : 0,                      $
;                type   : 0,                      $
;                Acoeff : 0.,                     $
;                form   : 'A',                    $
;                lindx  : 0,                      $
;                uindx  : 0,                      $
;                lstwt  : 0,                      $
;                ustwt  : 0,                      $
;                leng   : 0.,                     $
;                ueng   : 0.,                     $
;                engunit: 1                       $
;                }
;
;		  See cw_adas108_proc.pro for a full description of this
;		  structure.
;	
;
;	DSFULL	- String; The full system file name of the input 
;		  dataset selected by the user for processing.
;
;	LCHOICE	- Integer; Variable to flag user choice of archive
;                 option. 
;                 0 = No Archive
;                 1 = Old Archive
;                 2 = Refresh from Old Archive
;                 3 = New Archive
;
;       BITFILE - String; the path to the directory containing bitmaps
;                 for the 'escape to series menu' button.
;
; OPTIONAL INPUTS:
;	None
;
; INPUT/OUTPUT: 
;                 ADAS Analysis options if refreshing from archive
;       IXOPS   - Optimisation 0-off 1-on
;
;       IBPTS   - Bad Point Option 0-off 1-on
;
;       IFPTS   - Optimising 1-One point, 2-Two point
;
;       IDIFF   - Dipole X-section fitting, 0-Ratio 1-Difference
;
; OUTPUTS:
;	PROCVAL	- On output the structure records the final settings of
;		  the processing selections widget if the user pressed the
;		  'Done' button, otherwise it is not changed from input.
;
;       GOMENU  - Int; flag - set to 1 if user has selected 'escape direct
;                 to series menu , otherwise 0
;
; OPTIONAL OUTPUTS:
;	None
;
; KEYWORD PARAMETERS:
;	FONT_LARGE - The name of a larger font.
;
;	FONT_SMALL - The name of a smaller font.
;
;	EDIT_FONTS - A structure of two fonts used in the table editor
;		     adas_edtab.pro, {font_norm:'',font_input:''}.
;
; CALLS:
;	ADAS108_PROC	Invoke the IDL interface for ADAS108 data
;		 	processing options/input.
;
; SIDE EFFECTS:
;	Two way communications with ADAS108 FORTRAN via the
;	bi-directional UNIX pipe.
;
; CATEGORY:
;	Adas system.
;
; WRITTEN:
;       Hugh Summers, Univ.of Strathclyde, 23-May-1999
;
; VERSION:
;	1.1	Hugh Summers					04-06-99
;  
; MODIFIED:
;	1.1	First release
;-----------------------------------------------------------------------------

PRO a8ispf, pipe, lpend, procval, dsfull, lchoice, 			$
            ixops, ibpts, ifpts, idiff, bbval, bcval, iwhc, bitfile,	$ 
            gomenu,							$
	    FONT_LARGE=font_large, FONT_SMALL=font_small, 		$
	    EDIT_FONTS=edit_fonts


                ;**** Set defaults for keywords ****

  IF NOT (KEYWORD_SET(font_large)) THEN font_large = ''
  IF NOT (KEYWORD_SET(font_small)) THEN font_small = ''
  IF NOT (KEYWORD_SET(edit_fonts)) THEN 				$
                                edit_fonts = {font_norm:'',font_input:''}

		;*******************************************
		;**** Set default value if non provided ****
                ;****  Set energy units off always to   ****
                ;****  force a selection                ****
		;*******************************************

  idum = 0
  readf, pipe, idum  
  nedim = idum
  readf, pipe, idum                               
  iwhc = idum
  nxdim = nedim & ntdim = nedim 
  if (procval.new lt 0) then begin
     temp_arr = fltarr(ntdim)
     xsect_arr = fltarr(nxdim)
     eng_arr = fltarr(nedim)
     ifout_arr = intarr(3)

     procval = {                                 $
                new    : 0,                      $
		ifout  : ifout_arr,	         $
		ldfit  : 0, 			 $
		maxt   : 0,			 $
		maxx   : 0,			 $
                maxe   : 0,                      $
                tin    : temp_arr,        	 $
                xin    : xsect_arr,       	 $
                ein    : eng_arr,                $
                z      : 0,                      $
                zo     : 0,                      $
                zeff   : 0,                      $
                type   : 0,                      $
                Acoeff : 0.,                     $
                form   : 'A',                    $
                lindx  : 0,                      $
                uindx  : 0,                      $
                lstwt  : 0,                      $
                ustwt  : 0,                      $
                leng   : 0.,                     $
                ueng   : 0.,                     $
                engunit: 1                       $
               }

                ;**** force units to those of the ****
                ;**** archive if chosen ****

      if lchoice eq 1 or lchoice eq 2 then begin
          procval.ifout(0) = 3
          procval.ifout(1) = 7
          procval.ifout(2) = 10
      endif else begin
          for i = 0,2 do begin
              procval.ifout(i) = 5*i
          endfor  
      endelse

  endif else begin


     procval = { 			         $
                new    : procval.new,            $
		ifout  : procval.ifout,          $
		ldfit  : procval.ldfit,          $
		maxt   : procval.maxt,		 $
		maxx   : procval.maxx,		 $
                maxe   : procval.maxe,           $
                tin    : procval.tin,        	 $
                xin    : procval.xin,       	 $
                ein    : procval.ein,            $
                z      : procval.z,              $
                zo     : procval.zo,             $
                zeff   : procval.zeff,           $
                type   : procval.type,           $
                Acoeff : procval.Acoeff,         $
                form   : procval.form,           $
                lindx  : procval.lindx,          $
                uindx  : procval.uindx,          $
                lstwt  : procval.lstwt,          $
                ustwt  : procval.ustwt,          $
                leng   : procval.leng,           $
                ueng   : procval.ueng,           $
                engunit: 1                       $
              }

                ;**** force units to those of the ****
                ;**** archive if chosen ****

      if lchoice eq 1 or lchoice eq 2 then begin
          procval.ifout(0) = 3
          procval.ifout(1) = 7
          procval.ifout(2) = 10
      endif else begin
          for i = 0,2 do begin
              procval.ifout(i) = 5*i
          endfor  
      endelse

  endelse

  lpend = 0

		;******************************************
		;**** Now can define other array sizes ****
		;******************************************

  eng   = fltarr(nedim,5)
  xsect = fltarr(nxdim,5)
  temp  = fltarr(ntdim,5)

		;**********************************************
		;**** Read data from fortran if refreshing ****
		;**********************************************

  if lchoice eq 2 then begin
    if iwhc eq 1 then begin
      readf, pipe, z
      readf, pipe, zo   
      readf, pipe, zeff 
      readf, pipe, type
      readf, pipe, Acoeff 
      readf, pipe, lindx
      readf, pipe, uindx
      readf, pipe, lstwt
      readf, pipe, ustwt
      readf, pipe, leng
      readf, pipe, ueng
      readf, pipe, maxe
      readf, pipe, maxt                      
      maxx = maxe
      

		;********************************
                ;**** Read data from fortran ****
                ;********************************

      for i = 0,maxe-1 do begin
        readf, pipe, format='(1x,e12.4,1x,e12.4)',engt,xsectt
                     eng(i,0) = engt
                     xsect(i,0) = xsectt
      endfor
      for i = 0,maxt-1 do begin
        readf, pipe, format = '(1x,e12.4)',tempt
                     temp(i,0) = tempt
      endfor
    
      ixops = 0
      ibpts = ixops & ifpts = ixops & idiff = ixops
      readf, pipe, ixops
      readf, pipe, ibpts
      readf, pipe, ifpts
      readf, pipe, idiff
    endif else if iwhc eq 2 then begin
      cform = ''
      readf, pipe, z
      readf, pipe, zo
      readf, pipe, type
      readf, pipe, Acoeff 
      readf, pipe, cform
      procval.form = strtrim(cform,2)
      readf, pipe, lindx
      readf, pipe, uindx  
      readf, pipe, lstwt
      readf, pipe, ustwt
      readf, pipe, leng
      readf, pipe, ueng
      readf, pipe, maxe
      readf, pipe, maxt                      
      maxx = maxe 
      for i = 0,maxe-1 do begin
          readf, pipe, format='(1x,e12.4,1x,e12.4)',engt,xsectt
                       eng(i,0) = engt
                       xsect(i,0) = xsectt
      endfor
      for i = 0,maxt-1 do begin
          readf, pipe, format = '(1x,e12.4)',tempt
                       temp(i,0) = tempt
      endfor
      zeff = z+1
      bbval = 0.0
      bcval = 0.0
      readf, pipe, bbval
      readf, pipe, bcval
    endif
                ;*********************************************
                ;*********************************************
                ;**** Convert standard forms to all units ****
                ;*********************************************

    if procval.engunit eq 0 then begin                                        
           leng = leng/1.09737d5                                              
           ueng = ueng/1.09737d5
    end                                              
 
    eij = ueng-leng
    
      if type eq 1 or type eq 5 then begin
          if procval.form eq 'f' then begin
            Acoeff = 8.03232e9*eij*eij*lstwt*Acoeff/ustwt
            procval.form = 'A'
          endif else if procval.form eq 'S' then begin
            Acoeff = 2.67744e9*eij*eij*eij*Acoeff/ustwt
            procval.form = 'A'
          endif
        s = 3.73491e-10*ustwt*Acoeff/(eij*eij*eij)
        fij = 3.333333d-1*eij*s/lstwt
      endif                                                           
 
    for ict = 0,maxe-1 do begin
      x = eng(ict,0)
      y = xsect(ict,0)
      eng(ict,0) = eij*(x-1.0d0)                     ; energy units
      eng(ict,1) = x*eij
      eng(ict,2) = (eij*(x-1.0d0))/(zo*zo)
      eng(ict,3) = x
      eng(ict,4) = (eij*(x-1.0d0))/(zeff*zeff)
      xsect(ict,0) = y/(lstwt*eij*x)                 ; xsect units
      xsect(ict,1) = y/(ustwt*eij*(x-1.0d0))
      xsect(ict,2) = y
      xsect(ict,3) = y*zeff*zeff
				               	; hugh's units here
      xsect(ict,4) = xsect(ict,2)
    endfor
    z1 = z+1.0d0                                                            
    for itout = 0,maxt-1 do begin
      te = temp(itout,0)                             ; temp units
      temp(itout,1) = te/1.16054d4                 
      temp(itout,2) = te/(z1*z1)
      temp(itout,3) = te/(1.5789d5*eij)
    endfor

		;*******************************************
		;****    Copy values to structure       ****
		;*******************************************
     procval = { 			         $
                new    : procval.new,            $
		ifout  : procval.ifout,          $
		ldfit  : procval.ldfit, 	 $
		maxt   : maxt,	         	 $
		maxx   : maxx,		         $ 
                maxe   : maxe,                   $
                tin    : procval.tin,            $
                xin    : procval.xin,       	 $
                ein    : procval.ein,            $
                z      : z,                      $
                zo     : zo,                     $
                zeff   : zeff,                   $
                type   : type,                   $
                Acoeff : Acoeff,                 $
                form   : procval.form,           $
                lindx  : lindx,                  $
                uindx  : uindx,                  $
                lstwt  : lstwt,                  $
                ustwt  : ustwt,                  $
                leng   : leng,                   $
                ueng   : ueng,                   $
                engunit: procval.engunit         $
              }
endif 

 
		;****************************************
		;**** Pop-up processing input widget ****
		;****************************************

  adas108_proc, procval, dsfull, lchoice,				$
                nedim, nxdim, ntdim,					$ 
                eng, xsect, temp, action, bitfile,			$
		FONT_LARGE=font_large, FONT_SMALL=font_small, 		$
		EDIT_FONTS=edit_fonts

		;********************************************
		;****  Act on the event from the widget  ****
		;**** There are only two possible events ****
		;**** 'Done' and 'Cancel'.               ****
		;********************************************
  gomenu = 0
  if action eq 'Done' then begin
    lpend = 0
  endif else if action eq 'Menu' then begin
    lpend = 0
    gomenu = 1
  endif else begin
    lpend = 1
  end
		;**************************************
		;**** Write edited data to fortran ****
		;**************************************

  printf, pipe, lpend
  printf, pipe, format = '(1x,f6.2)', procval.z
  printf, pipe, format = '(1x,f6.2)', procval.zo
  printf, pipe, format = '(1x,i3)', procval.type
  printf, pipe, format = '(1x,e12.4)', procval.Acoeff
  printf, pipe, format = '(1x,1a1)', procval.form
  printf, pipe, format = '(1x,i5)', procval.lindx
  printf, pipe, format = '(1x,i5)', procval.uindx
  printf, pipe, format = '(1x,f5.1)', procval.lstwt
  printf, pipe, format = '(1x,f5.1)', procval.ustwt
  printf, pipe, format = '(1x,f13.6)', procval.leng
  printf, pipe, format = '(1x,f13.6)', procval.ueng
  printf, pipe, format = '(1x,i3)', procval.maxe
  printf, pipe, format = '(1x,i3)', procval.maxt
 
  for i = 0,procval.maxe-1 do begin
    printf, pipe, format='(1x,e12.4)',			$
                  procval.ein(i)
    printf, pipe, format='(1x,e12.4)',			$
                  procval.xin(i)
  endfor
  for i = 0,procval.maxt-1 do begin
    printf, pipe, format='(1x,e12.4)',			$
                  procval.tin(i)
  endfor
 
  for i = 0,n_elements(procval.ifout)-1 do begin
    unit = procval.ifout(i)-(5*i)
    printf, pipe, format = '(1x,i3)', unit
  endfor
    printf, pipe, format = '(1x,i3)', procval.engunit

END
