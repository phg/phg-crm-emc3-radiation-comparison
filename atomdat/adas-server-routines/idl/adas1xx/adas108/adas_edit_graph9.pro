; SCCS info: Module @(#)$Header: /home/adascvs/idl/adas1xx/adas108/adas_edit_graph9.pro,v 1.1 2004/07/06 11:08:49 whitefor Exp $ Date $Date: 2004/07/06 11:08:49 $
;+
; PROJECT:
;       ADAS IBM MVS to DEC UNIX conversion & development
;
; NAME:
;       ADAS_EDIT_GRAPH9
;
; PURPOSE:
;       To plot two arrays and allow the user to edit the graph.
;	  Specific to adas108
;
; EXPLANATION:
;       Creates a window for the display of the graph and allows the user
;       to alter the plot by adding,deleting and moving points.
;
; USE:  General
;
; INPUTS:
;       X      - Float Array; Contains the x-coordinates of the plot.
;
;       Y      - Float Array; Contains the y-coordinates of the plot.
;
;       LPEND  - Integer; 0 - Done, 1 - Cancel, 2- Re-process
;
;       P1     - Integer; Detects print button option
;
;
; OPTIONAL INPUTS:
;
; OUTPUTS:
;       X      - Float Array; The edited x-coordinates of the plot.
;
;       Y      - Float Array; The edited y-coordinates of the plot.;
; OPTIONAL OUTPUTS:
;
; KEYWORD PARAMETERS:
;       FONT 
;       NSP - The number of requested spline points
;       XMIN- The minimum range value for the x-axis
;       XMAX- The maximum range value for the x-axis
;       YMIN- The minimum range value for the y-axis
;       YMAX- The maximum range value for the y-axis
;	RESONANCES - A structure giving the positions of resonances
;	         {res105, nithr:0, nrthr:0, xv:fltarr(12)}
;		NITHR - number of shell group resonances
;		NRTHR - number of resonance group resonances
;		XV    - position of resonances (1-(1+a)/(xp+a))
;       NO_PRINT - 1 => no print button to be displayed
;                  0 => display print button
;
; CALLS:
;       PULL_PT     - Allows user to interactively move a point 
;                     with the mouse.
;       DEL_PT      - Delets a mouse selected point.
;       ADD_PT      - Adds a mouse selected point between x-values.
;       ADD_ANY_PT  - Adds a mouse selected point after a specifed value.
;       ADD_PT_VAL  - Adds an exact point between x-values
;       REF_PT      - Refreshes the original plot.
;       HELP_PT     - Pops-up some instructions in a help widget.
;
; SIDE EFFECTS:
;       None
;
; CATEGORY: ADAS System
;
; WRITTEN: DAVID H.BROOKS, Univ.of Strathclyde, EXT.4213/4205
;
; MODIFIED: 
;	1.1	Hugh Summers
;		First Release
;		Based on adas_edit_graph  
; VERSION: 
;	1.1	??-05-99

;-
;-----------------------------------------------------------------------------
pro edit_graph_event9,event

  COMMON dge_com,xcopy,ycopy,nsize,xval,yval,nspcopy,$
                 xmaxc,xminc,ymaxc,yminc,lpendc,print1
  COMMON refresh_com,xbak,ybak,base,addxv,addyv, res, xtitle, ytitle, title
  name = strmid(tag_names(event,/structure_name),7,1000)
                   
    case (name) of 

    "BUTTON":begin
                     ;**** Get the event type  ****

       widget_control,event.id,get_value = button

                     ;****Process the events****

         if button eq 'Move a Point' then begin

             widget_control,base,sensitive=0
               pull_pt,xcopy,ycopy,nsize,nspcopy,xmaxc,xminc,$
                       ymaxc,yminc,lpendc
             widget_control,base,sensitive=1
           widget_control,event.id,set_button=0

         end else if button eq 'Delete a Point' then begin

             widget_control,base,sensitive=0
               del_pt,xcopy,ycopy,nsize,nspcopy,xmaxc,xminc,$
                      ymaxc,yminc,lpendc
             widget_control,base,sensitive=1
           widget_control,event.id,set_button=0

         end else if button eq 'Add X-Point' then begin

             widget_control,base,sensitive=0
               add_pt,xcopy,ycopy,nsize,nspcopy,xmaxc,xminc,$
                      ymaxc,yminc,lpendc
             widget_control,base,sensitive=1
           widget_control,event.id,set_button=0

         end else if button eq 'Add Anywhere' then begin

             widget_control,base,sensitive=0
               add_any_pt,xcopy,ycopy,nsize,nspcopy,xmaxc,xminc,$
                          ymaxc,yminc,lpendc
             widget_control,base,sensitive=1
           widget_control,event.id,set_button=0

         end else if button eq 'Click to Insert' then begin

             widget_control,base,sensitive=0
               add_pt_val,xcopy,ycopy,nsize,xval,yval,nspcopy,$
                          xmaxc,xminc,ymaxc,yminc
             widget_control,base,sensitive=1
           widget_control,event.id,set_button=0
           lpendc = 2

         end else if button eq 'Cancel' then begin

           widget_control,/destroy,event.top
           lpendc = 1

         end else if button eq 'Print' then begin
           print1 = 1
         end else if button eq 'Refresh' then begin

           ref_pt,xbak,ybak,xcopy,ycopy,nsize,nspcopy,$
                  xmaxc,xminc,ymaxc,yminc, res, xtitle, ytitle, title

         end else if button eq 'Help' then begin
           help_pt
         end else if button eq 'Done' then begin

           widget_control,/destroy,event.top
           if lpendc ne 2 then lpendc = 0

         end
    end

    "TEXT_CH":begin

       widget_control,event.id,get_value = text
       widget_control,event.id,get_uvalue = uvalue

       if uvalue eq 1 then begin
         xval = text(0)
         widget_control,addyv,/input_focus
       end else if uvalue eq 2 then begin
         yval = text(0)
         widget_control,addxv,/input_focus
       end

    end

    endcase

end
;-----------------------------------------------------------------------------
pro adas_edit_graph9, x, y, lpend, p1, NSP = nsp, XMAX = xmax, $
                     XMIN = xmin, YMAX = ymax, YMIN = ymin, FONT = font,$
		     RESONANCES = res, XTITLE = xtitle, YTITLE = ytitle,$
                     NO_PRINT=no_print, TITLE = title

                     ;**** Set the defaults for the keywords ****
  IF NOT (KEYWORD_SET(font)) THEN font = ''
  IF NOT (KEYWORD_SET(xmax)) THEN xmax = max(x)
;  IF NOT (KEYWORD_SET(xmin)) THEN xmin = 0.
  IF NOT (KEYWORD_SET(xmin)) THEN xmin = -1.
  IF NOT (KEYWORD_SET(ymax)) THEN ymax = max(y)
  IF NOT (KEYWORD_SET(ymin)) THEN ymin = 0.
  IF NOT (KEYWORD_SET(nsp)) THEN nsp = '-1'
  IF NOT (KEYWORD_SET(no_print)) THEN no_print = 0
  IF NOT (KEYWORD_SET(res)) THEN begin
    xvarr = fltarr(12)
    res = {res105, nithr:0, nrthr:0, xv:xvarr}
  endif
  IF NOT (KEYWORD_SET(xtitle)) THEN xtitle = ' '
  IF NOT (KEYWORD_SET(ytitle)) THEN ytitle = ' '
  IF NOT (KEYWORD_SET(title)) THEN title = 'Graph Editor'
  COMMON dge_com,xcopy,ycopy,nsize,xval,yval,nspcopy,$
                 xmaxc,xminc,ymaxc,yminc,lpendc,print1
  COMMON refresh_com,xbak,ybak,base,addxv,addyv, rescom, xtitlecom, ytitlecom, titlecom
                     ;**** copy all necessary variables to common block ****
    nsize = n_elements(x)
    nspcopy = nsp
    xmaxc = xmax
    xminc = xmin
    ymaxc = ymax
    yminc = ymin
    lpendc = lpend
    xbak = x
    ybak = y
    xcopy = x
    ycopy = y
    print1 = p1
    rescom = res
    xtitlecom = xtitle
    ytitlecom = ytitle
    titlecom = title
                     ;**** Create the widget base ****
    base=widget_base(title='ADAS: Graph Editor',XOFFSET=200,$
                     YOFFSET=0,/row)
    row1 = widget_base(base,/row)
    col1 = widget_base(row1,/column)
    row2 = widget_base(col1,/row)
    row3 = widget_base(col1,/row)
    row4 = widget_base(col1,/row)
    col2 = widget_base(row3,/column)
    row5 = widget_base(col2,/row,/exclusive,/frame)
    row6 = widget_base(col2,/row,/frame)
      col62 = widget_base(row6,/column)  
      col63 = widget_base(row6,/column,/exclusive)
      row62a = widget_base(col62,/row)
      row62b = widget_base(col62,/row)
      row7 = widget_base(row4,/row)

                     ;**** Define the plotting area ****
        draw = widget_draw(row2,xsize=600,ysize=400)

                     ;**** Add buttons at bottom of window ****
        move = widget_button(row5,value = 'Move a Point',$
                             font=font,/no_release)
        delete  = widget_button(row5,value = 'Delete a Point',$
                                font=font,/no_release)
        addx    = widget_button(row5,value = 'Add X-Point',$
                                font=font,/no_release)
        adda    = widget_button(row5,value = 'Add Anywhere',$
                               font=font,/no_release)
        label1  = widget_label(row62a,value = 'Insert Point by Value: ',$
                               font=font)
        label2  = widget_label(row62a,value = 'X-val: ',font=font)
        addxv   = widget_text(row62a,xsize = 8,/frame,/edit,uvalue = 1)
        label3  = widget_label(row62b,value = '                       ',$
                               font=font)
        label4  = widget_label(row62b,value = 'Y-val: ',font=font)
        addyv   = widget_text(row62b,xsize = 8,/frame,/edit,uvalue = 2)
        click   = widget_button(col63,value = 'Click to Insert',$
                                font=font,/no_release)
        cancel = widget_button(row7,value = 'Cancel',font=font)
        if no_print eq 0 then $
          print  = widget_button(row7,value = 'Print',font=font)
        refresh = widget_button(row7,value = 'Refresh',font=font)
        help = widget_button(row7,value = 'Help',font=font)
        done   = widget_button(row7,value = 'Done',font=font)

                     ;**** Realize the widget ****
        widget_control,/realize,base
        widget_control,get_value = window,draw
        wset = window
    
                     ;**** Set initial plot ****
        plot, x , y , xrange = [xmin,xmax],yrange=[ymin,ymax],/xstyle,$
              title=title, psym =4, xtitle=xtitle, ytitle=ytitle
        oplot,[0.0,0.0],[0,ymax+1],linestyle=1
        if nsp ne -1 then begin
          fact = nsp/(max(x)-min(x))
          t = findgen(nsp+1)/fact+min(x)
; 
         if nsize eq 2 then z=((t-x(1))*y(1)-(t-x(0))*y(0))/(x(0)-x(1))
         if nsize gt 2 then z = spline(x,y,t)
          oplot,t,z
        end

		     ;**** Plot resonances ****
	for i=0,res.nithr-1 do begin
	  plots,[res.xv(i),res.xv(i)],[0,0.4]
	endfor
	for i=res.nithr,res.nithr+res.nrthr-1 do begin
	  plots,[res.xv(i),res.xv(i)],[0,0.4], linestyle=2
	endfor
                     ;**** pass off to event handler ****
      xmanager,'adas_edit_graph9',base,event_handler='edit_graph_event9',$
      /modal,/just_reg
      x = xcopy
      y = ycopy
      lpend = lpendc
      p1 = print1
end
