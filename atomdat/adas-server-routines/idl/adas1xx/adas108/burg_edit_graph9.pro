; SCCS info: Module @(#)$Header: /home/adascvs/idl/adas1xx/adas108/burg_edit_graph9.pro,v 1.1 2004/07/06 11:43:15 whitefor Exp $ Date $Date: 2004/07/06 11:43:15 $
;+
; PROJECT:
;       ADAS IBM MVS to DEC UNIX conversion & development
;
; NAME:
;       BURG_EDIT_GRAPH9
;
; PURPOSE:
;       To plot two arrays and allow the user to edit the graph.
;
; EXPLANATION:
;       Creates a window for the display of the graph and allows the user
;       to alter the plot by adding,deleting and moving points.
;
; USE:  ADAS108 Burgess/Summers branch specific
;
; INPUTS:
;       X      - Float Array; Contains the x-coordinates of the plot.
;
;       Y      - Float Array; Contains the y-coordinates of the plot.
;
;       B      - Float array; Contains the nine knot points of the 
;                Burgess/Summers spline analysis.
;
;       P1     - Integer; Detects print button option
;
;       RMS    - Float; The R.M.S value of the Burgess spline.
;
;	BVALU  - Float; B-value from the processing screen
;
;	CVALU  - Float; C-value from the processing screen
;
;	TYPE   - Integer; Transition type - used to choose minimum C-value
;		 for slider
;
; OPTIONAL INPUTS:
;
; OUTPUTS:
;       X      - Float Array; The edited x-coordinates of the plot.
;
;       Y      - Float Array; The edited y-coordinates of the plot.
;
;       LPEND  - Integer; 0 - Done, 1 - Cancel, 2 - Re-process.
;
;       B      - Float array; The new altered values of the knot points.
;
; OPTIONAL OUTPUTS:
;
; KEYWORD PARAMETERS:
;       FONT
;       NSP - The number of requested spline points
;       XMIN- The minimum range value for the x-axis
;       XMAX- The maximum range value for the x-axis
;       YMIN- The minimum range value for the y-axis
;       YMAX- The maximum range value for the y-axis
;
; CALLS:
;       PULL_KNOT9  - Allows user to interactively move the knots
;                     in the vertical direction only.
;       PULL_PT     - Allows user to interactively move a point
;                     with the mouse.
;       DEL_PT      - Delets a mouse selected point.
;       ADD_PT      - Adds a mouse selected point between x-values.
;       ADD_ANY_PT  - Adds a mouse selected point after a specifed value.
;       ADD_PT_VAL  - Adds an exact point between x-values
;       BURG_REF    - Refreshes the original plot for the Burgess option.
;       HELP_PT     - Pops-up some instructions in a help widget.
;
; SIDE EFFECTS:
;       This routine uses a common block to pass the input variables
;       between the many routines it calls.
;
; CATEGORY: ADAS System
;
; WRITTEN: 
;           Hugh Summers,  Univ.of Strathclyde,   EXT.4196
;
; MODIFIED:
;	1.1	Hugh Summers
;		First Release
; VERSION: 
;	1.1	08-06-99
;-
;-----------------------------------------------------------------------------
pro burg_edit_event9,event

  COMMON bge_com9,xcopy,ycopy,nsize,xval,yval,nspcopy,$
                 xmaxc,xminc,ymaxc,yminc,lpendc,bcopy,k,ksize,$
                 pipe,ploty,plotx,sliderbid,slidercid, bbcopy, bccopy
  COMMON refreshb_com9,xbak,ybak,base,addxv,addyv,bbak,bbbak,bcbak, $
                       print1,font1

  name = strmid(tag_names(event,/structure_name),7,1000)
    case (name) of 
                ;**** Get the event type ****

    "BUTTON":begin

                ;**** Get the button ****
       widget_control,event.id,get_value = button

         if button eq 'Move Knot  ' then begin
                ;**** warn fortran of no slider ****
             printf, pipe, 0
                ;**** Desensitize all the other options ****
             widget_control, base, sensitive = 0
               pull_knot9,k,bcopy,xcopy,ycopy,ksize,nspcopy,$
                          xmaxc,xminc,ymaxc,yminc,font1
                ;**** Resensitize the options and switch off button ****
             widget_control, base, sensitive = 1
           widget_control, event.id, set_button = 0

         end else if button eq 'Move Point ' then begin
                ;**** warn fortran of no slider ****
             printf, pipe, 0

             widget_control, base, sensitive=0
               pull_pt,xcopy,ycopy,nsize,nspcopy,$
                            xmaxc,xminc,ymaxc,yminc,lpendc
             widget_control, base, sensitive=1
           widget_control,event.id,set_button=0

         end else if button eq 'Delete Pt. ' then begin
                ;**** warn fortran of no slider ****
             printf, pipe, 0

             widget_control, base, sensitive=0
               del_pt,xcopy,ycopy,nsize,nspcopy,xmaxc,xminc,$
                      ymaxc,yminc,lpendc
             widget_control, base, sensitive=1
           widget_control,event.id,set_button=0

         end else if button eq 'Add X-Pt.  ' then begin
                ;**** warn fortran of no slider ****
             printf, pipe, 0

             widget_control, base, sensitive=0
               add_pt,xcopy,ycopy,nsize,nspcopy,xmaxc,xminc,$
                      ymaxc,yminc,lpendc
             widget_control,base,  sensitive=1
           widget_control,event.id,set_button=0

         end else if button eq 'Add Any Pt ' then begin
                ;**** warn fortran of no slider ****
             printf, pipe, 0

             widget_control, base, sensitive=0
               add_any_pt,xcopy,ycopy,nsize,nspcopy,xmaxc,xminc,$
                          ymaxc,yminc,lpendc
             widget_control, base, sensitive=1
           widget_control,event.id,set_button=0

         end else if button eq 'Click to Insert' then begin
                ;**** warn fortran of no slider ****
             printf, pipe, 0

             widget_control, base, sensitive=0
               add_pt_val,xcopy,ycopy,nsize,xval,yval,nspcopy,$
                          xmaxc,xminc,ymaxc,yminc
             widget_control, base, sensitive=1
           widget_control,event.id,set_button=0
           lpendc = 2

         end else if button eq 'Cancel' then begin
                ;**** tell fortran to continue ****
           printf, pipe, 2

           widget_control,/destroy,event.top
           lpendc = 1

         end else if button eq 'Print' then begin
                ;**** warn fortran of no slider ****
           printf, pipe, 0

           print1 = 1

         end else if button eq 'Refresh' then begin
                ;**** warn fortran of no slider ****
           printf, pipe, 0

           burg_ref9,xbak,ybak,bbak,xcopy,ycopy,bcopy,k,nsize,nspcopy,$
                     xmaxc,xminc,ymaxc,yminc
                     
           widget_control,sliderbid,set_value=bbbak
           widget_control,slidercid,set_value=bcbak

         end else if button eq 'Help' then begin
                ;**** warn fortran of no slider ****
           printf, pipe, 0

           help_pt

         end else if button eq 'Done' then begin
                ;**** tell fortran to continue ****
           printf, pipe, 2

           widget_control,/destroy,event.top
           if lpendc ne 2 then lpendc = 0

         end
    end
    "TEXT_CH":begin
                  ;**** warn fortran of no slider ****
       printf, pipe, 0

                  ;**** Get the input values **** 
       widget_control,event.id,get_value = text
       widget_control,event.id,get_uvalue = uvalue

       if uvalue eq 1 then begin
         xval = text(0)
         widget_control,addyv,/input_focus
       end else if uvalue eq 2 then begin
         yval = text(0)
         widget_control,addxv,/input_focus
       end

    end
    else:begin

       widget_control, event.id, get_value = bcvalu
       

                  ;**** tell fortran slider used and values ****
       if event.id eq sliderbid then begin
           bvalu = bcvalu
           widget_control, slidercid, get_value = cvalu
       endif else if event.id eq slidercid then begin
           widget_control, sliderbid, get_value = bvalu
           cvalu = bcvalu
       endif
           printf, pipe, 1
           printf, pipe, bvalu
           printf, pipe, cvalu
                  ;**** refresh & update plot variables ****
       ict = 0
       itout = ict
       rms = 0.
       b    = fltarr(9)
       ns   = 70
       z    = fltarr(ns+1)
         readf, pipe, format = '(1x,i3)', ict
         readf, pipe, format = '(1x,i3)', itout
         readf, pipe, format = '(1x,i3)', ixtyp

       dgex = fltarr(ict)
       dgey = dgex
       toa  = fltarr(itout)
       goa  = toa

         readf, pipe, format = '(1x,e12.4)', rms
         for i = 0, ict-1 do begin
           readf, pipe, format = '(1x,e12.4,1x,e12.4)', dgext, dgeyt      
                        dgex(i) = dgext
                        dgey(i) = dgeyt
         endfor
         for i = 0,8 do begin
           readf, pipe, format = '(1x,e12.4)', bt
                        b(i) = bt
         endfor
         for i = 0, itout-1 do begin
           readf, pipe, format = '(1x,e12.4,1x,e12.4)', toat, goat
                        toa(i) = toat
                        goa(i) = goat
         endfor
       ploty = goa
       plotx = alog10(toa)
       xcopy = dgex
       ycopy = dgey
       bcopy = b
       bbcopy = bvalu
       bccopy = cvalu
       
       
                   ;**** build the new plot ****
         plot, k, bcopy, xrange = [xminc,xmaxc], yrange=[yminc,ymaxc], $
               /xstyle, title = 'Graph Editor:  R.M.S=' $
               +strtrim(string(rms,format='(E10.4)'),2), psym = 7 
       for i = -4,4 do begin
         plots, [0.25*i,0.25*i],[!y.crange]
       endfor
       fact = ns/(max(k)-min(k))
       t    = findgen(ns+1)/fact+min(k) 
       for i = 0, ns do begin
         z(i) = sp9(t(i),bcopy)
       endfor 
       oplot, t, z 
       oplot, xcopy, ycopy, psym = 4
       oplot, xcopy, ycopy, linestyle = 2
    end
    endcase
end
;-------------------------------------------------------------------------------------

pro burg_edit_graph9, x, y, b, lpend, p1, rms, pipec, 		$
		      py, px, bvalu, cvalu, type, 		$
                      NSP = nsp, XMAX = xmax, XMIN = xmin, 	$
                      YMAX = ymax, YMIN = ymin, FONT = font
                   ;**** Set the defaults for the keywords ****

  IF NOT (KEYWORD_SET(font)) THEN font = ''
  IF NOT (KEYWORD_SET(xmax)) THEN xmax = 1.0
  IF NOT (KEYWORD_SET(xmin)) THEN xmin = -1.0
  IF NOT (KEYWORD_SET(ymax)) THEN ymax = max(b)
  IF NOT (KEYWORD_SET(ymin)) THEN ymin = 0.
  IF NOT (KEYWORD_SET(nsp)) THEN nsp = '-1'
    COMMON bge_com9,xcopy,ycopy,nsize,xval,yval,nspcopy,$
                    xmaxc,xminc,ymaxc,yminc,lpendc,bcopy,k,ksize,$
                    pipe,ploty,plotx,sliderbid,slidercid,bbcopy, bccopy
    COMMON refreshb_com9,xbak,ybak,base,addxv,addyv,bbak,bbbak,bcbak,  $
                         print1,font1

                ;**** copy all necessary variables to common block ****
   pipe = pipec
   ploty = py
   plotx = px
   nsize = n_elements(x)
   nspcopy = nsp
   xmaxc = xmax
   xminc = xmin
   ymaxc = ymax
   yminc = ymin
   lpendc = lpend
   xbak = x
   ybak = y
   bbak = b
   bbbak=bvalu
   bcbak=cvalu
   xcopy = x
   ycopy = y
   bcopy = b
   bbcopy=bvalu
   bccopy=cvalu
   print1 = p1
   font1 = font
                ;**** create arrays for Burgess knots ****
       k = [-1.0,-0.75,-0.50,-0.25,0.0,0.25,0.5,0.75,1.0]
       ksize = 9
       ns = 70
       z = fltarr(ns+1)

                ;**** create the widget ****
       base=widget_base(title='ADAS: Graph Editor',XOFFSET=200,$
                   YOFFSET=0,/row)
       row1 = widget_base(base,/row)
       col1 = widget_base(row1,/column)
       row2 = widget_base(col1,/row)
       row3 = widget_base(col1,/row)
       row4 = widget_base(col1,/row)
       col2 = widget_base(row3,/column)
       row5s = widget_base(col2,/row,/frame)
       row5 = widget_base(col2,/row,/exclusive,/frame)
       row6 = widget_base(col2,/row,/frame)
        col62 = widget_base(row6,/column)  
        col63 = widget_base(row6,/column,/exclusive)
        row62a = widget_base(col62,/row)
        row62b = widget_base(col62,/row)
        row7 = widget_base(row4,/row)

                ;**** Set up the plotting area ****
        draw = widget_draw(row2,xsize=610,ysize=400)

        bvaltitl = widget_label(row5s, value = ' Adjust B-Value:-', $
                                font = font)
        sliderbid = cw_fslider(row5s, min=slidermin, max=1.0, xsize = 200, $
                          format = '(G6.3)' ,/drag, value = bvalu)
        cvaltitl = widget_label(row5s, value = ' Adjust C-Value:-', $
                                font = font)
        if type gt 1 and type lt 5 then slidermin= 0.01 else slidermin=1.01
        slidercid = cw_fslider(row5s, min=slidermin, max=3.0, xsize = 200, $
                          format = '(G6.3)' ,/drag, value = cvalu)

        knot = widget_button(row5,value = 'Move Knot  ',$
                                font = font,/no_release)
        move = widget_button(row5,value = 'Move Point ',$
                                font=font,/no_release)
        delete  = widget_button(row5,value = 'Delete Pt. ',$
                                font=font,/no_release)
        addx    = widget_button(row5,value = 'Add X-Pt.  ',$
                                font=font,/no_release)
        adda    = widget_button(row5,value = 'Add Any Pt ',$
                                font=font,/no_release)
;
        label1  = widget_label(row62a,value = 'Insert Point by Value: ',$
                                font=font)
        label2  = widget_label(row62a,value = 'X-val: ',font=font)
        addxv  = widget_text(row62a,xsize = 8,/frame,/edit,uvalue = 1)
        label3  = widget_label(row62b,value = '                       ',$
                                font=font)
      
        label4  = widget_label(row62b,value = 'Y-val: ',font=font)
        addyv  = widget_text(row62b,xsize = 8,/frame,/edit,uvalue = 2)
        click   = widget_button(col63,value = 'Click to Insert',$
                                font=font,/no_release)
;
        cancel = widget_button(row7,value = 'Cancel',font=font)
        print  = widget_button(row7,value = 'Print',font=font)
        refresh = widget_button(row7,value = 'Refresh',font=font)
        help = widget_button(row7,value = 'Help',font=font)
        done   = widget_button(row7,value = 'Done',font=font)

                ;**** Realize the widget ****
          widget_control,/realize,base
          widget_control,get_value = window,draw
          wset = window

                ;**** set initial plot ****
        plot, k, bcopy, xrange=[xmin,xmax], yrange=[ymin,ymax], /xstyle,$
          title='Graph Editor:  R.M.S='+strtrim(string(rms,format='(E10.4)'),2), psym = 7
          for i = -4,4 do begin
            plots,[0.25*i,0.25*i],[!y.crange]
          endfor
          fact =  ns/(max( k)-min( k))
           t = findgen(ns+1)/fact+min(k)
           for i = 0,ns do begin
              z(i) = sp9(t(i),bcopy)
           endfor
           oplot, t, z 
           oplot, xcopy, ycopy, psym = 4
           oplot, xcopy, ycopy, linestyle = 2

  xmanager,'burg_edit_graph9',base,event_handler='burg_edit_event9',$
  /modal,/just_reg
  x = xcopy
  y = ycopy
  lpend = lpendc
  p1 = print1
  b = bcopy
  py = ploty
  px = plotx
  bvalu = bbcopy
  cvalu = bccopy
end
