; SCCS info: Module @(#)$Header: /home/adascvs/idl/adas1xx/adas108/sp9.pro,v 1.1 2004/07/06 15:20:49 whitefor Exp $ Date $Date: 2004/07/06 15:20:49 $
;+
; PROJECT:
;       ADAS IBM MVS to DEC UNIX conversion & development
;
; NAME:
;       SP9
;
; PURPOSE:
;       To calculate the Burgess/Summers spline for continuous
;       updating,on the editable graph screen,during interactions.
;
; EXPLANATION:
;       Calculates the Burgess/Summers type spline.
;
; USE:  
;       ADAS108 Burgess/Summers branch.
;
; INPUTS:
;       X      - Real; The point at which to evaluate the spline.
;
;       P      - Float Array; The spline values at the knot points.
;
; OPTIONAL INPUTS:
;
; OUTPUTS:
;       SP9    - Real; The evaluated spline
;
; OPTIONAL OUTPUTS:
;
; KEYWORD PARAMETERS:
;
; CALLS:
;
; SIDE EFFECTS:
;       None
;
; CATEGORY: ADAS System
;
; WRITTEN: 
;         Hugh Summers, Univ.of Strathclyde,  08-June-1999
;  
; MODIFIED:
;	1.1	Hugh Summers
;		First release
; VERSION: 
;	1.1	08-06-99
;-
;-----------------------------------------------------------------------------
function sp9,x,p

 if n_elements(p) eq 9 then begin
 
         nspline = 9
         ishft = (nspline-1)/2
         jshft = ishft
         kshft = ishft+1
         j0    = ishft-1

         s = fltarr(nspline-2)
       
       s(0)=20.2871865*p(0)-46.0103057*p(1)+32.615596*p(2)-		$
            8.73926486*p(3)+2.34146341*p(4)-0.626588801*p(5)+		$
            0.16489179*p(6)-0.0384747509*p(7)+0.00549639299*p(8)
       s(1)=-5.43593267*p(0)+38.0515287*p(1)-67.0779801*p(2)+		$
            43.6963243*p(3)-11.7073171*p(4)+3.13294401*p(5)-		$
            0.824458949*p(6)+0.192373755*p(7)-0.027481965*p(8)
       s(2)=1.45654414*p(0)-10.195809*p(1)+43.6963243*p(2)-		$
            70.0460323*p(3)+44.4878049*p(4)-11.9051872*p(5)+		$
            3.13294401*p(6)-0.731020268*p(7)+0.104431467*p(8)
       s(3)=-0.390243902*p(0)+2.73170732*p(1)-11.7073171*p(2)+		$
            44.4878049*p(3)-70.2439024*p(4)+44.4878049*p(5)-		$
            11.7073171*p(6)+2.73170732*p(7)-0.390243902*p(8)
       s(4)=0.104431467*p(0)-0.731020268*p(1)+3.13294401*p(2)-		$
            11.9051872*p(3)+44.4878049*p(4)-70.0460323*p(5)+		$
            43.6963243*p(6)-10.195809*p(7)+1.45654414*p(8)
       s(5)=-0.027481965*p(0)+0.192373755*p(1)-0.824458949*p(2)+	$
            3.13294401*p(3)-11.7073171*p(4)+43.6963243*p(5)-		$
            67.0779801*p(6)+38.0515287*p(7)-5.43593267*p(8)
       s(6)=0.00549639299*p(0)-0.0384747509*p(1)+0.16489179*p(2)-	$
            0.626588801*p(3)+2.34146341*p(4)-8.73926486*p(5)+		$
            32.615596*p(6)-46.0103057*p(7)+20.2871865*p(8)
;
         jx=fix(x*(j0+1))
         xi=x*(j0+1)-jx
         if xi lt 0.0  then begin
           xi=xi+1.0
           jx=jx-1
         endif
         if jx eq j0+1 then begin
           jx=jx-1
           xi=xi+1.0
         endif
;         
         aa=1.0-xi
         bb=xi
         cc=(aa^3-aa)/96.0
         dd=(bb^3-bb)/96.0
;
         if jx eq -j0-1  then begin
            spl9=aa*p(-j0-2+kshft)+bb*p(-j0+kshft-1)
            spl9=spl9+(cc+dd)*s(jx+jshft)
         endif else if jx eq j0  then begin
               spl9=aa*p(j0+kshft-1)+bb*p(j0+kshft)
               spl9=spl9+(cc+dd)*s(jx+jshft-1)
         endif else begin
            spl9=aa*p(jx+kshft-1)+bb*p(jx+kshft)
            spl9=spl9+cc*s(jx+jshft-1)+dd*s(jx+jshft)
         endelse
  endif else begin
    print,'ERROR: SP9 - P is not a 9 element array'
    spl9=0.0
  endelse

  return,spl9
end
