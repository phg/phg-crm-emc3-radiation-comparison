; SCCS info: Module @(#)$Header: /home/adascvs/idl/adas1xx/adas108/a8outga.pro,v 1.1 2004/07/06 10:03:53 whitefor Exp $ Date $Date: 2004/07/06 10:03:53 $
;+
; PROJECT:
;       ADAS IBM MVS to DEC UNIX conversion & development
;
; NAME:
;       A8OUTGA
;
; PURPOSE:
;       To read omegas and upsilons after fortran processing, popup the 
;       ADAS_GRAPH_EDITOR write any alterations to the fortran and plot the
;       final upsilons.
;
; USE:
;       ADAS108 Specific
;
; INPUTS:
;       PIPE    - The IDL unit number of the bi-directional pipe to the
;                 ADAS108 FORTRAN process.
;
;       LPEND   - Integer, 0 if the user pressed the 'Done' button
;                 or 1 if the user pressed 'Cancel'.  Maps directly onto
;                 the logical variable LPEND in ADAS108 FORTRAN.
;
;       LCHOICE - Integer; Variable to flag user choice of archive
;                 option. 
;                 0 = No Archive
;                 1 = Old Archive
;                 2 = Refresh form Old Archive
;                 3 = New Archive
;
;       OUTVAL -  A Structure which holds the output widget settings see 
;                 a8spf1.pro
;
;       PROCVAL - A Structure which holds the processing widget settings
;                 see a8ispf.pro.
;
;       ROOT -    The path to the user's default archive directory, 
;                 '/home/user/adas/arch108'
;
;       DEVICE -  The name of the current output device.
;
;       DATE -    The date
;
;       HEADER -  The ADAS release header for the final plot.  
;
;       TITLX  -  The datset name, for display on the plot.
;
;       BITFILE - String; the path to the directory containing bitmaps
;                 for the 'escape to series menu' button.
;
; OPTIONAL INPUTS:
;                 None
;
; OUTPUTS:   
;       GOMENU  - Int; flag - set to 1 if user has selected 'escape direct
;                 to series menu , otherwise 0
;
; OPTIONAL OUTPUTS:
;                 None
;
; KEYWORD PARAMETERS:
;       FONT - The name of a font.         
;
; CALLS:
;       ADAS_EDIT_GRAPH9 - The ADAS version of the interactive Graph Editor
;       ADAS108_PLOT - The plotting procedure for the Upsilon graph
;
; SIDE EFFECTS:
;       Two way communications with the Fortran process via the Unix pipe.
;
; CATEGORY:
;       ADAS System
;
; WRITTEN: 
;       Hugh Summers, Univ.of Strathclyde,  10-06-99
;
; MODIFIED:
;	1.1	Hugh Summers
;		First release
;
; VERSION:
;	1.1	10-06-99
;
;-----------------------------------------------------------------------------
pro a8outga, pipe, lpend, lchoice, root, outval, procval, device, date, $
             header , titlx, bitfile, gomenu, FONT = font

                   ;**** re-set lpend ****
  lpend = 0
                   ;********************************
                   ;**** read data from fortran ****                   
                   ;****    then alter to DGE   **** 
                   ;****    required forms  &   ****
                   ;**** set up arrays for DGE  ****  
                   ;********************************

	
  readf, pipe, format = '(1x,i3)', ictn
  readf, pipe, format = '(1x,i3)', itoutn
  readf, pipe, format = '(1x,i3)', ixtyp
	
  indim1 = ictn
  xa = fltarr(indim1)
  ya = xa & apoma = xa 
  indim2 = itoutn
  toa = fltarr(indim2)
  goa = toa
  apgoa = toa
  eij = 0.0
  readf, pipe, format = '(1x,e12.4)', bxc
  readf, pipe, format = '(1x,e12.4)', bpxc
  readf, pipe, format = '(1x,e12.4)', fxc1
  readf, pipe, format = '(1x,e12.4)', fxc2
  readf, pipe, format = '(1x,e12.4)', fxc3
  readf, pipe, format = '(1x,e12.4)', xkc
  readf, pipe, format = '(1x,e12.4)', S
  readf, pipe, format = '(1x,e12.4)', eij

  for i = 0,ictn-1 do begin
    readf, pipe, format = '(1x,e12.4,1x,e12.4,1x,e12.4)', xat, yat, apomat
                 xa(i) = xat
                 ya(i) = yat
                 apoma(i) = apomat
  endfor
  for i = 0,itoutn-1 do begin
    readf, pipe, format = '(1x,e12.4,1x,e12.4,1x,e12.4)', toat, goat, apgoat
                 toa(i) = toat
                 goa(i) = goat
                 apgoa(i) = apgoat
  endfor

  dgey = ya/apoma
;  dgex = 1-1/xa
  dgex = 1-2/xa
  ploty = goa
  plotx = alog10(toa)
  plotyap = apgoa
                   ;********************************
                   ;**** Popup diagnostic graph ****
                   ;****         editor         ****                   
                   ;********************************
  print1 = 0
;  xtit = '1-1/X '
  xtit = '1-2/X '
  ytit = 'OM/Approx.OM '
  tit = 'Collision Strength Ratio Graph'
  if outval.grpscal1 eq 1 then begin
    adas_edit_graph9, dgex,    dgey,    lpend,   print1,  nsp = 70, $
                     xmax = float(outval.xmax1), xmin = float(outval.xmin1), $
                     ymax = float(outval.ymax1), ymin = float(outval.ymin1), $
                     font = font, xtitle=xtit, ytitle=ytit, title=tit
  end else begin
      adas_edit_graph9, dgex,    dgey,    lpend,   print1,  nsp = 70, $
        font = font, xtitle=xtit, ytitle=ytit, title=tit
  end
                   ;**** Re-scale arrays if alterations made ****
                   
    ictn = n_elements(dgey)
    xa = fltarr(ictn)
    ya = fltarr(ictn)
    apoma = ya
;**** test print
;    print,'In a8outga.pro: after adas_edit_graph,ictn,dgex(0),dgey(0)',	$
;          'dgex(ictn-1),dgey(ictn-1)=',dgex(0),dgey(0),dgex(ictn-1),dgey(ictn-1)
    
                   ;********************************
                   ;****  Calculate x-sections  ****
                   ;********************************

    ithrsc = fix((procval.type-1)/4)+1
    iasymc = procval.type-4*(ithrsc-1)
    if iasymc eq 4 then ithrsc = 0
    
;    print,'In a8outga.pro: ithrsc,iasymc=',ithrsc,iasymc 
    
    for i = 0,ictn-1 do begin
;      xa(i) = 1/(1-dgex(i))
      xa(i) = 2/(1-dgex(i))
    endfor
    
    for i = 0,ictn-1 do begin
      if xa(i) gt xkc then begin
        if      iasymc eq 1 then apoma(i) = fxc3*s*alog(xa(i)-xkc+fxc2)	$
        else if iasymc eq 2 then apoma(i) = fxc2+fxc3/(xa(i)-xkc+1.0)	$
        else if iasymc eq 3 then apoma(i) = fxc3/(xa(i)-xkc+fxc2)^2	$
        else if iasymc eq 4 then apoma(i) = 0.0				$
        else apoma(i) = 0.0
      endif
      if xa(i) le xkc then begin
        if      ithrsc eq 0 then apoma(i) = 1.0				$
        else if ithrsc eq 1 then apoma(i) = fxc1*			$
                                 ((xa(i)-1.0)/(xkc-1.0))^bxc*		$
                                 exp(-(bxc-bpxc)*			$
                                 ((xa(i)-xkc)/(xkc-1.0)))		$
        else if ithrsc eq 2 then apoma(i) = fxc1*			$
                                 ((xa(i)-1.0)/(xkc-1.0))^bxc*		$
                                 exp(-(bxc-bpxc)*			$
                                 ((xa(i)-xkc)/(xkc-1.0)))		$		
        else apoma(i) = 1.0
      endif
    endfor    
                   ;**** get new array values ****                 

    for i = 0,ictn-1 do begin
      ya(i) = apoma(i)*dgey(i)
    endfor 

                   ;****print new values to pipe****
                   ;****   for re-analysing     ****    

    printf, pipe, ictn
    printf, pipe, lpend
    
    if lpend eq 2 then begin
      z1=procval.z+1
      zeff=procval.zeff
      zo=procval.zo
      for i = 0,ictn-1 do begin
        xt=xa(i)
        if procval.ifout(0) eq 0 then xa(i)=eij*(xa(i)-1.0)
        if procval.ifout(0) eq 1 then xa(i)=eij*xa(i)
        if procval.ifout(0) eq 2 then xa(i)=eij*(xa(i)-1.0)/(zo*zo)
        if procval.ifout(0) eq 3 then xa(i)=xa(i)
        if procval.ifout(0) eq 4 then xa(i)=eij*(xa(i)-1.0)/(zeff*zeff)
        if procval.ifout(1) eq 5 then ya(i)=ya(i)/			$
       		(procval.lstwt*eij*xt)
        if procval.ifout(1) eq 6 then ya(i)=ya(i)/			$
      		(procval.ustwt*eij*(xt-1.0))
        if procval.ifout(1) eq 7 then ya(i)=ya(i)
        if procval.ifout(1) eq 8 then ya(i)=ya(i)*zeff*zeff
      endfor
      
      for i = 0,ictn-1 do begin
        printf, pipe, format = '(1x,e12.4,1x,e12.4)', 			$
                      xa(i), ya(i)
      endfor
    end

    if lpend eq 1 or lpend eq 2 then goto, LABELEND

                   ;**** re-set lpend ****
    lpend = 0
    arch = 0       ;**** default archive option is off

    infofit = string(ixtyp,bxc,bpxc,fxc1,fxc2,fxc3,xkc,			$
    	      FORMAT='("type = ",i2," b0 = ",f8.5," bp0 = ",f8.5," f1 = ",f8.5," f2 = ",f8.5," f3 = ",f8.5," xk = ",f8.5)')
    adas108_plot, plotx, ploty, plotyap, dgex, dgey, b, lpend, print1, arch, 	$
                  lchoice, outval, action, device, date, header, titlx, $
                  infofit, bitfile, gomenu, 				$
                  wintitle = 'UPSILON GRAPH', nsp = 70, font = font  
    printf, pipe, lpend

                   ;**** Print archiving decision if continuing ****

    if lpend eq 0 then printf, pipe, arch
    
    if lpend eq 1 then goto, LABELEND


                   ;**** special case if archiving selected after ****
                   ;**** picking no archiving initially ****

    if arch eq 1 and lchoice eq 0 then begin
      dsarch = root+'archive.dat'
      printf, pipe,format = '(1a80)', dsarch
    end

LABELEND:

end


 
