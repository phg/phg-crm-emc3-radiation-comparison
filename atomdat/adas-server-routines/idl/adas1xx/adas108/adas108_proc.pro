; SCCS info: Module @(#)$Header: /home/adascvs/idl/adas1xx/adas108/adas108_proc.pro,v 1.1 2004/07/06 10:10:20 whitefor Exp $ Date $Date: 2004/07/06 10:10:20 $
;+
; PROJECT:
;       ADAS IBM MVS to   UNIX conversion
;
; NAME:
;	ADAS108_PROC
;
; PURPOSE:
;	IDL ADAS user interface, processing options/input.
;
; EXPLANATION:
;	This routine creates and manages a pop-up window which allows
;	the user to select options and input data to control ADAS108
;	processing.
;
; USE:
;	This routine is ADAS108 specific, see a8ispf.pro for how it
;	is used.
;
; INPUTS:
;	PROCVAL	- A structure which determines the initial settings of
;		  the processing options widget.  The value is passed
;		  unmodified into cw_adas108_proc.pro.
;
;		  See cw_adas108_proc.pro for a full description of this
;		  structure.
;
;	DSFULL	- String; The full system file name of the input 
;		          dataset selected by the user for processing.
;
;	LCHOICE - Integer: The archiving option, see a1spf0.pro 
;
;	NTDIM	- Integer : Maximum number of temperatures allowed.
;
;	NEDIM	- Integer : Maximum number of energies allowed.
;
;	NXDIM	- Integer : Maximum number of x-sections allowed.
;
;       TEMP    - Float Array; The output temperature set.
;
;       ENG     - Float Array;  The input energy set.
;
;       XSECT   - Float Array;  The input x-section set.
;
;       BITFILE - String; the path to the directory containing bitmaps
;                 for the 'escape to series menu' button.
;
; OPTIONAL INPUTS:
;	None
;
; OUTPUTS:
;	PROCVAL	- On output the structure records the final settings of
;		  the processing selections widget if the user pressed the
;		  'Done' button, otherwise it is not changed from input.
;
;	ACT	- String; Either 'Done', 'Cancel' or 'Menu' for the button the
;		  user pressed to terminate the processing options
;		  window.
;
; OPTIONAL OUTPUTS:
;	None
;
; KEYWORD PARAMETERS:
;	FONT_LARGE - The name of a larger font.
;
;	FONT_SMALL - The name of a smaller font.
;
;	EDIT_FONTS - A structure of two fonts used in the table editor
;		     adas_edtab.pro, {font_norm:'',font_input:''}.
;
; CALLS:
;	CW_ADAS108_PROC	Declares the processing options widget.
;	ADAS108_PROC_EV Called indirectly during widget management,
;			routine included in this file.
;	XMANAGER
;
; SIDE EFFECTS:
;       This widget uses a common block PROC108_BLK, to pass the
;       variables VALUE and ACTION between the two routines. 
;
; CATEGORY:
;	Widgets
;
; WRITTEN:
;       Hugh Summers, University of Strathclyde, 23-May-1999
;
; MODIFIED:
;       1.1	Hugh Summers
;		First Release
;
; VERSION:
;	1.1	23-05-99
;-
;-----------------------------------------------------------------------------


PRO adas108_proc_ev, event


  COMMON proc108_blk,action,value

    action = event.action
    CASE event.action OF

		;**** 'Done' button ****

	'Done'  : begin

		;**** Get the output widget value ****

		      widget_control,event.id,get_value=value

		      widget_control,event.top,/destroy

	           end


		;**** 'Cancel' button ****

	'Cancel': widget_control,event.top,/destroy

		;**** 'Menu' button ****

	'Menu': widget_control,event.top,/destroy

    END

END

;-----------------------------------------------------------------------------


PRO adas108_proc, procval, dsfull, lchoice,				$
                  nedim, nxdim, ntdim,					$
                  eng, xsect, temp, act, bitfile,			$
		  FONT_LARGE=font_large, FONT_SMALL=font_small, 	$
		  EDIT_FONTS=edit_fonts


		;**** declare common variables ****

  COMMON proc108_blk,action,value

		;**** Copy "procval" to common ****

  value = procval

		;**** Set defaults for keywords ****

  IF NOT (KEYWORD_SET(font_large)) THEN font_large = ''
  IF NOT (KEYWORD_SET(font_small)) THEN font_small = ''
  IF NOT (KEYWORD_SET(edit_fonts)) THEN 				$
				edit_fonts = { font_norm:'', font_input:''}
		
                ;**** create base widget ****

  procid = widget_base(TITLE='ADAS108: PROCESSING OPTIONS', 		$
					XOFFSET=200,YOFFSET=0)

		;**** Declare processing widget ****

  cwid = cw_adas108_proc(procid, dsfull,lchoice,			$
                         nedim, nxdim, ntdim,				$
                         eng, xsect, temp, bitfile,			$
			 PROCVAL=value, 				$
		 	 FONT_LARGE=font_large, FONT_SMALL=font_small, 	$
			 EDIT_FONTS=edit_fonts)

		;**** Realize the new widget ****

  dynlabel, procid
  widget_control,procid,/realize

		;**** make widget modal ****

  xmanager,'adas108_proc',procid,event_handler='adas108_proc_ev', 	$
					/modal,/just_reg

		;*** copy value back to procval for return to e1ispf ***

  act = action
  procval = value
 
END

