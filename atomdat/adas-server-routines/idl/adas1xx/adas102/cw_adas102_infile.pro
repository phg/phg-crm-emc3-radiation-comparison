; Copyright (c) 1995 Strathclyde University .
; SCCS info: Module @(#)$Header: /home/adascvs/idl/adas1xx/adas102/cw_adas102_infile.pro,v 1.2 2004/07/06 12:27:39 whitefor Exp $ Date $Date: 2004/07/06 12:27:39 $
;+
; PROJECT:
;       ADAS IBM MVS to DEC UNIX conversion
;
; NAME: CW_ADAS102_INFILE()
;
; PURPOSE:
;	Archive file selection for an input dataset.
;
; EXPLANATION:
;	This function creates a compound widget consisting of the
;	compound widget cw_adas_root, compound widget cw_archive_select,
;	a title and a message.  This widget provides its own error
;	messages.
;
;	This widget generates events as the user interacts with it.
;	The event structure returned is;
;	{ID:0L, TOP:0L, HANDLER:0L, ACTION:''}
;	ACTION has one of two values 'nofile' or 'newfile' indicating
;	the current state of the file selection process.
;
; USE:
;	See routine cw_adas102_in.pro for an example.
;
; INPUTS:
;       PARENT   - Long integer; the ID of the parent widget.
;
; OPTIONAL INPUTS:
;	None.
;
; OUTPUTS:
;       The return value of this function is the ID of the compound
;       widget which is of type LONG integer.
;
; OPTIONAL OUTPUTS:
;       None
;
; KEYWORD PARAMETERS:
;	VALUE	- A structure which determines the initial settings of
;		  widgets cw_adas_root and cw_file_select.
;		  The structure must be;
;		  {ROOTPATH:'', FILE:'', CENTROOT:'', USERROOT:'' }
;		  The elements of the structure are as follows;
;
;		  ROOTPATH - Current data directory e.g '/usr/fred/adas/'
;		  FILE     - Current data file in ROOTPATH e.g 'input.dat'
;		  CENTROOT - Default central data store e.g '/usr/adas/'
;		  USERROOT - Default user data store e.g '/usr/fred/adas/'
;
;		  The archive file selected by the user is obtained by
;		  appending ROOTPATH and FILE.  In the above example
;		  the full name of the data file is;
;		  /usr/fred/adas/input.dat
;
;		  Path names may be supplied with or without the trailing
;		  '/'.  The widget cw_adas_root will add this where
;		  required so that USERROOT will always end in '/' on
;		  output.
;
;		  The default value is;
;		  {ROOTPATH:'./', FILE:'', CENTROOT:'', USERROOT:''}
;		  i.e ROOTPATH is set to the user's current directory.
;
;	TITLE	- The title to be included in this widget, used
;                 to indicate exactly what the required input dataset is,
;                 e.g 'Input COPASE Dataset'
;
;       FONT    - A font to use for all text in this widget.
;
;	UVALUE	- A user value for this widget.
;
; CALLS:
;       CW_LOADSTATE    Recover compound widget state.
;       CW_SAVESTATE    Save compound widget state.
;	CW_ADAS_ROOT	Used to select root adas data directory.
;	CW_ARCHIVE_SELECT Used to select an archive file.
;       See side effects for other related widget management routines.
;
; SIDE EFFECTS:
;	This widget uses a COMMON BLOCK: CW_INFILE_BLK to hold the
;       widget state.
;
;       Three other routines are included which are used to manage the
;       widget;
;
;	INFILE_GET_VAL()
;	INFILE_EVENT()
;
; CATEGORY:
;	Compound Widget
;
; WRITTEN:
;       Conversion of cw_adas102_infile.pro by David H.Brooks, U. of Strathclyde
;
; VERSION:
;       1.1     Hugh P. Summers, University of Strathclyde
;	  1.2	    Richard Martin 
;			Removed obsolete cw_loadstate/savestate statements.
; MODIFIED:
;	1.1	15-11-96
;	1.2	29-01-03
;-
;-----------------------------------------------------------------------------


FUNCTION infile102_get_val, id


		;**** Return to caller on error ****
  ON_ERROR, 2

		;**** Retrieve the state ****

  first_child = widget_info(id,/child)
  widget_control, first_child, get_uvalue=state, /no_copy

		;**** Get latest root and file name ****

  widget_control, state.fileid, get_value=flesval

		;**** Update state ****

  state.inarchval.file = flesval.file
  state.inarchval.rootpath = flesval.root

  inarchval=state.inarchval
  widget_control, first_child, set_uvalue=state, /no_copy

  RETURN, inarchval

END

;-----------------------------------------------------------------------------

FUNCTION infile102_event, event


		;**** Base ID of compound widget ****

  base=event.handler

		;**** Retrieve the state ****

  first_child = widget_info(base,/child)
  widget_control, first_child, get_uvalue=state, /no_copy

		;**** Clear any existing error message ****

  widget_control, state.messid, set_value=' '

		;************************
		;**** Process Events ****
		;************************

  CASE event.id OF

    state.rootid: begin

	  if event.action eq 'newroot' then begin

		;**** Reset the value of the file selection widget ****

	      widget_control, state.rootid, get_value=rootval
	      flesval = { ROOT:rootval.rootpath, FILE:'',		$
                          ARCH1:'Old Archive', ARCH2:'New Archive',	$
                          ARCH3:'No Archive' ,REFRESH:''}
	      widget_control, state.fileid, set_value=flesval

		;**** Update the state ****

	      state.inarchval.file = ''
	      state.inarchval.rootpath = rootval.rootpath

		;**** Resensitise file selection ****

	      widget_control,state.fileid,/sensitive

	  endif else if event.action eq 'rootedit' then begin

		;**** When path is being edited desensitise file selection ****

	      widget_control,state.fileid,sensitive=0

	  endif
	  action = 'nofile'
	end

    state.fileid: begin
	  if event.action eq 'nofile' then begin
	      action = event.action
	  endif else begin

		;**** Get latest root and file name ****

	      widget_control, state.fileid, get_value=flesval
              state.inarchval.refresh=flesval.refresh

                ;**** keep buttons on for 'no archive' ****

            if flesval.file ne '' then begin      
  	        filename=flesval.root+flesval.file

		;**** Test for file read access ****

	        file_acc, filename, exist, read, write, execute, filetype
	        if read eq 0 then begin
	            action = 'nofile'
	            widget_control, state.messid, 			$
		    set_value='No read access for file'
	        endif else begin
	            action = 'newfile'
	        endelse
            endif else begin
                filename = flesval.root+''   ; no archive
                action = 'newfile'
            endelse
	  endelse
	end

    ELSE:

  ENDCASE

		;**** Save the new state structure ****

  widget_control, first_child, set_uvalue=state, /no_copy  
  
  RETURN, {ID:base, TOP:event.top, HANDLER:0L, ACTION:action}

END

;-----------------------------------------------------------------------------

FUNCTION cw_adas102_infile, parent, VALUE=value, TITLE=title, 		$
                            FONT=font, UVALUE=uvalue



  IF (N_PARAMS() LT 1) THEN MESSAGE, 'Must specify PARENT for cw_adas_root'

		;**** Set defaults for keywords ****

  IF NOT (KEYWORD_SET(value)) THEN BEGIN
      inarchval = {inarchval, ROOTPATH:'./', 				$
			      FILE:'', 					$
			      CENTROOT:'', 				$
			      USERROOT:'',				$
                              REFRESH:'' }
  END ELSE BEGIN
      inarchval = {inarchval, ROOTPATH:value.rootpath, 			$
			      FILE:value.file, 				$
			      CENTROOT:value.centroot, 			$
			      USERROOT:value.userroot,			$
                              REFRESH:value.refresh }
      if strtrim(inarchval.rootpath) eq '' then begin
          inarchval.rootpath = './'
      endif else if strmid(inarchval.rootpath,strlen(inarchval.rootpath)-1,1) $
							     ne '/' then begin
          inarchval.rootpath = inarchval.rootpath+'/'
      endif
      if strmid(inarchval.file,0,1) eq '/' then begin
          inarchval.file = strmid(inarchval.file,1,strlen(inarchval.file)-1)
      endif
  END

  IF NOT (KEYWORD_SET(title)) THEN title = ''
  IF NOT (KEYWORD_SET(font)) THEN font = ''
  IF NOT (KEYWORD_SET(uvalue))  THEN uvalue = 0

		;**** Create the main base for the widget ****

  cwid = WIDGET_BASE(parent, UVALUE = uvalue, 				$
		     EVENT_FUNC = "infile102_event", 			$
		     FUNC_GET_VALUE = "infile102_get_val", 		$
		     /COLUMN)

		;**** Create base to hold the value of state ****

  first_child = widget_base(cwid)

		;**** Title for input file ****

  rc = widget_label(first_child, value=title, font=font)

		;**** Root path name widget ****

  rootval = {	ROOTPATH:inarchval.rootpath, 				$
		CENTROOT:inarchval.centroot, 				$
		USERROOT:inarchval.userroot }
  rootid = cw_adas_root(cwid, value=rootval, font=font, nocentral=1)

	 	;**** File selection widget ****

  flesval = {	ROOT:inarchval.rootpath, FILE:inarchval.file,		$
                ARCH1:'Old Archive', ARCH2:'New Archive',		$
                ARCH3:'No Archive', REFRESH:inarchval.refresh }
  fileid = cw_archive_select(cwid, value=flesval, font=font)

		;**** Message ****

  messid = widget_label(cwid, value=' ', font=font)

		;**** Test for file read access ****

  filename = inarchval.rootpath+inarchval.file
  if inarchval.file ne '' then begin
      file_acc, filename, exist, read, write, execute, filetype
      if filetype eq '-' and read eq 0 then begin
          widget_control,messid,set_value='No read access for file'
      endif
  endif
  
		;**** Create state structure ****

  new_state = { ROOTID:rootid, FILEID:FILEID, MESSID:messid, 		$
		inarchval:inarchval, FONT:font }

		;**** Save initial state structure ****

  widget_control, first_child, set_uvalue=new_state, /no_copy

  RETURN, cwid

END
