; Copyright (c) 1995 Strathclyde University .
; SCCS info: Module @(#)$Header: /home/adascvs/idl/adas1xx/adas102/a2outgb.pro,v 1.1 2004/07/06 10:01:10 whitefor Exp $ Date $Date: 2004/07/06 10:01:10 $
;+
; PROJECT:
;       ADAS IBM MVS to DEC UNIX conversion & development
;
; NAME:
;       A2OUTGA
;
; PURPOSE:
;       To read upsilons after fortran processing, popup the 
;       ADAS_GRAPH_EDITOR write any alterations to the fortran and plot the
;       final upsilons.
;
; USE:
;       ADAS102 Specific
;
; INPUTS:
;       PIPE    - The IDL unit number of the bi-directional pipe to the
;                 ADAS102 FORTRAN process.
;
;       LPEND   - Integer, 0 if the user pressed the 'Done' button
;                 or 1 if the user pressed 'Cancel'.  Maps directly onto
;                 the logical variable LPEND in ADAS102 FORTRAN.
;
;       LCHOICE - Integer; Variable to flag user choice of archive
;                 option. 
;                 0 = No Archive
;                 1 = Old Archive
;                 2 = Refresh form Old Archive
;                 3 = New Archive
;
;       OUTVAL -  A Structure which holds the output widget settings see 
;                 a2spf1.pro
;
;       PROCVAL - A Structure which holds the processing widget settings
;                 see a2ispf.pro.
;
;       ROOT -    The path to the user's default archive directory, 
;                 '/home/user/adas/arch102'
;
;       DEVICE -  The name of the current output device.
;
;       DATE -    The date
;
;       HEADER -  The ADAS release header for the final plot.  
;
;       TITLX  -  The datset name, for display on the plot.
;
;       BITFILE - String; the path to the directory containing bitmaps
;                 for the 'escape to series menu' button.
;
; OPTIONAL INPUTS:
;                 None
;
; OUTPUTS:   
;       GOMENU  - Int; flag - set to 1 if user has selected 'escape direct
;                 to series menu , otherwise 0
;
; OPTIONAL OUTPUTS:
;                 None
;
; KEYWORD PARAMETERS:
;       FONT - The name of a font.         
; CALLS:
;       BURG_EDIT_GRAPH - The ADAS version of the interactive Graph Editor
;       ADAS102_PLOT - The plotting procedure for the Upsilon graph
;
; SIDE EFFECTS:
;       Two way communications with the Fortran process via the Unix pipe.
;
; CATEGORY:
;       ADAS System
;
; WRITTEN: 
;       Conversion of a1outgb.pro by David H.Brooks, Univ.of Strathclyde
;
; MODIFIED:
;	1.1	David H. Brooks
; VERSION:
;	1.1	15-11-96
;-
;-----------------------------------------------------------------------------

pro a2outgb, pipe, lpend, lchoice, root, outval, procval, device, date, $
             header, titlx, bitfile, gomenu, FONT = font

                   ;**** re-set lpend ****
   lpend = 0
                   ;********************************
                   ;**** read data from fortran ****                   
                   ;****    then alter to DGE   **** 
                   ;****    required forms  &   ****
                   ;**** set up arrays for DGE  ****  
                   ;********************************

   readf, pipe, format = '(1x,i3)', ict
   readf, pipe, format = '(1x,i3)', itout
   readf, pipe, format = '(1x,e12.4)', rms
   dgex = fltarr(ict)
   dgey = dgex
   toa = fltarr(itout)
   goa = toa
   for i = 0,ict-1 do begin
    readf, pipe, format = '(1x,e12.4,1x,e12.4)', dgext, dgeyt
                 dgex(i) = dgext
                 dgey(i) = dgeyt
   endfor
   b = fltarr(5)
   for i = 0,4 do begin
     readf, pipe, format = '(1x,e12.4)', bt
                  b(i) = bt
   endfor
   for i = 0,itout-1 do begin
     readf, pipe, format = '(1x,e12.4,1x,e12.4)', toat, goat
                   toa(i) = toat
                   goa(i) = goat
   endfor
   ploty = goa
   plotx = alog10(toa)

                   ;********************************
                   ;**** Popup diagnostic graph ****
                   ;****         editor         ****                   
                   ;********************************

    print1 = 0
    if outval.grpscal1 eq 1 then begin
      burg_edit_graph, dgex, dgey, b, lpend, print1, rms, pipe, ploty, plotx, $
        	      outval.bcval, procval.type, 			      $
		      xmax = float(outval.xmax1), xmin = float(outval.xmin1), $
                      ymax = float(outval.ymax1), ymin = float(outval.ymin1), $
                      font = font
    end else begin
      burg_edit_graph, dgex, dgey, b, lpend, print1, rms, pipe, ploty, plotx, $
                       outval.bcval, procval.type, font = font
    end

                   ;****print new values to pipe****
                   ;****   for re-analysing     ****    

    ictn = n_elements(dgey)
    printf, pipe, ictn
    printf, pipe, lpend

                   ;**** Re-scale arrays if alterations made ****

    if lpend eq 2 then begin
      for i = 0,ictn-1 do begin
        printf, pipe, format = '(1x,e12.4,1x,e12.4)', 			$
                      dgex(i), dgey(i)
      endfor
    end

    if lpend eq 1 or lpend eq 2 then goto, LABELEND

                   ;**** re-set lpend ****

    lpend = 0
    arch = 0       ;**** default archive option is off
 
    adas_burg_plot, plotx, ploty,  dgex, dgey, b, lpend, print1, arch, 	$
                    lchoice, outval, action, device, date, header, titlx, $
                    rms, bitfile, gomenu, wintitle = 'UPSILON GRAPH', 	$
		    nsp = 70, font = font  
    printf, pipe, lpend

                   ;**** Print archiving decision if continuing ****

    if lpend eq 0 then printf, pipe, arch
     
    if lpend eq 1 then goto, LABELEND


                   ;**** special case if archiving selected after ****
                   ;**** picking no archiving initially ****

    if arch eq 1 and lchoice eq 0 then begin
      dsarch = root+'archive.dat'
      printf, pipe,format = '(1a80)', dsarch
    end

LABELEND:

end


 
