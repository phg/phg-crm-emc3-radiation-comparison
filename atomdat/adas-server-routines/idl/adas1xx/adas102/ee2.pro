; Copyright (c) 1999, Strathclyde University
; SCCS info: Module @(#)$Header: /home/adascvs/idl/adas1xx/adas102/ee2.pro,v 1.1 2004/07/06 13:48:00 whitefor Exp $ Date $Date: 2004/07/06 13:48:00 $
;+
; PROJECT:
;       ADAS IBM MVS to UNIX conversion
;
; NAME:
;       ee2
;
; PURPOSE:
;       Evaluates exp(x)E2(x) where E2 is the second exponential integral
;
; USE:
;       General
;
; INPUTS:
;       x	-	The independent variable of the function.
; OPTIONAL INPUTS:
;       None
;
; OUTPUTS:
;       ee2	-	The evaluated function.
;
; OPTIONAL OUTPUTS:
;       None
;
; KEYWORD PARAMETERS:
;       None
;
; CALLS:
;       None
;
; SIDE EFFECTS:
;       None
;
; CATEGORY:
;       Adas system.
;
; WRITTEN:
;       Hugh Summers, University of Strathclyde, 19/05/99
;
; MODIFIED:
;	1.1	Hugh Summers
;		First release

; VERSION:
;	1.1	19/05/99
;
;-----------------------------------------------------------------------------

FUNCTION ee2, x

       if (x le 30.0D0)then begin
;                                                       
          ee2=1.0D0-x*eei(x)
;                                               
       endif else begin
;                                                                 
          x1=1.0D0/x                                                       
          ee2=x1*(1.0D0-x1*(2.0D0-x1*(6.0D0-x1*(24.0D0-x1*(120.0D0-x1*	$     
              (720.0D0-x1*5040.0D0))))))
;                                        
       endelse                                                           
;
      
    return, ee2

END
