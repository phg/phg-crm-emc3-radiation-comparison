; Copyright (c) 1995 Strathclyde University .
; SCCS info: Module @(#)$Header: /home/adascvs/idl/adas1xx/adas102/a2outga.pro,v 1.2 2004/07/06 10:01:04 whitefor Exp $ Date $Date: 2004/07/06 10:01:04 $
;+
; PROJECT:
;       ADAS IBM MVS to DEC UNIX conversion & development
;
; NAME:
;       A2OUTGA
;
; PURPOSE:
;       To read omegas and upsilons after fortran processing, popup the 
;       ADAS_GRAPH_EDITOR write any alterations to the fortran and plot the
;       final upsilons.
;
; USE:
;       ADAS102 Specific
;
; INPUTS:
;       PIPE    - The IDL unit number of the bi-directional pipe to the
;                 ADAS102 FORTRAN process.
;
;       LPEND   - Integer, 0 if the user pressed the 'Done' button
;                 or 1 if the user pressed 'Cancel'.  Maps directly onto
;                 the logical variable LPEND in ADAS102 FORTRAN.
;
;       LCHOICE - Integer; Variable to flag user choice of archive
;                 option. 
;                 0 = No Archive
;                 1 = Old Archive
;                 2 = Refresh form Old Archive
;                 3 = New Archive
;
;       OUTVAL -  A Structure which holds the output widget settings see 
;                 a2spf1.pro
;
;       PROCVAL - A Structure which holds the processing widget settings
;                 see a2ispf.pro.
;
;       ROOT -    The path to the user's default archive directory, 
;                 '/home/user/adas/arch102'
;
;       DEVICE -  The name of the current output device.
;
;       DATE -    The date
;
;       HEADER -  The ADAS release header for the final plot.  
;
;       TITLX  -  The datset name, for display on the plot.
;
;       BITFILE - String; the path to the directory containing bitmaps
;                 for the 'escape to series menu' button.
;
; OPTIONAL INPUTS:
;                 None
;
; OUTPUTS:   
;       GOMENU  - Int; flag - set to 1 if user has selected 'escape direct
;                 to series menu , otherwise 0
;
; OPTIONAL OUTPUTS:
;                 None
;
; KEYWORD PARAMETERS:
;       FONT - The name of a font.         
;
; CALLS:
;       ADAS_EDIT_GRAPH - The ADAS version of the interactive Graph Editor
;       ADAS102_PLOT - The plotting procedure for the Upsilon graph
;
; SIDE EFFECTS:
;       Two way communications with the Fortran process via the Unix pipe.
;
; CATEGORY:
;       ADAS System
;
; WRITTEN: 
;       conversion of a1outga.pro by David H.Brooks, Univ.of Strathclyde
;
; MODIFIED:
;	1.1	Hugh P. Summers
;	1.2	Hugh P. Summers
; VERSION:
;	1.1	15-11-96
;	1.2	21-05-99
;-
;-----------------------------------------------------------------------------
pro a2outga, pipe, lpend, lchoice, root, outval, procval, device, date, $
             header , titlx, bitfile, gomenu, FONT = font

                   ;**** re-set lpend ****
  lpend = 0
                   ;********************************
                   ;**** read data from fortran ****                   
                   ;****    then alter to DGE   **** 
                   ;****    required forms  &   ****
                   ;**** set up arrays for DGE  ****  
                   ;********************************

  readf, pipe, format = '(1x,i3)', ictn
  readf, pipe, format = '(1x,i3)', itoutn
  indim1 = ictn
  xa = fltarr(indim1)
  ya = xa & apoma = xa 
  indim2 = itoutn
  toa = fltarr(indim2)
  goa = toa
  eij = 0.0
  readf, pipe, format = '(1x,e12.4)', fxc2
  readf, pipe, format = '(1x,e12.4)', fxc3
  readf, pipe, format = '(1x,e12.4)', S
  readf, pipe, format = '(1x,e12.4)', eij
  for i = 0,ictn-1 do begin
    readf, pipe, format = '(1x,e12.4,1x,e12.4,1x,e12.4)', xat, yat, apomat
                 xa(i) = xat
                 ya(i) = yat
                 apoma(i) = apomat
  endfor
  for i = 0,itoutn-1 do begin
    readf, pipe, format = '(1x,e12.4,1x,e12.4)', toat, goat
                 toa(i) = toat
                 goa(i) = goat
  endfor

  dgey = ya/apoma
  dgex = 1.0/((157890.0*eij/xa)+1.0)
  ploty = goa
  plotx = alog10(toa)
                   ;********************************
                   ;**** Popup diagnostic graph ****
                   ;****         editor         ****                   
                   ;********************************
  print1 = 0
  xtit = '1/(X+1) '
  ytit = 'U/Approx.U '
  tit = 'Upsilon Ratio Graph'
  if outval.grpscal1 eq 1 then begin
    adas_edit_graph, dgex,    dgey,    lpend,   print1,  nsp = 70, $
                     xmax = float(outval.xmax1), xmin = float(outval.xmin1), $
                     ymax = float(outval.ymax1), ymin = float(outval.ymin1), $
                     font = font, xtitle=xtit, ytitle=ytit, title=tit
  end else begin
      adas_edit_graph, dgex,    dgey,    lpend,   print1,  nsp = 70, $
        font = font, xtitle=xtit, ytitle=ytit, title=tit
  end
                   ;**** Re-scale arrays if alterations made ****
    xa = fltarr(n_elements(dgex))
    ya = fltarr(n_elements(dgey))
    apoma = ya
 
                   ;********************************
                   ;****  Calculate x-sections  ****
                   ;********************************

      for i = 0,n_elements(dgex)-1 do begin
        xa(i) = 157890.0*eij*dgex(i)/(1.0-dgex(i))
      endfor
    for i = 0,n_elements(dgey)-1 do begin
      if procval.type eq 1 then begin
        apoma(i) = fxc3*S*(alog(1.0D0+fxc2)+eei((1.0D0+fxc2)*		$
                   1.5789D5*eij/xa(i)))                  
      end else if procval.type eq 2 and outval.axop eq 1 then begin
        apoma(i) = fxc3+fxc2*(1.5789D5*eij/xa(i))*eei(1.5789D5*eij/xa(i))            
      end else if procval.type eq 3 and outval.axop eq 1 then begin
        apoma(i) = fxc3*1.5789D5*eij*ee2((1.0D0+fxc2)*1.5789D5*		$    
                   eij/xa(i))/((1.0D0+fxc2)*xa(i))         
        apoma(i) = fxc3/(xa(i)+fxc2)^2
      end else if procval.type eq 4 then begin
        apoma(i) = 0.0d0
      end else if procval.type eq 0 then begin
        apoma(i) = 0.0d0
      end
    endfor    
                   ;**** get new array values ****                 

      for i = 0,n_elements(dgey)-1 do begin
        ya(i) = apoma(i)*dgey(i)
      endfor 
      ictn = n_elements(dgey)

                   ;****print new values to pipe****
                   ;****   for re-analysing     ****    

    printf, pipe, ictn
    printf, pipe, lpend
    if lpend eq 2 then begin
    
;  convert back to user units at this point before transmitting
      
      z1=procval.z+1
      for i = 0,ictn-1 do begin
        ate=1.5789d5/xa(i)
        if procval.ifout(0) eq 0 then xa(i)=xa(i)
        if procval.ifout(0) eq 1 then xa(i)=xa(i)/11605.4
        if procval.ifout(0) eq 2 then xa(i)=xa(i)/(z1*z1)
        if procval.ifout(0) eq 3 then xa(i)=xa(i)/(1.5789d5*eij)
        if procval.ifout(1) eq 5 then ya(i)=ya(i)/			$
       		(4.60488d7*procval.lstwt*exp(eij*ate)/sqrt(ate))
        if procval.ifout(1) eq 6 then ya(i)=ya(i)/			$
      		(4.60488d7*procval.ustwt/sqrt(ate))
        if procval.ifout(1) eq 7 then ya(i)=ya(i)
        if procval.ifout(1) eq 8 then ya(i)=ya(i)*z1*z1
      endfor
      
      for i = 0,ictn-1 do begin
        printf, pipe, format = '(1x,e12.4,1x,e12.4)', 			$
                      xa(i), ya(i)
      endfor
    end

    if lpend eq 1 or lpend eq 2 then goto, LABELEND

                   ;**** re-set lpend ****
    lpend = 0
    arch = 0       ;**** default archive option is off
    b = intarr(5)
    for i = 0,4 do b(i) = -1

    adas102_plot, plotx, ploty, dgex, dgey, b, lpend, print1, arch, 	$
                  lchoice, outval, action, device, date, header, titlx, $
                  bitfile, gomenu, wintitle = 'UPSILON GRAPH', nsp = 70,$
                  font = font  
    printf, pipe, lpend

                   ;**** Print archiving decision if continuing ****

    if lpend eq 0 then printf, pipe, arch
    
    if lpend eq 1 then goto, LABELEND


                   ;**** special case if archiving selected after ****
                   ;**** picking no archiving initially ****

    if arch eq 1 and lchoice eq 0 then begin
      dsarch = root+'archive.dat'
      printf, pipe,format = '(1a80)', dsarch
    end

LABELEND:

end


 
