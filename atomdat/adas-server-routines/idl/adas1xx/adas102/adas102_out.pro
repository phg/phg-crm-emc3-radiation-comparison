; Copyright (c) 1995 Strathclyde University .
; SCCS info: Module @(#)$Header: /home/adascvs/idl/adas1xx/adas102/adas102_out.pro,v 1.1 2004/07/06 10:08:56 whitefor Exp $ Date $Date: 2004/07/06 10:08:56 $
;+
; PROJECT:
;       ADAS IBM MVS to   UNIX conversion
;
; NAME:
;	ADAS102_OUT
;
; PURPOSE:
;	IDL ADAS user interface, output options.
;
; EXPLANATION:
;	This routine creates and manages a pop-up window which allows
;	the user to select options and input data to control ADAS102
;	graphical and file output.
;
; USE:
;	This routine is ADAS102 specific, see e1spf1.pro for how it
;	is used.
;
; INPUTS:
;	VAL	- A structure which determines the initial settings of
;		  the output options widget.  The value is passed
;		  unmodified into cw_adas102_out.pro.
;
;		  See cw_adas102_out.pro for a full description of this
;		  structure.
;
;	DSFULL	- String; The full system file name of the input 
;		  dataset selected by the user for processing.
;
;       ROOT    - String; The rootpath to the user's archive dir.
;
;       TYPE    - Integer; The transition type, see procval structure.
;
;       BITFILE - String; the path to the directory containing bitmaps
;                 for the 'escape to series menu' button.
;
; OPTIONAL INPUTS:
;	None.
;
; OUTPUTS:
;	ACT	- String; Either 'Done' or 'Cancel' for the button the
;		  user pressed to terminate the output options window.
;
; OPTIONAL OUTPUTS:
;	None.
;
; KEYWORD PARAMETERS:
;	DEVLIST	- A string array;  Each element of the array is the
;		  name of a hardcopy graphics output device.  This
;		  list must mirror the DEVCODE list.  DEVLIST is the
;		  list which the user will see.
;		  e.g ['Post-Script','HP-PCL','HP-GL']
;
;	FONT	- Supplies the font to be used for the interface widgets.
;
; CALLS:
;	CW_ADAS102_OUT	Creates the output options widget.
;	XMANAGER
;	See side effects for widget management routine.
;
; SIDE EFFECTS:
;	This routine uses a common block OUT102_BLK to maintain its state.
;	ADAS102_OUT_EV	is included in this file and is called
;			indirectly from XMANAGER during widget
;			management.
;
; CATEGORY:
;	Widgets
;
; WRITTEN:
;       Conversion of adas101_out.pro by David H.Brooks, Univ.of Strathclyde
;
; MODIFIED:
;	1.1	Hugh P. Summers
; VERSION:
;	1.1	15-11-96
;-
;-----------------------------------------------------------------------------


pro adas102_out_ev, event

  common out102_blk,action,value
	

		;**** Find the event type and copy to common ****

    action = event.action

    case action of

		;**** 'Done' button ****

	'Done'  : begin

			;**** Get the output widget value ****

     		     child = widget_info(event.id,/child)
		     widget_control,child,get_value=value 

			;**** Kill the widget to allow IDL to ****
			;**** continue and interface with     ****
			;**** FORTRAN only if there is work   ****
			;**** for the FORTRAN to do.          ****
		     if (value.grpout eq 1) or (value.texout eq 1) then begin
		        widget_control,event.top,/destroy
		     endif 
	          end


		;**** 'Cancel' button ****

	'Cancel': widget_control,event.top,/destroy

		;**** 'Menu' button ****

	'Menu': widget_control,event.top,/destroy

    endcase

end

;-----------------------------------------------------------------------------


pro adas102_out, val, dsfull, act, root, type, bitfile, devlist=devlist, $
		font=font

  common out102_blk,action,value

		;**** Copy value to common ****
  value = val

		;**** Set defaults for keywords ****

  if not (keyword_set(font)) then font = ''
  if not (keyword_set(devlist)) then devlist = ''

		;***************************************
		;**** Pop-up a new widget           ****
		;***************************************

                ;**** create base widget ****

  outid = widget_base(title='ADAS102 OUTPUT OPTIONS',xoffset=150,yoffset=0)

		;**** Declare output options widget ****

  cwid = cw_adas102_out(outid, dsfull, root, type, bitfile, value=value,  $
			devlist=devlist, font=font )

		;**** Realize the new widget ****

  dynlabel, outid
  widget_control,outid,/realize

		;**** make widget modal ****

  xmanager,'adas102_out',outid,event_handler='adas102_out_ev',/modal,/just_reg
 
		;**** Return the output value from common ****

  act = action
  val = value

END

