; Copyright (c) 1999, Strathclyde University
; SCCS info: Module @(#)$Header: /home/adascvs/idl/adas1xx/adas102/eei.pro,v 1.1 2004/07/06 13:48:12 whitefor Exp $ Date $Date: 2004/07/06 13:48:12 $
;+
; PROJECT:
;       ADAS IBM MVS to UNIX conversion
;
; NAME:
;       eei
;
; PURPOSE:
;       Evaluates exp(x)E1(x) where E1 is the first exponential integral
;
; USE:
;       General
;
; INPUTS:
;       x	-	The independent variable of the function.
; OPTIONAL INPUTS:
;       None
;
; OUTPUTS:
;       eei	-	The evaluated function.
;
; OPTIONAL OUTPUTS:
;       None
;
; KEYWORD PARAMETERS:
;       None
;
; CALLS:
;       None
;
; SIDE EFFECTS:
;       None
;
; CATEGORY:
;       Adas system.
;
; WRITTEN:
;       Hugh Summers, University of Strathclyde, 19/05/99
;
; MODIFIED:
;	1.1	Hugh Summers
;		First release

; VERSION:
;	1.1	19/05/99
;
;-----------------------------------------------------------------------------

FUNCTION eei, x

;
      if (x le 1.0D0) then begin
;
         a = -alog(x) - 0.57721566D0 + x * (0.99999193D0 - x * 		$
             (0.24991055D0 - x * (0.05519968D0 - x *			$
             (0.00976004D0 - x * 0.00107857D0))))
         y = 0.5D0 * x
         z = 1.0D0 - y * (0.9998684D0 - y * (0.4982926D0 - y *		$
             (0.1595332D0 - y * 0.0293641D0)))
         eei = a / (z * z)
;
      endif else begin
;
         eei = (0.2677737343D0 + x * (8.6347608925D0 + x *		$
               (18.059016973D0 + x * (8.5733287401D0 + x)))) /		$
               (x * (3.9584969228D0 + x * (21.0996530827D0 + x *	$
               (25.6329561486D0 + x * (9.5733223454D0 + x)))))
;
      endelse
;
      
    return, eei

END
