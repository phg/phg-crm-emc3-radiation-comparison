; Copyright (c) 1995, Strathclyde University.
; SCCS info: Module @(#)$Header: /home/adascvs/idl/adas1xx/adas101/cw_archive_select.pro,v 1.15 2004/07/06 13:04:32 whitefor Exp $ Date @(#)$Header: /home/adascvs/idl/adas1xx/adas101/cw_archive_select.pro,v 1.15 2004/07/06 13:04:32 whitefor Exp $
;+
; PROJECT:
;       ADAS IBM MVS to UNIX conversion
;
; NAME: CW_ARCHIVE_SELECT()
;
; PURPOSE:
;	Allows UNIX system directory/archive file selection,
;       allocation of a new archive file or ignore archiving.
;
; EXPLANATION:
;       This function produces a compund widget with three archiving
;       button options; 'Old Archive','New Archive' or 'No Archive'. 
;       If 'Old archive' is selected the function  allows the user to
;       step through a UNIX file system selecting directories and files.
;       The widget consists of a non-editable text widget
;	which displays the name of the currently selected file/directory
;	and below it a selection list of all available choices
;	in the currently selected directory.  The '..' symbol, meaning
;	parent directory, may also appear in the selection list to allow
;	the user to back-track through the file system to explore
;	another directory path.  The file selection process starts
;	from a root directory which is specified by the caller.  The
;	user is not able to select the parent of this 'root' directory.
;	This is useful for confining the user to a particular part
;	of the UNIX file-system hierarchy. Once an 'Old Archive' is
;       selected, an index can be refreshed by clicking on 'Refresh from 
;       Archive' and entering a number in the text box provided.
;       If 'New Archive' is selected the same as above applies except
;       that the text widget becomes editable to allow the user to enter
;       a name and the 'refresh' option is not open.The file is instantly 
;       created so as to keep the various tests on whether the 
;       file exists ( the routine was originally modified from 
;       cw_file_select.pro). If 'No Archive' is selected no files are
;        selected and all tests are bypassed.
;
;	This widget generates events as the user interacts with it.
;	The event structure returned is;
;	{ID:0L, TOP:0L, HANDLER:0L, ACTION:''}
;	ACTION has one of two values indicating the state of the file
;	selection process, either 'nofile' when the current selection
;	is a directory or 'newfile' when the current selection is file.
;
; USE:
;	An example of how to use this widget;
;
;		value = {ROOT:'/', FILE:''}
;		base = widget_base()
;		id = cw_archive_select(base,VALUE=value)
;		widget_control,base,/realize
;
;	.... process events using widget_event or xmanager ....
;		rc = widget_event()
;
;	.... get the selected file ....
;		widget_control,id,get_value=value
;		filename = value.root+value.file
;
;	Note: this widget will generate events at every user interaction.
;
; INPUTS:
;       PARENT   - The ID of the parent widget.
;
; OPTIONAL INPUTS:
;	None.
;
; OUTPUTS:
;       The return value of this function is the ID of the compound
;       widget which is of type LONG integer.
;
; OPTIONAL OUTPUTS:
;       None
;
; KEYWORD PARAMETERS:
;	VALUE	- A structure which must be;
;		  {ROOT:'', FILE:''	$
;                  ARCH1:'', ARCH2:'',		$
;                  ARCH3:'', REFRESH:'' }
;		  The elements of the structure are as follows;
;
;		  ROOT - Root directory e.g '/usr/fred/adas/'
;		  FILE - Current data file in ROOT e.g 'adf/input.dat'
;		  ARCH1 - Label for the first archiving button
;		  ARCH2 - Label for the second archiving button
;		  ARCH3 - Label for the third archiving button
;		  REFRESH - Name of the archive chosen by the user
;
;		  The caller must ensure that ROOT and FILE are valid
;		  names otherwise this widget may cause an error.
;		  Both ROOT and FILE may be set to '' on entry.  This
;		  will cause ROOT to be set to the user's current
;		  directory './'.
;
;		  The default value is;
;		  flesval = {ROOT:'./', FILE:''}
;
;	TITLE	- A title to be added to the left of the selection list.
;
;	XSIZE	- The width in characters of the current selection text
;		  widget.
;
;	YSIZE	- The height of the selection list in rows of characters.
;
;       FONT    - A font to use for all text in this widget.
;
;	UVALUE	- A user value for this widget.
;
; CALLS:
;	FILE_ACC	Determine file type and access.
;	ARCH_EXTRACT_PATH Extract directory path to current file from
;			full name (Routine in this file).
;       CW_LOADSTATE    Recover compound widget state.
;       CW_SAVESTATE    Save compound widget state.
;       See side effects for other related widget management routines.
;
; SIDE EFFECTS:
;	This widget uses a COMMON BLOCK: CW_ARCHSEL_BLK to hold the
;       widget state.
;
;       Three other routines are included which are used to manage the
;       widget;
;
;	ARCHSEL_SET_VAL
;	ARCHSEL_GET_VAL()
;	ARCHSEL_EVENT()
;
;	One extra utility routine is included;
;
;	ARCH_EXTRACT_PATH()
;
; CATEGORY:
;	Compound Widget
;
; WRITTEN:
;       David H.Brooks, Univ.of Strathclyde, 02-July-1995
;       Extensively modified cw_file_select.pro for ADAS1XX
;       archiving application. The original routine was written by
;       Andrew Bowen, Tessella Support Services plc, 13-May-1993
;
; VERSION:
;       1.1	David H.Brooks       			    
;		First release
;	1.2	Tim Hammond (Tessella Support Services plc) 
;		Tidied up code formatting and changed names of routines
;               to avoid conflict when other file selection routines
;               are used.
;	1.3	Tim Hammond				   
;		Minor syntax correction
;	1.4	Tim Hammond				  
;		Changed name of structure fselval to flesval
;	1.5     Tim Hammond                              
;		As above (missed some first time)
;	1.6	Tim Hammond
;		Reduced default value of ysize keyword from 15 to 10.
;	1.7	William Osborn
;		Made 'list' an array when the directory doesn't exist
;	1.8	William Osborn
;		Made 'list' an array when the directory doesn't exist - missed
;		one instance
;	1.9	William Osborn
;		Changed a test for 'value.file' to one for 'file' so that
;		directory is listed correctly.
;	1.10	William Osborn
;		Changes to make more user-friendly.
;	1.11	William Osborn
;		Removed diagnostic print statement
;	1.12	William Osborn
;               Replaced set_value with set_button for button widgets
;       1.13    Hugh Summers
;               Adjusted for compatibility with adas101 and adas102
;	1.14	Richard Martin
;		Replace edit keyword in widget_control to editable.
;	1.15	Richard Martin
;		Removed obsolete cw_loadstate, cw_savestate.
;
; MODIFIED:
;	1.1	02-07-95
;	1.2	11-07-95
;	1.3	11-07-95
;	1.4	11-07-95
;	1.5	11-07-95
;	1.6	07-03-96
;	1.7	16-07-96
;	1.8	16-07-96
;	1.9	30-08-96
;	1.10	13-09-96
;	1.11	13-09-96
;	1.12	03-10-96
;       1.13    23-11-96
;	1.14	1-12-98
;	1.15	29-01-02
;
;-
;-----------------------------------------------------------------------------

FUNCTION arch_extract_path, filename

		;**** Find the end of the directory path ***

  pathend = -1
  i = strlen(filename) - 1
  while (i gt 0 and pathend eq -1) do begin
      if strmid(filename,i,1) eq '/' then pathend = i
      i = i - 1
  end

		;**** Extract directory path ****

  if pathend gt 0 then begin
      path = strmid(filename,0,pathend)
  end else begin
      path = ''
  end

  RETURN, path

END

;-----------------------------------------------------------------------------

PRO archsel_set_val, id, value


		;**** Return to caller on error ****
  ON_ERROR, 2

		;**** Retrieve the state ****

  first_child = widget_info(id,/child)
  widget_control, first_child, get_uvalue=state, /no_copy

		;**** Tidy pathname and rootname to conform to ****
		;**** routine pathname convention	       ****

  if strtrim(value.root) eq '' then begin
      value.root = './'
  endif else if strmid(value.root,strlen(value.root)-1,1) ne '/' then begin
      value.root = value.root+'/'
  endif
  if strmid(value.file,0,1) eq '/' then begin
      value.file = strmid(value.file,1,strlen(value.file)-1)
  endif

		;**** File name ****

  widget_control, state.fileid, set_value=value.file

		;**** See if the new 'file' is a file or a directory ****

  file = value.root+value.file
  file_acc, file, fileexist, read, write, execute, filetype

  if filetype ne 'd' then begin

		;**** Extract directory path ****

      file = arch_extract_path(file)
  endif

		;**** Get list of files in directory ****

  list = findfile(file)

		;**** If the current directory is not 'root' ****
		;**** add the '..' symbol to the list.       ****

  if strtrim(file) ne '' then begin
      if strtrim(list(0)) ne '' then begin
          list = ['..',list]
      endif else begin
          list = ['..']
      endelse
  endif else list = ['']

		;**** Set selection list ****

  widget_control, state.selid, set_value=list

                ;**** Set Archive buttons ****

  widget_control, state.arch1, set_value=value.arch1
  widget_control, state.arch2, set_value=value.arch2
  widget_control, state.arch3, set_value=value.arch3

  if value.refresh ne '' then begin
    widget_control, state.refr, set_button=1
    widget_control, state.refl, /sensitive
    widget_control, state.reft, /sensitive, set_value=value.refresh
    rbut = 1
  endif else begin
    widget_control, state.refr, set_button=0
    widget_control, state.refl, sensitive=0
    widget_control, state.reft, sensitive=0, set_value=''
    rbut = 0
  endelse

  flesval = {flesval, ROOT:value.root, FILE:value.file,			$
                      ARCH1:value.arch1, ARCH2:value.arch2,		$
                      ARCH3:value.arch3, REFRESH:value.refresh }

		;**** Recreate state structure to hold new list ****

  new_state =  { FILEID:state.fileid, SELID:state.selid, 		$
		 LIST:list, flesval:flesval,				$
                 ARCH1:state.arch1, ARCH2:state.arch2,			$
                 ARCH3:state.arch3, REFR:state.refr,			$
                 REFL:state.refl, REFT:state.reft, REFRESHBUT:rbut }

		;**** Save the new state ****

  widget_control, first_child, set_uvalue=new_state, /no_copy

END

;-----------------------------------------------------------------------------


FUNCTION archsel_get_val, id


		;**** Return to caller on error ****
  ON_ERROR, 2

		;**** Retrieve the state ****

  first_child = widget_info(id,/child)
  widget_control, first_child, get_uvalue=state, /no_copy

		;**** Get the refresh value ****
  if state.refreshbut eq 1 then begin
    widget_control, state.reft, get_value=value
    state.flesval.refresh = value(0)
  endif else state.flesval.refresh = ''


  flesval=state.flesval
  widget_control, first_child, set_uvalue=state, /no_copy  

  RETURN, flesval

END

;-----------------------------------------------------------------------------

FUNCTION archsel_event, event


		;**** Base ID of compound widget ****

  base=event.handler

		;**** Retrieve the state ****

  first_child = widget_info(base,/child)
  widget_control, first_child, get_uvalue=state, /no_copy

		;**** The default event is no file selected ****

  action = 'nofile'

		;************************
		;**** Process Events ****
		;************************

  CASE event.id OF

		;******************************
		;**** Selection list event ****
		;******************************

    state.selid: begin


		;**** See if the current 'file' is a file or directory ****

	file = state.flesval.root+state.flesval.file
	file_acc, file, fileexist, read, write, execute, filetype
	if state.list(event.index) eq '..' then begin

			;*****************************
			;**** Move up a directory ****
			;*****************************

  	    if filetype ne 'd' then begin
		;**** strip off two names for a file ***
	        state.flesval.file = arch_extract_path(state.flesval.file)
	    endif
	    state.flesval.file = arch_extract_path(state.flesval.file)

	    widget_control, state.fileid, set_value=state.flesval.file
	    file = state.flesval.root+state.flesval.file
	    list = findfile(file)
	    if strtrim(state.flesval.file) ne '' then begin
	        if strtrim(list(0)) ne '' then begin
	            list = ['..',list]
	        endif else begin
	            list = ['..']
	        endelse
	    endif
	    widget_control, state.selid, set_value=list


	endif else begin
			;*******************************************
			;**** Down a directory or select a file ****
			;*******************************************
			;**** Determine the new file name       ****
                        ;*******************************************

	    if filetype eq 'd' then begin
	        if strtrim(state.flesval.file) eq '' then begin
	            state.flesval.file = state.list(event.index)
	        endif else begin
	            state.flesval.file = $
		    state.flesval.file+'/'+state.list(event.index)
	        endelse
	    endif else begin
	        path = arch_extract_path(state.flesval.file)
	        if strtrim(path) eq '' then begin
	            state.flesval.file = state.list(event.index)
	        endif else begin
	            state.flesval.file = path+'/'+state.list(event.index)
	        endelse
	    endelse

		;**** See if the new 'file' is a file or directory ****

	    file = state.flesval.root+state.flesval.file
	    file_acc, file, fileexist, read, write, execute, filetype
                ;**** Display new name or report error if a file ****
            if filetype ne '-' then begin
	        widget_control,state.fileid,set_value=state.flesval.file
            endif else begin
                widget_control, state.arch2, get_uvalue = num
                if num ne 1 then begin
	            widget_control,state.fileid,set_value=state.flesval.file
                endif else begin
                    button = ['ok']
                    title='ADAS1XX: ERROR'
                    message='You Have Selected an Old Archive...'
                    respond=popup(message=message, title=title,		$
                                  button=button,font=font)
                endelse
            endelse


		;**** Update list if new selection is a directory ****

	    if filetype eq 'd' then begin
	        list = findfile(file)
	        if strtrim(state.flesval.file) ne '' then begin
	            if strtrim(list(0)) ne '' then begin
		        list = ['..',list]
	            endif else begin
		        list = ['..']
	            endelse
	        endif
	        widget_control, state.selid, set_value=list
	    endif else begin
	        list = state.list
	        action = 'newfile'
	    endelse

	endelse

      end


    state.arch1: begin
        widget_control, state.arch2, set_uvalue = 0
        widget_control, state.selid, /sensitive
        widget_control, state.fileid, /sensitive
        widget_control, state.fileid, editable=0
        list = state.list
        widget_control, state.refr, /sensitive
    end

    state.arch2: begin
        widget_control, state.arch2, set_uvalue = 1
        widget_control, state.selid, /sensitive
        widget_control, state.fileid, /sensitive
        widget_control, state.fileid, /editable
	state.refreshbut=0
        list = state.list
        widget_control, state.refr, sensitive=0, set_button=0
        widget_control, state.refl, sensitive=0
        widget_control, state.reft, sensitive=0
    end

    state.arch3: begin
        widget_control, state.arch2, set_uvalue = 0
        widget_control, state.selid, sensitive=0
        widget_control, state.fileid, sensitive=0
        list = state.list
        widget_control, state.refr, sensitive=0, set_button=0
        widget_control, state.refl, sensitive=0
        widget_control, state.reft, sensitive=0
	state.refreshbut=0
        state.flesval.file = ''
        action = 'newfile'
    end

    state.fileid:begin
        widget_control, state.fileid, get_uvalue = uvalue
        if uvalue eq 0 then begin
            widget_control, state.fileid, get_value = value
            state.flesval.file = value(0)
            file1 = state.flesval.root+state.flesval.file
            file_acc, file1, exist, read, write, execute, type
            if exist eq 0 and type ne 'd' then begin
                widget_control, state.fileid, set_uvalue = 1
                get_lun, unit
                openw, unit, file1
                free_lun, unit
                action = 'newfile'
            endif
        endif else begin
            file1 = state.flesval.root+state.flesval.file
            file_acc, file1, exist, read, write, execute, type
            if exist eq 1 and type ne 'd' then begin
                spawn, 'rm '+file1
                widget_control, state.fileid, get_value = value
                state.flesval.file = value(0)
                file1 = state.flesval.root+state.flesval.file
                get_lun, unit
                openw, unit, file1
                free_lun, unit
                action = 'newfile'
            endif
        endelse  
        list = state.list
    end

    state.refr:begin
        if state.refreshbut eq 0 then begin
	  state.refreshbut = 1
          widget_control, state.refl, /sensitive
          widget_control, state.reft, /sensitive
	endif else begin
	  state.refreshbut = 0
          widget_control, state.refl, sensitive=0
          widget_control, state.reft, sensitive=0
	endelse
        list = state.list
    end

    state.reft:begin
        widget_control, state.reft, get_value=value
        state.flesval.refresh = value(0)
        list = state.list
        action = 'newfile'
    end
  
  ENDCASE

		;**** recreate state structure to fit in new list ****

  new_state =  { FILEID:state.fileid, SELID:state.selid, 		$
		 LIST:list, flesval:state.flesval, 			$
                 ARCH1:state.arch1, ARCH2:state.arch2,			$
                 ARCH3:state.arch3, REFR:state.refr,			$
                 REFL:state.refl, REFT:state.reft, 			$
		 REFRESHBUT:state.refreshbut }

		;**** Save the new state structure ****

  widget_control, first_child, set_uvalue=new_state, /no_copy

		;**** Generate event indicating if file is selected ****

  RETURN, {ID:base, TOP:event.top, HANDLER:0L, ACTION:action}

END

;-----------------------------------------------------------------------------

FUNCTION cw_archive_select, parent, VALUE=value, TITLE=title, 		$
		            XSIZE=xsize, YSIZE=ysize, FONT=font, 	$
                            UVALUE=uvalue

  IF (N_PARAMS() LT 1) THEN MESSAGE, 'Must specify PARENT for cw_archive_select'

		;**** Set defaults for keywords ****

  IF NOT (KEYWORD_SET(value)) THEN begin
      flesval = {flesval, ROOT:'./', FILE:'',				$
                          ARCH1:'Old Archive',ARCH2:'New Archive',	$
                          ARCH3:'No Archive', REFRESH:''}
  ENDIF ELSE BEGIN
      flesval = {flesval, ROOT:value.root, FILE:value.file,		$
                          ARCH1:value.arch1, ARCH2:value.arch2,		$
                          ARCH3:value.arch3, REFRESH:value.refresh }
      if strtrim(flesval.root) eq '' then begin
          flesval.root = './'
      endif else if strmid(flesval.root,strlen(flesval.root)-1,1) 	$
      ne '/' then begin
          flesval.root = flesval.root+'/'
      endif
      if strmid(flesval.file,0,1) eq '/' then begin
          flesval.file = strmid(flesval.file,1,strlen(flesval.file)-1)
      endif
  ENDELSE

  IF NOT (KEYWORD_SET(title)) THEN title = 'Archive File'
  IF NOT (KEYWORD_SET(xsize)) THEN xsize = 60
  IF NOT (KEYWORD_SET(ysize)) THEN ysize = 10
  IF NOT (KEYWORD_SET(font)) THEN font = ''
  IF NOT (KEYWORD_SET(uvalue))  THEN uvalue = 0

		;**** Create the main base for the widget ****

  cwid = WIDGET_BASE(parent, UVALUE = uvalue, 				$
		     EVENT_FUNC = "archsel_event", 			$
		     FUNC_GET_VALUE = "archsel_get_val", 		$
		     PRO_SET_VALUE = "archsel_set_val", 		$
		     /ROW)

		;**** Create base to hold the value of state ****

  first_child = widget_base(cwid)

		;**** Title ****

  rc = widget_label(first_child, value=title, font=font)

                ;**** Archive choices ****

  col = widget_base(cwid, /column)
  rowa = widget_base(col, /row, /exclusive)
  rowb = widget_base(col, /row, /frame)
  rowc = widget_base(col, /row)
  arch1 = widget_button(rowa, value = flesval.arch1, font=font, /no_release)
  arch2 = widget_button(rowa, value = flesval.arch2, font=font, /no_release)
  arch3 = widget_button(rowa, value = flesval.arch3, font=font, /no_release)

                ;**** new addition refresh button ****

  colb = widget_base(rowb, /column)
  rba = widget_base(colb, /row, /nonexclusive)
  rbb = widget_base(colb, /row)
  refr = widget_button(rba, value='Refresh from Archive', font=font)
  refl = widget_label(rbb, value = 'Archive Index Number: ', font=font)
  reft = widget_text(rbb, value=flesval.refresh, /edit, xsize=8, font=font)
  widget_control, refr, sensitive = 0
  widget_control, refl, sensitive = 0
  widget_control, reft, sensitive = 0

  base = widget_base(rowc, /column)

		;**** File name ****

  fileid = widget_text(base, value=flesval.file, xsize=xsize, font=font)
  widget_control, fileid, sensitive=0

		;**** Selection list ****

  selid = widget_list(base, ysize=ysize, font=font)
  widget_control, selid, sensitive=0

		;**** See if the current 'file' is a file ****
		;**** or a directory.                     ****

  listdir = flesval.root+flesval.file
  file_acc, listdir, fileexist, read, write, execute, filetype

  if filetype ne 'd' then begin
		;**** Extract directory path ****
      listdir = arch_extract_path(listdir)
  endif

		;**** Get list of files in directory ****

  list = findfile(listdir,count=count)

                ;**** special case: only one file ****
                ;**** therefore add a blank DHB 17/05/95 ****

  if count eq 1 then begin
      list1 = strarr(2)
      list1(0) = list
      list = list1
  endif else if count eq 0 then list = ['']

		;**** If the current directory is not 'root' ****
		;**** add the '..' symbol to the list.       ****

  if strtrim(flesval.file) ne '' then begin
      if strtrim(list(0)) ne '' then begin
          list = ['..',list]
      endif else begin
          list = ['..']
      endelse
  endif

		;**** Set selection list ****

  widget_control, selid, set_value=list

                ;**** Set uvalue of arch2 for event handler      ****
                ;**** to flag a contradiction i.e. if you choose ****
                ;**** new archive and select an old one          ****

  widget_control, arch2, set_uvalue = 0

                ;**** set fileid uvalue to trap mistaken entry ****

  widget_control, fileid, set_uvalue = 0

		;**** Create state structure ****

  new_state =  { FILEID:fileid, SELID:selid, LIST:list, flesval:flesval ,$
                 ARCH1:arch1,  ARCH2:arch2,  ARCH3:arch3, REFR:refr,	 $
                 REFL:refl, REFT:reft, REFRESHBUT:0 }

		;**** Save initial state structure ****

  widget_control, first_child, set_uvalue=new_state, /no_copy

  RETURN, cwid

END
