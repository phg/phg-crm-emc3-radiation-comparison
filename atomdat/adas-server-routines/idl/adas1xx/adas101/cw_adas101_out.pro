; Copyright (c) 1995, Strathclyde University.
; SCCS Info : Module @(#)$Header: /home/adascvs/idl/adas1xx/adas101/cw_adas101_out.pro,v 1.7 2004/07/06 12:26:59 whitefor Exp $ Date $Date: 2004/07/06 12:26:59 $     
;+
; PROJECT:
;       ADAS IBM MVS to   UNIX conversion
;
; NAME:
;	CW_ADAS101_OUT()
;
; PURPOSE:
;	Produces a widget for ADAS101 output options.
;
; EXPLANATION:
;	This function declares a compound widget consisting of two
;	graphical output selection widget cw_101_gr_sel.pro, and an 
;	output file widget cw_adas_outfile.pro.  The text output
;	file specified in this widget is for tabular (paper.txt)
;	output.  There are also two unmapped widgets which respond
;       to two buttons. These invoke two different analysis options.
;       This widget also includes a button for browsing the comments
;       from the input dataset, a 'Cancel' button and a 'Done' button.
;	The compound widgets cw_101_gr_sel.pro and cw_adas_outfile.pro
;	included in this file are self managing.  This widget only
;	handles events from the 'Done' and 'Cancel' buttons.
;
; USE:
;	This widget is specific to ADAS101, see adas101_out.pro	for use.
;
; INPUTS:
;	PARENT	- Long integer; ID of parent widget.
;
;	DSFULL	- Name of input dataset for this application.
;
;       ROOT    - String; The path to the users archive directory.
;
;       TYPE    - Integer; The transition type.
;                 1-Electric dipole
;                 2-Non-dipole
;                 3-Spin change
;                 4-other
;
;       BITFILE - String; the path to the directory containing bitmaps
;                 for the 'escape to series menu' button.
;
; OPTIONAL INPUTS:
;	None.
;
; OUTPUTS:
;	The return value of this function is the ID of the declared widget.
;
; OPTIONAL OUTPUTS:
;	None.
;
; KEYWORD PARAMETERS:
;	DEVLIST	- A string array;  Each element of the array is the
;		  name of a hardcopy graphics output device.  This
;		  list must mirror the DEVCODE list.  DEVLIST is the
;		  list which the user will see.
;		  e.g ['Post-Script','HP-PCL','HP-GL']
;
;	VALUE	- A structure which determines the initial settings of
;		  the output options widget.  The structure is made
;		  up of three parts.  Two of the parts are the same as
;                 the value structure of one of the two main compound widgets
;		  included in this widget.  See cw_101_gr_sel and
;		  cw_adas_outfile for more details. The other part is
;                 associated with the two analysis options.
;                 The default value is;
;
;             {out101_set, 						$
;			GRPOUT:0, GTIT1:'', 				$
;			GRPSCAL1:0, 					$
;			XMIN1:'',  XMAX1:'',   				$
;			YMIN1:'',  YMAX1:'',   				$
;			GRPSCAL2:0, 					$
;			XMIN2:'',  XMAX2:'',   				$
;			YMIN2:'',  YMAX2:'',   				$
;                       ANOPT:-1,  AXOP:0, ADIF:0, 			$ 
;                       ADOPT:1,  BDPTOPT:0,  				$
;                       BCVAL:'',   BXVAL:0,  BYVAL:'',  		$
; 		        HRDOUT:0, HARDNAME:'', 				$
;	                GRPDEF:'', GRPFMESS:'', 			$
;			GRPSEL:-1, GRPRMESS:'', 			$
;			DEVSEL:-1, GRSELMESS:'', 			$
;			TEXOUT:0, TEXAPP:-1, 				$
;			TEXREP:0, TEXDSN:'', 				$
;                       TEXDEF:'',TEXMES:'' 				$
;              }
;                 For the analysis options;
;                       ANOPT      Integer; Chosen option 1-ADAS, 2-Burgess
;                       AXOP       Integer; Optimising off-0, on-1
;                       ADIF       Integer; Fitting for dipole x-section
;						0-Ratio,1-Difference
;                       ADOPT      Integer; 1-One point optimising,
;						2-two point optimising
;                       BDPTOPT    Integer; Bad point option, 0-off, 1-on
;                       BCVAL      Integer; Burgess C value
;                       BXVAL      Integer; Point at infinity option 
;						for type 2 transitions
;                                           	0-off, 1-on
;                       BYVAL      Integer; Point at infinity y-value
;  For CW_101_GR_SEL;
;			GRPOUT	   Integer; Activation button 1 on, 0 off
;			GTIT1	   String; Graph title
;			GRPSCAL1   Integer; Scaling activation 1 on, 0 off
;			XMIN1	   String; x-axis minimum for 1st plot
;			XMAX1	   String; x-axis maximum for 1st plot
;			YMIN1	   String; y-axis minimum for 1st plot
;			YMAX1	   String; y-axis maximum for 1st plot
;			GRPSCAL2   Integer; Scaling activation 1 on, 0 off
;			XMIN2	   String; x-axis minimum for 2nd plot
;			XMAX2	   String; x-axis maximum for 2nd plot
;			YMIN2	   String; y-axis minimum for 2nd plot
;			YMAX2	   String; y-axis maximum for 2nd plot
;			HRDOUT	   Integer; Hard copy activ' 1 on, 0 off
;			HARDNAME   String; Hard copy output file name
;			GRPDEF	   String; Default output file name 
;			GRPFMESS   String; File name error message
;			GRPSEL	   Integer; index of selected graph in GRPLIST
;			GRPRMESS   String; Scaling ranges error message
;			DEVSEL	   Integer; index of selected device in DEVLIST
;			GRSELMESS  String; General error message
;
;		  For CW_ADAS_OUTFILE tabular output (paper.txt);
;			TEXOUT	Integer; Activation button 1 on, 0 off
;			TEXAPP	Integer; Append button 1 on, 0 off, 
;					-1 no button
;			TEXREP	Integer; Replace button 1 on, 0 off, 
;					-1 no button
;			TEXDSN	String; Output file name
;			TEXDEF	String; Default file name
;			TEXMES	String; file name error message
;
;
;	UVALUE	- A user value for this widget.
;
;	FONT	- String; the name of a font to be used for all text in
;		  this widget.  Default to system default font.
;
; CALLS:
;	CW_ADAS_DSBR	Input dataset name and comments browse button.
;	CW_101_GR_SEL	Graphical output selection widget.
;	CW_ADAS_OUTFILE	Output file name entry widget.
;	See side effects for other related widget management routines.
;
; SIDE EFFECTS:
;
; 	Two other routines are included in this file
;	OUT101_GET_VAL()
;	OUT101_EVENT()
;
; CATEGORY:
;	Compound Widget
;
; WRITTEN:
;       David H.Brooks, Univ.of Strathclyde, Ext.4213/4205
;
; MODIFIED:
;	1.1	David H. Brooks
;		First release
;	1.2	Tim Hammond (Tessella Support Services plc)
;		Tidied up code and comments slightly
;	1.3	Tim Hammond
;		Replaced call to cw_adas_gr_sel with a call to 
;		cw_101_gr_sel - specifically for adas101.
;	1.4	Tim Hammond
;		Renamed structure grselval to gr101val.
;	1.5	Tim Hammond
;		Added BUTTON keyword to call to cw_adas_dsbr
;	1.6	William Osborn
;		Added menu button
;	1.7	William Osborn
;		Removed xsize=n keyword from widget_label command.
;		The dynlabel procedure does not override these
;		assignments and they were taken as being values in pixels
;		on some machines.
; VERSION:
;	1.1	02-Jul-1995
;	1.2	12-Jul-1995
;	1.3	12-Jul-1995
;	1.4	12-Jul-1995
;	1.5	13-Jul-1995
;	1.6	30-05-96
;	1.7	01-08-96
;-
;-----------------------------------------------------------------------------

FUNCTION out101_get_val, id

                ;**** Return to caller on error ****
  ON_ERROR, 2

                ;**** Retrieve the state ****

  parent=widget_info(id,/parent)
  widget_control, parent, get_uvalue=state, /no_copy

		;**** Get graphical output settings ****

  widget_control,state.grpid,get_value=gr101val

		;**** Get text output settings ****

  widget_control,state.paperid,get_value=papos

                ;**** Get editable options settings ****
                ;**** and process options uvalues ****

  if state.anopt eq 1 then begin
    widget_control,state.adbut1,get_uvalue = adopt1
    widget_control,state.adbut3,get_uvalue = bdpt1
    widget_control,state.adbut5,get_uvalue = axop1
    widget_control,state.adbut7,get_uvalue = adif1   
      if adopt1 eq 1 then begin
        adopt = 1
      end else begin
        adopt = 2
      end
        anopt = 1
      if bdpt1 eq 1 then begin
        bdpt = 1
      end else begin
        bdpt = 0
      end
      if axop1 eq 1 then begin
        axop = 1
      end else begin
        axop = 0
      end
      if adif1 eq 1 then begin
        adif = 0
      end else begin
        adif = 1
      end

                ;**** Defaults for unselected option ****

      bcval = -1
      bxval = bcval & byval = bcval    
  end else begin    
      widget_control,state.butxt1, get_value = bcvalt
      widget_control, state.pinfid, get_uvalue = bxvalt
      widget_control,state.butxt3, get_value = byvalt
      bcval = bcvalt(0)
      byval = byvalt(0)
      if bxvalt eq 0 then begin
        bxval = 0
      end else begin
        bxval = 1
      end
        anopt = 2
                ;**** Defaults for unselected option ****
        adopt = 1
        bdpt  = 0
        axop = 0
        adif = 0
  end
                ;**** save out structure ****  

  os = { out101_set, 						 $
	   GRPOUT:gr101val.outbut, GTIT1:gr101val.gtit1, 	 $
	   GRPSCAL1:gr101val.scalbut1, 			 	 $
	   XMIN1:gr101val.xmin1,   XMAX1:gr101val.xmax1,    	 $
	   YMIN1:gr101val.ymin1,   YMAX1:gr101val.ymax1,    	 $
	   GRPSCAL2:gr101val.scalbut2, 			 	 $
	   XMIN2:gr101val.xmin2,   XMAX2:gr101val.xmax2,    	 $
	   YMIN2:gr101val.ymin2,   YMAX2:gr101val.ymax2,    	 $
           ANOPT:anopt,            AXOP:axop,                    $
           ADIF:adif,              ADOPT:adopt,                  $
           BDPTOPT:bdpt,           BCVAL:float(bcval),           $
           BXVAL:bxval,            BYVAL:float(byval),           $
	   HRDOUT:gr101val.hrdout, HARDNAME:gr101val.hardname,   $
	   GRPDEF:gr101val.grpdef, GRPFMESS:gr101val.grpfmess,   $
	   GRPSEL:gr101val.grpsel, GRPRMESS:gr101val.grprmess,   $
	   DEVSEL:gr101val.devsel, GRSELMESS:gr101val.grselmess, $
	   TEXOUT:papos.outbut,    TEXAPP:papos.appbut, 	 $
	   TEXREP:papos.repbut,    TEXDSN:papos.filename, 	 $
	   TEXDEF:papos.defname,   TEXMES:papos.message 	 $
        }

                ;**** Return the state ****

  widget_control, parent, set_uvalue=state, /no_copy

  RETURN, os 

END

;-----------------------------------------------------------------------------

FUNCTION out101_event, event


                ;**** Base ID of compound widget ****

  parent=event.top

                ;**** Retrieve the state ****

  widget_control, parent, get_uvalue=state, /no_copy

		;*********************************
		;**** Clear previous messages ****
		;*********************************

  widget_control,state.messid,set_value=' '

                ;************************
                ;**** Process events ****
                ;************************

  CASE event.id OF

		;***********************
		;**** Cancel button ****
		;***********************

    state.cancelid: new_event = {ID:parent, TOP:event.top, 		$
				HANDLER:0L, ACTION:'Cancel'}

		;*********************
		;**** Done button ****
		;*********************

    state.doneid: begin

		;****************************************************
		;**** Popup warning if no analysis option picked ****
		;****************************************************

          if state.anopt eq -1 then begin
             buttons = ['Continue']
             title = 'ADAS101 ERROR'
             message = 'You MUST select an ANALYSIS Option'
             act = popup(message=message,title=title,buttons=buttons)
          end else begin
                ;**** Return the state before checking can start ****
		;**** with the get_value keyword.                ****
            widget_control, parent, set_uvalue=state, /no_copy

		;***************************************
		;**** Check for errors in the input ****
		;***************************************

	    error = 0

		;**** Get a copy of the widget value ****

	    widget_control,event.handler,get_value=os
	
		;**** Check for widget error messages ****

	    if os.grpout eq 1 and strtrim(os.grpfmess) ne '' then error=1
	    if os.grpout eq 1 and os.grpscal1 eq 1 and 			$
	  			strtrim(os.grprmess) ne '' then error=1
	    if os.grpout eq 1 and os.grpscal2 eq 1 and 			$
	  			strtrim(os.grprmess) ne '' then error=1
	    if os.grpout eq 1 and strtrim(os.grselmess) ne '' then error=1
	    if os.texout eq 1 and strtrim(os.texmes) ne '' then error=1

                ;**** Retrieve the state   ****

            widget_control, parent, get_uvalue=state, /no_copy

	    if error eq 1 then begin
	      widget_control,state.messid,set_value= 			$

		'**** Error in output settings ****'

	      new_event = 0L
	    end else begin
	      new_event = {ID:parent, TOP:event.top, HANDLER:0L, ACTION:'Done'}
	    end
          end

        end
                ;**** Process the analysis options   ****

    state.adasbut:begin
          widget_control, state.gr2base, map = 0
          widget_control, state.gr1base, map = 1
          state.anopt = 1
    end
    state.burgbut:begin
          widget_control, state.gr1base, map = 0
          widget_control, state.gr2base, map = 1
          state.anopt = 2
    end
    state.adbut1:begin
         widget_control, state.adbut1, set_uvalue = 1
         widget_control, state.adbut2, set_uvalue = 0
    end
    state.adbut2:begin
         widget_control, state.adbut1, set_uvalue = 0
         widget_control, state.adbut2, set_uvalue = 1
    end
    state.adbut3:begin
         widget_control, state.adbut3, set_uvalue = 1
         widget_control, state.adbut4, set_uvalue = 0
    end     
    state.adbut4:begin
         widget_control, state.adbut3, set_uvalue = 0
         widget_control, state.adbut4, set_uvalue = 1

    end  
    state.adbut5:begin
         widget_control, state.adbut5, set_uvalue = 1
         widget_control, state.adbut6, set_uvalue = 0
    end
    state.adbut6:begin
         widget_control, state.adbut5, set_uvalue = 0
         widget_control, state.adbut6, set_uvalue = 1
    end
    state.adbut7:begin
         widget_control, state.adbut7, set_uvalue = 1
         widget_control, state.adbut8, set_uvalue = 0
    end     
    state.adbut8:begin
         widget_control, state.adbut7, set_uvalue = 0
         widget_control, state.adbut8, set_uvalue = 1

    end  
    state.butxt1:begin
         widget_control, state.butxt1, get_value = temp
         widget_control, state.butxt1, set_value = temp(0)
         burgchk = float(temp(0))
		;***************************************
		;**** Popups for incorrect c-values ****
		;***************************************
         if state.type eq 1 or state.type eq 4 then begin
           if burgchk le 1.0 then begin
             butts = ['Continue']
             title = 'ADAS101 WARNING!'
             mess = 'C must be > 1 for this transition type'
             action = popup(message=mess,title=title,buttons=butts) 
           end
         end else begin
           if burgchk le 0.0 then begin
             butts = ['Continue']
             title = 'ADAS101 WARNING!'
             mess = 'C must be > 0 for this transition type'
             action = popup(message=mess,title=title,buttons=butts)
           end 
         end 
    end
    state.butxt3:begin
         widget_control, state.butxt1, /input_focus
         widget_control, state.butxt3, get_value = temp
         widget_control, state.butxt3, set_value = temp(0)
    end
    state.pinfid:begin
         widget_control, state.pinfid, set_uvalue = 1
         widget_control, state.butxt3, /sensitive
         widget_control, state.bulbl2, /sensitive
    end
		;*********************
                ;**** Menu button ****
		;*********************

    state.outid: begin
        new_event = {ID:parent, TOP:event.top, HANDLER:0L,          $
                         ACTION:'Menu'}
    end


    ELSE: new_event = 0L

  ENDCASE

                ;**** Return the state   ****

          widget_control, parent, set_uvalue=state, /no_copy

  RETURN, new_event
END

;-----------------------------------------------------------------------------

FUNCTION cw_adas101_out, parent, dsfull, root, type, bitfile, 	$
		DEVLIST=devlist, VALUE=value, UVALUE=uvalue, FONT=font

  IF (N_PARAMS() EQ 0) THEN MESSAGE, 'Must specify parent for cw_adas101_out'
  ON_ERROR, 2					;return to caller

		;**** Set defaults for keywords ****
  IF NOT (KEYWORD_SET(devlist)) THEN devlist = ''
  IF NOT (KEYWORD_SET(uvalue)) THEN uvalue = 0
  IF NOT (KEYWORD_SET(font)) THEN font = ''
  IF NOT (KEYWORD_SET(value)) THEN begin
	os = {out101_set, 						$
			GRPOUT:0, GTIT1:'', 				$
			GRPSCAL1:0, 					$
			XMIN1:'',  XMAX1:'',   				$
			YMIN1:'',  YMAX1:'',   				$
			GRPSCAL2:0, 					$
			XMIN2:'',  XMAX2:'',   				$
			YMIN2:'',  YMAX2:'',   				$
                        ANOPT:-1,  AXOP:0, ADIF:0, 			$
                        ADOPT:1,  BDPTOPT:0,  				$
                        BCVAL:'',   BXVAL:0,  BYVAL:'',  		$
 		        HRDOUT:0, HARDNAME:'', 				$
	                GRPDEF:'', GRPFMESS:'',				$
			GRPSEL:-1, GRPRMESS:'', 			$
			DEVSEL:-1, GRSELMESS:'', 			$
			TEXOUT:0, TEXAPP:-1, 				$
			TEXREP:0, TEXDSN:'', 				$
                        TEXDEF:'',TEXMES:'' 				$
              }
  END ELSE BEGIN
	os = {out101_set, 						$
			GRPOUT:value.grpout, GTIT1:value.gtit1, 	$
			GRPSCAL1:value.grpscal1, 			$
			XMIN1:value.xmin1,     XMAX1:value.xmax1,   	$
			YMIN1:value.ymin1,     YMAX1:value.ymax1,   	$
			GRPSCAL2:value.grpscal2, 			$
			XMIN2:value.xmin2,     XMAX2:value.xmax2,   	$
			YMIN2:value.ymin2,     YMAX2:value.ymax2,   	$
                        ANOPT:value.anopt,   AXOP:value.axop, 		$ 
                        ADIF:value.adif ,    ADOPT:value.adopt, 	$
                        BDPTOPT:value.bdptopt, BCVAL:value.bcval, 	$
                        BXVAL:value.bxval,   BYVAL:value.byval,  	$
			HRDOUT:value.hrdout, HARDNAME:value.hardname, 	$
			GRPDEF:value.grpdef, GRPFMESS:value.grpfmess, 	$
			GRPSEL:value.grpsel, GRPRMESS:value.grprmess, 	$
			DEVSEL:value.devsel, GRSELMESS:value.grselmess, $
			TEXOUT:value.texout, TEXAPP:value.texapp, 	$
			TEXREP:value.texrep, TEXDSN:value.texdsn, 	$
			TEXDEF:value.texdef, TEXMES:value.texmes 	$
	      }
  END
		;**********************************************
		;**** Create the 101 Output options widget ****
		;**********************************************

		;**** create base widget ****

  cwid = widget_base( parent, UVALUE = uvalue, 				$
			EVENT_FUNC = "out101_event", 			$
			FUNC_GET_VALUE = "out101_get_val", 		$
			/COLUMN)

		;**** Add dataset name and browse button ****

  rc = cw_adas_dsbr(cwid, dsfull, font=font, button='Browse Index')

		;**** Widget for graphics selection ****
		;**** Note change in names for GRPOUT and GRPSCAL ****

  gr101val = {  OUTBUT:os.grpout, GTIT1:os.gtit1, 			$
                SCALBUT1:os.grpscal1, 					$
		XMIN1:os.xmin1, XMAX1:os.xmax1, 			$
		YMIN1:os.ymin1, YMAX1:os.ymax1, 			$
                SCALBUT2:os.grpscal2, 					$
		XMIN2:os.xmin2, XMAX2:os.xmax2, 			$
		YMIN2:os.ymin2, YMAX2:os.ymax2, 			$
		HRDOUT:os.hrdout, HARDNAME:os.hardname, 		$
		GRPDEF:os.grpdef, GRPFMESS:os.grpfmess, 		$
		GRPSEL:os.grpsel, GRPRMESS:os.grprmess, 		$
		DEVSEL:os.devsel, GRSELMESS:os.grselmess }

                ;**** ADAS/Burgess analysis switching bases ****

  newrow = widget_base(cwid,/row,/frame)
  newcol = widget_base(newrow,/column,/frame,/exclusive)
  adasbut = widget_button(newcol,value = 'ADAS Analysis Option   ',	$
                          /no_release,font=font)
  burgbut = widget_button(newcol,value = 'Burgess Analysis Option',	$
                          /no_release,font=font)

                ;**** sensitize the panel in line with ****
                ;**** graph output request switch  and ****
                ;**** analysis option switch           ****

  widget_control, newcol, /sensitive

                ;**** base widget for switching ****

  actbase = widget_base(cwid,/frame)
    widget_control, actbase, /sensitive
    gr1base = widget_base(actbase,/column)
    gr2base = widget_base(actbase,/column)
      if os.anopt eq -1 then begin
        widget_control, gr1base, map=0
        widget_control, gr2base, map=0
        widget_control, burgbut, set_button=0
        widget_control, adasbut, set_button=0
      end 

                ;**** widget for ADAS analysis option ****

  row = widget_base(gr1base,/row)
  col1 = widget_base(row,/column)
  col2 = widget_base(row,/column)
    arow1 = widget_base(col1,/row)
    arow2 = widget_base(col1,/row)
    acol1 = widget_base(arow1,/column)
    arow3  = widget_base(arow1,/row,/exclusive)
      optlevid = widget_label(acol1,value = 'Optimisation Level : ',font=font)
      adbut1 = widget_button(arow3,value = '1 ',font=font,/no_release)
      adbut2 = widget_button(arow3,value = '2  ',font=font,/no_release)
    acol2 = widget_base(arow2,/column)
    arow4  = widget_base(arow2,/row,/exclusive)
      badptid = widget_label(acol2,value = 'Bad Point Option   : ',font=font)
      adbut3 = widget_button(arow4,value = 'On ',font=font,/no_release)
      adbut4 = widget_button(arow4,value = 'Off',font=font,/no_release)
  arow5 = widget_base(col2,/row)
  arow6 = widget_base(col2,/row)
  acol3 = widget_base(arow5,/column)
  arow7  = widget_base(arow5,/row,/exclusive)
    optonid = widget_label(acol3,value = '          Optimisation Option : ',$
                           font=font)
    adbut5 = widget_button(arow7,value = 'On ',font=font,/no_release)
    adbut6 = widget_button(arow7,value = 'Off       ',font=font,/no_release)
  acol4  = widget_base(arow6,/column)
  arow8  = widget_base(arow6,/row,/exclusive)
    fitid = widget_label(acol4,value = '       Dipole X-sect. Fitting : ',$
                         font=font)
    adbut7 = widget_button(arow8,value = 'Ratio',font=font,/no_release)
    adbut8 = widget_button(arow8,value = 'Difference',font=font,/no_release)

                ;**** Set defaults ****

    if os.adopt eq 1 then begin
      widget_control, adbut1, /set_button
      widget_control, adbut1, set_uvalue = 1
      widget_control, adbut2, set_uvalue = 0
    end else begin 
      widget_control, adbut2, /set_button
      widget_control, adbut2, set_uvalue = 1
      widget_control, adbut1, set_uvalue = 0
    end
    if os.bdptopt eq 0 then begin
      widget_control, adbut4, /set_button
      widget_control, adbut4, set_uvalue = 1
      widget_control, adbut3, set_uvalue = 0
    end else begin
      widget_control, adbut3, /set_button
      widget_control, adbut3, set_uvalue = 1
      widget_control, adbut4, set_uvalue = 0
    end
    if os.axop eq 0 then begin
      widget_control, adbut6, /set_button
      widget_control, adbut6, set_uvalue = 1
      widget_control, adbut5, set_uvalue = 0 
    end else begin
      widget_control, adbut5, /set_button
      widget_control, adbut5, set_uvalue = 1
      widget_control, adbut6, set_uvalue = 0 
    end
    if os.adif eq 0 then begin
      widget_control, adbut7, /set_button
      widget_control, adbut7, set_uvalue = 1
      widget_control, adbut8, set_uvalue = 0 
    end else begin
      widget_control, adbut8, /set_button
      widget_control, adbut8, set_uvalue = 1
      widget_control, adbut7, set_uvalue = 0 
    end

                ;**** widget for Burgess analysis option ****

  brow1 = widget_base(gr2base,/row)
  brow2 = widget_base(gr2base,/row)
  brow2c = widget_base(brow2,/column,/exclusive)
  cvalid = widget_label(brow1,value = '  C-Value :           ',font=font)
    if os.bcval eq -1 then begin
      tag = ''
    end else begin
      tag = strtrim(string(os.bcval,format='(e8.2)'),2)
    end
    butxt1 = widget_text(brow1,value = tag,$
                        /edit,xsize = 8,font=font)
    pinfid=widget_button(brow2c,value='Insert Non-dipole Point at Infinity : ',$
                         font=font,/no_release)
      bulbl2 = widget_label(brow2,value = 'Y-val',font=font)
      butxt3 = widget_text(brow2,value = '',/edit,xsize = 8,font=font)
      widget_control, bulbl2, sensitive = 0
      widget_control, butxt3, sensitive = 0
      widget_control, pinfid, set_uvalue = 0

                ;**** widget for graphics selection ****

  base = widget_base(cwid,/row,/frame)
  grpid = cw_101_gr_sel(base, /SIGN, OUTPUT='Graphical Output', 	$
			 DEVLIST=devlist, VALUE=gr101val, FONT=font 	$
			)

		;**** Widget for text output ****

  outfval = { OUTBUT:os.texout, APPBUT:-1, REPBUT:os.texrep, 		$
	      FILENAME:os.texdsn, DEFNAME:os.texdef, MESSAGE:os.texmes, $
              ROOT:root }
  base = widget_base(cwid,/row,/frame)
  paperid = cw_adas_outfile(base, OUTPUT='Text Output', 		$
                        VALUE=outfval, FONT=font)

		;**** Error message ****

  messid = widget_label(cwid,value=' ',font=font)

		;**** add the exit buttons ****

  base = widget_base(cwid,/row)
  menufile = bitfile + '/menu.bmp'
  read_X11_bitmap, menufile, bitmap1
  outid = widget_button(base, value=bitmap1)          ;menu button
  cancelid = widget_button(base,value='Cancel',font=font)
  doneid = widget_button(base,value='Done',font=font)
  
		;**** create a state structure for the pop-up ****
		;**** window.                                 ****

  new_state = { GRPID:grpid, PAPERID:paperid, OUTID:outid,		$
		CANCELID:cancelid, DONEID:doneid, MESSID:messid, 	$
                ADBUT1:adbut1, ADBUT2:adbut2, ADBUT3:adbut3, 		$
                ADBUT4:adbut4, ADBUT5:adbut5, ADBUT6:adbut6, 		$
                ADBUT7:adbut7, ADBUT8:adbut8, BUTXT1:butxt1, 		$
                PINFID:pinfid, BUTXT3:butxt3, ADASBUT:adasbut, 		$ 
                ANOPT:os.anopt, BURGBUT:burgbut, GR1BASE:gr1base, 	$
                GR2BASE:gr2base, ACTBASE:actbase, BULBL2:bulbl2 , 	$
                TYPE:type }

                ;**** Save initial state structure ****

  widget_control, parent, set_uvalue=new_state, /no_copy

  RETURN, cwid

END

