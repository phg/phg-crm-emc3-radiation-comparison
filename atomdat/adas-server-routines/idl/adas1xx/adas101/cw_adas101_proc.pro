; Copyright (c) 1995, Strathclyde University.
; SCCS info: Module @(#)$Header: /home/adascvs/idl/adas1xx/adas101/cw_adas101_proc.pro,v 1.9 2004/07/06 12:27:27 whitefor Exp $ Date $Date: 2004/07/06 12:27:27 $
;+
; PROJECT:
;       ADAS IBM MVS to   UNIX conversion
;
; NAME:
;	CW_ADAS101_PROC()
;
; PURPOSE:
;	Produces a widget for ADAS101 processing options/input.
;
; EXPLANATION:
;	This function creates a compound widget consisting of :-
;	   a text widget holding an editable 'Run title', 
;	   the dataset name/browse widget cw_adas_dsbr,
;          a template compound widget for adding information about 
;          the transiton, 
;	   a table widget for temperature/density data cw_adas_table,
;	   buttons to enter default values into the table, 
;	   a message widget, a 'Cancel' and a 'Done' button, and
;          energy units options buttons.
;
;	The compound widgets included in this widget are self managing.
;	This widget only manages events from the two 'Defaults' buttons,
;	the 'Cancel' button and the 'Done' button, and from the energy units
;       buttons.
;
;	This widget only generates events for the 'Done' and 'Cancel'
;	buttons.
;
; USE:
;	This widget is specific to ADAS101, see adas101_proc.pro
;	for use.
;
; INPUTS:
;	TOPPARENT- Long integer, ID of parent widget.
;
;	DSFULL	- String; The full system file name of the input 
;		  dataset selected by the user for processing.
;
;	LCHOICE - Integer; The archiving choice of the user, see 
;                 a1spf0.pro
;
;	NTDIM	- Integer; maximum number of temperatures allowed.
;
;	NEDIM	- Integer; maximum number of energies allowed.
;
;	NXDIM	- Integer; maximum number of x-sections allowed.
;
;	TEMP	- float array; The temperature values (K).
;
;	ENG	- float array; The energy values (Ryd).
;
;	XSECT	- float array; The x-section values (X).
;
;       BITFILE - String; the path to the directory containing bitmaps
;                 for the 'escape to series menu' button.
;
; OPTIONAL INPUTS:
;	None
;
; OUTPUTS:
;	The return value of this function is the ID of the declared widget.
;
; OPTIONAL OUTPUTS:
;	None
;
; KEYWORD PARAMETERS:
;	PROCVAL	- A structure which determines the initial settings of
;		  the processing options widget.
;
;		  The default PROCVAL is created thus;
;      temp_arr = fltarr(ntdim)
;      xsect_arr = fltarr(nxdim)
;      eng_arr = fltarr(nedim)
;      ifout_arr = intarr(3)
;     ps = { 			 		  $
;                new    : 0,                      $
;                ifout  : ifout_arr,              $
;                ldfit  : 0,                      $
;                maxt   : 0,                      $
;                maxx   : 0,                      $
;                maxe   : 0,                      $
;                tin    : temp_arr,               $
;                xin    : xsect_arr,              $
;                ein    : eng_arr,                $
;                z      : 0,                      $
;                zo     : 0,                      $
;                zeff   : 0,                      $
;                type   : 0,                      $
;                Acoeff : 0.,                     $
;                form   : 'A',                    $
;                lindx  : 0,                      $
;                uindx  : 0,                      $
;                lstwt  : 0,                      $
;                ustwt  : 0,                      $
;                leng   : 0.,                     $
;                ueng   : 0.,                     $
;                engunit: 1                       $
;              }
;		NEW     Flag which defines whether or not default values
;			exist or not. (< 0 if no values exist)
;		IFOUT   Array containing three values indicating which units 
;                       are being used 
;		LDFIT   
;		MAXT    Number of temperature values entered/refreshed
;		MAXX    Number of x-sections values entered/refreshed
;		MAXE    Number of energy values entered/refreshed
;		TIN     User supplied temperature values.
;		EIN 	User supplied energy values.
;		XIN 	User supplied x-section values
;               Z       Ion Charge
;               Z0      Nuclear Charge
;               ZEFF    Ion Charge+1
;		TYPE    Transition Type; see cw_adas101_out.pro
;		ACOEFF  The Transition Probability 
;               FORM    The form of the Acoeff (always converted to 'A')
;               LINDX   Lower Index
;               UINDX   Upper Index
;               LSTWT   Lower level statistical weight
;               USTWT   Upper levewl statistical wieght
;               LENG    Lower level energy
;               UENG    Upper level energy
;               ENGUNIT The energy units (0-cm-1,2-Rydberg)
;
;	UVALUE	- A user value for the widget. Default 0.
;
;	FONT_LARGE - The name of a larger font.  Default current system
;		     font
;
;	FONT_SMALL - The name of a smaller font. Default current system
;		     font.
;
;	EDIT_FONTS - A structure of two fonts used in the table editor
;		     adas_edtab.pro, {font_norm:'',font_input:''}.  Defaults
;		     to current system font.
;
;	NUM_FORM   - String; Numeric format to use in tables.  Default
;			'(E10.3)'
;		     
;
; CALLS:
;	POPUP		Popup warning window with buttons.
;	CW_ADAS_DSBR	Dataset name and comments browsing button.
;	CW_ADAS_TABLE	Adas data table widget.
;       CW_ADAS_TMPL         Transition Information
;	See side effects for other related widget management routines.
;
; SIDE EFFECTS:
;
;	The following widget management routines are included in this file;
;	PROC101_GET_VAL()	Returns the current PROCVAL structure.
;	PROC101_EVENT()		Process and issue events.
;	
; CATEGORY:
;	Compound Widget
;
; WRITTEN:
;       David Brooks, Univ.of Strathclyde, Ext.4213/4205
;       Used cw_adas201_proc.pro as framework and extensively modified it.
; MODIFIED:
;	1.1	David Brooks	
;		First release
;	1.2	Tim Hammond (Tessella Support Services plc)	
;		Tidied code up
;	1.3	Tim Hammond
;		Added BUTTON keyword to cw_adas_dsbr call
;	1.4	Tim Hammond
;		Added flexible use of fonts and table size depending on
;		machine.
;	1.5	William Osborn
;		Added menu button
;	1.6	William Osborn
;		Changed cm-1 <-> Rydberg event code so that blank selections
;		are ignored rather than causing errors
;	1.7	David H.Brooks
;		Corrected error in unit conversions                         
;	1.8	Hugh Summers
;		Corrected error in f-s-A conversions 
;	1.9	Richard Martin
;		Changed call to cw_tmpl.pro to cw_adas_tmpl.pro.                        
;
; VERSION:
;	1.1	02-07-95     
;	1.2	11-07-95      
;	1.3	13-07-95
;	1.4	27-02-96
;	1.5	30-05-96
;	1.6	28-06-96
;	1.7	20-11-96
;	1.8	08-05-99
;	1.9	15-11-00
;
;-
;-----------------------------------------------------------------------------

FUNCTION proc101_get_val, id

                ;**** Return to caller on error ****
  ON_ERROR, 2

		 ;***************************************
                 ;****     Retrieve the state 	     ****
		 ;**** Get first_child widget id     ****
		 ;**** because state is stored there ****
		 ;***************************************

  first_child = widget_info(id, /child)
  widget_control, first_child, get_uvalue = state


		;****************************************************
		;**** Get new temperature data from table widget ****
		;****************************************************

  widget_control,state.tempid,get_value=tempval
  tabledata = tempval.value
  ifout = intarr(3)
    for i = 0,2 do begin
      ifout(i) = tempval.units(i)+(5*i)
    endfor
  ldfit = state.ldfit

		;**** Copy out temperature values ****

  ein = dblarr(state.nedim)
  xin = dblarr(state.nxdim)
  tin = dblarr(state.ntdim)
  blanks = where(strtrim(tabledata(ifout(0),*),2) eq '')
		;**** next line assumes that all blanks are ****
		;**** at the end of the columns             ****
  if blanks(0) ge 0 then maxe=blanks(0) else maxe=state.nedim
  ein(0:maxe-1) = double(tabledata(ifout(0),0:maxe-1))
  blanks = where(strtrim(tabledata(ifout(1),*),2) eq '')
  if blanks(0) ge 0 then maxx=blanks(0) else maxx=state.nxdim
  xin(0:maxx-1) = double(tabledata(ifout(1),0:maxx-1))
  blanks = where(strtrim(tabledata(ifout(2),*),2) eq '')
  if blanks(0) ge 0 then maxt=blanks(0) else maxt=state.ntdim
  tin(0:maxt-1) = double(tabledata(ifout(2),0:maxt-1))

		;**** Fill out the rest with zero ****

  if maxt lt state.ntdim then begin
    tin(maxt:state.ntdim-1) = double(0.0)
  endif
  if maxx lt state.nxdim then begin
    xin(maxx:state.nxdim-1) = double(0.0)
  endif 
  if maxe lt state.nedim then begin
    xin(maxe:state.nedim-1) = double(0.0)
  endif 

                ;**** get edits from template widget ****

  widget_control,state.infoid,get_value=values

		;***********************************************
		;**** write selected values to PS structure ****
		;***********************************************

     ps = { 			 		 $
                new    : 0,                      $
		ifout  : ifout,			 $
		ldfit  : ldfit, 		 $
		maxt   : maxt,			 $
		maxx   : maxx,			 $
                maxe   : maxe,                   $
                tin    : tin,       		 $
                xin    : xin,       		 $
                ein    : ein,                    $
                z      : values.z,               $
                zo     : values.zo,              $
                zeff   : values.zeff,            $
                type   : values.type,            $
                Acoeff : values.Acoeff,          $
                form   : values.form,            $
                lindx  : values.lindx,           $
                uindx  : values.uindx,           $
                lstwt  : values.lstwt,           $
                ustwt  : values.ustwt,           $
                leng   : values.leng,            $
                ueng   : values.ueng,            $
                engunit: state.ps.engunit        $
              }
   
  widget_control, first_child, set_uvalue=state, /no_copy

  RETURN, ps

END

;-----------------------------------------------------------------------------

FUNCTION proc101_event, event

                ;**** Base ID of compound widget ****

  parent=event.handler

		;**********************************************
                ;**** Retrieve the user value state        ****
		;**** Get id of first_child widget because ****
		;**** user value "state" is stored there   ****
		;**********************************************

  first_child = widget_info(parent, /child)
  widget_control, first_child, get_uvalue=state,/no_copy

		;*********************************
		;**** Clear previous messages ****
		;*********************************

  widget_control,state.messid,set_value=' '

                ;************************
                ;**** Process events ****
                ;************************

  cASE event.id OF

		;*************************************
		;**** Default temperature button ****
		;*************************************

    state.deftid: begin

		;**** popup window to confirm overwriting current values ****

	action= popup(message='Confirm Overwrite values with Defaults', $
			buttons=['Confirm','Cancel'],font=state.font)

	if action eq 'Confirm' then begin

		;**** Get current table widget value ****

 	   widget_control,state.tempid,get_value=tempval

           units = tempval.units(2)+10   
  	   maxt = state.maxt
 	   if maxt gt 0 then begin

                ;**** default temperature values ****
                ;**** can add up to maxt-1 as required ****

            tvalues = [2.50e+04,5.00e+04,1.25e+05,2.50e+05,5.00e+05,	$
                       1.25e+06,2.50e+06,5.00e+06]
            ndef = n_elements(tvalues)
            deftemp = strarr(ndef)
            for i = 0,ndef-1 do begin
               deftemp(i) = strtrim(string(tvalues(i),format=state.num_form))
            end
            for i = 0,maxt-1 do begin
               if i le ndef-1 then begin
   	         tempval.value(units,i) =  $
			   string(deftemp(i) , format=state.num_form)
               end else begin
                 tempval.value(units,i) = ''
               end
            endfor
 	  end

		;**** Fill out the rest with blanks ****

 	  if maxt lt state.ntdim then begin
            tempval.value(units,maxt:state.ntdim-1) = ''
 	  end

		;**** Set ldfit variable to 0 i.e.  ****
		;**** temperature selected          ****

	  state.ldfit = 0

		;**** Copy new data to table widget ****

 	  widget_control,state.tempid, set_value=tempval
	end

      end

		;*****************************
		;**** cm-1 /rydberg option****
		;*****************************

    state.cmid:begin
      state.ps.engunit = 0
      widget_control, state.tempid, get_value = tempval
      widget_control, state.infoid, get_value = values
      leng = 0.
      z1 = 0
      ueng = leng & eij = leng & Acoeff = leng & s = leng & fij = leng
      lstwt = z1 & ustwt = z1
        leng = values.leng/1.09737d5       
        ueng = values.ueng/1.09737d5      
        eij = ueng-leng
        if state.ps.type eq 1 then begin
            if state.ps.form eq 'f' then begin
              Acoeff = 8.03232e9*eij*eij*state.ps.lstwt*Acoeff/state.ps.ustwt
            end else if state.ps.form eq 'S' then begin
              Acoeff = 2.67744e9*eij*eij*eij*Acoeff/state.ps.ustwt
            end
          s = 3.73491e-10*state.ps.ustwt*Acoeff/(eij*eij*eij)
          fij = 3.333333d-1*eij*s/state.ps.lstwt
        end 
        eng = fltarr(state.ps.maxe,5)
        for ict = 0,state.ps.maxe-1 do begin
	  if strcompress(tempval.value(3,ict), /remove_all) ne '' then begin
            x = float(tempval.value(3,ict))
            eng(ict,0) = eij*(x-1.0d0)                    
            eng(ict,1) = x*eij
            eng(ict,2) = (eij*(x-1.0d0))/(state.ps.zo*state.ps.zo)
            eng(ict,3) = x
            eng(ict,4) = (eij*(x-1.0d0))/(state.ps.zeff*state.ps.zeff)
	    for j=0,4 do begin
	      tempval.value(j,ict) = 		$
	        strtrim(string(eng(ict,j),format=state.num_form),2)
	    endfor
	  endif
	endfor
        xsect = fltarr(state.ps.maxx,5)
	for ict = 0,state.ps.maxx-1 do begin
	  if strcompress(tempval.value(7,ict), /remove_all) ne '' then begin
            y = float(tempval.value(7,ict))
            x = float(tempval.value(3,ict))
            xsect(ict,0) = y/(state.ps.lstwt*eij*x)             
            xsect(ict,1) = y/(state.ps.ustwt*eij*(x-1.0d0))
            xsect(ict,2) = y
            xsect(ict,3) = y*state.ps.zeff*state.ps.zeff
	;**** Dummy units (could be replaced by new one's-Hugh's choice) ****
            xsect(ict,4) = xsect(ict,2)
	    for j=0,4 do begin
	      tempval.value(j+5,ict) = 				$
	        strtrim(string(xsect(ict,j),format=state.neg_form),2)
	    endfor
	  endif
        endfor
      z1 = state.ps.z+1.0d0 
      temp = fltarr(state.ps.maxt,5) 
        for itout = 0,state.ps.maxt-1 do begin
	  if strcompress(tempval.value(10,itout),/remove_all) ne '' then begin
            te = float(tempval.value(10,itout))                   
            temp(itout,0) = te
            temp(itout,1) = te/1.16054d4                 
            temp(itout,2) = te/(z1*z1)
            temp(itout,3) = te/(1.5789d5*eij)
	    for j=0,3 do begin
    	      tempval.value(10+j,itout) = 				$
	        strtrim(string(temp(itout,j),format=state.num_form),2)
	    endfor
    	    tempval.value(14,itout) = 				$
	      strtrim(string(temp(itout,3),format=state.num_form),2)
	  endif
        endfor
     
      widget_control, state.tempid, set_value = tempval      
    end
    state.rydid:begin
      state.ps.engunit = 1
      widget_control, state.tempid, get_value = tempval
      widget_control, state.infoid, get_value = values
      leng = 0.
      z1 = 0
      ueng = leng & eij = leng & Acoeff = leng & s = leng & fij = leng
      lstwt = z1 & ustwt = z1
        leng = values.leng  
        ueng = values.ueng 
        eij = ueng-leng
        if state.ps.type eq 1 then begin
            if state.ps.form eq 'f' then begin
              Acoeff = 8.03232e9*eij*eij*state.ps.lstwt*Acoeff/state.ps.ustwt
            end else if state.ps.form eq 'S' then begin
              Acoeff = 2.67744e9*eij*eij*eij*Acoeff/state.ps.ustwt
            end
          s = 3.73491e-10*state.ps.ustwt*Acoeff/(eij*eij*eij)
          fij = 3.333333d-1*eij*s/state.ps.lstwt
        end 
        eng = fltarr(state.ps.maxe,5)
        for ict = 0,state.ps.maxe-1 do begin
	  if strcompress(tempval.value(3,ict), /remove_all) ne '' then begin
            x = float(tempval.value(3,ict))
            eng(ict,0) = eij*(x-1.0d0)                   
            eng(ict,1) = x*eij
            eng(ict,2) = (eij*(x-1.0d0))/(state.ps.zo*state.ps.zo)
            eng(ict,3) = x
            eng(ict,4) = (eij*(x-1.0d0))/(state.ps.zeff*state.ps.zeff)
	    for j=0,4 do begin
	      tempval.value(j,ict) = 		$
	        strtrim(string(eng(ict,j),format=state.num_form),2)
	    endfor
          endif
	endfor
        xsect = fltarr(state.ps.maxx,5)
        for ict = 0,state.ps.maxx-1 do begin
	  if strcompress(tempval.value(7,ict), /remove_all) ne '' then begin
            y = float(tempval.value(7,ict))
            x = float(tempval.value(3,ict))
            xsect(ict,0) = y/(state.ps.lstwt*eij*x)            
            xsect(ict,1) = y/(state.ps.ustwt*eij*(x-1.0d0))
            xsect(ict,2) = y
            xsect(ict,3) = y*state.ps.zeff*state.ps.zeff
            xsect(ict,4) = xsect(ict,2)
	    for j=0,4 do begin
	      tempval.value(j+5,ict) = 				$
	        strtrim(string(xsect(ict,j),format=state.neg_form),2)
	    endfor
	  endif
        endfor
      z1 = state.ps.z+1.0d0 
      temp = fltarr(state.ps.maxt,5) 
        for itout = 0,state.ps.maxt-1 do begin
	  if strcompress(tempval.value(10,itout),/remove_all) ne '' then begin
            te = float(tempval.value(10,itout))                     
            temp(itout,0) = te
            temp(itout,1) = te/1.16054d4                 
            temp(itout,2) = te/(z1*z1)
            temp(itout,3) = te/(1.5789d5*eij)
	    for j=0,3 do begin
    	      tempval.value(10+j,itout) = 				$
	        strtrim(string(temp(itout,j),format=state.num_form),2)
	    endfor
    	    tempval.value(14,itout) = 				$
	      strtrim(string(temp(itout,3),format=state.num_form),2)
	  endif
        endfor

      widget_control, state.tempid, set_value = tempval
    end
		;***********************
		;**** Cancel button ****
		;***********************

    state.cancelid: new_event = {ID:parent, TOP:event.top, 		$
				HANDLER:0L, ACTION:'Cancel'}

		;*********************
		;**** Done button ****
		;*********************

    state.doneid: begin

		;***************************************
		;**** Check all user input is legal ****
		;***************************************

	error = 0

        	;********************************************
		;*** Have to restore "state" before calls ***
		;*** to proc101_get_val so it can be used ***
		;*** there.				  ***
		;********************************************
		
	widget_control, first_child, set_uvalue=state, /no_copy

	widget_control,event.handler,get_value=ps

		;*** reset state variable ***

	widget_control, first_child,get_uvalue=state, /no_copy

		;**** check temp/density values entered ****

	if error eq 0 and ps.maxt eq 0 then begin
	  error = 1
	  message='Error: No temperatures entered.'
        end
	if error eq 0 and ps.maxx eq 0 then begin
          error = 1
          message='Error: No x-sects entered.'
        end
	if error eq 0 and ps.maxe eq 0 then begin
          error = 1
          message='Error: No energies entered.'
        end


		;**** return value or flag error ****

	if error eq 0 then begin
	  new_event = {ID:parent, TOP:event.top, HANDLER:0L, ACTION:'Done'}
        end else begin
	  widget_control,state.messid,set_value=message
	  new_event = 0L
        end

      end

		;*********************
                ;**** Menu button ****
		;*********************

        state.outid: begin
            new_event = {ID:parent, TOP:event.top, HANDLER:0L,          $
                         ACTION:'Menu'}
        end

    ELSE: new_event = 0L

  ENDCASE

		;*********************************************
		;*** make "state" available to other files ***
		;*********************************************

  widget_control, first_child, set_uvalue=state,/no_copy

  RETURN, new_event
END

;-----------------------------------------------------------------------------

function cw_adas101_proc, topparent, dsfull, lchoice, nedim, nxdim, 	$
                          ntdim, eng, xsect, temp, bitfile, 		$
			  procval=procval, 				$
                          uvalue=uvalue, font_large=font_large, 	$
                          font_small=font_small, edit_fonts=edit_fonts, $
                          num_form=num_form

		;**** Set defaults for keywords ****

  if not (keyword_set(uvalue)) then uvalue = 0
  if not (keyword_set(font_large)) then font_large = ''
  if not (keyword_set(font_small)) then font_small = ''
  if not (keyword_set(edit_fonts)) then 				$
				edit_fonts = {font_norm:'',font_input:''}
  if not (keyword_set(num_form)) then num_form = '(E10.4)'

  if not (keyword_set(procval)) then begin     
     ps = { 			 		 $
                new    : new,                    $
		ifout  : ifout,			 $
		ldfit  : ldfit, 		 $
		maxt   : maxt,			 $
		maxx   : maxx,			 $
                maxe   : maxe,                   $
                tin    : tin,       		 $
                xin    : xin,       		 $
                ein    : ein,                    $
                z      : z,                      $
                zo     : zo,                     $
                zeff   : zeff,                   $
                type   : type,                   $
                Acoeff : Acoeff,                 $
                form   : form,                   $
                lindx  : lindx,                  $
                uindx  : uindx,                  $
                lstwt  : lstwt,                  $
                ustwt  : ustwt,                  $
                leng   : leng,                   $
                ueng   : ueng,                   $
                engunit: engunit                 $
              }
  endif else begin
     ps = { 			 		 $
                new    : procval.new,            $
		ifout  : procval.ifout,	         $
		ldfit  : procval.ldfit,          $
		maxt   : procval.maxt,		 $
		maxx   : procval.maxx,		 $
                maxe   : procval.maxe,           $
                tin    : procval.tin,       	 $
                xin    : procval.xin,       	 $
                ein    : procval.ein,            $
                z      : procval.z,              $
                zo     : procval.zo,             $
                zeff   : procval.zeff,           $
                type   : procval.type,           $
                Acoeff : procval.Acoeff,         $
                form   : procval.form,           $
                lindx  : procval.lindx,          $
                uindx  : procval.uindx,          $
                lstwt  : procval.lstwt,          $
                ustwt  : procval.ustwt,          $
                leng   : procval.leng,           $
                ueng   : procval.ueng,           $
                engunit: procval.engunit         $
              }

  endelse

                ;*********************************************************
                ;**** Modify certain parameters and results depending ****
                ;**** on the machine being used                       ****
                ;*********************************************************

  machine = GETENV('TARGET_MACHINE')
  if machine eq 'HPUX' then begin
    y_size = 4
    large_font = font_small
  endif else begin
    y_size = 6
    large_font = font_large
  endelse

		;****************************************************
		;**** The adas table widget requires data to be *****
		;**** input as strings, so all the numeric data *****
		;**** has to be written into a string array.    *****
		;**** cols.1,2,3,4,5 - energy units             *****
		;**** cols.6,7,8,9,10 - x-section units         *****
		;**** cols.11,12,13,14,15 - Temperature units   *****
		;****************************************************

  maxt = ps.maxt  
  maxx = ps.maxx
  maxe = ps.maxe
  neg_form='(E11.4)'
  tabledata = strarr(15,nxdim) 

  if (maxt gt 0) then begin
    

    tabledata(0,0:maxe-1) = 						$
	        strtrim(string(eng(0:maxe-1,0),format=num_form),2)
    tabledata(1,0:maxe-1) = 						$
		strtrim(string(eng(0:maxe-1,1),format=num_form),2)
    tabledata(2,0:maxe-1) = 						$
                strtrim(string(eng(0:maxe-1,2),format=num_form),2)
    tabledata(3,0:maxe-1) = 						$
                strtrim(string(eng(0:maxe-1,3),format=num_form),2)
    tabledata(4,0:maxe-1) = 						$
                strtrim(string(eng(0:maxe-1,4),format=num_form),2)
    tabledata(5,0:maxx-1) = 						$
	        strtrim(string(xsect(0:maxx-1,0),format=num_form),2)
    tabledata(6,0:maxx-1) = 						$
		strtrim(string(xsect(0:maxx-1,1),format=neg_form),2)
    tabledata(7,0:maxx-1) = 						$
                strtrim(string(xsect(0:maxx-1,2),format=num_form),2)     
    tabledata(8,0:maxx-1) = 						$
	        strtrim(string(xsect(0:maxx-1,3),format=num_form),2)
    tabledata(9,0:maxx-1) = 						$
	        strtrim(string(xsect(0:maxx-1,4),format=num_form),2)
    tabledata(10,0:maxt-1) = 						$
	        strtrim(string(temp(0:maxt-1,0),format=num_form),2)
    tabledata(11,0:maxt-1) = 						$
		strtrim(string(temp(0:maxt-1,1),format=num_form),2)
    tabledata(12,0:maxt-1) = 						$
                strtrim(string(temp(0:maxt-1,2),format=num_form),2)
    tabledata(13,0:maxt-1) = 						$
                strtrim(string(temp(0:maxt-1,3),format=num_form),2)
    tabledata(14,0:maxt-1) = 						$
                strtrim(string(temp(0:maxt-1,3),format=num_form),2)

	;**** fill rest of table with blanks ****

    if maxe lt nedim then tabledata(0:4,maxe:nedim-1) = ' '
    if maxx lt nxdim then tabledata(5:9,maxx:nxdim-1) = ' '
    if maxt lt ntdim then tabledata(10:14,maxt:ntdim-1) = ' '
  end

        ;**** default blanks everywhere ****

  if lchoice eq 0 or lchoice eq 3 then begin
   if maxe lt nedim then begin
    blanks = where(eng eq 0.0) 
    tabledata(0:4,blanks) = ' ' 
    tabledata(0:4,maxe:nedim-1) = ' '
   end
   if maxx lt nxdim then begin
    blanks = where(xsect eq 0.0) 
    tabledata(5:9,blanks) = ' ' 
    tabledata(5:9,maxx:nxdim-1) = ' '
   end
   if maxt lt ntdim then begin
    blanks = where(temp eq 0.0) 
    tabledata(10:14,blanks) = ' ' 
    tabledata(10:14,maxt:nedim-1) = ' '
   end
  
  end

		;********************************************************
		;**** Create the 101 Processing options/input window ****
		;********************************************************

		;**** create titled base widget ****

  parent = widget_base(topparent, UVALUE = uvalue, 			$
			title = 'ADAS101 PROCESSING OPTIONS', 		$
			EVENT_FUNC = "proc101_event", 			$
			FUNC_GET_VALUE = "proc101_get_val", 		$
			/COLUMN)

		;******************************************************
		;**** Create a dummy widget just to hold value of *****
		;**** "state" variable so as not to get confused  *****
		;**** with any other values. Adopt IDL practice   *****
		;**** of using first child widget                 *****
		;******************************************************

  first_child = widget_base(parent)

  topbase = widget_base(first_child,/column)

		;*******************************************************
		;**** add run title, dataset name and browse button ****
		;*******************************************************

  rc = cw_adas_dsbr(topbase, dsfull, font=large_font,			$
                    button='Browse Index')


  infoid = cw_adas_tmpl(topbase,ntdim,nxdim,nedim,				$
                   procval=procval,font_large=large_font,		$
                   font_small=font_small)

                ;***************************************************
                ;****       cm-1 & Rydberg toggle buttons       ****
                ;***************************************************

  newrow = widget_base(topbase,/row,/frame)
  newcol = widget_base(newrow,/column)
  nextrow = widget_base(newrow,/row,/exclusive)
  englbl = widget_label(newcol,value=' Select Energy Form : ',font=large_font)
  cmid = widget_button(nextrow, value = 'cm -1', font=large_font,/no_release)
  rydid = widget_button(nextrow,value = 'Rydbergs',font=large_font,/no_release)
  if ps.engunit eq 0 then begin
    widget_control, cmid, /set_button
  end else begin
    widget_control, rydid, /set_button
  end

		;**********************
		;**** Another base ****
		;**********************

  tablebase = widget_base(parent,/row)

		;************************************************
		;**** base for the table and defaults button ****
		;************************************************

  base = widget_base(tablebase,/column,/frame)

  colhead = ['Input Energy','Input X-sect','Output Temp.']


		;********************************************
		;**** convert FORTRAN index to IDL index ****
		;********************************************

  units = intarr(3)
  units(0) = ps.ifout(0)
  units(1) = ps.ifout(1)-5
  units(2) = ps.ifout(2)-10    

  unitname = [['Ej*k^2     ','Ei*k^2     ','Ej*(k/Z0)^2','X           ',$
               'Ej*(k/zeff)^2'],['exc:pi*a0^2','dex:pi*a0^2',		$
               'Omega      ','Scaled Omega','Hughs choice.'],['Kelvin     ',$
               'eV         ','Scaled     ','Reduced     ','Dummy       .']]

		;**********************************************************
		;**** three columns in the table and 15 sets of units. ****
		;**** column 1 switches between sets 0,1,2,3 & 4       ****
		;**** in the input array tabledata as the units change.****
		;**** Similarly for columns 2 and 3                    ****
		;**********************************************************

  unitind = [[0,1,2,3,4],[5,6,7,8,9],[10,11,12,13,14]]
  unitstitle = ['Energy units','X-section units','Temperature units']


		;****************************
		;**** table of data   *******
		;****************************

  tempid = cw_adas_table(base, tabledata, units, unitname, unitind, 	$
			UNITSTITLE = unitstitle, 			$
                        COLHEAD=colhead, 				$
			ORDER = [1,0,1], /DIFFLEN, 			$ 
			LIMITS = [1,0,1], CELLSIZE = 12, 		$
			/SCROLL, ytexsize=y_size, NUM_FORM = num_form, 	$
			FONTS = edit_fonts, FONT_LARGE = large_font, 	$
			FONT_SMALL = font_small)

		;*************************
		;**** Default buttons ****
		;*************************

  tbase = widget_base(base, /column)
  deftid = widget_button(tbase,value=' Default Temperature Values  ',	$
		font=large_font)

		;**** Error message ****

  messid = widget_label(parent,font=large_font, 			$
  value='Edit the processing options data and press Done to proceed')

		;**** add the exit buttons ****

  base = widget_base(parent,/row)
  menufile = bitfile + '/menu.bmp'
  read_X11_bitmap, menufile, bitmap1
  outid = widget_button(base, value=bitmap1)          ;menu button
  cancelid = widget_button(base,value='Cancel',font=large_font)
  doneid = widget_button(base,value='Done',font=large_font)
  
		;**** create a state structure for the pop-up ****
		;**** 		     window.                  ****


   new_state = {messid:messid,          $
		deftid:deftid,		$
		tempid:tempid, 		$
		cancelid:cancelid,	$
		doneid:doneid, 		$
		outid:outid,		$
                cmid:cmid,              $
                rydid:rydid,            $
		dsfull:dsfull,		$
		ntdim:ntdim,		$
		nxdim:nxdim,		$
                nedim:nedim,            $
		maxt:maxt, 		$
		maxx:maxx, 		$
                maxe:maxe,              $
		ldfit:ps.ldfit,		$
		font:font_large,	$
                infoid:infoid,          $
		num_form:num_form,      $
		neg_form:neg_form,      $
                ps:ps                   $
	      }

                 ;**** Save initial state structure ****

   widget_control, first_child, set_uvalue=new_state,/no_copy

  RETURN, parent

END

