; Copyright (c) 1995 Strathclyde University .
; SCCS info: Module @(#)$Header: /home/adascvs/idl/adas1xx/adas101/cw_adas_tmpl.pro,v 1.3 2004/07/06 13:03:44 whitefor Exp $ Date $Date: 2004/07/06 13:03:44 $
;+
; NAME: 
;       CW_ADAS_TMPL
;
; PURPOSE:
;       To display an editable template containing information about 
;       the transition for analysis
;
; INPUTS:
;       PARENT - The ID of the parent widget.
;
;       NTDIM  - The maximum number of temperatures allowed.
;
;       NEDIM  - The maximum number of energies allowed.
;
;       NXDIM  - The maximum number of x-sections allowed
;
; KEYWORD PARAMETERS:
;       PROCVAL - A structure containing the information about
;                 the transition (see cw_adas101_proc.pro for
;                 a complete description).
;
;	UVALUE - Supplies the user value for the widget.
;
;       FONT_LARGE - The name of a larger font.
;
;       FONT_SMALL - The name of a smaller font.
;
; OUTPUTS:
;       The ID of the created widget is returned.
;       PROCVAL is updated if alterations are made.
;
; COMMON BLOCKS:
;	None.
;
; SIDE EFFECTS:
;       None
;
; PROCEDURE:
;       ADAS_TMPL_SET_VALUE    Sets the initial widget value.
;       ADAS_TMPL_GET_VALUE    Gets the final widget value.
;       ADAS_TMPL_EV           Processes events.
;
; WRITTEN:
;       David H.Brooks, Univ.of Strathclyde, Ext.4213/4205
;
; MODIFICATION HISTORY:
;	1.1	David H.Brooks
;		First release
;	1.2	Hugh Summers
;		Corrected confusion between nuclear and ion charge
;	1.3	Richard Martin
;		Renamed from cw_tmpl.pro to cw_adas_tmpl.pro
; VERSION:
;       1.1  	02-July-1995
;       1.2  	18-May-1999
;	1.3	11-20-2000
;
;-------------------------------------------------------------------------------
PRO adas_tmpl_set_value, id, value

        ;**** Return to caller ****
  ON_ERROR, 2

        ;**** Retrieve the state. ****
  stash = WIDGET_INFO(id, /CHILD)
  WIDGET_CONTROL, stash, GET_UVALUE=state, /NO_COPY

	;**** Set the value here.****
  widget_control,state.zid2,$
    set_value=strcompress(string(value.z,format='(f6.2)'),/remove_all)
  widget_control,state.zoid2,$
    set_value=strcompress(string(value.zo,format='(f6.2)'),/remove_all)
  widget_control,state.typeid2,$
    set_value=strcompress(string(value.type,format='(i3)'),/remove_all)
  widget_control,state.probid2,$
    set_value=strcompress(string(value.Acoeff,format='(e8.2)'),/remove_all)
  widget_control,state.indxid2,$
    set_value=strcompress(string(value.lindx,format='(i5)'),/remove_all)
  widget_control,state.indxid4,$
    set_value=strcompress(string(value.uindx,format='(i5)'),/remove_all)
  widget_control,state.stwtid2,$
    set_value=strcompress(string(value.lstwt,format='(f5.1)'),/remove_all)
  widget_control,state.stwtid4,$
    set_value=strcompress(string(value.ustwt,format='(f5.1)'),/remove_all)
  widget_control,state.levengid2,$
    set_value=strcompress(string(value.leng,format='(f8.6)'),/remove_all)
  widget_control,state.levengid4,$
    set_value=strcompress(string(value.ueng,format='(f8.6)'),/remove_all)

	;**** Restore the state.****
  WIDGET_CONTROL, stash, SET_UVALUE=state, /NO_COPY

END
;-------------------------------------------------------------------------------
FUNCTION adas_tmpl_get_value, id, value

	;**** Return to caller.****
  ON_ERROR, 2

	;**** Retrieve the structure **** 
  stash = WIDGET_INFO(id, /CHILD)
  WIDGET_CONTROL, stash, GET_UVALUE=state, /NO_COPY

	;**** Get the value here****

  widget_control,state.zid2,get_value=zval
  widget_control,state.zoid2,get_value = zoval
  widget_control,state.typeid2,get_value = typeval
  widget_control,state.probid2,get_value = Aval
  widget_control,state.indxid2,get_value = lindxval
  widget_control,state.indxid4,get_value = uindxval
  widget_control,state.stwtid2,get_value = lstwtval
  widget_control,state.stwtid4,get_value = ustwtval
  widget_control,state.levengid2,get_value = lengval
  widget_control,state.levengid4,get_value = uengval
  if state.bchoice eq 1 then begin
    formval = 'S'
  end else if state.bchoice eq 2 then begin
    formval = 'f'
  end else if state.bchoice eq 3 then begin
    formval = 'A'
  end else if state.bchoice eq 4 then begin
    formval = ''
  end
        ;**** save the structure ****

       ps = { 			                 $
                new    : state.new,              $
		ifout  : state.ifout,		 $
		ldfit  : state.ldfit, 		 $
		maxt   : state.maxt,	         $
		maxx   : state.maxx,	         $
                maxe   : state.maxe,             $
                tin    : state.tin,        	 $
                xin    : state.xin,       	 $
                ein    : state.ein,              $
                z      : long(zval),             $
                zo     : long(zoval),            $
                zeff   : state.zeff,             $
                type   : long(typeval),          $
                Acoeff : float(Aval),            $
                form   : formval   ,             $
                lindx  : long(lindxval),         $
                uindx  : long(uindxval),         $
                lstwt  : float(lstwtval),        $
                ustwt  : float(ustwtval),        $
                leng   : float(lengval),         $
                ueng   : float(uengval),         $
                engunit: state.engunit           $
             }

	;**** Restore the state.****

  WIDGET_CONTROL, stash, SET_UVALUE=state, /NO_COPY


RETURN,ps

END

;-----------------------------------------------------------------------------

FUNCTION adas_tmpl_ev, ev

  parent=ev.handler
	;**** Retrieve the structure ****

  stash = WIDGET_INFO(parent, /CHILD)
  WIDGET_CONTROL, stash, GET_UVALUE=state, /NO_COPY

        ;**** process events ****

  CASE ev.id OF
   
   state.zid2:begin
     widget_control,ev.id,get_value = value
     widget_control,state.zid2,set_value=value(0)
   end
   state.zoid2:begin
     widget_control,ev.id,get_value = value
     widget_control,state.zoid2,set_value = value(0)
   end
   state.typeid2:begin
     widget_control,ev.id,get_value = value
     widget_control,state.typeid2,set_value = value(0)
   end
   state.probid2:begin
     widget_control,ev.id,get_value = value
     widget_control,state.probid2,set_value = value(0)
   end
   state.indxid2:begin
     widget_control,ev.id,get_value = value
     widget_control,state.indxid2,set_value = value(0)
   end
   state.indxid4:begin
     widget_control,ev.id,get_value = value
     widget_control,state.indxid4,set_value = value(0)
   end
   state.stwtid2:begin
     widget_control,ev.id,get_value = value
     widget_control,state.stwtid2,set_value = value(0)
   end
   state.stwtid4:begin
     widget_control,ev.id,get_value = value
     widget_control,state.stwtid4,set_value = value(0)
   end
   state.levengid2:begin
     widget_control,ev.id,get_value = value
     widget_control,state.levengid2,set_value = value(0)
   end
   state.levengid4:begin
     widget_control,ev.id,get_value = value
     widget_control,state.levengid4,set_value = value(0)
   end
   state.sid:begin
     prob = 0.
     leng = state.ps.leng
     ueng = state.ps.ueng

        ;****    check on units and calculate     ****
        ;****other forms of transition probabliity****

       if state.engunit eq 0 then leng = leng/1.09737d5
       if state.engunit eq 0 then ueng = ueng/1.09737d5
         eij = ueng-leng
         widget_control, state.probid2, get_value = value
           if state.bchoice eq 3 then begin
             prob = float(value(0))*state.ps.ustwt/(2.67744e9*eij*eij*eij)
           end else if state.bchoice eq 2 then begin
             prob = $
         8.03232e9*eij*eij*state.ps.lstwt*float(value(0))/state.ps.ustwt
             prob = prob*state.ps.ustwt/(2.67744e9*eij*eij*eij)
           end else begin
             prob = float(value(0))
           end
         widget_control, state.probid2, set_value = $
           strcompress(string(prob,format='(e8.2)'),/remove_all)
     state.bchoice = 1
   end
   state.fid:begin
     prob = 0.
     leng = state.ps.leng
     ueng = state.ps.ueng
       if state.engunit eq 0 then leng = leng/1.09737d5
       if state.engunit eq 0 then ueng = ueng/1.09737d5
         eij = ueng-leng
         widget_control, state.probid2, get_value = value
           if state.bchoice eq 3 then begin
             prob = $
         float(value(0))*state.ps.ustwt/(8.03232e9*eij*eij*state.ps.lstwt)
           end else if state.bchoice eq 1 then begin
             prob = 2.67744e9*eij*eij*eij*float(value(0))/state.ps.ustwt
             prob = prob*state.ps.ustwt/(8.03232e9*eij*eij*state.ps.lstwt)
           end else begin
             prob = float(value(0))
           end
         widget_control, state.probid2, set_value = $
           strcompress(string(prob,format='(e8.2)'),/remove_all)
     state.bchoice = 2
   end
   state.aid:begin
     prob = 0.
     leng = state.ps.leng
     ueng = state.ps.ueng
       if state.engunit eq 0 then leng = leng/1.09737d5
       if state.engunit eq 0 then ueng = ueng/1.09737d5
         eij = ueng-leng
         widget_control, state.probid2, get_value = value
           if state.bchoice eq 1 then begin
             prob = eij*eij*eij*2.67744e9*float(value(0))/state.ps.ustwt
           end else if state.bchoice eq 2 then begin
             prob = $
         8.03232e9*eij*eij*state.ps.lstwt*float(value(0))/state.ps.ustwt
           end else begin
             prob = float(value(0))
           end
         widget_control, state.probid2, set_value = $
           strcompress(string(prob,format='(e8.2)'),/remove_all)
     state.bchoice = 3
   end
  endcase



        ;**** Restore the state structure ****

  WIDGET_CONTROL, stash, SET_UVALUE=state, /NO_COPY

  RETURN, { ID:parent, TOP:ev.top, HANDLER:0L, ACTION:'done' }

END

;-----------------------------------------------------------------------------

FUNCTION cw_adas_tmpl, parent, ntdim,nxdim,nedim,				$
                  procval=procval, font_large=font_large,		$
                  font_small=font_small,UVALUE = uval

  IF (N_PARAMS() EQ 0) THEN MESSAGE, 'Must specify a parent for Cw_ADAS_Tmpl'

  ON_ERROR, 2					;return to caller

	;**** set defaults for keywords ****

  IF NOT (KEYWORD_SET(uval))  THEN uval = 0
  IF NOT (KEYWORD_SET(font_large)) THEN font = ''
  IF NOT (KEYWORD_SET(font_small)) THEN font = ''
  IF NOT (KEYWORD_SET(procval)) THEN begin
     temp_arr = fltarr(ntdim)
     xsect_arr = fltarr(nxdim)
     eng_arr = fltarr(nedim)
     ps = { 			                 $
                new    : 0,                      $
		ifout  : 1,			 $
		ldfit  : 0, 			 $
		maxt   : 0,			 $
		maxx   : 0,			 $
                maxe   : 0,                      $
                tin    : temp_arr,               $
                xin    : xsect_arr,         	 $
                ein    : eng_arr,                $
                z      : z,                      $
                zo     : zo,                     $
                zeff   : zeff,                   $
                type   : type,                   $
                Acoeff : Acoeff,                 $
                form   : form,                   $
                lindx  : lindx,                  $
                uindx  : uindx,                  $
                lstwt  : lstwt,                  $
                ustwt  : ustwt,                  $
                leng   : leng,                   $
                ueng   : ueng,                   $
                engunit: engunit                 $
              }
  endif else begin
      ps = { 			                         $
                new    : procval.new,                    $
		ifout  : procval.ifout,			 $
		ldfit  : procval.ldfit, 	         $
		maxt   : procval.maxt,			 $
		maxx   : procval.maxx,			 $
                maxe   : procval.maxe,                   $
                tin    : procval.tin,             	 $
                xin    : procval.xin,       	         $
                ein    : procval.ein,                    $
                z      : procval.z,                      $
                zo     : procval.zo,                     $
                zeff   : procval.zeff,                   $
                type   : procval.type,                   $
                Acoeff : procval.Acoeff,                 $
                form   : procval.form,                   $
                lindx  : procval.lindx,                  $
                uindx  : procval.uindx,                  $
                lstwt  : procval.lstwt,                  $
                ustwt  : procval.ustwt,                  $
                leng   : procval.leng,                   $
                ueng   : procval.ueng,                   $
                engunit: procval.engunit                 $
              }

  end

        ;**** create the main base ****

  mainbase = WIDGET_BASE(parent, UVALUE = uval, 			$
		EVENT_FUNC = "adas_tmpl_ev", 				$
		FUNC_GET_VALUE = "adas_tmpl_get_value", 			$
		PRO_SET_VALUE = "adas_tmpl_set_value")


    first_child = widget_base(mainbase)
    topbase = widget_base(first_child,/column)

    row1 = widget_base(topbase,/row)
    zoid1 = widget_label(row1,value = ' Nuclear Charge:  ',font=font_large)
    zoid2 = widget_text(row1,/edit,xsize = 8,font = font_small)
    zid1 = widget_label(row1,value = '                 Ion Charge: ',	$
                         font=font_large)
    zid2 = widget_text(row1,/edit,xsize = 8,font = font_small)
 
    row2 = widget_base(topbase,/row,/frame)
    col1 = widget_base(row2,/column)
    col2 = widget_base(row2,/column,/frame)
    row3 = widget_base(col1,/row)
    row4 = widget_base(col2,/row)
    row5 = widget_base(col2,/row)
    c5a = widget_base(row5,/column)
    c5b = widget_base(row5,/row,/exclusive)
    typeid1 = widget_label(row3,value = 'Transition Type: ',font=font_large)
    typeid2 = widget_text(row3,/edit,xsize = 8,font = font_small)
    probid1 = widget_label(row4,value = '   Transition Probability: ',	$
                           font=font_large)
    probid2 = widget_text(row4,$
                        /edit,xsize = 8,font = font_small)
    formid = widget_label(c5a,value = '   Form: ',font=font_large)
    sid = widget_button(c5b,value = 'S',font=font_large,/no_release)
    fid = widget_button(c5b,value = 'f',font=font_large,/no_release)
    aid = widget_button(c5b,value = 'A',font=font_large,/no_release)
 
    col3 = widget_base(topbase,/column,/frame)
    row6 = widget_base(col3,/row)
    row7 = widget_base(col3,/row)
    row8 = widget_base(col3,/row)
    col4 = widget_base(col3,/column,/frame)
    row9 = widget_base(col4,/row)
    row10 = widget_base(col4,/row)
    col5 = widget_base(row10,/column)
    row11 = widget_base(row10,/row,/exclusive)
    lbl1 = widget_label(row6,value = '   Lower Level ',			$
                        font=font_large)
    lbl2 = widget_label(row6,value = '                           Upper Level',$
                        font=font_large)
    indxid1 = widget_label(row7,value = '          Index: ',font=font_large)
    indxid2 = widget_text(row7,/edit,xsize = 8,font = font_small)
    indxid3 = widget_label(row7,value = '                       Index: ',$
                           font=font_large)
    indxid4 = widget_text(row7,/edit,xsize = 8,font = font_small)
    stwtid1 = widget_label(row8,value = '      Stat. Wt.: ',font=font_large)
    stwtid2 = widget_text(row8,/edit,xsize = 8,font = font_small)
    stwtid3 = widget_label(row8,value = '                   Stat. Wt.: ',$
                           font=font_large)
    stwtid4 = widget_text(row8,/edit,xsize = 8,font = font_small)
    levengid1 = widget_label(row9,value = '  Level Energy: ',font=font_large)
    levengid2 = widget_text(row9,/edit,xsize = 8,font = font_small)
    levengid3 = widget_label(row9,value = '                Level Energy: ',$
                             font=font_large)
    levengid4 = widget_text(row9,/edit,xsize = 8,font = font_small)
    bchoice = -1

        ;**** Create the state ****

  state = { ZID2:zid2, ZOID2:zoid2, TYPEID2:typeid2,			$
                PROBID2:probid2, SID:sid, FID:fid, AID:aid,		$
                INDXID2:indxid2, INDXID4:indxid4, STWTID2:stwtid2,	$
                STWTID4:stwtid4, LEVENGID2:levengid2, LEVENGID4:levengid4,$
                BCHOICE:bchoice,FORM:ps.form, NEW:ps.new, IFOUT:ps.ifout,$
                LDFIT:ps.ldfit, $;IBSEL:ps.ibsel,			$
                MAXT:ps.maxt, MAXX:ps.maxx, MAXE:ps.maxe, TIN:ps.tin,	$
                XIN:ps.xin, EIN:ps.ein, ENGUNIT:ps.engunit, ZEFF:ps.zeff,$
                PS:ps }

        ;**** sensitize the appropriate button ****

    if ps.form eq 'S' then begin
      widget_control,state.sid,/set_button
      state.bchoice = 1
    end else if ps.form eq 'f' then begin
      widget_control,state.fid,/set_button
      state.bchoice = 2
    end else if ps.form eq 'A' then begin
      widget_control,state.aid,/set_button
      state.bchoice = 3
    end else if ps.form eq '' then begin
      state.bchoice = 4        ; default
    end

	;**** Save out the initial state structure ****

  WIDGET_CONTROL, WIDGET_INFO(mainbase, /CHILD), SET_UVALUE=state, /NO_COPY
  widget_control, mainbase,set_value = ps

  RETURN, mainbase

END





