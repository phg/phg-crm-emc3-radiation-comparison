; Copyright (c) 1995 Strathclyde University .
; SCCS info: Module @(#)$Header: /home/adascvs/idl/adas1xx/adas101/adas101_in.pro,v 1.3 2010/11/30 07:57:59 mog Exp $ Date $Date: 2010/11/30 07:57:59 $
;+
; PROJECT:
;       ADAS IBM MVS to DEC UNIX conversion
;
; NAME:
;	ADAS101_IN
;
; PURPOSE:
;	IDL ADAS user interface, input file selection.
;
; EXPLANATION:
;	This routine creates and manages a pop-up window which allows
;	the user to select a file from the UNIX file system.  It is
;	intended as a general routine and hence the widget titles may
;	be passed into this routine using keywords.
;
; USE:
;	See the ADAS205 routine b5spf0.pro for an example of how to
;	use this routine.
;
; INPUTS:
;	VAL	- A structure which determines the initial settings of
;		  the dataset selection widget.  VALUE is passed un-modified
;		  through to cw_adas101_in.pro, see that routine for a full
;		  description.
;
; OPTIONAL INPUTS:
;	None
;
; OUTPUTS:
;	VAL	- On output the structure records the final settings of
;		  the dataset selection widget if the user pressed the
;		  'Done' button otherwise it is not changed from input.
;
;	ACT	- A string; indicates the user's action when the pop-up
;		  window is terminated, i.e which button was pressed to
;		  complete the input.  Possible values are 'Done' and
;		  'Cancel'.
;
; OPTIONAL OUTPUTS:
;	None
;
; KEYWORD PARAMETERS:
;	WINTITLE- A title to be used on the banner for the pop-up window.
;		  Intended to indicate which application is running,
;		  e.g 'ADAS 205 INPUT'
;
;	TITLE	- The title to be included in the input file widget, used
;		  to indicate exactly what the required input dataset is,
;		  e.g 'Input COPASE Dataset'
;
;	FONT	- Supplies the font to be used for the interface widgets.
;
; CALLS:
;	CW_ADAS101_IN	Dataset selection widget creation.
;	XMANAGER	Manages the pop=up window.
;	ADAS101_IN_EV	Event manager, called indirectly during XMANAGER
;			event management.
;
; SIDE EFFECTS:
;	XMANAGER is called in /modal mode. Any other widget become
;	inactive.
;
; CATEGORY:
;	Adas system.
;
; WRITTEN:
;       Tim Hammond, Tessella Support Services plc, 11-Jul-1995
;
; MODIFIED:
;	1.1	Tim Hammond
;       	First release - created from adas_in.pro - only
;		difference is that all routines called are ..adas101..
;		rather than simply ..adas.. due to the different
;		nature of the input screen for this program.
;	1.2	William Osborn
;		Added dynlabel procedure for IDL v4.0.1 machines
;	1.3    	Martin O'Mullane
;		Rename common block in_blk to in101_blk.
; VERSION:
;       1.1	11-07-95
;	1.2	30-05-96
;	1.3	30-11-2010
;-

;-----------------------------------------------------------------------------


PRO adas101_in_ev, event

    COMMON in101_blk,action,value

		;**** Find the event type and copy to common ****

    action = event.action

    CASE action OF

		;**** 'Done' button ****

	'Done'  : begin

			;**** Get the output widget value ****

  		  widget_control,event.id,get_value=value 
		  widget_control,event.top,/destroy

         end


		;**** 'Cancel' button ****

	'Cancel': widget_control, event.top, /destroy

    END

END

;-----------------------------------------------------------------------------


PRO adas101_in, val, act, WINTITLE = wintitle, TITLE = title, FONT = font

    COMMON in101_blk,action,value

		;**** Copy value to common ****

    value = val

		;**** Set defaults for keywords ****

    IF NOT (KEYWORD_SET(wintitle)) THEN wintitle = 'ADAS INPUT FILE'
    IF NOT (KEYWORD_SET(title)) THEN title = ''
    IF NOT (KEYWORD_SET(font)) THEN font = ''

		;***************************************
		;**** Pop-up a new widget if needed ****
		;***************************************

                ;**** create base widget ****

    inid = widget_base(TITLE=wintitle,XOFFSET=200,YOFFSET=0)

		;**** Declare output options widget ****

    cwid = cw_adas101_in(inid, VALUE=value, TITLE=title, FONT=font)

		;**** Realize the new widget ****

    dynlabel, inid
    widget_control,inid,/realize

		;**** make widget modal ****

    xmanager,'adas101_in',inid,event_handler='adas101_in_ev',/modal,/just_reg
 
		;**** Return the output value from common ****

    act = action
    val = value

END

