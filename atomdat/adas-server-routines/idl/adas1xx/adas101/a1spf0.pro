; SCCS info: Module @(#)$Header: /home/adascvs/idl/adas1xx/adas101/a1spf0.pro,v 1.3 2004/07/06 10:00:05 whitefor Exp $ Date $Date: 2004/07/06 10:00:05 $
;+
; PROJECT:
;       ADAS IBM MVS to UNIX conversion
;
; NAME:
;	A1SPF0
;
; PURPOSE:
;	IDL user interface and communications with ADAS101 FORTRAN
;	process via pipe.
;
; EXPLANATION:
;	Firstly this routine invokes the part of the user interface
;	used to select the input archive for ADAS101.  When the user's
;	interactions are complete this routine communicates with the
;	ADAS101 FORTRAN application via a UNIX pipe.  Communications
;	are to the FORTRAN subroutine A1SPF0.
;
; USE:
;	The use of this routine is specific to ADAS101, see adas101.pro.
;
; INPUTS:
;	PIPE	- The IDL unit number of the bi-directional pipe to the
;		  ADAS101 FORTRAN process.
;
;	VALUE	- A structure which determines the initial settings of
;		  the archive selection widget.  The initial value is
;		  set in adas101.pro.  VALUE is passed un-modified
;		  through to cw_adas_in.pro, see that routine for a full
;		  description.
;
; OPTIONAL INPUTS:
;	None
;
; OUTPUTS:
;	VALUE	- On output the structure records the final settings of
;		  the dataset selection widget if the user pressed the
;		  'Done' button otherwise it is not changed from input.
;
;	DSFULL	- String; The full system file name of the input 
;		  archive selected by the user for processing.
;
;	REP	- String; Indicates whether the user pressed the 'Done'
;		  or 'Cancel' button on the interface.  The action is
;		  converted to the strings 'NO' and 'YES' respectively
;		  to match up with the existing FORTRAN code.  In the
;		  original IBM ISPF interface REP was the reply to the
;		  question 'DO YOU WANT TO END PROGRAM EXECUTION'.
;
;       LCHOICE - Integer; Records the archiving option selected by the 
;                 user for future processing.
;                 0 - No Archive
;                 1 - Examine Old Archive
;                 2 - Refresh index from Old Archive
;                 3 - create New Archive
;
; OPTIONAL OUTPUTS:
;	None
;
; KEYWORD PARAMETERS:
;	FONT	- The name of the font to be used for the 
;                 interface widgets.
;
; CALLS:
;	ADAS_IN		Pops-up the Archive selection widget.
;
; SIDE EFFECTS:
;       None
;
; CATEGORY:
;	Adas system.
;
; WRITTEN:
;       David H.Brooks, Univ.of Strathclyde, Ext.4213/4205
;
; MODIFIED:
;       1.1	David H. Brooks				    30-Jun-1995
;	1.2	Tim Hammond (Tessella Support Services plc) 11-Jul-1995
;	1.2	Tim Hammond				    11-Jul-1995
; VERSION:
;	1.1	First Release
;	1.2	Tidied code up
;	1.3	Changed call from adas_in to adas101_in as the input
;		screen has quite a different form.
;-
;-----------------------------------------------------------------------------


PRO a1spf0, pipe, value, dsfull, lchoice, rep, FONT=font

		;***********************************
                ;**** Set defaults for keywords ****
		;***********************************

    IF NOT (KEYWORD_SET(font)) THEN font = ''

                ;***********************************
                ;**** Reset value.refresh to '' ****
                ;**** so as to simplify choices ****
                ;***********************************

    value.refresh = ''

		;**********************************
		;**** Pop-up input file widget ****
		;**********************************

    adas101_in, value, action, WINTITLE = 'ADAS 101 INPUT', 		$
	        TITLE = 'Input Archive File', FONT = font

		;********************************************
		;**** Act on the event from the widget   ****
		;**** There are only two possible events ****
		;**** 'Done' and 'Cancel'.               ****
		;********************************************

    if action eq 'Done' then begin
        rep = 'NO'
    endif else begin
        rep = 'YES'
    endelse

		;********************************
		;**** Construct dataset name ****
		;********************************

    dsfull = strcompress(value.rootpath+value.file)

		;*****************************************
                ;**** Set up flag for archive choice  ****
                ;**** 0 (no archive), 1 (old archive) ****
                ;**** 2 (refresh)   , 3 (new archive) ****
		;*****************************************
 
    if lchoice eq -1 then begin
        if value.file eq '' then begin
            lchoice = 0
        endif else begin
            openr, /get_lun, tempunit, dsfull

                ;**** Check on the size of the file  ****
                ;****if just created it will be empty****

            status = fstat(tempunit)
            if status.size ne 0 then begin
                if value.refresh eq '' then begin
                    lchoice = 1
                endif else begin
                    lchoice = 2
                endelse
            endif else begin
                lchoice = 3
            endelse
            free_lun, tempunit
        endelse
    endif

		;*******************************
                ;*** write dsname and choice ***
                ;***      to fortran         ***
		;*******************************

    printf, pipe, format = '(1a3)', rep
    printf, pipe, format = '(i5)', lchoice
    if lchoice ne 0 then begin
        printf, pipe, dsfull
        if lchoice eq 2 then begin
            printf, pipe, format = '(i5)', long(value.refresh) 
        endif
    endif

END
