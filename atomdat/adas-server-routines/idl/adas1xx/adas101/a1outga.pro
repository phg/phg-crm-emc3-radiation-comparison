; Copyright (c) 1995 Strathclyde University .
; SCCS info: Module @(#)$Header: /home/adascvs/idl/adas1xx/adas101/a1outga.pro,v 1.4 2004/07/06 09:59:37 whitefor Exp $ Date $Date: 2004/07/06 09:59:37 $
;+
; PROJECT:
;       ADAS IBM MVS to DEC UNIX conversion & development
;
; NAME:
;       A1OUTGA
;
; PURPOSE:
;       To read omegas and upsilons after fortran processing, popup the 
;       ADAS_GRAPH_EDITOR write any alterations to the fortran and plot the
;       final upsilons.
;
; USE:
;       ADAS101 Specific
;
; INPUTS:
;       PIPE    - The IDL unit number of the bi-directional pipe to the
;                 ADAS101 FORTRAN process.
;
;       LPEND   - Integer, 0 if the user pressed the 'Done' button
;                 or 1 if the user pressed 'Cancel'.  Maps directly onto
;                 the logical variable LPEND in ADAS101 FORTRAN.
;
;       LCHOICE - Integer; Variable to flag user choice of archive
;                 option. 
;                 0 = No Archive
;                 1 = Old Archive
;                 2 = Refresh form Old Archive
;                 3 = New Archive
;
;       OUTVAL -  A Structure which holds the output widget settings see 
;                 a1spf1.pro
;
;       PROCVAL - A Structure which holds the processing widget settings
;                 see a1ispf.pro.
;
;       ROOT -    The path to the user's default archive directory, 
;                 '/home/user/adas/arch101'
;
;       DEVICE -  The name of the current output device.
;
;       DATE -    The date
;
;       HEADER -  The ADAS release header for the final plot.  
;
;       TITLX  -  The datset name, for display on the plot.
;
;       BITFILE - String; the path to the directory containing bitmaps
;                 for the 'escape to series menu' button.
;
; OPTIONAL INPUTS:
;                 None
;
; OUTPUTS:   
;       GOMENU  - Int; flag - set to 1 if user has selected 'escape direct
;                 to series menu , otherwise 0
;
; OPTIONAL OUTPUTS:
;                 None
;
; KEYWORD PARAMETERS:
;       FONT - The name of a font.         
;
; CALLS:
;       ADAS_EDIT_GRAPH - The ADAS version of the interactive Graph Editor
;       ADAS101_PLOT - The plotting procedure for the Upsilon graph
;
; SIDE EFFECTS:
;       Two way communications with the Fortran process via the Unix pipe.
;
; CATEGORY:
;       ADAS System
;
; WRITTEN: 
;       David H.Brooks, Univ.of Strathclyde,  23-06-95
;
; MODIFIED:
;	1.1	David H. Brooks
;		First release
;	1.2	William Osborn
;		Added menu button
;       1.3     William Osborn
;               Added titles to adas_edit_graph
;	1.4	Hugh Summers
; VERSION:
;	1.1	23-06-95
;	1.2	30-05-96
;	1.3	03-10-96
;	1.4	??-05-99
;-
;-----------------------------------------------------------------------------
pro a1outga, pipe, lpend, lchoice, root, outval, procval, device, date, $
             header , titlx, bitfile, gomenu, FONT = font

                   ;**** re-set lpend ****
  lpend = 0
                   ;********************************
                   ;**** read data from fortran ****                   
                   ;****    then alter to DGE   **** 
                   ;****    required forms  &   ****
                   ;**** set up arrays for DGE  ****  
                   ;********************************

  readf, pipe, format = '(1x,i3)', ictn
  readf, pipe, format = '(1x,i3)', itoutn
  indim1 = ictn
  xa = fltarr(indim1)
  ya = xa & apoma = xa 
  indim2 = itoutn
  toa = fltarr(indim2)
  goa = toa
  eij = 0.0
  readf, pipe, format = '(1x,e12.4)', fxc2
  readf, pipe, format = '(1x,e12.4)', fxc3
  readf, pipe, format = '(1x,e12.4)', S
  readf, pipe, format = '(1x,e12.4)', eij
  for i = 0,ictn-1 do begin
    readf, pipe, format = '(1x,e12.4,1x,e12.4,1x,e12.4)', xat, yat, apomat
                 xa(i) = xat
                 ya(i) = yat
                 apoma(i) = apomat
  endfor
  for i = 0,itoutn-1 do begin
    readf, pipe, format = '(1x,e12.4,1x,e12.4)', toat, goat
                 toa(i) = toat
                 goa(i) = goat
  endfor

  dgey = ya/apoma
  dgex = 1-1/xa
  ploty = goa
  plotx = alog10(toa)
                   ;********************************
                   ;**** Popup diagnostic graph ****
                   ;****         editor         ****                   
                   ;********************************
  print1 = 0
  xtit = '1-1/X '
  ytit = 'OM/Approx.OM '
  tit = 'Collision Strength Ratio Graph'
  if outval.grpscal1 eq 1 then begin
    adas_edit_graph, dgex,    dgey,    lpend,   print1,  nsp = 70, $
                     xmax = float(outval.xmax1), xmin = float(outval.xmin1), $
                     ymax = float(outval.ymax1), ymin = float(outval.ymin1), $
                     font = font, xtitle=xtit, ytitle=ytit, title=tit
  end else begin
      adas_edit_graph, dgex,    dgey,    lpend,   print1,  nsp = 70, $
        font = font, xtitle=xtit, ytitle=ytit, title=tit
  end
                   ;**** Re-scale arrays if alterations made ****
    xa = fltarr(n_elements(dgex))
    ya = fltarr(n_elements(dgey))
    apoma = ya
 
                   ;********************************
                   ;****  Calculate x-sections  ****
                   ;********************************

      for i = 0,n_elements(dgex)-1 do begin
        xa(i) = 1/(1-dgex(i))
      endfor
    for i = 0,n_elements(dgey)-1 do begin
      if procval.type eq 1 then begin
        apoma(i) = fxc3*S*ALOG(xa(i)+fxc2)
      end else if procval.type eq 2 and outval.axop eq 1 then begin
        apoma(i) = fxc3+fxc2/xa(i)
      end else if procval.type eq 3 and outval.axop eq 1 then begin
        apoma(i) = fxc3/(xa(i)+fxc2)^2
      end else if procval.type eq 4 then begin
        apoma(i) = 0.0d0
      end else if procval.type eq 0 then begin
        apoma(i) = 0.0d0
      end
    endfor    
                   ;**** get new array values ****                 

      for i = 0,n_elements(dgey)-1 do begin
        ya(i) = apoma(i)*dgey(i)
      endfor 
      ictn = n_elements(dgey)

                   ;****print new values to pipe****
                   ;****   for re-analysing     ****    

    printf, pipe, ictn
    printf, pipe, lpend
    if lpend eq 2 then begin
      
      z1=procval.z+1
      zeff=procval.zeff
      zo=procval.zo
      for i = 0,ictn-1 do begin
        xt=xa(i)
        if procval.ifout(0) eq 0 then xa(i)=eij*(xa(i)-1.0)
        if procval.ifout(0) eq 1 then xa(i)=eij*xa(i)
        if procval.ifout(0) eq 2 then xa(i)=eij*(xa(i)-1.0)/(zo*zo)
        if procval.ifout(0) eq 3 then xa(i)=xa(i)
        if procval.ifout(0) eq 4 then xa(i)=eij*(xa(i)-1.0)/(zeff*zeff)
        if procval.ifout(1) eq 5 then ya(i)=ya(i)/			$
       		(procval.lstwt*eij*xt)
        if procval.ifout(1) eq 6 then ya(i)=ya(i)/			$
      		(procval.ustwt*eij*(xt-1.0))
        if procval.ifout(1) eq 7 then ya(i)=ya(i)
        if procval.ifout(1) eq 8 then ya(i)=ya(i)*zeff*zeff
      endfor
      
      for i = 0,ictn-1 do begin
        printf, pipe, format = '(1x,e12.4,1x,e12.4)', 			$
                      xa(i), ya(i)
      endfor
    end

    if lpend eq 1 or lpend eq 2 then goto, LABELEND

                   ;**** re-set lpend ****
    lpend = 0
    arch = 0       ;**** default archive option is off
    b = intarr(5)
    for i = 0,4 do b(i) = -1

    adas101_plot, plotx, ploty, dgex, dgey, b, lpend, print1, arch, 	$
                  lchoice, outval, action, device, date, header, titlx, $
                  bitfile, gomenu, wintitle = 'UPSILON GRAPH', nsp = 70,$
                  font = font  
    printf, pipe, lpend

                   ;**** Print archiving decision if continuing ****

    if lpend eq 0 then printf, pipe, arch
    
    if lpend eq 1 then goto, LABELEND


                   ;**** special case if archiving selected after ****
                   ;**** picking no archiving initially ****

    if arch eq 1 and lchoice eq 0 then begin
      dsarch = root+'archive.dat'
      printf, pipe,format = '(1a80)', dsarch
    end

LABELEND:

end


 
