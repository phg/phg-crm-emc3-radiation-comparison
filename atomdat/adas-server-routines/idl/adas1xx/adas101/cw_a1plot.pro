; Copyright (c) 1995 Strathclyde University .
; SCCS info: Module @(#)$Header: /home/adascvs/idl/adas1xx/adas101/cw_a1plot.pro,v 1.3 2004/07/06 12:25:37 whitefor Exp $ Date $Date: 2004/07/06 12:25:37 $
;+
; NAME: CW_A1PLOT
;
; PURPOSE:
;       To create a widget which displays the Upsilon plot,
;       and allows a choice of archiving and printing.
;
; INPUTS:
;       PARENT - The ID of the parent widget.
;
;       BITFILE - String; the path to the directory containing bitmaps
;                 for the 'escape to series menu' button.
;
; KEYWORD PARAMETERS:
;       FONT   - The name of a font
;	UVALUE - Supplies the user value for the widget.
;       PLOTVAL- A struicture containing the value of the window display.
;
; OUTPUTS:
;       The ID of the created widget is returned.
;
; SIDE EFFECTS:
;      None
;
; CALLS:
;       A1PLOT_EVENT - The event handling routine
;
; INCLUDED:
;       A1PLOT_GET_VALUE- The procedure to get the value of the widget
;
; WRITTEN:
;       DAVID H.BROOKS, UNIV.OF STRATHCLYDE, EXT.4213/4205
;
; MODIFICATION HISTORY:
;	1.1	DAVID H.BROOKS
;		First Release
;	1.2	William Osborn
;		Added menu button
;	1.3	William Osborn
;		Removed xsize=n keyword from widget_label command.
;		The dynlabel procedure does not override these
;		assignments and they were taken as being values in pixels
;		on some machines.
; VERSION
;	1.1   	02-July-1995   
;	1.2	30-05-96
;	1.3	01-08-96
;
;-----------------------------------------------------------------------

FUNCTION a1plot_get_value, id

	        ;**** Return to caller ****
  ON_ERROR, 2

	        ;**** Retrieve the structure ****
  stash = WIDGET_INFO(id, /CHILD)
  WIDGET_CONTROL, stash, GET_UVALUE=state, /NO_COPY

	        ;**** Get the graphics area value ****
  widget_control,state.drawid,get_value=window


  ps = { WIN:window }

  WIDGET_CONTROL, stash, SET_UVALUE=state, /NO_COPY

                ;**** Return the value ****
return,ps

END

;-----------------------------------------------------------------------------

FUNCTION a1plot_event, ev

  parent=ev.handler


	        ;**** Retrieve the structure ****
  stash = WIDGET_INFO(parent, /CHILD)
  WIDGET_CONTROL, stash, GET_UVALUE=state, /NO_COPY
		;*********************************
		;**** Clear previous messages ****
		;*********************************
  widget_control,state.messid,set_value=''


                ;************************
                ;**** Process events ****
                ;************************
  CASE ev.id OF

		;***********************
		;**** Archive button****
		;***********************
    state.archiveid: actn = 'Archive'
   		;***********************
		;**** Print button  ****
		;***********************
    state.printid:   actn = 'Print'
		;***********************
		;**** Cancel button ****
		;***********************
    state.cancelid:  actn = 'Cancel'
		;*********************
		;**** Done button ****
		;*********************
    state.doneid:    actn = 'Done'
		;*********************
		;**** Menu button ****
		;*********************
    state.outid:    actn = 'Menu'

    ELSE:

  ENDCASE

                ;**** Restore the state structure ****
  WIDGET_CONTROL, stash, SET_UVALUE=state, /NO_COPY


  RETURN, { ID:parent, TOP:ev.top, HANDLER:0L, ACTION:actn  }
END

;-----------------------------------------------------------------------------

FUNCTION cw_a1plot, parent, bitfile, plotval = plotval, UVALUE = uval, $
			FONT = font

  IF (N_PARAMS() EQ 0) THEN MESSAGE, 'Must specify a parent for cw_a1plot'

  ON_ERROR, 2		

	        ;**** Set the defaults for keywords ****
  IF NOT (KEYWORD_SET(uval))  THEN uval = 0
  IF NOT (KEYWORD_SET(font)) THEN font = ''
  IF NOT (KEYWORD_SET(plotval)) THEN begin
    ps = { WIN:0 }
  END ELSE BEGIN
    ps = { WIN:plotval.win }
  END

  mainbase = WIDGET_BASE(parent, UVALUE = uval, $
		EVENT_FUNC = "a1plot_event", $
		FUNC_GET_VALUE = "a1plot_get_value",/column)
                ;*********************************
                ;**** Create window for plot  ****
                ;*********************************
  plotbase = widget_base(mainbase,/row)
  drawid = widget_draw(plotbase,xsize=850,ysize=750)


		;*****************
		;**** Buttons ****
		;*****************
  spacerow = widget_base(mainbase,/row)
  space = widget_label(spacerow,value='')
  base = widget_base(mainbase,/row,/frame)
		;**** Menu Button ****
  menufile = bitfile + '/menu.bmp'
  read_X11_bitmap, menufile, bitmap1
  outid = widget_button(base, value=bitmap1)

		;**** Print Button ****
  printid = widget_button(base,value='Print',font=font)

		;**** Archive Button ****
  archiveid = widget_button(base,value='Archive',font=font)

		;**** Cancel Button ****
  cancelid = widget_button(base,value='Cancel',font=font)

		;**** Done Button ****
  doneid = widget_button(base,value='Done',font=font)

		;**** Error message(alter to my choice) ****
   messid = widget_label(parent,font=font,value='*')

                ;**** Save the initial state ****
  state = { ARCHIVEID:archiveid, PRINTID:printid, $
            CANCELID:cancelid, DONEID:doneid, MESSID:messid, $
            DRAWID:drawid, FONT:font, OUTID:outid}
  WIDGET_CONTROL, WIDGET_INFO(mainbase, /CHILD), SET_UVALUE=state, /NO_COPY

  RETURN, mainbase

END





