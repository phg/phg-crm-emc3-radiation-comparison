; Copyright (c) 1995 Strathclyde University .
; SCCS info: Module @(#)$Header: /home/adascvs/idl/adas1xx/adas101/adas_burg_plot.pro,v 1.3 2004/07/06 11:08:13 whitefor Exp $ Date $Date: 2004/07/06 11:08:13 $
;+
; PROJECT:
;       ADAS IBM MVS to DEC UNIX conversion & development
;
; NAME:
;       ADAS_BURG_PLOT
;
; PURPOSE:
;       To plot the Upsilon graph for the BURGESS option & to allow
;       printing of both this and the reduced omega graph.
;
; EXPLANATION:
;       creates a window for the display of the graph and displays
;       output as the user directs through interaction with the
;       widget.
;
; USE:  ADAS101 Specific
;
; INPUTS:
;       XC    - Array; The x-coordinates for the Upsilon graph 
;                      (log10(Te)))
;
;       YC    - Array; The y-coordinates for the Upsilon graph
;                      (Upsilon)
;
;       DGEX  - Array; The x-coordinates for the Reduced Omega
;                      graph.
;
;       DGEY  - Array; The y-coordinates for the Reduced Omega
;                      graph.
;
;       B     - Array; The five Burgess Knot points.
;
;       LPENDC- Integer; Determines whether 'cancel' or 'done' 
;                        selected.
;
;       PRINT1- Integer; Determines whether Reduced Omega printout
;                        is requested.
;
;       ARCHC - Integer; Determines whether archiving is requested.
;
;       LCHOICEC-Integer; Holds archiving option
;
;       DEVICE - String; The current graphics device.
;
;       DATE   - String; The date
;
;       HEADER - String; The Printout header.
;
;       TITLX  - String; Part of the graph title (The dataset name)
;
;       RMS    - Real; A measure of the accuracy of the chosen Scaling
;                      parameter C.
;
;       BITFILE - String; the path to the directory containing bitmaps
;                 for the 'escape to series menu' button.
;
; OPTIONAL INPUTS:
;
; OUTPUTS:
;       VAL   - Structure; Contains the values of the output widget
;                          selected by the user, see a1spf1.pro.
;
;       ACT   - String; The evnt action; 'Cancel' or 'Done'
;
;       GOMENU  - Int; flag - set to 1 if user has selected 'escape direct
;                 to series menu , otherwise 0
;
; OPTIONAL OUTPUTS:
;
; KEYWORD PARAMETERS:
;       WINTITLE - A title for the window
;       FONT 
;       NSP - The number of requested spline points
;
; CALLS:
;       ADAS_BURG_PLOT_EV
;       XMANAGER
;
; SIDE EFFECTS:
;       None
;
; CATEGORY: ADAS System
;
; WRITTEN: Andrew Bowen, Tessella Support Services plc,  5-May-1993
;
; MODIFIED: 
;	1.1	David H.Brooks, Univ.of Strathclyde,
;           	Altered adas207_plot.pro to the needs of ADAS101
;	1.2	Tim Hammond, Tessella Support Services plc
;		Tidied up graph titling
;	1.3 	William Osborn
;		Added menu button and dynlabel proc
; VERSION:
;	1.1	23-06-95
;	1.2	14-07-95
;	1.3	30-05-96
;-
;-----------------------------------------------------------------------------
PRO adas_burg_plot_ev, event

  COMMON plot101b_blk, action, value, lpend, arch, lchoice, datab, plotdev, $
                   plotfile, fileopen, win, print1, gomenu
  newplot = 0
  print = 0
  done = 0

		;**** Find the event type and copy to common ****
    action = event.action

    CASE action OF
            
                ;**** 'Print' button ****
        'Print' : begin
               newplot = 1
               print = 1
           end
                ;**** 'Archive' button ****

        'Archive' : begin
                arch = 1
                if lchoice eq 0 then begin
                  buttons = ['Continue']
                  title = 'ADAS101:DEFAULT ACTION CONTINUING'
                  message = $
"Archive selection clashes with 'no archive' flag arch101/archive.dat created"
                  dec = popup(message=message,title=title,$
                              buttons=buttons,font=font)
                end
           end

		;**** 'Done' button ****
	'Done'  : begin

                if fileopen eq 1 then begin
                   set_plot,plotdev
                   device,/close_file
                end
                set_plot,'X'
		widget_control,event.top,/destroy
                done = 1
                lpend = 0

	   end

		;**** 'Menu' button ****

	'Menu': begin
                if fileopen eq 1 then begin
                   set_plot,plotdev
                   device,/close_file
                end
		widget_control,event.top,/destroy
		done = 1
		lpend = 1
		gomenu = 1
		set_plot,'X'
	   end

		;**** 'Cancel' button ****
	'Cancel': begin
                  widget_control,event.top,/destroy
                  done = 1
                  lpend = 1
                  set_plot,'X'
           end

    END

  if done eq 0 then begin
                ;**** Set graphics device ****
     if print1 eq 1 then begin
       set_plot,plotdev
       if fileopen eq 0 then begin
         fileopen = 1
         device,filename = plotfile
         device, /landscape
       end 
                ;**** plot first graph if requested ****
       plot101, datab.dgex, datab.dgey, datab.title1, datab.xtitle1, $
                datab.ytitle1, datab.xmin1, datab.xmax1, datab.ymin1, $
                datab.ymax1, datab.nsp, datab.ldef1, datab.b
     end      
     if print eq 1 then begin
       set_plot,plotdev
       if fileopen eq 0 then begin
         fileopen = 1
         device,filename = plotfile
         device, /landscape
       end
     end else begin
       set_plot,'X'
       wset,win
     end
                ;**** Draw Graphics ****
     plot101, datab.x, datab.y, datab.title2, datab.xtitle2, datab.ytitle2, $ 
              datab.xmin2, datab.xmax2, datab.ymin2, datab.ymax2, datab.nsp, $
              datab.ldef2, datab.b

  end

END

;-----------------------------------------------------------------------------


PRO adas_burg_plot, xc, yc, dgex, dgey, b, lpendc, print1c, archc, lchoicec, $
                    val, act, device, date, header, titlx, rms, bitfile, $
		    gomenu, WINTITLE = wintitle, NSP = nspc, FONT = font

  COMMON plot101b_blk,action,value, lpend, arch, lchoice, datab, plotdev, $
                   plotfile, fileopen, win, print1, gomenucom

		;**** Copy values to common ****
  value = val
  lpend = lpendc
  arch = archc
  lchoice = lchoicec
  plotdev = device
  plotfile = value.hardname
  fileopen = 0
  print1 = print1c
  gomenucom = gomenu

		;**** Set defaults for keywords ****
  IF NOT (KEYWORD_SET(wintitle)) THEN wintitle = ''
  IF NOT (KEYWORD_SET(nsp)) THEN NSP = -1
  IF NOT (KEYWORD_SET(font)) THEN font = 'courier-bold14'

		;***************************************
		;**** Pop-up a new widget if needed ****
		;***************************************
  plotval = {WIN:0}

                ;**** create base widget ****
  inid = widget_base(TITLE=wintitle,XOFFSET=200,YOFFSET=0)
		;**** Declare output options widget ****
  cwid = cw_a1plot(inid, bitfile, plotval = plotval, FONT=font)

		;**** Realize the new widget ****
  widget_control,inid,/realize
                ;**** get the id of the graphics area ****
  widget_control,cwid,get_value = window
  win = window.win
  wset,win
                 ;**** Set up titles ****
   title1 = strarr(5)
   title2 = title1
     title1(0) = "OPTIMISED FIT TO OMEGA DATA POINTS "
       if (strtrim(strcompress(value.gtit1),2) ne ' ' ) then begin
         title1(0) = title1(0)+': '+strupcase(strtrim(value.gtit1,2))
       end
     title1(1) = 'ADAS    :'+strupcase(header)
     title1(2) = 'FILE     : '+strcompress(titlx)+$
                 ' R.M.S.= '+strtrim(string(rms,format='(E10.4)'),2)
     title1(3) = 'KEY     : (DIAMONDS - INPUT DATA) '+$
                '(FULL LINE - BURGESS SPLINE FIT)'+$
                '(CROSSES - BURGESS KNOT POINTS)'
     title1(4) = ' '
     title2(0) = "UPSILON GRAPH "
       if (strtrim(strcompress(value.gtit1),2) ne ' ' ) then begin
          title2(0) = title2(0)+': '+strupcase(strtrim(value.gtit1,2))
       end
     title2(1) = 'ADAS    :'+strupcase(header)
     title2(2) = 'FILE     :'+strcompress(titlx)
     title2(3) = 'KEY     : (DIAMONDS - OUTPUT DATA) '+$
                '(FULL LINE -  SPLINE FIT)'
     title2(4) = ' '
  xtitle1 = ' REDUCED ENERGY '
  ytitle1 = ' REDUCED OMEGA '
  xtitle2 = ' LOG10(TE(K)) '
  ytitle2 = ' GAMMA '
                ;**** Put the graphing data into common ****
  datab = { dgex:dgex, dgey:dgey, title1:title1, xtitle1:xtitle1,  $
           ytitle1:ytitle1, $
           x:xc , y:yc, title2:title2, xtitle2:xtitle2, ytitle2:ytitle2, $
           xmin1:value.xmin1, xmax1:value.xmax1, ymin1:value.ymin1, $
           ymax1:value.ymax1, ldef1:value.grpscal1, $
           xmin2:value.xmin2, xmax2:value.xmax2, ymin2:value.ymin2, $
           ymax2:value.ymax2, NSP:nspc, ldef2:value.grpscal2, b:b }
  
  plot101, xc, yc, title2, xtitle2, ytitle2, value.xmin2, value.xmax2, $
           value.ymin2, value.ymax2,  nsp, value.grpscal2, b

		;**** make widget modal ****
  xmanager,'adas_burg_plot',inid,$
            event_handler='adas_burg_plot_ev',/modal,/just_reg
 
		;**** Return the output value from common ****
  act = action
  val = value
  lpendc = lpend
  archc = arch
  gomenu = gomenucom

END

