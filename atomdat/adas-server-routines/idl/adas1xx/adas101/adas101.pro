; SCCS info: Module @(#)$Header: /home/adascvs/idl/adas1xx/adas101/adas101.pro,v 1.18 2004/07/06 10:08:00 whitefor Exp $ Date $Date: 2004/07/06 10:08:00 $
;+
; PROJECT:
;       ADAS IBM MVS to UNIX conversion and development
;
; NAME:
;	ADAS101
;
; PURPOSE:
;	The highest level routine for the ADAS 101 program.
;
; EXPLANATION:
;	This routine is called from the main adas system routine, adas.pro,
;	to start the ADAS 101 application.  Associated with adas101.pro
;	is a FORTRAN executable.  The IDL code provides the user
;	interface and output graphics whilst the FORTRAN code reads
;	in data files, performs numerical processing and creates the
;	output files.  The IDL code communicates with the FORTRAN
;	executable via a bi-directional UNIX pipe.  The unit number
;	used by the IDL for writing to and reading from this pipe is
;	allocated when the FORTRAN executable is 'spawned' (see code
;	below).  Pipe communications in the FORTRAN process are to
;	stdin and stdout, i.e streams 5 and 6.
;
;	The FORTRAN code is an independent process under the UNIX system.
;	The IDL process can only exert control over the FORTRAN in the
;	data which it communicates via the pipe.  The communications
;	between the IDL and FORTRAN must be exactly matched to avoid
;	input conversion errors.  The correct ammounts of data must be
;	passed so that neither process 'hangs' waiting for communications
;	which will never occur.
;
;	The FORTRAN code performs some error checking which is
;	independent of IDL.  In cases of error the FORTRAN may write
;	error messages.  To prevent these error messages from conflicting
;	with the pipe communications all FORTRAN errors are written to
;	output stream 0, which is stderr for UNIX.  These error messages
;	will appear in the window from which the ADAS session/IDL session
;	is being run.
;
;	In the case of severe errors the FORTRAN code may terminate
;	itself prematurely.  In order to detect this, and prevent the
;	IDL program from 'hanging' or crashing, the IDL checks to see
;	if the FORTRAN executable is still an active process before
;	each group of pipe communications.  The process identifier
;	for the FORTRAN process, PID, is recorded when the process is
;	first 'spawned'.  The system is then checked for the presence
;	of the FORTRAN PID.
;
; USE:
;	First the system settings must be established by calling
;	adas_sys_set.pro then adas101.pro is called to start the
;	ADAS 101 application;
;
;	adas_sys_set, adasrel, fortdir, userroot, centroot, $
;		      devlist, devcode, font_large, font_small, edit_fonts
;	adas209,   adasrel, fortdir, userroot, centroot, devlist, $
;		   devcode, font_large, font_small, edit_fonts
;
; INPUTS:
;	ADASREL - A string indicating the ADAS system version, 
;		  e.g ' ADAS RELEASE: ADAS93 V1.1.0'.  The first
;		  character should be a space.
;
;	FORTDIR - A string holding the path to the directory where the
;		  FORTRAN executables are, e.g '/disk/adas/fortran/exec'
;
;	USERROOT - A string holding the path to the root directory of
;		   the user's adas data. e.g '/disk/bowen/adas'
;		   This root directory will be used by adas to construct
;		   other path names.  For example the users default data
;		   for adas101 should be in /disk/bowen/adas/default.
;		   In particular the user's default interface settings will
;		   be stored in the directory USERROOT+'/defaults'.  An
;		   error will occur if the defaults directory does not
;		   exist.
;
;	CENTROOT - Like USERROOT, but this directory points to the
;		   central data area for the system.  User defaults are
;		   not stored on CENTROOT.
;
;	DEVLIST  - A string array of hardcopy device names, used for
;		   graphical output. e.g ['Post-Script','HP-PCL','HP-GL']
;		   This array must mirror DEVCODE.  DEVCODE holds the
;		   actual device names used in a SET_PLOT statement.
;
;	DEVCODE  - A string array of hardcopy device code names used in
;		   the SET_PLOT statement, e.g ['PS', 'PCL', 'HP']
;		   This array must mirror DEVLIST.
;
;	FONT_LARGE - The name of a larger font e.g 'courier_bold14'
;
;	FONT_SMALL - The name of a smaller font e.g 'courier_bold12'
;
;	EDIT_FONTS - A structure of two fonts used in the table editor
;		     adas_edtab.pro, {font_norm:'',font_input:''}.
;		     The two fonts are used to differentiate between
;		     editable and non-editable parts of the table. You
;		     may have to experiment to get two fonts which give
;		     the same default spacing in the edit widget.
;		     e.g {font_norm:'helvetica_bold14', $
;			  font_input:'helvetica_oblique14'}
;
; OPTIONAL INPUTS:
;	None
;
; OUTPUTS:
;	None - Note this routine should leave all inputs un-modified.
;
; OPTIONAL OUTPUTS:
;	None
;
; KEYWORD PARAMETERS:
;	None
;
; CALLS:
;	FIND_PROCESS	Checks to see if a given process is active.
;	A1SPF0		Pipe comms with FORTRAN A1SPF0 routine.
;	A1ISPF		Pipe comms with FORTRAN A1ISPF routine.
;	XXDATE		Get date and time from operating system.
;	A1SPF1		Pipe comms with FORTRAN A1SPF1 routine.
;       A1OUTGA         Pipe comms with FORTRAN A1OUTGA routine
;       A1OUTGB         Pipe comms with FORTRAN A1OUTGB routine
;
; SIDE EFFECTS:
;	This routine spawns a FORTRAN executable.  Note the pipe 
;	communications routines listed above.  In addition to these
;	pipe communications there is one explicit communication of the
;	date to the FORTRAN code, search for 'printf,pipe' to find it.
;
; CATEGORY:
;	Adas system.
;	
; WRITTEN:
;       David H.Brooks Univ.of Strathclyde, 10-May-1995
;       Modified adas201.pro into adas101.pro, by altering default
;       settings,spawn command,output structures etc. and adding adas101
;       specific procedures e.g.a1ispf.pro., a1outga.pro
;       
;
; VERSION:
;       1.1     David H. Brooks
;		First Release
;	1.2	Tim Hammond (Tessella Support Services plc)
;		Tidied up comments and code and put in
;		standard directory names etc.
;	1.3	Tim Hammond
;		Altered default text output filename
;	1.4	Tim Hammond
;		Increased version number to 1.2 in line with changes
;		made to other 101 routines.
;	1.5	Tim Hammond
;               Increased version number to 1.3 in line with changes
;               made to other 101 routines.
;	1.6     Tim Hammond
;               Increased version number to 1.4 and corrected minor syntax
;		error.
;	1.7     Tim Hammond
;               Increased version number to 1.5
;	1.8	Tim Hammond
;		Increased version number to 1.6
;	1.9	Tim Hammond
;		Increased version number to 1.7
;	1.10	William Osborn
;		Added menu buttons
;	1.11	William Osborn
;		Increased version number to 1.8
;	1.12	William Osborn
;		Increased version number to 1.9
;	1.13	William Osborn
;		Increased version number to 1.10
;	1.14	Richard Martin
;		Increased version number to 1.11
;	1.15	Richard Martin
;		Increased version number to 1.12
;	1.16	?
;		
;	1.17	Richard Martin
;		Increased version number to 1.13
;	1.18	Richard Martin
;		Increased version number to 1.14
;	
; MODIFIED:
;	1.1	10-07-95
;	1.2	11-07-95
;	1.3	13-07-95
;	1.4	17-07-95
;	1.5	17-07-95
;	1.6	17-07-95
;	1.7	15-11-95
;	1.8	16-02-96
;	1.9	13-05-96
;	1.10	30-05-96
;	1.11	14-10-96
;	1.13	25-11-96
;	1.14	04-12-98
;	1.15	18-10-99
;	1.16	?
;	1.17	18-10-99
;	1.18	18-03-02
;
;-
;-----------------------------------------------------------------------------


PRO ADAS101,	adasrel, fortdir, userroot, centroot, 			$
		devlist, devcode, font_large, font_small, edit_fonts

		;************************
		;**** Initialisation ****
		;************************

    adasprog = ' PROGRAM: ADAS101 V1.14'
    lpend = 0
    gomenu = 0
    deffile = userroot+'/defaults/adas101_defaults.dat'
    device = ''
    bitfile = centroot + '/bitmaps'

		;******************************************
		;**** Search for user default settings ****
		;**** If not found create defaults     ****
		;******************************************

    files = findfile(deffile)
    if files(0) eq deffile then begin
        restore, deffile
        inval.centroot = centroot+'/arch101/'
        inval.userroot = userroot+'/arch101/'
    end else begin
        inval = { 							$
    		  ROOTPATH:userroot+'/arch101/', 			$
		  FILE:'', 						$
		  CENTROOT:centroot+'/arch101/', 			$
		  USERROOT:userroot+'/arch101/',			$
                  REFRESH:''}

        procval = {NEW:-1}
        outval = { 							$
		    GRPOUT:0, GTIT1:'', 				$
                    GRPSCAL1:0, 					$
		    XMIN1:'',  XMAX1:'',   				$
		    YMIN1:'',  YMAX1:'',   				$
                    GRPSCAL2:0, 					$
		    XMIN2:'',  XMAX2:'',   				$
		    YMIN2:'',  YMAX2:'',   				$
                    ANOPT:-1,  AXOP:0, ADIF:0,  			$
                    ADOPT:1,  BDPTOPT:0,  				$
                    BCVAL:'',   BXVAL:0,  BYVAL:'',  			$
     		    HRDOUT:0, HARDNAME:'', 				$
		    GRPDEF:'', GRPFMESS:'', 				$
		    GRPSEL:-1, GRPRMESS:'', 				$
		    DEVSEL:-1, GRSELMESS:'', 				$
		    TEXOUT:0, TEXAPP:-1, 				$
		    TEXREP:0, TEXDSN:inval.rootpath+'paper.txt',	$
		    TEXDEF:'',TEXMES:'' 				$
                 }
    end

                ;****************************
                ;**** Start fortran code ****
                ;****************************

    spawn, fortdir+'/adas101.out', unit=pipe, /noshell, PID=pid

		;************************************************
		;**** Get date and write to fortran via pipe ****
		;************************************************

    date = xxdate()
    printf, pipe, date(0)

LABEL100:
		;**** Check FORTRAN still running ****
  
    if find_process(pid) eq 0 then goto, LABELEND

		;************************************************
		;**** Communicate with a1spf0 in fortran and ****
		;**** invoke user interface widget for       ****
		;**** Data file selection                    ****
		;************************************************

    lchoice = -1

    a1spf0, pipe, inval, dsfull, lchoice, rep, FONT=font_large

		;**** If cancel selected then end program ****

    if rep eq 'YES' then goto, LABELEND

LABEL200:

    if find_process(pid) eq 0 then goto, LABELEND

    a1ispf, pipe, lpend, procval, dsfull, lchoice, 			$
            ixops, ibpts, ifpts, idiff, bcval, iwhc, bitfile, gomenu,   $
            FONT_LARGE=font_large, FONT_SMALL=font_small, 		$
	    EDIT_FONTS=edit_fonts 

                ;**** If menu button clicked, tell FORTRAN to stop ****

    if gomenu eq 1 then begin
        printf, pipe, 1
        goto, LABELEND
    endif else begin
        printf, pipe, 0
    endelse

		;**** If cancel selected then go back to ****
                ;**** the previous interface window ****
  
    if lpend eq 1 then goto, LABEL100

		;************************************************
                ;**** If Refresh selected then replace ADAS *****
                ;**** analysis options defaults with those  *****
                ;**** from the archive                      *****
		;************************************************

    if lchoice eq 2 then begin
        if iwhc eq 1 then begin
            outval.axop = ixops
            outval.adif = idiff
            outval.adopt = ifpts
            outval.bdptopt = ibpts
        endif else begin
            outval.bcval = bcval
        endelse
    endif                

                ;**** create header for output ****

    header=adasrel+adasprog+' DATE: '+date(0)+' TIME: '+date(1)

LABEL300:

    if find_process(pid) eq 0 then goto, LABELEND

    a1spf1, pipe, lpend, inval, procval, outval, dsfull, header, 	$
            bitfile, gomenu, DEVLIST=devlist, FONT=font_small  

                ;**** If menu button clicked, tell FORTRAN to stop ****

    if gomenu eq 1 then begin
        printf, pipe, 1
        goto, LABELEND
    endif else begin
        printf, pipe, 0
    endelse
     
		;**** If cancel selected then go back to ****
                ;**** the previous interface window ****
  
    if lpend eq 1 then goto, LABEL200

                ;************************************** 
                ;**** Set up graphic displays for  ****
                ;**** chosen option                ****
                ;************************************** 

    if outval.devsel ge 0 then device = devcode(outval.devsel)
    titlx = strtrim(dsfull,2)
    if outval.grpout eq 1 then begin
        if outval.anopt eq 1 then begin
    	    if find_process(pid) eq 0 then goto, LABELEND
            a1outga, pipe, lpend, lchoice, inval.rootpath, outval, 	$
                     procval, device, date, header, titlx, bitfile, 	$
		     gomenu, FONT = font_large
            wait, 1.0 
        endif else begin
    	    if find_process(pid) eq 0 then goto, LABELEND
            a1outgb, pipe, lpend, lchoice, inval.rootpath, outval, 	$
                     procval, device, date, header, titlx, bitfile, 	$
		     gomenu, FONT = font_large
            wait, 1.0
        endelse
		;**** If menu button clicked, tell FORTRAN to stop ****

     	if gomenu eq 1 then begin
            printf, pipe, 1
            goto, LABELEND
    	endif else begin
            printf, pipe, 0
    	endelse

    endif

    if lpend eq 1 or lpend eq 2 then goto, LABEL300

LABELEND:

		;**** Ensure appending is not enabled for ****
		;**** text output at start of next run.   ****

   outval.texapp = -1

		;**** Save user defaults ****

    save, inval, procval, outval, filename=deffile

END
