; Copyright (c) 1995, Strathclyde University.
; SCCS info: Module @(#)$Header: /home/adascvs/idl/adas1xx/adas101/sp5.pro,v 1.3 2004/07/06 15:20:44 whitefor Exp $ Date $Date: 2004/07/06 15:20:44 $
;+
;+
; PROJECT:
;       ADAS IBM MVS to DEC UNIX conversion & development
;
; NAME:
;       SP5
;
; PURPOSE:
;       To calculate the Burgess spline for continuous
;       updating,on the editable graph screen,during interactions.
;
; EXPLANATION:
;       Calculates the Burgess type spline.
;
; USE:  
;       ADAS101 and ADAS102 Burgess branch.
;
; INPUTS:
;       X      - Float Array; The points at which to evaluate
;                             the spline.
;
;       Y      - Float Array; The knot points.
;
; OPTIONAL INPUTS:
;
; OUTPUTS:
;       X      - Float Array; The new values of the x-coordinates
;
; OPTIONAL OUTPUTS:
;
; KEYWORD PARAMETERS:
;
; CALLS:
;
; SIDE EFFECTS:
;       None
;
; CATEGORY: ADAS System
;
; WRITTEN: 
;         A.LANZAFAME, univ.of Strathclyde,  02-July-1995
;         Translated from BBC BASIC
; MODIFIED:
;	1.1	A. Lanzafame
;		First release
;       1.2     Hugh Summers
;               Adjusted for use by adas101 and adas102
;	1.3	Hugh Summers
;		Corrected error causing modification of input x
; VERSION: 
;	1.1	02-07-95
;       1.2     23-11-96
;	1.3	17/05/99
;-
;-----------------------------------------------------------------------------
function sp5,x,y

 if n_elements(y) eq 5 then begin

    x0=x
    
    A=y(0)
    B=y(1)
    C=y(2)
    D=y(3)
    E=y(4)

    F=16*(19*A-43*B+30*C-7*D+E)/15
    G=16*(-A+7*B-12*C+7*D-E)/3
    H=16*(A-7*B+30*C-43*D+19*E)/15
    if x0 le .25 then begin
      x0=x0-.125
      D=.5*(A+B)-F/128
      C=4*(B-A)
      B=.5*F
      A=0
    endif else if x0 le .5 and x0 gt .25 then begin
      x0=x0-.375
      D=.5*(B+C)-(F+G)/256
      C=4*(C-B)-(G-F)/96
      B=.25*(F+G)
      A=2*(G-F)/3
    endif else if x0 le .75 and x0 gt .5 then begin
      x0=x0-.625
      A=2*(H-G)/3
      B=.25*(H+G)
      E=C 
      C=4*(D-C)-A/64
      D=.5*(E+D)-B/64
    endif else if x0 gt .75 then begin
      x0=x0-.875
      A=0
      B=.5*H
      C=4*(E-D)
      D=.5*(E+D)-B/64
    end
    spl5=D+x0*(C+x0*(B+x0*A))
  endif else begin
    print,'ERROR: SP5 - y is not a 5 element array'
    spl5=0.0
  endelse
  return,spl5
end
