; Copyright (c) 1996, Strathclyde University.
;+
; PROJECT:
;       ADAS IBM MVS to UNIX conversion
;
; NAME:
;       CASPF2
;
; PURPOSE:
;       IDL control program for the interactive version of the adas 310
;	calculations. 
;
; EXPLANATION:
;	This routine kicks off an interactive version of the 310 FORTRAN 
;	- adas310.out - and communicates with it via a bi-directional 
;	UNIX pipe. 
;	The progress of the calculations is displayed on an information
;	widget for the user. Once the calculations are finished the FORTRAN
;	process terminates and the routine returns to the purely IDL
;	output options screen.
;	All calculations and file operations are handled by the FORTRAN so
;	the IDL continuously monitors to see whether the FORTRAN is still
;	running and if not it will terminate as well.
;	Communications are with the FORTRAN subroutine RUN310 and main
;	program ADAS310.
;
; USE:
;       The use of this routine is specific to ADAS310, See adas310.pro.
;
; INPUTS:
;       INVAL	- Structure containing the final settings of the input
;		  options widget.
;
;	PROCVAL	- Structure containing the final settings of the processing
;                 options widget.
;
;       OUTVAL	- Structure containing the final settings of the output
;                 options widget.
;
;       DATE    - String; the date.
;
;	FORTDIR	- String; the full pathname of the directory containing
;		  the FORTRAN executables.
;
;	CENTROOT- String; Path pointing to the central data area on each
;		  machine. Used to construct variable dslpath to pass to
;		  FORTRAN.
;
;	MXCOR	- Integer;Parameter - use unknown except as size of other
;			  unknown variables.
;
;	MXEPS	- Integer;Parameter - use unknown except as size of other
;                         unknown variables.
;
;	MXDEF	- Integer;Parameter - use unknown except as size of other
;                         unknown variables.
;
; OPTIONAL INPUTS:
;       None.
;
; OUTPUTS:
;	None.
;     
; OPTIONAL OUTPUTS:
;       None.
;
; KEYWORD PARAMETERS:
;
;       FONT    - Supplies the font to be used for the information widgets.
;
; CALLS:
;	XXTCON	- General temperature conversion routine
;
; SIDE EFFECTS:
;       This routine communicates with the ADAS310a FORTRAN process
;       via UNIX pipes.
;       It also pops up an information widget which keeps the user
;       updated as the calculation progresses.
;
; CATEGORY:
;       Adas system.
;
; WRITTEN:
;       Tim Hammond, Tessella Support Services plc, 11th January 1996
;
; MODIFIED:
;       1.1   Tim Hammond         
;		First version
;	1.2   Tim Hammond
;		Corrected call to xxtcon which converted procval.ts
;		from eV into Kelvin.
;       1.3   William Osborn
;               Added outval.title output down pipe
;       1.4   William Osborn
;               Added xsize=560 to widget_draw
;	1.5   Richard Martin
;		Added font keyword to widget_label statements
;	1.6   Martin O'Mullane
;		Set FALSE to fortran for lnion and lnocx as these
;               options have not yet been expose in the interactive
;               version of the code.
;
; VERSION:
;	1.1	08-02-96
;	1.2	14-02-96
;	1.3	02-10-96
;       1.4	14-10-96
;	1.5	20-09-99
;	1.6	18-02-2010
;
;-
;-----------------------------------------------------------------------------

PRO caspf2, inval, procval, outval, date, fortdir, centroot, mxcor, 	$
            mxeps, mxdef, font=font

		;***********************************
                ;**** Return to caller on error ****
		;***********************************

    ON_ERROR, 2

		;**********************************
                ;**** Set defaults for keyword ****
		;**********************************

    IF NOT (KEYWORD_SET(font)) THEN font = ''

                ;**************************************
		;**** Create an information widget ****
                ;**************************************

    complete = 0
    widget_control, /hourglass
    base = widget_base(/column, xoffset=300, yoffset=200,               $
    title = "ADAS310: INFORMATION")
    lab0 = widget_label(base, value='',font=font)
    lab1 = widget_label(base,                                           $
    value="    ADAS310 CALCULATIONS UNDERWAY - PLEASE WAIT    ",font=font)
    grap = widget_draw(base, ysize=20, xsize=560)
    base2 = widget_base(base, /row)
    lab2 = widget_label(base2, value="                    PROCESSING ",font=font)
    dummy = strcompress(complete) + ' ' 
    lab3 = widget_label(base2, value=dummy,font=font)
    lab4 = widget_label(base2, value="% COMPLETED",font=font)
    widget_control, base, /realize

		;*************************************************
		;**** Spawn the FORTRAN routine 'adas310.out' ****
		;*************************************************

    spawn, fortdir+'/adas310.out', unit=pipe, /noshell, PID=pid

                ;**********************************************
                ;**** Write initial information to FORTRAN ****
                ;**** Communications are with ADAS310.FOR  ****
                ;**********************************************

    nobatch = 0				;we are not running in batch	
    idum = 0				;dummy variable
    printf, pipe, nobatch

                ;*********************************************
                ;**** Write all necessary data to FORTRAN ****
                ;**** Communications are with RUN310.FOR  ****
                ;*********************************************

		;***********************
		;**** Send the date ****
		;***********************

    printf, pipe, date(0)

		;****************************
		;**** Input options data ****
		;****************************

    printf, pipe, inval.iz0
    printf, pipe, (inval.z+1)			;as defined in FORTRAN
    printf, pipe, inval.dsnex
    printf, pipe, inval.dsncx
    dslpath = centroot 
    printf, pipe, dslpath

		;*********************************
		;**** Processing options data ****
		;*********************************

    intemp = procval.ts				;Convert temperature to K
    xxtcon, 2, 1, 1, 1, intemp, outtemp
    printf, pipe, outtemp
    printf, pipe, procval.w
    printf, pipe, procval.cion
    printf, pipe, procval.cpy
    printf, pipe, procval.w1
    printf, pipe, procval.bsim
    printf, pipe, procval.nip
    printf, pipe, procval.intd
    printf, pipe, procval.iprs
    printf, pipe, procval.ilow
    printf, pipe, procval.ionip
    printf, pipe, procval.nionip
    printf, pipe, procval.ilprs
    printf, pipe, procval.ivdisp
    printf, pipe, procval.noscan
    if procval.noscan eq 0 then begin
	printf, pipe, procval.izeff
    endif else begin
	printf, pipe, procval.nimp
	for i=0,(procval.nimp-1) do begin
	    printf, pipe, procval.izimpa(i)
	    printf, pipe, procval.amimpa(i)
	    printf, pipe, procval.frimpa(i)
	endfor
    endelse
    printf, pipe, procval.nmin
    printf, pipe, procval.nmax
    printf, pipe, procval.imax
    for i=0, (procval.imax-1) do begin
	printf, pipe, procval.nrep(i)
	printf, pipe, procval.wbrep(i)
    endfor
    printf, pipe, procval.deref
    printf, pipe, procval.dpref
    xxtcon, 2, 1, 1, 1, procval.teref, reftemp_kelvin
    printf, pipe, reftemp_kelvin		;Reference temp. in Kelvin
    printf, pipe, procval.ndens
    for i=0, (procval.ndens-1) do begin
        printf, pipe, procval.densa(i)
        printf, pipe, procval.denpa(i)
    endfor
    printf, pipe, procval.ntemp

		;**** Convert temperatures to K before giving to FORTRAN ****

    xxtcon, 2, 1, 1, procval.ntemp, procval.tea(*), etemps_kelvin
    xxtcon, 2, 1, 1, procval.ntemp, procval.tpa(*), ptemps_kelvin
    for i=0, (procval.ntemp-1) do begin
        printf, pipe, etemps_kelvin(i)
        printf, pipe, ptemps_kelvin(i)
    endfor
    printf, pipe, procval.beref
    printf, pipe, procval.nbeng
    for i=0, (procval.nbeng-1) do begin
        printf, pipe, procval.bmenga(i)
    endfor
    printf, pipe, procval.densh
    printf, pipe, procval.jcor
    for i=0, (mxcor-1) do begin
        printf, pipe, procval.cor(i)
    endfor
    printf, pipe, procval.jmax
    for i=0, (mxeps-1) do begin
        printf, pipe, procval.epsil(i)
        printf, pipe, procval.fij(i)
        printf, pipe, procval.wij(i)
    endfor
    printf, pipe, procval.jdef
    for i=0, (mxdef-1) do begin
        printf, pipe, procval.defect(i)
    endfor

    ; Force lnoion and lnocx to be FALSE here - do not expose these
    ; options in the interactive version of adas310.
    
    printf, pipe, 0L
    printf, pipe, 0L

		;*****************************
		;**** Output options data ****
		;*****************************

    printf, pipe, strmid(outval.title,0,24)
    printf, pipe, outval.texout
    printf, pipe, outval.texout1
    printf, pipe, outval.texout2
    printf, pipe, outval.texout3
    printf, pipe, outval.texout4
    if outval.texout eq 1 and 						$
    strcompress(outval.texdsn, /remove_all) ne '' then begin
	filename = outval.texdsn
    endif else begin
	filename = 'temp1.0'
    endelse
    printf, pipe, filename
    if outval.texout1 eq 1 and 						$
    strcompress(outval.texdsn1, /remove_all) ne '' then begin
	filename = outval.texdsn1
    endif else begin
	filename = 'temp1.1'
    endelse
    printf, pipe, filename
    if outval.texout2 eq 1 and 						$
    strcompress(outval.texdsn2, /remove_all) ne '' then begin
	filename = outval.texdsn2
    endif else begin
	filename = 'temp2.2'
    endelse
    printf, pipe, filename
    if outval.texout3 eq 1 and 						$
    strcompress(outval.texdsn3, /remove_all) ne '' then begin
	filename = outval.texdsn3
    endif else begin
	filename = 'temp3.3'
    endelse
    printf, pipe, filename
    if outval.texout4 eq 1 and 						$
    strcompress(outval.texdsn4, /remove_all) ne '' then begin
	filename = outval.texdsn4
    endif else begin
	filename = 'temp4.4'
    endelse
    printf, pipe, filename

                ;*****************************************************
                ;**** Wait for a continue signal from the FORTRAN ****
                ;*****************************************************

    readf, pipe, idum
    if idum eq 1 then begin			;Error opening files
	print, '**** CASPF2.PRO: File opening error in FORTRAN ****'
	goto, LABELEND
    endif

		;**********************************************
		;**** Read how many passes through v2bnmod ****
		;**********************************************

    readf, pipe, idum
    nloops = idum
    inewstart = 0
    for k=1, nloops do begin
        readf, pipe, idum
        i1 = idum
        readf, pipe, idum
        i2 = idum
        itotal = i1 * i2 * 4
	if k lt nloops then begin
            fstep = 1000.0 / float(itotal * (nloops-1))
	endif else begin
	    fstep = (1000.0 - float(inewstart)) / float(itotal)
	endelse
        for i=1, itotal do begin
	    readf, pipe, idum
	    if idum eq 1 then begin

		;***************************************
		;**** Update the information widget ****
		;***************************************

		istart = inewstart
	        iend = istart + fix(fstep)
	   	inewstart = iend
	        for j=istart,iend do begin
		    x = float(j)/1000.0
	    	    plots, [x, x], [0.0, 1.0], /normal
                    percent = fix(x * 100)
		    if percent gt 100 then percent=100
		    labelval = strcompress(string(percent), /remove_all) 
		    widget_control, lab3, set_value=labelval
	        endfor
	    endif else begin
	        print, '**** CASPF2.PRO: ERROR IN FORTRAN ****'
	        goto, LABELEND
	    endelse
        endfor
    endfor

		;************************************************
		;**** Wait for the final signal from FORTRAN ****
		;************************************************

    readf, pipe, idum

                ;****************************************
                ;**** Destroy the information widget ****
                ;****************************************

LABELEND:

    widget_control, base, /destroy

END
