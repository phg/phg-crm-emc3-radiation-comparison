; Copyright (c) 1995, Strathclyde University
; SCCS INFO : Module @(#)$Header: /home/adascvs/idl/adas3xx/adas310/caispf.pro,v 1.9 2004/07/06 11:58:59 whitefor Exp $ Date $Date: 2004/07/06 11:58:59 $
;+
 PROJECT:
;       ADAS IBM MVS to UNIX conversion
;
; NAME:
;       CAISPF
;
; PURPOSE:
;       IDL user interface for 310 processing options
;
; EXPLANATION:
;       This routine invokes the part of the user interface
;       used to specify the processing options for ADAS310.
;
; USE:
;       The use of this routine is specific to ADAS310, see adas310.pro.
;
; INPUTS:
;	INVAL	- A structure containing the final settings of the input
;		  options screen.
;
;	PROCVAL	- A structure which determines the initial settings of
;                 the processing options widget.  The initial value is
;                 set in adas310.pro.  If adas310.pro passes a blank
;                 dummy structure of the form {NEW:-1} into PROCVAL then
;                 PROCVAL is reset to a default structure.
;
;                 The PROCVAL structure is;
;                       procval = { 	NEW	:       0,		$
;					ITOGGLE1:	0,		$
;					ITOGGLE2:	0,		$
;					ITOGGLE3:	0,		$
;					TS	:	1.0e8,		$
;					W	:	0.0e0,		$
;					CION	:	1.0e0,		$
;					CPY	:	1.0e0,		$
;					W1	:	1.0e8,		$
;					BSIM	:	float,		$
;					NIP	:	0-4,		$
;					INTD	:	0-3,		$
;					IPRS	:	flag,		$
;					ILOW	:	flag,		$
;					IONIP	:	flag,		$
;					NIONIP	:	0-999,		$
;					ILPRS	:	flag,		$
;					IVDISP	:	flag,		$
;					NOSCAN	: 	flag,		$
;					IZEFF	:	integer,	$
;					NIMP	: 	int [0-mximp],	$
;					IZIMPA	:	intarr(mximp),	$
;					AMIMPA	:	fltarr(mximp),	$
;					FRIMPA	:	fltarr(mximp),	$
;					NMIN	:	1,		$
;					NMAX	:	integer,	$
;					IMAX	:	integer,	$
;					NREP	:	intarr(mxnrp),	$
;					WBREP	:	fltarr(mxnrp),	$
;					IDREF	:	integer,	$
;					DEREF	:	float,		$
;					DPREF	:	float,		$
;					ITREF	:	integer,	$
;					TEREF	:	float,		$
;					NDENS	:	integer,	$
;					DENSA	:	fltarr(mxden),	$
;					DENPA	:	fltarr(mxden),	$
;					NTEMP	:	integer,	$
;					TEA	:	fltarr(mxtmp),	$
;					TPA	:	fltarr(mxtmp),	$
;					IBREF	:	integer,	$
;					BEREF	:	float,		$
;					NBENG	:	integer,	$
;					BMENGA	:	fltarr(mxeng),	$
;					DENSH	:	float,		$
;					JCOR	:	integer,	$
;					COR	:	fltarr(mxcor),	$
;					JMAX	:	integer,	$
;					EPSIL	:	fltarr(mxeps),	$
;					FIJ	:	fltarr(mxeps),	$
;					WIJ	:	fltarr(mxeps),	$
;					JDEF	:	integer,	$
;					DEFECT	:	fltarr(mxdef)	}
;
;			Where the elements represent the following:
;
;		NEW	- Flag which defines whether or not default values
;                         exist or not. (< 0 if no values exist)
;
;		ITOGGLE1- Flag indicating which of two parts of the processing
;			  widget are displayed:
;				0 => Display mode of operation section
;				1 => Display representative N-shell section
;
;		ITOGGLE2- Flag indicating which of three tables are displayed
;			  on the processing widget:
;				0 => Display the electron/proton density table
;				1 => Display the electron/proton temp. table
;				2 => Display the beam energy table
;
;		ITOGGLE3- Flag indicating which of two sets of parameters
;			  are displayed on the processing widget:
;				0 => Display the actual parameters section
;				1 => Display the parameter switches section
;
;		TS	- The external radiation field temperature (in eV)
;			  [Default value = 8.61668e3 eV = 1.0e8 K]
;
;		W	- The external radiation field dilution factor
;			  [Default value = 0.0]
;
;		CION	- The multiplier of ground level electron impact
;			  ionisation rate coefficient (a.k.a. "Multiplier
;			  for ionisation cross-sections") [Default = 1.0]
;
;		CPY	- The multiplier for the electron excitation rate
;			  coefficient from the ground level (a.k.a.
;			  "Multiplier for Regemorter cross-sections")
;			  [Default = 1.0]
;
;		W1	- The external ionising radiation field dilution
;			  factor for photo-ionisation from the ground level
;			  [Default = 1.0e8]
;
;		BSIM	- The Beam Species Isotope Mass - an added feature
;			  in the idl_adas version of adas310. Currently
;			  the value of BSIM will simly be passed to the
;			  processing code and not used. The default value
;			  will be determined from the value of inval.iz0
;
;		NIP	- Integer; the range of delta N for the impact
;			  parameter cross-sections [range 0-4]
;
;		INTD	- Integer; the order of Maxwell quadrature for the
;			  impact parameter cross-sections [range 0-3]
;
;		IPRS	- Integer; flag which controls cross-sections
;			  beyond NIP range:
;				0 => Default to van Regemorter cross-sections
;				1 => Use Percival-Richards cross-sections
;
;		ILOW	- Integer; flag which controls access of special,
;			  low-level data:
;				0 => No special, low-level cross-section
;				     data accessed
;				1 => Special, low-level cross-section data
;				     is accessed
;
;		IONIP	- Integer; Controls inclusion of ion impact collisions
;				0 => No ion impact collisions included
;				1 => Ion impact excitation and ionisation
;				     included
;
;		NIONIP	- Integer; Range of delta N for ion impact cross-
;			  sections [range 0-999]
;
;		ILPRS	- Integer; Controls use of Lodge-Percival-Richards
;			  ion impact cross-sections:
;				0 => Default to Vainshtein cross-sections
;				1 => Use Lodge-Percival-Richards cross-sections
;
;		IVDISP	- Integer; Controls use of beam energy in calculation
;			  of ion cross-sections:
;				0 => Ion impact at thermal Maxwellian energies
;				1 => Ion impact at displaced thermal energies
;				     according to the neutral beam energy
;				     parameter
;
;		NOSCAN	- Integer; Controls mode of operation:
;				0 => Single impurity
;				1 => Multiple impurities
;
;		IZEFF	- Integer; Nuclear charge of impurity [only set if
;			  NOSCAN=0]
;
;		NIMP	- Integer; Number of impurity species [only set if
;			  NOSCAN=1] (Maximum = mximp)
;
;		IZIMPA	- Integer array; The nuclear charges of the impurities
;			  [Entered by the user if NOSCAN=1]
;			  
;		AMIMPA	- Float array; Atomic mass numbers of the impurities
;			  [Entered by the user if NOSCAN=1]
;
;		FRIMPA	- Float array; Impurity fractions
;			  [Entered by the user if NOSCAN=1]
;
;		NMIN	- Integer; The lowest representative N-shell
;
;		NMAX	- Integer; the highest representative N-shell
;
;		IMAX	- Integer; the number of representative N-shell levels
;			  Minimum currently set to 3
;
;		NREP	- Integer array; the set of representative N-shell
;			  levels (maximum of mxnrp)
;
;		WBREP	- Float array; Unknown usage
;
;		IDREF	- Integer; Index of density reference values
;
;		DEREF	- Float; Reference electron density (cm-3)
;
;		DPREF	- Float; Reference proton density (cm-3)
;
;		ITREF	- Integer; Index of temperature reference value
;
;		TEREF	- Float; Reference electron temperature (eV)
;
;		NDENS	- Integer; number of electron/proton densities
;			  [Maximum of mxden]
;
;		DENSA	- Float array; Electron densities (cm-3)
;
;		DENPA	- Float array; Proton densities (cm-3)
;
;		NTEMP	- Integer; Number of electron/proton temperatures
;			  [Maximum of mxtmp]
;
;		TEA	- Float array; Electron temperatures (eV)
;
;		TPA	- Float array; Proton temperatures (eV)
;
;		IBREF	- Integer; Index of the reference beam energy
;
;		BEREF	- Float; Reference beam energy (eV/amu)
;
;		NBENG	- Integer; Number of beam energies in the scan
;			  [Maximum of mxeng]
;
;		BMENGA	- Float array; Beam energies in scan (eV/amu)
;
;		DENSH	- Float; Beam density (cm-3)
;
;		JCOR	- Integer; unknown
;	
;		COR	- Float array; unknown
;
;		JMAX	- Integer; unknown
;
;		EPSIL	- Float array; unknown
;
;		FIJ	- Float array; unknown
;
;		WIJ	- Float array; unknown
;
;		JDEF	- Integer; Number of quantum defects [maximum mxdef]
;
;		DEFECT	- Float array; Set of quantum defects
;
;		End of definition of PROCVAL
;
;	BITFILE - String; the path to the directory containing bitmaps
;                  for the 'escape to series menu' button.
;
;	MXIMP	- Integer; Maximum number of impurities
;
;	MXNRP	- Integer; Maximum number of representative N-shell levels
;
;	MXDEN	- Integer; Maximum number of electron/proton densities
;
;	MXTMP	- Integer; Maximum number of electron/proton temperatures
;
;	MXENG	- Integer; Maximum number of beam energies in scan
;
;	MXCOR	- Integer; Unknown
;
;	MXEPS   - Integer; Unknown
;
;       MXDEF   - Integer; Maximum number of quantum defects
;
; OPTIONAL INPUTS:
;       None
;
; OUTPUTS:
;       LPEND   - Integer, 0 if the user pressed the 'Done' button
;                 or 1 if the user pressed 'Cancel'. 
;
;       PROCVAL - On output the structure records the final settings of
;                 the processing selections widget if the user pressed the
;                 'Done' button, otherwise it is not changed from input.
;
;       GOMENU  - Int; flag - set to 1 if user has selected 'escape direct
;                 to series menu , otherwise 0
;
; OPTIONAL OUTPUTS:
;       None
;
; KEYWORD PARAMETERS:
;       FONT_LARGE - The name of a larger font.
;
;       FONT_SMALL - The name of a smaller font.
;
;       EDIT_FONTS - A structure of two fonts used in the table editor
;                    adas_edtab.pro, {font_norm:'',font_input:''}.
;
; CALLS:
;	I4Z0IE		Used to convert the atomic number inval.iz0 to the
;			element symbol
;	XXEIAM		Used to convert the above symbol to default atomic
;			mass for the beam species isotope
;       ADAS310_PROC    Invoke the IDL interface for ADAS310 data
;                       processing options/input
;
; SIDE EFFECTS:
;       None.
;
; CATEGORY:
;       Adas system.
;
; WRITTEN:
;       Tim Hammond, Tessella Support Services plc, 20th December 1995
;
; MODIFIED:
;       1.1             Tim Hammond
;                       First version.
;	1.2		Tim Hammond
;			Added several of the 'maximum number' variables in
;			the call to adas310_proc.
;	1.3		Tim Hammond
;			Added TITLE tag to procval structure.
;	1.4		Tim Hammond
;			Added ITOGGLE1 and ITOGGLE2 tags to procval structure.
;	1.5		Tim Hammond
;			Added ITOGGLE3 tag to procval structure.
;	1.6		Tim Hammond
;			Removed TITLE tag from procval (now in outval).
;	1.7		Tim Hammond
;			Set initial value of procval.idref to -1
;	1.8		Tim Hammond
;			Set initial values of procval.itref and ibref
;			to -1
;	1.9		Tim Hammond
;			Modified temperature units from Kelvin to eV
;			(they are converted to Kelvin before being
;			passed to the FORTRAN)
;
; VERSION:
;       1.1             20-12-95
;	1.2		20-12-95
;	1.3		20-12-95
;	1.4		20-12-95
;	1.5		20-12-95
;	1.6		03-01-96
;	1.7		04-01-96
;	1.8		04-01-96
;	1.9		08-02-96
;
;-
;-----------------------------------------------------------------------------

PRO caispf, inval, procval, bitfile, mximp, mxnrp, mxden, mxtmp, mxeng, $
            mxcor, mxeps, mxdef, lpend, gomenu, FONT_LARGE=font_large,  $
            FONT_SMALL=font_small, EDIT_FONTS=edit_fonts

                ;**** Set defaults for keywords ****

    IF NOT (KEYWORD_SET(font_large)) THEN font_large = ''
    IF NOT (KEYWORD_SET(font_small)) THEN font_small = ''
    IF NOT (KEYWORD_SET(edit_fonts)) THEN                               $
    edit_fonts = {font_norm:'',font_input:''}
   
                ;*******************************************
                ;**** Set default value if non provided ****
                ;*******************************************

    if (procval.new lt 0) then begin
	i4z0ie, inval.iz0, element
	xxeiam, element, bsimdef
        procval = {	NEW		:	0,			$
			ITOGGLE1	:	0,			$
			ITOGGLE2	:	0,			$
			ITOGGLE3	:	0,			$
			TS		:	8.61668e3,		$
			W		:	0.0,			$
			CION		:	1.0,			$
			CPY		:	1.0,			$
			W1		:	1.0e8,			$
			BSIM		:	bsimdef,		$
			NIP		:	-1,			$
			INTD		:	-1,			$
			IPRS		:	0,			$
			ILOW		:	0,			$
			IONIP		:	0,			$
			NIONIP		:	-1,			$
			ILPRS		:	0,			$
			IVDISP		:	0,			$
			NOSCAN		:	0,			$
			IZEFF		:	0,			$
			NIMP		:	0,			$
			IZIMPA		:	intarr(mximp),		$
			AMIMPA		:	fltarr(mximp),		$
			FRIMPA		:	fltarr(mximp),		$
			NMIN		:	0,			$
			NMAX		:	0,			$
			IMAX		:	0,			$
			NREP		:	intarr(mxnrp),		$
			WBREP		:	fltarr(mxnrp),		$
			IDREF		:	-1,			$
			DEREF		:	0.0,			$
			DPREF		:	0.0,			$
			ITREF		:	-1,			$
			TEREF		:	0.0,			$
			NDENS		:	0,			$
			DENSA		:	fltarr(mxden),		$
			DENPA		:	fltarr(mxden),		$
			NTEMP		:	0,			$
			TEA		:	fltarr(mxtmp),		$
			TPA		:	fltarr(mxtmp),		$
			IBREF		:	-1,			$
			BEREF		:	0.0,			$
			NBENG		:	0,			$
			BMENGA		:	fltarr(mxeng),		$
			DENSH		:	-1.0,			$
			JCOR		:	0,			$
			COR		:	fltarr(mxcor),		$
			JMAX		:	0,			$
			EPSIL		:	fltarr(mxeps),		$
			FIJ		:	fltarr(mxeps),		$
			WIJ		:	fltarr(mxeps),		$
			JDEF		:	0,			$
			DEFECT		:	fltarr(mxdef)		}
    endif

		;***************************************************
		;**** Set values not yet accessible to the user ****
		;***************************************************

    procval.wbrep(*) = 0.0
    procval.jcor = 1
    procval.cor(0) = 0.0
    procval.jmax = 1
    procval.epsil(0) = 0.75
    procval.fij(0) = 0.0
    procval.wij(0) = 0.0
    procval.jdef = 1
    procval.defect(0) = 0.0

                ;****************************************
                ;**** Pop-up processing input widget ****
                ;****************************************

    adas310_proc, inval, procval, bitfile, mximp, mxnrp, mxden, 	$
                  mxtmp, mxeng, action,	FONT_LARGE=font_large, 		$
                  FONT_SMALL=font_small, EDIT_FONTS=edit_fonts

                ;********************************************
                ;****  Act on the event from the widget  ****
                ;****  There are 3 possible events:      ****
		;**** 'Done', 'Cancel' or 'Menu'         ****
                ;********************************************

    if action eq 'Done' then begin
        lpend = 0
    endif else if action eq 'Menu' then begin
        lpend = 0
        gomenu = 1
    endif else begin
        lpend = 1
    endelse

END
