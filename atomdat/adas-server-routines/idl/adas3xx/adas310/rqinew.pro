;----------------------------------------------------------------------
;+
; PROJECT    :  ADAS
;
; NAME       :  rqinew
;
; PURPOSE    :  Calculates Percival-Richards ion impact ionisation rate.
;
; ARGUMENTS  :  All output arguments will be defined appropriately.
;               Inputs will be converted to correct type, if possible,
;               internally without changing calling type.
;                   result = rqinew(iz=iz, n=n, zimp=zimp, mass=mass, vdisp=vdisp, te=te)
;
;
;               NAME      I/O    TYPE    DETAILS
; REQUIRED   :  iz         I     integer Target ion charge+1
;               n          I     integer Principal quantum  number of initial
;                                        target level
;               zimp       I     integer Projectile charge
;               mass       I     double  Projectile mass (proton units)
;               vdisp      I     double  Constant mean speed shift for the
;                                        collision (beam picture)
;               te         I     double  Ion temperature
;
;
; KEYWORDS      kelvin           requested temperature in K (default eV)
;               help             prints help to screen
;
;
; NOTES      :  Calls the fortran code.
;               Units of result are cm^3/s
;
;
; AUTHOR     :  Martin O'Mullane
;
; DATE       :  18-09-2008
;
;
; MODIFIED:
;         1.1   Martin O'Mullane
;                - First version.
;
; VERSION:
;         1.1   18-09-2008
;-
;----------------------------------------------------------------------

FUNCTION rqinew, iz     = iz,     $
                 n      = n,      $
                 zimp   = zimp,   $
                 mass   = mass,   $
                 vdisp  = vdisp,  $
                 te     = te,     $
                 kelvin = kelvin, $
                 help   = help


; If asked for help

if keyword_set(help) then begin
   doc_library, 'rqinew'
   return, -1L
endif

fortdir = getenv('ADASFORT')
fortdir = fortdir

if n_elements(iz) EQ 0 OR   $
   n_elements(n) EQ 0 OR   $
   n_elements(zimp) EQ 0 OR $
   n_elements(mass) EQ 0 OR $
   n_elements(vdisp) EQ 0 OR $
   n_elements(te) EQ 0      $
   then message, 'essential parameters missing'

; inputs

n_te = n_elements(te)

; Kelvin for routine - assume i/p in eV unless /kelvin

if keyword_set(kelvin) then tp_in = double(te) else tp_in = double(te * 11605.0)

z_in     = double(iz)
n_in     = long(n)
zimp_in  = double(zimp)
mass_in  = double(mass)
vdisp_in = double(vdisp)

; results

rate = dblarr(n_te)

dummy = CALL_EXTERNAL(fortdir+'/rqinew_if.so','rqinew_if', $
                      n_te, z_in, n_in, zimp_in, mass_in, vdisp_in, tp_in, rate)

return, rate

END
