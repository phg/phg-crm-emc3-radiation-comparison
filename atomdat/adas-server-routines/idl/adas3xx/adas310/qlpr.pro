;----------------------------------------------------------------------
;+
; PROJECT    :  ADAS
;
; NAME       :  qlpr
;
; PURPOSE    :  Calculates Lodge-Percival-Richards ion impact excitation
;               cross section (J.Phys.B. (1976)9,239) with scaling
;               to arbitrary projectile charge following recommendations
;               of Reinnhold, Olsen & Fritsch (Phys.Rev.A (1990) 41,4837).
;
; ARGUMENTS  :  All output arguments will be defined appropriately.
;               Inputs will be converted to correct type, if possible,
;               internally without changing calling type.
;                   result = qlpr(iz=iz, nlow=n1, ....)
;
;
;               NAME      I/O    TYPE    DETAILS
; REQUIRED   :  iz         I     integer Target ion charge+1
;               nlow       I     integer Principal quantum  number of
;                                        initial level
;               nup        I     integer Principal quantum  number of
;                                        final level
;               zimp       I     integer Projectile charge
;               energy     I     double  Energy of equivalent electron (eV)
;               mass       I     double  Projectile mass (proton units)
;               dex_sigma  O     double  De-excitation cross section (cm2).
;
;
; KEYWORDS      help             prints help to screen
;
;
; NOTES      :  Calls the fortran code.
;               Units of result are cm^2
;
;
; AUTHOR     :  Martin O'Mullane
;
; DATE       :  20-10-2010
;
;
; MODIFIED:
;         1.1   Martin O'Mullane
;                - First version.
;
; VERSION:
;         1.1   20-10-2010
;-
;----------------------------------------------------------------------

FUNCTION qlpr, iz        = iz,         $
               nlow      = nlow,       $
               nup       = nup,        $
               zimp      = zimp,       $
               mass      = mass,       $
               energy    = energy,     $
               dex_sigma = dex_sigma,  $
               help      = help


; If asked for help

if keyword_set(help) then begin
   doc_library, 'qlpr'
   return, -1L
endif

fortdir = getenv('ADASFORT')
fortdir = fortdir

if n_elements(iz)    EQ 0 OR   $
   n_elements(nlow)  EQ 0 OR   $
   n_elements(nup)   EQ 0 OR   $
   n_elements(zimp)  EQ 0 OR   $
   n_elements(mass)  EQ 0 OR   $
   n_elements(energy)EQ 0      $
   then message, 'essential parameters missing'

; inputs

n_eng = n_elements(energy)

if nlow EQ nup then message, 'nlow cannot be same as nup'

if nlow LT nup then begin
   nl_in    = long(nlow)
   nu_in    = long(nup)
endif else begin
   nl_in    = long(nup)
   nu_in    = long(nlow)
endelse

z_in      = double(iz)
zimp_in   = double(zimp)
mass_in   = double(mass)

; convert energy to reduced units E/25keV

energy_in = double(energy) / 25.0e3


; results

sigma = dblarr(n_eng)

dummy = CALL_EXTERNAL(fortdir+'/qlpr_if.so','qlpr_if', $
                      n_eng, z_in, nl_in, nu_in, energy_in, zimp_in, $
                      mass_in, sigma)
sigma = sigma * !dpi * 5.29177D-9^2.0

if arg_present(dex_sigma) then begin

   dex_sigma = dblarr(n_eng)
   dummy = CALL_EXTERNAL(fortdir+'/qlpr_if.so','qlpr_if', $
                         n_eng, z_in, nu_in, nl_in, energy_in, zimp_in, $
                         mass_in, dex_sigma)

   dex_sigma = dex_sigma * !dpi * 5.29177D-9^2.0

endif

return, sigma

END
