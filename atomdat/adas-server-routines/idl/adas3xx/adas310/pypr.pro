;----------------------------------------------------------------------
;+
; PROJECT    :  ADAS
;
; NAME       :  pypr
;
; PURPOSE    :  Calculates PY Factor (cf. Van Regemorter, 1962) using
;               Percival, Richard and coworkers cross-sections.
;
;               Valid only for electron induced transitions between whole principal
;               quantum shells in hydrogen and hydrogenic ions, for n,n11>4.
;               However adjustments made to allow use of formulae for n<4.
;
; ARGUMENTS  :  All output arguments will be defined appropriately.
;               Inputs will be converted to correct type, if possible,
;               internally without changing calling type.
;
;                pypr
;
;               NAME      I/O    TYPE    DETAILS
; REQUIRED   :  E          I     float   1/v**2 with v the initial effective principal quantum number
;               E11        I     float   1/v11**2 with v11 the final effective principal quantum number
;               N          I     int     Initial principal quantum number
;               N11        I     int     Final principal quantum number (require n11>n and v11>v)
;               EM         I     float   Reduced mass of colliding particle (must be 1.0 in this case)
;               Z1         I     float   Target ion charge +1
;               PHI        I     float   (Ih/Eij)*F with eij=transition energy, F=abs. oscill. strength
;               WI         I     float   Statistical weight of initial level
;               WJ         I     float   Statistical weight of final level
;               TE         I     float() Electron temperature (K)
;               INTD       I     int      <3 for two point gaussian quadrature.
;                                         =3 for three point gaussian quadrature
;                                         >3 for four point gaussian quadrature
;               PY         O     float   Van Regemorter P factor
;               RDEXC      O     float   Dexcitation rate coefficient(cm+3 sec-1)
;               EXC        O     float() Excitation rate coefficient (cm+3 sec-1)
;
; KEYWORDS      help             prints help to screen
;
;
; NOTES      :  Calls fortran code.
;               Arguments same as fortran with exception that the excitation
;               rate (EXC) is output in addition to the P-factor. Another
;               enhancement is that temperature may be a vector.
;
;
; AUTHOR     :  Martin O'Mullane
;
; DATE       :  16/03/11
;
;
; MODIFIED:
;       1.1     Martin O'Mullane
;              	- First version.
;
;
; VERSION:
;       1.1    16-03-2011
;
;-
;-------------------------------------------------------------------------

PRO pypr, E, E11, N, N11, EM, Z1, PHI, WI, WJ, TE, INTD, PY, EXC, RDEXC, $
          help = help

; If asked for help

if keyword_set(help) then begin
   doc_library, 'pypr'
   return
endif

e_in    =  double(e)
e11_in  =  double(e11)
n_in    =  long(n)
n11_in  =  long(n11)
em_in   =  double(em)
z1_in   =  double(z1)
phi_in  =  double(phi)
wi_in   =  double(wi)
wj_in   =  double(wj)
te_in   =  double(te)
intd_in =  long(intd)

n_te    = n_elements(te_in)

py      = dblarr(n_te)
exc     = dblarr(n_te)
rdexc   = dblarr(n_te)

fortdir = getenv('ADASFORT')

for j = 0, n_te-1 do begin

   py_tmp    = 0.0D0
   rdexc_tmp = 0.0D0
   dummy = CALL_EXTERNAL(fortdir+'/pypr_if.so', 'pypr_if',      $
                         e_in, e11_in, n_in, n11_in, em_in,     $
                         z1_in, phi_in, wi_in, wj_in, te_in[j], $
                         intd_in, py_tmp, rdexc_tmp)
   py[j]    = py_tmp
   rdexc[j] = rdexc_tmp

endfor

exc = wj_in * rdexc / (wi_in * exp((e_in-e11_in)*13.6/(te_in/8066.0)))

END
