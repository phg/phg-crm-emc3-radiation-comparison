; Copyright (c) 1996, Strathclyde University.
; SCCS info: Module @(#)$Header: /home/adascvs/idl/adas3xx/adas310/cw_adas310_proc.pro,v 1.12 2004/07/06 12:41:22 whitefor Exp $ Date $Date: 2004/07/06 12:41:22 $
;+
; PROJECT:
;       ADAS IBM MVS to UNIX conversion
;
; NAME:
;       CW_ADAS310_PROC()
;
; PURPOSE:
;       Produces a widget for ADAS310 processing options/input.
;
; EXPLANATION:
;       This function creates a compound widget consisting of :-
;	   a three-way button box widget controlling the display
;	   of parameters for user input,
;	   several typeins and menu widgets for entering/displaying
;	   these parameters,
;	   a two-way button box widget for controlling the display
;	   of two sets of data widgets: 
;		(1) A set of widgets associated with the impurities
;		    required in the calculation
;	  	(2) A set of widgets associated with the representative
;		    N-shells required in the calculation
;	   a three-way button box widget for controlling the display
;	   of three sets of output scan values:
;		(1) A set of widgets for the output electron and proton
;		    densities
;		(2) A set of widgets for the output electron/proton
;		    temperatures
;		(3) A set of widgets for the output beam energies
;          a message widget, and 'Cancel', 'Menu' and 'Done' buttons.
;
;       The compound widgets included in this widget are self managing.
;       This widget only manages events from the 'Cancel' button, the
;	'Menu' button and the 'Done' button.
;
;       This widget only generates events for the 'Done', 'Menu' and
;       'Cancel' buttons.
;
; USE:
;       This widget is specific to ADAS310, see adas310_proc.pro
;       for use.
;
; INPUTS:
;       TOPPARENT  - Long integer, ID of parent widget.
;
;       BITFILE    - String; the path to the directory containing bitmaps
;                    for the 'escape to series menu' button.
;
;       INVAL      - A structure containing the final settings of the input
;                    options screen.
;
;       MXNRP   - Integer; maximum number of representative N-shells
;
;       MXDEN   - Integer; Maximum number of electron/proton densities
;
;       MXTMP   - Integer; Maximum number of electron/proton temperatures
;
;       MXENG   - Integer; Maximum number of beam energies in scan
;
;       MXIMP   - Integer; Maximum number of impurities
;
; OPTIONAL INPUTS:
;       None
;
; OUTPUTS:
;       The return value of this function is the ID of the declared widget.
;
; OPTIONAL OUTPUTS:
;       None
;
; KEYWORD PARAMETERS:
;       PROCVAL - A structure which determines the initial settings of
;                 the processing options widget.
;
;                 The default PROCVAL is created in caispf.pro - see that
;		  routine for a detailed description.
;
;       UVALUE  - A user value for the widget. Default 0.
;
;       FONT_LARGE - The name of a larger font.  Default current system
;                    font
;
;       FONT_SMALL - The name of a smaller font. Default current system
;                    font.
;
;       EDIT_FONTS - A structure of two fonts used in the table editor
;                    adas_edtab.pro, {font_norm:'',font_input:''}.  Defaults
;                    to current system font.
;
;       NUM_FORM   - String; Numeric format to use in tables.  Default
;                       '(E10.3)'
;
;
; CALLS:
;       CW_ADAS_TABLE   Adas data table widget.
;       READ_X11_BITMAP Reads in the bitmap for the 'escape to series
;                       menu' button.
;       NUM_CHK         Checks validity of a user's numbers
;	CW_BGROUP	Button list compound widget
;	CW_BSELECTOR	Pull-down menu compound widget
;	I4Z0IE		Converts atomic number into element symbol
;	I4EIZ0		Converts element symbol into atomic number
;	XXEIAM		Converts element symbol into atomic mass
;
;       See side effects for other related widget management routines.
;
; SIDE EFFECTS:
;
;       The following widget management routines are included in this file;
;       PROC310_GET_VAL()       Returns the current PROCVAL structure.
;       PROC310_EVENT()         Process and issue events.
;
; CATEGORY:
;       Compound Widget
;
; WRITTEN:
;       Tim Hammond, Tessella Support Services plc, 20th December 1995
;
; MODIFIED:
;       1.1     Tim Hammond
;               First version - incomplete
;	1.2	Tim Hammond
;		First version containing all widgets - no events handled yet
;	1.3	Tim Hammond
;		Added checking of all input parameters and parameter switches
;	1.4	Tim Hammond
;		Added checking of impurity data
;	1.5	Tim Hammond
;		Added checking of representative N-shell data
;	1.6	Tim Hammond
;		Completed checking of all input data
;	1.7	Tim Hammond
;		Complete working version
;	1.8	Tim Hammond
;		Modified comments slightly
;	1.9	Tim Hammond
;		Added functionality to clear table buttons.
;		Ensured that all temperatures are interpreted in eV 
;		rather than K.
;		Altered size of arrays into terms of the parameters
;		set in adas310.pro to allow them to be changed more 
;		easily.
;	1.10	Harvey Anderson
;		Altered the format statements associated with reading
;		the impurity fraction from the table. Originally the
;		routine would only read data upto 2 decimal places.
;		This was changed to allow program to read up to 4
;		decimal places.
;		Altered the array tabben so that it is  strarr(1,25),
;		previously it was set to strarr(1,20), since I
;		changed mxeng, mxden and mxtmp to 25. 
;	1.11	Richard Martin
;		Changed the above version no. from 2.0 to 1.10
;	1.12	Richard martin
;		Changed widget label name switch to switchl for IDL 5.4 compatibility.
; VERSION:
;       1.1     20-12-95
;	1.2	04-01-96
;	1.3	05-01-96
;	1.4	05-01-96
;	1.5	08-01-96
;	1.6	09-01-96
;	1.7	10-01-96
;	1.8	10-01-96
;	1.9	08-02-96
;	1.10	04-02-97
;	1.11	28-03-97
;	1.12	10-11-00
;-
;-----------------------------------------------------------------------------

FUNCTION proc310_get_val, id

    COMMON cw_proc310_blk, jcor, cor, jmax, epsil, fij, wij, jdef, 	$
			   defect, wbrep0, iz0, izeff0, izimpa0, 	$
			   amimpa0, frimpa0, nrep0, densa0, denpa0, 	$
			   tea0, tpa0, bmenga0, nimp0

		;***********************************
                ;**** Return to caller on error ****
		;***********************************

    ON_ERROR, 2

                 ;***************************************
                 ;****     Retrieve the state        ****
                 ;**** Get first_child widget id     ****
                 ;**** because state is stored there ****
                 ;***************************************

    first_child = widget_info(id, /child)
    widget_control, first_child, get_uvalue = state

		;***************************
		;**** Get toggle values ****
		;***************************

    widget_control, state.base1id, get_value=itoggle1
    widget_control, state.base2id, get_value=itoggle2
    widget_control, state.paramid, get_value=itoggle3

		;********************************
		;**** Get general parameters ****
		;********************************

    widget_control, state.radid, get_value=radval
    widget_control, state.genid, get_value=genval
    widget_control, state.mulid, get_value=mulval
    widget_control, state.mul2id, get_value=mul2val
    widget_control, state.ionid, get_value=ionval
    widget_control, state.isoid, get_value=isoval
    if strcompress(radval(0), /remove_all) eq '' then begin
	ts = 8.61668e3			;In eV
    endif else begin
        ts = float(radval(0))
    endelse
    if strcompress(genval(0), /remove_all) eq '' then begin
        w = 0.0
    endif else begin
        w = float(genval(0))
    endelse
    if strcompress(mulval(0), /remove_all) eq '' then begin
	cion = 1.0
    endif else begin
        cion = float(mulval(0))
    endelse
    if strcompress(mul2val(0), /remove_all) eq '' then begin
	cpy = 1.0
    endif else begin
        cpy = float(mul2val(0))
    endelse
    if strcompress(ionval(0), /remove_all) eq '' then begin
        w1 = 1.0e8
    endif else begin
        w1 = float(ionval(0))
    endelse
    if strcompress(isoval(0), /remove_all) eq '' then begin
	i4z0ie, iz0, element
        xxeiam, element, bsim
    endif else begin
        bsim = float(isoval(0))
    endelse

		;************************************
		;**** Get parameter switches (I) ****
		;************************************

    widget_control, state.delid, get_value=delval
    widget_control, state.intid, get_value=intval
    widget_control, state.prxid, get_value=iprs
    widget_control, state.accid, get_value=ilow
    nip = fix(delval(0))
    intd = fix(intval(0))

		;************************************
		;**** Get parameter switches (I) ****
		;************************************

    widget_control, state.actid, get_value=ionip
    widget_control, state.dnrid, get_value=dnrval
    widget_control, state.lodid, get_value=ilprs
    widget_control, state.beaid, get_value=ivdisp
    nionip = fix(dnrval(0))

		;**********************************
		;**** Get impurity information ****
		;**********************************

    widget_control, state.scanid, get_value=noscan
    izimpa = izimpa0
    amimpa = amimpa0
    frimpa = frimpa0
    if noscan eq 0 then begin
        widget_control, state.symid, get_value=symval
        i4eiz0, symval(0), izeff
	nimp = nimp0
    endif else begin
	izeff = izeff0
	widget_control, state.imptabid, get_value=imptable
	impvals = where(strcompress(imptable.value(0,*), /remove_all)	$
        ne '')
	nimp = n_elements(impvals)
	for i=0, (nimp-1) do begin
	    symbol = strcompress(imptable.value(0,i), /remove_all)
	    i4eiz0, symbol, izeff1
	    izimpa(i) = izeff1
	endfor
	amimpa(0:nimp-1) = float(imptable.value(1,0:nimp-1))
	frimpa(0:nimp-1) = float(imptable.value(2,0:nimp-1))
	if nimp lt n_elements(izimpa0) then begin
	    izimpa(nimp:*) = 0
	    amimpa(nimp:*) = 0.0
	    frimpa(nimp:*) = 0.0
	endif
    endelse

		;*****************************************
		;**** Get representative N-shell data ****
		;*****************************************

    nrep = nrep0
    widget_control, state.nminid, get_value=nminval
    widget_control, state.nmaxid, get_value=nmaxval
    nmin = fix(nminval(0))
    nmax = fix(nmaxval(0))
    widget_control, state.nshtabid, get_value=nshtable
    nshvals = where(strcompress(nshtable.value(0,*), /remove_all) ne '')
    imax = n_elements(nshvals)
    nrep(0:imax-1) = fix(nshtable.value(0,0:imax-1))
    if imax lt n_elements(nrep0) then nrep(imax:*) = 0

		;******************************************
		;**** Get electron/proton density data ****
		;******************************************

    densa = densa0
    denpa = denpa0
    widget_control, state.dentabid, get_value=dentable
    widget_control, state.denrefid, get_value=idval
    idref = fix(idval(0))
    deref = float(dentable.value(0,(idref-1)))
    dpref = float(dentable.value(1,(idref-1)))
    denvals = where(strcompress(dentable.value(0,*), /remove_all) ne '')
    ndens = n_elements(denvals)
    densa(0:ndens-1) = float(dentable.value(0,0:ndens-1))
    denpa(0:ndens-1) = float(dentable.value(1,0:ndens-1))
    if ndens lt n_elements(densa0) then begin
	densa(ndens:*) = 0.0
	denpa(ndens:*) = 0.0
    endif

		;**********************************************
		;**** Get electron/proton temperature data ****
		;**********************************************

    tea = tea0
    tpa = tpa0
    widget_control, state.temtabid, get_value=temtable
    widget_control, state.temrefid, get_value=itval
    itref = fix(itval(0))
    teref = float(temtable.value(0,(itref-1)))
    temvals = where(strcompress(temtable.value(0,*), /remove_all) ne '')
    ntemp = n_elements(temvals)
    tea(0:ntemp-1) = float(temtable.value(0,0:ntemp-1))
    if ntemp lt n_elements(tea0) then tea(ntemp:*) = 0.0
    tpa = tea

		;**************************************
		;**** Get beam energy/density data ****
		;**************************************

    bmenga = bmenga0
    widget_control, state.bentabid, get_value=bentable
    widget_control, state.benrefid, get_value=ibval
    widget_control, state.beamdenid, get_value=beamdenval
    ibref = fix(ibval(0))
    beref = float(bentable.value(0,(ibref-1)))
    benvals = where(strcompress(bentable.value(0,*), /remove_all) ne '')
    nbeng = n_elements(benvals)
    bmenga(0:nbeng-1) = float(bentable.value(0,0:nbeng-1))
    if nbeng lt n_elements(bmenga0) then bmenga(nbeng:*) = 0.0
    densh = float(beamdenval(0))

                ;***********************************************
                ;**** write selected values to PS structure ****
                ;***********************************************

     ps = {             new             :       0,			$
			itoggle1	:	itoggle1,		$
			itoggle2	:	itoggle2,		$
			itoggle3	:	itoggle3,		$
			ts		:	ts,			$
			w		:	w,			$
			cion		:	cion,			$
			cpy		:	cpy,			$
			w1		:	w1,			$
			bsim		:	bsim,			$
			nip		:	nip,			$
			intd		:	intd,			$
			iprs		:	iprs,			$
			ilow		:	ilow,			$
			ionip		:	ionip,			$
			nionip		:	nionip,			$
			ilprs		:	ilprs,			$
			ivdisp		:	ivdisp,			$
			noscan		:	noscan,			$
			izeff		:	izeff,			$
			nimp		:	nimp,			$
			izimpa		:	izimpa,			$
			amimpa		:	amimpa,			$
			frimpa		:	frimpa,			$
			nmin		:	nmin,			$
			nmax		:	nmax,			$
			imax		:	imax,			$
			nrep		:	nrep,			$
			wbrep		:	wbrep0,			$
			idref		:	idref,			$
			deref		:	deref,			$
			dpref		:	dpref,			$
			itref		:	itref,			$
			teref		:	teref,			$
			ndens		:	ndens,			$
			densa		:	densa,			$
			denpa		:	denpa,			$
			ntemp		:	ntemp,			$
			tea		:	tea,			$
			tpa		:	tpa,			$
			ibref		:	ibref,			$
			beref		:	beref,			$
			nbeng		:	nbeng,			$
			bmenga		:	bmenga,			$
			densh		:	densh,			$
			jcor		:	jcor,			$
			cor		:	cor,			$
			jmax		:	jmax,			$
			epsil		:	epsil,			$
			fij		:	fij,			$
			wij		:	wij,			$
			jdef		:	jdef,			$
			defect		:	defect			}

    widget_control, first_child, set_uvalue=state, /no_copy

    RETURN, ps

END


;-----------------------------------------------------------------------------

FUNCTION proc310_event, event

		;************************************
                ;**** Base ID of compound widget ****
		;************************************

    parent=event.handler

                ;**********************************************
                ;**** Retrieve the user value state        ****
                ;**** Get id of first_child widget because ****
                ;**** user value "state" is stored there   ****
                ;**********************************************

    first_child = widget_info(parent, /child)
    widget_control, first_child, get_uvalue=state, /no_copy

                ;*********************************
                ;**** Clear previous messages ****
                ;*********************************

    widget_control, state.messid, set_value=' '

                ;************************
                ;**** Process events ****
                ;************************

    CASE event.id OF

		;**************************
		;**** Parameter switch ****
		;**************************

	state.paramid: begin
	    widget_control, state.paramid, get_value=itoggle3
	    if itoggle3 eq 0 then begin
	    	widget_control, state.parambase2, map=1
	    	widget_control, state.parambase3, map=0
	    	widget_control, state.parambase4, map=0
	    endif else if itoggle3 eq 1 then begin
	    	widget_control, state.parambase2, map=0
	    	widget_control, state.parambase3, map=1
	    	widget_control, state.parambase4, map=0
	    endif else begin
	    	widget_control, state.parambase2, map=0
	    	widget_control, state.parambase3, map=0
	    	widget_control, state.parambase4, map=1
 	    endelse
	end

		;***************************
		;**** Parameter typeins ****
		;***************************

	state.radid: widget_control, state.genid, /input_focus
	state.genid: widget_control, state.mulid, /input_focus
	state.mulid: widget_control, state.mul2id, /input_focus
        state.mul2id: widget_control, state.ionid, /input_focus
        state.ionid: widget_control, state.isoid, /input_focus
	state.isoid: widget_control, state.radid, /input_focus

		;****************************
		;**** Parameter switches ****
		;****************************

	state.delid: widget_control, state.intid, /input_focus
        state.intid: widget_control, state.delid, /input_focus

		;*********************************
		;**** First table base switch ****
		;*********************************

	state.base1id: begin
	    widget_control, state.base1id, get_value=itoggle1
	    widget_control, state.impbase, map=(1-itoggle1)
	    widget_control, state.nshtop, map=itoggle1
	end

		;**********************************
		;**** Second table base switch ****
		;**********************************

	state.base2id: begin
	    widget_control, state.base2id, get_value=itoggle2
	    if itoggle2 eq 0 then begin
		widget_control, state.denbase, map=1
		widget_control, state.tembase, map=0
		widget_control, state.benbase, map=0
	    endif else if itoggle2 eq 1 then begin
		widget_control, state.denbase, map=0
		widget_control, state.tembase, map=1
		widget_control, state.benbase, map=0
	    endif else begin
		widget_control, state.denbase, map=0
		widget_control, state.tembase, map=0
		widget_control, state.benbase, map=1
	    endelse
	end

		;******************************
		;**** Impurity mode switch ****
		;******************************

	state.scanid: begin
	    widget_control, state.scanid, get_value=noscan
	    widget_control, state.impbase2, map=(1-noscan)
	    widget_control, state.impbase3, map=noscan
        end

		;*************************
		;**** N-Shell typeins ****
		;*************************

	state.nminid: widget_control, state.nmaxid, /input_focus
	state.nmaxid: widget_control, state.nminid, /input_focus

		;*******************************
		;**** N-shell 'clear table' ****
		;*******************************

	state.nshclearid: begin

		;****************************************
                ;**** Get current table widget value ****
		;****************************************

	    widget_control, state.nshtabid, get_value=tempval
	    tempvals = where(strcompress(tempval.value(0,*), 		$
                             /remove_all) ne '')
	    if tempvals(0) ne -1 then begin

		;*********************************	
		;**** Popup window to confirm ****
		;*********************************

		messval = 'Are you sure you want to clear the table?'
                action = popup(message=messval, font=state.font_large,	$
                               buttons=['Confirm','Cancel'], 		$
                               title='ADAS310 Warning:-')

                if action eq 'Confirm' then begin

		;***********************************
		;**** Create a dummy zero array ****
		;***********************************

		    empty = strarr(state.mxnrp)
		    tempval.value(0,*) = empty

		;***************************************
                ;**** Copy new data to table widget ****
		;***************************************

		    widget_control, state.nshtabid, set_value=tempval
	        endif
	    endif
	end

                ;*******************************
                ;**** Density 'clear table' ****
                ;*******************************

        state.denclearid: begin

                ;****************************************
                ;**** Get current table widget value ****
                ;****************************************

            widget_control, state.dentabid, get_value=tempval
            tempvals = where(strcompress(tempval.value(0,*),            $
                             /remove_all) ne '')
            if tempvals(0) ne -1 then begin

                ;*********************************
                ;**** Popup window to confirm ****
                ;*********************************

                messval = 'Are you sure you want to clear the table?'
                action = popup(message=messval, font=state.font_large,  $
                               buttons=['Confirm','Cancel'],            $
                               title='ADAS310 Warning:-')

                if action eq 'Confirm' then begin

                ;***********************************
                ;**** Create a dummy zero array ****
                ;***********************************                    

		    empty = strarr(state.mxden)
                    tempval.value(0,*) = empty
                    tempval.value(1,*) = empty

                ;***************************************
                ;**** Copy new data to table widget ****
                ;***************************************

                    widget_control, state.dentabid, set_value=tempval
                endif
            endif
        end

                ;***********************************
                ;**** Temperature 'clear table' ****
                ;***********************************

        state.temclearid: begin

                ;****************************************
                ;**** Get current table widget value ****
                ;****************************************

            widget_control, state.temtabid, get_value=tempval
            tempvals = where(strcompress(tempval.value(0,*),            $
                             /remove_all) ne '')
            if tempvals(0) ne -1 then begin

                ;*********************************
                ;**** Popup window to confirm ****
                ;*********************************

                messval = 'Are you sure you want to clear the table?'
                action = popup(message=messval, font=state.font_large,  $
                               buttons=['Confirm','Cancel'],            $
                               title='ADAS310 Warning:-')

                if action eq 'Confirm' then begin

                ;***********************************
                ;**** Create a dummy zero array ****
                ;***********************************                    

		    empty = strarr(state.mxtmp)
                    tempval.value(0,*) = empty

                ;***************************************
                ;**** Copy new data to table widget ****
                ;***************************************

                    widget_control, state.temtabid, set_value=tempval
                endif
            endif
	end

                ;***********************************
                ;**** Beam energy 'clear table' ****
                ;***********************************

        state.benclearid: begin

                ;****************************************
                ;**** Get current table widget value ****
                ;****************************************

            widget_control, state.bentabid, get_value=tempval
            tempvals = where(strcompress(tempval.value(0,*),            $
                             /remove_all) ne '')
            if tempvals(0) ne -1 then begin

                ;*********************************
                ;**** Popup window to confirm ****
                ;*********************************

                messval = 'Are you sure you want to clear the table?'
                action = popup(message=messval, font=state.font_large,  $
                               buttons=['Confirm','Cancel'],            $
                               title='ADAS310 Warning:-')

                if action eq 'Confirm' then begin

                ;***********************************
                ;**** Create a dummy zero array ****
                ;***********************************                    

		    empty = strarr(state.mxeng)
                    tempval.value(0,*) = empty

                ;***************************************
                ;**** Copy new data to table widget ****
                ;***************************************

                    widget_control, state.bentabid, set_value=tempval
                endif
            endif
	end

		;*************************************
		;**** Beam energy/density typeins ****
		;*************************************

	state.benrefid: widget_control, state.beamdenid, /input_focus
	state.beamdenid: widget_control, state.benrefid, /input_focus

                ;***********************
                ;**** Cancel button ****
                ;***********************

        state.cancelid: new_event = {ID:parent, TOP:event.top,              $
                                     HANDLER:0L, ACTION:'Cancel'}

                ;*********************
                ;**** Menu button ****
                ;*********************

        state.outid: new_event = {ID:parent, TOP:event.top,                 $
                                  HANDLER:0L, ACTION:'Menu'}

                ;*********************
                ;**** Done button ****
                ;*********************

        state.doneid: begin

                ;***************************************
                ;**** Check all user input is legal ****
                ;***************************************

            error = 0
	    confirm = 0

		;**************************
		;**** Check parameters ****
		;**************************

	    widget_control, state.radid, get_value=radval
	    radchk = strcompress(radval(0), /remove_all)
	    if num_chk(radval(0), sign=1) ne 0 and radchk ne '' then begin
		error = 1
	 	message = '**** Error: The radiation field temperature'+ $
                          ' is invalid ****'
		widget_control, state.radid, /input_focus
	    endif
	    if error eq 0 then begin
		widget_control, state.genid, get_value=genval
	        genchk = strcompress(genval(0), /remove_all)
		if num_chk(genval(0), sign=1) ne 0 and genchk ne '' then begin
                    error = 1
                    message = '**** Error: The general radiation field'+ $
                              ' dilution is invalid ****'
		    widget_control, state.genid, /input_focus
		endif
	    endif
            if error eq 0 then begin
                widget_control, state.mulid, get_value=mulval
	        mulchk = strcompress(mulval(0), /remove_all)
                if num_chk(mulval(0), sign=1) ne 0 and mulchk ne '' then begin
                    error = 1
                    message = '**** Error: The multiplier for ionisation'+$
                              ' cross-sections is invalid ****'
		    widget_control, state.mulid, /input_focus
                endif
            endif
            if error eq 0 then begin
                widget_control, state.mul2id, get_value=mul2val
	        mul2chk = strcompress(mul2val(0), /remove_all)
                if num_chk(mul2val(0), sign=1) ne 0 and mul2chk ne '' then begin
                    error = 1
                    message = '**** Error: The multiplier for regemorter'+$
                              ' cross-sections is invalid ****'
		    widget_control, state.mul2id, /input_focus
                endif
            endif
            if error eq 0 then begin
                widget_control, state.ionid, get_value=ionval
	        ionchk = strcompress(ionval(0), /remove_all)
                if num_chk(ionval(0), sign=1) ne 0 and ionchk ne '' then begin
                    error = 1
                    message = '**** Error: The ionising radiation field'+ $
                              ' dilution is invalid ****'
		    widget_control, state.ionid, /input_focus
                endif
            endif
            if error eq 0 then begin
                widget_control, state.isoid, get_value=isoval
	        isochk = strcompress(isoval(0), /remove_all)
                if num_chk(isoval(0), sign=1) ne 0 and isochk ne '' then begin
                    error = 1
                    message = '**** Error: The beam species isotope mass'+$
			      ' is invalid ****'
		    widget_control, state.isoid, /input_focus
                endif
            endif

		;*****************************************************
		;**** If there was an error bring the appropriate ****
		;**** widget to the fore                          ****
		;*****************************************************

	    if error eq 1 then begin
		widget_control, state.paramid, set_value=0
		widget_control, state.parambase2, map=1
		widget_control, state.parambase3, map=0
		widget_control, state.parambase4, map=0
	    endif

		;**************************************
		;**** Check parameter switches (I) ****
		;**************************************
	
	    error2 = 0
	    if error eq 0 then begin
		widget_control, state.delid, get_value=delval
		delchk = strcompress(delval(0), /remove_all)
		if num_chk(delval(0), sign=1, /integer) ne 0 and 	$
		delchk ne '' then begin
		    error = 1
		    error2 = 1
		    message = '**** Error: The delta N range for impac'+$
                              't parameter cross-sections is invalid ****'
		endif else begin
		    if delchk eq '' then begin
		        error = 1
		        error2 = 1
		        message = '**** Error: You must enter '+	$
                                  'the delta N range of'+		$
                                  ' input parameter cross-sections'+	$
                                  ' ****'
		    endif
		    if error eq 0 then begin
		        if fix(delval(0)) gt 4 then begin
			    error = 1
			    error2 = 1
			    message = '**** Error: The delta N range '+	$
                                      'for impact parameter cross-sec'+	$
                                      'tions is too large ****'
		        endif
		    endif
		endelse
		if error eq 1 then widget_control, state.delid, 	$
		/input_focus
	    endif
            if error eq 0 then begin
                widget_control, state.intid, get_value=intval
                intchk = strcompress(intval(0), /remove_all)
                if num_chk(intval(0), sign=1, /integer) ne 0 and 	$
		intchk ne '' then begin
                    error = 1
                    error2 = 1
                    message = '**** Error: The integral order of impac'+$
                              't parameter cross-sections is invalid ****'
                endif else begin
                    if intchk eq '' then begin
                        error = 1
                        error2 = 1
                        message = '**** Error: You must enter '+ 	$
                                  'the integral order of'+		$
                                  ' input parameter cross-sections'+	$
                                  ' ****'
                    endif
                    if error eq 0 then begin
                        if fix(intval(0)) gt 3 then begin
                            error = 1
                            error2 = 1
                            message = '**** Error: The integral order'+	$
                                      ' of impact parameter cross-sec'+	$
                                      'tions is too large ****'
			endif
                    endif
                endelse
		if error eq 1 then widget_control, state.intid, /input_focus
            endif
	
		;*****************************************************
		;**** If there was an error bring the appropriate ****
		;**** widget to the fore                          ****
		;*****************************************************

	    if error eq 1 and error2 eq 1 then begin
		widget_control, state.paramid, set_value=1
		widget_control, state.parambase2, map=0
		widget_control, state.parambase3, map=1
		widget_control, state.parambase4, map=0
	    endif

		;*************************************
		;**** Check parameter switch (II) ****
		;*************************************

	    error3 = 0
            if error eq 0 then begin
                widget_control, state.dnrid, get_value=dnrval
                dnrchk = strcompress(dnrval(0), /remove_all)
                if num_chk(dnrval(0), sign=1, /integer) ne 0 and 	$
		dnrchk ne '' then begin
                    error = 1
                    error3 = 1
                    message = '**** Error: The delta N range for ion '+	$
                              'impact cross-sections is invalid ****'
                endif else begin
                    if dnrchk eq '' then begin
                        error = 1
                        error3 = 1
                        message = '**** Error: You must enter '+ 	$
                                  'the delta N range for'+		$
                                  ' ion impact cross-sections ****'
                    endif
                    if error eq 0 then begin
                        if fix(dnrval(0)) gt 999 then begin
                            error = 1
                            error3 = 1
                            message = '**** Error: The delta N range '+	$
                                      'for ion impact cross-sec'+	$
                                      'tions is too large ****'
			endif
                    endif
                endelse
		if error eq 1 then widget_control, state.dnrid, /input_focus
            endif

		;*****************************************************
		;**** If there was an error bring the appropriate ****
		;**** widget to the fore                          ****
		;*****************************************************

	    if error eq 1 and error3 eq 1 then begin
		widget_control, state.paramid, set_value=2
		widget_control, state.parambase2, map=0
		widget_control, state.parambase3, map=0
		widget_control, state.parambase4, map=1
	    endif

		;************************************************
		;**** Check values related to the impurities ****
		;************************************************

	    error4 = 0
	    if error eq 0 then begin
	        widget_control, state.scanid, get_value=scanval
	        if scanval eq 0 then begin		;single impurity
		    widget_control, state.symid, get_value=symval
		    symchk = strcompress(symval(0), /remove_all)
		    if symchk eq '' then begin
		        error4 = 1
		        error = 1
		        message = '**** Error: You must enter an element'+$
                                  ' symbol for the impurity ****'
		        widget_control, state.symid, /input_focus
		    endif else begin
		        i4eiz0, symchk, atnumber
		        if atnumber eq 0 then begin
			    error4 = 1
                            error = 1
                            message = '**** Error: The impurity element'+$
                                      ' symbol is invalid ****'
                            widget_control, state.symid, /input_focus
		        endif
		    endelse
	        endif else begin			;multiple impurities
		    widget_control, state.imptabid, get_value=imptable
		    impvals = where(strcompress(imptable.value(0,*),	$
  		    /remove_all) ne '')
		    if impvals(0) eq -1 then begin
			error = 1
			error4 = 1
			message = '**** Error: No impurity information'+$
			          ' entered ****'
		    endif else begin
			nimp = n_elements(impvals)
			valid_symbol = 0
			for i=0, (nimp-1) do begin
			    symbol =					$
			    strcompress(imptable.value(0,i),/remove_all)
			    i4eiz0, symbol, atnumber
			    if atnumber eq 0 then 			$
                            valid_symbol=valid_symbol+1
			endfor
			if valid_symbol ne 0 then begin
			    error = 1
			    error4 = 1
			    message = '**** Error: At least one of the'+$
                                      ' impurity element symbols is'+	$
                                      ' invalid ****'
			endif else begin
			    totalfrac = 0.0
			    for i=0,(nimp-1) do begin
			        totalfrac = totalfrac + 		$
                                float(imptable.value(2,i))
			    endfor
			    if totalfrac le 0.0 then begin
				error = 1
				error4 = 1
				message = '**** Error: The total stop'+	$
				          'ping ion fraction is zero '+	$
					  '****'
			    endif else if totalfrac gt 1.0 then begin
                    	        infbutts = [' Cancel ','  OK  ']
                                infomess = ' The stopping ion fractio'+	$
                                           'ns must be renormalised '
                                action = popup(message=infomess,	$
                                               buttons=infbutts,	$
                                               font=state.font_large)
                                if action eq ' Cancel ' then begin
                                    error = 1
                                    message = ' '
                                endif else begin
				    for i=0,(nimp-1) do begin
                                        new_val = 			$
                                        float(imptable.value(2,i))/	$
                                        totalfrac
				        imptable.value(2,i) = 		$
				        string(new_val, format='(f6.4)')
				    endfor
				    if nimp lt state.mximp then begin
				        imptable.value(2,nimp:*) = ''
				    endif
				    widget_control, state.imptabid, 	$
                                    set_value=imptable
			        endelse
			    endif
		        endelse
		    endelse
	        endelse
	    endif

		;*****************************************************
		;**** If there was an error bring the appropriate ****
		;**** widget to the fore                          ****
		;*****************************************************

	    if error eq 1 and error4 eq 1 then begin
		widget_control, state.base1id, set_value=0
		widget_control, state.impbase, map=1
		widget_control, state.impbase2, map=(1-scanval)
		widget_control, state.impbase3, map=scanval
		widget_control, state.nshtop, map=0
	    endif

		;*********************************************
		;**** Check representative N-shell values ****
		;*********************************************

	    error5 = 0
	    if error eq 0 then begin
		widget_control, state.nminid, get_value=nminval
		nminchk = strcompress(nminval(0), /remove_all)
		if nminchk eq '' then begin
		    error = 1
		    error5 = 1
		    message = '**** Error: You must enter a value for'+	$
                              ' the minimum N-shell ****'
		endif else begin
		    if num_chk(nminchk, sign=1, /integer) ne 0 then begin
			error = 1
			error5 = 1
			message = '**** Error: The minimum N-shell'+	$
               			  ' value is invalid ****'
		    endif else begin
			if fix(nminchk) lt 1 then begin
			    error = 1
			    error5 = 1	
			    message = '**** Error: The minimum N-shell'+$
                                      ' value must be >=1 ****'
			endif
		    endelse
		endelse
		if error eq 1 then widget_control, state.nminid, /input_focus
	    endif
	    if error eq 0 then begin
		widget_control, state.nmaxid, get_value=nmaxval
		nmaxchk = strcompress(nmaxval(0), /remove_all)
		if nmaxchk eq '' then begin
		    error = 1
		    error5 = 1
		    message = '**** Error: You must enter a value for'+	$
                              ' the maximum N-shell ****'
		endif else begin
		    if num_chk(nmaxchk, sign=1, /integer) ne 0 then begin
			error = 1
			error5 = 1
			message = '**** Error: The maximum N-shell'+	$
               			  ' value is invalid ****'
		    endif else begin
			if fix(nmaxchk) le fix(nminchk) then begin
			    error = 1
			    error5 = 1	
			    message = '**** Error: The maximum N-shell'+$
                                      ' value must be greater than the'+$
          			      ' minimum ****'
			endif else if (fix(nmaxchk) - fix(nminchk)) 	$
                        lt 2 then begin
			    error = 1
			    error5 = 1	
			    message = '**** Error: A minimum of 3 shel'+$
				      'ls is required ****'
			endif
		    endelse
		endelse
		if error eq 1 then widget_control, state.nmaxid, /input_focus
	    endif
	    if error eq 0 then begin
	        widget_control, state.nshtabid, get_value=nshtable
	        nshvals = where(strcompress(nshtable.value(0,*),	$
  	        /remove_all) ne '')
	        if nshvals(0) eq -1 then begin
		    error = 1
		    error5 = 1
		    message = '**** Error: No representative N-shell '+	$
                              ' information entered ****'
	        endif else begin
		    nnsh = n_elements(nshvals)
		    if nnsh lt 3 then begin
			error = 1
			error5 = 1
			message = '**** Error: At least 3 representat'+	$
                                  'ive N-shells are required ****'
		    endif else begin
			if fix(nshtable.value(0,0)) ne nminchk then begin
                   	    infbutts = [' Cancel ','  Reset  ']
                            infomess = ' The first representative N-sh'+$
                                       'ell must equal the minimum N-s'+$
                                       'hell. Reset? '
                            action = popup(message=infomess,		$
                                           buttons=infbutts,		$
                                           font=state.font_large)
                            if action eq ' Cancel ' then begin
                                error = 1
				error5 = 1
                                message = ' '
                            endif else begin
				nshtable.value(0,0) = string(nminchk,	$
				format='(i3)')
				widget_control, state.nshtabid, 	$
                                set_value=nshtable
				if fix(nshtable.value(0,0)) ge 		$
				fix(nshtable.value(0,1)) then begin
				    error = 1
				    error5 = 1
				    message = '**** Error: The represe'+$
				              'ntative N-shell table i'+$
					      's not in ascending orde'+$
					      'r ****'
			    	endif
			    endelse
			endif
			if error eq 0 then begin
			    if max(fix(nshtable.value(0,*))) gt		$
			    nmaxchk then begin
				error = 1
				error5 = 1
				message = '**** Error: Highest represe'+$
                                          'ntative N-shell is greater '+$
				  	  ' than the minimum N-shell v'+$
                                          'alue ****'
			    endif
			endif
		    endelse
		endelse
	    endif

                ;*****************************************************
                ;**** If there was an error bring the appropriate ****
                ;**** widget to the fore                          ****
                ;*****************************************************

            if error eq 1 and error5 eq 1 then begin
                widget_control, state.base1id, set_value=1
		widget_control, state.impbase, map=0
		widget_control, state.nshtop, map=1
	    endif

		;**********************************************
		;**** Check electron/proton density values ****
		;**********************************************

	    error6 = 0
	    if error eq 0 then begin
		widget_control, state.dentabid, get_value=dentable
		denvals = where(strcompress(dentable.value(0,*),	$
                /remove_all) ne '')
	        if denvals(0) eq -1 then begin
		    error = 1
		    error6 = 1
		    message = '**** Error: No electron/proton density'+	$
                              ' values entered ****'
	        endif else begin
		    widget_control, state.denrefid, get_value=denrefval
		    if strcompress(denrefval(0), /remove_all) eq '' 	$
 		    then begin
			error = 1
			error6 = 1
			message = '**** Error: You must enter an index'+$
				  ' for the density reference value '+	$
				  '****'
			widget_control, state.denrefid, /input_focus
		    endif else begin
			if num_chk(denrefval(0), sign=1, /integer) ne 	$
			0 then begin
			    error = 1
			    error6 = 1
			    message = '**** Error: The density referen'+$
				      'ce index is invalid ****'
			    widget_control, state.denrefid, /input_focus
			endif else begin
			    if fix(denrefval(0)) gt (max(denvals)+1) or	$
			    fix(denrefval(0)) lt (min(denvals)+1) then	$
			    begin
				error = 1
				error6 = 1
				message = '**** Error: The density ref'+$
					  'erence index is out of rang'+$
					  'e ****'
				widget_control, state.denrefid, 	$
				/input_focus
			    endif
			endelse
		    endelse
		endelse
	    endif

                ;*****************************************************
                ;**** If there was an error bring the appropriate ****
                ;**** widget to the fore                          ****
                ;*****************************************************

            if error eq 1 and error6 eq 1 then begin
		widget_control, state.base2id, set_value=0
		widget_control, state.denbase, map=1
		widget_control, state.tembase, map=0
		widget_control, state.benbase, map=0
	    endif

		;**************************************************
		;**** Check Electron/proton temperature values ****
		;**************************************************

	    error7 = 0
	    if error eq 0 then begin
		widget_control, state.temtabid, get_value=temtable
		temvals = where(strcompress(temtable.value(0,*),	$
                /remove_all) ne '')
	        if temvals(0) eq -1 then begin
		    error = 1
		    error7 = 1
		    message = '**** Error: No electron/proton tempera'+	$
                              'ture values entered ****'
	        endif else begin
		    widget_control, state.temrefid, get_value=temrefval
		    if strcompress(temrefval(0), /remove_all) eq '' 	$
 		    then begin
			error = 1
			error7 = 1
			message = '**** Error: You must enter an index'+$
				  ' for the temperature reference va'+	$
				  'lue ****'
			widget_control, state.temrefid, /input_focus
		    endif else begin
			if num_chk(temrefval(0), sign=1, /integer) ne 	$
			0 then begin
			    error = 1
			    error7 = 1
			    message = '**** Error: The temperature ref'+$
				      'erence index is invalid ****'
			    widget_control, state.temrefid, /input_focus
			endif else begin
			    if fix(temrefval(0)) gt (max(temvals)+1) or	$
			    fix(temrefval(0)) lt (min(temvals)+1) then	$
			    begin
				error = 1
				error7 = 1
				message = '**** Error: The temperature'+$
					  ' reference index is out of '+$
					  'range ****'
				widget_control, state.temrefid, 	$
				/input_focus
			    endif
			endelse
		    endelse
		endelse
	    endif

                ;*****************************************************
                ;**** If there was an error bring the appropriate ****
                ;**** widget to the fore                          ****
                ;*****************************************************

            if error eq 1 and error7 eq 1 then begin
		widget_control, state.base2id, set_value=1
		widget_control, state.denbase, map=0
		widget_control, state.tembase, map=1
		widget_control, state.benbase, map=0
	    endif

		;**********************************
		;**** Check Beam energy values ****
		;**********************************

	    error8 = 0
	    if error eq 0 then begin
		widget_control, state.bentabid, get_value=bentable
		benvals = where(strcompress(bentable.value(0,*),	$
                /remove_all) ne '')
	        if benvals(0) eq -1 then begin
		    error = 1
		    error8 = 1
		    message = '**** Error: No beam energy values ente'+	$
                              'red ****'
	        endif else begin
		    widget_control, state.benrefid, get_value=benrefval
		    if strcompress(benrefval(0), /remove_all) eq '' 	$
 		    then begin
			error = 1
			error8 = 1
			message = '**** Error: You must enter an index'+$
				  ' for the beam energy reference va'+	$
				  'lue ****'
			widget_control, state.benrefid, /input_focus
		    endif else begin
			if num_chk(benrefval(0), sign=1, /integer) ne 	$
			0 then begin
			    error = 1
			    error8 = 1
			    message = '**** Error: The beam energy ref'+$
				      'erence index is invalid ****'
			    widget_control, state.benrefid, /input_focus
			endif else begin
			    if fix(benrefval(0)) gt (max(benvals)+1) or	$
			    fix(benrefval(0)) lt (min(benvals)+1) then	$
			    begin
				error = 1
				error8 = 1
				message = '**** Error: The beam energy'+$
					  ' reference index is out of '+$
					  'range ****'
				widget_control, state.benrefid, 	$
				/input_focus
			    endif
			endelse
		    endelse
		endelse
	    endif
	    if error eq 0 then begin
		widget_control, state.beamdenid, get_value=beamdenval
		if strcompress(beamdenval(0), /remove_all) eq ''	$
		then begin
		    error = 1
		    error8 = 1
		    message = '**** Error: You must enter a value for '+$
			      'the beam density ****'
		    widget_control, state.beamdenid, /input_focus
		endif else begin
		    if num_chk(beamdenval(0), sign=1) ne 0 then begin
                        error = 1
                        error8 = 1
                        message = '**** Error: The beam density is inv'+$
			          'alid ****'
			widget_control, state.beamdenid, /input_focus
		    endif
		endelse
	    endif

                ;*****************************************************
                ;**** If there was an error bring the appropriate ****
                ;**** widget to the fore                          ****
                ;*****************************************************

            if error eq 1 and error8 eq 1 then begin
                widget_control, state.base2id, set_value=2
                widget_control, state.denbase, map=0
                widget_control, state.tembase, map=0
                widget_control, state.benbase, map=1
            endif

		;************************************
		;**** return value or flag error ****
		;************************************

            if error eq 0 then begin
                new_event = {ID:parent, TOP:event.top, 			$
 		             HANDLER:0L, ACTION:'Done'}
            endif else begin
                widget_control, state.messid, set_value=message
                new_event = 0L
            endelse
	end

        ELSE: new_event = 0L

    ENDCASE

                ;*********************************************
                ;*** make "state" available to other files ***
                ;*********************************************

    widget_control, first_child, set_uvalue=state, /no_copy

    RETURN, new_event

END

;-----------------------------------------------------------------------------
  
FUNCTION cw_adas310_proc, topparent, bitfile, inval, PROCVAL=procval, 	$
			  mxnrp, mxden, mxtmp, mxeng, mximp,		$
                          FONT_LARGE=font_large, FONT_SMALL=font_small,	$
                          EDIT_FONTS=edit_fonts, NUM_FORM=num_form,	$
			  UVALUE=uvalue

    COMMON cw_proc310_blk, jcorcom, corcom, jmaxcom, epsilcom, fijcom, 	$
			   wijcom, jdefcom, defectcom, wbrepcom, iz0com,$
			   izeffcom, izimpacom, amimpacom, frimpacom, 	$
			   nrepcom, densacom, denpacom, teacom, tpacom,	$
			   bmengacom, nimpcom

		;***********************************
                ;**** Set defaults for keywords ****
		;***********************************

    if not (keyword_set(uvalue)) then uvalue = 0
    if not (keyword_set(font_large)) then font_large = ''
    if not (keyword_set(font_small)) then font_small = ''
    if not (keyword_set(edit_fonts)) then                               $
    edit_fonts = {font_norm:'',font_input:''}
    if not (keyword_set(num_form)) then num_form = '(E10.3)'

		;*******************************************
		;**** Set up the common block variables ****
		;*******************************************

    iz0com = inval.iz0
    izeffcom = procval.izeff
    izimpacom = procval.izimpa
    amimpacom = procval.amimpa
    frimpacom = procval.frimpa
    nrepcom = procval.nrep
    wbrepcom = procval.wbrep
    densacom = procval.densa
    denpacom = procval.denpa
    teacom = procval.tea
    tpacom = procval.tpa
    bmengacom = procval.bmenga
    jcorcom = procval.jcor
    corcom = procval.cor
    jmaxcom = procval.jmax
    epsilcom = procval.epsil
    fijcom = procval.fij
    wijcom = procval.wij
    jdefcom = procval.jdef
    defectcom = procval.defect
    nimpcom = procval.nimp

                ;********************************************************
                ;**** Create the 310 Processing options/input window ****
                ;********************************************************

		;***********************************
                ;**** Create titled base widget ****
		;***********************************

    parent = widget_base(topparent, UVALUE = uvalue,                    $
                         title = 'ADAS310 PROCESSING OPTIONS',          $
                         EVENT_FUNC = "proc310_event",                  $
                         FUNC_GET_VALUE = "proc310_get_val",            $
                         /COLUMN)

                ;******************************************************
                ;**** Create a dummy widget just to hold value of *****
                ;**** "state" variable so as not to get confused  *****
                ;**** with any other values. Adopt IDL practice   *****
                ;**** of using first child widget                 *****
                ;******************************************************

    first_child = widget_base(parent)
    topbase = widget_base(first_child, /column)

                ;***********************************************
		;**** A base for the parameter and switches ****
                ;***********************************************

    topparambase = widget_base(topbase, /column, /frame)
    pswitchbase = widget_base(topparambase, /row)
    switchl = widget_label(pswitchbase, font=font_large,			$
    value="Select which parameters to display  : ")
    selections = ['General','Switches (I)','Switches(II)']
    paramid = cw_bgroup(pswitchbase, selections, font=font_small,	$
                        set_value=procval.itoggle3, /exclusive, /row)
    parambase = widget_base(topparambase)

		;*******************************
		;**** Add parameter typeins ****
		;*******************************
 
    parambase2 = widget_base(parambase, /column, /frame)
    paramhead2 = widget_label(parambase2, font=font_large,		$
    value="Please enter the following parameters:-")
    radbase = widget_base(parambase2, /row)
    genbase = widget_base(parambase2, /row)
    mulbase = widget_base(parambase2, /row)
    mul2base = widget_base(parambase2, /row)
    ionbase = widget_base(parambase2, /row)
    isobase = widget_base(parambase2, /row)
    radlab = widget_label(radbase, font=font_small,			$
    value="Radiation field temperature (eV)         : ")
    tsval = " " + strtrim(string(procval.ts, format='(E11.3)'), 2)
    radid = widget_text(radbase, font=font_small, /editable,		$
    value=tsval, xsize=10)
    radlab2 = widget_label(radbase, font=font_small,			$
    value=" [Blank for default]   ")
    genlab = widget_label(genbase, font=font_small,                     $
    value="General radiation field dilution         : ")
    wval = " " + strtrim(string(procval.w, format='(E11.3)'), 2)
    genid = widget_text(genbase, font=font_small, /editable,		$
    value=wval, xsize=10)
    genlab2 = widget_label(genbase, font=font_small,                    $
    value=" [Blank for default]   ")
    mullab = widget_label(mulbase, font=font_small,                     $
    value="Multiplier for ionisation cross-sections : ")
    cionval = " " + strtrim(string(procval.cion, format='(E11.3)'), 2)
    mulid = widget_text(mulbase, font=font_small, /editable,            $
    value=cionval, xsize=10)
    mullab2 = widget_label(mulbase, font=font_small,                    $
    value=" [Blank for default]   ")
    mul2lab = widget_label(mul2base, font=font_small,          		$
    value="Multiplier for Regemorter cross-sections : ")
    cpyval = " " + strtrim(string(procval.cpy, format='(E11.3)'), 2)
    mul2id = widget_text(mul2base, font=font_small, /editable,          $
    value=cpyval, xsize=10)
    mul2lab2 = widget_label(mul2base, font=font_small,         		$
    value=" [Blank for default]   ")
    ionlab = widget_label(ionbase, font=font_small,                   	$
    value="Ionising radiation field dilution        : ")
    w1val = " " + strtrim(string(procval.w1, format='(E11.3)'), 2)
    ionid = widget_text(ionbase, font=font_small, /editable,            $
    value=w1val, xsize=10)
    ionlab2 = widget_label(ionbase, font=font_small,                   	$
    value=" [Blank for default]   ")
    isolab = widget_label(isobase, font=font_small, 			$
    value="Beam species isotope mass                : ")
    if procval.bsim eq 0.0 then begin
	isoval = " " 
    endif else begin
	isoval = " " + strtrim(string(procval.bsim, format='(f5.1)'), 2)
    endelse
    isoid = widget_text(isobase, font=font_small, /editable,          	$
    value=isoval, xsize=10)
    isolab2 = widget_label(isobase, font=font_small,                   	$
    value=" [Blank for default]   ")

		;***************************************************
		;**** Map or unmap base according to the toggle ****
		;***************************************************

    if procval.itoggle3 ne 0 then begin
    	widget_control, parambase2, map=0
    endif

		;***********************************
		;**** Add parameter switches(I) ****
		;***********************************

    noyes = [' NO  ',' YES ']
    parambase3 = widget_base(parambase, /column, /frame)
    paramhead3 = widget_label(parambase3, font=font_large,		$
    value="Please enter the following parameter switches:-")
    switbuf = widget_label(parambase3, value=" ", font=font_large)
    delbase = widget_base(parambase3, /row)
    intbase = widget_base(parambase3, /row)
    prxbase = widget_base(parambase3, /row)
    accbase = widget_base(parambase3, /row)
    switbuf = widget_label(parambase3, value=" ", font=font_large)
    switbuf = widget_label(parambase3, value=" ", font=font_large)
    dellab = widget_label(delbase, font=font_small,                     $
    value="Delta N range for impact parameter cross-sections :")
    if procval.nip lt 0 or procval.nip gt 4 then begin
	delval = " "
    endif else begin
    	delval = " " + strtrim(string(procval.nip, format='(i)'),2)
    endelse
    delid = widget_text(delbase, font=font_small, /editable,		$
    xsize=4, value=delval)
    dellab2 = widget_label(delbase, font=font_small,                    $
    value=" [Max. value of 4]")
    intlab = widget_label(intbase, font=font_small,                     $
    value="Integral order of impact parameter cross-sections :")
    if procval.intd lt 0 or procval.intd gt 3 then begin
	intval = " "
    endif else begin
    	intval = " " + strtrim(string(procval.intd, format='(i)'),2)
    endelse
    intid = widget_text(intbase, font=font_small, /editable,		$
    xsize=4, value=intval)
    intlab2 = widget_label(intbase, font=font_small,                    $
    value=" [Max. value of 3]")
    prxlab = widget_label(prxbase, font=font_small,                     $
    value="Use Percival-Richards cross-sections              :")
    prxid = cw_bselector(prxbase, noyes, set_value=procval.iprs)
    info1 = widget_label(prxbase, font=font_small,			$
    value="[NO defaults to van Regemorter X-sections]")
    acclab = widget_label(accbase, font=font_small,                     $
    value="Access special low-level cross-section data       :")
    accid = cw_bselector(accbase, noyes, set_value=procval.ilow)

		;***************************************************
		;**** Map or unmap base according to the toggle ****
		;***************************************************

    if procval.itoggle3 ne 1 then begin
    	widget_control, parambase3, map=0
    endif

		;************************************
		;**** Add parameter switches(II) ****
		;************************************

    parambase4 = widget_base(parambase, /column, /frame)
    paramhead4 = widget_label(parambase4, font=font_large,		$
    value="Please enter the following parameter switches:-")
    switbuf = widget_label(parambase4, value=" ", font=font_large)
    actbase = widget_base(parambase4, /row)
    dnrbase = widget_base(parambase4, /row)
    lodbase = widget_base(parambase4, /row)
    beabase = widget_base(parambase4, /row)
    switbuf = widget_label(parambase4, value=" ", font=font_large)
    switbuf = widget_label(parambase4, value=" ", font=font_large)
    actlab = widget_label(actbase, font=font_small,                     $
    value="Activate ion impact cross-sections                :")
    actid = cw_bselector(actbase, noyes, set_value=procval.ionip)
    dnrlab = widget_label(dnrbase, font=font_small,                     $
    value="Delta N range for ion impact cross-sections       :")
    if procval.nionip lt 0 or procval.nionip gt 999 then begin
	dnrval = " "
    endif else begin
    	dnrval = " " + strtrim(string(procval.nionip, format='(i3)'),2)
    endelse
    dnrid = widget_text(dnrbase, font=font_small, /editable,		$
    xsize=4, value=dnrval)
    lodlab = widget_label(lodbase, font=font_small,                     $
    value="Use Lodge ion impact cross-sections               :")
    lodid = cw_bselector(lodbase, noyes, set_value=procval.ilprs)
    info2 = widget_label(lodbase, font=font_small,			$
    value="[NO defaults to Vainshtein X-sections]")
    bealab = widget_label(beabase, font=font_small,                     $
    value="Use beam energy in forming ion cross-sections     :")
    beaid = cw_bselector(beabase, noyes, set_value=procval.ivdisp)

		;***************************************************
		;**** Map or unmap base according to the toggle ****
		;***************************************************

    if procval.itoggle3 ne 2 then begin
    	widget_control, parambase4, map=0
    endif

		;*****************************************
		;**** Another base for all the tables ****
		;*****************************************

    tablebase = widget_base(topbase, /row)

		;**************************************
		;**** Base for impurities/N-shells ****
		;**************************************

    tablebase1 = widget_base(tablebase, /column, /frame)
    basehead = widget_label(tablebase1, font=font_large,		$
    value="Select table for display:-")
    selections = ['Impurity information                         ',	$
                  'Representative N-shells                      ']
    base1id = cw_bgroup(tablebase1, selections, font=font_small,	$
                        set_value=procval.itoggle1, /exclusive)

		;*************************************
		;**** Another base to aid clarity ****
		;*************************************

    tabbase1 = widget_base(tablebase1, /frame)

		;*********************************************
		;**** Base for the Impurities information ****
		;*********************************************

    if procval.itoggle1 eq 0 then begin
	impmap = 1
    endif else begin
	impmap = 0
    endelse
    impbase = widget_base(tabbase1, /column, map=impmap)

		;*******************************************
		;**** Single/multiple impurities switch ****
		;*******************************************

    impswitbase = widget_base(impbase, /row)
    scanlabel = widget_label(impswitbase, font=font_small, 		$
    value='Select mode of operation: ')
    selections = ['  Single impurity  ','Multiple impurities']
    scanid = cw_bselector(impswitbase, selections,          		$
                          set_value=procval.noscan)

		;***********************************************
		;**** Set up the single inpurity input, but ****
		;**** only map if Single impurity selected  ****
		;***********************************************

    impbase1 = widget_base(impbase)
    impbase2 = widget_base(impbase1, /row, map=(1-procval.noscan))
    imphead = widget_label(impbase2, font=font_small,			$
                           value="Enter element symbol of impurity: ")
    if procval.izeff gt 0 then begin
	i4z0ie, procval.izeff, symval
    endif else begin
	symval = " "
    endelse
    symid = widget_text(impbase2, font=font_small, value=" " + symval, 	$
                        /editable, xsize=4)

		;**************************************************
		;**** Set up the table anyway, but only map if ****
		;**** Multiple impurities selected             ****
		;**************************************************
    
    tabimp = strarr(3,mximp)
    impbase3 = widget_base(impbase1, /column, map=procval.noscan)
    if (procval.nimp ne 0) then begin
	for i=0, (procval.nimp-1) do begin
	    i4z0ie, procval.izimpa(i), outsymbol
	    tabimp(0,i) = '  ' + strtrim(outsymbol, 2)
	    tabimp(1,i) = ' ' + string(procval.amimpa(i), format='(f4.1)')
	    tabimp(2,i) = '  ' + string(procval.frimpa(i), format='(f6.4)')
	endfor
    endif
    imptitle = 'Multiple impurities (total fraction must be <= 1.0)'
    imptabid = cw_adas_table(impbase3, tabimp, coledit=[1,1,1],		$
               colhead=[['Symbol','Atomic','Fraction'],[' ',		$
               'Mass no.',' ']], title=imptitle, font_large=font_large,	$
	       /rowskip, font_small=font_small, limits=[0,2,2],		$
	       fltint=['(a)','(f4.1)','(f6.4)'], /scroll, ytexsize=5)

		;******************************************
		;**** Base for representative N-shells ****
		;******************************************

    if procval.itoggle1 eq 0 then begin
	nshmap = 0
    endif else begin
	nshmap = 1
    endelse
    nshtop = widget_base(tabbase1, /row, map=nshmap)
    nshbase = widget_base(nshtop, /column)
    nshhead = widget_label(nshbase, font=font_small, 			$
                           value="Enter limits on N-shells: ")
    nshbase2 = widget_base(nshbase, /row)
    nshlab1 = widget_label(nshbase2, font=font_small,			$
			   value='Minimum N-shell: ')
    if procval.nmin gt 0 then begin
   	nminval = " " + strtrim(string(procval.nmin),2)
    endif else begin
	nminval = " "
    endelse
    nminid = widget_text(nshbase2, /editable, xsize=5, value=nminval,	$
                         font=font_small)
    nshbase4 = widget_base(nshbase, /row)
    nshlab2 = widget_label(nshbase4, font=font_small,			$
			   value='Maximum N-shell: ')
    if procval.nmax gt 0 then begin
   	nmaxval = " " + strtrim(string(procval.nmax),2)
    endif else begin
	nmaxval = " "
    endelse
    nmaxid = widget_text(nshbase4, /editable, xsize=5, value=nmaxval,	$
                         font=font_small)
    nshfooter = widget_label(nshbase, font=font_small, 			$
		value='Note:                    ')
    nshfooter2 = widget_label(nshbase, font=font_small, 		$
		value='The first representative ')
    nshfooter3 = widget_label(nshbase, font=font_small, 		$
		value='N-shell is set equal to  ')
    nshfooter4 = widget_label(nshbase, font=font_small,			$
		value='the minimum N-shell      ')

		;***********************
		;**** N-shell table ****
		;***********************

    tabnsh = strarr(1,mxnrp)
    if (procval.imax gt 0) then begin
	for i=0, (procval.imax-1) do begin
	    tabnsh(0,i) = '  ' + strtrim(string(procval.nrep(i)), 2)
	endfor
    endif
    nshtitle = 'Representative N-shells'
    nshbase3 = widget_base(nshtop, /column)
    nshtabid = cw_adas_table(nshbase3, tabnsh, coledit=[1],		$
               colhead=['N Shell'], title=nshtitle, order=[1],		$
	       font_large=font_large, /rowskip, font_small=font_small, 	$
               limits=[2], fltint=['(i3)'], /scroll, ytexsize=4)
    nshclearid = widget_button(nshbase3, font=font_large,		$
                               value='Clear Table')

		;**************************************
		;**** Base for temp/energy/density ****
		;**************************************

    tablebase2 = widget_base(tablebase, /column, /frame)
    basehead = widget_label(tablebase2, font=font_large,		$
    value="Select table for display:-")
    selections = ['Electron/proton density scan',			$
                  'Electron/proton temperature scan',			$
                  'Beam energy scan']
    base2id = cw_bgroup(tablebase2, selections, font=font_small,	$
                        set_value=procval.itoggle2, /exclusive)

                ;*************************************
                ;**** Another base to aid clarity ****
                ;*************************************

    tabbase2 = widget_base(tablebase2, /frame)

		;************************************************
		;**** Base for the electron/proton densities ****
		;************************************************
    
    if procval.itoggle2 eq 0 then begin
	denmap = 1
    endif else begin
	denmap = 0
    endelse
    denbase = widget_base(tabbase2, /column, map=denmap)

		;***************************************
		;**** Set up the data for the table ****
		;***************************************

    tabden = strarr(2,mxden)
    if (procval.ndens ne 0) then begin
	tabden(0,0:procval.ndens-1) = '  ' + 				$
	strtrim(string(procval.densa(0:procval.ndens-1),		$
	format=num_form),2)
	tabden(1,0:procval.ndens-1) = '  ' + 				$
	strtrim(string(procval.denpa(0:procval.ndens-1),		$
	format=num_form),2)
    endif
    dentitle = 'Electron/proton densities (units: cm-3)'
    denhead = ['Electron density','Proton density']

		;***********************************************
		;**** Actual table, clear button and typein ****
		;***********************************************

    dentabid = cw_adas_table(denbase, tabden, coledit=[1,1], 		$
               limits=[2,2], title=dentitle, font_large=font_large,	$
               font_small=font_small, colhead=denhead, /scroll,		$
               ytexsize=3, num_form=num_form, order=[1,1], /rowskip)
    denclearid = widget_button(denbase, font=font_large,		$
                               value='Clear Table')
    denbase2 = widget_base(denbase, /row)
    denlabel = widget_label(denbase2, font=font_small,			$
	       value='Enter index of reference densities : ')
    if procval.idref ge 0 then begin
	if procval.idref le procval.ndens then begin
	    denrefval = " " + string(procval.idref, format='(i2)')
	endif else begin
	    denrefval = " "
	endelse
    endif else begin
	denrefval = " "
    endelse
    denrefid = widget_text(denbase2, font=font_small, /editable,	$
                           xsize=4, value=denrefval)

		;********************************************
		;**** Base for the electron/proton temps ****
		;********************************************
    
    if procval.itoggle2 eq 1 then begin
	temmap = 1
    endif else begin
	temmap = 0
    endelse
    tembase = widget_base(tabbase2, /column, map=temmap)

		;***************************************
		;**** Set up the data for the table ****
		;***************************************

    tabtem = strarr(1,mxtmp)
    if (procval.ntemp ne 0) then begin
	tabtem(0,0:procval.ntemp-1) = '  ' + 				$
	strtrim(string(procval.tea(0:procval.ntemp-1),			$
	format=num_form),2)
    endif
    temtitle = 'Electron/proton temperatures (units: eV)'
    temhead = ['Electron/proton temperature']

		;***********************************************
		;**** Actual table, clear button and typein ****
		;***********************************************

    temtabid = cw_adas_table(tembase, tabtem, coledit=[1], 		$
               limits=[2], title=temtitle, font_large=font_large,	$
               font_small=font_small, colhead=temhead, /scroll,		$
               ytexsize=3, num_form=num_form, order=[1])
    temclearid = widget_button(tembase, font=font_large,		$
                               value='Clear Table')
    tembase2 = widget_base(tembase, /row)
    temlabel = widget_label(tembase2, font=font_small,			$
	       value='Enter index of reference temps : ')
    if procval.itref ge 0 then begin
	if procval.itref le procval.ntemp then begin
	    temrefval = " " + string(procval.itref, format='(i2)')
	endif else begin
	    temrefval = " "
	endelse
    endif else begin
	temrefval = " "
    endelse
    temrefid = widget_text(tembase2, font=font_small, /editable,	$
                           xsize=4, value=temrefval)

		;************************************
		;**** Base for the beam energies ****
		;************************************
    
    if procval.itoggle2 eq 2 then begin
	benmap = 1
    endif else begin
	benmap = 0
    endelse
    benbase = widget_base(tabbase2, /column, map=benmap)

		;***************************************
		;**** Set up the data for the table ****
		;***************************************

    tabben = strarr(1,25)
    if (procval.nbeng ne 0) then begin
	tabben(0,0:procval.nbeng-1) =         			$
	strtrim(string(procval.bmenga(0:procval.nbeng-1), 		$
	format=num_form),2)
    endif
    bentitle = 'Beam energy scan values (units: eV/amu)'
    benhead = ['Beam energy']

		;************************************************
		;**** Actual table, clear button and typeins ****
		;***********************************************

    bentabid = cw_adas_table(benbase, tabben, coledit=[1], 		$
               limits=[2], title=bentitle, font_large=font_small,	$
               font_small=font_small, colhead=benhead, /scroll,		$
               ytexsize=2, num_form=num_form, order=[1])
    benclearid = widget_button(benbase, font=font_small,		$
                               value='Clear Table')
    benbase2 = widget_base(benbase, /row)
    benlabel = widget_label(benbase2, font=font_small,			$
	       value='Enter reference energy index : ')
    if procval.ibref ge 0 then begin
	if procval.ibref le procval.nbeng then begin
	    benrefval = " " + string(procval.ibref, format='(i2)')
	endif else begin
	    benrefval = " "
	endelse
    endif else begin
	benrefval = " "
    endelse
    benrefid = widget_text(benbase2, font=font_small, /editable,	$
                           xsize=4, value=benrefval)
    benbase3 = widget_base(benbase, /row)
    benlabel = widget_label(benbase3, font=font_small,			$
	       value='Enter beam density (cm-3) : ')
    if procval.densh gt 0.0 then begin
	beamdenrefval = " " + strtrim(string(procval.densh,format='(E11.1)'),2)
    endif else begin
	beamdenrefval = " "
    endelse
    beamdenid = widget_text(benbase3, font=font_small, /editable,	$
                            xsize=8, value=beamdenrefval)

		;***********************
                ;**** Error message ****
		;***********************

    messid = widget_label(parent, font=font_large, value='Edit the '+	$
                          'processing options data and press Done '+	$
                          'to proceed')
                           
		;******************************
                ;**** Add the exit buttons ****
		;******************************

    base = widget_base(parent, /row)
    menufile = bitfile + '/menu.bmp'
    read_X11_bitmap, menufile, bitmap1
    outid = widget_button(base, value=bitmap1)          ;menu button
    cancelid = widget_button(base, value='Cancel', font=font_large)
    doneid = widget_button(base, value='Done', font=font_large)

		;*************************************************
                ;**** Create a state structure for the pop-up ****
                ;****                window.                  ****
		;*************************************************

    new_state = {       messid          :       messid,                 $
                        outid           :       outid,                  $
                        cancelid        :       cancelid,               $
                        doneid          :       doneid,                 $
			radid		:	radid,			$
			genid		:	genid,			$
			mulid		:	mulid,			$
			mul2id		:	mul2id,			$
			ionid		:	ionid,			$
			isoid		:	isoid,			$
			delid		:	delid,			$
			intid		:	intid,			$
			prxid		:	prxid,			$
			accid		:	accid,			$
			actid		:	actid,			$
			dnrid		:	dnrid,			$
			lodid		:	lodid,			$
			beaid		:	beaid,			$
			base1id		:	base1id,		$
			base2id		:	base2id,		$
			paramid		:	paramid,		$
			scanid		:	scanid,			$
			symid		:	symid,			$
			nminid		:	nminid,			$
			nmaxid		:	nmaxid,			$
			parambase2	:	parambase2,		$
			parambase3	:	parambase3,		$
			parambase4	:	parambase4,		$
			impbase		:	impbase,		$
			impbase2	:	impbase2,		$
			impbase3	:	impbase3,		$
			imptabid	:	imptabid,		$
			nshtop		:	nshtop,			$
			nshtabid	:	nshtabid,		$
			nshclearid	:	nshclearid,		$
			dentabid	:	dentabid,		$
			denclearid	:	denclearid,		$
			denbase		:	denbase,		$
			denrefid	:	denrefid,		$
			temtabid	:	temtabid,		$
			temclearid	:	temclearid,		$
			tembase		:	tembase,		$
			temrefid	:	temrefid,		$
			bentabid	:	bentabid,		$
			benclearid	:	benclearid,		$
			benbase		:	benbase,		$
			benrefid	:	benrefid,		$
			beamdenid	:	beamdenid,		$
			mxnrp		:	mxnrp,			$
			mxden		:	mxden,			$
			mxtmp		:	mxtmp,			$
			mxeng		:	mxeng,			$
			mximp		:	mximp,			$
                        font_large	:	font_large,             $
                        font_small	:	font_small,		$
			num_form        :       num_form                }


		;**************************************
                ;**** Save initial state structure ****
		;**************************************

    widget_control, first_child, set_uvalue=new_state, /no_copy

    RETURN, parent

END
