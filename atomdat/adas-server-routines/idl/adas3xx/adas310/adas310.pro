; Copyright (c) 1995, Strathclyde University.
; SCCS info: Module @(#)$Header: /home/adascvs/idl/adas3xx/adas310/adas310.pro,v 1.10 2004/07/06 10:34:44 whitefor Exp $ Date $Date: 2004/07/06 10:34:44 $
;+
; PROJECT:
;       ADAS IBM MVS to UNIX conversion
;
; NAME:
;       ADAS310
;
; PURPOSE:
;       The highest level routine for the ADAS 310 program.
;
; NOTE:
;	The comment blocks for adas 310 refer to two methods of using the
;	code. The batch method will not initially be implemented as the
;	interactive code runs reasonably quickly for even large 
;	calculations. It is possible that the batch capability
;	will be added at a later date. The comments will be left for
;	reference.
;
; EXPLANATION:
;       This routine is called from the main adas system routine, adas.pro,
;       to start the ADAS 310 application.  Associated with adas310.pro
;	is a FORTRAN executable. The IDL code provides the user
;       interface and output graphics whilst the FORTRAN code reads
;       in data files, performs numerical processing and creates the
;       output files. The IDL code communicates with the FORTRAN
;       executable via a bi-directional UNIX pipe.  The unit number
;       used by the IDL for writing to and reading from this pipe is
;       allocated when the FORTRAN executable is 'spawned' (see code
;       below).  Pipe communications in the FORTRAN process are to
;       stdin and stdout, i.e streams 5 and 6.
;
;	310 is different from all previous conversions in that no actual
;	pipe communications are performed before the processing stage and
;	indeed the FORTRAN executable is not actually spawned until this
;	point. Once the user has chosen an interactive or foreground run,
;	the FORTRAN is either kicked off and the pipe communications
;	performed en masse, or the relevan information is written to the
;	associated scratch file for possible future execution.
;
;       The FORTRAN code is an independent process under the UNIX system.
;       The IDL process can only exert control over the FORTRAN in the
;       data which it communicates via the pipe.  The communications
;       between the IDL and FORTRAN must be exactly matched to avoid
;       input conversion errors.  The correct ammounts of data must be
;       passed so that neither process 'hangs' waiting for communications
;       which will never occur.
;
;       The FORTRAN code performs some error checking which is
;       independent of IDL.  In cases of error the FORTRAN may write
;       error messages.  To prevent these error messages from conflicting
;       with the pipe communications all FORTRAN errors are written to
;       output stream 0, which is stderr for UNIX.  These error messages
;       will appear in the window from which the ADAS session/IDL session
;       is being run. In the case of the batch job, the error messages
;       will appear as standard cron job output which is usually
;       emailed to the user. If the job has completed successfully then
;       the user will get a message telling them so.
;
;       In the case of severe errors the FORTRAN code may terminate
;       itself prematurely.  In order to detect this, and prevent the
;       IDL program from 'hanging' or crashing, the IDL checks to see
;       if the FORTRAN executable is still an active process before
;       each group of pipe communications.  The process identifier
;       for the FORTRAN process, PID, is recorded when the process is
;       first 'spawned'.  The system is then checked for the presence
;       of the FORTRAN PID. This checking does not occur for the
;       batch cases.
;
; USE:
;       First the system settings must be established by calling
;       adas_sys_set.pro then adas310.pro is called to start the
;       ADAS 310 application;
;
;       adas_sys_set, adasrel, fortdir, userroot, centroot,             $
;                     devlist, devcode, font_large, font_small, edit_fonts
;       adas310,   adasrel, fortdir, userroot, centroot, devlist,       $
;                  devcode, font_large, font_small, edit_fonts
;
; INPUTS:
;       ADASREL -  A string indicating the ADAS system version,
;                  e.g ' ADAS RELEASE: ADAS93 V1.4'.  The first
;                  character should be a space.
;
;       FORTDIR -  A string holding the path to the directory where the
;                  FORTRAN executables are, e.g '/disk/adas/fortran/exec'
;
;       USERROOT - A string holding the path to the root directory of
;                  the user's adas data. e.g '/disk/bowen/adas'
;                  This root directory will be used by adas to construct
;                  other path names.  For example the users default data
;                  for adas205 should be in /disk/bowen/adas/adf04.  In
;                  particular the user's default interface settings will
;                  be stored in the directory USERROOT+'/defaults'.  An
;                  error will occur if the defaults directory does not
;                  exist.
;
;       CENTROOT - Like USERROOT, but this directory points to the
;                  central data area for the system.  User defaults are
;                  not stored on CENTROOT.
;
;       DEVLIST -  A string array of hardcopy device names, used for
;                  graphical output. e.g ['Post-Script','HP-PCL','HP-GL']
;                  This array must mirror DEVCODE.  DEVCODE holds the
;                  actual device names used in a SET_PLOT statement.
;
;       DEVCODE -  A string array of hardcopy device code names used in
;                  the SET_PLOT statement, e.g ['PS', 'PCL', 'HP']
;                  This array must mirror DEVLIST.
;
;       FONT_LARGE - The name of a larger font e.g 'courier_bold14'
;
;       FONT_SMALL - The name of a smaller font e.g 'courier_bold12'
;
;       EDIT_FONTS - A structure of two fonts used in the table editor
;                    adas_edtab.pro, {font_norm:'',font_input:''}.
;                    The two fonts are used to differentiate between
;                    editable and non-editable parts of the table. You
;                    may have to experiment to get two fonts which give
;                    the same default spacing in the edit widget.
;                    e.g {font_norm:'helvetica_bold14', 		$
;                         font_input:'helvetica_oblique14'}
;
; OPTIONAL INPUTS:
;       None
;
; OUTPUTS:
;       None - Note this routine should leave all inputs un-modified.
;
; OPTIONAL OUTPUTS:
;       None
;
; KEYWORD PARAMETERS:
;       None
;
; CALLS:
;       FIND_PROCESS    Checks to see if a given process is active.
;	CASPF0		Controls the input options screen.
;	CAISPF		Controls the processing options screen.
;	CASPF1		Controls the output options screen.
;	CASPF2		Controls the interactive calculations.
;       XXDATE          Get date and time from operating system.
;
; SIDE EFFECTS:
;       This routine spawns a FORTRAN executable.
;
; CATEGORY:
;       Adas system.
;
; WRITTEN:
;       Tim Hammond, Tessella Support Services plc, 18th December 1995
;
; MODIFIED:
;       1.1     Tim Hammond 
;		First version
;	1.2	Tim Hammond
;		Increased version number to 1.2
;	1.3	William Osborn
;		Updated version number to 1.3
;       1.4     William Osborn
;               Increased version number to 1.4
;       1.5     William Osborn
;               Increased version number to 1.5
;       1.6	Increased this size of the values mxeng,mxden
;		and mxtmp to 25. Increased version number to 1.6
;		Harvey Anderson
;	1.7	Richard Martin
;		Increased version no to 1.7
;	1.8	Richard Martin
;		Increased version no to 1.8
;	1.9	Richard Martin
;		Increased version no to 1.9
;	1.10	Richard Martin
;		Increased version no to 1.10
;
; VERSION:
;       1.1     08-02-96
;	1.2	13-05-96
;	1.3	11-07-96
;       1.4	14-10-96
;       1.5	25-11-96
;	1.6	23-01-97
;	1.7	23-06-98
;	1.8	15-10-99
;	1.9	10-11-00
;	1.10	14-03-2001
;
;-
;-----------------------------------------------------------------------------

PRO adas310, adasrel, fortdir, userroot, centroot, devlist, devcode, 	$
             font_large, font_small, edit_fonts

                ;************************
                ;**** Initialisation ****
                ;************************

    adasprog = ' PROGRAM: ADAS310 V1.10'
    lpend = 0
    gomenu = 0
    ipset = 0				;'Run number' counter for fg jobs
    ipbatset = 0			;'Run number' counter for bg jobs
    lbatch = 0				;Marker for whether a batch file
                                        ;has been created.
    dsbatch = ''                        ;batch-file name
    deffile = userroot + '/defaults/adas310_defaults.dat'
    bitfile = centroot + '/bitmaps'
    device = ''

                ;**********************************************
		;**** Parameters previously set in FORTRAN ****
                ;**********************************************

    mxcor = 20				;unknown
    mxeps = 10				;unknown
    mxdef = 10				;max. no. of quantum defects
    mxeng = 25				;max. no. of beam energies in scans
    mxden = 25				;max. no. of elec/proton densities
    mxtmp = 25				;max. no. of elec/proton temperatures
    mximp = 10				;max. no. of impurities
    mxnrp = 30				;max. no. of representative 
					;N-shell levels
    iutmp = 51				;unit no. for temp. scratch file
    iups1 = 7				;unit no. for 1st passing file
    iups2 = 10				;unit no. for 2nd passing file
    iups3 = 18				;unit no. for 3rd passing file
    iups4 = 62				;unit no. for 4th passing file
    
                ;******************************************
                ;**** Search for user default settings ****
                ;**** If not found create defaults     ****
                ;******************************************

    files = findfile(deffile)
    if files(0) eq deffile then begin
        restore, deffile
        inval.ecentroot = centroot + '/adf18/'
        inval.euserroot = userroot + '/adf18/'
        inval.ccentroot = centroot + '/adf01/'
        inval.cuserroot = userroot + '/adf01/'
    endif else begin
	inval   = {	EROOTPATH	:	userroot + '/adf18/',	$
			EFILE		:	'',			$
			ECENTROOT	:	centroot + '/adf18/',	$
			EUSERROOT	:	userroot + '/adf18/',	$
			CROOTPATH       :       userroot + '/adf01/',   $
                        CFILE           :       '',                     $
                        CCENTROOT       :       centroot + '/adf01/',   $
                        CUSERROOT       :       userroot + '/adf01/',	$
			Z		:	0.0,			$
			IZ0		:	1,			$
			DSNEX		:	'',			$
			DSNCX		:	'' 			}
	procval = {	NEW		:	-1			}
	outval  = {	TITLE		:	'',			$
			TEXOUT		:	0,			$
			TEXOUT1		:	0,			$
			TEXOUT2		:	0,			$
			TEXOUT3		:	0,			$
			TEXOUT4		:	0,			$
			TEXAPP		:	-1,			$
			TEXAPP1		:	-1,			$
			TEXAPP2		:	-1,			$
			TEXAPP3		:	-1,			$
			TEXAPP4		:	-1,			$
			TEXREP		:	0,			$
			TEXREP1		:	0,			$
			TEXREP2		:	0,			$
			TEXREP3		:	0,			$
			TEXREP4		:	0,			$
			TEXDSN		:	'',			$
			TEXDSN1		:	'',			$
			TEXDSN2		:	'',			$
			TEXDSN3		:	'',			$
			TEXDSN4		:	'',			$
			TEXDEF		:	'paper.txt',		$
			TEXDEF1		:	'data.pass1',		$
			TEXDEF2		:	'data.pass2',		$
			TEXDEF3		:	'data.pass3',		$
			TEXDEF4		:	'data.pass4',		$
			TEXMES		:	'',			$
			TEXMES1		:	'',			$
			TEXMES2		:	'',			$
			TEXMES3		:	'',			$
			TEXMES4		:	'' 			}
    endelse

                ;******************
                ;**** Get date ****
                ;******************

    date = xxdate()

LABEL100:

                ;************************************************
                ;**** Invoke user interface widget for       ****
                ;**** Data file selection                    ****
                ;************************************************

    caspf0, inval, rep, FONT_LARGE=font_large, FONT_SMALL=font_small

    ipset = 0
    ipbatset = 0

		;*********************************************
                ;**** If cancel selected then end program ****
		;*********************************************

    if rep eq 'YES' then goto, LABELEND

LABEL200:

                ;************************************************
                ;**** Invoke user interface widget for       ****
                ;**** processing options selection           ****
                ;************************************************

    caispf, inval, procval, bitfile, mximp, mxnrp, mxden, mxtmp, mxeng,	$
	    mxcor, mxeps, mxdef, lpend, gomenu, FONT_LARGE=font_large, 	$
	    FONT_SMALL=font_small, EDIT_FONTS=edit_fonts 

		;*************************************************
                ;**** If menu button clicked, end the program ****
		;*************************************************

    if gomenu eq 1 then begin
        goto, LABELEND
    endif

		;******************************************
                ;**** If cancel selected then goto 100 ****
		;******************************************

    if lpend eq 1 then goto, LABEL100

LABEL300:
		;******************************************
		;**** Invoke user interface widget for ****
		;**** output options screen 	       ****
		;******************************************

    caspf1, inval, procval, outval, bitfile, lpend, gomenu, 		$
	    FONT_LARGE=font_large, FONT_SMALL=font_small

		;*************************************************
                ;**** If menu button clicked erase any output ****
		;**** and then end the program                ****
		;*************************************************

    if gomenu eq 1 then begin
	outval.texmes = ''
	outval.texmes1 = ''
	outval.texmes2 = ''
	outval.texmes3 = ''
	outval.texmes4 = ''
        goto, LABELEND
    endif

		;***************************************
                ;**** If cancel selected then erase ****
		;**** output messages and goto 200  ****
		;***************************************

    if lpend eq 1 then begin
	outval.texmes = ''
	outval.texmes1 = ''
	outval.texmes2 = ''
	outval.texmes3 = ''
	outval.texmes4 = ''
	goto, LABEL200
    endif

		;***************************************************
		;**** If user has selected 'Run Now' then begin ****
		;**** an interactive calculation		****
		;***************************************************

    if lpend eq 0 then begin
	caspf2, inval, procval, outval, date, fortdir, centroot, mxcor,	$
	        mxeps, mxdef, font=font_large
    endif else begin

		;******************************************
		;**** User has selected 'Run in Batch' ****
		;**** NOT CURRENTLY IMPLEMENTED        ****
		;******************************************

    endelse

		;**************************************
		;**** Back for more output options ****
		;**************************************

    GOTO, LABEL300

LABELEND:

		;****************************
                ;**** Save user defaults ****
		;****************************

    save, inval, procval, outval, filename=deffile

END
