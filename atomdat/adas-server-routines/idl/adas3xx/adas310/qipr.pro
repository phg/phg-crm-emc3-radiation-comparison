;----------------------------------------------------------------------
;+
; PROJECT    :  ADAS
;
; NAME       :  qlpr
;
; PURPOSE    :  Calculates Percival-Richards ion impact ionisation
;               cross sections.
;
; ARGUMENTS  :  All output arguments will be defined appropriately.
;               Inputs will be converted to correct type, if possible,
;               internally without changing calling type.
;                   result = qipr(iz=iz, n=n, ....)
;
;
;               NAME      I/O    TYPE    DETAILS
; REQUIRED   :  iz         I     integer Target ion charge+1
;               n          I     integer Principal quantum  number of level
;               zimp       I     integer Projectile charge
;               energy     I     double  Energy of equivalent electron (eV)
;
;
; KEYWORDS      help             prints help to screen
;
;
; NOTES      :  Diirect translation of fortran code embedded in rqinew.for.
;               Units of result are cm^2
;
;
; AUTHOR     :  Martin O'Mullane
;
; DATE       :  20-10-2010
;
;
; MODIFIED:
;         1.1   Martin O'Mullane
;                - First version.
;
; VERSION:
;         1.1   20-10-2010
;-
;----------------------------------------------------------------------

FUNCTION qipr, iz        = iz,         $
               n         = n,          $
               zimp      = zimp,       $
               energy    = energy,     $
               help      = help


; If asked for help

if keyword_set(help) then begin
   doc_library, 'qipr'
   return, -1L
endif

iz_in   = double(iz)
n_in    = double(n)
zimp_in = double(zimp)
eng_in  = double(energy)

u    = (iz_in / n_in)^2
ecut = 1.15d0 * u


xener = 4.00346d-5 * eng_in
t1    = dblarr(n_elements(eng_in))
ind   = where(xener GT ecut, count, complement=ind_c, ncomplement=count_c)
if count_c GT 0 then t1[ind_c] = fltarr(count_c) + 1.0D-30
if count   GT 0 then t1[ind]   = 1.66667d0 - 0.25d0 / (xener[ind] / u - 1.0d0)

sigma = 8.7972d-17 * (zimp_in / iz_in)^2 * 4.0* n_in^2 * t1 / xener

return, sigma

END
