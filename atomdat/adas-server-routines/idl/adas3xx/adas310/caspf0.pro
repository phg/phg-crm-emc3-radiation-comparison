; Copyright (c) 1995, Strathclyde University           
; SCCS info: Module @(#)$Header: /home/adascvs/idl/adas3xx/adas310/caspf0.pro,v 1.1 2004/07/06 11:59:26 whitefor Exp $ Date $Date: 2004/07/06 11:59:26 $
;+
; PROJECT:
;       ADAS IBM MVS to UNIX conversion
;
; NAME:
;	CASPF0
;
; PURPOSE:
;	IDL user interface for 310 input options.
;
; EXPLANATION:
;	This routine invokes the part of the user interface
;	used to select the input dataset for ADAS310.
;
; USE:
;	The use of this routine is specific to ADAS310, see adas310.pro.
;
; INPUTS:
;	VALUE	- A structure which determines the initial settings of
;		  the dataset selection widget.  The initial value is
;		  set in adas310.pro.  VALUE is passed un-modified
;		  through to cw_adas310_in.pro, see that routine for a full
;		  description.
;
; OPTIONAL INPUTS:
;	None
;
; OUTPUTS:
;	VALUE	- On output the structure records the final settings of
;		  the dataset selection widget if the user pressed the
;		  'Done' button otherwise it is not changed from input.
;
;	REP	- String; Indicates whether the user pressed the 'Done'
;		  or 'Cancel' button on the interface.  The action is
;		  converted to the strings 'NO' and 'YES' respectively
;		  to match up with the existing FORTRAN code.  In the
;		  original IBM ISPF interface REP was the reply to the
;		  question 'DO YOU WANT TO END PROGRAM EXECUTION'.
;		  Note that for this routine, no communication with
;		  any FORTRAN routine actually takes place, but the
;		  style and format used has been kept for consistency.
;
; OPTIONAL OUTPUTS:
;	None
;
; KEYWORD PARAMETERS:
;       FONT_LARGE      - Supplies the large font to be used for the
;                         interface widgets.
;
;       FONT_SMALL      - Supplies the small font to be used for the
;                         interface widgets.
;
; CALLS:
;	ADAS310_IN	Pops-up the input selection widget.
;
; SIDE EFFECTS:
;	None.
;
; CATEGORY:
;	Adas system.
;
; WRITTEN:
;       Tim Hammond, Tessella Support Services plc, 18th December 1995
;
; MODIFIED:
;	1.1	Tim Hammond
;		First version.
;
; VERSION:
;	1.1	18-12-95
;
;-
;-----------------------------------------------------------------------------

PRO caspf0, value, rep, FONT_LARGE=font_large, FONT_SMALL=font_small


                ;**** Set defaults for keywords ****

    IF NOT (KEYWORD_SET(font_large)) THEN font_large = ''
    IF NOT (KEYWORD_SET(font_small)) THEN font_small = ''


		;**********************************
		;**** Pop-up input file widget ****
		;**********************************

    adas310_in, value, action, WINTITLE = 'ADAS 310 INPUT', 		$
     		FONT_LARGE=font_large, FONT_SMALL=font_small

		;******************************************
		;**** Act on the event from the widget ****
		;******************************************
		;**** There are only two possible events ****
		;**** 'Done' and 'Cancel'.               ****
		;********************************************

   if action eq 'Done' then begin
       rep = 'NO'
   endif else begin
       rep = 'YES'
   endelse

END
