; Copyright (c) 1996, Strathclyde University.
; SCCS info: Module @(#)$Header: /home/adascvs/idl/adas3xx/adas310/adas310_out.pro,v 1.2 2004/07/06 10:34:55 whitefor Exp $ Date $Date: 2004/07/06 10:34:55 $
;+
; PROJECT:
;       ADAS IBM MVS to UNIX conversion
;
; NAME:
;       ADAS310_OUT
;
; PURPOSE:
;       IDL ADAS user interface, output options.
;
; EXPLANATION:
;       This routine creates and manages a pop-up window which allows
;       the user to select options and input data to control ADAS310
;	calculations and file output.
;
; USE:
;       This routine is specific to adas310.
;
; INPUTS:
;	INVAL	- A structure containing the final settings of the
;                 input options widget.
;
;       PROCVAL - A structure containing the final settings of the
;                 processing options widget.
;
;       OUTVAL  - A structure which determines the initial settings of
;                 the output options widget.  The value is passed
;                 unmodified into cw_adas310_out.pro. See that routine
;		  for a full description.
;
;       BITFILE - String; the path to the directory containing bitmaps
;                 for the 'escape to series menu' button.
;
; OPTIONAL INPUTS:
;       None.
;
; OUTPUTS:
;       ACTION	- String;'Menu', 'Cancel', 'Run Now' or 'Run in Batch'
;		  for the button the user pressed to terminate the
;                 output options window.

;
; OPTIONAL OUTPUTS:
;       None.
;
; KEYWORD PARAMETERS:
;
;       FONT_LARGE- Supplies the large font to be used.
;
;       FONT_SMALL- Supplies the small font to be used.
;
; CALLS:
;       CW_ADAS310_OUT  Creates the output options widget.
;	DYNLABEL	Sets dynamic_resize keyword for label widgets.
;       XMANAGER
;       See side effects for widget management routine.
;
; SIDE EFFECTS:
;       This routine uses a common block OUT310_BLK to maintain its state.
;       ADAS310_OUT_EV  is included in this file and is called
;                       indirectly from XMANAGER during widget
;                       management.
;
; CATEGORY:
;       Widgets
;
; WRITTEN:
;       Tim Hammond, Tessella Support Services plc, 10th January 1996
;
; MODIFIED:
;       1.1     Tim Hammond
;               First release
;	1.2    	William Osborn
;		Added dynlabel procedure
;
; VERSION:
;       1.1     08-02-96
;	1.2	09-07-96
;
;-
;-----------------------------------------------------------------------------
PRO adas310_out_ev, event

    COMMON out310_blk, action, value

		;************************************************
                ;**** Find the event type and copy to common ****
		;************************************************

    action = event.action
    CASE action of

		;**************************
		;**** 'Run Now' button ****
		;**************************

	'Run Now' : begin

		;*************************************
		;**** Get the output widget value ****
		;*************************************

	    child = widget_info(event.id, /child)
            widget_control, child, get_value=value

            	;*****************************************
              	;**** Kill the widget to allow IDL to ****
              	;**** continue and initialise         ****
              	;**** FORTRAN only if there is work   ****
              	;**** for the FORTRAN to do.          ****
              	;*****************************************

	    if (value.texout eq 1) or (value.texout1 eq 1) or		$
	    (value.texout2 eq 1) or (value.texout3 eq 1) or		$
	    (value.texout4 eq 1) then begin
	        widget_control, event.top, /destroy
	    endif
	end
		;*************************
                ;**** 'Cancel' button ****
		;*************************

        'Cancel': widget_control, event.top, /destroy

		;***********************
                ;**** 'Menu' button ****
		;***********************

        'Menu': widget_control, event.top, /destroy

        ELSE: 		;do nothing

    endcase

END

;-----------------------------------------------------------------------------

PRO adas310_out, inval, procval, outval, action, bitfile, 		$
		 font_large=font_large, font_small=font_small

    COMMON out310_blk, act, value

		;******************************
                ;**** Copy value to common ****
		;******************************

    value = outval

		;***********************************
                ;**** Set defaults for keywords ****
		;***********************************

    if not (keyword_set(font_large)) then font_large = ''
    if not (keyword_set(font_small)) then font_small = ''

                ;*****************************
                ;**** Pop-up a new widget ****
                ;*****************************

    outid = widget_base(title='ADAS310 OUTPUT OPTIONS', xoffset=100, 	$
                        yoffset=1)
    cwid = cw_adas310_out(outid, inval, procval, bitfile,    		$
                          value=outval, font_large=font_large, 		$
		          font_small=font_small)

		;********************************
                ;**** Realize the new widget ****
		;********************************

    dynlabel, outid
    widget_control, outid, /realize

		;***************************
                ;**** make widget modal ****
		;***************************

    xmanager, 'adas310_out', outid, event_handler='adas310_out_ev', 	$
              /modal, /just_reg

                ;**** Return the output value from common ****

    action = act
    outval = value

END
