; Copyright (c) 1996, Strathclyde University.
; SCCS Info : Module @(#)$Header: /home/adascvs/idl/adas3xx/adas310/cw_adas310_out.pro,v 1.2 2004/07/06 12:40:35 whitefor Exp $ Date $Date: 2004/07/06 12:40:35 $
;+
; PROJECT:
;       ADAS IBM MVS to UNIX conversion
;
; NAME:
;       CW_ADAS310_OUT()
;
; PURPOSE:
;       Produces a widget for ADAS310 output options.
;
; EXPLANATION:
;       This function declares a compound widget consisting of:
;        - a typein text widget for a user-defined title for any calculations
;	 - 5 output file widgets (cw_adas_outfile) for text and
;	   passing file output. The widget requires that at least one 
;	   of these be selected for any output to be produced
;	 - buttons for 'Run Now', 'Run in Batch','Cancel' and 'Return to
;	   series menu' (this latter is represented by a bitmapped button).
;	This widget only generates events for the four buttons mentioned.
;	Note that the 'batch' capability is currently not implemented
;	see comments block in adas310.pro for more details.
;
; USE:
;       This routine is specific to adas310.
;
; INPUTS:
;       PARENT  - Long integer; ID of parent widget.
;
;	INVAL	- A structure containing the settings of the input
;                 options widget.
;
;       PROCVAL - A structure containing the settings of the processing
;                 options widget.
;
;       BITFILE - String; the path to the directory containing bitmaps
;                 for the 'escape to series menu' button.
;
; OPTIONAL INPUTS:
;       None.
;
;
; OUTPUTS:
;       The return value of this function is the ID of the declared widget.
;
; OPTIONAL OUTPUTS:
;       None.
;
; KEYWORD PARAMETERS:
;       OUTVAL	- A structure which determines the initial settings of
;                 the output options widget.  
;
;       UVALUE  - A user value for this widget.
;
;       FONT_LARGE- Supplies the large font to be used.
;
;       FONT_SMALL- Supplies the small font to be used.
;
; CALLS:
;       CW_ADAS_OUTFILE Output file name entry widget.
;       See side effects for other related widget management routines.
;
; SIDE EFFECTS:
;       Two other routines are included in this file
;       OUT310_GET_VAL()
;       OUT310_EVENT()
;
; CATEGORY:
;       Compound Widget
;
; WRITTEN:
;       Tim Hammond, Tessella Support Services plc, 10th January 1996
;
; MODIFIED:
;       1.1     Tim Hammond
;               First version
;	1.2	William Osborn
;		Removed xsize=n keyword from widget_label command.
;		The dynlabel procedure does not override these
;		assignments and they were taken as being values in pixels
;		on some machines.
;
; VERSION:
;       1.1     08-02-96
;	1.2	01-08-96
;
;-
;-----------------------------------------------------------------------------

FUNCTION out310_get_val, id

    COMMON outblock310, font, font_small

		;***********************************
                ;**** Return to caller on error ****
		;***********************************

    ON_ERROR, 2

		;****************************
                ;**** Retrieve the state ****
		;****************************

    parent = widget_info(id, /parent)
    widget_control, parent, get_uvalue=state, /no_copy

                ;***********************************
                ;**** Get run title from widget ****
                ;**** Then centre in in string  ****
                ;**** of 40 characters          ****
                ;***********************************

    widget_control, state.runid, get_value=title
    title = strcompress(title(0))
    title_len = strlen(title)
    if (title_len gt 40 ) then begin
        title = strmid(title, 0, 37)
        widget_control, state.messid, set_value="Title too long - truncated "
        widget_control, state.runid, set_value=title
        wait, 1
    endif
    pad = (40 - title_len)/2
    spaces = '                                         '
    title(0) = strmid(spaces,0,(pad-1)) + title + strmid(spaces,0,(pad-1))

		;*********************************
		;**** Get the output settings ****
		;*********************************

    widget_control, state.summaryid, get_value=summaryval
    widget_control, state.pass1id, get_value=pass1val
    widget_control, state.pass2id, get_value=pass2val
    widget_control, state.pass3id, get_value=pass3val
    widget_control, state.pass4id, get_value=pass4val

    os = {	TITLE		:	title(0),			$
		TEXOUT		:	summaryval.outbut,		$
		TEXOUT1		:	pass1val.outbut,		$
		TEXOUT2		:	pass2val.outbut,		$
		TEXOUT3		:	pass3val.outbut,		$
		TEXOUT4		:	pass4val.outbut,		$
		TEXAPP		:	summaryval.appbut,		$	
		TEXAPP1		:	pass1val.appbut,		$
		TEXAPP2		:	pass2val.appbut,		$
		TEXAPP3		:	pass3val.appbut,		$
		TEXAPP4		:	pass4val.appbut,		$
		TEXREP		:	summaryval.repbut,		$
		TEXREP1		:	pass1val.repbut,		$
		TEXREP2		:	pass2val.repbut,		$
		TEXREP3		:	pass3val.repbut,		$
		TEXREP4		:	pass4val.repbut,		$
		TEXDSN		:	summaryval.filename,		$	
		TEXDSN1		:	pass1val.filename,		$
		TEXDSN2		:	pass2val.filename,		$
		TEXDSN3		:	pass3val.filename,		$
		TEXDSN4		:	pass4val.filename,		$
		TEXDEF		:	summaryval.defname,		$
		TEXDEF1		:	pass1val.defname,		$
		TEXDEF2		:	pass2val.defname,		$
		TEXDEF3		:	pass3val.defname,		$
		TEXDEF4		:	pass4val.defname,		$
		TEXMES		:	summaryval.message,		$
		TEXMES1		:	pass1val.message,		$
		TEXMES2		:	pass2val.message,		$
		TEXMES3		:	pass3val.message,		$
		TEXMES4		:	pass4val.message		}

		;**************************
                ;**** Return the state ****
		;**************************

    widget_control, parent, set_uvalue=state, /no_copy

    RETURN, os

END

;-----------------------------------------------------------------------------

FUNCTION out310_event, event

    COMMON outblock310, font, font_small

                ;**** Base ID of compound widget ****

    parent=event.top

                ;**** Retrieve the state ****

    widget_control, parent, get_uvalue=state, /no_copy

                ;*********************************
                ;**** Clear previous messages ****
                ;*********************************

    widget_control, state.messid, set_value=' '

                ;************************
                ;**** Process events ****
                ;************************

    CASE event.id OF

                ;***********************
                ;**** Cancel button ****
                ;***********************

        state.cancelid: begin
            new_event = {	ID	:	parent, 		$
				TOP	:	event.top,    		$
                         	HANDLER	:	0L, 			$
				ACTION	:	'Cancel'		}
        end

                ;***********************
                ;**** Menu button ****
                ;***********************

        state.outid: begin
            new_event = {	ID	:	parent, 		$
				TOP	:	event.top, 		$
				HANDLER	:	0L,          		$
                         	ACTION	:	'Menu'			}
        end

                ;************************
                ;**** Run Now button ****
                ;************************

    	state.nowid: begin

                ;***************************************
                ;**** Check for errors in the input ****
                ;***************************************

            error = 0
	    widget_control, state.summaryid, get_value=summaryval
	    widget_control, state.pass1id, get_value=pass1val
	    widget_control, state.pass2id, get_value=pass2val
	    widget_control, state.pass3id, get_value=pass3val
	    widget_control, state.pass4id, get_value=pass4val
	    if (summaryval.outbut eq 1) and 				$
	    (strtrim(summaryval.message) ne '') then error=1
	    if (error eq 0) and (pass1val.outbut eq 1) and 		$
	    (strtrim(pass1val.message) ne '') then error=1
	    if (error eq 0) and (pass2val.outbut eq 1) and 		$
	    (strtrim(pass2val.message) ne '') then error=1
	    if (error eq 0) and (pass3val.outbut eq 1) and 		$
	    (strtrim(pass3val.message) ne '') then error=1
	    if (error eq 0) and (pass4val.outbut eq 1) and 		$
	    (strtrim(pass4val.message) ne '') then error=1

		;*******************************
		;**** Return error or event ****
		;*******************************

	    if error eq 1 then begin
                widget_control, state.messid, set_value=                  $
                '**** Error in output settings ****'
                new_event = 0L
            endif else begin
                new_event = {	ID	:	parent, 		$
				TOP	:	event.top, 		$
				HANDLER	:	0L,          		$
                         	ACTION	:	'Run Now'		}
	    endelse
	end

        ELSE: begin
            new_event = 0L
        end

    ENDCASE

                ;**** Return the state   ****

    widget_control, parent, set_uvalue=state, /no_copy

    RETURN, new_event

END

;-----------------------------------------------------------------------------

FUNCTION cw_adas310_out, parent, inval, procval, bitfile, 		$
	 		 VALUE=outval, FONT_LARGE=font_large,		$
			 FONT_SMALL=font_small

    COMMON outblock310, fontlargecom, fontsmallcom

		;***********************************
                ;**** Set defaults for keywords ****
		;***********************************
                          
    IF NOT (KEYWORD_SET(uvalue)) THEN uvalue = 0
    IF NOT (KEYWORD_SET(font_large)) THEN font_large = ''
    IF NOT (KEYWORD_SET(font_small)) THEN font_small = ''
    fontlargecom = font_large
    fontsmallcom = font_small

                ;**********************************************
                ;**** Create the 310 Output options widget ****
                ;**********************************************
                                    
    cwid = widget_base( parent, UVALUE = uvalue,                        $
                        EVENT_FUNC = "out310_event",                    $
                        FUNC_GET_VALUE = "out310_get_val",              $
                        /COLUMN)
                ;***********************
                ;**** add run title ****
                ;***********************

    base = widget_base(cwid, /row)
    rc = widget_label(base, value='Title for run: ', font=font_small)
    runid = widget_text(base, value=outval.title, xsize=38, 		$
			font=font_small, /editable)

		;*****************************
		;**** Text output widgets ****
		;*****************************

    outfval = 	{	OUTBUT		:	outval.texout,		$
			APPBUT		:	outval.texapp,		$
			REPBUT		:	outval.texrep,		$
			FILENAME	:	outval.texdsn,		$
			DEFNAME		:	outval.texdef,		$
			MESSAGE		:	outval.texmes		}
    base = widget_base(cwid, /row, /frame)
    summaryid = cw_adas_outfile(base, output='Run Summary Output  ',	$
				value=outfval, font=font_small)
    outfval1 = 	{	OUTBUT		:	outval.texout1,		$
			APPBUT		:	outval.texapp1,		$
			REPBUT		:	outval.texrep1,		$
			FILENAME	:	outval.texdsn1,		$
			DEFNAME		:	outval.texdef1,		$
			MESSAGE		:	outval.texmes1		}
    base = widget_base(cwid, /row, /frame)
    pass1id = cw_adas_outfile(base, output='First Passing File  ',	$
			      value=outfval1, font=font_small)
    outfval2 = 	{	OUTBUT 		:	outval.texout2,		$
			APPBUT 		:	outval.texapp2,		$
			REPBUT 		:	outval.texrep2,		$
			FILENAME 	:	outval.texdsn2,		$
			DEFNAME 	:	outval.texdef2,		$
			MESSAGE 	:	outval.texmes2		}
    base = widget_base(cwid, /row, /frame)
    pass2id = cw_adas_outfile(base, output='Second Passing File ',	$
			      value=outfval2, font=font_small)
    outfval3 = 	{	OUTBUT 		:	outval.texout3,		$
			APPBUT 		:	outval.texapp3,		$
			REPBUT 		:	outval.texrep3,		$
			FILENAME 	:	outval.texdsn3,		$
			DEFNAME 	:	outval.texdef3,		$
			MESSAGE 	:	outval.texmes3		}
    base = widget_base(cwid, /row, /frame)
    pass3id = cw_adas_outfile(base, output='Third Passing File  ',	$
			      value=outfval3, font=font_small)
    outfval4 = 	{	OUTBUT 		:	outval.texout4,		$
			APPBUT 		:	outval.texapp4,		$
			REPBUT 		:	outval.texrep4,		$
			FILENAME 	:	outval.texdsn4,		$
			DEFNAME 	:	outval.texdef4,		$
			MESSAGE 	:	outval.texmes4		}
    base = widget_base(cwid, /row, /frame)
    pass4id = cw_adas_outfile(base, output='Fourth Passing File ',	$
			      value=outfval4, font=font_small)

		;***********************
                ;**** Error message ****
		;***********************

    messid = widget_label(cwid, value=' ', font=font_large)

		;******************************
                ;**** Add the exit buttons ****
		;******************************

    base = widget_base(cwid, /row)
    menufile = bitfile + '/menu.bmp'
    read_X11_bitmap, menufile, bitmap1
    outid = widget_button(base, value=bitmap1)          ;menu button
    cancelid = widget_button(base, value='Cancel', font=font_large)
    nowid = widget_button(base, value='Run Now', font=font_large)

                ;*************************************************
                ;**** create a state structure for the pop-up ****
                ;**** window.                                 ****
                ;*************************************************

    new_state = {       MESSID		:	messid,			$
			RUNID		:	runid,			$
			SUMMARYID	:	summaryid,		$
			PASS1ID		:	pass1id,		$
			PASS2ID		:	pass2id,		$
			PASS3ID		:	pass3id,		$
			PASS4ID		:	pass4id,		$
			OUTID		:	outid,			$
			NOWID		:	nowid,			$
			CANCELID        :       cancelid		}

                ;**** Save initial state structure ****

    widget_control, parent, set_uvalue=new_state, /no_copy

    RETURN, cwid

END
