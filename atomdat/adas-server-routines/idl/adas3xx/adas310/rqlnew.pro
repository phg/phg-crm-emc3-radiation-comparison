;----------------------------------------------------------------------
;+
; PROJECT    :  ADAS
;
; NAME       :  rqlnew
;
; PURPOSE    :  Calculates Lodge-Percival-Richards ion impact excitation rate.
;
; ARGUMENTS  :  All output arguments will be defined appropriately.
;               Inputs will be converted to correct type, if possible,
;               internally without changing calling type.
;                   result = rqlnew(iz=iz, n=n, zimp=zimp, mass=mass, vdisp=vdisp, te=te)
;
;
;               NAME      I/O    TYPE    DETAILS
; REQUIRED   :  iz         I     integer Target ion charge+1
;               nlow       I     integer Principal quantum  number of
;                                        initial level
;               nup        I     integer Principal quantum  number of
;                                        final level
;               zimp       I     integer Projectile charge
;               mass       I     double  Projectile mass (proton units)
;               vdisp      I     double  Constant mean speed shift for the
;                                        collision (beam picture)
;               te         I     double  Ion temperature
;               dex_rate   O     double  De-excitation rate (cm3/s) with
;                                        both temperature and speed used
;                                        in detailed balance.
;
;
; KEYWORDS      kelvin           requested temperature in K (default eV)
;               help             prints help to screen
;
;
; NOTES      :  Calls the fortran code.
;               Units of result are cm^3/s
;
;
; AUTHOR     :  Martin O'Mullane
;
; DATE       :  20-10-2010
;
;
; MODIFIED:
;         1.1   Martin O'Mullane
;                - First version.
;
; VERSION:
;         1.1   20-10-2010
;-
;----------------------------------------------------------------------

FUNCTION rqlnew, iz       = iz,        $
                 nlow     = nlow,      $
                 nup      = nup,       $
                 zimp     = zimp,      $
                 mass     = mass,      $
                 vdisp    = vdisp,     $
                 te       = te,        $
                 dex_rate = dex_rate,  $
                 kelvin   = kelvin,    $
                 help     = help


; If asked for help

if keyword_set(help) then begin
   doc_library, 'rqlnew'
   return, -1L
endif

fortdir = getenv('ADASFORT')
fortdir = fortdir

if n_elements(iz)    EQ 0 OR   $
   n_elements(nlow)  EQ 0 OR   $
   n_elements(nup)   EQ 0 OR   $
   n_elements(zimp)  EQ 0 OR   $
   n_elements(mass)  EQ 0 OR   $
   n_elements(vdisp) EQ 0 OR   $
   n_elements(te)    EQ 0      $
   then message, 'essential parameters missing'

; inputs

n_te = n_elements(te)

; Kelvin for routine - assume i/p in eV unless /kelvin

if keyword_set(kelvin) then tp_in = double(te) else tp_in = double(te * 11605.0)

if nlow EQ nup then message, 'nlow cannot be same as nup'

if nlow LT nup then begin
   nl_in    = long(nlow)
   nu_in    = long(nup)
endif else begin
   nl_in    = long(nup)
   nu_in    = long(nlow)
endelse

z_in     = double(iz)
zimp_in  = double(zimp)
mass_in  = double(mass)
vdisp_in = double(vdisp)


; results

rate = dblarr(n_te)

dummy = CALL_EXTERNAL(fortdir+'/rqlnew_if.so','rqlnew_if', $
                      n_te, z_in, nl_in, nu_in, zimp_in, $
                      mass_in, vdisp_in, tp_in, rate)


if arg_present(dex_rate) then begin

   emp      = 1.67265d-27
   tev      = tp_in / 1.1605d+04
   trh      = tev / 13.6042
   enu      = double(nu_in*nu_in)
   enl      = double(nl_in*nl_in)
   eijrh    = z_in * z_in * (1.0D0/enl - 1.0D0/enu)
   edrh     = 0.5 * mass_in * emp * vdisp_in^2 *1.0d-04 / 2.179d-18
   dex_rate = enl / enu * rate * exp(eijrh / (trh+edrh))

endif

return, rate

END
