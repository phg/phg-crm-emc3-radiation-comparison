;----------------------------------------------------------------------
;+
; PROJECT    :  ADAS
;
; NAME       :  run_adas310
;
; PURPOSE    :  Runs ADAS310 beam stopping code as an IDL subroutine.
;
; ARGUMENTS  :  All output arguments will be defined appropriately.
;
;               NAME      I/O    TYPE   DETAILS
; INPUTS     :  adf18      I     str    expansion file name.
;               adf01      I     str    charge exchange file name.
;               iz0        I     int    Nuclear charge of beam species
;               iz1        I     int    Recombining ion charge (beam
;                                       charge + 1)
;               ts         I     real   Radiation field temperature (eV)
;               wdil       I     real   External radiation field dilution
;                                       factor
;               cion       I     real   Multiplier of ground level e-impact
;                                       ionisation rate
;               cpy        I     real   Multiplier of ground level e-impact
;                                       excitation rate
;               wdpi       I     real   External radiation field dilution
;                                       factor photo-ionisation
;               bsim       I     real   Beam species isotope mass
;               nip        I     int    Range of delta n for impact parameter
;                                       cross sections
;               intd       I     int    Order of maxwell quadrature for
;                                       cross sections
;               iprs       I     int    Beyond nip - use 0 van regemorter,
;                                                        1 Percival-Richards
;               ilow       I     int    =1 Access special low level data
;                                       =0 Do not
;               ionip      I     int    =1 Include ion impact collisions
;                                       =0 Do not
;               nionip     I     int    Delta n for ion impact excitation
;                                       cross sections
;               ilprs      I     int    =1 Lodge-Percival-Richards or
;                                       =0 Vainshtein default
;               ivdisp     I     int    =1 Use beam energy in calculation
;                                          of cross section
;                                       =0 Do not
;               izimp      I     int()  Impurity atomic number
;               amimp      I     real() Impurity mass
;               frimp      I     real() Impurity fraction - total LE 1.0
;               nmin       I     int    Minimum representative shell
;               nmax       I     int    Maximunm representative shell
;               nrep       I     int()  Representative shells (max 25)
;               te         I     real() Electron temperatures requested
;               tp         I     real() Ion temperatures requested
;               dens       I     real() Electron densities requested
;               denp       I     real() Ion densities requested
;               bmeng      I     real() Beam energies requested
;               tref       I     real   Reference electron temperatures
;               dref       I     real   Reference density (electron)
;               bref       I     real   Reference beam energy
;               bdens      I     real   Beam density
;
;               log        O     str    Output logfile name
;               adf21      O     str    Output adf21 stopping dataset name.
;               adf26      O     str    Output adf26 population dataset name
;               crmat      O     str    Output collisional-radiative matrix is
;                                              written to this file.
;               repmap     O     str    Output file with mapping between the
;                                       n-representations
;
; KEYWORDS      no_ion             -    Switch off ion impact contribution.
;               no_cx              -    Switch off CX contribution.
;               pbar               -    Display a progress bar when running.
;               help               -    If specified this comment section
;                                       is written to screen.
;
;
; OUTPUTS    :  An adf21 file is the output.
;
;
; NOTES      :  run_adas310 uses the spawn command. Therefore IDL v5.3
;               cannot be used. Any other version should work.
;
; AUTHOR     :  Martin O'Mullane
;
; DATE       :  08-04-2004
;
; MODIFIED
;       1.1     Martin O'Mullane
;                 - First version.
;       1.2     Martin O'Mullane
;                 - Allow adf26 files as output.
;                 - Add /help keyword.
;       1.3     Allan Whiteford
;                 - Fixed typo which stopped nmin being passed in.
;       1.4     Martin O'Mullane
;                 - Add no_ion and no_cx keywords.
;       1.5     Martin O'Mullane
;                 - Use adas_writefile for output file production.
;                 - Add crmat as an output file option.
;                 - Some parameter checks tested the wrong parameters.
;       1.6     Martin O'Mullane
;                 - Better documentation for iz1 and change to internal
;                   a310_iz1 variable.
;                 - Add repmap output file as an argument.
;
; DATE
;       1.1     08-04-2004
;       1.2     21-01-2005
;       1.3     18-01-2006
;       1.4     18-02-2010
;       1.5     15-10-2015
;       1.6     16-04-2019
;
;-
;----------------------------------------------------------------------


PRO run_adas310, iz0    = iz0,    $
                 iz1    = iz1,    $
                 adf18  = adf18,  $
                 adf01  = adf01,  $
                 ts     = ts,     $
                 wdil   = wdil,   $
                 cion   = cion,   $
                 cpy    = cpy,    $
                 wdpi   = wdpi,   $
                 bsim   = bsim,   $
                 nip    = nip,    $
                 intd   = intd,   $
                 iprs   = iprs,   $
                 ilow   = ilow,   $
                 ionip  = ionip,  $
                 nionip = nionip, $
                 ilprs  = ilprs,  $
                 ivdisp = ivdisp, $
                 izimp  = izimp,  $
                 amimp  = amimp,  $
                 frimp  = frimp,  $
                 nmin   = nmin,   $
                 nmax   = nmax,   $
                 nrep   = nrep,   $
                 te     = te,     $
                 tp     = tp,     $
                 dens   = dens,   $
                 denp   = denp,   $
                 bmeng  = bmeng,  $
                 tref   = tref,   $
                 dref   = dref,   $
                 bref   = bref,   $
                 bdens  = bdens,  $
                 log    = log,    $
                 adf21  = adf21,  $
                 adf26  = adf26,  $
                 crmat  = crmat,  $
                 repmap = repmap, $
                 pbar   = pbar,   $
                 no_ion = no_ion, $
                 no_cx  = no_cx,  $
                 help   = help


; If asked for help

if keyword_set(help) then begin
   doc_library, 'run_adas310'
   return
endif



; First check that we are not using IDL v5.3

if !version.release EQ '5.3' then begin
   message, 'This version of IDL (v5.3) will not work - ' + $
            'change to v5.0, v5.1, v5.2 or v5.4'
   return
endif


; Check that we get all inputs and that they are correct. Otherwise print
; a message and return to command line.

on_error, 2
close, /all

; Input screen

if n_elements(iz0) EQ 0 then begin
    a310_iz0 = 1
    print, 'Using a default hydrogen beam'
endif else a310_iz0 = long(iz0)

if n_elements(iz1) EQ 0 then begin
    a310_iz1 = 1.0D0
    print, 'Using a default hydrogen beam charge'
endif else begin
   if iz1 EQ 0 then message, 'Recombining charge cannot be zero'
   a310_iz1 = double(iz1)
endelse

if n_elements(adf18) EQ 0 then message, 'An expansion file is necessary'
a310_adf18 = adf18

if n_elements(adf01) EQ 0 then begin
    a310_adf01 = '/home/adas/adas/adf01/qcx#h0/qcx#h0_old#h1.dat'
    print, 'Using a default adf01 file : ' + a310_adf01
endif else a310_adf01 = adf01


; Processing screen
;
; Switches

if n_elements(ts) EQ 0 then begin
    a310_ts = 8617.0D0
    print, 'Using a default radiation field temperature : ' + string(a310_ts)
endif else a310_ts = double(ts)

if n_elements(wdil) EQ 0 then begin
    a310_wdil = 0.0D0
    print, 'Using a default radiation dilution factor : ' + string(a310_wdil)
endif else a310_wdil = double(wdil)

if n_elements(cion) EQ 0 then begin
    a310_cion = 1.0D0
    print, 'Using a  default multiplier of ground level ' + $
           'e-impact ionisation rate: ' + string(a310_cion)
endif else a310_cion = double(cion)

if n_elements(cpy) EQ 0 then begin
    a310_cpy = 1.0D0
    print, 'Using a  default multiplier of ground level ' + $
           'e-impact excitation rate: ' + string(a310_cpy)
endif else a310_cpy = double(cpy)

if n_elements(wdpi) EQ 0 then begin
    a310_wdpi = 0.0D0
    print, 'Using a default photo-ionisation radiation ' + $
           'field dilution factor : ' + string(a310_wdpi)
endif else a310_wdpi = double(wdpi)

if n_elements(bsim) EQ 0 then begin
    a310_bsim = 1.0D0
    print, 'Using a default beam species isotope mass : ' + string(a310_bsim)
endif else a310_bsim = double(bsim)

if n_elements(nip) EQ 0 then begin
    a310_nip = 2L
    print, 'Using a default delta n for impact parameter cross sections : ' + string(a310_nip)
endif else a310_nip = long(nip)

if n_elements(intd) EQ 0 then begin
    a310_intd = 3L
    print, 'Using a default order of maxwell quadrature : ' + string(a310_intd)
endif else a310_intd = long(intd)

if keyword_set(iprs) then a310_iprs = 1 else a310_iprs = 0
if keyword_set(ilow) then a310_ilow = 1 else a310_ilow = 0
if keyword_set(ionip) then a310_ionip = 1 else a310_ionip = 0

if n_elements(nionip) EQ 0 then begin
    a310_nionip = 2L
    print, 'Using a default delta n for ion impact excitation : ' + string(a310_nionip)
endif else a310_nionip = long(nionip)

if keyword_set(ilprs) then a310_ilprs = 1 else a310_ilprs = 0
if keyword_set(ivdisp) then a310_ivdisp = 1 else a310_ivdisp = 0


; Impurities

if n_elements(izimp) EQ 0 then message, 'Impurity atomic numbers are required'
a310_izimp = long(izimp)
if n_elements(amimp) EQ 0 then message, 'Impurity masses are required'
a310_amimp = double(amimp)
if n_elements(frimp) EQ 0 then message, 'Impurity fraction are required'
a310_frimp = double(frimp)

len_izimp = n_elements(a310_izimp)
len_amimp = n_elements(a310_amimp)
len_frimp = n_elements(a310_frimp)

if (len_amimp ne len_izimp) or (len_izimp ne len_frimp) then $
   print, 'Impurity Z/M/fraction size mismatch - smallest  used'

nimp = min([len_izimp,len_amimp,len_frimp])



; Representative n-shells

if n_elements(nmin) EQ 0 then begin
    a310_nmin = 1L
    print, 'Using a default minimum n : ' + string(a310_nmin)
endif else a310_nmin = long(nmin)

if n_elements(nmax) EQ 0 then begin
    a310_nmax = 1L
    print, 'Using a default maximum n : ' + string(a310_nmax)
endif else a310_nmax = long(nmax)

if n_elements(nrep) EQ 0 then message, 'A set of representative n-shells is required'
a310_nrep = long(nrep)
imax = n_elements(nrep)


; Plasma and beam parameters

if n_elements(te) EQ 0 then message, 'Electron temperatures are required'
a310_te = double(te)

if n_elements(tp) EQ 0 then begin
    a310_tp = a310_te
    print, 'Setting ion temperatures to equal electron temperatures'
endif else a310_tp = double(tp)

len_te = n_elements(a310_te)
len_tp = n_elements(a310_tp)

if (len_te ne len_tp) then print, 'Te/Tion size mismatch - smallest  used'
ntemp = min([len_te,len_tp])

if n_elements(dens) EQ 0 then message, 'Electron densmperatures are required'
a310_dens = double(dens)

if n_elements(denp) EQ 0 then begin
    a310_denp = a310_dens
    print, 'Setting ion densities to equal electron densities'
endif else a310_denp = double(denp)

len_dens = n_elements(a310_dens)
len_denp = n_elements(a310_denp)

if (len_dens ne len_denp) then print, 'dens/nion size mismatch - smallest  used'
ndens = min([len_dens,len_denp])


if n_elements(bmeng) EQ 0 then message, 'Beam energies are required'
a310_bmeng = double(bmeng)
nbmeng     = n_elements(bmeng)

if n_elements(tref) EQ 0 then begin
    a310_tref = a310_te[ntemp / 2]
    print, 'Using a generated reference temperature : ' + string(a310_tref)
endif else a310_tref = double(tref)
ind       = i4indf(a310_te, a310_tref)
a310_tref = a310_te[ind]

if n_elements(dref) EQ 0 then begin
    a310_dref = a310_dens[ntemp / 2]
    print, 'Using a generated reference density : ' + string(a310_dref)
endif else a310_dref = double(dref)
ind       = i4indf(a310_dens, a310_dref)
a310_dref  = a310_dens[ind]
a310_dpref = a310_denp[ind]

if n_elements(bref) EQ 0 then begin
    a310_bref = a310_bmeng[ntemp / 2]
    print, 'Using a generated reference beam energy : ' + string(a310_bref)
endif else a310_bref = double(bref)
ind       = i4indf(a310_bmeng, a310_bref)
a310_bref = a310_bmeng[ind]

if n_elements(bdens) EQ 0 then begin
    a310_bdens = 1.0D10
    print, 'Using a default beam density : ' + string(a310_bdens)
endif else a310_bdens = double(bdens)


; Specialized options - switch off ion or CX contributions

if keyword_set(no_ion) then lnoion = 1L else lnoion = 0L
if keyword_set(no_cx) then lnocx = 1L else lnocx = 0L


; Output - restrict to paper.txt, adf26 andf adf21 files

if n_elements(log) NE 0 then begin
    a310_log = log
    texout   = 1L
endif else texout = 0L

if n_elements(adf21) EQ 0 AND n_elements(adf26) EQ 0 then $
   message, 'An output file is required'

if n_elements(adf21) NE 0 then a310_adf21 = adf21 else a310_adf21 = 'NULL'
if n_elements(adf26) NE 0 then a310_adf26 = adf26 else a310_adf26 = 'NULL'
if n_elements(crmat) NE 0 then a310_crmat = crmat else a310_crmat = 'NULL'
if n_elements(repmap) NE 0 then a310_repmap = repmap else a310_repmap = 'NULL'




;------------------
; Now run the code
;------------------

fortdir = getenv('ADASFORT')
spawn, fortdir+'/adas310.out', unit=pipe, /noshell, PID=pid

; No running in batch

nobatch = 0
printf, pipe, nobatch

date = xxdate()
printf, pipe, date[0]

printf, pipe, a310_iz0
printf, pipe, a310_iz1
printf, pipe, a310_adf18
printf, pipe, a310_adf01

dslpath = getenv('ADASCENT')
printf, pipe, dslpath

intemp = a310_ts                         ; Convert temperature to K
xxtcon, 2, 1, 1, 1, intemp, outtemp
printf, pipe, outtemp
printf, pipe, a310_wdil
printf, pipe, a310_cion
printf, pipe, a310_cpy
printf, pipe, a310_wdpi
printf, pipe, a310_bsim
printf, pipe, a310_nip
printf, pipe, a310_intd
printf, pipe, a310_iprs
printf, pipe, a310_ilow
printf, pipe, a310_ionip
printf, pipe, a310_nionip
printf, pipe, a310_ilprs
printf, pipe, a310_ivdisp

printf, pipe, 1L
printf, pipe, nimp
for i = 0, nimp-1 do begin
    printf, pipe, a310_izimp[i]
    printf, pipe, a310_amimp[i]
    printf, pipe, a310_frimp[i]
endfor

printf, pipe, a310_nmin
printf, pipe, a310_nmax
printf, pipe, imax
for i = 0, imax-1 do begin
    printf, pipe, a310_nrep[i]
    printf, pipe, 0.0D0
endfor


printf, pipe, a310_dref
printf, pipe, a310_dpref
xxtcon, 2, 1, 1, 1, a310_tref, reftemp_kelvin
printf, pipe, reftemp_kelvin
printf, pipe, ndens
for i = 0, ndens-1 do begin
    printf, pipe, a310_dens[i]
    printf, pipe, a310_denp[i]
endfor

printf, pipe, ntemp
xxtcon, 2, 1, 1, ntemp, a310_te[*], etemps_kelvin
xxtcon, 2, 1, 1, ntemp, a310_tp[*], ptemps_kelvin
for i = 0, ntemp-1 do begin
    printf, pipe, etemps_kelvin[i]
    printf, pipe, ptemps_kelvin[i]
endfor

printf, pipe, a310_bref
printf, pipe, nbmeng
for i = 0, nbmeng-1 do printf, pipe, a310_bmeng[i]
printf, pipe, a310_bdens


; Set values not yet accessible to the user

mxcor     = 20
mxeps     = 10
mxdef     = 10
cor       = dblarr(mxcor)
epsil     = dblarr(mxeps)
fij       = dblarr(mxeps)
wij       = dblarr(mxeps)
defect    = dblarr(mxdef)
jcor      = 1
cor[0]    = 0.0
jmax      = 1
epsil[0]  = 0.75
fij[0]    = 0.0
wij[0]    = 0.0
jdef      = 1
defect[0] = 0.0

printf, pipe, jcor
for i = 0, mxcor-1 do printf, pipe, cor[i]
printf, pipe, jmax
for i = 0, mxeps-1 do begin
    printf, pipe, epsil[i]
    printf, pipe, fij[i]
    printf, pipe, wij[i]
endfor
printf, pipe, jdef
for i = 0, mxdef-1 do printf, pipe, defect[i]

printf, pipe, lnoion
printf, pipe, lnocx


printf, pipe, 'Generated by run_adas310'
printf, pipe, texout
printf, pipe, 1L
printf, pipe, 1L
printf, pipe, 1L
printf, pipe, 1L

; select a random extension for output files

rand = strmid(strtrim(string(randomu(seed)), 2), 2, 4)

if texout EQ 1 then printf, pipe, a310_log else printf, pipe, 'temp1.0' + rand
printf, pipe, 'temp1.1' + rand
printf, pipe, 'temp1.2' + rand
printf, pipe, 'temp1.3' + rand
printf, pipe, 'temp1.4' + rand

idum = 0
readf, pipe, idum
if idum EQ 1 then message, 'A problem has occurred with file opening'

readf, pipe, idum
nloops = idum
inewstart = 0
for k = 1, nloops do begin
    readf, pipe, idum
    i1 = idum
    readf, pipe, idum
    i2 = idum
    itotal = i1 * i2 * 4
    info = 'run_adas310 (loop ' + string(k, format='(i2)') + ' of ' + $
                     string(nloops, format='(i2)') + ')  '
    if keyword_set(pbar) then begin
       adas_progressbar, pipe     = pipe,     $
                         n_call   = itotal,   $
                         adasprog = info
    endif else begin
       print, info
       for i=1, itotal do readf, pipe, idum
    endelse
endfor

; Final signal

idum = 0L
readf, pipe, idum


; Add comments to the bottom of the output files

date = xxdate()
name = xxuser()

com = ['C--------------------------------------------------------------------', $
       'C', $
       'C  Code      : run_adas310', $
       'C  Producer  : ' + name[0],  $
       'C  Date      : ' + date[0],  $
       'C', $
       'C--------------------------------------------------------------------']


if a310_adf21 NE 'NULL' then begin

   adas_readfile, file='temp1.4' + rand, all=all, nlines=nlines
   adas_writefile, file=a310_adf21, all=all, comments=com

endif

if a310_adf26 NE 'NULL' then begin

   adas_readfile, file='temp1.1' + rand, all=all, nlines=nlines
   adas_writefile, file=a310_adf26, all=all, comments=com

endif


if a310_crmat NE 'NULL' then begin

   adas_readfile, file='temp1.2' + rand, all=all, nlines=nlines
   adas_writefile, file=a310_crmat, all=all, comments=com

endif

if a310_repmap NE 'NULL' then begin

   adas_readfile, file='temp1.3' + rand, all=all, nlines=nlines
   adas_writefile, file=a310_repmap, all=all, comments=com

endif


; Remove unwanted files

file_delete, 'temp1.1' + rand, $
             'temp1.2' + rand, $
             'temp1.3' + rand, $
             'temp1.4' + rand, /allow_nonexistent


END
