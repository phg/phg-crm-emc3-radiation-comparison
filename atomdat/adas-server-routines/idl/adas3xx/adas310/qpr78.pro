;----------------------------------------------------------------------
;+
; PROJECT    :  ADAS
;
; NAME       :  qpr78
;
; PURPOSE    :  Calculates electron collision cROSS-SECTIONS FOR
;               transitions between principal qUANTUM SHELLS IN
;               Hydrogen and hydrogenic ions.                  
; 
;               Percival and Richards, MNRAS(1978)183,329.                           
; 
;               Valid for incident electron energies in range
;                            (z1/n1)**2<e1<137**2  
;               and for n1,n2>4.                                                    
;               Anomalies develop in original specification of Percival Richards for
;               s=n2-n1 large or if n1 is <5.  Hence a modified prescription is used
;               in these cases consistent with Banks et al (1973) ASTR. LETT 14,161
;                                                                   
;
; ARGUMENTS  :  All output arguments will be defined appropriately.
;               Inputs will be converted to correct type, if possible,
;               internally without changing calling type.
;
;                qpr78
;
;               NAME    I/O    TYPE       DETAILS
; REQUIRED   :   Z1      I      double    Target ion charge +1                   
;                N1      I      int       Initial principal quantum number       
;                N2      I      int       Final principal quantum number         
;                ENERGY  I      double()  Energy of incident electron in rydbergs
;                PHI     I      double    (IH/EIJ)F where
;                                             EIJ=TRANSITION ENERGY, F=ABS. OSCILL. STRENGTH
;                QPR78   O      double()  Cross-section in PI*A0**2 units
;
; NOTES      :  Calls fortran code.
;
;
; AUTHOR     :  Martin O'Mullane
;
; DATE       :  11-04-2011
;
;
; MODIFIED:
;       1.1     Martin O'Mullane
;              	- First version.
;
;
; VERSION:
;       1.1    11-04-2011
;
;-
;----------------------------------------------------------------------

FUNCTION qpr78, iz1       = iz1,        $
                nlow      = nlow,       $ 
                nup       = nup,        $ 
                phi       = phi,        $ 
                energy    = energy,     $ 
                help      = help          


fortdir = getenv('ADASFORT')


; If asked for help

if keyword_set(help) then begin
   doc_library, 'qp78r'
   return, -1L
endif


if n_elements(iz1)   EQ 0 OR   $
   n_elements(nlow)  EQ 0 OR   $
   n_elements(nup)   EQ 0 OR   $
   n_elements(phi)   EQ 0 OR   $
   n_elements(energy)EQ 0      $
   then message, 'essential parameters missing'

; inputs

n_eng = n_elements(energy)

if nlow EQ nup then message, 'nlow cannot be same as nup'
n1_in    = long(nlow)
n2_in    = long(nup)
z1_in    = double(iz1)
e1_in    = double(energy)
phi_in   = double(phi)



; results

sigma = dblarr(n_eng)

dummy = CALL_EXTERNAL(fortdir+'/qpr78_if.so','qpr78_if',  $
                      n_eng, z1_in, n1_in, n2_in, e1_in, phi_in, sigma)

return, sigma

END
