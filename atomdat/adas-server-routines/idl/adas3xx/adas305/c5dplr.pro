;----------------------------------------------------------------------
;+
; PROJECT    :  ADAS
;
; NAME       :  c5dplr
;
; PURPOSE    :  Doppler broaden Stark components.
;
; ARGUMENTS  :  All output arguments will be defined appropriately.
;               Inputs will be converted to correct type, if possible,
;               internally without changing calling type.
;
;                c5dplr
;
;               NAME      I/O    TYPE    DETAILS
; REQUIRED   :
;               amss       I     double  Atomic mass of hydrogen in plasma
;
;               ndpix      I     long    Maximum number of pixels in range 
;               npix       I     long    Actual number of pixels in range
;               wvmin      I     double  Minimum wavelength (A)
;               wvmax      I     double  Maximum wavelength (A)
;               ndcomp     I     long    Maximum number of components
;               ncomp      I     long    Actual number of components
;               wvcomp()   I     double  Wavelength of component
;               emcomp()   I     double  Emissivity of component
;               tev        I     double  Specific plasma electron temperature (eV)
;               amss       I     double  Atomic mass of hydrogen in plasma
;               doppler()  O     double  Doppler broadened feature
;
; NOTES      :  Calls fortran code.
;
;
; AUTHOR     :  Martin O'Mullane
;
; DATE       :  21/02/05
;
;
; MODIFIED:
;       1.1     Martin O'Mullane
;              	- First version.
;
;
; VERSION:
;       1.1    21/02/05
;
;-
;----------------------------------------------------------------------

PRO c5dplr, ndpix, npix, wvmin, wvmax, ndcomp,   $
            ncomp, wvcomp, emcomp, tev, amss,    $
            doppler


fortdir = getenv('ADASFORT')

ndpix   = long(ndpix)
npix    = long(npix)
wvmin   = double(wvmin)  
wvmax   = double(wvmax)  
ndcomp  = long(ndcomp)
ncomp   = long(ncomp)
wvcomp  = double(wvcomp)  
emcomp  = double(emcomp)  
tev     = double(tev)  
amss    = double(amss)  
doppler = double(doppler)  

dummy = CALL_EXTERNAL(fortdir+'/c5dplr_if.so','c5dplr_if', $
                      ndpix, npix, wvmin, wvmax, ndcomp,   $
                      ncomp, wvcomp, emcomp, tev, amss,    $
                      doppler)

END
