;----------------------------------------------------------------------
;+
; PROJECT    :  ADAS
;
; NAME       :  adas305_get_stark
;
; PURPOSE    :  Calculates the Hydrogen Stark feature either
;               as components or (optionally) as a Doppler 
;               broadened feature.
;
; ARGUMENTS  :  All output arguments will be defined appropriately.
;
;               NAME      I/O    TYPE    DETAILS
; REQUIRED   :  beam       I     str     Structure containing the beam details:
;                                         mass    : H beam mass 
;                                         energy  : in eV/amu
;                                         te      : in eV
;                                         density : in cm-3
;                                         dc_x    : direction cosine for velocity x-component
;                                         dc_y    : y
;                                         dc_y    : z
;               plasma     I     str     Structure containing the plasma details:
;                                         mass    : H mass in plasma
;                                         te      : in eV
;                                         density : in cm-3
;                                         Zeff    : effective charge
;               bfield     I     str     Structure containing the magnetic 
;                                        field details:
;                                         value   : in Tesla        
;                                         dc_x    : direction cosine for x-component
;                                         dc_y    : y
;                                         dc_y    : z
;               efield     I     str     Structure containing the electric
;                                        field details:
;                                         value   : in Volts       
;                                         dc_x    : direction cosine for x-component
;                                         dc_y    : y
;                                         dc_y    : z
;               obs        I     str     Structure containing the viewing
;                                        line of sight details:
;                                         dc_x    : direction cosine for x-component
;                                         dc_y    : y
;                                         dc_y    : z
;                                         sigma   : specific sigma polarisation 
;                                                   intensity multiplier
;                                         pi      : specific pi polarisation 
;                                                   intensity multiplier
;               n_lower    I     integer lower principal quantum number
;                                        of requested transition
;               n_upper    I     integer upper principal quantum number
;               adf22      I     str     adf22 file for population of
;                                        upper level (optional parameter).
;               wave_comp  O     float() Wavelength components of Stark feature
;               emiss_comp O     float() Associated emission components
;
; OPTIONAL:     emiss_doppler O  float() Emission Doppler-broadened on to
;                                        a wavelength grid (see /doppler
;                                        keyword below)
;               wave_doppler  O  float() Wavelength grid for emiss_doppler
;
; KEYWORDS      doppler            -     If specified convolve the component
;                                        emission with a Doppler broadening.
;                                        Extra inputs are required (but see
;					 also the auto_wave keyword):
;                                        wave_min       float   : min wavelength  
;                                        wave_max       float   : max wavelength     
;                                        npix           int     : number pixels        
;                                        emiss_doppler  float() : See above
;                                        wave_doppler   float() : See above
;		auto_wave          -     Automatically generates a wavelength
;                                        grid. Elimanates the need to supply
;					 wave_min and wave_max. npix can
;					 still be specified but a default
;					 will be chosen if it isn't.
;               nocheck            -     No check will be performed on the
;                                        inputs. Note that the user will get 
;                                        what they deserve.
;               help               -     If specified this comment
;                                        section is written to screen.
; 
;
; NOTES      :  The IDL routines, stark.pro and c5dplr.pro call
;               fortran code.
;
;               If an adf22 dataset is input the population of the
;               upper level is renormalised to that in the adf22
;               dataset. No checking is preformed.
;
;
; EXAMPLE    :  beam   = {mass : 2.0, energy : 40.0e3, te : 10.0, $
;                         density : 4.27e9,                       $
;                         dc_x : 0.0, dc_y : 0.0, dc_z : 1.0}
;               plasma = {mass : 2.0, te : 4.44e3, density : 2.49e13, zeff : 2.0}
;               bfield = {value : 3.3915, dc_x : 0.788, dc_y : 0.0053, dc_z : 0.6152}
;               efield = {value : 0.0000, dc_x : 1.000, dc_y : 0.0000, dc_z : 0.0000}
;               obs    = {dc_x : 0.8701, dc_y : -0.047, dc_z : 0.4905, sigma : 0.51, pi : 1.0}
;               wave_min = 6510
;               wave_max = 6550
;               npix     = 500 
;
;
; AUTHOR     :  Martin O'Mullane
; DATE       :  22-02-2005
; 
;
; MODIFIED:
;       1.1     Martin O'Mullane
;                 - First version.
;       1.2     Martin O'Mullane
;                 - Add direction cosines for beam velocity.
;                 - Add optional adf22 for NU populations.
;       1.3     Allan Whiteford
;                 - Added auto_wave keyword to automatically
;                   generate a  wavelength grid (i.e. no
;                   need for wave_min and wave_max).
;                 - Added keyword to return the wavelength grid
;                   (Works for either an automatically generated
;                   grid or a manually suppled wave_min/wave_max)
;
; VERSION:
;       1.1    22-02-2005 
;       1.2    28-09-2006 
;       1.3    06-08-2008 
;-
;----------------------------------------------------------------------

PRO adas305_check_tags, inval, class

typ = (size(inval))[2]
if typ NE 8 then message, class + ' must be a structure'

names = tag_names(inval)
names = names[sort(names)]
num   = n_tags(inval)

case strlowcase(class) of

  'beam' : begin
             if num NE 7 then message, '7 structure elements are expected in ' + class
             if names[0] NE 'DC_X'    OR $
                names[1] NE 'DC_Y'    OR $
                names[2] NE 'DC_Z'    OR $
                names[3] NE 'DENSITY' OR $
                names[4] NE 'ENERGY'  OR $
                names[5] NE 'MASS'    OR $
                names[6] NE 'TE'      then $
                message, 'A structure tag(s) is incorrect in ' + class
           end
         
  'plasma' : begin
                if num NE 4 then message, '4 structure elements are expected in ' + class
                if names[0] NE 'DENSITY' OR $
                   names[1] NE 'MASS'    OR $
                   names[2] NE 'TE'      OR $
                   names[3] NE 'ZEFF'  then $
                   message, 'A structure tag(s) is incorrect in ' + class
             end

  'bfield' : begin
                if num NE 4 then message, '4 structure elements are expected in ' + class
                if names[0] NE 'DC_X'    OR $
                   names[1] NE 'DC_Y'    OR $
                   names[2] NE 'DC_Z'    OR $
                   names[3] NE 'VALUE' then $
                   message, 'A structure tag(s) is incorrect in ' + class
             end

  'efield' : begin
                if num NE 4 then message, '4 structure elements are expected in ' + class
                if names[0] NE 'DC_X'    OR $
                   names[1] NE 'DC_Y'    OR $
                   names[2] NE 'DC_Z'    OR $
                   names[3] NE 'VALUE' then $
                   message, 'A structure tag(s) is incorrect in ' + class
             end

  'obs'    : begin
                if num NE 5 then message, '5 structure elements are expected in ' + class
                if names[0] NE 'DC_X'    OR $
                   names[1] NE 'DC_Y'    OR $
                   names[2] NE 'DC_Z'    OR $
                   names[3] NE 'PI'      OR $
                   names[4] NE 'SIGMA' then $
                   message, 'A structure tag(s) is incorrect in ' + class
             end

endcase

END

;----------------------------------------------------------------------

PRO adas305_get_stark,  beam           =  beam,         $  
                        plasma         =  plasma,       $
                        bfield         =  bfield,       $
                        efield         =  efield,       $
                        obs            =  obs,          $  
                        n_lower        = n_lower,       $
                        n_upper        = n_upper,       $
                        adf22          = adf22,         $
                        wave_comp      = wave_comp,     $
                        emiss_comp     = emiss_comp,    $
                        doppler        = doppler,       $
                        wave_min       = wave_min,      $
                        wave_max       = wave_max,      $
                        npix           = npix,          $
                        emiss_doppler  = emiss_doppler, $
                        wave_doppler   = wave_doppler,  $
                        auto_wave      = auto_wave,     $
                        nocheck        = nocheck,       $
                        help           = help



; If asked for help

if keyword_set(help) then begin 
   doc_library, 'adas305_get_stark' 
   return
endif


; Dimensions

ndcomp = 300L
ndpix  = 1200L


; Check inputs as a default but bypass for /nocheck keyword.

if NOT keyword_set(nocheck) then begin

   if n_elements(beam) EQ 0 then message, 'Beam parameters must be set'
   if n_elements(plasma) EQ 0 then message, 'Plasma parameters must be set'
   if n_elements(bfield) EQ 0 then message, 'Magnetic field parameters must be set'
   if n_elements(efield) EQ 0 then message, 'Electric field parameters must be set'
   if n_elements(obs) EQ 0 then message, 'Observation parameters must be set'

   if n_elements(n_lower) EQ 0 then message, 'Lower n must be set'
   if n_elements(n_upper) EQ 0 then message, 'Upper n must be set'
   
   if n_elements(adf22) EQ 0 then popu_in = 1.0

   adas305_check_tags, beam,   'beam'
   adas305_check_tags, plasma, 'plasma'
   adas305_check_tags, bfield, 'bfield'
   adas305_check_tags, efield, 'efield'
   adas305_check_tags, obs,    'obs'
   
   if keyword_set(doppler) and not keyword_set(auto_wave)then begin
   
      if n_elements(wave_min) EQ 0 then message, 'Lower wavelength must be set'
      if n_elements(wave_max) EQ 0 then message, 'Upper wavelength must be set'
      if n_elements(npix) EQ 0 then message, 'Number of pixels must be set'
  
      if npix GT ndpix then message, 'Too many pixels specified'
  
   endif
   
   if n_lower EQ n_upper then message, 'Lower and upper n cannot be the same'
   if n_lower GT n_upper then message, 'Lower n must be lower than the supplied upper n'
   
   if n_lower LT 0 OR n_lower GT 3 then message, 'Lower n must be in range 1 - 3'
   if n_upper LT 2 OR n_upper GT 4 then message, 'Upper n must be in range 2 - 4'
   
endif 

; Get optional population of upper level

if n_elements(adf22) EQ 0 then begin 
   
   popu_in = 1.0

endif else begin


   files    = [adf22]
   fraction = [1.0]
   ein      = beam.energy
   te_22    = [plasma.te]
   dens_22  = [plasma.density]

   read_adf21, files=files, energy=ein, te=te_22,  dens=dens_22,   $
               fraction=fraction,  data=data
   popu_in = data[0]
   
endelse

amdeut  = double(beam.mass)
amss    = double(plasma.mass)
bener   = double(beam.energy)
densb   = double(beam.density)
tbev    = double(beam.te)
bmag    = double(bfield.value)
db1     = double(bfield.dc_x)
db2     = double(bfield.dc_y)
db3     = double(bfield.dc_z)
emag    = double(efield.value)
de1     = double(efield.dc_x)
de2     = double(efield.dc_y)
de3     = double(efield.dc_z)
do1     = double(obs.dc_x)
do2     = double(obs.dc_y)
do3     = double(obs.dc_z)
dv1     = double(beam.dc_x)
dv2     = double(beam.dc_y)
dv3     = double(beam.dc_z)
polo    = double(obs.sigma)
polp    = double(obs.pi)
dens    = double(plasma.density)
te      = double(plasma.te)
zeff    = double(plasma.zeff)
nu      = long(n_upper)
nl      = long(n_lower)
ncomp   = 0L         
popu    = double(popu_in)
        
wvcomp  = dblarr(ndcomp)
emcomp  = dblarr(ndcomp)




stark,  amdeut , amss  ,                         $
        bener  , dv1   , dv2   , dv3 , densb ,   $
        bmag   , db1   , db2   , db3 ,           $
        emag   , de1   , de2   , de3 ,           $
        do1    , do2   , do3   ,                 $
        polo   , polp  ,                         $
        dens   , te    , zeff  ,                 $
        nu     , nl    , popu  ,                 $
        ndcomp , ncomp , wvcomp, emcomp

if ncomp GT ndcomp then message, 'Increase ndcomp'

if arg_present(wave_comp)  then wave_comp  = wvcomp[0:ncomp-1] 
if arg_present(emiss_comp) then emiss_comp = emcomp[0:ncomp-1]



if keyword_set(doppler) then begin
 
   if keyword_set(auto_wave) or (n_elements(wave_min) eq 0 and n_elements(wave_max) eq 0) then begin
      wave_min=min(wvcomp[where(wvcomp gt 0.0)]) - 3d0 * 4.6344D-5 * mean(wvcomp) * sqrt(tbev/amss)
      wave_max=max(wvcomp) + 3d0 * 4.6344D-5 * mean(wvcomp) * sqrt(tbev/amss)
      if n_elements(npix) eq 0 then npix=1024
   endif   

   wvmin   = double(wave_min)
   wvmax   = double(wave_max)
   npix_in = double(npix)
   dwave   = dblarr(ndpix)
   
   if wvmin GT wvmax then begin
      tmp   = wvmin
      wvmin = wvmax
      wvmax = tmp
   endif
   
   w_min = min(wvcomp[0:ncomp-1], max=w_max)

   if wvmax LT w_min then begin
      message, 'Cannot apply Doppler broadening', /continue
      message, 'Supplied maximum wavelength too short', /continue
      return
   endif
   if wvmin GT w_max then begin
      message, 'Cannot apply Doppler broadening', /continue
      message, 'Supplied minimum wavelength too long', /continue
      return
   endif
   
   c5dplr, ndpix, npix_in, wvmin, wvmax, ndcomp,     $
           ncomp, wvcomp, emcomp, tbev, amss,        $
           dwave
   
   if arg_present(emiss_doppler) then emiss_doppler = dwave[0:npix-1]
   if arg_present(wave_doppler) then wave_doppler=wave_min+((wave_max-wave_min)/(npix-1))*findgen(npix)

endif

END
