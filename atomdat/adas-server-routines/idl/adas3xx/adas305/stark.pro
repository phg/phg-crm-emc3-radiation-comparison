;----------------------------------------------------------------------
;+
; PROJECT    :  ADAS
;
; NAME       :  stark
;
; PURPOSE    :  
;
; ARGUMENTS  :  All output arguments will be defined appropriately.
;               Inputs will be converted to correct type, if possible,
;               internally without changing calling type.
;
;               The argument list is the same as fortran.
;
;               NAME      I/O    TYPE    DETAILS
; REQUIRED   :  amdeut     I     double  Atomic mass of hydrogen in beam
;               amss       I     double  Atomic mass of hydrogen in plasma
;               bener      I     double  Energy of ith beam component (eV/amu)
;               densb      I     double  Specific neutral beam density (cm-3)
;               bmag       I     double  Specific magnetic field induction (Tesla)
;               db1        I     double  D.C. for x-cpt of bmag
;               db2        I     double  D.C. for y-cpt of bmag
;               db3        I     double  D.C. for z-cpt of bmag
;               emag       I     double  Specific electric field strength (volts)
;               de1        I     double  D.C. for x-cpt of emag
;               de2        I     double  D.C. for y-cpt of emag
;               de3        I     double  D.C. for z-cpt of emag
;               do1        I     double  D.C. for x-cpt of specific viewing line
;               do2        I     double  D.C. for y-cpt of specific viewing line
;               do3        I     double  D.C. for z-cpt of specific viewing line
;               polo       I     double  Specific sigma polarisation intensity multiplier
;               polp       I     double  Specific  pi   polarisation intensity multiplier
;               dens       I     double  Specific plasma electron density (cm-3)
;               te         I     double  Specific plasma electron temperature (eV)
;               zeff       I     double  Specific plasma effective Z
;               nu         I     long    Upper principal quantum number line
;               nl         I     long    Lower principal quantum number line
;               ndcomp     I     long    Maximum number of components
;               ncomp      I     long    Actual number of components
;               wvcomp()   O     double  Wavelength of component
;               emcomp()   O     double  Emissivity of component
;
;
; NOTES      :  Calls fortran code.
;
;
; AUTHOR     :  Martin O'Mullane
;
; DATE       :  21/02/05
;
;
; MODIFIED:
;       1.1     Martin O'Mullane
;                - First version.
;       1.2     Martin O'Mullane
;                - Add beam velocity direction cosines.
;                - Add population of upper level.
;
;
; VERSION:
;       1.1    21-02-2005
;       1.2    28-09-2006
;
;-
;----------------------------------------------------------------------

PRO stark, amdeut , amss  ,                         $
           bener  , dv1   , dv2   , dv3 , densb ,   $
           bmag   , db1   , db2   , db3 ,           $
           emag   , de1   , de2   , de3 ,           $
           do1    , do2   , do3   ,                 $
           polo   , polp  ,                         $
           dens   , te    , zeff  ,                 $
           nu     , nl    , popu  ,                 $
           ndcomp , ncomp , wvcomp, emcomp

; Check number of parameters

if n_params() NE 30 then message, 'Not all parameters present'

; Convert to appropriate type.

amdeut = double(amdeut)
amss   = double(amss)
bener  = double(bener)
densb  = double(densb)
bmag   = double(bmag)
dv1    = double(dv1)
dv2    = double(dv2)
dv3    = double(dv3)
db1    = double(db1)
db2    = double(db2)
db3    = double(db3)
emag   = double(emag)
de1    = double(de1)
de2    = double(de2)
de3    = double(de3)
do1    = double(do1)
do2    = double(do2)
do3    = double(do3)
polo   = double(polo)
polp   = double(polp)
dens   = double(dens)
te     = double(te)
zeff   = double(zeff)
nu     = long(nu)
nl     = long(nl)
popu   = double(popu)
ndcomp = long(ndcomp)
ncomp  = long(ncomp)
wvcomp = double(wvcomp)
emcomp = double(emcomp)


fortdir = getenv('ADASFORT')

dummy = CALL_EXTERNAL(fortdir+'/stark_if.so','stark_if',      $
                      amdeut , amss  ,                        $
                      bener  , dv1   , dv2   , dv3 , densb ,  $
                      bmag   , db1   , db2   , db3 ,          $
                      emag   , de1   , de2   , de3 ,          $
                      do1    , do2   , do3   ,                $
                      polo   , polp  ,                        $
                      dens   , te    , zeff  ,                $
                      nu     , nl    , popu  ,                $
                      ndcomp , ncomp , wvcomp, emcomp)


END
