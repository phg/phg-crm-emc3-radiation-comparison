;----------------------------------------------------------------------
;+
; PROJECT:
;       ADAS
;
; NAME:
;       lform8
;
; PURPOSE:
;       Calculate the l-shell distribution using the LPARMS
;       method of adf01.
;
; USE:
;       General
;
; INPUTS:
;       XLCUTA -  critical l for the fitting (non-integral)
;       PL2A   -  parameter of l-shell fitting
;       PL3A   -  parameter of l-shell fitting
;       L      -  azithumal quantum number.
;
; OPTIONAL INPUTS:
;       None
;
; OUTPUTS:
;       LFORM8    -    l-shell split.
;
; OPTIONAL OUTPUTS:
;       None
;
; KEYWORD PARAMETERS:
;
; NOTES:
;       In the IDL way the input L can be a vector or the parameters
;       can be vectors (of same length).
;
; CALLS:
;       None
;
; SIDE EFFECTS:
;       None
;
; CATEGORY:
;       Adas system.
;
; WRITTEN:
;       Martin O'Mullane
;
; MODIFIED:
;       1.1     Martin O'Mullane
;                - First release
;
; VERSION:
;       1.1     11-07-2007
;
;-----------------------------------------------------------------------------


FUNCTION lform8, xlcut, pl2a, pl3a, l

l_in     = double(reform(l))
xlcut_in = double(reform(xlcut))
pl2a_in  = double(reform(pl2a))
pl3a_in  = double(reform(pl3a))

n_l     = n_elements(l_in)
n_xlcut = n_elements(xlcut_in)
n_pl2a  = n_elements(pl2a_in)
n_pl3a  = n_elements(pl3a_in)

if n_xlcut NE n_pl2a OR n_pl2a NE n_pl3a OR n_xlcut NE n_pl3a then $
   message, 'L-paramaterisation vectors must be same length'

if n_l GT 1 AND n_xlcut GT 1 then begin

   data = dblarr(n_xlcut, n_l)
   for j = 0, n_l-1 do begin

      data[*,j] = ((1.0 - TANH(1.2*(l_in[j]-xlcut_in))) / 2.0 ) *            $
                  ((2.0 * l_in[j] + 1.0) / (2.0 * xlcut_in + 1.0))^pl2a_in + $
                  ((1.0 + TANH(1.2 * (l_in[j] - xlcut_in))) / 2) *           $
                  EXP(pl3a_in * (1.0 - l_in[j] / xlcut_in) )

   endfor

endif else begin

   data = ((1.0 - TANH(1.2*(l_in-xlcut_in))) / 2.0 ) *            $
          ((2.0 * l_in + 1.0) / (2.0 * xlcut_in + 1.0))^pl2a_in + $
          ((1.0 + TANH(1.2 * (l_in - xlcut_in))) / 2) *           $
          EXP(pl3a_in * (1.0 - l_in / xlcut_in) )

endelse

return, data

END
