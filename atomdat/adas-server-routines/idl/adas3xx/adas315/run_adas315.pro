;----------------------------------------------------------------------
;+
; PROJECT    :  ADAS
;
; NAME       :  run_adas315
;
; PURPOSE    :  Generate cross section data for a specified element
;               and charge state from universal adf49 data.
;
; ARGUMENTS  :  All output arguments will be defined appropriately.
;
;               NAME      I/O   TYPE    DETAILS
; INPUTS     :  iz0        I    int     receiver atomic number.
;               iz1        I    int     receiver charge state.
;               energy     I    dbl()   beam energies (eV/amu).
;               adf49      I    str     universal scaled CX cross section dataset.
;               adf01      I    str     output file for cross sections.
;               comments   I    str()   optional comment block to append to
;                                       adf01 dataset.
;
; KEYWORDS   :  lparams    I      -     output l-parameters (but only if
;                                       present in the adf49 file).
;               no_n_norm  I      -     Do not normalise n-shell cross section
;                                       to the total. Default is to normalise.
;               help       I      -     Display header as help.
;
; OUTPUTS    :  all information is returned in ADAS datasets.
;
;
; NOTES      :
;
;
; AUTHOR     :  Adam Foster / Martin O'Mullane
;
; DATE       :  02-07-2007
;
;
;
; MODIFIED:
;       1.1     Martin O'Mullane
;                 - First version.
;       1.2     Martin O'Mullane
;                 - Add optional comment block for adf01 output file.
;       1.3     Adam Foster
;                 - Add more checks to avoid spline ringing.
;                 - Force alpha to have a minimum of 1.3 if the
;                   scaled values is less than this.
;       1.4     Adam Foster
;                 - Force empty high-n shells to use alpha
;                   extrappolation.
;
; VERSION:
;       1.1     02-07-2007
;       1.2     12-09-2007
;       1.3     03-10-2007
;       1.4     03-12-2007
;-
;----------------------------------------------------------------------

PRO run_adas315_calc, iz0, iz1, energy, adf49, lpar, nonorm, fulldata

; Constant for routine

THRESHOLD = 2.0D-40


; Get universal information from adf49

read_adf49, file=adf49, fulldata=all_49

n_eng = n_elements(all_49.enrgya)
n_n   = n_elements(all_49.n)


; Unscale total sigma and n according to requested residual charge
; and determine appropriate nmin and nmax

n_unscl     = dblarr(n_eng, n_n)
signa_unscl = dblarr(n_eng, n_n)
nmin        = 99.0
nmax        = 0.0

for j = 0, n_eng-1 do begin

   signa_unscl[j,*] = all_49.signa[j,*] * (iz1^all_49.alpha) * (iz1^all_49.delta[j])
   n_unscl[j,*]     = all_49.n * (iz1^all_49.gamma[j])

   ind = where(signa_unscl[j,*] GT THRESHOLD, num)
   if num GT 0 then begin
      nmin = min([nmin, reform(n_unscl[j,ind])])
      nmax = max([nmax, reform(n_unscl[j,ind])])
   endif

endfor

eng_unscl = all_49.enrgya * (iz1^all_49.beta)


; require integer nmin and nmax for adf01

nmin  = ceil(nmin)
nmax  = floor(nmax)
num_n = nmax - nmin + 1


; Map signa_unscl onto new n-grid

signa_unscl_n = dblarr(n_eng, num_n) + THRESHOLD


for j = 0, n_eng-1 do begin

   yin = reform(signa_unscl[j, *])
   ind = where(yin GT THRESHOLD, num)

   if num GT 2 then begin

      xin = reform(n_unscl[j, ind])
      yin = yin[ind]

      n1   = min(ceil(xin))
      n2   = max(floor(xin))
      xout = double(indgen(n2-n1+1) + n1)

      xxsple,  xin  = xin,$
               yin  = yin,$
               xout = xout,$
               yout = yout, /log, opt=-1

      signa_unscl_n[j, xout-nmin] = yout

   endif

endfor


; Total cross section

xin = eng_unscl
yin = all_49.sigta * (iz1^all_49.alpha)

xxsple,  xin  = xin,$
         yin  = yin,$
         xout = energy,$
         yout = yout, /extrap,/log

sigta = yout


; Interpolate the unscaled data onto the required energy/n grid
;
; n-shell cross sections
; l paramters
; nl cross sections

num_energy = n_elements(energy)
signa      = dblarr(num_energy, nmax)

for j = 0, num_n-1 do begin

   yin = reform(signa_unscl_n[*, j])
   ind = where(yin GT THRESHOLD, num)

   if num GT 2 then begin

      ; eliminate any n-shell cross section with a dip

      chk = where(signa_unscl_n[ind[0]:ind[num-1], j] EQ THRESHOLD, n1)

      if n1 EQ 0 then begin

         xin = eng_unscl[ind]
         yin = yin[ind]
         xxsple,  xin  = xin,$
                  yin  = yin,$
                  xout = energy,$
                  yout = yout, /log, extrap=extrap, opt=-1

         ; restrict any output energy to input eng_unscl range

         ind_ext = where(extrap EQ 1, n2, complement=ind_intrp)

         ; exclude if n-shell total is less then 1% of the total

         if total(yin) GT 0.01*total(signa_unscl_n) then begin

            if n2 GT 0 then yout[ind_ext] = 0.0

            signa[*, nmin+j-1] = yout

         endif

      endif

   endif

endfor


; Determine actual nmin and nmax

nmin_act = 99
nmax_act = 0

for j = 0, nmax-1 do begin
  in = j+1
  if total(signa[*,j]) GT 0.0 then nmin_act = min([nmin_act, in])
  if total(signa[*,j]) GT 0.0 then nmax_act = max([nmax_act, in])
endfor


; If there are any shells which total zero, remove those below it
; (this is to prevent ringing)

for j = nmin_act-1, nmax_act-1 do begin
  in = j + 1
  if total(signa[*,j]) EQ 0.0 then nmin_act = max([nmin_act, in+1])
endfor


; Re-normalize to total cross section (by default)

signa_tot = total(signa, 2)

if nonorm EQ 0 then begin
   for j = 0, num_n-1 do signa[*,j] = signa[*,j] * sigta / signa_tot
endif

; n extrapolation parameter

alpha = dblarr(num_energy)

for j = 0, num_energy-1 do begin

   nlast = where(signa[j,*] GT THRESHOLD, num)
   if num GT 1 then begin
      nlast    = max(nlast)
      alpha[j] = alog(signa[j,nlast-1] / signa[j, nlast]) / $
                 alog(double(nlast) / double(nlast-1) )

; Force a minimum value of alpha to 1.3

      alpha[j] = max([alpha[j], 1.3])

   endif

endfor


; Check for any empty high n shells

for j = 0, num_energy-1 do begin

   nlast = where(signa[j,*] GT THRESHOLD, num)
   if num GT 1 then begin
      nlast = max(nlast)
      if (nlast LT n_elements(signa[j,*])-1) then begin
        for k = nlast, n_elements(signa[j,*])-1 do begin
           signa[j,k] = signa[j,k-1] / ((double(k)/double(k-1))^alpha[j])
        endfor
      endif
   endif

endfor


; Split into nl cross sections

if lpar EQ 1 then begin

   ; parameters

   lparams_unscl = dblarr(6, num_energy)

   for j = 0, 5 do begin

      xxsple, xin  = eng_unscl, yin  = reform(all_49.lparms[j,*]),$
              xout = energy,    yout = yout

      lparams_unscl[j, *] = yout

   endfor

   xlcuta = lparams_unscl[3,*] * (iz1^lparams_unscl[0,*])
   pl2a   = lparams_unscl[4,*] * (iz1^lparams_unscl[1,*])
   pl3a   = lparams_unscl[5,*] * (iz1^lparams_unscl[2,*])


   ; nl cross sections

   sigla  = dblarr(num_energy, nmax*(nmax+1)/2)

   il     = indgen(nmax)
   lsplit = lform8(xlcuta, pl2a, pl3a, il)

   for j = 0, num_n-1 do begin

      in = j + 1
      if in GT 1 then begin
         for il = 0, in-1 do begin
            ind = i4idfl(in, il, /idl_index)
            sigla[*, ind] = signa[*,j] * lsplit[*,il] / total(lsplit[*,0:in-1],2)
         endfor
      endif else sigla[*, 0] = signa[*,j]

   endfor

   lparms = 1
   lsetl  = 1

   lforma = intarr(num_energy) + 8

endif else begin

   lforma = intarr(num_energy)
   xlcuta = dblarr(num_energy)
   pl2a   = dblarr(num_energy)
   pl3a   = dblarr(num_energy)
   sigla  = -1

   lparms = 0
   lsetl  = 0

endelse


; Fill in adf01 fulldata structure

str_d = '  '
strput, str_d, strupcase(all_49.symbd)

symbr = xxesym(iz0, /upper)
str_r = '  '
strput, str_r, symbr


fulldata  = { symbr   :  str_r,       $
              symbd   :  str_d,       $
              izr     :  iz1,         $
              izd     :  all_49.izd,  $
              indd    :  all_49.indd, $
              nenrgy  :  num_energy,  $
              nmin    :  nmin_act,    $
              nmax    :  nmax_act,    $
              lparms  :  lparms,      $
              lsetl   :  lsetl,       $
              enrgya  :  energy,      $
              alphaa  :  alpha,       $
              lforma  :  lforma,      $
              xlcuta  :  xlcuta,      $
              pl2a    :  pl2a,        $
              pl3a    :  pl3a,        $
              sigta   :  sigta,       $
              signa   :  signa,       $
              sigla   :  sigla        }


END
;----------------------------------------------------------------------



PRO run_adas315, iz0       = iz0,       $
                 iz1       = iz1,       $
                 adf49     = adf49,     $
                 adf01     = adf01,     $
                 energy    = energy,    $
                 lparams   = lparams,   $
                 no_n_norm = no_n_norm, $
                 comments  = comments,  $
                 help      = help


; If asked for help

if keyword_set(help) then begin
   doc_library, 'run_adas315'
   return
endif


; Check the file names

if n_elements(adf49) EQ 0 then begin

    message,'An adf49 driver file is required'

endif else begin

   a49 = adf49
   file_acc, a49, exist, read, write, execute, filetype
   if exist ne 1 then message, 'adf49 file does not exist '+ a49
   if read ne 1 then message, 'adf49 file cannot be read from this userid '+ a49

endelse


if n_elements(adf01) EQ 0 then message,'An adf01 output file is required'


; Species information

if n_elements(iz0) EQ 0 then begin
   message,'Receiver atomic number is required'
endif else iz0_in = iz0

if n_elements(iz1) EQ 0 then begin
   message,'Receiver ionisation stage is required'
endif else iz1_in = iz1


; Energy

if n_elements(energy) EQ 0 then begin
   energy_in = [ 0.01,  0.02,  0.05,  0.10,  0.20,   0.50,  1.00, $
                 2.00,  5.00, 10.00, 15.00, 20.00,  25.00, 30.00, $
                35.00, 40.00, 45.00, 50.00, 75.00, 100.00] * 1000.0
   message, 'Energies set to defaults', /continue
endif else energy_in = double(energy)


if keyword_set(lparams) then lpar = 1 else lpar = 0
if keyword_set(no_n_norm) then nonorm = 1 else nonorm = 0


; Run the calculation

run_adas315_calc, iz0_in, iz1_in, energy_in, adf49, lpar, nonorm, all_01


; Write the output file

if n_elements(comments) GT 0 then write_adf01, outfile=adf01, fulldata=all_01, $
                                               comments=comments               $
                             else write_adf01, outfile=adf01, fulldata=all_01

END
