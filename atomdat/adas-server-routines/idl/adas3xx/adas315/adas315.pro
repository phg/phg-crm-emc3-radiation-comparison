;+
; PROJECT:
;       ADAS
;
; NAME:
;       ADAS315
;
; PURPOSE:
;       Generate adf01 datasets from universal scaled adf49 data.
;
; USE:
;       First the system settings must be established by calling
;       adas_sys_set.pro then adas315.pro is called.
;
;       adas_sys_set, adasrel, fortdir, userroot, centroot, $
;                     devlist, devcode, font_large, font_small, edit_fonts
;       adas315,   adasrel, fortdir, userroot, centroot, devlist, $
;                  devcode, font_large, font_small, edit_fonts
;
; INPUTS:
;       ADASREL - A string indicating the ADAS system version,
;                 e.g ' ADAS RELEASE: ADAS93 V1.4'.  The first
;                 character should be a space.
;
;       FORTDIR - Not applicable here.
;
;       USERROOT - A string holding the path to the root directory of
;                  the user's adas data. e.g '/disk/bowen/adas'
;                  This root directory will be used by adas to construct
;                  other path names.  For example the users default data
;                  for adas205 should be in /disk/bowen/adas/adf04.  In
;                  particular the user's default interface settings will
;                  be stored in the directory USERROOT+'/defaults'.  An
;                  error will occur if the defaults directory does not
;                  exist.
;
;       CENTROOT - Like USERROOT, but this directory points to the
;                  central data area for the system.  User defaults are
;                  not stored on CENTROOT.
;
;       DEVLIST - A string array of hardcopy device names, used for
;                 graphical output. e.g ['Post-Script','HP-PCL','HP-GL']
;                 This array must mirror DEVCODE.  DEVCODE holds the
;                 actual device names used in a SET_PLOT statement.
;
;       DEVCODE - A string array of hardcopy device code names used in
;                 the SET_PLOT statement, e.g ['PS', 'PCL', 'HP']
;                 This array must mirror DEVLIST.
;
;       FONT_LARGE - The name of a larger font e.g 'courier_bold14'
;
;       FONT_SMALL - The name of a smaller font e.g 'courier_bold12'
;
;       EDIT_FONTS - A structure of two fonts used in the table editor
;                    adas_edtab.pro, {font_norm:'',font_input:''}.
;                    The two fonts are used to differentiate between
;                    editable and non-editable parts of the table. You
;                    may have to experiment to get two fonts which give
;                    the same default spacing in the edit widget.
;                    e.g {font_norm:'helvetica_bold14', $
;                         font_input:'helvetica_oblique14'}
;
; OPTIONAL INPUTS:
;       None
;
; OUTPUTS:
;       None - Note this routine should leave all inputs un-modified.
;
; OPTIONAL OUTPUTS:
;       None
;
; KEYWORD PARAMETERS:
;       None
;
; CALLS:
;
; SIDE EFFECTS:
;
; CATEGORY:
;       Adas system.
;
; WRITTEN:
;       Martin O'Mullane
;
; MODIFIED:
;       1.1     Martin O'Mullane
;                - First release.
;
; VERSION:
;       1.1     05-07-2007
;
;-
;-----------------------------------------------------------------------------

PRO adas315,    adasrel, fortdir, userroot, centroot,                   $
                devlist, devcode, font_large, font_small, edit_fonts


; Initialisation

adasprog = ' PROGRAM: ADAS315 v1.1'
deffile  = userroot+'/defaults/adas315_defaults.dat'
bitfile  = centroot+'/bitmaps'
device   = ''


; Restore or set the defaults

files = findfile(deffile)
if files[0] eq deffile then begin

    restore, deffile
    inval.centroot = centroot+'/adf49/'
    inval.userroot = userroot+'/adf49/'

endif else begin

    inval = { ROOTPATH   :  userroot+'/adf49',    $
              FILE       :  '',                   $
              CENTROOT   :  centroot+'/adf49',    $
              USERROOT   :  userroot+'/adf49'     }

    a01name = userroot + '/pass/adas315_adf01.pass'

   ; match n_eng to ndeng in adas315_out.pro

    n_eng   = 40
    outval  = { A01OUT    :   0,            $
                A01APP    :   -1,           $
                A01REP    :   0,            $
                A01DSN    :   '',           $
                A01DEF    :   a01name,      $
                A01MES    :   '',           $
                LPARAMS   :   0,            $
                IZ0_R     :   0,            $
                IZ1_R     :   0,            $
                n_eng     :   n_eng,        $
                energy    :   dblarr(n_eng) }

endelse


date = xxdate()


;----------------------------
; Ask for input adf49 dataset
;----------------------------

LABEL100:

    adas_in, inval, act, wintitle='ADAS315 INPUT',     $
                         title='Choose adf49 universal file:', $
                         font=font_large
    rep = strupcase(act)

    if rep eq 'CANCEL' then goto, LABELEND

    dsn49 = inval.rootpath + inval.file



;----------------------------------------
; Ask for output ion and destination file
;----------------------------------------

LABEL300:


    rep = adas315_out(outval, dsn49, bitfile,        $
                       FONT_LARGE = font_large,      $
                       FONT_SMALL = font_small,      $
                       EDIT_FONTS = edit_fonts  )

    if rep eq 'CANCEL' then goto, LABEL100
    if rep eq 'MENU' then goto, LABELEND



;--------------------------------
; Calculate and write outut files
;--------------------------------

    if outval.a01out EQ 1 then begin

       iz0     = outval.iz0_r
       iz1     = outval.iz1_r
       energy  = outval.energy * 1000.0                ; panels in keV/amu
       adf49   = inval.rootpath + inval.file
       adf01   = outval.a01dsn
       lparams = outval.lparams

       run_adas315, iz0 = iz0, iz1 = iz1, adf49 = adf49, $
                    energy = energy, adf01 = adf01, lparams = lparams

    endif

;----------------
; Return for more
;----------------

goto, LABEL100


;----------------------------
; Exit and save defaults
;----------------------------

LABELEND:

    save, inval,  outval, filename=deffile

END
