;+
; PROJECT:
;       ADAS
;
; NAME:
;       ADAS315_OUT
;
; PURPOSE:
;       Acquire the output dataset names, energy range and receiver
;       element and charge state. These are passed back to adas315.pro.
;
;
; EXPLANATION:
;
;
; NOTES:
;
;
; INPUTS:
;        outval  - Structure of output files
;        dsn49   - scaled data file.
;        bitfile - Directory of bitmaps for menu button
;
;
; OPTIONAL INPUTS:
;       None
;
; OUTPUTS:
;       The function returns a string rep
;           rep = 'CONTINUE' if everything is ok
;           rep = 'CANCEL' if there is a problem or indeed if the user
;                          closes the widget with the mouse.
;           rep = 'MENU' if user want to exit to menu at this point
;
; OPTIONAL OUTPUTS:
;       None
;
; KEYWORD PARAMETERS:
;       FONT_LARGE - The name of a larger font e.g 'courier_bold14'
;
;       FONT_SMALL - The name of a smaller font e.g 'courier_bold12'
;
;
; CALLS:
;
;       ADAS315_OUT_MENU   : React to menu button event. The standard IDL
;                            reaction to button events cannot deal with
;                            pixmapped buttons. Hence the special handler.
;       ADAS315_OUT_EVENT  : Reacts to cancel and Done. Does the file
;                            existence error checking.
;
; SIDE EFFECTS:
;
; CATEGORY:
;       Widgets
;
; WRITTEN:
;       Martin O'Mullane
;
; MODIFIED:
;       1.1     Martin O'Mullane
;                - First release
;
; VERSION:
;       1.1     28-06-2007
;
;-
;-----------------------------------------------------------------------------



PRO ADAS315_OUT_MENU, event

; The 'normal' get_value from a widget_button does not work with a
; bitmap button. Hence this specialised event handler.

; Get the info structure out of the user value of the top-level base.

Widget_Control, event.top, Get_UValue=info

 formdata = {cancel : 0, menu:1}
 *info.ptrToFormData =formdata
 widget_Control, event.top, /destroy

END


;-----------------------------------------------------------------------------


PRO ADAS315_OUT_EVENT, event

; React to button events - Cancel and Done but not menu (this requires a
; specialised event handler ADAS315_OUT_MENU). Also deal with the passing
; directory output Default button here.

; Get the info structure out of the user value of the top-level base.

Widget_Control, event.top, Get_UValue=info

; Clear any messages

widget_control, info.messID, set_value = ' '


; Process events

CASE event.id OF

  info.cancelID : begin
                    formdata = {cancel:1, menu : 0}
                    *info.ptrToFormData = formdata
                    widget_Control, event.top, /destroy
                  end

  info.defeID   : begin

                        messval = 'Confirm Overwrite energies with defaults'
                        action = popup(message=messval, font=info.font,       $
                                       buttons=['Confirm','Cancel'],          $
                                       title='ADAS315 Warning:-')

                        if action eq 'Confirm' then begin

                           widget_control, info.engID, get_value=tempval

                           eng_def = [ 0.01,  0.02,  0.05,  0.10,  0.20,   0.50,  1.00, $
                                       2.00,  5.00, 10.00, 15.00, 20.00,  25.00, 30.00, $
                                      35.00, 40.00, 45.00, 50.00, 75.00, 100.00]
                           n_eng = n_elements(eng_def)

                           tempval.value[0,0:n_eng-1] = $
                                   strtrim(string(eng_def[0:n_eng-1],format=info.num_format),2)

                           if n_eng LT info.ndeng then tempval.value[0,n_eng:info.ndeng-1] = ''

                           widget_control, info.engID, set_value=tempval

                        endif

                  end

  info.scleID   : begin

                        messval = 'Confirm Overwrite energies with scaled defaults'
                        action = popup(message=messval, font=info.font,       $
                                       buttons=['Confirm','Cancel'],          $
                                       title='ADAS315 Warning:-')

                        if action eq 'Confirm' then begin

                           err  = 0
                           mess = ''

                           widget_control, info.engID, get_value=tempval

                           widget_control, info.z1ID, Get_Value=tmp
                           if tmp EQ 0 then begin
                              err  = 1
                              mess = 'A non-zero receiver Z1 is required'
                           endif else iz1 = tmp


                           if err EQ 0 then begin

                              eng_scl = (info.data49.enrgya / 1000.0) * (iz1^info.data49.beta)

                              n_eng = n_elements(eng_scl)

                              tempval.value[0,0:n_eng-1] = $
                                      strtrim(string(eng_scl[0:n_eng-1],format=info.num_format),2)

                              if n_eng LT info.ndeng then tempval.value[0,n_eng:info.ndeng-1] = ''

                              widget_control, info.engID, set_value=tempval

                          endif else begin
                             widget_Control, info.messID, Set_Value=mess
                          endelse

                        endif

                  end


  info.clrID    : begin

                    widget_control, info.engID, get_value=tempval
                    tempvals = where(strcompress(tempval.value[0,*], /remove_all) NE '')

                    if tempvals[0] NE -1 then begin

                        messval = 'Are you sure you want to clear the table?'
                        action = popup(message=messval, font=info.font,       $
                                       buttons=['Confirm','Cancel'],          $
                                       title='ADAS315 Warning:-')

                        if action EQ 'Confirm' then begin
                            empty = strarr(info.ndeng)
                            tempval.value[0,*] = empty
                            widget_control, info.engID, set_value=tempval
                        endif

                    endif

                  end

  info.doneID   : begin

                    ; gather the data for return to calling program

                    err  = 0
                    mess = ''

                    ; species and L params inclusion

                    widget_control, info.lparID, Get_Value=lpar

                    widget_control, info.z0ID, Get_Value=tmp

                    if tmp EQ 0 then begin
                       err  = 1
                       mess = 'A non-zero receiver Z0 is required'
                    endif else iz0 = tmp

                    if err EQ 0 then begin

                       widget_control, info.z1ID, Get_Value=tmp

                       if tmp EQ 0 then begin
                          err  = 1
                          mess = 'A non-zero receiver Z1 is required'
                       endif else iz1 = tmp

                    endif

                    ; output file

                    widget_Control, info.a01ID, Get_Value=a01

                    ; no need to set mess as it is flagged in the cw

                    if a01.outbut EQ 1 AND strtrim(a01.message) NE '' then err=1

                    ; energies

                    if err EQ 0 then begin

                       widget_control, info.engID, get_value=tempval

                       blanks  = where(strtrim(tempval.value[0,*],2) ne '', maxw)
                       temp    = double(tempval.value)
                       energy  = reform(temp[0,*])

                       if total(energy) EQ 0.0 then begin
                          err = 1
                          mess = 'Select at least one energy'
                       endif

                    endif

                    if err EQ 0 then begin
                       formdata = { a01      : a01,      $
                                    iz0      : iz0,      $
                                    iz1      : iz1,      $
                                    lparams  : lpar,     $
                                    energy   : energy,   $
                                    cancel   : 0,        $
                                    menu     : 0         }
                       *info.ptrToFormData = formdata
                       widget_control, event.top, /destroy
                    endif else begin
                       widget_Control, info.messID, Set_Value=mess
                    endelse

             end

  else : print,'ADAS315_OUT_EVENT : You should not see this message! '

ENDCASE

END

;-----------------------------------------------------------------------------



FUNCTION ADAS315_OUT, outval, dsn49, bitfile,   $
                      FONT_LARGE = font_large,  $
                      FONT_SMALL = font_small,  $
                      EDIT_FONTS = edit_fonts


; Constants of this subroutine

  ndeng      = 40
  num_format = '(E10.3)

; Set defaults for keywords and extract info for paper.txt question

  IF n_elements(font_large) eq 0 THEN font_large = ''
  IF n_elements(font_small) eq 0 THEN font_small = ''


  a01val   =  { outbut   : outval.A01OUT, $
                appbut   : outval.A01APP, $
                repbut   : outval.A01REP, $
                filename : outval.A01DSN, $
                defname  : outval.A01DEF, $
                message  : outval.A01MES  }

; Energies

  engdata = strarr(1,ndeng)

  if outval.n_eng gt 0 then begin

    engdata[0,0:outval.n_eng-1] = $
            strtrim(string(outval.energy[0:outval.n_eng-1],format='(f10.2)'),2)

  endif

  if outval.n_eng LT ndeng then engdata[outval.n_eng:ndeng-1] = ''



; Create modal top level base widget


  parent = Widget_Base(Column=1, Title='ADAS 315 OUTPUT', $
                       XOFFSET=100, YOFFSET=20)
  rc     = widget_label(parent,value='  ',font=font_large)
  base   = widget_base(parent, /column)


  mcol  = widget_base(parent, /column)


; species selection on left


  mrow   = widget_base(mcol, /row)

  mc2    = widget_base(mrow, /column)

  gap    = widget_label(mc2, value='  ', font=font_large)
  z0ID   = cw_field(mc2, title='Receiver Z0 : ', $
                       /integer, xsize=3,        $
                       fieldfont=font_small, font=font_small)

  z1ID   = cw_field(mc2, title='Receiver Z1 : ', $
                       /integer, xsize=3,        $
                       fieldfont=font_small, font=font_small)

  gap    = widget_label(mc2, value='  ', font=font_large)
  gap    = widget_label(mc2, value='  ', font=font_large)

  mr2    = widget_base(mc2, /row)
  lparID = cw_bgroup(mr2, ['NO','YES'], set_value=outval.lparams, $
                     label_top='Include L parameters :',            $
                     /no_release, /exclusive, font=font_large)


; energy selection on right

  gap    = widget_label(mrow, value='   ', font=font_large)
  mc2    = widget_base(mrow, /column, /frame)

  engtitle = 'Beam energy (units: keV/amu)'
  enghead  = ['Beam energy']

  engID    = cw_adas_table(mc2, engdata,                         $
                           coledit=[1],                          $
                           limits=[2],                           $
                           title=engtitle,                       $
                           font_large=font_large,                $
                           font_small=font_small,                $
                           colhead=enghead, /scroll,             $
                           ytexsize=12,                          $
                           num_form=num_format,                  $
                           order=[1],                            $
                           event_funct = 'ADAS_PROC_NULL_EVENTS')

  clrID  = widget_button(mc2, value='Clear Table',font=font_large)
  defeID = widget_button(mc2, value='Default Energies',font=font_large)
  scleID = widget_button(mc2, value='Scaled Energies',font=font_large)



; adf01 output

  gap   = widget_label(mcol, value='   ', font=font_large)
  mc2   = widget_base(mcol, /column, /frame)

  a01ID = cw_adas_outfile(mc2, OUTPUT='adf01 file         ',   $
                                VALUE=a01val, FONT=font_large, xsize=50)



; Message area

  messID = widget_label(parent,value='     Choose output options   ',font=font_large)


; Buttons

  menufile = bitfile + '/menu.bmp'
  read_X11_bitmap, menufile, bitmap1

  mrow     = widget_base(parent,/row)
  menuID   = widget_button(mrow,value=bitmap1,font=font_large, $
                           event_pro='ADAS315_OUT_MENU')
  cancelID = widget_button(mrow,value='Cancel',font=font_large)
  doneID   = widget_button(mrow,value='Done',font=font_large)


; Initialise

  read_adf49, file=dsn49, fulldata=data49

  widget_control, z0ID, set_value=outval.iz0_r
  widget_control, z1ID, set_value=outval.iz1_r

; Realize the ADAS315_OUT input widget.

   dynlabel, parent
   widget_Control, parent, /realize

; Create a pointer to store the information collected from the form.
; The initial data stored here is set to CANCEL, so nothing needs to
; be done if the user kills the widget with the mouse.

   ptrToFormData = Ptr_New({cancel:1, menu:0})


; Create an info structure with program information.

  info = { messID          :  messID,             $
           menuID          :  menuID,             $
           cancelID        :  cancelID,           $
           doneID          :  doneID,             $
           a01ID           :  a01ID,              $
           engID           :  engID,              $
           clrID           :  clrID,              $
           defeID          :  defeID,             $
           scleID          :  scleID,             $
           z0ID            :  z0ID,               $
           z1ID            :  z1ID,               $
           lparID          :  lparID,             $
           data49          :  data49,             $
           ndeng           :  ndeng,              $
           num_format      :  num_format,         $
           font            :  font_large,         $
           parent          :  parent,             $
           ptrToFormData   :  ptrToFormData       }


; Store the info structure in the user value of the top-level base and launch
; the widget into the world.

  widget_control, parent, Set_UValue=info

  XManager, 'ADAS315_OUT', parent, Event_Handler='ADAS315_OUT_EVENT'


; When the widget is destroyed, the block is released, and we
; return here. Get the form data that was collected by the form
; and stored in the pointer location.

formdata = *ptrToFormData

rep='CONTINUE'
IF N_Elements(formdata) EQ 0 THEN BEGIN
   Ptr_Free, ptrToFormData
   rep ='CANCEL'
ENDIF

IF formdata.cancel EQ 1 THEN BEGIN
   Ptr_Free, ptrToFormData
   rep ='CANCEL'
   RETURN,rep
ENDIF

IF formdata.menu EQ 1 THEN BEGIN
   Ptr_Free, ptrToFormData
   rep = 'MENU'
   RETURN, rep
ENDIF

if rep eq 'CONTINUE' then begin

    energy = formdata.energy
    ind    = where(energy NE 0.0, n_eng)

    outval =  { A01OUT      :   formdata.a01.outbut,        $
                A01APP      :   formdata.a01.appbut,        $
                A01REP      :   formdata.a01.repbut,        $
                A01DSN      :   formdata.a01.filename,      $
                A01DEF      :   formdata.a01.defname,       $
                A01MES      :   formdata.a01.message,       $
                IZ0_R       :   formdata.iz0,               $
                IZ1_R       :   formdata.iz1,               $
                N_ENG       :   n_eng,                      $
                ENERGY      :   energy[ind],                $
                LPARAMS     :   formdata.lparams            }

   Ptr_Free, ptrToFormData

endif


; And tell ADAS315 that we are finished

RETURN, rep

END
