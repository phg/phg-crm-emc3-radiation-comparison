;+
; PROJECT:
;       ADAS
;
; NAME:
;       ADAS316_OUT
;
; PURPOSE:
;       Acquire the output dataset names and wavelength interval setings.
;       These are passed back to adas316.pro.
;
;
; EXPLANATION:
;
;
; NOTES:
;
;
; INPUTS:
;        outval  - Structure of output files
;        bitfile - Directory of bitmaps for menu button
;
;
; OPTIONAL INPUTS:
;       None
;
; OUTPUTS:
;       The function returns a string rep
;           rep = 'CONTINUE' if everything is ok
;           rep = 'CANCEL' if there is a problem or indeed if the user
;                          closes the widget with the mouse.
;           rep = 'MENU' if user want to exit to menu at this point
;
; OPTIONAL OUTPUTS:
;       None
;
; KEYWORD PARAMETERS:
;       FONT_LARGE - The name of a larger font e.g 'courier_bold14'
;
;       FONT_SMALL - The name of a smaller font e.g 'courier_bold12'
;
;
; CALLS:
;
;       ADAS316_OUT_MENU   : React to menu button event. The standard IDL
;                            reaction to button events cannot deal with
;                            pixmapped buttons. Hence the special handler.
;       ADAS316_OUT_EVENT  : Reacts to cancel and Done. Does the file
;                            existence error checking.
;
; SIDE EFFECTS:
;
; CATEGORY:
;       Widgets
;
; WRITTEN:
;       Martin O'Mullane
;
; MODIFIED:
;       1.1     Martin O'Mullane
;                - First release
;
; VERSION:
;       1.1     28-06-2007
;
;-
;-----------------------------------------------------------------------------



PRO ADAS316_OUT_MENU, event

; The 'normal' get_value from a widget_button does not work with a
; bitmap button. Hence this specialised event handler.

; Get the info structure out of the user value of the top-level base.

Widget_Control, event.top, Get_UValue=info

 formdata = {cancel : 0, menu:1}
 *info.ptrToFormData =formdata
 widget_Control, event.top, /destroy

END


;-----------------------------------------------------------------------------


PRO ADAS316_OUT_EVENT, event

; React to button events - Cancel and Done but not menu (this requires a
; specialised event handler ADAS316_OUT_MENU). Also deal with the passing
; directory output Default button here.

; Get the info structure out of the user value of the top-level base.

Widget_Control, event.top, Get_UValue=info

; Clear any messages

widget_control, info.messID, set_value = ' '


; Process events

CASE event.id OF

  info.cancelid : begin
                    formdata = {cancel:1, menu : 0}
                    *info.ptrToFormData = formdata
                    widget_Control, event.top, /destroy
                  end

  info.doneid   : begin

                    ; gather the data for return to calling program

                    err  = 0
                    mess = ''

                    widget_Control, info.a26ID, Get_Value=a26
                    widget_Control, info.a12ID, Get_Value=a12
                    widget_Control, info.a40ID, Get_Value=a40

                    ; no need to set mess as it is flagged in the cw

                    if a26.outbut EQ 1 AND strtrim(a26.message) NE '' then err=1
                    if a12.outbut EQ 1 AND strtrim(a12.message) NE '' then err=1
                    if a40.outbut EQ 1 AND strtrim(a40.message) NE '' then err=1

                    ; adf40 intervals

                    if err EQ 0 then begin

                       widget_control, info.wvlID, get_value=tempval

                       blanks = where(strtrim(tempval.value[0,*],2) ne '', maxw)
                       temp   = double(tempval.value)
                       npix   = fix(reform(temp[0,*]))
                       wvmin  = reform(temp[1,*])
                       wvmax  = reform(temp[2,*])

                       if total(npix) EQ 0 then begin
                          err = 1
                          mess = 'Select at least one wavelength interval'
                       endif

                    endif

                    if err EQ 0 then widget_control, info.threshID, get_value=thresh

                    if err EQ 0 then begin
                       err = num_chk(thresh[0], sign=0)
                       if err EQ 1 then mess = 'Incorrect threshold'
                    endif

                    if err EQ 0 then begin
                       formdata = { a26      : a26,      $
                                    a12      : a12,      $
                                    a40      : a40,      $
                                    nwvl     : maxw,     $
                                    npix     : npix,     $
                                    wvmax    : wvmax,    $
                                    wvmin    : wvmin,    $
                                    thresh   : thresh[0], $
                                    cancel   : 0,        $
                                    menu     : 0         }
                       *info.ptrToFormData = formdata
                       widget_control, event.top, /destroy
                    endif else begin
                       widget_Control, info.messID, Set_Value=mess
                    endelse

             end

  else : print,'ADAS316_OUT_EVENT : You should not see this message! ',userEvent

ENDCASE

END

;-----------------------------------------------------------------------------



FUNCTION ADAS316_OUT, outval,                  $
                      pipe, bitfile, nloops,   $
                      FONT_LARGE = font_large, $
                      FONT_SMALL = font_small, $
                      EDIT_FONTS = edit_fonts


; Constants of this subroutine

  ndwvl    = 5


; Set defaults for keywords and extract info for paper.txt question

  IF n_elements(font_large) eq 0 THEN font_large = ''
  IF n_elements(font_small) eq 0 THEN font_small = ''


  a26val   =  { outbut   : outval.A26OUT, $
                appbut   : outval.A26APP, $
                repbut   : outval.A26REP, $
                filename : outval.A26DSN, $
                defname  : outval.A26DEF, $
                message  : outval.A26MES  }

  a12val   =  { outbut   : outval.A12OUT, $
                appbut   : outval.A12APP, $
                repbut   : outval.A12REP, $
                filename : outval.A12DSN, $
                defname  : outval.A12DEF, $
                message  : outval.A12MES  }

  a40val =    { outbut   : outval.A40OUT, $
                appbut   : outval.A40APP, $
                repbut   : outval.A40REP, $
                filename : outval.A40DSN, $
                defname  : outval.A40DEF, $
                message  : outval.A40MES  }

; Spectral Intervals

  wvldata = strarr(3,ndwvl)

  if outval.nwvl gt 0 then begin

    wvldata[0,0:outval.nwvl-1] = $
		strtrim(string(outval.npix[0:outval.nwvl-1],format='(i4)'),2)
    wvldata[1,0:outval.nwvl-1] = $
		strtrim(string(outval.wvmin[0:outval.nwvl-1],format='(f10.2)'),2)
    wvldata[2,0:outval.nwvl-1] = $
		strtrim(string(outval.wvmax[0:outval.nwvl-1],format='(f10.2)'),2)

  endif

  if outval.nwvl LT ndwvl then wvldata[*,outval.nwvl:ndwvl-1] = ''



; Get data from fortran

  nloops = -1L
  ndw    = -1L

  readf, pipe, nloops
  readf, pipe, ndw

  if ndw GT ndwvl then begin

     message = 'ndwave dimension mismatch between IDL and FORTRAN'
     buttons = ['OK']
     action  = popup(message = message, buttons = buttons, $
                     font = font_large)

  endif

; Create modal top level base widget


  parent = Widget_Base(Column=1, Title='ADAS 316 OUTPUT', $
                       XOFFSET=100, YOFFSET=20)
  rc     = widget_label(parent,value='  ',font=font_large)
  base   = widget_base(parent, /column)


  mlab  = widget_label(parent,value='  ',font=font_large)
  mcol  = widget_base(parent, /column)


; Passing and principal adf12 output

  a26ID = cw_adas_outfile(mcol, OUTPUT='adf26 file         ',   $
                                VALUE=a26val, FONT=font_large, xsize=50)


; adf12 and adf40 depend on the spectral intervals (and threshold value)


  base  = widget_base(mcol, /column, /frame)

  wvlID = cw_adas_table(base, wvldata,                                  $
              coledit=[1,1,1],		                                $
              colhead=['# pixels','min wave','max wave'],/scroll,       $
	      title='Spectral Intervals',limits=[1,2,2],CELLSIZE = 12, 	$
	      fltint=['(i4)','(f10.2)','(f10.2)'],                      $
              /rowskip,  /difflen, /gaps,                               $
              FONTS = edit_fonts, font_small=font_small,                $
              font_large=font_large,                                    $
              EVENT_FUNCT = 'ADAS_PROC_NULL_EVENTS')


  threshID  = cw_field(base, title='Emissivity Threshold:', $
                       floating=1, fieldfont=font_small, font=font_small)

  a12ID = cw_adas_outfile(base, OUTPUT='adf12 file         ',          $
                                VALUE=a12val, FONT=font_large, xsize=50)

  a40ID = cw_adas_outfile(base, OUTPUT='adf40 file         ',   $
                                VALUE=a40val, FONT=font_large, xsize=50)



; Message area

  messID = widget_label(parent,value='     Choose output options   ',font=font_large)


; Buttons

  menufile = bitfile + '/menu.bmp'
  read_X11_bitmap, menufile, bitmap1

  mrow     = widget_base(parent,/row)
  menuID   = widget_button(mrow,value=bitmap1,font=font_large, $
                           event_pro='ADAS316_OUT_MENU')
  cancelID = widget_button(mrow,value='Cancel',font=font_large)
  doneID   = widget_button(mrow,value='Done',font=font_large)


; Initialise threshold value

  widget_control, threshID, set_value=outval.thresh


; Realize the ADAS316_OUT input widget.

   dynlabel, parent
   widget_Control, parent, /realize

; Create a pointer to store the information collected from the form.
; The initial data stored here is set to CANCEL, so nothing needs to
; be done if the user kills the widget with the mouse.

   ptrToFormData = Ptr_New({cancel:1, menu:0})


; Create an info structure with program information.

  info = { messID          :  messID,             $
           menuID          :  menuID,             $
           cancelID        :  cancelID,           $
           doneID          :  doneID,             $
           a26ID           :  a26ID,              $
           a12ID           :  a12ID,              $
           a40ID           :  a40ID,              $
           wvlID           :  wvlID,              $
           threshID        :  threshID,           $
           font            :  font_large,         $
           parent          :  parent,             $
           ptrToFormData   :  ptrToFormData       }


; Store the info structure in the user value of the top-level base and launch
; the widget into the world.

  widget_control, parent, Set_UValue=info

  XManager, 'ADAS316_OUT', parent, Event_Handler='ADAS316_OUT_EVENT'


; When the widget is destroyed, the block is released, and we
; return here. Get the form data that was collected by the form
; and stored in the pointer location.

formdata = *ptrToFormData

rep='CONTINUE'
IF N_Elements(formdata) EQ 0 THEN BEGIN
   Ptr_Free, ptrToFormData
   rep ='CANCEL'
ENDIF

IF formdata.cancel EQ 1 THEN BEGIN
   Ptr_Free, ptrToFormData
   rep ='CANCEL'
   RETURN,rep
ENDIF

IF formdata.menu EQ 1 THEN BEGIN
   Ptr_Free, ptrToFormData
   rep = 'MENU'
   RETURN, rep
ENDIF

if rep eq 'CONTINUE' then begin

    outval =  { A26OUT      :   formdata.a26.outbut,        $
                A26APP      :   formdata.a26.appbut,        $
                A26REP      :   formdata.a26.repbut,        $
                A26DSN      :   formdata.a26.filename,      $
                A26DEF      :   formdata.a26.defname,       $
                A26MES      :   formdata.a26.message,       $
                A12OUT      :   formdata.a12.outbut,        $
                A12APP      :   formdata.a12.appbut,        $
                A12REP      :   formdata.a12.repbut,        $
                A12DSN      :   formdata.a12.filename,      $
                A12DEF      :   formdata.a12.defname,       $
                A12MES      :   formdata.a12.message,       $
                A40OUT      :   formdata.a40.outbut,        $
                A40APP      :   formdata.a40.appbut,        $
                A40REP      :   formdata.a40.repbut,        $
                A40DSN      :   formdata.a40.filename,      $
                A40DEF      :   formdata.a40.defname,       $
                A40MES      :   formdata.a40.message,       $
                NWVL        :   formdata.nwvl,              $
                NPIX        :   formdata.npix,              $
                WVMIN       :   formdata.wvmin,             $
                WVMAX       :   formdata.wvmax,             $
                THRESH      :   formdata.thresh             }

   Ptr_Free, ptrToFormData

endif


; And tell ADAS316 that we are finished

RETURN, rep

END
