;+
; PROJECT:
;       ADAS
;
; NAME:
;       ADAS316_PLOT
;
; PURPOSE: Plots an annotated overview of the reference case.
;
;
; EXPLANATION:
;
;
; NOTES:
;
;
;
; INPUTS:
;        pipe       - to communicate with fortran.
;
; OPTIONAL INPUTS:
;       None
;
; OUTPUTS:
;       The function returns a string rep
;           rep = 'CONTINUE' if everything is ok
;           rep = 'CANCEL' if there is a problem
;           rep = 'MENU' to abandon the program or if the user
;                          closes the widget with the mouse.
;
; OPTIONAL OUTPUTS:
;       None
;
; KEYWORD PARAMETERS:
;       FONT_LARGE - The name of a larger font e.g 'courier_bold14'
;       FONT_SMALL - The name of a smaller font e.g 'courier_bold12'
;
;
; CALLS:
;
;       ADAS316_PLOT_DRAW       : Plots and annotates the reference case.
;       ADAS316_PLOT_CURSOR     : Prints in status are valur under cursor.
;       ADAS316_PLOT_MENU       : React to presses of menu button.
;       ADAS316_PLOT_EVENT      : Event handler for all other actions.
;
; SIDE EFFECTS:
;
; CATEGORY:
;       Widgets
;
; WRITTEN:
;       Martin O'Mullane
;
; MODIFIED:
;       1.1     Martin O'Mullane
;                - First release
;
; VERSION:
;       1.1     21-06-2007
;
;-
;-----------------------------------------------------------------------------

PRO adas316_plot_draw, plotval,             $
                       print    = print,    $
                       info_str = info_str

; Screen or file?

if (keyword_set(print)) then begin

  grtype = print.devcode( where(print.dev eq print.devlist) )
  if print.paper eq 'A4' then begin
     xsize = 24.0
     ysize = 18.0
  endif else begin
     xsize = 23.0
     ysize = 17.0
  endelse

  set_plot, grtype

  !p.font=0
  device,/color,bits=8,filename=print.dsn,           $
         /Helvetica, font_size=12, /landscape,       $
         xsize=xsize, ysize=ysize

endif

; Get data and plot

nrep  = plotval.nrep
bnact = plotval.bnact
f1    = plotval.f1
f2    = plotval.f2
f3    = plotval.f3

f1_mul = plotval.xpop
f3_mul = plotval.densh / plotval.dens

xmin = min(nrep, max=xmax)
ymin = min(bnact, max=ymax)

ymax = 100.0 * max(f3 * f3_mul)
ymin = 0.01

plot_io, [xmin, xmax], [ymin, ymax], /nodata, $
         xtitle = 'representative n-shell', $
         ytitle = 'bn'

oplot, nrep, bnact
oplot, nrep, f1 * f1_mul, linestyle=1
oplot, nrep, f2,          linestyle=2
oplot, nrep, f3 * f3_mul, linestyle=3


; Annotate - fiddly to get positioning correct

xp = 0.78
yp = 0.9

plots, [xp, xp+0.06], [yp,      yp     ], linestyle=0, /normal
plots, [xp, xp+0.06], [yp-0.03, yp-0.03], linestyle=1, /normal
plots, [xp, xp+0.06], [yp-0.06, yp-0.06], linestyle=2, /normal
plots, [xp, xp+0.06], [yp-0.09, yp-0.09], linestyle=3, /normal

xyouts, xp+0.08, yp,      'b!dn!n', /normal
xyouts, xp+0.08, yp-0.03, 'fI * n1/n+', /normal
xyouts, xp+0.08, yp-0.06, 'fII', /normal
xyouts, xp+0.08, yp-0.09, 'fIII * nh/ne', /normal

xp = 0.5
yp = 0.9

xyouts, xp, yp,      'Z0 (rec) : ' + string(plotval.iz0, format='(i2)'), /normal
xyouts, xp, yp-0.03, 'Z1 (rec) : ' + string(plotval.iz1, format='(i2)'), /normal
xyouts, xp, yp-0.06, 'Temp     : ' + string(plotval.tion, format='(e8.2)') + 'K', /normal
xyouts, xp, yp-0.09, 'Ne        : ' + string(plotval.dens, format='(e8.2)') + 'cm!u-3!n', /normal
xyouts, xp, yp-0.12, 'Np        : ' + string(plotval.densp, format='(e8.2)') + 'cm!u-3!n', /normal
xyouts, xp, yp-0.15, 'NH        : ' + string(plotval.densh, format='(e8.2)') + 'cm!u-3!n', /normal
xyouts, xp, yp-0.18, 'Nimp      : ' + string(plotval.denimp, format='(e8.2)') + 'cm!u-3!n', /normal
xyouts, xp, yp-0.21, 'Nion      : ' + string(plotval.denion, format='(e8.2)') + 'cm!u-3!n', /normal
xyouts, xp, yp-0.24, 'B         : ' + string(plotval.bmener, format='(e8.2)') + 'ev/amu', /normal

for j = 0, plotval.nimp-1 do begin
   str = 'Z, fraction : ' + string(plotval.zimp[j], plotval.frimp[j], format='(i3, f5.1)')
   xyouts, xp, yp - 0.27 - j*0.02, str, /normal
endfor


; If output is going to a file add ADAS release info and tidy up
; For screen print file identification at top.

if keyword_set(print) then begin

  xyouts, 0.01, 0.98, info_str, charsize=0.7, /normal

  user_name  = xxuser()
  print_time = xxdate()

  out_str = 'Printed by : ' + user_name + ' at ' + print_time[1] + ' on ' + $
            print_time[0]

  xyouts, 0.7, 0.98, out_str, charsize=0.7, /normal

  device,/close
  set_plot,'X'
  !p.font=-1

endif


END
;-----------------------------------------------------------------------------


FUNCTION ADAS316_PLOT_NULL_EVENTS, event

   ; The purpose of this event handler is to do nothing
   ; and ignore all events that come to it.

   RETURN, 0

END

;-----------------------------------------------------------------------------

PRO ADAS316_PLOT_CURSOR, event

; Put all mouse movement events in here

; Get the info structure out of the user value of the top-level base.

  Widget_Control, event.id, Get_UValue=handlerid
  Widget_Control, handlerid, Get_UValue=info

  xpos = event.x
  ypos = event.y

  res = convert_coord(xpos, ypos, /device)

  out_str = '(' +  string(res[0], format='(g10.4)') + ', ' + $
                   string(res[1], format='(g10.4)') +')'
  out_str = strtrim(out_str)
  out_str = strcompress(out_str)

  widget_control, info.valueID, set_value=out_str

END
;-----------------------------------------------------------------------------

PRO ADAS316_PLOT_MENU, event

; The 'normal' get_value from a widget_button does not work with a
; bitmap button. Hence this specialised event handler.

; Get the info structure out of the user value of the top-level base.

Widget_Control, event.top, Get_UValue=info


 formdata = {cancel : 0, menu:1}
 *info.ptrToFormData = formdata
 widget_Control, event.top, /destroy

END

;-----------------------------------------------------------------------------


PRO ADAS316_PLOT_EVENT, event

; Get the info structure out of the user value of the top-level base.

Widget_Control, event.handler, Get_UValue=info


; Clear any messages

widget_control, info.messID, set_value = ' '


; Process events

CASE event.id OF

  info.printid : begin

                    print_val = { dsn     : '',                  $
                                  defname : 'adas316_plot.ps',   $
                                  replace :  1,                  $
                                  devlist :  info.devlist,       $
                                  dev     :  info.devlist[0],    $
                                  paper   :  'A4',               $
                                  write   :  0,                  $
                                  message :  'Note: cannot append to graphics files'}

                    act = 0
                    adas_file_gr, print_val, act, font=info.font, $
                                  title='File name for graphical output'

                    print_stuff = { dsn     : print_val.dsn,   $
                                    devcode : info.devcode,    $
                                    devlist : info.devlist,    $
                                    dev     : print_val.dev,   $
                                    paper   : print_val.paper  }

                    if print_val.write EQ 1 then begin

                       info_str = info.adasrel + ' ' + info.adasprog
                       adas316_plot_draw, info.plotval,         $
                                          print = print_stuff,  $
                                          info_str = info_str

                       widget_control, info.messID, $
                                       set_value = 'Plot written to file'

                    endif else begin

                       widget_control, info.messID,$
                                       set_value = 'No file selected'

                    endelse

                  end

  info.cancelid : begin
                    formdata = {cancel:1}
                    *info.ptrToFormData = formdata
                    widget_Control, event.top, /destroy
                  end

  info.doneid   : begin
                    formdata = {cancel    : 0,           $
                                menu      : 0            }
                    *info.ptrToFormData = formdata
                    widget_control, event.top, /destroy
                  end

  else : print,'ADAS316_IN_EVENT : You should not see this message! '

ENDCASE

END

;-----------------------------------------------------------------------------


FUNCTION adas316_plot, pipe, bitfile, devlist, devcode, adasrel, adasprog, $
                       FONT_LARGE = font_large, $
                       FONT_SMALL = font_small

; Set defaults for keywords

IF n_elements(font_large) eq 0 THEN font_large = ''
IF n_elements(font_small) eq 0 THEN font_small = ''


; Get data from fortran for plotting

fdum = 0.0D0
idum = -1L

readf, pipe, fdum
xpop = fdum
readf, pipe, fdum
dens = fdum
readf, pipe, fdum
densh = fdum

imax = -1L
readf, pipe, imax

nrep  = intarr(imax)
bnact = dblarr(imax)
f1    = dblarr(imax)
f2    = dblarr(imax)
f3    = dblarr(imax)

readf, pipe, nrep
readf, pipe, bnact
readf, pipe, f1
readf, pipe, f2
readf, pipe, f3

readf, pipe, idum
iz0 = idum

readf, pipe, idum
iz1 = idum

readf, pipe, fdum
tion = fdum

readf, pipe, fdum
densp = fdum

readf, pipe, fdum
denimp = fdum

readf, pipe, fdum
denion = fdum

readf, pipe, fdum
bmener = fdum

readf, pipe, idum
nimp = idum

zimp  = fltarr(nimp)
frimp = fltarr(nimp)

readf, pipe, zimp
readf, pipe, frimp

plotval = { xpop   : xpop,   $
            dens   : dens,   $
            densh  : densh,  $
            nrep   : nrep,   $
            f1     : f1,     $
            f2     : f2,     $
            f3     : f3,     $
            bnact  : bnact,  $
            iz0    : iz0,    $
            iz1    : iz1,    $
            tion   : tion,   $
            densp  : densp,  $
            denimp : denimp, $
            denion : denion, $
            bmener : bmener, $
            nimp   : nimp,   $
            zimp   : zimp,   $
            frimp  : frimp   }



; Create modal top level base widget

  parent = Widget_Base(Column=1, Title='ADAS316 : Overview of reference case', $
                         xoffset=100, yoffset=20)
  rc     = Widget_label(parent,value='  ',font=font_large)
  base   = Widget_base(parent, /column)

; Draw area

  gap    = Widget_label(base,value=' ',font=font_small)
  mbase  = Widget_base(base, /row)
  lbase  = Widget_base(mbase, /column)
  lab    = Widget_label(parent,value=' ',font=font_large)

  device, get_screen_size=scrsz
  xsize = scrsz[0]*0.5
  ysize = scrsz[1]*0.6

  drawID = Widget_draw(lbase, ysize=ysize, xsize=xsize, $
                       /motion_events, event_pro='ADAS316_PLOT_CURSOR', $
                       uvalue=parent)

  widget_control,drawID,get_value=wid
  wset, wid


; Place for value under cursor

  gap       = widget_label(lbase, value='  ', font=font_large)
  valueID   = Widget_label(lbase,value='  ',font=font_small)
  gap       = widget_label(lbase, value='  ', font=font_large)


; Buttons

  messID = widget_label(parent, value='  ', font=font_large)

  menufile = bitfile + '/menu.bmp'
  read_X11_bitmap, menufile, bitmap1

  mrow     = widget_base(parent,/row)
  menuID   = widget_button(mrow,value=bitmap1,font=font_large, $
                           event_pro='ADAS316_PLOT_MENU')
  gap      = widget_label(mrow, value='  ', font=font_large)
  printID  = widget_button(mrow,value='Print',font=font_large)
  gap      = widget_label(mrow, value='  ', font=font_large)
  cancelID = widget_button(mrow,value='Cancel',font=font_large)
  doneID   = widget_button(mrow,value='Done',font=font_large)


; Realize the ADAS316_PLOT input widget.

   dynlabel, parent
   widget_Control, parent, /realize

; Plot the first transition - must wait until after it is realized!

   adas316_plot_draw, plotval


; Create a pointer to store the information collected from the form.
; The initial data stored here is set to CANCEL, so nothing needs to
; be done if the user kills the widget with the mouse.

   ptrToFormData = Ptr_New({cancel:1, menu:0})

; Create an info structure with program information.

  info = { messID            :    messID,            $
           menuID            :    menuID,            $
           printID           :    printID,           $
           cancelID          :    cancelID,          $
           doneID            :    doneID,            $
           valueID           :    valueID,           $
           font              :    font_large,        $
           devlist           :    devlist,           $
           devcode           :    devcode,           $
           adasrel           :    adasrel,           $
           adasprog          :    adasprog,          $
           plotval           :    plotval,           $
           ptrToFormData     :    ptrToFormData      }


; Store the info structure in the user value of the top-level base and launch
; the widget into the world.

  widget_control, parent, Set_UValue=info

  XManager, 'ADAS316_PLOT', parent, Event_Handler='ADAS316_PLOT_EVENT'


; When the widget is destroyed, the block is released, and we
; return here. Get the form data that was collected by the form
; and stored in the pointer location.

  formdata = *ptrToFormData

  if n_elements(formdata) EQ 0 then begin
     rep ='CANCEL'
     ptr_free, ptrtoformdata
     return, rep
  endif

  if formdata.cancel EQ 1 then begin
     Ptr_Free, ptrToFormData
     rep ='CANCEL'
     return, rep
  endif

  if formdata.menu EQ 1 then begin
     Ptr_Free, ptrToFormData
     rep ='MENU
     return, rep
  endif

  Ptr_Free, ptrToFormData
  rep='CONTINUE'
  return, rep

END
