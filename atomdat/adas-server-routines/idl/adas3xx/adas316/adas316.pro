;+
; PROJECT:
;       ADAS
;
; NAME:
;       ADAS316
;
; PURPOSE:
;       Run the bundle-n CX.
;
; USE:
;       First the system settings must be established by calling
;       adas_sys_set.pro then adas316.pro is called.
;
;       adas_sys_set, adasrel, fortdir, userroot, centroot, $
;                     devlist, devcode, font_large, font_small, edit_fonts
;       adas316,   adasrel, fortdir, userroot, centroot, devlist, $
;                  devcode, font_large, font_small, edit_fonts
;
; INPUTS:
;       ADASREL - A string indicating the ADAS system version,
;                 e.g ' ADAS RELEASE: ADAS93 V1.4'.  The first
;                 character should be a space.
;
;       FORTDIR - Not applicable here.
;
;       USERROOT - A string holding the path to the root directory of
;                  the user's adas data. e.g '/disk/bowen/adas'
;                  This root directory will be used by adas to construct
;                  other path names.  For example the users default data
;                  for adas205 should be in /disk/bowen/adas/adf04.  In
;                  particular the user's default interface settings will
;                  be stored in the directory USERROOT+'/defaults'.  An
;                  error will occur if the defaults directory does not
;                  exist.
;
;       CENTROOT - Like USERROOT, but this directory points to the
;                  central data area for the system.  User defaults are
;                  not stored on CENTROOT.
;
;       DEVLIST - A string array of hardcopy device names, used for
;                 graphical output. e.g ['Post-Script','HP-PCL','HP-GL']
;                 This array must mirror DEVCODE.  DEVCODE holds the
;                 actual device names used in a SET_PLOT statement.
;
;       DEVCODE - A string array of hardcopy device code names used in
;                 the SET_PLOT statement, e.g ['PS', 'PCL', 'HP']
;                 This array must mirror DEVLIST.
;
;       FONT_LARGE - The name of a larger font e.g 'courier_bold14'
;
;       FONT_SMALL - The name of a smaller font e.g 'courier_bold12'
;
;       EDIT_FONTS - A structure of two fonts used in the table editor
;                    adas_edtab.pro, {font_norm:'',font_input:''}.
;                    The two fonts are used to differentiate between
;                    editable and non-editable parts of the table. You
;                    may have to experiment to get two fonts which give
;                    the same default spacing in the edit widget.
;                    e.g {font_norm:'helvetica_bold14', $
;                         font_input:'helvetica_oblique14'}
;
; OPTIONAL INPUTS:
;       None
;
; OUTPUTS:
;       None - Note this routine should leave all inputs un-modified.
;
; OPTIONAL OUTPUTS:
;       None
;
; KEYWORD PARAMETERS:
;       None
;
; CALLS:
;
; SIDE EFFECTS:
;
; CATEGORY:
;       Adas system.
;
; WRITTEN:
;       Martin O'Mullane
;
; MODIFIED:
;       1.1     Martin O'Mullane
;                - First release.
;
; VERSION:
;       1.1     21-06-2007
;
;-
;-----------------------------------------------------------------------------

PRO adas316,    adasrel, fortdir, userroot, centroot,                   $
                devlist, devcode, font_large, font_small, edit_fonts


; Initialisation and IDL v5 check

adasprog = ' PROGRAM: ADAS316 v1.1'
deffile  = userroot+'/defaults/adas316_defaults.dat'
bitfile  = centroot+'/bitmaps'
device   = ''

thisRelease = StrMid(!Version.Release, 0, 1)
IF thisRelease LT '5' THEN BEGIN
   message = 'Sorry, ADAS316 requires IDL 5 or higher'
   tmp = popup(message=message, buttons=['Accept'],font=font_large)
   goto, LABELEND
ENDIF


; Restore or set the defaults

files = findfile(deffile)
if files[0] eq deffile then begin

    restore, deffile
    inval.centroot = centroot+'/adf25/a25_p316/'
    inval.userroot = userroot+'/adf25/a25_p316/'

endif else begin

    inval = { ROOTPATH   :  userroot+'/adf25/a25_p316/',    $
              FILE       :  '',                             $
              CENTROOT   :  centroot+'/adf25/a25_p316/',    $
              USERROOT   :  userroot+'/adf25/a25_p316/'     }

    a26name = userroot + '/pass/adas316_adf26.pass'
    a12name = userroot + '/pass/adas316_adf12.pass'
    a40name = userroot + '/pass/adas316_adf40.pass'

    outval  = { A26OUT    :   0,                          $
                A26APP    :   -1,                         $
                A26REP    :   0,                          $
                A26DSN    :   '',                         $
                A26DEF    :   A26name,                    $
                A26MES    :   '',                         $
                A12OUT    :   0,                          $
                A12APP    :   -1,                         $
                A12REP    :   0,                          $
                A12DSN    :   '',                         $
                A12DEF    :   A12name,                    $
                A12MES    :   '',                         $
                A40OUT    :   0,                          $
                A40APP    :   -1,                         $
                A40REP    :   0,                          $
                A40DSN    :   '',                         $
                A40DEF    :   A40name,                    $
                A40MES    :   '',                         $
                NWVL      :   0,                          $
                NPIX      :   intarr(5),                  $
                WVMIN     :   dblarr(5),                  $
                WVMAX     :   dblarr(5),                  $
                THRESH    :   1.0D-12                     }

endelse

;-------------------
; Start fortran code
;-------------------

spawn, fortdir+'/adas316.out', unit=pipe, /noshell, pid=pid

date = xxdate()
printf, pipe, date[0]


;----------------------------
; Ask for input adf25 dataset
;----------------------------

LABEL100:

    adas_in, inval, act, wintitle='ADAS316 INPUT',     $
                         title='Choose adf25 driver:', $
                         font=font_large
    rep = strupcase(act)

    printf, pipe, rep
    if rep eq 'CANCEL' then goto, LABELEND

    dsn = inval.rootpath + inval.file
    printf, pipe, dsn


;-----------------------
; Display reference case
;-----------------------

LABEL200:

    rep = adas316_plot(pipe, bitfile, devlist, devcode, adasrel, adasprog,$
                       FONT_LARGE=font_large, FONT_SMALL = font_small)

    printf, pipe, rep
    if rep eq 'CANCEL' then goto, LABEL100
    if rep eq 'MENU' then goto, LABELEND



;---------------------
; Ask for output files
;---------------------

LABEL300:

    rep = adas316_out(outval, pipe, bitfile, nloops,    $
                       FONT_LARGE = font_large,         $
                       FONT_SMALL = font_small,         $
                       EDIT_FONTS = edit_fonts  )

    printf, pipe, rep
    if rep eq 'CANCEL' then goto, LABEL200
    if rep eq 'MENU' then goto, LABELEND

    ; Pass information to fortran

    printf, pipe, outval.nwvl
    for j = 0, outval.nwvl-1 do begin
       printf, pipe, outval.npix[j]
       printf, pipe, outval.wvmin[j]
       printf, pipe, outval.wvmax[j]
    endfor
    printf, pipe, outval.thresh


    if outval.a26out EQ 1 then printf, pipe, outval.a26dsn else printf, pipe, 'NULL'
    if outval.a12out EQ 1 then printf, pipe, outval.a12dsn else printf, pipe, 'NULL'
    if outval.a40out EQ 1 then printf, pipe, outval.a40dsn else printf, pipe, 'NULL'


    ; Keep the user updated

    adas_progressbar, pipe     = pipe,         $
                      n_call   = nloops,       $
                      adasprog = 'ADAS316',    $
                      font     = font_large


;----------------
; Return for more
;----------------

goto, LABEL100


;----------------------------
; Exit and save defaults
;----------------------------

LABELEND:

    save, inval,  outval, filename=deffile

END
