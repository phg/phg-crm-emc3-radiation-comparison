;----------------------------------------------------------------------
;+
; PROJECT    :  ADAS
;
; NAME       :  cgbnhs
;
; PURPOSE    :  Run the core bundle-n code from IDL.
;
; ARGUMENTS  :  Most of the input and output is via structures.
;
;               NAME      I/O    TYPE    DETAILS
; REQUIRED   :
;              adf01       I     str     adf01 CX cross sections
;              adf18       I     str     adf18 map from low n to high n
;              ion         I     struc
;                                        { z0 : nuclear charge
;                                          z1 : ion state
;              plasma      I     struc
;                                        { denion : ion density (cm-3)
;                                          zeff   : zeff
;                                          zimp() : impurity charge
;                                          amimp():           mass
;                                          frimp():           fraction
;                                          te     : electron temperature (K)
;                                          tion   : ion temperature (K)
;                                          bmeng  : beam energy (eV/amu)
;                                          bmdens : beam density (cm-3)
;              radiation   I     struc
;                                        { ts : temperature external radiation field (K)
;                                          w  : general radiation dilution factor
;                                          w1 : external radiation field dilution factor
;                                               for photo-ionisation form the ground level
;              collision   I     struc
;                                        { cion    : adjustment multiplier for ground ionisation
;                                          cpy     : adjustment multiplier for VR cross sections
;                                          nip     : range of delta n for IP cross section (LE 4)
;                                          intd    : order Maxwell quadrature for IP cross section (LE 3)
;                                          iprs    : beyond nip - 0 : VR (default) 1 : PR
;                                          ilow    : 0 do not access low level data, 1 access data
;                                                    (accessing low level data is not implemented)
;                                          ionip   : 0 - exclude ion impact collisions
;                                                    1 - include ion impact excitation and ionisation
;                                          nionip  : range of delta n for ion impact excitation
;                                          ilprs   : 0 - Vainshtein, 1 - Lodge-Percival-Richards
;                                          ivdisp  : 0 - ion impact at Maxwellianenergies
;                                                    1 - ion impact at displaced thermal energies according
;                                                        to the neutral beam energy parameter
;              diel        I     struc
;                                        { cor()   : Bethe correction factors
;                                          epsil() : reduced energy core transition
;                                          fij()   : absorption oscillator strength
;                                          wij()   : dilution factor of core DR transition
;              n_rep       I     struc
;                                        { nmin     : lowest n for population
;                                          nmax     : highest n
;                                          nrep()   : representative n-shells
;                                          wbrep()  : dilution factors
;                                          defect() : quantum defect
; OUTPUTS    :
;              result      O     struc
;                                        { dens     : electron density (cm-3)
;                                          densp    : ion density (cm-3)
;                                          denimp   : impurity density (cm-3)
;                                          brep()   : representative bn factor
;                                          dexpbn() : exp(I_n/kT_e)bn for representative n-shell
;                                          pop()    : population of representative n-shell
;                                          dexpte() : exp(I_n/kT_e) for representative n-shell
;                                          alfa     : collisional radiative recombination (cm3 s-1)
;                                          s        : collisional radiative ionisation (cm3 s-1)
;                                          rl       : line power (erg cm3 s-1)
;                                          rr       : radiative recombination power (erg cm3 s-1)
;                                          rd       : dielectronic power (erg cm3 s-1)
;                                          rb       : Bremsstrahlung power (erg cm3 s-1)
;
; KEYWORDS   : help        I      -     Display header as help.
;              projection  I      -     if set include projection (default no)
;
;
; NOTES      :  - Calls fortran code.
;               - Most of the data for the input structures can be
;                 found in adf25 datasets. However this routine operates
;                 on one temperature/density/beam power set, not an
;                 array as in the adf25 dataset.
;               - This is a low level routine. adas3xx_bn.pro is better suited
;                 for population calculations.
;
;
; AUTHOR     :  Martin O'Mullane
;
; DATE       :  14-07-2009
;
;
; MODIFIED:
;       1.1     Martin O'Mullane
;               - First version.
;
; VERSION:
;       1.1    14-07-2009
;
;-
;----------------------------------------------------------------------

PRO cgbnhs, adf01      = adf01,      $
            adf18      = adf18,      $
            projection = projection, $
            ion        = ion,        $
            plasma     = plasma,     $
            radiation  = radiation,  $
            collision  = collision,  $
            diel       = diel,       $
            n_rep      = n_rep,      $
            result     = result,     $
            help       = help


; If asked for help

if keyword_set(help) then begin
   doc_library, 'cgbnhs'
endif


; Parameters for storage

ndrep  = 30L
ndcor  = 20L
nddiel = 10L
nddef  = 10L
ndimp  = 10L
ndlev  = 550L
ndein  = 30L



; Convert inputs to correct type/sizes

cxfile = string(replicate(32B, 120))
strput, cxfile, adf01, 0
exfile = string(replicate(32B, 120))
strput, exfile, adf18, 0

z0 = double(ion.z0)
z  = double(ion.z1)

if keyword_set(projection) then il_lproj = 1L else il_lproj = 0L

denion = double(plasma.denion)
nimp   = long(plasma.nimp)
if nimp GT 0 then begin
   zimp   = double(plasma.zimp)
   frimp  = double(plasma.frimp)
   amimp  = double(plasma.amimp)
endif else begin
   zimp = 0.0D0
   frimp = 0.0D0
   amimp = 0.0D0
endelse
zeff   = double(plasma.zeff)


zimpa  = dblarr(ndimp)
amimpa = dblarr(ndimp)
frimpa = dblarr(ndimp)


if nimp EQ 0 then begin

   if zeff EQ 1.0 then begin
      denimp = 0.0D0
      densp  = denion
      dens   = denion
   endif else message, 'Inconsistent zeff'

endif else begin

   zsum1  = total(zimp * frimp)
   zsum2  = total(zimp^2 * frimp)

   if abs(zeff - zsum2/zsum1) LE 0.0 then begin

      denimp = denion
      densp  = 0.0D0
      dens   = zsum1 * denion

   endif else begin

      dens   = ( (1.0D0 - zsum1) * zsum2 + zsum1 * (zsum2 - 1.0D0) ) * $
               denion / (zeff * (1.0D0 - zsum1) + (zsum2 - 1.0D0))
      densp  = (zsum2 - zeff * zsum1) * $
               denion / (zeff * (1.0D0 - zsum1) + (zsum2 - 1.0D0))
      denimp = denion - densp

   endelse

   zimpa[0:nimp-1]  = zimp
   amimpa[0:nimp-1] = amimp
   frimpa[0:nimp-1] = amimp

endelse


te       = double(plasma.te)
tp       = double(plasma.tion)
ts       = double(radiation.ts)
w        = double(radiation.w)
w1       = double(radiation.w1)

cion     = double(collision.cion)
cpy      = double(collision.cpy)
nip      = long(collision.nip)
intd     = long(collision.intd)
iprs     = long(collision.iprs)
ilow     = long(collision.ilow)
ionip    = long(collision.ionip)
nionip   = long(collision.nionip)
ilprs    = long(collision.ilprs)
ivdisp   = long(collision.ivdisp)

bmener   = double(plasma.bmeng)
densh    = double(plasma.bmdens)


nmin     = long(n_rep.nmin)
nmax     = long(n_rep.nmax)
imax     = n_elements(n_rep.nrep)
jdef     = n_elements(n_rep.defect)
nrep     = lonarr(ndrep+1)
wbrep    = dblarr(ndrep)
defect   = dblarr(nddef)

nrep[0:imax-1]   = n_rep.nrep
wbrep[0:imax-1]  = n_rep.wbrep
defect[0:jdef-1] = n_rep.defect

jcor  = n_elements(diel.cor)
jmax  = n_elements(diel.epsil)
cor   = dblarr(ndcor)
epsil = dblarr(nddiel)
fij   = dblarr(nddiel)
wij   = dblarr(nddiel)

cor[0:jcor-1]   = double(diel.cor)
epsil[0:jmax-1] = double(diel.epsil)
fij[0:jmax-1]   = double(diel.fij)
wij[0:jmax-1]   = double(diel.wij)



; Allocate storage for  outputs

brep   = dblarr(ndrep)
dexpbn = dblarr(ndrep)
pop    = dblarr(ndrep)
dexpte = dblarr(ndlev)
alfa   = 0.0D0
s      = 0.0D0
rl     = 0.0D0
rr     = 0.0D0
rd     = 0.0D0
rb     = 0.0D0


; Get Path

fortdir = getenv('ADASFORT')
if fortdir eq '' then message, 'Cannot locate ADAS binaries'

; Call wrapper routine - unload from memory if nsuph1 is set


dummy = CALL_EXTERNAL(fortdir+'/cgbnhs_if.so','cgbnhs_if',  $
                      ndrep, ndcor, nddiel, nddef, ndimp,   $
                      ndlev, ndein, z0, il_lproj, dens,     $
                      densp, te, tp, ts, w,                 $
                      z, cion, cpy, w1, nip,                $
                      intd, iprs, ilow, ionip, nionip,      $
                      ilprs, ivdisp, zeff, nimp, zimpa,     $
                      amimpa, frimpa, denimp, bmener, densh,$
                      nmin, nmax, imax, nrep, wbrep,        $
                      jcor, cor, jmax, epsil, fij,          $
                      wij, jdef, defect, brep, dexpbn,      $
                      pop, dexpte, alfa, s, rl,             $
                      rr, rd, rb, cxfile, exfile)

if arg_present(result) then begin

   result = { dens   :  dens,              $
              densp  :  densp,             $
              denimp :  denimp,            $
              brep   :  brep[0:imax-1],    $
              dexpbn :  dexpbn[0:imax-1],  $
              pop    :  pop[0:imax-1],     $
              dexpte :  dexpte[0:nmax-1],  $
              alfa   :  alfa,              $
              s      :  s,                 $
              rl     :  rl,                $
              rr     :  rr,                $
              rd     :  rd,                $
              rb     :  rb                 }

endif

END
