;----------------------------------------------------------------------
;+
; PROJECT    :  ADAS
;
; NAME       :  run_adas316
;
; PURPOSE    :  Prepare effective emission coefficient data for
;               bundle-n charge exchange and output via adf12 and
;               adf40 datasets.
;
; ARGUMENTS  :  All output arguments will be defined appropriately.
;
;               NAME      I/O   TYPE    DETAILS
; INPUTS     :  adf25      I    str     driver file.
;               adf26      I    str     detailed information on reference case.
;               adf12      I    str     effective emission for each transition
;                                       within all (input) wavelength ranges.
;               adf40      I    str     feature emission for input wavelength
;                                       ranges.
;               npix       I    int()   number of pixels. (5 ranges permitted)
;               wvmin      I    float() minimum wavelengths (A)
;               wvmax      I    float() maximum wavelengths (A)
;               thresh     I    float   emissivity threshold (defaults to 1e-12).
;
; KEYWORDS   :  help       I      -     Display header as help.
;
; OUTPUTS    :  all information is returned in ADAS datasets.
;
;
; NOTES      :  - run_adas316 used the spawn command. Therefore IDL v5.3
;                 cannot be used. Any other version will work.
;               - A emissivity threshold and wavelength intervals are required
;                 for adf12 and adf40 output. If not set a wide range
;                 (100-10000A) is used as a default.
;
;
; AUTHOR     :  Martin O'Mullane
;
; DATE       :  02-07-2007
;
;
;
; MODIFIED:
;       1.1     Martin O'Mullane
;                 - First version.
;       1.2     Martin O'Mullane
;                 - thresh_in should be double not long.
;
; VERSION:
;       1.1     02-07-2007
;       1.2     20-03-2019
;-
;----------------------------------------------------------------------
PRO run_adas316_calc, dsn25, dsn26, dsn12, dsn40,       $
                      npix, wvmin, wvmax, emis_thresh

; Now run the ADAS316 code - the input has been checked already.

fortdir = getenv('ADASFORT')
spawn, fortdir+'/adas316.out', unit=pipe, /noshell, PID=pid

; Input screen

date = xxdate()
printf, pipe, date[0]

printf, pipe, 'DONE'
printf, pipe, dsn25


; Plot status screen

fdum = 0.0D0
idum = -1L
imax = -1L
nimp = -1L

readf, pipe, fdum
readf, pipe, fdum
readf, pipe, fdum

readf, pipe, imax

nrep  = intarr(imax)
bnact = dblarr(imax)
f1    = dblarr(imax)
f2    = dblarr(imax)
f3    = dblarr(imax)

readf, pipe, nrep
readf, pipe, bnact
readf, pipe,f1
readf, pipe,f2
readf, pipe,f3

readf, pipe, idum
readf, pipe, idum
readf, pipe, fdum
readf, pipe, fdum
readf, pipe, fdum
readf, pipe, fdum
readf, pipe, fdum

readf, pipe, nimp
zimpa  = dblarr(nimp)
frimpa = dblarr(nimp)
readf, pipe, zimpa
readf, pipe, frimpa

printf, pipe, 'CONTINUE'


; Output (and calculations) screen

nloops = -1L
ndw    = -1L

readf, pipe, nloops
readf, pipe, ndw

printf, pipe, 'CONTINUE'

nwvl = n_elements(npix)
printf, pipe, nwvl
for j = 0, nwvl-1 do begin
   printf, pipe, npix[j]
   printf, pipe, wvmin[j]
   printf, pipe, wvmax[j]
endfor
printf, pipe, emis_thresh

printf, pipe, dsn26
printf, pipe, dsn12
printf, pipe, dsn40

idum = -1L
for j = 0, nloops-1 do readf, pipe, idum

printf, pipe, 'CANCEL'

; Wait for fortran to write output files before finishing

adas_wait, pid, delay=0.3


END
;----------------------------------------------------------------------------



PRO run_adas316, adf25  = adf25,  $
                 adf26  = adf26,  $
                 adf12  = adf12,  $
                 adf40  = adf40,  $
                 npix   = npix,   $
                 wvmin  = wvmin,  $
                 wvmax  = wvmax,  $
                 thresh = thresh, $
                 help   = help


; If asked for help

if keyword_set(help) then begin
   doc_library, 'run_adas316'
   return
endif


; First check that we are not using IDL v5.3

if !version.release EQ '5.3' then begin
   message, 'This version of IDL (v5.3) will not work - ' + $
            'change to v5.0, v5.1, v5.2 or v5.4 or v6 and above'
   return
endif


; Check that we get all inputs and that they are correct. Otherwise print
; a message and return to command line.

on_error, 2


; Check the file names

if n_elements(adf25) EQ 0 then begin

    message,'An adf25 driver file is required'

endif else begin

   a25 = adf25
   file_acc, a25, exist, read, write, execute, filetype
   if exist ne 1 then message, 'adf25 file does not exist '+ a25
   if read ne 1 then message, 'adf25 file cannot be read from this userid '+ a25

endelse

if n_elements(adf26) NE 0 then a26 = adf26 else a26 = 'NULL'
if n_elements(adf12) NE 0 then a12 = adf12 else a12 = 'NULL'
if n_elements(adf40) NE 0 then a40 = adf40 else a40 = 'NULL'


; Suitable defaults

if n_elements(npix) EQ 0 then begin
   npix_in = 300
   message, 'Number of pixels set to 300', /continue
endif else npix_in = long(npix)

if n_elements(wvmin) EQ 0 then begin
   wvmin_in = 100.0D0
   message, 'Minimum wavelength set to 100A', /continue
endif else wvmin_in = double(wvmin)

if n_elements(wvmax) EQ 0 then begin
   wvmax_in = 10000.0D0
   message, 'Maximum wavelength set to 10000A', /continue
endif else wvmax_in = double(wvmax)

if n_elements(thresh) EQ 0 then begin
   thresh_in = 1.0D-12
   message, 'Emission threshold set to 1e-12', /continue
endif else thresh_in = double(thresh)

n1 = n_elements(npix)
n2 = n_elements(wvmin)
n3 = n_elements(wvmax)

tst = (n1 EQ n2) AND (n1 EQ n3) AND (n2 EQ n3)
if tst EQ 0 then message, 'Make npix, wvmin and wvmax the same length'

if n1 GT 5 then message, 'Too many wavelength intervals defined'

; Run ADAS316

run_adas316_calc, a25, a26, a12, a40, npix_in, wvmin_in, wvmax_in, thresh_in



; Free up allocated units

close, /all


END
