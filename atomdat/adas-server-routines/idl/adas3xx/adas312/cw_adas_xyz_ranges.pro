; Copyright (c) 1997, Strathclyde University.
;
; PROJECT:
;
;       ADAS
;
; NAME:
;
;       CW_ADAS_XYZ_RANGES()
;
; PURPOSE:
;
;       Produces a widget for specifying axis scaling in graphical output.
;
; EXPLANATION:
;
;       This function declares a compound widget to be used for
;       specifying x ,y and z axis ranges to be used in graphical output
;       plotting.  The widget consists of a toggle button, used to
;       activate the remainder of the widget, six text widgets used
;       for numeric data entry and an error message.  When the toggle
;       button is on all text widgets will receive input, when the
;       toggle is off the text widgets are desensitized.
;
;       The four text widgets are used for XMIN, XMAX, YMIN , YMAX,
;       ZMIN and ZMAX.Click on a text widget to direct keyboard input
;       to that input. When the user presses the return key the keyboard
;       input is automatically transfered to the next input.  Also the
;       values entered so far are checked to see if they are legal numeric
;       values.
;
;       When the GET_VALUE call is used, the widget makes a more
;       complete check on the entered numeric values.  It checks that
;       all four values have been entered as legal numbers and that
;       minimum values are less than maximum values.  In the event of
;       an error an error message is issued.  The caller can test to
;       see if the widget input is in error by testing to see if the
;       message returned in the widget value structure is not the
;       null string.
;
;       This widget does not generate any events.
;
; USE:
;       An example of usage;
;
;       pro example_event,event
;       common example_blk,rngid
;       widget_control,event.id,get_value=eventval
;       case eventval of
;           'Done':begin
;                   widget_control,rngid,get_value=state
;                   print,state
;                  end
;           'exit':exit
;       endcase
;end
;pro example
;       common example_blk,rngid
;       value = {  scalbut:0    ,$
;                  xmin:''      ,$
;                  xmax:''      ,$
;                  ymin:''      ,$
;                  ymax:''      ,$
;                  zmin:''      ,$
;                  zmax:''      ,$
;                  GRPRMESS:'' }
;
;       base = widget_base(/column,/frame)
;       rngid = cw_adas_xyz_ranges(base,value=value)
;       rc = widget_button(base,value='Done',uvalue='done')
;       ra = widget_button(base,value='exit')
;       widget_control,base,/realize
;       xmanager,'example',base,event_handler='start_event'
;end
;
;
; INPUTS:
;       PARENT   - The ID of the parent widget.
;
; OPTIONAL INPUTS:
;       None.
;
; OUTPUTS:
;       The return value of this function is the ID of the compound
;       widget which is of type LONG integer.
;
; OPTIONAL OUTPUTS:
;       None
;
; KEYWORD PARAMETERS:
;       VALUE    - A structure describing the value of the widget;
;                  {SCALBUT:0, XMIN:'', XMAX:'', YMIN:'', YMAX:'',$
;                   ZMIN:'',   ZMAX:'', GRPRMESS:''}
;
;                  SCALBUT  State of the activation button 0-off 1-on.
;                  XMIN     Minimum on x-axis, as a string.
;                  XMAX     Maximum on x-axis, as a string.
;                  YMIN     Minimum on y-axis, as a string.
;                  YMAX     Maximum on y-axis, as a string.
;                  ZMIN     Minimum on z-axis, as a string.
;                  ZMAX     Maximum on z-axis, as a string.
;                  GRPRMESS Error message for ranges input.
;                  TITLE    Title for button
;
;       SIGN     - Defines if values need to be positive or negative
;                  if sign < 0 then values must be negative
;                  if sign > 0 then values must be positive
;                  if sign = 0 then values can be either pos. or neg.
;
;       FONT     - A font to use for all text in this widget.
;
;       UVALUE   - A user value for this widget.
;
;       BELOW    - If set to a non-zero value allows the user to
;                  position the axes range boxes below the title
;                  rather than to the side. On wide widgets this
;                  can improve the look of the disply. THe default
;                  is for them to be slightly to the right of the
;                  title, i.e. BELOW=0.
;
;       TITLE   - Defines title associated with the toggle button.
;
; CALLS:
;       NUM_CHK         Called fron RNG_GET_VAL, check numeric values.
;       See side effects for other related widget management routines.
;
; SIDE EFFECTS:
;       Three other routines are included which are used to manage the
;       widget;
;
;       RNG_SET_VAL
;       RNG_GET_VAL()
;       RNG_EVENT()
;
; CATEGORY:
;       Compound Widget
;
; WRITTEN:
;
;       This frame work of this routine was originally written
;       by Andrew Bowen and saved as cw_adas_ranges.pro. This
;       original widget only included the option to scale the
;       X and Y axis. It was then modified by Harvey Anderson
;       to included the Z axis and was then saved as
;       cw_adas_xyz_ranges.pro
;
; MODIFIED:
;       1       Harvey Anderson
;               University of Strathclyde
;               anderson@chamba.phys.strath.ac.uk
;               -First release
;       1.1     Richard Martin
;               Put under SCCS control.
;       1.2     Martin O'Mullane
;                 - The set_val, get_val and event routines now have
;                   unique names. There was a naming conflict with 
;                   cw_adas_ranges but this routine now conforms to
;                   ADAS naming standards.
;                 -rngval is now an anonymous structure.
;
; VERSION:
;       1       24/6/97
;       1.1     17-07-97
;       1.2     13-05-2005
;
;-
;-----------------------------------------------------------------------------

PRO rng_xyz_set_val, id, value


                ;**** Return to caller on error ****
  ON_ERROR, 2

               ;***************************************
               ;****     Retrieve the state        ****
               ;**** Get first_child widget id     ****
               ;**** because state is stored there ****
               ;***************************************

  first_child = widget_info(id, /child)
  widget_control, first_child, get_uvalue = state


                ;**** Sensitise or desensitise with output button setting ****
  if value.scalbut eq 1 then begin
    widget_control,state.exscid,set_button=1
    widget_control,state.rngbasid,/sensitive
    widget_control,state.xminid,set_value=value.xmin
    widget_control,state.xmaxid,set_value=value.xmax
    widget_control,state.yminid,set_value=value.ymin
    widget_control,state.ymaxid,set_value=value.ymax
    widget_control,state.zminid,set_value=value.zmin
    widget_control,state.zmaxid,set_value=value.zmax
  end else if value.scalbut eq 0 then begin
    widget_control,state.exscid,set_button=0
    widget_control,state.rngbasid,sensitive=0
    widget_control,state.xminid,set_value=''
    widget_control,state.xmaxid,set_value=''
    widget_control,state.yminid,set_value=''
    widget_control,state.ymaxid,set_value=''
    widget_control,state.zminid,set_value=''
    widget_control,state.zmaxid,set_value=''
  end

                ;**** Copy message to widget ****
  if strtrim(value.grprmess) ne '' then begin
    message = value.grprmess
  end else begin
    message = ' '
  end
  widget_control,state.messid,set_value=message


                ;**** Copy the new value to state structure ****
  state.rngval.scalbut = value.scalbut
  state.rngval.xmin = value.xmin
  state.rngval.xmax = value.xmax
  state.rngval.ymin = value.ymin
  state.rngval.ymax = value.ymax
  state.rngval.zmin = value.zmin
  state.rngval.zmax = value.zmax
  state.rngval.grprmess = value.grprmess

                ;**** Save the new state ****
  widget_control, first_child, set_uvalue=state, /no_copy

END

;-----------------------------------------------------------------------------


FUNCTION rng_xyz_get_val, id


                ;**** Return to caller on error ****
  ON_ERROR, 2

               ;***************************************
               ;****     Retrieve the state        ****
               ;**** Get first_child widget id     ****
               ;**** because state is stored there ****
               ;***************************************

  first_child = widget_info(id, /child)
  widget_control, first_child, get_uvalue = state

                ;**** Clear existing error message ****
  widget_control,state.messid,set_value=' '
  state.rngval.grprmess = ''

                ;**** Get scaling ranges and copy to value ****
  if state.rngval.scalbut eq 1 then begin

    widget_control,state.xminid,get_value=xmin
    widget_control,state.xmaxid,get_value=xmax
    widget_control,state.yminid,get_value=ymin
    widget_control,state.ymaxid,get_value=ymax
    widget_control,state.zminid,get_value=zmin
    widget_control,state.zmaxid,get_value=zmax
                ;**** Update state ****
    state.rngval.xmin = xmin(0)
    state.rngval.xmax = xmax(0)
    state.rngval.ymin = ymin(0)
    state.rngval.ymax = ymax(0)
    state.rngval.zmin = zmin(0)
    state.rngval.zmax = zmax(0)

    error = 0

                ;**** Check for missing values ****
    if error eq 0 then begin
      if strtrim(state.rngval.xmin) eq '' then error = 1
      if strtrim(state.rngval.xmax) eq '' then error = 1
      if strtrim(state.rngval.ymin) eq '' then error = 1
      if strtrim(state.rngval.ymax) eq '' then error = 1
      if strtrim(state.rngval.zmin) eq '' then error = 1
      if strtrim(state.rngval.zmax) eq '' then error = 1
      if error eq 1 then begin
        state.rngval.grprmess = '**** Missing Value ****'
      end
    end

                ;**** Check for illegal values ****
    if error eq 0 then begin
      if num_chk(state.rngval.xmin,/sign) eq 1 then error = 1
      if num_chk(state.rngval.xmax,/sign) eq 1 then error = 1
      if num_chk(state.rngval.ymin,/sign) eq 1 then error = 1
      if num_chk(state.rngval.ymax,/sign) eq 1 then error = 1
      if num_chk(state.rngval.zmin,/sign) eq 1 then error = 1
      if num_chk(state.rngval.zmax,/sign) eq 1 then error = 1
      if error eq 1 then begin
        state.rngval.grprmess = '**** Illegal Numeric Value ****'
      end
    end

                ;**** Check x range ****
    if error eq 0 then begin
      if float(state.rngval.xmin) ge float(state.rngval.xmax) then error=1
      if error eq 1 then begin
        state.rngval.grprmess = '**** X-MIN must be less than X-MAX ****'
      end
    end

                ;**** Check y range ****
    if error eq 0 then begin
      if float(state.rngval.ymin) ge float(state.rngval.ymax) then error=1
      if error eq 1 then begin
        state.rngval.grprmess = '**** Y-MIN must be less than Y-MAX ****'
      end
    end
                ;**** Check z range ****
    if error eq 0 then begin
      if float(state.rngval.zmin) ge float(state.rngval.zmax) then error=1
      if error eq 1 then begin
        state.rngval.grprmess = '**** Z-MIN must be less than Z-MAX ****'
      end
    end
    if error eq 1 then $
        widget_control,state.messid,set_value=state.rngval.grprmess

  end



  RETURN, state.rngval

END

;-----------------------------------------------------------------------------

FUNCTION rng_xyz_event, event


                ;**** Base ID of compound widget ****
  base=event.handler

               ;***************************************
               ;****     Retrieve the state        ****
               ;**** Get first_child widget id     ****
               ;**** because state is stored there ****
               ;***************************************

  first_child = widget_info(base, /child)
  widget_control, first_child, get_uvalue = state

                ;**** Clear existing error message ****
  widget_control,state.messid,set_value=' '
  state.rngval.grprmess = ''

                ;************************
                ;**** Process Events ****
                ;************************
  CASE event.id OF

    state.exscid: begin
        if state.rngval.scalbut eq 0 then begin
          state.rngval.scalbut = 1
          widget_control,state.exscid,set_button=1
          widget_control,state.rngbasid,/sensitive
          widget_control,state.xminid,set_value=state.rngval.xmin
          widget_control,state.xmaxid,set_value=state.rngval.xmax
          widget_control,state.yminid,set_value=state.rngval.ymin
          widget_control,state.ymaxid,set_value=state.rngval.ymax
          widget_control,state.zminid,set_value=state.rngval.zmin
          widget_control,state.zmaxid,set_value=state.rngval.zmax
        end else begin
          state.rngval.scalbut = 0
          widget_control,state.exscid,set_button=0
          widget_control,state.rngbasid,sensitive=0
          widget_control,state.xminid,get_value=xmin
          widget_control,state.xmaxid,get_value=xmax
          widget_control,state.yminid,get_value=ymin
          widget_control,state.ymaxid,get_value=ymax
          widget_control,state.zminid,get_value=zmin
          widget_control,state.zmaxid,get_value=zmax
          state.rngval.xmin = xmin(0)
          state.rngval.xmax = xmax(0)
          state.rngval.ymin = ymin(0)
          state.rngval.ymax = ymax(0)
          state.rngval.zmin = zmin(0)
          state.rngval.zmax = zmax(0)
          widget_control,state.xminid,set_value=''
          widget_control,state.xmaxid,set_value=''
          widget_control,state.yminid,set_value=''
          widget_control,state.ymaxid,set_value=''
          widget_control,state.zminid,set_value=''
          widget_control,state.zmaxid,set_value=''
        end
    end

    state.xminid: widget_control,state.xmaxid,/input_focus

    state.xmaxid: widget_control,state.yminid,/input_focus

    state.yminid: widget_control,state.ymaxid,/input_focus

    state.ymaxid: widget_control,state.zminid,/input_focus

    state.zminid: widget_control,state.zmaxid,/input_focus

    state.zmaxid: widget_control,state.xminid,/input_focus

    ELSE:

  ENDCASE

                ;**** Check numeric values ****
  if event.id ne state.exscid then begin
    error = 0
    i = 0
    id = [state.xminid, state.xmaxid, state.yminid, state.ymaxid,$
          state.zminid, state.zmaxid ]
    while (error eq 0 and i lt 4) do begin

      valueid = id(i)
      widget_control,valueid,get_value=num
      if strtrim(num(0)) ne '' then begin
        error = num_chk(num(0),/sign)
        if error eq 1 then begin
          state.rngval.grprmess = '**** Illegal Numeric Value ****'
          widget_control,state.messid,set_value=state.rngval.grprmess
          widget_control,valueid,/input_focus
        end
      end
      i = i + 1

    end
  end


                ;**** Save the new state structure ****
    widget_control, first_child, set_uvalue=state, /no_copy

  RETURN, 0L
END

;-----------------------------------------------------------------------------

FUNCTION cw_adas_xyz_ranges, parent, SIGN=sign, BELOW=below,$
                VALUE=value, FONT=font, UVALUE=uvalue, TITLE=title



  IF (N_PARAMS() LT 1) THEN MESSAGE, 'Must specify PARENT for cw_adas_ranges'

                ;**** Set defaults for keywords ****

  IF (NOT KEYWORD_SET(sign)) then sign=0

  IF NOT (KEYWORD_SET(value)) THEN begin
          rngval = {    SCALBUT:0, $
                        XMIN:'', XMAX:'', $
                        YMIN:'', YMAX:'', $
                        ZMIN:'', ZMAX:'', $
                        GRPRMESS:'', SIGN:sign }
        end else begin
          rngval = {    SCALBUT:value.scalbut, $
                        XMIN:value.xmin, XMAX:value.xmax, $
                        YMIN:value.ymin, YMAX:value.ymax, $
                        ZMIN:value.zmin, ZMAX:value.zmax, $
                        GRPRMESS:value.grprmess , SIGN:sign}
        end
  IF NOT (KEYWORD_SET(font)) THEN font = ''
  IF NOT (KEYWORD_SET(uvalue))  THEN uvalue = 0
  IF NOT (KEYWORD_SET(below)) THEN below = 0
  IF NOT (KEYWORD_SET(title)) THEN title='Explicit Scaling'


                ;**** Create the main base for the widget ****
  main    = WIDGET_BASE(parent, UVALUE = uvalue, $
                EVENT_FUNC = "rng_xyz_event", $
                FUNC_GET_VALUE = "rng_xyz_get_val", $
                PRO_SET_VALUE = "rng_xyz_set_val", $
                /COLUMN)

                ;******************************************************
                ;**** Create a dummy widget just to hold value of *****
                ;**** "state" variable so as not to get confused  *****
                ;**** with any other values. Adopt IDL practice   *****
                ;**** of using first child widget                 *****
                ;******************************************************

    first_child = widget_base(main)

    scbasid = widget_base(first_child,/column)

                ;**** Scaling and ranges widget ****
  inbase = widget_base(scbasid,/row)

  if below eq 0 then begin
      base = widget_base(inbase,/column,/nonexclusive)
      exscid = widget_button(base,value=title,font=font)
      rngbasid = widget_base(inbase,/row)
  endif else begin
      base = widget_base(inbase,/column)
      newrow = widget_base(base,/row,/nonexclusive)
      exscid = widget_button(newrow,value=title,font=font)
      rngbasid = widget_base(base,/row)
  endelse

  minbase = widget_base(rngbasid,/column)
  maxbase = widget_base(rngbasid,/column)

  base = widget_base(minbase,/row)
  rc = widget_label(base,value='X-min :',font=font)
  xminid = widget_text(base,/editable,xsize=8,font=font)

  base = widget_base(maxbase,/row)
  rc = widget_label(base,value='X-max :',font=font)
  xmaxid = widget_text(base,/editable,xsize=8,font=font)

  base = widget_base(minbase,/row)
  rc = widget_label(base,value='Y-min :',font=font)
  yminid = widget_text(base,/editable,xsize=8,font=font)

  base = widget_base(maxbase,/row)
  rc = widget_label(base,value='Y-max :',font=font)
  ymaxid = widget_text(base,/editable,xsize=8,font=font)

  base = widget_base(minbase,/row)
  rc = widget_label(base,value='Z-min :',font=font)
  zminid = widget_text(base,/editable,xsize=8,font=font)

  base = widget_base(maxbase,/row)
  rc = widget_label(base,value='Z-max :',font=font)
  zmaxid = widget_text(base,/editable,xsize=8,font=font)

                ;**** Message ****
  if strtrim(rngval.grprmess) eq '' then begin
    message = ' '
  end else begin
    message = rngval.grprmess
  end
  messid = widget_label(scbasid,value=message,font=font)


                ;**** Set initial state according to value ****
  if rngval.scalbut eq 1 then begin
    widget_control,exscid,set_button=1
    widget_control,xminid,set_value=rngval.xmin
    widget_control,xmaxid,set_value=rngval.xmax
    widget_control,yminid,set_value=rngval.ymin
    widget_control,ymaxid,set_value=rngval.ymax
    widget_control,zminid,set_value=rngval.zmin
    widget_control,zmaxid,set_value=rngval.zmax
  end else begin
    widget_control,rngbasid,sensitive=0
  end

                ;**** Create state structure ****
  new_state = { EXSCID:exscid, RNGBASID:rngbasid, MESSID:messid, $
                XMINID:xminid, XMAXID:xmaxid, YMINID:yminid, YMAXID:ymaxid, $
                ZMINID:zminid, ZMAXID:zmaxid, RNGVAL:rngval, $
                FONT:font }

                ;**** Save initial state structure ****
    widget_control, first_child, set_uvalue=new_state,/no_copy

  RETURN, main

END
