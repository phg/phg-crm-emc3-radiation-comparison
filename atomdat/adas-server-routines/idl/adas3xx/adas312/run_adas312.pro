;----------------------------------------------------------------------
;+
; PROJECT    :  ADAS
;
; NAME       :  run_adas312
;
; PURPOSE    :  Runs ADAS312 adf21/adf22 extraction code as an
;               IDL subroutine.
;
; ARGUMENTS  :  All output arguments will be defined appropriately.
;
;               NAME      I/O    TYPE   DETAILS
; INPUTS     :  adf26      I     str    population file from adas310.
;               nlow       I     int    Lower n-level
;               nup        I     int    Upper n-level.
;               outfile    O     str    beam stopping, beam emission
;                                       excited population level or
;                                       filename.
;
;  If nlow and nup are specified a beam emission file is produced.
;  If nup only is specified a beam density file is produced.
;  If neither is specified a beam stopping file is produced.
;
;
; KEYWORDS      help               -    If specified this comment section
;                                       is written to screen.
;
;
; OUTPUTS    :  An adf21 or adf22 file is the output.
;
;
; NOTES      :  run_adas312 uses the spawn command. Therefore IDL v5.3
;               cannot be used. Any other version should work.
;
; AUTHOR     :  Martin O'Mullane
;
; DATE       :  27-11-2008
;
; MODIFIED
;       1.1     Martin O'Mullane
;                 - First version.
;       1.2     Martin O'Mullane
;                 - Improve comments.
;
; DATE
;       1.1     27-11-2008
;       1.2     17-04-2019
;
;-
;----------------------------------------------------------------------


PRO run_adas312, adf26   = adf26,   $
                 nlow    = nlow,    $
                 nup     = nup,     $
                 outfile = outfile, $
                 help    = help


; If asked for help

if keyword_set(help) then begin
   doc_library, 'run_adas312'
   return
endif


; First check that we are not using IDL v5.3

if !version.release EQ '5.3' then begin
   message, 'This version of IDL (v5.3) will not work - ' + $
            'change to any other v5.x or above'
   return
endif


; Check that we get all inputs and that they are correct. Otherwise print
; a message and return to command line.

on_error, 2
close, /all


; Input screen

if n_elements(adf26) EQ 0 then message, 'An adf26 population file is necessary'
a312_adf26 = adf26

if n_elements(outfile) EQ 0 then message, 'An output file is necessary'
a312_outfile = outfile

a312_nup  = -1L
a312_nlow = -1L
option    = -1L

if n_elements(nup) EQ 0  AND n_elements(nlow) EQ 0 then option = 0L

if n_elements(nup) GT 0  AND n_elements(nlow) GT 0 then begin
   a312_nup  = long(nup)
   a312_nlow = long(nlow)
   option    = 1L
endif

if n_elements(nup) GT 0  AND n_elements(nlow) EQ 0 then begin
   a312_nup  = long(nup)
   option    = 2L
endif

if option EQ -1L then message, 'No valid choice'

; remove outfile before proceeding

file_delete, a312_outfile, /quiet


;------------------
; Now run the code
;------------------

fortdir = getenv('ADASFORT')
spawn, fortdir+'/adas312.out', unit=pipe, /noshell, PID=pid

date = xxdate()
printf, pipe, date[0]

rep = 'NO'
printf, pipe, rep
printf, pipe, a312_adf26


; information from adf26 file

readf, pipe, teref
readf, pipe, ebref
readf, pipe, neref
readf, pipe, temin
readf, pipe, temax
readf, pipe, ebmin
readf, pipe, ebmax
readf, pipe, nemin
readf, pipe, nemax

ntemp = 0L
readf, pipe, ntemp
ttemp = strarr(ntemp)
for i = 0,ntemp-1 do begin
   readf, pipe, ftemp
   ttemp[i] = string(ftemp)
endfor

; onto processing

idone   = 1L
icancel = 0L
imenu   = 0L
printf, pipe, idone
printf, pipe, icancel
printf, pipe, imenu

printf, pipe, option + 1

dflag = 0L
case option of

  1 : begin
        printf, pipe, a312_nlow
        printf, pipe, a312_nup
        readf, pipe, dflag
      end
  2 : begin
        printf, pipe, a312_nup
        readf, pipe, dflag
      end
  else : 
endcase

if option EQ 1 AND dflag EQ 0 then $
   message, 'Data for selected transition is unavaliable'

; intercept graphics and final output

idone   = 1L
icancel = 0L
imenu   = 0L
printf, pipe, idone
printf, pipe, icancel
printf, pipe, imenu

printf, pipe, 1L
printf, pipe, a312_outfile
printf, pipe, 0L

idone   = 0
icancel = 0
imenu   = 1
printf, pipe, idone
printf, pipe, icancel
printf, pipe, imenu

; make sure all files are written before returning - close, /all doesn't work!

wait, 2

END
