;
; CATEGORY:
;       Adas system.
;       
; WRITTEN:
;       Harvey Anderson, University of Strathlcyde, 10-07-97
;             
;
; MODIFIED:
;       1.1     Harvey Anderson
;               First Version
;       1.2     Harvey Anderson
;               Introduced the variable dflag to the 
;               structure proc_in.
;       1.3     Harvey Anderson
;               Introduced the variable dflag to the 
;               structure proc_in.
;       1.3     Martin O'Mullane
;               Rename proc_in to procval.
;               Simplify the procval structure.
;
; VERSION:
;       1.1     10-07-97
;       1.2     17-03-99
;       1.3     09-12-2004
;
;-
;-----------------------------------------------------------------------------
PRO CCISPF, procval, pipe

;**********************************;
;                                  ;
;    READ DATA FROM THE FORTRAN    ;
;                                  ;
;**********************************;

readf,pipe,teref
readf,pipe,ebref
readf,pipe,neref
readf,pipe,temin
readf,pipe,temax
readf,pipe,ebmin
readf,pipe,ebmax
readf,pipe,nemin
readf,pipe,nemax

readf,pipe,ntemp
ttemp = strarr(ntemp)
for i = 0,ntemp-1 do begin
   readf,pipe,ftemp
   ttemp(i) = string(ftemp)
endfor

;*****************************************;
;                                         ;
;   SET DEFAULT VALUE IF NOT PROVIDED     ;
;                                         ;
;*****************************************;

IF ( procval.new lt 0 ) THEN BEGIN
        procval = {  new     :      0,     $
                     option  :      1,     $
                     n_low   :    '2',     $
                     n_up    :    '3',     $
                     temin   :  temin,     $
                     temax   :  temax,     $
                     ebmin   :  ebmin,     $
                     ebmax   :  ebmax,     $
                     nemin   :  nemin,     $
                     nemax   :  nemax,     $
                     teref   :  teref,     $
                     ebref   :  ebref,     $
                     neref   :  neref,     $
                     ntemp   :  ntemp,     $
                     ttemp   :  ttemp      }
ENDIF

END
