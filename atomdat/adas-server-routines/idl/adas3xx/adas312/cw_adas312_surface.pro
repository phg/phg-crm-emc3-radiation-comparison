; Copyright (c) 1997, Strathclyde University.
; SCCS info: Module @(#)$Header: /home/adascvs/idl/adas3xx/adas312/cw_adas312_surface.pro,v 1.3 2004/07/06 12:42:07 whitefor Exp $ Date $Date: 2004/07/06 12:42:07 $
;
; CATEGORY:
;	Adas system.
;	
; WRITTEN:
;       Harvey Anderson, University of Strathlcyde, 10-07-97
;             
;
; MODIFIED:
;	1.1	Harvey Anderson
;		First Version
; 	1.2	Harvey Anderson
;		Inserted code which allowed the colour of the
;		surface plot to be retained when the plot was written
;		to a file. 
;	1.3	Harvey Anderson
;		Inserted code which retained the modification of 1.2 but
;		allowed the different file formats to function
;		correctly. Also added message that the plot has been
;		written to a file.
;
; VERSION:
;	1.1	10-07-97
;	1.2	28-07-97
;	1.3	17-04-98
;-
;-----------------------------------------------------------------------------
function cw_adas312_surface_event,event
	common	draw_blk,rotz,rotx,surf_type,axis,hist_surf,$
	        horz_surf,shad_colour,xlog,ylog,zlog,zlocation ,$
	        xmargin,ymargin,zdata,ydata,xdata,x_title ,$
	        y_title,z_title,main_title,adas_info,file_info,$
	        pname,pdevice,stitle,cfactor

	        
widget_control,widget_info(event.handler,/child),get_uvalue=state,/no_copy
case event.id of
	  	
	     state.right:begin
				rotz=rotz+15
				adas312_draw
			end
	     state.left:begin
				rotz=rotz-15
				adas312_draw
			end
	    state.choriz:begin
	    			rotx=rotx+15
	    			adas312_draw
		     end
	    state.achoriz:begin
	    			rotx=rotx-15
	    			adas312_draw
		       end
	    state.zoomin:begin
	    			if xmargin(1) lt 1.0 then begin
				   cfactor=cfactor-0.05
	    			endif else begin
	    			   xmargin=xmargin*0.8
	    			   ymargin=ymargin*0.8
	    			   cfactor=1.00
	    			endelse
	    			adas312_draw
			  end
	   state.zoomout:begin
	   	    		if xmargin(1) lt 1.0 then begin
				   cfactor=cfactor+0.05
				   if cfactor ge 1.0 then xmargin=xmargin*1.25
	   	    		endif else begin
	   			   xmargin=xmargin*1.25
	   			   ymargin=ymargin*1.25
	   			   cfactor=1.00
	   			endelse
	   			adas312_draw
			 end	
	   state.bmenu:begin
	   	new_event={ID:event.handler,TOP:event.top,$
				HANDLER:0L,ACTION:'Menu'}
			end	
	   state.done:begin
	   	new_event={ID:event.handler,TOP:event.top,$
				HANDLER:0L,ACTION:'Done'}
		      end
	   state.prtplot: begin
				if pdevice eq 0 then  begin
                                   dname = 'PS'
				   set_plot,dname
                                   device,filename = pname,/color,bits=8
                                endif
				if pdevice eq 1 then  begin
                                   dname = 'PCL'
                                   set_plot,dname
				   device,filename = pname
                                endif
				if pdevice eq 2 then  begin
                                   dname = 'HP'
 				   set_plot,dname
				   device,filename = pname
                                endif
				adas312_draw
				device,/close 
				set_plot,'X'
 				message = 'Plot  written to print file.'
	    			widget_control,state.info, set_value='Plot  written to print file.'
			  end
	
	    state.hist:begin
	    			surf_type=1
	    			hist_surf=1
	    			horz_surf=0
	    			adas312_draw
	    		end
	    state.horz:begin
	    		   	surf_type=1
	    		   	hist_surf=0
	    		   	horz_surf=1
	    		   	adas312_draw
			    end
	   state.defa:begin
	   			surf_type=1
	   			hist_surf=0
	   			horz_surf=0
	   			adas312_draw
	   		end
	   state.blue_white:begin
	   		 	surf_type=0
	   		 	shad_colour=1
	   		 	adas312_draw
			    end
	   state.red_temp:begin
	   		  	surf_type=0
	   		  	shad_colour=3
	   		  	adas312_draw
			  end
	   state.grey:begin
	   		    	surf_type=0
	   		    	shad_colour=0
	   		    	adas312_draw
		       end
	   state.xaxis_surf:begin
			if ( xlog eq 0 ) then begin
			xlog=1 
			widget_control,state.xaxis_surf,set_value='Yes X-axis'
			endif else begin
			if ( xlog eq 1 ) then begin
			xlog=0
			widget_control,state.xaxis_surf,set_value='X-axis'
			end
			endelse
			adas312_draw
		       end
	   state.yaxis_surf:begin
	   	if ( ylog eq 0 ) then begin
			ylog=1 
			widget_control,state.yaxis_surf,set_value='Yes Y-axis'
			endif else begin
			if ( ylog eq 1 ) then begin
			ylog=0
			widget_control,state.yaxis_surf,set_value='Y-axis'
			end
			endelse
			adas312_draw	  
	 	       end
	   state.zaxis_surf:begin
	   	if ( zlog eq 0 ) then begin
			zlog=1 
			widget_control,state.zaxis_surf,set_value='Yes Z-axis'
		endif else begin
			if ( zlog eq 1 ) then begin
			zlog=0
			widget_control,state.zaxis_surf,set_value='Z-axis'
			end
			endelse
			adas312_draw	  
	 	end	 	       
	   state.low_right:begin
	   			zlocation=1
				adas312_draw
			   end
	   state.low_left:begin
	   		 	zlocation=2
	   		 	adas312_draw
			end
	   state.upp_right:begin
	   		 	zlocation=4
	   		 	adas312_draw
			   end
	   state.upp_left:begin  
	   			zlocation=3
	   			adas312_draw
			 end 
	state.range:begin
			if ( axis ne 1 or axis eq 2 or axis eq 4 ) then begin
			axis=1
			widget_control,state.range,set_value='Extend axis'
			endif else begin
			if ( axis ne 2 or axis eq 1 or axis eq 4 ) then begin
			axis=2
		        widget_control,state.range,set_value='Exact axis'
			end
			endelse
			adas312_draw
			end 
	state.axis_choice:begin
			if ( axis ne 4 or axis eq 1 or axis eq 2 ) then begin
			axis=4 
			widget_control,state.axis_choice,set_value='Show Axis'
			widget_control,state.zaxis,sensitive=0
			widget_control,state.range,sensitive=0
			endif else begin
			if ( axis ne 1 or axis eq 2 or axis eq 4) then begin
			axis=1 
			widget_control,state.axis_choice,set_value='Hide axis'
			widget_control,state.zaxis,sensitive=1
			widget_control,state.range,sensitive=1
			end
			endelse
			adas312_draw	  
			end
	state.no_zaxis:begin
			if ( zlocation ne -1 ) then begin
			zlocation=-1
			widget_control,state.no_zaxis,set_value='Show axis'
			endif else begin
			zlocation=3
			widget_control,state.no_zaxis,set_value='Hide axis'
			endelse
			adas312_draw
			end  
	state.yright:begin
			yaxis_pos=0
			adas312_draw	
		     end
	state.yleft:begin
			yaxis_pos=1
			adas312_draw
			end
	endcase
widget_control,widget_info(event.handler,/child),set_uvalue=state,/no_copy
	return,new_event
end
function cw_adas312_surface,base,state,bitmap1,font=font,setting,proc_out
	common	draw_blk,rotz,rotx,surf_type,axis,hist_surf,$
	        horz_surf,shad_colour,xlog,ylog,zlog,zlocation ,$
	        xmargin,ymargin,zdata,ydata,xdata,x_title ,$
	        y_title,z_title,main_title,adas_info,file_info,$
	        pname,pdevice,stitle,cfactor
	        pname   = proc_out.hardname
	        pdevice = proc_out.devsel
	      	zdata=setting.zdata
		ydata=setting.ydata
		xdata=setting.xdata
		axis=setting.axis
		rotz=setting.rotz
		rotx=setting.rotx
		xlog=setting.xlog
		ylog=setting.ylog
		zlog=setting.zlog
		zlocation=setting.zlocation
		shad_colour=setting.shad_colour
		hist_surf=setting.hist_surf
		horz_surf=setting.horz_surf
		surf_type=setting.surf_type
		xmargin=setting.xmargin
		ymargin=setting.ymargin
		x_title=setting.x_title
		y_title=setting.y_title
		z_title=setting.z_title
		main_title=setting.main_title
		adas_info=setting.adas_info
		file_info=setting.file_info
		stitle=proc_out.gtit1
		cfactor=setting.cfactor
			
	parent=widget_base(base,title='ADAS311 GRAPHICAL OUTPUT',$
		event_func="cw_adas312_surface_event",$
		/column,/frame)
	first_child=widget_base(parent,/column,/frame)
	second_child=widget_base(parent,/row)
	third_child = widget_base(parent,/row)
	first_grandchild=widget_base(first_child,/column,/frame)


	device,get_screen_size=scrsz
	xwidth=scrsz(0)*0.65
	xheight=scrsz(1)*0.65
	surfacedisplay=widget_draw(	first_grandchild,$
				  	xsize=xwidth,$
				  	ysize=xheight,$
				  	retain=2,/frame)

	third_grandchild = widget_base(first_child,/row)
	second_grandchild=widget_base(first_child,/row)
	first_ggchild=widget_base(second_grandchild,/column,/frame)
	second_ggchild=widget_base(second_grandchild,/column,/frame)
	third_ggchild=widget_base(second_grandchild,/column,/frame)
	fourth_ggchild=widget_base(second_grandchild,/column,/frame)

	rc = widget_label(first_ggchild,value='Rotate image about',font=font)
	rc = widget_label(first_ggchild,value=' vertical axis',font=font)
	right=widget_button(first_ggchild,value = 'Anti Clockwise',font=font)
	left=widget_button(first_ggchild,value='Clockwise',font=font)

	rc = widget_label(second_ggchild,value='Rotate image about',font=font)
	rc = widget_label(second_ggchild,value=' horizontal axis',font=font)
	achoriz=widget_button(second_ggchild,value ='Anti Clockwise',font=font)
	choriz=widget_button(second_ggchild,value='Clockwise',font=font)

	rc = widget_label(third_ggchild,value='Magnify Image',font=font)
	rc = widget_label(third_ggchild,value='             ',font=font)
	zoomin=widget_button(third_ggchild,value='Step In',font=font)
	zoomout=widget_button(third_ggchild,value='Step Out',font=font)

	rc = widget_label(fourth_ggchild,value='Image Display',font=font)
	rc = widget_label(fourth_ggchild,value='             ',font=font)
	sprop=widget_button(fourth_ggchild,value='Surface Properties',$
	font=font,/menu)
	mesh=widget_button(sprop,value='Meshed Surface',font=font,/menu)
	hist=widget_button(mesh,value='Stacked Histogram',font=font)
	horz=widget_button(mesh,value='Horizontal lines',font=font)
	defa=widget_button(mesh,value='Wire Mesh',font=font)

        if proc_out.devsel eq 0 then begin
		shad=widget_button(sprop,value='Shaded Surface',font=font,/menu)
		colo_surf=widget_button(shad,value='Colour',font=font,/menu)
		blue_white=widget_button(colo_surf,value='Blue',font=font)
		red_temp=widget_button(colo_surf,value='Orange',font=font)
		grey=widget_button(colo_surf,value='Grey',font=font)
        endif else begin
		shad       = 0L
		colo_surf  = 0L
		blue_white = 0L
		red_temp   = 0L
		grey	   = 0L
	endelse

	aprop=widget_button(fourth_ggchild,value='Axis Properties',$
	font=font,/menu)
	log_surf=widget_button(aprop,value='Log Plots',font=font,/menu)
	style=widget_button(aprop,value='Axis Style',font=font,/menu)
	axis_choice=widget_button(style,value='Hide Axis',font=font)
	range=widget_button(style,value='Extend axis',font=font)
	xaxis_surf=widget_button(log_surf,value='X-axis',font=font)
	yaxis_surf=widget_button(log_surf,value='Y-axis',font=font)
	zaxis_surf=widget_button(log_surf,value='Z-axis',font=font)	
	zaxis=widget_button(aprop,value='Z-Axis',font=font,/menu)
	low_right=widget_button(zaxis,value='Lower right',font=font)
	low_left=widget_button(zaxis,value='Lower left',font=font)
	upp_right=widget_button(zaxis,value='Upper right',font=font)
	upp_left=widget_button(zaxis,value='Upper left',font=font)
	no_zaxis=widget_button(zaxis,value='Hide axis',font=font)
	bmenu=widget_button(second_child,value=bitmap1,font=font)
	if setting.prtbut eq 1 then begin
		prtplot = widget_button(second_child,value='Print',font=font)
	end else begin
		prtplot = 0L
	end
      
	done=widget_button(second_child,value='Done ',font=font)
        info=widget_label(second_child,value='                                          ',font=font)
		state={	right		:	right,		$
			left		:	left,		$
			achoriz		:	achoriz,	$
			choriz		:	choriz,		$
			zoomin		:	zoomin,		$
			zoomout		:	zoomout,	$
			bmenu		: bmenu,		$
			done		: done,			$
			sprop		: sprop, 		$
			mesh		: mesh,			$
			hist		: hist,			$
			horz		: horz,			$
			defa		: defa,			$
			shad		: shad,			$
			colo_surf	: colo_surf,		$
			blue_white	: blue_white,		$
			red_temp	: red_temp,		$
			grey		: grey,			$
			aprop		: aprop,		$
			log_surf	: log_surf,		$
			xaxis_surf	: xaxis_surf,		$
			yaxis_surf	: yaxis_surf,		$
			zaxis_surf	: zaxis_surf,		$			
			zaxis		: zaxis,		$
			low_right	: low_right,		$
			low_left	: low_left,		$
			upp_right	: upp_right,		$
			upp_left	: upp_left,             $
			axis_choice	: axis_choice,		$
			range		: range,                $
			no_zaxis	: no_zaxis,		$
                        info		: info,			$
			prtplot		: prtplot		}
	widget_control,parent,/realize
	widget_control,surfacedisplay,get_value=cur_win
	
	if ( n_params() eq 5 ) then begin
	 wset,cur_win
	 adas312_draw
	end
	widget_control,widget_info(parent,/child),set_uvalue=state,/no_copy
	return,parent
end
	
	
