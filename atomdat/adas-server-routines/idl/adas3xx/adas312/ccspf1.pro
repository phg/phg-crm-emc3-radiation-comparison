; Copyright (c) 1997, Strathclyde University.
; SCCS info: Module @(#)ccspf1.pro      1.1 Date 07/17/97
;
; CATEGORY:
;       Adas system.
;       
; WRITTEN:
;       Harvey Anderson, University of Strathlcyde, 10-07-97
;             
;
; MODIFIED:
;       1.1     Harvey Anderson
;               First Version
;       1.2     Lorne Horton
;               Bug fix in case where only text file is requested
;
; VERSION:
;       1.1     10-07-97
;       1.2     12-02-03
;-
;-----------------------------------------------------------------------------
PRO CCSPF1,proc_out,setting,bitmap1,font=font,action,pipe

        printf,pipe,proc_out.txtout
        IF proc_out.txtout eq 1 THEN BEGIN
                printf,pipe,proc_out.txtname
        END 
        
        ; wait until fortran has finished writing file

        printf,pipe,proc_out.outbut
        IF proc_out.outbut eq 1 THEN BEGIN
                tval = proc_out.grpsel + 1
                printf,pipe,tval
                readf,pipe,iecount
                readf,pipe,incount
                
       ; read the zdata
                
                for j = 0 , incount-1 do begin
                  for i = 0 , iecount-1 do begin
                      readf,pipe,tzdata
                      setting.zdata(j,i) = tzdata
                  endfor
                endfor
                
       ; read the xdata
       
               for i = 0 , iecount-1 do begin
                     readf,pipe,txdata
                     setting.xdata(i) = txdata
               endfor
       
       ; read the ydata
       
              for j = 0 , incount-1 do begin
                     readf,pipe,tydata
                     setting.ydata(j) = tydata
              endfor

                IF proc_out.scalbut ne 1 AND $
                   proc_out.hrdout ne 1 THEN BEGIN
                   setting.prtbut =0
                   adas312_surface,setting,proc_out,bitmap1,font=font,action
                END
                
                IF proc_out.scalbut ne 1 AND $
                   proc_out.hrdout eq 1 THEN BEGIN
                   setting.prtbut = 1
                   adas312_surface,setting,proc_out,bitmap1,font=font,action
                END
        END ELSE BEGIN
;               printf,pipe,proc_out.outbut
        END
END
                           
                   
                
