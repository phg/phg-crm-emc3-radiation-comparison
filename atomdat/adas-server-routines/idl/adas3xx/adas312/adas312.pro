;
; PROJECT :
;               ADAS
;
; NAME:
;               ADAS312
;
; PURPOSE :
;               Highest level routine of the adas program
;               ADAS312.
;
; EXPLANATION:
;
; USE:
;
; INPUTS:
;
;       ADASREL - A string indicating the ADAS system version, 
;                 e.g ' ADAS RELEASE: ADAS93 V1.1.0'.  The first
;                 character should be a space.
;
;       FORTDIR - A string holding the path to the directory where the
;                 FORTRAN executables are, e.g '/disk/adas/fortran/exec'
;
;       USERROOT - A string holding the path to the root directory of
;                  the user's adas data. e.g '/disk/bowen/adas'
;                  This root directory will be used by adas to construct
;                  other path names.  For example the users default data
;                  for adas506 should be in /disk/bowen/adas/adf20.  In
;                  particular the user's default interface settings will
;                  be stored in the directory USERROOT+'/defaults'.  An
;                  error will occur if the defaults directory does not
;                  exist.
;
;       CENTROOT - Like USERROOT, but this directory points to the
;                  central data area for the system.  User defaults are
;                  not stored on CENTROOT.
;
;       DEVLIST - A string array of hardcopy device names, used for
;                 graphical output. e.g ['Post-Script','HP-PCL','HP-GL']
;                 This array must mirror DEVCODE.  DEVCODE holds the
;                 actual device names used in a SET_PLOT statement.
;
;       DEVCODE - A string array of hardcopy device code names used in
;                 the SET_PLOT statement, e.g ['PS', 'PCL', 'HP']
;                 This array must mirror DEVLIST.
;
;       FONT_LARGE - The name of a larger font e.g 'courier_bold14'
;
;       FONT_SMALL - The name of a smaller font e.g 'courier_bold12'
;
;       EDIT_FONTS - A structure of two fonts used in the table editor
;                    adas_edtab.pro, {font_norm:'',font_input:''}.
;                    The two fonts are used to differentiate between
;                    editable and non-editable parts of the table. You
;                    may have to experiment to get two fonts which give
;                    the same default spacing in the edit widget.
;                    e.g {font_norm:'helvetica_bold14', $
;                         font_input:'helvetica_oblique14'}
;
; OPTIONAL INPUTS:
;       None
;
; OUTPUTS:
;       None - Note this routine should leave all inputs un-modified.
;
; OPTIONAL OUTPUTS:
;       None
;
; KEYWORD PARAMETERS:
;       None
;
; CALLS:
;       FIND_PROCESS    Checks to see if a given process is active.
;       CCSPF0          Pipe comms with FORTRAN CCSPF0 routine.
;       CCISPF          Pipe comms with FORTRAN CCISPF routine.
;       XXDATE          Get date and time from operating system.
;       CCSPF1          Pipe comms with FORTRAN CCSPF1 routine.
;       CCOUTG          Pipe comms with FORTRAN CCOUTG routine.
;
; SIDE EFFECTS:
;       This routine spawns a FORTRAN executable.  Note the pipe 
;       communications routines listed above.  In addition to these
;       pipe communications there is one explicit communication of the
;       date to the FORTRAN code, search for 'printf,pipe' to find it.
;
; CATEGORY:
;       Adas system.
;       
; WRITTEN:
;       Harvey Anderson, University of Strathlcyde, 10-07-97
;             
;
; MODIFIED:
;       1.1     Harvey Anderson
;               First Version
;       1.2     Richard Martin
;               Increased version no. to 1.2.
;       1.3     Richard Martin
;               Increased version no. to 1.3.
;       1.4     Harvey Anderson
;               Introduced the call to the routine adas312_mess.
;               Also added the parameter procval.dlfag to check
;               that the selected data exist.
;               Increased version no to 1.4.
;       1.5     Richard Martin
;               Increased version no to 1.5.
;       1.6     Martin O'Mullane
;               Flawed logic when beam stopping was selected. There
;               is no need to get transition information in this case.
;               Change output default name to adas312_out.pass.     
;               Increase version number to 1.6.
;
; VERSION:
;       1.1     10-07-97
;       1.2     01-11-97
;       1.3     23-06-98
;       1.4     17-03-99
;       1.5     15-10-99
;       1.6     06-02-2003
;-
;-----------------------------------------------------------------------------
pro adas312, adasrel, fortdir, userroot, centroot, devlist, $
             devcode, font_large,font_small, edit_fonts

          ;************************
          ;**** Initialisation ****
          ;************************

  adasprog = ' PROGRAM: ADAS312 V1.7'
  lpend    = 0
  gomenu   = 0
  deffile  = userroot+'/defaults/adas312_defaults.dat'
  device   = ''
  bitfile  = centroot + '/bitmaps'
  
  menufile = bitfile+'/menu.bmp'
  read_X11_bitmap, menufile, menu_bitmap

          ;******************************************
          ;**** Search for user default settings ****
          ;**** If not found create defaults     ****
          ;******************************************

  files = findfile(deffile)
  if files[0] eq deffile then begin
     restore,deffile
     inval.centroot=centroot+'/adf26'
     inval.userroot=userroot+'/adf26'
  end else begin
     inval = {  ROOTPATH : userroot+'/adf26/', $
                FILE     : '',                 $
                CENTROOT : centroot+'/adf26/', $
                USERROOT : userroot+'/adf26/' }
     procval  = {new:-1}
     proc_out = {new:-1}
  end

  IF ( proc_out.new lt 0 ) THEN BEGIN
   
  proc_out = {  new       :  0,                $
                outbut    :  0,                $
                gtit1     :  '',               $
                scalbut   :  0,                $
                xmin      : '0',               $
                xmax      : '0',               $
                ymin      : '0',               $
                ymax      : '0',               $
                zmin      : '0',               $
                zmax      : '0',               $
                hrdout    :  0,                $
                txtout    :  0,                $
                txtname   :  '',               $
                hardname  : '',                $
                grpdef    : 'adas312_out.pass',$
                grpfmess  : '',                $
                grpsel    :  1,                $
                grprmess  : '',                $
                grptmess  : '',                $
                devsel    : 0,                 $
                grselmess : ''                 }
  ENDIF
  
  setting  = {  zdata       : fltarr(25,25),                         $
                ydata       : fltarr(25),                            $
                xdata       : fltarr(25),                            $
                axis        : 1,                                     $
                rotz        : 30,                                    $
                rotx        : 30,                                    $
                xlog        : 0,                                     $
                ylog        : 0,                                     $
                zlog        : 0,                                     $
                zlocation   : 3,                                     $
                shad_colour : 0,                                     $
                hist_surf   : 0,                                     $
                horz_surf   : 0,                                     $
                surf_type   : 1,                                     $
                xmargin     : [10.0,2.0],                            $
                ymargin     : [4.0,2.0],                             $
                x_title     : 'NEUTRAL BEAM ENERGY ( eV amu!E-1!N )',$
                y_title     : 'TARGET DENSITY ( cm!E3!N )',          $
                z_title     : '',                                    $
                main_title  : '',                                    $
                adas_info   : '',                                    $
                file_info   : ' FILE  : ',                           $
                font        : '',                                    $
                prtbut      : 0,                                     $
                cfactor     :1.0                                     }

                    
;               *********************************
;               *                               *
;               *       START FORTRAN CODE      *
;               *                               *
;               *********************************

   spawn, fortdir + '/adas312.out',unit=pipe,/noshell,PID=pid

;         ********************************************
;         *                                          *
;         *   CHECK THAT FORTRAN IS STILL RUNNING    *
;         *                                          *
;         ********************************************

   
   if find_process(pid) eq 0 then goto, labelend

        
   date = xxdate()
   printf, pipe, date[0]


LABELSTART:
                ;**** Check FORTRAN still running ****
  
   if find_process(pid) eq 0 then goto, labelend

               ;************************************************
               ;**** Communicate with ccspf0 in fortran and ****
               ;**** invoke user interface widget for       ****
               ;**** Data file selection                    ****
               ;************************************************

   ccspf0, pipe, inval, dsfull, rep, FONT=font_large

               ;**** If cancel selected then end program ****

   if rep eq 'YES' then goto, LABELEND


               ;**** Check FORTRAN still running ****

   if (find_process(pid) eq 0) then goto, LABELEND
   
   setting.file_info = setting.file_info + dsfull
   ccispf, procval, pipe


LABELSECOND:

   action = adas312_proc(procval, dsfull, bitfile, $
                         font_large=font_large, font_small=font_small)

   if action eq 'MENU' then begin
      idone   = 0
      icancel = 0
      imenu   = 1
      printf, pipe, idone
      printf, pipe, icancel
      printf, pipe, imenu
      goto, LABELEND
   endif
   
   if action eq 'CANCEL' then begin
      idone   = 0
      icancel = 1
      imenu   = 0
      printf, pipe, idone
      printf, pipe, icancel
      printf, pipe, imenu
      goto, LABELSTART
   end
   
   if action eq 'DONE' then begin
      
      idone   = 1
      icancel = 0
      imenu   = 0
      printf, pipe, idone
      printf, pipe, icancel
      printf, pipe, imenu
      
      printf, pipe, procval.option + 1      ; IDL->fortran indexing
      
      dflag = 0L
      case procval.option of
         0 : begin
               setting.z_title = 'EFFECTIVE STOPPING COEFFICIENT ( cm!E3!N s!E-1!N )'
               setting.main_title = ' BEAM STOPPING COEFFICIENT SURFACE PLOT :'
             end
         1 : begin
               setting.z_title = 'EFFECTIVE EMISSION COEFFICIENT ( cm!E3!N s!E-1!N )'
               setting.main_title = ' BEAM EMISSION COEFFICIENT SURFACE PLOT :'
               printf, pipe, procval.n_low
               printf, pipe, procval.n_up
               readf, pipe, dflag
             end
         2 : begin
               setting.z_title = 'BEAM EXCITED STATE POPULATION FRACTION'
               setting.main_title = ' BEAM EXCITED STATE POPULATION SURFACE PLOT :'
               printf, pipe, procval.n_up
               readf, pipe, dflag
             end
      endcase
      
      if procval.option EQ 2 AND dflag EQ 0 then begin
         tmp = popup(message = 'Data for selected transition is unavaliable', $
                     title   = '   ADAS312: INFORMATION    ',                 $
                     buttons = ['Accept'], font = font_large)
          goto, LABELSECOND
      end


LABELTHIRD:

      header = adasrel+adasprog+ ' DATE : '+ date[0] + ' TIME :' + date[1]
      setting.adas_info = header
            
      adas312_out, proc_out, dsfull, devlist, procval, menu_bitmap, $
                   font=font_large, action
      
      if action eq 'Menu' then begin
           idone   = 0
           icancel = 0
           imenu   = 1
           printf, pipe, idone
           printf, pipe, icancel
           printf, pipe, imenu
           goto, LABELEND
      end
      if action eq 'Cancel' then begin
           idone   = 0
           icancel = 1
           imenu   = 0
           printf, pipe, idone
           printf, pipe, icancel
           printf, pipe, imenu
           goto, LABELSECOND
      end
      if action eq 'Done' then begin
           idone   = 1
           icancel = 0
           imenu   = 0
           printf, pipe, idone
           printf, pipe, icancel
           printf, pipe, imenu
           ccspf1, proc_out, setting, menu_bitmap, font=font_large, action, pipe
           if action eq 'Menu' then begin
              idone   = 0
              icancel = 0
              imenu   = 1
              printf, pipe, idone
              printf, pipe, icancel
              printf, pipe, imenu
              goto, LABELEND
           end
           if action eq 'Done' then begin 
              idone   = 1
              icancel = 0
              imenu   = 0
              printf, pipe, idone
              printf, pipe, icancel
              printf, pipe, imenu                             
              goto, LABELTHIRD
           end 
      end  
 
 endif    ; 'Done'                

LABELEND:

; Save user defaults

 save, inval, procval, proc_out, filename=deffile


END
