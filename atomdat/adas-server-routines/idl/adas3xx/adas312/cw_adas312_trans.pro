; Copyright (c) 1997, Strathclyde University.
; SCCS info: Module @(#)$Header: /home/adascvs/idl/adas3xx/adas312/cw_adas312_trans.pro,v 1.2 2004/07/06 12:42:13 whitefor Exp $ Date $Date: 2004/07/06 12:42:13 $
;
; CATEGORY:
;	Adas system.
;	
; WRITTEN:
;       Harvey Anderson, University of Strathlcyde, 10-07-97
;             
;
; MODIFIED:
;	1.1	Harvey Anderson
;		First Version
;	1.2	Harvey Anderson
;		Introduced the use of the function long() to
;		convert string to integer. Also increased the
;		range of valid quantum numbers to [1:25].
; VERSION:
;	1.1	10-07-97
;	1.2	17-03-99
;
;-
;-----------------------------------------------------------------------------
pro rtn_set_val,id,value
  first_child = widget_info(id,/child)
  widget_control, first_child,get_uvalue = status
  if value.bembut eq 1 then begin
  	widget_control,status.lowertxt,set_value=value.lowerpqn
  	widget_control,status.uppertxt,set_value=value.upperpqn
  end 
  if value.bembut eq 0 then begin
  	widget_control,status.lowertxt,set_value=''
  	widget_control,status.uppertxt,set_value=''
  end
 
  status.numval.lowerpqn=value.lowerpqn
  status.numval.upperpqn=value.upperpqn
  
  widget_control,first_child,set_uvalue=status,/no_copy
  
END
FUNCTION rtn_get_val,id
	first_child = widget_info(id,/child)
	widget_control, first_child, get_uvalue = status
	 if status.numval.bembut eq 1 then begin
	   widget_control,status.lowertxt,get_value=lowpqn
	   widget_control,status.uppertxt,get_value=upppqn
	end
	;****** update state ******
	
	status.numval.lowerpqn = lowpqn(0)
	status.numval.upperpqn = upppqn(0)

	error = 0
	
	;******* check for missing value ********
	
	if error eq 0 then begin
		if strtrim(status.numval.lowerpqn) eq '' then error = 1
		if strtrim(status.numval.upperpqn) eq '' then error = 1
		if error eq 1 then begin
		 status.numval.grpmess = '**** Missing Value ****'
		end
	end
	
	;***** check for illegal values ********
	
	if error eq 0 then begin
		if num_chk(status.numval.lowerpqn,/sign) eq 1 then error = 1
		if num_chk(status.numval.upperpqn,/sign) eq 1 then error = 1
		if error eq 1 then begin
		  status.numval.grpmess = '**** Illegal Numeric Value ****'
		end
	end
	
    ;**** check the lower quantum no. is not ge upper quantum no.****
    
    	if error eq 0 then begin
	      
    if  long(status.numval.lowerpqn) ge long(status.numval.upperpqn) then $
	error =1
    	  if error eq 1 then begin
    	    status.numval.grpmess = '**** lower quantum no. must be less than the upper quantum no. ****'
	  end
	end
	
   ;**** check to ensure that qauntum nubers are in the permitted range *****
   
   	if error eq 0 then begin
   	  if long(status.numval.upperpqn) gt 25 then error = 1
   	  if long(status.numval.lowerpqn) lt  1 then error = 1
   	  if error eq 1 then begin
   	  status.numval.grpmess = '**** Insert quantum no. within the permitted range ****'
	  end
	 end

	if error eq 1 then begin
	  widget_control,status.messid,set_value=status.numval.grpmess
	end else begin
	  status.numval.grpmess = '             '
	  widget_control,status.messid,set_value=status.numval.grpmess
	end
return,status
end
FUNCTION rtn_event,event
    base=event.handler
    first_child = widget_info(base,/child)
    widget_control,first_child,get_uvalue = status
    CASE event.id OF
    	status.lowertxt:widget_control,status.uppertxt,/input_focus
    	status.uppertxt:widget_control,status.lowertxt,/input_focus
    else :
    endcase
    widget_control, first_child, set_uvalue=status,/no_copy
    return, 0L
  END     
FUNCTION cw_adas312_trans,parent,value=value,font=font

IF (N_PARAMS() LT 1 ) THEN MESSAGE,'Must specify PARENT for cw_adas312_trans'

	;***** Return to caller on error *****
	
	ON_ERROR, 2
	
IF NOT (KEYWORD_SET(value)) THEN begin

	numval = { lowerpqn : '',$
		   upperpqn : '',$
		   bembut   : 1 ,$
		   grpmess : '' }
end else begin
	numval = { lowerpqn : value.lowerpqn,$
		   upperpqn : value.upperpqn,$
		   bembut   : value.bembut  ,$
		   grpmess : value.grpmess }
end
IF NOT (KEYWORD_SET(font)) THEN font = ''
	main = widget_base(parent,/frame,/column,$
		EVENT_FUNC = 'rtn_event',$
		FUNC_GET_VALUE = 'rtn_get_val',$
		PRO_SET_VALUE = 'rtn_set_val')
	first_child = widget_base(main)
	mainid = widget_base(first_child,/column)
	selectid = widget_base(mainid,/row)
	editid = widget_base(mainid,/row)
	rangeid = widget_base(editid,/column)
	lowid = widget_base(editid,/column)
	uppid = widget_base(editid,/column)
	lbltitle = widget_label(selectid,$
	 value='Beam emission: Select quantum numbers',font=font)
	lblrange = widget_label(rangeid,value='                        Range',font=font)
	lbllower = widget_label(lowid,value='Lower Level',font=font)
	lblupper = widget_label(uppid,value='Upper Level',font=font)
	lbltxt = widget_label(rangeid,font=font,value='Principal quantum no.       N (1 - 25)')
	lowertxt = widget_text(lowid,/editable,xsize=8,font=font)
	uppertxt = widget_text(uppid,/editable,xsize=8,font=font)
	
	;******* error message ******
	
	if strtrim(numval.grpmess) eq '' then begin
	message = $
	  '                                                                  '
	end else begin
		message = numval.grpmess
	end
	messid = widget_label(mainid,value=message,font = font)
	
	;****** set initial values *********
	
	if  numval.bembut eq 1 then begin
		widget_control,lowertxt,set_value=numval.lowerpqn
		widget_control,uppertxt,set_value=numval.upperpqn
	end else begin
	 	widget_control,mainid,sensitive=0
	end
		;****** create new state structure ********
		
	new_status = { 	LOWERTXT        :	lowertxt	,$
		 	UPPERTXT	:	uppertxt	,$
		 	NUMVAL		:	numval		,$
		 	MESSID		:	messid		,$
		 	FONT		:	font		}
		 	
		 ;***** save intial state structure ********
		 
	widget_control,first_child,set_uvalue=new_status,/no_copy
	RETURN,main
end		   
