; Copyright (c) 1997, Strathclyde University.
; SCCS info: Module @(#)$Header: /home/adascvs/idl/adas3xx/adas312/adas312_surface.pro,v 1.1 2004/07/06 10:36:15 whitefor Exp $ Date $Date: 2004/07/06 10:36:15 $
;
; MODIFIED:
;	1.1	Harvey Anderson
;		First Version
;
; VERSION:
;	1.1	10-07-97
;-
;-----------------------------------------------------------------------------
pro adas312_surface_event,event
	common plot312_blk, action ,pname, pdevice
	action = event.action
	case action of
	'Menu':widget_control,event.top,/destroy
	'Done':begin
	       widget_control,event.top,/destroy
	       end
	endcase
end 
pro adas312_surface,setting,proc_out,bitmap1,font=font,act
	common plot312_blk,action , pname ,pdevice
	pname = proc_out.hardname
	pdevice = proc_out.devsel
		base=widget_base(title='ADAS312')
	cwd=cw_adas312_surface(base,state,bitmap1,font=font,setting,proc_out)
	widget_control,base,/realize
	xmanager,'adas312_surface',base
	act = action
end
