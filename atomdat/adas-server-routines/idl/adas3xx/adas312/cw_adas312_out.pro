; Copyright (c) 1997, Strathclyde University.
;
;+
; PROJECT:
;       ADAS
;
; NAME:
;       CW_ADAS312_OUT()
;
; PURPOSE:
;       Generates a widget used for graphical output selection.
;
; EXPLANATION:
;       This widget consists of a titled activation button, a text
;       widget for entering a graph title, a scaling ranges input
;       widget cw_adas_ranges.pro, an output file widget
;       cw_adas_outfile.pro, a single selection widget cw_single_sel.pro
;       with a list of available hardcopy devices.  A general error
;       message is also included in the widget.
;
;       cw_adas_ranges.pro and cw_adas_outfile.pro handle their own
;       events and perform their own error checking.  The general
;       activation button desensitises all other component widgets when
;       it is set off.  The only other events processed by this widget
;       are from the two single selection lists for graphs and devices.
;
;       Many of the elements of this widget are optional and controlled
;       by the caller.
;
;       This widget generates an event when the activation button is
;       pressed.  The event structure is;
;               {ID:0L, TOP:0L, HANDLER:0L, OUTBUT:0}
;       where OUTBUT is 0 if the activation button is off and 1 if
;       the activaion button is on.
;
; USE:
;       An example of use is given below.  Also see cw_adas205_out.pro
;       for another example.
;
;       grplist = ['Graph 1','Graph2','Graph3']
;       devlist = ['Device 1','Device 2','Device 3']
;       value = {$
;               OUTBUT:0 , GTIT1:'', $
;               SCALBUT:0, $
;               XMIN:'', XMAX:'', $
;               YMIN:'', YMAX:'', $
;               HRDOUT:0, HARDNAME:'', $
;               GRPDEF:'', GRPFMESS:'', $
;               GRPSEL:-1, GRPRMESS:'', $
;               DEVSEL:-1, GRSELMESS:'' }
;
;       base=widget_base(/column)
;       grpid=cw_adas_gr_sel(base,value=value,grplist=grplist,devlist=devlist)
;       rc=widget_button(base,value='Done')
;       widget_control,base,/realize
;       rc=widget_event()
;       widget_control,grpid,get_value=value
;
;       if strtrim(value.grselmess) eq '' and $
;          strtrim(value.grpfmess) eq '' and $
;          strtrim(value.grprmess) eq '' then begin
;
;         if value.outbut eq 1 then begin
;           print,'Graph selected: ',grplist(value.grpsel)
;
;           if value.hrdout eq 1 then begin
;             print,'Device selected: ',devlist(value.devsel)
;             print,'Hard copy file: ',value.hardname
;           end
;
;           if value.scalbut eq 1 then begin
;             print,'Scaling ranges:'
;             print,'XMIN: ',value.xmin,'  XMAX: ',value.xmax
;             print,'YMIN: ',value.ymin,'  YMAX: ',value.ymax
;           end
;         end else begin
;           print,'No graphical output requested.'
;         end
;
;       end else begin
;         print,'Graphical selection error;'
;         print,value.grselmess,value.grpfmess,value.grprmess
;       end
;
; INPUTS:
;       PARENT   - The ID of the parent widget.
;
; OPTIONAL INPUTS:
;       None.
;
; OUTPUTS:
;       The return value of this function is the ID of the compound
;       widget which is of type LONG integer.
;
; OPTIONAL OUTPUTS:
;       None
;
; KEYWORD PARAMETERS:
;       SIGN      - Integer: < 0  => Graph ranges must be negative
;                            0    => Graph ranges can be positive or negative
;                            > 0  => Graph ranges must be positive
;       OUTPUT    - A string; Label to use for activate button.  Default
;                   'Graphical Output'.
;
;       GRPLIST   - A string array; A list of graphs one array element per
;                   graph.  If the list is empty i.e grplist='' then the
;                   selection list widget will not appear.  Default value=''.
;
;       LISTTITLE - A string; title for the graph selection list.
;                   Default 'Select Graph'.
;
;       DEVLIST   - A string array; A list of devices one array element per
;                   device.  If the list is empty i.e devlist='' then the
;                   selection list widget will not appear.  Default value=''.
;
;       VALUE     - A structure which defines the value of this widget.
;                   The structure is made up of a number of parts each
;                   pertaining to one of the widgets in this compound
;                   widget.  Default value;
;
;                     { OUTBUT:0 , GTIT1:'', $
;                       SCALBUT:0, $
;                       XMIN:'', XMAX:'', $
;                       YMIN:'', YMAX:'', $
;                       HRDOUT:0, HARDNAME:'', $
;                       GRPDEF:'', GRPFMESS:'', $
;                       GRPSEL:-1, GRPRMESS:'', $
;                       DEVSEL:-1, GRSELMESS:'' }
;
;                   Note these do not appear in the same order as in the
;                   value structure.
;                   General;
;                       OUTBUT     Integer; Activation button 1 on, 0 off
;                       GTIT1      String; Graph title
;                       GRSELMESS  String; General error message
;
;                   For CW_ADAS_RANGES;
;                       SCALBUT    Integer; Scaling activation 1 on, 0 off,
;                                  -1 no widget.
;                       XMIN       String; x-axis minimum, string of number
;                       XMAX       String; x-axis maximum, string of number
;                       YMIN       String; y-axis minimum, string of number
;                       YMAX       String; y-axis maximum, string of number
;                       GRPRMESS   String; Scaling ranges error message
;
;                   For CW_ADAS_OUTFILE hard copy output file;
;                       HRDOUT     Integer; Hard copy activ' 1 on, 0 off,
;                                  -1 no widget.
;                       HARDNAME   String; Hard copy output file name
;                       GRPDEF     String; Default output file name
;                       GRPFMESS   String; File name error message
;                       (Replace and append values are fixed)
;
;                   For CW_SINGLE_SEL graph selection;
;                       DEVSEL     Integer; index of selected device in DEVLIST
;                                  -1 indicates no selection made.
;
;       FONT      - String; Name of a font to use for all text in this widget.
;
;       UVALUE    - A user value for this widget.
;
; CALLS:
;       CW_ADAS_RANGES  Specify explict x and y axis ranges.
;       CW_ADAS_OUTFILE Specify output file name.
;       CW_SINGLE_SEL   A single selection list widget.
;       See side effects for other related widget management routines.
;
; SIDE EFFECTS:
;       Three other routines are included which are used to manage the
;       widget;
;
;       out312_SET_VAL
;       out312_GET_VAL()
;       out312_EVENT()
;
; CATEGORY:
;       Compound Widget
;
; WRITTEN:
;       Andrew Bowen, Tessella Support Services plc, 6-May-1995
;
; MODIFIED:
;       1      Harvey Anderson
;
;       1.1    Richard Martin
;               - Put under SCCS control.
;       1.2    Martin O'Mullane
;               - The set_val, get_val and event routines now have
;                 unique names. There was a naming conflict with 
;                 cw_adas_gr_sel but this routine now conforms to
;                 ADAS naming standards.
;               - grselval is no longer a named structure to avoid
;                 conflict when running 312 subsequebt to other ADAS
;                 programs where this struccture is anonymous.
;
; VERSION:
;       1       10-07-97
;       1.1     17-07-97
;       1.2     13-05-2005
;
;-
;-----------------------------------------------------------------------------

PRO out312_set_val, id, value


                ;**** Return to caller on error ****
  ON_ERROR, 2

               ;***************************************
               ;****     Retrieve the state        ****
               ;**** Get first_child widget id     ****
               ;**** because state is stored there ****
               ;***************************************

  first_child = widget_info(id, /child)
  widget_control, first_child, get_uvalue = state

                ;**** Value for the file widget ****
  outfval = { OUTBUT:value.hrdout, APPBUT:-1, REPBUT:0, $
              FILENAME:value.hardname, DEFNAME:value.grpdef, $
              MESSAGE:value.grpfmess }

                ;**** Value for the ranges widget ****
  if state.scbasid gt 0 then begin
    rngval = {  SCALBUT:value.scalbut, $
                XMIN:value.xmin, XMAX:value.xmax, $
                YMIN:value.ymin, YMAX:value.ymax, $
                ZMIN:value.zmin, ZMAX:value.zmax, $
                GRPRMESS:value.grprmess, SIGN:value.sign }
  end
                ;**** Value for the text widget ****
   outtval = { OUTBUT:os312.txtout, APPBUT:-1, REPBUT:0, $
               FILENAME:os312.txtname, DEFNAME:os312.grpdef,$
               MESSAGE:os312.grptmess }

                ;**** Sensitise or desensitise with output button setting ****

  if value.outbut eq 0 then begin

    widget_control,state.actid,set_button=0

                ;**** desensitise graph title ****
    widget_control,state.tibasid,sensitive=0

                ;**** desensitise graph selection list ****
    if state.listid gt 0 then widget_control,state.lsbasid,sensitive=0

                ;**** desensitise device selection list ****
;    if state.devbasid gt 0 then widget_control,state.devbasid,sensitive=0

                ;**** desensitise filename and hide settings ****
    widget_control,state.devbasid,sensitive=0
    widget_control,state.fbaseid,sensitive=0
    outfval.outbut = 0
    widget_control,state.fileid,set_value=outfval
    widget_control,state.paperid,set_value=outtval

                ;**** desensitise ranges and hide settings ****
    if state.scbasid gt 0 then begin
      widget_control,state.scbasid,sensitive=0
      rngval.scalbut = 0
      widget_control,state.rangeid,set_value=rngval
    end

  end else begin

    widget_control,state.actid,set_button=1

    widget_control,state.tibasid,/sensitive

    if state.listid gt 0 then widget_control,state.lsbasid,/sensitive

   ; if state.devbasid gt 0 then widget_control,state.devbasid,/sensitive
    widget_control,state.devbasid,/sensitive
    widget_control,state.fbaseid,/sensitive
    widget_control,state.fileid,set_value=outfval
    widget_control,state.paperid,set_value=outtval

    if state.scbasid gt 0 then begin
      widget_control,state.scbasid,/sensitive
      widget_control,state.rangeid,set_value=rngval
    end

  end

                ;**** Update value of graph selection ****
  if state.listid gt 0 then widget_control,state.listid,set_value=value.grpsel

                ;**** Update value of device selection ****
  if state.devid gt 0 then widget_control,state.devid,set_value=value.devsel

                ;**** Update message ****
  if strtrim(value.grselmess) ne '' then begin
    message = value.grselmess
  end else begin
    message = ' '
  end
  widget_control,state.messid,set_value=message

                ;**** Copy the new value to state structure ****
  state.os312.outbut = value.outbut
  state.os312.gtit1 = value.gtit1
  state.os312.scalbut = value.scalbut
  state.os312.xmin = value.xmin
  state.os312.xmax = value.xmax
  state.os312.ymin = value.ymin
  state.os312.ymax = value.ymax
  state.os312.zmin = value.zmin
  state.os312.zmax = value.zmax
  state.os312.hrdout = value.hrdout
  state.os312.hardname = value.hardname
  state.os312.txtout = value.txtout
  state.os312.txtname = value.txtname
  state.os312.grpdef = value.grpdef
  state.os312.grpfmess = value.grpfmess
  state.os312.grptmess = value.grptmess
  state.os312.grpsel = value.grpsel
  state.os312.grprmess = value.grprmess
  state.os312.devsel = value.devsel
  state.os312.grselmess = value.grselmess
  state.os312.new = value.new


                ;**** Save the new state ****
  widget_control, first_child, set_uvalue=state, /no_copy

END

;-----------------------------------------------------------------------------


FUNCTION out312_get_val, id


                ;**** Return to caller on error ****
  ON_ERROR, 2

               ;***************************************
               ;****     Retrieve the state        ****
               ;**** Get first_child widget id     ****
               ;**** because state is stored there ****
               ;***************************************

  first_child = widget_info(id, /child)
  widget_control, first_child, get_uvalue = state

                ;**** Get the graph title ****
                ;*** truncate to 30 characters ***
  widget_control,state.titleid,get_value=gtit1
  if (strlen(gtit1(0)) gt 30 ) then begin
        gtit1(0) = strmid(gtit1(0),0,30)
        message = 'Warning : Title truncated to 30 characters'
        widget_control,state.messid,set_value=message
        wait, 1.0
        message = ' '
  endif
  state.os312.gtit1 = gtit1(0)

                ;**** Get the latest information ****
  widget_control,state.fileid,get_value=outfval
  state.os312.hardname = outfval.filename
  state.os312.grpfmess = outfval.message
  widget_control,state.paperid,get_value=outtval
  state.os312.txtname = outtval.filename
  state.os312.grptmess = outtval.message
  state.os312.txtout = outtval.outbut
  if state.os312.outbut eq 1 then state.os312.hrdout = outfval.outbut
  if state.os312.outbut eq 1 then state.os312.txtout = outtval.outbut

                ;**** Get scaling ranges and copy to value ****
  if state.scbasid gt 0 then begin
    widget_control,state.rangeid,get_value=rngval
    state.os312.xmin = rngval.xmin
    state.os312.xmax = rngval.xmax
    state.os312.ymin = rngval.ymin
    state.os312.ymax = rngval.ymax
    state.os312.zmin = rngval.zmin
    state.os312.zmax = rngval.zmax
    state.os312.grprmess = rngval.grprmess
    if state.os312.outbut eq 1 then state.os312.scalbut = rngval.scalbut
  end

                ;**** Clear any error message ****
  if state.os312.hrdout ge 0 then begin
    state.os312.grselmess = ''
    widget_control,state.messid,set_value=' '
  end

  if state.os312.outbut eq 1 then begin

                ;**** Check that a graph has been selected if appropriate ****
    if state.listid gt 0L then begin
      if state.os312.grpsel lt 0 then begin
        state.os312.grselmess = '**** You must select a graph and a'$
          +' temperature ****'
        widget_control,state.messid,set_value=state.os312.grselmess
      end
    end

                ;**** Check that a device has been selected if appropriate ****
    if state.os312.hrdout eq 1 and state.devid gt 0L then begin
      if state.os312.devsel lt 0 then begin
        state.os312.grselmess = '**** You must select a device ****'
        widget_control,state.messid,set_value=state.os312.grselmess
      end
    end

  endif

  RETURN, state.os312

END

;-----------------------------------------------------------------------------

FUNCTION out312_event, event


                ;**** Base ID of compound widget ****
  base=event.handler

               ;***************************************
               ;****     Retrieve the state        ****
               ;**** Get first_child widget id     ****
               ;**** because state is stored there ****
               ;***************************************

  first_child = widget_info(base, /child)
  widget_control, first_child, get_uvalue = state

  new_event = 0L
                ;************************
                ;**** Process Events ****
                ;************************
  CASE event.id OF

    state.actid: begin
                        ;**** Sensitise and desensitise with toggle ****
                        ;**** of output button.                     ****
        if state.os312.outbut eq 1 then begin

          state.os312.outbut = 0
        ;  new_event = {ID:base, TOP:event.top, HANDLER:0L, OUTBUT:0}

                        ;**** Desensitise graph title ****
          widget_control,state.tibasid,sensitive=0

                        ;**** Desensitise graph selection list ****
          if state.listid gt 0 then $
                                widget_control,state.lsbasid,sensitive=0

                        ;**** Desensitise device selection list ****
          if state.devbasid gt 0 then widget_control,state.devbasid,sensitive=0

                        ;**** Desensitise filename and hide settings ****
          outfval = {   OUTBUT:0, APPBUT:-1, REPBUT:0, $
                        FILENAME:state.os312.hardname, $
                        DEFNAME:state.os312.grpdef, $
                        MESSAGE:'' }

          widget_control,state.fbaseid,sensitive=0
          widget_control,state.fileid,set_value=outfval

                        ;**** Desensitise scaling and hide settings ****
          if state.scbasid gt 0 then begin
          rngval = {    SCALBUT:0, $
                        XMIN:state.os312.xmin, XMAX:state.os312.xmax, $
                        YMIN:state.os312.ymin, YMAX:state.os312.ymax, $
                        ZMIN:state.os312.zmin, ZMAX:state.os312.zmax, $
                        GRPRMESS:'' , SIGN:state.os312.sign}
            widget_control,state.scbasid,sensitive=0
            widget_control,state.rangeid,set_value=rngval
          end

        end else begin

          state.os312.outbut = 1
;         new_event = {ID:base, TOP:event.top, HANDLER:0L, OUTBUT:1}


                        ;**** Sensitise graph title ****
          widget_control,state.tibasid,/sensitive

                        ;**** Sensitise selection list ****
          if state.listid gt 0 then widget_control,state.lsbasid,/sensitive

                        ;**** Sensitise device selection list ****
          if state.devbasid gt 0 then widget_control,state.devbasid,/sensitive

                        ;**** Sensitise filename and restore settings ****
          widget_control,state.fbaseid,/sensitive
          widget_control,state.fileid,get_value=outfval
          outfval.outbut = state.os312.hrdout
          widget_control,state.fileid,set_value=outfval

                        ;**** Sensitise scaling and restore settings ****
          if state.scbasid gt 0 then begin
            widget_control,state.scbasid,/sensitive
            widget_control,state.rangeid,get_value=rngval
            rngval.scalbut = state.os312.scalbut
            widget_control,state.rangeid,set_value=rngval
          end

        end
    end

    state.listid: state.os312.grpsel = event.index

    state.devid: state.os312.devsel = event.index

    state.doneid: begin
                        ;**********************************;
                        ;  First Check for error messages  ;
                        ;**********************************

                widget_control,state.rangeid,get_value=rngval
                widget_control,state.paperid,get_value=outtval
                widget_control,state.fileid,get_value=outfval
                if strtrim(rngval.grprmess) ne '' or $
                   strtrim(outtval.message) ne '' or $
                   strtrim(outfval.message) ne '' then begin
                        new_event = 0L
                end else begin
                        new_event = { ID:0L, TOP:event.top,$
                        HANDLER:0L,ACTION:'Done'}
                end

                  end
    state.cancelid: begin
                        new_event = { ID:0L, TOP:event.top,$
                        HANDLER:0L,ACTION:'Cancel'}
                    end

    state.outid : begin

                        new_event = { ID:0L, TOP:event.top,$
                        HANDLER:0L,ACTION:'Menu'}
                  end

    ELSE:

  ENDCASE

                ;**** Save the new state structure ****
    widget_control, first_child, set_uvalue=state, /no_copy

  RETURN, new_event
END

;-----------------------------------------------------------------------------

FUNCTION cw_adas312_out, parent, dsfull, SIGN=sign, OUTPUT=output, $
            GRPLIST=grplist, LISTTITLE=listtitle, DEVLIST=devlist, $
            VALUE=value, FONT=font, UVALUE=uvalue,bitmap1



  IF (N_PARAMS() LT 1) THEN MESSAGE, 'Must specify PARENT for cw_adas312_out'

                ;**** Set defaults for keywords ****
  IF NOT (KEYWORD_SET(sign)) THEN sign = 0
  IF NOT (KEYWORD_SET(output)) THEN output = 'Graphical Output'
  IF NOT (KEYWORD_SET(grplist)) THEN grplist = ''
  IF NOT (KEYWORD_SET(listtitle)) THEN listtitle = 'Select Graph'
  IF NOT (KEYWORD_SET(devlist)) THEN devlist = ''
  IF NOT (KEYWORD_SET(value)) THEN begin
          os312 = {out312_set,                     $  
                        new:0,                  $
                        OUTBUT:0 , GTIT1:'',    $
                        SCALBUT:0,              $
                        XMIN:'', XMAX:'',       $
                        YMIN:'', YMAX:'',       $
                        ZMIN:'', ZMAX:'',       $
                        SIGN:sign,              $
                        HRDOUT:0, HARDNAME:'',  $
                        TXTOUT:0, TXTNAME:'',   $
                        GRPDEF:'', GRPFMESS:'', $
                        GRPSEL:-1, GRPRMESS:'', $
                        DEVSEL:-1, GRSELMESS:'',$
                        GRPTMESS:''}
        end else begin
          os312 = {out312_set,                                            $ 
                        new:value.new,                                 $
                        OUTBUT:value.outbut, GTIT1:value.gtit1,        $
                        SCALBUT:value.scalbut,                         $
                        XMIN:value.xmin, XMAX:value.xmax,              $
                        YMIN:value.ymin, YMAX:value.ymax,              $
                        ZMIN:value.zmin, ZMAX:value.zmax,              $
                        SIGN:sign,                                     $
                        HRDOUT:value.hrdout, HARDNAME:value.hardname,  $
                        TXTOUT:value.txtout, TXTNAME:value.txtname,    $
                        GRPDEF:value.grpdef, GRPFMESS:value.grpfmess,  $
                        GRPSEL:value.grpsel, GRPRMESS:value.grprmess,  $
                        DEVSEL:value.devsel, GRSELMESS:value.grselmess,$
                        GRPTMESS:value.grptmess}
        end
  IF NOT (KEYWORD_SET(font)) THEN font = ''
  IF NOT (KEYWORD_SET(uvalue))  THEN uvalue = 0

                ;**** Create the main base for the widget ****
  main = WIDGET_BASE(parent, UVALUE = uvalue, $
                EVENT_FUNC = "out312_event", $
                FUNC_GET_VALUE = "out312_get_val", $
                PRO_SET_VALUE = "out312_set_val", $
                /ROW)

                ;******************************************************
                ;**** Create a dummy widget just to hold value of *****
                ;**** "state" variable so as not to get confused  *****
                ;**** with any other values. Adopt IDL practice   *****
                ;**** of using first child widget                 *****
                ;******************************************************

  first_child = widget_base(main,/column)

  cwid = widget_base(first_child,/column)

                ;*** Add input filename and browse button ***
  rc = cw_adas_dsbr(cwid, dsfull, font=font)

                ;**** Left hand base ****
  lfid = widget_base(cwid,/column,/frame)

                ;**** Right hand base ****
  rgid = widget_base(cwid,/column)

                ;*****bottom base ******
  ctid = widget_base(cwid,/column,/frame)

                ;**** Create button to activate graphing ****
  base = widget_base(lfid,/column,/nonexclusive)
  actid = widget_button(base,value=output,font=font)

                ;**** Graph title ****
  tibasid = widget_base(lfid,/row)
  rc = widget_label(tibasid,value='Graph Title',font=font)
  titleid = widget_text(tibasid,value=os312.gtit1, $
                        /editable,xsize=30,font=font)

                ;**** Scaling and ranges widget ****
  if os312.scalbut ge 0 then begin
    tempid = widget_base(lfid,/row)
    scbasid = widget_base(tempid,/frame)
    rngval = {  SCALBUT:os312.scalbut,             $
                XMIN:os312.xmin, XMAX:os312.xmax,     $
                YMIN:os312.ymin, YMAX:os312.ymax,     $
                ZMIN:os312.zmin, ZMAX:os312.zmax,     $
                GRPRMESS:os312.grprmess , SIGN:sign}
    rangeid = cw_adas_xyz_ranges(scbasid, SIGN=sign, VALUE=rngval, FONT=font)
  end else begin
    scbasid = 0L
  end

                ;**** Output file name widget ****
  if os312.hrdout ge 0 then begin
    outfval = { OUTBUT:os312.hrdout, APPBUT:-1, REPBUT:0, $
                FILENAME:os312.hardname, DEFNAME:os312.grpdef, $
                MESSAGE:os312.grpfmess }

    fbaseid = widget_base(lfid,/row,/frame)
    fileid = cw_adas_outfile(fbaseid, OUTPUT='Enable Hard Copy', $
                              VALUE=outfval, FONT=font)
  end else begin
    fbaseid = 0L
    fileid = 0L
  end
        ;******** blank label used to help pos312ition select widget ****
     temp = widget_label(fbaseid,value='        ')

                ;**** Add selection list if required ****
  if strtrim(grplist(0)) ne '' then begin
     lsbasid = widget_base(tempid,/frame)
    listid = cw_single_sel(lsbasid,grplist,title=listtitle, $
                                value=os312.grpsel, font=font)
  end else begin
    os312.grpsel = -1
    lsbasid = 0L
    listid = 0L
  end

                ;**** Add hardcopy device selection list ****
  if os312.hrdout ge 0 and strtrim(devlist(0)) ne '' then begin
    devbasid = widget_base(lfid,/row)
    devid = cw_single_sel(fbaseid,devlist,title='Select Device', $
                                value=os312.devsel, font=font)
  end else begin
    os312.devsel = -1
    devbasid = 0L
    devid = 0L
  end
                  ;**** Widget for text output ****

    outtval = { OUTBUT:os312.txtout, APPBUT:-1, REPBUT:0,            $
                FILENAME:os312.txtname, DEFNAME:os312.grpdef,     $
                MESSAGE:os312.grptmess }
    base = widget_base(ctid,/column)
    paperid = cw_adas_outfile(base, OUTPUT='Passing File      ',                $
                              VALUE=outtval, FONT=font)

                ;**** Add message widget ****
  if strtrim(os312.grselmess) ne '' then begin
    message = os312.grselmess
  end else begin
    message = ' '
  end
  messid = widget_label(lfid,value=message,font=font)

                ;**** Set initial state according to value ****
  if os312.outbut eq 1 then begin

    widget_control,actid,set_button=1

  end else begin

    widget_control,tibasid,sensitive=0

    if scbasid gt 0 then begin
      widget_control,scbasid,sensitive=0
      rngval.scalbut = 0
      widget_control,rangeid,set_value=rngval
    end
    widget_control,devbasid,sensitive=0
    widget_control,fbaseid,sensitive=0
    outfval.outbut = 0
    widget_control,fileid,set_value=outfval

    if listid gt 0 then widget_control,lsbasid,sensitive=0

  ;  if devbasid gt 0 then widget_control,devbasid,sensitive=0

  end
        base1 = widget_base(cwid, /row)

        outid = widget_button(base1, value=bitmap1)             ;menu button
        cancelid = widget_button(base1, value='Cancel',font=font)
        doneid = widget_button(base1, value='Done', font=font)

                ;**** Create state structure ****
  new_state = { ACTID:actid, $
                TIBASID:tibasid, TITLEID:titleid, $
                SCBASID:scbasid, RANGEID:rangeid, $
                FBASEID:fbaseid, FILEID:fileid, $
                LSBASID:lsbasid, LISTID:listid, $
                DEVBASID:devbasid, DEVID:devid, MESSID:messid, $
                os312:os312, FONT:font ,PAPERID:paperid,$
                CANCELID:cancelid, OUTID:outid, DONEID:doneid}

                ;**** Save initial state structure ****
  widget_control, first_child, set_uvalue=new_state,/no_copy

  RETURN, main

END
