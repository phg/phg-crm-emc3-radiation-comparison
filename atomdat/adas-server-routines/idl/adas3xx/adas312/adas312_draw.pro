; Copyright (c) 1997, Strathclyde University.
; SCCS info: Module @(#)$Header: /home/adascvs/idl/adas3xx/adas312/adas312_draw.pro,v 1.1 2004/07/06 10:36:05 whitefor Exp $ Date $Date: 2004/07/06 10:36:05 $
;
;	PROJECT : ADAS
;
;	NAME	: ADAS312_DRAW
;
;	PURPOSE : To draw a surface plot or shaded surface. Also included
;		  are additional features to allow the user to customise
;		  plot.
;
;	USE	: This routine in specific to ADAS312 and is employed to
;		  draw a surface or shaded plot depending upon the
;		  response of the user.
;
;	INPUTS	:
;
;		ROTZ		: Specifies the counterclockwise rotation
;				  about the z axis.
;		ROTX		: Specifies the angle of rotation about
;				  the x axis.
;		SURF_TYPE	: General variable which is used as a switch.
;				  if surf_type is equal to 1 then a surface
;				  plot is displayed.If surf_type is equal to 0
;				  then a shaded surface plot is displayed.
;		AXIS		: Variable used to determine the style of the
;				  x,y and z axes. If axis equal 1 then the axes
;				  is plotted exactly according to the data
;				  representing the surface. If axis equal 2 then
;				  the axes is extended beyond the range of the
;				  data representing the surface. If axis equal 4
;				  then all of the axes are suppressed.
;		HIST_SURF	: Variable used to control the type of surface which
;				  is plotted. If hist_surf is equal to 1 then a
;				  stacked histogram plot is displayed.
;		HORZ_SURF	: Variable used to control the type of surface which
;				  is plotted. If horz_surf is equal to 1 then meshed
;				  surface is drawn in which ONLY the lines across
;				  the surface perpendicular to the line of sight
;				  are displayed.
;		SHAD_COLOUR	: General variable used to determine the colour used
;				  for a shaded surface. The actual value assigned
;				  to shad_colour directly references a particular colour 
;				  in the IDL colour table.
;		XLOG		: Variable used as a switch to control whether the xaxis
;				  has a logarithmic scale. If xlog is equal to 1 then
;			          the xaxis is displayed with a logarithmic scale
;		YLOG		: Variable used as a switch to control whether the yaxis
;				  has a logarithmic scale. If ylog is equal to 1 then
;				  the yaxis is displayed with a logarithmic scale
;		ZLOG		: Variable used as a switch to control whether the zaxis
;				  has a logarithmic scale. If zlog is equal to 1 then
;				  the zaxis is displayed with a logarithmic scale
;		ZLOCATION	: Variable used to specify the placement of the z axis.
;				  The position of the z axis is governed by the value
;				  of zlocation as follows:
;								1-lower right
;								2-lower left
;								3-upper left
;								4-upper right
;
;		XMARGIN		: Variable containing data which specifies the xmargin.
;		YMARGIN		: Variable containing data which specifies the ymargin.
;		ZDATA		: A two dimensional array Z(x,y).
;		YDATA		: Array containing the y axis data.
;		XDATA		: Array containing the x axis data.
;		Z_TITLE		: String containing the zaxis title.
;		Y_TITLE		: String containing the y axis title.
;		X_TITLE		: String containing the x axis title.
;		MAIN_TITLE	: String containing the main title of the plot.
;		ADAS_INFO	: String containing the standard ADAS information i.e
;				  ADAS version,ADAS program, time date.
;		FILE_INFO	: Input file .
;
;	OUTPUT	: Draws a surface plot, particular style dependent on
;		  the users responses.
;
;   SIDE EFFECTS: Draws plot in current window which is usually set
;		      with the IDL command WSET . 	
;
;	CALLS	: Calls the IDL routine LOADCT to load a colour from the IDL
;		  colour table.	
;
;	CONTACT : HARVEY ANDERSON
;		  UNIVERSITY OF STRATHCLYDE
;		  ANDERSON@PHYS.STRATH.AC.UK
;
;	DATE	: 21/6/97
;
;	MODIFIED:
;		  1.1	HARVEY ANDERSON
;			First version
;	VERSION	:
;		  1.1	21/6/97
;
;----------------------------------------------------------------------
;
pro adas312_draw
	common	draw_blk,rotz,rotx,surf_type,axis,hist_surf,$
	        horz_surf,shad_colour,xlog,ylog,zlog,zlocation ,$
	        xmargin,ymargin,zdata,ydata,xdata,x_title ,$
	        y_title,z_title,main_title,adas_info,file_info,$
	        pname,pdevice,stitle,cfactor

		;******************************
		;*** Define Plotting Region ***
		;******************************
		
!p.region=[0.05,0.0,0.95,0.85]

		;******************************
		;****** Plot the Surface ******
		;******************************

if  xmargin(1) lt 1.0 then begin
	tmp=size(zdata)
	xsize=fix(tmp(1)*cfactor)
	ysize=fix(tmp(2)*cfactor)
	xdatatmp=congrid(xdata,xsize,ysize)
	ydatatmp=congrid(ydata,xsize,ysize)
	zdatatmp=congrid(zdata,xsize,ysize)
endif else begin
	xdatatmp=xdata
	ydatatmp=ydata	
	zdatatmp=zdata
endelse
	 
if ( surf_type eq 1 ) then begin
		  surface,zdatatmp,xdatatmp,ydatatmp 	$
			 ,xstyle=axis	  	$
			 ,ystyle=axis		$
			 ,zstyle=axis		$
			 ,az=rotz		$
			 ,ax=rotx		$
			 ,xmargin=xmargin	$
			 ,ymargin=ymargin	$
			 ,zaxis=zlocation	$
			 ,xtype=xlog		$
			 ,ytype=ylog		$
			 ,ztype=zlog		$			 			 
			 ,charsize=1.6		$
			 ,xtitle=x_title	$
			 ,ytitle=y_title	$
			 ,ztitle=z_title	$
			 ,horizontal=horz_surf	$
			 ,lego=hist_surf
end			  
	
if ( surf_type eq 0 ) then begin
	       loadct,shad_colour,/silent
	       shade_surf,zdatatmp,xdatatmp,ydatatmp  $
			 ,xstyle=axis	  	$
			 ,ystyle=axis		$
			 ,zstyle=axis		$
			 ,az=rotz		$
			 ,ax=rotx		$
			 ,xmargin=xmargin	$
			 ,ymargin=ymargin	$
	   		 ,zaxis=zlocation	$
	   		 ,charsize=1.6		$
			 ,xtitle=x_title	$
			 ,ytitle=y_title	$
			 ,ztitle=z_title	$
			 ,xtype=xlog		$
			 ,ytype=ylog		$
			 ,ztype=zlog				
end

		;******************************
		;**** Write out the Titles ****
		;******************************
		
	xyouts,0.02,0.95,string(main_title + stitle ),charsize=1.0,/normal
	xyouts,0.02,0.92,string(adas_info),charsize=0.9,/normal
	xyouts,0.02,0.89,string(file_info),charsize=0.9,/normal
end	
