;+
; PROJECT:
;       ADAS 
;
; NAME:
;       ADAS312_PROC
;
; PURPOSE:
;       IDL ADAS user interface, processing options.
;
; EXPLANATION:
;       This routine creates and manages a pop-up window which allows
;       the user to select options and input data to control ADAS312
;       processing.
;
; NOTES:
;       This routine replaces a number of older adas312 routines, 
;               adas312_in.pro
;               cw_adas312_dis.pro
;               cw_adas312_in.pro
;               cw_adas312_trans.pro
;
; USE:
;       This routine is ADAS312 specific.
;
; INPUTS:
;       VALUE   - A structure which determines the initial settings of
;                 the processing options widget.
;
;
;       BITFILE - String; the path to the directory containing bitmaps
;                 for the 'escape to series menu' button.
;
;
; OPTIONAL INPUTS:
;       None
;
; OUTPUTS:
;       VALUE   - On output the structure records the final settings of
;                 the processing selections widget if the user pressed the
;                 'Done' button, otherwise it is not changed from input.
;
;       REP     - String; Either 'CANCEL' or 'MENU' or 'CONTINUE' depending
;                 on how the user terminated the processing options
;                 window.
;
; OPTIONAL OUTPUTS:
;       None
;
; KEYWORD PARAMETERS:
;       FONT_LARGE - The name of a larger font.
;
;       FONT_SMALL - The name of a smaller font.
;
;
; CALLS:
;       DYNLABEL           Sets dynamic_resize keyword for label widgets.
;       ADAS312_PROC_MENU  Reacts to the menu button
;       ADAS312_PROC_EVENT Handles any events.
;       XMAMAGER
;
; SIDE EFFECTS:
;
;
; CATEGORY:
;       Widgets
;
; WRITTEN:
;       Martin O'Mullane
;
; MODIFIED:
;       1.1     Martin O'Mullane
;                - First version.
;
; VERSION:
;       1.1     09-12-2004
;
;-
;-----------------------------------------------------------------------------


PRO ADAS312_PROC_MENU, event

; The 'normal' get_value from a widget_button does not work with a
; bitmap button. Hence this specialised event handler.

 ; Get the info structure out of the user value of the top-level base.

 Widget_Control, event.top, Get_UValue=info

 formdata = {cancel : 0, menu:1}
 *info.ptrToFormData = formdata
 widget_Control, event.top, /destroy

END

;-----------------------------------------------------------------------------


PRO ADAS312_PROC_EVENT, event

; Get the info structure out of the user value of the top-level base.

Widget_Control, event.top, Get_UValue=info
Widget_Control, event.id, Get_Value=userEvent

; Clear any messages 

widget_control, info.messID, set_value = ' '


CASE event.id OF

  info.cancelID : begin
                    formdata = {cancel:1}
                    *info.ptrToFormData = formdata
                    widget_Control, event.top, /destroy
                  end 
   
  info.optionID : begin
                    
                    opt = userEvent
                    
                    case opt of
                       0 : widget_control, info.transID, sensitive = 0
                       1 : begin
                              widget_control, info.transID, sensitive = 1
                              widget_control, info.lowerID, sensitive = 1
                           end
                       2 : begin
                              widget_control, info.transID, sensitive = 1
                              widget_control, info.lowerID, sensitive = 0
                           end
                    endcase
                  end 
   
  info.doneID   : begin
                                           
                   ; Gather the data for return to calling program
                   ; but do some consistency checks first
                
                    err  = 0
                    mess = ''
                    
                    widget_control, info.optionID, get_value=option
                    option = option[0]
                    
                    widget_control, info.upperID, get_value=n_up
                    if option EQ 1 then widget_control, info.lowerID, get_value=n_low $
                                   else n_low = -1
                    
                    n_up  = n_up[0]
                    n_low = n_low[0]

                    if option EQ 1 then begin
                    
                       if n_up LT 1 OR n_up GT 25 then begin
                          err  = 1
                          mess = 'Upper n must be in the range 1 - 20'
                       endif
                       
                       if err EQ 0 AND ( n_low LT 1 OR n_low GT 20 ) then begin
                          err  = 1
                          mess = 'Lower n must be in the range 1 - 20'
                       endif

                       if err EQ 0 AND ( n_up LE N_low) then begin
                          err  = 1
                          mess = 'Upper n must be greater than the lower one'
                       endif
                    
                    endif
                    
                    if option EQ 2 then begin
                    
                       if n_up LT 1 OR n_up GT 20 then begin
                          err  = 1
                          mess = 'Upper n must be in the range 1 - 20'
                       endif
                    
                    endif
                    
                    if err EQ 0 then begin
                    
                       formdata = {option   : option,       $    
                                   n_low    : n_low,        $    
                                   n_up     : n_up,         $    
                                   menu     : 0,            $  
                                   cancel   : 0             }  
                        *info.ptrToFormData = formdata
                        widget_control, event.top, /destroy
                    
                    endif else begin
                       widget_Control, info.messID, Set_Value=mess
                    endelse

                  end 
   
  else : print,'ADAS312_IN_EVENT : You should not see this message! ',userEvent
                   
ENDCASE

END
;-----------------------------------------------------------------------------



FUNCTION adas312_proc, inval, dsfull, bitfile,  $
                       FONT_LARGE = font_large, $
                       FONT_SMALL = font_small

; Set defaults for keywords

  if n_elements(font_large) eq 0 then font_large = ''
  if n_elements(font_small) eq 0 then font_small = ''


 
; Create modal top level base widget

                
  parent = Widget_Base(Column=1, Title='ADAS 312 : PROCESSING OPTIONS', $
                       XOFFSET=100, YOFFSET=1)


  rc        = widget_label(parent,value='  ',font=font_large)
  mainbase  = widget_base(parent, /column)
  
  rc = cw_adas_dsbr(mainbase, dsfull, font=font_large)
  
  
  infobase = widget_base(mainbase,/column,/frame)
  infobase1 = widget_base(infobase,/column,/frame)        
  label1= widget_label(infobase1, font=font,                      $
  value = "------------- Receiver --------------       - Neutral donor -   ")
  label2   = widget_label(infobase1, font=font,                   $
  value = "        Nuclear   Initial      Final                  Nuclear   ")
  label3   = widget_label(infobase1, font=font,                   $
  value = "Symbol  charge   ion charge  ion charge     Symbol    charge    ")
  label4   = widget_label(infobase1, font=font,                   $
  value = "-------------------------------------------------------------   ")
  label5val = "                                                               "

  ;************************************************************************
  ;*** This is a temporary solution for first 10 adf26/bdn#97 datasets ****
  ;*** (h_h1,h_he2,h_li3,h_be4,h_b5,h_c6,h_n7,h_o8,h_f9,h_ne10).       ****
  ;************************************************************************

  str1=strpos(dsfull,'_')
  str2=strpos(dsfull,'.')
  longsymbol=strmid(dsfull,str1+1,str2-str1-1)

  harvey_fix = ['h1','he2','li3','be4','b5','c6','n7','o8','f9','ne10']
  
  res = where(harvey_fix EQ longsymbol, count)
          
  if count EQ 1 then begin            
  
      if strlen(longsymbol) eq 4 then begin
              symbol=strmid(dsfull,str1+1,2)
              charge=strmid(dsfull,str1+3,2)
              dummy=fix(charge)
              dummy=dummy-1
              charge2=strtrim(string(dummy),2)
      endif
      if strlen(longsymbol) eq 3 then begin
              symbol=strmid(dsfull,str1+1,2)
              charge=strmid(dsfull,str1+3,1)
              dummy=fix(charge)
              dummy=dummy-1
              charge2=strtrim(string(dummy),2)
      endif
      if strlen(longsymbol) eq 2 then begin
              symbol=strmid(dsfull,str1+1,1)
              charge=strmid(dsfull,str1+2,1)
              dummy=fix(charge)
              dummy=dummy-1
              charge2=strtrim(string(dummy),2)
      endif

      strput, label5val, symbol, 1
      strput, label5val, charge, 10
      strput, label5val, charge, 20
      strput, label5val, charge2, 32
      strput, label5val, '1', 56
      strput, label5val, 'H', 46
      
   endif else begin
      
      strput, label5val, 'Using a non standard adf26 filename' + $
                         ' - information not available', 0
                         
   endelse


  label5 = widget_label(infobase1, font=font, value=label5val)


  procbase = widget_base(mainbase,/column,/frame)

  ; Output file types

  rbase = widget_base(procbase,/row)
  
  opts = ['Generate beam stopping file',       $
           'Generate beam emission file',      $
           'Generate beam density file']
  optionID = cw_bgroup(rbase, opts,                        $
                       set_value=0,                        $
                       /no_release, /exclusive,  /row,     $
                       font = font_large)


  ; Show ranges and reference values
  
  col1   = widget_base(procbase,/column,/frame)
  lab    = widget_label(col1, value='adf26 input file plasma parameter information:',$
                        font=font_large)
  row1   = widget_base(col1,/row)
  row2   = widget_base(col1,/row)
  col2   = widget_base(row1,/column)
  row3   = widget_base(col2,/row)
  row4   = widget_base(col2,/row)
  row5   = widget_base(col2,/row)
  col3   = widget_base(row1,/column)
  row6   = widget_base(col3,/row)
  row7   = widget_base(col3,/row)
  row8   = widget_base(col3,/row)
  label1 = widget_label(row3,value='Temperature range (eV):',font=font_large)
  label2 = widget_label(row4,value='Energy range  (eV/amu):',font=font_large)
  label3 = widget_label(row5,value='Density range   (cm-3):',font=font_large)
  text1  = widget_text(row3,font=font_large)
  text2  = widget_text(row4,font=font_large)
  text3  = widget_text(row5,font=font_large)
  label4 = widget_label(row6,value='Reference Temperature (eV):',font=font_large)
  label5 = widget_label(row7,value='Reference Energy  (eV/amu):',font=font_large)
  label6 = widget_label(row8,value='Reference Density   (cm-3):',font=font_large)
  text4  = widget_text(row6,xsize=10,font=font_large)
  text5  = widget_text(row7,xsize=10,font=font_large)
  text6  = widget_text(row8,xsize=10,font=font_large)
  fmt    = '(E10.3)'
  t1     = strtrim(string(inval.temin,fmt)+' -'+string(inval.temax,fmt),2)
  t2     = strtrim(string(inval.ebmin,fmt)+' -'+string(inval.ebmax,fmt),2)
  t3     = strtrim(string(inval.nemin,fmt)+' -'+string(inval.nemax,fmt),2)
  t4     = strtrim(string(inval.teref,fmt),2)
  t5     = strtrim(string(inval.ebref,fmt),2)
  t6     = strtrim(string(inval.neref,fmt),2)
  widget_control, text1, set_value=t1
  widget_control, text2, set_value=t2
  widget_control, text3, set_value=t3
  widget_control, text4, set_value=t4
  widget_control, text5, set_value=t5
  widget_control, text6, set_value=t6


  
  ; Ask for transition quantum numbers
  
  transID  = widget_base(procbase,/column)
  selectid = widget_base(transID,/row)
  lbltitle = widget_label(selectid,$
             value = 'Beam emission or density: Select quantum number(s)', $
             font=font_large)
             
  upperID  = cw_field(selectid, title='Upper n :', xsize=10, $
                      integer=1, fieldfont=font_small, font=font_small)
  
  lowerID  = cw_field(selectid, title='Lower n :', xsize=10, $
                      integer=1, fieldfont=font_small, font=font_small)
          

                      
; Error/Instruction message.
          
  messID = widget_label(parent,value='     Select output file type      ',$
                        font=font_large)


; Buttons at the bottom
                
  menufile = bitfile + '/menu.bmp'
  read_X11_bitmap, menufile, bitmap1
                
  mrow     = widget_base(parent,/row)
  menuID   = widget_button(mrow,value=bitmap1,font=font_large, $
                           event_pro='ADAS312_PROC_MENU')
  cancelID = widget_button(mrow,value='Cancel',font=font_large)
  doneID   = widget_button(mrow,value='Done',font=font_large)


; Initialise the widget

  tc = widget_info(transID,/child)
  widget_control, tc, get_uvalue = lower_id
  
  widget_control, optionID, set_value = inval.option

  case inval.option of
    0 : widget_control, transID, sensitive=0
    1 : begin
           widget_control, upperID, set_value = inval.n_up
           widget_control, lowerID, set_value = inval.n_low
        end
    2 : begin
           widget_control, upperID, set_value = inval.n_up
           widget_control, lowerID, sensitive = 0
        end
  endcase


; Realize the ADAS312_PROC input widget.

   dynlabel, parent
   widget_Control, parent, /realize

; Create a pointer to store the information collected from the form.
; The initial data stored here is set to CANCEL, so nothing needs to
; be done if the user kills the widget with the mouse.

   ptrToFormData = Ptr_New({cancel:1, menu:0})

; Create an info structure with program information.

  info = { messID          :  messID,         $
           cancelID        :  cancelID,       $
           doneID          :  doneID,         $
           optionID        :  optionID,       $
           transID         :  transID,        $
           lowerID         :  lowerID,        $
           upperID         :  upperID,        $
           ptrToFormData   :  ptrToFormData   }  
  
            
; Store the info structure in the user value of the top-level base and launch
; the widget into the world.

  widget_control, parent, Set_UValue=info

  XManager, 'ADAS312_PROC', parent, Event_Handler='ADAS312_PROC_EVENT'


; When the widget is destroyed, the block is released, and we
; return here. Get the form data that was collected by the form
; and stored in the pointer location.

formdata = *ptrToFormData


rep='DONE'
IF N_Elements(formdata) EQ 0 THEN BEGIN
   Ptr_Free, ptrToFormData
   rep ='CANCEL'
ENDIF
 
IF formdata.cancel EQ 1 THEN BEGIN
   Ptr_Free, ptrToFormData
   rep ='CANCEL'
   RETURN,rep
ENDIF

IF formdata.menu EQ 1 THEN BEGIN
   Ptr_Free, ptrToFormData
   rep ='MENU
   RETURN,rep
ENDIF
 

if rep eq 'DONE' then begin
   inval.option = formdata.option
   inval.n_low  = formdata.n_low
   inval.n_up   = formdata.n_up
   Ptr_Free, ptrToFormData
endif

RETURN, rep

END
