; Copyright (c) 1999, Strathclyde University.
; SCCS info: Module @(#)$Header: /home/adascvs/idl/adas3xx/adas313/cdispf.pro,v 1.1 2004/07/06 12:02:41 whitefor Exp $ Date $Date: 2004/07/06 12:02:41 $
;
; CATEGORY:
;	Adas system.
;	
; WRITTEN:
;       Harvey Anderson, University of Strathlcyde, 30-04-98
;       Based on ccispf.pro      
;
; MODIFIED:
;	1.1	Harvey Anderson
;		First Version
;
; VERSION:
;	1.1	30-04-98
;-
;-----------------------------------------------------------------------------
PRO CDISPF,proc_in,pipe

	;**********************************;
	;				   ;
	;    READ DATA FROM THE FORTRAN    ;
	;				   ;
	;**********************************;
	
	readf,pipe,teref
	readf,pipe,ebref
	readf,pipe,neref
	readf,pipe,temin
	readf,pipe,temax
	readf,pipe,ebmin
	readf,pipe,ebmax
	readf,pipe,nemin
	readf,pipe,nemax
	readf,pipe,ntemp
	ttemp = strarr(ntemp)
	for i = 0,ntemp-1 do begin
	   readf,pipe,ftemp
	   ttemp(i) = string(ftemp)
	endfor
	
	;*****************************************;
	;					  ;
	;   SET DEFAULT VALUE IF NOT PROVIDED     ;
	;					  ;
	;*****************************************;
	
	IF ( proc_in.new lt 0 ) THEN BEGIN
		proc_in = {	title		:	  ' ',$
				new		:	    0,$
		                option		:	    1,$
				lowern		:         '3',$
				uppern		:         '3',$
				lowerl		:	  '0',$
				upperl		:	  '1',$
				meta		:	   1 ,$
				names		: ['Singlet','Triplet'],$
				mult		:	   0 ,$
				loweridx	:	  '1',$
			        upperidx	:	  '1',$
				bembut		:	   1 ,$
				grpmess		:          '',$
				temin		:	temin,$
				temax		:	temax,$
				ebmin		:	ebmin,$
				ebmax		:	ebmax,$
				nemin		:	nemin,$
				nemax		:	nemax,$
				teref		:	teref,$
				ebref		:	ebref,$
				neref		:	neref,$
				ntemp		:	ntemp,$
				ttemp		:	ttemp }
	ENDIF

END
