; Copyright (c) 1999, Strathclyde University.
;
; MODIFIED:
;       1.1     Harvey Anderson
;               First Version
;       1.2     Martin O'Mullane
;                 - Identify routine to xmanager as ADAS313_OUT an
;                   not ttcw_adas_gr_sel which was shared among a
;                   few routines.
;                 - Give the common block a unique name.
;
; VERSION:
;       1.1     06-05-98
;       1.2     13-05-2005
;-
;-----------------------------------------------------------------------------
pro adas313_out_event,event
        common general_313_out, grpid , action , value_out
        action = event.action
        case action of
        'Done': begin
                 widget_control,grpid,get_value=value_out
                 widget_control,event.top,/destroy
                end
        'Cancel':widget_control,event.top,/destroy
        'Menu':widget_control,event.top,/destroy
        else: print,'Some other button has been pressed'
        endcase
end
;-----------------------------------------------------------------------------


pro adas313_out ,value,dsfull,devlist,proc_in,bitmap1,font=font,act
        common general_313_out, grpid , action , value_out
        value_out = value

        maxt=n_elements(proc_in.ttemp)
        grplist = strarr(maxt)
        for i=0,maxt-1 do begin
          grplist(i)=string(proc_in.ttemp(i),format='(E10.3)')+'eV'
        end

        listtitle='Select temperature'
        parent=widget_base(title='ADAS313 OUTPUT OPTIONS',/column)
        grpid=cw_adas312_out(parent,dsfull,value=value,grplist=grplist,$
                                  devlist=devlist,listtitle=listtitle,$
                                  font=font,bitmap1)
        state   = { grpid       : grpid }
        widget_control,parent,/realize
        widget_control,widget_info(parent,/child),set_uvalue=state,/no_copy
        xmanager,'ADAS313_OUT',parent,event_handler='adas313_out_event'
        act = action
        value = value_out
end
