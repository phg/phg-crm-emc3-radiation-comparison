; Copyright (c) 1999, Strathclyde University.
; SCCS info: Module @(#)$Header: /home/adascvs/idl/adas3xx/adas313/cw_adas313_trans.pro,v 1.1 2004/07/06 12:42:27 whitefor Exp $ Date $Date: 2004/07/06 12:42:27 $
;
;
; CATEGORY:
;	Adas system.
;
; DESCRIPTION:
;
;       Produces a compound widget which contains editable
;       widget text boxes and three toggle buttons. This 
;	routine is particular to ADAS313. It allow the user 
;	to enter the quantum numbers which describe the
;	initial and final configuration associated with the
;	effective emission coefficient	of interest.
;
; INPUT:
;
;	The input structure contains the initial settings
;	and return the structure with users selected
;	values.
;
;	struct_in = {  lowern   : '3',$
;		       uppern   : '4',$
;          	       lowerl   : '1',$
;                      upperl   : '1',$
;                        meta   :  1 ,$
;			names   : strarr(2),$
;			 mult   :  0 ,$
;		       grpmess  : '' }
;
;	lowern	:	lower principal quantum number
;	uppern	:	upper principal quantum number
;	lowerl	:	lower total L 	quantum number
;	upperl	:	upper total L   quantum number
;	meta    :	labels the reference metastable.
;			meta = 1 : 1s2(1)S is reference
;			meta = 2 : 1s2s(1)S is reference
;			meta = 3 : 1s2s(3)S is reference
;	mult    :	Index corresponding to the
;			choosen multiplicity.
;			mult = 0 Singlet.
;			mult = 1 Triplet.
;	
; WRITTEN
;
;       Harvey Anderson, University of Strathlcyde, 01-05-98
;             
;
; MODIFIED:
;	1.1	Harvey Anderson
;		First Version
;	1.2	Harvey Anderson
;		Introduced the multiplicity selection button.
; VERSION:
;	1.1	01-05-98
;	1.2	28-05-98
;-
;-----------------------------------------------------------------------------
pro rtn_set_val,id,value
  first_child = widget_info(id,/child)
  widget_control, first_child,get_uvalue = status
  	widget_control,status.lowern,set_value=value.lowern
  	widget_control,status.uppern,set_value=value.uppern
	widget_control,status.lowerl,set_value=value.lowerl
	widget_control,status.upperl,set_value=value.uppern
	widget_control,status.meta,set_value=value.meta
	widget_control,status.mult,set_value=value.mult
     if value.meta eq 1 then begin
	widget_control,status.meta1,set_button = 1
	widget_control,status.meta2,set_button = 0
	widget_control,status.meta3,set_button = 0
     end 
     if value.meta eq 2 then begin
	widget_control,status.meta1,set_button = 0
	widget_control,status.meta2,set_button = 1
	widget_control,status.meta3,set_button = 0
     end 
     if value.meta eq 3 then begin
	widget_control,status.meta1,set_button = 0
	widget_control,status.meta2,set_button = 0
	widget_control,status.meta3,set_button = 1
     end 
  	status.numval.lowern=value.lowern
  	status.numval.uppern=value.uppern
  	status.numval.lowerl=value.lowerl
  	status.numval.upperl=value.upperl
  	status.numval.meta=value.meta
	status.numval.mult=value.mult
  widget_control,first_child,set_uvalue=status,/no_copy
END
FUNCTION rtn_get_val,id
	first_child = widget_info(id,/child)
	widget_control, first_child, get_uvalue = status
	widget_control,status.lowern,get_value=lowern
	widget_control,status.uppern,get_value=uppern
	widget_control,status.lowerl,get_value=lowerl
	widget_control,status.upperl,get_value=upperl
	widget_control,status.multi,get_value=mult
	
	;****** update state ******
	
	status.numval.lowern = lowern(0)
	status.numval.uppern = uppern(0)
	status.numval.lowerl = lowerl(0)
	status.numval.upperl = upperl(0)
	status.numval.mult   = mult(0)	
	error = 0
	
	;******* check for missing value ********
	
	if error eq 0 then begin
		if strtrim(status.numval.lowern) eq '' then error = 1
		if strtrim(status.numval.uppern) eq '' then error = 1
		if strtrim(status.numval.lowerl) eq '' then error = 1
		if strtrim(status.numval.upperl) eq '' then error = 1

		if error eq 1 then begin
		 status.numval.grpmess = '**** Missing Value ****'
		end
	end
	
	;***** check for illegal values ********
	
	if error eq 0 then begin

		if num_chk(status.numval.lowern,/sign) eq 1 then error = 1
		if num_chk(status.numval.uppern,/sign) eq 1 then error = 1
		if num_chk(status.numval.lowerl,/sign) eq 1 then error = 1
		if num_chk(status.numval.upperl,/sign) eq 1 then error = 1

		if error eq 1 then begin
		  status.numval.grpmess = '**** Illegal Numeric Value ****'
		end
	end
;
;**********************************************************************;
;       check for valid input when 1s2(1)S is reference metastable     ;
;**********************************************************************;
;
	if error eq 0 then begin
		if status.numval.meta eq 1 then begin
		  if status.numval.uppern eq 1 then error =1
                if error eq 1 then begin
		status.numval.grpmess = 'Invalid quantum numbers for 1s2(1)S metastable reference'
                end

		end
	endif	
;
;**********************************************************************;
;       check for valid input when 1s2s(1)S is reference metastable    ;
;**********************************************************************;
;
	if error eq 0 then begin
	     if status.numval.meta eq 2 then begin
		if status.numval.uppern eq 1 then error =1
                if status.numval.lowern eq 1 then error =1
                if status.numval.uppern eq 2 and status.numval.upperl $
                eq 0 then error =1
                if error eq 1 then begin
		status.numval.grpmess = 'Invalid quantum numbers for 1s2s(1)S metastable reference'
                end
	     end
	endif	
;
;**********************************************************************;
;       check for valid input when 1s2s(3)S is reference metastable    ;
;**********************************************************************;
;
	if error eq 0 then begin
		if status.numval.meta eq 3 then begin
		if status.numval.uppern eq 1 then error =1
                if status.numval.lowern eq 1 then error =1
                if status.numval.uppern eq 2 and status.numval.upperl $
                eq 0 then error =1
                  if error eq 1 then begin
      status.numval.grpmess = 'Invalid quantum numbers for 1s2s(3)S metastable reference'
                  end
		end
	endif	
;
;**********************************************************************;
;	general check to ensure only dipole allowed transitions are    ;
;	selected regardless of the metastable reference.               ;
;**********************************************************************;
;
     if error eq 0 then begin
            if status.numval.lowern gt  status.numval.uppern then error =1
            	     if error eq 1 then begin
		status.numval.grpmess = 'Invalid lower principal quantum number'
             end
    end
     if error eq 0 then begin
      if status.numval.lowern eq 1 and status.numval.lowerl gt 0 then error =1
      if status.numval.lowern eq 2 and status.numval.lowerl gt 1 then error =1
      if status.numval.lowern eq 3 and status.numval.lowerl gt 2 then error =1
      if status.numval.lowern eq 4 and status.numval.lowerl gt 3 then error =1
      if status.numval.lowern eq 2 and status.numval.lowerl gt 1 then error =1
      if status.numval.lowern eq 3 and status.numval.lowerl gt 2 then error =1
      if status.numval.lowern eq 4 and status.numval.lowerl gt 3 then error =1
             if error eq 1 then begin
      status.numval.grpmess = ' L quantum number is out of range'
             end
    end
    if error eq 0 then begin
            if status.numval.uppern gt 4 then error =1
            if status.numval.lowern lt 1 then error =1
	     if error eq 1 then begin
      status.numval.grpmess = 'Insert quantum no. within the permitted range'
             end
    end
    if error eq 0 then begin
    	        if status.numval.lowerl lt 0 then error =1
		if status.numval.upperl gt 3 then error =1
    	        if error eq 1 then begin
      status.numval.grpmess = 'Insert quantum no. within the permitted range'
                end  
    end
    if error eq 0 then begin
           if status.numval.upperl gt status.numval.lowerl then begin
           if  status.numval.upperl ne strcompress(status.numval.lowerl+1,$
           /remove_all) then error =1
        end
          if error eq 1 then begin
    	    status.numval.grpmess = '**** Dipole allowed transitions only ****'
	  end
    end
     if error eq 0 then begin
          if status.numval.lowerl gt status.numval.upperl then begin
	     if status.numval.lowerl ne strcompress(status.numval.upperl+1,$ 
             /remove_all) then error =1
          end
          if error eq 1 then begin
    	    status.numval.grpmess = '**** Dipole allowed transitions only ****'
	  end
    end
     if error eq 0 then begin
	 if status.numval.lowerl eq status.numval.upperl then error=1
          if error eq 1 then begin
    	    status.numval.grpmess = '**** Dipole allowed transitions only ****'
	  end
     endif
;
;**********************************************************************;
;       	         end of checking    			       ;
;**********************************************************************;
;	
	if error eq 1 then begin
	  widget_control,status.messid,set_value=status.numval.grpmess
	end else begin
	  status.numval.grpmess = ''
	  widget_control,status.messid,set_value=status.numval.grpmess
	end
return,status
end
FUNCTION rtn_event,event
    base=event.handler
    first_child = widget_info(base,/child)
    widget_control,first_child,get_uvalue = status
    CASE event.id OF
    	status.lowern:widget_control,status.lowerl,/input_focus
    	status.lowerl:widget_control,status.uppern,/input_focus
       	status.uppern:widget_control,status.upperl,/input_focus
        status.upperl:widget_control,status.lowern,/input_focus
        status.meta1 : status.numval.meta = 1
	status.meta2 : status.numval.meta = 2
	status.meta3 : status.numval.meta = 3
    else :
    endcase
    widget_control, first_child, set_uvalue=status,/no_copy
    return, 0L
  END   
;  
FUNCTION cw_adas313_trans,parent,value=value,font=font

IF (N_PARAMS() LT 1 ) THEN MESSAGE,'Must specify PARENT for cw_adas313_trans'

	;***** Return to caller on error *****
	
	ON_ERROR, 2
	
IF NOT (KEYWORD_SET(value)) THEN begin

	numval = { lowern   : '',$
		   uppern   : '',$
          	   lowerl   : '',$
                   upperl   : '',$
                   meta     : 1 ,$
		   names    : strarr(2),$
		   mult	    : 0,$
		   grpmess  : '' }
end else begin
	numval = { lowern   : value.lowern  ,$
		   uppern   : value.uppern  ,$
		   lowerl   : value.lowerl  ,$
                   upperl   : value.upperl  ,$
                   meta     : value.meta    ,$
		   names    : value.names   ,$
		   mult	    : value.mult    ,$
		   grpmess  : value.grpmess }
end

IF NOT (KEYWORD_SET(font)) THEN font = ''
;
	main = widget_base(parent,/frame,/column,$
		        EVENT_FUNC = 'rtn_event',$
		  FUNC_GET_VALUE = 'rtn_get_val',$
		   PRO_SET_VALUE = 'rtn_set_val')

        first_child = widget_base(main,/column)
        
	textid =widget_base(first_child,/row)
	mainid = widget_base(first_child,/column)
        selectid = widget_base(mainid,/row)

	editid1 = widget_base(selectid,/column,/frame)
        editid2 = widget_base(selectid,/column,/frame)
        editid3 = widget_base(editid2,/column,/exclusive)
        editid4 =widget_base(editid2,/column,/frame)
        qnlabel = widget_base(editid1,/row)
	nrangeid = widget_base(editid1,/row)
        lrangeid = widget_base(editid1,/row)
	nlowid = widget_base(editid1,/column)
	nuppid = widget_base(editid1,/column)
	multid = widget_base(editid4,/row)

	lbltitle = widget_label(textid,$
	 value='Select quantum numbers & metastable reference',font=font)

	lbllower = widget_label(qnlabel,value='      Lower Level',font=font)
	lblupper = widget_label(qnlabel,value='Upper Level',font=font)

	lbltxt = widget_label(nrangeid,font=font,value='N ( 1 - 4)')
        lbltxt1 = widget_label(lrangeid,font=font,value='L ( 0 - 3)')

	lowern = widget_text(nrangeid,/editable,xsize=8,font=font)
        lowerl = widget_text(lrangeid,/editable,xsize=8,font=font)
	uppern = widget_text(nrangeid,/editable,xsize=8,font=font)
	upperl = widget_text(lrangeid,/editable,xsize=8,font=font)

        multi  = cw_bselector(multid,numval.names,font=font)

        meta1 = widget_button(editid3,value='1s2(1)S',font=font)
        meta2 = widget_button(editid3,value='1s2s(1)S',font=font)
        meta3 = widget_button(editid3,value='1s2s(3)S',font=font)

     	;******* error message ******
	
	if strtrim(numval.grpmess) eq '' then begin
		message = '                                      '
	end else begin
		message = numval.grpmess
	end

	messid = widget_label(mainid,value=message,font = font)
	
	;****** set initial values *********

		widget_control,lowern,set_value=numval.lowern
		widget_control,uppern,set_value=numval.uppern
                widget_control,lowerl,set_value=numval.lowerl
		widget_control,upperl,set_value=numval.upperl
        if  numval.meta eq 1 then begin
		widget_control,meta1,set_button = 1
		widget_control,meta2,set_button = 0
                widget_control,meta3,set_button = 0
        end
        if  numval.meta eq 2 then begin
		widget_control,meta1,set_button = 0
		widget_control,meta2,set_button = 1
                widget_control,meta3,set_button = 0
        end
        if  numval.meta eq 3 then begin
		widget_control,meta1,set_button = 0
		widget_control,meta2,set_button = 0
                widget_control,meta3,set_button = 1
        end       
		widget_control,multi,set_value = numval.mult
		

		;****** create new state structure ********
		
	new_status = { 	LOWERN          :	lowern	        ,$
		 	UPPERN   	:	uppern	        ,$
  			LOWERL		:	lowerl		,$
			UPPERL		:	upperl		,$
		 	NUMVAL		:	numval		,$
  			META1		:	meta1		,$
			META2		:	meta2		,$
			META3		:	meta3		,$
			MULTI		:	multi		,$
		 	MESSID		:	messid		,$
		 	FONT		:	font		}
		 	
		 ;***** save intial state structure ********
		 
	widget_control,first_child,set_uvalue=new_status,/no_copy
	RETURN,main
end		   
