;----------------------------------------------------------------------
;+
; PROJECT    :  ADAS
;
; NAME       :  run_adas313
;
; PURPOSE    :  Runs ADAS313 GCRC/BME production code as an IDL subroutine.
;
; ARGUMENTS  :  All output arguments will be defined appropriately.
;
;               NAME      I/O    TYPE   DETAILS
; INPUTS     :  adf26      I     str    bundle-n population file.
;               adf21      O     str    adf21 (adf22) BMS (GCRC) file name.
;               i_high     I     int    upper index of GCRC coeff. (1-3) for /gcrc.
;               i_low      I     int    lower index of GCRC coeff. (1-3) for /gcrc.
;               relmet     I     int    relative metastabel (/bme)
;                                           1 : 1S2(1)S
;                                           2 : 1S2S(1)S
;                                           3 : 1S2S(3)S
;               n_high     I     int    upper n (1-4) for /bme
;               l_high     I     int    upper l (0-n-1) for /bme
;               n_low      I     int    lower n (1-4) for /bme
;               l_low      I     int    lower l (0-n-1) for /bme
;               multi      I     int    multiplicity
;                                        0 : singlet
;                                        1 : triplet
;
;
; KEYWORDS      gcrc               -    request generalised CR coefficients.
;               bme                -    request beam emission.
;               help               -    If specified this comment section
;                                       is written to screen.
;
;
; OUTPUTS    :  An adf21 or adf22 file is the output.
;
;
; NOTES      :  run_adas313 uses the spawn command. Therefore IDL v5.3
;               cannot be used. Any other version should work.
;
; AUTHOR     :  Martin O'Mullane
;
; DATE       :  07-11-2006
;
; MODIFIED
;       1.1     Martin O'Mullane
;                 - First version.
;
; DATE
;       1.1     07-11-2006
;
;-
;----------------------------------------------------------------------


PRO run_adas313, adf26  = adf26,  $
                 adf21  = adf21,  $
                 gcrc   = gcrc,   $
                 i_low  = i_low,  $
                 i_high = i_high, $
                 bme    = bme,    $
                 relmet = relmet, $
                 n_high = n_high, $
                 l_high = l_high, $
                 n_low  = n_low,  $
                 l_low  = l_low,  $
                 multi  = multi,  $
                 help   = help


; If asked for help

if keyword_set(help) then begin
   doc_library, 'run_adas311'
   return
endif



; First check that we are not using IDL v5.3

if !version.release EQ '5.3' then begin
   message, 'This version of IDL (v5.3) will not work - ' + $
            'change to another v5 or v6'
   return
endif


; Check that we get all_low inputs and that they are correct. Otherwise print
; a message and return to command line.

on_error, 2
close, /all

; Input screen

if n_elements(adf26) EQ 0 then message, 'A bundle-n population file is necessary'
a313_adf26 = adf26

; GCRC or BME

if keyword_set(gcrc) AND keyword_set(bme) then message, 'Only one of /GCRC or /BME'

if keyword_set(gcrc) then begin

   a313_choice = 1

   if n_elements(i_low) EQ 0 then begin
      message, 'Set lower index (i_low)'
   endif else begin
      if i_low LT 1 OR i_low GT 3 then message, 'i_low must be between 1-3'
      a313_low = long(i_low)
   endelse

   if n_elements(i_high) EQ 0 then begin
      message, 'Set higher index (i_high)'
   endif else begin
      if i_high LT 1 OR i_high GT 3 then message, 'i_high must be between 1-3'
      a313_high = long(i_high)
   endelse

endif


if keyword_set(bme) then begin

   a313_choice = 2

   if n_elements(n_low) EQ 0 then begin
      message, 'Set lower n quantum number (n_low)'
   endif else begin
      if n_low LT 1 OR n_low GT 4 then message, 'n_low must be between 1-4'
      a313_n_low = long(n_low)
      if n_elements(l_low) EQ 0 then begin
         message, 'Set lower l quantum number (l_low)'
      endif else begin
         if l_low LT 0 OR l_low GT n_low-1 then message, 'n_low must be between 0-n-1'
         a313_l_low = long(l_low)
      endelse
   endelse

   if n_elements(n_high) EQ 0 then begin
      message, 'Set higher n quantum number (n_high)'
   endif else begin
      if n_high LT 1 OR n_high GT 4 then message, 'n_high must be between 1-4'
      a313_n_high = long(n_high)
      if n_elements(l_high) EQ 0 then begin
         message, 'Set higher l quantum number (l_high)'
      endif else begin
         if l_high LT 0 OR l_high GT n_high-1 then message, 'n_high must be between 0-n-1'
         a313_l_high = long(l_high)
      endelse
   endelse

   if n_elements(relmet) EQ 0 then begin
      message, 'Set driving metastable (1-3)'
   endif else begin
      if relmet LT 1 OR relmet GT 3 then message, 'relmet must be between 1-3'
      a313_relmet = long(relmet)
   endelse

   if n_elements(multi) EQ 0 then begin
      message, 'Set driving metastable (1-3)'
   endif else begin
      if multi EQ 0 OR multi EQ 1 then a313_multi = long(multi) $
                                  else message, 'multi must be 0 or 1'
   endelse

endif


; Output - only adf21/adf22

if n_elements(adf21) NE 0 then begin
   a313_adf21 = adf21
   a313_out21 = 1L
endif else message, 'An output adf21/adf22 dataset is required'


;------------------
; Now run the code
;------------------

fortdir  = getenv('ADASFORT')
centroot = getenv('ADASCENT')

spawn, fortdir+'/adas313.out', unit=pipe, /noshell, PID=pid

date = xxdate()

; ugly - but 313 has this adf04 dataset hardwired for A-value

printf, pipe,centroot+'/adf04/helike/helike_kvi97#he0.dat'
printf, pipe, date[0]


rep = 'NO '
printf, pipe, rep
printf, pipe, a313_adf26


TEREF     = 0.0D0
EBREF     = 0.0D0
NEREF     = 0.0D0
TERAY_L   = 0.0D0
TERAY_H   = 0.0D0
EBRAY_L   = 0.0D0
EBRAY_H   = 0.0D0
NERAY_L   = 0.0D0
NERAY_H   = 0.0D0
ITCOUNT   = 0L

readf, pipe, TEREF
readf, pipe, EBREF
readf, pipe, NEREF
readf, pipe, TERAY_L
readf, pipe, TERAY_H
readf, pipe, EBRAY_L
readf, pipe, EBRAY_H
readf, pipe, NERAY_L
readf, pipe, NERAY_H
readf, pipe, ITCOUNT

teray = dblarr(itcount)

fdum = 0.0D0
for j = 0, itcount-1 do begin
   readf, pipe, fdum
   teray[j] = fdum
endfor

idone   = 1
icancel = 0
imenu   = 0

printf, pipe, idone
printf, pipe, icancel
printf, pipe, imenu

printf, pipe, a313_choice


if a313_choice EQ 1 then begin

   index = [[1,2,3], [4,5,6], [7,8,9]]

   ind = index[a313_high-1, a313_low-1]
   printf, pipe, ind

endif else begin

   printf, pipe, a313_relmet
   printf, pipe, a313_n_high
   printf, pipe, a313_l_high
   printf, pipe, a313_n_low
   printf, pipe, a313_l_low
   printf, pipe, a313_multi

endelse

idum = 0
readf, pipe, idum


idone   = 1
icancel = 0
imenu   = 0

printf, pipe, idone
printf, pipe, icancel
printf, pipe, imenu

printf, pipe, a313_out21
printf, pipe, a313_adf21
printf, pipe, 0L

idone   = 0
icancel = 0
imenu   = 1

printf, pipe, idone
printf, pipe, icancel
printf, pipe, imenu

adas_wait, pid
close, pipe

END
