; Copyright (c) 1999, Strathclyde University.
; SCCS info: Module @(#)$Header: /home/adascvs/idl/adas3xx/adas313/adas313_surface.pro,v 1.1 2004/07/06 10:36:45 whitefor Exp $ Date $Date: 2004/07/06 10:36:45 $
;
; MODIFIED:
;	1.1	Harvey Anderson
;		First Version
;
; VERSION:
;	1.1	06-05-98
;-
;-----------------------------------------------------------------------------
pro adas313_surface_event,event
	common plot313_blk, action ,pname, pdevice
	action = event.action
	case action of
	'Menu':widget_control,event.top,/destroy
	'Done':begin
	       widget_control,event.top,/destroy
	       end
	endcase
end 
pro adas313_surface,setting,proc_out,bitmap1,font=font,act
	common plot313_blk,action , pname ,pdevice
	pname = proc_out.hardname
	pdevice = proc_out.devsel
		base=widget_base(title='ADAS313')
	cwd=cw_adas313_surface(base,state,bitmap1,font=font,setting,proc_out)
	widget_control,base,/realize
	xmanager,'adas313_surface',base
	act = action
end
