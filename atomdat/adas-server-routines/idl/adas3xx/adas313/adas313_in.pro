; Copyright (c) 1999, Strathclyde University.
;
; MODIFIED:
;       1.1     Harvey Anderson
;               First Version
;       1.2     Martin O'Mullane
;                 - Give the common block a unique name.
;
; VERSION:
;       1.1     30-04-98
;       1.2     13-05-2005
;-
;-----------------------------------------------------------------------------
pro adas313_in_event,event
        common general_313_in, action, cwid ,proc_out
        action = event.action
        case action of
             'Done': begin
                     widget_control,cwid,get_value = proc_out
                     widget_control,event.top,/destroy
                     end
             'Cancel':widget_control,event.top,/destroy
             'Menu':widget_control,event.top,/destroy
        endcase
end
;-----------------------------------------------------------------------------


pro adas313_in,proc_in,dsfull,bitmap1,font=font,act
        common general_313_in, action , cwid ,proc_out
        proc_out = proc_in
        base = widget_base(/column,title='ADAS313 PROCESSING OPTIONS')
        cwid = cw_adas313_in(base,value=proc_in,dsfull,bitmap1,font=font)
        widget_control,base,/realize
        xmanager,'adas313_in',base
        act = action
        proc_in = proc_out
end
