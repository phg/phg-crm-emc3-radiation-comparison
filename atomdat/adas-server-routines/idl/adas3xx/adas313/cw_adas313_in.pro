; Copyright (c) 1999, Strathclyde University.
; SCCS info: Module @(#)$Header: /home/adascvs/idl/adas3xx/adas313/cw_adas313_in.pro,v 1.4 2005/04/13 08:36:16 allan Exp $ Date $Date: 2005/04/13 08:36:16 $
;
; CATEGORY:
;	Adas system.
;	
; WRITTEN:
;       Harvey Anderson, University of Strathlcyde, 10-07-97
;             
;
; MODIFIED:
;	1.1	Harvey Anderson
;		First Version
;	1.2	Martin O'Mullane
;               If input filename does not correspond to a central ADAS
;               adf26 file do not attempt to parse it - put up a
;               message instead.
;	1.3	Allan Whiteford
;               Changed call from cw_adas312_dis to cw_adas313_dis.
;               cw_adas312_dis was removed from ADAS312 so was moved
;               here.
;	1.4	Allan Whiteford
;               Changed rstrpos to strpos with /reverse_search.
;
; VERSION:
;	1.1	10-07-97
;	1.2	04-11-2004
;	1.3	16-12-2004
;	1.4	13-04-2005
;-
;-----------------------------------------------------------------------------

FUNCTION rnn_event,event

widget_control,widget_info(event.handler,/child),get_uvalue = state

 case event.id of
 	state.option1 : begin
 			widget_control,state.tranbase,sensitive = 0		
 			widget_control,state.gcrcbase,sensitive = 1 
 			widget_control,state.transid,get_value = out
 			widget_control,out.messid,set_value=''
           		state.inval.option = 1
 			end
 	state.option2 : begin
 			widget_control,state.tranbase,sensitive = 1
 			widget_control,state.gcrcbase,sensitive = 0
 			widget_control,state.gcrcid,get_value = out
 			widget_control,out.messid,set_value=''
			state.inval.option = 2
 			end
 	state.cancelid:begin
 			new_event = { ID:0L,TOP:event.top,$
 				HANDLER:0L, ACTION :'Cancel' }
 		       end
 	state.menuid:begin
 			new_event = { ID:0L, TOP:event.top,$
 			   HANDLER:0L,ACTION:'Menu'}
 		     end
 	state.doneid:begin
 	
 		;*************************************;
 		;   Check for errors in the input     ;
	        ;   in both widgets                   ;
 		;*************************************;
 		
 		widget_control,state.transid,get_value=traval
		widget_control,state.gcrcid,get_value=gcrval
 		if strtrim(traval.numval.grpmess) ne '' then begin
 			new_event = 0L
 		end else begin
		if strtrim(gcrval.numval.grpmess) ne '' then begin
 			new_event = 0L
 		end else begin
 			new_event = { ID:0L, TOP:event.top,$
 			HANDLER:0L,ACTION:'Done'}
 		end
 		end
		end
 		
 endcase
  widget_control,widget_info(event.handler,/child),set_uvalue= state,/no_copy
 RETURN,new_event
end
;-----------------------------------------------------------------------------


FUNCTION rnn_get_val,id
	widget_control,widget_info(id,/child),get_uvalue = state
	widget_control,state.transid,get_value=tranval
	widget_control,state.gcrcid,get_value=gcrcval
	;**** update the state structure ****
		state.inval.lowern = tranval.numval.lowern
		state.inval.uppern = tranval.numval.uppern
		state.inval.lowerl = tranval.numval.lowerl
		state.inval.upperl = tranval.numval.upperl
		state.inval.meta   = tranval.numval.meta
		state.inval.mult   = tranval.numval.mult
		state.inval.loweridx = gcrcval.numval.loweridx
		state.inval.upperidx = gcrcval.numval.upperidx
RETURN,state.inval
END
;-----------------------------------------------------------------------------


PRO rnn_set_val,id,value

	ON_ERROR, 2

	widget_control,widget_info(id,/child),get_uvalue = state
 
 	disval	=	{ temin	:	value.temin,	$
 			  temax	:	value.temax,	$
 			  ebmin	:	value.ebmin,	$
 			  ebmax :	value.ebmax,	$
 			  nemin	:	value.nemin,	$
 			  nemax	:	value.nemax,	$
 			  teref	:	value.teref,	$
 			  ebref	:	value.ebref,	$
 			  neref	:	value.neref	}
 			  
 	tranval =	{ lowern	: value.lowern,	$
 			  uppern	: value.uppern,	$
			  lowerl	: value.lowerl, $
			  upperl	: value.upperl, $
 			  option	: value.option,	$
 			  grpmess	: value.grpmess	}

        gcrcval = {  loweridx   	: value.loweridx,$
		     upperidx   	: value.upperidx,$
		     option     	: value.option ,$
		     grpmess    	: ''  };

 			  
 	widget_control,state.disid,set_value=disvaltt1

 
  
  
  if state.inval.option eq 1 then begin
		widget_control,state.option1,set_button = 1
		widget_control,state.option2,set_button = 0
		widget_control,state.tranbase,sensitive = 0
 	        widget_control,state.gcrcbase,sensitive = 1
                widget_control,out.messid,set_value=''
		state.inval.option = 2
		end 
  if state.inval.option eq 2 then begin
		widget_control,state.option1,set_button = 0
		widget_control,state.option2,set_button = 1
		widget_control,state.tranbase,sensitive  = 1
		widget_control,state.gcrcbase,sensitive  = 0
		state.inval.option = 1
	end
 
	state.inval.lowern 	= value.lowern
	state.inval.uppern 	= value.uppern

	state.inval.lowerl 	= value.lowerl
	state.inval.upperl 	= value.upperl

	state.inval.meta 	= value.meta
 
	state.inval.loweridx    =  value.loweridx
	state.inval.upperidx	=  value.upperidx
	
	state.inval.grpmess  	= value.grpmess
	state.inval.temin    	= value.temin
	state.inval.temax	= value.temax
	state.inval.ebmin	= value.ebmin
	state.inval.ebmax	= value.ebmax
	state.inval.nemin	= value.nemin
	state.inval.nemax	= value.nemax
	state.inval.teref	= value.teref
	state.inval.ebref	= value.ebref
	state.inval.neref	= value.neref
	state.inval.option	= value.option
	state.inval.new		= value.new
	state.inval.title	= value.title
	
widget_control,widget_info(id,/child),set_uvalue = state,/no_copy
END
;-----------------------------------------------------------------------------


FUNCTION cw_adas313_in,parent,value=value,dsfull,bitmap1,font=font

ON_ERROR, 2
	
IF (N_PARAMS() LT 1) THEN MESSAGE,'Must specify PARENT for cw_adas312_in'

	IF NOT (KEYWORD_SET(value)) THEN begin
	  inval = { title	: ' ',$
	  	    new		:  0,$
	  	    option 	: 1 ,$
	  	    lowern 	: '',$
		    uppern 	: '',$
	  	    lowerl 	: '',$
		    upperl 	: '',$
	  	    meta 	: '',$
		    names	: strarr(2),$
		    mult	:  0,$
		    loweridx	: '',$
		    upperidx	: '',$
		    bembut   	: 1 ,$
		    grpmess 	: '',$
		    temin	: '',$
		    temax	: '',$
	            ebmin	: '',$
		    ebmax	: '',$
	            nemin	: '',$
		    nemax	: '',$
		    teref	: '',$
		    ebref	: '',$
		    neref	: '',$
		    ntemp	: '',$
		    ttemp	: strarr(25) } 
	END ELSE BEGIN
	  inval = { title	: value.title	,$
	  	    new		: value.new     ,$
	  	    option 	: value.option  ,$
	  	    lowern 	: value.lowern  ,$
		    uppern 	: value.uppern  ,$
	  	    lowerl 	: value.lowerl  ,$
		    upperl 	: value.upperl  ,$
	  	    meta 	: value.meta    ,$
	            names	: value.names   ,$
		    mult	: value.mult    ,$
		    loweridx	: value.loweridx,$
		    upperidx	: value.upperidx,$
		    bembut   	: value.bembut  ,$
		    grpmess 	: value.grpmess ,$
		    temin	: value.temin	,$
		    temax	: value.temax	,$
		    ebmin	: value.ebmin	,$
		    ebmax	: value.ebmax	,$
	            nemin	: value.nemin	,$
		    nemax	: value.nemax	,$
		    teref	: value.teref	,$
		    ebref	: value.ebref	,$
		    neref	: value.neref   ,$
		    ntemp	: value.ntemp	,$
		    ttemp	: value.ttemp	}
	END
	
	main = widget_base(parent,/column,$
	       EVENT_FUNC='rnn_event',$
	       FUNC_GET_VALUE='rnn_get_val',$
	       PRO_SET_VALUE='rnn_set_val')
	mainbase = widget_base(main,/column)
	
		;***********************
		;**** add run title ****
		;***********************

        tbase = widget_base(mainbase, /row)
        rc = widget_label(tbase, value='Title for Run', font=font)
        runid = widget_text(tbase, value=value.title, xsize=38, font=font, /edit)

	rc = cw_adas_dsbr(mainbase, dsfull, font=font)

	infobase = widget_base(mainbase,/column,/frame)
	infobase1 = widget_base(infobase,/column,/frame)	
    	label1= widget_label(infobase1, font=font,			$
    	value = "------------- Receiver --------------       - Neutral donor -   ")
    	label2   = widget_label(infobase1, font=font,			$
    	value = "        Nuclear   Initial      Final                  Nuclear   ")
    	label3   = widget_label(infobase1, font=font,			$
    	value = "Symbol  charge   ion charge  ion charge     Symbol    charge    ")
    	label4   = widget_label(infobase1, font=font,			$
    	value = "-------------------------------------------------------------   ")
    	label5val = "                                                               "

	;************************************************************************
	;*** This is a temporary solution for first 10 adf26/bnl#98 datasets ****
	;**(he_h1,he_he2,he_li3,he_be4,he_b5,he_c6,he_n7,he_o8,he_f9,he_ne10).***
	;************************************************************************

    	str1=strpos(dsfull,'_',/reverse_search)
    	str2=strpos(dsfull,'.')
    	longsymbol=strmid(dsfull,str1+1,str2-str1-1)
        
        
        harvey_fix = ['h1','he2','li3','be4','b5','c6','n7','o8','f9','ne10']
        
        res = where(harvey_fix EQ longsymbol, count)
                
        if count EQ 1 then begin            
        
	   if strlen(longsymbol) eq 4 then begin
		   symbol=strmid(dsfull,str1+1,2)
		   charge=strmid(dsfull,str1+3,2)
		   dummy=fix(charge)
		   dummy=dummy-1
		   charge2=strtrim(string(dummy),2)
	   endif
	   if strlen(longsymbol) eq 3 then begin
		   symbol=strmid(dsfull,str1+1,2)
		   charge=strmid(dsfull,str1+3,1)
		   dummy=fix(charge)
		   dummy=dummy-1
		   charge2=strtrim(string(dummy),2)
	   endif
	   if strlen(longsymbol) eq 2 then begin
		   symbol=strmid(dsfull,str1+1,1)
		   charge=strmid(dsfull,str1+2,1)
		   dummy=fix(charge)
		   dummy=dummy-1
		   charge2=strtrim(string(dummy),2)
	   endif

    	   strput, label5val, symbol, 1
    	   strput, label5val, charge, 10
    	   strput, label5val, charge, 20
    	   strput, label5val, charge2, 32
       	   strput, label5val, '2', 56
    	   strput, label5val, 'he', 46

         endif else begin
            
            strput, label5val, 'Using a non standard adf26 filename' + $
                               ' - information not available', 0
                               
         endelse


	;************************************************************************
	
    	label5 = widget_label(infobase1, font=font, value=label5val)

	
	procbase = widget_base(mainbase,/column,/frame)
	
	;***** Option Widget *****
	
	rowid = widget_base(procbase,/row,/exclusive)
	
	option1 = widget_button(rowid,value ='Generate beam stopping file',font=font)
	option2 = widget_button(rowid,value ='Generate beam emission file',font=font)
	
	;**** Display Widget *****
	
	disval = { 	temin	:	inval.temin,	$
			temax	:	inval.temax,	$
			ebmin	:	inval.ebmin,	$
			ebmax	:	inval.ebmax,	$
			nemin	:	inval.nemin,	$
			nemax	:	inval.nemax,	$
			teref	:	inval.teref,	$
			ebref	:	inval.ebref,	$
			neref	:	inval.neref	}
			
	disid = cw_adas313_dis(procbase,value = disval ,font=font)
	
	;**** Transtition & Coupling coefficient Widget ****
	
	tranval = {	lowern	:  	inval.lowern,	$
			uppern	:  	inval.uppern,	$
			lowerl	:  	inval.lowerl,	$
			upperl	:  	inval.upperl,	$
			meta	:	inval.meta,	$
			names   :	inval.names,    $
			mult	:       inval.mult,	$
			grpmess	:  	inval.grpmess}
			
        gcrcval = {  loweridx   : 	inval.loweridx,$
		     upperidx   : 	inval.upperidx,$
		     grpmess    :       ''  }

	 trngcr = widget_base(procbase,/row)

	 gcrcbase = widget_base(trngcr,/column)

	 tranbase = widget_base(trngcr,/column)

         gcrcid  = cw_adas313_gcrc(gcrcbase,value=gcrcval,font=font)
	  
	 transid = cw_adas313_trans(tranbase ,value = tranval,font=font)
	
	;**** Widget buttons ****
	
	buttbase = widget_base(main,/row)

	menuid = widget_button(buttbase,value = bitmap1)
	cancelid = widget_button(buttbase,value = 'Cancel',font=font)
	doneid = widget_button(buttbase,value = 'Done',font=font)
	
	;**** Initial settings *****
	
	if inval.option eq 1 then begin
		widget_control,option1,set_button = 1
		widget_control,option2,set_button = 0
		widget_control,tranbase,sensitive = 0
		widget_control,gcrcbase,sensitive = 1
	end 
	if inval.option eq 2 then begin
		widget_control,option1,set_button = 0
		widget_control,option2,set_button = 1
		widget_control,tranbase,sensitive = 1
		widget_control,gcrcbase,sensitive = 0
	end
	
	;**** Create new state structure *****
	
	new_state  =	{	OPTION1	: 	option1	,$
				OPTION2 :	option2 ,$
				TRANSID :	transid ,$
				TRANBASE:	tranbase,$
				GCRCBASE:	gcrcbase,$
				GCRCVAL	:	gcrcval,$
				GCRCID	:	gcrcid	,$
				INVAL	:	inval	,$
				TRANVAL :	tranval	,$
				DISVAL	:	disval	,$
				DISID	:	disid	,$
				MENUID	:	menuid  ,$
				DONEID	:	doneid	,$
				CANCELID:	cancelid }
				
	;**** Save initial state structure ****
	
	widget_control,mainbase,set_uvalue=new_state,/no_copy
RETURN,main	
end
	
