; Copyright (c) 1999, Strathclyde University.
; SCCS info: Module @(#)$Header: /home/adascvs/idl/adas3xx/adas313/adas313.pro,v 1.4 2004/11/04 17:06:17 mog Exp $ Date $Date: 2004/11/04 17:06:17 $
;
; PROJECT :
;		ADAS
;
; NAME:
;		ADAS313
;
; PURPOSE :
;	 	Highest level routine of the adas program
;		ADAS313.
;
; EXPLANATION:
;
; USE:
;
; INPUTS:
;
;	ADASREL - A string indicating the ADAS system version, 
;		  e.g ' ADAS RELEASE: ADAS93 V1.1.0'.  The first
;		  character should be a space.
;
;	FORTDIR - A string holding the path to the directory where the
;		  FORTRAN executables are, e.g '/disk/adas/fortran/exec'
;
;	USERROOT - A string holding the path to the root directory of
;		   the user's adas data. e.g '/disk/bowen/adas'
;		   This root directory will be used by adas to construct
;		   other path names.  For example the users default data
;		   for adas506 should be in /disk/bowen/adas/adf20.  In
;		   particular the user's default interface settings will
;		   be stored in the directory USERROOT+'/defaults'.  An
;		   error will occur if the defaults directory does not
;		   exist.
;
;	CENTROOT - Like USERROOT, but this directory points to the
;		   central data area for the system.  User defaults are
;		   not stored on CENTROOT.
;
;	DEVLIST - A string array of hardcopy device names, used for
;		  graphical output. e.g ['Post-Script','HP-PCL','HP-GL']
;		  This array must mirror DEVCODE.  DEVCODE holds the
;		  actual device names used in a SET_PLOT statement.
;
;	DEVCODE - A string array of hardcopy device code names used in
;		  the SET_PLOT statement, e.g ['PS', 'PCL', 'HP']
;		  This array must mirror DEVLIST.
;
;	FONT_LARGE - The name of a larger font e.g 'courier_bold14'
;
;	FONT_SMALL - The name of a smaller font e.g 'courier_bold12'
;
;	EDIT_FONTS - A structure of two fonts used in the table editor
;		     adas_edtab.pro, {font_norm:'',font_input:''}.
;		     The two fonts are used to differentiate between
;		     editable and non-editable parts of the table. You
;		     may have to experiment to get two fonts which give
;		     the same default spacing in the edit widget.
;		     e.g {font_norm:'helvetica_bold14', $
;			  font_input:'helvetica_oblique14'}
;
; OPTIONAL INPUTS:
;	None
;
; OUTPUTS:
;	None - Note this routine should leave all inputs un-modified.
;
; OPTIONAL OUTPUTS:
;	None
;
; KEYWORD PARAMETERS:
;	None
;
; CALLS:
;
;
; SIDE EFFECTS:
;	This routine spawns a FORTRAN executable.  Note the pipe 
;	communications routines listed above.  In addition to these
;	pipe communications there is one explicit communication of the
;	date to the FORTRAN code, search for 'printf,pipe' to find it.
;
; CATEGORY:
;	Adas system.
;	
; WRITTEN:
;       Harvey Anderson, University of Strathlcyde, 04-30-98
;	Based on the adas312.pro
;             
;
; MODIFIED:
;	1.1	Harvey Anderson
;		First Version
;	1.2	Richard Martin
;		Increased version no. to 1.2
;	1.3	Richard Martin
;		Increased version no. to 1.3
;	1.4	Martin O'Mullane
;               Change output default name to adas313_graph.ps.     
;		Increase version number to 1.4 due to change
;               of behaviour when parsing adf26 files in.
;
; VERSION:
;	1.1	04-30-98
;	1.2	15-10-99
;	1.3	14-03-2001
;	1.4	04-11-2004
;
;-
;-----------------------------------------------------------------------------
pro adas313, adasrel, fortdir, userroot, centroot, devlist, $
             devcode, font_large,font_small, edit_fonts

		;************************
		;**** Initialisation ****
		;************************

	adasprog = ' PROGRAM: ADAS313 V1.4'
	lpend =0
	gomenu=0
	deffile= userroot+'/defaults/adas313_defaults.dat'
	device=''
	bitfile= centroot + '/bitmaps'
	menu=bitfile+'/menu.bmp'
	date = xxdate()
	read_X11_bitmap,menu,bitmap1
	
		;******************************************
		;**** Search for user default settings ****
		;**** If not found create defaults     ****
		;******************************************

	files = findfile(deffile)
	if files(0) eq deffile then begin
	restore,deffile
	inval.centroot=centroot+'/adf26'
	inval.userroot=userroot+'/adf26'
	end else begin
   		inval = { $
		ROOTPATH:userroot+'/adf26/bnl99#he_fast', $
		FILE:'', $
		CENTROOT:centroot+'/adf26/bnl99#he_fast', $
		USERROOT:userroot+'/adf26/bnl99#he_fast' }
    		proc_in  = {new:-1}
    		proc_out = {new:-1}
        end

	IF ( proc_out.new lt 0 ) THEN BEGIN
	 
	proc_out = {    new	: 0,$
	                outbut  : 0,$
			gtit1	: '',$
			scalbut	: 0,$
			xmin	:'0',$
			xmax	:'0',$
			ymin	:'0',$
			ymax	:'0',$
			zmin	:'0',$
			zmax	:'0',$
			hrdout	: 0,$
			txtout  : 0,$
			txtname : '',$
			hardname:'',$
			grpdef	:'adas313_graph.ps',$
			grpfmess:'',$
			grpsel	: 1,$
			grprmess:'',$
			grptmess:'',$
			devsel:0,$
			grselmess:''}
		ENDIF
					    
  setting  = {	    zdata	:fltarr(25,25), $
		    ydata	:fltarr(25),	$
		    xdata	:fltarr(25),	$
		    axis	: 1,		$
		    rotz	: 30,		$
		    rotx	: 30,		$
		    xlog	: 0,		$
		    ylog	: 0,		$
		    zlog	: 0,		$		    
		    zlocation	: 3,		$
		    shad_colour : 0,		$
		    hist_surf	: 0,		$
		    horz_surf	: 0,		$
	            surf_type	: 1,		$
		    xmargin	: [10.0,2.0],	$
		    ymargin	: [4.0,2.0],	$
		    x_title	: 'NEUTRAL BEAM ENERGY ( eV amu!E-1!N )',$
	            y_title	: 'TARGET DENSITY ( cm!E3!N )',$
		    z_title	: '',$
      		    main_title	: '',$
		    adas_info   :adasrel+adasprog+ ' DATE : '+date(0)+ ' TIME :'+date(1),$
		    file_info	: ' FILE  : ' ,$
		    font	: '',$
		    prtbut	: 0,		$
		    cfactor     :1.0}

		    
;  		*********************************
;  		*				*
;  		* 	START FORTRAN CODE	*
;  		*				*
;  		*********************************
;
  
  	spawn,fortdir+'/adas313.out',unit=pipe,/noshell,PID=pid
;
;	  ********************************************
;	  *					     *
;         *   CHECK THAT FORTRAN IS STILL RUNNING    *
;         *					     *
;         ********************************************

	if find_process(pid) eq 0 then goto, labelend

		printf,pipe,centroot+'/adf04/helike/helike_kvi97#he0.dat'
		printf,pipe,date(0)


labelstart:
		;**** Check FORTRAN still running ****

        if find_process(pid) eq 0 then goto, labelend

		;************************************************
		;**** Communicate with  fortran and          ****
		;**** invoke user interface widget for       ****
		;**** Data file selection                    ****
		;************************************************

    cdspf0, pipe, inval, dsfull, rep, FONT=font_large

		;**** If cancel selected then end program ****

    if rep eq 'YES' then begin 
	 goto, labelend
    endif

		;**** Check FORTRAN still running ****

    if (find_process(pid) eq 0) then begin 
	goto, labelend
    endif
	     setting.file_info = setting.file_info + dsfull
	 	cdispf,proc_in,pipe
labelsecond:
		adas313_in,proc_in,dsfull,bitmap1,font=font_large,action
			if action eq 'Menu' then begin
			        idone   = 0
			        icancel = 0
			        imenu   = 1
				printf,pipe,idone
			        printf,pipe,icancel
			        printf,pipe,imenu
	   			goto,labelend
			end
			if action eq 'Cancel' then begin
			        idone   = 0
			        icancel = 1
			        imenu   = 0
			        printf,pipe,idone
			        printf,pipe,icancel
			        printf,pipe,imenu
			        goto,labelstart
			end
			if action eq 'Done' then begin
				idone   = 1
			        icancel = 0
			        imenu   = 0
			        printf,pipe,idone
			        printf,pipe,icancel
			        printf,pipe,imenu
				cdsets,proc_in,setting,pipe
			        adas313_mess,font=font_large,pipe
labelthird:
				adas313_out,proc_out,dsfull,devlist,proc_in,bitmap1,font=font_large,action
				   if action eq 'Menu' then begin
				        idone   = 0
			        	icancel = 0
			        	imenu   = 1
					printf,pipe,idone
			        	printf,pipe,icancel
			        	printf,pipe,imenu
					goto,labelend
				   end
				   if action eq 'Cancel' then begin
				        idone   = 0
			        	icancel = 1
			        	imenu   = 0
			        	printf,pipe,idone
			        	printf,pipe,icancel
			        	printf,pipe,imenu
				   	goto,labelsecond
				   end
				   if action eq 'Done' then begin
				   	idone   = 1
			        	icancel = 0
			        	imenu   = 0
			        	printf,pipe,idone
			        	printf,pipe,icancel
			        	printf,pipe,imenu
				   cdspf1,proc_out,setting,bitmap1,font=font_large,action,pipe
					if action eq 'Menu' then begin
				          idone   = 0
			        	  icancel = 0
			        	  imenu   = 1
				          printf,pipe,idone
			        	  printf,pipe,icancel
			        	  printf,pipe,imenu
					  goto,labelend
					end
					if action eq 'Done' then begin 
				          idone   = 1
			        	  icancel = 0
			        	  imenu   = 0
				          printf,pipe,idone
			        	  printf,pipe,icancel
			        	  printf,pipe,imenu				
					  goto,labelthird
					end 
				   end	
			end
;	 end  		  

 labelend:
 save,inval,proc_in,proc_out,filename=deffile
 end 
