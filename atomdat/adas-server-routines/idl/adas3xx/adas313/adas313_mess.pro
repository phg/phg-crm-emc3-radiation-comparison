; Copyright (c) 1999, Strathclyde University.
; SCCS info: Module @(#)$Header: /home/adascvs/idl/adas3xx/adas313/adas313_mess.pro,v 1.1 2004/07/06 10:36:39 whitefor Exp $ Date $Date: 2004/07/06 10:36:39 $
;
; CATEGORY:
;	Adas system.
;	
; WRITTEN:
;       Harvey Anderson, University of Strathlcyde, 30-04-98
;             
;
; MODIFIED:
;	1.1	Harvey Anderson
;		First Version
;
; VERSION:
;	1.1	28-05-98
;-
;-----------------------------------------------------------------------------
pro adas313_mess,font=font,pipe
	base = widget_base(/column,xoffset=300, yoffset=200,               $
               title = "ADAS313: INFORMATION")
	lab  =widget_label(base,                                           $
        value="                                                   ")
        lab1 = widget_label(base,                                          $
        value="    ADAS313 CALCULATIONS UNDERWAY - PLEASE WAIT    ",font=font)
	lab2  =widget_label(base,                                          $
        value="                                                   ")

	widget_control,base,/realize

		;************************************************
		;**** Wait for the final signal from FORTRAN ****
		;************************************************

         readf, pipe, idum

                ;****************************************
                ;**** Destroy the information widget ****
                ;****************************************

         widget_control, base, /destroy

end
