; Copyright (c) 1999, Strathclyde University.
; SCCS info: Module @(#)$Header: /home/adascvs/idl/adas3xx/adas313/cdspf1.pro,v 1.1 2004/07/06 12:03:00 whitefor Exp $ Date $Date: 2004/07/06 12:03:00 $
;
; CATEGORY:
;	Adas system.
;	
; WRITTEN:
;       Harvey Anderson, University of Strathlcyde, 10-07-97
;             
;
; MODIFIED:
;	1.1	Harvey Anderson
;		First Version
;
; VERSION:
;	1.1	06-05-98
;-
;-----------------------------------------------------------------------------
PRO CDSPF1,proc_out,setting,bitmap1,font=font,action,pipe

	printf,pipe,proc_out.txtout
	IF proc_out.txtout eq 1 THEN BEGIN
	;print,proc_out.txtname
		printf,pipe,proc_out.txtname
	END 
	
	; wait until fortran has finished writing file

	printf,pipe,proc_out.outbut
	IF proc_out.outbut eq 1 THEN BEGIN
                tval = proc_out.grpsel + 1
		printf,pipe,tval
		readf,pipe,iecount
		readf,pipe,incount
		
       ; read the zdata
       		
		for j = 0 , incount-1 do begin
		  for i = 0 , iecount-1 do begin
		      readf,pipe,tzdata
		      setting.zdata(i,j) = tzdata
		  endfor
		endfor
		
       ; read the xdata
       
               for i = 0 , iecount-1 do begin
               	     readf,pipe,txdata
               	     setting.xdata(i) = txdata
               endfor
       
       ; read the ydata
       
              for j = 0 , incount-1 do begin
                     readf,pipe,tydata
                     setting.ydata(j) = tydata
              endfor

		IF proc_out.scalbut ne 1 AND $
		   proc_out.hrdout ne 1 THEN BEGIN
		   setting.prtbut =0
                   adas313_surface,setting,proc_out,bitmap1,font=font,action
		END
		
		IF proc_out.scalbut ne 1 AND $
		   proc_out.hrdout eq 1 THEN BEGIN
		   setting.prtbut = 1
    		   adas313_surface,setting,proc_out,bitmap1,font=font,action
                END
        END ELSE BEGIN
        	printf,pipe,proc_out.outbut
        END
END
             		   
		   
		
