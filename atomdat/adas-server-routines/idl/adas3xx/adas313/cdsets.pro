; Copyright (c) 1999, Strathclyde University.
; SCCS info: Module @(#)$Header: /home/adascvs/idl/adas3xx/adas313/cdsets.pro,v 1.1 2004/07/06 12:02:51 whitefor Exp $ Date $Date: 2004/07/06 12:02:51 $
;
; CATEGORY:
;	Adas system.
;	
; WRITTEN:
;       Harvey Anderson, University of Strathlcyde, 30-04-98
;             
;
; MODIFIED:
;	1.1	Harvey Anderson
;		First Version
;
; VERSION:
;	1.1	30-04-98
;-
;-----------------------------------------------------------------------------
PRO CDSETS,proc_in,setting,pipe
;
	printf,pipe,proc_in.option
;
	IF proc_in.option eq 1 THEN BEGIN
        setting.z_title = 'CROSS COUPLING COEFFICIENT ( cm!E3!N s!E-1!N )'
        setting.main_title = ' CROSS COUPLING COEFFICIENT SURFACE PLOT :'
	   if proc_in.loweridx eq 1 then begin
		if proc_in.upperidx eq 1 then begin
		   printf,pipe,1
		end else if proc_in.upperidx eq 2 then begin
		   printf,pipe,2
		end else if proc_in.upperidx eq 3 then begin
		   printf,pipe,3
		end
	   end
	   if proc_in.loweridx eq 2 then begin
		if proc_in.upperidx eq 1 then begin
		   printf,pipe,4
		end else if proc_in.upperidx eq 2 then begin
		   printf,pipe,5
		end else if proc_in.upperidx eq 3 then begin
		   printf,pipe,6
		end
	   end
	   if proc_in.loweridx eq 3 then begin
		if proc_in.upperidx eq 1 then begin
		   printf,pipe,7
		end else if proc_in.upperidx eq 2 then begin
		   printf,pipe,8
		end else if proc_in.upperidx eq 3 then begin
		   printf,pipe,9
		end
	   end

	END
	IF proc_in.option eq 2 THEN BEGIN
	setting.z_title = 'EFFECTIVE EMISSION COEFFICIENT ( cm!E3!N s!E-1!N )'
	setting.main_title = ' BEAM EMISSION COEFFICIENT SURFACE PLOT :'
		    printf,pipe,proc_in.meta		   
		    printf,pipe,proc_in.uppern
		    printf,pipe,proc_in.upperl
		    printf,pipe,proc_in.lowern
		    printf,pipe,proc_in.lowerl
	            printf,pipe,proc_in.mult
		end
	END
END
