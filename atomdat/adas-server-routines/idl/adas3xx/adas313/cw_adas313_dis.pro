;----------------------------------------------------------------------------
;
;	PROJECT : 	ADAS
;	
;	NAME	:	cw_adas313_dis.pro
;
;	PURPOSE	:	Produces a widget used to display the range
;			of values for which the emission or stopping
;			can be extracted from the ADF26 file.
;
;	USE	:	This widget is specific to ADAS313.
;
;	INPUT	:
;			Parent	- Long integer; ID of the parent widget.
;			Value	- A structure containg the following.
;
;				  Temin	: Minimum temperature.
;				  Temax : Maximum temperature
;				  Ebmin : Minimum beam energy.
;				  Ebmax : Maximum beam energy.
;				  Nemin : Minimum target density.
;				  Nemax : Maximum target density.
;				  Teref : Reference temperature.
;				  Ebref	: Reference beam energy.
;				  Neref	: Reference target density.
;
;	AUTHOR	:	HARVEY ANDERSON
;			UNIVERSITY OF STRATHCLYDE
;			ANDERSON@CHAMBA.PHYS.STRATH.AC.UK
;
;	DATE	:	01/7/97
;
;	MODIFIED: 	
;	     1.1	Richard Martin
;			Put under SCCS control.
;	     1.2	Richard Martin
;			Changed /col to /column in widget_base commands.
;			/col caused code to crash on HP machines.
;	     1.3	Allan Whiteford
;			This routine was cw_adas312_dis, it was removed
;                       from ADAS312 but it is still required in ADAS313
;
;	VERSION:
;	     1.1	17-07-97
;	     1.2	29-08-97
;	     1.3	16-12-04
;
;----------------------------------------------------------------------------
;
FUNCTION cw_adas313_dis, PARENT,VALUE=value,FONT=font
		        
IF (N_PARAMS() LT 1) THEN MESSAGE, 'Must specify parent for cw_adas313_dis'

	;***** return to caller on error *****
	
	ON_ERROR,2
	
	;***** set defaults for keywords *****
	
IF NOT (KEYWORD_SET(value)) THEN BEGIN

     	dis = { 	temin	 : '',$
			temax	 : '',$
			ebmin	 : '',$
			ebmax	 : '',$
			nemin	 : '',$
			nemax	 : '',$
			teref	 : '',$
			ebref	 : '',$
			neref	 : '' }
END ELSE BEGIN

     	dis = { 	temin	 : value.temin,$
			temax	 : value.temax,$
			ebmin	 : value.ebmin,$
			ebmax	 : value.ebmax,$
			nemin	 : value.nemin,$
			nemax	 : value.nemax,$
			teref	 : value.teref,$
			ebref	 : value.ebref,$
			neref	 : value.neref }
END

IF NOT (KEYWORD_SET(font)) THEN font=''

	
        
        col1=widget_base(parent,/column,/frame)
        rc = widget_label(col1, $
           value='Input plasma parameter information:',font=font)
	row1=widget_base(col1,/row)
	row2=widget_base(col1,/row)
	col2=widget_base(row1,/column)
	row3=widget_base(col2,/row)
	row4=widget_base(col2,/row)
	row5=widget_base(col2,/row)
	col3=widget_base(row1,/column)
	row6=widget_base(col3,/row)
	row7=widget_base(col3,/row)
	row8=widget_base(col3,/row)
        label1=widget_label(row3,value='Temperature range (eV):',font=font)
        label2=widget_label(row4,value='Energy range  (eV/amu):',font=font)
        label3=widget_label(row5,value='Density range   (cm-3):',font=font)
        text1=widget_text(row3,font=font)
        text2=widget_text(row4,font=font)
        text3=widget_text(row5,font=font)
        label4=widget_label(row6,value='Reference Temperature (eV):',font=font)
        label5=widget_label(row7,value='Reference Energy  (eV/amu):',font=font)
        label6=widget_label(row8,value='Reference Density   (cm-3):',font=font)
        text4=widget_text(row6,xsize=10,font=font)
        text5=widget_text(row7,xsize=10,font=font)
        text6=widget_text(row8,xsize=10,font=font)
        num_font='(E10.3)'
        text1_out=string(dis.temin,num_font)+' -'+string(dis.temax,num_font)
        text2_out=string(dis.ebmin,num_font)+' -'+string(dis.ebmax,num_font)
        text3_out=string(dis.nemin,num_font)+' -'+string(dis.nemax,num_font)
        text4_out=string(dis.teref,num_font)
        text5_out=string(dis.ebref,num_font)
        text6_out=string(dis.neref,num_font)
        text1_out=strtrim(text1_out,2)
        text2_out=strtrim(text2_out,2)
        text3_out=strtrim(text3_out,2)
        text4_out=strtrim(text4_out,2)
        text5_out=strtrim(text5_out,2)
        text6_out=strtrim(text6_out,2)
        widget_control,text1,set_value=text1_out
        widget_control,text2,set_value=text2_out
        widget_control,text3,set_value=text3_out
        widget_control,text4,set_value=text4_out
        widget_control,text5,set_value=text5_out
        widget_control,text6,set_value=text6_out
	RETURN,PARENT
END
