; Copyright (c) 1999, Strathclyde University.
; SCCS info: Module @(#)$Header: /home/adascvs/idl/adas3xx/adas313/cw_adas313_gcrc.pro,v 1.1 2004/07/06 12:42:16 whitefor Exp $ Date $Date: 2004/07/06 12:42:16 $
;
; CATEGORY:
;	Adas system.
;
; DESCRIPTION:
;
;       Produces a compound widget which contains editable
;       widget text boxes. This routine is particular to ADAS313. 
;	It allow the user to enter the index numbers which 
;	reference the collisional-radiative coupling coefficients.
;
; INPUT:
;
;	The input structure contains the initial settings
;	and return the structure with users selected
;	values.
;
;	struct_in = {  loweridx   : '3',$
;		       upperidx   : '4',$
;		       grpmess    : ''  }
;
;	loweridx	:	lower index number
;	upperidx	:	upper index number
;
;	
; WRITTEN:
;       Harvey Anderson, University of Strathlcyde, 02-05-98
;             
;
; MODIFIED:
;	1.1	Harvey Anderson
;		First Version
;
; VERSION:
;	1.1     02-05-98
;-
;-----------------------------------------------------------------------------
pro ret_set_val,id,value
  first_child = widget_info(id,/child)
  widget_control, first_child,get_uvalue = status
  widget_control,status.lowertxt,set_value=value.loweridx
  widget_control,status.uppertxt,set_value=value.upperidx
 
  status.numval.loweridx=value.loweridx
  status.numval.upperidx=value.upperidx
  
  widget_control,first_child,set_uvalue=status,/no_copy
  
END
FUNCTION ret_get_val,id
	first_child = widget_info(id,/child)
	widget_control, first_child, get_uvalue = status
	widget_control,status.lowertxt,get_value=lowidx
	widget_control,status.uppertxt,get_value=uppidx
	
	;****** update state ******
	
	status.numval.loweridx = lowidx(0)
	status.numval.upperidx = uppidx(0)
	error = 0
	
	;******* check for missing value ********
	
	if error eq 0 then begin
	  if strtrim(status.numval.loweridx) eq '' then error = 1
	  if strtrim(status.numval.upperidx) eq '' then error = 1
	  if error eq 1 then begin
	    status.numval.grpmess = '**** Missing Value ****'
	  end
	end
	
	;***** check for illegal values ********
	
	if error eq 0 then begin
		if num_chk(status.numval.loweridx,/sign) eq 1 then error = 1
		if num_chk(status.numval.upperidx,/sign) eq 1 then error = 1
		if error eq 1 then begin
		  status.numval.grpmess = 'Illegal Numeric Value'
		end
	end
	
	
   ;**** check to ensure that index numbers are in the permitted range *****
   
   	if error eq 0 then begin
   	       if status.numval.upperidx gt 3 then error = 1
   	       if status.numval.loweridx gt 3 then error = 1
   	       if status.numval.upperidx eq 0 then error = 1
   	       if status.numval.loweridx eq 0 then error = 1
   	        if error eq 1 then begin
   	          status.numval.grpmess = 'Insert index within the permitted range'
	        end
	end

	if error eq 1 then begin
	  widget_control,status.messid,set_value=status.numval.grpmess
	end else begin
	  status.numval.grpmess = ''
	  widget_control,status.messid,set_value=status.numval.grpmess
	end
return,status
end
FUNCTION ret_event,event
    base=event.handler
    first_child = widget_info(base,/child)
    widget_control,first_child,get_uvalue = status
    CASE event.id OF
    	status.lowertxt:widget_control,status.uppertxt,/input_focus
    	status.uppertxt:widget_control,status.lowertxt,/input_focus
    else :
    endcase
    widget_control, first_child, set_uvalue=status,/no_copy
    return, 0L
  END     
FUNCTION cw_adas313_gcrc,parent,value=value,font=font

IF (N_PARAMS() LT 1 ) THEN MESSAGE,'Must specify PARENT for cw_adas312_trans'

	;***** Return to caller on error *****
	
	ON_ERROR, 2
	
IF NOT (KEYWORD_SET(value)) THEN begin

	numval = { loweridx : '',$
		   upperidx : '',$
		   grpmess : ''  }
end else begin
	numval = { loweridx : value.loweridx,$
		   upperidx : value.upperidx,$
		   grpmess  : value.grpmess }
end
	
IF NOT (KEYWORD_SET(font)) THEN font = ''
	main = widget_base(parent,/frame,/column,$
		EVENT_FUNC = 'ret_event',$
		FUNC_GET_VALUE = 'ret_get_val',$
		PRO_SET_VALUE = 'ret_set_val')
	first_child = widget_base(main)
	mainid = widget_base(first_child,/column)
	selectid = widget_base(mainid,/row)

	editid = widget_base(mainid,/row,/frame)
	rangeid = widget_base(editid,/column)
	lowid = widget_base(editid,/column)
	uppid = widget_base(editid,/column)

	lbltitle = widget_label(selectid,$
	 value='Select coupling coefficient',font=font)
	lblblk   = widget_label(rangeid,value=' ',font=font)
	lbllower = widget_label(lowid,value='Lower Index',font=font)
	lblupper = widget_label(uppid,value='Upper Index',font=font)
	lbltxt = widget_label(rangeid,font=font,value='Range ( 1 - 3)')
	lowertxt = widget_text(lowid,/editable,xsize=5,font=font)
	uppertxt = widget_text(uppid,/editable,xsize=5,font=font)
	
	;******* error message ******
	
	if strtrim(numval.grpmess) eq '' then begin
		message = '                                     '
	end else begin
		message = numval.grpmess
	end
	messid = widget_label(mainid,value=message,font = font)
	
	;****** set initial values *********

	widget_control,lowertxt,set_value=numval.loweridx
	widget_control,uppertxt,set_value=numval.upperidx

		;****** create new state structure ********
		
	new_status = { 	LOWERTXT        :	lowertxt	,$
		 	UPPERTXT	:	uppertxt	,$
		 	NUMVAL		:	numval		,$
		 	MESSID		:	messid		,$
		 	FONT		:	font		}
		 	
		 ;***** save intial state structure ********
		 
	widget_control,first_child,set_uvalue=new_status,/no_copy
	RETURN,main
end		   
