; Copyright (c) 1995, Strathclyde University .
; SCCS info: Module @(#)$Header: /home/adascvs/idl/adas3xx/adas302/adas302.pro,v 1.3 2004/07/06 10:27:47 whitefor Exp $ Date $Date: 2004/07/06 10:27:47 $
;
; PROJECT:
;       ADAS IBM MVS to UNIX conversion
;
; NAME:
;	ADAS302
;
; PURPOSE:
;	The highest level routine for the ADAS302 program.
;
; EXPLANATION:
;	This routine is called from the main adas system routine, adas.pro,
;	to start the ADAS302 application.  Associated with adas302.pro
;	is a FORTRAN executable.  The IDL code provides the user
;	interface and output graphics whilst the FORTRAN code reads
;	in data files, performs numerical processing and creates the
;	output files.  The IDL code communicates with the FORTRAN
;	executable via a bi-directional UNIX pipe.  The unit number
;	used by the IDL for writing to and reading from this pipe is
;	allocated when the FORTRAN executable is 'spawned' (see code
;	below).  Pipe communications in the FORTRAN process are to
;	stdin and stdout, i.e streams 5 and 6.
;
;	The FORTRAN code is an independent process under the UNIX system.
;	The IDL process can only exert control over the FORTRAN in the
;	data which it communicates via the pipe.  The communications
;	between the IDL and FORTRAN must be exactly matched to avoid
;	input conversion errors.  The correct ammounts of data must be
;	passed so that neither process 'hangs' waiting for communications
;	which will never occur.
;
;	The FORTRAN code performs some error checking which is
;	independent of IDL.  In cases of error the FORTRAN may write
;	error messages.  To prevent these error messages from conflicting
;	with the pipe communications all FORTRAN errors are written to
;	output stream 0, which is stderr for UNIX.  These error messages
;	will appear in the window from which the ADAS session/IDL session
;	is being run.
;
;	In the case of severe errors the FORTRAN code may terminate
;	itself prematurely.  In order to detect this, and prevent the
;	IDL program from 'hanging' or crashing, the IDL checks to see
;	if the FORTRAN executable is still an active process before
;	each group of pipe communications.  The process identifier
;	for the FORTRAN process, PID, is recorded when the process is
;	first 'spawned'.  The system is then checked for the presence
;	of the FORTRAN PID.
;
; USE:
;	First the system settings must be established by calling
;	adas_sys_set.pro then adas302.pro is called to start the
;	ADAS302 application;
;
;	adas_sys_set, adasrel, fortdir, userroot, centroot, 		$
;		      devlist, devcode, font_large, font_small, edit_fonts
;	adas302,   adasrel, fortdir, userroot, centroot, devlist, 	$
;		   devcode, font_large, font_small, edit_fonts
;
; INPUTS:
;	ADASREL - A string indicating the ADAS system version, 
;		  e.g ' ADAS RELEASE: ADAS93 V1.1.0'.  The first
;		  character should be a space.
;
;	FORTDIR - A string holding the path to the directory where the
;		  FORTRAN executables are, e.g '/disk/adas/fortran/exec'
;
;	USERROOT - A string holding the path to the root directory of
;		   the user's adas data. e.g '/disk/bowen/adas'
;		   This root directory will be used by adas to construct
;		   other path names.  For example the users default data
;		   for adas302 should be in /disk/bowen/adas/adf24.  In
;		   particular the user's default interface settings will
;		   be stored in the directory USERROOT+'/defaults'.  An
;		   error will occur if the defaults directory does not
;		   exist.
;
;	CENTROOT - Like USERROOT, but this directory points to the
;		   central data area for the system.  User defaults are
;		   not stored on CENTROOT.
;
;	DEVLIST - A string array of hardcopy device names, used for
;		  graphical output. e.g ['Post-Script','HP-PCL','HP-GL']
;		  This array must mirror DEVCODE.  DEVCODE holds the
;		  actual device names used in a SET_PLOT statement.
;
;	DEVCODE - A string array of hardcopy device code names used in
;		  the SET_PLOT statement, e.g ['PS', 'PCL', 'HP']
;		  This array must mirror DEVLIST.
;
;	FONT_LARGE - The name of a larger font e.g 'courier_bold14'
;
;	FONT_SMALL - The name of a smaller font e.g 'courier_bold12'
;
;	EDIT_FONTS - A structure of two fonts used in the table editor
;		     adas_edtab.pro, {font_norm:'',font_input:''}.
;		     The two fonts are used to differentiate between
;		     editable and non-editable parts of the table. You
;		     may have to experiment to get two fonts which give
;		     the same default spacing in the edit widget.
;		     e.g {font_norm:'helvetica_bold14', $
;			  font_input:'helvetica_oblique14'}
;
; OPTIONAL INPUTS:
;	None
;
; OUTPUTS:
;	None - Note this routine should leave all inputs un-modified.
;
; OPTIONAL OUTPUTS:
;	None
;
; KEYWORD PARAMETERS:
;	None
;
; CALLS:
;	FIND_PROCESS	Checks to see if a given process is active.
;	XXDATE		Get date and time from operating system.
;	C2SPF0		Pipe comms with FORTRAN C2SPF0 routine.
;	C2SETP		Pipe comms with FORTRAN C2SETP routine.
;	C2ISPF		Pipe comms with FORTRAN C2ISPF routine.
;	C2SPF1		Pipe comms with FORTRAN C2SPF1 routine.
;	C2OUTG		Pipe comms with FORTRAN C2OUTG routine.
;
; SIDE EFFECTS:
;	This routine spawns a FORTRAN executable.  Note the pipe 
;	communications routines listed above.  In addition to these
;	pipe communications there is one explicit communication of the
;	date to the FORTRAN code, and other communications of user's
;	intentions to immediately leave the program by pressing the 
;	return to menu button, search for 'printf, pipe' to find them.
;
; CATEGORY:
;	Adas system.
;	
; WRITTEN:
;       Hugh Summers, University of Strathclyde, 12/11/1996
;
; VERSION:
;	1.1	Hugh Summers
;               First written
;	1.2	Richard Martin
;		Increased version number to 1.2
;	1.3	Richard Martin
;		Increased version number to 1.3
;
; MODIFIED:
;	1.1	12/11/96
;	1.2	04/04/96
;	1.3	18-03-02
;
;
;-----------------------------------------------------------------------------


PRO ADAS302,	adasrel, fortdir, userroot, centroot,                   $
		devlist, devcode, font_large, font_small, edit_fonts

		;************************
		;**** Initialisation ****
		;************************

    adasprog = ' PROGRAM: ADAS302 V1.3'
    lpend = 0
    gomenu = 0
    deffile = userroot+'/defaults/adas302_defaults.dat'
    device = ''
    bitfile = centroot+'/bitmaps'

		;******************************************
		;**** Search for user default settings ****
		;**** If not found create defaults     ****
		;******************************************

    files = findfile(deffile)
    if files(0) eq deffile then begin
        restore, deffile
        inval.centroot = centroot+'/adf02/'
        inval.userroot = userroot+'/adf02/'
    endif else begin
        inval 	= { 	ROOTPATH	:	userroot+'/adf02/', 	$
			FILE		:		'', 		$
			CENTROOT	:	centroot+'/adf02/',	$
			USERROOT	:	userroot+'/adf02/' 	}
   	procval = {	NEW		:	-1			}
	tianame = userroot + '/pass/tiaout.pass'
   	outval =  {	GRPOUT		:	0, 			$
			GTIT1		:	'', 			$
			GRPSCAL		:	0,			$
			XMIN		:	'',  			$
			XMAX		:	'', 			$
			YMIN		:	'',  			$
			YMAX		:	'',			$
			HRDOUT		:	0,			$
			HARDNAME	:	'', 			$
			GRPDEF		:	'',			$
			GRPFMESS	:	'', 			$
			GRPSEL		:	-1,			$
			GRPRMESS	:	'', 			$
			DEVSEL		:	-1, 			$
			GRSELMESS	:	'', 			$
			TEXOUT		:	0, 			$
			TEXAPP		:	-1, 			$
			TEXREP		:	0, 			$
			TEXDSN		:	'', 			$
			TEXDEF		:	'paper.txt',		$
			TEXMES		:	'', 			$
			TIAOUT		:	0, 			$
			TIAAPP		:	-1, 			$
			TIAREP		:	0, 			$
			TIADSN		:	'', 			$
			TIADEF		:	tianame,		$
			TIAMES		:	'' 			}
    endelse

 		;**** Always set text output to not append since Fortran
		;**** won't have opened it

    outval.texapp = -1

		;****************************
		;**** Start fortran code ****
		;****************************

    spawn, fortdir+'/adas302.out', unit=pipe, /noshell, PID=pid

		;************************************************
		;**** Get date and write to fortran via pipe ****
		;************************************************

    date = xxdate()
    printf, pipe, date(0)

LABEL100:
;    print,'adas302.pro: at label 100'
		;**** Check FORTRAN still running ****
  
    if find_process(pid) eq 0 then goto, LABELEND

		;************************************************
		;**** Communicate with c2spf0 in fortran and ****
		;**** invoke user interface widget for       ****
		;**** Data file selection                    ****
		;************************************************

    c2spf0, pipe, inval, dsfull, rep, FONT=font_large

		;**** If cancel selected then end program ****

    if rep eq 'YES' then goto, LABELEND
  
		;**** Check FORTRAN still running ****

    if find_process(pid) eq 0 then goto, LABELEND

		;*************************************
		;**** Read pipe comms from c2setp ****
		;*************************************

    c2setp, pipe, nbsel


LABEL200:
;    print,'adas302.pro: at label 200'
		;**** Check FORTRAN still running ****

    if find_process(pid) eq 0 then goto, LABELEND

		;************************************************
		;**** Communicate with c2ispf in fortran and ****
		;**** invoke user interface widget for       ****
		;**** Processing options                     ****
		;************************************************

    c2ispf, pipe, lpend, procval, dsfull, nbsel, gomenu, bitfile,       $
	    FONT_LARGE=font_large, FONT_SMALL=font_small,               $
	    EDIT_FONTS=edit_fonts

		;**** If menu button clicked, tell FORTRAN to stop ****

    if gomenu eq 1 then begin
 	printf, pipe, 1
 	goto, LABELEND
    endif else begin
 	printf, pipe, 0
    endelse


 		;**** If cancel selected then goto 100 ****

    if lpend eq 1 then goto, LABEL100

		;**** Fortran processing now in progress. ****
		;**** Create header for output.           ****

    header = adasrel+adasprog+' DATE: '+date(0)+' TIME: '+date(1)

		;**** Create header for output. ****

    if find_process(pid) eq 0 then goto, LABELEND

                
LABEL300:
;    print,'adas302.pro: at label 300'
		;**** Check FORTRAN still running ****

    if find_process(pid) eq 0 then goto, LABELEND

		;************************************************
		;**** Communicate with c2spf1 in fortran and ****
		;**** invoke user interface widget for       ****
		;**** Output options                         ****
		;************************************************

    c2spf1, pipe, lpend, outval, dsfull, header, bitfile, gomenu,	 $
            pid, DEVLIST=devlist, FONT=font_large

                ;**** Extra check to see whether FORTRAN is still there ****

    if find_process(pid) eq 0 then goto, LABELEND

		;**** If menu button clicked, tell FORTRAN to stop ****

    if gomenu eq 1 then begin
	printf, pipe, 1
        outval.texmes = ''
	outval.tiames = ''
	goto, LABELEND
    endif else begin
	printf, pipe, 0
    endelse

		;**** If cancel selected then erase output ****
		;**** messages and goto 200.		   ****

    if lpend eq 1 then begin
       outval.texmes = ''
       goto, LABEL200
    end

		;************************************************
		;**** If graphical output requested          ****
		;**** Communicate with c2outg in fortran and ****
		;**** invoke widget Graphical output         ****
		;************************************************

    if outval.grpout eq 1 then begin

		;**** User explicit scaling ****

        grpscal = outval.grpscal
        xmin = outval.xmin
        xmax = outval.xmax
        ymin = outval.ymin
        ymax = outval.ymax

		 ;**** Hardcopy output ****

        hrdout = outval.hrdout
        hardname = outval.hardname
        if outval.devsel ge 0 then device = devcode(outval.devsel)
 
		 ;*** user title for graph ***

       utitle = outval.gtit1
 
       c2outg, dsfull, pipe, utitle, grpscal, xmin, xmax, ymin, ymax, 	$
	       hrdout, hardname, device, header, bitfile, gomenu,      	$
               FONT=font_large

		;**** If menu button clicked, tell FORTRAN to stop ****

;       print,'adas302.pro: at exit from c2outg'

        if gomenu eq 1 then begin
            printf, pipe, 1
            outval.texmes = ''
	    outval.tiames = ''
            goto, LABELEND
        endif else begin
            printf, pipe, 0
        endelse
    endif
	
    if outval.tiaout ne 0 and procval.ltsel ne 0 then begin

		;**** Wait for fortran to finish ****

        widget_control, /hourglass
        waitid = widget_base(/column)
        waitid = widget_base(title='ADAS 302 Message',/column)
        lab = widget_label(waitid,value='Calculating and stacking data for'+$
			   ' TIA file', font=font_large)
        widget_control, waitid, /realize
        cont = 0
        readf, pipe, cont
        widget_control, waitid, /destroy
    endif

		;**** Back for more output options ****

GOTO, LABEL300

LABELEND:
;    print,'adas302.pro: at label end'

		;**** Save user defaults ****

    save, inval, procval, outval, filename=deffile

		;**** If the Fortran is writing files, wait for it ****
    if find_process(pid) ne 0 then begin
        cont = 0
        readf,pipe,cont
    endif

END
