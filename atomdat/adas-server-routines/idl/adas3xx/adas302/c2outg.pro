; Copyright (c) 1995, Strathclyde University .
; SCCS info: Module @(#)$Header: /home/adascvs/idl/adas3xx/adas302/c2outg.pro,v 1.1 2004/07/06 11:48:49 whitefor Exp $ Date $Date: 2004/07/06 11:48:49 $
;
;+
; PROJECT:
;       ADAS IBM MVS to UNIX conversion
;
; NAME:
;	C2OUTG
;
; PURPOSE:
;	Communication with ADAS302 FORTRAN via UNIX pipe and
;	graphics output.
;
; EXPLANATION:
;	The routine begins by reading information from the ADAS302
;	FORTRAN process via a UNIX pipe.  Then the IDL graphical
;	output routine for ADAS302 is invoked.  Communications are to
;	the FORTRAN subroutine C2OUTG.
;
; USE:
;	The use of this routine is specific to ADAS302 see adas302.pro.
;
; INPUTS:
;	DSFULL   - Data file name
;	PIPE	 - The IDL unit number of the bi-directional pipe to the
;		   ADAS302 FORTRAN process.
;
;	UTITLE   - Optional comment by user
;
;	GRPSCAL	 - Integer; 0 if default scaling required 1 if user
;		   specified scaling to be used.
;
;	XMIN	 - String; User specified x-axis minimum, number as string.
;
;	XMAX	 - String; User specified x-axis maximum, number as string.
;
;	YMIN	 - String; User specified y-axis minimum, number as string.
;
;	YMAX	 - String; User specified y-axis maximum, number as string.
;
;	HRDOUT	 - Integer; 0 if no hardcopy required, 1 if hardcopy.
;
;	HARDNAME - String; file name for hardcopy output.
;
;	DEVICE	 - String; IDL name for hardcopy output device.
;
;	HEADER	 - ADAS version header information for inclusion in the
;		   graphical output.
;
; OPTIONAL INPUTS:
;	None.
;
; OUTPUTS:
;	None.
;
; OPTIONAL OUTPUTS:
;	None.
;
; KEYWORD PARAMETERS:
;	FONT	- String; Name of the font to be used for the graphical
;		  output widget.
;
; CALLS:
;	ADAS302_PLOT	ADAS302 graphical output.
;
; SIDE EFFECTS:
;	This routine reads information from FORTRAN via a UNIX pipe.
;
; CATEGORY:
;	Adas system.
;
; WRITTEN:
;       Hugh Summers, University of Strathclyde,  12/11/1996
;
; MODIFIED:
;	1.1	Hugh Summers
;               First edition				
;
; VERSION:
;	1.1	12/11/96
;
;-----------------------------------------------------------------------------

PRO c2outg, dsfull, pipe, utitle, grpscal, xmin, xmax, ymin, ymax,     $
	    hrdout, hardname, device, header, bitfile, gomenu, FONT=font

		;***********************************
                ;**** Set defaults for keywords ****
		;***********************************

    IF NOT (KEYWORD_SET(font)) THEN font = ''

		;*************************************
		;**** Declare variables for input ****
		;*************************************

    sdum = " "
    idum = 0
    fdum = 0.0
    strg = make_array(9, /string, value=" ")
    nmx = 0

		;********************************
		;**** Read data from fortran ****
		;********************************

    readf, pipe, format = '(a40)' , sdum
    title = sdum
    readf, pipe, format = '(a120)', sdum
    titlx = sdum
    readf, pipe, format = '(a80)' , sdum
    titlm = sdum
    readf, pipe, format = '(a8)' , sdum
    date = sdum
    readf, pipe, format = '(a2)' , sdum
    esym = sdum
    readf, pipe, idum
    iz0 = idum
    readf, pipe, idum
    iz = idum
    readf, pipe, format = '(a5)' , sdum
    cprimy = sdum
    readf, pipe, format = '(a5)' , sdum
    csecdy = sdum
    readf, pipe, format = '(a3)' , sdum
    ctype = sdum
    readf, pipe, idum
    ieval = idum

		;**************************************
		;**** now declare array dimensions ****
		;**************************************

    eeva = dblarr(ieval) 
    siaa = dblarr(ieval) 
    for i=0, ieval-1 do begin
        readf, pipe, fdum
        eeva(i) = fdum 
    endfor
    for i=0, ieval-1 do begin
        readf, pipe, fdum
        siaa(i) = fdum 
    endfor
    readf, pipe, idum
    ldef1 = idum
    readf, pipe, idum
    lfsel = idum
    if (lfsel eq 1) then begin
        readf, pipe, idum
        nmx = idum
        siam = fltarr(nmx) 		;**** declare arrays ****
        efitm = fltarr(nmx)
        for i = 0, nmx-1 do begin
	    readf, pipe, fdum
            siam(i) = fdum 
        endfor
        for i = 0, nmx-1 do begin
	    readf, pipe, fdum 
            efitm(i) = fdum
        endfor
    endif
    for i = 0, 1 do begin
        readf, pipe, sdum 
        strg(i) = sdum
    endfor
    for i = 3, 5 do begin
        readf, pipe, sdum 
        strg(i) = sdum
    endfor
    readf, pipe, sdum, format = '(a32)'
    head1 = sdum
    readf, pipe, sdum, format = '(a16)'
    head2 = sdum
    readf, pipe, sdum, format = '(a16)'
    head3 = sdum

		;***********************
		;**** Plot the data ****
		;***********************

    adas302_plot, dsfull, 						$
  		  title , titlx, titlm , utitle,date, esym, iz0, iz, 	$
  		  cprimy, csecdy, ctype, ieval,      	        	$
		  eeva  , siaa , siam  , efitm,                       	$
                  ldef1 , xmin , xmax  , ymin , ymax,                 	$
   		  lfsel , nmx  ,                                      	$
   		  strg  , head1, head2, head3,                        	$
		  hrdout, hardname, device, header, bitfile, gomenu,  	$
                  FONT=font

END
