; Copyright (c) 1995, Strathclyde University .
; SCCS info: Module @(#)$Header: /home/adascvs/idl/adas3xx/adas302/cw_adas302_out.pro,v 1.1 2004/07/06 12:36:49 whitefor Exp $ Date $Date: 2004/07/06 12:36:49 $
;
;+
; PROJECT:
;       ADAS IBM MVS to UNIX conversion
;
; NAME:
;	CW_ADAS302_OUT()
;
; PURPOSE:
;	Produces a widget for ADAS302 output options.
;
; EXPLANATION:
;	This function declares a compound widget consisting of the
;	graphical output selection widget cw_adas_gr_sel.pro, and an 
;	output file widget cw_adas_outfile.pro. The two text output
;	files specified in this widget are for tabular (paper.txt)
;	output and TIA coefficient file. This widget also includes 
;	a button for browsing the comments
;       from the input dataset, a 'Cancel' button , 'Done' button and an
;	The compound widgets cw_adas_gr_sel.pro and cw_adas_outfile.pro
;	included in this file are self managing.  This widget only
;	handles events from the 'Done' and 'Cancel' buttons.
;       'Escape to series menu' button.  This latter is represented by a
;       bitmapped button)
;	This widget only generates events for the 'Done', 'Cancel'
;	and 'Escape to series menu' buttons.
;
;       **** The way the output file logic works: ****
;
;	In c2spf1.pro, the Fortran is told whether a new file must be opened
;	for text output. This happens either if it is the first time we have
;	looked at the output screen with output temperatures selected or if
;	the user has selected 'replace'. Once a file has been opened, the
;	append button is added and set.
;
;	**** The way the TIA file logic works: ****
;
;	Append is always set to false (-1). The TIA data will only be processed
;	once a file has been selected for output and then it will be built up
;	in the Fortran until the user exits 302 when it is written out. The
;	filename may be changed at any time - it is the filename present
;	when the routine is exited that is used. N.B. The user must have
;	pressed 'Done' for any filename to be registered. It is no use changing
;	the name and pressing the menu button on the output screen before
;	pressing 'Done'.
;
; USE:
;	This widget is specific to ADAS302, see adas302_out.pro	for use.
;
; INPUTS:
;	PARENT	- Long integer; ID of parent widget.
;
;	DSFULL	- Name of input dataset for this application.
;
;       BITFILE - String; the path to the dirctory containing bitmaps       
;                 for the 'escape to series menu' button.
;
;	IOSEL	- Integer; 1 => Text output selection made on processing screen
;			   0 => Not made. Corresponds to LOSEL in Fortran.
;
; OPTIONAL INPUTS:
;	None.
;
; OUTPUTS:
;	The return value of this function is the ID of the declared widget.
;
; OPTIONAL OUTPUTS:
;	None.
;
; KEYWORD PARAMETERS:
;	DEVLIST	- A string array;  Each element of the array is the
;		  name of a hardcopy graphics output device.  This
;		  list must mirror the DEVCODE list.  DEVLIST is the
;		  list which the user will see.
;		  e.g ['Post-Script','HP-PCL','HP-GL']
;
;	VALUE	- A structure which determines the initial settings of
;		  the output options widget.  The structure is made
;		  up of two parts.  Each part is the same as the value
;		  structure of one of the two main compound widgets
;		  included in this widget.  See cw_adas_gr_sel and
;		  cw_adas_outfile for more details.  The default value is;
;
;		      { GRPOUT	:	0, 			$
;			GTIT1	:	'', 			$
;			GRPSCAL	:	0, 			$
;			XMIN	:	'',  			$
;			XMAX	:	'',   			$
;			YMIN	:	'',  			$
;			YMAX	:	'',   			$
;			HRDOUT	:	0, 			$
;			HARDNAME:	'', 			$
;			GRPDEF	:	'', 			$
;			GRPFMESS:	'', 			$
;			GRPSEL	:	-1, 			$
;			GRPRMESS:	'', 			$
;			DEVSEL	:	-1, 			$
;			GRSELMESS:	'', 			$
;			TEXOUT	:	0, 			$
;			TEXAPP	:	-1, 			$
;			TEXREP	:	0, 			$
;			TEXDSN	:	'', 			$
;			TEXDEF	:	'',			$
;			TEXMES	:	'', 		 	$
;			TIAOUT	:	0, 			$
;			TIAAPP	:	-1, 			$
;			TIAREP	:	0, 			$
;			TIADSN	:	'', 			$
;			TIADEF	:	'',			$
;			TIAMES	:	''  		 	}
;
;		  For CW_ADAS_GR_SEL;
;			GRPOUT	   Integer; Activation button 1 on, 0 off
;			GTIT1	   String; Graph title
;			GRPSCAL	   Integer; Scaling activation 1 on, 0 off
;			XMIN	   String; x-axis minimum, string of number
;			XMAX	   String; x-axis maximum, string of number
;			YMIN	   String; y-axis minimum, string of number
;			YMAX	   String; y-axis maximum, string of number
;			HRDOUT	   Integer; Hard copy activ' 1 on, 0 off
;			HARDNAME   String; Hard copy output file name
;			GRPDEF	   String; Default output file name 
;			GRPFMESS   String; File name error message
;			GRPSEL	   Integer; index of selected graph in GRPLIST
;			GRPRMESS   String; Scaling ranges error message
;			DEVSEL	   Integer; index of selected device in DEVLIST
;			GRSELMESS  String; General error message
;
;		  For CW_ADAS_OUTFILE tabular output (paper.txt);
;			TEXOUT	Integer; Activation button 1 on, 0 off
;			TEXAPP	Integer; Append button 1 on, 0 off, -1 no button
;			TEXREP	Integer; Replace button 1 on, 0 off, 
;					-1 no button
;			TEXDSN	String; Output file name
;			TEXDEF	String; Default file name
;			TEXMES	String; file name error message
;
;		  For CW_ADAS_OUTFILE tia coefficient file
;			TIAOUT  Integer; Activation button 1 on, 0 off
;			TIAAPP	Integer; Append button 1 on, 0 off, -1 no button
;			TIAREP	Integer; Replace but' 1 on, 0 off, -1 no button
;			TIADSN	String; Output file name
;			TIADEF	String; Default file name
;			TIAMES	String; file name error message
;
;	UVALUE	- A user value for this widget.
;
;	FONT	- String; the name of a font to be used for all text in
;		  this widget.  Default to system default font.
;
; CALLS:
;	CW_ADAS_DSBR	Input dataset name and comments browse button.
;	CW_ADAS_GR_SEL	Graphical output selection widget.
;	CW_ADAS_OUTFILE	Output file name entry widget.
;	See side effects for other related widget management routines.
;
; SIDE EFFECTS:
;
; 	Two other routines are included in this file
;	OUT506_GET_VAL()
;	OUT506_EVENT()
;
; CATEGORY:
;	Compound Widget
;
; WRITTEN:
;       Hugh Summers, University of Strathclyde, 12/11/96         
;
; MODIFIED:
;	1.1	Hugh Summers
;   	        First version
;
; VERSION:
;	1.1	12/11/96
;
;-----------------------------------------------------------------------------

FUNCTION out302_get_val, id

  ;---------------------------------------------------------------------------
  ;	Return to caller on error
  ;---------------------------------------------------------------------------

  ON_ERROR, 2

  ;---------------------------------------------------------------------------
  ;	Retrieve the state
  ;---------------------------------------------------------------------------

  parent=widget_info(id,/parent)
  widget_control, parent, get_uvalue=state, /no_copy

  ;---------------------------------------------------------------------------
  ;	Get graphical output settings
  ;---------------------------------------------------------------------------

  widget_control,state.grpid,get_value=grselval

  ;---------------------------------------------------------------------------
  ;	Get text output settings
  ;---------------------------------------------------------------------------

  widget_control,state.paperid,get_value=papos

  ;---------------------------------------------------------------------------
  ;	Get tia coefficient file settings
  ;---------------------------------------------------------------------------

  widget_control,state.contid,get_value=tiaos

    os = { 	out302_set, 						$
	   	GRPOUT		:	grselval.outbut, 		$
		GTIT1		:	grselval.gtit1, 	 	$
 		GRPSCAL		:	grselval.scalbut,		$
	   	XMIN		:	grselval.xmin,     		$
		XMAX		:	grselval.xmax,    	 	$
	   	YMIN		:	grselval.ymin,     		$
		YMAX		:	grselval.ymax,    	 	$
	   	HRDOUT		:	grselval.hrdout, 		$
		HARDNAME	:	grselval.hardname,   		$
	   	GRPDEF		:	grselval.grpdef, 		$
		GRPFMESS	:	grselval.grpfmess,   		$
	   	GRPSEL		:	grselval.grpsel, 		$
		GRPRMESS	:	grselval.grprmess,   		$
	   	DEVSEL		:	grselval.devsel, 		$
		GRSELMESS	:	grselval.grselmess, 		$
	   	TEXOUT		:	papos.outbut,  			$
		TEXAPP		:	papos.appbut, 		 	$
	   	TEXREP		:	papos.repbut,  			$
		TEXDSN		:	papos.filename, 	 	$
	   	TEXDEF		:	papos.defname, 			$
		TEXMES		:	papos.message, 		 	$
	   	TIAOUT		:	tiaos.outbut,  			$
		TIAAPP		:	tiaos.appbut, 		 	$
	   	TIAREP		:	tiaos.repbut,  			$
		TIADSN		:	tiaos.filename, 	 	$
	   	TIADEF		:	tiaos.defname, 			$
		TIAMES		:	tiaos.message 		 	}

  ;---------------------------------------------------------------------------
  ;	Return the state
  ;---------------------------------------------------------------------------

  widget_control, parent, set_uvalue=state, /no_copy

  RETURN, os 

END

;-----------------------------------------------------------------------------

FUNCTION out302_event, event

  COMMON cw302out_blk, cwoval

  ;---------------------------------------------------------------------------
  ;	Base ID of compound widget
  ;---------------------------------------------------------------------------

  parent=event.top

  ;---------------------------------------------------------------------------
  ;	Retrieve the state
  ;---------------------------------------------------------------------------

  widget_control, parent, get_uvalue=state, /no_copy

  ;---------------------------------------------------------------------------
  ;	Clear previous messages
  ;---------------------------------------------------------------------------

  widget_control,state.messid,set_value=' '

  ;---------------------------------------------------------------------------
  ;	Process events
  ;---------------------------------------------------------------------------

  CASE event.id OF

  ;---------------------------------------------------------------------------
  ;	Cancel button
  ;---------------------------------------------------------------------------

    state.cancelid: new_event = {ID:parent, TOP:event.top, 		$
				HANDLER:0L, ACTION:'Cancel'}

  ;---------------------------------------------------------------------------
  ;	Done button
  ;---------------------------------------------------------------------------

    state.doneid: begin

  ;---------------------------------------------------------------------------
  ;	Return the state before checking can start 
  ;	with the get_value keyword.                
  ;---------------------------------------------------------------------------

          widget_control, parent, set_uvalue=state, /no_copy

  ;---------------------------------------------------------------------------
  ;	Check for errors in the input
  ;---------------------------------------------------------------------------

	  error = 0

  ;---------------------------------------------------------------------------
  ;	Get a copy of the widget value
  ;---------------------------------------------------------------------------

	  widget_control,event.handler,get_value=os
	
  ;---------------------------------------------------------------------------
  ;	Check for widget error messages
  ;---------------------------------------------------------------------------

	  if os.grpout eq 1 and strtrim(os.grpfmess) ne '' then error=1
	  if os.grpout eq 1 and os.grpscal eq 1 and $
				strtrim(os.grprmess) ne '' then error=1
	  if os.grpout eq 1 and strtrim(os.grselmess) ne '' then error=1
	  if os.texout eq 1 and strtrim(os.texmes) ne '' then error=1
 	  if os.tiaout eq 1 and strtrim(os.tiames) ne '' then error=1

  ;---------------------------------------------------------------------------
  ;	Retrieve the state 
  ;---------------------------------------------------------------------------

          widget_control, parent, get_uvalue=state, /no_copy

	  if error eq 1 then begin
	    widget_control,state.messid,set_value= 			$

		'**** Error in output settings ****'
	    new_event = 0L
	  endif else begin
	    new_event = {ID:parent, TOP:event.top,			$
			 HANDLER:0L, ACTION:'Done'}
	  endelse
        end

		;**** Menu button ****

	state.outid: begin
	    new_event = {ID:parent, TOP:event.top, HANDLER:0L,      	$
                         ACTION:'Menu'}
	end

    ELSE: new_event = 0L

  ENDCASE

  ;---------------------------------------------------------------------------
  ;	Return the state
  ;---------------------------------------------------------------------------

          widget_control, parent, set_uvalue=state, /no_copy

  RETURN, new_event
END

;-----------------------------------------------------------------------------

FUNCTION cw_adas302_out, parent, dsfull, bitfile, iosel, DEVLIST=devlist, $
		VALUE=value, UVALUE=uvalue, FONT=font

  COMMON cw302out_blk, cwoval

  IF (N_PARAMS() EQ 0) THEN MESSAGE, 'Must specify parent for cw_adas302_out'
  ON_ERROR, 2					;return to caller

  cwoval=value

  ;---------------------------------------------------------------------------
  ;	Set defaults for keywords
  ;---------------------------------------------------------------------------

  IF NOT (KEYWORD_SET(devlist)) THEN devlist = ''
  IF NOT (KEYWORD_SET(uvalue)) THEN uvalue = 0
  IF NOT (KEYWORD_SET(font)) THEN font = ''
  IF NOT (KEYWORD_SET(value)) THEN begin
	os = {	out302_set, 						$
		GRPOUT		:	0, 				$
		GTIT1		:	'', 				$
		GRPSCAL		:	0, 				$
		XMIN		:	'',  				$
		XMAX		:	'',   				$
		YMIN		:	'',  				$
		YMAX		:	'',   				$
		HRDOUT		:	0, 				$
		HARDNAME	:	'', 				$
		GRPDEF		:	'', 				$
		GRPFMESS	:	'', 				$
		GRPSEL		:	-1, 				$
		GRPRMESS	:	'', 				$
		DEVSEL		:	-1, 				$
		GRSELMESS	:	'', 				$
		TEXOUT		:	0, 				$
		TEXAPP		:	-1, 				$
		TEXREP		:	0, 				$
		TEXDSN		:	'', 				$
         	TEXDEF		:	'paper.txt', 			$
		TEXMES		:	'',  				$
		TIAOUT		:	0, 				$
		TIAAPP		:	-1, 				$
		TIAREP		:	0, 				$
		TIADSN		:	'', 				$
         	TIADEF		:	'tiaout.pass', 			$
		TIAMES		:	''  				}
  ENDIF ELSE BEGIN
	os = {	out302_set, 						$
		GRPOUT		:	value.grpout, 			$
		GTIT1		:	value.gtit1, 			$
		GRPSCAL		:	value.grpscal, 			$
		XMIN		:	value.xmin, 			$
		XMAX		:	value.xmax,   			$
		YMIN		:	value.ymin,     		$
		YMAX		:	value.ymax,   			$
		HRDOUT		:	value.hrdout, 			$
		HARDNAME	:	value.hardname, 		$
		GRPDEF		:	value.grpdef, 			$
		GRPFMESS	:	value.grpfmess, 		$
		GRPSEL		:	value.grpsel, 			$
		GRPRMESS	:	value.grprmess, 		$
		DEVSEL		:	value.devsel, 			$
		GRSELMESS	:	value.grselmess, 		$
		TEXOUT		:	value.texout, 			$
		TEXAPP		:	value.texapp, 			$
		TEXREP		:	value.texrep, 			$
		TEXDSN		:	value.texdsn, 			$
		TEXDEF		:	value.texdef, 			$
		TEXMES		:	value.texmes, 			$
		TIAOUT		:	value.tiaout, 			$
		TIAAPP		:	value.tiaapp, 			$
		TIAREP		:	value.tiarep, 			$
		TIADSN		:	value.tiadsn, 			$
		TIADEF		:	value.tiadef, 			$
		TIAMES		:	value.tiames 			}
  ENDELSE

  ;---------------------------------------------------------------------------
  ;	Create the 302 Output options widget
  ;---------------------------------------------------------------------------
  ;---------------------------------------------------------------------------
  ;	create base widget
  ;---------------------------------------------------------------------------

  cwid = widget_base( parent, UVALUE = uvalue, 				$
			EVENT_FUNC = "out302_event", 			$
			FUNC_GET_VALUE = "out302_get_val", 		$
			/COLUMN)

  ;---------------------------------------------------------------------------
  ;	Add dataset name and browse button
  ;---------------------------------------------------------------------------

  rc = cw_adas_dsbr(cwid,dsfull,font=font)

  ;---------------------------------------------------------------------------
  ;	Widget for graphics selection
  ;	Note change in names for GRPOUT and GRPSCAL
  ;---------------------------------------------------------------------------


    grselval = {  	OUTBUT		:	os.grpout, 		$
			GTIT1		:	os.gtit1, 		$
			SCALBUT		:	os.grpscal, 		$
			XMIN		:	os.xmin, 		$
			XMAX		:	os.xmax, 		$
			YMIN		:	os.ymin, 		$
			YMAX		:	os.ymax, 		$
			HRDOUT		:	os.hrdout, 		$
			HARDNAME	:	os.hardname, 		$
			GRPDEF		:	os.grpdef, 		$
			GRPFMESS	:	os.grpfmess, 		$
			GRPSEL		:	os.grpsel, 		$
			GRPRMESS	:	os.grprmess, 		$
			DEVSEL		:	os.devsel, 		$
			GRSELMESS	:	os.grselmess 		}
  base = widget_base(cwid,/row,/frame)
  grpid = cw_adas_gr_sel(base, /SIGN, OUTPUT='Graphical Output', 	$
			 DEVLIST=devlist, VALUE=grselval, FONT=font 	$
			)

  ;---------------------------------------------------------------------------
  ;	Widget for text output
  ;---------------------------------------------------------------------------

    papfval = { 	OUTBUT		:	os.texout, 		$
			APPBUT		:	os.texapp,		$
			REPBUT		:	os.texrep, 		$
	        	FILENAME	:	os.texdsn, 		$
			DEFNAME		:	os.texdef, 		$
			MESSAGE		:	os.texmes 		}
  base = widget_base(cwid,/row,/frame)
  paperid = cw_adas_outfile(base, OUTPUT='Text Output', 		$
                        VALUE=papfval, FONT=font)

  ;**** If losel wasn't set in Fortran, make the text output base insensitive

  widget_control, paperid, sensitive = iosel

  ;---------------------------------------------------------------------------
  ;	Widget for tia file
  ;---------------------------------------------------------------------------

    tiafval = { 	OUTBUT		:	os.tiaout, 		$
			APPBUT		:	-1, 			$
			REPBUT		:	os.tiarep, 		$
	        	FILENAME	:	os.tiadsn, 		$
			DEFNAME		:	os.tiadef, 		$
			MESSAGE		:	os.tiames 		}
  base = widget_base(cwid,/column,/frame)
  contid = cw_adas_outfile(base, OUTPUT='TIA Coefft. File', 		$
                        VALUE=tiafval, FONT=font)
  mess = widget_label(base,value='N.B. Only when you exit ADAS 302'+$
		  ' will TIA output be sent to this file',font=font)

  ;---------------------------------------------------------------------------
  ;	Error message
  ;---------------------------------------------------------------------------

  messid = widget_label(cwid,value=' ',font=font)

  ;---------------------------------------------------------------------------
  ;	add the exit buttons
  ;---------------------------------------------------------------------------

  base = widget_base(cwid,/row)
  menufile = bitfile + '/menu.bmp'
  read_X11_bitmap, menufile, bitmap1
  outid = widget_button(base, value=bitmap1)		;menu button
  cancelid = widget_button(base,value='Cancel',font=font)
  doneid = widget_button(base,value='Done',font=font)
  
  ;---------------------------------------------------------------------------
  ;	create a state structure for the pop-up window.
  ;---------------------------------------------------------------------------


    new_state = { 	GRPID		:	grpid, 			$
			PAPERID		:	paperid,          	$
		  	CONTID		:	contid, 		$
		  	CANCELID	:	cancelid, 		$
			DONEID		:	doneid, 		$
			MESSID		:	messid,   		$
			OUTID		:	outid   		}

  ;---------------------------------------------------------------------------
  ;	Save initial state structure
  ;---------------------------------------------------------------------------

  widget_control, parent, set_uvalue=new_state, /no_copy
  RETURN, cwid

END

