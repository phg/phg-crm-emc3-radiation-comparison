; Copyright (c) 1995, Strathclyde University .
; SCCS info: Module @(#)$Header: /home/adascvs/idl/adas3xx/adas302/c2setp.pro,v 1.1 2004/07/06 11:48:55 whitefor Exp $ Date $Date: 2004/07/06 11:48:55 $
;
; PROJECT:
;       ADAS IBM MVS to UNIX conversion
;
; NAME:
;	C2SETP
;
; PURPOSE:
;	IDL communications with ADAS302 FORTRAN process via pipe.
;
; EXPLANATION:
;	This IDL routine reads data from the ADAS302 FORTRAN subroutine
;	C2SETP via a UNIX pipe.  In the original IBM version the FORTRAN
;	subroutine C2SETP put information needed by the ISPF interface
;	into the ISPF pool area for later use.  Now the same information
;	is written to IDL for use in its user unterface.
;
; USE:
;	The use of this routine is specific to ADAS302.  See
;	adas302.pro for an example.
;
; INPUTS:
;	PIPE	- The IDL unit number of the bi-directional pipe to the
;		  ADAS302 FORTRAN process.
;
; OPTIONAL INPUTS:
;	None
;
; OUTPUTS:
;       NBLKS        - Number of data sub-blocks read in.
;
; OPTIONAL OUTPUTS:
;	None
;
; KEYWORD PARAMETERS:
;	None
;
; CALLS:
;	None
;
; SIDE EFFECTS:
;	Reads data from a pipe.
;
; CATEGORY:
;	Adas system.
;
; WRITTEN:
;       Hugh Summers, University of Strathclyde, 12/11/1996
;
; MODIFIED:
;	1.1		Hugh Summers
;			First version
;
; VERSION:
;	1.1		12-11-96
;
;-----------------------------------------------------------------------------

PRO c2setp, pipe, nbsel

		;**********************************
		;**** Initialise new variables ****
		;**********************************

    idum  = 0

		;********************************
		;**** Read data from fortran ****
		;********************************

    readf, pipe, idum
    nbsel = idum

END
