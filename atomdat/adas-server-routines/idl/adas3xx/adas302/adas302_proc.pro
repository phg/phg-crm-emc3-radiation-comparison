; Copyright (c) 1995, Strathclyde University .
; SCCS info: Module @(#)$Header: /home/adascvs/idl/adas3xx/adas302/adas302_proc.pro,v 1.1 2004/07/06 10:27:55 whitefor Exp $ Date $Date: 2004/07/06 10:27:55 $
;
;+
; PROJECT:
;       ADAS IBM MVS to UNIX conversion
;
; NAME:
;	ADAS302_PROC
;
; PURPOSE:
;	IDL ADAS user interface, processing options/input.
;
; EXPLANATION:
;	This routine creates and manages a pop-up window which allows
;	the user to select options and input data to control ADAS302
;	processing.
;
; USE:
;	This routine is ADAS302 specific, see c2ispf.pro for how it
;	is used.
;
; INPUTS:
;	PROCVAL	- A structure which determines the initial settings of
;		  the processing options widget.  The value is passed
;		  unmodified into cw_adas302_proc.pro.
;
;		  See cw_adas302_proc.pro for a full description of this
;		  structure.
;
;	DSFULL	- String; The full system file name of the input 
;		          dataset selected by the user for processing.
;
;	NSTORE  - Integer: maximum number of data blocks which can be
;			   read from the data set.
;
;	NEDIM	- Integer : Maximum number of energies allowed.
;
;	NDEIN	- Integer : Maximum number of user input energies allowed.
;
;	NDTIN	- Integer : Maximum number of user input temps. allowed.
;
;	NBSEL   - Integer : number of data blocks accepted and read-in
;
;	CPRIMY  - String array : Primary species for each data blocks
;				 dimension data block index (NSTORE).
;	CSECDY  - String array : Secondary species for each data blocks.
;				 dimension data block index (NSTORE).
;	CTYPE   - String array : DATA type  for each data block
;				 dimension data block index (NSTORE)
;	IEA     - Integer array: Number of energies
;				 dimension data block index (NSTORE)
;	EVALS   - Double array : Energies - 
;				 1st dimension - energy index
;				 2nd dimension - units 1 - cm s-1
;						       2 - at. units
;						       3 - eV/amu
;				 3rd dimension - data block index
;
;       BITFILE - String; the path to the directory containing bitmaps
;                 for the 'escape to series menu' button.
;
; OPTIONAL INPUTS:
;	None
;
; OUTPUTS:
;	PROCVAL	- On output the structure records the final settings of
;		  the processing selections widget if the user pressed the
;		  'Done' button, otherwise it is not changed from input.
;
;	ACT	- String; 'Done', 'Cancel' or 'Menu' for the button the
;		  user pressed to terminate the processing options
;		  window.
;
; OPTIONAL OUTPUTS:
;	None
;
; KEYWORD PARAMETERS:
;	FONT_LARGE - The name of a larger font.
;
;	FONT_SMALL - The name of a smaller font.
;
;	EDIT_FONTS - A structure of two fonts used in the table editor
;		     adas_edtab.pro, {font_norm:'',font_input:''}.
;
; CALLS:
;	CW_ADAS302_PROC	Declares the processing options widget.
;	DYNLABEL	Recursively adds dynamic_resize keyword to label
;			widgets for version 4.0.1 and higher of IDL
;	ADAS302_PROC_EV Called indirectly during widget management,
;			routine included in this file.
;	XMANAGER
;
; SIDE EFFECTS:
;       This widget uses a common block PROC302_BLK, to pass the
;       variables VALUE and ACTION between the two routines. 
;
; CATEGORY:
;	Widgets
;
; WRITTEN:
;       H. P. Summers, University of Strathclyde, 12-Nov-1996
;
; MODIFIED:
;       1.1     Hugh Summers
;               First Release
;
; VERSION:
;	1.1	12-11-96
;
;-----------------------------------------------------------------------------

PRO adas302_proc_ev, event

    COMMON proc302_blk, action, value

    action = event.action
    CASE event.action OF

		;**** 'Done' button ****

	'Done'  : begin

		;**** Get the output widget value ****

	    widget_control, event.id, get_value=value 
            widget_control, event.top, /destroy
	end

		;**** 'Cancel' button ****

	'Cancel': widget_control, event.top, /destroy

                ;**** 'Menu' button ****

        'Menu': widget_control, event.top, /destroy

        ELSE:                   ;**** Do nothing ****

    ENDCASE

END

;-----------------------------------------------------------------------------

PRO adas302_proc, procval, dsfull, nstore, nedim, ndein, ndtin, nbsel, 	$
		  cprimy, csecdy, ctype, iea, evals, 	 		$
		  act, bitfile, FONT_LARGE=font_large, 			$
                  FONT_SMALL=font_small, EDIT_FONTS=edit_fonts


		;**** declare common variables ****

    COMMON proc302_blk, action, value

		;**** Copy "procval" to common ****

    value = procval

		;**** Set defaults for keywords ****

    IF NOT (KEYWORD_SET(font_large)) THEN font_large = ''
    IF NOT (KEYWORD_SET(font_small)) THEN font_small = ''
    IF NOT (KEYWORD_SET(edit_fonts)) THEN 				$
    edit_fonts = { font_norm:'', font_input:''}
		
                ;**** create base widget ****

    procid = widget_base(TITLE='ADAS302 PROCESSING OPTIONS', 		$
			 XOFFSET=50, YOFFSET=1)

		;**** Declare processing widget ****

    cwid = cw_adas302_proc(procid, dsfull, act, nstore,			$
			   nedim,  ndein, ndtin, nbsel, 		$
			   cprimy, csecdy, ctype, 		$
			   iea, evals, 					$
			   bitfile, PROCVAL=value, UVALUE=uvalue,	$
		 	   FONT_LARGE=font_large, FONT_SMALL=font_small,$
			   EDIT_FONTS=edit_fonts)

		;**** Realize the new widget ****

    dynlabel, procid
    widget_control, procid, /realize

		;**** make widget modal ****

    xmanager, 'adas302_proc', procid, event_handler='adas302_proc_ev', 	$
	      /modal, /just_reg

		;*** copy value back to procval for return to c2ispf ***

    act = action
    procval = value
 
END

