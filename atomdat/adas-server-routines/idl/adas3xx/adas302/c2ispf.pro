; Copyright (c) 1995, Strathclyde University .
; SCCS info: Module @(#)$Header: /home/adascvs/idl/adas3xx/adas302/c2ispf.pro,v 1.1 2004/07/06 11:48:40 whitefor Exp $ Date $Date: 2004/07/06 11:48:40 $
;
;+
; PROJECT:
;       ADAS IBM MVS to   UNIX conversion
;
; NAME:
;	C2ISPF
;
; PURPOSE:
;	IDL user interface and communications with ADAS302 FORTRAN
;	process via pipe.
;
; EXPLANATION:
;	The routine begins by reading information from the ADAS302
;	FORTRAN process via a UNIX pipe.  Then part of the ADAS302
;	IDL user interface is invoked to determine how the user
;	wishes to process the input dataset.  When the user's
;	interactions are complete the information gathered with
;	the user interface is written back to the FORTRAN process
;	via the pipe.  Communications are to the FORTRAN subroutine
;	C2ISPF.
;
; USE:
;	The use of this routine is specific to ADAS302, see adas302.pro.
;
; INPUTS:
;	PIPE	- The IDL unit number of the bi-directional pipe to the
;		  ADAS302 FORTRAN process.
;
;	LPEND	- Integer, 0 if the user pressed the 'Done' button
;		  or 1 if the user pressed 'Cancel'.  Maps directly onto
;		  the logical variable LPEND in ADAS302 FORTRAN.
;
;	PROCVAL	- A structure which determines the initial settings of
;		  the processing options widget.  The initial value is
;		  set in adas302.pro.  If adas302.pro passes a blank 
;		  dummy structure of the form {NEW:-1} into PROCVAL then
;		  PROCVAL is reset to a default structure.
;
;		  The PROCVAL structure is;
;			procval = {                        	$
;                			new  : 0 ,              $
;                			title : '',             $
;                			ibsel : 0 ,             $
;                			amd   : 2,              $
;					amr   : 2, 		$
;					ieval : 0, 		$
;                			ietyp : 1,              $
;                			ein   : temp_arr,       $
;					itval : 0,    		$ 	
;                			ittyp : 1,              $
;                			tin   : temp_arr,       $
;                			lfsel : 0,              $
;					tolval: '5',            $
;                			losel : 0,              $
;                			ltsel : 0               $
;              			}
;
;
;		  See cw_adas302_proc.pro for a full description of this
;		  structure.
;	
;
;	DSFULL	- String; The full system file name of the input 
;		  dataset selected by the user for processing.
;
;       BITFILE - String; the path to the dirctory containing bitmaps
;                 for the 'escape to series menu' button.
;
; OPTIONAL INPUTS:
;	None
;
; OUTPUTS:
;	PROCVAL	- On output the structure records the final settings of
;		  the processing selections widget if the user pressed the
;		  'Done' button, otherwise it is not changed from input.
;
;       LPEND   - Integer, 0 if the user pressed the 'Done' button
;                 or 1 if the user pressed 'Cancel'.  Maps directly onto
;                 the logical variable LPEND in ADAS302 FORTRAN.
;
;       GOMENU  - Int; flag - set to 1 if user has selected 'escape direct
;                 to series menu , otherwise 0
;
; OPTIONAL OUTPUTS:
;	None
;
; KEYWORD PARAMETERS:
;	FONT_LARGE - The name of a larger font.
;
;	FONT_SMALL - The name of a smaller font.
;
;	EDIT_FONTS - A structure of two fonts used in the table editor
;		     adas_edtab.pro, {font_norm:'',font_input:''}.
;
; CALLS:
;	ADAS302_PROC	Invoke the IDL interface for ADAS302 data
;		 	processing options/input.
;
; SIDE EFFECTS:
;	Two way communications with ADAS302 FORTRAN via the
;	bi-directional UNIX pipe.
;
; CATEGORY:
;	Adas system.
;
; WRITTEN:
;       Hugh P. Summers, University of Strathclyde, 07-Mar-1996
;
; MODIFIED:
;	1.1	Hugh Summers
;               First release
;
; VERSION:
;	1.1	07-03-96
;
;-
;-----------------------------------------------------------------------------

PRO c2ispf, pipe, lpend, procval, dsfull, nbsel, gomenu, bitfile,	$
	    FONT_LARGE=font_large, FONT_SMALL=font_small, 		$
	    EDIT_FONTS=edit_fonts


                ;**** Set defaults for keywords ****

    IF NOT (KEYWORD_SET(font_large)) THEN font_large = ''
    IF NOT (KEYWORD_SET(font_small)) THEN font_small = ''
    IF NOT (KEYWORD_SET(edit_fonts)) THEN 				$
    edit_fonts = {font_norm:'',font_input:''}

		;********************************************
		;****     Declare variables for input    ****
		;**** arrays will be declared after sizes****
		;**** have been read.                    ****
                ;********************************************

    lpend = 0
    nstore= 0
    nedim = 0
    ndein = 0
    ndtin = 0
    nbsel = 0
    input = 0

		;********************************
		;**** Read data from fortran ****
		;********************************

    readf, pipe, input
    nstore = input
    readf, pipe, input
    nedim = input
    readf, pipe, input
    ndein = input
    readf, pipe, input
    ndtin = input
    readf, pipe, input
    nbsel = input

		;******************************************
		;**** Now can define other array sizes ****
		;******************************************

    cprimy = strarr(nbsel)
    csecdy = strarr(nbsel)
    ctype  = strarr(nbsel)
    iea    = intarr(nbsel)
    evals  = dblarr(nedim, 3, nbsel)
    tvals  = dblarr(nedim, 3, nbsel)

		;********************************
                ;**** Read data from fortran ****
                ;********************************

    next_item = 0.0
    sdum = ' '
    for j=0, nbsel-1 do begin
        readf, pipe, sdum
        cprimy(j) = sdum
    endfor
    for j=0, nbsel-1 do begin
        readf, pipe, sdum
        csecdy(j) = sdum
    endfor
    for j=0, nbsel-1 do begin
        readf, pipe, sdum
        ctype(j) = sdum
    endfor
    for j=0, nbsel-1 do begin
        readf, pipe, input
        iea(j) = input
    endfor
    for k = 0, nbsel-1 do begin 
        for j =  0 , 2 do begin
	    for i = 0, iea(k)-1 do begin
                readf, pipe, next_item
                evals(i,j,k) = next_item
            endfor
        endfor
    endfor

		;********************************************
		;**** Set default value if none provided ****
		;********************************************

    if (procval.new lt 0) then begin
         ener_arr = fltarr(ndein)
         temp_arr = fltarr(ndtin)
         procval = {	new  	: 	0 ,             		$
			title 	: 	'',				$
			ibsel 	: 	0 ,				$
       			amd     : 	2.0,		                $
			amr     :	2.0, 				$
			ieval  	: 	0,              		$
			ietyp  	: 	1,              		$
			ein   	: 	ener_arr,			$
			itval  	: 	0,              		$
			ittyp  	: 	1,              		$
			tin   	: 	temp_arr,			$
			lfsel 	: 	0,				$
			tolval	: 	'5',				$
                	losel 	: 	0,               		$
                 	ltsel 	: 	0               		}
   endif

		;****************************************
		;**** Pop-up processing input widget ****
		;****************************************

    adas302_proc, procval, dsfull, nstore, 				$
		  nedim, ndein, ndtin, nbsel,  				$
		  cprimy, csecdy, ctype, 		        	$
		  iea, evals, action, bitfile,				$
		  FONT_LARGE=font_large, FONT_SMALL=font_small, 	$
		  EDIT_FONTS=edit_fonts


		;********************************************
		;****  Act on the event from the widget  ****
                ;**** There are three  possible  actions ****
                ;**** 'Done', 'Cancel'and 'Menu'.        ****
		;********************************************

    if action eq 'Done' then begin
        lpend = 0
    endif else if action eq 'Menu' then begin
        lpend = 0
        gomenu = 1
    endif else begin
        lpend = 1
    endelse

		;*******************************
		;**** Write data to fortran ****
		;*******************************

    printf, pipe, lpend   
    title_short = strmid(procval.title,0,39) 	
    printf, pipe, title_short, format='(a40)' 
    printf, pipe, procval.ibsel  			
    printf, pipe, procval.amd
    printf, pipe, procval.amr
    printf, pipe, procval.ietyp
    printf, pipe, procval.ieval

    for j = 0, procval.ieval-1 do begin
        printf, pipe, procval.ein(j)
    endfor

    printf, pipe, procval.ittyp
    printf, pipe, procval.itval

    for j = 0, procval.itval-1 do begin
        printf, pipe, procval.tin(j)
    endfor

    printf, pipe, procval.lfsel
    tolval = float(procval.tolval)
    printf, pipe, tolval
    printf, pipe, procval.losel
    printf, pipe, procval.ltsel

END
