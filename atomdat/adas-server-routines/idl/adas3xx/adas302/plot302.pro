; Copyright (c) 1995, Strathclyde University .
; SCCS info: Module @(#)$Header: /home/adascvs/idl/adas3xx/adas302/plot302.pro,v 1.1 2004/07/06 14:31:01 whitefor Exp $ Date $Date: 2004/07/06 14:31:01 $
;
;+
; PROJECT:
;       ADAS IBM MVS to UNIX conversion
;
; NAME:
;	PLOT302
;
; PURPOSE:
;	Plot one graph for ADAS302.
;
; EXPLANATION:
;	This routine plots ADAS302 output for a single graph.
;
; USE:
;	Use is specific to ADAS302.  See adas302_plot.pro for
;	example.
;
; INPUTS:
;	X	- Double array; list of x values.
;
;	Y	- 2D double array; y values, 2nd index ordinary level,
;		  indexed by iord1 and iord2.
;
;	ITVAL   - Integer : Number of data points and spline fit points
;
;	NPLOTS  - Integer : Number of plots  1: no minimax fit, 2 : minimax fit
;
;	NMX     - Integer : Number of points used in polynomial fit.
;
;	TITLE	- String array : General title for program run. 5 lines.
;
;	XTITLE  - String : title for x-axis annotation
;
;	YTITLE  - String : title for y-axis annotation
;
;	STRG    - String array; 6 lines of info regarding data.
;                               ' ELEMENT SYMBOL           = '
;                               ' NUCLEAR CHARGE           = '
;                               '    IDENTIFICATION        = '
;                               '    FINAL CHARGE          = '
;                               '    FINAL STATE           = '
;                               '                          = '
;
;	HEAD1   - String : '----- REACTION INFORMATION -----' 
;
;	HEAD2   - String : ' DONOR:         '  
;
;	HEAD3   - String : ' RECEIVER:      '
;
;	EEVA    - fltarr : energy values - for display only.
;
;	LDEF1	- Integer; 1 if user specified axis limits to be used, 
;		  0 if default scaling to be used.
;
;	XMIN	- String; Lower limit for x-axis of graph, number as string.
;
;	XMAX	- String; Upper limit for x-axis of graph, number as string.
;
;	YMIN	- String; Lower limit for y-axis of graph, number as string.
;
;	YMAX	- String; Upper limit for y-axis of graph, number as string.
;
; OPTIONAL INPUTS:
;	None.
;
; OUTPUTS:
;	None.
;
; OPTIONAL OUTPUTS:
;	None.
;
; KEYWORD PARAMETERS:
;	None.
;
; CALLS:
;	None.
;
; SIDE EFFECTS:
;	None.
;
; CATEGORY:
;	Adas system.	
;
; WRITTEN:
;       Hugh Summers, University of Strathclyde,  12/11/1996
;
; MODIFIED:
;	1.1	Hugh Summers
;		First version
;
; VERSION:
;	1.1	12-11-96
;	
;----------------------------------------------------------------------------

PRO plot302, x , y, ieval, nplots, nmx, title, xtitle, ytitle, 		$
             strg, head1, head2, head3, eeva,                  		$
             ldef1, xmin, xmax, ymin, ymax

    COMMON Global_lw_data, left, right, top, bottom, grtop, grright

		;****************************************************
		;**** Suitable character size for current device ****
		;**** Aim for 60 characters in y direction.      ****
		; *** also reduce a little to get on page        ****
		;****************************************************

    charsize = (!d.y_vsize/!d.y_ch_size)/60.0 
    small_check = GETENV('VERY_SMALL')
    if small_check eq 'YES' then charsize=charsize*0.8

		;******************************
		;**** set makeplot counter ****
		;******************************

    makeplot = 1

		;***************************************
		;**** Construct graph title         ****
		;**** "!C" is the new line control. ****
		;***************************************

    !p.font=-1
    if small_check eq 'YES' then begin
        gtitle =  title(0) + "!C!C" + title(1) + "!C!C" + title(2)+"!C!C" + $
          title(3) + "!C!C" + title(4)
    endif else begin
        gtitle =  title(0) + "!C!C" + title(1) + "!C" + title(2)+"!C" + $
          title(3) + "!C" + title(4)
    endelse
		;**********************************************
		;**** Find x and y ranges for auto scaling ****
		;**********************************************
  
    if ldef1 eq 0 then begin

		;********************************************
		;**** identify values in the valid range ****
		;**** plot routines only work within     ****
		;**** single precision limits.	         ****
		;********************************************

        xvals = where(x gt 1.0e-37 and x lt 1.0e37)
        yvals = where(y gt 1.0e-37 and y lt 1.0e37)
        if xvals(0) gt -1 then begin
            xmax = max(x(xvals), min=xmin)
            makeplot = 1
        endif else begin
            makeplot = 0
        endelse
        if yvals(0) gt -1 then begin
            ymax = max(y(yvals), min=ymin)
            makeplot = 1
        endif else begin
            makeplot = 0
        endelse
        style = 0
    endif else begin

		;********************************************************
		;**** Check that at least some data in in axes range ****
		;********************************************************

        xvals = where(x ge xmin and x le xmax)
        yvals = where(y ge ymin and y le ymax)
        if xvals(0) eq -1 or yvals(0) eq -1 then begin
            makeplot = 0
        endif
        style = 1
    endelse

		;**************************************
		;**** Set up log-log plotting axes ****
		;**************************************

    plot_oo, [xmin,xmax], [ymin,ymax], /nodata, ticklen=1.0, 		$
	     position=[left,bottom,grright,grtop], xtitle=xtitle, 	$
             ytitle=ytitle, xstyle=style, ystyle=style, 		$
             charsize=charsize
    if makeplot eq 1 then begin

		;**********************
		;****  Make plots  ****
		;**********************

        oplot, x(0,0:ieval-1), y(0,0:ieval-1), psym=-1
        if (nplots eq 2) then begin
            oplot, x(1,*), y(1,*), linestyle=5
        endif 
    endif else begin                    ;no plot possible
        xyouts, 0.2, 0.5, /normal, charsize=charsize*1.5,               $
                '---- No data lies within range ----'
    endelse

		;****************************
		;**** Annotation to plot ****
		;****************************

    rhs = grright + 0.02

		;********************
		;**** plot title ****
		;********************

    if small_check eq 'YES' then begin
	charsize = charsize * 0.9
    endif

    xyouts, (left-0.05), (top+0.05), gtitle, /normal, alignment=0.0, 	$
	    charsize = charsize

		;********************************
		;**** right hand side labels ****
		;********************************

    cpp = grtop - 0.02
    xyouts, rhs,  cpp, head1, /normal, alignment=0.0,charsize=charsize*0.8

    cpp = cpp - 0.02
    xyouts, rhs, cpp, strg(4), /normal, alignment= 0.0, 		$
    charsize=charsize*0.8

    cpp = cpp -0.03
    xyouts, rhs,  cpp, head2, /normal, alignment=0.0,charsize=charsize*0.8

    cpp = cpp - 0.02
    xyouts, rhs, cpp, strg(0), /normal, alignment= 0.0, 		$
        charsize=charsize*0.8

    cpp = cpp - 0.03
    xyouts, rhs,  cpp, head3, /normal, alignment=0.0,charsize=charsize*0.8

    cpp = cpp - 0.02
    xyouts, rhs, cpp, strg(2), /normal, alignment= 0.0, 		$
    charsize=charsize*0.8

END
