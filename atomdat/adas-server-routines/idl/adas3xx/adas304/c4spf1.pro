; Copyright (c) 1995, Strathclyde University.
; SCCS Info: Module @(#)$Header: /home/adascvs/idl/adas3xx/adas304/c4spf1.pro,v 1.2 2004/07/06 11:52:09 whitefor Exp $ Date $Date: 2004/07/06 11:52:09 $
;
;+
; PROJECT:
;       ADAS IBM MVS to UNIX conversion
;
; NAME:
;	C4SPF1
;
; PURPOSE:
;	IDL user interface and communications with ADAS304 FORTRAN
;	process via pipe.
;
; EXPLANATION:
;	First this routine invokes the 'Output Options' part of the
;	interface.  Then the results of the user's
;	interactions are written back to the FORTRAN via the pipe.
;	Communications are with the FORTRAN subroutine C4SPF1.
;
; USE:
;	The use of this routine is specific to ADAS304, See adas304.pro.
;
; INPUTS:
;	PIPE	- The IDL unit number of the bi-directional pipe to the
;		  ADAS304 FORTRAN process.
;
;	OUTVAL	- A structure which determines the initial settings of
;		  the dataset selection widget.  The initial value is
;		  set in adas304.pro.  OUTVAL is passed un-modified
;		  through to cw_adas_in.pro, see that routine for a full
;		  description.
;
;       PROCVAL - Structure containing the final settings of the
;		  processing options widget.
;
;	HEADER  - Header information used for text output.
;
;       BITFILE - String; the path to the dirctory containing bitmaps
;                 for the 'escape to series menu' button.
;
;	PID	- Integer; the process id of the FORTRAN part of the code.
;
; OPTIONAL INPUTS:
;	None.
;
; OUTPUTS:
;	LPEND	- Integer; indicates user action on leaving output
;		  selections.  lpend is set to 1 if the user exited
;		  with the 'Cancel' or 'Menu'  button and 0 if the user 
;		  exited with 'Done'.
;
;	OUTVAL	- On output the structure records the final settings of
;		  the output selection widget if the user pressed the
;		  'Done' button otherwise it is not changed from input.
;
;       GOMENU  - Int; flag - set to 1 if user has selected 'escape direct
;                 to series menu' , otherwise 0
;
; OPTIONAL OUTPUTS:
;	None.
;
; KEYWORD PARAMETERS:
;	DEVLIST	- A string array;  Each element of the array is the
;		  name of a hardcopy graphics output device.  This
;		  list must mirror the DEVCODE list.  DEVLIST is the
;		  list which the user will see.
;		  e.g ['Post-Script','HP-PCL','HP-GL']
;
;	FONT_LARGE- Supplies the large font to be used.
;
;	FONT_SMALL- Supplies the small font to be used.
;
; CALLS:
;	ADAS304_OUT	User interface - output options.
;
; SIDE EFFECTS:
;	This routine communicates with the ADAS304 FORTRAN process
;	via a UNIX pipe.
;
; CATEGORY:
;	Adas system.
;
; WRITTEN:
;       Tim Hammond, Tessella Support Services plc, 06-12-95  
;
; MODIFIED: 
;	1.1	Tim Hammond
;		First version
;	1.2	Richard Martin
;		Changed to 'lpend=1' when 'menu' pressed.
;
; VERSION:
;	1.1	06-12-95
;	1.2	28-02-97
;
;-----------------------------------------------------------------------------

PRO c4spf1, pipe, lpend, outval, procval, header, bitfile, 		$
            gomenu, pid, line1, dataset, DEVLIST=devlist, 		$
	    FONT_LARGE=font_large, FONT_SMALL=font_small


                ;**** Set defaults for keywords ****

    IF NOT (KEYWORD_SET(font_large)) THEN font_large = ''
    IF NOT (KEYWORD_SET(font_small)) THEN font_small = ''


		;**** Read whether it was possible to fit a polynomial ****

    idum = 0
    readf, pipe, idum
    
    procval.lfsel = idum

		;**************************************
		;**** Pop-up output options widget ****
		;**************************************

    adas304_out, outval, procval, action, bitfile,  			$
                 DEVLIST=devlist, FONT_LARGE=font_large, FONT_SMALL=font_small

		;*********************************************
		;**** Act on the output from the widget   ****
		;**** There are three    possible actions ****
		;**** 'Done', 'Cancel' and 'Menu'.        ****
		;*********************************************

    if action eq 'Done' then begin
        lpend = 0
    endif else if action eq 'Menu' then begin
	lpend = 1
	gomenu = 1
    endif else begin
        lpend = 1
    endelse

		;*******************************
		;**** Write data to fortran ****
		;*******************************

    printf, pipe, lpend

    if lpend eq 0 then begin

	printf, pipe, outval.grpout

		;**** Extra check to see whether FORTRAN is still there ****

	if find_process(pid) eq 0 then return

		;*** If text output requested tell FORTRAN ****
     
        printf, pipe, outval.texout
        
        if (outval.texout eq 1) then begin
            printf, pipe, outval.texdsn
        endif

    endif

		;**************************************************
		;**** Set messages and settings ready for when ****
		;**** output window is re-displayed.           ****
		;**************************************************

    if lpend eq 0 then begin
        if outval.texout eq 1 then begin

		;**** If text output is requested enable append ****
		;**** for next time and update the default to   ****
		;**** the current text file.                    ****

            outval.texapp=0
	    outval.texdef = outval.texdsn
            if outval.texrep ge 0 then outval.texrep = 0
      	    outval.texmes = 'Output written to file.'
        endif

    endif

		;*************************************************
		;**** If text output has been requested write ****
		;**** header to pipe for XXADAS to read.      ****
		;*************************************************

    if (lpend eq 0 and outval.texout eq 1) then begin
        printf, pipe, header
	printf, pipe, line1	
        printf, pipe, dataset
    endif

END
