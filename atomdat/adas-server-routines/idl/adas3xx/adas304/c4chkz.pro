; Copyright (c) 1995 Strathclyde University 
; SCCS info: Module @(#)$Header: /home/adascvs/idl/adas3xx/adas304/c4chkz.pro,v 1.2 2004/07/06 11:51:01 whitefor Exp $ Date $Date: 2004/07/06 11:51:01 $
;+
; PROJECT:
;       ADAS IBM MVS to UNIX conversion
;
; NAME:
;       C4CHKZ
;
; PURPOSE:
;       Pipe communications with C4CHKZ FORTRAN routine.
;
; EXPLANATION:
;       This routine is called by adas304.pro to pass information on
;	the stopping ion symbol and charge contained in the adf21 filename
;	used by the adas 304 routines. Previously C4CHKZ.FOR would
;	analyse the filenames and extract this information itself,
;	but this is no longer possible with the UNIX conversion and
;	so the information is piped to it for each of the filenames
;	it has been given.
;
; USE:
;       Specific to adas 304. See adas304.pro for example.
;
; INPUTS:
;       PIPE    - The IDL unit number of the bi-directional pipe to the
;                 ADAS304 FORTRAN process.
;
;       VALUE   - A structure containing the input selections of the user.
;
;	NUM_FILES - Integer; the number of stopping ion files chosen by 
;		    the user.
;
;	PID	  - Integer; the process id of the FORTRAN code.
;
; OPTIONAL INPUTS:
;       None
;
; OUTPUTS:
;       TSYMM	-  String array; the element symbols for the stopping ion
;		   files (in upper case).
;
;	ITZM	-  Integer array; the ion charges for each symbol.
;
; OPTIONAL OUTPUTS:
;       None
;
; KEYWORD PARAMETERS:
;       None
;
; CALLS:
;       FIND_PROCESS    Checks to see if a given process is active.
;
; SIDE EFFECTS:
;       This routine communicates with the ADAS304 FORTRAN process
;       via a UNIX pipe.
;
; CATEGORY:
;       Adas system.
;
; WRITTEN:
;       Tim Hammond, Tessella Support Services plc, 21st November 1995
;
; MODIFIED:
;       1.1     Tim Hammond
;               First version.
;       1.2     Martin O'Mullane
;                 - Added logic to detect a '_' before the symbol
;                   in the filename to allow files of the form
;                   bms<yy>#<bm>_<type>_<el><n>.dat, eg bms97_fast_b5.dat.
;
; VERSION:
;       1.1     21-11-95
;       1.2     13-04-2004
;
;-
;-----------------------------------------------------------------------------

PRO c4chkz, pipe, value, num_files, pid, tsymm, itzm
    
    
    
    tsymm = strarr(10)
    itzm = intarr(10)
    for i=0, num_files-1 do begin
	if find_process(pid) eq 0 then goto, LABELRETURN
	symbol = strcompress(strupcase(value.ions(i)), /remove_all)
        st = strpos(value.ions(i), '_') + 1
        symbol = strmid(symbol,st)
	symbyte = byte(symbol)
	length = n_elements(symbyte)
        finished = 0
	for j=0, length-1 do begin
	    if symbyte(j) ge 48 and symbyte(j) lt 57 and 		$
            finished eq 0 then begin
		finished = 1
		tsymm(i) = strmid(symbol, 0, j)
		itzm(i) = strmid(symbol, j, length)
	    endif
	endfor
	printf, pipe, tsymm(i)
	printf, pipe, itzm(i)
    endfor

LABELRETURN:

END
