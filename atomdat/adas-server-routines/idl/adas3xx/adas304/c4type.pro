;+
; PROJECT:
;       ADAS
;
; NAME:
;       C4TYPE
;
; PURPOSE:
;       Interrogates input file to ADAS304 to determine whether it
;       is a beam stopping, beam emission or beam density file.
;
; EXPLANATION:
;       If adf21 is part of the file name it is BMS.
;       If add22 and contains an A-value is is BME. 
;       If add22 and contains an N-level is is BMP.
;       For non-standard names check for A-value or N-level.
;       Return an unknown flag if type cannot be determined.
;       This will not affect the running of ADAS304 but the
;       y-axis label cannot be set. 
;
; USE:
;       The use of this routine is specific to ADAS304, see adas304.pro.
;
; INPUTS:
;       DSNAME  - Input file.
;
;
; OPTIONAL INPUTS:
;       None
;
; OUTPUTS:
;       TYPE    - 0 : BMS, 1 : BME, 2 : BMP, 3 : unknown.
;
; OPTIONAL OUTPUTS:
;       None
;
; SIDE EFFECTS:
;       Opens files.
;
; CATEGORY:
;       Adas system.
;
; WRITTEN:
;       Martin O'Mullane
;
; MODIFIED:
;       1.1  Martin O'Mullane
;             - First version.
;
; VERSION:
;       1.1  10-12-2004
;
;-----------------------------------------------------------------------------

FUNCTION c4type, dsname

; Assume unknown

tflag = 3

; Is there an adf21 in filename

index = strpos(dsname, 'adf21')
if index NE -1 then begin
   tflag = 0
   goto, LABELEND
endif

; Or adf22

index = strpos(dsname, 'adf22')

if index NE -1 then begin

   adas_readfile, file=dsname, all=all
   index = where(strpos(all, 'bme') NE -1)
   if index NE -1 then  begin
      tflag = 1
      goto, LABELEND
   endif
   index = where(strpos(all, 'A-VALUE') NE -1)
   if index NE -1 then  begin
      tflag = 1
      goto, LABELEND
   endif
   
   index = where(strpos(all, 'bmp') NE -1)
   if index NE -1 then  begin
      tflag = 2
      goto, LABELEND
   endif
   index = where(strpos(all, 'N-LEVEL') NE -1)
   if index NE -1 then  begin
      tflag = 2
      goto, LABELEND
   endif

endif


; Probably a passing file

adas_readfile, file=dsname, all=all

index = where(strpos(all, 'adf21') NE -1)
if index NE -1 then  begin
   tflag = 0
   goto, LABELEND
endif


index = where(strpos(all, 'bme') NE -1)
if index NE -1 then  begin
   tflag = 1
   goto, LABELEND
endif
index = where(strpos(all, 'A-VALUE') NE -1)
if index NE -1 then  begin
   tflag = 1
   goto, LABELEND
endif

index = where(strpos(all, 'bmp') NE -1)
if index NE -1 then  begin
   tflag = 2
   goto, LABELEND
endif
index = where(strpos(all, 'N-LEVEL') NE -1)
if index NE -1 then  begin
   tflag = 2
   goto, LABELEND
endif


LABELEND:

RETURN, tflag

END
