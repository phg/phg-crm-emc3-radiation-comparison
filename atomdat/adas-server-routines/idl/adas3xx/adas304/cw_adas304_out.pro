; Copyright (c) 1995, Strathclyde University.
; SCCS Info : Module @(#)$Header: /home/adascvs/idl/adas3xx/adas304/cw_adas304_out.pro,v 1.2 2004/07/06 12:38:19 whitefor Exp $ Date $Date: 2004/07/06 12:38:19 $
;+
; PROJECT:
;       ADAS IBM MVS to UNIX conversion
;
; NAME:
;	CW_ADAS304_OUT()
;
; PURPOSE:
;	Produces a widget for ADAS304 output options.
;
; EXPLANATION:
;	This function declares a compound widget consisting of:
;	 - a selection button for graphical output
;	 - for each graph a 'select axis limits' button
;	 - for each graph a set of typeins for max and min axis values
;	 - note that each graph set is only mapped when the appropriate
;	   selection has been made on the processing screen (i.e. value
;	   of procval.ifit)
;	 - the associated hard copy widget comprising a filename and
;	   list of output devices
;	 - output file widgets cw_adas_outfile for text output
;	 - a text typein for the user to assign a title to the graph
;	 - buttons for 'Done', 'Cancel' and 'Escape to series menu' 
;	   (this latter is represented by a bitmapped button).
;	This widget only generates events for the 'Done', 'Cancel'
;	and 'Escape to series menu' buttons.
;
; USE:
;	This routine is specific to adas304.
;
; INPUTS:
;	PARENT	- Long integer; ID of parent widget.
;
;	PROCVAL - A structure containing the settings of the processing
;		  options widget.
;
;       BITFILE - String; the path to the dirctory containing bitmaps
;                 for the 'escape to series menu' button.
;
; OPTIONAL INPUTS:
;	None.
;
; OUTPUTS:
;	The return value of this function is the ID of the declared widget.
;
; OPTIONAL OUTPUTS:
;	None.
;
; KEYWORD PARAMETERS:
;	DEVLIST	- A string array;  Each element of the array is the
;		  name of a hardcopy graphics output device.  This
;		  list must mirror the DEVCODE list.  DEVLIST is the
;		  list which the user will see.
;		  e.g ['Post-Script','HP-PCL','HP-GL']
;
;	VALUE	- A structure which determines the initial settings of
;		  the output options widget.  The structure has the 
;		  following form:
;
;		       {GRPOUT:			0,			$
;			GTIT1:			'',			$
;			LDEF1:			0,			$
;			LDEF2:			0,			$
;			LDEF3:			0,			$
;			XMIN1:			'',			$
;			XMAX1:			'',			$
;			YMIN1:			'',			$
;			YMAX1:			'',			$
;			XMIN2:			'',			$
;			XMAX2:			'',			$
;			YMIN2:			'',			$
;			YMAX2:			'',			$
;			XMIN3:			'',			$
;			XMAX3:			'',			$
;			YMIN3:			'',			$
;			YMAX3:			'',			$
;			HRDOUT:			0,			$
;			HARDNAME:		'',			$
;			GRPDEF:			'',			$
;			GRPFMESS:		'',			$
;			DEVSEL:			-1,			$
;			TEXOUT:			0,			$
;			TEXAPP:			-1,			$
;			TEXREP:			0,			$
;			TEXDSN:			'',			$
;			TEXDEF:			'paper.txt',		$
;			TEXMES:			'' 			}
;
;			GRPOUT	     Int; flag whether graphical output
;				     chosen
;
;			GTIT1	     String; Graph title
;
;			LDEFn	     Int; flag whether user has selected
;				     explicit axes for graph n
;
;			XMINn	     Float; min of x-axis for graph n
;
;			XMAXn	     Float; max of x-axis for graph n
;
;			YMINn	     Float; min of y-axis for graph n
;
;			YMAXn	     Float; max of y-axis for graph n
;
;			HRDOUT	     Integer; Hard copy activ' 1 on, 0 off
;
;			HARDNAME     String; Hard copy output file name
;
;			GRPDEF	     String; Default output file name 
;
;			GRPFMESS     String; File name error message
;
;
;			DEVSEL	     Integer; index of selected device in 
;				     DEVLIST
;
;			TEXOUT	     Integer; Activation button 1 on, 0 off
;
;			TEXAPP	     Integer; Append button 1 on, 0 off, 
;				     -1 no button
;
;			TEXREP	     Integer; Replace button 1 on, 0 off, 
;				     -1 no button
;
;			TEXDSN	     String; Output file name
;
;			TEXDEF	     String; Default file name
;
;			TEXMES	     String; file name error message
;
;	UVALUE	- A user value for this widget.
;
;       FONT_LARGE- Supplies the large font to be used.
;
;       FONT_SMALL- Supplies the small font to be used.
;
; CALLS:
;	CW_ADAS_OUTFILE	Output file name entry widget.
;	See side effects for other related widget management routines.
;
; SIDE EFFECTS:
;
; 	Two other routines are included in this file
;	OUT304_GET_VAL()
;	OUT304_EVENT()
;
; CATEGORY:
;	Compound Widget
;
; WRITTEN:
;       Tim Hammond, Tessella Support Services plc, 6th December 1995
;
; MODIFIED:
;	1.1	Tim Hammond	
;		First version
;	1.2	William Osborn
;		Removed xsize=n keyword from widget_label command.
;		The dynlabel procedure does not override these
;		assignments and they were taken as being values in pixels
;		on some machines.
;
; VERSION:
;	1.1	6-12-95
;	1.2	01-08-96
;
;-
;-----------------------------------------------------------------------------

FUNCTION out304_get_val, id

    COMMON outblock304, font, font_small, ldef1, ldef2, ldef3, 		$
                        oldindex, ifit

                ;**** Return to caller on error ****

    ON_ERROR, 2

                ;**** Retrieve the state ****

    parent=widget_info(id, /parent)
    widget_control, parent, get_uvalue=state, /no_copy

		;**** Get graph title ****

    widget_control, state.titleid, get_value=graphtitleval
    graphtitle = graphtitleval(0)

		;**** Get user defined limits ****
	
    widget_control, state.bexminbut, get_value=xmin
    xmin1 = xmin(0)
    widget_control, state.bexmaxbut, get_value=xmax
    xmax1 = xmax(0)
    widget_control, state.beyminbut, get_value=ymin
    ymin1 = ymin(0)
    widget_control, state.beymaxbut, get_value=ymax
    ymax1 = ymax(0)
    widget_control, state.dexminbut, get_value=xmin
    xmin2 = xmin(0)
    widget_control, state.dexmaxbut, get_value=xmax
    xmax2 = xmax(0)
    widget_control, state.deyminbut, get_value=ymin
    ymin2 = ymin(0)
    widget_control, state.deymaxbut, get_value=ymax
    ymax2 = ymax(0)
    widget_control, state.texminbut, get_value=xmin
    xmin3 = xmin(0)
    widget_control, state.texmaxbut, get_value=xmax
    xmax3 = xmax(0)
    widget_control, state.teyminbut, get_value=ymin
    ymin3 = ymin(0)
    widget_control, state.teymaxbut, get_value=ymax
    ymax3 = ymax(0)

		;**** Get hard copy settings ****

    widget_control, state.hardid, get_value=hardval
    if hardval.outbut eq 1 then begin
	widget_control, state.devid, get_value=devices
	devindex=devices
    endif else begin
	devindex = oldindex
    endelse

		;**** Get text output settings ****

    widget_control, state.paperid, get_value=papos

    os = { out304_set, 						$
		GRPOUT		:	state.grpout,		$
	   	GTIT1		:	graphtitle,		$
		LDEF1		:	ldef1,			$
		LDEF2		:	ldef2,			$
		LDEF3		:	ldef3,			$
	   	XMIN1		:	xmin1, 			$
		XMAX1		:	xmax1, 	 		$
	   	YMIN1		:	ymin1, 			$
		YMAX1		:	ymax1,   		$
	   	XMIN2		:	xmin2, 			$
		XMAX2		:	xmax2,    		$
	   	YMIN2		:	ymin2, 			$
		YMAX2		:	ymax2, 	 		$
	   	XMIN3		:	xmin3,  		$
		XMAX3		:	xmax3,   		$
	   	YMIN3		:	ymin3,  		$
		YMAX3		:	ymax3, 	 		$
	   	HRDOUT		:	hardval.outbut,		$
		HARDNAME	:	hardval.filename,	$
	   	GRPDEF		:	hardval.defname, 	$
		GRPFMESS	:	hardval.message,   	$
	   	DEVSEL		:	devindex,		$
	   	TEXOUT		:	papos.outbut,  		$
		TEXAPP		:	papos.appbut, 		$
	   	TEXREP		:	papos.repbut,  		$
		TEXDSN		:	papos.filename, 	$
	   	TEXDEF		:	papos.defname, 		$
		TEXMES		:	papos.message 		}

                ;**** Return the state ****

  widget_control, parent, set_uvalue=state, /no_copy

  RETURN, os 

END

;-----------------------------------------------------------------------------

FUNCTION out304_event, event

    COMMON outblock304, font, font_small, ldef1, ldef2, ldef3, 		$
                        oldindex, ifit

                ;**** Base ID of compound widget ****

    parent=event.top

                ;**** Retrieve the state ****

    widget_control, parent, get_uvalue=state, /no_copy

		;*********************************
		;**** Clear previous messages ****
		;*********************************

    widget_control, state.messid, set_value=' '

                ;************************
                ;**** Process events ****
                ;************************

    CASE event.id OF

		;***********************
		;**** Cancel button ****
		;***********************

        state.cancelid: begin
            new_event = {ID:parent, TOP:event.top, 			$
		         HANDLER:0L, ACTION:'Cancel'}
        end

		;**** Do we want graphical output? button ****

	state.grapflag: begin
	    if state.grpout eq 1 then state.grpout=0 else state.grpout=1
	    widget_control, state.plotbase, sensitive=state.grpout
	    widget_control, state.hardbase, sensitive=state.grpout
	end

		;**** Explicit scaling for beam energy plot ****

	state.beamdef: begin
	    if ldef1 eq 1 then ldef1=0 else ldef1=1
	    widget_control, state.beambase3b, sensitive=ldef1
	end

		;**** Explicit scaling for target density plot ****

	state.dendef: begin
	    if ldef2 eq 1 then ldef2=0 else ldef2=1
	    widget_control, state.denbase3b, sensitive=ldef2
	end

		;**** Explicit scaling for target temperature plot ****

	state.tedef: begin
	    if ldef3 eq 1 then ldef3=0 else ldef3=1
	    widget_control, state.tebase3b, sensitive=ldef3
	end

		;**** min/max buttons for beam energy plot ****

	state.bexminbut: widget_control, state.bexmaxbut, /input_focus
	state.bexmaxbut: widget_control, state.beyminbut, /input_focus
	state.beyminbut: widget_control, state.beymaxbut, /input_focus
	state.beymaxbut: widget_control, state.bexminbut, /input_focus

		;**** min/max buttons for target density plot ****

	state.dexminbut: widget_control, state.dexmaxbut, /input_focus
	state.dexmaxbut: widget_control, state.deyminbut, /input_focus
	state.deyminbut: widget_control, state.deymaxbut, /input_focus
	state.deymaxbut: widget_control, state.dexminbut, /input_focus

		;**** min/max buttons for target temperature plot ****

	state.texminbut: widget_control, state.texmaxbut, /input_focus
	state.texmaxbut: widget_control, state.teyminbut, /input_focus
	state.teyminbut: widget_control, state.teymaxbut, /input_focus
	state.teymaxbut: widget_control, state.texminbut, /input_focus

		;*********************
		;**** Done button ****
		;*********************

        state.doneid: begin

		;****************************************************
                ;**** Return the state before checking can start ****
		;**** with the get_value keyword.                ****
		;****************************************************

            widget_control, parent, set_uvalue=state, /no_copy

		;***************************************
		;**** Check for errors in the input ****
		;***************************************

	    error = 0

		;**** Get a copy of the widget value ****

	    widget_control, event.handler, get_value=os
	
		;**** Check for widget error messages ****

     	    if os.grpout eq 1 and strtrim(os.grpfmess) ne '' then error=1
	    if os.texout eq 1 and strtrim(os.texmes) ne '' then error=1
	    if error eq 1 then warnmess = '**** Error in output settings ****'

		;**** Check a device has been selected if ****
		;**** hard copy requested                 ****

	    if error ne 1 and os.grpout eq 1 and os.hrdout eq 1 and 	$
	    os.devsel eq -1 then begin
		warnmess = '**** Error: You must select an output device ****'
		error = 1
	    endif

		;**** Check for errors in scalings if relevant ****

 	    if ifit eq 0 and os.ldef1 eq 1 and error ne 1 then begin
		serror = 0
		serror = serror + num_chk(os.xmin1, sign=1)
		serror = serror + num_chk(os.xmax1, sign=1)
		serror = serror + num_chk(os.ymin1, sign=1)
		serror = serror + num_chk(os.ymax1, sign=1)
		if serror eq 0 then begin
		    if (float(os.xmin1) ge float(os.xmax1)) or 		$
                    (float(os.ymin1) ge float(os.ymax1))		$
                    then serror = 1
		endif
		if serror gt 0 then begin
		    error = 1
                    warnmess = '**** Error in output settings: '+	$
                               'Axis limits are invalid ****'
		endif
	    endif else if ifit eq 1 and os.ldef2 eq 1 			$
            and error ne 1 then begin
		serror = 0
                serror = serror + num_chk(os.xmin2, sign=1)
                serror = serror + num_chk(os.xmax2, sign=1)
                serror = serror + num_chk(os.ymin2, sign=1)
                serror = serror + num_chk(os.ymax2, sign=1)
                if serror eq 0 then begin
                    if (float(os.xmin2) ge float(os.xmax2)) or 		$
                    (float(os.ymin2) ge float(os.ymax2)) 		$
                    then serror = 1
                endif
                if serror gt 0 then begin
                    error = 1
                    warnmess = '**** Error in output settings: '+	$
                               'Axis limits'+				$
                               ' are invalid ****'
                endif
            endif else if ifit eq 2 and os.ldef3 eq 1            	$
            and error ne 1 then begin
                serror = 0
                serror = serror + num_chk(os.xmin3, sign=1)
                serror = serror + num_chk(os.xmax3, sign=1)
                serror = serror + num_chk(os.ymin3, sign=1)
                serror = serror + num_chk(os.ymax3, sign=1)
                if serror eq 0 then begin
                    if (float(os.xmin3) ge float(os.xmax3)) or          $
                    (float(os.ymin3) ge float(os.ymax3))                $
                    then serror = 1
                endif
                if serror gt 0 then begin
                    error = 1
                    warnmess = '**** Error in output settings: '+       $
                               'Axis limits'+     			$
                               ' are invalid ****'
                endif
            endif

                ;**** Retrieve the state   ****

            widget_control, parent, get_uvalue=state, /no_copy

	    if error eq 1 then begin
	        widget_control, state.messid, set_value=warnmess
	        new_event = 0L
	    endif else begin
	        new_event = {ID:parent, TOP:event.top, HANDLER:0L, 	$
                             ACTION:'Done'}
	    endelse

        end

		;**** Menu button ****

	state.outid: begin
	    new_event = {ID:parent, TOP:event.top, HANDLER:0L,      	$
                         ACTION:'Menu'}
	end

        ELSE: begin
            new_event = 0L
          end

    ENDCASE

                ;**** Return the state   ****

    widget_control, parent, set_uvalue=state, /no_copy

    RETURN, new_event

END

;-----------------------------------------------------------------------------

FUNCTION cw_adas304_out, parent, procval, DEVLIST=devlist,  		$
		         VALUE=value, UVALUE=uvalue, FONT_LARGE=font_large,$
			 FONT_SMALL=font_small, bitfile

    COMMON outblock304, fontlargecom, fontsmallcom, ldef1com, ldef2com,	$
                        ldef3com, oldindex, ifitcom


    IF (N_PARAMS() EQ 0) THEN MESSAGE, 'Must specify parent for cw_adas304_out'
    ON_ERROR, 2					;return to caller

		;**** Set defaults for keywords ****

    IF NOT (KEYWORD_SET(devlist)) THEN devlist = ''
    IF NOT (KEYWORD_SET(uvalue)) THEN uvalue = 0
    IF NOT (KEYWORD_SET(font_large)) THEN font_large = ''
    IF NOT (KEYWORD_SET(font_small)) THEN font_small = ''
    IF NOT (KEYWORD_SET(value)) THEN begin
	os = {out304_set, 						$
			GRPOUT		:	0,			$
			GTIT1		:	'', 			$
			LDEF1		:	0,			$
			LDEF2		:	0,			$
			LDEF3		:	0,			$
                        XMIN1           :       '',                     $
                        XMAX1           :       '',                     $
                        YMIN1           :       '',                     $
                        YMAX1           :       '',                     $
                        XMIN2           :       '',                     $
                        XMAX2           :       '',                     $
                        YMIN2           :       '',                     $
                        YMAX2           :       '',                     $
                        XMIN3           :       '',                     $
                        XMAX3           :       '',                     $
                        YMIN3           :       '',                     $
                        YMAX3           :       '',                     $
			HRDOUT		:	0, 			$
			HARDNAME	:	'', 			$
			GRPDEF		:	'', 			$
			GRPFMESS	:	'',		 	$
			DEVSEL		:	-1, 			$
			TEXOUT		:	0, 			$
			TEXAPP		:	-1,			$
			TEXREP		:	0, 			$
			TEXDSN		:	'', 			$
        		TEXDEF		:	'paper.txt',		$
			TEXMES		:	''			}
    ENDIF ELSE BEGIN
	os = {out304_set, 						$
			GRPOUT		:	value.grpout,		$
			GTIT1		:	value.gtit1, 		$
			LDEF1		:	value.ldef1,		$
			LDEF2		:	value.ldef2,		$
			LDEF3		:	value.ldef3,		$
                        XMIN1           :       value.xmin1,            $
                        XMAX1           :       value.xmax1,            $
                        YMIN1           :       value.ymin1,            $
                        YMAX1           :       value.ymax1,            $
                        XMIN2           :       value.xmin2,            $
                        XMAX2           :       value.xmax2,            $
                        YMIN2           :       value.ymin2,            $
                        YMAX2           :       value.ymax2,            $
                        XMIN3           :       value.xmin3,            $
                        XMAX3           :       value.xmax3,            $
                        YMIN3           :       value.ymin3,            $
                        YMAX3           :       value.ymax3,            $
			HRDOUT		:	value.hrdout, 		$
			HARDNAME	:	value.hardname, 	$
			GRPDEF		:	value.grpdef, 		$
			GRPFMESS	:	value.grpfmess,		$
			DEVSEL		:	value.devsel, 		$
			TEXOUT		:	value.texout, 		$
			TEXAPP		:	value.texapp, 		$
			TEXREP		:	value.texrep, 		$
			TEXDSN		:	value.texdsn, 		$
			TEXDEF		:	value.texdef, 		$
			TEXMES		:	value.texmes		}
    ENDELSE

    fontlargecom = font_large
    fontsmallcom = font_small
    ldef1com = os.ldef1
    ldef2com = os.ldef2
    ldef3com = os.ldef3
    oldindex = os.devsel
    ifitcom = procval.ifit

		;**********************************************
		;**** Create the 304 Output options widget ****
		;**********************************************

		;**** create base widget ****

    cwid = widget_base( parent, UVALUE = uvalue, 			$
			EVENT_FUNC = "out304_event", 			$
			FUNC_GET_VALUE = "out304_get_val", 		$
			/COLUMN)

		;**** Main graphics base ****

    graphbase = widget_base(cwid, /column, /frame)

		;**** Button to activate graphing ****

    graphbase1 = widget_base(graphbase, /row, /nonexclusive)
    grapflag = widget_button(graphbase1, value='Graphical output',	$
			     font=font_large)
    widget_control, grapflag, set_button=os.grpout

                ;**** Graph title ****

    titlebase = widget_base(graphbase, /row)
    rc = widget_label(titlebase, value='Graph Title', font=font_small)
    titleid = widget_text(titlebase, value=os.gtit1, font=font_small,	$
			  /editable, xsize=30)


		;**** Base for all plots ****

    plotbase = widget_base(graphbase, /frame)

		;**** Beam energy plot base ****

    if (procval.ifit eq 0) then bemap=1 else bemap=0
    beambase = widget_base(plotbase, /column, map=bemap)
    beamhead = widget_label(beambase, font=font_small,			$
			    value='Beam energy plot:-')
    beambase3 = widget_base(beambase, /row)
    beambase3a = widget_base(beambase3, /row, /nonexclusive)
    beamdef = widget_button(beambase3a, value='Explicit scaling  ',	$
			    font=font_small)
    widget_control, beamdef, set_button=os.ldef1
    beambase3b = widget_base(beambase3, /column, /frame)
    beambase3c = widget_base(beambase3b, /row)
    beamxmin = widget_label(beambase3c, font=font_small, value='X-min: ')
    bexminbut = widget_text(beambase3c, font=font_small, value=os.xmin1,$
		           xsize=8, /editable)
    beamxmax = widget_label(beambase3c, font=font_small, value='X-max: ')
    bexmaxbut = widget_text(beambase3c, font=font_small, value=os.xmax1,$
		           xsize=8, /editable)
    beambase3d = widget_base(beambase3b, /row)
    beamymin = widget_label(beambase3d, font=font_small, value='Y-min: ')
    beyminbut = widget_text(beambase3d, font=font_small, value=os.ymin1,$
		           xsize=8, /editable)
    beamymax = widget_label(beambase3d, font=font_small, value='Y-max: ')
    beymaxbut = widget_text(beambase3d, font=font_small, value=os.ymax1,$
		           xsize=8, /editable)
    widget_control, beambase3b, sensitive=os.ldef1
    

		;**** Target density plot base ****

    if (procval.ifit eq 1) then demap=1 else demap=0
    denbase = widget_base(plotbase, /column, map=demap)
    denhead = widget_label(denbase, font=font_small,			$
		           value='Target density plot:-')
    denbase3 = widget_base(denbase, /row)
    denbase3a = widget_base(denbase3, /row, /nonexclusive)
    dendef = widget_button(denbase3a, value='Explicit scaling  ',       $
                           font=font_small)
    widget_control, dendef, set_button=os.ldef2
    denbase3b = widget_base(denbase3, /column, /frame)
    denbase3c = widget_base(denbase3b, /row)
    denxmin = widget_label(denbase3c, font=font_small, value='X-min: ')
    dexminbut = widget_text(denbase3c, font=font_small, value=os.xmin2,	$
		            xsize=8, /editable)
    denxmax = widget_label(denbase3c, font=font_small, value='X-max: ')
    dexmaxbut = widget_text(denbase3c, font=font_small, value=os.xmax2,	$
		            xsize=8, /editable)
    denbase3d = widget_base(denbase3b, /row)
    denymin = widget_label(denbase3d, font=font_small, value='Y-min: ')
    deyminbut = widget_text(denbase3d, font=font_small, value=os.ymin2,	$
		            xsize=8, /editable)
    denymax = widget_label(denbase3d, font=font_small, value='Y-max: ')
    deymaxbut = widget_text(denbase3d, font=font_small, value=os.ymax2,	$
		            xsize=8, /editable)
    widget_control, denbase3b, sensitive=os.ldef2

		;**** Target temperatures plot base ****

    if (procval.ifit eq 2) then temap=1 else temap=0
    tebase = widget_base(plotbase, /column, map=temap)
    tehead = widget_label(tebase, font=font_small,			$
			  value='Target temperature plot:-')
    tebase3 = widget_base(tebase, /row)
    tebase3a = widget_base(tebase3, /row, /nonexclusive)
    tedef = widget_button(tebase3a, value='Explicit scaling  ',         $
                          font=font_small)
    widget_control, tedef, set_button=os.ldef3
    tebase3b = widget_base(tebase3, /column, /frame)
    tebase3c = widget_base(tebase3b, /row)
    texmin = widget_label(tebase3c, font=font_small, value='X-min: ')
    texminbut = widget_text(tebase3c, font=font_small, value=os.xmin3,	$
		           xsize=8, /editable)
    texmax = widget_label(tebase3c, font=font_small, value='X-max: ')
    texmaxbut = widget_text(tebase3c, font=font_small, value=os.xmax3,	$
		           xsize=8, /editable)
    tebase3d = widget_base(tebase3b, /row)
    teymin = widget_label(tebase3d, font=font_small, value='Y-min: ')
    teyminbut = widget_text(tebase3d, font=font_small, value=os.ymin3,	$
		           xsize=8, /editable)
    teymax = widget_label(tebase3d, font=font_small, value='Y-max: ')
    teymaxbut = widget_text(tebase3d, font=font_small, value=os.ymax3,	$
		           xsize=8, /editable)
    widget_control, tebase3b, sensitive=os.ldef3


		;**** Base for hard-copy widgets ****

    if os.hrdout ge 0 and strtrim(devlist(0)) ne '' then begin
        hardbase = widget_base(graphbase, /row, /frame)
        outfval = { OUTBUT:os.hrdout, APPBUT:-1, REPBUT:0,		$
                    FILENAME:os.hardname, DEFNAME:os.grpdef,		$
                    MESSAGE:os.grpfmess }
        hardid = cw_adas_outfile(hardbase, OUTPUT='Enable Hard Copy',	$
				 VALUE=outfval, FONT=font_small)
        hardbase2 = widget_base(hardbase, /column)
        devid = cw_single_sel(hardbase, devlist, title='Select Device',	$
                              value=os.devsel, font=font_small)
    endif else begin
	devid = 0L
	hardid = 0L
	hardbase = 0L
	hardbase2 = 0L
    endelse

		;**** Set the overall sensitivity of the bases ****

    widget_control, plotbase, sensitive=os.grpout
    widget_control, hardbase, sensitive=os.grpout

		;**** Widget for text output ****

    outfval = { OUTBUT:os.texout, APPBUT:-1, REPBUT:os.texrep, 		$
	        FILENAME:os.texdsn, DEFNAME:os.texdef, MESSAGE:os.texmes }
    base = widget_base(cwid,/row,/frame)
    paperid = cw_adas_outfile(base, OUTPUT='Text Output', 		$
                              VALUE=outfval, FONT=font_large)

		;**** Error message ****

    messid = widget_label(cwid,value=' ', font=font_large)

		;**** add the exit buttons ****

    base = widget_base(cwid, /row)
    menufile = bitfile + '/menu.bmp'
    read_X11_bitmap, menufile, bitmap1
    outid = widget_button(base, value=bitmap1)		;menu button
    cancelid = widget_button(base, value='Cancel', font=font_large)
    doneid = widget_button(base, value='Done', font=font_large)

		;*************************************************
		;**** create a state structure for the pop-up ****
		;**** window.                                 ****
		;*************************************************

    new_state = { 	PAPERID		:	paperid,		$
		  	CANCELID	:	cancelid, 		$
			DONEID		:	doneid, 		$
			MESSID		:	messid,			$
			TITLEID		:	titleid,		$
			OUTID		:	outid,			$
			TEBASE		:	tebase,			$
			DENBASE		:	denbase,		$
			BEAMBASE	:	beambase,		$
			GRAPFLAG	:	grapflag,		$
			PLOTBASE	:	plotbase,		$
			BEAMDEF		:	beamdef,		$
			BEAMBASE3B	:	beambase3b,		$
			BEYMINBUT	:	beyminbut,		$
			BEYMAXBUT	:	beymaxbut,		$
			BEXMINBUT	:	bexminbut,		$
			BEXMAXBUT	:	bexmaxbut,		$
			DENDEF		:	dendef,			$
			DENBASE3B	:	denbase3b,		$
			DEYMINBUT	:	deyminbut,		$
			DEYMAXBUT	:	deymaxbut,		$
			DEXMINBUT	:	dexminbut,		$
			DEXMAXBUT	:	dexmaxbut,		$
			TEDEF		:	tedef,			$
			TEBASE3B	:	tebase3b,		$
			TEYMINBUT	:	teyminbut,		$
			TEYMAXBUT	:	teymaxbut,		$
			TEXMINBUT	:	texminbut,		$
			TEXMAXBUT	:	texmaxbut,		$
			HARDBASE	:	hardbase,		$
			HARDBASE2	:	hardbase2,		$
			HARDID		:	hardid,			$
			GRPOUT		:	os.grpout,		$
			DEVID		:	devid			}

                ;**** Save initial state structure ****

    widget_control, parent, set_uvalue=new_state, /no_copy

    RETURN, cwid

END

