; Copyright (c) 1995, Strathclyde University 
; SCCS INFO : Module @(#)$Header: /home/adascvs/idl/adas3xx/adas304/c4ispf.pro,v 1.2 2004/07/06 11:51:21 whitefor Exp $ Date $Date: 2004/07/06 11:51:21 $
;
;+
; PROJECT:
;       ADAS IBM MVS to UNIX conversion
;
; NAME:
;	C4ISPF
;
; PURPOSE:
;	IDL user interface and communications with ADAS304 FORTRAN
;	process via pipe.
;
; EXPLANATION:
;	The routine begins by reading information from the ADAS304
;	FORTRAN process via a UNIX pipe.  Then part of the ADAS304
;	IDL user interface is invoked to determine how the user
;	wishes to process the input dataset.  When the user's
;	interactions are complete the information gathered with
;	the user interface is written back to the FORTRAN process
;	via the pipe.  Communications are to the FORTRAN subroutine
;	C4ISPF.
;
; USE:
;	The use of this routine is specific to ADAS304, see adas304.pro.
;
; INPUTS:
;	PIPE	- The IDL unit number of the bi-directional pipe to the
;		  ADAS304 FORTRAN process.
;
;	PROCVAL	- A structure which determines the initial settings of
;		  the processing options widget.  The initial value is
;		  set in adas304.pro.  If adas304.pro passes a blank 
;		  dummy structure of the form {NEW:-1} into PROCVAL then
;		  PROCVAL is reset to a default structure.
;
;		  The PROCVAL structure is;
;			procval = {                        	$
; 		                      	new   : 0,              $
;              		         	title : '',             $
;					sifrac: fltarr(10),	$
;					itzm  : intarr(10),	$
;					tsymm : strarr(10),	$
;		                        ubmeng: ubmeng,         $
;		                        utdens: utdens,         $
;		                        uttemp: uttemp,         $
;		                        nreq  : 0,              $
;		                        nbe   : nbe,            $
;		                        ntdens: ntdens,         $
;		                        nttemp: nttemp,         $
;		                        beref : beref,          $
;		                        tdref : tdref,          $
;		                        ttref : ttref,          $
;		                        bemnmx: bemnmx,         $
;		                        tdmnmx: tdmnmx,         $
;		                        ttmnmx: ttmnmx,         $
;		                        be    : be,             $
;		                        tdens : tdens,          $
;		                        ttemp : ttemp,          $
;					ifit  : 0,		$
;				     num_files: 0,		$
;	                       		lfsel : 0,              $
;                       		tolval: '5'             $
;             			  }
;
;		  See cw_adas304_proc.pro for a full description of this
;		  structure.
;	
;
;       BITFILE - String; the path to the dirctory containing bitmaps
;                 for the 'escape to series menu' button.
;
;       STOPSTRING - String; the list of stopping ions chosen.
;
;	ITZM	   - Int array; the charges of the stopping ions
;
;	TSYMM	   - String array; the symbols of the stopping ions
;
;	NUM_FILES  - Integer; number of stopping ions chosen by the user.
;
; OPTIONAL INPUTS:
;	None
;
; OUTPUTS:
;	LPEND	- Integer, 0 if the user pressed the 'Done' button
;		  or 1 if the user pressed 'Cancel'.  Maps directly onto
;		  the logical variable LPEND in ADAS304 FORTRAN.
;
;	PROCVAL	- On output the structure records the final settings of
;		  the processing selections widget if the user pressed the
;		  'Done' button, otherwise it is not changed from input.
;
;       GOMENU  - Int; flag - set to 1 if user has selected 'escape direct
;                 to series menu , otherwise 0
;
; OPTIONAL OUTPUTS:
;	None
;
; KEYWORD PARAMETERS:
;	FONT_LARGE - The name of a larger font.
;
;	FONT_SMALL - The name of a smaller font.
;
;	EDIT_FONTS - A structure of two fonts used in the table editor
;		     adas_edtab.pro, {font_norm:'',font_input:''}.
;
; CALLS:
;	ADAS304_PROC	Invoke the IDL interface for ADAS304 data
;		 	processing options/input.
;
; SIDE EFFECTS:
;	Two way communications with ADAS304 FORTRAN via the
;	bi-directional UNIX pipe.
;
; CATEGORY:
;	Adas system.
;
; WRITTEN:
;       Tim Hammond, Tessella Support Services plc, 21st November 1995
;
; MODIFIED:
;	1.1     Tim Hammond
;		First version.
; 	1.2 	Richard Martin
;		Changed to 'LPEND=1' when 'MENU' pressed.
;
; VERSION:
;	1.1		21-11-95
;	1.2		28-02-97
;
;-----------------------------------------------------------------------------


PRO c4ispf, pipe, lpend, procval, bitfile, gomenu, stopstring, 		$
	    itzm, tsymm, num_files, FONT_LARGE=font_large, 		$
            FONT_SMALL=font_small, EDIT_FONTS=edit_fonts


                ;**** Set defaults for keywords ****

    IF NOT (KEYWORD_SET(font_large)) THEN font_large = ''
    IF NOT (KEYWORD_SET(font_small)) THEN font_small = ''
    IF NOT (KEYWORD_SET(edit_fonts)) THEN 				$
    edit_fonts = {font_norm:'',font_input:''}

		;*********************************************
		;**** Declare variables for input,        ****
		;**** arrays will be declared after sizes ****
		;**** have been read.                     ****
                ;*********************************************

    lpend = 0				;'done' flag
    itemp =0				;Dummy int var used for reading
    ftemp =0.0				;Dummy flt var used for reading

		;********************************
		;**** Read data from fortran ****
		;********************************

    readf, pipe, itemp
    mxreq = itemp			;max. no. of output triplets
    readf, pipe, itemp
    nbe = itemp
    readf, pipe, itemp
    ntdens = itemp
    readf, pipe, itemp
    nttemp = itemp
    readf, pipe, ftemp
    beref = ftemp			;reference beam energy
    readf, pipe, ftemp			
    tdref = ftemp			;reference ion density
    readf, pipe, ftemp
    ttref = ftemp			;reference ion temp.
    ubmeng = fltarr(mxreq)		;user's output beam energies
    utdens = fltarr(mxreq)		;user's output ion densities
    uttemp = fltarr(mxreq)		;user's output ion temps.
    bemnmx = fltarr(4)			;input range for beam energies
    tdmnmx = fltarr(4) 			;input range for ion densities
    ttmnmx = fltarr(4) 			;input range for ion temps.
    for i=0, 3 do begin
	readf, pipe, ftemp
	bemnmx(i) = ftemp
	readf, pipe, ftemp
   	tdmnmx(i) = ftemp
	readf, pipe, ftemp
	ttmnmx(i) = ftemp
    endfor
    be = fltarr(nbe)			;Beam energies from file
    tdens = fltarr(ntdens)		;Target densities from file
    ttemp = fltarr(nttemp)		;Target temps. from file
    for i=0, nbe-1 do begin
	readf, pipe, ftemp
    	be(i) = ftemp
    endfor
    for i=0, ntdens-1 do begin
	readf, pipe, ftemp
    	tdens(i) = ftemp
    endfor
    for i=0, nttemp-1 do begin
	readf, pipe, ftemp
    	ttemp(i) = ftemp
    endfor

		;*******************************************
		;**** Set default value if non provided ****
		;*******************************************
    if (procval.new lt 0) then begin
        procval = {		        				$
		  	new   		: 	0 ,           		$
		  	title 		: 	'',			$
                        sifrac          :       fltarr(10),             $
                        itzm            :       intarr(10),             $
                        tsymm           :       strarr(10),             $
			ubmeng		:	ubmeng,			$
			utdens		:	utdens,			$
			uttemp		:	uttemp,			$
			nreq		:	0,			$
			nbe		:	nbe,			$
			ntdens		:	ntdens,			$
			nttemp		:	nttemp,			$
			beref		:	beref,			$
			tdref  		:	tdref,			$
			ttref		:	ttref,			$
			bemnmx		:	bemnmx,			$
			tdmnmx		:	tdmnmx,			$
			ttmnmx		:	ttmnmx,			$
			be		:	be,			$
			tdens		:	tdens,			$
			ttemp		:	ttemp,			$
			ifit		:	0,			$
                        num_files       :       1,                      $
		  	lfsel 		: 	0,			$
		  	tolval		: 	5  			}
    endif
    procval.itzm = itzm
    procval.tsymm = tsymm
    procval.num_files = num_files
    procval.nbe = nbe
    procval.ntdens = ntdens
    procval.nttemp = nttemp
    procval.beref = beref
    procval.tdref = tdref
    procval.ttref = ttref
    procval.bemnmx = bemnmx
    procval.tdmnmx = tdmnmx
    procval.ttmnmx = ttmnmx
    procval.be = be
    procval.tdens = tdens
    procval.ttemp = ttemp

		;****************************************
		;**** Pop-up processing input widget ****
		;****************************************

    adas304_proc, procval, action, bitfile, stopstring, mxreq, 		$
		  FONT_LARGE=font_large, FONT_SMALL=font_small, 	$
		  EDIT_FONTS=edit_fonts

		;********************************************
		;****  Act on the event from the widget  ****
		;**** There are only two possible events ****
		;**** 'Done' and 'Cancel'.               ****
		;********************************************

    if action eq 'Done' then begin
        lpend = 0
    endif else if action eq 'Menu' then begin
        lpend = 1
        gomenu = 1
    endif else begin
        lpend = 1
    endelse

		;*******************************
		;**** Write data to fortran ****
		;*******************************

    printf, pipe, lpend
    if lpend eq 0 then begin
        printf, pipe, procval.title
	for i=0, num_files-1 do begin
	    printf, pipe, procval.sifrac(i)
	endfor
        printf, pipe, procval.lfsel
	if procval.lfsel eq 1 then printf, pipe, procval.tolval
        printf, pipe, procval.ifit+1
        printf, pipe, procval.nreq
	for i=0, procval.nreq-1 do begin
	    printf, pipe, procval.ubmeng(i)
	    printf, pipe, procval.utdens(i)
	    printf, pipe, procval.uttemp(i)
	endfor
    endif

END
