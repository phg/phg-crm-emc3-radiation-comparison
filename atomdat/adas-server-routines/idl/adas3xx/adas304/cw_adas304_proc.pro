; Copyright (c) 1995, Strathclyde University.
; SCCS info: Module @(#)cw_adas304_proc.pro	1.6 Date 03/18/02
;+
; PROJECT:
;       ADAS IBM MVS to UNIX conversion
;
; NAME:
;	CW_ADAS304_PROC()
;
; PURPOSE:
;	Produces a widget for ADAS304 processing options/input.
;
; EXPLANATION:
;	This function creates a compound widget consisting of :-
;	   a text widget holding an editable 'Run title', 
;	   a widget to request a mimax fit and enter a tolerance for it,
;	   a table widget holding the output values.
;	   a table widget holding stopping ion fractions,
;	   a message widget, and 'Cancel', 'Menu' and 'Done' buttons.
;
;	The compound widgets included in this widget are self managing.
;	This widget only manages events from the two 'Defaults' buttons,
;	the 'Cancel' button, the 'Menu' button and the 'Done' button.
;
;	This widget only generates events for the 'Done', 'Menu' and 
;	'Cancel' buttons.
;
; USE:
;	This widget is specific to ADAS304, see adas304_proc.pro
;	for use.
;
; INPUTS:
;	TOPPARENT  - Long integer, ID of parent widget.
;
;       BITFILE    - String; the path to the directory containing bitmaps
;                    for the 'escape to series menu' button.
;
;	STOPSTRING - String; the list of stopping ions chosen.
;
;	MXREQ	   - Integer; max no. of user-entered data triplets 
;		     allowed.
;
; OPTIONAL INPUTS:
;	None
;
; OUTPUTS:
;	The return value of this function is the ID of the declared widget.
;
; OPTIONAL OUTPUTS:
;	None
;
; KEYWORD PARAMETERS:
;	PROCVAL	- A structure which determines the initial settings of
;		  the processing options widget.
;
;		  The default PROCVAL is created thus;
;
;		enarr = fltarr(ndein)
;     		ps = {proc304_set, $
;			new   : 0,		$
;                	title : '',             $
;                       sifrac: fltarr(10),     $
;                       itzm  : intarr(10),     $
;                       tsymm : strarr(10),     $
;			ubmeng: fltarr(25),     $         
;                       utdens: fltarr(25),     $
;                       uttemp: fltarr(25),     $
;                       nreq  : 0,              $
;                       nbe   : 0,              $
;                       ntdens: 0,              $
;                       nttemp: 0,              $
;                       beref : 0.0,            $
;                       tdref : 0.0,            $
;                       ttref : 0.0,            $
;                       bemnmx: fltarr(4),      $
;                       tdmnmx: fltarr(4),      $
;                       ttmnmx: fltarr(4),      $
;                       be    : fltarr(25),     $
;                       tdens : fltarr(25),     $
;                       ttemp : fltarr(25),     $
;			ifit  : 0,		$
;		    num_files : 0,		$
;                	lfsel : 0,              $
;                	tolval: '5'		$
;             }
;
;
;		NEW     Flag which defines whether or not default values
;			exist or not. (< 0 if no values exist)
;
; 		TITLE	Entered general title for program run
;
;		SIFRAC  Float array; the fractions for each stopping ion
;
;		ITZM    Integer array; the charges on the stopping ions
;
;		TSYMM   String array; the symbols of the stopping ions
;			
;		UBMENG  Float array; user entered output beam energies
;
;		UTDENS  Float array; user entered output target densities
;
;		UTTEMP  Float array; user entered output target temps.
;
;		NREQ    Integer; Number of energy/dens/temp output triplets
;
;		NBE     Integer; Number of input beam energies
;
;		NTDENS  Integer; Number of input beam densities
;
;		NTTEMP  Integer; Number of input beam temperatures
;
;		BEREF   Float; reference beam energy - read from first 
;			input data file
;
;		TDREF   Float; reference target density - read from first
;                       input data file
;
;               TTREF   Float; reference target temp - read from first
;                       input data file
;
;               BEMNMX  Float array; ranges of values for minimum and 
;			maximum input beam energies found amongst the
;			data files
;
;		TDMNMX  Float array; ranges of values for minimum and
;                       maximum input target densities found amongst the
;                       data files
;
;               TTMNMX  Float array; ranges of values for minimum and
;                       maximum input target temps found amongst the
;                       data files
;
;               BE	Float array; input beam energies from first file
;
;		TDENS	Float array; input target densities from first file.
;
;		TTEMP	Float array; input target temps from first file.
;
;		IFIT    Flag showing which co-ordinate is required on output
;			graph. 0=Energy, 1=Density, 2=Temperature
;
;	     NUM_FILES  Number of stopping ions chosen by the user
;
;		LFSEL   Flag as to whether polynomial fit is chosen
;
;		TOLVAL  Tolerance required for goodness of fit if
;			polynomial fit is selected.
;
;		All of these structure elements map onto variables of
;		the same name in the ADAS304 FORTRAN program.
;
;
;	UVALUE	- A user value for the widget. Default 0.
;
;	FONT_LARGE - The name of a larger font.  Default current system
;		     font
;
;	FONT_SMALL - The name of a smaller font. Default current system
;		     font.
;
;	EDIT_FONTS - A structure of two fonts used in the table editor
;		     adas_edtab.pro, {font_norm:'',font_input:''}.  Defaults
;		     to current system font.
;
;	NUM_FORM   - String; Numeric format to use in tables.  Default
;			'(E10.3)'
;		     
;
; CALLS:
;	CW_ADAS_TABLE	Adas data table widget.
;	CW_OPT_VALUE    Adas option selection widget with required input 
;			value. 
;       READ_X11_BITMAP Reads in the bitmap for the 'escape to series
;                       menu' button.
;       NUM_CHK         Checks validity of a user's numbers
;
;	See side effects for other related widget management routines.
;
; SIDE EFFECTS:
;
;	The following widget management routines are included in this file;
;	PROC304_GET_VAL()	Returns the current PROCVAL structure.
;	PROC304_EVENT()		Process and issue events.
;	
; CATEGORY:
;	Compound Widget
;
; WRITTEN:
;       Tim Hammond, Tessella Support Services plc, 23-Nov-1995
;
; MODIFIED:
;	1.1	Tim Hammond				
;		First version
;	1.2	Tim Hammond
;		Initial fully-working version
;	1.3	Tim Hammond
;		Tidied up comments and code
;	1.4	Harvey Anderson & Richard Martin
;		Increased the size of the array be from 15 to 20.
;		Changed the text associated with the text editor.
;		Changed to prevent running 'sort' on one row of input data.
;	1.5	Richard Martin
;		Array sizes of ubmeng, utdens, uttemp set to be mxreq, which
;		is set in adas304.for.
;	1.6	Richard Martin
;		IDL 5.5 updates.
;	1.7	Martin O'Mullane
;		Change energy label in table to eV/amu from the incorrect eV.
;
; VERSION:
;	1.1	23-11-95
;	1.2	29-11-95
;	1.3	11-12-95
;	1.4	26-03-97
;	1.5	16-09-97
;	1.6	08-03-02
;	1.7	27-05-2004
;-
;-----------------------------------------------------------------------------

FUNCTION proc304_get_val, id

    COMMON cw_proc304_blk, itzm, tsymm, num_files, beref, tdref, ttref,	$
                           nbe, ntdens, nttemp, bemnmx, tdmnmx, ttmnmx, $
                           be, tdens, ttemp


                ;**** Return to caller on error ****

    ON_ERROR, 2

		 ;***************************************
                 ;****     Retrieve the state 	     ****
		 ;**** Get first_child widget id     ****
		 ;**** because state is stored there ****
		 ;***************************************

    first_child = widget_info(id, /child)
    widget_control, first_child, get_uvalue = state

		;***********************************
		;**** Get run title from widget ****
		;**** Then centre in in string  ****
		;**** of 40 characters 		****
		;***********************************

    widget_control, state.runid, get_value=title
    title_len = strlen(strtrim(title(0),2)) 
    if (title_len gt 40) then begin 
	title = strmid(title,0,38)
        widget_control, state.messid, set_value="Title too long - truncated "
        widget_control, state.runid, set_value=title
	wait, 1
    endif
    pad = (40 - title_len)/2 
    spaces = '                                         '
    title(0) = strmid(spaces,0,(pad-1)) + title(0) + strmid(spaces,0,(pad-1))
  

		;***********************************************
		;**** Get new output data from table widget ****
		;***********************************************

    widget_control, state.tabid, get_value=tempval
    tabledata = tempval.value

		;**** Copy out beam energy values ****

    ubmeng = fltarr(state.mxreq)
    utdens = fltarr(state.mxreq)
    uttemp = fltarr(state.mxreq)
    blanks = where(strtrim(tabledata(0,*),2) eq '')

		;**** next line assumes that all blanks are ****
		;**** at the end of the columns             ****

    if blanks(0) ge 0 then nreq=blanks(0) else nreq=state.mxreq

                ;**** Only perform following if there is 1 or ****
                ;**** more energy present in the table        ****

    if (nreq ge 1) then begin
        ubmeng(0:nreq-1) = float(tabledata(0,0:nreq-1))
        utdens(0:nreq-1) = float(tabledata(2,0:nreq-1))
        uttemp(0:nreq-1) = float(tabledata(4,0:nreq-1))
    endif

		;**** Fill out the rest with zero ****

    if nreq lt state.mxreq then begin
        ubmeng(nreq:*) = float(0.0)
        utdens(nreq:*) = float(0.0)
        uttemp(nreq:*) = float(0.0)
    endif
    
		;*************************************************
		;**** Get selection of polyfit from widget    ****
		;*************************************************

    widget_control,state.optid,get_uvalue=polyset,/no_copy
    lfsel = polyset.optionset.option[0]
    if (num_chk(polyset.optionset.value[0]) eq 0) then begin
        tolval = (polyset.optionset.value[0])
    endif else begin
        tolval = -999
    endelse
    widget_control, state.optid, set_uvalue=polyset, /no_copy

		;************************************
		;**** Get stopping ion fractions ****
		;************************************

    widget_control, state.fracid, get_value=fractable
    sifrac = fltarr(10)
    sifrac(0:num_files-1) = float(fractable.value(2,0:num_files-1))

		;**** Get coordinate for output graph ****

    widget_control, state.coordid, get_value=ifit

		;***********************************************
		;**** write selected values to PS structure ****
		;***********************************************

     ps = { 		new   		: 	0, 			$
                	title 		: 	title,          	$
			sifrac		:	sifrac,			$
			itzm		:	itzm,			$
			tsymm		:	tsymm,			$
                        ubmeng          :       ubmeng,                 $
                        utdens          :       utdens,                 $
                        uttemp          :       uttemp,                 $
                        nreq            :       nreq,                   $
                        nbe             :       nbe,                    $
                        ntdens          :       ntdens,                 $
                        nttemp          :       nttemp,                 $
                        beref           :       beref,                  $
                        tdref           :       tdref,                  $
                        ttref           :       ttref,                  $
                        bemnmx          :       bemnmx,                 $
                        tdmnmx          :       tdmnmx,                 $
                        ttmnmx          :       ttmnmx,                 $
			be		:	be,			$
			tdens		:	tdens,			$
			ttemp		:	ttemp,			$
			ifit		:	ifit[0],			$
			num_files	:	num_files,		$
                	lfsel 		: 	lfsel,              	$
                	tolval		: 	tolval			}
   
    widget_control, first_child, set_uvalue=state, /no_copy

    RETURN, ps

END

;-----------------------------------------------------------------------------

FUNCTION proc304_event, event

    COMMON cw_proc304_blk, itzm, tsymm, num_files, beref, tdref, ttref,	$
                           nbe, ntdens, nttemp, bemnmx, tdmnmx, ttmnmx, $
                           be, tdens, ttemp

                ;**** Base ID of compound widget ****

    parent=event.handler

		;**********************************************
                ;**** Retrieve the user value state        ****
		;**** Get id of first_child widget because ****
		;**** user value "state" is stored there   ****
		;**********************************************

    first_child = widget_info(parent, /child)
    widget_control, first_child, get_uvalue=state,/no_copy
  
		;*********************************
		;**** Clear previous messages ****
		;*********************************

    widget_control,state.messid,set_value=' '

                ;************************
                ;**** Process events ****
                ;************************

    CASE event.id OF

                ;**** Clear table button ****

        state.clearid: begin

                ;**** popup window to confirm ****

            action = popup(message='Are you sure you want to clear the table?',$
                           buttons=['Confirm','Cancel'], font=state.font,$
                           title='ADAS304 Warning:-')

            if action eq 'Confirm' then begin

                ;**** Get current table widget value ****

                widget_control, state.tabid, get_value=tempval

                ;**** Zero the relevant columns ****

                empty = strarr(state.mxreq)
		tempval.value(0,*) = empty
		tempval.value(2,*) = empty
		tempval.value(4,*) = empty
                state.nreq = 0

                ;**** Copy new data to table widget ****

                widget_control, state.tabid, set_value=tempval

            endif

        end

		;*************************************
		;**** Default temperature button ****
		;*************************************

    	state.deftid: begin

 		;**** popup window to confirm overwriting current values ****

            action= popup(message='Confirm Overwrite values with Defaults', $
	                  buttons=['Confirm','Cancel'], font=state.font,    $
		          title='ADAS304 Warning:-')

	    if action eq 'Confirm' then begin

		;**** Get current table widget value ****

	     	widget_control, state.tabid, get_value=tempval

                ;***********************************************
                ;**** Copy defaults into value structure    ****
                ;**** For default values use reference vals ****
                ;***********************************************

		;**** First get output coords setting ****

		widget_control, state.coordid, get_value=coords

		;**** If beam energies selected then fill in  ****
		;**** these from file and set temperature and ****
		;**** density to the reference values         ****

		if coords eq 0 then begin
		    tempval.value(0,0:nbe-1) = 				$
		    string(be, format=state.num_form)
		    tempval.value(2,0:nbe-1) = 				$
		    string(tdref, format=state.num_form)
		    tempval.value(4,0:nbe-1) = 				$
		    string(ttref, format=state.num_form)
		    if nbe lt state.mxreq then begin
			tempval.value(0,nbe:*) = ''
			tempval.value(2,nbe:*) = ''
			tempval.value(4,nbe:*) = ''
		    endif

                ;**** If densities selected then fill in      ****
                ;**** these from file and set temperature and ****
                ;**** beam energies to the reference values   ****

		endif else if coords eq 1 then begin
		    tempval.value(0,0:ntdens-1) =               	$
                    string(beref, format=state.num_form)
                    tempval.value(2,0:ntdens-1) =               	$
                    string(tdens, format=state.num_form)
                    tempval.value(4,0:ntdens-1) =                	$
                    string(ttref, format=state.num_form)
                    if ntdens lt state.mxreq then begin
                        tempval.value(0,ntdens:*) = ''
                        tempval.value(2,ntdens:*) = ''
                        tempval.value(4,ntdens:*) = ''
                    endif

                ;**** If temperatures selected then fill in   ****
                ;**** these from file and set densities and   ****
                ;**** beam energies to the reference values   ****

		endif else begin
		    tempval.value(0,0:nttemp-1) =                       $
                    string(beref, format=state.num_form)
                    tempval.value(2,0:nttemp-1) =                       $
                    string(tdref, format=state.num_form)
                    tempval.value(4,0:nttemp-1) =                       $
                    string(ttemp, format=state.num_form)
                    if nttemp lt state.mxreq then begin
                        tempval.value(0,nttemp:*) = ''
                        tempval.value(2,nttemp:*) = ''
                        tempval.value(4,nttemp:*) = ''
                    endif
		endelse

		;**** Copy new data to table widget ****

             	widget_control, state.tabid, set_value=tempval

	    endif
    end

		;***********************
		;**** Cancel button ****
		;***********************

    state.cancelid: new_event = {ID:parent, TOP:event.top, 		$
				 HANDLER:0L, ACTION:'Cancel'}

                ;*********************
                ;**** Menu button ****
                ;*********************

    state.outid: new_event = {ID:parent, TOP:event.top, 		$
		              HANDLER:0L, ACTION:'Menu'}

		;*********************
		;**** Done button ****
		;*********************

    state.doneid: begin

		;***************************************
		;**** Check all user input is legal ****
		;***************************************

	error = 0

        	;********************************************
		;*** Have to restore "state" before calls ***
		;*** to proc304_get_val so it can be used ***
		;*** there.				  ***
		;********************************************
		
	widget_control, first_child, set_uvalue=state, /no_copy

	widget_control, event.handler, get_value=ps

		;*** reset state variable ***

	widget_control, first_child, get_uvalue=state, /no_copy

		;**** Check stopping ion fractions ****

	widget_control, state.fracid, get_value=fractable
   	fracvals = where(strcompress(fractable.value(2,*), /remove_all) ne '')
	if fracvals(0) eq -1 then begin
	    error = 1
	    message = '*** Error: No stopping ion fractions entered ***'
	endif else begin
	    num_fractions = n_elements(fracvals)
	    if num_fractions lt num_files then begin
		error = 1
		message = '*** Error: Stopping ion fractions not entered'+$
                ' for all ions ***'
	    endif else begin
		totalfrac = 0.0
		for i=0, num_files-1 do begin
		    totalfrac = totalfrac + float(fractable.value(2,i))
		endfor
		if totalfrac lt 0.997 or totalfrac gt 1.003 then begin
		    infbutts = [' Cancel ','  OK  ']
		    infomess = ' The stopping ion fractions must be '+	$
                               'renormalised '
		    action = popup(message=infomess, buttons=infbutts,	$
				   font=state.font)
		    if action eq ' Cancel ' then begin
			error = 1
			message = ' '
		    endif else begin
			for i=0, num_files-1 do begin
			    new_val = fractable.value(2,i) / totalfrac
			    fractable.value(2,i) =        		$
			    string(new_val, format='(f5.3)')
			endfor
			if num_files lt 10 then begin
			    fractable.value(2, num_files:*) = ''
			endif
			widget_control, state.fracid, set_value=fractable
		    endelse
		endif
	    endelse
	endelse


		;**** check output values have been entered ****

	if error eq 0 then begin
	    widget_control, state.tabid, get_value=tempval
	    tempvals = 							$
            where(strcompress(tempval.value(0,*), /remove_all) ne '')
	    if tempvals(0) eq -1 then begin
	        error = 1
	        message='*** Error: No output values entered ***'
	    endif
	endif

		;**** Check that user has not selected the ****
		;**** wrong coordinates			   ****

        if error eq 0 then begin
	    widget_control, state.coordid, get_value=coords
	    if coords(0) eq 0 then begin
		nonzero = 						$
		where(strcompress(tempval.value(0,*),/remove_all) ne '')
		sortvals = float(tempval.value(0, nonzero))
		if n_elements(nonzero) gt 1 then begin
 		 valcheck = where(sortvals(sort(sortvals)) ne   		$
                 sortvals(reverse(sort(sortvals))))
		 if valcheck(0) eq -1 then begin
		    error = 1
		    message = '*** Error: Beam energies must be in '+	$
		              'ascending order ***'
		 endif
		endif
	    endif else if coords(0) eq 1 then begin
                nonzero =                                               $
                where(strcompress(tempval.value(2,*),/remove_all) ne '')
                sortvals = float(tempval.value(2, nonzero))
		if n_elements(nonzero) gt 1 then begin		
                  valcheck = where(sortvals(sort(sortvals)) ne            $
                  sortvals(reverse(sort(sortvals))))
                 if valcheck(0) eq -1 then begin
                    error = 1
                    message = '*** Error: Densities must be in '+   	$
                              'ascending order ***'
                 endif
                endif
	    endif else begin
                nonzero =                                               $
                where(strcompress(tempval.value(4,*),/remove_all) ne '')
                sortvals = float(tempval.value(4, nonzero))
		if n_elements(nonzero) gt 1 then begin
                  valcheck = where(sortvals(sort(sortvals)) ne            $
                  sortvals(reverse(sort(sortvals))))
                 if valcheck(0) eq -1 then begin
                    error = 1
                    message = '*** Error: Temperatures must be in '+   	$
                              'ascending order ***'
                 endif
                endif
	    endelse
        endif

		;*** Check to see if sensible tolerance is selected.

	if (error eq 0 and ps.lfsel eq 1) then begin 			$
	    if (float(ps.tolval) lt 0)  or 				$
            (float(ps.tolval) gt 100)  or				$
	    (num_chk(ps.tolval) ne 0) then begin
	        error = 1
	        message='*** Error: Tolerance for polyfit must be 0-100% ***'
	    endif
	endif

		;**** return value or flag error ****

	if error eq 0 then begin
	    new_event = {ID:parent, TOP:event.top, HANDLER:0L, ACTION:'Done'}
	endif else begin
	    widget_control, state.messid, set_value=message
	    new_event = 0L
        endelse
	
    end

    ELSE: new_event = 0L

    ENDCASE

		;*********************************************
		;*** make "state" available to other files ***
		;*********************************************

    widget_control, first_child, set_uvalue=state,/no_copy

    RETURN, new_event

END

;-----------------------------------------------------------------------------

function cw_adas304_proc, topparent, bitfile, stopstring, mxreq,	$
			  procval=procval, uvalue=uvalue, 		$
			  font_large=font_large, font_small=font_small, $
			  edit_fonts=edit_fonts, num_form=num_form

    COMMON cw_proc304_blk, itzmcom, tsymmcom, num_filescom, berefcom,	$
                           tdrefcom, ttrefcom, nbecom, ntdenscom, 	$
                           nttempcom, bemnmxcom, tdmnmxcom, ttmnmxcom,	$
			   becom, tdenscom, ttempcom

		;**** Set defaults for keywords ****

    if not (keyword_set(uvalue)) then uvalue = 0
    if not (keyword_set(font_large)) then font_large = ''
    if not (keyword_set(font_small)) then font_small = ''
    if not (keyword_set(edit_fonts)) then 				$
	edit_fonts = {font_norm:'',font_input:''}
    if not (keyword_set(num_form)) then num_form = '(E10.3)'
    if not (keyword_set(procval)) then begin
        enarr = fltarr(ndein)
        ps = { 								$
			new   		: 	0, 			$
			title 		: 	'',    			$
			sifrac		:	fltarr(10),		$
			itzm		:	intarr(10),		$
			tsymm		:	strarr(10),		$
                        ubmeng          :       fltarr(mxreq),	   $
                        utdens          :       fltarr(mxreq),	      $
                        uttemp          :       fltarr(mxreq),	      $
                        nreq            :       0,                      $
                        nbe             :       0,                      $
                        ntdens          :       0,                      $
                        nttemp          :       0,                      $
                        beref           :       0.0,                    $
                        tdref           :       0.0,                    $
                        ttref           :       0.0,                    $
                        bemnmx          :       fltarr(4),              $
                        tdmnmx          :       fltarr(4),              $
                        ttmnmx          :       fltarr(4),              $
			be		:	fltarr(mxreq),	      $
			tdens		:	fltarr(mxreq),	      $
			ttemp		:	fltarr(mxreq),	      $
			ifit		:	0,			$
		      	num_files	:	1,			$
                	lfsel 		: 	0,             		$
                	tolval		: 	'5'			}
    endif else begin
        ps = { 								$
			new   		: 	procval.new,   		$
			title 		: 	procval.title, 		$
			sifrac          :       procval.sifrac,		$
                        itzm            :       procval.itzm,		$
                        tsymm           :       procval.tsymm,		$
                        ubmeng          :       procval.ubmeng,         $
                        utdens          :       procval.utdens,         $
                        uttemp          :       procval.uttemp,         $
                        nreq            :       procval.nreq,           $
                        nbe             :       procval.nbe,            $
                        ntdens          :       procval.ntdens,         $
                        nttemp          :       procval.nttemp,         $
                        beref           :       procval.beref,          $
                        tdref           :       procval.tdref,          $
                        ttref           :       procval.ttref,          $
                        bemnmx          :       procval.bemnmx,         $
                        tdmnmx          :       procval.tdmnmx,         $
                        ttmnmx          :       procval.ttmnmx,         $
			be		:	procval.be,		$
			tdens		:	procval.tdens,		$
			ttemp		:	procval.ttemp,		$
			ifit		:	procval.ifit,		$
			num_files       :       procval.num_files,	$
                	lfsel 		: 	procval.lfsel, 		$
                	tolval		: 	procval.tolval 		}
    endelse
    itzmcom = ps.itzm
    tsymmcom = ps.tsymm
    num_filescom = ps.num_files
    berefcom = ps.beref
    tdrefcom = ps.tdref
    ttrefcom = ps.ttref
    nbecom = ps.nbe
    ntdenscom = ps.ntdens
    nttempcom = ps.nttemp
    bemnmxcom = ps.bemnmx
    tdmnmxcom = ps.tdmnmx
    ttmnmxcom = ps.ttmnmx
    becom = ps.be
    tdenscom = ps.tdens
    ttempcom = ps.ttemp


                ;**********************************************
		;**** Assemble stopping ion fraction table ****
                ;**********************************************
		;**** col 1 has ion symbols, col2 has ion  ****
		;**** charges and col 3 (only editable one)****
		;**** has the fractions			   ****
                ;**********************************************

    fracdata = strarr(3, 10)

		;**** Make second letter of any elements small case ****

    elements = strcompress(ps.tsymm(*), /remove_all)
    for i=0, n_elements(elements)-1 do begin
	if strlen(elements(i)) gt 1 then begin
	    dummy_str = strlowcase(strmid(elements(i), 1, 1))
            dummy_str2 = elements(i)
	    strput, dummy_str2, dummy_str, 1
	    elements(i) = dummy_str2
	endif
    endfor
    fracdata(0,*) = '  ' + elements(*)
    fracdata(1,*) = '  ' + strtrim(string(ps.itzm(*)), 2)
    fracvals = where(ps.sifrac ne 0.0)
    if fracvals(0) ne -1 then begin
	fracdata(2,fracvals) =        					$
        strtrim(string(ps.sifrac(fracvals),format='(f5.3)'), 2)
    endif

		;**** fill rest of table with blanks ****

    if ps.num_files lt 10 then begin
	fracdata(*,(ps.num_files):*) = ' '
    endif 

		;****************************************************
 		;**** Assemble beam energy/density/temperature  *****
		;****               table data                  *****
 		;****************************************************
 		;**** The adas table widget requires data to be *****
 		;**** input as strings, so all the numeric data *****
 		;**** has to be written into a string array.    *****
 		;**** col 0 has user beam energy values         *****
		;**** col 1 has reference beam energy values    *****
 		;**** col 2 has user density values             *****
		;**** col 3 has reference density values        *****
		;**** col 4 has user temperature values         *****
		;**** col 5 has reference temperature values    *****
 		;****************************************************

    tabledata = strarr(6, mxreq)

		;**** Copy out temperature & density values ****
		;**** number of temperature and density     ****
		;**** values for this data block            ****

    if (ps.nreq gt 0) then begin
        tabledata(0,*) = 						$
        strtrim(string(ps.ubmeng(*), format=num_form),2)
        tabledata(2,*) = 						$
        strtrim(string(ps.utdens(*), format=num_form),2)
        tabledata(4,*) = 						$
        strtrim(string(ps.uttemp(*), format=num_form),2)
 
		;**** fill rest of table with blanks ****
	if (ps.nreq lt mxreq) then begin
        	blanks = where(ps.ubmeng eq 0.0) 
        	tabledata(0, blanks) = ' ' 
        	blanks = where(ps.utdens eq 0.0) 
        	tabledata(2, blanks) = ' ' 
        	blanks = where(ps.uttemp eq 0.0) 
        	tabledata(4, blanks) = ' '
        endif 
    endif

		;**** Fill in the reference columns ****

    tabledata(1,1) = ' MIN RANGE '
    tabledata(3,1) = ' MIN RANGE '
    tabledata(5,1) = ' MIN RANGE '
    tabledata(1,2) = ' --------- '
    tabledata(3,2) = ' --------- '
    tabledata(5,2) = ' --------- '
    tabledata(1,6) = ' MAX RANGE '
    tabledata(3,6) = ' MAX RANGE '
    tabledata(5,6) = ' MAX RANGE '
    tabledata(1,7) = ' --------- '
    tabledata(3,7) = ' --------- '
    tabledata(5,7) = ' --------- '
    tabledata(1,3) = string(ps.bemnmx(0), format=num_form)
    tabledata(1,4) = string(ps.bemnmx(1), format=num_form)
    tabledata(1,8) = string(ps.bemnmx(2), format=num_form)
    tabledata(1,9) = string(ps.bemnmx(3), format=num_form)
    tabledata(3,3) = string(ps.tdmnmx(0), format=num_form)
    tabledata(3,4) = string(ps.tdmnmx(1), format=num_form)
    tabledata(3,8) = string(ps.tdmnmx(2), format=num_form)
    tabledata(3,9) = string(ps.tdmnmx(3), format=num_form)
    tabledata(5,3) = string(ps.ttmnmx(0), format=num_form)
    tabledata(5,4) = string(ps.ttmnmx(1), format=num_form)
    tabledata(5,8) = string(ps.ttmnmx(2), format=num_form)
    tabledata(5,9) = string(ps.ttmnmx(3), format=num_form)

		;********************************************************
		;**** Create the 304 Processing options/input window ****
		;********************************************************

		;**** create titled base widget ****

    parent = widget_base(topparent, UVALUE = uvalue, 			$
			 title = 'ADAS304 PROCESSING OPTIONS', 		$
			 EVENT_FUNC = "proc304_event", 			$
			 FUNC_GET_VALUE = "proc304_get_val", 		$
			 /COLUMN)

		;******************************************************
		;**** Create a dummy widget just to hold value of *****
		;**** "state" variable so as not to get confused  *****
		;**** with any other values. Adopt IDL practice   *****
		;**** of using first child widget                 *****
		;******************************************************

    first_child = widget_base(parent)

    topbase = widget_base(first_child, /column)

		;***********************
		;**** add run title ****
		;***********************

    base = widget_base(topbase, /row)
    rc = widget_label(base, value='Title for run: ', font=font_large)
    runid = widget_text(base, value=ps.title, xsize=38, font=font_large, /edit)

		;*******************************
		;**** add stopping ion list ****
		;*******************************

    listbase = widget_base(topbase, /row)
    stopvalue = 'Stopping ion list:' + stopstring
    rc = widget_label(listbase, value=stopvalue, font=font_large)


		;**********************
		;**** Another base ****
		;**********************

    tablebase = widget_base(parent, /row)

		;************************************************
		;**** base for the energy/density/temp table ****
		;************************************************

    enbase = widget_base(tablebase, /column, /frame)

		;**** first the co-ordinate selector ****

    selectbase = widget_base(enbase, /frame, /column)
    selectbase2 = widget_base(enbase, /frame, /column)
    selectlabel = widget_label(selectbase, font=font_large, 		$
    value='Select co-ordinate type for output graph:- ')
    selections = ['Energy ','Density ','Temperature ']
    coordid = cw_bgroup(selectbase, selections, font=font_small,	$
                        set_value=ps.ifit, /exclusive)
    coorlabel = 'Output values: '
    tablehead = [['Output','Input','Output','Input','Output','Input'],	$
                 ['Beam','Beam','Electron','Electron','Electron','Electron'],		$
                 ['Energies','Energies','Densities','Densities',	$
		  'Temperatures','Temperatures'],			$
                 ['(Units : eV/amu)','(Units : eV/amu)','(Units : cm-3)',	$
                  '(Units : cm-3)','(Units : eV)','(Units : eV)']]
    formats = [num_form, '(a)', num_form, '(a)', num_form, '(a)']

                ;**************************
                ;**** the table itself ****
                ;**************************

    tabid = cw_adas_table(selectbase2, tabledata, TITLE=coorlabel,	$
                          FONTS=edit_fonts, FONT_LARGE=font_large,	$    
         		  /SCROLL, FONT_SMALL=font_small,		$
			  COLEDIT=[1,0,1,0,1,0], xtexsize=56,		$
 			  COLHEAD=tablehead, FLTINT=formats)

 		;*******************************
		;**** Default/Clear buttons ****
		;*******************************

    tbase = widget_base(selectbase2, /row)
    deftid = widget_button(tbase, value=' Default Output Values  ',$
 	                   font=font_large)
    clearid = widget_button(tbase, font=font_large,                     $
                            value='     Clear Table     ')


		;**************************************************
		;**** base for the stopping ion fraction table ****
		;**************************************************

    fracbase = widget_base(tablebase, /column, /frame)
    fractitle = [[' '],['Stopping ion fractions: ']]
    frachead = [[' Ion',' Ion',''],['Symbol','Charge','Fraction'],	$
	        ['','','']]
    formats = ['(a)','(i2)','(f5.3)']

		;**************************
		;**** the table itself ****
		;**************************

    fracid = cw_adas_table(fracbase, fracdata, TITLE=fractitle,		$
                           COLHEAD=frachead, FONTS = edit_fonts,	$
			   FONT_LARGE = font_large, /SCROLL,		$
			   FONT_SMALL = font_small, COLEDIT=[0,0,1],	$
			   LIMITS=[0,0,2], FLTINT=formats)
    footstring = "Note:  Total fraction should = 1.00"
    footstring2 = "(Otherwise values will be renormalised)"
    footlabel = widget_label(fracbase, font=font_large, 		$
		             value=footstring)
    footlabel2 = widget_label(fracbase, font=font_large, 		$
		              value=footstring2)

		;**************************************
		;**** Add polynomial fit selection ****
		;**************************************

    polyset = { option:intarr(1), val:strarr(1)}  ; defined thus because 
						  ; cw_opt_value.pro
                                                  ; expects arrays
    polyset.option = [ps.lfsel]
    polyset.val = [ps.tolval]
    options = ['Fit Polynomial']
    optbase = widget_base(topbase,/frame)
    optid   = cw_opt_value(optbase, options, 				$
			   title='Polynomial Fitting',			$
			   limits = [0,100], 				$
			   value=polyset, font=font_large 		$
			   )

		;**** Error message ****

    messid = widget_label(parent,font=font_large, 			$
    value='Edit the processing options data and press Done to proceed')

		;**** add the exit buttons ****

    base = widget_base(parent, /row)
    menufile = bitfile + '/menu.bmp'
    read_X11_bitmap, menufile, bitmap1
    outid = widget_button(base, value=bitmap1)          ;menu button
    cancelid = widget_button(base, value='Cancel', font=font_large)
    doneid = widget_button(base, value='Done', font=font_large)
  
		;**** create a state structure for the pop-up ****
		;**** 		     window.                  ****

    new_state = { 	runid		:	runid, 			$
		  	messid		:	messid,			$
			outid		:	outid,			$
			fracid		:	fracid,			$
			tabid		:	tabid,			$
 	  		deftid		:	deftid,			$
			clearid		:	clearid,		$
		  	optid		:	optid,			$
		  	cancelid	:	cancelid,		$
		  	doneid		:	doneid, 		$
			coordid		:	coordid,		$
		  	font		:	font_large,		$
			mxreq		:	mxreq,			$
			nreq		:	ps.nreq,		$
		  	num_form	:	num_form		$
	        }

                 ;**** Save initial state structure ****

    widget_control, first_child, set_uvalue=new_state,/no_copy

    RETURN, parent

END
