; Copyright (c) 1995 Strathclyde University .
; SCCS info: Module @(#)$Header: /home/adascvs/idl/adas3xx/adas304/adas304_in.pro,v 1.3 2004/07/06 10:29:44 whitefor Exp $ Date $Date: 2004/07/06 10:29:44 $
;+
; PROJECT:
;       ADAS IBM MVS to DEC UNIX conversion
;
; NAME:
;       ADAS304_IN
;
; PURPOSE:
;       IDL ADAS user interface, input file selection.
;
; EXPLANATION:
;       This routine creates and manages a pop-up window which allows
;       the user to select various input files from the UNIX file system.
;	Like the somewhat related adas405_in.pro upon which it is based,
;	this routine does not conform to the standard adas input
;	window. In this case it is due to the fact that for adas304
;	various input files need to be specified rather than just the
;	single one. 
; USE:
;       See c4spf0.pro for an example of how to
;       use this routine.
;
; INPUTS:
;       INVAL	- A structure which determines the initial settings of
;                 the input screen widgets.
;                 INVAL is passed un-modified through to cw_adas304_in.pro, 
;		  see that routine for a full description.
;
; OPTIONAL INPUTS:
;       None
;
; OUTPUTS:
;       INVAL   - On output the structure records the final settings of
;                 the input screen widgets if the user pressed the 
;		  'Done' button, otherwise it is not changed from input.
;
;       ACTION  - A string; indicates the user's action when the pop-up
;                 window is terminated, i.e which button was pressed to
;                 complete the input.  Possible values are 'Done' and
;                 'Cancel'.
;
;	DATAFLAG- Set to -1 if input data file is changed
;
; OPTIONAL OUTPUTS:
;       None
;
; KEYWORD PARAMETERS:
;       WINTITLE	- Title to be used on the banner for the pop-up window.
;                 	  Intended to indicate which application is running,
;                 	  e.g 'ADAS 304 INPUT'
;
;       FONT_LARGE      - Supplies the large font to be used for the
;                         interface widgets.
;
;       FONT_SMALL      - Supplies the small font to be used for the
;                         interface widgets.
; CALLS:
;       CW_ADAS304_IN   Dataset selection widget creation.
;	DYNLABEL	Sets dynamic_resize keyword for label widgets.
;       XMANAGER        Manages the pop=up window.
;       ADAS304_IN_EV   Event manager, called indirectly during XMANAGER
;                       event management.
;
; SIDE EFFECTS:
;       XMANAGER is called in /modal mode. Any other widget become
;       inactive.
;
; CATEGORY:
;       Adas system.
;
; WRITTEN:
;       Tim Hammond, Tessella Support Services plc, 16th November 1995
;
; MODIFIED:
;       1.1     Tim Hammond
;		First release. Created from skeleton of adas405_in
;	1.2    	William Osborn
;		Added dynlabel procedure
;	1.3	Richard Martin
;		Included dataflag in call statement, and set dataflag=-1 
;		if new input data file is selected.
;
; VERSION:
;       1.1     16-11-95
;	1.2	09-07-96
;	1.3	03-03-97
;
;-----------------------------------------------------------------------------
;-

PRO adas304_in_ev, event

    COMMON in304_blk, action, inval

                ;**** Find the event type and copy to common ****

    action = event.action

    CASE action OF

                ;**** 'Done' button ****

        'Done'  : begin

                ;**** Get the output widget value ****

            widget_control, event.id, get_value=inval
            widget_control, event.top, /destroy

        end


                ;**** 'Cancel' button ****

        'Cancel': widget_control, event.top, /destroy

    ENDCASE

END

;-----------------------------------------------------------------------------


PRO adas304_in, inval, dataflag, action, WINTITLE=wintitle,			$
                FONT_LARGE=font_large, FONT_SMALL=font_small

    COMMON in304_blk, actioncom, invalcom

                ;**** Copy value to common ****

    invalcom = inval

                ;**** Set defaults for keywords ****

    IF NOT (KEYWORD_SET(wintitle)) THEN wintitle = 'ADAS INPUT FILE'
    IF NOT (KEYWORD_SET(font_large)) THEN font_large = ''
    IF NOT (KEYWORD_SET(font_small)) THEN font_small = ''

                ;*********************************
                ;**** Pop-up the input screen ****
                ;*********************************

                ;**** create base widget ****

    inid = widget_base(TITLE=wintitle, XOFFSET=100, YOFFSET=1)

                ;**** create input compound widget ****

    cwid = cw_adas304_in(inid, VALUE=inval, 				$
                         FONT_LARGE=font_large,	FONT_SMALL=font_small)

                ;**** Realize the new widget ****

    dynlabel, inid
    widget_control, inid, /realize

                ;**** make widget modal ****

    xmanager, 'adas304_in', inid, event_handler='adas304_in_ev', 	$
              /modal, /just_reg

    		;*** reset to defaults if new input file selected
    		
    IF invalcom.group NE inval.group THEN BEGIN
    	dataflag=-1
    ENDIF ELSE BEGIN
    	dataflag=0
    ENDELSE
    		
                ;**** Return the output value from common ****

    action = actioncom
    inval = invalcom

END
