; Copyright (c) 1995, Strathclyde University.
;+
; PROJECT:
;       ADAS IBM MVS to UNIX conversion
;
; NAME:
;       ADAS304_PLOT
;
; PURPOSE:
;       Generates ADAS304 graphical output.
;
; EXPLANATION:
;       This routine creates a window for the display of graphical
;       output. A separate routine PLOT304 actually plots the
;       graph.
;       Note that the rather cumbersome method used to generate the
;       string variables which are passed to plot304.pro to be used as
;       labels is necessary because of the absence of a mono-spaced font
;       from the IDL plotting routines.
;
; USE:
;       This routine is specific to ADAS304.
;
; INPUTS:
;
;       UTITLE  - String; Optional comment by user added to graph title.
;
;       LDEF1   - Integer; 1 - use user entered graph scales
;                          0 - use default axes scaling
;
;       X       - 2D Fltarr;  spline x-values
;
;       Y       - 2D Fltarr;  spline y-values
;
;       NREQ    - Integer; number of data pairs
;
;       XFIT    - 2d fltarr; minimax data
;
;       YFIT    - 2d fltarr; minimax data
;
;       NFIT    - Integer; number of minimax points
;
;       IFIT    -       Integer; shows which graph is being plotted
;                       0 = beam energy
;                       1 = target density
;                       2 = target temperature
;
;       NSITYP  -       Integer; number of ions to be plotted
;
;       HRDOUT - Integer; 1 if hardcopy output activated, 0 if not.
;
;       HARDNAME- String; Filename for harcopy output.
;
;       DEVICE  - String; IDL name of hardcopy output device.
;
;       HEADER  - String; ADAS version number header to include in graph.
;
;       TITLEFLAG-  =0 if adf21 data being used
;                   =1 if adf22 beam emission.
;                   =2 if adf22 beam density.
;                   =3 if unknown.
;
;       XMIN1   - String; Lower limit for x-axis of graph, number as string.
;
;       XMAX1   - String; Upper limit for x-axis of graph, number as string.
;
;       YMIN1   - String; Lower limit for y-axis of graph, number as string.
;
;       YMAX1   - String; Upper limit for y-axis of graph, number as string.
;
;       SIFRAC  - Float array; the stopping ion fractions (used in labels)
;
;       TSYMM   - String array; the stopping ion symbols
;
;       ITZM    - Integer array; the stopping ion charges
;
;       BITFILE - String; the path to the directory containing bitmaps
;                 for the 'escape to series menu' button.
;
;       LFSEL   -       Integer; 1 = Minimax plots requested, 0=not
;
;       LINE1   - String; the names of the ions being plotted
;
;       DATASET - String; the name of the data subset being used
;
;       UBMENG  - Float array; user entered output beam energies
;
;       UTDENS  - Float array; user entered output target densities
;
;       UTTEMP  - Float array; user entered output target temperatures
;
;       FINFO   - String array; minimax information strings
;
; OPTIONAL INPUTS:
;       None.
;
; OUTPUTS:
;       GOMENU  - Int; flag - set to 1 if user has selected 'escape direct
;                 to series menu', otherwise 0
;
;       BEAMEVENT-String 'BEAM STOPPING ..' or 'BEAM EMISSION ..' depending
;                 on value of TITLEFLAG
;
; OPTIONAL OUTPUTS:
;       None.
;
; KEYWORD PARAMETERS:
;       FONT    - String; The name of a font to use for text in the
;                 graphical output widget.
;
; CALLS:
;       CW_ADAS_GRAPH   Graphical output widget.
;       PLOT304         Make one plot to an output device for 304
;       XMANAGER
;
; SIDE EFFECTS:
;       This routine uses a common block to maintain its state 
;       PLOT304_BLK.
;
;       One other routine is included in this file;
;       ADAS304_PLOT_EV Called via XMANAGER during widget management.
;
; CATEGORY:
;       Adas system.
;
; WRITTEN:
;       Tim Hammond, Tessella Support Services plc, 7th December 1995
;
; MODIFIED:
;       1.1     Tim Hammond
;               First version
;       1.2     Tim Hammond
;               Modified construction of strings and labels for cases
;               where there is only one ion or when minimax fits are
;               not required.
;       1.3     Tim Hammond
;               Made labels adjust themseleves more to the display
;       1.4     Richard Martin
;               Added TITLEFLAG and BEAMEVENT. Graph title corresponds to
;               whether adf21 or adf22 files are used.
;       1.5     Martin O'Mullane
;               Extend TITLEFLAG to 0-3 and pass it to plot304.pro.
;       1.6     Martin O'Mullane
;               Make sure TITLEFLAG is passed to plot304 when called
;               from print to file part of the code.
;
; VERSION:
;       1.1     11-12-95
;       1.2     26-01-96
;       1.3     06-08-96
;       1.4     12-03-97
;       1.5     10-12-2004
;       1.6     23-10-2008
;
;-
;----------------------------------------------------------------------------

PRO adas304_plot_ev, event

    COMMON plot304_blk, action, plotdev, plotfile, fileopen, win,       $
                          data, gomenu

    newplot = 0
    print = 0
    done = 0
                ;****************************************
                ;**** Set graph and device requested ****
                ;****************************************

    CASE event.action OF

        'print'    : begin
            newplot = 1
            print = 1
        end


        'done'     : begin
            if fileopen eq 1 then begin
                set_plot, plotdev
                device, /close_file
            end
            set_plot,'X'
            widget_control, event.top, /destroy
            done = 1
        end

        'bitbutton' : begin
            if fileopen eq 1 then begin
                set_plot, plotdev
                device, /close_file
            end
            set_plot,'X'
            widget_control, event.top, /destroy
            done = 1
            gomenu = 1
        end

    END

                ;*******************************
                ;**** Make requested plot/s ****
                ;*******************************

    if done eq 0 then begin

                ;**** Set graphics device ****

        if print eq 1 then begin
            set_plot, plotdev
            if fileopen eq 0 then begin
                fileopen = 1
                device, filename=plotfile
                device, /landscape
            endif
        endif else begin
            set_plot,'X'
            wset, win
        endelse

                ;**** Draw graphics ****

        if newplot eq 1 then begin
            plot304, data.ldef1, data.rightstring,                      $
                     data.rightstring2, data.x, data.y, data.nreq,      $
                     data.xfit, data.yfit, data.nfit, data.ifit,        $
                     data.nsityp, data.title, data.xmin1, data.xmax1,   $
                     data.ymin1, data.ymax1, data.lfsel,                $
                     data.rightstring3, data.rightstring4,              $
                     data.rightstring5, data.rightstring6,              $
                     data.rightstring7, data.rightstring8,              $
                     data.rightstring9, data.rightstring10,             $
                     data.rightstring11, data.rightstring12,            $
                     data.rightstring13, data.rightstring14, data.ions, $
                     data.beamevent, data.titleflag
            if print eq 1 then begin
                message = 'Plot  written to print file.'
                grval = {WIN:0, MESSAGE:message}
                widget_control, event.id, set_value=grval
            endif
        endif
    endif

END

;----------------------------------------------------------------------------
                                                                
PRO adas304_plot, utitle, ldef1, x, y, nreq, xfit, yfit, nfit, ifit,    $
                    nsityp, hrdout, hardname, device, header, titleflag,$
                    xmin1, xmax1, ymin1, ymax1, sifrac, tsymm, itzm,    $
                    bitfile, lfsel, gomenu, line1, dataset, ubmeng,     $
                    utdens, uttemp, finfo, FONT=font

    COMMON plot304_blk, action, plotdev, plotfile, fileopen, win,       $
                        data, gomenucom

                ;**** Copy input values to common ****

    plotdev = device
    plotfile = hardname
    fileopen = 0
    gomenucom = gomenu

                ;************************************
                ;**** Create general graph titles****
                ;************************************

                ;**** Header ****
    
    case titleflag of
      0 : beamevent = 'BEAM STOPPING COEFFICIENT'      
      1 : beamevent = 'BEAM EMISSION COEFFICIENT'      
      2 : beamevent = 'BEAM EXCITED POPULATION FRACTION'
      3 : beamevent = 'UNKNOWN COEFFICIENT'
    endcase      

    small_check = GETENV('VERY_SMALL')
    if ifit eq 0 then begin
        title = beamevent + " vs BEAM ENERGY"
    endif else if ifit eq 1 then begin
        title = beamevent + " vs TARGET DENSITY"
    endif else begin
        title = beamevent + " vs TARGET TEMPERATURE"
    endelse
    if ( strtrim(strcompress(utitle),2)  ne ' ' ) then begin
        title = title + ': ' + strupcase(strtrim(utitle,2))
    endif
    if small_check eq 'YES' then title=title+'!C'
    title =  title + '!C!CADAS    : ' + header
    if small_check eq 'YES' then title=title+'!C'
    title =  title + '!CFILE     : ' + dataset
    if small_check eq 'YES' then title=title+'!C'
    title =  title + '!CIONS    :  ' + line1
    if small_check eq 'YES' then title=title+'!C'
    title = title + '!CKEY     :  (CROSSES/FULL LINE - SPLINE)'
    if lfsel eq 1 then title = title + ' (DASH LINE - MINIMAX)'

                ;**** Right-hand side labels ****

    rightstring = ''
    rightstring2 = ''
    rightstring3 = ''
    rightstring4 = ''
    rightstring5 = ''
    rightstring6 = ''
    rightstring7 = ''
    rightstring8 = ''
    rightstring9 = ''
    rightstring10= ''
    rightstring11= ''
    rightstring12= ''
    rightstring13= ''
    rightstring14= ''
    for i=0, (nsityp-1) do begin
        if (i+1) lt 10 then rightstring = rightstring + ' '
        rightstring = rightstring + strtrim(string(i+1),2) + '!C'
        rightstring2 = rightstring2 + strtrim(tsymm(i),2) + '!C'
        if (itzm(i)) lt 10 then rightstring3 = rightstring3 + ' '
        rightstring3 = rightstring3 + strtrim(string(itzm(i)),2) + '!C'
        rightstring4 = rightstring4 + string(sifrac(i), format='(f5.3)') + '!C'
        rightstring5 = rightstring5 + '!C'
        rightstring6 = rightstring6 + '!C'
        rightstring7 = rightstring7 + '!C'
        rightstring8 = rightstring8 + '!C'
        rightstring9 = rightstring9 + '!C'
        rightstring10 = rightstring10 + '!C'
        rightstring11 = rightstring11 + '!C'
        rightstring12 = rightstring12 + '!C'
        rightstring13 = rightstring13 + '!C'
        rightstring14 = rightstring14 + '!C'
        if small_check eq 'YES' then rightstring=rightstring+'!C'
        if small_check eq 'YES' then rightstring2=rightstring2+'!C'
        if small_check eq 'YES' then rightstring3=rightstring3+'!C'
        if small_check eq 'YES' then rightstring4=rightstring4+'!C'
        if small_check eq 'YES' then rightstring5=rightstring5+'!C'
        if small_check eq 'YES' then rightstring6=rightstring6+'!C'
        if small_check eq 'YES' then rightstring7=rightstring7+'!C'
        if small_check eq 'YES' then rightstring8=rightstring8+'!C'
        if small_check eq 'YES' then rightstring9=rightstring9+'!C'
        if small_check eq 'YES' then rightstring10=rightstring10+'!C'
        if small_check eq 'YES' then rightstring11=rightstring11+'!C'
        if small_check eq 'YES' then rightstring12=rightstring12+'!C'
        if small_check eq 'YES' then rightstring13=rightstring13+'!C'
        if small_check eq 'YES' then rightstring14=rightstring14+'!C'
    endfor
    rightstring5 = rightstring5 + '!C!C!C!C!C!C!C!C'
    rightstring6 = rightstring6 + '!C!C!C!C!C!C!C!C'
    rightstring7 = rightstring7 + '!C!C!C!C!C!C!C!C'
    rightstring8 = rightstring8 + '!C!C!C!C!C!C!C!C'
    rightstring9 = rightstring9 + '!C!C!C!C!C!C!C!C'
    rightstring10 = rightstring10 + '!C!C!C!C!C!C!C!C'
    rightstring11 = rightstring11 + '!C!C!C!C!C!C!C!C'
    rightstring12 = rightstring12 + '!C!C!C!C!C!C!C!C'
    rightstring13 = rightstring13 + '!C!C!C!C!C!C!C!C'
    rightstring14 = rightstring14 + '!C!C!C!C!C!C!C!C'
    for i=0, (nreq-1) do begin
        if (i+1) lt 10 then rightstring5 = rightstring5 + ' '
        rightstring5 = rightstring5 + strtrim(string(i+1),2) + '!C'
        rightstring6 = rightstring6 + string(ubmeng(i), format='(E10.3)') + '!C'
        rightstring7 = rightstring7 + string(utdens(i), format='(E10.3)') + '!C'
        rightstring8 = rightstring8 + string(uttemp(i), format='(E10.3)') + '!C'
        rightstring9 = rightstring9 + '!C'
        rightstring10 = rightstring10 + '!C'
        rightstring11 = rightstring11 + '!C'
        rightstring12 = rightstring12 + '!C'
        rightstring13 = rightstring13 + '!C'
        rightstring14 = rightstring14 + '!C'
        if small_check eq 'YES' then rightstring5=rightstring5+'!C'
        if small_check eq 'YES' then rightstring6=rightstring6+'!C'
        if small_check eq 'YES' then rightstring7=rightstring7+'!C'
        if small_check eq 'YES' then rightstring8=rightstring8+'!C'
        if small_check eq 'YES' then rightstring9=rightstring9+'!C'
        if small_check eq 'YES' then rightstring10=rightstring10+'!C'
        if small_check eq 'YES' then rightstring11=rightstring11+'!C'
        if small_check eq 'YES' then rightstring12=rightstring12+'!C'
        if small_check eq 'YES' then rightstring13=rightstring13+'!C'
        if small_check eq 'YES' then rightstring14=rightstring14+'!C'
    endfor
    rightstring9 = rightstring9 + '!C!C!C!C!C!C!C'
    rightstring10 = rightstring10 + '!C!C!C!C!C!C!C'
    rightstring11 = rightstring11 + '!C!C!C!C!C!C!C'
    rightstring12 = rightstring12 + '!C!C!C!C!C!C!C'
    rightstring13 = rightstring13 + '!C!C!C!C!C!C!C'
    rightstring14 = rightstring14 + '!C!C!C!C!C!C!C'
    if lfsel eq 1 then begin
        stype = ' '
        sdeg = 0
        sacc = 0.0
        slower = 0.0
        supper = 0.0
        if small_check eq 'YES' then rightstring9=rightstring9+'!C'
        if small_check eq 'YES' then rightstring10=rightstring10+'!C'
        if small_check eq 'YES' then rightstring11=rightstring11+'!C'
        if small_check eq 'YES' then rightstring12=rightstring12+'!C'
        if small_check eq 'YES' then rightstring13=rightstring13+'!C'
        if small_check eq 'YES' then rightstring14=rightstring14+'!C'
        for i=0, nsityp do begin
            reads, finfo(i), stype, sdeg, sacc, slower, supper,         $
            format='(a6, 10x, i2, 11x, f6.2, 23x, f7.2, 7x, f7.2)'
            if i eq (nsityp) then begin
                rightstring10 = rightstring10 + '!C'
                rightstring11 = rightstring11 + '!C'
                rightstring12 = rightstring12 + '!C'
                rightstring13 = rightstring13 + '!C'
                rightstring14 = rightstring14 + '!C'
            endif
            rightstring10 = rightstring10 + stype + '!C'
            rightstring11 = rightstring11 + strtrim(string(sdeg),2) + '!C'
            rightstring12 = rightstring12 + string(sacc, format='(f4.2)')+ '!C'
            rightstring13 = rightstring13 + string(slower, format='(f5.2)')+'!C'
            rightstring14 = rightstring14 + string(supper, format='(f5.2)')+'!C'
            if small_check eq 'YES' then rightstring10=rightstring10+'!C'
            if small_check eq 'YES' then rightstring11=rightstring11+'!C'
            if small_check eq 'YES' then rightstring12=rightstring12+'!C'
            if small_check eq 'YES' then rightstring13=rightstring13+'!C'
            if small_check eq 'YES' then rightstring14=rightstring14+'!C'
        endfor
        ions = strarr(nsityp)
        for i=0, (nsityp-1) do begin
            if nsityp gt 1 then begin
                ions(i) = strtrim(tsymm(i),2) + strtrim(string(itzm(i)),2)
            endif
            rightstring9 = rightstring9 + strtrim(tsymm(i),2) +         $
                           strtrim(string(itzm(i)),2) + '!C'
            if small_check eq 'YES' then rightstring9=rightstring9+'!C'
        endfor
        rightstring9 = rightstring9 + '!CALL'
    endif else begin
        ions = strarr(nsityp)
        if nsityp gt 1 then begin
            for i=0, (nsityp-1) do begin
                ions(i) = strtrim(tsymm(i),2) + strtrim(string(itzm(i)),2)
            endfor
        endif
    endelse

                ;*************************************
                ;**** Create graph display widget ****
                ;*************************************

    graphid = widget_base(TITLE='ADAS304 GRAPHICAL OUTPUT',             $
                          XOFFSET=1,YOFFSET=1)
    device, get_screen_size = scrsz
    xwidth=scrsz(0)*0.75
    yheight=scrsz(1)*0.75
    bitval = bitfile + '/menu.bmp'
    cwid = cw_adas_graph(graphid, print=hrdout, FONT=font,              $
                         xsize=xwidth, ysize=yheight,                   $
                         bitbutton=bitval)

                ;**** Realize the new widget ****

    widget_control, graphid, /realize

                ;**** Get the id of the graphics area ****

    widget_control, cwid, get_value=grval
    win = grval.win

                ;*******************************************
                ;**** Put the graphing data into common ****
                ;*******************************************

    data = {    X               :       x,                              $
                Y               :       y,                              $
                NREQ            :       nreq,                           $
                XFIT            :       xfit,                           $
                YFIT            :       yfit,                           $
                NFIT            :       nfit,                           $
                IFIT            :       ifit,                           $
                NSITYP          :       nsityp,                         $
                XMIN1           :       xmin1,                          $
                XMAX1           :       xmax1,                          $
                YMIN1           :       ymin1,                          $
                YMAX1           :       ymax1,                          $
                RIGHTSTRING     :       rightstring,                    $
                RIGHTSTRING2    :       rightstring2,                   $
                RIGHTSTRING3    :       rightstring3,                   $
                RIGHTSTRING4    :       rightstring4,                   $
                RIGHTSTRING5    :       rightstring5,                   $
                RIGHTSTRING6    :       rightstring6,                   $
                RIGHTSTRING7    :       rightstring7,                   $
                RIGHTSTRING8    :       rightstring8,                   $
                RIGHTSTRING9    :       rightstring9,                   $
                RIGHTSTRING10   :       rightstring10,                  $
                RIGHTSTRING11   :       rightstring11,                  $
                RIGHTSTRING12   :       rightstring12,                  $
                RIGHTSTRING13   :       rightstring13,                  $
                RIGHTSTRING14   :       rightstring14,                  $
                IONS            :       ions,                           $
                BEAMEVENT       :       beamevent,                      $
                TITLEFLAG       :       titleflag,                      $
                TITLE           :       title,                          $
                LFSEL           :       lfsel,                          $
                LDEF1           :       ldef1                           }

                ;**** Initialise to plot 0 ****
    wset, win

    plot304, ldef1, rightstring, rightstring2, x, y, nreq, xfit,        $
             yfit, nfit, ifit, nsityp, title, xmin1, xmax1, ymin1,      $
             ymax1, lfsel, rightstring3, rightstring4, rightstring5,    $
             rightstring6, rightstring7, rightstring8, rightstring9,    $
             rightstring10, rightstring11, rightstring12, rightstring13,$
             rightstring14, ions, beamevent, titleflag

                ;***************************
                ;**** make widget modal ****
                ;***************************

    xmanager, 'adas304_plot', graphid, /modal, /just_reg,               $
              event_handler='adas304_plot_ev'

    gomenu = gomenucom

END
