;+
; PROJECT:
;       ADAS
;
; NAME:
;       ADAS304
;
; PURPOSE:
;       The highest level routine for the ADAS 304 program.
;
; EXPLANATION:
;       This routine is called from the main adas system routine, adas.pro,
;       to start the ADAS 304 application.  Associated with adas304.pro
;       is a FORTRAN executable.  The IDL code provides the user
;       interface and output graphics whilst the FORTRAN code reads
;       in data files, performs numerical processing and creates the
;       output files.  The IDL code communicates with the FORTRAN
;       executable via a bi-directional UNIX pipe.  The unit number
;       used by the IDL for writing to and reading from this pipe is
;       allocated when the FORTRAN executable is 'spawned' (see code
;       below).  Pipe communications in the FORTRAN process are to
;       stdin and stdout, i.e streams 5 and 6.
;
;       The FORTRAN code is an independent process under the UNIX system.
;       The IDL process can only exert control over the FORTRAN in the
;       data which it communicates via the pipe.  The communications
;       between the IDL and FORTRAN must be exactly matched to avoid
;       input conversion errors.  The correct ammounts of data must be
;       passed so that neither process 'hangs' waiting for communications
;       which will never occur.
;
;       The FORTRAN code performs some error checking which is
;       independent of IDL.  In cases of error the FORTRAN may write
;       error messages.  To prevent these error messages from conflicting
;       with the pipe communications all FORTRAN errors are written to
;       output stream 0, which is stderr for UNIX.  These error messages
;       will appear in the window from which the ADAS session/IDL session
;       is being run.
;
;       In the case of severe errors the FORTRAN code may terminate
;       itself prematurely.  In order to detect this, and prevent the
;       IDL program from 'hanging' or crashing, the IDL checks to see
;       if the FORTRAN executable is still an active process before
;       each group of pipe communications.  The process identifier
;       for the FORTRAN process, PID, is recorded when the process is
;       first 'spawned'.  The system is then checked for the presence
;       of the FORTRAN PID.
;
; USE:
;       First the system settings must be established by calling
;       adas_sys_set.pro then adas304.pro is called to start the
;       ADAS 304 application;
;
;       adas_sys_set, adasrel, fortdir, userroot, centroot, $
;                     devlist, devcode, font_large, font_small, edit_fonts
;       adas304,   adasrel, fortdir, userroot, centroot, devlist, $
;                  devcode, font_large, font_small, edit_fonts
;
; INPUTS:
;       ADASREL - A string indicating the ADAS system version, 
;                 e.g ' ADAS RELEASE: ADAS93 V1.1.0'.  The first
;                 character should be a space.
;
;       FORTDIR - A string holding the path to the directory where the
;                 FORTRAN executables are, e.g '/disk/adas/fortran/exec'
;
;       USERROOT - A string holding the path to the root directory of
;                  the user's adas data. e.g '/disk/bowen/adas'
;                  This root directory will be used by adas to construct
;                  other path names.  For example the users default data
;                  for adas205 should be in /disk/bowen/adas/adf04.  In
;                  particular the user's default interface settings will
;                  be stored in the directory USERROOT+'/defaults'.  An
;                  error will occur if the defaults directory does not
;                  exist.
;
;       CENTROOT - Like USERROOT, but this directory points to the
;                  central data area for the system.  User defaults are
;                  not stored on CENTROOT.
;
;       DEVLIST - A string array of hardcopy device names, used for
;                 graphical output. e.g ['Post-Script','HP-PCL','HP-GL']
;                 This array must mirror DEVCODE.  DEVCODE holds the
;                 actual device names used in a SET_PLOT statement.
;
;       DEVCODE - A string array of hardcopy device code names used in
;                 the SET_PLOT statement, e.g ['PS', 'PCL', 'HP']
;                 This array must mirror DEVLIST.
;
;       FONT_LARGE - The name of a larger font e.g 'courier_bold14'
;
;       FONT_SMALL - The name of a smaller font e.g 'courier_bold12'
;
;       EDIT_FONTS - A structure of two fonts used in the table editor
;                    adas_edtab.pro, {font_norm:'',font_input:''}.
;                    The two fonts are used to differentiate between
;                    editable and non-editable parts of the table. You
;                    may have to experiment to get two fonts which give
;                    the same default spacing in the edit widget.
;                    e.g {font_norm:'helvetica_bold14', $
;                         font_input:'helvetica_oblique14'}
;
; OPTIONAL INPUTS:
;       None
;
; OUTPUTS:
;       None - Note this routine should leave all inputs un-modified.
;
; OPTIONAL OUTPUTS:
;       None
;
; KEYWORD PARAMETERS:
;       None
;
; CALLS:
;       FIND_PROCESS    Checks to see if a given process is active.
;       XXDATE          Get date and time from operating system.
;       C4TYPE          Determine type of coefficient in file.
;       C4SPF0          Pipe comms with FORTRAN C4SPF0 routine.
;       C4ISPF          Pipe comms with FORTRAN C4ISPF routine.
;       C4SPF1          Pipe comms with FORTRAN C4SPF1 routine.
;       C4OUTG          Pipe comms with FORTRAN C4OUTG routine.
;
; SIDE EFFECTS:
;       This routine spawns a FORTRAN executable.  Note the pipe 
;       communications routines listed above. 
;       There are also communications of the variable gomenu to the
;       FORTRAN which is used as a signal to stop the program in its
;       tracks and return immediately to the series menu.
;
; CATEGORY:
;       Adas system.
;       
; WRITTEN:
;       Tim Hammond, Tessella Support Services plc, 16th November 1995
;
; MODIFIED:
;       1.1     Tim Hammond
;               First version 
;       1.2     Tim Hammond
;               Ensured that text output messages are cleared when
;               the return to menu button is used.
;       1.3     Tim Hammond
;               Increased version number to 1.2
;       1.4     William Osborn
;               Updated version number to 1.3
;       1.5     William Osborn
;               Updated version number to 1.4
;       1.6     Harvey Anderson & Richard Martin
;               Detects input file type i.e. 'adf21' or 'adf22' in order
;               to have correct graph titles.
;               Fixed bug which caused a crash when 'menu' was pressed.
;       1.7     Richard Martin
;               Increased vesrion number to 1.6
;       1.8     Richard Martin
;               Increased vesrion number to 1.7
;       1.9     Martin O'Mullane
;                - The logic to determine whether adf21 or adf22 files are
;                  used did not work if only one impurity was selected.
;                  Interrogate the first element of dsfull, not the second!
;                - Increased version number to 1.8.
;       1.10    Martin O'Mullane
;                - Query the input files to determine their type. Expand
;                  to test for bmp type.
;               
;
; VERSION:
;       1.1     16-11-95
;       1.2     06-02-96
;       1.3     13-05-96
;       1.4     11-07-96
;       1.5     14-10-96
;       1.6     26-03-97
;       1.7     01-11-97
;       1.8     18-03-02
;       1.9     18-05-2004
;       1.10    10-12-2004
;
;-----------------------------------------------------------------------------


PRO ADAS304,    adasrel, fortdir, userroot, centroot,                   $
                devlist, devcode, font_large, font_small, edit_fonts

                ;************************
                ;**** Initialisation ****
                ;************************

    adasprog = ' PROGRAM: ADAS304 V1.8'
    lpend = 0
    gomenu = 0
    deffile = userroot+'/defaults/adas304_defaults.dat'
    bitfile = centroot+'/bitmaps'
    device = ''

                ;******************************************
                ;**** Search for user default settings ****
                ;**** If not found create defaults     ****
                ;******************************************

    files = findfile(deffile)
    if files(0) eq deffile then begin
        restore, deffile
        inval.centroot = centroot+'/adf21/'
        inval.userroot = userroot+'/adf21/'
    endif else begin
        inval = {       ROOTPATH        :       userroot+'/adf21/',     $
                        CENTROOT        :       centroot+'/adf21/',     $
                        USERROOT        :       userroot+'/adf21/',     $
                        GROUP           :       ' ',                    $
                        PREFIX          :       ' ',                    $
                        IONS            :       strarr(10),             $
                        STOPSTRING      :       ' ',                    $
                        DSFULL          :       strarr(10)              }
        procval = {     NEW             :       -1                      }
        outval = {      GRPOUT          :       0,                      $
                        GTIT1           :       '',                     $
                        LDEF1           :       0,                      $
                        LDEF2           :       0,                      $
                        LDEF3           :       0,                      $
                        XMIN1           :       '',                     $
                        XMAX1           :       '',                     $
                        YMIN1           :       '',                     $
                        YMAX1           :       '',                     $
                        XMIN2           :       '',                     $
                        XMAX2           :       '',                     $
                        YMIN2           :       '',                     $
                        YMAX2           :       '',                     $
                        XMIN3           :       '',                     $
                        XMAX3           :       '',                     $
                        YMIN3           :       '',                     $
                        YMAX3           :       '',                     $
                        HRDOUT          :       0,                      $
                        HARDNAME        :       '',                     $
                        GRPDEF          :       '',                     $
                        GRPFMESS        :       '',                     $ 
                        DEVSEL          :       -1,                     $
                        TEXOUT          :       0,                      $
                        TEXAPP          :       -1,                     $
                        TEXREP          :       0,                      $
                        TEXDSN          :       '',                     $
                        TEXDEF          :       'paper.txt',            $
                        TEXMES          :       ''                      }
    endelse


                ;****************************
                ;**** Start fortran code ****
                ;****************************

    spawn, fortdir+'/adas304.out', unit=pipe, /noshell, PID=pid

                ;************************************************
                ;**** Get date and write to fortran via pipe ****
                ;************************************************

    date = xxdate()
    printf, pipe, date(0)

LABEL100:
                ;**** Check FORTRAN still running ****
  
    if find_process(pid) eq 0 then goto, LABELEND

                ;************************************************
                ;**** Communicate with c4spf0 in fortran and ****
                ;**** invoke user interface widget for       ****
                ;**** Data file selection                    ****
                ;************************************************

    c4spf0, pipe, inval, dataflag, rep, num_files, FONT_LARGE=font_large,               $
            FONT_SMALL=font_small

                ;***new input data=> dataflag=-1 else dataflag=0
                
    procval.new=dataflag
                
                ;**** If cancel selected then end program ****

    if rep eq 'YES' then goto, LABELEND

                ;**** Check FORTRAN still running ****

    if find_process(pid) eq 0 then goto, LABELEND

                ;**** Call c4chkz to communicate with same    ****
                ;**** FORTRAN routine regarding consistencies ****
                ;**** of names and charges etc.               ****

    c4chkz, pipe, inval, num_files, pid, tsymm, itzm

                ;**** make up the strings line1 and dataset   ****
                ;**** for possible use by text output routine ****

    line1 = ''
    dataset = ''
    for i=0, num_files-1 do begin
        line1 = line1 + strupcase(strcompress(tsymm(i), /remove_all))
        line1 = line1 + strcompress(string(itzm(i)), /remove_all) + ' '
    endfor
    if strcompress(inval.prefix, /remove_all) ne '' then begin
        dataset = dataset + strcompress(inval.prefix, /remove_all) + '#'
    endif
    dataset = dataset + inval.group 

LABEL200:
                ;**** Check FORTRAN still running ****

    if find_process(pid) eq 0 then goto, LABELEND

                ;************************************************
                ;**** Communicate with c4ispf in fortran and ****
                ;**** invoke user interface widget for       ****
                ;**** Processing options                     ****
                ;************************************************

    c4ispf, pipe, lpend, procval, bitfile, gomenu, inval.stopstring,    $
            itzm, tsymm, num_files, FONT_LARGE=font_large,              $
            FONT_SMALL=font_small, EDIT_FONTS=edit_fonts

                ;**** If menu button clicked, tell FORTRAN to stop ****

    if gomenu eq 1 then begin
        printf, pipe, 1
        goto, LABELEND
    endif else begin
        printf, pipe, 0
    endelse
 
                ;**** If cancel selected then goto 100 ****

    if lpend eq 1 then goto, LABEL100

                ;**** Fortran processing now in progress. Get ****
                ;**** date and time for this run.             ****

    date = xxdate()

                ;**** Create header for output. ****

    header = adasrel+adasprog+' DATE: '+date(0)+' TIME: '+date(1)

LABEL300:
                ;**** Check FORTRAN still running ****

    if find_process(pid) eq 0 then goto, LABELEND

                ;************************************************
                ;**** Communicate with c4spf1 in fortran and ****
                ;**** invoke user interface widget for       ****
                ;**** Output options                         ****
                ;************************************************

    c4spf1, pipe, lpend, outval, procval, header, bitfile, gomenu, pid, $
            line1, dataset,                                             $
            DEVLIST=devlist, FONT_LARGE=font_large, FONT_SMALL=font_small


                ;**** If menu button clicked, tell FORTRAN to stop ****

    if gomenu eq 1 then begin
        printf, pipe, 1
        outval.texmes = ''
        goto, LABELEND
    endif else begin
        printf, pipe, 0
    endelse

                ;**** If cancel selected then erase output ****
                ;**** messages and goto 200.               ****

    if lpend eq 1 then begin
        outval.texmes = ''
        goto, LABEL200
    end

                ;**** Determine input data type i.e. beam  ****
                ;**** stopping (adf21) or beam emission    ****
                ;**** (adf22) and write to fortran         ****
                
    titleflag = c4type(inval.dsfull[0])        
    printf, pipe, titleflag

                ;************************************************
                ;**** If graphical output requested          ****
                ;**** Communicate with c4outg in fortran and ****
                ;**** invoke widget Graphical output         ****
                ;************************************************

    if outval.grpout eq 1 then begin

               ;**** User explicit scaling ****

        if procval.ifit eq 0 then begin
            grpscal = outval.ldef1
            xmin = outval.xmin1
            xmax = outval.xmax1
            ymin = outval.ymin1
            ymax = outval.ymax1
        endif else if procval.ifit eq 1 then begin
            grpscal = outval.ldef2
            xmin = outval.xmin2
            xmax = outval.xmax2
            ymin = outval.ymin2
            ymax = outval.ymax2
        endif else begin
            grpscal = outval.ldef3
            xmin = outval.xmin3
            xmax = outval.xmax3
            ymin = outval.ymin3
            ymax = outval.ymax3
        endelse

                ;**** Hardcopy output ****

        hrdout = outval.hrdout
        hardname = outval.hardname
        if outval.devsel ge 0 then device = devcode(outval.devsel)

                ;*** user title for graph ***

        utitle = outval.gtit1
  
        c4outg, pipe, utitle, grpscal, xmin, xmax, ymin, ymax,          $
                num_files, procval.nreq, procval.ifit, procval.lfsel,   $
                hrdout, hardname, device, header, bitfile, gomenu,      $
                titleflag,line1, dataset, procval.sifrac, procval.tsymm,$
                procval.itzm, procval.ubmeng, procval.utdens,           $
                procval.uttemp, FONT=font_large
    endif

                ;**** If menu button clicked, tell FORTRAN to stop ****
                                                                        
    if gomenu eq 1 then begin
        printf, pipe, 1
        outval.texmes = ''
        goto, LABELEND
    endif else begin
        printf, pipe, 0
    endelse

                ;**** Back for more output options ****

GOTO, LABEL300

LABELEND: 

                ;**** Save user defaults ****

    save, inval, procval, outval, filename=deffile


END
