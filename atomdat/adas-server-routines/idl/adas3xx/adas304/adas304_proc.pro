; Copyright (c) 1995, Strathclyde University .
; SCCS Info : Module   @(#)$Header: /home/adascvs/idl/adas3xx/adas304/adas304_proc.pro,v 1.2 2004/07/06 10:30:05 whitefor Exp $ Date $Date: 2004/07/06 10:30:05 $
;+
; PROJECT:
;       ADAS IBM MVS to UNIX conversion
;
; NAME:
;	ADAS304_PROC
;
; PURPOSE:
;	IDL ADAS user interface, processing options/input.
;
; EXPLANATION:
;	This routine creates and manages a pop-up window which allows
;	the user to select options and input data to control ADAS304
;	processing.
;
; USE:
;	This routine is ADAS304 specific, see c4ispf.pro for how it
;	is used.
;
; INPUTS:
;	PROCVAL	- A structure which determines the initial settings of
;		  the processing options widget.  The value is passed
;		  unmodified into cw_adas304_proc.pro.
;
;		  See cw_adas304_proc.pro for a full description of this
;		  structure.
;
;       BITFILE - String; the path to the directory containing bitmaps
;                 for the 'escape to series menu' button.
;
;       STOPSTRING - String; the list of stopping ions chosen.
;
;	MXREQ	   - Integer; the maximum number of user input data
;		     triplets allowed.
;
; OPTIONAL INPUTS:
;	None
;
; OUTPUTS:
;	PROCVAL	- On output the structure records the final settings of
;		  the processing selections widget if the user pressed the
;		  'Done' button, otherwise it is not changed from input.
;
;	ACT	- String; 'Done', 'Menu' or 'Cancel' for the button the
;		  user pressed to terminate the processing options
;		  window.
;
; OPTIONAL OUTPUTS:
;	None
;
; KEYWORD PARAMETERS:
;	FONT_LARGE - The name of a larger font.
;
;	FONT_SMALL - The name of a smaller font.
;
;	EDIT_FONTS - A structure of two fonts used in the table editor
;		     adas_edtab.pro, {font_norm:'',font_input:''}.
;
; CALLS:
;	CW_ADAS304_PROC	Declares the processing options widget.
;	DYNLABEL	Sets dynamic_resize keyword for label widgets.
;	ADAS304_PROC_EV Called indirectly during widget management,
;			routine included in this file.
;	XMANAGER
;
; SIDE EFFECTS:
;       This widget uses a common block PROC304_BLK, to pass the
;       variables VALUE and ACTION between the two routines. 
;
; CATEGORY:
;	Widgets
;
; WRITTEN:
;       Tim Hammond, Tessella Support Services plc, 21-Nov-1995
;
; MODIFIED:
;	1.1	Tim Hammond		
;		First version
;	1.2    	William Osborn
;		Added dynlabel procedure
;
; VERSION:
;	1.1	11-12-95
;	1.2	09-07-96
;
;-
;-----------------------------------------------------------------------------


PRO adas304_proc_ev, event


    COMMON proc304_blk, action, value

    action = event.action

    CASE event.action OF

		;**** 'Done' button ****

	'Done'  : begin

		;**** Get the output widget value ****

	    widget_control,event.id,get_value=value 
	    widget_control,event.top,/destroy

	end

		;**** 'Cancel' button ****

	'Cancel': widget_control,event.top,/destroy

                ;**** 'Menu' button ****

        'Menu': widget_control, event.top, /destroy

    END

END

;-----------------------------------------------------------------------------


PRO adas304_proc, procval, act, bitfile, stopstring, mxreq, 		$
		  FONT_LARGE=font_large, FONT_SMALL=font_small, 	$
		  EDIT_FONTS=edit_fonts


		;**** declare common variables ****

    COMMON proc304_blk, action, value

		;**** Copy "procval" to common ****

    value = procval

		;**** Set defaults for keywords ****

    IF NOT (KEYWORD_SET(font_large)) THEN font_large = ''
    IF NOT (KEYWORD_SET(font_small)) THEN font_small = ''
    IF NOT (KEYWORD_SET(edit_fonts)) THEN 				$
    edit_fonts = { font_norm:'', font_input:''}
		
                ;**** create base widget ****

    procid = widget_base(TITLE='ADAS304 PROCESSING OPTIONS', 		$
			 XOFFSET=50,YOFFSET=0)

		;**** Declare processing widget ****

    cwid = cw_adas304_proc(procid, bitfile, stopstring, mxreq, 		$
		           PROCVAL=value, 				$
		           FONT_LARGE=font_large, FONT_SMALL=font_small,$
		           EDIT_FONTS=edit_fonts)

		;**** Realize the new widget ****

    dynlabel, procid
    widget_control, procid, /realize

		;**** make widget modal ****

    xmanager, 'adas304_proc', procid, event_handler='adas304_proc_ev', 	$
    /modal, /just_reg

		;*** copy value back to procval for return to c4ispf ***
    act = action
    procval = value
 
END

