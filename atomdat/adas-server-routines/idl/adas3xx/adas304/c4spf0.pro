; Copyright (c) 1995, Strathclyde University           
; SCCS info: Module @(#)$Header: /home/adascvs/idl/adas3xx/adas304/c4spf0.pro,v 1.2 2004/07/06 11:51:57 whitefor Exp $ Date $Date: 2004/07/06 11:51:57 $
;+
; PROJECT:
;       ADAS IBM MVS to UNIX conversion
;
; NAME:
;	C4SPF0
;
; PURPOSE:
;	IDL user interface and communications with ADAS304 FORTRAN
;	process via pipe.
;
; EXPLANATION:
;	Firstly this routine invokes the part of the user interface
;	used to select the input dataset for ADAS304.  When the user's
;	interactions are complete this routine communicates with the
;	ADAS304 FORTRAN application via a UNIX pipe.  Communications
;	are to the FORTRAN subroutine C4SPF0.
;
; USE:
;	The use of this routine is specific to ADAS304, see adas304.pro.
;
; INPUTS:
;	PIPE	- The IDL unit number of the bi-directional pipe to the
;		  ADAS304 FORTRAN process.
;
;	VALUE	- A structure which determines the initial settings of
;		  the dataset selection widget.  The initial value is
;		  set in adas304.pro.  VALUE is passed un-modified
;		  through to cw_adas_in.pro, see that routine for a full
;		  description.
;
; OPTIONAL INPUTS:
;	None
;
; OUTPUTS:
;	VALUE	- On output the structure records the final settings of
;		  the dataset selection widget if the user pressed the
;		  'Done' button otherwise it is not changed from input.
;
;	REP	- String; Indicates whether the user pressed the 'Done'
;		  or 'Cancel' button on the interface.  The action is
;		  converted to the strings 'NO' and 'YES' respectively
;		  to match up with the existing FORTRAN code.  In the
;		  original IBM ISPF interface REP was the reply to the
;		  question 'DO YOU WANT TO END PROGRAM EXECUTION'.
;
;	NUM_FILES - Integer; the number of stopping ion files chosen
;		    by the user. Needed later by c4chkz.pro
;
; OPTIONAL OUTPUTS:
;	None
;
; KEYWORD PARAMETERS:
;       FONT_LARGE      - Supplies the large font to be used for the
;                         interface widgets.
;
;       FONT_SMALL      - Supplies the small font to be used for the
;                         interface widgets.
;
; CALLS:
;	ADAS304_IN	Pops-up the input selection widget.
;
; SIDE EFFECTS:
;	This routine communicates with the ADAS304 FORTRAN process
;	via a UNIX pipe.
;
; CATEGORY:
;	Adas system.
;
; WRITTEN:
;       Tim Hammond, Tessella Support Services plc, 16/11/1995
;
; MODIFIED:
;	1.1	Tim Hammond
;		First version.
;
; MODIFIED:
;	1.2	Richard Martin
;		Included 'dataflag' in call statement. This is updated in
;		adas304_in routine if input data file is changed.
;
; VERSION:
;	1.1	16-11-95
;
;-
;-----------------------------------------------------------------------------

PRO c4spf0, pipe, value, dataflag, rep, num_files, FONT_LARGE=font_large,		$
            FONT_SMALL=font_small


                ;**** Set defaults for keywords ****

    IF NOT (KEYWORD_SET(font_large)) THEN font_large = ''
    IF NOT (KEYWORD_SET(font_small)) THEN font_small = ''


		;**********************************
		;**** Pop-up input file widget ****
		;**********************************

   adas304_in, value, dataflag, action, WINTITLE = 'ADAS 304 INPUT', 		$
     		FONT_LARGE=font_large, FONT_SMALL=font_small

		;******************************************
		;**** Act on the event from the widget ****
		;******************************************
		;**** There are only two possible events ****
		;**** 'Done' and 'Cancel'.               ****
		;********************************************

   if action eq 'Done' then begin
       rep = 'NO'
   endif else begin
       rep = 'YES'
   endelse

		;*******************************
		;**** Write data to fortran ****
		;*******************************

    printf, pipe, rep
    if rep eq 'NO' then begin
        counter = where(strcompress(value.dsfull, /remove_all) ne '')
	num_files = n_elements(counter)
        printf, pipe, num_files
	for i=0, num_files-1 do begin
	    printf, pipe, value.dsfull(i)
	endfor
        num_files = fix(num_files)
    endif

END
