; Copyright (c) 1995 Strathclyde University 
; SCCS info: Module @(#)$Header: /home/adascvs/idl/adas3xx/adas304/cw_adas304_in.pro,v 1.14 2004/07/06 12:38:13 whitefor Exp $ Date $Date: 2004/07/06 12:38:13 $
;+
; PROJECT:
;       ADAS IBM MVS to UNIX conversion
;
; NAME:
;       CW_ADAS304_IN()
;
; PURPOSE:
;       Data file selection for adas 304.
;
; EXPLANATION:
;       This function creates a compound widget consisting of text typeins
;	for the input file group name (e.g. bms93#h on UNIX machines) and
;	an optional member prefix which can be used to distinguish between
;	different data sets with the same group name. In this case the 
;	member prefix must come before the filename and be followed by a #
;	symbol. For example, valid filenames in the sub-directory
;	../adf21/bms93#h would be: bms93#h_be4.dat and 
;			           jfk#bms93#h_be4.dat
;	There is also an information widget showing a list of selected
;	ions (NONE when none are selected), a data root selection compound
;	widget cw_adas_root, a 'Browse comments' button which cycles
;	through all selected files when pressed, a 'Reselect Ion List'
;	button which pops up a selection widget showing all files which
;	match the values entered in the rest of the widgets and a 'Cancel'
;	and a 'Done' button.
;
;	The value of this widget is contained in the VALUE structure.
;
; USE:
;       See routine adas304_in.pro for an example.
;
; INPUTS:
;       PARENT  - Long integer; the ID of the parent widget.
;
; OPTIONAL INPUTS:
;       None
;
; OUTPUTS:
;       This function returns the ID of the compound widget created.
;
; OPTIONAL OUTPUTS:
;       None
;
; KEYWORD PARAMETERS:
;       VALUE   - A structure which determines the initial settings of
;                 the entire compound widget. The structure must be:
;		 	{ ROOTPATH:'',					$
;			  CENTROOT:'',					$
;			  USERROOT:'',					$
;			  GROUP:' ',					$
;			  PREFIX:' ',					$
;			  IONS: strarr(10),				$
;			  STOPSTRING:' ',				$
;			  DSFULL:strarr(10)				}
;
;		  Where the elements of the structure are as follows:
;
;                 ROOTPATH   - Current data directory e.g '/usr/fred/adas/'
;
;                 CENTROOT   - Default central data store e.g '/usr/adas/'
;
;                 USERROOT   - Default user data store e.g '/usr/fred/adas/'
;
;		  GROUP	     - String: the input file group name (e.g. bms93#h)
;
;		  PREFIX     - String: the required member prefix for
;			       all files (case insensitive, 3 chars or less).
;
;		  IONS	     - String array: the selected ions (maximum of 10)
;
;		  STOPSTRING - String; the list of currently selected ions.
;
;		  DSFULL     - String array; the full names of the 
;			       currently selected data files.
;
;                 Path names may be supplied with or without the trailing
;                 '/'.  The underlying routines add this character where
;                 required so that USERROOT will always end in '/' on
;                 output.
;
;       FONT_LARGE  - Supplies the large font to be used for the
;                     interface widgets.
;
;       FONT_SMALL  - Supplies the small font to be used for the
;                     interface widgets.
; CALLS:
;	CW_ADAS_ROOT	Data root selection widget.
;
; SIDE EFFECTS:
;       IN304_GET_VAL() Widget management routine in this file.
;       IN304_EVENT()   Widget management routine in this file.
;
; CATEGORY:
;       Compound Widget
;
; WRITTEN:
;       Tim Hammond, Tessella Support Services plc, 16th November 1995
;
; MODIFIED:
;       1.1     Tim Hammond 
;		First release. Created from skeleton of cw_adas405_in.
;	1.2	Tim Hammond
;		Fully working version apart from the browse button.
;	1.3	Tim Hammond
;		Initial fully working version
;	1.4	Tim Hammond
;		Tidied up comments and code.
;	1.5	William Osborn
;		Added 'alias ls ls;' to UNIX command which gets directory
;		listing so as not to be fooled by strange aliases
;	1.6	William Osborn
;		Put in test for no files before comments browsing event code
;	1.7	William Osborn
;		Added 'alias ls ls;' in other place needed
;	1.8	William Osborn
;		Removed 'alias' bits and replaced with other checks for
;		'/'s in group name and directory listing
;	1.9	William Osborn
;		Added one element to array short_list to solve very strange
;		problem with SUNs not recognising the first element.
;	1.10	William Osborn
;		Added check for absence of any files in the browse button
;		event code which was giving crashes.
;	1.11	William Osborn
;		Added /noshell oprion to spawn commands and changed the UNIX
;		commands appropriately so as to give same behaviour on all
;		machines.
;	1.12	Harvey Anderson
;		Modified text which is displayed to the user, changed
;		' bms93#<beam>' to ' bms97#<beam>' .
;	1.13	Richard Martin
;		Changed to v. 1.13
;	1.14	Richard Martin
;		Increase size of allowed 'member, to accommodate the
;               99 He data.
;
; VERSION:
;       1.1     16-11-95
;	1.2	20-11-95
;	1.3	20-11-95
;	1.4	11-12-95
;	1.5	27-06-96
;	1.6	27-06-96
;	1.7	27-06-96
;	1.8	27-06-96
;	1.9	24-07-96
;	1.10	29-07-96
;	1.11	10-09-96
;	1.12	04-02-97
;	1.13	26-03-97
;	1.14	17-02-2004
;
;-----------------------------------------------------------------------------
;-

FUNCTION in304_get_val, id


                ;**** Return to caller on error ****

    ON_ERROR, 2

                 ;***************************************
                 ;****     Retrieve the state        ****
                 ;**** Get first_child widget id     ****
                 ;**** because state is stored there ****
                 ;***************************************

    first_child = widget_info(id, /child)
    widget_control, first_child, get_uvalue = state

		;**** Get the settings ****

    widget_control, state.rootid, get_value=fileval
    widget_control, state.groupid, get_value=group
    widget_control, state.memid, get_value=prefix

		;**** Get the list of ions ****

    widget_control, state.stopid, get_value=listval
    stopstring = listval(0)


    ps = {	ROOTPATH	:	fileval.rootpath,		$
		CENTROOT	:	fileval.centroot,		$
		USERROOT	:	fileval.userroot,		$
		GROUP		:	group(0),			$
		PREFIX		:	prefix(0),			$
		IONS		:	state.inval.ions,		$
		STOPSTRING	:	stopstring,			$
		DSFULL		:	state.inval.dsfull		}

    widget_control, first_child, set_uvalue=state, /no_copy

    RETURN, ps

END

;-----------------------------------------------------------------------------

FUNCTION in304_event, event

    COMMON adas304_inblock, userdir, centdir

                ;**** Base ID of compound widget ****

    parent = event.handler

                ;**** Default output no event ****

    new_event = 0L

                ;**********************************************
                ;**** Retrieve the user value state        ****
                ;**** Get id of first_child widget because ****
                ;**** user value "state" is stored there   ****
                ;**********************************************

    first_child = widget_info(parent, /child)
    widget_control, first_child, get_uvalue=state, /no_copy
    topparent = widget_info(parent, /parent)

		;**** Clear any messages away ****

    widget_control, state.messid, set_value='   '

                ;************************
                ;**** Process events ****
                ;************************

    CASE event.id OF

		;**** Text typeins ****

	state.groupid: widget_control, state.memid, /input_focus
	state.memid: widget_control, state.groupid, /input_focus

		;**** Select stopping ions ****

	state.selectid: begin
	
		;**** First check whether there is a valid groupname ****

            validgroup = 0
	    widget_control, state.groupid, get_value=groupval
	    groupval(0) = strcompress(groupval(0),/remove_all)
	    if groupval(0) ne '' then begin

		if strmid(groupval(0),strlen(groupval(0))-1,1) eq '/' then $
		    groupval(0) = strmid(groupval(0),0,strlen(groupval(0))-1)

		;**** Now check validity of member prefix ****

		widget_control, state.memid, get_value=memval
		memcheck = strcompress(memval(0), /remove_all)
		if (memcheck ne '' and strlen(memcheck) gt 6) then begin
		    widget_control, state.messid, 			$
		    set_value='*** The specified member prefix is invalid ***'
		endif else begin

		;**** Now check existence of a directory with this name ****

		    widget_control, state.rootid, get_value=rootvalue
		    checkcom = ['ls', rootvalue.rootpath + '/.']
		    spawn, checkcom, short_list, /noshell
		;**** Add one element to the array short_list so as ****
		;**** to prevent strange behaviour on SUNs          ****
		    temp = strarr(n_elements(short_list)+1)
		    temp(1:n_elements(short_list))=short_list
		    short_list = temp
		    diravailable = where(short_list eq 			$
                    strcompress(groupval(0), /remove_all))
		    if diravailable(0) eq -1 then diravailable = 	$
		where(short_list eq strcompress(groupval(0), /remove_all)+'/')
		    navail = n_elements(diravailable)
		    if navail eq 1 then begin
			if diravailable(0) ne -1 then begin
			    validgroup = 1
			endif else begin
			    widget_control, state.messid, set_value='***'+$
	                    ' No directory matches the group name ***'
			endelse
		    endif else begin
			widget_control, state.messid, set_value='***'+	$
                        'More than one directory matches the group name ***'
		    endelse
		endelse
	    endif else begin
		widget_control, state.messid,				$
		set_value='*** You must first enter a group name ***'
	    endelse
	    if validgroup eq 1 then begin
                group = strcompress(groupval(0), /remove_all)
	
		;**** Group is valid, check for files ****

		listcom2 = ['ls',rootvalue.rootpath + '/' + group]
		spawn, listcom2, files, /noshell
		if n_elements(files) gt 0 then begin
		    countarray = intarr(n_elements(files))
		    if memcheck ne '' then begin
			for i=0, n_elements(files)-1 do begin
			    position = strpos(files(i), memcheck)
			    if position ne -1 then begin
				countarray(i) = 4
			    endif
			endfor
		        filevals = where(countarray eq 4)
		        if filevals(0) ne -1 then begin
		            files = files(filevals)
		        endif else begin
		            files = 0
		        endelse
		    endif else begin
                        for i=0, n_elements(files)-1 do begin
                            position = strpos(files(i), '#')
                            if position le 3 then begin
                                countarray(i) = 4
                            endif
                        endfor
                        filevals = where(countarray ne 4)
                        if filevals(0) ne -1 then begin
                            files = files(filevals)
                        endif else begin
                            files = 0
                        endelse
                    endelse
		endif
		if n_elements(files) gt 0 then begin
		    n_files = 0
		    filearray = intarr(50)
		    for i=0, (n_elements(files)-1) do begin
			if memcheck eq '' or strpos(files(i), memcheck) ne -1 $
			then begin
			    grpos = strpos(files(i), group)
                            if grpos(0) ne -1 then begin
			        underpos = strpos(files(i), '_')
			        datpos = strpos(files(i), '.dat')
			        if underpos(0) ne -1 and datpos(0) ne -1   $
                                then begin
                                    filearray(n_files) = i
				    n_files = n_files + 1
			        endif
			    endif
			endif
		    endfor
		    if n_files gt 0 then begin
		;**** Store the names of the ions in the ion array ****
		 	ion = strarr(n_files)
			for i=0, n_files-1 do begin
			    start = strpos(files(i), '_') + 1
			    length = strpos(files(i), '.dat') - 	$
                            strpos(files(i), '_') - 1
			    ion(i) = strmid(files(i), start, length)
			endfor
                        neations = ion
                        for i=0, n_files-1 do begin
			    capital = strupcase(strmid(ion(i),0,1))
			    strtemp = ion(i)
			    strput, strtemp, capital
			    neations(i) = strtemp
			endfor
		;**** Decide which ions are currently selected and ****
		;**** set them on the popup widget                 ****
			selections=replicate(-1, 10)
			n_ions = where(state.inval.ions ne '')
			if n_ions(0) ne -1 then begin
			    dummy = 0
			    a = strcompress(state.inval.ions, /remove_all)
			    for j=0, n_files-1 do begin
			        b = strcompress(ion(j), /remove_all)
				chosen = where(a eq b)
				if chosen(0) ne -1 then begin
				    selections(dummy) = j
				    dummy = dummy + 1
				endif
			    endfor
			endif
		;**** Pop up the selection widget ****
			selected = multi_sel(neations, maxsel=10, 	$
                        /ordered, title='Select Stopping Ions',		$
			selected=selections, font=state.font_large)
		;**** Update settings following user's actions ****
			newstopions = where(selected ne -1)
			if (newstopions(0) ne -1) then begin
			    state.inval.ions=''
                  	    stopstring = ''
			    for i=0, (n_elements(newstopions)-1) do begin
				state.inval.ions(i) = 			$
   				ion(selected(newstopions(i)))
				labelbit = 				$
				neations(selected(newstopions(i)))
				stopstring = stopstring + ' ' +		$
                                strcompress(string(labelbit),/remove_all)
				state.inval.dsfull(i) =			$
				rootvalue.rootpath +			$
 				strcompress(groupval(0), /remove_all)+'/'
                                state.inval.dsfull(i) =                 $
                                state.inval.dsfull(i) +                 $
				files(selected(newstopions(i)))
			    endfor
                            if n_elements(newstopions) lt 10 then	$
                            state.inval.dsfull(n_elements(newstopions):*)=''
			endif else begin
			    state.inval.ions=''
			    stopstring = 'NONE'
			endelse
			widget_control, state.stopid, set_value=stopstring
		    endif else begin
                        widget_control, state.messid, set_value='*** There'+$
                        ' are no data files which match the given details ***'
		    endelse
		endif else begin
		    widget_control, state.messid, set_value='*** There'+$
                    ' are no data files which match the given details ***'
		endelse
	    endif
		
	end

                ;***********************
                ;**** Browse button ****
                ;***********************

        state.browseid: begin

                ;**** Invoke comments browsing ****

	    files = where(strcompress(state.inval.dsfull, /remove_all) ne '')
	    if (files(0)ne -1) then begin
	    	for i=0, n_elements(files)-1 do begin
		    xxtext, state.inval.dsfull(files(i)), font=state.font_large
	    	endfor
	    endif else begin
		widget_control, state.messid, set_value='*** No valid files'+$
		 ' have been selected ***'
	    endelse
        end

                ;***********************
                ;**** Cancel button ****
                ;***********************

        state.cancelid: begin
            new_event = {ID:parent, TOP:event.top,          		$
                         HANDLER:0L, ACTION:'Cancel'}
        end

	state.doneid: begin
            new_event = 0L

                ;**** First check whether there is a valid groupname ****

            validgroup = 0
            widget_control, state.groupid, get_value=groupval
	    groupval(0) = strcompress(groupval(0),/remove_all)
            if groupval(0) ne '' then begin

		if strmid(groupval(0),strlen(groupval(0))-1,1) eq '/' then $
		    groupval(0) = strmid(groupval(0),0,strlen(groupval(0))-1)

                ;**** Now check validity of member prefix ****

                widget_control, state.memid, get_value=memval
                memcheck = strcompress(memval(0), /remove_all)
                if (memcheck ne '' and strlen(memcheck) gt 3) then begin
                    widget_control, state.messid,                       $
                    set_value='*** The specified member prefix is invalid ***'
                endif else begin

                ;**** Now check existence of a directory with this name ****

                    widget_control, state.rootid, get_value=rootvalue
                    checkcom = ['ls', rootvalue.rootpath + '/.']
                    spawn, checkcom, short_list, /noshell
		;**** Add one element to the array short_list so as ****
		;**** to prevent strange behaviour on SUNs          ****
		    temp = strarr(n_elements(short_list)+1)
		    temp(1:n_elements(short_list))=short_list
		    short_list = temp
                    diravailable = where(short_list eq                  $
                    strcompress(groupval(0), /remove_all))
		    if diravailable(0) eq -1 then diravailable = 	$
		where(short_list eq strcompress(groupval(0), /remove_all)+'/')
                    navail = n_elements(diravailable)
                    if navail eq 1 then begin
                        if diravailable(0) ne -1 then begin
                            validgroup = 1
                        endif else begin
                            widget_control, state.messid, set_value='***'+$
                            ' No directory matches the group name ***'
                        endelse
                    endif else begin
                        widget_control, state.messid, set_value='***'+  $
                        'More than one directory matches the group name ***'
                    endelse
                endelse
            endif else begin
                widget_control, state.messid,                           $
                set_value='*** You must first enter a group name ***'
            endelse
            if validgroup eq 1 then begin
                group = strcompress(groupval(0), /remove_all)

		;*****************************************
		;**** Now see whether the user has    ****
		;**** Selected any stopping ion files ****
		;*****************************************

		filetest = where(state.inval.ions ne '')
		if filetest(0) eq -1 then begin
		    widget_control, state.messid,                         $
                    set_value='*** You must first select at least one '+  $
			      'stopping ion ***'
		endif else begin
		;**** Now check that stopping ions chosen ****
		;**** do correspond to real files         ****
		    listcom3 = ['ls', rootvalue.rootpath + '/' + group]
		    spawn, listcom3, files, /noshell
                    if n_elements(files) gt 0 then begin
                        countarray = intarr(n_elements(files))
                        if memcheck ne '' then begin
                            for i=0, n_elements(files)-1 do begin
                                position = strpos(files(i), memcheck)
                                if position ne -1 then begin
                                    countarray(i) = 4
                                endif
                            endfor
                            filevals = where(countarray eq 4)
                            if filevals(0) ne -1 then begin
                                files = files(filevals)
                            endif else begin
                                files = 0
                            endelse
                        endif else begin
			    for i=0, n_elements(files)-1 do begin
                                position = strpos(files(i), '#')
				if position le 3 then begin
				    countarray(i) = 4
                                endif
                            endfor
                            filevals = where(countarray ne 4)
                            if filevals(0) ne -1 then begin
                                files = files(filevals)
                            endif else begin
                                files = 0
                            endelse
			endelse
                    endif
		    if n_elements(files) gt 0 then begin
			n_files = 0
			filearray = intarr(50)
			for i=0, (n_elements(files)-1) do begin
			    if memcheck eq '' or 			$
			    strpos(files(i), memcheck) ne -1 then begin
				grpos = strpos(files(i), group)
				if (grpos(0) ne -1) then begin
				    underpos = strpos(files(i), '_')
				    datpos = strpos(files(i), '.dat')
				    if underpos(0) ne -1 and datpos(0) ne -1 $
				    then begin
					filearray(n_files) = i
					n_files = n_files + 1
				    endif
				endif
			    endif
			endfor
		;**** Store the names of the ions in the ion array ****
			if n_files gt 0 then begin
		    	    ion = strarr(n_files)
			    for i=0,n_files-1 do begin
			        start = strpos(files(i), '_') + 1
			        length = strpos(files(i), '.dat') -	$
			        strpos(files(i), '_') - 1
			        ion(i) = strmid(files(i), start, length)
			    endfor
			;**** Were all files found? ****
			chosen=where(strcompress(state.inval.ions,/remove_all)$
                         ne '')
			number = n_elements(chosen)
			numberchk = 0
			for i=0, number-1 do begin
			    for j=0, n_elements(files)-1 do begin
				flag = strpos(files(j), state.inval.ions(i))
				if flag ne -1 then begin
				    numberchk=numberchk+1
				    state.inval.dsfull(i) = 		$
				    rootvalue.rootpath +		$
				    strcompress(groupval(0),/remove_all)+'/'
				    state.inval.dsfull(i) =               $
				    state.inval.dsfull(i) +               $
          			    files(j)
				endif
			    endfor
			endfor
			if number lt 10 then state.inval.dsfull(number:*)=''
			if number eq numberchk then begin
                            new_event = {ID:parent, TOP:event.top, 	$
                                         HANDLER:0L, ACTION:'Done'}
			endif else begin
			    widget_control, state.messid, set_value='***'+$
			    ' At least one of the requested ions was not'+$
			    ' found ****'
			endelse
			endif else begin
			    widget_control, state.messid, set_value='***'+$
                            ' There are no data files which match the'+	  $
                            ' given details ***'
			endelse
		    endif else begin
			widget_control, state.messid, set_value='*** There'+$
                        ' are no data files which match the given details ***'
		    endelse
		endelse
	    endif 
	end

        ELSE: new_event = 0L

    ENDCASE


                ;*********************************************
                ;*** make "state" available to other files ***
                ;*********************************************

    widget_control, first_child, set_uvalue=state, /no_copy

    RETURN, new_event

END

;-----------------------------------------------------------------------------

FUNCTION cw_adas304_in, parent, VALUE=value,				$
			FONT_LARGE=font_large, FONT_SMALL=font_small

    COMMON adas304_inblock, userdir, centdir

    ON_ERROR, 2                                 ;return to caller on error

                ;**** Set defaults for keywords ****

    IF NOT (KEYWORD_SET(value)) THEN begin
        inset = {	ROOTPATH	:	'./',			$
			CENTROOT	:	'',			$
			USERROOT	:	'',			$
			GROUP		:	' ',			$
			PREFIX		:	' ',			$
			IONS		:	strarr(10),		$
			STOPSTRING	:	' ',			$
			DSFULL		:	strarr(10)		}
    ENDIF ELSE BEGIN
	inset = {	ROOTPATH        :       value.rootpath,		$
			CENTROOT        :       value.centroot,		$
			USERROOT        :       value.userroot,		$
			GROUP		:	value.group,		$
			PREFIX		:	value.prefix,		$
			IONS		:	value.ions,		$
			STOPSTRING	:	value.stopstring,	$
			DSFULL		:	value.dsfull		}
        if strtrim(inset.rootpath) eq '' then begin
            inset.rootpath = './'
        endif else if                                                   $
        strmid(inset.rootpath, strlen(inset.rootpath)-1,1) ne '/' then begin
            inset.rootpath = inset.rootpath+'/'
        endif
    ENDELSE
    userdir = inset.userroot
    centdir = inset.centroot
    IF NOT (KEYWORD_SET(font_large)) THEN font_large = ''
    IF NOT (KEYWORD_SET(font_small)) THEN font_small = ''

                ;*********************************
                ;**** Create the Input widget ****
                ;*********************************

                ;**** create base widget ****

    topbase = widget_base(parent, EVENT_FUNC = "in304_event",		$
                          FUNC_GET_VALUE = "in304_get_val",		$
		          /column)

                ;******************************************************
                ;**** Create a dummy widget just to hold value of *****
                ;**** "state" variable so as not to get confused  *****
                ;**** with any other values. Adopt IDL practice   *****
                ;**** of using first child widget                 *****
                ;******************************************************

    first_child = widget_base(topbase)

    cwid = widget_base(first_child, /column)

                ;***********************************
                ;**** Pathname selection widget ****
                ;***********************************

    scriptbase = widget_base(cwid, /column, /frame)

    scrval = {	ROOTPATH	:	inset.rootpath,			$
		CENTROOT	:	inset.centroot,			$
		USERROOT	:	inset.userroot			}
    scrtitle = widget_label(scriptbase, font=font_large,		$
	                    value='Input Stopping Ion File Details:-')
    rootid = cw_adas_root(scriptbase, value=scrval, font=font_large)

		;**** Details typeins and information widget ****

    groupbase = widget_base(scriptbase, /row)
    grouplabel = widget_label(groupbase, font=font_large,		$
                 value='Group name for input files    :')
    groupid = widget_text(groupbase, font=font_large, xsize=9,		$
                          value=inset.group, /editable)
    grouphint = widget_label(groupbase, font=font_large,		$
                value='( Usually - bms97#<beam> )')
    membase = widget_base(scriptbase, /row)
    memlabel = widget_label(membase, font=font_large,               	$
               value='Class prefix for input member :')
    memid = widget_text(membase, font=font_large, xsize=9,          	$
                          value=inset.prefix, /editable)
    memhint = widget_label(membase, font=font_large,                	$
              value='( Up to 3 characters - Blank=none )')
    stopbase = widget_base(scriptbase, /row)
    stoplabel = widget_label(stopbase, font=font_large,                	$
                value='Stopping Ion List :')
    stopbase2 = widget_base(scriptbase, /row)
    stoplabel = widget_label(stopbase2, font=font_large,               	$
                value='(maximum of 10)')
    buffbase = widget_base(scriptbase, /row)
    bufflabel = widget_label(buffbase, font=font_large,                	$
                value=' ')

		;**** Construct the list of chosen stopping ions ****

    ionlist = where(inset.ions ne '')
    if ionlist(0) eq -1 then begin
	stopstring = 'NONE'
    endif else begin
        stopstring = ' '
	for i=0, n_elements(ionlist)-1 do begin
            label = inset.ions(i)
	    capital = strupcase(strmid(label,0,1))
	    strput, label, capital
	    stopstring = stopstring + label + ' '
	endfor
    endelse
    stopid = widget_label(stopbase, font=font_large, value=stopstring)

		;**** Error message ****

    messid = widget_label(cwid, font=font_large,			$
    value='Edit the processing options data and press Done to proceed')

                ;*****************
                ;**** Buttons ****
                ;*****************

    base = widget_base(cwid, /row)

                ;**** Browse Dataset button ****

    browseid = widget_button(base, value='Browse Comments',		$
                             font=font_large)

		;**** Reselect ion list button ****

    ionvals = where(inset.ions ne '')
    if ionvals(0) gt -1 then begin
	sellabel = 'Reselect Ion List'
    endif else begin
	sellabel = 'Select Ion List'
    endelse
    selectid = widget_button(base, value=sellabel, font=font_large)

                ;**** Cancel Button ****

    cancelid = widget_button(base, value='Cancel', font=font_large)

                ;**** Done Button ****

    doneid = widget_button(base, value='Done', font=font_large)

                ;*************************************************
                ;**** create a state structure for the pop-up ****
                ;****                window.                  ****
                ;*************************************************

    new_state = {	font_large	:	font_large,		$
			font_small	:	font_small,		$
			inval		:	inset,			$
			doneid		:	doneid,			$
			browseid	:	browseid,		$
			selectid	:	selectid,		$
			rootid		:	rootid,			$
			memid		:	memid,			$
			groupid		:	groupid,		$
			stopid		:	stopid,			$
			messid		:	messid,			$
			cancelid	:	cancelid		}

               ;**** Save initial state structure ****

    widget_control, first_child, set_uvalue=new_state, /no_copy
    
    RETURN, topbase

END
