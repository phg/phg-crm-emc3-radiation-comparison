; Copyright (c) 1995, Strathclyde University.
; SCCS info: Module @(#)$Header: /home/adascvs/idl/adas3xx/adas304/adas304_out.pro,v 1.2 2004/07/06 10:29:49 whitefor Exp $ Date $Date: 2004/07/06 10:29:49 $
;+
; PROJECT:
;       ADAS IBM MVS to UNIX conversion
;
; NAME:
;	ADAS304_OUT
;
; PURPOSE:
;	IDL ADAS user interface, output options.
;
; EXPLANATION:
;	This routine creates and manages a pop-up window which allows
;	the user to select options and input data to control ADAS304
;	graphical and file output.
;
; USE:
;	This routine is specific to adas304.                  
;
; INPUTS:
;	OUTVAL	- A structure which determines the initial settings of
;		  the output options widget.  The value is passed
;		  unmodified into cw_adas304_out.pro.
;
;		  See cw_adas304_out.pro for a full description of this
;		  structure.
;
;       PROCVAL - A structure containing the final settings of the
;		  processing options widget.
;
;       BITFILE - String; the path to the dirctory containing bitmaps
;                 for the 'escape to series menu' button.
;
; OPTIONAL INPUTS:
;	None.
;
; OUTPUTS:
;	ACT	- String;'Done', 'Cancel' or 'Menu' for the button the
;		  user pressed to terminate the output options window.
;
; OPTIONAL OUTPUTS:
;	None.
;
; KEYWORD PARAMETERS:
;	DEVLIST	- A string array;  Each element of the array is the
;		  name of a hardcopy graphics output device.  This
;		  list must mirror the DEVCODE list.  DEVLIST is the
;		  list which the user will see.
;		  e.g ['Post-Script','HP-PCL','HP-GL']
;
;       FONT_LARGE- Supplies the large font to be used.
;
;       FONT_SMALL- Supplies the small font to be used.
;
; CALLS:
;	CW_ADAS304_OUT	Creates the output options widget.
;	DYNLABEL	Sets dynamic_resize keyword for label widgets.
;	XMANAGER
;	See side effects for widget management routine.
;
; SIDE EFFECTS:
;	This routine uses a common block OUT304_BLK to maintain its state.
;	ADAS304_OUT_EV	is included in this file and is called
;			indirectly from XMANAGER during widget
;			management.
;
; CATEGORY:
;	Widgets
;
; WRITTEN:
;       Tim Hammond, Tessella Support Services plc, 7-Dec-1995 
;
; MODIFIED:
;	1.1	Tim Hammond	
;		First release
;	1.2    	William Osborn
;		Added dynlabel procedure
;
; VERSION:
;	1.1	07-12-95
;	1.2	09-07-96
; 
;-
;-----------------------------------------------------------------------------


pro adas304_out_ev, event

    common out304_blk, action, value
	

		;**** Find the event type and copy to common ****

    action = event.action

    case action of

		;**** 'Done' button ****

	'Done'  : begin

			;**** Get the output widget value ****

     	     child = widget_info(event.id, /child)
	     widget_control, child, get_value=value 

                        ;*****************************************
			;**** Kill the widget to allow IDL to ****
			;**** continue and interface with     ****
			;**** FORTRAN only if there is work   ****
			;**** for the FORTRAN to do.          ****
			;*****************************************

	     if (value.grpout eq 1) or (value.texout eq 1) then begin
	        widget_control, event.top, /destroy
	     endif 
	end


		;**** 'Cancel' button ****

	'Cancel': widget_control, event.top, /destroy

		;**** 'Menu' button ****

	'Menu': widget_control, event.top, /destroy

    endcase

END

;-----------------------------------------------------------------------------


pro adas304_out, outval, procval, act, bitfile, devlist=devlist,	$
                 font_large=font_large, font_small=font_small

    common out304_blk, action, value

		;**** Copy value to common ****

    value = outval

		;**** Set defaults for keywords ****

    if not (keyword_set(font_large)) then font_large = ''
    if not (keyword_set(font_small)) then font_small = ''
    if not (keyword_set(devlist)) then devlist = ''

		;***************************************
		;**** Pop-up a new widget           ****
		;***************************************

                ;**** create base widget ****

    outid = widget_base(title='ADAS304 OUTPUT OPTIONS', xoffset=100,	$
                        yoffset=1)

		;**** Declare output options widget ****

    cwid = cw_adas304_out(outid, procval, bitfile, 			 $
                          value=value, devlist=devlist, 		 $
                          font_large=font_large, font_small=font_small)

		;**** Realize the new widget ****

    dynlabel, outid
    widget_control, outid, /realize

		;**** make widget modal ****

    xmanager, 'adas304_out', outid, event_handler='adas304_out_ev',	$
              /modal, /just_reg
 
		;**** Return the output value from common ****

    act = action
    outval = value

END

