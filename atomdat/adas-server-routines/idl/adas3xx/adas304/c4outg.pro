; Copyright (c) 1995, Strathclyde University.
; SCCS info: Module @(#)$Header: /home/adascvs/idl/adas3xx/adas304/c4outg.pro,v 1.2 2004/07/06 11:51:44 whitefor Exp $ Date $Date: 2004/07/06 11:51:44 $
;+
; PROJECT:
;       ADAS IBM MVS to UNIX conversion
;
; NAME:
;	C4OUTG
;
; PURPOSE:
;	Communication with ADAS304 FORTRAN via UNIX pipe and
;	graphics output.
;
; EXPLANATION:
;	The routine begins by reading information from the ADAS304
;	FORTRAN process via a UNIX pipe.  Then the IDL graphical
;	output routine for ADAS304 is invoked.  Communications are to
;	the FORTRAN subroutine C4OUTG.
;
; USE:
;	The use of this routine is specific to ADAS304 see adas304.pro.
;
; INPUTS:
;	PIPE	 - The IDL unit number of the bi-directional pipe to the
;		   ADAS304 FORTRAN process.
;
;	UTITLE   - Optional comment by user
;
;	GRPSCAL	 - Integer; 0 if default scaling required 1 if user
;		   specified scaling to be used.
;
;	XMIN	 - String; User sepcified x-axis minimum, number as string.
;
;	XMAX	 - String; User sepcified x-axis maximum, number as string.
;
;	YMIN	 - String; User specified y-axis minimum, number as string.
;
;	YMAX	 - String; User specified y-axis maximum, number as string.
;
;	NSITYP   - Integer; no. of user-selected ions
;
;	NREQ	 - Integer; no. of user entered output values
;
;       IFIT    -       Integer; shows which graph is being plotted
;                       0 = beam energy
;                       1 = target density
;                       2 = target temperature
;
;	LFSEL	 - Integer; 1 if minimax required, 0 if not
;
;	HRDOUT	 - Integer; 0 if no hardcopy required, 1 if hardcopy.
;
;	HARDNAME - String; file name for hardcopy output.
;
;	DEVICE	 - String; IDL name for hardcopy output device.
;
;	HEADER	 - ADAS version header information for inclusion in the
;		   graphical output.
;
;	TITLEFLAG- =0 if adf21 data being used; =1 if adf22 used.
;
;       BITFILE - String; the path to the directory containing bitmaps
;                 for the 'escape to series menu' button.
;
;	LINE1	- String; the names of the ions being plotted
;
;	DATASET	- String; the name of the data subset being used
;
;       SIFRAC  - Float array; the stopping ion fractions (used in labels)
;
;	TSYMM	- String array; the stopping ion symbols
;
;	ITZM	- Integer array; the stopping ion charges
;
;	UBMENG	- Float array; user entered output beam energies
;
;	UTDENS	- Float array; user entered output target densities
;
;	UTTEMP	- Float array; user entered output target temperatures
;
; OPTIONAL INPUTS:
;	None.
;
; OUTPUTS:
;       GOMENU  - Int; flag - set to 1 if user has selected 'escape direct
;                 to series menu', otherwise 0
;
; OPTIONAL OUTPUTS:
;	None.
;
; KEYWORD PARAMETERS:
;	FONT	- String; Name of the font to be used for the graphical
;		  output widget.
;
; CALLS:
;	ADAS304_PLOT	ADAS304 graphical output.
;
; SIDE EFFECTS:
;	This routine reads information from FORTRAN via a UNIX pipe.
;
; CATEGORY:
;	Adas system.
;
; WRITTEN:
;       Tim Hammond, Tessella Support Services plc, 7th December 1995
;
; MODIFIED:
;	1.1		Tim Hammond			
;			First version
;
; MODIFIED:		Richard Martin
;			Flag TITLEFLAG passed on to ADAS_304_PLOT
;
; VERSION:
;	1.1		07-12-95
;	1.2		12-03-97
;-
;-----------------------------------------------------------------------------

PRO c4outg, pipe, utitle, grpscal, xmin, xmax, ymin, ymax, nsityp,	$
		  nreq, ifit, lfsel, hrdout, hardname, device, header, 	$
                  bitfile, gomenu, titleflag, line1, dataset, sifrac, 	$
                  tsymm,itzm, ubmeng, utdens, uttemp, FONT=font


                ;**** Set defaults for keywords ****

    IF NOT (KEYWORD_SET(font)) THEN font = ''


		;**** Declare variables for input ****

    input = 0
    fdum = 0.0
    sdum = ' '

		;********************************
		;**** Read data from fortran ****
		;********************************

		;**** Read spline data ****

    x = fltarr(nreq, nsityp+1)
    y = fltarr(nreq, nsityp+1)
    for i=0, nreq-1 do begin
	for j=0, nsityp-1 do begin
	    readf, pipe, fdum
	    x(i,j) = fdum
	    readf, pipe, fdum
	    y(i,j) = fdum
	endfor
    endfor
    x(*,nsityp) = x(*,nsityp-1)
    for i=0, nreq-1 do begin
	readf, pipe, fdum
	y(i,nsityp) = fdum
    endfor

		;**** Read minimax data ****

    if lfsel eq 1 then begin
        readf, pipe, input
        nfit = input				;no. of mimax points
        xfit = fltarr(nfit, nsityp+1)			;minimax data
        yfit = fltarr(nfit, nsityp+1)			;minimax data
        finfo = strarr(nsityp+1)			;minimax info.
        for i=0, nsityp do begin
	    for j=0, nfit-1 do begin
	        readf, pipe, fdum
	        xfit(j,i) = fdum
	        readf, pipe, fdum
	        yfit(j,i) = fdum
	    endfor
	    readf, pipe, sdum
	    finfo(i) = sdum
        endfor
    endif else begin
        nfit = 0
        xfit = 0
	yfit = 0
    endelse

  
		;***********************
		;**** Plot the data ****
		;***********************

    adas304_plot, utitle, grpscal, x, y, nreq, xfit, yfit, nfit, ifit,	$
                  nsityp, hrdout, hardname, device, header, titleflag, 	$
                  xmin, xmax, ymin, ymax, sifrac, tsymm, itzm, 		$
                  bitfile, lfsel, gomenu, line1, dataset, ubmeng,	$
                  utdens, uttemp, finfo, FONT=font


END
