;+
; PROJECT:
;       ADAS
;
; NAME:
;       PLOT304
;
; PURPOSE:
;       Plot graphs for ADAS304.
;
; EXPLANATION:
;       This routine plots ADAS304 output for one plot.
;
; USE:
;       Use is specific to ADAS304.  See adas304_plot.pro for
;       example.
;
; INPUTS:
;
;       LDEF1   -       Integer; 1 if user specified axis limits to     
;                       be used, 0 if default scaling to be used.
;
;       X       -       2d Fltarr; x-values to plot
;
;       Y       -       2d Fltarr; y-values to plot 
;       
;       NREQ    -       Integer; number of x-y points to plot for
;                       each of the NSITYP ions
;
;       RIGHTSTRING  -  String; left hand column of title to side of graph
;                       ('INDEX')
;
;       RIGHTSTRING2 -  String; second column of title to side of graph
;                       ('ION')
;
;       RIGHTSTRING3 -  String; third column of title to side of graph
;                       ('CHARGE')
;
;       RIGHTSTRING4 -  String; fourth column of title to side of graph
;                       ('FRACTION')
;
;       RIGHTSTRING5 -  String; first column of second title to side of graph
;                       ('INDEX')
;
;       RIGHTSTRING6 -  String; second column of second title to side of graph
;                       ('BEAM ENERGY')
;
;       RIGHTSTRING7 -  String; third column of second title to side of graph
;                       ('TARGET DENSITY')
;
;       RIGHTSTRING8 -  String; fourth column of second title to side of graph
;                       (' TARGET TEMP')
;
;       RIGHTSTRING9 -  String; first column of third title to side of graph
;                       (' ION')
;
;       RIGHTSTRING10-  String; second column of third title to side of graph
;                       (' TYPE')
;
;       RIGHTSTRING11-  String; third column of third title to side of graph
;                       (' DEGREE')
;
;       RIGHTSTRING12-  String; fourth column of third title to side of graph
;                       (' ACCURACY')
;
;       RIGHTSTRING13-  String; fifth column of third title to side of graph
;                       (' LOWER END GRADIENT')
;
;       RIGHTSTRING14-  String; sixth column of third title to side of graph
;                       (' UPPER END GRADIENT')
;
;       XFIT    -       2d Float array; minimax x-values
;
;       YFIT    -       2d Float array; minimax y-values
;
;       NFIT    -       Integer; number of minimax points
;
;       IFIT    -       Integer; shows which graph is being plotted
;                       0 = beam energy
;                       1 = target density
;                       2 = target temperature
;
;       NSITYP  -       Integer; number of ions to be plotted
;
;       TITLE   -       String; heading to go above graph
;
;       XMIN1   -       Float; user-defined x-axis minimum
;
;       XMAX1   -       Float; user-defined x-axis maximum
;
;       YMIN1   -       Float; user-defined y-axis minimum
;
;       YMAX1   -       Float; user-defined y-axis maximum
;
;       LFSEL   -       Integer; 1 = Minimax plots requested, 0=not
;
;       IONS    -       String array; the ion symbols to be used in labels
;
;       BEAMEVENT-      String; either 'BEAM STOPPING ...' or 'BEAM EMISSION..'
;
; OPTIONAL INPUTS:
;       None.
;
; OUTPUTS:
;       None.
;
; OPTIONAL OUTPUTS:
;       None.
;
; KEYWORD PARAMETERS:
;       FONT    - String; Name of font passed to graphical output
;                 widget.
;
; CALLS:
;       None.
;
; SIDE EFFECTS:
;       None.
;
; CATEGORY:
;       Adas system.
;
; WRITTEN:
;       Tim Hammond, Tessella Support Services plc,  7th December 1995
;
; MODIFIED:
;       1.1     Tim Hammond      
;               First version
;       1.2     Tim Hammond
;               Removed unnecessary labels from when minimax fits not plotted.
;       1.3     Tim Hammond
;               Added new COMMON block Global_lw_data which contains the
;               values of left, right, top, bottom, grtop, grright
;       1.4     Tim Hammond
;               Added check for new environment variable VERY_SMALL
;               If this is set to the string "YES" (preferably in the
;               .adas_begin file) then the font sized used on the graphs
;               is reduced and the positioning of labels adjusted
;               slightly to make things fit on a very small screen.
;       1.5     Richard Martin
;               Added BEAMEVENT
;       1.6     Martin O'Mullane
;               Change x-axis label to eV/amu from the incorrect eV.
;       1.7     Martin O'Mullane
;               y-axis units depend on the new titleflag parameter.
;
; VERSION:
;       1.1     07-12-95
;       1.2     26-01-96
;       1.3     27-02-96
;       1.4     06-08-96
;       1.5     12-03-97
;       1.6     27-05-2004
;       1.7     10-12-2004
;
;-
;----------------------------------------------------------------------------

PRO plot304, ldef1, rightstring, rightstring2, x, y, nreq, xfit,        $
             yfit, nfit, ifit, nsityp, title, xmin1, xmax1, ymin1,      $
             ymax1, lfsel, rightstring3, rightstring4, rightstring5,    $
             rightstring6, rightstring7, rightstring8, rightstring9,    $
             rightstring10, rightstring11, rightstring12,               $
             rightstring13, rightstring14, ions, beamevent, titleflag
        

  COMMON Global_lw_data, left, right, tp, bot, grtop, grright

                ;****************************************************
                ;**** Suitable character size for current device ****
                ;**** Aim for 60 characters in y direction.      ****
                ;****************************************************

    charsize = (!d.y_vsize/!d.y_ch_size)/60.0
    small_check = GETENV('VERY_SMALL')
    if small_check eq 'YES' then charsize=charsize*0.8

                ;**** Initialise titles ****
    
    case titleflag of
      0 : yunits = ' (cm!E3!N s!E-1!N)'
      1 : yunits = ' (ph cm!E3!N s!E-1!N)'
      2 : yunits = ' '
      3 : yunits = ' ?'
    endcase      

    ytitle = beamevent + yunits
    if ifit eq 0 then begin
        xtitle = 'BEAM ENERGY (eV/amu)'
    endif else if ifit eq 1 then begin
        xtitle = 'TARGET DENSITY (cm!E-3!N)'
    endif else begin
        xtitle = 'TARGET TEMPERATURE (eV)'
    endelse
    if small_check eq 'YES' then begin
        righthead = '---------- USER INPUT DATA ----------!C!C' +               $
                    '-------- STOPPING ION FRACTIONS -------!C!C' +             $
                    '      INDEX    ION    CHARGE     FRACTION !C'
    endif else begin
        righthead = '---------- USER INPUT DATA ----------!C' +         $
                    '-------- STOPPING ION FRACTIONS -------!C!C' +             $
                    '      INDEX    ION    CHARGE     FRACTION !C'
    endelse
    for i=0, nsityp-1 do begin
        righthead = righthead + '!C'
        if small_check eq 'YES' then righthead=righthead+'!C'
    endfor
    righthead = righthead + '!C!C!C' +                                  $
                '---- ENERGY/DENSITY/TEMP TRIPLETS ----!C!C' +          $
                '      INDEX   BEAM     TARGET      TARGET!C'+          $
                '              ENERGY   DENSITY      TEMP.!C' +         $
                '             (eV/amu)   (cm!E-3!N)        (eV)!C'
    for i=0, nreq-1 do begin
        righthead = righthead + '!C'
        if small_check eq 'YES' then righthead=righthead+'!C'
    endfor
    if lfsel eq 1 then begin
        if small_check eq 'YES' then begin
            righthead = righthead + '!C!C!C' +                          $
                        '---------- FITTING INFORMATION ----------!C!C'+$
                        '     ION   TYPE   DEG   ACCRCY  END GRADIENT!C!C'+$
                        '                           (%)   LOWER    UPPER'
        endif else begin
            righthead = righthead + '!C!C!C' +                          $
                        '---------- FITTING INFORMATION ----------!C!C'+$
                        '     ION   TYPE   DEG   ACCRCY  END GRADIENT!C'+$
                        '                           (%)   LOWER    UPPER'
        endelse
    endif
    erase

                ;**** Find x and y ranges for auto scaling,        ****
                ;**** check x and y in range for explicit scaling. ****

     makeplot = 1
     style = 0
     ystyle = 0
 
     if ldef1 eq 0 then begin

                ;**** identify values in the valid range ****
                ;**** plot routines only work within ****
                ;**** single precision limits.       ****

        xvals = where (x gt 1.6e-36 and x lt 1.0e37)
        yvals = where (y gt 1.6e-36 and y lt 1.0e37)
        if xvals(0) gt -1 then begin
            maxx = max(x(xvals))
            minx = min(x(xvals))
        endif else begin
            makeplot = 0
        endelse
        if yvals(0) gt -1 then begin
            maxy = max(y(yvals))
            miny = min(y(yvals))
        endif else begin
            makeplot = 0
        endelse
        if makeplot eq 1 then begin
            if miny le 1.0e-36 then begin
                ystyle = 1
                miny = 1.0e-36
            endif else begin
                ystyle = 0
            endelse
        endif
        style = 0
     endif else begin
        minx = xmin1
        maxx = xmax1
        miny = ymin1
        maxy = ymax1
        xvals = where(x gt minx and x lt maxx)
        yvals = where(y gt miny and y lt maxy)
        if xvals(0) eq -1 or yvals(0) eq -1 then begin
            makeplot = 0
        endif else begin
            makeplot = 1
        endelse
        style = 1
        ystyle = 1
     endelse

     if makeplot eq 1 then begin

                ;**** Set up log-log plotting axes ****
 
        plot_oo, [minx, maxx], [miny, maxy], /nodata, ticklen=1.0,      $
                  position=[left, bot, grright, grtop],                 $
                  xtitle=xtitle, ytitle=ytitle, xstyle=style,           $
                  ystyle=ystyle, charsize=charsize

                ;*********************************
                ;**** Make and annotate plots ****
                ;*********************************

                ;**** Spline plots ****
        for i=0, nsityp do begin
            plots, x(*,i), y(*,i)
            oplot, x(*,i), y(*,i), psym=1
        endfor

                ;**** Minimax plots ****

        if lfsel eq 1 then begin
            for i=0, nsityp do begin
                oplot, xfit(*,i), yfit(*,i), linestyle=2
            endfor
        endif

                ;**** Find suitable point for annotation ****

        for i=0, nsityp do begin
            if (miny eq 10^(!y.crange(0))) then begin
                if (x(nreq-1) ge minx) and (x(nreq-1) le maxx) and      $
                (y(nreq-1,i) gt miny) and                               $
                (y(nreq-1,i) lt (10^(!y.crange(1)))) then begin
                    iplot = nreq - 1
                endif else begin
                    iplot = -1
                    for id = 0, nreq-2 do begin
                        if (x(id) ge minx) and (x(id) le maxx) and      $
                        (y(id,i) gt miny) and (y(id,i) le maxy) then begin
                            iplot = id
                        endif
                    endfor
                endelse
            endif else begin
                if (x(nreq-1) ge minx) and (x(nreq-1) le maxx) and  $
                (y(nreq-1,i) ge miny) and                           $
                (y(nreq-1,i) lt (10^(!y.crange(1)))) then begin
                    iplot = nreq - 1
                endif else begin
                    iplot = -1
                    for id = 0, nreq-2 do begin
                        if (x(id) ge minx) and (x(id) le maxx) and  $
                        (y(id,i) ge miny) and                       $
                        (y(id,i) le maxy) then begin
                            iplot = id
                        endif
                    endfor
                endelse
            endelse
    
                ;**** Annotate plots with level numbers ****

            if iplot ne -1 then begin
                if i lt nsityp then begin
                    xyouts, x(iplot), y(iplot,i), ' ' + ions(i),        $
                    charsize=charsize*0.8
                endif else begin
                    xyouts, x(iplot), y(iplot,i), ' ALL',               $
                    charsize=charsize*0.8
                endelse
            endif
        endfor
    endif else begin                    ;no plot possible
        xyouts, 0.2, 0.5, /normal, charsize=charsize*1.5,               $
                '---- No data lies within range ----'
    endelse

                ;**** Output title above graphs ****
    
    if small_check eq 'YES' then begin
        xyouts, 0.1, 0.96, title, charsize=charsize, /normal
    endif else begin
        xyouts, 0.1, 0.9, title, charsize=charsize, /normal
    endelse

                ;**** Output titles to right of graphs ****

    if small_check eq 'YES' then charsize=charsize*0.95
    xyouts, 0.72, 0.8, righthead, charsize=charsize*0.8, /normal
    xyouts, 0.76, 0.74, rightstring, charsize=charsize*0.8, /normal
    xyouts, 0.81, 0.74, rightstring2, charsize=charsize*0.8, /normal
    xyouts, 0.855, 0.74, rightstring3, charsize=charsize*0.8, /normal
    xyouts, 0.92, 0.74, rightstring4, charsize=charsize*0.8, /normal
    xyouts, 0.76, 0.74, rightstring5, charsize=charsize*0.8, /normal
    xyouts, 0.78, 0.74, rightstring6, charsize=charsize*0.8, /normal
    xyouts, 0.85, 0.74, rightstring7, charsize=charsize*0.8, /normal
    xyouts, 0.92, 0.74, rightstring8, charsize=charsize*0.8, /normal
    xyouts, 0.747, 0.74, rightstring9, charsize=charsize*0.8, /normal
    xyouts, 0.78, 0.74, rightstring10, charsize=charsize*0.8, /normal
    xyouts, 0.83, 0.74, rightstring11, charsize=charsize*0.8, /normal
    xyouts, 0.86, 0.74, rightstring12, charsize=charsize*0.8, /normal
    xyouts, 0.90, 0.74, rightstring13, charsize=charsize*0.8, /normal
    xyouts, 0.95, 0.74, rightstring14, charsize=charsize*0.8, /normal

END
