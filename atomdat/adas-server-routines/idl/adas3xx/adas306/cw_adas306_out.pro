; Copyright (c) 1995, Strathclyde University.
; SCCS Info : Module @(#)$Header: /home/adascvs/idl/adas3xx/adas306/cw_adas306_out.pro,v 1.2 2004/07/06 12:38:45 whitefor Exp $ Date $Date: 2004/07/06 12:38:45 $
;+
; PROJECT:
;       ADAS IBM MVS to UNIX conversion
;
; NAME:
;	CW_ADAS306_OUT()
;
; PURPOSE:
;	Produces a widget for ADAS306 output options.
;
; EXPLANATION:
;	This function declares a compound widget consisting of the
;	graphical output selection widget cw_306_gr_sel.pro, and an 
;	output file widget cw_adas_outfile.pro.  The text output
;	file specified in this widget is for tabular (paper.txt)
;	output.  This widget also includes a button for browsing the comments
;       from the input dataset, a 'Cancel' button and a 'Done' button.
;	The compound widgets cw_306_gr_sel.pro and cw_adas_outfile.pro
;	included in this file are self managing.  This widget only
;	handles events from the 'Done' and 'Cancel' buttons.
;
;	This widget only generates events for the 'Done' and 'Cancel'
;	buttons.
;
; USE:
;	Specific to adas306.                  
;
; CALLED BY:
;	adas306_out.pro
;
; INPUTS:
;	PARENT	- Long integer; ID of parent widget.
;
;	DSFULL	- Name of input dataset for this application.
;
;       BITFILE - String; the path to the dirctory containing bitmaps
;                 for the 'escape to series menu' button.
;
;	NGRF	- Int; number of graphs
;
; OPTIONAL INPUTS:
;	None.
;
; OUTPUTS:
;	The return value of this function is the ID of the declared widget.
;
; OPTIONAL OUTPUTS:
;	None.
;
; KEYWORD PARAMETERS:
;	DEVLIST	- A string array;  Each element of the array is the
;		  name of a hardcopy graphics output device.  This
;		  list must mirror the DEVCODE list.  DEVLIST is the
;		  list which the user will see.
;		  e.g ['Post-Script','HP-PCL','HP-GL']
;
;	VALUE	- A structure which determines the initial settings of
;		  the output options widget.  The structure is made
;		  up of two parts.  Each part is the same as the value
;		  structure of one of the two main compound widgets
;		  included in this widget.  See cw_306_gr_sel and
;		  cw_adas_outfile for more details.  The default value is;
;
;		      { GRPOUT:0, GTIT1:'', $
;			GRPSCAL:0, $
;			XMINA:'',  XMAXA:'',   $
;			YMINA:'',  YMAXA:'',   $
;			XMINB:'',  XMAXB:'',   $
;			YMINB:'',  YMAXB:'',   $
;			HRDOUT:0, HARDNAME:'', $
;			GRPDEF:'', GRPFMESS:'', $
;			GRPSEL:-1, GRPRMESS:'', $
;			DEVSEL:-1, GRSELMESS:'', $
;			TEXOUT:0, TEXAPP:-1, $
;			TEXREP:0, TEXDSN:'', $
;			TEXDEF:'',TEXMES:'', $
;		      }
;
;		  For CW_306_GR_SEL;
;			GRPOUT	   Integer; Activation button 1 on, 0 off
;			GTIT1	   String; Graph title
;			GRPSCAL	   Integer; Scaling activation 1 on, 0 off
;			XMINA	   String; x-axis minimum, string of number
;			XMAXA	   String; x-axis maximum, string of number
;			YMINA	   String; y-axis minimum, string of number
;			YMAXA	   String; y-axis maximum, string of number
;			XMINB      String; x-axis minimum (plot B)
;                       XMAXB      String; x-axis minimum (plot B)
;                       YMINB      String; y-axis minimum (plot B)
;                       YMAXB      String; y-axis minimum (plot B)
;			HRDOUT	   Integer; Hard copy activ' 1 on, 0 off
;			HARDNAME   String; Hard copy output file name
;			GRPDEF	   String; Default output file name 
;			GRPFMESS   String; File name error message
;			GRPSEL	   Integer; index of selected graph in GRPLIST
;			GRPRMESS   String; Scaling ranges error message
;			DEVSEL	   Integer; index of selected device in DEVLIST
;			GRSELMESS  String; General error message
;
;		  For CW_ADAS_OUTFILE tabular output (paper.txt);
;			TEXOUT	Integer; Activation button 1 on, 0 off
;			TEXAPP	Integer; Append button 1 on, 0 off, -1 no button
;			TEXREP	Integer; Replace button 1 on, 0 off, -1 no button
;			TEXDSN	String; Output file name
;			TEXDEF	String; Default file name
;			TEXMES	String; file name error message
;
;
;	UVALUE	- A user value for this widget.
;
;	FONT	- String; the name of a font to be used for all text in
;		  this widget.  Default to system default font.
;
; CALLS:
;	CW_ADAS_DSBR	Input dataset name and comments browse button.
;	CW_308_GR_SEL	Graphical output selection widget.
;	CW_ADAS_OUTFILE	Output file name entry widget.
;	See side effects for other related widget management routines.
;
; SIDE EFFECTS:
;
; 	Two other routines are included in this file
;	OUT306_GET_VAL()
;	OUT306_EVENT()
;
; CATEGORY:
;	Compound Widget
;
; WRITTEN:
;       William Osborn, Tessella Support Services plc, 23-May-1996
;
; MODIFIED:
;	1.1	William Osborn
;		First Release
;	1.2	William Osborn
;		Removed xsize=n keyword from widget_label command.
;		The dynlabel procedure does not override these
;		assignments and they were taken as being values in pixels
;		on some machines.
;
; VERSION:
;	1.1	23-05-96
;	1.2	01-08-96
;
;-
;-----------------------------------------------------------------------------

FUNCTION out306_get_val, id

                ;**** Return to caller on error ****

    ON_ERROR, 2

                ;**** Retrieve the state ****

    parent=widget_info(id,/parent)
    widget_control, parent, get_uvalue=state, /no_copy

		;**** Get graphical output settings ****

    widget_control,state.grpid,get_value=grvalsel

		;**** Get text output settings ****

    widget_control,state.paperid,get_value=papos

    os = { out306_set, 						 $
	   GRPOUT:grvalsel.outbut, GTIT1:grvalsel.gtit1, 	 $
	   GRPSCAL:grvalsel.scalbut, 			 	 $
	   XMINA:grvalsel.xmina,     XMAXA:grvalsel.xmaxa,    	 $
	   YMINA:grvalsel.ymina,     YMAXA:grvalsel.ymaxa,    	 $
	   XMINB:grvalsel.xminb,     XMAXB:grvalsel.xmaxb,	 $
           YMINB:grvalsel.yminb,     YMAXB:grvalsel.ymaxb,       $
	   HRDOUT:grvalsel.hrdout, HARDNAME:grvalsel.hardname,   $
	   GRPDEF:grvalsel.grpdef, GRPFMESS:grvalsel.grpfmess,   $
	   GRPSEL:grvalsel.grpsel, GRPRMESS:grvalsel.grprmess,   $
	   DEVSEL:grvalsel.devsel, GRSELMESS:grvalsel.grselmess, $
	   TEXOUT:papos.outbut,  TEXAPP:papos.appbut, 		 $
	   TEXREP:papos.repbut,  TEXDSN:papos.filename, 	 $
	   TEXDEF:papos.defname, TEXMES:papos.message 		 $
        }

                ;**** Return the state ****

    widget_control, parent, set_uvalue=state, /no_copy

    RETURN, os 

END

;-----------------------------------------------------------------------------

FUNCTION out306_event, event

    COMMON out306_block, ngrf

                ;**** Base ID of compound widget ****

    parent=event.top

                ;**** Retrieve the state ****

    widget_control, parent, get_uvalue=state, /no_copy

		;*********************************
		;**** Clear previous messages ****
		;*********************************

    widget_control,state.messid,set_value=' '

                ;************************
                ;**** Process events ****
                ;************************

    CASE event.id OF

		;***********************
		;**** Cancel button ****
		;***********************

        state.cancelid: new_event = {ID:parent, TOP:event.top, 		$
				    HANDLER:0L, ACTION:'Cancel'}

		;*********************
		;**** Done button ****
		;*********************

        state.doneid: begin

		;****************************************************
                ;**** Return the state before checking can start ****
		;**** with the get_value keyword.                ****
		;****************************************************

            widget_control, parent, set_uvalue=state, /no_copy

		;***************************************
		;**** Check for errors in the input ****
		;***************************************

	    error = 0

		;**** Get a copy of the widget value ****

	    widget_control,event.handler,get_value=os
	
		;**** Check for widget error messages ****

            extraerror = 0
	    if os.grpout eq 1 and strtrim(os.grpfmess) ne '' then error=1
	    if os.grpout eq 1 and os.grpscal eq 1 and 			$
            strtrim(os.grprmess) ne '' then error=1
	    if os.grpout eq 1 and strtrim(os.grselmess) ne '' then error=1
	    if os.texout eq 1 and strtrim(os.texmes) ne '' then error=1
            if os.grpout eq 1 and ngrf eq 0 then begin
                error = 1
                extraerror = 1
            endif

                ;**** Retrieve the state   ****

            widget_control, parent, get_uvalue=state, /no_copy

	    if error eq 1 then begin
                if extraerror eq 0 then begin
	            widget_control,state.messid,set_value= 		$
		    '**** Error in output settings ****'
                endif else begin
                    widget_control,state.messid,set_value= 		$
                    '**** Error in output settings: No graphs chosen '	$
                    +'on processing screen ****'
                endelse
	        new_event = 0L
	     end else begin
	        new_event = {ID:parent, TOP:event.top, HANDLER:0L, ACTION:'Done'}
	     end

        end

		;*********************
                ;**** Menu button ****
		;*********************

        state.outid: begin
            new_event = {ID:parent, TOP:event.top, HANDLER:0L,          $
                         ACTION:'Menu'}
        end

    ELSE: new_event = 0L

  ENDCASE

                ;**** Return the state   ****

          widget_control, parent, set_uvalue=state, /no_copy

  RETURN, new_event
END

;-----------------------------------------------------------------------------

FUNCTION cw_adas306_out, parent, dsfull, ngrf, bitfile, DEVLIST=devlist, $
		VALUE=value, UVALUE=uvalue, FONT=font

    COMMON out306_block, ngraphs

    ngraphs = ngrf

    IF (N_PARAMS() EQ 0) THEN MESSAGE, 'Must specify parent for cw_adas306_out'
    ON_ERROR, 2					;return to caller

		;**** Set defaults for keywords ****

    IF NOT (KEYWORD_SET(devlist)) THEN devlist = ''
    IF NOT (KEYWORD_SET(uvalue)) THEN uvalue = 0
    IF NOT (KEYWORD_SET(font)) THEN font = ''
    IF NOT (KEYWORD_SET(value)) THEN begin
	os = {out306_set, 						$
			GRPOUT:0, GTIT1:'', 				$
			GRPSCAL:0, 					$
			XMINA:'',  XMAXA:'',   				$
			YMINA:'',  YMAXA:'',   				$
			XMINB:'',  XMAXB:'',   				$
			YMINB:'',  YMAXB:'',   				$
			HRDOUT:0, HARDNAME:'', 				$
			GRPDEF:'', GRPFMESS:'',				$
			GRPSEL:-1, GRPRMESS:'', 			$
			DEVSEL:-1, GRSELMESS:'', 			$
			TEXOUT:0, TEXAPP:-1, 				$
			TEXREP:0, TEXDSN:'', 				$
                        TEXDEF:'paper.txt',TEXMES:'' 			$
              }
    END ELSE BEGIN
	os = {out306_set, $
			GRPOUT:value.grpout, GTIT1:value.gtit1, 	$
			GRPSCAL:value.grpscal, 				$
			XMINA:value.xmina,     XMAXA:value.xmaxa,   	$
			YMINA:value.ymina,     YMAXA:value.ymaxa,   	$
			XMINB:value.xminb,     XMAXB:value.xmaxb,   	$
                        YMINB:value.yminb,     YMAXB:value.ymaxb,   	$
			HRDOUT:value.hrdout, HARDNAME:value.hardname, 	$
			GRPDEF:value.grpdef, GRPFMESS:value.grpfmess, 	$
			GRPSEL:value.grpsel, GRPRMESS:value.grprmess, 	$
			DEVSEL:value.devsel, GRSELMESS:value.grselmess, $
			TEXOUT:value.texout, TEXAPP:value.texapp, 	$
			TEXREP:value.texrep, TEXDSN:value.texdsn, 	$
			TEXDEF:value.texdef, TEXMES:value.texmes 	$
	      }
    END

		;**********************************************
		;**** Create the 306 Output options widget ****
		;**********************************************

		;**** create base widget ****

    cwid = widget_base( parent, UVALUE = uvalue, 			$
			EVENT_FUNC = "out306_event", 			$
			FUNC_GET_VALUE = "out306_get_val", 		$
			/COLUMN)

		;**** Add dataset name and browse button ****

    rc = cw_adas_dsbr(cwid,dsfull,font=font)

		;*****************************************************
		;**** Widget for graphics selection               ****
		;**** Note change in names for GRPOUT and GRPSCAL ****
		;*****************************************************

    grvalsel = {  OUTBUT:os.grpout, GTIT1:os.gtit1, SCALBUT:os.grpscal, $
		  XMINA:os.xmina, XMAXA:os.xmaxa, 			$
		  YMINA:os.ymina, YMAXA:os.ymaxa, 			$
		  XMINB:os.xminb, XMAXB:os.xmaxb, 			$
                  YMINB:os.yminb, YMAXB:os.ymaxb, 			$
		  HRDOUT:os.hrdout, HARDNAME:os.hardname, 		$
		  GRPDEF:os.grpdef, GRPFMESS:os.grpfmess, 		$
		  GRPSEL:os.grpsel, GRPRMESS:os.grprmess, 		$
		  DEVSEL:os.devsel, GRSELMESS:os.grselmess }
    base = widget_base(cwid,/row,/frame)

		;**** Use the 308_gr_sel widget for the graph selections ****

    grpid = cw_308_gr_sel(base, /SIGN, OUTPUT='Graphical Output', 	$
			   DEVLIST=devlist, VALUE=grvalsel, FONT=font 	$
			  )

		;**** Widget for text output ****

    outfval = { OUTBUT:os.texout, APPBUT:-1, REPBUT:os.texrep, 		$
	        FILENAME:os.texdsn, DEFNAME:os.texdef, MESSAGE:os.texmes }
    base = widget_base(cwid,/row,/frame)
    paperid = cw_adas_outfile(base, OUTPUT='Text Output', 		$
                          VALUE=outfval, FONT=font)

		;**** Error message ****

    messid = widget_label(cwid,value=' ',font=font)

		;**** add the exit buttons ****

    base = widget_base(cwid,/row)
    menufile = bitfile + '/menu.bmp'
    read_X11_bitmap, menufile, bitmap1
    outid = widget_button(base, value=bitmap1)          ;menu button
    cancelid = widget_button(base,value='Cancel',font=font)
    doneid = widget_button(base,value='Done',font=font)
  
		;*************************************************
		;**** create a state structure for the pop-up ****
		;**** window.                                 ****
		;*************************************************

    new_state = { GRPID:grpid, PAPERID:paperid, OUTID:outid,		$
		  CANCELID:cancelid, DONEID:doneid, MESSID:messid }

                ;**** Save initial state structure ****

    widget_control, parent, set_uvalue=new_state, /no_copy
    RETURN, cwid

END

