; Copyright (c) 1995, Strathclyde University .
; SCCS Info : Module @(#)$Header: /home/adascvs/idl/adas3xx/adas306/adas306_proc.pro,v 1.1 2004/07/06 10:30:45 whitefor Exp $ Date $Date: 2004/07/06 10:30:45 $
;+
; PROJECT:
;       ADAS IBM MVS to UNIX conversion
;
; NAME:
;	ADAS306_PROC
;
; PURPOSE:
;	IDL ADAS user interface, processing options/input.
;
; EXPLANATION:
;	This routine creates and manages a pop-up window which allows
;	the user to select options and input data to control ADAS306
;	processing.
;
; USE:
;	This routine is ADAS306 specific, see c6ispf.pro for how it
;	is used.
;
; INPUTS:
;	PROCVAL	- A structure which determines the initial settings of
;		  the processing options widget.  The value is passed
;		  unmodified into cw_adas306_proc.pro.
;		  See cw_adas306_proc.pro for a full description of this
;		  structure.
;
;	DSFULL	- String; The full system file name of the input 
;		          dataset selected by the user for processing.
;
;       mxtab   - (Int) Maximum number of emissivity tables allowed.
;
;       mxgrf   - (Int) Maximum number of emissivity graphs allowed.
;
;       symbd   - (String) Element symbol of donor (for information).
;
;       idz0    - (Int) Donor nuclear charge (for information).
;
;       symbr   - (String) Element symbol of receiver (for information).
;
;       irz0    - (Int) Receiver nuclear charge (for information).
;
;       irz1    - (Int) Receiver ion initial charge (for information).
;
;       irz2    - (Int) Receiver ion final charge (for information).
;
;       ngrnd   - (Int) Minimum allowed N quantum number.
;
;       ntot    - (Int) Maximum allowed N quantum number.
;
;       BITFILE - String; the path to the dirctory containing bitmaps
;                 for the 'escape to series menu' button.
;
; OPTIONAL INPUTS:
;	None
;
; OUTPUTS:
;	PROCVAL	- On output the structure records the final settings of
;		  the processing selections widget if the user pressed the
;		  'Done' button, otherwise it is not changed from input.
;
;	ACT	- String; Either 'Done', 'Cancel' or 'Menu' for the button
;		  the user pressed to terminate the processing options
;		  window.
;
; OPTIONAL OUTPUTS:
;	None
;
; KEYWORD PARAMETERS:
;	FONT_LARGE - The name of a larger font.
;
;	FONT_SMALL - The name of a smaller font.
;
;	EDIT_FONTS - A structure of two fonts used in the table editor
;		     adas_edtab.pro, {font_norm:'',font_input:''}.
;
; CALLS:
;	CW_ADAS306_PROC	Declares the processing options widget.
;	ADAS306_PROC_EV Called indirectly during widget management,
;			routine included in this file.
;	XMANAGER
;
; SIDE EFFECTS:
;       This widget uses a common block PROC306_BLK, to pass the
;       variables VALUE and ACTION between the two routines. 
;
; CATEGORY:
;	Widgets
;
; WRITTEN:
;       William Osborn, Tessella Support Services plc, 6-Jun-1995
;
; MODIFIED:
;	1.1	William Osborn			 22-5-96
; VERSION:
;	1.1	First written
;-
;-----------------------------------------------------------------------------


PRO adas306_proc_ev, event


    COMMON proc306_blk,action,value

    action = event.action

    CASE event.action OF

		;**** 'Done' button ****

	'Done'  : begin

		;**** Get the output widget value ****

	    widget_control,event.id,get_value=value 
	    widget_control,event.top,/destroy

	end

		;**** 'Cancel' button ****

	'Cancel': widget_control,event.top,/destroy


		;**** 'Menu' button ****

	'Menu': widget_control,event.top,/destroy

    END

END

;-----------------------------------------------------------------------------


PRO adas306_proc, procval, dsfull, act,    				$
                  symbd, idz0, symbr, irz0, irz1, irz2,                 $
		  ntot, ngrnd, mxtab, mxgrf, bitfile,			$
		  FONT_LARGE=font_large, FONT_SMALL=font_small, 	$
		  EDIT_FONTS=edit_fonts


		;**** declare common variables ****

    COMMON proc306_blk,action,value

		;**** Copy "procval" to common ****

    value = procval

		;**** Set defaults for keywords ****

    IF NOT (KEYWORD_SET(font_large)) THEN font_large = ''
    IF NOT (KEYWORD_SET(font_small)) THEN font_small = ''
    IF NOT (KEYWORD_SET(edit_fonts)) THEN 				$
	edit_fonts = { font_norm:'', font_input:''}
		
                ;**** create base widget ****

    procid = widget_base(TITLE='ADAS306 PROCESSING OPTIONS', 		$
			 XOFFSET=01,YOFFSET=01)

		;**** Declare processing widget ****

    cwid = cw_adas306_proc(procid, dsfull, act,                  	$
                           symbd, idz0, symbr, irz0, irz1, irz2,        $
        		   ntot, ngrnd,	mxtab, mxgrf, bitfile,		$
		           PROCVAL=value, 				$
		           FONT_LARGE=font_large, FONT_SMALL=font_small,$
		           EDIT_FONTS=edit_fonts)

		;**** Realize the new widget ****

    dynlabel, cwid

    widget_control,procid,/realize

		;**** make widget modal ****

    xmanager,'adas306_proc',procid,event_handler='adas306_proc_ev', 	$
	/modal,/just_reg

		;*** copy value back to procval for return to c6ispf ***
    act = action
    procval = value
 
END

