; Copyright (c) 1995 Strathclyde University.
; SCCS info: Module @(#)$Header: /home/adascvs/idl/adas3xx/adas306/adas306_plot.pro,v 1.3 2004/07/06 10:30:42 whitefor Exp $ Date $Date: 2004/07/06 10:30:42 $
;+
; PROJECT:
;       ADAS IBM MVS to UNIX conversion
;
; NAME:
;	ADAS306_PLOT
;
; PURPOSE:
;	Generates ADAS306 graphical output.
;
; EXPLANATION:
;	This routine creates a window for the display of graphical
;	output and displays output as the user directs by interaction
;	with the widget. If there are more than one set of graphs
;       then the user can switch between them at will.
;
; USE:
;	This routine is specific to ADAS306, see c6outg.pro for
;	example use.
;
; INPUTS:
;	(Most of these inputs map exactly onto variables of the same
;	 name in the FORTRAN code.)
;
;	DSNINC  - String; Input COPASE data set name.
;
;	NGRF	- Integer; Number of requested graphs.
;
;	GRPSCAL - Integer; flag whether default graph scaling.
;
;       GTIT1   - String; Title for graph.
;
;	XMINA	- Double; Minimum value for X axis (plot A).
;
;	XMAXA	- Double; Maximum value for X axis (plot A).
;
;	YMINA	- Double; Minimum value for Y axis (plot A).
;
;	YMAXA	- Double; Maximum value for Y axis (plot A).
;
;       XMINB   - Double; Minimum value for X axis (plot B).
;
;       XMAXB   - Double; Maximum value for X axis (plot B).
;
;       YMINB   - Double; Minimum value for Y axis (plot B).
;
;       YMAXB   - Double; Maximum value for Y axis (plot B).
;
;	XA	- Fltarr; X values for plots A
;
;	YA	- Fltarr; Y values for plots A
;
;       XB      - Fltarr; X values for plots B
;
;       YB      - Fltarr; Y values for plots B
;
;	NPU	- Intarr; Upper principal quantum numbers for graphs
;
;	NPL	- Intarr; Lower principal quantum numbers for graphs
;
;	GRAPHDEX  Intarr; indices of the graphs to be plotted.
;
;	QEFF	- Effective rate coefficients (used in labels)
;
;	RITIT	- String; First part of right hand label of graphs
;
;	RITIT2	- String; Second part of right hand label of graphs
;
;	HRDOUT  - Integer; 1 if hardcopy output activated, 0 if not.
;
;	HRDNAM  - String; Filename for harcopy output.
;
;	DEVICE	- String; IDL name of hardcopy output device.
;
;	HEADER	- String; ADAS version number header to include in graph.
;
; OPTIONAL INPUTS:
;	None.
;
; OUTPUTS:
;	None.
;
; OPTIONAL OUTPUTS:
;	None.
;
; KEYWORD PARAMETERS:
;	FONT	- String; The name of a font to use for text in the
;		  graphical output widget.
;
; CALLS:
;	CW_ADAS_GRAPH	Graphical output widget.
;	PLOT308		Make one plot to an output device for 306/308.
;	XMANAGER
;
; SIDE EFFECTS:
;	This routine uses a common block to maintain its state PLOT306_BLK.
;
;	One other routine is included in this file;
;	ADAS306_PLOT_EV	Called via XMANAGER during widget management.
;
; CATEGORY:
;	Adas system.
;
; WRITTEN:
;       William Osborn, Tessella Support Services plc,  23-May-1996
;
; MODIFIED:
;	1.1	William Osborn
;		First written
;	1.2	William Osborn
;		Reset !p.multi on exiting so as not to interfere with other
;		routines
;	1.3	Harvey Anderson ( University of Strathclyde )
;		Replaced the call to plot308 by the call to the
;		routine plot306. Both these routines are identical with the
;		exception of the different graphical annotation. 
; VERSION:
;	1.1	23-05-96
;	1.2	06-06-96
;	1.3	14-17-98
;
;----------------------------------------------------------------------------

PRO adas306_plot_ev, event

    COMMON plot306_blk, data, action, nplot, iplot, win, plotdev, 	$
		      plotfile, fileopen, gomenu


    newplot = 0
    print = 0
    done = 0
    gomenu = 0
		;****************************************
		;**** Set graph and device requested ****
		;****************************************
    CASE event.action OF

        'previous' : begin
                        if iplot gt 0 then begin
                            newplot = 1    
                            iplot = iplot - 1
			    first = iplot
			    last = iplot
			endif
	        end

        'next'     : begin
                        if iplot lt nplot-1 then begin
                            newplot = 1
			    iplot = iplot + 1
			    first = iplot
			    last = iplot
			endif
		end

	'print'	   : begin
			newplot = 1
			print = 1
			first = iplot
			last = iplot
		end

	'printall' : begin
			newplot = 1
			print = 1
			first = 0		
			last = nplot - 1
		end

	'done'	   : begin
			if fileopen eq 1 then begin
			  set_plot,plotdev
			  device,/close_file
			end
			set_plot,'X'
			!p.multi=[0,1,1]
			widget_control,event.top,/destroy
			done = 1
		end
	'bitbutton'	   : begin
			if fileopen eq 1 then begin
			  set_plot,plotdev
			  device,/close_file
			end
			set_plot,'X'
			!p.multi=[0,1,1]
			widget_control,event.top,/destroy
			done = 1
			gomenu = 1
		end
    END

		;*******************************
		;**** Make requested plot/s ****
		;*******************************

    if done eq 0 then begin
		;**** Set graphics device ****
        if print eq 1 then begin

            set_plot,plotdev
            if fileopen eq 0 then begin
                fileopen = 1
                device,filename=plotfile
	        device,/landscape
            endif

        endif else begin

            set_plot,'X'
            wset,win
    
        endelse

		;**** Draw graphics ****
        if newplot eq 1 then begin
            for i =first, last do begin
	        plot306,data.dsninc, data.header,			   $
			data.xmina, data.xmaxa, data.ymina, data.ymaxa,    $
			data.xminb, data.xmaxb, data.yminb, data.ymaxb,    $
			data.xa(i,*), data.ya(i,*), 			   $
			data.xb(i,*), data.yb(i,*), data.grpscal,	   $
			data.npu, data.npl, data.graphdex(i), data.qeff(i),$
			data.ritit, data.rtit2, data.gtit1

	           if print eq 1 then begin
	               message = 'Plot written to print file.'
	               grval = {WIN:0, MESSAGE:message}
	               widget_control,event.id,set_value=grval
	          endif
             endfor

        endif

    endif

END

;----------------------------------------------------------------------------

PRO adas306_plot, dsninc, ngrf, grpscal, gtit1,  			$
		xmina, xmaxa, ymina, ymaxa, 				$
		xminb, xmaxb, yminb, ymaxb, 				$
		xa, ya,	xb, yb,						$
		npu, npl, graphdex, qeff, ritit, rtit2, 		$
		hrdout, hrdnam, device, header, 			$
		bitfile, gomenu, FONT=font



    COMMON plot306_blk, data, action, nplot, iplot, win, plotdev, 	$
		      plotfile, fileopen, gomenucom

		;**** Copy input values to common ****
    plotdev = device
    plotfile = hrdnam
    fileopen = 0
    nplot = ngrf
    gomenucom = gomenu

		;*************************************
		;**** Create graph display widget ****
		;*************************************

    graphid = widget_base( TITLE='ADAS306 GRAPHICAL OUTPUT', 		$
                           XOFFSET=01, YOFFSET=0 )
    device, get_screen_size = scrsz
    xwidth=scrsz(0)*0.97
    yheight=scrsz(1)*0.85
    if nplot gt 1 then multiplot = 1 else multiplot = 0
    bitval = bitfile + '/menu.bmp'
    cwid = cw_adas_graph( graphid, print=hrdout, FONT=font,		$
	                xsize = xwidth, ysize = yheight, 		$
			multiplot = multiplot, bitbutton=bitval )

                ;**** Realize the new widget ****

    widget_control, graphid, /realize

		;**** Get the id of the graphics area ****

    widget_control, cwid, get_value=grval
    win = grval.win

		;*******************************************
		;**** Put the graphing data into common ****
		;*******************************************

    data = {	DSNINC:dsninc, HEADER:header, GRPSCAL:grpscal,		$
		XMINA:xmina,	XMAXA:xmaxa, 				$
		YMINA:ymina,	YMAXA:ymaxa, 				$
		XMINB:xminb,    XMAXB:xmaxb, 				$
		YMINB:yminb,	YMAXB:ymaxb, 				$
		XA:xa, YA:ya, XB:xb, YB:yb,  				$
		NPU:npu, NPL:npl, GRAPHDEX:graphdex, QEFF:qeff,	     	$
		RITIT:ritit, RTIT2:rtit2, GTIT1:gtit1 }

		;**** Initialise plot ****
    iplot = 0
    wset,win
    plot306,	data.dsninc, data.header, 				$
		data.xmina, data.xmaxa, data.ymina, data.ymaxa, 	$
		data.xminb, data.xmaxb, data.yminb, data.ymaxb,		$
		data.xa(0,*), data.ya(0,*), 				$
		data.xb(0,*), data.yb(0,*), data.grpscal,	  	$
		data.npu, data.npl, data.graphdex(0), data.qeff(0),	$
		data.ritit, data.rtit2, data.gtit1


		;***************************
                ;**** make widget modal ****
		;***************************

    xmanager,'adas306_plot',graphid,event_handler='adas306_plot_ev', 	$
    /modal,/just_reg

    gomenu = gomenucom

END
