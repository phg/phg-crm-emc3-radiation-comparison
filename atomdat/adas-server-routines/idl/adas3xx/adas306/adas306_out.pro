; Copyright (c) 1995, Strathclyde University .
; SCCS info: Module @(#)$Header: /home/adascvs/idl/adas3xx/adas306/adas306_out.pro,v 1.2 2004/07/06 10:30:35 whitefor Exp $ Date $Date: 2004/07/06 10:30:35 $
;+
; PROJECT:
;       ADAS IBM MVS to   UNIX conversion
;
; NAME:
;	ADAS306_OUT
;
; PURPOSE:
;	IDL ADAS user interface, output options.
;
; EXPLANATION:
;	This routine creates and manages a pop-up window which allows
;	the user to select options and input data to control ADAS306
;	graphical and file output. The window is a standard one.
;
; USE:
;	This routine is used only by adas 306. See 
;       c6spf1.pro for how it is used.
;
; CALLED BY: 
;	adas306 (c6spf1.pro)
;
; INPUTS:
;	VAL	- A structure which determines the initial settings of
;		  the output options widget.  The value is passed
;		  unmodified into cw_adas306_out.pro.
;
;		  See cw_adas306_out.pro for a full description of this
;		  structure.
;
;	DSFULL	- String; The full system file name of the input 
;		  dataset selected by the user for processing.
;
;       OUTTITLE - String: the title of the output options window.
;
;	NGRF	 - Number of graphs requested by the user.
;
;       BITFILE - String; the path to the dirctory containing bitmaps
;                 for the 'escape to series menu' button.
;
; OPTIONAL INPUTS:
;	None.
;
; OUTPUTS:
;	ACT	- String; Either 'Done', 'Cancel' or 'Menu' for the button the
;		  user pressed to terminate the output options window.
;
; OPTIONAL OUTPUTS:
;	None.
;
; KEYWORD PARAMETERS:
;	DEVLIST	- A string array;  Each element of the array is the
;		  name of a hardcopy graphics output device.  This
;		  list must mirror the DEVCODE list.  DEVLIST is the
;		  list which the user will see.
;		  e.g ['Post-Script','HP-PCL','HP-GL']
;
;	FONT	- Supplies the font to be used for the interface widgets.
;
; CALLS:
;	CW_ADAS306_OUT	Creates the output options widget.
;	DYNLABEL	Sets dynamic_resize keyword for label widgets.
;	XMANAGER
;	See side effects for widget management routine.
;
; SIDE EFFECTS:
;	This routine uses a common block OUT306_BLK to maintain its state.
;	ADAS306_OUT_EV	is included in this file and is called
;			indirectly from XMANAGER during widget
;			management.
;
; CATEGORY:
;	Widgets
;
; WRITTEN:
;       William Osborn, Tessella Support Services plc, 23-May-1996
;
; MODIFIED:
;	1.1	William Osborn
;		First Release
;	1.2    	William Osborn
;		Added dynlabel procedure
;
; VERSION:
;	1.1	23-05-96
;	1.2	09-07-96
;-
;-----------------------------------------------------------------------------


pro adas306_out_ev, event

    common out306_blk,action,value
	

		;**** Find the event type and copy to common ****

    action = event.action

    case action of

		;**** 'Done' button ****

	'Done'  : begin

			;**** Get the output widget value ****

     		     child = widget_info(event.id,/child)
		     widget_control,child,get_value=value 

			;*****************************************
			;**** Kill the widget to allow IDL to ****
			;**** continue and interface with     ****
			;**** FORTRAN only if there is work   ****
			;**** for the FORTRAN to do.          ****
			;*****************************************

		     if (value.grpout eq 1) or (value.texout eq 1) then begin
		         widget_control,event.top,/destroy
		     endif 
	          end


		;**** 'Cancel' button ****

	'Cancel': widget_control,event.top,/destroy

		;**** 'Menu' button ****

	'Menu': widget_control,event.top,/destroy

    endcase

end

;-----------------------------------------------------------------------------


pro adas306_out, val, dsfull, act, outtitle, ngrf, bitfile, devlist=devlist, $
                 font=font

    common out306_blk,action,value

		;**** Copy value to common ****

    value = val

		;**** Set defaults for keywords ****

    if not (keyword_set(font)) then font = ''
    if not (keyword_set(devlist)) then devlist = ''

		;***************************************
		;**** Pop-up a new widget           ****
		;***************************************

                ;**** create base widget ****

    outid = widget_base(title=outtitle,xoffset=100,yoffset=0)

		;**** Declare output options widget ****

    cwid = cw_adas306_out(outid, dsfull, ngrf, bitfile, value=value,  	$
  			devlist=devlist, font=font )

		;**** Realize the new widget ****

    dynlabel, outid
    widget_control,outid,/realize

		;**** make widget modal ****

    xmanager,'adas306_out',outid,event_handler='adas306_out_ev',/modal,$
			/just_reg
 
		;**** Return the output value from common ****

    act = action
    val = value

END

