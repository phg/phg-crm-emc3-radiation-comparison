;----------------------------------------------------------------------
;+
; PROJECT    :  ADAS
;
; NAME       :  run_adas307
;
; PURPOSE    :  Runs ADAS307 bulk CX coefficient generation code
;               as an IDL subroutine.
;
; ARGUMENTS  :  All output arguments will be defined appropriately.
;
;               NAME      I/O    TYPE   DETAILS
; INPUTS     :  adf01      I     str     CX cross section file.
;               mass       I     double  mass of receiver.
;               ref_te     I     double  electron temperature
;               ref_tion   I     double  reference ion temperature requested
;               ref_dens   I     double  electron densitie requested
;               ref_dion   I     double  reference ion densities requested
;               ref_zeff   I     double  reference Zeff
;               ref_bmag   I     double  reference magnetic field
;               ref_energy I     double  reference beam energy
;               tion       I     double  ion temperatures in adf12 file
;               dion       I     double  ion densities in adf12 file
;               zeff       I     double  Zeff values in adf12 file
;               bmag       I     double  magnetic filed values in adf12 file
;               energy     I     double  beam energies in adf12 file
;               n1req      I     int     upper n of transition
;               n2req      I     int     lower n of transition
;               adf12      I     str     name of output adf12 passing file.
;               log        I     str    name of output text file.
;                                       (defaults to no output).
;
; KEYWORDS   :  elec       I      -     =1 return e-impact values
;                                       =0 do not (default)
;               kelvin            -     requested temperature in K (default eV)
;
; OUTPUTS    :  The adf12 and (optional) log files are written.
;
;
; NOTES      :  - run_adas307 used the spawn command. Therefore IDL v5.3
;                 cannot be used. Any other version will work.
;               - The Eikonal model model for CX rates cannot be set
;                 in this version.
;
; AUTHOR     :  Martin O'Mullane
;
; DATE       :  09-10-2005
;
; UPDATE     :
;
; MODIFIED:
;       1.1     Martin O'Mullane
;                - First version.
;
; VERSION:
;       1.1     09-10-2005
;-
;----------------------------------------------------------------------

PRO run_adas307_calc, file, nl, nu, mass,                                $
                      ref_te_in, ref_tion_in, ref_dens, ref_dion,        $
                      ref_energy, ref_zeff, ref_bmag,                    $
                      tion_in,  dion, energy, zeff, bmag,                $
                      is_cx, is_elec, is_log, output_file, outfile_adf12

; Now run the ADAS307 code - the input has been checked already.
;
; This code is very baroque. Spawn the adas307 and then adas307b parts.

fortdir = getenv('ADASFORT')


date  = xxdate()
title = 'run_adas307 IDL command version'



; Spawn the executable which interpolates the CX data

spawn, fortdir+'/adas307.out', unit=pipe, /noshell, PID=pid

printf, pipe, date[0]
printf, pipe, 'NO'
printf, pipe, file

cxsetp, pipe, mxtab, mxgrf, symbd, idz0, symbr, irz0, irz1, irz2, ngrnd, ntot


printf, pipe, 0L
printf, pipe, 0L

printf, pipe, 0L
printf, pipe, 0L

idum = -1L
fdum = 0.0

readf, pipe, idum
mxneng = idum
readf, pipe, idum
mxnshl = idum


enrgya = dblarr(mxneng)
for j = 0, mxneng-1 do begin
   readf, pipe, fdum
   enrgya[j] = fdum
endfor

alphaa = dblarr(mxneng)
for j = 0, mxneng-1 do begin
   readf, pipe, fdum
   alphaa[j] = fdum
endfor

xsecna = dblarr(mxneng, mxnshl)
for i = 0, mxneng-1 do begin
   for j = 0, mxnshl-1 do begin
      readf, pipe, fdum
      xsecna[i,j] = fdum
   endfor
endfor

fracla = dblarr(mxneng, mxnshl*(mxnshl+1)/2)
for i = 0, mxneng-1 do begin
   for j = 0, (mxnshl*(mxnshl+1)/2)-1 do begin
      readf, pipe, fdum
      fracla[i,j] = fdum
   endfor
endfor

readf, pipe, idum
nminf = idum
readf, pipe, idum
nmaxf = idum
readf, pipe, idum
nenrgy = idum


printf, pipe, 0L
printf, pipe, 1L

close, /all


; Spawn the executable which generates the output files

spawn, fortdir+'/adas307b.out', unit=pipe, /noshell, PID=pid

printf, pipe, 0L

printf, pipe, output_file
printf, pipe, outfile_adf12
printf, pipe, title
printf, pipe, ' '


if is_log then texout = 1 else texout = 0
printf, pipe, texout
printf, pipe, 0L
printf, pipe, 1L
printf, pipe, 0L

printf, pipe, 17L
printf, pipe, 14L

nbeam = n_elements(energy)
ndion = n_elements(dion)
ntion = n_elements(tion_in)
nzeff = n_elements(zeff)
nbmag = n_elements(bmag)

printf, pipe, nbeam
printf, pipe, ndion
printf, pipe, ntion
printf, pipe, nzeff
printf, pipe, nbmag

for j = 0, nbeam-1 do printf, pipe, energy[j]
for j = 0, ndion-1 do printf, pipe, dion[j]
for j = 0, ntion-1 do printf, pipe, tion_in[j]
for j = 0, nzeff-1 do printf, pipe, zeff[j]
for j = 0, nbmag-1 do printf, pipe, bmag[j]

; Data from adf01 file

for j = 0, mxneng-1 do printf, pipe, enrgya[j]
for j = 0, mxneng-1 do printf, pipe, alphaa[j]

for i = 0, mxneng-1 do begin
   for j = 0, mxnshl-1 do printf, pipe, xsecna[i,j]
endfor

for i = 0, mxneng-1 do begin
   for j = 0, (mxnshl*(mxnshl+1)/2) - 1 do printf, pipe, fracla[i,j]
endfor


printf, pipe, nu
printf, pipe, nl

printf, pipe, 1L
if is_cx then iemms = 1L
if is_elec then iemms = 2L
printf, pipe, iemms
printf, pipe, 1L

printf, pipe, mass
printf, pipe, ref_tion_in
printf, pipe, ref_te_in
printf, pipe, ref_dion
printf, pipe, ref_dens
printf, pipe, ref_zeff
printf, pipe, ref_bmag
printf, pipe, ref_energy

printf, pipe, idz0
printf, pipe, irz0
printf, pipe, irz1
printf, pipe, irz2

printf, pipe, nminf
printf, pipe, nmaxf
printf, pipe, nenrgy

printf, pipe, file
printf, pipe, date[0]
printf, pipe, symbr
printf, pipe, symbd
printf, pipe, 1L

idum = 1L
readf, pipe, idum
readf, pipe, idum
readf, pipe, idum
readf, pipe, idum
readf, pipe, idum
readf, pipe, idum


; Wait until the output files are written

adas_wait, pid, delay=0.3

END



;----------------------------------------------------------------------------

PRO run_adas307, adf01      =  adf01,      $
                 mass       =  mass,       $
                 ref_te     =  ref_te,     $
                 ref_tion   =  ref_tion,   $
                 ref_dens   =  ref_dens,   $
                 ref_dion   =  ref_dion,   $
                 ref_zeff   =  ref_zeff,   $
                 ref_bmag   =  ref_bmag,   $
                 ref_energy =  ref_energy, $
                 tion       =  tion,       $
                 dion       =  dion,       $
                 zeff       =  zeff,       $
                 bmag       =  bmag,       $
                 energy     =  energy,     $
                 n1req      =  n1req,      $
                 n2req      =  n2req,      $
                 elec       =  elec,       $
                 kelvin     =  kelvin,     $
                 adf12      =  adf12,      $
                 log        =  log,        $
                 help       =  help

; If asked for help

if keyword_set(help) then begin
   doc_library, 'run_adas307'
   return
endif


; First check that we are not using IDL v5.3

if !version.release EQ '5.3' then begin
   message, 'This version of IDL (v5.3) will not work - ' + $
            'change to any other version 5 or higher'
   return
endif


; Check that we get all inputs and that they are correct. Otherwise print
; a message and return to command line.

on_error, 2

; An output file is the only output - make sure it's present

if n_elements(adf12) EQ 0 then begin
    message,'An adf12 output filename is required'
endif else outfile_adf12 = adf12


; Check the file name and extract the atomic number and mass from line 1

if n_elements(adf01) EQ 0 then begin
    message,'A CX cross section ion file (adf01) is required'
endif else file = adf01

file_acc, file, exist, read, write, execute, filetype
if exist ne 1 then message, 'adf01 file does not exist '+file
if read ne 1 then message, 'adf01 file cannot be read from this userid '+file

str = ''
openr, lun, file, /get_lun
readf, lun, str
free_lun, lun

elem = strtrim(strmid(str,0,2),2)
st_z = strtrim(strmid(str,3,2),2)
reads, st_z, z

xxeiam, strupcase(elem), mass_file

if n_elements(mass) EQ 0 then mass = mass_file



; Set suitable defaults for the keywords

if keyword_set(elec) then begin
   is_cx   = 0
   is_elec = 1
endif else begin
   is_cx   = 1
   is_elec = 0
endelse


; Necessary inputs

; Limit n1req and n2req to one even though ADAS308 can tolerate 2.

if n_elements(n1req) NE 1 OR n_elements(n2req) NE 1 then $
                             message, 'Only one n-n prediction allowed'

nl = min([n1req,n2req], max=nu)




; Reference and varying inputs

if n_elements(ref_te)     NE 1 then message, 'Error in reference electron temperature'
if n_elements(ref_tion)   NE 1 then message, 'Error in reference ion temperature'
if n_elements(ref_dens)   NE 1 then message, 'Error in reference electron density'
if n_elements(ref_dion)   NE 1 then message, 'Error in reference ion density'
if n_elements(ref_energy) NE 1 then message, 'Error in reference energy'
if n_elements(ref_zeff)   NE 1 then message, 'Error in reference Zeff'
if n_elements(ref_bmag)   NE 1 then message, 'Error in reference magnetic field'

ref_te     = cast_value(ref_te,     type='double', mess='Reference electron temperature')
ref_tion   = cast_value(ref_tion,   type='double', mess='Reference ion temperature')
ref_dens   = cast_value(ref_dens,   type='double', mess='Reference electron density')
ref_dion   = cast_value(ref_dion,   type='double', mess='Reference ion density')
ref_energy = cast_value(ref_energy, type='double', mess='Reference beam energy')
ref_zeff   = cast_value(ref_zeff,   type='double', mess='Reference Zeff')
ref_bmag   = cast_value(ref_bmag,   type='double', mess='Reference magnetic field')

if n_elements(tion)   LT 1 then message, 'Error in requested ion temperature'
if n_elements(dion)   LT 1 then message, 'Error in requested ion density'
if n_elements(energy) LT 1 then message, 'Error in requested energy'
if n_elements(zeff)   LT 1 then message, 'Error in requested Zeff'
if n_elements(bmag)   LT 1 then message, 'Error in requested magnetic field'

tion   = cast_value(tion,   type='double', mess='Ion temperature')
dion   = cast_value(dion,   type='double', mess='Ion density')
energy = cast_value(energy, type='double', mess='Beam energy')
zeff   = cast_value(zeff,   type='double', mess='Zeff')
bmag   = cast_value(bmag,   type='double', mess='Magnetic field')


; Temperature in Kelvin?

if keyword_set(kelvin) then begin
   tion_in     = tion/11605.0D0
   ref_te_in   = ref_te/11605.0D0
   ref_tion_in = ref_tion/11605.0D0
endif else begin
   tion_in     = tion
   ref_te_in   = ref_te
   ref_tion_in = ref_tion
endelse


; Output file (paper.txt)

if n_elements(log) EQ 0 then begin
   output_file = 'NULL'
   is_log = 0
endif else begin
   output_file = log
   is_log = 1
endelse


; Run ADAS307

run_adas307_calc, file, nl, nu, mass,                               $
                  ref_te_in, ref_tion_in, ref_dens, ref_dion,       $
                  ref_energy, ref_zeff, ref_bmag,                   $
                  tion_in, dion, energy, zeff, bmag,                $
                  is_cx, is_elec, is_log, output_file, outfile_adf12


; Free up allocated units

close,/all

END
