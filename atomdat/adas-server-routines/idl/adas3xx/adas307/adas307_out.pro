; Copyright (c) 1996, Strathclyde University.
; SCCS info: Module @(#)$Header: /home/adascvs/idl/adas3xx/adas307/adas307_out.pro,v 1.1 2004/07/06 10:31:13 whitefor Exp $ Date $Date: 2004/07/06 10:31:13 $
;
;+
; PROJECT:
;       ADAS IBM MVS to UNIX conversion
;
; NAME:
;	ADAS307_OUT
;
; PURPOSE:
;	IDL ADAS user interface, output options.
;
; EXPLANATION:
;	This routine creates and manages a pop-up window which allows
;	the user to select options and input data to control ADAS307
;	graphical and file output.
;
; USE:
;	This routine is specifically for use with adas307.             
;
; INPUTS:
;	VAL	- A structure which determines the initial settings of
;		  the output options widget.  The value is passed
;		  unmodified into cw_adas307_out.pro.
;
;		  See cw_adas307_out.pro for a full description of this
;		  structure.
;
;	DSFULL	- String; The full system file name of the input 
;		  dataset selected by the user for processing.
;
;       BITFILE - String; the path to the dirctory containing bitmaps
;                 for the 'escape to series menu' button.
;
; OPTIONAL INPUTS:
;	None.
;
; OUTPUTS:
;	ACT	- String; Either 'Done' or 'Cancel' for the button the
;		  user pressed to terminate the output options window.
;
; OPTIONAL OUTPUTS:
;	None.
;
; KEYWORD PARAMETERS:
;	FONT	- Supplies the font to be used for the interface widgets.
;
; CALLS:
;	CW_ADAS307_OUT	Creates the output options widget.
;	XMANAGER
;	See side effects for widget management routine.
;
; SIDE EFFECTS:
;	This routine uses a common block OUT307_BLK to maintain its state.
;	ADAS307_OUT_EV	is included in this file and is called
;	indirectly from XMANAGER during widget management.
;
; CATEGORY:
;	Compound Widget.
;
; WRITTEN:
;       William Osborn, Tessella Support Services plc, 24th May 1996
;
; MODIFIED:
;	1.1	William Osborn		24/05/96
;
; VERSION:
;	1.1	First Release. Written using adas309_out as a template.
;
;-
;-----------------------------------------------------------------------------


pro adas307_out_ev, event

  common out307_blk,action,value
	

		;**** Find the event type and copy to common ****

    action = event.action

    case action of

		;**** 'Run Now' button ****

	'Run Now'  : begin

			;**** Get the output widget value ****

     		     child = widget_info(event.id, /child)
		     widget_control, child, get_value=value 

                        ;*****************************************
			;**** Kill the widget to allow IDL to ****
			;**** continue and interface with     ****
			;**** FORTRAN only if there is work   ****
			;**** for the FORTRAN to do.          ****
			;*****************************************

		     if (value.datout eq 1) or (value.texout eq 1) then begin
		        widget_control, event.top, /destroy
		     endif 
	          end


                ;**** 'Run in Batch' button ****

        'Run In Batch'  : begin

                        ;**** Get the output widget value ****

                     child = widget_info(event.id, /child)
                     widget_control, child, get_value=value

                        ;*****************************************
                        ;**** Kill the widget to allow IDL to ****
                        ;**** continue and interface with     ****
                        ;**** FORTRAN only if there is work   ****
                        ;**** for the FORTRAN to do.          ****
                        ;*****************************************

                     if (value.datout eq 1) or (value.texout eq 1) then begin
                        widget_control, event.top, /destroy
                     endif
                  end

		;**** 'Cancel' button ****

	'Cancel': widget_control, event.top, /destroy

		;**** 'Menu' button ****

	'Menu': widget_control, event.top, /destroy

        ELSE: begin   				
			;do nothing 
              end
    endcase

end

;-----------------------------------------------------------------------------


pro adas307_out, val, dsfull, act, bitfile, font=font

  common out307_blk, action, value

		;**** Copy value to common ****

  value = val

		;**** Set defaults for keywords ****

  if not (keyword_set(font)) then font = ''

		;***************************************
		;**** Pop-up a new widget           ****
		;***************************************

                ;**** create base widget ****

  outid = widget_base(title='ADAS307 OUTPUT OPTIONS', xoffset=100, yoffset=100)

		;**** Declare output options widget ****

  cwid = cw_adas307_out(outid, dsfull, bitfile, value=value,  	 	$
			 font=font )

		;**** Realize the new widget ****

  dynlabel, outid
  widget_control, outid, /realize

		;**** make widget modal ****

  xmanager,'adas307_out', outid, event_handler='adas307_out_ev',	$
                          /modal, /just_reg
 
		;**** Return the output value from common ****

  act = action
  val = value

END

