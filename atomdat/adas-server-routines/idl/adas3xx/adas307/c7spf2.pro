; Copyright (c) 1996, Strathclyde University.
; SCCS Info: Module @(#)$Header: /home/adascvs/idl/adas3xx/adas307/c7spf2.pro,v 1.2 2004/07/06 11:54:47 whitefor Exp $ Date $Date: 2004/07/06 11:54:47 $
;
;+
; PROJECT:
;       ADAS IBM MVS to UNIX conversion
;
; NAME:
;	C7SPF2
;
; PURPOSE:
;	IDL user interface and communications with ADAS307 FORTRAN
;	process via pipe.
;
; EXPLANATION:
;	First this routine reads some information from ADAS307 FORTRAN
;	via the UNIX pipe.  Next the second FORTRAN program ADAS307B
;	is spawned to perform the calculation. Data is fed to this process
;	by a second pipe and the results read back in. The second FORTRAN
;	process is then terminated and communications with the first
;	begin again.
;	Communications are with the FORTRAN subroutines RUN307 and 
;	ADAS307B for the second FORTRAN program and ADAS307 for the first.
;
; USE:
;	The use of this routine is specific to ADAS307, See adas307.pro.
;
; INPUTS:
;	PIPE	- The IDL unit number of the bi-directional pipe to the
;		  ADAS307 FORTRAN process.
;
;	DSFULL  - The name of the data set being analyzed.
;
;	VALUE	- A structure which determines the initial settings of
;		  the dataset selection widget.  The initial value is
;		  set in adas307.pro.  VALUE is passed un-modified
;		  through to cw_adas_in.pro, see that routine for a full
;		  description.
;
;       PROCVAL - A structure which contains the settings from the
;		  processing widget - see cw_adas307_proc.pro for details.
;
;       symbd   - (String) Element symbol of donor (for information).
;
;       idz0    - (Int) Donor nuclear charge (for information).
;
;       symbr   - (String) Element symbol of receiver (for information).
;
;       irz0    - (Int) Receiver nuclear charge (for information).
;
;       irz1    - (Int) Receiver ion initial charge (for information).
;
;       irz2    - (Int) Receiver ion final charge (for information).
;
;	ipset   - (Int) the number of the run to be used in the output.
;
;	DATE	- (String) the date.
;
;	HEADER  - Header information used for text output.
;
; OPTIONAL INPUTS:
;	None.
;
; OUTPUTS:
;	LPEND	- Integer; indicates user action on leaving output
;		  selections.  lpend is set to 1 if the user exited
;		  with the 'Cancel' button, 0 if the user exited
;		  with 'Run Now' and 2 if the user exited with 'Run in
;		  Batch'.
;
;	VALUE	- On output the structure records the final settings of
;		  the output selection widget if the user pressed a
;		  'Run..' button otherwise it is not changed from input.
;
; OPTIONAL OUTPUTS:
;	None.
;
; KEYWORD PARAMETERS:
;
;	FONT	- Supplies the font to be used for the interface widgets.
;
; CALLS:
;
; SIDE EFFECTS:
;	This routine communicates with the ADAS307 FORTRAN processes
;	via UNIX pipes.
;	It also pops up an information widget which keeps the user 
;	updated as the calculation progresses.
;
; CATEGORY:
;	Adas system.
;
; WRITTEN:
;       William Osborn, Tessella Support Services plc, 24-May-1996
;
; MODIFIED:
;	1.1	William Osborn 				
;               First release. Written using c9spf2.pro as a template.
;       1.2     William Osborn
;               Added xsize=560 to widget_draw
;
; VERSION:
;	1.1	24-05-96
;       1.2	14-10-96
;
;-
;-----------------------------------------------------------------------------

PRO   c7spf2, pipe, value, dsfull, header, procval, 			$
              idz0, irz0, irz1, irz2, symbr, symbd, ipset, date, 	$
              fortdir, FONT=font


                ;**** Return to caller on error ****

    ON_ERROR, 2

                ;**** Set defaults for keywords ****

    IF NOT (KEYWORD_SET(font)) THEN font = ''

		;**** Create an information widget ****

    complete = 0
    widget_control, /hourglass
    base = widget_base(/column, xoffset=300, yoffset=200,               $
    title = "ADAS307: INFORMATION")
    lab0 = widget_label(base, value='')
    lab1 = widget_label(base,                                           $
    value="    ADAS307 SCANS UNDERWAY - PLEASE WAIT    ")
    grap = widget_draw(base, ysize=20, xsize=560)
    lab2 = widget_label(base,                                           $
    value = "PROCESSING  "+strcompress(complete, /remove_all)+          $
    "% COMPLETED")
    widget_control, base, /realize

		;**** Define some variables before read from pipe ****

    intdum = 0
    fltdum = 0.0
    readf, pipe, intdum
    mxneng = intdum
    readf, pipe, intdum
    mxnshl = intdum
    enrgya = dblarr(mxneng)
    alphaa = dblarr(mxneng)
    xsecna = dblarr(mxneng, mxnshl)
    fracla = dblarr(mxneng, (mxnshl*(mxnshl+1))/2)
    for i = 0, (mxneng - 1) do begin
        readf, pipe, fltdum
        enrgya(i) = fltdum
    endfor
    for i = 0, (mxneng - 1) do begin
        readf, pipe, fltdum
        alphaa(i) = fltdum
    endfor
    for i = 0, (mxneng - 1) do begin
        for j = 0, (mxnshl - 1) do begin
            readf, pipe, fltdum
            xsecna(i,j) = fltdum
        endfor
    endfor
    for i = 0, (mxneng - 1) do begin
        for j = 0, ( (mxnshl*(mxnshl+1)/2.0) - 1 ) do begin
            readf, pipe, fltdum
            fracla(i,j) = fltdum
        endfor
    endfor
    readf, pipe, intdum
    nminf = intdum
    readf, pipe, intdum
    nmaxf = intdum
    readf, pipe, intdum
    nenrgy = intdum

		;**** Spawn the second FORTRAN routine 'adas307b.out' ****

    spawn, fortdir+'/adas307b.out', unit=pipeb, /noshell, PID=pidb


                ;*********************************************
                ;**** Write initial information to FORTRAN ***
                ;**** Communications are with adas307b.for ***
                ;*********************************************

    nobatch = 0
    iunt07 = 17
    iunt14 = 14
    printf, pipeb, nobatch		;we are not running in batch
    printf, pipeb, value.texdsn
    printf, pipeb, value.datdsn
    printf, pipeb, header        
    printf, pipeb, procval.title 
    printf, pipeb, value.texout
    printf, pipeb, value.texapp
    printf, pipeb, value.datout
    printf, pipeb, value.datapp
    printf, pipeb, iunt07
    printf, pipeb, iunt14

		;**** Update information widget ****

    for i=0,99 do begin
        x = (float(i)+1)/1000.0
        plots, [x, x],[0.0,1.0], /normal
    endfor
    widget_control, lab2, set_value="PROCESSING 10% COMPLETED"

		;*********************************************
		;**** Write all necessary data to FORTRAN ****
		;**** Communications are with run307.for  ****
		;*********************************************

    printf, pipeb, procval.nbmeng
    printf, pipeb, procval.ndensz
    printf, pipeb, procval.ntiev 
    printf, pipeb, procval.nzeff
    printf, pipeb, procval.nbmag
    for i = 0, (procval.nbmeng - 1) do begin
        printf, pipeb, procval.bmenga(i)
    endfor
    for i = 0, (procval.ndensz - 1) do begin
        printf, pipeb, procval.densza(i)
    endfor
    for i = 0, (procval.ntiev  - 1) do begin
        printf, pipeb, procval.tieva(i)
    endfor
    for i = 0, (procval.nzeff  - 1) do begin
        printf, pipeb, procval.zeffa(i)
    endfor
    for i = 0, (procval.nbmag  - 1) do begin
        printf, pipeb, procval.bmaga(i)
    endfor
    for i = 0, (mxneng - 1) do begin
        printf, pipeb, enrgya(i)
    endfor 
    for i = 0, (mxneng - 1) do begin
        printf, pipeb, alphaa(i)
    endfor 
    for i = 0, (mxneng - 1) do begin
        for j = 0, (mxnshl - 1) do begin
            printf, pipeb, xsecna(i,j)
        endfor
    endfor 
    for i = 0, (mxneng - 1) do begin
        for j = 0, ( (mxnshl*(mxnshl+1)/2.0) - 1 ) do begin
            printf, pipeb, fracla(i,j)
        endfor
    endfor 

		;**** Update information widget ****

    for i=100,199 do begin
        x = (float(i)+1)/1000.0
        plots, [x, x],[0.0,1.0], /normal
    endfor
    widget_control, lab2, set_value="PROCESSING 20% COMPLETED"

    printf, pipeb, procval.ntu
    printf, pipeb, procval.ntl
    printf, pipeb, (procval.itheor + 1)
    printf, pipeb, (procval.iemms + 1)
    printf, pipeb, (procval.ibstat + 1)
    printf, pipeb, procval.ramsno
    printf, pipeb, procval.tiev
    printf, pipeb, procval.tev
    printf, pipeb, procval.densz
    printf, pipeb, procval.dens
    printf, pipeb, procval.zeff
    printf, pipeb, procval.bmag
    printf, pipeb, procval.bmeng
    printf, pipeb, idz0
    printf, pipeb, irz0 
    printf, pipeb, irz1 
    printf, pipeb, irz2 
    printf, pipeb, nminf
    printf, pipeb, nmaxf
    printf, pipeb, nenrgy
    printf, pipeb, dsfull
    printf, pipeb, date(0)
    printf, pipeb, symbr  
    printf, pipeb, symbd  
    printf, pipeb, ipset  

		;**** Read in progress reports from FORTRAN ****

    readf, pipeb, input  
    for i=200,299 do begin
        x = (float(i)+1)/1000.0
        plots, [x, x],[0.0,1.0], /normal
    endfor
    mess = "PROCESSING 30% COMPLETED"
    widget_control, lab2, set_value=mess
    readf, pipeb, input  
    for i=300,499 do begin
        x = (float(i)+1)/1000.0
        plots, [x, x],[0.0,1.0], /normal
    endfor
    mess = "PROCESSING 50% COMPLETED"
    widget_control, lab2, set_value=mess
    readf, pipeb, input  
    for i=500,699 do begin
        x = (float(i)+1)/1000.0
        plots, [x, x],[0.0,1.0], /normal
    endfor
    mess = "PROCESSING 70% COMPLETED"
    widget_control, lab2, set_value=mess
    readf, pipeb, input  
    for i=700,799 do begin
        x = (float(i)+1)/1000.0
        plots, [x, x],[0.0,1.0], /normal
    endfor
    mess = "PROCESSING 80% COMPLETED"
    widget_control, lab2, set_value=mess
    readf, pipeb, input  
    for i=800,899 do begin
        x = (float(i)+1)/1000.0
        plots, [x, x],[0.0,1.0], /normal
    endfor
    mess = "PROCESSING 90% COMPLETED"
    widget_control, lab2, set_value=mess

		;**** Wait for a continue signal from the FORTRAN ****

    readf, pipeb, input 
    for i=900,999 do begin
        x = (float(i)+1)/1000.0
        plots, [x, x],[0.0,1.0], /normal
    endfor

		;**** Destroy the information widget ****

    widget_control, base, /destroy

END
