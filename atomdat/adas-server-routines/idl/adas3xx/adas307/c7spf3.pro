; Copyright (c) 1996, Strathclyde University.
; SCCS Info: Module @(#)$Header: /home/adascvs/idl/adas3xx/adas307/c7spf3.pro,v 1.2 2004/07/06 11:54:53 whitefor Exp $ Date $Date: 2004/07/06 11:54:53 $    
;
;+
; PROJECT:
;       ADAS IBM MVS to UNIX conversion
;
; NAME:
;	C7SPF3
;
; PURPOSE:
;	This routines sets up and modifies batch control files for 
;	ADAS307 calculations which are requested by the user to be
;	run in batch.
;       There are no pipe communications between IDL and FORTRAN,
;	instead all the relevant information is fed out to 3 files:
;		batchXXXX.tmp - this contains all the actual data
;				required by the FORTRAN part B codes
;				to carry out the calculations. This
;				file can become quite large so it
;				is compressed when not being used.
;	      controlXXXX.tmp - this holds a sequence of UNIX
;				commands necessary to run the batch
;				job.
;	         infoXXXX.tmp -	this holds the initial information
;				required by the FORTRAN B codes such
;				as which units to use for I/O.
;
;	The last two files are generated in adas307.pro.
;	The XXXX is a 4 digit number generated at random which ensures
; 	there is no confusion if there is more than one batch job in the
;	queue.
;
; EXPLANATION:
;	First this routine reads some information from ADAS307 FORTRAN
;	via the UNIX pipe.  Next an information widget is created.
;	The file batchXXXX.tmp is then created and all required data
;	is written to this whilst the information widget is updated
;	to inform the user how much longer there is to go. Finally
;	the batch file is compressed, the widget is detroyed and
;	control returns to the output options screen.
;
; USE:
;	The use of this routine is specific to ADAS307, See adas307.pro.
;
; INPUTS:
;	PIPE	- The IDL unit number of the bi-directional pipe to the
;		  ADAS307 FORTRAN process.
;
;	DSFULL  - The name of the data set being analyzed.
;
;	VALUE	- A structure which determines the initial settings of
;		  the dataset selection widget.  The initial value is
;		  set in adas307.pro.  VALUE is passed un-modified
;		  through to cw_adas_in.pro, see that routine for a full
;		  description.
;
;       PROCVAL - A structure which contains the settings from the
;                 processing widget - see cw_adas307_proc.pro for details.
;
;       symbd   - (String) Element symbol of donor (for information).
;
;       idz0    - (Int) Donor nuclear charge (for information).
;
;       symbr   - (String) Element symbol of receiver (for information).
;
;       irz0    - (Int) Receiver nuclear charge (for information).
;
;       irz1    - (Int) Receiver ion initial charge (for information).
;
;       irz2    - (Int) Receiver ion final charge (for information).
;
;       ipset   - (Int) the number of the run to be used in the output.
;
;       DATE    - (String) the date.
;
;	HEADER  - Header information used for text output.
;
; OPTIONAL INPUTS:
;	None.
;
; OUTPUTS:
;	LPEND	- Integer; indicates user action on leaving output
;		  selections.  lpend is set to 1 if the user exited
;		  with the 'Cancel' button and 0 if the user exited
;		  with 'Done'.
;
;	VALUE	- On output the structure records the final settings of
;		  the output selection widget if the user pressed the
;		  'Done' button otherwise it is not changed from input.
;	
;	DSBATCH - The name of the batch file 'batchxxxx.tmp'
;
; OPTIONAL OUTPUTS:
;	None.
;
; KEYWORD PARAMETERS:
;
;	FONT	- Supplies the font to be used for the interface widgets.
;
; CALLS:
;	ADAS307_OUT	User interface - output options.
;
; SIDE EFFECTS:
;	This routine communicates with the ADAS307 FORTRAN process
;	via a UNIX pipe (only the first FORTRAN process).
;
; CATEGORY:
;	Adas system.
;
; WRITTEN:
;       William Osborn, Tessella Support Services plc, 24-May-1996
;
; MODIFIED:
;	1.1	William Osborn  				24/05/96
;	1.2	Richard Martin
;		Added check for 'gzip'- it is used if it is in the
;		users path.			
;
; VERSION:
;	1.1	First release. Written using c9spf3 as a template
;	1.2	22-09-99
;
;-
;-----------------------------------------------------------------------------

PRO   c7spf3, pipe, value, dsfull, header, procval, 			$
              idz0, irz0, irz1, irz2, symbr, symbd, ipset, date,        $
              dsbatch, fortdir, l2batch, font=font


                ;**** Create an information widget ****

    complete = 0
    widget_control, /hourglass
    base = widget_base(/column, xoffset=300, yoffset=200,               $
    title = "ADAS307: INFORMATION")
    lab0 = widget_label(base, value='', font=font)
    lab1 = widget_label(base,                                           $
    value="    ADAS307 UPDATING BATCH FILE - PLEASE WAIT    ", font=font)
    grap = widget_draw(base, xsize=450, ysize=20)
    lab2 = widget_label(base,                                           $
    value = "PROCESSING  "+strcompress(complete, /remove_all)+          $
    "% COMPLETED", font=font)
    widget_control, base, /realize

		;**** Define some variables before read from pipe ****

    intdum = 0
    fltdum = 0.0
    readf, pipe, intdum
    mxneng = intdum
    readf, pipe, intdum
    mxnshl = intdum
    enrgya = dblarr(mxneng)
    alphaa = dblarr(mxneng)
    xsecna = dblarr(mxneng, mxnshl)
    fracla = dblarr(mxneng, (mxnshl*(mxnshl+1))/2)
    for i = 0, (mxneng - 1) do begin
        readf, pipe, fltdum
        enrgya(i) = fltdum
    endfor
    for i = 0, (mxneng - 1) do begin
        readf, pipe, fltdum
        alphaa(i) = fltdum
    endfor
    for i = 0, (mxneng - 1) do begin
        for j = 0, (mxnshl - 1) do begin
            readf, pipe, fltdum
            xsecna(i,j) = fltdum
        endfor
    endfor

                ;**** Update information widget ****

    for i=0,99 do begin
        x = (float(i)+1)/1000.0
        plots, [x, x],[0.0,1.0], /normal
    endfor
    widget_control, lab2, set_value="PROCESSING 10% COMPLETED"

    for i = 0, (mxneng - 1) do begin
        for j = 0, ( (mxnshl*(mxnshl+1)/2.0) - 1 ) do begin
            readf, pipe, fltdum
            fracla(i,j) = fltdum
        endfor
    endfor
    readf, pipe, intdum
    nminf = intdum
    readf, pipe, intdum
    nmaxf = intdum
    readf, pipe, intdum
    nenrgy = intdum

                ;**** Update information widget ****

    for i=100,199 do begin
        x = (float(i)+1)/1000.0
        plots, [x, x],[0.0,1.0], /normal
    endfor
    widget_control, lab2, set_value="PROCESSING 20% COMPLETED"
       


                ;*********************************************
		;**** Generate the name of the batch file ****
                ;*********************************************

    spawn,'which gzip', result
    result2=findfile(result(0))
    if result2(0) ne '' then begin
	  gzipflag=1
    endif else begin
	  gzipflag=0	  
    endelse

    if dsbatch eq '' then begin
        dsbatch = 'batch'+strmid(strtrim(string(randomu(seed)), 2), 2, 4)
        dsbatch = dsbatch + '.tmp'
    endif else begin
	  if gzipflag eq 1 then begin
    		spawn, result2(0)+' -d ./'+dsbatch
    	  endif else begin
    		spawn, 'uncompress ./'+dsbatch
    	  endelse
    endelse

                ;**** Update information widget ****

    for i=200,299 do begin
        x = (float(i)+1)/1000.0
        plots, [x, x],[0.0,1.0], /normal
    endfor
    widget_control, lab2, set_value="PROCESSING 30% COMPLETED"
       
                ;*********************************************
                ;**** Open the batch file                 ****
                ;*********************************************

    openw, bunit, dsbatch, /GET_LUN, /APPEND

                ;*********************************************
                ;**** Write initial information to batch   ***
                ;*********************************************

    printf, bunit, value.texdsn
    printf, bunit, value.datdsn
    printf, bunit, header        
    printf, bunit, procval.title 
    printf, bunit, value.texout
    printf, bunit, value.texapp
    printf, bunit, value.datout
    printf, bunit, value.datapp


		;*********************************************
		;**** Write all data to batch file        ****
		;*********************************************

    printf, bunit, procval.nbmeng
    printf, bunit, procval.ndensz
    printf, bunit, procval.ntiev 
    printf, bunit, procval.nzeff
    printf, bunit, procval.nbmag

                ;**** Update information widget ****

    for i=300, 399 do begin
        x = (float(i)+1)/1000.0
        plots, [x, x],[0.0,1.0], /normal
    endfor
    mess = "PROCESSING 40% COMPLETED"
    widget_control, lab2, set_value=mess

    printf, bunit, procval.bmenga(0:procval.nbmeng-1)
    printf, bunit, procval.densza(0:procval.ndensz-1)
    printf, bunit, procval.tieva(0:procval.ntiev-1)
    printf, bunit, procval.zeffa(0:procval.nzeff-1)
    printf, bunit, procval.bmaga(0:procval.nbmag-1)
    printf, bunit, enrgya(0:mxneng-1)
    printf, bunit, alphaa(0:mxneng-1)

                ;**** Update information widget ****

    for i=400, 599 do begin
        x = (float(i)+1)/1000.0
        plots, [x, x],[0.0,1.0], /normal
    endfor
    mess = "PROCESSING 60% COMPLETED"
    widget_control, lab2, set_value=mess

    printf, bunit, xsecna(0:mxneng-1, 0:mxnshl-1)
    printf, bunit, fracla(0:mxneng-1, 0:(mxnshl*(mxnshl+1)/2.0) - 1)

                ;**** Update information widget ****

    for i=600, 799 do begin
        x = (float(i)+1)/1000.0
        plots, [x, x],[0.0,1.0], /normal
    endfor
    mess = "PROCESSING 80% COMPLETED"
    widget_control, lab2, set_value=mess


    printf, bunit, procval.ntu
    printf, bunit, procval.ntl
    printf, bunit, (procval.itheor + 1)
    printf, bunit, (procval.iemms + 1)
    printf, bunit, (procval.ibstat + 1)
    printf, bunit, procval.ramsno
    printf, bunit, procval.tiev
    printf, bunit, procval.tev
    printf, bunit, procval.densz
    printf, bunit, procval.dens
    printf, bunit, procval.zeff
    printf, bunit, procval.bmag
    printf, bunit, procval.bmeng
    printf, bunit, idz0
    printf, bunit, irz0 
    printf, bunit, irz1 
    printf, bunit, irz2 
    printf, bunit, nminf
    printf, bunit, nmaxf
    printf, bunit, nenrgy
    printf, bunit, dsfull
    printf, bunit, date(0)
    printf, bunit, symbr  
    printf, bunit, symbd  
    printf, bunit, ipset  
    close, bunit

                ;**** Update information widget ****

    for i=800, 899 do begin
        x = (float(i)+1)/1000.0
        plots, [x, x],[0.0,1.0], /normal
    endfor
    mess = "PROCESSING 90% COMPLETED"
    widget_control, lab2, set_value=mess

	;**** Compress the batch file, compression of order 90% ****

    if gzipflag eq 1 then begin
    		spawn, result2+' ./'+dsbatch
    endif else begin
    		spawn, 'compress ./'+dsbatch
    endelse

                ;**** Update information widget ****

    for i=900, 999 do begin
        x = (float(i)+1)/1000.0
        plots, [x, x],[0.0,1.0], /normal
    endfor

        ;**** Destroy the information widget ****

    widget_control, base, /destroy
                                      

END
