; Copyright (c) 1996, Strathclyde University.
; SCCS info: Module @(#)$Header: /home/adascvs/idl/adas3xx/adas307/cw_adas307_proc.pro,v 1.2 2004/07/06 12:39:06 whitefor Exp $ Date $Date: 2004/07/06 12:39:06 $
;+
; PROJECT:
;       ADAS IBM MVS to UNIX conversion
;
; NAME:
;	CW_ADAS307_PROC()
;
; PURPOSE:
;	Produces a widget for ADAS307 processing options/input.
;
; EXPLANATION:
;	This function creates a complicated compound widget consisting of :-
;	   a text widget holding an editable 'Run title', 
;	   the dataset name/browse widget ,
;	   an information widget detailing the receiver and donor ions,
;	   a text widget holding atomic mass number of receiver,
;	   text widgets holding the plasma parameter values,
;	   a text widget for the reference beam energy,
;	   text widgets for upper and lower levels of selected transition
;          a selector widget for the charge exchange theory,
;          a selector widget for the donor state(if eikonal model
;                               selected for above),
;          a selector widget for the emission measure model,
;	   five table widgets:
;		- values for beam energy scan
;		- values for plasma ion density scan
;		- values for plasma ion temperature scan
;		- values for plasma effective Z scan
;		- values for plasma magnetic induction scan
;	   a message widget, and 'Cancel', 'Run now' and 'Run in batch'
;          buttons.
;
;	The compound widgets included in this widget are self managing.
;	This widget only manages events from the 'Cancel' button and the 
;	'Run..' buttons.
;
;	This widget only generates events for the 'Cancel' and 'Run...'
;	buttons.
;
; USE:
;	This widget is specific to ADAS307, see adas307_proc.pro
;	for use.
;
; INPUTS:
;	TOPPARENT- Long integer, ID of parent widget.
;
;	DSFULL	- String; The full system file name of the input 
;		  dataset selected by the user for processing.
;
;       symbd   - (String) Element symbol of donor (for information).
;
;       idz0    - (Int) Donor nuclear charge (for information).
;
;       symbr   - (String) Element symbol of receiver (for information).
;
;       irz0    - (Int) Receiver nuclear charge (for information).
;
;       irz1    - (Int) Receiver ion initial charge (for information).
;
;       irz2    - (Int) Receiver ion final charge (for information).
;
;       ntot    - (Int) Maximum allowed N quantum number.
;
;       ngrnd   - (Int) Minimum allowed N quantum number.
;
;	Most of the inputs map exactly onto variables of the same
;	name in the ADAS307 FORTRAN program.
;
; OPTIONAL INPUTS:
;	None
;
; OUTPUTS:
;	The return value of this function is the ID of the declared widget.
;
;	ACT 	- String; result of this widget, 'Cancel', 'Run Now' or
;			'Run in Batch'.
;
;       BITFILE - String; the path to the dirctory containing bitmaps
;                 for the 'escape to series menu' button.
;
; OPTIONAL OUTPUTS:
;	None
;
; KEYWORD PARAMETERS:
;	PROCVAL	- A structure which determines the initial settings of
;		  the processing options widget.
;
;		  The default PROCVAL is created thus;
;
;     		ps = {proc307_set, 		$
;			new   : 0,		$
;                	title : '',             $
;                       ramsno: 0.0,            $
;                       tiev  : 0.0 ,           $
;                       tev   : 0.0 ,           $
;                       densz : 0.0 ,           $
;                       dens  : 0.0 ,           $
;                       zeff  : 0.0 ,           $
;                       bmag  : 0.0 ,           $
;                       bmeng : 0.0 ,           $
;                       ntu   : 0,              $
;                       ntl   : 0,              $
;                       nbmeng: 0,              $
;                       ndensz: 0,              $
;                       ntiev : 0,              $
;                       nzeff : 0,              $
;                       nbmag : 0,              $
;                       bmenga: fltarr(24),     $
;                       densza: fltarr(24),     $
;                       tieva : fltarr(12),     $
;                       zeffa : fltarr(12),     $
;                       bmaga : fltarr(12),     $
;			itable: 0,		$
;			ibstat: 0,		$
;			iemms:  0,		$
;			itheor: 0		$
;             }
;
;		NEW     Flag which defines whether or not default values
;			exist or not. (< 0 if no values exist)
; 		TITLE	Entered general title for program run
;
;		RAMSNO  (Real) atomic mass number of receiver ion
;
;		TIEV    (Real) Plasma ion temp. in eV
;
;		TEV	(Real) Plasma electron temp. in eV
;
;		DENSZ	(Real) Plasma ion density in cm-3
;
;		DENS    (Real) Plasma electron density in cm-3
;
;		ZEFF	(Real) Effective Z
;
;		BMAG	(Real) Magnetic field B (T)
;
;		BMENG	(Real) Reference beam energy (eV/amu)
;
;		NTU	(Int) Upper prin. quant. no. for selected transition
;
;		NTL	(Int) Lower prin. quant. no. for selected transition
;
;		NBMENG	(Int) No. of values for beam energy scan
;
;		NDENSZ	(Int) No. of values for plasma ion density scan
;
;		NTIEV	(Int) No. of values for plasma ion temp. scan
;
;		NZEFF	(Int) No. of values for plasma effective Z scan
;
;		NBMAG	(Int) No. of values for plasma mag. induction scan
;
;		BMENGA	(Fltarr) Plasma beam energies for scan (eV/amu)
;
;		DENSZA	(Fltarr) Plasma ion densities for scan (cm-3)
;
;		TIEVA	(Fltarr) Plasma ion temps. for scan (eV)
;
;		ZEFFA	(Fltarr) Plasma effective Z's for scan
;
;		BMAGA	(Fltarr) Plasma mag. induction values for scan (T)
;
;		ITABLE	(Int) Flag indicating which table is active:
;			0 - Plasma beam energies
;			1 - Plasma ion densities   
;			2 - Plasma ion temps.
;			3 - Plasma effective Z's 
;			4 - Plasma mag. induction values
;
;		ITHEOR	(Int) Flag indicating charge exchange theory to 
;			be used.
;			0 = Use input data set
;			1 = Use Eikonal model
;
;		IBSTAT	(Int) Flag indicating which donor state is required
;			for Eikonal model (see above).
;			0 = H(1S)
;			1 = H(2S)
;			2 = H(2P)
;			3 = He(1s2)
;			4 = He(1s2s)
;
;		IEMMS	(Int) Flag indicating which emission model is req'd.
;			0 = Charge exchange
;			1 = Electron impact excitation
;			2 = Radiative recombination
;
;		Most of these structure elements map onto variables of
;		the same name in the ADAS307 FORTRAN program.
;
;
;	UVALUE	- A user value for the widget. Default 0.
;
;	FONT_LARGE - The name of a larger font.  Default current system
;		     font
;
;	FONT_SMALL - The name of a smaller font. Default current system
;		     font.
;
;	EDIT_FONTS - A structure of two fonts used in the table editor
;		     adas_edtab.pro, {font_norm:'',font_input:''}.  Defaults
;		     to current system font.
;
;	NUM_FORM   - String array; Numeric format to use in tables.  Default
;			'(e10.3)'
;		     
;
; CALLS:
;	POPUP		Popup warning window with buttons.
;	CW_ADAS_DSBR	Dataset name and comments browsing button.
;	CW_ADAS_TABLE   Adas data table widget.
;	CW_OPT_VALUE    Adas option selection widget with required input 
;			value. 
;
;	See side effects for other related widget management routines.
;
; SIDE EFFECTS:
;
;	The following widget management routines are included in this file;
;	PROC307_GET_VAL()	Returns the current PROCVAL structure.
;	PROC307_EVENT()		Process and issue events.
;	
; CATEGORY:
;	Compound Widget
;
; WRITTEN:
;       William Osborn, Tessella Support Services plc, 24-May-1996
;
; MODIFIED:
;	1.1	William Osborn  	 		24-05-96
;	1.2	Richard Martin				30-06-97
;
; VERSION:
;	1.1 	First Release. Written using cw_adas309_proc.pro v1.2
;		as a template.
;	1.2	Corrected ntu and ntl to integer conversion.
;
;-
;-----------------------------------------------------------------------------

FUNCTION proc307_get_val, id



                ;**** Return to caller on error ****

    ON_ERROR, 2

		 ;***************************************
                 ;****     Retrieve the state 	     ****
		 ;**** Get first_child widget id     ****
		 ;**** because state is stored there ****
		 ;***************************************

    first_child = widget_info(id, /child)
    widget_control, first_child, get_uvalue = state

		;***********************************
		;**** Get run title from widget ****
		;**** Then centre in in string  ****
		;**** of 40 characters 		****
		;***********************************

    widget_control, state.runid, get_value=title
    title = strcompress(title(0))
    title_len = strlen(title) 
    if (title_len gt 40 ) then begin 
        title = strmid(title, 0, 37)
        widget_control, state.messid, set_value="Title too long - truncated "
        widget_control, state.runid, set_value=title
	wait, 1
    endif
    pad = (40 - title_len)/2 
    spaces = '                                         '
    title(0) = strmid(spaces,0,(pad-1)) + title + strmid(spaces,0,(pad-1))

		;***********************************************
		;**** Get value of atomic mass of receiver   ***
		;***********************************************

    widget_control, state.amtext, get_value=massno
    if (num_chk(massno(0)) eq 0) then begin
        ramsno = float(massno(0))
    endif else begin
        ramsno = -99.0
    endelse
    widget_control, state.amtext, set_value=massno

                ;***********************************************
                ;**** Get value of ion temperature           ***
                ;***********************************************

    widget_control, state.ittext, get_value=iontemp
    if (num_chk(iontemp(0)) eq 0) then begin
        tiev = float(iontemp(0))
    endif else begin
        tiev = -99.0
    endelse
    widget_control, state.ittext, set_value=iontemp

                ;***********************************************
                ;**** Get value of electron temperature      ***
                ;***********************************************

    widget_control, state.ettext, get_value=eletemp
    if (num_chk(eletemp(0)) eq 0) then begin
        tev = float(eletemp(0))
    endif else begin
        tev = -99.0
    endelse
    widget_control, state.ettext, set_value=eletemp
    
                ;***********************************************
                ;**** Get value of ion density               ***
                ;***********************************************

    widget_control, state.idtext, get_value=iondens
    if (num_chk(iondens(0)) eq 0) then begin
        densz = float(iondens(0))
    endif else begin
        densz = -99.0
    endelse
    widget_control, state.idtext, set_value=iondens

                ;***********************************************
                ;**** Get value of electron density          ***
                ;***********************************************

    widget_control, state.edtext, get_value=eledens
    if (num_chk(eledens(0)) eq 0) then begin
        dens = float(eledens(0))
    endif else begin
        dens = -99.0
    endelse
    widget_control, state.edtext, set_value=eledens

                ;***********************************************
                ;**** Get value of Z effective               ***
                ;***********************************************

    widget_control, state.zftext, get_value=zefval 
    if (num_chk(zefval(0)) eq 0) then begin
        zeff = float(zefval(0))
    endif else begin
        zeff = -99.0
    endelse
    widget_control, state.zftext, set_value=zefval 

                ;***********************************************
                ;**** Get value of B magnetic                ***
                ;***********************************************

    widget_control, state.bmtext, get_value=bmaval 
    if (num_chk(bmaval(0)) eq 0) then begin
        bmag = float(bmaval(0))
    endif else begin
        bmag = -99.0
    endelse
    widget_control, state.bmtext, set_value=bmaval 

                ;***********************************************
                ;**** Get value of reference beam energy     ***
                ;***********************************************

    widget_control, state.betext, get_value=bmeval
    if (num_chk(bmeval(0)) eq 0) then begin
        bmeng = float(bmeval(0))
    endif else begin
        bmeng = -99.0
    endelse
    widget_control, state.betext, set_value=bmeval

                ;***********************************************
                ;**** Get value of upper N level             ***
                ;***********************************************

    widget_control, state.ntutext, get_value=ntuval
    if (num_chk(ntuval(0)) eq 0) then begin
        ntu = fix(ntuval(0))        
    endif else begin
        ntu = -99        
    endelse
    widget_control, state.ntutext, set_value=ntuval

                ;***********************************************
                ;**** Get value of lower N level             ***
                ;***********************************************

    widget_control, state.ntltext, get_value=ntlval
    if (num_chk(ntlval(0)) eq 0) then begin
        ntl = fix(ntlval(0))        
    endif else begin
        ntl = -99        
    endelse
    widget_control, state.ntltext, set_value=ntlval

                ;***********************************************
                ;**** Get values of cex theory/emis model    ***
                ;***********************************************

    widget_control, state.cexther, get_value=theory
    itheor = theory
    widget_control, state.cexstat, get_value=donor
    ibstat = donor
    widget_control, state.emimod, get_value=model
    iemms = model


                ;***********************************************
                ;**** Get values related to tables           ***
                ;***********************************************

    widget_control, state.tabmenu, get_value = tableval
    itable = tableval

                ;***********************************************
                ;**** Get beam energies                      ***
                ;***********************************************

    widget_control, state.betabid, get_value = bevalues
    list = where( strlen( bevalues.value(0,*)), nbmeng)
    nbmeng = fix(nbmeng)
    bmenga = fltarr(24)
    if (nbmeng ne 0) then begin
        bmenga(0:nbmeng-1) = float( bevalues.value(0,0:nbmeng-1))
    endif

                ;***********************************************
                ;**** Get ion densities                      ***
                ;***********************************************

    widget_control, state.idtabid, get_value = idvalues
    list = where( strlen( idvalues.value(0,*)), ndensz)
    ndensz = fix(ndensz)
    densza = fltarr(24)
    if (ndensz ne 0) then begin
        densza (0:ndensz-1) = float( idvalues.value(0,0:ndensz-1))
    endif

                ;***********************************************
                ;**** Get ion temperatures                   ***
                ;***********************************************

    widget_control, state.ittabid, get_value = itvalues
    list = where( strlen( itvalues.value(0,*)), ntiev)
    ntiev = fix(ntiev)
    tieva = fltarr(12)
    if (ntiev ne 0) then begin
        tieva (0:ntiev-1) = float( itvalues.value(0,0:ntiev-1))
    endif

                ;***********************************************
                ;**** Get effective Z's                      ***
                ;***********************************************

    widget_control, state.petabid, get_value = pevalues
    list = where( strlen( pevalues.value(0,*)), nzeff)
    nzeff = fix(nzeff)
    zeffa = fltarr(12)
    if (nzeff ne 0) then begin
        zeffa (0:nzeff-1) = float( pevalues.value(0,0:nzeff-1))
    endif

                ;***********************************************
                ;**** Get magnetic inductions                ***
                ;***********************************************

    widget_control, state.mitabid, get_value = mivalues
    list = where( strlen( mivalues.value(0,*)), nbmag)
    nbmag = fix(nbmag)
    bmaga = fltarr(12)
    if (nbmag ne 0) then begin
        bmaga (0:nbmag-1) = float( mivalues.value(0,0:nbmag-1))
    endif

		;***********************************************
		;**** write selected values to PS structure ****
		;***********************************************

     ps = { 			 			$
	        new   : 0, 				$
                title : title,          		$
                ramsno: ramsno, 	         	$
                tiev  : tiev  ,        		 	$
                tev   : tev   ,         		$
                densz : densz ,         		$
                dens  : dens  ,         		$
                zeff  : zeff  ,         		$
                bmag  : bmag  ,         		$
                bmeng : bmeng ,           		$
                ntu   : ntu,              		$
                ntl   : ntl,              		$
                nbmeng: nbmeng,              		$
                ndensz: ndensz,              		$
                ntiev : ntiev,              		$
                nzeff : nzeff,              		$
                nbmag : nbmag,              		$
                bmenga: bmenga,         		$
                densza: densza,         		$
                tieva : tieva,          		$
                zeffa : zeffa,          		$
                bmaga : bmaga,          		$
		itable: itable,				$
		ibstat: ibstat,				$
		iemms : iemms,				$
		itheor: itheor		 		$
              }

   
    widget_control, first_child, set_uvalue=state, /no_copy

    RETURN, ps

END

;-----------------------------------------------------------------------------

FUNCTION proc307_event, event

    COMMON blk_307, ntot, ngrnd

                ;**** Base ID of compound widget ****

    parent = event.handler

		;**********************************************
                ;**** Retrieve the user value state        ****
		;**** Get id of first_child widget because ****
		;**** user value "state" is stored there   ****
		;**********************************************

    first_child = widget_info(parent, /child)
    widget_control, first_child, get_uvalue=state,/no_copy

		;*********************************
		;**** Clear previous messages ****
		;*********************************

    widget_control,state.messid,set_value=' '

                ;************************
                ;**** Process events ****
                ;************************

    CASE event.id OF

    state.tabmenu: begin
        widget_control, state.tabmenu, get_value=itable
        CASE itable OF
            0: begin
                widget_control, state.bebase, map=1
                widget_control, state.idbase, map=0
                widget_control, state.itbase, map=0
		widget_control, state.pebase, map=0
		widget_control, state.mibase, map=0
            end
	    1: begin
                widget_control, state.bebase, map=0
                widget_control, state.idbase, map=1
                widget_control, state.itbase, map=0
		widget_control, state.pebase, map=0
		widget_control, state.mibase, map=0
	    end
	    2: begin
                widget_control, state.bebase, map=0
                widget_control, state.idbase, map=0
                widget_control, state.itbase, map=1
		widget_control, state.pebase, map=0
		widget_control, state.mibase, map=0
	    end
	    3: begin
                widget_control, state.bebase, map=0
                widget_control, state.idbase, map=0
                widget_control, state.itbase, map=0
                widget_control, state.pebase, map=1
		widget_control, state.mibase, map=0
	    end
	    4:begin
                widget_control, state.bebase, map=0
                widget_control, state.idbase, map=0
                widget_control, state.itbase, map=0
                widget_control, state.pebase, map=0
                widget_control, state.mibase, map=1
            end
        ENDCASE
    end

    state.cexther: begin
        widget_control, state.cexther, get_value = sensi
        widget_control, state.cexstat, sensitive = sensi
        widget_control, state.cexlab2, sensitive = sensi
    end

		;***********************
		;**** Cancel button ****
		;***********************

    state.cancelid: new_event = {ID:parent, TOP:event.top, 		$
				HANDLER:0L, ACTION:'Cancel'}

    state.amtext: widget_control, state.ittext, /input_focus

    state.ittext: widget_control, state.ettext,  /input_focus

    state.ettext: widget_control, state.idtext,  /input_focus

    state.idtext: widget_control, state.edtext,  /input_focus

    state.edtext: widget_control, state.zftext,  /input_focus

    state.zftext: widget_control, state.bmtext,  /input_focus

    state.bmtext: widget_control, state.betext,  /input_focus

    state.betext: widget_control, state.ntutext, /input_focus

    state.ntutext: widget_control, state.ntltext,/input_focus

    state.ntltext: widget_control, state.amtext, /input_focus

		;*********************
		;**** Menu button ****
		;*********************

    state.outid: begin
        new_event = {ID:parent, TOP:event.top, HANDLER:0L,          $
                         ACTION:'Menu'}
    end

		;*********************
		;**** Done button ****
		;*********************

    state.doneid: begin

		;***************************************
		;**** Check all user input is legal ****
		;***************************************

	error = 0

        	;********************************************
		;*** Have to restore "state" before calls ***
		;*** to proc307_get_val so it can be used ***
		;*** there.				  ***
		;********************************************

	widget_control, first_child, set_uvalue=state, /no_copy

	widget_control, event.handler, get_value=ps

		;*** reset state variable ***

	widget_control, first_child, get_uvalue=state, /no_copy

                ;***********************************************
		;**** Check to see if sensible mass entered ****
                ;***********************************************

	if (error eq 0 and (ps.ramsno le 0.0 or 			$
        ps.ramsno gt 300.0)) then begin
            error = 1
            message='Error: Invalid atomic mass number for receiver'
        endif

                ;***********************************************
		;**** Check to see if sensible ion temp     ****
                ;***********************************************

        if (error eq 0 and (ps.tiev le 0.0) ) then begin
            error = 1
            message='Error: Invalid ion temperature'
        endif

                ;***********************************************
                ;**** Check to see if sensible ion density  ****
                ;***********************************************

        if (error eq 0 and (ps.densz le 0.0) ) then begin
            error = 1
            message='Error: Invalid ion density'
        endif

                ;***********************************************
                ;**** Check to see if sensible elec temp    ****
                ;***********************************************

        if (error eq 0 and (ps.tev le 0.0) ) then begin
            error = 1
            message='Error: Invalid electron temperature'
        endif

                ;***********************************************
                ;**** Check to see if sensible elec density ****
                ;***********************************************

        if (error eq 0 and (ps.dens le 0.0) ) then begin
            error = 1
            message='Error: Invalid electron density'
        endif

                ;***********************************************
                ;**** Check to see if sensible  Z effective ****
                ;***********************************************

        if (error eq 0 and (ps.zeff le 0.0) ) then begin
            error = 1
            message='Error: Invalid Z effective'
        endif

                ;***********************************************
                ;**** Check to see if sensible B Magnetic   ****
                ;***********************************************

        if (error eq 0 and (ps.bmag le 0.0) ) then begin
            error = 1
            message='Error: Invalid B magnetic'
        endif

                ;***********************************************
                ;**** Check to see if sensible beam energy  ****
                ;***********************************************

        if (error eq 0 and (ps.bmeng le 0.0) ) then begin
            error = 1
            message='Error: Invalid reference beam energy'
        endif

                ;***********************************************
                ;**** Check to see if sensible upper level N****
                ;***********************************************

        if (error eq 0 and (ps.ntu le 0.0) ) then begin
            error = 1
            message='Error: Invalid upper N level for selected transition'
        endif
        if (error eq 0 and (ps.ntu gt ntot) ) then begin
            error = 1
            message='Error: Upper N level for selected transition is '+$
            'greater than allowed maximum'
	endif

                ;***********************************************
                ;**** Check to see if sensible lower level N****
                ;***********************************************

        if (error eq 0 and (ps.ntl le 0.0) ) then begin
            error = 1
            message='Error: Invalid lower N level for selected transition'
        endif
        if (error eq 0 and (ps.ntl lt ngrnd) ) then begin
            error = 1
            message='Error: Lower N level for selected transition is '+$
            'less than allowed minimum'
	endif
        if (error eq 0 and (ps.ntl gt ps.ntu) ) then begin
            error = 1
            message='Error: Lower N level for selected transition is '+$
            'greater than upper N level'
	endif
        if (error eq 0 and (ps.ntl eq ps.ntu) ) then begin
            error = 1
            message='Error: Both quantum numbers N are the same for '+$
	    'the selected transition'
	endif


                ;***********************************************
                ;**** Check for sensible beam energy scan   ****
                ;***********************************************

	if (error eq 0 and (ps.nbmeng eq 0)) then begin
	    error = 1 
	    message='Error: No scan values entered for beam energy'
        endif

                ;***********************************************
                ;**** Check for sensible ion density scan   ****
                ;***********************************************

        if (error eq 0 and (ps.ndensz eq 0)) then begin
            error = 1
	    message='Error: No scan values entered for ion density'
	endif

                ;***********************************************
                ;**** Check for sensible ion temp scan      ****
                ;***********************************************

	if (error eq 0 and (ps.ntiev eq 0)) then begin
            error = 1
            message='Error: No scan values entered for ion temperature'
	endif

                ;***********************************************
                ;**** Check for sensible effective Z scan   ****
                ;***********************************************

        if (error eq 0 and (ps.nzeff eq 0)) then begin
            error = 1
            message='Error: No scan values entered for plasma effective Z'
        endif

                ;***********************************************
                ;**** Check for sensible effective Z scan   ****
                ;***********************************************

        if (error eq 0 and (ps.nbmag eq 0)) then begin
            error = 1
            message='Error: No scan values entered for plasma magnetic induction'
        endif

		;**** return value or flag error ****

	if error eq 0 then begin
	    new_event = {ID:parent, TOP:event.top, HANDLER:0L, ACTION:'Done'}
        end else begin
	    widget_control, state.messid, set_value=message
	    new_event = 0L
        end

      end

    ELSE: new_event = 0L

  ENDCASE

		;*********************************************
		;*** make "state" available to other files ***
		;*********************************************

    widget_control, first_child, set_uvalue=state,/no_copy

    RETURN, new_event

END

;-----------------------------------------------------------------------------

function cw_adas307_proc, topparent, dsfull, act, 			$
                symbd, idz0, symbr, irz0, irz1, irz2,           	$
		ntot, ngrnd, bitfile,         				$
		procval=procval, uvalue=uvalue, 			$
		font_large=font_large, font_small=font_small,   	$
		edit_fonts=edit_fonts, num_form=num_form

    COMMON blk_307, ntotcom, ngrndcom
    ntotcom = ntot
    ngrndcom = ngrnd

		;**** Set defaults for keywords ****

    if not (keyword_set(uvalue)) then uvalue = 0
    if not (keyword_set(font_large)) then font_large = 			$
    '-misc-fixed-medium-r-normal--20-140-100-100-c-100-iso8859-1'
    if not (keyword_set(font_small)) then font_small = ''
    if not (keyword_set(edit_fonts)) then 				$
    edit_fonts = {font_norm:'',font_input:''}
    if not (keyword_set(num_form)) then num_form = '(e10.3)'
    if not (keyword_set(procval)) then begin
        bmarr = fltarr(6)
        osarr = intarr(10)
        os2arr=fltarr(10)
        rparr=intarr(20)
        ps = { 								$ 
		new   : 0, 						$
		title : '',             				$
                ramsno: 0.0,            				$
                tiev  : 0.0 ,           				$
                tev   : 0.0 ,           				$
                densz : 0.0 ,           				$
                dens  : 0.0 ,           				$
                zeff  : 0.0 ,           				$
                bmag  : 0.0 ,           				$
                bmeng : 0.0 ,           				$
                ntu   : 0,              				$
                ntl   : 0,              				$
                nbmeng: 0,              				$
                ndensz: 0,              				$
                ntiev : 0,              				$
                nzeff : 0,              				$
                nbmag : 0,              				$
                bmenga: fltarr(24),     				$
                densza: fltarr(24),     				$
                tieva : fltarr(12),     				$
                zeffa : fltarr(12),     				$
                bmaga : fltarr(12),     				$
		itable: 0,						$
		ibstat: 0,						$
		iemms : 0,						$
		itheor: 0						$
              }
    endif else begin
	ps = { 								$ 
		new   : procval.new,    				$
                title : procval.title,  				$
		ramsno: procval.ramsno, 				$
		tiev  :	procval.tiev,					$
		tev   : procval.tev,					$
		densz : procval.densz,					$
		dens  : procval.dens,					$
		zeff  : procval.zeff,					$
		bmag  : procval.bmag,					$
                bmeng : procval.bmeng,  				$
                ntu   : procval.ntu,    				$
                ntl   : procval.ntl,    				$
                nbmeng: procval.nbmeng, 				$
                ndensz: procval.ndensz, 				$
                ntiev : procval.ntiev,  				$
                nzeff : procval.nzeff,  				$
                nbmag : procval.nbmag,  				$
                bmenga: procval.bmenga, 				$
                densza: procval.densza, 				$
                tieva : procval.tieva,  				$
                zeffa : procval.zeffa,  				$
                bmaga : procval.bmaga,  				$
		itable: procval.itable,					$
		ibstat: procval.ibstat,					$
		iemms : procval.iemms,					$
		itheor: procval.itheor  				$
	     }
    endelse

		;********************************************************
		;**** Create the 307 Processing options/input window ****
		;********************************************************

		;**** create titled base widget ****

    parent = widget_base(topparent, UVALUE = uvalue, 			$
			title = 'ADAS307 PROCESSING OPTIONS',		$
			EVENT_FUNC = "proc307_event", 			$
			FUNC_GET_VALUE = "proc307_get_val", 		$
			/COLUMN)

		;******************************************************
		;**** Create a dummy widget just to hold value of *****
		;**** "state" variable so as not to get confused  *****
		;**** with any other values. Adopt IDL practice   *****
		;**** of using first child widget                 *****
		;******************************************************

    first_child = widget_base(parent)

    topbase = widget_base(first_child, /column)

		;***********************
		;**** add run title ****
		;***********************

    base = widget_base(topbase, /row)
    rc = widget_label(base, value='Title for Run', font=font_large)
    runid = widget_text(base, value=ps.title, xsize=38, font=font_large, /edit)

		;**** add dataset name and browse button ****

    rc = cw_adas_dsbr(topbase, dsfull, font=font_large)


 		;************************************************
 		;**** base for the 'Information' widgets     ****
	 	;************************************************

    infobase = widget_base(topbase, /row, /frame)
    infobase1 = widget_base(infobase, /column, /frame)
    label1   = widget_label(infobase1, font=font_small,			$
    value = "------------- Receiver --------------       - Neutral donor -   ")
    label2   = widget_label(infobase1, font=font_small,			$
    value = "        Nuclear   Initial      Final                  Nuclear   ")
    label3   = widget_label(infobase1, font=font_small,			$
    value = "Symbol  charge   ion charge  ion charge     Symbol    charge    ")
    label4   = widget_label(infobase1, font=font_small,			$
    value = "-------------------------------------------------------------   ")
    label5val = "                                                               "
    strput, label5val, symbr, 1
    strput, label5val, (strtrim(string(irz0),2)), 10
    strput, label5val, (strtrim(string(irz1),2)), 20
    strput, label5val, (strtrim(string(irz2),2)), 32
    strput, label5val, (strtrim(string(idz0),2)), 56
    strput, label5val, symbd, 46
    label5 = widget_label(infobase1, font=font_small, value=label5val)

                ;************************************************
                ;**** base for atomic mass no.   widgets     ****
                ;************************************************

    ambase = widget_base(infobase, /column)
    amdum = widget_label(ambase, font=font_large, value=" ")
    amlabel1 = widget_label(ambase, font=font_large,			$
    value=" Please input following receiver information:-")
    ambase2 = widget_base(ambase, /row)
    amlabel2 = widget_label(ambase2, font=font_small,                    $
    value = "    Atomic mass number of receiver ")
    if (procval.ramsno ne 0.0) then begin
        amval = string (procval.ramsno, format='(f5.1)')
    endif else begin
        amval = "  "
    endelse
    amtext = widget_text(ambase2, xsize=5,				$
    value = amval, /editable)

                ;************************************************
                ;**** Another base to hold the next two      ****
                ;************************************************

    bigbase = widget_base(topbase, /row)

		;************************************************
		;**** Base for all other parameters          ****
		;************************************************

    rightbase = widget_base(bigbase, /column)

                ;************************************************
                ;**** Base for plasma parameters             ****
                ;************************************************

    plasbase = widget_base(rightbase, /column, /frame)
    plastit = widget_label(plasbase, font=font_large,                    $
    value = "Input reference plasma parameter information:-")

                ;************************************************
                ;**** Base for ion/electron temperatures     ****
                ;************************************************

    plasbase1 = widget_base(plasbase, /row)
    itlab = widget_label(plasbase1, font=font_small,                     $
    value = "Ion temp. (eV)   :")
    if (procval.tiev ne 0.0) then begin
        itval = string (procval.tiev, format='(E7.1)')
    endif else begin
        itval = "       "
    endelse
    ittext = widget_text(plasbase1, xsize=7, /editable, value = itval)
    etlab = widget_label(plasbase1, font=font_small,                    $
    value = "Elec temp. (eV)  :")
    if (procval.tev ne 0.0) then begin
        etval = string (procval.tev, format='(E7.1)')
    endif else begin
        etval = "       "
    endelse
    ettext = widget_text(plasbase1, xsize=7, /editable, value = etval)

                ;************************************************
                ;**** Base for ion/electron densities        ****
                ;************************************************

    plasbase2 = widget_base(plasbase, /row)
    idlab = widget_label(plasbase2, font=font_small,                     $
    value = "Ion dens. (cm-3) :")
    if (procval.densz ne 0.0) then begin
        idval = string (procval.densz, format='(E7.1)')
    endif else begin
        idval = "       "
    endelse
    idtext = widget_text(plasbase2, xsize=7, /editable, value = idval)
    edlab = widget_label(plasbase2, font=font_small,                    $
    value = "Elec dens. (cm-3):")
    if (procval.dens ne 0.0) then begin
        edval = string (procval.dens, format='(E7.1)')
    endif else begin        edval = "       "
    endelse
    edtext = widget_text(plasbase2, xsize=7, /editable, value = edval)

                ;************************************************
                ;**** Base for Zeff/B Magn                   ****
                ;************************************************

    plasbase3 = widget_base(plasbase, /row)
    zflab = widget_label(plasbase3, font=font_small,                     $
    value = "Z effective      :")
    if (procval.zeff ne 0.0) then begin
        zfval = string (procval.zeff, format='(d7.2)')
    endif else begin
        zfval = "       "
    endelse
    zftext = widget_text(plasbase3, xsize=7, /editable, value = zfval)
    bmlab = widget_label(plasbase3, font=font_small,                    $
    value = "B Magn. (T)      :")
    if (procval.bmag ne 0.0) then begin
        bmval = string (procval.bmag, format='(d7.2)')
    endif else begin
        bmval = "       "    
    endelse
    bmtext = widget_text(plasbase3, xsize=7, /editable, value = bmval)

                ;************************************************
                ;**** Base for reference beam energy         ****
                ;************************************************

    bebase = widget_base(rightbase, /column, /frame)
    belab1 = widget_label(bebase, font=font_large,			$
    value = "Input reference beam parameter information:-")

    bebase2 = widget_base(bebase, /row)
    belab2 = widget_label(bebase2, font=font_small,			$
    value = "Beam energy (eV/amu):")
    if (procval.bmeng ne 0.0) then begin
        beval = string (procval.bmeng, format='(E7.1)')
    endif else begin
        beval = "       "
    endelse
    betext = widget_text(bebase2, xsize=7, /editable, value = beval)

                ;************************************************
                ;**** Base for selected transition           ****
                ;************************************************

    stbase = widget_base(rightbase, /column, /frame)
    stlab1 = widget_label(stbase, font=font_large,                      $
    value = "Input selected transition:-")
   
    stbase2 = widget_base(stbase, /row)
    stlab2 = widget_label(stbase2, font=font_small,			$
    value = "Upper N level  :")
    if (procval.ntu ne 0) then begin
        ntuval = string (procval.ntu, format='(i3)')
    endif else begin
        ntuval = "    "
    endelse
    ntutext = widget_text(stbase2, xsize=4, /editable, value = ntuval)
    stlab3 = widget_label(stbase2, font=font_small,                     $
    value = "   Lower N level  :")
    if (procval.ntl ne 0) then begin
        ntlval = string (procval.ntl, format='(i3)')
    endif else begin
        ntlval = "    "
    endelse
    ntltext = widget_text(stbase2, xsize=4, /editable, value = ntlval)

    stbase3 = widget_base(stbase)
    stlab4 = widget_label(stbase3, font=font_small,			$
    value = "Note: Maximum allowed N quantum no. : "+strtrim(string(ntot),2))
    stbase4 = widget_base(stbase)
    stlab5 = widget_label(stbase4, font=font_small,			$
    value = "      Minimum allowed N quantum no. : "+strtrim(string(ngrnd),2))

                ;************************************************
                ;**** Base for charge exchange theory        ****
                ;************************************************

    cexbase = widget_base(rightbase, /column, /frame)
    cexbase1 = widget_base(cexbase, /row)
    cexlab1 = widget_label(cexbase1, font=font_small,                   $
    value = "Select charge exchange theory : ")
    cexther = cw_bselector(cexbase1, ['Use input data set',             $
    'Use Eikonal model'], set_value = procval.itheor)
    cexbase2 = widget_base(cexbase, /row)
    cexlab2 = widget_label(cexbase2, font=font_small,                   $
    value = "Select donor state            : ")
    cexstat = cw_bselector(cexbase2, [' H  (1S)',' H  (2S)',            $
    ' H  (2P)',' HE  (1S2)',' HE  (1S2S)'], set_value = procval.ibstat)
    emibase = widget_base(cexbase, /row)
    emilab = widget_label(emibase, font=font_small,                     $
    value = "Select emission measure model : ")
    emimod = cw_bselector(emibase, ['Charge exchange             ',     $
    	'Electron impact excitation','Radiative Recombination  '],	$
	            set_value = procval.iemms)
    widget_control, cexstat, sensitive=procval.itheor
    widget_control, cexlab2, sensitive=procval.itheor

                ;************************************************
                ;**** Base for the 5 input tables            ****
                ;************************************************

    tabbase = widget_base(bigbase, /frame, /column)

                ;************************************************
                ;**** Base for table headings - always there ****
                ;************************************************

    headbase = widget_base(tabbase, /frame, /column)
    heading = widget_label(headbase, font=font_large,			$
    value = "           Input scan information:-            ")
    tabmenu = cw_bgroup(headbase, /column, /exclusive, font=font_small, $
    set_value = procval.itable, ['Beam energy scan',		$
    'Ion density scan','Ion temperature scan', 'Plasma effective Z scan',$
    'Magnetic induction scan'])

                ;************************************************
                ;**** Bases for the 5 input tables - these   ****
		;**** are mapped and unmapped according to   ****
		;**** the value of procval.itable            ****
                ;************************************************

    tabbase2 = widget_base(tabbase, /frame)
    limi = intarr(2)			;This is set to a 2-element
					;array even though only the first
					;element is used to ensure that
					;it is treated as an array and
					;not as a scalar
    limi(0) = 2

                ;************************************************
                ;**** Base for the beam energy scan          ****
                ;************************************************

    if (procval.itable eq 0) then begin
        bemap = 1
    endif else begin
        bemap = 0
    endelse
    bebase = widget_base(tabbase2, /column, map=bemap)
    tabbe = strarr(1, 24)
    if (procval.nbmeng ne 0) then begin
        tabbe(0,0:procval.nbmeng - 1) =					$
        strtrim(string(procval.bmenga(0:procval.nbmeng - 1), 		$
        format=num_form), 2)
    endif

    betabid = cw_adas_table( bebase, tabbe, coledit=[1],		$
              colhead=['Energy (eV/amu)'], /scroll,			$
              title='Beam energy scan', limits = limi,			$
              /rowskip, font_small = font_small)

                ;************************************************
                ;**** Base for the ion density scan          ****
                ;************************************************

    if (procval.itable eq 1) then begin
        idmap = 1
    endif else begin
        idmap = 0
    endelse
    idbase = widget_base(tabbase2, /column, map=idmap)
    tabid = strarr(1, 24)
    if (procval.ndensz ne 0) then begin
        tabid(0, 0:procval.ndensz-1) =					$
	strtrim(string(procval.densza(0:procval.ndensz-1),		$
        format = num_form), 2)
    endif

    idtabid = cw_adas_table( idbase, tabid, coledit=[1],		$
              colhead= ['Ion density (cm-3)'], /scroll,			$
              title='Ion density scan', limits = limi,			$
              /rowskip, font_small = font_small )

                ;************************************************
                ;**** Base for the ion temperature scan      ****
                ;************************************************

    if (procval.itable eq 2) then begin
        itmap = 1
    endif else begin
        itmap = 0
    endelse
    itbase = widget_base(tabbase2, /column, map = itmap)
    tabit = strarr(1,12)
    if (procval.ntiev ne 0) then begin
        tabit(0, 0:procval.ntiev-1) =					$
        strtrim(string(procval.tieva(0:procval.ntiev-1),		$
        format=num_form),2)
    endif

    ittabid = cw_adas_table( itbase, tabit, coledit=[1],                 $
              colhead= ['Ion temperature (eV)'], /scroll,                $
              title='Ion temperature scan', limits=limi,                 $
              /rowskip, font_small=font_small )

                ;************************************************
                ;**** Base for the plasma effective Z scan   ****
                ;************************************************

    if (procval.itable eq 3) then begin
        pemap = 1
    endif else begin
        pemap = 0
    endelse
    pebase = widget_base(tabbase2, /column, map=pemap)
    tabpe = strarr(1,12)
    if (procval.nzeff ne 0) then begin
        tabpe(0,0:procval.nzeff-1) =                                    $
        strtrim(string(procval.zeffa(0:procval.nzeff-1),                $
        format='(f7.3)'), 2)
    endif

    petabid = cw_adas_table( pebase, tabpe, coledit=[1],                $
              colhead= ['Plasma effective Z'], /scroll,                 $
              title='Plasma effective Z scan', limits=limi,             $
              /rowskip, font_small=font_small,				$
	       num_form='(f7.3)')

                ;************************************************
                ;**** Base for the plasma mag. induction scan****
                ;************************************************

    if (procval.itable eq 4) then begin
        mimap = 1
    endif else begin
        mimap = 0
    endelse
    mibase = widget_base(tabbase2, /column, map=mimap)
    tabmi = strarr(1,12)
    if (procval.nbmag ne 0) then begin
        tabmi(0,0:procval.nbmag-1) =                                    $
        strtrim(string(procval.bmaga(0:procval.nbmag-1),                $
        format='(f7.3)'), 2)
    endif

    mitabid = cw_adas_table( mibase, tabmi, coledit=[1],                $
              colhead= ['Magnetic induction (T)'], /scroll,             $
              title='Magnetic induction scan', limits=limi,             $
              /rowskip, font_small=font_small,                          $
               num_form='(f7.3)')


		;**** Error message ****

    messid = widget_label(parent, font=font_large, 			$
    value='Edit the processing options data and press Done to proceed')

		;**** add the exit buttons ****

    base = widget_base(parent, /row)
    menufile = bitfile + '/menu.bmp'
    read_X11_bitmap, menufile, bitmap1
    outid = widget_button(base, value=bitmap1)          ;menu button
    cancelid = widget_button(base, value='Cancel', font=font_large)
    doneid = widget_button(base, value='Done', font=font_large)
  
		;*************************************************
		;**** create a state structure for the pop-up ****
		;**** 		     window.                  ****
		;*************************************************

    new_state = { runid:runid,  					$
		  messid:messid, 					$
		  cancelid:cancelid,					$
		  doneid:doneid, 					$
		  outid:outid,						$
		  dsfull:dsfull,					$
		  font:font_large,					$
		  amtext:amtext,					$
                  ittext:ittext,					$
		  ettext:ettext,					$
		  idtext:idtext,					$
		  edtext:edtext,					$
                  zftext:zftext,					$
		  bmtext:bmtext,					$
		  betext:betext,					$
		  ntutext:ntutext,					$
		  ntltext:ntltext,					$
  		  cexther:cexther,					$
                  cexstat:cexstat,					$
		  cexlab2:cexlab2,					$
		  emimod:emimod,					$
		  tabmenu:tabmenu,					$
 		  betabid:betabid,					$
		  idtabid:idtabid,					$
		  ittabid:ittabid,					$
		  petabid:petabid,					$
		  mitabid:mitabid,					$
		  bebase:bebase,					$
		  idbase:idbase,					$
		  itbase:itbase,					$
		  pebase:pebase,					$
		  mibase:mibase,					$
		  num_form:num_form 					$
	        }

                 ;**** Save initial state structure ****

    widget_control, first_child, set_uvalue=new_state, /no_copy

    RETURN, parent

END

