;----------------------------------------------------------------------
;+
; PROJECT    :  ADAS
;
; NAME       :  cxfrac
;
; PURPOSE    :  To convert l resolved partial cross-sections from
;               absolute values to fractions of the n and l resolved data,
;               respectively.
;
; ARGUMENTS  :  All output arguments will be defined appropriately.
;               Inputs will be converted to correct type, if possible,
;               internally without changing calling type.
;
;                  cxfrac, mxneng, etc.
;
;               NAME      I/O    TYPE    DETAILS
;               mxneng     I     int     Maximum no. of energies.
;               mxnshl     I     int     Maximum no. of n shells.
;               nenrgy     I     int     Number of energies read.
;               nmin       I     int     Lowest n-shell for which data read.
;               nmax       I     int     Highest n-shell for which data read.
;               lsetl      I     int     Flags if l-resolved data present.
;                                          1  => l-resolved data present.
;                                          0  => l-resolved data absent.
;               signa(,)   I     float   n-resolved charge exchange cross-sections.
;                                        Units: cm2
;                                          1st dimension: energy index
;                                          2nd dimension: n-shell
; 
;               fracla(,)  I/O   float   l-resolved charge exchange cross-sections.
;                                          input : absolute values (cm2)
;                                          output: fraction of n-resolved data.
;                                             1st dimension: energy index
;                                             2nd dimension: indexed by i4idfl(n,l)
;
;
; NOTES      :  Calls fortran code.
;
;
; AUTHOR     :  Martin O'Mullane
;
; DATE       :  09/12/12
;
;
; MODIFIED:
;       1.1     Martin O'Mullane
;              	- First version.
;
;
; VERSION:
;       1.1    09/12/12
;
;-
;----------------------------------------------------------------------

PRO cxfrac, mxneng, mxnshl, nenrgy, nmin, nmax,   $
            lsetl, signa, fracla

fortdir = getenv('ADASFORT')

mxneng = long(mxneng)
mxnshl = long(mxnshl)
nenrgy = long(nenrgy)
nmin   = long(nmin)  
nmax   = long(nmax)  
lsetl  = long(lsetl) 

fracla = double(fracla)
signa  = double(signa)

dummy = CALL_EXTERNAL(fortdir+'/cxfrac_if.so','cxfrac_if',  $
                      mxneng, mxnshl, nenrgy, nmin, nmax,   $
                      lsetl, signa, fracla)


END
