; Copyright (c) 1995, Strathclyde University.
; SCCS info: Module @(#)$Header: /home/adascvs/idl/adas3xx/adaslib/cxsetp.pro,v 1.2 2004/07/06 13:11:12 whitefor Exp $ Date $Date: 2004/07/06 13:11:12 $  
;+
; PROJECT:
;       ADAS IBM MVS to UNIX conversion
;
; NAME:
;	CXSETP
;
; PURPOSE:
;	IDL communications with ADAS308 FORTRAN process via pipe.
;
; EXPLANATION:
;	This IDL routine reads data from the ADAS308 FORTRAN subroutine
;	CXSETP via a UNIX pipe.  In the original IBM version the FORTRAN
;	subroutine CXSETP put information needed by the ISPF interface
;	into the ISPF pool area for later use.  Now the same information
;	is written to IDL for use in its user unterface.
;
; USE:
;	The use of this routine is specific to ADAS306 and ADAS308.
;       See adas308.pro for an example.
;
; INPUTS:
;	PIPE	- The IDL unit number of the bi-directional pipe to the
;		  ADAS308 FORTRAN process.
;
; OPTIONAL INPUTS:
;	None
;
; OUTPUTS:
;	mxtab	- (Int) Maximum number of emissivity tables allowed.
;
;	mxgrf	- (Int) Maximum number of emissivity graphs allowed.
;
;	symbd	- (String) Element symbol of donor (for information).
;
;	idz0	- (Int) Donor nuclear charge (for information).
;
;	symbr	- (String) Element symbol of receiver (for information).
;
;	irz0	- (Int) Receiver nuclear charge (for information).
;
;	irz1	- (Int) Receiver ion initial charge (for information).
;
;	irz2	- (Int) Receiver ion final charge (for information).
;
;	ngrnd	- (Int) Minimum allowed N quantum number.
;
;	ntot	- (Int) Maximum allowed N quantum number.
;
; OPTIONAL OUTPUTS:
;	None
;
; KEYWORD PARAMETERS:
;	None
;
; CALLS:
;	None
;
; SIDE EFFECTS:
;	Reads data from a pipe.
;
; CATEGORY:
;	Adas system.
;
; WRITTEN:
;       Tim Hammond, Tessella Support Services plc, 05/06/1995
;
; MODIFIED:
;	1.1		Tim Hammond			05-06-95
;	1.2		Tim Hammond			20-06-95
;
; VERSION:
;	1.1		First release	
;	1.2		Tidied code up
;
;-----------------------------------------------------------------------------



PRO cxsetp, pipe, mxtab, mxgrf, symbd, idz0, symbr, irz0, irz1, irz2, $
            ngrnd, ntot

		;**********************************
		;**** Initialise new variables ****
		;**********************************

    itemp = 0                           ;Dummy int var used for reading
    stemp = ' '				;Dummy string used for reading


		;********************************
		;**** Read data from fortran ****
		;********************************

    readf, pipe, itemp
    mxtab = itemp
    readf, pipe, itemp
    mxgrf = itemp
    readf, pipe, stemp
    symbd = stemp
    readf, pipe, itemp
    idz0 = itemp
    readf, pipe, stemp
    symbr = stemp
    readf, pipe, itemp
    irz0 = itemp
    readf, pipe, itemp
    irz1 = itemp
    readf, pipe, itemp
    irz2 = itemp
    readf, pipe, itemp
    ngrnd = itemp
    readf, pipe, itemp
    ntot = itemp

END
