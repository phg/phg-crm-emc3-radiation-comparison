;----------------------------------------------------------------------
;+
; PROJECT    :  ADAS
;
; NAME       :  cxeiqp
;
; PURPOSE    : Calculates ECIP (exchange classical impact-parameter) cross
;              sections.
;
; ARGUMENTS  :  All output arguments will be defined appropriately.
;               Inputs will be converted to correct type, if possible,
;               internally without changing calling type.
;
;                cxeiqp
;
;               NAME      I/O    TYPE    DETAILS
;               EI()       I     double  Binding energy of state i.
;                                        Units: ryd
;               DEIJ       I     double  Transition energy (ryd).
;                                        Units: ryd
;               EM         I     double  Reduced mass for colliding particle.
;                                        Units: electron masses
;               Z          I     double  Target ion charge * charge colliding particle
;               PHI        I     double  Fij/eij where:
;                                        Fij = absorption oscillator strength;
;               SC         I     double  External global scaling factor
;               WI         I     double  Statistical weight of state i.
;               WJ         I     double  Statistical weight of state j.
;               R          I     double  Mean atomic radius (atomic units) (5n**2+1)/4z
;
;               esigma()   O     double  EI * Cross section (units: pi a0**2)
;               FLAG       O     double  1.0  Weak coupling region
;                                        0.0  Strong coupling region.
;
;
; KEYWORDS      help             prints help to screen
;
;
; NOTES      :  Calls fortran code.
;
;
; AUTHOR     :  Martin O'Mullane
;
; DATE       :  03/05/11
;
;
; MODIFIED:
;       1.1     Martin O'Mullane
;               - First version.
;
;
; VERSION:
;       1.1    03-05-2011
;
;-
;----------------------------------------------------------------------

PRO cxeiqp, ei       =  ei,       $
            deij     =  deij,     $
            mass     =  mass,     $
            z        =  z,        $
            phi      =  phi,      $
            sc       =  sc,       $
            wi       =  wi,       $
            wj       =  wj,       $
            r        =  r,        $
            esigma   =  esigma,   $
            flag     =  flag,     $
            help     =  help

; If asked for help

if keyword_set(help) then begin
   doc_library, 'cxeiqp'
   return
endif

ei     = double(ei)
deij   = double(deij)
mass   = double(mass)
z      = double(z)
phi    = double(phi)
sc     = double(sc)
wi     = double(wi)
wj     = double(wj)
r      = double(r)
flag   = -1.0D0



fortdir = getenv('ADASFORT')

num = n_elements(ei)


esigma = dblarr(num)

for  j = 0, num-1 do begin

   esigma_in = 0.0D0
   ei_in     = ei[j]

   dummy = CALL_EXTERNAL(fortdir+'/cxeiqp_if.so','cxeiqp_if',   $
                         ei_in, deij, mass, z, phi,                $
                         sc, wi, wj, r, esigma_in,                 $
                         flag)
   esigma[j] = esigma_in

endfor

if num EQ 1 then esigma = esigma[0]

END
