;----------------------------------------------------------------------
;+
; PROJECT    :  ADAS
;
; NAME       :  cxqntb
;
; PURPOSE    :
;
; ARGUMENTS  :  All output arguments will be defined appropriately.
;               Inputs will be converted to correct type, if possible,
;               internally without changing calling type.
;
;                cxqntb
;
;               NAME      I/O    TYPE    DETAILS
; REQUIRED   :  file       I     str     full name of ADAS adf01 file
;               energy     I     dbl()   beam energy components (ev/amu)
;               fraction   I     dbl()   beam fractions in energy components
;               n          I     int()   n-levels requested
; OPTIONAL   :  qcxn       O     dbl(,)  CX rate coefficient (cm^3 s^-1)
;                                          first index - component energy index
;                                          second      - n-level
;               qthrep     O     dbl()   mean qcxn coefficients for representative
;                                        n-shells (averaged over beam fractions)
;                                       (cm3 sec-1)
;               alpha      O     dbl    value of 1/n^alpha tail
;
;
;
; NOTES      :  Calls fortran code.
;
;               Designed to calculate beam driven rate (vbeam * sigma) for
;               a number of beam energy fractions (3 for H beam). Hence
;               the number of fraction must equal the number of beam energies.
;
;               Cross sections can be recovered by dividing by
;               1.384D6 * sqrt(energy).
;
; AUTHOR     :  Martin O'Mullane
;
; DATE       :  25-10-2010
;
;
; MODIFIED:
;       1.1     Martin O'Mullane
;               - First version.
;
;
; VERSION:
;       1.1    25-10-2010
;
;-
;----------------------------------------------------------------------

PRO cxqntb, file     = file,      $
            energy   = energy,    $
            fraction = fraction,  $
            n        = n,         $
            qcxn     = qcxn,      $
            qthrep   = qthrep,    $
            alpha    = alpha,     $
            help     = help

; If asked for help

if keyword_set(help) then begin
   doc_library, 'cxqntb'
   return
endif


; Sanitize the input

; file name

if n_elements(file) eq 0 then message, 'A file name must be passed'
file_acc, file, exist, read, write, execute, filetype
if exist ne 1 then message, 'adf01 file does not exist '+file
if read ne 1 then message, 'adf01 file cannot be read from this userid '+file


iunit  = 67L

a01file = string(replicate(32B, 132))
if strlen(a01file) GT 132 then message, 'Use shorter file-name' $
                          else strput, a01file, file, 0

; Required input

if n_elements(energy) EQ 0 OR $
   n_elements(fraction) EQ 0 OR $
   n_elements(n) EQ 0 then message, 'Required inputs are missing'

if n_elements(fraction) NE n_elements(energy) then $
   message, 'Number of energies must be same as number of fractions'

; Set dimensions

ndbm  = 30L
ndrep = 30L
ndlev = 550L

; Fill internal arrays

res = where(energy NE 0.0, nbeam)
if nbeam GT ndbm then message, 'Number of energies limited to ' + string(ndbm)

bmena            = dblarr(ndbm)
bmena[0:nbeam-1] = energy
bmfra            = dblarr(ndbm)
bmfra[0:nbeam-1] = fraction

inrep = n_elements(n)
if inrep GT ndrep then message, 'Number of n-shells limited to ' + string(ndrep)

nrep_in            = lonarr(ndrep+1)
nrep_in[0:inrep-1] = long(n)


qthrep_in= dblarr(ndrep+1)
qcxn_in  = dblarr(ndbm, ndrep+1)
alpha_in = 0.0D0


; Call fortran code

adasfort = getenv('ADASFORT')
if adasfort eq '' then message, 'Cannot locate ADAS binaries'


dummy = CALL_EXTERNAL(adasfort+'/cxqntb_if.so','cxqntb_if', $
                      iunit   , ndbm      , ndrep   , ndlev ,  $
                      inrep   , nrep_in   ,                    $
                      nbeam   , bmena     , bmfra    ,         $
                      qcxn_in , qthrep_in , alpha_in ,         $
                      a01file)

if arg_present(qcxn) then qcxn = qcxn_in[0:nbeam-1, 0:inrep-1]
if arg_present(qthrep) then qthrep = qthrep_in[0:inrep-1]

END
