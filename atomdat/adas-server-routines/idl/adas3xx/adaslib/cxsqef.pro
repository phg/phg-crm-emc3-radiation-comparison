;----------------------------------------------------------------------
;+
; PROJECT    :  ADAS
;
; NAME       :  cxsqef
;
; PURPOSE    :  Subroutine to evaluate Maxwell averaged effective rate
;               coefficients for charge exchange/Stark studies.
;               The source data is effective coefficients in the
;               collisional/radiative sense or effective emission
;               coefficients for photon emission but before averaging
;               over ion/atom speed distribution functions.
;  
;               The function also returns the raw eff. coefft. data for
;               verification and graphing purposes.
; 
;               The effective rate coefficient appropriate to one of
;               the particles being in a monoenergetic beam and the other
;               belonging to a Maxwell distribution may be returned.
;               The target and projectile roles may be reversed. Arbitrary
;               relative speeds are allowed.
;
; ARGUMENTS  :  All output arguments will be defined appropriately.
;               Inputs will be converted to correct type, if possible,
;               internally without changing calling type.
;
;               The argument list is the same as fortran routine except
;               that iunit is not specified.
;
;
;              NAME       I/O    TYPE    DETAILS
; REQUIRED   : a12file     I     str     adf12 filename
;              ibsel       I     long    selector for particular rate coefft.
;              nqeff       I     long    number of rates to be evaluated (when ttar>0)
;                                        a 1d array of plasma/beam conditions are
;                                        evaluated to give a vector of rates. at
;                                        the moment, epro, ttar, ti, densi, zeff &
;                                        bmag are allowed to vary along the vector.
;              epro()      I     double  incident particle energy (ev/amu)
;              ttar()      I     double  maxwell temperature of target particles (ev)
;                                        if (ttar.le.0) then rates for t=0 are
;                                        returned
;              em1         I     double  atomic mass number of first particle
;              em2         I     double  atomic mass number of second particle
;              iord        I     long    1 for 1st particle incident and monoenergetic
;                          I             2 for 2nd particle incident and monoenergetic
;              ti()        I     double  plasma ion temperature (ev)
;              densi()     I     double  plasma ion density (cm-3)
;              zeff()      I     double  plasma z effective
;              bmag()      I     double  plasma magnetic field (tesla)
; 
;              qeff        O     double  rate coefficient (cm3 sec-1)
;              nener       I/O   long    number of source data values - if
;                                        not set defaults to 24.
;              ener()      O     double  set of energies (ev/amu) for
;                                        selected source data
;              qener()     O     double  rate coeffts.(cm**3 sec-1) for
;                                        selected source data
;              csymb       O     str     adf12 file: element symbol
;              czion       O     str     adf12 file: emitting ion charge
;              cwavel      O     str     adf12 file: wavelength (A)
;              cdonor      O     str     adf12 file: donor neutral atom
;              crecvr      O     str     adf12 file: receiver nucleus
;              ctrans      O     str     adf12 file: transition
;              cfile       O     str     adf12 file: aspecific ion file source
;              ctype       O     str     adf12 file: type of emissivity
;              cindm       O     str     adf12 file: emissivity index
;              ircode      O     long    return code from subroutine:
;                                        0 => normal completion - no error detected
;                                        1 => error opening requested data set
;                                             exist - data set not connected
;                                        3 => the selected data-block 'ibsel' is out
;                                             of range or does not exist.
;
; NOTES      :  Calls fortran code.
;
;
; AUTHOR     :  Martin O'Mullane
;
; DATE       :  02/12/04
;
;
; MODIFIED:
;       1.1     Martin O'Mullane
;              	- First version.
;       1.2     Martin O'Mullane
;              	- Add comments.
;       1.3     Martin O'Mullane
;              	- Fix typo in comments.
;       1.4     Martin O'Mullane
;              	- Close iunit at end of routine.
;       1.5     Martin O'Mullane
;              	- Use a fixed unit number as g77 cannot open units
;                 above 100 as given by get_lun.
;       1.6     Martin O'Mullane
;              	- Parameter list changed for new xxdata_12 access.
;              	- Warn if old style paremeter list is used but
;                 return correct data anyway.
;
;
; VERSION:
;       1.1    02-12-2004
;       1.2    03-12-2004
;       1.3    08-02-2005
;       1.4    27-09-2005
;       1.5    28-02-2007
;       1.6    05-06-2007
;
;-
;----------------------------------------------------------------------

PRO cxsqef, a12file , ibsel  ,                  $
            nqeff   , epro   , ttar   ,         $
            em1     , em2    , iord   ,         $
            ti      , densi  , zeff   , bmag,   $
            nener   , ener   , qener  ,         $
            csymb   , czion  , cwavel ,         $
            cdonor  , crecvr , ctrans ,         $
            cfile   , ctype  , cindm  ,         $
            qeff    , ircode


; Check number of parameters - warn for old style calling convention

if n_params() EQ 23 then begin

    message, 'The parameter list of CXSQEF has increased', /continue
    message, 'It is necessary to add extra output variables', /continue
    message, 'Qeff is still returned but consider changing calling code', /continue
    
endif else begin

   if n_params() NE 26 then message, 'Not all parameters present', /continue

endelse


; Check of mandatory inputs.

if n_elements(nener) EQ 0 then begin
   message, 'nerer set to 24', /continue
   nener  = 24L
endif

nener_v = long(nener)

if ibsel LE 0 then message, 'ibsel cannot be zero'
if nqeff LE 0 then message, 'nqeff cannot be zero'


; Convert input parameters to appropriate type

; get_lun, iunit
; iunit  = long(iunit)

iunit  = 67L

ibsel  = long(ibsel)
nqeff  = long(nqeff)

epro   = double(epro)
ttar   = double(ttar)
em1    = double(em1)
em2    = double(em2)
iord   = long(iord)
ti     = double(ti)
densi  = double(densi)
zeff   = double(zeff)
bmag   = double(bmag)


; Define output parameters

ener   = dblarr(nener_v)
qener  = dblarr(nener_v,nqeff)
qeff   = dblarr(nqeff)
ircode = 0L

csymb  = 'A2'       
czion  = 'B2'    
cwavel = 'C2345678'      
cdonor = 'D23456'    
crecvr = 'E2345'    
ctrans = 'F234567'      
cfile  = 'G23456789a'       
ctype  = 'H2'    
cindm  = 'I23'        

file = string(replicate(32B, 132))
if strlen(a12file) GT 132 then message, 'Use shorter file-name' $
                          else strput, file, a12file, 0

; Combine string data into a byte array

str_vars = csymb + czion + cwavel + cdonor + crecvr + ctrans + $
           cfile + ctype + cindm  + file
str_vars = byte(str_vars)
str_vars = long(str_vars)


; Run the executable

fortdir = getenv('ADASFORT')

dummy = CALL_EXTERNAL(fortdir+'/cxsqef_if.so','cxsqef_if',   $
                      iunit, ibsel, nqeff, epro, ttar,       $
                      em1, em2, iord, ti, densi,             $
                      zeff, bmag, nener_v, ener, qener,      $
                      qeff, ircode, str_vars)


; Convert mangled strings back to calling parameter names

res = byte(str_vars)
res = string(res)

csymb  = strmid(res,  0, 2)
czion  = strmid(res,  2, 2)
cwavel = strmid(res,  4, 8)
cdonor = strmid(res, 12, 6)
crecvr = strmid(res, 18, 5)
ctrans = strmid(res, 23, 7)
cfile  = strmid(res, 30, 10)
ctype  = strmid(res, 40, 2)
cindm  = strmid(res, 42, 3)


; If old sytle calling sequence return expected data

if n_params() EQ 23 then begin

    csymb   = ctrans
    czion   = cdonor
    cwavel  = crecvr
    cdonor  = cfile 
    crecvr  = '' 
    ctrans  = cindm
    cfile   = qeff  
    ctype   = ircode
    
endif

; free_lun, iunit

END
