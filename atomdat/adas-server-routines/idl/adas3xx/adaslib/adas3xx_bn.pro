;----------------------------------------------------------------------
;+
; PROJECT    :  ADAS
;
; NAME       :  cgbnhs
;
; PURPOSE    :  Run the core bundle-n code from IDL.
;
; ARGUMENTS  :  Most of the input and output is via structures.
;
;               NAME      I/O    TYPE    DETAILS
; REQUIRED   :
;              adf01       I     str     adf01 CX cross sections
;              adf18       I     str     adf18 map from low n to high n
;              ion         I     struc
;                                        { z0 : nuclear charge
;                                          z1 : ion state
;              plasma      I     struc
;                                        { denion : ion density (cm-3)
;                                          zeff   : zeff
;                                          zimp() : impurity charge
;                                          amimp():           mass
;                                          frimp():           fraction
;                                          te     : electron temperature (K)
;                                          tion   : ion temperature (K)
;                                          bmeng  : beam energy (eV/amu)
;                                          bmdens : beam density (cm-3)
;              radiation   I     struc
;                                        { ts : temperature external radiation field (K)
;                                          w  : general radiation dilution factor
;                                          w1 : external radiation field dilution factor
;                                               for photo-ionisation form the ground level
;              collision   I     struc
;                                        { cion    : adjustment multiplier for ground ionisation
;                                          cpy     : adjustment multiplier for VR cross sections
;                                          nip     : range of delta n for IP cross section (LE 4)
;                                          intd    : order Maxwell quadrature for IP cross section (LE 3)
;                                          iprs    : beyond nip - 0 : VR (default) 1 : PR
;                                          ilow    : 0 do not access low level data, 1 access data
;                                                    (accessing low level data is not implemented)
;                                          ionip   : 0 - exclude ion impact collisions
;                                                    1 - include ion impact excitation and ionisation
;                                          nionip  : range of delta n for ion impact excitation
;                                          ilprs   : 0 - Vainshtein, 1 - Lodge-Percival-Richards
;                                          ivdisp  : 0 - ion impact at Maxwellianenergies
;                                                    1 - ion impact at displaced thermal energies according
;                                                        to the neutral beam energy parameter
;              diel        I     struc
;                                        { cor()   : Bethe correction factors
;                                          epsil() : reduced energy core transition
;                                          fij()   : absorption oscillator strength
;                                          wij()   : dilution factor of core DR transition
;              n_rep       I     struc
;                                        { nmin     : lowest n for population
;                                          nmax     : highest n
;                                          nrep()   : representative n-shells
;                                          wbrep()  : dilution factors
;                                          defect() : quantum defect
; OUTPUTS    :
;              result      O     struc
;                                        { dens     : electron density (cm-3)
;                                          brep     : representative bn factor
;                                          dexpbn   : exp(I_n/kT_e)bn for representative n-shell
;                                          pop      : population of representative n-shell
;                                          dexpte   : exp(I_n/kT_e) for representative n-shell
;                                          f1       : excitation part of b-factor
;                                          f2       : free electron recom. part of b-factor
;                                          f3       : cx recom. part of b-factor
;                                          expsh    : Saha exponential factor = Nn/(NeN+bn)
;                                          alpha    : collisional radiative recombination (cm3 s-1)
;                                          alpha_cx : collisional radiative recombination (cm3 s-1)
;                                          s        : collisional radiative ionisation (cm3 s-1)
;                                          plt      : line power (W cm3)
;                                          prb      : radiative recombination power (W cm3)
;                                          prc      : charge exchange power (W cm3)
;
; KEYWORDS   : help        I      -     Display header as help.
;
;
; NOTES      :  - Calls cgbnhs.pro for low levl calculations - dependent on call_external.
;               - Most of the data for the input structures can be
;                 found in adf25 datasets. However this routine operates
;                 on one temperature/density/beam power set, not an
;                 array as in the adf25 dataset.
;
;
; AUTHOR     :  Martin O'Mullane
;
; DATE       :  14-07-2009
;
;
; MODIFIED:
;       1.1     Martin O'Mullane
;               - First version.
;       1.2     Martin O'Mullane
;               - Add f1, f2, f3 and expsh as outputs.
;
; VERSION:
;       1.1    14-07-2009
;       1.2    22-07-2009
;
;-
;----------------------------------------------------------------------

PRO adas3xx_bn, adf01      = adf01,      $
                adf18      = adf18,      $
                ion        = ion,        $
                plasma     = plasma,     $
                radiation  = radiation,  $
                collision  = collision,  $
                diel       = diel,       $
                n_rep      = n_rep,      $
                result     = result,     $
                help       = help


; If asked for help

if keyword_set(help) then begin
   doc_library, 'adas3xx_bn'
endif


; Call cgbnhs 4 times with different conditions

n_nrep = n_elements(n_rep.nrep)
f1     = fltarr(n_nrep)
f2     = fltarr(n_nrep)
f3     = fltarr(n_nrep)
bncalc = fltarr(n_nrep)

xpop   = fltarr(2)
alpha  = fltarr(2)
hiss   = fltarr(2)

for iposnt = 1, 4 do begin

   if iposnt EQ 2 OR iposnt EQ 4 then projection = 1 else projection = 0

   if iposnt EQ 1 OR iposnt EQ 3 then radiation.w1 = 1e8 else radiation.w1 = 0.0
   if iposnt EQ 3 OR iposnt EQ 4 then plasma.bmdens = 1e7 else plasma.bmdens = 0.0

   cgbnhs, adf01      = adf01,      $
           adf18      = adf18,      $
           projection = projection, $
           ion        = ion,        $
           plasma     = plasma,     $
           radiation  = radiation,  $
           collision  = collision,  $
           diel       = diel,       $
           n_rep      = n_rep,      $
           result     = res

   for j = 0, n_nrep-1 do begin

      case iposnt of

        1 : begin
               f2[j] = res.brep[j]
               rlrb  = res.rl
               prb   = (res.rl + res.rr + res.rd + res.rb) / res.dens
            end

        2 : begin
               f1[j]    = (res.brep[j] - f2[j]) / res.pop[0]
               xpop[1]  = res.pop[0]
               rllt     = res.rl - rlrb
               plt      = rllt / res.dens
               alpha[1] = res.alfa
               hiss[1]  = res.s
            end

        3 : begin
               f3[j] = (res.brep[j] - f2[j]) / (plasma.bmdens/res.dens)
               rlrc  = res.rl - rlrb
               prc   = rlrc / plasma.bmdens
            end

        4 : begin
               bncalc[j] = f1[j] * res.pop[0] + f2[j] + f3[j] * plasma.bmdens / res.dens
               xpop[0]   = res.pop[0]
               alpha[0]  = res.alfa
               hiss[0]   = res.s
            end

     endcase

  endfor

endfor

expsh = 4.14131d-16 * res.dexpte * n_rep.nrep^2 * res.dens / (plasma.te^1.5)

plt = plt / (1e7 * xpop[1])
prb = prb / 1e7
prc = prc / 1e7

if arg_present(result) then result = { dens     : res.dens,   $
                                       brep     : res.brep,   $
                                       dexpbn   : res.dexpbn, $
                                       pop      : res.pop,    $
                                       dexpte   : res.dexpte, $
                                       f1       : f1,         $
                                       f2       : f2,         $
                                       f3       : f3,         $
                                       bncalc   : bncalc,     $
                                       expsh    : expsh,      $
                                       alpha    : alpha[1],   $
                                       alpha_cx : alpha[0],   $
                                       s        : hiss[0],    $
                                       plt      : plt,        $
                                       prb      : prb,        $
                                       prc      : prc         }

END
