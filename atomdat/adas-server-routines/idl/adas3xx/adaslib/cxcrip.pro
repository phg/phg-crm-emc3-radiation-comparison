;----------------------------------------------------------------------
;+
; PROJECT    :  ADAS
;
; NAME       :  cxcrip
;
; PURPOSE    :  Calculates electron and positive ion collision excitation
;               and deexcitation rate coefficients for dipole transitions
;               in the impact parameter approximation.
;
;               See Burgess and Summers, 1976, MON.NOT.R.AST.SOC., 174, 345
;
;               Optionally a set of incident particle energies and
;               collision strengths may be provided, in which case the
;               impact parameter theory is used to calculate the collision
;               strengths at high energy with values scaled to the highest
;               energy input collision strength.
;
; ARGUMENTS  :  All output arguments will be defined appropriately.
;               Inputs will be converted to correct type, if possible,
;               internally without changing calling type.
;
;
;               NAME      I/O    TYPE    DETAILS
;               izt        I     long    Target ion charge.
;               izc        I     long    Charge of colliding particle.
;               wi         I     double  Statistical weight of state i.
;               ei         I     double  Binding energy of state i.
;                                        Units: ryd
;               wj         I     double  Statistical weight of state j.
;               ej         I     double  Binding energy of state j.
;                                        Units: ryd
;               em         I     double  Reduced mass for colliding particle.
;                                        Units: electron masses
;               phi        I     double  Fij/Eij where:
;                                        Fij = absorption oscillator strength;
;                                        Eij = Ei-Ej = the transition energy (ryd).
;               eps()      I     double  Incident electron energies (optional).
;                                        Units: ryd
;                                        Max number: 20
;               omeg()     I     double  Collision strengths (optional).
;                                        Max number: 20
;               tva()      I     double  Temperatures (incident particle distribution).
;                                        Units: eV.
;                                        Max number: 40
;
;               rat        O     double  Ratio of last input omeg to i.p. omega.
;               excit()    O     double  Collisional excitation rate coefficients.
;                                        Units: cm3 sec-1
;                                        Dimension: temperature index.
;               dexcit()   O     double  Collisional deexcitation rate coefficients.
;                                        Units: cm3 sec-1
;                                        Dimension: temperature index.
;               gamma()    O     double  Gamma rate parameters.
;                                         Dimension: temperature index.
;
; KEYWORDS      help             prints help to screen
;
;
; NOTES      :  Calls fortran code.
;               Input temperature in eV whereas some other ADAS ECIP routines
;               used Kelvin.
;
;
; AUTHOR     :  Martin O'Mullane
;
; DATE       :  03/05/11
;
;
; MODIFIED:
;       1.1     Martin O'Mullane
;              	- First version.
;
;
; VERSION:
;       1.1    03-05-2011
;
;-
;----------------------------------------------------------------------

PRO cxcrip, izt      =  izt,      $
            izc      =  izc,      $
            wi       =  wi,       $
            ei       =  ei,       $
            wj       =  wj,       $
            ej       =  ej,       $
            mass     =  mass,     $
            phi      =  phi,      $
            eps      =  eps,      $
            omeg     =  omeg,     $
            tva      =  tva,      $
            excit    =  excit,    $
            dexcit   =  dexcit,   $
            gamma    =  gamma,    $
            help     =  help


; If asked for help

if keyword_set(help) then begin
   doc_library, 'cxcrip'
   return
endif


MXCOLL = 20L
MXTEMP = 40L

IZT    = long(IZT)
IZC    = long(IZC)
WI     = double(WI)
EI     = double(EI)
WJ     = double(WJ)
EJ     = double(EJ)
mass   = double(mass)
PHI    = double(PHI)

if n_elements(eps) GT 0 then begin
   NCOLL  = long(n_elements(omeg))
   EPS    = double(EPS)
   OMEG   = double(OMEG)
endif else begin
   NCOLL  = 0L
   EPS    = 0.0D0
   OMEG   = 0.0D0
endelse

NTEMP  = long(n_elements(tva))
TVA_in = dblarr(mxtemp)
tva_in[0:ntemp-1] = double(tva)

if ntemp EQ 0 then message, 'Supply at least one temperature'

RAT    = 0.0D0
QI     = dblarr(mxtemp)
QJ     = dblarr(mxtemp)
GA     = dblarr(mxtemp)


fortdir = getenv('ADASFORT')

dummy = CALL_EXTERNAL(fortdir+'/cxcrip_if.so','cxcrip_if', $
                      mxcoll, mxtemp, izt, izc, wi,        $
                      ei, wj, ej, mass, phi,               $
                      ncoll, eps, omeg, ntemp, tva_in,     $
                      rat, qi, qj, ga)

if arg_present(excit)  then excit  = qi[0:ntemp-1]
if arg_present(dexcit) then dexcit = qj[0:ntemp-1]
if arg_present(gamma)  then gamma  = ga[0:ntemp-1]

END
