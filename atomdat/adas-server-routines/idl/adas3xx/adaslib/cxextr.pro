;----------------------------------------------------------------------
;+
; PROJECT    :  ADAS
;
; NAME       :  cxextr
;
; PURPOSE    :  
;
; ARGUMENTS  :  All output arguments will be defined appropriately.
;               Inputs will be converted to correct type, if possible,
;               internally without changing calling type.
;
;                cxextr
;
;               NAME      I/O    TYPE    DETAILS
; REQUIRED   :
;               mxneng     I     int     Maximum no. of energies.
;               mxnshl     I     int     Maximum no. of n shells.
;               nminf      I     int     Lowest n-shell for which data read.
;               nmaxf      I     int     Highest n-shell for which data read.
;               nenrgy     I     int     Number of energies in dataset.
;               lparms     I     int     Flags if l-splitting parameters present.
;                                           1 => l-splitting parameters present.
;                                           0 => l-splitting parameters absent.
;               alphaa()   I     float   Extrapolation parameter alpha.
;                                          dimension: energy index
;               lforma()   I     int     Parameters for calculating l-res x-sec.
;                                          dimension: energy index
;               xlcuta()   I     float   Parameters for calculating l-res x-sec.
;                                          dimension: energy index
;               pl2a()     I     float   Parameters for calculating l-res x-sec.
;                                          dimension: energy index
;               pl3a()     I     float   Parameters for calculating l-res x-sec.
;                                          dimension: energy index
;
;               xsecna(,)  I/O   float   N-resolved charge exchange cross-sections.
;                                          units: cm2
;                                            1st dimension: energy index
;                                            2nd dimension: n-shell
;               fracla(,)  I/O   float   L-resolved charge exchange cross-sections
;                                        Expressed as fraction of corresponding
;                                        N-resolved cross-section.
;                                            1st dimension: energy index
;                                            2nd dimension: referenced by i4idfl(n,l)
;
;
; NOTES      :  Calls fortran code.
;
;
; AUTHOR     :  Martin O'Mullane
;
; DATE       :  10/12/12
;
;
; MODIFIED:
;       1.1     Martin O'Mullane
;              	- First version.
;
;
; VERSION:
;       1.1    10/12/12
;
;-
;----------------------------------------------------------------------

PRO cxextr, mxneng , mxnshl    , nminf  , nmaxf  , $
            nenrgy , il_lparms , alphaa , lforma , $
            xlcuta , pl2a      , pl3a   , xsecna , $
            fracla

fortdir = getenv('ADASFORT')

lforma    = long(lforma)
mxneng    = long(mxneng)
mxnshl    = long(mxnshl)
nenrgy    = long(nenrgy)
nmaxf     = long(nmaxf) 
nminf     = long(nminf)
il_lparms = long(il_lparms)

alphaa = double(alphaa)
fracla = double(fracla)
pl2a   = double(pl2a)  
pl3a   = double(pl3a)  
xlcuta = double(xlcuta)
xsecna = double(xsecna)

dummy = CALL_EXTERNAL(fortdir+'/cxextr_if.so','cxextr_if',    $
                      mxneng , mxnshl    , nminf  , nmaxf  ,  $
                      nenrgy , il_lparms , alphaa , lforma ,  $
                      xlcuta , pl2a      , pl3a   , xsecna ,  $
                      fracla)


END
