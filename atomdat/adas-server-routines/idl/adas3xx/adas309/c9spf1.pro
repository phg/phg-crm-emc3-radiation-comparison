; Copyright (c) 1995, Strathclyde University.
; SCCS Info: Module @(#)$Header: /home/adascvs/idl/adas3xx/adas309/c9spf1.pro,v 1.4 2004/07/06 11:57:45 whitefor Exp $ Date $Date: 2004/07/06 11:57:45 $      
;
;+
; PROJECT:
;       ADAS IBM MVS to UNIX conversion
;
; NAME:
;	C9SPF1
;
; PURPOSE:
;	IDL user interface and communications with ADAS309 FORTRAN
;	process via pipe.
;
; EXPLANATION:
;	In this routine the 'Output Options' part of the
;	interface is invoked.  The resulting action of the user's
;	is written to the FORTRAN via the pipe.
;	Communications are with the FORTRAN subroutine C9SPF1.
;	Note that the usual large amount of FORTRAN-IDL pipe 
;	communications in these routines has been removed due to the
;	fact that there are two FORTRAN routines rather than the usual
;	one and the communications are therefore handled elsewhere.
;
; USE:
;	The use of this routine is specific to ADAS309, See adas309.pro.
;
; INPUTS:
;	PIPE	- The IDL unit number of the bi-directional pipe to the
;		  ADAS309 FORTRAN process.
;
;	DSFULL  - The name of the data set being analyzed.
;
;	VALUE	- A structure which determines the initial settings of
;		  the dataset selection widget.  The initial value is
;		  set in adas309.pro.  VALUE is passed un-modified
;		  through to cw_adas_in.pro, see that routine for a full
;		  description.
;
;	HEADER  - Header information used for text output.
;
;       BITFILE - String; the path to the dirctory containing bitmaps
;                 for the 'escape to series menu' button.
;
; OPTIONAL INPUTS:
;	None.
;
; OUTPUTS:
;	LPEND	- Integer; indicates user action on leaving output
;		  selections.  lpend is set to 1 if the user exited
;		  with the 'Cancel' button, 0 if the user exited
;		  with 'Run Now' and 2 if the user exited with 'Run in 
;		  Batch'.
;
;	VALUE	- On output the structure records the final settings of
;		  the output selection widget if the user pressed either
;		  of the 'Run' buttons otherwise it is not changed from input.
;
;       GOMENU  - Int; flag - set to 1 if user has selected 'escape direct
;                 to series menu , otherwise 0
;
; OPTIONAL OUTPUTS:
;	None.
;
; KEYWORD PARAMETERS:
;
;	FONT	- Supplies the font to be used for the interface widgets.
;
; CALLS:
;	ADAS309_OUT	User interface - output options.
;
; SIDE EFFECTS:
;	This routine communicates with the ADAS309 FORTRAN process
;	via a UNIX pipe.
;
; CATEGORY:
;	Adas system.
;
; WRITTEN:
;       Tim Hammond, Tessella Support Services plc, 29-jun-1995
;
; MODIFIED:
;	1.1	Tim Hammond  				29-06-95
;	1.2	Tim Hammond				10-07-95
;       1.3     Tim Hammond                             10-07-95
;	1.4	William Osborn				05-07-96
;
; VERSION:
;	1.1	First release.
;	1.2	Tidied up comments and code.
;       1.3     Included error trapping
;	1.4	Added menu button
;-
;-----------------------------------------------------------------------------

PRO   c9spf1, pipe, lpend, value, dsfull, header, bitfile, gomenu,	$
              FONT=font

                ;**** Return to caller on error ****

    ON_ERROR, 2

                ;**** Set defaults for keywords ****

    IF NOT (KEYWORD_SET(font)) THEN font = ''

		;**** Define some variables before read from pipe ****


		;**************************************
		;**** Pop-up output options widget ****
		;**************************************

    outtitle = "ADAS309 OUTPUT OPTIONS"
    adas309_out, value, dsfull, action, bitfile, FONT=font

		;*************************************************
		;**** Act on the output from the widget       ****
		;**** There are three    possible actions     ****
		;**** 'Cancel', 'Run Now' and 'Run in Batch'. ****
		;*************************************************

    if action eq 'Run Now' then begin
        lpend = 0
    endif else if action eq 'Run In Batch' then begin
        lpend = 2
    endif else if action eq 'Menu' then begin
	lpend = 0
	gomenu = 1
    endif else begin
        lpend = 1
    end

		;*******************************
		;**** Write data to fortran ****
		;*******************************

    printf, pipe, lpend


		;**************************************************
		;**** Set messages and settings ready for when ****
		;**** output window is re-displayed.           ****
		;**************************************************

    if (lpend eq 0) or (lpend eq 2) then begin
        if value.texout eq 1 then begin
            value.texdef = value.texdsn
            if value.texrep ge 0 then value.texrep = 0
            value.texmes = 'Output written to file.'
        endif
	if value.datout eq 1 then begin
            if value.datrep ge 0 then value.datrep = 0
	    value.datmes = 'Output written to file.'
        endif
    endif

END
